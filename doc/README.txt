Install easy_install to install pip as root
	easy_install-3.3 pip

In ../tests/ run to install python requirements as root
	pip install -r requirements.txt

Read install.txt file in this directory to postgresql :)

RPM dependencies
	avahi-0.6.25-12
	avahi-autoipd-0.6.25-12
	avahi-glib-0.6.25-12
	avahi-libs-0.6.25-12
	bash-4.1.2-15
	ca-certificates-2013.1.95-65.1
	ccid-1.3.9-6
	cpio-2.10-11
	cups-1.4.2-50
	cups-libs-1.4.2-50
	device-mapper-multipath-0.4.9-72
	device-mapper-multipath-libs-0.4.9-72
	e2fsprogs-1.41.12-18
	e2fsprogs-libs-1.41.12-18
	firefox-24.6.0-1
	gdb-7.2-60
	GeoIP-1.4.8-1
	GeoIP-devel-1.4.8-1
	ghc-xmonad-contrib-0.10-7.1
	ghc-xmonad-contrib-devel-0.10-7.1
	ghc-xmonad-devel-0.10-3.4.2
	git-all-1.7.1-3
	git-cvs-1.7.1-3
	git-email-1.7.1-3
	git-gui-1.7.1-3
	gitk-1.7.1-3
	git-svn-1.7.1-3
	glib-1.2.10-33
	glibc-2.12-1.132
	glibc-common-2.12-1.132
	glibc-devel-2.12-1.132
	glibc-headers-2.12-1.132
	glibc-static-2.12-1.132
	gnome-icon-theme-2.28.0-2
	gnome-themes-2.28.1-6
	gnome-vfs2-2.24.2-6
	gnutls-devel-2.8.5-14
	gpg-pubkey-04bbaa7b-4c881cbf
	gpg-pubkey-66fd4949-4803fe57
	gpm-1.20.6-12
	grub-0.97-83
	gtk2-engines-2.18.4-5
	hmaccalc-0.9.12-1
	httpd-2.2.15-30
	httpd-devel-2.2.15-30
	httpd-manual-2.2.15-30
	httpd-tools-2.2.15-30
	imake-1.0.2-11
	imlib2-1.4.4-1
	initscripts-9.03.40-2
	inotify-tools-3.14-1
	iptstate-2.2.2-4
	java-1.7.0-openjdk-1.7.0.55-2.4.7.1
	jline-0.9.94-0.8
	jwhois-4.0-19
	kernel-2.6.32-431.20.3
	kernel-firmware-2.6.32-431.20.3
	kernel-headers-2.6.32-431.20.3
	kexec-tools-2.0.0-273
	kpartx-0.4.9-72
	ksh-20120801-10
	lftp-4.0.9-1
	libart_lgpl-2.3.20-5.1
	libbonobo-2.24.2-5
	libbonoboui-2.24.2-3
	libcgroup-0.40.rc1-5
	libcom_err-1.41.12-18
	libcom_err-devel-1.41.12-18
	libcurl-devel-7.19.7-37
	libdbi-0.8.3-4
	libdrm-devel-2.4.45-2
	libevent-2.0.12-1
	libFS-1.0.4-2
	libgcrypt-devel-1.4.5-11
	libglade2-2.6.4-3.1
	libgnome-2.28.0-11
	libgnomecanvas-2.26.0-4
	libgnomeui-2.24.1-4
	libgpg-error-devel-1.7-4
	libhugetlbfs-2.12-2
	libicu-4.2.1-9.1
	libicu-devel-4.2.1-9.1
	libidn-devel-1.18-2
	libmng-1.0.10-4.1
	libnetfilter_conntrack-0.0.100-2
	libnfnetlink-1.0.0-1
	libpqxx-4.0.1-1
	librabbitmq-0.5.1-1
	librabbitmq-devel-0.5.1-1
	librabbitmq-tools-0.5.1-1
	libselinux-python-2.0.94-5.3
	libsmbclient-3.6.9-168
	libss-1.41.12-18
	libtool-2.2.6-15.5
	libvirt-0.10.2-29
	libvirt-client-0.10.2-29
	libvirt-python-0.10.2-29
	libwmf-0.2.8.4-22
	libXdamage-devel-1.1.3-4
	libXdmcp-devel-1.1.1-3
	libXfixes-devel-5.0-3
	libxml2-devel-2.7.6-14
	libxslt-devel-1.1.26-2
	libXxf86vm-devel-1.1.2-2
	libyaml-devel-0.1.6-1
	logwatch-7.3.6-52
	ltrace-0.5-23.45svn
	lzo-2.03-3.1
	mcstrans-0.3.1-4
	memcached-1.4.4-3
	mesa-libGL-devel-9.2-0.5
	microcode_ctl-1.17-17
	mkbootdisk-1.5.5-3
	mobile-broadband-provider-info-1.20100122-2
	ModemManager-0.4.0-5
	mod_python-3.3.1-16
	mod_security-2.7.3-3
	mod_ssl-2.2.15-30
	mutt-1.5.20-4
	mx-3.1.1-6
	mysql-5.1.73-3
	ncurses-devel-5.7-3
	neon-devel-0.29.3-3
	net-snmp-5.5-49
	net-snmp-libs-5.5-49
	net-snmp-utils-5.5-49
	NetworkManager-0.8.1-66
	NetworkManager-glib-0.8.1-66
	nfs-utils-lib-1.1.5-6
	nmap-5.51-3
	nscd-2.12-1.132
	nspr-4.10.2-1
	nspr-devel-4.10.2-1
	nss-3.15.3-6
	nss-devel-3.15.3-6
	nss-softokn-3.14.3-10
	nss-softokn-devel-3.14.3-10
	nss-softokn-freebl-3.14.3-10
	nss-softokn-freebl-devel-3.14.3-10
	nss-sysinit-3.15.3-6
	nss-tools-3.15.3-6
	nss-util-3.15.3-1
	nss-util-devel-3.15.3-1
	numad-0.5-9.20130814git
	oddjob-0.30-5
	openbox-3.5.0-4
	openbox-libs-3.5.0-4
	openjade-1.3.2-36
	opensp-1.5.2-12.1
	openssl-1.0.1e-16
	openssl-devel-1.0.1e-16
	ORBit-0.5.17-30
	patchutils-0.3.1-3.1
	pax-3.4-10.1
	pcre-devel-7.8-6
	perl-Authen-SASL-2.13-2
	perl-CGI-3.51-136
	perl-Chart-2.4.2-3
	perl-Class-Singleton-1.4-6
	perl-Compress-Raw-Zlib-2.021-136
	perl-Compress-Zlib-2.021-136
	perl-Convert-ASN1-0.22-1
	perl-Convert-BinHex-1.119-10.1
	perl-Crypt-SSLeay-0.57-16
	perl-Date-Manip-6.24-1
	perl-DateTime-0.5300-2
	perl-DateTime-Format-DateParse-0.05-4
	perl-DBD-MySQL-4.013-3
	perl-DBD-Pg-2.15.1-4
	perl-DBD-SQLite-1.27-3
	perl-DBI-1.609-4
	perl-Digest-HMAC-1.01-22
	perl-Digest-SHA1-2.12-2
	perl-Digest-SHA-5.47-136
	perl-Email-Date-Format-1.002-5
	perl-GD-2.44-3
	perl-GSSAPI-0.26-6
	perl-HTML-Parser-3.64-2
	perl-HTML-Scrubber-0.08-7
	perl-HTML-Tagset-3.20-4
	perl-IO-Compress-Base-2.021-136
	perl-IO-Compress-Zlib-2.021-136
	perl-IO-Socket-SSL-1.31-2
	perl-IO-String-1.08-9
	perl-IO-stringy-2.110-10.1
	perl-LDAP-0.40-1
	perl-libwww-perl-5.833-2
	perl-List-MoreUtils-0.22-10
	perl-Mail-Sendmail-0.79-12
	perl-MailTools-2.04-4
	perl-MIME-Lite-3.027-2
	perl-MIME-tools-5.427-4
	perl-MIME-Types-1.28-2
	perl-Net-LibIDN-0.12-3
	perl-Net-SMTP-SSL-1.01-4
	perl-Net-SSLeay-1.35-9
	perl-Params-Validate-0.92-3
	perl-SOAP-Lite-0.710.10-3
	perl-TermReadKey-2.30-13
	perl-Test-Simple-0.92-136
	perl-Text-Iconv-1.7-6
	perl-TimeDate-1.16-11.1
	perl-WWW-Curl-4.09-3
	perl-XML-Filter-BufferText-1.01-8
	perl-XML-LibXML-1.70-5
	perl-XML-NamespaceSupport-1.10-3
	perl-XML-Parser-2.36-7
	perl-XML-SAX-0.96-7
	perl-XML-SAX-Writer-0.50-8
	perl-YAML-Syck-1.07-4
	phonon-backend-gstreamer-4.6.2-28
	php-5.3.3-27
	php-cli-5.3.3-27
	php-common-5.3.3-27
	php-devel-5.3.3-27
	php-gd-5.3.3-27
	php-imap-5.3.3-27
	php-mbstring-5.3.3-27
	php-mysql-5.3.3-27
	php-pdo-5.3.3-27
	php-pear-1.9.4-4
	php-pecl-igbinary-1.1.1-3
	php-pecl-redis-2.2.5-1
	php-pgsql-5.3.3-27
	php-snmp-5.3.3-27
	php-soap-5.3.3-27
	php-xml-5.3.3-27
	plymouth-0.8.3-27
	plymouth-core-libs-0.8.3-27
	plymouth-scripts-0.8.3-27
	ppp-2.4.5-5
	procmail-3.22-25.1
	pulseaudio-esound-compat-0.9.21-14
	pylint-0.21.1-2
	python-crypto-2.0.1-22
	python-devel-2.6.6-52
	python-logilab-astng-0.20.1-1
	python-logilab-common-0.57.1-1
	python-numeric-24.2-14
	python-pip-1.3.1-4
	python-setuptools-0.6.10-3
	pyxdg-0.18-1
	qemu-img-0.12.1.2-2.415
	qt3-3.3.8b-30
	qt-4.6.2-28
	qt-sqlite-4.6.2-28
	qt-x11-4.6.2-28
	ratpoison-1.4.5-2
	rdist-6.1.5-49
	readline-devel-6.0-4
	recode-3.6-28.1
	recode-devel-3.6-28.1
	redhat-lsb-4.0-7
	redhat-lsb-compat-4.0-7
	redhat-lsb-core-4.0-7
	redhat-lsb-graphics-4.0-7
	redhat-lsb-printing-4.0-7
	redhat-menus-14.0.0-3
	redis-2.8.0-1
	rhpl-0.221-2
	rp-pppoe-3.10-10
	rrdtool-1.3.8-6
	rsh-0.17-60
	ruby-1.8.7.352-13
	ruby-devel-1.8.7.352-13
	rubygems-1.3.7-5
	ruby-irb-1.8.7.352-13
	ruby-libs-1.8.7.352-13
	ruby-rdoc-1.8.7.352-13
	samba-common-3.6.9-168
	samba-winbind-3.6.9-168
	samba-winbind-clients-3.6.9-168
	sendmail-8.14.4-8
	setools-3.3.7-4
	setools-console-3.3.7-4
	setools-gui-3.3.7-4
	sqlite-devel-3.6.20-1
	subversion-perl-1.6.11-10
	svrcore-4.0.4-5.1
	swig-1.3.40-6
	symlinks-1.4-2.1
	sysstat-9.0.4-22
	system-gnome-theme-60.0.2-1
	system-icon-theme-6.0.0-2
	talk-0.17-36
	telnet-0.17-47
	tmux-1.6-3
	tokyocabinet-1.4.33-6
	tree-1.5.3-2
	tzdata-2014e-1
	tzdata-java-2014e-1
	urlview-0.9-7
	valgrind-3.8.1-3.2
	vmware-open-vm-tools-common-8.3.12-493255
	vmware-open-vm-tools-kmod-8.3.12-493255
	vmware-open-vm-tools-nox-8.3.12-493255
	vmware-tools-common-8.3.12-493255
	vmware-tools-nox-8.3.12-493255
	wpa_supplicant-0.7.3-4
	xmonad-0.10-3.4.2
	xmonad-basic-0.10-3.4.2
	xmonad-config-0.10-3.4.2
	xmonad-core-0.10-3.4.2
	xorg-x11-apps-7.6-6
	xulrunner-17.0.10-1
	ypbind-1.20.4-30
	yp-tools-2.9-12
	zsh-4.3.10-7


To install nose and selenium for webdriver must exec this commands in console:
1.- Install Python 2.7
% wget http://python.org/ftp/python/2.7.6/Python-2.7.6.tar.xz
% tar xf Python-2.7.6.tar.xz
% cd Python-2.7.6
% ./configure --prefix=/usr/local --enable-unicode=ucs4 --enable-shared LDFLAGS="-Wl,-rpath /usr/local/lib"
% make && make altinstall
% cd ..

2.- Intall PiP
% wget --no-check-certificate https://pypi.python.org/packages/source/s/setuptools/setuptools-1.4.2.tar.gz
% tar xzf setuptools-1.4.2.tar.gz
% cd setuptools-1.4.2
% python2.7 setup.py install
% curl https://raw.githubusercontent.com/pypa/pip/master/contrib/get-pip.py | python2.7 -
% pip2.7 install distribute ecdsa Fabric lazy nose paramiko psycopg2 pycrypto redis selenium setuptools unittest2 wsgiref 
% pip2.7 install git+https://github.com/arkanus/nose-selenium.git
% pip2.7 install git+https://github.com/arkanus/selenium-sunbro.git







