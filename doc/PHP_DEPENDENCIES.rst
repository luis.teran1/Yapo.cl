Module dependencies for Yapo.cl PHP apps
========================================
These are the modules required for the Yapo.cl php apps, they must be installed preferably from the OSs package manager.

    php-gd
    php-soap
    php-pgsql
