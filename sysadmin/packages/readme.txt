---------
These packages provides the scripts to check the hardware of the HP servers.
It's needed in production in some NAGIOS test:

- hpacucli
- hp-health
