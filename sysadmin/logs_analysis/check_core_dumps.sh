#!/bin/bash

DATE=$1
#DATE=$(date +%Y%m%d --date="1 days ago")
DIRS="/var/empty/bapache"
DIRS+=" /var/empty/trans"

for dir in $DIRS; do
	if [ -d "$dir" ]; then
		TOTAL=$(ls -lh --time-style=+date=%Y%m%d $dir | grep $DATE | wc -l)
		if [ $TOTAL != "0" ]; then
			echo "dir: $dir, NEW core dumps!"
			ls -lh --time-style="+date=%Y-%m-%d_%H:%M:%S" $dir | grep `date +%Y-%m-%d --date="1 days ago"`
		else
			echo "dir: $dir, No core dumps"
		fi
	else
		echo "dir: $dir, does not exists"
	fi
done;
