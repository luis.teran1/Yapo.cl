import re
import sys

dateold = str(sys.argv[1])
datenew = str(sys.argv[2])

def read_log(log):
    result = {}
    for line in log.split("\n"):
        try:
		l = re.search("[0-9]* :\w*", line).group(0)
		value, key = l.split(" ")
		result[key] = int(value)
	except AttributeError:
		pass
    return result

if __name__ == "__main__":
	log0 = open("daily_trans_errors/"+dateold+"_all_trans_errors.log").read()
	log1 = open("daily_trans_errors/"+datenew+"_all_trans_errors.log").read()
	log0 = read_log(log0)
	log1 = read_log(log1)

	# detecting new errors
	for k in log1.keys():
		if not log0.has_key(k):
			print "%s, %d times" % (k, log1[k])
