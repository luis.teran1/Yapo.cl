#!/bin/bash

LOG_DIR=/opt/logs
if [ $# -eq 0 ] ; then
  DATE=$(date +%Y%m%d --date="1 days ago")
else
  DATE=$1
fi

# prepare environment
mkdir -p ~/${DATE}


############################################
############ core dumps ####################
############################################
echo -e "CORE DUMPS"
echo -e ""

ssh ch2  "sh /tmp/check_core_dumps.sh $DATE" > ~/$DATE/${DATE}_core_dump_ch2.log
ssh ch3  "sh /tmp/check_core_dumps.sh $DATE" > ~/$DATE/${DATE}_core_dump_ch3.log
ssh ch4  "sh /tmp/check_core_dumps.sh $DATE" > ~/$DATE/${DATE}_core_dump_ch4.log
ssh ch10 "sh /tmp/check_core_dumps.sh $DATE" > ~/$DATE/${DATE}_core_dump_ch10.log

cat ~/$DATE/${DATE}_core_dump_ch2.log ~/$DATE/${DATE}_core_dump_ch3.log ~/$DATE/${DATE}_core_dump_ch4.log ~/$DATE/${DATE}_core_dump_ch10.log

############################################
############ postgres ######################
############################################
echo -e "POSTGRES"
echo -e ""

# postgres is logging slow queries durations, notices, warnings and errors. We should check all this information in the future.
egrep -o "duration.*" $LOG_DIR/postgres/$DATE.log | sort -k2 -nr | uniq -c > ~/$DATE/$DATE"_postgres_duration.log"

echo -e "Queries duration:"
head -n 10 ~/$DATE/${DATE}_postgres_duration.log
echo -e ""

############################################
############ trans    ######################
############################################
echo -e "TRANS"
echo -e ""

# get all BOS errors.
grep "\<BOS\>" $LOG_DIR/trans/$DATE.log > ~/$DATE/${DATE}_trans_bos.txt

echo -e "BOS errors"
cat ~/$DATE/${DATE}_trans_bos.txt
echo -e ""

# get differences between previous days trans errors.
egrep -o ":ERROR_.*" $LOG_DIR/trans/$DATE.log | sort -n | uniq -c > daily_trans_errors/${DATE}_all_trans_errors.log
python trans_error_log_reader.py 20130424 $DATE > ~/$DATE/${DATE}_new_trans_errors.txt 

echo -e "New trans errors"
cat ~/$DATE/${DATE}_new_trans_errors.txt
echo -e ""

# get all non trans errors (some false alarm strings are removed). BOS errors are looked separately.
cat $LOG_DIR/trans/$DATE.log | grep -i "ERROR" | grep -v " BOS " | grep -vi -f false_alarm_strings.txt | grep -v -f trans_errors.txt | grep -v -f known_trans_errors.txt > ~/$DATE/${DATE}_trans_manual_review.txt

echo -e "File ~/$DATE/${DATE}_trans_manual_review.txt has to be manually reviewed"
echo -e ""

############################################
############ aindex   ######################
############################################
#echo -e "AINDEX"
#echo -e ""

cat $LOG_DIR/{aindex,index_merge}/$DATE.log | egrep "(ERROR|WARNING)" | sed "s/.*\((.*).*\) in document id:.*/\1/" | sort | uniq -c | sort -k1 -nr > ~/$DATE/${DATE}_aindex_errors.txt

echo -e "AINDEX errors"
head -n 5 ~/$DATE/${DATE}_aindex_errors.txt
echo -e ""

############################################
############ asearch  ######################
############################################
#echo -e "ASERCH"
#echo -e ""
cat $LOG_DIR/asearch/$DATE.log | egrep "(ERROR|WARNING)" | grep -vi -f known_asearch_errors.txt | sed "s/.*\(asearch: .*\)/\1/" | sort | uniq -c | sort -k1 -nr > ~/$DATE/${DATE}_asearch_errors.txt

echo -e "ASEARCH errors"
head -n 10 ~/$DATE/${DATE}_asearch_errors.txt
echo -e ""

############################################
############ bapache  ######################
############################################
echo -e "BAPACHE"

# Look for "File does not exist" errors
grep "File does not exist" $LOG_DIR/wwwerror/$DATE.log | grep -v -f known_missing_files.txt | sed "s/.*File does not exist: \(\(\/[[:graph:]]*\)*\).*/\1/" | sort | uniq -c | sort -k1 -nr > ~/$DATE/${DATE}_bapache_no_files.txt

echo -e 'Error: "File does not exist:"'
head -n 5 ~/$DATE/${DATE}_bapache_no_files.txt
echo -e ""

# Look for "client denied by server configuration" errors
grep "client denied by server configuration" $LOG_DIR/wwwerror/$DATE.log | sed "s/.*client denied by server configuration: \(\(\/[[:graph:]]*\)*\).*/\1/" | sort | uniq -c | sort -k1 -nr > ~/$DATE/${DATE}_bapache_client_denied.txt

echo -e 'Error: "client denied by server configuration"'
head -n 5 ~/$DATE/${DATE}_bapache_client_denied.txt
echo -e ""

# Look for "Unable to open file" errors
grep "Unable to open file" $LOG_DIR/wwwerror/$DATE.log | sed "s/.*Unable to open file: '\(\(\/[[:graph:]]*\)*\).*/\1/" | sort | uniq -c | sort -k1 -nr > ~/$DATE/${DATE}_bapache_cant_read.txt

echo -e 'Error: "Unable to open file"'
head -n 5 ~/$DATE/${DATE}_bapache_cant_read.txt
echo -e ""

# Look for "error reading the headers" errors
grep "error reading the headers" $LOG_DIR/wwwerror/$DATE.log | wc -l > ~/$DATE/${DATE}_bapache_error_headers.txt

echo 'Error: "error reading the headers" ocurred '
cat ~/$DATE/${DATE}_bapache_error_headers.txt
echo -e ""

# Look for signal errors
grep -f sig_errors.txt $LOG_DIR/wwwerror/$DATE.log > ~/$DATE/${DATE}_bapache_sig_errors.txt

echo -e "Signal errors:"
head -n 5 ~/$DATE/${DATE}_bapache_sig_errors.txt
echo -e ""

# Look for uncategorised errors
grep -v -f apache_categories_checked.txt $LOG_DIR/wwwerror/$DATE.log | grep -v -f sig_errors.txt | grep -vi -f known_bapache_errors.txt |  grep -v "Showing error_page" > ~/$DATE/${DATE}_bapache_manual_review.txt
echo -e "File ~/$DATE/${DATE}_bapache_manual_review.txt has to be manually reviewed"



exit

# php
echo $LOG_DIR/{ADREPLY,adwatch,CONTROLPANEL,NEWAD,SENDERROR,SENDPASS,store,SUPPORT,VERIFY}/$DATE.log

# TODO: check new logs that we use 
# 1.- find /opt/logs/ -name $(date '+%Y%m%d').log
# 2.- validate if our logs are correctly compressed
