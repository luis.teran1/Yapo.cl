#!/usr/bin/perl -w
use strict;
$| = 1;

#start subdir bsearch cleandir 1159513270

my %start;
my $nestlevel = 0;
my $starttime = 0;
my $endtime = 0;
my $regressstart = 0;
my $stresstart = 0;

while (<>) {
	my ($tag, $maker, $dir, $target, $time) = split(/ /);
	
	if ($tag eq "start") {
		if ($starttime == 0) {
			$starttime = $time;
		}
		if ($maker eq "regress" && $regressstart == 0) {
			$regressstart = $time;
		}
		if ($dir =~ /stresstest/ && $stresstart == 0) {
			$stresstart = $time;
		}
		$nestlevel++;
		$start{$maker . $dir. $target} = $time;
		
	} elsif ($tag eq "end") {
		$nestlevel--;
		my $runtime = $time - $start{$maker . $dir. $target};
		if ($runtime > 10) {
			print "  " x $nestlevel;
			print "Make $target " . ($target eq "testsuite" ? "" : "in ") . "$dir ended after ";
		       	time_diff($runtime);
		}
		$endtime = $time;
	}
	
}


print "\n";
print "Time spent to build: ";
time_diff($starttime, $regressstart);
if ($stresstart) {
	print "Time spent to do tests: ";
	time_diff($regressstart, $stresstart);
	print "Time spent to do stresstests: ";
	time_diff($stresstart, $endtime);
} else {
	print "Time spent to do tests: ";
	time_diff($regressstart, $endtime);
}
print "Total time: "; 
time_diff($starttime, $endtime);

sub time_diff {
	my $start = 0;
       	$start = shift if @_ >= 2;
	my $end = shift;
	my $diff = $end - $start;

	my $mdiff = int($diff/60);
	my $sdiff = $diff%60;

	if ($mdiff) {
		print "$mdiff minutes, ";
	}
	print "$sdiff seconds.\n";
}
