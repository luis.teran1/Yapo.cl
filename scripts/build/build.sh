#! /bin/bash

export LANG=en_US.ISO-8859-15
export MAILTO=dev@blocket.se

regress_log=/home/build/regress_log_`date +%Y%m%d_%H%M%S`.log
buildhost=`/bin/uname -ni`

if ps xc | grep make > /dev/null; then
	echo "make already running" | mail -s "The build in $buildhost needs attention!!... Build in progress" $MAILTO
	exit 1
fi

cd
touch /home/build/.build_start
rm -rf src 2>/dev/null
if ! svn co svn+ssh://b21/opt/svnroot/src/head src >/dev/null 2>&1; then
	echo "SVN checkout failed!" | mail -s "Build system error on $buildhost" $MAILTO
	exit 1
fi
cd src

make rpm REGRESS_LOG=$regress_log REGRESS_LOG_KEEP=1 DO_STRESSTEST=1 >> $regress_log 2>&1 

build_failure=$?

selenium_failure=0
# Broken, only checks controlpanel tests.
if [ -f regress_final/selenium/core/log.txt ]
then
	if ! grep -q "result:passed" regress_final/selenium-core/core/log.txt
	then 
		selenium_failure=1
	fi
fi


if [ $build_failure == 0 -a $selenium_failure == 0 ]
then
	if [ -f /home/build/.build_failure ]
	then
		mail -s "Build of RPMs OK again" $MAILTO <<"FLOFF"
     ____  ____  ____  ____
    /\   \/\   \/\   \/\   \
   /  \___\ \___\ \___\ \___\
   \  / __/_/   / /   / /   /
    \/_/\   \__/\/___/\/___/
      /  \___\    /  \___\
      \  / __/_  _\  /   /
       \/_/\   \/\ \/___/
         /  \__/  \___\
         \  / _\  /   /
          \/_/\ \/___/
            /  \___\
            \  /   /
             \/___/
FLOFF
		rm /home/build/.build_failure
	fi

	rm $regress_log

	mv -f /home/build/.build_start /home/build/.last_success

	TODAY=`date '+%Y%m%d'`
	if [ ! -f /home/build/stats/$TODAY.timing ]
        then 
	    scripts/build/timing.pl < build.timing > /home/build/stats/$TODAY.timing
	    mail -s "Build timing data $TODAY" $MAILTO < /home/build/stats/$TODAY.timing
	fi
	
else
	printf "Build of src failed.\nCheck $regress_log for details\n\n" > /home/build/mail.txt

	if [ $build_failure != 0 ]
	then
		if grep rpmbuild $regress_log > /dev/null; then
			tail -10 $regress_log >> /home/build/mail.txt
		else
			(grep -C 2 'DIFF:' $regress_log ; fgrep -B 8 '***' $regress_log ; grep -E -B 8 'FAILED([^n]|n($|[^o]|o($|[^-])))*$' $regress_log ; fgrep -B 5 'difference exceeds limit' $regress_log) >> /home/build/mail.txt
		fi
	fi
	if [ $selenium_failure == 1 ]
	then
	    cat regress_final/selenium/core/log.txt >>  /home/build/mail.txt
	fi
	grep "^==" $regress_log >> /home/build/mail.txt
	if [ -f regress/final/.trans_maps.after ]; then
		diff -u regress/final/.trans_maps.{before,after} >> /home/build/mail.txt
		grep sbrk $regress_log >> /home/build/mail.txt
	fi
	printf "\n\nChangeLog\n\n" >> /home/build/mail.txt
	/home/build/bin/svn2cl.sh --stdout -r "{`stat -c %x ~/.last_success | cut -f1 -d'.'`}:HEAD" 2>/dev/null >> /home/build/mail.txt
	cat /home/build/mail.txt | mail -s "Build failed on $buildhost" $MAILTO
	mv -f /home/build/.build_start /home/build/.build_failure
fi

sleep 30

killall -9 httpd 2>/dev/null
killall -9 httpd.worker 2>/dev/null
killall -9 memcached 2>/dev/null
for semid in `ipcs -s | cut -d' ' -f 2`; do ipcrm -s $semid 2>/dev/null; done
rm -rf src 2>/dev/null
