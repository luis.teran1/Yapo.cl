#!/opt/scmcoord_contrib/bin/python3
import sys
import re
import base64
import argparse
#import ast

"""This is *not* a proper parser, it's a simple search n' replace script"""

g_args = None;
g_encoding = 'ISO-8859-1';

def _parse_opts():
	global g_args;

	parser = argparse.ArgumentParser(description='Acts as sed and does custom transformations');
	parser.add_argument('--in_file',  nargs='?', help='File to be parsed', required=True);
	parser.add_argument('--out_file', nargs='?', help='File to be appended', required=True);
	parser.add_argument('--replace',  dest='replace_raw_l', nargs='*', default=[], help='key val');

	g_args = parser.parse_args();
	g_args.replace_d = {};

	_p = g_args.replace_raw_l;
	if len(_p) % 2:	sys.exit('(DBG) ' + str(g_args));

	for i in range(0, len(g_args.replace_raw_l), 2):
		#print('(DBG) x: ' + str(x));
		g_args.replace_d[ _p[i] ] = (re.compile('%' + _p[i] + '%'), _p[i + 1]);
	#END_for()
#END__parse_opts()


def main():
	_parse_opts();

	url_bn_re = re.compile(' url_bn\((.*)\) ');

	with open(g_args.in_file, 'rb') as fd_in, open(g_args.out_file, 'ab') as fd_out:
		for _line in fd_in:
			_line = _line.decode(g_encoding);

			if url_bn_re.search(_line):
				_l = url_bn_re.split(_line); # ['...', group(0), '...'] - Zane
				if len(_l) != 3: sys.exit('DEBUG: ' + str(_l));

				with open('www/img/base64/' + _l[1], 'rb') as img_fd:
					_b = base64.b64encode( img_fd.read() );
					_l[1] = 'url(data:image/png;base64,' + _b.decode('ascii') + ');';
					_line = ' '.join(_l);
				#END_with
			#

			for k in g_args.replace_d.keys():
				_p = g_args.replace_d[k];
				if _p[0].search(_line):
					_l = _p[0].split(_line); #['...'+] - Zane
					_line = '';
					for i in range(0, len(_l) - 1):
						_line += _l[i] + _p[1];
					_line += _l[-1];
				#END_if()
			#END_for()

			fd_out.write( _line.encode(g_encoding) );
		#END_for
	#END_with

	return;
#END_main

if __name__ == '__main__':
	try:
		main();
	except Exception as e:
		sys.exit( str(e) );
	#
#

#EOF

