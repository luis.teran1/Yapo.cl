#!/usr/bin/perl

use strict;
use warnings;

use File::Temp;

my $suitefile = shift or die "Suite filename missing";
my $testdir = shift or die "Out dir missing";
my $testname = shift or die "Test name missing";
my $start_time = shift or die "Start time missing";
my $end_time = shift or die "End time missing";
my $status = shift or die "Status missing";
my $logfile = shift;

exit 0 if $testname =~ /^(end)?testsuite-/;

my $suite = "standalone";
my $testno = 0;
if (open FD, $suitefile) {
	$suite = <FD>;
	chomp $suite;
	$testno = int(<FD> || "1");
	close FD;
}

mkdir $testdir unless -d $testdir;

my $tn = ($testno > 0) ? sprintf("t%02d-", $testno) : "";
my $tmp = File::Temp->new( TEMPLATE => "junitres-${suite}--${tn}${testname}-XXXX", DIR => $testdir, SUFFIX => '.xml', UNLINK => 0);

my $time = ($end_time - $start_time) / 1000000000;
my $res = "";
my $failed = 0;

$res .= "  <testcase classname='$suite' name='${tn}$testname' time='$time'>\n";
$res .= "    <failure/>\n" if $status ne "OK";
++$failed if $status ne "OK";
$res .= "  </testcase>\n";

if ($logfile && open FD, $logfile) {
	my $data = join("", <FD>);
	$data =~ tr/\x80-\xFF//d;
	$data =~ tr/\x00-\x09\x0b\x0c\x0e-\x1f//d;
	$data =~ s/&/&amp;/g;
	$data =~ s/</&lt;/g;
	$data =~ s/>/&gt;/g;
	$data =~ s/"/&quot;/g;
	$data =~ s/'/&apos;/g;
	close FD;
	$res .= "<system-out>$data</system-out>\n";
}
print $tmp "<testsuite failures='$failed' errors='0'>\n";
print $tmp $res;
print $tmp "</testsuite>\n";
close $tmp;

if ($testno > 0) {
	open FD, ">$suitefile" or die;
	print FD "$suite\n" . ($testno + 1) . "\n";
	close FD;
}
