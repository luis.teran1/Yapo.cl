#!/bin/env php -d memory_limit=1800M
<?php
require_once('migration.conf.php');

prepare();

$options = getopt('i:c:d:f:t:T:'); # command-line options: dump-file, (OR) org-connection, org-dbname, org-encoding, dest-encoding, tables (comma-separated, default:all-configured)

$origin_dump_file = tempnam(realpath(sys_get_temp_dir()), 'origin_dump.');
$destination_dump_file = tempnam(realpath(sys_get_temp_dir()), 'destination_dump.');

# Step 1: dump data from origin

$connection_parameter = @$options['c']?$options['c']:$conf['origin_connection_parameter'];
$database_name = @$options['d']?$options['d']:$conf['origin_database_name'];
$tables_parameter = implode(' ', array_map(create_function('$table_name', 'return implode(" ", array("-t", $table_name));'), isset($options['T'])?explode(",", $options['T']):array_keys($conf['tables'])));

$origin_dump_command = "pg_dump --data-only --no-privileges --no-tablespaces --no-owner --disable-triggers $tables_parameter $connection_parameter $database_name";
if (@$options['i'])
	$origin_dump_file = $options['i'];
else
	if (file_put_contents($origin_dump_file, shell_exec($origin_dump_command)) === FALSE)
		exit("Error: couldn't write the original dump");

# Step 2: re-encode data in the destination character encoding

$original_encoding = @$options['f']?$options['f']:$conf['origin_encoding'];
$destination_encoding = @$options['t']?$options['t']:$conf['destination_encoding'];

$encoding_conversion_command = "iconv -f $original_encoding -t $destination_encoding -c $origin_dump_file";
$reencoded_origin_dump_file = "$origin_dump_file.reencoded";

if (file_put_contents($reencoded_origin_dump_file, shell_exec($encoding_conversion_command)) === FALSE)
	exit("Error: couldn't reencode the original dump");

# Step 3: read in and parse dump

#$reencoded_origin_dump_file = $options['P'];

if (($file_in = @fopen($reencoded_origin_dump_file, 'r')) === FALSE)
	exit("Error: couldn't open reencoded file to be parsed");

$tables = array();
$parameter_insertions = array();

while (($line = fgets($file_in)) !== FALSE) {

	# Lines that we dismiss
	if (preg_match("/^[[:space:]]*--/", $line) || trim($line) == '' || preg_match("/^[[:space:]]*ALTER/", $line))
		continue;

	# Lines with settings
	$matches = array();
	if (preg_match("/^[[:space:]]*SET[[:space:]]+([[:word:]]+)[[:space:]]*=[[:space:]]*(.*);$/", $line, $matches))
		print (preg_replace("/$original_encoding/i", $destination_encoding, "SET $matches[1] = $matches[2];\n"));

	# Lines with sequence updates
	$matches = array();
	if (preg_match("/^[[:space:]]*SELECT[[:space:]]+pg_catalog.setval[(]'([[:word:]]+)', ([[:digit:]]+), (true|false)[)];/", $line, $matches)) {
		if (isset($conf['sequences'][$matches[1]]))
			if ($conf['sequences'][$matches[1]] == "##DISPOSE")
				continue;
			else
				$new_name = $conf['sequences'][$matches[1]];
		else
			$new_name = $matches[1];
		print ("SELECT pg_catalog.setval('$new_name', $matches[2], $matches[3]);\n");
	}

	# Lines starting a dataset:
	$matches = array();
	if (preg_match("/[[:space:]]*COPY[[:space:]]+([[:word:]]+)[[:space:]]*[(](.*)[)][[:space:]]*FROM[[:space:]]+stdin;/", $line, $matches)) {
		$orig_table_name = $matches[1];
		$table_name = @$conf['tables'][$orig_table_name]?$conf['tables'][$orig_table_name]:$orig_table_name;
		print ("TRUNCATE $table_name CASCADE;\n");

		$table_fieldnames = preg_split("/[\s,]+/", $matches[2]);
		$remove_fields = array();
		if (@$conf['fields'][$orig_table_name]) {
			foreach ($conf['fields'][$orig_table_name] as $field_name => $field_mapping) {
				if (($field_position = array_search($field_name, $table_fieldnames)) !== FALSE) {
					if ($field_mapping['table'] == '##DISPOSE') {
						$remove_fields[] = $field_position;
						array_splice($table_fieldnames, $field_position, 1);
					}
					else if ($field_mapping['table'] == $orig_table_name)
						$table_fieldnames[$field_position] = $field_mapping['field'];
					else {
						null; # case not implemented so far
					}
				}
			}
		}

		print ("COPY $table_name (" . implode(", ", $table_fieldnames) . ") FROM stdin;\n");

		if ($table_name == 'ads') {
			if(($category_index = array_search('category', $table_fieldnames)) === FALSE)
				exit("Error: the ads table doesn't have a category field");
			if(($ad_id_index = array_search('ad_id', $table_fieldnames)) === FALSE)
				exit("Error: the ads table doesn't have an ad_id field");
			if(($title_index = array_search('subject', $table_fieldnames)) === FALSE)
				exit("Error: the ads table doesn't have a title field");
			if(($text_index = array_search('body', $table_fieldnames)) === FALSE)
				exit("Error: the ads table doesn't have a text field");
			if(($price_index = array_search('price', $table_fieldnames)) === FALSE)
				exit("Error: the ads table doesn't have a price field");
			if(($old_price_index = array_search('old_price', $table_fieldnames)) === FALSE)
				exit("Error: the ads table doesn't have a old_price field");
		}
		else if ($table_name == 'ad_params') {
			if (($name_index = array_search('name', $table_fieldnames)) === FALSE)
				exit("Error: the ads table doesn't have a category field");
		}
		else if ($table_name == 'users') {
			if (($email_index = array_search('email', $table_fieldnames)) === FALSE)
				exit("Error: the users table doesn't have an email field");
			if (($user_id_index = array_search('user_id', $table_fieldnames)) === FALSE)
				exit("Error: the users table doesn't have an user_id field");
		}
		else if ($table_name == 'ad_codes') {
			if (($code_type_index = array_search('code_type', $table_fieldnames)) === FALSE)
				exit("Error: the ad_codes table doesn't have a code_type field");
		}

		while ((($line = fgets($file_in)) !== FALSE) && (trim($line) != '\.')) {
			$row_data = preg_split("/[\t]/", rtrim($line, "\n"));
			foreach ($remove_fields as $field_position) {
				array_splice($row_data, $field_position, 1);
			}
			if ($table_name == 'ads') {
				$category = $row_data[$category_index];
				if(isset($conf['category_splits'][$category])) {
					foreach ($conf['category_splits'][$category] as $condition => $destination_category) {
						if ((strpos($condition, 'add_param') !== FALSE)) {
							$matches = array();
							if (preg_match('/add_param\(([\w]+),([\d|\w]+)\)/', $condition, $matches)) {
								$parameter_insertions[] = array($row_data[$ad_id_index], $matches[1], $matches[2]);
								## Ugly hack: sorry Julio to durty up your code
								if ($matches[1] == "currency" ) {
									if (!empty($row_data[$price_index]))
										$row_data[$price_index] = convert_ufs($row_data[$price_index]);
									if (!empty($row_data[$old_price_index]))
										$row_data[$old_price_index] = convert_ufs($row_data[$old_price_index]);
								}
							}
						}
						if (substr($condition, 0, 1) == '/') {
							$dislocalized_condition = strtr(strtolower($condition), array (
								'�' => '[�a]',
								'a' => '[�a]',
								'�' => '[�e]',
								'e' => '[�e]',
								'�' => '[�i]',
								'i' => '[�i]',
								'�' => '[�o]',
								'o' => '[�o]',
								'�' => '[�u�]',
								'u' => '[�u�]',
								'�' => '[�u�]',
								'�' => '[�n]',
							));
							if (preg_match($dislocalized_condition, $row_data[$title_index] . "\n" . $row_data[$text_index])) {
								$row_data[$category_index] = $destination_category;
								print(">>>k ".$row_data[$ad_id_index] . "\n");
								break;
							}
						}
						else if (substr($condition, 0, 3) == 'any') {
							$row_data[$category_index] = $destination_category;
							if (substr($condition, -7) == ':review')
								print(">>>r ".$row_data[$ad_id_index] . "\n");
							if (substr($condition, -7) == ':delete')
								print(">>>d ".$row_data[$ad_id_index] . "\n");
							if (substr($condition, -5) == ':mail')
								print(">>>m ".$row_data[$ad_id_index] . "\n");
							break;
						}
					}
				}
				else {
					print(">>> UNKNOWN ORIGIN CATEGORY " . $category . ", " . $row_data[$ad_id_index] . "\n");
					$row_data[$category_index] = $conf['unknown_category'];
				}
			}
			else if ($table_name == 'ad_params') {
				if (array_search($row_data[$name_index], array_keys($conf['ad_params_adaptions'])) !== FALSE) {
					$adaption = $conf['ad_params_adaptions'][$row_data[$name_index]];
					if ($adaption == "##DISPOSE")
						continue; # skip row
					else
						$row_data[$name_index] = $adaption;
				}
			}
			else if ($table_name == 'users') {
				if ($row_data[$email_index] == '\N') {
					$row_data[$email_index] = "unknown$row_data[$user_id_index]@unkno.wn";
				}
			}
			else if ($table_name == 'ad_codes') {
				if (in_array($row_data[$code_type_index], $conf['filter_ad_codes'])) {
					continue;
				}
			}
			print (implode("\t", $row_data)."\n");
		}
		print "\\.\n"; # end of table
	}
}

fclose($file_in);

# add the remaining inserts

foreach ($parameter_insertions as $ad_param)
	print "INSERT INTO ad_params(ad_id, name, value) VALUES ($ad_param[0], '$ad_param[1]', '$ad_param[2]');\n";

exit; # the script ends here :-)

function prepare() {
	if ( !function_exists('sys_get_temp_dir')) {
		function sys_get_temp_dir() {
			if( $temp=getenv('TMP') )        return $temp;
			if( $temp=getenv('TEMP') )        return $temp;
			if( $temp=getenv('TMPDIR') )    return $temp;
			$temp=tempnam(__FILE__,'');
			if (file_exists($temp)) {
				unlink($temp);
				return dirname($temp);
			}
			return null;
		}
	}
}

function convert_ufs($price = NULL) {
	if (!isset($price))
		return null;

	global $conf;
	$price /= $conf['uf_convertion'];
	$price = round($price, 2) * 100;
	return $price;
}
?>
