<?php

# Configuration file for the migration process
# of ads from one DB schema to another

# Origin and destination character encodings

$conf['origin_encoding'] = 'UTF8';
$conf['destination_encoding'] = 'LATIN1';

$conf['origin_connection_parameter'] = '-h /dev/shm/regress-julio/pgsql10000/data';
$conf['origin_database_name'] = 'blocketdb';

# Table mappings - sorted
#
# We use an array so the order is kept when traversing

$conf['tables'] = array(
	#'origin_table_name' => 'destination_table_name',
	'admins' => 'admins',
	'admin_privs' => 'admin_privs',
	                      
	'stores' => 'stores',              
	'store_users' => 'store_users',         
	                      
	'tokens' => 'tokens',              
	                      
	'users' => 'users',               
	'user_params' => 'user_params',         
	                      
	'payment_groups' => 'payment_groups',      
	'pay_log' => 'pay_log',             
	'pay_log_references' => 'pay_log_references',  
	'payments' => 'payments',            
	                      
	'ads' => 'ads',                 
	'ad_params' => 'ad_params',           
	'ad_codes' => 'ad_codes',            
	'ad_actions' => 'ad_actions',          
	'action_params' => 'action_params',       
	'action_states' => 'action_states',       
	'state_params' => 'state_params',        
	'ad_images' => 'ad_images',           
	'ad_image_changes' => 'ad_image_changes',    
	'ad_images_digests' => 'ad_images_digests',   
	'ad_media' => 'ad_media',            
	'ad_media_changes' => 'ad_media_changes',    
	'ad_changes' => 'ad_changes',          

	'block_lists' => 'block_lists',
	'block_rules' => 'block_rules',
	'block_rule_conditions' => 'block_rule_conditions',
	'blocked_items' => 'blocked_items',
);

# Field mapping overriding - the mapping will be driven by the previos
# tables configuration, but you can override specific fields here

$conf['fields'] = array(
	#'origin_table_name' => array(
	#	'origin_field_name' => array(
	#		'table' => 'destination_table_name',
	#		'field' => 'destination_field_name'
	#	)
	#),
	'users' => array(
		'phone' => array(
			'table' => "##DISPOSE", # in order to remove it
			'field' => "##DISPOSE"
		),
		#'email' => array(
		#	'table' => "users", # in order to rename
		#	'field' => "emilio" # the field
		#),
	),
	'stores' => array(
		'show_map' => array(
			'table' => "##DISPOSE", # in order to remove it
			'field' => "##DISPOSE"
		)
	),
);

# Sequence mappings - sorted - apart from those brought directly from the previous tables

$conf['sequences'] = array(
	#'sequence_name' => '##DISPOSE', # do not alter this sequence
	#'sequence_name2' => 'new_sequence_name2', # rename this sequence
);

# Ad parameters modifications
$conf['ad_params_adaptions'] = array(
	'zipcode' => '##DISPOSE',
	'no_email' => '##DISPOSE',
	'area' => 'communes',
);

# Category splitting - ordering is important!
$conf['category_splits'] = array(

'1000' => array('any' => '1000'),
'1010' => array('any:add_param(currency,uf)' => '1020'),
'1020' => array('any:add_param(currency,uf)' => '1040'),
'1050' => array('any:add_param(currency,uf)' => '1060'),
'1070' => array('any:add_param(currency,uf)' => '1080'),
'1080' => array('any:add_param(currency,uf)' => '1100'),
'1090' => array('any:add_param(currency,uf)' => '1120'),

'2000' => array('any' => '2000'),
'2010' => array('any' => '2020'), # "autos" to "autos, camionetas y 4x4"
'2030' => array('any' => '2060'),
'2040' => array('any' => '2100'),
'2050' => array('any' => '2080'),
'2070' => array (
	'/\bavion/' => '2080',
	'any' => '2120',
	),

'2073' => array('any' => '2120'), # "tractores" to "Otros vehiculos"
'2075' => array('any' => '2120'), # "cuatrimotos" to "Otros vehiculos"
'2076' => array('any' => '2120'), # "acoplados" to "Otros vehiculos"
'2077' => array('any' => '2040'),
'2078' => array('any' => '2040'),

'2080' => array('any' => '2020'), # "camionetas y todoterreno" to "autos, camionetas y 4x4"

'3000' => array('any' => '5000'),
'3010' => array('any' => '5040'),
'3020' => array(
	'/\b(mesa|mesas|silla|sillas|cama|camas|sofa|sofas|sillon|sillones|lampara|lamparas|mueble|muebles|mueble de cocina|muebles de cocina|comoda|comodas|escritorio|escritorios|cajonera|cajoneras|cuadro|cuadros|espejo|espejos|comedor|comedores|colchon|colchones|biblioteca|bibliotecas|futon|futones|alfombra|alfombras|baul|baules|librero|libreros|camarote|camarotes|litera|literas|velador|veladores|closet|closets|armario|armarios|taburete|taburetes)/' => '5020',
	'/\b(mantel|manteles|toalla|toallas|servilleta|servilletas|funda|fundas|sabana|sabanas|juego de dormitorio|juegos de dormitorio|juego de cama|juegos de cama|ropa de cama|ropas de cama|cortina|cortinas|cortina de ba�o|cortinas de ba�o)/' => '5080',
	'any:review' => '5020' # UNSPECIFIED DEFAULT
	),
'3030' => array('any' => '5120'),
'3040' => array('any' => '5060'),
'3050' => array(
	'/\b(mantel|manteles|toalla|toallas|servilleta|servilletas|funda|fundas|sabana|sabanas|juego de dormitorio|juegos de dormitorio|juego de cama|juegos de cama|ropa de cama|ropas de cama|cortina|cortinas|cortina de ba�o|cortinas de ba�o)/' => '5080',
	'/\b(bolso|bolsos|gafas|gafas de sol|lentes|cartera|carteras|gorra|gorras|gorro|gorros|sombrero|sombreros|billetera|billeteras|morral|morrales|mochila|mochilas|guante|guantes|cinturon|cinturones)/' => '5140',
	'/\b(anillo|anillos|brillante|brillantes|diamante|diamantes|joya|joyas|oro|bisuteria|bisuterias|reloj|relojes)/' => '5160',
	'/\b(chaqueta|chaquetas|chala|chalas|sandalia|sandalias|vestido|vestidos|uniforme|uniformes|zapato|zapatos|zapatilla|zapatillas|polera|poleras|camisa|camisas|pitillo|pitillos|bota|botas|boto|botos|bototo|bototos|ropa|ropas|moda|modas|kimono|kimonos|chalita|chalitas|short|shorts|calza|calzas|peto|petos|jeans|cortavientos|chaleco|chalecos|pantalon|pantalones|parka|parkas|levis|flada|faldas|traje de ba�o|trajes de ba�o|traje|trajes|lenceria|lencerias|braga|bragas)/' => '5100',
	'any:review' => '5100' # UNSPECIFIED DEFAULT
	),

'4000' => array(
	'/\b(ca�a|ca�as|caza|pesca|carpa|carpas|tienda de campa�a|camping|acampada|acampadas)/' => '6040',
	'any' => '6000'
	),
'4010' => array(
	'/\b(bicicleta|mountain bike|bicicleta est�tica|triciclo|ciclismo)/' => '6180',
	'/\b(ca�a|ca�as|caza|pesca|carpa|carpas|tienda de campa�a|camping|acampada|acampadas)/' => '6040',
	'any' => '6180'
),
'4020' => array(
	'/\b(bicicleta|mountain bike|bicicleta est�tica|triciclo|ciclismo)/' => '6060',
	'/\b(ca�a|ca�as|caza|pesca|carpa|carpas|tienda de campa�a|camping|acampada|acampadas)/' => '6040',
	'any' => '6020'
	),
'4030' => array(
	'/\b(ca�a|ca�as|caza|pesca|carpa|carpas|tienda de campa�a|camping|acampada|acampadas)/' => '6040',
	'any' => '6140'
	),
'4040' => array(
	'/\b(CD|casette|casettes|vinilo|vinilos)/' => '6100',
	'/\b(libro|libros|revista|revistas|coleccion|colecciones|enciclopedia|enciclopedias)/' => '6120',
	'/\b(ca�a|ca�as|caza|pesca|carpa|carpas|tienda de campa�a|camping|acampada|acampadas)/' => '6040',
	'any:review' => '6120',
	),
'4050' => array(
	'/\b(ca�a|ca�as|caza|pesca|carpa|carpas|tienda de campa�a|camping|acampada|acampadas)/' => '6040',
	'any:mail' => '8020', # watch out! this category will be gone
	),
'4060' => array(
	'/\b(ca�a|ca�as|caza|pesca|carpa|carpas|tienda de campa�a|camping|acampada|acampadas)/' => '6040',
	'/\b(revista|revistas|coleccion|colecciones|enciclopedia|enciclopedias)/' => '6160',
	'any:review' => '6160'
	),
'4070' => array(
	'/\b(guitarra|guitarras|bateria|piano|pianos|organo|flauta|flautas|trompeta|trompetas|acordeon|acordeones|amplificador|violin|violines|parlante|parlantes|teclado|teclados|saxofon|saxofones|platillo|platillos|marshall|pedal|pedales|amplificador|amplificadores|amplificacion|conga|congas|quinto|quintos|djembe|timbal|timbales|trombon|trombones|violoncello|arpa|saxo|saxos|armonica|armonicas)/' => '6080',
	'/\b(CD|casette|casettes|vinilo|vinilos)/' => '6100',
	'/\b(ca�a|ca�as|caza|pesca|carpa|carpas|tienda de campa�a|camping|acampada|acampadas)/' => '6040',
	'any' => '6080'
	),
'4100' => array(
	'/\b(ca�a|ca�as|caza|pesca|carpa|carpas|tienda de campa�a|camping|acampada|acampadas)/' => '6040',
	'any' => '6200'
	),

'5000' => array('any' => '3000'),
'5010' => array('any' => '3060'),
'5020' => array('any' => '3080'),
'5030' => array('any' => '3040'),
'5040' => array('any' => '3020'),

'6000' => array('any' => '7000'),
'6010' => array('any' => '7020'),
'6020' => array('any' => '7040'),
'6030' => array('any' => '7060'),
'6040' => array('any:add_param(service_type,2)' => '7060'),
'6060' => array('any' => '7080'),
'6070' => array(
	'/\b(patente|taxi|botilleria|licencia)/' => '8020',
	'any' => '7080',
	),
'7000' => array('any' => '8000'),
'7010' => array('any' => '8020'),

);

$conf['unknown_category'] = '8020';

# Ad category changes
$conf['ad_category'] = array(
	# '0000000' => '1010', # new category for the ad
);

$conf['filter_ad_codes'] = array(
	'buyfile',
);

# $ to UF relation valid on Friday 9th of December
# This is the actual value that is going to be used
$conf['uf_convertion'] = 22246.56;

?>
