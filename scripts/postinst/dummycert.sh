#!/bin/bash

BDIR=/opt/blocket

check_all () {
	test -f $BDIR/conf/ssl.crt/server.crt && test -f $BDIR/conf/ssl.key/server.key && test -f  $BDIR/conf/ssl.crl/ca.crl && exit 0
	echo "CA cert exist but your certificate structure is not complete."
	echo "Please check before trying to restart apache"
	exit 1
}

test -f /opt/blocket/conf/ssl.crt/ca.crt && check_all

mkdir /tmp/BDCA.$$
dir=/tmp/BDCA.$$

openssl genrsa -passout pass:pass -out /tmp/BDCA.$$/ca.key 1024
openssl req -passin pass:pass -new -key /tmp/BDCA.$$/ca.key -out /tmp/BDCA.$$/ca.csr << EOF
SE
BDCAland
BDCAcity
BDCAAB

BDCACA



EOF

openssl x509 -req -days 15 -in /tmp/BDCA.$$/ca.csr -signkey /tmp/BDCA.$$/ca.key -out /tmp/BDCA.$$/ca.pem

cat > /tmp/BDCA.$$/BDCA.conf << EOF
RANDFILE                = /dev/arandom

[ ca ]
default_ca              = CA_default

[ CA_default ]
dir                     = .
certs                   = $dir
crl_dir                 = $dir
database                = $dir/index.txt
new_certs_dir           = $dir
certificate             = $dir/ca.pem
serial                  = $dir/ca.srl
crl                     = $dir/ca.crl
private_key             = $dir/ca.key

default_days            = 365
default_crl_days        = 30
default_md              = md5
preserve                = no

policy                  = policy_match

[ policy_match ]
countryName             = match
stateOrProvinceName     = match
organizationName        = match
OrganizationalUnitName  = optional
commonName              = supplied
emailAddress            = optional

[ policy_anything ]
countryName             = optional
stateOrProvinceName     = optional
localityName            = optional
organizationName        = optional
organizationalUnitName  = optional
commonName              = supplied
emailAddress            = optional
[ req ]
default_bits            = 2048
default_keyfile         = privkey.pem
distinguished_name      = req_distinguished_name
attributes              = req_attributes

[ req_distinguished_name ]
countryName                     = Country Name (2 letter code)
countryName_default             = SE
countryName_min                 = 2
countryName_max                 = 2

stateOrProvinceName             = State or Province Name (full name)
stateOrProvinceName_default     = Stockholm

localityName                    = Locality Name (eg, city)
localityName_default            = Stockholm

0.organizationName              = Organization Name (eg, company)
0.organizationName_default      = BDCA

organizationalUnitName          = Organizational Unit Name (eg, section)

commonName                      = Common Name (eg, fully qualified host name)
commonName_max                  = 64

emailAddress                    = Email Address
emailAddress_max                = 64

[ req_attributes ]
challengePassword               = A challenge password
challengePassword_min           = 4
challengePassword_max           = 20

unstructuredName                = An optional company name

[ x509v3_extensions ]

nsCaRevocationUrl               = http://www.bomnegocio.com/crl.pem
nsComment                       = "Blocket CRL"

nsCertType                      = 0x40

EOF
export SSLCFG=$dir/BDCA.conf
export SSLROOT=$dir
openssl genrsa -passout pass:pass 1024 > $dir/server.key
openssl rsa -in $dir/server.key -passin pass:pass -out $dir/server.key
#IP_NUMBER_ETH0=$(grep `hostname` /etc/hosts | cut -f 1)
IP_NUMBER_ETH0=$(tr '\t' ' ' < /etc/hosts | grep `hostname` | cut -d ' ' -f 1)

openssl req -passin pass:pass -new -key $dir/server.key -config $dir/BDCA.conf -out $dir/server.csr <<EOG
SE
BDCAland
BDCAcity
Blocket

$IP_NUMBER_ETH0



EOG
touch $dir/index.txt
openssl x509 -req -days 15 -in $dir/server.csr -CA $dir/ca.pem -CAkey $dir/ca.key -CAcreateserial -out $dir/server.crt
openssl ca -config $SSLCFG -gencrl -out $dir/ca.crl

test -d /opt/blocket/conf/ssl.crt || mkdir /opt/blocket/conf/ssl.crt
test -d /opt/blocket/conf/ssl.key || mkdir /opt/blocket/conf/ssl.key
test -d /opt/blocket/conf/ssl.crt || mkdir /opt/blocket/conf/ssl.crt
test -d /opt/blocket/conf/ssl.crl || mkdir /opt/blocket/conf/ssl.crl

cp $dir/server.crt /opt/blocket/conf/ssl.crt/ || exit 1 
cp $dir/server.key /opt/blocket/conf/ssl.key/ || exit 1
cp $dir/ca.key /opt/blocket/conf/ssl.key/ || exit 1
cp $dir/ca.pem /opt/blocket/conf/ssl.crt/ca.crt || exit 1
cp $dir/ca.crl /opt/blocket/conf/ssl.crl/ || exit 1
hash=$(openssl crl -noout -hash < $dir/ca.crl)
ln -s ca.crl /opt/blocket/conf/ssl.crl/$hash.r0
rm -rf /tmp/BDCA.$$
echo "Dummy certificate structure complete. Make real certs using the official BlocketCA"
echo 'Remember to run ln -s ca.crl $(openssl crl -noout -hash < ca.crl).r0 in the ssl.crl directory'
