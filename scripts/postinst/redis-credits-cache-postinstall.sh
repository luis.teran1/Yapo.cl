#!/bin/bash
# Post installation script for credits cache
BDIR=/opt/blocket
test -x /usr/sbin/useradd && ADDUSER=/usr/sbin/useradd
test -x /usr/sbin/adduser && ADDUSER=/usr/sbin/adduser
create_duid () {
	$ADDUSER -g nobody -d /var/redis/$1 $1 || exit 1
	mkdir /var/redis/$1 >/dev/null 2>&1
	chmod 555 /var/redis/$1
}

mkdir /var/redis
grep -w ^redis /etc/passwd || create_duid redis
mkdir -p /var/redis/redis_credits_cache/db > /dev/null 2>&1
chown -R redis:nobody /var/redis/redis_credits_cache
ln -sf $BDIR/etc/init.d/redis-credits-cache /etc/init.d/redis-credits-cache
exit 0
