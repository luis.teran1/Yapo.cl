#!/bin/bash
# Post installation script for bsearch-csearch
BDIR=/opt/blocket

test -d ~bsearch/index || mkdir ~bsearch/index
chown bsearch ~bsearch/index

ln -sf $BDIR/etc/init.d/csearch /etc/init.d/csearch
echo "csearch is installed. Re-index and run /etc/init.d/csearch restart"
