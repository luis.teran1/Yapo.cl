#!/bin/bash
# Post installation script for redis_email
BDIR=/opt/blocket
test -x /usr/sbin/useradd && ADDUSER=/usr/sbin/useradd
test -x /usr/sbin/adduser && ADDUSER=/usr/sbin/adduser
create_duid () {
	$ADDUSER -g nobody -d /var/redis/$1 $1 || exit 1
	mkdir /var/redis/$1 >/dev/null 2>&1
	chmod 555 /var/redis/$1
}

mkdir /var/redis
grep -w ^redisemail /etc/passwd || create_duid redisemail
mkdir -p /var/redis/redisemail/db > /dev/null 2>&1
chown -R redisemail:nobody /var/redis/redisemail
ln -sf $BDIR/etc/init.d/redisemail /etc/init.d/redisemail
exit 0
