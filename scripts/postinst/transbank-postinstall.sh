#!/bin/bash
# Post installation script for transbank package
BDIR=/opt/blocket

if [ ! -d "$BDIR/cgi-bin/log" ]; then
	mkdir $BDIR/cgi-bin/log
fi

is_qa() {
	hostname | grep stg &> /dev/null
	return $?
}

set_qa_transbank_conf () {
	cp ${BDIR}/cgi-bin/qa_conf/privada.pem ${BDIR}/cgi-bin/maestros/privada.pem
	cp ${BDIR}/cgi-bin/qa_conf/tbk_public_key.pem ${BDIR}/cgi-bin/maestros/tbk_public_key.pem
	cp ${BDIR}/cgi-bin/qa_conf/tbk_config.dat ${BDIR}/cgi-bin/datos/tbk_config.dat
	printf "\tUpdated tbk_conf\n"
}

if is_qa; then
	echo "QA environment detected. Updating transbank configuration:"
	set_qa_transbank_conf
fi

chown -R apache:apache $BDIR/cgi-bin
chmod -R 755 $BDIR/cgi-bin
exit 0
