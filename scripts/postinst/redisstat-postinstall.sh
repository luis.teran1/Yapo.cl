#!/bin/bash
# Post installation script for redisstat
BDIR=/opt/blocket
test -x /usr/sbin/useradd && ADDUSER=/usr/sbin/useradd
test -x /usr/sbin/adduser && ADDUSER=/usr/sbin/adduser
create_duid () {
	$ADDUSER -g nobody -d /var/redis/$1 $1 || exit 1
	mkdir /var/redis/$1 >/dev/null 2>&1
	chmod 555 /var/redis/$1
}

mkdir /var/redis
grep -w ^redisstat /etc/passwd || create_duid redisstat
mkdir -p /var/redis/redisstat/db > /dev/null 2>&1
chown -R redisstat:nobody /var/redis/redisstat
ln -sf $BDIR/etc/init.d/redisstat /etc/init.d/redisstat
exit 0
