#!/bin/bash
# Post installation script for redis_accounts
BDIR=/opt/blocket
test -x /usr/sbin/useradd && ADDUSER=/usr/sbin/useradd
test -x /usr/sbin/adduser && ADDUSER=/usr/sbin/adduser
create_duid () {
	$ADDUSER -g nobody -d /var/redis/$1 $1 || exit 1
	mkdir /var/redis/$1 >/dev/null 2>&1
	chmod 555 /var/redis/$1
}

mkdir /var/redis
grep -w ^redis_wordsranking /etc/passwd || create_duid redis_wordsranking
mkdir -p /var/redis/redis_wordsranking/db > /dev/null 2>&1
chown -R redis_wordsranking:nobody /var/redis/redis_wordsranking
ln -sf $BDIR/etc/init.d/redis_wordsranking /etc/init.d/redis_wordsranking
exit 0
