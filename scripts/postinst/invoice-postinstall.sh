#!/bin/bash
# Post installation script for payment invoice/bills, we need php soap
BDIR=/opt/blocket

if [ ! -e "$BDIR/modules/soap.so" ]; then
	if [ ! -e "/usr/lib64/php/modules/soap.so" ]; then
		echo "php-soap is not installed! argh!"
	else
		ln -s /usr/lib64/php/modules/soap.so $BDIR/modules/soap.so
	fi
fi

exit 0
