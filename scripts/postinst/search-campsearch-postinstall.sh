#!/bin/bash
# Post installation script for search-campsearch
BDIR=/opt/blocket

ln -sf $BDIR/etc/init.d/campsearch /etc/init.d/campsearch
echo "campsearch is installed. Re-index and run /etc/init.d/campsearch restart"
