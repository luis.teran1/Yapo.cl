#!/bin/bash
# Post installation script for bsearch-bsearch
BDIR=/opt/blocket

ln -sf $BDIR/etc/init.d/newssearch /etc/init.d/newssearch
test -d ~bsearch/images || mkdir ~bsearch/images
chown bsearch ~bsearch/images
echo "newssearch is installed. Re-index and run /etc/init.d/newssearch restart"
