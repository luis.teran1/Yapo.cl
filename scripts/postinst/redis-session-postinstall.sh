#!/bin/bash
# Post installation script for redis_session
BDIR=/opt/blocket
test -x /usr/sbin/useradd && ADDUSER=/usr/sbin/useradd
test -x /usr/sbin/adduser && ADDUSER=/usr/sbin/adduser
create_duid () {
	$ADDUSER -g nobody -d /var/redis/$1 $1 || exit 1
	mkdir /var/redis/$1 >/dev/null 2>&1
	chmod 555 /var/redis/$1
}

mkdir /var/redis
grep -w ^redissession /etc/passwd || create_duid redissession
mkdir -p /var/redis/redissession/db > /dev/null 2>&1
chown -R redissession:nobody /var/redis/redissession
ln -sf $BDIR/etc/init.d/redissession /etc/init.d/redissession
exit 0
