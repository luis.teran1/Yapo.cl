#!/bin/bash 

if [ ! -d "/var/www/.gnupg" ] ; then
	echo "Directory for apache gnupg use does not exist. Trying to create.";
	mkdir /var/www/.gnupg;
	chown apache:apache /var/www/.gnupg;
       	chmod 700 /var/www/.gnupg;
fi
