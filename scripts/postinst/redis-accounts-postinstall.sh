#!/bin/bash
# Post installation script for redis_accounts
BDIR=/opt/blocket
test -x /usr/sbin/useradd && ADDUSER=/usr/sbin/useradd
test -x /usr/sbin/adduser && ADDUSER=/usr/sbin/adduser
create_duid () {
	$ADDUSER -g nobody -d /var/redis/$1 $1 || exit 1
	mkdir /var/redis/$1 >/dev/null 2>&1
	chmod 555 /var/redis/$1
}

mkdir /var/redis
grep -w ^redisaccounts /etc/passwd || create_duid redisaccounts
mkdir -p /var/redis/redisaccounts/db > /dev/null 2>&1
chown -R redisaccounts:nobody /var/redis/redisaccounts
ln -sf $BDIR/etc/init.d/redisaccounts /etc/init.d/redisaccounts
exit 0
