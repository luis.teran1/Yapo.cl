#!/bin/bash
# Post installation script for batch
BDIR=/opt/blocket
test -x /usr/sbin/useradd && ADDUSER=/usr/sbin/useradd
test -x /usr/sbin/adduser && ADDUSER=/usr/sbin/adduser
create_duid () {
	/usr/sbin/groupadd $1
	$ADDUSER -g $1 -d /home/$1 $1 || exit 1
	chmod 750 /home/$1
}

grep "^batch:" /etc/passwd || create_duid batch
mkdir /home/batch/cron.d >/dev/null 2>&1

# regenerate_pageviews_per_cat_prov.pl
echo '5 1 * * * /opt/blocket/bin/regenerate_pageviews_per_cat_prov.pl /opt/logs/www_statsUxU/`date +\%Y\%m\%d -d yesterday`.log 2> /dev/null' > /home/batch/cron.d/stats1x1

#force_visits_dump.pl
echo '0 0 * * * /opt/blocket/bin/force_visits_dump.pl 2> /dev/null' >> /home/batch/cron.d/stats1x1

echo "batch-stats1x1 is installed. Apply applicable cron entries from /home/batch/cron.d"
