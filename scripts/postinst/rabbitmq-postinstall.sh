#!/bin/bash
# Post installation script for rabbitmq

setup_rabbitmq () {
	rabbitmqctl add_vhost msg_center
	rabbitmqctl set_permissions -p msg_center yapo "msg_center" "msg_center" "msg_center"
	rabbitmqctl add_vhost newad_event
	rabbitmqctl set_permissions -p newad_event yapo "newad_event" "newad_event" "newad_event"
	rabbitmqctl add_vhost feedback_event
	rabbitmqctl set_permissions -p feedback_event yapo "feedback_event" "feedback_event" "feedback_event"
	rabbitmqctl add_vhost backend_event
	rabbitmqctl set_permissions -p backend_event yapo "backend_event" "backend_event" "backend_event"
}

setup_rabbitmq
