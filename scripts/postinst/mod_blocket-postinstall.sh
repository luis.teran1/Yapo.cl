#!/bin/bash
# Post install script for mod_blocket
BDIR=/opt/blocket
touch $BDIR/www-ssl/index.htm > /dev/null 2>&1
touch $BDIR/www-ssl/security/index.htm > /dev/null 2>&1
echo "*****************************************************************************"
echo "* mod_blocket has been installed. Please make a graceful restart of apache. *"
echo "*****************************************************************************"
