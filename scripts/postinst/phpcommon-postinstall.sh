#!/bin/bash
# Post install script for common 
BDIR=/opt/blocket
if test -d /usr/lib64/php/modules ; then
cd ${BDIR}/modules ; for a in `find /usr/lib64/php/modules/ -type f` ; do ln -s $a 2>/dev/null; done
fi
test -d /var/opt/blocket/tmp/php || mkdir -p /var/opt/blocket/tmp/php
chown apache:apache /var/opt/blocket/tmp/php > /dev/null 2>&1
echo '30 0 * * * root find '$BDIR'/logs -name "*.gz" -mtime +7 -exec rm {} \; >/dev/null 2>&1' > /etc/cron.d/blocket-phpcommon
#/etc/init.d/bapache graceful
echo "phpcommon is installed. Apache needs a graceful restart"

