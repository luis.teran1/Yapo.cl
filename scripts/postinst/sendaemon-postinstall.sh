#!/bin/bash
# Post installation script for sendaemon

pip install python-daemon==1.6.1 pika==0.9.14
echo "sendaemon is installed. Please restart all instances"
