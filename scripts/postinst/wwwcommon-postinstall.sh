#!/bin/bash
# Post install script for www-common 
BDIR=/opt/blocket
ln -sf $BDIR/etc/init.d/bapache /etc/init.d/bapache
ln -sf $BDIR/www-ssl/img/favicon.ico $BDIR/www-ssl/favicon.ico

# FT 431: When install www-common in dav server th post install script doesn't find the /op/bloccket/modules dir
if [ ! -d "$BDIR/modules" ]; then
	mkdir -p $BDIR/modules
fi

cd ${BDIR}/modules ; for a in `find /etc/httpd/modules/ -type f` ; do ln -s $a 2>/dev/null ; done 
mkdir ${BDIR}/logs > /dev/null 2>&1
mkdir ${BDIR}/www > /dev/null 2>&1
mkdir -p /var/empty/bapache
chmod 777 ${BDIR}/logs > /dev/null 2>&1
#/etc/init.d/bapache graceful
echo "www-common is installed. Apache needs a graceful restart"

