#!/bin/bash
# Post installation script for bsearch-msearch
BDIR=/opt/blocket

test -d ~bsearch/index || mkdir ~bsearch/index
chown bsearch ~bsearch/index

ln -sf $BDIR/etc/init.d/msearch /etc/init.d/msearch
echo "msearch is installed. Re-index and run /etc/init.d/msearch restart"
