#!/bin/bash
# Post install script for dav_put
BDIR=/opt/blocket
ln -sf $BDIR/etc/init.d/dav_replay /etc/init.d/dav_replay
mkdir /opt/images > /dev/null 2>&1
mkdir /opt/images/replay_folder > /dev/null 2>&1
mkdir /opt/images/replay_folder/tmp > /dev/null 2>&1
mkdir -p /opt/images/storeimages/{00..99} > /dev/null 2>&1
chown apache.root /opt/images/replay_folder > /dev/null 2>&1
chown apache.root /opt/images/replay_folder/tmp > /dev/null 2>&1

