#!/bin/bash
# Post installation script for josesearch
BDIR=/opt/blocket

test -d ~bsearch/index || mkdir ~bsearch/index
chown bsearch ~bsearch/index

ln -sf $BDIR/etc/init.d/josesearch /etc/init.d/josesearch
echo "josesearch is installed. Re-index and run /etc/init.d/josesearch restart"
