#!/bin/bash
# Post installation script for transhandler
BDIR=/opt/blocket
test -x /usr/sbin/useradd && ADDUSER=/usr/sbin/useradd
test -x /usr/sbin/adduser && ADDUSER=/usr/sbin/adduser
create_duid () {
	/usr/sbin/groupadd www
	$ADDUSER -g nobody -G www -d /var/empty/$1 $1 || exit 1
	mkdir /var/empty/$1 >/dev/null 2>&1
	chmod 755 /var/empty/$1
}
update_slave () {
	host=$(hostname)
	if [ $host == 'ch10.yapo.int.cl' ] || [ $host == 'ch39.yapo.int.cl' ]
	then
		sed -i 's/ch7.yapo.int.cl/ch20.yapo.int.cl/' /opt/blocket/conf/trans.conf
	fi
}

update_slave
grep "^trans:" /etc/passwd || create_duid trans
ln -sf $BDIR/etc/init.d/trans /etc/init.d/trans
#/etc/init.d/trans restart
echo "Trans is installed. Check bconf and restart"
