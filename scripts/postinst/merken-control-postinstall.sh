#!/bin/bash

echo "Updating bapache configuration ..."

/opt/blocket/bin/merken-ctl --enable
RETVAL=$?

if [ $RETVAL = 0 ]; then
	echo "Now, this host is a \"merken_host\". Don't forget restart bapache"
else
	echo "An error occurred"
fi
