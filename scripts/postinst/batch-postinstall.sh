#!/bin/bash
# Post installation script for batch
BDIR=/opt/blocket
test -x /usr/sbin/useradd && ADDUSER=/usr/sbin/useradd
test -x /usr/sbin/adduser && ADDUSER=/usr/sbin/adduser
create_duid () {
	/usr/sbin/groupadd $1
	$ADDUSER -g $1 -d /home/$1 $1 || exit 1
	chmod 750 /home/$1
}

if [ ! -x $BDIR/libexec/sendmail ] ; then
	ln -s /usr/lib/sendmail $BDIR/libexec/sendmail
fi

grep "^batch:" /etc/passwd || create_duid batch
mkdir /home/batch/getpaid >/dev/null 2>&1
chown batch /home/batch/getpaid
mkdir /home/batch/cron.d >/dev/null 2>&1
echo '*/9 * * * * /opt/blocket/bin/getpaid.pl -f=linewise -p=resourcearea -c=/opt/blocket/conf/bconf.conf -w=/home/batch/getpaid 2>&1 | logger -p local0.info -t pay_log' > /home/batch/cron.d/getpaid
echo '*/9 0 * * * /opt/blocket/bin/getpaid.pl -f=linewise -p=resourcearea -c=/opt/blocket/conf/bconf.conf -w=/home/batch/getpaid --yesterday 2>&1 | logger -p local0.info -t pay_log' >> /home/batch/cron.d/getpaid
echo '*/11 * * * * /opt/blocket/bin/getpaid.pl -f=linewise -p=viatel -c=/opt/blocket/conf/bconf.conf -w=/home/batch/getpaid 2>&1 | logger -p local0.info -t pay_log' >> /home/batch/cron.d/getpaid
echo '*/11 0 * * * /opt/blocket/bin/getpaid.pl -f=linewise -p=viatel -c=/opt/blocket/conf/bconf.conf -w=/home/batch/getpaid --yesterday 2>&1 | logger -p local0.info -t pay_log' >> /home/batch/cron.d/getpaid

echo '33 1 * * * /opt/blocket/bin/trans_command.pl "cmd:delete_inactive_watch_users\ncommit:1\nend\n" | logger -p local0.info -t adwatch' > /home/batch/cron.d/trans_gc
echo '35 1 * * * /opt/blocket/bin/trans_command.pl "cmd:delete_images\ncommit:1\nend\n" 2>&1 | /usr/bin/logger -t delete_image -p local0.info' >> /home/batch/cron.d/trans_gc
echo '50 * * * * /opt/blocket/bin/trans_command.pl "cmd:flush_stuck_mails\ncommit:1\nend\n" >/dev/null 2>&1' >> /home/batch/cron.d/trans_gc
echo '20 6 * * * /opt/blocket/bin/daily_clean_ads.pl 2>&1 |/usr/bin/logger -t oldads -p local0.info' >> /home/batch/cron.d/trans_gc
echo '40 6 * * * /opt/blocket/bin/trans_command.pl "cmd:archive_voucher_actions\ncommit:1\nend\n" >/dev/null 2>&1' >> /home/batch/cron.d/trans_gc
echo '01 0 * * * /opt/blocket/bin/trans_command.pl "cmd:archive_ad_bulk\ncommit:1\nend\n" >/dev/null 2>&1' >> /home/batch/cron.d/trans_gc

# Stats' gathering
echo '2 5 * * *  /opt/blocket/bin/daily_stats.pl /opt/logs/www_statsUxU/`date +\%Y\%m\%d -d yesterday`.log 2>&1 | logger -p local0.info -t daily_stats' > /home/batch/cron.d/daily_stats

# Reviewing Stats
echo '2 3 * * *  /opt/blocket/bin/mailgranskstatdaily.sh 2>&1 | logger -p local0.info -t reviewing_stats' >> /home/batch/cron.d/daily_stats

# Mail creation process
echo '2 7 * * *  /opt/blocket/bin/trans_command.pl "cmd:mail_stats\ncommit:1\nday:`date +\%Y-\%m-\%d -d yesterday`\nend\n" | logger -p local0.info -t daily_stats' >> /home/batch/cron.d/daily_stats

#echo '5 * * * *      /opt/blocket/bin/spool_redir_hourly.pl -lasthour -logdir=/logs/redir/ -dbhost="10.0.12.21" -dbuser="batch" | logger -p local0.info -t daily_stats' >> /home/batch/cron.d/daily_stats
echo '*/10 * * * *      /opt/blocket/bin/spool_redir_tenminutesly.pl -lasttenminutes -logdir=/opt/logs/redir/ -dbhost="10.49.0.30" -dbuser="batch" | logger -p local0.info -t daily_stats' >> /home/batch/cron.d/daily_stats

echo '0 0 * * *  /opt/blocket/bin/update_uf_value.sh -h 10.0.0.200 -p 5656' > /home/batch/cron.d/update_uf

# Half time mails and other cleanup
echo '18 2 * * * /opt/blocket/bin/send_half_time_mail.pl -pghost ch7.yapo.int.cl -bconffile /opt/blocket/conf/bconf.conf 2&>1 | /usr/bin/logger -t clean_ads -p local0.info' >> /home/batch/cron.d/clean_ads
echo '28 2 * * * /opt/blocket/bin/delete_monthly_ads -command delete 2&>1 | /usr/bin/logger -t clean_ads -p local0.info' >> /home/batch/cron.d/clean_ads
echo '28 3 * * * /opt/blocket/bin/delete_monthly_ads -command flush 2&>1 | /usr/bin/logger -t clean_ads -p local0.info' >> /home/batch/cron.d/clean_ads

# Send email for unfinished ads
touch /home/batch/cron.d/unfinished_ads
echo '*/30 * * * * /usr/bin/php -c /opt/blocket/conf/php.ini /opt/blocket/bin/unfinished_ads.php 2>&1 | /usr/bin/logger -t unfinished_ads -p local0.info' > /home/batch/cron.d/unfinished_ads

# The following is for deletion of certain ads, specified in "list_ads.txt"
#echo '0 3 13 12 ? 2011  /opt/blocket/bin/delete_ads_and_send_mails.pl -bconffile ../../../regress_final/conf/bconf.conf -file list_ads.txt -username adminname -passwd hashed_password -remote_addr 10.0.0.200' >> /home/batch/cron.d/delete_ads_and_send_mails
# Uncommment when the schedule for deletion of ads from dead categories is defined. Password for de the admin user must be hashed, just as it appears in a SELECT statement over the admins table. 

echo "batch is installed. Apply applicable cron entries from /home/batch/cron.d"
