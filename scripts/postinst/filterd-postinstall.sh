#!/bin/bash
# Post installation script for transhandler
BDIR=/opt/blocket
test -x /usr/sbin/useradd && ADDUSER=/usr/sbin/useradd
test -x /usr/sbin/adduser && ADDUSER=/usr/sbin/adduser
create_duid () {
	/usr/sbin/groupadd www
	$ADDUSER -g nobody -G www -d /var/empty/$1 $1 || exit 1
	mkdir /var/empty/$1 >/dev/null 2>&1
	chmod 755 /var/empty/$1
}

grep "^filterd:" /etc/passwd || create_duid filterd
ln -sf $BDIR/etc/init.d/filterd /etc/init.d/filterd
mkdir -p /var/run/filterd
chown -R filterd.nobody /var/run/filterd
#/etc/init.d/filterd restart
echo "Filterd is installed. Manual restart needed."
