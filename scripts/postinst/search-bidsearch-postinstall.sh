#!/bin/bash
# Post installation script for bsearch-bidsearch
BDIR=/opt/blocket

ln -sf $BDIR/etc/init.d/bidsearch /etc/init.d/bidsearch
echo "bidsearch is installed. Re-index and run /etc/init.d/bidsearch restart"
