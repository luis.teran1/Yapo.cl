#!/bin/bash
# Post installation script for spiderpoints
BDIR=/opt/blocket
test -x /usr/sbin/useradd && ADDUSER=/usr/sbin/useradd
test -x /usr/sbin/adduser && ADDUSER=/usr/sbin/adduser
create_duid () {
	$ADDUSER -g nobody -d /var/empty/$1 $1 || exit 1
	mkdir /var/empty/$1 >/dev/null 2>&1
	chmod 755 /var/empty/$1
}

grep "^spiderpoints:" /etc/passwd || create_duid spiderpoints
ln -sf $BDIR/etc/init.d/spiderpoints /etc/init.d/spiderpoints
echo "Spiderpoints is installed. Check config and restart"
