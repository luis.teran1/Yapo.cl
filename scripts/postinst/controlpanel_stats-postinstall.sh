#!/bin/bash
BDIR=/opt/blocket
ln -sf $BDIR/www/img/favicon.ico $BDIR/www/favicon.ico
ln -sf $BDIR/www-ssl/img/favicon.ico $BDIR/www-ssl/favicon.ico
echo "To actually enable stats:"
echo "set bconf controlpanel.modules.xxx.graph=/cpstats/xxx.png"
echo "enable the cron job by ln -s /opt/blocket/etc/cron.d/controlpanel_stats /etc/cron.d/ ; killall -HUP crond"
test -d /opt/blocket/var || mkdir /opt/blocket/var
