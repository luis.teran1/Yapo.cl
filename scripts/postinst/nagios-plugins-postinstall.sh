#!/bin/bash
# Post installation script for nagios-plugins package

BDIR=/opt/blocket
NAGIOS_PLUGINS=/usr/lib64/nagios/plugins

ln -sf $BDIR/sysadmin/nagios-plugins/check_asearch $NAGIOS_PLUGINS/check_asearch
ln -sf $BDIR/sysadmin/nagios-plugins/check_trans $NAGIOS_PLUGINS/check_trans
ln -sf $BDIR/sysadmin/nagios-plugins/check_hphw $NAGIOS_PLUGINS/check_hphw
