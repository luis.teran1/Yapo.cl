#!/bin/bash
# Post installation script for transbank package
BDIR=/opt/blocket

if [ ! -d "$BDIR/ts-cgi-bin/log" ]; then
	mkdir $BDIR/ts-cgi-bin/log
fi

is_qa() {
	hostname | grep stg &> /dev/null
	return $?
}

set_qa_ts_transbank_conf () {
	cp ${BDIR}/ts-cgi-bin/qa_conf/privada.pem ${BDIR}/ts-cgi-bin/maestros/privada.pem
	cp ${BDIR}/ts-cgi-bin/qa_conf/tbk_public_key.pem ${BDIR}/ts-cgi-bin/maestros/tbk_public_key.pem
	cp ${BDIR}/ts-cgi-bin/qa_conf/tbk_config.dat ${BDIR}/ts-cgi-bin/datos/tbk_config.dat
	printf "\tUpdated ts tbk_conf\n"
}

if is_qa; then
	echo "QA environment detected. Updating transbank-telesales configuration:"
	set_qa_ts_transbank_conf
fi

chown -R apache:apache $BDIR/ts-cgi-bin
chmod -R 755 $BDIR/ts-cgi-bin
exit 0
