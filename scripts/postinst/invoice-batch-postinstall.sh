#!/bin/bash

echo "# invoice / bill data processing" >  /home/batch/cron.d/payment_invoice_batch
echo "*/5 * * * * batch /usr/bin/php -c /opt/blocket/conf/php.ini /opt/blocket/bin/bill_processing.php" >> /home/batch/cron.d/payment_invoice_batch
echo "*/5 * * * * batch /usr/bin/php -c /opt/blocket/conf/php.ini /opt/blocket/bin/bill_send_email.php" >> /home/batch/cron.d/payment_invoice_batch

