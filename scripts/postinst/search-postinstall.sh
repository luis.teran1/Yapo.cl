#!/bin/bash
# Post installation script for bsearch-common
BDIR=/opt/blocket
test -x /usr/sbin/useradd && ADDUSER=/usr/sbin/useradd
test -x /usr/sbin/adduser && ADDUSER=/usr/sbin/adduser
create_duid () {
	$ADDUSER -g nobody -d /var/opt/$1 $1 || exit 1
	mkdir /var/opt/$1 >/dev/null 2>&1
	chmod 755 /var/opt/$1
}
test -d /var/opt/run || mkdir /var/opt/run
chmod 1777 /var/opt/run

grep "^bsearch:" /etc/passwd || create_duid bsearch


