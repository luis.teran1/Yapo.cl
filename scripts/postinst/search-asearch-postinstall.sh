#!/bin/bash
# Post installation script for search-asearch
BDIR=/opt/blocket
test -d ~bsearch/index || mkdir ~bsearch/index
chown bsearch ~bsearch/index
ln -sf $BDIR/etc/init.d/asearch /etc/init.d/asearch
echo "asearch is installed. Re-index and run /etc/init.d/asearch restart"
