#!/bin/bash
# Post installation script for memcache
BDIR=/opt/blocket
test -x /usr/sbin/useradd && ADDUSER=/usr/sbin/useradd
test -x /usr/sbin/adduser && ADDUSER=/usr/sbin/adduser
create_duid () {
	$ADDUSER -g nobody -d /var/empty/$1 $1 || exit 1
	mkdir /var/empty/$1
	chmod 555 /var/empty/$1
}

grep -w ^memcache /etc/passwd || create_duid memcache
ln -sf $BDIR/etc/init.d/bmemcached /etc/init.d/bmemcached
/etc/init.d/bmemcached stop > /dev/null 2>&1
/etc/init.d/bmemcached start 
exit 0
