#!/bin/bash
# Post installation script for bconf
BDIR=/opt/blocket
is_qa() {
	hostname | grep stg &> /dev/null
	return $?
}
set_xiti_id(){
	printf "\tUpdating Xiti ID\n"
	sed -i 's/xiti_settings.xiti.2.default=site:535162/xiti_settings.xiti.2.default=site:535161/' ${BDIR}/conf/bconf.txt.xiti
	sed -i 's/xiti_settings.xiti.1.true.value=site:535499/xiti_settings.xiti.1.true.value=site:535498/' ${BDIR}/conf/bconf.txt.xiti
}

set_nubox_cert(){
	printf "\tUpdating Nubox Cert\n"
	sed -i 's/payment.nubox.system=2/payment.nubox.system=1/' ${BDIR}/conf/bconf.txt.*
	sed -i 's/ServiFactura\//ServiFacturaCert\//' ${BDIR}/conf/bconf.txt.*
}

set_payment_rest(){
	printf "\tUpdating Payment rest Cert\n"
	sed -i 's/PaymentApp.notify_url=https:\/\/www2.yapo.cl\/khipu_notify/PaymentApp.notify_url=http:\/\/200.29.173.165\/khipu_notify/' ${BDIR}/conf/bconf.txt.payment_rest
	sed -i 's/PaymentApp.secret=fedb3738c6299b72be3fdfa7c1489f198d3abc1d/PaymentApp.secret=420089d5908bcdccac45366d4e3795924679290f/' ${BDIR}/conf/bconf.txt.payment_rest
	sed -i 's/PaymentApp.receiver_id=74384/PaymentApp.receiver_id=8484/' ${BDIR}/conf/bconf.txt.payment_rest
}

set_msg_center(){
	printf "\tUpdating messaging center.\n"
	sed -i 's/yapo.messaging.advgo.net/yapo.api.messaging-pre.advgo.net/' ${BDIR}/conf/bconf.txt.messaging-center
	sed -i 's/zBUB7um+TOtvyyPWLC1gT3qWDKWa360yCZLOmqnbQ2w=/zw0b3hQ5iIKm8sChttxZNFT3AJvt\/JHnlmz8LchlJsM=/' ${BDIR}/conf/bconf.txt.messaging-center
	sed -i 's/AK259164/AK148053/' ${BDIR}/conf/bconf.txt.messaging-center
}
set_apple_purchase(){
	printf "\tUpdating Apple Purchase URL\n"
	sed -i 's/buy\(.itunes.apple.com\)/sandbox\1/' ${BDIR}/conf/bconf.txt.site
}
set_bconf_to_stg(){
	printf "\tUpdating BConfs to match STG environment\n"
	sed -i 's/ch29.yapo.int.cl/ch29stg.yapo.int.cl/' ${BDIR}/conf/bconf*
	sed -i 's/mc_token.cache.ttl=1800/mc_token.cache.ttl=30/' ${BDIR}/conf/bconf.txt.redis_nextgen_api
	sed -i 's/carga-masiva.yapo.cl/10.45.1.93/' ${BDIR}/conf/bconf.txt.bifrost
	sed -i 's/\(.*common\..*search\.host\.3\..*=.*\)/#\1/' ${BDIR}/conf/bconf.txt.site
}

if is_qa; then
	echo "QA environment detected. Updating configuration:"
	set_xiti_id
	set_nubox_cert
	set_payment_rest
	set_msg_center
	set_apple_purchase
	set_bconf_to_stg
fi
