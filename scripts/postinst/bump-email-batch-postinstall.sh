#!/bin/bash

echo "# bump email that we send to users so they can buy our product" >  /home/batch/cron.d/bump_email_batch
echo "30 9 * * * batch /usr/bin/php -c /opt/blocket/conf/php.ini /opt/blocket/bin/bump_advertisement_email.php | logger -p local0.info -t BUMP_ADVERTISEMENT" >> /home/batch/cron.d/bump_email_batch

