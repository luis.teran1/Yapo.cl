#!/bin/bash
# Post installation script for redisstat
BDIR=/opt/blocket
test -x /usr/sbin/useradd && ADDUSER=/usr/sbin/useradd
test -x /usr/sbin/adduser && ADDUSER=/usr/sbin/adduser
create_duid () {
	$ADDUSER -g nobody -d /var/redis/$1 $1 || exit 1
	mkdir /var/redis/$1 >/dev/null 2>&1
	chmod 555 /var/redis/$1
}

mkdir /var/redis
grep -w ^redis /etc/passwd || create_duid redis
mkdir -p /var/redis/redis_cardata/db > /dev/null 2>&1
chown -R redis:nobody /var/redis/redis_cardata
ln -sf $BDIR/etc/init.d/redis-cardata /etc/init.d/redis-cardata
exit 0
