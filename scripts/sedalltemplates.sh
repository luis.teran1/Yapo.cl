find templates -type f | grep -E '\.tmpl$|\.tmpl\.in$' | while read file; do
	if head -n 1 "$file" | egrep "deffun|deffilter" >/dev/null; then
		echo ignoring file: $file
	else
		echo fixing file: $file
		sed -i -e "1 i <%&log_string(\"TEMPLATE_TRACE\", \"$file\")%>" "$file" 
	fi
done
