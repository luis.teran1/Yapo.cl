#!/usr/bin/perl -w

## EXTERN LIBS

use strict;
use blocket::bconf;
use perl_trans;
use Getopt::Long;

use Sys::Syslog qw(:standard :macros);

# Get bconf variables

my $config_dir = '/opt/blocket';

GetOptions("configdir=s" => \$config_dir,
);
	   
die "usage: $0 [-configdir <bconf.conf>] <stats1x1log-file>" if !@ARGV;

my $bconf = blocket::bconf::init_conf($config_dir);

## GLOBAL VARIABLES
my $BASE_DIR = "";
my $data_logfile = shift || die 'USE:   perl regenerate_pageviews_per_cat_reg_logs LOG-FILEYYYYMMDD';
my $DATE = $data_logfile;
$DATE =~ s/\D//g;
$DATE =~ s/^.*(\d{8})$/$1/g;
my $YEAR = $DATE;
my $MON = $DATE;
my $DAY = $DATE;
$YEAR =~ s/^(\d{4}).*$/$1/g;
$MON =~ s/^\d{4}(\d{2})\d{2}$/$1/g;
$DAY =~ s/^.*(\d{2})$/$1/g;
my $D1 = "$YEAR$MON$DAY";
my $D3 = "$YEAR-$MON-$DAY";

my $run_log = "rpvpcp_run";
my $skipped_log = "rpvpcp_skipped";
my $result_log = "rpvpcp_result";
my %pageviews = ();
my %most_viewed_ads = ();
my %regions;
my $max_pageviews_updates_per_dump = $$bconf{'*'}{'common'}{'stats1x1'}{'max_pageviews_updates_per_dump'};
# Take the number of provinces from trans bconf
for ( my $i = 1; $i <= (keys %{$$bconf{'*'}{'common'}{'region'}}) ; $i++ ) {
	$regions{$i} = 1;
}
$regions{'puk'} = 1; # unknown region


###############
## MAIN START #
###############

## PARSE LOG FILES
logger($run_log, "Regenerating pageviews/cat/prov for $data_logfile");
&parse_data_from_log($data_logfile);

## SAVE RESULTS TO LOG FILE
logger($run_log, "Saving results into LOG...");
foreach my $combination ( keys %pageviews ) { 
	chomp $combination;
	chomp $pageviews{$combination};
	logger($result_log, "$combination $pageviews{$combination}");
}
## SAVE RESULTS TO DB
logger($run_log, "Saving results into DB...\n\n");
&store_data_into_db($D3);
&store_most_popular_ads();
logger($run_log, "Done!");

############
# MAIN END #
############

sub parse_data_from_log
{
	logger($skipped_log, "Starting skipped log...\n");

	my $log_file = $_[0];

	my $tm = time;
	my @grep_lns = `grep "_frontpage" $log_file`;
	my $cnt_grep_lns = @grep_lns;

	foreach my $line ( @grep_lns ) {
		&extract_data_from_log_line($line);
	}

	logger($run_log, "[frontpage] ${cnt_grep_lns}L ".(time-$tm)."s");

	foreach my $region ( keys %regions ) {
		my $tm = time;
		my @grep_lns = `grep "_c${region}[^0-9]" $log_file`;
		my $cnt_grep_lns = @grep_lns;
		
		foreach my $line ( @grep_lns ) {
			&extract_data_from_log_line($line);
		}

		logger($run_log, "[$region] ${cnt_grep_lns}L ".(time-$tm)."s");
	}
}

sub count_most_popular_ads 
{
	my $line = $_[0];
	
	if ( $line =~ /^.*_(\d+)\.htm.*$/ ) {
		$most_viewed_ads{"$1"}++;
	}
}

sub extract_data_from_log_line
{
	my $line = $_[0];
	my $old_line = $line;
	my ($page_type, $user_region, $user_category, $page_region, $page_category, $extra_data);
	my $region = '\d{1,2}|puk'; #regexp to avoid repeating below
	my $category = '\d{1,4}'; #regexp to avoid repeating below

	# cleanup log line so we keep only interesting data
	chomp $line;
	$line =~ s/^.*GET \/1x1_pages_//gi;
	$line =~ s/\.gif.*$//gi;
	
	# match against different shapes and add up to previous numbers

	if ( $line =~ /^(vi|view)_c(${region})_a(${region})_(${category})$/ ) {
		($page_type, $user_region, $page_region, $page_category) = ($1, $2 eq 'puk' ? 0 : $2, $3 eq 'puk' ? 0 : $3, $4);
		$pageviews{"$page_type,$user_region,$page_region,0,$page_category,0"}++; 
		&count_most_popular_ads($_[0]);
	} 
	elsif ( $line =~ /^(ar|mailform)_c(${region})_a(${region})_(${category})_(${category})$/ ) {
		($page_type, $user_region, $page_region, $user_category, $page_category) = ($1, $2 eq 'puk' ? 0 : $2, $3 eq 'puk' ? 0 : $3, $4, $5);
		$pageviews{"$page_type,$user_region,$page_region,$user_category,$page_category,0"}++; 
	} 
	elsif ( $line =~ /^(ai.*|adform)_c(${region})_a(${region})_(${category})$/ ) {
		($page_type, $user_region, $page_region, $user_category) = ($1, $2 eq 'puk' ? 0 : $2, $3 eq 'puk' ? 0 : $3, $4);
		$pageviews{"$page_type,$user_region,$page_region,$user_category,0,0"}++;
	} 
	#elsif ( $line =~ /^(li|list)_c(${region})_a(${region})_(${category})_(${category})_(\d+)$/ ) {
	elsif ( $line =~ /^(li|list)_c(${region})_(${category})$/ ) {
		($page_type, $user_region, $page_category) = ($1, $2 eq 'puk' ? 0 : $2, $3);
		$pageviews{"$page_type,$user_region,0,0,$page_category,0"}++;
	}
	elsif ( $line =~ /^(frontpage)$/ ) {
		($page_type) = ($1);
		$pageviews{"$page_type,0,0,0,0,0"}++;
	}
	elsif ( $line =~ /^(.{1,20})_c(${region})_(${category})$/ ) {
		($page_type, $user_region, $page_category) = ($1, $2 eq 'puk' ? 0 : $2, $3);
      		$pageviews{"$page_type,$user_region,0,0,$page_category,0"}++;
	}
	elsif ( $line =~ /^(.{1,20})_c(${region})_a(${region})/ ) {
		($page_type, $user_region, $page_region) = ($1, $2 eq 'puk' ? 0 : $2, $3 eq 'puk' ? 0 : $3);
      		$pageviews{"$page_type,$user_region,$page_region,0,0,0"}++;
	}
	elsif ( $line =~ /^(.{1,20})_c(${region})/ ) {
		($page_type, $user_region) = ($1, $2 eq 'puk' ? 0 : $2);
      		$pageviews{"$page_type,$user_region,0,0,0,0"}++;
	}
	else {
		if ( $line ) {
			logger($skipped_log, "[$line] $old_line\n");
		}
	}
}

sub store_most_popular_ads
{
	my %trans;
	my %trans_adreplies;
	my $i=0;
	my @sorted_ads = sort {$most_viewed_ads{$a} <=> $most_viewed_ads{$b} } keys %most_viewed_ads;
	my @hundredsorted_ads = @sorted_ads[-101..-1];
	foreach my $combination ( @hundredsorted_ads ) {
		$trans{'type'}[$i] = 'adviews';
		$trans{'list_id'}[$i] = $combination;
		#$trans{'quantity'}[$i] = $most_viewed_ads{$combination};
		$i = $i+1;
	}
	if (defined $trans{'quantity'}){
		my $rundate = `date +'\%Y-\%m-\%d' -d 'yesterday'`;
		chomp $rundate;
		# All adview numbers of most adreplied ads are stored
		$trans_adreplies{'stat_time'} = $rundate;	
		$trans_adreplies{'cmd'} = 'get_top_adreplies';
		$trans_adreplies{'commit'} = 1;
		my %resp_adreplies = bconf_trans($$bconf{'*'}{'common'}{'transaction'}{'host'}, %trans_adreplies);
		my $j = 0;
		while (defined ($resp_adreplies{"get_top_adreplies.$j.list_id"})) {
			$trans{'type'}[$i] = 'adviews';
			$trans{'list_id'}[$i] = $resp_adreplies{"get_top_adreplies.$j.list_id"};
			$trans{'quantity'}[$i] = $most_viewed_ads{$resp_adreplies{"get_top_adreplies.$j.list_id"}};
			$i = $i+1;
			$j = $j+1;
		}

		$trans{'stat_time'} = $rundate;
		$trans{'cmd'} = 'insert_most_popular_ads';
		$trans{'commit'} = 1;
		my %resp = bconf_trans($$bconf{'*'}{'common'}{'transaction'}{'host'}, %trans);
	}
}
sub store_data_into_db
{
	my $date = $_[0];
	my %trans;
	my $i=0;
	foreach my $combination ( keys %pageviews ) {
		my ($page_type,$user_region,$page_region,$user_category,$page_category,$extra_data) = split(/,/, $combination);
		$trans{'date'}[$i] = $date;
		$trans{'page_type'}[$i] = $page_type;
		$trans{'user_region'}[$i] = $user_region;
		$trans{'page_region'}[$i] = $page_region;
		$trans{'user_category'}[$i] = $user_category;
		$trans{'page_category'}[$i] = $page_category;
		$trans{'extra_data'}[$i] = $extra_data;
		$trans{'pageviews'}[$i] = $pageviews{$combination};
		if (($i+1)>= $max_pageviews_updates_per_dump){
			$trans{'cmd'} = 'insert_pageviews_per_reg_cat';
			$trans{'commit'} = 1;
			my %resp = bconf_trans($$bconf{'*'}{'common'}{'transaction'}{'host'}, %trans);
			%trans = ();
			$i = 0;
		} else{
			$i = $i+1;
		}
	}
	if (defined $trans{'date'}){
		$trans{'cmd'} = 'insert_pageviews_per_reg_cat';
		$trans{'commit'} = 1;
		my %resp = bconf_trans($$bconf{'*'}{'common'}{'transaction'}{'host'}, %trans);
		%trans = ();
	}
}

sub logger {
	my $tag = $_[0];
	my $line = $_[1];
	setlogmask ( Sys::Syslog::LOG_UPTO(LOG_INFO) );
	openlog $tag, 'nofatal', LOG_LOCAL0;
	syslog "info|local0", $line;
	closelog;
}
