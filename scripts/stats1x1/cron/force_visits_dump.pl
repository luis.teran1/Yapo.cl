#!/usr/bin/perl -w
use strict;

use Getopt::Long;
use blocket::bconf;
use perl_trans;
use LWP::UserAgent;

# Get bconf variables
my $configfile = "/opt/blocket/conf/bconf.conf";

GetOptions("configfile=s" => \$configfile);

my $bconf = blocket::bconf::init_conf($configfile);
die "Couldn't load bconf!\n" if !$bconf;


my $counter_url = $$bconf{'*'}{'common'}{'base_url'}{'vid'};
die "Couldn't load common.base_url.vid from bconf\n" if !$counter_url;

$counter_url .= "/1x1_pages_forcedump.gif";
my $allowed_referer = $$bconf{'*'}{'common'}{'stats1x1'}{'valid_domains'}{'0'};
print "Using $allowed_referer as Referer\n";

my $ua = LWP::UserAgent->new;
$ua->agent("Dump-o-matic");
print "Using 'Dump-o-matic' as User Agent\n";
print "Invoking $counter_url\n";

#print "Running pixel: ".`ps uwx | grep pixel | grep -v grep`."\n";

my $response = $ua->get($counter_url, Referer => $allowed_referer);

die "Error when invoking  $counter_url [" . $response->status_line . "]\n" if !$response->is_success;

