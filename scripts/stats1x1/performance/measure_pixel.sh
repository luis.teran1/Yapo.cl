#!/bin/bash

while true; do

	read VISITORS OPEN CLOSED <<< `GET http://spock.schibstediberica.net:20025/server-status | awk '/closed\:/ {OPEN=$7} ; /dumped\:/ {VISITORS=$6} ; /dump\:/ {CLOSED=$7} ; END {print VISITORS " " OPEN " " CLOSED }'`
	read load1 load5 load15 <<< `w | head -n 1 | sed -e 's/.*age: //' -e 's/,//g'`
	read reads dummy1 writes dummy2 <<< `vmstat -p /dev/sda2 | tail -n 1`
	read pixel_pid pixel_user pixel_pr pixel_ni pixel_virt pixel_res pixel_shr pixel_s pixel_pcpu pixel_pmem pixel_dummy3 <<< `top -n1 -b -u julio | grep '\<pixel\>' | grep -v grep | sort -n | tail -n 1`
	read trans_pid trans_user trans_pr trans_ni trans_virt trans_res trans_shr trans_s trans_pcpu trans_pmem trans_dummy3 <<< `top -n1 -b -u julio | grep trans | grep -v grep | sort -n | tail -n 1`
	read postmaster_pid postmaster_user postmaster_pr postmaster_ni postmaster_virt postmaster_res postmaster_shr postmaster_s postmaster_pcpu postmaster_pmem postmaster_dummy3 <<< `top -n1 -b -u julio | grep postmaster | grep -v grep | sort -n | tail -n 1`

	read speed1 timelog1 <<< `tail -n 2 /home/julio/risk-test-hermes-performance/regress_final/logs/replay_log | head -n 1 | sed -e 's/.*speed \([0-9]\+\).*\(........\).$/\1 \2/'`
	#read speed2 timelog2 <<< `tail -n 2 /home/julio/risk-test-hermes-performance/regress_final/logs/replay_log2 | head -n 1 | sed -e 's/.*speed \([0-9]\+\).*\(........\).$/\1 \2/'`
	#read speed3 timelog3 <<< `tail -n 2 /home/julio/risk-test-hermes-performance/regress_final/logs/replay_log3 | head -n 1 | sed -e 's/.*speed \([0-9]\+\).*\(........\).$/\1 \2/'`
	speed2=0
	speed3=0
	timelog2=00:00:00
	timelog3=00:00:00

	read PENDING_TRANS WORKERS FREE_WORKERS <<< `printf "cmd:transinfo\ncommit:1\nend\n" | nc localhost 20005 | awk 'BEGIN {FS="[: \t]"} ; /pending_transactions total .internal.external.:/ {PENDING_TRANS=$4} ; /^nworkers:/ {WORKERS=$2} ; /^free:/ {FREE_WORKERS=$2} ; END {print PENDING_TRANS " " WORKERS " " FREE_WORKERS}'`

	LC_ALL=C echo `date +%Y%m%d%H%M%S` $timelog1 $timelog2 $timelog3 $speed1 $speed2 $speed3 $(expr $speed1 + $speed2 + $speed3 ) $VISITORS $OPEN $CLOSED $load1 $load5 $load15 $reads $writes $pixel_virt $pixel_pcpu $pixel_pmem $trans_virt $trans_pcpu $trans_pmem $postmaster_virt $postmaster_pcpu $postmaster_pmem $PENDING_TRANS $WORKERS $FREE_WORKERS

done

