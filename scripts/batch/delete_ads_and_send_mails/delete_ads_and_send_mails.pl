#!/usr/bin/perl -w

use strict;
use DBI;
use Getopt::Long;
use Data::Dumper;
use blocket::bconf;
use perl_trans;

my $limit = 1;
my $bconffile = "";
my $file = "";
my $username = "";
my $passwd = "";
my $remote_addr = "";

# select ads that belong to the category to disappear.

GetOptions("limit=s" => \$limit,
	   "file=s" => \$file,
	   "username=s" => \$username,
	   "passwd=s" => \$passwd,
	   "remote_addr=s" => \$remote_addr,
	   "bconffile=s"  => \$bconffile);


die "usage() not found" if (!$bconffile || !$file || !$username || !$passwd || !$remote_addr);

my %ad;
open FILE, "<", $file;
while(<FILE>) {
	/^(\d*)$/ or next;
	$ad{$1} = 1;
}
close FILE;

my $bconf = blocket::bconf::init_conf($bconffile);
my $transhost = $$bconf{'*'}{'common'}{'transaction'}{'host'};


# Run admail transaction for all ads

my $token = authenticate($username, $passwd, $remote_addr);

foreach my $ad_id (keys %ad) {

	my %trans_delete;
	$trans_delete{'cmd'} = "deletead";
	$trans_delete{'commit'} = "1";
	$trans_delete{'do_not_send_mail'} = "1";
	$trans_delete{'ad_id'} = $ad_id;
	$trans_delete{'token'} = $token;
	$trans_delete{'remote_addr'} = $remote_addr;
	$trans_delete{'remote_browser'} = "vi";

	my %resp_delete = bconf_trans($transhost, %trans_delete);
	
	if ($resp_delete{'status'} !~ m/TRANS_OK/) {
		print "Delete failed, ad_id=$ad_id\n";
		$token = authenticate($username, $passwd, $remote_addr);
		next;
	}
	$token = $resp_delete{'token'};

	print "Send deleted mail for ".$ad_id."\n";
	my %transcmd;

	$transcmd{'cmd'} = "admail";
	$transcmd{'commit'} = "1";
	$transcmd{'ad_id'} = $ad_id;
	$transcmd{'mail_type'} = "category_deleted";	# placeholder

	my %resp = bconf_trans($transhost, %transcmd);

	if ($resp{'status'} ne "TRANS_OK") {
		print Dumper(%resp);
		die "Failed to admail, bailing out";
	}
}

exit;

sub authenticate {
	my ($username, $passwd, $remote_addr) = @_;
	my %trans_auth;
	$trans_auth{'cmd'} = "authenticate";
	$trans_auth{'commit'} = "1";
	$trans_auth{'remote_addr'} = $remote_addr;
	$trans_auth{'remote_browser'} = "vi";
	$trans_auth{'username'} = $username;
	$trans_auth{'passwd'} = $passwd;
	my %resp_auth = bconf_trans($transhost, %trans_auth);

	if ($resp_auth{'status'} ne "TRANS_OK") {
		print Dumper(%resp_auth);
		die "Failed to authenticate, bailing out";
	}
	return $resp_auth{'token'};
}
