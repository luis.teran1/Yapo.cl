#!/bin/bash

# 1st argument is the csv file without headers
# 2nd argument, the current ads category
# 3rd argument, the final ads category
# 4rd argument, the new ad param 
# 5th argument, the server name
# 6th argument, the server port

split -l 5000 $1 new_cat_$2_$3_$4

tmp_file=/tmp/prefix.tmp

for i in `ls new*`; do
	echo 'cmd:change_category' > $tmp_file;
	cat $i | sed 's/,.*$//' | sed 's/^/ad_id:/' >> $tmp_file;
	echo "category:$3" >> $tmp_file;
	if  test $4 -gt 0
	then
		echo "new_gender:$4" >> $tmp_file;
	fi;
	echo "from_category:$2" >> $tmp_file;
	echo 'remote_addr:127.0.0.1' >> $tmp_file;
	echo 'commit:1' >> $tmp_file;
	echo 'end' >> $tmp_file;
	mv $tmp_file $i;
	echo "$i has been created";
	time cat $i | nc $5 $6;
done;

echo 'New files with trans commands were created. Delete them before starting with the new category';
