#!/usr/bin/perl -w

use strict;
use Getopt::Long;
use blocket::bconf;
use Sys::Syslog;

require "perl_trans.pm";

my $configfile;
my $debug = 0;
my $per_info = 0;

sub my_log {
	my ($loglevel, $message) = @_;

	if ($debug) {
		print "($loglevel) $message\n";
	} else {
		syslog($loglevel, $message);
	}

}

# init, argv, open syslog
GetOptions("configfile=s" => \$configfile,
	   "debug" => \$debug,
	   "per_info" => \$per_info);

if ($#ARGV < 0) {
    my_log('error', '(ERROR) No queue to flush');
    exit(1);
}

openlog("flush_queue", 'ndelay', 'local0');
my $bconf = blocket::bconf::init_conf($configfile);

my %param;
$param{cmd} = "flushqueue";
$param{commit} = "1";
$param{per_info} = "1" if $per_info;
$param{queue} = shift;

# send each paid-entry to trans (redir and tradedouble handled in trans)
my %resp = bconf_trans($$bconf{'*'}{'common'}{'transaction'}{'host'}, %param);

if (!%resp) {
	my_log('crit', '(CRIT) Failed to communicate with trans');
	exit(1);
}

if (!($resp{status} eq "TRANS_OK" || $resp{status} =~ /^TRANS_ERROR/)) {
	my_log('crit', "(CRIT) trans returned status: $resp{status}");
	exit(1);
}

my_log('info', "(INFO) $param{queue}: $resp{flushed} commands flushed");
