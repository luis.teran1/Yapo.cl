#!/bin/bash
#
# flushqueue.sh
# Flushes a trans queue and logs the result to target flushqueue-<queue>
# The flush will happen an hour at a time with configurable sleep intervals
#
# usage: flushqueue.sh <queue> [host [port]]
#

QUEUE=$1      # queue we are going to flush
HOST=$2       # trans host to target request          (default: 10.45.1.221)
PORT=$3       # trans port on target host             (default: 5656)
SLEEP=$4      # seconds to sleep between batches      (default: 0)
HOURS_AGO=$5  # time reference to start flushing from (default: 0)

# Terminal sequence to delete current line and restart the cursor
ERASE_LINE="\33[2K\r"

# visualsleep <msg> <time>
# Sleeps for a given time, printing a message with a countdown
function visualsleep {
	MSG=$1
	TIME=$2
	for ((i = 0; $i < ${TIME}; i++))
	do
		printf "${ERASE_LINE} ${MSG} $(($TIME - $i))"
		sleep 1
	done
	printf "${ERASE_LINE}"
}

# usage
# Prints usage information and terminates the script with an error
function usage {
	echo "usage: ./flushqueue.sh <queue> [host [port [sleep [hours ago]]]]"
	echo -e "\t queue        queue we are going to flush"
	echo -e "\t host         trans host to target request          (default: 10.45.1.221)"
	echo -e "\t port         trans port on target host             (default: 5656)"
	echo -e "\t sleep        seconds to sleep between batches      (default: 0)"
	echo -e "\t hours ago    time reference to start flushing from (default: 0)"
	exit 1
}

if [ -z "$QUEUE" ]
then
	usage
fi

if [ -z "$HOST" ]
then
	HOST=10.45.1.221
fi

if [ -z "$PORT" ]
then
	PORT=5656
fi

if [ -z "$SLEEP" ]
then
	SLEEP=0
fi

if [ -z "$HOURS_AGO" ]
then
	HOURS_AGO=0
fi

for x in $(seq ${HOURS_AGO} | tac);
do
	echo "Running with ${x} hours ago"
	time printf "cmd:flushqueue\nqueue:${QUEUE}\nmaxtime:`date -d \"${x} hours ago\" +\%s`\ncommit:1\nend\n" \
		| nc ${HOST} ${PORT} \
		| bash -c "tee >(logger -p local0.info -t flushqueue-${QUEUE})"
	visualsleep "will resume in" ${SLEEP}
done
