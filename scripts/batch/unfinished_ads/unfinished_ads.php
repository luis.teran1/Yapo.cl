<?php
require_once("bTransaction.php");
require_once("bAd.php");

#GLOBAL $BCONF;

/* WARNING: you are not supposed to do this... but because instantiating blocket_newad misses their methods/attributes and
	we don't need them here, we did the following: 	*/
class blocket_newad {}

$trans = new bTransaction();
$trans->add_data("log_string", log_string());
$result = $trans->send_command('get_all_unfinished_ads');

if ($trans->has_error()) {
	syslog(LOG_INFO, log_string()." Error when trying to get all unfinished ads with unsent mail notifications.");
	exit();
}

$trans_mail = new bTransaction();
$trans_status = new bTransaction();

$total_unf = 0;
$total_mail = 0;

if (!isset($result["get_all_unfinished_ads"]) || count($result["get_all_unfinished_ads"]) == 0) {
	echo "Total unfinished processed: $total_unf\tTotal mails: $total_mail\n";
	exit();
}

foreach($result["get_all_unfinished_ads"] as $k => $v) {
	$des = unserialize(base64_decode($v["unf_data"]));
	
	$trans_mail->add_data("adname", $des->ad->name);
	$trans_mail->add_data("to", $v["unf_email"]);
	$trans_mail->add_data("ad_subject", $des->ad->subject);
	$trans_mail->add_data("unf_id", $v["unf_id"]);
	$trans_mail->add_data("session", $v["unf_session_id"]);
	if(is_array($des->images) && count($des->images) > 0) {
		$trans_mail->add_data("img", $des->images[0]);
	}
	$result_mail = $trans_mail->send_command('unfinished_ad_mail');

	/* If mail sent successfully, mark unfinished ad as active ("retrievable") */	
	if(!$trans_mail->has_error()) {
		$trans_status->add_data("unf_ad_id", $v["unf_id"]);
		$trans_status->add_data("unf_status", "active");
		$trans_status->send_command('status_unfinished_ad');
		$trans_status->reset();
		$total_mail++;
	}
	$trans_mail->reset();
	$total_unf++;
	unset($des);

}

echo "Total unfinished processed: $total_unf\tTotal mails: $total_mail\n"; 

