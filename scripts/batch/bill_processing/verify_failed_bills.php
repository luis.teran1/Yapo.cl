<?php

require_once('autoload_lib.php');
require_once('init.php');
openlog("BILL_FAILED", LOG_ODELAY, LOG_LOCAL0);

class bill_verification {

	var $buffer = array();
	var $auth_token = "";
	var $soap_client;
	var $trans;
	var $logging = true;

	function update_invoice_status($purchase_id, $doc_type) {

		try {
			$params = array(
				"token" => $this->auth_token,
				"folioCargado" => $purchase_id,
				"codigoSII" => $doc_type == "bill" ? "39" : "33",
			);
			$result = $this->soap_client->ConsultaResultadoDocumento($params);
			$xml_str= $result->ConsultaResultadoDocumentoResult->any;
			$xml_element = new SimpleXMLElement($xml_str);

			$this->trans->reset();
			$this->trans->add_data('log_string', log_string());
			$this->trans->add_data('purchase_id', $purchase_id);
			if ($xml_element->Resultado == "OK") {
				$doc_num = (string)$xml_element->Archivos->Archivo->Documento['Folio'];
				$ext_doc_id = (string)$xml_element->Archivos->Archivo->Identificador;

				$this->trans->add_data('doc_num', $doc_num);
				$this->trans->add_data('external_doc_id', $ext_doc_id);
				$this->trans->add_data('status', 'sent');
			} else {
				$this->trans->add_data('status', 'paid');
			}
			$result_update = $this->trans->send_command('purchase_update');

			if ($this->trans->has_error(true)) {
				$this->writelog("error when trying to update the purchase state. purchase_id: $purchase_id", LOG_ERR);
			}
		}
		catch (Exception $e)
		{
			$this->writelog("Failed to process purchase $purchase_id ".$e->getMessage(), LOG_ERR);
			$this->writelog("Last response: ".$this->soap_client->__getLastResponse());
		}
	}

	function writelog($message, $log_level = LOG_INFO) {
		if ($this->logging) {
			syslog($log_level, log_string().$message);
		} else {
			echo($message.PHP_EOL);
		}
	}

	function run($log = true) {

		$this->logging = $log;
		$this->trans = new bTransaction();
		$this->trans->add_data('log_string', log_string());
		$this->trans->add_data('payment_status', 'paid');
		$this->trans->add_data('purchase_status', 'failed');
		$result = $this->trans->send_command('purchase_get_bills');

		if ($this->trans->has_error(true)) {
			$this->writelog("Error when trying to get the bills/invoices in failed status.");
			exit();
		}

		if (!isset($result['purchase_get_bills'])) {
			$this->writelog("bill_verification: no bills or invoices found");
			exit();
		}

		/* Authentication process */
		$nubox_domain = bconf_get($BCONF, '*.payment.nubox.domain');
		$nubox_auth_url = $nubox_domain . bconf_get($BCONF, '*.payment.nubox.auth_url');
		$nubox_load_url = $nubox_domain . bconf_get($BCONF, '*.payment.nubox.load_url');
		$nubox_client_id = bconf_get($BCONF, '*.payment.nubox.client_id');
		$nubox_user_id = bconf_get($BCONF, '*.payment.nubox.user_id');
		$nubox_pass = base64_decode(bconf_get($BCONF, '*.payment.nubox.password'));
		$nubox_serie = bconf_get($BCONF, "*.payment.nubox.serie");
		$nubox_system = bconf_get($BCONF, "*.payment.nubox.system");

		$params = array( "rutCliente" => $nubox_client_id,
			"rutUsuario" => $nubox_user_id,
			"contrasena" => $nubox_pass,
			"sistema" => $nubox_system,
			"numeroSerie" => $nubox_serie
		);

		try {
			$this->soap_client = new SoapClient($nubox_auth_url);
			$auth_response = $this->soap_client->Autenticar($params);
			$this->auth_token = $auth_response->AutenticarResult;
			$this->writelog("Auth Token = " . $this->auth_token);
		}
		catch (Exception $e){
			$this->writelog("Error auth nubox ".$e->getMessage());
			exit();
		}

		$this->soap_client = new SoapClient($nubox_load_url, array('connection_timeout' => 120));

		foreach ($result['purchase_get_bills'] as $k => $v) {
			$this->update_invoice_status($v['purchase_id'], $v['doc_type']);
		}
	}
}

$log_param = true;
if ($argc > 1 && ($argv[1] == "false" || $argv[1] == "echo")) $log_param = false;
$bill_verification = new bill_verification();
$bill_verification->run($log_param);
