#!/bin/bash

. /etc/init.d/functions
. /opt/blocket/conf/bill_processing_service.sh

checkPid()
{
	if [ -f $PIDFILE ]
	then
		PID=$(cat $PIDFILE)
		ps "$PID" > /dev/null 2>&1
		OUTPUT=$?
		if [ "$OUTPUT" -eq 0 ]
		then
			return 0
		else
			rm -f $PIDFILE
		fi
	fi
	return 1
}

checkProc()
{
	PID=$(ps -fA | grep "$CMD" | grep -v 'grep' | awk '{ print $2; }' )
	if [ -z "$PID" ]
	then
		return 0
	fi
	return $PID
}

status()
{
	checkPid
	OUTPUT=$?
	checkProc
	PROC=$?

	if [ $OUTPUT -eq 0 ]
	then
		echo "the service seems to be running"
		if [ $PROC -gt 0 ]
		then
			echo "and the process running with PID $PID"
			# ps $PID
		else
			echo "but the process sleeping"
		fi
	else
		echo "the service seems not to be running"
		if [ $PROC -gt 0 ]
		then
			echo "but the process still running with PID $PID"
			# ps $PID
		else
			echo "neither the process"
		fi
	fi
}

start()
{
	# I should check the service
	checkPid
	OUTPUT=$?
	if [ $OUTPUT -eq 0 ]
	then
		echo -n "the service seems to be running"
		failure
		echo
		return 0
	fi

	echo -n "starting service"
	($EXECFILE > /dev/null 2>&1 &) & > /dev/null 2>&1
	sleep 2
	PID=$(cat $PIDFILE)
	ps $PID > /dev/null
	OUTPUT=$?
	if [ "$OUTPUT" -eq 0 ]
	then
		success
	else
		failure
	fi
	echo
}

stop()
{
	if [ -f $PIDFILE ]
	then
		PID=$(cat $PIDFILE)
	else
		echo -n "servive seems to be stoped"
		failure
		echo
		return 0
	fi
	ps "$PID" > /dev/null 2>&1
	OUTPUT=$?
	if [ "$OUTPUT" -eq 0 ]
	then
		echo -n "stoping service [$PID]"
		# kill -15 $PID
		rm -f $PIDFILE
		success
	else
		echo -n "could not get the proccess"
		failure
		rm -f $PIDFILE
	fi
	echo
}

do_kill()
{
	checkProc
	PROC=$?
	if [ $PROC -gt 0 ]
	then
		echo "killing process [$PID]"
		(kill -9 $PID 2>&1) > /dev/null
	fi
}

reset()
{
	stop
	start
}

case "$1" in
	status)
		status
		checkProc
		;;
	start)
		start
		;;
	stop)
		stop
		;;
	kill)
		stop
		do_kill
		;;
	restart)
		restart
		;;
	*)
		echo $"Usage: $0 {start|stop|restart|status|kill}"
		exit 1
esac
