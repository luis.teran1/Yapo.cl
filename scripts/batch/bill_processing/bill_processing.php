<?php

use Yapo\SAPService;
/*

bill_processing.php

This script process the pending bill and send the information to nubox

v0.4 Ana Mora
- Add SAP interaction

v0.3: Pablo Mansilla / Jaime Moreira
- CHUS1063: multiproduct capable :)

v0.2: Boris Cruchet (boris@schibsted.cl)
- CHUS590: modify the user to send the bill to S.I.I.

v0.1: Boris Cruchet (boris@schibsted.cl)
- Create the script

*/

require_once('autoload_lib.php');
require_once('JSON.php');
require_once('httpful.php');
require_once('init.php');
openlog("BILL_INVOICE", LOG_ODELAY, LOG_LOCAL0);

class bill_processing {

	var $buffer = array();
	var $sap_buffer = array();
	var $auth_token = "";
	var $nubox_region = "";
	var $soap_client;
	var $trans;
	var $SAPService;
	var $logging = true;

	/* This line is used only for information purpose */
	var $csv_header = '"TIPO";FOLIO;SECUENCIA;FECHA;RUT;RAZONSOCIAL;GIRO;COMUNA;DIRECCION;AFECTO;PRODUCTO;DESCRIPCION;CANTIDAD;PRECIO;PORCENTDSCTO;EMAIL;"TIPOSERVICIO";PERIODODESDE;PERIODOHASTA;FECHAVENCIMIENTO;CODSUCURSAL;VENDEDOR;CODRECEPTOR;CODITEM;UNIDADMEDIDA;PORCENTDSCTO2;PORCENTDSCTO3;CODIGOIMP;MONTOIMP;INDICADORTRASLADO;FORMAPAGO;MEDIOPAGO;TERMINOSPAGOSDIAS;TERMINOSPAGOCODIGO;COMUNADESTINO;RUTSOLICITANTEFACTURA';

	function writelog($message, $log_level = LOG_INFO) {
		if ($this->logging) {
			syslog($log_level, log_string().$message);
		} else {
			echo($message.PHP_EOL);
		}
	}

	function fill_sap_buffer($v){
		if (count($this->sap_buffer) > 0) return

		$this->writelog("Filling SAP information for purchase_id=". $v['FOLIO'] . "\n", LOG_DEBUG);
		$iva = bconf_get($BCONF, '*.payment.iva');

		$this->sap_buffer = array(
			"purchaseID" => (int)$v['FOLIO'],
			"docTypeCode" => (int)$v['TIPO'],
			"commune" => $v['COMUNA'],
			"region" => $this->nubox_region,
			"rut" => $v['RUT'],
			"lob" => $v['GIRO'],
			"address" => $v['DIRECCION'],
			"iva" => (int)$iva,
			"email" => $v['EMAIL'],
			"businessName" => $v['RAZONSOCIAL']
		);
	}

	function write_to_sap() {

		$this->writelog("Sending purchase_id=". $this->sap_buffer['purchaseID'] . " to SAP\n");
		//$this->writelog(var_export($this->sap_buffer, true), LOG_DEBUG);

		$response = $this->SAPService->execute(
			"sellOrderCreation",
			$this->sap_buffer
		);

		if (!$response) {
			$this->writelog("Missing response from SAP Integration MS");
		} else {
			if (isset($response->body->status) && $response->body->status == "OK"){
				$this->writelog("Purchase ". $this->sap_buffer['purchaseID']. " succesfully processed");
			} else {
				$this->writelog(isset($response->body->ErrorMessage) ? $response->body->ErrorMessage : "An error ocurred");
			}
		}
		/* Clear sap buffer */
		$this->sap_buffer = array();
	}

	function write($purchase_id) {
		if (count($this->buffer) == 0) return;

		$this->writelog("Starting to process purchase_id= $purchase_id");

		$sap_enabled = bconf_get($BCONF, '*.sap_service.enabled');
		/* Generate string */
		$content = $this->csv_header."\n";
		foreach ($this->buffer as $bf) {
			/* Fill SAP information */
			if ($sap_enabled == 1) {
				$this->fill_sap_buffer($bf);
			}

			$s = implode(";", $bf)."\n";
			$this->writelog($s, LOG_DEBUG);
			$content .= $s;
		}
		//$this->writelog(substr($content, strlen($this->csv_header)+1, -1), LOG_DEBUG);

		/* Send to web service */
		$params = array(
			"token" => $this->auth_token,
			"archivo" => $content,
			"opcionFolios" => 1, // automatic
			"opcionRutClienteExiste" => 2, // update all info, but no the email
			"opcionRutClienteNoExiste" => 1, // do not stop
		);

		try {

			// Query if the purchase already has a bill
			$this->writelog("checking for invoices");
			$query_params = array(
				"token" => $this->auth_token,
				"folioCargado" => $purchase_id,
				"codigoSII" => "33"
			);
			$result = $this->soap_client->ConsultaResultadoDocumento($query_params);
			$xml_str= $result->ConsultaResultadoDocumentoResult->any;
			$xml_element = new SimpleXMLElement($xml_str);
			if ($xml_element->CantidadEncontrada > 0){
				$this->writelog ('--> factura ' . $xml_element->Archivos[0]->Archivo->Documento['Folio'] . ' asociada');
				throw new Exception('la compra ya tiene asociada la factura ' . $xml_element->Archivos[0]->Archivo->Documento['Folio']);
			}
			$this->writelog("checking for bills");
			$query_params["codigoSII"] = "39";
			$result = $this->soap_client->ConsultaResultadoDocumento($query_params);
			$xml_str= $result->ConsultaResultadoDocumentoResult->any;
			$xml_element = new SimpleXMLElement($xml_str);
			if ($xml_element->CantidadEncontrada > 0){
				$this->writelog ('--> boleta ' . $xml_element->Archivos[0]->Archivo->Documento['Folio'] . ' asociada');
				throw new Exception('la compra ya tiene asociada la boleta ' . $xml_element->Archivos[0]->Archivo->Documento['Folio']);
			}

			// if there is not bill or invoice, get a new one
			$time_start = microtime(true);
			$result = $this->soap_client->CargarYEmitir($params);
			$time_end = microtime(true);
			$time = $time_end - $time_start;

			$this->writelog("The request lasted $time seconds");
			$xml_str= $result->CargarYEmitirResult->any;
			$xml_element = new SimpleXMLElement($xml_str);

			if ($xml_element->Resultado == "OK") {
				$this->writelog("File loaded successfully.");
			} else if ($xml_element->Resultado == "C1") {
				$this->writelog($xml_element->Descripcion);
				throw new Exception('Load failed: Validation problems, purchase_id: '.$purchase_id.' - '.$xml_element->Descripcion);
			} else if ($xml_element->Resultado == "E1") {
				throw new Exception('Load failed: Emission problems, purchase_id: '.$purchase_id.' - '.$xml_element->Descripcion);
			}

			/* Don't remove the casting here !!! */
			$file_id = (int)$xml_element->Identificador;
			$document_id = (int)$xml_element->Documentos->Documento["Folio"];

			$this->writelog("document_id = $document_id. file_id = $file_id");

			$this->trans->reset();

			$this->trans->add_data('log_string', log_string());
			$this->trans->add_data('purchase_id', $purchase_id);
			$this->trans->add_data('status', 'sent');
			$this->trans->add_data('doc_num', $document_id);
			$this->trans->add_data('external_doc_id', $file_id);

			$result_update = $this->trans->send_command('purchase_update');

			if ($this->trans->has_error(true)) {
				throw new Exception('error when trying to update the purchase state');
			}

			$this->writelog("Result of processing purchase_id: $purchase_id: OK");
		}
		catch (Exception $e) {
			$this->trans->reset();

			$this->trans->add_data('log_string', log_string());
			$this->trans->add_data('purchase_id', $purchase_id);
			$this->trans->add_data('status', 'failed');
			$result_update = $this->trans->send_command('purchase_update');

			$this->writelog("Result of processing purchase_id: $purchase_id: ERROR");
			$this->writelog("Bill/invoice processing ERROR: ".$e->getMessage());
		}

		/* Send information to SAP MS*/
		if ($sap_enabled == 1) {
			$this->write_to_sap();
		}

		/* Clear buffer */
		$this->buffer = array();
	}

	function fill_buffer($v) {
		if ($v['doc_type'] == 'bill') {
			$price = (int)$v['price'];

			$rut = bconf_get($BCONF, "*.payment.nubox.bill.rut");
			$name = bconf_get($BCONF, "*.payment.nubox.bill.name");
			$lob = bconf_get($BCONF, "*.payment.nubox.bill.lob");
			$commune = bconf_get($BCONF, "*.payment.nubox.bill.commune");
			$address = bconf_get($BCONF, "*.payment.nubox.bill.address");
		}
		else {
			$iva = bconf_get($BCONF, '*.payment.iva');
			$price = ($v['price']*100)/(100+$iva);

			/*
			FT835: We use 3 decimals to send it to Nubox
			The decimal separator must be "comma"
			*/
			$price = round($price,3);
			$price = str_replace(".",",", $price);

			$rut = $v['rut'];
			$name = $v['name'];
			$lob = $v['lob'];
			$commune = bconf_get($BCONF, "*.common.region.$v[region].commune.$v[communes].name");
			$address = $v['address'];
		}
		//Save region for SAP endpoint
		$this->nubox_region = $v['region'];

		list($date) = explode(' ', $v['receipt']);
		$date = explode('-', $date);
		$date = "$date[2]-$date[1]-$date[0]";

		$sequence = count($this->buffer) + 1;
		$product_name = bconf_get($BCONF, "*.payment.product.$v[product_id].name");

		$csv_data = array(
			"TIPO" => bconf_get($BCONF, "*.payment.nubox.doc_type.$v[doc_type].id"),
			"FOLIO" => $v['purchase_id'],
			"SECUENCIA" => $sequence,
			"FECHA" => $date,
			"RUT" => $rut,
			"RAZONSOCIAL" => $this->receipt_clean($name),
			"GIRO" => $this->receipt_clean($lob),
			"COMUNA" => $this->receipt_clean($commune),
			"DIRECCION" => $this->receipt_clean($address),
			"AFECTO" => "si",
			"PRODUCTO" => $v['purchase_id'] . " - " . $product_name,
			"DESCRIPCION" => "",
			"CANTIDAD" => $v["product_quantity"],
			"PRECIO" => $price,
			"PORCENTDSCTO" => "0",
			"EMAIL" => $v['email'],
			"TIPOSERVICIO" => "3", // 1: sell, 2: home service, 3: services
			"PERIODODESDE" => "",
			"PERIODOHASTA" => "",
			"FECHAVENCIMIENTO" => "",
			"CODSUCURSAL" => "",
			"VENDEDOR" => "",
			"CODRECEPTOR" => "",
			"CODITEM" => "",
			"UNIDADMEDIDA" => "",
			"PORCENTDSCTO2" => "",
			"PORCENTDSCTO3" => "",
			"CODIGOIMP" => "",
			"MONTOIMP" => "",
			"INDICADORTRASLADO" => "",
			"FORMAPAGO" => "",
			"MEDIOPAGO" => "",
			"TERMINOSPAGOSDIAS" => "",
			"TERMINOSPAGOCODIGO" => "",
			"COMUNADESTINO" => "",
			"RUTSOLICITANTEFACTURA" => ""
		);

		array_push($this->buffer, $csv_data);
	}

	function run($log = true) {
		$this->logging = $log;
		$this->SAPService = new SAPService();
		$this->trans = new bTransaction();
		$this->trans->add_data('log_string', log_string());
		$this->trans->add_data('payment_status', 'paid');
		$this->trans->add_data('purchase_status', 'paid');
		$result = $this->trans->send_command('purchase_get_bills');

		if ($this->trans->has_error(true)) {
			$this->writelog("bill_processing: Error when trying to get the pending bills.", LOG_ERR);
			exit();
		}

		if (!isset($result['purchase_get_bills'])) {
			$this->writelog("bill_processing: no bills or invoices found");
			exit();
		}

		/* Authentication process */
		$nubox_domain = bconf_get($BCONF, '*.payment.nubox.domain');
		$nubox_auth_url = $nubox_domain . bconf_get($BCONF, '*.payment.nubox.auth_url');
		$nubox_load_url = $nubox_domain . bconf_get($BCONF, '*.payment.nubox.load_url');
		$nubox_pdf_url  = $nubox_domain . bconf_get($BCONF, '*.payment.nubox.pdf_url');
		$nubox_client_id = bconf_get($BCONF, '*.payment.nubox.client_id');
		$nubox_user_id = bconf_get($BCONF, '*.payment.nubox.user_id');
		$nubox_pass = base64_decode(bconf_get($BCONF, '*.payment.nubox.password'));
		$nubox_serie = bconf_get($BCONF, "*.payment.nubox.serie");
		$nubox_system = bconf_get($BCONF, "*.payment.nubox.system");

		$params = array( "rutCliente" => $nubox_client_id,
			"rutUsuario" => $nubox_user_id,
			"contrasena" => $nubox_pass,
			"sistema" => $nubox_system,
			"numeroSerie" => $nubox_serie
		);

		try {
			$this->soap_client = new SoapClient($nubox_auth_url);
			$auth_response = $this->soap_client->Autenticar($params);
			$this->auth_token = $auth_response->AutenticarResult;
			$this->writelog("Auth Token = " . $this->auth_token);
		}
		catch (Exception $e){
			$this->writelog($e->getMessage(), LOG_ERR);
			exit();
		}

		$this->soap_client = new SoapClient($nubox_load_url, array('connection_timeout' => 120));

		$current_purchase_id = -1;
		foreach ($result['purchase_get_bills'] as $k => $v) {
			if ($v['purchase_id'] != $current_purchase_id) {
				$this->write($current_purchase_id);
			}
			$current_purchase_id = $v['purchase_id'];
			$this->fill_buffer($v);
		}
		$this->write($current_purchase_id);
	}

    function receipt_clean($val) {
	   /*
	   * Used to clean the data that is sent to the external that take care of the bill/invoices
	   */
           // replace signs
	   $signs = array("\\", "�", "�", "~", "|", "\"", "�", "/", "'", "�", "�", "[", "^", "`", "]", "}", "{", "�", "�", ">", "< ", ";");
	   $val = str_replace($signs, '-', $val);
	   // replace non-ascii-printable characters
	   $val = preg_replace('/[^\x20-\x7E]/',' ', $val);
	   return $val;
    }

}

$log_param = true;
if ($argc > 1 && ($argv[1] == "false" || $argv[1] == "echo")) $log_param = false;
$bill_processing = new bill_processing();
$bill_processing->run($log_param);
