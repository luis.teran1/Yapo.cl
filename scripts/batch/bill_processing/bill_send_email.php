<?php

/*

bill_send_email.php

This script take all bills/invoices sent to nubox and send a email to the user
with a link to download the PDF file.

v0.2: Jaime Moreira / Pablo Mansilla
- Multiproduct capable... ugly, but capable :)

v0.1: Boris Cruchet (boris@schibsted.cl)
- Create the script

*/

require_once('autoload_lib.php');
require_once('init.php');

openlog("BILL_SENDMAIL", LOG_ODELAY, LOG_LOCAL0);

class bill_send_mail {
	var $buffer = array();
	var $trans;
	var $logging = true;

	function writelog($message, $log_level = LOG_INFO) {
		if ($this->logging) {
			syslog($log_level, log_string().$message);
		} else {
			echo($message.PHP_EOL);
		}
	}

	function send_mail() {
		if (count($this->buffer) == 0) return;
		$product_id = "";
		$quantity = "";
		$price = "";

		$purchase_id = $this->buffer[0]['purchase_id'];
		$doc_type = $this->buffer[0]['doc_type'];
		$doc_num = $this->buffer[0]['doc_num'];
		$external_doc_id = $this->buffer[0]['external_doc_id'];
		$email = $this->buffer[0]['email'];
		$bill_name = $this->buffer[0]['account_name'];
		$auth_code = $this->buffer[0]['auth_code'];

		list($date) = explode(' ', $this->buffer[0]['receipt']);
		$date = explode('-', $date);
		$bill_date = "$date[2]/$date[1]/$date[0]";

		// Here we are changing the data that is sent in the e-mail, so products are grouped **just** by product
		// Before this, we were grouping by product and price.
		// We used two foreach iterations to add legibility
		$product_list = array();
		foreach ($this->buffer as $v) {
			if (array_key_exists($v['product_id'], $product_list)) {
				$product_list[$v['product_id']]['quantity'] += $v['product_quantity'];
				$product_list[$v['product_id']]['price'] += $v['product_subtotal'];
			} else {
				$product_list[$v['product_id']] = array();
				$product_list[$v['product_id']]['quantity'] = $v['product_quantity'];
				$product_list[$v['product_id']]['price'] = $v['product_subtotal'];
			}
		}

		$product_id = array();
		$quantity = array();
		$price = array();
		foreach ($product_list as $key => $value) {
			array_push($product_id, $key);
			array_push($quantity, $value['quantity']);
			array_push($price, $value['price']);
		}

		$product_id = implode("@", $product_id);
		$quantity = implode("@", $quantity);
		$price = implode("@", $price);

		$this->trans->reset();
		$this->trans->add_data('log_string', log_string());
		$this->trans->add_data('doc_type', $doc_type);
		$this->trans->add_data('doc_num', $doc_num);
		$this->trans->add_data('external_doc_id', $external_doc_id);
		$this->trans->add_data('purchase_id', $purchase_id);
		$this->trans->add_data('product_id', $product_id);
		$this->trans->add_data('quantity', $quantity);
		$this->trans->add_data('total_price', $price);
		$this->trans->add_data('to', $email);
		$this->trans->add_data('bill_date', $bill_date);
		$this->trans->add_data('auth_code', $auth_code);

		if ($bill_name != "") {
			$this->trans->add_data('bill_name', $bill_name);
		}

		$this->writelog("Starting to process purchase_id= $purchase_id");

		$this->trans->send_command('payment_receipt_mail');
		if ($this->trans->has_error(true)) {
			$this->writelog("Error when trying to send the mail ($doc_num - $external_doc_id - $email)", LOG_ERR);
		}

		$this->writelog("sending mail for $purchase_id");

		$this->trans->reset();

		$this->trans->add_data('log_string', log_string());
		$this->trans->add_data('purchase_id', $purchase_id);
		$this->trans->add_data('status', 'confirmed');

		$result_update = $this->trans->send_command('purchase_update');

		if ($this->trans->has_error(true)) {
			$this->writelog("error when trying to update the purchase state. purchase_id: $purchase_id", LOG_ERR);
		}

		$this->buffer = array();
	}

	function fill_buffer($v) {
		array_push($this->buffer, $v);
	}

	function run($log = true) {
		$this->logging = $log;
		$this->trans = new bTransaction();
		$this->trans->add_data('log_string', log_string());
		$this->trans->add_data('payment_status', 'paid');
		$this->trans->add_data('purchase_status', 'sent');
		$result = $this->trans->send_command('purchase_get_bills');

		if ($this->trans->has_error(true)) {
			$this->writelog("Error when trying to get the bills/invoices to send the email.");
			exit();
		}

		if (!isset($result['purchase_get_bills'])) {
			$this->writelog("bill_send_email: no bills or invoices found");
			syslog(LOG_INFO, log_string()."bill_send_email: no bills or invoices found");
			echo "bill_send_email: no bills or invoices found\n";
			exit();
		}

		$current_purchase_id = -1;
		foreach ($result['purchase_get_bills'] as $k => $v) {
			if ($v['purchase_id'] != $current_purchase_id) {
				$this->send_mail($current_purchase_id);
			}
			$current_purchase_id = $v['purchase_id'];
			$this->fill_buffer($v);
		}
		$this->send_mail($current_purchase_id);
	}
}

$log_param = true;
if ($argc > 1 && ($argv[1] == "false" || $argv[1] == "echo")) $log_param = false;
$bill_send_mail = new bill_send_mail();
$bill_send_mail->run($log_param);
?>
