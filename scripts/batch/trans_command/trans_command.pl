#!/usr/bin/perl
use Getopt::Long;
use blocket::bconf;
use perl_trans;
use strict;

my $configfile = "/opt/blocket/conf/bconf.conf";

GetOptions("configfile=s" => \$configfile);
die ("usage: $0 [-configfile <bconf.conf>]") if !@ARGV;

my $bconf = blocket::bconf::init_conf($configfile);

# Command
my $command = @ARGV[0];
$command =~ s/\\n/\n/g;

# Explode command
my %params =  split(/[\n:]/, $command);
my %resp = bconf_trans($$bconf{'*'}{'common'}{'transaction'}{'host'}, %params);

# Check if command failed
die ("Failed to run command: " . @ARGV[0]) if (!$resp{'status'} eq "TRANS_OK");

# Print result
foreach my $key (sort keys %resp) {
	print "$key: $resp{$key}\n";
}
