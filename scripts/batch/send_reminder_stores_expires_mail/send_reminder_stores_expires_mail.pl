#!/usr/bin/perl -w


use strict;
use DBI;
use Getopt::Long;
use Data::Dumper;
use blocket::bconf;
use perl_trans;

my $pghost = "";
my $bconffile = "";
my $interval = "";
my $date_range = "days";
my $counter = 0;

# select ads that are active, not spidered and $interval days old. 
# They should not have a half_time mail ad_action. Limit (bconf?)

GetOptions("pghost=s" => \$pghost,
	   "interval=s" => \$interval,
	   "date_range=s" => \$date_range,
	   "bconffile=s"  => \$bconffile);

die "usage(executable -pghost -bconffile) not found" if (!$bconffile || !$pghost);

my $bconf = blocket::bconf::init_conf($bconffile);

$interval = $$bconf{'stores_expires'}{'warning_interval'} unless($interval);
$date_range = $$bconf{'stores_expires'}{'warning_range'} ;
die "Failed to get period to el expires stores from\n" unless($interval);

my $transhost = $$bconf{'*'}{'common'}{'transaction'}{'host'};
my $pgdbh = DBI->connect("DBI:Pg:dbname=blocketdb;host=$pghost", $ENV{'USER'}, '') || die "Failed to connect to postgresql\n";

my $db_res = $pgdbh->selectall_hashref("
SELECT
        st.store_id,
		st.name as store_name,
		st.date_start,
		acc.name as account_name,
		acc.email as account_email
FROM
        stores st
		join accounts acc using(account_id)
		WHERE st.status = 'active' AND DATE_TRUNC('$date_range', now()) = DATE_TRUNC('$date_range', date_end - interval '$interval $date_range');", "store_id");

my $datestring = localtime();
print "Interval:$interval $date_range - Start:$datestring\n";

# Run store_reminder_mail transaction for all store expires
foreach my $store_id (keys %{$db_res}) {

	print "Send reminder_store_mail for store_id =".$store_id."\n";
	my %transcmd;

	$transcmd{'cmd'} = "store_reminder_mail";
	$transcmd{'commit'} = "1";
	$transcmd{'name'} = $db_res-> { $store_id } ->{ 'account_name' };
	$transcmd{'email'} = $db_res-> { $store_id } ->{ 'account_email' };
	$transcmd{'store_id'} = $db_res-> { $store_id } ->{ 'store_id' };
	$transcmd{'store_name'} = $db_res-> { $store_id } ->{ 'store_name' };
	$transcmd{'date_start'} = $db_res-> { $store_id } ->{ 'date_start' };
	$transcmd{'days'} = $interval;

	my %resp = bconf_trans($transhost, %transcmd);

	select(undef, undef, undef, 0.25);

	if ($resp{'status'} ne "TRANS_OK") {
		print Dumper(%resp);
		die "Failed to store reminder mail, bailing out";
	}

	$counter = $counter + 1;
}
$datestring = localtime();
print "Emails sent: $counter End: $datestring\n";
