#!/bin/bash
pastmonth=`date +%m -d "-1 month"`
pastyear=`date +%Y -d "-1 month"`
thismonth=`date +%m`
thisyear=`date +%Y`

cd /opt/blocket/bin

/opt/blocket/bin/show_template -a controlpanel start_time=$pastyear-$pastmonth-01 end_time=$thisyear-$thismonth-01 monthly=1 stats/reviewstats.txt  >/home/batch/cron/granskstat.txt

cd /home/batch/cron


for email in kontoret@blocket.se jesper.l@blocket.se planning@besedo.se oscar.g@blocket.se tine.hauge@besedo.se planningKL@besedo.com fredrik.thalberg@besedo.com joacim.orhall@besedo.com
do
 cat granskstat.txt | /opt/blocket/libexec/sendmail -s "Månadsstatistik granskning" $email
done
