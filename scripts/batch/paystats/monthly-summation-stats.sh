#!/bin/bash

if [ -z "$BDIR" ]; then
	BDIR=/opt/blocket
fi

if [ -z "$STATSDIR" ]; then
	export STATSDIR=/home/batch/cron/stats
fi

mailaddrs="peter@blocket.se zakay@blocket.se patrik@blocket.se"

if [ -x $BDIR/libexec/sendmail ] ; then
	sendmail=$BDIR/libexec/sendmail
else
	sendmail=/usr/bin/sendmail
fi

if [ -z "$1" ] || [ -z "$2" ] ; then
	startdate=`date -d 'last month' +%Y-%m-%d`
	month=`date -d 'last month' +%Y-%m`
else
	startdate="$1-$2-01"
	month="$1-$2"
fi

cd $BDIR/bin
./show_template month=$startdate stats/monthly_summation.xml | iconv -f iso-8859-1 -t UTF8  > $STATSDIR/monthly-summation-$month.xml

for email in $mailaddrs
do
	printf "\n" | $sendmail -a $STATSDIR/monthly-summation-$month.xml -s "Monthly summation $month" $email
done
