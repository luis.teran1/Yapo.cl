#!/bin/bash
scriptdir=/opt/blocket/bin
export LOGDIR=/log
export STATSDIR=/home/batch/cron/stats
export SCRATCHDIR=/scratch
pghost="10.0.100.7"
mailaddrs="martin@blocket.se patrik@blocket.se erik.k@blocket.se eddy@blocket.se"

runyear=$1
if [ -z "$runyear" ]; then
   runyear=`date +%Y -d yesterday`
fi

runmonth=$2
if [ -z "$runmonth" ]; then
   runmonth=`date +%m -d yesterday`
fi

runperiod=$1$2

$scriptdir/monthly-ad-stats.sh $runyear $runmonth > $STATSDIR/monthly-$runperiod-ads.txt

for email in $mailaddrs
do
    if [ -x /opt/blocket/libexec/sendmail ] ; then
        sendmail=/opt/blocket/libexec/sendmail
    else
        sendmail=/usr/bin/sendmail
    fi
 cat $STATSDIR/monthly-$runperiod-ads.txt | $sendmail -s "Monthly stats $runperiod" $email
done
