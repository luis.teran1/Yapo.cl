#!/usr/bin/perl -w

use strict;
use DBI; 

use Data::Dumper;

$|=1;
defined($ARGV[0]) or die "usage: X db-host YYYYMMDD";
defined($ARGV[1]) or die "usage: X db-host YYYYMMDD";

my $pghost = $ARGV[0];
my $rundate = $ARGV[1];

my $pgdbh = DBI->connect("DBI:Pg:dbname=blocketdb;host=$pghost", $ENV{'USER'}, '') || die "Failed to connect to postgresql\n";

my $today = $pgdbh->selectall_arrayref("select '$rundate'::timestamp + '1 day'::interval as today")->[0][0];
my $yesterday = $rundate;

my $pending_review_new = $pgdbh->selectall_hashref("select extract(hour from timestamp) as hour, count(*) from ad_actions join action_states using (ad_id, action_id) where action_states.state in ('pending_review') and timestamp > '$yesterday' and timestamp < '$today' and action_type in ('new', 'renew', 'prolong') and queue not in ('autorefuse', 'autoaccept') group by hour order by hour;", 1);

my $pending_review_edit = $pgdbh->selectall_hashref("select extract(hour from timestamp) as hour, count(*) from ad_actions join action_states using (ad_id, action_id) where action_states.state in ('pending_review') and timestamp > '$yesterday' and timestamp < '$today' and action_type in ('edit') and queue not in ('autorefuse', 'autoaccept')group by hour order by hour;", 1);

my $pending_review_other = $pgdbh->selectall_hashref("select extract(hour from timestamp) as hour, count(*) from ad_actions join action_states using (ad_id, action_id) where action_states.state in ('pending_review') and timestamp > '$yesterday' and timestamp < '$today' and action_type not in ('new', 'renew', 'prolong', 'edit') and queue not in ('autorefuse', 'autoaccept') group by hour order by hour;", 1);

my $pending_review_auto = $pgdbh->selectall_hashref("select extract(hour from timestamp) as hour, count(*) from ad_actions join action_states using (ad_id, action_id) where action_states.state in ('pending_review') and timestamp > '$yesterday' and timestamp < '$today' and queue in ('autorefuse', 'autoaccept') group by hour order by hour;", 1);

my $tot_new;
my $tot_edit;
my $tot_other;
my $tot_hour;
my $tot_tot;
my $tot_auto;

print "Submitted to the manual review queue\n";
print "            New     Edited    Others    Total           Automatic queue\n";
my $i;
for ($i=0; $i<24; $i++) {
    if (!defined $pending_review_new->{$i}{'count'}) {
	$pending_review_new->{$i}{'count'} = 0;
    }
    if (!defined $pending_review_edit->{$i}{'count'}) {
	$pending_review_edit->{$i}{'count'} = 0;
    }
    if (!defined $pending_review_other->{$i}{'count'}) {
	$pending_review_other->{$i}{'count'} = 0;
    }
    if (!defined $pending_review_auto->{$i}{'count'}) {
	$pending_review_auto->{$i}{'count'} = 0;
    }

    $tot_hour = $pending_review_new->{$i}{'count'} + 
	$pending_review_edit->{$i}{'count'} + 
	$pending_review_other->{$i}{'count'};

    printf("%02d-%02d%10d%10d%10d%10d\t\t%10d\n", 
	    $i, $i+1, 
	    $pending_review_new->{$i}{'count'}, 
	    $pending_review_edit->{$i}{'count'}, 
	    $pending_review_other->{$i}{'count'},
	    $tot_hour,
	    $pending_review_auto->{$i}{'count'});
	    
    $tot_new += $pending_review_new->{$i}{'count'};
    $tot_edit += $pending_review_edit->{$i}{'count'};
    $tot_other +=  $pending_review_other->{$i}{'count'};
    $tot_auto +=  $pending_review_auto->{$i}{'count'};
    $tot_tot += $tot_hour;
}
print "-" x 67;
print "\n";
printf "Total:%8d%10d%10d%10d\t\t%10d\n", $tot_new, $tot_edit, $tot_other, $tot_tot, $tot_auto; 
print "\n";
print "'New' are new ones and prolonged .\n";
print "'Edited' are edited ads.\n";
print "'Others' is everything else. Today they consist of the following actions:\n";

my $action_types_ref = $pgdbh->selectall_arrayref("select distinct action_type from ad_actions join action_states using (ad_id, action_id) where action_states.state in ('pending_review') and timestamp > '$yesterday' and timestamp < '$today' and action_type not in ('edit', 'new', 'renew', 'prolong')");

foreach my $row ( @{$action_types_ref} ) {
	print @{$row}[0] . "\n";
}
