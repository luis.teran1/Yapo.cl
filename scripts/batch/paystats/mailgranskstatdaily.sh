#!/bin/bash
export PGOPTIONS="-c search_path=bpv,public"

if [ -z "$pghost" ]; then
	pghost="10.49.0.30"
fi

if [ -z "$BDIR" ]; then
	BDIR=/opt/blocket
fi
if [ -z "$WORKDIR" ]; then
	WORKDIR=/home/batch/cron.d
fi

offset=1
if [ -n "$1" ] ; then
        offset=$1
fi
(( next_day = offset - 1 ))


if [ "$offset" = 0 ]; then
	yesterday=`date +%Y-%m-%d`
	today=`date +%Y-%m-%d -d "+1 day"`
else
	yesterday=`date +%Y-%m-%d -d "-$offset day"`
	today=`date +%Y-%m-%d -d "-$next_day day"`
fi

cd $BDIR/bin

$BDIR/bin/show_template -a controlpanel start_time=$yesterday end_time=$today stats/reviewstats.txt > $WORKDIR/granskstatdaily.txt
$BDIR/bin/reviewstathourly.pl $pghost $yesterday >> $WORKDIR/granskstatdaily.txt

for email in andries@schibsted.com.br joao@schibsted.com.br anajulia@schibsted.com.br alvaro@schibsted.com.br cristhiano@schibsted.com.br

do
 echo "Subject: Reviewing Statistics $yesterday" | cat - $WORKDIR/granskstatdaily.txt | $BDIR/libexec/sendmail $email
done
