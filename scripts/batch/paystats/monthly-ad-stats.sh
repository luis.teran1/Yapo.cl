#!/bin/bash

runmonth=$1
if [ -z "$runmonth" ] ; then
  echo "usage: $0 YYYY MM";
  exit 1;
fi

startdate=${1}-${2}-01
enddate=`date -d "$startdate + 1 month" +%Y-%m-%d`
year=`date -d "$startdate" +%Y`

cd /opt/blocket/bin/
./show_template startdate=$startdate enddate=$enddate year=$year runmonth=$runmonth stats/monthly_ad_stats.txt
