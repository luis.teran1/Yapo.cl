#!/usr/bin/perl -w

use strict;
use Getopt::Long;
use blocket::bconf;
use Data::Dumper;

require "perl_trans.pm";

my $configfile;
my $debug = 0;
my $day;
my $month;

sub my_log {
	my ($loglevel, $message) = @_;

	print "($loglevel) $message\n";
}

# init, argv, open syslog
GetOptions("configfile=s" => \$configfile,
	   "day=s" => \$day,
	   "month=s" => \$month,
	   "debug" => \$debug);

my $bconf = blocket::bconf::init_conf($configfile);

my %param;
$param{cmd} = "mail_stats";
$param{commit} = "1";
if ($month) {
	$param{month} = $month;
} elsif ($day) {
	$param{day} = $day;
} else {
	$param{day} = `date +'\%Y-\%m-\%d' -d 'yesterday'`;
}

my %resp = bconf_trans($bconf->{'*'}->{'common'}->{'transaction'}->{'host'}, %param);

if (!%resp) {
	my_log('crit', '(CRIT) Failed to communicate with trans');
	exit(1);
}

if ($resp{status} ne "TRANS_OK") {
	my_log('crit', "(CRIT) trans returned status: $resp{status}");
	exit(1);
}

print Dumper(%resp) if $debug;

