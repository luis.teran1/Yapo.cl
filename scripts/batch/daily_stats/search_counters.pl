#!/usr/bin/perl

use Getopt::Long;
use blocket::bconf;
use perl_trans;
use strict;
use Data::Dumper;

my $configfile = "/opt/blocket/conf/bconf.conf";
my $debug;
my $rundate = 'yesterday';

# init, argv, open syslog
GetOptions("configfile=s" => \$configfile,
	   "rundate=s" => \$rundate,
	   "debug" => \$debug);

die "usage: $0 [-configfile <bconf.conf>] [-rundate <date>] [-debug] <searchlog-file> [<counter-name> ...]" if !@ARGV;

my $bconf;
my $i = 20;
do {
	$bconf = blocket::bconf::init_conf($configfile);
	sleep(1);
} while !$bconf && $i-- > 0;
die "(CRIT) Failed to load bconf" if !$bconf;

$|=1;

my %hourdata = ();

my $searchlog = shift;
open SEARCHLOG, '<', $searchlog;
while (<SEARCHLOG>) {
	/^... .. ([0-9]{2}):.*invword_counter: ([^ ]+) ([0-9]+)/ or next;

	my $hour = $1;
	my $counter_name = $2;
	my $val = $3;

	$hourdata{$counter_name}[$hour] += $val;
}
close SEARCHLOG;

$rundate = `date +'\%Y-\%m-\%d' -d '$rundate'`;
chomp $rundate;
my $crit = 0;
foreach my $key (sort keys %hourdata) {
	my @vals = @{$hourdata{$key}};
	my $hour;

	foreach my $i (0 .. $#vals) {
		my $val = $vals[$i];

		next if !defined($val) || $val == 0;

		next if @ARGV && !grep $_ eq $key, @ARGV;

		print "$key ($i) => $val\n" if $debug;

		my %trans;
		$trans{'cmd'} = 'update_stats';
		$trans{'commit'} = 1;
		$trans{'stat_name'} = $key;
		$trans{'stat_value'} = $val;
		$trans{'stat_time'} = sprintf('%s %02d:00:00', $rundate, $i);

		my %resp = bconf_trans($$bconf{'*'}{'common'}{'transaction'}{'host'}, %trans);
		if (!%resp) {
			print STDERR "(CRIT) Failed to communicate with trans.\n";
			$crit++;
		} elsif ($resp{'status'} ne "TRANS_OK") {
			print STDERR "(CRIT) trans returned status: $resp{status}\n";
			$crit++;
		}
	}
}

exit 1 if $crit;
