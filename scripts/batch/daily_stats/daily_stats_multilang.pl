#!/usr/bin/perl -w

use Getopt::Long;
use blocket::bconf;
use perl_trans;
use strict;
use Data::Dumper;

my $configfile = "/opt/blocket/conf/bconf.conf";
my $debug;
my $rundate = 'yesterday';
my $lang;
my $from_test;

# init, argv, open syslog
GetOptions("configfile=s" => \$configfile,
	   "rundate=s" => \$rundate,
	   "debug" => \$debug,
	   "lang=s" => \$lang,
	   "from_test" => \$from_test # This is used to tell the script to NOT run templates found in bconf.txt.stats (mainly for testing).	
);
	   
die "usage: $0 [-configfile <bconf.conf>] [-rundate <date>] [-debug] [-from_test] [-lang <lang>] <stats1x1log-file> [<stat_name> ...]" if !@ARGV;

my $bconf;
my $i = 20;
do {
	$bconf = blocket::bconf::init_conf($configfile);
	sleep(1);
} while !$bconf && $i-- > 0;
die "(CRIT) Failed to load bconf" if !$bconf;
die "(CRIT) Lang parameter needed " if !$lang;

$|=1;

my %trans;
my $max_vid;
my $before_yesterday = `date +'\%Y-\%m-\%d' -d '2 day ago'`;
$before_yesterday =~ s/\n//g;

# we get the max vid from the day before yesterday in order to calculate the created visitors yesterday
$trans{'cmd'} = 'select_maxvid';
$trans{'date'} = $before_yesterday;
$trans{'commit'} = 1;

my %resp = bconf_trans($$bconf{'*'}{'common'}{'transaction'}{'host'}, %trans);
if (!%resp) {
	print STDERR "(CRIT) Failed to communicate with trans.\n";
} elsif ($resp{'status'} ne "TRANS_OK") {
	print STDERR "(CRIT) trans returned status: $resp{status}\n";
}

if (!$resp{'max'}) {
	$max_vid = 0;
} else {
	$max_vid = $resp{'max'};
}
my %hips;
my %ips;
my %lips;
my %firstips;
my %data;

my $uip_number_per_aiform_lang= 0;
my %aiform_ips = ();
my $uip_number_per_aiconfirm_lang= 0;
my %aiconfirm_ips = ();
my $uip_number_per_aipreview_lang= 0;
my %aipreview_ips = ();
my $uip_number_per_verify_lang= 0;
my %verify_ips = ();

my $stats1x1log = shift;
open IPLOG, '<', $stats1x1log;
while (<IPLOG>) {
    
     	/^.*: (.*) \[.{11}:(\d{2}):.*\] .*1x1_pages_([a-z]+)_.*_l(\w{2}).*\.gif.*/ or next;

	my $ip = $1;
	my $hour = $2;
	my $page_type = $3;
	my $ilang = $4;
    
	$hips{$hour}{$ip}++;
	my $num = ++$ips{$ip};
    	++$lips{$ilang}{$ip};
	$hips{$hour}{$ip}++;
	if ($ips{$ip} == 1) {
		$firstips{$hour}{$ip} = 1;
	}

	# Increment the correct page counter only for current language
	if ($ilang eq $lang) {
		if ($page_type eq "aiform" ) {
			++$aiform_ips{$ip};
			if ($aiform_ips{$ip} == 1) {
				$uip_number_per_aiform_lang++;
			}
		} elsif ($page_type eq "vf" || $page_type eq "verify") {
			++$verify_ips{$ip};
			if ($verify_ips{$ip} == 1) {
				$uip_number_per_verify_lang++;
			}
		} elsif ($page_type eq "aipreview" ) {
			++$aipreview_ips{$ip} ;
			if ($aipreview_ips{$ip} == 1) {
				$uip_number_per_aipreview_lang++;
			}
		} elsif ($page_type eq "aiconfirm" ) {
			++$aiconfirm_ips{$ip};
			if ($aiconfirm_ips{$ip} == 1) {
				$uip_number_per_aiconfirm_lang++;
			}
		}
	}
}
close IPLOG;

# Visits per language 
if ($lang) {
    my %vids;
    my %vid_time;
    my $first_time_visitors = 0;
    
    open STATS_LOG, '<', $stats1x1log;
    while (<STATS_LOG>) {
	/^.*1x1_pages_.*_l$lang.gif\?r=(\d{1,20})(.*)\" (\d{1,9}) \d{3} \[(.*?)\].*/ or next;
        my $time = $1;
	my $left_url = $2; # new_click 
        my $vid = $3;
	my $redir_value = $4 if (defined $4); # redir value
	if ($vid > $max_vid && !exists($vids{$vid})) {
	    ++$first_time_visitors;
        }

        ++$vids{$vid};
    
	# check if is a visits directly from a banner
	if (index($left_url, 'new_click') != -1) {
            $vid_time{$vid}{$time}{'new_click'} = 1;
	} else {
            $vid_time{$vid}{$time}{'new_click'} = 0;
	}

	if (defined $redir_value && $redir_value eq "") {
            $vid_time{$vid}{$time}{'redir'} = 0;
	} else {

            $vid_time{$vid}{$time}{'redir'} = 1;
	}
    }
    close STATS_LOG;
    
    my $visits = 0;
    my $entering_visits = 0;
    # entering visits comming at least the first pageview from a banner 
    my $paid_visits = 0; # entering visits comming at least the first pageview from a banner 
    # entering visit where user has redir cookie but doen't come from banner campaign
    my $free_entering_visits = 0; # entering visit where user has redir cookie but doen't come from banner campaign
    my $average_visit_time = 0;
    
    foreach my $vid ( sort keys %vids ) {
        my $count = 0;
        my $time_interval = 0;
        my $just_came = 0;
        my $campaign_visit = 0;
        my $visit_duration = 0;
        foreach my $time ( sort { $a <=> $b } keys %{$vid_time{$vid}} ) {
            if ($count == 0 ) {
                $time_interval = $time;
                $visits++;
		if ($vid_time{$vid}{$time}{'new_click'}) {
        		$campaign_visit = 1;
		}
                $just_came = 1;
                $visit_duration = 0;
            } else {
                $time_interval = $time - $time_interval;
                if ($time_interval > 1800000) {
                        $visits++;
                        $just_came = 1;
                        $average_visit_time += $visit_duration;
                        $visit_duration = 0;
			if ($vid_time{$vid}{$time}{'new_click'}) {
        			$campaign_visit = 1;
			}
                } else {
                    if ($just_came == 1 ) {
                    	if ($campaign_visit == 1 ) {
			    $paid_visits++;
        	  	    $campaign_visit = 0;
			} elsif ($vid_time{$vid}{$time}{'redir'} == 0) {
			    $free_entering_visits++;
			}
                        $entering_visits++;
                        $just_came = 0;
                    }
                    $visit_duration += $time_interval;
                }
                $time_interval = $time;
            }
            $count++;
        }
        $average_visit_time += $visit_duration;
    }
    
    $average_visit_time = ( $entering_visits == 0 ) ? 0 : $average_visit_time / ($entering_visits * 1000);
    $average_visit_time =~ m/(\d+)\./;
    $average_visit_time = $1 if ( $1 );
	
    chomp (my $page_views_lang=  `cat $stats1x1log | egrep 1x1_.*l$lang | wc -l`);
    chomp (my $adviews_lang = `cat $stats1x1log | egrep 1x1_pages_vi_.*l$lang | wc -l`);

    %data = (%data, 
        "unique_ip_".$lang => scalar keys %{$lips{$lang}},
	"unique_ip_per_aiform_".$lang => $uip_number_per_aiform_lang,
	"unique_ip_per_aiconfirm_".$lang => $uip_number_per_aiconfirm_lang,
	"unique_ip_per_aipreview_".$lang => $uip_number_per_aipreview_lang,
	"unique_ip_per_verify_".$lang => $uip_number_per_verify_lang,
	"page_views_".$lang => $page_views_lang,
	"visits_".$lang => $visits,
	"adviews_1x1_".$lang => $adviews_lang,
        "entering_visits_from_click_".$lang => $paid_visits,
        "free_entering_visits_".$lang => $free_entering_visits,
        "avg_duration_of_visits_".$lang => $average_visit_time,
        "first_time_visitors_".$lang => $first_time_visitors,
	"entering_visits_".$lang => $entering_visits,
	"visitors_" . $lang => scalar keys %vids);

}

# getting last stat execution time
my $last_execution_time;
%trans = ();
$trans{'cmd'} = 'last_stat_time';
$trans{'commit'} = 1;

%resp = ();
%resp = bconf_trans($$bconf{'*'}{'common'}{'transaction'}{'host'}, %trans);
if (!%resp) {
	print STDERR "(CRIT) Failed to communicate with trans.\n";
} elsif ($resp{'status'} ne "TRANS_OK") {
	print STDERR "(CRIT) trans returned status: $resp{status}\n";
}


if (!$resp{'last_stat_time.0.value'}) {
	$last_execution_time = `date +'\%Y-\%m-\%d \%H:\%M:\%S' -d '$rundate'`;
} else {
	$last_execution_time = $resp{'last_stat_time.0.value'};
}

$rundate = `date +'\%Y-\%m-\%d' -d '$rundate'`;
chomp $rundate;
my $crit = 0;


foreach my $key (sort keys %data) {
	my $val = $data{$key};
	my $hour;

	next if @ARGV && !grep $_ eq $key, @ARGV;

	print "$key => $val\n" if $debug;

	my %trans;
	$trans{'cmd'} = 'update_stats';
	$trans{'commit'} = 1;
	$trans{'stat_name'} = $key;
	$trans{'stat_value'} = $val;
	$trans{'stat_time'} = "$rundate 00:00:00";
	$trans{'last_stat_time'} = $last_execution_time;
	$trans{'lang'} = $lang;
		
	my %resp = bconf_trans($$bconf{'*'}{'common'}{'transaction'}{'host'}, %trans);
	if (!%resp) {
		print STDERR "(CRIT) Failed to communicate with trans.\n";
		$crit++;
	} elsif ($resp{'status'} ne "TRANS_OK") {
		print STDERR "(CRIT) trans returned status: $resp{status}\n";
		$crit++;
	}
}
exit 1 if $from_test; # If we do not want to update stats, exit at this point.

foreach my $key (sort keys %{$$bconf{'*'}{'stats'}}) {
	my $data = $$bconf{'*'}{'stats'}{$key};

	next if !$$data{'data_template'};
	next if $$data{'not_multilang'};
	next if $data{$key};

	my $max = 0;
	$max = 23 if $$data{'type'} eq 'hourly';

	for my $i (0 .. $max) {
		next if @ARGV && !grep $_ eq $key, @ARGV;

		print "$key ($i)\n" if $debug;

		my %update_trans;
		$update_trans{'cmd'} = 'update_stats';
		$update_trans{'commit'} = 1;
		$update_trans{'stat_name'} = $key;
		$update_trans{'stat_time'} = sprintf('%s %02d:00:00', $rundate, $i);
		$update_trans{'last_stat_time'} = $last_execution_time;
		
		if ($lang) {
			$update_trans{'lang'} = $lang;
		}

		my %resp = bconf_trans($$bconf{'*'}{'common'}{'transaction'}{'host'}, %update_trans);
		if (!%resp) {
			print STDERR "(CRIT) Failed to communicate with trans.\n";
			$crit++;
		} elsif ($resp{'status'} ne "TRANS_OK") {
			print STDERR "(CRIT) trans returned status: $resp{status}\n";
			$crit++;
		}
		$resp{'o_value'} = join(', ', @{$resp{'o_value'}}) if $debug && ref($resp{'o_value'}) eq "ARRAY";
		print "$key => $resp{o_value}\n" if $debug && defined $resp{'o_value'};
	}
}

exit 1 if $crit;
