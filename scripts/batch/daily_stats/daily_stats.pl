#!/usr/bin/perl

use Getopt::Long;
use blocket::bconf;
use perl_trans;
use strict;
use Data::Dumper;
use MIME::Base64;
use Date::Parse;

my $configfile = "/opt/blocket/conf/bconf.conf";
my $debug;
my $rundate = 'yesterday';
my $from_test;
my $stats1x1log = "";

# In order to run the script in regress, modify the following to suit your needs....
# export BASEDIR=/home/$USER/yapo_cl
# export PERL5LIB=$BASEDIR/regress_final/lib/perl5/5.8.8:$BASEDIR/modules/perl_bconf/lib:$BASEDIR/lib/perl
# ... -configfile $BASEDIR/regress_final/conf/bconf.conf

# init, argv, open syslog
GetOptions("configfile=s" => \$configfile,
	   "rundate=s" => \$rundate,
	   "pixel_log=s" => \$stats1x1log,
	   "debug" => \$debug,
	   "from_test" => \$from_test # This is used to tell the script to NOT run templates found in bconf.txt.stats (mainly for testing).
);

my $bconf;
my $i = 20;
do {
	$bconf = blocket::bconf::init_conf($configfile);
	sleep(1);
} while !$bconf && $i-- > 0;
die "(CRIT) Failed to load bconf" if !$bconf;

sub bsearch_query {
	my ($query, $engine) = @_;
	my @resp;

	$engine = 'asearch' if not $engine;

	my $addr = inet_aton($bconf->{'*'}->{'common'}->{$engine}->{'host'}->{'1'}->{'name'});
	my $paddr = sockaddr_in($bconf->{'*'}->{'common'}->{$engine}->{'host'}->{'1'}->{'port'}, $addr);

	socket(BSEARCH, PF_INET, SOCK_STREAM, getprotobyname('tcp')) || return @resp;
	setsockopt(BSEARCH, SOL_SOCKET, SO_SNDTIMEO, pack('L!L!', 5, 0) ) || return @resp;
	setsockopt(BSEARCH, SOL_SOCKET, SO_RCVTIMEO, pack('L!L!', 5, 0) ) || return @resp;
	connect(BSEARCH, $paddr) || return undef;
	BSEARCH->autoflush(1);

	print STDERR "$query\n" if $debug;
	print BSEARCH "$query\n";

	@resp = <BSEARCH>;
	shift @resp while ($resp[0] =~ /^info:/);

	close (BSEARCH);

	return @resp;
}

my $total_lists = 0;
my $empty_lists = 0;
my $total_searchs = 0;
my $empty_searchs = 0;

sub list_management {

	my $info = $_[0];

	if ($info =~ /.*_qh(\d+).*http.*&?q=([^&]*).*/){
		my $qh = $1;
		my $qs = $2;

		if ($qs eq ""){
			$total_lists++;
			if ($qh eq "0"){
				$empty_lists++;
			}
		} else {
			$total_searchs++;
			if ($qh eq "0"){
				$empty_searchs++;
			}
		}
	} else {
		$total_lists++;
		$info =~ /.*_qh(\d+).*/;
		my $qh = $1;
		if ($qh eq "0"){
                        $empty_lists++;
                }
	}
}
$|=1;

my %hips;
my %ips;
my %firstips;

my @top_5_ips = ();
my @top_5_visits = ();

my $uip_number_per_aiform = 0;
my %aiform_ips = ();
my $uip_number_per_aiconfirm = 0;
my %aiconfirm_ips = ();
my $uip_number_per_aipreview = 0;
my %aipreview_ips = ();
my $uip_number_per_verify = 0;
my %verify_ips = ();

if ($stats1x1log ne "") {
	open IPLOG, '<', $stats1x1log;
	while (<IPLOG>) {
		/^.*[pixel|www_statsUxU]: (.*) \[.{11}:(\d{2}):.*\] "GET \/1x1_pages_([a-z]+)_(.*)/ || next;

		my $ip = $1;
		my $hour = $2;
		my $page_type = $3;
		my $request_info = $4;

		$hips{$hour}{$ip}++;
		my $num = ++$ips{$ip};
		if ($ips{$ip} == 1) {
			$firstips{$hour}{$ip} = 1;
		}

		# Find the right position in top 5 list, if any.
		my $i = 0;
		for (; $i < @top_5_ips; $i++) {
			last if $num < $top_5_visits[$i];

			# Remove the old value if already in list.
			if ($ip eq $top_5_ips[$i]) {
				splice @top_5_ips, $i, 1;
				splice @top_5_visits, $i, 1;
				$i--;
			}
		}
		if (@top_5_ips < 5 || $i > 0) {
			splice @top_5_ips, $i, 0, $ip;
			splice @top_5_visits, $i, 0, $num;
			while (@top_5_ips > 5) {
				shift @top_5_ips;
				shift @top_5_visits;
			}
		}

		# Increment the correct page counter
		if ($page_type eq "aiform" ) {
			++$aiform_ips{$ip};
			if ($aiform_ips{$ip} == 1) {
				$uip_number_per_aiform++;
			}
		} elsif ($page_type eq "vf" || $page_type eq "verify") {
			++$verify_ips{$ip};
			if ($verify_ips{$ip} == 1) {
				$uip_number_per_verify++;
			}
		} elsif ($page_type eq "aipreview" ) {
			++$aipreview_ips{$ip} ;
			if ($aipreview_ips{$ip} == 1) {
				$uip_number_per_aipreview++;
			}
		} elsif ($page_type eq "aiconfirm" ) {
			++$aiconfirm_ips{$ip};
			if ($aiconfirm_ips{$ip} == 1) {
				$uip_number_per_aiconfirm++;
			}
		} elsif ($page_type eq "li" ) {
			list_management($4);
		}
	}
	close IPLOG;
}

# dfc => Directly coming From Campaigns
# $*_dfc[0] => excluding predefined campaigns
# $*_dfc[1] => all campaigns
my $paid_visits_dfc_excluding = 0;
my $paid_ev_dfc_excluding = 0;
my @paid_pv_dfc = (0, 0);
# VisitIDS Coming Directly From Campaigns
my %vids_cdfc;
my $excluded_campaigns;
my $excluded_campaigns_enable;

$excluded_campaigns_enable = $bconf->{'*'}->{'statsmail'}->{'kpis'}->{'excluded_campaigns'};
if (defined $excluded_campaigns_enable && $excluded_campaigns_enable == 1 ) {
	$excluded_campaigns = $bconf->{'*'}->{'statsmail'}->{'paid'}->{'excluded_campaigns'};
}

my $inactivity_time_limit_in_seconds = $bconf->{'*'}->{'common'}->{'stats1x1'}->{'inactivity_time_limit_in_seconds'};

if ($stats1x1log ne '') {
	open STATS_LOG, '<', $stats1x1log;
	while (<STATS_LOG>) {
		/^.*[pixel|www_statsUxU]: .* \[([0-9]{2})\/(\S{3})\/[0-9]{4}:(.+)\] \"GET.*\.gif\?r=\d+(&new_click=1)? .*\" (\d+) \d{3} \[(.*)-\d{4}\/\d{2}\/\d{2}\+\d{2}_\d{2}_\d{2}\] ./ or next;

		my $time = "$2 $1 $3";
		$time = str2time($time);
		my $redir = decode_base64($6);
		my $no_excluded = 1;
		if (defined $excluded_campaigns_enable && $excluded_campaigns_enable == 1 && grep(/^($excluded_campaigns)/, $redir)) {
			$no_excluded = 0;
		}
		if (defined $4) {
			# if new_click add it to the array
			$vids_cdfc{$5}{$time} = [1, $no_excluded];
		} else {
			# else does not contain new_click
			$vids_cdfc{$5}{$time} = [0, $no_excluded];
		}
	}
	close STATS_LOG;
}

# The hash vids_cdfc contains information about the visitor coming directly from a paid campaign (vid),
# the time of his pageview, if the user clicked on a campaign ($vids_cdfc{$vid}{$time}[0] = 1)
# and if that pageview is NOT contained on the excluded ones ($vids_cdfc{$vid}{$time}[1] = 1)
# If is the first pageview from a tagged campaing that does not belong to an earlier and active visit (campaing or not)
#	we add a visit and a pageview
# Else if it has not passed the time of inactivity between two pageviews and does belong to an active campaing visit
# 	we add a pageview
#	Also we check if it is the second pageview in which case we add one entering visit
# Else if he has clicked on a campaign and any previous visits have closed
# 	we add a new visit and reset the pageviews per visit ($just_came)
# Else
#	we make sure that the visit is not considered from campaign (is_click = 0)
foreach my $vid (keys %vids_cdfc) {
    my $time_previous = 0;
    my $just_came = 0;
	my $is_click = 0;
	foreach my $time ( sort { $a <=> $b } keys %{%vids_cdfc->{$vid}} ) {
        if ($just_came == 0 && ${$vids_cdfc{$vid}{$time}}[0] == 1) {
            $paid_visits_dfc_excluding += $vids_cdfc{$vid}{$time}[1];
			$paid_pv_dfc[0] += ${$vids_cdfc{$vid}{$time}}[1];
			$paid_pv_dfc[1]++;
			$is_click = 1;
        } elsif (($time - $time_previous) <= $inactivity_time_limit_in_seconds && $is_click == 1) {
			$paid_pv_dfc[0] += ${$vids_cdfc{$vid}{$time}}[1];
			$paid_pv_dfc[1]++;
            if ($just_came == 1) {
            	$paid_ev_dfc_excluding += ${$vids_cdfc{$vid}{$time}}[1];
            }
        } elsif (($time - $time_previous) > $inactivity_time_limit_in_seconds && ${$vids_cdfc{$vid}{$time}}[0] == 1) {
			$just_came = 0;
			$is_click = 1;
            $paid_visits_dfc_excluding += $vids_cdfc{$vid}{$time}[1];
			$paid_pv_dfc[0] += ${$vids_cdfc{$vid}{$time}}[1];
			$paid_pv_dfc[1]++;
		} else {
			$is_click = 0;
		}
        $just_came++;
		$time_previous = $time;
    }
}

sub from_log_top5_ads_data {
	my %res = ();

	my @rows = `egrep -o '[0-9]+\\.htm' $stats1x1log | sort | uniq -c | sort -nr | head -n 5 | awk '{print \$1 "\\t" \$2}'`;
	my $i = 1;
	foreach my $row (@rows) {
		my @row = split /\t/, $row;

		$row[1] =~ s/.htm//;
		chomp $row[1];
		chomp $row[0];

		$res{"top_view_${i}_list_id"} = $row[1];
		$res{"top_view_${i}_events"} = $row[0];

		printf "ONE RESULT: %s, %s\n", $row[1], $row[0];

		$i++;
	}
	return %res;
}

my %data = ("unique_ip" => scalar keys %ips,
		"unique_ip_per_aiform" => $uip_number_per_aiform,
		"unique_ip_per_aiconfirm" => $uip_number_per_aiconfirm,
		"unique_ip_per_aipreview" => $uip_number_per_aipreview,
		"unique_ip_per_verify" => $uip_number_per_verify,
		"paid_pv_dfc" => $paid_pv_dfc[1],
		"total_lists" => $total_lists,
		"empty_lists" => $empty_lists,
		"total_searchs" => $total_searchs,
		"empty_searchs" => $empty_searchs
);

if (defined $excluded_campaigns_enable && $excluded_campaigns_enable == 1 ) {
	$data{"paid_visits_dfc_excluding"} = $paid_visits_dfc_excluding;
	$data{"paid_ev_dfc_excluding"} = $paid_ev_dfc_excluding;
	$data{"paid_pv_dfc_excluding"} = $paid_pv_dfc[0];
}


my %hourdata;
foreach my $hour (keys %hips) {
	$hourdata{"hour_unique_ip"}[$hour] = scalar keys %{$hips{$hour}};
	$hourdata{"first_visit_ip"}[$hour] = scalar keys %{$firstips{$hour}};
}

for (my $i = 0; $i < @top_5_ips; $i++) {
	my $pos = @top_5_ips - $i;

	my $hostname = `host $top_5_ips[$i] | tail -1 | awk '{print \$NF}'`;
	if ($hostname) {
		$hostname = "no PTR record" if $hostname eq "record\n";
		$hostname =~ s/\.?\n$//;
		$data{"top_${pos}_host"} = $top_5_ips[$i] . " ($hostname)";
	} else {
		$data{"top_${pos}_host"} = $top_5_ips[$i];
	}
	$data{"top_${pos}_visits"} = $top_5_visits[$i];
}

if ($stats1x1log ne '') {
	%data = (%data, from_log_top5_ads_data());
}

# getting last stat execution time
my %trans;
my $last_execution_time;
$trans{'cmd'} = 'last_stat_time';
$trans{'commit'} = 1;

my %resp = bconf_trans($$bconf{'*'}{'common'}{'transaction'}{'host'}, %trans);
if (!%resp) {
	print STDERR "(CRIT) Failed to communicate with trans.\n";
} elsif ($resp{'status'} ne "TRANS_OK") {
	print STDERR "(CRIT) trans returned status: $resp{status}\n";
}


if (!$resp{'last_stat_time.0.value'}) {
	$last_execution_time = `date +'\%Y-\%m-\%d \%H:\%M:\%S' -d '$rundate'`;
} else {
	$last_execution_time = $resp{'last_stat_time.0.value'};
}

$rundate = `date +'\%Y-\%m-\%d' -d '$rundate'`;
chomp $rundate;
my $crit = 0;
foreach my $key (sort keys %data) {
	my $val = $data{$key};
	my $hour;

	next if @ARGV && !grep $_ eq $key, @ARGV;

	print "$key => $val\n" if $debug;

	my %trans;
	$trans{'cmd'} = 'update_stats';
	$trans{'commit'} = 1;
	$trans{'stat_name'} = $key;
	$trans{'stat_value'} = $val;
	$trans{'stat_time'} = "$rundate 00:00:00";
	$trans{'last_stat_time'} = $last_execution_time;

	my %resp = bconf_trans($$bconf{'*'}{'common'}{'transaction'}{'host'}, %trans);
	if (!%resp) {
		print STDERR "(CRIT) Failed to communicate with trans.\n";
		$crit++;
	} elsif ($resp{'status'} ne "TRANS_OK") {
		print STDERR "(CRIT) trans returned status: $resp{status}\n";
		$crit++;
	}
}

foreach my $key (sort keys %hourdata) {
	my @vals = @{$hourdata{$key}};
	my $hour;

	foreach my $i (0 .. $#vals) {
		my $val = $vals[$i];

		next if !defined($val);

		next if @ARGV && !grep $_ eq $key, @ARGV;

		print "$key ($i) => $val\n" if $debug;

		my %trans;
		$trans{'cmd'} = 'update_stats';
		$trans{'commit'} = 1;
		$trans{'stat_name'} = $key;
		$trans{'stat_value'} = $val;
		$trans{'stat_time'} = sprintf('%s %02d:00:00', $rundate, $i);
		$trans{'last_stat_time'} = $last_execution_time;

		my %resp = bconf_trans($$bconf{'*'}{'common'}{'transaction'}{'host'}, %trans);
		if (!%resp) {
			print STDERR "(CRIT) Failed to communicate with trans.\n";
			$crit++;
		} elsif ($resp{'status'} ne "TRANS_OK") {
			print STDERR "(CRIT) trans returned status: $resp{status}\n";
			$crit++;
		}
	}
}
exit 1 if $from_test; # we do not want the queries to execute and overwrite our test data...

foreach my $key (sort keys %{$$bconf{'*'}{'stats'}}) {
	my $data = $$bconf{'*'}{'stats'}{$key};

	next if $$data{'just_multilang'};
	next if !$$data{'data_template'};
	next if $data{$key};

	my $max = 0;
	$max = 23 if $$data{'type'} eq 'hourly';

	for my $i (0 .. $max) {
		next if @ARGV && !grep $_ eq $key, @ARGV;

		print "$key ($i)\n" if $debug;

		my %trans;
		$trans{'cmd'} = 'update_stats';
		$trans{'commit'} = 1;
		$trans{'stat_name'} = $key;
		$trans{'stat_time'} = sprintf('%s %02d:00:00', $rundate, $i);
		$trans{'last_stat_time'} = $last_execution_time;

		my %resp = bconf_trans($$bconf{'*'}{'common'}{'transaction'}{'host'}, %trans);
		if (!%resp) {
			print STDERR "(CRIT) Failed to communicate with trans.\n";
			$crit++;
		} elsif ($resp{'status'} ne "TRANS_OK") {
			print STDERR "(CRIT) trans returned status: $resp{status}\n";
			$crit++;
		}
		$resp{'o_value'} = join(', ', @{$resp{'o_value'}}) if $debug && ref($resp{'o_value'}) eq "ARRAY";
		print "$key => $resp{o_value}\n" if $debug;
	}
}

exit 1 if $crit;
