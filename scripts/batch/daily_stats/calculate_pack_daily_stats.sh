#!/bin/bash
# usage: script.sh trans_server trans_port [days back]

TRANS_SERVER=$1
TRANS_PORT=$2
DAYSBACK=1

if [ $# -lt 2 ]
then
	echo "parameters missing!!"
	echo "usage: calculate_pack_daily_stats.sh trans_server trans_port [days back]"
	exit -1
fi

if [ $# == 3 ]
then
	DAYSBACK=$3
fi

DATE=$(date +%Y-%m-%d -d "$DAYSBACK day ago"); \
printf "cmd:get_pack_daily_stats\npack_type:cars\ndate:$DATE\ncommit:1\nend\n" | nc $TRANS_SERVER $TRANS_PORT
printf "cmd:get_pack_daily_stats\npack_type:inmorent\ndate:$DATE\ncommit:1\nend\n" | nc $TRANS_SERVER $TRANS_PORT
printf "cmd:get_pack_daily_stats\npack_type:inmosell\ndate:$DATE\ncommit:1\nend\n" | nc $TRANS_SERVER $TRANS_PORT
