#!/usr/bin/perl -w

use strict;
use Getopt::Long;
use blocket::bconf;
use Sys::Syslog;
use Fcntl qw(:flock);

require "perl_trans.pm";

my $configfile;
my $debug = 0;

sub my_log {
	my ($loglevel, $message) = @_;

	if ($debug) {
		print "($loglevel) $message\n";
	} else {
		syslog($loglevel, $message);
	}

}

# init, argv, open syslog
GetOptions("configfile=s" => \$configfile,
	   "debug" => \$debug);

openlog("generate_adcodes", 'ndelay', 'local0');
my $bconf = blocket::bconf::init_conf($configfile);


unless (flock(DATA, LOCK_EX|LOCK_NB)) {
	my_log('warn', '(WARN) generate_adcodes.pl already running!');
	print("$0 is already running. Exiting.\n") if ($debug);
	exit(1);
}

my $enabled = $bconf->{'generate_adcodes'}->{'batch'}->{'enabled'};
if (!$enabled) {
	my_log('info', 'generate_adcodes.pl disabled by bconf! (generate_adcodes.batch.enabled)');
	exit(2);
}

my %param;
$param{cmd} = "create_adcodes";
$param{commit} = "1";
$param{paylimit} = $bconf->{'generate_adcodes'}->{'paylimit'};
delete $param{paylimit} if !$param{paylimit};
$param{verifylimit} = $bconf->{'generate_adcodes'}->{'verifylimit'};
delete $param{verifylimit} if !$param{verifylimit};
$param{adwatchlimit} = $bconf->{'generate_adcodes'}->{'adwatchlimit'};
delete $param{adwatchlimit} if !$param{adwatchlimit};

# send each paid-entry to trans (redir and tradedouble handled in trans)
my %resp = bconf_trans($$bconf{'*'}{'common'}{'transaction'}{'host'}, %param);

if (!%resp) {
	my_log('crit', '(CRIT) Failed to communicate with trans');
	exit(1);
}

if (!($resp{status} eq "TRANS_OK" || $resp{status} =~ /^TRANS_ERROR/)) {
	my_log('crit', "(CRIT) trans returned status: $resp{status}");
	exit(1);
}

$resp{pay_num_generated} = 0 if !$resp{pay_num_generated};
$resp{verify_num_generated} = 0 if !$resp{verify_num_generated};
$resp{adwatch_num_generated} = 0 if !$resp{adwatch_num_generated};
my_log('info', "(INFO) $resp{pay_num_generated} paycodes, $resp{verify_num_generated} verifycodes, $resp{adwatch_num_generated} generated");
__DATA__
