#!/usr/bin/env perl

#
# webmeasure.pl
#
# Script to create stats from webmeasure log
#
# Author: Boris Cruchet Carrillo
#
# History:
#
# 2012-01-20 - Boris Cruchet Carrillo  - Begining development.
#

use POSIX qw(strftime);
use strict;
use Getopt::Long;
use Data::Dumper;
use Time::Local;
use Mail::Sendmail;
use MIME::Base64;
use MIME::Lite;

my $logfile ;
my $dst_mail = "stats\@schibsted.cl";
my $debug = 0 ;


GetOptions("l=s" => \$logfile,
        "debug" => \$debug,
        "d=s"  => \$dst_mail);

$debug = 1 if $debug;
die "I need a log file to process.\n" if (!$logfile);
die "Cannot find " . $logfile . "\n" if (! -e $logfile);


my $yesterday = strftime "%Y%m%d", localtime((time() - (24 * 60 * 60 ))) ;
#my $session_id = strftime "%Y%m%d%H%M", localtime((time() - (24 * 60 * 60 )));
#my $yesterday = strftime "%Y%m%d", localtime;

my $min_daily = 0;
my %stats;
my $file;
my $log_msg = '';

open(LOG, $logfile) or die "Unable to open logfile: $logfile\n";

while(<LOG>){

	if ($_ !~ /\b$yesterday/) {
		next;
	}

	if ($_ =~ /\bbegin transaction (\d*) for (.*)$/ ) {
		$file = $2;
	}
	elsif ($_ =~ /total serial time: (.*)/) {
		if ($stats{$file}{"serial_min"} eq "") {
			$stats{$file}{"serial_min"} = $1;
		}

		if ($stats{$file}{"serial_max"} eq "") {
			$stats{$file}{"serial_max"} = $1;
		}

		if ($stats{$file}{"serial_average"} eq "") {
			$stats{$file}{"serial_average"} = 0;
			$stats{$file}{"serial_average"} = 0;
		}

		if ( $stats{$file}{"serial_min"}  >= $1 ) {
			$stats{$file}{"serial_min"} = $1;
		}

		if ( $stats{$file}{"serial_max"} <= $1 ) {
			$stats{$file}{"serial_max"} = $1; 
		}

		$stats{$file}{"serial_average"} += $1;
		$stats{$file}{"serial_count"} += 1;

	}
	elsif ($_ =~ /total parallel time: (.*)/ ) {
		if ($stats{$file}{"parallel_min"} eq "") {
			$stats{$file}{"parallel_min"} = $1;
		}

		if ($stats{$file}{"parallel_max"} eq "") {
			$stats{$file}{"parallel_max"} = $1;
		}

		if ($stats{$file}{"parallel_average"} eq "") {
			$stats{$file}{"parallel_average"} = 0;
			$stats{$file}{"parallel_count"} = 0;
		}

		if ( $stats{$file}{"parallel_min"}  >= $1 ) {
			$stats{$file}{"parallel_min"} = $1;
		}

		if ( $stats{$file}{"parallel_max"} <= $1 ) {
			$stats{$file}{"parallel_max"} = $1; 
		}

		$stats{$file}{"parallel_average"} += $1;
		$stats{$file}{"parallel_count"} += 1;
	}
#	elsif ($_ !~ /end transaction/ ){
	elsif ($_ != /(.*) \[INFO\] $yesterday.* (http.*) (.*)$/ ) {
		#$log_msg .= $_;
		$log_msg .= "$1;$file;$2;$3\n";
	}
}
close(LOG);

my @array_keys = keys %stats;
my $msg = "<table>"
	."<tr>"
	."<th>URL</th>"
	."<th>Parallel min.</th>"
	."<th>Parallel max.</th>"
	."<th>Parallel avg.</th>"
	."<th>Serial min.</th>"
	."<th>Serial max.</th>"
	."<th>Serial avg.</th>"
	."</tr>";
for(my $i = 0; $i < @array_keys; $i++) {

	$stats{$array_keys[$i]}{"serial_average"} = $stats{$array_keys[$i]}{"serial_average"} / $stats{$array_keys[$i]}{"serial_count"};
	$stats{$array_keys[$i]}{"parallel_average"} = $stats{$array_keys[$i]}{"parallel_average"} / $stats{$array_keys[$i]}{"parallel_count"};

	$stats{$array_keys[$i]}{"serial_average"} = sprintf("%.3f", $stats{$array_keys[$i]}{"serial_average"});
	$stats{$array_keys[$i]}{"parallel_average"} = sprintf("%.3f", $stats{$array_keys[$i]}{"parallel_average"});

	$msg.= "<tr>"
	."<td>".$array_keys[$i]."</b></td>\n"
	."<td style=\"text-align: right;\">".sprintf("%.3f", $stats{$array_keys[$i]}{"parallel_min"})."</td>\n"
	."<td style=\"text-align: right;\">".sprintf("%.3f", $stats{$array_keys[$i]}{"parallel_max"})."</td>\n"
	."<td style=\"text-align: right;\">".sprintf("%.3f", $stats{$array_keys[$i]}{"parallel_average"})."</td>\n"
	."<td style=\"text-align: right;\">".sprintf("%.3f", $stats{$array_keys[$i]}{"serial_min"})."</td>\n"
	."<td style=\"text-align: right;\">".sprintf("%.3f", $stats{$array_keys[$i]}{"serial_max"})."</td>\n"
	."<td style=\"text-align: right;\">".sprintf("%.3f", $stats{$array_keys[$i]}{"serial_average"})."</td>\n"
	."</tr>\n";
}
$msg .= "</table>";

my $mail2 = MIME::Lite->new(
	From    => 'stats@yapo.cl',
	To      => $dst_mail,
	Subject => 'Yapo performance stats '.$yesterday,
	Type    => 'multipart/mixed',
);
$mail2->attach(
	Type => 'text/html',
	Data => $msg
);

$mail2->attach(
	Type     => 'text/csv',
	Filename => 'stats_'.$yesterday.'.csv',
	Data     => $log_msg,
	Disposition => 'attachment'
);

$mail2->send;

