#!/bin/bash

# Edit this
CWD="./"
cd $CWD

# only region
urls_region=(
	"/valparaiso"
	"/biobio"
	"/magallanes_antartica"
	"/region_metropolitana"
	"/chile"
)
rand=$RANDOM
index=`expr $rand % ${#urls_region[@]}`;
region=${urls_region[$index]}

# region and category
urls_region_cat=(
	"/region_metropolitana/departamentos"
	"/biobio/departamentos"
	"/chile/departamentos"
	"/region_metropolitana/autos"
	"/biobio/autos"
	"/chile/autos"
)
rand=$RANDOM
index=`expr $rand % ${#urls_region_cat[@]}`;
region_cat=${urls_region_cat[$index]}

dyn_url=`perl get_ads_url.pl -o -p $region_cat -n 1`;

./webmeasure.pl -b http://www.yapo.cl 
./webmeasure.pl -b http://www2.yapo.cl -p /ai/form/0
./webmeasure.pl -b http://www.yapo.cl  -p $region
./webmeasure.pl -b http://www.yapo.cl  -p $region_cat
./webmeasure.pl -b http://www.yapo.cl  -p $dyn_url
