#!/usr/bin/env perl
#
# webmeasure.pl
#
# Script to measure the performance of the server downloading all the data
# necessary to show a randon page of the website.
#
# Author: Pablo Alvarez de Sotomayor Posadillo
#
# History:
#
# 2012-01-17 - Pablo Alvarez de Sotomayor Posadillo - Begining development.
#

use strict;
use warnings;

# Libraries
use POSIX qw(strftime);
use HTML::LinkExtractor;
use Log::Handler;
use Time::HiRes qw(gettimeofday tv_interval);
use WWW::Curl;
use WWW::Curl::Easy;
use Data::Dumper;

# Initialization of variables
my $base_url = "http://www.yapo.cl";
my $path = "/";
my $debug = 0;
my $log_file = "./webmeasure.log";
my $log = Log::Handler->new(
	screen => {
            log_to   => "STDERR",
            maxlevel => "warning",
            minlevel => "emergency",
	}
);

# help
my $help =
    "Usage: webmeasure.pl [flags]\n" .
    "\n" .
    "-p <path>: Path to test (default is /).\n" .
    "-b <base_url>: Base url (default is http://www.yapo.cl).\n" .
    "-l <log_file>: Log file (default is ./webmeasure.log).\n" .
    "-d: Enable debug mode.\n" .
    "\n" .
    "Examples:\n" .
    "webmeasure.pl -b http://www.yapo.cl -p / -l /var/log/webmeasure.log\n" .
    "\n" .
    "Author: Pablo Alvarez de Sotomayor Posadillo<ritho\@schibstediberica.es>\n";

my $error = 0;
my $session_id = strftime "%Y%m%d%H%M", localtime;

for(my $i = 0; $i <= $#ARGV; $i++) {
    my $arg = $ARGV[$i];
    if($arg =~ s/^-//) {
        if($arg =~ m/p/ && $i < $#ARGV) {
            $path = $ARGV[++$i];
            if($path =~ m/^http[s]{0,1}:\/\//) {
                $error = 1;
            } elsif($path !~ m/^\//) {
				$path = '/' . $path;
			}
		} elsif($arg =~ m/b/ && $i < $#ARGV) {
            $base_url = $ARGV[++$i];
            if($base_url !~ m/^http[s]{0,1}:\/\//) {
                $error = 1;
            } elsif($base_url =~ m/^.*\/$/) {
				$base_url = substr $base_url, 0, -1;
			}
		} elsif($arg =~ m/l/ && $i < $#ARGV) {
            $log_file = $ARGV[++$i];
		} elsif($arg =~ m/d/) {
			$debug = 1;
        } else {
            $error = 1;
		}
    } elsif($arg =~ m/h/) {
        $error = 1;
	} else {
        $error = 1;
	}
}

# In case of error we show the help information.
if($error) {
    print $help;
    exit();
}

# Adding the log file to $log
if($debug) {
	$log->add(
		file => {
            filename => $log_file,
            maxlevel => "debug",
            minlevel => "emergency",
		}
		);
} else {
	$log->add(
		file => {
            filename => $log_file,
            maxlevel => "info",
            minlevel => "emergency",
		}
		);
}

# Function to measure the download time of a file.
sub downloadTime (@) {
	$log->debug($session_id." Downloading @_...\n");
	my $curl = WWW::Curl::Easy->new;

	$curl->setopt(CURLOPT_HEADER, 1);
	$curl->setopt(CURLOPT_FOLLOWLOCATION, 1);
	$curl->setopt(CURLOPT_URL, @_);

	# A filehandle, reference to a scalar or reference to a typeglob can be used here.
	my $response_body;
	open (my $fileb, ">", \$response_body);
	$curl->setopt(CURLOPT_WRITEDATA, \$fileb);

	# Make the request and measuring the time of request.
	my $timestamp = [gettimeofday];
	my $retcode = $curl->perform;
	my $timestamp2 = [gettimeofday];

	# Looking at the results...
	if($retcode != 0) {
		# Error code, type of error, error message
		print "An error happened: $retcode " . $curl->strerror($retcode) . " " . $curl->errbuf . "\n";
	}

	# Getting from the html file all the files to download
	my $response_code = $curl->getinfo(CURLINFO_HTTP_CODE);
	if($response_code != 200) {
		print "Response code: $response_code\n";
	}

	return tv_interval($timestamp, $timestamp2);
}

# measure de total enlapsed time of http request.
sub measureTotal(@)
{
	my (@urls) = @_;

	my %easy;
	my $curlm = WWW::Curl::Multi->new;
	my $active_handles = 0;

	for ($active_handles = 0 ; $active_handles < @urls; $active_handles++) {

		my $curl = WWW::Curl::Easy->new;
		# This should be a handle unique id.
		my $curl_id = $active_handles + 1 ;
		$easy{$curl_id} = $curl;

		$curl->setopt(CURLOPT_PRIVATE,$curl_id);
		$curl->setopt(CURLOPT_HEADER, 1);
		$curl->setopt(CURLOPT_FOLLOWLOCATION, 1);
		$curl->setopt(CURLOPT_URL, $urls[$curl_id]);

		# Add some easy handles
		$curlm->add_handle($curl);
	}

	my $timestamp = [gettimeofday];

	while ($active_handles) {
		my $active_transfers = $curlm->perform;

		if ($active_transfers != $active_handles) {
			while (my ($id, $response) = $curlm->info_read() ) {
				if ($id) {
					$active_handles--;
					my $actual_easy_handle = $easy{$id};
					# do NOTHING :)

					# letting the curl handle get garbage collected, or we leak memory.
					delete $easy{$id};
				}
			}
		}
	}
	my $timestamp2 = [gettimeofday];
	return tv_interval ($timestamp, $timestamp2);
}

$log->debug($session_id." We are working with curl $WWW::Curl::VERSION.\n");
my $curl = WWW::Curl::Easy->new;

$curl->setopt(CURLOPT_HEADER, 1);
$curl->setopt(CURLOPT_FOLLOWLOCATION, 1);
$curl->setopt(CURLOPT_URL, $base_url . $path);

# A filehandle, reference to a scalar or reference to a typeglob can be used here.
my $response_body;
open (my $fileb, ">", \$response_body);
$curl->setopt(CURLOPT_WRITEDATA, \$fileb);

# start the session
$log->info($session_id." begin transaction $session_id for $base_url$path");

# Make the request and measuring the time of request.
my $timestamp = [gettimeofday];
my $retcode = $curl->perform;
my $timestamp2 = [gettimeofday];

my %time_array = ($base_url . $path => tv_interval ($timestamp, $timestamp2));

# Looking at the results...
if($retcode != 0) {
	# Error code, type of error, error message
	print("An error happened: $retcode ".$curl->strerror($retcode)." ".$curl->errbuf."\n");
	exit();
}

# Getting from the html file all the files to download
my $response_code = $curl->getinfo(CURLINFO_HTTP_CODE);
if($response_code == 200) {
	my $extractor = new HTML::LinkExtractor();
	$extractor->parse(\$response_body);
	my $url = '';
	for my $Link(@{$extractor->links}) {
		if($$Link{tag} =~ m/img|script/) {
			if($$Link{src} !~ m/^\// && $$Link{src} && $$Link{src} !~ m/^http/) {
				$url = $base_url . "/" . $$Link{src};
			} elsif($$Link{src} =~ m/^\//) {
				$url = $base_url . $$Link{src};
			} elsif($$Link{src} =~ m/^http/) {
				$url = $$Link{src};
			}
		} elsif($$Link{tag} =~ m/link/ && $$Link{type} && $$Link{type} =~ m/css|x-icon/) {
			if($$Link{href} !~ m/^\// && $$Link{href} !~ m/^http/) {
				$url = $base_url . "/" . $$Link{href};
			} elsif($$Link{href} =~ m/^\//) {
				$url = $base_url . $$Link{href};
			} elsif($$Link{href} =~ m/^http/) {
				$url = $$Link{href};
			}
		}

		if($url && !exists($time_array{$url})) {
			$time_array{$url} = downloadTime $url;
		}
	}
}

# Print the time results
my $total_time = 0;
my @array_keys = keys %time_array;
for(my $i = 0; $i < @array_keys; $i++) {
	$log->info($session_id." ".$array_keys[$i] . " " . $time_array{$array_keys[$i]} . "\n");
	$total_time = $total_time + $time_array{$array_keys[$i]};
}

my $total_parallel_time = measureTotal(@array_keys);

$log->info($session_id." total serial time: $total_time\n");
$log->info($session_id." total parallel time: $total_parallel_time\n");
$log->info($session_id." end transaction $session_id for $base_url$path");

