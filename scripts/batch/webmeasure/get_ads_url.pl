#!/usr/bin/env perl

#
# get_ads_url.pl
#
# return the list of urls from list ads, to ad detail
#
# Author: Boris Cruchet
# base on Pablo Alvarez de Sotomayor Posadillo webmeasure.pl script
#
# History:
#
# 2012-01-24 - Boris Cruchet C. - Modify the script webmeasure.pl.
# 2012-01-17 - Pablo Alvarez de Sotomayor Posadillo - Begining development.
#

use strict;
use warnings;

# Libraries
use POSIX qw(strftime);
use HTML::LinkExtractor;
use Log::Handler;
use Time::HiRes qw(gettimeofday tv_interval);
use WWW::Curl;
use WWW::Curl::Easy;

# Initialization of variables
my $base_url = "http://www.yapo.cl";
my $path = "/";
my $debug = 0;
my $n_results = 0;
my $only_path;
my $log_file = "./get_ads_url.log";
my $log = Log::Handler->new(
	screen => {
            log_to   => "STDERR",
            maxlevel => "warning",
            minlevel => "emergency",
	}
);

# help
my $help =
	"Usage: get_ads_url.pl [flags]\n" .
	"\n" .
	"\t-p <path>: Path to test (default is /).\n" .
	"\t-b <base_url>: Base url (default is http://www.yapo.cl).\n" .
	"\t-l <log_file>: Log file (default is ./webmeasure.log).\n" .
	"\t-n <number>: Show the first number of links.\n" .
	"\t-o: show only the path in the url, not base domain.\n" .
	"\t-d: Enable debug mode.\n" .
	"\n" .
	"Examples:\n" .
	"\tget_ads_url.pl -b http://www.yapo.cl -p / -l /var/log/get_ads_url.log\n";

my $error = 0;
my $session_id = strftime "%Y%m%d%H%M", localtime;

for(my $i = 0; $i <= $#ARGV; $i++) {
	my $arg = $ARGV[$i];
	if($arg =~ s/^-//) {
		if($arg =~ m/p/ && $i < $#ARGV) {
			$path = $ARGV[++$i];
			if($path =~ m/^http[s]{0,1}:\/\//) {
				$error = 1;
			} elsif($path !~ m/^\//) {
				$path = '/' . $path;
			}
		} elsif($arg =~ m/b/ && $i < $#ARGV) {
			$base_url = $ARGV[++$i];
			if($base_url !~ m/^http[s]{0,1}:\/\//) {
				$error = 1;
			} elsif($base_url =~ m/^.*\/$/) {
				$base_url = substr $base_url, 0, -1;
			}
		} elsif($arg =~ m/l/ && $i < $#ARGV) {
			$log_file = $ARGV[++$i];
		} elsif($arg =~ m/d/) {
			$debug = 1;
		} elsif($arg eq 'o') {
			$only_path = 1;
		} elsif($arg eq 'n' && $i < $#ARGV) {
			$n_results = $ARGV[++$i];
			if ($n_results !~ m/(\d*)/) {
				$error = 1;
			}
		} else {
			$error = 1;
		}
	} elsif($arg =~ m/h/) {
		$error = 1;
	} else {
		$error = 1;
	}
}

# In case of error we show the help information.
if($error) {
    print $help;
    exit();
}

# Adding the log file to $log
if($debug) {
	$log->add(
		file => {
			filename => $log_file,
			maxlevel => "debug",
			minlevel => "emergency",
		}
	);
} else {
	$log->add(
		file => {
			filename => $log_file,
			maxlevel => "info",
			minlevel => "emergency",
		}
	);
}

$log->debug($session_id." We are working with curl $WWW::Curl::VERSION.\n");

my $curl = WWW::Curl::Easy->new;

$curl->setopt(CURLOPT_HEADER, 1);
$curl->setopt(CURLOPT_FOLLOWLOCATION, 1);
$curl->setopt(CURLOPT_URL, $base_url . $path);

# A filehandle, reference to a scalar or reference to a typeglob can be used here.
my $response_body;
open (my $fileb, ">", \$response_body);
$curl->setopt(CURLOPT_WRITEDATA, \$fileb);

# start the session
$log->info($session_id." begin transaction $session_id for $base_url$path");

# Make the request and measuring the time of request.
my $timestamp = [gettimeofday];
my $retcode = $curl->perform;
my $timestamp2 = [gettimeofday];

my %time_array = ($base_url . $path => tv_interval ($timestamp, $timestamp2));

# Looking at the results...
if($retcode != 0) {
	# Error code, type of error, error message
	print("An error happened: $retcode ".$curl->strerror($retcode)." ".$curl->errbuf."\n");
	exit();
}

# Getting from the html file all the files to download
my $response_code = $curl->getinfo(CURLINFO_HTTP_CODE);
if($response_code == 200) {
	my $extractor = new HTML::LinkExtractor();
	my $url = '';
	my $list_ids = '';
	my $i = 0;

	$extractor->parse(\$response_body);

	for my $Link(@{$extractor->links}) {
		if ($$Link{tag} eq 'a' && $$Link{href} =~ m/.*_(\d*)\.htm/) {
			# find the list_id in a $list_ids.
			# if not in, show the URL
			if ($list_ids !~ /;$1;/) {
				$list_ids .= ';'.$1.";";

				$url = $$Link{href};
				if ($only_path) {
					$url =~ s/$base_url//;
				}
				print "$url\n";

				$i++;
			}
			if ($n_results != 0 && $i >= $n_results) {
				exit();
			}

		}
	}
}
$log->info($session_id." end transaction $session_id for $base_url$path");
