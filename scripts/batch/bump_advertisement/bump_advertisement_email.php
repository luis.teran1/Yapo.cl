<?php

/*

bump_advertisement_email.php

This script takes all ads that follow the business rules and sends their
owners an email and notifications offering the possibility of
buying a Bump or delete the ad.

*/

require_once('autoload_lib.php');
require_once('init.php');
require_once('httpful.php');
require_once('JSON.php');
openlog("BUMP_ADVERTISEMENT", LOG_ODELAY, LOG_LOCAL0);

// FUNCTIONS
/**
 * waiting function prints one dot by second only on stdout
 */
function waiting($time = 20)
{
    for ($i=0; $i < $time; $i++) {
        echo ".";
        sleep(1);
    }
    echo "\n";
}
/**
 * logging send msg to syslog and stdout
 */
function logging($msg)
{
    syslog(LOG_INFO, log_string().$msg);
    echo $msg."\n";
}
// END FUNCTIONS

$account_session = new AccountSession();
$yapo_request = new Yapo\YapoRequest($account_session);

$notifications = new Yapo\Notifications();

// Get all categories from Bconf
$categories = bconf_get($BCONF, '*.cat');
$delay = array(
    "email" => 1, // Time in seconds between emails
    "category" => 30, // Seconds of delay after process category
    "not_found" => 10, // Seconds of delay when ads not found by category
);
$commitEmails = true; // Configuration to confirm emails (commit:1 or commit:0)
$summary = array(
    "catAds" => array(),  // List of categories with out ads
    "totalErr" => 0, // Counter of mails with errors
    "totalAds" => 0, // Counter of ads processed
);

// If bconf delay is defined use it
$bconfDelay = bconf_get($BCONF, '*.bump_advertisement_email.delay');
if (!empty($bconfDelay)) {
    $delay = $bconfDelay;
    logging("Overwrite delay conf");
}

// Get bconf about bump notifications
$notificationsParams = array ();
$bconfNotifications = bconf_get($BCONF, '*.Notifications.bump');
if (!empty($bconfNotifications)) {
    $notificationsParams = $bconfNotifications;
}

// Get bconf about listing url and secure url
$baseUrlSecure = bconf_get($BCONF, '*.common.base_url.secure');
$baseUrlLi = bconf_get($BCONF, '*.common.base_url.li');
// Init bTransaction
$trans = new bTransaction();

// Run for all categories (but process only leaf nodes)
foreach ($categories as $category => $info) {
    // Only process leaf categories (subcategories, not parents)
    if (!empty($info["leaf"])) {
        logging("Search ads: $category {$info['name']}");
        $trans->reset();
        $trans->add_data('category', $category);
        $result = $trans->send_command('bump_target_advertisement');

        // If this trans returns error check trans logs because probably you have a conf problem
        if ($trans->has_error(true)) {
            logging("Error on trans to get ads\n".var_export($trans->get_errors(), true));
            exit();
        }
        if (!isset($result['bump_target_advertisement'])) {
            logging("No ads found");
            $summary['catAds']["{$category} - {$info['name']}"] = 0;
            waiting($delay['not_found']);
        } else {
            $len = count($result['bump_target_advertisement']);
            logging("Ads found: {$len}");

            $summary['catAds']["{$category} - {$info['name']}"] = $len;
            // Verify if category's notifications are enabled
            $isNotificationEnabled = bconf_get($BCONF, '*.bump_notifications.'.$category.'.enabled') == '1';
            logging("Notification on $category ".($isNotificationEnabled?"enabled":"disabled"));
            // Process all ads to send email and notifications
            foreach ($result['bump_target_advertisement'] as $ad) {
                // Verify if notifications are allowed, and if the ad is valid to send a notification
                if ($isNotificationEnabled &&
                    array_key_exists('email', $ad) &&
                    array_key_exists('subject', $ad) &&
                    array_key_exists('list_id', $ad)) {
                    $key = base64_encode($notificationsParams['prefix'].'_'.$ad['email'].'_'.$category);
                    $okPath = $baseUrlLi.sprintf($notificationsParams['actionpath'], $key, 'acceptLink');
                    $okRedirect = $baseUrlSecure.sprintf($notificationsParams['ok'], $ad['list_id']);
                    $cancelPath = $baseUrlLi.sprintf($notificationsParams['actionpath'], $key, 'rejectLink');
                    $cancelRedirect = $baseUrlSecure.sprintf($notificationsParams['cancel'], $ad['list_id']);
                    $response = $notifications->execute(
                        'bump',
                        array(
                            'id' => $key,
                            'type' => $notificationsParams['type'],
                            'email' => $ad['email'],
                            'timeout' => (int)$notificationsParams['timeout'],
                            'payload' => array(
                                'adTitle' => $ad['subject'],
                                'acceptLink' => $okPath.'?redirect='.base64_encode($okRedirect),
                                'rejectLink' => $cancelPath.'?redirect='.base64_encode($cancelRedirect)
                            )
                        )
                    );
                    logging("Sending notification to {$ad['email']} response code {$response->code}");
                }
                logging("Sending mail for ad_id {$ad['ad_id']}");
                // Call to admail trans
                $trans->reset();
                $trans->add_data('mail_type', 'mail_advertisement_bump');
                $trans->add_data('ad_id', $ad['ad_id']);
                $trans->send_command('admail', $commitEmails);
                if ($trans->has_error(true)) {
                    logging("Error\n".var_export($trans->get_errors(), true));
                    $summary['totalErr'] = $summary['totalErr'] + 1;
                } else {
                    logging("Success");
                    $summary['totalAds'] = $summary['totalAds'] + 1;
                }
                waiting($delay['email']);
            }
            waiting($delay['category']);
        }
    }
}
logging("Summary: ".print_r($summary, true));
