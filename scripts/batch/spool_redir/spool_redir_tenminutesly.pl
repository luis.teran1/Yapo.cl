#!/usr/bin/perl -w
use DBI;
use POSIX qw(strftime floor);
use Getopt::Long;
use Data::Dumper;

use strict;

my $LOGDIR = '/log/redir';
my $dbhost = "10.0.0.4";
my $dbuser = "";
my $dbpassword = "";
my $dbname = "blocketdb";
my $dboptions = "";
my $rundate = "";
my $runhour = "";
my $runtenminutes = "";
my $yesterday = 0;
my $lasthour = 0;
my $lasttenminutes = 0;
my $regress = 0;
my $logfile = "";
my $log_date;

GetOptions ("dbhost=s" => \$dbhost,
            "dbuser=s" => \$dbuser,
            "dboptions=s" => \$dboptions,
            "logdir=s" => \$LOGDIR,
            "rundate=s" => \$rundate,
            "runhour=s" => \$runhour,
            "runtenminutes=s" => \$runtenminutes,
            "yesterday" => \$yesterday,
            "lasthour" => \$lasthour,
            "lasttenminutes" => \$lasttenminutes,
            "logfile=s" => \$logfile);

if (!$rundate) {
        if ($yesterday) {
                $rundate = strftime "%Y%m%d", localtime(time() - 24 * 3600);
        } else {
                $rundate = strftime "%Y%m%d", localtime;
        }
}

if (!$runhour) {
	if ($lasthour) {
		$runhour = strftime "%H", localtime(time() - 1 * 3600);
		$rundate = strftime "%Y%m%d", localtime(time() - 1 * 3600);
	} else {
		$runhour = strftime "%H", localtime;
		$rundate = strftime "%Y%m%d", localtime;
	}
}

if (!$runtenminutes) {
	if ($lasttenminutes) {
		$runtenminutes = strftime "%M", localtime(time() - 10 * 60);
		$runhour = strftime "%H", localtime(time() - 1 * 3600);
		$rundate = strftime "%Y%m%d", localtime(time() - 1 * 3600);
	} else {
		$runtenminutes = strftime "%M", localtime;
		$runhour = strftime "%H", localtime;
		$rundate = strftime "%Y%m%d", localtime;
	}
}

# trim to every ten minutes lapses
$runtenminutes = floor($runtenminutes / 10) * 10;
if ($runtenminutes == 0 ) {
	$runtenminutes = "00";
}

print "Ten-minutes portion spooled: date: $rundate hour: $runhour minute: $runtenminutes\n";

sub pgdate {
        my $syslogdate = shift;
	my $logfile_date = $logfile;
	$logfile_date =~ s/-//g;
        my $year = substr($logfile_date,0,4);
        my $month = substr($logfile_date,4,2);
        my $day = substr($logfile_date,6,2);
	$syslogdate =~ / ([0-9]+:[0-9]+:[0-9]+) .*?redir:/;
        return "$year-$month-$day $1";
}

my $dsn = "DBI:Pg:dbname=$dbname;host=$dbhost";
if ($dboptions) {
        $dsn .= ";options='$dboptions'";
}

my $dbh = DBI->connect($dsn, $dbuser, $dbpassword) || die "Failed to connect to db\n";

$dbh->{RaiseError} = 1;
$dbh->{PrintError} = 0;

my $sth_new_code_redir = $dbh->prepare("INSERT INTO redir_stats (date, code) VALUES (date_trunc('minute', ?::timestamp), ?)");
my $sth_update_redir = $dbh->prepare("UPDATE redir_stats SET first_redir = ?, repeated_redir = ?, inserted_ad = ?, paid_ad = ?, verified_ad = ?, approved_ad = ?, ad_reply = ? WHERE date = date_trunc('minute', ?::timestamp) AND code = ?") || die;

$logfile = "$rundate.log" if !$logfile;

if (!open(FILE, "$LOGDIR/$logfile")) {
        print "Failed to open $LOGDIR/$logfile\n";
        exit;
}

# Read events from logfile
print scalar localtime() . " reading $LOGDIR/$logfile\n";

my $codesh;
while (<FILE>) {
	my ($code, $type, $hour, $minute);

	if (/^... .. (..):(.).:.. \d+\.\d+\.\d+\.\d+ redir: ([^ ]+) ([^ ]+)( [^ ]+)?( [^ ]+)?/) {
		$hour = $1;
		$minute = $2 . "0";
		$code = $3;
		$type = $4;
		if ( ($hour != $runhour) || ($minute != $runtenminutes) ) {
			next;
		}
	} else {
		next;
	}

	if (!defined($codesh) || !defined($codesh->{$code})) {
		$codesh->{$code}->{'first_redir'} = 0;
		$codesh->{$code}->{'repeated_redir'} = 0;
		$codesh->{$code}->{'inserted_ad'} = 0;
		$codesh->{$code}->{'paid_ad'} = 0;
		$codesh->{$code}->{'verified_ad'} = 0;
		$codesh->{$code}->{'approved_ad'} = 0;
		$codesh->{$code}->{'ad_reply'} = 0;
	}

	if (defined($codesh->{$code}->{$type})) {
		$codesh->{$code}->{$type}++;
	}

	if (!defined($log_date)) {
		$log_date = pgdate($_);
	}
}
close(FILE);

$dbh->begin_work;
# First delete the "logdate"'s redir
# Insert "logdate"'s redir again
while (my ($code, $data) = each (%$codesh)) {
	print $code . "\n\t";
	print $data->{'first_redir'} . "\t";
	print $data->{'repeated_redir'} . "\t";
	print $data->{'inserted_ad'} . "\t";
	print $data->{'paid_ad'} . "\t";
	print $data->{'verified_ad'} . "\t";
	print $data->{'approved_ad'} . "\t";
	print $data->{'ad_reply'} . "\t";
	print "\n";
	
	if ($sth_update_redir->execute(0, 0, 0, 0, 0, 0, 0, $log_date, $code) == '0E0') {
		$sth_new_code_redir->execute($log_date, $code);
	}
	$sth_update_redir->execute($data->{'first_redir'},
				   $data->{'repeated_redir'},
				   $data->{'inserted_ad'},
				   $data->{'paid_ad'},
				   $data->{'verified_ad'},
				   $data->{'approved_ad'},
				   $data->{'ad_reply'},
			   	   $log_date,
			  	   $code);
}
$dbh->commit;
$dbh->disconnect;
print "Done reading $LOGDIR/$logfile\n";
