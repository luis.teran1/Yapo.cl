#!/usr/bin/perl -w
use DBI;
use POSIX qw(strftime);
use Getopt::Long;
use Data::Dumper;

my $LOGDIR = '/log/redir';
my $dbhost = "10.0.0.4";
my $dbuser = "";
my $dbpassword = "";
my $dbname = "blocketdb";
my $dboptions = "";
my $rundate = "";
my $yesterday = 0;
my $regress = 0;
my $logfile = "";
my $log_date;

GetOptions ("dbhost=s" => \$dbhost,
            "dbuser=s" => \$dbuser,
            "dboptions=s" => \$dboptions,
            "logdir=s" => \$LOGDIR,
            "rundate=s" => \$rundate,
            "yesterday" => \$yesterday,
            "logfile=s" => \$logfile);

if (!$rundate) {
        if ($yesterday) {
                $rundate = strftime "%Y%m%d", localtime(time() - 24 * 3600);
        } else {
                $rundate = strftime "%Y%m%d", localtime;
        }
}

sub pgdate {
        my $syslogdate = shift;
	my $logfile_date = $logfile;
	$logfile_date =~ s/-//g;
        my $year = substr($logfile_date,0,4);
        my $month = substr($logfile_date,4,2);
        my $day = substr($logfile_date,6,2);
	$syslogdate =~ / ([0-9]+:[0-9]+:[0-9]+) .*?redir:/;
        return "$year-$month-$day $1";
}

$dsn = "DBI:Pg:dbname=$dbname;host=$dbhost";
if ($dboptions) {
        $dsn .= ";options='$dboptions'";
}

my $dbh = DBI->connect($dsn, $dbuser, $dbpassword) || die "Failed to connect to db\n";

$dbh->{RaiseError} = 1;
$dbh->{PrintError} = 0;

$sth_clean_code_redir = $dbh->prepare("DELETE FROM redir_stats WHERE date = date_trunc('day', ?::timestamp) AND code = ?");
$sth_insert_redir = $dbh->prepare("INSERT INTO redir_stats (code, date, first_redir, repeated_redir, inserted_ad, paid_ad, verified_ad, approved_ad, ad_reply) VALUES (?, date_trunc('day', ?::timestamp), ?, ?, ?, ?, ?, ?, ?)") || die;

$logfile = "$rundate.log" if !$logfile;

if (!open(FILE, "$LOGDIR/$logfile")) {
        print "Failed to open $LOGDIR/$logfile\n";
        exit;
}

# Read events from logfile
print scalar localtime() . " reading $LOGDIR/$logfile\n";

while (<FILE>) {
	my ($code, $type);

	if (/redir: ([^ ]+) ([^ ]+)( [^ ]+)?( [^ ]+)?/) {
		$code = $1;
		$type = $2;
	} else {
		next;
	}

	if (!defined($codesh) || !defined($codesh->{$code})) {
		$codesh->{$code}->{'first_redir'} = 0;
		$codesh->{$code}->{'repeated_redir'} = 0;
		$codesh->{$code}->{'inserted_ad'} = 0;
		$codesh->{$code}->{'paid_ad'} = 0;
		$codesh->{$code}->{'verified_ad'} = 0;
		$codesh->{$code}->{'approved_ad'} = 0;
		$codesh->{$code}->{'ad_reply'} = 0;
	}

	if (defined($codesh->{$code}->{$type})) {
		$codesh->{$code}->{$type}++;
	}

	if (!defined($log_date)) {
		$log_date = pgdate($_);
	}
}
close(FILE);

$dbh->begin_work;
# First delete the "logdate"'s redir
# Insert "logdate"'s redir again
while (my ($code, $data) = each (%$codesh)) {
	print $code . "\n\t";
	print $data->{'first_redir'} . "\t";
	print $data->{'repeated_redir'} . "\t";
	print $data->{'inserted_ad'} . "\t";
	print $data->{'paid_ad'} . "\t";
	print $data->{'verified_ad'} . "\t";
	print $data->{'approved_ad'} . "\t";
	print $data->{'ad_reply'} . "\t";
	print "\n";
	
	$sth_clean_code_redir->execute($log_date, $code);
	$sth_clean_code_redir->finish;
	$sth_insert_redir->execute($code,
				   $log_date,
				   $data->{'first_redir'},
				   $data->{'repeated_redir'},
				   $data->{'inserted_ad'},
				   $data->{'paid_ad'},
				   $data->{'verified_ad'},
				   $data->{'approved_ad'},
				   $data->{'ad_reply'});
	$sth_insert_redir->finish;
}
$dbh->commit;
$sth_clean_code_redir = undef;
$sth_insert_redir = undef;
$dbh->disconnect;
print "Done reading $LOGDIR/$logfile\n";
