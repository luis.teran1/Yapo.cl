#!/usr/bin/perl

use strict;

use Getopt::Long;
use POSIX qw(strftime);

my $rundate;
my $yesterday;
my $logdir = '/log/ip_log';
my $debug = 0;
my $ignore;
my $minreport;
my $mailto;
my $subj;

GetOptions ('rundate=s' => \$rundate,
	    'yesterday' => \$yesterday,
	    'logdir' => \$logdir,
	    'ignore=s' => \$ignore,
	    'minreport=i' => \$minreport,
	    'debug' => \$debug,
	    'mailto=s' => \$mailto,
	    'subject=s' => \$subj);

my $ip = $ARGV[0] or die "IP required";

if (!$rundate) {
	if ($yesterday) {
		$rundate = strftime "%Y%m%d", localtime(time() - 24 * 3600);
	} else {
		$rundate = strftime "%Y%m%d", localtime;
	}
}

my $file = "$logdir/$rundate.log";

my $report = '';

sub print_acc {
	my ($account, $startspan, $endspan) = @_;
	my $es = '';

	return 0 unless $account;

	return $account if $minreport && $account < $minreport;
	
	$es = 'es' if $account > 1;

	if ($startspan ne $endspan) {
		$report .= "$account access$es between $startspan and $endspan.\n";
	} else {
		$report .= "$account accces$es at $startspan.\n";
	}
	return 0;
}

if (! -f $file && -f "$file.bz2") {
        open LOG, "bunzip2 -c $file.bz2 |" or die "Failed to open logfile";
} else {
        open LOG, '<', $file or die "Failed top open logfile";
}

(my $ipre = $ip) =~ s/\./\\./g;
my $regex = '\[[^:]*:([0-9]{2}):([0-9]{2}):([0-9]{2})[^]]*] ' . $ipre;

my $lasttime = -1000;
my $startspan = '';
my $account = 0;
my $endspan = '';
my $acctot = 0;
my $accrep = 0;
my $accign = 0;

print "Regex: $regex\n" if $debug;
while (<LOG>) {
	/$regex/ or next;

	my $time = ($1 * 60 + $2) * 60 + $3;
	my $tstr = "$1:$2";

	$acctot++;
	print "$tstr =~ $ignore ($&)\n" if $debug && $ignore && $tstr =~ /$ignore/;
	next if $ignore && $tstr =~ /$ignore/;
	$accrep++;

	if ($time < $lasttime + 120) {
		$account++;
	} else {
		$accign += print_acc($account, $startspan, $endspan);
		$account = 1;
		$startspan = $tstr;
	}
	$endspan = $tstr;
	$lasttime = $time;
}
$accign += print_acc($account, $startspan, $endspan);
$accrep -= $accign;

if ($accrep) {
	my $accigntime = $acctot - $accign - $accrep;
	$report .= "\nTotal $accrep reported accesses ($accigntime in ignored timespans).\n";
	$report .= "$accign too sparse to be reported.\n" if $accign;
}

if ($mailto) {
	if ($report) {
		$subj = "Access report $ip" if !$subj;
		open MAIL, "|/bin/mail -s '$subj' $mailto";
		print MAIL $report;
	}
} else {
	print $report;
}
