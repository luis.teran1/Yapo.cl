#!/usr/bin/perl -w

use strict;
use DBI;
use Getopt::Long;
use blocket::bconf;

my $bconffile = "";
my $pghost = "";
my $use_cat = "false";
my $categories = "0";
my $filename = "telesales.csv";
my $zipfile = "telesales.zip";
my $isregress = "";

GetOptions(
	"bconffile=s"	=> \$bconffile,
	"pghost=s"		=> \$pghost, 
	"use_cat=s"		=> \$use_cat,
	"categories=s"	=> \$categories,
	"isregress=s"	=> \$isregress
);

my $bconf = blocket::bconf::init_conf($bconffile);
my $destination_mail = $$bconf{'telesales_clients'}{'destination'};

# connect to blocketdb
my $pgdbh = DBI->connect("DBI:Pg:dbname=blocketdb;host=$pghost", $ENV{'USER'}, '')
	or die "Failed to connect to postgresql";

# search path
my $db_res = $pgdbh->do("SET search_path = bpv, public;");

# register the query
my $sth = $pgdbh->prepare("SELECT telesales_clients_fun($use_cat, '$categories')");

# execute the query
$sth->execute
	or die "failed to execute: $?\n";

# open csv file
open my $fh, ">", $filename
	or die("Could not open file. $!");

print $fh "User ID,Nombre,Mail,Telefono,Region,Categoria,Pri-Pro,Avisos Activos,Avisos Publicados,Avisos Rechazados,Borrado por vendido en Yapo,Precio Promedio Publicaciones,Fecha ultima insercion,Compro Premium,Nuevo\n";
# each line to the csv file, we remove the parenthesis first
while (my @row = $sth->fetchrow_array) {
	my $tex = join('\n', @row);
	$tex =~ m/\((.+)\)/;
	if (defined $1) {
		print $fh $1 . "\n";
	}
}

if ($isregress ne "yes") {
	# compress the file
	system("zip $zipfile $filename > /dev/null") == 0
		or die "cannot zip the file: $?";
	# send the email
	system("mailx -a \"telesales.zip\" -s \"Telesales report\" -- $destination_mail < /dev/null > /dev/null");
}

# close the file handler and db connection
close $fh;
$pgdbh->disconnect;
