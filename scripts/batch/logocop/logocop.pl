#!/usr/bin/perl -w
#
# logocop -- A simple log file cop.
#
#
# Dependencies:
#	 perl-Digest-SHA1  
#
# To-do:
#	Clean up the many arguments passed to scan_log()
#	Daily statistics.
#	Use the number of bytes hashed from the checkpoint file to fix the issue
#		with log files < hash buffer size.
#	Refactor to allow for email grouping (result of several log entries in one email)
#
use strict;
use POSIX "strftime";
use Digest::SHA1 qw(sha1_hex);
use Getopt::Long;

my $script_path = rindex($0, '/') > 0 ? substr($0, 0, rindex($0, '/')) : '.';
my $nomail = 0;

require $script_path.'/logocop.conf';

GetOptions (
	"nomail" => \$nomail
);

our %cfg;
our %log_dirs;
our %patterns;

my %symtab = ( 
	'machine_name' => `uname -n`,
);


#
# Figure out the previous log position, if any, using the checkpoint hash structure.
#
sub get_log_position($\%) {
	my $log_name = shift;
	my $checkpoint = shift;

	if (defined $checkpoint->{'old_offset'}) {
		if (defined $checkpoint->{'old_header_hash'}) {
			if($checkpoint->{'old_header_hash'} ne $checkpoint->{'header_hash'}) { return 0; }
		}
		return $checkpoint->{'old_offset'} > 0 ? $checkpoint->{'old_offset'} : 0;
	}
	return 0;
}

sub load_checkpoint($\%) {
	my $log_name = shift;
	my $checkpoint = shift;

	open(CHECKPOINT, "<".$cfg{'notify_basedir'}.$log_name.'.logcheckpoint') or return 0;

	$checkpoint->{'old_offset'} = <CHECKPOINT>;
	chomp $checkpoint->{'old_offset'};
	$checkpoint->{'old_header_hash'} = <CHECKPOINT>;
	chomp $checkpoint->{'old_header_hash'};
	if (index($checkpoint->{'old_header_hash'}, '$') > 0) {
		($checkpoint->{'old_bytes_hashed'}, $checkpoint->{'old_header_hash'}) = split(/\$/, $checkpoint->{'old_header_hash'});
	}
	$checkpoint->{'old_full_log_name'} = <CHECKPOINT>;
	if (defined $checkpoint->{'old_full_log_name'}) {
		chomp $checkpoint->{'old_full_log_name'};
	}

	close(CHECKPOINT);
}

sub save_checkpoint($\%) {
	my $log_name = shift;
	my $checkpoint = shift;

	open(CHECKPOINT, ">".$cfg{'notify_basedir'}.$log_name.'.logcheckpoint') or return 0;
	print CHECKPOINT $checkpoint->{'offset'}."\n";
	print CHECKPOINT $checkpoint->{'bytes_hashed'}.'$'.$checkpoint->{'header_hash'}."\n";
	print CHECKPOINT $checkpoint->{'full_log_name'}."\n";
	close(CHECKPOINT);
	return 1;
}

#
# Get the complete and final log file pathname given a logentry name.
#
sub get_full_log_name($) {
	my $log_name = shift;

	$log_name = $log_dirs{$log_name}->{'log'};
	$log_name = strftime($log_name, localtime());

	return $log_name;
}

sub get_plural($) {
	my $num = shift;

	return $num != 1 ? 's' : '';
}

#
# Given a string template and a symtab and a functab, substitute into the template and return it.
#
sub expand_template($\%\%)
{
	my $template = shift;
	my $symtab = shift;
	my $functab = shift;

	my $func_rx = qr/&([a-z_]+)\((.*?)\)/;

	# Expand symbols
	while ((my $sym, my $val) = each(%{$symtab})) {
		$template =~ s/\$$sym/$val/g;
	}

	# Process functions
	while ( $template =~ /$func_rx/ ) {
		if (defined $functab->{$1}) {
			my $result = $functab->{$1}(split(/,/,$2));
			$template =~ s/$func_rx/$result/;
		}
	}

	return $template;
}

#
# Get configuration option from log_entry with fallback to global cfg.
#
sub get_cfg(%$)
{
	my $log_entry = shift;
	my $param = shift;

	if (defined $log_entry->{$param}) {
		return $log_entry->{$param};
	} elsif (defined $cfg{$param}) {
		return $cfg{$param};
	}
	return undef;
}

sub send_email($\%\@\%) {
	my $full_log_name = shift;
	my $log_entry = shift;
	my $lines = shift;
	my $symtab = shift;

	if($nomail) {
		return 1;
	}

	my $sendmail = get_cfg($log_entry, 'sendmail'); 
	if (!defined $sendmail) { return 0; }

	my $rcpt = get_cfg($log_entry, 'email');
	if (!defined $rcpt) { $rcpt = 'root'; }

	my $template = get_cfg($log_entry, 'subject');
	if (!defined $template) {
		$template = 'ALARM: $lines_matched error&get_plural($lines_matched) in $log_name at $machine_name';
	}

	my %functab = (
		'get_plural' => \&get_plural,
	);

	my $subject = expand_template($template, %{$symtab}, %functab);

	my $content = "File ".$full_log_name." contains the following errors:\n\n";

	$content .= join("\n", @{$lines});

	if ($symtab->{'lines_matched'} > $symtab->{'max_lines_reported'}) {
		$content .= "\n[...]\n\n(WARNING: Only the first ".$symtab->{'max_lines_reported'}." line".get_plural($symtab->{'max_lines_reported'})." of ".$symtab->{'lines_matched'}." are reported here)\n";
	}	

	chomp $subject;
	open(SENDMAIL, "|".$sendmail) or die "Cannot open ".$sendmail.": $!";
	print SENDMAIL "From: Logocop <logocop\@blocket.se>\n";
	print SENDMAIL "Reply-to: noreply\@blocket.se\n";
	print SENDMAIL "Subject: ".$subject."\n";
	print SENDMAIL "To: ".$rcpt."\n";
	print SENDMAIL "Content-type: text/plain\n\n";
	print SENDMAIL $content."\n";
	close(SENDMAIL);

	return 1; 
}

#
# Goes through all the log entries and merges the patterns referenced into
# one pattern array.
#
sub merge_patterns() {

	while( (my $log_name, my $log_entry) = each(%log_dirs)) {
		my @merged = ();
		foreach my $pattern_name (@{$log_entry->{'pattern_names'}}) {
			@merged = (@merged, @{$patterns{$pattern_name}});
		}
		$log_entry->{'patterns'} = \@merged;
	}

}

#
# Scan the specified file use the settings of log_entry
#
sub scan_log($$\%\%\%$)
{
	my ($log_name, $full_log_name, $log_entry, $symtab, $checkpoint, $no_load_offset) = @_;
	my $header_buffer;
	
	open(LOG, $full_log_name) or return 0;

	$symtab->{"lines_matched"} = 0;
	$symtab->{"log_name"} = $log_name;
	$symtab->{"max_lines_reported"} = defined get_cfg($log_entry, 'max_lines_reported') ? get_cfg($log_entry, 'max_lines_reported') : 40;

	# We use a hash of the first block of the file to detect log rotation / log truncations / etc
	$checkpoint->{'bytes_hashed'} = read(LOG, $header_buffer, 128);
	$checkpoint->{'header_hash'} = sha1_hex($header_buffer);
	$checkpoint->{'full_log_name'} = $full_log_name;

	if (!$no_load_offset) {
		my $last_position = get_log_position($log_name, %{$checkpoint});
		seek(LOG, $last_position, 0);
	}

	my @lines_matched;
	my $pattern_2 = '';
	while(my $line = <LOG>) {
		chomp $line;
		if ($line eq '') { next; }
		foreach my $pattern (@{$log_entry->{'patterns'}}) {
			if ($line =~ /$pattern/ || ($pattern_2 ne '' && $line =~ /$pattern_2/)) {
				if ($symtab->{'lines_matched'}++ < $symtab->{'max_lines_reported'}) {
					push(@lines_matched, $line);
				}
			
				# Handle more lines for the SQL log
				if ($log_name eq 'pgsql' && $pattern_2 eq '') {
					my $pgsql_pattern = get_cfg($log_entry, 'pgsql_pattern');
					($pattern_2) = ($line =~ /$pgsql_pattern/);
				}
				
				last; # Don't report multiple hits in a single line.
			} else {
				$pattern_2 = '';
			}
		}

	}

	$checkpoint->{'offset'} = tell(LOG);
	close(LOG);

	save_checkpoint($log_name, %{$checkpoint});

	if ($symtab->{'lines_matched'} > 0) {
		send_email($full_log_name, %{$log_entry}, @lines_matched, %{$symtab});
	}

}

#########################################################################
# main									#
#########################################################################

merge_patterns();

while( (my $log_name, my $log_entry) = each(%log_dirs)) {

	my %checkpoint;
	my $full_log_name = get_full_log_name($log_name);
	my $no_load_offset = 0;
	load_checkpoint($log_name, %checkpoint);
	if (defined $checkpoint{'old_full_log_name'} && $checkpoint{'old_full_log_name'} ne $full_log_name) {
		scan_log($log_name, $checkpoint{'old_full_log_name'}, %{$log_entry}, %symtab, %checkpoint, $no_load_offset);
		$no_load_offset = 1;
	}
	scan_log($log_name, $full_log_name, %{$log_entry}, %symtab, %checkpoint, $no_load_offset);
}


