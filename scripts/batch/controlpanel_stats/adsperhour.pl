#!/usr/bin/perl -w

use blocket::bconf;
use RRDs;
use Getopt::Long;
use Socket;
use Sys::Hostname;
require "perl_trans.pm";

my $BDIR;
if (defined($ENV{'BDIR'})) {
	$BDIR = $ENV{BDIR};
} else {
	$BDIR='/opt/blocket';
}

my $pngfile="$BDIR/www-ssl/cpstats/adsperhour.png";
my $rrdfile="$BDIR/var/adsperhour.rrd";
my $now = time();

if (!stat($rrdfile)) {
	RRDs::create($rrdfile, "--start", $now - 300,
		"DS:clearsphly:GAUGE:300:U:U",
		"DS:newadsphly:GAUGE:300:U:U",
		"DS:clearsph:GAUGE:300:U:U",
		"DS:newadsph:GAUGE:300:U:U",
		"RRA:AVERAGE:0.5:1:288");
}

sub fetch_data {
	my $addr = inet_ntoa(scalar(gethostbyname(hostname())) || 'localhost');

	my $configfile = "/opt/blocket/conf/bconf.conf";
	GetOptions("configfile=s" => \$configfile);

	my $bconf = blocket::bconf::init_conf($configfile);
	if (!$bconf->{'*'}->{'common'}->{'transaction'}->{'host'}->{'1'}->{'name'}) {
        	die "Failed to read bconf from trans.";
	}

	my %param;
	$param{cmd} = "websql";
        $param{commit} = "1";
        $param{remote_addr} = $addr;
	$param{query} = "31";
	$param{internal} = "1";
        
        my %resp = bconf_trans($$bconf{'*'}{'common'}{'transaction'}{'host'}, %param);

        if (!%resp) {
		return("N:0:0:0:0");
        }

	#XXX parse trans result

	my $r = 0;
	my $retval = "N";
	while (defined($resp{"result.$r.count"})) {
		$retval .= ":" . $resp{"result.$r.count"};
		$r++;
	}
	while ($r < 4) {
		$retval .= ":0";
		$r++;
	}

	return ($retval);
}

my $RRDerr;
RRDs::update("$rrdfile", "--template", "clearsphly:newadsphly:clearsph:newadsph",fetch_data());
$RRDerr = RRDs::error();
die "ERROR while updating $rrdfile: $RRDerr\n" if $RRDerr;

RRDs::graph("$pngfile", "--start", $now - 86400, "--end", $now, "-w", 300, "-h", 80,
		"-c", "BACK#FFFFEB", "-c", "SHADEA#FFFFEB", "-c", "SHADEB#FFFFEB", "-L", 6,
		"-v", "Ads influx/h", "-a", "PNG",
		"DEF:adsph=$rrdfile:newadsph:AVERAGE",
		"DEF:paidph=$rrdfile:clearsph:AVERAGE",
		"DEF:adsphly=$rrdfile:newadsphly:AVERAGE",
		"DEF:paidphly=$rrdfile:clearsphly:AVERAGE",
		'AREA:paidph#00AF00:Paid+Verified',
		'LINE2:adsph#FF0000:New',
		'AREA:paidphly#80FF80:P+V before',
		'LINE1:adsphly#FF8080:New before\g');
$RRDerr = RRDs::error();
die "ERROR while graphing $pngfile: $RRDerr\n" if $RRDerr;
exit(0);
