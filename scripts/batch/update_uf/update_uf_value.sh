#!/bin/bash
# Argument list = -h trans_host -p trans_port 

usage() {
cat << EOF
usage: $0 options
Gets more recent value of UF and updates conf accordingly
uses www.mindicador.cl as default and is build around it

OPTIONS:
	-h	trans_host
	-p	trans_port
	-f	feed url
	-t	if set, write mail to the specified text file instead
	-u	usage
EOF
}

TRANS_HOST=
TRANS_PORT=
URL='https://si3.bcentral.cl/Indicadoressiete/secure/Indicadoresdiarios.aspx'
TO_TEXT=false

while getopts "h:p:f:t:u" OPTION
do
     case $OPTION in
         u)
             usage
             exit 1
             ;;
         h)
             TRANS_HOST=$OPTARG
             ;;
         p)
             TRANS_PORT=$OPTARG
             ;;
         f)
             URL=$OPTARG
             ;;
         t)
             TO_TEXT=$OPTARG
             ;;
         ?)
             usage
             exit
             ;;
     esac
done

if [[ -z $TRANS_HOST ]] || [[ -z $TRANS_PORT ]]
then
     usage
     exit 1
fi

function send_email() {
	ADDRESSES=$(bconf_get "*.*.common.uf_conversion_error_addr")

	if [ $TO_TEXT != false ]
	then
		echo Subject: $1 > $TO_TEXT
		echo To: $ADDRESSES >> $TO_TEXT
		echo $2 >> $TO_TEXT
	else
		printf "$2" | mail -s "$1" $ADDRESSES 
	fi
}

# update a bconf value
function set_dynamic_bconf() {
	KEY=$1
	VALUE=$2
	printf "cmd:setconf\nbatch:1\nremote_addr:127.0.0.1\nset:$KEY=$VALUE\ncommit:1\nend\n" | nc $TRANS_HOST $TRANS_PORT > /dev/null
}

# Gets one bconf entry
function bconf_get() {
	KEY="$1"
	VALUE=$(printf "cmd:bconf\ncommit:1\nend\n" | nc $TRANS_HOST $TRANS_PORT | grep "conf:$KEY=" | sed -e 's/conf://g')
	VALUE=$(echo $VALUE | sed -e 's/.*=//g')
	echo $VALUE
}

# We have unformated values:
#   * UF do not containt dots
#   * Dates do not have parenthesis and do not use slashes
DATE=$(date +"%F %R:%S.%N")
UF=($(echo $URL | xargs curl -s | tr -d '\n' | sed 's/.*lblValor1_1.*\([0-9]\{2\}\.[0-9]\{3\},[0-9]\{2\}\).*lblSerie1_2.*/\1/g' | sed 's/\.//g'))

if [ -z "$UF" ]; then
	send_email "UF Autoupdate: Problem fetching URL" "There was a problem fetch the UF value from $URL, UF value is empty."
	exit 0
fi

# Validate UF value
if [ -z $(echo $UF | grep -Eq '^[-+]?[0-9]+\,?[0-9]*$' && echo is_number) ]; then
	send_email "UF Autoupdate: Problem UF value" "There was a problem. UF value is not a number, UF: $UF"
	exit 0
fi

# Update bconfs
set_dynamic_bconf "*.*.common.uf_conversion_factor" $UF
send_email "UF Autoupdate: no incidences" "There UF has been changed correctly. The new UF value is $UF."
set_dynamic_bconf "*.*.common.uf_conversion_updated" $DATE

exit 0
