#!/bin/bash

# Trans variables.
TRANSHOST=
TRANSPORT=


# Describes the usage of the script
function usage() {
	local scriptname=`basename "$0"`
	echo "usage: ${scriptname} -h TRANS HOST -p TRANS PORT "
	echo "DESCRIPTION:"
	echo " -h"
	echo "	Trans hostname."
	echo " -p"
	echo "	Trans port."
	echo ""
	exit 0	
}

# Calls the flush_queue for the queue enqueue with the given trans host and trans port
function flush_queue() {
	local host=$1
	local port=$2
	local offset=$((60 * $(date +%:::z | cut -d 0 -f 2) + 1))
	local maxtime=$(date -d "${offset} minutes ago" +"%s")
	local command="cmd:flushqueue\nqueue:enqueue\nmaxtime:${maxtime}\ncommit:1\nend\n"
	local output=$(printf ${command} | nc ${host} ${port} | grep "status:TRANS_OK" | grep -c -v "error")
	if [[ $output -eq 1 ]]; then
		echo "OK"
	else
		echo "ERROR FLUSHING enqueue"
	fi
}


while getopts ":h:p:" opt; do
	case "${opt}" in
		h)
			if [[ -n "$OPTARG" ]]; then
				TRANSHOST=$OPTARG
			else
				echo "Invalid trans host"
				usage
			fi
			;;
		p)
			if [[ $OPTARG =~ ^[0-9]+$ ]]; then
				TRANSPORT=$OPTARG
			else
				echo "Invalid trans port"
				usage
			fi
			;;
		\?)
			usage
			;;
	esac
done
shift $((OPTIND-1))

[ "${1:-}" = "--" ] && shift

if [[ -z "$TRANSHOST" || -z "$TRANSPORT" ]]; then
	usage
fi

flush_queue ${TRANSHOST} ${TRANSPORT}
