#!/bin/bash

# Scripts checks possible makefiles for given file and
# genereates an install_path and then checks which rpm
# the install_path matches
#
# XXX: Doesn't work with .h files 
# XXX: Doesn't work with lib/common .[ch] files

DEBUG=0

debug () {
	if [ "$DEBUG" -eq 1 ] ; then
		echo $1
	fi
}

usage () {
	echo "Usage: $0 st <optional:rpm>"
	echo "	return all rpms needed for current svn rev"
	echo ""
	echo "Usage: $0 <file> <optional:rpm>"
	echo "	returns rpm needed for given file (abs path or filename only)"
	echo ""
	echo "Usage: $0 <revision_number> <optional:rpm>"
	echo "	returns rpm needed for given svn revision"
	exit 1
}

if [ -z $1 ] ; then
	usage
fi

if [ ! -z $2 ] ; then
	RPM=$2
	printed_rpms=""
fi

find_rpm () {
	local file_arg=$1
	local mk=$2
	local org_file=$3
	local file=`basename $file_arg`

	local TOPDIR=.
	local incmk=$(eval echo `grep '^include ' $mk | grep -v mk/ | cut -b 9-`)

	# Check for install_dir
	install_dir=`grep INSTALLDIR $mk $incmk | cut -d '=' -f 2`

	for dir in $install_dir ; do
		# Remove last slash
		dir=`echo $dir | sed -e "s/\(.*\)\/$/\\1/"`

		# Found install dir
		rpm_file=`echo $dir/$file`

		# line nr for the file
		rpm_file_line=`grep -n $rpm_file"$" rpm/blocket.spec | cut -d ':' -f 1`

		# For all occurences in blocket.spec, check the above package
		for line in $rpm_file_line ; do
			rpm_package_name=`grep -n "%files " rpm/blocket.spec | awk "BEGIN {FS=\":\"} { if (\\$1 < $line) print \\$2 }" | cut -d ' '  -f 2 | tail -n1`
			if [ -z "$RPM" ] ; then
				printf "%-30s %-30s %-40s " blocket-$rpm_package_name $dir $mk
				if [ ! -z "$org_file" ] ; then
					printf "%-40s (%s)\n"  $org_file $file_arg
				else
					printf "%-40s\n"  $file_arg
				fi
			elif [ `awk "BEGIN{print(match(\"$printed_rpms\", \"(^| )$rpm_package_name( |$)\"))}"` -eq 0 ] ; then
				printed_rpms="$printed_rpms $rpm_package_name"
				printf "%s\n" blocket-$rpm_package_name
			fi
		done
	done
}

find_src () {
	local file_arg=$1
	local mkfiles=$2
	local org_file=$3
	local file=`basename $file_arg`
	local path=`dirname $file_arg`

	tmpl_pos=`expr match $file '.*.tmpl$'`
	# Check if file is template
	if [ $tmpl_pos -gt 0 ] ; then 
		template_dir_pos=`expr match $path ".*templates/"`
		template_dir=`echo ${path:$template_dir_pos} | cut -d '/' -f 1`
		
		for mk in `find . -name 'Makefile' -print -or -name local*.mk -print -or -name build -prune -or -name .svn -prune -or -name regress_final -prune | xargs egrep -l "^USE_TEMPLATES.?=.*\<$template_dir\>.*" 2> /dev/null` ; do
			is_lib=`egrep "^LIB[ ]?=" $mk | sed -e 's/.*=\s*\(.*\)\s*/\1/g'`
			if [ ${#is_lib} -gt 0 ] ; then
				find_src $is_lib.so $mk $file_arg
			else 
				is_prog=`egrep "^PROG[ ]?=" $mk | sed -e 's/.*=\s*\(.*\)\s*/\1/g'`
				if [ ${#is_prog} -gt 0 ] ; then
					find_src $is_prog $mk
				fi
			fi
		done
		return
	fi

	# Search for makefiles
	if [ ${#mkfiles} -eq 0 ] ; then
		mkfiles=`find $path -name Makefile -print -or -name local*.mk -print -or -name .svn -prune`

		# Check if mkfiles found
		while [ -z "$mkfiles" ] ; do
			# if empty exit 1
			if [ -z $path ] || [ "$path" == "." ] ; then
				return 1;
			fi

			# Get topdir
			path=`dirname $path`

			# Find make files
			mkfiles=`find $path -name Makefile -print -or -name local*.mk -print -or -name .svn -prune`
		done
	fi

	# Check all makefiles
	for mk in $mkfiles ; do
		# Just to keepin it clean and if in the future we get more cases its good to seperate them
		check_srcs=false
		check_rpm=true

		# Check if file exists as prog or install_target in makefile
		if [ -z "`egrep "(PROG|INSTALL_TARGETS|IN_FILES).*\<$file(\.in)?\>" $mk`" ] ; then
			# Check if the target is a LIB
			lib_pos=`expr match $file "[^.]*"`
			if [ $lib_pos -gt 0 ] ; then 
				lib_file="${file:0:$lib_pos}"
				if [ -z "`egrep "(LIB).*\<$lib_file(\.in)?\>" $mk`" ] ; then
					check_srcs=true
					check_rpm=false
				fi
			else
				check_srcs=true
				check_rpm=false
			fi
		fi

		# Check if file is source
		if [ check_srcs ] ; then
			if [ -n "`svn info $file_arg 2>/dev/null`" ] ; then
				if [ -n "`egrep \"^SRCS[ ]?[+]?=.*\<$file\>.*\" $mk 2> /dev/null`" ] ; then 
					is_lib=`egrep "^LIB[ ]?=" $mk | sed -e 's/.*=\s*\(.*\)\s*/\1/g'`
					if [ ${#is_lib} -gt 0 ] ; then
						find_src $is_lib.so $mk $file_arg
					else 
						is_prog=`egrep "^PROG[ ]?=" $mk | sed -e 's/.*=\s*\(.*\)\s*/\1/g'`
						if [ ${#is_prog} -gt 0 ] ; then
							if [ ! -z "$org_file" ] ; then
								find_rpm $is_prog $mk $org_file 
							else
								find_rpm $is_prog $mk $file_arg 
							fi
						else
							echo "Unknown: $file"
						fi
					fi
				fi
			fi
		fi

		# Find the rpm
		if [ check_rpm ] ; then
			find_rpm $file $mk $org_file 
		fi
	done

	return 0;
}

find_by_revision () {
	local svn_revision=$1
	local svn_path=`svn info  | grep URL | awk '{print $2}'`
	local src_pos=`expr match $svn_path '.*/src/'`
	local svn_dir=`echo /src/${svn_path:src_pos}/`
	local files=`svn log -v -r $svn_revision | fgrep "/src/" | egrep -v "D " | awk '{print $2}' | sort | uniq`
	for file in $files ; do
		path_pos=`expr match $file $svn_dir`
		file=${file:path_pos}
		find_src $file
	done
}

find_by_array () {
	for file in $@ ; do
		find_src $file
	done
}

# Possible locations
if [ -f $1 ] ;  then 
	# Argument is file
	debug "Argument is a file: $1"
	find_src $1;
elif [ "st" == "$1" ] ; then
	# Check for rpms for current changes
	debug "Check svn st: $2"
	files=`svn -q st | egrep -v "(D) "  | awk '{print $2}' | sort | uniq`
	if [ -z "$files" ] ; then
		find_by_revision `svn info | grep "Last Changed Rev" | awk '{print $4}'`
	else
		find_by_array $files
	fi
elif [ ! -z $1 ] ; then
	svn_log=`svn log -r $1 2>/dev/null`
	if [ ! -z "$svn_log" ] ; then
		debug "Check svn revision: $1"
		find_by_revision $1
	else
		# Argument seems to be a filename without a path
		files=`find . -name $1 -type f -print -or -name build -prune -or -name regress_final -prune -or -name .svn -prune -or -name dest -prune`
		if [ ${#files} -eq 0 ] ; then
			echo "Can't locate file: <$1>"
		else
			debug "Argument is file: <$1>, Possible locations: $files"
			find_by_array $files
		fi
	fi
else
	echo "Don't know what to do"
fi
