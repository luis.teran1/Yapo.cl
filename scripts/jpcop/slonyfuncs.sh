#!/bin/bash

if [ -f /jopocop/etc/hostconfig ] ; then
	. /jopocop/etc/hostconfig
else
	echo -n "Enter master database hostname: "
	read -e PGMASTER_HOST
	echo -n "Enter slave db hostname (or hostnames separated by space): "
	read -e PGSLAVE_HOST
fi

PGSQLDIR=/opt/pgsql
RPMDIR=/var/tmp/jpc/rpms
HOSTIP=`/sbin/ifconfig eth0 | sed -n 's/.*inet addr:\([^\ ]*\).*/\1/p'`
DBNAME=blocketdb
CLUSTERNAME=`slonik_init_cluster | sed -n '/cluster name/s/.*=[ \t]*\([^ \t;][^ \t;]*\).*/\1/p'`
MASTERNODE=`sed -n 's/.*\$MASTERNODE.*=[ \t]*\([^ \t;][^ \t;]*\).*/\1/p' /etc/slon_tools.conf`
SLAVENODES=`sed -n '/^[ \t]*add_node/s/.*=> \([^ \t,][^ \t,]*\).*/\1/p' /etc/slon_tools.conf | grep -v "^$MASTERNODE$"`

log_error () {
	echo "ERROR: $*"
}

as_postgres () {
	if [ $USER = "postgres" ]; then
                $*
        else
                su - postgres sh -c "$*"
        fi
}

run_sql () {
	echo "$*" > /tmp/run_sql.$$

	if [ $USER = "postgres" ]; then
		psql $DBNAME -At --file /tmp/run_sql.$$
	else
		as_postgres "psql $DBNAME -At --file /tmp/run_sql.$$"
	fi

	rm -f /tmp/run_sql.$$

	return $?
}

run_slonik () {
	as_postgres $* | as_postgres slonik 2>&1
}

get_conninfo () {
        echo "cluster name = $CLUSTERNAME;"
	as_postgres "slonik_init_cluster" | grep "admin conninfo"
}

get_temporary_set() {
	highest_set_id=`run_sql "select set_id from _$CLUSTERNAME.sl_set ORDER BY set_id DESC limit 1"`

        if [ $highest_set_id -lt 90 ]; then
		echo 90
	else
		echo $(($highest_set_id + 1))
	fi
}

set_from_table_name () {
        typeset fqtn=$1
        typeset nspname=${fqtn%.*}
	run_sql "select tab_set from _$CLUSTERNAME.sl_table WHERE tab_nspname = '$nspname' limit 1"
}

set_from_seq_name () {
	run_sql "select seq_set from _$CLUSTERNAME.sl_sequence WHERE seq_relname = '$1' limit 1"
}

# Run a SQL query on all DB nodes, without locking/removing triggers.
psql_run_query () {
        test -z "$1" && log_error "SQL query missing" && return
	
	# Run query on all nodes
	echo "$1" | as_postgres psql `run_sql "SELECT no_comment FROM _$CLUSTERNAME.sl_node WHERE no_id = $MASTERNODE and no_active;"|sed -e's:^Node .* - \(.*\)@\(.*\):\1 -h \2:'`
	for slave in $SLAVENODES; do
	    echo "$1" | as_postgres psql `run_sql "SELECT no_comment FROM _$CLUSTERNAME.sl_node WHERE no_id = $slave and no_active;"|sed -e's:^Node .* - \(.*\)@\(.*\):\1 -h \2:'`
	done
}

#######################################################
# $1 - table name
# $2 - full sql query to alter the table in table $1
#######################################################

psql_alter_table () {
	test -z "$1" && log_error "Table name missing" && return
        test -z "$2" && log_error "SQL query missing" && return
        set_number=$(set_from_table_name $1)
	test -z "$set_number" && set_number=1
	echo "$2" > /tmp/jpcsql_at.$$
        run_slonik "slonik_execute_script $set_number /tmp/jpcsql_at.$$"
	rm -f /tmp/jpcsql_at.$$
}

psql_replicate_table () {
	test -z "$1" && log_error "Table name missing" && return
        set_number=$(set_from_table_name $1)
	test -z "$set_number" && return
        temporary_set=$(get_temporary_set)
        get_conninfo > /tmp/slonik_header.$$
	next_t_id=$(run_sql "select max(tab_id) + 1 from _$CLUSTERNAME.sl_table")
	test -z "$next_t_id" && log_error "Failed finding next table id for replication" && return
	cat << EOT > /tmp/slonik_body.$$
	create set (id=$temporary_set, origin=$MASTERNODE, comment='psql_adt_tempset');
	wait for event (origin=ALL, confirmed=ALL, wait on = $MASTERNODE);
	set add table (set id=$temporary_set, origin=$MASTERNODE, id=$next_t_id, fully qualified name = '$1', comment='');
	wait for event (origin=ALL, confirmed=ALL, wait on = $MASTERNODE);
EOT
	typeset slave
	for slave in $SLAVENODES; do
		echo "subscribe set (id = $temporary_set, provider = $MASTERNODE, receiver = $slave, forward = no);"
		echo "wait for event (origin=ALL, confirmed=ALL, wait on = $MASTERNODE);"
	done >> /tmp/slonik_body.$$
	run_slonik "cat /tmp/slonik_header.$$ /tmp/slonik_body.$$"
	while [ `run_sql "select count(*) from _$CLUSTERNAME.sl_event where ev_type = 'ENABLE_SUBSCRIPTION' and ev_data1 = '$temporary_set' and ev_seqno > ( select max(con_seqno) from _$CLUSTERNAME.sl_confirm where con_origin = $MASTERNODE and con_received = $slave)"` -gt 0 ]; do
		sleep 1
	done
	run_slonik "slonik_merge_sets $MASTERNODE $set_number $temporary_set"
	rm /tmp/slonik_header.$$ /tmp/slonik_body.$$
}

psql_replicate_sequence () {
	test -z "$1" && log_error "Sequence name missing" && return
	set_number=$2
	test -z "$set_number" && set_number=$(set_from_seq_name $1)
	test -z "$set_number" && set_number=1
        temporary_set=$(get_temporary_set)
	next_s_id=$(run_sql "select max(seq_id) + 1 from _$CLUSTERNAME.sl_sequence")
	test -z "$next_s_id" && log_error "Failed finding next sequence id for replication" && return
        get_conninfo > /tmp/slonik_header.$$
	cat << EOT > /tmp/slonik_body.$$
	create set (id=$temporary_set, origin=$MASTERNODE, comment='psql_ads_tempset');
	wait for event (origin=ALL, confirmed=ALL, wait on = $MASTERNODE);
	set add sequence (set id=$temporary_set, origin=$MASTERNODE, id=$next_s_id, fully qualified name = '$1', comment='');
	wait for event (origin=ALL, confirmed=ALL, wait on = $MASTERNODE);
EOT
        typeset slave
        for slave in $SLAVENODES; do
                echo "subscribe set (id = $temporary_set, provider = $MASTERNODE, receiver = $slave, forward = no);"
                echo "wait for event (origin=ALL, confirmed=ALL, wait on = $MASTERNODE);"
        done >> /tmp/slonik_body.$$
	run_slonik "cat /tmp/slonik_header.$$ /tmp/slonik_body.$$"
	# wait here a little so slon can catch up
	while [ `run_sql "select count(*) from _$CLUSTERNAME.sl_event where ev_type = 'ENABLE_SUBSCRIPTION' and ev_data1 = '$temporary_set' and ev_seqno > ( select max(con_seqno) from _$CLUSTERNAME.sl_confirm where con_origin = $MASTERNODE and con_received = $slave)"` -gt 0 ]; do
		sleep 1
	done
	run_slonik "slonik_merge_sets $MASTERNODE $set_number $temporary_set"
	rm -f /tmp/slonik_header.$$ /tmp/slonik_body.$$
}

psql_add_table () {
	test -z "$1" && log_error "Table name missing" && return
        test -z "$2" && log_error "SQL query missing" && return
	psql_run_query "$2"
	psql_replicate_table $1
	rm -f /tmp/jpcsql_adt.$$
}

psql_add_sequence () {
	test -z "$1" && log_error "Sequence name missing" && return
        test -z "$2" && log_error "SQL query missing" && return
	set_number=$3
	test -z "$set_number" && set_number=1
	psql_run_query "$2"
	psql_replicate_sequence $1 $set_number
	rm -f /tmp/jpcsql_ads.$$
}

psql_drop_table () {
	test -z "$1" && log_error "Table name missing" && return
        test -z "$2" && log_error "SQL query missing" && return
	set_number=$(set_from_table_name $1)
	test -z "$set_number" && return
	echo "$2" > /tmp/jpcsql_drt.$$
	nspname=`echo $1 | sed -n 's/\([^ \t]\)\..*/\1/p'`
	relname=`echo $1 | sed -n 's/.*\.\([^ \t]\)/\1/p'`
	t_id=$(run_sql "select tab_id from _$CLUSTERNAME.sl_table where tab_nspname = '$nspname' AND tab_relname = '$relname'")
	test -z "$t_id" && log_error "Failed finding table id for replication" && return
	get_conninfo > /tmp/slonik_header.$$
	cat << EOT > /tmp/slonik_body.$$
	set drop table (origin=$MASTERNODE, id=$t_id);
	wait for event (origin = $MASTERNODE, confirmed = ALL, wait on = $MASTERNODE);
EOT
	run_slonik "cat /tmp/slonik_header.$$ /tmp/slonik_body.$$"
	run_slonik "slonik_execute_script $set_number /tmp/jpcsql_drt.$$"
	rm -f /tmp/jpcsql_drt.$$ /tmp/slonik_header.$$ /tmp/slonik_body.$$
}

psql_table_exists () {
	test -z "$1" && log_error "Table name missing" && return
	run_sql "\d $1" 1>/dev/null 2>&1
	return $?
}

psql_table_wait () {
        echo -n "Waiting for table $1..."
        while ! psql_table_exists $1
        do
                sleep 1
                echo -n .
        done
        echo
}

schemasort () {
	tr '\012' '@' | tr ';' '\012' | sort | tr '\012' ';' | tr '@' '\012'
}

check_replication () {
	retstat=0
	for h in $PGMASTER_HOST $PGSLAVE_HOST ; do
		as_postgres "pg_dump -s -n public --no-acl -h $h $DBNAME" | egrep -v "EXECUTE|TRIGGER|_$CLUSTERNAME|BEFORE|AFTER|FOR EACH ROW|^$|^--|START WITH" | schemasort > /tmp/public.schema.$h
	done
	if diff --brief /tmp/public.schema.* ; then
		:
	else
		log_error "There are schema differences:"
		diff -u /tmp/public.schema.* | sed 's/^/ERROR: /'
		retstat=1
	fi
	nonrep_tab=$(run_sql "select relname from pg_catalog.pg_class where relkind = 'r' and relnamespace = (select oid from pg_catalog.pg_namespace where nspname = 'public') and relname not in (select tab_relname from _$CLUSTERNAME.sl_table)")
	nonrep_seq=$(run_sql "select relname from pg_catalog.pg_class where relkind = 'S' and relname not like 'sl_%' and relname not in (select seq_relname from _$CLUSTERNAME.sl_sequence)")
	if [ -n "$nonrep_tab" ] ; then
		retstat=1
		log_error "Tables are not replicated: $nonrep_tab" | tr '\012' ','
		echo
	fi
	if [ -n "$nonrep_seq" ] ; then
		retstat=1
		log_error "Sequences are not replicated: $nonrep_seq" | tr '\012' ','
		echo
	fi
	return $retstat
}
