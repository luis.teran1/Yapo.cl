
-- statistics tables
CREATE TABLE visitor (
  vid INTEGER PRIMARY KEY,
  creation DATE NOT NULL
);

CREATE TABLE visits (
  vid INTEGER NOT NULL REFERENCES visitor(vid),
  day DATE NOT NULL,
  n_visits INTEGER NOT NULL DEFAULT 1,
  n_entering_visits INTEGER NOT NULL DEFAULT 0,	
  n_free_entering_visits INTEGER NOT NULL DEFAULT 0,
  n_seconds_spent INTEGER NOT NULL DEFAULT 0,
  n_visits_from_click INTEGER NOT NULL DEFAULT 0,
  n_entering_visits_from_click INTEGER NOT NULL DEFAULT 0,
  PRIMARY KEY (vid, day)
);

GRANT select,insert,delete,update ON visitor to bwriters;
GRANT select on visitor TO breaders;
GRANT select,insert,delete,update ON visits to bwriters;
GRANT select on visits TO breaders;

CREATE TABLE pageviews_per_reg_cat (
  date DATE NOT NULL,
  page_type VARCHAR(20) NOT NULL DEFAULT 'unknown',
  user_region INTEGER NOT NULL DEFAULT 0,
  user_category INTEGER NOT NULL DEFAULT 0,
  page_region INTEGER NOT NULL DEFAULT 0,
  page_category INTEGER NOT NULL DEFAULT 0,
  extra_data    INTEGER NOT NULL DEFAULT 0,
  pageviews     INTEGER NOT NULL DEFAULT 0,
  PRIMARY KEY (date, page_type, user_region, page_region, user_category, page_category, extra_data)
);

GRANT select,insert,delete,update ON pageviews_per_reg_cat to bwriters;
GRANT select on pageviews_per_reg_cat TO breaders;

-- vim:syntax=sql
