-- Se agregan los avisos 126, 127 y 128 de los cuales 126 y 127 estan listos para ser archivados.
CREATE OR REPLACE FUNCTION truncate_archive_schema()
  RETURNS void LANGUAGE plpgsql AS
$func$
DECLARE
   arch_payment_groups text = (SELECT CONCAT('blocket_', date_part('year', CURRENT_DATE), '.payment_groups'));
BEGIN

-- truncate simply goes here:
EXECUTE 'TRUNCATE TABLE ' || arch_payment_groups || ' CASCADE';

END
$func$
;

SELECT truncate_archive_schema();
DROP FUNCTION truncate_archive_schema();

CREATE OR REPLACE FUNCTION select_by_schema(table_in text, column_out text, column_in text, parameter integer)
  RETURNS TABLE(resp varchar) LANGUAGE plpgsql AS
$func$
DECLARE
   schema text = (SELECT CONCAT('blocket_', date_part('year', CURRENT_DATE)));
BEGIN
RETURN QUERY
	EXECUTE '
		SELECT ' || column_out || '::varchar FROM ' || schema || '.' || table_in || ' WHERE ' || column_in || '=' || parameter || '
	';
RETURN;
END
$func$
;

-- fields (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email)
INSERT INTO tokens VALUES (715, 'X5bf305aa35abb3d4000000007a7a36db00000000', 9, '2018-11-19 15:49:14.049168', '2018-11-19 15:49:14.252369', '10.0.40.130', 'authenticate', NULL, NULL);
INSERT INTO tokens VALUES (716, 'X5bf305aa1a357f440000000010831a7e00000000', 9, '2018-11-19 15:49:14.252369', '2018-11-19 15:49:25.379583', '10.0.40.130', 'spamfilter', NULL, NULL);
INSERT INTO tokens VALUES (717, 'X5bf305b545139f66000000004c2810ad00000000', 9, '2018-11-19 15:49:25.379583', '2018-11-19 15:49:32.155613', '10.0.40.130', 'listadmins', NULL, NULL);
INSERT INTO tokens VALUES (718, 'X5bf305bc4cf8b400000000005f9ac56b00000000', 9, '2018-11-19 15:49:32.155613', '2018-11-19 15:49:32.177057', '10.0.40.130', 'listadmins', NULL, NULL);
INSERT INTO tokens VALUES (719, 'X5bf305bc1d44a543000000001898be3d00000000', 9, '2018-11-19 15:49:32.177057', '2018-11-19 15:49:41.679233', '10.0.40.130', 'search_ads', NULL, NULL);
INSERT INTO tokens VALUES (720, 'X5bf305c61b8bb30300000000305e483f00000000', 9, '2018-11-19 15:49:41.679233', '2018-11-19 15:49:41.701011', '10.0.40.130', 'listadmins', NULL, NULL);
INSERT INTO tokens VALUES (721, 'X5bf305c66df98f2100000000450dfe5e00000000', 9, '2018-11-19 15:49:41.701011', '2018-11-19 15:49:44.32497', '10.0.40.130', 'search_ads', NULL, NULL);
INSERT INTO tokens VALUES (722, 'X5bf305c87311c2aa00000000d7d0fdd00000000', 9, '2018-11-19 15:49:44.32497', '2018-11-19 15:49:44.426863', '10.0.40.130', 'adqueues', NULL, NULL);
INSERT INTO tokens VALUES (723, 'X5bf305c84fa8229b0000000047967cd400000000', 9, '2018-11-19 15:49:44.426863', '2018-11-19 15:49:44.43536', '10.0.40.130', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (724, 'X5bf305c852696f1300000000b6e88bd00000000', 9, '2018-11-19 15:49:44.43536', '2018-11-19 15:49:44.443103', '10.0.40.130', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (725, 'X5bf305c85340f7f8000000002b9af3d800000000', 9, '2018-11-19 15:49:44.443103', '2018-11-19 15:49:49.284717', '10.0.40.130', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (726, 'X5bf305cd183e5606000000006acbe9d000000000', 9, '2018-11-19 15:49:49.284717', '2018-11-19 15:49:49.370333', '10.0.40.130', 'adqueues', NULL, NULL);
INSERT INTO tokens VALUES (727, 'X5bf305cd3456dbc3000000001a78f1c300000000', 9, '2018-11-19 15:49:49.370333', '2018-11-19 15:49:49.381727', '10.0.40.130', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (728, 'X5bf305cd306b57d90000000063cc55d100000000', 9, '2018-11-19 15:49:49.381727', '2018-11-19 15:49:49.389782', '10.0.40.130', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (729, 'X5bf305cd5ded46dc0000000019440bfa00000000', 9, '2018-11-19 15:49:49.389782', '2018-11-19 15:49:53.225453', '10.0.40.130', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (730, 'X5bf305d1d9daced000000001c2c778d00000000', 9, '2018-11-19 15:49:53.225453', '2018-11-19 15:49:56.467114', '10.0.40.130', 'review', NULL, NULL);
INSERT INTO tokens VALUES (731, 'X5bf305d45e514424000000003843294700000000', 9, '2018-11-19 15:49:56.467114', '2018-11-19 15:50:00.904119', '10.0.40.130', 'review', NULL, NULL);
INSERT INTO tokens VALUES (732, 'X5bf305d95539b660000000001c2d251200000000', 9, '2018-11-19 15:50:00.904119', '2018-11-19 15:50:00.971865', '10.0.40.130', 'adqueues', NULL, NULL);
INSERT INTO tokens VALUES (733, 'X5bf305d91a33ac200000000029c16fe100000000', 9, '2018-11-19 15:50:00.971865', '2018-11-19 15:50:00.980381', '10.0.40.130', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (734, 'X5bf305d93d2efae50000000035c520e300000000', 9, '2018-11-19 15:50:00.980381', '2018-11-19 15:50:00.990349', '10.0.40.130', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (735, 'X5bf305d9581eefa000000000788b83b300000000', 9, '2018-11-19 15:50:00.990349', '2018-11-19 15:50:05.052574', '10.0.40.130', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (736, 'X5bf305dd8251a250000000075c360a200000000', 9, '2018-11-19 15:50:05.052574', '2018-11-19 15:50:13.241898', '10.0.40.130', 'review', NULL, NULL);
INSERT INTO tokens VALUES (737, 'X5bf308a6672bee96000000001425f51200000000', 9, '2018-11-19 16:01:57.825598', '2018-11-19 16:01:58.062513', '10.0.40.130', 'authenticate', NULL, NULL);
INSERT INTO tokens VALUES (738, 'X5bf308a626dd5b4a000000005b21ee5e00000000', 9, '2018-11-19 16:01:58.062513', '2018-11-19 16:02:00.658739', '10.0.40.130', 'spamfilter', NULL, NULL);
INSERT INTO tokens VALUES (739, 'X5bf308a96128e956000000006862172000000000', 9, '2018-11-19 16:02:00.658739', '2018-11-19 16:02:07.457467', '10.0.40.130', 'listadmins', NULL, NULL);
INSERT INTO tokens VALUES (740, 'X5bf308af20cca540000000032c8082500000000', 9, '2018-11-19 16:02:07.457467', '2018-11-19 16:02:07.478112', '10.0.40.130', 'listadmins', NULL, NULL);
INSERT INTO tokens VALUES (741, 'X5bf308af6a2bbc130000000077a626900000000', 9, '2018-11-19 16:02:07.478112', '2018-11-19 16:02:16.190414', '10.0.40.130', 'search_ads', NULL, NULL);
INSERT INTO tokens VALUES (742, 'X5bf308b83f91ee600000000042bf205100000000', 9, '2018-11-19 16:02:16.190414', '2018-11-19 16:02:16.202162', '10.0.40.130', 'listadmins', NULL, NULL);
INSERT INTO tokens VALUES (743, 'X5bf308b826f3bc000000007dc270ad00000000', 9, '2018-11-19 16:02:16.202162', '2018-11-19 16:02:28.236837', '10.0.40.130', 'search_ads', NULL, NULL);
INSERT INTO tokens VALUES (744, 'X5bf308c42608fbfa0000000037a00aeb00000000', 9, '2018-11-19 16:02:28.236837', '2018-11-19 16:02:45.495461', '10.0.40.130', 'deletead', NULL, NULL);
INSERT INTO tokens VALUES (745, 'X5bf308d53e90e20e000000001cc1b06a00000000', 9, '2018-11-19 16:02:45.495461', '2018-11-19 16:03:01.880212', '10.0.40.130', 'deletead', NULL, NULL);
INSERT INTO tokens VALUES (746, 'X5bf308e64fae3fd9000000005caf371300000000', 9, '2018-11-19 16:03:01.880212', '2018-11-19 16:03:01.902855', '10.0.40.130', 'listadmins', NULL, NULL);
INSERT INTO tokens VALUES (747, 'X5bf308e6663fa2db00000000108dc0b400000000', 9, '2018-11-19 16:03:01.902855', '2018-11-19 16:03:07.228975', '10.0.40.130', 'search_ads', NULL, NULL);
INSERT INTO tokens VALUES (748, 'X5bf308eb65d3df4f000000003772916700000000', 9, '2018-11-19 16:03:07.228975', '2018-11-19 16:03:09.896031', '10.0.40.130', 'deletead', NULL, NULL);
INSERT INTO tokens VALUES (749, 'X5bf308ee4409f00d0000000034b969ee00000000', 9, '2018-11-19 16:03:09.896031', '2018-11-19 16:03:22.666601', '10.0.40.130', 'deletead', NULL, NULL);
INSERT INTO tokens VALUES (750, 'X5bf308fb2cd1aa1f000000001aab847200000000', 9, '2018-11-19 16:03:22.666601', '2018-11-19 16:03:22.686598', '10.0.40.130', 'listadmins', NULL, NULL);
INSERT INTO tokens VALUES (751, 'X5bf308fb20bdb4ca00000000e1cb96900000000', 9, '2018-11-19 16:03:22.686598', '2018-11-19 16:03:34.165073', '10.0.40.130', 'search_ads', NULL, NULL);
INSERT INTO tokens VALUES (752, 'X5bf309061e4426ee0000000054f4347a00000000', 9, '2018-11-19 16:03:34.165073', '2018-11-19 16:14:40.926564', '10.0.40.130', 'deletead', NULL, NULL);
INSERT INTO tokens VALUES (753, 'X5bf30ba17c3f0613000000003d864e0600000000', 9, '2018-11-19 16:14:40.926564', '2018-11-19 16:14:40.948123', '10.0.40.130', 'listadmins', NULL, NULL);
INSERT INTO tokens VALUES (754, 'X5bf30ba1681ebd23000000005fd4b35700000000', 9, '2018-11-19 16:14:40.948123', '2018-11-19 16:16:04.226821', '10.0.40.130', 'search_ads', NULL, NULL);
INSERT INTO tokens VALUES (755, 'X5bf30bf4e58892c00000000d697f1100000000', 9, '2018-11-19 16:16:04.226821', '2018-11-19 16:16:04.267041', '10.0.40.130', 'listadmins', NULL, NULL);
INSERT INTO tokens VALUES (756, 'X5bf30bf477829bef00000000757f3d3800000000', 9, '2018-11-19 16:16:04.267041', '2018-11-19 16:16:07.226442', '10.0.40.130', 'search_ads', NULL, NULL);
INSERT INTO tokens VALUES (757, 'X5bf30bf73a599ae4000000002fc9438200000000', 9, '2018-11-19 16:16:07.226442', '2018-11-19 16:16:07.261534', '10.0.40.130', 'listadmins', NULL, NULL);
INSERT INTO tokens VALUES (758, 'X5bf30bf72b62b8e800000000ba8ef9300000000', 9, '2018-11-19 16:16:07.261534', '2018-11-19 17:16:07.261534', '10.0.40.130', 'search_ads', NULL, NULL);

-- fields (account_id, account_action_id, account_action_type, "timestamp", token_id)
INSERT INTO account_actions VALUES (6, 1, 'edition', '2018-11-19 15:43:39.531461', NULL);
INSERT INTO account_actions VALUES (6, 2, 'edition', '2018-11-19 15:45:19.90046', NULL);
INSERT INTO account_actions VALUES (6, 3, 'edition', '2018-11-19 15:48:25.908789', NULL);

-- fields (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang)
INSERT INTO ads VALUES (126, 8000087, '2018-11-19 15:53:45.963611', 'deleted', 'let', 'ManyAdsAccount', '987654321', 15, 0, 1240, 57, NULL, false, false, false, 'Casa para Archivar', 'achivado', 60000000, NULL, NULL, NULL, '2018-11-19 15:50:05', NULL, NULL, '$1024$2y1bQ}VYRD}zF4QCbf07a971b784888658a20e62156de64dfde5ffb6', '2018-11-19 16:02:45.501588', 'es');
INSERT INTO ads VALUES (127, 8000085, '2018-11-19 15:51:12.514315', 'deleted', 'sell', 'ManyAdsAccount', '987654321', 15, 0, 2020, 57, NULL, false, false, false, 'Chevrolet corsa 2003', 'Archivado', 2100000, NULL, NULL, NULL, '2018-11-19 15:49:53', NULL, NULL, '$1024$2y1bQ}VYRD}zF4QCbf07a971b784888658a20e62156de64dfde5ffb6', '2018-11-19 16:03:34.17164', 'es');
INSERT INTO ads VALUES (128, 8000086, '2018-11-19 15:53:45.932868', 'active', 'sell', 'ManyAdsAccount', '987654321', 15, 0, 6180, 57, NULL, false, false, false, 'Tu hermana (Servidor)', 'Esta disponible las 24 horas del d�a, no se cansa nunca, tiene muchos servicios disponibles y puedes agregarles mas.

Se vende en el barrio el Golf.

Se aceptan Ofertas.', 130000, NULL, NULL, NULL, '2018-11-19 15:49:56', NULL, NULL, '$1024$2y1bQ}VYRD}zF4QCbf07a971b784888658a20e62156de64dfde5ffb6', '2018-11-19 15:53:45.932868', 'es');


UPDATE ads SET list_time='2018-11-19 15:51:50.556756', orig_list_time='2014-01-15 10:44:46', modified_at='2018-11-19 15:51:50.556756' WHERE ad_id = 100;
UPDATE ads SET list_time='2018-11-19 15:40:32.767194' WHERE ad_id = 95;
UPDATE ads SET list_time='2018-11-19 15:51:50.667367', status='deleted', orig_list_time='2014-01-15 10:44:27', modified_at='2018-11-19 16:03:09.899556' WHERE ad_id = 97;
UPDATE ads SET list_time='2018-11-19 15:51:12.50833', status='deleted', orig_list_time='2014-01-15 10:44:19', modified_at='2018-11-19 16:03:07.238615' WHERE ad_id = 96;
UPDATE ads SET list_time='2018-11-19 15:53:46.026561', status='deleted', orig_list_time='2014-01-15 10:44:40', modified_at='2018-11-19 16:02:28.240778' WHERE ad_id = 99;
UPDATE ads SET list_time='2018-11-19 15:51:50.620307', orig_list_time='2014-01-15 10:44:35', modified_at='2018-11-19 15:51:50.620307' WHERE ad_id = 98;

-- fields (payment_group_id, code, status, added_at, parent_payment_group_id)
INSERT INTO payment_groups VALUES (166, '12345', 'paid', '2018-11-19 15:41:18.341774', NULL);
INSERT INTO payment_groups VALUES (167, '65432', 'paid', '2018-11-19 15:41:18.341774', 166);
INSERT INTO payment_groups VALUES (168, '32323', 'paid', '2018-11-19 15:41:18.341774', 166);
INSERT INTO payment_groups VALUES (169, '1134556998', 'verified', '2018-11-19 15:43:39.4005', NULL);
INSERT INTO payment_groups VALUES (170, '1492598639', 'verified', '2018-11-19 15:45:19.825283', NULL);
INSERT INTO payment_groups VALUES (171, '1420682339', 'verified', '2018-11-19 15:48:25.781995', NULL);
INSERT INTO payment_groups VALUES (172, '54545', 'paid', '2018-11-19 15:50:37.863231', NULL);
INSERT INTO payment_groups VALUES (173, '90001', 'paid', '2018-11-19 15:50:37.863231', 172);
INSERT INTO payment_groups VALUES (174, '90002', 'paid', '2018-11-19 15:50:37.863231', 172);
INSERT INTO payment_groups VALUES (175, '90003', 'paid', '2018-11-19 15:50:37.863231', 172);
INSERT INTO payment_groups VALUES (176, '90004', 'paid', '2018-11-19 15:51:10.069635', NULL);
INSERT INTO payment_groups VALUES (177, '90005', 'paid', '2018-11-19 15:51:10.069635', 176);
INSERT INTO payment_groups VALUES (178, '90006', 'paid', '2018-11-19 15:51:10.069635', 176);
INSERT INTO payment_groups VALUES (179, '90007', 'paid', '2018-11-19 15:51:47.985496', NULL);
INSERT INTO payment_groups VALUES (180, '90008', 'paid', '2018-11-19 15:51:47.985496', 179);
INSERT INTO payment_groups VALUES (181, '90009', 'paid', '2018-11-19 15:51:47.985496', 179);
INSERT INTO payment_groups VALUES (182, '90010', 'paid', '2018-11-19 15:51:47.985496', 179);
INSERT INTO payment_groups VALUES (183, '90011', 'paid', '2018-11-19 15:53:43.533549', NULL);
INSERT INTO payment_groups VALUES (184, '90012', 'paid', '2018-11-19 15:53:43.533549', 183);
INSERT INTO payment_groups VALUES (185, '90013', 'paid', '2018-11-19 15:53:43.533549', 183);
INSERT INTO payment_groups VALUES (186, '90014', 'paid', '2018-11-19 15:53:43.533549', 183);
INSERT INTO payment_groups VALUES (187, '90015', 'paid', '2018-11-19 15:53:43.533549', 183);
INSERT INTO payment_groups VALUES (188, '90016', 'paid', '2018-11-19 15:53:43.533549', 183);
INSERT INTO payment_groups VALUES (189, '90017', 'paid', '2018-11-19 15:54:12.926631', NULL);

-- fields (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id)
INSERT INTO ad_actions VALUES (97, 3, 'bump', 762, 'accepted', 'normal', NULL, NULL, NULL);
INSERT INTO ad_actions VALUES (100, 2, 'bump', 702, 'pending_bump', 'normal', NULL, NULL, 167);
INSERT INTO ad_actions VALUES (99, 2, 'bump', 703, 'pending_bump', 'normal', NULL, NULL, 168);
INSERT INTO ad_actions VALUES (100, 3, 'bump', 705, 'accepted', 'normal', NULL, NULL, NULL);
INSERT INTO ad_actions VALUES (99, 3, 'bump', 707, 'accepted', 'normal', NULL, NULL, NULL);
INSERT INTO ad_actions VALUES (127, 1, 'new', 717, 'accepted', 'normal', NULL, NULL, 170);
INSERT INTO ad_actions VALUES (128, 1, 'new', 718, 'accepted', 'normal', NULL, NULL, 171);
INSERT INTO ad_actions VALUES (126, 1, 'new', 719, 'accepted', 'normal', NULL, NULL, 169);
INSERT INTO ad_actions VALUES (126, 2, 'bump', 726, 'pending_bump', 'normal', NULL, NULL, 173);
INSERT INTO ad_actions VALUES (128, 2, 'bump', 727, 'pending_bump', 'normal', NULL, NULL, 174);
INSERT INTO ad_actions VALUES (127, 2, 'bump', 728, 'pending_bump', 'normal', NULL, NULL, 175);
INSERT INTO ad_actions VALUES (126, 3, 'bump', 730, 'accepted', 'normal', NULL, NULL, NULL);
INSERT INTO ad_actions VALUES (128, 3, 'bump', 732, 'accepted', 'normal', NULL, NULL, NULL);
INSERT INTO ad_actions VALUES (127, 3, 'bump', 734, 'accepted', 'normal', NULL, NULL, NULL);
INSERT INTO ad_actions VALUES (128, 4, 'bump', 773, 'pending_bump', 'normal', NULL, NULL, 184);
INSERT INTO ad_actions VALUES (96, 2, 'bump', 739, 'pending_bump', 'normal', NULL, NULL, 177);
INSERT INTO ad_actions VALUES (127, 4, 'bump', 740, 'pending_bump', 'normal', NULL, NULL, 178);
INSERT INTO ad_actions VALUES (96, 3, 'bump', 742, 'accepted', 'normal', NULL, NULL, NULL);
INSERT INTO ad_actions VALUES (126, 5, 'label', 775, 'accepted', 'normal', NULL, NULL, 186);
INSERT INTO ad_actions VALUES (127, 5, 'bump', 744, 'accepted', 'normal', NULL, NULL, NULL);
INSERT INTO ad_actions VALUES (127, 6, 'label', 777, 'accepted', 'normal', NULL, NULL, 188);
INSERT INTO ad_actions VALUES (100, 4, 'daily_bump', 754, 'accepted', 'normal', NULL, NULL, 182);
INSERT INTO ad_actions VALUES (100, 5, 'bump', 756, 'accepted', 'normal', NULL, NULL, NULL);
INSERT INTO ad_actions VALUES (98, 2, 'weekly_bump', 757, 'accepted', 'normal', NULL, NULL, 180);
INSERT INTO ad_actions VALUES (128, 5, 'bump', 779, 'accepted', 'normal', NULL, NULL, NULL);
INSERT INTO ad_actions VALUES (98, 3, 'bump', 759, 'accepted', 'normal', NULL, NULL, NULL);
INSERT INTO ad_actions VALUES (97, 2, 'weekly_bump', 760, 'accepted', 'normal', NULL, NULL, 181);
INSERT INTO ad_actions VALUES (126, 4, 'daily_bump', 780, 'accepted', 'normal', NULL, NULL, 185);
INSERT INTO ad_actions VALUES (126, 6, 'bump', 782, 'accepted', 'normal', NULL, NULL, NULL);
INSERT INTO ad_actions VALUES (99, 4, 'daily_bump', 783, 'accepted', 'normal', NULL, NULL, 187);
INSERT INTO ad_actions VALUES (99, 5, 'bump', 785, 'accepted', 'normal', NULL, NULL, NULL);
INSERT INTO ad_actions VALUES (96, 4, 'label', 788, 'accepted', 'normal', NULL, NULL, 189);
INSERT INTO ad_actions VALUES (99, 6, 'deactivate', 789, 'deactivated', 'normal', NULL, NULL, NULL);
INSERT INTO ad_actions VALUES (126, 7, 'deactivate', 790, 'deactivated', 'normal', NULL, NULL, NULL);
INSERT INTO ad_actions VALUES (127, 7, 'deactivate', 791, 'deactivated', 'normal', NULL, NULL, NULL);
INSERT INTO ad_actions VALUES (96, 5, 'deactivate', 792, 'deactivated', 'normal', NULL, NULL, NULL);
INSERT INTO ad_actions VALUES (97, 4, 'deactivate', 793, 'deactivated', 'normal', NULL, NULL, NULL);
INSERT INTO ad_actions VALUES (99, 7, 'delete', 794, 'deleted', 'normal', NULL, NULL, NULL);
INSERT INTO ad_actions VALUES (126, 8, 'delete', 795, 'deleted', 'normal', NULL, NULL, NULL);
INSERT INTO ad_actions VALUES (96, 6, 'delete', 796, 'deleted', 'normal', NULL, NULL, NULL);
INSERT INTO ad_actions VALUES (97, 5, 'delete', 797, 'deleted', 'normal', NULL, NULL, NULL);
INSERT INTO ad_actions VALUES (127, 8, 'delete', 798, 'deleted', 'normal', NULL, NULL, NULL);

-- fields (ad_id, action_id, name, value)
INSERT INTO action_params VALUES (126, 1, 'source', 'web');
INSERT INTO action_params VALUES (126, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params VALUES (127, 1, 'source', 'web');
INSERT INTO action_params VALUES (127, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params VALUES (128, 1, 'source', 'web');
INSERT INTO action_params VALUES (128, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params VALUES (126, 5, 'label_type', 'opportunity');
INSERT INTO action_params VALUES (127, 6, 'label_type', 'opportunity');
INSERT INTO action_params VALUES (96, 4, 'label_type', 'opportunity');
INSERT INTO action_params VALUES (99, 6, 'source', 'web');
INSERT INTO action_params VALUES (126, 7, 'source', 'web');
INSERT INTO action_params VALUES (127, 7, 'source', 'web');
INSERT INTO action_params VALUES (96, 5, 'source', 'web');
INSERT INTO action_params VALUES (97, 4, 'source', 'web');
INSERT INTO action_params VALUES (99, 7, 'source', 'web');
INSERT INTO action_params VALUES (126, 8, 'source', 'web');
INSERT INTO action_params VALUES (96, 6, 'source', 'web');
INSERT INTO action_params VALUES (97, 5, 'source', 'web');
INSERT INTO action_params VALUES (127, 8, 'source', 'web');

-- fields (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id)
INSERT INTO action_states VALUES (100, 2, 698, 'reg', 'initial', '2018-11-19 15:41:18.341774', '10.0.40.130', NULL);
INSERT INTO action_states VALUES (100, 2, 699, 'unpaid', 'pending_pay', '2018-11-19 15:41:18.341774', '10.0.40.130', NULL);
INSERT INTO action_states VALUES (99, 2, 700, 'reg', 'initial', '2018-11-19 15:41:18.341774', '10.0.40.130', NULL);
INSERT INTO action_states VALUES (99, 2, 701, 'unpaid', 'pending_pay', '2018-11-19 15:41:18.341774', '10.0.40.130', NULL);
INSERT INTO action_states VALUES (100, 2, 702, 'pending_bump', 'pay', '2018-11-19 15:41:20.767098', NULL, NULL);
INSERT INTO action_states VALUES (99, 2, 703, 'pending_bump', 'pay', '2018-11-19 15:41:20.804724', NULL, NULL);
INSERT INTO action_states VALUES (100, 3, 704, 'reg', 'initial', '2018-11-19 15:41:20.843917', '10.0.40.130', NULL);
INSERT INTO action_states VALUES (100, 3, 705, 'accepted', 'bump', '2018-11-19 15:41:20.843917', '10.0.40.130', NULL);
INSERT INTO action_states VALUES (99, 3, 706, 'reg', 'initial', '2018-11-19 15:41:20.863317', '10.0.40.130', NULL);
INSERT INTO action_states VALUES (99, 3, 707, 'accepted', 'bump', '2018-11-19 15:41:20.863317', '10.0.40.130', NULL);
INSERT INTO action_states VALUES (126, 1, 708, 'reg', 'initial', '2018-11-19 15:43:39.4005', '10.0.40.130', NULL);
INSERT INTO action_states VALUES (126, 1, 709, 'unverified', 'verifymail', '2018-11-19 15:43:39.4005', '10.0.40.130', NULL);
INSERT INTO action_states VALUES (126, 1, 710, 'pending_review', 'verify', '2018-11-19 15:43:39.617154', '10.0.40.130', NULL);
INSERT INTO action_states VALUES (127, 1, 711, 'reg', 'initial', '2018-11-19 15:45:19.825283', '10.0.40.130', NULL);
INSERT INTO action_states VALUES (127, 1, 712, 'unverified', 'verifymail', '2018-11-19 15:45:19.825283', '10.0.40.130', NULL);
INSERT INTO action_states VALUES (127, 1, 713, 'pending_review', 'verify', '2018-11-19 15:45:19.938524', '10.0.40.130', NULL);
INSERT INTO action_states VALUES (128, 1, 714, 'reg', 'initial', '2018-11-19 15:48:25.781995', '10.0.40.130', NULL);
INSERT INTO action_states VALUES (128, 1, 715, 'unverified', 'verifymail', '2018-11-19 15:48:25.781995', '10.0.40.130', NULL);
INSERT INTO action_states VALUES (128, 1, 716, 'pending_review', 'verify', '2018-11-19 15:48:25.983188', '10.0.40.130', NULL);
INSERT INTO action_states VALUES (127, 1, 717, 'accepted', 'accept', '2018-11-19 15:49:53.231279', '10.0.40.130', 729);
INSERT INTO action_states VALUES (128, 1, 718, 'accepted', 'accept', '2018-11-19 15:49:56.482817', '10.0.40.130', 730);
INSERT INTO action_states VALUES (126, 1, 719, 'accepted', 'accept', '2018-11-19 15:50:05.059267', '10.0.40.130', 735);
INSERT INTO action_states VALUES (126, 2, 720, 'reg', 'initial', '2018-11-19 15:50:37.863231', '10.0.40.130', NULL);
INSERT INTO action_states VALUES (126, 2, 721, 'unpaid', 'pending_pay', '2018-11-19 15:50:37.863231', '10.0.40.130', NULL);
INSERT INTO action_states VALUES (128, 2, 722, 'reg', 'initial', '2018-11-19 15:50:37.863231', '10.0.40.130', NULL);
INSERT INTO action_states VALUES (128, 2, 723, 'unpaid', 'pending_pay', '2018-11-19 15:50:37.863231', '10.0.40.130', NULL);
INSERT INTO action_states VALUES (127, 2, 724, 'reg', 'initial', '2018-11-19 15:50:37.863231', '10.0.40.130', NULL);
INSERT INTO action_states VALUES (127, 2, 725, 'unpaid', 'pending_pay', '2018-11-19 15:50:37.863231', '10.0.40.130', NULL);
INSERT INTO action_states VALUES (126, 2, 726, 'pending_bump', 'pay', '2018-11-19 15:50:39.964688', NULL, NULL);
INSERT INTO action_states VALUES (128, 2, 727, 'pending_bump', 'pay', '2018-11-19 15:50:39.994507', NULL, NULL);
INSERT INTO action_states VALUES (127, 2, 728, 'pending_bump', 'pay', '2018-11-19 15:50:40.021541', NULL, NULL);
INSERT INTO action_states VALUES (126, 3, 729, 'reg', 'initial', '2018-11-19 15:50:40.060791', '10.0.40.130', NULL);
INSERT INTO action_states VALUES (126, 3, 730, 'accepted', 'bump', '2018-11-19 15:50:40.060791', '10.0.40.130', NULL);
INSERT INTO action_states VALUES (128, 3, 731, 'reg', 'initial', '2018-11-19 15:50:40.075734', '10.0.40.130', NULL);
INSERT INTO action_states VALUES (128, 3, 732, 'accepted', 'bump', '2018-11-19 15:50:40.075734', '10.0.40.130', NULL);
INSERT INTO action_states VALUES (127, 3, 733, 'reg', 'initial', '2018-11-19 15:50:40.086771', '10.0.40.130', NULL);
INSERT INTO action_states VALUES (127, 3, 734, 'accepted', 'bump', '2018-11-19 15:50:40.086771', '10.0.40.130', NULL);
INSERT INTO action_states VALUES (96, 2, 735, 'reg', 'initial', '2018-11-19 15:51:10.069635', '10.0.40.130', NULL);
INSERT INTO action_states VALUES (96, 2, 736, 'unpaid', 'pending_pay', '2018-11-19 15:51:10.069635', '10.0.40.130', NULL);
INSERT INTO action_states VALUES (127, 4, 737, 'reg', 'initial', '2018-11-19 15:51:10.069635', '10.0.40.130', NULL);
INSERT INTO action_states VALUES (127, 4, 738, 'unpaid', 'pending_pay', '2018-11-19 15:51:10.069635', '10.0.40.130', NULL);
INSERT INTO action_states VALUES (96, 2, 739, 'pending_bump', 'pay', '2018-11-19 15:51:12.477186', NULL, NULL);
INSERT INTO action_states VALUES (127, 4, 740, 'pending_bump', 'pay', '2018-11-19 15:51:12.489152', NULL, NULL);
INSERT INTO action_states VALUES (96, 3, 741, 'reg', 'initial', '2018-11-19 15:51:12.50833', '10.0.40.130', NULL);
INSERT INTO action_states VALUES (96, 3, 742, 'accepted', 'bump', '2018-11-19 15:51:12.50833', '10.0.40.130', NULL);
INSERT INTO action_states VALUES (127, 5, 743, 'reg', 'initial', '2018-11-19 15:51:12.514315', '10.0.40.130', NULL);
INSERT INTO action_states VALUES (127, 5, 744, 'accepted', 'bump', '2018-11-19 15:51:12.514315', '10.0.40.130', NULL);
INSERT INTO action_states VALUES (98, 2, 745, 'reg', 'initial', '2018-11-19 15:51:47.985496', '10.0.40.130', NULL);
INSERT INTO action_states VALUES (98, 2, 746, 'unpaid', 'pending_pay', '2018-11-19 15:51:47.985496', '10.0.40.130', NULL);
INSERT INTO action_states VALUES (97, 2, 747, 'reg', 'initial', '2018-11-19 15:51:47.985496', '10.0.40.130', NULL);
INSERT INTO action_states VALUES (97, 2, 748, 'unpaid', 'pending_pay', '2018-11-19 15:51:47.985496', '10.0.40.130', NULL);
INSERT INTO action_states VALUES (100, 4, 749, 'reg', 'initial', '2018-11-19 15:51:47.985496', '10.0.40.130', NULL);
INSERT INTO action_states VALUES (100, 4, 750, 'unpaid', 'pending_pay', '2018-11-19 15:51:47.985496', '10.0.40.130', NULL);
INSERT INTO action_states VALUES (98, 2, 751, 'accepted', 'pay', '2018-11-19 15:51:50.456552', NULL, NULL);
INSERT INTO action_states VALUES (97, 2, 752, 'accepted', 'pay', '2018-11-19 15:51:50.484374', NULL, NULL);
INSERT INTO action_states VALUES (100, 4, 753, 'accepted', 'pay', '2018-11-19 15:51:50.505058', NULL, NULL);
INSERT INTO action_states VALUES (100, 4, 754, 'accepted', 'updating_counter', '2018-11-19 15:51:50.54329', '10.0.40.130', NULL);
INSERT INTO action_states VALUES (100, 5, 755, 'reg', 'initial', '2018-11-19 15:51:50.556756', NULL, NULL);
INSERT INTO action_states VALUES (100, 5, 756, 'accepted', 'bump', '2018-11-19 15:51:50.556756', NULL, NULL);
INSERT INTO action_states VALUES (98, 2, 757, 'accepted', 'updating_counter', '2018-11-19 15:51:50.574091', '10.0.40.130', NULL);
INSERT INTO action_states VALUES (98, 3, 758, 'reg', 'initial', '2018-11-19 15:51:50.620307', NULL, NULL);
INSERT INTO action_states VALUES (98, 3, 759, 'accepted', 'bump', '2018-11-19 15:51:50.620307', NULL, NULL);
INSERT INTO action_states VALUES (97, 2, 760, 'accepted', 'updating_counter', '2018-11-19 15:51:50.625418', '10.0.40.130', NULL);
INSERT INTO action_states VALUES (97, 3, 761, 'reg', 'initial', '2018-11-19 15:51:50.667367', NULL, NULL);
INSERT INTO action_states VALUES (97, 3, 762, 'accepted', 'bump', '2018-11-19 15:51:50.667367', NULL, NULL);
INSERT INTO action_states VALUES (128, 4, 763, 'reg', 'initial', '2018-11-19 15:53:43.533549', '10.0.40.130', NULL);
INSERT INTO action_states VALUES (128, 4, 764, 'unpaid', 'pending_pay', '2018-11-19 15:53:43.533549', '10.0.40.130', NULL);
INSERT INTO action_states VALUES (126, 4, 765, 'reg', 'initial', '2018-11-19 15:53:43.533549', '10.0.40.130', NULL);
INSERT INTO action_states VALUES (126, 4, 766, 'unpaid', 'pending_pay', '2018-11-19 15:53:43.533549', '10.0.40.130', NULL);
INSERT INTO action_states VALUES (126, 5, 767, 'reg', 'initial', '2018-11-19 15:53:43.533549', '10.0.40.130', NULL);
INSERT INTO action_states VALUES (126, 5, 768, 'unpaid', 'pending_pay', '2018-11-19 15:53:43.533549', '10.0.40.130', NULL);
INSERT INTO action_states VALUES (99, 4, 769, 'reg', 'initial', '2018-11-19 15:53:43.533549', '10.0.40.130', NULL);
INSERT INTO action_states VALUES (99, 4, 770, 'unpaid', 'pending_pay', '2018-11-19 15:53:43.533549', '10.0.40.130', NULL);
INSERT INTO action_states VALUES (127, 6, 771, 'reg', 'initial', '2018-11-19 15:53:43.533549', '10.0.40.130', NULL);
INSERT INTO action_states VALUES (127, 6, 772, 'unpaid', 'pending_pay', '2018-11-19 15:53:43.533549', '10.0.40.130', NULL);
INSERT INTO action_states VALUES (128, 4, 773, 'pending_bump', 'pay', '2018-11-19 15:53:45.765774', NULL, NULL);
INSERT INTO action_states VALUES (126, 4, 774, 'accepted', 'pay', '2018-11-19 15:53:45.799635', NULL, NULL);
INSERT INTO action_states VALUES (126, 5, 775, 'accepted', 'pay', '2018-11-19 15:53:45.834414', NULL, NULL);
INSERT INTO action_states VALUES (99, 4, 776, 'accepted', 'pay', '2018-11-19 15:53:45.861203', NULL, NULL);
INSERT INTO action_states VALUES (127, 6, 777, 'accepted', 'pay', '2018-11-19 15:53:45.887765', NULL, NULL);
INSERT INTO action_states VALUES (128, 5, 778, 'reg', 'initial', '2018-11-19 15:53:45.932868', '10.0.40.130', NULL);
INSERT INTO action_states VALUES (128, 5, 779, 'accepted', 'bump', '2018-11-19 15:53:45.932868', '10.0.40.130', NULL);
INSERT INTO action_states VALUES (126, 4, 780, 'accepted', 'updating_counter', '2018-11-19 15:53:45.94982', '10.0.40.130', NULL);
INSERT INTO action_states VALUES (126, 6, 781, 'reg', 'initial', '2018-11-19 15:53:45.963611', NULL, NULL);
INSERT INTO action_states VALUES (126, 6, 782, 'accepted', 'bump', '2018-11-19 15:53:45.963611', NULL, NULL);
INSERT INTO action_states VALUES (99, 4, 783, 'accepted', 'updating_counter', '2018-11-19 15:53:45.977709', '10.0.40.130', NULL);
INSERT INTO action_states VALUES (99, 5, 784, 'reg', 'initial', '2018-11-19 15:53:46.026561', NULL, NULL);
INSERT INTO action_states VALUES (99, 5, 785, 'accepted', 'bump', '2018-11-19 15:53:46.026561', NULL, NULL);
INSERT INTO action_states VALUES (96, 4, 786, 'reg', 'initial', '2018-11-19 15:54:12.926631', '10.0.40.130', NULL);
INSERT INTO action_states VALUES (96, 4, 787, 'unpaid', 'pending_pay', '2018-11-19 15:54:12.926631', '10.0.40.130', NULL);
INSERT INTO action_states VALUES (96, 4, 788, 'accepted', 'pay', '2018-11-19 15:54:15.230579', NULL, NULL);
INSERT INTO action_states VALUES (99, 6, 789, 'deactivated', 'user_deactivated', '2018-11-19 16:00:14.362281', '10.0.40.130', NULL);
INSERT INTO action_states VALUES (126, 7, 790, 'deactivated', 'user_deactivated', '2018-11-19 16:00:51.21634', '10.0.40.130', NULL);
INSERT INTO action_states VALUES (127, 7, 791, 'deactivated', 'user_deactivated', '2018-11-19 16:01:23.332828', '10.0.40.130', NULL);
INSERT INTO action_states VALUES (96, 5, 792, 'deactivated', 'user_deactivated', '2018-11-19 16:01:28.764611', '10.0.40.130', NULL);
INSERT INTO action_states VALUES (97, 4, 793, 'deactivated', 'user_deactivated', '2018-11-19 16:01:34.119103', '10.0.40.130', NULL);
INSERT INTO action_states VALUES (99, 7, 794, 'deleted', 'admin_deleted', '2015-11-19 16:02:28.240778', '10.0.40.130', 743);
INSERT INTO action_states VALUES (126, 8, 795, 'deleted', 'admin_deleted', '2015-11-19 16:02:28.240778', '10.0.40.130', 744);
INSERT INTO action_states VALUES (127, 8, 798, 'deleted', 'admin_deleted', '2015-11-19 16:02:28.240778', '10.0.40.130', 751);
INSERT INTO action_states VALUES (96, 6, 796, 'deleted', 'admin_deleted', '2015-11-19 16:02:28.240778', '10.0.40.130', 747);
INSERT INTO action_states VALUES (97, 5, 797, 'deleted', 'admin_deleted', '2015-11-19 16:02:28.240778', '10.0.40.130', 748);
SELECT pg_catalog.setval('action_states_state_id_seq', 798, true);

-- fields (ad_id, action_id, state_id, is_param, column_name, old_value, new_value)
INSERT INTO ad_changes VALUES (100, 4, 754, true, 'daily_bump', NULL, '6');
INSERT INTO ad_changes VALUES (98, 2, 757, true, 'weekly_bump', NULL, '3');
INSERT INTO ad_changes VALUES (97, 2, 760, true, 'weekly_bump', NULL, '3');
INSERT INTO ad_changes VALUES (126, 4, 780, true, 'daily_bump', NULL, '6');
INSERT INTO ad_changes VALUES (99, 4, 783, true, 'daily_bump', NULL, '6');
INSERT INTO ad_changes VALUES (126, 5, 775, true, 'label', NULL, 'opportunity');
INSERT INTO ad_changes VALUES (127, 6, 777, true, 'label', NULL, 'opportunity');
INSERT INTO ad_changes VALUES (96, 4, 788, true, 'label', NULL, 'opportunity');
INSERT INTO ad_changes VALUES (99, 6, 789, true, 'daily_bump', '6', NULL);
INSERT INTO ad_changes VALUES (126, 7, 790, true, 'daily_bump', '6', NULL);
INSERT INTO ad_changes VALUES (126, 7, 790, true, 'label', 'opportunity', NULL);
INSERT INTO ad_changes VALUES (127, 7, 791, true, 'label', 'opportunity', NULL);
INSERT INTO ad_changes VALUES (96, 5, 792, true, 'label', 'opportunity', NULL);
INSERT INTO ad_changes VALUES (97, 4, 793, true, 'weekly_bump', '3', NULL);

-- fields (code, code_type)
INSERT INTO ad_codes VALUES (24741, 'pay');
INSERT INTO ad_codes VALUES (56764, 'pay');
INSERT INTO ad_codes VALUES (88787, 'pay');

-- fields (ad_id, name, value)
INSERT INTO ad_params VALUES (126, 'rooms', '1');
INSERT INTO ad_params VALUES (126, 'size', '100');
INSERT INTO ad_params VALUES (126, 'util_size', '100');
INSERT INTO ad_params VALUES (126, 'garage_spaces', '1');
INSERT INTO ad_params VALUES (126, 'condominio', '2000');
INSERT INTO ad_params VALUES (126, 'communes', '315');
INSERT INTO ad_params VALUES (126, 'currency', 'peso');
INSERT INTO ad_params VALUES (126, 'geoposition_is_precise', '0');
INSERT INTO ad_params VALUES (126, 'estate_type', '2');
INSERT INTO ad_params VALUES (126, 'bathrooms', '1');
INSERT INTO ad_params VALUES (126, 'built_year', '1998');
INSERT INTO ad_params VALUES (126, 'equipment', '');
INSERT INTO ad_params VALUES (126, 'country', 'UNK');
INSERT INTO ad_params VALUES (127, 'regdate', '2003');
INSERT INTO ad_params VALUES (127, 'mileage', '140000');
INSERT INTO ad_params VALUES (127, 'gearbox', '1');
INSERT INTO ad_params VALUES (127, 'fuel', '1');
INSERT INTO ad_params VALUES (127, 'cartype', '1');
INSERT INTO ad_params VALUES (127, 'communes', '315');
INSERT INTO ad_params VALUES (127, 'currency', 'peso');
INSERT INTO ad_params VALUES (127, 'brand', '18');
INSERT INTO ad_params VALUES (127, 'model', '32');
INSERT INTO ad_params VALUES (127, 'version', '25');
INSERT INTO ad_params VALUES (127, 'geoposition_is_precise', '0');
INSERT INTO ad_params VALUES (127, 'plates', 'VS4832');
INSERT INTO ad_params VALUES (127, 'country', 'UNK');
INSERT INTO ad_params VALUES (128, 'communes', '315');
INSERT INTO ad_params VALUES (128, 'currency', 'peso');
INSERT INTO ad_params VALUES (128, 'geoposition_is_precise', '0');
INSERT INTO ad_params VALUES (128, 'country', 'UNK');
INSERT INTO ad_params VALUES (100, 'daily_bump', '6');
INSERT INTO ad_params VALUES (98, 'weekly_bump', '3');

SELECT pg_catalog.setval('ads_ad_id_seq', 128, true);

INSERT INTO email_uuid (email, uuid, name) VALUES ('many@ads.cl', '00fdce7a-06ed-4677-8949-ffff29ad13b1', '');

SELECT pg_catalog.setval('list_id_seq', 8000087, true);
SELECT pg_catalog.setval('pay_code_seq', 469, true);

-- fields (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status)
INSERT INTO pay_log VALUES (213, 'save', 0, '65432', NULL, '2018-11-19 15:41:18', 167, 'SAVE');
INSERT INTO pay_log VALUES (214, 'save', 0, '32323', NULL, '2018-11-19 15:41:18', 168, 'SAVE');
INSERT INTO pay_log VALUES (215, 'paycard_save', 0, '12345', NULL, '2018-11-19 15:41:21', 166, 'SAVE');
INSERT INTO pay_log VALUES (216, 'paycard', 1100, '00167a', NULL, '2018-11-19 15:41:21', 167, 'OK');
INSERT INTO pay_log VALUES (217, 'paycard', 1100, '00168a', NULL, '2018-11-19 15:41:21', 168, 'OK');
INSERT INTO pay_log VALUES (218, 'paycard', 2200, '12345', NULL, '2018-11-19 15:41:21', 166, 'OK');
INSERT INTO pay_log VALUES (219, 'save', 0, '1134556998', NULL, '2018-11-19 15:43:39', 169, 'SAVE');
INSERT INTO pay_log VALUES (220, 'verify', 0, '1134556998', NULL, '2018-11-19 15:43:40', 169, 'OK');
INSERT INTO pay_log VALUES (221, 'save', 0, '1492598639', NULL, '2018-11-19 15:45:20', 170, 'SAVE');
INSERT INTO pay_log VALUES (222, 'verify', 0, '1492598639', NULL, '2018-11-19 15:45:20', 170, 'OK');
INSERT INTO pay_log VALUES (223, 'save', 0, '1420682339', NULL, '2018-11-19 15:48:26', 171, 'SAVE');
INSERT INTO pay_log VALUES (224, 'verify', 0, '1420682339', NULL, '2018-11-19 15:48:26', 171, 'OK');
INSERT INTO pay_log VALUES (225, 'save', 0, '90001', NULL, '2018-11-19 15:50:38', 173, 'SAVE');
INSERT INTO pay_log VALUES (226, 'save', 0, '90002', NULL, '2018-11-19 15:50:38', 174, 'SAVE');
INSERT INTO pay_log VALUES (227, 'save', 0, '90003', NULL, '2018-11-19 15:50:38', 175, 'SAVE');
INSERT INTO pay_log VALUES (228, 'paycard_save', 0, '54545', NULL, '2018-11-19 15:50:40', 172, 'SAVE');
INSERT INTO pay_log VALUES (229, 'paycard', 2900, '00173a', NULL, '2018-11-19 15:50:40', 173, 'OK');
INSERT INTO pay_log VALUES (230, 'paycard', 1100, '00174a', NULL, '2018-11-19 15:50:40', 174, 'OK');
INSERT INTO pay_log VALUES (231, 'paycard', 2150, '00175a', NULL, '2018-11-19 15:50:40', 175, 'OK');
INSERT INTO pay_log VALUES (232, 'paycard', 6150, '54545', NULL, '2018-11-19 15:50:40', 172, 'OK');
INSERT INTO pay_log VALUES (233, 'save', 0, '90005', NULL, '2018-11-19 15:51:10', 177, 'SAVE');
INSERT INTO pay_log VALUES (234, 'save', 0, '90006', NULL, '2018-11-19 15:51:10', 178, 'SAVE');
INSERT INTO pay_log VALUES (235, 'paycard_save', 0, '90004', NULL, '2018-11-19 15:51:12', 176, 'SAVE');
INSERT INTO pay_log VALUES (236, 'paycard', 1100, '00177a', NULL, '2018-11-19 15:51:12', 177, 'OK');
INSERT INTO pay_log VALUES (237, 'paycard', 2150, '00178a', NULL, '2018-11-19 15:51:12', 178, 'OK');
INSERT INTO pay_log VALUES (238, 'paycard', 3250, '90004', NULL, '2018-11-19 15:51:12', 176, 'OK');
INSERT INTO pay_log VALUES (239, 'save', 0, '90008', NULL, '2018-11-19 15:51:48', 180, 'SAVE');
INSERT INTO pay_log VALUES (240, 'save', 0, '90009', NULL, '2018-11-19 15:51:48', 181, 'SAVE');
INSERT INTO pay_log VALUES (241, 'save', 0, '90010', NULL, '2018-11-19 15:51:48', 182, 'SAVE');
INSERT INTO pay_log VALUES (242, 'paycard_save', 0, '90007', NULL, '2018-11-19 15:51:50', 179, 'SAVE');
INSERT INTO pay_log VALUES (243, 'paycard', 3100, '00180a', NULL, '2018-11-19 15:51:50', 180, 'OK');
INSERT INTO pay_log VALUES (244, 'paycard', 3100, '00181a', NULL, '2018-11-19 15:51:50', 181, 'OK');
INSERT INTO pay_log VALUES (245, 'paycard', 5000, '00182a', NULL, '2018-11-19 15:51:51', 182, 'OK');
INSERT INTO pay_log VALUES (246, 'paycard', 11200, '90007', NULL, '2018-11-19 15:51:51', 179, 'OK');
INSERT INTO pay_log VALUES (247, 'save', 0, '90012', NULL, '2018-11-19 15:53:44', 184, 'SAVE');
INSERT INTO pay_log VALUES (248, 'save', 0, '90013', NULL, '2018-11-19 15:53:44', 185, 'SAVE');
INSERT INTO pay_log VALUES (249, 'save', 0, '90014', NULL, '2018-11-19 15:53:44', 186, 'SAVE');
INSERT INTO pay_log VALUES (250, 'save', 0, '90015', NULL, '2018-11-19 15:53:44', 187, 'SAVE');
INSERT INTO pay_log VALUES (251, 'save', 0, '90016', NULL, '2018-11-19 15:53:44', 188, 'SAVE');
INSERT INTO pay_log VALUES (252, 'paycard_save', 0, '90011', NULL, '2018-11-19 15:53:46', 183, 'SAVE');
INSERT INTO pay_log VALUES (253, 'paycard', 1100, '00184a', NULL, '2018-11-19 15:53:46', 184, 'OK');
INSERT INTO pay_log VALUES (254, 'paycard', 12500, '00185a', NULL, '2018-11-19 15:53:46', 185, 'OK');
INSERT INTO pay_log VALUES (255, 'paycard', 3150, '00186a', NULL, '2018-11-19 15:53:46', 186, 'OK');
INSERT INTO pay_log VALUES (256, 'paycard', 5000, '00187a', NULL, '2018-11-19 15:53:46', 187, 'OK');
INSERT INTO pay_log VALUES (257, 'paycard', 2200, '00188a', NULL, '2018-11-19 15:53:46', 188, 'OK');
INSERT INTO pay_log VALUES (258, 'paycard', 23950, '90011', NULL, '2018-11-19 15:53:46', 183, 'OK');
INSERT INTO pay_log VALUES (259, 'save', 0, '90017', NULL, '2018-11-19 15:54:13', 189, 'SAVE');
INSERT INTO pay_log VALUES (260, 'paycard_save', 0, '90017', NULL, '2018-11-19 15:54:15', 189, 'SAVE');
INSERT INTO pay_log VALUES (261, 'paycard', 2100, '00189a', NULL, '2018-11-19 15:54:15', 189, 'OK');
INSERT INTO pay_log VALUES (262, 'paycard', 2100, '90017', NULL, '2018-11-19 15:54:15', 189, 'OK');

SELECT pg_catalog.setval('pay_log_pay_log_id_seq', 262, true);

-- fields (pay_log_id, ref_num, ref_type, reference)
INSERT INTO pay_log_references VALUES (213, 0, 'remote_addr', '10.0.40.130');
INSERT INTO pay_log_references VALUES (214, 0, 'remote_addr', '10.0.40.130');
INSERT INTO pay_log_references VALUES (215, 0, 'auth_code', '402957');
INSERT INTO pay_log_references VALUES (215, 1, 'trans_date', '19/11/2018 15:41:20');
INSERT INTO pay_log_references VALUES (215, 2, 'placements_type', 'VN');
INSERT INTO pay_log_references VALUES (215, 3, 'placements_number', '1');
INSERT INTO pay_log_references VALUES (215, 4, 'pay_type', 'credit');
INSERT INTO pay_log_references VALUES (215, 5, 'external_response', '0');
INSERT INTO pay_log_references VALUES (215, 6, 'external_id', '85567077930645452699');
INSERT INTO pay_log_references VALUES (215, 7, 'cc_number', '5937');
INSERT INTO pay_log_references VALUES (215, 8, 'remote_addr', '10.0.1.77');
INSERT INTO pay_log_references VALUES (216, 0, 'webpay', '1');
INSERT INTO pay_log_references VALUES (216, 1, 'webpay', '1');
INSERT INTO pay_log_references VALUES (217, 0, 'webpay', '1');
INSERT INTO pay_log_references VALUES (217, 1, 'webpay', '1');
INSERT INTO pay_log_references VALUES (219, 0, 'adphone', '987654321');
INSERT INTO pay_log_references VALUES (219, 1, 'remote_addr', '10.0.40.130');
INSERT INTO pay_log_references VALUES (220, 0, 'remote_addr', '10.0.40.130');
INSERT INTO pay_log_references VALUES (221, 0, 'adphone', '987654321');
INSERT INTO pay_log_references VALUES (221, 1, 'remote_addr', '10.0.40.130');
INSERT INTO pay_log_references VALUES (222, 0, 'remote_addr', '10.0.40.130');
INSERT INTO pay_log_references VALUES (223, 0, 'adphone', '987654321');
INSERT INTO pay_log_references VALUES (223, 1, 'remote_addr', '10.0.40.130');
INSERT INTO pay_log_references VALUES (224, 0, 'remote_addr', '10.0.40.130');
INSERT INTO pay_log_references VALUES (225, 0, 'remote_addr', '10.0.40.130');
INSERT INTO pay_log_references VALUES (226, 0, 'remote_addr', '10.0.40.130');
INSERT INTO pay_log_references VALUES (227, 0, 'remote_addr', '10.0.40.130');
INSERT INTO pay_log_references VALUES (228, 0, 'auth_code', '692409');
INSERT INTO pay_log_references VALUES (228, 1, 'trans_date', '19/11/2018 15:50:39');
INSERT INTO pay_log_references VALUES (228, 2, 'placements_type', 'VN');
INSERT INTO pay_log_references VALUES (228, 3, 'placements_number', '1');
INSERT INTO pay_log_references VALUES (228, 4, 'pay_type', 'credit');
INSERT INTO pay_log_references VALUES (228, 5, 'external_response', '0');
INSERT INTO pay_log_references VALUES (228, 6, 'external_id', '75098404268616405022');
INSERT INTO pay_log_references VALUES (228, 7, 'cc_number', '4639');
INSERT INTO pay_log_references VALUES (228, 8, 'remote_addr', '10.0.1.77');
INSERT INTO pay_log_references VALUES (229, 0, 'webpay', '1');
INSERT INTO pay_log_references VALUES (229, 1, 'webpay', '1');
INSERT INTO pay_log_references VALUES (230, 0, 'webpay', '1');
INSERT INTO pay_log_references VALUES (230, 1, 'webpay', '1');
INSERT INTO pay_log_references VALUES (231, 0, 'webpay', '1');
INSERT INTO pay_log_references VALUES (231, 1, 'webpay', '1');
INSERT INTO pay_log_references VALUES (233, 0, 'remote_addr', '10.0.40.130');
INSERT INTO pay_log_references VALUES (234, 0, 'remote_addr', '10.0.40.130');
INSERT INTO pay_log_references VALUES (235, 0, 'auth_code', '124626');
INSERT INTO pay_log_references VALUES (235, 1, 'trans_date', '19/11/2018 15:51:12');
INSERT INTO pay_log_references VALUES (235, 2, 'placements_type', 'VN');
INSERT INTO pay_log_references VALUES (235, 3, 'placements_number', '1');
INSERT INTO pay_log_references VALUES (235, 4, 'pay_type', 'credit');
INSERT INTO pay_log_references VALUES (235, 5, 'external_response', '0');
INSERT INTO pay_log_references VALUES (235, 6, 'external_id', '70650427431467860935');
INSERT INTO pay_log_references VALUES (235, 7, 'cc_number', '1268');
INSERT INTO pay_log_references VALUES (235, 8, 'remote_addr', '10.0.1.77');
INSERT INTO pay_log_references VALUES (236, 0, 'webpay', '1');
INSERT INTO pay_log_references VALUES (236, 1, 'webpay', '1');
INSERT INTO pay_log_references VALUES (237, 0, 'webpay', '1');
INSERT INTO pay_log_references VALUES (237, 1, 'webpay', '1');
INSERT INTO pay_log_references VALUES (239, 0, 'remote_addr', '10.0.40.130');
INSERT INTO pay_log_references VALUES (240, 0, 'remote_addr', '10.0.40.130');
INSERT INTO pay_log_references VALUES (241, 0, 'remote_addr', '10.0.40.130');
INSERT INTO pay_log_references VALUES (242, 0, 'auth_code', '133761');
INSERT INTO pay_log_references VALUES (242, 1, 'trans_date', '19/11/2018 15:51:50');
INSERT INTO pay_log_references VALUES (242, 2, 'placements_type', 'VN');
INSERT INTO pay_log_references VALUES (242, 3, 'placements_number', '1');
INSERT INTO pay_log_references VALUES (242, 4, 'pay_type', 'credit');
INSERT INTO pay_log_references VALUES (242, 5, 'external_response', '0');
INSERT INTO pay_log_references VALUES (242, 6, 'external_id', '10837302344770311699');
INSERT INTO pay_log_references VALUES (242, 7, 'cc_number', '7335');
INSERT INTO pay_log_references VALUES (242, 8, 'remote_addr', '10.0.1.77');
INSERT INTO pay_log_references VALUES (243, 0, 'webpay', '1');
INSERT INTO pay_log_references VALUES (243, 1, 'webpay', '1');
INSERT INTO pay_log_references VALUES (244, 0, 'webpay', '1');
INSERT INTO pay_log_references VALUES (244, 1, 'webpay', '1');
INSERT INTO pay_log_references VALUES (245, 0, 'webpay', '1');
INSERT INTO pay_log_references VALUES (245, 1, 'webpay', '1');
INSERT INTO pay_log_references VALUES (247, 0, 'remote_addr', '10.0.40.130');
INSERT INTO pay_log_references VALUES (248, 0, 'remote_addr', '10.0.40.130');
INSERT INTO pay_log_references VALUES (249, 0, 'remote_addr', '10.0.40.130');
INSERT INTO pay_log_references VALUES (250, 0, 'remote_addr', '10.0.40.130');
INSERT INTO pay_log_references VALUES (251, 0, 'remote_addr', '10.0.40.130');
INSERT INTO pay_log_references VALUES (252, 0, 'auth_code', '226403');
INSERT INTO pay_log_references VALUES (252, 1, 'trans_date', '19/11/2018 15:53:45');
INSERT INTO pay_log_references VALUES (252, 2, 'placements_type', 'VN');
INSERT INTO pay_log_references VALUES (252, 3, 'placements_number', '1');
INSERT INTO pay_log_references VALUES (252, 4, 'pay_type', 'credit');
INSERT INTO pay_log_references VALUES (252, 5, 'external_response', '0');
INSERT INTO pay_log_references VALUES (252, 6, 'external_id', '95324928748298813275');
INSERT INTO pay_log_references VALUES (252, 7, 'cc_number', '9908');
INSERT INTO pay_log_references VALUES (252, 8, 'remote_addr', '10.0.1.77');
INSERT INTO pay_log_references VALUES (253, 0, 'webpay', '1');
INSERT INTO pay_log_references VALUES (253, 1, 'webpay', '1');
INSERT INTO pay_log_references VALUES (254, 0, 'webpay', '1');
INSERT INTO pay_log_references VALUES (254, 1, 'webpay', '1');
INSERT INTO pay_log_references VALUES (255, 0, 'webpay', '1');
INSERT INTO pay_log_references VALUES (255, 1, 'webpay', '1');
INSERT INTO pay_log_references VALUES (256, 0, 'webpay', '1');
INSERT INTO pay_log_references VALUES (256, 1, 'webpay', '1');
INSERT INTO pay_log_references VALUES (257, 0, 'webpay', '1');
INSERT INTO pay_log_references VALUES (257, 1, 'webpay', '1');
INSERT INTO pay_log_references VALUES (259, 0, 'remote_addr', '10.0.40.130');
INSERT INTO pay_log_references VALUES (260, 0, 'auth_code', '321851');
INSERT INTO pay_log_references VALUES (260, 1, 'trans_date', '19/11/2018 15:54:15');
INSERT INTO pay_log_references VALUES (260, 2, 'placements_type', 'VN');
INSERT INTO pay_log_references VALUES (260, 3, 'placements_number', '1');
INSERT INTO pay_log_references VALUES (260, 4, 'pay_type', 'credit');
INSERT INTO pay_log_references VALUES (260, 5, 'external_response', '0');
INSERT INTO pay_log_references VALUES (260, 6, 'external_id', '60690281382641437075');
INSERT INTO pay_log_references VALUES (260, 7, 'cc_number', '9181');
INSERT INTO pay_log_references VALUES (260, 8, 'remote_addr', '10.0.1.77');
INSERT INTO pay_log_references VALUES (261, 0, 'webpay', '1');
INSERT INTO pay_log_references VALUES (261, 1, 'webpay', '1');

SELECT pg_catalog.setval('payment_groups_payment_group_id_seq', 189, true);

-- fields (payment_group_id, payment_type, pay_amount, discount)
INSERT INTO payments VALUES (167, 'bump', 1100, 0);
INSERT INTO payments VALUES (168, 'bump', 1100, 0);
INSERT INTO payments VALUES (169, 'ad_action', 0, 0);
INSERT INTO payments VALUES (170, 'ad_action', 0, 0);
INSERT INTO payments VALUES (171, 'ad_action', 0, 0);
INSERT INTO payments VALUES (173, 'bump', 2900, 0);
INSERT INTO payments VALUES (174, 'bump', 1100, 0);
INSERT INTO payments VALUES (175, 'bump', 2150, 0);
INSERT INTO payments VALUES (177, 'bump', 1100, 0);
INSERT INTO payments VALUES (178, 'bump', 2150, 0);
INSERT INTO payments VALUES (180, 'weekly_bump', 3100, 0);
INSERT INTO payments VALUES (181, 'weekly_bump', 3100, 0);
INSERT INTO payments VALUES (182, 'daily_bump', 5000, 0);
INSERT INTO payments VALUES (184, 'bump', 1100, 0);
INSERT INTO payments VALUES (185, 'daily_bump', 12500, 0);
INSERT INTO payments VALUES (186, 'label', 3150, 0);
INSERT INTO payments VALUES (187, 'daily_bump', 5000, 0);
INSERT INTO payments VALUES (188, 'label', 2200, 0);
INSERT INTO payments VALUES (189, 'label', 2100, 0);

-- fields (purchase_id, doc_type, doc_num, external_doc_id, status, receipt, delivery, rut, name, lob, address, region, communes, contact, email, tax, discount, seller_id, total_price, payment_group_id, payment_method, payment_platform, account_id)
INSERT INTO purchase VALUES (2, 'bill', 1, 1001, 'confirmed', '2018-01-19 15:51:10.069635', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'many@ads.cl', 0, 0, NULL, 2200, 166, 'webpay', 'desktop', 6);
INSERT INTO purchase VALUES (3, 'bill', 2, 1002, 'confirmed', '2018-01-19 15:51:10.069635', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'many@ads.cl', 0, 0, NULL, 6150, 172, 'webpay', 'desktop', 6);
INSERT INTO purchase VALUES (4, 'bill', 3, 1003, 'confirmed', '2018-01-19 15:51:10.069635', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'many@ads.cl', 0, 0, NULL, 3250, 176, 'webpay', 'desktop', 6);
INSERT INTO purchase VALUES (5, 'bill', 4, 1004, 'confirmed', '2018-01-19 15:51:10.069635', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'many@ads.cl', 0, 0, NULL, 11200, 179, 'khipu', 'desktop', 6);
INSERT INTO purchase VALUES (6, 'bill', 5, 1005, 'confirmed', '2018-01-19 15:51:10.069635', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'many@ads.cl', 0, 0, NULL, 23950, 183, 'servipag', 'desktop', 6);
INSERT INTO purchase VALUES (7, 'bill', 6, 1006, 'confirmed', '2018-01-19 15:51:10.069635', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'many@ads.cl', 0, 0, NULL, 2100, 189, 'webpay_plus', 'msite', 6);

-- fields (purchase_detail_id, purchase_id, product_id, price, action_id, ad_id, payment_group_id)
INSERT INTO purchase_detail VALUES (2, 2, 1, 1100, 2, 100, 167);
INSERT INTO purchase_detail VALUES (3, 2, 1, 1100, 2, 99, 168);
INSERT INTO purchase_detail VALUES (4, 3, 1, 2900, 2, 126, 173);
INSERT INTO purchase_detail VALUES (5, 3, 1, 1100, 2, 128, 174);
INSERT INTO purchase_detail VALUES (6, 3, 1, 2150, 2, 127, 175);
INSERT INTO purchase_detail VALUES (7, 4, 1, 1100, 2, 96, 177);
INSERT INTO purchase_detail VALUES (8, 4, 1, 2150, 4, 127, 178);
INSERT INTO purchase_detail VALUES (9, 5, 2, 3100, 2, 98, 180);
INSERT INTO purchase_detail VALUES (10, 5, 2, 3100, 2, 97, 181);
INSERT INTO purchase_detail VALUES (11, 5, 8, 5000, 4, 100, 182);
INSERT INTO purchase_detail VALUES (12, 6, 1, 1100, 4, 128, 184);
INSERT INTO purchase_detail VALUES (13, 6, 8, 12500, 4, 126, 185);
INSERT INTO purchase_detail VALUES (14, 6, 9, 3150, 5, 126, 186);
INSERT INTO purchase_detail VALUES (15, 6, 8, 5000, 4, 99, 187);
INSERT INTO purchase_detail VALUES (16, 6, 9, 2200, 6, 127, 188);
INSERT INTO purchase_detail VALUES (17, 7, 9, 2100, 4, 96, 189);

SELECT pg_catalog.setval('purchase_detail_purchase_detail_id_seq', 17, true);
SELECT pg_catalog.setval('purchase_purchase_id_seq', 7, true);

-- fields (purchase_id, purchase_state_id, status, "timestamp", remote_addr)
INSERT INTO purchase_states VALUES (2, 1, 'pending', '2018-11-19 15:41:18.341774', '10.0.40.130');
INSERT INTO purchase_states VALUES (3, 2, 'pending', '2018-11-19 15:50:37.863231', '10.0.40.130');
INSERT INTO purchase_states VALUES (4, 3, 'pending', '2018-11-19 15:51:10.069635', '10.0.40.130');
INSERT INTO purchase_states VALUES (5, 4, 'pending', '2018-11-19 15:51:47.985496', '10.0.40.130');
INSERT INTO purchase_states VALUES (6, 5, 'pending', '2018-11-19 15:53:43.533549', '10.0.40.130');
INSERT INTO purchase_states VALUES (7, 6, 'pending', '2018-11-19 15:54:12.926631', '10.0.40.130');

SELECT pg_catalog.setval('purchase_states_purchase_state_id_seq', 6, true);

-- fields (ad_id, action_id, admin_id, review_time, queue, action_type, action, refusal_reason_text, category)
INSERT INTO review_log VALUES (127, 1, 9, '2018-11-19 15:49:53.231279', 'all', 'new', 'accepted', NULL, 2020);
INSERT INTO review_log VALUES (128, 1, 9, '2018-11-19 15:49:56.482817', 'all', 'new', 'accepted', NULL, 6180);
INSERT INTO review_log VALUES (126, 1, 9, '2018-11-19 15:50:05.059267', 'all', 'new', 'accepted', NULL, 1240);

-- fields (ad_id, action_id, state_id, name, value)
INSERT INTO state_params VALUES (126, 1, 710, 'queue', 'normal');
INSERT INTO state_params VALUES (127, 1, 713, 'queue', 'normal');
INSERT INTO state_params VALUES (128, 1, 716, 'queue', 'normal');
INSERT INTO state_params VALUES (127, 1, 717, 'filter_name', 'all');
INSERT INTO state_params VALUES (128, 1, 718, 'filter_name', 'all');
INSERT INTO state_params VALUES (126, 1, 719, 'filter_name', 'all');

SELECT pg_catalog.setval('tokens_token_id_seq', 758, true);
SELECT pg_catalog.setval('trans_queue_id_seq', 118, true);
