CREATE ROLE bwriters;
CREATE ROLE breaders;

-- admins status
CREATE TYPE enum_admins_status AS ENUM ('active', 'deleted');

CREATE TABLE admins (
  admin_id serial,
  username varchar(50) NOT NULL unique,
  passwd varchar(50) NOT NULL,
  fullname varchar(50),
  jabbername varchar(50),
  email varchar(50),
  mobile varchar(50),
  status enum_admins_status NOT NULL DEFAULT 'active',
  PRIMARY KEY (admin_id)
);
GRANT select,insert,delete,update ON admins to bwriters;
GRANT select on admins TO breaders;
GRANT select,insert,delete,update ON admins_admin_id_seq to bwriters;
GRANT select on admins_admin_id_seq TO breaders;

CREATE INDEX index_admins_fullname ON admins (fullname);

CREATE TABLE admin_privs (
  admin_id integer NOT NULL REFERENCES admins (admin_id) ON DELETE CASCADE,
  priv_name varchar(50) NOT NULL,
  PRIMARY KEY (admin_id, priv_name)
);
GRANT select,insert,delete,update ON admin_privs to bwriters;
GRANT select on admin_privs TO breaders;

CREATE INDEX index_admin_privs_ad_id ON admin_privs (admin_id);

-- Stores

CREATE TYPE enum_stores_status AS ENUM (
    'pending_review',
    'active',
    'inactive',
    'deleted',
    'expired',
    'pending',
    'user_deactivated',
    'admin_deactivated'
);

CREATE TABLE stores (
	store_id serial PRIMARY KEY,
    name character varying(100),
    phone character varying(100),
    address character varying(60),
    info_text text,
    status enum_stores_status DEFAULT 'pending'::enum_stores_status NOT NULL,
    date_start timestamp without time zone DEFAULT LOCALTIMESTAMP,
    date_end timestamp without time zone,
    region smallint,
    url character varying(200) UNIQUE,
    hide_phone boolean,
    commune smallint,
    address_number character varying(10),
    website_url character varying(200),
    main_image varchar(20),
	main_image_offset integer,
    logo_image varchar(20) UNIQUE,
    account_id integer
);


CREATE INDEX index_stores_date_start ON stores (date_start);
CREATE INDEX index_stores_date_end ON stores (date_end);
CREATE INDEX index_stores_status ON stores (status);
CREATE INDEX index_stores_account_id ON stores (account_id);

GRANT select,insert,delete,update ON stores TO bwriters;
GRANT select ON stores TO breaders;
GRANT select,insert,delete,update ON stores_store_id_seq to bwriters;
GRANT select on stores_store_id_seq TO breaders;


CREATE TYPE enum_store_media_type AS ENUM ('main_image','logo_image');

CREATE TABLE store_media (
	store_media_id bigint PRIMARY KEY,
	store_id integer,
	upload_time TIMESTAMP without time zone DEFAULT LOCALTIMESTAMP,
	image_type enum_store_media_type NOT NULL DEFAULT 'main_image',
	FOREIGN KEY (store_id) REFERENCES stores (store_id)
);

GRANT select,insert,delete,update ON store_media TO bwriters;
GRANT select ON store_media TO breaders;

CREATE SEQUENCE store_image_id;
GRANT select,insert,delete,update ON store_image_id to bwriters;
GRANT select on store_image_id TO breaders;

CREATE TYPE enum_store_params_name AS ENUM ('import_id', 'import_partner', 'infopage_no_ads', 'image_extra', 'import_type', 'username', 'passwd', 'textcolor', 'bgcolor', 'url_path', 'hide_email', 'free_ads', 'free_ads_deadline', 'import_lock', 'store_type');

CREATE TABLE store_params (
  store_id integer NOT NULL,
  name enum_store_params_name NOT NULL,
  value text NOT NULL,
  PRIMARY KEY (store_id,name),
  FOREIGN KEY (store_id) REFERENCES stores (store_id)
);
CREATE INDEX index_store_params_name_value ON store_params (name, value);
CREATE INDEX index_store_params_store_id ON store_params (store_id);

GRANT select,insert,delete,update ON store_params TO bwriters;
GRANT select ON store_params TO breaders;

CREATE TABLE store_users (
  store_id integer REFERENCES stores (store_id),
  email varchar(100) NOT NULL,
  PRIMARY KEY (store_id, email)
);

GRANT select,insert,delete,update ON store_users TO bwriters;
GRANT select ON store_users TO breaders;

CREATE INDEX index_store_users_store_id ON store_users (store_id);

CREATE TABLE tokens (
  token_id serial,
  token varchar(50) NOT NULL UNIQUE,
  admin_id integer REFERENCES admins (admin_id),
  created_at timestamp default NULL,
  valid_to timestamp default NULL,
  remote_addr varchar(20) NOT NULL,
  info varchar(255),
  store_id integer REFERENCES stores (store_id),
  email varchar(60),
  PRIMARY KEY  (token_id),
 CHECK ((admin_id IS NOT NULL OR store_id IS NOT NULL) AND (admin_id IS NULL OR email IS NULL))
);
GRANT select,insert,delete,update ON tokens to bwriters;
GRANT select on tokens TO breaders;
GRANT select,insert,delete,update ON tokens_token_id_seq to bwriters;
GRANT select on tokens_token_id_seq TO breaders;

CREATE INDEX index_tokens_valid_to ON tokens (valid_to);
CREATE INDEX index_tokens_email ON tokens (email);
CREATE INDEX index_tokens_store_id ON tokens (store_id);
CREATE INDEX index_tokens_admin_id ON tokens (admin_id);

CREATE TABLE store_login_tokens (
	store_login_token_id serial PRIMARY KEY,
	store_id integer NOT NULL REFERENCES stores (store_id),
	email varchar(60) NOT NULL,
	token varchar(50) NOT NULL UNIQUE,
	created_at timestamp NOT NULL default CURRENT_TIMESTAMP,
	valid_to timestamp NOT NULL,
	remote_addr varchar(100) NOT NULL,
	token_id integer REFERENCES tokens (token_id) -- Set when used to login.
);
GRANT select,insert,delete,update ON store_login_tokens to bwriters;
GRANT select on store_login_tokens TO breaders;
GRANT select,insert,delete,update ON store_login_tokens_store_login_token_id_seq to bwriters;
GRANT select on store_login_tokens_store_login_token_id_seq TO breaders;

CREATE INDEX index_store_login_tokens_store_id ON store_login_tokens(store_id);
CREATE INDEX index_store_login_tokens_token_id ON store_login_tokens(token_id);

-- Tables for recording payments.

CREATE SEQUENCE order_id_suffix_seq;
GRANT select,insert,delete,update ON order_id_suffix_seq to bwriters;
GRANT select on order_id_suffix_seq TO breaders;

CREATE TYPE enum_payment_groups_status AS ENUM ('unpaid', 'unverified', 'paid', 'verified', 'cleared');

CREATE TABLE payment_groups (
  payment_group_id serial PRIMARY KEY,
  code varchar(20),
  status enum_payment_groups_status NOT NULL DEFAULT 'unpaid',
  added_at timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  parent_payment_group_id integer REFERENCES payment_groups (payment_group_id)
);
CREATE INDEX index_payment_group_code ON payment_groups(code);
CREATE INDEX index_payment_groups_added_at ON payment_groups(added_at);
CREATE INDEX index_payment_groups_status ON payment_groups (status);
CREATE INDEX index_payment_groups_parent_payment_group_id ON payment_groups(parent_payment_group_id);

GRANT select,insert,delete,update ON payment_groups to bwriters;
GRANT select on payment_groups TO breaders;
GRANT select,insert,delete,update ON payment_groups_payment_group_id_seq to bwriters;
GRANT select on payment_groups_payment_group_id_seq TO breaders;

CREATE TYPE enum_payments_payment_type AS
ENUM (	'ad_action',
	'extra_images',
	'video',
	'voucher_refill',
	'bump',
	'autobump',
	'gallery',
	'gallery_1',
	'gallery_30',
	'upselling_gallery',
	'weekly_bump',
	'upselling_weekly_bump',
	'monthly_store',
	'quarterly_store',
	'biannual_store',
	'annual_store',
	'daily_bump',
	'upselling_daily_bump',
	'label',
	'upselling_label',
	'upselling_standard_cars',
	'upselling_advanced_cars',
	'upselling_premium_cars',
	'upselling_standard_inmo',
	'upselling_advanced_inmo',
	'upselling_premium_inmo',
	'upselling_standard_others',
	'upselling_advanced_others',
	'upselling_premium_others',
	'at_combo1',
	'at_combo2',
	'at_combo3',
	'discount_combo1',
	'discount_combo2',
	'discount_combo3',
	'discount_combo4',
	'promo_bump',
	'if_pack_car_premium',
	'PCAR_001_1',
	'PCAR_005_1',
	'PCAR_010_1',
	'PCAR_020_1',
	'PCAR_030_1',
	'PCAR_040_1',
	'PCAR_050_1',
	'PCAR_075_1',
	'PCAR_100_1',
	'PCAR_125_1',
	'PCAR_001_2',
	'PCAR_005_2',
	'PCAR_010_2',
	'PCAR_020_2',
	'PCAR_030_2',
	'PCAR_040_2',
	'PCAR_050_2',
	'PCAR_075_2',
	'PCAR_100_2',
	'PCAR_125_2',
	'PCAR_001_3',
	'PCAR_005_3',
	'PCAR_010_3',
	'PCAR_020_3',
	'PCAR_030_3',
	'PCAR_040_3',
	'PCAR_050_3',
	'PCAR_075_3',
	'PCAR_100_3',
	'PCAR_125_3',
	'PCAR_001_4',
	'PCAR_005_4',
	'PCAR_010_4',
	'PCAR_020_4',
	'PCAR_030_4',
	'PCAR_040_4',
	'PCAR_050_4',
	'PCAR_075_4',
	'PCAR_100_4',
	'PCAR_125_4',
	'PRES_001_1',
	'PRES_005_1',
	'PRES_010_1',
	'PRES_020_1',
	'PRES_030_1',
	'PRES_040_1',
	'PRES_050_1',
	'PRES_075_1',
	'PRES_100_1',
	'PRES_125_1',
	'PRES_001_2',
	'PRES_005_2',
	'PRES_010_2',
	'PRES_020_2',
	'PRES_030_2',
	'PRES_040_2',
	'PRES_050_2',
	'PRES_075_2',
	'PRES_100_2',
	'PRES_125_2',
	'PRES_001_3',
	'PRES_005_3',
	'PRES_010_3',
	'PRES_020_3',
	'PRES_030_3',
	'PRES_040_3',
	'PRES_050_3',
	'PRES_075_3',
	'PRES_100_3',
	'PRES_125_3',
	'PRES_001_4',
	'PRES_005_4',
	'PRES_010_4',
	'PRES_020_4',
	'PRES_030_4',
	'PRES_040_4',
	'PRES_050_4',
	'PRES_075_4',
	'PRES_100_4',
	'PRES_125_4',
	'credits',
	'autofact',
	'inserting_fee',
	'if_pack_car',
	'if_pack_inmo',
	'RPCAR_010_1',
	'RPCAR_015_1',
	'RPCAR_020_1',
	'RPCAR_025_1',
	'RPCAR_030_1',
	'RPCAR_035_1',
	'RPCAR_040_1',
	'RPCAR_050_1',
	'RPCAR_060_1',
	'RPCAR_075_1',
	'RPCAR_080_1',
	'RPCAR_090_1',
	'RPCAR_100_1',
	'RPCAR_125_1',
	'RPCAR_150_1',
	'RPCAR_200_1',
	'RPRES_010_1',
	'RPRES_015_1',
	'RPRES_020_1',
	'RPRES_025_1',
	'RPRES_030_1',
	'RPRES_035_1',
	'RPRES_040_1',
	'RPRES_045_1',
	'RPRES_050_1',
	'RPRES_060_1',
	'RPRES_070_1',
	'RPRES_075_1',
	'RPRES_080_1',
	'RPRES_090_1',
	'RPRES_100_1',
	'RPRES_125_1',
	'RPRES_135_1',
	'RPRES_150_1',
	'RPRES_200_1'
);

CREATE TABLE payments (
  payment_group_id integer NOT NULL REFERENCES payment_groups (payment_group_id),
  payment_type enum_payments_payment_type NOT NULL,
  pay_amount integer NOT NULL,
  discount integer NOT NULL CHECK (discount = 0 OR discount = pay_amount) DEFAULT 0,
  PRIMARY KEY (payment_group_id, payment_type)
);
GRANT select,insert,delete,update ON payments TO bwriters;
GRANT select ON payments TO breaders;

CREATE INDEX index_payment_group ON payments(payment_group_id);
CREATE INDEX index_payment_payment_type ON payments(payment_type);
CREATE INDEX index_payments_pay_amount ON payments(pay_amount);

CREATE TYPE enum_pay_log_pay_type AS ENUM ('save', 'payphone', 'paycard', 'payvoucher', 'verify', 'adminclear', 'voucher_save', 'voucher_paycard', 'paycard_save', 'campaign', 'free');
CREATE TYPE enum_pay_log_status AS ENUM ('OK', 'ERR', 'LESS', 'MORE', 'SAVE', 'PARENT',
'ERR_NO_CREDIT', 'ERR_DUP_ORDER', 'ERR_COMM_BANK', 'ERR_PAYEX_TRANS',
'ERR_UNK_PAYMETHOD', 'ERR_SIGN_CALL', 'ERR_PAYEX_TECH', 'ERR_CANCELLED', 'ERR_PAYEX_DB',
'ERR_PRICE_INCORRECT','ERR_ORDER_INVALID','ERR_MAC_INVALID');

CREATE TABLE pay_log (
	pay_log_id serial PRIMARY KEY,
	pay_type enum_pay_log_pay_type NOT NULL,
	amount integer NOT NULL default 0,
	code varchar(30),
	token_id integer REFERENCES tokens (token_id),
	paid_at timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP,
	payment_group_id integer REFERENCES payment_groups (payment_group_id),
	status enum_pay_log_status NOT NULL,
		CHECK (CASE
			WHEN status IN ('OK', 'LESS', 'MORE', 'SAVE', 'PARENT') THEN payment_group_id IS NOT NULL
			ELSE pay_type IN ('paycard', 'voucher_paycard') OR payment_group_id IS NULL
		END),
		CHECK (CASE
			WHEN pay_type IN ('payphone', 'payvoucher') THEN amount > 0 OR status = 'PARENT'
			WHEN pay_type IN ('paycard', 'voucher_paycard') THEN CASE WHEN amount > 0
			THEN status IN ('OK', 'ERR', 'LESS', 'MORE') ELSE
				status IN ('PARENT', 'ERR_NO_CREDIT', 'ERR_DUP_ORDER', 'ERR_COMM_BANK',
					'ERR_PAYEX_TRANS', 'ERR_UNK_PAYMETHOD', 'ERR_SIGN_CALL',
					'ERR_PAYEX_TECH', 'ERR_CANCELLED', 'ERR_PAYEX_DB',
					'ERR_PRICE_INCORRECT','ERR_ORDER_INVALID','ERR_MAC_INVALID') END
			WHEN pay_type IN ('verify', 'campaign', 'free') THEN amount = 0
			WHEN pay_type IN ('save', 'voucher_save', 'paycard_save') THEN amount = 0 AND status = 'SAVE'
			ELSE amount = 0 AND token_id IS NOT NULL
		END)
);
CREATE INDEX index_pay_log_status ON pay_log (status);
CREATE INDEX index_pay_log_status_paid_at ON pay_log (status, paid_at);
CREATE INDEX index_pay_log_pay_type ON pay_log (pay_type);
CREATE UNIQUE INDEX unique_pay_log_pay_type_code_paid_at ON pay_log (pay_type, code, paid_at, COALESCE(payment_group_id, 0));
CREATE INDEX index_pay_log_code ON pay_log (code varchar_pattern_ops);
CREATE INDEX index_pay_log_code_1 ON pay_log (code);
CREATE INDEX index_pay_log_paid_at ON pay_log (paid_at);
CREATE INDEX index_pay_log_payment_group_id ON pay_log (payment_group_id);
CREATE INDEX index_pay_log_amount ON pay_log (amount);
CREATE INDEX index_pay_log_token_id ON pay_log (token_id);

GRANT SELECT, INSERT, DELETE, UPDATE ON pay_log TO bwriters;
GRANT SELECT ON pay_log TO breaders;
GRANT SELECT, INSERT, DELETE, UPDATE ON pay_log_pay_log_id_seq TO bwriters;
GRANT SELECT ON pay_log_pay_log_id_seq To breaders;

CREATE TYPE enum_pay_log_references_ref_type AS ENUM ('adphone', 'payphone', 'payex', 'remote_addr',
	'order_id', 'webpay', 'pay_type', 'auth_code',
	'trans_date', 'placements_number', 'placements_type',
	'external_id','external_response','cc_number');

CREATE TABLE pay_log_references (
	pay_log_id integer NOT NULL REFERENCES pay_log (pay_log_id),
	ref_num integer NOT NULL DEFAULT 0,
	ref_type enum_pay_log_references_ref_type NOT NULL,
	reference varchar(60) NOT NULL,
	PRIMARY KEY (pay_log_id, ref_num)
);
GRANT select,insert,delete,update ON pay_log_references TO bwriters;
GRANT select ON pay_log_references TO breaders;

CREATE INDEX index_pay_log_references_ref_type_reference ON pay_log_references (ref_type, reference varchar_pattern_ops);
CREATE INDEX index_pay_log_references_ref_type_reference_1 ON pay_log_references (ref_type, reference);
CREATE INDEX index_pay_log_references_pay_log_id ON pay_log_references (pay_log_id);

CREATE SEQUENCE uid_seq;
GRANT select,insert,delete,update ON uid_seq to bwriters;
GRANT select on uid_seq TO breaders;

CREATE TABLE users (
	user_id serial,
	uid integer not null default 0,
	email varchar(80) not null UNIQUE,
	account integer not null default 0,
	paid_total integer not null default 0,
	PRIMARY KEY (user_id)
);
CREATE INDEX index_users_email_prefix ON users (email varchar_pattern_ops);
CREATE INDEX index_users_uid ON users (uid);
GRANT select,insert,delete,update ON users to bwriters;
GRANT select on users TO breaders;
GRANT select,insert,delete,update ON users_user_id_seq to bwriters;
GRANT select on users_user_id_seq TO breaders;

CREATE TABLE vouchers (
	voucher_id serial PRIMARY KEY,
	balance integer NOT NULL default 0,
	store_id integer NOT NULL UNIQUE REFERENCES stores (store_id)
);
GRANT select,insert,delete,update ON vouchers to bwriters;
GRANT select on vouchers TO breaders;
GRANT select,insert,delete,update ON vouchers_voucher_id_seq to bwriters;
GRANT select on vouchers_voucher_id_seq TO breaders;

CREATE INDEX index_vouchers_store_id ON vouchers(store_id);

CREATE TYPE enum_voucher_actions_action_type AS ENUM ('fill', 'pay', 'adjust');
CREATE TYPE enum_voucher_states_state AS ENUM ('unpaid', 'paid', 'used');
CREATE TYPE enum_voucher_states_transition AS ENUM ('initial', 'pay');

CREATE TABLE voucher_actions (
	voucher_id integer NOT NULL REFERENCES vouchers (voucher_id) ON UPDATE CASCADE,
	voucher_action_id serial NOT NULL,
	action_type enum_voucher_actions_action_type NOT NULL,
	current_state integer,
	state enum_voucher_states_state,
	amount integer NOT NULL,
	payment_group_id integer REFERENCES payment_groups (payment_group_id),
	PRIMARY KEY (voucher_id, voucher_action_id)
);
GRANT select,insert,delete,update ON voucher_actions to bwriters;
GRANT select on voucher_actions TO breaders;
CREATE INDEX index_voucher_actions_payment_group_id ON voucher_actions (payment_group_id);
CREATE INDEX index_voucher_actions_voucher_id ON voucher_actions(voucher_id);

CREATE TABLE voucher_states (
	voucher_id integer NOT NULL,
	voucher_action_id integer NOT NULL,
	voucher_state_id serial NOT NULL,
	state enum_voucher_states_state NOT NULL,
	transition enum_voucher_states_transition NOT NULL,
	timestamp timestamp NOT NULL default CURRENT_TIMESTAMP,
	remote_addr varchar(80),
	token_id integer REFERENCES tokens (token_id),
	resulting_balance integer,
	PRIMARY KEY (voucher_id, voucher_action_id, voucher_state_id),
	FOREIGN KEY (voucher_id, voucher_action_id) REFERENCES voucher_actions (voucher_id, voucher_action_id)
);
GRANT select,insert,delete,update ON voucher_states to bwriters;
GRANT select on voucher_states TO breaders;
GRANT select,insert,delete,update ON voucher_states_voucher_state_id_seq to bwriters;
GRANT select on voucher_states_voucher_state_id_seq TO breaders;

CREATE INDEX index_voucher_states_token_id ON voucher_states (token_id);
CREATE INDEX index_voucher_states_voucher_id_voucher_action_id ON voucher_states(voucher_id, voucher_action_id);

CREATE TYPE enum_ads_status AS ENUM ('inactive', 'unpublished', 'active', 'deleted', 'refused', 'hidden', 'disabled', 'deactivated');
CREATE TYPE enum_ads_type AS ENUM ('sell', 'buy', 'swap', 'let', 'rent');

CREATE TABLE ads (
	ad_id serial PRIMARY KEY,
	list_id integer UNIQUE,
	list_time timestamp default NULL,
	status enum_ads_status NOT NULL default 'inactive',
	type enum_ads_type NOT NULL default 'sell',
	name varchar(100) NOT NULL default '',
	phone varchar(100) NOT NULL default '',
	region smallint NOT NULL,
	city smallint NOT NULL default 0,
	category integer NOT NULL default 0,
	user_id integer NOT NULL default 0 REFERENCES users (user_id),
	passwd varchar(30) default NULL,
	phone_hidden bool NOT NULL default 'f',
	no_salesmen bool NOT NULL default 'f',
	company_ad bool NOT NULL default 'f',
	subject varchar(50) NOT NULL default '',
	body text,
	price bigint default NULL,
	image varchar(20) default NULL,
	infopage text default NULL,
	infopage_title varchar(50) default NULL,
	orig_list_time timestamp default NULL,
	old_price bigint default NULL,
	store_id integer default NULL,
	salted_passwd varchar(100) default NULL,
	modified_at timestamp default NULL,
        lang varchar NOT NULL default 'es'
);

GRANT select,insert,delete,update ON ads to bwriters;
GRANT select on ads TO breaders;
GRANT select,insert,delete,update ON ads_ad_id_seq to bwriters;
GRANT select on ads_ad_id_seq TO breaders;

CREATE INDEX index_ads_category ON ads (category);
CREATE INDEX index_ads_category_region ON ads (category, region);
CREATE INDEX index_ads_company_ad ON ads (company_ad);
CREATE INDEX index_ads_status ON ads (status);
CREATE INDEX index_ads_status_ad_id ON ads (status, ad_id);
CREATE INDEX index_ads_status_list_time ON ads (status, list_time);
CREATE INDEX index_ads_list_time ON ads (list_time);
CREATE INDEX index_ads_user_id ON ads (user_id);
CREATE INDEX index_ads_name ON ads (lower(name));
CREATE INDEX index_ads_passwd ON ads (lower(passwd));
CREATE INDEX index_ads_phone ON ads (phone varchar_pattern_ops);
CREATE INDEX index_ads_phone_repl ON ads (replace(replace(phone, '-', ''), ' ', '') varchar_pattern_ops);
CREATE INDEX index_store_id ON ads (store_id);
CREATE INDEX index_ads_subject ON ads (lower(subject) varchar_pattern_ops);
CREATE INDEX index_ads_price ON ads (price);

CREATE SEQUENCE list_id_seq;
GRANT select,insert,delete,update ON list_id_seq to bwriters;
GRANT select on list_id_seq TO breaders;

CREATE TABLE ad_images (
  ad_id integer NOT NULL,
  seq_no smallint default 0 NOT NULL,
  name varchar(20) NOT NULL default '',
  PRIMARY KEY (ad_id, seq_no),
  FOREIGN KEY (ad_id) REFERENCES ads (ad_id)
);
GRANT select,insert,delete,update ON ad_images to bwriters;
GRANT select on ad_images TO breaders;

CREATE TYPE enum_ad_media_media_type AS ENUM ('image','video');

CREATE TABLE ad_media (
        ad_media_id bigint PRIMARY KEY,
        ad_id INTEGER REFERENCES ads(ad_id),
        seq_no SMALLINT DEFAULT 0 NOT NULL,
        upload_time TIMESTAMP NOT NULL,
	media_type enum_ad_media_media_type NOT NULL DEFAULT 'image',
	width integer,
	height integer,
	length integer,
	CHECK (CASE media_type WHEN 'video' THEN
			width IS NOT NULL AND
			height IS NOT NULL AND
			length IS NOT NULL
		ELSE
			TRUE
		END)
);
CREATE INDEX index_ad_media_ad_id ON ad_media (ad_id);

GRANT select,insert,delete,update ON ad_media TO bwriters;
GRANT select ON ad_media TO breaders;

CREATE SEQUENCE next_image_id;
GRANT select,insert,delete,update ON next_image_id to bwriters;
GRANT select on next_image_id TO breaders;


CREATE TYPE enum_ad_params_name AS ENUM (
	'address',
	'address_number',
	'apartment_type',
	'area',
	'available_weeks',
	'available_weeks_offseason',
	'available_weeks_peakseason',
	'bathrooms',
	'beds',
	'boat_length_feet',
	'boat_length_meters',
	'boat_type',
	'brand',
	'built_year',
	'can_build',
	'capacity',
	'cartype',
	'clothing_size',
	'communes',
	'condition',
	'condominio',
	'contract_type',
	'country',
	'cubiccms',
	'currency',
	'daily_bump',
	'digest',
	'equipment',
	'estate_type',
	'external_ad_id',
	'external_modified_date',
	'ext_code',
	'footwear_gender',
	'footwear_size',
	'footwear_type',
	'fuel',
	'gallery',
	'garage_spaces',
	'gearbox',
	'gender',
	'geoposition',
	'geoposition_is_precise',
	'hide_address',
	'id_orig',
	'image_text1',
	'image_text10',
	'image_text11',
	'image_text12',
	'image_text13',
	'image_text14',
	'image_text15',
	'image_text16',
	'image_text17',
	'image_text18',
	'image_text19',
	'image_text2',
	'image_text20',
	'image_text21',
	'image_text22',
	'image_text23',
	'image_text24',
	'image_text25',
	'image_text26',
	'image_text27',
	'image_text28',
	'image_text29',
	'image_text3',
	'image_text30',
	'image_text31',
	'image_text32',
	'image_text33',
	'image_text34',
	'image_text35',
	'image_text36',
	'image_text37',
	'image_text38',
	'image_text39',
	'image_text4',
	'image_text40',
	'image_text41',
	'image_text42',
	'image_text43',
	'image_text44',
	'image_text45',
	'image_text46',
	'image_text47',
	'image_text48',
	'image_text49',
	'image_text5',
	'image_text50',
	'image_text6',
	'image_text7',
	'image_text8',
	'image_text9',
	'internal_memory',
	'inserting_fee',
	'iptu',
	'irrig_hect',
	'job_category',
	'label',
	'link_type',
	'max_rent',
	'mileage',
	'model',
	'monthly_rent',
	'multi_email',
	'new_realestate',
	'pack_status',
	'package_price',
	'periodic_fee',
	'plates',
	'prev_currency',
	'profession',
	'regdate',
	'rooms',
	'service_type',
	'services',
	'show_type',
	'size',
	'util_size',
	'store',
	'street_id',
	'upselling_daily_bump',
	'upselling_weekly_bump',
	'vehicle_type',
	'version',
	'video_height',
	'video_length',
	'video_name',
	'video_width',
	'weekly_bump',
	'weekly_rent',
	'weekly_rent_offseason',
	'weekly_rent_peakseason',
	'working_day',
	'yearly_rent',
	'zone_type',
	'ad_evaluation_result',
	'ad_evaluation_reason'
);

CREATE TABLE ad_params (
  ad_id integer NOT NULL,
  name enum_ad_params_name NOT NULL,
  value text NOT NULL,
  PRIMARY KEY  (ad_id,name),
  FOREIGN KEY (ad_id) REFERENCES ads (ad_id)
);
GRANT select,insert,delete,update ON ad_params to bwriters;
GRANT select on ad_params TO breaders;

CREATE INDEX index_ad_params_name_value ON ad_params (name, value);
CREATE INDEX index_ad_params_ad_id ON ad_params (ad_id);

CREATE TYPE enum_ad_actions_action_type AS ENUM (
  'new',
  'edit',
  'renew',
  'prolong',
  'delete',
  'extra_images',
  'editrefused',
  'import',
  'adminedit',
  'closed',
  'undo_delete',
  'store_change',
  'video',
  'post_refusal',
  'bump',
  'autobump',
  'smartbump',
  'status_change',
  'half_time_mail',
  'postpone_archive',
  'gallery',
  'gallery_1',
  'gallery_30',
  'remove_gallery',
  'gal_exp_mail',
  'scarface_edit',
  'weekly_bump',
  'account_to_pro',
  'move_to_category',
  'daily_bump',
  'account_assign',
  'upselling_bump',
  'upselling_daily_bump',
  'upselling_weekly_bump',
  'upselling_gallery',
  'upselling_label',
  'remove_upselling',
  'label',
  'remove_label',
  'deactivate','activate',
  'disable',
  'phone_update',
  'upselling_standard_cars',
  'upselling_advanced_cars',
  'upselling_premium_cars',
  'upselling_standard_inmo',
  'upselling_advanced_inmo',
  'upselling_premium_inmo',
  'upselling_standard_others',
  'upselling_advanced_others',
  'upselling_premium_others',
  'update_ad_params',
  'autofact',
  'inserting_fee',
  'pack2if',
  'if_pack_car',
  'if_pack_inmo',
  'at_combo1',
  'at_combo2',
  'at_combo3',
  'discount_combo1',
  'discount_combo2',
  'discount_combo3',
  'discount_combo4',
  'promo_bump',
  'if_pack_car_premium',
  'move_to_region'
);

CREATE TYPE enum_ad_actions_queue AS ENUM (
	'new',
	'normal',
	'abuse',
	'unsolved',
	'technical_error',
	'autoaccept',
	'autorefuse',
	'video',
	'medium',
	'difficult',
	'abuse2',
	'spidered',
	'region',
	'mama_normal',
	'mama_abuse',
	'mama_abuse2',
	'mama_medium',
	'mama_difficult',
	'mama_video',
	'mama_spidered',
	'mm_double_ref',
	'mm_insuftext_ref',
	'mm_rudewords_ref',
	'mm_del_dup_acc',
	'mm_fewchange_acc',
	'mm_mass_acc',
	'mm_forbidwrd_ref',
	'mm_fraud_ref',
	'mama_error',
	'yellow_countries',
	'red_countries',
	'category_change',
	'edit',
	'edit_refused',
	'whitelist',
	'pack',
	'first_da',
	'duplicated',
	'duplicated_active',
	'double_pro_refuse',
	'pro_refuse_changes',
	'duplicated_pro',
	'jobs',
	'jobs_evaders',
	'inmo_evaders',
	'preprocessing',
	'bad_content_pro');

CREATE TABLE ad_actions (
  ad_id integer NOT NULL,
  action_id integer default 0 NOT NULL,
  action_type enum_ad_actions_action_type NOT NULL default 'new',
  current_state integer,
  state varchar(16) not null default 'reg',
  queue enum_ad_actions_queue NOT NULL default 'normal',
  locked_by integer default NULL REFERENCES admins(admin_id),
  locked_until timestamp default NULL,
  payment_group_id integer REFERENCES payment_groups (payment_group_id),
  PRIMARY KEY (ad_id, action_id),
  FOREIGN KEY (ad_id) REFERENCES ads (ad_id)
);

CREATE INDEX index_ad_actions_state_queue ON ad_actions (state, queue);
CREATE INDEX index_ad_actions_queue ON ad_actions (queue);
CREATE INDEX index_ad_actions_state ON ad_actions (state);
CREATE INDEX index_payment_group_id ON ad_actions (payment_group_id);
CREATE INDEX index_ad_actions_action_type ON ad_actions (action_type);
CREATE INDEX index_ad_actions_action_type_ad_id ON ad_actions (action_type, ad_id);
CREATE INDEX index_ad_actions_ad_id ON ad_actions(ad_id);
CREATE INDEX index_ad_actions_locked_by ON ad_actions(locked_by);

GRANT select,insert,delete,update ON ad_actions to bwriters;
GRANT select on ad_actions TO breaders;

--
CREATE TABLE ad_queues (
	ad_id integer NOT NULL,
	action_id integer NOT NULL,
	queue enum_ad_actions_queue NOT NULL default 'normal',
	queued_at timestamp NOT NULL default CURRENT_TIMESTAMP,
	locked_by integer default NULL REFERENCES admins(admin_id),
	locked_until timestamp default NULL,
	PRIMARY KEY  (ad_id, action_id),
	FOREIGN KEY (ad_id, action_id) REFERENCES ad_actions (ad_id, action_id)
);

GRANT select,insert,delete,update ON ad_queues to bwriters;
GRANT select on ad_queues TO breaders;
CREATE INDEX index_ad_queues_queue ON ad_queues (queue);
CREATE INDEX index_ad_queues_queued_at ON ad_queues (queued_at);
CREATE INDEX index_ad_queues_ad_id_action_id ON ad_queues(ad_id, action_id);
CREATE INDEX index_ad_queues_locked_by ON ad_queues(locked_by);

CREATE TYPE enum_action_params_name AS ENUM (
	'TDUID',
	'prev_action_id',
	'adqueue_id',
	'event_mail',
	'event_view',
	'event_galleryview',
	'redir', 'video_new',
	'source', 'cleared_by',
	'campaign_id',
	'deletion_reason',
	'user_text',
	'user_text_testimonial',
	'deletion_timer',
	'pack_result',
	'force_review',
	'label_type',
	'force_queue',
	'service_order',
	'service_amount',
	'ad_evaluation_result',
	'ad_evaluation_reason',
	'ad_evaluation_queue',
	'ad_evaluation_timestamp'
);

CREATE TABLE action_params (
  ad_id integer NOT NULL,
  action_id integer NOT NULL,
  name enum_action_params_name NOT NULL,
  value text NOT NULL,
  PRIMARY KEY (ad_id, action_id, name),
  FOREIGN KEY (ad_id, action_id) REFERENCES ad_actions (ad_id, action_id)
);

GRANT select,insert,delete,update ON action_params to bwriters;
GRANT select on action_params TO breaders;

CREATE INDEX index_action_params_name_value ON action_params (name, value);
CREATE INDEX index_action_params_ad_id_action_id ON action_params (ad_id, action_id);

CREATE TYPE enum_action_states_state AS ENUM (
	'reg',
	'unpaid',
	'unverified',
	'refused',
	'accepted',
	'pending_review',
	'locked',
	'closed',
	'deleted',
	'undo_deleted',
	'hidden',
	'pending_bump',
	'postpone_archive',
	'autoaccept',
	'autorefuse',
	'disabled','deactivated','activated'
);

CREATE TYPE enum_action_states_transition AS ENUM (
	'initial',
	 'paymail',
	 'verifymail',
	 'reminder',
	 'maxtime',
	 'pay',
	 'verify',
	 'adminclear',
	 'checkout',
	 'checkin',
	 'refuse',
	 'accept',
	 'accept_w_chngs',
	 'newqueue',
	 'adminedit',
	 'user_deleted',
	 'admin_deleted',
	 'expired_deleted',
	 'import',
	 'import_deleted',
	 'undo_delete',
	 'comm_err',
	 'cancelled',
	 'store_change',
	 'paycard',
	 'newad',
	 'paymailsent',
	 'bump',
	 'status_change',
	 'pending_pay',
	 'cleared_by',
	 'campaign_clear',
	 'postpone_archive',
	 'scarface_edit',
	 'updating_counter',
	 'user_deactivated',
	 'admin_deactivated',
	 'user_disabled',
	 'admin_disabled',
	 'user_activated',
	 'admin_activated',
	 'auto_deleted',
	 'autobump',
	 'smartbump'
);

CREATE TABLE action_states (
  ad_id integer NOT NULL,
  action_id integer NOT NULL,
  state_id serial UNIQUE NOT NULL,
  state enum_action_states_state NOT NULL default 'reg',
  transition enum_action_states_transition NOT NULL default 'initial',
  timestamp timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  remote_addr varchar(20) default '',
  token_id integer REFERENCES tokens (token_id),
  check(CASE state WHEN 'deleted' THEN transition IN ('user_deleted', 'admin_deleted', 'expired_deleted', 'import_deleted', 'auto_deleted') END),
--XXX restrict transitions to match valid states.
  PRIMARY KEY  (ad_id,action_id,state_id),
  FOREIGN KEY (ad_id, action_id) REFERENCES ad_actions (ad_id, action_id)
);
GRANT select,insert,delete,update ON action_states to bwriters;
GRANT select on action_states TO breaders;
GRANT select,insert,delete,update ON action_states_state_id_seq to bwriters;
GRANT select on action_states_state_id_seq TO breaders;

CREATE INDEX index_action_states_transition_timestamp ON action_states (transition, timestamp);
CREATE INDEX index_action_states_state ON action_states (state);
CREATE INDEX index_remote_addr ON action_states(remote_addr);
CREATE INDEX index_action_states_token_id ON action_states (token_id);
CREATE INDEX index_action_states_state_ad_id ON action_states (state, ad_id);
CREATE INDEX index_action_states_state_timestamp ON action_states (state, "timestamp");
CREATE INDEX index_action_states_ad_id_action_id_state ON action_states (ad_id, action_id, state);
CREATE INDEX index_action_states_state_reg_timestamp ON action_states ("timestamp", (state = 'reg'));
CREATE INDEX index_action_states_ad_id_action_id ON action_states (ad_id, action_id);

CREATE TYPE enum_state_params_name AS ENUM (
	'reason',
	'unsolved',
	'abuse',
	'technical_error',
	'queue',
	'filter_name',
	'refusal_text',
	'mama_info',
	'previous_category',
	'previous_region'
);

CREATE TABLE state_params (
  ad_id integer NOT NULL,
  action_id integer NOT NULL,
  state_id integer NOT NULL,
  name enum_state_params_name NOT NULL,
  value text NOT NULL,
  PRIMARY KEY  (ad_id, action_id, state_id, name),
  FOREIGN KEY (ad_id, action_id, state_id) REFERENCES action_states (ad_id, action_id, state_id)
);
GRANT select,insert,delete,update ON state_params to bwriters;
GRANT select on state_params TO breaders;

CREATE INDEX index_state_params_name_value ON state_params (name, value);
CREATE INDEX index_state_params_ad_id_action_id_state_id ON state_params (ad_id, action_id, state_id);

CREATE TABLE ad_changes (
  ad_id integer NOT NULL,
  action_id integer NOT NULL,
  state_id integer NOT NULL,
  is_param bool NOT NULL,
  column_name varchar(50) NOT NULL,
  old_value text,
  new_value text,
  PRIMARY KEY (ad_id, action_id, state_id, is_param, column_name),
  FOREIGN KEY (ad_id, action_id, state_id) REFERENCES action_states (ad_id, action_id, state_id),
  CHECK (NOT (old_value IS NULL AND new_value IS NULL))
);
GRANT select,insert,delete,update ON ad_changes to bwriters;
GRANT select on ad_changes TO breaders;

CREATE INDEX index_ad_changes_ad_id_action_id_state_id ON ad_changes(ad_id, action_id, state_id);

CREATE TABLE ad_image_changes (
  ad_id integer NOT NULL,
  action_id integer NOT NULL,
  state_id integer NOT NULL,
  seq_no integer NOT NULL,
  name varchar(20),
  is_new bool NOT NULL default 'f',
  PRIMARY KEY (ad_id, action_id, state_id, seq_no),
  FOREIGN KEY (ad_id, action_id, state_id) REFERENCES action_states (ad_id, action_id, state_id),
	CHECK (seq_no = 0 OR name IS NOT NULL)
);
GRANT select,insert,delete,update ON ad_image_changes TO bwriters;
GRANT select ON ad_image_changes TO breaders;

CREATE INDEX index_ad_images_changes_ad_id_action_id_state_id ON ad_image_changes (ad_id, action_id, state_id);

CREATE TABLE ad_media_changes (
  ad_id integer NOT NULL,
  action_id integer NOT NULL,
  state_id integer NOT NULL,
  seq_no integer NOT NULL,
  ad_media_id bigint,
  is_new bool NOT NULL default 'f',
  media_type enum_ad_media_media_type NOT NULL DEFAULT 'image',
  PRIMARY KEY (ad_id, action_id, state_id, seq_no),
  FOREIGN KEY (ad_id, action_id, state_id) REFERENCES action_states (ad_id, action_id, state_id),
	CHECK (seq_no = 0 OR ad_media_id IS NOT NULL)
);
GRANT select,insert,delete,update ON ad_media_changes TO bwriters;
GRANT select ON ad_media_changes TO breaders;

CREATE INDEX index_ad_media_changes_ad_id_action_id_state_id ON ad_media_changes (ad_id, action_id, state_id);


CREATE TABLE filters (
  filter_id serial UNIQUE PRIMARY KEY,
  name varchar(32) NOT NULL UNIQUE,
  tables text NOT NULL default '',
  condition text NOT NULL,
  use_archive bool NOT NULL DEFAULT 'f'
);
GRANT select,insert,delete,update ON filters to bwriters;
GRANT select on filters TO breaders;

-- Redirs stats
CREATE TABLE redir_stats (
  id SERIAL PRIMARY KEY,
  code varchar(255) NOT NULL default '',
  date timestamp NOT NULL,
  first_redir integer NOT NULL default 0,
  repeated_redir integer NOT NULL default 0,
  inserted_ad integer NOT NULL default 0,
  paid_ad integer NOT NULL default 0,
  verified_ad integer NOT NULL default 0,
  approved_ad integer NOT NULL default 0,
  ad_reply integer NOT NULL default 0,
  pageviews integer NOT NULL default 0
);
GRANT select,insert,delete,update ON redir_stats to bwriters;
GRANT select on redir_stats TO breaders;
GRANT select,insert,delete,update ON redir_stats_id_seq to bwriters;
GRANT select on redir_stats_id_seq TO breaders;

CREATE INDEX index_redir_stats_code ON redir_stats (code);
CREATE INDEX index_redir_stats_date ON redir_stats (date);
CREATE UNIQUE INDEX index_redir_stats_code_date ON redir_stats (code, date);

CREATE SEQUENCE trans_queue_id_seq;

CREATE TABLE trans_queue (
  trans_queue_id integer PRIMARY KEY default nextval('trans_queue_id_seq'),
  added_at timestamp NOT NULL,
  added_by varchar(80) NOT NULL,
  execute_at timestamp DEFAULT NULL,
  executed_at timestamp default NULL,
  executed_by varchar(80) default NULL,
  status varchar(80) default NULL, -- XXX not null?
  locked_at timestamp default NULL,
  locked_by varchar(80) default NULL,
  command text NOT NULL,
  response text,
  queue varchar(30) NOT NULL,
  sub_queue varchar(30) DEFAULT NULL,
  info varchar(60) DEFAULT NULL
);

GRANT select,insert,delete,update ON trans_queue to bwriters;
GRANT select on trans_queue TO breaders;
GRANT select,insert,delete,update ON trans_queue_id_seq to bwriters;
GRANT select on trans_queue_id_seq TO breaders;

CREATE INDEX index_trans_queue_status ON trans_queue (status);
CREATE INDEX index_trans_queue_command ON trans_queue (command text_pattern_ops);
CREATE INDEX index_trans_queue_executed_at ON trans_queue (executed_at);
CREATE INDEX index_trans_queue_queue_null_executed_at ON trans_queue (queue, (executed_at IS NULL));
CREATE INDEX index_trans_queue_locked_at ON trans_queue (locked_at);
CREATE INDEX index_trans_queue_at_nullstats_execute_at ON trans_queue (execute_at) WHERE status IS NULL and queue = 'AT';
CREATE INDEX index_trans_queue_at_unrun_sub_queue_info ON trans_queue (sub_queue, info) WHERE queue = 'AT' AND status IS NULL;

CREATE TYPE enum_user_params_name AS ENUM ('email', 'phone', 'refusal_divide_credit', 'first_approved_ad', 'pre_first_inserted_ad', 'first_inserted_ad');

CREATE TABLE user_params (
  user_id integer NOT NULL REFERENCES users (user_id),
  name enum_user_params_name NOT NULL default 'email',
  value varchar(200) NOT NULL default '',
  PRIMARY KEY (user_id,name)
);
GRANT select,insert,delete,update ON user_params to bwriters;
GRANT select on user_params TO breaders;

CREATE INDEX index_user_params_user_id ON user_params (user_id);

--
-- Table structure for table `ad_codes`
--

CREATE TYPE enum_ad_codes_code_type AS ENUM ('pay', 'verify', 'adwatch');

CREATE TABLE ad_codes (
  code bigint default NULL,
  code_type enum_ad_codes_code_type NOT NULL,
  PRIMARY KEY  (code, code_type)
);
GRANT select,insert,delete,update ON ad_codes to bwriters;
GRANT select on ad_codes TO breaders;
CREATE INDEX index_ad_codes_code ON ad_codes (code);

CREATE SEQUENCE pay_code_seq;
CREATE SEQUENCE verify_code_seq;
CREATE SEQUENCE adwatch_code_seq;

GRANT select,insert,delete,update ON pay_code_seq to bwriters;
GRANT select on pay_code_seq TO breaders;

GRANT select,insert,delete,update ON verify_code_seq to bwriters;
GRANT select on verify_code_seq TO breaders;

GRANT SELECT,INSERT,DELETE,UPDATE ON adwatch_code_seq to bwriters;
GRANT SELECT ON adwatch_code_seq TO breaders;

CREATE TABLE example (
  id integer PRIMARY KEY,
  col integer default 0
);
GRANT select,insert,delete,update ON example to bwriters;
GRANT select on example TO breaders;


--
-- Table for parameters holding mail.
--
CREATE TABLE hold_mail_params (
  id serial NOT NULL,
  name varchar(255) NOT NULL,
  value text NOT NULL,
  PRIMARY KEY  (id)
);
GRANT select,insert,delete,update ON hold_mail_params to bwriters;
GRANT select on hold_mail_params TO breaders;
GRANT select,insert,delete,update ON hold_mail_params_id_seq to bwriters;
GRANT select on hold_mail_params_id_seq TO breaders;

--
-- Table structure for table `notices`
--

CREATE TABLE notices (
  notice_id serial PRIMARY KEY,
  body text NOT NULL,
  uid integer default NULL UNIQUE,
  user_id integer default NULL UNIQUE REFERENCES users(user_id),
  ad_id integer default NULL UNIQUE REFERENCES ads(ad_id) UNIQUE,
  abuse bool NOT NULL DEFAULT 'f',
  created_at timestamp NOT NULL default CURRENT_TIMESTAMP,
  token_id integer NOT NULL REFERENCES tokens(token_id),
  CHECK (ad_id IS NOT NULL OR uid IS NOT NULL OR user_id IS NOT NULL AND
	CASE
		WHEN uid IS NOT NULL THEN ad_id IS NULL AND user_id IS NULL
		WHEN user_id IS NOT NULL THEN ad_id IS NULL AND uid IS NULL
		WHEN ad_id IS NOT NULL THEN uid IS NULL AND user_id IS NULL AND abuse != 't'
	END)
);
GRANT select,insert,delete,update ON notices to bwriters;
GRANT select on notices TO breaders;
GRANT select,insert,delete,update ON notices_notice_id_seq to bwriters;
GRANT select on notices_notice_id_seq TO breaders;

CREATE INDEX index_notices_token_id ON notices (token_id);
CREATE INDEX index_notices_user_id ON notices(user_id);
CREATE INDEX index_notices_ad_id ON notices(ad_id);

-- All views have been moved to scripts/db/views

CREATE TABLE conf (
  key varchar(255) PRIMARY KEY,
  value text default NULL,
  modified_at timestamp NOT NULL default CURRENT_TIMESTAMP,
  token_id integer REFERENCES tokens(token_id)
);
GRANT select,insert,delete,update ON conf TO bwriters;
GRANT select ON conf TO breaders;

CREATE INDEX index_conf_token_id ON conf(token_id);

--
-- mail_log
--

CREATE TYPE enum_mail_log_mail_type AS ENUM ('tip', 'password', 'adreply', 'support', 'adreply_cc', 'store_pwd', 'storereply', 'storereply_cc', 'factory_mail');

CREATE TABLE mail_log (
	mail_log_id serial NOT NULL PRIMARY KEY,
	mail_type enum_mail_log_mail_type NOT NULL,
	sent_at timestamp NOT NULL default CURRENT_TIMESTAMP,
	email varchar(200),
	remote_addr varchar(100),
	list_id integer
);
CREATE INDEX index_mail_log_mail_type ON mail_log (mail_type);
CREATE INDEX index_mail_log_sent_at ON mail_log (sent_at);
CREATE INDEX index_mail_log_email_sent_at ON mail_log (email, sent_at);
CREATE INDEX mail_log_mail_type_remote_addr ON mail_log (mail_type, remote_addr);
GRANT select,insert,delete,update ON mail_log TO bwriters;
GRANT select ON mail_log TO breaders;
GRANT select,insert,delete,update ON mail_log_mail_log_id_seq TO bwriters;
GRANT select ON mail_log_mail_log_id_seq TO breaders;

--
-- Tracable Event logging
--
CREATE TABLE event_log (
	event_id serial NOT NULL PRIMARY KEY,
	event_name varchar(20) NOT NULL,
	event text NOT NULL,
	token_id integer REFERENCES tokens (token_id),
	time timestamp NOT NULL
);

GRANT select,insert,delete,update ON event_log to bwriters;
GRANT select on event_log TO breaders;
GRANT select,insert,delete,update ON event_log_event_id_seq TO bwriters;
GRANT select ON event_log_event_id_seq TO breaders;

CREATE INDEX index_event_log_event_name ON event_log (event_name);
CREATE INDEX index_event_log_time ON event_log (time);
CREATE INDEX index_event_log_event_substring ON event_log (substring(event FROM '^[0-9]+'));
CREATE INDEX index_event_log_token_id ON event_log(token_id);

--
-- Blocked lists and objects
--
CREATE TYPE enum_block_lists_list_type AS ENUM ('ip', 'email', 'category', 'general', 'country', 'refusal_reason');

CREATE TABLE block_lists (
	list_id serial NOT NULL PRIMARY KEY,
	list_name varchar(128) NOT NULL,
	base_list bool NOT NULL DEFAULT 'f',
	list_type enum_block_lists_list_type NOT NULL DEFAULT 'general',
	ignore varchar(20)
);

GRANT select,insert,delete,update ON block_lists to bwriters;
GRANT select on block_lists TO breaders;
GRANT select,insert,delete,update ON block_lists_list_id_seq TO bwriters;
GRANT select ON block_lists_list_id_seq TO breaders;

CREATE TABLE blocked_items (
	item_id serial NOT NULL PRIMARY KEY,
	value varchar(128) NOT NULL,
	list_id integer REFERENCES block_lists (list_id),
	notice text,
	token_id integer REFERENCES tokens (token_id)
);

GRANT select,insert,delete,update ON blocked_items to bwriters;
GRANT select on blocked_items TO breaders;
GRANT select,insert,delete,update ON blocked_items_item_id_seq TO bwriters;
GRANT select ON blocked_items_item_id_seq TO breaders;

CREATE INDEX index_blocked_items_list_id ON blocked_items (list_id);
CREATE INDEX index_blocked_items_token_id ON blocked_items(token_id);

CREATE TYPE enum_block_rules_action AS ENUM ('delete', 'spamfilter', 'stop', 'move_to_queue');
CREATE TYPE enum_block_rules_application AS ENUM ('adreply', 'newad', 'sendtip', 'storereply', 'clear', 'ad_evaluation');
CREATE TYPE enum_block_rules_combine_rule AS ENUM ('and', 'or');

CREATE TABLE block_rules (
	rule_id serial NOT NULL PRIMARY KEY,
	rule_name varchar(128) NOT NULL,
	action enum_block_rules_action NOT NULL,
	application enum_block_rules_application NOT NULL,
	combine_rule enum_block_rules_combine_rule NOT NULL,
	count_yesterday integer NOT NULL DEFAULT 0,
	count_today integer NOT NULL DEFAULT 0,
	count_date date NOT NULL DEFAULT CURRENT_DATE,
	target enum_ad_actions_queue default NULL,
	CHECK (CASE application
		WHEN 'newad' THEN action IN ('stop')
		WHEN 'clear' THEN action IN ('move_to_queue')
		WHEN 'ad_evaluation' THEN action IN ('move_to_queue')
		ELSE action IN ('spamfilter', 'delete')
	END),
	CONSTRAINT block_rules_check_1 CHECK (CASE action
		WHEN 'move_to_queue' THEN target IS NOT NULL
		ELSE target IS NULL
	END)
);

GRANT select,insert,delete,update ON block_rules TO bwriters;
GRANT select on block_rules TO breaders;
GRANT select,insert,delete,update ON block_rules_rule_id_seq TO bwriters;
GRANT select ON block_rules_rule_id_seq TO breaders;

CREATE TABLE block_rule_conditions (
	condition_id serial NOT NULL PRIMARY KEY,
	rule_id integer NOT NULL REFERENCES block_rules (rule_id),
	list_id integer NOT NULL REFERENCES block_lists (list_id),
	whole_word integer NOT NULL DEFAULT 0,
	fields varchar(50)[] NOT NULL
);

GRANT select,insert,delete,update ON block_rule_conditions TO bwriters;
GRANT select on block_rule_conditions TO breaders;
GRANT select,insert,delete,update ON block_rule_conditions_condition_id_seq TO bwriters;
GRANT select ON block_rule_conditions_condition_id_seq TO breaders;

CREATE INDEX index_block_rule_conditions_rule_id ON block_rule_conditions(rule_id);
CREATE INDEX index_block_rule_conditions_list_id ON block_rule_conditions(list_id);

CREATE TYPE enum_mail_queue_state AS ENUM ('blocked', 'deleted', 'manual', 'reg', 'sent', 'abuse');

CREATE TABLE mail_queue (
	mail_queue_id serial NOT NULL PRIMARY KEY,
	state enum_mail_queue_state NOT NULL,
	template_name varchar(60) NOT NULL,
	added_at timestamp NOT NULL default CURRENT_TIMESTAMP,
	remote_addr varchar(100) NOT NULL,
	sender_email varchar(200) NOT NULL,
	sender_name varchar(200) NOT NULL,
	receipient_email varchar(200) NOT NULL,
	receipient_name varchar(100) NOT NULL,
	subject varchar(100) NOT NULL,
	body text NOT NULL,
	list_id integer,
	rule_id	integer default NULL,
	reason varchar(255) default NULL,
	sender_phone varchar(100) default NULL
);
CREATE INDEX mail_queue_remote_addr_state ON mail_queue (remote_addr, state);

CREATE INDEX index_mail_queue_added_at ON mail_queue (added_at);
CREATE INDEX index_mail_queue_sender_name ON mail_queue (lower(sender_name) varchar_pattern_ops);
CREATE INDEX index_mail_queue_sender_email ON mail_queue (lower(sender_email) varchar_pattern_ops);
CREATE INDEX index_mail_queue_receipient_name ON mail_queue (lower(receipient_name) varchar_pattern_ops);
CREATE INDEX index_mail_queue_receipient_email ON mail_queue (lower(receipient_email) varchar_pattern_ops);
CREATE INDEX index_mail_queue_state_spam ON mail_queue (state)  WHERE state = ANY (ARRAY['manual'::enum_mail_queue_state, 'abuse'::enum_mail_queue_state]);

CREATE INDEX mail_queue_list_id ON mail_queue (list_id);

GRANT select,insert,delete,update ON mail_queue TO bwriters;
GRANT select ON mail_queue TO breaders;
GRANT select,insert,delete,update ON mail_queue_mail_queue_id_seq TO bwriters;
GRANT select ON mail_queue_mail_queue_id_seq TO breaders;

---
--- Table for to see who is on_call
---

CREATE TABLE on_call (
	on_call_id serial NOT NULL PRIMARY KEY,
	admin_id integer NOT NULL REFERENCES admins (admin_id),
	start_date timestamp NOT NULL,
	end_date timestamp NOT NULL
);
GRANT select,insert,delete,update ON on_call TO bwriters;
GRANT select on on_call TO breaders;
GRANT select,insert,delete,update ON on_call_on_call_id_seq TO bwriters;
GRANT select ON on_call_on_call_id_seq TO breaders;

CREATE INDEX index_on_call_admin_id ON on_call(admin_id);

---
--- on_call_actions specifies the event reports from on_call
---

CREATE TABLE on_call_actions (
	action_id serial NOT NULL PRIMARY KEY,
	admin_id integer NOT NULL REFERENCES admins (admin_id),
	start_time timestamp NOT NULL,
	time_spent interval NOT NULL CHECK (time_spent >= INTERVAL '0'),
	subject varchar(100) NOT NULL,
	body text NOT NULL,
	token_id integer REFERENCES tokens (token_id),
	faq bool NOT NULL DEFAULT FALSE
);
GRANT select,insert,delete,update ON on_call_actions TO bwriters;
GRANT select on on_call_actions TO breaders;
GRANT select,insert,delete,update ON on_call_actions_action_id_seq TO bwriters;
GRANT select ON on_call_actions_action_id_seq TO breaders;

CREATE INDEX index_on_call_actions_admin_id ON on_call_actions(admin_id);
CREATE INDEX index_on_call_actions_token_id ON on_call_actions(token_id);
CREATE INDEX index_on_call_actions_faq ON on_call_actions (faq);

---
--- Pricelist
---

CREATE TABLE pricelist (
	list varchar(20) NOT NULL,
	item varchar(20) NOT NULL,
	price integer NOT NULL,
	PRIMARY KEY (list, item)
);

GRANT select,insert,delete,update ON pricelist TO bwriters;
GRANT select on pricelist TO breaders;

---
--- Bids
---
CREATE TABLE bid_ads (
	bid_ad_id serial NOT NULL PRIMARY KEY,
	campaign_id integer NOT NULL,
	list_time timestamp NOT NULL,
	end_time timestamp DEFAULT NULL,
	category integer DEFAULT NULL,
	name varchar (100) NOT NULL,
	subject varchar (100) NOT NULL,
	body text NOT NULL
);

CREATE INDEX index_bid_ads_campaign_id ON bid_ads (campaign_id);
GRANT SELECT on bid_ads_bid_ad_id_seq TO breaders;
GRANT SELECT,UPDATE on bid_ads_bid_ad_id_seq TO bwriters;
GRANT SELECT ON bid_ads TO breaders;
GRANT SELECT,INSERT,DELETE,UPDATE ON bid_ads TO bwriters;

CREATE TYPE enum_bid_media_media_type AS ENUM ('image', 'video');

CREATE TABLE bid_media (
	media_id bigint NOT NULL PRIMARY KEY,
	bid_ad_id integer  REFERENCES bid_ads(bid_ad_id),
	seq_no smallint default 0 NOT NULL,
	media_type enum_bid_media_media_type NOT NULL
);

CREATE INDEX index_bid_media_bid_ad_id_seq_no ON bid_media (bid_ad_id, seq_no);
GRANT SELECT ON bid_media TO breaders;
GRANT SELECT,INSERT,DELETE,UPDATE ON bid_media TO bwriters;

CREATE INDEX index_bid_media_bid_ad_id ON bid_media(bid_ad_id);

CREATE TYPE enum_bid_bids_status AS ENUM ('inactive', 'active', 'refused');

CREATE TABLE bid_bids (
	bid_id serial NOT NULL PRIMARY KEY,
	bid_ad_id integer NOT NULL REFERENCES bid_ads(bid_ad_id),
	bid integer NOT NULL,
	bid_time timestamp NOT NULL,
	name varchar (100) NOT NULL,
	company varchar (100),
	email varchar (100) NOT NULL,
	phone varchar (20) NOT NULL,
	ssecnr varchar (13) NOT NULL,
	status enum_bid_bids_status NOT NULL,
	remote_addr varchar(20) NOT NULL,
	token_id integer REFERENCES tokens (token_id)
);

GRANT SELECT ON bid_bids_bid_id_seq TO breaders;
GRANT SELECT,UPDATE ON bid_bids_bid_id_seq TO bwriters;
GRANT SELECT ON bid_bids TO breaders;
GRANT SELECT,INSERT,DELETE,UPDATE ON bid_bids TO bwriters;

CREATE INDEX index_bid_bids_bid_ad_id ON bid_bids(bid_ad_id);
CREATE INDEX index_bid_bids_token_id ON bid_bids(token_id);

CREATE TYPE bid_type AS (
	name varchar (100),
	company varchar (100),
	bid integer,
	bid_time timestamp
);

-- Table for aggregating daily ad statistics
CREATE TABLE stats_daily_ad_actions (
	stat_id serial NOT NULL PRIMARY KEY,
	stat_time date NOT NULL,
	action_type varchar(16),
	transition varchar(16),
	state varchar(16),
	pay_log_status varchar(20),
	pay_type varchar(20),
	payment_type varchar(16),
	category integer,
	ad_type varchar(8),
	company_ad bool,
	event_count integer NOT NULL,
	amount real NOT NULL,
	info varchar(30) NOT NULL
);

GRANT SELECT ON stats_daily_ad_actions_stat_id_seq TO breaders;
GRANT SELECT,INSERT,DELETE,UPDATE ON stats_daily_ad_actions_stat_id_seq TO bwriters;
GRANT SELECT ON stats_daily_ad_actions TO breaders;
GRANT SELECT,INSERT,DELETE,UPDATE ON stats_daily_ad_actions TO bwriters;

CREATE INDEX index_stats_daily_ad_actions_actions_type ON stats_daily_ad_actions (action_type);
CREATE INDEX index_stats_daily_ad_actions_transition ON stats_daily_ad_actions (transition);
CREATE INDEX index_stats_daily_ad_actions_state ON stats_daily_ad_actions (state);
CREATE INDEX index_stats_daily_ad_actions_pay_log_status ON stats_daily_ad_actions (pay_log_status);
CREATE INDEX index_stats_daily_ad_actions_pay_type ON stats_daily_ad_actions (pay_type);
CREATE INDEX index_stats_daily_ad_actions_payment_type ON stats_daily_ad_actions (payment_type);
CREATE INDEX index_stats_daily_ad_actions_category ON stats_daily_ad_actions (category);
CREATE INDEX index_stats_daily_ad_actions_ad_type ON stats_daily_ad_actions (ad_type);
CREATE INDEX index_stats_daily_ad_actions_company_ad ON stats_daily_ad_actions (company_ad);


CREATE TABLE stats_daily (
	stat_time date NOT NULL,
	name varchar(50),
	value text,
	PRIMARY KEY (stat_time,name)
);

GRANT SELECT ON stats_daily TO breaders;
GRANT SELECT,INSERT,DELETE,UPDATE ON stats_daily TO bwriters;


CREATE TABLE stats_hourly (
	stat_time timestamp NOT NULL CHECK (date_trunc('hour', stat_time) = stat_time),
	name varchar,
	value text,
	PRIMARY KEY (stat_time,name)
);

GRANT SELECT ON stats_hourly TO breaders;
GRANT SELECT,INSERT,DELETE,UPDATE ON stats_hourly TO bwriters;

CREATE TYPE enum_store_actions_action_type AS ENUM ('new', 'edit', 'bump', 'status_change', 'spackage_start', 'spackage_stop', 'extend');
CREATE TYPE enum_store_action_states_state AS ENUM ('reg', 'unpaid', 'accepted', 'paid');

CREATE TABLE store_actions (
	store_id integer NOT NULL,
	action_id integer NOT NULL,
	action_type enum_store_actions_action_type NOT NULL default 'new',
	current_state integer,
	state enum_store_action_states_state not null default 'reg',
	payment_group_id integer,

	PRIMARY KEY (store_id,action_id),
	FOREIGN KEY (store_id) REFERENCES stores (store_id),
	FOREIGN KEY (payment_group_id) REFERENCES payment_groups (payment_group_id)
);
GRANT select,insert,delete,update ON store_actions to bwriters;
GRANT select on store_actions TO breaders;

CREATE INDEX index_store_payment_group_id ON store_actions (payment_group_id);
CREATE INDEX index_store_actions_store_id ON store_actions (store_id);
CREATE INDEX index_store_actions_payment_group_id ON store_actions (payment_group_id);

CREATE TABLE store_action_states (
	store_id integer NOT NULL,
	action_id integer NOT NULL,
	state_id serial UNIQUE NOT NULL,
	state enum_store_action_states_state NOT NULL default 'reg',
	timestamp timestamp NOT NULL,
	token_id integer REFERENCES tokens (token_id),

	PRIMARY KEY (store_id,action_id,state_id),
	FOREIGN KEY (store_id,action_id) REFERENCES store_actions (store_id,action_id)
);
GRANT select,insert,delete,update ON store_action_states to bwriters;
GRANT select on store_action_states TO breaders;
GRANT select,insert,delete,update ON store_action_states_state_id_seq to bwriters;
GRANT select on store_action_states_state_id_seq TO breaders;
CREATE INDEX index_store_action_states_state ON store_action_states (state);
CREATE INDEX index_store_action_states_token_id ON store_action_states (token_id);
CREATE INDEX index_store_action_states_state_store_id ON store_action_states (state, store_id);
CREATE INDEX index_store_action_states_state_timestamp ON store_action_states (state, "timestamp");
CREATE INDEX index_store_action_states_store_id_action_id_state ON store_action_states (store_id, action_id, state);
CREATE INDEX index_store_action_states_state_reg_timestamp ON store_action_states ("timestamp", (state = 'reg'));
CREATE INDEX index_store_action_states_store_id_action_id ON store_action_states (store_id, action_id);

CREATE TABLE store_changes (
	store_id integer NOT NULL,
	action_id integer NOT NULL,
	state_id integer NOT NULL,
	is_param bool NOT NULL,
	column_name varchar(50) NOT NULL,
	old_value text,
	new_value text,
	PRIMARY KEY (store_id, action_id, state_id, is_param, column_name),
	FOREIGN KEY (store_id, action_id, state_id) REFERENCES store_action_states (store_id, action_id, state_id),
	CHECK (NOT (old_value IS NULL AND new_value IS NULL))
	);
GRANT select,insert,delete,update ON store_changes to bwriters;
GRANT select on store_changes TO breaders;

CREATE INDEX index_store_changes_store_id_action_id_state_id ON store_changes (store_id, action_id, state_id);

CREATE TYPE enum_sms_users_status AS ENUM ('active', 'paused', 'adminpaused');

-- Sms users
CREATE TABLE sms_users (
	sms_user_id serial PRIMARY KEY,
	phone varchar(14) UNIQUE NOT NULL,
	sms_central varchar(12) NOT NULL,
	credit numeric(10,2) NOT NULL,
	credit_total numeric(10,2) NOT NULL,
	status enum_sms_users_status NOT NULL DEFAULT 'paused',
	free_sms integer NOT NULL DEFAULT 0
);

CREATE INDEX index_sms_users_phone ON sms_users (phone);

GRANT SELECT, INSERT, DELETE, UPDATE ON sms_users TO bwriters;
GRANT SELECT ON sms_users TO breaders;
GRANT SELECT, INSERT, DELETE, UPDATE ON sms_users_sms_user_id_seq TO bwriters;
GRANT SELECT ON sms_users_sms_user_id_seq To breaders;

-- Watch users

CREATE TABLE watch_users (
	watch_user_id serial PRIMARY KEY,
	watch_unique_id char(36) NOT NULL UNIQUE,
	last_activity timestamp NOT NULL default CURRENT_TIMESTAMP
);

GRANT SELECT ON watch_users TO breaders;
GRANT SELECT,INSERT,DELETE,UPDATE ON watch_users TO bwriters;
GRANT SELECT ON watch_users_watch_user_id_seq TO breaders;
GRANT SELECT,UPDATE ON watch_users_watch_user_id_seq TO bwriters;

CREATE INDEX index_watch_users_watch_unique_id ON watch_users(watch_unique_id);
CREATE INDEX index_watch_users_last_activity ON watch_users(last_activity);

-- Sms log

CREATE TYPE enum_sms_log_sms_class AS ENUM ('start','stop','paused','alert');
CREATE TYPE enum_sms_log_sms_type AS ENUM ('save', 'ok', 'error', 'mo');

CREATE TABLE sms_log (
	sms_log_id serial PRIMARY KEY,
	sms_user_id integer NOT NULL REFERENCES sms_users (sms_user_id),
	session_id varchar(30) NOT NULL,
	sms_type enum_sms_log_sms_type NOT NULL,
	cost numeric(10, 2) NOT NULL,
	logged_at timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP,
	mobile_originated bool NOT NULL DEFAULT 'f',
	message varchar DEFAULT NULL,
	sms_class enum_sms_log_sms_class DEFAULT NULL,
	status_code integer NOT NULL,
	discount numeric(10, 2) NOT NULL default 0
);

CREATE INDEX index_sms_log_sms_log_id ON sms_log (sms_log_id);
CREATE INDEX index_sms_log_sms_user_id ON sms_log (sms_user_id);
CREATE INDEX index_sms_log_session_id ON sms_log (session_id);
CREATE UNIQUE INDEX index_sms_log_sms_type_session_id ON sms_log (sms_type, session_id);

GRANT SELECT, INSERT, DELETE, UPDATE ON sms_log TO bwriters;
GRANT SELECT ON sms_log TO breaders;
GRANT SELECT, INSERT, DELETE, UPDATE ON sms_log_sms_log_id_seq TO bwriters;
GRANT SELECT ON sms_log_sms_log_id_seq To breaders;

-- Watch queries

CREATE TYPE enum_watch_queries_sms_status AS ENUM ('active','paused');

CREATE TABLE watch_queries (
	watch_user_id integer NOT NULL REFERENCES watch_users (watch_user_id),
	watch_query_id integer NOT NULL,
	color_id integer NOT NULL,
	query_string text NOT NULL,
	last_view timestamp NOT NULL default CURRENT_TIMESTAMP,
	sms_user_id integer REFERENCES sms_users (sms_user_id) default NULL,
	sms_activation_code integer default NULL,
	sms_activation_expiry timestamp default NULL,
	sms_status enum_watch_queries_sms_status DEFAULT NULL,
	PRIMARY KEY (watch_user_id, watch_query_id),
	CHECK (sms_user_id IS NULL = sms_status IS NULL)
);
GRANT SELECT ON watch_queries TO breaders;
GRANT SELECT,INSERT,DELETE,UPDATE ON watch_queries TO bwriters;
CREATE INDEX index_watch_queries_sms_activation_code ON watch_queries (sms_activation_code);
CREATE INDEX index_watch_queries_watch_user_id ON watch_queries(watch_user_id);
CREATE INDEX index_watch_queries_sms_user_id ON watch_queries(sms_user_id);

-- sms_log "subclass"

CREATE TABLE sms_log_watch (
	sms_log_id integer PRIMARY KEY REFERENCES sms_log (sms_log_id),
	list_id integer,
	query_string text NOT NULL,
	watch_user_id integer,
	watch_query_id integer,
	FOREIGN KEY(watch_user_id, watch_query_id) REFERENCES watch_queries MATCH FULL ON DELETE SET NULL
);

CREATE INDEX index_sms_log_watch_watch_user_id_watch_query_id ON sms_log_watch (watch_user_id, watch_query_id);

GRANT SELECT, INSERT, DELETE, UPDATE ON sms_log_watch TO bwriters;
GRANT SELECT ON sms_log_watch TO breaders;


-- Watch ads

CREATE TABLE watch_ads (
	watch_user_id integer NOT NULL REFERENCES watch_users (watch_user_id),
	list_id integer NOT NULL REFERENCES ads (list_id)
		ON DELETE CASCADE,
	PRIMARY KEY (watch_user_id, list_id)
);
GRANT SELECT ON watch_ads TO breaders;
GRANT SELECT,INSERT,DELETE,UPDATE ON watch_ads TO bwriters;

CREATE INDEX index_watch_ads_watch_user_id ON watch_ads(watch_user_id);
CREATE INDEX index_watch_ads_list_id ON watch_ads(list_id);

--
-- Abuse Handling Tool
--

CREATE SEQUENCE reporter_id_seq;
CREATE TABLE abuse_reporters (
	uid INTEGER PRIMARY KEY NOT NULL default 0,
	accepted_reports INTEGER NOT NULL default 0,
	rejected_reports INTEGER NOT NULL default 0
);
GRANT select,insert,delete,update ON abuse_reporters TO bwriters;
GRANT select on abuse_reporters TO breaders;

CREATE SEQUENCE report_id_seq;
CREATE TABLE abuse_reports (
	report_id INTEGER PRIMARY KEY default nextval('report_id_seq'),
	ad_id INTEGER NOT NULL,
	user_id INTEGER REFERENCES users (user_id),
	reporter_id INTEGER REFERENCES abuse_reporters(uid) NOT NULL,
	report_type VARCHAR(15) NOT NULL,
	text TEXT,
	status VARCHAR(15) NOT NULL default 'unsolved',
	resolution VARCHAR(32),
        lang varchar NOT NULL default 'es',
	admin_id INTEGER  REFERENCES admins (admin_id),
	date_insert TIMESTAMP default NOW()
);

GRANT select,insert,delete,update ON abuse_reports TO bwriters;
GRANT select on abuse_reports TO breaders;
GRANT select,insert,delete,update ON report_id_seq to bwriters;
GRANT select on report_id_seq TO breaders;

CREATE SEQUENCE abuse_locks_id_seq;
CREATE TABLE abuse_locks (
	abuse_locks_id INTEGER PRIMARY KEY default nextval('abuse_locks_id_seq'),
	ad_id INTEGER NOT NULL,
	locked_until TIMESTAMP NOT NULL,
	admin_id INTEGER  REFERENCES admins (admin_id)
);
GRANT select,insert,delete,update ON abuse_locks TO bwriters;
GRANT select on abuse_locks TO breaders;
GRANT select,insert,delete,update ON abuse_locks_id_seq to bwriters;
GRANT select on abuse_locks_id_seq TO breaders;

---
--- MAMA Synonyms
---
CREATE TABLE synonyms (
	syn_id serial NOT NULL,
	category integer default 0,
	word varchar(100) NOT NULL,
	seme_id integer default NULL REFERENCES synonyms(syn_id),
	PRIMARY KEY (syn_id),
	UNIQUE (category, word)
);
GRANT select,insert,delete,update ON synonyms TO bwriters;
GRANT select on synonyms TO breaders;

CREATE INDEX synonyms_seme_id ON synonyms (seme_id);

---
--- MAMA Item info
---

CREATE SEQUENCE item_id_seq;
CREATE TABLE iteminfo_items (
	item_id integer PRIMARY KEY default nextval('item_id_seq'),
	word varchar(90) NOT NULL,
	defining bool NOT NULL default 'f',
	parent_id integer default NULL REFERENCES iteminfo_items(item_id),
	UNIQUE (word,parent_id)
);
GRANT select,insert,delete,update ON iteminfo_items TO bwriters;
GRANT select on iteminfo_items TO breaders;
GRANT select,insert,delete,update ON item_id_seq to bwriters;
GRANT select on item_id_seq TO breaders;


CREATE SEQUENCE data_id_seq;
CREATE TABLE iteminfo_data (
	data_id integer PRIMARY KEY default nextval('data_id_seq'),
	name varchar(90) NOT NULL,
	value varchar(90) NOT NULL,
	item_id integer NOT NULL REFERENCES iteminfo_items(item_id),
	UNIQUE (name,item_id)

);
GRANT select,insert,delete,update ON iteminfo_data TO bwriters;
GRANT select on iteminfo_data TO breaders;
GRANT select,insert,delete,update ON data_id_seq to bwriters;
GRANT select on data_id_seq TO breaders;

-- Image digests
CREATE TABLE ad_images_digests (
  ad_id integer NOT NULL,
  name varchar(20) NOT NULL default '',
  digest varchar(40),
  uid INTEGER NOT NULL DEFAULT 0,
  previously_inserted BOOLEAN DEFAULT 'false',
  PRIMARY KEY (ad_id, name),
  FOREIGN KEY (ad_id) REFERENCES ads (ad_id)
);
CREATE INDEX index_ad_images_digests_uid ON ad_images_digests (uid);
CREATE INDEX ad_images_digests_digests_index ON ad_images_digests (digest);
GRANT select,insert,delete,update ON ad_images_digests to bwriters;
GRANT select on ad_images_digests TO breaders;

-- mama backup main table
CREATE TABLE mama_main_backup (
	backup_id	SERIAL PRIMARY KEY,
	backup_date	TIMESTAMP NOT NULL default CURRENT_TIMESTAMP,
	admin_id	INTEGER,
	FOREIGN KEY (admin_id) REFERENCES admins(admin_id)
);
GRANT select,insert,delete,update ON mama_main_backup to bwriters;
GRANT select on mama_main_backup TO breaders;
GRANT select,insert,delete,update ON mama_main_backup_backup_id_seq to bwriters;
GRANT select on mama_main_backup_backup_id_seq TO breaders;

-- mama lists and words
CREATE TABLE mama_wordlists (
	wordlist_id		SERIAL PRIMARY KEY,
	name			VARCHAR(50) NOT NULL,
	category 		INTEGER NOT NULL DEFAULT 0,
	suggestion_threshold	INTEGER NOT NULL,
	UNIQUE (name,category)
);
GRANT select,insert,delete,update ON mama_wordlists to bwriters;
GRANT select on mama_wordlists TO breaders;

-- mama lists and words backup
CREATE TABLE mama_wordlists_backup (
	backup_id		INTEGER,
	name			VARCHAR(50) NOT NULL,
	category 		INTEGER NOT NULL DEFAULT 0,
	suggestion_threshold	INTEGER NOT NULL,
	wordlist_id		INTEGER NOT NULL,
	UNIQUE (name,category,backup_id),
	FOREIGN KEY (backup_id) REFERENCES mama_main_backup(backup_id),
	PRIMARY KEY (backup_id, name, category)
);
GRANT select,insert,delete,update ON mama_wordlists_backup to bwriters;
GRANT select on mama_wordlists_backup TO breaders;

CREATE TYPE enum_mama_words_status AS ENUM ('inactive', 'active','discarded');
CREATE TABLE mama_words (
	word_id		SERIAL PRIMARY KEY,
	word		VARCHAR,
	wordlist_id	INTEGER NOT NULL,
	status		enum_mama_words_status NOT NULL DEFAULT 'inactive',
	frequency_ok	INTEGER NOT NULL DEFAULT 0,
	frequency_ko	INTEGER NOT NULL DEFAULT 0,
	FOREIGN KEY (wordlist_id) REFERENCES mama_wordlists(wordlist_id),
	UNIQUE (word,wordlist_id)
);
CREATE INDEX index_mama_words_wordlist_id ON mama_words(wordlist_id);
GRANT select,insert,delete,update ON mama_words to bwriters;
GRANT select on mama_words TO breaders;

CREATE TABLE mama_words_backup (
	backup_id	INTEGER NOT NULL,
	word_id		INTEGER NOT NULL,
	word		VARCHAR,
	wordlist_id	INTEGER NOT NULL,
	status		enum_mama_words_status NOT NULL DEFAULT 'inactive',
	frequency_ok	INTEGER NOT NULL DEFAULT 0,
	frequency_ko	INTEGER NOT NULL DEFAULT 0,
	FOREIGN KEY (backup_id) REFERENCES mama_main_backup(backup_id),
	UNIQUE (word,wordlist_id,backup_id),
	PRIMARY KEY (backup_id,word_id,wordlist_id)
);
GRANT select,insert,delete,update ON mama_words_backup to bwriters;
GRANT select on mama_words_backup TO breaders;

-- mama attribute lists and words and relation to categories
CREATE TABLE mama_attribute_wordlists (
	attribute_wordlist_id		SERIAL PRIMARY KEY,
	name			VARCHAR(50) NOT NULL,
	suggestion_threshold	INTEGER NOT NULL,
	UNIQUE (name)
);
GRANT select,insert,delete,update ON mama_attribute_wordlists to bwriters;
GRANT select on mama_attribute_wordlists TO breaders;


-- BACKUP mama attribute lists and words and relation to categories
CREATE TABLE mama_attribute_wordlists_backup (
	backup_id		INTEGER NOT NULL,
	attribute_wordlist_id	INTEGER,
	name			VARCHAR(50) NOT NULL,
	suggestion_threshold	INTEGER NOT NULL,
	UNIQUE (name,backup_id),
	PRIMARY KEY(name, backup_id),
	FOREIGN KEY (backup_id) REFERENCES mama_main_backup(backup_id)
);
GRANT select,insert,delete,update ON mama_attribute_wordlists_backup to bwriters;
GRANT select on mama_attribute_wordlists_backup TO breaders;

CREATE TYPE enum_mama_attribute_words_status AS ENUM ('inactive', 'active','discarded');
CREATE TABLE mama_attribute_words (
	word_id		SERIAL PRIMARY KEY,
	word		VARCHAR,
	attribute_wordlist_id	INTEGER NOT NULL,
	status		enum_mama_attribute_words_status NOT NULL DEFAULT 'inactive',
	frequency_ok	INTEGER NOT NULL DEFAULT 0,
	frequency_ko	INTEGER NOT NULL DEFAULT 0,
	FOREIGN KEY (attribute_wordlist_id) REFERENCES mama_attribute_wordlists(attribute_wordlist_id),
	UNIQUE (word,attribute_wordlist_id)
);
GRANT select,insert,delete,update ON mama_attribute_words to bwriters;
GRANT select on mama_attribute_words TO breaders;

CREATE TABLE mama_attribute_words_backup (
	backup_id	INTEGER NOT NULL,
	word_id		INTEGER NOT NULL,
	word		VARCHAR,
	attribute_wordlist_id	INTEGER NOT NULL,
	status		enum_mama_attribute_words_status NOT NULL DEFAULT 'inactive',
	frequency_ok	INTEGER NOT NULL DEFAULT 0,
	frequency_ko	INTEGER NOT NULL DEFAULT 0,
	FOREIGN KEY (backup_id) REFERENCES mama_main_backup(backup_id),
	UNIQUE (word,attribute_wordlist_id,backup_id),
	PRIMARY KEY (backup_id,word,attribute_wordlist_id)
);

GRANT select,insert,delete,update ON mama_attribute_words_backup to bwriters;
GRANT select on mama_attribute_words_backup TO breaders;


CREATE TABLE mama_attribute_categories (
	attribute_wordlist_id	INTEGER NOT NULL,
	category	INTEGER NOT NULL,
	FOREIGN KEY (attribute_wordlist_id) REFERENCES mama_attribute_wordlists (attribute_wordlist_id),
	PRIMARY KEY (attribute_wordlist_id, category)
);
GRANT select,insert,delete,update ON mama_attribute_categories to bwriters;
GRANT select on mama_attribute_categories TO breaders;

CREATE TABLE mama_attribute_categories_backup (
	backup_id	INTEGER NOT NULL,
	attribute_wordlist_id	INTEGER NOT NULL,
	category	INTEGER NOT NULL,
	FOREIGN KEY (backup_id) REFERENCES mama_main_backup (backup_id),
	PRIMARY KEY (attribute_wordlist_id, category,backup_id)
);
GRANT select,insert,delete,update ON mama_attribute_categories_backup to bwriters;
GRANT select on mama_attribute_categories_backup TO breaders;

-- mama exceptions

CREATE TYPE enum_mama_exception_lists_type AS ENUM ('embedded', 'combined');
CREATE TABLE mama_exception_lists (
	id		SERIAL PRIMARY KEY,
	type		enum_mama_exception_lists_type NOT NULL DEFAULT 'embedded',
	category	INTEGER NOT NULL,
	word		VARCHAR,
	UNIQUE (word, category, type)
);
GRANT select,insert,delete,update ON mama_exception_lists to bwriters;
GRANT select on mama_exception_lists TO breaders;

-- mama exceptions backup

CREATE TABLE mama_exception_lists_backup (
	backup_id	INTEGER NOT NULL,
	id		INTEGER,
	type		enum_mama_exception_lists_type NOT NULL DEFAULT 'embedded',
	category	INTEGER NOT NULL,
	word		VARCHAR,
	UNIQUE (word, category, type,backup_id),
	PRIMARY KEY (word, category, type, backup_id),
	FOREIGN KEY (backup_id) REFERENCES mama_main_backup (backup_id)
);
GRANT select,insert,delete,update ON mama_exception_lists_backup to bwriters;
GRANT select on mama_exception_lists_backup TO breaders;


CREATE TABLE mama_exception_words (
	exception_id		SERIAL PRIMARY KEY,
	exception_list	INTEGER NOT NULL,
	exception	VARCHAR,
	FOREIGN KEY (exception_list) REFERENCES mama_exception_lists (id) ON DELETE CASCADE
);
GRANT select,insert,delete,update ON mama_exception_words to bwriters;
GRANT select on mama_exception_words TO breaders;


CREATE TABLE mama_exception_words_backup (
	backup_id	INTEGER NOT NULL,
	exception_id	INTEGER NOT NULL,
	exception_list	INTEGER NOT NULL,
	exception	VARCHAR,
	FOREIGN KEY (backup_id) REFERENCES mama_main_backup (backup_id),
	PRIMARY KEY (backup_id,exception_id)
);
GRANT select,insert,delete,update ON mama_exception_words_backup to bwriters;
GRANT select on mama_exception_words_backup TO breaders;

-- Table to create a review log so that we can execute fast
-- and not heavy load queries from the cp. From more accurate
-- data and information use the ad_actions / states / params
CREATE TABLE review_log (
	ad_id INTEGER NOT NULL,
	action_id INTEGER NOT NULL,
	admin_id INTEGER NOT NULL,
	review_time TIMESTAMP default NOW(),
	queue VARCHAR(32) NOT NULL,
	action_type enum_ad_actions_action_type NOT NULL,
    action VARCHAR(10) NOT NULL,
	refusal_reason_text TEXT,
	category INTEGER NOT NULL,
	PRIMARY KEY (ad_id, action_id, admin_id, review_time)
);
CREATE INDEX index_review_action ON review_log(action);
GRANT select,insert,delete,update ON review_log to bwriters;
GRANT select on review_log TO breaders;

--Type to use in the stored procedure get_reviewers_times
CREATE TYPE review_period AS (
                            reviewer_name VARCHAR(50),
                            day VARCHAR(10),
                            start_time TIMESTAMP,
                            end_time TIMESTAMP,
                            period INTERVAL,
                            total_reviews INTEGER
                            );

CREATE TYPE enum_most_popular_aspect AS ENUM ('adreplies', 'adviews');
--- most_popular_ads
CREATE TABLE most_popular_ads(
	id	SERIAL,
	type	enum_most_popular_aspect NOT NULL DEFAULT 'adviews',
	list_id	INTEGER NOT NULL,
	quantity INTEGER NOT NULL,
	PRIMARY KEY (id,type)
);
GRANT select,insert,delete,update ON most_popular_ads to bwriters;
GRANT select on most_popular_ads TO breaders;

-- users testimonials table
CREATE TYPE enum_user_testimonial_areas AS ENUM ('insert_ad_form', 'delete_ad_form');
CREATE TABLE user_testimonial (
    user_testimonial_id serial PRIMARY KEY,
    ad_id integer NOT NULL,
    area enum_user_testimonial_areas NOT NULL,
    content varchar(1500) NOT NULL
);
CREATE INDEX index_user_testimonial_ad_id ON user_testimonial(ad_id);

GRANT select,insert,delete,update ON user_testimonial TO bwriters;
GRANT select ON user_testimonial TO breaders;

-- ads that only get to preview step
CREATE TABLE unfinished_ads (
	unf_ad_id serial PRIMARY KEY,
	session_id varchar(100) NOT NULL,
	email varchar(80) NOT NULL,
	added timestamp DEFAULT NULL,
	modified_at timestamp NOT NULL,
	mailed_at timestamp DEFAULT NULL,
	status enum_ads_status DEFAULT 'unpublished',
	data text default ''
);
CREATE INDEX index_unfinished_ad_id ON unfinished_ads(unf_ad_id);

GRANT select,insert,delete,update ON unfinished_ads to bwriters;
GRANT select on unfinished_ads TO breaders;
GRANT select,insert,delete,update ON unfinished_ads_unf_ad_id_seq TO bwriters;
GRANT select ON unfinished_ads_unf_ad_id_seq TO breaders;


-- purchase tables used by Yapo (for communicate with webpay and make invoices)
CREATE TYPE enum_purchase_status AS ENUM ('pending', 'paid', 'refused','sent','confirmed','refunded','voided', 'failed');
CREATE TYPE enum_purchase_doc_type AS ENUM ('invoice', 'bill', 'voucher');
CREATE TYPE enum_purchase_payment_method AS ENUM ('webpay_plus', 'webpay','servipag','appstore', 'oneclick', 'khipu', 'credits', 'unknown');
CREATE TYPE enum_purchase_payment_platform AS ENUM ('msite','desktop','unknown','android','ios');
CREATE TABLE purchase (
	purchase_id	serial unique,
	doc_type	enum_purchase_doc_type NOT NULL,
	doc_num		bigint,
	external_doc_id	bigint,
	status		enum_purchase_status NOT NULL,
	receipt		timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	delivery	timestamp,
	rut		character varying(12),
	name		character varying(200),
	lob		character varying(200),
	address		character varying(500),
	region		smallint,
	communes	smallint,
	contact		character varying(200),
	email		character varying(80) NOT NULL,
	tax		integer NOT NULL,
	discount	integer NOT NULL default(0),
	seller_id	integer default NULL,
	total_price	integer NOT NULL,
	payment_group_id	integer,
	payment_method		enum_purchase_payment_method DEFAULT 'webpay'::enum_purchase_payment_method NOT NULL,
	payment_platform	enum_purchase_payment_platform DEFAULT 'unknown'::enum_purchase_payment_platform NOT NULL,
	account_id 		integer,
	PRIMARY KEY (purchase_id)
);
GRANT select,insert,delete,update ON purchase TO bwriters;
GRANT select ON purchase TO breaders;
GRANT select,insert,delete,update ON purchase_purchase_id_seq TO bwriters;
GRANT select ON purchase_purchase_id_seq TO breaders;

CREATE INDEX index_purchase_id ON purchase(purchase_id);
CREATE INDEX index_purchase_doc ON purchase(doc_type, doc_num) WHERE doc_num is not null;
CREATE INDEX index_purchase_rut ON purchase(rut) where rut is not null and doc_num is not null;
CREATE INDEX index_purchase_email ON purchase(email);

CREATE TABLE purchase_detail (
	purchase_detail_id	serial NOT NULL,
	purchase_id 		integer NOT NULL,
	product_id 		integer NOT NULL,
	price			integer NOT NULL,
	action_id		integer,
	ad_id			integer,
	payment_group_id integer not null default 0,
	FOREIGN KEY(purchase_id) REFERENCES purchase (purchase_id) ON DELETE CASCADE,
	FOREIGN KEY(ad_id) 	 REFERENCES ads (ad_id),
	PRIMARY KEY(purchase_id, purchase_detail_id)
);
GRANT select,insert,delete,update ON purchase_detail TO bwriters;
GRANT select ON purchase_detail TO breaders;
GRANT select,insert,delete,update ON purchase_detail_purchase_detail_id_seq TO bwriters;
GRANT select ON purchase_detail_purchase_detail_id_seq TO breaders;

CREATE INDEX index_purchase_detail_id on purchase_detail(purchase_id,purchase_detail_id);
CREATE INDEX index_puchase_detail_ad_product on purchase_detail(ad_id, product_id);
CREATE INDEX index_purchase_detail_purchase_id_payment_group_id ON purchase_detail(purchase_id, payment_group_id);

CREATE TABLE purchase_states (
	purchase_id 		integer NOT NULL,
	purchase_state_id	serial NOT NULL,
	status			enum_purchase_status NOT NULL,
	timestamp 		timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	remote_addr 		varchar(20) default '',
	FOREIGN KEY(purchase_id) REFERENCES purchase (purchase_id) on DELETE CASCADE,
	PRIMARY KEY(purchase_id, purchase_state_id)
);
GRANT select,insert,delete,update ON purchase_states TO bwriters;
GRANT select ON purchase_states TO breaders;
GRANT select,insert,delete,update ON purchase_states_purchase_state_id_seq TO bwriters;
GRANT select ON purchase_states_purchase_state_id_seq TO breaders;

CREATE INDEX index_purchase_states ON purchase_states (purchase_id, purchase_state_id);

CREATE TYPE enum_purchase_params_name AS ENUM ('fail_on_yapo', 'oneclick_id');

CREATE TABLE purchase_params (
	purchase_id integer NOT NULL,
	name enum_purchase_params_name NOT NULL,
	value text NOT NULL,
	PRIMARY KEY (purchase_id,name)
);
CREATE INDEX index_purchase_params_name_value ON purchase_params (name, value);
CREATE INDEX index_purchase_params_purchase_id ON purchase_params (purchase_id);

GRANT select,insert,delete,update ON purchase_params TO bwriters;
GRANT select ON purchase_params TO breaders;

CREATE TYPE enum_purchase_detail_params_name AS ENUM (
    'frequency',
    'num_days',
    'total_bump',
    'last_date',
    'use_night',
    'plates',
    'with_delay',
    'error',
    'error_at'
);

CREATE TABLE purchase_detail_params (
	purchase_detail_id integer NOT NULL,
	name enum_purchase_detail_params_name NOT NULL,
	value text NOT NULL,
	PRIMARY KEY (purchase_detail_id,name)
);
CREATE INDEX index_purchase_detail_params_name_value ON purchase_detail_params (name, value);
CREATE INDEX index_purchase_detail_params_purchase_detail_id ON purchase_detail_params (purchase_detail_id);

GRANT select,insert,delete,update ON purchase_detail_params TO bwriters;
GRANT select ON purchase_detail_params TO breaders;
-- end purchase tables.-



--Enum of the account status
CREATE TYPE enum_account_status as ENUM ('active', 'inactive', 'pending_confirmation');

CREATE TYPE enum_account_gender as ENUM ('male', 'female');

-- Table to be used to store the accounts
CREATE TABLE accounts (
        account_id              SERIAL UNIQUE,
        name               VARCHAR(200) NOT NULL,
        rut                 VARCHAR(12) NULL,
        email                   VARCHAR(80) NOT NULL UNIQUE,
        phone                   VARCHAR(100) NULL,
        is_company              BOOLEAN NULL,
        region                  SMALLINT NULL,
        salted_passwd     varchar(100) NOT NULL,
        creation_date   TIMESTAMP NOT NULL Default CURRENT_TIMESTAMP,
        activation_date   TIMESTAMP,
        status                  enum_account_status NOT NULL,
        address         VARCHAR(500) NULL,
        contact         VARCHAR(200) NULL,
        lob             VARCHAR(200) NULL,
        commune         SMALLINT NULL,
        user_id                 INTEGER NOT NULL,
        phone_hidden              BOOLEAN DEFAULT FALSE,
        gender   enum_account_gender DEFAULT NULL,
        birthdate date DEFAULT NULL,
        PRIMARY KEY(account_id),
        FOREIGN KEY ( user_id ) REFERENCES users ( user_id )
);
CREATE INDEX index_accounts_status ON accounts(status);
CREATE INDEX index_accounts_email ON accounts(email);

GRANT select,insert,delete,update ON accounts TO bwriters;
GRANT select ON accounts TO breaders;
GRANT select,insert,delete,update ON accounts_account_id_seq TO bwriters;
GRANT select ON accounts_account_id_seq TO breaders;

CREATE INDEX index_accounts_user_id ON accounts ( user_id );

ALTER TABLE stores ADD CONSTRAINT store_account_id_fk FOREIGN KEY (account_id) REFERENCES accounts (account_id);


CREATE TYPE enum_account_params_name AS ENUM (
'is_pro_for',
'available_slots',
'source',
'pack_type',
'business_name'
);

CREATE TABLE account_params (
  account_id integer NOT NULL,
  name enum_account_params_name NOT NULL,
  value text NOT NULL,
  PRIMARY KEY  (account_id,name),
  FOREIGN KEY (account_id) REFERENCES accounts (account_id)
);
GRANT select,insert,delete,update ON account_params to bwriters;
GRANT select on account_params TO breaders;

CREATE INDEX index_account_params_name_value ON account_params (name, value);
CREATE INDEX index_account_params_account_id ON account_params (account_id);

CREATE TYPE enum_account_action_type as ENUM (
	'creation',
	'activation',
	'edition',
	'deactivation',
	'add_pro_category_param',
	'remove_pro_category_param',
	'password_change',
	'phone_update',
	'fix_pack_slots'
);

CREATE TABLE account_actions (
  account_id integer NOT NULL,
  account_action_id  integer NOT NULL,
  account_action_type enum_account_action_type NOT NULL,
  timestamp TIMESTAMP NOT NULL Default CURRENT_TIMESTAMP,
  token_id INTEGER REFERENCES tokens ( token_id ),
  PRIMARY KEY  (account_id,account_action_id),
  FOREIGN KEY (account_id) REFERENCES accounts (account_id)
);
GRANT select,insert,delete,update ON account_actions to bwriters;
GRANT select on account_actions TO breaders;

CREATE INDEX index_account_actions_account_id ON account_actions (account_id);
CREATE INDEX index_account_actions_action_type_account_id ON account_actions (account_action_type, account_id);
CREATE INDEX index_account_actions_token_id ON account_actions (token_id);

CREATE TABLE account_changes (
  account_id integer NOT NULL,
  account_action_id integer NOT NULL,
  is_param bool NOT NULL,
  column_name varchar(50) NOT NULL,
  old_value text,
  new_value text,
  PRIMARY KEY (account_id, account_action_id, is_param, column_name),
  CHECK (NOT (old_value IS NULL AND new_value IS NULL))
);
GRANT select, insert, delete, update ON account_changes to bwriters;
GRANT select on account_changes TO breaders;

CREATE INDEX index_account_changes_account_id_action_id ON account_changes(account_id, account_action_id);

-- end accounts tables.-

CREATE EXTENSION "uuid-ossp";

CREATE TABLE email_uuid (
	email VARCHAR(128) not null UNIQUE,
	uuid UUID not null UNIQUE DEFAULT uuid_generate_v4(),
	name varchar(100) NOT NULL DEFAULT '',
	PRIMARY KEY(email)
);

CREATE INDEX index_email_uuid_uuid ON email_uuid(uuid);
GRANT SELECT,INSERT,DELETE,UPDATE on email_uuid TO bwriters;
GRANT SELECT on email_uuid TO breaders;

-- Table to be used to store Transbank Log Info
CREATE TABLE transbank_log (
	log_id			SERIAL UNIQUE,
	status			VARCHAR(10),
	orden_compra		bigint,
	codigo_comercio		bigint,
	tipo_transaccion	VARCHAR(20),
	respuesta		INTEGER,
	monto			INTEGER,
	codigo_autorizacion	VARCHAR(10),
	fecha_contable		CHAR(4),
	fecha_transaccion	CHAR(4),
	hora_transaccion	VARCHAR(6),
	id_sesion		VARCHAR(40),
	id_transaccion		bigint,
	tipo_pago		CHAR(2),
	numero_cuotas		INTEGER,
	vci			CHAR(3),
	fecha			TIMESTAMP,
        PRIMARY KEY(log_id)
);
CREATE INDEX index_orden_compra ON transbank_log(orden_compra);
CREATE INDEX index_codigo_autorizacion ON transbank_log(codigo_autorizacion);

GRANT select,insert,delete,update ON transbank_log TO bwriters;
GRANT select ON transbank_log TO breaders;
-- end transbank_log tables.-

CREATE TYPE enum_stats_type AS ENUM (
    'view',
    'mail',
    'galleryview',
    'store_view',
    'monthlyview',
    'monthlymail',
    'monthlygalleryview',
    'monthlystore_view'
);

-- Table to speed up ad archiving process
CREATE TABLE archive_cache (
	ad_id integer NOT NULL UNIQUE
);

GRANT select,insert,delete,update ON archive_cache TO bwriters;
GRANT select ON archive_cache TO breaders;
-- end archive_cache tables

------------------------------------------
-- SOCIAL ACCOUNTS
------------------------------------------

CREATE TYPE enum_social_accounts_social_partner AS ENUM (
'facebook_id',
'google_id',
'apple_id'
);

CREATE TABLE social_accounts (
    social_account_id serial unique,
    data_joined timestamp DEFAULT CURRENT_TIMESTAMP,
    email character varying(80) NOT NULL,
    social_id text,
    social_partner enum_social_accounts_social_partner,
    account_id integer NOT NULL,
    PRIMARY KEY (social_account_id),
    FOREIGN KEY (account_id) REFERENCES accounts (account_id)
);
GRANT select,insert,delete,update ON social_accounts TO bwriters;
GRANT select ON social_accounts TO breaders;
GRANT select,update ON social_accounts_social_account_id_seq TO bwriters;
GRANT select ON social_accounts_social_account_id_seq TO breaders;

CREATE INDEX index_social_account_id ON social_accounts(social_account_id);
CREATE INDEX index_social_accounts_account_id ON social_accounts(account_id);
CREATE INDEX index_social_accounts_social_id ON social_accounts(social_id);

CREATE TYPE enum_social_accounts_params AS ENUM (
'first_name',
'last_name',
'location',
'source',
'image'
);

CREATE TABLE social_accounts_params (
  social_account_id integer NOT NULL,
  name enum_social_accounts_params NOT NULL,
  value text NOT NULL,
  PRIMARY KEY  (social_account_id,name),
  FOREIGN KEY (social_account_id) REFERENCES social_accounts (social_account_id) ON DELETE CASCADE
);
GRANT select,insert,delete,update ON social_accounts_params to bwriters;
GRANT select on social_accounts_params TO breaders;

CREATE INDEX index_social_accounts_params_name_value ON social_accounts_params (name, value);
CREATE INDEX index_social_accounts_params_account_id ON social_accounts_params (social_account_id);
------------------------------------------
-- END SOCIAL ACCOUNTS
------------------------------------------


------------------------------------------
-- PURCHASE TABLES USED BY YAPO (IN-APP PURCHASE)
------------------------------------------
CREATE TYPE enum_purchase_in_app_currency AS ENUM ('CLP','USD');
CREATE TYPE enum_purchase_in_app_status AS ENUM ('pending', 'confirmed', 'refused', 'refunded', 'failed');

CREATE TABLE purchase_in_app (
    purchase_in_app_id serial unique,
    status      	enum_purchase_in_app_status NOT NULL,
    receipt_date	timestamp DEFAULT CURRENT_TIMESTAMP,
    delivery   	 	timestamp,
    region      	smallint,
    communes    	smallint,
    email       	character varying(80) NOT NULL,
    is_company      BOOLEAN NULL,
    external_receipt    text,
    currency        	enum_purchase_in_app_currency DEFAULT 'USD'::enum_purchase_in_app_currency NOT NULL,
    payment_method      enum_purchase_payment_method DEFAULT 'unknown'::enum_purchase_payment_method NOT NULL,
    payment_platform    enum_purchase_payment_platform DEFAULT 'unknown'::enum_purchase_payment_platform NOT NULL,
    account_id      integer NOT NULL,
    product_id      integer NOT NULL,
    product_name    character varying(30) NOT NULL,
    price           real NOT NULL,
    ad_id          	integer NOT NULL,
	remote_addr 	varchar(20) default '',
    action_id       integer default 0,
    PRIMARY KEY (purchase_in_app_id)
);
GRANT select,insert,delete,update ON purchase_in_app TO bwriters;
GRANT select ON purchase_in_app TO breaders;
GRANT select,insert,delete,update ON purchase_in_app_purchase_in_app_id_seq TO bwriters;
GRANT select ON purchase_in_app_purchase_in_app_id_seq TO breaders;

CREATE INDEX index_purchase_in_app_id ON purchase_in_app(purchase_in_app_id);
CREATE INDEX index_purchase_in_app_email ON purchase_in_app(email);
------------------------------------------
-- END PURCHASE TABLES (IN-APP PURCHASE) .-
------------------------------------------

---------------------
-- PACKS
---------------------
CREATE TYPE enum_pack_status as ENUM ('active','expired');
-- the use of type is ONLY for cars, the others are for testing
CREATE TYPE enum_pack_type as ENUM ('car','real_estate','moto');
CREATE TYPE enum_pack_param_name as ENUM ('service_order','service_amount');

CREATE TABLE packs (
	pack_id			SERIAL UNIQUE,
	account_id		INTEGER NOT NULL,
	status			enum_pack_status DEFAULT 'active'::enum_pack_status NOT NULL,
	type			enum_pack_type NOT NULL,
	slots			INTEGER NOT NULL,
	used			INTEGER NOT NULL DEFAULT 0,
	date_start		TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	date_end		TIMESTAMP NOT NULL,
	product_id		INTEGER NOT NULL,
	payment_group_id 	INTEGER,
	token_id 		INTEGER REFERENCES tokens ( token_id ),
	PRIMARY KEY ( pack_id ),
	FOREIGN KEY ( account_id ) REFERENCES accounts ( account_id ),
	FOREIGN KEY ( payment_group_id ) REFERENCES payment_groups ( payment_group_id )
);
ALTER TABLE packs ADD CONSTRAINT slots_check CHECK (used <= slots AND used >= 0 AND slots > 0);

CREATE INDEX index_packs_date_start ON packs (date_start);
CREATE INDEX index_packs_date_end ON packs (date_end);
CREATE INDEX index_packs_status ON packs (status);
CREATE INDEX index_packs_account_id ON packs (account_id);
CREATE INDEX index_packs_token_id ON packs (token_id);

GRANT select,insert,delete,update ON packs TO bwriters;
GRANT select ON packs TO breaders;
GRANT select,insert,delete,update ON packs_pack_id_seq TO bwriters;
GRANT select ON packs_pack_id_seq TO breaders;

CREATE TABLE pack_params (
	pack_param_id SERIAL UNIQUE,
	pack_id INT NOT NULL,
	name enum_pack_param_name NOT NULL,
	value text NOT NULL,
	PRIMARY KEY  (pack_param_id),
	FOREIGN KEY (pack_id) REFERENCES packs (pack_id)
);

GRANT select,insert,delete,update ON pack_params TO bwriters;
GRANT select ON pack_params TO breaders;
---------------------
-- END PACKS
---------------------

---------------------
-- FILTERD
---------------------

CREATE type filter_data_result AS (
    o_rule_id integer,
    o_rule_name varchar,
    o_action enum_block_rules_action,
    o_target enum_ad_actions_queue,
    o_combi_match boolean,
    o_matched_column varchar,
    o_matched_value varchar
);

CREATE EXTENSION hstore;

---------------------
-- END FILTERD
---------------------

CREATE TABLE streets (
	street_id SERIAL UNIQUE,
	region_id INTEGER,
	commune_id INTEGER,
	street_name VARCHAR(150),
	lat DOUBLE PRECISION,
	lng DOUBLE PRECISION,
	PRIMARY KEY ( street_id )
);
CREATE INDEX index_streets_commune_id ON streets (commune_id);

-- The data load is in the start-build-pgsql.sh file
-- COPY streets (commune_id, region_id, street_name) FROM 'street_names.csv' DELIMITER ';' QUOTE '\'' CSV;

---------------------
-- REFUND
---------------------

CREATE TYPE enum_refund_status AS ENUM (
	'refunded',
	'pending',
	'failed'
);

CREATE TABLE refunds (
	refund_id			SERIAL UNIQUE,
	purchase_id			INTEGER NOT NULL,
	ad_id				INTEGER NOT NULL,
	action_id			INTEGER NOT NULL,
	first_name			VARCHAR(50) NOT NULL,
	last_name			VARCHAR(50) NOT NULL,
	rut					VARCHAR(12) NULL,
	email				VARCHAR(60) NOT NULL,
	bank_account_type	INTEGER NOT NULL,
	bank				INTEGER NOT NULL,
	bank_account_number	VARCHAR(30) NOT NULL,
	creation_date		TIMESTAMP NOT NULL Default CURRENT_TIMESTAMP,
	status				enum_refund_status NOT NULL,
	PRIMARY KEY(refund_id)
);
CREATE INDEX index_refunds_email ON refunds(email);
CREATE UNIQUE INDEX index_refunds_ids ON refunds(ad_id, action_id, purchase_id);

GRANT select,insert,delete,update ON refunds TO bwriters;
GRANT select ON refunds TO breaders;
GRANT select,usage,update ON refunds_refund_id_seq TO bwriters;
GRANT select ON refunds_refund_id_seq TO breaders;

---------------------
-- END REFUND
---------------------
--- PROTOTYPE
CREATE TYPE enum_dashboard_status AS ENUM (
	'active',
	'admin_show',
	'deactivated',
	'deleted',
	'deleting',
	'disabled',
	'edit-reviewing',
	'editrefused',
	'hidden',
	'inactive',
	'pack_show',
	'pending-review',
	'refused',
	'review-edit',
	'review-edit-disabled',
	'undo_deleted',
	'unpublished'
);

CREATE TABLE dashboard_ads (
	ad_id             integer PRIMARY KEY,
	user_id           integer NOT NULL,
	list_id           integer UNIQUE,
	list_time         timestamp DEFAULT NULL,
	company_ad        bool NOT NULL DEFAULT 'f',
	type              enum_ads_type NOT NULL DEFAULT 'sell',
	category          integer NOT NULL DEFAULT 0,
	ad_status         enum_dashboard_status NOT NULL DEFAULT 'inactive',
	subject           varchar(50) NOT NULL DEFAULT '',
	body              text,
	price             bigint DEFAULT NULL,
	ad_media_id       bigint,
	image_count       integer NOT NULL DEFAULT 0,
	daily_bump        varchar(32),
	weekly_bump       varchar(32),
	label             varchar(32),
	gallery_date      varchar(32),
	ad_pack_status    varchar(32),
	ad_params_list    text,
	last_change       timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	auto_bump         varchar(32),
	paid              bool NOT NULL DEFAULT 't'
);

GRANT select,insert,delete,update ON dashboard_ads to bwriters;
GRANT select on dashboard_ads TO breaders;

CREATE INDEX index_dashboard_ads_user_id ON dashboard_ads (user_id);

---------------------
-- AUTOBUMP
---------------------
CREATE TYPE enum_autombump_status AS ENUM (
	'pending',
	'success',
	'error',
	'cancel',
	'skipped'
);

CREATE TABLE autobump (
	autobump_id SERIAL UNIQUE PRIMARY KEY,
	ad_id INTEGER NOT NULL,
	purchase_detail_id INTEGER,
	status enum_autombump_status NOT NULL DEFAULT 'pending',
	execute_at timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	executed_at timestamp DEFAULT NULL,
	token_id INTEGER REFERENCES tokens ( token_id )
);

GRANT select,insert,delete,update ON autobump to bwriters;
GRANT select on autobump TO breaders;

CREATE INDEX index_autobump_ad_id ON autobump(ad_id);
CREATE INDEX index_autobump_purchase_detail_id ON autobump(purchase_detail_id);
CREATE INDEX index_autobump_execute ON autobump(execute_at);
CREATE INDEX index_autobump_token_id ON autobump(token_id);


---------------------
-- END AUTOBUMP
---------------------

-- vim:syntax=sql
