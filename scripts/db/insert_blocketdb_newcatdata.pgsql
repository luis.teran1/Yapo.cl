INSERT INTO users (user_id, uid, email, account, paid_total) VALUES (1100, 1100, 'v2user1@blocket.se', 0, 180);
INSERT INTO users (user_id, uid, email, account, paid_total) VALUES (1101, 1100, 'v2user2_tillgodo@blocket.se', 1000, 80);

INSERT INTO ads (ad_id, list_id, orig_list_time, list_time, status, "type", name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, old_price, image, infopage, infopage_title, store_id) VALUES (610000, 8350000, NULL, '2008-01-11 11:09:12', 'active', 'sell', 'Arne Anka', '08-000000', 11, 0, 1061, 1100, '11111', false, false, false, 'Snabb cigarrb�t', 'Tr�b�t i fint skick', 23456, NULL, NULL, NULL, NULL, NULL);
INSERT INTO ads (ad_id, list_id, orig_list_time, list_time, status, "type", name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, old_price, image, infopage, infopage_title, store_id) VALUES (610001, 8350001, NULL, '2008-01-11 11:09:21', 'active', 'sell', 'Arne Anka', '08-000000', 11, 0, 1020, 1100, '11111', false, true, false, 'Ferrari', 'Brun ferrari s�ljes till vrakpris. G�r att k�ra p� metanol.', 100000, NULL, NULL, NULL, NULL, NULL);
INSERT INTO ads (ad_id, list_id, orig_list_time, list_time, status, "type", name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, old_price, image, infopage, infopage_title, store_id) VALUES (610002, 8350002, NULL, '2008-01-11 11:09:55', 'active', 'sell', 'Mimmi Tillgodo', '08-000000', 11, 0, 1200, 1101, '11111', false, false, false, 'Eldriven sn�skoter', 'Sn�skoter med eldrift. N�got begr�nsad aktionsradie.', 123000, NULL, NULL, NULL, NULL, NULL);
INSERT INTO ads (ad_id, list_id, orig_list_time, list_time, status, "type", name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, old_price, image, infopage, infopage_title, store_id) VALUES (610003, 8350003, NULL, '2008-01-11 11:10:16', 'active', 'sell', 'Mimmi Tillgodo', '08-000000', 11, 0, 2062, 1101, '11111', false, false, false, 'Electrolux EWS12410W', 'Liten smidig tv�ttmaskin i nyskick', 5000, NULL, NULL, NULL, NULL, NULL);
INSERT INTO ads (ad_id, list_id, orig_list_time, list_time, status, "type", name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, old_price, image, infopage, infopage_title, store_id) VALUES (610004, 8350004, NULL, '2008-01-11 11:11:12', 'active', 'let', 'Arne Anka', '08-000000', 11, 0, 1061, 1100, '11111', false, false, false, 'L�neb�t', 'Med stor motor', NULL, NULL, NULL, NULL, NULL, NULL);

INSERT INTO payment_groups (payment_group_id, code, status, added_at) VALUES (76500, '74046', 'cleared', '2008-01-11 11:05:53.661982');
INSERT INTO payment_groups (payment_group_id, code, status, added_at) VALUES (76501, '42023', 'cleared', '2008-01-11 11:05:06.490185');
INSERT INTO payment_groups (payment_group_id, code, status, added_at) VALUES (76502, '48092', 'cleared', '2008-01-11 11:08:23.895089');
INSERT INTO payment_groups (payment_group_id, code, status, added_at) VALUES (76503, '16069', 'cleared', '2008-01-11 11:06:55.041996');
INSERT INTO payment_groups (payment_group_id, code, status, added_at) VALUES (76504, '16070', 'cleared', '2008-01-11 11:11:55.041996');

INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (610000, 11000, 'new', 15, 'accepted', 'normal', NULL, NULL, 76500);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (610001, 11000, 'new', 16, 'accepted', 'normal', NULL, NULL, 76501);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (610002, 11000, 'new', 19, 'accepted', 'normal', NULL, NULL, 76502);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (610003, 11000, 'new', 20, 'accepted', 'normal', NULL, NULL, 76503);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (610004, 1, 'new', 11026, 'accepted', 'normal', NULL, NULL, 76504);

INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (610001, 11000, 11001, 'reg', 'initial', '2008-01-11 11:05:06.490185', '192.168.5.91', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (610001, 11000, 11002, 'unpaid', 'newad', '2008-01-11 11:05:06.490185', '192.168.5.91', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (610001, 11000, 11003, 'unpaid', 'paymailsent', '2008-01-11 11:05:06.55735', '192.168.5.91', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (610000, 11000, 11004, 'reg', 'initial', '2008-01-11 11:05:53.661982', '192.168.5.91', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (610000, 11000, 11005, 'unpaid', 'newad', '2008-01-11 11:05:53.661982', '192.168.5.91', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (610000, 11000, 11006, 'unpaid', 'paymailsent', '2008-01-11 11:05:53.694567', '192.168.5.91', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (610003, 11000, 11007, 'reg', 'initial', '2008-01-11 11:06:55.041996', '192.168.5.91', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (610003, 11000, 11008, 'unpaid', 'newad', '2008-01-11 11:06:55.041996', '192.168.5.91', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (610003, 11000, 11009, 'unpaid', 'paymailsent', '2008-01-11 11:06:55.077552', '192.168.5.91', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (610002, 11000, 11010, 'reg', 'initial', '2008-01-11 11:08:23.895089', '192.168.5.91', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (610002, 11000, 11011, 'unpaid', 'newad', '2008-01-11 11:08:23.895089', '192.168.5.91', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (610002, 11000, 11012, 'unpaid', 'paymailsent', '2008-01-11 11:08:23.955234', '192.168.5.91', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (610000, 11000, 11013, 'pending_review', 'adminclear', '2008-01-11 11:08:59.296212', '192.168.5.91', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (610001, 11000, 11014, 'pending_review', 'adminclear', '2008-01-11 11:09:03.801986', '192.168.5.91', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (610000, 11000, 11015, 'accepted', 'accept', '2008-01-11 11:09:12.520829', '192.168.5.91', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (610001, 11000, 11016, 'accepted', 'accept', '2008-01-11 11:09:21.203509', '192.168.5.91', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (610002, 11000, 11017, 'pending_review', 'adminclear', '2008-01-11 11:09:41.12284', '192.168.5.91', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (610003, 11000, 11018, 'pending_review', 'adminclear', '2008-01-11 11:09:46.841784', '192.168.5.91', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (610002, 11000, 11019, 'accepted', 'accept', '2008-01-11 11:09:55.555867', '192.168.5.91', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (610003, 11000, 11020, 'accepted', 'accept', '2008-01-11 11:10:16.94285', '192.168.5.91', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (610004, 1, 11021, 'reg', 'initial', '2008-01-11 11:05:06.490185', '192.168.5.91', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (610004, 1, 11022, 'unpaid', 'newad', '2008-01-11 11:05:06.490185', '192.168.5.91', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (610004, 1, 11023, 'unpaid', 'paymailsent', '2008-01-11 11:05:06.55735', '192.168.5.91', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (610004, 1, 11024, 'pending_review', 'adminclear', '2008-01-11 11:05:06.55735', '192.168.5.91', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (610004, 1, 11025, 'locked', 'checkout', '2008-01-11 11:05:06.55735', '192.168.5.91', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (610004, 1, 11026, 'accepted', 'accept', '2008-01-11 11:10:16.94285', '192.168.5.91', NULL);

INSERT INTO ad_params (ad_id, name, value) VALUES (610001, 'regdate', '1995');
INSERT INTO ad_params (ad_id, name, value) VALUES (610001, 'mileage', '8');
INSERT INTO ad_params (ad_id, name, value) VALUES (610001, 'gearbox', '2');
INSERT INTO ad_params (ad_id, name, value) VALUES (610001, 'fuel', '3');
INSERT INTO ad_params (ad_id, name, value) VALUES (610001, 'area', '116');
INSERT INTO ad_params (ad_id, name, value) VALUES (610000, 'area', '116');
INSERT INTO ad_params (ad_id, name, value) VALUES (610000, 'currency', 'uf');
INSERT INTO ad_params (ad_id, name, value) VALUES (610003, 'area', '116');
INSERT INTO ad_params (ad_id, name, value) VALUES (610002, 'area', '116');
INSERT INTO ad_params (ad_id, name, value) VALUES (610004, 'available_weeks', '2008:22');
INSERT INTO ad_params (ad_id, name, value) VALUES (610004, 'weekly_rent', '1000');

INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (110000, 'save', 0, '42023', NULL, '2008-01-11 11:05:06', 76501, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (110001, 'save', 0, '74046', NULL, '2008-01-11 11:05:54', 76500, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (110002, 'save', 0, '16069', NULL, '2008-01-11 11:06:55', 76503, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (110003, 'save', 0, '48092', NULL, '2008-01-11 11:08:24', 76502, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (110004, 'payphone', 90, '74046', NULL, '2008-01-11 11:08:59', 76500, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (110005, 'payphone', 90, '42023', NULL, '2008-01-11 11:09:04', 76501, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (110006, 'payphone', 60, '48092', NULL, '2008-01-11 11:09:41', 76502, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (110007, 'payphone', 20, '16069', NULL, '2008-01-11 11:09:47', 76503, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (110008, 'save', 0, '16070', NULL, '2008-01-11 11:08:24', 76504, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (110009, 'payphone', 60, '16070', NULL, '2008-01-11 11:08:24', 76504, 'OK');

INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (110000, 0, 'adphone', '08000000');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (110000, 1, 'remote_addr', '192.168.5.91');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (110001, 0, 'adphone', '08000000');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (110001, 1, 'remote_addr', '192.168.5.91');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (110002, 0, 'adphone', '08000000');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (110002, 1, 'remote_addr', '192.168.5.91');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (110003, 0, 'adphone', '08000000');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (110003, 1, 'remote_addr', '192.168.5.91');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (110004, 0, 'remote_addr', '192.168.5.91');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (110005, 0, 'remote_addr', '192.168.5.91');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (110006, 0, 'remote_addr', '192.168.5.91');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (110007, 0, 'remote_addr', '192.168.5.91');

INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (610000, 11000, 11015, 'filter_name', 'all');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (610001, 11000, 11016, 'filter_name', 'all');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (610002, 11000, 11019, 'filter_name', 'all');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (610003, 11000, 11020, 'filter_name', 'all');

SET SEARCH_PATH=bpv,public;
SELECT generate_ad_codes('pay',100) FROM (VALUES (1),(2),(3),(4),(5),(6),(7),(8),(9),(10)) AS a CROSS JOIN (VALUES (1),(2),(3),(4),(5),(6),(7),(8),(9),(10)) AS b;
SELECT generate_ad_codes('verify',100) FROM (VALUES (1),(2),(3),(4),(5),(6),(7),(8),(9),(10)) AS a CROSS JOIN (VALUES (1),(2),(3),(4),(5),(6),(7),(8),(9),(10)) AS b;


--Migrate inmo ads
select * from bpv.migrate_ads_inmo();
