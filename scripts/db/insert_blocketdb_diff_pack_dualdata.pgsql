INSERT INTO account_params (account_id, name, value) VALUES (6, 'is_pro_for', '7080,2020,2040,1220,1240');
INSERT INTO account_params (account_id, name, value) VALUES (6, 'pack_type', 'car');

--Change all passwords to 123123123
update ads set salted_passwd =  E'$1024$2y1bQ}VYRD}zF4QCbf07a971b784888658a20e62156de64dfde5ffb6', passwd = null;
update accounts set salted_passwd =  E'$1024$2y1bQ}VYRD}zF4QCbf07a971b784888658a20e62156de64dfde5ffb6';
