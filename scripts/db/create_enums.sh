#!/bin/sh

if [ -n "$1" -a "$1" != "slave" -a "$1" != "archives" ]; then
	PGHOST=$1
	shift
fi
if [ "$1" = "slave" ]; then
	slave=1
	shift;
fi
if [ "$1" = "archives" ]; then
	archives=1
	shift;
fi

PGDATABASE=blocketdb
export PGHOST
export PGDATABASE

psql -tA -c "
SELECT
	t.relname, bpv.group_concat(n.nspname),
	regexp_replace(r.conname, '^' || t.relname || '_(.*)_check', E'\\\\1'),
	pg_catalog.pg_get_constraintdef(r.oid, true)
FROM
	pg_catalog.pg_constraint r
	JOIN pg_class t ON
		r.conrelid = t.oid
		AND r.contype = 'c'
	JOIN pg_class ts ON
		ts.relname = t.relname
	JOIN pg_namespace n ON
		n.oid = ts.relnamespace
WHERE
	conname ~ ('^' || t.relname || '_[a-z_]+_check')
	AND consrc LIKE '%ANY%' GROUP BY 1, 3, 4
ORDER BY
	1,
	2,
	3
;" | sed -r -e 's/::(text|character varying)(\[\])?//g' -e 's/([^|]*)\|([^|]*)\|([^|]*)\|.*ARRAY\[(.*)\]\)\)/\1 \2 \3 \4/' | while read table nspaces column check; do
	echo "-- ${table} ${column}"
	echo "CREATE TYPE public.enum_${table}_${column} AS ENUM ($check);"
	# Ugh, seems this is the only way
	echo "CREATE OR REPLACE FUNCTION public.enum_trigger_${table}_${column} () RETURNS trigger AS 'BEGIN NEW.${column}_enum := NEW.${column}; RETURN NEW; END;' LANGUAGE plpgsql;"

	for nsp in `echo $nspaces | tr ',' ' '`; do
		if [ "$archives" != "1" -a "$nsp" != "public" ]; then
			continue;
		fi
		echo "ALTER TABLE $nsp.$table ADD ${column}_enum public.enum_${table}_${column} DEFAULT NULL;"
		if [ "$slave" != "1" ]; then
			echo "CREATE TRIGGER trigger_${table}_${column}_enum BEFORE INSERT OR UPDATE ON $nsp.$table FOR EACH ROW EXECUTE PROCEDURE public.enum_trigger_${table}_${column} ();"
		fi
	done
done

# Some columns without checks, using already existing enums
echo "-- extras"
echo "public voucher_actions state voucher_states_state
public ad_actions state action_states_state
public ad_queues queue ad_actions_queue
public store_actions state store_action_states_state
" | while read nsp table column enum; do
	if [ "$archives" != "1" -a "$nsp" != "public" ]; then
		continue;
	fi
	echo "ALTER TABLE $nsp.$table ADD ${column}_enum public.enum_${enum} DEFAULT NULL;"
	if [ "$slave" != "1" ]; then
		echo "CREATE TRIGGER trigger_${table}_${column}_enum BEFORE INSERT OR UPDATE ON $nsp.$table FOR EACH ROW EXECUTE PROCEDURE public.enum_trigger_${enum} ();"
	fi
done

# XXX indexes

