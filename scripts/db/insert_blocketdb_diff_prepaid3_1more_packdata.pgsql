INSERT INTO packs (pack_id, account_id, status, type, slots, used, date_start, date_end, product_id) VALUES (4, 5, 'active', 'car', 10, 1, NOW(), NOW() + interval '1 day', 11010);

--Change all passwords to 123123123
update ads set salted_passwd =  E'$1024$2y1bQ}VYRD}zF4QCbf07a971b784888658a20e62156de64dfde5ffb6', passwd = null;
update accounts set salted_passwd =  E'$1024$2y1bQ}VYRD}zF4QCbf07a971b784888658a20e62156de64dfde5ffb6';
