--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET client_encoding = 'LATIN1';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;

SET search_path = public, pg_catalog;

--
-- Truncate al tables form public scheme
--
BEGIN;
SET CONSTRAINTS ALL DEFERRED;

TRUNCATE TABLE public.admins CASCADE;
TRUNCATE TABLE public.abuse_locks CASCADE;
TRUNCATE TABLE public.abuse_reporters CASCADE;
TRUNCATE TABLE public.users CASCADE;
TRUNCATE TABLE public.abuse_reports CASCADE;
TRUNCATE TABLE public.ads CASCADE;
TRUNCATE TABLE public.dashboard_ads CASCADE;
TRUNCATE TABLE public.payment_groups CASCADE;
TRUNCATE TABLE public.ad_actions CASCADE;
TRUNCATE TABLE public.action_params CASCADE;
TRUNCATE TABLE public.stores CASCADE;
TRUNCATE TABLE public.tokens CASCADE;
TRUNCATE TABLE public.action_states CASCADE;
TRUNCATE TABLE public.ad_changes CASCADE;
TRUNCATE TABLE public.ad_codes CASCADE;
TRUNCATE TABLE public.ad_image_changes CASCADE;
TRUNCATE TABLE public.ad_images CASCADE;
TRUNCATE TABLE public.ad_images_digests CASCADE;
TRUNCATE TABLE public.ad_media CASCADE;
TRUNCATE TABLE public.ad_media_changes CASCADE;
TRUNCATE TABLE public.ad_params CASCADE;
TRUNCATE TABLE public.ad_queues CASCADE;
TRUNCATE TABLE public.admin_privs CASCADE;
TRUNCATE TABLE public.bid_ads CASCADE;
TRUNCATE TABLE public.bid_bids CASCADE;
TRUNCATE TABLE public.bid_media CASCADE;
TRUNCATE TABLE public.block_lists CASCADE;
TRUNCATE TABLE public.block_rules CASCADE;
TRUNCATE TABLE public.block_rule_conditions CASCADE;
TRUNCATE TABLE public.blocked_items CASCADE;
TRUNCATE TABLE public.conf CASCADE;
TRUNCATE TABLE public.event_log CASCADE;
TRUNCATE TABLE public.example CASCADE;
TRUNCATE TABLE public.filters CASCADE;
TRUNCATE TABLE public.hold_mail_params CASCADE;
TRUNCATE TABLE public.iteminfo_items CASCADE;
TRUNCATE TABLE public.iteminfo_data CASCADE;
TRUNCATE TABLE public.mail_log CASCADE;
TRUNCATE TABLE public.mail_queue CASCADE;
TRUNCATE TABLE public.mama_attribute_wordlists CASCADE;
TRUNCATE TABLE public.mama_attribute_categories CASCADE;
TRUNCATE TABLE public.mama_main_backup CASCADE;
TRUNCATE TABLE public.mama_attribute_categories_backup CASCADE;
TRUNCATE TABLE public.mama_attribute_wordlists_backup CASCADE;
TRUNCATE TABLE public.mama_attribute_words CASCADE;
TRUNCATE TABLE public.mama_attribute_words_backup CASCADE;
TRUNCATE TABLE public.mama_exception_lists CASCADE;
TRUNCATE TABLE public.mama_exception_lists_backup CASCADE;
TRUNCATE TABLE public.mama_exception_words CASCADE;
TRUNCATE TABLE public.mama_exception_words_backup CASCADE;
TRUNCATE TABLE public.mama_wordlists CASCADE;
TRUNCATE TABLE public.mama_wordlists_backup CASCADE;
TRUNCATE TABLE public.mama_words CASCADE;
TRUNCATE TABLE public.mama_words_backup CASCADE;
TRUNCATE TABLE public.most_popular_ads CASCADE;
TRUNCATE TABLE public.notices CASCADE;
TRUNCATE TABLE public.on_call CASCADE;
TRUNCATE TABLE public.on_call_actions CASCADE;
TRUNCATE TABLE public.pageviews_per_reg_cat CASCADE;
TRUNCATE TABLE public.pay_log CASCADE;
TRUNCATE TABLE public.pay_log_references CASCADE;
TRUNCATE TABLE public.payments CASCADE;
TRUNCATE TABLE public.pricelist CASCADE;
TRUNCATE TABLE public.redir_stats CASCADE;
TRUNCATE TABLE public.review_log CASCADE;
TRUNCATE TABLE public.sms_users CASCADE;
TRUNCATE TABLE public.sms_log CASCADE;
TRUNCATE TABLE public.watch_users CASCADE;
TRUNCATE TABLE public.watch_queries CASCADE;
TRUNCATE TABLE public.sms_log_watch CASCADE;
TRUNCATE TABLE public.state_params CASCADE;
TRUNCATE TABLE public.stats_daily CASCADE;
TRUNCATE TABLE public.stats_daily_ad_actions CASCADE;
TRUNCATE TABLE public.stats_hourly CASCADE;
TRUNCATE TABLE public.store_actions CASCADE;
TRUNCATE TABLE public.store_action_states CASCADE;
TRUNCATE TABLE public.store_changes CASCADE;
TRUNCATE TABLE public.store_login_tokens CASCADE;
TRUNCATE TABLE public.store_params CASCADE;
TRUNCATE TABLE public.synonyms CASCADE;
TRUNCATE TABLE public.trans_queue CASCADE;
TRUNCATE TABLE public.unfinished_ads CASCADE;
TRUNCATE TABLE public.user_params CASCADE;
TRUNCATE TABLE public.user_testimonial CASCADE;
TRUNCATE TABLE public.visitor CASCADE;
TRUNCATE TABLE public.visits CASCADE;
TRUNCATE TABLE public.vouchers CASCADE;
TRUNCATE TABLE public.voucher_actions CASCADE;
TRUNCATE TABLE public.voucher_states CASCADE;
TRUNCATE TABLE public.watch_ads CASCADE;

COMMIT;

--
-- Name: mail_queue_mail_queue_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dany
--

SELECT pg_catalog.setval('mail_queue_mail_queue_id_seq', 1, false);


--
-- Name: abuse_locks_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dany
--

SELECT pg_catalog.setval('abuse_locks_id_seq', 1, false);


--
-- Name: report_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dany
--

SELECT pg_catalog.setval('report_id_seq', 1, false);


--
-- Name: action_states_state_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dany
--

SELECT pg_catalog.setval('action_states_state_id_seq', 200, false);


--
-- Name: admins_admin_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dany
--

SELECT pg_catalog.setval('admins_admin_id_seq', 22, true);


--
-- Name: ads_ad_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dany
--

SELECT pg_catalog.setval('ads_ad_id_seq', 20, false);


--
-- Name: adwatch_code_seq; Type: SEQUENCE SET; Schema: public; Owner: dany
--

SELECT pg_catalog.setval('adwatch_code_seq', 1, false);


--
-- Name: bid_ads_bid_ad_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dany
--

SELECT pg_catalog.setval('bid_ads_bid_ad_id_seq', 1, false);


--
-- Name: bid_bids_bid_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dany
--

SELECT pg_catalog.setval('bid_bids_bid_id_seq', 1, false);


--
-- Name: block_lists_list_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dany
--

SELECT pg_catalog.setval('block_lists_list_id_seq', 100, false);


--
-- Name: block_rule_conditions_condition_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dany
--

SELECT pg_catalog.setval('block_rule_conditions_condition_id_seq', 18, true);


--
-- Name: block_rules_rule_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dany
--

SELECT pg_catalog.setval('block_rules_rule_id_seq', 100, false);


--
-- Name: blocked_items_item_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dany
--

SELECT pg_catalog.setval('blocked_items_item_id_seq', 12, true);


--
-- Name: data_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dany
--

SELECT pg_catalog.setval('data_id_seq', 1294, true);


--
-- Name: event_log_event_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dany
--

SELECT pg_catalog.setval('event_log_event_id_seq', 1, false);


--
-- Name: filters_filter_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dany
--

SELECT pg_catalog.setval('filters_filter_id_seq', 1, false);


--
-- Name: hold_mail_params_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dany
--

SELECT pg_catalog.setval('hold_mail_params_id_seq', 1, false);


--
-- Name: item_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dany
--

SELECT pg_catalog.setval('item_id_seq', 59, true);


--
-- Name: list_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dany
--

SELECT pg_catalog.setval('list_id_seq', 8000000, false);


--
-- Name: mail_log_mail_log_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dany
--

SELECT pg_catalog.setval('mail_log_mail_log_id_seq', 1, false);


--
-- Name: mama_attribute_wordlists_attribute_wordlist_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dany
--

SELECT pg_catalog.setval('mama_attribute_wordlists_attribute_wordlist_id_seq', 1, false);


--
-- Name: mama_attribute_words_word_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dany
--

SELECT pg_catalog.setval('mama_attribute_words_word_id_seq', 1, false);


--
-- Name: mama_exception_lists_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dany
--

SELECT pg_catalog.setval('mama_exception_lists_id_seq', 1, false);


--
-- Name: mama_exception_words_exception_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dany
--

SELECT pg_catalog.setval('mama_exception_words_exception_id_seq', 1, false);


--
-- Name: mama_main_backup_backup_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dany
--

SELECT pg_catalog.setval('mama_main_backup_backup_id_seq', 1, false);


--
-- Name: mama_wordlists_wordlist_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dany
--

SELECT pg_catalog.setval('mama_wordlists_wordlist_id_seq', 1, false);


--
-- Name: mama_words_word_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dany
--

SELECT pg_catalog.setval('mama_words_word_id_seq', 1, false);


--
-- Name: most_popular_ads_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dany
--

SELECT pg_catalog.setval('most_popular_ads_id_seq', 1, false);


--
-- Name: next_image_id; Type: SEQUENCE SET; Schema: public; Owner: dany
--

SELECT pg_catalog.setval('next_image_id', 1, false);


--
-- Name: notices_notice_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dany
--

SELECT pg_catalog.setval('notices_notice_id_seq', 1, false);


--
-- Name: on_call_actions_action_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dany
--

SELECT pg_catalog.setval('on_call_actions_action_id_seq', 1, false);


--
-- Name: on_call_on_call_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dany
--

SELECT pg_catalog.setval('on_call_on_call_id_seq', 1, false);


--
-- Name: order_id_suffix_seq; Type: SEQUENCE SET; Schema: public; Owner: dany
--

SELECT pg_catalog.setval('order_id_suffix_seq', 1, false);


--
-- Name: pay_code_seq; Type: SEQUENCE SET; Schema: public; Owner: dany
--

SELECT pg_catalog.setval('pay_code_seq', 56, true);


--
-- Name: pay_log_pay_log_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dany
--

SELECT pg_catalog.setval('pay_log_pay_log_id_seq', 1, false);


--
-- Name: payment_groups_payment_group_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dany
--

SELECT pg_catalog.setval('payment_groups_payment_group_id_seq', 60, false);


--
-- Name: redir_stats_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dany
--

SELECT pg_catalog.setval('redir_stats_id_seq', 1, false);


--
-- Name: reporter_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dany
--

SELECT pg_catalog.setval('reporter_id_seq', 1, false);


--
-- Name: sms_log_sms_log_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dany
--

SELECT pg_catalog.setval('sms_log_sms_log_id_seq', 1, false);


--
-- Name: sms_users_sms_user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dany
--

SELECT pg_catalog.setval('sms_users_sms_user_id_seq', 1, false);


--
-- Name: stats_daily_ad_actions_stat_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dany
--

SELECT pg_catalog.setval('stats_daily_ad_actions_stat_id_seq', 1, false);


--
-- Name: store_action_states_state_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dany
--

SELECT pg_catalog.setval('store_action_states_state_id_seq', 1, false);


--
-- Name: store_login_tokens_store_login_token_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dany
--

SELECT pg_catalog.setval('store_login_tokens_store_login_token_id_seq', 1, false);


--
-- Name: stores_store_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dany
--

SELECT pg_catalog.setval('stores_store_id_seq', 1, false);


--
-- Name: synonyms_syn_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dany
--

SELECT pg_catalog.setval('synonyms_syn_id_seq', 245, true);


--
-- Name: tokens_token_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dany
--

SELECT pg_catalog.setval('tokens_token_id_seq', 1, false);


--
-- Name: trans_queue_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dany
--

SELECT pg_catalog.setval('trans_queue_id_seq', 17, false);


--
-- Name: uid_seq; Type: SEQUENCE SET; Schema: public; Owner: dany
--

SELECT pg_catalog.setval('uid_seq', 50, false);


--
-- Name: unfinished_ads_unf_ad_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dany
--

SELECT pg_catalog.setval('unfinished_ads_unf_ad_id_seq', 1, false);


--
-- Name: user_testimonial_user_testimonial_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dany
--

SELECT pg_catalog.setval('user_testimonial_user_testimonial_id_seq', 1, false);


--
-- Name: users_user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dany
--

SELECT pg_catalog.setval('users_user_id_seq', 50, false);


--
-- Name: verify_code_seq; Type: SEQUENCE SET; Schema: public; Owner: dany
--

SELECT pg_catalog.setval('verify_code_seq', 90, true);


--
-- Name: voucher_actions_voucher_action_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dany
--

SELECT pg_catalog.setval('voucher_actions_voucher_action_id_seq', 1, false);


--
-- Name: voucher_states_voucher_state_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dany
--

SELECT pg_catalog.setval('voucher_states_voucher_state_id_seq', 1, false);


--
-- Name: vouchers_voucher_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dany
--

SELECT pg_catalog.setval('vouchers_voucher_id_seq', 1, false);


--
-- Name: watch_users_watch_user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dany
--

SELECT pg_catalog.setval('watch_users_watch_user_id_seq', 1, false);


SET search_path = blocket_2009, pg_catalog;

--
-- Data for Name: ads; Type: TABLE DATA; Schema: blocket_2009; Owner: dany
--



--
-- Data for Name: payment_groups; Type: TABLE DATA; Schema: blocket_2009; Owner: dany
--



--
-- Data for Name: ad_actions; Type: TABLE DATA; Schema: blocket_2009; Owner: dany
--



--
-- Data for Name: action_params; Type: TABLE DATA; Schema: blocket_2009; Owner: dany
--



--
-- Data for Name: tokens; Type: TABLE DATA; Schema: blocket_2009; Owner: dany
--



--
-- Data for Name: action_states; Type: TABLE DATA; Schema: blocket_2009; Owner: dany
--



--
-- Data for Name: ad_changes; Type: TABLE DATA; Schema: blocket_2009; Owner: dany
--



--
-- Data for Name: ad_image_changes; Type: TABLE DATA; Schema: blocket_2009; Owner: dany
--



--
-- Data for Name: ad_images; Type: TABLE DATA; Schema: blocket_2009; Owner: dany
--



--
-- Data for Name: ad_media; Type: TABLE DATA; Schema: blocket_2009; Owner: dany
--



--
-- Data for Name: ad_media_changes; Type: TABLE DATA; Schema: blocket_2009; Owner: dany
--



--
-- Data for Name: ad_params; Type: TABLE DATA; Schema: blocket_2009; Owner: dany
--



--
-- Data for Name: mail_queue; Type: TABLE DATA; Schema: blocket_2009; Owner: dany
--



--
-- Data for Name: notices; Type: TABLE DATA; Schema: blocket_2009; Owner: dany
--



--
-- Data for Name: pay_log; Type: TABLE DATA; Schema: blocket_2009; Owner: dany
--



--
-- Data for Name: pay_log_references; Type: TABLE DATA; Schema: blocket_2009; Owner: dany
--



--
-- Data for Name: payments; Type: TABLE DATA; Schema: blocket_2009; Owner: dany
--



--
-- Data for Name: state_params; Type: TABLE DATA; Schema: blocket_2009; Owner: dany
--



--
-- Data for Name: voucher_actions; Type: TABLE DATA; Schema: blocket_2009; Owner: dany
--



--
-- Data for Name: voucher_states; Type: TABLE DATA; Schema: blocket_2009; Owner: dany
--



SET search_path = blocket_2010, pg_catalog;

--
-- Data for Name: ads; Type: TABLE DATA; Schema: blocket_2010; Owner: dany
--



--
-- Data for Name: payment_groups; Type: TABLE DATA; Schema: blocket_2010; Owner: dany
--



--
-- Data for Name: ad_actions; Type: TABLE DATA; Schema: blocket_2010; Owner: dany
--



--
-- Data for Name: action_params; Type: TABLE DATA; Schema: blocket_2010; Owner: dany
--



--
-- Data for Name: tokens; Type: TABLE DATA; Schema: blocket_2010; Owner: dany
--



--
-- Data for Name: action_states; Type: TABLE DATA; Schema: blocket_2010; Owner: dany
--



--
-- Data for Name: ad_changes; Type: TABLE DATA; Schema: blocket_2010; Owner: dany
--



--
-- Data for Name: ad_image_changes; Type: TABLE DATA; Schema: blocket_2010; Owner: dany
--



--
-- Data for Name: ad_images; Type: TABLE DATA; Schema: blocket_2010; Owner: dany
--



--
-- Data for Name: ad_media; Type: TABLE DATA; Schema: blocket_2010; Owner: dany
--



--
-- Data for Name: ad_media_changes; Type: TABLE DATA; Schema: blocket_2010; Owner: dany
--



--
-- Data for Name: ad_params; Type: TABLE DATA; Schema: blocket_2010; Owner: dany
--



--
-- Data for Name: mail_queue; Type: TABLE DATA; Schema: blocket_2010; Owner: dany
--



--
-- Data for Name: notices; Type: TABLE DATA; Schema: blocket_2010; Owner: dany
--



--
-- Data for Name: pay_log; Type: TABLE DATA; Schema: blocket_2010; Owner: dany
--



--
-- Data for Name: pay_log_references; Type: TABLE DATA; Schema: blocket_2010; Owner: dany
--



--
-- Data for Name: payments; Type: TABLE DATA; Schema: blocket_2010; Owner: dany
--



--
-- Data for Name: state_params; Type: TABLE DATA; Schema: blocket_2010; Owner: dany
--



--
-- Data for Name: voucher_actions; Type: TABLE DATA; Schema: blocket_2010; Owner: dany
--



--
-- Data for Name: voucher_states; Type: TABLE DATA; Schema: blocket_2010; Owner: dany
--



SET search_path = blocket_2011, pg_catalog;

--
-- Data for Name: ads; Type: TABLE DATA; Schema: blocket_2011; Owner: dany
--



--
-- Data for Name: payment_groups; Type: TABLE DATA; Schema: blocket_2011; Owner: dany
--



--
-- Data for Name: ad_actions; Type: TABLE DATA; Schema: blocket_2011; Owner: dany
--



--
-- Data for Name: action_params; Type: TABLE DATA; Schema: blocket_2011; Owner: dany
--



--
-- Data for Name: tokens; Type: TABLE DATA; Schema: blocket_2011; Owner: dany
--



--
-- Data for Name: action_states; Type: TABLE DATA; Schema: blocket_2011; Owner: dany
--



--
-- Data for Name: ad_changes; Type: TABLE DATA; Schema: blocket_2011; Owner: dany
--



--
-- Data for Name: ad_image_changes; Type: TABLE DATA; Schema: blocket_2011; Owner: dany
--



--
-- Data for Name: ad_images; Type: TABLE DATA; Schema: blocket_2011; Owner: dany
--



--
-- Data for Name: ad_media; Type: TABLE DATA; Schema: blocket_2011; Owner: dany
--



--
-- Data for Name: ad_media_changes; Type: TABLE DATA; Schema: blocket_2011; Owner: dany
--



--
-- Data for Name: ad_params; Type: TABLE DATA; Schema: blocket_2011; Owner: dany
--



--
-- Data for Name: mail_queue; Type: TABLE DATA; Schema: blocket_2011; Owner: dany
--



--
-- Data for Name: notices; Type: TABLE DATA; Schema: blocket_2011; Owner: dany
--



--
-- Data for Name: pay_log; Type: TABLE DATA; Schema: blocket_2011; Owner: dany
--



--
-- Data for Name: pay_log_references; Type: TABLE DATA; Schema: blocket_2011; Owner: dany
--



--
-- Data for Name: payments; Type: TABLE DATA; Schema: blocket_2011; Owner: dany
--



--
-- Data for Name: state_params; Type: TABLE DATA; Schema: blocket_2011; Owner: dany
--



--
-- Data for Name: voucher_actions; Type: TABLE DATA; Schema: blocket_2011; Owner: dany
--



--
-- Data for Name: voucher_states; Type: TABLE DATA; Schema: blocket_2011; Owner: dany
--



SET search_path = blocket_2012, pg_catalog;

--
-- Data for Name: ads; Type: TABLE DATA; Schema: blocket_2012; Owner: dany
--



--
-- Data for Name: payment_groups; Type: TABLE DATA; Schema: blocket_2012; Owner: dany
--



--
-- Data for Name: ad_actions; Type: TABLE DATA; Schema: blocket_2012; Owner: dany
--



--
-- Data for Name: action_params; Type: TABLE DATA; Schema: blocket_2012; Owner: dany
--



--
-- Data for Name: tokens; Type: TABLE DATA; Schema: blocket_2012; Owner: dany
--



--
-- Data for Name: action_states; Type: TABLE DATA; Schema: blocket_2012; Owner: dany
--



--
-- Data for Name: ad_changes; Type: TABLE DATA; Schema: blocket_2012; Owner: dany
--



--
-- Data for Name: ad_image_changes; Type: TABLE DATA; Schema: blocket_2012; Owner: dany
--



--
-- Data for Name: ad_images; Type: TABLE DATA; Schema: blocket_2012; Owner: dany
--



--
-- Data for Name: ad_media; Type: TABLE DATA; Schema: blocket_2012; Owner: dany
--



--
-- Data for Name: ad_media_changes; Type: TABLE DATA; Schema: blocket_2012; Owner: dany
--



--
-- Data for Name: ad_params; Type: TABLE DATA; Schema: blocket_2012; Owner: dany
--



--
-- Data for Name: mail_queue; Type: TABLE DATA; Schema: blocket_2012; Owner: dany
--



--
-- Data for Name: notices; Type: TABLE DATA; Schema: blocket_2012; Owner: dany
--



--
-- Data for Name: pay_log; Type: TABLE DATA; Schema: blocket_2012; Owner: dany
--



--
-- Data for Name: pay_log_references; Type: TABLE DATA; Schema: blocket_2012; Owner: dany
--



--
-- Data for Name: payments; Type: TABLE DATA; Schema: blocket_2012; Owner: dany
--



--
-- Data for Name: state_params; Type: TABLE DATA; Schema: blocket_2012; Owner: dany
--



--
-- Data for Name: voucher_actions; Type: TABLE DATA; Schema: blocket_2012; Owner: dany
--



--
-- Data for Name: voucher_states; Type: TABLE DATA; Schema: blocket_2012; Owner: dany
--



SET search_path = public, pg_catalog;

--
-- Data for Name: admins; Type: TABLE DATA; Schema: public; Owner: dany
--

INSERT INTO admins VALUES (2, 'erik', '2d541162141d281abb9f5ba1b14f8072a1d2d29b', 'Erik', 'erik@jabber', 'erik@blocket.se', '070-111111', 'active');
INSERT INTO admins VALUES (3, 'zakay', '2d541162141d281abb9f5ba1b14f8072a1d2d29b', 'Zakay', 'zakay@jabber', 'zakay@blocket.se', '070-111111', 'active');
INSERT INTO admins VALUES (4, 'thomas', 'a94a8fe5ccb19ba61c4c0873d391e987982fbbd3', 'Thomas', 'thomas@jabber', 'test@schibstediberica.es', '070-111111', 'active');
INSERT INTO admins VALUES (5, 'kjell', '430985da851e9223368e820ebde5beaf6c6ef1cc', 'Kjell', 'kjell@jabber', 'kjell@blocket.se', '0709-6459256', 'active');
INSERT INTO admins VALUES (6, 'torsten', '430985da851e9223368e820ebde5beaf6c6ef1cc', 'Torsten Svensson', 'torsten@jabber', 'svara-inte@blocket.se', '0709-6459256', 'deleted');
INSERT INTO admins VALUES (50, 'mama', '67b757a4f76e5fda3fe86c8a9009630a5e53a053', 'M.A.M.A.', 'blocket1@jabber', 'blocket1@blocket.se', '0733555501', 'active');
INSERT INTO admins VALUES (51, 'bender00', '67b757a4f76e5fda3fe86c8a9009630a5e53a053', 'M.A.M.A. bender 00', NULL, NULL, NULL, 'active');
INSERT INTO admins VALUES (52, 'bender01', '67b757a4f76e5fda3fe86c8a9009630a5e53a053', 'M.A.M.A. bender 01', NULL, NULL, NULL, 'active');
INSERT INTO admins VALUES (53, 'bender02', '67b757a4f76e5fda3fe86c8a9009630a5e53a053', 'M.A.M.A. bender 02', NULL, NULL, NULL, 'active');
INSERT INTO admins VALUES (54, 'bender03', '67b757a4f76e5fda3fe86c8a9009630a5e53a053', 'M.A.M.A. bender 03', NULL, NULL, NULL, 'active');
INSERT INTO admins VALUES (55, 'bender04', '67b757a4f76e5fda3fe86c8a9009630a5e53a053', 'M.A.M.A. bender 04', NULL, NULL, NULL, 'active');
INSERT INTO admins VALUES (9, 'dany', '118ab5d614b5a8d4222fc7eee1609793e4014800', NULL, NULL, 'dany@tetsuo.schibsted.cl', NULL, 'active');


--
-- Data for Name: abuse_locks; Type: TABLE DATA; Schema: public; Owner: dany
--



--
-- Data for Name: abuse_reporters; Type: TABLE DATA; Schema: public; Owner: dany
--



--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: dany
--

INSERT INTO users VALUES (1, 1, 'uid1@blocket.se', 0, 0);
INSERT INTO users VALUES (2, 2, 'uid2@blocket.se', 0, 100);
INSERT INTO users VALUES (3, 3, 'prepaid3@blocket.se', 1000, 1000);
INSERT INTO users VALUES (4, 4, 'prepaid@blocket.se', 100000, 1000);
INSERT INTO users VALUES (5, 5, 'prepaid5@blocket.se', 1000, 1000);
INSERT INTO users VALUES (6, 6, 'kim@blocket.se', 1000, 1000);
INSERT INTO users VALUES (50, 50, 'boris@schibsted.cl', 0, 0);
INSERT INTO users VALUES (51, 50, 'dany@schibsted.cl', 0, 0);


--
-- Data for Name: abuse_reports; Type: TABLE DATA; Schema: public; Owner: dany
--



--
-- Data for Name: ads; Type: TABLE DATA; Schema: public; Owner: dany
--

INSERT INTO ads VALUES (1, 6000667, '2011-11-09 11:00:00', 'active', 'sell', 'Aurelio Rodr�guez', '1231231231', 2, 0, 1020, 5, '11111', false, false, false, 'Departamento Tarapac�', 'Con 2 dormitorios, en Alto Hospicio, 250m2 de espacio y 2 plazas de garaje', 45000000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'es');
INSERT INTO ads VALUES (2, NULL, NULL, 'inactive', 'sell', 'Thomas Svensson', '08-112233', 11, 0, 4100, 3, 'testb', false, false, false, 'Barncykel', 'Röd barncykel, 28 tum.', 666, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'es');
INSERT INTO ads VALUES (3, NULL, NULL, 'inactive', 'sell', 'Sven Ingvars', '08-121314', 11, 0, 4100, 5, 'testc', false, false, false, 'Damcykel', 'Damcykel med stång i mitten. 33,2 tum i plast.', 667, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'es');
INSERT INTO ads VALUES (4, NULL, NULL, 'inactive', 'sell', 'Klas Klasson', '08-121314', 11, 0, 4100, 5, 'testc', true, true, true, 'Hustomte', 'En tre fot stor hustomte.', 667, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'es');
INSERT INTO ads VALUES (5, NULL, NULL, 'inactive', 'sell', 'Dummy', '08-121314', 11, 0, 4100, 5, 'testc', false, false, false, 'Damcykel', 'Damcykel med stång i mitten. 33,2 tum i plast.', 667, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'es');
INSERT INTO ads VALUES (6, 3456789, '2006-04-05 09:21:31', 'active', 'sell', 'Bengt Bedrup', '08-121314', 10, 0, 2020, 5, '11111', true, true, false, 'Race car', 'Car with two doors ...
33.2-inch wheels made of plastic.', 11667, NULL, NULL, 'Testar länk', NULL, NULL, NULL, NULL, NULL, 'es');
INSERT INTO ads VALUES (7, NULL, NULL, 'inactive', 'sell', 'Dummy', '08-121314', 11, 0, 4100, 5, 'testc', false, false, false, 'Damcykel', 'Damcykel med stång i mitten. 33,2 tum i plast.', 667, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'es');
INSERT INTO ads VALUES (8, 6431717, '2006-04-06 09:21:31', 'active', 'sell', 'Juan P�rez', '084123456', 11, 0, 5020, 1, '11111', false, false, false, 'Una mesa', 'Y qu� mesa... menuda mesa!!', 157000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'es');
INSERT INTO ads VALUES (9, 999999, NULL, 'inactive', 'sell', 'Dummy', '08-121314', 11, 0, 4100, 1, 'testc', false, false, false, 'Damcykel med blahonga', 'Damcykel med stång rakt upp i mitten. 33,2 tum i plast.', 667, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'es');
INSERT INTO ads VALUES (10, 6000666, '2006-04-05 10:21:31', 'active', 'sell', 'Cristobal Col�n', '0812131491', 12, 0, 6020, 5, '11111', false, false, false, 'Pesas muy grandes', 'Una de 120 kg y la otra de 57, mas ligera.', 90000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'es');
INSERT INTO ads VALUES (11, 6394117, '2006-04-07 11:21:31', 'active', 'sell', 'Andrea Gonz�lez', '0987654321', 13, 0, 7060, 1, '11111', false, false, false, 'Peluquera a domicilio', 'Voy, corto el pelo, me pagas y m voy....', 12000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'es');
INSERT INTO ads VALUES (12, 6394118, '2011-11-06 11:21:31', 'active', 'let', 'Boris Felipe', '0987654000', 15, 0, 1040, 1, '11111', false, false, false, 'Arriendo mi preciosa casa en �u�oa', 'Dos habitaciones, metros cuadrados.... hect�reas dir�a yo... y plazas de garaje a tutipl�n', 500000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'es');
INSERT INTO ads VALUES (31, 8000011, '2012-11-07 18:45:29', 'active', 'buy', 'Boris Cruchet', '1234567890', 15, 0, 9020, 50, NULL, false, false, false, 'Busco articulos para bebe', 'Busco articulos para bebe
Busco articulos para bebe
Busco articulos para bebe
Busco articulos para bebe', 23000, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$=OuFc |Y=aKK4f''[8313372cecb937ca210ecd37029ec4e9bd5b4a6a', '2012-11-07 18:45:29.301937', 'es');
INSERT INTO ads VALUES (20, 8000000, '2012-11-07 18:44:32', 'active', 'sell', 'Boris Cruchet', '1234567890', 11, 0, 8020, 50, NULL, false, false, false, 'Otro producto', 'Aviso de otro producto para API', 1000, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$W/ Y({nR^(d=^2{)8094017a37b99b700eb2de73203167aaa6b7cef0', '2012-11-07 18:44:32.076978', 'es');
INSERT INTO ads VALUES (23, 8000003, '2012-11-07 18:44:45', 'active', 'let', 'dany', '123123123', 11, 0, 1040, 51, NULL, false, false, false, 'Linda Casa de gran tama�o', 'Arriendo mi casa', 140000, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$E9CBN`?>?nfl>i''Le48c1368e4a43b35da28af7fbf0052c591f5e64c', '2012-11-07 18:44:45.671224', 'es');
INSERT INTO ads VALUES (21, 8000001, '2012-11-07 18:44:34', 'active', 'sell', 'dany', '123123123', 11, 0, 1020, 51, NULL, false, false, false, 'Departamento en Valdivia', 'Lindo depto en valdivia', 25000000, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$-I9gR~PYT=jO|sHA7d5f84b7ee357b77bcef2df81625f89da6cd238f', '2012-11-07 18:44:34.906444', 'es');
INSERT INTO ads VALUES (26, 8000006, '2012-11-07 18:45:02', 'active', 'sell', 'Boris Cruchet', '1234567890', 8, 0, 7020, 50, NULL, false, false, false, 'Oferton de empleo', 'Buscamos community manager', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$#.Rj0duZn~?zQ}ywf29d01585011a8fd520f76788cc38692585beb99', '2012-11-07 18:45:02.182113', 'es');
INSERT INTO ads VALUES (22, 8000002, '2012-11-07 18:44:41', 'active', 'sell', 'Boris Cruchet', '1234567890', 11, 0, 7060, 50, NULL, false, false, false, 'Aviso de Servicio', 'Aviso de Servicio para Api Aviso de Servicio para Api Aviso de Servicio para Api Aviso de Servicio para Api', 12400, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$%|Rr7!LSOhE %k-kdb0b59c92a3f50a627565832d5e7ef1a21d7cec0', '2012-11-07 18:44:41.984225', 'es');
INSERT INTO ads VALUES (24, 8000004, '2012-11-07 18:44:52', 'active', 'sell', 'Boris Cruchet', '1234567890', 15, 0, 7040, 50, NULL, false, false, false, 'Busco empleo de programador', 'Soy muy diestro programando en Basic y Pascal', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$r!5r[`.>V\\7\\%06u6cf50485c1f8d346940e6e06813d3a1a50cfa7f8', '2012-11-07 18:44:52.931095', 'es');
INSERT INTO ads VALUES (29, 8000009, '2012-11-07 18:45:18', 'active', 'sell', 'Boris Cruchet', '1234567890', 15, 0, 4040, 50, NULL, false, false, false, 'Cartera Luis Guaton', 'Super filete', 320000, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$''xBN''0g~n$ZGw5G*ff06327b06ace795bfa10bf817ac353b2b2ccc88', '2012-11-07 18:45:18.562067', 'es');
INSERT INTO ads VALUES (25, 8000005, '2012-11-07 18:44:57', 'active', 'let', 'dany', '123123123', 11, 0, 1260, 51, NULL, false, false, false, 'Oficina en el centro', 'linda oficina en el centro de valdivia
piso 3', 50000, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$12]:blS*g!+J#AkHd7ac557830fddb341e9efd2fc9e71ce9657c3b20', '2012-11-07 18:44:57.022859', 'es');
INSERT INTO ads VALUES (27, 8000007, '2012-11-07 18:45:05', 'active', 'sell', 'dany', '123123123', 11, 0, 1080, 51, NULL, false, false, false, 'Local Comercial', 'espacioso local comercial en el centro', 40000, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$ecGfK,%Vz=Z>BB"K86f044d88c527c2a47d5e2af802d4a164d57a5df', '2012-11-07 18:45:05.171239', 'es');
INSERT INTO ads VALUES (35, 8000015, '2012-11-07 18:45:55', 'active', 'sell', 'Boris Cruchet', '1234567890', 6, 0, 2040, 50, NULL, false, false, true, 'Super camion', 'Test api Test api Test api Test api Test api Test api Test api Test api Test api Test api Test api Test api.

Test api Test api Test api Test api Test api Test api Test api Test api Test api Test api Test api Test api.

Test api Test api Test api Test api Test api Test api Test api Test api Test api Test api Test api Test api.', 7000000, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$Q/uiXO66>rs\\T[p985c65ecf5bb6a71689da19d6e3eac3b04188def6', '2012-11-07 18:45:55.99175', 'es');
INSERT INTO ads VALUES (28, 8000008, '2012-11-07 18:45:11', 'active', 'sell', 'dany', '123123123', 11, 0, 1100, 51, NULL, false, false, false, 'Terreno en Futrono', 'para que construyas tu casa como tu quieras', 12000000, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$9p=~G$PH>+>Cs=l%e32f96d7a8fa04c2c2c46ad2caa2676413e2dda9', '2012-11-07 18:45:11.856153', 'es');
INSERT INTO ads VALUES (30, 8000010, '2012-11-07 18:45:25', 'active', 'let', 'dany', '123123123', 11, 0, 1120, 51, NULL, false, false, false, 'Estacionamiento central', 'Estacionamiento mensual en el centro', 20000, NULL, NULL, NULL, NULL, NULL, NULL, '$1024${RG ?7fg=f3AK30I6f45e187e105702fa11b9ac36b0451177bcfa73c', '2012-11-07 18:45:25.116642', 'es');
INSERT INTO ads VALUES (33, 8000013, '2012-11-07 18:45:44', 'active', 'sell', 'dany', '123123123', 10, 0, 2020, 51, NULL, false, false, false, 'Chevrolet Camaro 2000', 'Lo vendo por que gasta demasiado combustible', 2000000, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$N)eu=wI$cbBQrWO e6393c2aa603bfab6150e0d3c65e71c1d6564e82', '2012-11-07 18:45:44.356053', 'es');
INSERT INTO ads VALUES (32, 8000012, '2012-11-07 18:45:37', 'active', 'sell', 'Boris Cruchet', '1234567890', 5, 0, 4020, 50, NULL, false, false, false, 'Terno de lana de yak', 'Terno de lana de yak
Terno de lana de yak
Terno de lana de yak
Terno de lana de yak
Terno de lana de yak', 1200000, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$gUhw,",uS.9~\\UUa16d6597c017496bccafb1c4742f32e1fca3f24b0', '2012-11-07 18:45:37.462247', 'es');
INSERT INTO ads VALUES (34, 8000014, '2012-11-07 18:45:49', 'active', 'sell', 'dany', '123123123', 10, 0, 2060, 51, NULL, false, false, false, 'Moto Kinlon', 'estupenda moto papeles al dia', 600000, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$v2/[,\\^Yo%/4\\HSjdc3e5fa8ba0a3ad39646d998812f41a74b79437e', '2012-11-07 18:45:49.769035', 'es');


--
-- Data for Name: payment_groups; Type: TABLE DATA; Schema: public; Owner: dany
--

INSERT INTO payment_groups VALUES (0, '012345678', 'unverified', '2012-11-08 11:08:43.752092', NULL);
INSERT INTO payment_groups VALUES (1, '123456789', 'unverified', '2012-11-08 11:08:43.754089', NULL);
INSERT INTO payment_groups VALUES (2, '11223', 'unpaid', '2012-11-08 11:08:43.754488', NULL);
INSERT INTO payment_groups VALUES (3, '200000001', 'unverified', '2012-11-08 11:08:43.754727', NULL);
INSERT INTO payment_groups VALUES (50, '59876', 'cleared', '2012-11-08 11:08:43.754956', NULL);
INSERT INTO payment_groups VALUES (60, '112200000', 'verified', '2012-11-07 18:32:17.399926', NULL);
INSERT INTO payment_groups VALUES (61, '112200001', 'verified', '2012-11-07 18:33:21.115411', NULL);
INSERT INTO payment_groups VALUES (62, '112200002', 'verified', '2012-11-07 18:33:59.530492', NULL);
INSERT INTO payment_groups VALUES (63, '112200003', 'verified', '2012-11-07 18:34:57.239955', NULL);
INSERT INTO payment_groups VALUES (64, '112200004', 'verified', '2012-11-07 18:35:39.10571', NULL);
INSERT INTO payment_groups VALUES (65, '112200005', 'verified', '2012-11-07 18:36:53.436739', NULL);
INSERT INTO payment_groups VALUES (66, '112200006', 'verified', '2012-11-07 18:37:13.727781', NULL);
INSERT INTO payment_groups VALUES (67, '112200007', 'verified', '2012-11-07 18:38:11.799809', NULL);
INSERT INTO payment_groups VALUES (68, '112200008', 'verified', '2012-11-07 18:39:18.105811', NULL);
INSERT INTO payment_groups VALUES (69, '112200009', 'verified', '2012-11-07 18:40:28.002906', NULL);
INSERT INTO payment_groups VALUES (70, '235392023', 'verified', '2012-11-07 18:40:38.489403', NULL);
INSERT INTO payment_groups VALUES (71, '370784046', 'verified', '2012-11-07 18:41:24.638537', NULL);
INSERT INTO payment_groups VALUES (72, '506176069', 'verified', '2012-11-07 18:42:05.546683', NULL);
INSERT INTO payment_groups VALUES (73, '641568092', 'verified', '2012-11-07 18:42:20.391131', NULL);
INSERT INTO payment_groups VALUES (74, '776960115', 'verified', '2012-11-07 18:43:22.494636', NULL);
INSERT INTO payment_groups VALUES (75, '912352138', 'verified', '2012-11-07 18:43:48.005484', NULL);


--
-- Data for Name: ad_actions; Type: TABLE DATA; Schema: public; Owner: dany
--

INSERT INTO ad_actions VALUES (1, 1, 'new', 154, 'accepted', 'normal', NULL, NULL, 0);
INSERT INTO ad_actions VALUES (2, 1, 'new', 4, 'unverified', 'normal', NULL, NULL, 1);
INSERT INTO ad_actions VALUES (3, 1, 'new', 6, 'unpaid', 'normal', NULL, NULL, 2);
INSERT INTO ad_actions VALUES (4, 1, 'new', 8, 'unverified', 'normal', NULL, NULL, 3);
INSERT INTO ad_actions VALUES (8, 1, 'new', 9, 'accepted', 'normal', NULL, NULL, NULL);
INSERT INTO ad_actions VALUES (10, 1, 'new', 11, 'accepted', 'normal', NULL, NULL, NULL);
INSERT INTO ad_actions VALUES (11, 1, 'new', 12, 'accepted', 'normal', NULL, NULL, NULL);
INSERT INTO ad_actions VALUES (6, 1, 'new', 53, 'accepted', 'normal', NULL, NULL, 50);
INSERT INTO ad_actions VALUES (9, 1, 'new', 43, 'refused', 'normal', NULL, NULL, NULL);
INSERT INTO ad_actions VALUES (12, 1, 'new', 164, 'accepted', 'normal', NULL, NULL, 0);
INSERT INTO ad_actions VALUES (33, 1, 'new', 275, 'accepted', 'normal', 2, '2012-11-07 18:50:31.016009', 73);
INSERT INTO ad_actions VALUES (34, 1, 'new', 278, 'accepted', 'normal', 2, '2012-11-07 18:50:45.905405', 74);
INSERT INTO ad_actions VALUES (35, 1, 'new', 279, 'accepted', 'normal', 2, '2012-11-07 18:50:45.905405', 75);
INSERT INTO ad_actions VALUES (20, 1, 'new', 250, 'accepted', 'normal', 2, '2012-11-07 18:49:23.985146', 60);
INSERT INTO ad_actions VALUES (21, 1, 'new', 251, 'accepted', 'normal', 2, '2012-11-07 18:49:23.985146', 61);
INSERT INTO ad_actions VALUES (22, 1, 'new', 254, 'accepted', 'normal', 2, '2012-11-07 18:49:36.689715', 62);
INSERT INTO ad_actions VALUES (23, 1, 'new', 255, 'accepted', 'normal', 2, '2012-11-07 18:49:36.689715', 63);
INSERT INTO ad_actions VALUES (24, 1, 'new', 258, 'accepted', 'normal', 2, '2012-11-07 18:49:47.021139', 64);
INSERT INTO ad_actions VALUES (25, 1, 'new', 259, 'accepted', 'normal', 2, '2012-11-07 18:49:47.021139', 65);
INSERT INTO ad_actions VALUES (26, 1, 'new', 262, 'accepted', 'normal', 2, '2012-11-07 18:49:59.201019', 66);
INSERT INTO ad_actions VALUES (27, 1, 'new', 263, 'accepted', 'normal', 2, '2012-11-07 18:49:59.201019', 67);
INSERT INTO ad_actions VALUES (28, 1, 'new', 266, 'accepted', 'normal', 2, '2012-11-07 18:50:06.81803', 68);
INSERT INTO ad_actions VALUES (29, 1, 'new', 267, 'accepted', 'normal', 2, '2012-11-07 18:50:06.81803', 69);
INSERT INTO ad_actions VALUES (30, 1, 'new', 270, 'accepted', 'normal', 2, '2012-11-07 18:50:20.14425', 70);
INSERT INTO ad_actions VALUES (31, 1, 'new', 271, 'accepted', 'normal', 2, '2012-11-07 18:50:20.14425', 71);
INSERT INTO ad_actions VALUES (32, 1, 'new', 274, 'accepted', 'normal', 2, '2012-11-07 18:50:31.016009', 72);


--
-- Data for Name: action_params; Type: TABLE DATA; Schema: public; Owner: dany
--

INSERT INTO action_params VALUES (20, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params VALUES (21, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params VALUES (22, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params VALUES (23, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params VALUES (24, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params VALUES (25, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params VALUES (26, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params VALUES (27, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params VALUES (28, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params VALUES (29, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params VALUES (30, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params VALUES (31, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params VALUES (32, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params VALUES (33, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params VALUES (34, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params VALUES (35, 1, 'redir', 'dW5rbm93bg==');


--
-- Data for Name: stores; Type: TABLE DATA; Schema: public; Owner: dany
--



--
-- Data for Name: tokens; Type: TABLE DATA; Schema: public; Owner: dany
--

INSERT INTO tokens VALUES (99999917, 'd12ac643ce5c39ddb765bc452d40790932d7c0f67823aaa', 5, '2006-04-06 15:19:46', '2006-04-06 16:19:46', '192.168.4.75', 'clear', NULL, NULL);
INSERT INTO tokens VALUES (99999918, 'a5e4e5b16bc9c806b1c22cfd638440034791acc68f23600', 5, '2006-04-06 16:19:31', '2006-04-06 16:19:46', '192.168.4.75', 'authenticate', NULL, NULL);
INSERT INTO tokens VALUES (99999919, 'a80cbcd0b019780754593a1ea8b28001c88086439101100', 5, '2006-04-06 16:19:46', '2006-04-06 16:19:46', '192.168.4.75', 'clear', NULL, NULL);
INSERT INTO tokens VALUES (99999992, 'b7e4e5b16bc9c806b1c22cfd638440034791acc68f23600', 5, '2011-11-06 05:19:31', '2011-11-06 05:19:46', '192.168.4.75', 'adqueues', NULL, NULL);
INSERT INTO tokens VALUES (99999993, 'b9e4e5b16bc9c806b1c22cfd638440034791acc68f23600', 5, '2011-11-06 05:19:31', '2011-11-06 05:19:46', '192.168.4.75', 'adqueues', NULL, NULL);
INSERT INTO tokens VALUES (99999998, 'b5e4e5b16bc9c806b1c22cfd638440034791acc68f23600', 5, '2006-04-06 15:19:31', '2006-04-06 15:19:46', '192.168.4.75', 'authenticate', NULL, NULL);
INSERT INTO tokens VALUES (99999999, '580cbcd0b019780754593a1ea8b28001c88086439101100', 5, '2006-04-06 15:19:46', '2006-04-06 16:19:46', '192.168.4.75', 'clear', NULL, NULL);
INSERT INTO tokens VALUES (99999994, 'b7e4e9b16bc9c806b1c22cfd638440034791acc68f23600', 5, '2011-11-06 05:19:31', '2011-11-06 05:19:46', '192.168.4.75', 'adqueues', NULL, NULL);
INSERT INTO tokens VALUES (99999995, 'b9e6e6b16bc9c806b1c22cfd638440034791acc68f23600', 5, '2011-11-06 06:19:31', '2011-11-06 06:19:46', '192.168.4.75', 'adqueues', NULL, NULL);
INSERT INTO tokens VALUES (3, 'X509ad636e8868100000000070a5d77500000000', 9, '2012-11-07 18:44:22.380339', '2012-11-07 18:44:23.983044', '10.0.1.53', 'adqueues', NULL, NULL);
INSERT INTO tokens VALUES (4, 'X509ad63874cc0b6f0000000030a8323600000000', 9, '2012-11-07 18:44:23.983044', '2012-11-07 18:44:24.022555', '10.0.1.53', 'adqueues', NULL, NULL);
INSERT INTO tokens VALUES (5, 'X509ad638372c0d2b00000000743b44ea00000000', 9, '2012-11-07 18:44:24.022555', '2012-11-07 18:44:24.027354', '10.0.1.53', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (6, 'X509ad63850230d5000000000647aa83f00000000', 9, '2012-11-07 18:44:24.027354', '2012-11-07 18:44:24.030726', '10.0.1.53', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (7, 'X509ad63874026485000000007f2cf47900000000', 9, '2012-11-07 18:44:24.030726', '2012-11-07 18:44:24.038015', '10.0.1.53', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (8, 'X509ad6383ef0a5d500000000e6b031300000000', 9, '2012-11-07 18:44:24.038015', '2012-11-07 18:44:24.040697', '10.0.1.53', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (9, 'X509ad63821199963000000004bcb144500000000', 9, '2012-11-07 18:44:24.040697', '2012-11-07 18:44:24.043927', '10.0.1.53', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (10, 'X509ad638137631f8000000005161a0b200000000', 9, '2012-11-07 18:44:24.043927', '2012-11-07 18:44:32.049972', '10.0.1.53', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (11, 'X509ad640508ca8880000000053b61fd500000000', 9, '2012-11-07 18:44:32.049972', '2012-11-07 18:44:34.903545', '10.0.1.53', 'review', NULL, NULL);
INSERT INTO tokens VALUES (12, 'X509ad64343dcde7000000003175d2e200000000', 9, '2012-11-07 18:44:34.903545', '2012-11-07 18:44:36.687673', '10.0.1.53', 'review', NULL, NULL);
INSERT INTO tokens VALUES (13, 'X509ad64579414f4400000000eac36f700000000', 9, '2012-11-07 18:44:36.687673', '2012-11-07 18:44:36.738027', '10.0.1.53', 'adqueues', NULL, NULL);
INSERT INTO tokens VALUES (14, 'X509ad6457933c3db000000004ea91d4300000000', 9, '2012-11-07 18:44:36.738027', '2012-11-07 18:44:36.740897', '10.0.1.53', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (15, 'X509ad6455a5e6d180000000028be0a7e00000000', 9, '2012-11-07 18:44:36.740897', '2012-11-07 18:44:36.743823', '10.0.1.53', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (16, 'X509ad6456e43e6f300000000db1595800000000', 9, '2012-11-07 18:44:36.743823', '2012-11-07 18:44:36.751695', '10.0.1.53', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (17, 'X509ad645770d3991000000004e4b220200000000', 9, '2012-11-07 18:44:36.751695', '2012-11-07 18:44:36.754199', '10.0.1.53', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (18, 'X509ad6456335c98200000000135cd3bf00000000', 9, '2012-11-07 18:44:36.754199', '2012-11-07 18:44:36.758234', '10.0.1.53', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (19, 'X509ad645776525dc000000004dd7295700000000', 9, '2012-11-07 18:44:36.758234', '2012-11-07 18:44:41.982489', '10.0.1.53', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (20, 'X509ad64a684006f000000004a3d89b300000000', 9, '2012-11-07 18:44:41.982489', '2012-11-07 18:44:45.669685', '10.0.1.53', 'review', NULL, NULL);
INSERT INTO tokens VALUES (21, 'X509ad64e4402ed41000000001ea85b0000000000', 9, '2012-11-07 18:44:45.669685', '2012-11-07 18:44:47.019696', '10.0.1.53', 'review', NULL, NULL);
INSERT INTO tokens VALUES (22, 'X509ad64f1fa8bc460000000058653fc400000000', 9, '2012-11-07 18:44:47.019696', '2012-11-07 18:44:47.050942', '10.0.1.53', 'adqueues', NULL, NULL);
INSERT INTO tokens VALUES (23, 'X509ad64f64a90ebd000000006bed011a00000000', 9, '2012-11-07 18:44:47.050942', '2012-11-07 18:44:47.053381', '10.0.1.53', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (24, 'X509ad64fc077aa60000000054464d0600000000', 9, '2012-11-07 18:44:47.053381', '2012-11-07 18:44:47.058944', '10.0.1.53', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (25, 'X509ad64f7e8116d00000000071c11c8e00000000', 9, '2012-11-07 18:44:47.058944', '2012-11-07 18:44:47.06608', '10.0.1.53', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (26, 'X509ad64f232d90d2000000004051c7000000000', 9, '2012-11-07 18:44:47.06608', '2012-11-07 18:44:47.068473', '10.0.1.53', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (27, 'X509ad64f10b63bfa00000000347f87a700000000', 9, '2012-11-07 18:44:47.068473', '2012-11-07 18:44:47.070954', '10.0.1.53', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (28, 'X509ad64f6562f65e000000001f3ea40a00000000', 9, '2012-11-07 18:44:47.070954', '2012-11-07 18:44:52.929337', '10.0.1.53', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (29, 'X509ad65525255f1c000000005a2f01cd00000000', 9, '2012-11-07 18:44:52.929337', '2012-11-07 18:44:57.021423', '10.0.1.53', 'review', NULL, NULL);
INSERT INTO tokens VALUES (30, 'X509ad6594fe6d640000000001e5922f800000000', 9, '2012-11-07 18:44:57.021423', '2012-11-07 18:44:59.199159', '10.0.1.53', 'review', NULL, NULL);
INSERT INTO tokens VALUES (31, 'X509ad65b28d81f11000000002a45435900000000', 9, '2012-11-07 18:44:59.199159', '2012-11-07 18:44:59.230776', '10.0.1.53', 'adqueues', NULL, NULL);
INSERT INTO tokens VALUES (32, 'X509ad65b24db6ee7000000006a49947000000000', 9, '2012-11-07 18:44:59.230776', '2012-11-07 18:44:59.23354', '10.0.1.53', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (33, 'X509ad65b1c1b090600000000688dd0e000000000', 9, '2012-11-07 18:44:59.23354', '2012-11-07 18:44:59.236025', '10.0.1.53', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (34, 'X509ad65b490a73510000000050f324ea00000000', 9, '2012-11-07 18:44:59.236025', '2012-11-07 18:44:59.241261', '10.0.1.53', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (35, 'X509ad65b292ca158000000006ba3fd6a00000000', 9, '2012-11-07 18:44:59.241261', '2012-11-07 18:44:59.243575', '10.0.1.53', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (36, 'X509ad65b1cc68ace000000007a4574d300000000', 9, '2012-11-07 18:44:59.243575', '2012-11-07 18:44:59.245501', '10.0.1.53', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (37, 'X509ad65b3d6b2ee30000000053f297f900000000', 9, '2012-11-07 18:44:59.245501', '2012-11-07 18:45:02.180603', '10.0.1.53', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (38, 'X509ad65e6e80b9bd00000000d8e3c3400000000', 9, '2012-11-07 18:45:02.180603', '2012-11-07 18:45:05.16856', '10.0.1.53', 'review', NULL, NULL);
INSERT INTO tokens VALUES (39, 'X509ad66147172d7600000000171c060400000000', 9, '2012-11-07 18:45:05.16856', '2012-11-07 18:45:06.719728', '10.0.1.53', 'review', NULL, NULL);
INSERT INTO tokens VALUES (40, 'X509ad66337f69cb1000000003e24670700000000', 9, '2012-11-07 18:45:06.719728', '2012-11-07 18:45:06.876064', '10.0.1.53', 'adqueues', NULL, NULL);
INSERT INTO tokens VALUES (41, 'X509ad663386d40390000000062831e4200000000', 9, '2012-11-07 18:45:06.876064', '2012-11-07 18:45:06.882134', '10.0.1.53', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (42, 'X509ad663cbb30ad00000000775de60e00000000', 9, '2012-11-07 18:45:06.882134', '2012-11-07 18:45:06.889839', '10.0.1.53', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (43, 'X509ad66370ee2155000000002dd4ca1100000000', 9, '2012-11-07 18:45:06.889839', '2012-11-07 18:45:06.902859', '10.0.1.53', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (44, 'X509ad6634328fa5300000000464534e00000000', 9, '2012-11-07 18:45:06.902859', '2012-11-07 18:45:06.908586', '10.0.1.53', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (45, 'X509ad6637f366ac30000000013b5a2db00000000', 9, '2012-11-07 18:45:06.908586', '2012-11-07 18:45:06.91292', '10.0.1.53', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (46, 'X509ad663581a73230000000037438aa00000000', 9, '2012-11-07 18:45:06.91292', '2012-11-07 18:45:11.854679', '10.0.1.53', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (47, 'X509ad668452b75bd00000000515bc26700000000', 9, '2012-11-07 18:45:11.854679', '2012-11-07 18:45:18.559004', '10.0.1.53', 'review', NULL, NULL);
INSERT INTO tokens VALUES (48, 'X509ad66f12206fa10000000064d4320400000000', 9, '2012-11-07 18:45:18.559004', '2012-11-07 18:45:20.139987', '10.0.1.53', 'review', NULL, NULL);
INSERT INTO tokens VALUES (49, 'X509ad67029c1022b0000000036fbde8800000000', 9, '2012-11-07 18:45:20.139987', '2012-11-07 18:45:20.188146', '10.0.1.53', 'adqueues', NULL, NULL);
INSERT INTO tokens VALUES (50, 'X509ad67065672806000000001b2c663300000000', 9, '2012-11-07 18:45:20.188146', '2012-11-07 18:45:20.193244', '10.0.1.53', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (64, 'X509ad67b78852e300000000317c7f5100000000', 9, '2012-11-07 18:45:31.072364', '2012-11-07 18:45:37.458457', '10.0.1.53', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (51, 'X509ad67051813ac6000000005ccc4de300000000', 9, '2012-11-07 18:45:20.193244', '2012-11-07 18:45:20.200153', '10.0.1.53', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (65, 'X509ad6814ec5a247000000003ff5931c00000000', 9, '2012-11-07 18:45:37.458457', '2012-11-07 18:45:44.351762', '10.0.1.53', 'review', NULL, NULL);
INSERT INTO tokens VALUES (52, 'X509ad67069038f8b0000000058053b3600000000', 9, '2012-11-07 18:45:20.200153', '2012-11-07 18:45:20.211468', '10.0.1.53', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (53, 'X509ad6702709d796000000002d067ccc00000000', 9, '2012-11-07 18:45:20.211468', '2012-11-07 18:45:20.216403', '10.0.1.53', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (66, 'X509ad68813ff9d94000000005b80d2f400000000', 9, '2012-11-07 18:45:44.351762', '2012-11-07 18:45:45.903318', '10.0.1.53', 'review', NULL, NULL);
INSERT INTO tokens VALUES (54, 'X509ad67076ad963600000000bb2e65300000000', 9, '2012-11-07 18:45:20.216403', '2012-11-07 18:45:20.221875', '10.0.1.53', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (55, 'X509ad67018f37de6000000002b510dc00000000', 9, '2012-11-07 18:45:20.221875', '2012-11-07 18:45:25.114952', '10.0.1.53', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (67, 'X509ad68a3753792a000000004edbee900000000', 9, '2012-11-07 18:45:45.903318', '2012-11-07 18:45:45.946053', '10.0.1.53', 'adqueues', NULL, NULL);
INSERT INTO tokens VALUES (56, 'X509ad6755ff9335900000000177494b700000000', 9, '2012-11-07 18:45:25.114952', '2012-11-07 18:45:29.300528', '10.0.1.53', 'review', NULL, NULL);
INSERT INTO tokens VALUES (68, 'X509ad68a37a64bd200000000dca78500000000', 9, '2012-11-07 18:45:45.946053', '2012-11-07 18:45:45.950685', '10.0.1.53', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (57, 'X509ad67974762d6a00000000326c42b00000000', 9, '2012-11-07 18:45:29.300528', '2012-11-07 18:45:31.014291', '10.0.1.53', 'review', NULL, NULL);
INSERT INTO tokens VALUES (58, 'X509ad67b1b79b1270000000052c696400000000', 9, '2012-11-07 18:45:31.014291', '2012-11-07 18:45:31.051177', '10.0.1.53', 'adqueues', NULL, NULL);
INSERT INTO tokens VALUES (69, 'X509ad68a246b0d6e000000005ccbaaef00000000', 9, '2012-11-07 18:45:45.950685', '2012-11-07 18:45:45.955316', '10.0.1.53', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (59, 'X509ad67b4f1dc6740000000045dc0b3100000000', 9, '2012-11-07 18:45:31.051177', '2012-11-07 18:45:31.054589', '10.0.1.53', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (60, 'X509ad67b1f89af6800000000182839c500000000', 9, '2012-11-07 18:45:31.054589', '2012-11-07 18:45:31.057911', '10.0.1.53', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (70, 'X509ad68a5b0ba952000000007451e3af00000000', 9, '2012-11-07 18:45:45.955316', '2012-11-07 18:45:45.963211', '10.0.1.53', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (61, 'X509ad67b16cf301b0000000048b650c100000000', 9, '2012-11-07 18:45:31.057911', '2012-11-07 18:45:31.066542', '10.0.1.53', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (62, 'X509ad67b3cc372f000000003395bae900000000', 9, '2012-11-07 18:45:31.066542', '2012-11-07 18:45:31.069277', '10.0.1.53', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (71, 'X509ad68a7b24cde7000000003e3c86300000000', 9, '2012-11-07 18:45:45.963211', '2012-11-07 18:45:45.966238', '10.0.1.53', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (63, 'X509ad67b42fbc594000000004137661300000000', 9, '2012-11-07 18:45:31.069277', '2012-11-07 18:45:31.072364', '10.0.1.53', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (72, 'X509ad68a1e97270800000000423bfb5d00000000', 9, '2012-11-07 18:45:45.966238', '2012-11-07 18:45:45.969462', '10.0.1.53', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (73, 'X509ad68a1affce6800000000568dc3b900000000', 9, '2012-11-07 18:45:45.969462', '2012-11-07 18:45:49.76722', '10.0.1.53', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (74, 'X509ad68e6062640000000066f66e00000000', 9, '2012-11-07 18:45:49.76722', '2012-11-07 18:45:55.990182', '10.0.1.53', 'review', NULL, NULL);
INSERT INTO tokens VALUES (75, 'X509ad69471ba29ec0000000051e19d2b00000000', 9, '2012-11-07 18:45:55.990182', '2012-11-07 18:45:57.488476', '10.0.1.53', 'review', NULL, NULL);
INSERT INTO tokens VALUES (76, 'X509ad6955d334451000000005abdb97700000000', 9, '2012-11-07 18:45:57.488476', '2012-11-07 18:45:57.491436', '10.0.1.53', 'adqueues', NULL, NULL);
INSERT INTO tokens VALUES (77, 'X509ad6959559d05000000007a7c737d00000000', 9, '2012-11-07 18:45:57.491436', '2012-11-07 19:45:57.491436', '10.0.1.53', 'adqueues', NULL, NULL);


--
-- Data for Name: action_states; Type: TABLE DATA; Schema: public; Owner: dany
--

INSERT INTO action_states VALUES (1, 1, 150, 'reg', 'initial', '2012-11-08 11:08:43.762279', '192.168.4.75', NULL);
INSERT INTO action_states VALUES (1, 1, 151, 'unverified', 'verify', '2012-11-08 11:08:43.764343', '192.168.4.75', NULL);
INSERT INTO action_states VALUES (1, 1, 152, 'pending_review', 'verify', '2012-11-08 11:08:43.764799', '192.168.4.75', NULL);
INSERT INTO action_states VALUES (1, 1, 153, 'locked', 'checkout', '2012-11-08 11:08:43.764985', '192.168.4.75', 99999992);
INSERT INTO action_states VALUES (1, 1, 154, 'accepted', 'accept', '2012-11-08 11:08:43.76523', '192.168.4.75', 99999993);
INSERT INTO action_states VALUES (2, 1, 3, 'reg', 'initial', '2012-11-08 11:08:43.765463', '192.168.4.85', NULL);
INSERT INTO action_states VALUES (2, 1, 4, 'unverified', 'verifymail', '2012-11-08 11:08:43.765665', '192.168.4.85', NULL);
INSERT INTO action_states VALUES (3, 1, 5, 'reg', 'initial', '2012-11-08 11:08:43.765846', '192.168.4.85', NULL);
INSERT INTO action_states VALUES (3, 1, 6, 'unpaid', 'paymail', '2012-11-08 11:08:43.76606', '192.168.4.85', NULL);
INSERT INTO action_states VALUES (4, 1, 7, 'reg', 'initial', '2012-11-08 11:08:43.766244', '192.168.4.85', NULL);
INSERT INTO action_states VALUES (4, 1, 8, 'unverified', 'verifymail', '2012-11-08 11:08:43.766413', '192.168.4.85', NULL);
INSERT INTO action_states VALUES (6, 1, 50, 'reg', 'initial', '2012-11-08 11:08:43.766581', '192.168.4.75', NULL);
INSERT INTO action_states VALUES (6, 1, 51, 'unpaid', 'newad', '2012-11-08 11:08:43.76675', '192.168.4.75', NULL);
INSERT INTO action_states VALUES (6, 1, 52, 'pending_review', 'pay', '2012-11-08 11:08:43.768386', '192.168.4.75', 99999998);
INSERT INTO action_states VALUES (6, 1, 53, 'accepted', 'accept', '2012-11-08 11:08:43.768587', '192.168.4.75', 99999999);
INSERT INTO action_states VALUES (9, 1, 40, 'reg', 'initial', '2012-11-08 11:08:43.768787', '192.168.4.75', NULL);
INSERT INTO action_states VALUES (9, 1, 41, 'unpaid', 'newad', '2012-11-08 11:08:43.76896', '192.168.4.75', NULL);
INSERT INTO action_states VALUES (9, 1, 42, 'pending_review', 'pay', '2012-11-08 11:08:43.769112', '192.168.4.75', 99999918);
INSERT INTO action_states VALUES (9, 1, 43, 'refused', 'refuse', '2012-11-08 11:08:43.769277', '192.168.4.75', 99999919);
INSERT INTO action_states VALUES (12, 1, 160, 'reg', 'initial', '2012-11-08 11:08:43.769466', '192.168.4.75', NULL);
INSERT INTO action_states VALUES (12, 1, 161, 'unverified', 'verify', '2012-11-08 11:08:43.769622', '192.168.4.75', NULL);
INSERT INTO action_states VALUES (12, 1, 162, 'pending_review', 'verify', '2012-11-08 11:08:43.769806', '192.168.4.75', NULL);
INSERT INTO action_states VALUES (12, 1, 163, 'locked', 'checkout', '2012-11-08 11:08:43.769981', '192.168.4.75', 99999994);
INSERT INTO action_states VALUES (12, 1, 164, 'accepted', 'accept', '2012-11-08 11:08:43.770159', '192.168.4.75', 99999995);
INSERT INTO action_states VALUES (10, 1, 54, 'reg', 'initial', '2012-11-08 11:20:00', '192.168.4.75', NULL);
INSERT INTO action_states VALUES (10, 1, 55, 'unpaid', 'newad', '2012-11-08 11:08:43.770948', '192.168.4.75', NULL);
INSERT INTO action_states VALUES (10, 1, 56, 'pending_review', 'pay', '2012-11-08 11:08:43.771123', '192.168.4.75', 99999998);
INSERT INTO action_states VALUES (10, 1, 57, 'accepted', 'accept', '2012-11-08 11:08:43.771314', '192.168.4.75', 99999999);
INSERT INTO action_states VALUES (20, 1, 202, 'pending_review', 'verify', '2012-11-07 18:32:17.509741', '10.0.1.24', NULL);
INSERT INTO action_states VALUES (21, 1, 203, 'reg', 'initial', '2012-11-07 18:33:21.115411', '10.0.1.53', NULL);
INSERT INTO action_states VALUES (21, 1, 204, 'unverified', 'verifymail', '2012-11-07 18:33:21.115411', '10.0.1.53', NULL);
INSERT INTO action_states VALUES (21, 1, 205, 'pending_review', 'verify', '2012-11-07 18:33:21.125714', '10.0.1.53', NULL);
INSERT INTO action_states VALUES (22, 1, 206, 'reg', 'initial', '2012-11-07 18:33:59.530492', '10.0.1.24', NULL);
INSERT INTO action_states VALUES (22, 1, 207, 'unverified', 'verifymail', '2012-11-07 18:33:59.530492', '10.0.1.24', NULL);
INSERT INTO action_states VALUES (22, 1, 208, 'pending_review', 'verify', '2012-11-07 18:33:59.535331', '10.0.1.24', NULL);
INSERT INTO action_states VALUES (23, 1, 209, 'reg', 'initial', '2012-11-07 18:34:57.239955', '10.0.1.53', NULL);
INSERT INTO action_states VALUES (23, 1, 210, 'unverified', 'verifymail', '2012-11-07 18:34:57.239955', '10.0.1.53', NULL);
INSERT INTO action_states VALUES (23, 1, 211, 'pending_review', 'verify', '2012-11-07 18:34:57.250137', '10.0.1.53', NULL);
INSERT INTO action_states VALUES (24, 1, 212, 'reg', 'initial', '2012-11-07 18:35:39.10571', '10.0.1.24', NULL);
INSERT INTO action_states VALUES (24, 1, 213, 'unverified', 'verifymail', '2012-11-07 18:35:39.10571', '10.0.1.24', NULL);
INSERT INTO action_states VALUES (24, 1, 214, 'pending_review', 'verify', '2012-11-07 18:35:39.109892', '10.0.1.24', NULL);
INSERT INTO action_states VALUES (25, 1, 215, 'reg', 'initial', '2012-11-07 18:36:53.436739', '10.0.1.53', NULL);
INSERT INTO action_states VALUES (25, 1, 216, 'unverified', 'verifymail', '2012-11-07 18:36:53.436739', '10.0.1.53', NULL);
INSERT INTO action_states VALUES (25, 1, 217, 'pending_review', 'verify', '2012-11-07 18:36:53.443143', '10.0.1.53', NULL);
INSERT INTO action_states VALUES (26, 1, 218, 'reg', 'initial', '2012-11-07 18:37:13.727781', '10.0.1.24', NULL);
INSERT INTO action_states VALUES (26, 1, 219, 'unverified', 'verifymail', '2012-11-07 18:37:13.727781', '10.0.1.24', NULL);
INSERT INTO action_states VALUES (26, 1, 220, 'pending_review', 'verify', '2012-11-07 18:37:13.736256', '10.0.1.24', NULL);
INSERT INTO action_states VALUES (27, 1, 221, 'reg', 'initial', '2012-11-07 18:38:11.799809', '10.0.1.53', NULL);
INSERT INTO action_states VALUES (27, 1, 222, 'unverified', 'verifymail', '2012-11-07 18:38:11.799809', '10.0.1.53', NULL);
INSERT INTO action_states VALUES (27, 1, 223, 'pending_review', 'verify', '2012-11-07 18:38:11.804033', '10.0.1.53', NULL);
INSERT INTO action_states VALUES (28, 1, 224, 'reg', 'initial', '2012-11-07 18:39:18.105811', '10.0.1.53', NULL);
INSERT INTO action_states VALUES (28, 1, 225, 'unverified', 'verifymail', '2012-11-07 18:39:18.105811', '10.0.1.53', NULL);
INSERT INTO action_states VALUES (28, 1, 226, 'pending_review', 'verify', '2012-11-07 18:39:18.118777', '10.0.1.53', NULL);
INSERT INTO action_states VALUES (29, 1, 227, 'reg', 'initial', '2012-11-07 18:40:28.002906', '10.0.1.24', NULL);
INSERT INTO action_states VALUES (29, 1, 228, 'unverified', 'verifymail', '2012-11-07 18:40:28.002906', '10.0.1.24', NULL);
INSERT INTO action_states VALUES (29, 1, 229, 'pending_review', 'verify', '2012-11-07 18:40:28.016664', '10.0.1.24', NULL);
INSERT INTO action_states VALUES (30, 1, 230, 'reg', 'initial', '2012-11-07 18:40:38.489403', '10.0.1.53', NULL);
INSERT INTO action_states VALUES (30, 1, 231, 'unverified', 'verifymail', '2012-11-07 18:40:38.489403', '10.0.1.53', NULL);
INSERT INTO action_states VALUES (30, 1, 232, 'pending_review', 'verify', '2012-11-07 18:40:38.500251', '10.0.1.53', NULL);
INSERT INTO action_states VALUES (31, 1, 233, 'reg', 'initial', '2012-11-07 18:41:24.638537', '10.0.1.24', NULL);
INSERT INTO action_states VALUES (31, 1, 234, 'unverified', 'verifymail', '2012-11-07 18:41:24.638537', '10.0.1.24', NULL);
INSERT INTO action_states VALUES (31, 1, 235, 'pending_review', 'verify', '2012-11-07 18:41:24.642911', '10.0.1.24', NULL);
INSERT INTO action_states VALUES (32, 1, 236, 'reg', 'initial', '2012-11-07 18:42:05.546683', '10.0.1.24', NULL);
INSERT INTO action_states VALUES (32, 1, 237, 'unverified', 'verifymail', '2012-11-07 18:42:05.546683', '10.0.1.24', NULL);
INSERT INTO action_states VALUES (32, 1, 238, 'pending_review', 'verify', '2012-11-07 18:42:05.552033', '10.0.1.24', NULL);
INSERT INTO action_states VALUES (33, 1, 239, 'reg', 'initial', '2012-11-07 18:42:20.391131', '10.0.1.53', NULL);
INSERT INTO action_states VALUES (33, 1, 240, 'unverified', 'verifymail', '2012-11-07 18:42:20.391131', '10.0.1.53', NULL);
INSERT INTO action_states VALUES (33, 1, 241, 'pending_review', 'verify', '2012-11-07 18:42:20.398055', '10.0.1.53', NULL);
INSERT INTO action_states VALUES (34, 1, 242, 'reg', 'initial', '2012-11-07 18:43:22.494636', '10.0.1.53', NULL);
INSERT INTO action_states VALUES (34, 1, 243, 'unverified', 'verifymail', '2012-11-07 18:43:22.494636', '10.0.1.53', NULL);
INSERT INTO action_states VALUES (34, 1, 244, 'pending_review', 'verify', '2012-11-07 18:43:22.500976', '10.0.1.53', NULL);
INSERT INTO action_states VALUES (35, 1, 245, 'reg', 'initial', '2012-11-07 18:43:48.005484', '10.0.1.24', NULL);
INSERT INTO action_states VALUES (35, 1, 246, 'unverified', 'verifymail', '2012-11-07 18:43:48.005484', '10.0.1.24', NULL);
INSERT INTO action_states VALUES (35, 1, 247, 'pending_review', 'verify', '2012-11-07 18:43:48.011031', '10.0.1.24', NULL);
INSERT INTO action_states VALUES (20, 1, 248, 'locked', 'checkout', '2012-11-07 18:44:23.985146', '10.0.1.53', 3);
INSERT INTO action_states VALUES (21, 1, 249, 'locked', 'checkout', '2012-11-07 18:44:23.985146', '10.0.1.53', 3);
INSERT INTO action_states VALUES (20, 1, 250, 'accepted', 'accept', '2012-11-07 18:44:32.076978', '10.0.1.53', 10);
INSERT INTO action_states VALUES (21, 1, 251, 'accepted', 'accept', '2012-11-07 18:44:34.906444', '10.0.1.53', 11);
INSERT INTO action_states VALUES (22, 1, 252, 'locked', 'checkout', '2012-11-07 18:44:36.689715', '10.0.1.53', 12);
INSERT INTO action_states VALUES (23, 1, 253, 'locked', 'checkout', '2012-11-07 18:44:36.689715', '10.0.1.53', 12);
INSERT INTO action_states VALUES (22, 1, 254, 'accepted', 'accept', '2012-11-07 18:44:41.984225', '10.0.1.53', 19);
INSERT INTO action_states VALUES (23, 1, 255, 'accepted', 'accept', '2012-11-07 18:44:45.671224', '10.0.1.53', 20);
INSERT INTO action_states VALUES (24, 1, 256, 'locked', 'checkout', '2012-11-07 18:44:47.021139', '10.0.1.53', 21);
INSERT INTO action_states VALUES (25, 1, 257, 'locked', 'checkout', '2012-11-07 18:44:47.021139', '10.0.1.53', 21);
INSERT INTO action_states VALUES (24, 1, 258, 'accepted', 'accept', '2012-11-07 18:44:52.931095', '10.0.1.53', 28);
INSERT INTO action_states VALUES (25, 1, 259, 'accepted', 'accept', '2012-11-07 18:44:57.022859', '10.0.1.53', 29);
INSERT INTO action_states VALUES (26, 1, 260, 'locked', 'checkout', '2012-11-07 18:44:59.201019', '10.0.1.53', 30);
INSERT INTO action_states VALUES (27, 1, 261, 'locked', 'checkout', '2012-11-07 18:44:59.201019', '10.0.1.53', 30);
INSERT INTO action_states VALUES (26, 1, 262, 'accepted', 'accept', '2012-11-07 18:45:02.182113', '10.0.1.53', 37);
INSERT INTO action_states VALUES (27, 1, 263, 'accepted', 'accept', '2012-11-07 18:45:05.171239', '10.0.1.53', 38);
INSERT INTO action_states VALUES (28, 1, 264, 'locked', 'checkout', '2012-11-07 18:45:06.81803', '10.0.1.53', 39);
INSERT INTO action_states VALUES (29, 1, 265, 'locked', 'checkout', '2012-11-07 18:45:06.81803', '10.0.1.53', 39);
INSERT INTO action_states VALUES (28, 1, 266, 'accepted', 'accept', '2012-11-07 18:45:11.856153', '10.0.1.53', 46);
INSERT INTO action_states VALUES (29, 1, 267, 'accepted', 'accept', '2012-11-07 18:45:18.562067', '10.0.1.53', 47);
INSERT INTO action_states VALUES (30, 1, 268, 'locked', 'checkout', '2012-11-07 18:45:20.14425', '10.0.1.53', 48);
INSERT INTO action_states VALUES (31, 1, 269, 'locked', 'checkout', '2012-11-07 18:45:20.14425', '10.0.1.53', 48);
INSERT INTO action_states VALUES (30, 1, 270, 'accepted', 'accept', '2012-11-07 18:45:25.116642', '10.0.1.53', 55);
INSERT INTO action_states VALUES (31, 1, 271, 'accepted', 'accept', '2012-11-07 18:45:29.301937', '10.0.1.53', 56);
INSERT INTO action_states VALUES (32, 1, 272, 'locked', 'checkout', '2012-11-07 18:45:31.016009', '10.0.1.53', 57);
INSERT INTO action_states VALUES (33, 1, 273, 'locked', 'checkout', '2012-11-07 18:45:31.016009', '10.0.1.53', 57);
INSERT INTO action_states VALUES (32, 1, 274, 'accepted', 'accept', '2012-11-07 18:45:37.462247', '10.0.1.53', 64);
INSERT INTO action_states VALUES (33, 1, 275, 'accepted', 'accept', '2012-11-07 18:45:44.356053', '10.0.1.53', 65);
INSERT INTO action_states VALUES (34, 1, 276, 'locked', 'checkout', '2012-11-07 18:45:45.905405', '10.0.1.53', 66);
INSERT INTO action_states VALUES (35, 1, 277, 'locked', 'checkout', '2012-11-07 18:45:45.905405', '10.0.1.53', 66);
INSERT INTO action_states VALUES (34, 1, 278, 'accepted', 'accept', '2012-11-07 18:45:49.769035', '10.0.1.53', 73);
INSERT INTO action_states VALUES (35, 1, 279, 'accepted', 'accept', '2012-11-07 18:45:55.99175', '10.0.1.53', 74);


--
-- Data for Name: ad_changes; Type: TABLE DATA; Schema: public; Owner: dany
--

INSERT INTO ad_changes VALUES (20, 1, 250, false, 'lang', 'es', NULL);
INSERT INTO ad_changes VALUES (21, 1, 251, false, 'lang', 'es', NULL);
INSERT INTO ad_changes VALUES (22, 1, 254, false, 'lang', 'es', NULL);
INSERT INTO ad_changes VALUES (23, 1, 255, false, 'lang', 'es', NULL);
INSERT INTO ad_changes VALUES (24, 1, 258, false, 'lang', 'es', NULL);
INSERT INTO ad_changes VALUES (25, 1, 259, false, 'lang', 'es', NULL);
INSERT INTO ad_changes VALUES (26, 1, 262, false, 'lang', 'es', NULL);
INSERT INTO ad_changes VALUES (27, 1, 263, false, 'lang', 'es', NULL);
INSERT INTO ad_changes VALUES (28, 1, 266, false, 'lang', 'es', NULL);
INSERT INTO ad_changes VALUES (29, 1, 267, false, 'lang', 'es', NULL);
INSERT INTO ad_changes VALUES (30, 1, 270, false, 'lang', 'es', NULL);
INSERT INTO ad_changes VALUES (31, 1, 271, false, 'lang', 'es', NULL);
INSERT INTO ad_changes VALUES (32, 1, 274, false, 'lang', 'es', NULL);
INSERT INTO ad_changes VALUES (33, 1, 275, false, 'lang', 'es', NULL);
INSERT INTO ad_changes VALUES (34, 1, 278, false, 'lang', 'es', NULL);
INSERT INTO ad_changes VALUES (35, 1, 279, false, 'lang', 'es', NULL);


--
-- Data for Name: ad_codes; Type: TABLE DATA; Schema: public; Owner: dany
--

INSERT INTO ad_codes VALUES (12345, 'pay');
INSERT INTO ad_codes VALUES (65432, 'pay');
INSERT INTO ad_codes VALUES (32323, 'pay');
INSERT INTO ad_codes VALUES (54545, 'pay');
INSERT INTO ad_codes VALUES (112200000, 'verify');
INSERT INTO ad_codes VALUES (112200001, 'verify');
INSERT INTO ad_codes VALUES (112200002, 'verify');
INSERT INTO ad_codes VALUES (112200003, 'verify');
INSERT INTO ad_codes VALUES (112200004, 'verify');
INSERT INTO ad_codes VALUES (112200005, 'verify');
INSERT INTO ad_codes VALUES (112200006, 'verify');
INSERT INTO ad_codes VALUES (112200007, 'verify');
INSERT INTO ad_codes VALUES (112200008, 'verify');
INSERT INTO ad_codes VALUES (112200009, 'verify');
INSERT INTO ad_codes VALUES (90001, 'pay');
INSERT INTO ad_codes VALUES (90002, 'pay');
INSERT INTO ad_codes VALUES (90003, 'pay');
INSERT INTO ad_codes VALUES (90004, 'pay');
INSERT INTO ad_codes VALUES (90005, 'pay');
INSERT INTO ad_codes VALUES (90006, 'pay');
INSERT INTO ad_codes VALUES (90007, 'pay');
INSERT INTO ad_codes VALUES (90008, 'pay');
INSERT INTO ad_codes VALUES (90009, 'pay');
INSERT INTO ad_codes VALUES (90010, 'pay');
INSERT INTO ad_codes VALUES (90011, 'pay');
INSERT INTO ad_codes VALUES (90012, 'pay');
INSERT INTO ad_codes VALUES (90013, 'pay');
INSERT INTO ad_codes VALUES (90014, 'pay');
INSERT INTO ad_codes VALUES (90015, 'pay');
INSERT INTO ad_codes VALUES (90016, 'pay');
INSERT INTO ad_codes VALUES (90017, 'pay');
INSERT INTO ad_codes VALUES (90018, 'pay');
INSERT INTO ad_codes VALUES (90019, 'pay');
INSERT INTO ad_codes VALUES (90020, 'pay');
INSERT INTO ad_codes VALUES (90021, 'pay');
INSERT INTO ad_codes VALUES (90022, 'pay');
INSERT INTO ad_codes VALUES (90023, 'pay');
INSERT INTO ad_codes VALUES (90024, 'pay');
INSERT INTO ad_codes VALUES (90025, 'pay');
INSERT INTO ad_codes VALUES (90026, 'pay');
INSERT INTO ad_codes VALUES (90027, 'pay');
INSERT INTO ad_codes VALUES (90028, 'pay');
INSERT INTO ad_codes VALUES (90029, 'pay');
INSERT INTO ad_codes VALUES (90030, 'pay');
INSERT INTO ad_codes VALUES (90031, 'pay');
INSERT INTO ad_codes VALUES (90032, 'pay');
INSERT INTO ad_codes VALUES (90033, 'pay');
INSERT INTO ad_codes VALUES (90034, 'pay');
INSERT INTO ad_codes VALUES (90035, 'pay');
INSERT INTO ad_codes VALUES (90036, 'pay');
INSERT INTO ad_codes VALUES (90037, 'pay');
INSERT INTO ad_codes VALUES (90038, 'pay');
INSERT INTO ad_codes VALUES (90039, 'pay');
INSERT INTO ad_codes VALUES (90040, 'pay');
INSERT INTO ad_codes VALUES (42023, 'pay');
INSERT INTO ad_codes VALUES (74046, 'pay');
INSERT INTO ad_codes VALUES (16069, 'pay');
INSERT INTO ad_codes VALUES (48092, 'pay');
INSERT INTO ad_codes VALUES (80115, 'pay');
INSERT INTO ad_codes VALUES (22138, 'pay');
INSERT INTO ad_codes VALUES (54161, 'pay');
INSERT INTO ad_codes VALUES (86184, 'pay');
INSERT INTO ad_codes VALUES (28207, 'pay');
INSERT INTO ad_codes VALUES (60230, 'pay');
INSERT INTO ad_codes VALUES (92253, 'pay');
INSERT INTO ad_codes VALUES (34276, 'pay');
INSERT INTO ad_codes VALUES (66299, 'pay');
INSERT INTO ad_codes VALUES (98322, 'pay');
INSERT INTO ad_codes VALUES (40345, 'pay');
INSERT INTO ad_codes VALUES (72368, 'pay');
INSERT INTO ad_codes VALUES (14391, 'pay');
INSERT INTO ad_codes VALUES (46414, 'pay');
INSERT INTO ad_codes VALUES (78437, 'pay');
INSERT INTO ad_codes VALUES (20460, 'pay');
INSERT INTO ad_codes VALUES (52483, 'pay');
INSERT INTO ad_codes VALUES (84506, 'pay');
INSERT INTO ad_codes VALUES (26529, 'pay');
INSERT INTO ad_codes VALUES (58552, 'pay');
INSERT INTO ad_codes VALUES (90575, 'pay');
INSERT INTO ad_codes VALUES (32598, 'pay');
INSERT INTO ad_codes VALUES (64621, 'pay');
INSERT INTO ad_codes VALUES (96644, 'pay');
INSERT INTO ad_codes VALUES (38667, 'pay');
INSERT INTO ad_codes VALUES (70690, 'pay');
INSERT INTO ad_codes VALUES (12713, 'pay');
INSERT INTO ad_codes VALUES (44736, 'pay');
INSERT INTO ad_codes VALUES (76759, 'pay');
INSERT INTO ad_codes VALUES (18782, 'pay');
INSERT INTO ad_codes VALUES (50805, 'pay');
INSERT INTO ad_codes VALUES (82828, 'pay');
INSERT INTO ad_codes VALUES (24851, 'pay');
INSERT INTO ad_codes VALUES (56874, 'pay');
INSERT INTO ad_codes VALUES (88897, 'pay');
INSERT INTO ad_codes VALUES (30920, 'pay');
INSERT INTO ad_codes VALUES (62943, 'pay');
INSERT INTO ad_codes VALUES (94966, 'pay');
INSERT INTO ad_codes VALUES (36989, 'pay');
INSERT INTO ad_codes VALUES (69012, 'pay');
INSERT INTO ad_codes VALUES (11035, 'pay');
INSERT INTO ad_codes VALUES (43058, 'pay');
INSERT INTO ad_codes VALUES (75081, 'pay');
INSERT INTO ad_codes VALUES (17104, 'pay');
INSERT INTO ad_codes VALUES (49127, 'pay');
INSERT INTO ad_codes VALUES (81150, 'pay');
INSERT INTO ad_codes VALUES (23173, 'pay');
INSERT INTO ad_codes VALUES (55196, 'pay');
INSERT INTO ad_codes VALUES (87219, 'pay');
INSERT INTO ad_codes VALUES (29242, 'pay');
INSERT INTO ad_codes VALUES (61265, 'pay');
INSERT INTO ad_codes VALUES (93288, 'pay');
INSERT INTO ad_codes VALUES (235392023, 'verify');
INSERT INTO ad_codes VALUES (370784046, 'verify');
INSERT INTO ad_codes VALUES (506176069, 'verify');
INSERT INTO ad_codes VALUES (641568092, 'verify');
INSERT INTO ad_codes VALUES (776960115, 'verify');
INSERT INTO ad_codes VALUES (912352138, 'verify');
INSERT INTO ad_codes VALUES (147744161, 'verify');
INSERT INTO ad_codes VALUES (283136184, 'verify');
INSERT INTO ad_codes VALUES (418528207, 'verify');
INSERT INTO ad_codes VALUES (553920230, 'verify');
INSERT INTO ad_codes VALUES (689312253, 'verify');
INSERT INTO ad_codes VALUES (824704276, 'verify');
INSERT INTO ad_codes VALUES (960096299, 'verify');
INSERT INTO ad_codes VALUES (195488322, 'verify');
INSERT INTO ad_codes VALUES (330880345, 'verify');
INSERT INTO ad_codes VALUES (466272368, 'verify');
INSERT INTO ad_codes VALUES (601664391, 'verify');
INSERT INTO ad_codes VALUES (737056414, 'verify');
INSERT INTO ad_codes VALUES (872448437, 'verify');
INSERT INTO ad_codes VALUES (107840460, 'verify');
INSERT INTO ad_codes VALUES (243232483, 'verify');
INSERT INTO ad_codes VALUES (378624506, 'verify');
INSERT INTO ad_codes VALUES (514016529, 'verify');
INSERT INTO ad_codes VALUES (649408552, 'verify');
INSERT INTO ad_codes VALUES (784800575, 'verify');
INSERT INTO ad_codes VALUES (920192598, 'verify');
INSERT INTO ad_codes VALUES (155584621, 'verify');
INSERT INTO ad_codes VALUES (290976644, 'verify');
INSERT INTO ad_codes VALUES (426368667, 'verify');
INSERT INTO ad_codes VALUES (561760690, 'verify');
INSERT INTO ad_codes VALUES (697152713, 'verify');
INSERT INTO ad_codes VALUES (832544736, 'verify');
INSERT INTO ad_codes VALUES (967936759, 'verify');
INSERT INTO ad_codes VALUES (203328782, 'verify');
INSERT INTO ad_codes VALUES (338720805, 'verify');
INSERT INTO ad_codes VALUES (474112828, 'verify');
INSERT INTO ad_codes VALUES (609504851, 'verify');
INSERT INTO ad_codes VALUES (744896874, 'verify');
INSERT INTO ad_codes VALUES (880288897, 'verify');
INSERT INTO ad_codes VALUES (115680920, 'verify');
INSERT INTO ad_codes VALUES (251072943, 'verify');
INSERT INTO ad_codes VALUES (386464966, 'verify');
INSERT INTO ad_codes VALUES (521856989, 'verify');
INSERT INTO ad_codes VALUES (657249012, 'verify');
INSERT INTO ad_codes VALUES (792641035, 'verify');
INSERT INTO ad_codes VALUES (928033058, 'verify');
INSERT INTO ad_codes VALUES (163425081, 'verify');
INSERT INTO ad_codes VALUES (298817104, 'verify');
INSERT INTO ad_codes VALUES (434209127, 'verify');
INSERT INTO ad_codes VALUES (569601150, 'verify');
INSERT INTO ad_codes VALUES (704993173, 'verify');
INSERT INTO ad_codes VALUES (840385196, 'verify');
INSERT INTO ad_codes VALUES (975777219, 'verify');
INSERT INTO ad_codes VALUES (211169242, 'verify');
INSERT INTO ad_codes VALUES (346561265, 'verify');
INSERT INTO ad_codes VALUES (481953288, 'verify');
INSERT INTO ad_codes VALUES (617345311, 'verify');
INSERT INTO ad_codes VALUES (752737334, 'verify');
INSERT INTO ad_codes VALUES (888129357, 'verify');
INSERT INTO ad_codes VALUES (123521380, 'verify');
INSERT INTO ad_codes VALUES (258913403, 'verify');
INSERT INTO ad_codes VALUES (394305426, 'verify');
INSERT INTO ad_codes VALUES (529697449, 'verify');
INSERT INTO ad_codes VALUES (665089472, 'verify');
INSERT INTO ad_codes VALUES (800481495, 'verify');
INSERT INTO ad_codes VALUES (935873518, 'verify');
INSERT INTO ad_codes VALUES (171265541, 'verify');
INSERT INTO ad_codes VALUES (306657564, 'verify');
INSERT INTO ad_codes VALUES (442049587, 'verify');
INSERT INTO ad_codes VALUES (577441610, 'verify');
INSERT INTO ad_codes VALUES (712833633, 'verify');
INSERT INTO ad_codes VALUES (848225656, 'verify');
INSERT INTO ad_codes VALUES (983617679, 'verify');
INSERT INTO ad_codes VALUES (219009702, 'verify');
INSERT INTO ad_codes VALUES (354401725, 'verify');
INSERT INTO ad_codes VALUES (489793748, 'verify');
INSERT INTO ad_codes VALUES (625185771, 'verify');
INSERT INTO ad_codes VALUES (760577794, 'verify');
INSERT INTO ad_codes VALUES (895969817, 'verify');
INSERT INTO ad_codes VALUES (131361840, 'verify');
INSERT INTO ad_codes VALUES (266753863, 'verify');
INSERT INTO ad_codes VALUES (402145886, 'verify');
INSERT INTO ad_codes VALUES (537537909, 'verify');
INSERT INTO ad_codes VALUES (672929932, 'verify');
INSERT INTO ad_codes VALUES (808321955, 'verify');
INSERT INTO ad_codes VALUES (943713978, 'verify');
INSERT INTO ad_codes VALUES (179106001, 'verify');
INSERT INTO ad_codes VALUES (314498024, 'verify');
INSERT INTO ad_codes VALUES (449890047, 'verify');
INSERT INTO ad_codes VALUES (585282070, 'verify');


--
-- Data for Name: ad_image_changes; Type: TABLE DATA; Schema: public; Owner: dany
--

INSERT INTO ad_image_changes VALUES (20, 1, 250, 0, '5152331900.jpg', false);
INSERT INTO ad_image_changes VALUES (21, 1, 251, 0, NULL, false);
INSERT INTO ad_image_changes VALUES (22, 1, 254, 0, NULL, false);
INSERT INTO ad_image_changes VALUES (23, 1, 255, 0, NULL, false);
INSERT INTO ad_image_changes VALUES (24, 1, 258, 0, '5160855219.jpg', false);
INSERT INTO ad_image_changes VALUES (25, 1, 259, 0, '5169378538.jpg', false);
INSERT INTO ad_image_changes VALUES (26, 1, 262, 0, NULL, false);
INSERT INTO ad_image_changes VALUES (27, 1, 263, 0, NULL, false);
INSERT INTO ad_image_changes VALUES (28, 1, 266, 0, NULL, false);
INSERT INTO ad_image_changes VALUES (29, 1, 267, 0, '5177901857.jpg', false);
INSERT INTO ad_image_changes VALUES (30, 1, 270, 0, NULL, false);
INSERT INTO ad_image_changes VALUES (31, 1, 271, 0, NULL, false);
INSERT INTO ad_image_changes VALUES (32, 1, 274, 0, NULL, false);
INSERT INTO ad_image_changes VALUES (33, 1, 275, 0, NULL, false);
INSERT INTO ad_image_changes VALUES (34, 1, 278, 0, NULL, false);
INSERT INTO ad_image_changes VALUES (35, 1, 279, 0, NULL, false);


--
-- Data for Name: ad_images; Type: TABLE DATA; Schema: public; Owner: dany
--



--
-- Data for Name: ad_images_digests; Type: TABLE DATA; Schema: public; Owner: dany
--

INSERT INTO ad_images_digests VALUES (20, '5152331900.jpg', '0fbd6900d9f6a5a63cb4e159219fc506', 50, false);
INSERT INTO ad_images_digests VALUES (24, '5160855219.jpg', '21c1602c8cd47649d3b7e964e1a5b763', 50, false);
INSERT INTO ad_images_digests VALUES (25, '5169378538.jpg', '2bb0050597bee09b33ceb1f92b82c716', 50, false);
INSERT INTO ad_images_digests VALUES (29, '5177901857.jpg', 'c14c9541698d5b24afe9f9079e5a246a', 50, false);


--
-- Data for Name: ad_media; Type: TABLE DATA; Schema: public; Owner: dany
--

INSERT INTO ad_media VALUES (100012344, 6, 0, '2012-11-08 11:08:43.771512', 'image', NULL, NULL, NULL);
INSERT INTO ad_media VALUES (100012345, 6, 1, '2012-11-08 11:08:43.772269', 'image', NULL, NULL, NULL);
INSERT INTO ad_media VALUES (100012346, 6, 2, '2012-11-08 11:08:43.772474', 'image', NULL, NULL, NULL);
INSERT INTO ad_media VALUES (5152331900, 20, 0, '2012-11-07 18:32:04.212638', 'image', NULL, NULL, NULL);
INSERT INTO ad_media VALUES (5160855219, 24, 0, '2012-11-07 18:35:28.283567', 'image', NULL, NULL, NULL);
INSERT INTO ad_media VALUES (5169378538, 25, 0, '2012-11-07 18:36:38.740244', 'image', NULL, NULL, NULL);
INSERT INTO ad_media VALUES (5177901857, 29, 0, '2012-11-07 18:40:17.773217', 'image', NULL, NULL, NULL);


--
-- Data for Name: ad_media_changes; Type: TABLE DATA; Schema: public; Owner: dany
--



--
-- Data for Name: ad_params; Type: TABLE DATA; Schema: public; Owner: dany
--

INSERT INTO ad_params VALUES (1, 'rooms', '2');
INSERT INTO ad_params VALUES (1, 'size', '250');
INSERT INTO ad_params VALUES (1, 'garage_spaces', '2');
INSERT INTO ad_params VALUES (1, 'condominio', '400');
INSERT INTO ad_params VALUES (1, 'communes', '5');
INSERT INTO ad_params VALUES (1, 'currency', 'peso');
INSERT INTO ad_params VALUES (1, 'country', 'UNK');
INSERT INTO ad_params VALUES (6, 'regdate', '1986');
INSERT INTO ad_params VALUES (6, 'mileage', '1');
INSERT INTO ad_params VALUES (6, 'fuel', '2');
INSERT INTO ad_params VALUES (6, 'gearbox', '2');
INSERT INTO ad_params VALUES (11, 'service_type', '6');
INSERT INTO ad_params VALUES (12, 'rooms', '3');
INSERT INTO ad_params VALUES (12, 'size', '120');
INSERT INTO ad_params VALUES (12, 'garage_spaces', '1');
INSERT INTO ad_params VALUES (12, 'condominio', '400');
INSERT INTO ad_params VALUES (12, 'communes', '323');
INSERT INTO ad_params VALUES (12, 'currency', 'peso');
INSERT INTO ad_params VALUES (12, 'country', 'UNK');
INSERT INTO ad_params VALUES (20, 'prev_currency', 'peso');
INSERT INTO ad_params VALUES (20, 'country', 'UNK');
INSERT INTO ad_params VALUES (21, 'rooms', '2');
INSERT INTO ad_params VALUES (21, 'size', '100');
INSERT INTO ad_params VALUES (21, 'garage_spaces', '2');
INSERT INTO ad_params VALUES (21, 'condominio', '50000');
INSERT INTO ad_params VALUES (21, 'communes', '243');
INSERT INTO ad_params VALUES (21, 'prev_currency', 'peso');
INSERT INTO ad_params VALUES (21, 'country', 'UNK');
INSERT INTO ad_params VALUES (22, 'service_type', '2|7|8');
INSERT INTO ad_params VALUES (22, 'prev_currency', 'peso');
INSERT INTO ad_params VALUES (22, 'country', 'UNK');
INSERT INTO ad_params VALUES (23, 'rooms', '4');
INSERT INTO ad_params VALUES (23, 'size', '120');
INSERT INTO ad_params VALUES (23, 'garage_spaces', '4');
INSERT INTO ad_params VALUES (23, 'condominio', '10000');
INSERT INTO ad_params VALUES (23, 'communes', '242');
INSERT INTO ad_params VALUES (23, 'prev_currency', 'peso');
INSERT INTO ad_params VALUES (23, 'country', 'UNK');
INSERT INTO ad_params VALUES (24, 'job_category', '20|24');
INSERT INTO ad_params VALUES (24, 'country', 'UNK');
INSERT INTO ad_params VALUES (25, 'size', '50');
INSERT INTO ad_params VALUES (25, 'garage_spaces', '1');
INSERT INTO ad_params VALUES (25, 'communes', '243');
INSERT INTO ad_params VALUES (25, 'prev_currency', 'peso');
INSERT INTO ad_params VALUES (25, 'country', 'UNK');
INSERT INTO ad_params VALUES (25, 'estate_type', '1');
INSERT INTO ad_params VALUES (25, 'services', '1');
INSERT INTO ad_params VALUES (26, 'job_category', '19|20|24');
INSERT INTO ad_params VALUES (26, 'country', 'UNK');
INSERT INTO ad_params VALUES (27, 'size', '100');
INSERT INTO ad_params VALUES (27, 'garage_spaces', '1');
INSERT INTO ad_params VALUES (27, 'communes', '239');
INSERT INTO ad_params VALUES (27, 'prev_currency', 'peso');
INSERT INTO ad_params VALUES (27, 'country', 'UNK');
INSERT INTO ad_params VALUES (28, 'size', '250');
INSERT INTO ad_params VALUES (28, 'condominio', '10');
INSERT INTO ad_params VALUES (28, 'communes', '233');
INSERT INTO ad_params VALUES (28, 'prev_currency', 'peso');
INSERT INTO ad_params VALUES (28, 'country', 'UNK');
INSERT INTO ad_params VALUES (29, 'condition', '1');
INSERT INTO ad_params VALUES (29, 'prev_currency', 'peso');
INSERT INTO ad_params VALUES (29, 'country', 'UNK');
INSERT INTO ad_params VALUES (30, 'size', '12');
INSERT INTO ad_params VALUES (30, 'garage_spaces', '1');
INSERT INTO ad_params VALUES (30, 'condominio', '1000');
INSERT INTO ad_params VALUES (30, 'communes', '243');
INSERT INTO ad_params VALUES (30, 'prev_currency', 'peso');
INSERT INTO ad_params VALUES (30, 'country', 'UNK');
INSERT INTO ad_params VALUES (31, 'condition', '1');
INSERT INTO ad_params VALUES (31, 'prev_currency', 'peso');
INSERT INTO ad_params VALUES (31, 'country', 'UNK');
INSERT INTO ad_params VALUES (32, 'gender', '2');
INSERT INTO ad_params VALUES (32, 'condition', '2');
INSERT INTO ad_params VALUES (32, 'prev_currency', 'peso');
INSERT INTO ad_params VALUES (32, 'country', 'UNK');
INSERT INTO ad_params VALUES (33, 'regdate', '2000');
INSERT INTO ad_params VALUES (33, 'mileage', '10000');
INSERT INTO ad_params VALUES (33, 'gearbox', '1');
INSERT INTO ad_params VALUES (33, 'fuel', '1');
INSERT INTO ad_params VALUES (33, 'cartype', '1');
INSERT INTO ad_params VALUES (33, 'prev_currency', 'peso');
INSERT INTO ad_params VALUES (33, 'brand', '18');
INSERT INTO ad_params VALUES (33, 'model', '19');
INSERT INTO ad_params VALUES (33, 'version', '2');
INSERT INTO ad_params VALUES (33, 'country', 'UNK');
INSERT INTO ad_params VALUES (34, 'regdate', '2005');
INSERT INTO ad_params VALUES (34, 'mileage', '10000');
INSERT INTO ad_params VALUES (34, 'cubiccms', '2');
INSERT INTO ad_params VALUES (34, 'prev_currency', 'peso');
INSERT INTO ad_params VALUES (34, 'country', 'UNK');
INSERT INTO ad_params VALUES (35, 'regdate', '2010');
INSERT INTO ad_params VALUES (35, 'mileage', '400000');
INSERT INTO ad_params VALUES (35, 'gearbox', '1');
INSERT INTO ad_params VALUES (35, 'fuel', '4');
INSERT INTO ad_params VALUES (35, 'prev_currency', 'peso');
INSERT INTO ad_params VALUES (35, 'country', 'UNK');


--
-- Data for Name: ad_queues; Type: TABLE DATA; Schema: public; Owner: dany
--



--
-- Data for Name: admin_privs; Type: TABLE DATA; Schema: public; Owner: dany
--

INSERT INTO admin_privs VALUES (2, 'adqueue');
INSERT INTO admin_privs VALUES (3, 'adqueue');
INSERT INTO admin_privs VALUES (3, 'admin');
INSERT INTO admin_privs VALUES (3, 'adminad.edit_ad');
INSERT INTO admin_privs VALUES (3, 'adminad.clear_ad');
INSERT INTO admin_privs VALUES (3, 'credit');
INSERT INTO admin_privs VALUES (3, 'config');
INSERT INTO admin_privs VALUES (3, 'adqueue.admin_queue');
INSERT INTO admin_privs VALUES (3, 'adqueue.admin_settings');
INSERT INTO admin_privs VALUES (3, 'adqueue.show_num_ads=2');
INSERT INTO admin_privs VALUES (3, 'search');
INSERT INTO admin_privs VALUES (3, 'search.search_ads');
INSERT INTO admin_privs VALUES (3, 'notice_abuse');
INSERT INTO admin_privs VALUES (4, 'admin');
INSERT INTO admin_privs VALUES (5, 'admin');
INSERT INTO admin_privs VALUES (5, 'adqueue');
INSERT INTO admin_privs VALUES (5, 'adminad');
INSERT INTO admin_privs VALUES (5, 'adminad.clear_ad');
INSERT INTO admin_privs VALUES (5, 'adminad.edit_ad');
INSERT INTO admin_privs VALUES (5, 'credit');
INSERT INTO admin_privs VALUES (5, 'config');
INSERT INTO admin_privs VALUES (5, 'adqueue.admin_queue');
INSERT INTO admin_privs VALUES (5, 'adqueue.admin_settings');
INSERT INTO admin_privs VALUES (5, 'adqueue.show_num_ads=2');
INSERT INTO admin_privs VALUES (6, 'admin');
INSERT INTO admin_privs VALUES (6, 'adqueue');
INSERT INTO admin_privs VALUES (6, 'adminad.edit_ad');
INSERT INTO admin_privs VALUES (50, 'admin');
INSERT INTO admin_privs VALUES (50, 'adqueue');
INSERT INTO admin_privs VALUES (51, 'admin');
INSERT INTO admin_privs VALUES (51, 'adqueue');
INSERT INTO admin_privs VALUES (52, 'admin');
INSERT INTO admin_privs VALUES (52, 'adqueue');
INSERT INTO admin_privs VALUES (53, 'admin');
INSERT INTO admin_privs VALUES (53, 'adqueue');
INSERT INTO admin_privs VALUES (54, 'admin');
INSERT INTO admin_privs VALUES (54, 'adqueue');
INSERT INTO admin_privs VALUES (55, 'admin');
INSERT INTO admin_privs VALUES (55, 'adqueue');
INSERT INTO admin_privs VALUES (9, 'search.paylog');
INSERT INTO admin_privs VALUES (9, 'search.reviewers');
INSERT INTO admin_privs VALUES (9, 'search.maillog');
INSERT INTO admin_privs VALUES (9, 'search.abuse_report');
INSERT INTO admin_privs VALUES (9, 'notice_abuse');
INSERT INTO admin_privs VALUES (9, 'adminad.clear_ad');
INSERT INTO admin_privs VALUES (9, 'adminad.edit_ad');
INSERT INTO admin_privs VALUES (9, 'adminad');
INSERT INTO admin_privs VALUES (9, 'admin');
INSERT INTO admin_privs VALUES (9, 'adqueue');
INSERT INTO admin_privs VALUES (9, 'stores');
INSERT INTO admin_privs VALUES (9, 'clearad');
INSERT INTO admin_privs VALUES (9, 'filter');
INSERT INTO admin_privs VALUES (9, 'popular_ads');
INSERT INTO admin_privs VALUES (9, 'search');
INSERT INTO admin_privs VALUES (9, 'credit');
INSERT INTO admin_privs VALUES (9, 'adminedit');
INSERT INTO admin_privs VALUES (9, 'config');
INSERT INTO admin_privs VALUES (9, 'adqueue.admin_settings');
INSERT INTO admin_privs VALUES (9, 'adqueue.show_num_ads=2');
INSERT INTO admin_privs VALUES (9, 'adqueue.admin_queue');
INSERT INTO admin_privs VALUES (9, 'adqueue.refusals');
INSERT INTO admin_privs VALUES (9, 'adqueue.refusals_score');
INSERT INTO admin_privs VALUES (9, 'filter.lists');
INSERT INTO admin_privs VALUES (9, 'filter.rules');
INSERT INTO admin_privs VALUES (9, 'filter.spamfilter');
INSERT INTO admin_privs VALUES (9, 'search.search_ads');
INSERT INTO admin_privs VALUES (9, 'search.uid_emails');
INSERT INTO admin_privs VALUES (9, 'search.mass_delete');
INSERT INTO admin_privs VALUES (9, 'on_call');
INSERT INTO admin_privs VALUES (9, 'Websql');
INSERT INTO admin_privs VALUES (9, 'adqueue.video');
INSERT INTO admin_privs VALUES (9, 'bids');
INSERT INTO admin_privs VALUES (9, 'on_call.duty');
INSERT INTO admin_privs VALUES (9, 'scarface');
INSERT INTO admin_privs VALUES (9, 'ais');
INSERT INTO admin_privs VALUES (9, 'landing_page');
INSERT INTO admin_privs VALUES (9, 'Adminuf');
INSERT INTO admin_privs VALUES (9, 'adqueue.approve_refused');
INSERT INTO admin_privs VALUES (9, 'scarface.warning');


--
-- Data for Name: bid_ads; Type: TABLE DATA; Schema: public; Owner: dany
--



--
-- Data for Name: bid_bids; Type: TABLE DATA; Schema: public; Owner: dany
--



--
-- Data for Name: bid_media; Type: TABLE DATA; Schema: public; Owner: dany
--



--
-- Data for Name: block_lists; Type: TABLE DATA; Schema: public; Owner: dany
--

INSERT INTO block_lists VALUES (1, 'E-post adresser - raderas', true, 'email', NULL);
INSERT INTO block_lists VALUES (2, 'E-post adresser - spamfiltret', true, 'email', NULL);
INSERT INTO block_lists VALUES (3, 'IP-adresser som - raderas', true, 'ip', NULL);
INSERT INTO block_lists VALUES (4, 'IP-adresser - spamfiltret', true, 'ip', NULL);
INSERT INTO block_lists VALUES (5, 'E-postadresser som inte f�r annonsera', false, 'email', NULL);
INSERT INTO block_lists VALUES (6, 'Telefonnummer som inte f�r annonsera', false, 'general', '-+ ');
INSERT INTO block_lists VALUES (7, 'Tipsmottagare', false, 'email', NULL);
INSERT INTO block_lists VALUES (8, 'Synonymer f�r ordet Annonsera', false, 'general', NULL);
INSERT INTO block_lists VALUES (9, 'Synonymer f�r ordet Gratis', false, 'general', NULL);
INSERT INTO block_lists VALUES (10, 'Sajter som kan spamma', false, 'general', NULL);
INSERT INTO block_lists VALUES (11, 'Ord som anv�nds av sajter som spammar', false, 'general', NULL);
INSERT INTO block_lists VALUES (12, 'Fula ord', false, 'general', NULL);
INSERT INTO block_lists VALUES (13, 'F�rbjudna rubriker i L�gg in annons', false, 'general', NULL);
INSERT INTO block_lists VALUES (14, 'Engelska fraser som anv�nds i mejlsvar', false, 'general', NULL);
INSERT INTO block_lists VALUES (15, 'Byten.se', false, 'general', NULL);
INSERT INTO block_lists VALUES (16, 'whitelist', false, 'email', NULL);
INSERT INTO block_lists VALUES (17, 'were_whitelist', false, 'email', NULL);


--
-- Data for Name: block_rules; Type: TABLE DATA; Schema: public; Owner: dany
--

INSERT INTO block_rules VALUES (1, 'Sajtspam', 'delete', 'adreply', 'and', 0, 0, '2012-11-08', NULL);
INSERT INTO block_rules VALUES (2, 'Annonsera gratis', 'delete', 'adreply', 'and', 0, 0, '2012-11-08', NULL);
INSERT INTO block_rules VALUES (3, 'Fula ord', 'stop', 'newad', 'and', 0, 0, '2012-11-08', NULL);
INSERT INTO block_rules VALUES (4, 'Fula ord', 'delete', 'adreply', 'and', 0, 0, '2012-11-08', NULL);
INSERT INTO block_rules VALUES (5, 'Blockerade epostadresser  - raderas', 'delete', 'adreply', 'and', 0, 0, '2012-11-08', NULL);
INSERT INTO block_rules VALUES (6, 'Blockerade epostadresser', 'stop', 'newad', 'and', 0, 0, '2012-11-08', NULL);
INSERT INTO block_rules VALUES (7, 'Blockerade ip-adresser - raderas', 'delete', 'adreply', 'and', 0, 0, '2012-11-08', NULL);
INSERT INTO block_rules VALUES (8, 'Blockerade telefonnummer', 'stop', 'newad', 'and', 0, 0, '2012-11-08', NULL);
INSERT INTO block_rules VALUES (9, 'Blockerade tipsmottagare', 'delete', 'sendtip', 'and', 0, 0, '2012-11-08', NULL);
INSERT INTO block_rules VALUES (10, 'Byten.se', 'delete', 'adreply', 'and', 0, 0, '2012-11-08', NULL);
INSERT INTO block_rules VALUES (11, 'Byten.se', 'delete', 'sendtip', 'and', 0, 0, '2012-11-08', NULL);
INSERT INTO block_rules VALUES (12, 'Blockerade ord i rubriken', 'stop', 'newad', 'and', 0, 0, '2012-11-08', NULL);
INSERT INTO block_rules VALUES (13, 'Blockerade engelska mail', 'spamfilter', 'adreply', 'and', 0, 0, '2012-11-08', NULL);
INSERT INTO block_rules VALUES (14, 'Blockerade epostadresser - spamfilter', 'spamfilter', 'adreply', 'and', 0, 0, '2012-11-08', NULL);
INSERT INTO block_rules VALUES (15, 'Blockerade ip-adresser - spamfilter', 'spamfilter', 'adreply', 'and', 0, 0, '2012-11-08', NULL);
INSERT INTO block_rules VALUES (16, 'Fula ord', 'delete', 'sendtip', 'or', 0, 0, '2012-11-08', NULL);

INSERT INTO block_rules (rule_id, rule_name, action, application, combine_rule, target) VALUES (17, 'whitelist rule','move_to_queue','clear','and','whitelist');

--
-- Data for Name: block_rule_conditions; Type: TABLE DATA; Schema: public; Owner: dany
--

INSERT INTO block_rule_conditions VALUES (1, 1, 10, 0, '{subject,body}');
INSERT INTO block_rule_conditions VALUES (2, 1, 11, 0, '{subject,body}');
INSERT INTO block_rule_conditions VALUES (3, 2, 8, 0, '{subject,body}');
INSERT INTO block_rule_conditions VALUES (4, 2, 9, 0, '{subject,body}');
INSERT INTO block_rule_conditions VALUES (5, 3, 12, 0, '{subject,body}');
INSERT INTO block_rule_conditions VALUES (6, 4, 12, 0, '{subject,body}');
INSERT INTO block_rule_conditions VALUES (7, 5, 1, 1, '{email}');
INSERT INTO block_rule_conditions VALUES (8, 6, 5, 1, '{email}');
INSERT INTO block_rule_conditions VALUES (9, 7, 3, 1, '{remote_addr}');
INSERT INTO block_rule_conditions VALUES (10, 8, 6, 1, '{phone}');
INSERT INTO block_rule_conditions VALUES (11, 9, 7, 1, '{email}');
INSERT INTO block_rule_conditions VALUES (12, 10, 15, 0, '{name,email,subject,body}');
INSERT INTO block_rule_conditions VALUES (13, 11, 15, 0, '{name,email,subject,body}');
INSERT INTO block_rule_conditions VALUES (14, 12, 13, 0, '{subject}');
INSERT INTO block_rule_conditions VALUES (15, 13, 14, 0, '{body}');
INSERT INTO block_rule_conditions VALUES (16, 14, 2, 1, '{subject}');
INSERT INTO block_rule_conditions VALUES (17, 15, 4, 1, '{remote_addr}');
INSERT INTO block_rule_conditions VALUES (18, 16, 12, 0, '{body}');

INSERT INTO block_rule_conditions VALUES (19, 17,16,1,'{email}');

--
-- Data for Name: blocked_items; Type: TABLE DATA; Schema: public; Owner: dany
--

INSERT INTO blocked_items VALUES (1, 'ful@fisk.se', 1, NULL, NULL);
INSERT INTO blocked_items VALUES (2, 'ful@fisk.se', 5, NULL, NULL);
INSERT INTO blocked_items VALUES (3, '0733555501', 6, NULL, NULL);
INSERT INTO blocked_items VALUES (4, 'ful@fisk.se', 7, NULL, NULL);
INSERT INTO blocked_items VALUES (5, 'fisk.se', 10, NULL, NULL);
INSERT INTO blocked_items VALUES (6, 'vatten', 11, NULL, NULL);
INSERT INTO blocked_items VALUES (7, 'fitta', 12, NULL, NULL);
INSERT INTO blocked_items VALUES (8, 'analakrobat', 12, NULL, NULL);
INSERT INTO blocked_items VALUES (9, 'buy', 13, NULL, NULL);
INSERT INTO blocked_items VALUES (10, 'sale', 13, NULL, NULL);
INSERT INTO blocked_items VALUES (11, 'salu', 13, NULL, NULL);
INSERT INTO blocked_items VALUES (12, 'free', 14, NULL, NULL);


--
-- Data for Name: conf; Type: TABLE DATA; Schema: public; Owner: dany
--

INSERT INTO conf VALUES ('*.controlpanel.modules.adqueue.settings.auto_abuse', '1', '2012-11-08 11:08:43.166432', NULL);
INSERT INTO conf VALUES ('*.*.common.accepted.1.name', 'Sin fotos', '2012-11-08 11:08:43.168303', NULL);
INSERT INTO conf VALUES ('*.*.common.accepted.1.text', 'Para mejorar las posibilidades de que tu aviso sea visto, te recomendamos que agregues im�genes del producto que ofreces o buscas.', '2012-11-08 11:08:43.168464', NULL);
INSERT INTO conf VALUES ('*.*.common.accepted.2.name', 'Foto principal movida', '2012-11-08 11:08:43.168599', NULL);
INSERT INTO conf VALUES ('*.*.common.accepted.2.text', 'Para mejorar las posibilidades de que tu aviso sea visto, hemos alterado el orden de las fotograf�as', '2012-11-08 11:08:43.168705', NULL);
INSERT INTO conf VALUES ('*.*.common.accepted_order.1', '1', '2012-11-08 11:08:43.168826', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal.0.name', 'T�tulo y Descripci�n de Otros', '2012-11-08 11:08:43.169044', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal.1.name', 'T�tulo y Descripci�n del Veh�culo', '2012-11-08 11:08:43.169172', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal.2.name', 'T�tulo y Descripci�n de los Bienes Ra�ces', '2012-11-08 11:08:43.169276', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal.3.name', 'Restricciones para los Animales Dom�sticos', '2012-11-08 11:08:43.169375', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal.4.name', 'Links para otros Sitios', '2012-11-08 11:08:43.169475', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal.5.name', 'Aviso Empresa', '2012-11-08 11:08:43.169592', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal.6.name', 'Contacto', '2012-11-08 11:08:43.169712', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal.7.name', 'Varios elementos', '2012-11-08 11:08:43.16984', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal.8.name', 'Varios elementos - Empleo', '2012-11-08 11:08:43.169962', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal.9.name', 'Avisos personales', '2012-11-08 11:08:43.170061', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal.10.name', 'Varios elementos Veh�culos - Propiedad', '2012-11-08 11:08:43.170203', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal.11.name', 'Aviso doble', '2012-11-08 11:08:43.170313', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal.12.name', 'Aviso caducado', '2012-11-08 11:08:43.170431', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal.13.name', 'IP extranjero', '2012-11-08 11:08:43.170538', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal.14.name', 'Ilegal', '2012-11-08 11:08:43.170636', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal.15.name', 'Contenido de im�genes', '2012-11-08 11:08:43.170735', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal.16.name', 'Error en la imagen', '2012-11-08 11:08:43.170853', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal.17.name', 'Idioma', '2012-11-08 11:08:43.170971', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal.18.name', 'Link', '2012-11-08 11:08:43.171091', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal.19.name', 'Imagen obscena', '2012-11-08 11:08:43.171221', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal.20.name', 'Ofensivo', '2012-11-08 11:08:43.17135', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal.21.name', 'Pirater�a', '2012-11-08 11:08:43.171469', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal.22.name', 'Elementos', '2012-11-08 11:08:43.171586', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal.23.name', 'Marketing', '2012-11-08 11:08:43.171686', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal.24.name', 'Palabras de B�squeda', '2012-11-08 11:08:43.171804', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal.25.name', 'Contrase�a', '2012-11-08 11:08:43.171931', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal.26.name', 'Virus', '2012-11-08 11:08:43.172046', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal.27.name', 'Estado de origen', '2012-11-08 11:08:43.172163', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal.29.name', 'No es realista', '2012-11-08 11:08:43.172279', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal.30.name', 'Categor�a equivocada', '2012-11-08 11:08:43.172378', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal.31.name', 'Spam', '2012-11-08 11:08:43.172477', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal.32.name', 'Propiedad intelectual', '2012-11-08 11:08:43.172583', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal.33.name', 'Privacidad', '2012-11-08 11:08:43.172682', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal.34.name', 'Intercambios', '2012-11-08 11:08:43.172799', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal.35.name', 'Menores de edad', '2012-11-08 11:08:43.172918', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal.0.text', 'Por favor, comprueba que el nombre incluido / modelo / t�tulo / marca del art�culo que deseas vender, est� de acuerdo con  el t�tulo y la descripci�n del producto. El t�tulo del aviso debe describir el producto o servicio anunciado, no se permiten incluir nombres de empresas o URL (direcci�n Web). No es permitido el uso de caracteres especiales o caracteres en may�sculas en el t�tulo.', '2012-11-08 11:08:43.173019', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal.1.text', 'Por favor,  verifica el nombre y modelo (ejemplo: Honda Civic 1.6]) del veh�culo que deseas vender. En la descripci�n, incluye  informaci�n espec�fica.', '2012-11-08 11:08:43.173141', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal.2.text', 'Comprueba  que incluya el t�tulo. El t�tulo del aviso debe describir el producto o servicio anunciado, no se le permite incluir nombres de empresas o  URL (direcci�n Web). No es permitido el uso de caracteres especiales o caracteres en may�sculas en el t�tulo.', '2012-11-08 11:08:43.173252', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal.3.text', 'No permitimos avisos de animales prohibidos por las leyes chilenas de protecci�n animal.', '2012-11-08 11:08:43.173374', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal.4.text', 'No permitimos la inclusi�n de un enlace dirigido a otra p�gina web de e-mail o n�meros de tel�fono en el texto del aviso. No est� permitido hablar de otros sitios web o las subastas de avisos clasificados. Estos v�nculos y referencias deben ser eliminados antes de volver a enviar la notificaci�n.', '2012-11-08 11:08:43.173481', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal.5.text', 'Has introducido un aviso en forma individual o categor�as que no permiten avisos de empresas. Las empresas deben insertar avisos y hacer una lista como los avisos de empresas. Los avisos de empresas no est�n permitidos en las siguientes categor�as: Video Juegos, telefon�a, ropa y prendas de vestir, bolsos, mochilas y accesorios, joyas, relojes y joyas, etc.', '2012-11-08 11:08:43.173586', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal.6.text', 'No hemos podido verificar tu informaci�n de contacto. Por favor, verifica que toda la informaci�n introducida es correcta. Despu�s de la revisi�n, puedes volver a enviar tu aviso.', '2012-11-08 11:08:43.173718', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal.7.text', 'Hay muchos elementos en el mismo aviso. Por favor, escribe  cada elemento de los avisos por separado. No est� permitido introducir m�s de un veh�culo o propiedad en el mismo aviso, excepto si la transacci�n es un intercambio (por ejemplo, 2 por 1).                             ', '2012-11-08 11:08:43.173841', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal.8.text', 'Hay muchas ofertas de trabajo en el mismo aviso. Escribe una oferta por aviso.  No puedes publicar m�s de una propiedad en el mismo aviso.', '2012-11-08 11:08:43.174036', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal.9.text', 'Has introducido un aviso de una empresa. Los avisos deben ser personales.', '2012-11-08 11:08:43.174149', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal.10.text', 'Hay muchos elementos en el mismo aviso. Escribe cada elemento de la lista por separado, para que tu aviso sea m�s relevante para los compradores. No est� permitido introducir m�s de un veh�culo o propiedad en el mismo aviso, a menos que la transacci�n sea  un intercambio necesario (por  ejemplo, 2 por 1).', '2012-11-08 11:08:43.174252', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal.11.text', 'Ya existe otro aviso como el que has introducido. No puedes copiar el texto de los avisos de otros anunciantes, ya que est�n bajo la ley de derechos de autor.. Tampoco est� permitido publicar varios avisos con el mismo producto  o servicio. El aviso anterior debe ser borrado antes de publicar un nuevo  aviso. Tampoco se permite hacer publicidad del mismo art�culo, o servicio en  las diferentes categor�as de avisos en las diferentes regiones.', '2012-11-08 11:08:43.174358', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal.12.text', 'El  producto  ya no est� disponible/ha expirado/fue vendido.', '2012-11-08 11:08:43.17448', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal.13.text', 'Nuestro sitio ofrece clasificados s�lo en Chile. Los productos o servicios s�lo se  encuentran en  Chile y el aviso ser� colocado en la zona donde se encuentra. No se aceptan avisos de fuera del pa�s o en otro idioma que no sea el espa�ol.', '2012-11-08 11:08:43.174582', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal_order.21', '24', '2012-11-08 11:08:43.180063', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal_order.22', '13', '2012-11-08 11:08:43.180208', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal_order.23', '16', '2012-11-08 11:08:43.18032', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal_order.24', '23', '2012-11-08 11:08:43.180452', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal.14.text', 'Tu aviso contiene productos ilegales y/o prohibidos  para la  venta en nuestro sitio. No est� permito hacer publicidad de productos falsificados o falsos, como los productos de marca, CD / VCD / DVD, software / consola de juegos. Para que el aviso sea aceptado y para que este requisitto se aplique, el anunciante debe garantizar que los productos son originales. Los avisos que incluyen los productos para su reventa a la luz de la legislaci�n chilena no pueden ser publicados. Tenemos restricciones sobre el aviso de ciertos bienes y servicios. Lee nuestros T�rminos. Los servicios ofrecidos o solicitados deben cumplir con las leyes chilenas y reglamentarias aplicables a cada profesi�n.', '2012-11-08 11:08:43.17473', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal.15.text', 'Las im�genes deben concordar con los avisos y deben ser relevantes para el art�culo o servicio anunciado. Las im�genes deben representar el art�culo anunciado. No puedes utilizar las im�genes para art�culos del cat�logo de segunda mano, o utilizar logotipos e im�genes de la empresa, excepto en los "Servicios" y "Empleo". No se permite el uso de im�genes de otros anunciantes, sin su consentimiento previo, ni marcas de agua o logos de sitios de la competencia. Las im�genes est�n protegidas por la legislaci�n sobre derechos de autor. No se permite el uso de una imagen en m�s de un aviso.', '2012-11-08 11:08:43.17517', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal.16.text', 'Una o m�s im�genes contienen errores y no se muestran correctamente. Por favor, aseg�rate que el formato de las  im�genes  es  JPG, GIF o BMP. ', '2012-11-08 11:08:43.175333', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal.17.text', 'Su aviso no est� en espa�ol', '2012-11-08 11:08:43.175437', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal.18.text', 'Est�s utilizando enlaces que no son relevantes para el aviso y/o que no funcionan', '2012-11-08 11:08:43.175554', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal.19.text', 'No se permite  publicar im�genes obscenas que muestren a la gente desnuda, en ropa interior o traje de ba�o.', '2012-11-08 11:08:43.175676', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal.20.text', 'Tu aviso puede ser ofensivo para los grupos �tnicos / religiosos, o puede ser considerado  racista, xen�fobo o terrorista, ya que atenta contra el g�nero humano. No se permiten avisos que violen normas constitucionales y que incorporen  contenidos, mensajes o productos de naturaleza violenta o degradantes.', '2012-11-08 11:08:43.175824', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal.21.text', 'No  est� permitido hacer publicidad de productos falsificados o falsos, como los productos de marca, CD / VCD / DVD, software / consola de juegos. Para que el aviso sea aceptado, el anunciante debe garantizar que los productos son originales. Los avisos que incluyen los productos para su reventa a la luz de la legislaci�n chilena, no pueden ser publicados.', '2012-11-08 11:08:43.175955', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal.22.text', 'S�lo se  permite hacer publicidad de ventas, arriendos, empleo y servicios.', '2012-11-08 11:08:43.17606', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal.23.text', 'El aviso contiene productos o servicios que no est�n permitidos en nuestro sitio. Para m�s informaci�n sobre estos productos, visite la p�gina de las Reglas. Si usted tiene alguna pregunta p�ngase en contacto con Atenci�n al Cliente.', '2012-11-08 11:08:43.176167', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal.24.text', 'No est�  permitido el uso de avisos  que tengan como �nico prop�sito el  marketing,  lo que no implica necesariamente el suministro de bienes, trabajos o servicios.', '2012-11-08 11:08:43.17629', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal.25.text', 'No est�  permitido el uso de avisos  que tengan como �nico prop�sito el  marketing,  lo que no implica necesariamente el suministro de bienes, trabajos o servicios.', '2012-11-08 11:08:43.176432', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal.26.text', 'No se pueden  introducir o difundir en la red,  programas de datos (virus y software maliciosos) que puedan  causar da�os al proveedor de acceso, a sistemas inform�ticos  de nuestros usuarios del sitio o de terceros que utilicen la misma red.', '2012-11-08 11:08:43.176567', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal.27.text', 'No  se ha declarado la autenticidad de tu producto. Para que el aviso sea  aceptado, el anunciante debe garantizar que los productos son originales.', '2012-11-08 11:08:43.176679', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal.29.text', 'No se permite la publicaci�n de avisos que no posean ofertas cre�bles y realistas. No se permite que los avisos contengan cualquier informaci�n con contenidos falsos, ambiguos o inexactos, con el fin de inducir al error a potenciales receptores de dicha informaci�n.', '2012-11-08 11:08:43.176782', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal.30.text', 'El aviso se publicar� en la categor�a que mejor describa el art�culo o servicio. Nos reservamos el derecho, si es necesario, a mover el aviso a la categor�a m�s apropiada. Bienes y servicios que no entren en la misma categor�a ser�n publicados en diferentes avisos. Para la venta se publicar� en "Se vende" y los avisos que demanden  un producto se publicar�n en "Se compra". En algunas categor�as los avisos podr�an incluir las opciones de "Arriendo" y "Se busca  arriendo". En otras categor�as, si es necesario, los avisos de "Se arrienda"  se publicar�n en "Venta" y los avisos "Quiero  arrendar", se publicar�n en "Se compra".', '2012-11-08 11:08:43.176923', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal.31.text', 'No est� permitido enviar publicidad no solicitada o autorizada, material publicitario, "correo basura", "cartas en cadena", "marketing piramidal" o cualquier otra forma de solicitud.Tu aviso se inscribe en estas categor�as.', '2012-11-08 11:08:43.177048', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal.32.text', 'No se permite difundir, transmitir o poner a disposici�n de terceros cualquier tipo de informaci�n, elemento o contenido que implica la violaci�n de los derechos de propiedad intelectual, incluyendo derechos de autor y propiedad industrial,  marcas, derechos de autor o de propiedad de los due�os de este sitio o de terceros.', '2012-11-08 11:08:43.177153', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal.33.text', 'No se permite difundir, transmitir o poner a disposici�n de terceros, cualquier tipo de informaci�n, elemento o contenido que implique  la violaci�n del secreto de las comunicaciones y la intimidad.', '2012-11-08 11:08:43.177276', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal.34.text', 'No se permiten m�s de cinco referencias a los productos  que podr�an constituir la base del intercambio. Los intercambios est�n permitidos en el sitio, pero se debe hacer una lista de menos de cinco referencias a los productos que forman la base del intercambio.', '2012-11-08 11:08:43.177387', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal.35.text', 'Categor�a errada', '2012-11-08 11:08:43.177519', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal_order.0', '32', '2012-11-08 11:08:43.177641', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal_order.1', '33', '2012-11-08 11:08:43.177748', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal_order.2', '31', '2012-11-08 11:08:43.177852', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal_order.3', '27', '2012-11-08 11:08:43.177993', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal_order.4', '25', '2012-11-08 11:08:43.178104', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal_order.5', '0', '2012-11-08 11:08:43.178243', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal_order.6', '5', '2012-11-08 11:08:43.178366', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal_order.7', '18', '2012-11-08 11:08:43.178469', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal_order.8', '19', '2012-11-08 11:08:43.178571', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal_order.9', '3', '2012-11-08 11:08:43.178689', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal_order.10', '20', '2012-11-08 11:08:43.1788', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal_order.11', '2', '2012-11-08 11:08:43.178928', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal_order.12', '1', '2012-11-08 11:08:43.179034', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal_order.13', '12', '2012-11-08 11:08:43.179141', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal_order.14', '10', '2012-11-08 11:08:43.179244', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal_order.15', '6', '2012-11-08 11:08:43.179382', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal_order.16', '7', '2012-11-08 11:08:43.179512', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal_order.17', '9', '2012-11-08 11:08:43.179617', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal_order.18', '14', '2012-11-08 11:08:43.17972', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal_order.19', '11', '2012-11-08 11:08:43.179823', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal_order.20', '22', '2012-11-08 11:08:43.179943', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal_order.25', '29', '2012-11-08 11:08:43.180556', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal_order.26', '34', '2012-11-08 11:08:43.18072', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal_order.27', '8', '2012-11-08 11:08:43.180844', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal_order.28', '35', '2012-11-08 11:08:43.180995', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal_order.29', '21', '2012-11-08 11:08:43.181101', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal_order.30', '4', '2012-11-08 11:08:43.181221', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal_order.31', '30', '2012-11-08 11:08:43.181326', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal_order.32', '26', '2012-11-08 11:08:43.181428', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal_order.33', '25', '2012-11-08 11:08:43.181552', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal_order.35', '17', '2012-11-08 11:08:43.181671', NULL);
INSERT INTO conf VALUES ('*.*.ais.synonyms.word.1.word', 'caravana', '2012-11-08 11:08:43.181779', NULL);
INSERT INTO conf VALUES ('*.*.ais.synonyms.word.1.synonyms.2', 'casa-m�vel', '2012-11-08 11:08:43.181886', NULL);
INSERT INTO conf VALUES ('*.*.ais.synonyms.word.1.synonyms.4', 'roulotte', '2012-11-08 11:08:43.181993', NULL);
INSERT INTO conf VALUES ('*.*.ais.synonyms.word.2.word', 'piso', '2012-11-08 11:08:43.182092', NULL);
INSERT INTO conf VALUES ('*.*.ais.synonyms.word.2.synonyms.1', 'apartamento', '2012-11-08 11:08:43.182222', NULL);
INSERT INTO conf VALUES ('*.*.ais.synonyms.word.2.synonyms.2', 'loft', '2012-11-08 11:08:43.182331', NULL);
INSERT INTO conf VALUES ('*.*.ais.synonyms.word.3.word', 'vw', '2012-11-08 11:08:43.18243', NULL);
INSERT INTO conf VALUES ('*.*.ais.synonyms.word.3.synonyms.1', 'volkswagen', '2012-11-08 11:08:43.182529', NULL);
INSERT INTO conf VALUES ('*.*.ais.synonyms.word.4.word', 'carro', '2012-11-08 11:08:43.182628', NULL);
INSERT INTO conf VALUES ('*.*.ais.synonyms.word.4.synonyms.1', 'autom�vel', '2012-11-08 11:08:43.182735', NULL);
INSERT INTO conf VALUES ('*.*.ais.synonyms.word.4.synonyms.2', 'buga', '2012-11-08 11:08:43.182853', NULL);
INSERT INTO conf VALUES ('*.*.ais.misspellings.word.1.misspelled.1', 'vlokswagen', '2012-11-08 11:08:43.182973', NULL);
INSERT INTO conf VALUES ('*.*.ais.misspellings.word.1.misspelled.2', 'volksbagen', '2012-11-08 11:08:43.183071', NULL);
INSERT INTO conf VALUES ('*.*.ais.misspellings.word.1.misspelled.3', 'volsvagen', '2012-11-08 11:08:43.18317', NULL);
INSERT INTO conf VALUES ('*.*.ais.misspellings.word.1.intended', 'volkswagen', '2012-11-08 11:08:43.183298', NULL);
INSERT INTO conf VALUES ('*.*.ais.misspellings.word.2.misspelled.1', 'apratamento', '2012-11-08 11:08:43.183418', NULL);
INSERT INTO conf VALUES ('*.*.ais.misspellings.word.2.misspelled.2', 'apatamento', '2012-11-08 11:08:43.183536', NULL);
INSERT INTO conf VALUES ('*.*.ais.misspellings.word.2.misspelled.3', 'apartametno', '2012-11-08 11:08:43.183643', NULL);
INSERT INTO conf VALUES ('*.*.ais.misspellings.word.2.intended', 'apartamento', '2012-11-08 11:08:43.183744', NULL);
INSERT INTO conf VALUES ('*.*.ais.misspellings.word.3.misspelled.1', 'carabana', '2012-11-08 11:08:43.183844', NULL);
INSERT INTO conf VALUES ('*.*.ais.misspellings.word.3.misspelled.2', 'cravana', '2012-11-08 11:08:43.184001', NULL);
INSERT INTO conf VALUES ('*.*.ais.misspellings.word.3.intended', 'caravana', '2012-11-08 11:08:43.184103', NULL);
INSERT INTO conf VALUES ('*.*.common.uf_conversion_factor', '22245,5290', '2012-11-08 11:08:43.18422', NULL);
INSERT INTO conf VALUES ('*.*.common.uf_conversion_updated', '2012-11-08 11:08:43.184328-03', '2012-11-08 11:08:43.184328', NULL);


--
-- Data for Name: event_log; Type: TABLE DATA; Schema: public; Owner: dany
--



--
-- Data for Name: example; Type: TABLE DATA; Schema: public; Owner: dany
--

INSERT INTO example VALUES (0, 12);
INSERT INTO example VALUES (1, 56);
INSERT INTO example VALUES (2, 32);
INSERT INTO example VALUES (3, 156);
INSERT INTO example VALUES (4, 2345);


--
-- Data for Name: filters; Type: TABLE DATA; Schema: public; Owner: dany
--

INSERT INTO filters VALUES (1, 'all', '', '1=1', true);
INSERT INTO filters VALUES (2, 'unpaid', '', 'ad_actions.state = ''unpaid''', false);
INSERT INTO filters VALUES (3, 'unverified', '', 'ad_actions.state = ''unverified''', false);
INSERT INTO filters VALUES (4, 'new', '', 'ad_actions.queue = ''normal'' AND ad_actions.state IN (''pending_review'', ''locked'')', false);
INSERT INTO filters VALUES (5, 'unsolved', '', 'ad_actions.queue = ''unsolved'' AND ad_actions.state IN (''pending_review'',''locked'')', false);
INSERT INTO filters VALUES (6, 'abuse', '', 'ad_actions.queue = ''abuse'' AND ad_actions.state IN (''pending_review'',''locked'')', false);
INSERT INTO filters VALUES (7, 'accepted', '', 'ad_actions.state = ''accepted''', false);
INSERT INTO filters VALUES (8, 'refused', '', 'ad_actions.state = ''refused''', true);
INSERT INTO filters VALUES (9, 'technical_error', '', 'ad_actions.queue = ''technical_error'' AND ad_actions.state NOT IN (''accepted'', ''refused'')', false);
INSERT INTO filters VALUES (10, 'published', '', 'ad_actions.state IN (''accepted'', ''published'') AND NOT EXISTS (SELECT * FROM action_states AS newer WHERE ad_id = ad_actions.ad_id AND action_id != ad_actions.action_id AND state = ''accepted'' AND timestamp > newer.timestamp) AND NOT EXISTS ( SELECT * FROM ads WHERE ads.status = ''deleted'' AND ad_id=ad_actions.ad_id)', false);
INSERT INTO filters VALUES (11, 'deleted', '', 'ad_actions.state = ''deleted''', true);
INSERT INTO filters VALUES (12, 'autoaccept', '', 'ad_actions.queue = ''autoaccept'' AND ad_actions.state IN (''pending_review'', ''locked'')', false);
INSERT INTO filters VALUES (13, 'autorefuse', '', 'ad_actions.queue = ''autorefuse'' AND ad_actions.state IN (''pending_review'', ''locked'')', false);
INSERT INTO filters VALUES (16, 'video', '', 'ad_actions.queue = ''video'' AND ad_actions.state IN (''pending_review'', ''locked'')', false);
INSERT INTO filters VALUES (17, 'spidered', '', 'ad_actions.queue = ''spidered'' AND ad_actions.state IN (''pending_review'', ''locked'')', false);


--
-- Data for Name: hold_mail_params; Type: TABLE DATA; Schema: public; Owner: dany
--



--
-- Data for Name: iteminfo_items; Type: TABLE DATA; Schema: public; Owner: dany
--

INSERT INTO iteminfo_items VALUES (1, 'root', false, NULL);
INSERT INTO iteminfo_items VALUES (2, 'primogenito', true, 1);
INSERT INTO iteminfo_items VALUES (3, 'segundon', false, 1);
INSERT INTO iteminfo_items VALUES (4, 'car_info', false, 1);
INSERT INTO iteminfo_items VALUES (5, 'location_info', false, 1);
INSERT INTO iteminfo_items VALUES (6, 'object_info', false, 1);
INSERT INTO iteminfo_items VALUES (7, 'moto_info', false, 1);
INSERT INTO iteminfo_items VALUES (8, 'sac', false, 6);
INSERT INTO iteminfo_items VALUES (9, 'chanel', false, 8);
INSERT INTO iteminfo_items VALUES (10, 'bmw', false, 7);
INSERT INTO iteminfo_items VALUES (11, 'triumph', false, 7);
INSERT INTO iteminfo_items VALUES (12, 'r100', true, 10);
INSERT INTO iteminfo_items VALUES (13, 'r1100 gs', true, 10);
INSERT INTO iteminfo_items VALUES (14, 'k100', true, 10);
INSERT INTO iteminfo_items VALUES (15, 'america', true, 11);
INSERT INTO iteminfo_items VALUES (16, 'adventurer', true, 11);
INSERT INTO iteminfo_items VALUES (17, '1000', false, 12);
INSERT INTO iteminfo_items VALUES (18, '1100', false, 13);
INSERT INTO iteminfo_items VALUES (19, '1100 75 ans', false, 13);
INSERT INTO iteminfo_items VALUES (20, '1100 abs', false, 13);
INSERT INTO iteminfo_items VALUES (21, '1100 abs ergo', false, 13);
INSERT INTO iteminfo_items VALUES (22, '1100 ergo', false, 13);
INSERT INTO iteminfo_items VALUES (23, 'k100 lt', false, 14);
INSERT INTO iteminfo_items VALUES (24, 'k100 rs', false, 14);
INSERT INTO iteminfo_items VALUES (25, '865', false, 15);
INSERT INTO iteminfo_items VALUES (26, '900', false, 16);
INSERT INTO iteminfo_items VALUES (27, 'ford', false, 4);
INSERT INTO iteminfo_items VALUES (28, 'renault', false, 4);
INSERT INTO iteminfo_items VALUES (29, 'seat', false, 4);
INSERT INTO iteminfo_items VALUES (30, 'escort', true, 27);
INSERT INTO iteminfo_items VALUES (31, 'fiesta', true, 27);
INSERT INTO iteminfo_items VALUES (32, 'laguna', true, 28);
INSERT INTO iteminfo_items VALUES (33, 'megane', true, 28);
INSERT INTO iteminfo_items VALUES (34, 'scenic', true, 28);
INSERT INTO iteminfo_items VALUES (35, 'cordoba', true, 29);
INSERT INTO iteminfo_items VALUES (36, 'ibiza', true, 29);
INSERT INTO iteminfo_items VALUES (37, 'leon', true, 29);
INSERT INTO iteminfo_items VALUES (38, 'cosworth', false, 30);
INSERT INTO iteminfo_items VALUES (39, 'rs 2000', false, 30);
INSERT INTO iteminfo_items VALUES (40, '1.8 turbo', false, 31);
INSERT INTO iteminfo_items VALUES (41, '1.4 senso 5p', false, 31);
INSERT INTO iteminfo_items VALUES (42, '1.8 16s', false, 32);
INSERT INTO iteminfo_items VALUES (43, '1.8 gpl', false, 32);
INSERT INTO iteminfo_items VALUES (44, '2.0 rxe', false, 32);
INSERT INTO iteminfo_items VALUES (45, '1.9 dti', false, 32);
INSERT INTO iteminfo_items VALUES (46, '2.2 d rt', false, 32);
INSERT INTO iteminfo_items VALUES (47, '2.2 d rxe', false, 32);
INSERT INTO iteminfo_items VALUES (48, '1.9 dci 110 authentique', false, 32);
INSERT INTO iteminfo_items VALUES (49, '2.2 dci 150 initiale', false, 32);
INSERT INTO iteminfo_items VALUES (50, '1.9 dci 120 ch expression', false, 32);
INSERT INTO iteminfo_items VALUES (51, '1.9 sdi 3p', false, 36);
INSERT INTO iteminfo_items VALUES (52, '1.9 tdi 105 reference', false, 37);
INSERT INTO iteminfo_items VALUES (53, '2.0 tdi 140 stylance', false, 37);
INSERT INTO iteminfo_items VALUES (54, 'tdi 110 signo', false, 37);
INSERT INTO iteminfo_items VALUES (55, 'tdi 150 fr', false, 37);
INSERT INTO iteminfo_items VALUES (56, 'tdi 150 sport', false, 37);
INSERT INTO iteminfo_items VALUES (57, 'rx4 1.9 dci', false, 34);
INSERT INTO iteminfo_items VALUES (58, '1.9 d rte', false, 34);
INSERT INTO iteminfo_items VALUES (59, 'd rte', false, 34);
INSERT INTO iteminfo_items VALUES (2001, '22', false, 5);
INSERT INTO iteminfo_items VALUES (2002, '01', false, 2001);
INSERT INTO iteminfo_items VALUES (2003, '01000', false, 2002);
INSERT INTO iteminfo_items VALUES (2004, 'bourg en bresse', false, 2003);
INSERT INTO iteminfo_items VALUES (2005, 'st denis les bourg', false, 2003);
INSERT INTO iteminfo_items VALUES (2006, '12', false, 5);
INSERT INTO iteminfo_items VALUES (2007, '75', false, 2006);
INSERT INTO iteminfo_items VALUES (2008, '75001', false, 2007);
INSERT INTO iteminfo_items VALUES (2009, 'paris', false, 2008);


--
-- Data for Name: iteminfo_data; Type: TABLE DATA; Schema: public; Owner: dany
--

INSERT INTO iteminfo_data VALUES (1, 'Hipoteca', 'De cojones', 1);
INSERT INTO iteminfo_data VALUES (2, 'Sueldo', 'Justico', 1);
INSERT INTO iteminfo_data VALUES (3, 'Broncas', 'Todas', 2);
INSERT INTO iteminfo_data VALUES (4, 'Bici', 'Heredada de su hermano', 3);
INSERT INTO iteminfo_data VALUES (5, 'price_logic_max_cat_2120', '10000', 6);
INSERT INTO iteminfo_data VALUES (6, 'price_logic_max_cat_3040', '20000', 6);
INSERT INTO iteminfo_data VALUES (7, 'price_logic_max_cat_3060', '10000', 6);
INSERT INTO iteminfo_data VALUES (8, 'price_logic_max_cat_8020', '20000', 6);
INSERT INTO iteminfo_data VALUES (9, 'price_logic_max_cat_8020', '560', 8);
INSERT INTO iteminfo_data VALUES (10, 'price_logic_max_cat_22', '1050', 9);
INSERT INTO iteminfo_data VALUES (11, 'price_fraud_cat_8020', '1050', 9);
INSERT INTO iteminfo_data VALUES (12, 'price_logic_max_cat_8060', '2000', 6);
INSERT INTO iteminfo_data VALUES (13, 'price_logic_max_cat_4100', '15000', 6);
INSERT INTO iteminfo_data VALUES (14, 'price_logic_max_cat_4120', '10000', 6);
INSERT INTO iteminfo_data VALUES (15, 'price_logic_max_cat_39', '10000', 6);
INSERT INTO iteminfo_data VALUES (16, 'price_logic_max_cat_41', '20000', 6);
INSERT INTO iteminfo_data VALUES (17, 'price_fraud_1976', '1600', 12);
INSERT INTO iteminfo_data VALUES (18, 'price_logic_min_1976', '2560', 12);
INSERT INTO iteminfo_data VALUES (19, 'price_logic_max_1976', '6400', 12);
INSERT INTO iteminfo_data VALUES (20, 'price_fraud_1977', '975', 12);
INSERT INTO iteminfo_data VALUES (21, 'price_logic_min_1977', '1560', 12);
INSERT INTO iteminfo_data VALUES (22, 'price_logic_max_1977', '3900', 12);
INSERT INTO iteminfo_data VALUES (23, 'price_fraud_1978', '1498.5', 12);
INSERT INTO iteminfo_data VALUES (24, 'price_logic_min_1978', '2397.6', 12);
INSERT INTO iteminfo_data VALUES (25, 'price_logic_max_1978', '5994', 12);
INSERT INTO iteminfo_data VALUES (26, 'price_fraud_1979', '1450', 12);
INSERT INTO iteminfo_data VALUES (27, 'price_logic_min_1979', '2320', 12);
INSERT INTO iteminfo_data VALUES (28, 'price_logic_max_1979', '5800', 12);
INSERT INTO iteminfo_data VALUES (29, 'price_fraud_1980', '1768', 12);
INSERT INTO iteminfo_data VALUES (30, 'price_logic_min_1980', '2828.8', 12);
INSERT INTO iteminfo_data VALUES (31, 'price_logic_max_1980', '7072', 12);
INSERT INTO iteminfo_data VALUES (32, 'price_fraud_1981', '1325', 12);
INSERT INTO iteminfo_data VALUES (33, 'price_logic_min_1981', '2120', 12);
INSERT INTO iteminfo_data VALUES (34, 'price_logic_max_1981', '5300', 12);
INSERT INTO iteminfo_data VALUES (35, 'price_fraud_1982', '1325', 12);
INSERT INTO iteminfo_data VALUES (36, 'price_logic_min_1982', '2120', 12);
INSERT INTO iteminfo_data VALUES (37, 'price_logic_max_1982', '5300', 12);
INSERT INTO iteminfo_data VALUES (38, 'price_fraud_1983', '1500', 12);
INSERT INTO iteminfo_data VALUES (39, 'price_logic_min_1983', '2400', 12);
INSERT INTO iteminfo_data VALUES (40, 'price_logic_max_1983', '6000', 12);
INSERT INTO iteminfo_data VALUES (41, 'price_fraud_1984', '1900', 12);
INSERT INTO iteminfo_data VALUES (42, 'price_logic_min_1984', '3040', 12);
INSERT INTO iteminfo_data VALUES (43, 'price_logic_max_1984', '7600', 12);
INSERT INTO iteminfo_data VALUES (44, 'price_fraud_1988', '1450', 12);
INSERT INTO iteminfo_data VALUES (45, 'price_logic_min_1988', '2320', 12);
INSERT INTO iteminfo_data VALUES (46, 'price_logic_max_1988', '5800', 12);
INSERT INTO iteminfo_data VALUES (47, 'price_fraud_1989', '1500', 12);
INSERT INTO iteminfo_data VALUES (48, 'price_logic_min_1989', '2400', 12);
INSERT INTO iteminfo_data VALUES (49, 'price_logic_max_1989', '6000', 12);
INSERT INTO iteminfo_data VALUES (50, 'price_fraud_1990', '2000', 12);
INSERT INTO iteminfo_data VALUES (51, 'price_logic_min_1990', '3200', 12);
INSERT INTO iteminfo_data VALUES (52, 'price_logic_max_1990', '8000', 12);
INSERT INTO iteminfo_data VALUES (53, 'price_fraud_1991', '1900', 12);
INSERT INTO iteminfo_data VALUES (54, 'price_logic_min_1991', '3040', 12);
INSERT INTO iteminfo_data VALUES (55, 'price_logic_max_1991', '7600', 12);
INSERT INTO iteminfo_data VALUES (56, 'price_fraud_1992', '1942.5', 12);
INSERT INTO iteminfo_data VALUES (57, 'price_logic_min_1992', '3108', 12);
INSERT INTO iteminfo_data VALUES (58, 'price_logic_max_1992', '7770', 12);
INSERT INTO iteminfo_data VALUES (59, 'price_fraud_1993', '2322.5', 12);
INSERT INTO iteminfo_data VALUES (60, 'price_logic_min_1993', '3716', 12);
INSERT INTO iteminfo_data VALUES (61, 'price_logic_max_1993', '9290', 12);
INSERT INTO iteminfo_data VALUES (62, 'price_fraud_1994', '2625', 12);
INSERT INTO iteminfo_data VALUES (63, 'price_logic_min_1994', '4200', 12);
INSERT INTO iteminfo_data VALUES (64, 'price_logic_max_1994', '10500', 12);
INSERT INTO iteminfo_data VALUES (65, 'price_fraud_1995', '1691.5', 12);
INSERT INTO iteminfo_data VALUES (66, 'price_logic_min_1995', '2706.4', 12);
INSERT INTO iteminfo_data VALUES (67, 'price_logic_max_1995', '6766', 12);
INSERT INTO iteminfo_data VALUES (68, 'price_fraud_1996', '2525', 12);
INSERT INTO iteminfo_data VALUES (69, 'price_logic_min_1996', '4040', 12);
INSERT INTO iteminfo_data VALUES (70, 'price_logic_max_1996', '10100', 12);
INSERT INTO iteminfo_data VALUES (71, 'price_fraud_1975', '2487.5', 11);
INSERT INTO iteminfo_data VALUES (72, 'price_logic_min_1975', '3980', 11);
INSERT INTO iteminfo_data VALUES (73, 'price_logic_max_1975', '9950', 11);
INSERT INTO iteminfo_data VALUES (74, 'price_fraud_1977', '2150', 11);
INSERT INTO iteminfo_data VALUES (75, 'price_logic_min_1977', '3440', 11);
INSERT INTO iteminfo_data VALUES (76, 'price_logic_max_1977', '8600', 11);
INSERT INTO iteminfo_data VALUES (77, 'price_fraud_1978', '2400', 11);
INSERT INTO iteminfo_data VALUES (78, 'price_logic_min_1978', '3840', 11);
INSERT INTO iteminfo_data VALUES (79, 'price_logic_max_1978', '9600', 11);
INSERT INTO iteminfo_data VALUES (80, 'price_fraud_1979', '2625', 11);
INSERT INTO iteminfo_data VALUES (81, 'price_logic_min_1979', '4200', 11);
INSERT INTO iteminfo_data VALUES (82, 'price_logic_max_1979', '10500', 11);
INSERT INTO iteminfo_data VALUES (83, 'price_fraud_1980', '3050', 11);
INSERT INTO iteminfo_data VALUES (84, 'price_logic_min_1980', '4880', 11);
INSERT INTO iteminfo_data VALUES (85, 'price_logic_max_1980', '12200', 11);
INSERT INTO iteminfo_data VALUES (86, 'price_fraud_1981', '2750', 11);
INSERT INTO iteminfo_data VALUES (87, 'price_logic_min_1981', '4400', 11);
INSERT INTO iteminfo_data VALUES (88, 'price_logic_max_1981', '11000', 11);
INSERT INTO iteminfo_data VALUES (89, 'price_fraud_1986', '6750', 11);
INSERT INTO iteminfo_data VALUES (90, 'price_logic_min_1986', '10800', 11);
INSERT INTO iteminfo_data VALUES (91, 'price_logic_max_1986', '27000', 11);
INSERT INTO iteminfo_data VALUES (92, 'price_fraud_1992', '1428.5', 11);
INSERT INTO iteminfo_data VALUES (93, 'price_logic_min_1992', '2285.6', 11);
INSERT INTO iteminfo_data VALUES (94, 'price_logic_max_1992', '5714', 11);
INSERT INTO iteminfo_data VALUES (95, 'price_fraud_1993', '1461', 11);
INSERT INTO iteminfo_data VALUES (96, 'price_logic_min_1993', '2337.6', 11);
INSERT INTO iteminfo_data VALUES (97, 'price_logic_max_1993', '5844', 11);
INSERT INTO iteminfo_data VALUES (98, 'price_fraud_1994', '1866.5', 11);
INSERT INTO iteminfo_data VALUES (99, 'price_logic_min_1994', '2986.4', 11);
INSERT INTO iteminfo_data VALUES (100, 'price_logic_max_1994', '7466', 11);
INSERT INTO iteminfo_data VALUES (101, 'price_fraud_1995', '1736.5', 11);
INSERT INTO iteminfo_data VALUES (102, 'price_logic_min_1995', '2778.4', 11);
INSERT INTO iteminfo_data VALUES (103, 'price_logic_max_1995', '6946', 11);
INSERT INTO iteminfo_data VALUES (104, 'price_fraud_1996', '1842.5', 11);
INSERT INTO iteminfo_data VALUES (105, 'price_logic_min_1996', '2948', 11);
INSERT INTO iteminfo_data VALUES (106, 'price_logic_max_1996', '7370', 11);
INSERT INTO iteminfo_data VALUES (107, 'price_fraud_1997', '1906', 11);
INSERT INTO iteminfo_data VALUES (108, 'price_logic_min_1997', '3049.6', 11);
INSERT INTO iteminfo_data VALUES (109, 'price_logic_max_1997', '7624', 11);
INSERT INTO iteminfo_data VALUES (110, 'price_fraud_1998', '2011.5', 11);
INSERT INTO iteminfo_data VALUES (111, 'price_logic_min_1998', '3218.4', 11);
INSERT INTO iteminfo_data VALUES (112, 'price_logic_max_1998', '8046', 11);
INSERT INTO iteminfo_data VALUES (113, 'price_fraud_1999', '2202.5', 11);
INSERT INTO iteminfo_data VALUES (114, 'price_logic_min_1999', '3524', 11);
INSERT INTO iteminfo_data VALUES (115, 'price_logic_max_1999', '8810', 11);
INSERT INTO iteminfo_data VALUES (116, 'price_fraud_2000', '2071.5', 11);
INSERT INTO iteminfo_data VALUES (117, 'price_logic_min_2000', '3314.4', 11);
INSERT INTO iteminfo_data VALUES (118, 'price_logic_max_2000', '8286', 11);
INSERT INTO iteminfo_data VALUES (119, 'price_fraud_2001', '2397.5', 11);
INSERT INTO iteminfo_data VALUES (120, 'price_logic_min_2001', '3836', 11);
INSERT INTO iteminfo_data VALUES (121, 'price_logic_max_2001', '9590', 11);
INSERT INTO iteminfo_data VALUES (122, 'price_fraud_2002', '2549', 11);
INSERT INTO iteminfo_data VALUES (123, 'price_logic_min_2002', '4078.4', 11);
INSERT INTO iteminfo_data VALUES (124, 'price_logic_max_2002', '10196', 11);
INSERT INTO iteminfo_data VALUES (125, 'price_fraud_2003', '2785', 11);
INSERT INTO iteminfo_data VALUES (126, 'price_logic_min_2003', '4456', 11);
INSERT INTO iteminfo_data VALUES (127, 'price_logic_max_2003', '11140', 11);
INSERT INTO iteminfo_data VALUES (128, 'price_fraud_2004', '3007', 11);
INSERT INTO iteminfo_data VALUES (129, 'price_logic_min_2004', '4811.2', 11);
INSERT INTO iteminfo_data VALUES (130, 'price_logic_max_2004', '12028', 11);
INSERT INTO iteminfo_data VALUES (131, 'price_fraud_2005', '4200.5', 11);
INSERT INTO iteminfo_data VALUES (132, 'price_logic_min_2005', '6720.8', 11);
INSERT INTO iteminfo_data VALUES (133, 'price_logic_max_2005', '16802', 11);
INSERT INTO iteminfo_data VALUES (134, 'price_fraud_2006', '4121.5', 11);
INSERT INTO iteminfo_data VALUES (135, 'price_logic_min_2006', '6594.4', 11);
INSERT INTO iteminfo_data VALUES (136, 'price_logic_max_2006', '16486', 11);
INSERT INTO iteminfo_data VALUES (137, 'price_fraud_2007', '4257.5', 11);
INSERT INTO iteminfo_data VALUES (138, 'price_logic_min_2007', '6812', 11);
INSERT INTO iteminfo_data VALUES (139, 'price_logic_max_2007', '17030', 11);
INSERT INTO iteminfo_data VALUES (140, 'price_fraud_2008', '4357.5', 11);
INSERT INTO iteminfo_data VALUES (141, 'price_logic_min_2008', '6972', 11);
INSERT INTO iteminfo_data VALUES (142, 'price_logic_max_2008', '17430', 11);
INSERT INTO iteminfo_data VALUES (143, 'price_fraud_2001', '2995', 15);
INSERT INTO iteminfo_data VALUES (144, 'price_logic_min_2001', '4792', 15);
INSERT INTO iteminfo_data VALUES (145, 'price_logic_max_2001', '11980', 15);
INSERT INTO iteminfo_data VALUES (146, 'price_fraud_2002', '2625', 15);
INSERT INTO iteminfo_data VALUES (147, 'price_logic_min_2002', '4200', 15);
INSERT INTO iteminfo_data VALUES (148, 'price_logic_max_2002', '10500', 15);
INSERT INTO iteminfo_data VALUES (149, 'price_fraud_2003', '2600', 15);
INSERT INTO iteminfo_data VALUES (150, 'price_logic_min_2003', '4160', 15);
INSERT INTO iteminfo_data VALUES (151, 'price_logic_max_2003', '10400', 15);
INSERT INTO iteminfo_data VALUES (152, 'price_fraud_2004', '3250', 15);
INSERT INTO iteminfo_data VALUES (153, 'price_logic_min_2004', '5200', 15);
INSERT INTO iteminfo_data VALUES (154, 'price_logic_max_2004', '13000', 15);
INSERT INTO iteminfo_data VALUES (155, 'price_fraud_2007', '3750', 15);
INSERT INTO iteminfo_data VALUES (156, 'price_logic_min_2007', '6000', 15);
INSERT INTO iteminfo_data VALUES (157, 'price_logic_max_2007', '15000', 15);
INSERT INTO iteminfo_data VALUES (158, 'price_fraud_1975', '1720.5', 10);
INSERT INTO iteminfo_data VALUES (159, 'price_logic_min_1975', '2752.8', 10);
INSERT INTO iteminfo_data VALUES (160, 'price_logic_max_1975', '6882', 10);
INSERT INTO iteminfo_data VALUES (161, 'price_fraud_1976', '1610.5', 10);
INSERT INTO iteminfo_data VALUES (162, 'price_logic_min_1976', '2576.8', 10);
INSERT INTO iteminfo_data VALUES (163, 'price_logic_max_1976', '6442', 10);
INSERT INTO iteminfo_data VALUES (164, 'price_fraud_1977', '1150', 10);
INSERT INTO iteminfo_data VALUES (165, 'price_logic_min_1977', '1840', 10);
INSERT INTO iteminfo_data VALUES (166, 'price_logic_max_1977', '4600', 10);
INSERT INTO iteminfo_data VALUES (167, 'price_fraud_1978', '1491.5', 10);
INSERT INTO iteminfo_data VALUES (168, 'price_logic_min_1978', '2386.4', 10);
INSERT INTO iteminfo_data VALUES (169, 'price_logic_max_1978', '5966', 10);
INSERT INTO iteminfo_data VALUES (170, 'price_fraud_1979', '1272.5', 10);
INSERT INTO iteminfo_data VALUES (171, 'price_logic_min_1979', '2036', 10);
INSERT INTO iteminfo_data VALUES (172, 'price_logic_max_1979', '5090', 10);
INSERT INTO iteminfo_data VALUES (173, 'price_fraud_1980', '1623', 10);
INSERT INTO iteminfo_data VALUES (174, 'price_logic_min_1980', '2596.8', 10);
INSERT INTO iteminfo_data VALUES (175, 'price_logic_max_1980', '6492', 10);
INSERT INTO iteminfo_data VALUES (176, 'price_fraud_1981', '1257', 10);
INSERT INTO iteminfo_data VALUES (177, 'price_logic_min_1981', '2011.2', 10);
INSERT INTO iteminfo_data VALUES (178, 'price_logic_max_1981', '5028', 10);
INSERT INTO iteminfo_data VALUES (179, 'price_fraud_1982', '1237.5', 10);
INSERT INTO iteminfo_data VALUES (180, 'price_logic_min_1982', '1980', 10);
INSERT INTO iteminfo_data VALUES (181, 'price_logic_max_1982', '4950', 10);
INSERT INTO iteminfo_data VALUES (182, 'price_fraud_1983', '1111', 10);
INSERT INTO iteminfo_data VALUES (183, 'price_logic_min_1983', '1777.6', 10);
INSERT INTO iteminfo_data VALUES (184, 'price_logic_max_1983', '4444', 10);
INSERT INTO iteminfo_data VALUES (185, 'price_fraud_1984', '1127', 10);
INSERT INTO iteminfo_data VALUES (186, 'price_logic_min_1984', '1803.2', 10);
INSERT INTO iteminfo_data VALUES (187, 'price_logic_max_1984', '4508', 10);
INSERT INTO iteminfo_data VALUES (188, 'price_fraud_1985', '1425', 10);
INSERT INTO iteminfo_data VALUES (189, 'price_logic_min_1985', '2280', 10);
INSERT INTO iteminfo_data VALUES (190, 'price_logic_max_1985', '5700', 10);
INSERT INTO iteminfo_data VALUES (191, 'price_fraud_1986', '1179', 10);
INSERT INTO iteminfo_data VALUES (192, 'price_logic_min_1986', '1886.4', 10);
INSERT INTO iteminfo_data VALUES (193, 'price_logic_max_1986', '4716', 10);
INSERT INTO iteminfo_data VALUES (194, 'price_fraud_1987', '1336.5', 10);
INSERT INTO iteminfo_data VALUES (195, 'price_logic_min_1987', '2138.4', 10);
INSERT INTO iteminfo_data VALUES (196, 'price_logic_max_1987', '5346', 10);
INSERT INTO iteminfo_data VALUES (197, 'price_fraud_1988', '1402', 10);
INSERT INTO iteminfo_data VALUES (198, 'price_logic_min_1988', '2243.2', 10);
INSERT INTO iteminfo_data VALUES (199, 'price_logic_max_1988', '5608', 10);
INSERT INTO iteminfo_data VALUES (200, 'price_fraud_1989', '1327.5', 10);
INSERT INTO iteminfo_data VALUES (201, 'price_logic_min_1989', '2124', 10);
INSERT INTO iteminfo_data VALUES (202, 'price_logic_max_1989', '5310', 10);
INSERT INTO iteminfo_data VALUES (203, 'price_fraud_1990', '1678', 10);
INSERT INTO iteminfo_data VALUES (204, 'price_logic_min_1990', '2684.8', 10);
INSERT INTO iteminfo_data VALUES (205, 'price_logic_max_1990', '6712', 10);
INSERT INTO iteminfo_data VALUES (206, 'price_fraud_1991', '1612', 10);
INSERT INTO iteminfo_data VALUES (207, 'price_logic_min_1991', '2579.2', 10);
INSERT INTO iteminfo_data VALUES (208, 'price_logic_max_1991', '6448', 10);
INSERT INTO iteminfo_data VALUES (209, 'price_fraud_1992', '1703', 10);
INSERT INTO iteminfo_data VALUES (210, 'price_logic_min_1992', '2724.8', 10);
INSERT INTO iteminfo_data VALUES (211, 'price_logic_max_1992', '6812', 10);
INSERT INTO iteminfo_data VALUES (212, 'price_fraud_1993', '1699.5', 10);
INSERT INTO iteminfo_data VALUES (213, 'price_logic_min_1993', '2719.2', 10);
INSERT INTO iteminfo_data VALUES (214, 'price_logic_max_1993', '6798', 10);
INSERT INTO iteminfo_data VALUES (215, 'price_fraud_1994', '1561', 10);
INSERT INTO iteminfo_data VALUES (216, 'price_logic_min_1994', '2497.6', 10);
INSERT INTO iteminfo_data VALUES (217, 'price_logic_max_1994', '6244', 10);
INSERT INTO iteminfo_data VALUES (218, 'price_fraud_1995', '1942', 10);
INSERT INTO iteminfo_data VALUES (219, 'price_logic_min_1995', '3107.2', 10);
INSERT INTO iteminfo_data VALUES (220, 'price_logic_max_1995', '7768', 10);
INSERT INTO iteminfo_data VALUES (221, 'price_fraud_1996', '1993', 10);
INSERT INTO iteminfo_data VALUES (222, 'price_logic_min_1996', '3188.8', 10);
INSERT INTO iteminfo_data VALUES (223, 'price_logic_max_1996', '7972', 10);
INSERT INTO iteminfo_data VALUES (224, 'price_fraud_1997', '2398', 10);
INSERT INTO iteminfo_data VALUES (225, 'price_logic_min_1997', '3836.8', 10);
INSERT INTO iteminfo_data VALUES (226, 'price_logic_max_1997', '9592', 10);
INSERT INTO iteminfo_data VALUES (227, 'price_fraud_1998', '2456.5', 10);
INSERT INTO iteminfo_data VALUES (228, 'price_logic_min_1998', '3930.4', 10);
INSERT INTO iteminfo_data VALUES (229, 'price_logic_max_1998', '9826', 10);
INSERT INTO iteminfo_data VALUES (230, 'price_fraud_1999', '2640', 10);
INSERT INTO iteminfo_data VALUES (231, 'price_logic_min_1999', '4224', 10);
INSERT INTO iteminfo_data VALUES (232, 'price_logic_max_1999', '10560', 10);
INSERT INTO iteminfo_data VALUES (233, 'price_fraud_2000', '2942.5', 10);
INSERT INTO iteminfo_data VALUES (234, 'price_logic_min_2000', '4708', 10);
INSERT INTO iteminfo_data VALUES (235, 'price_logic_max_2000', '11770', 10);
INSERT INTO iteminfo_data VALUES (236, 'price_fraud_2001', '3435.5', 10);
INSERT INTO iteminfo_data VALUES (237, 'price_logic_min_2001', '5496.8', 10);
INSERT INTO iteminfo_data VALUES (238, 'price_logic_max_2001', '13742', 10);
INSERT INTO iteminfo_data VALUES (239, 'price_fraud_2002', '3541', 10);
INSERT INTO iteminfo_data VALUES (240, 'price_logic_min_2002', '5665.6', 10);
INSERT INTO iteminfo_data VALUES (241, 'price_logic_max_2002', '14164', 10);
INSERT INTO iteminfo_data VALUES (242, 'price_fraud_2003', '3900', 10);
INSERT INTO iteminfo_data VALUES (243, 'price_logic_min_2003', '6240', 10);
INSERT INTO iteminfo_data VALUES (244, 'price_logic_max_2003', '15600', 10);
INSERT INTO iteminfo_data VALUES (245, 'price_fraud_2004', '4205.5', 10);
INSERT INTO iteminfo_data VALUES (246, 'price_logic_min_2004', '6728.8', 10);
INSERT INTO iteminfo_data VALUES (247, 'price_logic_max_2004', '16822', 10);
INSERT INTO iteminfo_data VALUES (248, 'price_fraud_2005', '4750.5', 10);
INSERT INTO iteminfo_data VALUES (249, 'price_logic_min_2005', '7600.8', 10);
INSERT INTO iteminfo_data VALUES (250, 'price_logic_max_2005', '19002', 10);
INSERT INTO iteminfo_data VALUES (251, 'price_fraud_2006', '5451.5', 10);
INSERT INTO iteminfo_data VALUES (252, 'price_logic_min_2006', '8722.4', 10);
INSERT INTO iteminfo_data VALUES (253, 'price_logic_max_2006', '21806', 10);
INSERT INTO iteminfo_data VALUES (254, 'price_fraud_2007', '6064', 10);
INSERT INTO iteminfo_data VALUES (255, 'price_logic_min_2007', '9702.4', 10);
INSERT INTO iteminfo_data VALUES (256, 'price_logic_max_2007', '24256', 10);
INSERT INTO iteminfo_data VALUES (257, 'price_fraud_2008', '6650.5', 10);
INSERT INTO iteminfo_data VALUES (258, 'price_logic_min_2008', '10640.8', 10);
INSERT INTO iteminfo_data VALUES (259, 'price_logic_max_2008', '26602', 10);
INSERT INTO iteminfo_data VALUES (260, 'price_fraud_1996', '1950', 26);
INSERT INTO iteminfo_data VALUES (261, 'price_logic_min_1996', '3120', 26);
INSERT INTO iteminfo_data VALUES (262, 'price_logic_max_1996', '7800', 26);
INSERT INTO iteminfo_data VALUES (263, 'price_fraud_1997', '1750', 26);
INSERT INTO iteminfo_data VALUES (264, 'price_logic_min_1997', '2800', 26);
INSERT INTO iteminfo_data VALUES (265, 'price_logic_max_1997', '7000', 26);
INSERT INTO iteminfo_data VALUES (266, 'price_fraud_2001', '2800', 26);
INSERT INTO iteminfo_data VALUES (267, 'price_logic_min_2001', '4480', 26);
INSERT INTO iteminfo_data VALUES (268, 'price_logic_max_2001', '11200', 26);
INSERT INTO iteminfo_data VALUES (269, 'price_fraud_1996', '1875', 16);
INSERT INTO iteminfo_data VALUES (270, 'price_logic_min_1996', '3000', 16);
INSERT INTO iteminfo_data VALUES (271, 'price_logic_max_1996', '7500', 16);
INSERT INTO iteminfo_data VALUES (272, 'price_fraud_1997', '1766.5', 16);
INSERT INTO iteminfo_data VALUES (273, 'price_logic_min_1997', '2826.4', 16);
INSERT INTO iteminfo_data VALUES (274, 'price_logic_max_1997', '7066', 16);
INSERT INTO iteminfo_data VALUES (275, 'price_fraud_1998', '2000', 16);
INSERT INTO iteminfo_data VALUES (276, 'price_logic_min_1998', '3200', 16);
INSERT INTO iteminfo_data VALUES (277, 'price_logic_max_1998', '8000', 16);
INSERT INTO iteminfo_data VALUES (278, 'price_fraud_2001', '2800', 16);
INSERT INTO iteminfo_data VALUES (279, 'price_logic_min_2001', '4480', 16);
INSERT INTO iteminfo_data VALUES (280, 'price_logic_max_2001', '11200', 16);
INSERT INTO iteminfo_data VALUES (281, 'price_fraud_1992', '5625', 38);
INSERT INTO iteminfo_data VALUES (282, 'price_logic_min_1992', '9000', 38);
INSERT INTO iteminfo_data VALUES (283, 'price_logic_max_1992', '22500', 38);
INSERT INTO iteminfo_data VALUES (284, 'price_fraud_1993', '9233', 38);
INSERT INTO iteminfo_data VALUES (285, 'price_logic_min_1993', '14772.8', 38);
INSERT INTO iteminfo_data VALUES (286, 'price_logic_max_1993', '36932', 38);
INSERT INTO iteminfo_data VALUES (287, 'price_fraud_1994', '10500', 38);
INSERT INTO iteminfo_data VALUES (288, 'price_logic_min_1994', '16800', 38);
INSERT INTO iteminfo_data VALUES (289, 'price_logic_max_1994', '42000', 38);
INSERT INTO iteminfo_data VALUES (290, 'price_fraud_1995', '12000', 38);
INSERT INTO iteminfo_data VALUES (291, 'price_logic_min_1995', '19200', 38);
INSERT INTO iteminfo_data VALUES (292, 'price_logic_max_1995', '48000', 38);
INSERT INTO iteminfo_data VALUES (293, 'price_fraud_1992', '1450', 39);
INSERT INTO iteminfo_data VALUES (294, 'price_logic_min_1992', '2320', 39);
INSERT INTO iteminfo_data VALUES (295, 'price_logic_max_1992', '5800', 39);
INSERT INTO iteminfo_data VALUES (296, 'price_fraud_1993', '2250', 39);
INSERT INTO iteminfo_data VALUES (297, 'price_logic_min_1993', '3600', 39);
INSERT INTO iteminfo_data VALUES (298, 'price_logic_max_1993', '9000', 39);
INSERT INTO iteminfo_data VALUES (299, 'price_fraud_1995', '1783', 39);
INSERT INTO iteminfo_data VALUES (300, 'price_logic_min_1995', '2852.8', 39);
INSERT INTO iteminfo_data VALUES (301, 'price_logic_max_1995', '7132', 39);
INSERT INTO iteminfo_data VALUES (302, 'price_fraud_1996', '1683', 39);
INSERT INTO iteminfo_data VALUES (303, 'price_logic_min_1996', '2692.8', 39);
INSERT INTO iteminfo_data VALUES (304, 'price_logic_max_1996', '6732', 39);
INSERT INTO iteminfo_data VALUES (305, 'price_fraud_1998', '6100', 39);
INSERT INTO iteminfo_data VALUES (306, 'price_logic_min_1998', '9760', 39);
INSERT INTO iteminfo_data VALUES (307, 'price_logic_max_1998', '24400', 39);
INSERT INTO iteminfo_data VALUES (308, 'price_fraud_1975', '991.5', 30);
INSERT INTO iteminfo_data VALUES (309, 'price_logic_min_1975', '1586.4', 30);
INSERT INTO iteminfo_data VALUES (310, 'price_logic_max_1975', '3966', 30);
INSERT INTO iteminfo_data VALUES (311, 'price_fraud_1976', '750', 30);
INSERT INTO iteminfo_data VALUES (312, 'price_logic_min_1976', '1200', 30);
INSERT INTO iteminfo_data VALUES (313, 'price_logic_max_1976', '3000', 30);
INSERT INTO iteminfo_data VALUES (314, 'price_fraud_1977', '1500', 30);
INSERT INTO iteminfo_data VALUES (315, 'price_logic_min_1977', '2400', 30);
INSERT INTO iteminfo_data VALUES (316, 'price_logic_max_1977', '6000', 30);
INSERT INTO iteminfo_data VALUES (317, 'price_fraud_1979', '437.5', 30);
INSERT INTO iteminfo_data VALUES (318, 'price_logic_min_1979', '700', 30);
INSERT INTO iteminfo_data VALUES (319, 'price_logic_max_1979', '1750', 30);
INSERT INTO iteminfo_data VALUES (320, 'price_fraud_1980', '1050', 30);
INSERT INTO iteminfo_data VALUES (321, 'price_logic_min_1980', '1680', 30);
INSERT INTO iteminfo_data VALUES (322, 'price_logic_max_1980', '4200', 30);
INSERT INTO iteminfo_data VALUES (323, 'price_fraud_1981', '375', 30);
INSERT INTO iteminfo_data VALUES (324, 'price_logic_min_1981', '600', 30);
INSERT INTO iteminfo_data VALUES (325, 'price_logic_max_1981', '1500', 30);
INSERT INTO iteminfo_data VALUES (326, 'price_fraud_1982', '230', 30);
INSERT INTO iteminfo_data VALUES (327, 'price_logic_min_1982', '368', 30);
INSERT INTO iteminfo_data VALUES (328, 'price_logic_max_1982', '920', 30);
INSERT INTO iteminfo_data VALUES (329, 'price_fraud_1983', '343.5', 30);
INSERT INTO iteminfo_data VALUES (330, 'price_logic_min_1983', '549.6', 30);
INSERT INTO iteminfo_data VALUES (331, 'price_logic_max_1983', '1374', 30);
INSERT INTO iteminfo_data VALUES (332, 'price_fraud_1984', '520', 30);
INSERT INTO iteminfo_data VALUES (333, 'price_logic_min_1984', '832', 30);
INSERT INTO iteminfo_data VALUES (334, 'price_logic_max_1984', '2080', 30);
INSERT INTO iteminfo_data VALUES (335, 'price_fraud_1985', '365', 30);
INSERT INTO iteminfo_data VALUES (336, 'price_logic_min_1985', '584', 30);
INSERT INTO iteminfo_data VALUES (337, 'price_logic_max_1985', '1460', 30);
INSERT INTO iteminfo_data VALUES (338, 'price_fraud_1986', '703.5', 30);
INSERT INTO iteminfo_data VALUES (339, 'price_logic_min_1986', '1125.6', 30);
INSERT INTO iteminfo_data VALUES (340, 'price_logic_max_1986', '2814', 30);
INSERT INTO iteminfo_data VALUES (341, 'price_fraud_1987', '432', 30);
INSERT INTO iteminfo_data VALUES (342, 'price_logic_min_1987', '691.2', 30);
INSERT INTO iteminfo_data VALUES (343, 'price_logic_max_1987', '1728', 30);
INSERT INTO iteminfo_data VALUES (344, 'price_fraud_1988', '526', 30);
INSERT INTO iteminfo_data VALUES (345, 'price_logic_min_1988', '841.6', 30);
INSERT INTO iteminfo_data VALUES (346, 'price_logic_max_1988', '2104', 30);
INSERT INTO iteminfo_data VALUES (347, 'price_fraud_1989', '421.5', 30);
INSERT INTO iteminfo_data VALUES (348, 'price_logic_min_1989', '674.4', 30);
INSERT INTO iteminfo_data VALUES (349, 'price_logic_max_1989', '1686', 30);
INSERT INTO iteminfo_data VALUES (350, 'price_fraud_1990', '459', 30);
INSERT INTO iteminfo_data VALUES (351, 'price_logic_min_1990', '734.4', 30);
INSERT INTO iteminfo_data VALUES (352, 'price_logic_max_1990', '1836', 30);
INSERT INTO iteminfo_data VALUES (353, 'price_fraud_1991', '460.5', 30);
INSERT INTO iteminfo_data VALUES (354, 'price_logic_min_1991', '736.8', 30);
INSERT INTO iteminfo_data VALUES (355, 'price_logic_max_1991', '1842', 30);
INSERT INTO iteminfo_data VALUES (356, 'price_fraud_1992', '614', 30);
INSERT INTO iteminfo_data VALUES (357, 'price_logic_min_1992', '982.4', 30);
INSERT INTO iteminfo_data VALUES (358, 'price_logic_max_1992', '2456', 30);
INSERT INTO iteminfo_data VALUES (359, 'price_fraud_1993', '699.5', 30);
INSERT INTO iteminfo_data VALUES (360, 'price_logic_min_1993', '1119.2', 30);
INSERT INTO iteminfo_data VALUES (361, 'price_logic_max_1993', '2798', 30);
INSERT INTO iteminfo_data VALUES (362, 'price_fraud_1994', '684', 30);
INSERT INTO iteminfo_data VALUES (363, 'price_logic_min_1994', '1094.4', 30);
INSERT INTO iteminfo_data VALUES (364, 'price_logic_max_1994', '2736', 30);
INSERT INTO iteminfo_data VALUES (365, 'price_fraud_1995', '768', 30);
INSERT INTO iteminfo_data VALUES (366, 'price_logic_min_1995', '1228.8', 30);
INSERT INTO iteminfo_data VALUES (367, 'price_logic_max_1995', '3072', 30);
INSERT INTO iteminfo_data VALUES (368, 'price_fraud_1996', '883.5', 30);
INSERT INTO iteminfo_data VALUES (369, 'price_logic_min_1996', '1413.6', 30);
INSERT INTO iteminfo_data VALUES (370, 'price_logic_max_1996', '3534', 30);
INSERT INTO iteminfo_data VALUES (371, 'price_fraud_1997', '956.5', 30);
INSERT INTO iteminfo_data VALUES (372, 'price_logic_min_1997', '1530.4', 30);
INSERT INTO iteminfo_data VALUES (373, 'price_logic_max_1997', '3826', 30);
INSERT INTO iteminfo_data VALUES (374, 'price_fraud_1998', '1153.5', 30);
INSERT INTO iteminfo_data VALUES (375, 'price_logic_min_1998', '1845.6', 30);
INSERT INTO iteminfo_data VALUES (376, 'price_logic_max_1998', '4614', 30);
INSERT INTO iteminfo_data VALUES (377, 'price_fraud_1999', '1244.5', 30);
INSERT INTO iteminfo_data VALUES (378, 'price_logic_min_1999', '1991.2', 30);
INSERT INTO iteminfo_data VALUES (379, 'price_logic_max_1999', '4978', 30);
INSERT INTO iteminfo_data VALUES (380, 'price_fraud_2000', '1298.5', 30);
INSERT INTO iteminfo_data VALUES (381, 'price_logic_min_2000', '2077.6', 30);
INSERT INTO iteminfo_data VALUES (382, 'price_logic_max_2000', '5194', 30);
INSERT INTO iteminfo_data VALUES (383, 'price_fraud_2001', '1873.5', 30);
INSERT INTO iteminfo_data VALUES (384, 'price_logic_min_2001', '2997.6', 30);
INSERT INTO iteminfo_data VALUES (385, 'price_logic_max_2001', '7494', 30);
INSERT INTO iteminfo_data VALUES (386, 'price_fraud_2002', '1600', 30);
INSERT INTO iteminfo_data VALUES (387, 'price_logic_min_2002', '2560', 30);
INSERT INTO iteminfo_data VALUES (388, 'price_logic_max_2002', '6400', 30);
INSERT INTO iteminfo_data VALUES (389, 'price_fraud_2007', '1150', 30);
INSERT INTO iteminfo_data VALUES (390, 'price_logic_min_2007', '1840', 30);
INSERT INTO iteminfo_data VALUES (391, 'price_logic_max_2007', '4600', 30);
INSERT INTO iteminfo_data VALUES (392, 'price_fraud_1993', '300', 40);
INSERT INTO iteminfo_data VALUES (393, 'price_logic_min_1993', '480', 40);
INSERT INTO iteminfo_data VALUES (394, 'price_logic_max_1993', '1200', 40);
INSERT INTO iteminfo_data VALUES (395, 'price_fraud_1996', '10000', 40);
INSERT INTO iteminfo_data VALUES (396, 'price_logic_min_1996', '16000', 40);
INSERT INTO iteminfo_data VALUES (397, 'price_logic_max_1996', '40000', 40);
INSERT INTO iteminfo_data VALUES (398, 'price_fraud_2001', '1836', 40);
INSERT INTO iteminfo_data VALUES (399, 'price_logic_min_2001', '2937.6', 40);
INSERT INTO iteminfo_data VALUES (400, 'price_logic_max_2001', '7344', 40);
INSERT INTO iteminfo_data VALUES (401, 'price_fraud_2002', '2100', 40);
INSERT INTO iteminfo_data VALUES (402, 'price_logic_min_2002', '3360', 40);
INSERT INTO iteminfo_data VALUES (403, 'price_logic_max_2002', '8400', 40);
INSERT INTO iteminfo_data VALUES (404, 'price_fraud_2004', '3930', 41);
INSERT INTO iteminfo_data VALUES (405, 'price_logic_min_2004', '6288', 41);
INSERT INTO iteminfo_data VALUES (406, 'price_logic_max_2004', '15720', 41);
INSERT INTO iteminfo_data VALUES (407, 'price_fraud_2005', '4316.5', 41);
INSERT INTO iteminfo_data VALUES (408, 'price_logic_min_2005', '6906.4', 41);
INSERT INTO iteminfo_data VALUES (409, 'price_logic_max_2005', '17266', 41);
INSERT INTO iteminfo_data VALUES (410, 'price_fraud_2006', '4622.5', 41);
INSERT INTO iteminfo_data VALUES (411, 'price_logic_min_2006', '7396', 41);
INSERT INTO iteminfo_data VALUES (412, 'price_logic_max_2006', '18490', 41);
INSERT INTO iteminfo_data VALUES (413, 'price_fraud_2007', '5154.5', 41);
INSERT INTO iteminfo_data VALUES (414, 'price_logic_min_2007', '8247.2', 41);
INSERT INTO iteminfo_data VALUES (415, 'price_logic_max_2007', '20618', 41);
INSERT INTO iteminfo_data VALUES (416, 'price_fraud_2008', '6725', 41);
INSERT INTO iteminfo_data VALUES (417, 'price_logic_min_2008', '10760', 41);
INSERT INTO iteminfo_data VALUES (418, 'price_logic_max_2008', '26900', 41);
INSERT INTO iteminfo_data VALUES (419, 'price_fraud_1975', '100', 31);
INSERT INTO iteminfo_data VALUES (420, 'price_logic_min_1975', '160', 31);
INSERT INTO iteminfo_data VALUES (421, 'price_logic_max_1975', '400', 31);
INSERT INTO iteminfo_data VALUES (422, 'price_fraud_1976', '1150', 31);
INSERT INTO iteminfo_data VALUES (423, 'price_logic_min_1976', '1840', 31);
INSERT INTO iteminfo_data VALUES (424, 'price_logic_max_1976', '4600', 31);
INSERT INTO iteminfo_data VALUES (425, 'price_fraud_1977', '220', 31);
INSERT INTO iteminfo_data VALUES (426, 'price_logic_min_1977', '352', 31);
INSERT INTO iteminfo_data VALUES (427, 'price_logic_max_1977', '880', 31);
INSERT INTO iteminfo_data VALUES (428, 'price_fraud_1978', '375', 31);
INSERT INTO iteminfo_data VALUES (429, 'price_logic_min_1978', '600', 31);
INSERT INTO iteminfo_data VALUES (430, 'price_logic_max_1978', '1500', 31);
INSERT INTO iteminfo_data VALUES (431, 'price_fraud_1980', '225', 31);
INSERT INTO iteminfo_data VALUES (432, 'price_logic_min_1980', '360', 31);
INSERT INTO iteminfo_data VALUES (433, 'price_logic_max_1980', '900', 31);
INSERT INTO iteminfo_data VALUES (434, 'price_fraud_1981', '250', 31);
INSERT INTO iteminfo_data VALUES (435, 'price_logic_min_1981', '400', 31);
INSERT INTO iteminfo_data VALUES (436, 'price_logic_max_1981', '1000', 31);
INSERT INTO iteminfo_data VALUES (437, 'price_fraud_1982', '100', 31);
INSERT INTO iteminfo_data VALUES (438, 'price_logic_min_1982', '160', 31);
INSERT INTO iteminfo_data VALUES (439, 'price_logic_max_1982', '400', 31);
INSERT INTO iteminfo_data VALUES (440, 'price_fraud_1983', '166.5', 31);
INSERT INTO iteminfo_data VALUES (441, 'price_logic_min_1983', '266.4', 31);
INSERT INTO iteminfo_data VALUES (442, 'price_logic_max_1983', '666', 31);
INSERT INTO iteminfo_data VALUES (443, 'price_fraud_1984', '184', 31);
INSERT INTO iteminfo_data VALUES (444, 'price_logic_min_1984', '294.4', 31);
INSERT INTO iteminfo_data VALUES (445, 'price_logic_max_1984', '736', 31);
INSERT INTO iteminfo_data VALUES (446, 'price_fraud_1985', '191.5', 31);
INSERT INTO iteminfo_data VALUES (447, 'price_logic_min_1985', '306.4', 31);
INSERT INTO iteminfo_data VALUES (448, 'price_logic_max_1985', '766', 31);
INSERT INTO iteminfo_data VALUES (449, 'price_fraud_1986', '316.5', 31);
INSERT INTO iteminfo_data VALUES (450, 'price_logic_min_1986', '506.4', 31);
INSERT INTO iteminfo_data VALUES (451, 'price_logic_max_1986', '1266', 31);
INSERT INTO iteminfo_data VALUES (452, 'price_fraud_1987', '285', 31);
INSERT INTO iteminfo_data VALUES (453, 'price_logic_min_1987', '456', 31);
INSERT INTO iteminfo_data VALUES (454, 'price_logic_max_1987', '1140', 31);
INSERT INTO iteminfo_data VALUES (455, 'price_fraud_1988', '323', 31);
INSERT INTO iteminfo_data VALUES (456, 'price_logic_min_1988', '516.8', 31);
INSERT INTO iteminfo_data VALUES (457, 'price_logic_max_1988', '1292', 31);
INSERT INTO iteminfo_data VALUES (458, 'price_fraud_1989', '321.5', 31);
INSERT INTO iteminfo_data VALUES (459, 'price_logic_min_1989', '514.4', 31);
INSERT INTO iteminfo_data VALUES (460, 'price_logic_max_1989', '1286', 31);
INSERT INTO iteminfo_data VALUES (461, 'price_fraud_1990', '352.5', 31);
INSERT INTO iteminfo_data VALUES (462, 'price_logic_min_1990', '564', 31);
INSERT INTO iteminfo_data VALUES (463, 'price_logic_max_1990', '1410', 31);
INSERT INTO iteminfo_data VALUES (464, 'price_fraud_1991', '395', 31);
INSERT INTO iteminfo_data VALUES (465, 'price_logic_min_1991', '632', 31);
INSERT INTO iteminfo_data VALUES (466, 'price_logic_max_1991', '1580', 31);
INSERT INTO iteminfo_data VALUES (467, 'price_fraud_1992', '475', 31);
INSERT INTO iteminfo_data VALUES (468, 'price_logic_min_1992', '760', 31);
INSERT INTO iteminfo_data VALUES (469, 'price_logic_max_1992', '1900', 31);
INSERT INTO iteminfo_data VALUES (470, 'price_fraud_1993', '487', 31);
INSERT INTO iteminfo_data VALUES (471, 'price_logic_min_1993', '779.2', 31);
INSERT INTO iteminfo_data VALUES (472, 'price_logic_max_1993', '1948', 31);
INSERT INTO iteminfo_data VALUES (473, 'price_fraud_1994', '549.5', 31);
INSERT INTO iteminfo_data VALUES (474, 'price_logic_min_1994', '879.2', 31);
INSERT INTO iteminfo_data VALUES (475, 'price_logic_max_1994', '2198', 31);
INSERT INTO iteminfo_data VALUES (476, 'price_fraud_1995', '651.5', 31);
INSERT INTO iteminfo_data VALUES (477, 'price_logic_min_1995', '1042.4', 31);
INSERT INTO iteminfo_data VALUES (478, 'price_logic_max_1995', '2606', 31);
INSERT INTO iteminfo_data VALUES (479, 'price_fraud_1996', '829.5', 31);
INSERT INTO iteminfo_data VALUES (480, 'price_logic_min_1996', '1327.2', 31);
INSERT INTO iteminfo_data VALUES (481, 'price_logic_max_1996', '3318', 31);
INSERT INTO iteminfo_data VALUES (482, 'price_fraud_1997', '1048.5', 31);
INSERT INTO iteminfo_data VALUES (483, 'price_logic_min_1997', '1677.6', 31);
INSERT INTO iteminfo_data VALUES (484, 'price_logic_max_1997', '4194', 31);
INSERT INTO iteminfo_data VALUES (485, 'price_fraud_1998', '1168', 31);
INSERT INTO iteminfo_data VALUES (486, 'price_logic_min_1998', '1868.8', 31);
INSERT INTO iteminfo_data VALUES (487, 'price_logic_max_1998', '4672', 31);
INSERT INTO iteminfo_data VALUES (488, 'price_fraud_1999', '1340', 31);
INSERT INTO iteminfo_data VALUES (489, 'price_logic_min_1999', '2144', 31);
INSERT INTO iteminfo_data VALUES (490, 'price_logic_max_1999', '5360', 31);
INSERT INTO iteminfo_data VALUES (491, 'price_fraud_2000', '1591', 31);
INSERT INTO iteminfo_data VALUES (492, 'price_logic_min_2000', '2545.6', 31);
INSERT INTO iteminfo_data VALUES (493, 'price_logic_max_2000', '6364', 31);
INSERT INTO iteminfo_data VALUES (494, 'price_fraud_2001', '1940.5', 31);
INSERT INTO iteminfo_data VALUES (495, 'price_logic_min_2001', '3104.8', 31);
INSERT INTO iteminfo_data VALUES (496, 'price_logic_max_2001', '7762', 31);
INSERT INTO iteminfo_data VALUES (497, 'price_fraud_2002', '2489', 31);
INSERT INTO iteminfo_data VALUES (498, 'price_logic_min_2002', '3982.4', 31);
INSERT INTO iteminfo_data VALUES (499, 'price_logic_max_2002', '9956', 31);
INSERT INTO iteminfo_data VALUES (500, 'price_fraud_2003', '3363', 31);
INSERT INTO iteminfo_data VALUES (501, 'price_logic_min_2003', '5380.8', 31);
INSERT INTO iteminfo_data VALUES (502, 'price_logic_max_2003', '13452', 31);
INSERT INTO iteminfo_data VALUES (503, 'price_fraud_2004', '3318.5', 31);
INSERT INTO iteminfo_data VALUES (504, 'price_logic_min_2004', '5309.6', 31);
INSERT INTO iteminfo_data VALUES (505, 'price_logic_max_2004', '13274', 31);
INSERT INTO iteminfo_data VALUES (506, 'price_fraud_2005', '3736', 31);
INSERT INTO iteminfo_data VALUES (507, 'price_logic_min_2005', '5977.6', 31);
INSERT INTO iteminfo_data VALUES (508, 'price_logic_max_2005', '14944', 31);
INSERT INTO iteminfo_data VALUES (509, 'price_fraud_2006', '4460', 31);
INSERT INTO iteminfo_data VALUES (510, 'price_logic_min_2006', '7136', 31);
INSERT INTO iteminfo_data VALUES (511, 'price_logic_max_2006', '17840', 31);
INSERT INTO iteminfo_data VALUES (512, 'price_fraud_2007', '5120.5', 31);
INSERT INTO iteminfo_data VALUES (513, 'price_logic_min_2007', '8192.8', 31);
INSERT INTO iteminfo_data VALUES (514, 'price_logic_max_2007', '20482', 31);
INSERT INTO iteminfo_data VALUES (515, 'price_fraud_2008', '5896.5', 31);
INSERT INTO iteminfo_data VALUES (516, 'price_logic_min_2008', '9434.4', 31);
INSERT INTO iteminfo_data VALUES (517, 'price_logic_max_2008', '23586', 31);
INSERT INTO iteminfo_data VALUES (518, 'price_fraud_1975', '4529.5', 27);
INSERT INTO iteminfo_data VALUES (519, 'price_logic_min_1975', '7247.2', 27);
INSERT INTO iteminfo_data VALUES (520, 'price_logic_max_1975', '18118', 27);
INSERT INTO iteminfo_data VALUES (521, 'price_fraud_1976', '1860', 27);
INSERT INTO iteminfo_data VALUES (522, 'price_logic_min_1976', '2976', 27);
INSERT INTO iteminfo_data VALUES (523, 'price_logic_max_1976', '7440', 27);
INSERT INTO iteminfo_data VALUES (524, 'price_fraud_1977', '1113.5', 27);
INSERT INTO iteminfo_data VALUES (525, 'price_logic_min_1977', '1781.6', 27);
INSERT INTO iteminfo_data VALUES (526, 'price_logic_max_1977', '4454', 27);
INSERT INTO iteminfo_data VALUES (527, 'price_fraud_1978', '699', 27);
INSERT INTO iteminfo_data VALUES (528, 'price_logic_min_1978', '1118.4', 27);
INSERT INTO iteminfo_data VALUES (529, 'price_logic_max_1978', '2796', 27);
INSERT INTO iteminfo_data VALUES (530, 'price_fraud_1979', '1120.5', 27);
INSERT INTO iteminfo_data VALUES (531, 'price_logic_min_1979', '1792.8', 27);
INSERT INTO iteminfo_data VALUES (532, 'price_logic_max_1979', '4482', 27);
INSERT INTO iteminfo_data VALUES (533, 'price_fraud_1980', '1116.5', 27);
INSERT INTO iteminfo_data VALUES (534, 'price_logic_min_1980', '1786.4', 27);
INSERT INTO iteminfo_data VALUES (535, 'price_logic_max_1980', '4466', 27);
INSERT INTO iteminfo_data VALUES (536, 'price_fraud_1981', '956.5', 27);
INSERT INTO iteminfo_data VALUES (537, 'price_logic_min_1981', '1530.4', 27);
INSERT INTO iteminfo_data VALUES (538, 'price_logic_max_1981', '3826', 27);
INSERT INTO iteminfo_data VALUES (539, 'price_fraud_1982', '561.5', 27);
INSERT INTO iteminfo_data VALUES (540, 'price_logic_min_1982', '898.4', 27);
INSERT INTO iteminfo_data VALUES (541, 'price_logic_max_1982', '2246', 27);
INSERT INTO iteminfo_data VALUES (542, 'price_fraud_1983', '526', 27);
INSERT INTO iteminfo_data VALUES (543, 'price_logic_min_1983', '841.6', 27);
INSERT INTO iteminfo_data VALUES (544, 'price_logic_max_1983', '2104', 27);
INSERT INTO iteminfo_data VALUES (545, 'price_fraud_1984', '483.5', 27);
INSERT INTO iteminfo_data VALUES (546, 'price_logic_min_1984', '773.6', 27);
INSERT INTO iteminfo_data VALUES (547, 'price_logic_max_1984', '1934', 27);
INSERT INTO iteminfo_data VALUES (548, 'price_fraud_1985', '354', 27);
INSERT INTO iteminfo_data VALUES (549, 'price_logic_min_1985', '566.4', 27);
INSERT INTO iteminfo_data VALUES (550, 'price_logic_max_1985', '1416', 27);
INSERT INTO iteminfo_data VALUES (551, 'price_fraud_1986', '473', 27);
INSERT INTO iteminfo_data VALUES (552, 'price_logic_min_1986', '756.8', 27);
INSERT INTO iteminfo_data VALUES (553, 'price_logic_max_1986', '1892', 27);
INSERT INTO iteminfo_data VALUES (554, 'price_fraud_1987', '402.5', 27);
INSERT INTO iteminfo_data VALUES (555, 'price_logic_min_1987', '644', 27);
INSERT INTO iteminfo_data VALUES (556, 'price_logic_max_1987', '1610', 27);
INSERT INTO iteminfo_data VALUES (557, 'price_fraud_1988', '430', 27);
INSERT INTO iteminfo_data VALUES (558, 'price_logic_min_1988', '688', 27);
INSERT INTO iteminfo_data VALUES (559, 'price_logic_max_1988', '1720', 27);
INSERT INTO iteminfo_data VALUES (560, 'price_fraud_1989', '410', 27);
INSERT INTO iteminfo_data VALUES (561, 'price_logic_min_1989', '656', 27);
INSERT INTO iteminfo_data VALUES (562, 'price_logic_max_1989', '1640', 27);
INSERT INTO iteminfo_data VALUES (563, 'price_fraud_1990', '454.5', 27);
INSERT INTO iteminfo_data VALUES (564, 'price_logic_min_1990', '727.2', 27);
INSERT INTO iteminfo_data VALUES (565, 'price_logic_max_1990', '1818', 27);
INSERT INTO iteminfo_data VALUES (566, 'price_fraud_1991', '442.5', 27);
INSERT INTO iteminfo_data VALUES (567, 'price_logic_min_1991', '708', 27);
INSERT INTO iteminfo_data VALUES (568, 'price_logic_max_1991', '1770', 27);
INSERT INTO iteminfo_data VALUES (569, 'price_fraud_1992', '541', 27);
INSERT INTO iteminfo_data VALUES (570, 'price_logic_min_1992', '865.6', 27);
INSERT INTO iteminfo_data VALUES (571, 'price_logic_max_1992', '2164', 27);
INSERT INTO iteminfo_data VALUES (572, 'price_fraud_1993', '645', 27);
INSERT INTO iteminfo_data VALUES (573, 'price_logic_min_1993', '1032', 27);
INSERT INTO iteminfo_data VALUES (574, 'price_logic_max_1993', '2580', 27);
INSERT INTO iteminfo_data VALUES (575, 'price_fraud_1994', '710.5', 27);
INSERT INTO iteminfo_data VALUES (576, 'price_logic_min_1994', '1136.8', 27);
INSERT INTO iteminfo_data VALUES (577, 'price_logic_max_1994', '2842', 27);
INSERT INTO iteminfo_data VALUES (578, 'price_fraud_1995', '869', 27);
INSERT INTO iteminfo_data VALUES (579, 'price_logic_min_1995', '1390.4', 27);
INSERT INTO iteminfo_data VALUES (580, 'price_logic_max_1995', '3476', 27);
INSERT INTO iteminfo_data VALUES (581, 'price_fraud_1996', '1012.5', 27);
INSERT INTO iteminfo_data VALUES (582, 'price_logic_min_1996', '1620', 27);
INSERT INTO iteminfo_data VALUES (583, 'price_logic_max_1996', '4050', 27);
INSERT INTO iteminfo_data VALUES (584, 'price_fraud_1997', '1226.5', 27);
INSERT INTO iteminfo_data VALUES (585, 'price_logic_min_1997', '1962.4', 27);
INSERT INTO iteminfo_data VALUES (586, 'price_logic_max_1997', '4906', 27);
INSERT INTO iteminfo_data VALUES (587, 'price_fraud_1998', '1405', 27);
INSERT INTO iteminfo_data VALUES (588, 'price_logic_min_1998', '2248', 27);
INSERT INTO iteminfo_data VALUES (589, 'price_logic_max_1998', '5620', 27);
INSERT INTO iteminfo_data VALUES (590, 'price_fraud_1999', '1682.5', 27);
INSERT INTO iteminfo_data VALUES (591, 'price_logic_min_1999', '2692', 27);
INSERT INTO iteminfo_data VALUES (592, 'price_logic_max_1999', '6730', 27);
INSERT INTO iteminfo_data VALUES (593, 'price_fraud_2000', '2113.5', 27);
INSERT INTO iteminfo_data VALUES (594, 'price_logic_min_2000', '3381.6', 27);
INSERT INTO iteminfo_data VALUES (595, 'price_logic_max_2000', '8454', 27);
INSERT INTO iteminfo_data VALUES (596, 'price_fraud_2001', '2815.5', 27);
INSERT INTO iteminfo_data VALUES (597, 'price_logic_min_2001', '4504.8', 27);
INSERT INTO iteminfo_data VALUES (598, 'price_logic_max_2001', '11262', 27);
INSERT INTO iteminfo_data VALUES (599, 'price_fraud_2002', '3258', 27);
INSERT INTO iteminfo_data VALUES (600, 'price_logic_min_2002', '5212.8', 27);
INSERT INTO iteminfo_data VALUES (601, 'price_logic_max_2002', '13032', 27);
INSERT INTO iteminfo_data VALUES (602, 'price_fraud_2003', '3888', 27);
INSERT INTO iteminfo_data VALUES (603, 'price_logic_min_2003', '6220.8', 27);
INSERT INTO iteminfo_data VALUES (604, 'price_logic_max_2003', '15552', 27);
INSERT INTO iteminfo_data VALUES (605, 'price_fraud_2004', '4701', 27);
INSERT INTO iteminfo_data VALUES (606, 'price_logic_min_2004', '7521.6', 27);
INSERT INTO iteminfo_data VALUES (607, 'price_logic_max_2004', '18804', 27);
INSERT INTO iteminfo_data VALUES (608, 'price_fraud_2005', '5431', 27);
INSERT INTO iteminfo_data VALUES (609, 'price_logic_min_2005', '8689.6', 27);
INSERT INTO iteminfo_data VALUES (610, 'price_logic_max_2005', '21724', 27);
INSERT INTO iteminfo_data VALUES (611, 'price_fraud_2006', '6420', 27);
INSERT INTO iteminfo_data VALUES (612, 'price_logic_min_2006', '10272', 27);
INSERT INTO iteminfo_data VALUES (613, 'price_logic_max_2006', '25680', 27);
INSERT INTO iteminfo_data VALUES (614, 'price_fraud_2007', '8082', 27);
INSERT INTO iteminfo_data VALUES (615, 'price_logic_min_2007', '12931.2', 27);
INSERT INTO iteminfo_data VALUES (616, 'price_logic_max_2007', '32328', 27);
INSERT INTO iteminfo_data VALUES (617, 'price_fraud_2008', '9657', 27);
INSERT INTO iteminfo_data VALUES (618, 'price_logic_min_2008', '15451.2', 27);
INSERT INTO iteminfo_data VALUES (619, 'price_logic_max_2008', '38628', 27);
INSERT INTO iteminfo_data VALUES (620, 'price_fraud_1998', '1700', 42);
INSERT INTO iteminfo_data VALUES (621, 'price_logic_min_1998', '2720', 42);
INSERT INTO iteminfo_data VALUES (622, 'price_logic_max_1998', '6800', 42);
INSERT INTO iteminfo_data VALUES (623, 'price_fraud_1999', '1550', 42);
INSERT INTO iteminfo_data VALUES (624, 'price_logic_min_1999', '2480', 42);
INSERT INTO iteminfo_data VALUES (625, 'price_logic_max_1999', '6200', 42);
INSERT INTO iteminfo_data VALUES (626, 'price_fraud_2001', '3800', 42);
INSERT INTO iteminfo_data VALUES (627, 'price_logic_min_2001', '6080', 42);
INSERT INTO iteminfo_data VALUES (628, 'price_logic_max_2001', '15200', 42);
INSERT INTO iteminfo_data VALUES (629, 'price_fraud_2002', '2972.5', 42);
INSERT INTO iteminfo_data VALUES (630, 'price_logic_min_2002', '4756', 42);
INSERT INTO iteminfo_data VALUES (631, 'price_logic_max_2002', '11890', 42);
INSERT INTO iteminfo_data VALUES (632, 'price_fraud_1996', '1250', 43);
INSERT INTO iteminfo_data VALUES (633, 'price_logic_min_1996', '2000', 43);
INSERT INTO iteminfo_data VALUES (634, 'price_logic_max_1996', '5000', 43);
INSERT INTO iteminfo_data VALUES (635, 'price_fraud_1998', '850', 43);
INSERT INTO iteminfo_data VALUES (636, 'price_logic_min_1998', '1360', 43);
INSERT INTO iteminfo_data VALUES (637, 'price_logic_max_1998', '3400', 43);
INSERT INTO iteminfo_data VALUES (638, 'price_fraud_1999', '1575', 43);
INSERT INTO iteminfo_data VALUES (639, 'price_logic_min_1999', '2520', 43);
INSERT INTO iteminfo_data VALUES (640, 'price_logic_max_1999', '6300', 43);
INSERT INTO iteminfo_data VALUES (641, 'price_fraud_2000', '1100', 43);
INSERT INTO iteminfo_data VALUES (642, 'price_logic_min_2000', '1760', 43);
INSERT INTO iteminfo_data VALUES (643, 'price_logic_max_2000', '4400', 43);
INSERT INTO iteminfo_data VALUES (644, 'price_fraud_1994', '831', 44);
INSERT INTO iteminfo_data VALUES (645, 'price_logic_min_1994', '1329.6', 44);
INSERT INTO iteminfo_data VALUES (646, 'price_logic_max_1994', '3324', 44);
INSERT INTO iteminfo_data VALUES (647, 'price_fraud_1995', '775', 44);
INSERT INTO iteminfo_data VALUES (648, 'price_logic_min_1995', '1240', 44);
INSERT INTO iteminfo_data VALUES (649, 'price_logic_max_1995', '3100', 44);
INSERT INTO iteminfo_data VALUES (650, 'price_fraud_1996', '1387.5', 44);
INSERT INTO iteminfo_data VALUES (651, 'price_logic_min_1996', '2220', 44);
INSERT INTO iteminfo_data VALUES (652, 'price_logic_max_1996', '5550', 44);
INSERT INTO iteminfo_data VALUES (653, 'price_fraud_1997', '1400', 44);
INSERT INTO iteminfo_data VALUES (654, 'price_logic_min_1997', '2240', 44);
INSERT INTO iteminfo_data VALUES (655, 'price_logic_max_1997', '5600', 44);
INSERT INTO iteminfo_data VALUES (656, 'price_fraud_1998', '1852.5', 45);
INSERT INTO iteminfo_data VALUES (657, 'price_logic_min_1998', '2964', 45);
INSERT INTO iteminfo_data VALUES (658, 'price_logic_max_1998', '7410', 45);
INSERT INTO iteminfo_data VALUES (659, 'price_fraud_1999', '1749.5', 45);
INSERT INTO iteminfo_data VALUES (660, 'price_logic_min_1999', '2799.2', 45);
INSERT INTO iteminfo_data VALUES (661, 'price_logic_max_1999', '6998', 45);
INSERT INTO iteminfo_data VALUES (662, 'price_fraud_2000', '2148.5', 45);
INSERT INTO iteminfo_data VALUES (663, 'price_logic_min_2000', '3437.6', 45);
INSERT INTO iteminfo_data VALUES (664, 'price_logic_max_2000', '8594', 45);
INSERT INTO iteminfo_data VALUES (665, 'price_fraud_2001', '2000', 45);
INSERT INTO iteminfo_data VALUES (666, 'price_logic_min_2001', '3200', 45);
INSERT INTO iteminfo_data VALUES (667, 'price_logic_max_2001', '8000', 45);
INSERT INTO iteminfo_data VALUES (668, 'price_fraud_1995', '1625', 46);
INSERT INTO iteminfo_data VALUES (669, 'price_logic_min_1995', '2600', 46);
INSERT INTO iteminfo_data VALUES (670, 'price_logic_max_1995', '6500', 46);
INSERT INTO iteminfo_data VALUES (671, 'price_fraud_1996', '1440', 46);
INSERT INTO iteminfo_data VALUES (672, 'price_logic_min_1996', '2304', 46);
INSERT INTO iteminfo_data VALUES (673, 'price_logic_max_1996', '5760', 46);
INSERT INTO iteminfo_data VALUES (674, 'price_fraud_1997', '1275', 46);
INSERT INTO iteminfo_data VALUES (675, 'price_logic_min_1997', '2040', 46);
INSERT INTO iteminfo_data VALUES (676, 'price_logic_max_1997', '5100', 46);
INSERT INTO iteminfo_data VALUES (677, 'price_fraud_1998', '1400', 46);
INSERT INTO iteminfo_data VALUES (678, 'price_logic_min_1998', '2240', 46);
INSERT INTO iteminfo_data VALUES (679, 'price_logic_max_1998', '5600', 46);
INSERT INTO iteminfo_data VALUES (680, 'price_fraud_2004', '4250', 46);
INSERT INTO iteminfo_data VALUES (681, 'price_logic_min_2004', '6800', 46);
INSERT INTO iteminfo_data VALUES (682, 'price_logic_max_2004', '17000', 46);
INSERT INTO iteminfo_data VALUES (683, 'price_fraud_1995', '1441.5', 47);
INSERT INTO iteminfo_data VALUES (684, 'price_logic_min_1995', '2306.4', 47);
INSERT INTO iteminfo_data VALUES (685, 'price_logic_max_1995', '5766', 47);
INSERT INTO iteminfo_data VALUES (686, 'price_fraud_1996', '1229', 47);
INSERT INTO iteminfo_data VALUES (687, 'price_logic_min_1996', '1966.4', 47);
INSERT INTO iteminfo_data VALUES (688, 'price_logic_max_1996', '4916', 47);
INSERT INTO iteminfo_data VALUES (689, 'price_fraud_1997', '1750', 47);
INSERT INTO iteminfo_data VALUES (690, 'price_logic_min_1997', '2800', 47);
INSERT INTO iteminfo_data VALUES (691, 'price_logic_max_1997', '7000', 47);
INSERT INTO iteminfo_data VALUES (692, 'price_fraud_1999', '2325', 47);
INSERT INTO iteminfo_data VALUES (693, 'price_logic_min_1999', '3720', 47);
INSERT INTO iteminfo_data VALUES (694, 'price_logic_max_1999', '9300', 47);
INSERT INTO iteminfo_data VALUES (695, 'price_fraud_2000', '1600', 47);
INSERT INTO iteminfo_data VALUES (696, 'price_logic_min_2000', '2560', 47);
INSERT INTO iteminfo_data VALUES (697, 'price_logic_max_2000', '6400', 47);
INSERT INTO iteminfo_data VALUES (698, 'price_fraud_2002', '3325', 48);
INSERT INTO iteminfo_data VALUES (699, 'price_logic_min_2002', '5320', 48);
INSERT INTO iteminfo_data VALUES (700, 'price_logic_max_2002', '13300', 48);
INSERT INTO iteminfo_data VALUES (701, 'price_fraud_2003', '4100', 48);
INSERT INTO iteminfo_data VALUES (702, 'price_logic_min_2003', '6560', 48);
INSERT INTO iteminfo_data VALUES (703, 'price_logic_max_2003', '16400', 48);
INSERT INTO iteminfo_data VALUES (704, 'price_fraud_2004', '3625', 48);
INSERT INTO iteminfo_data VALUES (705, 'price_logic_min_2004', '5800', 48);
INSERT INTO iteminfo_data VALUES (706, 'price_logic_max_2004', '14500', 48);
INSERT INTO iteminfo_data VALUES (707, 'price_fraud_2005', '4800', 48);
INSERT INTO iteminfo_data VALUES (708, 'price_logic_min_2005', '7680', 48);
INSERT INTO iteminfo_data VALUES (709, 'price_logic_max_2005', '19200', 48);
INSERT INTO iteminfo_data VALUES (710, 'price_fraud_2006', '5000', 48);
INSERT INTO iteminfo_data VALUES (711, 'price_logic_min_2006', '8000', 48);
INSERT INTO iteminfo_data VALUES (712, 'price_logic_max_2006', '20000', 48);
INSERT INTO iteminfo_data VALUES (713, 'price_fraud_2003', '5175', 49);
INSERT INTO iteminfo_data VALUES (714, 'price_logic_min_2003', '8280', 49);
INSERT INTO iteminfo_data VALUES (715, 'price_logic_max_2003', '20700', 49);
INSERT INTO iteminfo_data VALUES (716, 'price_fraud_2004', '5475', 49);
INSERT INTO iteminfo_data VALUES (717, 'price_logic_min_2004', '8760', 49);
INSERT INTO iteminfo_data VALUES (718, 'price_logic_max_2004', '21900', 49);
INSERT INTO iteminfo_data VALUES (719, 'price_fraud_2005', '6467.5', 49);
INSERT INTO iteminfo_data VALUES (720, 'price_logic_min_2005', '10348', 49);
INSERT INTO iteminfo_data VALUES (721, 'price_logic_max_2005', '25870', 49);
INSERT INTO iteminfo_data VALUES (722, 'price_fraud_2006', '8950', 49);
INSERT INTO iteminfo_data VALUES (723, 'price_logic_min_2006', '14320', 49);
INSERT INTO iteminfo_data VALUES (724, 'price_logic_max_2006', '35800', 49);
INSERT INTO iteminfo_data VALUES (725, 'price_fraud_2001', '3400', 50);
INSERT INTO iteminfo_data VALUES (726, 'price_logic_min_2001', '5440', 50);
INSERT INTO iteminfo_data VALUES (727, 'price_logic_max_2001', '13600', 50);
INSERT INTO iteminfo_data VALUES (728, 'price_fraud_2002', '3445', 50);
INSERT INTO iteminfo_data VALUES (729, 'price_logic_min_2002', '5512', 50);
INSERT INTO iteminfo_data VALUES (730, 'price_logic_max_2002', '13780', 50);
INSERT INTO iteminfo_data VALUES (731, 'price_fraud_2004', '4950', 50);
INSERT INTO iteminfo_data VALUES (732, 'price_logic_min_2004', '7920', 50);
INSERT INTO iteminfo_data VALUES (733, 'price_logic_max_2004', '19800', 50);
INSERT INTO iteminfo_data VALUES (734, 'price_fraud_2005', '4950', 50);
INSERT INTO iteminfo_data VALUES (735, 'price_logic_min_2005', '7920', 50);
INSERT INTO iteminfo_data VALUES (736, 'price_logic_max_2005', '19800', 50);
INSERT INTO iteminfo_data VALUES (737, 'price_fraud_1984', '1750', 32);
INSERT INTO iteminfo_data VALUES (738, 'price_logic_min_1984', '2800', 32);
INSERT INTO iteminfo_data VALUES (739, 'price_logic_max_1984', '7000', 32);
INSERT INTO iteminfo_data VALUES (740, 'price_fraud_1988', '1750', 32);
INSERT INTO iteminfo_data VALUES (741, 'price_logic_min_1988', '2800', 32);
INSERT INTO iteminfo_data VALUES (742, 'price_logic_max_1988', '7000', 32);
INSERT INTO iteminfo_data VALUES (743, 'price_fraud_1989', '1650', 32);
INSERT INTO iteminfo_data VALUES (744, 'price_logic_min_1989', '2640', 32);
INSERT INTO iteminfo_data VALUES (745, 'price_logic_max_1989', '6600', 32);
INSERT INTO iteminfo_data VALUES (746, 'price_fraud_1991', '2733', 32);
INSERT INTO iteminfo_data VALUES (747, 'price_logic_min_1991', '4372.8', 32);
INSERT INTO iteminfo_data VALUES (748, 'price_logic_max_1991', '10932', 32);
INSERT INTO iteminfo_data VALUES (749, 'price_fraud_1992', '1000', 32);
INSERT INTO iteminfo_data VALUES (750, 'price_logic_min_1992', '1600', 32);
INSERT INTO iteminfo_data VALUES (751, 'price_logic_max_1992', '4000', 32);
INSERT INTO iteminfo_data VALUES (752, 'price_fraud_1993', '1625', 32);
INSERT INTO iteminfo_data VALUES (753, 'price_logic_min_1993', '2600', 32);
INSERT INTO iteminfo_data VALUES (754, 'price_logic_max_1993', '6500', 32);
INSERT INTO iteminfo_data VALUES (755, 'price_fraud_1994', '828', 32);
INSERT INTO iteminfo_data VALUES (756, 'price_logic_min_1994', '1324.8', 32);
INSERT INTO iteminfo_data VALUES (757, 'price_logic_max_1994', '3312', 32);
INSERT INTO iteminfo_data VALUES (758, 'price_fraud_1995', '1057.5', 32);
INSERT INTO iteminfo_data VALUES (759, 'price_logic_min_1995', '1692', 32);
INSERT INTO iteminfo_data VALUES (760, 'price_logic_max_1995', '4230', 32);
INSERT INTO iteminfo_data VALUES (761, 'price_fraud_1996', '1136.5', 32);
INSERT INTO iteminfo_data VALUES (762, 'price_logic_min_1996', '1818.4', 32);
INSERT INTO iteminfo_data VALUES (763, 'price_logic_max_1996', '4546', 32);
INSERT INTO iteminfo_data VALUES (764, 'price_fraud_1997', '1295.5', 32);
INSERT INTO iteminfo_data VALUES (765, 'price_logic_min_1997', '2072.8', 32);
INSERT INTO iteminfo_data VALUES (766, 'price_logic_max_1997', '5182', 32);
INSERT INTO iteminfo_data VALUES (767, 'price_fraud_1998', '1616', 32);
INSERT INTO iteminfo_data VALUES (768, 'price_logic_min_1998', '2585.6', 32);
INSERT INTO iteminfo_data VALUES (769, 'price_logic_max_1998', '6464', 32);
INSERT INTO iteminfo_data VALUES (770, 'price_fraud_1999', '1857.5', 32);
INSERT INTO iteminfo_data VALUES (771, 'price_logic_min_1999', '2972', 32);
INSERT INTO iteminfo_data VALUES (772, 'price_logic_max_1999', '7430', 32);
INSERT INTO iteminfo_data VALUES (773, 'price_fraud_2000', '2170', 32);
INSERT INTO iteminfo_data VALUES (774, 'price_logic_min_2000', '3472', 32);
INSERT INTO iteminfo_data VALUES (775, 'price_logic_max_2000', '8680', 32);
INSERT INTO iteminfo_data VALUES (776, 'price_fraud_2001', '3146', 32);
INSERT INTO iteminfo_data VALUES (777, 'price_logic_min_2001', '5033.6', 32);
INSERT INTO iteminfo_data VALUES (778, 'price_logic_max_2001', '12584', 32);
INSERT INTO iteminfo_data VALUES (779, 'price_fraud_2002', '3565.5', 32);
INSERT INTO iteminfo_data VALUES (780, 'price_logic_min_2002', '5704.8', 32);
INSERT INTO iteminfo_data VALUES (781, 'price_logic_max_2002', '14262', 32);
INSERT INTO iteminfo_data VALUES (782, 'price_fraud_2003', '4029', 32);
INSERT INTO iteminfo_data VALUES (783, 'price_logic_min_2003', '6446.4', 32);
INSERT INTO iteminfo_data VALUES (784, 'price_logic_max_2003', '16116', 32);
INSERT INTO iteminfo_data VALUES (785, 'price_fraud_2004', '4702.5', 32);
INSERT INTO iteminfo_data VALUES (786, 'price_logic_min_2004', '7524', 32);
INSERT INTO iteminfo_data VALUES (787, 'price_logic_max_2004', '18810', 32);
INSERT INTO iteminfo_data VALUES (788, 'price_fraud_2005', '5904.5', 32);
INSERT INTO iteminfo_data VALUES (789, 'price_logic_min_2005', '9447.2', 32);
INSERT INTO iteminfo_data VALUES (790, 'price_logic_max_2005', '23618', 32);
INSERT INTO iteminfo_data VALUES (791, 'price_fraud_2006', '7192.5', 32);
INSERT INTO iteminfo_data VALUES (792, 'price_logic_min_2006', '11508', 32);
INSERT INTO iteminfo_data VALUES (793, 'price_logic_max_2006', '28770', 32);
INSERT INTO iteminfo_data VALUES (794, 'price_fraud_2007', '9243', 32);
INSERT INTO iteminfo_data VALUES (795, 'price_logic_min_2007', '14788.8', 32);
INSERT INTO iteminfo_data VALUES (796, 'price_logic_max_2007', '36972', 32);
INSERT INTO iteminfo_data VALUES (797, 'price_fraud_2008', '11569.5', 32);
INSERT INTO iteminfo_data VALUES (798, 'price_logic_min_2008', '18511.2', 32);
INSERT INTO iteminfo_data VALUES (799, 'price_logic_max_2008', '46278', 32);
INSERT INTO iteminfo_data VALUES (800, 'price_fraud_2000', '3866.5', 57);
INSERT INTO iteminfo_data VALUES (801, 'price_logic_min_2000', '6186.4', 57);
INSERT INTO iteminfo_data VALUES (802, 'price_logic_max_2000', '15466', 57);
INSERT INTO iteminfo_data VALUES (803, 'price_fraud_2001', '3982', 57);
INSERT INTO iteminfo_data VALUES (804, 'price_logic_min_2001', '6371.2', 57);
INSERT INTO iteminfo_data VALUES (805, 'price_logic_max_2001', '15928', 57);
INSERT INTO iteminfo_data VALUES (806, 'price_fraud_2002', '4284', 57);
INSERT INTO iteminfo_data VALUES (807, 'price_logic_min_2002', '6854.4', 57);
INSERT INTO iteminfo_data VALUES (808, 'price_logic_max_2002', '17136', 57);
INSERT INTO iteminfo_data VALUES (809, 'price_fraud_2003', '6075', 57);
INSERT INTO iteminfo_data VALUES (810, 'price_logic_min_2003', '9720', 57);
INSERT INTO iteminfo_data VALUES (811, 'price_logic_max_2003', '24300', 57);
INSERT INTO iteminfo_data VALUES (812, 'price_fraud_2007', '7950', 57);
INSERT INTO iteminfo_data VALUES (813, 'price_logic_min_2007', '12720', 57);
INSERT INTO iteminfo_data VALUES (814, 'price_logic_max_2007', '31800', 57);
INSERT INTO iteminfo_data VALUES (815, 'price_fraud_1997', '1533', 58);
INSERT INTO iteminfo_data VALUES (816, 'price_logic_min_1997', '2452.8', 58);
INSERT INTO iteminfo_data VALUES (817, 'price_logic_max_1997', '6132', 58);
INSERT INTO iteminfo_data VALUES (818, 'price_fraud_1998', '2033', 58);
INSERT INTO iteminfo_data VALUES (819, 'price_logic_min_1998', '3252.8', 58);
INSERT INTO iteminfo_data VALUES (820, 'price_logic_max_1998', '8132', 58);
INSERT INTO iteminfo_data VALUES (821, 'price_fraud_1999', '2248', 58);
INSERT INTO iteminfo_data VALUES (822, 'price_logic_min_1999', '3596.8', 58);
INSERT INTO iteminfo_data VALUES (823, 'price_logic_max_1999', '8992', 58);
INSERT INTO iteminfo_data VALUES (824, 'price_fraud_2000', '2313', 58);
INSERT INTO iteminfo_data VALUES (825, 'price_logic_min_2000', '3700.8', 58);
INSERT INTO iteminfo_data VALUES (826, 'price_logic_max_2000', '9252', 58);
INSERT INTO iteminfo_data VALUES (827, 'price_fraud_2001', '2300', 58);
INSERT INTO iteminfo_data VALUES (828, 'price_logic_min_2001', '3680', 58);
INSERT INTO iteminfo_data VALUES (829, 'price_logic_max_2001', '9200', 58);
INSERT INTO iteminfo_data VALUES (830, 'price_fraud_1997', '1816.5', 59);
INSERT INTO iteminfo_data VALUES (831, 'price_logic_min_1997', '2906.4', 59);
INSERT INTO iteminfo_data VALUES (832, 'price_logic_max_1997', '7266', 59);
INSERT INTO iteminfo_data VALUES (833, 'price_fraud_1998', '2100', 59);
INSERT INTO iteminfo_data VALUES (834, 'price_logic_min_1998', '3360', 59);
INSERT INTO iteminfo_data VALUES (835, 'price_logic_max_1998', '8400', 59);
INSERT INTO iteminfo_data VALUES (836, 'price_fraud_2001', '3450', 59);
INSERT INTO iteminfo_data VALUES (837, 'price_logic_min_2001', '5520', 59);
INSERT INTO iteminfo_data VALUES (838, 'price_logic_max_2001', '13800', 59);
INSERT INTO iteminfo_data VALUES (839, 'price_fraud_2007', '9125', 59);
INSERT INTO iteminfo_data VALUES (840, 'price_logic_min_2007', '14600', 59);
INSERT INTO iteminfo_data VALUES (841, 'price_logic_max_2007', '36500', 59);
INSERT INTO iteminfo_data VALUES (842, 'price_fraud_1988', '1500', 34);
INSERT INTO iteminfo_data VALUES (843, 'price_logic_min_1988', '2400', 34);
INSERT INTO iteminfo_data VALUES (844, 'price_logic_max_1988', '6000', 34);
INSERT INTO iteminfo_data VALUES (845, 'price_fraud_1994', '5000', 34);
INSERT INTO iteminfo_data VALUES (846, 'price_logic_min_1994', '8000', 34);
INSERT INTO iteminfo_data VALUES (847, 'price_logic_max_1994', '20000', 34);
INSERT INTO iteminfo_data VALUES (848, 'price_fraud_1995', '1600', 34);
INSERT INTO iteminfo_data VALUES (849, 'price_logic_min_1995', '2560', 34);
INSERT INTO iteminfo_data VALUES (850, 'price_logic_max_1995', '6400', 34);
INSERT INTO iteminfo_data VALUES (851, 'price_fraud_1996', '1641.5', 34);
INSERT INTO iteminfo_data VALUES (852, 'price_logic_min_1996', '2626.4', 34);
INSERT INTO iteminfo_data VALUES (853, 'price_logic_max_1996', '6566', 34);
INSERT INTO iteminfo_data VALUES (854, 'price_fraud_1997', '1526', 34);
INSERT INTO iteminfo_data VALUES (855, 'price_logic_min_1997', '2441.6', 34);
INSERT INTO iteminfo_data VALUES (856, 'price_logic_max_1997', '6104', 34);
INSERT INTO iteminfo_data VALUES (857, 'price_fraud_1998', '1791', 34);
INSERT INTO iteminfo_data VALUES (858, 'price_logic_min_1998', '2865.6', 34);
INSERT INTO iteminfo_data VALUES (859, 'price_logic_max_1998', '7164', 34);
INSERT INTO iteminfo_data VALUES (860, 'price_fraud_1999', '2001.5', 34);
INSERT INTO iteminfo_data VALUES (861, 'price_logic_min_1999', '3202.4', 34);
INSERT INTO iteminfo_data VALUES (862, 'price_logic_max_1999', '8006', 34);
INSERT INTO iteminfo_data VALUES (863, 'price_fraud_2000', '2689', 34);
INSERT INTO iteminfo_data VALUES (864, 'price_logic_min_2000', '4302.4', 34);
INSERT INTO iteminfo_data VALUES (865, 'price_logic_max_2000', '10756', 34);
INSERT INTO iteminfo_data VALUES (866, 'price_fraud_2001', '3090.5', 34);
INSERT INTO iteminfo_data VALUES (867, 'price_logic_min_2001', '4944.8', 34);
INSERT INTO iteminfo_data VALUES (868, 'price_logic_max_2001', '12362', 34);
INSERT INTO iteminfo_data VALUES (869, 'price_fraud_2002', '3618', 34);
INSERT INTO iteminfo_data VALUES (870, 'price_logic_min_2002', '5788.8', 34);
INSERT INTO iteminfo_data VALUES (871, 'price_logic_max_2002', '14472', 34);
INSERT INTO iteminfo_data VALUES (872, 'price_fraud_2003', '4496.5', 34);
INSERT INTO iteminfo_data VALUES (873, 'price_logic_min_2003', '7194.4', 34);
INSERT INTO iteminfo_data VALUES (874, 'price_logic_max_2003', '17986', 34);
INSERT INTO iteminfo_data VALUES (875, 'price_fraud_2004', '5294', 34);
INSERT INTO iteminfo_data VALUES (876, 'price_logic_min_2004', '8470.4', 34);
INSERT INTO iteminfo_data VALUES (877, 'price_logic_max_2004', '21176', 34);
INSERT INTO iteminfo_data VALUES (878, 'price_fraud_2005', '6196.5', 34);
INSERT INTO iteminfo_data VALUES (879, 'price_logic_min_2005', '9914.4', 34);
INSERT INTO iteminfo_data VALUES (880, 'price_logic_max_2005', '24786', 34);
INSERT INTO iteminfo_data VALUES (881, 'price_fraud_2006', '7274.5', 34);
INSERT INTO iteminfo_data VALUES (882, 'price_logic_min_2006', '11639.2', 34);
INSERT INTO iteminfo_data VALUES (883, 'price_logic_max_2006', '29098', 34);
INSERT INTO iteminfo_data VALUES (884, 'price_fraud_2007', '8958', 34);
INSERT INTO iteminfo_data VALUES (885, 'price_logic_min_2007', '14332.8', 34);
INSERT INTO iteminfo_data VALUES (886, 'price_logic_max_2007', '35832', 34);
INSERT INTO iteminfo_data VALUES (887, 'price_fraud_2008', '9717.5', 34);
INSERT INTO iteminfo_data VALUES (888, 'price_logic_min_2008', '15548', 34);
INSERT INTO iteminfo_data VALUES (889, 'price_logic_max_2008', '38870', 34);
INSERT INTO iteminfo_data VALUES (890, 'price_fraud_1975', '1040', 28);
INSERT INTO iteminfo_data VALUES (891, 'price_logic_min_1975', '1664', 28);
INSERT INTO iteminfo_data VALUES (892, 'price_logic_max_1975', '4160', 28);
INSERT INTO iteminfo_data VALUES (893, 'price_fraud_1976', '552', 28);
INSERT INTO iteminfo_data VALUES (894, 'price_logic_min_1976', '883.2', 28);
INSERT INTO iteminfo_data VALUES (895, 'price_logic_max_1976', '2208', 28);
INSERT INTO iteminfo_data VALUES (896, 'price_fraud_1977', '434.5', 28);
INSERT INTO iteminfo_data VALUES (897, 'price_logic_min_1977', '695.2', 28);
INSERT INTO iteminfo_data VALUES (898, 'price_logic_max_1977', '1738', 28);
INSERT INTO iteminfo_data VALUES (899, 'price_fraud_1978', '508', 28);
INSERT INTO iteminfo_data VALUES (900, 'price_logic_min_1978', '812.8', 28);
INSERT INTO iteminfo_data VALUES (901, 'price_logic_max_1978', '2032', 28);
INSERT INTO iteminfo_data VALUES (902, 'price_fraud_1979', '506.5', 28);
INSERT INTO iteminfo_data VALUES (903, 'price_logic_min_1979', '810.4', 28);
INSERT INTO iteminfo_data VALUES (904, 'price_logic_max_1979', '2026', 28);
INSERT INTO iteminfo_data VALUES (905, 'price_fraud_1980', '585.5', 28);
INSERT INTO iteminfo_data VALUES (906, 'price_logic_min_1980', '936.8', 28);
INSERT INTO iteminfo_data VALUES (907, 'price_logic_max_1980', '2342', 28);
INSERT INTO iteminfo_data VALUES (908, 'price_fraud_1981', '425', 28);
INSERT INTO iteminfo_data VALUES (909, 'price_logic_min_1981', '680', 28);
INSERT INTO iteminfo_data VALUES (910, 'price_logic_max_1981', '1700', 28);
INSERT INTO iteminfo_data VALUES (911, 'price_fraud_1982', '415', 28);
INSERT INTO iteminfo_data VALUES (912, 'price_logic_min_1982', '664', 28);
INSERT INTO iteminfo_data VALUES (913, 'price_logic_max_1982', '1660', 28);
INSERT INTO iteminfo_data VALUES (914, 'price_fraud_1983', '531.5', 28);
INSERT INTO iteminfo_data VALUES (915, 'price_logic_min_1983', '850.4', 28);
INSERT INTO iteminfo_data VALUES (916, 'price_logic_max_1983', '2126', 28);
INSERT INTO iteminfo_data VALUES (917, 'price_fraud_1984', '463', 28);
INSERT INTO iteminfo_data VALUES (918, 'price_logic_min_1984', '740.8', 28);
INSERT INTO iteminfo_data VALUES (919, 'price_logic_max_1984', '1852', 28);
INSERT INTO iteminfo_data VALUES (920, 'price_fraud_1985', '393.5', 28);
INSERT INTO iteminfo_data VALUES (921, 'price_logic_min_1985', '629.6', 28);
INSERT INTO iteminfo_data VALUES (922, 'price_logic_max_1985', '1574', 28);
INSERT INTO iteminfo_data VALUES (923, 'price_fraud_1986', '383', 28);
INSERT INTO iteminfo_data VALUES (924, 'price_logic_min_1986', '612.8', 28);
INSERT INTO iteminfo_data VALUES (925, 'price_logic_max_1986', '1532', 28);
INSERT INTO iteminfo_data VALUES (926, 'price_fraud_1987', '413', 28);
INSERT INTO iteminfo_data VALUES (927, 'price_logic_min_1987', '660.8', 28);
INSERT INTO iteminfo_data VALUES (928, 'price_logic_max_1987', '1652', 28);
INSERT INTO iteminfo_data VALUES (929, 'price_fraud_1988', '413.5', 28);
INSERT INTO iteminfo_data VALUES (930, 'price_logic_min_1988', '661.6', 28);
INSERT INTO iteminfo_data VALUES (931, 'price_logic_max_1988', '1654', 28);
INSERT INTO iteminfo_data VALUES (932, 'price_fraud_1989', '408.5', 28);
INSERT INTO iteminfo_data VALUES (933, 'price_logic_min_1989', '653.6', 28);
INSERT INTO iteminfo_data VALUES (934, 'price_logic_max_1989', '1634', 28);
INSERT INTO iteminfo_data VALUES (935, 'price_fraud_1990', '472', 28);
INSERT INTO iteminfo_data VALUES (936, 'price_logic_min_1990', '755.2', 28);
INSERT INTO iteminfo_data VALUES (937, 'price_logic_max_1990', '1888', 28);
INSERT INTO iteminfo_data VALUES (938, 'price_fraud_1991', '550.5', 28);
INSERT INTO iteminfo_data VALUES (939, 'price_logic_min_1991', '880.8', 28);
INSERT INTO iteminfo_data VALUES (940, 'price_logic_max_1991', '2202', 28);
INSERT INTO iteminfo_data VALUES (941, 'price_fraud_1992', '628.5', 28);
INSERT INTO iteminfo_data VALUES (942, 'price_logic_min_1992', '1005.6', 28);
INSERT INTO iteminfo_data VALUES (943, 'price_logic_max_1992', '2514', 28);
INSERT INTO iteminfo_data VALUES (944, 'price_fraud_1993', '751.5', 28);
INSERT INTO iteminfo_data VALUES (945, 'price_logic_min_1993', '1202.4', 28);
INSERT INTO iteminfo_data VALUES (946, 'price_logic_max_1993', '3006', 28);
INSERT INTO iteminfo_data VALUES (947, 'price_fraud_1994', '886', 28);
INSERT INTO iteminfo_data VALUES (948, 'price_logic_min_1994', '1417.6', 28);
INSERT INTO iteminfo_data VALUES (949, 'price_logic_max_1994', '3544', 28);
INSERT INTO iteminfo_data VALUES (950, 'price_fraud_1995', '1025', 28);
INSERT INTO iteminfo_data VALUES (951, 'price_logic_min_1995', '1640', 28);
INSERT INTO iteminfo_data VALUES (952, 'price_logic_max_1995', '4100', 28);
INSERT INTO iteminfo_data VALUES (953, 'price_fraud_1996', '1200.5', 28);
INSERT INTO iteminfo_data VALUES (954, 'price_logic_min_1996', '1920.8', 28);
INSERT INTO iteminfo_data VALUES (955, 'price_logic_max_1996', '4802', 28);
INSERT INTO iteminfo_data VALUES (956, 'price_fraud_1997', '1526.5', 28);
INSERT INTO iteminfo_data VALUES (957, 'price_logic_min_1997', '2442.4', 28);
INSERT INTO iteminfo_data VALUES (958, 'price_logic_max_1997', '6106', 28);
INSERT INTO iteminfo_data VALUES (959, 'price_fraud_1998', '1923.5', 28);
INSERT INTO iteminfo_data VALUES (960, 'price_logic_min_1998', '3077.6', 28);
INSERT INTO iteminfo_data VALUES (961, 'price_logic_max_1998', '7694', 28);
INSERT INTO iteminfo_data VALUES (962, 'price_fraud_1999', '2253.5', 28);
INSERT INTO iteminfo_data VALUES (963, 'price_logic_min_1999', '3605.6', 28);
INSERT INTO iteminfo_data VALUES (964, 'price_logic_max_1999', '9014', 28);
INSERT INTO iteminfo_data VALUES (965, 'price_fraud_2000', '2633.5', 28);
INSERT INTO iteminfo_data VALUES (966, 'price_logic_min_2000', '4213.6', 28);
INSERT INTO iteminfo_data VALUES (967, 'price_logic_max_2000', '10534', 28);
INSERT INTO iteminfo_data VALUES (968, 'price_fraud_2001', '3158', 28);
INSERT INTO iteminfo_data VALUES (969, 'price_logic_min_2001', '5052.8', 28);
INSERT INTO iteminfo_data VALUES (970, 'price_logic_max_2001', '12632', 28);
INSERT INTO iteminfo_data VALUES (971, 'price_fraud_2002', '3611', 28);
INSERT INTO iteminfo_data VALUES (972, 'price_logic_min_2002', '5777.6', 28);
INSERT INTO iteminfo_data VALUES (973, 'price_logic_max_2002', '14444', 28);
INSERT INTO iteminfo_data VALUES (974, 'price_fraud_2003', '4475', 28);
INSERT INTO iteminfo_data VALUES (975, 'price_logic_min_2003', '7160', 28);
INSERT INTO iteminfo_data VALUES (976, 'price_logic_max_2003', '17900', 28);
INSERT INTO iteminfo_data VALUES (977, 'price_fraud_2004', '5082.5', 28);
INSERT INTO iteminfo_data VALUES (978, 'price_logic_min_2004', '8132', 28);
INSERT INTO iteminfo_data VALUES (979, 'price_logic_max_2004', '20330', 28);
INSERT INTO iteminfo_data VALUES (980, 'price_fraud_2005', '5637', 28);
INSERT INTO iteminfo_data VALUES (981, 'price_logic_min_2005', '9019.2', 28);
INSERT INTO iteminfo_data VALUES (982, 'price_logic_max_2005', '22548', 28);
INSERT INTO iteminfo_data VALUES (983, 'price_fraud_2006', '6450', 28);
INSERT INTO iteminfo_data VALUES (984, 'price_logic_min_2006', '10320', 28);
INSERT INTO iteminfo_data VALUES (985, 'price_logic_max_2006', '25800', 28);
INSERT INTO iteminfo_data VALUES (986, 'price_fraud_2007', '7576', 28);
INSERT INTO iteminfo_data VALUES (987, 'price_logic_min_2007', '12121.6', 28);
INSERT INTO iteminfo_data VALUES (988, 'price_logic_max_2007', '30304', 28);
INSERT INTO iteminfo_data VALUES (989, 'price_fraud_2008', '8842.5', 28);
INSERT INTO iteminfo_data VALUES (990, 'price_logic_min_2008', '14148', 28);
INSERT INTO iteminfo_data VALUES (991, 'price_logic_max_2008', '35370', 28);
INSERT INTO iteminfo_data VALUES (992, 'price_fraud_1987', '750', 35);
INSERT INTO iteminfo_data VALUES (993, 'price_logic_min_1987', '1200', 35);
INSERT INTO iteminfo_data VALUES (994, 'price_logic_max_1987', '3000', 35);
INSERT INTO iteminfo_data VALUES (995, 'price_fraud_1989', '750', 35);
INSERT INTO iteminfo_data VALUES (996, 'price_logic_min_1989', '1200', 35);
INSERT INTO iteminfo_data VALUES (997, 'price_logic_max_1989', '3000', 35);
INSERT INTO iteminfo_data VALUES (998, 'price_fraud_1991', '350', 35);
INSERT INTO iteminfo_data VALUES (999, 'price_logic_min_1991', '560', 35);
INSERT INTO iteminfo_data VALUES (1000, 'price_logic_max_1991', '1400', 35);
INSERT INTO iteminfo_data VALUES (1001, 'price_fraud_1993', '775', 35);
INSERT INTO iteminfo_data VALUES (1002, 'price_logic_min_1993', '1240', 35);
INSERT INTO iteminfo_data VALUES (1003, 'price_logic_max_1993', '3100', 35);
INSERT INTO iteminfo_data VALUES (1004, 'price_fraud_1994', '856', 35);
INSERT INTO iteminfo_data VALUES (1005, 'price_logic_min_1994', '1369.6', 35);
INSERT INTO iteminfo_data VALUES (1006, 'price_logic_max_1994', '3424', 35);
INSERT INTO iteminfo_data VALUES (1007, 'price_fraud_1995', '699', 35);
INSERT INTO iteminfo_data VALUES (1008, 'price_logic_min_1995', '1118.4', 35);
INSERT INTO iteminfo_data VALUES (1009, 'price_logic_max_1995', '2796', 35);
INSERT INTO iteminfo_data VALUES (1010, 'price_fraud_1996', '825', 35);
INSERT INTO iteminfo_data VALUES (1011, 'price_logic_min_1996', '1320', 35);
INSERT INTO iteminfo_data VALUES (1012, 'price_logic_max_1996', '3300', 35);
INSERT INTO iteminfo_data VALUES (1013, 'price_fraud_1997', '1232.5', 35);
INSERT INTO iteminfo_data VALUES (1014, 'price_logic_min_1997', '1972', 35);
INSERT INTO iteminfo_data VALUES (1015, 'price_logic_max_1997', '4930', 35);
INSERT INTO iteminfo_data VALUES (1016, 'price_fraud_1998', '1326', 35);
INSERT INTO iteminfo_data VALUES (1017, 'price_logic_min_1998', '2121.6', 35);
INSERT INTO iteminfo_data VALUES (1018, 'price_logic_max_1998', '5304', 35);
INSERT INTO iteminfo_data VALUES (1019, 'price_fraud_1999', '1411', 35);
INSERT INTO iteminfo_data VALUES (1020, 'price_logic_min_1999', '2257.6', 35);
INSERT INTO iteminfo_data VALUES (1021, 'price_logic_max_1999', '5644', 35);
INSERT INTO iteminfo_data VALUES (1022, 'price_fraud_2000', '2046', 35);
INSERT INTO iteminfo_data VALUES (1023, 'price_logic_min_2000', '3273.6', 35);
INSERT INTO iteminfo_data VALUES (1024, 'price_logic_max_2000', '8184', 35);
INSERT INTO iteminfo_data VALUES (1025, 'price_fraud_2001', '2396', 35);
INSERT INTO iteminfo_data VALUES (1026, 'price_logic_min_2001', '3833.6', 35);
INSERT INTO iteminfo_data VALUES (1027, 'price_logic_max_2001', '9584', 35);
INSERT INTO iteminfo_data VALUES (1028, 'price_fraud_2002', '2957.5', 35);
INSERT INTO iteminfo_data VALUES (1029, 'price_logic_min_2002', '4732', 35);
INSERT INTO iteminfo_data VALUES (1030, 'price_logic_max_2002', '11830', 35);
INSERT INTO iteminfo_data VALUES (1031, 'price_fraud_2003', '3562', 35);
INSERT INTO iteminfo_data VALUES (1032, 'price_logic_min_2003', '5699.2', 35);
INSERT INTO iteminfo_data VALUES (1033, 'price_logic_max_2003', '14248', 35);
INSERT INTO iteminfo_data VALUES (1034, 'price_fraud_2004', '3935', 35);
INSERT INTO iteminfo_data VALUES (1035, 'price_logic_min_2004', '6296', 35);
INSERT INTO iteminfo_data VALUES (1036, 'price_logic_max_2004', '15740', 35);
INSERT INTO iteminfo_data VALUES (1037, 'price_fraud_2005', '4332', 35);
INSERT INTO iteminfo_data VALUES (1038, 'price_logic_min_2005', '6931.2', 35);
INSERT INTO iteminfo_data VALUES (1039, 'price_logic_max_2005', '17328', 35);
INSERT INTO iteminfo_data VALUES (1040, 'price_fraud_2006', '4738.5', 35);
INSERT INTO iteminfo_data VALUES (1041, 'price_logic_min_2006', '7581.6', 35);
INSERT INTO iteminfo_data VALUES (1042, 'price_logic_max_2006', '18954', 35);
INSERT INTO iteminfo_data VALUES (1043, 'price_fraud_2007', '5379', 35);
INSERT INTO iteminfo_data VALUES (1044, 'price_logic_min_2007', '8606.4', 35);
INSERT INTO iteminfo_data VALUES (1045, 'price_logic_max_2007', '21516', 35);
INSERT INTO iteminfo_data VALUES (1046, 'price_fraud_2008', '5580.5', 35);
INSERT INTO iteminfo_data VALUES (1047, 'price_logic_min_2008', '8928.8', 35);
INSERT INTO iteminfo_data VALUES (1048, 'price_logic_max_2008', '22322', 35);
INSERT INTO iteminfo_data VALUES (1049, 'price_fraud_2002', '3400', 51);
INSERT INTO iteminfo_data VALUES (1050, 'price_logic_min_2002', '5440', 51);
INSERT INTO iteminfo_data VALUES (1051, 'price_logic_max_2002', '13600', 51);
INSERT INTO iteminfo_data VALUES (1052, 'price_fraud_2003', '3200', 51);
INSERT INTO iteminfo_data VALUES (1053, 'price_logic_min_2003', '5120', 51);
INSERT INTO iteminfo_data VALUES (1054, 'price_logic_max_2003', '12800', 51);
INSERT INTO iteminfo_data VALUES (1055, 'price_fraud_2004', '3250', 51);
INSERT INTO iteminfo_data VALUES (1056, 'price_logic_min_2004', '5200', 51);
INSERT INTO iteminfo_data VALUES (1057, 'price_logic_max_2004', '13000', 51);
INSERT INTO iteminfo_data VALUES (1058, 'price_fraud_2005', '3350', 51);
INSERT INTO iteminfo_data VALUES (1059, 'price_logic_min_2005', '5360', 51);
INSERT INTO iteminfo_data VALUES (1060, 'price_logic_max_2005', '13400', 51);
INSERT INTO iteminfo_data VALUES (1061, 'price_fraud_1984', '1500', 36);
INSERT INTO iteminfo_data VALUES (1062, 'price_logic_min_1984', '2400', 36);
INSERT INTO iteminfo_data VALUES (1063, 'price_logic_max_1984', '6000', 36);
INSERT INTO iteminfo_data VALUES (1064, 'price_fraud_1985', '300', 36);
INSERT INTO iteminfo_data VALUES (1065, 'price_logic_min_1985', '480', 36);
INSERT INTO iteminfo_data VALUES (1066, 'price_logic_max_1985', '1200', 36);
INSERT INTO iteminfo_data VALUES (1067, 'price_fraud_1986', '250', 36);
INSERT INTO iteminfo_data VALUES (1068, 'price_logic_min_1986', '400', 36);
INSERT INTO iteminfo_data VALUES (1069, 'price_logic_max_1986', '1000', 36);
INSERT INTO iteminfo_data VALUES (1070, 'price_fraud_1987', '418.5', 36);
INSERT INTO iteminfo_data VALUES (1071, 'price_logic_min_1987', '669.6', 36);
INSERT INTO iteminfo_data VALUES (1072, 'price_logic_max_1987', '1674', 36);
INSERT INTO iteminfo_data VALUES (1073, 'price_fraud_1988', '246.5', 36);
INSERT INTO iteminfo_data VALUES (1074, 'price_logic_min_1988', '394.4', 36);
INSERT INTO iteminfo_data VALUES (1075, 'price_logic_max_1988', '986', 36);
INSERT INTO iteminfo_data VALUES (1076, 'price_fraud_1989', '217.5', 36);
INSERT INTO iteminfo_data VALUES (1077, 'price_logic_min_1989', '348', 36);
INSERT INTO iteminfo_data VALUES (1078, 'price_logic_max_1989', '870', 36);
INSERT INTO iteminfo_data VALUES (1079, 'price_fraud_1990', '385', 36);
INSERT INTO iteminfo_data VALUES (1080, 'price_logic_min_1990', '616', 36);
INSERT INTO iteminfo_data VALUES (1081, 'price_logic_max_1990', '1540', 36);
INSERT INTO iteminfo_data VALUES (1082, 'price_fraud_1991', '365.5', 36);
INSERT INTO iteminfo_data VALUES (1083, 'price_logic_min_1991', '584.8', 36);
INSERT INTO iteminfo_data VALUES (1084, 'price_logic_max_1991', '1462', 36);
INSERT INTO iteminfo_data VALUES (1085, 'price_fraud_1992', '418.5', 36);
INSERT INTO iteminfo_data VALUES (1086, 'price_logic_min_1992', '669.6', 36);
INSERT INTO iteminfo_data VALUES (1087, 'price_logic_max_1992', '1674', 36);
INSERT INTO iteminfo_data VALUES (1088, 'price_fraud_1993', '419', 36);
INSERT INTO iteminfo_data VALUES (1089, 'price_logic_min_1993', '670.4', 36);
INSERT INTO iteminfo_data VALUES (1090, 'price_logic_max_1993', '1676', 36);
INSERT INTO iteminfo_data VALUES (1091, 'price_fraud_1994', '684.5', 36);
INSERT INTO iteminfo_data VALUES (1092, 'price_logic_min_1994', '1095.2', 36);
INSERT INTO iteminfo_data VALUES (1093, 'price_logic_max_1994', '2738', 36);
INSERT INTO iteminfo_data VALUES (1094, 'price_fraud_1995', '826.5', 36);
INSERT INTO iteminfo_data VALUES (1095, 'price_logic_min_1995', '1322.4', 36);
INSERT INTO iteminfo_data VALUES (1096, 'price_logic_max_1995', '3306', 36);
INSERT INTO iteminfo_data VALUES (1097, 'price_fraud_1996', '813.5', 36);
INSERT INTO iteminfo_data VALUES (1098, 'price_logic_min_1996', '1301.6', 36);
INSERT INTO iteminfo_data VALUES (1099, 'price_logic_max_1996', '3254', 36);
INSERT INTO iteminfo_data VALUES (1100, 'price_fraud_1997', '1050.5', 36);
INSERT INTO iteminfo_data VALUES (1101, 'price_logic_min_1997', '1680.8', 36);
INSERT INTO iteminfo_data VALUES (1102, 'price_logic_max_1997', '4202', 36);
INSERT INTO iteminfo_data VALUES (1103, 'price_fraud_1998', '1403', 36);
INSERT INTO iteminfo_data VALUES (1104, 'price_logic_min_1998', '2244.8', 36);
INSERT INTO iteminfo_data VALUES (1105, 'price_logic_max_1998', '5612', 36);
INSERT INTO iteminfo_data VALUES (1106, 'price_fraud_1999', '1567', 36);
INSERT INTO iteminfo_data VALUES (1107, 'price_logic_min_1999', '2507.2', 36);
INSERT INTO iteminfo_data VALUES (1108, 'price_logic_max_1999', '6268', 36);
INSERT INTO iteminfo_data VALUES (1109, 'price_fraud_2000', '2024.5', 36);
INSERT INTO iteminfo_data VALUES (1110, 'price_logic_min_2000', '3239.2', 36);
INSERT INTO iteminfo_data VALUES (1111, 'price_logic_max_2000', '8098', 36);
INSERT INTO iteminfo_data VALUES (1112, 'price_fraud_2001', '2379', 36);
INSERT INTO iteminfo_data VALUES (1113, 'price_logic_min_2001', '3806.4', 36);
INSERT INTO iteminfo_data VALUES (1114, 'price_logic_max_2001', '9516', 36);
INSERT INTO iteminfo_data VALUES (1115, 'price_fraud_2002', '3110.5', 36);
INSERT INTO iteminfo_data VALUES (1116, 'price_logic_min_2002', '4976.8', 36);
INSERT INTO iteminfo_data VALUES (1117, 'price_logic_max_2002', '12442', 36);
INSERT INTO iteminfo_data VALUES (1118, 'price_fraud_2003', '3624', 36);
INSERT INTO iteminfo_data VALUES (1119, 'price_logic_min_2003', '5798.4', 36);
INSERT INTO iteminfo_data VALUES (1120, 'price_logic_max_2003', '14496', 36);
INSERT INTO iteminfo_data VALUES (1121, 'price_fraud_2004', '3941', 36);
INSERT INTO iteminfo_data VALUES (1122, 'price_logic_min_2004', '6305.6', 36);
INSERT INTO iteminfo_data VALUES (1123, 'price_logic_max_2004', '15764', 36);
INSERT INTO iteminfo_data VALUES (1124, 'price_fraud_2005', '4407', 36);
INSERT INTO iteminfo_data VALUES (1125, 'price_logic_min_2005', '7051.2', 36);
INSERT INTO iteminfo_data VALUES (1126, 'price_logic_max_2005', '17628', 36);
INSERT INTO iteminfo_data VALUES (1127, 'price_fraud_2006', '5228', 36);
INSERT INTO iteminfo_data VALUES (1128, 'price_logic_min_2006', '8364.8', 36);
INSERT INTO iteminfo_data VALUES (1129, 'price_logic_max_2006', '20912', 36);
INSERT INTO iteminfo_data VALUES (1130, 'price_fraud_2007', '6039', 36);
INSERT INTO iteminfo_data VALUES (1131, 'price_logic_min_2007', '9662.4', 36);
INSERT INTO iteminfo_data VALUES (1132, 'price_logic_max_2007', '24156', 36);
INSERT INTO iteminfo_data VALUES (1133, 'price_fraud_2008', '7001', 36);
INSERT INTO iteminfo_data VALUES (1134, 'price_logic_min_2008', '11201.6', 36);
INSERT INTO iteminfo_data VALUES (1135, 'price_logic_max_2008', '28004', 36);
INSERT INTO iteminfo_data VALUES (1136, 'price_fraud_2005', '5950', 52);
INSERT INTO iteminfo_data VALUES (1137, 'price_logic_min_2005', '9520', 52);
INSERT INTO iteminfo_data VALUES (1138, 'price_logic_max_2005', '23800', 52);
INSERT INTO iteminfo_data VALUES (1139, 'price_fraud_2006', '6500', 52);
INSERT INTO iteminfo_data VALUES (1140, 'price_logic_min_2006', '10400', 52);
INSERT INTO iteminfo_data VALUES (1141, 'price_logic_max_2006', '26000', 52);
INSERT INTO iteminfo_data VALUES (1142, 'price_fraud_2007', '7203', 52);
INSERT INTO iteminfo_data VALUES (1143, 'price_logic_min_2007', '11524.8', 52);
INSERT INTO iteminfo_data VALUES (1144, 'price_logic_max_2007', '28812', 52);
INSERT INTO iteminfo_data VALUES (1145, 'price_fraud_2008', '9750', 52);
INSERT INTO iteminfo_data VALUES (1146, 'price_logic_min_2008', '15600', 52);
INSERT INTO iteminfo_data VALUES (1147, 'price_logic_max_2008', '39000', 52);
INSERT INTO iteminfo_data VALUES (1148, 'price_fraud_2005', '6987.5', 53);
INSERT INTO iteminfo_data VALUES (1149, 'price_logic_min_2005', '11180', 53);
INSERT INTO iteminfo_data VALUES (1150, 'price_logic_max_2005', '27950', 53);
INSERT INTO iteminfo_data VALUES (1151, 'price_fraud_2006', '7982.5', 53);
INSERT INTO iteminfo_data VALUES (1152, 'price_logic_min_2006', '12772', 53);
INSERT INTO iteminfo_data VALUES (1153, 'price_logic_max_2006', '31930', 53);
INSERT INTO iteminfo_data VALUES (1154, 'price_fraud_2007', '8383.5', 53);
INSERT INTO iteminfo_data VALUES (1155, 'price_logic_min_2007', '13413.6', 53);
INSERT INTO iteminfo_data VALUES (1156, 'price_logic_max_2007', '33534', 53);
INSERT INTO iteminfo_data VALUES (1157, 'price_fraud_2008', '10250', 53);
INSERT INTO iteminfo_data VALUES (1158, 'price_logic_min_2008', '16400', 53);
INSERT INTO iteminfo_data VALUES (1159, 'price_logic_max_2008', '41000', 53);
INSERT INTO iteminfo_data VALUES (1160, 'price_fraud_2000', '2980', 54);
INSERT INTO iteminfo_data VALUES (1161, 'price_logic_min_2000', '4768', 54);
INSERT INTO iteminfo_data VALUES (1162, 'price_logic_max_2000', '11920', 54);
INSERT INTO iteminfo_data VALUES (1163, 'price_fraud_2001', '3241.5', 54);
INSERT INTO iteminfo_data VALUES (1164, 'price_logic_min_2001', '5186.4', 54);
INSERT INTO iteminfo_data VALUES (1165, 'price_logic_max_2001', '12966', 54);
INSERT INTO iteminfo_data VALUES (1166, 'price_fraud_2002', '3950', 54);
INSERT INTO iteminfo_data VALUES (1167, 'price_logic_min_2002', '6320', 54);
INSERT INTO iteminfo_data VALUES (1168, 'price_logic_max_2002', '15800', 54);
INSERT INTO iteminfo_data VALUES (1169, 'price_fraud_2003', '4100', 54);
INSERT INTO iteminfo_data VALUES (1170, 'price_logic_min_2003', '6560', 54);
INSERT INTO iteminfo_data VALUES (1171, 'price_logic_max_2003', '16400', 54);
INSERT INTO iteminfo_data VALUES (1172, 'price_fraud_2004', '5033', 54);
INSERT INTO iteminfo_data VALUES (1173, 'price_logic_min_2004', '8052.8', 54);
INSERT INTO iteminfo_data VALUES (1174, 'price_logic_max_2004', '20132', 54);
INSERT INTO iteminfo_data VALUES (1175, 'price_fraud_2002', '5250', 55);
INSERT INTO iteminfo_data VALUES (1176, 'price_logic_min_2002', '8400', 55);
INSERT INTO iteminfo_data VALUES (1177, 'price_logic_max_2002', '21000', 55);
INSERT INTO iteminfo_data VALUES (1178, 'price_fraud_2003', '5750', 55);
INSERT INTO iteminfo_data VALUES (1179, 'price_logic_min_2003', '9200', 55);
INSERT INTO iteminfo_data VALUES (1180, 'price_logic_max_2003', '23000', 55);
INSERT INTO iteminfo_data VALUES (1181, 'price_fraud_2004', '7014', 55);
INSERT INTO iteminfo_data VALUES (1182, 'price_logic_min_2004', '11222.4', 55);
INSERT INTO iteminfo_data VALUES (1183, 'price_logic_max_2004', '28056', 55);
INSERT INTO iteminfo_data VALUES (1184, 'price_fraud_2005', '7406', 55);
INSERT INTO iteminfo_data VALUES (1185, 'price_logic_min_2005', '11849.6', 55);
INSERT INTO iteminfo_data VALUES (1186, 'price_logic_max_2005', '29624', 55);
INSERT INTO iteminfo_data VALUES (1187, 'price_fraud_1999', '2250', 37);
INSERT INTO iteminfo_data VALUES (1188, 'price_logic_min_1999', '3600', 37);
INSERT INTO iteminfo_data VALUES (1189, 'price_logic_max_1999', '9000', 37);
INSERT INTO iteminfo_data VALUES (1190, 'price_fraud_2000', '3055.5', 37);
INSERT INTO iteminfo_data VALUES (1191, 'price_logic_min_2000', '4888.8', 37);
INSERT INTO iteminfo_data VALUES (1192, 'price_logic_max_2000', '12222', 37);
INSERT INTO iteminfo_data VALUES (1193, 'price_fraud_2001', '3446', 37);
INSERT INTO iteminfo_data VALUES (1194, 'price_logic_min_2001', '5513.6', 37);
INSERT INTO iteminfo_data VALUES (1195, 'price_logic_max_2001', '13784', 37);
INSERT INTO iteminfo_data VALUES (1196, 'price_fraud_2002', '4447.5', 37);
INSERT INTO iteminfo_data VALUES (1197, 'price_logic_min_2002', '7116', 37);
INSERT INTO iteminfo_data VALUES (1198, 'price_logic_max_2002', '17790', 37);
INSERT INTO iteminfo_data VALUES (1199, 'price_fraud_2003', '5235.5', 37);
INSERT INTO iteminfo_data VALUES (1200, 'price_logic_min_2003', '8376.8', 37);
INSERT INTO iteminfo_data VALUES (1201, 'price_logic_max_2003', '20942', 37);
INSERT INTO iteminfo_data VALUES (1202, 'price_fraud_2004', '5725.5', 37);
INSERT INTO iteminfo_data VALUES (1203, 'price_logic_min_2004', '9160.8', 37);
INSERT INTO iteminfo_data VALUES (1204, 'price_logic_max_2004', '22902', 37);
INSERT INTO iteminfo_data VALUES (1205, 'price_fraud_2005', '6721', 37);
INSERT INTO iteminfo_data VALUES (1206, 'price_logic_min_2005', '10753.6', 37);
INSERT INTO iteminfo_data VALUES (1207, 'price_logic_max_2005', '26884', 37);
INSERT INTO iteminfo_data VALUES (1208, 'price_fraud_2006', '7476.5', 37);
INSERT INTO iteminfo_data VALUES (1209, 'price_logic_min_2006', '11962.4', 37);
INSERT INTO iteminfo_data VALUES (1210, 'price_logic_max_2006', '29906', 37);
INSERT INTO iteminfo_data VALUES (1211, 'price_fraud_2007', '8677', 37);
INSERT INTO iteminfo_data VALUES (1212, 'price_logic_min_2007', '13883.2', 37);
INSERT INTO iteminfo_data VALUES (1213, 'price_logic_max_2007', '34708', 37);
INSERT INTO iteminfo_data VALUES (1214, 'price_fraud_2008', '10337.5', 37);
INSERT INTO iteminfo_data VALUES (1215, 'price_logic_min_2008', '16540', 37);
INSERT INTO iteminfo_data VALUES (1216, 'price_logic_max_2008', '41350', 37);
INSERT INTO iteminfo_data VALUES (1217, 'price_fraud_1983', '300', 29);
INSERT INTO iteminfo_data VALUES (1218, 'price_logic_min_1983', '480', 29);
INSERT INTO iteminfo_data VALUES (1219, 'price_logic_max_1983', '1200', 29);
INSERT INTO iteminfo_data VALUES (1220, 'price_fraud_1984', '1500', 29);
INSERT INTO iteminfo_data VALUES (1221, 'price_logic_min_1984', '2400', 29);
INSERT INTO iteminfo_data VALUES (1222, 'price_logic_max_1984', '6000', 29);
INSERT INTO iteminfo_data VALUES (1223, 'price_fraud_1985', '300', 29);
INSERT INTO iteminfo_data VALUES (1224, 'price_logic_min_1985', '480', 29);
INSERT INTO iteminfo_data VALUES (1225, 'price_logic_max_1985', '1200', 29);
INSERT INTO iteminfo_data VALUES (1226, 'price_fraud_1986', '290', 29);
INSERT INTO iteminfo_data VALUES (1227, 'price_logic_min_1986', '464', 29);
INSERT INTO iteminfo_data VALUES (1228, 'price_logic_max_1986', '1160', 29);
INSERT INTO iteminfo_data VALUES (1229, 'price_fraud_1987', '458', 29);
INSERT INTO iteminfo_data VALUES (1230, 'price_logic_min_1987', '732.8', 29);
INSERT INTO iteminfo_data VALUES (1231, 'price_logic_max_1987', '1832', 29);
INSERT INTO iteminfo_data VALUES (1232, 'price_fraud_1988', '301.5', 29);
INSERT INTO iteminfo_data VALUES (1233, 'price_logic_min_1988', '482.4', 29);
INSERT INTO iteminfo_data VALUES (1234, 'price_logic_max_1988', '1206', 29);
INSERT INTO iteminfo_data VALUES (1235, 'price_fraud_1989', '281', 29);
INSERT INTO iteminfo_data VALUES (1236, 'price_logic_min_1989', '449.6', 29);
INSERT INTO iteminfo_data VALUES (1237, 'price_logic_max_1989', '1124', 29);
INSERT INTO iteminfo_data VALUES (1238, 'price_fraud_1990', '345', 29);
INSERT INTO iteminfo_data VALUES (1239, 'price_logic_min_1990', '552', 29);
INSERT INTO iteminfo_data VALUES (1240, 'price_logic_max_1990', '1380', 29);
INSERT INTO iteminfo_data VALUES (1241, 'price_fraud_1991', '339', 29);
INSERT INTO iteminfo_data VALUES (1242, 'price_logic_min_1991', '542.4', 29);
INSERT INTO iteminfo_data VALUES (1243, 'price_logic_max_1991', '1356', 29);
INSERT INTO iteminfo_data VALUES (1244, 'price_fraud_1992', '478', 29);
INSERT INTO iteminfo_data VALUES (1245, 'price_logic_min_1992', '764.8', 29);
INSERT INTO iteminfo_data VALUES (1246, 'price_logic_max_1992', '1912', 29);
INSERT INTO iteminfo_data VALUES (1247, 'price_fraud_1993', '470', 29);
INSERT INTO iteminfo_data VALUES (1248, 'price_logic_min_1993', '752', 29);
INSERT INTO iteminfo_data VALUES (1249, 'price_logic_max_1993', '1880', 29);
INSERT INTO iteminfo_data VALUES (1250, 'price_fraud_1994', '692.5', 29);
INSERT INTO iteminfo_data VALUES (1251, 'price_logic_min_1994', '1108', 29);
INSERT INTO iteminfo_data VALUES (1252, 'price_logic_max_1994', '2770', 29);
INSERT INTO iteminfo_data VALUES (1253, 'price_fraud_1995', '758.5', 29);
INSERT INTO iteminfo_data VALUES (1254, 'price_logic_min_1995', '1213.6', 29);
INSERT INTO iteminfo_data VALUES (1255, 'price_logic_max_1995', '3034', 29);
INSERT INTO iteminfo_data VALUES (1256, 'price_fraud_1996', '837', 29);
INSERT INTO iteminfo_data VALUES (1257, 'price_logic_min_1996', '1339.2', 29);
INSERT INTO iteminfo_data VALUES (1258, 'price_logic_max_1996', '3348', 29);
INSERT INTO iteminfo_data VALUES (1259, 'price_fraud_1997', '1160', 29);
INSERT INTO iteminfo_data VALUES (1260, 'price_logic_min_1997', '1856', 29);
INSERT INTO iteminfo_data VALUES (1261, 'price_logic_max_1997', '4640', 29);
INSERT INTO iteminfo_data VALUES (1262, 'price_fraud_1998', '1414.5', 29);
INSERT INTO iteminfo_data VALUES (1263, 'price_logic_min_1998', '2263.2', 29);
INSERT INTO iteminfo_data VALUES (1264, 'price_logic_max_1998', '5658', 29);
INSERT INTO iteminfo_data VALUES (1265, 'price_fraud_1999', '1672.5', 29);
INSERT INTO iteminfo_data VALUES (1266, 'price_logic_min_1999', '2676', 29);
INSERT INTO iteminfo_data VALUES (1267, 'price_logic_max_1999', '6690', 29);
INSERT INTO iteminfo_data VALUES (1268, 'price_fraud_2000', '2244.5', 29);
INSERT INTO iteminfo_data VALUES (1269, 'price_logic_min_2000', '3591.2', 29);
INSERT INTO iteminfo_data VALUES (1270, 'price_logic_max_2000', '8978', 29);
INSERT INTO iteminfo_data VALUES (1271, 'price_fraud_2001', '2835', 29);
INSERT INTO iteminfo_data VALUES (1272, 'price_logic_min_2001', '4536', 29);
INSERT INTO iteminfo_data VALUES (1273, 'price_logic_max_2001', '11340', 29);
INSERT INTO iteminfo_data VALUES (1274, 'price_fraud_2002', '3760', 29);
INSERT INTO iteminfo_data VALUES (1275, 'price_logic_min_2002', '6016', 29);
INSERT INTO iteminfo_data VALUES (1276, 'price_logic_max_2002', '15040', 29);
INSERT INTO iteminfo_data VALUES (1277, 'price_fraud_2003', '4309.5', 29);
INSERT INTO iteminfo_data VALUES (1278, 'price_logic_min_2003', '6895.2', 29);
INSERT INTO iteminfo_data VALUES (1279, 'price_logic_max_2003', '17238', 29);
INSERT INTO iteminfo_data VALUES (1280, 'price_fraud_2004', '4872.5', 29);
INSERT INTO iteminfo_data VALUES (1281, 'price_logic_min_2004', '7796', 29);
INSERT INTO iteminfo_data VALUES (1282, 'price_logic_max_2004', '19490', 29);
INSERT INTO iteminfo_data VALUES (1283, 'price_fraud_2005', '5702.5', 29);
INSERT INTO iteminfo_data VALUES (1284, 'price_logic_min_2005', '9124', 29);
INSERT INTO iteminfo_data VALUES (1285, 'price_logic_max_2005', '22810', 29);
INSERT INTO iteminfo_data VALUES (1286, 'price_fraud_2006', '6530', 29);
INSERT INTO iteminfo_data VALUES (1287, 'price_logic_min_2006', '10448', 29);
INSERT INTO iteminfo_data VALUES (1288, 'price_logic_max_2006', '26120', 29);
INSERT INTO iteminfo_data VALUES (1289, 'price_fraud_2007', '7412.5', 29);
INSERT INTO iteminfo_data VALUES (1290, 'price_logic_min_2007', '11860', 29);
INSERT INTO iteminfo_data VALUES (1291, 'price_logic_max_2007', '29650', 29);
INSERT INTO iteminfo_data VALUES (1292, 'price_fraud_2008', '8648', 29);
INSERT INTO iteminfo_data VALUES (1293, 'price_logic_min_2008', '13836.8', 29);
INSERT INTO iteminfo_data VALUES (1294, 'price_logic_max_2008', '34592', 29);
INSERT INTO iteminfo_data VALUES (10001, 'name', 'rhone alpes', 2001);
INSERT INTO iteminfo_data VALUES (10002, 'name', 'ain', 2002);
INSERT INTO iteminfo_data VALUES (10003, 'name', 'ile de france', 2006);
INSERT INTO iteminfo_data VALUES (10004, 'name', 'paris', 2007);
INSERT INTO iteminfo_data VALUES (10005, 'is_coast', '0', 2001);
INSERT INTO iteminfo_data VALUES (10006, 'is_coast', '0', 2006);
INSERT INTO iteminfo_data VALUES (10007, 'is_mountain', '0', 2001);
INSERT INTO iteminfo_data VALUES (10008, 'is_mountain', '1', 2006);
INSERT INTO iteminfo_data VALUES (10009, 'price_fraud_cat_1080_s', '738.23986261018', 2001);
INSERT INTO iteminfo_data VALUES (10010, 'price_logic_min_cat_1080_s', '1181.1837801763', 2001);
INSERT INTO iteminfo_data VALUES (10011, 'price_logic_max_cat_1080_s', '2952.9594504407', 2001);
INSERT INTO iteminfo_data VALUES (10012, 'price_fraud_cat_1080_s', '739.23986261018', 2006);
INSERT INTO iteminfo_data VALUES (10013, 'price_logic_min_cat_1080_s', '1182.1837801763', 2006);
INSERT INTO iteminfo_data VALUES (10014, 'price_logic_max_cat_1080_s', '2953.9594504407', 2006);
INSERT INTO iteminfo_data VALUES (10015, 'price_fraud_cat_1020_s', '1154.3440838249', 2001);
INSERT INTO iteminfo_data VALUES (10016, 'price_logic_min_cat_1020_s', '1846.9505341198', 2001);
INSERT INTO iteminfo_data VALUES (10017, 'price_logic_max_cat_1020_s', '4617.3763352994', 2001);
INSERT INTO iteminfo_data VALUES (10018, 'price_fraud_cat_1020_s', '1155.3440838249', 2006);
INSERT INTO iteminfo_data VALUES (10019, 'price_logic_min_cat_1020_s', '1847.9505341198', 2006);
INSERT INTO iteminfo_data VALUES (10020, 'price_logic_max_cat_1020_s', '4618.3763352994', 2006);
INSERT INTO iteminfo_data VALUES (10021, 'price_fraud_cat_10_s', '3', 2001);
INSERT INTO iteminfo_data VALUES (10022, 'price_logic_min_cat_10_s', '4.8', 2001);
INSERT INTO iteminfo_data VALUES (10023, 'price_logic_max_cat_10_s', '12', 2001);
INSERT INTO iteminfo_data VALUES (10024, 'price_fraud_cat_10_s', '4', 2006);
INSERT INTO iteminfo_data VALUES (10025, 'price_logic_min_cat_10_s', '5.8', 2006);
INSERT INTO iteminfo_data VALUES (10026, 'price_logic_max_cat_10_s', '13', 2006);
INSERT INTO iteminfo_data VALUES (10027, 'price_fraud_cat_11_s', '1.5', 2001);
INSERT INTO iteminfo_data VALUES (10028, 'price_logic_min_cat_11_s', '2.4', 2001);
INSERT INTO iteminfo_data VALUES (10029, 'price_logic_max_cat_11_s', '6', 2001);
INSERT INTO iteminfo_data VALUES (10030, 'price_fraud_cat_11_s', '2.5', 2006);
INSERT INTO iteminfo_data VALUES (10031, 'price_logic_min_cat_11_s', '3.4', 2006);
INSERT INTO iteminfo_data VALUES (10032, 'price_logic_max_cat_11_s', '7', 2006);
INSERT INTO iteminfo_data VALUES (10033, 'price_fraud_cat_12_s', '1.8', 2001);
INSERT INTO iteminfo_data VALUES (10034, 'price_logic_min_cat_12_s', '2.88', 2001);
INSERT INTO iteminfo_data VALUES (10035, 'price_logic_max_cat_12_s', '7.2', 2001);
INSERT INTO iteminfo_data VALUES (10036, 'price_fraud_cat_12_s', '2.8', 2006);
INSERT INTO iteminfo_data VALUES (10037, 'price_logic_min_cat_12_s', '3.88', 2006);
INSERT INTO iteminfo_data VALUES (10038, 'price_logic_max_cat_12_s', '8.2', 2006);


--
-- Data for Name: mail_log; Type: TABLE DATA; Schema: public; Owner: dany
--



--
-- Data for Name: mail_queue; Type: TABLE DATA; Schema: public; Owner: dany
--



--
-- Data for Name: mama_attribute_wordlists; Type: TABLE DATA; Schema: public; Owner: dany
--



--
-- Data for Name: mama_attribute_categories; Type: TABLE DATA; Schema: public; Owner: dany
--



--
-- Data for Name: mama_main_backup; Type: TABLE DATA; Schema: public; Owner: dany
--



--
-- Data for Name: mama_attribute_categories_backup; Type: TABLE DATA; Schema: public; Owner: dany
--



--
-- Data for Name: mama_attribute_wordlists_backup; Type: TABLE DATA; Schema: public; Owner: dany
--



--
-- Data for Name: mama_attribute_words; Type: TABLE DATA; Schema: public; Owner: dany
--



--
-- Data for Name: mama_attribute_words_backup; Type: TABLE DATA; Schema: public; Owner: dany
--



--
-- Data for Name: mama_exception_lists; Type: TABLE DATA; Schema: public; Owner: dany
--



--
-- Data for Name: mama_exception_lists_backup; Type: TABLE DATA; Schema: public; Owner: dany
--



--
-- Data for Name: mama_exception_words; Type: TABLE DATA; Schema: public; Owner: dany
--



--
-- Data for Name: mama_exception_words_backup; Type: TABLE DATA; Schema: public; Owner: dany
--



--
-- Data for Name: mama_wordlists; Type: TABLE DATA; Schema: public; Owner: dany
--



--
-- Data for Name: mama_wordlists_backup; Type: TABLE DATA; Schema: public; Owner: dany
--



--
-- Data for Name: mama_words; Type: TABLE DATA; Schema: public; Owner: dany
--



--
-- Data for Name: mama_words_backup; Type: TABLE DATA; Schema: public; Owner: dany
--



--
-- Data for Name: most_popular_ads; Type: TABLE DATA; Schema: public; Owner: dany
--



--
-- Data for Name: notices; Type: TABLE DATA; Schema: public; Owner: dany
--



--
-- Data for Name: on_call; Type: TABLE DATA; Schema: public; Owner: dany
--



--
-- Data for Name: on_call_actions; Type: TABLE DATA; Schema: public; Owner: dany
--



--
-- Data for Name: pageviews_per_reg_cat; Type: TABLE DATA; Schema: public; Owner: dany
--



--
-- Data for Name: pay_log; Type: TABLE DATA; Schema: public; Owner: dany
--

INSERT INTO pay_log VALUES (0, 'adminclear', 0, '59876', 99999917, '2006-04-06 15:19:46', 50, 'OK');
INSERT INTO pay_log VALUES (1, 'save', 0, '112200000', NULL, '2012-11-07 18:32:17', 60, 'SAVE');
INSERT INTO pay_log VALUES (2, 'verify', 0, '112200000', NULL, '2012-11-07 18:32:18', 60, 'OK');
INSERT INTO pay_log VALUES (3, 'save', 0, '112200001', NULL, '2012-11-07 18:33:21', 61, 'SAVE');
INSERT INTO pay_log VALUES (4, 'verify', 0, '112200001', NULL, '2012-11-07 18:33:21', 61, 'OK');
INSERT INTO pay_log VALUES (5, 'save', 0, '112200002', NULL, '2012-11-07 18:34:00', 62, 'SAVE');
INSERT INTO pay_log VALUES (6, 'verify', 0, '112200002', NULL, '2012-11-07 18:34:00', 62, 'OK');
INSERT INTO pay_log VALUES (7, 'save', 0, '112200003', NULL, '2012-11-07 18:34:57', 63, 'SAVE');
INSERT INTO pay_log VALUES (8, 'verify', 0, '112200003', NULL, '2012-11-07 18:34:57', 63, 'OK');
INSERT INTO pay_log VALUES (9, 'save', 0, '112200004', NULL, '2012-11-07 18:35:39', 64, 'SAVE');
INSERT INTO pay_log VALUES (10, 'verify', 0, '112200004', NULL, '2012-11-07 18:35:39', 64, 'OK');
INSERT INTO pay_log VALUES (11, 'save', 0, '112200005', NULL, '2012-11-07 18:36:53', 65, 'SAVE');
INSERT INTO pay_log VALUES (12, 'verify', 0, '112200005', NULL, '2012-11-07 18:36:53', 65, 'OK');
INSERT INTO pay_log VALUES (13, 'save', 0, '112200006', NULL, '2012-11-07 18:37:14', 66, 'SAVE');
INSERT INTO pay_log VALUES (14, 'verify', 0, '112200006', NULL, '2012-11-07 18:37:14', 66, 'OK');
INSERT INTO pay_log VALUES (15, 'save', 0, '112200007', NULL, '2012-11-07 18:38:12', 67, 'SAVE');
INSERT INTO pay_log VALUES (16, 'verify', 0, '112200007', NULL, '2012-11-07 18:38:12', 67, 'OK');
INSERT INTO pay_log VALUES (17, 'save', 0, '112200008', NULL, '2012-11-07 18:39:18', 68, 'SAVE');
INSERT INTO pay_log VALUES (18, 'verify', 0, '112200008', NULL, '2012-11-07 18:39:18', 68, 'OK');
INSERT INTO pay_log VALUES (19, 'save', 0, '112200009', NULL, '2012-11-07 18:40:28', 69, 'SAVE');
INSERT INTO pay_log VALUES (20, 'verify', 0, '112200009', NULL, '2012-11-07 18:40:28', 69, 'OK');
INSERT INTO pay_log VALUES (21, 'save', 0, '235392023', NULL, '2012-11-07 18:40:38', 70, 'SAVE');
INSERT INTO pay_log VALUES (22, 'verify', 0, '235392023', NULL, '2012-11-07 18:40:39', 70, 'OK');
INSERT INTO pay_log VALUES (23, 'save', 0, '370784046', NULL, '2012-11-07 18:41:25', 71, 'SAVE');
INSERT INTO pay_log VALUES (24, 'verify', 0, '370784046', NULL, '2012-11-07 18:41:25', 71, 'OK');
INSERT INTO pay_log VALUES (25, 'save', 0, '506176069', NULL, '2012-11-07 18:42:06', 72, 'SAVE');
INSERT INTO pay_log VALUES (26, 'verify', 0, '506176069', NULL, '2012-11-07 18:42:06', 72, 'OK');
INSERT INTO pay_log VALUES (27, 'save', 0, '641568092', NULL, '2012-11-07 18:42:20', 73, 'SAVE');
INSERT INTO pay_log VALUES (28, 'verify', 0, '641568092', NULL, '2012-11-07 18:42:20', 73, 'OK');
INSERT INTO pay_log VALUES (29, 'save', 0, '776960115', NULL, '2012-11-07 18:43:22', 74, 'SAVE');
INSERT INTO pay_log VALUES (30, 'verify', 0, '776960115', NULL, '2012-11-07 18:43:23', 74, 'OK');
INSERT INTO pay_log VALUES (31, 'save', 0, '912352138', NULL, '2012-11-07 18:43:48', 75, 'SAVE');
INSERT INTO pay_log VALUES (32, 'verify', 0, '912352138', NULL, '2012-11-07 18:43:48', 75, 'OK');


--
-- Data for Name: pay_log_references; Type: TABLE DATA; Schema: public; Owner: dany
--

INSERT INTO pay_log_references VALUES (0, 0, 'remote_addr', '192.168.4.75');
INSERT INTO pay_log_references VALUES (1, 0, 'adphone', '1234567890');
INSERT INTO pay_log_references VALUES (1, 1, 'remote_addr', '10.0.1.24');
INSERT INTO pay_log_references VALUES (2, 0, 'remote_addr', '10.0.1.24');
INSERT INTO pay_log_references VALUES (3, 0, 'adphone', '123123123');
INSERT INTO pay_log_references VALUES (3, 1, 'remote_addr', '10.0.1.53');
INSERT INTO pay_log_references VALUES (4, 0, 'remote_addr', '10.0.1.53');
INSERT INTO pay_log_references VALUES (5, 0, 'adphone', '1234567890');
INSERT INTO pay_log_references VALUES (5, 1, 'remote_addr', '10.0.1.24');
INSERT INTO pay_log_references VALUES (6, 0, 'remote_addr', '10.0.1.24');
INSERT INTO pay_log_references VALUES (7, 0, 'adphone', '123123123');
INSERT INTO pay_log_references VALUES (7, 1, 'remote_addr', '10.0.1.53');
INSERT INTO pay_log_references VALUES (8, 0, 'remote_addr', '10.0.1.53');
INSERT INTO pay_log_references VALUES (9, 0, 'adphone', '1234567890');
INSERT INTO pay_log_references VALUES (9, 1, 'remote_addr', '10.0.1.24');
INSERT INTO pay_log_references VALUES (10, 0, 'remote_addr', '10.0.1.24');
INSERT INTO pay_log_references VALUES (11, 0, 'adphone', '123123123');
INSERT INTO pay_log_references VALUES (11, 1, 'remote_addr', '10.0.1.53');
INSERT INTO pay_log_references VALUES (12, 0, 'remote_addr', '10.0.1.53');
INSERT INTO pay_log_references VALUES (13, 0, 'adphone', '1234567890');
INSERT INTO pay_log_references VALUES (13, 1, 'remote_addr', '10.0.1.24');
INSERT INTO pay_log_references VALUES (14, 0, 'remote_addr', '10.0.1.24');
INSERT INTO pay_log_references VALUES (15, 0, 'adphone', '123123123');
INSERT INTO pay_log_references VALUES (15, 1, 'remote_addr', '10.0.1.53');
INSERT INTO pay_log_references VALUES (16, 0, 'remote_addr', '10.0.1.53');
INSERT INTO pay_log_references VALUES (17, 0, 'adphone', '123123123');
INSERT INTO pay_log_references VALUES (17, 1, 'remote_addr', '10.0.1.53');
INSERT INTO pay_log_references VALUES (18, 0, 'remote_addr', '10.0.1.53');
INSERT INTO pay_log_references VALUES (19, 0, 'adphone', '1234567890');
INSERT INTO pay_log_references VALUES (19, 1, 'remote_addr', '10.0.1.24');
INSERT INTO pay_log_references VALUES (20, 0, 'remote_addr', '10.0.1.24');
INSERT INTO pay_log_references VALUES (21, 0, 'adphone', '123123123');
INSERT INTO pay_log_references VALUES (21, 1, 'remote_addr', '10.0.1.53');
INSERT INTO pay_log_references VALUES (22, 0, 'remote_addr', '10.0.1.53');
INSERT INTO pay_log_references VALUES (23, 0, 'adphone', '1234567890');
INSERT INTO pay_log_references VALUES (23, 1, 'remote_addr', '10.0.1.24');
INSERT INTO pay_log_references VALUES (24, 0, 'remote_addr', '10.0.1.24');
INSERT INTO pay_log_references VALUES (25, 0, 'adphone', '1234567890');
INSERT INTO pay_log_references VALUES (25, 1, 'remote_addr', '10.0.1.24');
INSERT INTO pay_log_references VALUES (26, 0, 'remote_addr', '10.0.1.24');
INSERT INTO pay_log_references VALUES (27, 0, 'adphone', '123123123');
INSERT INTO pay_log_references VALUES (27, 1, 'remote_addr', '10.0.1.53');
INSERT INTO pay_log_references VALUES (28, 0, 'remote_addr', '10.0.1.53');
INSERT INTO pay_log_references VALUES (29, 0, 'adphone', '123123123');
INSERT INTO pay_log_references VALUES (29, 1, 'remote_addr', '10.0.1.53');
INSERT INTO pay_log_references VALUES (30, 0, 'remote_addr', '10.0.1.53');
INSERT INTO pay_log_references VALUES (31, 0, 'adphone', '1234567890');
INSERT INTO pay_log_references VALUES (31, 1, 'remote_addr', '10.0.1.24');
INSERT INTO pay_log_references VALUES (32, 0, 'remote_addr', '10.0.1.24');


--
-- Data for Name: payments; Type: TABLE DATA; Schema: public; Owner: dany
--

INSERT INTO payments VALUES (0, 'ad_action', 20, 0);
INSERT INTO payments VALUES (1, 'ad_action', 20, 0);
INSERT INTO payments VALUES (2, 'ad_action', 20, 0);
INSERT INTO payments VALUES (3, 'ad_action', 20, 0);
INSERT INTO payments VALUES (50, 'ad_action', 20, 0);
INSERT INTO payments VALUES (60, 'ad_action', 0, 0);
INSERT INTO payments VALUES (61, 'ad_action', 0, 0);
INSERT INTO payments VALUES (62, 'ad_action', 0, 0);
INSERT INTO payments VALUES (63, 'ad_action', 0, 0);
INSERT INTO payments VALUES (64, 'ad_action', 0, 0);
INSERT INTO payments VALUES (65, 'ad_action', 0, 0);
INSERT INTO payments VALUES (66, 'ad_action', 0, 0);
INSERT INTO payments VALUES (67, 'ad_action', 0, 0);
INSERT INTO payments VALUES (68, 'ad_action', 0, 0);
INSERT INTO payments VALUES (69, 'ad_action', 0, 0);
INSERT INTO payments VALUES (70, 'ad_action', 0, 0);
INSERT INTO payments VALUES (71, 'ad_action', 0, 0);
INSERT INTO payments VALUES (72, 'ad_action', 0, 0);
INSERT INTO payments VALUES (73, 'ad_action', 0, 0);
INSERT INTO payments VALUES (74, 'ad_action', 0, 0);
INSERT INTO payments VALUES (75, 'ad_action', 0, 0);


--
-- Data for Name: pricelist; Type: TABLE DATA; Schema: public; Owner: dany
--

INSERT INTO pricelist VALUES ('if', 'BL1', 1295);
INSERT INTO pricelist VALUES ('if', 'BL2', 1295);
INSERT INTO pricelist VALUES ('if', 'BL3', 995);
INSERT INTO pricelist VALUES ('if', 'BL4', 995);


--
-- Data for Name: redir_stats; Type: TABLE DATA; Schema: public; Owner: dany
--



--
-- Data for Name: review_log; Type: TABLE DATA; Schema: public; Owner: dany
--

INSERT INTO review_log VALUES (20, 1, 9, '2012-11-07 18:44:32.076978', 'normal', 'new', 'accepted', NULL, 8020);
INSERT INTO review_log VALUES (21, 1, 9, '2012-11-07 18:44:34.906444', 'normal', 'new', 'accepted', NULL, 1020);
INSERT INTO review_log VALUES (22, 1, 9, '2012-11-07 18:44:41.984225', 'normal', 'new', 'accepted', NULL, 7060);
INSERT INTO review_log VALUES (23, 1, 9, '2012-11-07 18:44:45.671224', 'normal', 'new', 'accepted', NULL, 1040);
INSERT INTO review_log VALUES (24, 1, 9, '2012-11-07 18:44:52.931095', 'normal', 'new', 'accepted', NULL, 7040);
INSERT INTO review_log VALUES (25, 1, 9, '2012-11-07 18:44:57.022859', 'normal', 'new', 'accepted', NULL, 1060);
INSERT INTO review_log VALUES (26, 1, 9, '2012-11-07 18:45:02.182113', 'normal', 'new', 'accepted', NULL, 7020);
INSERT INTO review_log VALUES (27, 1, 9, '2012-11-07 18:45:05.171239', 'normal', 'new', 'accepted', NULL, 1080);
INSERT INTO review_log VALUES (28, 1, 9, '2012-11-07 18:45:11.856153', 'normal', 'new', 'accepted', NULL, 1100);
INSERT INTO review_log VALUES (29, 1, 9, '2012-11-07 18:45:18.562067', 'normal', 'new', 'accepted', NULL, 5140);
INSERT INTO review_log VALUES (30, 1, 9, '2012-11-07 18:45:25.116642', 'normal', 'new', 'accepted', NULL, 1120);
INSERT INTO review_log VALUES (31, 1, 9, '2012-11-07 18:45:29.301937', 'normal', 'new', 'accepted', NULL, 5120);
INSERT INTO review_log VALUES (32, 1, 9, '2012-11-07 18:45:37.462247', 'normal', 'new', 'accepted', NULL, 5100);
INSERT INTO review_log VALUES (33, 1, 9, '2012-11-07 18:45:44.356053', 'normal', 'new', 'accepted', NULL, 2020);
INSERT INTO review_log VALUES (34, 1, 9, '2012-11-07 18:45:49.769035', 'normal', 'new', 'accepted', NULL, 2060);
INSERT INTO review_log VALUES (35, 1, 9, '2012-11-07 18:45:55.99175', 'normal', 'new', 'accepted', NULL, 2040);


--
-- Data for Name: sms_users; Type: TABLE DATA; Schema: public; Owner: dany
--



--
-- Data for Name: sms_log; Type: TABLE DATA; Schema: public; Owner: dany
--



--
-- Data for Name: watch_users; Type: TABLE DATA; Schema: public; Owner: dany
--



--
-- Data for Name: watch_queries; Type: TABLE DATA; Schema: public; Owner: dany
--



--
-- Data for Name: sms_log_watch; Type: TABLE DATA; Schema: public; Owner: dany
--



--
-- Data for Name: state_params; Type: TABLE DATA; Schema: public; Owner: dany
--

INSERT INTO state_params VALUES (20, 1, 250, 'filter_name', 'normal');
INSERT INTO state_params VALUES (21, 1, 251, 'filter_name', 'normal');
INSERT INTO state_params VALUES (22, 1, 254, 'filter_name', 'normal');
INSERT INTO state_params VALUES (23, 1, 255, 'filter_name', 'normal');
INSERT INTO state_params VALUES (24, 1, 258, 'filter_name', 'normal');
INSERT INTO state_params VALUES (25, 1, 259, 'filter_name', 'normal');
INSERT INTO state_params VALUES (26, 1, 262, 'filter_name', 'normal');
INSERT INTO state_params VALUES (27, 1, 263, 'filter_name', 'normal');
INSERT INTO state_params VALUES (28, 1, 266, 'filter_name', 'normal');
INSERT INTO state_params VALUES (29, 1, 267, 'filter_name', 'normal');
INSERT INTO state_params VALUES (30, 1, 270, 'filter_name', 'normal');
INSERT INTO state_params VALUES (31, 1, 271, 'filter_name', 'normal');
INSERT INTO state_params VALUES (32, 1, 274, 'filter_name', 'normal');
INSERT INTO state_params VALUES (33, 1, 275, 'filter_name', 'normal');
INSERT INTO state_params VALUES (34, 1, 278, 'filter_name', 'normal');
INSERT INTO state_params VALUES (35, 1, 279, 'filter_name', 'normal');


--
-- Data for Name: stats_daily; Type: TABLE DATA; Schema: public; Owner: dany
--



--
-- Data for Name: stats_daily_ad_actions; Type: TABLE DATA; Schema: public; Owner: dany
--



--
-- Data for Name: stats_hourly; Type: TABLE DATA; Schema: public; Owner: dany
--



--
-- Data for Name: store_actions; Type: TABLE DATA; Schema: public; Owner: dany
--



--
-- Data for Name: store_action_states; Type: TABLE DATA; Schema: public; Owner: dany
--



--
-- Data for Name: store_changes; Type: TABLE DATA; Schema: public; Owner: dany
--



--
-- Data for Name: store_login_tokens; Type: TABLE DATA; Schema: public; Owner: dany
--



--
-- Data for Name: store_params; Type: TABLE DATA; Schema: public; Owner: dany
--



--
-- Data for Name: synonyms; Type: TABLE DATA; Schema: public; Owner: dany
--

INSERT INTO synonyms VALUES (1, 3040, 'armoire', NULL);
INSERT INTO synonyms VALUES (2, 3040, 'armoires ', 1);
INSERT INTO synonyms VALUES (3, 3040, 'banc', NULL);
INSERT INTO synonyms VALUES (4, 3040, 'bancs ', 3);
INSERT INTO synonyms VALUES (5, 3040, 'banquette ', NULL);
INSERT INTO synonyms VALUES (6, 3040, 'banquettes ', 5);
INSERT INTO synonyms VALUES (7, 3040, 'bergere', NULL);
INSERT INTO synonyms VALUES (8, 3040, 'bergeres', 7);
INSERT INTO synonyms VALUES (9, 3040, 'bibliotheque ', NULL);
INSERT INTO synonyms VALUES (10, 3040, 'biblioteque', 9);
INSERT INTO synonyms VALUES (11, 3040, 'bibliotheques', 9);
INSERT INTO synonyms VALUES (12, 3040, 'buffet ', NULL);
INSERT INTO synonyms VALUES (13, 3040, 'bufet', 12);
INSERT INTO synonyms VALUES (14, 3040, 'buffets', 12);
INSERT INTO synonyms VALUES (15, 3040, 'bureau ', NULL);
INSERT INTO synonyms VALUES (16, 3040, 'bureaux ', 15);
INSERT INTO synonyms VALUES (17, 3040, 'canape', NULL);
INSERT INTO synonyms VALUES (18, 3040, 'canaper ', 17);
INSERT INTO synonyms VALUES (19, 3040, 'canapes ', 17);
INSERT INTO synonyms VALUES (20, 3040, 'canapee', 17);
INSERT INTO synonyms VALUES (21, 3040, 'cannape', 17);
INSERT INTO synonyms VALUES (22, 3040, 'chaise ', NULL);
INSERT INTO synonyms VALUES (23, 3040, 'chaises ', 22);
INSERT INTO synonyms VALUES (24, 3040, 'chevet ', NULL);
INSERT INTO synonyms VALUES (25, 3040, 'chevets ', 24);
INSERT INTO synonyms VALUES (26, 3040, 'clic clac', NULL);
INSERT INTO synonyms VALUES (27, 3040, 'clic-clac', 26);
INSERT INTO synonyms VALUES (28, 3040, 'cliclac', 26);
INSERT INTO synonyms VALUES (29, 3040, 'commode ', NULL);
INSERT INTO synonyms VALUES (30, 3040, 'commodes ', 29);
INSERT INTO synonyms VALUES (31, 3040, 'comode', 29);
INSERT INTO synonyms VALUES (32, 3040, 'etagere ', NULL);
INSERT INTO synonyms VALUES (33, 3040, 'etageres ', 32);
INSERT INTO synonyms VALUES (34, 3040, 'fauteuil ', NULL);
INSERT INTO synonyms VALUES (35, 3040, 'fauteil ', 34);
INSERT INTO synonyms VALUES (36, 3040, 'fauteils ', 34);
INSERT INTO synonyms VALUES (37, 3040, 'fauteuille ', 34);
INSERT INTO synonyms VALUES (38, 3040, 'fauteuilles ', 34);
INSERT INTO synonyms VALUES (39, 3040, 'fauteuils ', 34);
INSERT INTO synonyms VALUES (40, 3040, 'matelas ', NULL);
INSERT INTO synonyms VALUES (41, 3040, 'matelat ', 40);
INSERT INTO synonyms VALUES (42, 3040, 'meuble ', NULL);
INSERT INTO synonyms VALUES (43, 3040, 'meubles ', 42);
INSERT INTO synonyms VALUES (44, 3040, 'mezzanine ', NULL);
INSERT INTO synonyms VALUES (45, 3040, 'mezanine ', 44);
INSERT INTO synonyms VALUES (46, 3040, 'porte-manteau', NULL);
INSERT INTO synonyms VALUES (47, 3040, 'porte-manteaux', 46);
INSERT INTO synonyms VALUES (48, 3040, 'porte manteau', 46);
INSERT INTO synonyms VALUES (49, 3040, 'porte manteaux', 46);
INSERT INTO synonyms VALUES (50, 3040, 'pouf', NULL);
INSERT INTO synonyms VALUES (51, 3040, 'poufs ', 50);
INSERT INTO synonyms VALUES (52, 3040, 'rangement ', NULL);
INSERT INTO synonyms VALUES (53, 3040, 'rangements ', 52);
INSERT INTO synonyms VALUES (54, 3040, 'rocking chair', NULL);
INSERT INTO synonyms VALUES (55, 3040, 'rocking-chair', 54);
INSERT INTO synonyms VALUES (56, 3040, 'siege ', NULL);
INSERT INTO synonyms VALUES (57, 3040, 'sieges', 56);
INSERT INTO synonyms VALUES (58, 3040, 'sommier ', NULL);
INSERT INTO synonyms VALUES (59, 3040, 'sommiers ', 58);
INSERT INTO synonyms VALUES (60, 3040, 'table ', NULL);
INSERT INTO synonyms VALUES (61, 3040, 'tables ', 60);
INSERT INTO synonyms VALUES (62, 3040, 'tabouret ', NULL);
INSERT INTO synonyms VALUES (63, 3040, 'tabourets ', 62);
INSERT INTO synonyms VALUES (64, 3040, 'tiroir ', NULL);
INSERT INTO synonyms VALUES (65, 3040, 'tiroirs ', 64);
INSERT INTO synonyms VALUES (66, 3040, 'vaisselier ', NULL);
INSERT INTO synonyms VALUES (67, 3040, 'vaissellier ', 66);
INSERT INTO synonyms VALUES (68, 3060, 'chauffe-eau', NULL);
INSERT INTO synonyms VALUES (69, 3060, 'chauffe eau', 68);
INSERT INTO synonyms VALUES (70, 3060, 'cuiseur', NULL);
INSERT INTO synonyms VALUES (71, 3060, 'cuisseur', 70);
INSERT INTO synonyms VALUES (72, 3060, 'cuit vapeur', NULL);
INSERT INTO synonyms VALUES (73, 3060, 'cuit-vapeur', 72);
INSERT INTO synonyms VALUES (74, 3060, 'gauffrer', NULL);
INSERT INTO synonyms VALUES (75, 3060, 'gauffrier', 74);
INSERT INTO synonyms VALUES (76, 3060, 'gaufrier', 74);
INSERT INTO synonyms VALUES (77, 3060, 'gaziniere', NULL);
INSERT INTO synonyms VALUES (78, 3060, 'gazinniere', 77);
INSERT INTO synonyms VALUES (79, 3060, 'grille-pain', NULL);
INSERT INTO synonyms VALUES (80, 3060, 'grille pain', 79);
INSERT INTO synonyms VALUES (81, 3060, 'lave linge', NULL);
INSERT INTO synonyms VALUES (82, 3060, 'lave-linge', 81);
INSERT INTO synonyms VALUES (83, 3060, 'lave vaisselle', NULL);
INSERT INTO synonyms VALUES (84, 3060, 'lave-vaisselle', 83);
INSERT INTO synonyms VALUES (85, 3060, 'micro onde', NULL);
INSERT INTO synonyms VALUES (86, 3060, 'micro ondes', 85);
INSERT INTO synonyms VALUES (87, 3060, 'micro-onde', 85);
INSERT INTO synonyms VALUES (88, 3060, 'micro-ondes', 85);
INSERT INTO synonyms VALUES (89, 3060, 'mixer', NULL);
INSERT INTO synonyms VALUES (90, 3060, 'mixeur', 89);
INSERT INTO synonyms VALUES (91, 3060, 'multifonction', NULL);
INSERT INTO synonyms VALUES (92, 3060, 'multifonctions', 91);
INSERT INTO synonyms VALUES (93, 3060, 'plaque a gaz', NULL);
INSERT INTO synonyms VALUES (94, 3060, 'plaque gaz', 93);
INSERT INTO synonyms VALUES (95, 3060, 'plaque de cuisson', NULL);
INSERT INTO synonyms VALUES (96, 3060, 'plaques de cuisson', 95);
INSERT INTO synonyms VALUES (97, 3060, 'plaque electrique', NULL);
INSERT INTO synonyms VALUES (98, 3060, 'plaques electriques', 97);
INSERT INTO synonyms VALUES (99, 3060, 'presse agrumes', NULL);
INSERT INTO synonyms VALUES (100, 3060, 'presse agrume', 99);
INSERT INTO synonyms VALUES (101, 3060, 'refrigerateur', NULL);
INSERT INTO synonyms VALUES (102, 3060, 'refrigirateur', 101);
INSERT INTO synonyms VALUES (103, 3060, 'refregirateur', 101);
INSERT INTO synonyms VALUES (104, 3060, 'seche cheveux', NULL);
INSERT INTO synonyms VALUES (105, 3060, 'seche-cheveux', 104);
INSERT INTO synonyms VALUES (106, 3060, 'seche linge', NULL);
INSERT INTO synonyms VALUES (107, 3060, 'seche-linge', 106);
INSERT INTO synonyms VALUES (108, 3060, 'table cuisson', NULL);
INSERT INTO synonyms VALUES (109, 3060, 'table de cuisson', 108);
INSERT INTO synonyms VALUES (110, 3060, 'vitroceramique', NULL);
INSERT INTO synonyms VALUES (111, 3060, 'vitro-ceramique', 110);
INSERT INTO synonyms VALUES (112, 3060, 'whirlpool', NULL);
INSERT INTO synonyms VALUES (113, 3060, 'whirpool', 112);
INSERT INTO synonyms VALUES (114, 3060, 'wirlpool', 112);
INSERT INTO synonyms VALUES (115, 8020, 'bouteille de gaz', NULL);
INSERT INTO synonyms VALUES (116, 8020, 'bouteille gaz', 115);
INSERT INTO synonyms VALUES (117, 8020, 'bouteilles de gaz', 115);
INSERT INTO synonyms VALUES (118, 8020, 'bouteilles gaz', 115);
INSERT INTO synonyms VALUES (119, 8020, 'brique', NULL);
INSERT INTO synonyms VALUES (120, 8020, 'briques', 119);
INSERT INTO synonyms VALUES (121, 8020, 'carrelage', NULL);
INSERT INTO synonyms VALUES (122, 8020, 'carrelages', 121);
INSERT INTO synonyms VALUES (123, 8020, 'casier a bouteille', NULL);
INSERT INTO synonyms VALUES (124, 8020, 'casier a bouteilles', 123);
INSERT INTO synonyms VALUES (125, 8020, 'chauffage', NULL);
INSERT INTO synonyms VALUES (126, 8020, 'chauffages', 125);
INSERT INTO synonyms VALUES (127, 8020, 'chauffe eau', NULL);
INSERT INTO synonyms VALUES (128, 8020, 'chauffe-eau', 127);
INSERT INTO synonyms VALUES (129, 8020, 'convecteur', NULL);
INSERT INTO synonyms VALUES (130, 8020, 'convecteurs', 129);
INSERT INTO synonyms VALUES (131, 8020, 'cuve', NULL);
INSERT INTO synonyms VALUES (132, 8020, 'cuves', 131);
INSERT INTO synonyms VALUES (133, 8020, 'disjoncteur', NULL);
INSERT INTO synonyms VALUES (134, 8020, 'disjoncteurs', 133);
INSERT INTO synonyms VALUES (135, 8020, 'fenetre', NULL);
INSERT INTO synonyms VALUES (136, 8020, 'fenetres', 135);
INSERT INTO synonyms VALUES (137, 8020, 'plante', NULL);
INSERT INTO synonyms VALUES (138, 8020, 'plantes', 137);
INSERT INTO synonyms VALUES (139, 8020, 'porte', NULL);
INSERT INTO synonyms VALUES (140, 8020, 'portes', 139);
INSERT INTO synonyms VALUES (141, 8020, 'radiateur', NULL);
INSERT INTO synonyms VALUES (142, 8020, 'radiateurs', 141);
INSERT INTO synonyms VALUES (143, 8020, 'remblai', NULL);
INSERT INTO synonyms VALUES (144, 8020, 'remblais', 143);
INSERT INTO synonyms VALUES (145, 8020, 'store', NULL);
INSERT INTO synonyms VALUES (146, 8020, 'stores', 145);
INSERT INTO synonyms VALUES (147, 8020, 'taille haie', NULL);
INSERT INTO synonyms VALUES (148, 8020, 'taille-haie', 147);
INSERT INTO synonyms VALUES (149, 8020, 'taille haies', 147);
INSERT INTO synonyms VALUES (150, 8020, 'taille-haies', 147);
INSERT INTO synonyms VALUES (151, 8020, 'tonneau', NULL);
INSERT INTO synonyms VALUES (152, 8020, 'tonneaux', 151);
INSERT INTO synonyms VALUES (153, 8020, 'tuile', NULL);
INSERT INTO synonyms VALUES (154, 8020, 'tuiles', 153);
INSERT INTO synonyms VALUES (155, 8020, 'tuyau', NULL);
INSERT INTO synonyms VALUES (156, 8020, 'tuyaux', 155);
INSERT INTO synonyms VALUES (157, 8020, 'vasque', NULL);
INSERT INTO synonyms VALUES (158, 8020, 'vasques', 157);
INSERT INTO synonyms VALUES (159, 8020, 'volet', NULL);
INSERT INTO synonyms VALUES (160, 8020, 'volets', 159);
INSERT INTO synonyms VALUES (161, 8060, 'biberon', NULL);
INSERT INTO synonyms VALUES (162, 8060, 'biberons', 161);
INSERT INTO synonyms VALUES (163, 8060, 'chaussure', NULL);
INSERT INTO synonyms VALUES (164, 8060, 'chaussures', 163);
INSERT INTO synonyms VALUES (165, 8060, 'ensemble fille', NULL);
INSERT INTO synonyms VALUES (166, 8060, 'ensemble filles', 165);
INSERT INTO synonyms VALUES (167, 8060, 'ensembles fille', 165);
INSERT INTO synonyms VALUES (168, 8060, 'ensembles filles', 165);
INSERT INTO synonyms VALUES (169, 8060, 'ensemble garcon', NULL);
INSERT INTO synonyms VALUES (170, 8060, 'ensemble garcons', 169);
INSERT INTO synonyms VALUES (171, 8060, 'ensembles garcon', 169);
INSERT INTO synonyms VALUES (172, 8060, 'ensembles garcons', 169);
INSERT INTO synonyms VALUES (173, 8060, 'gigoteuse', NULL);
INSERT INTO synonyms VALUES (174, 8060, 'gigoteuses', 173);
INSERT INTO synonyms VALUES (175, 8060, 'landau', NULL);
INSERT INTO synonyms VALUES (176, 8060, 'landeau', 175);
INSERT INTO synonyms VALUES (177, 8060, 'lit parapluie', NULL);
INSERT INTO synonyms VALUES (178, 8060, 'lit-parapluie', 177);
INSERT INTO synonyms VALUES (179, 8060, 'pantalon', NULL);
INSERT INTO synonyms VALUES (180, 8060, 'pantalons', 179);
INSERT INTO synonyms VALUES (181, 8060, 'porte bebe', NULL);
INSERT INTO synonyms VALUES (182, 8060, 'porte-bb', 181);
INSERT INTO synonyms VALUES (183, 8060, 'porte-bebe', 181);
INSERT INTO synonyms VALUES (184, 8060, 'poussette', NULL);
INSERT INTO synonyms VALUES (185, 8060, 'pousette', 184);
INSERT INTO synonyms VALUES (186, 8060, 'pyjama', NULL);
INSERT INTO synonyms VALUES (187, 8060, 'pyjamas', 186);
INSERT INTO synonyms VALUES (188, 8060, 'salopette', NULL);
INSERT INTO synonyms VALUES (189, 8060, 'salopettes', 188);
INSERT INTO synonyms VALUES (190, 8060, 'tee shirt', NULL);
INSERT INTO synonyms VALUES (191, 8060, 'tee-shirt', 190);
INSERT INTO synonyms VALUES (192, 8060, 'tshirt', 190);
INSERT INTO synonyms VALUES (193, 8060, 'vertbaudet', NULL);
INSERT INTO synonyms VALUES (194, 8060, 'vert baudet', 193);
INSERT INTO synonyms VALUES (195, 8060, 'verbaudet', 193);
INSERT INTO synonyms VALUES (196, 8060, 'vetement bebe', NULL);
INSERT INTO synonyms VALUES (197, 8060, 'vetements bebe', 196);
INSERT INTO synonyms VALUES (198, 8060, 'vetements enfant', NULL);
INSERT INTO synonyms VALUES (199, 8060, 'vetement enfant', 198);
INSERT INTO synonyms VALUES (200, 8060, 'vetements enfants', 198);
INSERT INTO synonyms VALUES (201, 8060, 'vetements fille', NULL);
INSERT INTO synonyms VALUES (202, 8060, 'vetements filles', 201);
INSERT INTO synonyms VALUES (203, 8060, 'vetements garcon', NULL);
INSERT INTO synonyms VALUES (204, 8060, 'vetements garcons', 203);
INSERT INTO synonyms VALUES (205, 41, 'babyfoot', NULL);
INSERT INTO synonyms VALUES (206, 41, 'baby-foot', 205);
INSERT INTO synonyms VALUES (207, 41, 'baby foot', 205);
INSERT INTO synonyms VALUES (208, 41, 'barbie', NULL);
INSERT INTO synonyms VALUES (209, 41, 'barbies', 208);
INSERT INTO synonyms VALUES (210, 41, 'camion de pompier', NULL);
INSERT INTO synonyms VALUES (211, 41, 'camion pompier', 210);
INSERT INTO synonyms VALUES (212, 41, 'domino', NULL);
INSERT INTO synonyms VALUES (213, 41, 'dominos', 212);
INSERT INTO synonyms VALUES (214, 41, 'dora', NULL);
INSERT INTO synonyms VALUES (215, 41, 'dora l''exploratrice', 214);
INSERT INTO synonyms VALUES (216, 41, 'figurine', NULL);
INSERT INTO synonyms VALUES (217, 41, 'figurines', 216);
INSERT INTO synonyms VALUES (218, 41, 'fisher price', NULL);
INSERT INTO synonyms VALUES (219, 41, 'fischer price', 218);
INSERT INTO synonyms VALUES (220, 41, 'fisherprice', 218);
INSERT INTO synonyms VALUES (221, 41, 'fisher-price', 218);
INSERT INTO synonyms VALUES (222, 41, 'jeu de construction', NULL);
INSERT INTO synonyms VALUES (223, 41, 'jeux de construction', 222);
INSERT INTO synonyms VALUES (224, 41, 'jeu de societe', NULL);
INSERT INTO synonyms VALUES (225, 41, 'jeux de societe', 224);
INSERT INTO synonyms VALUES (226, 41, 'jeu educatif', NULL);
INSERT INTO synonyms VALUES (227, 41, 'jeux educatifs', 226);
INSERT INTO synonyms VALUES (228, 41, 'jouet', NULL);
INSERT INTO synonyms VALUES (229, 41, 'jouets', 228);
INSERT INTO synonyms VALUES (230, 41, 'lego', NULL);
INSERT INTO synonyms VALUES (231, 41, 'legos', 230);
INSERT INTO synonyms VALUES (232, 41, 'peluche', NULL);
INSERT INTO synonyms VALUES (233, 41, 'pelluche', 232);
INSERT INTO synonyms VALUES (234, 41, 'peluches', 232);
INSERT INTO synonyms VALUES (235, 41, 'playmobil', NULL);
INSERT INTO synonyms VALUES (236, 41, 'playmobils', 235);
INSERT INTO synonyms VALUES (237, 41, 'playmobile', 235);
INSERT INTO synonyms VALUES (238, 41, 'poupee', NULL);
INSERT INTO synonyms VALUES (239, 41, 'poupees', 238);
INSERT INTO synonyms VALUES (240, 41, 'puzzle', NULL);
INSERT INTO synonyms VALUES (241, 41, 'puzzles', 240);
INSERT INTO synonyms VALUES (242, 41, 'trottinette', NULL);
INSERT INTO synonyms VALUES (243, 41, 'trotinette', 242);
INSERT INTO synonyms VALUES (244, 41, 'vtech', NULL);
INSERT INTO synonyms VALUES (245, 41, 'v-tech', 244);


--
-- Data for Name: trans_queue; Type: TABLE DATA; Schema: public; Owner: dany
--

INSERT INTO trans_queue VALUES (15, '2012-11-07 18:45:49.773264', '24344@tetsuo.schibsted.cl', NULL, '2012-11-07 18:46:12.510879', '24344@tetsuo.schibsted.cl', 'TRANS_OK', '2012-11-07 18:46:12.161997', NULL, 'cmd:admail
commit:1
ad_id:34
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue VALUES (16, '2012-11-07 18:45:55.994929', '24344@tetsuo.schibsted.cl', NULL, '2012-11-07 18:46:12.5195', '24344@tetsuo.schibsted.cl', 'TRANS_OK', '2012-11-07 18:46:12.161997', NULL, 'cmd:admail
commit:1
ad_id:35
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue VALUES (13, '2012-11-07 18:45:37.465544', '24344@tetsuo.schibsted.cl', NULL, '2012-11-07 18:46:12.520983', '24344@tetsuo.schibsted.cl', 'TRANS_OK', '2012-11-07 18:46:12.161997', NULL, 'cmd:admail
commit:1
ad_id:32
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue VALUES (14, '2012-11-07 18:45:44.359942', '24344@tetsuo.schibsted.cl', NULL, '2012-11-07 18:46:12.531677', '24344@tetsuo.schibsted.cl', 'TRANS_OK', '2012-11-07 18:46:12.161997', NULL, 'cmd:admail
commit:1
ad_id:33
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue VALUES (9, '2012-11-07 18:45:11.858753', '24344@tetsuo.schibsted.cl', NULL, '2012-11-07 18:46:12.532072', '24344@tetsuo.schibsted.cl', 'TRANS_OK', '2012-11-07 18:46:12.161997', NULL, 'cmd:admail
commit:1
ad_id:28
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue VALUES (12, '2012-11-07 18:45:29.30436', '24344@tetsuo.schibsted.cl', NULL, '2012-11-07 18:46:12.533473', '24344@tetsuo.schibsted.cl', 'TRANS_OK', '2012-11-07 18:46:12.161997', NULL, 'cmd:admail
commit:1
ad_id:31
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue VALUES (11, '2012-11-07 18:45:25.119431', '24344@tetsuo.schibsted.cl', NULL, '2012-11-07 18:46:12.533796', '24344@tetsuo.schibsted.cl', 'TRANS_OK', '2012-11-07 18:46:12.161997', NULL, 'cmd:admail
commit:1
ad_id:30
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue VALUES (10, '2012-11-07 18:45:18.565636', '24344@tetsuo.schibsted.cl', NULL, '2012-11-07 18:46:12.533959', '24344@tetsuo.schibsted.cl', 'TRANS_OK', '2012-11-07 18:46:12.161997', NULL, 'cmd:admail
commit:1
ad_id:29
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue VALUES (8, '2012-11-07 18:45:05.177711', '24344@tetsuo.schibsted.cl', NULL, '2012-11-07 18:46:12.534309', '24344@tetsuo.schibsted.cl', 'TRANS_OK', '2012-11-07 18:46:12.161997', NULL, 'cmd:admail
commit:1
ad_id:27
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue VALUES (7, '2012-11-07 18:45:02.184725', '24344@tetsuo.schibsted.cl', NULL, '2012-11-07 18:46:12.535115', '24344@tetsuo.schibsted.cl', 'TRANS_OK', '2012-11-07 18:46:12.161997', NULL, 'cmd:admail
commit:1
ad_id:26
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue VALUES (4, '2012-11-07 18:44:45.673851', '24344@tetsuo.schibsted.cl', NULL, '2012-11-07 18:46:12.718931', '24344@tetsuo.schibsted.cl', 'TRANS_OK', '2012-11-07 18:46:12.536039', NULL, 'cmd:admail
commit:1
ad_id:23
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue VALUES (1, '2012-11-07 18:44:32.089538', '24344@tetsuo.schibsted.cl', NULL, '2012-11-07 18:46:12.719538', '24344@tetsuo.schibsted.cl', 'TRANS_OK', '2012-11-07 18:46:12.536039', NULL, 'cmd:admail
commit:1
ad_id:20
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue VALUES (3, '2012-11-07 18:44:41.995131', '24344@tetsuo.schibsted.cl', NULL, '2012-11-07 18:46:12.71984', '24344@tetsuo.schibsted.cl', 'TRANS_OK', '2012-11-07 18:46:12.536039', NULL, 'cmd:admail
commit:1
ad_id:22
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue VALUES (5, '2012-11-07 18:44:52.934659', '24344@tetsuo.schibsted.cl', NULL, '2012-11-07 18:46:12.720046', '24344@tetsuo.schibsted.cl', 'TRANS_OK', '2012-11-07 18:46:12.536039', NULL, 'cmd:admail
commit:1
ad_id:24
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue VALUES (2, '2012-11-07 18:44:34.909303', '24344@tetsuo.schibsted.cl', NULL, '2012-11-07 18:46:12.720556', '24344@tetsuo.schibsted.cl', 'TRANS_OK', '2012-11-07 18:46:12.536039', NULL, 'cmd:admail
commit:1
ad_id:21
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue VALUES (6, '2012-11-07 18:44:57.025615', '24344@tetsuo.schibsted.cl', NULL, '2012-11-07 18:46:12.720705', '24344@tetsuo.schibsted.cl', 'TRANS_OK', '2012-11-07 18:46:12.536039', NULL, 'cmd:admail
commit:1
ad_id:25
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);


--
-- Data for Name: unfinished_ads; Type: TABLE DATA; Schema: public; Owner: dany
--

INSERT INTO unfinished_ads VALUES (1, 'mc1x829700d532d409fd06698a2f6b4e6362d30907da', 'boris@schibsted.cl', '2012-11-07 18:31:37.383321', '2012-11-07 18:32:17.691741', NULL, 'deleted', 'O:13:"blocket_newad":90:{s:2:"ad";O:3:"bAd":33:{s:6:"region";i:11;s:4:"area";N;s:12:"hide_address";N;s:10:"image_text";N;s:4:"type";s:1:"s";s:8:"category";i:8020;s:12:"sub_category";N;s:14:"category_group";i:8020;s:4:"name";s:13:"Boris Cruchet";s:5:"email";s:18:"boris@schibsted.cl";s:5:"phone";i:1234567890;s:12:"phone_hidden";N;s:10:"company_ad";s:1:"0";s:5:"store";N;s:7:"subject";s:13:"Otro producto";s:4:"body";s:31:"Aviso de otro producto para API";s:8:"infopage";N;s:14:"infopage_title";N;s:5:"price";i:1000;s:6:"passwd";s:62:"$1024$X9?$\\4&`FP/%gu#W81817697e047867ec3ac154e34203234774fe310";s:6:"coords";N;s:5:"state";N;s:6:"images";N;s:4:"lang";s:2:"es";s:16:"digests_presents";N;s:10:"thumbnails";N;s:17:"thumbnail_digests";N;s:7:"old_cat";N;s:4:"city";N;s:13:"prev_currency";s:4:"peso";s:25:"available_weeks_offseason";N;s:26:"available_weeks_peakseason";N;s:15:"available_weeks";N;}s:5:"ad_id";N;s:7:"list_id";N;s:6:"action";N;s:14:"refused_reason";N;s:20:"refused_refusal_text";N;s:25:"already_paid_extra_images";N;s:10:"company_ad";s:1:"0";s:4:"name";s:13:"Boris Cruchet";s:5:"email";s:18:"boris@schibsted.cl";s:5:"phone";s:10:"1234567890";s:12:"phone_hidden";N;s:7:"account";i:0;s:4:"paid";N;s:6:"images";a:1:{i:0;s:14:"5152331900.jpg";}s:6:"stores";N;s:12:"is_new_image";a:1:{i:0;s:1:"1";}s:16:"thumbnail_digest";a:1:{i:0;s:32:"0fbd6900d9f6a5a63cb4e159219fc506";}s:14:"digest_present";a:1:{i:0;s:1:"0";}s:9:"type_list";a:2:{i:0;s:1:"s";i:1;s:1:"k";}s:14:"orig_list_time";N;s:14:"refusal_edited";N;s:15:"no_footer_links";i:1;s:9:"action_id";N;s:22:"pre_refuse_action_type";N;s:18:"focus_extra_images";N;s:10:"loadaction";b:1;s:11:"has_gallery";N;s:11:"store_token";N;s:8:"store_id";N;s:8:"login_id";N;s:9:"addimages";N;s:8:"addvideo";N;s:8:"order_id";N;s:6:"params";s:19:"types,prev_currency";s:15:"red_arrow_price";N;s:7:"voucher";N;s:14:"voucher_result";N;s:8:"ad_price";s:1:"0";s:10:"area_unset";i:1;s:9:"link_type";N;s:12:"job_category";N;s:12:"service_type";N;s:9:"offseason";N;s:23:"offseason_checked_descr";N;s:10:"peakseason";N;s:24:"peakseason_checked_descr";N;s:15:"available_weeks";N;s:29:"available_weeks_checked_descr";N;s:13:"show_password";i:1;s:13:"salted_passwd";N;s:12:"hide_address";N;s:21:"allow_category_change";b:0;s:13:"video_enabled";i:0;s:10:"video_name";N;s:11:"video_width";N;s:12:"video_height";N;s:12:"video_length";N;s:11:"video_added";i:0;s:18:"already_paid_video";N;s:7:"set_uid";N;s:21:"add_gallery_from_mail";N;s:2:"ht";N;s:10:"del_reason";N;s:9:"user_text";N;s:21:"user_text_testimonial";N;s:9:"unf_ad_id";s:1:"1";s:14:"state_progress";s:7:"preview";s:12:"state_params";a:2:{s:2:"cl";b:0;s:2:"sp";N;}s:6:"caller";O:14:"blocket_caller":6:{s:6:"region";s:2:"15";s:4:"city";i:0;s:4:"type";s:1:"s";s:11:"default_set";b:1;s:10:"cookie_set";b:0;s:6:"layout";i:0;}s:13:"current_state";s:7:"preview";s:11:"instance_id";i:0;s:11:"static_vars";a:7:{i:0;s:10:"company_ad";i:1;s:4:"name";i:2;s:5:"email";i:3;s:5:"phone";i:4;s:12:"phone_hidden";i:5;s:4:"area";i:6;s:12:"hide_address";}s:13:"volatile_vars";a:12:{i:0;s:15:"no_footer_links";i:1;s:13:"video_enabled";i:2;s:11:"static_vars";i:3;s:13:"redirect_done";i:4;s:13:"volatile_vars";i:5;s:13:"current_state";i:6;s:11:"use_session";i:7;s:12:"state_params";i:8;s:14:"current_method";i:9;s:10:"navigation";i:10;s:9:"init_data";i:11;s:11:"extra_bconf";}s:13:"redirect_done";b:0;s:11:"use_session";b:1;s:6:"errors";a:1:{s:17:"accept_conditions";s:23:"ERROR_ACCEPT_CONDITIONS";}s:8:"warnings";a:0:{}s:8:"messages";N;s:16:"application_name";s:2:"ai";s:15:"single_instance";N;s:14:"current_method";s:3:"get";s:10:"navigation";N;s:9:"init_data";N;s:11:"extra_bconf";N;s:10:"headstyles";a:1:{i:0;s:6:"ai.css";}s:13:"ignore_fields";N;s:14:"show_body_warn";i:1;s:4:"area";N;s:7:"gallery";b:0;}');
INSERT INTO unfinished_ads VALUES (2, 'mc1x95c7a5a1041ea2970efbea1148471eb5a276e4fa', 'dany@schibsted.cl', '2012-11-07 18:32:53.318467', '2012-11-07 18:33:21.166515', NULL, 'deleted', 'O:13:"blocket_newad":90:{s:2:"ad";O:3:"bAd":39:{s:6:"region";i:11;s:4:"area";N;s:12:"hide_address";N;s:10:"image_text";N;s:4:"type";s:1:"s";s:8:"category";i:1020;s:12:"sub_category";N;s:14:"category_group";i:1020;s:4:"name";s:4:"dany";s:5:"email";s:17:"dany@schibsted.cl";s:5:"phone";i:123123123;s:12:"phone_hidden";N;s:10:"company_ad";s:1:"0";s:5:"store";N;s:7:"subject";s:24:"Departamento en Valdivia";s:4:"body";s:23:"Lindo depto en valdivia";s:8:"infopage";N;s:14:"infopage_title";N;s:5:"price";i:25000000;s:6:"passwd";N;s:6:"coords";N;s:5:"state";N;s:6:"images";N;s:4:"lang";s:2:"es";s:16:"digests_presents";N;s:10:"thumbnails";N;s:17:"thumbnail_digests";N;s:7:"old_cat";N;s:4:"city";N;s:13:"prev_currency";s:4:"peso";s:5:"rooms";i:2;s:4:"size";i:100;s:13:"garage_spaces";i:2;s:10:"condominio";i:50000;s:8:"communes";i:243;s:8:"currency";N;s:25:"available_weeks_offseason";N;s:26:"available_weeks_peakseason";N;s:15:"available_weeks";N;}s:5:"ad_id";N;s:7:"list_id";N;s:6:"action";N;s:14:"refused_reason";N;s:20:"refused_refusal_text";N;s:25:"already_paid_extra_images";N;s:10:"company_ad";s:1:"0";s:4:"name";s:4:"dany";s:5:"email";s:17:"dany@schibsted.cl";s:5:"phone";s:9:"123123123";s:12:"phone_hidden";N;s:7:"account";i:0;s:4:"paid";N;s:6:"images";a:0:{}s:6:"stores";N;s:12:"is_new_image";N;s:16:"thumbnail_digest";N;s:14:"digest_present";N;s:9:"type_list";a:4:{i:0;s:1:"s";i:1;s:1:"u";i:2;s:1:"k";i:3;s:1:"h";}s:14:"orig_list_time";N;s:14:"refusal_edited";N;s:15:"no_footer_links";i:1;s:9:"action_id";N;s:22:"pre_refuse_action_type";N;s:18:"focus_extra_images";N;s:10:"loadaction";b:1;s:11:"has_gallery";N;s:11:"store_token";N;s:8:"store_id";N;s:8:"login_id";N;s:9:"addimages";N;s:8:"addvideo";N;s:8:"order_id";N;s:6:"params";s:79:"types,price,rooms,size,garage_spaces,condominio,communes,currency,prev_currency";s:15:"red_arrow_price";N;s:7:"voucher";N;s:14:"voucher_result";N;s:8:"ad_price";N;s:10:"area_unset";i:1;s:9:"link_type";N;s:12:"job_category";N;s:12:"service_type";N;s:9:"offseason";N;s:23:"offseason_checked_descr";N;s:10:"peakseason";N;s:24:"peakseason_checked_descr";N;s:15:"available_weeks";N;s:29:"available_weeks_checked_descr";N;s:13:"show_password";i:1;s:13:"salted_passwd";N;s:12:"hide_address";N;s:21:"allow_category_change";b:0;s:13:"video_enabled";i:0;s:10:"video_name";N;s:11:"video_width";N;s:12:"video_height";N;s:12:"video_length";N;s:11:"video_added";i:0;s:18:"already_paid_video";N;s:7:"set_uid";N;s:21:"add_gallery_from_mail";N;s:2:"ht";N;s:10:"del_reason";N;s:9:"user_text";N;s:21:"user_text_testimonial";N;s:9:"unf_ad_id";N;s:14:"state_progress";s:7:"preview";s:12:"state_params";a:2:{s:2:"cl";b:0;s:2:"sp";N;}s:6:"caller";O:14:"blocket_caller":6:{s:6:"region";i:11;s:4:"city";i:0;s:4:"type";s:1:"s";s:11:"default_set";b:0;s:10:"cookie_set";b:0;s:6:"layout";i:0;}s:13:"current_state";s:7:"preview";s:11:"instance_id";i:0;s:11:"static_vars";a:7:{i:0;s:10:"company_ad";i:1;s:4:"name";i:2;s:5:"email";i:3;s:5:"phone";i:4;s:12:"phone_hidden";i:5;s:4:"area";i:6;s:12:"hide_address";}s:13:"volatile_vars";a:12:{i:0;s:15:"no_footer_links";i:1;s:13:"video_enabled";i:2;s:11:"static_vars";i:3;s:13:"redirect_done";i:4;s:13:"volatile_vars";i:5;s:13:"current_state";i:6;s:11:"use_session";i:7;s:12:"state_params";i:8;s:14:"current_method";i:9;s:10:"navigation";i:10;s:9:"init_data";i:11;s:11:"extra_bconf";}s:13:"redirect_done";b:0;s:11:"use_session";b:1;s:6:"errors";a:0:{}s:8:"warnings";a:0:{}s:8:"messages";a:0:{}s:16:"application_name";s:2:"ai";s:15:"single_instance";N;s:14:"current_method";s:3:"get";s:10:"navigation";N;s:9:"init_data";N;s:11:"extra_bconf";N;s:10:"headstyles";a:1:{i:0;s:6:"ai.css";}s:13:"ignore_fields";N;s:14:"show_body_warn";i:1;s:15:"show_image_warn";i:1;s:4:"area";N;}');
INSERT INTO unfinished_ads VALUES (3, 'mc1x829700d532d409fd06698a2f6b4e6362d30907da', 'boris@schibsted.cl', '2012-11-07 18:33:53.5505', '2012-11-07 18:33:59.60707', NULL, 'deleted', 'O:13:"blocket_newad":89:{s:2:"ad";O:3:"bAd":34:{s:6:"region";i:11;s:4:"area";N;s:12:"hide_address";N;s:10:"image_text";N;s:4:"type";s:1:"s";s:8:"category";i:7060;s:12:"sub_category";N;s:14:"category_group";i:7060;s:4:"name";s:13:"Boris Cruchet";s:5:"email";s:18:"boris@schibsted.cl";s:5:"phone";i:1234567890;s:12:"phone_hidden";N;s:10:"company_ad";s:1:"0";s:5:"store";N;s:7:"subject";s:17:"Aviso de Servicio";s:4:"body";s:107:"Aviso de Servicio para Api Aviso de Servicio para Api Aviso de Servicio para Api Aviso de Servicio para Api";s:8:"infopage";N;s:14:"infopage_title";N;s:5:"price";i:12400;s:6:"passwd";N;s:6:"coords";N;s:5:"state";N;s:6:"images";N;s:4:"lang";s:2:"es";s:16:"digests_presents";N;s:10:"thumbnails";N;s:17:"thumbnail_digests";N;s:7:"old_cat";N;s:4:"city";N;s:13:"prev_currency";s:4:"peso";s:12:"service_type";N;s:25:"available_weeks_offseason";N;s:26:"available_weeks_peakseason";N;s:15:"available_weeks";N;}s:5:"ad_id";N;s:7:"list_id";N;s:6:"action";N;s:14:"refused_reason";N;s:20:"refused_refusal_text";N;s:25:"already_paid_extra_images";N;s:10:"company_ad";s:1:"0";s:4:"name";s:13:"Boris Cruchet";s:5:"email";s:18:"boris@schibsted.cl";s:5:"phone";s:10:"1234567890";s:12:"phone_hidden";N;s:7:"account";i:0;s:4:"paid";N;s:6:"images";a:0:{}s:6:"stores";N;s:12:"is_new_image";N;s:16:"thumbnail_digest";N;s:14:"digest_present";N;s:9:"type_list";a:1:{i:0;s:1:"s";}s:14:"orig_list_time";N;s:14:"refusal_edited";N;s:15:"no_footer_links";i:1;s:9:"action_id";N;s:22:"pre_refuse_action_type";N;s:18:"focus_extra_images";N;s:10:"loadaction";b:1;s:11:"has_gallery";N;s:11:"store_token";N;s:8:"store_id";N;s:8:"login_id";N;s:9:"addimages";N;s:8:"addvideo";N;s:8:"order_id";N;s:6:"params";s:32:"service_type,price,prev_currency";s:15:"red_arrow_price";N;s:7:"voucher";N;s:14:"voucher_result";N;s:8:"ad_price";N;s:10:"area_unset";i:1;s:9:"link_type";N;s:12:"job_category";N;s:12:"service_type";s:5:"2|7|8";s:9:"offseason";N;s:23:"offseason_checked_descr";N;s:10:"peakseason";N;s:24:"peakseason_checked_descr";N;s:15:"available_weeks";N;s:29:"available_weeks_checked_descr";N;s:13:"show_password";i:1;s:13:"salted_passwd";N;s:12:"hide_address";N;s:21:"allow_category_change";b:0;s:13:"video_enabled";i:0;s:10:"video_name";N;s:11:"video_width";N;s:12:"video_height";N;s:12:"video_length";N;s:11:"video_added";i:0;s:18:"already_paid_video";N;s:7:"set_uid";N;s:21:"add_gallery_from_mail";N;s:2:"ht";N;s:10:"del_reason";N;s:9:"user_text";N;s:21:"user_text_testimonial";N;s:9:"unf_ad_id";N;s:14:"state_progress";s:7:"preview";s:12:"state_params";a:2:{s:2:"cl";b:0;s:2:"sp";N;}s:6:"caller";O:14:"blocket_caller":6:{s:6:"region";s:2:"15";s:4:"city";i:0;s:4:"type";s:1:"s";s:11:"default_set";b:1;s:10:"cookie_set";b:0;s:6:"layout";i:0;}s:13:"current_state";s:7:"preview";s:11:"instance_id";i:0;s:11:"static_vars";a:7:{i:0;s:10:"company_ad";i:1;s:4:"name";i:2;s:5:"email";i:3;s:5:"phone";i:4;s:12:"phone_hidden";i:5;s:4:"area";i:6;s:12:"hide_address";}s:13:"volatile_vars";a:12:{i:0;s:15:"no_footer_links";i:1;s:13:"video_enabled";i:2;s:11:"static_vars";i:3;s:13:"redirect_done";i:4;s:13:"volatile_vars";i:5;s:13:"current_state";i:6;s:11:"use_session";i:7;s:12:"state_params";i:8;s:14:"current_method";i:9;s:10:"navigation";i:10;s:9:"init_data";i:11;s:11:"extra_bconf";}s:13:"redirect_done";b:0;s:11:"use_session";b:1;s:6:"errors";a:0:{}s:8:"warnings";a:0:{}s:8:"messages";a:0:{}s:16:"application_name";s:2:"ai";s:15:"single_instance";N;s:14:"current_method";s:3:"get";s:10:"navigation";N;s:9:"init_data";N;s:11:"extra_bconf";N;s:4:"area";N;s:10:"headstyles";a:1:{i:0;s:6:"ai.css";}s:13:"ignore_fields";N;s:15:"show_image_warn";i:1;}');
INSERT INTO unfinished_ads VALUES (4, 'mc1x95c7a5a1041ea2970efbea1148471eb5a276e4fa', 'dany@schibsted.cl', '2012-11-07 18:34:48.883266', '2012-11-07 18:34:57.294043', NULL, 'deleted', 'O:13:"blocket_newad":90:{s:2:"ad";O:3:"bAd":39:{s:6:"region";i:11;s:4:"area";N;s:12:"hide_address";N;s:10:"image_text";N;s:4:"type";s:1:"u";s:8:"category";i:1040;s:12:"sub_category";N;s:14:"category_group";i:1040;s:4:"name";s:4:"dany";s:5:"email";s:17:"dany@schibsted.cl";s:5:"phone";i:123123123;s:12:"phone_hidden";N;s:10:"company_ad";s:1:"0";s:5:"store";N;s:7:"subject";s:25:"Linda Casa de gran tama�o";s:4:"body";s:16:"Arriendo mi casa";s:8:"infopage";N;s:14:"infopage_title";N;s:5:"price";i:140000;s:6:"passwd";N;s:6:"coords";N;s:5:"state";N;s:6:"images";N;s:4:"lang";s:2:"es";s:16:"digests_presents";N;s:10:"thumbnails";N;s:17:"thumbnail_digests";N;s:7:"old_cat";N;s:4:"city";N;s:13:"prev_currency";s:4:"peso";s:5:"rooms";i:4;s:4:"size";i:120;s:13:"garage_spaces";i:4;s:10:"condominio";i:10000;s:8:"communes";i:242;s:8:"currency";N;s:25:"available_weeks_offseason";N;s:26:"available_weeks_peakseason";N;s:15:"available_weeks";N;}s:5:"ad_id";N;s:7:"list_id";N;s:6:"action";N;s:14:"refused_reason";N;s:20:"refused_refusal_text";N;s:25:"already_paid_extra_images";N;s:10:"company_ad";s:1:"0";s:4:"name";s:4:"dany";s:5:"email";s:17:"dany@schibsted.cl";s:5:"phone";s:9:"123123123";s:12:"phone_hidden";N;s:7:"account";i:0;s:4:"paid";N;s:6:"images";a:0:{}s:6:"stores";N;s:12:"is_new_image";N;s:16:"thumbnail_digest";N;s:14:"digest_present";N;s:9:"type_list";a:4:{i:0;s:1:"s";i:1;s:1:"u";i:2;s:1:"k";i:3;s:1:"h";}s:14:"orig_list_time";N;s:14:"refusal_edited";N;s:15:"no_footer_links";i:1;s:9:"action_id";N;s:22:"pre_refuse_action_type";N;s:18:"focus_extra_images";N;s:10:"loadaction";b:1;s:11:"has_gallery";N;s:11:"store_token";N;s:8:"store_id";N;s:8:"login_id";N;s:9:"addimages";N;s:8:"addvideo";N;s:8:"order_id";N;s:6:"params";s:79:"types,price,rooms,size,garage_spaces,condominio,communes,currency,prev_currency";s:15:"red_arrow_price";N;s:7:"voucher";N;s:14:"voucher_result";N;s:8:"ad_price";N;s:10:"area_unset";i:1;s:9:"link_type";N;s:12:"job_category";N;s:12:"service_type";N;s:9:"offseason";N;s:23:"offseason_checked_descr";N;s:10:"peakseason";N;s:24:"peakseason_checked_descr";N;s:15:"available_weeks";N;s:29:"available_weeks_checked_descr";N;s:13:"show_password";i:1;s:13:"salted_passwd";N;s:12:"hide_address";N;s:21:"allow_category_change";b:0;s:13:"video_enabled";i:0;s:10:"video_name";N;s:11:"video_width";N;s:12:"video_height";N;s:12:"video_length";N;s:11:"video_added";i:0;s:18:"already_paid_video";N;s:7:"set_uid";N;s:21:"add_gallery_from_mail";N;s:2:"ht";N;s:10:"del_reason";N;s:9:"user_text";N;s:21:"user_text_testimonial";N;s:9:"unf_ad_id";N;s:14:"state_progress";s:7:"preview";s:12:"state_params";a:2:{s:2:"cl";b:0;s:2:"sp";N;}s:6:"caller";O:14:"blocket_caller":6:{s:6:"region";i:15;s:4:"city";i:0;s:4:"type";s:1:"s";s:11:"default_set";b:0;s:10:"cookie_set";b:0;s:6:"layout";i:0;}s:13:"current_state";s:7:"preview";s:11:"instance_id";i:0;s:11:"static_vars";a:7:{i:0;s:10:"company_ad";i:1;s:4:"name";i:2;s:5:"email";i:3;s:5:"phone";i:4;s:12:"phone_hidden";i:5;s:4:"area";i:6;s:12:"hide_address";}s:13:"volatile_vars";a:12:{i:0;s:15:"no_footer_links";i:1;s:13:"video_enabled";i:2;s:11:"static_vars";i:3;s:13:"redirect_done";i:4;s:13:"volatile_vars";i:5;s:13:"current_state";i:6;s:11:"use_session";i:7;s:12:"state_params";i:8;s:14:"current_method";i:9;s:10:"navigation";i:10;s:9:"init_data";i:11;s:11:"extra_bconf";}s:13:"redirect_done";b:0;s:11:"use_session";b:1;s:6:"errors";a:0:{}s:8:"warnings";a:0:{}s:8:"messages";a:0:{}s:16:"application_name";s:2:"ai";s:15:"single_instance";N;s:14:"current_method";s:3:"get";s:10:"navigation";N;s:9:"init_data";N;s:11:"extra_bconf";N;s:4:"area";N;s:10:"headstyles";a:1:{i:0;s:6:"ai.css";}s:13:"ignore_fields";N;s:14:"show_body_warn";i:1;s:15:"show_image_warn";i:1;}');
INSERT INTO unfinished_ads VALUES (6, 'mc1x95c7a5a1041ea2970efbea1148471eb5a276e4fa', 'dany@schibsted.cl', '2012-11-07 18:36:40.96863', '2012-11-07 18:36:53.502015', NULL, 'deleted', 'O:13:"blocket_newad":89:{s:2:"ad";O:3:"bAd":37:{s:6:"region";i:11;s:4:"area";N;s:12:"hide_address";N;s:10:"image_text";N;s:4:"type";s:1:"u";s:8:"category";i:1060;s:12:"sub_category";N;s:14:"category_group";i:1060;s:4:"name";s:4:"dany";s:5:"email";s:17:"dany@schibsted.cl";s:5:"phone";i:123123123;s:12:"phone_hidden";N;s:10:"company_ad";s:1:"0";s:5:"store";N;s:7:"subject";s:20:"Oficina en el centro";s:4:"body";s:45:"linda oficina en el centro de valdivia
piso 3";s:8:"infopage";N;s:14:"infopage_title";N;s:5:"price";i:50000;s:6:"passwd";N;s:6:"coords";N;s:5:"state";N;s:6:"images";N;s:4:"lang";s:2:"es";s:16:"digests_presents";N;s:10:"thumbnails";N;s:17:"thumbnail_digests";N;s:7:"old_cat";N;s:4:"city";N;s:13:"prev_currency";s:4:"peso";s:4:"size";i:50;s:13:"garage_spaces";i:1;s:8:"communes";i:243;s:8:"currency";N;s:25:"available_weeks_offseason";N;s:26:"available_weeks_peakseason";N;s:15:"available_weeks";N;}s:5:"ad_id";N;s:7:"list_id";N;s:6:"action";N;s:14:"refused_reason";N;s:20:"refused_refusal_text";N;s:25:"already_paid_extra_images";N;s:10:"company_ad";s:1:"0";s:4:"name";s:4:"dany";s:5:"email";s:17:"dany@schibsted.cl";s:5:"phone";s:9:"123123123";s:12:"phone_hidden";N;s:7:"account";i:0;s:4:"paid";N;s:6:"images";a:1:{i:0;s:14:"5169378538.jpg";}s:6:"stores";N;s:12:"is_new_image";a:1:{i:0;s:1:"1";}s:16:"thumbnail_digest";a:1:{i:0;s:32:"2bb0050597bee09b33ceb1f92b82c716";}s:14:"digest_present";a:1:{i:0;s:1:"0";}s:9:"type_list";a:4:{i:0;s:1:"s";i:1;s:1:"u";i:2;s:1:"k";i:3;s:1:"h";}s:14:"orig_list_time";N;s:14:"refusal_edited";N;s:15:"no_footer_links";i:1;s:9:"action_id";N;s:22:"pre_refuse_action_type";N;s:18:"focus_extra_images";N;s:10:"loadaction";b:1;s:11:"has_gallery";N;s:11:"store_token";N;s:8:"store_id";N;s:8:"login_id";N;s:9:"addimages";N;s:8:"addvideo";N;s:8:"order_id";N;s:6:"params";s:62:"types,price,size,garage_spaces,communes,currency,prev_currency";s:15:"red_arrow_price";N;s:7:"voucher";N;s:14:"voucher_result";N;s:8:"ad_price";N;s:10:"area_unset";i:1;s:9:"link_type";N;s:12:"job_category";N;s:12:"service_type";N;s:9:"offseason";N;s:23:"offseason_checked_descr";N;s:10:"peakseason";N;s:24:"peakseason_checked_descr";N;s:15:"available_weeks";N;s:29:"available_weeks_checked_descr";N;s:13:"show_password";i:1;s:13:"salted_passwd";N;s:12:"hide_address";N;s:21:"allow_category_change";b:0;s:13:"video_enabled";i:0;s:10:"video_name";N;s:11:"video_width";N;s:12:"video_height";N;s:12:"video_length";N;s:11:"video_added";i:0;s:18:"already_paid_video";N;s:7:"set_uid";N;s:21:"add_gallery_from_mail";N;s:2:"ht";N;s:10:"del_reason";N;s:9:"user_text";N;s:21:"user_text_testimonial";N;s:9:"unf_ad_id";N;s:14:"state_progress";s:7:"preview";s:12:"state_params";a:2:{s:2:"cl";b:0;s:2:"sp";N;}s:6:"caller";O:14:"blocket_caller":6:{s:6:"region";i:15;s:4:"city";i:0;s:4:"type";s:1:"s";s:11:"default_set";b:0;s:10:"cookie_set";b:0;s:6:"layout";i:0;}s:13:"current_state";s:7:"preview";s:11:"instance_id";i:0;s:11:"static_vars";a:7:{i:0;s:10:"company_ad";i:1;s:4:"name";i:2;s:5:"email";i:3;s:5:"phone";i:4;s:12:"phone_hidden";i:5;s:4:"area";i:6;s:12:"hide_address";}s:13:"volatile_vars";a:12:{i:0;s:15:"no_footer_links";i:1;s:13:"video_enabled";i:2;s:11:"static_vars";i:3;s:13:"redirect_done";i:4;s:13:"volatile_vars";i:5;s:13:"current_state";i:6;s:11:"use_session";i:7;s:12:"state_params";i:8;s:14:"current_method";i:9;s:10:"navigation";i:10;s:9:"init_data";i:11;s:11:"extra_bconf";}s:13:"redirect_done";b:0;s:11:"use_session";b:1;s:6:"errors";a:0:{}s:8:"warnings";a:0:{}s:8:"messages";a:0:{}s:16:"application_name";s:2:"ai";s:15:"single_instance";N;s:14:"current_method";s:3:"get";s:10:"navigation";N;s:9:"init_data";N;s:11:"extra_bconf";N;s:4:"area";N;s:10:"headstyles";a:1:{i:0;s:6:"ai.css";}s:13:"ignore_fields";N;s:14:"show_body_warn";i:1;}');
INSERT INTO unfinished_ads VALUES (5, 'mc1x829700d532d409fd06698a2f6b4e6362d30907da', 'boris@schibsted.cl', '2012-11-07 18:35:30.897062', '2012-11-07 18:35:39.15397', NULL, 'deleted', 'O:13:"blocket_newad":89:{s:2:"ad";O:3:"bAd":34:{s:6:"region";i:15;s:4:"area";N;s:12:"hide_address";N;s:10:"image_text";N;s:4:"type";s:1:"s";s:8:"category";i:7040;s:12:"sub_category";N;s:14:"category_group";i:7040;s:4:"name";s:13:"Boris Cruchet";s:5:"email";s:18:"boris@schibsted.cl";s:5:"phone";i:1234567890;s:12:"phone_hidden";N;s:10:"company_ad";s:1:"0";s:5:"store";N;s:7:"subject";s:27:"Busco empleo de programador";s:4:"body";s:45:"Soy muy diestro programando en Basic y Pascal";s:8:"infopage";N;s:14:"infopage_title";N;s:5:"price";N;s:6:"passwd";N;s:6:"coords";N;s:5:"state";N;s:6:"images";N;s:4:"lang";s:2:"es";s:16:"digests_presents";N;s:10:"thumbnails";N;s:17:"thumbnail_digests";N;s:7:"old_cat";N;s:4:"city";N;s:13:"prev_currency";N;s:12:"job_category";N;s:25:"available_weeks_offseason";N;s:26:"available_weeks_peakseason";N;s:15:"available_weeks";N;}s:5:"ad_id";N;s:7:"list_id";N;s:6:"action";N;s:14:"refused_reason";N;s:20:"refused_refusal_text";N;s:25:"already_paid_extra_images";N;s:10:"company_ad";s:1:"0";s:4:"name";s:13:"Boris Cruchet";s:5:"email";s:18:"boris@schibsted.cl";s:5:"phone";s:10:"1234567890";s:12:"phone_hidden";N;s:7:"account";i:0;s:4:"paid";N;s:6:"images";a:1:{i:0;s:14:"5160855219.jpg";}s:6:"stores";N;s:12:"is_new_image";a:1:{i:0;s:1:"1";}s:16:"thumbnail_digest";a:1:{i:0;s:32:"21c1602c8cd47649d3b7e964e1a5b763";}s:14:"digest_present";a:1:{i:0;s:1:"0";}s:9:"type_list";a:1:{i:0;s:1:"s";}s:14:"orig_list_time";N;s:14:"refusal_edited";N;s:15:"no_footer_links";i:1;s:9:"action_id";N;s:22:"pre_refuse_action_type";N;s:18:"focus_extra_images";N;s:10:"loadaction";b:1;s:11:"has_gallery";N;s:11:"store_token";N;s:8:"store_id";N;s:8:"login_id";N;s:9:"addimages";N;s:8:"addvideo";N;s:8:"order_id";N;s:6:"params";s:12:"job_category";s:15:"red_arrow_price";N;s:7:"voucher";N;s:14:"voucher_result";N;s:8:"ad_price";N;s:10:"area_unset";i:1;s:9:"link_type";N;s:12:"job_category";s:6:"3|5|10";s:12:"service_type";N;s:9:"offseason";N;s:23:"offseason_checked_descr";N;s:10:"peakseason";N;s:24:"peakseason_checked_descr";N;s:15:"available_weeks";N;s:29:"available_weeks_checked_descr";N;s:13:"show_password";i:1;s:13:"salted_passwd";N;s:12:"hide_address";N;s:21:"allow_category_change";b:0;s:13:"video_enabled";i:0;s:10:"video_name";N;s:11:"video_width";N;s:12:"video_height";N;s:12:"video_length";N;s:11:"video_added";i:0;s:18:"already_paid_video";N;s:7:"set_uid";N;s:21:"add_gallery_from_mail";N;s:2:"ht";N;s:10:"del_reason";N;s:9:"user_text";N;s:21:"user_text_testimonial";N;s:9:"unf_ad_id";N;s:14:"state_progress";s:7:"preview";s:12:"state_params";a:2:{s:2:"cl";b:0;s:2:"sp";N;}s:6:"caller";O:14:"blocket_caller":6:{s:6:"region";s:2:"15";s:4:"city";i:0;s:4:"type";s:1:"s";s:11:"default_set";b:1;s:10:"cookie_set";b:0;s:6:"layout";i:0;}s:13:"current_state";s:7:"preview";s:11:"instance_id";i:0;s:11:"static_vars";a:7:{i:0;s:10:"company_ad";i:1;s:4:"name";i:2;s:5:"email";i:3;s:5:"phone";i:4;s:12:"phone_hidden";i:5;s:4:"area";i:6;s:12:"hide_address";}s:13:"volatile_vars";a:12:{i:0;s:15:"no_footer_links";i:1;s:13:"video_enabled";i:2;s:11:"static_vars";i:3;s:13:"redirect_done";i:4;s:13:"volatile_vars";i:5;s:13:"current_state";i:6;s:11:"use_session";i:7;s:12:"state_params";i:8;s:14:"current_method";i:9;s:10:"navigation";i:10;s:9:"init_data";i:11;s:11:"extra_bconf";}s:13:"redirect_done";b:0;s:11:"use_session";b:1;s:6:"errors";a:0:{}s:8:"warnings";a:0:{}s:8:"messages";a:0:{}s:16:"application_name";s:2:"ai";s:15:"single_instance";N;s:14:"current_method";s:3:"get";s:10:"navigation";N;s:9:"init_data";N;s:11:"extra_bconf";N;s:4:"area";N;s:10:"headstyles";a:1:{i:0;s:6:"ai.css";}s:13:"ignore_fields";N;s:14:"show_body_warn";i:1;}');
INSERT INTO unfinished_ads VALUES (7, 'mc1x829700d532d409fd06698a2f6b4e6362d30907da', 'boris@schibsted.cl', '2012-11-07 18:37:01.700181', '2012-11-07 18:37:13.77436', NULL, 'deleted', 'O:13:"blocket_newad":90:{s:2:"ad";O:3:"bAd":34:{s:6:"region";i:8;s:4:"area";N;s:12:"hide_address";N;s:10:"image_text";N;s:4:"type";s:1:"s";s:8:"category";i:7020;s:12:"sub_category";N;s:14:"category_group";i:7020;s:4:"name";s:13:"Boris Cruchet";s:5:"email";s:18:"boris@schibsted.cl";s:5:"phone";i:1234567890;s:12:"phone_hidden";N;s:10:"company_ad";s:1:"0";s:5:"store";N;s:7:"subject";s:17:"Oferton de empleo";s:4:"body";s:26:"Buscamos community manager";s:8:"infopage";N;s:14:"infopage_title";N;s:5:"price";N;s:6:"passwd";N;s:6:"coords";N;s:5:"state";N;s:6:"images";N;s:4:"lang";s:2:"es";s:16:"digests_presents";N;s:10:"thumbnails";N;s:17:"thumbnail_digests";N;s:7:"old_cat";N;s:4:"city";N;s:13:"prev_currency";N;s:12:"job_category";N;s:25:"available_weeks_offseason";N;s:26:"available_weeks_peakseason";N;s:15:"available_weeks";N;}s:5:"ad_id";N;s:7:"list_id";N;s:6:"action";N;s:14:"refused_reason";N;s:20:"refused_refusal_text";N;s:25:"already_paid_extra_images";N;s:10:"company_ad";s:1:"0";s:4:"name";s:13:"Boris Cruchet";s:5:"email";s:18:"boris@schibsted.cl";s:5:"phone";s:10:"1234567890";s:12:"phone_hidden";N;s:7:"account";i:0;s:4:"paid";N;s:6:"images";a:0:{}s:6:"stores";N;s:12:"is_new_image";N;s:16:"thumbnail_digest";N;s:14:"digest_present";N;s:9:"type_list";a:1:{i:0;s:1:"s";}s:14:"orig_list_time";N;s:14:"refusal_edited";N;s:15:"no_footer_links";i:1;s:9:"action_id";N;s:22:"pre_refuse_action_type";N;s:18:"focus_extra_images";N;s:10:"loadaction";b:1;s:11:"has_gallery";N;s:11:"store_token";N;s:8:"store_id";N;s:8:"login_id";N;s:9:"addimages";N;s:8:"addvideo";N;s:8:"order_id";N;s:6:"params";s:12:"job_category";s:15:"red_arrow_price";N;s:7:"voucher";N;s:14:"voucher_result";N;s:8:"ad_price";N;s:10:"area_unset";i:1;s:9:"link_type";N;s:12:"job_category";s:6:"1|2|11";s:12:"service_type";N;s:9:"offseason";N;s:23:"offseason_checked_descr";N;s:10:"peakseason";N;s:24:"peakseason_checked_descr";N;s:15:"available_weeks";N;s:29:"available_weeks_checked_descr";N;s:13:"show_password";i:1;s:13:"salted_passwd";N;s:12:"hide_address";N;s:21:"allow_category_change";b:0;s:13:"video_enabled";i:0;s:10:"video_name";N;s:11:"video_width";N;s:12:"video_height";N;s:12:"video_length";N;s:11:"video_added";i:0;s:18:"already_paid_video";N;s:7:"set_uid";N;s:21:"add_gallery_from_mail";N;s:2:"ht";N;s:10:"del_reason";N;s:9:"user_text";N;s:21:"user_text_testimonial";N;s:9:"unf_ad_id";N;s:14:"state_progress";s:7:"preview";s:12:"state_params";a:2:{s:2:"cl";b:0;s:2:"sp";N;}s:6:"caller";O:14:"blocket_caller":6:{s:6:"region";s:2:"15";s:4:"city";i:0;s:4:"type";s:1:"s";s:11:"default_set";b:1;s:10:"cookie_set";b:0;s:6:"layout";i:0;}s:13:"current_state";s:7:"preview";s:11:"instance_id";i:0;s:11:"static_vars";a:7:{i:0;s:10:"company_ad";i:1;s:4:"name";i:2;s:5:"email";i:3;s:5:"phone";i:4;s:12:"phone_hidden";i:5;s:4:"area";i:6;s:12:"hide_address";}s:13:"volatile_vars";a:12:{i:0;s:15:"no_footer_links";i:1;s:13:"video_enabled";i:2;s:11:"static_vars";i:3;s:13:"redirect_done";i:4;s:13:"volatile_vars";i:5;s:13:"current_state";i:6;s:11:"use_session";i:7;s:12:"state_params";i:8;s:14:"current_method";i:9;s:10:"navigation";i:10;s:9:"init_data";i:11;s:11:"extra_bconf";}s:13:"redirect_done";b:0;s:11:"use_session";b:1;s:6:"errors";a:0:{}s:8:"warnings";a:0:{}s:8:"messages";a:0:{}s:16:"application_name";s:2:"ai";s:15:"single_instance";N;s:14:"current_method";s:3:"get";s:10:"navigation";N;s:9:"init_data";N;s:11:"extra_bconf";N;s:4:"area";N;s:10:"headstyles";a:1:{i:0;s:6:"ai.css";}s:13:"ignore_fields";N;s:14:"show_body_warn";i:1;s:15:"show_image_warn";i:1;}');
INSERT INTO unfinished_ads VALUES (8, 'mc1x95c7a5a1041ea2970efbea1148471eb5a276e4fa', 'dany@schibsted.cl', '2012-11-07 18:38:01.210607', '2012-11-07 18:38:11.843005', NULL, 'deleted', 'O:13:"blocket_newad":90:{s:2:"ad";O:3:"bAd":37:{s:6:"region";i:11;s:4:"area";N;s:12:"hide_address";N;s:10:"image_text";N;s:4:"type";s:1:"s";s:8:"category";i:1080;s:12:"sub_category";N;s:14:"category_group";i:1080;s:4:"name";s:4:"dany";s:5:"email";s:17:"dany@schibsted.cl";s:5:"phone";i:123123123;s:12:"phone_hidden";N;s:10:"company_ad";s:1:"0";s:5:"store";N;s:7:"subject";s:15:"Local Comercial";s:4:"body";s:38:"espacioso local comercial en el centro";s:8:"infopage";N;s:14:"infopage_title";N;s:5:"price";i:40000;s:6:"passwd";N;s:6:"coords";N;s:5:"state";N;s:6:"images";N;s:4:"lang";s:2:"es";s:16:"digests_presents";N;s:10:"thumbnails";N;s:17:"thumbnail_digests";N;s:7:"old_cat";N;s:4:"city";N;s:13:"prev_currency";s:4:"peso";s:4:"size";i:100;s:13:"garage_spaces";i:1;s:8:"communes";i:239;s:8:"currency";N;s:25:"available_weeks_offseason";N;s:26:"available_weeks_peakseason";N;s:15:"available_weeks";N;}s:5:"ad_id";N;s:7:"list_id";N;s:6:"action";N;s:14:"refused_reason";N;s:20:"refused_refusal_text";N;s:25:"already_paid_extra_images";N;s:10:"company_ad";s:1:"0";s:4:"name";s:4:"dany";s:5:"email";s:17:"dany@schibsted.cl";s:5:"phone";s:9:"123123123";s:12:"phone_hidden";N;s:7:"account";i:0;s:4:"paid";N;s:6:"images";a:0:{}s:6:"stores";N;s:12:"is_new_image";N;s:16:"thumbnail_digest";N;s:14:"digest_present";N;s:9:"type_list";a:4:{i:0;s:1:"s";i:1;s:1:"u";i:2;s:1:"k";i:3;s:1:"h";}s:14:"orig_list_time";N;s:14:"refusal_edited";N;s:15:"no_footer_links";i:1;s:9:"action_id";N;s:22:"pre_refuse_action_type";N;s:18:"focus_extra_images";N;s:10:"loadaction";b:1;s:11:"has_gallery";N;s:11:"store_token";N;s:8:"store_id";N;s:8:"login_id";N;s:9:"addimages";N;s:8:"addvideo";N;s:8:"order_id";N;s:6:"params";s:62:"types,price,size,garage_spaces,communes,currency,prev_currency";s:15:"red_arrow_price";N;s:7:"voucher";N;s:14:"voucher_result";N;s:8:"ad_price";N;s:10:"area_unset";i:1;s:9:"link_type";N;s:12:"job_category";N;s:12:"service_type";N;s:9:"offseason";N;s:23:"offseason_checked_descr";N;s:10:"peakseason";N;s:24:"peakseason_checked_descr";N;s:15:"available_weeks";N;s:29:"available_weeks_checked_descr";N;s:13:"show_password";i:1;s:13:"salted_passwd";N;s:12:"hide_address";N;s:21:"allow_category_change";b:0;s:13:"video_enabled";i:0;s:10:"video_name";N;s:11:"video_width";N;s:12:"video_height";N;s:12:"video_length";N;s:11:"video_added";i:0;s:18:"already_paid_video";N;s:7:"set_uid";N;s:21:"add_gallery_from_mail";N;s:2:"ht";N;s:10:"del_reason";N;s:9:"user_text";N;s:21:"user_text_testimonial";N;s:9:"unf_ad_id";N;s:14:"state_progress";s:7:"preview";s:12:"state_params";a:2:{s:2:"cl";b:0;s:2:"sp";N;}s:6:"caller";O:14:"blocket_caller":6:{s:6:"region";i:15;s:4:"city";i:0;s:4:"type";s:1:"s";s:11:"default_set";b:0;s:10:"cookie_set";b:0;s:6:"layout";i:0;}s:13:"current_state";s:7:"preview";s:11:"instance_id";i:0;s:11:"static_vars";a:7:{i:0;s:10:"company_ad";i:1;s:4:"name";i:2;s:5:"email";i:3;s:5:"phone";i:4;s:12:"phone_hidden";i:5;s:4:"area";i:6;s:12:"hide_address";}s:13:"volatile_vars";a:12:{i:0;s:15:"no_footer_links";i:1;s:13:"video_enabled";i:2;s:11:"static_vars";i:3;s:13:"redirect_done";i:4;s:13:"volatile_vars";i:5;s:13:"current_state";i:6;s:11:"use_session";i:7;s:12:"state_params";i:8;s:14:"current_method";i:9;s:10:"navigation";i:10;s:9:"init_data";i:11;s:11:"extra_bconf";}s:13:"redirect_done";b:0;s:11:"use_session";b:1;s:6:"errors";a:0:{}s:8:"warnings";a:0:{}s:8:"messages";a:0:{}s:16:"application_name";s:2:"ai";s:15:"single_instance";N;s:14:"current_method";s:3:"get";s:10:"navigation";N;s:9:"init_data";N;s:11:"extra_bconf";N;s:4:"area";N;s:10:"headstyles";a:1:{i:0;s:6:"ai.css";}s:13:"ignore_fields";N;s:14:"show_body_warn";i:1;s:15:"show_image_warn";i:1;}');
INSERT INTO unfinished_ads VALUES (9, 'mc1x95c7a5a1041ea2970efbea1148471eb5a276e4fa', 'dany@schibsted.cl', '2012-11-07 18:39:08.844707', '2012-11-07 18:39:18.23853', NULL, 'deleted', 'O:13:"blocket_newad":90:{s:2:"ad";O:3:"bAd":37:{s:6:"region";i:11;s:4:"area";N;s:12:"hide_address";N;s:10:"image_text";N;s:4:"type";s:1:"s";s:8:"category";i:1100;s:12:"sub_category";N;s:14:"category_group";i:1100;s:4:"name";s:4:"dany";s:5:"email";s:17:"dany@schibsted.cl";s:5:"phone";i:123123123;s:12:"phone_hidden";N;s:10:"company_ad";s:1:"0";s:5:"store";N;s:7:"subject";s:18:"Terreno en Futrono";s:4:"body";s:43:"para que construyas tu casa como tu quieras";s:8:"infopage";N;s:14:"infopage_title";N;s:5:"price";i:12000000;s:6:"passwd";N;s:6:"coords";N;s:5:"state";N;s:6:"images";N;s:4:"lang";s:2:"es";s:16:"digests_presents";N;s:10:"thumbnails";N;s:17:"thumbnail_digests";N;s:7:"old_cat";N;s:4:"city";N;s:13:"prev_currency";s:4:"peso";s:4:"size";i:250;s:10:"condominio";i:10;s:8:"communes";i:233;s:8:"currency";N;s:25:"available_weeks_offseason";N;s:26:"available_weeks_peakseason";N;s:15:"available_weeks";N;}s:5:"ad_id";N;s:7:"list_id";N;s:6:"action";N;s:14:"refused_reason";N;s:20:"refused_refusal_text";N;s:25:"already_paid_extra_images";N;s:10:"company_ad";s:1:"0";s:4:"name";s:4:"dany";s:5:"email";s:17:"dany@schibsted.cl";s:5:"phone";s:9:"123123123";s:12:"phone_hidden";N;s:7:"account";i:0;s:4:"paid";N;s:6:"images";a:0:{}s:6:"stores";N;s:12:"is_new_image";N;s:16:"thumbnail_digest";N;s:14:"digest_present";N;s:9:"type_list";a:2:{i:0;s:1:"s";i:1;s:1:"k";}s:14:"orig_list_time";N;s:14:"refusal_edited";N;s:15:"no_footer_links";i:1;s:9:"action_id";N;s:22:"pre_refuse_action_type";N;s:18:"focus_extra_images";N;s:10:"loadaction";b:1;s:11:"has_gallery";N;s:11:"store_token";N;s:8:"store_id";N;s:8:"login_id";N;s:9:"addimages";N;s:8:"addvideo";N;s:8:"order_id";N;s:6:"params";s:59:"types,price,size,condominio,communes,currency,prev_currency";s:15:"red_arrow_price";N;s:7:"voucher";N;s:14:"voucher_result";N;s:8:"ad_price";N;s:10:"area_unset";i:1;s:9:"link_type";N;s:12:"job_category";N;s:12:"service_type";N;s:9:"offseason";N;s:23:"offseason_checked_descr";N;s:10:"peakseason";N;s:24:"peakseason_checked_descr";N;s:15:"available_weeks";N;s:29:"available_weeks_checked_descr";N;s:13:"show_password";i:1;s:13:"salted_passwd";N;s:12:"hide_address";N;s:21:"allow_category_change";b:0;s:13:"video_enabled";i:0;s:10:"video_name";N;s:11:"video_width";N;s:12:"video_height";N;s:12:"video_length";N;s:11:"video_added";i:0;s:18:"already_paid_video";N;s:7:"set_uid";N;s:21:"add_gallery_from_mail";N;s:2:"ht";N;s:10:"del_reason";N;s:9:"user_text";N;s:21:"user_text_testimonial";N;s:9:"unf_ad_id";N;s:14:"state_progress";s:7:"preview";s:12:"state_params";a:2:{s:2:"cl";b:0;s:2:"sp";N;}s:6:"caller";O:14:"blocket_caller":6:{s:6:"region";i:15;s:4:"city";i:0;s:4:"type";s:1:"s";s:11:"default_set";b:0;s:10:"cookie_set";b:0;s:6:"layout";i:0;}s:13:"current_state";s:7:"preview";s:11:"instance_id";i:0;s:11:"static_vars";a:7:{i:0;s:10:"company_ad";i:1;s:4:"name";i:2;s:5:"email";i:3;s:5:"phone";i:4;s:12:"phone_hidden";i:5;s:4:"area";i:6;s:12:"hide_address";}s:13:"volatile_vars";a:12:{i:0;s:15:"no_footer_links";i:1;s:13:"video_enabled";i:2;s:11:"static_vars";i:3;s:13:"redirect_done";i:4;s:13:"volatile_vars";i:5;s:13:"current_state";i:6;s:11:"use_session";i:7;s:12:"state_params";i:8;s:14:"current_method";i:9;s:10:"navigation";i:10;s:9:"init_data";i:11;s:11:"extra_bconf";}s:13:"redirect_done";b:0;s:11:"use_session";b:1;s:6:"errors";a:0:{}s:8:"warnings";a:0:{}s:8:"messages";a:0:{}s:16:"application_name";s:2:"ai";s:15:"single_instance";N;s:14:"current_method";s:3:"get";s:10:"navigation";N;s:9:"init_data";N;s:11:"extra_bconf";N;s:4:"area";N;s:10:"headstyles";a:1:{i:0;s:6:"ai.css";}s:13:"ignore_fields";N;s:14:"show_body_warn";i:1;s:15:"show_image_warn";i:1;}');
INSERT INTO unfinished_ads VALUES (11, 'mc1x95c7a5a1041ea2970efbea1148471eb5a276e4fa', 'dany@schibsted.cl', '2012-11-07 18:40:29.414867', '2012-11-07 18:40:38.547799', NULL, 'deleted', 'O:13:"blocket_newad":90:{s:2:"ad";O:3:"bAd":38:{s:6:"region";i:11;s:4:"area";N;s:12:"hide_address";N;s:10:"image_text";N;s:4:"type";s:1:"u";s:8:"category";i:1120;s:12:"sub_category";N;s:14:"category_group";i:1120;s:4:"name";s:4:"dany";s:5:"email";s:17:"dany@schibsted.cl";s:5:"phone";i:123123123;s:12:"phone_hidden";N;s:10:"company_ad";s:1:"0";s:5:"store";N;s:7:"subject";s:23:"Estacionamiento central";s:4:"body";s:36:"Estacionamiento mensual en el centro";s:8:"infopage";N;s:14:"infopage_title";N;s:5:"price";i:20000;s:6:"passwd";N;s:6:"coords";N;s:5:"state";N;s:6:"images";N;s:4:"lang";s:2:"es";s:16:"digests_presents";N;s:10:"thumbnails";N;s:17:"thumbnail_digests";N;s:7:"old_cat";N;s:4:"city";N;s:13:"prev_currency";s:4:"peso";s:4:"size";i:12;s:13:"garage_spaces";i:1;s:10:"condominio";i:1000;s:8:"communes";i:243;s:8:"currency";N;s:25:"available_weeks_offseason";N;s:26:"available_weeks_peakseason";N;s:15:"available_weeks";N;}s:5:"ad_id";N;s:7:"list_id";N;s:6:"action";N;s:14:"refused_reason";N;s:20:"refused_refusal_text";N;s:25:"already_paid_extra_images";N;s:10:"company_ad";s:1:"0";s:4:"name";s:4:"dany";s:5:"email";s:17:"dany@schibsted.cl";s:5:"phone";s:9:"123123123";s:12:"phone_hidden";N;s:7:"account";i:0;s:4:"paid";N;s:6:"images";a:0:{}s:6:"stores";N;s:12:"is_new_image";N;s:16:"thumbnail_digest";N;s:14:"digest_present";N;s:9:"type_list";a:4:{i:0;s:1:"s";i:1;s:1:"u";i:2;s:1:"k";i:3;s:1:"h";}s:14:"orig_list_time";N;s:14:"refusal_edited";N;s:15:"no_footer_links";i:1;s:9:"action_id";N;s:22:"pre_refuse_action_type";N;s:18:"focus_extra_images";N;s:10:"loadaction";b:1;s:11:"has_gallery";N;s:11:"store_token";N;s:8:"store_id";N;s:8:"login_id";N;s:9:"addimages";N;s:8:"addvideo";N;s:8:"order_id";N;s:6:"params";s:73:"types,price,size,garage_spaces,condominio,communes,currency,prev_currency";s:15:"red_arrow_price";N;s:7:"voucher";N;s:14:"voucher_result";N;s:8:"ad_price";N;s:10:"area_unset";i:1;s:9:"link_type";N;s:12:"job_category";N;s:12:"service_type";N;s:9:"offseason";N;s:23:"offseason_checked_descr";N;s:10:"peakseason";N;s:24:"peakseason_checked_descr";N;s:15:"available_weeks";N;s:29:"available_weeks_checked_descr";N;s:13:"show_password";i:1;s:13:"salted_passwd";N;s:12:"hide_address";N;s:21:"allow_category_change";b:0;s:13:"video_enabled";i:0;s:10:"video_name";N;s:11:"video_width";N;s:12:"video_height";N;s:12:"video_length";N;s:11:"video_added";i:0;s:18:"already_paid_video";N;s:7:"set_uid";N;s:21:"add_gallery_from_mail";N;s:2:"ht";N;s:10:"del_reason";N;s:9:"user_text";N;s:21:"user_text_testimonial";N;s:9:"unf_ad_id";N;s:14:"state_progress";s:7:"preview";s:12:"state_params";a:2:{s:2:"cl";b:0;s:2:"sp";N;}s:6:"caller";O:14:"blocket_caller":6:{s:6:"region";i:15;s:4:"city";i:0;s:4:"type";s:1:"s";s:11:"default_set";b:0;s:10:"cookie_set";b:0;s:6:"layout";i:0;}s:13:"current_state";s:7:"preview";s:11:"instance_id";i:0;s:11:"static_vars";a:7:{i:0;s:10:"company_ad";i:1;s:4:"name";i:2;s:5:"email";i:3;s:5:"phone";i:4;s:12:"phone_hidden";i:5;s:4:"area";i:6;s:12:"hide_address";}s:13:"volatile_vars";a:12:{i:0;s:15:"no_footer_links";i:1;s:13:"video_enabled";i:2;s:11:"static_vars";i:3;s:13:"redirect_done";i:4;s:13:"volatile_vars";i:5;s:13:"current_state";i:6;s:11:"use_session";i:7;s:12:"state_params";i:8;s:14:"current_method";i:9;s:10:"navigation";i:10;s:9:"init_data";i:11;s:11:"extra_bconf";}s:13:"redirect_done";b:0;s:11:"use_session";b:1;s:6:"errors";a:0:{}s:8:"warnings";a:0:{}s:8:"messages";a:0:{}s:16:"application_name";s:2:"ai";s:15:"single_instance";N;s:14:"current_method";s:3:"get";s:10:"navigation";N;s:9:"init_data";N;s:11:"extra_bconf";N;s:4:"area";N;s:10:"headstyles";a:1:{i:0;s:6:"ai.css";}s:13:"ignore_fields";N;s:14:"show_body_warn";i:1;s:15:"show_image_warn";i:1;}');
INSERT INTO unfinished_ads VALUES (10, 'mc1x829700d532d409fd06698a2f6b4e6362d30907da', 'boris@schibsted.cl', '2012-11-07 18:39:56.93924', '2012-11-07 18:40:28.087923', NULL, 'deleted', 'O:13:"blocket_newad":90:{s:2:"ad";O:3:"bAd":34:{s:6:"region";i:15;s:4:"area";N;s:12:"hide_address";N;s:10:"image_text";N;s:4:"type";s:1:"s";s:8:"category";i:5140;s:12:"sub_category";N;s:14:"category_group";i:5140;s:4:"name";s:13:"Boris Cruchet";s:5:"email";s:18:"boris@schibsted.cl";s:5:"phone";i:1234567890;s:12:"phone_hidden";N;s:10:"company_ad";s:1:"0";s:5:"store";N;s:7:"subject";s:19:"Cartera Luis Guaton";s:4:"body";s:12:"Super filete";s:8:"infopage";N;s:14:"infopage_title";N;s:5:"price";i:320000;s:6:"passwd";N;s:6:"coords";N;s:5:"state";N;s:6:"images";N;s:4:"lang";s:2:"es";s:16:"digests_presents";N;s:10:"thumbnails";N;s:17:"thumbnail_digests";N;s:7:"old_cat";N;s:4:"city";N;s:13:"prev_currency";s:4:"peso";s:9:"condition";i:1;s:25:"available_weeks_offseason";N;s:26:"available_weeks_peakseason";N;s:15:"available_weeks";N;}s:5:"ad_id";N;s:7:"list_id";N;s:6:"action";N;s:14:"refused_reason";N;s:20:"refused_refusal_text";N;s:25:"already_paid_extra_images";N;s:10:"company_ad";s:1:"0";s:4:"name";s:13:"Boris Cruchet";s:5:"email";s:18:"boris@schibsted.cl";s:5:"phone";s:10:"1234567890";s:12:"phone_hidden";N;s:7:"account";i:0;s:4:"paid";N;s:6:"images";a:1:{i:0;s:14:"5177901857.jpg";}s:6:"stores";N;s:12:"is_new_image";a:1:{i:0;s:1:"1";}s:16:"thumbnail_digest";a:1:{i:0;s:32:"c14c9541698d5b24afe9f9079e5a246a";}s:14:"digest_present";a:1:{i:0;s:1:"0";}s:9:"type_list";a:2:{i:0;s:1:"s";i:1;s:1:"k";}s:14:"orig_list_time";N;s:14:"refusal_edited";N;s:15:"no_footer_links";i:1;s:9:"action_id";N;s:22:"pre_refuse_action_type";N;s:18:"focus_extra_images";N;s:10:"loadaction";b:1;s:11:"has_gallery";N;s:11:"store_token";N;s:8:"store_id";N;s:8:"login_id";N;s:9:"addimages";N;s:8:"addvideo";N;s:8:"order_id";N;s:6:"params";s:29:"types,condition,prev_currency";s:15:"red_arrow_price";N;s:7:"voucher";N;s:14:"voucher_result";N;s:8:"ad_price";N;s:10:"area_unset";i:1;s:9:"link_type";N;s:12:"job_category";N;s:12:"service_type";N;s:9:"offseason";N;s:23:"offseason_checked_descr";N;s:10:"peakseason";N;s:24:"peakseason_checked_descr";N;s:15:"available_weeks";N;s:29:"available_weeks_checked_descr";N;s:13:"show_password";i:1;s:13:"salted_passwd";N;s:12:"hide_address";N;s:21:"allow_category_change";b:0;s:13:"video_enabled";i:0;s:10:"video_name";N;s:11:"video_width";N;s:12:"video_height";N;s:12:"video_length";N;s:11:"video_added";i:0;s:18:"already_paid_video";N;s:7:"set_uid";N;s:21:"add_gallery_from_mail";N;s:2:"ht";N;s:10:"del_reason";N;s:9:"user_text";N;s:21:"user_text_testimonial";N;s:9:"unf_ad_id";s:2:"10";s:14:"state_progress";s:7:"preview";s:12:"state_params";a:2:{s:2:"cl";b:0;s:2:"sp";N;}s:6:"caller";O:14:"blocket_caller":6:{s:6:"region";s:2:"15";s:4:"city";i:0;s:4:"type";s:1:"s";s:11:"default_set";b:1;s:10:"cookie_set";b:0;s:6:"layout";i:0;}s:13:"current_state";s:7:"preview";s:11:"instance_id";i:0;s:11:"static_vars";a:7:{i:0;s:10:"company_ad";i:1;s:4:"name";i:2;s:5:"email";i:3;s:5:"phone";i:4;s:12:"phone_hidden";i:5;s:4:"area";i:6;s:12:"hide_address";}s:13:"volatile_vars";a:12:{i:0;s:15:"no_footer_links";i:1;s:13:"video_enabled";i:2;s:11:"static_vars";i:3;s:13:"redirect_done";i:4;s:13:"volatile_vars";i:5;s:13:"current_state";i:6;s:11:"use_session";i:7;s:12:"state_params";i:8;s:14:"current_method";i:9;s:10:"navigation";i:10;s:9:"init_data";i:11;s:11:"extra_bconf";}s:13:"redirect_done";b:0;s:11:"use_session";b:1;s:6:"errors";a:0:{}s:8:"warnings";a:0:{}s:8:"messages";a:0:{}s:16:"application_name";s:2:"ai";s:15:"single_instance";N;s:14:"current_method";s:3:"get";s:10:"navigation";N;s:9:"init_data";N;s:11:"extra_bconf";N;s:4:"area";N;s:10:"headstyles";a:1:{i:0;s:6:"ai.css";}s:13:"ignore_fields";N;s:14:"show_body_warn";i:1;s:7:"gallery";b:0;}');
INSERT INTO unfinished_ads VALUES (14, 'mc1x95c7a5a1041ea2970efbea1148471eb5a276e4fa', 'dany@schibsted.cl', '2012-11-07 18:42:05.625938', '2012-11-07 18:42:20.444471', NULL, 'deleted', 'O:13:"blocket_newad":90:{s:2:"ad";O:3:"bAd":41:{s:6:"region";i:10;s:4:"area";N;s:12:"hide_address";N;s:10:"image_text";N;s:4:"type";s:1:"s";s:8:"category";i:2020;s:12:"sub_category";N;s:14:"category_group";i:2020;s:4:"name";s:4:"dany";s:5:"email";s:17:"dany@schibsted.cl";s:5:"phone";i:123123123;s:12:"phone_hidden";N;s:10:"company_ad";s:1:"0";s:5:"store";N;s:7:"subject";s:21:"Chevrolet Camaro 2000";s:4:"body";s:44:"Lo vendo por que gasta demasiado combustible";s:8:"infopage";N;s:14:"infopage_title";N;s:5:"price";i:2000000;s:6:"passwd";N;s:6:"coords";N;s:5:"state";N;s:6:"images";N;s:4:"lang";s:2:"es";s:16:"digests_presents";N;s:10:"thumbnails";N;s:17:"thumbnail_digests";N;s:7:"old_cat";N;s:4:"city";N;s:13:"prev_currency";s:4:"peso";s:7:"mileage";i:10000;s:7:"regdate";i:2000;s:7:"cartype";i:1;s:7:"gearbox";i:1;s:4:"fuel";i:1;s:5:"brand";i:18;s:5:"model";i:19;s:7:"version";i:2;s:25:"available_weeks_offseason";N;s:26:"available_weeks_peakseason";N;s:15:"available_weeks";N;}s:5:"ad_id";N;s:7:"list_id";N;s:6:"action";N;s:14:"refused_reason";N;s:20:"refused_refusal_text";N;s:25:"already_paid_extra_images";N;s:10:"company_ad";s:1:"0";s:4:"name";s:4:"dany";s:5:"email";s:17:"dany@schibsted.cl";s:5:"phone";s:9:"123123123";s:12:"phone_hidden";N;s:7:"account";i:0;s:4:"paid";N;s:6:"images";a:0:{}s:6:"stores";N;s:12:"is_new_image";N;s:16:"thumbnail_digest";N;s:14:"digest_present";N;s:9:"type_list";a:2:{i:0;s:1:"s";i:1;s:1:"k";}s:14:"orig_list_time";N;s:14:"refusal_edited";N;s:15:"no_footer_links";i:1;s:9:"action_id";N;s:22:"pre_refuse_action_type";N;s:18:"focus_extra_images";N;s:10:"loadaction";b:1;s:11:"has_gallery";N;s:11:"store_token";N;s:8:"store_id";N;s:8:"login_id";N;s:9:"addimages";N;s:8:"addvideo";N;s:8:"order_id";N;s:6:"params";s:82:"types,price,mileage,regdate,cartype,gearbox,fuel,prev_currency,brand,model,version";s:15:"red_arrow_price";N;s:7:"voucher";N;s:14:"voucher_result";N;s:8:"ad_price";N;s:10:"area_unset";i:1;s:9:"link_type";N;s:12:"job_category";N;s:12:"service_type";N;s:9:"offseason";N;s:23:"offseason_checked_descr";N;s:10:"peakseason";N;s:24:"peakseason_checked_descr";N;s:15:"available_weeks";N;s:29:"available_weeks_checked_descr";N;s:13:"show_password";i:1;s:13:"salted_passwd";N;s:12:"hide_address";N;s:21:"allow_category_change";b:0;s:13:"video_enabled";i:0;s:10:"video_name";N;s:11:"video_width";N;s:12:"video_height";N;s:12:"video_length";N;s:11:"video_added";i:0;s:18:"already_paid_video";N;s:7:"set_uid";N;s:21:"add_gallery_from_mail";N;s:2:"ht";N;s:10:"del_reason";N;s:9:"user_text";N;s:21:"user_text_testimonial";N;s:9:"unf_ad_id";N;s:14:"state_progress";s:7:"preview";s:12:"state_params";a:2:{s:2:"cl";b:0;s:2:"sp";N;}s:6:"caller";O:14:"blocket_caller":6:{s:6:"region";i:15;s:4:"city";i:0;s:4:"type";s:1:"s";s:11:"default_set";b:0;s:10:"cookie_set";b:0;s:6:"layout";i:0;}s:13:"current_state";s:7:"preview";s:11:"instance_id";i:0;s:11:"static_vars";a:7:{i:0;s:10:"company_ad";i:1;s:4:"name";i:2;s:5:"email";i:3;s:5:"phone";i:4;s:12:"phone_hidden";i:5;s:4:"area";i:6;s:12:"hide_address";}s:13:"volatile_vars";a:12:{i:0;s:15:"no_footer_links";i:1;s:13:"video_enabled";i:2;s:11:"static_vars";i:3;s:13:"redirect_done";i:4;s:13:"volatile_vars";i:5;s:13:"current_state";i:6;s:11:"use_session";i:7;s:12:"state_params";i:8;s:14:"current_method";i:9;s:10:"navigation";i:10;s:9:"init_data";i:11;s:11:"extra_bconf";}s:13:"redirect_done";b:0;s:11:"use_session";b:1;s:6:"errors";a:0:{}s:8:"warnings";a:0:{}s:8:"messages";a:0:{}s:16:"application_name";s:2:"ai";s:15:"single_instance";N;s:14:"current_method";s:3:"get";s:10:"navigation";N;s:9:"init_data";N;s:11:"extra_bconf";N;s:4:"area";N;s:10:"headstyles";a:1:{i:0;s:6:"ai.css";}s:13:"ignore_fields";N;s:14:"show_body_warn";i:1;s:15:"show_image_warn";i:1;}');
INSERT INTO unfinished_ads VALUES (12, 'mc1x829700d532d409fd06698a2f6b4e6362d30907da', 'boris@schibsted.cl', '2012-11-07 18:41:12.291564', '2012-11-07 18:41:24.690057', NULL, 'deleted', 'O:13:"blocket_newad":90:{s:2:"ad";O:3:"bAd":34:{s:6:"region";i:15;s:4:"area";N;s:12:"hide_address";N;s:10:"image_text";N;s:4:"type";s:1:"k";s:8:"category";i:5120;s:12:"sub_category";N;s:14:"category_group";i:5120;s:4:"name";s:13:"Boris Cruchet";s:5:"email";s:18:"boris@schibsted.cl";s:5:"phone";i:1234567890;s:12:"phone_hidden";N;s:10:"company_ad";s:1:"0";s:5:"store";N;s:7:"subject";s:25:"Busco articulos para bebe";s:4:"body";s:103:"Busco articulos para bebe
Busco articulos para bebe
Busco articulos para bebe
Busco articulos para bebe";s:8:"infopage";N;s:14:"infopage_title";N;s:5:"price";i:23000;s:6:"passwd";s:62:"$1024$.zo7{nd%M;<g[XD{9f8ad1aa6a34b7fa42056d161a5c2c3c342be980";s:6:"coords";N;s:5:"state";N;s:6:"images";N;s:4:"lang";s:2:"es";s:16:"digests_presents";N;s:10:"thumbnails";N;s:17:"thumbnail_digests";N;s:7:"old_cat";N;s:4:"city";N;s:13:"prev_currency";s:4:"peso";s:9:"condition";i:1;s:25:"available_weeks_offseason";N;s:26:"available_weeks_peakseason";N;s:15:"available_weeks";N;}s:5:"ad_id";N;s:7:"list_id";N;s:6:"action";N;s:14:"refused_reason";N;s:20:"refused_refusal_text";N;s:25:"already_paid_extra_images";N;s:10:"company_ad";s:1:"0";s:4:"name";s:13:"Boris Cruchet";s:5:"email";s:18:"boris@schibsted.cl";s:5:"phone";s:10:"1234567890";s:12:"phone_hidden";N;s:7:"account";i:0;s:4:"paid";N;s:6:"images";a:0:{}s:6:"stores";N;s:12:"is_new_image";N;s:16:"thumbnail_digest";N;s:14:"digest_present";N;s:9:"type_list";a:2:{i:0;s:1:"s";i:1;s:1:"k";}s:14:"orig_list_time";N;s:14:"refusal_edited";N;s:15:"no_footer_links";i:1;s:9:"action_id";N;s:22:"pre_refuse_action_type";N;s:18:"focus_extra_images";N;s:10:"loadaction";b:1;s:11:"has_gallery";N;s:11:"store_token";N;s:8:"store_id";N;s:8:"login_id";N;s:9:"addimages";N;s:8:"addvideo";N;s:8:"order_id";N;s:6:"params";s:29:"types,condition,prev_currency";s:15:"red_arrow_price";N;s:7:"voucher";N;s:14:"voucher_result";N;s:8:"ad_price";s:1:"0";s:10:"area_unset";i:1;s:9:"link_type";N;s:12:"job_category";N;s:12:"service_type";N;s:9:"offseason";N;s:23:"offseason_checked_descr";N;s:10:"peakseason";N;s:24:"peakseason_checked_descr";N;s:15:"available_weeks";N;s:29:"available_weeks_checked_descr";N;s:13:"show_password";i:1;s:13:"salted_passwd";N;s:12:"hide_address";N;s:21:"allow_category_change";b:0;s:13:"video_enabled";i:0;s:10:"video_name";N;s:11:"video_width";N;s:12:"video_height";N;s:12:"video_length";N;s:11:"video_added";i:0;s:18:"already_paid_video";N;s:7:"set_uid";N;s:21:"add_gallery_from_mail";N;s:2:"ht";N;s:10:"del_reason";N;s:9:"user_text";N;s:21:"user_text_testimonial";N;s:9:"unf_ad_id";s:2:"12";s:14:"state_progress";s:7:"preview";s:12:"state_params";a:2:{s:2:"cl";b:0;s:2:"sp";N;}s:6:"caller";O:14:"blocket_caller":6:{s:6:"region";s:2:"15";s:4:"city";i:0;s:4:"type";s:1:"s";s:11:"default_set";b:1;s:10:"cookie_set";b:0;s:6:"layout";i:0;}s:13:"current_state";s:7:"preview";s:11:"instance_id";i:0;s:11:"static_vars";a:7:{i:0;s:10:"company_ad";i:1;s:4:"name";i:2;s:5:"email";i:3;s:5:"phone";i:4;s:12:"phone_hidden";i:5;s:4:"area";i:6;s:12:"hide_address";}s:13:"volatile_vars";a:12:{i:0;s:15:"no_footer_links";i:1;s:13:"video_enabled";i:2;s:11:"static_vars";i:3;s:13:"redirect_done";i:4;s:13:"volatile_vars";i:5;s:13:"current_state";i:6;s:11:"use_session";i:7;s:12:"state_params";i:8;s:14:"current_method";i:9;s:10:"navigation";i:10;s:9:"init_data";i:11;s:11:"extra_bconf";}s:13:"redirect_done";b:0;s:11:"use_session";b:1;s:6:"errors";a:1:{s:17:"accept_conditions";s:23:"ERROR_ACCEPT_CONDITIONS";}s:8:"warnings";a:0:{}s:8:"messages";N;s:16:"application_name";s:2:"ai";s:15:"single_instance";N;s:14:"current_method";s:3:"get";s:10:"navigation";N;s:9:"init_data";N;s:11:"extra_bconf";N;s:4:"area";N;s:10:"headstyles";a:1:{i:0;s:6:"ai.css";}s:13:"ignore_fields";N;s:15:"show_image_warn";i:1;s:7:"gallery";b:0;}');
INSERT INTO unfinished_ads VALUES (16, 'mc1x829700d532d409fd06698a2f6b4e6362d30907da', 'boris@schibsted.cl', '2012-11-07 18:43:39.511629', '2012-11-07 18:43:48.055742', NULL, 'deleted', 'O:13:"blocket_newad":89:{s:2:"ad";O:3:"bAd":37:{s:6:"region";i:6;s:4:"area";N;s:12:"hide_address";N;s:10:"image_text";N;s:4:"type";s:1:"s";s:8:"category";i:2040;s:12:"sub_category";N;s:14:"category_group";i:2040;s:4:"name";s:13:"Boris Cruchet";s:5:"email";s:18:"boris@schibsted.cl";s:5:"phone";i:1234567890;s:12:"phone_hidden";N;s:10:"company_ad";i:1;s:5:"store";N;s:7:"subject";s:12:"Super camion";s:4:"body";s:328:"Test api Test api Test api Test api Test api Test api Test api Test api Test api Test api Test api Test api.

Test api Test api Test api Test api Test api Test api Test api Test api Test api Test api Test api Test api.

Test api Test api Test api Test api Test api Test api Test api Test api Test api Test api Test api Test api.";s:8:"infopage";N;s:14:"infopage_title";N;s:5:"price";i:7000000;s:6:"passwd";N;s:6:"coords";N;s:5:"state";N;s:6:"images";N;s:4:"lang";s:2:"es";s:16:"digests_presents";N;s:10:"thumbnails";N;s:17:"thumbnail_digests";N;s:7:"old_cat";N;s:4:"city";N;s:13:"prev_currency";s:4:"peso";s:7:"mileage";i:400000;s:7:"regdate";i:2010;s:7:"gearbox";i:1;s:4:"fuel";i:4;s:25:"available_weeks_offseason";N;s:26:"available_weeks_peakseason";N;s:15:"available_weeks";N;}s:5:"ad_id";N;s:7:"list_id";N;s:6:"action";N;s:14:"refused_reason";N;s:20:"refused_refusal_text";N;s:25:"already_paid_extra_images";N;s:10:"company_ad";i:1;s:4:"name";s:13:"Boris Cruchet";s:5:"email";s:18:"boris@schibsted.cl";s:5:"phone";s:10:"1234567890";s:12:"phone_hidden";N;s:7:"account";i:0;s:4:"paid";N;s:6:"images";a:0:{}s:6:"stores";N;s:12:"is_new_image";N;s:16:"thumbnail_digest";N;s:14:"digest_present";N;s:9:"type_list";a:2:{i:0;s:1:"s";i:1;s:1:"k";}s:14:"orig_list_time";N;s:14:"refusal_edited";N;s:15:"no_footer_links";i:1;s:9:"action_id";N;s:22:"pre_refuse_action_type";N;s:18:"focus_extra_images";N;s:10:"loadaction";b:1;s:11:"has_gallery";N;s:11:"store_token";N;s:8:"store_id";N;s:8:"login_id";N;s:9:"addimages";N;s:8:"addvideo";N;s:8:"order_id";N;s:6:"params";s:54:"types,price,mileage,regdate,gearbox,fuel,prev_currency";s:15:"red_arrow_price";N;s:7:"voucher";N;s:14:"voucher_result";N;s:8:"ad_price";N;s:10:"area_unset";i:1;s:9:"link_type";N;s:12:"job_category";N;s:12:"service_type";N;s:9:"offseason";N;s:23:"offseason_checked_descr";N;s:10:"peakseason";N;s:24:"peakseason_checked_descr";N;s:15:"available_weeks";N;s:29:"available_weeks_checked_descr";N;s:13:"show_password";i:1;s:13:"salted_passwd";N;s:12:"hide_address";N;s:21:"allow_category_change";b:0;s:13:"video_enabled";i:0;s:10:"video_name";N;s:11:"video_width";N;s:12:"video_height";N;s:12:"video_length";N;s:11:"video_added";i:0;s:18:"already_paid_video";N;s:7:"set_uid";N;s:21:"add_gallery_from_mail";N;s:2:"ht";N;s:10:"del_reason";N;s:9:"user_text";N;s:21:"user_text_testimonial";N;s:9:"unf_ad_id";N;s:14:"state_progress";s:7:"preview";s:12:"state_params";a:2:{s:2:"cl";b:0;s:2:"sp";N;}s:6:"caller";O:14:"blocket_caller":6:{s:6:"region";s:2:"15";s:4:"city";i:0;s:4:"type";s:1:"s";s:11:"default_set";b:1;s:10:"cookie_set";b:0;s:6:"layout";i:0;}s:13:"current_state";s:7:"preview";s:11:"instance_id";i:0;s:11:"static_vars";a:7:{i:0;s:10:"company_ad";i:1;s:4:"name";i:2;s:5:"email";i:3;s:5:"phone";i:4;s:12:"phone_hidden";i:5;s:4:"area";i:6;s:12:"hide_address";}s:13:"volatile_vars";a:12:{i:0;s:15:"no_footer_links";i:1;s:13:"video_enabled";i:2;s:11:"static_vars";i:3;s:13:"redirect_done";i:4;s:13:"volatile_vars";i:5;s:13:"current_state";i:6;s:11:"use_session";i:7;s:12:"state_params";i:8;s:14:"current_method";i:9;s:10:"navigation";i:10;s:9:"init_data";i:11;s:11:"extra_bconf";}s:13:"redirect_done";b:0;s:11:"use_session";b:1;s:6:"errors";a:0:{}s:8:"warnings";a:0:{}s:8:"messages";a:0:{}s:16:"application_name";s:2:"ai";s:15:"single_instance";N;s:14:"current_method";s:3:"get";s:10:"navigation";N;s:9:"init_data";N;s:11:"extra_bconf";N;s:4:"area";N;s:10:"headstyles";a:1:{i:0;s:6:"ai.css";}s:13:"ignore_fields";N;s:15:"show_image_warn";i:1;}');
INSERT INTO unfinished_ads VALUES (13, 'mc1x829700d532d409fd06698a2f6b4e6362d30907da', 'boris@schibsted.cl', '2012-11-07 18:41:59.395518', '2012-11-07 18:42:05.605811', NULL, 'deleted', 'O:13:"blocket_newad":89:{s:2:"ad";O:3:"bAd":35:{s:6:"region";i:5;s:4:"area";N;s:12:"hide_address";N;s:10:"image_text";N;s:4:"type";s:1:"s";s:8:"category";i:5100;s:12:"sub_category";N;s:14:"category_group";i:5100;s:4:"name";s:13:"Boris Cruchet";s:5:"email";s:18:"boris@schibsted.cl";s:5:"phone";i:1234567890;s:12:"phone_hidden";N;s:10:"company_ad";s:1:"0";s:5:"store";N;s:7:"subject";s:20:"Terno de lana de yak";s:4:"body";s:104:"Terno de lana de yak
Terno de lana de yak
Terno de lana de yak
Terno de lana de yak
Terno de lana de yak";s:8:"infopage";N;s:14:"infopage_title";N;s:5:"price";i:1200000;s:6:"passwd";N;s:6:"coords";N;s:5:"state";N;s:6:"images";N;s:4:"lang";s:2:"es";s:16:"digests_presents";N;s:10:"thumbnails";N;s:17:"thumbnail_digests";N;s:7:"old_cat";N;s:4:"city";N;s:13:"prev_currency";s:4:"peso";s:9:"condition";i:2;s:6:"gender";i:2;s:25:"available_weeks_offseason";N;s:26:"available_weeks_peakseason";N;s:15:"available_weeks";N;}s:5:"ad_id";N;s:7:"list_id";N;s:6:"action";N;s:14:"refused_reason";N;s:20:"refused_refusal_text";N;s:25:"already_paid_extra_images";N;s:10:"company_ad";s:1:"0";s:4:"name";s:13:"Boris Cruchet";s:5:"email";s:18:"boris@schibsted.cl";s:5:"phone";s:10:"1234567890";s:12:"phone_hidden";N;s:7:"account";i:0;s:4:"paid";N;s:6:"images";a:0:{}s:6:"stores";N;s:12:"is_new_image";N;s:16:"thumbnail_digest";N;s:14:"digest_present";N;s:9:"type_list";a:2:{i:0;s:1:"s";i:1;s:1:"k";}s:14:"orig_list_time";N;s:14:"refusal_edited";N;s:15:"no_footer_links";i:1;s:9:"action_id";N;s:22:"pre_refuse_action_type";N;s:18:"focus_extra_images";N;s:10:"loadaction";b:1;s:11:"has_gallery";N;s:11:"store_token";N;s:8:"store_id";N;s:8:"login_id";N;s:9:"addimages";N;s:8:"addvideo";N;s:8:"order_id";N;s:6:"params";s:36:"types,condition,gender,prev_currency";s:15:"red_arrow_price";N;s:7:"voucher";N;s:14:"voucher_result";N;s:8:"ad_price";N;s:10:"area_unset";i:1;s:9:"link_type";N;s:12:"job_category";N;s:12:"service_type";N;s:9:"offseason";N;s:23:"offseason_checked_descr";N;s:10:"peakseason";N;s:24:"peakseason_checked_descr";N;s:15:"available_weeks";N;s:29:"available_weeks_checked_descr";N;s:13:"show_password";i:1;s:13:"salted_passwd";N;s:12:"hide_address";N;s:21:"allow_category_change";b:0;s:13:"video_enabled";i:0;s:10:"video_name";N;s:11:"video_width";N;s:12:"video_height";N;s:12:"video_length";N;s:11:"video_added";i:0;s:18:"already_paid_video";N;s:7:"set_uid";N;s:21:"add_gallery_from_mail";N;s:2:"ht";N;s:10:"del_reason";N;s:9:"user_text";N;s:21:"user_text_testimonial";N;s:9:"unf_ad_id";N;s:14:"state_progress";s:7:"preview";s:12:"state_params";a:2:{s:2:"cl";b:0;s:2:"sp";N;}s:6:"caller";O:14:"blocket_caller":6:{s:6:"region";s:2:"15";s:4:"city";i:0;s:4:"type";s:1:"s";s:11:"default_set";b:1;s:10:"cookie_set";b:0;s:6:"layout";i:0;}s:13:"current_state";s:7:"preview";s:11:"instance_id";i:0;s:11:"static_vars";a:7:{i:0;s:10:"company_ad";i:1;s:4:"name";i:2;s:5:"email";i:3;s:5:"phone";i:4;s:12:"phone_hidden";i:5;s:4:"area";i:6;s:12:"hide_address";}s:13:"volatile_vars";a:12:{i:0;s:15:"no_footer_links";i:1;s:13:"video_enabled";i:2;s:11:"static_vars";i:3;s:13:"redirect_done";i:4;s:13:"volatile_vars";i:5;s:13:"current_state";i:6;s:11:"use_session";i:7;s:12:"state_params";i:8;s:14:"current_method";i:9;s:10:"navigation";i:10;s:9:"init_data";i:11;s:11:"extra_bconf";}s:13:"redirect_done";b:0;s:11:"use_session";b:1;s:6:"errors";a:0:{}s:8:"warnings";a:0:{}s:8:"messages";a:0:{}s:16:"application_name";s:2:"ai";s:15:"single_instance";N;s:14:"current_method";s:3:"get";s:10:"navigation";N;s:9:"init_data";N;s:11:"extra_bconf";N;s:4:"area";N;s:10:"headstyles";a:1:{i:0;s:6:"ai.css";}s:13:"ignore_fields";N;s:15:"show_image_warn";i:1;}');
INSERT INTO unfinished_ads VALUES (15, 'mc1x95c7a5a1041ea2970efbea1148471eb5a276e4fa', 'dany@schibsted.cl', '2012-11-07 18:43:15.697077', '2012-11-07 18:43:22.545503', NULL, 'deleted', 'O:13:"blocket_newad":90:{s:2:"ad";O:3:"bAd":36:{s:6:"region";i:10;s:4:"area";N;s:12:"hide_address";N;s:10:"image_text";N;s:4:"type";s:1:"s";s:8:"category";i:2060;s:12:"sub_category";N;s:14:"category_group";i:2060;s:4:"name";s:4:"dany";s:5:"email";s:17:"dany@schibsted.cl";s:5:"phone";i:123123123;s:12:"phone_hidden";N;s:10:"company_ad";s:1:"0";s:5:"store";N;s:7:"subject";s:11:"Moto Kinlon";s:4:"body";s:29:"estupenda moto papeles al dia";s:8:"infopage";N;s:14:"infopage_title";N;s:5:"price";i:600000;s:6:"passwd";N;s:6:"coords";N;s:5:"state";N;s:6:"images";N;s:4:"lang";s:2:"es";s:16:"digests_presents";N;s:10:"thumbnails";N;s:17:"thumbnail_digests";N;s:7:"old_cat";N;s:4:"city";N;s:13:"prev_currency";s:4:"peso";s:7:"mileage";i:10000;s:7:"regdate";i:2005;s:8:"cubiccms";i:2;s:25:"available_weeks_offseason";N;s:26:"available_weeks_peakseason";N;s:15:"available_weeks";N;}s:5:"ad_id";N;s:7:"list_id";N;s:6:"action";N;s:14:"refused_reason";N;s:20:"refused_refusal_text";N;s:25:"already_paid_extra_images";N;s:10:"company_ad";s:1:"0";s:4:"name";s:4:"dany";s:5:"email";s:17:"dany@schibsted.cl";s:5:"phone";s:9:"123123123";s:12:"phone_hidden";N;s:7:"account";i:0;s:4:"paid";N;s:6:"images";a:0:{}s:6:"stores";N;s:12:"is_new_image";N;s:16:"thumbnail_digest";N;s:14:"digest_present";N;s:9:"type_list";a:2:{i:0;s:1:"s";i:1;s:1:"k";}s:14:"orig_list_time";N;s:14:"refusal_edited";N;s:15:"no_footer_links";i:1;s:9:"action_id";N;s:22:"pre_refuse_action_type";N;s:18:"focus_extra_images";N;s:10:"loadaction";b:1;s:11:"has_gallery";N;s:11:"store_token";N;s:8:"store_id";N;s:8:"login_id";N;s:9:"addimages";N;s:8:"addvideo";N;s:8:"order_id";N;s:6:"params";s:50:"types,price,mileage,regdate,cubiccms,prev_currency";s:15:"red_arrow_price";N;s:7:"voucher";N;s:14:"voucher_result";N;s:8:"ad_price";N;s:10:"area_unset";i:1;s:9:"link_type";N;s:12:"job_category";N;s:12:"service_type";N;s:9:"offseason";N;s:23:"offseason_checked_descr";N;s:10:"peakseason";N;s:24:"peakseason_checked_descr";N;s:15:"available_weeks";N;s:29:"available_weeks_checked_descr";N;s:13:"show_password";i:1;s:13:"salted_passwd";N;s:12:"hide_address";N;s:21:"allow_category_change";b:0;s:13:"video_enabled";i:0;s:10:"video_name";N;s:11:"video_width";N;s:12:"video_height";N;s:12:"video_length";N;s:11:"video_added";i:0;s:18:"already_paid_video";N;s:7:"set_uid";N;s:21:"add_gallery_from_mail";N;s:2:"ht";N;s:10:"del_reason";N;s:9:"user_text";N;s:21:"user_text_testimonial";N;s:9:"unf_ad_id";N;s:14:"state_progress";s:7:"preview";s:12:"state_params";a:2:{s:2:"cl";b:0;s:2:"sp";N;}s:6:"caller";O:14:"blocket_caller":6:{s:6:"region";i:15;s:4:"city";i:0;s:4:"type";s:1:"s";s:11:"default_set";b:0;s:10:"cookie_set";b:0;s:6:"layout";i:0;}s:13:"current_state";s:7:"preview";s:11:"instance_id";i:0;s:11:"static_vars";a:7:{i:0;s:10:"company_ad";i:1;s:4:"name";i:2;s:5:"email";i:3;s:5:"phone";i:4;s:12:"phone_hidden";i:5;s:4:"area";i:6;s:12:"hide_address";}s:13:"volatile_vars";a:12:{i:0;s:15:"no_footer_links";i:1;s:13:"video_enabled";i:2;s:11:"static_vars";i:3;s:13:"redirect_done";i:4;s:13:"volatile_vars";i:5;s:13:"current_state";i:6;s:11:"use_session";i:7;s:12:"state_params";i:8;s:14:"current_method";i:9;s:10:"navigation";i:10;s:9:"init_data";i:11;s:11:"extra_bconf";}s:13:"redirect_done";b:0;s:11:"use_session";b:1;s:6:"errors";a:0:{}s:8:"warnings";a:0:{}s:8:"messages";a:0:{}s:16:"application_name";s:2:"ai";s:15:"single_instance";N;s:14:"current_method";s:3:"get";s:10:"navigation";N;s:9:"init_data";N;s:11:"extra_bconf";N;s:4:"area";N;s:10:"headstyles";a:1:{i:0;s:6:"ai.css";}s:13:"ignore_fields";N;s:14:"show_body_warn";i:1;s:15:"show_image_warn";i:1;}');


--
-- Data for Name: user_params; Type: TABLE DATA; Schema: public; Owner: dany
--

INSERT INTO user_params VALUES (50, 'first_approved_ad', '2012-11-07 18:44:32.076978-03');
INSERT INTO user_params VALUES (51, 'first_approved_ad', '2012-11-07 18:44:34.906444-03');

INSERT INTO user_params VALUES (50, 'first_inserted_ad', '2012-11-07 18:44:32.076978-03');
INSERT INTO user_params VALUES (51, 'first_inserted_ad', '2012-11-07 18:44:34.906444-03');


--
-- Data for Name: user_testimonial; Type: TABLE DATA; Schema: public; Owner: dany
--



--
-- Data for Name: visitor; Type: TABLE DATA; Schema: public; Owner: dany
--



--
-- Data for Name: visits; Type: TABLE DATA; Schema: public; Owner: dany
--



--
-- Data for Name: vouchers; Type: TABLE DATA; Schema: public; Owner: dany
--



--
-- Data for Name: voucher_actions; Type: TABLE DATA; Schema: public; Owner: dany
--



--
-- Data for Name: voucher_states; Type: TABLE DATA; Schema: public; Owner: dany
--



--
-- Data for Name: watch_ads; Type: TABLE DATA; Schema: public; Owner: dany
--



--
-- PostgreSQL database dump complete
--

INSERT INTO ad_params VALUES (2, 'communes', '233');
INSERT INTO ad_params VALUES (3, 'communes', '236');
INSERT INTO ad_params VALUES (4, 'communes', '243');
INSERT INTO ad_params VALUES (5, 'communes', '234');
INSERT INTO ad_params VALUES (6, 'communes', '216');
INSERT INTO ad_params VALUES (7, 'communes', '233');
INSERT INTO ad_params VALUES (8, 'communes', '243');
INSERT INTO ad_params VALUES (9, 'communes', '234');
INSERT INTO ad_params VALUES (10, 'communes', '272');
INSERT INTO ad_params VALUES (11, 'communes', '283');
INSERT INTO ad_params VALUES (31, 'communes', '319');
INSERT INTO ad_params VALUES (20, 'communes', '243');
INSERT INTO ad_params VALUES (26, 'communes', '139');
INSERT INTO ad_params VALUES (22, 'communes', '232');
INSERT INTO ad_params VALUES (24, 'communes', '297');
INSERT INTO ad_params VALUES (29, 'communes', '333');

--Migrate inmo ads
select * from bpv.migrate_ads_inmo();
