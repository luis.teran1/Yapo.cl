
[ -z "$IPV6_MIGRATE" ] && exit 0

[ -z "$TOPDIR" -o -z "$PGHOST" -o -z "$USER" ] && ( echo "TOPDIR, PGHOST and USER must be set" ; exit 1 )

export CLUSTERNAME=bcluster
export MASTERDBNAME=blocketdb
export SLAVEDBNAME=repl
export MASTERHOST=$PGHOST
export SLAVEHOST=$PGHOST
export REPLICATIONUSER=$USER
export BPVSCHEMA=bpv

TOPDIR=$(cd $TOPDIR ; pwd)

set -e

echo -e "\e[33mCleaning DB (ugh)\e[0m"
psql -h "$MASTERHOST" "$MASTERDBNAME" -c "
ALTER TABLE action_states ALTER remote_addr SET DEFAULT NULL;
UPDATE action_states SET remote_addr = NULL WHERE remote_addr !~ E'(\\\\d+\\\\.){3}\\\\d+';
ALTER TABLE tokens ALTER remote_addr DROP NOT NULL, ALTER remote_addr SET DEFAULT NULL; -- not sure if needed in prod
UPDATE tokens SET remote_addr = NULL WHERE remote_addr !~ E'(\\\\d+\\\\.){3}\\\\d+'; -- not sure if needed in prod
"

echo -e "\e[33mDuplicating db\e[0m"
psql -h "$SLAVEHOST" -c "CREATE DATABASE $SLAVEDBNAME;"
psql -h "$SLAVEHOST" "$SLAVEDBNAME" -c "CREATE LANGUAGE plpgsql;"
pg_dump -s -U $REPLICATIONUSER -h $MASTERHOST --schema=public $MASTERDBNAME --no-owner --no-acl --exclude-table denounced_ads_valid | psql -U $REPLICATIONUSER -h $SLAVEHOST $SLAVEDBNAME | sort | uniq -c
# Simply copy the archives.
for year in $(seq 2004 $(($(date +%Y) +1))); do
	pg_dump -U $REPLICATIONUSER -h $MASTERHOST --schema="blocket_$year" --format=custom $MASTERDBNAME --no-owner --no-acl | pg_restore -U $REPLICATIONUSER -h $SLAVEHOST --format=custom --dbname=$SLAVEDBNAME --no-owner --no-acl
done | sort | uniq -c

echo -e "\e[33mUpdating column types\e[0m"

psql -h "$SLAVEHOST" "$SLAVEDBNAME" -c "
ALTER TABLE tokens ALTER remote_addr DROP NOT NULL, ALTER remote_addr DROP DEFAULT, ALTER remote_addr TYPE inet USING remote_addr::inet;
ALTER TABLE store_login_tokens ALTER remote_addr TYPE inet USING remote_addr::inet;
ALTER TABLE voucher_states ALTER remote_addr TYPE inet USING remote_addr::inet;
ALTER TABLE action_states ALTER remote_addr DROP DEFAULT, ALTER remote_addr TYPE inet USING remote_addr::inet;
ALTER TABLE mail_log ALTER remote_addr TYPE inet USING remote_addr::inet;
ALTER TABLE mail_queue ALTER remote_addr TYPE inet USING remote_addr::inet;
ALTER TABLE bid_bids ALTER remote_addr TYPE inet USING remote_addr::inet;
"
 
echo -e "\e[33mStarting slony\e[0m"
sh $TOPDIR/platform/scripts/db/startslony.sh existing

echo -e "\e[33mWaiting for replication to catch up\e[0m"
slonik <<_EOF_
cluster name = $CLUSTERNAME;

node 1 admin conninfo = 'dbname=$MASTERDBNAME host=$MASTERHOST user=$REPLICATIONUSER';
node 2 admin conninfo = 'dbname=$SLAVEDBNAME host=$SLAVEHOST user=$REPLICATIONUSER';

wait for event (origin = 1, confirmed = 2);
_EOF_

echo -e "\e[33mStopping trans\e[0m"
if [ -z "$notfinal" ]; then
	make -C $TOPDIR trans-regress-stop
else
	make trans-test-stop
fi

echo -e "\e[33mSwitching master\e[0m"

slonik <<_EOF_
cluster name = $CLUSTERNAME;

node 1 admin conninfo = 'dbname=$MASTERDBNAME host=$MASTERHOST user=$REPLICATIONUSER';
node 2 admin conninfo = 'dbname=$SLAVEDBNAME host=$SLAVEHOST user=$REPLICATIONUSER';

lock set (id = 1, origin = 1);
wait for event (origin = 1, confirmed = 2);
move set (id = 1, old origin = 1, new origin = 2);
wait for event (origin = 1, confirmed = 2);
_EOF_

echo -e "\e[33mStarting trans (skipped)\e[0m"
#sed -i -e 's/name = blocketdb/name = repl/' ${TOPDIR}/regress_final/conf/trans.conf
# XXX need to populate the dbprocs here or trans won't start
#make -C $TOPDIR trans-regress-start

echo -e "\e[33mDropping $MASTERDBNAME\e[0m"

slonik <<_EOF_
cluster name = $CLUSTERNAME;

node 2 admin conninfo = 'dbname=$SLAVEDBNAME host=$SLAVEHOST user=$REPLICATIONUSER';
node 1 admin conninfo = 'dbname=$MASTERDBNAME host=$MASTERHOST user=$REPLICATIONUSER';

drop node ( id = 1, event node = 2 );
wait for event (origin = 2, confirmed = 1);
_EOF_

kill $(< $MASTERHOST/slony_master_pid)

psql -U $REPLICATIONUSER -h "$SLAVEHOST" $SLAVEDBNAME -c "DROP DATABASE blocketdb;"

echo -e "\e[33mRegenerating $MASTERDBNAME\e[0m"

export DBPATCH="
ALTER TABLE tokens ALTER remote_addr DROP NOT NULL, ALTER remote_addr DROP DEFAULT, ALTER remote_addr TYPE inet USING remote_addr::inet;
ALTER TABLE store_login_tokens ALTER remote_addr TYPE inet USING remote_addr::inet;
ALTER TABLE voucher_states ALTER remote_addr TYPE inet USING remote_addr::inet;
ALTER TABLE action_states ALTER remote_addr DROP DEFAULT, ALTER remote_addr TYPE inet USING remote_addr::inet;
ALTER TABLE mail_log ALTER remote_addr TYPE inet USING remote_addr::inet;
ALTER TABLE mail_queue ALTER remote_addr TYPE inet USING remote_addr::inet;
ALTER TABLE bid_bids ALTER remote_addr TYPE inet USING remote_addr::inet;
"

cd $TOPDIR/scripts/db
psql -U $REPLICATIONUSER -h "$SLAVEHOST" "$SLAVEDBNAME" -c "REASSIGN OWNED BY breaders TO $REPLICATIONUSER"
psql -U $REPLICATIONUSER -h "$SLAVEHOST" "$SLAVEDBNAME" -c "REASSIGN OWNED BY bwriters TO $REPLICATIONUSER"
existing_db=1 PGDATA=$MASTERHOST ./start-build-pgsql.sh $MASTERHOST 1 1 || true
cd -

echo -e "\e[33mReplicating back\e[0m"

slonik <<_EOF_
cluster name = $CLUSTERNAME;

node 1 admin conninfo = 'dbname=$MASTERDBNAME host=$MASTERHOST user=$REPLICATIONUSER';
node 2 admin conninfo = 'dbname=$SLAVEDBNAME host=$SLAVEHOST user=$REPLICATIONUSER';

store node (id = 1, comment = 'Orig node', event node=2);
store path (server = 1, client = 2, conninfo='dbname=$MASTERDBNAME host=$MASTERHOST user=$REPLICATIONUSER');
store path (server = 2, client = 1, conninfo='dbname=$SLAVEDBNAME host=$SLAVEHOST user=$REPLICATIONUSER');
_EOF_

slon $CLUSTERNAME "dbname=$MASTERDBNAME user=$REPLICATIONUSER host=$MASTERHOST" < /dev/null > $MASTERHOST/slony_master 2>&1 &

# Simply copy the archives.
for year in $(seq 2004 $(($(date +%Y) +1))); do
	pg_dump -a -U $REPLICATIONUSER -h $SLAVEHOST --schema="blocket_$year" --format=custom $SLAVEDBNAME --no-owner --no-acl | pg_restore -U $REPLICATIONUSER -h $MASTERHOST --format=custom --dbname=$MASTERDBNAME --no-owner --no-acl
done | sort | uniq -c

slonik <<_EOF_
	 cluster name = $CLUSTERNAME;

	 node 1 admin conninfo = 'dbname=$MASTERDBNAME host=$MASTERHOST user=$REPLICATIONUSER';
	 node 2 admin conninfo = 'dbname=$SLAVEDBNAME host=$SLAVEHOST user=$REPLICATIONUSER';

	 subscribe set ( id = 1, provider = 2, receiver = 1, forward = no);
_EOF_

echo -e "\e[33mStopping trans (skipped)\e[0m"
#make -C $TOPDIR trans-regress-stop

echo -e "\e[33mStopping slony\e[0m"

slonik <<_EOF_
cluster name = $CLUSTERNAME;

node 2 admin conninfo = 'dbname=$SLAVEDBNAME host=$SLAVEHOST user=$REPLICATIONUSER';
node 1 admin conninfo = 'dbname=$MASTERDBNAME host=$MASTERHOST user=$REPLICATIONUSER';

drop node ( id = 1, event node = 2 );
wait for event (origin = 2, confirmed = 1);
_EOF_

killall slon

echo -e "\e[33mDropping $SLAVEDBNAME\e[0m"

psql -U $REPLICATIONUSER -h "$MASTERHOST" $MASTERDBNAME -c "DROP DATABASE $SLAVEDBNAME;"

echo -e "\e[33mResetting sequnces for regression tests (not needed in prod)\e[0m"
for seq in `psql -U $REPLICATIONUSER -h "$MASTERHOST" $MASTERDBNAME -tA -c "SELECT c.relname FROM pg_catalog.pg_class c LEFT JOIN pg_catalog.pg_namespace n ON n.oid = c.relnamespace WHERE c.relkind IN ('S','') AND n.nspname = 'public' AND c.relname NOT IN ('mail_log_mail_log_id_seq', 'block_rule_conditions_condition_id_seq', 'invoices_invoice_id_seq', 'payment_history_payment_id_seq', 'stores_store_id_seq', 'boutiques_boutiques_seq_id_seq', 'boutiques_store_list_id_seq', 'reply_tracking_seq', 'pay_log_pay_log_id')"` ; do
	if [ $seq != "mail_log_mail_log_id_seq" ]; then
		lval=`psql -U $REPLICATIONUSER -h "$MASTERHOST" $MASTERDBNAME -tA -c "SELECT last_value FROM $seq"`
		psql -U $REPLICATIONUSER -h "$MASTERHOST" $MASTERDBNAME -c "ALTER SEQUENCE $seq RESTART WITH $lval;"
	fi
done | uniq -c

echo -e "\e[33mStarting trans\e[0m"
if [ -z "$notfinal" ]; then
	#sed -i -e 's/name = repl/name = blocketdb/' ${TOPDIR}/regress_final/conf/trans.conf
	sed -i -e 's/name = repl/name = blocketdb/' ${TOPDIR}/regress_final/conf/trans.conf
	make -C $TOPDIR trans-regress-start
else
	make trans-test-start
fi

