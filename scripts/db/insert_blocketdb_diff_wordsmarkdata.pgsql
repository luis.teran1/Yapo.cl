INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (716, 'X591089a1607766460000000076a935de00000000', 9, '2017-05-08 12:07:13.045759', '2017-05-08 12:08:03.131359', '10.0.10.169', 'spamfilter', NULL, NULL);
SELECT pg_catalog.setval('tokens_token_id_seq', 730, false);

--
-- Data for Name: conf; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.controlpanel.modules.adqueue.highlight.text.1.categories', '*', '2017-05-08 12:08:03.133944', 716);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.controlpanel.modules.adqueue.highlight.text.1.types', '*', '2017-05-08 12:08:03.133944', 716);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.controlpanel.modules.adqueue.highlight.text.1.where', 'subject,body', '2017-05-08 12:08:03.133944', 716);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.controlpanel.modules.adqueue.highlight.text.1.regex', '(two|one){1,2}', '2017-05-08 12:08:03.133944', 716);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.controlpanel.modules.adqueue.highlight.text.1.word_boundary', '0', '2017-05-08 12:08:03.133944', 716);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.controlpanel.modules.adqueue.highlight.text.1.frame_ad', '0', '2017-05-08 12:08:03.133944', 716);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.controlpanel.modules.adqueue.highlight.text.1.warning_type', 'WARNING_PHONE_IN_DESCRIPTION', '2017-05-08 12:08:03.133944', 716);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.controlpanel.modules.adqueue.highlight.text.1.pure_regex', '1', '2017-05-08 12:08:03.133944', 716);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.controlpanel.modules.adqueue.highlight.text.1.color', '0', '2017-05-08 12:08:03.133944', 716);

INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.controlpanel.modules.adqueue.highlight.text.2.categories', '*', '2017-05-08 12:08:03.133944', 716);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.controlpanel.modules.adqueue.highlight.text.2.types', '*', '2017-05-08 12:08:03.133944', 716);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.controlpanel.modules.adqueue.highlight.text.2.where', 'subject,body', '2017-05-08 12:08:03.133944', 716);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.controlpanel.modules.adqueue.highlight.text.2.regex', 'or', '2017-05-08 12:08:03.133944', 716);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.controlpanel.modules.adqueue.highlight.text.2.word_boundary', '0', '2017-05-08 12:08:03.133944', 716);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.controlpanel.modules.adqueue.highlight.text.2.frame_ad', '0', '2017-05-08 12:08:03.133944', 716);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.controlpanel.modules.adqueue.highlight.text.2.color', '1', '2017-05-08 12:08:03.133944', 716);
