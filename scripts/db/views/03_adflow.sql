CREATE OR REPLACE VIEW v_adflow AS
SELECT
	ads.*,
	action_id,
	payment_groups.code,
	payment_groups.status AS payments_status,
	users.email
FROM
	ads JOIN 
	ad_actions USING (ad_id)
	JOIN users USING (user_id)
	LEFT JOIN payment_groups USING (payment_group_id)
ORDER BY
	ads.ad_id;

GRANT select,insert,delete,update ON v_adflow TO bwriters;
GRANT select ON v_adflow TO breaders;
