CREATE OR REPLACE VIEW v_ads AS
SELECT 
	ad_id,
	list_id,
	list_time,
	status::varchar,
	type::varchar,
	name,
	phone,
	region,
	city,
	category,
	user_id,
	salted_passwd,
	phone_hidden,
	no_salesmen,
	company_ad,
	subject,
	body,
	price,
	infopage,
	infopage_title,
	store_id
FROM 
	public.ads
UNION ALL
SELECT 
	ad_id,
	list_id,
	list_time,
	status::varchar,
	type::varchar,
	name,
	phone,
	region,
	city,
	category,
	user_id,
	salted_passwd,
	phone_hidden,
	no_salesmen,
	company_ad,
	subject,
	body,
	price,
	infopage,
	infopage_title,
	store_id
FROM 
	blocket_2010.ads;

GRANT select,insert,delete,update ON v_ads to bwriters;
GRANT select on v_ads TO breaders;

CREATE OR REPLACE VIEW v_pay_log AS
SELECT 
	pay_log_id,
	pay_type::varchar,
	amount,
	code,
	token_id,
	paid_at,
	payment_group_id,
	status::varchar
FROM
	public.pay_log
UNION ALL 
SELECT 
	pay_log_id,
	pay_type::varchar,
	amount,
	code,
	token_id,
	paid_at,
	payment_group_id,
	status::varchar
FROM 
	blocket_2010.pay_log;

GRANT select,insert,delete,update ON v_pay_log to bwriters;
GRANT select on v_pay_log TO breaders;

CREATE OR REPLACE VIEW v_pay_log_references AS
SELECT
	pay_log_id,
	ref_num,
	ref_type::varchar,
	reference
FROM
	public.pay_log_references
UNION ALL
SELECT
	pay_log_id,
	ref_num,
	ref_type::varchar,
	reference
FROM
	blocket_2010.pay_log_references;

GRANT select,insert,delete,update ON v_pay_log_references to bwriters;
GRANT select on v_pay_log_references TO breaders;

CREATE OR REPLACE VIEW v_ad_actions AS
SELECT 
	ad_id,
       	action_id, 
	action_type::varchar, 
	current_state, 
	state::varchar, 
	queue::varchar, 
	locked_by, 
	locked_until, 
	payment_group_id
FROM 
	public.ad_actions
UNION ALL
SELECT 
	ad_id,
       	action_id, 
	action_type::varchar, 
	current_state, 
	state::varchar, 
	queue::varchar, 
	locked_by, 
	locked_until, 
	payment_group_id
FROM 
	blocket_2010.ad_actions;

GRANT select,insert,delete,update ON v_ad_actions to bwriters;
GRANT select on v_ad_actions TO breaders;

CREATE OR REPLACE VIEW v_payments AS
SELECT 
	payment_group_id,
	payment_type::varchar,
	pay_amount AS price,
	discount
FROM 
	public.payments
UNION ALL 
SELECT 
	payment_group_id,
	payment_type::varchar,
	pay_amount AS price,
	discount
FROM 
	blocket_2010.payments;

GRANT select,insert,delete,update ON v_payments to bwriters;
GRANT select on v_payments TO breaders;

CREATE OR REPLACE VIEW v_ad_params AS
SELECT 
	ad_id,
	name::varchar,
	value
FROM
	public.ad_params
UNION ALL
SELECT 
	ad_id,
	name::varchar,
	value
FROM
	blocket_2010.ad_params;

GRANT select,insert,delete,update ON v_ad_params to bwriters;
GRANT select on v_ad_params TO breaders;

CREATE OR REPLACE VIEW v_ad_changes AS
SELECT
	ad_id,
	action_id,
	state_id,
	is_param,
	column_name,
	old_value,
	new_value
FROM
	public.ad_changes
UNION ALL
SELECT
	ad_id,
	action_id,
	state_id,
	is_param,
	column_name,
	old_value,
	new_value
FROM
	blocket_2010.ad_changes;

GRANT select,insert,delete,update ON v_ad_changes to bwriters;
GRANT select on v_ad_changes TO breaders;

CREATE OR REPLACE VIEW v_action_states AS
SELECT 
	ad_id,
	action_id, 
	state_id, 
	state::varchar, 
	transition::varchar, 
	timestamp, 
	host(remote_addr::inet) AS remote_addr, 
	token_id	
FROM 
	public.action_states
UNION ALL 
SELECT 
	ad_id,
	action_id, 
	state_id, 
	state::varchar, 
	transition::varchar, 
	timestamp, 
	host(remote_addr::inet) AS remote_addr, 
	token_id	
FROM 
	blocket_2010.action_states;

GRANT select,insert,delete,update ON v_action_states to bwriters;
GRANT select on v_action_states TO breaders;
