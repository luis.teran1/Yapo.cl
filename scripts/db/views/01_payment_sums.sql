CREATE OR REPLACE VIEW v_payments_sums AS
SELECT
	payment_groups.code,
	payment_groups.status,
	pay_log.pay_type,
	pay_log.paid_at,
	pay_log.token_id,
	SUM(pay_amount - discount) AS price,
	SUM(amount) AS paid,
	payment_group_id
FROM
	payment_groups
	JOIN payments USING (payment_group_id)
	JOIN pay_log USING (payment_group_id)
GROUP BY
	payment_groups.code,
	payment_groups.status,
	pay_log.pay_type,
	pay_log.paid_at,
	pay_log.token_id,
	payment_group_id;

GRANT select,insert,delete,update ON v_payments_sums to bwriters;
GRANT select on v_payments_sums TO breaders;
