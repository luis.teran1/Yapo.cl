CREATE OR REPLACE VIEW v_ads_current AS
SELECT 
	ad_id,
	list_id,
	list_time,
	status::varchar,
	type::varchar,
	name,
	phone,
	region,
	city,
	category,
	user_id,
	salted_passwd,
	phone_hidden,
	no_salesmen,
	company_ad,
	subject,
	body,
	price,
	infopage,
	infopage_title,
	store_id
FROM 
	public.ads
UNION ALL
SELECT 
	ad_id,
	list_id,
	list_time,
	status::varchar,
	type::varchar,
	name,
	phone,
	region,
	city,
	category,
	user_id,
	salted_passwd,
	phone_hidden,
	no_salesmen,
	company_ad,
	subject,
	body,
	price,
	infopage,
	infopage_title,
	store_id
FROM 
	blocket_2012.ads;

GRANT select,insert,delete,update ON v_ads_current to bwriters;
GRANT select on v_ads_current TO breaders;

CREATE OR REPLACE VIEW v_ad_actions_current AS
SELECT 
	ad_id,
	action_id, 
	action_type::varchar, 
	current_state, 
	state::varchar, 
	queue::varchar, 
	locked_by, 
	locked_until, 
	payment_group_id
FROM 
	public.ad_actions
UNION ALL 
SELECT 
	ad_id,
	action_id, 
	action_type::varchar, 
	current_state, 
	state::varchar, 
	queue::varchar, 
	locked_by, 
	locked_until, 
	payment_group_id
FROM 
	blocket_2009.ad_actions;

CREATE OR REPLACE VIEW v_action_states_current AS
SELECT 
	ad_id,
	action_id, 
	state_id, 
	state::varchar, 
	transition::varchar, 
	timestamp, 
	host(remote_addr::inet) AS remote_addr, 
	token_id	
FROM 
	public.action_states
UNION ALL 
SELECT 
	ad_id,
	action_id, 
	state_id, 
	state::varchar, 
	transition::varchar, 
	timestamp, 
	host(remote_addr::inet) AS remote_addr, 
	token_id	
FROM 
	blocket_2009.action_states;

GRANT select,insert,delete,update ON v_action_states_current to bwriters;
GRANT select on v_action_states_current TO breaders;

CREATE OR REPLACE VIEW v_aa_as_current AS
SELECT 
	ad_actions.ad_id,
	ad_actions.action_id, 
	ad_actions.action_type::varchar, 
	ad_actions.payment_group_id, 
	action_states.state_id, 
	action_states.state::varchar, 
	action_states.transition::varchar, 
	action_states.timestamp, 
	host(action_states.remote_addr::inet) AS remote_addr, 
	action_states.token_id	
FROM 
	public.ad_actions JOIN public.action_states using (ad_id, action_id)
UNION ALL 
SELECT 
	ad_actions.ad_id,
	ad_actions.action_id, 
	ad_actions.action_type::varchar, 
	ad_actions.payment_group_id, 
	action_states.state_id, 
	action_states.state::varchar, 
	action_states.transition::varchar, 
	action_states.timestamp, 
	host(action_states.remote_addr::inet) AS remote_addr, 
	action_states.token_id	
FROM 
	blocket_2009.ad_actions JOIN blocket_2009.action_states using (ad_id, action_id);

GRANT select ON v_aa_as_current TO bwriters;
GRANT select ON v_aa_as_current TO breaders;

CREATE OR REPLACE VIEW v_aa_as_pg_current AS
SELECT 
	ad_actions.ad_id,
	ad_actions.action_id, 
	ad_actions.action_type::varchar, 
	ad_actions.payment_group_id, 
	action_states.state_id, 
	action_states.state::varchar, 
	action_states.transition::varchar, 
	action_states.timestamp, 
	host(action_states.remote_addr::inet) AS remote_addr, 
	action_states.token_id,
	payment_groups.code,
	payment_groups.status::varchar
FROM 
	public.ad_actions JOIN public.action_states using (ad_id, action_id) JOIN public.payment_groups USING (payment_group_id)
UNION ALL 
SELECT 
	ad_actions.ad_id,
	ad_actions.action_id, 
	ad_actions.action_type::varchar, 
	ad_actions.payment_group_id, 
	action_states.state_id, 
	action_states.state::varchar, 
	action_states.transition::varchar, 
	action_states.timestamp, 
	host(action_states.remote_addr::inet) AS remote_addr, 
	action_states.token_id,
	payment_groups.code,
	payment_groups.status::varchar
FROM 
	blocket_2009.ad_actions JOIN blocket_2009.action_states using (ad_id, action_id) JOIN blocket_2009.payment_groups USING (payment_group_id);

GRANT select ON v_aa_as_pg_current TO bwriters;
GRANT select ON v_aa_as_pg_current TO breaders;

CREATE OR REPLACE VIEW v_pay_log_current AS
SELECT 
	pay_log_id,
	amount,
	code,
	token_id,
	paid_at,
	payment_group_id,
	status::varchar,
	pay_type::varchar
FROM 
	public.pay_log
UNION ALL
SELECT 
	pay_log_id,
	amount,
	code,
	token_id,
	paid_at,
	payment_group_id,
	status::varchar,
	pay_type::varchar
FROM 
	blocket_2009.pay_log;

GRANT SELECT ON v_pay_log_current TO bwriters;
GRANT SELECT ON v_pay_log_current TO breaders;
