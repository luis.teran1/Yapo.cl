UPDATE account_params SET value = '10' where account_id = 10 and name = 'available_slots';

INSERT INTO ad_actions VALUES (134, 2, 'status_change', 747, 'disabled', 'normal', NULL, NULL, NULL);
INSERT INTO ad_actions VALUES (133, 2, 'status_change', 749, 'disabled', 'normal', NULL, NULL, NULL);
INSERT INTO action_states VALUES (134, 2, 746, 'reg', 'initial', '2019-06-10 17:13:53.332883', '172.28.0.19', NULL);
INSERT INTO action_states VALUES (134, 2, 747, 'disabled', 'status_change', '2019-06-10 17:13:53.332883', '172.28.0.19', NULL);
INSERT INTO action_states VALUES (133, 2, 748, 'reg', 'initial', '2019-06-10 17:14:10.495211', '172.28.0.19', NULL);
INSERT INTO action_states VALUES (133, 2, 749, 'disabled', 'status_change', '2019-06-10 17:14:10.495211', '172.28.0.19', NULL);
SELECT pg_catalog.setval('action_states_state_id_seq', 749, true);
INSERT INTO ad_changes VALUES (134, 2, 746, true, 'pack_status', 'active', 'disabled');
INSERT INTO ad_changes VALUES (133, 2, 748, true, 'pack_status', 'active', 'disabled');

UPDATE ad_params SET value = 'disabled' WHERE ad_id in (133, 134) and name = 'pack_status';

UPDATE ads SET status = 'disabled' WHERE ad_id in (133, 134);

UPDATE packs set used = '0' WHERE pack_id in (1, 2);
UPDATE packs set status = 'expired' WHERE pack_id in (1);
UPDATE users SET uid = 3 WHERE email = 'cuentaproconpack@yapo.cl';
