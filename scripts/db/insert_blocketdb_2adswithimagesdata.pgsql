BEGIN;
SELECT pg_catalog.setval('action_states_state_id_seq', 214, true);
SELECT pg_catalog.setval('admins_admin_id_seq', 10, false);
SELECT pg_catalog.setval('ads_ad_id_seq', 22, true);
SELECT pg_catalog.setval('list_id_seq', 8000002, true);
SELECT pg_catalog.setval('next_image_id', 1, true);
SELECT pg_catalog.setval('pay_code_seq', 59, true);
SELECT pg_catalog.setval('pay_log_pay_log_id_seq', 6, true);
SELECT pg_catalog.setval('payment_groups_payment_group_id_seq', 62, true);
SELECT pg_catalog.setval('tokens_token_id_seq', 9, true);
SELECT pg_catalog.setval('trans_queue_id_seq', 3, true);
SELECT pg_catalog.setval('uid_seq', 50, true);
SELECT pg_catalog.setval('users_user_id_seq', 50, true);
INSERT INTO users VALUES (50, 50, 'joaquin@schibsted.cl', 0, 0);
INSERT INTO ads VALUES (20, 8000000, NOW() - interval '1 week', 'active', 'sell', 'Joaco', '786678', 15, 0, 3040, 50, NULL, false, false, false, 'Manzana macbook', 'pro', 999000, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$l?yN6F$WV>y/\\MPL51e9f000e25bdb51c695ad087f0f558a453df045', '2014-04-23 10:19:44.612228', 'es');
INSERT INTO ads VALUES (21, 8000001, NOW() - interval '1 week', 'active', 'sell', 'Joaco', '786678', 1, 0, 4020, 50, NULL, false, false, false, 'Shorts', 'realmente curtos', 9000, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$}t6''_cvWZa(hF P:38bf18a7f94b6ac0583247eeca873347c7489f62', '2014-04-23 10:19:47.102217', 'es');
INSERT INTO ads VALUES (22, 8000002, NOW() - interval '1 week', 'active', 'sell', 'Joaco', '786678', 15, 0, 6060, 50, NULL, false, false, false, 'Sin foto', 'less than a month, we should exclude this ad', 9000, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$-pPi_({`J''{=+:<o020c867e8002fc55720084ad821ad0ed3a938e86', '2014-04-23 10:19:50.571337', 'es');
INSERT INTO ad_actions VALUES (20, 1, 'new', 211, 'accepted', 'normal', 5, '2014-04-23 10:24:40.921502', NULL);
INSERT INTO ad_actions VALUES (21, 1, 'new', 212, 'accepted', 'normal', 5, '2014-04-23 10:24:40.921502', NULL);
INSERT INTO ad_actions VALUES (22, 1, 'new', 214, 'accepted', 'normal', 5, '2014-04-23 10:24:48.140595', NULL);
INSERT INTO action_params VALUES (20, 1, 'source', 'web');
INSERT INTO action_params VALUES (20, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params VALUES (21, 1, 'source', 'web');
INSERT INTO action_params VALUES (21, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params VALUES (22, 1, 'source', 'web');
INSERT INTO action_params VALUES (22, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO tokens VALUES (1, 'X5357bde96b20574b000000002748099500000000', 5, '2014-04-23 10:19:36.93504', '2014-04-23 10:19:39.633617', '10.0.1.188', 'authenticate', NULL, NULL);
INSERT INTO tokens VALUES (2, 'X5357bdec21d54c42000000004b53a45b00000000', 5, '2014-04-23 10:19:39.633617', '2014-04-23 10:19:40.915846', '10.0.1.188', 'adqueues', NULL, NULL);
INSERT INTO tokens VALUES (3, 'X5357bded595ad25e000000005be5336600000000', 5, '2014-04-23 10:19:40.915846', '2014-04-23 10:19:44.606628', '10.0.1.188', 'adqueues', NULL, NULL);
INSERT INTO tokens VALUES (4, 'X5357bdf17663faf5000000004f4a3cfa00000000', 5, '2014-04-23 10:19:44.606628', '2014-04-23 10:19:47.07792', '10.0.1.188', 'review', NULL, NULL);
INSERT INTO tokens VALUES (5, 'X5357bdf32f34501f000000003fc8c000000000', 5, '2014-04-23 10:19:47.07792', '2014-04-23 10:19:48.135771', '10.0.1.188', 'review', NULL, NULL);
INSERT INTO tokens VALUES (6, 'X5357bdf47796d3f80000000062b43bbb00000000', 5, '2014-04-23 10:19:48.135771', '2014-04-23 10:19:50.565488', '10.0.1.188', 'adqueues', NULL, NULL);
INSERT INTO tokens VALUES (7, 'X5357bdf76f76d71000000000573c3aba00000000', 5, '2014-04-23 10:19:50.565488', '2014-04-23 10:19:51.845607', '10.0.1.188', 'review', NULL, NULL);
INSERT INTO tokens VALUES (8, 'X5357bdf843ea325000000007ea2703b00000000', 5, '2014-04-23 10:19:51.845607', '2014-04-23 10:19:51.867816', '10.0.1.188', 'adqueues', NULL, NULL);
INSERT INTO tokens VALUES (9, 'X5357bdf830bb6e8e000000007233978100000000', 5, '2014-04-23 10:19:51.867816', '2014-04-23 11:19:51.867816', '10.0.1.188', 'adqueues', NULL, NULL);
INSERT INTO ad_codes VALUES (35311, 'pay');
INSERT INTO ad_codes VALUES (67334, 'pay');
INSERT INTO ad_codes VALUES (99357, 'pay');
INSERT INTO action_states VALUES (20, 1, 211, 'accepted', 'accept', '2014-04-23 10:24:26.395225', '192.168.4.75', 99999999);
INSERT INTO action_states VALUES (21, 1, 212, 'accepted', 'accept', '2014-04-23 10:24:26.395225', '192.168.4.75', 99999999);
INSERT INTO action_states VALUES (22, 1, 214, 'accepted', 'accept', '2014-04-23 10:24:26.395225', '192.168.4.75', 99999999);
INSERT INTO ad_image_changes VALUES (20, 1, 211, 0, '8369378538.jpg', false);
INSERT INTO ad_image_changes VALUES (21, 1, 212, 0, '8377901857.jpg', false);
INSERT INTO ad_image_changes VALUES (22, 1, 214, 0, NULL, false);
INSERT INTO ad_images_digests VALUES (20, '8369378538.jpg', '7c26910c1aa16abd2235bb5abb9ef152', 50, false);
INSERT INTO ad_images_digests VALUES (21, '8377901857.jpg', '7c26910c1aa16abd2235bb5abb9ef152', 50, false);
INSERT INTO ad_media VALUES (8352331900, NULL, 0, '2014-04-23 10:10:43.196532', 'image', NULL, NULL, NULL);
INSERT INTO ad_media VALUES (8360855219, NULL, 0, '2014-04-23 10:10:43.200828', 'image', NULL, NULL, NULL);
INSERT INTO ad_media VALUES (8369378538, 20, 0, '2014-04-23 10:12:20.559197', 'image', NULL, NULL, NULL);
INSERT INTO ad_media VALUES (8377901857, 21, 0, '2014-04-23 10:13:49.794733', 'image', NULL, NULL, NULL);
INSERT INTO ad_params VALUES (20, 'prev_currency', 'peso');
INSERT INTO ad_params VALUES (20, 'country', 'UNK');
INSERT INTO ad_params VALUES (21, 'gender', '2');
INSERT INTO ad_params VALUES (21, 'condition', '1');
INSERT INTO ad_params VALUES (21, 'prev_currency', 'peso');
INSERT INTO ad_params VALUES (21, 'country', 'UNK');
INSERT INTO ad_params VALUES (22, 'prev_currency', 'peso');
INSERT INTO ad_params VALUES (22, 'country', 'UNK');
INSERT INTO payment_groups VALUES (60, '59876', 'cleared', '2014-04-23 10:24:26.353643', NULL);
INSERT INTO payment_groups VALUES (61, '59876', 'cleared', '2014-04-23 10:24:26.353643', NULL);
INSERT INTO payment_groups VALUES (62, '59876', 'cleared', '2014-04-23 10:24:26.353643', NULL);
INSERT INTO pay_log VALUES (1, 'save', 0, '112200000', NULL, '2014-04-23 10:12:38', 60, 'SAVE');
INSERT INTO pay_log VALUES (2, 'verify', 0, '112200000', NULL, '2014-04-23 10:12:38', 60, 'OK');
INSERT INTO pay_log VALUES (3, 'save', 0, '112200001', NULL, '2014-04-23 10:13:56', 61, 'SAVE');
INSERT INTO pay_log VALUES (4, 'verify', 0, '112200001', NULL, '2014-04-23 10:13:56', 61, 'OK');
INSERT INTO pay_log VALUES (5, 'save', 0, '112200002', NULL, '2014-04-23 10:14:34', 62, 'SAVE');
INSERT INTO pay_log VALUES (6, 'verify', 0, '112200002', NULL, '2014-04-23 10:14:34', 62, 'OK');
INSERT INTO pay_log_references VALUES (1, 0, 'adphone', '786678');
INSERT INTO pay_log_references VALUES (1, 1, 'remote_addr', '10.0.1.188');
INSERT INTO pay_log_references VALUES (2, 0, 'remote_addr', '10.0.1.188');
INSERT INTO pay_log_references VALUES (3, 0, 'adphone', '786678');
INSERT INTO pay_log_references VALUES (3, 1, 'remote_addr', '10.0.1.188');
INSERT INTO pay_log_references VALUES (4, 0, 'remote_addr', '10.0.1.188');
INSERT INTO pay_log_references VALUES (5, 0, 'adphone', '786678');
INSERT INTO pay_log_references VALUES (5, 1, 'remote_addr', '10.0.1.188');
INSERT INTO pay_log_references VALUES (6, 0, 'remote_addr', '10.0.1.188');
INSERT INTO payments VALUES (60, 'ad_action', 0, 0);
INSERT INTO payments VALUES (61, 'ad_action', 0, 0);
INSERT INTO payments VALUES (62, 'ad_action', 0, 0);
INSERT INTO review_log VALUES (20, 1, 5, '2014-04-23 10:19:44.612228', 'normal', 'new', 'accepted', NULL, 3040);
INSERT INTO review_log VALUES (21, 1, 5, '2014-04-23 10:19:47.102217', 'normal', 'new', 'accepted', NULL, 5100);
INSERT INTO review_log VALUES (22, 1, 5, '2014-04-23 10:19:50.571337', 'normal', 'new', 'accepted', NULL, 6060);
INSERT INTO state_params VALUES (20, 1, 211, 'filter_name', 'normal');
INSERT INTO state_params VALUES (21, 1, 212, 'filter_name', 'normal');
INSERT INTO state_params VALUES (22, 1, 214, 'filter_name', 'normal');
INSERT INTO trans_queue VALUES (1, '2014-04-23 10:19:44.654424', '16893@mazaru.schibsted.cl', NULL, NULL, NULL, NULL, NULL, NULL, 'cmd:admail
commit:1
ad_id:20
action_id:1
mail_type:accept
', NULL, 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue VALUES (2, '2014-04-23 10:19:47.158327', '16893@mazaru.schibsted.cl', NULL, NULL, NULL, NULL, NULL, NULL, 'cmd:admail
commit:1
ad_id:21
action_id:1
mail_type:accept
', NULL, 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue VALUES (3, '2014-04-23 10:19:50.580129', '16893@mazaru.schibsted.cl', NULL, NULL, NULL, NULL, NULL, NULL, 'cmd:admail
commit:1
ad_id:22
action_id:1
mail_type:accept
', NULL, 'accepted_mail', NULL, NULL);
COMMIT;
INSERT INTO user_params VALUES (50, 'first_approved_ad', '2014-04-23 10:19:44.612228-03');
INSERT INTO user_params VALUES (50, 'first_inserted_ad', '2014-04-23 10:19:44.612228-03');

--Migrate inmo ads
select * from bpv.migrate_ads_inmo();
