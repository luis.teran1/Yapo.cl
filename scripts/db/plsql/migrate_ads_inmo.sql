-- This function is generated to migrate automatically data for testing
CREATE OR REPLACE FUNCTION migrate_ads_inmo(
	OUT o_migrated_ads integer
) AS $$
DECLARE
	l_rec record;
	l_new_cat integer;
	l_new_et varchar;
	l_new_type enum_ads_type;
BEGIN

	o_migrated_ads := 1;
	FOR
		l_rec
	IN
		SELECT
			ad_id,
			body,
			category,
			type,
			status,
			COALESCE(list_id, 0) AS list_id,
			list_time
		FROM
			ads 
		WHERE
			category in (1020,1040,1060,1080,1100,1120)
	LOOP
	
		IF l_rec.type in ('sell', 'buy') THEN
			l_new_cat := 1220;
			l_new_type := 'sell';
		ELSE
			l_new_cat := 1240;
			l_new_type := 'let';
		END IF;
		
		CASE l_rec.category
			WHEN 1020 THEN
				l_new_et := '1';
			WHEN 1040 THEN
				l_new_et := '2';
			WHEN 1060 THEN
				l_new_et := '3';
			WHEN 1080 THEN
				l_new_et := '4';
			WHEN 1100 THEN
				l_new_et := '5';
			WHEN 1120 THEN
				l_new_et := '6';
		END CASE;

		UPDATE ads SET category = l_new_cat, type = l_new_type WHERE ad_id = l_rec.ad_id;
		INSERT INTO ad_params VALUES(l_rec.ad_id, 'estate_type', l_new_et); 
		o_migrated_ads := o_migrated_ads +1 ;
	END LOOP;
END;
$$ LANGUAGE plpgsql;
