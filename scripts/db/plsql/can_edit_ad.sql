CREATE OR REPLACE FUNCTION can_edit_ad(
	i_ad_id ads.ad_id%TYPE,
	i_category_edition_limits integer[][]
) RETURNS BOOLEAN AS $$
DECLARE
	l_category ads.category%TYPE;
	l_age_days interval;
	l_allowed_days interval;
	l_cat_days integer[];
BEGIN

	SELECT
		category,
		DATE_TRUNC('day', AGE(COALESCE(orig_list_time, list_time)))
	INTO
		l_category,
		l_age_days
	FROM
		ads
	WHERE
		ad_id = i_ad_id;

	-- Loop over category edition date limits
	IF array_ndims(i_category_edition_limits) = 2 THEN
		FOREACH l_cat_days SLICE 1 IN ARRAY i_category_edition_limits
		LOOP
			IF l_category = l_cat_days[1] THEN
				l_allowed_days := (''||l_cat_days[2]||' day')::INTERVAL;
				RETURN l_age_days < l_allowed_days;
			END IF;
		END LOOP;
	END IF;

	RETURN true;
END;
$$ LANGUAGE plpgsql;
