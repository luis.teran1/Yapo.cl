CREATE OR REPLACE FUNCTION delete_watch_user(i_watch_user_id watch_users.watch_user_id%TYPE) RETURNS VOID AS $$
BEGIN
	DELETE FROM watch_users WHERE watch_user_id = i_watch_user_id;
	IF NOT FOUND THEN
		RAISE EXCEPTION 'ERROR_WATCH_USER_MISSING';
	END IF;
END
$$ LANGUAGE PLPGSQL;
