CREATE OR REPLACE FUNCTION copy_payment_group(
		i_payment_group_id payment_groups.payment_group_id%TYPE,
		i_schema varchar,
		OUT o_status integer
		) AS $$
DECLARE
	l_payment_group_id payment_groups.payment_group_id%TYPE;
BEGIN
	EXECUTE
		'SELECT
			payment_group_id
		FROM
			' || quote_ident(i_schema) || '.payment_groups
		WHERE
			payment_group_id = ' || quote_literal(i_payment_group_id)
	INTO
		l_payment_group_id;
	
	IF l_payment_group_id IS NOT NULL THEN
		o_status := 1;
		RETURN;
	END IF;
	o_status := 0;

-- payment_groups

	EXECUTE
		'INSERT INTO ' ||
			quote_ident(i_schema) || '.payment_groups
			(payment_group_id, code, status, added_at)
		SELECT
			payment_group_id, code, status::' || get_column_type (i_schema, 'payment_groups', 'status') || ', added_at
		FROM
			payment_groups
		WHERE
			payment_group_id = ' || quote_literal(i_payment_group_id);

-- payments

	EXECUTE
		'INSERT INTO ' ||
			quote_ident(i_schema) || '.payments
			(payment_group_id, payment_type, pay_amount, discount)
		SELECT
			payment_group_id, payment_type::' || get_column_type (i_schema, 'payments', 'payment_type') || ', pay_amount, discount
		FROM
			payments
		WHERE
			payment_group_id = ' || quote_literal(i_payment_group_id);

-- pay_log tokens

	PERFORM
		copy_token(token_id, i_schema)
	FROM
		pay_log
	WHERE
		payment_group_id = i_payment_group_id
		AND token_id IS NOT NULL;

-- pay_log

	EXECUTE
		'INSERT INTO ' ||
			quote_ident(i_schema) || '.pay_log
			(pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status)
		SELECT
			pay_log_id, pay_type::' || get_column_type (i_schema, 'pay_log', 'pay_type') ||
			', amount, code, token_id, paid_at, payment_group_id, status::' ||
			get_column_type (i_schema, 'pay_log', 'status') || '
		FROM
			pay_log
		WHERE
			payment_group_id = ' || quote_literal(i_payment_group_id);

	EXECUTE
		'INSERT INTO ' ||
			quote_ident(i_schema) || '.pay_log_references
			(pay_log_id, ref_num, ref_type, reference)
		SELECT
			pay_log_id, ref_num, ref_type::' || get_column_type (i_schema, 'pay_log_references', 'ref_type') ||
			', reference
		FROM
			pay_log_references
		WHERE
			pay_log_id IN (SELECT
					pay_log_id
				FROM
					pay_log
				WHERE
					payment_group_id = ' || quote_literal(i_payment_group_id) || ')';
END;
$$ LANGUAGE plpgsql;
