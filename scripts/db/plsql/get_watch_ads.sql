CREATE OR REPLACE FUNCTION get_watch_ads(i_watch_unique_id watch_users.watch_unique_id%TYPE,
					 OUT o_list_id ads.list_id%TYPE) RETURNS SETOF INTEGER AS $$
DECLARE
	l_list_id ads.list_id%TYPE;
	l_watch_user_id watch_users.watch_user_id%TYPE;
BEGIN
	l_watch_user_id := get_watch_user_id (i_watch_unique_id);

	IF l_watch_user_id IS NULL THEN
		RAISE EXCEPTION 'FACTORY_TRANSLATE:error:ERROR_NO_SUCH_UNIQUE_ID';
	END IF;

	FOR
		l_list_id
	IN
		SELECT
			list_id 
		FROM 
			watch_ads
		WHERE 
			watch_user_id = l_watch_user_id
	LOOP
		o_list_id := l_list_id;	

		RETURN NEXT;
	END LOOP;
END
$$ LANGUAGE plpgsql;
