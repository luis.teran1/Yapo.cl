CREATE OR REPLACE FUNCTION insert_account_action(
    i_account_id account_actions.account_id%TYPE,
    i_account_action_type account_actions.account_action_type%TYPE,
    i_token_id account_actions.token_id%TYPE,
    OUT o_account_action_id account_actions.account_action_id%TYPE
) AS $$
DECLARE
    l_max_retry integer;
BEGIN
    -- add action_state
    SELECT
        COALESCE( max(account_action_id) + 1, 1)
    INTO
        o_account_action_id
    FROM
        account_actions
    WHERE
        account_id = i_account_id;

    -- do a couple of retries in case there is someone else inserting at the same time
    l_max_retry := 5;

    -- insert action
    WHILE l_max_retry > 0 LOOP
        BEGIN
            INSERT INTO
                account_actions
                (account_id,
                 account_action_id,
                 account_action_type,
                 token_id
                 )
            VALUES
                (i_account_id,
                 o_account_action_id,
                 i_account_action_type,
                 i_token_id);

            RETURN;

        EXCEPTION WHEN UNIQUE_VIOLATION THEN
            o_account_action_id := o_account_action_id + 1;
        END;

        l_max_retry := l_max_retry - 1;
    END LOOP;

    RAISE EXCEPTION 'ERROR_INSERTING_ACCOUNT_ACTION';
END;
$$ LANGUAGE plpgsql;
