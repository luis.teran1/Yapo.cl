CREATE OR REPLACE FUNCTION get_action_id_by_list_id(i_list_id ads.list_id%TYPE,
						    i_added_check varchar(50),
						    i_grace_time interval,
						    i_reg_state_id action_states.state_id%TYPE,
						    i_action varchar(20),
						    OUT o_ad_id ad_actions.ad_id%TYPE,
						    OUT o_action_id ad_actions.action_id%TYPE) AS $$
DECLARE
	l_insert_action_id ad_actions.action_id%TYPE;
	l_timestamp timestamp;
BEGIN
	SELECT
		ads.ad_id,
		MAX(ad_actions.action_id)
	INTO
		o_ad_id,
		o_action_id
	FROM 
		ads
		JOIN ad_actions USING (ad_id)
		LEFT JOIN ad_params AS ext_id ON
			ads.ad_id = ext_id.ad_id AND
			ext_id.name = 'external_ad_id'
	WHERE 
		ads.list_id = i_list_id AND
		CASE 
			WHEN ext_id.value IS NOT NULL AND store_id IS NULL then true 
			WHEN ext_id.value IS NOT NULL AND store_id IS NOT NULL then false
			WHEN ext_id.value IS NULL then true
		END AND
		ad_actions.state = 'accepted'
	GROUP BY
		ads.ad_id;
	
	IF NOT FOUND THEN
		RAISE EXCEPTION 'ERROR_NO_SUCH_AD';
	END IF;

	IF i_action = 'deleted' OR i_action = 'store_deleted' THEN
		SELECT
			reg.timestamp
		INTO
			l_timestamp
		FROM
			action_states AS reg
			LEFT JOIN action_states AS pay ON
				pay.ad_id = reg.ad_id AND
				pay.action_id = reg.action_id AND
				pay.transition IN ('pay', 'verify', 'adminclear', 'import')
		WHERE
			reg.state = 'reg' AND
			reg.ad_id = o_ad_id
		ORDER BY
			pay.timestamp
		LIMIT 1;
		IF i_action != 'store_deleted' AND (i_added_check IS NULL OR 
						    to_char(l_timestamp, 'HH24MISS') != i_added_check) THEN
			RAISE EXCEPTION 'ERROR_NO_SUCH_AD';
		END IF;
	ELSIF i_action = 'refused' THEN
		SELECT
			MAX(timestamp),
                        MAX(action_id)
		INTO
			l_timestamp,
                        o_action_id
		FROM
			action_states
		WHERE
			ad_id = o_ad_id AND
			state = 'refused';
		IF l_timestamp + i_grace_time < CURRENT_TIMESTAMP THEN
			RAISE EXCEPTION 'ERROR_AD_TOO_OLD';
		END IF;
	ELSIF i_action LIKE 'spidered%' THEN
		SELECT
			reg.timestamp
		INTO
			l_timestamp
		FROM
			action_states AS reg
			LEFT JOIN action_states AS pay ON
				pay.ad_id = reg.ad_id AND
				pay.action_id = reg.action_id AND
				pay.transition IN ('pay', 'verify', 'adminclear', 'import') 
		WHERE
			reg.state = 'reg' AND
			reg.ad_id = o_ad_id
		ORDER BY
			pay.timestamp
		LIMIT 1;
		IF NOT FOUND OR to_char(l_timestamp, 'HH24MISS') != i_added_check THEN
			RAISE EXCEPTION 'ERROR_NO_SUCH_AD';
		END IF;
	ELSIF i_reg_state_id IS NOT NULL OR i_added_check IS NOT NULL THEN 
		SELECT
			reg.timestamp
		INTO
			l_timestamp
		FROM
			action_states AS reg
			LEFT JOIN action_states AS pay ON
				pay.ad_id = reg.ad_id AND
				pay.action_id = reg.action_id AND
				pay.transition IN ('pay', 'verify', 'adminclear', 'import') 
		WHERE
			reg.state = 'reg' AND
			reg.ad_id = o_ad_id AND
			reg.state_id = i_reg_state_id
		ORDER BY
			pay.timestamp
		LIMIT 1;
		IF NOT FOUND OR to_char(l_timestamp, 'HH24MISS') != i_added_check THEN
			RAISE EXCEPTION 'ERROR_NO_SUCH_AD';
		END IF;
	END IF;
END
$$ LANGUAGE plpgsql;
