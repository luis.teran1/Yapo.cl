CREATE OR REPLACE FUNCTION check_upselling_refuse(
	i_ad_id ads.ad_id%TYPE,
	i_action_id ad_actions.action_id%TYPE,
	i_remote_addr action_states.remote_addr%TYPE,
	i_token_id action_states.token_id%TYPE,
	OUT o_upselling_action_id ad_actions.action_id%TYPE,
	OUT o_remove_upselling_products boolean
) AS $$
DECLARE
	l_action_type varchar;
	l_queue_id integer;
	l_action_id integer;
	l_state_id integer;
BEGIN
	o_upselling_action_id := i_action_id +1;

	SELECT action_type,action_id INTO l_action_type,o_upselling_action_id FROM ad_actions
	WHERE ad_id = i_ad_id
	AND action_id > i_action_id
	AND action_type in ('upselling_bump', 'upselling_daily_bump', 'upselling_weekly_bump', 'upselling_gallery', 'upselling_label', 'upselling_standard_cars','upselling_advanced_cars','upselling_premium_cars','upselling_standard_inmo','upselling_advanced_inmo','upselling_premium_inmo','upselling_standard_others','upselling_advanced_others','upselling_premium_others','inserting_fee')
	AND state = 'accepted';

	IF NOT FOUND THEN
		SELECT action_type,action_id INTO l_action_type,o_upselling_action_id FROM ad_actions
		WHERE ad_id = i_ad_id
		AND action_id < i_action_id
		AND action_type in('if_pack_car', 'if_pack_inmo')
		AND state = 'accepted';
	END IF;

	IF l_action_type IS NOT NULL THEN
		PERFORM insert_state(i_ad_id, o_upselling_action_id, 'refused', 'refuse',  i_remote_addr, i_token_id);

		IF l_action_type in ('upselling_daily_bump', 'upselling_weekly_bump', 'upselling_bump', 'upselling_standard_cars','upselling_advanced_cars','upselling_premium_cars','upselling_standard_inmo','upselling_advanced_inmo','upselling_premium_inmo','upselling_standard_others','upselling_advanced_others','upselling_premium_others','inserting_fee') THEN
			-- Schedule removal of trans queue upselling bump products
			o_remove_upselling_products := TRUE;

			-- Remove upselling
			l_action_id := insert_ad_action(i_ad_id, 'remove_upselling', NULL);
			PERFORM insert_state(i_ad_id, l_action_id, 'reg', 'initial', i_remote_addr, NULL);
			l_state_id := insert_state(i_ad_id, l_action_id, 'accepted', 'accept', i_remote_addr, NULL);

			IF l_action_type in ('upselling_weekly_bump', 'upselling_daily_bump', 'inserting_fee') THEN
				-- add removal of gallery ad_params to ad_changes
				PERFORM insert_ad_change(i_ad_id, l_action_id, l_state_id, true, l_action_type::varchar, NULL);
				-- apply ad_changes
				PERFORM apply_ad_changes(i_ad_id, l_action_id, NULL);
			END IF;

		ELSIF l_action_type = 'upselling_gallery' THEN
			-- This is only for actions bigger than one because the refuse process
			-- delete authomatically the galleries for new ads.
			IF i_action_id > 1 THEN
				-- Remove upselling gallery
				l_action_id := insert_ad_action(i_ad_id, 'remove_gallery', NULL);
				PERFORM insert_state(i_ad_id, l_action_id, 'reg', 'initial', i_remote_addr, NULL);
				l_state_id := insert_state(i_ad_id, l_action_id, 'accepted', 'accept', i_remote_addr, NULL);

				-- add removal of gallery ad_params to ad_changes
				PERFORM insert_ad_change(i_ad_id, l_action_id, l_state_id, true, 'gallery', NULL);

				-- apply ad_changes
				PERFORM apply_ad_changes(i_ad_id, l_action_id, NULL);
			END IF;

		ELSIF l_action_type = 'upselling_label' THEN
			-- This is only for actions bigger than one because the refuse process
			-- delete authomatically the labels for new ads.
			IF i_action_id > 1 THEN
				-- Remove upselling label
				l_action_id := insert_ad_action(i_ad_id, 'remove_label', NULL);
				PERFORM insert_state(i_ad_id, l_action_id, 'reg', 'initial', i_remote_addr, NULL);
				l_state_id := insert_state(i_ad_id, l_action_id, 'accepted', 'accept', i_remote_addr, NULL);

				-- add removal of label ad_params to ad_changes
				PERFORM insert_ad_change(i_ad_id, l_action_id, l_state_id, true, 'label', NULL);

				-- apply ad_changes
				PERFORM apply_ad_changes(i_ad_id, l_action_id, NULL);
			END IF;
		END IF;
	END IF;
END;
$$ LANGUAGE plpgsql;
