CREATE OR REPLACE FUNCTION delete_watch_queries(i_watch_unique_id watch_users.watch_unique_id%TYPE,
							i_last_view watch_queries.last_view%TYPE,
					      	OUT o_deleted_queries INTEGER,
					      	OUT o_watch_user_id watch_users.watch_user_id%TYPE) AS $$
BEGIN
	-- Get watch user id
	SELECT
		wu.watch_user_id
	INTO
		o_watch_user_id
	FROM
		watch_users as wu JOIN
		watch_queries as wq USING(watch_user_id)
	WHERE
		wu.watch_unique_id = i_watch_unique_id AND
		wq.last_view < i_last_view;
		
	IF FOUND THEN
		-- SELECT queries that we are going to delete
		SELECT 
			count(watch_query_id)	
		INTO
			o_deleted_queries
		FROM 
			watch_queries
		WHERE
			watch_user_id = o_watch_user_id AND
			last_view < i_last_view;
	
		DELETE FROM
			watch_queries
		WHERE
			watch_user_id = o_watch_user_id AND
			last_view < i_last_view;
		
	ELSE 
		o_deleted_queries := 0;
		o_watch_user_id := 0;
	END IF;
END
$$ LANGUAGE PLPGSQL;

