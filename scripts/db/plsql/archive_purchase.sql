CREATE OR REPLACE FUNCTION archive_purchase(i_purchase_ids integer[], i_schema varchar, i_clean boolean
		) RETURNS void AS $$
DECLARE
        l_schema varchar;
        items record;
BEGIN

        RAISE NOTICE 'ARCHIVE: Starting a batch of purchase % ', i_purchase_ids;

        -- set schema
        IF i_schema IS NULL THEN
                l_schema := 'blocket_' || extract(year from CURRENT_TIMESTAMP);
        ELSE
                l_schema := i_schema;
        END IF;

        RAISE NOTICE 'ARCHIVE: Creating temporary table archive_purchase';

        -- Select all purchase_detail_id by purchase_id
        CREATE TEMPORARY TABLE
                archive_purchase_ids
        ON COMMIT DROP
        AS SELECT
                i_purchase_ids[generate_series] AS purchase_id
        FROM
                generate_series(array_lower(i_purchase_ids, 1), array_upper(i_purchase_ids, 1));

        CREATE UNIQUE INDEX unique_archive_purchase_ids ON archive_purchase_ids (purchase_id);
        -- ANALYZE helps determine the most efficient execution plans for queries
	ANALYZE archive_purchase_ids;

        RAISE NOTICE 'ARCHIVE: Creating temporary table archive_payment_groups_copy';

        -- Select payment_group_id parent by purchase
        CREATE TEMPORARY TABLE
                archive_payment_groups_copy
        ON COMMIT DROP
        AS SELECT DISTINCT
                purchase.payment_group_id as payment_group_id
        FROM
                purchase
                JOIN archive_purchase_ids USING (purchase_id);

        CREATE UNIQUE INDEX unique_archive_payment_groups_copy ON archive_payment_groups_copy (payment_group_id);
	-- ANALYZE helps determine the most efficient execution plans for queries
        ANALYZE archive_payment_groups_copy;

        RAISE NOTICE 'ARCHIVE: Creating temporary table archive_payment_groups_clean';

        -- Select payment_group_id parent by purchase
        CREATE TEMPORARY TABLE
                archive_payment_groups_clean
        ON COMMIT DROP
        AS SELECT DISTINCT
                purchase.payment_group_id as payment_group_id
        FROM
                purchase
                JOIN archive_purchase_ids USING (purchase_id);


        CREATE UNIQUE INDEX unique_archive_payment_groups_clean ON archive_payment_groups_clean (payment_group_id);
	-- ANALYZE helps determine the most efficient execution plans for queries
        ANALYZE archive_payment_groups_clean;

        RAISE NOTICE 'ARCHIVE: Starting copy phase copy_ad_from_archive';

        PERFORM copy_purchase_from_archive(l_schema);

        IF i_clean IS NULL OR i_clean THEN
                RAISE NOTICE 'ARCHIVE: Starting delete phase clean_ad_from_archive';
                PERFORM clean_purchase_from_archive();
        END IF;

        RAISE NOTICE 'ARCHIVE: Done';
END;
$$ LANGUAGE plpgsql;
