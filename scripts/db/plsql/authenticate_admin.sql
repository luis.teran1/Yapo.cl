CREATE OR REPLACE FUNCTION authenticate_admin (i_username admins.username%TYPE,
					      i_passwd admins.passwd%TYPE,
					      i_remote_addr tokens.remote_addr%TYPE,
					      i_valid_interval INTERVAL,
					      i_info tokens.info%TYPE,
					      i_auth_type TEXT,
					      i_auth_resource TEXT,
					      OUT o_auth_id INTEGER,
					      OUT o_admin_privs TEXT,
					      OUT o_token tokens.token%TYPE) AS $$
DECLARE
	l_email tokens.email%TYPE;
BEGIN
	IF i_auth_type = 'admin' THEN
		SELECT
			admins.admin_id,
			group_concat(priv_name)
		INTO
			o_auth_id,
			o_admin_privs
		FROM
			admins
			LEFT JOIN admin_privs ON 
				(admins.admin_id = admin_privs.admin_id)
		WHERE
			status = 'active' AND
			username = i_username AND
			passwd = i_passwd
		GROUP BY
			admins.admin_id;
		
		IF NOT FOUND THEN
			RAISE EXCEPTION 'ERROR_AUTH_LOGIN';
		END IF;
	
	ELSIF i_auth_type = 'store' THEN
		IF NOT EXISTS (SELECT
			*
		FROM
			stores
			JOIN store_params AS store_passwd ON
				store_passwd.store_id = stores.store_id AND
				store_passwd.name = 'passwd' AND
				store_passwd.value = i_passwd
			JOIN store_users ON
				store_users.store_id = stores.store_id AND
				store_users.email = i_username
		WHERE
			stores.store_id = i_auth_resource::integer)
		THEN
			RAISE EXCEPTION 'ERROR_AUTH_LOGIN';
		END IF;

		o_auth_id := i_auth_resource;
		l_email := i_username;

		IF NOT EXISTS (SELECT
			*
		FROM
			store_params
		WHERE
			store_id = i_auth_resource::integer AND
		       	name = 'import_id') THEN

			o_admin_privs := 'storeedit';
		END IF;

	ELSE
		RAISE EXCEPTION 'ERROR_AUTH_TYPE %', i_auth_type ;

	END IF;

	o_token := generate_token(o_auth_id, i_remote_addr, i_valid_interval, i_info, i_auth_type, l_email, NULL);


END;
$$ LANGUAGE plpgsql;
