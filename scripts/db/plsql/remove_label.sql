CREATE OR REPLACE FUNCTION remove_label(
	i_ad_id ads.ad_id%TYPE,
	i_remote_addr tokens.remote_addr%TYPE,
	i_remote_browser varchar,
	i_token_id action_states.token_id%TYPE,
	OUT o_email users.email%TYPE
) AS $$
DECLARE
	l_action_id ad_actions.action_id%TYPE;
	l_state_id action_states.state_id%TYPE;
BEGIN
	SELECT
		email
	INTO
		o_email
	FROM
		ads JOIN users USING (user_id)
	WHERE
		ad_id = i_ad_id;

	IF NOT FOUND
	THEN
		RAISE EXCEPTION 'ERROR_AD_NOT_FOUND';
	END IF;

	IF EXISTS(SELECT * FROM ad_params WHERE name='label' AND ad_id = i_ad_id)
	THEN
		-- add ad_action and action_states
		l_action_id := insert_ad_action(i_ad_id, 'remove_label', NULL);
		IF i_token_id IS NULL THEN
			PERFORM insert_state(i_ad_id, l_action_id, 'reg', 'initial', i_remote_addr, NULL);
			l_state_id := insert_state(i_ad_id, l_action_id, 'accepted', 'accept', i_remote_addr, NULL);
		ELSE
			PERFORM insert_state(i_ad_id, l_action_id, 'reg', 'initial', i_remote_addr, i_token_id);
			l_state_id := insert_state(i_ad_id, l_action_id, 'accepted', 'adminclear', i_remote_addr, i_token_id);
		END IF;

		-- add removal of label ad_params to ad_changes
		PERFORM insert_ad_change(i_ad_id, l_action_id, l_state_id, true, 'label', NULL);

		-- apply ad_changes
		PERFORM apply_ad_changes(i_ad_id, l_action_id, NULL);
	ELSE
		RAISE EXCEPTION 'ERROR_AD_MISSING_LABEL';
	END IF;
END
$$ LANGUAGE plpgsql;
