CREATE OR REPLACE FUNCTION update_conf(i_key conf.key%TYPE, 
				       i_value conf.value%TYPE,
				       i_token_id conf.token_id%TYPE) RETURNS VOID AS $$
DECLARE
BEGIN
	IF EXISTS (SELECT * FROM conf WHERE key = i_key) THEN
		UPDATE 
			conf
		SET
			value = i_value,
			token_id = i_token_id,
			modified_at = CURRENT_TIMESTAMP
		WHERE
			key = i_key;

		EXECUTE log_event('setconf', 'UPDATE ' || i_key || '=' || i_value, i_token_id);
	ELSE
		INSERT INTO 
			conf
			(key, 
			 value,
			 token_id,
			 modified_at)
		VALUES
			(i_key,
			 i_value,
			 i_token_id,
			 CURRENT_TIMESTAMP);
		EXECUTE log_event('setconf', 'INSERT ' || i_key || '=' || i_value, i_token_id);
	END IF;
END;
$$ LANGUAGE plpgsql;
