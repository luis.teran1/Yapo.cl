CREATE OR REPLACE FUNCTION lock_queue_commands(i_queue trans_queue.queue%TYPE,
                                               i_lock_name trans_queue.locked_by%TYPE,
                                               i_maxtime integer,
                                               i_limit integer)
RETURNS TABLE (o_trans_queue_id trans_queue.trans_queue_id%TYPE, o_command trans_queue.command%TYPE) AS $$
	WITH locked_tasks AS (
		SELECT
			trans_queue_id,
			command
		FROM
			trans_queue
		WHERE
			queue = i_queue
			AND executed_at IS NULL
			AND COALESCE(execute_at <= CURRENT_TIMESTAMP, true)
			AND (locked_by IS NULL OR locked_at < CURRENT_TIMESTAMP - INTERVAL '2 minute')
			AND (i_maxtime IS NULL OR EXTRACT(EPOCH FROM added_at::timestamptz) < i_maxtime)
		ORDER BY
			trans_queue_id ASC
		LIMIT
			i_limit
		FOR UPDATE
	) UPDATE
		trans_queue
	SET
		locked_at = CURRENT_TIMESTAMP,
		locked_by = i_lock_name
	FROM
		locked_tasks
	WHERE
		trans_queue.trans_queue_id = locked_tasks.trans_queue_id
	AND
		executed_at IS NULL
	RETURNING
		locked_tasks.trans_queue_id, locked_tasks.command;
$$ LANGUAGE sql;
