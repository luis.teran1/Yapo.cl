CREATE OR REPLACE FUNCTION is_newbie_user(i_user_id users.user_id%TYPE)
		RETURNS BOOL AS $$
DECLARE
	l_user_id integer;
BEGIN
	SELECT
		user_id 
	INTO
		l_user_id
	FROM
		user_params
	WHERE
		user_id = i_user_id
		AND name = 'pre_first_inserted_ad';

	RETURN FOUND;
END;
$$ LANGUAGE plpgsql;
