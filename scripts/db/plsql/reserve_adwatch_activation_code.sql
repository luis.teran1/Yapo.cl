CREATE OR REPLACE FUNCTION reserve_adwatch_activation_code(i_watch_unique_id watch_users.watch_unique_id%TYPE,
							   i_watch_query_id watch_queries.watch_query_id%TYPE,
							   i_expiration_time integer,
							   OUT o_activation_code ad_codes.code%TYPE) AS $$
DECLARE
	l_watch_user_id watch_users.watch_user_id%TYPE;
BEGIN
	SELECT
		watch_user_id
	FROM
		watch_users
	INTO
		l_watch_user_id
	WHERE
		watch_unique_id = i_watch_unique_id;
	
	IF NOT FOUND THEN
		RAISE EXCEPTION 'FACTORY_TRANSLATE:error:ERROR_NO_SUCH_UNIQUE_ID';
	END IF;

	SELECT 
		sms_activation_code
	FROM
		watch_queries
	INTO
		o_activation_code
	WHERE
		watch_user_id = l_watch_user_id 
		AND watch_query_id = i_watch_query_id
		AND sms_activation_code IS NOT NULL
		AND sms_activation_expiry > CURRENT_TIMESTAMP;
	IF NOT FOUND THEN		
		LOCK watch_queries IN ROW EXCLUSIVE MODE;
		SELECT
			get_ad_code('adwatch')
		INTO
			o_activation_code;
		UPDATE	
			watch_queries
		SET
			sms_activation_code = o_activation_code,
			sms_activation_expiry = CURRENT_TIMESTAMP + INTERVAL '1 minute' * i_expiration_time
		WHERE
			watch_user_id = l_watch_user_id 
			AND watch_query_id = i_watch_query_id;
		PERFORM generate_ad_codes('adwatch', 0);
	END IF;
END;
$$ LANGUAGE plpgsql;
