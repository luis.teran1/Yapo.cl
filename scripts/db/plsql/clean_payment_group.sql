CREATE OR REPLACE FUNCTION clean_payment_group (
		i_payment_group_id payment_groups.payment_group_id%TYPE,
		OUT o_status integer
		) AS $$
DECLARE
	l_rec record;
BEGIN
	PERFORM
		1
	FROM
		ad_actions
	WHERE
		payment_group_id = i_payment_group_id;
	
	IF FOUND THEN
		o_status := 1;
		RETURN;
	END IF;

	PERFORM
		1
	FROM
		voucher_actions
	WHERE
		payment_group_id = i_payment_group_id;
	
	IF FOUND THEN
		o_status := 1;
		RETURN;
	END IF;

	FOR l_rec IN SELECT * FROM pay_log WHERE payment_group_id = i_payment_group_id LOOP
		DELETE FROM pay_log_references WHERE pay_log_id = l_rec.pay_log_id;
		DELETE FROM pay_log WHERE pay_log_id = l_rec.pay_log_id;
		IF l_rec.token_id IS NOT NULL THEN
			PERFORM clean_token(l_rec.token_id);
		END IF;
	END LOOP;
	DELETE FROM payments WHERE payment_group_id = i_payment_group_id;
	DELETE FROM payment_groups WHERE payment_group_id = i_payment_group_id;
END
$$ LANGUAGE plpgsql;
