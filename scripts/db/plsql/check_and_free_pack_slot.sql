CREATE OR REPLACE FUNCTION check_and_free_pack_slot(
	i_ad_id		ads.ad_id%TYPE,
	i_action_id ad_actions.action_id%TYPE,
	i_state_id action_states.state_id%TYPE,
	i_pack_conf varchar[][]
) RETURNS void AS $$
DECLARE
	l_category ads.category%TYPE;
	l_user_id ads.user_id%TYPE;
	l_pack_id packs.pack_id%TYPE;
	l_pack_type varchar;
	l_action_for_pack integer;
	l_state_for_pack integer;
	l_action_type ad_actions.action_type%TYPE;
BEGIN

	SELECT action_type INTO l_action_type FROM ad_actions WHERE ad_id = i_ad_id and action_id = i_action_id;

	--Check Pack to make it 'free' to a slot
	IF (EXISTS ( SELECT * FROM ad_params WHERE ad_id = i_ad_id AND name = 'pack_status' AND value = 'active' )
		OR (
			EXISTS ( SELECT * FROM ads WHERE ad_id = i_ad_id AND status = 'inactive' )
			AND
			EXISTS ( SELECT * FROM ad_changes WHERE ad_id = i_ad_id AND column_name = 'pack_status' AND new_value = 'active' )
		) ) AND (l_action_type = 'new' OR l_action_type = 'post_refusal' OR l_action_type = 'delete')
	THEN

		SELECT user_id, category INTO l_user_id, l_category FROM ads WHERE ad_id = i_ad_id;

		l_pack_type := find_pack_type(l_category, i_pack_conf);

		SELECT p.pack_id INTO l_pack_id FROM packs p JOIN accounts a USING(account_id)
		WHERE 
			a.user_id = l_user_id
			AND p.status = 'active'
			AND p.used > 0
			AND p.type = l_pack_type::enum_pack_type
		ORDER BY
			date_start DESC
		LIMIT 1 FOR UPDATE;

		IF FOUND THEN
			UPDATE
				packs
			SET
				used = used - 1
			WHERE
				pack_id = l_pack_id;
		ELSE
			RAISE NOTICE 'ERROR_NO_SLOTS_USED';
		END IF;

		SELECT o_action_id, o_state_id INTO l_action_for_pack, l_state_for_pack
		FROM insert_ad_action_state(i_ad_id, 'disable'::enum_ad_actions_action_type,'disabled'::enum_action_states_state, 'admin_disabled'::enum_action_states_transition,'127.0.0.1',null);

		PERFORM insert_ad_change(i_ad_id, l_action_for_pack, l_state_for_pack, true, 'pack_status','disabled');
		PERFORM apply_ad_changes(i_ad_id, l_action_for_pack, NULL);

	END IF;
END;
$$ LANGUAGE plpgsql;
