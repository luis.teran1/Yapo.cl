CREATE OR REPLACE FUNCTION get_ads_by_uid(
	i_uid users.uid%TYPE,
	i_email users.email%TYPE,
	i_ad_status enum_ads_status,
	i_category integer,
	i_ad_id integer,
	i_type varchar,
	i_region integer,
	i_limit integer,
	OUT o_ad_id ads.ad_id%TYPE,
	OUT o_subject ads.subject%TYPE,
	OUT o_body ads.body%TYPE,
	OUT o_category ads.category%TYPE,
	OUT o_status ads.status%TYPE,
	OUT o_list_id ads.list_id%TYPE,
	OUT o_list_time ads.list_time%TYPE,
	OUT o_price ads.price%TYPE,
	OUT o_currency varchar,
	OUT o_image char(10),
	OUT o_email varchar,
	OUT o_account_id accounts.account_id%TYPE,
	OUT o_require_password boolean,
	OUT o_pack_status varchar
) RETURNS SETOF record AS $$
DECLARE
	l_rec record;
	l_uid users.uid%TYPE;
	l_require_password boolean;
	l_type ads.type%TYPE;
	l_query varchar;
BEGIN
	SELECT uid INTO l_uid FROM users where email = i_email;

	IF NOT EXISTS (
		SELECT *
		FROM users JOIN accounts USING (user_id)
		WHERE users.uid = l_uid AND accounts.status = 'active'
	) THEN
		l_require_password := true;
	END IF;

	CASE
		WHEN i_type = 'h' THEN
			l_type := 'rent';
		WHEN i_type = 'k' THEN
			l_type := 'buy';
		WHEN i_type = 'u' THEN
			l_type := 'let';
		ELSE
			l_type := 'sell';
	END CASE;

	-- FORMAT use %N$F
	-- N: POSITION OF PARAM
	-- F IS FORMAT WITH THIS OPTIONS s, I, L
	l_query := FORMAT(
		E'SELECT
			ads.ad_id,
			subject,
			body,
			category,
			ads.status,
			COALESCE(list_id, 0) AS list_id,
			list_time,
			price,
			users.email,
			COALESCE(account_id, 0) AS account_id,
			users.email <> %1$L AS require_password,
			ap1.value AS pack_status,
			ap2.value AS currency
		FROM
			users
			JOIN ads ON (
				users.user_id = ads.user_id
				AND users.uid IN(%9$L, %2$L)
				AND ads.status IN (%3$L)
			)
			LEFT JOIN accounts ON(ads.user_id = accounts.user_id AND accounts.status = \'active\')
			LEFT JOIN ad_params ap1 ON(ads.ad_id = ap1.ad_id AND ap1.name = \'pack_status\')
			LEFT JOIN ad_params ap2 ON(ads.ad_id = ap2.ad_id AND ap2.name = \'currency\')
		WHERE
			CASE WHEN %4$L IS NOT NULL THEN ads.ad_id <> %4$L ELSE true END
			AND CASE WHEN %5$L IS NOT NULL THEN ads.type = %6$L ELSE true END
			AND CASE WHEN %5$L = \'k\' THEN ads.region = %7$L ELSE true END
			AND CASE WHEN %8$L IS NOT NULL AND \'disabled\' IN (%3$L) THEN (%8$L / 1000) = (ads.category / 1000) ELSE true END
		ORDER BY require_password, ads.list_time DESC
		LIMIT
			CASE WHEN %10$s > 0 THEN %10$s ELSE NULL END;',
		i_email,
		i_uid,
		i_ad_status,
		i_ad_id,
		i_type,
		l_type,
		i_region,
		i_category,
		l_uid,
		i_limit
	);

	-- RAISE NOTICE 'OUTPUT_QUERY %', l_query; -- Used for debug process
	FOR
		l_rec
	IN
		EXECUTE l_query
	LOOP
		o_ad_id := l_rec.ad_id;
		o_subject := l_rec.subject;
		o_body := l_rec.body;
		o_category := l_rec.category;
		o_status := l_rec.status;
		o_list_id := l_rec.list_id;
		o_list_time := l_rec.list_time;
		o_price := l_rec.price;
		o_currency := l_rec.currency;
		If o_currency IS NULL THEN
			o_currency := 'peso';
		END IF;

		SELECT lpad(ad_media_id::text, 10, '0') || '.jpg'
		INTO o_image
		FROM ad_media
		WHERE ad_id = l_rec.ad_id AND seq_no = 0;
		IF NOT FOUND THEN
			o_image := '0';
		END IF;

		o_email := l_rec.email;
		o_account_id := l_rec.account_id;
		o_pack_status := l_rec.pack_status;
		IF l_require_password THEN
			o_require_password := l_require_password;
		ELSE
			IF o_account_id = 0 THEN
				o_require_password := true;
			ELSE
				o_require_password := l_rec.require_password;
			END IF;
		END IF;
		RETURN NEXT;
	END LOOP;
END
$$ LANGUAGE plpgsql;
