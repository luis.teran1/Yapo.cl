CREATE OR REPLACE FUNCTION account_associate(i_account_id social_accounts.account_id%TYPE,
		i_social_partner social_accounts.social_partner%TYPE,
		i_social_id social_accounts.social_id%TYPE,
		OUT o_social_account_id social_accounts.social_account_id%TYPE) AS $$
DECLARE
	l_email social_accounts.email%TYPE;
BEGIN
	o_social_account_id := NULL;

	SELECT
		email
	INTO
		l_email
	FROM
		accounts
	WHERE
		account_id = i_account_id;

	IF NOT FOUND THEN
		RAISE EXCEPTION 'FACTORY_TRANSLATE:error:ACCOUNT_NOT_EXISTS';
	END IF;

	SELECT
		account_id
	INTO
		o_social_account_id
	FROM
		social_accounts
	WHERE
		account_id = i_account_id and social_partner = i_social_partner;

	IF FOUND THEN
		RAISE EXCEPTION 'FACTORY_TRANSLATE:error:SOCIAL_ACCOUNT_ALREADY_EXISTS';
	END IF;

	INSERT INTO social_accounts(
		data_joined,
		email,
		social_id,
		social_partner,
		account_id
	) VALUES (
		NOW(),
		l_email,
		i_social_id,
		i_social_partner,
		i_account_id
	) RETURNING
		social_account_id
	INTO
		o_social_account_id;
END;
$$ LANGUAGE plpgsql;
