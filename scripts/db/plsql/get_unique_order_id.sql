CREATE OR REPLACE FUNCTION get_unique_order_id(i_order_id varchar,
					       i_remote_addr action_states.remote_addr%TYPE,
					       OUT o_unique_order_id varchar) AS $$
DECLARE
	l_payment_group_id payments.payment_group_id%TYPE;
BEGIN

	SELECT 
		i_order_id || 'a' || NEXTVAL('order_id_suffix_seq')
	INTO
		o_unique_order_id;

	-- order_id is payment_group_id prefixed by 2 chars.
	l_payment_group_id := substring(i_order_id from 3)::integer;

 	IF NOT insert_pay_log('paycard_save', o_unique_order_id, 0, NULL, NULL, NULL, i_remote_addr, NULL, l_payment_group_id, 'SAVE') THEN
 		o_unique_order_id := '';
 	END IF;
	
END;
$$ LANGUAGE plpgsql;
