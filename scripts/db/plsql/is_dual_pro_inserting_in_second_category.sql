CREATE OR REPLACE FUNCTION is_dual_pro_inserting_in_second_category(
	i_category integer, --Category of ad inserted
	i_first_pack_type varchar,--First pack type
	i_pack_conf varchar[][] --All pack configuration
) RETURNS bool AS $$
DECLARE
	l_other_pack_types varchar[];
	l_other_type varchar;
	l_pack_cats integer[];
BEGIN
	--Search for the others pack types
	l_other_pack_types:= find_others_pack_types(i_first_pack_type, i_pack_conf);

	IF l_other_pack_types IS NOT NULL THEN
		--For each pack_type
		FOREACH l_other_type IN ARRAY l_other_pack_types LOOP
			--Get the categories
			l_pack_cats := find_pack_categories(l_other_type, i_pack_conf);
			--If the category of the ad belongs to this other categories return true
			IF EXISTS( select * from unnest(l_pack_cats) t where t = i_category) THEN
				return true;
			END IF;
		END LOOP;
	END IF;
	--If the category does not belog to other pack return false
	return false;
END;
$$ LANGUAGE plpgsql;

