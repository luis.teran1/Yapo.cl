CREATE OR REPLACE FUNCTION clean_voucher_action(
		i_voucher_id voucher_actions.voucher_id%TYPE,
		i_action_id voucher_actions.voucher_action_id%TYPE,
		OUT o_status integer
		) AS $$
DECLARE
	l_payment_group_id payment_groups.payment_group_id%TYPE;
	l_rec record;
BEGIN
	o_status := 0;

	FOR l_rec IN SELECT * FROM voucher_states WHERE voucher_id = i_voucher_id AND voucher_action_id = i_action_id LOOP
		DELETE FROM voucher_states WHERE voucher_state_id = l_rec.voucher_state_id;
		IF l_rec.token_id IS NOT NULL THEN
			PERFORM clean_token(l_rec.token_id);
		END IF;
	END LOOP;

	DELETE FROM
		voucher_actions
	WHERE
		voucher_id = i_voucher_id
		AND voucher_action_id = i_action_id
	RETURNING
		payment_group_id
	INTO
		l_payment_group_id;

	IF l_payment_group_id IS NOT NULL THEN
		PERFORM clean_payment_group(l_payment_group_id);
	END IF;
END
$$ LANGUAGE plpgsql;
