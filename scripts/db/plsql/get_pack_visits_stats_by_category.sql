CREATE OR REPLACE FUNCTION get_pack_visits_stats_by_category (
					i_category VARCHAR,
					i_user_id VARCHAR,
					i_date DATE
				)
RETURNS FLOAT AS $$
DECLARE
	l_schema VARCHAR;
	o_visits FLOAT;
BEGIN
	l_schema := 'blocket_' || extract(year from i_date);
	EXECUTE format('SELECT AVG(stats.visits) from %s.view_%s stats INNER JOIN ads using (list_id) WHERE category = ANY(ARRAY[%s]) AND status = ''active'' AND DATE(list_time) < DATE ''%s'' AND ads.user_id = %s;',
		l_schema, TO_CHAR(i_date, 'yyyymm'), i_category, i_date, COALESCE(i_user_id, 'ads.user_id'))
		INTO o_visits;
	RETURN o_visits;
END
$$ LANGUAGE plpgsql;
