CREATE OR REPLACE FUNCTION preprocessing_import(
	i_ad_id integer,
	i_action_id integer,
	i_queue ad_actions.queue%TYPE,
	i_remote_addr action_states.remote_addr%TYPE
) RETURNS void AS $$
DECLARE
	l_action_type varchar;
	l_state ad_actions.state%TYPE;
	l_list_id ads.list_id%TYPE;
	l_status ads.status%TYPE;
	l_new_status ads.status%TYPE;
	l_orig_list_time ads.orig_list_time%TYPE;
	l_list_time ads.list_time%TYPE;
	l_old_list_time ads.list_time%TYPE;
BEGIN
	-- get the action type and state of the ad action
	SELECT
		action_type,
		state
	INTO
		l_action_type,
		l_state
	FROM
		ad_actions
	WHERE
		ad_id = i_ad_id AND
		action_id = i_action_id;
	
	-- Check if the ad is an import
	IF l_action_type = 'import' AND l_state = 'pending_review' THEN
		-- fetch the data of the ad
		SELECT
			list_id,
			list_time,
			orig_list_time,
			status
		INTO
			l_list_id,
			l_old_list_time,
			l_orig_list_time,
			l_status
		FROM
			ads
		WHERE
			ad_id = i_ad_id;

		l_list_time := COALESCE(l_old_list_time, CURRENT_TIMESTAMP);

		-- accept the import
		PERFORM accept_action (
				i_ad_id,
				i_action_id,
				'import',
				i_remote_addr,
				NULL,
				NULL,
				'import',
				NULL);

		-- remove from queue
		DELETE
		FROM
			ad_queues
		WHERE
			ad_id = i_ad_id
		AND
			action_id = i_action_id;

		IF l_list_id IS NULL THEN
			l_list_id := NEXTVAL('list_id_seq');
		END IF;
	
		IF l_status = 'hidden' THEN
			l_new_status := 'hidden';
		ELSIF l_list_time IS NULL THEN
			l_new_status := 'inactive';
		ELSE
			l_new_status := 'active';
		END IF;

		IF l_orig_list_time IS NULL THEN
			l_orig_list_time := l_list_time;
		END IF;

		-- update the ad data
		UPDATE
			ads
		SET
			list_id = l_list_id,
			list_time = l_list_time,
			status = l_new_status,
			modified_at = CURRENT_TIMESTAMP,
			orig_list_time = l_orig_list_time
		WHERE
			ad_id = i_ad_id;
	END IF;

END;
$$ LANGUAGE plpgsql;
