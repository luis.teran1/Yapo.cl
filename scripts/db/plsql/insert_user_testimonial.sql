CREATE OR REPLACE FUNCTION insert_user_testimonial(i_ad_id ads.ad_id%TYPE,
				     i_testimonial user_testimonial.content%TYPE,
				     i_site_area enum_user_testimonial_areas,
				     OUT o_status integer) AS $$
DECLARE
	l_ad_id		ads.ad_id%TYPE;
	l_status	ads.status%TYPE;
	l_testimonial	user_testimonial.content%TYPE;
BEGIN
	o_status := -1;

	SELECT
		ad_id,
		status
	INTO
		l_ad_id,
		l_status
	FROM
		ads
	WHERE
		ad_id = i_ad_id;

	IF NOT FOUND THEN
		o_status := 1;
		RETURN;
	END IF;

	-- check status, insert testimonial only on deleted ad
	IF l_status != 'deleted' THEN
		o_status := 2;
		RETURN;
	END IF;	

	IF i_testimonial IS NULL THEN
		o_status := 3;
		RETURN;
	END IF;

	SELECT
		content
	INTO
		l_testimonial
	FROM
		user_testimonial
	WHERE
		ad_id = l_ad_id;

	IF NOT FOUND THEN
		o_status := 0;
		INSERT INTO
			user_testimonial (ad_id, area, content)
		VALUES (l_ad_id,
			i_site_area,
			i_testimonial);
	ELSE
		o_status := 4;
		UPDATE user_testimonial
		SET	content = i_testimonial,
			area	= i_site_area
		WHERE	ad_id	= l_ad_id;
	END IF;

END;
$$ LANGUAGE plpgsql;
