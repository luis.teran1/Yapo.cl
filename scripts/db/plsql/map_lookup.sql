CREATE OR REPLACE FUNCTION map_lookup(
		i_map varchar[][],
		i_key varchar,
		OUT o_value varchar) AS $$
DECLARE
	l_i integer;
BEGIN
	IF i_map IS NOT NULL and array_lower(i_map, 1) IS NOT NULL THEN
		FOR l_i IN array_lower(i_map, 1) .. array_upper(i_map, 1) LOOP
			IF i_key = i_map[l_i][1] THEN
				o_value := i_map[l_i][2];
				RETURN;
			END IF;
		END LOOP;
	END IF;
END;
$$ IMMUTABLE LANGUAGE plpgsql;
