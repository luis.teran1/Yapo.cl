/*
shows each action with the values of the column
*/
CREATE OR REPLACE FUNCTION historical_ad_changes_by_action(i_ad_id ad_changes.ad_id%TYPE,
					i_column_name ad_changes.column_name%TYPE,
				      	i_schema text,
					OUT o_action_id ad_changes.action_id%TYPE,
					OUT o_new_value ad_changes.new_value%TYPE)RETURNS SETOF record AS $$
DECLARE
	l_rec record;
BEGIN
	FOR
		l_rec
	IN
        
	EXECUTE '
		SELECT
			t2.o_action_id,
			(
			 CASE WHEN t2.o_new_value IS NULL THEN   
				(CASE WHEN (SELECT 
							t1.o_new_value --search backward
						FROM 
							changes_per_action('||i_ad_id||','''||i_column_name||''',''' || quote_ident(i_schema) || ''') AS t1
						where 
							t1.o_action_id< t2.o_action_id
						AND
							t1.o_new_value IS NOT NULL
						ORDER BY
							o_action_id 
						DESC LIMIT 1) 

					IS NULL THEN

						(SELECT
							t1.o_old_value --search forward
						FROM
							changes_per_action('||i_ad_id||','''||i_column_name||''',''' || quote_ident(i_schema) || ''') AS t1
						WHERE
							t1.o_action_id> t2.o_action_id
						AND
							t1.o_new_value IS NOT NULL
						ORDER BY 
							o_action_id ASC LIMIT 1)
					ELSE
						(SELECT
							t1.o_new_value --search backward
						FROM
							changes_per_action('||i_ad_id||','''||i_column_name||''',''' || quote_ident(i_schema) || ''') AS t1
						WHERE
							t1.o_action_id< t2.o_action_id
						AND
							t1.o_new_value IS NOT NULL
						ORDER BY
							o_action_id DESC LIMIT 1
						)
					END		
				)
			 ELSE   
				o_new_value
			 END
			) AS o_new_value
		FROM(	
			SELECT		--select the last action
				o_action_id,
				(
					CASE WHEN o_new_value IS NULL 
					THEN
						(SELECT 
							t1.o_old_value 
						FROM 
							changes_per_action('||i_ad_id||','''||i_column_name||''',''' || quote_ident(i_schema) || ''') AS t1 
						WHERE 
							t1.o_action_id>=o_action_id
						AND
							o_old_value IS NOT NULL
						ORDER BY
							o_action_id ASC LIMIT 1 
						)
					ELSE
						o_new_value
					END
				) AS o_new_value
			FROM 
				changes_per_action('||i_ad_id||','''||i_column_name||''',''' || quote_ident(i_schema) || ''')
			WHERE 
				o_action_id=1

			UNION ALL

				SELECT	--select all actions greater than or equal two
					o_action_id,
					o_new_value
				FROM 
					changes_per_action('||i_ad_id||','''||i_column_name||''',''' || quote_ident(i_schema) || ''')
				WHERE 
					o_action_id>=2
			) AS t2
			'
        LOOP
                o_action_id := l_rec.o_action_id;
                o_new_value := l_rec.o_new_value;

                RETURN NEXT;
        END LOOP;
END
$$ LANGUAGE plpgsql;
