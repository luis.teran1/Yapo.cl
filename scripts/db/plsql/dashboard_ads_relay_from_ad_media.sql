CREATE OR REPLACE FUNCTION dashboard_ads_relay_from_ad_media(
) RETURNS TRIGGER AS $$
DECLARE
	l_image_count integer;
BEGIN
	l_image_count := 0;
	IF TG_OP = 'UPDATE' OR TG_OP = 'INSERT' THEN

		IF NEW.ad_id is not null THEN

			SELECT COUNT(1) INTO l_image_count
			FROM ad_media WHERE ad_media.ad_id = NEW.ad_id;

			IF NEW.seq_no = 0 THEN
				UPDATE dashboard_ads
				SET
					ad_media_id = NEW.ad_media_id,
					image_count = l_image_count
				WHERE ad_id = NEW.ad_id;
			ELSE
				UPDATE dashboard_ads
				SET
					image_count = l_image_count
				WHERE ad_id = NEW.ad_id;
			END IF;
		END IF;
	ELSIF TG_OP = 'DELETE' THEN
		IF OLD.ad_id is not null THEN

			SELECT COUNT(1) INTO l_image_count
			FROM ad_media WHERE ad_media.ad_id = OLD.ad_id;

			IF OLD.seq_no = 0 THEN
				UPDATE dashboard_ads
				SET
					ad_media_id = NULL,
					image_count = l_image_count
				WHERE ad_id = OLD.ad_id;
			ELSE
				UPDATE dashboard_ads
				SET
					image_count = l_image_count
				WHERE ad_id = OLD.ad_id;
			END IF;

		END IF;
	END IF;
	RETURN NULL;
END;
$$ LANGUAGE plpgsql;
