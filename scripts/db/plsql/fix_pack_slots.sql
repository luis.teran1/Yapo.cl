CREATE OR REPLACE FUNCTION fix_pack_slots(
    i_email varchar,
    i_token_id tokens.token_id%TYPE,
    OUT o_released_slots integer,
    OUT o_total_slots integer,
    OUT o_pack_ads_count integer
) AS $$
DECLARE
    l_account_id integer;
    l_user_id integer;
    l_pack_ads_count integer;
    l_rec record;
BEGIN
    SELECT account_id, user_id INTO l_account_id, l_user_id
    FROM accounts WHERE email = i_email;

    IF NOT FOUND THEN
        RAISE EXCEPTION 'FACTORY_TRANSLATE:email:ACCOUNT_NOT_FOUND';
    END IF;

    IF EXISTS (SELECT * FROM ads WHERE user_id = l_user_id AND status = 'inactive') THEN
        RAISE EXCEPTION 'FACTORY_TRANSLATE:email:USER_HAVE_ADS_PENDING_REVIEW';
    END IF;


    SELECT count(ad_id) INTO l_pack_ads_count
    FROM ads INNER JOIN ad_params USING (ad_id)
    WHERE status = 'active' AND ad_params.name = 'pack_status' AND  user_id = l_user_id;

    o_pack_ads_count := l_pack_ads_count;

    SELECT COALESCE(sum(used), 0), COALESCE(sum(slots), 0) INTO o_released_slots, o_total_slots
    FROM packs WHERE account_id = l_account_id and status = 'active';

    FOR l_rec IN (
        SELECT pack_id, slots, used
        FROM packs
        WHERE status = 'active' AND account_id = l_account_id
        ORDER BY date_start ASC
        FOR UPDATE
    )
    LOOP
        IF l_pack_ads_count >= l_rec.slots THEN
            UPDATE packs SET used = l_rec.slots WHERE pack_id = l_rec.pack_id;
            o_released_slots := o_released_slots - l_rec.slots;
            l_pack_ads_count := l_pack_ads_count - l_rec.slots;
        ELSE
            UPDATE packs SET used = l_pack_ads_count WHERE pack_id = l_rec.pack_id;
            o_released_slots := o_released_slots - l_pack_ads_count;
            l_pack_ads_count := 0;
        END IF;
    END LOOP;


    IF l_pack_ads_count > 0 THEN
        RAISE NOTICE 'WARNING: this user has not enough slots for his/her ads';
    END IF;

    PERFORM insert_account_action(l_account_id, 'fix_pack_slots', i_token_id);
END;
$$ LANGUAGE plpgsql;
