CREATE OR REPLACE FUNCTION change_account_password(i_email accounts.email%TYPE,
		i_salted_passwd accounts.salted_passwd%TYPE,
		out o_rows integer) AS $$
DECLARE
	l_account_id accounts.account_id%TYPE;
BEGIN
	UPDATE
		accounts
	SET
		salted_passwd = i_salted_passwd
	WHERE
		i_email = email
	RETURNING
		account_id
	INTO
		l_account_id;

	IF NOT FOUND THEN
		RAISE EXCEPTION 'FACTORY_TRANSLATE:error:FORGOT_PASSWORD_CHANGE_ERROR';
	END IF;

	PERFORM insert_account_action(l_account_id,'password_change',NULL);
END;
$$ LANGUAGE plpgsql;
