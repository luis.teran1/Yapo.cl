/*
Return the historical of ad per each action, in the case of price, subject and category.
Call the stored procedure changes_per_action, that return a action_id,old_value and new_value, these two last of table ad_changes.
*/
CREATE OR REPLACE FUNCTION historical_ad_changes(i_ad_id ad_changes.ad_id%TYPE,
				      	i_schema text,
					OUT o_price_old ad_changes.new_value%TYPE,
					OUT o_category_old ad_changes.new_value%TYPE,
					OUT o_subject_old ad_changes.new_value%TYPE,
					OUT o_body_old ad_changes.new_value%TYPE,
					OUT o_internal_memory_old ad_changes.new_value%TYPE,
					OUT o_action_id ad_changes.action_id%TYPE )RETURNS SETOF record AS $$
DECLARE
	l_rec record;
BEGIN
	FOR
		l_rec
	IN
        
	EXECUTE 

		'
		SELECT
			subject.o_action_id AS o_action_id,
			subject.o_value  AS o_subject_old,
			category.o_value AS o_category_old,
			body.o_value AS o_body_old,
			(CASE WHEN price.o_value::text =''NULO''
				THEN
					(SELECT ''''::TEXT)
				ELSE
					price.o_value
				END
			) AS o_price_old,
			(CASE WHEN internal_memory.o_value::text = ''NULO''
				THEN
					(SELECT ''''::TEXT)
				ELSE
					internal_memory.o_value
				END
			) AS o_internal_memory_old
		FROM
			(SELECT
					t1.o_action_id,
					
					(CASE WHEN --if all columns are null takes the value of ads
						 (SELECT COUNT(o_action_id)::int
						 FROM historical_ad_changes_by_action('||i_ad_id||',''price'',''' || quote_ident(i_schema) || ''')) =   ( SELECT COUNT(o_new_value)::int
															FROM historical_ad_changes_by_action('||i_ad_id||',''price'',''' || quote_ident(i_schema) || ''')
															WHERE o_new_value IS NULL)
					THEN ---all rows null
						(SELECT price::text FROM ' || quote_ident(i_schema) || '.ads WHERE ad_id='||i_ad_id||')	
					ELSE --not all rows null
						o_new_value
					END
					)as o_value
			FROM
				historical_ad_changes_by_action('||i_ad_id||',''price'',''' || quote_ident(i_schema) || ''') AS t1
			ORDER BY
				1
			) AS price LEFT JOIN
			(SELECT
					t1.o_action_id,
					(CASE WHEN t1.o_new_value IS NULL THEN  --subject not found in ad_changes
						(SELECT subject::text FROM ' || quote_ident(i_schema) || '.ads WHERE ad_id='||i_ad_id||')
					ELSE  o_new_value
					END ) AS o_value
			FROM  
				historical_ad_changes_by_action('||i_ad_id||',''subject'',''' || quote_ident(i_schema) || ''') AS t1
			ORDER BY
				1
			) AS subject  USING(o_action_id) LEFT JOIN
			(SELECT
					t1.o_action_id,
					(CASE WHEN t1.o_new_value IS NULL THEN
						(SELECT category::text FROM ' || quote_ident(i_schema) || '.ads WHERE ad_id='||i_ad_id||')
					ELSE  o_new_value
					END ) AS o_value
			FROM
				historical_ad_changes_by_action('||i_ad_id||',''category'',''' || quote_ident(i_schema) || ''') AS t1
			ORDER BY
				1
			) AS category  USING(o_action_id) LEFT JOIN
			(SELECT
					t1.o_action_id,
					(CASE WHEN t1.o_new_value IS NULL THEN
						(SELECT body::text FROM ' || quote_ident(i_schema) || '.ads WHERE ad_id='||i_ad_id||')
					ELSE  o_new_value
					END ) AS o_value
			FROM
				historical_ad_changes_by_action('||i_ad_id||',''body'',''' || quote_ident(i_schema) || ''') AS t1
			ORDER BY
				1
			) AS body  USING(o_action_id)   LEFT JOIN
			(SELECT
					t1.o_action_id ,
					(CASE WHEN    --if all rows are null
						 (SELECT COUNT(o_action_id)::int
						 FROM historical_ad_changes_by_action('||i_ad_id||',''internal_memory'',''' || quote_ident(i_schema) || ''')) =   ( SELECT COUNT(o_new_value)::int
															FROM historical_ad_changes_by_action('||i_ad_id||',''internal_memory'',''' || quote_ident(i_schema) || ''')
															WHERE o_new_value IS NULL)
					THEN  -- select internal_memory of ad_params
						(SELECT 
							ad_params.value::TEXT
						FROM ad_params WHERE ad_id='||i_ad_id||' AND name = ''internal_memory''
						
						)	
					ELSE  --if one value is null
						(CASE WHEN (		(SELECT
											(CASE WHEN t2.o_new_value IS NULL THEN
												(SELECT category::TEXT FROM ' || quote_ident(i_schema) || '.ads WHERE ad_id='||i_ad_id||' )
											ELSE  o_new_value::TEXT
											END ) AS o_value
									FROM
										historical_ad_changes_by_action('||i_ad_id||',''category'',''' || quote_ident(i_schema) || ''') AS t2
									WHERE
										o_action_id = t1.o_action_id
									)
								) NOT IN (''3060'') 
							THEN 	 		
								(SELECT ''__''::text)


							ELSE	--if not is category 3060
								o_new_value


								--(SELECT ''djebjdede''::text)
							END
						)
					END ) AS o_value
			FROM
				historical_ad_changes_by_action('||i_ad_id||',''internal_memory'',''' || quote_ident(i_schema) || ''') AS t1
			ORDER BY
				1
			) AS internal_memory  USING(o_action_id) 
		'

	LOOP
		o_price_old := l_rec.o_price_old;
		o_subject_old := l_rec.o_subject_old;
		o_category_old := l_rec.o_category_old;
		o_body_old := l_rec.o_body_old;
		o_internal_memory_old := l_rec.o_internal_memory_old;
		o_action_id := l_rec.o_action_id;

		RETURN NEXT;
	END LOOP;
END
$$ LANGUAGE plpgsql;
