CREATE OR REPLACE FUNCTION get_filter_data_json(i_ad_id ads.ad_id%TYPE,
		i_action_id ad_actions.action_id%TYPE,
		i_user_id users.user_id%TYPE,
		i_ref_types enum_pay_log_references_ref_type[],
		i_references varchar[],
		i_remote_addr action_states.remote_addr%TYPE,
		OUT o_filter_data_json varchar) AS $$
DECLARE
	l_filter_data varchar[][];
	l_i integer;
BEGIN

	if COALESCE(i_ad_id, i_action_id, i_user_id) IS NULL THEN
		RETURN;
	END IF;

	l_filter_data := ARRAY[
		['application', 'clear'],
		['name', get_changed_value(i_ad_id, i_action_id, false, 'name')],
		['subject', get_changed_value(i_ad_id, i_action_id, false, 'subject')],
		['body', get_changed_value(i_ad_id, i_action_id, false, 'body')],
		['phone', get_changed_value(i_ad_id, i_action_id, false, 'phone')],
		['category', get_changed_value(i_ad_id, i_action_id, false, 'category')],
		['email', (
			SELECT
				email
			FROM
				users
			WHERE
				user_id = (SELECT o_value FROM get_changed_value(i_ad_id, i_action_id, false, 'user_id'))::integer
			)],
		['remote_addr', host(i_remote_addr::inet)],
		['country', (SELECT value FROM ad_params WHERE ad_id = i_ad_id AND name = 'country')],
		['uid', (
			SELECT
				uid::text
			FROM
				users
			WHERE
				user_id = i_user_id::integer
			)],
		['lang', get_changed_value(i_ad_id, i_action_id, false, 'lang')]
	];

	-- We ALWAYS (and I mean it) get here with empty ref_types
	IF i_ref_types IS NOT NULL AND array_lower(i_ref_types, 1) IS NOT NULL THEN
		FOR l_i IN array_lower(i_ref_types, 1) .. array_upper(i_ref_types, 1) LOOP
			l_filter_data := l_filter_data || ARRAY[[i_ref_types[l_i]::varchar, i_references[l_i]]];
		END LOOP;
	END IF;

	SELECT
		hstore_to_json(hstore(l_filter_data))
	INTO
		o_filter_data_json;
	
END
$$ LANGUAGE plpgsql;
