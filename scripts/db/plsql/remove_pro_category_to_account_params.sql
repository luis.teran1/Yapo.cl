CREATE OR REPLACE FUNCTION remove_pro_category_to_account_params(
        i_account_id accounts.account_id%TYPE,
        i_account_action_id account_actions.account_action_id%TYPE,
        i_category varchar,
        i_token_id integer,
        i_pack_conf varchar[][],
        OUT o_pro_categories account_params.value%TYPE
        ) AS $$
DECLARE
	l_pro_categories varchar;
	l_first_pack_type varchar;
	l_old_is_pro_for varchar;
	l_old_first_pack_type varchar;
BEGIN

	SELECT value INTO l_old_first_pack_type FROM account_params WHERE account_id = i_account_id AND name = 'pack_type';
	IF NOT FOUND THEN
		l_old_first_pack_type := '';
	END IF;

	SELECT
		COALESCE(string_agg(categories, ','), '') INTO l_pro_categories
	FROM
	(
		SELECT
			UNNEST(string_to_array(value, ',')) AS categories
		FROM
			account_params
		WHERE
			name = 'is_pro_for'
			AND account_id = i_account_id
	) pro
	WHERE
		pro.categories != i_category::TEXT;

	IF l_pro_categories != '' THEN
		UPDATE account_params
		SET value = l_pro_categories
		WHERE
			account_id = i_account_id AND name = 'is_pro_for'
		RETURNING
			value
		INTO
			o_pro_categories;

		l_first_pack_type := first_pack_type(l_pro_categories, i_pack_conf, i_account_id);
		IF l_first_pack_type IS NOT NULL THEN
			UPDATE account_params
			SET value = l_first_pack_type
			WHERE
				account_id = i_account_id AND name = 'pack_type';
		ELSE
			DELETE FROM
				account_params
			WHERE
				account_id = i_account_id
			AND
				name = 'pack_type';
			l_first_pack_type := '';
		END IF;
	ELSE
		DELETE FROM account_params WHERE account_id = i_account_id AND (name = 'is_pro_for' OR name='pack_type');
		o_pro_categories := '';
		l_first_pack_type := '';
	END IF;
	PERFORM insert_account_change(i_account_id, i_account_action_id, true, 'pack_type', l_old_first_pack_type, l_first_pack_type);

END;
$$ LANGUAGE plpgsql;

