CREATE OR REPLACE FUNCTION get_reviewers_times(
                        i_start_date TIMESTAMP, 
                        i_end INTEGER,
                        i_time_interval INTEGER
                        ) RETURNS SETOF review_period AS $$
DECLARE
    admins_record RECORD;
    time_review TIMESTAMP;
    current_record review_period;

BEGIN

    FOR admins_record IN 
        SELECT DISTINCT admin_id, to_char(review_time, 'YYYY-MM-DD') AS day FROM review_log WHERE review_time BETWEEN i_start_date AND i_start_date + INTERVAL '1 day' *i_end ORDER BY 1, 2
    LOOP
        current_record.reviewer_name := '';
        FOR time_review IN
            SELECT review_time FROM review_log where admin_id = admins_record.admin_id AND to_char(review_time, 'YYYY-MM-DD') = admins_record.day ORDER BY 1
            
        LOOP
            IF current_record.reviewer_name = '' THEN
    
                SELECT COALESCE(fullname,username) INTO current_record.reviewer_name FROM admins where admin_id = admins_record.admin_id;
                current_record.day := admins_record.day;
                current_record.start_time := time_review;
                current_record.end_time := time_review;
                current_record.period := INTERVAL '1 minute';
                current_record.total_reviews := 1;
            ELSIF current_record.end_time >= time_review - INTERVAL '1 minutes' * i_time_interval  THEN
                current_record.end_time := time_review;
                current_record.total_reviews := current_record.total_reviews + 1;
                current_record.period := time_review - current_record.start_time;
            ELSE
                RETURN NEXT current_record;

                SELECT COALESCE(fullname, username) INTO current_record.reviewer_name FROM admins where admin_id = admins_record.admin_id;
                current_record.day := admins_record.day;
                current_record.start_time := time_review;
                current_record.end_time := time_review;
                current_record.total_reviews := 1;
                current_record.period := INTERVAL '1 minute';
            END IF;                

        END LOOP;            
        RETURN NEXT current_record;
    END LOOP;

END;
$$ LANGUAGE plpgsql;
