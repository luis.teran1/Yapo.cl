CREATE OR REPLACE FUNCTION get_ad_code(
	i_code_type ad_codes.code_type%TYPE,
	o_code OUT ad_codes.code%TYPE
) AS $$
DECLARE
	l_random_code boolean;
BEGIN

	SELECT value::boolean INTO l_random_code FROM conf WHERE key = '*.*.get_ad_codes.' || i_code_type || '.random';

	-- If it's a verify_code return a random code ( code is a not-null constraint (code,status) )
	IF l_random_code THEN
		-- Prime number selected based on http://planetmath.org/goodhashtableprimes
		o_code := (RANDOM() * 1610612741)::bigint;
		RETURN;
	END IF;

	LOOP
		BEGIN
			DELETE FROM ad_codes WHERE code IN (
				SELECT code FROM ad_codes WHERE code_type = i_code_type LIMIT 1 FOR UPDATE NOWAIT
			) RETURNING code INTO o_code;

			IF NOT FOUND THEN
				RAISE EXCEPTION 'ERROR_NO_PAY_CODES';
			END IF;
		EXCEPTION WHEN lock_not_available THEN
			-- Re-try grabbing an ad-code in case the lock wasn't granted since that indicates
			-- concurrent execution(s) of get_ad_code()
		END;
		EXIT WHEN o_code IS NOT NULL;
	END LOOP;
END;
$$ LANGUAGE plpgsql;
