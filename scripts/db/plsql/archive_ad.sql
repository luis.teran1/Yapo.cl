CREATE OR REPLACE FUNCTION archive_ad(i_ad_ids integer[], i_schema varchar, i_clean boolean) RETURNS void AS $$
DECLARE
	l_schema varchar;
BEGIN

	RAISE NOTICE 'ARCHIVE: Starting a batch of % ads', array_length(i_ad_ids, 1);

	-- set schema
	IF i_schema IS NULL THEN
		l_schema := 'blocket_' || extract(year from CURRENT_TIMESTAMP);
	ELSE
		l_schema := i_schema;
	END IF;

	RAISE NOTICE 'ARCHIVE: Creating temporary table archive_ad_ids';

	-- It seems that a temporary table is more effective than using the array directly.
	CREATE TEMPORARY TABLE
		archive_ad_ids
	ON COMMIT DROP	
	AS SELECT
		i_ad_ids[generate_series] AS ad_id
	FROM
		generate_series(array_lower(i_ad_ids, 1), array_upper(i_ad_ids, 1));
	
	CREATE UNIQUE INDEX unique_archive_ad_ids_ad_id ON archive_ad_ids (ad_id);
	ANALYZE archive_ad_ids;
	
	RAISE NOTICE 'ARCHIVE: Creating temporary table archive_payment_groups_copy';

	-- payment group ids to copy.
	EXECUTE '
		CREATE TEMPORARY TABLE
			archive_payment_groups_copy
		ON COMMIT DROP	
		AS SELECT DISTINCT
			payment_group_id
		FROM
			ad_actions
			JOIN archive_ad_ids USING (ad_id)
			LEFT JOIN ' || quote_ident(l_schema) || '.payment_groups AS already USING (payment_group_id)
		WHERE
			ad_actions.payment_group_id IS NOT NULL
			AND already.payment_group_id IS NULL';
	
	CREATE UNIQUE INDEX unique_archive_payment_groups_copy_payment_group_id ON archive_payment_groups_copy (payment_group_id);
	ANALYZE archive_payment_groups_copy;
	
	RAISE NOTICE 'ARCHIVE: Creating temporary table archive_payment_groups_clean';

	-- Select payment group ids references by our ads but nowhere else.
	-- Note: We do not need to check parent_payment_group_id since payment groups that are
	--       parents are never directly connected to ad_actions.
	CREATE TEMPORARY TABLE
		archive_payment_groups_clean
	ON COMMIT DROP	
	AS SELECT DISTINCT
		ad_actions.payment_group_id
	FROM
		ad_actions
		JOIN archive_ad_ids USING (ad_id)
		LEFT JOIN ad_actions AS ad_actions_still ON
			ad_actions_still.payment_group_id = ad_actions.payment_group_id
			AND ad_actions_still.ad_id NOT IN (SELECT ad_id FROM archive_ad_ids)
		LEFT JOIN voucher_actions AS voucher_actions_still ON
			voucher_actions_still.payment_group_id = ad_actions.payment_group_id
	WHERE
		ad_actions.payment_group_id IS NOT NULL
		AND ad_actions_still.payment_group_id IS NULL
		AND voucher_actions_still.payment_group_id IS NULL;
	
	CREATE UNIQUE INDEX unique_archive_payment_groups_clean_payment_group_id ON archive_payment_groups_clean (payment_group_id);
	ANALYZE archive_payment_groups_clean;

	RAISE NOTICE 'ARCHIVE: Creating temporary table archive_purchase_ids';

	-- Select purchase ids referenced by payment_groups_ids since we will use them as references to the purchase family tables
	CREATE TEMPORARY TABLE
		archive_purchase_ids
	ON COMMIT DROP	
	AS SELECT DISTINCT
		purchase.purchase_id
	FROM
		purchase
		JOIN archive_payment_groups_clean USING (payment_group_id);
	
	CREATE UNIQUE INDEX unique_archive_purchase_ids ON archive_purchase_ids (purchase_id);
	ANALYZE archive_purchase_ids;
	 
	RAISE NOTICE 'ARCHIVE: Disconnecting ad_media images';

	-- Mark media for garbing by disconnecting them from the ads.
	IF i_clean IS NULL OR i_clean THEN
		UPDATE
			ad_media
		SET
			ad_id = NULL
		FROM
			archive_ad_ids
		WHERE
			ad_media.ad_id = archive_ad_ids.ad_id;
	END IF;

	RAISE NOTICE 'ARCHIVE: Starting copy phase copy_ad_from_archive';

	PERFORM copy_ad_from_archive(l_schema);

	IF i_clean IS NULL OR i_clean THEN
		RAISE NOTICE 'ARCHIVE: Starting delete phase clean_ad_from_archive';
		PERFORM clean_ad_from_archive();
	END IF;

	RAISE NOTICE 'ARCHIVE: Done';
END;
$$ LANGUAGE plpgsql;
