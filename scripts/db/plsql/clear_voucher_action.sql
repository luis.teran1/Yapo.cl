CREATE OR REPLACE FUNCTION clear_voucher_action(i_order_id varchar,
						i_ref_types enum_pay_log_references_ref_type[],
						i_references varchar[],
						i_remote_addr voucher_states.remote_addr%TYPE,
						i_amount voucher_actions.amount%TYPE,
						i_clear_status pay_log.status%TYPE,
						OUT o_status integer,
						OUT o_new_balance vouchers.balance%TYPE,
						OUT o_store_name stores.name%TYPE,
						OUT o_timestamp varchar) AS $$
DECLARE
	l_payment_group_id payment_groups.payment_group_id%TYPE;
	l_voucher_id voucher_actions.voucher_id%TYPE;
	l_voucher_action_id voucher_actions.voucher_action_id%TYPE;
	l_amount voucher_actions.amount%TYPE;
	l_state voucher_actions.state%TYPE;
	l_clear_status pay_log.status%TYPE;
BEGIN
	l_payment_group_id := substring(i_order_id from '__#"%#"a%' for '#')::integer;

	SELECT
		voucher_id, 
		voucher_action_id,
		amount,
		state
	INTO
		l_voucher_id,
		l_voucher_action_id,
		l_amount,
		l_state
	FROM
		voucher_actions
	WHERE
		payment_group_id = l_payment_group_id;

	l_clear_status := i_clear_status;
	IF NOT FOUND THEN
		IF l_clear_status = 'OK' THEN
			l_clear_status := 'ERR';
		END IF;
	ELSIF i_amount != l_amount THEN
		RAISE EXCEPTION 'ERROR_AMOUNT_MISMATCH';
	END IF;

	IF l_clear_status = 'OK' THEN
		o_status := 0;
		IF l_state != 'unpaid' THEN
			l_clear_status := 'ERR';
			o_status := 1;
		END IF;
	ELSE
		o_status := 1;
		l_amount := 0;
	END IF;

	IF l_clear_status = 'OK' THEN
		SELECT
			o_balance
		INTO
			o_new_balance
		FROM
			voucher_state_insert(l_voucher_id, l_voucher_action_id, 'paid', 'pay', 'voucher_paycard', i_order_id, i_remote_addr, NULL, i_ref_types, i_references, l_payment_group_id);
	ELSE
		IF NOT insert_pay_log ('voucher_paycard', i_order_id, l_amount, NULL, i_ref_types, i_references, i_remote_addr, NULL, l_payment_group_id, l_clear_status) THEN
			o_status := 5;
		END IF;
	END IF;

	-- Lookup store name
	SELECT
		name
	INTO
		o_store_name
	FROM
		vouchers
		JOIN stores USING (store_id)
	WHERE
		voucher_id = l_voucher_id;


	SELECT
		date_trunc('minute', NOW())
	INTO
		o_timestamp;

END
$$ LANGUAGE plpgsql;
