CREATE OR REPLACE FUNCTION can_insert_pay_log (i_pay_type pay_log.pay_type%TYPE,
					   i_code pay_log.code%TYPE,
					   i_paid_at pay_log.paid_at%TYPE,
					   i_payment_group_id pay_log.payment_group_id%TYPE)
RETURNS bool AS $$
DECLARE
	bogus integer;
BEGIN

	SELECT
		1
	INTO
		bogus
	FROM
		pay_log
	WHERE
		pay_type = i_pay_type AND
		code = i_code AND
		paid_at = CAST(CAST(i_paid_at AS timestamp with time zone) AS timestamp(0))
		AND COALESCE(payment_group_id = i_payment_group_id, true);

	RETURN NOT FOUND;
END
$$ LANGUAGE plpgsql;
