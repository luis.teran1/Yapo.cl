CREATE OR REPLACE FUNCTION insertreport(i_list_id ads.list_id%TYPE,
		 			    i_report_id abuse_reports.report_id%TYPE,
		 			    i_abuse_type abuse_reports.report_type%TYPE,
					    i_email users.email%TYPE,
					    i_message abuse_reports.text%TYPE,
					    i_lang abuse_reports.lang%TYPE,
					    i_uid users.uid%TYPE,
					    i_fake_reporters_email_domain users.email%TYPE,
					    OUT o_uid users.uid%TYPE,
					    OUT o_report_id abuse_reports.report_id%TYPE) AS $$
DECLARE
	l_ad_id ads.ad_id%TYPE; 
	l_uid users.uid%TYPE;
	l_user_id users.user_id%TYPE;
	l_reported_user_id users.user_id%TYPE;
	l_email users.email%TYPE;
	l_report_id abuse_reports.report_id%TYPE;
BEGIN
	--  Check if ad is active
	SELECT 
		ad_id 
	INTO 
		l_ad_id
	FROM 
		ads
	WHERE
		list_id = i_list_id AND 
		status = 'active';

	-- It's not an active ad
	IF NOT FOUND THEN
		RAISE EXCEPTION 'ERROR_NOT_ACTIVE_AD';
	END IF;


	l_uid := i_uid;	
	-- uid/user management
	-- Check if i_uid is a valid uid
	IF i_uid IS NOT NULL THEN
		SELECT
			uid
		INTO
			l_uid
		FROM
			users
		WHERE
			uid = i_uid;
	END IF;

	-- No uid or not valid uid
	IF l_uid IS NULL THEN 
		l_uid := NEXTVAL('uid_seq');
		--fake email
		l_email := EXTRACT('EPOCH' FROM CURRENT_TIMESTAMP) || '@' || i_fake_reporters_email_domain ;

		INSERT INTO
			users
			(uid,		
			 email)
		VALUES
			(l_uid,
			 l_email);

		l_user_id := CURRVAL('users_user_id_seq');

	END IF;

	o_uid := l_uid;

	-- Reporter management
	SELECT 
		uid
	INTO 
		l_uid
	FROM 
		abuse_reporters
	WHERE
		uid = o_uid;
	
	IF NOT FOUND THEN
		INSERT INTO	
			abuse_reporters
			(uid)
		VALUES
			(o_uid);
	END IF;
	
	-- First call
	--IF i_report_id IS NULL THEN
	-- we should check that this is the first report of this user whit this abuse type for this ad 
	SELECT 
		report_id 
	INTO 
		l_report_id
	FROM 
		abuse_reports
	WHERE 
		report_type = i_abuse_type AND
		reporter_id =o_uid AND
		ad_id =	l_ad_id;

	-- Update report
	IF l_report_id IS NOT NULL AND l_report_id <> 0 THEN
		SELECT out_uid, out_report_id INTO l_uid, l_report_id FROM updatereport(l_ad_id, l_report_id, i_abuse_type, i_email, i_message, i_uid, i_fake_reporters_email_domain);
		o_uid := l_uid;
		o_report_id := l_report_id;
		

	-- Insert new report
	ELSE
		INSERT INTO	
			abuse_reports
			(ad_id,
			user_id,
			reporter_id,
			report_type,
			lang)
		VALUES
			(l_ad_id,
			NULL,
			o_uid,
			i_abuse_type,
			i_lang);

		o_report_id := CURRVAL('report_id_seq');
	END IF;
END
$$ LANGUAGE plpgsql;
