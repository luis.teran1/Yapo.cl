/*
Past the parameters ad_id,column_name this stored procedure returns
each action with its modification, empty if there were no changes
and "NULO" in the case that a value of column is not entered
*/
CREATE OR REPLACE FUNCTION changes_per_action(i_ad_id ad_changes.ad_id%TYPE,
						i_column_name ad_changes.column_name%TYPE,
						i_schema text,
						OUT o_ad_id ad_changes.ad_id%TYPE ,
						OUT o_action_id ad_changes.action_id%TYPE ,
						OUT o_old_value ad_changes.old_value%TYPE,
						OUT o_new_value ad_changes.new_value%TYPE)RETURNS SETOF record AS $$
DECLARE
	l_rec record;
BEGIN
	FOR
		l_rec
	IN
        
	EXECUTE 

		'
		SELECT
			t1.ad_id AS o_ad_id,
			t1.action_id AS o_action_id,
			t2.old_value AS o_old_value,
                        (
                        CASE WHEN t2.new_value IS NULL AND
                                               (SELECT 
                                                        COUNT(state_id) 
                                                FROM 
                                                        ad_changes adc1 
                                                WHERE 
                                                        ad_id='||i_ad_id||' 
                                                AND 
                                                        column_name='''|| i_column_name||'''
                                                AND 
                                                        action_id=((    SELECT 
                                                                                action_id 
                                                                        FROM 
                                                                                ad_changes
                                                                        WHERE
                                                                                ad_id='||i_ad_id||' AND column_name = '''|| i_column_name||'''
                                                                        AND 
                                                                                action_id > t1.action_id
									--AND old_value IS NOT NULL 
                                                                        ORDER BY 
                                                                                1 ASC LIMIT 1)) 
                                                ) = 2
                                THEN
                                        (SELECT COALESCE((      SELECT 
                                                                        old_value 
                                                                FROM 
                                                                        ad_changes adc2 
                                                                WHERE 
                                                                        adc2.ad_id='||i_ad_id||' 
                                                                AND 
                                                                        adc2.action_id=(        SELECT
                                                                                                        action_id FROM ad_changes
                                                                                                WHERE
                                                                                                        ad_id='||i_ad_id||'
                                                                                                AND 
                                                                                                        column_name = '''|| i_column_name||'''
                                                                                                AND 
                                                                                                        action_id > t1.action_id
                                                                                                --AND	old_value IS NOT NULL 
												ORDER BY 1 ASC LIMIT 1
                                                                                        )  
                                                                AND
                                                                        adc2.column_name='''||i_column_name||'''
                                                                AND
                                                                        adc2.state_id = (
                                                                                        SELECT
                                                                                                state_id
                                                                                        FROM
                                                                                                ad_changes
                                                                                        WHERE
                                                                                                column_name='''||i_column_name||'''
                                                                                        AND
                                                                                                ad_id='||i_ad_id||' AND action_id=adc2.action_id
                                                                                        ORDER BY 
                                                                                                state_id ASC limit 1
                                                                                        ) 
                                                        ) ,''NULO'')
                                        )
                                ELSE
                                        t2.new_value
                                END
                        )AS o_new_value
		FROM
			(SELECT
				ad_id,
				action_id,
				current_state
			 FROM
				' || quote_ident(i_schema) || '.ad_actions
			 WHERE
				  ad_id='||i_ad_id||' ) AS t1 LEFT JOIN
			(
			SELECT
				COALESCE(new_value,''NULO'') AS new_value,
				COALESCE(old_value,''NULO'') AS old_value,
				ad_id AS ad_id,
				action_id AS action_id,
				state_id AS state_id
				FROM
				      ' || quote_ident(i_schema) || '.ad_changes  adc
				WHERE   column_name='''||i_column_name||'''
				AND     ad_id='||i_ad_id||'
				AND state_id IN(
						SELECT
							state_id
						FROM
						       ' || quote_ident(i_schema) || '.ad_changes
						WHERE   column_name=adc.column_name
						AND     ad_id=adc.ad_id
						AND action_id=adc.action_id
						ORDER BY state_id DESC limit 1
						)
			GROUP BY 1,2,3,4,5
			ORDER BY  ad_id,action_id,state_id
			) AS t2 USING (action_id) ;	
		'

	LOOP
		o_ad_id := l_rec.o_ad_id;
		o_action_id := l_rec.o_action_id;
		o_old_value := l_rec.o_old_value;
		o_new_value := l_rec.o_new_value;

		RETURN NEXT;
	END LOOP;
END
$$ LANGUAGE plpgsql;
