CREATE OR REPLACE FUNCTION get_watch_queries(i_watch_unique_id watch_users.watch_unique_id%TYPE,
					     OUT o_watch_query_id watch_queries.watch_query_id%TYPE,
					     OUT o_query_string watch_queries.query_string%TYPE,
					     OUT o_last_view watch_queries.last_view%TYPE,
					     OUT o_color_id watch_queries.color_id%TYPE,
					     OUT o_adwatch_sms_active BOOLEAN,
					     OUT o_sms_user_status sms_users.status%TYPE,
					     OUT o_watch_query_sms_status watch_queries.sms_status%TYPE,
					     OUT o_num_sms_today integer) RETURNS SETOF record AS $$
DECLARE
	l_rec record;
	l_watch_user_id watch_users.watch_user_id%TYPE;
BEGIN
	l_watch_user_id := get_watch_user_id (i_watch_unique_id);

	IF l_watch_user_id IS NULL THEN
		RAISE EXCEPTION 'FACTORY_TRANSLATE:error:ERROR_NO_SUCH_UNIQUE_ID';
	END IF;

	FOR
		l_rec
	IN

		SELECT
	                watch_queries.*, 
			sms_users.status as sms_user_status
		FROM 
			watch_queries LEFT JOIN sms_users USING (sms_user_id)
		WHERE 
			watch_user_id = l_watch_user_id
		ORDER BY
			watch_query_id
	LOOP
		o_watch_query_id := l_rec.watch_query_id;	
		o_query_string := l_rec.query_string;
		o_last_view := l_rec.last_view;
		o_color_id := l_rec.color_id;
		o_sms_user_status := l_rec.sms_user_status;	
		o_watch_query_sms_status := l_rec.sms_status;

		IF l_rec.sms_user_id IS NULL THEN
			o_adwatch_sms_active := FALSE;
		ELSE
			o_adwatch_sms_active := TRUE;
		END IF;

		o_num_sms_today := get_watch_query_sms_count_today(l_watch_user_id, o_watch_query_id);
		RETURN NEXT;
	END LOOP;
END
$$ LANGUAGE plpgsql;
