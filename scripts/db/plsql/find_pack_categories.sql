CREATE OR REPLACE FUNCTION find_pack_categories(
	i_type varchar,
	i_pack_conf varchar[][],
	OUT o_pack_categories integer[]
) AS $$
DECLARE
	l_pack_conf_array varchar[];
	l_rec record;
	l_value varchar;
	l_counter integer;
BEGIN
	FOREACH l_pack_conf_array SLICE 1 IN ARRAY i_pack_conf LOOP
		IF i_type = 'all' OR EXISTS( select * from unnest(l_pack_conf_array) t where t = i_type::varchar)  THEN
			l_counter := 0;
			FOR l_value IN SELECT unnest(l_pack_conf_array)
			LOOP
				-- skip first element of each sub array and values 0
				IF l_counter > 0 AND l_value != '0' THEN
					o_pack_categories := array_append(o_pack_categories, l_value::integer);
				END IF;
				l_counter := l_counter + 1 ;
			END LOOP;
			IF i_type != 'all' THEN
				EXIT; -- exit loop
			END IF;
		END IF;
	END LOOP;
END;
$$ LANGUAGE plpgsql;
