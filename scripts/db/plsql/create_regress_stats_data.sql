CREATE OR REPLACE FUNCTION create_regress_stats_data()
RETURNS VOID AS $$
DECLARE
	l_date DATE;
	l_interval VARCHAR;
	l_schema VARCHAR;
	l_list_id INTEGER[];
	l_ads INTEGER[];
	l_value INTEGER;
	l_total INTEGER;
	l_rec record;
BEGIN

	FOR l_rec IN (select list_id from ads where list_id is not null) LOOP
		l_total := 0;
		FOR i IN  1..29 LOOP
			l_date := CURRENT_DATE - (INTERVAL '1 DAY' * (29 - i));
			l_schema := 'blocket_' || extract(year from l_date);

			SELECT round(random() * 100) INTO l_value;
			l_total := l_total + l_value;
			RAISE NOTICE '%', format('INSERT INTO %s.view_%s (list_id, date, visits, total) VALUES (%s, ''%s'', %s, %s);', l_schema, TO_CHAR(l_date, 'yyyymm'), l_rec.list_id, l_date, l_value, l_total);
			EXECUTE format('INSERT INTO %s.view_%s (list_id, date, visits, total) VALUES (%s, ''%s'', %s, %s);', l_schema, TO_CHAR(l_date, 'yyyymm'), l_rec.list_id, l_date, l_value, l_total);
		END LOOP;
	END LOOP;
END
$$ LANGUAGE plpgsql;
