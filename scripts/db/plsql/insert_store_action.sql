CREATE OR REPLACE FUNCTION insert_store_action(i_store_id store_actions.store_id%TYPE,
					       i_action_type store_actions.action_type%TYPE,
					       i_payment_group_id store_actions.payment_group_id%TYPE,
					       OUT o_action_id store_actions.action_id%TYPE) AS $$
BEGIN
	LOCK store_actions IN ROW EXCLUSIVE MODE;

	-- add action_state
	SELECT
		COALESCE(max(action_id), 0) + 1
	INTO
		o_action_id
	FROM
		store_actions
	WHERE
		store_id = i_store_id;

	-- insert action
	INSERT INTO
		store_actions
		(store_id,
		 action_id,
		 action_type,
		 payment_group_id)
	VALUES
		(i_store_id,
		 o_action_id,
		 i_action_type,
		 i_payment_group_id);

END;
$$ LANGUAGE plpgsql;
