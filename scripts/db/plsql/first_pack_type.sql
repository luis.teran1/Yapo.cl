CREATE OR REPLACE FUNCTION first_pack_type(
	i_is_pro_for varchar,
	i_pack_conf varchar[][],
	i_account_id integer,
	OUT o_pack_type varchar
) AS $$
DECLARE
	l_pack_conf_array varchar[];
	l_user_pro_cats varchar[];
	l_first_cat varchar;
	l_user_packs varchar[];
	l_pack varchar;
BEGIN
	l_user_pro_cats := string_to_array(i_is_pro_for,',');
	IF l_user_pro_cats IS NOT NULL THEN
		FOREACH l_first_cat IN ARRAY l_user_pro_cats LOOP
			FOREACH l_pack_conf_array SLICE 1 IN ARRAY i_pack_conf LOOP
				IF EXISTS( select * from unnest(l_pack_conf_array) t where t = l_first_cat) THEN
					l_user_packs := l_user_packs || l_pack_conf_array[1];
				END IF;
			END LOOP;
		END LOOP;
	END IF;
	IF l_user_packs IS NOT NULL THEN
		FOREACH l_pack in ARRAY l_user_packs LOOP
			IF EXISTS( SELECT * FROM account_params WHERE account_id = i_account_id AND name = 'pack_type' AND value = l_pack) THEN
				o_pack_type := l_pack;
				RETURN;
			END IF;
		END LOOP;
		IF o_pack_type IS NULL THEN
			o_pack_type := l_user_packs[1];
		END IF;
	END IF;
END;
$$ LANGUAGE plpgsql;
