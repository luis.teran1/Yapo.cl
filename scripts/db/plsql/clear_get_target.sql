CREATE OR REPLACE FUNCTION clear_get_target(i_order_id varchar) RETURNS varchar AS $$
DECLARE
	l_payment_group_id payment_groups.payment_group_id%TYPE;
BEGIN
	IF i_order_id IS NULL THEN
		RETURN 'ad_action';
	END IF;

	l_payment_group_id := substring(i_order_id from '__#"%#"a%' for '#')::integer;

	IF EXISTS (
			SELECT
				*
			FROM
				ad_actions
			WHERE
				payment_group_id = l_payment_group_id
			) THEN
		RETURN 'ad_action';
	END IF;

	IF EXISTS (
		SELECT
			*
		FROM
			store_actions
		WHERE
			payment_group_id = l_payment_group_id
	) THEN
		RETURN 'store_action';
	END IF;
	RAISE EXCEPTION 'ERROR_TARGET_NOT_FOUND';
	RETURN NULL;
END
$$ LANGUAGE plpgsql;
