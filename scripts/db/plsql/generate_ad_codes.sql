CREATE OR REPLACE FUNCTION generate_ad_codes(i_code_type ad_codes.code_type%TYPE, 
					     i_num_codes integer,
					     OUT o_num_codes integer,
					     OUT o_num_generated integer) AS $$
DECLARE
	l_code numeric;
	l_prime numeric;
	l_mod numeric;
	l_off numeric;
	l_seq varchar;
BEGIN
	l_prime := 32535392023;	

	IF i_code_type = 'pay' THEN
		l_mod := 90000;
		l_off := 10000;
	ELSIF i_code_type = 'verify' THEN
		l_mod := 900000000;
		l_off := 100000000;
	ELSIF i_code_type = 'adwatch' THEN
		l_mod := 9000;
		l_off := 1000;
	ELSE
		RAISE EXCEPTION 'ERROR_INVALID_CODE_TYPE';
	END IF;	

	l_seq := i_code_type || '_code_seq';

	SELECT
		COUNT(1)
	INTO
		o_num_codes
	FROM
		ad_codes
	WHERE
		code_type = i_code_type;
	
	o_num_generated := 0;
	WHILE 
		o_num_generated = 0 AND 
		(o_num_codes < i_num_codes OR i_num_codes = 0) AND  -- i_num_codes = 0 means always generate a code
		clock_timestamp() < transaction_timestamp() + INTERVAL '4 seconds' 
	LOOP
		l_code := (NEXTVAL(l_seq) * l_prime) % l_mod + l_off;
		
		IF i_code_type IN ('pay', 'verify') THEN 
			IF NOT EXISTS (
				SELECT
					*
				FROM
					payment_groups
					LEFT JOIN pay_log USING (payment_group_id)
				WHERE
					payment_groups.code = l_code::varchar
					AND added_at > CURRENT_TIMESTAMP - interval '5 days'
					AND CASE
						WHEN payment_groups.status IN ('unpaid', 'unverified') THEN added_at > CURRENT_TIMESTAMP - INTERVAL '3 days'
						ELSE pay_log.status IN ('OK', 'MORE') AND pay_log.paid_at > CURRENT_TIMESTAMP - INTERVAL '3 days'
					END
				)
			AND NOT EXISTS (
				SELECT
					*
				FROM
					ad_codes
				WHERE
					code = l_code::bigint AND
					code_type = i_code_type
				) 
			THEN
				INSERT INTO
					ad_codes
				VALUES
					(l_code, 
					i_code_type);
				o_num_codes := o_num_codes + 1;
				o_num_generated := o_num_generated + 1;
			END IF;
		ELSIF i_code_type = 'adwatch' THEN
			IF NOT EXISTS (
				SELECT 
					*
				FROM 
					watch_queries
				WHERE
					sms_activation_code = l_code::integer
				AND 
					sms_activation_expiry >= CURRENT_TIMESTAMP - interval '2 hours'
			)
			AND NOT EXISTS (
				SELECT
					*
				FROM
					ad_codes
				WHERE
					code = l_code::bigint AND
					code_type = i_code_type
				) 
			THEN
				INSERT INTO
					ad_codes
				VALUES
					(l_code, 
					i_code_type);
				o_num_codes := o_num_codes + 1;
				o_num_generated := o_num_generated + 1;
			END IF;
		END IF;
	END LOOP;	
END;
$$ LANGUAGE plpgsql;

