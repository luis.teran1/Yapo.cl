CREATE OR REPLACE FUNCTION copy_payment_group_from_archive(
		i_schema varchar
		) RETURNS void AS $$
DECLARE
	l_payment_group_ids integer[];
BEGIN
-- payment_groups
	RAISE NOTICE 'ARCHIVE: Copying from payment_groups';
	EXECUTE
		'INSERT INTO ' ||
			quote_ident(i_schema) || '.payment_groups
			(payment_group_id, code, status, added_at)
		SELECT
			payment_group_id, code, status::' || get_column_type (i_schema, 'payment_groups', 'status') || ', added_at
		FROM
			payment_groups
			JOIN archive_payment_groups_copy USING (payment_group_id)';

-- payments
	RAISE NOTICE 'ARCHIVE: Copying from payments';
	EXECUTE
		'INSERT INTO ' ||
			quote_ident(i_schema) || '.payments
			(payment_group_id, payment_type, pay_amount, discount)
		SELECT
			payment_group_id, payment_type::' || get_column_type (i_schema, 'payments', 'payment_type') || ', pay_amount, discount
		FROM
			payments
			JOIN archive_payment_groups_copy USING (payment_group_id)';

-- pay_log
	RAISE NOTICE 'ARCHIVE: Copying from pay_log';
	EXECUTE
		'INSERT INTO ' ||
			quote_ident(i_schema) || '.pay_log
			(pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status)
		SELECT
			pay_log_id, pay_type::' || get_column_type (i_schema, 'pay_log', 'pay_type') ||
			', amount, code, copy_token(token_id, ' || quote_literal(i_schema) || '), 
			paid_at, payment_group_id, status::' || get_column_type (i_schema, 'pay_log', 'status') || '
		FROM
			pay_log
			JOIN archive_payment_groups_copy USING (payment_group_id)';

	RAISE NOTICE 'ARCHIVE: Copying from pay_log_references';
	EXECUTE
		'INSERT INTO ' ||
			quote_ident(i_schema) || '.pay_log_references
			(pay_log_id, ref_num, ref_type, reference)
		SELECT
			pay_log_id, ref_num, ref_type::' || get_column_type (i_schema, 'pay_log_references', 'ref_type') ||
			', reference
		FROM
			pay_log_references
			JOIN pay_log USING (pay_log_id)
			JOIN archive_payment_groups_copy USING (payment_group_id)';
END;
$$ LANGUAGE plpgsql;
