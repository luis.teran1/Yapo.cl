CREATE OR REPLACE FUNCTION telesales_clients_fun(
	i_usecat BOOLEAN,
	i_catlist VARCHAR,
	OUT o_user_id VARCHAR,
	OUT o_name VARCHAR,
	OUT o_email VARCHAR,
	OUT o_phone VARCHAR,
	OUT o_region VARCHAR,
	OUT o_category VARCHAR,
	OUT o_profesional VARCHAR,
	OUT o_active_ads VARCHAR,
	OUT o_published_ads VARCHAR,
	OUT o_refused_ads VARCHAR,
	OUT o_soy VARCHAR,
	OUT o_avg_price_ads VARCHAR,
	OUT o_last_ins_ads VARCHAR,
	OUT o_premium VARCHAR,
	OUT o_new VARCHAR
) RETURNS SETOF RECORD AS $$
DECLARE
	U record;                                                     -- Declare a cursor variable
	U_C record;                                                     -- Declare a cursor variable
	par2 record;                                                     -- Declare a cursor variable
	par3 record;                                                     -- Declare a cursor variable
BEGIN

FOR U IN
	SELECT 
		user_id
	FROM
		users
		JOIN ads USING (user_id)
	WHERE 
		status = 'active'
		AND (NOT i_usecat OR category::varchar = ANY(string_to_array(i_catlist, ',')))
	GROUP BY
		user_id
	HAVING 
		count(*) > 2
	ORDER BY
		user_id
LOOP
	FOR U_C IN
		SELECT			
			SUM(
				CASE WHEN status = 'active' 
				AND (NOT i_usecat OR category::varchar = ANY(string_to_array(i_catlist, ','))) 
				THEN 1 
				ELSE 0 
				END
			) AS active_car,
			MIN(user_id) AS user_id,
			MIN(name) AS name,
			MIN(email) AS email,
			MIN(phone) AS phone,
			STRING_AGG(region::VARCHAR, '|') AS regions,
			MIN(category) AS category,
			BOOL_OR(company_ad) AS pro,
			COUNT(ad_id) AS avisos,
			STRING_AGG(ad_id::VARCHAR, '|') AS ad_ids,
			STRING_AGG(status::VARCHAR, '|') AS ad_statuses,
			SUM(CASE WHEN status = 'active' THEN 1 ELSE 0 END) AS active,
			SUM(CASE WHEN status IN ('active','deleted') THEN 1 ELSE 0 END) AS published,
			SUM(CASE WHEN status = 'refused' THEN 1 ELSE 0 END) AS refused,
			COALESCE(AVG(price), 0) AS avg_price,
			COALESCE(MAX(list_time), NOW())  AS last_ins
		FROM 
			users
			JOIN ads USING(user_id)
		WHERE 
			user_id = U.user_id
		GROUP BY
			user_id, category
		ORDER BY 
			user_id, category
	LOOP 
		FOR par2 IN 
			SELECT
				U_C.user_id as user_id,
				U_C.name as name,
				U_C.email as email,
				U_C.phone as phone,
				(STRING_TO_ARRAY(UNNEST(STRING_TO_ARRAY(U_C.regions, '^')), '|'))[1] AS region,
				U_C.category AS category,
				U_C.pro AS profesional,
				U_C.active AS active_ads,
				U_C.published AS published_ads,
				U_C.refused AS refused_ads,
				U_C.avg_price AS avg_price_ads,
				U_C.last_ins::timestamp(0) AS last_ins_ads
		LOOP
			FOR par3 in 
				SELECT 
					par2.user_id as user_id,
					par2.name as name,
					par2.email as email,
					par2.phone as phone,
					par2.region as region,
					par2.category as category,
					CASE WHEN par2.profesional::BOOLEAN THEN 'yes' ELSE 'no' END AS profesional,
					par2.active_ads as active_ads,
					par2.published_ads as published_ads,
					par2.refused_ads as refused_ads,
					(SELECT
						COUNT(*)
					FROM
						ads
						JOIN action_params USING (ad_id)
						JOIN users USING(user_id)
					WHERE
						user_id = par2.user_id
						AND category = par2.category::INTEGER
						AND action_params.name = 'deletion_reason'
						AND value = '1'
					) AS soy,
					ROUND(par2.avg_price_ads::numeric) AS avg_price_ads,
					par2.last_ins_ads as last_ins_ads,
					(SELECT
						CASE WHEN COUNT(*) > 0 THEN 'yes' ELSE 'no' END
					FROM
						ad_actions
						JOIN ads USING(ad_id)
						JOIN users USING(user_id)
					WHERE
						user_id = par2.user_id
						AND category = par2.category::INTEGER
						AND action_type IN ('bump', 'weekly_bump')
					) AS premium,
					'new' AS new
			LOOP 
				o_user_id := par3.user_id;
				o_name := par3.name;
				o_email := par3.email;
				o_phone := par3.phone;
				o_region := par3.region;
				o_category := par3.category;
				o_profesional := par3.profesional;
				o_active_ads := par3.active_ads;
				o_published_ads := par3.published_ads;
				o_refused_ads := par3.refused_ads;
				o_soy := par3.soy;
				o_avg_price_ads := par3.avg_price_ads;
				o_last_ins_ads := par3.last_ins_ads;
				o_premium := par3.premium;
				o_new := par3.new;
				return NEXT;
			END LOOP;
		END LOOP;	
	END LOOP;
END LOOP;
END;
$$ LANGUAGE plpgsql;
