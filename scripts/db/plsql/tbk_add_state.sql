CREATE OR REPLACE FUNCTION tbk_add_state(i_tbk_purchase_order payment_groups.payment_group_id%TYPE,
	i_status varchar,
	i_tbk_response pay_log_references.reference%TYPE,
	i_remote_addr action_states.remote_addr%TYPE,
	i_remote_browser varchar,
	i_ref_auth_code pay_log_references.reference%TYPE,
	i_ref_trans_date pay_log_references.reference%TYPE,
	i_ref_placements_type pay_log_references.reference%TYPE,
	i_ref_placements_number pay_log_references.reference%TYPE,
	i_ref_pay_type pay_log_references.reference%TYPE,
	i_ref_external_id pay_log_references.reference%TYPE,
	i_ref_cc_number pay_log_references.reference%TYPE,
	OUT o_state varchar(100)) AS $$
DECLARE
	l_action_id ad_actions.action_id%TYPE;
	l_ad_id ad_actions.ad_id%TYPE;
	l_purchase_id purchase.purchase_id%TYPE;
	l_total_price purchase.total_price%TYPE;
	l_ad_code pay_log.code%TYPE;
BEGIN
	l_action_id := 0;
	l_ad_id := 0;
	l_purchase_id = 0;
	o_state := 'none';


	-- Get the purchase_id with the payment_group_id
	SELECT
		purchase_id, total_price
	INTO
		l_purchase_id, l_total_price
	FROM
		purchase
	WHERE
		payment_group_id = i_tbk_purchase_order;

	IF NOT FOUND THEN
		RAISE EXCEPTION 'ERROR_NO_SUCH_PURCHASE_ID';
	END IF;

	-- get the ad_code for the payment
	SELECT
		code
	INTO
		l_ad_code
	FROM
		payment_groups
	WHERE
		payment_group_id = i_tbk_purchase_order;

	o_state := i_status;

	-- Get the current ad action
	SELECT
		action_id, ad_id
	INTO
		l_action_id, l_ad_id
	FROM
		ad_actions
	WHERE
		payment_group_id = i_tbk_purchase_order;

	IF o_state = 'authorized' THEN
		
		-- Update pay tables (the blocket tables)
		UPDATE payment_groups SET status = 'unpaid' WHERE payment_group_id = i_tbk_purchase_order OR parent_payment_group_id = i_tbk_purchase_order;
		
		PERFORM insert_pay_log('paycard_save',  --pay_log.pay_type
					l_ad_code, 	--pay_log.code
					0, 		--pay_log.amount
					null, 		--pay_log.paid_at (timestamp)
					ARRAY['auth_code','trans_date','placements_type','placements_number','pay_type','external_response','external_id','cc_number']::enum_pay_log_references_ref_type[], 	--pay_log_references.ref_type[]
					ARRAY[i_ref_auth_code,i_ref_trans_date,i_ref_placements_type,i_ref_placements_number,i_ref_pay_type,i_tbk_response,i_ref_external_id,i_ref_cc_number]::varchar[],
					i_remote_addr, 	--remote_address
					null, 		--Token
					i_tbk_purchase_order, --Payment_group_id
					'SAVE');	--pay_log.status
		

	END IF;

	IF o_state = 'payment_ok' THEN

		-- Update pay tables (the blocket tables)
		UPDATE payment_groups SET status = 'paid' WHERE payment_group_id = i_tbk_purchase_order;

		-- Update all childs because packs doesn't paid using clear
		UPDATE payment_groups SET status = 'paid' WHERE parent_payment_group_id = i_tbk_purchase_order;

		-- New pay_log
		INSERT INTO
			pay_log
			(pay_type, amount, code, paid_at, payment_group_id, status) 
		VALUES
			('paycard', l_total_price, l_ad_code, NOW(), i_tbk_purchase_order, 'OK');
	END IF;

	IF o_state <> 'init' AND o_state <> 'authorized' AND o_state <> 'payment_ok' THEN
		-- The transaction failed
		UPDATE
			purchase
		SET
			status = 'refused'
		WHERE
			purchase_id = l_purchase_id;

		-- New pay_log
		INSERT INTO pay_log
			(pay_type, code, paid_at, payment_group_id, status)
		VALUES
			('paycard', l_ad_code, NOW(), i_tbk_purchase_order, o_state::enum_pay_log_status);
	END IF;
END;
$$ LANGUAGE plpgsql;
