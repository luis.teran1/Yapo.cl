CREATE OR REPLACE FUNCTION watch_query(i_watch_unique_id watch_users.watch_unique_id%TYPE,
				       i_watch_query_id watch_queries.watch_query_id%TYPE,
				       i_query_string watch_queries.query_string%TYPE,
				       i_colors integer[],
				       i_max_queries integer,
				       OUT o_watch_unique_id watch_users.watch_unique_id%TYPE,
				       OUT o_watch_user_id watch_users.watch_user_id%TYPE,
				       OUT o_watch_query_id watch_queries.watch_query_id%TYPE,
				       OUT o_color_id watch_queries.color_id%TYPE) AS $$
BEGIN
	o_watch_user_id := get_watch_user_id (i_watch_unique_id);
	-- User admin
	IF o_watch_user_id IS NOT NULL THEN
		o_watch_unique_id := i_watch_unique_id;
	ELSE
		-- Create new user
		SELECT
			create_watch_user.o_watch_user_id,
			create_watch_user.o_watch_unique_id
		INTO
			o_watch_user_id,
			o_watch_unique_id
		FROM
			create_watch_user();
	END IF;

	-- Query
	UPDATE
		watch_queries
	SET
		query_string = i_query_string
	WHERE
		watch_user_id = o_watch_user_id
		AND watch_query_id = i_watch_query_id
	RETURNING
		color_id
	INTO
		o_color_id;

	IF NOT FOUND THEN
		LOCK watch_queries IN ROW EXCLUSIVE MODE;

		-- Check max queries
		IF (SELECT COUNT(1) FROM watch_queries WHERE watch_user_id = o_watch_user_id) >= i_max_queries THEN
			RAISE EXCEPTION 'FACTORY_TRANSLATE:error:ERROR_MAX_WATCH_QUERIES_REACHED';
		END IF;	

		-- Choose a random available color
		SELECT
			i_colors[generate_series]
		INTO
			o_color_id
		FROM
			generate_series (array_lower (i_colors, 1), array_upper (i_colors, 1))
			LEFT JOIN watch_queries ON
				watch_user_id = o_watch_user_id
				AND color_id = i_colors[generate_series]
		WHERE
			color_id IS NULL
		ORDER BY
			random();

		IF o_color_id IS NULL THEN
			o_color_id := i_colors[1];
		END IF;

		-- Check if query exists
		
		IF EXISTS ( 
			   SELECT 
			   	watch_query_id
			   FROM
			   	watch_queries
			   WHERE
			   	watch_user_id = o_watch_user_id AND
			   	query_string = i_query_string) THEN
				   RAISE EXCEPTION 'FACTORY_TRANSLATE:error:ERROR_WATCH_QUERY_EXISTS';
		END IF;

		-- Insert new query and set watch_query_id = MAX + 1
		INSERT INTO watch_queries (
			watch_user_id,
			watch_query_id,
			query_string,
			color_id)
		VALUES
			(o_watch_user_id,
			 COALESCE((SELECT MAX(watch_query_id) + 1 FROM watch_queries WHERE watch_user_id = o_watch_user_id), 1),
			 i_query_string,
			 o_color_id)
		RETURNING
			watch_query_id
		INTO
			o_watch_query_id;
	ELSE
		o_watch_query_id := i_watch_query_id;
	END IF;
END
$$ LANGUAGE plpgsql;
