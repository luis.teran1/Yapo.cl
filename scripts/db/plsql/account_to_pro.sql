CREATE OR REPLACE FUNCTION account_to_pro(
					i_email accounts.email%TYPE,
					i_category ads.category%TYPE,
					i_pack_conf varchar[][],
					OUT o_is_pro boolean,
					OUT o_just_became_pro boolean) AS $$
DECLARE
    l_user_id ads.user_id%TYPE;
    l_action_id ad_actions.action_id%TYPE;
    l_state_id action_states.state_id%TYPE;
    l_account_id accounts.account_id%TYPE;
    l_account_action_id account_actions.account_action_id%TYPE;
    l_pro_categories varchar;
    l_old_is_pro_for varchar;
	l_ad record;
BEGIN

	o_is_pro := true;
	o_just_became_pro := false;

	SELECT
		user_id
	INTO
		l_user_id
	FROM
		users
	WHERE
		email = lower(i_email);

	-- update the account
	-- TODO, check redis
	UPDATE accounts SET is_company = 't' WHERE email = lower(i_email) AND is_company = 'f';
	IF FOUND THEN
		o_just_became_pro := true;
	END IF;

	--Get account_id
	select account_id INTO l_account_id FROM accounts WHERE user_id = l_user_id;
	IF FOUND THEN
		IF NOT exists (select value from account_params where account_id = l_account_id and name = 'is_pro_for' and value like '%'||i_category||'%') THEN
			SELECT value INTO l_old_is_pro_for FROM account_params WHERE account_id = l_account_id AND name = 'is_pro_for';
			IF NOT FOUND THEN
				l_old_is_pro_for := '';
			END IF;
			l_account_action_id := insert_account_action(l_account_id, 'add_pro_category_param', null);
			l_pro_categories := insert_pro_category_to_account_params(l_account_id, l_account_action_id, i_category::text, NULL, i_pack_conf);
			PERFORM insert_account_change(l_account_id, l_account_action_id, true, 'is_pro_for', l_old_is_pro_for, l_pro_categories);
		END IF;
	END IF;

	-- check if the user (without account) just became pro
	PERFORM ad_id FROM ads WHERE status = 'active' AND user_id = l_user_id AND company_ad = 't';
	IF NOT FOUND THEN
		o_just_became_pro := true;
	END IF;

	-- inserting ad change to all the user ads
	FOR l_ad IN
	UPDATE ads SET company_ad = 't'
		WHERE
			status = 'active' AND user_id = l_user_id
			RETURNING ad_id
	LOOP
		-- check if the add is not already in 'account_to_pro' state
		PERFORM action_id FROM ad_actions WHERE ad_id = l_ad.ad_id AND action_type = 'account_to_pro';
		IF NOT FOUND THEN
			-- change it to pro
			l_action_id := insert_ad_action(l_ad.ad_id, 'account_to_pro', NULL);
			PERFORM insert_state(l_ad.ad_id, l_action_id, 'reg', 'initial', '127.0.0.1', NULL);
			l_state_id := insert_state(l_ad.ad_id, l_action_id, 'accepted', 'adminclear', '127.0.0.1', NULL);
			-- apply ad_changes
			PERFORM apply_ad_changes(l_ad.ad_id, l_action_id, NULL);
		END IF;
	END LOOP;

END
$$ LANGUAGE plpgsql;
