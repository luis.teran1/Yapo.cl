CREATE OR REPLACE FUNCTION get_ad_value(i_ad_id ads.ad_id%TYPE,
					OUT o_sum payments.pay_amount%TYPE) AS $$
DECLARE
	l_extra_images_price payments.pay_amount%TYPE;
	l_gallery_price payments.pay_amount%TYPE;
BEGIN
	-- Get last payment for ad_action that is not edit
	SELECT
		pay_amount - discount
	INTO
		o_sum
	FROM
		payments JOIN
		ad_actions USING (payment_group_id)
	WHERE
		ad_actions.ad_id = i_ad_id AND
		ad_actions.action_type IN ('new', 'renew', 'prolong') AND
		payments.payment_type = 'ad_action'
	ORDER BY
		action_id DESC
	LIMIT 1;

	-- Get the last extra_images payment if ad currently has extra_images
	IF EXISTS(SELECT * FROM ad_media WHERE (media_type = 'image' OR media_type IS NULL) AND ad_id = i_ad_id AND seq_no > 0) THEN
		SELECT
			pay_amount - discount
		INTO
			l_extra_images_price
		FROM
			payments JOIN
			ad_actions USING (payment_group_id)
		WHERE
			ad_actions.ad_id = i_ad_id AND
			payments.payment_type = 'extra_images'
		LIMIT 1;
	END IF;

	-- If have paid for extra images then add it to the total sum
	IF l_extra_images_price IS NOT NULL THEN
		o_sum := o_sum + l_extra_images_price;
	END IF;

	-- Get the last gallery payment if ad currently has gallery
	IF EXISTS(SELECT * FROM ad_params WHERE name = 'gallery' AND ad_id = i_ad_id AND value::timestamp > CURRENT_TIMESTAMP) THEN
		SELECT
			pay_amount - discount
		INTO
			l_gallery_price
		FROM
			payments JOIN
			ad_actions USING (payment_group_id)
		WHERE
			ad_actions.ad_id = i_ad_id AND
			payments.payment_type = 'gallery'
		LIMIT 1;
	END IF;

	-- If have paid for gallery then add it to the total sum
	IF l_gallery_price IS NOT NULL THEN
		o_sum := o_sum + l_gallery_price;
	END IF;

	RETURN;
END
$$ LANGUAGE plpgsql;
