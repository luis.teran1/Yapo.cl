CREATE OR REPLACE FUNCTION add_purchase_detail_param(
	i_payment_group_id  purchase_detail.payment_group_id%TYPE,
	i_param_name purchase_detail_params.name%TYPE,
	i_param_value purchase_detail_params.value%TYPE,
	OUT o_purchase_detail_id integer) AS $$
BEGIN
	SELECT
		purchase_detail_id
	INTO
		o_purchase_detail_id
	FROM
		purchase_detail
	WHERE
		payment_group_id = i_payment_group_id;

	IF NOT FOUND THEN
		o_purchase_detail_id := 0;
		RETURN;
	END IF;

	PERFORM
		value
	FROM
		purchase_detail_params
	WHERE
		purchase_detail_id = o_purchase_detail_id AND name = i_param_name;

	IF NOT FOUND THEN
		INSERT INTO
			purchase_detail_params (purchase_detail_id, name, value)
		VALUES (o_purchase_detail_id,
			i_param_name,
			i_param_value);
		IF i_param_name = 'error' THEN
			INSERT INTO purchase_detail_params (purchase_detail_id, name, value)
			VALUES (o_purchase_detail_id, 'error_at', now()::text);
		END IF;
	ELSE
		UPDATE purchase_detail_params
		SET value = i_param_value
		WHERE purchase_detail_id = o_purchase_detail_id and name = i_param_name;
		IF i_param_name = 'error' THEN
			UPDATE purchase_detail_params
			SET value = now()::text
			WHERE purchase_detail_id = o_purchase_detail_id and name = 'error_at';
		END IF;
	END IF;
END;
$$ LANGUAGE plpgsql;
