CREATE OR REPLACE FUNCTION update_store_status(
					i_store_id stores.store_id%TYPE,
					i_status stores.status%TYPE,
					i_token_id tokens.token_id%type,
					i_payment_group_id payment_groups.payment_group_id%TYPE,
					OUT o_store_status stores.status%TYPE
					) AS $$

DECLARE
	l_store_id stores.store_id%TYPE;
	l_store_old_status stores.status%TYPE;
	l_action_id integer;
	l_state_id integer;
BEGIN

	SELECT
		store_id, status
	INTO
		l_store_id, l_store_old_status
	FROM
		stores
	WHERE
		store_id = i_store_id;

	IF l_store_id IS NULL THEN
		RAISE EXCEPTION 'ERROR_STORE_NOT_FOUND';
	END IF;

	SELECT
		isas.o_action_id,
		isas.o_state_id
	INTO
		l_action_id,
		l_state_id
	FROM
		insert_store_action_state(CAST(l_store_id AS integer),'status_change','reg',i_token_id,i_payment_group_id) AS isas;

	PERFORM insert_store_change(i_store_id, l_action_id, l_state_id, false, 'status'::text, i_status::text);
	PERFORM apply_store_changes(l_store_id, l_action_id);
	PERFORM insert_store_state(l_store_id, l_action_id, 'accepted', i_token_id);

	o_store_status = i_status;
END;
$$ LANGUAGE plpgsql;
