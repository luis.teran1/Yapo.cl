CREATE OR REPLACE FUNCTION insert_account_change(
	i_account_id accounts.account_id%TYPE,
	i_account_action_id account_actions.account_action_id%TYPE,
	i_is_param account_changes.is_param%TYPE,
	i_column_name account_changes.column_name%TYPE,
	i_old_value account_changes.old_value%TYPE,
	i_new_value account_changes.new_value%TYPE
) RETURNS VOID AS $$
BEGIN

	IF i_old_value IS NULL AND i_new_value IS NULL THEN
		RETURN;
	END IF;

	IF i_old_value IS NULL OR i_new_value IS NULL OR i_old_value != i_new_value THEN
		INSERT INTO
			account_changes
			(account_id, account_action_id, is_param, column_name, old_value, new_value)
		VALUES
			(i_account_id, i_account_action_id, i_is_param, i_column_name, i_old_value, i_new_value);
	END IF;
END;
$$ LANGUAGE plpgsql;
