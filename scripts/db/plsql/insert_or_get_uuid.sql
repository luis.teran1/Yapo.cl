CREATE OR REPLACE FUNCTION insert_or_get_uuid(i_email VARCHAR,
								o_uuid OUT UUID) AS $$
BEGIN
	LOOP
		SELECT uuid INTO o_uuid FROM email_uuid WHERE email = i_email;
		IF NOT FOUND THEN
			BEGIN
				INSERT INTO email_uuid(email) VALUES (i_email) RETURNING uuid INTO o_uuid;
			EXCEPTION WHEN unique_violation THEN
				-- Do nothing, and loop to try the UPDATE again.
				CONTINUE;
			END;
		END IF;
		EXIT;
	END LOOP;
END;
$$
LANGUAGE plpgsql;
