CREATE OR REPLACE FUNCTION get_queue_for_action(i_ad_id ads.ad_id%TYPE,
		i_action_id ad_actions.action_id%TYPE,
		i_user_id users.user_id%TYPE,
		i_ref_types enum_pay_log_references_ref_type[],
		i_references varchar[],
		i_remote_addr action_states.remote_addr%TYPE,
		i_auto_move_abuse bool,
		i_no_auto_categories integer[],
		i_category_price_limits integer[][],
		i_global_price_limit integer,
		i_blocked_items_lists integer[],
		i_queue_prios varchar[][],
		i_queue_percent_groups varchar[],
		i_queue_percent varchar[][][],
		i_pack_result integer,
		i_filter_data_jstr varchar,
		OUT o_queue ad_actions.queue%TYPE) AS $$
DECLARE
	l_filter_data varchar[][];
	l_prio integer;
	l_new_prio integer;
	l_percent integer;
	l_rec record;
	l_i integer;
	l_queue_prio_groups varchar[];
	l_action_type ad_actions.action_type%TYPE;
	l_o_queue ad_actions.queue%TYPE;
	l_force_queue varchar;
	l_json_cursor CURSOR FOR SELECT * FROM json_populate_recordset(null::filter_data_result, i_filter_data_jstr::json);
	l_array_cursor CURSOR FOR SELECT * FROM filter_data('clear', l_filter_data);
BEGIN

	-- Check if the ad must be moved to other queue
	l_force_queue := has_force_queue(i_ad_id,i_action_id);
	-- RAISE NOTICE 'Forced queue: OLD % -> NEW %, (% - %)', o_queue, l_force_queue, i_ad_id,i_action_id;
	IF l_force_queue IS NOT NULL THEN
		o_queue := l_force_queue;
		RETURN;
	END IF;

	-- Alright, is not DTB so let's do our regular stuff
	o_queue := 'normal';
	l_queue_prio_groups=ARRAY['default'];

    -- Check if the ad is edit or edit_refused
	SELECT action_type INTO l_action_type FROM ad_actions WHERE action_id = i_action_id AND ad_id = i_ad_id;
	IF l_action_type = 'edit'  THEN
		o_queue := 'edit';
	END IF;

	-- logic to check is is a ad from a new user
	IF is_newbie_user(i_user_id) THEN
		o_queue := 'first_da';
	END IF;

	IF autorefuse(i_ad_id, i_action_id) THEN
		o_queue := 'autorefuse';
	ELSIF autoaccept(i_ad_id, i_action_id, i_no_auto_categories, i_category_price_limits, i_global_price_limit, i_blocked_items_lists) THEN
		o_queue := 'autoaccept';
	END IF;

	IF i_pack_result != -1 THEN
		o_queue := 'pack';
	END IF;

	IF i_pack_result = -3 THEN
		o_queue := 'double_pro_refuse';
	END IF;
	IF i_pack_result = -4 THEN
		o_queue := 'pro_refuse_changes';
	END IF;
	IF i_pack_result = 5 THEN
		o_queue := 'edit';
	END IF;

	-- Move to abuse queue if requested and user has abuse notices
	IF i_auto_move_abuse AND has_abuse_notices(i_user_id) THEN
		o_queue := 'abuse';
	END IF;

	l_prio := queue_in_array_lookup('default', o_queue::text, l_queue_prio_groups, i_queue_prios);
	IF l_prio IS NULL THEN
		l_prio := -1;
	END IF;

	IF i_filter_data_jstr IS NULL THEN
		l_filter_data := ARRAY[
			['name', get_changed_value(i_ad_id, i_action_id, false, 'name')],
			['subject', get_changed_value(i_ad_id, i_action_id, false, 'subject')],
			['body', get_changed_value(i_ad_id, i_action_id, false, 'body')],
			['phone', get_changed_value(i_ad_id, i_action_id, false, 'phone')],
			['category', get_changed_value(i_ad_id, i_action_id, false, 'category')],
			['email', (
				SELECT
					email
				FROM
					users
				WHERE
					user_id = (SELECT o_value FROM get_changed_value(i_ad_id, i_action_id, false, 'user_id'))::integer
				)],
			['remote_addr', host(i_remote_addr::inet)],
			['country', (SELECT value FROM ad_params WHERE ad_id = i_ad_id AND name = 'country')],
			['uid', (
				SELECT
					uid::text
				FROM
					users
				WHERE
					user_id = i_user_id::integer
				)],
			['lang', get_changed_value(i_ad_id, i_action_id, false, 'lang')]
		];

		-- We ALWAYS (and I mean it) get here with empty ref_types
		IF i_ref_types IS NOT NULL AND array_lower(i_ref_types, 1) IS NOT NULL THEN
			FOR l_i IN array_lower(i_ref_types, 1) .. array_upper(i_ref_types, 1) LOOP
				l_filter_data := l_filter_data || ARRAY[[i_ref_types[l_i]::varchar, i_references[l_i]]];
			END LOOP;
		END IF;

		OPEN l_array_cursor;
	ELSE
		OPEN l_json_cursor;
	END IF;

	-- Save this value to avoid something womewhere
	l_o_queue := o_queue;

	LOOP
		-- If someone knows a prettier way to do this, please tell me!
		IF i_filter_data_jstr IS NULL THEN
			FETCH NEXT FROM l_array_cursor INTO l_rec;
		ELSE
			FETCH NEXT FROM l_json_cursor INTO l_rec;
		END IF;
		EXIT WHEN l_rec IS NULL;
		IF l_rec.o_action = 'move_to_queue' AND l_rec.o_target IS NOT NULL  AND ((l_rec.o_target = 'whitelist' AND o_queue != 'pack' AND i_pack_result != 5) OR l_rec.o_target != 'whitelist')  THEN
			l_new_prio := queue_in_array_lookup('default', l_rec.o_target::text, l_queue_prio_groups, i_queue_prios);
			IF l_new_prio IS NULL THEN
				l_new_prio := 0;
			END IF;
			l_percent := queue_in_array_lookup('default', l_rec.o_target::text, i_queue_percent_groups, i_queue_percent);
			-- RAISE NOTICE 'percentage for queue % is %', l_rec.o_target, l_percent;
			IF l_percent IS NULL THEN
				l_percent := 100;
			END IF;
			-- RAISE NOTICE 'considering queue % prio % vs. queue % prio %', l_rec.o_target, l_new_prio, o_queue, l_prio;
			IF l_new_prio > l_prio AND 100*random() <= l_percent THEN
				o_queue := l_rec.o_target;
				l_prio := l_new_prio;
			END IF;
		END IF;
	END LOOP;

	IF o_queue = 'jobs_evaders' THEN
		IF is_inserting_fee_ad(i_ad_id, null) THEN
			o_queue := 'jobs';
		END IF;
	END IF;
	IF o_queue = 'whitelist' AND is_force_review(i_ad_id, i_action_id) THEN
		-- RAISE NOTICE 'Forced review: Ad was supposed to go to whitelist -> Goes to normal';
		o_queue := l_o_queue;
	END IF;

END
$$ LANGUAGE plpgsql;
