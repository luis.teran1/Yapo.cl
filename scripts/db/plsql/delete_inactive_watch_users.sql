CREATE OR REPLACE FUNCTION delete_inactive_watch_users (i_garb_days int,
							OUT o_set int) RETURNS SETOF INT AS $$
DECLARE
	l_user_id int;
	l_rec record;
BEGIN
	FOR
		l_rec
	IN
		SELECT	
			watch_user_id 
		FROM 
			watch_users 
		WHERE 
			last_activity < CURRENT_TIMESTAMP - INTERVAL '1 day' *  i_garb_days
	LOOP
		l_user_id := l_rec.watch_user_id;
		DELETE FROM
			watch_queries 
		WHERE
			watch_user_id = l_user_id;
	
		DELETE FROM 
			watch_ads
		WHERE
			watch_user_id = l_user_id;
	 
		PERFORM delete_watch_user(l_user_id);
		o_set := l_user_id;
		RETURN NEXT;
	END LOOP;
END
$$ LANGUAGE plpgsql;
