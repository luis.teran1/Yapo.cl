CREATE OR REPLACE FUNCTION validate_token(i_token tokens.token%TYPE,
					  i_remote_addr tokens.remote_addr%TYPE,
					  i_valid_interval INTERVAL,
					  i_info tokens.info%TYPE,
					  i_priv_required VARCHAR(50)[],
					  i_store_id_required stores.store_id%TYPE,
					  OUT o_auth_id admins.admin_id%TYPE,
					  OUT o_new_token tokens.token%TYPE,
					  OUT o_remote_addr tokens.remote_addr%TYPE,
                                          OUT o_error VARCHAR(50)) AS $$
DECLARE
	l_token_id tokens.token_id%TYPE;
	l_valid_to tokens.valid_to%TYPE;
	l_auth_type VARCHAR(50);
	l_email tokens.email%TYPE;
	l_admin_id tokens.admin_id%TYPE;
BEGIN
	SELECT
		token_id,
		valid_to,
		COALESCE(stores.store_id, admins.admin_id),
		CASE WHEN stores.store_id IS NOT NULL THEN 'store' ELSE 'admin' END,
		tokens.email,
		CASE WHEN stores.store_id IS NOT NULL THEN tokens.admin_id ELSE NULL END
	INTO
		l_token_id,
		l_valid_to,
		o_auth_id,
		l_auth_type,
		l_email,
		l_admin_id
	FROM
		tokens 
	LEFT JOIN admins ON
		admins.admin_id = tokens.admin_id AND
		admins.status = 'active'
	LEFT JOIN stores ON
		stores.store_id = tokens.store_id AND
		stores.status = 'active' AND
		CURRENT_TIMESTAMP BETWEEN stores.date_start AND stores.date_end
	WHERE
		token = i_token;
	
	IF NOT FOUND OR o_auth_id IS NULL THEN
		RAISE EXCEPTION 'ERROR_TOKEN_INVALID';
	END IF;
	
	IF l_valid_to <= CURRENT_TIMESTAMP THEN
		SELECT
			remote_addr
		INTO
			o_remote_addr
		FROM
			tokens
		WHERE
			COALESCE(store_id, admin_id) = o_auth_id
			AND CASE WHEN l_admin_id IS NOT NULL THEN admin_id = l_admin_id ELSE email = l_email END
			AND valid_to > l_valid_to;
		
		IF FOUND THEN
                        o_error = 'ERROR_TOKEN_REPLACED';
                        RETURN;
		ELSE
			RAISE EXCEPTION 'ERROR_TOKEN_OLD';
		END IF;
	END IF;
	
	IF l_auth_type = 'admin' AND i_priv_required IS NOT NULL THEN
		IF NOT EXISTS(SELECT
				*
			FROM
				admin_privs
			WHERE
				admin_id = o_auth_id AND
				priv_name = ANY(i_priv_required)) THEN
			RAISE EXCEPTION 'ERROR_INSUFFICIENT_PRIVS';
		END IF;
	END IF;

	IF l_auth_type = 'store' THEN
		IF (i_store_id_required IS NOT NULL AND o_auth_id != i_store_id_required) THEN
			RAISE EXCEPTION 'ERROR_TOKEN_INVALID';
		END IF;
	END IF;

	o_new_token := generate_token(o_auth_id, i_remote_addr, i_valid_interval, i_info, l_auth_type, l_email, l_admin_id);
END;
$$ LANGUAGE plpgsql;
