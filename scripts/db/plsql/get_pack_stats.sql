CREATE OR REPLACE FUNCTION get_pack_stats (
                                        i_user_id INTEGER,
                                        i_pack_type VARCHAR,
                                        i_date DATE
                                )
RETURNS FLOAT AS $$
DECLARE
        l_schema VARCHAR;
        l_list_ids VARCHAR;
        o_average_visits FLOAT;
BEGIN
        -- Get the list_ids
        SELECT
                array_to_string(array_agg(list_id), ',')
        INTO
                l_list_ids
        FROM
                ads
        WHERE
                category = ANY(
                                CASE
                                        WHEN i_pack_type = 'inmosell' THEN ARRAY[1220]
                                        WHEN i_pack_type = 'inmorent' THEN ARRAY[1240]
                                        ELSE ARRAY[2020,2040]
                                END
                        )
                AND status = 'active'
                AND user_id = COALESCE(i_user_id, user_id)
                AND DATE(list_time) <= i_date;

        IF l_list_ids IS NULL THEN
                RETURN 0.0;
        END IF;

        -- Get the schema where the data should be
        l_schema := 'blocket_' || extract(year from i_date);

        -- Get the visits average
        EXECUTE
                format('SELECT COALESCE(ROUND(AVG(visits)), 0.0) FROM %s.view_%s WHERE list_id = ANY(ARRAY[%s]) AND date = ''%s'' AND visits > 0;',
                        l_schema,
                        TO_CHAR(i_date, 'yyyymm'),
                        l_list_ids,
                        i_date)
        INTO o_average_visits;

        RETURN o_average_visits;
END
$$ LANGUAGE plpgsql;
