CREATE OR REPLACE FUNCTION edit_store(i_store_id stores.store_id%TYPE,
					i_store_name stores.name%TYPE,
					i_store_info_text stores.info_text%TYPE,
					i_store_url stores.url%TYPE,
					i_store_phone stores.phone%TYPE,
					i_store_hide_phone stores.url%TYPE,
					i_store_address stores.address%TYPE,
					i_store_address_number stores.address_number%TYPE,
					i_store_main_image stores.main_image%TYPE,
					i_store_main_image_offset stores.main_image_offset%TYPE,
					i_store_logo_image stores.logo_image%TYPE,
					i_store_region stores.region%TYPE,
					i_store_commune stores.commune%TYPE,
					i_store_website_url stores.website_url%TYPE,
					i_token_id tokens.token_id%TYPE,
					OUT o_store_id stores.store_id%TYPE,
					OUT o_action_id store_actions.action_id%TYPE,
					OUT o_state_id store_action_states.state_id%TYPE,
					OUT o_store_status stores.status%TYPE
					) AS $$
DECLARE
	l_state store_action_states.state%TYPE;
	l_store_url stores.url%TYPE;
BEGIN

	SELECT status INTO o_store_status FROM stores WHERE store_id = i_store_id;

	-- Check url isn't taken on another store
	SELECT url INTO l_store_url FROM stores WHERE url = i_store_url AND store_id != i_store_id;
	IF l_store_url IS NOT NULL THEN
		RAISE EXCEPTION 'FACTORY_TRANSLATE:name:ERROR_STORE_URL_ALREADY_EXIST';
	END IF;

	o_store_id := i_store_id;
	-- Check if store exist
	IF NOT EXISTS (SELECT 1 FROM stores WHERE store_id = i_store_id) THEN
		RAISE EXCEPTION 'FACTORY_TRANSLATE:error:ERROR_STORE_NOT_FOUND';
	END IF;

	-- Get action and state id
	SELECT
		isas.o_action_id,
		isas.o_state_id
	INTO
		o_action_id,
		o_state_id
	FROM
		insert_store_action_state(
					  CAST(o_store_id AS integer),
					  'edit',
					  'reg',
					  i_token_id,
					  NULL
					 ) AS isas;

	-- Update all
	PERFORM insert_store_change(o_store_id, o_action_id, o_state_id, false, 'name', i_store_name);
	PERFORM insert_store_change(o_store_id, o_action_id, o_state_id, false, 'info_text', i_store_info_text);
	PERFORM insert_store_change(o_store_id, o_action_id, o_state_id, false, 'url', i_store_url::text);
	PERFORM insert_store_change(o_store_id, o_action_id, o_state_id, false, 'phone', i_store_phone);
	PERFORM insert_store_change(o_store_id, o_action_id, o_state_id, false, 'hide_phone', i_store_hide_phone);
	PERFORM insert_store_change(o_store_id, o_action_id, o_state_id, false, 'address', i_store_address);
	PERFORM insert_store_change(o_store_id, o_action_id, o_state_id, false, 'address_number', i_store_address_number);
	PERFORM insert_store_change(o_store_id, o_action_id, o_state_id, false, 'main_image', i_store_main_image::text);
	PERFORM insert_store_change(o_store_id, o_action_id, o_state_id, false, 'main_image_offset', i_store_main_image_offset::text);
	PERFORM insert_store_change(o_store_id, o_action_id, o_state_id, false, 'logo_image', i_store_logo_image::text);
	PERFORM insert_store_change(o_store_id, o_action_id, o_state_id, false, 'region', i_store_region::text);
	PERFORM insert_store_change(o_store_id, o_action_id, o_state_id, false, 'commune', i_store_commune::text);
	PERFORM insert_store_change(o_store_id, o_action_id, o_state_id, false, 'website_url', i_store_website_url::text);

	PERFORM apply_store_changes(o_store_id, o_action_id);

	o_state_id := insert_store_state(i_store_id, o_action_id, 'accepted',i_token_id);

	IF o_store_status = 'inactive' THEN
		PERFORM update_store_status(
			CAST(i_store_id AS integer),
			'active'::enum_stores_status,
			i_token_id,
			NULL
		);
		o_store_status = 'active';
	END IF;

END;
$$ LANGUAGE plpgsql;

