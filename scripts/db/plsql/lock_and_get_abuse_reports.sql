CREATE OR REPLACE FUNCTION lock_and_get_abuse_reports( i_report_type abuse_reports.report_type%TYPE,
							i_admin_id abuse_locks.admin_id%TYPE,
							i_interval INTEGER,
							i_skip_ad INTEGER,
							i_ad_id ads.ad_id%TYPE,
							OUT o_report_id abuse_reports.report_id%TYPE,
							OUT o_report_type abuse_reports.report_type%TYPE,
							OUT o_reporter_id abuse_reports.reporter_id%TYPE,
							OUT o_user_id abuse_reports.user_id%TYPE,
							OUT o_email users.email%TYPE,
							OUT o_accepted_reports abuse_reporters.accepted_reports%TYPE,
							OUT o_rejected_reports abuse_reporters.rejected_reports%TYPE,
							OUT o_text abuse_reports.text%TYPE,
							OUT o_priority INTEGER,
							OUT o_list_id ads.list_id%TYPE,
							OUT o_date abuse_reports.date_insert%TYPE ) RETURNS SETOF RECORD AS $$

DECLARE
	l_ad_id INTEGER;
	l_rec RECORD;
BEGIN

	LOCK TABLE abuse_locks;

	IF i_skip_ad = '1' THEN
		SELECT
			ad_id,
			list_id
		INTO
			l_ad_id,
			o_list_id
		FROM
			ads JOIN
			abuse_reports USING (ad_id) LEFT JOIN
			abuse_locks USING (ad_id)
		WHERE
			ads.status = 'active' AND
			abuse_reports.status = 'unsolved' AND
			abuse_reports.report_type = i_report_type AND
			(
				abuse_locks.locked_until IS NULL OR
				abuse_locks.locked_until < CURRENT_TIMESTAMP
			)
			AND ad_id > i_ad_id 
		ORDER BY ad_id
		LIMIT 1;
	ELSE 
		SELECT
			ad_id,
			list_id
		INTO
			l_ad_id,
			o_list_id
		FROM
			ads JOIN
			abuse_reports USING (ad_id) LEFT JOIN
			abuse_locks USING (ad_id)
		WHERE
			ads.status = 'active' AND
			abuse_reports.status = 'unsolved' AND
			abuse_reports.report_type = i_report_type AND
			(
				abuse_locks.locked_until IS NULL OR
				abuse_locks.locked_until < CURRENT_TIMESTAMP OR
				abuse_locks.admin_id = i_admin_id
			)
		ORDER BY ad_id
		LIMIT 1;
	END IF;

	IF l_ad_id IS NULL THEN
		RETURN;
	END IF;

	UPDATE
		abuse_locks
	SET
		locked_until = CURRENT_TIMESTAMP + (INTERVAL '1 SECOND' * i_interval),
		admin_id = i_admin_id
	WHERE
		ad_id = l_ad_id;

	IF NOT FOUND THEN
		INSERT
		INTO
			abuse_locks(ad_id, locked_until,admin_id)
		VALUES
			(l_ad_id, CURRENT_TIMESTAMP + (INTERVAL '1 SECOND' * i_interval),i_admin_id);
	END IF;

	FOR
		l_rec
	IN
		SELECT
			*
		FROM
			get_abuse_reports(o_list_id, i_report_type)
		LOOP
			o_report_id := l_rec.o_report_id;
			o_report_type := l_rec.o_report_type;
			o_reporter_id := l_rec.o_reporter_id;
			o_user_id := l_rec.o_user_id;
			o_email := l_rec.o_email;
			o_accepted_reports := l_rec.o_accepted_reports;
			o_rejected_reports := l_rec.o_rejected_reports;
			o_text := l_rec.o_text;
			o_priority := l_rec.o_priority;
			o_date := l_rec.o_date;
			RETURN NEXT;
		END LOOP;
END
$$ LANGUAGE plpgsql;

