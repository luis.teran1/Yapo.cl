CREATE OR REPLACE FUNCTION autorefuse(i_ad_id ad_actions.ad_id%TYPE,
				      i_action_id ad_actions.action_id%TYPE) RETURNS BOOL AS $$
DECLARE
	l_status	ads.status%TYPE;
	l_action_type	ad_actions.action_type%TYPE;
BEGIN
	SELECT
		action_type,
		ads.status
	INTO
		l_action_type,
		l_status
	FROM
		ads NATURAL JOIN ad_actions
	WHERE
		ads.ad_id = i_ad_id AND
		action_id = i_action_id;

	IF NOT FOUND THEN
		RETURN false;
	END IF;

	-- Autorefuse edit/renew actions when the ad is deleted

	IF l_action_type IN ('edit', 'renew', 'extra_images') AND l_status = 'deleted' THEN
		RETURN true;
	END IF;

	RETURN false;
END;
$$ LANGUAGE plpgsql STABLE;
