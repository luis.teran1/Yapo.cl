CREATE OR REPLACE FUNCTION insert_mail_queue(i_id INTEGER,
					     i_mail_type mail_log.mail_type%TYPE,
					     i_template_name mail_queue.template_name%TYPE,
					     i_s_email mail_queue.sender_email%TYPE,
					     i_s_phone mail_queue.sender_phone%TYPE,
					     i_s_name mail_queue.sender_name%TYPE,
					     i_body mail_queue.body%TYPE,
					     i_remote_addr mail_queue.remote_addr%TYPE,
					     i_buy_prefix varchar,
					     o_mail_queue_id OUT mail_queue.mail_queue_id%TYPE,
					     o_subject OUT mail_queue.subject%TYPE,
					     o_r_email OUT mail_queue.receipient_email%TYPE) AS $$
DECLARE
	l_r_name ads.name%TYPE;
	l_schema varchar;
	l_type varchar(8);
BEGIN
	IF i_mail_type = 'storereply' THEN
		SELECT
			email, info_header, name
		INTO
			o_r_email,
			l_r_name,
			o_subject
		FROM
			stores
		WHERE
			store_id = i_id;
	ELSE
		SELECT 
			coalesce(value, email) as email, 
			ads.name,
			subject,
			type
		INTO
			o_r_email,
			l_r_name,
			o_subject,
			l_type
		FROM 
			ads NATURAL JOIN users 
			LEFT JOIN ad_params on ads.ad_id = ad_params.ad_id and ad_params.name = 'multi_email' 
		WHERE 
			ads.list_id = i_id;
	END IF;
	
	IF NOT FOUND THEN
		RAISE EXCEPTION 'ERROR_NOT_FOUND';
	END IF;
	
	IF l_type = 'buy' OR l_type = 'rent' THEN
		o_subject := i_buy_prefix || ' ' || o_subject;
	END IF;

	l_schema := 'blocket_' || extract(year from CURRENT_TIMESTAMP);
	EXECUTE '
		INSERT INTO ' || quote_ident(l_schema) || '.mail_queue (
			state,
			template_name,
			remote_addr,
			sender_email,
			sender_phone,
			sender_name,
			receipient_email,
			receipient_name,
			subject,
			body,
			list_id
		) VALUES (
			''reg'',
			' || quote_literal_null(i_template_name) || ',
			' || quote_literal_null(host(i_remote_addr::inet)) || ',
			' || quote_literal_null(i_s_email) || ',
			' || quote_literal_null(i_s_phone) || ',
			' || quote_literal_null(i_s_name) || ',
			' || quote_literal_null(o_r_email) || ',
			' || quote_literal_null(l_r_name) || ',
			' || quote_literal_null(o_subject) || ',
			' || quote_literal_null(i_body) || ',
			' || quote_literal_null(i_id) || '
		)';

	INSERT INTO mail_log (
		mail_type,
		email,
		remote_addr,
		list_id
	) VALUES (
		i_mail_type,
		o_r_email,
		i_remote_addr,
		i_id
	);

	o_mail_queue_id := currval('mail_queue_mail_queue_id_seq');
END;
$$ LANGUAGE plpgsql;
