CREATE OR REPLACE FUNCTION update_or_insert_pageviews(i_pageviews integer[], 
		i_code varchar[],
		i_date varchar[]) RETURNS VOID AS $$
DECLARE 
	l_i integer;
	l_date varchar;
	l_code varchar;
	l_pageviews integer;

BEGIN

	IF i_pageviews IS NOT NULL THEN  
		l_i := 1; -- Arrays in postgresql are 1-based
		LOOP 

			l_date := i_date[l_i];
			l_code := i_code[l_i];
			l_pageviews := i_pageviews[l_i];
			EXIT WHEN l_date IS NULL;
			UPDATE
				redir_stats
			SET
				pageviews = pageviews + l_pageviews
			WHERE
				code = l_code AND
				date = l_date::timestamp;
		
			IF NOT FOUND THEN
				INSERT INTO
						redir_stats
						(pageviews,
						 code,
						 date)	
					VALUES
						(l_pageviews,
						 l_code,
						 l_date::timestamp);
			END IF;
			l_i := l_i +1;
		END LOOP;
	END IF;

END;
$$ LANGUAGE plpgsql;
