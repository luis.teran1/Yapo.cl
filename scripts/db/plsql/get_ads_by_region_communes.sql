CREATE OR REPLACE FUNCTION get_ads_by_region_communes(
    i_region_id integer,
    i_status character varying[],
    i_communes_list character varying[])
    RETURNS TABLE(id integer) AS $$

BEGIN

    RETURN QUERY
    SELECT a.ad_id FROM ads a
    INNER JOIN ad_params p ON p.ad_id = a.ad_id AND a.region = i_region_id
        AND NOT a.status = ANY(i_status::enum_ads_status[]) AND p.name = 'communes'
    WHERE p.value = ANY(i_communes_list::varchar[]);

END;
$$ LANGUAGE plpgsql;
