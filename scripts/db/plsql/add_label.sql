CREATE OR REPLACE FUNCTION add_label(
	i_ad_id ads.ad_id%TYPE,
	i_action_id integer,
	i_label_type varchar,
	i_remote_addr tokens.remote_addr%TYPE,
	i_remote_browser varchar,
	i_token_id action_states.token_id%TYPE,
	OUT o_email users.email%TYPE,
	OUT o_label varchar
) AS $$
DECLARE
	l_action_id ad_actions.action_id%TYPE;
	l_state_id action_states.state_id%TYPE;
BEGIN
	SELECT
		email
	INTO
		o_email
	FROM
		ads JOIN users USING (user_id)
	WHERE
		ad_id = i_ad_id;

	IF NOT FOUND THEN
		RAISE EXCEPTION 'ERROR_AD_NOT_FOUND';
	END IF;

	IF EXISTS(SELECT * FROM ad_params WHERE name='label' AND ad_id = i_ad_id)
	THEN
		RAISE EXCEPTION 'ERROR_LABEL_ALREADY_EXISTS_FOR_AD';
	END IF;

	IF i_action_id IS NULL THEN
		-- add ad_action and action_states
		l_action_id := insert_ad_action(i_ad_id, 'label', NULL);
		PERFORM insert_state(i_ad_id, l_action_id, 'reg', 'initial', i_remote_addr, i_token_id);
		l_state_id := insert_state(i_ad_id, l_action_id, 'accepted', 'adminclear', i_remote_addr, i_token_id);
		o_label = i_label_type;
	ELSE
		SELECT state_id, action_id
		INTO l_state_id, l_action_id
		FROM action_states
		WHERE ad_id = i_ad_id AND action_id = i_action_id AND  state = 'accepted';
		IF NOT FOUND THEN
			RAISE EXCEPTION 'FACTORY_TRANSLATE:action_id:ERROR_ACTION_ID_NOT_FOUND';
		END IF;

		SELECT value INTO o_label
		FROM action_params
		WHERE  ad_id = i_ad_id AND action_id = i_action_id and name = 'label_type';
		IF NOT FOUND THEN
			RAISE EXCEPTION 'FACTORY_TRANSLATE:action_id:ERROR_ACTION_PARAM_NOT_FOUND';
		END IF;
	END IF;

	-- add addition of gallery ad_params to ad_changes
	PERFORM insert_ad_change(i_ad_id, l_action_id, l_state_id, true, 'label', o_label);

	-- apply ad_changes
	PERFORM apply_ad_changes(i_ad_id, l_action_id, NULL);
END
$$ LANGUAGE plpgsql;
