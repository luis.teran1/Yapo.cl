CREATE OR REPLACE FUNCTION phone_standarization_rm(i_code VARCHAR,
					i_phone VARCHAR) RETURNS VARCHAR AS $$
DECLARE
	l_number VARCHAR := right(trim(leading '0' FROM regexp_replace(i_phone,'[^0-9]','','g')),9);
	l_rm_reg_ex VARCHAR := '5622[2-9][0-9]{6}';
	l_cel_reg_ex VARCHAR := '569[5-9][0-9]{7}';
BEGIN
	IF (SELECT SUBSTRING(l_number FROM '^' || l_rm_reg_ex || '$')) IS NOT NULL THEN
		RETURN SUBSTR(l_number, 3);
	ELSIF (SELECT SUBSTRING(l_number FROM '^' || SUBSTR(l_rm_reg_ex, 3) || '$')) IS NOT NULL THEN
		RETURN l_number;
	ELSIF (SELECT SUBSTRING(l_number FROM '^' || SUBSTR(l_rm_reg_ex, 4) || '$')) IS NOT NULL THEN
		RETURN '2' || l_number;
	ELSIF (SELECT SUBSTRING(l_number FROM '^' || SUBSTR(l_rm_reg_ex, 5) || '$')) IS NOT NULL THEN
	      RETURN '22' || l_number;
	ELSIF (SELECT SUBSTRING(l_number FROM '^' || l_cel_reg_ex || '$')) IS NOT NULL THEN
	      RETURN SUBSTR(l_number, 3);
	ELSIF (SELECT SUBSTRING(l_number FROM '^' || SUBSTR(l_cel_reg_ex, 3) || '$')) IS NOT NULL THEN
	      RETURN l_number;
	ELSIF (SELECT SUBSTRING(l_number FROM '^' || SUBSTR(l_cel_reg_ex, 4) || '$')) IS NOT NULL THEN
		RETURN '9' || l_number;
	ELSE
		RETURN l_number;
	END IF;
END;
$$ LANGUAGE plpgsql;
