CREATE OR REPLACE FUNCTION insert_ad_action_queue(
	i_ad_id ad_actions.ad_id%TYPE,
	i_action_type ad_actions.action_type%TYPE,
	i_payment_group_id ad_actions.payment_group_id%TYPE,
	i_queue ad_actions.queue%TYPE,
	OUT o_action_id ad_actions.action_id%TYPE) AS $$

DECLARE
	l_max_retry integer;

BEGIN
	-- do a couple of retries in case there is someone else inserting at the same time
	l_max_retry := 5;

	-- insert action (get the action_id to try to avoid clash of action_id)
	WHILE l_max_retry > 0 LOOP
		BEGIN
			INSERT INTO
				ad_actions
				(ad_id,
				 action_id,
				 action_type,
				 queue,
				 payment_group_id)
			SELECT
				 i_ad_id,
				 COALESCE( max(action_id) + 1, 1),
				 i_action_type,
				 i_queue,
				 i_payment_group_id
			FROM
				ad_actions
			WHERE
				ad_id = i_ad_id
			RETURNING action_id INTO o_action_id;
			RETURN;
		EXCEPTION WHEN UNIQUE_VIOLATION THEN
			RAISE NOTICE 'Error trying to insert new ad_action: action_id already exists';
		END;

		l_max_retry := l_max_retry - 1;
	END LOOP;

	RAISE EXCEPTION 'ERROR_INSERTING_AD_ACTION';
END;
$$ LANGUAGE plpgsql;
