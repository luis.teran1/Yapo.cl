CREATE OR REPLACE FUNCTION bid_insert(i_bid_ad_id bid_ads.bid_ad_id%TYPE, 
					i_bid bid_bids.bid%TYPE,
					i_name bid_bids.name%TYPE,
					i_company bid_bids.company%TYPE,
					i_email bid_bids.email%TYPE,
					i_phone bid_bids.phone%TYPE,
					i_ssecnr bid_bids.ssecnr%TYPE,
					i_campaign_expire_time timestamp,
					i_remote_addr bid_bids.remote_addr%TYPE) RETURNS bid_bids.bid_id%TYPE AS $$
DECLARE
	l_max_bid	bid_bids.bid%TYPE;
	l_campaign_id	bid_ads.campaign_id%TYPE;
BEGIN
	IF CURRENT_TIMESTAMP > i_campaign_expire_time THEN
		RAISE EXCEPTION 'FACTORY_TRANSLATE:campaign_id:ERROR_BID_CAMPAIGN_EXPIRED';
	END IF;

	LOCK TABLE bid_bids IN EXCLUSIVE MODE;

	SELECT
		campaign_id
	INTO
		l_campaign_id
	FROM
		bid_ads JOIN
		bid_bids USING (bid_ad_id)
	WHERE
		bid_ads.bid_ad_id = i_bid_ad_id;

	SELECT
		MAX(bid)
	INTO
		l_max_bid
	FROM
		bid_bids
		JOIN bid_ads USING(bid_ad_id)
	WHERE
		bid_bids.bid_ad_id = i_bid_ad_id AND
		bid_ads.campaign_id = l_campaign_id AND
		status = 'active';

	IF NOT FOUND THEN
		l_max_bid = 0;
	END IF;

	IF i_bid <= l_max_bid THEN
		RAISE EXCEPTION 'FACTORY_TRANSLATE:bid:ERROR_BID_TOO_SMALL';
	END IF;

	INSERT INTO
		bid_bids
		(
			bid_ad_id,
			bid,
			bid_time,
			name,
			company,
			email,
			phone,
			ssecnr,
			status,
			remote_addr
		)
	VALUES
		(
		 	i_bid_ad_id,
			i_bid,
			CURRENT_TIMESTAMP,
			i_name,
			i_company,
			i_email,
			i_phone,
			i_ssecnr,
			'inactive',
			i_remote_addr
		);
	
	RETURN CURRVAL('bid_bids_bid_id_seq');
END;
$$ LANGUAGE plpgsql;
