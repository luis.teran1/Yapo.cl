CREATE OR REPLACE FUNCTION clean_ad_from_archive (
		) RETURNS void AS $$
DECLARE
	l_token_id tokens.token_id%TYPE;
BEGIN
	RAISE NOTICE 'ARCHIVE: Deleting from ad_images_digests';
	DELETE FROM
		ad_images_digests
	USING
		archive_ad_ids
	WHERE
		ad_images_digests.ad_id = archive_ad_ids.ad_id;

	RAISE NOTICE 'ARCHIVE: Deleting from ad_changes';
	DELETE FROM
		ad_changes
	USING
		archive_ad_ids
	WHERE
		ad_changes.ad_id = archive_ad_ids.ad_id;

	RAISE NOTICE 'ARCHIVE: Deleting from ad_image_changes';
	DELETE FROM
		ad_image_changes
	USING
		archive_ad_ids
	WHERE
		ad_image_changes.ad_id = archive_ad_ids.ad_id;

	RAISE NOTICE 'ARCHIVE: Deleting from ad_media_changes';
	DELETE FROM
		ad_media_changes
	USING
		archive_ad_ids
	WHERE
		ad_media_changes.ad_id = archive_ad_ids.ad_id;

	RAISE NOTICE 'ARCHIVE: Deleting from state_params';
	DELETE FROM
		state_params
	USING
		archive_ad_ids
	WHERE
		state_params.ad_id = archive_ad_ids.ad_id;

	RAISE NOTICE 'ARCHIVE: Deleting from action_states';
	FOR
		l_token_id
	IN
		DELETE FROM
			action_states
		USING
			archive_ad_ids
		WHERE
			action_states.ad_id = archive_ad_ids.ad_id
		RETURNING
			token_id
	LOOP
		PERFORM clean_token(l_token_id);
	END LOOP;

	RAISE NOTICE 'ARCHIVE: Deleting from action_params';
	DELETE FROM
		action_params
	USING
		archive_ad_ids
	WHERE
		action_params.ad_id = archive_ad_ids.ad_id;

	RAISE NOTICE 'ARCHIVE: Deleting from voucher_states';
	FOR
		l_token_id
	IN
		DELETE FROM
			voucher_states
		USING
			voucher_actions
			JOIN ad_actions USING (payment_group_id)
			JOIN archive_ad_ids USING (ad_id)
		WHERE
			voucher_states.voucher_id = voucher_actions.voucher_id
			AND voucher_states.voucher_action_id = voucher_actions.voucher_action_id
		RETURNING
			token_id
	LOOP
		PERFORM clean_token(l_token_id);
	END LOOP;

	RAISE NOTICE 'ARCHIVE: Deleting from voucher_actions';
	DELETE FROM
		voucher_actions
	USING
		ad_actions
		JOIN archive_ad_ids USING (ad_id)
	WHERE
		voucher_actions.payment_group_id = ad_actions.payment_group_id;
	
	RAISE NOTICE 'ARCHIVE: Deleting from ad_actions';
	DELETE FROM
		ad_actions
	USING
		archive_ad_ids
	WHERE
		ad_actions.ad_id = archive_ad_ids.ad_id;

	RAISE NOTICE 'ARCHIVE: Deleting from purchase_detail';
	DELETE FROM
		purchase_detail
	USING
		archive_purchase_ids
	WHERE
		purchase_detail.purchase_id = archive_purchase_ids.purchase_id;
				
	RAISE NOTICE 'ARCHIVE: Deleting from purchase_states';
	DELETE FROM
		purchase_states
	USING
		archive_purchase_ids
	WHERE
		purchase_states.purchase_id = archive_purchase_ids.purchase_id;		
				
	RAISE NOTICE 'ARCHIVE: Deleting from purchase';
	DELETE FROM
		purchase
	USING
		archive_purchase_ids
	WHERE
		purchase.purchase_id = archive_purchase_ids.purchase_id;
				
	PERFORM clean_payment_group_from_archive();

	RAISE NOTICE 'ARCHIVE: Deleting from notices';
	FOR
		l_token_id
	IN
		DELETE FROM
			notices
		USING
			archive_ad_ids
		WHERE
			notices.ad_id = archive_ad_ids.ad_id
		RETURNING
			token_id
	LOOP
		PERFORM clean_token(l_token_id);
	END LOOP;

	RAISE NOTICE 'ARCHIVE: Deleting from ad_params';
	DELETE FROM
		ad_params
	USING
		archive_ad_ids
	WHERE
		ad_params.ad_id = archive_ad_ids.ad_id;

	RAISE NOTICE 'ARCHIVE: Deleting from ads';
	DELETE FROM
		ads
	USING
		archive_ad_ids
	WHERE
		ads.ad_id = archive_ad_ids.ad_id;
END;
$$ LANGUAGE plpgsql;
