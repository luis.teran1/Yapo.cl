CREATE OR REPLACE FUNCTION insert_phone_update(i_ad_id INTEGER,
						i_phone VARCHAR,
						i_old_phone VARCHAR) RETURNS INTEGER AS $$
DECLARE

	l_action_id ad_actions.action_id%TYPE;
	l_state_id action_states.state_id%TYPE;

BEGIN
	IF i_phone IS NULL OR i_phone = i_old_phone THEN
		RETURN 0;
	END IF;
	l_action_id := insert_ad_action(i_ad_id, 'phone_update', NULL);
	l_state_id := insert_state(i_ad_id, l_action_id, 'accepted', 'accept', '127.0.0.1', NULL);
	PERFORM
		insert_ad_change
		(
			i_ad_id,
			l_action_id,
			l_state_id,
			FALSE,
			'phone',
			i_phone
		);
	PERFORM apply_ad_changes(i_ad_id, l_action_id, NULL);
	RETURN 1;
END;
$$ LANGUAGE plpgsql;
