CREATE OR REPLACE FUNCTION generate_token(i_auth_id INTEGER,
					  i_remote_addr tokens.remote_addr%TYPE,
					  i_valid_interval INTERVAL,
					  i_info tokens.info%TYPE,
					  i_auth_type TEXT,
					  i_email tokens.email%TYPE,
					  i_admin_id tokens.admin_id%TYPE,
					  OUT o_token tokens.token%TYPE) AS $$
BEGIN

	-- Genereate 104 random bits (double precision is 52 bits).
	o_token := 'X' || TO_HEX(CAST(EXTRACT(EPOCH FROM NOW()) AS INT)) || TO_HEX(CAST(RANDOM() * 9223372036854775807 AS BIGINT)) || TO_HEX(CAST(RANDOM() * 9223372036854775807 AS BIGINT));

	IF i_auth_type = 'admin' THEN
	
		UPDATE
			tokens
		SET	
			valid_to = CURRENT_TIMESTAMP
		WHERE
			admin_id = i_auth_id
			AND store_id IS NULL
			AND valid_to > CURRENT_TIMESTAMP;
	
	
		INSERT INTO
			tokens (token,
				admin_id,
				created_at,
				valid_to,
				remote_addr,
				info)
		VALUES
			(o_token,
			 i_auth_id,
			 CURRENT_TIMESTAMP,
			 CURRENT_TIMESTAMP + i_valid_interval,
			 i_remote_addr,
			 i_info);

	ELSIF i_auth_type = 'store' THEN
		UPDATE
			tokens
		SET	
			valid_to = CURRENT_TIMESTAMP
		WHERE
			store_id = i_auth_id
			AND CASE WHEN i_admin_id IS NOT NULL THEN admin_id = i_admin_id ELSE email = i_email END
			AND valid_to > CURRENT_TIMESTAMP;
	
	
		INSERT INTO
			tokens (token,
				store_id,
				created_at,
				valid_to,
				remote_addr,
				info,
				email,
				admin_id)
		VALUES
			(o_token,
			 i_auth_id,
			 CURRENT_TIMESTAMP,
			 CURRENT_TIMESTAMP + i_valid_interval,
			 i_remote_addr,
			 i_info,
			 i_email,
			 i_admin_id);

	ELSE
		RAISE EXCEPTION 'ERROR_AUTH_TYPE %', i_auth_type ;
	END IF;
END;
$$ LANGUAGE plpgsql;
