CREATE OR REPLACE FUNCTION apply_ad_changes(i_ad_id ad_actions.ad_id%TYPE,
					    i_action_id ad_actions.action_id%TYPE,
					    i_image_state_id action_states.state_id%TYPE) RETURNS VOID AS $$

DECLARE
	l_name ads.name%TYPE;
	l_phone ads.phone%TYPE;
	l_region ads.region%TYPE;
	l_city ads.city%TYPE;
	l_type ads.type%TYPE;
	l_category ads.category%TYPE;
	l_passwd ads.passwd%TYPE;
	l_subject ads.subject%TYPE;
	l_body ads.body%TYPE;
	l_price ads.price%TYPE;
	l_infopage ads.infopage%TYPE;
	l_infopage_title ads.infopage_title%TYPE; 
	l_phone_hidden ads.phone_hidden%TYPE;
	l_company_ad ads.company_ad%TYPE;
	l_user_id ads.user_id%TYPE;
	l_store_id ads.store_id%TYPE;
	l_salted_passwd ads.salted_passwd%TYPE;

	l_ad_changes_cursor CURSOR (i_ad_id ads.ad_id%TYPE, 
				    i_action_id ad_actions.action_id%TYPE) IS 
		SELECT
			is_param,
			column_name,
			new_value
		FROM
			ad_changes
		WHERE
			ad_id = i_ad_id AND
			action_id = i_action_id AND
			(state_id, column_name) IN (
				SELECT
					MAX(state_id),
					column_name
				FROM
					ad_changes
				WHERE
					ad_id = i_ad_id AND
					action_id = i_action_id
				GROUP BY
					column_name)
		ORDER BY
			state_id;

	l_is_param ad_changes.is_param%TYPE;
	l_column_name ad_changes.column_name%TYPE;
	l_new_value ad_changes.new_value%TYPE;
BEGIN
-- XXX What to do if old_value is wrong?
	SELECT 
		name,
		phone,
		region,
		city,
		type,
		category,
		passwd,
		subject,
		body,
		price,
		infopage,
		infopage_title, 
		phone_hidden,
		company_ad,
		user_id,
		store_id,
		salted_passwd
	INTO
		l_name,
		l_phone,
		l_region,
		l_city,
		l_type,
		l_category,
		l_passwd,
		l_subject,
		l_body,
		l_price,
		l_infopage,
		l_infopage_title, 
		l_phone_hidden,
		l_company_ad,
		l_user_id,
		l_store_id,
		l_salted_passwd
	FROM
		ads
	WHERE
		ad_id = i_ad_id;
	
	IF NOT FOUND THEN
		RAISE EXCEPTION 'ERROR_AD_NOT_FOUND';
	END IF;
	
	OPEN l_ad_changes_cursor(i_ad_id, i_action_id);
	LOOP
		FETCH l_ad_changes_cursor INTO l_is_param, l_column_name, l_new_value;
		EXIT WHEN NOT FOUND;
		IF NOT l_is_param THEN
			-- XXX column names, use EXECUTE?
			IF l_column_name = 'name' THEN
				l_name := l_new_value;
			ELSIF l_column_name = 'phone' THEN
				l_phone := l_new_value;
			ELSIF l_column_name = 'region' THEN
				l_region := l_new_value;
			ELSIF l_column_name = 'city' THEN
				l_city := l_new_value;
			ELSIF l_column_name = 'type' THEN
				l_type := l_new_value;
			ELSIF l_column_name = 'category' THEN
				l_category := l_new_value;
			ELSIF l_column_name = 'passwd' THEN
				l_passwd := l_new_value;
			ELSIF l_column_name = 'subject' THEN
				l_subject := l_new_value;
			ELSIF l_column_name = 'body' THEN
				l_body := l_new_value;
			ELSIF l_column_name = 'price' THEN
				l_price := l_new_value;
			ELSIF l_column_name = 'infopage' THEN
				l_infopage := l_new_value;
			ELSIF l_column_name = 'infopage_title' THEN
				l_infopage_title := l_new_value;
			ELSIF l_column_name = 'phone_hidden' THEN
				l_phone_hidden := l_new_value;
			ELSIF l_column_name = 'company_ad' THEN
				l_company_ad := l_new_value;
			ELSIF l_column_name = 'user_id' THEN
				l_user_id := l_new_value;
			ELSIF l_column_name = 'store_id' THEN
				l_store_id := l_new_value;
			ELSIF l_column_name = 'salted_passwd' THEN
				l_salted_passwd := l_new_value;
			END IF;
		ELSE -- is_param
			IF l_new_value IS NULL OR l_new_value = '' THEN -- param removed
				DELETE FROM
					ad_params
				WHERE
					ad_id = i_ad_id AND
					name = l_column_name::enum_ad_params_name;
			ELSE 
				UPDATE
					ad_params
				SET
					value = l_new_value
				WHERE
					ad_id = i_ad_id AND
					name = l_column_name::enum_ad_params_name;
				IF NOT FOUND THEN
					INSERT INTO
						ad_params
						       (ad_id,
							name,
							value)
					VALUES
					       (i_ad_id,
						l_column_name::enum_ad_params_name,
						l_new_value);
				END IF;
			END IF;
		END IF;
	END LOOP;
	CLOSE l_ad_changes_cursor;
	
	UPDATE
		ads
	SET
		name = l_name,
		phone = l_phone,
		region = l_region,
		city = l_city,
		type = l_type,
		category = l_category,
		passwd = l_passwd,
		subject = l_subject,
		body = l_body,
		price = l_price,
		infopage = l_infopage,
		infopage_title = l_infopage_title,
		phone_hidden = l_phone_hidden,
		company_ad = l_company_ad,
		user_id = l_user_id,
		store_id = l_store_id,
		salted_passwd = l_salted_passwd
	WHERE
		ad_id = i_ad_id;
	
	IF i_image_state_id IS NOT NULL THEN
		-- Unlink images
		UPDATE
			ad_media	
		SET
			ad_id = NULL
		WHERE	
			COALESCE(media_type, 'image') = 'image' AND
			ad_id = i_ad_id;

		-- Compat. Move ad_image_changes to ad_media
		INSERT INTO 
			ad_media
		SELECT
			TRIM(trailing '.jpg' FROM name)::bigint	AS ad_media_id,
			ad_id,
			seq_no,
			CURRENT_TIMESTAMP AS upload_time,
			'image' AS media_type
		FROM
			ad_image_changes
		WHERE
			ad_id = i_ad_id AND
			action_id = i_action_id AND
			state_id = i_image_state_id AND
			name IS NOT NULL AND
			NOT EXISTS (
				SELECT
					ad_media_id
				FROM
					ad_media
				WHERE
					COALESCE(media_type, 'image') = 'image' AND
					ad_media_id = TRIM(trailing '.jpg' FROM ad_image_changes.name)::bigint
			);

		-- Link new images
		UPDATE
			ad_media
		SET
			ad_id = i_ad_id,
			seq_no = ad_image_changes.seq_no
		FROM
			ad_image_changes
		WHERE
			ad_image_changes.ad_id = i_ad_id AND
			ad_image_changes.action_id = i_action_id AND
			ad_image_changes.state_id = i_image_state_id AND
			ad_image_changes.name IS NOT NULL AND
			ad_media.ad_media_id = TRIM(trailing '.jpg' from ad_image_changes.name)::bigint;
	
		UPDATE
			ad_media
		SET
			ad_id = NULL
		WHERE
			ad_id = i_ad_id AND media_type = 'video';
		
		UPDATE
			ad_media
		SET
			ad_id = i_ad_id
		FROM
			ad_media_changes
		WHERE
			ad_media_changes.ad_id = i_ad_id AND
			ad_media_changes.action_id = i_action_id AND
			ad_media_changes.state_id = i_image_state_id AND
			ad_media.ad_media_id = ad_media_changes.ad_media_id AND
			ad_media.media_type = 'video'
			
		;
	END IF;			
END
$$ LANGUAGE plpgsql;
