CREATE OR REPLACE FUNCTION get_next_unused_media_id(i_image_dir ad_media.ad_media_id%TYPE, OUT o_media_id ad_media.ad_media_id%TYPE) AS $$

DECLARE
	l_retries_left integer;

BEGIN
	l_retries_left := 1000;

	o_media_id := NULL;

	WHILE l_retries_left > 0 AND o_media_id IS NULL LOOP

		-- get next seed
		o_media_id := NEXTVAL('next_image_id');

		-- get "random" image_id
		o_media_id := (o_media_id * 9608523319) % 100000000;

		-- add image_dir
		o_media_id := i_image_dir * 100000000 + o_media_id;

		-- check availability
		IF EXISTS (SELECT ad_media_id FROM ad_media WHERE ad_media_id = o_media_id) THEN
			o_media_id := NULL;
		END IF;

		l_retries_left := l_retries_left - 1;

	END LOOP;

END
$$ LANGUAGE plpgsql;
