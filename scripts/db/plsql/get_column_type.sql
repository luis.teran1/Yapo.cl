CREATE OR REPLACE FUNCTION get_column_type (
		i_schema text,
		i_table text,
		i_column text
		) RETURNS text AS $$
SELECT
	pg_catalog.format_type(a.atttypid, a.atttypmod)
FROM
	pg_catalog.pg_attribute a
	JOIN pg_class t ON
		a.attrelid = t.oid
	JOIN pg_namespace n ON
		n.oid = t.relnamespace
WHERE
	NOT a.attisdropped
	AND a.attname = $3
	AND t.relname = $2
	AND n.nspname = $1;
$$ STABLE LANGUAGE sql;
