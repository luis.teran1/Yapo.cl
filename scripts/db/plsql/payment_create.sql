CREATE OR REPLACE FUNCTION payment_create(i_code_type varchar,
		i_status payment_groups.status%TYPE,
		i_pay_type pay_log.pay_type%TYPE,
		i_payment_types varchar[],
		i_payment_prices integer[],
		i_ref_types enum_pay_log_references_ref_type[],
		i_references varchar[],
		i_remote_addr action_states.remote_addr%TYPE,
		i_token_id tokens.token_id%TYPE,
		i_parent_payment_group_id payment_groups.parent_payment_group_id%TYPE,
		OUT o_payment_group_id payment_groups.payment_group_id%TYPE,
		OUT o_paycode payment_groups.code%TYPE) AS $$
DECLARE
	l_i integer;
BEGIN
	IF i_code_type IS NOT NULL THEN
		o_paycode := get_ad_code(i_code_type);
	ELSE
		o_paycode := NULL;
	END IF;

	-- Create new payment
	INSERT INTO payment_groups (
		code,
		status,
		parent_payment_group_id
	) VALUES (
		o_paycode,
		i_status,
		i_parent_payment_group_id
	) RETURNING
		payment_group_id
	INTO
		o_payment_group_id;

	FOR l_i IN array_lower(i_payment_types, 1) .. array_upper(i_payment_types, 1) LOOP
		IF i_payment_prices[l_i] > 0 THEN
			INSERT INTO payments (
				payment_group_id,
				payment_type,
				pay_amount -- XXX discount should be inserted here maybe?
			) VALUES (
				o_payment_group_id,
				i_payment_types[l_i],
				i_payment_prices[l_i]
			);
		END IF;
	END LOOP;

	PERFORM insert_pay_log(i_pay_type, o_paycode::varchar, 0, NULL, i_ref_types, i_references,
			       i_remote_addr, i_token_id, o_payment_group_id, 'SAVE');
END;
$$ LANGUAGE plpgsql;
