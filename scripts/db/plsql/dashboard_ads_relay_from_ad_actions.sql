CREATE OR REPLACE FUNCTION dashboard_ads_relay_from_ad_actions(
) RETURNS TRIGGER AS $$
DECLARE
	l_user_id           integer;
	l_list_id           integer;
	l_list_time         timestamp;
	l_company_ad        bool;
	l_type              enum_ads_type;
	l_category          integer;
	l_ad_status         enum_dashboard_status;
	l_subject           varchar;
	l_body              text;
	l_price             bigint;
	l_ad_media_id       bigint;
	l_image_count       integer;
	l_daily_bump        varchar;
	l_weekly_bump       varchar;
	l_label             varchar;
	l_gallery_date      varchar;
	l_ad_pack_status    varchar;
	l_ad_params_list    text;
	l_changed_subject   varchar;
	l_changed_category  varchar;
	l_changed_price     varchar;
	l_disabled_edit     varchar;
	l_changed_image_count integer;
BEGIN
	IF TG_OP = 'UPDATE' OR TG_OP = 'INSERT' THEN

		SELECT
			user_id,list_id,list_time,company_ad,type,category,status,subject,price,REPLACE(body, E'\n', '<br>') as body
		INTO
			l_user_id,l_list_id,l_list_time,l_company_ad,l_type,l_category,l_ad_status,l_subject,l_price,l_body
		FROM ads WHERE ad_id = NEW.ad_id;

		SELECT COUNT(1) INTO l_image_count FROM ad_media WHERE ad_media.ad_id = NEW.ad_id;

		-- When the ad is inserted and cleared is inserted on dashboard_ads
		IF NEW.action_type = 'new' AND NEW.state IN ('unpaid', 'pending_review') THEN
			-- Insert or update ad into new table
			UPDATE dashboard_ads SET
				list_id = l_list_id,
				list_time = l_list_time,
				company_ad = l_company_ad,
				type = l_type,
				category = l_category,
				ad_status = l_ad_status,
				subject = l_subject,
				body = l_body,
				price = l_price,
				image_count = l_image_count,
				paid = NEW.state != 'unpaid'
			WHERE ad_id = NEW.ad_id;

			IF NOT FOUND THEN
				INSERT INTO dashboard_ads (ad_id,user_id,list_id,list_time,company_ad,type,category,ad_status,subject,body,price,image_count,paid)
				VALUES (NEW.ad_id,l_user_id,l_list_id,l_list_time,l_company_ad,l_type,l_category,l_ad_status,l_subject,l_body,l_price,l_image_count,NEW.state != 'unpaid');
			END IF;

		ELSIF NEW.action_type = 'new' AND NEW.state = 'accepted' THEN
			l_ad_status := 'active';
			IF EXISTS ( SELECT * FROM ad_params WHERE ad_id = NEW.ad_id AND name = 'pack_status' AND value = 'disabled') THEN
				l_ad_status := 'disabled';
			END IF;
			UPDATE dashboard_ads SET
				list_id = l_list_id,
				list_time = l_list_time,
				category = l_category,
				ad_status = l_ad_status,
				image_count = l_image_count
			WHERE ad_id = NEW.ad_id;
		END IF;

		-- Get the changed values only for Subject, Category and Price
		IF NEW.action_type in ('editrefused') AND NEW.state = 'pending_review' THEN
			SELECT new_value INTO l_changed_subject FROM ad_changes WHERE ad_id = NEW.ad_id AND action_id = NEW.action_id AND column_name = 'subject';
			IF NOT FOUND THEN
				l_changed_subject := l_subject;
			END IF;

			SELECT new_value INTO l_changed_category FROM ad_changes WHERE ad_id = NEW.ad_id AND action_id = NEW.action_id AND column_name = 'category';
			IF NOT FOUND THEN
				l_changed_category := l_category::varchar;
			END IF;

			SELECT new_value INTO l_changed_price FROM ad_changes WHERE ad_id = NEW.ad_id AND action_id = NEW.action_id AND column_name = 'price';
			IF FOUND THEN
				l_changed_price := nullif(l_changed_price,'');
			ELSE
				l_changed_price := l_price::varchar;
			END IF;

			SELECT COUNT(1) INTO l_changed_image_count FROM ad_image_changes WHERE ad_id = NEW.ad_id AND name IS NOT NULL AND name != '' AND action_id = NEW.action_id;
			IF NOT FOUND THEN
				l_changed_image_count := l_image_count;
			END IF;

		END IF;

		-- Update for temporary changes when the user edit
		IF NEW.action_type = 'edit' AND NEW.state = 'pending_review' THEN
			l_disabled_edit := 'review-edit';
			IF l_ad_status = 'disabled' THEN
				l_disabled_edit := 'review-edit-disabled';
			END IF;

			UPDATE dashboard_ads SET
				ad_status = l_disabled_edit::enum_dashboard_status
			WHERE ad_id = NEW.ad_id;

		END IF;

		IF NEW.action_type = 'editrefused' AND NEW.state = 'pending_review' THEN
			UPDATE dashboard_ads SET
				subject = l_changed_subject,
				category = l_changed_category::integer,
				price = l_changed_price::bigint,
				image_count = l_changed_image_count,
				ad_status = 'editrefused'
			WHERE ad_id = NEW.ad_id;
		END IF;

		-- If the action is refused restore the original values
		IF NEW.action_type = 'edit' AND NEW.state in ('refused') THEN
			UPDATE dashboard_ads SET
				subject = l_subject,
				category = l_category,
				price = l_price,
				image_count = l_image_count,
				ad_status = l_ad_status
			WHERE ad_id = NEW.ad_id;
		END IF;

		IF NEW.action_type = 'editrefused' AND NEW.state in ('refused') THEN
			UPDATE dashboard_ads SET
				subject = l_subject,
				category = l_category,
				price = l_price,
				image_count = l_image_count,
				ad_status = l_ad_status
			WHERE ad_id = NEW.ad_id;
		END IF;

		IF NEW.action_type = 'status_change' AND NEW.state = 'accepted' AND l_ad_status = 'active' THEN
			SELECT
				CASE
					WHEN new_value = 'deactivated' THEN '0'
					WHEN new_value = 'active' THEN '1'
					WHEN new_value = 'disabled' THEN '0'
				END
			INTO l_ad_pack_status
			FROM ad_changes
			WHERE ad_id = NEW.ad_id AND column_name = 'pack_status'
			ORDER BY action_id DESC
			LIMIT 1;

			IF NOT FOUND THEN
				l_ad_status = 'admin_show';
			END IF;
			IF l_ad_pack_status = '1' THEN
				l_ad_status = 'pack_show';
			END IF;

			UPDATE dashboard_ads SET
				ad_status = l_ad_status
			WHERE ad_id = NEW.ad_id;

		END IF;
	END IF;

	RETURN NULL;
END;
$$ LANGUAGE plpgsql;
