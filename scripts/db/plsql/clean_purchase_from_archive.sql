CREATE OR REPLACE FUNCTION clean_purchase_from_archive() RETURNS void AS $$
DECLARE
        l_token_id tokens.token_id%TYPE;
BEGIN
        RAISE NOTICE 'ARCHIVE: Unlink payment_group_id parent by children';
        UPDATE
                payment_groups
        SET
                parent_payment_group_id = NULL
        FROM
                archive_payment_groups_clean
        WHERE
                payment_groups.parent_payment_group_id IS NOT NULL
        AND
                payment_groups.parent_payment_group_id = archive_payment_groups_clean.payment_group_id;

        RAISE NOTICE 'ARCHIVE: Deleting from purchase_detail';
        DELETE FROM
                purchase_detail
        USING
                archive_purchase_ids
        WHERE
                purchase_detail.purchase_id = archive_purchase_ids.purchase_id;

        RAISE NOTICE 'ARCHIVE: Deleting from purchase_states';
        DELETE FROM
                purchase_states
        USING
                archive_purchase_ids
        WHERE
                purchase_states.purchase_id = archive_purchase_ids.purchase_id;

        RAISE NOTICE 'ARCHIVE: Deleting from purchase';
        DELETE FROM
                purchase
        USING
                archive_purchase_ids
        WHERE
                purchase.purchase_id = archive_purchase_ids.purchase_id;

        PERFORM clean_payment_group_from_archive();

END;
$$ LANGUAGE plpgsql;
