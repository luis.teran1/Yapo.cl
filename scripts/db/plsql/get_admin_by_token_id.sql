CREATE OR REPLACE FUNCTION get_admin_by_token_id(i_token_id tokens.token_id%TYPE) RETURNS admins.admin_id%TYPE AS $$
DECLARE
	l_admin_id admins.admin_id%TYPE;
BEGIN
	SELECT
		admin_id
	INTO
		l_admin_id
	FROM
		tokens
	WHERE
		token_id = i_token_id;
	RETURN l_admin_id;
END
$$ LANGUAGE plpgsql;
