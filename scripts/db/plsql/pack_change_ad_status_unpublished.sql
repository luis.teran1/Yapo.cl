CREATE OR REPLACE FUNCTION pack_change_ad_status_unpublished(
	i_account_id accounts.account_id%TYPE,
	i_ad_id ads.ad_id%TYPE,
	i_new_status varchar,
	i_remote_addr varchar,
	i_pack_conf varchar[][],
	OUT o_available_slots integer
) AS $$
DECLARE
	l_pack_id packs.pack_id%TYPE;
	l_ad_status ads.status%TYPE;
	l_ad_category ads.category%TYPE;
	l_ad_user_id ads.user_id%TYPE;
	l_user_id accounts.user_id%TYPE;
	l_is_pro_for varchar;
	l_pack_type varchar;
	l_pack_categories integer[][];
BEGIN
	SELECT
		user_id
	INTO
		l_user_id
	FROM
		accounts
	WHERE
		account_id = i_account_id;
	IF NOT FOUND THEN
		RAISE EXCEPTION 'FACTORY_TRANSLATE:error:ERROR_ACCOUNT_NOT_FOUND';
	END IF;

	SELECT
		status, category, user_id
	INTO
		l_ad_status, l_ad_category, l_ad_user_id
	FROM
		ads
	WHERE
		ad_id = i_ad_id;

	IF NOT FOUND THEN
		RAISE EXCEPTION 'FACTORY_TRANSLATE:error:ERROR_AD_NOT_FOUND';
	END IF;

	IF l_ad_user_id != l_user_id THEN
		RAISE EXCEPTION 'FACTORY_TRANSLATE:error:ERROR_AD_NOT_BELONG_ACCOUNT';
	END IF;

	l_pack_type := find_pack_type(l_ad_category, i_pack_conf);
	IF l_pack_type is NULL THEN
		RAISE EXCEPTION 'FACTORY_TRANSLATE:error:ERROR_CATEGORY_NOT_CONFIGURED_ON_PACK';
	END IF;

	l_pack_categories := find_pack_categories(l_pack_type, i_pack_conf);

	SELECT
		value
	INTO
		l_is_pro_for
	FROM
		account_params
	WHERE
		account_id = i_account_id AND name = 'is_pro_for';

	IF
		l_is_pro_for IS NULL
		OR NOT (string_to_array(l_is_pro_for,',')::integer[] && l_pack_categories)
	THEN
		RAISE EXCEPTION 'FACTORY_TRANSLATE:error:ERROR_USER_NOT_PRO_FOR_AD_CATEGORY';
	END IF;

	IF i_new_status = 'active' THEN
		SELECT
			pack_id
		INTO
			l_pack_id
		FROM
			packs
		WHERE
			account_id = i_account_id
			AND status = 'active'
			AND slots - used > 0
		ORDER BY
			date_start ASC
		LIMIT 1 FOR UPDATE;

		IF NOT FOUND THEN
			RAISE EXCEPTION 'FACTORY_TRANSLATE:error:ERROR_NO_SLOTS_AVAILABLE';
		ELSE
			IF l_ad_status = 'disabled' THEN
				PERFORM ad_status_change(i_ad_id, i_new_status::enum_ads_status, i_remote_addr, null);
				UPDATE
					packs
				SET
					used = used + 1
				WHERE
					pack_id = l_pack_id;
			ELSE
				RAISE EXCEPTION 'FACTORY_TRANSLATE:error:ERROR_AD_INVALID_STATUS_TRANSITION';
			END IF;
		END IF;
	ELSE -- here new status is disabled
		SELECT
			pack_id
		INTO
			l_pack_id
		FROM
			packs
		WHERE
			account_id = i_account_id
			AND status = 'active'
			AND used > 0
		ORDER BY
			date_start DESC
		LIMIT 1 FOR UPDATE;

		IF NOT FOUND THEN
			RAISE EXCEPTION 'FACTORY_TRANSLATE:error:ERROR_NO_SLOTS_USED';
		ELSE
			IF l_ad_status = 'active' OR l_ad_status = 'deactivated' THEN
				PERFORM ad_status_change(i_ad_id, i_new_status::enum_ads_status, i_remote_addr, null);
				UPDATE
					packs
				SET
					used = used - 1
				WHERE
					pack_id = l_pack_id;
			ELSE
				RAISE EXCEPTION 'FACTORY_TRANSLATE:error:ERROR_AD_INVALID_STATUS_TRANSITION';
			END IF;
		END IF;
	END IF;

	SELECT COALESCE(value::integer, 0)
	INTO o_available_slots
	FROM account_params
	WHERE account_id = i_account_id and name = 'available_slots';

END;
$$ LANGUAGE plpgsql;


