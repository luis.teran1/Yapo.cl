CREATE OR REPLACE FUNCTION dashboard_ads_relay_from_ad_changes(
) RETURNS TRIGGER AS $$
BEGIN
	--Code for premium services
	IF (TG_OP = 'UPDATE' OR TG_OP = 'INSERT') AND NEW.column_name = 'pack_status'
		AND NOT EXISTS (select * from ad_params where ad_id = NEW.ad_id and name = 'inserting_fee')
	THEN
		UPDATE dashboard_ads
		SET ad_pack_status = CASE WHEN NEW.new_value = 'active' THEN 1 ELSE 0 END
		WHERE ad_id = NEW.ad_id;
	END IF;
	RETURN NULL;
END;
$$ LANGUAGE plpgsql;
