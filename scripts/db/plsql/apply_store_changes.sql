CREATE OR REPLACE FUNCTION apply_store_changes(i_store_id store_actions.store_id%TYPE,
					       i_action_id store_actions.action_id%TYPE
					       ) RETURNS VOID AS $$

DECLARE
	l_name               stores.name%TYPE;
	l_phone              stores.phone%TYPE;
	l_address            stores.address%TYPE;
	l_info_text          stores.info_text%TYPE;
	l_status             stores.status%TYPE;
	l_date_start         stores.date_start%TYPE;
	l_date_end           stores.date_end%TYPE;
	l_region             stores.region%TYPE;
	l_url                stores.url%TYPE;
	l_hide_phone         stores.hide_phone%TYPE;
	l_commune            stores.commune%TYPE;
	l_address_number     stores.address_number%TYPE;
	l_website_url        stores.website_url%TYPE;
	l_main_image         stores.main_image%TYPE;
	l_main_image_offset         stores.main_image_offset%TYPE;
	l_logo_image         stores.logo_image%TYPE;

	l_store_changes_cursor CURSOR (i_store_id stores.store_id%TYPE, 
				       i_action_id store_actions.action_id%TYPE) IS 
		SELECT
			is_param,
			column_name,
			new_value
		FROM
			store_changes
		WHERE
			store_id = i_store_id AND
			action_id = i_action_id AND
			(state_id, column_name) IN (
				SELECT
					MAX(state_id),
					column_name
				FROM
					store_changes
				WHERE
					store_id = i_store_id AND
					action_id = i_action_id
				GROUP BY
					column_name)
		ORDER BY
			state_id;

	l_is_param store_changes.is_param%TYPE;
	l_column_name store_changes.column_name%TYPE;
	l_new_value store_changes.new_value%TYPE;
	l_usernames varchar[];
	l_i integer;
BEGIN
-- XXX What to do if old_value is wrong?
	SELECT 
		name,
		phone,
		address,
		info_text,
		status,
		date_start,
		date_end,
		region,
		url,
		hide_phone,
		commune,
		address_number,
		website_url,
		main_image,
		main_image_offset,
		logo_image
	INTO
		l_name,
		l_phone,
		l_address,
		l_info_text,
		l_status,
		l_date_start,
		l_date_end,
		l_region,
		l_url,
		l_hide_phone,
		l_commune,
		l_address_number,
		l_website_url,
		l_main_image,
		l_main_image_offset,
		l_logo_image
	FROM
		stores
	WHERE
		store_id = i_store_id;
	
	IF NOT FOUND THEN
		RAISE EXCEPTION 'ERROR_STORE_NOT_FOUND';
	END IF;
	
	OPEN l_store_changes_cursor(i_store_id, i_action_id);
	LOOP
		FETCH l_store_changes_cursor INTO l_is_param, l_column_name, l_new_value;
		EXIT WHEN NOT FOUND;
		IF NOT l_is_param THEN
			-- XXX column names, use EXECUTE?
			IF l_column_name = 'name' THEN
				l_name := l_new_value;
			ELSIF l_column_name = 'phone' THEN
				l_phone := l_new_value;
			ELSIF l_column_name = 'address' THEN
				l_address := l_new_value;
			ELSIF l_column_name = 'info_text' THEN
				l_info_text := l_new_value;
			ELSIF l_column_name = 'status' THEN
				l_status := l_new_value;
			ELSIF l_column_name = 'date_start' THEN
				l_date_start := l_new_value;
			ELSIF l_column_name = 'date_end' THEN
				l_date_end := l_new_value;
			ELSIF l_column_name = 'region' THEN
				l_region := l_new_value;
			ELSIF l_column_name = 'url' THEN
				l_url := l_new_value;
			ELSIF l_column_name = 'hide_phone' THEN
				l_hide_phone := l_new_value;
			ELSIF l_column_name = 'commune' THEN
				l_commune := l_new_value;
			ELSIF l_column_name = 'address_number' THEN
				l_address_number := l_new_value;
			ELSIF l_column_name = 'website_url' THEN
				l_website_url := l_new_value;
			ELSIF l_column_name = 'main_image' THEN
				l_main_image := l_new_value;
			ELSIF l_column_name = 'main_image_offset' THEN
				l_main_image_offset := l_new_value;
			ELSIF l_column_name = 'logo_image' THEN
				l_logo_image := l_new_value;
			END IF;
		ELSE -- is_param
			IF l_new_value IS NULL OR l_new_value = '' THEN -- param removed
				DELETE FROM
					store_params
				WHERE
					store_id = i_store_id AND
					name = l_column_name;
			ELSE 
				UPDATE
					store_params
				SET
					value = l_new_value
				WHERE
					store_id = i_store_id AND
					name = l_column_name::enum_store_params_name;
				IF NOT FOUND THEN
					INSERT INTO
						store_params
						       (store_id,
							name,
							value)
					VALUES
					       (i_store_id,
						l_column_name::enum_store_params_name,
						l_new_value);
				END IF;
			END IF;
		END IF;
	END LOOP;
	CLOSE l_store_changes_cursor;
	
	UPDATE
		stores
	SET
		name      	= l_name,
		phone     	= l_phone,
		address   	= l_address,
		info_text 	= l_info_text,
		status    	= l_status,
		date_start	= l_date_start,
		date_end  	= l_date_end,
		region    	= l_region,
		url       	= l_url,
		hide_phone	= l_hide_phone,
		commune   	= l_commune,
		address_number	= l_address_number,
		website_url	= l_website_url,
		main_image	= l_main_image,
		main_image_offset	= l_main_image_offset,
		logo_image 	= l_logo_image
	WHERE
		store_id = i_store_id;

	IF l_main_image IS NOT NULL AND l_main_image NOT LIKE 'DEFAULT_%' THEN
		-- Unlink images
		UPDATE
			store_media	
		SET
			store_id = NULL
		WHERE	
			image_type = 'main_image' AND
			store_id = i_store_id;

		UPDATE
			store_media
		SET
			store_id = i_store_id
		WHERE
			TRIM(trailing '.jpg' FROM l_main_image)::bigint = store_media_id
			AND image_type = 'main_image';
	END IF;			

	IF l_logo_image IS NOT NULL THEN
		-- Unlink images
		UPDATE
			store_media	
		SET
			store_id = NULL
		WHERE	
			image_type = 'logo_image' AND
			store_id = i_store_id;

		UPDATE
			store_media
		SET
			store_id = i_store_id
		WHERE
			TRIM(trailing '.jpg' FROM l_logo_image)::bigint = store_media_id
			AND image_type = 'logo_image';
	END IF;			
	
/*	IF l_usernames IS NOT NULL THEN
		DELETE FROM
			store_users
		WHERE
			store_id = i_store_id;

		FOR l_i IN array_lower (l_usernames, 1) .. array_upper (l_usernames, 1) LOOP
			INSERT INTO store_users (
				store_id,
				email
			) VALUES (
				i_store_id,
				l_usernames[l_i]
			);
		END LOOP;
	END IF;
*/
END
$$ LANGUAGE plpgsql;
