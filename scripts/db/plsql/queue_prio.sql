CREATE OR REPLACE FUNCTION queue_prio(
		i_queue_list varchar[],
		i_queue_prios varchar[][],
		i_queue_percent varchar[],
		i_queue_percent_groups varchar[],
		OUT o_queue ad_actions.queue%TYPE) AS $$

DECLARE
	l_queue_prio_groups varchar[];
	l_queue varchar;
	l_prio integer;
	l_new_prio integer;
	l_percent integer;
BEGIN
	l_queue_prio_groups=ARRAY['default'];
	l_prio = -1;
	-- Save this value to avoid stranger things
	FOREACH l_queue IN ARRAY i_queue_list
	LOOP
		l_new_prio := queue_in_array_lookup('default', l_queue, l_queue_prio_groups, i_queue_prios);
		IF l_new_prio IS NULL THEN
			l_new_prio := 0;
		END IF;
		l_percent := queue_in_array_lookup('default', l_queue, i_queue_percent_groups, i_queue_percent);
		IF l_percent IS NULL THEN
			l_percent := 100;
		END IF;
		IF l_new_prio > l_prio AND 100*random() <= l_percent THEN
			o_queue := l_queue;
			l_prio := l_new_prio;
		END IF;
	END LOOP;
END;
$$ LANGUAGE plpgsql;
