CREATE OR REPLACE FUNCTION bid_ad_delete(i_bid_ad_id bid_ads.bid_ad_id%TYPE,
					 OUT o_bid_ad_id bid_ads.bid_ad_id%TYPE)
	RETURNS bid_bids.bid_id%TYPE AS $$
BEGIN
	IF EXISTS(SELECT bid_ad_id FROM bid_ads WHERE bid_ad_id=i_bid_ad_id) THEN
		DELETE FROM bid_bids WHERE bid_ad_id=i_bid_ad_id;
		DELETE FROM bid_media WHERE bid_ad_id=i_bid_ad_id;
		DELETE FROM bid_ads WHERE bid_ad_id=i_bid_ad_id;
		o_bid_ad_id = i_bid_ad_id;
	ELSE
		RAISE EXCEPTION 'FACTORY_TRANSLATE:bid_ad_id:ERROR_NO_SUCH_AD';
	END IF;
END
$$ LANGUAGE plpgsql;
