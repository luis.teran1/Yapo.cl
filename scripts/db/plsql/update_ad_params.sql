CREATE OR REPLACE FUNCTION update_ad_params(
	i_ad_ids integer[],
	i_ad_param varchar[],
	i_value varchar[],
	i_token tokens.token%TYPE,
	i_remote_addr action_states.remote_addr%TYPE
) RETURNS void AS $$
DECLARE
	l_action_id ad_actions.action_id%TYPE;
	l_state_id action_states.state_id%TYPE;
	l_current_state ad_actions.state%TYPE;
	l_i integer;
	l_j integer;
BEGIN
	-- Procedure to mass update ad params of a set of ads
	IF array_length(i_ad_param, 1) != array_length(i_value, 1) THEN
		RAISE EXCEPTION 'AD_PARAM_VALUE_LENGTH_MISMATCH';
	END IF;

	IF array_length(i_ad_param, 1) = 0 THEN
		--RAISE NOTICE 'NOTHING_TO_DO';
		RETURN;
	END IF;

	-- Iterate between the ads ids
	FOR l_i IN array_lower(i_ad_ids, 1) .. array_upper(i_ad_ids,1)
	LOOP
		-- Verify if ad was deleted
		SELECT
			aa.state
		INTO
			l_current_state
		FROM
			ad_actions AS aa
		JOIN
			action_states AS ae ON ae.ad_id = aa.ad_id AND aa.current_state = ae.state_id
		WHERE
			aa.ad_id = i_ad_ids[l_i]
		ORDER BY
			timestamp DESC
		LIMIT 1;

		IF l_current_state = 'deleted' THEN
			CONTINUE;
		END IF;

		-- Create the update ad params action with the respective state
		SELECT
			o_action_id,
			o_state_id
		INTO
			l_action_id,
			l_state_id
		FROM insert_ad_action_state(i_ad_ids[l_i], 'update_ad_params', 'reg', 'initial', i_remote_addr, get_token_id(i_token));

		-- Accept the state
		l_state_id := insert_state(i_ad_ids[l_i], l_action_id, 'accepted', 'adminclear', i_remote_addr, NULL);

		-- Update ad params
		FOR l_j IN array_lower(i_ad_param, 1) .. array_upper(i_ad_param,1)
		LOOP
			PERFORM insert_ad_change(i_ad_ids[l_i], l_action_id, l_state_id, true, i_ad_param[l_j], i_value[l_j]);
		END LOOP;

		-- Actually apply the changes
		PERFORM apply_ad_changes(i_ad_ids[l_i], l_action_id, NULL);

	END LOOP;
END;
$$ LANGUAGE plpgsql;
