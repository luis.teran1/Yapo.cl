CREATE OR REPLACE FUNCTION insert_all_ad_changes(i_ad_id action_states.ad_id%TYPE,
						 i_action_id action_states.action_id%TYPE,
						 i_state_id action_states.state_id%TYPE,
						 i_ads_name ads.name%TYPE,
						 i_ads_phone ads.phone%TYPE,
						 i_ads_region ads.region%TYPE,
						 i_ads_city ads.city%TYPE,
						 i_ads_type ads.type%TYPE,
						 i_ads_category ads.category%TYPE,
						 i_ads_passwd ads.salted_passwd%TYPE,
						 i_user_id ads.user_id%TYPE,
						 i_store_id ads.store_id%TYPE,
						 i_phone_hidden ads.phone_hidden%TYPE,
						 i_company_ad ads.company_ad%TYPE,
						 i_ads_subject ads.subject%TYPE,
						 i_ads_body ads.body%TYPE,
						 i_ads_price ads.price%TYPE,
						 i_ads_infopage ads.infopage%TYPE,
						 i_ads_infopage_title ads.infopage_title%TYPE, 
						 i_ad_images varchar[],
						 i_ad_thumb_digest varchar[],
						 i_ad_image_had_digest bool[],
						 i_ad_params varchar[][],
						 i_edit_type varchar(20),
						 i_static_params text[],
 						 i_lang varchar) RETURNS VOID AS $$
DECLARE
	l_name ads.name%TYPE;
	l_phone ads.phone%TYPE;
	l_region ads.region%TYPE;
	l_city ads.city%TYPE;
	l_type ads.type%TYPE;
	l_category ads.category%TYPE;
	l_passwd ads.passwd%TYPE;
	l_subject ads.subject%TYPE;
	l_body ads.body%TYPE;
	l_price ads.price%TYPE;
	l_infopage ads.infopage%TYPE;
	l_infopage_title ads.infopage_title%TYPE; 
	l_old_param_value ad_params.value%TYPE;
	l_phone_hidden ads.phone_hidden%TYPE;
	l_company_ad ads.company_ad%TYPE;
	l_user_id ads.user_id%TYPE;
	l_i integer;
	l_ad_param_name VARCHAR; --ad_params.name%TYPE;
	l_ad_param_value ad_params.value%TYPE;
	l_is_new_image bool;
	l_image ads.image%TYPE;
	l_digest ad_images_digests.digest%TYPE;
	l_uid users.uid%TYPE;
	l_old_cat ads.category%TYPE;
	l_ad_image_name ad_image_changes.name%TYPE;
	l_ad_image_id bigint;
	l_image_ad_id action_states.ad_id%TYPE;
	l_temp_ad_status ads.status%TYPE;
	l_skip_image_changes boolean;
BEGIN
	-- Get uid
        SELECT
                uid
        INTO
                l_uid
        FROM
                users
        WHERE
                user_id = i_user_id;
	
	-- Ad images 
	IF i_ad_images[1] IS NULL THEN

		INSERT INTO
			ad_image_changes (
				ad_id,
				action_id,
				state_id,
				seq_no,
				name,
				is_new)
		VALUES (
			i_ad_id,
			i_action_id,
			i_state_id,
			0,
			NULL,
			'f');
	ELSE
		l_i := 1;
		LOOP
			l_ad_image_name := i_ad_images[l_i];
			l_skip_image_changes := FALSE;
			EXIT WHEN l_ad_image_name IS NULL;

			l_ad_image_id := TRIM(trailing '.jpg' FROM l_ad_image_name)::bigint;
			SELECT
				ad_media.ad_id,
				ads.status
			FROM
				ad_media
			LEFT JOIN
				ads USING (ad_id)
			WHERE
				ad_media_id = l_ad_image_id
			INTO
				l_image_ad_id,
				l_temp_ad_status;

			-- This is to prevent img theft
			IF (l_image_ad_id IS NOT NULL AND l_image_ad_id != i_ad_id) THEN
				RAISE NOTICE 'ERROR_INVALID_UPDATE_TO_ADMEDIA';
				IF l_temp_ad_status = 'active' THEN
					l_skip_image_changes := TRUE;
				END IF;
			END IF;

			-- If the image is new or not
			l_is_new_image := l_image_ad_id IS NULL;

			IF l_skip_image_changes = FALSE THEN 
				INSERT INTO
					ad_image_changes (
						ad_id,
						action_id,
						state_id,
						seq_no,
						name,
						is_new)
				VALUES (
					i_ad_id,
					i_action_id,
					i_state_id,
					l_i - 1,
					l_ad_image_name,
					COALESCE(l_is_new_image, 'f'));
			END IF;

			IF COALESCE(length(i_ad_thumb_digest[l_i]), 0) = 32 THEN
				-- Add extra images thumb digests
				SELECT 
					digest 
				INTO
					l_digest
				FROM
					ad_images_digests
				WHERE
					ad_id = i_ad_id AND
					name = l_ad_image_name;

				IF l_digest IS NULL THEN
					INSERT
					INTO
						ad_images_digests
							(name,
							digest,
							ad_id,
							previously_inserted,
							uid)
					VALUES
						(l_ad_image_name,
						i_ad_thumb_digest[l_i],
						i_ad_id,
						i_ad_image_had_digest[l_i],
						l_uid);
				ELSE
					UPDATE
						ad_images_digests
					SET
						digest = i_ad_thumb_digest[l_i],
						previously_inserted = i_ad_image_had_digest[l_i]
					WHERE	
						ad_id = i_ad_id AND
						name = l_ad_image_name;
				END IF;
			END IF;

			l_i := l_i + 1;
		END LOOP;
	END IF;
	
	IF i_edit_type != 'review' THEN
		PERFORM insert_ad_change(i_ad_id, i_action_id, i_state_id, false, 'phone', i_ads_phone);
		PERFORM insert_ad_change(i_ad_id, i_action_id, i_state_id, false, 'city', i_ads_city::text);
		IF i_ads_passwd IS NOT NULL THEN
			PERFORM insert_ad_change(i_ad_id, i_action_id, i_state_id, false, 'salted_passwd', i_ads_passwd);
			PERFORM insert_ad_change(i_ad_id, i_action_id, i_state_id, false, 'passwd', NULL);
		END IF;
		PERFORM insert_ad_change(i_ad_id, i_action_id, i_state_id, false, 'subject', i_ads_subject);
		PERFORM insert_ad_change(i_ad_id, i_action_id, i_state_id, false, 'body', i_ads_body);
		PERFORM insert_ad_change(i_ad_id, i_action_id, i_state_id, false, 'phone_hidden', i_phone_hidden::text);
		PERFORM insert_ad_change(i_ad_id, i_action_id, i_state_id, false, 'infopage', i_ads_infopage);
		PERFORM insert_ad_change(i_ad_id, i_action_id, i_state_id, false, 'infopage_title', i_ads_infopage_title);
		PERFORM insert_ad_change(i_ad_id, i_action_id, i_state_id, false, 'store_id', i_store_id::text);
	ELSIF i_ads_infopage IS NULL THEN
		PERFORM insert_ad_change(i_ad_id, i_action_id, i_state_id, false, 'infopage', NULL);
		PERFORM insert_ad_change(i_ad_id, i_action_id, i_state_id, false, 'infopage_title', NULL);
	END IF;
	PERFORM insert_ad_change(i_ad_id, i_action_id, i_state_id, false, 'price', i_ads_price::text);

	IF i_company_ad IS NOT NULL THEN
		PERFORM insert_ad_change(i_ad_id, i_action_id, i_state_id, false, 'company_ad', i_company_ad::text);
	END IF;
	IF i_edit_type IN ('adminedit', 'new', 'prolong', 'editrefused', 'import', 'edit') THEN
		PERFORM insert_ad_change(i_ad_id, i_action_id, i_state_id, false, 'name', i_ads_name);
		PERFORM insert_ad_change(i_ad_id, i_action_id, i_state_id, false, 'category', i_ads_category::text);
		PERFORM insert_ad_change(i_ad_id, i_action_id, i_state_id, false, 'region', i_ads_region::text);
		PERFORM insert_ad_change(i_ad_id, i_action_id, i_state_id, false, 'type', i_ads_type::text);
	ELSIF i_edit_type = 'review' THEN
		IF i_ads_subject IS NOT NULL THEN
			PERFORM insert_ad_change(i_ad_id, i_action_id, i_state_id, false, 'subject', i_ads_subject);
		END IF;
		IF i_ads_body IS NOT NULL THEN
			PERFORM insert_ad_change(i_ad_id, i_action_id, i_state_id, false, 'body', i_ads_body);
		END IF;
		IF i_ads_category IS NOT NULL THEN
			PERFORM insert_ad_change(i_ad_id, i_action_id, i_state_id, false, 'category', i_ads_category::text);
		END IF;
		IF i_ads_type IS NOT NULL THEN
			PERFORM insert_ad_change(i_ad_id, i_action_id, i_state_id, false, 'type', i_ads_type::text);
		END IF;
	END IF;
	-- Allow category change from old category (<1000) to new category 
	-- XXX: Prolong is matched above but should it be?? 
	IF i_edit_type IN ('edit', 'renew') THEN 
		SELECT
			category
		INTO
			l_old_cat
		FROM
			ads
		WHERE
			ad_id = i_ad_id;

		IF l_old_cat < 1000 AND  i_ads_category >= 1000 THEN
			PERFORM insert_ad_change(i_ad_id, i_action_id, i_state_id, false, 'category', i_ads_category::text);
		END IF;
	END IF;
	IF i_edit_type IN ('adminedit', 'import') THEN
		PERFORM insert_ad_change(i_ad_id, i_action_id, i_state_id, false, 'user_id', i_user_id::text);
	END IF;

	-- Add new/changed params
	IF i_ad_params IS NOT NULL THEN
		l_i := 1;
	
		LOOP
			l_ad_param_name := i_ad_params[l_i][1];
			l_ad_param_value := i_ad_params[l_i][2];
			EXIT WHEN l_ad_param_name IS NULL;
			
			PERFORM insert_ad_change(i_ad_id, i_action_id, i_state_id, true, l_ad_param_name,
						 l_ad_param_value);
			l_i := l_i + 1;
		END LOOP;
	END IF;
	
	-- Add removed params
	DECLARE 
		rec_ad_params RECORD;
	BEGIN
		FOR
			rec_ad_params
		IN
			SELECT
				CAST( name AS VARCHAR )
			FROM
				ad_params
			WHERE
				ad_id = i_ad_id AND
				(i_ad_params IS NULL OR
				CAST( name AS VARCHAR ) <> ALL (i_ad_params[1:array_upper(i_ad_params,1)][1])) AND
				CAST( name AS VARCHAR ) <> ALL (i_static_params)
		LOOP
			PERFORM insert_ad_change(i_ad_id, i_action_id, i_state_id, true, rec_ad_params.name, NULL);
		END LOOP;
	END;
END
$$ LANGUAGE plpgsql;
