CREATE OR REPLACE FUNCTION quote_literal_null(i_val anyelement) RETURNS text AS $$
	SELECT COALESCE(quote_literal($1), 'NULL');
$$ LANGUAGE sql IMMUTABLE;
