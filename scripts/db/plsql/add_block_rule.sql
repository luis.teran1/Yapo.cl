CREATE OR REPLACE FUNCTION add_block_rule(i_rule_id block_rules.rule_id%TYPE,
					  i_rule_name block_rules.rule_name%TYPE,
					  i_application block_rules.application%TYPE,
					  i_combine_rule block_rules.combine_rule%TYPE,
					  i_action block_rules.action%TYPE,
					  i_target block_rules.target%TYPE,
					  o_rule_id OUT block_rules.rule_id%TYPE) AS $$
BEGIN
	-- Add or update rule
	IF i_rule_id IS NOT NULL THEN
		DELETE FROM block_rule_conditions WHERE rule_id = i_rule_id;

		-- insert a new block_rule
		UPDATE 
			block_rules 
		SET
			
			rule_name = i_rule_name,
			action = i_action,
			target = i_target,
			application = i_application,
			combine_rule = i_combine_rule
		WHERE
			rule_id = i_rule_id;

		o_rule_id := i_rule_id;
	ELSE
		o_rule_id := nextval('block_rules_rule_id_seq');
	
		-- insert a new block_rule
		INSERT INTO 
			block_rules (rule_id, rule_name, action, target, application, combine_rule)
		VALUES
			(o_rule_id, i_rule_name, i_action, i_target, i_application, i_combine_rule);
	END IF;
END;
$$ LANGUAGE plpgsql;
