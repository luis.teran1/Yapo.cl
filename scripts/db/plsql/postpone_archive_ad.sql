CREATE OR REPLACE FUNCTION postpone_archive_ad (
		i_ad_id ads.ad_id%TYPE
		) RETURNS void AS $$
BEGIN
	PERFORM insert_ad_action_state (
			i_ad_id,
			'postpone_archive',
			'postpone_archive',
			'postpone_archive',
			NULL,
			NULL);
END
$$ LANGUAGE plpgsql;
