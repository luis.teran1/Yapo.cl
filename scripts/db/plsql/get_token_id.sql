CREATE OR REPLACE FUNCTION get_token_id(i_token varchar, OUT o_token_id integer) RETURNS integer AS $$
BEGIN
	SELECT
		token_id
	INTO
		o_token_id
	FROM
		tokens
	WHERE
		token = i_token
	LIMIT 1;

	IF NOT FOUND THEN
		o_token_id := NULL;
	END IF;
END;
$$ STABLE LANGUAGE plpgsql;
