CREATE OR REPLACE FUNCTION get_user_id_by_ad_id(i_ad_id ads.ad_id%TYPE, 
						OUT o_user_id ads.user_id%TYPE) AS $$
BEGIN
	SELECT
		user_id
	INTO
		o_user_id
	FROM
		ads
	WHERE
		ad_id = i_ad_id;

	IF NOT FOUND
	THEN
		RAISE EXCEPTION 'ERROR_USER_NOT_FOUND';
	END IF;
END;
$$ LANGUAGE plpgsql;
