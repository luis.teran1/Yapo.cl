CREATE OR REPLACE FUNCTION phone_standarization_accounts(
	i_comune_conf integer[][],
	OUT o_account_id integer,
	OUT o_updated integer
)
RETURNS SETOF record AS $$
DECLARE
	l_rec record;
	l_counter INTEGER := 0;
	l_new_phone varchar;
	l_update_result integer;
	l_code varchar;
	l_last_commune varchar;
BEGIN

	FOR l_rec IN (SELECT account_id, phone, commune FROM accounts WHERE status = 'active' AND phone is not null)
	LOOP
		IF l_rec.commune is null THEN
			l_new_phone := phone_standarization_rm('', l_rec.phone);
		ELSE
			l_code := get_area_code(l_rec.commune::INTEGER, i_comune_conf);
			IF l_code = '2' THEN
				l_new_phone := phone_standarization_rm(l_code, l_rec.phone) ;
			ELSE
				l_new_phone := phone_standarization_norm(l_code, l_rec.phone);
			END IF;
		END IF;
		o_updated := 0;
		IF l_new_phone != l_rec.phone THEN
			UPDATE accounts SET phone = l_new_phone WHERE account_id = l_rec.account_id;
			PERFORM insert_account_action(l_rec.account_id, 'phone_update', NULL);
			o_updated := 1;
		END IF;
		RAISE NOTICE 'PHONE UPDATE ACCOUNT % OLD:% NEW:% UPDATED:%', l_rec.account_id, l_rec.phone, l_new_phone, o_updated;
		o_account_id := l_rec.account_id;
		RETURN NEXT;
	END LOOP;
END;
$$ LANGUAGE plpgsql;
