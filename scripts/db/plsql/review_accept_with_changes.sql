CREATE OR REPLACE FUNCTION review_accept_with_changes(i_ad_id ad_actions.ad_id%TYPE,
					 i_action_id ad_actions.action_id%TYPE,
					 i_token tokens.token%TYPE,
					 i_remote_addr action_states.remote_addr%TYPE,
					 i_filter_name filters.name%TYPE,
					 i_ads_type ads.type%TYPE,
					 i_ads_category ads.category%TYPE,
					 i_company_ad ads.company_ad%TYPE,
					 i_ads_price ads.price%TYPE,
					 i_ads_subject ads.subject%TYPE,
					 i_ads_body ads.body%TYPE,
					 i_remove_infopage bool,
					 i_remove_images bool[],
					 i_ad_params varchar[][],
					 i_valid_params varchar[],
					 i_gallery_expire_minutes integer,
					 i_set_first smallint,
					 i_lang varchar,
					 i_reason state_params.value%TYPE,
					 i_acceptance_text state_params.value%TYPE,
					 OUT o_list_id ads.list_id%TYPE,
					 OUT o_first_listing bool,
					 OUT o_redir_code redir_stats.code%TYPE,
					 OUT o_remote_addr action_states.remote_addr%TYPE,
					 OUT o_action_type ad_actions.action_type%TYPE,
					 OUT o_company_ad ads.company_ad%TYPE) AS $$
DECLARE
	l_state_id action_states.state_id%TYPE;
	l_list_time ads.list_time%TYPE;
	l_orig_list_time ads.list_time%TYPE;
	l_action_type ad_actions.action_type%TYPE;
	l_action_type_before_refuse ad_actions.action_type%TYPE;
	l_ads_category ads.category%TYPE;
	l_ad_status ads.status%TYPE;
	l_img_change_state action_states.state_id%TYPE;
	l_ad_images varchar[];
	l_i integer;
	l_rec record;
	l_ads_price ads.price%TYPE;
	l_price ads.price%TYPE;
	l_currency varchar;
	l_old_price ads.price%TYPE;
	l_prev_currency varchar;
	l_status integer;
	l_user_id ads.user_id%TYPE;
	l_first_approved_ad user_params.value%TYPE;
	l_external_ad_id ads.list_id%TYPE;
	l_admin_id integer;
	l_pack_status varchar;
	l_pack_act_id ad_actions.action_id%TYPE;
BEGIN
	-- Get price
	SELECT
		price,
		old_price,
		user_id
	INTO
		l_price,
		l_old_price,
		l_user_id
	FROM
		ads
	WHERE
		ad_id = i_ad_id;

	-- Get currency
	SELECT
		value
	INTO
		l_currency
	FROM
		ad_params
	WHERE
		ad_id = i_ad_id AND name = 'currency';

	SELECT
		value
	INTO
		l_prev_currency
	FROM
		ad_params
	WHERE
		ad_id = i_ad_id AND name = 'prev_currency';

	-- Insert the accepted with changes state
	l_state_id := insert_state(i_ad_id,
				   i_action_id, 
				   'accepted',
				   'accept_w_chngs', 
				   i_remote_addr, 
				   get_token_id(i_token));
	INSERT INTO
		state_params 
	VALUES (
		i_ad_id,
		i_action_id,
		l_state_id,
		'filter_name',
		i_filter_name);

	-- Build up a new array of images from the old one and i_remove_images
	SELECT
		MAX(state_id)
	INTO
		l_img_change_state
	FROM
		ad_image_changes
	WHERE
		ad_id = i_ad_id
		AND action_id = i_action_id;

	IF i_set_first IS NOT NULL THEN
		l_i := 1;
	ELSE
		l_i := 0;
	END IF;

	IF l_img_change_state IS NOT NULL THEN
		FOR
			l_rec
		IN
			SELECT
				name,
				seq_no
			FROM
				ad_image_changes
			WHERE
				ad_id = i_ad_id
				AND action_id = i_action_id
				AND state_id = l_img_change_state
			ORDER BY
				seq_no
		LOOP
			IF i_set_first IS NOT NULL AND l_rec.seq_no = i_set_first THEN --CHUS 261: Change order of images
				l_ad_images[1] := l_rec.name;
			ELSE
				IF i_remove_images[l_rec.seq_no + 1] IS NULL OR NOT i_remove_images[l_rec.seq_no + 1] THEN
					l_ad_images[l_i + 1] := l_rec.name;
					l_i := l_i + 1;
				END IF;
			END IF;
		END LOOP;
	ELSE
		FOR
			l_rec
		IN
			SELECT
				ad_media_id||'.jpg' AS name,
				seq_no
			FROM
				ad_media
			WHERE
				COALESCE(media_type, 'image') = 'image' AND
				ad_id = i_ad_id
			ORDER BY
				seq_no

		LOOP
			IF i_set_first IS NOT NULL AND l_rec.seq_no = i_set_first THEN --CHUS 261: Change order of images
				l_ad_images[1] := l_rec.name;
			ELSE
				IF i_remove_images[l_rec.seq_no + 1] IS NULL OR NOT i_remove_images[l_rec.seq_no + 1] THEN
					l_ad_images[l_i + 1] := l_rec.name;
					l_i := l_i + 1;
				END IF;
			END IF;
		END LOOP;
	END IF;

	IF i_ads_price IS NULL AND i_token IS NULL THEN
		-- Autoaccept doesn't send the price. Find it.
		l_ads_price := get_changed_value(i_ad_id,
						 i_action_id,
						 false,
						 'price');
	ELSE
		l_ads_price := i_ads_price;
	END IF;
	PERFORM insert_all_ad_changes(i_ad_id,
				      i_action_id,
				      l_state_id,
				      NULL,
				      NULL,
				      NULL,
				      NULL,
				      i_ads_type,
				      i_ads_category,
				      NULL,
				      NULL,
				      NULL,
				      NULL,
				      i_company_ad,
				      i_ads_subject,
				      i_ads_body,
				      l_ads_price,
				      CASE WHEN i_remove_infopage THEN NULL ELSE '' END,
				      NULL,
				      l_ad_images,
				      NULL,
				      NULL,
				      i_ad_params,
				      'review',
				      i_valid_params,  -- Mark all valid params as static, so they're not deleted
				      i_lang);

	-- Get current status information
	SELECT
		list_id,
		list_time,
		orig_list_time,
		action_type,
		status,
		company_ad
	INTO
		o_list_id,
		l_list_time,
		l_orig_list_time,
		l_action_type,
		l_ad_status,
		o_company_ad
	FROM
		ad_actions
		JOIN ads USING (ad_id)
	WHERE
		ad_id = i_ad_id AND
		action_id = i_action_id;

	-- Get link_type
	SELECT
		value::integer
	INTO
		l_external_ad_id
	FROM
		ad_params
	WHERE
		ad_id = i_ad_id AND
		name = 'external_ad_id' AND
		ad_id = (
			SELECT 
				ad_id
			FROM
				ad_params
			WHERE
				name = 'link_type' AND
				value = 'spidered_balcao' AND
				ad_id = i_ad_id
		);

	-- Check if the ad never been listed
	IF o_list_id IS NULL THEN
		IF l_external_ad_id IS NULL THEN
			o_list_id := NEXTVAL('list_id_seq');
		ELSE
			o_list_id := l_external_ad_id;
		END IF;
		o_first_listing := true;
	ELSE
		o_first_listing := false;
	END IF;

	PERFORM accept_action (i_ad_id, i_action_id, NULL, i_remote_addr, i_token, i_gallery_expire_minutes,
			l_action_type, l_state_id);

	-- Store the original list_time
	IF l_orig_list_time IS NULL AND l_list_time IS NOT NULL THEN
		l_orig_list_time := l_list_time;
	END IF;

	-- Update list_time if needed
	IF l_list_time IS NULL OR l_action_type IN ('renew', 'prolong') OR (l_action_type IN ('editrefused') AND l_ad_status IN ('refused')) THEN
		l_list_time := date_trunc('second', CURRENT_TIMESTAMP);
	ELSE
		SELECT
			atbr.o_action_type
		INTO
			l_action_type_before_refuse
		FROM
			action_type_before_refuse(i_ad_id, i_action_id, NULL) AS atbr;

		IF l_action_type_before_refuse IN ('renew', 'prolong') THEN
			l_list_time := date_trunc('second', CURRENT_TIMESTAMP);
		END IF;
	END IF;

	-- Reset the orig_list_time if ad is prolonged or if list_time never changed
	IF l_orig_list_time = l_list_time OR l_action_type IN ('prolong') OR (l_action_type IN ('editrefused') AND l_action_type_before_refuse IN ('prolong')) THEN
		l_orig_list_time := NULL;
	END IF;

	-- Set old_price to the original price
	-- accept_action sets old_price only if it changes with this action, we want to set it always.
	-- XXX set old_price only when price changes is maybe better?
	IF l_old_price IS NULL THEN
		l_old_price := l_price;

		IF l_currency IS NULL THEN
			l_prev_currency := 'peso';
		ELSE
			l_prev_currency := l_currency;
		END IF;

		IF l_ads_price = l_old_price THEN
			l_old_price := NULL;
			l_prev_currency := NULL;
		END IF;
	ELSIF l_old_price IS NOT NULL AND (l_action_type IN ('prolong') OR l_action_type_before_refuse IN ('prolong')) THEN
		l_old_price := NULL;
		l_prev_currency := NULL;
	END IF;

	l_pack_status := update_ad_pack_status(i_ad_id, i_action_id, l_user_id);
	-- Update ad status
	UPDATE
		ads
	SET
		status = l_pack_status::enum_ads_status,
		list_id = o_list_id,
		list_time = l_list_time,
		orig_list_time = l_orig_list_time,
		old_price = l_old_price,
		modified_at = CURRENT_TIMESTAMP
	WHERE
		ad_id = i_ad_id;


       -- update subject
       IF i_ads_subject IS NOT NULL THEN
               UPDATE
			ads
               SET
			subject = i_ads_subject
               WHERE
			ad_id = i_ad_id;
       END IF;

       -- update body
       IF i_ads_body IS NOT NULL THEN
               UPDATE
			ads
               SET
			body = i_ads_body
               WHERE
			ad_id = i_ad_id;
       END IF;

	-- Inserts/updates prev_currency in ad_params
	IF l_prev_currency IS NOT NULL THEN
		IF NOT EXISTS (SELECT * FROM ad_params WHERE ad_id = i_ad_id AND name = 'prev_currency') THEN
			INSERT INTO
				ad_params
			VALUES
				(i_ad_id, 'prev_currency', l_prev_currency);
		ELSE
			UPDATE
				ad_params
			SET
				value = l_prev_currency
			WHERE
				ad_id = i_ad_id AND name = 'prev_currency';
		END IF;
	END IF;

	--Add new action to activate trigger
	IF l_action_type in ('new') AND EXISTS ( SELECT * FROM ad_params WHERE ad_id = i_ad_id AND name = 'pack_status' AND value = 'active') THEN
		l_pack_act_id := insert_ad_action(i_ad_id, 'status_change', NULL);
		PERFORM insert_state(i_ad_id, l_pack_act_id, 'accepted', 'status_change', i_remote_addr, get_token_id(i_token));
	END IF;

	-- Unlock the action in ad_queues
	DELETE
	FROM
		ad_queues
	WHERE
		ad_id = i_ad_id AND
		action_id = i_action_id;

	-- Check for redir
	SELECT
		value
	INTO
		o_redir_code
	FROM
		action_params
	WHERE
		ad_id = i_ad_id AND
		action_id = i_action_id AND
		name = 'redir';

	-- Get remote addr on the reg state of current action
	SELECT
		remote_addr
	INTO
		o_remote_addr
	FROM
		action_states
	WHERE
		ad_id = i_ad_id AND
		action_id = i_action_id AND
		state = 'reg';

	o_action_type := COALESCE(l_action_type, l_action_type_before_refuse);

	-- Insert first_approved_ad into user_params if it doesn't already exist
	SELECT
		value
	INTO
		l_first_approved_ad
	FROM
		user_params
	WHERE
		user_id = l_user_id AND
		name = 'first_approved_ad';

	IF l_first_approved_ad IS NULL THEN
		INSERT INTO
			user_params (user_id, name, value)
		VALUES
			(l_user_id, 'first_approved_ad', NOW());
	END IF;

    --Get the admin id to put in the review log
	--Only if it is a human action (not autoaccept)
	IF i_token IS NOT NULL THEN
		SELECT
		   COALESCE(admin_id, 0)
		INTO
			l_admin_id
		FROM
			tokens
		WHERE
			token = i_token;

		--Insert a row in the review log
		INSERT INTO
			review_log(
				ad_id,
				action_id,
				admin_id,
				queue,
				action_type,
				action,
				category,
				refusal_reason_text)
		VALUES 
			(i_ad_id,
			i_action_id,
			COALESCE(l_admin_id, 0),
			i_filter_name,
			COALESCE(o_action_type, (SELECT action_type FROM ad_actions WHERE ad_id = i_ad_id AND action_id = i_action_id)),
			'accept_ch',
			COALESCE(i_ads_category,(SELECT category FROM ads WHERE ad_id = i_ad_id)),
			COALESCE(i_acceptance_text,(SELECT value FROM conf WHERE key = '*.*.common.accepted.' || i_reason || '.name'), i_reason)
			);
	END IF;

END
$$ LANGUAGE plpgsql;
