CREATE OR REPLACE FUNCTION voucher_state_insert(i_voucher_id vouchers.voucher_id%TYPE, 
		i_voucher_action_id voucher_actions.voucher_action_id%TYPE,
		i_state voucher_states.state%TYPE,
		i_transition voucher_states.transition%TYPE,
		i_pay_type pay_log.pay_type%TYPE,
		i_order_id pay_log.code%TYPE,
		i_remote_addr voucher_states.remote_addr%TYPE,
		i_token_id voucher_states.token_id%TYPE,
		i_ref_types enum_pay_log_references_ref_type[],
		i_references varchar[],
		i_payment_group_id payment_groups.payment_group_id%TYPE,
		OUT o_voucher_state_id voucher_states.voucher_state_id%TYPE,
		OUT o_balance vouchers.balance%TYPE) AS $$
DECLARE
	l_current_state voucher_states.state%TYPE;
	l_current_voucher_state_id voucher_states.voucher_state_id%TYPE;
	l_amount voucher_actions.amount%TYPE;
	l_new_balance vouchers.balance%TYPE;
	l_voucher_state_id voucher_states.voucher_state_id%TYPE;
BEGIN
	SELECT
		state,
		current_state,
		amount,
		balance
	INTO
		l_current_state,
		l_current_voucher_state_id,
		l_amount,
		o_balance
	FROM
		voucher_actions
		JOIN vouchers USING (voucher_id)
	WHERE
		voucher_id = i_voucher_id AND
		voucher_action_id = i_voucher_action_id
	FOR UPDATE;
	
	IF l_current_state IS NULL AND i_state NOT IN ('used', 'unpaid') THEN
		RAISE EXCEPTION 'ERROR_NO_SUCH_ACTION';
	END IF;
	
	IF i_state IN ('paid') THEN
		-- Add the amount to balance.
		o_balance := o_balance + abs(l_amount);
		l_new_balance := o_balance;
	ELSIF i_state IN ('used') THEN
		-- Subtract the amount from balance.
		o_balance := o_balance - abs(l_amount);
		IF o_balance < 0 THEN
			RAISE EXCEPTION 'ERROR_NEGATIVE_BALANCE';
		END IF;
		l_new_balance := o_balance;
	ELSE
		l_new_balance := NULL;
	END IF;
	
	INSERT INTO
		voucher_states
		(voucher_id,
		 voucher_action_id,
		 state,
		 transition,
		 timestamp,
		 remote_addr,
		 token_id,
		 resulting_balance)
	VALUES
		(i_voucher_id,
		 i_voucher_action_id,
		 i_state,
		 i_transition,
		 now(),
		 i_remote_addr,
		 i_token_id,
		 l_new_balance)
	RETURNING
		voucher_state_id
	INTO
		o_voucher_state_id;

	-- Update the action to point to the newly added state
	UPDATE
		voucher_actions
	SET
		current_state = o_voucher_state_id,
		state = i_state
	WHERE
		voucher_id = i_voucher_id AND
		voucher_action_id = i_voucher_action_id;
	
	IF NOT FOUND THEN
		RAISE EXCEPTION 'ERROR_NO_SUCH_ACTION';
	END IF;

	-- Apply the amount, if appropriate.
	IF l_new_balance IS NOT NULL THEN
		UPDATE
			vouchers
		SET
			balance = l_new_balance
		WHERE
			voucher_id = i_voucher_id;
		PERFORM
			insert_pay_log (i_pay_type,
					i_order_id,
					abs(l_amount),
					NULL, -- paid_at
					i_ref_types,
					i_references,
					i_remote_addr,
					i_token_id,
					i_payment_group_id,
					'OK');
	END IF;
END;
$$ LANGUAGE plpgsql;
