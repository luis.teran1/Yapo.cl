CREATE OR REPLACE FUNCTION copy_voucher_action (
		i_voucher_id voucher_actions.voucher_id%TYPE,
		i_action_id voucher_actions.voucher_action_id%TYPE,
		i_schema varchar,
		OUT o_status integer
		) AS $$
BEGIN
	o_status := 0;

	-- tokens
	EXECUTE 'LOCK ' || quote_ident(i_schema) || '.tokens';

	PERFORM
		copy_token(token_id, i_schema)
	FROM
		voucher_states
	WHERE
		voucher_id = i_voucher_id
		AND voucher_action_id = i_action_id
		AND token_id IS NOT NULL;
	
	PERFORM
		copy_payment_group(payment_group_id, i_schema)
	FROM
		voucher_actions
	WHERE
		voucher_id = i_voucher_id
		AND voucher_action_id = i_action_id
		AND payment_group_id IS NOT NULL;

	-- voucher actions
	EXECUTE
		'INSERT INTO ' ||
			quote_ident(i_schema) || '.voucher_actions
			(voucher_id, voucher_action_id, action_type, current_state, state, amount, payment_group_id)
		SELECT
			voucher_id, voucher_action_id, action_type::' ||
			get_column_type (i_schema, 'voucher_actions', 'action_type') || ', current_state, state::' ||
			get_column_type (i_schema, 'voucher_actions', 'state') || ', amount, payment_group_id
		FROM
			voucher_actions
		WHERE
			voucher_id = ' || quote_literal(i_voucher_id) || '
			AND voucher_action_id = ' || quote_literal(i_action_id);

	-- voucher states
	EXECUTE
		'INSERT INTO ' ||
			quote_ident(i_schema) || '.voucher_states
			(voucher_id, voucher_action_id, voucher_state_id, state, transition, timestamp, remote_addr,
			 token_id, resulting_balance)
		SELECT
			voucher_id, voucher_action_id, voucher_state_id, state::' ||
			get_column_type (i_schema, 'voucher_states', 'state') || ', transition::' ||
			get_column_type (i_schema, 'voucher_states', 'transition') || ', timestamp, host(remote_addr::inet),
			token_id, resulting_balance
		FROM
			voucher_states
		WHERE
			voucher_id = ' || quote_literal(i_voucher_id) || '
			AND voucher_action_id = ' || quote_literal(i_action_id);

END;
$$ LANGUAGE plpgsql;
