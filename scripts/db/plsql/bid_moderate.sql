CREATE OR REPLACE FUNCTION bid_moderate(i_bid_id bid_bids.bid_id%TYPE,
					i_status bid_bids.status%TYPE,
					i_token tokens.token%TYPE,
					OUT o_set varchar
					) RETURNS SETOF VARCHAR AS $$
DECLARE
	l_bid_id	bid_bids.bid_id%TYPE;
	l_bid		bid_bids.bid%TYPE;
	l_name		bid_bids.name%TYPE;
	l_email		bid_bids.email%TYPE;
	l_campaign_id	bid_ads.campaign_id%TYPE;
	l_subject	bid_ads.subject%TYPE;
	l_bid_ad_id	bid_ads.bid_ad_id%TYPE;
	l_max_bid_id	bid_bids.bid_id%TYPE;
	l_max_bid	bid_bids.bid%TYPE;
	l_max_name	bid_bids.name%TYPE;
	l_max_email	bid_bids.email%TYPE;
	l_company	bid_bids.company%TYPE;
	l_ssecnr	bid_bids.ssecnr%TYPE;
	l_phone		bid_bids.phone%TYPE;
BEGIN
	LOCK TABLE bid_bids IN EXCLUSIVE MODE;
	
	SELECT 
		bid_bids.bid_id,
		bid_bids.bid,
		bid_bids.name,
		bid_bids.email,
		bid_ads.subject,
		bid_bids.company,
		bid_bids.ssecnr,
		bid_bids.phone
	FROM
		bid_bids JOIN bid_ads USING (bid_ad_id)
	INTO
		l_bid_id,
		l_bid,
		l_name,
		l_email,
		l_subject,
		l_company,
		l_ssecnr,
		l_phone
	WHERE
		bid_id = i_bid_id;

	IF NOT FOUND THEN
		RAISE EXCEPTION 'FACTORY_TRANSLATE:bid:ERROR_BID_NOT_FOUND';
	END IF;

	IF i_status = 'active' THEN
		SELECT
			campaign_id,
			bid_ad_id
		INTO
			l_campaign_id,
			l_bid_ad_id
		FROM
			bid_ads JOIN
			bid_bids USING (bid_ad_id)
		WHERE
			bid_bids.bid_id = i_bid_id;

		o_set := 'bid_ad_id:' || l_bid_ad_id;
		RETURN NEXT;

		SELECT
			bid_bids.bid_id,
			bid_bids.bid,
			bid_bids.name,
			bid_bids.email
		INTO
			l_max_bid_id,
			l_max_bid,
			l_max_name,
			l_max_email
		FROM
			bid_bids 
			JOIN bid_ads USING (bid_ad_id)
		WHERE
			bid_ads.campaign_id = l_campaign_id AND
			bid_ads.bid_ad_id = l_bid_ad_id AND
			status = 'active' AND
			bid_bids.bid = (SELECT MAX(bid) FROM bid_bids WHERE bid_ad_id = l_bid_ad_id AND status = 'active');

		IF NOT FOUND THEN
			l_max_bid = 0;
		ELSE
			IF  l_bid > l_max_bid THEN
				o_set := 'max_bid_id:' || l_max_bid_id::VARCHAR;
				RETURN NEXT;
				o_set := 'max_bid:' || l_max_bid::VARCHAR;
				RETURN NEXT;
				o_set := 'max_name:' || l_max_name;
				RETURN NEXT;
				o_set := 'max_email:' || l_max_email;
				RETURN NEXT;
			ELSE
				-- There's already a bid at or above this level, so we can't accept this one.
				RAISE EXCEPTION 'FACTORY_TRANSLATE:bid:ERROR_BID_TOO_SMALL';
			END IF;
		END IF;
	END IF;
	

	UPDATE
		bid_bids
	SET
		status = i_status,
		token_id = get_token_id(i_token)
	WHERE
		bid_id = i_bid_id;

	o_set := 'status:BID_UPDATED_ID_' || i_bid_id::VARCHAR;
	RETURN NEXT;
	o_set := 'bid_id:' || i_bid_id::VARCHAR;
	RETURN NEXT;
	o_set := 'bid:' || l_bid::VARCHAR;
	RETURN NEXT;
	o_set := 'name:' || l_name;
	RETURN NEXT;
	o_set := 'email:' || l_email;
	RETURN NEXT;
	o_set := 'subject:' || l_subject;
	RETURN NEXT;
	IF l_company IS NULL THEN
		o_set := 'company:';
	ELSE 
		o_set := 'company:' || l_company;
	END IF;
	RETURN NEXT;
	o_set := 'ssecnr:' ||l_ssecnr;
	RETURN NEXT;
	o_set := 'phone:' ||l_phone;
	RETURN NEXT;

END;
$$ LANGUAGE plpgsql;
