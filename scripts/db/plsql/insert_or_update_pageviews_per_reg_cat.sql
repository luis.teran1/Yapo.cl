CREATE OR REPLACE FUNCTION insert_or_update_pageviews_per_reg_cat(i_date varchar[],
		i_page_type varchar[],
		i_user_region integer[],
		i_user_category integer[],
		i_page_region integer[],
		i_page_category integer[],
		i_extra_data integer[],
		i_pageviews integer[]) RETURNS VOID AS $$
DECLARE
	l_i integer;
	l_date date;
	l_page_type varchar;
	l_user_region integer;
	l_user_category integer;
	l_page_region integer;
	l_page_category integer;
	l_extra_data integer;
	l_pageviews integer;
BEGIN
	IF i_date IS NOT NULL THEN 
		l_i := 0;
		LOOP
			
			l_date := i_date[l_i + 1]::date;
			l_page_type := i_page_type[l_i+1];
			l_user_region := i_user_region[l_i+1];
			l_user_category := i_user_category[l_i+1];
			l_page_region := i_page_region[l_i+1];
			l_page_category := i_page_category[l_i+1];
			l_extra_data := i_extra_data[l_i+1];
			l_pageviews := i_pageviews[l_i+1];
			EXIT WHEN l_date IS NULL;

			UPDATE
				pageviews_per_reg_cat
			SET
			        pageviews = l_pageviews
			WHERE
			        date = l_date AND
			        page_type =l_page_type AND
			        user_region = l_user_region AND
			        page_region = l_page_region AND
			        user_category = l_user_category AND
			        page_category = l_page_category AND
			        extra_data = l_extra_data;
						
			IF NOT FOUND THEN
				INSERT INTO
        				pageviews_per_reg_cat (
					       	date,
						page_type,
						user_region,
						page_region,
						user_category,
                				page_category,
				                extra_data,
			                	pageviews
					)
				VALUES (
				        l_date,
			        	l_page_type,
				        l_user_region,
				        l_page_region,
				        l_user_category,
				        l_page_category,
				        l_extra_data,
				        l_pageviews
				);	
			END IF;
			l_i := l_i +1;
		END LOOP;
	END IF;
END;
$$ LANGUAGE plpgsql;

