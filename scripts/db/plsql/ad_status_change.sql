CREATE OR REPLACE FUNCTION ad_status_change(
		i_ad_id ads.ad_id%TYPE,
		i_newstatus ads.status%TYPE,
		i_remote_addr action_states.remote_addr%TYPE,
		i_token_id action_states.token_id%TYPE,
		OUT o_oldstatus ads.status%TYPE,
		OUT o_action_id ad_actions.action_id%TYPE
		) AS $$
DECLARE
	l_state action_states.state%TYPE;
	l_state_id action_states.state_id%TYPE;
BEGIN
	SELECT
		status
	INTO
		o_oldstatus
	FROM
		ads
	WHERE
		ad_id = i_ad_id
	FOR UPDATE;

	IF NOT FOUND THEN
		RAISE EXCEPTION 'ERROR_AD_NOT_FOUND';
	END IF;

	IF i_newstatus = 'active' THEN
		IF o_oldstatus != 'hidden' AND o_oldstatus != 'disabled' THEN
			RAISE EXCEPTION 'ERROR_INVALID_STATUS';
		END IF;
		l_state := 'accepted';

	ELSIF i_newstatus = 'disabled' THEN
		IF o_oldstatus != 'active' AND o_oldstatus != 'deactivated' THEN
			RAISE EXCEPTION 'ERROR_INVALID_STATUS';
		END IF;
		l_state := 'disabled';
	ELSE
		IF o_oldstatus != 'active' THEN
			RAISE EXCEPTION 'ERROR_INVALID_STATUS';
		END IF;
		l_state := i_newstatus;
	END IF;

	o_action_id := insert_ad_action(i_ad_id, 'status_change', NULL);
	l_state_id := insert_state(i_ad_id, o_action_id, 'reg', 'initial', i_remote_addr, i_token_id);

	-- This control PACK STATUS param
	IF i_newstatus = 'disabled' OR ( i_newstatus = 'active' AND o_oldstatus = 'disabled' ) THEN
		-- add addition of gallery ad_params to ad_changes
		PERFORM insert_ad_change(i_ad_id, o_action_id, l_state_id, true, 'pack_status', i_newstatus::varchar);

		-- apply ad_changes
		PERFORM apply_ad_changes(i_ad_id, o_action_id, NULL);
	END IF;

	UPDATE
		ads
	SET
		status = i_newstatus,
		modified_at = CURRENT_TIMESTAMP
	WHERE
		ad_id = i_ad_id;

	PERFORM insert_state(i_ad_id, o_action_id, l_state, 'status_change', i_remote_addr, i_token_id);
END;
$$ LANGUAGE plpgsql;
