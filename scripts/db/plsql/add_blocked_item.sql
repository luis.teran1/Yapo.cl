CREATE OR REPLACE FUNCTION add_blocked_item(i_item_id blocked_items.item_id%TYPE,
					  i_value blocked_items.value%TYPE,
					  i_list_id block_lists.list_id%TYPE,
					  i_notice blocked_items.notice%TYPE,
					  i_token tokens.token%TYPE) RETURNS bool AS $$
BEGIN
	IF COALESCE(i_notice = '', true) THEN
		IF EXISTS(SELECT
				*
			FROM
				block_lists
			WHERE
				list_id = i_list_id
				AND list_type IN ('ip', 'email')) THEN
			RAISE EXCEPTION 'ERROR_NOTICE_REQUIRED';
		END IF;
	END IF;

	-- Check if the item already exists and is unchanged.
	IF EXISTS(
		SELECT
			*
		FROM
			blocked_items
		WHERE
			COALESCE(item_id = i_item_id, true)
			AND value = i_value
			AND list_id = i_list_id
			AND notice = i_notice
		) THEN
		RETURN false;
	END IF;

	-- Delete current item if id is given
	IF i_item_id IS NOT NULL THEN
		DELETE FROM blocked_items WHERE item_id = i_item_id;
	END IF;


	-- Check if value already exists, update it
	UPDATE
		blocked_items
	SET
		notice = i_notice,
		token_id = get_token_id(i_token)
	WHERE
		list_id = i_list_id AND
		value = i_value;

	-- Item doesn't exist, add it item
	IF NOT FOUND THEN
		INSERT INTO blocked_items (value, list_id, token_id, notice) VALUES (i_value, i_list_id, get_token_id(i_token), i_notice);
	END IF;

	RETURN true;
END;
$$ LANGUAGE plpgsql;
