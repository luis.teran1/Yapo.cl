CREATE OR REPLACE FUNCTION get_newad_event_extra_info(
	i_ad_id ads.ad_id%TYPE,
	i_action_id ad_actions.action_id%TYPE,
	OUT o_ad_status ads.status%TYPE,
	OUT o_user_id ads.user_id%TYPE,
	OUT o_creation_date action_states.timestamp%TYPE,
	OUT o_remote_addr action_states.remote_addr%TYPE,
	OUT o_state ad_actions.state%TYPE,
	OUT o_action_type ad_actions.action_type%TYPE,
	OUT o_paid boolean
) AS $$
BEGIN
	-- retrieve the ad status and user id
	SELECT
		status,
		user_id
	INTO
		o_ad_status,
		o_user_id
	FROM
		ads
	WHERE
		ad_id = i_ad_id;
	-- retrieve the creation date
	SELECT
		(timestamp AT TIME ZONE 'america/santiago') AT TIME ZONE 'UTC'
	INTO
		o_creation_date
	FROM
		action_states
	WHERE
		ad_id = i_ad_id
	AND
		state = 'reg'
	AND
		transition = 'initial';
	-- check if the action is a payment and has been accepted
	SELECT
		ad_actions.state,
		action_type,
		remote_addr
	INTO
		o_state,
		o_action_type,
		o_remote_addr
	FROM
		ad_actions
	LEFT JOIN
		action_states
	USING
		(ad_id, action_id)
	WHERE
		ad_id = i_ad_id
	AND
		action_id = i_action_id
	AND
		current_state = action_states.state_id;

	IF 
		o_state = 'accepted'
	AND
		o_action_type IN (
			'bump',
			'autobump',
			'gallery',
			'gallery_1',
			'gallery_30',
			'weekly_bump',
			'daily_bump',
			'upselling_bump',
			'upselling_daily_bump',
			'upselling_weekly_bump',
			'upselling_gallery',
			'upselling_label',
			'label',
			'upselling_standard_cars',
			'upselling_advanced_cars',
			'upselling_premium_cars',
			'upselling_standard_inmo',
			'upselling_advanced_inmo',
			'upselling_premium_inmo',
			'upselling_standard_others',
			'upselling_advanced_others',
			'upselling_premium_others',
			'autofact',
			'inserting_fee',
			'if_pack_car',
			'if_pack_inmo',
			'at_combo1',
			'at_combo2',
			'at_combo3'
		)
	THEN
		o_paid := true;
	ELSE
		o_paid :=false;
	END IF;
END;
$$ LANGUAGE plpgsql;
