CREATE OR REPLACE FUNCTION score_duplicate_ad(i_source_ad ads.ad_id%TYPE, i_other_ad ads.ad_id%TYPE) 
RETURNS FLOAT AS $$
DECLARE
	l_source_ad RECORD;
	l_other_ad RECORD; 
	l_param RECORD;
	l_o_param RECORD;
	l_score FLOAT;

	l_brand ad_params.value%TYPE;
	l_model ad_params.value%TYPE;
	l_fuel ad_params.value%TYPE;
	l_regdate ad_params.value%TYPE;
	
	l_o_brand ad_params.value%TYPE;
	l_o_model ad_params.value%TYPE;
	l_o_fuel ad_params.value%TYPE;
	l_o_regdate ad_params.value%TYPE;
	
	l_area ad_params.value%TYPE;
	l_rooms ad_params.value%TYPE;
	l_size ad_params.value%TYPE;

	l_o_area ad_params.value%TYPE;
	l_o_rooms ad_params.value%TYPE;
	l_o_size ad_params.value%TYPE;
BEGIN
	l_score = 0;
	
	SELECT * INTO l_source_ad FROM ads WHERE ad_id = i_source_ad;
	SELECT * INTO l_other_ad FROM ads WHERE ad_id = i_other_ad;

	IF l_source_ad.category != l_other_ad.category THEN
		l_score := l_score - 50;
	END IF;
	
	l_score := l_score + score_string(l_source_ad.subject, l_other_ad.subject)*0.25;
	l_score := l_score + score_string(l_source_ad.body, l_other_ad.body)*0.5;
	
	IF l_source_ad.category >= 2000 THEN
		FOR l_param IN SELECT * FROM ad_params WHERE ad_id = i_source_ad LOOP
			SELECT * INTO l_o_param FROM ad_params WHERE ad_id = l_param.ad_id AND name = l_param.name;
			IF  l_o_param.value = l_param.value THEN
				l_score := l_score + 5;
			END IF;
		END LOOP;
	ELSE
		-- The code below came from custojusto and is not applyed to us right now, but it could be used as a base to improve the score system
		--IF l_source_ad.category >= 2000 AND l_source_ad.category < 3000 THEN
			-- If it's a car
			--SELECT value INTO l_brand	FROM ad_params WHERE ad_id = i_source_ad AND name = 'brand';
			--SELECT value INTO l_model	FROM ad_params WHERE ad_id = i_source_ad AND name = 'model';
			--SELECT value INTO l_fuel 	FROM ad_params WHERE ad_id = i_source_ad AND name = 'fuel';
			--SELECT value INTO l_regdate FROM ad_params WHERE ad_id = i_source_ad AND name = 'regdate';

			--SELECT value INTO l_o_brand	  FROM ad_params WHERE ad_id = i_other_ad AND name = 'brand';
			--SELECT value INTO l_o_model	  FROM ad_params WHERE ad_id = i_other_ad AND name = 'model';
			--SELECT value INTO l_o_fuel 	  FROM ad_params WHERE ad_id = i_other_ad AND name = 'fuel';
			--SELECT value INTO l_o_regdate FROM ad_params WHERE ad_id = i_other_ad AND name = 'regdate';

			--IF l_brand != l_o_brand AND l_model != l_o_model THEN
			--	l_score := l_score - 20;
			--END IF;
			--IF l_brand = l_o_brand AND l_model = l_o_model AND l_regdate != l_regdate THEN
			--	l_score := l_score - 5;
			--END IF;
			--IF l_brand = l_o_brand AND l_model = l_o_model AND l_fuel != l_fuel THEN
			--	l_score := l_score - 5;
			--END IF;
			--IF l_brand = l_o_brand AND l_model != l_o_model THEN
			--	l_score := l_score - 10;
			--END IF;
		--ELSE
			-- If it's a apartment
			--SELECT value INTO l_area  FROM ad_params WHERE ad_id = i_source_ad AND name = 'area';
			SELECT value INTO l_rooms FROM ad_params WHERE ad_id = i_source_ad AND name = 'rooms';

			--SELECT value INTO l_o_area  FROM ad_params WHERE ad_id = i_other_ad AND name = 'area';
			SELECT value INTO l_o_rooms FROM ad_params WHERE ad_id = i_other_ad AND name = 'rooms';
			
			--IF l_area = l_o_area THEN
			--	l_score := l_score + 20;
			--ELSE
			--	l_score := l_score - 10;
			--END IF;
			
			IF l_rooms = l_o_rooms THEN
				l_score := l_score + 10;
			ELSE
				l_score := l_score - 5;
			END IF;
		--END IF;
	END IF;
	
	return l_score;
END
$$ LANGUAGE plpgsql;
