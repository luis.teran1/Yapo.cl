CREATE OR REPLACE FUNCTION scarface_multiple_resolve_get_users_to_warn(i_list_id ads.list_id%TYPE)
RETURNS SETOF RECORD AS $$
DECLARE
	l_schema varchar;

BEGIN
	l_schema := 'blocket_' || extract(year from CURRENT_TIMESTAMP);

	RETURN QUERY EXECUTE '
		SELECT
			sender_email, ad_id
		FROM
			' || quote_ident(l_schema) || '.mail_queue
		JOIN
			ads USING(list_id)
		WHERE
			list_id = ' || i_list_id
	;
END;
$$ LANGUAGE plpgsql;
