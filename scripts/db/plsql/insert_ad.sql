CREATE OR REPLACE FUNCTION insert_ad(
	i_email users.email%TYPE,
	i_pay_type varchar,
	i_cat_price payments.pay_amount%TYPE,
	i_extra_images_price payments.pay_amount%TYPE,
	i_gallery_price payments.pay_amount%TYPE,
	i_redir_code redir_stats.code%TYPE,
	i_previous_action_id ad_actions.action_id%TYPE,
	i_remote_addr action_states.remote_addr%TYPE,
	i_ads_name ads.name%TYPE,
	i_ads_phone ads.phone%TYPE,
	i_ads_region ads.region%TYPE,
	i_ads_city ads.city%TYPE,
	i_ads_type ads.type%TYPE,
	i_ads_category ads.category%TYPE,
	i_ads_passwd ads.salted_passwd%TYPE,
	i_uid users.uid%TYPE,
	i_store_id ads.store_id%TYPE,
	i_phone_hidden ads.phone_hidden%TYPE,
	i_company_ad ads.company_ad%TYPE,
	i_ads_subject ads.subject%TYPE,
	i_ads_body ads.body%TYPE,
	i_ads_price ads.price%TYPE,
	i_ads_infopage ads.infopage%TYPE,
	i_ads_infopage_title ads.infopage_title%TYPE,
	i_ad_images varchar[],
	i_ad_thumb_digest varchar[],
	i_ad_image_had_digest bool[],
	i_ad_params varchar[][],
	i_token tokens.token%TYPE,
	i_action_type ad_actions.action_type%TYPE,
	i_action_params varchar[][],
	i_list_id ads.list_id%TYPE,
	i_ad_id ads.ad_id%TYPE,
	i_static_params text[],
	i_lang varchar,
	i_category_edition_limits integer[][],
	OUT o_ad_id ads.ad_id%TYPE,
	OUT o_action_id ad_actions.action_id%TYPE,
	OUT o_list_time ads.list_time%TYPE,
	OUT o_orig_list_time ads.orig_list_time%TYPE,
	OUT o_modified_at ads.modified_at%TYPE,
	OUT o_paycode payment_groups.code%TYPE,
	OUT o_uid users.uid%TYPE,
	OUT o_account users.account%TYPE,
	OUT o_prepaid integer,
	OUT o_user_id users.user_id%TYPE,
	OUT o_is_new_user integer,
	OUT o_payment_group_id payment_groups.payment_group_id%TYPE,
	OUT o_action_type ad_actions.action_type%TYPE) AS $$
DECLARE
	l_uid users.uid%TYPE;
	l_i integer;
	l_ad_image_name varchar;
	l_ad_param_name ad_params.name%TYPE;
	l_ad_param_value ad_params.value%TYPE;
	l_action_param_name action_params.name%TYPE;
	l_action_param_value action_params.value%TYPE;
	l_action_id ad_actions.action_id%TYPE;
	l_state action_states.state%TYPE;
	l_transition action_states.transition%TYPE;
	l_queue ad_actions.queue%TYPE;
	l_code_type ad_codes.code_type%TYPE;
	l_paycode ad_codes.code%TYPE;
	l_prepaid integer;
	l_account users.account%TYPE;
	l_ad_id ads.ad_id%TYPE;
	l_state_id action_states.state_id%TYPE;
	l_ref_types enum_pay_log_references_ref_type[];
	l_references varchar[];
	l_voucher vouchers.balance%TYPE;
	l_transfer_ad_id ads.ad_id%TYPE;
	l_cat_price_discount payments.discount%TYPE;
	l_extra_images_price_discount payments.discount%TYPE;
	l_gallery_price_discount payments.discount%TYPE;
	l_ad_image_id bigint;
	l_temp_ad_id ads.ad_id%TYPE;
	l_temp_ad_status ads.status%TYPE;
	l_ad_status ads.status%TYPE;
	l_skip_media_update boolean;
	l_user_id users.user_id%TYPE;
	l_value user_params.value%TYPE;
BEGIN
	l_queue = 'preprocessing';
	IF i_action_type = 'edit' AND i_token IS NOT NULL AND get_auth_type(i_token) = 'admin' THEN
		o_action_type = 'adminedit';
	ELSE
		o_action_type = i_action_type;
	END IF;

	IF i_store_id IS NOT NULL AND i_pay_type != 'verify' AND i_action_type != 'adminedit' THEN
		IF i_token IS NULL OR get_auth_type(i_token) != 'store' THEN
			RAISE EXCEPTION 'ERROR_STORE_NOT_LOGGED_IN';
		END IF;
	END IF;

	-- check user id
	SELECT
		*
	INTO 
		l_account, o_user_id, l_uid 
	FROM 
		get_user_by_email(i_email);

	l_prepaid := 0;
	o_is_new_user := 0;
	-- existing user (email)
	IF i_pay_type = 'voucher' AND o_action_type NOT IN ('adminedit', 'import') THEN
		IF i_token IS NULL OR get_auth_type(i_token) != 'store' THEN
			RAISE NOTICE 'NOT_GOOD_TOKEN';
			RAISE EXCEPTION 'ERROR_PAY_TYPE_INVALID';
		END IF;

		SELECT
			balance
		INTO
			l_voucher
		FROM
			vouchers
		WHERE
			store_id = i_store_id;

		IF NOT FOUND OR l_voucher < i_cat_price + i_extra_images_price + i_gallery_price THEN
			RAISE NOTICE 'NOT_ENOUGH_VOUCHER';
			RAISE EXCEPTION 'ERROR_PAY_TYPE_INVALID';
		END IF;
	END IF;

	IF i_pay_type = 'campaign' THEN
		IF o_action_type NOT IN ('new', 'editrefused') THEN
			RAISE EXCEPTION 'ERROR_PAY_TYPE_INVALID';
		END IF;
	END IF;

	-- Getting the real uid for partners inserting by API
	IF i_action_type = 'import' AND i_ad_params IS NOT NULL THEN
		l_queue = 'normal';
		l_i := 1;
		LOOP
			IF i_ad_params[l_i][1] = 'link_type' THEN
				l_uid := get_uid_by_link_type(i_ad_params[l_i][2]);
				EXIT;
			END IF;
			l_i := l_i + 1;
		END LOOP;
	END IF;

	IF o_user_id IS NOT NULL THEN
		IF o_action_type NOT IN ('adminedit', 'import') THEN
			IF i_pay_type = 'verify' THEN
				IF l_account < i_cat_price + i_extra_images_price + i_gallery_price THEN
					RAISE NOTICE 'NOT_ENOUGH';
					RAISE EXCEPTION 'ERROR_PAY_TYPE_INVALID';
				END IF;
				l_account := l_account - (i_cat_price + i_extra_images_price + i_gallery_price);
				l_prepaid := 1;
				
				UPDATE
					users
				SET
					account = l_account
				WHERE
					user_id = o_user_id;
			END iF;
		END IF;

		-- here I should set the pre_first_inserted_ad or first_inserted_ad flag if the user does not have the flag with other user_id with the same uid
		IF i_action_type = 'new' THEN
			SELECT
				uid
			INTO
				l_uid
			FROM
				users
			WHERE
				user_id = o_user_id;

			SELECT
				value
			INTO
				l_value
			FROM
				users u
			INNER JOIN
				user_params up USING (user_id)
			WHERE
				u.uid = l_uid AND up.name = 'first_inserted_ad';

			IF NOT FOUND THEN
				IF EXISTS( SELECT
						*
					FROM
						user_params
					WHERE
						user_id = o_user_id AND name = 'pre_first_inserted_ad')
				THEN
					UPDATE
						user_params
					SET
						value = NOW()
					WHERE
						user_id = o_user_id AND name = 'pre_first_inserted_ad';
				ELSE
					INSERT INTO
						user_params (user_id, name, value)
					VALUES
						(o_user_id, 'pre_first_inserted_ad', NOW());
				END IF;
			END IF;
		END IF;

	ELSIF i_pay_type IN ('verify') THEN
		RAISE NOTICE 'NOT_EXISTING_USER';
		RAISE EXCEPTION 'ERROR_PAY_TYPE_INVALID';

	-- not existing user, but has uid (from cookie)
	ELSIF i_uid IS NOT NULL THEN
		SELECT
			uid
		INTO
			l_uid
		FROM
			users
		WHERE
			uid = i_uid;

		-- Don't use fake UIDs
		IF NOT FOUND THEN
			l_uid := NEXTVAL('uid_seq');
		END IF;

		INSERT INTO
			users
			(uid,
			 email)
		VALUES
			(l_uid,
			 i_email);

		l_user_id := CURRVAL('users_user_id_seq');

		-- here I should set the pre_first_inserted_ad or first_inserted_ad flag if the user does not have the flag with other user_id with the same uid
		IF i_action_type = 'new' THEN
			SELECT
				value
			INTO
				l_value
			FROM
				users u
			INNER JOIN
				user_params up USING (user_id)
			WHERE
				u.uid = l_uid AND up.name = 'first_inserted_ad';

			IF NOT FOUND THEN
				INSERT INTO
					user_params (user_id, name, value)
				VALUES
					(l_user_id, 'pre_first_inserted_ad', NOW());
			ELSE
				INSERT INTO
					user_params (user_id, name, value)
				VALUES
					(l_user_id, 'first_inserted_ad', l_value);
			END IF;
		END IF;

	-- new user
	ELSE 
		IF l_uid IS NULL THEN
			l_uid := NEXTVAL('uid_seq');
		END IF;

		INSERT INTO
			users
			(uid,
			 email)
		VALUES	 
			(l_uid,
			i_email);

		l_user_id := CURRVAL('users_user_id_seq');

		-- set first_ad flag
		IF i_action_type = 'new' THEN
			INSERT INTO
				user_params (user_id, name, value)
			VALUES
				(l_user_id, 'pre_first_inserted_ad', NOW());
		END IF;


	END IF;

	IF o_user_id IS NULL THEN
		o_user_id := CURRVAL('users_user_id_seq');
		o_is_new_user := 1;
	END IF;

	-- check for existing ad - either by list_id or ad_id
	
	IF i_list_id IS NOT NULL OR i_ad_id IS NOT NULL THEN
		
		SELECT
			ad_id,
			list_time,
			orig_list_time,
			modified_at,
			status
		INTO
			l_ad_id,
			o_list_time,
			o_orig_list_time,
			o_modified_at,
			l_ad_status
		FROM
			ads
		WHERE
			CASE WHEN i_ad_id IS NOT NULL THEN
				ad_id = i_ad_id
			ELSE
				list_id = i_list_id
			END;

		IF NOT FOUND THEN
			RAISE EXCEPTION 'ERROR_ORIG_AD_NOT_FOUND';
		END IF;

		IF o_action_type NOT IN ('adminedit') AND NOT can_edit_ad(l_ad_id, i_category_edition_limits) THEN
			RAISE EXCEPTION 'ERROR_TOO_OLD_TO_EDIT';
		END IF;

		IF o_action_type NOT IN ('adminedit', 'import') AND l_ad_status IN ('inactive', 'unpublished', 'hidden', 'deleted', 'deactivated') THEN
			RAISE EXCEPTION 'ERROR_ORIGINAL_AD_DEACTIVATED';
		END IF;

		SELECT
			iaas.o_action_id,
			iaas.o_state_id
		INTO
			l_action_id,
			l_state_id
		FROM
			insert_ad_action_state(
				     CAST(l_ad_id AS integer),
				     o_action_type,
				     'reg', 
				     'initial', 
				     i_remote_addr, 
				     get_token_id(i_token)
			) AS iaas;

		PERFORM insert_all_ad_changes(l_ad_id,
					      l_action_id,
					      l_state_id,
					      i_ads_name,
					      i_ads_phone,
					      i_ads_region,
					      i_ads_city,
					      i_ads_type,
					      i_ads_category,
					      i_ads_passwd,
					      o_user_id,
					      i_store_id,
					      i_phone_hidden,
					      i_company_ad,
					      i_ads_subject,
					      i_ads_body,
					      i_ads_price,
					      i_ads_infopage,
					      i_ads_infopage_title,
					      i_ad_images,
					      i_ad_thumb_digest,
					      i_ad_image_had_digest,
					      i_ad_params,
					      o_action_type::text,
					      i_static_params,
					      i_lang);
						
	ELSE -- new ad
		-- Insert the ad
	
		INSERT 
		INTO 
			ads
			(name,
			 phone,
			 region,
			 city,
			 type,
			 category,
			 salted_passwd,
			 user_id,
			 phone_hidden,
			 company_ad,
			 subject,
			 body,
			 price,
			 infopage,
			 infopage_title, 
			 store_id,
			 lang) 
		VALUES
			(i_ads_name,
			 i_ads_phone,
			 i_ads_region,
			 i_ads_city,
			 i_ads_type,
			 i_ads_category,
			 i_ads_passwd,
			 o_user_id,
			 i_phone_hidden,
			 i_company_ad,
			 i_ads_subject,
			 i_ads_body,
			 i_ads_price,
			 i_ads_infopage,
			 i_ads_infopage_title, 
			 i_store_id,
			 i_lang);
		
		l_ad_id := CURRVAL('ads_ad_id_seq');

		-- Add images
		IF i_ad_images IS NOT NULL THEN
			l_i := 0;
	
			LOOP
				l_ad_image_name := i_ad_images[l_i + 1];
				l_skip_media_update := FALSE;
				EXIT WHEN l_ad_image_name IS NULL;

				l_ad_image_id := TRIM(trailing '.jpg' FROM l_ad_image_name)::bigint;

				SELECT
					ad_media.ad_id,
					ads.status
				FROM
					ad_media
				LEFT JOIN
					ads USING (ad_id)
				WHERE
					ad_media_id = l_ad_image_id
				INTO
					l_temp_ad_id,
					l_temp_ad_status;

				-- we only let the user update empty entries in ad_media or the current ad entries
				IF (l_temp_ad_id IS NOT NULL AND l_temp_ad_id != l_ad_id) THEN
					RAISE NOTICE 'ERROR_INVALID_UPDATE_TO_ADMEDIA';
					IF l_temp_ad_status = 'active' THEN
						l_skip_media_update := TRUE;
					END IF;
				END IF;

				IF l_skip_media_update = FALSE THEN
					UPDATE
						ad_media
					SET
						ad_id = l_ad_id,
						seq_no = l_i
					WHERE
						ad_media_id = l_ad_image_id;
				END IF;

	
				IF COALESCE(length(i_ad_thumb_digest[l_i + 1]), 0) = 32 THEN

					INSERT 
					INTO
						ad_images_digests
						(name,
						digest,
						ad_id,
						previously_inserted,
						uid)
					VALUES
						(i_ad_images[l_i + 1],
						i_ad_thumb_digest[l_i + 1],
						l_ad_id,
						i_ad_image_had_digest[l_i + 1],
						l_uid);
				END IF;
	
				l_i := l_i + 1;
	
			END LOOP;
		END IF;

		-- Add ad_params
		IF i_ad_params IS NOT NULL THEN
			l_i := 1;
		
			LOOP
				l_ad_param_name := i_ad_params[l_i][1];
				l_ad_param_value := i_ad_params[l_i][2];
				EXIT WHEN l_ad_param_name IS NULL;

				INSERT INTO
					ad_params
					(ad_id,
					 name,
					 value)
				VALUES
					(l_ad_id,
					l_ad_param_name,
					replace(l_ad_param_value, E'\'\'', E'\''));
		
				l_i := l_i + 1;
			END LOOP;
		END IF;

		-- Create ad_action
		l_action_id := 1;
	
		INSERT INTO
			ad_actions
			(ad_id,
			 action_id,
			 action_type,
			 queue)
		VALUES
			(l_ad_id,
			 l_action_id,
			 o_action_type,
			 l_queue);
		PERFORM insert_state(CAST(l_ad_id AS integer),
				     l_action_id, 
				     'reg', 
				     'initial', 
				     i_remote_addr, 
				     get_token_id(i_token));
	END IF; -- new ad

	-- Add action_params
	IF i_action_params IS NOT NULL THEN
		l_i := 1;
	
		LOOP
			l_action_param_name := i_action_params[l_i][1];
			l_action_param_value := i_action_params[l_i][2];
			EXIT WHEN l_action_param_name IS NULL;

			INSERT INTO
				action_params
				(ad_id,
				 action_id,
				 name,
				 value)
			VALUES
				(l_ad_id,
				 l_action_id,
 				 l_action_param_name,
	 			 l_action_param_value);
		
			l_i := l_i + 1;
		END LOOP;
	END IF;

	-- Redir action param
	IF i_redir_code IS NOT NULL THEN
		-- insert into action_params
		INSERT INTO
			action_params
			(ad_id,
			 action_id,
			 name,
			 value)
		VALUES
			(l_ad_id,
			 l_action_id,
			 'redir',
			 i_redir_code);
	END IF;


	IF o_action_type NOT IN ('adminedit', 'import') THEN
		-- Create 'unpaid' or 'unverified' ad_action
		IF i_pay_type = 'phone' THEN
			l_state := 'unpaid';
			l_transition := 'newad';
			l_code_type := 'pay';
		ELSIF i_pay_type = 'card' THEN
			l_state := 'unpaid';
			l_transition := 'newad';
			l_code_type := 'pay';
		ELSIF i_pay_type = 'voucher' THEN
			l_state := 'unpaid';
			l_transition := 'newad';
			l_code_type := NULL;
		ELSIF i_pay_type = 'campaign' THEN
			IF o_action_type = 'editrefused' THEN
				l_state = 'unverified';
			ELSE
				l_state := 'unpaid';
			END IF;
			l_transition := 'newad';
			l_code_type := NULL;
			l_cat_price_discount := i_cat_price;
			l_extra_images_price_discount := i_extra_images_price;
			l_gallery_price_discount := i_gallery_price;
		ELSE
			l_state := 'unverified';
			l_transition := 'verifymail';
			l_code_type := 'verify';
		END IF;
	
		PERFORM insert_state(CAST(l_ad_id AS integer), 
				     l_action_id, 
				     l_state, 
				     l_transition, 
				     i_remote_addr, 
				     get_token_id(i_token));
	
		IF l_code_type IS NULL THEN
			l_paycode := NULL;
		ELSE
			l_paycode := get_ad_code(l_code_type);
		END IF;

		-- Create new payment
		INSERT INTO payment_groups (
			code,
			status
		) VALUES (
			l_paycode,
			(l_state::text)::enum_payment_groups_status
		);
		-- XXX This is where we want to insert a value discount for certain ads (US1575)
		INSERT INTO payments (
			payment_group_id,
			payment_type,
			pay_amount,
			discount
		) VALUES (
			CURRVAL('payment_groups_payment_group_id_seq'),
			'ad_action',
			i_cat_price,
			COALESCE(l_cat_price_discount, 0)
		);
		o_payment_group_id := CURRVAL('payment_groups_payment_group_id_seq');
		UPDATE
			ad_actions
		SET
			payment_group_id = o_payment_group_id
		WHERE
			ad_id = l_ad_id AND
			action_id = l_action_id;
			
		IF i_extra_images_price > 0 THEN
			INSERT INTO
				payments
				(payment_group_id,
			 	payment_type,
			 	pay_amount,
				discount)
			VALUES
				(o_payment_group_id,
			 	'extra_images',
			 	i_extra_images_price,
				COALESCE(l_extra_images_price_discount, 0)
				);
		END IF;

		IF i_gallery_price > 0 THEN
			INSERT INTO
				payments
				(payment_group_id,
			 	payment_type,
			 	pay_amount,
				discount)
			VALUES
				(o_payment_group_id,
			 	'gallery',
			 	i_gallery_price,
				COALESCE(l_gallery_price_discount, 0)
				);
		END IF;

		l_references := string_to_array(i_ads_phone, ',');
		IF l_references IS NOT NULL THEN 
			FOR l_i IN 1 .. array_upper(l_references, 1) LOOP
				l_ref_types[l_i] := 'adphone';
			END LOOP;
		END IF;
		PERFORM insert_pay_log('save', l_paycode::varchar, 0, NULL, l_ref_types, l_references,
				       i_remote_addr, NULL, o_payment_group_id, 'SAVE');
	END IF;
	
	o_ad_id := l_ad_id;
	o_action_id := l_action_id;
	o_uid := l_uid;
	o_prepaid := l_prepaid;
	o_account := l_account;
	o_paycode := l_paycode;
END;
$$ LANGUAGE plpgsql;
