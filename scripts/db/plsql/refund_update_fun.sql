CREATE OR REPLACE FUNCTION refund_update_fun(
	i_refund_id			refunds.refund_id%TYPE,
	i_status				refunds.status%TYPE
) RETURNS varchar AS $$
DECLARE
BEGIN
	UPDATE
		refunds
	SET
		status = i_status
	WHERE
		refund_id = i_refund_id;
	IF NOT FOUND THEN
		RAISE EXCEPTION 'FACTORY_TRANSLATE:error:ERROR_REFUND_NOT_FOUND';
	END IF;
	return i_status;
END;
$$ LANGUAGE plpgsql;
