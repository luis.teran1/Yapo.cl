CREATE OR REPLACE FUNCTION update_hourly_stats(i_stat_time stats_hourly.stat_time%TYPE,
					      i_stat_name stats_hourly.name%TYPE,
					      i_stat_value stats_hourly.value%TYPE,
					      i_is_delta bool,
					      OUT o_value stats_hourly.value%TYPE) AS $$
DECLARE
	l_prev_value stats_hourly.value%TYPE;
	l_new_value stats_hourly.value%TYPE;
	l_stat_time stats_hourly.stat_time%TYPE;
BEGIN
	l_new_value := i_stat_value;

	IF i_stat_time IS NULL THEN
		l_stat_time := CURRENT_TIMESTAMP;
	ELSE
		l_stat_time := i_stat_time;
	END IF;

	l_stat_time := date_trunc('hour', l_stat_time);

	SELECT 
		value
	INTO
		l_prev_value
	FROM
		stats_hourly
	WHERE
		stat_time = l_stat_time AND
		name = i_stat_name;

	IF FOUND THEN
		IF i_is_delta THEN
			l_new_value := l_prev_value::integer + i_stat_value::integer;
		END IF;

		UPDATE
			stats_hourly
		SET
			value = l_new_value
		WHERE	
			stat_time = l_stat_time AND
			name = i_stat_name;
	ELSE
		INSERT INTO 
			stats_hourly
		VALUES
			(	
				l_stat_time,
				i_stat_name,
				i_stat_value
			);
	END IF;
	o_value = l_new_value;
END;
$$ LANGUAGE plpgsql;
