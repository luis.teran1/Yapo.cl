CREATE OR REPLACE FUNCTION count_admin_actions(i_transition action_states.transition%TYPE,
					       i_queue ad_actions.queue%TYPE,
					       i_start_time action_states.timestamp%TYPE,
					       i_end_time action_states.timestamp%TYPE,
					       i_state action_states.state%TYPE,
					       OUT o_count integer,
					       OUT o_username admins.username%TYPE) RETURNS SETOF RECORD AS $$
DECLARE
	l_rec record;
BEGIN
	IF i_queue IS NULL THEN
		FOR 
			l_rec
		IN
			SELECT 
				COUNT(*), 
				username 
			FROM 
				action_states JOIN
				tokens USING (token_id) JOIN
				admins USING (admin_id)
			WHERE 
				timestamp > i_start_time AND 
				timestamp < i_end_time AND 
				transition = i_transition AND
				state = i_state
		 	GROUP BY 
				username 
			ORDER BY 
				count DESC
		LOOP
			o_count := l_rec.count;
			o_username := l_rec.username;
			RETURN NEXT;
		END LOOP;
	ELSE
		FOR 
			l_rec
		IN
			SELECT 
				COUNT(*),
				username
			FROM 
				ad_actions JOIN
				action_states USING (ad_id, action_id) JOIN
				tokens USING (token_id) JOIN
				admins USING (admin_id)
			WHERE 
				timestamp > i_start_time AND 
				timestamp < i_end_time AND 
				transition = i_transition AND
				action_states.state = i_state AND
				queue = i_queue
 			GROUP BY 
				username 
			ORDER BY 
				count DESC
		LOOP
			o_count := l_rec.count;
			o_username := l_rec.username;
			RETURN NEXT;
		END LOOP;
	END IF;
END;
$$ LANGUAGE plpgsql;
