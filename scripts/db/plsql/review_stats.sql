CREATE OR REPLACE FUNCTION review_stats(i_start_time action_states.timestamp%TYPE,
					i_end_time action_states.timestamp%TYPE,
					OUT o_review_manual_accepted integer,
					OUT o_review_auto_accepted integer,
					OUT o_review_total_accepted integer,
					OUT o_review_accepted_paid_new integer,
					OUT o_review_accepted_paid_edit integer,
					OUT o_review_accepted_paid_extra_images integer,
					OUT o_review_accepted_paid_video integer,
					OUT o_review_accepted_paid_editrefused integer,
					OUT o_review_accepted_verified integer,
					OUT o_review_accepted_cleared integer,
					OUT o_review_manual_refused integer,
					OUT o_review_auto_refused integer,
					OUT o_review_total_refused integer,
					OUT o_review_refused_new integer,
					OUT o_review_refused_edit integer,
					OUT o_review_refused_renew integer,
					OUT o_review_refused_editrefused integer,
					OUT o_review_refused_misc integer) AS $$
BEGIN
	SELECT 
		COUNT(*) 
	INTO
		o_review_total_accepted
	FROM 
		v_action_states_current 
	WHERE 
		timestamp > i_start_time AND 
		timestamp < i_end_time AND 
		state = 'accepted' AND
		transition = 'accept';

	SELECT 
		COUNT(*) 
	INTO
		o_review_total_refused
	FROM 
		v_action_states_current 
	WHERE 
		timestamp > i_start_time AND 
		timestamp < i_end_time AND 
		state = 'refused' AND
		transition = 'refuse';

	SELECT
		COUNT(*)
	INTO
		o_review_auto_accepted
	FROM
		v_action_states_current
	WHERE 
		timestamp > i_start_time AND 
		timestamp < i_end_time AND
		state = 'accepted' AND
		transition = 'accept' AND 
		token_id IS NULL;

	SELECT
		COUNT(*)
	INTO
		o_review_auto_refused
	FROM
		v_action_states_current
	WHERE 
		timestamp > i_start_time AND 
		timestamp < i_end_time AND
		state = 'refused' AND
		transition = 'refuse' AND 
		token_id IS NULL;

	o_review_manual_accepted := o_review_total_accepted - o_review_auto_accepted;
	o_review_manual_refused := o_review_total_refused - o_review_auto_refused;

	SELECT 
		COUNT(*) 
	INTO
		o_review_accepted_paid_new
	FROM 
		v_aa_as_pg_current
	WHERE
		timestamp > i_start_time AND 
		timestamp < i_end_time AND
		state = 'accepted' AND
		transition = 'accept' AND 
		status = 'paid' AND 
		action_type in ('new', 'renew', 'prolong') AND
		token_id IS NOT NULL;

	SELECT 
		COUNT(*) 
	INTO
		o_review_accepted_paid_edit
	FROM 
		v_aa_as_pg_current
	WHERE
		timestamp > i_start_time AND 
		timestamp < i_end_time AND
		state = 'accepted' AND
		transition = 'accept' AND 
		status = 'paid' AND 
		action_type = 'edit' AND
		token_id IS NOT NULL;

	SELECT 
		COUNT(*) 
	INTO
		o_review_accepted_paid_extra_images
	FROM 
		v_aa_as_pg_current
	WHERE
		timestamp > i_start_time AND 
		timestamp < i_end_time AND
		state = 'accepted' AND
		transition = 'accept' AND 
		status = 'paid' AND 
		action_type = 'extra_images' AND
		token_id IS NOT NULL;


	SELECT 
		COUNT(*) 
	INTO
		o_review_accepted_paid_video
	FROM 
		v_aa_as_pg_current
	WHERE
		timestamp > i_start_time AND 
		timestamp < i_end_time AND
		state = 'accepted' AND
		transition = 'accept' AND 
		status = 'paid' AND 
		action_type = 'video' AND
		token_id IS NOT NULL;

	SELECT 
		COUNT(*) 
	INTO
		o_review_accepted_paid_editrefused
	FROM 
		v_aa_as_pg_current
	WHERE
		timestamp > i_start_time AND 
		timestamp < i_end_time AND
		state = 'accepted' AND
		transition = 'accept' AND 
		status = 'paid' AND 
		action_type = 'editrefused' AND
		token_id IS NOT NULL;

	SELECT 
		COUNT(*) 
	INTO
		o_review_accepted_verified
	FROM 
		v_aa_as_pg_current
	WHERE
		timestamp > i_start_time AND 
		timestamp < i_end_time AND
		state = 'accepted' AND
		transition = 'accept' AND 
		status = 'verified' AND
		token_id IS NOT NULL;

	SELECT 
		COUNT(*) 
	INTO
		o_review_accepted_cleared
	FROM 
		v_aa_as_pg_current
	WHERE
		timestamp > i_start_time AND 
		timestamp < i_end_time AND
		state = 'accepted' AND
		transition = 'accept' AND 
		status = 'cleared' AND
		token_id IS NOT NULL;

	SELECT 
		COUNT(*) 
	INTO
		o_review_refused_new
	FROM 
		v_aa_as_current
	WHERE
		timestamp > i_start_time AND 
		timestamp < i_end_time AND
		state = 'refused' AND
		transition = 'refuse' AND 
		action_type in ('new', 'prolong') AND
		token_id IS NOT NULL;

	SELECT 
		COUNT(*) 
	INTO
		o_review_refused_edit
	FROM 
		v_aa_as_current
	WHERE
		timestamp > i_start_time AND 
		timestamp < i_end_time AND
		state = 'refused' AND
		transition = 'refuse' AND 
		action_type = 'edit' AND
		token_id IS NOT NULL;

	SELECT 
		COUNT(*) 
	INTO
		o_review_refused_renew
	FROM 
		v_aa_as_current
	WHERE
		timestamp > i_start_time AND 
		timestamp < i_end_time AND
		state = 'refused' AND
		transition = 'refuse' AND 
		action_type = 'renew' AND
		token_id IS NOT NULL;

	SELECT 

		COUNT(*) 
	INTO
		o_review_refused_editrefused
	FROM 
		v_aa_as_current
	WHERE
		timestamp > i_start_time AND 
		timestamp < i_end_time AND
		state = 'refused' AND
		transition = 'refuse' AND 
		action_type = 'editrefused' AND
		token_id IS NOT NULL;

	o_review_refused_misc := o_review_manual_refused  
				- o_review_refused_new 
				- o_review_refused_edit
				- o_review_refused_renew 
				- o_review_refused_editrefused ;


END
$$ LANGUAGE plpgsql;
