CREATE OR REPLACE FUNCTION is_force_review(
	i_ad_id ad_actions.ad_id%TYPE,
	i_action_id ad_actions.action_id%TYPE
) RETURNS BOOLEAN AS $$
BEGIN
	RETURN EXISTS (SELECT value FROM action_params WHERE ad_id  = i_ad_id AND action_id = i_action_id AND name = 'force_review');
END;
$$ LANGUAGE plpgsql;
