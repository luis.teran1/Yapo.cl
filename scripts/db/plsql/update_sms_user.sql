CREATE OR REPLACE FUNCTION update_sms_user (
		i_sms_user_id sms_users.sms_user_id%TYPE,
		i_phone sms_users.phone%TYPE,
		i_free_sms_delta sms_users.free_sms%TYPE,
		OUT o_sms_user_id sms_users.sms_user_id%TYPE,
		OUT o_phone sms_users.phone%TYPE,
		OUT o_free_sms sms_users.free_sms%TYPE
		) AS $$
BEGIN
	IF i_sms_user_id IS NOT NULL THEN
		o_sms_user_id := i_sms_user_id;

		SELECT
			phone,
			free_sms
		INTO
			o_phone,
			o_free_sms
		FROM
			sms_users
		WHERE
			sms_user_id = i_sms_user_id
		FOR UPDATE;

		IF NOT FOUND THEN
			RAISE EXCEPTION 'FACTORY_TRANSLATE:error:ERROR_SMS_USER_NOT_FOUND';
		END IF;
	ELSE
		BEGIN 
			SELECT
				sms_user_id,
				phone,
				free_sms
			INTO STRICT
				o_sms_user_id,
				o_phone,
				o_free_sms
			FROM
				sms_users
			WHERE
				phone like '%' || i_phone
			FOR UPDATE;
		EXCEPTION
			WHEN NO_DATA_FOUND THEN
				RAISE EXCEPTION 'FACTORY_TRANSLATE:error:ERROR_SMS_USER_NOT_FOUND';
			WHEN TOO_MANY_ROWS THEN
				RAISE EXCEPTION 'FACTORY_TRANSLATE:error:ERROR_MULTIPLE_SMS_USERS_FOUND';
		END;
	END IF;

	o_free_sms := o_free_sms + i_free_sms_delta;
	IF o_free_sms < 0 THEN
		RAISE EXCEPTION 'FACTORY_TRANSLATE:error:ERROR_NEGATIVE_FREE_SMS';
	END IF;

	UPDATE
		sms_users
	SET
		free_sms = o_free_sms
	WHERE
		sms_user_id = o_sms_user_id;
END
$$ LANGUAGE plpgsql;
