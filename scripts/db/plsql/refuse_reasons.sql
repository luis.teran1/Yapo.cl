CREATE OR REPLACE FUNCTION refuse_reasons(i_start_time action_states.timestamp%TYPE,
					  i_end_time action_states.timestamp%TYPE,
					  OUT o_count bigint,
					  OUT o_refuse_reason state_params.value%TYPE) RETURNS SETOF RECORD AS $$
	SELECT 
		COUNT(*), 
		value as refuse_reason
	FROM 
		action_states JOIN state_params USING (ad_id, action_id, state_id) 
	WHERE 
		timestamp > $1 AND 
		timestamp < $2 AND 
		transition = 'refuse' AND
		name = 'reason' 
	GROUP BY 
		refuse_reason
	ORDER BY 
		count DESC;
$$ LANGUAGE sql;
