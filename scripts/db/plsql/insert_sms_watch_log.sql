CREATE OR REPLACE FUNCTION insert_sms_watch_log(i_sms_log_id sms_log_watch.sms_log_id%TYPE, 
					i_list_id sms_log_watch.list_id%TYPE,
					i_query_id sms_log_watch.watch_query_id%TYPE,
					i_watch_user_id sms_log_watch.watch_user_id%TYPE) RETURNS sms_log_watch.sms_log_id%TYPE AS $$
DECLARE
	l_query_string sms_log_watch.query_string%TYPE;
BEGIN
	SELECT 
		query_string
	INTO
		l_query_string	
	FROM
		watch_queries
	WHERE
		watch_user_id = i_watch_user_id AND
		watch_query_id = i_query_id;

	IF NOT FOUND THEN
		RAISE EXCEPTION 'ERROR_NO_SUCH_WATCH_QUERY';
	END IF;	

	INSERT INTO sms_log_watch (
		sms_log_id,
		list_id,
		query_string,
		watch_user_id,
		watch_query_id
	) VALUES (
		i_sms_log_id,
		i_list_id,
		l_query_string,
		i_watch_user_id,
		i_query_id);

	RETURN i_sms_log_id;
END;
$$ LANGUAGE plpgsql;
