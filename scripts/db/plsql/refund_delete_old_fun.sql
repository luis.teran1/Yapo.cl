CREATE OR REPLACE FUNCTION refund_delete_old_fun(
	i_days	INTEGER
) RETURNS INTEGER AS $$
DECLARE
	l_res	INTEGER;
BEGIN
	WITH	X
	AS		(DELETE FROM 
				refunds
			WHERE
				creation_date < now() - (''||i_days||' day')::INTERVAL
			RETURNING 
				*)
	SELECT	count(*)	INTO	l_res
	FROM	X;
	RETURN	l_res;
END;
$$ LANGUAGE plpgsql;
