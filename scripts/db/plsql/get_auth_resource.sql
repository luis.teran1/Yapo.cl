CREATE OR REPLACE FUNCTION get_auth_resource(i_token tokens.token%TYPE) RETURNS integer AS $$
DECLARE
	l_store_id tokens.store_id%TYPE;
	l_admin_id tokens.admin_id%TYPE;
BEGIN
	SELECT
		store_id,
		admin_id
	INTO
		l_store_id,
		l_admin_id
	FROM
		tokens
	WHERE
		token = i_token;
	
	IF NOT FOUND THEN
		RAISE EXCEPTION 'ERROR_TOKEN_INVALID';
	END IF;

	IF l_store_id IS NOT NULL THEN
		RETURN l_store_id;
	END IF;
	IF l_admin_id IS NOT NULL THEN
		RETURN l_admin_id;
	END IF;

	-- Shouldn't happen.
	RAISE EXCEPTION 'ERROR_NO_AUTH_RESOURCE';
END
$$ STABLE LANGUAGE plpgsql;
