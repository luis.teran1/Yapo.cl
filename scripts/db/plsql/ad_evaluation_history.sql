CREATE OR REPLACE FUNCTION ad_evaluation_history(
	i_ad_id ad_actions.ad_id%TYPE,
	i_schema text,
	i_ad_evaluation_transition varchar,
	i_ad_evaluation_admin varchar,
	OUT o_action_type ad_actions.action_type%TYPE,
	OUT o_state varchar,
	OUT o_transition varchar,
	OUT o_timestamp action_states.timestamp%TYPE,
	OUT o_admin admins.username%TYPE) RETURNS SETOF record AS $$
DECLARE
	l_rec record;
BEGIN
	FOR
		l_rec
	IN
		EXECUTE 'SELECT	
			aa.action_type as action_type, ap1.value as evaluation, ap2.value as timestamp 
		FROM
			' || quote_ident(i_schema) || '.ad_actions aa
			JOIN ' || quote_ident(i_schema) || '. action_params ap1 ON (aa.ad_id=ap1.ad_id AND aa.action_id=ap1.action_id)
			JOIN ' || quote_ident(i_schema) || '. action_params ap2 ON (aa.ad_id=ap2.ad_id AND aa.action_id=ap2.action_id)
		WHERE
			aa.ad_id = ' || quote_literal(i_ad_id) || '
		AND
			ap1.name = \'ad_evaluation_result\'
		AND
			ap2.name = \'ad_evaluation_timestamp\'
		'
        LOOP
		o_action_type := l_rec.action_type;
		o_transition := i_ad_evaluation_transition;
		o_state := l_rec.evaluation;
		o_timestamp := l_rec.timestamp;
		o_admin := i_ad_evaluation_admin;

		RETURN NEXT;
	END LOOP;
END
$$ LANGUAGE plpgsql;
