CREATE OR REPLACE FUNCTION insert_visitor(i_vid integer[], 
		i_creation varchar[]) RETURNS VOID AS $$
DECLARE
	l_i integer;
	l_visitor_id integer;
	creation_date date;
BEGIN
	IF i_vid IS NOT NULL THEN
		l_i := 1; -- Arrays in postgresql are 1-based
		LOOP
			l_visitor_id := i_vid[l_i];
			creation_date := i_creation[l_i]::date;
			EXIT WHEN l_visitor_id IS NULL;

			PERFORM 
				vid
			FROM 
				visitor
			WHERE 
				vid = l_visitor_id;

			IF NOT FOUND THEN
				INSERT INTO
					visitor	
				VALUES
					(l_visitor_id,
					 creation_date);
			END IF;
			l_i := l_i +1;
		END LOOP;
	END IF;	
END;
$$ LANGUAGE plpgsql;
