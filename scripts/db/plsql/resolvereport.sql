CREATE OR REPLACE FUNCTION resolvereport(i_report_id abuse_reports.report_id%TYPE,
		 			    i_resolution abuse_reports.resolution%TYPE,
		 			    i_admin_id abuse_reports.admin_id%TYPE,
		 			    i_score boolean,
		 			    i_fake_reporters_email_domain users.email%TYPE,
					    OUT o_ad_id ads.ad_id%TYPE,
					    OUT o_abuse_type abuse_reports.report_type%TYPE,
					    OUT o_reporter_id abuse_reports.reporter_id%TYPE,
					    OUT o_report_id abuse_reports.report_id%TYPE,
					    OUT o_resolution abuse_reports.resolution%TYPE,
					    OUT o_lang abuse_reports.lang%TYPE,
					    OUT o_email users.email%TYPE) AS $$
DECLARE
	l_reporter_id abuse_reports.reporter_id%TYPE; 
	l_abuse_type abuse_reports.report_type%TYPE; 
	l_lang abuse_reports.lang%TYPE; 
	l_user_id users.user_id%TYPE;
	l_email users.email%TYPE;
	l_ad_id ads.ad_id%TYPE;
BEGIN

	--  Check if report exists
	SELECT 
		reporter_id, ad_id, report_type, user_id,lang
	INTO 
		l_reporter_id,l_ad_id, l_abuse_type, l_user_id, l_lang
	FROM 
		abuse_reports
	WHERE
		report_id = i_report_id AND 
		status IN ('unsolved', 'askinfo');

	-- It's a valid report
	IF FOUND THEN
		o_ad_id := l_ad_id;
		o_resolution := i_resolution;
		o_abuse_type := l_abuse_type;
		o_report_id := i_report_id;
		o_reporter_id := l_reporter_id;
		o_lang := l_lang;
		UPDATE 
			abuse_reports
		SET 
			admin_id = i_admin_id,
			resolution = i_resolution,
			status = 'solved'
		WHERE 
			report_id = i_report_id;


		IF i_score THEN 
			IF i_resolution = 'accept' OR i_resolution = 'accept_and_delete' OR i_resolution = 'accept_delete_and_warn' OR i_resolution = 'accept_and_notify_owner' THEN
				UPDATE 
					abuse_reporters
				SET 
					accepted_reports = accepted_reports + 1
				WHERE 
					uid = l_reporter_id;

			ELSIF i_resolution = 'reject' OR i_resolution = 'reject_and_notify_reporter' THEN
				UPDATE 
					abuse_reporters
				SET 
					rejected_reports = rejected_reports + 1
				WHERE 
					uid = l_reporter_id;
			END IF;
		END IF;

		IF l_user_id IS NOT NULL THEN
			SELECT 
				email 
			INTO 
				l_email
			FROM 
				users 
			WHERE
				email NOT LIKE '%@' || i_fake_reporters_email_domain AND
				user_id = l_user_id;

			o_email := l_email;
		END IF;

	ELSE 
		RAISE EXCEPTION 'ERROR_NOT_VALID_REPORT';
	END IF;
END;
$$ LANGUAGE plpgsql;
