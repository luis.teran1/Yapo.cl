CREATE OR REPLACE FUNCTION is_inserting_fee_ad(
	i_ad_id ads.ad_id%TYPE,
	i_category ads.category%TYPE
) RETURNS bool AS $$
DECLARE
	l_ad_id ads.ad_id%TYPE;
BEGIN
	IF i_category IS NULL THEN
		IF EXISTS(SELECT 1 FROM ad_actions WHERE action_type = 'inserting_fee' AND state = 'accepted' AND ad_id = i_ad_id) THEN
			return true;
		END IF;	
	END IF;	

	SELECT ads.ad_id INTO l_ad_id
	FROM ads JOIN ad_params adp using(ad_id)
	WHERE ads.ad_id = i_ad_id AND ads.category = i_category AND adp.name = 'inserting_fee' AND adp.value = '1';

	IF l_ad_id IS NOT NULL THEN
		return true;
	END IF;	
	return false;
END;
$$ LANGUAGE plpgsql;
