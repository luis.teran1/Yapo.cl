CREATE OR REPLACE FUNCTION apply_product_to_store(
					i_store_id stores.store_id%TYPE,
					i_expire_time varchar,
					i_product_id integer,
					i_payment_group_id payment_groups.payment_group_id%TYPE,
					i_token_id tokens.token_id%TYPE,
					OUT o_status stores.status%TYPE,
					OUT o_date_end stores.date_end%TYPE,
					OUT o_email accounts.email%TYPE
					) AS $$
DECLARE
	l_store_id stores.store_id%TYPE;
	l_status stores.status%TYPE;
	l_action_id store_actions.action_id%TYPE;
	l_state_id store_actions.current_state%TYPE;
	l_date_end stores.date_end%TYPE;
BEGIN

	SELECT
		stores.store_id,
		stores.status,
		accounts.email
	INTO
		l_store_id,
		l_status,
		o_email
	FROM
		stores JOIN accounts USING(account_id)
	WHERE store_id = i_store_id;

	IF l_store_id IS NULL THEN
		RAISE EXCEPTION 'FACTORY_TRANSLATE:error:ERROR_STORE_NOT_EXIST';
	END IF;

	IF l_status IN ('admin_deactivated') THEN
		RAISE EXCEPTION 'FACTORY_TRANSLATE:error:ERROR_STORE_ADMIN_DEACTIVATED';
	END IF;


	IF l_status = 'expired' THEN
		PERFORM update_store_status(
			CAST(l_store_id AS integer),
			'active'::enum_stores_status,
			i_token_id,
			NULL
		);
	ELSIF l_status = 'pending' THEN
		PERFORM update_store_status(
			CAST(l_store_id AS integer),
			'inactive'::enum_stores_status,
			i_token_id,
			NULL
		);
	END IF;

	SELECT action_id, current_state INTO l_action_id, l_state_id 
	FROM store_actions WHERE payment_group_id = i_payment_group_id;

	IF l_action_id IS NULL THEN
		SELECT o_action_id ,o_state_id
		FROM
			insert_store_action_state(
				i_store_id,
				'extend',
				'reg',
				i_token_id,
				i_payment_group_id
			)
		INTO l_action_id, l_state_id;
	ELSE
		PERFORM insert_store_state(
			l_store_id,
			l_action_id,
			'paid',
			i_token_id
		);
	END IF;

	SELECT date_end, status 
	INTO l_date_end,o_status 
	FROM stores 
	WHERE store_id = l_store_id;
	
	IF l_date_end is NULL OR l_date_end < now() THEN
		l_date_end := now() + (i_expire_time)::interval;
	ELSE
		l_date_end := l_date_end +(i_expire_time)::interval;
	END IF;
	o_date_end := l_date_end;

	PERFORM insert_store_change(l_store_id, l_action_id, l_state_id, false, 'date_end', l_date_end::text);
	PERFORM insert_store_change(l_store_id, l_action_id, l_state_id, true, 'store_type', i_product_id::text);
	PERFORM apply_store_changes(l_store_id, l_action_id);

	PERFORM insert_store_state(
		l_store_id,
		l_action_id,
		'accepted',
		i_token_id
	);

END;
$$ LANGUAGE plpgsql;
