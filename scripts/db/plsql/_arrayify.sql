CREATE OR REPLACE FUNCTION _arrayify (i_arr anyarray, i_elem anyelement) RETURNS anyarray AS $$
SELECT CASE
	WHEN $2 IS NULL THEN $1
	WHEN $1 IS NULL THEN ARRAY[$2]
	ELSE array_append($1, $2)
END
$$ IMMUTABLE LANGUAGE SQL;

DROP AGGREGATE IF EXISTS arrayify(anyelement);
CREATE AGGREGATE arrayify (
	BASETYPE = anyelement,
	SFUNC = _arrayify,
	STYPE = anyarray
);
