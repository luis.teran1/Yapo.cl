CREATE OR REPLACE FUNCTION move_to_queue(
	i_ad_id integer,
	i_action_id integer,
	i_queue ad_actions.queue%TYPE,
	i_remote_addr action_states.remote_addr%TYPE,
	OUT o_queue ad_actions.queue%TYPE
) AS $$
DECLARE
	l_action_type varchar;
	l_current_state integer; 
	l_state_id action_states.state_id%TYPE;
	l_queue ad_actions.queue%TYPE;
BEGIN
		
	-- if the ad is in a queue different from the default, skip
	SELECT
		queue,
		action_type
	INTO
		l_queue,
		l_action_type
	FROM
		ad_actions
	WHERE
		ad_id = i_ad_id
	AND
		action_id = i_action_id
	LIMIT 1;

	IF l_queue != 'preprocessing' THEN
		RAISE NOTICE 'move_to_queue: ad % in queue % different from preprocessing', i_ad_id, l_queue;
		RETURN;
	END IF;

	-- If the ad has already been moderated, skip
	PERFORM
		*
	FROM
		ad_actions
	WHERE
		ad_id = i_ad_id
	AND
		action_id = i_action_id
	AND
		state != 'pending_review';
	
	IF FOUND THEN
		RAISE NOTICE 'move_to_queue: ad % being moderated or already moderated', i_ad_id;
		RETURN;
	END IF;
	-- Move the ad to the new queue
	l_state_id := insert_state(i_ad_id,
				   i_action_id,
				   'pending_review',
				   'newqueue',
				   NULL,
				   NULL);
	
	-- Move the ad to the given queue
	SELECT 
		current_state
	INTO 
		l_current_state
	FROM 
		ad_actions 
	WHERE 
		ad_id = i_ad_id  AND  action_id = i_action_id;	

	INSERT INTO
		state_params (ad_id, action_id,state_id,name,value)
	VALUES
		(i_ad_id, i_action_id, l_current_state, 'queue', i_queue);

	-- Set the queue
	UPDATE
		ad_actions
	SET
		queue = i_queue
	WHERE
		ad_id = i_ad_id AND
		action_id = i_action_id;

	-- Update the queue in the ad_queues table
	UPDATE
		ad_queues
	SET
		queue = i_queue
	WHERE
		ad_id = i_ad_id AND
		action_id = i_action_id;

	o_queue = i_queue;
END;
$$ LANGUAGE plpgsql;
