CREATE OR REPLACE FUNCTION get_pack_adreply_report(
                        i_list_id ads.list_id%TYPE,
                        i_date DATE,
                        i_email VARCHAR,
                        OUT o_added_at mail_queue.added_at%TYPE,
                        OUT o_ad_subject VARCHAR,
                        OUT o_list_id ads.list_id%TYPE,
                        OUT o_account_name VARCHAR,
                        OUT o_formatted_date VARCHAR,
                        OUT o_sender_name mail_queue.sender_name%TYPE,
                        OUT o_sender_email mail_queue.sender_email%TYPE,
                        OUT o_sender_phone mail_queue.sender_phone%TYPE,
                        OUT o_body mail_queue.body%TYPE
) RETURNS SETOF record AS $$
DECLARE
		l_year INTEGER;
		l_last_year_schema VARCHAR;
		l_this_year_schema VARCHAR;
		l_rec record;
		l_query VARCHAR;
		l_ad_email VARCHAR;
		l_account_id INTEGER;
		l_pack_id INTEGER;
BEGIN

	IF i_list_id IS NOT NULL THEN
		SELECT email
		INTO l_ad_email
		FROM ads join users USING(user_id)
		WHERE list_id = i_list_id;

		SELECT account_id
		INTO l_account_id
		FROM accounts
		WHERE email = l_ad_email AND status = 'active';

		IF NOT FOUND THEN
			RAISE EXCEPTION 'FACTORY_TRANSLATE:error:ERROR_REPORT_ACCOUNT_NOT_FOUND';
		END IF;

		SELECT pack_id
		INTO l_pack_id
		FROM packs
		WHERE account_id = l_account_id AND status = 'active';

		IF NOT FOUND THEN
			RAISE EXCEPTION 'FACTORY_TRANSLATE:error:ERROR_REPORT_PACK_ACTIVE_NOT_FOUND';
		END IF;

		i_date = current_timestamp;
		l_year := extract(year from i_date);
		l_last_year_schema := 'blocket_' || (l_year - 1);
		l_this_year_schema := 'blocket_' || l_year;

		l_query := format(
			'SELECT
				mq.added_at,
				ads.subject,
				ads.list_id,
				ac.name,
				to_char(mq.added_at, ''DD/MM/YYYY HH24:MI:SS'') AS formatted_date,
				mq.sender_name,
				mq.sender_email,
				mq.sender_phone,
				mq.body
			FROM
				%s.mail_queue mq
				INNER JOIN ads USING(list_id)
				INNER JOIN accounts ac USING(user_id)
			WHERE
				list_id = %s
				AND DATE(added_at) > DATE(''%s'') - INTERVAL ''12 month''
			UNION ALL
			SELECT
				mq.added_at,
				ads.subject,
				ads.list_id,
				ac.name,
				to_char(mq.added_at, ''DD/MM/YYYY HH24:MI:SS''),
				mq.sender_name,
				mq.sender_email,
				mq.sender_phone,
				mq.body
			FROM
				%s.mail_queue mq
				INNER JOIN ads USING(list_id)
				INNER JOIN accounts ac USING(user_id)
			WHERE
				list_id = %s
				AND DATE(added_at) <= DATE(''%s'')
				AND ads.status IN (''active'', ''disabled'',''deactivated'')
			ORDER BY
				added_at DESC;',
			l_last_year_schema, i_list_id, i_date, l_this_year_schema, i_list_id, i_date
		);
	ELSE
		l_year := extract(year from i_date);
		l_this_year_schema := 'blocket_' || l_year;
		l_query := format(
			'SELECT
				mq.added_at,
				ads.subject,
				ads.list_id,
				ac.name,
				to_char(mq.added_at, ''DD/MM/YYYY HH24:MI:SS'') AS formatted_date,
				mq.sender_name,
				mq.sender_email,
				mq.sender_phone,
				mq.body
			FROM
				%s.mail_queue mq
				INNER JOIN ads USING(list_id)
				INNER JOIN accounts ac USING(user_id)
			WHERE
				extract(month from added_at) = extract(month from DATE(''%s''))
				AND ac.email = ''%s''
				AND ads.status IN (''active'', ''disabled'',''deactivated'')
			ORDER BY
				added_at DESC;',
			l_this_year_schema, i_date, i_email
		);
	END IF;


	FOR
		l_rec
	IN

	EXECUTE
		l_query
	LOOP
		o_added_at := l_rec.added_at;
		o_ad_subject:= l_rec.subject;
		o_list_id := l_rec.list_id;
		o_account_name := l_rec.name;
		o_formatted_date := l_rec.formatted_date;
		o_sender_name := l_rec.sender_name;
		o_sender_email := l_rec.sender_email;
		o_sender_phone := l_rec.sender_phone;
		o_body := l_rec.body;

		RETURN NEXT;
	END LOOP;
END
$$ LANGUAGE plpgsql;
