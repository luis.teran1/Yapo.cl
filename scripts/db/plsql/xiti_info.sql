DROP FUNCTION IF EXISTS xiti_info(varchar); 
CREATE OR REPLACE FUNCTION xiti_info(i_verify_code payment_groups.code%TYPE,
				o_ad_id OUT ads.ad_id%TYPE,
				o_list_id OUT ads.list_id%TYPE,
				o_category OUT ads.category%TYPE,
				o_region OUT ads.region%TYPE,
				o_company_ad OUT ads.company_ad%TYPE, 
				o_has_images OUT boolean,
				o_number_of_images OUT integer) AS $$
--
BEGIN
	SELECT
		ad_id
	INTO
		o_ad_id
	FROM
		ad_actions
        	JOIN payment_groups USING (payment_group_id)
	WHERE   
		payment_groups.code = i_verify_code AND
		action_type IN ('new', 'edit');

	IF FOUND THEN
		--
		SELECT  
			ad_id,
			list_id,
			category,
			region,
			company_ad 
		INTO	
			o_ad_id,
			o_list_id,
			o_category,
			o_region,
			o_company_ad
		FROM
			ads
		WHERE   
			ad_id = o_ad_id;

		--
		SELECT
			count(*)
		INTO
			o_number_of_images
		FROM
			ad_media
		WHERE
			ad_id = o_ad_id;

		IF o_number_of_images != 0 THEN
			o_has_images := 't';
		ELSE
			o_has_images := 'f';
		END IF;		

	END IF;
END
$$ LANGUAGE plpgsql;
