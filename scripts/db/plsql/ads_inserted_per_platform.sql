CREATE OR REPLACE FUNCTION ads_inserted_per_platform(	i_date_old TIMESTAMP,
							i_date_new TIMESTAMP,
							OUT o_date TIMESTAMP,
							OUT o_value action_params.value%TYPE,
							OUT o_inserted INTEGER )RETURNS SETOF record AS $$
DECLARE
	l_rec record;
BEGIN
	FOR
		l_rec
	IN
        
	EXECUTE 

	'
        SELECT
		DATE_TRUNC(''DAY'',inserted.timestamp)::TIMESTAMP AS o_date,
                action_params.value AS o_value ,
                count(inserted.ad_id) AS o_inserted
        FROM
		ad_actions
	JOIN
                action_states inserted ON
		(action_type = ''new'' AND
		ad_actions.ad_id = inserted.ad_id AND
		ad_actions.action_id = inserted.action_id)
	JOIN
		action_params ON
		(action_params.value IN (''web'',''msite'',''android'',''nga_api_android'',''ios'', ''nga_api_ios'') AND
		action_params.name=''source'' AND
		ad_actions.ad_id = action_params.ad_id AND ad_actions.action_id = action_params.action_id)
        WHERE
                inserted.transition = ''initial'' AND
		inserted.state=''reg'' AND
                inserted.timestamp BETWEEN  ''' || i_date_old ||''' AND ''' ||i_date_new ||'''::TIMESTAMP
	GROUP BY
		1,2
	ORDER BY 1 ASC,2 ASC
	'
	LOOP
		o_date := l_rec.o_date;
		o_value:= l_rec.o_value;
		o_inserted := l_rec.o_inserted;

		RETURN NEXT;
	END LOOP;
END
$$ LANGUAGE plpgsql;
