CREATE OR REPLACE FUNCTION create_social_accounts_params(
		i_email accounts.email%TYPE,
		i_gender accounts.gender%TYPE,
		i_birthdate accounts.birthdate%TYPE,
		i_social_account_id social_accounts.social_account_id%TYPE,
		i_social_account_params varchar[][],
		OUT o_social_account_id social_accounts.social_account_id%TYPE) AS $$
DECLARE
	l_social_account_id social_accounts.social_account_id%TYPE;
	l_email social_accounts.email%TYPE;
	l_i integer;
	l_social_accounts_param_name social_accounts_params.name%TYPE;
	l_social_accounts_param_value social_accounts_params.value%TYPE;
BEGIN

	SELECT
		social_account_id,
		email
	INTO
		l_social_account_id,
		l_email
	FROM
		social_accounts
	WHERE
		email = i_email
	OR
		social_account_id = i_social_account_id;

	IF NOT FOUND THEN
		RAISE EXCEPTION 'FACTORY_TRANSLATE:error:SOCIAL_ACCOUNT_NOT_FOUND';
	END IF;

	-- Update gender
	IF i_gender IS NOT NULL	THEN
		UPDATE
			accounts
		SET
			gender = i_gender
		WHERE
			email = l_email;
	END IF;

	-- Update birthdate
	IF i_birthdate IS NOT NULL THEN
		UPDATE
			accounts
		SET
			birthdate = i_birthdate
		WHERE
			email = l_email;
	END IF;

	-- Insert or update social_accounts_params
	IF i_social_account_params IS NOT NULL THEN
		l_i := 1;

		LOOP
			l_social_accounts_param_name := i_social_account_params[l_i][1];
			l_social_accounts_param_value := i_social_account_params[l_i][2];
			EXIT WHEN l_social_accounts_param_name IS NULL;

			UPDATE
				social_accounts_params
			SET
				value=l_social_accounts_param_value
			WHERE
				social_account_id=l_social_account_id
			AND
				name=l_social_accounts_param_name;

			IF NOT FOUND THEN
				INSERT INTO
					social_accounts_params
					(social_account_id,
					 name,
					 value)
				VALUES
					(l_social_account_id,
					l_social_accounts_param_name,
					replace(l_social_accounts_param_value, E'\'\'', E'\''));
			END IF;
			l_i := l_i + 1;
		END LOOP;
	END IF;

	o_social_account_id := l_social_account_id;
END;
$$ LANGUAGE plpgsql;
