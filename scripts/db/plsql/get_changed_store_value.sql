CREATE OR REPLACE FUNCTION get_changed_store_value(i_store_id store_actions.store_id%TYPE,
					     i_action_id store_actions.action_id%TYPE,
					     i_is_param store_changes.is_param%TYPE,
					     i_column_name store_changes.column_name%TYPE,
					     OUT o_value store_changes.new_value%TYPE) AS $$
BEGIN
	-- First check for a previous entry in store_changes.
	SELECT
		new_value
	INTO
		o_value
	FROM
		store_changes
	WHERE
		store_id = i_store_id AND
		action_id = i_action_id AND
		is_param = i_is_param AND
		column_name = i_column_name
	ORDER BY
		state_id DESC
	LIMIT
		1;

	IF NOT FOUND THEN
		-- If not found, look in store_params, store_users or stores.
		IF i_is_param THEN
			SELECT
				value
			INTO
				o_value
			FROM
				store_params
			WHERE
				store_id = i_store_id AND
				name = i_column_name::enum_store_params_name;
		ELSIF i_column_name = 'usernames' THEN
			SELECT
				group_concat_ordered (email)
			INTO
				o_value
			FROM
				store_users
			WHERE
				store_id = i_store_id;
		ELSE
			EXECUTE
				'SELECT ' ||
				quote_ident(i_column_name) || '::text' ||
				' FROM stores WHERE store_id = ' ||
				quote_literal(i_store_id)
			INTO
				o_value;
		END IF;
	END IF;
END;
$$ LANGUAGE plpgsql;

