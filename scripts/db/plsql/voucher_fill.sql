CREATE OR REPLACE FUNCTION voucher_fill(i_store_id stores.store_id%TYPE,
					i_token tokens.token%TYPE,
					i_amount voucher_actions.amount%TYPE,
					i_remote_addr voucher_states.remote_addr%TYPE,
					OUT o_payment_group_id payment_groups.payment_group_id%TYPE,
					OUT o_voucher_id vouchers.voucher_id%TYPE,
					OUT o_voucher_action_id voucher_actions.voucher_action_id%TYPE
		) AS $$
BEGIN

	IF get_auth_type(i_token) != 'store' OR get_auth_resource(i_token) != i_store_id THEN
		RAISE EXCEPTION 'ERROR_INSUFFICIENT_PRIVS';
	END IF;

	SELECT
		voucher_id
	INTO
		o_voucher_id
	FROM
		vouchers
	WHERE
		store_id = i_store_id;
	
	IF NOT FOUND THEN
		INSERT INTO vouchers (
			store_id
			)
		VALUES (
			i_store_id
		) RETURNING
			voucher_id
		INTO
			o_voucher_id;
	END IF;

	SELECT
		pc.o_payment_group_id
	INTO
		o_payment_group_id
	FROM
		payment_create(NULL,
				'unpaid',
				'voucher_save',
				ARRAY['voucher_refill'],
				ARRAY[i_amount],
				NULL,
				NULL,
				i_remote_addr,
				get_token_id(i_token),
				NULL) AS pc;

	o_voucher_action_id := voucher_action_insert(o_voucher_id,
					     'fill',
					     i_amount,
					     o_payment_group_id);
	PERFORM
		voucher_state_insert(o_voucher_id,
				o_voucher_action_id,
				'unpaid',
				'initial',
				NULL,
				NULL,
				i_remote_addr,
				get_token_id(i_token),
				NULL,
				NULL,
				o_payment_group_id);
END;
$$ LANGUAGE plpgsql;

