-- archive_by_purchase selects all purchases that can be deleted. The purchase must have all its associated ads ready to archive (deleted).
CREATE OR REPLACE FUNCTION archive_by_purchase(total_limit integer, config_time integer[], status_unarchivables varchar[], i_schema varchar) RETURNS integer AS $$
DECLARE
	l_schema varchar;
	obj record;
	array_purchase_ids integer[];
BEGIN
	-- set schema
	IF i_schema IS NULL THEN
		l_schema := 'blocket_' || extract(year from CURRENT_TIMESTAMP);
	ELSE
		l_schema := i_schema;
	END IF;

	RAISE NOTICE 'ARCHIVE: Starting to analyze ads per purchase';

	FOR obj IN
		SELECT  p_id, ads
			FROM (
			SELECT
				count(d.ad_id) as count_ads,
				p.purchase_id as p_id,
				array_agg(d.ad_id) as ads,
				status_unarchivables::enum_ads_status[] && array_agg(ads.status)
				as has_unarchivable_ads,
				count(d.ad_id) != (SELECT count(*) FROM purchase_detail WHERE purchase_id = p.purchase_id) as has_ad_id_null
			FROM purchase p
				join purchase_detail d ON (p.purchase_id = d.purchase_id AND p.status = 'confirmed' AND d.ad_id IS NOT NULL)
				join ads ON (ads.ad_id = d.ad_id)
			WHERE p.receipt <= CURRENT_TIMESTAMP - (interval '1 day' * config_time[5])
			GROUP BY p.purchase_id
		) AS sub1

		WHERE count_ads > 1 AND has_unarchivable_ads = FALSE AND has_ad_id_null = FALSE
		LIMIT total_limit

	LOOP
		IF FALSE NOT IN (SELECT can_archive_purchase_ads(obj.ads::integer[], config_time::integer[])) THEN
			RAISE NOTICE 'Sequence % is TRUE it can delete purchase %', obj.ads, obj.p_id;
			array_purchase_ids = array_append(array_purchase_ids, obj.p_id::integer);
		ELSE
			RAISE NOTICE 'Sequence % is FALSE it can not delete purchase %', obj.ads, obj.p_id;
		END IF;
	END LOOP;

	IF array_length(array_purchase_ids, 1) > 0 THEN
		RAISE NOTICE 'The selected purchase_ids are: %', array_purchase_ids;
		PERFORM archive_purchase(array_purchase_ids, l_schema, NULL);
	ELSE
	RAISE NOTICE 'There are no purchases to archive';
	END IF;
	RETURN array_length(array_purchase_ids, 1);
END
$$ LANGUAGE plpgsql;
