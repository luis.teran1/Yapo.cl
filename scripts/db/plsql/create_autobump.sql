CREATE OR REPLACE FUNCTION create_autobump(
	i_ad_id INTEGER,
	i_frequency INTEGER,
	i_days INTEGER,
	i_use_night INTEGER,
	i_purchase_detail_id INTEGER,
	i_token_id INTEGER,
	i_night_time_start INTEGER,
	i_night_time_end INTEGER,
	OUT o_autobump_id INTEGER,
	OUT o_execute_at TIMESTAMP
) RETURNS SETOF record AS $$
DECLARE
	l_date TIMESTAMP;
	l_date_end TIMESTAMP;
	l_night_start TIMESTAMP;
	l_night_end TIMESTAMP;
	l_ad_status ads.status%TYPE;
	l_ab_counter INTEGER;
BEGIN
	SELECT status INTO l_ad_status FROM ads where ad_id = i_ad_id;

	IF NOT FOUND THEN
		RAISE EXCEPTION 'FACTORY_TRANSLATE:ad_id:ERROR_AD_NOT_FOUND';
	END IF;
	IF l_ad_status not in ('active','disabled') THEN
		RAISE EXCEPTION 'FACTORY_TRANSLATE:ad_id:ERROR_AD_NOT_ACTIVE';
	END IF;

	-- Calculate date start & end
	l_date := CURRENT_TIMESTAMP; -- This variable is used in loop
	l_date_end := CURRENT_TIMESTAMP + interval '1 day' * i_days;

	l_ab_counter := 0;
	-- loop using date time
	WHILE l_date < l_date_end LOOP

		l_night_start := date_trunc('day', l_date) + (interval '1 minutes' * i_night_time_start);
		l_night_end := l_night_start + (interval '1 minutes' * i_night_time_end);
		
		-- If use_night is zero only increase l_date
		IF i_use_night = 0 AND l_date >= l_night_start AND l_date < l_night_end THEN
			-- Increase the time by hours
			l_date := l_date + interval '1 hour' * i_frequency;
		ELSE
			-- Insert row
			INSERT INTO autobump (ad_id, purchase_detail_id, token_id, execute_at)
			VALUES (i_ad_id, i_purchase_detail_id, i_token_id, l_date)
			RETURNING execute_at,autobump_id
			INTO o_execute_at,o_autobump_id;
			-- Increase the time by hours and count
			l_date := l_date + interval '1 hour' * i_frequency;
			l_ab_counter = l_ab_counter + 1;
			-- Return each execute_at and autobump_id
			RETURN NEXT;
		END IF;
	END LOOP;

	IF i_purchase_detail_id IS NOT NULL THEN
		INSERT INTO purchase_detail_params
		VALUES (i_purchase_detail_id, 'total_bump', l_ab_counter);

		INSERT INTO purchase_detail_params
		VALUES (i_purchase_detail_id, 'last_date', l_date);
	END IF;
END;
$$ LANGUAGE plpgsql;
