CREATE OR REPLACE FUNCTION refund_validate(
	i_ad_id integer,
	i_action_id integer,
	i_payment_group_id integer,
	OUT o_receipt timestamp,
	OUT o_purchase_id integer,
	OUT o_ad_id integer,
	OUT o_action_id integer,
	OUT o_refund_exists integer,
	OUT o_refund_id integer,
	OUT o_first_name varchar,
	OUT o_last_name varchar,
	OUT o_rut varchar,
	OUT o_email varchar,
	OUT o_bank_account_type integer,
	OUT o_bank integer,
	OUT o_bank_account_number varchar,
	OUT o_creation_date timestamp,
	OUT o_refund_status enum_refund_status
) AS $$
DECLARE
BEGIN

	SELECT ad_id, action_id INTO o_ad_id, o_action_id
	FROM ad_actions
	WHERE ad_id= i_ad_id AND action_id = i_action_id AND state='refused';

	IF NOT FOUND THEN
		RAISE EXCEPTION 'FACTORY_TRANSLATE:error:ERROR_ACTION_NOT_FOUND';
	END IF;

	o_refund_exists := -1;
	SELECT
		refunds.refund_id,
		refunds.first_name,
		refunds.last_name,
		refunds.rut,
		refunds.email,
		refunds.bank_account_type,
		refunds.bank,
		refunds.bank_account_number,
		refunds.creation_date,
		refunds.status
	INTO
		o_refund_id,
		o_first_name,
		o_last_name,
		o_rut,
		o_email,
		o_bank_account_type,
		o_bank,
		o_bank_account_number,
		o_creation_date,
		o_refund_status
	FROM
		refunds
	WHERE
		ad_id = i_ad_id AND action_id = i_action_id;

	IF FOUND THEN
		o_refund_exists := o_refund_id;
	END IF;

	SELECT P.receipt, P.purchase_id INTO o_receipt, o_purchase_id
	FROM purchase_detail D JOIN purchase P USING(purchase_id)
	WHERE D.ad_id = i_ad_id AND D.action_id = i_action_id AND P.status in ('paid','sent', 'confirmed', 'failed') AND D.payment_group_id = i_payment_group_id;

	IF NOT FOUND THEN
		SELECT receipt_date, purchase_in_app_id
		INTO o_receipt, o_purchase_id
		FROM purchase_in_app
		WHERE ad_id = i_ad_id AND action_id = i_action_id AND purchase_in_app_id = i_payment_group_id AND status IN ('pending', 'confirmed');
	END IF;
END;
$$ LANGUAGE plpgsql;
