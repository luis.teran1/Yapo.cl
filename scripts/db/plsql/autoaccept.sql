CREATE OR REPLACE FUNCTION autoaccept(i_ad_id ad_actions.ad_id%TYPE,
				      i_action_id ad_actions.action_id%TYPE,
				      i_no_auto_categories integer[],
				      i_category_price_limits integer[][],
				      i_global_price_limit integer,
				      i_blocked_items_lists integer[]) RETURNS BOOL AS $$
DECLARE
	l_action_type	ad_actions.action_type%TYPE;
	l_category	ads.category%TYPE;
	l_price_old	ads.price%TYPE;
	l_price_new	ads.price%TYPE;
	l_original_price ads.price%TYPE;
	l_i		integer;
	l_percentage_diff_price	float8;
	l_subject_old ad_changes.old_value%TYPE;
	l_subject_new ad_changes.new_value%TYPE;
BEGIN
	SELECT
		action_type
	INTO
		l_action_type
	FROM
		ad_actions
	WHERE
		ad_id = i_ad_id AND
		action_id = i_action_id;

	IF NOT FOUND THEN
		RETURN false;
	END IF;

	-- Autoaccept edit/renew actions with no changes or only price-changes (apart from passwd-changes)
	IF l_action_type NOT IN ('edit', 'renew', 'prolong') THEN
		RETURN false;
	END IF;

	IF EXISTS
		(SELECT * FROM ad_changes WHERE ad_id = i_ad_id AND action_id = i_action_id AND column_name NOT IN
			('price', 'passwd', 'area', 'salted_passwd', 'phone', 'currency', 'prev_currency', 'lang',
				'name', 'country', 'subject', 'geoposition_is_precise', 'geoposition', 'communes', 'address',
				'address_number', 'street_id'
			)
		) OR
	   EXISTS (SELECT * from ad_image_changes WHERE ad_id = i_ad_id AND action_id = i_action_id AND is_new) OR
	   EXISTS (SELECT * FROM ad_changes WHERE ad_id = i_ad_id AND action_id = i_action_id AND column_name IN ('name') AND
			EXISTS (SELECT * FROM blocked_items WHERE list_id = ANY(i_blocked_items_lists) AND ad_changes.new_value LIKE '%' || value || '%' ) ) THEN
		RETURN false; 
	END IF;

	-- Check category.
	l_category := get_changed_value(i_ad_id,
					i_action_id,
					false,
					'category');
	IF l_category = ANY (i_no_auto_categories) THEN
		RETURN false;
	END IF;

	-- Check subject
	-- It's normalized so the comparison ignores case changes
	SELECT
		old_value, new_value
	INTO
		l_subject_old, l_subject_new
	FROM
		ad_changes
	WHERE
		ad_id = i_ad_id
		AND action_id = i_action_id
		AND NOT is_param
		AND column_name = 'subject';

	IF LOWER(l_subject_old) <> LOWER(l_subject_new) 
	THEN
		RETURN FALSE;
	END IF;


	-- Now it only depends on price
	SELECT
		old_value, new_value
	INTO
		l_price_old, l_price_new
	FROM
		ad_changes
	WHERE
		ad_id = i_ad_id
		AND action_id = i_action_id
		AND NOT is_param
		AND column_name = 'price';

	SELECT	old_price
	INTO	l_original_price
	FROM	ads
	WHERE	ad_id = i_ad_id;

	-- If there's no "first price", we look for it in the first ad_change (action) of the ad where the price was inserted in an edit...
	IF l_original_price IS NULL THEN
		SELECT	ac.new_value
		INTO	l_original_price
		FROM	ad_changes ac
		INNER JOIN ad_actions aa USING (ad_id, action_id)
		WHERE	ac.ad_id = i_ad_id
		AND	ac.action_id < i_action_id
		AND	ac.old_value IS NULL
		AND	ac.column_name = 'price'
		AND	aa.action_type = 'edit'
		AND	aa.state = 'accepted'
		ORDER BY action_id ASC
		LIMIT	1;
	END IF;

	-- ...otherwise, we get the value from the current 'ad_change' (action), since it is the price the ad currently has.

	/*
	"old_price" is a column in the "ads" table which contains the very first price the ad ever had, or null if it hasn't been edited or was inserted without one.
	Some ads (specially old ads) have been inserted directly in the ads table, without recoding any history about that first value.
	On those cases, we get the current price value from column old_value in table ad_changes.
	 */	
	IF l_original_price IS NULL THEN
		l_original_price = l_price_old;
	END IF;
	
	IF l_price_old IS NULL AND l_price_new IS NULL THEN
		RETURN true;
	ELSIF l_price_old IS NULL AND l_price_new IS NOT NULL THEN
		RETURN false;
	ELSIF l_price_old IS NOT NULL AND l_price_new IS NULL THEN
		RETURN false;
	ELSIF l_price_new <= i_global_price_limit THEN
		RETURN false;
	ELSIF l_original_price IS NULL OR l_original_price = 0 THEN
		RETURN false;
	ELSIF l_price_new >= l_price_old OR l_price_new >= l_original_price THEN
		RETURN true;
	END IF;

	l_percentage_diff_price := cast((l_original_price - l_price_new) as float8) * 100 / l_original_price;

	-- Loop over category percentage limits
	-- TODO: get only needed category percentage in the beginning, instead of passing over the whole 2-dimensional array
	--	 (this is done in call_clear_ad_action.sql.tmpl)
	l_i := 1;
	LOOP
		EXIT WHEN i_category_price_limits[l_i][1] IS NULL;
		IF l_category = i_category_price_limits[l_i][1] THEN
			IF l_percentage_diff_price <= i_category_price_limits[l_i][2] THEN
				RETURN true;
			ELSE
				RETURN false;
			END IF;
		END IF;
		l_i := l_i + 1;
	END LOOP;

	RETURN false;
END;
$$ LANGUAGE plpgsql STABLE;
