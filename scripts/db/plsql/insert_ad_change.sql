CREATE OR REPLACE FUNCTION insert_ad_change(i_ad_id ad_changes.ad_id%TYPE,
					    i_action_id ad_changes.action_id%TYPE,
					    i_state_id ad_changes.state_id%TYPE,
					    i_is_param ad_changes.is_param%TYPE,
					    i_name VARCHAR ,--ad_changes.column_name%TYPE,
					    i_new_value ad_changes.new_value%TYPE) RETURNS VOID AS $$
DECLARE
	l_old_value ad_changes.old_value%TYPE;
BEGIN
	-- Find the old value.
	l_old_value := get_changed_value(i_ad_id,
					 i_action_id,
					 i_is_param,
					 i_name);
	
	-- Verify that the value has changed.
	IF l_old_value IS NULL AND i_new_value IS NULL THEN
		RETURN;
	END IF;
	IF l_old_value IS NULL OR i_new_value IS NULL OR l_old_value != i_new_value THEN
		INSERT INTO
			ad_changes (
				ad_id,
				action_id,
				state_id,
				is_param,
				column_name,
				old_value,
				new_value)
		VALUES
		       (i_ad_id,
			i_action_id,
			i_state_id,
			i_is_param,
			i_name,
			l_old_value,
			i_new_value);
	END IF;
END
$$ LANGUAGE plpgsql;
