CREATE OR REPLACE FUNCTION lock_queue_per_info(i_queue trans_queue.queue%TYPE,
					       i_lock_name trans_queue.locked_by%TYPE,
					       OUT o_trans_queue_id trans_queue.trans_queue_id%TYPE,
					       OUT o_command trans_queue.command%TYPE) RETURNS SETOF record AS $$
DECLARE
	l_rec record;
BEGIN
	LOCK trans_queue IN row exclusive MODE;

	FOR
		l_rec
	IN
		SELECT
			min(trans_queue_id) AS trans_queue_id
		FROM
			trans_queue
		WHERE
			queue = i_queue AND
			executed_at IS NULL AND
			COALESCE(execute_at <= CURRENT_TIMESTAMP, true) AND
			(locked_by IS NULL OR locked_at < CURRENT_TIMESTAMP - INTERVAL '2 minute')
		GROUP BY
			info
	LOOP
		o_trans_queue_id := l_rec.trans_queue_id;
		
		UPDATE
			trans_queue
		SET
			locked_at = CURRENT_TIMESTAMP,
			locked_by = i_lock_name
		WHERE
			trans_queue_id = o_trans_queue_id
		RETURNING
			command
		INTO
			o_command;

		RETURN NEXT;
	END LOOP;
END
$$ LANGUAGE plpgsql;
