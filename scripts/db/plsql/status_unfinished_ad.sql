CREATE OR REPLACE FUNCTION status_unfinished_ad (i_unf_ad_id	unfinished_ads.unf_ad_id%TYPE,
						i_unf_status	unfinished_ads.status%TYPE,
						OUT o_unf_ad_id	unfinished_ads.unf_ad_id%TYPE
						) AS $$
BEGIN

	-- Getting unfinished ad id, session id and email of the unfinished ad (if it comes)
	IF i_unf_ad_id IS NOT NULL AND i_unf_status IS NOT NULL
	THEN
		IF (i_unf_status = 'active')	-- update mailed_at if its updated to be 'active'
		THEN
			UPDATE
				unfinished_ads
			SET
				status = i_unf_status,
				modified_at = NOW(),
				mailed_at = NOW()
			WHERE
				unf_ad_id = i_unf_ad_id;
		ELSE

			UPDATE
				unfinished_ads
			SET
				status = i_unf_status,
				modified_at = NOW()
			WHERE
				unf_ad_id = i_unf_ad_id;
		END IF;
	END IF;

	o_unf_ad_id := i_unf_ad_id;

END;
$$ LANGUAGE plpgsql;

