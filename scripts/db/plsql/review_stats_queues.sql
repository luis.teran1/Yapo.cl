CREATE OR REPLACE FUNCTION review_stats_queues(i_start_time action_states.timestamp%TYPE,
		i_end_time action_states.timestamp%TYPE,
		OUT o_queue ad_actions.queue%TYPE,
		OUT o_accepted integer,
		OUT o_refused integer) RETURNS SETOF record AS $$
DECLARE
	l_rec record;
BEGIN
	FOR
		l_rec
	IN
		SELECT
			queue,
			COUNT(NULLIF (v_action_states_current.state, 'accepted')) AS refused,
			COUNT(NULLIF (v_action_states_current.state, 'refused')) AS accepted
		FROM
			v_ad_actions_current
			JOIN v_action_states_current USING (ad_id, action_id)
		WHERE
			(
			 	action_states.state = 'accepted'
				AND action_states.transition = 'accept'
			) OR (
				action_states.state = 'refused'
				AND action_states.transition = 'refuse'
			)
			AND timestamp >= i_start_time AND 
			timestamp < i_end_time
		GROUP BY
			queue
		ORDER BY
			queue
	LOOP
		o_queue := l_rec.queue;
		o_accepted := l_rec.accepted;
		o_refused := l_rec.refused;
		RETURN NEXT;
	END LOOP;
END;
$$ LANGUAGE plpgsql;
