CREATE OR REPLACE FUNCTION set_not_newbie_user(i_user_id users.user_id%TYPE)
		RETURNS VOID AS $$
BEGIN

	UPDATE
		user_params
	SET
		name = 'first_inserted_ad'
	WHERE
		user_id = i_user_id
		AND name = 'pre_first_inserted_ad';

END;
$$ LANGUAGE plpgsql;
