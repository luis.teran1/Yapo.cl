CREATE OR REPLACE FUNCTION lock_ads_to_review_pg_advisory(
	i_queue         text,
	i_token         tokens.token%TYPE,
	i_interval      integer,
	i_limit         integer,
	i_remote_addr   action_states.remote_addr%TYPE,
	i_multiplier    integer,
	OUT o_ad_id     ad_queues.ad_id%TYPE,
	OUT o_action_id ad_queues.action_id%TYPE
)
RETURNS SETOF record AS $$
	--
	-- Locks up to i_limit ads by updating the ad_queues table, assigns
	-- them to the admin identified by the token i_token and returns them
	-- as (ad_id, action_id, locked_at) rows.
	--
	-- Also updates the tables ad_actions and action_states via a call to
	-- the lockaction stored procedure.
	--
	-- @warning
	--	 Uses Postgresql's advisory locks in an attempt to reduce latency.
	--	 Note that there is no strict warranty on the number of returned
	--	 rows contrary to the pessimistic lock version from
	--	 scripts/db/plsql/lock_ads_to_review_global.sql (previously
	--	 aquire_locks).
	--
	-- @see
	--	 See the technical report at doc/techreports/DTC-60/ for some
	--	 details about the ad_queues lock problem.
	--
	-- @return
	--	 Up to i_limit rows (ad_id, action_id, locked_at) from the
	--	 ad_queues table. The locket_at timestamp is essentially returned
	--	 for debugging reasons.
	--
	-- @param i_queue
	--	 Name of the queue to fetch ads from. Be warned that it is NOT
	--	 a filter name: 'new' is allowed ONLY because this particular case
	--	 is hardcoded in the queries.
	--
	-- @param i_token
	--	 Token used to uniquely identify the admin performing the query.
	--
	-- @param i_interval
	--	 Time in seconds after which a locked ad is no longer considered
	--	 locked and can thus be reassigned to another admin.
	--
	-- @param i_limit
	--	 Maximum number of ads locked and returned to the admin performing
	--	 the query.
	--
	-- @param i_remote_addr
	--	 IP address of the admin performing the query.
	--
	-- @param i_multiplier
	--	 A multiplier used to make sure enough rows are returned. Ideally
	--	 it should be set to the maximum number of admins simultaneously
	--	 querying the database.
	--
DECLARE
	l_admin_id   admins.admin_id%TYPE;

	--
	-- The following solution was greatly inspired by this post:
	-- http://johtopg.blogspot.fr/2010/12/queues-in-sql.html
	--
	-- WARNING:
	--	Be very careful when editing this cursor's query as a
	--	seemingly innocuous change can totally confuse the planner.
	--	Always check your edits with PostgreSQL EXPLAIN command.
	--
	l_ad_queues_cursor CURSOR IS
		UPDATE
			ad_queues
		SET
			locked_by	= l_admin_id,
			locked_until = CURRENT_TIMESTAMP + (INTERVAL '1 SECOND' * i_interval)
		WHERE
			ad_id IN (
				SELECT
					ad_id
				FROM (
					--
					-- This seemingly redundant subquery is a trick to
					-- convince the planner to choose an index scan over
					-- a sequential scan. See the aforementionned post
					-- for the full explanation.
					--
					SELECT
						ad_id, action_id
					FROM
						ad_queues
					WHERE
						(queue::text = COALESCE(i_queue, queue::text)
							OR (i_queue = 'new' AND queue = 'normal')
						)
						AND (locked_by IS NULL OR locked_until < CURRENT_TIMESTAMP)
					ORDER BY
						queued_at
					LIMIT
						i_multiplier * i_limit
				) subquery
				WHERE
					--
					-- pg_try_advisory_xact_lock accepts one bigint or
					-- two ints.
					-- The lock is automatically released at the end of the transaction
					--
					pg_try_advisory_xact_lock(
						ad_id,
						action_id
					)
				LIMIT
					i_limit
				)
			--
			-- A very important check as the subquery might be out of
			-- date since it is executed against a snapshot of the databse.
			-- Again, see the blogspot post for the full explanation.
			--
			AND (queue::text = COALESCE(i_queue, queue::text)
				OR (i_queue = 'new' AND queue = 'normal')
			)
			AND (locked_by IS NULL OR locked_until < CURRENT_TIMESTAMP)

		RETURNING ad_id, action_id;

BEGIN
	l_admin_id := get_admin_by_token(i_token);

	OPEN l_ad_queues_cursor;
	LOOP
		FETCH l_ad_queues_cursor INTO o_ad_id, o_action_id;
		EXIT WHEN NOT FOUND;

		PERFORM lockaction(
			o_ad_id,
			o_action_id,
			i_interval,
			i_remote_addr,
			1,
			l_admin_id,
			i_token
		);

		RETURN NEXT;
	END LOOP;
END;
$$ LANGUAGE plpgsql;
