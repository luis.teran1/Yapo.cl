CREATE OR REPLACE FUNCTION queue_in_array_lookup(
		i_group varchar,
		i_queue varchar,
		i_group_list varchar[],
		i_queue_matrix varchar[][][],
		OUT o_value varchar) AS $$
DECLARE
	l_group varchar ;
	l_queue_map_index integer ;
	l_queue_index integer ;
BEGIN
	-- Ensure l_group is always defined by checking it's in the list of groups provided.  If not, set l_group to 'default'.
	FOR l_queue_map_index IN array_lower(i_group_list, 1) .. array_upper(i_group_list, 1) LOOP
		IF i_group = i_group_list[l_queue_map_index] THEN
			l_group := i_group ;
		END IF ;
	END LOOP ;
	IF l_group IS NULL THEN
		l_group := 'default' ;
	END IF ;
	l_queue_map_index = 0 ;
	-- Find the map for this group and do a map lookup in it for the queue.
	IF i_queue_matrix IS NOT NULL AND i_group_list IS NOT NULL THEN
		FOR l_queue_map_index IN array_lower(i_group_list, 1) .. array_upper(i_group_list, 1) LOOP
			IF l_group = i_group_list[l_queue_map_index] AND l_queue_map_index BETWEEN array_lower(i_queue_matrix, 1) AND array_upper(i_queue_matrix, 1) THEN
				FOR l_queue_index IN array_lower(i_queue_matrix, 2) .. array_upper(i_queue_matrix, 2) LOOP
					IF i_queue = i_queue_matrix[l_queue_map_index][l_queue_index][1] THEN
						o_value := i_queue_matrix[l_queue_map_index][l_queue_index][2];
						RETURN;
					END IF;
				END LOOP;
			END IF ;
		END LOOP ;
	END IF ;
END ;
$$ LANGUAGE plpgsql ;
