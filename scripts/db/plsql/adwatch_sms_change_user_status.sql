CREATE OR REPLACE FUNCTION adwatch_sms_change_user_status (
		i_sender sms_users.phone%TYPE,
		i_status sms_users.status%TYPE,
		i_token_id tokens.token_id%TYPE,
		OUT o_sms_user_id sms_users.sms_user_id%TYPE,
		OUT o_status sms_users.status%TYPE
		) AS $$
DECLARE
BEGIN
	SELECT
		sms_user_id,
		status
	FROM
		sms_users
	WHERE
		phone = i_sender
	INTO
		o_sms_user_id,
		o_status;

	IF NOT FOUND THEN
		RAISE EXCEPTION 'ERROR_PHONE_NOT_FOUND'; 
	END IF;

	IF i_status IS NOT NULL THEN
		UPDATE
			sms_users
		SET
			status = i_status
		WHERE
			sms_user_id = o_sms_user_id; -- phone = i_sender;
		o_status = i_status;
	END IF;
	
	IF i_token_id IS NOT NULL THEN
		PERFORM log_event('sms_user_status', 'sms_user_id: ' || o_sms_user_id::text || ', status: ' || o_status::text, i_token_id);
	END IF;
END
$$ LANGUAGE 'plpgsql';

