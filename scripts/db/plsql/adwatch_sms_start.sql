CREATE OR REPLACE FUNCTION adwatch_sms_start (
		i_sender sms_users.phone%TYPE,
		i_sms_central sms_users.sms_central%TYPE,
		i_session_id sms_log.session_id%TYPE,
		i_code watch_queries.sms_activation_code%TYPE,
		i_message sms_log.message%TYPE,
		OUT o_sms_user_id sms_users.sms_user_id%TYPE,
		OUT o_watch_user_id watch_users.watch_user_id%TYPE,
		OUT o_watch_query_id watch_queries.watch_query_id%TYPE,
		OUT o_sms_log_id sms_log.sms_log_id%TYPE
		) AS $$
DECLARE
	l_sms_central sms_users.sms_central%TYPE;
BEGIN
	IF i_code IS NOT NULL THEN
		SELECT
			watch_user_id,
			watch_query_id
		INTO
			o_watch_user_id,
			o_watch_query_id
		FROM
			watch_queries
		WHERE
			sms_user_id IS NULL
			AND sms_activation_code = i_code
			AND sms_activation_expiry > CURRENT_TIMESTAMP;
		
		IF NOT FOUND THEN
			RAISE EXCEPTION 'ERROR_CODE_NOT_FOUND';
		END IF;
	END IF;

	SELECT
		sms_user_id,
		sms_central
	INTO
		o_sms_user_id,
		l_sms_central
	FROM
		sms_users
	WHERE
		phone = i_sender;
	
	IF NOT FOUND THEN
		IF i_code IS NULL THEN
			RAISE EXCEPTION 'ERROR_SMS_USER_NOT_FOUND';
		END IF;

		INSERT INTO sms_users (
			phone,
			sms_central,
			credit,
			credit_total,
			status
		) VALUES (
			i_sender,
			i_sms_central,
			0,
			0,
			'active'
		) RETURNING
			sms_user_id
		INTO
			o_sms_user_id;
	ELSE
		IF i_code IS NULL THEN
			-- Reset amount spent (credit) and activate
			UPDATE 
				sms_users
			SET
				credit = 0,
				status = 'active'
			WHERE
				sms_user_id = o_sms_user_id;
		ELSE
			UPDATE 
				sms_users
			SET
				status = 'active'
			WHERE
				sms_user_id = o_sms_user_id;
		END IF;

		IF l_sms_central != i_sms_central THEN
			-- Assume user switched provider.
			UPDATE
				sms_users
			SET
				sms_central = i_sms_central
			WHERE
				sms_user_id = o_sms_user_id;
		END IF;
	END IF;

	INSERT INTO sms_log (
		sms_user_id,
		session_id,
		sms_type,
		cost,
		mobile_originated,
		status_code,
		message
	) VALUES (
		o_sms_user_id,
		i_session_id,
		'mo',
		0,
		true,
		1,
		i_message
	) RETURNING
		sms_log_id
	INTO
		o_sms_log_id;	

	IF o_watch_query_id IS NOT NULL THEN
		UPDATE
			watch_queries
		SET
			sms_user_id = o_sms_user_id,
			sms_activation_expiry = CURRENT_TIMESTAMP,
			sms_status = 'active'
		WHERE
			watch_user_id = o_watch_user_id
			AND watch_query_id = o_watch_query_id;
		INSERT INTO sms_log_watch (
			sms_log_id,
			watch_user_id,
			watch_query_id,
			query_string
		) VALUES (
			o_sms_log_id,
			o_watch_user_id,
			o_watch_query_id,
			(SELECT 
				query_string 
			FROM 
				watch_queries 
			WHERE 
				watch_user_id = o_watch_user_id AND 
				watch_query_id = o_watch_query_id)
		);
	END IF;
END
$$ LANGUAGE plpgsql;

