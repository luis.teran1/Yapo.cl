--
-- This function returns all ad_id+subject with the same uid as the input ad_id.
--
CREATE OR REPLACE FUNCTION find_associated_ads(i_ad_id ads.ad_id%TYPE,
				i_limit integer,
				OUT o_ad_id ads.ad_id%TYPE,
				OUT o_action_id ad_actions.action_id%TYPE,
				OUT o_subject ads.subject%TYPE,
				OUT o_body ads.body%TYPE,
				OUT o_category ads.category%TYPE,
				OUT o_list_id ads.list_id%TYPE,
				OUT o_list_time ads.list_time%TYPE,
				OUT o_status ads.status%TYPE,
				OUT o_type ads.type%TYPE,
				OUT o_region ads.region%TYPE,
				OUT o_city ads.city%TYPE,
				OUT o_current_state ad_actions.current_state%TYPE,
				OUT o_price ads.price%TYPE,
				OUT o_ad_params text,
				OUT o_image char(10),
				OUT o_score FLOAT) RETURNS SETOF record AS $$
DECLARE
	l_rec record;
	l_uid users.uid%TYPE;
BEGIN
	--
	-- Get uid from users
	--
	SELECT users.uid INTO l_uid
	FROM
		ads join users using(user_id)
	WHERE
		ads.ad_id = i_ad_id;

	--
	-- Loop through all ads with same uid and return matches.
	--
	FOR
		l_rec
	IN
		SELECT
			ad_id,
			subject,
			body,
			category,
			type,
			status,
			COALESCE(list_id, 0) AS list_id,
			list_time,
			city,
			region,
			COALESCE(price, 0) as price,
			score_duplicate_ad(i_ad_id, ads.ad_id) as score,
			(SELECT COALESCE((SELECT lpad(ad_media_id::text, 10, '0') || '.jpg' FROM ad_media WHERE ad_id = ads.ad_id AND seq_no=0),'0')) as image
		FROM
			ads join users using(user_id)
		WHERE
			users.uid = l_uid AND
			ads.ad_id != i_ad_id AND
			ads.status != 'deleted'
		ORDER BY score DESC
		LIMIT i_limit
	LOOP
		o_ad_id := l_rec.ad_id;
		o_subject := l_rec.subject;
		o_body := l_rec.body;
		o_type := l_rec.type;
		o_region := l_rec.region;
		o_city := l_rec.city;
		o_status := l_rec.status;
		o_list_id := l_rec.list_id;
		o_score := l_rec.score;
		o_image := l_rec.image;
		o_category := l_rec.category;
		o_price :=l_rec.price;
		--
		-- Select the last paid/verified but not refused action_id and its current state
		--
		SELECT
			MAX(action_id)
		INTO
			o_action_id
		FROM
			ad_actions
		WHERE
			ad_id = o_ad_id AND
			state IN ('pending_review', 'locked', 'accepted', 'deactivated');

		IF o_action_id IS NULL THEN
			CONTINUE;
		END IF;

		SELECT current_state INTO o_current_state
		FROM
			ad_actions
		WHERE
			ad_actions.ad_id = o_ad_id AND
			ad_actions.action_id = o_action_id;

		IF o_status IN ('active') THEN
			 o_list_time := l_rec.list_time;
		ELSE
			SELECT
				MAX(action_states.timestamp)
			INTO
				o_list_time
			FROM
				action_states
			WHERE
				action_states.state_id = o_current_state AND
				action_states.state IN ('pending_review', 'locked', 'deactivated');
			
			IF o_list_time IS NULL THEN
				CONTINUE;
			END IF;
		END IF;

		-- Concat all ad param values
		SELECT
			GROUP_CONCAT_ORDERED(name || ':' || value)
		INTO
			o_ad_params
		FROM
			ad_params
		WHERE
			ad_id = o_ad_id;

		RETURN NEXT;
	END LOOP;
END
$$ LANGUAGE plpgsql;
