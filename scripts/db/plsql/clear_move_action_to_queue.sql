CREATE OR REPLACE FUNCTION clear_move_action_to_queue(
		i_ad_id ads.ad_id%TYPE,
		i_action_id ad_actions.action_id%TYPE,
		i_queue ad_actions.queue%TYPE) RETURNS VOID AS $$
DECLARE
	l_current_state integer;
BEGIN

	SELECT 
		current_state 
	INTO 
		l_current_state
	FROM 
		ad_actions 
	WHERE 
		ad_id = i_ad_id  AND  action_id = i_action_id;	

	INSERT INTO
		state_params (ad_id, action_id,state_id,name,value)
	VALUES
		(i_ad_id, i_action_id, l_current_state, 'queue', i_queue);

	-- Set the queue
	UPDATE
		ad_actions
	SET
		queue = i_queue
	WHERE
		ad_id = i_ad_id AND
		action_id = i_action_id;

	-- Register the action in the ad_queues table
	INSERT
	INTO
		ad_queues 
		(ad_id,
		 action_id,
		 queue)
	VALUES
		(i_ad_id,
		 i_action_id, 
		 i_queue);
END
$$ LANGUAGE plpgsql;
