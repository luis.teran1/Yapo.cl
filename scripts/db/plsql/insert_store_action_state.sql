CREATE OR REPLACE FUNCTION insert_store_action_state(i_store_id store_actions.store_id%TYPE,
						  i_action_type store_actions.action_type%TYPE,
						  i_state store_action_states.state%TYPE,
						  i_token_id tokens.token_id%TYPE,
						  i_payment_group_id store_actions.payment_group_id%TYPE,
						  OUT o_action_id store_actions.action_id%TYPE,
						  OUT o_state_id store_action_states.state_id%TYPE
						) AS $$
BEGIN
	o_action_id := insert_store_action(i_store_id, i_action_type, i_payment_group_id);

	-- Create state
	o_state_id := insert_store_state(i_store_id,
					 o_action_id,
					 i_state,
					 i_token_id);
END;
$$ LANGUAGE plpgsql;
