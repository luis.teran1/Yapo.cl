CREATE OR REPLACE FUNCTION get_watch_user_id (
		i_watch_unique_id watch_users.watch_unique_id%TYPE,
		OUT o_watch_user_id watch_users.watch_user_id%TYPE
		) AS $$
DECLARE
	l_last_activity watch_users.last_activity%TYPE;
BEGIN
	SELECT
		last_activity,
		watch_user_id
	INTO
		l_last_activity,
		o_watch_user_id
	FROM
		watch_users
	WHERE
		watch_unique_id = i_watch_unique_id;
	
	IF FOUND AND l_last_activity::date != CURRENT_DATE THEN
		-- Update user activity
		UPDATE
			watch_users
		SET
			last_activity = CURRENT_TIMESTAMP
		WHERE
			watch_user_id = o_watch_user_id;
	END IF;
END
$$ LANGUAGE plpgsql;
