CREATE OR REPLACE FUNCTION lockaction(i_ad_id ads.ad_id%TYPE, 
				      i_action_id ad_actions.action_id%TYPE,
				      i_interval integer,
				      i_remote_addr action_states.remote_addr%TYPE,
				      i_lock integer,
				      i_admin_id admins.admin_id%TYPE,
				      i_token tokens.token%TYPE,
				      OUT o_ad_id ads.ad_id%TYPE,
				      OUT o_action_id ad_actions.action_id%TYPE) AS $$
DECLARE
	l_ad_id ads.ad_id%TYPE;
	l_action_id ad_actions.action_id%TYPE;
	l_last_state action_states.state%TYPE;
BEGIN
	l_ad_id := i_ad_id;
	l_action_id := i_action_id;

	IF l_ad_id IS NULL THEN
		RAISE EXCEPTION 'ad_id not set';
	END IF;

	IF i_admin_id IS NULL THEN
		i_admin_id := get_admin_by_token(i_token);
	END IF;

	SELECT
		state
	INTO
		l_last_state
	FROM
		ad_actions
	WHERE
		ad_actions.ad_id = l_ad_id AND
		ad_actions.action_id = l_action_id AND
		(locked_by IS NULL OR 
		locked_until < now() OR
		(locked_by = i_admin_id AND
		locked_until >= CURRENT_TIMESTAMP));

	IF NOT FOUND THEN
		RAISE EXCEPTION 'ERROR_QUEUE_EMPTY %, %, %, %', l_ad_id, l_action_id, i_admin_id, l_last_state;
	END IF;

	IF l_last_state NOT IN ('pending_review', 'locked') THEN
		RAISE EXCEPTION 'ERROR_INVALID_STATE';
	END IF;

        IF i_lock = 1 THEN
        	PERFORM insert_state(l_ad_id,
        		     l_action_id,
	        	     'locked',
		             'checkout',
        		     i_remote_addr,
		             get_token_id(i_token));
        	UPDATE 	
	        	ad_actions 
        	SET 	
			locked_by = i_admin_id,
			locked_until = CURRENT_TIMESTAMP + (INTERVAL '1 SECOND' * i_interval)
		WHERE
			ad_id = l_ad_id AND
			action_id = l_action_id;
        END IF;

	o_ad_id := l_ad_id;
	o_action_id := l_action_id;
END;
$$ LANGUAGE plpgsql;
