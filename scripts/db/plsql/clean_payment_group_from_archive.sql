CREATE OR REPLACE FUNCTION clean_payment_group_from_archive (
		) RETURNS void AS $$
DECLARE
	l_token_id tokens.token_id%TYPE;
BEGIN

	RAISE NOTICE 'ARCHIVE: Deleting from pay_log_references';
	DELETE FROM
		pay_log_references
	USING
		pay_log
		JOIN archive_payment_groups_clean USING (payment_group_id)
	WHERE
		pay_log_references.pay_log_id = pay_log.pay_log_id;
	
	RAISE NOTICE 'ARCHIVE: Deleting from pay_log';
	FOR
		l_token_id
	IN
		DELETE FROM
			pay_log
		USING
			archive_payment_groups_clean
		WHERE
			pay_log.payment_group_id = archive_payment_groups_clean.payment_group_id
		RETURNING
			token_id
	LOOP
		PERFORM clean_token(l_token_id);
	END LOOP;

	RAISE NOTICE 'ARCHIVE: Deleting from payments';
	DELETE FROM
		payments
	USING
		archive_payment_groups_clean
	WHERE
		payments.payment_group_id = archive_payment_groups_clean.payment_group_id;
	
	RAISE NOTICE 'ARCHIVE: Deleting from payment_groups';
	DELETE FROM
		payment_groups
	USING
		archive_payment_groups_clean
	WHERE
		payment_groups.payment_group_id = archive_payment_groups_clean.payment_group_id;
END
$$ LANGUAGE plpgsql;
