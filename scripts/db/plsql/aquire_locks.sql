CREATE OR REPLACE FUNCTION aquire_locks(
	i_queue ad_actions.queue%TYPE,
	i_token tokens.token%TYPE,
	i_interval integer,
	i_limit integer,
	i_remote_addr action_states.remote_addr%TYPE,
	OUT o_ad_id ad_queues.ad_id%TYPE,
	OUT o_action_id ad_queues.action_id%TYPE
)
RETURNS SETOF record AS $$
DECLARE
	l_admin_id admins.admin_id%TYPE; 

	l_ad_queues_cursor CURSOR (i_queue ad_actions.queue%TYPE) IS
	SELECT
		ad_id,
		action_id
	FROM
		ad_queues
	WHERE
		(queue = COALESCE(i_queue, queue) OR
		 (i_queue = 'new' AND queue = 'normal')) AND
		(locked_by IS NULL OR
		 locked_until < CURRENT_TIMESTAMP)
	ORDER BY
		queued_at
	LIMIT
		i_limit;
BEGIN
	l_admin_id := get_admin_by_token(i_token);

	LOCK TABLE ad_actions IN ROW SHARE MODE;
	LOCK TABLE ad_queues;
	OPEN l_ad_queues_cursor(i_queue);
	LOOP
		FETCH l_ad_queues_cursor INTO o_ad_id, o_action_id;
		EXIT WHEN NOT FOUND;

		UPDATE
			ad_queues
		SET
			locked_by = l_admin_id,
		        locked_until = CURRENT_TIMESTAMP + (INTERVAL '1 SECOND' * i_interval)
		WHERE
			ad_id = o_ad_id AND
			action_id = o_action_id;

		PERFORM lockaction(o_ad_id,
				o_action_id,
				i_interval,
				i_remote_addr,
				1,
				l_admin_id,
				i_token);
			
		RETURN NEXT;
	END LOOP;
END;
$$ LANGUAGE plpgsql;

