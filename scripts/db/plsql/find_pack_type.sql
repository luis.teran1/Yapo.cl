CREATE OR REPLACE FUNCTION find_pack_type(
	i_category integer,
	i_pack_conf varchar[][],
	OUT o_pack_type varchar
) AS $$
DECLARE
	l_pack_conf_array varchar[];
	
BEGIN
	FOREACH l_pack_conf_array SLICE 1 IN ARRAY i_pack_conf LOOP
		IF EXISTS( select * from unnest(l_pack_conf_array) t where t = i_category::varchar) THEN
			o_pack_type := l_pack_conf_array[1];
			EXIT; -- exit loop
		END IF;
	END LOOP;
END;
$$ LANGUAGE plpgsql;
