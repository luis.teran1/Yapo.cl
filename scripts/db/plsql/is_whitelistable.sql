CREATE OR REPLACE FUNCTION is_whitelistable(i_email blocked_items.value%TYPE,
				i_min_ads INTEGER,
				OUT o_is_whitelistable BOOLEAN,
				OUT o_reason VARCHAR(20)) AS $$
DECLARE
	l_user_id ads.user_id%TYPE;
	l_user_refused_ads INTEGER;
	l_user_ads_with_notes INTEGER;
	l_count_created_ads INTEGER;
	l_three_months_ago TIMESTAMP;
	
BEGIN

	o_is_whitelistable := TRUE;
	o_reason := 'ALL_OK';
	l_three_months_ago := NOW() - interval '3 months';

	SELECT
		user_id 
	INTO
		l_user_id
	FROM
		users
	WHERE
		email = i_email;


	-- If the user has inserted less than 3 ads o_is_whitelistable := FALSE 
	SELECT 
		count(1)
	INTO 
		l_count_created_ads
	FROM 
		ads
	WHERE 
		ads.user_id = l_user_id AND status NOT IN ('inactive')
		AND ads.list_time > l_three_months_ago;

	IF l_count_created_ads < i_min_ads THEN
		o_is_whitelistable := FALSE;
		o_reason := 'LESS_THAN_' || i_min_ads;
		return;
	END IF;

	-- If the user has ads with notes o_is_whitelistable := FALSE
	SELECT count(*)
	INTO l_user_ads_with_notes
 	FROM (
		SELECT distinct ads.ad_id , notices.*
		FROM ads
			join ad_actions using(ad_id)
			join action_states using(ad_id,action_id)
			join notices using(ad_id)
		WHERE ads.user_id = l_user_id  and timestamp > l_three_months_ago
	) as Foo;

	IF l_user_ads_with_notes > 0 THEN
		o_is_whitelistable := FALSE;
		o_reason := 'ADS_WITH_NOTES';
		return;
	END IF;

	-- If the user has refused ads in 3 months o_is_whitelistable := FALSE
	SELECT 
		COUNT(ass.*) 
	INTO
		l_user_refused_ads
	FROM 
		ads a JOIN action_states ass USING (ad_id) 
	WHERE
		a.user_id = l_user_id
		AND state = 'refused'
		AND timestamp > l_three_months_ago;

	IF l_user_refused_ads > 0 THEN
		o_is_whitelistable := FALSE;
		o_reason := 'REJECTED_ADS';
		RETURN;
	END IF;

END;
$$ LANGUAGE plpgsql;
