CREATE OR REPLACE FUNCTION remove_unfinished_actions(
	i_ad_id ads.ad_id%TYPE,
	i_remote_addr action_states.remote_addr%TYPE,
	i_token_id tokens.token_id%TYPE
) RETURNS void AS $$
DECLARE
	l_unfinished_actions record;
BEGIN
	-- Cancel unfinished edit and editrefused actions related with i_ad_id
	FOR l_unfinished_actions IN (
		SELECT action_id FROM ad_actions WHERE ad_id = i_ad_id AND state IN ('locked','pending_review','unverified') AND action_type IN ('edit', 'editrefused')
	)
	LOOP
		-- Create state
		PERFORM insert_state(i_ad_id,l_unfinished_actions.action_id,'closed','cancelled',i_remote_addr, i_token_id);
		 -- Unlock the action in ad_queues
		DELETE FROM ad_queues WHERE ad_id = i_ad_id AND action_id = l_unfinished_actions.action_id;
	END LOOP;

END;
$$ LANGUAGE plpgsql;
