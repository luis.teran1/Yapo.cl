CREATE OR REPLACE FUNCTION scarface_multiple_resolve(i_list_id ads.list_id%TYPE,
						     i_resolution abuse_reports.resolution%TYPE,
					 	     i_report_id abuse_reports.report_id%TYPE,
						     i_admin_id abuse_reports.admin_id%TYPE,
						     i_score boolean,
						     i_fake_reporters_email_domain users.email%TYPE,
						     OUT o_report_type abuse_reports.report_type%TYPE,
						     OUT o_emails varchar,
						     OUT o_ad_id abuse_reports.ad_id%TYPE,
						     OUT o_report_ids varchar,
						     OUT o_langs varchar,
						     OUT o_reporter_ids varchar,
						     OUT o_ad_email varchar) AS $$
DECLARE
	l_rec 		record;
	l_report_type	abuse_reports.report_type%TYPE;
	l_lang	        abuse_reports.lang%TYPE;
	l_email		users.email%TYPE;
	l_reporter_id	abuse_reports.reporter_id%TYPE; 
BEGIN
	o_emails := '';
	o_report_ids := '';
	o_reporter_ids := '';
	o_langs := '';

	SELECT
		report_type, ad_id
	INTO
		l_report_type, o_ad_id
	FROM
		abuse_reports
	WHERE
		report_id = i_report_id;

	o_report_type := l_report_type;

	FOR
		l_rec
	IN
		SELECT 
		        abuse_reports.report_id AS abuse_report_id,
			abuse_reports.report_type AS abuse_report_type
		FROM 
		        abuse_reports JOIN ads USING(ad_id) 
		WHERE 
		        list_id = i_list_id AND
			(i_resolution <> 'accept' OR report_type = l_report_type) AND
		        abuse_reports.status IN ('unsolved', 'askinfo')
	LOOP
		-- PERFORM resolvereport(l_rec.abuse_report_id,
		--		i_resolution,
		--		i_admin_id,
		--		CASE WHEN l_report_type = l_rec.abuse_report_type THEN i_score ELSE FALSE END);

		SELECT
			o_email,
			o_lang,
			o_reporter_id
		INTO
			l_email,
			l_lang,
			l_reporter_id
		FROM
			resolvereport(l_rec.abuse_report_id,
				i_resolution,
				i_admin_id,
				CASE WHEN l_report_type = l_rec.abuse_report_type THEN i_score ELSE FALSE END,
				i_fake_reporters_email_domain);

		IF o_emails = '' THEN
 			o_emails := l_email;
 			o_langs := l_lang;
			o_reporter_ids := l_reporter_id;
		ELSE
			o_emails := o_emails || ',' || l_email;
			o_langs := o_langs || ',' || l_lang;
			o_reporter_ids := o_reporter_ids || ',' || l_reporter_id;
		END IF;

		IF o_report_ids = '' THEN
 			o_report_ids := l_rec.abuse_report_id;
		ELSE
			o_report_ids := o_report_ids || ',' || l_rec.abuse_report_id;
		END IF;

	END LOOP;

	SELECT
		email
	INTO
		o_ad_email
	FROM
		ads join users using(user_id)
	WHERE
		list_id = i_list_id;
END;
$$ LANGUAGE plpgsql;
