CREATE OR REPLACE FUNCTION insert_pay_log (i_pay_type pay_log.pay_type%TYPE,
					   i_code pay_log.code%TYPE,
					   i_amount pay_log.amount%TYPE,
					   i_paid_at pay_log.paid_at%TYPE,
					   i_ref_types enum_pay_log_references_ref_type[],
					   i_references varchar[],
					   i_remote_addr action_states.remote_addr%TYPE,
					   i_token_id pay_log.token_id%TYPE,
					   i_payment_group_id pay_log.payment_group_id%TYPE,
					   i_status pay_log.status%TYPE) RETURNS bool AS $$
DECLARE
	l_i integer;
BEGIN
	-- The CAST cludge is to determine the value that will actually be stored in the insert,
	-- wrt summer time etc.
	-- XXX store with time zone instead?
	IF EXISTS(
		SELECT
			*
		FROM
			pay_log
		WHERE
			pay_type = i_pay_type AND
			code = i_code AND
			paid_at = CAST(CAST(i_paid_at AS timestamp with time zone) AS timestamp(0))
			AND COALESCE(payment_group_id = i_payment_group_id, true)
		) THEN
		RETURN false;
	END IF;
	
	INSERT INTO pay_log (
		pay_type,
		code,
		amount,
		paid_at,
		token_id,
		payment_group_id,
		status
	) VALUES (
		i_pay_type,
		i_code,
		COALESCE(i_amount, 0),
		COALESCE(i_paid_at, CURRENT_TIMESTAMP),
		i_token_id,
		i_payment_group_id,
		i_status
	);

	l_i := 1;
	LOOP
		EXIT WHEN i_ref_types[l_i] IS NULL OR i_references[l_i] IS NULL;

		-- XXX the regexp_replace might not be applicable to new reference types.
		INSERT INTO pay_log_references (
			pay_log_id,
			ref_num,
			ref_type,
			reference
		) VALUES (
			CURRVAL('pay_log_pay_log_id_seq'),
			l_i - 1,
			i_ref_types[l_i],
			regexp_replace(i_references[l_i], '[-]', '', 'g')
		);
		l_i := l_i + 1;
	END LOOP;

	-- Special handling of remote_addr
	IF i_remote_addr IS NOT NULL THEN
		INSERT INTO pay_log_references (
			pay_log_id,
			ref_num,
			ref_type,
			reference
		) VALUES (
			CURRVAL('pay_log_pay_log_id_seq'),
			l_i - 1,
			'remote_addr',
			host(i_remote_addr::inet)
		);
	END IF;
	
	RETURN true;
END
$$ LANGUAGE plpgsql;
