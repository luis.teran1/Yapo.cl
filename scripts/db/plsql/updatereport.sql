CREATE OR REPLACE FUNCTION updatereport(i_ad_id ads.ad_id%TYPE,
					    i_report_id abuse_reports.report_id%TYPE,
		 			    i_abuse_type abuse_reports.report_type%TYPE,
					    i_email users.email%TYPE,
					    i_message abuse_reports.text%TYPE,
					    i_uid users.uid%TYPE,
					    i_fake_reporters_email_domain users.email%TYPE,
					    OUT out_report_id abuse_reports.report_id%TYPE,
					    OUT out_uid users.uid%TYPE) AS $$
DECLARE
	l_uid users.uid%TYPE;
	l_user_id users.user_id%TYPE;
	l_report_id abuse_reports.report_id%TYPE;
	l_uid_reporter users.uid%TYPE;
	l_email users.email%TYPE;
BEGIN
	l_uid:= i_uid;

	IF i_email IS NOT NULL THEN
		-- check if the email exists in DB
		SELECT 
			user_id, uid
		INTO
			l_user_id, l_uid
		FROM 
			users 	
		WHERE
			email = i_email;


		IF NOT FOUND THEN
			-- check if the report has an associated fake user
			SELECT 
				user_id, uid
			INTO
				l_user_id, l_uid
			FROM 
				abuse_reports JOIN users USING(user_id)
			WHERE
				abuse_reports.report_id = i_report_id AND 
				users.email like '%@' || i_fake_reporters_email_domain;

			IF FOUND THEN	
				UPDATE 
					users
				SET 
					email= i_email
				WHERE 
					user_id = l_user_id AND
					email like '%@' || i_fake_reporters_email_domain;
			ELSE 
				l_uid := i_uid;
				INSERT INTO
					users
					(uid,			
					 email)
				VALUES
					(l_uid,
					 i_email);
				l_user_id := CURRVAL('users_user_id_seq');

			END IF;

		-- Email is in db 
		ELSE 	
			-- Conflict Two uids same user	
			IF i_uid != l_uid THEN 
				-- Reporter management
				SELECT 
					uid
				INTO 
					l_uid_reporter
				FROM 
					abuse_reporters
				WHERE
					uid = l_uid;
	
				IF NOT FOUND THEN
					INSERT INTO	
						abuse_reporters
						(uid)
					VALUES
						(l_uid);
				END IF;

				-- Check if exists another report with the same type, uid, ad 
				SELECT 
					report_id 
				INTO 
					l_report_id
				FROM 
					abuse_reports
				WHERE 
					report_type = i_abuse_type AND
					reporter_id = l_uid AND
					ad_id =	i_ad_id AND 
					report_id != i_report_id;
			
				IF FOUND THEN 
					-- i_report_id must be deleted
					DELETE FROM 
						abuse_reports 
					WHERE 
						report_id = i_report_id;
	
					UPDATE 
						abuse_reports
					SET 
						user_id = l_user_id,
						reporter_id = l_uid
					WHERE 
						report_id = l_report_id;
				END IF;	 
			END IF;	 
		END IF;
	END IF;

	-- Update report
	UPDATE 
		abuse_reports
	SET 
		text = COALESCE(text, '') || COALESCE(i_message, ''),
		user_id = COALESCE(l_user_id, user_id), 
		reporter_id = COALESCE(l_uid_reporter, l_uid, reporter_id),
		status='unsolved'
	WHERE
		report_id = COALESCE(l_report_id,i_report_id);


	--- Users/abuse_reporters cleaning
	IF i_email IS NOT NULL THEN
		PERFORM clean_fake_reporters(i_uid,l_uid, i_fake_reporters_email_domain); 
	END IF;

	out_uid := l_uid;
	out_report_id:= COALESCE(l_report_id,i_report_id);

END
$$ LANGUAGE plpgsql;
