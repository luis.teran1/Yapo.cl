CREATE OR REPLACE FUNCTION set_notice(i_notice_id notices.notice_id%TYPE,
				      i_abuse notices.abuse%TYPE,
				      i_body notices.body%TYPE,
				      i_uid notices.uid%TYPE,
				      i_user_id notices.user_id%TYPE,
				      i_ad_id notices.ad_id%TYPE,
				      i_token_id notices.token_id%TYPE,
				      o_notice_id OUT notices.notice_id%TYPE) AS $$
DECLARE
	l_update INTEGER;
BEGIN
	l_update := 0;
	IF i_ad_id IS NOT NULL THEN
		IF EXISTS(SELECT
				1
			FROM
				notices
			WHERE
				ad_id = i_ad_id AND
				COALESCE(notice_id != i_notice_id, true)) THEN
			l_update := 1;
		END IF;
	ELSIF i_uid IS NOT NULL THEN
		IF EXISTS(SELECT
				1
			FROM
				notices
			WHERE
				uid = i_uid AND
				COALESCE(notice_id != i_notice_id, true)) THEN
			l_update := 1;
		END IF;
	ELSIF i_user_id IS NOT NULL THEN
		IF EXISTS(SELECT
				1
			FROM
				notices
			WHERE
				user_id = i_user_id AND
				COALESCE(notice_id != i_notice_id, true)) THEN
			l_update := 1;
		END IF;
	END IF;

	IF l_update = 1 THEN
		UPDATE
			notices
		SET
			abuse = i_abuse,
			body = i_body,
			uid = i_uid,
			user_id = i_user_id,
			ad_id = i_ad_id,
			created_at = CURRENT_TIMESTAMP, -- FT 181: created_at is now 'modified'
			token_id = i_token_id
		WHERE
			(i_ad_id IS NOT NULL AND ad_id = i_ad_id) OR
			(i_user_id IS NOT NULL AND user_id = i_user_id) OR
			(i_uid IS NOT NULL AND uid = i_uid);
		
		IF NOT FOUND THEN
			RAISE EXCEPTION 'ERROR_INVALID_NOTICE_ID';
		END IF;
		
		o_notice_id := i_notice_id;
	ELSE
		INSERT INTO
			notices (
				abuse,
				body,
				uid,
				user_id,
				ad_id,
				created_at,
				token_id)
		VALUES (
			i_abuse,
			i_body,
			i_uid,
			i_user_id,
			i_ad_id,
			CURRENT_TIMESTAMP,
			i_token_id);
		o_notice_id := CURRVAL('notices_notice_id_seq');
	END IF;
END;
$$ LANGUAGE plpgsql;
