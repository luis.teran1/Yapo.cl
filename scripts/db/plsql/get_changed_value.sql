CREATE OR REPLACE FUNCTION get_changed_value(i_ad_id ad_actions.ad_id%TYPE,
					     i_action_id ad_actions.action_id%TYPE,
					     i_is_param ad_changes.is_param%TYPE,
					     i_column_name ad_changes.column_name%TYPE,
					     OUT o_value ad_changes.new_value%TYPE) AS $$
BEGIN
	-- First check for a previous entry in ad_changes.
	SELECT
		new_value
	INTO
		o_value
	FROM
		ad_changes
	WHERE
		ad_id = i_ad_id AND
		action_id = i_action_id AND
		is_param = i_is_param AND
		column_name = i_column_name
	ORDER BY
		state_id DESC
	LIMIT
		1;

	IF NOT FOUND THEN
		-- If not found, look in ad_params or ads.
		IF i_is_param THEN
			SELECT
				value
			INTO
				o_value
			FROM
				ad_params
			WHERE
				ad_id = i_ad_id AND
				name  = i_column_name::enum_ad_params_name;
		ELSE
			EXECUTE
				'SELECT ' ||
				quote_ident(i_column_name) || '::text' ||
				' FROM ads WHERE ad_id = ' ||
				quote_literal(i_ad_id)
			INTO
				o_value;
		END IF;
	END IF;
END;
$$ LANGUAGE plpgsql;
