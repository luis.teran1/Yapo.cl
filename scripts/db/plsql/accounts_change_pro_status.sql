CREATE OR REPLACE FUNCTION accounts_change_pro_status(
    i_account_id        integer[],
    i_pro_type          varchar[],
    i_action            integer[],
    i_token_id          integer,
    i_pack_conf         varchar[][],
    i_insertingfee_conf varchar[][],
    OUT o_email varchar
) AS $$
DECLARE
    l_pack_categories integer[];
    l_account_id accounts.account_id%TYPE;
    l_counter integer;
    l_email accounts.email%TYPE;
    l_old_is_pro_for varchar;
    l_pro_categories varchar;
    l_cat varchar;
    l_account_action_id account_actions.account_action_id%TYPE;
BEGIN
    o_email := '';
    -- VERIFY PRODUCTS
    IF i_account_id IS NULL OR array_length(i_account_id,1) = 0 THEN
        RAISE EXCEPTION 'FACTORY_TRANSLATE:error:ERROR_ACCOUNT_IDS_MISSING';
    END IF;
    IF i_pro_type IS NULL OR array_length(i_pro_type,1) = 0 THEN
        RAISE EXCEPTION 'FACTORY_TRANSLATE:error:ERROR_PRO_TYPE_MISSING';
    END IF;
    IF i_action IS NULL OR array_length(i_action,1) = 0 THEN
        RAISE EXCEPTION 'FACTORY_TRANSLATE:error:ERROR_ACTION_MISSING';
    END IF;

    IF array_length(i_account_id,1) != array_length(i_pro_type,1) OR
        array_length(i_pro_type,1) != array_length(i_action,1) OR
        array_length(i_action,1) != array_length(i_account_id,1) THEN
        RAISE EXCEPTION 'FACTORY_TRANSLATE:error:ERROR_PARAM_LENGTHS_INVALID';
    END IF;

    l_counter := 1;
    FOR l_account_id IN  SELECT unnest(i_account_id)
    LOOP
        SELECT email into l_email from accounts where account_id = l_account_id;
        IF FOUND THEN
            IF o_email = '' THEN
                o_email := l_email;
            ELSE
                o_email := o_email || ',' || l_email;
            END IF;

            l_pack_categories := find_pack_categories(i_pro_type[l_counter], array_cat(i_pack_conf, i_insertingfee_conf));


            SELECT value
            INTO l_old_is_pro_for
            FROM account_params
            WHERE account_id = l_account_id AND name = 'is_pro_for';
            IF NOT FOUND THEN
                l_old_is_pro_for := '';
            END IF;

            IF i_action[l_counter] = 1 THEN
                UPDATE accounts SET is_company = 't' WHERE account_id = l_account_id AND is_company = 'f';
                l_account_action_id := insert_account_action(l_account_id, 'add_pro_category_param', i_token_id);
                FOR l_cat IN SELECT unnest(l_pack_categories)
                LOOP
                    l_pro_categories := insert_pro_category_to_account_params(l_account_id, l_account_action_id, l_cat, i_token_id, i_pack_conf);
                END LOOP;
            ELSIF i_action[l_counter] = 0 THEN
                l_account_action_id := insert_account_action(l_account_id, 'remove_pro_category_param', i_token_id);
                FOR l_cat IN SELECT unnest(l_pack_categories)
                LOOP
                    l_pro_categories := remove_pro_category_to_account_params(l_account_id, l_account_action_id, l_cat, i_token_id, i_pack_conf);
                END LOOP;
            END IF;
            PERFORM insert_account_change(l_account_id, l_account_action_id, true, 'is_pro_for', l_old_is_pro_for, l_pro_categories);
        END IF;
        l_counter := l_counter + 1;
    END LOOP;
END;
$$ LANGUAGE plpgsql;

