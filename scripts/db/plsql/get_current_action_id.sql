CREATE OR REPLACE FUNCTION get_current_action_id(i_ad_id ads.ad_id%TYPE,
				   		 i_schema varchar,
						    OUT o_action_id ad_actions.action_id%TYPE) AS $$
BEGIN

	-- Find latest action id
	EXECUTE 'SELECT action_id FROM ' || quote_ident(i_schema) || '.action_states WHERE ad_id = ' || quote_literal(i_ad_id) || E' AND state IN (\'accepted\',\'deleted\',\'hidden\',\'undo_deleted\',\'unpaid\') ORDER BY timestamp DESC LIMIT 1' INTO o_action_id;

	-- If there are no accepted/deleted states then default is 1
	IF o_action_id IS NULL THEN
		o_action_id := 1;
	END IF;

	RETURN;
END
$$ LANGUAGE plpgsql;
