CREATE OR REPLACE FUNCTION in_array (i_element anyelement, i_array anyarray) RETURNS BOOL AS $$
DECLARE
	l_i INTEGER;
BEGIN
	l_i := 1;
	LOOP
		EXIT WHEN i_array[l_i] IS NULL;
		IF i_array[l_i] = i_element THEN
			RETURN true;
		END IF;
		l_i := l_i + 1;
	END LOOP;

	RETURN false;
END;
$$ LANGUAGE plpgsql;
