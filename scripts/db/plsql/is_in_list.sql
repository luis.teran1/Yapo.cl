CREATE OR REPLACE FUNCTION is_in_list(i_email blocked_items.value%TYPE,
					i_list_name block_lists.list_name%TYPE
				) RETURNS BOOLEAN AS $$
DECLARE
	l_list_id INTEGER;
BEGIN
	SELECT list_id INTO l_list_id FROM block_lists WHERE list_name = i_list_name;
	RETURN EXISTS (SELECT item_id FROM blocked_items WHERE value = i_email AND list_id = l_list_id);
END;
$$ LANGUAGE plpgsql;
