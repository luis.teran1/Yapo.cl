CREATE OR REPLACE FUNCTION clear_store_action(i_order_id varchar,
						i_ref_types enum_pay_log_references_ref_type[],
						i_references varchar[],
						i_remote_addr action_states.remote_addr%TYPE,
					  	i_amount pay_log.amount%TYPE,
						i_pay_time pay_log.paid_at%TYPE,
						i_clear_status pay_log.status%TYPE,
						i_paid_with_voucher bool,
						OUT o_status integer,
						OUT o_sum_price payments.pay_amount%TYPE,
						OUT o_ad_id integer[],
						OUT o_subjects varchar[], 
						OUT o_list_ids integer[], 
						OUT o_ad_price integer[],
						OUT o_extra_images_price integer[],
						OUT o_video_price integer[],
						OUT o_action_type store_actions.action_type%TYPE,
						OUT o_email tokens.email%TYPE
) AS $$
DECLARE
	l_parent_payment_group_id payment_groups.parent_payment_group_id%TYPE;
	l_store_id stores.store_id%TYPE;
	l_action_type store_actions.action_type%TYPE;
	l_action_id store_actions.action_id%TYPE;
	l_rec record;
	l_payment_rec record;
	l_clear_status pay_log.status%TYPE;
	l_status payment_groups.status%TYPE;
	l_amount pay_log.amount%TYPE;
	l_voucher_id vouchers.voucher_id%TYPE;
	l_voucher_action_id voucher_actions.voucher_action_id%TYPE;
	l_ad_amount integer;
	l_pay_type pay_log.pay_type%TYPE;
	l_list_id ads.list_id%TYPE;
	l_subject ads.subject%TYPE;
	l_ad_price integer;
	l_extra_images_price integer;
	l_video_price integer;
BEGIN

	IF i_paid_with_voucher THEN
		l_pay_type := 'payvoucher';
	ELSE
		l_pay_type := 'paycard';
	END IF;

	-- get parent_payment_group_id from order_id
	l_parent_payment_group_id := substring(i_order_id from '__#"%#"a%' for '#')::integer;

	-- get store_data from parent_payment_group_id
	SELECT
		status, store_actions.store_id, action_type, action_id, email
	INTO	
		l_status, l_store_id, l_action_type, l_action_id, o_email
	FROM 
		payment_groups 
		JOIN store_actions USING (payment_group_id)
		JOIN store_action_states USING (store_id, action_id)
		LEFT JOIN tokens USING (token_id)
	WHERE
		payment_group_id = l_parent_payment_group_id AND
		store_action_states.state = 'unpaid';

	IF NOT FOUND or l_status IS NULL OR l_status != 'unpaid' THEN
		IF i_clear_status = 'OK' THEN
		        l_clear_status := 'ERR';
		        l_amount := i_amount;
		ELSE
		        l_clear_status := i_clear_status;
		        l_amount := 0;
		END IF;

		IF insert_pay_log(l_pay_type, i_order_id, l_amount, i_pay_time, 
				i_ref_types, i_references, i_remote_addr, NULL, 
				NULL, l_clear_status) THEN 
			o_status := 1;
		ELSE
			o_status := 5;
			
		END IF;
		RETURN;
	END IF;
	o_status := 0;
	IF i_clear_status IS NOT NULL AND i_clear_status != 'OK' THEN
		IF insert_pay_log(l_pay_type, i_order_id, 0, i_pay_time, i_ref_types, 
				  i_references, i_remote_addr, NULL, 
				  l_parent_payment_group_id, i_clear_status) THEN
			o_status := 1;
		ELSE
		        o_status := 5;
		END IF;
		RETURN;
	END IF;

	-- Update payment_groups to paid
	UPDATE
		payment_groups
	SET
		status = 'paid'
	WHERE
		payment_group_id = l_parent_payment_group_id;
		--OR
		--parent_payment_group_id = l_parent_payment_group_id;
	
END;
$$ LANGUAGE plpgsql;
