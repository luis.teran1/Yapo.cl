CREATE OR REPLACE FUNCTION cmp_ad_params_char(i_val ad_params.name%TYPE, 
                                              i_char varchar) 
RETURNS bool AS $$ 
BEGIN 
      RETURN i_val::varchar = i_char; 
END 
$$ language plpgsql;

DROP OPERATOR IF EXISTS = (enum_ad_params_name, varchar);
CREATE OPERATOR = (
       PROCEDURE = cmp_ad_params_char, 
       LEFTARG=enum_ad_params_name, RIGHTARG=varchar);
