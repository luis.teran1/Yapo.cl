CREATE OR REPLACE FUNCTION update_pack_type(
	i_pack_conf varchar[][],
	OUT o_result integer[]
) AS $$
DECLARE
	l_account_param record;
	pack_type varchar;
	l_first_pack_type varchar;
BEGIN
	FOR l_account_param IN
		(SELECT
				ap.account_id as account_id,
				ap.value as is_pro_for
		FROM
				account_params ap
		WHERE
				ap.name = 'is_pro_for')
	LOOP
		IF l_account_param.is_pro_for IS NOT NULL AND NOT EXISTS (SELECT value FROM account_params WHERE name='pack_type' AND account_id=l_account_param.account_id) THEN
			l_first_pack_type := first_pack_type(l_account_param.is_pro_for, i_pack_conf, l_account_param.account_id);
			IF l_first_pack_type is NOT NULL THEN
				INSERT INTO account_params (account_id, name, value)
				VALUES (l_account_param.account_id, 'pack_type', l_first_pack_type);
			END IF;

			o_result := array_append(o_result, l_account_param.account_id);
		END IF;
	END LOOP;
END;
$$ LANGUAGE plpgsql;
