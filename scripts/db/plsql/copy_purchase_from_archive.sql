CREATE OR REPLACE FUNCTION copy_purchase_from_archive(
                                   i_schema varchar
                                   ) RETURNS void AS $$
BEGIN
-- tokens
        RAISE NOTICE 'ARCHIVE: Locking schema % (row exclusive)', i_schema;
        EXECUTE 'LOCK ' || quote_ident(i_schema) || '.tokens IN ROW EXCLUSIVE MODE';

        PERFORM copy_payment_group_from_archive(i_schema);

-- purchase family tables
        RAISE NOTICE 'ARCHIVE: Copying from purchase';
        EXECUTE
                'INSERT INTO ' ||
                        quote_ident(i_schema) || '.purchase
                        (purchase_id, doc_type, doc_num, external_doc_id, status, receipt, delivery, rut, name, lob, address, region, communes, contact, email, tax, discount, total_price, payment_group_id, seller_id, payment_method, payment_platform, account_id)
                SELECT
                        purchase_id, doc_type, doc_num, external_doc_id, status, receipt, delivery, rut, name, lob, address, region, communes, contact, email, tax, discount, total_price, payment_group_id, seller_id, payment_method, payment_platform, account_id
                FROM
                        purchase
                        JOIN archive_purchase_ids USING (purchase_id)';

        RAISE NOTICE 'ARCHIVE: Copying from purchase_states';
        EXECUTE
                'INSERT INTO ' ||
                        quote_ident(i_schema) || '.purchase_states
                        (purchase_id, purchase_state_id, status, timestamp, remote_addr)
                SELECT
                        purchase_id, purchase_state_id, status, timestamp, remote_addr
                FROM
                        purchase_states
                        JOIN archive_purchase_ids USING (purchase_id)';

        RAISE NOTICE 'ARCHIVE: Copying from purchase_detail';
        EXECUTE
                'INSERT INTO ' ||
                        quote_ident(i_schema) || '.purchase_detail
                        (purchase_detail_id, purchase_id, product_id, price, action_id, ad_id, payment_group_id)
                SELECT
                        purchase_detail_id, purchase_id, product_id, price, action_id, ad_id, payment_group_id
                FROM
                        purchase_detail
                        JOIN archive_purchase_ids USING (purchase_id)';

END;
$$ LANGUAGE plpgsql;
