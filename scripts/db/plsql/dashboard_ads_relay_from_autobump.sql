CREATE OR REPLACE FUNCTION dashboard_ads_relay_from_autobump(
) RETURNS TRIGGER AS $$
DECLARE
	l_pending integer;
	l_ad_id integer;
BEGIN
	--Code for premium services
	IF TG_OP = 'UPDATE' OR TG_OP = 'INSERT' THEN
		l_ad_id := NEW.ad_id;
	ELSIF TG_OP = 'DELETE' THEN
		l_ad_id := OLD.ad_id;
	END IF;

	SELECT count(*) INTO l_pending FROM autobump WHERE ad_id = l_ad_id AND status = 'pending';

	IF l_pending = 0 THEN
		l_pending = NULL;
	END IF;

	UPDATE dashboard_ads SET auto_bump = l_pending  WHERE ad_id = l_ad_id;

	RETURN NULL;
END;
$$ LANGUAGE plpgsql;
