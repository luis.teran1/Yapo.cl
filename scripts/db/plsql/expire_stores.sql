CREATE OR REPLACE FUNCTION expire_stores(OUT o_stores_expired INTEGER) AS $$
DECLARE
	l_rec record;
BEGIN

	o_stores_expired = 0;
	FOR l_rec IN (SELECT * FROM stores WHERE status = 'active' AND date_end < NOW() )LOOP
		PERFORM update_store_status(l_rec.store_id,'expired',NULL,NULL);
		o_stores_expired = o_stores_expired + 1;
	END LOOP;

END
$$ LANGUAGE plpgsql;
