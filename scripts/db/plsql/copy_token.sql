CREATE OR REPLACE FUNCTION copy_token(i_token_id tokens.token_id%TYPE,
				   i_schema varchar,
				   OUT o_token_id tokens.token_id%TYPE) AS $$
DECLARE
	l_token_id tokens.token_id%TYPE;
BEGIN
	o_token_id := i_token_id;

	IF i_token_id IS NULL THEN
		RETURN;
	END IF;

	EXECUTE 'SELECT token_id FROM ' || quote_ident(i_schema) || '.tokens WHERE token_id = ' || quote_literal(i_token_id) INTO l_token_id;
	IF l_token_id IS NOT NULL THEN
		RETURN;
	END IF;
	EXECUTE 'INSERT INTO ' || quote_ident(i_schema) || '.tokens SELECT * FROM tokens WHERE token_id = ' || quote_literal(i_token_id);
END;
$$ LANGUAGE plpgsql;
