CREATE OR REPLACE FUNCTION add_gallery(i_ad_id ads.ad_id%TYPE,
					i_days varchar,
					i_remote_addr tokens.remote_addr%TYPE,
					i_action_id integer,
					i_token_id action_states.token_id%TYPE,
					OUT o_email users.email%TYPE) AS $$
DECLARE
    l_action_id ad_actions.action_id%TYPE;
    l_state_id action_states.state_id%TYPE;
    l_days timestamp;
    l_start_from timestamp;
BEGIN
    SELECT
        email
    INTO
        o_email
    FROM
        ads JOIN users USING (user_id)
    WHERE
        ad_id = i_ad_id;

    IF NOT FOUND THEN
        RAISE EXCEPTION 'ERROR_AD_NOT_FOUND';
    END IF;


    IF i_action_id IS NULL THEN
        -- add ad_action and action_states
        l_action_id := insert_ad_action(i_ad_id, 'gallery', NULL);
        PERFORM insert_state(i_ad_id, l_action_id, 'reg', 'initial', i_remote_addr, i_token_id);
        l_state_id := insert_state(i_ad_id, l_action_id, 'accepted', 'adminclear', i_remote_addr, i_token_id);
    ELSE
        SELECT state_id, action_id
        INTO l_state_id, l_action_id
        FROM action_states
        WHERE ad_id = i_ad_id AND action_id = i_action_id AND  state = 'accepted';
    END IF;

	SELECT to_timestamp(value, 'YYYY-MM-DD HH24:MI:SS')
	INTO l_start_from
	FROM ad_params
	WHERE name='gallery' AND ad_id = i_ad_id;

	IF NOT FOUND THEN
		l_start_from := now();
	END IF;

    -- add addition of gallery ad_params to ad_changes
    PERFORM insert_ad_change(i_ad_id, l_action_id, l_state_id, true, 'gallery', to_char(l_start_from + (i_days)::INTERVAL, 'YYYY-MM-DD HH24:MI:SS'));

    -- apply ad_changes
    PERFORM apply_ad_changes(i_ad_id, l_action_id, NULL);

END
$$ LANGUAGE plpgsql;
