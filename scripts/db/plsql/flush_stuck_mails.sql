CREATE OR REPLACE FUNCTION flush_stuck_mails (
		OUT o_count integer
		) AS $$
DECLARE
	l_id mail_queue.mail_queue_id%TYPE;
	l_year integer;
	l_count integer;
BEGIN
	l_year := extract(year FROM CURRENT_TIMESTAMP);

	IF extract(doy FROM CURRENT_TIMESTAMP) <= 7 THEN
		EXECUTE '
			UPDATE
				' || quote_ident('blocket_' || (l_year - 1)) || '.mail_queue
			SET
				state = ''manual''
			WHERE
				state = ''reg''
				AND added_at BETWEEN CURRENT_TIMESTAMP - interval ''1 week'' AND CURRENT_TIMESTAMP - interval ''2 hour''';
		GET DIAGNOSTICS o_count = ROW_COUNT;
	ELSE
		o_count := 0;
	END IF;

	EXECUTE '
		UPDATE
			' || quote_ident('blocket_' || l_year) || '.mail_queue
		SET
			state = ''manual''
		WHERE
			state = ''reg''
			AND added_at BETWEEN CURRENT_TIMESTAMP - interval ''1 week'' AND CURRENT_TIMESTAMP - interval ''2 hour''';
	
	GET DIAGNOSTICS l_count = ROW_COUNT;
	o_count := coalesce(o_count, 0) + coalesce(l_count, 0);
END;
$$ LANGUAGE plpgsql;
