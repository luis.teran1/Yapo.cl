CREATE OR REPLACE FUNCTION review_refuse(
	i_ad_id ad_actions.ad_id%TYPE,
	i_action_id ad_actions.action_id%TYPE,
	i_token tokens.token%TYPE,
	i_remote_addr action_states.remote_addr%TYPE,
	i_filter_name filters.name%TYPE,
	i_reason state_params.value%TYPE,
	i_refusal_text state_params.value%TYPE,
	i_pack_conf varchar[][],
	OUT o_action_id ad_actions.action_id%TYPE,
	OUT o_action_type ad_actions.action_type%TYPE,
	OUT o_upselling_action_id ad_actions.action_id%TYPE,
	OUT o_remove_upselling_products boolean
) AS $$
DECLARE
	l_action_type ad_actions.action_type%TYPE;
	l_price payments.pay_amount%TYPE;
	l_ad_action_price payments.pay_amount%TYPE;
	l_user_id users.user_id%TYPE;
	l_state_id action_states.state_id%TYPE;
	l_action_state ad_actions.state%TYPE;
	l_ad_value payments.pay_amount%type;
	l_gallery_value ad_params.value%TYPE;
	l_label_value ad_params.value%TYPE;
    l_admin_id integer;
BEGIN
	SELECT
		action_type,
		user_id,
		ad_actions.state,
		gallery.value,
		label.value
	INTO
		l_action_type,
		l_user_id,
		l_action_state,
		l_gallery_value,
		l_label_value
	FROM
		ads
		JOIN ad_actions USING (ad_id)
		LEFT JOIN ad_params AS link_type ON
			link_type.ad_id = ads.ad_id
			AND link_type.name = 'link_type'
		LEFT JOIN ad_params AS gallery ON
			gallery.ad_id = ads.ad_id
			AND gallery.name = 'gallery'
		LEFT JOIN ad_params AS label ON
			label.ad_id = ads.ad_id
			AND label.name = 'label'
	WHERE
		ad_actions.ad_id = i_ad_id AND
		ad_actions.action_id = i_action_id AND
		(ads.store_id IS NULL OR link_type.ad_id IS NULL);
	IF NOT FOUND THEN
		RAISE EXCEPTION 'ERROR_NO_SUCH_ACTION';
	END IF;

	o_action_type := l_action_type;

	IF l_action_state = 'refused' THEN
		-- This action is already in state refused. 
		RAISE NOTICE 'ERROR_ACTION_ALREADY_REFUSED % % %', i_ad_id, i_action_id, i_token;
		RETURN;
	END IF;

	IF l_action_state in ('accepted', 'undo_deleted') THEN
		-- Post refusal.
		o_action_id := insert_ad_action(i_ad_id, 'post_refusal', NULL);

		-- Reset action_type.
		l_action_type := NULL;
	ELSE
		o_action_id := i_action_id;
	END IF;


	-- New state, include reason in params.
	l_state_id := insert_state(i_ad_id,
			     o_action_id, 
			     'refused',
			     'refuse', 
			     i_remote_addr, 
			     get_token_id(i_token));

	--Check Pack to make it 'free' to a slot
	PERFORM check_and_free_pack_slot(i_ad_id, o_action_id, l_state_id, i_pack_conf);

	--Chech upselling 

	SELECT proc.o_upselling_action_id, proc.o_remove_upselling_products
	INTO o_upselling_action_id, o_remove_upselling_products
	FROM check_upselling_refuse(i_ad_id, o_action_id, i_remote_addr, get_token_id(i_token)) AS proc;

	INSERT INTO state_params VALUES (i_ad_id,o_action_id,l_state_id,'reason',i_reason);
	INSERT INTO state_params VALUES (i_ad_id,o_action_id,l_state_id,'filter_name',i_filter_name);
	
	IF i_refusal_text IS NOT NULL THEN
		INSERT INTO state_params VALUES (i_ad_id,o_action_id,l_state_id,'refusal_text',	i_refusal_text);
	END IF;
	
	IF l_action_type = 'editrefused' THEN
		SELECT
			actype_before_refuse.o_action_type
		INTO
			l_action_type
		FROM
			action_type_before_refuse(i_ad_id, o_action_id, NULL) AS actype_before_refuse;
	END IF;

	-- If the action is new or accepted, set status refused
	IF l_action_type IS NULL OR l_action_type in ('new','undo_delete') THEN
		UPDATE ads SET status = 'refused' WHERE	ad_id = i_ad_id;
	END IF;

	-- Calculate how much money the user spent on the ad
	SELECT
		SUM(pay_amount - discount)
	INTO
		l_price
	FROM
		ad_actions
		JOIN payments USING (payment_group_id)
	WHERE
		ad_id = i_ad_id AND
		action_id = o_action_id;
	IF l_price IS NULL THEN
		l_price := 0;
	END IF;

	-- If the resufal is "refusal_divide_credit" then give the user double the amount of ad_action price, but only once per user
	IF i_reason = 'refusal_divide_credit' AND NOT EXISTS (SELECT value FROM user_params WHERE name = 'refusal_divide_credit' AND user_id = l_user_id) THEN
		-- Get the value of the ad_action payment
		SELECT
			pay_amount - discount, 
			l_price + pay_amount - discount
		INTO
			l_ad_action_price,
			l_price
		FROM
			payments JOIN
			ad_actions USING (payment_group_id)
		WHERE
			ad_actions.ad_id = i_ad_id AND
			ad_actions.action_type IN ('new', 'renew', 'prolong') AND
			payments.payment_type = 'ad_action'
		LIMIT 1;

		-- Add refusal_divide_credit param to the user
		INSERT INTO user_params	(user_id, name, value)
		VALUES(l_user_id,'refusal_divide_credit', l_ad_action_price);

	END IF;

	-- If the action_type was accepted then return total amount of money the ad is worth, cat price (+ extra images)
	IF l_action_state = 'accepted' THEN
		SELECT
			o_sum
		INTO
			l_ad_value
		FROM
			get_ad_value(i_ad_id);

		IF l_ad_value IS NOT NULL THEN
			l_price := l_ad_value;
		END IF;
	END IF;
	
	-- Add credit to the users account
	UPDATE
		users
	SET
		account = account + l_price
	WHERE
		user_id = l_user_id;

	-- We're refusing a gallery ad, we need to remove the gallery ad_param.
	IF l_gallery_value IS NOT NULL AND (l_action_type IS NULL OR l_action_type = 'new') THEN
		PERFORM remove_gallery(i_ad_id, i_remote_addr);
	END IF;

	-- We're refusing a label ad, we need to remove the label ad_param.
	IF l_label_value IS NOT NULL AND (l_action_type IS NULL OR l_action_type = 'new') THEN
		PERFORM remove_label(i_ad_id, i_remote_addr, 'controlpanel', get_token_id(i_token));
	END IF;

	-- Unlock the action in ad_queues
	DELETE
	FROM
		ad_queues
	WHERE
		ad_id = i_ad_id AND
		action_id = o_action_id;

    --Get the admin id to put in the review log
	--Only if it is a human action (not autoaccept)
	IF i_token IS NOT NULL THEN
    	SELECT 
    	   COALESCE(admin_id, 0)
    	INTO 
    	    l_admin_id 
    	FROM 
    	    tokens 
    	WHERE 
    	    token = i_token;
    	    
    	--Insert a row in the review log
    	INSERT INTO 
    	    review_log(
    	        ad_id,
    	        action_id, 
    	        admin_id, 
    	        queue, 
    	        action_type, 
    	        action,
    	        category,
    	        refusal_reason_text)
    	VALUES 
    	        (i_ad_id, 
    	        i_action_id, 
    	        COALESCE(l_admin_id, 0),
    	        i_filter_name, 
    	        COALESCE(l_action_type, (SELECT action_type FROM ad_actions WHERE ad_id = i_ad_id AND action_id = i_action_id)),
    	        'refused',
    	        (SELECT category FROM ads WHERE ad_id = i_ad_id),
    	        COALESCE(i_refusal_text, (SELECT value FROM conf WHERE key = '*.*.common.refusal.' || i_reason || '.name'), i_reason)
    	        );
	END IF; 
END
$$ LANGUAGE plpgsql;
