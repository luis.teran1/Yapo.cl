CREATE OR REPLACE FUNCTION update_filter_counts (i_rule_ids integer[]
) RETURNS void AS $$
DECLARE
	l_i integer;
	l_rule_id block_rules.rule_id%TYPE;
	l_count_date block_rules.count_date%TYPE;
BEGIN
	l_i := 1;
	LOOP
		l_rule_id := i_rule_ids[l_i];
		EXIT WHEN l_rule_id IS NULL;

		SELECT
			count_date
		INTO
			l_count_date
		FROM
			block_rules
		WHERE
			rule_id = l_rule_id;

		IF FOUND THEN
			-- Count
			IF l_count_date = CURRENT_DATE THEN
				UPDATE
					block_rules
				SET
					count_today = count_today + 1
				WHERE   
					rule_id = l_rule_id;
			ELSE            
				UPDATE          
					block_rules
				SET
					count_yesterday =
						CASE count_date
							WHEN CURRENT_DATE - interval '1 day' THEN count_today
							ELSE 0
						END,
					count_today = 1,
					count_date = CURRENT_DATE
				WHERE
					rule_id = l_rule_id;
			END IF;  
		END IF;
		l_i := l_i + 1;
	END LOOP;
END;
$$ LANGUAGE plpgsql;
