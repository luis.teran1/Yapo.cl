CREATE OR REPLACE FUNCTION copy_ad_from_archive(
				   i_schema varchar
				   ) RETURNS void AS $$
BEGIN
-- tokens
	RAISE NOTICE 'ARCHIVE: Locking schema % (row exclusive)', i_schema;
	EXECUTE 'LOCK ' || quote_ident(i_schema) || '.tokens IN ROW EXCLUSIVE MODE';

-- ads
	RAISE NOTICE 'ARCHIVE: Copying from ads';
	EXECUTE 
		'INSERT INTO ' ||
			quote_ident(i_schema) || '.ads
			(ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id,
			 passwd, salted_passwd, phone_hidden, company_ad, subject, body, price, infopage,
			 infopage_title, orig_list_time, old_price, store_id, modified_at, lang)
		SELECT
			ad_id, list_id, list_time, status::' || get_column_type(i_schema, 'ads', 'status') ||
			', type::' || get_column_type(i_schema, 'ads', 'type') || ', name, phone, region, city, category, user_id,
			passwd, salted_passwd, phone_hidden, company_ad, subject, body, price, infopage,
			infopage_title, orig_list_time, old_price, store_id, modified_at, lang
		FROM
			ads
			JOIN archive_ad_ids USING (ad_id)';

-- ad_params
	RAISE NOTICE 'ARCHIVE: Copying from ad_params';
	EXECUTE 
		'INSERT INTO ' ||
			quote_ident(i_schema) || '.ad_params
			(ad_id, name, value)
		SELECT
			ad_id, name::' || get_column_type(i_schema, 'ad_params', 'name') || ', value
		FROM
			ad_params
			JOIN archive_ad_ids USING (ad_id)';

-- notices
	RAISE NOTICE 'ARCHIVE: Copying from notices';
	EXECUTE 
		'INSERT INTO ' ||
			quote_ident(i_schema) || '.notices
			(notice_id, body, uid, user_id, ad_id, abuse, created_at, token_id)
		SELECT
			notice_id, body, uid, user_id, ad_id, abuse, created_at,
				copy_token(token_id, ' || quote_literal(i_schema) || ')
		FROM
			notices
			JOIN archive_ad_ids USING (ad_id)';

	PERFORM copy_payment_group_from_archive(i_schema);

-- ad_actions
	RAISE NOTICE 'ARCHIVE: Copying from ad_actions';
	EXECUTE 
		'INSERT INTO ' ||
			quote_ident(i_schema) || '.ad_actions
			(ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id)
		SELECT
			ad_id, action_id, action_type::' || get_column_type(i_schema, 'ad_actions', 'action_type') || 
			', current_state, state::' || get_column_type(i_schema, 'ad_actions', 'state') ||
			', queue::' || get_column_type(i_schema, 'ad_actions', 'queue') ||
			', locked_by, locked_until, payment_group_id
		FROM
			ad_actions
			JOIN archive_ad_ids USING (ad_id)';

	-- voucher actions
	RAISE NOTICE 'ARCHIVE: Copying from voucher_actions';
	EXECUTE
		'INSERT INTO ' ||
			quote_ident(i_schema) || '.voucher_actions
			(voucher_id, voucher_action_id, action_type, current_state, state, amount, payment_group_id)
		SELECT
			voucher_id, voucher_action_id, voucher_actions.action_type::' ||
			get_column_type (i_schema, 'voucher_actions', 'action_type') || ', voucher_actions.current_state, voucher_actions.state::' ||
			get_column_type (i_schema, 'voucher_actions', 'state') || ', amount, payment_group_id
		FROM
			voucher_actions
			JOIN ad_actions USING (payment_group_id)
			JOIN archive_ad_ids USING (ad_id)';

	-- voucher states
	RAISE NOTICE 'ARCHIVE: Copying from voucher_states';
	EXECUTE
		'INSERT INTO ' ||
			quote_ident(i_schema) || '.voucher_states
			(voucher_id, voucher_action_id, voucher_state_id, state, transition, timestamp, remote_addr,
			 token_id, resulting_balance)
		SELECT
			voucher_id, voucher_action_id, voucher_state_id, voucher_states.state::' ||
			get_column_type (i_schema, 'voucher_states', 'state') || ', transition::' ||
			get_column_type (i_schema, 'voucher_states', 'transition') || ', timestamp, remote_addr,
			copy_token(token_id, ' || quote_literal(i_schema) || '), resulting_balance
		FROM
			voucher_states
			JOIN voucher_actions USING (voucher_id, voucher_action_id)
			JOIN ad_actions USING (payment_group_id)
			JOIN archive_ad_ids USING (ad_id)';

-- action_params
	RAISE NOTICE 'ARCHIVE: Copying from action_params';
	EXECUTE 
		'INSERT INTO ' ||
			quote_ident(i_schema) || '.action_params
			(ad_id, action_id, name, value)
		SELECT
			ad_id, action_id, name::' || get_column_type(i_schema, 'action_params', 'name') || ', value
		FROM
			action_params
			JOIN archive_ad_ids USING (ad_id)';

-- action_states
	RAISE NOTICE 'ARCHIVE: Copying from action_states';
	EXECUTE 
		'INSERT INTO ' ||
			quote_ident(i_schema) || '.action_states
			(ad_id, action_id, state_id, state, transition, timestamp, remote_addr, token_id)
		SELECT
			ad_id, action_id, state_id, state::' || get_column_type(i_schema, 'action_states', 'state') ||
			', transition::' || get_column_type(i_schema, 'action_states', 'transition') ||
			', timestamp, remote_addr, copy_token(token_id, ' || quote_literal(i_schema) || ')
		FROM
			action_states
			JOIN archive_ad_ids USING (ad_id)';

-- state_params
	RAISE NOTICE 'ARCHIVE: Copying from state_params';
	EXECUTE 
		'INSERT INTO ' ||
			quote_ident(i_schema) || '.state_params
			(ad_id, action_id, state_id, name, value)
		SELECT
			ad_id, action_id, state_id, name::' || get_column_type (i_schema, 'state_params', 'name') || ', value
		FROM
			state_params
			JOIN archive_ad_ids USING (ad_id)';

-- Skip ad_image_changes

-- ad_changes
	RAISE NOTICE 'ARCHIVE: Copying from ad_changes';
	EXECUTE 
		'INSERT INTO ' ||
			quote_ident(i_schema) || '.ad_changes
			(ad_id, action_id, state_id, is_param, column_name, old_value, new_value)
		SELECT
			ad_id, action_id, state_id, is_param, column_name, old_value, new_value
		FROM
			ad_changes
			JOIN archive_ad_ids USING (ad_id)';


-- purchase family tables
	RAISE NOTICE 'ARCHIVE: Copying from purchase';
	EXECUTE 
		'INSERT INTO ' ||
			quote_ident(i_schema) || '.purchase
			(purchase_id, doc_type, doc_num, external_doc_id, status, receipt, delivery, rut, name, lob, address, region, communes, contact, email, tax, discount, total_price, payment_group_id, seller_id, payment_method, payment_platform, account_id)
		SELECT
			purchase_id, doc_type, doc_num, external_doc_id, status, receipt, delivery, rut, name, lob, address, region, communes, contact, email, tax, discount, total_price, payment_group_id, seller_id, payment_method, payment_platform, account_id
		FROM
			purchase
			JOIN archive_purchase_ids USING (purchase_id)';
	
	RAISE NOTICE 'ARCHIVE: Copying from purchase_states';
	EXECUTE 
		'INSERT INTO ' ||
			quote_ident(i_schema) || '.purchase_states
			(purchase_id, purchase_state_id, status, timestamp, remote_addr)
		SELECT
			purchase_id, purchase_state_id, status, timestamp, remote_addr
		FROM
			purchase_states
			JOIN archive_purchase_ids USING (purchase_id)';
	
	RAISE NOTICE 'ARCHIVE: Copying from purchase_detail';
	EXECUTE 
		'INSERT INTO ' ||
			quote_ident(i_schema) || '.purchase_detail
			(purchase_detail_id, purchase_id, product_id, price, action_id, ad_id)
		SELECT
			purchase_detail_id, purchase_id, product_id, price, action_id, ad_id
		FROM
			purchase_detail
			JOIN archive_purchase_ids USING (purchase_id)';

END;
$$ LANGUAGE plpgsql;
