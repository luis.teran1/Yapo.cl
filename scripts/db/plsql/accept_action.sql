CREATE OR REPLACE FUNCTION accept_action(
						i_ad_id ads.ad_id%TYPE,
						i_action_id ad_actions.action_id%TYPE,
						i_transition action_states.transition%TYPE,
						i_remote_addr action_states.remote_addr%TYPE,
						i_token tokens.token%TYPE,
						i_gallery_expire_minutes integer,
						i_action_type ad_actions.action_type%TYPE,
						i_accepted_state_id action_states.state_id%TYPE,
						OUT o_state_id action_states.state_id%TYPE) AS $$
DECLARE
	l_state_id action_states.state_id%TYPE;
	l_old_price ads.price%TYPE;
	l_new_price ads.price%TYPE;
BEGIN
	IF i_accepted_state_id IS NULL THEN
		-- Find the latest state that changes the images, if any.
		SELECT
			MAX(state_id)
		INTO
			l_state_id
		FROM
			action_states
			JOIN ad_image_changes USING (ad_id, action_id, state_id)
		WHERE
			ad_id = i_ad_id AND
			action_id = i_action_id;

		o_state_id := insert_state(i_ad_id, 
					i_action_id, 
					'accepted', 
					i_transition, 
					i_remote_addr, 
					get_token_id(i_token));
	ELSE
		l_state_id := i_accepted_state_id;
		o_state_id := i_accepted_state_id;
	END IF;

	IF i_gallery_expire_minutes IS NOT NULL THEN
		IF get_changed_value (
				i_ad_id,
				i_action_id,
				true,
				'gallery'
				) = 'request' THEN
			PERFORM insert_ad_change (
					i_ad_id,
					i_action_id,
					o_state_id,
					true,
					'gallery',
					(CURRENT_TIMESTAMP + interval '1 minute' * i_gallery_expire_minutes)::text
					);
		END IF;
	END IF;

	PERFORM apply_ad_changes(i_ad_id, i_action_id, l_state_id);

	-- Set old_price if the price changed with this action.
	SELECT
		old_value,
		new_value
	INTO
		l_old_price,
		l_new_price
	FROM
		ad_changes
	WHERE
		ad_id = i_ad_id AND action_id = i_action_id AND column_name = 'price'
	ORDER BY
		state_id DESC
	LIMIT 1;

	IF FOUND THEN
		IF l_old_price != l_new_price THEN
			UPDATE
				ads
			SET
				old_price = l_old_price
			WHERE
				ad_id = i_ad_id;
		END IF;
	END IF;
END;
$$ LANGUAGE plpgsql;
