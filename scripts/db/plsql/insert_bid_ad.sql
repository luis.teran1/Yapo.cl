CREATE OR REPLACE FUNCTION insert_bid_ad(
				i_bid_ad_id bid_ads.bid_ad_id%TYPE,
				i_campaign_id bid_ads.campaign_id%TYPE,
				i_list_time bid_ads.list_time%TYPE,
				i_end_time bid_ads.end_time%TYPE,
				i_category bid_ads.category%TYPE,
				i_images varchar[],
				i_name bid_ads.name%TYPE,
				i_subject bid_ads.subject%TYPE,
				i_body bid_ads.body%TYPE,
				i_video bid_media.media_id%TYPE,
				OUT o_bid_ad_id bid_ads.bid_ad_id %TYPE
			) AS $$
DECLARE
	l_list_time bid_ads.list_time%TYPE;
	l_i integer;
	l_image_name varchar;
BEGIN
	IF i_bid_ad_id IS NOT NULL THEN
		SELECT 
			bid_ad_id, list_time
		FROM 
			bid_ads 
		INTO 
			o_bid_ad_id, l_list_time
		WHERE 
			bid_ad_id = i_bid_ad_id;

		IF NOT FOUND THEN
			RAISE EXCEPTION 'FACTORY_TRANSLATE:bid_ad_id:ERROR_BID_AD_NOT_FOUND';
		END IF;

		UPDATE 
			bid_ads
		SET
			campaign_id = i_campaign_id,
			list_time = CASE WHEN i_list_time IS NOT NULL THEN i_list_time ELSE l_list_time END,
			end_time = i_end_time,
			name = i_name,
			subject = i_subject,
			body = i_body,
			category = i_category
		WHERE
			bid_ad_id = i_bid_ad_id;

		UPDATE 
			bid_media
		SET
			bid_ad_id = NULL
		WHERE
			bid_ad_id = i_bid_ad_id;

		l_i := 0;
		IF i_images IS NOT NULL THEN
			LOOP
				l_image_name := i_images[l_i + 1];
				EXIT WHEN l_image_name IS NULL;
				
				UPDATE 
					bid_media
				SET
					bid_ad_id = o_bid_ad_id,
					seq_no = l_i
				WHERE
					media_id = TRIM(trailing '.jpg' FROM l_image_name)::bigint;
				l_i := l_i + 1;
					
			END LOOP;
		END IF;	

		IF i_video IS NOT NULL THEN
			UPDATE
				bid_media
			SET
				bid_ad_id = o_bid_ad_id,
				seq_no = l_i
			WHERE
				media_id = i_video;
			l_i := l_i + 1;
		END IF;
		
	ELSE			
		INSERT INTO 
			bid_ads (campaign_id, list_time, end_time, name, subject, body, category) 
		VALUES (
			i_campaign_id,
			CASE WHEN i_list_time IS NOT NULL THEN 
				i_list_time 
			ELSE 
				CURRENT_TIMESTAMP
			END,
			i_end_time,
			i_name,
			i_subject,
			i_body,
			i_category)
		RETURNING
			bid_ad_id
		INTO
			o_bid_ad_id
		;

		l_i := 0;
		IF i_images IS NOT NULL THEN
			LOOP
				l_image_name := i_images[l_i + 1];
				EXIT WHEN l_image_name IS NULL;
				
				UPDATE 
					bid_media
				SET
					bid_ad_id = o_bid_ad_id,
					seq_no = l_i
				WHERE
					media_id = TRIM(trailing '.jpg' FROM l_image_name)::bigint;
				l_i := l_i + 1;
					
			END LOOP;
		END IF;

		IF i_video IS NOT NULL THEN
			UPDATE
				bid_media
			SET
				bid_ad_id = o_bid_ad_id,
				seq_no = l_i
			WHERE
				media_id = i_video;
			l_i := l_i + 1;
		END IF;
	END IF;
END
$$ LANGUAGE plpgsql;
