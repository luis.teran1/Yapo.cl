CREATE OR REPLACE FUNCTION view_all_ad_versions(i_schema text,
						i_ad_id action_states.ad_id%TYPE) RETURNS SETOF v_ads AS $$
DECLARE
	l_action_id ad_actions.action_id%TYPE;
	l_ad v_ads;
BEGIN
	FOR
		l_action_id
	IN	
		EXECUTE 'SELECT
				ad_actions.action_id
			FROM 
				' || i_schema || E'.ad_actions
			WHERE
				ad_actions.ad_id = ' || i_ad_id || '
			ORDER BY
				ad_actions.action_id'
	LOOP		
		SELECT 
		 	ad_id,
			list_id,
			list_time,
			status::varchar,
			type::varchar,
			name,
			phone,
			region,
			city,
			category,
			user_id,
			salted_passwd,
			phone_hidden,
			no_salesmen,
       			company_ad,
       			subject,
       			body,
       			price,
       			infopage,
       			infopage_title,
       			store_id
		INTO
			l_ad	
		FROM	
			view_ad(i_schema, i_ad_id, l_action_id, -1);

		RETURN NEXT l_ad;	
	END LOOP;
END
$$ LANGUAGE plpgsql;
