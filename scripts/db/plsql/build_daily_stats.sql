CREATE OR REPLACE FUNCTION build_daily_stats(
    i_date      date,
    i_list_id   INTEGER,
    i_total     INTEGER,
	i_type      TEXT
) RETURNS VOID AS $$
DECLARE 
	l_yesterday_total INTEGER;
	l_list_id INTEGER;
	l_today_year TEXT;
	l_today_month TEXT;
	l_today_day TEXT;
	l_yesterday_year TEXT;
	l_yesterday_month TEXT;
	l_yesterday_day TEXT;
	l_visits INTEGER;

/* Calculate the daily visits with the (VIEW|MAIL):total
 * members of redisstats and insert the difference
 * with the "yesterday" total into postgres db on
 * a daily basis */

BEGIN
	l_today_year := lpad(EXTRACT(year FROM i_date::date)::TEXT, 4, '0')::TEXT;
	l_today_month := lpad(EXTRACT(month FROM i_date::date)::TEXT, 2, '0')::TEXT;
	l_today_day := lpad(EXTRACT(day FROM i_date::date)::TEXT, 2, '0')::TEXT;

	l_yesterday_year := lpad(EXTRACT(year FROM (i_date - INTERVAL '1 day')::date)::TEXT, 4, '0')::TEXT;
	l_yesterday_month := lpad(EXTRACT(month FROM (i_date - INTERVAL '1 day')::date)::TEXT, 2, '0')::TEXT;
	l_yesterday_day := lpad(EXTRACT(day FROM (i_date - INTERVAL '1 day')::date)::TEXT, 2, '0')::TEXT;

	-- Can't save info for deleted ads
	IF NOT EXISTS( SELECT * FROM ads WHERE list_id = i_list_id and status != 'deleted') THEN
		RETURN;
	END IF;

	-- Get yesterday's visits
	EXECUTE '
		SELECT
			total
		FROM
			blocket_' || l_yesterday_year || '.' || i_type || '_' || l_yesterday_year || l_yesterday_month || ' a
		WHERE
			a.date::date = ''' || i_date ||'''::date - INTERVAL ''1 day''
			AND a.list_id = ' || i_list_id
		INTO
			l_yesterday_total;

	-- Maybe the ad was inserted today and l_yesterday_total cannot be NULL
	IF l_yesterday_total IS NULL THEN
		l_yesterday_total = i_total;
	END IF;


	l_visits := i_total - l_yesterday_total;

	-- The user delete the ad and someone saw it before rebuild_index 
	IF l_visits < 0 THEN
		RETURN;
	END IF;

	-- Does the record corresponding to i_date exist?
	EXECUTE '
		SELECT list_id
		FROM blocket_' || l_today_year || '.' || i_type || '_' || l_today_year || l_today_month || '
		WHERE list_id = '|| i_list_id ||'
		AND date = '''|| i_date ||''''
		INTO l_list_id;

	IF l_list_id IS NULL THEN
		-- The record for today doesn't exist, we insert a new one
		EXECUTE '
			INSERT INTO blocket_' || l_today_year || '.' || i_type || '_' || l_today_year || l_today_month || '
			(list_id, date, visits, total)
			VALUES (
				' || i_list_id || ',
				''' || i_date || ''',
				' || l_visits ||',
				' || i_total || '
			)';
	ELSE
		-- Update the existing record with new info of visits
		EXECUTE '
			UPDATE blocket_' || l_today_year || '.' || i_type || '_' || l_today_year || l_today_month || '
			SET
				visits = ' || l_visits ||',
				total = ' || i_total || '
			WHERE list_id = ' || i_list_id || '
			AND date = '''|| i_date ||'''';
		-- Isn't a new value, return 0?
	END IF;

END;
$$ LANGUAGE plpgsql;
