CREATE OR REPLACE FUNCTION create_regress_stats_structure()
RETURNS VOID AS $$
DECLARE
	l_date DATE;
	l_interval varchar;
	l_schema varchar;
BEGIN
	FOR i IN 0..11 LOOP
		l_date := CURRENT_DATE - INTERVAL '1 MONTH' * i;
		l_schema := 'blocket_' || extract(year from l_date);
		RAISE NOTICE 'DROPING TABLE %.view_% IF EXISTS', l_schema, TO_CHAR(l_date, 'yyyymm');
		EXECUTE format('DROP TABLE IF EXISTS %s.view_%s;', l_schema, TO_CHAR(l_date, 'yyyymm'));
		RAISE NOTICE 'CREATING TABLE %.view_%', l_schema, TO_CHAR(l_date, 'yyyymm');
		EXECUTE 
			format('CREATE TABLE IF NOT EXISTS %s.view_%s (
					view_id SERIAL PRIMARY KEY, 
					list_id INTEGER, 
					date DATE, 
					visits INTEGER,
					total INTEGER
					);', l_schema, TO_CHAR(l_date, 'yyyymm'));
		RAISE NOTICE 'DROPING TABLE %.mail_% IF EXISTS', l_schema, TO_CHAR(l_date, 'yyyymm');
		EXECUTE format('DROP TABLE IF EXISTS %s.mail_%s;', l_schema, TO_CHAR(l_date, 'yyyymm'));
		RAISE NOTICE 'CREATING TABLE %.mail_%', l_schema, TO_CHAR(l_date, 'yyyymm');
		EXECUTE 
			format('CREATE TABLE IF NOT EXISTS %s.mail_%s (
					mail_id SERIAL PRIMARY KEY, 
					list_id INTEGER, 
					date DATE, 
					visits INTEGER,
					total INTEGER
					);', l_schema, TO_CHAR(l_date, 'yyyymm'));
	END LOOP;
END
$$ LANGUAGE plpgsql;
