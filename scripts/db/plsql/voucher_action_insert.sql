CREATE OR REPLACE FUNCTION voucher_action_insert(i_voucher_id vouchers.voucher_id%type,
		i_action_type voucher_actions.action_type%type,
		i_amount voucher_actions.amount%type,
		i_payment_group_id voucher_actions.payment_group_id%type,
		OUT o_voucher_action_id voucher_actions.voucher_action_id%TYPE) AS $$
BEGIN
	INSERT INTO
		voucher_actions
		(voucher_id,
		 action_type,
		 amount,
		 payment_group_id)
	VALUES
		(i_voucher_id,
		 i_action_type,
		 i_amount,
		 i_payment_group_id)
	RETURNING
			voucher_action_id
	INTO
			o_voucher_action_id;
END;
$$ LANGUAGE plpgsql;
