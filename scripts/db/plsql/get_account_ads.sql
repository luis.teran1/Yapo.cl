CREATE OR REPLACE FUNCTION get_account_ads(
					i_user_id 				users.user_id%TYPE,
					i_account_id 			accounts.account_id%TYPE,
					i_query 		        varchar,
					i_pack_sort				integer,
					i_all_params			integer,
					OUT o_ad_id          	ads.ad_id%TYPE,
					OUT o_list_id        	ads.list_id%TYPE,
					OUT o_list_time      	ads.list_time%TYPE,
					OUT o_subject        	ads.subject%TYPE,
					OUT o_body				ads.body%TYPE,
					OUT o_company_ad		integer,
					OUT o_ad_type			ads.type%TYPE,
					OUT o_category          ads.category%TYPE,
					OUT o_price          	ads.price%TYPE,
					OUT o_ad_status        	varchar,
					OUT o_ad_media_id    	BIGINT,
					OUT o_image_count		BIGINT,
					OUT o_daily_bump		varchar,
					OUT o_weekly_bump		varchar,
					OUT o_label				varchar,
					OUT o_gallery_date		varchar,
					OUT o_ad_pack_status	varchar,
					OUT o_ad_params_list	varchar
) RETURNS SETOF record AS $$
DECLARE
	l_account_status 	accounts.status%TYPE;
	l_rec 			record;
	l_action_type 		ad_actions.action_type%TYPE;
	l_action_state 		ad_actions.state%TYPE;
	l_subject_changed	TEXT;
	l_category_changed  TEXT;
	l_price_changed	        TEXT;
	l_image_0_changed 	TEXT;
	l_image_count_changed	integer;
	l_time_min timestamp;
BEGIN
	IF i_user_id IS NOT NULL THEN
		SELECT
			status
		INTO
			l_account_status
		FROM
			accounts
		WHERE
			user_id = i_user_id;
	ELSE
		SELECT
			user_id,
			status
		INTO
			i_user_id,
			l_account_status
		FROM
			accounts
		WHERE
			 account_id = i_account_id;
	END IF;

	IF NOT FOUND THEN
		RAISE EXCEPTION 'ERROR_ACCOUNT_NOT_FOUND';
	END IF;

	l_time_min = CURRENT_TIMESTAMP - (INTERVAL '60 minutes');

	FOR
		l_rec
	IN
		SELECT
			ads.ad_id,
			ads.list_id,
			ads.list_time,
			ads.subject,
			REPLACE(body, E'\n', '<br>') as body,
			ads.category,
			ads.price,
			ads.status,
			ads.type,
			ads.company_ad,
			aac.action_id,
			aac.action_type,
			aac.state,
			aac.current_state,
			ad_media_id,
			CASE WHEN ap1.name = 'daily_bump' THEN ap1.value
			 	 WHEN ap1.name = 'upselling_daily_bump' THEN (ap1.value::INTEGER - 1)::VARCHAR
			END as daily_bump,
			CASE WHEN ap2.name = 'weekly_bump' THEN ap2.value
			 	 WHEN ap2.name = 'upselling_weekly_bump' THEN (ap2.value::INTEGER - 1)::VARCHAR
			END as weekly_bump,
			ap3.value as gallery_date,
			ap4.value as pack_status,
			ap5.value as label
		FROM
			ads
			INNER JOIN ad_actions 		aac USING(ad_id)
			INNER JOIN action_states 	act ON (aac.ad_id = act.ad_id AND aac.action_id = act.action_id AND aac.current_state = act.state_id)
			LEFT JOIN ad_media ON (ad_media.ad_id = ads.ad_id AND ad_media.seq_no = 0)
			LEFT JOIN ad_params ap1 ON (ads.ad_id = ap1.ad_id AND (ap1.name = 'daily_bump' OR ap1.name = 'upselling_daily_bump'))
			LEFT JOIN ad_params ap2 ON (ads.ad_id = ap2.ad_id AND (ap2.name = 'weekly_bump' OR ap2.name = 'upselling_weekly_bump'))
			LEFT JOIN ad_params ap3 ON (ads.ad_id = ap3.ad_id AND ap3.name = 'gallery')
			LEFT JOIN ad_params ap4 ON (ads.ad_id = ap4.ad_id AND ap4.name = 'pack_status')
			LEFT JOIN ad_params ap5 ON (ads.ad_id = ap5.ad_id AND ap5.name = 'label')

		WHERE
			ads.user_id = i_user_id
		and aac.action_id = (select max(action_id) from ad_actions where ad_id = ads.ad_id )
		and ads.status != 'hidden'
		and (
			CASE
				WHEN ads.status = 'refused' THEN aac.action_type = 'editrefused' AND act.state != 'refused'
				WHEN ads.status = 'deleted' AND act.timestamp < l_time_min  THEN false
				ELSE true
			END
		)
		and (
			CASE
				WHEN i_query is not null THEN lower(ads.subject) like '%'||i_query||'%'
				ELSE true
			END
		)
				
	ORDER BY
		(CASE
			WHEN i_pack_sort IS NULL THEN 1
			WHEN i_pack_sort = 1 AND ap4.value = 'active' THEN 1
			WHEN i_pack_sort = 1 AND ap4.value = 'disabled' THEN 2
			WHEN i_pack_sort = 0 AND ap4.value = 'active' THEN 2
			WHEN i_pack_sort = 0 AND ap4.value = 'disabled' THEN 1
			ELSE 3
		END),
		(CASE
			WHEN ads.status = 'inactive' THEN 1
			WHEN ads.status = 'refused' THEN 2
			ELSE 3
		END),
		ads.list_time DESC,
		ads.ad_id DESC

	LOOP
		o_ad_id        := l_rec.ad_id;
		o_list_id      := l_rec.list_id;
		o_list_time    := l_rec.list_time;
		o_subject      := l_rec.subject;
		o_body         := l_rec.body;
		o_category     := l_rec.category;
		o_price        := l_rec.price;
		o_ad_status    := l_rec.status;
		o_ad_type      := l_rec.type;
		o_company_ad   := l_rec.company_ad::INTEGER;
		o_ad_media_id  := l_rec.ad_media_id;
		l_action_type  := l_rec.action_type;
		l_action_state := l_rec.state;
		o_daily_bump   := l_rec.daily_bump;
		o_weekly_bump  := l_rec.weekly_bump;
		o_gallery_date := l_rec.gallery_date;
		o_label        := l_rec.label;

		IF i_all_params IS NOT NULL THEN
			SELECT STRING_AGG(name || ',' || REPLACE(value, E'\n', '<br>'), '|&|') INTO o_ad_params_list FROM ad_params WHERE ad_id = o_ad_id;
		END IF;

		SELECT
			COUNT(1)
		INTO
			o_image_count
		FROM
			ad_media
		WHERE
			ad_media.ad_id = l_rec.ad_id;

		IF l_action_type = 'editrefused' AND NOT (o_ad_status = 'disabled' and l_action_state = 'accepted') THEN
			o_ad_status := l_action_type;

			IF l_rec.state = 'refused' AND l_rec.action_type = 'new' THEN
				o_ad_status := 'refused';
			END IF;

			select new_value
			into l_subject_changed
			from ad_changes
			where ad_id = l_rec.ad_id
			      and action_id = l_rec.action_id
				 and column_name = 'subject' ;
			IF FOUND THEN
				o_subject := l_subject_changed;
			END IF;

			select new_value
			into l_category_changed
			from ad_changes
			where ad_id = l_rec.ad_id
			      and action_id = l_rec.action_id
				 and column_name = 'category' ;
			IF FOUND THEN
				o_category := l_category_changed;
			END IF;

			select new_value
			into l_price_changed
			from ad_changes
			where ad_id = l_rec.ad_id
			      and action_id = l_rec.action_id
				 and column_name = 'price' ;
			IF FOUND THEN
				o_price := cast(nullif(l_price_changed,'') as integer);
			END IF;


			select substring(name from 0 for position('.jpg' in name )) as id
			INTO l_image_0_changed
			from ad_image_changes
			where ad_id = l_rec.ad_id
				and action_id = l_rec.action_id
				and seq_no = 0;
			IF FOUND THEN
				o_ad_media_id := cast(nullif(l_image_0_changed,'') as BIGINT);
			END IF;


			select count(1)
			INTO l_image_count_changed
			FROM ad_image_changes
			WHERE ad_id = l_rec.ad_id
				AND name IS NOT NULL
				AND name != ''
				and action_id = l_rec.action_id;
			IF FOUND THEN
				o_image_count := l_image_count_changed;
			END IF;

		END IF;

		-- This is used to recognize if the ad is a pack ad being shown
		SELECT
			CASE
				WHEN new_value = 'deactivated' THEN '0'
				WHEN new_value = 'active' THEN '1'
				WHEN new_value = 'disabled' THEN '0'
			END
		INTO o_ad_pack_status
		FROM ad_changes
		WHERE ad_id = l_rec.ad_id AND column_name = 'pack_status'
		ORDER BY action_id DESC
		LIMIT 1;

		IF NOT FOUND THEN
			o_ad_pack_status = '';
		END IF;

		-- The special cases
		IF o_ad_status = 'active' THEN
			-- This is used to recognize if the ad is a pack ad being shown
			IF o_ad_pack_status = '1' THEN
				o_ad_status = 'pack_show';
			END IF;

			IF EXISTS (SELECT 'edited' FROM ad_actions WHERE state = 'pending_review' AND ad_id = o_ad_id LIMIT 1) THEN
				o_ad_status = 'review-edit';
			END IF;

			IF l_action_type = 'status_change' AND l_action_state = 'accepted' AND o_ad_status != 'pack_show' THEN
				o_ad_status = 'admin_show';
			END IF;

			IF l_action_type  = 'undo_delete' AND l_action_state = 'undo_deleted' THEN
				o_ad_status = 'undo_deleted';
			END IF;

			IF l_action_type  = 'editrefused' AND l_action_state = 'undo_deleted' THEN
				o_ad_status = 'undo_deleted';
			END IF;
		END IF;

		IF o_ad_status = 'disabled' THEN
			IF EXISTS (SELECT 'edited' FROM ad_actions WHERE state = 'pending_review' AND ad_id = o_ad_id LIMIT 1) THEN
				o_ad_status = 'review-edit-disabled';
			END IF;
		END IF;

		RETURN NEXT;
	END LOOP;
END;
$$ LANGUAGE plpgsql;
