CREATE OR REPLACE FUNCTION ad_history(
	i_ad_id ad_actions.ad_id%TYPE,
	i_schema text,
	OUT o_action_type ad_actions.action_type%TYPE,
	OUT o_state varchar,
	OUT o_transition varchar,
	OUT o_timestamp action_states.timestamp%TYPE,
	OUT o_admin admins.username%TYPE) RETURNS SETOF record AS $$
DECLARE
	l_rec record;
BEGIN
	FOR
		l_rec
	IN
		EXECUTE 'SELECT	
			action_type::varchar, 
			action_states.state::varchar, 
			transition::varchar, 
			timestamp, 
			username as admin
		FROM
			' || quote_ident(i_schema) || '.ad_actions 
			JOIN ' || quote_ident(i_schema) || '. action_states USING (ad_id, action_id) 
			LEFT JOIN ' || quote_ident(i_schema) || '.tokens USING (token_id) 
			LEFT JOIN admins USING (admin_id)
		WHERE
			ad_id = ' || quote_literal(i_ad_id) || '
		ORDER BY 
			state_id'
        LOOP
		o_action_type := l_rec.action_type;
		o_state := l_rec.state;
		o_transition := l_rec.transition;
		o_timestamp := l_rec.timestamp;
		o_admin := l_rec.admin;

		RETURN NEXT;
	END LOOP;
END
$$ LANGUAGE plpgsql;
