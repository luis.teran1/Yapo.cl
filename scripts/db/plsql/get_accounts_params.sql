CREATE OR REPLACE FUNCTION get_accounts_params(
	i_uid                 users.uid%TYPE,
	i_email               users.email%TYPE,
	i_pack_conf           varchar[][],
	i_insertingfee_conf   varchar[][],
	OUT o_account_id      accounts.account_id%TYPE,
	OUT o_email           accounts.email%TYPE,
	OUT o_pack_type       varchar,
	OUT o_first_pack_type varchar
) RETURNS SETOF record AS $$
DECLARE
	l_rec   record;
	l_pack_type  varchar;
BEGIN

	FOR
		l_rec
	IN
        SELECT
            acc.account_id,
            users.email,
            acc_p.value as param_value,
            p3.value as first_pack_type
        FROM
            users
        INNER JOIN accounts acc USING(user_id)
        LEFT JOIN account_params acc_p ON acc_p.account_id = acc.account_id AND acc_p.name ='is_pro_for'
        LEFT JOIN account_params p3 ON (p3.account_id = acc.account_id AND p3.name = 'pack_type')
        WHERE
            users.uid = i_uid
        OR
            users.email = i_email
	LOOP
		o_account_id    := l_rec.account_id;
		o_email      := l_rec.email;
		o_first_pack_type := l_rec.first_pack_type;
        SELECT
            string_agg(pack_type, ',')  INTO o_pack_type
        FROM
        (
            SELECT
                DISTINCT find_pack_type(cat::INTEGER, array_cat(i_pack_conf, i_insertingfee_conf))  AS pack_type
            FROM
                unnest(string_to_array(l_rec.param_value, ',')) AS cat
        ) AS ptypes
        WHERE
            pack_type != ''
            OR pack_type IS NOT NULL;

		RETURN NEXT;
	END LOOP;
END;
$$ LANGUAGE plpgsql;
