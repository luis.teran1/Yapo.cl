CREATE OR REPLACE FUNCTION add_to_whitelist(i_email blocked_items.value%TYPE,
					i_notice blocked_items.notice%TYPE
					  ) RETURNS bool AS $$
DECLARE
	l_whitelist_id INTEGER;
	l_were_whitelist_id INTEGER;
	l_updated BOOLEAN;
BEGIN
	l_updated := false;
	SELECT list_id INTO l_whitelist_id FROM block_lists WHERE list_name = 'whitelist';
	SELECT list_id INTO l_were_whitelist_id FROM block_lists WHERE list_name = 'were_whitelist';

	DELETE FROM blocked_items WHERE list_id = l_were_whitelist_id AND value = i_email;

	-- validate if exist
	IF NOT EXISTS (SELECT item_id FROM blocked_items WHERE value = i_email AND list_id = l_whitelist_id) THEN
		l_updated := add_blocked_item(NULL, i_email, l_whitelist_id, i_notice, '');
	END IF;

	RETURN l_updated;
END;
$$ LANGUAGE plpgsql;
