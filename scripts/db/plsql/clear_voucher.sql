CREATE OR REPLACE FUNCTION clear_voucher(i_ad_id ads.ad_id%TYPE, 
					i_action_id ad_actions.action_id%TYPE,
					i_store_id stores.store_id%TYPE,
					i_ref_types varchar[],
					i_references varchar[],
					i_remote_addr action_states.remote_addr%TYPE,
					i_token tokens.token%TYPE,
					i_auto_move_abuse bool,
					i_no_auto_categories integer[],
					i_category_price_limits integer[][],
					i_global_price_limit integer,
					i_blocked_items_lists integer[],
					i_queue_prios varchar[][],
					i_gallery_expire_minutes integer,
					OUT o_queue ad_actions.queue%TYPE,
					OUT o_balance vouchers.balance%TYPE,
					OUT o_action_type ad_actions.action_type%TYPE) AS $$
DECLARE
	l_user_id ads.user_id%TYPE;
	l_payment_group_id payment_groups.payment_group_id%TYPE;
	l_code pay_log.code%TYPE;
	l_tduid action_params.value%TYPE;
	l_cat_price payments.pay_amount%TYPE;
	l_pay_type pay_log.pay_type%TYPE;
	l_clear_status pay_log.status%TYPE;
	l_sum_price payments.pay_amount%TYPE;
	l_voucher_id vouchers.voucher_id%TYPE;
	l_voucher_action_id voucher_actions.voucher_action_id%TYPE;
	l_should_review bool;
BEGIN
	-- Lock to avoid double clear.
	PERFORM
		1
	FROM
		ad_actions
	WHERE
		ad_id = i_ad_id AND
		action_id = i_action_id
	FOR UPDATE;

	SELECT
		o_user_id
	INTO
		l_user_id
	FROM
		get_user_id_by_ad_id(i_ad_id);
	SELECT
		SUM(pay_amount - discount),
		SUM(CASE payment_type WHEN 'ad_action' THEN pay_amount - discount ELSE 0 END),
		action_type,
		payment_group_id
	INTO
		l_sum_price,
		l_cat_price,
		o_action_type,
		l_payment_group_id
	FROM
		ad_actions
		JOIN payment_groups USING (payment_group_id)
		JOIN payments USING (payment_group_id)
	WHERE
		state in ('unpaid', 'unverified') AND
		payment_groups.status in ('unpaid', 'unverified') AND
		ad_id = i_ad_id AND
		action_id = i_action_id
	GROUP BY
		action_type,
		payment_group_id,
		payment_groups.code;

	IF l_sum_price IS NULL OR l_sum_price = 0 THEN
		RAISE EXCEPTION 'No payment for ad: %, action: %', i_ad_id, i_action_id;
	END IF;
	
	SELECT
		voucher_id
	INTO
		l_voucher_id
	FROM
		vouchers
	WHERE
		store_id = i_store_id;

	IF NOT FOUND THEN
		RAISE EXCEPTION 'ERROR_NO_VOUCHER';
	END IF;

	-- Register the new voucher action.
	l_voucher_action_id := voucher_action_insert(l_voucher_id,
			'pay',
			-l_sum_price,
			l_payment_group_id);
	SELECT
		vsi.o_balance
	INTO
		o_balance
	FROM
		voucher_state_insert(l_voucher_id,
				l_voucher_action_id,
				'used',
				'pay',
				'payvoucher',
				NULL,
				i_remote_addr,
				get_token_id(i_token),
				i_ref_types,
				i_references,
				l_payment_group_id) AS vsi;

	-- Update the payments table, setting amount and pay_time
	UPDATE
		payment_groups
	SET
		status = 'paid'
	WHERE
		payment_group_id = l_payment_group_id;
	
	IF o_action_type = 'gallery' THEN
		l_should_review := FALSE;
	ELSE
		l_should_review := TRUE;
	END IF;
	
	IF l_should_review THEN 
	-- The action will be moved to "pending_review" after clearing.
		PERFORM insert_state(i_ad_id, 
				     i_action_id, 
				     'pending_review', 
				      'pay', 
			 	      i_remote_addr, 
			   	      get_token_id(i_token));
		o_queue := clear_move_action_to_queue(i_ad_id, i_action_id, l_user_id, i_ref_types, i_references, i_remote_addr, i_auto_move_abuse, i_no_auto_categories, i_category_price_limits, i_global_price_limit, i_blocked_items_lists, i_queue_prios, -1);
	ELSE
		PERFORM accept_action(i_ad_id, i_action_id, 'pay', i_remote_addr, 
				i_token, i_gallery_expire_minutes, o_action_type, NULL);
	END IF;
	IF o_action_type = 'new' THEN
		SELECT
			value
		INTO
			l_tduid
		FROM
			action_params
		WHERE
			ad_id = i_ad_id
			AND action_id = i_action_id
			AND name = 'TDUID';
	END IF;
END;
$$ LANGUAGE plpgsql;
