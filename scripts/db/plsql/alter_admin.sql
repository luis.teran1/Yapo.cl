CREATE OR REPLACE FUNCTION alter_admin(i_username admins.username%TYPE,
				       i_fullname admins.fullname%TYPE,
				       i_jabbername admins.jabbername%TYPE,
				       i_email admins.email%TYPE,
				       i_mobile admins.mobile%TYPE,
				       i_privs varchar(50)[],
				       i_passwd admins.passwd%TYPE,
				       i_admin_id admins.admin_id%TYPE,
				       OUT o_admin_id admins.admin_id%TYPE) AS $$
DECLARE
	l_i INTEGER;
	l_priv admin_privs.priv_name%TYPE;
BEGIN
	l_i := 1;

	IF EXISTS (
		SELECT
			*
		FROM
			admins
		WHERE
			username = i_username
			AND admin_id != i_admin_id
		) THEN
		RAISE EXCEPTION 'FACTORY_TRANSLATE:username:ERROR_ADMIN_USER_EXISTS';
	END IF;

	IF i_admin_id IS NULL
	THEN
		INSERT INTO 
			admins 
			(passwd, 
			 fullname, 
			 jabbername, 
			 email, 
			 mobile, 
			 username)
		VALUES
			(i_passwd, 
			 i_fullname, 
			 i_jabbername,
			 i_email,
			 i_mobile,
			 i_username);

		o_admin_id := CURRVAL('admins_admin_id_seq');
	ELSE
		o_admin_id := i_admin_id;
		UPDATE 
			admins 
		SET
			passwd = COALESCE(i_passwd, passwd), 
			fullname = COALESCE(i_fullname, fullname), 
			jabbername = COALESCE(i_jabbername, jabbername),
			email = COALESCE(i_email, email),
			mobile = COALESCE(i_mobile, mobile),
			username = COALESCE(i_username, username)
		WHERE
			admin_id = i_admin_id;

		IF NOT FOUND THEN
			RAISE EXCEPTION 'FACTORY_TRANSLATE:admin_id:ERROR_ADMIN_NOT_FOUND';
		END IF;

		IF i_privs IS NOT NULL THEN
			DELETE
			FROM
				admin_privs
			WHERE
				admin_id = i_admin_id;
		END IF;
	END IF;

	IF i_privs IS NOT NULL THEN
		LOOP
			l_priv := i_privs[l_i];
			EXIT WHEN l_priv IS NULL;
			INSERT INTO
				admin_privs
				(admin_id,
				 priv_name)
			VALUES
				(o_admin_id,
				 l_priv);

			l_i := l_i + 1;
		END LOOP; 
	END IF;
END;
$$ LANGUAGE plpgsql;
