/*
* Update status and list_time of autobump table for an list of autobump_id
* i_autobump_id, i_list_time and i_status should have the same number of elements
*/
CREATE OR REPLACE FUNCTION update_autobump(
	i_autobump_id integer[],
	i_list_time varchar[],
	i_status varchar[]
) RETURNS SETOF autobump AS $$
DECLARE
	l_autobump_id integer;
	l_ad_id integer;
	i integer;
	result_record autobump;
BEGIN
	i := 1;
	IF i_autobump_id IS NOT NULL AND i_list_time IS NOT NULL AND i_status IS NOT NULL THEN
		IF array_length(i_autobump_id, 1) <> array_length(i_list_time, 1) OR
			array_length(i_autobump_id, 1) <> array_length(i_status, 1) OR
			array_length(i_list_time, 1) <> array_length(i_status, 1) THEN
			RAISE EXCEPTION 'FACTORY_TRANSLATE:error:ERROR_DIFERENT_ARRAY_SIZE';
		END IF;
		-- Loop all autobump_id for UPDATE
		FOREACH l_autobump_id IN ARRAY i_autobump_id LOOP
			UPDATE
				autobump
			SET
				status=i_status[i]::enum_autombump_status,
				executed_at=i_list_time[i]::timestamp
			WHERE
				autobump_id=l_autobump_id
			RETURNING
				ad_id INTO l_ad_id;
			-- Create record result autobump
			result_record.autobump_id := l_autobump_id;
			result_record.ad_id := l_ad_id;
			result_record.status := i_status[i];
			RETURN NEXT result_record;
			i := i + 1;
		END LOOP;
	END IF;
END;
$$ LANGUAGE plpgsql;
