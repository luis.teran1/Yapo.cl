CREATE OR REPLACE FUNCTION clean_token(i_token_id tokens.token_id%TYPE, 
				       OUT o_status integer) AS $$
BEGIN
	RAISE NOTICE 'ARCHIVE: Trying to clear token %', i_token_id;
	o_status := 0;

	IF i_token_id IS NULL THEN
		RETURN;
	END IF;

	PERFORM
		*
	FROM
		action_states
	WHERE
		token_id = i_token_id;
	IF FOUND THEN
		o_status := 1;
		RETURN;
	END IF;

	PERFORM
		*
	FROM
		notices
	WHERE
		token_id = i_token_id;
	IF FOUND THEN
		o_status := 1;
		RETURN;
	END IF;

	PERFORM
		*
	FROM
		pay_log
	WHERE
		token_id = i_token_id;
	IF FOUND THEN
		o_status := 1;
		RETURN;
	END IF;

	PERFORM
		*
	FROM
		voucher_states
	WHERE
		token_id = i_token_id;
	IF FOUND THEN
		o_status := 1;
		RETURN;
	END IF;


	PERFORM
		*
	FROM
		store_login_tokens
	WHERE
		token_id = i_token_id;
	IF FOUND THEN
		o_status := 1;
		RETURN;
	END IF;


	PERFORM
		*
	FROM
		store_action_states
	WHERE
		token_id = i_token_id;
	IF FOUND THEN
		o_status := 1;
		RETURN;
	END IF;

	RAISE NOTICE 'ARCHIVE: Deleting token %', i_token_id;
	DELETE FROM tokens WHERE token_id = i_token_id;

END;
$$ LANGUAGE plpgsql;
