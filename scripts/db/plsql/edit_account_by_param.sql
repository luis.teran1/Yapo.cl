CREATE OR REPLACE FUNCTION edit_account_by_param(
		i_email accounts.email%TYPE,
		i_token_id integer,
		i_account_params varchar[][],
		i_social_account_params varchar[][],
		OUT o_account_id accounts.account_id%TYPE) AS $$
DECLARE
	l_account_id accounts.account_id%TYPE;
	l_i integer;
	l_length_array_social_account integer;
	l_length_array_account integer;
	l_social_accounts_names varchar;
	l_social_accounts_values varchar;
	l_accounts_names varchar;
	l_accounts_values varchar;
BEGIN

	l_length_array_social_account = array_length(i_social_account_params, 1);
	l_length_array_account = array_length(i_account_params, 1);

	SELECT
		account_id
	INTO
		l_account_id
	FROM
		accounts
	WHERE
		email = i_email;

	IF NOT FOUND THEN
		RAISE EXCEPTION 'FACTORY_TRANSLATE:error:ACCOUNT_DO_NOT_EXISTS';
	END IF;

	IF i_social_account_params IS NOT NULL THEN
		l_i := 1;
		l_social_accounts_names := '';
		l_social_accounts_values := '';
		LOOP
			EXIT WHEN l_length_array_social_account < l_i;

			IF i_social_account_params[l_i][2] IS NOT NULL THEN
				IF l_social_accounts_names = '' THEN
					l_social_accounts_names := i_social_account_params[l_i][1];
				ELSE
					l_social_accounts_names := l_social_accounts_names || ',' || i_social_account_params[l_i][1];
				END IF;
				IF l_social_accounts_values = '' THEN
					l_social_accounts_values := '''' || i_social_account_params[l_i][2] || '''';
				ELSE
					l_social_accounts_values := l_social_accounts_values || ',''' || i_social_account_params[l_i][2] || '''';
				END IF;
			END IF;

			l_i := l_i + 1;
		END LOOP;
	END IF;

	IF l_social_accounts_names IS NOT NULL AND l_social_accounts_values IS NOT NULL THEN
		EXECUTE FORMAT('UPDATE social_accounts SET ('
		|| l_social_accounts_names
		|| ') = ('
		|| l_social_accounts_values
		|| ') WHERE account_id = '
		|| l_account_id);
	END IF;

	IF i_account_params IS NOT NULL THEN
		l_i := 1;
		l_accounts_names := '';
		l_accounts_values := '';
		LOOP
			EXIT WHEN l_length_array_account < l_i;

			IF i_account_params[l_i][2] IS NOT NULL THEN
				IF l_accounts_names = '' THEN
					l_accounts_names := i_account_params[l_i][1];
				ELSE
					l_accounts_names := l_accounts_names || ',' || i_account_params[l_i][1];
				END IF;
				IF l_accounts_values = '' THEN
					l_accounts_values := '''' || i_account_params[l_i][2] || '''';
				ELSE
					l_accounts_values := l_accounts_values || ',''' || i_account_params[l_i][2] || '''';
				END IF;
			END IF;

			l_i := l_i + 1;
		END LOOP;
	END IF;
	IF l_accounts_names IS NOT NULL AND l_accounts_values IS NOT NULL THEN
		EXECUTE FORMAT('UPDATE accounts SET ('
		|| l_accounts_names
		|| ') = ('
		|| l_accounts_values
		|| ') WHERE account_id = '
		|| l_account_id);
	END IF;

	o_account_id := l_account_id;

	PERFORM insert_account_action(o_account_id,'edition',i_token_id);
END;
$$ LANGUAGE plpgsql;
