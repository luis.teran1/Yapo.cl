CREATE OR REPLACE FUNCTION change_ad_status(
	i_list_id     	ads.list_id%TYPE,
	i_ad_id     	ads.ad_id%TYPE,
	i_new_status  	enum_ads_status,
	i_remote_addr 	varchar,
	i_remote_browser	varchar,
	i_source		varchar,
	i_token_id 		tokens.token_id%TYPE,
	i_salted_passwd ads.salted_passwd%TYPE,
	i_pack_conf 	varchar[][],
	OUT o_email		varchar,
	OUT o_ad_id 	ads.ad_id%TYPE,
	OUT o_list_id 	ads.list_id%TYPE,
	OUT o_action_id ad_actions.action_id%TYPE,
	OUT o_status 	ads.status%TYPE,
	OUT o_subject	ads.subject%TYPE,
	OUT o_region	ads.region%TYPE,
	OUT o_category  ads.category%TYPE,
	OUT o_communes  ad_params.value%TYPE,
	OUT o_ad_salted_passwd	varchar
) AS $$
DECLARE
	l_pack_id		packs.pack_id%TYPE;
	l_ad_status		ads.status%TYPE;
	l_ad_category	ads.category%TYPE;
	l_ad_user_id	ads.user_id%TYPE;
	l_user_id		accounts.user_id%TYPE;
	l_account_id 	accounts.account_id%TYPE;
	l_pack_type 	varchar;
	l_pack_active 	boolean;
	l_pack_categories integer[][];
	l_acc_salted_passwd varchar;
	l_state_id		action_states.state_id%TYPE;
	l_first_pack_type varchar;
	l_ad_pack_status varchar;
BEGIN
	l_pack_active := false;
	-- Search for ad
	SELECT
		ads.ad_id, list_id, status, category, user_id, salted_passwd, subject, region, category, communes.value, pack_info.value
	INTO
		o_ad_id, o_list_id, l_ad_status, l_ad_category, l_ad_user_id, o_ad_salted_passwd, o_subject, o_region, o_category, o_communes, l_ad_pack_status
	FROM
		ads
		LEFT JOIN ad_params AS communes	ON (ads.ad_id = communes.ad_id AND communes.name = 'communes')
		LEFT JOIN ad_params as pack_info ON (ads.ad_id = pack_info.ad_id AND pack_info.name = 'pack_status')
	WHERE
		list_id = i_list_id OR ads.ad_id = i_ad_id;

	IF NOT FOUND THEN
		RAISE EXCEPTION 'FACTORY_TRANSLATE:error:ERROR_AD_NOT_FOUND';
	END IF;

	IF (i_new_status = 'active' AND l_ad_status NOT IN ('deactivated','disabled'))
		OR
		(i_new_status = 'deactivated' AND l_ad_status NOT IN( 'active','disabled')) THEN
		RAISE EXCEPTION 'FACTORY_TRANSLATE:error:ERROR_AD_STATUS_TRANSITION_INVALID';
	END IF;

	-- Search for account
	SELECT
		user_id, account_id, email, salted_passwd
	INTO
		l_user_id, l_account_id, o_email, l_acc_salted_passwd
	FROM
		accounts
	WHERE
		user_id = l_ad_user_id;

	-- if the call is not with token or batch we need validate the passwd versus the ad passwd or account passwd
	IF i_salted_passwd IS NOT NULL THEN
		IF i_salted_passwd != o_ad_salted_passwd THEN
			IF l_acc_salted_passwd IS NULL OR i_salted_passwd != l_acc_salted_passwd THEN
				RAISE EXCEPTION 'FACTORY_TRANSLATE:salted_passwd:ERROR_PASSWORD_MISMATCH';
			END IF;
		END IF;
	END IF;

	l_pack_type := find_pack_type(l_ad_category, i_pack_conf);
	-- is an ad from a pack category also that category is car
	IF l_pack_type IS NOT NULL AND l_account_id IS NOT NULL THEN
		l_pack_categories := find_pack_categories(l_pack_type, i_pack_conf);
		SELECT
			(status = 'active')
		INTO
			l_pack_active
		FROM
			packs
		WHERE
			account_id = l_account_id AND status = 'active';

		SELECT
			value
		INTO
			l_first_pack_type
		FROM
			account_params
		WHERE
			account_id = l_account_id AND name = 'pack_type';

        IF i_new_status = 'deactivated'
			AND l_ad_status = 'active'
			AND l_first_pack_type = l_pack_type
			AND l_ad_pack_status IS NOT NULL
		THEN

			SELECT pack_id INTO l_pack_id
			FROM packs
			WHERE
				account_id = l_account_id
				AND status = 'active'
				AND used > 0
			ORDER BY
				date_start DESC
			LIMIT 1 FOR UPDATE;

			IF FOUND THEN
				UPDATE
					packs
				SET
					used = used - 1
				WHERE
					pack_id = l_pack_id;
			ELSE
				RAISE NOTICE 'NO PACK FOUND FOR account %', l_account_id;
			END IF;

			PERFORM ad_status_change(o_ad_id, 'disabled'::enum_ads_status, i_remote_addr, null);
		END IF;
	END IF;

	IF i_new_status = 'active' THEN
		IF l_pack_type IS NOT NULL
			AND l_first_pack_type IS NOT NULL
			AND l_pack_type = l_first_pack_type
			AND l_ad_pack_status IS NOT NULL
			OR l_ad_pack_status = 'disabled'
		THEN
		-- if pro pasar a disabled
			i_new_status := 'disabled';
			SELECT
				iaas.o_action_id
			INTO
				o_action_id
			FROM
				insert_ad_action_state(
					o_ad_id,
					'disable'::enum_ad_actions_action_type,
					'disabled'::enum_action_states_state,
					case when i_token_id is null then 'user_disabled'::enum_action_states_transition else 'admin_disabled'::enum_action_states_transition end,
					i_remote_addr,
					i_token_id
				) AS iaas;
		-- else pasar a active
		ELSE
			SELECT
                iaas.o_action_id
            INTO
                o_action_id
            FROM
                insert_ad_action_state(
                    o_ad_id,
                    'activate'::enum_ad_actions_action_type,
                    'activated'::enum_action_states_state,
                    case when i_token_id is null then 'user_activated'::enum_action_states_transition else 'admin_activated'::enum_action_states_transition end,
                    i_remote_addr,
                    i_token_id
                ) AS iaas;
		END IF;
	ELSE -- here new status is disabled

		-- Remove pending actions
		PERFORM remove_unfinished_actions(o_ad_id, i_remote_addr, i_token_id);

		o_action_id := insert_ad_action(o_ad_id, 'deactivate'::enum_ad_actions_action_type, NULL);

		-- Create state
		l_state_id := insert_state(o_ad_id,
			o_action_id,
			'deactivated'::enum_action_states_state,
			case when i_token_id is null then 'user_deactivated'::enum_action_states_transition else 'admin_deactivated'::enum_action_states_transition end,
			i_remote_addr,
			i_token_id);


		-- Remove pending products
		PERFORM remove_products(o_ad_id,o_action_id, l_state_id, i_remote_addr, i_token_id);
	END IF;

	IF i_source IS NOT NULL THEN
		INSERT INTO action_params (ad_id, action_id, name, value) VALUES (o_ad_id, o_action_id,	'source', i_source);
	END IF;
	-- change ad status to new status
	UPDATE
		ads
	SET
		status = i_new_status,
		modified_at = CURRENT_TIMESTAMP
	WHERE
		ad_id = o_ad_id;

	o_status := i_new_status;

	if i_new_status = 'deactivated' then
		-- Delete from watch_ads
		DELETE FROM watch_ads WHERE list_id = o_list_id;
	end if;

	if o_email is null then
		select email into o_email from users where user_id = l_ad_user_id;
	end if;
END;
$$ LANGUAGE plpgsql;
