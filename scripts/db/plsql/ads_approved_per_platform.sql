CREATE OR REPLACE FUNCTION ads_approved_per_platform(	i_date_old TIMESTAMP,
							i_date_new TIMESTAMP,
							OUT o_date TIMESTAMP,
							OUT o_value action_params.value%TYPE,
							OUT o_approved INTEGER )RETURNS SETOF record AS $$
DECLARE
	l_rec record;
BEGIN
	FOR
		l_rec
	IN
        
	EXECUTE '
			SELECT
				DATE_TRUNC(''DAY'',approved.timestamp)::TIMESTAMP AS o_date,
				action_params.value AS o_value,
				COUNT(DISTINCT ad_actions.ad_id) AS o_approved
			FROM
				ad_actions
			JOIN
				action_states  AS verified ON
				ad_actions.ad_id = verified.ad_id AND
				ad_actions.action_id = verified.action_id AND
				action_type IN (''new'', ''editrefused'') AND
				verified.state = ''pending_review'' AND
				transition IN (''verify'',''pay'') AND
				timestamp::TIMESTAMP BETWEEN
				''' || i_date_old ||'''::TIMESTAMP AND
				''' ||i_date_new ||''' ::TIMESTAMP
			JOIN
				action_states AS approved ON
				verified.ad_id=approved.ad_id AND
				verified.action_id=approved.action_id AND
				action_type IN (''new'', ''editrefused'') AND
				approved.state = ''accepted'' AND
				approved.transition IN (''accept'', ''accept_w_chngs'') AND
				approved.timestamp::TIMESTAMP >= ''' || i_date_old ||'''
			JOIN
				action_params ON
				(action_params.value IN (''web'',''msite'',''android'',''nga_api_android'',''ios'', ''nga_api_ios'') AND
				action_params.name=''source'' AND
				approved.ad_id = action_params.ad_id AND
				approved.action_id = action_params.action_id)
			GROUP BY
				1,2
			'

	LOOP
		o_date := l_rec.o_date;
		o_value:= l_rec.o_value;
		o_approved := l_rec.o_approved;

		RETURN NEXT;
	END LOOP;
END
$$ LANGUAGE plpgsql;
