CREATE OR REPLACE FUNCTION get_pack_result(
	i_user_id ads.user_id%TYPE,
	i_ad_id ad_actions.ad_id%TYPE,
	i_action_id ad_actions.action_id%TYPE,
	i_action_type ad_actions.action_type%TYPE,
	i_pack_conf varchar[][],
	OUT o_pack_result	integer
	) AS $$
DECLARE
	l_account_id integer;
	l_category ads.category%TYPE;
	l_is_pro_for varchar;
	l_pack_type  varchar;
	l_first_pack_type  varchar;
	l_pack_id integer;
	l_changed_cat integer;
	l_pack_categories integer[];
	l_old_category varchar;
	l_old_pack_type varchar;
	l_old_pack_categories integer[];
	l_all_pack_categories integer[];
	l_ad_status ads.status%TYPE;
BEGIN

	-- Review imported ads
	-- if the ad was imported by partners ignore pack logic
	IF EXISTS (
		SELECT * FROM ad_params WHERE ad_id = i_ad_id AND name = 'link_type'
	) THEN
		RAISE NOTICE 'SET_PACK_RESULT: action does not apply for imported ads';
		o_pack_result := -1;
		RETURN;
	END IF;

	l_category := get_changed_value(i_ad_id, i_action_id, false, 'category')::integer;
	-- review the pack info
	-- check if the ad belongs to a category pro
	l_pack_type := find_pack_type(l_category, i_pack_conf);

	-- Check if the action is an edition changing the category
	SELECT old_value INTO l_old_category FROM ad_changes WHERE ad_id = i_ad_id AND action_id = i_action_id AND is_param = false AND column_name = 'category';
	IF l_old_category IS NOT NULL THEN
		l_changed_cat = 1;
		l_old_pack_type := find_pack_type(l_old_category::integer, i_pack_conf);
		RAISE NOTICE 'GET_PACK_RESULT: Change of category found from % to %', l_old_category, l_category;
	ELSE
		l_changed_cat = 0;
		RAISE NOTICE 'GET_PACK_RESULT: Changes of category not found';
	END IF;	

	RAISE DEBUG 'GET_PACK_RESULT: pack_type:% category:% old_category% old_pack_type%',l_pack_type, l_category, l_old_category, l_old_pack_type;

	-- if the ad doesn't belong to a category pro
	IF l_pack_type is NULL AND l_old_pack_type is NULL THEN
		o_pack_result := -1;
		RAISE NOTICE 'GET_PACK_RESULT: pack_type and old_pack_type are null';
	ELSE

		l_pack_categories := find_pack_categories(l_pack_type, i_pack_conf);
		IF l_old_pack_type IS NOT NULL THEN
			l_old_pack_categories := find_pack_categories(l_old_pack_type, i_pack_conf);
		END IF;

		-- check if the user is pro
		SELECT account_id INTO l_account_id	FROM accounts WHERE user_id = i_user_id AND status = 'active';
		-- if the user has account
		IF FOUND THEN
			---the user has account
			-- if the user have the param is_pro_for
			SELECT value INTO l_is_pro_for FROM account_params WHERE account_id = l_account_id AND name = 'is_pro_for';
			-- if the user have the param pack_type
			SELECT value INTO l_first_pack_type FROM account_params WHERE account_id = l_account_id AND name = 'pack_type';
		ELSE
			-- get the user ads with ad param "account to pro" for the current ad category
			IF NOT EXISTS(SELECT a.ad_id FROM ads a join ad_actions ac ON (a.ad_id = ac.ad_id  AND ac.action_type = 'account_to_pro') WHERE a.user_id = i_user_id and a.category = ANY( l_pack_categories )) THEN
				-- the user is not pro
				o_pack_result := -1;
			ELSE
				-- the user is pro but does not have a pack
				o_pack_result := 1;
			END IF;
		END IF;

		-- if have account
		IF l_account_id IS NOT NULL THEN
			RAISE DEBUG 'GET_PACK_RESULT: account_found';
			-- the user is not pro
			-- or the ad category is not configured for packs
			-- or none of pro_categories from user is configured as pack category
			l_all_pack_categories := find_pack_categories('all', i_pack_conf);
			
			IF l_is_pro_for IS NULL
				OR NOT EXISTS( select * from unnest(l_all_pack_categories) t where t = l_category OR t = l_old_category::integer )
				OR NOT 
				(
					(string_to_array(l_is_pro_for,',')::integer[] && l_pack_categories) 
					OR (l_old_pack_categories IS NOT NULL AND string_to_array(l_is_pro_for,',')::integer[] && l_old_pack_categories)
				) 
			THEN
				RAISE NOTICE 'GET_PACK_RESULT: ad does not belongs to pack';
				o_pack_result := -1;
			ELSE
				-- If the user is try to change the category in or out a pro categories 
				-- this action should be automatically refused
				RAISE NOTICE 'GET_PACK_RESULT: Check category (%) PACK TYPES: % to %', l_changed_cat, l_old_pack_type, l_pack_type;
				IF l_changed_cat = 1 AND ( (l_pack_type IS NOT NULL OR l_old_pack_type IS NOT NULL) AND ( l_pack_type IS DISTINCT FROM l_old_pack_type ) ) THEN
					RAISE NOTICE 'GET_PACK_RESULT: user pro is trying to change category: % to %',l_old_category, l_category;
					o_pack_result:= -4;
				ELSIF NOT EXISTS (SELECT pack_id FROM packs WHERE account_id = l_account_id) THEN
					RAISE NOTICE 'GET_PACK_RESULT: USER WITHOUT PACK';
					-- check if the user hasn't pack
					IF i_action_type IN ('new', 'editrefused', 'undo_delete') THEN
						--disable the ad and return a variable to show the message to buy a pack
						o_pack_result := 3;
						--DUAL PRO VERIFY
						IF is_dual_pro_inserting_in_second_category(l_category, l_first_pack_type, i_pack_conf) THEN
							RAISE NOTICE 'GET_PACK_RESULT: verified dual pro (user without pack): % - % - %',l_category, l_first_pack_type, i_pack_conf;
							o_pack_result := -3;
						END IF;
						IF is_inserting_fee_ad(i_ad_id, l_category) AND i_action_type in('edit', 'editrefused') THEN
							RAISE NOTICE 'SET_PACK_RESULT: action over an inserting fee ad';
							o_pack_result := 5;
						END IF;
					ELSE
						IF is_inserting_fee_ad(i_ad_id, l_category) AND i_action_type = 'edit' THEN
							RAISE NOTICE 'SET_PACK_RESULT: action over an inserting fee ad';
							o_pack_result := 5;
						ELSE
							RAISE NOTICE 'SET_PACK_RESULT: action does not apply for pack';
							o_pack_result := -1;
						END IF;
					END IF;
				ELSE
					RAISE NOTICE 'GET_PACK_RESULT: user with pack';
					--If the user has pack
					SELECT status INTO l_ad_status FROM ads where ad_id = i_ad_id;
					IF i_action_type IN ('new', 'undo_delete') OR ( i_action_type = 'editrefused' AND l_ad_status = 'refused' ) THEN
						-- check if the user has packs availables
						SELECT pack_id
						INTO l_pack_id
						FROM packs
						WHERE account_id = l_account_id AND status = 'active' AND type = l_pack_type::enum_pack_type AND slots - used > 0
						ORDER BY date_start ASC
						LIMIT 1;

						IF NOT FOUND THEN
							--disable the ad and return a variable to show the message to buy a pack or disable other ad
							o_pack_result := 2;
							RAISE NOTICE 'GET_PACK_RESULT: user has not slots (SUCCESS)';
							--DUAL PRO VERIFY
							IF is_dual_pro_inserting_in_second_category(l_category, l_first_pack_type, i_pack_conf) THEN
								RAISE NOTICE 'GET_PACK_RESULT: VERIFIED DUAL PRO: % - % - %',l_category, l_first_pack_type, i_pack_conf;
								o_pack_result := -3;
							END IF;
						ELSE
							-- (the user have slots!!! yujuuu)
							RAISE NOTICE 'GET_PACK_RESULT: user has slots (SUCCESS)';
							o_pack_result := 0;
						END IF;
					ELSE
						IF is_inserting_fee_ad(i_ad_id, l_category) AND i_action_type in('edit', 'editrefused') THEN
							RAISE NOTICE 'SET_PACK_RESULT: action over an inserting fee ad';
							o_pack_result := 5;
						ELSE
							-- accept the action but keep the actual status of the ad
							o_pack_result := -2;
							RAISE NOTICE 'SET_PACK_RESULT: action type doesnt apply';
						END IF;
					END IF;
				END IF;
			END IF;
		END IF;
	END IF;
END;
$$ LANGUAGE plpgsql;

