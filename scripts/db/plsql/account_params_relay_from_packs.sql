CREATE OR REPLACE FUNCTION account_params_relay_from_packs(
) RETURNS TRIGGER AS $$
BEGIN
    IF TG_OP = 'UPDATE' OR TG_OP = 'INSERT' THEN
        PERFORM 1 FROM account_params
        WHERE account_id = NEW.account_id AND name = 'available_slots' FOR UPDATE;

        IF FOUND THEN
            -- Update the account param available slots
            UPDATE account_params
            SET value = (
                SELECT COALESCE(SUM(slots) - SUM(used), 0)
                FROM packs
                WHERE account_id = NEW.account_id AND status = 'active'
            )::varchar
            WHERE account_id = NEW.account_id AND name = 'available_slots';

        ELSE
            INSERT INTO account_params(account_id, name, value)
            VALUES (
                NEW.account_id,
                'available_slots',
                (
                    SELECT COALESCE(SUM(slots) - SUM(used), 0)
                    FROM packs
                    WHERE account_id = NEW.account_id AND status = 'active'
                )::varchar
            );
        END IF;
    END IF;

    RETURN NULL;
END;
$$ LANGUAGE plpgsql;
