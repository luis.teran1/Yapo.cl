CREATE OR REPLACE FUNCTION insert_store_state(i_store_id stores.store_id%TYPE, 
					      i_action_id store_actions.action_id%TYPE,
					      i_state store_action_states.state%TYPE,
					      i_token_id store_action_states.token_id%TYPE) RETURNS store_action_states.state_id%TYPE AS $$
DECLARE
	l_current_state store_action_states.state%TYPE;
	l_current_state_id store_action_states.state_id%TYPE;
BEGIN
	SELECT
		state,
		current_state
	INTO
		l_current_state,
		l_current_state_id
	FROM
		store_actions
	WHERE
		store_actions.store_id = i_store_id AND
		store_actions.action_id = i_action_id;
	
	IF NOT FOUND AND i_state NOT IN ('reg') THEN
		RAISE EXCEPTION 'ERROR_NO_SUCH_ACTION';
	END IF;
	
	IF i_state IN ('accepted') THEN
		IF NOT l_current_state IN ('reg', 'paid') THEN
			RAISE EXCEPTION 'ERROR_INVALID_STATE';
		END IF;
	END IF;
	
	INSERT INTO
		store_action_states
		(store_id,
		 action_id,
		 state,
		 timestamp,
		 token_id)
	VALUES
		(i_store_id,
		 i_action_id,
		 i_state,
		 now(),
		 i_token_id);

	-- Update the action to point to the newly added state
	UPDATE
		store_actions
	SET
		current_state = CURRVAL('store_action_states_state_id_seq'),
		state = i_state
	WHERE
		store_id = i_store_id AND
		action_id = i_action_id;
	
	IF NOT FOUND THEN
		RAISE EXCEPTION 'ERROR_NO_SUCH_ACTION';
	END IF;
	
	RETURN CURRVAL('store_action_states_state_id_seq');
END;
$$ LANGUAGE plpgsql;
