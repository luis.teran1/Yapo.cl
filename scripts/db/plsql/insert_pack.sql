CREATE OR REPLACE FUNCTION insert_pack(
	i_account_id accounts.account_id%TYPE,
	i_pack_type varchar,
	i_slots packs.slots%TYPE,
	i_product_id packs.product_id%TYPE,
	i_expire_time varchar,
	i_expiration_date packs.date_end%TYPE,
	i_payment_group_id payment_groups.payment_group_id%TYPE,
	i_token tokens.token%TYPE,
	i_service_order varchar,
	i_service_amount varchar,
	i_pack_conf varchar[][],
	OUT o_pack_id packs.pack_id%TYPE,
	OUT o_available_slots integer,
	OUT o_email accounts.email%TYPE
) AS $$
DECLARE
	l_user_id accounts.user_id%TYPE;
	l_rec record;
	l_type_categories integer[];
	l_param varchar[];
BEGIN

	-- Search for user information
	SELECT user_id, email INTO l_user_id, o_email FROM accounts WHERE account_id = i_account_id;

	-- If ccount not found the transaction can't continue
	IF NOT FOUND THEN
		RAISE EXCEPTION 'FACTORY_TRANSLATE:error:ERROR_ACCOUNT_NOT_FOUND';
	END IF;

	-- Calculate the available slots on active packs
	SELECT
		SUM(slots) - SUM(used)
	INTO
		o_available_slots
	FROM
		packs
	WHERE
		account_id = i_account_id
		AND status = 'active'
		AND type = i_pack_type::enum_pack_type;

	-- Now we have 2 ways to insert packs
	-- The personalized packs use directly the expiration date
	IF i_expire_time IS NULL THEN
		-- personalized
		INSERT INTO packs
			(account_id, type, slots, date_end, product_id, payment_group_id, token_id)
		VALUES
			(i_account_id, i_pack_type::enum_pack_type, i_slots, i_expiration_date, i_product_id, i_payment_group_id, get_token_id(i_token))
		RETURNING
			pack_id
		INTO
			o_pack_id;
	ELSE
		-- Normal packs
		INSERT INTO packs
			(account_id, type, slots, date_end, product_id, payment_group_id, token_id)
		VALUES
			(i_account_id, i_pack_type::enum_pack_type, i_slots, now() + (i_expire_time)::interval, i_product_id, i_payment_group_id, get_token_id(i_token))
		RETURNING
			pack_id
		INTO
			o_pack_id;
	END IF;

	-- Only if the user does not have slots previously, use the new slots to activate the disabled ads using this new slots as limit
	IF o_available_slots IS NULL OR o_available_slots = 0 THEN
		-- Get the categories and search for disabled ads
		l_type_categories := find_pack_categories(i_pack_type, i_pack_conf);
		FOR l_rec IN (
				SELECT ad_id FROM ads WHERE user_id = l_user_id AND status = 'disabled' AND category = ANY( l_type_categories )
				ORDER BY list_time DESC LIMIT i_slots
		)
		LOOP
			SELECT
				p.o_available_slots
			INTO
				o_available_slots
			FROM
				pack_change_ad_status_unpublished(
					i_account_id,
					l_rec.ad_id,
					'active',
					'0.0.0.0',
					i_pack_conf::varchar[][]
				) AS p;
		END LOOP;
		-- If ads were not found to activate asign as output the available slots the new slots of this pack
		IF l_rec IS NULL THEN
			o_available_slots := i_slots;
		END IF;
	END IF;

	-- Save service order and amount as params
	IF i_service_order IS NOT NULL THEN
		INSERT INTO pack_params	(pack_id, name, value) VALUES (o_pack_id, 'service_order'::enum_pack_param_name, i_service_order);
	END IF;

	IF i_service_amount IS NOT NULL THEN
		INSERT INTO pack_params	(pack_id, name, value) VALUES (o_pack_id, 'service_amount'::enum_pack_param_name, i_service_amount);
	END IF;

	-- Get the new available slots from the user packs.
	SELECT SUM(slots) - SUM(used) INTO o_available_slots FROM packs WHERE account_id = i_account_id AND status = 'active';


END;
$$ LANGUAGE plpgsql;

