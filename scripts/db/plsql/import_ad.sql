CREATE OR REPLACE FUNCTION import_ad(i_email users.email%TYPE,
				     i_remote_addr action_states.remote_addr%TYPE,
				     i_ads_name ads.name%TYPE,
				     i_ads_phone ads.phone%TYPE,
				     i_ads_region ads.region%TYPE,
				     i_ads_city ads.city%TYPE,
				     i_ads_type ads.type%TYPE,
				     i_ads_category ads.category%TYPE,
				     i_ads_passwd ads.passwd%TYPE,
				     i_store_id ads.store_id%TYPE,
				     i_phone_hidden ads.phone_hidden%TYPE,
				     i_company_ad ads.company_ad%TYPE,
				     i_ads_subject ads.subject%TYPE,
				     i_ads_body ads.body%TYPE,
				     i_ads_price ads.price%TYPE,
				     i_ads_infopage ads.infopage%TYPE,
				     i_ads_infopage_title ads.infopage_title%TYPE, 
				     i_ad_images varchar[],
				     i_ad_params varchar[][],
				     i_link_type VARCHAR(50),
				     i_ext_ad_id VARCHAR(50),
				     i_digest VARCHAR(50),
				     i_list_time ads.list_time%TYPE,
				     i_old_price ads.old_price%TYPE,
				     i_link_type_group_check_whitelist varchar[],
				     i_pending_review_queue ad_queues.queue%TYPE,
				     i_static_params text[],
				     i_lang varchar,
				     i_category_edition_limits integer[][],
				     OUT o_ad_id ads.ad_id%TYPE,
				     OUT o_action_id ad_actions.action_id%TYPE,
				     OUT o_list_id ads.list_id%TYPE,
				     OUT o_is_new_ad bool,
				     OUT o_bump_ad bool,
				     OUT o_list_time ads.list_time%TYPE) AS $$
DECLARE
	l_ad_id ads.ad_id%TYPE;
	l_old_list_time ads.list_time%TYPE;
	l_status ads.status%TYPE;
	l_new_status ads.status%TYPE;
	l_deleted_at action_states.timestamp%TYPE;
	l_old_price ads.old_price%TYPE;
	l_orig_list_time ads.orig_list_time%TYPE;
BEGIN
	o_bump_ad := FALSE;
	SELECT
		ext_id.ad_id,
		list_time,
		status,
		orig_list_time
	INTO
		l_ad_id,
		l_old_list_time,
		l_status,
		l_orig_list_time
	FROM
		ads
		JOIN ad_params AS ext_id ON
			ads.ad_id = ext_id.ad_id AND
			ext_id.name = 'external_ad_id'
		JOIN ad_params AS link_type ON
			ext_id.ad_id = link_type.ad_id AND
			link_type.name = 'link_type'
	WHERE
		ext_id.value = i_ext_ad_id AND
		link_type.value = i_link_type
	ORDER BY CASE status WHEN 'active' THEN 1 ELSE 2 END, ad_id -- In case of duplicates (ads with same external_ad_id)
	LIMIT 1;

	IF FOUND THEN
		o_is_new_ad := FALSE;
	ELSE
		o_is_new_ad := TRUE;
	END IF;
	
	IF i_list_time IS NULL THEN
		o_list_time := COALESCE(l_old_list_time, CURRENT_TIMESTAMP);
	ELSE
		o_list_time := i_list_time;
	END IF;

	SELECT
		insert_ad.o_ad_id,
		insert_ad.o_action_id
	INTO
		o_ad_id,
		o_action_id
	FROM
		insert_ad(i_email,
			  NULL, -- pay_type
			  NULL, -- cat_price
			  NULL, -- extra_images_price
			  NULL, -- gallery_price
			  NULL, -- redir_code
			  NULL, -- previous_action_id
			  i_remote_addr,
			  i_ads_name,
			  i_ads_phone,
			  i_ads_region,
			  i_ads_city,
			  i_ads_type,
			  i_ads_category,
			  i_ads_passwd,
			  NULL,
			  i_store_id,
			  i_phone_hidden,
			  i_company_ad,
			  i_ads_subject,
			  i_ads_body,
			  i_ads_price,
			  i_ads_infopage,
			  i_ads_infopage_title,
			  i_ad_images,
			  NULL,
			  NULL,
			  i_ad_params || ARRAY[ARRAY['link_type',i_link_type],ARRAY['digest',i_digest]],
			  NULL,
			  'import',
			  NULL,
			  NULL,
			  l_ad_id,
			  i_static_params,
			  i_lang,
			  i_category_edition_limits);
	
	IF i_pending_review_queue IS NOT NULL THEN
		PERFORM insert_state(o_ad_id, 
				     o_action_id, 
				     'pending_review', 
				     'import', 
				     i_remote_addr, 
				     NULL);
		-- Set the queue
		UPDATE
			ad_actions
		SET
			queue = i_pending_review_queue
		WHERE
			ad_id = o_ad_id AND
			action_id = o_action_id;
		-- Register the action in the ad_queues table
		INSERT
		INTO
			ad_queues 
			(ad_id,
			 action_id,
			 queue)
		VALUES
			(o_ad_id,
			 o_action_id, 
			 i_pending_review_queue);
	ELSE
		PERFORM accept_action (
				o_ad_id,
				o_action_id,
				'import',
				i_remote_addr,
				NULL,
				NULL,
				'import',
				NULL);

		SELECT
			list_id
		INTO
			o_list_id
		FROM
			ads
		WHERE
			ad_id = o_ad_id;
	
		IF o_list_id IS NULL THEN
			o_list_id := NEXTVAL('list_id_seq');
		END IF;

		IF NOT o_is_new_ad AND i_store_id IS NOT NULL THEN
			o_list_time := NULL;
			o_bump_ad := TRUE;
		ELSIF l_old_list_time IS NULL OR o_list_time != l_old_list_time OR o_list_time <= CURRENT_TIMESTAMP - INTERVAL '50 day' THEN
			o_list_time := adjust_imported_list_time(i_link_type, i_store_id, o_list_time, i_link_type_group_check_whitelist);
		END IF;

		IF l_status = 'hidden' THEN
			l_new_status := 'hidden';
		ELSIF o_list_time IS NULL THEN
			l_new_status := 'inactive';
		ELSE
			l_new_status := 'active';
		END IF;

		IF l_orig_list_time IS NULL THEN
			l_orig_list_time := o_list_time;
		END IF;

		UPDATE
			ads
		SET
			list_id = o_list_id,
			list_time = o_list_time,
			status = l_new_status,
			modified_at = CURRENT_TIMESTAMP,
			orig_list_time = l_orig_list_time
		WHERE
			ad_id = o_ad_id;

		IF i_old_price IS NOT NULL THEN
			UPDATE
				ads
			SET
				old_price = i_old_price
			WHERE
				ad_id = o_ad_id;
		ELSE
			IF l_old_price IS NOT NULL THEN
				UPDATE 
					ads 
				SET
					old_price = NULL
				WHERE 
					ad_id = o_ad_id;
			END IF;
		END IF;
	END IF;
END;
$$ LANGUAGE plpgsql;
