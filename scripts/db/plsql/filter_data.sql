CREATE OR REPLACE FUNCTION filter_data (i_application block_rules.application%TYPE,
					i_fields varchar[][],
					o_rule_id OUT block_rules.rule_id%TYPE,
					o_rule_name OUT block_rules.rule_name%TYPE,
					o_action OUT block_rules.action%TYPE,
					o_target OUT block_rules.target%TYPE,
					o_combi_match OUT bool,
					o_matched_column OUT block_rules.rule_name%TYPE,
					o_matched_value OUT blocked_items.value%TYPE) RETURNS SETOF RECORD AS $$
DECLARE
	l_rule RECORD;
	l_condition RECORD;
	l_i integer;
	l_matched_condition bool;
	l_condition_match bool;
	l_rule_match bool;
	l_field varchar;
	l_no_conditions integer;	-- Set to number of AND conditions (always 1 in case of OR conditions)
	l_matched_value blocked_items.value%TYPE;
BEGIN
	-- Order by priority so we can return at first match.

	FOR
		l_rule
	IN
		SELECT
			*,
			CASE action
				WHEN 'spamfilter' THEN 1
				WHEN 'delete' THEN 2
				WHEN 'stop' THEN 3
				ELSE 0
			END AS prio
		FROM
			block_rules
		WHERE
			application = i_application
		ORDER BY
			prio DESC
	LOOP
		o_rule_id := NULL;
		o_rule_name := NULL;
		o_action := NULL;
		o_target := NULL;
		o_matched_column := NULL;
		o_matched_value := NULL;
		o_combi_match := false;
		l_no_conditions := 1;
		l_rule_match := true;
		l_condition_match := false;

		IF l_rule.combine_rule = 'and' THEN
			SELECT
				count(*)
			INTO
				l_no_conditions
			FROM
				block_rule_conditions
			WHERE
				rule_id = l_rule.rule_id;
		END IF;

		FOR
			l_condition
		IN
			SELECT
				block_rule_conditions.*,
				block_lists.ignore AS ignore
			FROM
				block_rule_conditions JOIN
				block_lists USING (list_id)
			WHERE
				rule_id = l_rule.rule_id
		LOOP
			o_rule_id := l_rule.rule_id;
			o_rule_name := l_rule.rule_name;
			o_action := l_rule.action;
			o_target := l_rule.target;
			l_matched_condition := false;
			l_i := 1;

			LOOP
				l_field := i_fields[l_i][1];
				EXIT WHEN l_field IS NULL;

				IF in_array(l_field, l_condition.fields) THEN
					IF l_condition.ignore IS NOT NULL THEN
						l_matched_value := check_blocked(l_condition.list_id, 
										 l_condition.whole_word, 
										 regexp_replace(i_fields[l_i][2], '[' || l_condition.ignore || ']', '', 'g'));
					ELSE
						l_matched_value := check_blocked(l_condition.list_id, 
										 l_condition.whole_word, 
										 i_fields[l_i][2]);
					END IF;

					IF (l_matched_value IS NOT NULL AND l_condition.whole_word <> -1) OR (l_matched_value IS NULL AND l_condition.whole_word = -1) THEN
						o_matched_column := l_field;
						l_matched_condition := true;
						l_condition_match := true;

						IF l_no_conditions = 1 THEN
							o_matched_value := l_matched_value;
							RETURN NEXT;
						ELSE
							o_matched_value := COALESCE(o_matched_value || ',' || l_matched_value,
											l_matched_value, '');
						END IF;
					END IF;
				END IF;

				l_i := l_i + 1;
			END LOOP;

			IF NOT l_matched_condition AND l_rule.combine_rule = 'and' THEN
				l_rule_match := false;
			END IF;
		END LOOP;

		IF (l_rule.combine_rule = 'and' AND l_rule_match) OR 
		   (l_rule.combine_rule = 'or' AND l_condition_match) THEN
		
			IF l_no_conditions > 1 THEN
				o_rule_id := l_rule.rule_id;
				o_rule_name := l_rule.rule_name;
				o_action := l_rule.action;
				o_target := l_rule.target;
				o_matched_column := NULL;
				o_combi_match := true;
				RETURN NEXT;
			END IF;
		END IF;
	END LOOP;
	-- No match.
	o_rule_id := NULL;
	o_rule_name := NULL;
	o_action := NULL;
	o_target := NULL;
	o_combi_match := NULL;
END;
$$ LANGUAGE plpgsql;
