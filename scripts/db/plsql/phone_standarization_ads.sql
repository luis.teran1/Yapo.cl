CREATE OR REPLACE FUNCTION phone_standarization_ads(
	i_region integer,
	i_comune_conf integer[][],
	OUT o_commune integer,
	OUT o_updated integer
)
RETURNS SETOF record AS $$
DECLARE
	l_counter integer := 0;
	l_rec record;
	l_new_phone varchar;
	l_update_result integer;
	l_code varchar;
	l_last_commune varchar;
	l_execute_out integer := 0;
BEGIN
	FOR l_rec IN (
		SELECT
			a.ad_id, a.phone, p.value as commune
		FROM
			ads a join ad_params p
		ON (
			a.ad_id = p.ad_id
			AND a.status != 'deleted'
			AND p.name = 'communes'
		)
		WHERE region = i_region
		ORDER BY p.value
	)
	LOOP
		IF l_last_commune != l_rec.commune THEN
			l_code := get_area_code(l_rec.commune::INTEGER, i_comune_conf);
			l_counter := 0;
			l_execute_out := 1;
		END IF;

		IF l_code = '2' THEN
			l_new_phone := phone_standarization_rm(l_code, l_rec.phone);
		ELSE
			l_new_phone := phone_standarization_norm(l_code, l_rec.phone);
		END IF;

		l_update_result := insert_phone_update(l_rec.ad_id, l_new_phone, l_rec.phone);
		l_counter := l_counter + l_update_result;

		RAISE NOTICE 'PHONE UPDATE AD % UP:%', l_rec.ad_id, l_update_result;

		l_last_commune := l_rec.commune;
		o_commune := l_last_commune;
		o_updated := l_counter;
		IF l_execute_out = 1 THEN
			l_execute_out:=0;
			RETURN next;
		END IF;

	END LOOP;
END;
$$ LANGUAGE plpgsql;
