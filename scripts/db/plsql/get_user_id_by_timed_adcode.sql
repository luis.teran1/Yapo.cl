CREATE OR REPLACE FUNCTION get_user_id_by_timed_adcode(i_code payment_groups.code%TYPE, 
						       i_timestamp action_states.timestamp%TYPE,
						       i_expire_days integer,
						       OUT o_user_id ads.user_id%TYPE) AS $$
BEGIN
	SELECT
		user_id
	INTO
		o_user_id
	FROM
		payment_groups
		JOIN ad_actions USING (payment_group_id)
		JOIN action_states USING (ad_id, action_id)
		JOIN ads USING (ad_id)
	WHERE
		ad_actions.current_state = action_states.state_id AND
		action_states.state in ('unpaid', 'unverified') AND
		payment_groups.status in ('unpaid', 'unverified') AND
		timestamp >= CURRENT_TIMESTAMP - (interval '1 day' * i_expire_days) AND
		date_trunc('second', timestamp) <= i_timestamp + interval '1 hour' AND
		code = i_code;

	IF NOT FOUND
	THEN
		RAISE EXCEPTION 'ERROR_USER_NOT_FOUND';
	END IF;
END;
$$ LANGUAGE plpgsql;
