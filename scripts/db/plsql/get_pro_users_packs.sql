CREATE OR REPLACE FUNCTION get_pro_users_packs(
	i_pack_type varchar,
	i_pack_conf varchar[][], 
--USER INFO
	OUT o_uid integer,
	OUT o_email varchar,
	OUT o_phone varchar,
	OUT o_region smallint,
	OUT o_first_ad varchar,
	OUT o_published_ads integer,
--PACK INFO
	OUT o_product_id integer,
	OUT o_price integer,
	OUT o_date_start timestamp,
	OUT o_date_end timestamp,
	OUT o_slots_used integer,
	OUT o_pack_type varchar

) RETURNS SETOF record AS $$
DECLARE
	l_rec record;
	l_last_email varchar;
	l_pack_categories integer[];
BEGIN
	l_pack_categories := find_pack_categories(i_pack_type, i_pack_conf);
	FOR	l_rec IN (
		SELECT
			u.uid,
			a.email,
			a.phone,
			a.region,
			a.user_id,
			up.value as first_approved_ad,
			p.product_id,
			coalesce(pud.price,0) as price,
			p.date_start,
			p.date_end,
			p.used,
			ap.value as is_pro_for
		FROM
			accounts a
			join account_params ap
			ON ( a.account_id = ap.account_id AND a.status = 'active' AND ap.name = 'is_pro_for' )
			join users u
			ON ( u.user_id = a.user_id )
			left join user_params up
			ON ( up.user_id = a.user_id AND up.name = 'first_approved_ad' )
			left join packs p
			ON ( a.account_id = p.account_id AND p.status = 'active' )
			left join purchase_detail pud
			ON ( pud.payment_group_id = p.payment_group_id )
		ORDER BY
			uid, email, date_start
	)
	LOOP
		IF NOT (string_to_array(l_rec.is_pro_for,',')::integer[] && l_pack_categories) THEN
			CONTINUE;
		END IF;

		o_uid := l_rec.uid;
		o_email := l_rec.email;
		o_phone := l_rec.phone;
		o_region := l_rec.region;
		o_first_ad := l_rec.first_approved_ad;
		o_product_id := l_rec.product_id;
		o_date_start := l_rec.date_start;
		o_date_end := l_rec.date_end;
		o_slots_used := l_rec.used;
		o_price := l_rec.price;
		o_pack_type := i_pack_type;
		
		IF o_email != l_last_email OR l_last_email IS NULL THEN
			SELECT count(ad_id)
			INTO o_published_ads
			FROM ads
			WHERE user_id = l_rec.user_id AND status in ('active', 'disabled') AND category = ANY( l_pack_categories );
			l_last_email := o_email;
		END IF;

		RETURN NEXT;
	END LOOP;
END;
$$ LANGUAGE plpgsql;
