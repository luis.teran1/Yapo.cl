CREATE OR REPLACE FUNCTION check_list_id_from_email(
						i_list_id ads.list_id%TYPE,
						i_account_email accounts.email%TYPE
						) RETURNS bool AS $$
BEGIN
	IF EXISTS(
		SELECT ads.list_id
		FROM accounts ac
			INNER JOIN users us USING(user_id)
			INNER JOIN ads USING(user_id)
		WHERE ac.email = i_account_email
			AND ads.list_id = i_list_id) THEN
			RETURN true;
	END IF;

	RETURN false;
END;
$$ LANGUAGE plpgsql;
