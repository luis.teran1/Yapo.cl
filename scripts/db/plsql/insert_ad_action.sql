CREATE OR REPLACE FUNCTION insert_ad_action(i_ad_id ad_actions.ad_id%TYPE,
		 			    i_action_type ad_actions.action_type%TYPE,
					    i_payment_group_id ad_actions.payment_group_id%TYPE,
					    OUT o_action_id ad_actions.action_id%TYPE) AS $$

DECLARE
    l_max_retry integer;

BEGIN
    -- add action_state
    SELECT
		COALESCE( max(action_id) + 1, 1)
    INTO
        o_action_id
    FROM
        ad_actions
    WHERE
        ad_id = i_ad_id;

    -- do a couple of retries in case there is someone else inserting at the same time
    l_max_retry := 5;

    -- insert action
    WHILE l_max_retry > 0 LOOP
        BEGIN
            INSERT INTO
                ad_actions
                (ad_id,
                 action_id,
                 action_type,
                 payment_group_id)
            VALUES
                (i_ad_id,
                 o_action_id,
                 i_action_type,
                 i_payment_group_id);
            RETURN;

        EXCEPTION WHEN UNIQUE_VIOLATION THEN
            o_action_id := o_action_id + 1;
        END;

        l_max_retry := l_max_retry - 1;
    END LOOP;

    RAISE EXCEPTION 'ERROR_INSERTING_AD_ACTION';
END;
$$ LANGUAGE plpgsql;
