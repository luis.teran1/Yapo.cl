CREATE OR REPLACE FUNCTION manage_account(
	i_email accounts.email%TYPE,
	i_account_id accounts.account_id%TYPE,
	i_status accounts.status%TYPE,
	i_token_id integer,
	OUT o_account_id accounts.account_id%TYPE,
	OUT o_email accounts.email%TYPE,
	OUT o_status accounts.status%TYPE,
	OUT o_account_action_id account_actions.account_action_id%TYPE
	
) AS $$
DECLARE
	l_transition enum_account_action_type;
BEGIN

	UPDATE
		accounts
	SET
		status = i_status,
		activation_date = NOW()
	WHERE
		CASE WHEN i_account_id IS NULL THEN email = lower(i_email) ELSE account_id = i_account_id END
	RETURNING
		account_id, email, status
	INTO
		o_account_id, o_email, o_status;

	IF NOT FOUND THEN
		RAISE EXCEPTION 'FACTORY_TRANSLATE:error:ACCOUNT_NOT_FOUND';
	END IF;

	l_transition := 'edition';

	IF i_status = 'active' THEN
		l_transition := 'activation';
	END IF;

	IF i_status = 'inactive' THEN
		l_transition := 'deactivation';
	END IF;

	o_account_action_id := insert_account_action(o_account_id,l_transition,i_token_id);

END;
$$ LANGUAGE plpgsql;
