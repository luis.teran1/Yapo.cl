CREATE OR REPLACE FUNCTION remove_products(
	i_ad_id ads.ad_id%TYPE,
	i_action_id ad_actions.action_id%TYPE,
	i_state_id action_states.state_id%TYPE,
	i_remote_addr action_states.remote_addr%TYPE,
	i_token_id tokens.token_id%TYPE
) RETURNS void AS $$
DECLARE
	l_products record;
	l_apply_changes integer;
BEGIN
	l_apply_changes := 0;
	FOR l_products IN (
		SELECT name,value FROM ad_params WHERE ad_id = i_ad_id AND name IN (
			'upselling_daily_bump',
			'upselling_weekly_bump',
			'daily_bump',
			'weekly_bump',
			'gallery',
			'label'
		)
	)
	LOOP
		PERFORM insert_ad_change(i_ad_id, i_action_id, i_state_id, true, l_products.name::varchar, NULL);
		l_apply_changes := 1;
	END LOOP;

	IF l_apply_changes = 1 THEN
		PERFORM apply_ad_changes(i_ad_id, i_action_id, NULL);
	END IF;
END;
$$ LANGUAGE plpgsql;
