CREATE OR REPLACE FUNCTION clear_minimum_info(i_ad_id ads.ad_id%TYPE,
					i_action_id ad_actions.action_id%TYPE,
					i_code payment_groups.code%TYPE,
					i_order_id varchar,
					i_transition action_states.transition%TYPE,
					i_status payment_groups.status%TYPE,
					i_amount pay_log.amount%TYPE,
					i_clear_status pay_log.status%TYPE,
					i_pay_time pay_log.paid_at%TYPE,
					i_token tokens.token%TYPE,
					i_store_id stores.store_id%TYPE,
					i_clear_expire_days integer,
					i_pack_conf varchar[][],
					i_ad_prods_wo_bump varchar[],
					OUT o_subject ads.subject%TYPE,
					OUT o_name ads.name%TYPE,
					OUT o_region ads.region%TYPE,
					OUT o_status integer,
					OUT o_sum_price payments.pay_amount%TYPE,
					OUT o_new_subject ad_changes.new_value%TYPE,
					OUT o_email users.email%TYPE,
					OUT o_ad_id ad_actions.ad_id%TYPE,
					OUT o_action_id ad_actions.action_id%TYPE,
					OUT o_action_type ad_actions.action_type%TYPE,
					OUT o_pack_result integer,
					OUT o_user_id ads.user_id%TYPE,
					OUT o_payment_group_id payment_groups.payment_group_id%TYPE,
					OUT o_need_filter_data bool
				) AS $$
DECLARE
	l_status payment_groups.status%TYPE;
	l_price payments.pay_amount%TYPE;
	l_code pay_log.code%TYPE;
	l_pay_type pay_log.pay_type%TYPE;
BEGIN
	o_need_filter_data := FALSE;
	-- Get payment info
	IF i_ad_id IS NOT NULL AND i_action_id IS NOT NULL THEN
		-- Lock to avoid double clear.

		o_user_id = get_user_id_by_ad_id(i_ad_id);

		SELECT
			SUM(pay_amount - discount),
			action_type,
			payment_group_id,
			payment_groups.code
		INTO
			o_sum_price,
			o_action_type,
			o_payment_group_id,
			l_code
		FROM
			ad_actions
			JOIN payment_groups USING (payment_group_id)
			JOIN payments USING (payment_group_id)
		WHERE
			state in ('unpaid', 'unverified') AND
			payment_groups.status in ('unpaid', 'unverified') AND
			ad_id = i_ad_id AND
			action_id = i_action_id
		GROUP BY
			action_type,
			payment_group_id,
			payment_groups.code,
			ad_actions.current_state;

		IF NOT FOUND THEN
			RAISE EXCEPTION 'No payment for ad: %, action: %', i_ad_id, i_action_id;
		END IF;

		o_ad_id := i_ad_id;
		o_action_id := i_action_id;

	ELSIF i_code IS NOT NULL THEN

		-- Lock to avoid double clear.
		SELECT
			payment_group_id
		INTO
			o_payment_group_id
		FROM
			payment_groups
		WHERE
			code = i_code AND
			status IN ('unpaid', 'unverified')
		ORDER BY
			payment_group_id DESC -- XXX Should check timestamp in action states interval 2 days
		LIMIT 1;

		BEGIN
			IF i_pay_time IS NOT NULL THEN
				o_user_id = get_user_id_by_timed_adcode(i_code, i_pay_time, i_clear_expire_days);
			ELSE
				o_user_id = get_user_id_by_paycode(i_code, i_clear_expire_days);
			END IF;
		EXCEPTION
			WHEN RAISE_EXCEPTION THEN
				o_status := 1;
				IF i_token IS NULL AND i_status = 'paid' THEN
					-- Check if payment already exists
					IF NOT can_insert_pay_log('payphone', i_code, i_pay_time, NULL) THEN
						o_status := 5;
					END IF;
				END IF;
				RETURN;
		END;

		SELECT
			sum(pay_amount - discount),
			ad_id,
			action_id,
			payment_groups.status,
			action_type
		INTO
			o_sum_price,
			o_ad_id,
			o_action_id,
			l_status,
			o_action_type
		FROM
			payment_groups
			JOIN payments USING (payment_group_id)
			JOIN ad_actions USING (payment_group_id)
			JOIN action_states USING (ad_id, action_id)
		WHERE
			current_state = state_id AND
			ad_actions.state in ('unpaid', 'unverified') AND
			payment_groups.status in ('unpaid', 'unverified') AND
			payment_groups.code = i_code AND
			timestamp > CURRENT_TIMESTAMP - (interval '1 day' * i_clear_expire_days)
		GROUP BY
			ad_id,
			action_id,
			payment_groups.status,
			action_type;

		IF NOT FOUND THEN
			RAISE EXCEPTION 'No payment for code: %', i_code;
		END IF;

	ELSIF i_order_id IS NOT NULL THEN
		o_payment_group_id := substring(i_order_id from '__#"%#"a%' for '#')::integer;

		SELECT
			sum(payments.pay_amount - discount),
			ad_id,
			action_id,
			payment_groups.status,
			action_type,
			user_id
		INTO
			o_sum_price,
			o_ad_id,
			o_action_id,
			l_status,
			o_action_type,
			o_user_id
		FROM
			payment_groups
			JOIN payments USING (payment_group_id)
			JOIN ad_actions USING (payment_group_id)
			JOIN ads USING (ad_id)
		WHERE
			ad_actions.state in ('unpaid', 'unverified') AND
			payment_groups.status in ('unpaid', 'unverified') AND
			payment_groups.payment_group_id = o_payment_group_id
		GROUP BY
			ad_id,
			action_id,
			payment_groups.status,
			action_type,
			user_id;

		IF NOT FOUND THEN
			-- Check if already paid.
			SELECT
				user_id,
				email
			INTO
				o_user_id,
				o_email
			FROM
				ad_actions
				JOIN ads USING (ad_id)
				JOIN users USING (user_id)
			WHERE
				ad_actions.payment_group_id = o_payment_group_id
				AND ad_actions.state IN ('pending_review', 'locked', 'accepted', 'rejected');

			IF NOT FOUND THEN
				o_payment_group_id := NULL;
			END IF;

			IF can_insert_pay_log ('paycard', i_order_id, i_pay_time, o_payment_group_id) THEN
				o_status := 1;
				IF i_clear_status != 'OK' OR o_user_id IS NULL THEN
					o_email := NULL;
				END IF;
			ELSE
				o_status := 5;
			END IF;
			RETURN;
		END IF;
	ELSE
		RAISE EXCEPTION 'ad_id, action_id, code or order_id must be supplied';
	END IF;

	-- Return subject and email for ad.
	SELECT
		subject,
		email,
		name,
		region
	INTO
		o_subject,
		o_email,
		o_name,
		o_region
	FROM
		ads
		JOIN users USING (user_id)
	WHERE
		ad_id = o_ad_id;

	o_status := 0;
	IF i_token IS NULL THEN

		IF i_clear_status IS NOT NULL AND i_clear_status != 'OK' THEN
			-- Zero amount since no payment was made.
			IF can_insert_pay_log ('paycard', i_order_id, i_pay_time, o_payment_group_id) THEN
				o_status := 1;
			ELSE
				o_status := 5;
			END IF;
			RETURN;
		END IF;

		-- Check the amount
		IF l_status = 'unpaid' THEN
			IF i_code IS NOT NULL THEN
				l_pay_type := 'payphone';
				l_code := i_code;
			ELSE
				l_pay_type := 'paycard';
				l_code := i_order_id;
			END IF;

			IF o_sum_price > i_amount THEN
				IF can_insert_pay_log (l_pay_type, l_code, i_pay_time, o_payment_group_id) THEN
					o_status := 2;
				ELSE
					o_status := 5;
				END IF;

				RETURN;
			ELSIF o_sum_price < i_amount THEN
				IF can_insert_pay_log (l_pay_type, l_code, i_pay_time, o_payment_group_id) THEN
					o_status := 3;
				ELSE
					o_status := 5;
					RETURN;
				END IF;
			ELSE
				IF can_insert_pay_log (l_pay_type, l_code, i_pay_time, o_payment_group_id) THEN
					o_status := 0;
				ELSE
					o_status := 5;
					RETURN;
				END IF;
			END IF;
		ELSE
			o_status := 4; -- verified

			-- We can not clear a bump without token in state <> unpaid
			IF o_action_type = 'bump' THEN
				RAISE EXCEPTION 'ERROR_PAYMENT_NOT_VALIDATED';
			END IF;

			-- Handle campaign ads
			IF i_transition = 'cleared_by' THEN
				l_pay_type := 'campaign';
			ELSIF i_transition = 'campaign_clear' THEN
				l_pay_type := 'campaign';
				IF o_action_type != 'editrefused' THEN
					IF subtract_free_ad(i_store_id) IS NULL THEN
						RAISE EXCEPTION 'ERROR_NO_FREE_ADS';
					END IF;
				END IF;
			ELSE
				l_pay_type := 'verify';
			END IF;

			IF NOT can_insert_pay_log(l_pay_type, i_code, i_pay_time, o_payment_group_id) THEN
				RAISE EXCEPTION 'ERROR_PAY_LOG_DUPLICATE';
			END IF;
		END IF;

	ELSE
		IF i_code IS NOT NULL THEN
			l_code := i_code;
		END IF;
		IF NOT can_insert_pay_log('adminclear', l_code, i_pay_time, o_payment_group_id) THEN
			RAISE EXCEPTION 'ERROR_PAY_LOG_DUPLICATE';
		END IF;
	END IF;

	IF EXISTS( SELECT * FROM unnest(i_ad_prods_wo_bump) WHERE unnest = o_action_type::varchar )THEN
		o_need_filter_data := FALSE;
	ELSIF o_action_type = 'bump' THEN
		o_need_filter_data := FALSE;
	ELSE
		o_need_filter_data := TRUE;
	END IF;

	IF o_need_filter_data THEN
		o_pack_result := get_pack_result(o_user_id, o_ad_id, o_action_id, o_action_type, i_pack_conf);
	ELSE
		o_pack_result := -1;
	END IF;

	-- Return the new subject of the ad
	SELECT
		new_value
	INTO
		o_new_subject
	FROM
		ad_changes
	WHERE
		ad_id = o_ad_id AND
		action_id = o_action_id AND
		column_name = 'subject';

END;
$$ LANGUAGE plpgsql;
