CREATE OR REPLACE FUNCTION video_stats(i_categories integer[],
				       i_start_time action_states.timestamp%TYPE,
				       i_end_time action_states.timestamp%TYPE,
				       OUT o_accepted bigint,
				       OUT o_refused bigint,
				       OUT o_total bigint) AS $$
BEGIN
	SELECT 
		COUNT(*) 
	INTO 
		o_accepted
	FROM 
		ads JOIN
		ad_actions USING (ad_id) JOIN 
		action_states using (ad_id, action_id) 
	WHERE 
		timestamp > i_start_time AND 
		timestamp < i_end_time AND 
		transition = 'accept' AND 
		queue = 'video' AND
		token_id IS NOT NULL AND
		category = ANY (i_categories);

	SELECT 
		COUNT(*) 
	INTO 
		o_refused
	FROM 
		ads JOIN
		ad_actions USING (ad_id) JOIN 
		action_states using (ad_id, action_id) 
	WHERE 
		timestamp > i_start_time AND 
		timestamp < i_end_time AND 
		transition = 'refuse' AND 
		queue = 'video' AND
		token_id IS NOT NULL AND
		category = ANY (i_categories);

	o_total := o_accepted + o_refused;
END
$$ LANGUAGE plpgsql;
