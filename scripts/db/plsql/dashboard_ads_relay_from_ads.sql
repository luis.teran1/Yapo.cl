CREATE OR REPLACE FUNCTION dashboard_ads_relay_from_ads(
) RETURNS TRIGGER AS $$
DECLARE
	l_ad_status enum_dashboard_status;
BEGIN
	IF TG_OP = 'UPDATE' THEN
		l_ad_status := NEW.status::varchar::enum_dashboard_status;

		IF OLD.status = 'deleted' and NEW.status = 'active' THEN
			l_ad_status := 'undo_deleted';
		END IF;

		UPDATE
			dashboard_ads
		SET
			list_id    = NEW.list_id,
			list_time  = NEW.list_time,
			subject    = NEW.subject,
			body       = NEW.body,
			category   = NEW.category,
			price      = NEW.price,
			ad_status  = l_ad_status,
			type       = NEW.type,
			user_id    = NEW.user_id,
			company_ad = NEW.company_ad,
			last_change = CURRENT_TIMESTAMP
		WHERE
			ad_id = NEW.ad_id;
	ELSIF TG_OP = 'INSERT' THEN
		INSERT INTO
			dashboard_ads (
				ad_id,
				user_id,
				list_id,
				list_time,
				subject,
				body,
				category,
				price,
				ad_status,
				type,
				company_ad
			)
		VALUES (
			NEW.ad_id,
			NEW.user_id,
			NEW.list_id,
			NEW.list_time,
			NEW.subject,
			NEW.body,
			NEW.category,
			NEW.price,
			NEW.status::varchar::enum_dashboard_status,
			NEW.type,
			NEW.company_ad
		);
	ELSIF TG_OP = 'DELETE' THEN
		DELETE FROM
			dashboard_ads
		WHERE
			ad_id = OLD.ad_id;
	END IF;
	RETURN NULL;
END;
$$ LANGUAGE plpgsql;
