CREATE OR REPLACE FUNCTION pack_change_ad_status(
    i_account_id accounts.account_id%TYPE,
    i_email accounts.email%TYPE,
    i_token_id integer,
    i_list_id ads.list_id%TYPE,
    i_ad_id ads.ad_id%TYPE,
    i_new_status varchar,
    i_remote_addr varchar,
    i_pack_conf varchar[][],
    OUT o_available_slots integer,
    OUT o_email varchar,
    OUT o_ad_id ads.ad_id%TYPE
) AS $$
DECLARE
    l_pack_id packs.pack_id%TYPE;
    l_ad_status ads.status%TYPE;
    l_ad_category ads.category%TYPE;
    l_ad_user_id ads.user_id%TYPE;
    l_user_id accounts.user_id%TYPE;
    l_is_pro_for varchar;
    l_pack_type varchar;
    l_pack_categories integer[][];
BEGIN

    -- Find the ad by ad_id or list_id depending of param received
    SELECT ad_id, status, category, user_id
    INTO o_ad_id, l_ad_status, l_ad_category, l_ad_user_id
    FROM ads
    WHERE
        CASE WHEN i_list_id IS NULL THEN
            ad_id = i_ad_id
        ELSE
            list_id = i_list_id
        END
    ;
    -- If the ad was not found returns error
    IF NOT FOUND THEN
        RAISE EXCEPTION 'FACTORY_TRANSLATE:error:ERROR_AD_NOT_FOUND';
    END IF;

    -- Search for account data:
    --   using the user_id of ad if token exist
    --   or use the other params
    SELECT user_id, account_id, email
    INTO l_user_id, i_account_id, o_email
    FROM accounts
    WHERE
        CASE
            WHEN i_token_id IS NOT NULL THEN
                user_id = l_ad_user_id
            WHEN i_account_id IS NOT NULL THEN
                account_id = i_account_id
            ELSE
                email = i_email
        END
    ;

    -- When account was not found returns error
    IF NOT FOUND THEN
        RAISE EXCEPTION 'FACTORY_TRANSLATE:error:ERROR_ACCOUNT_NOT_FOUND';
    END IF;

    -- Check if the user id from the ad is the same of the user id from account
    IF l_ad_user_id != l_user_id THEN
        RAISE EXCEPTION 'FACTORY_TRANSLATE:error:ERROR_AD_NOT_BELONG_ACCOUNT';
    END IF;

    -- Check if the category ad belongs to a configured pack category
    l_pack_type := find_pack_type(l_ad_category, i_pack_conf);
    IF l_pack_type is NULL THEN
        RAISE EXCEPTION 'FACTORY_TRANSLATE:error:ERROR_CATEGORY_NOT_CONFIGURED_ON_PACK';
    END IF;

    -- Search for is_pro_for param on account to check if the user is pro on this categories
    l_pack_categories := find_pack_categories(l_pack_type, i_pack_conf);
    SELECT value INTO l_is_pro_for
    FROM account_params
    WHERE account_id = i_account_id AND name = 'is_pro_for';

    IF l_is_pro_for IS NULL
        OR NOT (string_to_array(l_is_pro_for,',')::integer[] && l_pack_categories)
    THEN
        RAISE EXCEPTION 'FACTORY_TRANSLATE:error:ERROR_USER_NOT_PRO_FOR_AD_CATEGORY';
    END IF;

    IF i_new_status = 'active' THEN
        -- The only transition to activate
        --  disabled -> active
        -- So if the ad status is not disabled returns error
        IF l_ad_status != 'disabled' THEN
            RAISE EXCEPTION 'FACTORY_TRANSLATE:error:ERROR_AD_INVALID_STATUS_TRANSITION';
        END IF;

        -- Search and lock the oldest active pack with slots available for this account_id
        SELECT pack_id INTO l_pack_id
        FROM packs
        WHERE account_id = i_account_id AND status = 'active' AND slots - used > 0
        ORDER BY date_start ASC
        LIMIT 1 FOR UPDATE;

        -- Only can activate when exists slots available
        IF NOT FOUND THEN
            RAISE EXCEPTION 'FACTORY_TRANSLATE:error:ERROR_NO_SLOTS_AVAILABLE';
        END IF;

        -- Update the slots of pack selected
        UPDATE packs
        SET used = used + 1
        WHERE pack_id = l_pack_id;

    ELSE -- here new status is disabled
        -- The transitions allowed to disable are:
        --  active -> disabled
        --  deactivated -> disabled
        -- So if the status didn't match returns error
        IF l_ad_status != 'active' AND l_ad_status != 'deactivated' THEN
            RAISE EXCEPTION 'FACTORY_TRANSLATE:error:ERROR_AD_INVALID_STATUS_TRANSITION';
        END IF;

        -- Only if the ad is active release one slot because with the ads deactivated the slot was
        -- released in other process.
        IF l_ad_status = 'active' THEN

            -- Search and lock the newest pack with slots used
            SELECT pack_id  INTO l_pack_id
            FROM packs
            WHERE account_id = i_account_id AND status = 'active' AND used > 0
            ORDER BY date_start DESC
            LIMIT 1 FOR UPDATE;

            -- If the pack was not found returns error
            IF NOT FOUND THEN
                RAISE EXCEPTION 'FACTORY_TRANSLATE:error:ERROR_NO_SLOTS_USED';
            END IF;

            -- Release the slot
            UPDATE packs
            SET used = used - 1
            WHERE pack_id = l_pack_id;

        END IF;

    END IF;

    -- Process the new status change on ad
    PERFORM ad_status_change(o_ad_id, i_new_status::enum_ads_status, i_remote_addr, i_token_id);

    -- Query to return the available slots
    -- (this param was updated by triggers when the pack was updated)
    SELECT COALESCE(value::integer, 0) INTO o_available_slots
    FROM account_params
    WHERE account_id = i_account_id and name = 'available_slots';

END;
$$ LANGUAGE plpgsql;
