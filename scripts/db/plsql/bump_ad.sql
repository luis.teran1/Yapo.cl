CREATE OR REPLACE FUNCTION bump_ad(i_ad_id ads.ad_id%TYPE,
		i_remote_addr action_states.remote_addr%TYPE,
		i_batch_bump integer,
		i_action_id ad_actions.action_id%TYPE,
		i_token_id action_states.token_id%TYPE,
		i_new_list_time ads.list_time%TYPE,
		i_link_type_group_check_whitelist varchar[],
		i_batch boolean,
		i_bump_transition varchar,
		i_update_ad_status boolean,
		o_action_id OUT ad_actions.action_id%TYPE,
		o_list_time OUT ads.list_time%TYPE) AS $$
DECLARE
	l_state_id 		action_states.state_id%TYPE;
	l_i 			integer;
	l_store_id 		integer;
	l_link_type 		varchar;
	l_rec 			record;
	l_orig_list_time 	ads.orig_list_time%TYPE;
	l_paycode 		payment_groups.code%TYPE;
	l_payment_group_id  	payment_groups.payment_group_id%TYPE;
	l_status				ads.status%TYPE;
	l_update_query_ad       varchar;
BEGIN
	IF i_token_id IS NULL AND i_action_id IS NULL AND i_batch IS NULL THEN
		RAISE EXCEPTION 'ERROR_PARAMS_MISSING';
	END IF;

	l_payment_group_id := NULL;

	SELECT
		list_time,
		link_type.value,
		store_id,
		status,
		orig_list_time
	INTO
		o_list_time,
		l_link_type,
		l_store_id,
		l_status,
		l_orig_list_time
	FROM
		ads
		LEFT JOIN ad_params AS link_type ON
		link_type.ad_id = ads.ad_id
		AND link_type.name = 'link_type'
	WHERE
		ads.ad_id = i_ad_id
		AND status IN ('active', 'hidden', 'inactive', 'disabled', 'deactivated');

	IF NOT FOUND THEN
		IF EXISTS(SELECT * FROM ads WHERE ad_id = i_ad_id) THEN
			RAISE EXCEPTION 'ERROR_AD_NOT_ACTIVE';
		ELSE
			RAISE EXCEPTION 'ERROR_AD_NOT_FOUND';
		END IF;
	END IF;

	IF i_action_id IS NOT NULL AND i_token_id IS NULL THEN
		IF NOT EXISTS (
			SELECT
				*
		 	FROM
				ad_actions
			WHERE
				ad_id = i_ad_id
				AND action_id = i_action_id
				AND state = 'pending_bump'
			) THEN
			RAISE EXCEPTION 'ERROR_ACTION_NOT_FOUND';
		END IF;
		o_action_id := i_action_id;
	ELSE
		-- Insert pay stuff when is admin
		IF i_token_id IS NOT NULL THEN
			SELECT 
				o_payment_group_id
			INTO
				l_payment_group_id
			FROM
				payment_create(null,'cleared','adminclear','{bump}','{0}',null,null,i_remote_addr,i_token_id,null);
		END IF;

		-- Create new action
		o_action_id := insert_ad_action(i_ad_id, i_bump_transition::enum_ad_actions_action_type, l_payment_group_id);
		PERFORM insert_state(i_ad_id,
				o_action_id, 
				'reg', 
				'initial', 
				i_remote_addr, 
				NULL);
	END IF;

	l_state_id := insert_state(i_ad_id,
			o_action_id, 
			'accepted',
			i_bump_transition::enum_action_states_transition,
			i_remote_addr, 
			i_token_id);
	-- Is admin?
	IF l_payment_group_id IS NOT NULL AND i_token_id IS NOT NULL THEN
		--Insert new pay_log
		PERFORM insert_pay_log('adminclear', null, 0, null, null, null, i_remote_addr, i_token_id, l_payment_group_id, 'OK');
	END IF;

	IF l_orig_list_time IS NULL THEN
		l_orig_list_time := o_list_time;
	END IF;

	-- Update list time
	o_list_time := coalesce(i_new_list_time, CURRENT_TIMESTAMP);
	IF l_link_type IS NOT NULL AND i_batch_bump > 0 THEN
		o_list_time := adjust_imported_list_time(l_link_type, 
							 l_store_id, 
							 coalesce(i_new_list_time, o_list_time), 
							 i_link_type_group_check_whitelist);
	END IF;

	-- Format UPDATE query and executed
	l_update_query_ad := FORMAT('
		UPDATE
			ads
		SET
			list_time = ''%s'',
			modified_at = ''%s'',
			orig_list_time = ''%s''',
		o_list_time,
		CURRENT_TIMESTAMP,
		coalesce(l_orig_list_time, o_list_time)
	);
	IF i_update_ad_status THEN
		-- Update status only when i_update_ad_status is TRUE
		l_update_query_ad := FORMAT('
			%s, status = (CASE WHEN ''%s'' = ''deactivated'' THEN ''active'' ELSE status END)',
			l_update_query_ad,
			l_status
		);
	END IF;
	EXECUTE FORMAT('%s WHERE ad_id=%s', l_update_query_ad, i_ad_id);

	FOR
		l_rec
	IN
		SELECT
			name,seq_no
		FROM
			ad_images
		WHERE
			ad_id = i_ad_id
		ORDER BY
			seq_no
	LOOP
		INSERT INTO 
			ad_media(
				ad_media_id, 
				ad_id, 
				seq_no, 
				upload_time,
				media_type)
		VALUES (
			TRIM(trailing '.jpg' from l_rec.name)::bigint,
			i_ad_id,
			l_rec.seq_no,
			CURRENT_TIMESTAMP,
			'image');
	END LOOP;

END
$$ LANGUAGE plpgsql;
