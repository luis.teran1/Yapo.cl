CREATE OR REPLACE FUNCTION has_abuse_notices(i_user_id users.user_id%TYPE) RETURNS bool AS $$
BEGIN
	RETURN EXISTS (
		SELECT
			*
		FROM
			notices
		WHERE
			notices.abuse = 't' AND 
			(
				notices.user_id = i_user_id OR
				notices.uid = (SELECT uid FROM users WHERE user_id = i_user_id)
			)
		);
END;
$$ LANGUAGE plpgsql;
