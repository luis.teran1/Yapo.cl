CREATE OR REPLACE FUNCTION save_stats(
					i_stat_type varchar,
					i_ext_id integer,
					i_date date,
					i_hour time,
					i_value integer,
					OUT o_stat_id varchar) AS $$
DECLARE
	l_stat_id integer;
    l_schema varchar;
	l_value integer;
BEGIN
	-- NOTE: If you want the statistic of the whole year you have to ask for the schema blocket_<yyyy>
	-- and blocket_<yyyy + 1> because on january 1st the stats of december 31st are persisted on the new
	-- year schema. (e.g. for stats of 2013, query to the whole year and in the particular case of  december
	-- 31 ask on blocket_2014 instead of blocket_2013)
    l_schema := 'blocket_' || extract(year from CURRENT_TIMESTAMP);

	EXECUTE format('SELECT stat_id FROM %s.stats WHERE stat_type = ''%s'' and ext_id = %s ;', l_schema, i_stat_type, i_ext_id)
	INTO
		l_stat_id;


	IF l_stat_id IS NOT NULL THEN

		EXECUTE format('SELECT value FROM %s.stats_detail WHERE stat_id = %s AND reg_date = ''%s'' AND reg_hour = ''%s'';', l_schema, l_stat_id, i_date, i_hour)
		INTO
			l_value;

		IF l_value IS NOT NULL THEN
			RAISE EXCEPTION 'FACTORY_TRANSLATE:error:ERROR_STAT_ALREADY_EXIST';
		END IF;

	-- MAKE UPDATE OF TOTAL
		EXECUTE format('UPDATE %s.stats	SET total = total + %s WHERE stat_id = %s;', l_schema, i_value, l_stat_id);
	ELSE
	-- INSERT TOTAL
		EXECUTE format('INSERT INTO %s.stats (stat_type, ext_id, total) VALUES (''%s'', %s, %s  ) ;', l_schema, i_stat_type, i_ext_id, i_value);

	-- SEARCH FOR ID
		EXECUTE format('SELECT stat_id FROM %s.stats WHERE stat_type = ''%s'' and ext_id = %s ;', l_schema, i_stat_type, i_ext_id)
		INTO
			l_stat_id;

	END IF;

	-- MAKE INSERT IN DETAIL
		EXECUTE format('INSERT INTO %s.stats_detail	(stat_id, reg_date, reg_hour, value) VALUES (%s, ''%s'', ''%s'', %s) ;', l_schema, l_stat_id, i_date, i_hour, i_value);

	o_stat_id := l_stat_id;

END
$$ LANGUAGE plpgsql;
