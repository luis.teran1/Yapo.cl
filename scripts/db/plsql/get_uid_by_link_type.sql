CREATE OR REPLACE FUNCTION get_uid_by_link_type(i_link_type VARCHAR(50),
                                            OUT o_uid users.uid%TYPE) AS $$
BEGIN
       SELECT
               uid
       INTO
               o_uid
       FROM
               ads JOIN ad_params USING(ad_id) JOIN users using(user_id)
       WHERE
               ad_params.name = 'link_type' AND ad_params.value = i_link_type
       LIMIT 1;
END;
$$ LANGUAGE plpgsql;
