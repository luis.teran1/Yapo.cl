CREATE OR REPLACE FUNCTION scarface_edit(i_ad_id ads.ad_id%TYPE,
		i_category ads.category%TYPE,
		i_company_ad ads.company_ad%TYPE,
		i_type ads.type%TYPE,
		i_price ads.price%TYPE,
		i_remote_addr action_states.remote_addr%TYPE,
		i_token tokens.token%TYPE) RETURNS VOID AS $$
DECLARE
	l_ad_id ads.ad_id%TYPE;
	l_state_id action_states.state_id%TYPE;
	l_action_id ad_actions.action_id%TYPE;
BEGIN
	SELECT
		ads.ad_id
	INTO
		l_ad_id
	FROM
		ads
	WHERE
		ads.ad_id = i_ad_id
		AND status IN ('active', 'hidden');
	
	IF NOT FOUND THEN
		RAISE EXCEPTION 'ERROR_AD_NOT_ACTIVE';
	END IF;

	-- Create new action
	l_action_id := insert_ad_action(i_ad_id, 'scarface_edit', NULL);
	l_state_id := insert_state(i_ad_id,
			l_action_id, 
			'reg', 
			'initial', 
			i_remote_addr, 
			NULL);

	-- EDIT
	PERFORM insert_ad_change(i_ad_id, l_action_id, l_state_id, false, 'category', i_category::text);
	PERFORM insert_ad_change(i_ad_id, l_action_id, l_state_id, false, 'company_ad', i_company_ad::text);
	PERFORM insert_ad_change(i_ad_id, l_action_id, l_state_id, false, 'type', i_type::text);
	PERFORM insert_ad_change(i_ad_id, l_action_id, l_state_id, false, 'price', i_price::text);

	-- apply ad_changes
	PERFORM apply_ad_changes(i_ad_id, l_action_id, NULL);

	PERFORM insert_state(i_ad_id,
			l_action_id, 
			'accepted',
			'scarface_edit', 
			i_remote_addr, 
			get_token_id(i_token));
END
$$ LANGUAGE plpgsql;
