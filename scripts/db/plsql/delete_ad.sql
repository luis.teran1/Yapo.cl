CREATE OR REPLACE FUNCTION delete_ad(i_list_id ads.list_id%TYPE,
				i_passwd ads.passwd%TYPE,
				i_remote_addr action_states.remote_addr%TYPE,
				i_token tokens.token%TYPE,
				i_reason action_states.transition%TYPE,
				i_source action_params.value%TYPE,
				i_deletion_reason action_params.value%TYPE,
				i_user_text action_params.value%TYPE,
				i_testimonial user_testimonial.content%TYPE,
				i_site_area enum_user_testimonial_areas,
				i_deletion_timer action_params.value%TYPE,
				i_pack_conf varchar[][],
				OUT o_status integer,
				OUT o_action_id ad_actions.action_id%TYPE) AS $$
DECLARE
	l_ad_id		ads.ad_id%TYPE;
	l_status	ads.status%TYPE;
	l_passwd	ads.passwd%TYPE;
	l_store_id	ads.store_id%TYPE;
	l_unfinished_actions record;
	l_state_id action_states.state_id%TYPE;
BEGIN
	o_status := 0;

	SELECT
		ad_id,
		passwd,
		status,
		store_id
	INTO
		l_ad_id,
		l_passwd,
		l_status,
		l_store_id
	FROM
		ads
	WHERE
		list_id = i_list_id
	FOR UPDATE;

	IF NOT FOUND THEN
		o_status := 1;
		RETURN;
	END IF;

	-- check status, delete only valid on this status
	IF l_status NOT IN ('active', 'unpublished', 'hidden', 'disabled', 'deactivated') THEN
		o_status := 2;
		RETURN;
	END IF;	

	-- Require password or token, except for imported ads.
	IF EXISTS(SELECT * FROM ad_params WHERE ad_id = l_ad_id AND name = 'external_ad_id') THEN
		-- XXX prevent delete if passwd or token?
	ELSE
		-- if passwd check passwd
		IF i_passwd IS NOT NULL THEN
			IF l_passwd != i_passwd THEN
				o_status := 3; 
				RETURN;
			END IF;
		END IF;

		-- if store token check store id.
		IF i_token IS NOT NULL AND get_auth_type(i_token) = 'store' AND (l_store_id IS NULL OR l_store_id != get_auth_resource(i_token)) THEN
			o_status := 3;
			RETURN;
		END IF;
	END IF;

	PERFORM remove_unfinished_actions(l_ad_id, i_remote_addr, get_token_id(i_token));

	SELECT
		iaas.o_action_id,
		iaas.o_state_id
	INTO
		o_action_id,
		l_state_id
	FROM
		insert_ad_action_state(
			l_ad_id,
			'delete',
			'deleted',
			i_reason,
			i_remote_addr,
			get_token_id(i_token)
		) AS iaas;

	IF i_source IS NOT NULL THEN
		INSERT INTO
			action_params (ad_id, action_id, name, value)
		VALUES (l_ad_id,
			o_action_id,
			'source',
			i_source);
	END IF;

	IF i_deletion_reason IS NOT NULL THEN
		INSERT INTO
			action_params (ad_id, action_id, name, value)
		VALUES (l_ad_id,
			o_action_id,
			'deletion_reason',
			i_deletion_reason);
	END IF;
	
	IF i_deletion_timer IS NOT NULL THEN
		INSERT INTO
			action_params (ad_id, action_id, name, value)
		VALUES (l_ad_id,
			o_action_id,
			'deletion_timer',
			i_deletion_timer);
	END IF;

	IF i_user_text IS NOT NULL THEN
		INSERT INTO
			action_params (ad_id, action_id, name, value)
		VALUES (l_ad_id,
			o_action_id,
			'user_text',
			i_user_text);
	END IF;

	IF i_testimonial IS NOT NULL THEN
		INSERT INTO
			user_testimonial (ad_id, area, content)
		VALUES (l_ad_id,
			i_site_area,
			i_testimonial);
	END IF;

	--Check Pack to make it 'free' to a slot
	PERFORM check_and_free_pack_slot(l_ad_id, o_action_id, l_state_id, i_pack_conf);

	-- change ad status to deleted
	UPDATE
		ads
	SET
		status = 'deleted',
		modified_at = CURRENT_TIMESTAMP
	WHERE
		ad_id = l_ad_id;
	
	-- Delete from watch_ads
	DELETE FROM
		watch_ads
	WHERE
		list_id = i_list_id;
END;
$$ LANGUAGE plpgsql;
