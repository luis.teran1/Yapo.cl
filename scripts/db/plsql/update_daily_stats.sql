CREATE OR REPLACE FUNCTION update_daily_stats(i_stat_time stats_daily.stat_time%TYPE,
					      i_stat_name stats_daily.name%TYPE,
					      i_stat_name_suffix stats_daily.name%TYPE,
					      i_stat_value stats_daily.value%TYPE,
					      i_is_delta bool,
					      OUT o_value stats_daily.value%TYPE) AS $$
DECLARE
	l_prev_value stats_daily.value%TYPE;
	l_new_value stats_daily.value%TYPE;
	l_stat_time stats_daily.stat_time%TYPE;
	l_stat_name stats_daily.name%TYPE;
BEGIN
	l_new_value := i_stat_value;

	IF i_stat_name_suffix IS NULL THEN
		l_stat_name := i_stat_name;
	ELSE
		l_stat_name := i_stat_name || i_stat_name_suffix;
	END IF;

	IF i_stat_time IS NULL THEN
		l_stat_time := CURRENT_DATE;
	ELSE
		l_stat_time := i_stat_time;
	END IF;

	SELECT 
		value
	INTO
		l_prev_value
	FROM
		stats_daily
	WHERE
		stat_time = l_stat_time AND
		name = l_stat_name;

	IF FOUND THEN
		IF i_is_delta THEN
			l_new_value := l_prev_value::integer + i_stat_value::integer;
		END IF;

		UPDATE
			stats_daily
		SET
			value = l_new_value
		WHERE	
			stat_time = l_stat_time AND
			name = l_stat_name;
	ELSE
		INSERT INTO 
			stats_daily
		VALUES
			(	
				l_stat_time,
				l_stat_name,
				i_stat_value
			);
	END IF;
	o_value = l_new_value;
END;
$$ LANGUAGE plpgsql;
