CREATE OR REPLACE FUNCTION insert_purchase(
	i_doc_type purchase.doc_type%TYPE,
	i_receipt_date purchase.receipt%TYPE,
	i_rut purchase.rut%TYPE,
	i_name purchase.name%TYPE,
	i_lob purchase.lob%TYPE,
	i_address purchase.address%TYPE,
	i_region purchase.region%TYPE,
	i_commune purchase.communes%TYPE,
	i_contact purchase.contact%TYPE,
	i_email purchase.email%TYPE,
	i_store_id stores.store_id%TYPE,
	i_product_id integer[],
	i_product_price integer[],
	i_product_name varchar[],
	i_product_params varchar[],
	i_ads_list_id integer[],
	i_seller_id purchase.seller_id%TYPE,
	i_payment_method purchase.payment_method%TYPE,
	i_payment_platform purchase.payment_platform%TYPE,
	i_tax purchase.tax%TYPE,
	i_remote_addr purchase_states.remote_addr%TYPE,
	i_token_id tokens.token_id%TYPE,
	i_account_id accounts.account_id%TYPE,
	i_conf_products_store varchar[],
	i_conf_products_ad_id varchar[],
	i_conf_products_list_id varchar[],
	OUT o_payment_group_id purchase.payment_group_id%TYPE,
	OUT o_pay_code payment_groups.code%TYPE,
	OUT o_purchase_id purchase.purchase_id%TYPE,
	OUT o_purchase_status purchase.status%TYPE

) AS $$
DECLARE
	l_ad_id		ads.ad_id%TYPE;
	l_status	ads.status%TYPE;
	l_purchase_id	purchase.purchase_id%TYPE;
	l_payment_group_id payment_groups.payment_group_id%TYPE;
	l_action_id ad_actions.action_id%TYPE;
	l_total_price integer;
	l_prod_name varchar;
	l_ab_param varchar;
	l_ab_param_n enum_purchase_detail_params_name;
	l_ab_param_v varchar;
	l_ab_purchase_detail_id integer;
	l_counter integer;
	l_parent_pg_id integer;
	l_parent_paycode varchar;
	la_payment_groups integer[];
	la_ads_id integer[];
	la_list_ids_bump integer[];
	la_list_ids_weekly integer[];
	la_ad_ids_upselling integer[];
	la_ad_ids_upselling_label integer[];
	la_ad_ids_upselling_combos integer[];
	la_list_ids_gallery integer[];
	la_list_ids_label integer[];
	id integer;
BEGIN

	-- CALCULATE TOTAL
	select sum(elements) into l_total_price from unnest(i_product_price) as elements;

	-- IF NEED PAY 2 OR MORE PRODUCTS GENERATE A PARENT payment_group_id
	l_parent_pg_id := NULL;
	IF array_length(i_product_id,1) > 1 THEN
		l_parent_paycode := get_ad_code('pay');
		INSERT INTO payment_groups ( code, status ) VALUES (l_parent_paycode, 'unverified') RETURNING payment_group_id INTO l_parent_pg_id;
	END IF;

	-- VALIDATE ADS FOR PRODUCTS OF ADS (FOUND & STATUS)
	l_counter := 1;
	FOR l_prod_name IN SELECT unnest(i_product_name)
	LOOP
		IF EXISTS ( SELECT * FROM  unnest( i_conf_products_list_id ) WHERE unnest = l_prod_name) THEN
			SELECT	ad_id,status
			INTO	l_ad_id,l_status
			FROM	ads
			WHERE	list_id = i_ads_list_id[l_counter];
			IF NOT FOUND THEN
				RAISE EXCEPTION 'FACTORY_TRANSLATE:error:ERROR_AD_NOT_FOUND';
			END IF;
			-- check status, BUMP operate only on active and deactivated ads
			IF l_status NOT IN ('active', 'deactivated') AND l_prod_name = 'bump' THEN
				RAISE EXCEPTION 'FACTORY_TRANSLATE:error:ERROR_AD_NOT_ACTIVE';
			END IF;
		END IF;
		IF EXISTS ( SELECT * FROM  unnest( i_conf_products_ad_id ) WHERE unnest = l_prod_name) THEN
			SELECT	ad_id,status
			INTO	l_ad_id,l_status
			FROM	ads
			WHERE	ad_id = i_ads_list_id[l_counter];
			IF NOT FOUND THEN
				RAISE EXCEPTION 'FACTORY_TRANSLATE:error:ERROR_AD_NOT_FOUND';
			END IF;
		END IF;
		l_counter := l_counter + 1 ;
	END LOOP;

	--------------------------------------------
	-- START MAKING STUFF
	l_counter := 1;
	FOR l_prod_name IN SELECT unnest(i_product_name)
	LOOP
		IF EXISTS ( SELECT * FROM  unnest( i_conf_products_list_id ) WHERE unnest = l_prod_name) THEN
			SELECT 	ad_id, status
			INTO   	l_ad_id,	l_status
			FROM 	ads
			WHERE	list_id = i_ads_list_id[l_counter];
		END IF;
		IF EXISTS ( SELECT * FROM  unnest( i_conf_products_ad_id ) WHERE unnest = l_prod_name) THEN
			SELECT 	ad_id, status
			INTO   	l_ad_id,	l_status
			FROM 	ads
			WHERE	ad_id = i_ads_list_id[l_counter];
		END IF;
		--Inserting a new payment_group and similar stuff
		SELECT *
		INTO
			l_payment_group_id, o_pay_code
		FROM
			insert_payment_purchase(i_product_price[l_counter], i_remote_addr, l_prod_name::enum_payments_payment_type, l_parent_pg_id);

		la_payment_groups := array_append(la_payment_groups, l_payment_group_id);
		IF EXISTS ( SELECT * FROM  unnest( i_conf_products_list_id  ) WHERE unnest = l_prod_name)
		   OR EXISTS ( SELECT * FROM  unnest( i_conf_products_ad_id  ) WHERE unnest = l_prod_name)
		THEN
			la_ads_id := array_append( la_ads_id, l_ad_id);
		ELSE
			la_ads_id := array_append( la_ads_id, null);
		END IF;
		l_counter := l_counter + 1 ;
	END LOOP;

	IF l_counter > 2 THEN
		o_pay_code := l_parent_paycode;
	END IF;
	IF l_counter = 2 THEN
		l_parent_pg_id := l_payment_group_id;
	END IF;

	-- For payment with credits use voucher as doc_type
	IF i_payment_method = 'credits' THEN
		i_doc_type := 'voucher';
	END IF;
	-- Insert the header of purchase
	INSERT INTO purchase(
		doc_type,  status, rut, name, lob, address, region, communes, contact, email, seller_id, payment_method, tax, total_price, payment_group_id, account_id, payment_platform
	) VALUES (
		i_doc_type, 'pending', i_rut, i_name, i_lob, i_address, i_region, i_commune, i_contact, i_email, i_seller_id, i_payment_method, i_tax, l_total_price, l_parent_pg_id, i_account_id, i_payment_platform
	)
	RETURNING purchase_id
	INTO l_purchase_id;

	-- Insert the first state
	INSERT INTO purchase_states (purchase_id, status, remote_addr) VALUES (l_purchase_id,'pending',i_remote_addr);

	l_counter := 1;
	FOR l_prod_name IN  SELECT unnest(i_product_name)
	LOOP
		-- if the product need create an ad_action
		IF EXISTS ( SELECT * FROM  unnest( i_conf_products_list_id  ) WHERE unnest = l_prod_name)
		   OR EXISTS ( SELECT * FROM  unnest( i_conf_products_ad_id  ) WHERE unnest = l_prod_name)
		THEN
			-- Create new ad_action
			l_action_id := insert_ad_action(la_ads_id[l_counter], l_prod_name::enum_ad_actions_action_type, la_payment_groups[l_counter]);

			-- Adding label type to action params
			IF l_prod_name in ('label', 'upselling_label', 'upselling_advanced_inmo', 'upselling_premium_cars', 'upselling_premium_inmo', 'upselling_premium_others', 'at_combo3', 'if_pack_car_premium') THEN
				INSERT INTO action_params (ad_id, action_id, name, value) VALUES (la_ads_id[l_counter], l_action_id, 'label_type', i_product_params[l_counter]);
			END IF;
			-- Create the states for the action
			PERFORM insert_state(la_ads_id[l_counter],l_action_id,'reg','initial',i_remote_addr,i_token_id);
			PERFORM insert_state(la_ads_id[l_counter],l_action_id,'unpaid','pending_pay',i_remote_addr,i_token_id);
		-- else if the product need create a store_action
		ELSIF EXISTS ( SELECT * FROM unnest(i_conf_products_store) WHERE unnest =  l_prod_name ) THEN
			IF i_store_id IS NULL THEN
				RAISE EXCEPTION 'FACTORY_TRANSLATE:error:ERROR_STORE_ID_MISSING';
			END IF;
			PERFORM insert_store_action_state(i_store_id,'extend','unpaid',i_token_id,la_payment_groups[l_counter]);
		ELSE
			l_action_id := NULL;
		END IF;

		-- insert each detail
		INSERT INTO purchase_detail (purchase_id,product_id,price,ad_id,action_id,payment_group_id)
		VALUES (l_purchase_id,i_product_id[l_counter],i_product_price[l_counter],la_ads_id[l_counter],l_action_id,la_payment_groups[l_counter])
		RETURNING purchase_detail_id into l_ab_purchase_detail_id;

		-- Insert on params
		IF l_prod_name = 'autofact' THEN
			INSERT INTO purchase_detail_params
			VALUES (l_ab_purchase_detail_id, 'plates', i_product_params[l_counter]);
		END IF;

		IF l_prod_name = 'autobump' OR (l_prod_name = 'promo_bump' AND i_product_params[l_counter] <> '0') THEN
			-- for all autobumps params
			FOR l_ab_param IN SELECT unnest(string_to_array(i_product_params[l_counter], ';'))
			LOOP
				-- get name, value and purchase_detail_id and inserted in purchase_detail_params
				SELECT
					substring(l_ab_param, 0, position(':' in l_ab_param)), substring(l_ab_param, position(':' in l_ab_param)+1)
				INTO
					l_ab_param_n, l_ab_param_v;

				INSERT INTO purchase_detail_params
				VALUES (l_ab_purchase_detail_id, l_ab_param_n, l_ab_param_v);
			END LOOP;
		END IF;

		l_counter := l_counter + 1;
	END LOOP;

	SELECT
		status
	INTO
		o_purchase_status
	FROM
		purchase
	WHERE
		purchase_id = l_purchase_id;
	o_payment_group_id := l_parent_pg_id;
	o_purchase_id := l_purchase_id;

END;
$$ LANGUAGE plpgsql;
