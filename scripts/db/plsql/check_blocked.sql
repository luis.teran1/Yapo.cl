CREATE OR REPLACE FUNCTION check_blocked(i_list_id blocked_items.list_id%TYPE,
					 i_whole_word integer,
					 i_value blocked_items.value%TYPE) RETURNS blocked_items.value%TYPE AS $$
DECLARE
	l_list_type block_lists.list_type%TYPE;
BEGIN
	IF i_value IS NULL THEN
		RETURN NULL;
	END IF;

	SELECT
		list_type
	INTO 
		l_list_type
	FROM
		block_lists
	WHERE 
		list_id = i_list_id;

	IF l_list_type = 'ip' THEN -- this can be done a lot better, using inet type (LBC US348)
		RETURN (SELECT value FROM blocked_items WHERE list_id = i_list_id AND i_value LIKE (value || '%') LIMIT 1);
	ELSIF i_whole_word = 0 THEN -- PostgreSQL does not like special characters inside [], it seems.
		RETURN (SELECT value FROM blocked_items WHERE list_id = i_list_id AND i_value ~* regexp_replace(value, E'([\\^\\$\\\\\\(\\)\\{\\}\\[\\]\\*\\+\\?])', E'\\\\\\1', 'g') LIMIT 1);
	ELSIF i_whole_word = 2 THEN -- exact match, no regex makes servers happy
		RETURN (SELECT value FROM blocked_items WHERE list_id = i_list_id AND lower(i_value) = lower(value) LIMIT 1);
	ELSIF l_list_type = 'email' THEN -- block_lists of type email should be matched exactly (ignoring case).
		RETURN (SELECT value FROM blocked_items WHERE list_id = i_list_id AND lower(i_value) = lower(value) LIMIT 1);
	ELSE -- i_whole_word = 1, general lists and stuff like that
		RETURN (SELECT value FROM blocked_items WHERE list_id = i_list_id AND i_value ~* ('[[:<:]]' || regexp_replace(value, E'([\\^\\$\\\\\\(\\)\\{\\}\\[\\]\\*\\+\\?])', E'\\\\\\1', 'g') || '[[:>:]]') LIMIT 1);
	END IF;
END;
$$ STABLE LANGUAGE plpgsql;
