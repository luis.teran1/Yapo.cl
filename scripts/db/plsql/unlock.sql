CREATE OR REPLACE FUNCTION unlock(
	i_ad_id ads.ad_id%TYPE,
	i_action_id ad_actions.action_id%TYPE,
	i_remote_addr action_states.remote_addr%TYPE,
	i_token_id action_states.token_id%TYPE,
	i_new_queue ad_actions.queue%TYPE
) RETURNS VOID AS $$
DECLARE
	l_transition action_states.transition%TYPE;
	l_state_id action_states.state_id%TYPE;
	l_old_queue ad_actions.queue%TYPE;
	l_old_state action_states.state%TYPE;
BEGIN
	l_transition = 'checkin';
	IF i_new_queue IS NOT NULL THEN
		l_transition = 'newqueue';
	END IF;

	SELECT
		queue,
		state
	INTO
		l_old_queue,
		l_old_state
	FROM
		ad_actions
	WHERE
		ad_actions.ad_id = i_ad_id AND
		ad_actions.action_id = i_action_id
	FOR UPDATE;

        IF NOT FOUND THEN
                RAISE EXCEPTION 'ERROR_NO_SUCH_ACTION';
        END IF;

	IF l_old_state NOT IN ('pending_review', 'locked') THEN
		RAISE EXCEPTION 'ERROR_INVALID_STATE';
	END IF;

	-- Reset locked
	UPDATE
		ad_actions
	SET
		locked_by = NULL,
		locked_until = NULL
	FROM
		action_states
	WHERE
		action_states.ad_id = i_ad_id AND
		action_states.action_id = i_action_id AND
		action_states.state_id = ad_actions.current_state AND
		((i_new_queue IS NOT NULL AND
		  i_new_queue != l_old_queue) OR
		 (action_states.state = 'locked' AND
		  locked_by = get_admin_by_token_id(i_token_id))) AND
		ad_actions.ad_id = i_ad_id AND
		ad_actions.action_id = i_action_id;

	IF NOT FOUND THEN
		RETURN; -- No new state needed
	END IF;

	-- Insert a new state
	l_state_id := insert_state(i_ad_id,
				   i_action_id,
                  		   'pending_review',
				   l_transition,
				   i_remote_addr,
				   i_token_id);

	-- Change queue
	IF i_new_queue IS NOT NULL THEN
		UPDATE 
			ad_actions
		SET
			queue = i_new_queue
		WHERE 
			ad_id = i_ad_id AND
			action_id = i_action_id;

		-- Add queue to state param
		INSERT INTO
			state_params
		VALUES (
			i_ad_id,
			i_action_id,
			l_state_id,
			'queue',
			i_new_queue);
	END IF;

	-- Unlock the action in ad_queues
	UPDATE
		ad_queues
	SET
		locked_by = NULL,
		locked_until = NULL,
		queue = COALESCE(i_new_queue, queue)
	WHERE
		ad_id = i_ad_id AND
		action_id = i_action_id;
END;
$$ LANGUAGE plpgsql;
