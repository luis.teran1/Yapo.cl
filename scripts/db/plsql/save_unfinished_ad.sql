CREATE OR REPLACE FUNCTION save_unfinished_ad (	i_unf_ad_id	unfinished_ads.unf_ad_id%TYPE,
						i_session_id	unfinished_ads.session_id%TYPE,
						i_email		unfinished_ads.email%TYPE,
						i_data		unfinished_ads.data%TYPE,
						OUT o_unf_ad_id	unfinished_ads.unf_ad_id%TYPE
						) AS $$
DECLARE
	l_session_id		unfinished_ads.session_id%TYPE;
	l_email			unfinished_ads.email%TYPE;
BEGIN

	-- Getting unfinished ad id, session id and email of the unfinished ad (if it comes)
	IF i_unf_ad_id IS NOT NULL
	THEN
		SELECT
			unf_ad_id,
			session_id,
			email
		INTO
			o_unf_ad_id,
			l_session_id,
			l_email
		FROM
			unfinished_ads
		WHERE
			unf_ad_id = i_unf_ad_id;
	END IF;

	-- If these exists, we must update them, otherwise we insert a new one
	IF l_session_id IS NULL
	THEN
		INSERT INTO unfinished_ads (
			session_id,
			email,
			added,
			modified_at,
			status,
			data
		)
		VALUES
		(
			i_session_id,
			i_email,
			NOW(),
			NOW(),
			'unpublished',
			i_data
		);

		-- Getting the just-inserted unfinished ad id
		SELECT currval('unfinished_ads_unf_ad_id_seq') INTO o_unf_ad_id;

	ELSE
		UPDATE
			unfinished_ads
		SET
			email = i_email,
			modified_at = NOW(),
			data = i_data
		WHERE
			unf_ad_id = i_unf_ad_id;

		o_unf_ad_id := i_unf_ad_id;
	END IF;

END;
$$ LANGUAGE plpgsql;


