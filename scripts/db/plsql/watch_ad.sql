CREATE OR REPLACE FUNCTION watch_ad(i_watch_unique_id watch_users.watch_unique_id%TYPE,
				    i_list_id integer[],
				    i_max_saved_ads int,
				    OUT o_watch_unique_id watch_users.watch_unique_id%TYPE,
				    OUT o_watch_user_id watch_users.watch_user_id%TYPE,
				    OUT o_list_id integer[]) AS $$
DECLARE
	l_list_id integer[];
	l_i integer;
	l_num_saved_ads integer;
BEGIN
	-- Filter: save only active ads, also as a bonus filter duplicates in the i_list_id array
	SELECT
		ARRAY(
			SELECT
				list_id
			FROM
				ads
			WHERE
				list_id = ANY (i_list_id)
				AND status = 'active'
		)
	INTO
		l_list_id;

	IF array_dims(l_list_id) IS NULL THEN
		RAISE EXCEPTION 'FACTORY_TRANSLATE:list_id:ERROR_WATCH_AD_NOT_ACTIVE';
	END IF;

	o_list_id := l_list_id;


	o_watch_user_id := get_watch_user_id (i_watch_unique_id);
	-- User admin
	IF o_watch_user_id IS NOT NULL THEN
		o_watch_unique_id := i_watch_unique_id;
	ELSE
		-- Create new user
		SELECT
			create_watch_user.o_watch_user_id,
			create_watch_user.o_watch_unique_id
		INTO
			o_watch_user_id,
			o_watch_unique_id
		FROM
			create_watch_user();
	END IF;


	-- Check max num queries
	SELECT
		COUNT(1)
	INTO
		l_num_saved_ads
	FROM
		watch_ads
	WHERE
		watch_user_id = o_watch_user_id;
	IF l_num_saved_ads >= i_max_saved_ads THEN
		RAISE EXCEPTION 'FACTORY_TRANSLATE:error:ERROR_MAX_WATCH_ADS_REACHED';
	END IF;
	
	-- Filter: save only new save ads
	SELECT
		ARRAY(
		      SELECT
		      	o_list_id[generate_series]
		      FROM
		      	generate_series (array_lower (o_list_id, 1), array_upper (o_list_id, 1))
		      WHERE
		      	NOT EXISTS (
				SELECT
					list_id
				FROM
					watch_ads
				WHERE
					watch_user_id = o_watch_user_id
					AND list_id = o_list_id[generate_series]
				)
		)
	INTO
		l_list_id;

	o_list_id := l_list_id;

	IF array_dims(o_list_id) IS NOT NULL THEN
		BEGIN
			INSERT INTO
				watch_ads
				(watch_user_id,
				 list_id)
			SELECT
				o_watch_user_id,
				o_list_id[generate_series]
			FROM
				generate_series (array_lower (o_list_id, 1), array_upper (o_list_id, 1));
	        EXCEPTION WHEN UNIQUE_VIOLATION THEN
			-- do nothing
		END;
	END IF;
END
$$ LANGUAGE plpgsql;
