CREATE OR REPLACE FUNCTION get_user_ids_by_partner(i_partner VARCHAR) RETURNS SETOF INTEGER AS $$

BEGIN
	RETURN QUERY SELECT
		DISTINCT(users.user_id)
	FROM
		ad_params
	JOIN ads USING (ad_id)
	JOIN users USING (user_id)
	WHERE
		ad_params.name = 'link_type' AND ad_params.value = i_partner
	;
END;
$$ LANGUAGE plpgsql;
