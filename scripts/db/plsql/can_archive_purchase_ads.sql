-- can_archive_purchase_ads verifies whether every ad from the purchase can be archived.
--
-- Example:
-- Array ads = {1, 2, 3}
--	CASE FALSE:
--		Array can_be_archived = {1, 2}
--		len(ads) != len(can_be_archived)
--	CASE TRUE:
--		Array can_be_archived = {1, 2, 3}
--		len(ads) == len(can_be_archived)
CREATE OR REPLACE FUNCTION can_archive_purchase_ads(i_ad_ids integer[], config_time integer[]) RETURNS TABLE(resp boolean) AS $$
DECLARE
	time_refused integer = config_time[1];
	time_unpaid integer = config_time[2];
	time_deleted integer = config_time[3];
	time_postpone_archive integer = config_time[4];
BEGIN
        RETURN QUERY
        SELECT DISTINCT
			-- Cast to INT because count() return BIGINT
			CAST(count(ad_id) AS int) = array_length(i_ad_ids, 1)
        FROM
                action_states
        WHERE
                state_id IN (
                        SELECT
                                max(state_id)
                        FROM
                                action_states
                        WHERE
                                ad_id = ANY(i_ad_ids)
                        AND
                                transition != 'reminder'
                        GROUP BY ad_id)
                AND CASE
                        WHEN transition = 'refuse' THEN
				-- select if status is not active and is less than or equal to to bconf_time
				(SELECT status 
				FROM ads 
				WHERE ad_id = action_states.ad_id) != 'active'
	                        AND timestamp <= CURRENT_TIMESTAMP - (interval '1 day' * time_refused
				)
                        WHEN transition IN ('paymail', 'verifymail', 'paymailsent', 'pending_pay') THEN
				-- if status is inactive, deleted or refused and is less than or equal to to bconf_time
				(SELECT status
				FROM ads 
				WHERE ad_id = action_states.ad_id) IN ('inactive', 'deleted', 'refused')
                                AND timestamp <= CURRENT_TIMESTAMP - (interval '1 day' * time_unpaid)
                        WHEN state = 'deleted' OR transition = 'store_change' THEN
				 -- if status is deleted and is less than or equal to to bconf_time
				(SELECT status FROM ads WHERE ad_id = action_states.ad_id) = 'deleted'
                                AND timestamp <= CURRENT_TIMESTAMP - (interval '1 day' * time_deleted)
                        WHEN state = 'postpone_archive' THEN
				-- if less than or equal to to bconf_time
                                timestamp <= CURRENT_TIMESTAMP - (interval '1 day' * time_postpone_archive)
                        ELSE
                                false
                END
        AND
        action_states.ad_id = ANY(i_ad_ids);
END;
$$ LANGUAGE plpgsql;
