CREATE OR REPLACE FUNCTION find_others_pack_types(
	i_type varchar,
	i_pack_conf varchar[][],
	OUT o_pack_type varchar[]
) AS $$
DECLARE
	l_pack_conf_array varchar[];
BEGIN
	FOREACH l_pack_conf_array SLICE 1 IN ARRAY i_pack_conf LOOP
		IF i_type != l_pack_conf_array[1] THEN
			o_pack_type := array_append(o_pack_type, l_pack_conf_array[1]);
		END IF;
	END LOOP;
END;
$$ LANGUAGE plpgsql;

