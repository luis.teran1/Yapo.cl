CREATE OR REPLACE FUNCTION has_new_video(i_ad_id ad_actions.ad_id%TYPE,
				     i_action_id ad_actions.action_id%TYPE) RETURNS BOOL AS $$
DECLARE
	l_action_type	ad_actions.action_type%TYPE;
	l_action_id 	ad_actions.action_id%TYPE;
BEGIN
	-- Current action_id
	l_action_id := i_action_id;

	-- Current action_type
	SELECT
		action_type
	INTO
		l_action_type
	FROM
		ad_actions
	WHERE
		ad_id = i_ad_id AND
		action_id = i_action_id;

	IF NOT FOUND THEN
		RETURN false;
	END IF;

	-- If the ad has been refused earlier check the action before refuse for new videos
	IF l_action_type IN ('editrefused') THEN
		IF EXISTS(SELECT * FROM ad_changes WHERE ad_id = i_ad_id AND action_id = i_action_id AND is_param AND column_name = 'video_name' AND (new_value IS NULL OR new_value = '')) THEN
			RETURN false;
		END IF;
		SELECT
			o_action_type,
			o_action_id
		INTO
			l_action_type,
			l_action_id
		FROM
			action_type_before_refuse(i_ad_id, i_action_id, NULL);
	END IF;

	-- Check for new video
	IF l_action_type IN ('edit', 'renew', 'prolong', 'video') THEN
	 	IF EXISTS (SELECT * FROM action_params WHERE ad_id = i_ad_id AND action_id = l_action_id AND name = 'video_new') THEN
			RETURN true; 
		END IF;
	ELSIF l_action_type IN ('new') THEN
		IF EXISTS (SELECT ad_media_id FROM ad_media WHERE media_type = 'video' AND ad_id = i_ad_id) THEN
			RETURN true;
		END IF;
	END IF;

	RETURN false;
END;
$$ LANGUAGE plpgsql STABLE;
