CREATE OR REPLACE FUNCTION get_user_counters(i_email varchar,
					i_asearch_ids		integer[],
					OUT o_inactive_ads      integer,
					OUT o_active_ads	integer,
					OUT o_total          	integer
) AS $$
BEGIN
	IF i_asearch_ids is null then
		RAISE EXCEPTION 'ERROR_MISSING_AD_LIST';
	END IF;
	-- Get total number of ads for user
	SELECT
		COUNT(1)
	INTO
		o_total
	FROM
		ads INNER JOIN users USING(user_id)
                        INNER JOIN ad_actions USING(ad_id)
	WHERE
		users.email = lower(i_email)
                and ad_actions.action_id = (select max(action_id) from ad_actions where ad_id = ads.ad_id )
		AND (
			ads.status IN ('active', 'inactive')
			OR (ads.status = 'refused' and ad_actions.action_type = 'editrefused' and ad_actions.state != 'refused' )
			OR ( ads.list_id = ANY (i_asearch_ids))
		)
	;

	-- Get total number of ads being reviewed
	SELECT
		COUNT(1)
	INTO
		o_inactive_ads
	FROM
		ads INNER JOIN users USING(user_id)
                        INNER JOIN ad_actions USING(ad_id)
	WHERE
		users.email = lower(i_email)
                and ad_actions.action_id = (select max(action_id) from ad_actions where ad_id = ads.ad_id )
		AND (
			 (ads.status = 'active' AND not(ads.list_id = ANY(i_asearch_ids)))
			OR (ads.status = 'hidden' and not(ads.list_id = ANY(i_asearch_ids)))
			OR ads.status = 'inactive'
			OR (ads.status = 'refused' and ad_actions.action_type = 'editrefused' 
				and ad_actions.state != 'refused' 
				and not(ads.list_id = ANY(i_asearch_ids)))
			);

	o_active_ads := COALESCE(o_total, 0) - COALESCE(o_inactive_ads, 0);
	
END;
$$ LANGUAGE plpgsql;
