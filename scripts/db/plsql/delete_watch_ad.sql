CREATE OR REPLACE FUNCTION delete_watch_ad(i_watch_unique_id watch_users.watch_unique_id%TYPE,
					   i_list_id ads.list_id%TYPE,
					   OUT o_watch_user_id watch_users.watch_user_id%TYPE) AS $$
BEGIN
	-- Get watch user id
	SELECT
		watch_user_id
	INTO
		o_watch_user_id
	FROM
		watch_users
	WHERE
		watch_unique_id = i_watch_unique_id;
	
	IF NOT FOUND THEN
		RAISE EXCEPTION 'FACTORY_TRANSLATE:watch_unique_id:ERROR_WATCH_UNIQUE_ID_MISSING';
	END IF;

	-- Delete list_id from watch_ads
	DELETE FROM
		watch_ads
	WHERE
		watch_user_id = o_watch_user_id
		AND list_id = i_list_id;
	
	-- Auto garb watch users that don't have any queries or saved ads
	IF NOT EXISTS (SELECT * FROM watch_queries WHERE watch_user_id = o_watch_user_id)
		AND NOT EXISTS (SELECT * FROM watch_ads WHERE watch_user_id = o_watch_user_id) THEN
		PERFORM delete_watch_user(o_watch_user_id);
		o_watch_user_id := 0;
	END IF;
END
$$ LANGUAGE plpgsql;
