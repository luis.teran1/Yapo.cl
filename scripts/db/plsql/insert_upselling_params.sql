CREATE OR REPLACE FUNCTION insert_upselling_params(
	i_ad_id ads.ad_id%TYPE,
	i_action_id ad_actions.action_id%TYPE,
	i_counter integer,
	i_is_daily_bump integer,
	i_is_from_nga integer,
	OUT o_status integer,
	OUT o_action_id integer
) AS $$

DECLARE
    l_action_id ad_actions.action_id%TYPE;
    l_action_type varchar;
	l_action_related varchar;
	l_ad_param_present enum_ad_params_name;
	l_value varchar;
    l_state_id action_states.state_id%TYPE;
BEGIN

	o_status := -1;

	IF i_is_daily_bump IS NOT NULL THEN
		IF i_is_daily_bump = 0 THEN
			RAISE NOTICE 'Upselling bump';
			l_action_type := 'upselling_bump';
			l_action_related := 'bump';
		ELSE
			RAISE NOTICE 'Upselling daily_bump';
			l_action_type := 'upselling_daily_bump';
			l_action_related := 'daily_bump';
		END IF;
	ELSE
		RAISE NOTICE 'Upselling weekly_bump';
		l_action_type := 'upselling_weekly_bump';
		l_action_related := 'weekly_bump';
	END IF;

	IF NOT EXISTS(SELECT * FROM ads WHERE ad_id = i_ad_id AND status IN ('active', 'disabled', 'inactive')) THEN
		RAISE EXCEPTION 'FACTORY_TRANSLATE:ad_id:ERROR_AD_NOT_ACTIVE';
	END IF;

	-- Check if the product already have the product
	IF i_is_daily_bump IS NULL OR i_is_daily_bump = 1 THEN
		SELECT
			name, value INTO l_ad_param_present, l_value
		FROM ad_params
		WHERE
			ad_id = i_ad_id
			AND ( name = l_action_type::enum_ad_params_name OR name = l_action_related::enum_ad_params_name );

		IF FOUND AND (CAST(l_value AS integer) <= CAST(i_counter AS integer)) THEN
			RAISE EXCEPTION 'FACTORY_TRANSLATE:ad_id:ERROR_%_ALREADY_EXISTS_FOR_AD',UPPER(l_ad_param_present::varchar);
		END IF;
	END IF;

	IF i_is_from_nga IS NOT NULL THEN
		-- Create new ad_action
		l_action_id := insert_ad_action(i_ad_id, l_action_type::enum_ad_actions_action_type, NULL);
		PERFORM insert_state(i_ad_id,l_action_id,'reg','initial','127.0.0.1',NULL);
		PERFORM insert_state(i_ad_id,l_action_id,'unpaid','pending_pay','127.0.0.1',NULL);
		l_state_id = insert_state(i_ad_id,l_action_id,'accepted','pay','127.0.0.1',NULL);
	ELSE
		-- check for action of related product
		IF i_action_id IS NOT NULL THEN
			SELECT action_id, current_state INTO l_action_id, l_state_id FROM ad_actions WHERE ad_id = i_ad_id AND action_id = i_action_id AND state = 'accepted';
		ELSE
			SELECT MAX(action_id), MAX(current_state) INTO l_action_id, l_state_id FROM ad_actions WHERE ad_id = i_ad_id AND action_type = l_action_type::enum_ad_actions_action_type AND state = 'accepted';
		END IF;
	END IF;

	IF l_action_id IS NULL THEN
		RAISE EXCEPTION 'FACTORY_TRANSLATE:action_id:ERROR_ACTION_NOT_FOUND';
	END IF;

	/* Only for upselling daily and weekly not for upselling_bump when is_daily_bump = 0 */
	IF i_is_daily_bump IS NULL OR i_is_daily_bump = 1 THEN
		-- add addition of gallery ad_params to ad_changes
		PERFORM insert_ad_change(i_ad_id, l_action_id, l_state_id, true, l_action_type, i_counter::varchar);

		-- apply ad_changes
		PERFORM apply_ad_changes(i_ad_id, l_action_id, NULL);
	END IF;

	o_status := 0;
	o_action_id := l_action_id;

END
$$ LANGUAGE plpgsql;
