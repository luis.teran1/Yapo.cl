CREATE OR REPLACE FUNCTION get_account_passwd(i_email users.email%TYPE,
				OUT o_password        	accounts.salted_passwd%TYPE,
				OUT o_account_id       	accounts.account_id%TYPE,
				OUT o_account_status    accounts.status%TYPE
) AS $$
DECLARE
	l_password accounts.salted_passwd%TYPE;
	l_status accounts.status%TYPE;
	l_account_id accounts.account_id%TYPE;
BEGIN
	SELECT
		salted_passwd,
		status,
		account_id
	INTO
		l_password,
		l_status,
		l_account_id
	FROM
		accounts
	WHERE
		email= i_email;

	IF NOT FOUND OR l_status = 'pending_confirmation' OR l_status = 'inactive' THEN
		o_password = '-1';
		o_account_id = '-1';
	ELSE
		o_password = l_password;
		o_account_id = l_account_id;
	END IF;
	o_account_status = l_status;

END;
$$ LANGUAGE plpgsql;
