CREATE OR REPLACE FUNCTION view_ad(i_schema text,
				   i_ad_id action_states.ad_id%TYPE,
				   i_action_id action_states.action_id%TYPE,
				   i_state_id action_states.state_id%TYPE) RETURNS SETOF v_ads AS $$
DECLARE
	l_ad v_ads;
	l_rec RECORD;
	l_param_name text[];
	l_param_value text[];
	l_num_params integer;
	l_i integer;
	l_j integer;
	l_query varchar;
BEGIN
	-- The output of this query should have the same parameters of the view v_ads
	l_query := 'SELECT
			ad_id,
			list_id,
			list_time,
			status::varchar,
			type::varchar,
			name,
			phone,
			region,
			city,
			category,
			user_id,
			salted_passwd,
			phone_hidden,
			no_salesmen,
			company_ad,
			subject,
			body,
			price,
			infopage,
			infopage_title,
			store_id
		FROM ' || i_schema || '.ads
		WHERE ad_id = ' || i_ad_id;

	FOR
		l_ad
	IN
		EXECUTE l_query
	LOOP
		-- Fetch ad params
		l_num_params := 0;
		FOR
			l_rec
		IN
			EXECUTE
				'SELECT
					name::varchar,
					value
				FROM
					' || i_schema || '.ad_params
				WHERE
					ad_id = ' || i_ad_id
		LOOP
			l_param_name[l_num_params] := l_rec.name;
			l_param_value[l_num_params] := l_rec.value;
			l_num_params := l_num_params + 1;
		END LOOP;

		-- Apply ad_changes 
		FOR
			l_rec
		IN
			EXECUTE 'SELECT
					*
				FROM
					' || i_schema || '.ad_changes
				WHERE
					ad_id = ' || i_ad_id || '
				ORDER BY
					state_id DESC'
		LOOP
			EXIT WHEN i_state_id > 0 AND l_rec.state_id <= i_state_id;
			EXIT WHEN l_rec.action_id <= i_action_id;

			IF l_rec.column_name = 'name' THEN
				l_ad.name := l_rec.old_value;
			ELSIF l_rec.column_name = 'phone' THEN
				l_ad.phone := l_rec.old_value;
			ELSIF l_rec.column_name = 'region' THEN
				l_ad.region := l_rec.old_value;
			ELSIF l_rec.column_name = 'city' THEN
				l_ad.city := l_rec.old_value;
			ELSIF l_rec.column_name = 'type' THEN
				l_ad.type := l_rec.old_value;
			ELSIF l_rec.column_name = 'category' THEN
				l_ad.category := l_rec.old_value;
			ELSIF l_rec.column_name = 'subject' THEN
				l_ad.subject := l_rec.old_value;
			ELSIF l_rec.column_name = 'body' THEN
				l_ad.body := l_rec.old_value;
			ELSIF l_rec.column_name = 'price' THEN
				l_ad.price := l_rec.old_value;
			ELSIF l_rec.column_name = 'infopage' THEN
				l_ad.infopage := l_rec.old_value;
			ELSIF l_rec.column_name = 'infopage_title' THEN
				l_ad.infopage_title := l_rec.old_value;
			ELSIF l_rec.column_name = 'phone_hidden' THEN
				l_ad.phone_hidden := l_rec.old_value;
			ELSIF l_rec.column_name = 'no_salesmen' THEN
				l_ad.no_salesmen := l_rec.old_value;
			ELSIF l_rec.column_name = 'company_ad' THEN
				l_ad.company_ad := l_rec.old_value;
			ELSIF l_rec.column_name = 'user_id' THEN
				l_ad.user_id := l_rec.old_value;
			ELSIF l_rec.column_name = 'store_id' THEN
				l_ad.store_id := l_rec.old_value;
			END IF;
		END LOOP;

		RETURN NEXT l_ad;
	END LOOP;
END
$$ LANGUAGE plpgsql;
