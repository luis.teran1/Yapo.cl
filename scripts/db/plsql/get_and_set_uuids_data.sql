CREATE OR REPLACE FUNCTION get_and_set_uuids_data(i_sender_email VARCHAR,
	i_recipient_email VARCHAR,
	i_sender_name VARCHAR,
	i_list_id ads.list_id%TYPE,
	o_sen_uuid OUT UUID,
	o_sen_name OUT VARCHAR,
	o_rec_uuid OUT UUID,
	o_rec_name OUT VARCHAR) AS $$
BEGIN

	SELECT
		o_uuid INTO o_sen_uuid
	FROM
		insert_or_get_uuid(i_sender_email);
	
	SELECT
		o_uuid INTO o_rec_uuid
	FROM
		insert_or_get_uuid(i_recipient_email);

	SELECT name INTO o_sen_name FROM  email_uuid WHERE email = i_sender_email;
	IF o_sen_name = '' THEN
		UPDATE
			email_uuid
		SET
			name = i_sender_name
		WHERE
			email = i_sender_email
		RETURNING name INTO o_sen_name;
	END IF;
	
	SELECT name INTO o_rec_name FROM ads where list_id = i_list_id;
	
END;
$$
LANGUAGE plpgsql;

