CREATE OR REPLACE FUNCTION add_purchase_param(
	i_payment_group_id  purchase.payment_group_id%TYPE,
	i_param_name purchase_params.name%TYPE,
	i_param_value purchase_params.value%TYPE,
	OUT o_purchase_id integer) AS $$
BEGIN
	SELECT
		purchase_id
	INTO
		o_purchase_id
	FROM
		purchase
	WHERE
		payment_group_id = i_payment_group_id;

	IF NOT FOUND THEN
		o_purchase_id := 0;
		RETURN;
	END IF;

	PERFORM
		value
	FROM
		purchase_params
	WHERE
		purchase_id = o_purchase_id AND name = i_param_name;

	IF NOT FOUND THEN
		INSERT INTO
			purchase_params (purchase_id, name, value)
		VALUES (o_purchase_id,
			i_param_name,
			i_param_value);
	ELSE
		UPDATE purchase_params
		SET	value = i_param_value
		WHERE purchase_id = o_purchase_id and name = i_param_name;
	END IF;
END;
$$ LANGUAGE plpgsql;
