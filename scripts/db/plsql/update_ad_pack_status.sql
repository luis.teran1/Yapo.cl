CREATE OR REPLACE FUNCTION update_ad_pack_status(
	i_ad_id ad_actions.ad_id%TYPE,
	i_action_id ad_actions.action_id%TYPE,
	i_user_id ads.user_id%TYPE,
	OUT o_pack_status varchar
) AS $$
DECLARE
	l_account_id accounts.account_id%TYPE;
	l_pack_result integer;
BEGIN
	RAISE NOTICE 'start get_ad_pack_status for ad % with action %', i_ad_id, i_action_id;
	-- Check if the ad have the ad_param pack_status
	SELECT
		value
	INTO
		o_pack_status
	FROM
		ad_params
	WHERE
		ad_id = i_ad_id AND name = 'pack_status';

	IF NOT FOUND THEN
		o_pack_status := 'active';
	ELSE
		IF o_pack_status = 'active'::varchar THEN
			SELECT
				account_id
			INTO
				l_account_id
			FROM
				accounts
			WHERE
				user_id = i_user_id;

			IF NOT FOUND OR NOT EXISTS (
				SELECT
					*
				FROM
					packs
				WHERE
					account_id = l_account_id
					AND status = 'active'
			) THEN
				o_pack_status := 'disabled';

				UPDATE
					ad_params
				SET
					value = 'disabled'
				WHERE
					ad_id = i_ad_id
					AND name = 'pack_status';

				UPDATE
					ad_changes
				SET
					new_value = 'disabled'
				WHERE
					ad_id = i_ad_id
					AND action_id = i_action_id
					AND column_name = 'pack_status'
					AND new_value = 'active';
            END IF;
        ELSE
            IF EXISTS (
				SELECT
					*
				FROM
					ad_params
				WHERE
					ad_id = i_ad_id
					AND name = 'inserting_fee'
					AND value = '1'
			) THEN
                DELETE
				FROM ad_params
				WHERE
					ad_id = i_ad_id
					AND name = 'pack_status';

				o_pack_status := 'active';
            END IF;
        END IF;
    END IF;
END
$$ LANGUAGE plpgsql;
