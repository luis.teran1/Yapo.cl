CREATE OR REPLACE FUNCTION set_watch_query_sms_status(i_watch_unique_id watch_users.watch_unique_id%TYPE,
		i_watch_query_id watch_queries.watch_query_id%TYPE,
		i_sms_status watch_queries.sms_status%TYPE) RETURNS VOID AS $$
DECLARE
	l_watch_user_id	watch_queries.watch_user_id%TYPE;
BEGIN
	SELECT
		watch_user_id
	INTO
		l_watch_user_id
	FROM
		watch_users
	WHERE
		watch_unique_id = i_watch_unique_id;

	IF NOT FOUND THEN
		RAISE EXCEPTION 'FACTORY_TRANSLATE:error:ERROR_WATCH_USER_NOT_FOUND';
	END IF;
	
	UPDATE
		watch_queries
	SET
		sms_status = i_sms_status
	WHERE
		watch_query_id = i_watch_query_id AND
		watch_user_id = l_watch_user_id;

	IF NOT FOUND THEN
		RAISE EXCEPTION 'FACTORY_TRANSLATE:error:ERROR_WATCH_QUERY_NOT_FOUND';
	END IF;
END;
$$ LANGUAGE plpgsql;
