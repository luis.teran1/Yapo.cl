CREATE OR REPLACE FUNCTION get_user_by_email(i_email users.email%TYPE, 
					     OUT o_account users.account%TYPE, 
					     OUT o_user_id ads.user_id%TYPE, 
					     OUT o_uid users.uid%TYPE) AS $$
DECLARE
	l_email users.email%TYPE;
	l_paid_total users.paid_total%TYPE;
BEGIN
	l_email = lower(i_email);
	
	SELECT
		account, user_id, uid
	INTO
		o_account, o_user_id, o_uid
	FROM
		users
	WHERE
		email = l_email;
END;
$$ LANGUAGE plpgsql;
