CREATE OR REPLACE FUNCTION insert_ad_action_state(i_ad_id ad_actions.ad_id%TYPE,
		 			    i_action_type ad_actions.action_type%TYPE,
					    i_state action_states.state%TYPE,
					    i_transition action_states.transition%TYPE,
					    i_remote_addr action_states.remote_addr%TYPE,
					    i_token_id tokens.token_id%TYPE,
					    OUT o_action_id ad_actions.action_id%TYPE,
					    OUT o_state_id action_states.state_id%TYPE) AS $$
BEGIN
	o_action_id := insert_ad_action(i_ad_id, i_action_type, NULL);

	-- Create state
 	o_state_id := insert_state(i_ad_id, 
                             o_action_id, 
                             i_state, 
                             i_transition, 
                             i_remote_addr, 
                             i_token_id);
END;
$$ LANGUAGE plpgsql;
