CREATE OR REPLACE FUNCTION clear_ad_action(i_ad_id ads.ad_id%TYPE, 
					i_action_id ad_actions.action_id%TYPE,
					i_code payment_groups.code%TYPE,
					i_order_id varchar,
					i_queue ad_actions.queue%TYPE,
					i_transition action_states.transition%TYPE,
					i_status payment_groups.status%TYPE,
					i_amount pay_log.amount%TYPE,
					i_clear_status pay_log.status%TYPE,
					i_ref_types enum_pay_log_references_ref_type[],
					i_references varchar[],
					i_pay_time pay_log.paid_at%TYPE,
					i_remote_addr action_states.remote_addr%TYPE,
					i_token tokens.token%TYPE,
					i_store_id stores.store_id%TYPE,
					i_gallery_expire_minutes integer,
					i_clear_expire_days integer,
					i_pack_conf varchar[][],
					i_ad_prods_wo_bump varchar[],
					OUT o_subject ads.subject%TYPE,
					OUT o_name ads.name%TYPE,
					OUT o_region ads.region%TYPE,
					OUT o_status integer,
					OUT o_sum_price payments.pay_amount%TYPE,
					OUT o_new_subject ad_changes.new_value%TYPE,
					OUT o_email users.email%TYPE,
					OUT o_redir_code redir_stats.code%TYPE,
					OUT o_remote_addr action_states.remote_addr%TYPE,
					OUT o_ad_id ad_actions.ad_id%TYPE,
					OUT o_action_id ad_actions.action_id%TYPE,
					OUT o_action_type ad_actions.action_type%TYPE,
					OUT o_ref_type enum_pay_log_references_ref_type[],
					OUT o_reference varchar[],
					OUT o_pack_result integer
				) AS $$
DECLARE
	l_status payment_groups.status%TYPE;
	l_payment_type payments.payment_type%TYPE;
	l_current_state action_states.state_id%TYPE;
	l_price payments.pay_amount%TYPE;
	l_user_id ads.user_id%TYPE;
	l_payment_group_id payment_groups.payment_group_id%TYPE;
	l_code pay_log.code%TYPE;
	l_cat_price payments.pay_amount%TYPE;
	l_pay_type pay_log.pay_type%TYPE;
	l_clear_status pay_log.status%TYPE;
	l_amount pay_log.amount%TYPE;
	l_should_review bool;
	l_should_accept bool;
	l_accept_state_id action_states.state_id%TYPE;
	l_category ads.category%TYPE;
	l_type ads.type%TYPE;
	l_company_ad ads.company_ad%TYPE;
	l_payment_rec record;
BEGIN

	-- Get payment info
	IF i_ad_id IS NOT NULL AND i_action_id IS NOT NULL THEN
		-- Lock to avoid double clear.
		PERFORM
			1
		FROM
			ad_actions
		WHERE
			ad_id = i_ad_id AND
			action_id = i_action_id
		FOR UPDATE;

		l_user_id = get_user_id_by_ad_id(i_ad_id);

		SELECT
			SUM(pay_amount - discount),
			SUM(CASE payment_type WHEN 'ad_action' THEN pay_amount - discount ELSE 0 END),
			action_type,
			payment_group_id,
			payment_groups.code,
			ad_actions.current_state
		INTO
			o_sum_price,
			l_cat_price,
			o_action_type,
			l_payment_group_id,
			l_code,
			l_current_state
		FROM
			ad_actions
			JOIN payment_groups USING (payment_group_id)
			JOIN payments USING (payment_group_id)
		WHERE
			state in ('unpaid', 'unverified') AND
			payment_groups.status in ('unpaid', 'unverified') AND
			ad_id = i_ad_id AND
			action_id = i_action_id
		GROUP BY
			action_type,
			payment_group_id,
			payment_groups.code,
			ad_actions.current_state;

		IF NOT FOUND THEN
			RAISE EXCEPTION 'No payment for ad: %, action: %', i_ad_id, i_action_id;
		END IF;
		
		o_ad_id := i_ad_id;
		o_action_id := i_action_id;

	ELSIF i_code IS NOT NULL THEN
		
		-- Lock to avoid double clear.
		SELECT
			payment_group_id
		INTO
			l_payment_group_id
		FROM
			payment_groups
		WHERE
			code = i_code AND
			status IN ('unpaid', 'unverified')
		ORDER BY
			payment_group_id DESC -- XXX Should check timestamp in action states interval 2 days
		LIMIT 1
		FOR UPDATE;
		
		BEGIN
			IF i_pay_time IS NOT NULL THEN
				l_user_id = get_user_id_by_timed_adcode(i_code, i_pay_time, i_clear_expire_days);
			ELSE
				l_user_id = get_user_id_by_paycode(i_code, i_clear_expire_days);
			END IF;
		EXCEPTION
			WHEN RAISE_EXCEPTION THEN
				o_status := 1;
				IF i_token IS NULL AND i_status = 'paid' THEN
					-- Check if payment already exists
					IF NOT insert_pay_log('payphone', i_code, i_amount, i_pay_time, i_ref_types, i_references, NULL, NULL, NULL, 'ERR') THEN
						o_status := 5;
					END IF;
				END IF;
				RETURN;
		END;
		
		SELECT
			sum(pay_amount - discount),
			sum(CASE payment_type WHEN 'ad_action' THEN pay_amount-discount ELSE 0 END),
			ad_id,
			action_id,
			payment_groups.status,
			action_type,
			ad_actions.current_state
		INTO
			o_sum_price,
			l_cat_price,
			o_ad_id,	
			o_action_id,
			l_status,
			o_action_type,
			l_current_state
		FROM
			payment_groups
			JOIN payments USING (payment_group_id)
			JOIN ad_actions USING (payment_group_id)
			JOIN action_states USING (ad_id, action_id)
		WHERE
			current_state = state_id AND
			ad_actions.state in ('unpaid', 'unverified') AND
			payment_groups.status in ('unpaid', 'unverified') AND
			payment_groups.code = i_code AND
			timestamp > CURRENT_TIMESTAMP - (interval '1 day' * i_clear_expire_days)
		GROUP BY
			ad_id,
			action_id,
			payment_groups.status,
			action_type;

		IF NOT FOUND THEN
			RAISE EXCEPTION 'No payment for code: %', i_code;
		END IF;

	ELSIF i_order_id IS NOT NULL THEN
		l_payment_group_id := substring(i_order_id from '__#"%#"a%' for '#')::integer;

		SELECT
			sum(payments.pay_amount - discount),
			sum(CASE payment_type WHEN 'ad_action' THEN payments.pay_amount - discount ELSE 0 END),
			ad_id,
			action_id,
			payment_groups.status,
			action_type,
			user_id,
			current_state
		INTO
			o_sum_price,
			l_cat_price,
			o_ad_id,	
			o_action_id,
			l_status,
			o_action_type,
			l_user_id,
			l_current_state
		FROM
			payment_groups
			JOIN payments USING (payment_group_id)
			JOIN ad_actions USING (payment_group_id)
			JOIN ads USING (ad_id)
		WHERE
			ad_actions.state in ('unpaid', 'unverified') AND
			payment_groups.status in ('unpaid', 'unverified') AND
			payment_groups.payment_group_id = l_payment_group_id
		GROUP BY
			ad_id,
			action_id,
			payment_groups.status,
			action_type,
			user_id;

		IF NOT FOUND THEN
			-- Check if already paid.
			SELECT
				user_id,
				email,
				current_state
			INTO
				l_user_id,
				o_email,
				l_current_state
			FROM
				ad_actions
				JOIN ads USING (ad_id)
				JOIN users USING (user_id)
			WHERE
				ad_actions.payment_group_id = l_payment_group_id
				AND ad_actions.state IN ('pending_review', 'locked', 'accepted', 'rejected');

			IF NOT FOUND THEN
				l_payment_group_id := NULL;
			END IF;

			IF i_clear_status = 'OK' THEN
				l_clear_status := 'ERR';
				l_amount := i_amount;
			ELSE
				l_clear_status := i_clear_status;
				l_amount := 0;
			END IF;

			IF insert_pay_log ('paycard', i_order_id, l_amount, i_pay_time, i_ref_types, i_references, i_remote_addr, NULL, l_payment_group_id, l_clear_status) THEN
				o_status := 1;
				IF i_clear_status = 'OK' AND l_user_id IS NOT NULL THEN
					UPDATE
						users
					SET
						account = account + i_amount,
						paid_total = paid_total + i_amount
					WHERE
						user_id = l_user_id;
				ELSE
					o_email := NULL;
				END IF;
			ELSE
				o_status := 5;
			END IF;
			RETURN;
		END IF;
	ELSE
		RAISE EXCEPTION 'ad_id, action_id, code or order_id must be supplied';
	END IF;
	
	-- Return subject and email for ad.
	SELECT
		subject,
		email,
		name,
		region
	INTO
		o_subject,
		o_email,
		o_name,
		o_region
	FROM
		ads
		JOIN users USING (user_id)
	WHERE
		ad_id = o_ad_id;

	o_status := 0;
	IF i_token IS NULL THEN
	
		IF i_clear_status IS NOT NULL AND i_clear_status != 'OK' THEN
			-- Zero amount since no payment was made.
			IF insert_pay_log ('paycard', i_order_id, 0, i_pay_time, i_ref_types, i_references, i_remote_addr, NULL, l_payment_group_id, i_clear_status) THEN
				o_status := 1;
			ELSE
				o_status := 5;
			END IF;
			RETURN;
		END IF;

		-- Check the amount
		IF l_status = 'unpaid' THEN
			IF i_code IS NOT NULL THEN
				l_pay_type := 'payphone';
				l_code := i_code;
			ELSE
				l_pay_type := 'paycard';
				l_code := i_order_id;
			END IF;

			IF o_sum_price > i_amount THEN
				IF insert_pay_log (l_pay_type, l_code, i_amount, i_pay_time, i_ref_types, i_references, i_remote_addr, NULL, l_payment_group_id, 'LESS') THEN
					-- Not enough, update user credit and fail
					UPDATE
						users
					SET
						account = account + i_amount,
						paid_total = paid_total + i_amount
					WHERE
						user_id = l_user_id;
					o_status := 2;
				ELSE 	
					o_status := 5;
				END IF;

				RETURN;
			ELSIF o_sum_price < i_amount THEN
				IF insert_pay_log (l_pay_type, l_code, i_amount, i_pay_time, i_ref_types, i_references, i_remote_addr, NULL, l_payment_group_id, 'MORE') THEN
					-- User paid too much, update user credit
					UPDATE
						users
					SET
						account = account + i_amount - o_sum_price
					WHERE
						user_id = l_user_id;
	
					-- RAISE NOTICE 'Payment too large, % > %', i_amount, o_sum_price;
					o_status := 3;

					-- Check for redir
					SELECT
						value
					INTO
						o_redir_code
					FROM
						action_params
					WHERE
						ad_id = o_ad_id AND
						action_id = o_action_id AND
						name = 'redir';

					IF o_action_type = 'inserting_fee' THEN
						PERFORM insert_ad_change(o_ad_id, o_action_id, l_current_state, true, 'inserting_fee','1');
					END IF;
				ELSE
					o_status := 5;
					RETURN;
				END IF;
			ELSE
				IF insert_pay_log (l_pay_type, l_code, i_amount, i_pay_time, i_ref_types, i_references, i_remote_addr, NULL, l_payment_group_id, 'OK') THEN
					o_status := 0;

					-- Check for redir
					SELECT
						value
					INTO
						o_redir_code
					FROM
						action_params
					WHERE
						ad_id = o_ad_id AND
						action_id = o_action_id AND
						name = 'redir';

					IF o_action_type = 'inserting_fee' THEN
						PERFORM insert_ad_change(o_ad_id, o_action_id, l_current_state, true, 'inserting_fee','1');
					END IF;
				ELSE
					o_status := 5;
					RETURN;
				END IF;
			END IF;
		ELSE
			o_status := 4; -- verified

			-- We can not clear a bump without token in state <> unpaid
			IF o_action_type = 'bump' THEN
				RAISE EXCEPTION 'ERROR_PAYMENT_NOT_VALIDATED';
			END IF;
		
			-- Handle campaign ads
			IF i_transition = 'cleared_by' THEN
				l_pay_type := 'campaign';
			ELSIF i_transition = 'campaign_clear' THEN
				l_pay_type := 'campaign';
				IF o_action_type != 'editrefused' THEN
					IF subtract_free_ad(i_store_id) IS NULL THEN
						RAISE EXCEPTION 'ERROR_NO_FREE_ADS';
					END IF;
				END IF;
			ELSE
				l_pay_type := 'verify';

				-- Check for redir
				SELECT
					value
				INTO
					o_redir_code
				FROM
					action_params
				WHERE
					ad_id = o_ad_id AND
					action_id = o_action_id AND
					name = 'redir';

			END IF;

			IF NOT insert_pay_log(l_pay_type, i_code, NULL, i_pay_time, i_ref_types, i_references, i_remote_addr, NULL, l_payment_group_id, 'OK') THEN
				RAISE EXCEPTION 'ERROR_PAY_LOG_DUPLICATE';
			END IF;
		END IF;

		-- Get remote addr on the reg state of current action
		SELECT
			remote_addr
		INTO
			o_remote_addr
		FROM
			action_states
		WHERE
			ad_id = o_ad_id AND
			action_id = o_action_id AND
			state = 'reg';
			
		IF i_amount IS NOT NULL THEN
			UPDATE
				users
			SET
				paid_total = paid_total + i_amount
			WHERE
				user_id = l_user_id;
		END IF;
	ELSE
		IF i_code IS NOT NULL THEN
			l_code := i_code;
		END IF;
		IF NOT insert_pay_log ('adminclear', l_code, NULL, i_pay_time, i_ref_types, i_references, i_remote_addr, get_token_id(i_token), l_payment_group_id, 'OK') THEN
			RAISE EXCEPTION 'ERROR_PAY_LOG_DUPLICATE';
		END IF;
	END IF;

	-- Update the payments table, setting amount and pay_time
	UPDATE
		payment_groups
	SET
		status = i_status
	WHERE
		payment_group_id = l_payment_group_id;

	IF EXISTS( SELECT * FROM unnest(i_ad_prods_wo_bump) WHERE unnest = o_action_type::varchar )THEN
		l_should_review := FALSE;
		l_should_accept := TRUE;
	ELSIF o_action_type = 'bump' THEN
		l_should_review := FALSE;
		l_should_accept := FALSE;
	ELSE
		l_should_review := TRUE;
	END IF;

	IF l_should_review THEN
		o_pack_result := set_pack_result(l_user_id, o_ad_id, o_action_id, o_action_type, l_current_state, i_pack_conf);
		-- the ad must be reviewed
		PERFORM insert_state(o_ad_id,o_action_id,'pending_review',i_transition,i_remote_addr, get_token_id(i_token));
		PERFORM clear_move_action_to_queue(o_ad_id, o_action_id, i_queue);
		PERFORM set_not_newbie_user(l_user_id);
	ELSE
		IF l_should_accept THEN
			l_category := get_changed_value(o_ad_id, o_action_id, false, 'category')::integer;
			l_type := get_changed_value(o_ad_id, o_action_id, false, 'type');
			l_company_ad := get_changed_value(o_ad_id, o_action_id, false, 'company_ad')::boolean;
	
				PERFORM accept_action(o_ad_id, o_action_id, i_transition, i_remote_addr, 
					i_token, i_gallery_expire_minutes, o_action_type, NULL);
		ELSE
				PERFORM insert_state(o_ad_id, 
						o_action_id, 
						'pending_bump', 
						i_transition, 
						i_remote_addr, 
						get_token_id(i_token));
		END IF;
		o_pack_result := -1;
	END IF;

	-- Return the new subject of the ad
	SELECT
		new_value
	INTO
		o_new_subject
	FROM
		ad_changes
	WHERE
		ad_id = o_ad_id AND
		action_id = o_action_id AND
		column_name = 'subject';

	-- Return pay_log_references from previous record if existing
	FOR
		l_payment_rec
	IN
		SELECT
			ref_type,
			reference
		FROM
			pay_log_references JOIN
			pay_log USING (pay_log_id)
		WHERE
			payment_group_id = l_payment_group_id AND
			pay_log.pay_type = 'paycard_save' AND
			pay_log.status = 'SAVE'
	LOOP
		o_ref_type := o_ref_type || l_payment_rec.ref_type;
		o_reference := o_reference || l_payment_rec.reference;
	END LOOP;

END;
$$ LANGUAGE plpgsql;
