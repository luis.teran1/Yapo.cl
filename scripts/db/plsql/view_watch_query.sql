CREATE OR REPLACE FUNCTION view_watch_query(i_watch_unique_id watch_users.watch_unique_id%TYPE,
					    i_watch_query_id watch_queries.watch_query_id%TYPE,
					    OUT o_watch_user_id watch_users.watch_user_id%TYPE,
					    OUT o_last_view watch_queries.last_view%TYPE) AS $$
BEGIN
	o_watch_user_id := get_watch_user_id (i_watch_unique_id);

	IF o_watch_user_id IS NULL THEN
		RAISE EXCEPTION 'FACTORY_TRANSLATE:error:ERROR_NO_SUCH_UNIQUE_ID';
	END IF;

	o_last_view := CURRENT_TIMESTAMP;

	-- Query
	UPDATE
		watch_queries
	SET
		last_view = o_last_view
	WHERE
		watch_user_id = o_watch_user_id
		AND watch_query_id = i_watch_query_id;

	IF NOT FOUND THEN
		RAISE EXCEPTION 'FACTORY_TRANSLATE:watch_unique_id:ERROR_WATCH_QUERY_NOT_FOUND';
	END IF;
END
$$ LANGUAGE plpgsql;
