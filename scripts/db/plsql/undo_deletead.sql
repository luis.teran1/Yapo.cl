CREATE OR REPLACE FUNCTION undo_deletead(
	i_ad_id ads.ad_id%TYPE,
	i_token tokens.token%TYPE,
	i_pack_conf varchar[][],
	OUT o_status integer
) AS $$
DECLARE
	l_status ads.status%TYPE;
	l_action_id ad_actions.action_id%TYPE;
	l_user_id ads.user_id%TYPE;
	l_state_id action_states.state_id%TYPE;
	l_new_status ads.status%TYPE;
	l_pack_result integer;
BEGIN
	o_status := 0;

	-- get ad status
	SELECT
		status,
		user_id
	INTO
		l_status,
		l_user_id
	FROM
		ads
	WHERE
		ad_id = i_ad_id;

	-- return if ad was not found
	IF NOT FOUND THEN
		o_status := 1;
		RETURN;
	END IF;

	-- return if ad status not is 'deleted'
	IF l_status !='deleted' THEN
		o_status := 2;
		RETURN;
	END IF;	

	-- get next action id and insert ad action
	SELECT
		o_action_id
	INTO
		l_action_id
	FROM
		insert_ad_action(i_ad_id, 'undo_delete', NULL);

	-- insert action state
	l_state_id := insert_state(i_ad_id, l_action_id, 'undo_deleted','undo_delete',NULL,get_token_id(i_token));


	SELECT value INTO l_new_status
	FROM ad_params
	WHERE ad_id = i_ad_id AND name = 'pack_status';

	IF NOT FOUND THEN
		l_new_status := 'active';
	ELSE
		l_pack_result := set_pack_result(l_user_id,i_ad_id,l_action_id,'undo_delete',l_state_id,i_pack_conf);

		IF l_pack_result > -1  THEN
			PERFORM apply_ad_changes(i_ad_id, l_action_id, NULL);
		END IF;
		SELECT value INTO l_new_status
		FROM ad_params
		WHERE ad_id = i_ad_id AND name = 'pack_status';
	END IF;

	-- change ad status to active or disabled (if apply pack logic)
	UPDATE
		ads
	SET
		status = l_new_status,
		modified_at = CURRENT_TIMESTAMP
	WHERE
		ad_id = i_ad_id;

END;
$$ LANGUAGE plpgsql;
