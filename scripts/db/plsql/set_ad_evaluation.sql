CREATE OR REPLACE FUNCTION set_ad_evaluation(
	i_ad_id integer,
	i_action_id integer,
	i_ad_evaluation varchar,
	i_reason varchar,
	i_evaluation_queue varchar,
	i_token tokens.token%TYPE,
	i_remote_addr action_states.remote_addr%TYPE
) RETURNS void AS $$
DECLARE
	l_current_state ad_actions.state%TYPE;
BEGIN
		RAISE NOTICE 'BEGIN SET AD EVALUATION';
		-- Verify if ad was deleted
		SELECT
			aa.state
		INTO
			l_current_state
		FROM
			ad_actions AS aa
		JOIN
			action_states AS ae ON ae.ad_id = aa.ad_id AND aa.current_state = ae.state_id
		WHERE
			aa.ad_id = i_ad_id
		ORDER BY
			timestamp DESC
		LIMIT 1;

		IF l_current_state = 'deleted' THEN
			RETURN;
		END IF;

		-- verify if the action alredy has an evaluation
		PERFORM
			*
		FROM
			action_params
		WHERE
			ad_id = i_ad_id
		AND
			action_id = i_action_id
		AND
			name = 'ad_evaluation_result';
		IF FOUND THEN
			RAISE EXCEPTION 'EVALUATION_ALREADY_SET';
		END IF;
		-- Add the action param to the ad action of the ad
		INSERT INTO
			action_params (ad_id, action_id, name, value)
		VALUES
			(i_ad_id, i_action_id, 'ad_evaluation_result', i_ad_evaluation);
		INSERT INTO
			action_params
		VALUES
			(i_ad_id, i_action_id, 'ad_evaluation_reason', i_reason);
		INSERT INTO
			action_params
		VALUES
			(i_ad_id, i_action_id, 'ad_evaluation_timestamp', now()::timestamp);
		-- Check if the provided evaluation_queue is valid. If so, add it to the params
		IF (i_evaluation_queue <> '') THEN
			INSERT INTO
				action_params
			VALUES
				(i_ad_id, i_action_id, 'ad_evaluation_queue', i_evaluation_queue);
		END IF;
END;
$$ LANGUAGE plpgsql;
