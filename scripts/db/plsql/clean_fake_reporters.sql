CREATE OR REPLACE FUNCTION clean_fake_reporters(i_fake_uid users.uid%TYPE,
				i_real_uid users.uid%TYPE,
				i_fake_reporters_email_domain users.email%TYPE) RETURNS VOID AS $$ 
DECLARE
	l_ad_id ads.ad_id%TYPE; 
	l_uid users.uid%TYPE;
	l_user_id users.user_id%TYPE;
	l_reported_user_id users.user_id%TYPE;
	l_email users.email%TYPE;
	l_report_id abuse_reports.report_id%TYPE;
BEGIN
	SELECT
		user_id, uid
	INTO
		l_user_id, l_uid
	FROM
		users
	WHERE
		uid = i_fake_uid AND 
		email like '%@' || i_fake_reporters_email_domain;

	IF FOUND THEN 
                --- Remove fake_reporter_id from abuse_reports table
		UPDATE 
			abuse_reports
		SET 
			user_id = NULL,
			reporter_id = i_real_uid
		WHERE 
			(user_id = l_user_id OR user_id IS NULL) 
			AND reporter_id = i_fake_uid;

		SELECT
                	user_id, uid
	        INTO
        	        l_user_id, l_uid
	        FROM
        	        users
	        WHERE
        	        uid = i_fake_uid AND 
                	email not like '%@' || i_fake_reporters_email_domain;

		IF NOT FOUND THEN
                	UPDATE 
				abuse_reports
			SET 
				user_id = NULL,
				reporter_id = i_real_uid
			WHERE 
				reporter_id = i_fake_uid;

			DELETE FROM 
				abuse_reporters
			WHERE
				uid = i_fake_uid;
		END IF;

		DELETE FROM 
			users
		WHERE
			email like '%@' || i_fake_reporters_email_domain AND 
			uid = i_fake_uid;
	END IF;
END
$$ LANGUAGE plpgsql;
