CREATE OR REPLACE FUNCTION get_jobs_ad_adreply_report(
	i_list_id ads.list_id%TYPE,
	i_account_email accounts.email%TYPE,
	OUT o_added_at mail_queue.added_at%TYPE,
	OUT o_office_name VARCHAR,
	OUT o_subject VARCHAR,
	OUT o_formatted_date VARCHAR,
	OUT o_sender_name mail_queue.sender_name%TYPE,
	OUT o_sender_email mail_queue.sender_email%TYPE,
	OUT o_sender_phone mail_queue.sender_phone%TYPE,
	OUT o_body mail_queue.body%TYPE
) RETURNS SETOF record AS $$
DECLARE
	l_year INTEGER;
	l_last_year_schema VARCHAR;
	l_this_year_schema VARCHAR;
	l_rec record;
	l_query VARCHAR;
BEGIN

	l_year := extract(year from current_date);
	l_last_year_schema := 'blocket_' || (l_year - 1);
	l_this_year_schema := 'blocket_' || l_year;

	l_query := format(
		'SELECT
			mq.added_at,
			ads.subject,
			ads.name,
			to_char(mq.added_at, ''DD/MM/YYYY HH24:MI:SS'') AS formatted_date,
			mq.sender_name,
			mq.sender_email,
			mq.sender_phone,
			mq.body
		FROM
			%s.mail_queue mq
			INNER JOIN ads USING(list_id)
			INNER JOIN accounts USING(user_id)
		WHERE
			ads.list_id = %s
		AND
			accounts.email = ''%s''
		UNION ALL
		SELECT
			mq.added_at,
			ads.subject,
			ads.name,
			to_char(mq.added_at, ''DD/MM/YYYY HH24:MI:SS'') AS formatted_date,
			mq.sender_name,
			mq.sender_email,
			mq.sender_phone, mq.body
		FROM
			%s.mail_queue mq
			INNER JOIN ads USING(list_id)
			INNER JOIN accounts USING(user_id)
		WHERE
			ads.list_id = %s
		AND
			accounts.email = ''%s''
		ORDER
			BY added_at DESC;',
		l_last_year_schema, i_list_id, i_account_email, l_this_year_schema, i_list_id, i_account_email
	);

	FOR
		l_rec
	IN

	EXECUTE
		l_query
	LOOP
		o_added_at := l_rec.added_at;
		o_subject:= l_rec.subject;
		o_office_name := l_rec.name;
		o_formatted_date := l_rec.formatted_date;
		o_sender_name := l_rec.sender_name;
		o_sender_email := l_rec.sender_email;
		o_sender_phone := l_rec.sender_phone;
		o_body := l_rec.body;

		RETURN NEXT;
	END LOOP;

	IF NOT FOUND THEN
		RAISE EXCEPTION 'FACTORY_TRANSLATE:error:JOBS_AR_REPORT_ERROR_NO_CONTACTS';
	END IF;
END
$$ LANGUAGE plpgsql;
