CREATE OR REPLACE FUNCTION insert_account(i_name accounts.name%TYPE,
		i_rut accounts.rut%TYPE,
		i_email accounts.email%TYPE,
		i_phone accounts.phone%TYPE,
		i_is_company accounts.is_company%TYPE,
		i_region accounts.region%TYPE,
		i_salted_passwd accounts.salted_passwd%TYPE,
		i_address accounts.address%TYPE,
		i_contact accounts.contact%TYPE,
		i_lob accounts.lob%TYPE,
		i_commune accounts.commune%TYPE,
		i_uid users.uid%TYPE,
		i_gender accounts.gender%TYPE,
		i_source account_params.value%TYPE,
		i_business_name account_params.value%TYPE,
		i_pack_conf varchar[][],
		OUT o_user_id_ users.user_id%TYPE,
		OUT o_is_new_user integer,
		OUT o_account_id accounts.account_id%TYPE) AS $$
DECLARE
	l_uid users.uid%TYPE;
	l_account_id accounts.account_id%TYPE;
	l_status accounts.status%TYPE;
	l_user_id accounts.user_id%TYPE;
	l_ad record;
	l_action_id ad_actions.action_id%TYPE;
	l_state_id action_states.state_id%TYPE;
	l_account_to_pro integer;
	l_account_action_id account_actions.account_action_id%TYPE;
	l_pro_category varchar;
BEGIN

	o_is_new_user := 0;
	o_user_id_ := NULL;

	SELECT
		account_id,
		user_id,
		status
	INTO
		l_account_id,
		l_user_id,
		l_status
	FROM
		accounts
	WHERE
		lower(i_email) = email;

	IF FOUND THEN 
		IF l_status IN ('inactive') THEN
			UPDATE
				accounts
			SET
				name = i_name,
				rut  = i_rut,
				phone = i_phone,
				is_company = i_is_company,
				region = i_region,
				salted_passwd = i_salted_passwd,
				creation_date =  NOW(),
				status = 'pending_confirmation',
				address = i_address,
				contact = i_contact,
				lob     = i_lob,
				commune = i_commune,
				gender = i_gender

			WHERE
				account_id = l_account_id;
			o_user_id_   = l_user_id;
			o_account_id = l_account_id;
			PERFORM insert_account_action(l_account_id,'creation',NULL);
			RETURN;
		ELSE
			RAISE EXCEPTION 'FACTORY_TRANSLATE:error:ACCOUNT_ALREADY_EXISTS';
		END IF;
	END IF;

	SELECT
		o_user_id
	INTO
		o_user_id_
	FROM
		get_user_by_email(lower(i_email));

	
	IF o_user_id_ IS NULL THEN
	-- not existing user, but has uid (from cookie)
		IF i_uid IS NOT NULL THEN

			SELECT
				uid
			INTO
				l_uid
			FROM
				users
			WHERE
				uid = i_uid;

			-- Don't use fake UIDs
			IF NOT FOUND THEN
				l_uid := NEXTVAL('uid_seq');
			END IF;

		
			INSERT INTO
				users
				(uid,		
				 email)
			VALUES
				(l_uid,
				 lower(i_email));
		-- new user
		ELSE 

			INSERT INTO
				users
				(uid,
				 email)
			VALUES	 
				(NEXTVAL('uid_seq'),
				lower(i_email));


			l_uid := CURRVAL('uid_seq');
		END IF;

	END IF;
	IF o_user_id_ IS NULL THEN
		o_user_id_ := CURRVAL('users_user_id_seq');
		o_is_new_user := 1;
	END IF;
	

	INSERT INTO accounts (
		name,
		rut,
		email,
		phone,
		is_company,
		region,
		salted_passwd,
		creation_date,
		status,
		address,
		contact,
		lob,
		commune,
		gender,
		user_id
	) VALUES (
		i_name,
		i_rut,
		lower(i_email),
		i_phone,
		i_is_company,
		i_region,
		i_salted_passwd,
		NOW(),
		'pending_confirmation',
		i_address,
		i_contact,
		i_lob,
		i_commune,
		i_gender,
		o_user_id_
	) RETURNING
		account_id
	INTO
		o_account_id;

	l_account_action_id := insert_account_action(o_account_id,'creation',NULL);
	-- inserting ad change to all the current ads for the new account
	FOR l_ad IN
		SELECT ad_id FROM ads WHERE user_id = o_user_id_ and status = 'active'
	LOOP
		-- mark as 'old ad assigned to new account'
		l_action_id := insert_ad_action(l_ad.ad_id, 'account_assign', NULL);
		PERFORM insert_state(l_ad.ad_id, l_action_id, 'reg', 'initial', '127.0.0.1', NULL);
		l_state_id := insert_state(l_ad.ad_id, l_action_id, 'accepted', 'adminclear', '127.0.0.1', NULL);
		-- apply ad_changes
		PERFORM apply_ad_changes(l_ad.ad_id, l_action_id, NULL);
	END LOOP;

	-- Check if the user has pro ads and put the categories into account_params
	l_account_to_pro := 0;
	FOR l_ad IN
		SELECT DISTINCT a.category FROM ads a join ad_actions ac ON (a.ad_id = ac.ad_id  AND ac.action_type = 'account_to_pro') WHERE a.user_id = o_user_id_
	LOOP
		l_pro_category := insert_pro_category_to_account_params(o_account_id, l_account_action_id, l_ad.category::text, NULL, i_pack_conf);
		l_account_to_pro := 1;
	END LOOP;

	IF l_pro_category IS NOT NULL THEN
		PERFORM insert_account_change(o_account_id, l_account_action_id, true, 'is_pro_for', '', l_pro_category);
	END IF;
	-- Update the account type
	IF l_account_to_pro = 1 THEN
		UPDATE accounts SET is_company = 't' WHERE account_id = o_account_id AND is_company = 'f';
	END IF;

	-- Save source of creation
	IF i_source IS NOT NULL THEN
		INSERT INTO account_params (account_id, name, value) VALUES (o_account_id, 'source', i_source);
	END IF;

	-- Save business_name of creation
	IF i_business_name IS NOT NULL THEN
		INSERT INTO account_params (account_id, name, value) VALUES (o_account_id, 'business_name', i_business_name);
	END IF;

END;
$$ LANGUAGE plpgsql;
