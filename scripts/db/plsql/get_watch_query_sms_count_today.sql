CREATE OR REPLACE FUNCTION get_watch_query_sms_count_today(
	i_watch_user_id watch_users.watch_user_id%TYPE,
	i_watch_query_id watch_queries.watch_query_id%TYPE,
	OUT o_num_sms_today integer) AS $$
BEGIN
		SELECT
			COUNT(*)
		INTO
			o_num_sms_today
		FROM
			sms_log
			JOIN sms_log_watch USING (sms_log_id)
		WHERE
			watch_user_id = i_watch_user_id
			AND watch_query_id = i_watch_query_id
			AND sms_type = 'save'
			AND sms_class = 'alert'
			AND logged_at >= CURRENT_DATE;
END
$$ LANGUAGE plpgsql;
