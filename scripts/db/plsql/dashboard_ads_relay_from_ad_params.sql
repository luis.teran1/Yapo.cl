CREATE OR REPLACE FUNCTION dashboard_ads_relay_from_ad_params(
) RETURNS TRIGGER AS $$
DECLARE
	l_ad_params_list    text;
	l_ad_id integer;
BEGIN
	--Code for premium services
	IF TG_OP = 'UPDATE' OR TG_OP = 'INSERT' THEN
		l_ad_id := NEW.ad_id;
		IF NEW.name = 'gallery' THEN
			UPDATE dashboard_ads
			SET gallery_date = NEW.value
			WHERE ad_id = NEW.ad_id;
		ELSIF NEW.name = 'label' THEN
			UPDATE dashboard_ads
			SET label = NEW.value
			WHERE ad_id = NEW.ad_id;
		ELSIF NEW.name = 'weekly_bump' or NEW.name = 'upselling_weekly_bump' THEN
			UPDATE dashboard_ads
			SET weekly_bump = CASE WHEN NEW.name = 'upselling_weekly_bump' THEN NEW.value::integer - 1 ELSE NEW.value::integer END
			WHERE ad_id = NEW.ad_id;
		ELSIF NEW.name = 'daily_bump' or NEW.name = 'upselling_daily_bump' THEN
			UPDATE dashboard_ads
			SET daily_bump = CASE WHEN NEW.name = 'upselling_daily_bump' THEN NEW.value::integer - 1 ELSE NEW.value::integer END
			WHERE ad_id = NEW.ad_id;
		ELSIF NEW.name = 'pack_status' THEN
			UPDATE dashboard_ads
			SET ad_pack_status = CASE WHEN NEW.value = 'active' THEN 1 ELSE 0 END
			WHERE ad_id = NEW.ad_id;
		END IF;
	ELSIF TG_OP = 'DELETE' THEN
		l_ad_id := OLD.ad_id;
		IF OLD.name = 'gallery' THEN
			UPDATE dashboard_ads
			SET gallery_date = NULL
			WHERE ad_id = OLD.ad_id;
		ELSIF OLD.name = 'label' THEN
			UPDATE dashboard_ads
			SET label = NULL
			WHERE ad_id = OLD.ad_id;
		ELSIF OLD.name = 'weekly_bump' or OLD.name = 'upselling_weekly_bump' THEN
			UPDATE dashboard_ads
			SET weekly_bump = NULL
			WHERE ad_id = OLD.ad_id;
		ELSIF OLD.name = 'daily_bump' or OLD.name = 'upselling_daily_bump' THEN
			UPDATE dashboard_ads
			SET daily_bump = NULL
			WHERE ad_id = OLD.ad_id;
		ELSIF OLD.name = 'pack_status' THEN
			UPDATE dashboard_ads
			SET ad_pack_status = NULL
			WHERE ad_id = OLD.ad_id;
		END IF;
	END IF;


	SELECT STRING_AGG(name || ',' || REPLACE(value, E'\n', '<br>'), '|&|')
	INTO l_ad_params_list
	FROM ad_params WHERE ad_id = l_ad_id;

	UPDATE dashboard_ads SET ad_params_list = l_ad_params_list WHERE ad_id = l_ad_id;

	RETURN NULL;
END;
$$ LANGUAGE plpgsql;
