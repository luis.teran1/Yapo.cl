CREATE OR REPLACE FUNCTION edit_account(i_name accounts.name%TYPE,
		i_rut accounts.rut%TYPE,
		i_email accounts.email%TYPE,
		i_phone accounts.phone%TYPE,
		i_is_company accounts.is_company%TYPE,
		i_region accounts.region%TYPE,
		i_address accounts.address%TYPE,
		i_contact accounts.contact%TYPE,
		i_lob accounts.lob%TYPE,
		i_commune accounts.commune%TYPE,
		i_phone_hidden accounts.phone_hidden%TYPE,
		i_salted_password accounts.salted_passwd%TYPE,
		i_token_id integer,
		i_gender accounts.gender%TYPE,
		i_birthdate accounts.birthdate%TYPE,
		i_business_name account_params.value%TYPE,
		OUT o_account_id accounts.account_id%TYPE) AS $$
DECLARE
	l_account_id accounts.account_id%TYPE;
BEGIN

	SELECT
		account_id
	INTO
		l_account_id
	FROM
		accounts
	WHERE
		i_email = email;

	IF NOT FOUND THEN
		RAISE EXCEPTION 'FACTORY_TRANSLATE:error:ACCOUNT_DO_NOT_EXISTS';
	END IF;

	-- checking first if the user is pro for some category
	IF NOT i_is_company THEN
		PERFORM
			p.value
		FROM
			accounts a
			JOIN account_params p ON(a.account_id = p.account_id AND p.name = 'is_pro_for')
		WHERE
			i_email = email;

		IF FOUND THEN
			RAISE EXCEPTION 'FACTORY_TRANSLATE:error:ACCOUNT_IS_PRO';
		END IF;
	END IF;

	IF i_salted_password IS NOT NULL THEN
		UPDATE accounts SET (
			name,
			rut,
			phone,
			is_company,
			region,
			address,
			contact,
			lob,
			commune,
			phone_hidden,
			salted_passwd,
			birthdate,
			gender
		) = (
			i_name,
			i_rut,
			i_phone,
			i_is_company,
			i_region,
			i_address,
			i_contact,
			i_lob,
			i_commune,
			i_phone_hidden,
			i_salted_password,
			i_birthdate,
			i_gender
		)
		WHERE
			account_id = l_account_id
		RETURNING
			account_id
		INTO
			o_account_id;

		PERFORM insert_account_action(l_account_id,'edition',i_token_id);
		PERFORM insert_account_action(l_account_id,'password_change',i_token_id);
	ELSE
		UPDATE accounts SET (
			name,
			rut,
			phone,
			is_company,
			region,
			address,
			contact,
			lob,
			commune,
			phone_hidden,
			birthdate,
			gender
		) = (
			i_name,
			i_rut,
			i_phone,
			i_is_company,
			i_region,
			i_address,
			i_contact,
			i_lob,
			i_commune,
			i_phone_hidden,
			i_birthdate,
			i_gender
		)
		WHERE
			account_id = l_account_id
		RETURNING
			account_id
		INTO
			o_account_id;

		PERFORM insert_account_action(o_account_id,'edition',i_token_id);
	END IF;

    IF EXISTS(
        SELECT value
        FROM account_params
        WHERE account_id = l_account_id AND name = 'business_name'
    ) THEN
        IF i_business_name IS NULL THEN
            DELETE FROM account_params
            WHERE account_id = l_account_id AND name = 'business_name';
        ELSE
            UPDATE account_params SET value = i_business_name
            WHERE account_id = l_account_id AND name = 'business_name';
        END IF;
    ELSE
        IF i_business_name IS NOT NULL THEN
            INSERT INTO account_params (account_id, name, value)
            VALUES (l_account_id, 'business_name', i_business_name);
        END IF;
    END IF;
END;
$$ LANGUAGE plpgsql;
