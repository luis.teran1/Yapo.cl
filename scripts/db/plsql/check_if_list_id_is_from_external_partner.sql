CREATE OR REPLACE FUNCTION check_if_list_id_is_from_external_partner(
						i_list_id ads.list_id%TYPE
						) RETURNS bool AS $$
BEGIN
	RETURN EXISTS (
		SELECT
			ad_params.value
		FROM
			ad_params
		JOIN ads USING (ad_id)
		WHERE
			ad_params.name = 'external_ad_id'
		AND ads.list_id = i_list_id
	    LIMIT 1
	);
END;
$$ LANGUAGE plpgsql;
