CREATE OR REPLACE FUNCTION insert_pro_category_to_account_params(
					i_account_id accounts.account_id%TYPE,
					i_account_action_id account_actions.account_action_id%TYPE,
					i_category varchar,
					i_token_id integer,
					i_pack_conf varchar[][],
					OUT o_pro_categories account_params.value%TYPE
					) AS $$
DECLARE
	l_first_pack_type varchar;
	l_old_first_pack_type varchar;
BEGIN


	SELECT value INTO l_old_first_pack_type FROM account_params WHERE account_id = i_account_id AND name = 'pack_type';
	IF NOT FOUND THEN
		l_old_first_pack_type := '';
	END IF;

	UPDATE account_params
		SET value = (
					CASE WHEN POSITION(CAST(i_category as text) in value) = 0 THEN
							-- Change the order to get first inmo o auto, not services yet
							CASE WHEN POSITION('70' in value) = 1 THEN
								CAST(i_category as text) || ',' || value
							ELSE
								value || ',' || CAST(i_category as text)
							END
						ELSE
							value
					END
					)
	WHERE
		account_id = i_account_id AND name = 'is_pro_for'
	RETURNING
		value
	INTO
		o_pro_categories;

	IF NOT FOUND THEN
		INSERT INTO
			account_params(account_id, name, value)
		VALUES
			(i_account_id, 'is_pro_for', cast(i_category as text))
		RETURNING
			value
		INTO
			o_pro_categories;
	END IF;


	l_first_pack_type := first_pack_type(cast(o_pro_categories as text), i_pack_conf, i_account_id);

	IF l_first_pack_type IS NOT NULL THEN

		UPDATE
			account_params
		SET
			value = l_first_pack_type
		WHERE
			account_id = i_account_id
		AND
			name = 'pack_type';

		IF NOT FOUND THEN
			INSERT INTO
				account_params(account_id, name, value)
			VALUES
				(i_account_id, 'pack_type', l_first_pack_type);
		END IF;

		PERFORM insert_account_change(i_account_id, i_account_action_id, true, 'pack_type', l_old_first_pack_type, l_first_pack_type);
	END IF;

END;
$$ LANGUAGE plpgsql;
