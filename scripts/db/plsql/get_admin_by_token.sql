CREATE OR REPLACE FUNCTION get_admin_by_token(i_token tokens.token%TYPE) RETURNS admins.admin_id%TYPE AS $$
DECLARE
	l_admin_id admins.admin_id%TYPE;
BEGIN
	SELECT
		admin_id
	INTO
		l_admin_id
	FROM
		tokens
	WHERE
		token = i_token;
	RETURN l_admin_id;
END
$$ LANGUAGE plpgsql;
