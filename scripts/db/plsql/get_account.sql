CREATE OR REPLACE FUNCTION get_account(
	i_email varchar,
	i_user_id integer,
	i_account_id integer,
	i_pack_conf varchar[][],

	OUT o_account_id integer,
	OUT o_name varchar,
	OUT o_rut varchar,
	OUT o_email varchar,
	OUT o_phone varchar,
	OUT o_is_company boolean,
	OUT o_region smallint,
	OUT o_password varchar,
	OUT o_salt varchar,
	OUT o_creation_date timestamp,
	OUT o_status enum_account_status,
	OUT o_address varchar,
	OUT o_contact varchar,
	OUT o_lob varchar,
	OUT o_commune varchar,
	OUT o_phone_hidden boolean,
	OUT o_user_id integer,
	OUT o_total integer,
	OUT o_active_ads	integer,
	OUT o_inactive_ads	integer,
	OUT o_store_id integer,
	OUT o_store_url varchar,
	OUT o_store_status varchar,
	OUT o_is_pro_for varchar,
	OUT o_pack_status varchar,
	OUT o_pack_slots integer,
	OUT o_pack_type varchar,
	OUT o_has_pack_ads integer,
	OUT o_gender varchar,
	OUT o_birthdate timestamp,
	OUT o_social_partner text,
	OUT o_social_id text,
	OUT o_uuid UUID,
	OUT o_business_name varchar
) AS $$
DECLARE
BEGIN
	SELECT
		a.account_id,
		a.name,
		a.rut,
		a.email,
		a.phone,
		a.is_company,
		a.region,
		a.salted_passwd,
		regexp_replace(a.salted_passwd,E'^(\\$[0-9]+\\$.{16}).*',E'\\1') AS salt,
		a.creation_date,
		a.status,
		a.address,
		a.contact,
		a.lob,
		a.commune,
		a.phone_hidden,
		a.user_id,
		s.store_id,
		s.url AS store_url,
		s.status AS store_status,
		p.value as is_pro_for,
		p2.value as available_slots,
		p3.value as pack_type,
		a.gender,
		a.birthdate,
		sa.social_partner,
		sa.social_id,
		insert_or_get_uuid(a.email),
		p4.value as business_name
	INTO 
		o_account_id,
		o_name,
		o_rut,
		o_email,
		o_phone,
		o_is_company,
		o_region,
		o_password,
		o_salt,
		o_creation_date,
		o_status,
		o_address,
		o_contact,
		o_lob,
		o_commune,
		o_phone_hidden,
		o_user_id,
		o_store_id,
		o_store_url,
		o_store_status,
		o_is_pro_for,
		o_pack_slots,
		o_pack_type,
		o_gender,
		o_birthdate,
		o_social_partner,
		o_social_id,
		o_uuid,
		o_business_name
	FROM 
		accounts a
		LEFT JOIN stores s USING(account_id)
		LEFT JOIN account_params p ON(a.account_id = p.account_id AND p.name = 'is_pro_for')
		LEFT JOIN account_params p2 ON(a.account_id = p2.account_id AND p2.name = 'available_slots')
		LEFT JOIN account_params p3 ON(a.account_id = p3.account_id AND p3.name = 'pack_type')
		LEFT JOIN account_params p4 ON(a.account_id = p4.account_id AND p4.name = 'business_name')
		LEFT JOIN social_accounts sa ON(a.account_id = sa.account_id AND a.email = sa.email AND sa.social_partner = 'facebook_id')
	WHERE
		a.email= lower(i_email) OR a.user_id = i_user_id OR a.account_id = i_account_id;

	IF NOT FOUND THEN 
		RAISE EXCEPTION 'FACTORY_TRANSLATE:error:ACCOUNT_NOT_FOUND';
	END IF;

	SELECT status INTO o_pack_status FROM packs WHERE account_id = o_account_id and status = 'active' LIMIT 1;
	IF NOT FOUND THEN
		IF EXISTS (SELECT status FROM packs WHERE account_id = o_account_id and status = 'expired' LIMIT 1) THEN
			o_pack_status := 'expired';
		ELSE 
			o_pack_status := NULL;
		END IF;
	END IF;
	-- Check if the user has any ad pack of the pack categories
	IF o_pack_type IS NOT NULL THEN
		IF EXISTS (
			SELECT
				ad_id
			FROM
				ads
			WHERE
				user_id = o_user_id
				AND category = ANY (find_pack_categories(o_pack_type, i_pack_conf))
			LIMIT 1
		) THEN
			o_has_pack_ads = 1;
		END IF;
	END IF;

END;
$$ LANGUAGE plpgsql;
