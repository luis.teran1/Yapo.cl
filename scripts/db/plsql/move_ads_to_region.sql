CREATE OR REPLACE FUNCTION move_ads_to_region(
    i_from_region integer,
    i_to_region integer,
    -- List of communes to be filtered inside a region
    i_communes character varying[],
    i_status character varying[],
    i_remote_addr character varying,
    i_token_id integer)
    RETURNS void AS $$

DECLARE
    l_action_id ad_actions.action_id%TYPE;
    l_state_id action_states.state_id%TYPE;
    l_current_region ads.region%TYPE;
    l_current_state ad_actions.state%TYPE;
    l_index integer;
    l_ad_ids integer[];
    l_token_id varchar;
BEGIN
    -- Get Ad ids by requested region and its communes
    SELECT ARRAY(
        SELECT get_ads_by_region_communes(
        i_from_region,
        i_status,
        i_communes
        )
    ) into l_ad_ids;
    -- Starts job if there are any ads to change
    IF (array_length(l_ad_ids, 1) >= 1) THEN
        RAISE NOTICE 'Ad ids retrieved: (%), starting job.', array_length(l_ad_ids, 1);
        -- Iterate between the ads ids
        SELECT get_token_id(i_token_id::varchar) into l_token_id;
        FOR l_index IN array_lower(l_ad_ids, 1) .. array_upper(l_ad_ids, 1)
        LOOP
            -- Logging info of actual running ad
            RAISE NOTICE 'Ad id: (%), sending job, count %/100', 
                l_ad_ids[l_index], 
                ROUND(AVG((l_index * 100 / array_length(l_ad_ids, 1)))::numeric, 4);
            -- Create the move to region action with the respective state
            PERFORM update_ad_region(
                l_ad_ids[l_index]::integer,
                i_from_region,
                i_to_region,
                i_remote_addr::varchar,
                l_token_id::varchar);
        END LOOP;
    ELSE
        RAISE NOTICE 'No ads found';
    END IF;
END
$$ LANGUAGE plpgsql;
