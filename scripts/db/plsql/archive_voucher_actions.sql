CREATE OR REPLACE FUNCTION archive_voucher_actions (
	i_grace_time interval,
	out o_status integer) RETURNS SETOF integer AS $$
DECLARE
	l_rec record;
BEGIN
	FOR l_rec IN
		SELECT
			voucher_actions.voucher_id,
			voucher_action_id,
			'blocket_' || EXTRACT(year FROM timestamp) as schema 
		FROM
			voucher_actions
			JOIN voucher_states USING (voucher_id, voucher_action_id)
		WHERE
			voucher_state_id = current_state AND
			action_type NOT IN ('pay') AND
			timestamp < CURRENT_TIMESTAMP - i_grace_time
	LOOP
		o_status := copy_voucher_action(l_rec.voucher_id, l_rec.voucher_action_id, l_rec.schema);
		o_status := clean_voucher_action(l_rec.voucher_id, l_rec.voucher_action_id);
		RETURN NEXT;
	END LOOP;
END;
$$ LANGUAGE plpgsql;
