CREATE OR REPLACE FUNCTION insert_or_update_visits(i_day varchar[], 
		i_vid integer[],
		i_n_visits integer[],
		i_n_entering_visits integer[],
		i_n_free_entering_visits integer[],
		i_n_entering_visits_from_click integer[],
		i_n_visits_from_click integer[],
		i_n_seconds_spent integer[]) RETURNS VOID AS $$
DECLARE
	l_i integer;
	l_day varchar;
	l_vid integer;
	l_n_visits integer;
	l_n_entering_visits integer;
	l_n_free_entering_visits integer;
	l_n_visits_from_click integer;
	l_n_entering_visits_from_click integer;
	l_n_seconds_spent integer;
BEGIN
	IF i_day IS NOT NULL THEN 
		l_i := 1; -- Arrays in postgresql are 1-based
		LOOP
			
			l_day := i_day[l_i];
			l_vid := i_vid[l_i];
			l_n_visits := i_n_visits[l_i];
			l_n_entering_visits := i_n_entering_visits[l_i];
			l_n_free_entering_visits := i_n_free_entering_visits[l_i];
			l_n_visits_from_click := i_n_visits_from_click[l_i];
			l_n_entering_visits_from_click := i_n_entering_visits_from_click[l_i];
			l_n_seconds_spent := i_n_seconds_spent[l_i];
			EXIT WHEN l_day IS NULL;

			UPDATE
				visits
			SET
				n_visits = n_visits + l_n_visits,
				n_entering_visits = n_entering_visits + l_n_entering_visits,
				n_free_entering_visits = n_free_entering_visits + l_n_free_entering_visits,
				n_visits_from_click = n_visits_from_click + l_n_visits_from_click,
				n_entering_visits_from_click = n_entering_visits_from_click + l_n_entering_visits_from_click,
				n_seconds_spent = n_seconds_spent + l_n_seconds_spent
			WHERE
				vid = l_vid AND
				day = l_day::date;
			
			IF NOT FOUND THEN
				SELECT
					vid
				INTO
					l_vid
				FROM
					visitor
				WHERE
					vid = l_vid;

				IF NOT FOUND THEN
					l_vid := i_vid[l_i];
					PERFORM insert_visitor ( ARRAY[l_vid], ARRAY[CAST (CURRENT_DATE AS text)]);
				END IF;

				INSERT INTO
						visits
						(day,
						vid,
						n_visits,
						n_entering_visits,
						n_free_entering_visits,
						n_visits_from_click,
						n_entering_visits_from_click,
						n_seconds_spent)
					VALUES
						(l_day::date,
						 l_vid,
						 l_n_visits,
						 l_n_entering_visits,
						 l_n_free_entering_visits,
						 l_n_visits_from_click,
						 l_n_entering_visits_from_click,
						 l_n_seconds_spent);
			END IF;
			l_i := l_i + 1;
		END LOOP;
	END IF;
END;
$$ LANGUAGE plpgsql;

