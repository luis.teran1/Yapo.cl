CREATE OR REPLACE FUNCTION get_abuse_reports( i_list_id ads.list_id%TYPE,
					      i_report_type abuse_reports.report_type%TYPE,
						OUT o_report_id abuse_reports.report_id%TYPE,
						OUT o_report_type abuse_reports.report_type%TYPE,
						OUT o_reporter_id abuse_reports.reporter_id%TYPE,
						OUT o_user_id abuse_reports.user_id%TYPE,
						OUT o_email users.email%TYPE,
						OUT o_accepted_reports abuse_reporters.accepted_reports%TYPE,
						OUT o_rejected_reports abuse_reporters.rejected_reports%TYPE,
						OUT o_text abuse_reports.text%TYPE,
						OUT o_priority INTEGER,
						OUT o_list_id ads.list_id%TYPE, 
						OUT o_date abuse_reports.date_insert%TYPE) RETURNS SETOF RECORD AS $$

DECLARE
	l_ad_id INTEGER;
	l_rec record;
BEGIN
	SELECT
		ad_id
	INTO
		l_ad_id
	FROM
		ads
	WHERE
		list_id = i_list_id
	;

	IF l_ad_id IS NULL THEN
		RAISE EXCEPTION 'ERROR_INVALID_AD';
	END IF;

	FOR
		l_rec
	IN
		SELECT
			abuse_reports.report_id,
			abuse_reports.report_type,
			abuse_reports.reporter_id,
			abuse_reports.user_id,
			users.email,
			abuse_reporters.accepted_reports,
			abuse_reporters.rejected_reports,
			abuse_reports.text,
			CASE WHEN report_type = i_report_type THEN 1 ELSE 2 END AS priority,
			abuse_reports.date_insert

		FROM
			abuse_reports LEFT JOIN
			abuse_reporters ON (abuse_reports.reporter_id = abuse_reporters.uid) LEFT JOIN
			users ON (abuse_reports.user_id = users.user_id)
		WHERE
			abuse_reports.status = 'unsolved'
			AND ad_id = l_ad_id
		ORDER BY
			priority, abuse_reports.report_type
	LOOP
		o_report_id := l_rec.report_id;
		o_report_type := l_rec.report_type;
		o_reporter_id := l_rec.reporter_id;
		o_user_id := l_rec.user_id;
		o_email := l_rec.email;
		o_accepted_reports := l_rec.accepted_reports;
		o_rejected_reports := l_rec.rejected_reports;
		o_text := l_rec.text;
		o_priority := l_rec.priority;
		o_date := l_rec.date_insert;
		RETURN NEXT;
	END LOOP;
END
$$ LANGUAGE plpgsql;
