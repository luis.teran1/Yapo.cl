CREATE OR REPLACE FUNCTION has_force_queue(
	i_ad_id ad_actions.ad_id%TYPE,
	i_action_id ad_actions.action_id%TYPE,
	OUT o_queue varchar
) RETURNS varchar AS $$
BEGIN
	SELECT value
	INTO o_queue
	FROM action_params
	WHERE ad_id  = i_ad_id AND action_id = i_action_id AND name = 'force_queue';
END;
$$ LANGUAGE plpgsql;
