CREATE OR REPLACE FUNCTION refund_insert_fun(
	i_ad_id					refunds.ad_id%TYPE,
	i_action_id				refunds.action_id%TYPE,
	i_purchase_id			refunds.purchase_id%TYPE,
	i_first_name			refunds.first_name%TYPE,
	i_last_name				refunds.last_name%TYPE,
	i_rut					refunds.rut%TYPE,
	i_email					refunds.email%TYPE,
	i_bank_account_type		refunds.bank_account_type%TYPE,
	i_bank					refunds.bank%TYPE,
	i_bank_account_number	refunds.bank_account_number%TYPE
) RETURNS INTEGER AS $$
DECLARE
	l_refund_id INTEGER;
BEGIN	
	BEGIN
		INSERT INTO refunds(	
			ad_id,
			action_id,
			purchase_id,
			first_name,
			last_name,
			rut,
			email,
			bank_account_type,
			bank,
			bank_account_number,
			status
		) VALUES (
			i_ad_id,
			i_action_id,
			i_purchase_id,
			i_first_name,
			i_last_name,
			i_rut,
			i_email,
			i_bank_account_type,
			i_bank,
			i_bank_account_number,
			'pending'
		) RETURNING refund_id INTO l_refund_id;
	EXCEPTION WHEN unique_violation THEN
		RAISE EXCEPTION 'FACTORY_TRANSLATE:error:ERROR_REFUND_DUPLICATED';
	END;
	RETURN l_refund_id;
END;
$$ LANGUAGE plpgsql;
