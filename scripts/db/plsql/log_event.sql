CREATE OR REPLACE FUNCTION log_event(i_event_name event_log.event_name%TYPE, 
				  i_event event_log.event%TYPE,
				  i_token_id tokens.token_id%TYPE) RETURNS void AS $$
BEGIN
	INSERT INTO
		event_log
		(
		 event_name,
		 event,
		 token_id,
		 time
		)
	VALUES (
		i_event_name,
		i_event,
		i_token_id,
		CURRENT_TIMESTAMP
	       );
END;
$$ LANGUAGE plpgsql;
