CREATE OR REPLACE FUNCTION create_regress_mail_queue_data()
RETURNS VOID AS $$
DECLARE
        l_date DATE;
        l_day INTEGER;
        l_month INTEGER;
        l_hour INTEGER;
        l_minute INTEGER;
        l_year INTEGER;
        l_user INTEGER;
        l_schema VARCHAR;
        l_ads INTEGER[];
        l_sender_email VARCHAR;
        l_sender_name VARCHAR;
        l_receipient_email VARCHAR;
        l_receipient_name VARCHAR;
        l_subject VARCHAR;
BEGIN
        SELECT array_agg(distinct(list_id))
        INTO l_ads
        FROM ads
        WHERE list_id IS NOT NULL;

        FOR month IN 0..17 LOOP
                l_date := CURRENT_DATE - INTERVAL '1 MONTH' * month;
                l_year := extract(year from l_date);
                l_month:= extract(month from l_date);
                l_schema := 'blocket_' || l_year;
                RAISE NOTICE 'ADDING MAIL_QUEUE DATA FOR DATE % in SCHEMA %', l_date, l_schema;

                FOR i IN array_lower(l_ads, 1) .. array_upper(l_ads, 1) LOOP
                        SELECT round(random() * 2) INTO l_user;
                        SELECT round(random() * 27) + 1 INTO l_day;
                        SELECT round(random() * 23) INTO l_hour;
                        SELECT round(random() * 59) INTO l_minute;
                        -- get receipient_email, receipient_name
                        SELECT name, email, subject
                                INTO l_receipient_name, l_receipient_email, l_subject
                                FROM ads INNER JOIN users
                                USING(user_id)
                                WHERE list_id = l_ads[i];
                        -- get sender_email, sender_name
                        SELECT name, email
                                INTO l_sender_name, l_sender_email
                                FROM ads INNER JOIN users
                                USING(user_id)
                                ORDER BY random()
                                LIMIT 1;
                        -- INSERT NEW MESSAGE
                        EXECUTE format('INSERT INTO %s.mail_queue (state, template_name, added_at, remote_addr, sender_email, sender_name, receipient_email, receipient_name, subject, body, list_id, sender_phone)
                                VALUES (''sent'', ''mail/adreply.mime'', ''%s-%s-%s %s:%s'', ''10.0.0.1'', ''%s'', ''%s'', ''%s'', ''%s'', ''%s'', ''about your ad %s'', %s, ''123123123'')',
                                l_schema, l_year, l_month, l_day, l_hour, l_minute, l_sender_email, l_sender_name, l_receipient_email, l_receipient_name, l_subject, l_subject, l_ads[i]);
                END LOOP;
        END LOOP;
END
$$ LANGUAGE plpgsql;
