CREATE OR REPLACE FUNCTION get_enum_values(i_name pg_type.typname%TYPE) RETURNS SETOF pg_enum.enumlabel%TYPE AS $$
SELECT 
	enumlabel 
FROM 
	pg_enum 
WHERE 
	enumtypid = (SELECT oid FROM pg_type WHERE typname = $1)
ORDER BY
	enumlabel;
$$ LANGUAGE sql;
