CREATE OR REPLACE FUNCTION get_account_ads2(
					i_user_id 				users.user_id%TYPE,
					i_account_id 			accounts.account_id%TYPE,
					i_query 		        varchar,
					i_pack_sort				integer,
					i_all_params			integer,
					OUT o_ad_id          	ads.ad_id%TYPE,
					OUT o_list_id        	ads.list_id%TYPE,
					OUT o_list_time      	ads.list_time%TYPE,
					OUT o_subject        	ads.subject%TYPE,
					OUT o_body				ads.body%TYPE,
					OUT o_company_ad		integer,
					OUT o_ad_type			ads.type%TYPE,
					OUT o_category          ads.category%TYPE,
					OUT o_price          	ads.price%TYPE,
					OUT o_ad_status        	varchar,
					OUT o_ad_media_id    	BIGINT,
					OUT o_image_count		BIGINT,
					OUT o_daily_bump		varchar,
					OUT o_weekly_bump		varchar,
					OUT o_label				varchar,
					OUT o_gallery_date		varchar,
					OUT o_ad_pack_status	varchar,
					OUT o_ad_params_list	varchar,
					OUT o_auto_bump         varchar,
					OUT o_paid              boolean
) RETURNS SETOF record AS $$
DECLARE
	l_account_status 	accounts.status%TYPE;
	l_rec 			record;
	l_action_type 		ad_actions.action_type%TYPE;
	l_action_state 		ad_actions.state%TYPE;
	l_subject_changed	TEXT;
	l_category_changed  TEXT;
	l_price_changed	        TEXT;
	l_image_0_changed 	TEXT;
	l_image_count_changed	integer;
	l_time_min timestamp;
BEGIN
	IF i_user_id IS NOT NULL THEN
		SELECT status INTO l_account_status FROM accounts WHERE user_id = i_user_id;
	ELSE
		SELECT user_id,status INTO i_user_id,l_account_status FROM accounts WHERE account_id = i_account_id;
	END IF;

	IF NOT FOUND THEN
		RAISE EXCEPTION 'ERROR_ACCOUNT_NOT_FOUND';
	END IF;

	l_time_min = CURRENT_TIMESTAMP - (INTERVAL '60 minutes');

	FOR
		l_rec
	IN
		SELECT
			ad_id,
			list_id,
			list_time,
			subject,
			REPLACE(body, E'\n', '<br>') as body,
			category,
			price,
			ad_status,
			type,
			company_ad,
			ad_media_id,
			image_count,
			daily_bump,
			weekly_bump,
			label,
			gallery_date,
			ad_pack_status,
			ad_params_list,
			last_change,
			auto_bump,
			paid
		FROM
			dashboard_ads
		WHERE
			user_id = i_user_id
		and ad_status not in ('hidden', 'refused')
		and (
			CASE
				WHEN ad_status = 'deleted' AND last_change < l_time_min  THEN false ELSE true
			END
		)
		and (
			CASE
				WHEN i_query is not null THEN lower(subject) like '%'||i_query||'%'
				ELSE true
			END
		)
	ORDER BY
		(CASE
			WHEN i_pack_sort IS NULL THEN 1
			WHEN i_pack_sort = 1 AND ad_pack_status = '1' THEN 1
			WHEN i_pack_sort = 1 AND ad_pack_status = '0' THEN 2
			WHEN i_pack_sort = 0 AND ad_pack_status = '1' THEN 2
			WHEN i_pack_sort = 0 AND ad_pack_status = '0' THEN 1
			ELSE 3
		END),
		(CASE
			WHEN ad_status = 'inactive' THEN 1
			WHEN ad_status = 'refused' THEN 2
			ELSE 3
		END),
		list_time DESC,
		ad_id DESC

	LOOP
		o_ad_id        := l_rec.ad_id;
		o_list_id      := l_rec.list_id;
		o_list_time    := l_rec.list_time;
		o_subject      := l_rec.subject;
		o_body         := l_rec.body;
		o_category     := l_rec.category;
		o_price        := l_rec.price;
		o_ad_status    := l_rec.ad_status;
		o_ad_type      := l_rec.type;
		o_company_ad   := l_rec.company_ad::INTEGER;

		o_ad_media_id  := l_rec.ad_media_id;
		o_image_count  := l_rec.image_count;

		o_daily_bump   := l_rec.daily_bump;
		o_weekly_bump  := l_rec.weekly_bump;
		o_gallery_date := l_rec.gallery_date;
		o_label        := l_rec.label;

		o_ad_pack_status := l_rec.ad_pack_status;
		o_auto_bump      := l_rec.auto_bump;
		o_paid      := l_rec.paid;

		IF i_all_params IS NOT NULL AND i_all_params = 1 THEN
			o_ad_params_list := l_rec.ad_params_list;
		ELSE
			o_ad_params_list := '';
		END IF;

		RETURN NEXT;
	END LOOP;
END;
$$ LANGUAGE plpgsql;
