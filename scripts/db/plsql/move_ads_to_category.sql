CREATE OR REPLACE FUNCTION move_ads_to_category(
												i_ad_ids integer[],
												i_category ads.category%TYPE,
												i_new_gender varchar,
												i_from_category ads.category%TYPE,
												i_token tokens.token%TYPE,
												i_remote_addr action_states.remote_addr%TYPE
												) RETURNS void AS $$
DECLARE
	l_action_id ad_actions.action_id%TYPE;
	l_state_id action_states.state_id%TYPE;
	l_current_category ads.category%TYPE;
	l_current_state ad_actions.state%TYPE;
	l_index integer;
BEGIN
	-- Procedure to move a set of ads from one category to another

	-- Iterate between the ads ids
	FOR l_index IN array_lower(i_ad_ids, 1) .. array_upper(i_ad_ids,1)
	LOOP
		-- Verify if ad was deleted
		SELECT aa.state INTO l_current_state FROM ad_actions as aa join action_states as ae on ae.ad_id = aa.ad_id and aa.current_state = ae.state_id WHERE aa.ad_id = i_ad_ids[l_index] ORDER BY timestamp DESC LIMIT 1;
		IF l_current_state = 'deleted' THEN
			CONTINUE;
		END IF;

		-- Verify if ad was moved to another category before
		SELECT category INTO l_current_category FROM ads where ad_id = i_ad_ids[l_index]; -- get current category
		IF l_current_category = i_category THEN
			CONTINUE;
		END IF;

		--Create the move to category action with the respective state
		SELECT
			o_action_id,
			o_state_id
		INTO
			l_action_id,
			l_state_id
		FROM insert_ad_action_state(i_ad_ids[l_index], 'move_to_category', 'reg', 'initial', i_remote_addr, get_token_id(i_token));

		--It's necessary to create the state param 'previous_category' in order to track the action
		INSERT INTO state_params
					(	ad_id,
						action_id,
						state_id,
						name,
						value )
		VALUES (
				i_ad_ids[l_index],
				l_action_id,
				l_state_id,
				'previous_category',
				l_current_category);

		l_state_id := insert_state(i_ad_ids[l_index], l_action_id, 'accepted', 'adminclear', i_remote_addr, NULL);
		IF i_new_gender is NOT NULL AND i_new_gender in ('1', '2', '5') THEN
			PERFORM insert_ad_change(i_ad_ids[l_index], l_action_id, l_state_id, true, 'footwear_gender', i_new_gender);
		END IF;
		-- apply ad_changes
		PERFORM insert_ad_change(i_ad_ids[l_index], l_action_id, l_state_id, false, 'category', i_category::varchar);
		PERFORM apply_ad_changes(i_ad_ids[l_index], l_action_id, NULL);

	END LOOP;
END;
$$ LANGUAGE plpgsql;
