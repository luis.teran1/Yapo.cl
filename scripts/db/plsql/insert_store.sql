CREATE OR REPLACE FUNCTION insert_store(
					i_token tokens.token%TYPE,
					i_account_id accounts.account_id%TYPE,
					i_payment_group_id payment_groups.payment_group_id%TYPE,
					OUT o_store_id stores.store_id%TYPE,
					OUT o_return_state varchar(20),
					OUT o_email accounts.email%TYPE
					) AS $$
DECLARE
	l_account_status accounts.status%TYPE;
	l_account_id accounts.account_id%TYPE;
BEGIN

	SELECT account_id, status, email INTO l_account_id, l_account_status, o_email FROM accounts WHERE account_id = i_account_id;

	IF l_account_id IS NULL THEN	
		RAISE EXCEPTION 'FACTORY_TRANSLATE:error:ERROR_ACCOUNT_NOT_FOUND';
	END IF;

	IF l_account_status != 'active' THEN	
		RAISE EXCEPTION 'FACTORY_TRANSLATE:error:ERROR_ACCOUNT_INACTIVE';
	END IF;

	SELECT store_id INTO o_store_id FROM stores WHERE account_id = i_account_id;
	IF o_store_id IS NOT NULL THEN	
		o_return_state := 'STORE_ALREADY_EXISTS';
	ELSE

		INSERT INTO 
			stores
			(account_id)
		-- Add store_params
		VALUES
			(i_account_id)
		RETURNING
			store_id
		INTO
			o_store_id;

		PERFORM insert_store_action_state(
				CAST(o_store_id AS integer),
				'new',
				'reg', 
				get_token_id(i_token),
				i_payment_group_id
			) AS isas;

		PERFORM insert_store_action_state(
				CAST(o_store_id AS integer),
				'new',
				'unpaid', 
				get_token_id(i_token),
				i_payment_group_id
			) AS isas;

		o_return_state := 'STORE_CREATED';

	END IF;
END;
$$ LANGUAGE plpgsql;
