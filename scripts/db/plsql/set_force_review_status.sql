CREATE OR REPLACE FUNCTION set_force_review_status(
	i_ad_id ad_actions.ad_id%TYPE,
	i_action_id ad_actions.action_id%TYPE
) RETURNS VOID AS $$
BEGIN
	IF NOT EXISTS (SELECT ad_id FROM ad_actions WHERE ad_id = i_ad_id AND action_id = i_action_id and state in ('unpaid', 'unverified')) THEN
		RAISE EXCEPTION 'ERROR_AD_STATE_IS_ALREADY_VERIFIED';
	END IF;

	INSERT INTO action_params VALUES (i_ad_id, i_action_id, 'force_review', bool(1));
END
$$ LANGUAGE plpgsql;
