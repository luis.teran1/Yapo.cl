CREATE OR REPLACE FUNCTION sms_callback (
		i_session_id sms_log.session_id%TYPE,
		i_status_code sms_log.status_code%TYPE,
		OUT o_sms_log_id sms_log.sms_log_id%TYPE,
		OUT o_sms_user_id sms_users.sms_user_id%TYPE,
		OUT o_credit sms_users.credit%TYPE,
		OUT o_credit_total sms_users.credit_total%TYPE,
		OUT o_cost sms_log.cost%TYPE
		) AS $$
BEGIN
	INSERT INTO
		sms_log
		(sms_user_id, session_id, sms_type, cost, discount, mobile_originated, status_code)
	SELECT
		sms_user_id,
		i_session_id,
		(CASE i_status_code WHEN 1 THEN 'ok' ELSE 'error' END)::enum_sms_log_sms_type,
		CASE i_status_code WHEN 1 THEN cost ELSE 0 END,
		CASE i_status_code WHEN 1 THEN discount ELSE 0 END,
		'f',
		i_status_code
	FROM
		sms_log
	WHERE
		session_id = i_session_id
		AND sms_type = 'save'
	RETURNING
		sms_log_id,
		sms_user_id,
		cost - discount
	INTO
		o_sms_log_id,
		o_sms_user_id,
		o_cost
	;

	IF NOT FOUND THEN
		RAISE EXCEPTION 'ERROR_SMS_NOT_FOUND';
	END IF;

	IF i_status_code = 1 THEN
		UPDATE
			sms_users
		SET
			credit = credit + o_cost,
			credit_total = credit_total + o_cost
		WHERE
			sms_user_id = o_sms_user_id
		RETURNING
			credit,
			credit_total
		INTO
			o_credit,
			o_credit_total
		;
	ELSE
		SELECT
			credit,
			credit_total
		INTO
			o_credit,
			o_credit_total
		FROM
			sms_users
		WHERE
			sms_user_id = o_sms_user_id;
	END IF;
END
$$ LANGUAGE plpgsql;
