CREATE OR REPLACE FUNCTION stats_fetch_data_websql(i_start_date date,
		i_end_date date,
		o_date OUT varchar,
		o_queued_ads OUT integer,
		o_paid_ads OUT integer,
		o_paid_renew OUT integer,
		o_paid_prolong OUT integer,
		o_paid_edit OUT integer,
		o_extra_images OUT integer,
		o_videos OUT integer,
		o_income_ads OUT integer,
		o_income_extra_images OUT integer,
		o_deleted OUT integer,
		o_unique_ip OUT integer,
		o_pay_status_err OUT integer,
		o_pay_status_less OUT integer,
		o_pay_status_more OUT integer,
		o_pay_status_amount_err OUT integer,
		o_pay_status_amount_less OUT integer,
		o_pay_status_extra_more OUT integer
		) RETURNS SETOF RECORD AS $$

DECLARE
	l_loop_date stats_daily.stat_time%TYPE;

BEGIN
	l_loop_date = date_trunc('day', i_start_date);
 
	WHILE l_loop_date <= i_end_date LOOP

		-- Date
		o_date = l_loop_date;

		-- Queued ads
		SELECT
			COALESCE(sum(value::integer),0)
		FROM
			stats_daily
		INTO
			o_queued_ads
		WHERE
			(name = 'new_ads'
			OR name = 'renew_ads'
			OR name = 'prolong_ads'
			OR name = 'bump_ads')
			AND stat_time = l_loop_date;

		-- Paid ads
		SELECT
			COALESCE(sum(value::integer), 0)
		FROM
			stats_daily
		INTO
			o_paid_ads
		WHERE
			(name = 'payphone_20'
			OR name = 'payphone_30'
			OR name = 'payphone_40'
			OR name = 'payphone_60'
			OR name = 'payphone_80'
			OR name = 'payphone_90'
			OR name = 'paycard_20'
			OR name = 'paycard_30'
			OR name = 'paycard_40'
			OR name = 'paycard_60'
			OR name = 'paycard_80'
			OR name = 'paycard_90'
			OR name = 'payvoucher_20'
			OR name = 'payvoucher_30'
			OR name = 'payvoucher_40'
			OR name = 'payvoucher_60'
			OR name = 'payvoucher_80'
			OR name = 'payvoucher_90')
			AND stat_time = l_loop_date;

		-- Paid renew
		SELECT
			COALESCE(sum(value::integer), 0)
		FROM
			stats_daily
		INTO
			o_paid_renew
		WHERE
			name IN ('paid_renew', 'paid_bump')
			AND stat_time = l_loop_date;

		-- Paid prolong
		SELECT
			COALESCE(sum(value::integer), 0)
		FROM
			stats_daily
		INTO
			o_paid_prolong
		WHERE
			name = 'paid_prolong'
			AND stat_time = l_loop_date;

		-- Paid edit
		SELECT
			COALESCE(sum(value::integer), 0)
		FROM
			stats_daily
		INTO
			o_paid_edit
		WHERE
			name = 'paid_edit'
			AND stat_time = l_loop_date;

		-- Extra images
		SELECT
			COALESCE(sum(value::integer), 0)
		FROM
			stats_daily
		INTO
			o_extra_images
		WHERE
			(name = 'extra_images_10'
			OR name = 'extra_images_20'
			OR name = 'extra_images_40')
			AND stat_time = l_loop_date;

		-- Videos
		SELECT
			COALESCE(sum(value::integer), 0)
		FROM
			stats_daily
		INTO
			o_videos
		WHERE
			(name = 'video_0'
			OR name = 'video_20'
			OR name = 'video_40'
			OR name = 'video_70')
			AND stat_time = l_loop_date;

		-- Income ads 
		SELECT
		(
			(SELECT
				COALESCE(sum(value::integer), 0) * 16 
			FROM
				stats_daily
			WHERE
			(name = 'payphone_20'
				OR name = 'paycard_20'
				OR name = 'payvoucher_20')
				AND stat_time = l_loop_date)
			+

			(SELECT
				COALESCE(sum(value::integer), 0) * 24 
			FROM
				stats_daily
			WHERE
				(name = 'payphone_30'
				OR name = 'paycard_30'
				OR name = 'payvoucher_30')
				AND stat_time = l_loop_date)
			+

			(SELECT
				COALESCE(sum(value::integer), 0) * 32 
			FROM
				stats_daily
			WHERE
				(name = 'payphone_40'
				OR name = 'paycard_40'
				OR name = 'payvoucher_40')
				AND stat_time = l_loop_date)
			+

			(SELECT
				COALESCE(sum(value::integer), 0) * 48 
			FROM
				stats_daily
			WHERE
				(name = 'payphone_60'
				OR name = 'paycard_60'
				OR name = 'payvoucher_60')
				AND stat_time = l_loop_date)
			+

			(SELECT
				COALESCE(sum(value::integer), 0) * 64
			FROM
				stats_daily
			WHERE
				(name = 'payphone_80'
				OR name = 'paycard_80'
				OR name = 'payvoucher_80')
				AND stat_time = l_loop_date)
			+

			(SELECT
				COALESCE(sum(value::integer), 0) * 72
			FROM
				stats_daily
			WHERE
				(name = 'payphone_90'
				OR name = 'paycard_90'
				OR name = 'payvoucher_90')
				AND stat_time = l_loop_date)
		) 
		INTO
			o_income_ads;

		-- Income extra images
		SELECT
		(
			(SELECT
				sum(COALESCE(value,'0')::integer) * 8
			FROM
				stats_daily
			WHERE
				name = 'extra_images_10'
				AND stat_time = l_loop_date)
			+

			(SELECT
				sum(COALESCE(value,'0')::integer) * 16
			FROM
				stats_daily
			WHERE
				name = 'extra_images_20'
				AND stat_time = l_loop_date)
			+

			(SELECT
				sum(COALESCE(value,'0')::integer) * 32
			FROM
				stats_daily
			WHERE
				name = 'extra_images_40'
				AND stat_time = l_loop_date)
		)
		INTO
			o_income_extra_images;

		-- Deleted
		SELECT
			COALESCE(sum(value::integer), 0)
		FROM
			stats_daily
		INTO
			o_deleted
		WHERE
			name = 'deleted'
			AND stat_time = l_loop_date;

		-- Unique IP
		SELECT
			COALESCE(sum(value::integer), 0)
		FROM
			stats_daily
		INTO
			o_unique_ip
		WHERE
			name = 'unique_ip'
			AND stat_time = l_loop_date;

		-- Pay status error
		SELECT
			COALESCE(sum(value::integer), 0)
		FROM
			stats_daily
		INTO
			o_pay_status_err
		WHERE
			name = 'pay_status_err'
			AND stat_time = l_loop_date;

		-- Pay status less
		SELECT
			COALESCE(sum(value::integer), 0)
		FROM
			stats_daily
		INTO
			o_pay_status_less
		WHERE
			name = 'pay_status_less'
			AND stat_time = l_loop_date;

		-- Pay status more
		SELECT
			COALESCE(sum(value::integer), 0)
		FROM
			stats_daily
		INTO
			o_pay_status_more
		WHERE
			name = 'pay_status_more'
			AND stat_time = l_loop_date;

		-- Pay status amount error
		SELECT
			COALESCE(sum(value::integer), 0)
		INTO
			o_pay_status_amount_err
		FROM
			stats_daily
		WHERE
			name = 'pay_status_amount_err'
			AND stat_time = l_loop_date;

		-- Pay status amount less
		SELECT
			COALESCE(sum(value::integer), 0)
		INTO
			o_pay_status_amount_less
		FROM
			stats_daily
		WHERE
			name = 'pay_status_amount_less'
			AND stat_time = l_loop_date;

		-- Pay status amount more
		SELECT
			COALESCE(sum(value::integer), 0)
		INTO
			o_pay_status_extra_more
		FROM
			stats_daily
		WHERE
			name = 'pay_status_extra_more'
			AND stat_time = l_loop_date;


		l_loop_date := l_loop_date + interval '1 day'; 

			
		RETURN NEXT;
	END LOOP;

END
$$ LANGUAGE plpgsql;
