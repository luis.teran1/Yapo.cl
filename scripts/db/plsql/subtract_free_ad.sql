CREATE OR REPLACE FUNCTION subtract_free_ad(i_store_id stores.store_id%TYPE,
					    OUT o_free_ads integer) AS $$
BEGIN
	SELECT
		value
	INTO
		o_free_ads
	FROM
		store_params
	WHERE
		store_id = i_store_id AND
		name = 'free_ads';

	IF NOT FOUND OR o_free_ads < 1 THEN
		o_free_ads := NULL;
		RETURN;
	END IF;

	UPDATE
		store_params
	SET
		value = o_free_ads - 1
	WHERE
		store_id = i_store_id AND
		name = 'free_ads';
END
$$ LANGUAGE plpgsql;

