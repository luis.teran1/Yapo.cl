CREATE OR REPLACE FUNCTION insert_outgoing_sms (
		i_sms_user_id sms_users.sms_user_id%TYPE,
		i_sms_user_status sms_users.status%TYPE,
		i_cost sms_log.cost%TYPE,
		i_sms_type sms_log.sms_class%TYPE,
		OUT o_sms_log_id sms_log.sms_log_id%TYPE,
		OUT o_session_id sms_log.session_id%TYPE,
		OUT o_phone sms_users.phone%TYPE,
		OUT o_sms_central sms_users.sms_central%TYPE,
		OUT o_status sms_users.status%TYPE,
		OUT o_tariff sms_log.cost%TYPE
		) AS $$
DECLARE
	l_free_sms sms_users.free_sms%TYPE;
	l_discount sms_log.discount%TYPE;
BEGIN
	SELECT
		phone,
		sms_central,
		status,
		free_sms
	INTO
		o_phone,
		o_sms_central,
		o_status,
		l_free_sms
	FROM
		sms_users
	WHERE
		sms_user_id = i_sms_user_id;
	
	IF NOT FOUND THEN
		RAISE EXCEPTION 'ERROR_SMS_USER_NOT_FOUND';
	END IF;

	l_discount := 0;
	IF i_cost > 0 AND l_free_sms > 0 THEN
		-- Recheck in case of concurrent sends.
		UPDATE
			sms_users
		SET
			free_sms = free_sms - 1
		WHERE
			sms_user_id = i_sms_user_id
			AND free_sms > 0;

		IF FOUND THEN
			l_discount := i_cost;
		END iF;
	END IF;

	-- Block sending unless user is active OR transitioning from paused into active.
	IF (o_status <> 'active' AND NOT (o_status = 'paused' AND i_sms_user_status IS NOT NULL AND i_sms_user_status = 'active')) THEN
		RAISE EXCEPTION 'ERROR_SMS_USER_NOT_ACTIVE';
	END IF;	

	o_session_id := o_phone || ':' || nextval('sms_log_sms_log_id_seq');

	IF i_sms_user_status IS NOT NULL THEN
		SELECT
			t.o_status
		INTO
			o_status
		FROM
			adwatch_sms_change_user_status(o_phone, i_sms_user_status, NULL) AS t;
	END IF;

	INSERT INTO sms_log (
		sms_log_id,
		sms_user_id,
		cost,
		session_id,
		sms_type,
		sms_class,
		status_code,
		discount
	)  VALUES (
		currval('sms_log_sms_log_id_seq'),
		i_sms_user_id,
		i_cost,
		o_session_id,
		'save',
		i_sms_type,
		0,
		l_discount
	) RETURNING
		sms_log_id
	INTO
		o_sms_log_id;
	
	o_tariff := i_cost - l_discount;
END;
$$ LANGUAGE plpgsql;
