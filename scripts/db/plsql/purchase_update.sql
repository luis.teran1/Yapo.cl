CREATE OR REPLACE FUNCTION purchase_update(
		i_purchase_id purchase.purchase_id%TYPE,
		i_status purchase.status%TYPE,
		i_doc_num purchase.doc_num%TYPE,
		i_external_doc_id purchase.external_doc_id%TYPE
) RETURNS VOID AS $$

DECLARE
	v_doc_num purchase.doc_num%TYPE;
	v_external_doc_id purchase.external_doc_id%TYPE;
BEGIN
	-- Find the purchase
	SELECT
		doc_num, external_doc_id
	INTO
		v_doc_num, v_external_doc_id
	FROM
		purchase
	WHERE
		purchase_id = i_purchase_id;
	-- If document doesn't exist return trans error
	IF NOT FOUND THEN
		RAISE EXCEPTION 'FACTORY_TRANSLATE:error:ERROR_PURCHASE_NOT_FOUND';
	END IF;
	-- If document has number != i_doc_num return trans error
	IF v_doc_num IS NOT NULL AND v_doc_num != i_doc_num THEN
		RAISE EXCEPTION 'FACTORY_TRANSLATE:error:ERROR_PURCHASE_WITH_DIFERENT_NUMBER';
	ELSE
		-- Overwrite the doc number if was received as param (v_doc_num at this point can be NULL)
		IF i_doc_num IS NOT NULL THEN
			v_doc_num := i_doc_num;
		END IF;

		-- Overwrite external doc_id if was received as param (v_external_doc_id can be null at this moment)
		IF i_external_doc_id IS NOT NULL THEN
			v_external_doc_id := i_external_doc_id;
		END IF;

		-- Execute update
		UPDATE
			purchase
		SET
			doc_num 		=	v_doc_num,
			external_doc_id	=	v_external_doc_id,
			status			=	i_status
		WHERE
			purchase_id 	= 	i_purchase_id;
	END IF;
END;
$$ LANGUAGE plpgsql;
