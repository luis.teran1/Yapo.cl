CREATE OR REPLACE FUNCTION insert_payment_purchase(
				     i_price purchase_detail.price%TYPE,
				     i_remote_addr purchase_states.remote_addr%TYPE,
				     i_payment_type payments.payment_type%TYPE,
					 i_parent_payment_group_id payment_groups.parent_payment_group_id%TYPE,
				     OUT o_payment_group_id payment_groups.payment_group_id%TYPE,
				     OUT o_pay_code payment_groups.code%TYPE ) AS $$

BEGIN

	-- Create unpaid payment
	o_pay_code := get_ad_code('pay');

        INSERT INTO payment_groups (
                code,
                status,
				parent_payment_group_id
        ) VALUES (
                o_pay_code,
                'unverified',
				i_parent_payment_group_id
        ) RETURNING
                payment_group_id
        INTO
                o_payment_group_id;

	INSERT INTO payments (
		payment_group_id,
		payment_type,
		pay_amount -- XXX discount should be inserted here maybe?
	) VALUES (
		o_payment_group_id,
		i_payment_type,
		i_price
		);

        INSERT INTO pay_log (
                pay_type,
                code,
                amount,
                paid_at,
                token_id,
                payment_group_id,
                status
        ) VALUES (
                'save',
                o_pay_code,
                0,
                CURRENT_TIMESTAMP,
                NULL,
                o_payment_group_id,
                'SAVE'
        );

	INSERT INTO pay_log_references (
                        pay_log_id,
                        ref_num,
                        ref_type,
                        reference
                ) VALUES (
                        CURRVAL('pay_log_pay_log_id_seq'),
                        0,
                        'remote_addr',
                        host(i_remote_addr::inet)
                );

END;
$$ LANGUAGE plpgsql;
