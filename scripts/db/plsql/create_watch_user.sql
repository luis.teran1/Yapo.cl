CREATE OR REPLACE FUNCTION create_watch_user(OUT o_watch_unique_id watch_users.watch_unique_id%TYPE,
					     OUT o_watch_user_id watch_users.watch_user_id%TYPE) AS $$
BEGIN
	LOCK watch_users IN ROW EXCLUSIVE MODE;

	LOOP
		o_watch_unique_id := UUID();
		EXIT WHEN NOT EXISTS (SELECT * FROM watch_users WHERE watch_unique_id = o_watch_unique_id);
	END LOOP;

	INSERT INTO
		watch_users (watch_unique_id)
	VALUES
		(o_watch_unique_id)
	RETURNING
		watch_user_id
	INTO
		o_watch_user_id;
END
$$ LANGUAGE plpgsql;
