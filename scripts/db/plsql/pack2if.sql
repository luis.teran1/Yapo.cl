CREATE OR REPLACE FUNCTION pack2if(
    i_ad_id ads.ad_id%TYPE,
    i_action_id ad_actions.action_id%TYPE,
    i_token_id tokens.token_id%TYPE,
    i_remote_addr tokens.remote_addr%TYPE,
    i_service_order varchar,
    i_service_amount varchar,
    OUT out_action_id ad_actions.action_id%TYPE,
    OUT out_state_id action_states.state_id%TYPE,
    OUT out_list_time ads.list_time%TYPE
) AS $$
DECLARE
    l_pack_status varchar;
    l_ad_status ads.status%TYPE;
    l_orig_list_time ads.list_time%TYPE;
    l_state_action action_states.state%TYPE;
BEGIN
    SELECT ads.status, COALESCE(ap.value, ac.new_value), ads.list_time
    INTO l_ad_status, l_pack_status, l_orig_list_time
    FROM
        ads
        left join ad_params ap ON(ads.ad_id = ap.ad_id AND ap.name = 'pack_status')
        left join ad_changes ac ON(ads.ad_id = ac.ad_id AND ac.column_name = 'pack_status' and ac.action_id = 1)
    WHERE ads.ad_id = i_ad_id;

    IF NOT FOUND THEN
        RAISE EXCEPTION 'FACTORY_TRANSLATE:error:ERROR_AD_NOT_FOUND';
    END IF;

    IF l_pack_status != 'disabled' OR l_ad_status not in ('disabled','inactive') THEN
        RAISE EXCEPTION 'FACTORY_TRANSLATE:error:ERROR_INVALID_TRANSITION';
    END IF;

    IF i_action_id IS NOT NULL THEN
        out_action_id := i_action_id;
        SELECT st.state_id, st.state INTO out_state_id, l_state_action
        FROM ad_actions ac
            JOIN action_states st ON(ac.ad_id = st.ad_id AND ac.action_id = st.action_id AND ac.current_state = st.state_id)
        WHERE ac.ad_id = i_ad_id and ac.action_id = out_action_id;

        IF NOT FOUND THEN
            RAISE EXCEPTION 'FACTORY_TRANSLATE:error:ERROR_ACTION_NOT_FOUND';
        END IF;

        IF l_state_action != 'accepted' THEN
            RAISE EXCEPTION 'FACTORY_TRANSLATE:error:ERROR_ACTION_UNPAID';
        END IF;

    ELSE
        SELECT o_action_id, o_state_id INTO out_action_id, out_state_id
        FROM insert_ad_action_state(
            i_ad_id,
            'pack2if'::enum_ad_actions_action_type,
            'accepted'::enum_action_states_state,
            'status_change'::enum_action_states_transition,
            i_remote_addr,
            i_token_id
        );

        -- Save service order and amount as params
        IF i_service_order IS NOT NULL THEN
            INSERT INTO action_params (ad_id, action_id, name, value)
            VALUES (i_ad_id, out_action_id, 'service_order', i_service_order);
        END IF;

        IF i_service_amount IS NOT NULL THEN
            INSERT INTO action_params (ad_id, action_id, name, value)
            VALUES (i_ad_id, out_action_id, 'service_amount', i_service_amount);
        END IF;

    END IF;

    PERFORM insert_ad_change(i_ad_id, out_action_id, out_state_id, true, 'pack_status',null);
    PERFORM insert_ad_change(i_ad_id, out_action_id, out_state_id, true, 'inserting_fee','1');
    PERFORM apply_ad_changes(i_ad_id, out_action_id, NULL);

    out_list_time := date_trunc('second', CURRENT_TIMESTAMP);

    UPDATE
        ads
    SET
        status = CASE l_ad_status WHEN 'disabled' THEN 'active' ELSE status END,
        list_time = CASE l_ad_status WHEN 'disabled' THEN out_list_time ELSE list_time END,
        orig_list_time = l_orig_list_time,
        modified_at = CURRENT_TIMESTAMP
    WHERE
        ad_id = i_ad_id;

END
$$ LANGUAGE plpgsql;
