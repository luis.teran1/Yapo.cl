CREATE OR REPLACE FUNCTION adminedit(i_ad_id ad_actions.ad_id%TYPE,
				     i_action_id ad_actions.action_id%TYPE,
				     i_token tokens.token%TYPE,
				     i_remote_addr action_states.remote_addr%TYPE,
				     i_gallery_expire_minutes integer) RETURNS VOID AS $$
DECLARE
	l_state_id action_states.state_id%TYPE;
BEGIN
	PERFORM accept_action(i_ad_id, i_action_id, 'adminedit', i_remote_addr, i_token, i_gallery_expire_minutes,
			'adminedit', NULL);

	UPDATE
		ads
	SET
		modified_at = CURRENT_TIMESTAMP
	WHERE
		ad_id = i_ad_id;
END
$$ LANGUAGE plpgsql;
