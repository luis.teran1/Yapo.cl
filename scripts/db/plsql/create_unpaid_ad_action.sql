CREATE OR REPLACE FUNCTION create_unpaid_ad_action(i_action_type ad_actions.action_type%TYPE,
						i_parent_payment_group_id ad_actions.payment_group_id%TYPE,
				    		i_ad_id ads.ad_id%TYPE,
				    		i_price_name varchar[],
				    		i_price_amount integer[],
						i_remote_addr action_states.remote_addr%TYPE,
						i_token tokens.token%TYPE,
						i_pay_type pay_log.pay_type%TYPE,
						OUT o_action_id ad_actions.action_id%TYPE) AS $$
DECLARE
	l_payment_group_id ad_actions.payment_group_id%TYPE;
BEGIN
		-- Create unpaid payment
		SELECT
			o_payment_group_id
		INTO
			l_payment_group_id
		FROM
			payment_create(NULL,
				'unpaid',
				i_pay_type,
				i_price_name,
				i_price_amount,
				NULL,
				NULL,
				i_remote_addr,
				get_token_id(i_token),
				i_parent_payment_group_id
				);

		-- Create new ad_action
		o_action_id := insert_ad_action(i_ad_id, i_action_type, l_payment_group_id);
		PERFORM insert_state(i_ad_id,
					o_action_id, 
					'reg', 
					'initial', 
					i_remote_addr, 
					get_token_id(i_token));

		PERFORM insert_state(i_ad_id,
					o_action_id, 
					'unpaid', 
					'pending_pay', 
					i_remote_addr, 
					get_token_id(i_token));

			
	
END
$$ LANGUAGE plpgsql;
