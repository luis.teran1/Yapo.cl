CREATE OR REPLACE FUNCTION insert_purchase_in_app(
	i_receipt_date purchase_in_app.receipt_date%TYPE,
	i_email purchase_in_app.email%TYPE,
	i_is_company accounts.is_company%TYPE,
	i_external_receipt purchase_in_app.email%TYPE,
	i_status purchase_in_app.status%TYPE,
	i_product_id integer,
	i_product_price decimal,
	i_product_name varchar,
	i_ad_id integer,
	i_payment_method purchase.payment_method%TYPE,
	i_payment_platform purchase.payment_platform%TYPE,
	i_remote_addr purchase_in_app.remote_addr%TYPE,
	i_account_id accounts.account_id%TYPE,
	i_action_id purchase_in_app.action_id%TYPE,
	OUT o_purchase_id purchase_in_app.purchase_in_app_id%TYPE
) AS $$
DECLARE
	l_purchase_id	purchase_in_app.purchase_in_app_id%TYPE;
	l_region purchase_in_app.region%TYPE;
	l_commune purchase_in_app.communes%TYPE;
BEGIN

	IF i_product_id IS NULL THEN
		RAISE EXCEPTION 'FACTORY_TRANSLATE:error:ERROR_PRODUCT_ID_MISSING';
	END IF;
	IF i_product_price IS NULL THEN
		RAISE EXCEPTION 'FACTORY_TRANSLATE:error:ERROR_PRODUCT_PRICE_MISSING';
	END IF;
	IF i_product_name IS NULL THEN
		RAISE EXCEPTION 'FACTORY_TRANSLATE:error:ERROR_PRODUCT_NAME_MISSING';
	END IF;

	SELECT public.ads.region INTO l_region from ads where ad_id = i_ad_id;
	SELECT public.ad_params.value INTO l_commune from ad_params where ad_id = i_ad_id and name = 'communes';

	-- Insert the header of purchase
	INSERT INTO purchase_in_app(
		status, region, communes, email, payment_method, payment_platform,
		price, account_id, product_id, product_name, ad_id, receipt_date,
		external_receipt, remote_addr, is_company, action_id
	) VALUES (
		i_status, l_region, l_commune, i_email, i_payment_method, i_payment_platform,
		i_product_price, i_account_id, i_product_id, i_product_name, i_ad_id, i_receipt_date,
		i_external_receipt, i_remote_addr, i_is_company, i_action_id
	)
	RETURNING purchase_in_app_id
	INTO l_purchase_id;

	o_purchase_id = l_purchase_id;
END;
$$ LANGUAGE plpgsql;
