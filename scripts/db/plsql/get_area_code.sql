CREATE OR REPLACE FUNCTION get_area_code(
	i_commune integer,
	i_communes_conf integer[][],
	OUT o_code varchar
) AS $$
DECLARE
	l_conf_array integer[];
BEGIN
	o_code = 0;
	FOREACH l_conf_array SLICE 1 IN ARRAY i_communes_conf LOOP
		IF l_conf_array[1] = i_commune THEN
			o_code := l_conf_array[2]::varchar;
			EXIT; -- exit loop
		END IF;
	END LOOP;
END;
$$ LANGUAGE plpgsql;
