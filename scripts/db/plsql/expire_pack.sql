CREATE OR REPLACE FUNCTION expire_packs(
	i_pack_id INTEGER,
	i_pack_conf varchar[][],
	OUT o_account_id INTEGER,
	OUT o_email varchar,
	OUT o_slots_expired INTEGER,
	OUT o_available_slots INTEGER,
	OUT o_ads_disabled INTEGER
) RETURNS SETOF record AS $$
DECLARE
	l_rec_accounts record;
	l_rec_packs record;
	l_rec_ads record;
	l_user_id integer;
	l_type_categories integer[];
	l_available integer;
	l_to_expire integer;
	l_ads_to_disable integer;
	l_pack_id integer;
	l_pack_type packs.type%TYPE;
	l_available_per_pack integer;
	l_new_value integer;
BEGIN
	-- If there are pack_id then update the pack end date
	IF i_pack_id IS NOT NULL THEN
		UPDATE packs
		SET date_end = NOW()
		WHERE pack_id = i_pack_id;
	END IF;

	-- Iterate for each account_id/pack_type in expiration time,
	-- calculating how many slots going to expire
	FOR l_rec_accounts IN (
		SELECT
			p.account_id,
			a.user_id,
			a.email,
			p.type,
			sum(p.used) to_expire
		FROM packs p join accounts a USING(account_id)
		WHERE p.status = 'active' AND date_end <= NOW()
		AND p.pack_id = COALESCE(i_pack_id, p.pack_id)
		GROUP BY account_id, user_id, email, type
	)
	LOOP
		o_account_id := l_rec_accounts.account_id;
		o_slots_expired := l_rec_accounts.to_expire;
		o_email := l_rec_accounts.email;
		l_user_id := l_rec_accounts.user_id;
		l_pack_type := l_rec_accounts.type;
		l_to_expire := l_rec_accounts.to_expire;
		
		-- Get the availables
		SELECT SUM(slots) - SUM(used) INTO l_available
		FROM packs WHERE account_id = o_account_id AND status = 'active' AND type = l_pack_type AND date_end > NOW();
		IF l_available IS NULL THEN
			l_available := 0;
		END IF;

		-- Expire packs
		UPDATE packs SET status = 'expired', used = 0
		WHERE account_id = o_account_id AND status = 'active' AND type = l_pack_type AND date_end <= NOW() ;


		-- Calculate the ads to disable
		l_ads_to_disable := l_to_expire - l_available;
			
		RAISE NOTICE 'ads to disable %, slots to expire  %, disponible %', l_ads_to_disable, l_to_expire, l_available;

		-- Use the available slots to
		-- if the slots available are not enough to complete the expired slots
		-- we disable the diferent on ads.
		FOR l_rec_packs IN (
			SELECT pack_id, slots, used
			FROM packs
			WHERE account_id = o_account_id AND status = 'active' AND date_end >= NOW() AND slots - used > 0 AND type = l_pack_type
			ORDER BY date_start ASC FOR UPDATE
		)
		LOOP
			l_available_per_pack := l_rec_packs.slots - l_rec_packs.used; -- this is the available slots on this pack
			l_pack_id := l_rec_packs.pack_id;
			l_new_value := l_rec_packs.slots;
			-- if the rest to expire is bigger than the available on the pack
				-- use the available on the pack
			-- else
				-- use the expired rest of slots

			RAISE NOTICE ' pack_id: % slots:% available%', l_pack_id, l_new_value, l_available_per_pack;

			IF l_to_expire >= l_available_per_pack THEN
				l_to_expire := l_to_expire - l_available_per_pack;
				UPDATE packs SET used = l_new_value WHERE pack_id = l_pack_id;
				RAISE NOTICE 'ENTRE EN EL IF %', l_to_expire;
			ELSE
				l_new_value := l_rec_packs.used + l_to_expire;
				UPDATE packs SET used = l_new_value WHERE pack_id = l_pack_id;
				l_to_expire := 0;
				RAISE NOTICE 'ENTRE EN EL ELSE %', l_to_expire;
				EXIT; -- break the for
			END IF;
		END LOOP;
		
		-- Disable the ads
		o_ads_disabled := 0;
		IF  l_ads_to_disable > 0 THEN
			l_type_categories := find_pack_categories(l_pack_type::varchar, i_pack_conf);
			FOR l_rec_ads IN (
				SELECT a.ad_id, category
				FROM ads a join ad_params p ON (a.ad_id = p.ad_id AND p.name = 'pack_status')
				WHERE a.user_id = l_user_id AND a.status = 'active' AND p.value = 'active' AND category = ANY( l_type_categories )
				ORDER BY list_time ASC
				LIMIT l_ads_to_disable
			)
			LOOP
				PERFORM ad_status_change(l_rec_ads.ad_id, 'disabled'::enum_ads_status, '0.0.0.0', null);
				o_ads_disabled := o_ads_disabled + 1;
			END LOOP;
		END IF;

		-- Get the new available slots
		SELECT COALESCE(value::integer, 0)
		INTO o_available_slots
		FROM account_params
		WHERE account_id = o_account_id and name = 'available_slots';

		RETURN NEXT;
	END LOOP;

END
$$ LANGUAGE plpgsql;
