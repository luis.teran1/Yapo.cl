CREATE OR REPLACE FUNCTION refuse_reasons_action_type(ad_actions.action_type%TYPE,
						      i_start_time action_states.timestamp%TYPE,
					              i_end_time action_states.timestamp%TYPE,
						      OUT o_count BIGINT,
						      OUT o_refuse_reason state_params.value%TYPE) RETURNS SETOF RECORD AS $$
	SELECT 
		COUNT(*), 
		value AS refuse_reason
	FROM 
		action_states JOIN 
		state_params USING (ad_id, action_id, state_id) JOIN
		ad_actions USING (ad_id, action_id)
	WHERE 
		timestamp > $2 AND 
		timestamp < $3 AND 
		name = 'reason' AND
		action_type = $1
	GROUP BY 
		refuse_reason
	ORDER BY 
		count DESC;
$$ LANGUAGE sql;
