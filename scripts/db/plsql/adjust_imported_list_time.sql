CREATE OR REPLACE FUNCTION adjust_imported_list_time(i_link_type ad_params.value%TYPE,
		i_store_id ads.store_id%TYPE,
		i_list_time ads.list_time%TYPE,
		i_link_type_group_check_whitelist varchar[],
		o_list_time OUT ads.list_time%TYPE) AS $$
DECLARE
	l_max_hour integer;
	l_list_time_hour integer;
	l_group_check bool;
BEGIN
	l_group_check := TRUE;

	IF i_link_type_group_check_whitelist IS NOT NULL AND i_link_type = ANY (i_link_type_group_check_whitelist) THEN
		l_group_check := FALSE;
	END IF;

	IF i_list_time IS NULL THEN
		o_list_time := CURRENT_TIMESTAMP;
	ELSE
		o_list_time := i_list_time;
	END IF;

	IF l_group_check AND date_trunc('day', o_list_time) = CURRENT_DATE THEN
		-- if list_time is within 5 minutes of another ad from same store,
		-- shuffle it to avoid multiple ads listed next to eachother.
		-- XXX somewhat heavy (0.2 s) when i_store_id IS NULL
		IF EXISTS (
			SELECT
				*
			FROM
				ads
				JOIN ad_params AS link_type ON
					link_type.ad_id = ads.ad_id
					AND link_type.name = 'link_type'
			WHERE
				link_type.value = i_link_type
				AND (i_store_id IS NULL OR i_store_id = ads.store_id)
				AND ads.status = 'active'
				AND list_time BETWEEN o_list_time - INTERVAL '5 min' AND o_list_time + INTERVAL '5 min'
				) THEN
			o_list_time := date_trunc('day', o_list_time); -- Will be shuffled below.
		END IF;
	ELSIF CURRENT_TIMESTAMP - o_list_time >= interval '50 days' THEN
		-- Bump old ads.
		o_list_time := (CURRENT_DATE - interval '15 days')::timestamp;
	END IF;

	-- Imported ads are only allowed to have list_times between 07:00 and 23:59
	l_list_time_hour = EXTRACT('hour' FROM o_list_time);
	IF l_list_time_hour < 7 THEN
		IF date_trunc('day', o_list_time) = CURRENT_DATE THEN
			l_max_hour := EXTRACT('hour' FROM CURRENT_TIMESTAMP);
			IF l_max_hour <= 7 THEN -- Seems to be running an import before 08:00
				l_max_hour := 23;
				o_list_time := o_list_time - INTERVAL '1 day';
			END IF;
		ELSE
			l_max_hour := 23;
		END IF;
		o_list_time := date_trunc('second', o_list_time
				- INTERVAL '1 hour' * l_list_time_hour
				+ INTERVAL '1 hour' * (7 + RANDOM() * (l_max_hour - 7)));
	END IF;

END;
$$ LANGUAGE plpgsql;
