CREATE OR REPLACE FUNCTION _group_concat_ordered_final(text[]) RETURNS text AS $$
DECLARE
	l_res text;
	l_r record;
BEGIN
	FOR l_r IN SELECT _split_array FROM _split_array($1) ORDER BY 1 LOOP
		IF l_res IS NULL THEN
			l_res := l_r._split_array;
		ELSE
			l_res := l_res || ',' || l_r._split_array;
		END IF;
	END LOOP;

	RETURN l_res;
END;
$$ LANGUAGE plpgsql;

DROP AGGREGATE IF EXISTS group_concat_ordered(text);
CREATE AGGREGATE group_concat_ordered (
	BASETYPE = text,
	SFUNC = array_append,
	STYPE = text[],
	FINALFUNC = _group_concat_ordered_final,
	INITCOND='{}'
);
