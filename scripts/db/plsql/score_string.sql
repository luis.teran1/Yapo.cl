CREATE OR REPLACE FUNCTION score_string(i_source TEXT, i_other TEXT) 
RETURNS INTEGER AS $$
DECLARE
	l_source TEXT;
	l_other TEXT;
	l_lv INTEGER;
	l_size INTEGER;
BEGIN
	-- returns a value form 0 to 100 where 0 is completly different and 100 is total equal
	IF (i_source = i_other) THEN
		return 100;
	END IF;
	
	l_source := substring(i_source from 0 for 255);
	l_other := substring(i_other from 0 for 255);
	l_size = char_length(l_source);
	
	l_lv := levenshtein(l_source, l_other);
	
	return - (l_lv - l_size)*100/l_size;
	-- return ((l_size-l_lv)*100)/l_size;
END
$$ LANGUAGE plpgsql;
