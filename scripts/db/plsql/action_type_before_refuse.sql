CREATE OR REPLACE FUNCTION action_type_before_refuse(i_ad_id ad_actions.ad_id%TYPE,
					 	     i_action_id ad_actions.action_id%TYPE, 
						     i_schema text,
					             OUT o_action_id ad_actions.action_id%TYPE,
					             OUT o_action_type ad_actions.action_type%TYPE) AS $$
DECLARE
	l_prev_action_id ad_actions.action_id%TYPE;
	l_loop_counter integer;
	l_schema text;
BEGIN
	o_action_id := i_action_id;
	l_loop_counter := 0;

	IF i_ad_id IS NULL OR i_action_id IS NULL THEN
		RETURN;
	END IF;

	l_schema := COALESCE(i_schema, 'public');

	EXECUTE
		'SELECT
			action_type
		FROM
			' || quote_ident(l_schema) || '.ad_actions
		WHERE
			ad_id = ' || quote_literal(i_ad_id) || ' AND
			action_id = ' || quote_literal(i_action_id)
	INTO
		o_action_type;

	WHILE o_action_type = 'editrefused' LOOP
		l_prev_action_id := NULL;
		EXECUTE
			'SELECT
				value
			FROM
				' || quote_ident(l_schema) || '.action_params
			WHERE
				ad_id = ' || quote_literal(i_ad_id) || ' AND
				action_id = ' || quote_literal(o_action_id) || ' AND
				name = ''prev_action_id'''
		INTO
			l_prev_action_id;
		
		IF l_prev_action_id IS NULL THEN
			o_action_type := NULL;
			RETURN;
		END IF;

		o_action_type := NULL;
		EXECUTE
			'SELECT
				action_type
			FROM
				' || quote_ident(l_schema) || '.ad_actions
			WHERE
				ad_id = ' || quote_literal(i_ad_id) || ' AND
				action_id = ' || quote_literal(l_prev_action_id)
		INTO
			o_action_type;

		IF o_action_type IS NULL THEN
			RAISE EXCEPTION 'action_type_before_refuse failed (action_type)';
		END IF;
	
		o_action_id := l_prev_action_id;
		l_loop_counter := l_loop_counter + 1;


		IF l_loop_counter > 200 THEN
			RAISE EXCEPTION 'action_type_before_refuse failed (loop)';
		END IF;

	END LOOP;

END
$$ LANGUAGE plpgsql;
