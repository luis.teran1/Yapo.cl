CREATE OR REPLACE FUNCTION adwatch_sms_stop (
		i_sender sms_users.phone%TYPE,
		i_watch_unique_id watch_users.watch_unique_id%TYPE,
		i_watch_query_id watch_queries.watch_query_id%TYPE,
		i_token_id tokens.token_id%TYPE,
		OUT o_sms_user_id sms_users.sms_user_id%TYPE,
		OUT o_num_sms_queries_removed integer, 
		OUT o_sms_log_id sms_log.sms_log_id%TYPE 
		) AS $$
DECLARE
	l_watch_user_id watch_queries.watch_user_id%TYPE;
BEGIN
	IF i_sender IS NOT NULL THEN
		SELECT
			sms_user_id
		FROM
			sms_users
		WHERE
			phone = i_sender
		INTO
			o_sms_user_id;

		IF NOT FOUND THEN
			RAISE EXCEPTION 'ERROR_PHONE_NOT_FOUND'; 
		END IF;

		UPDATE
			watch_queries
		SET	
			sms_user_id = NULL,
			sms_status = NULL
		WHERE
			sms_user_id = o_sms_user_id; 
	ELSE
		SELECT
			watch_user_id
		FROM
			watch_users
		WHERE
			watch_unique_id = i_watch_unique_id
		INTO
			l_watch_user_id;

		IF NOT FOUND THEN
			RAISE EXCEPTION 'ERROR_WATCH_USER_NOT_FOUND'; 
		END IF;

		IF i_watch_query_id IS NULL THEN
			UPDATE
				watch_queries
			SET	
				sms_user_id = NULL,
				sms_status = NULL
			WHERE
				watch_user_id = l_watch_user_id;
		ELSE
			SELECT
				sms_user_id
			INTO
				o_sms_user_id
			FROM
				watch_queries
			WHERE
				watch_user_id = l_watch_user_id AND watch_query_id = i_watch_query_id;
				
			UPDATE
				watch_queries
			SET	
				sms_user_id = NULL,
				sms_status = NULL
			WHERE
				watch_user_id = l_watch_user_id AND watch_query_id = i_watch_query_id;
		END IF;
	END IF;

	GET DIAGNOSTICS o_num_sms_queries_removed = ROW_COUNT;

	IF i_token_id IS NOT NULL THEN
		PERFORM log_event ('adwatch_sms_stop', 'watch_user_id:' || COALESCE(l_watch_user_id::text, 'NULL') ||
				' watch_query_id:' || COALESCE(i_watch_query_id::text, 'NULL') ||
				' sms_user_id:' || COALESCE(o_sms_user_id::text, 'NULL'), i_token_id);
	END IF;
END
$$ LANGUAGE plpgsql;

