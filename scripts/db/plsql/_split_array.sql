CREATE OR REPLACE FUNCTION _split_array(text[]) RETURNS SETOF text AS $$
DECLARE
	l_i INTEGER;
BEGIN
	l_i := 1;
	LOOP
		EXIT WHEN $1[l_i] IS NULL;
		RETURN NEXT $1[l_i];
		l_i := l_i + 1;
	END LOOP;
	RETURN;
END;
$$ LANGUAGE plpgsql;
