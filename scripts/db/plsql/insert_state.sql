CREATE OR REPLACE FUNCTION insert_state(i_ad_id ads.ad_id%TYPE, 
					i_action_id ad_actions.action_id%TYPE,
					i_state action_states.state%TYPE,
					i_transition action_states.transition%TYPE,
					i_remote_addr action_states.remote_addr%TYPE,
					i_token_id action_states.token_id%TYPE) RETURNS action_states.state_id%TYPE AS $$
DECLARE
	l_current_state action_states.state%TYPE;
	l_action_type ad_actions.action_type%TYPE;
	l_token tokens.token%TYPE;
	l_admin_id tokens.admin_id%TYPE;

BEGIN
	SELECT
		state,
		action_type
	INTO
		l_current_state,
		l_action_type
	FROM
		ad_actions
	WHERE
		ad_actions.ad_id = i_ad_id AND
		ad_actions.action_id = i_action_id;

	IF NOT FOUND AND i_state NOT IN ('reg', 'deleted', 'undo_deleted') THEN
		RAISE EXCEPTION 'ERROR_NO_SUCH_ACTION';
	END IF;

	SELECT
		token
	INTO
		l_token
	FROM
		tokens
	WHERE
		tokens.token_id = i_token_id;

	IF i_state IN ('accepted') AND l_action_type NOT IN ('bump', 'gallery', 'gallery_1', 'gallery_30', 'weekly_bump', 'daily_bump', 'upselling_gallery', 'upselling_weekly_bump', 'upselling_daily_bump', 'label', 'upselling_label', 'autofact', 'inserting_fee', 'if_pack_car', 'if_pack_inmo', 'at_combo1', 'at_combo2', 'at_combo3', 'discount_combo1', 'discount_combo2', 'discount_combo3', 'discount_combo4', 'promo_bump', 'if_pack_car_premium' ) THEN
		IF l_current_state IN ('refused') THEN
			--CHECK PRIVS
			SELECT
				tokens.admin_id
			INTO
				l_admin_id
			FROM
				tokens
			JOIN
				admin_privs ON (tokens.admin_id = admin_privs.admin_id)
			WHERE
				token = l_token AND
				priv_name = 'adqueue.approve_refused';

			IF NOT FOUND THEN
				RAISE EXCEPTION 'ERROR_INSUFFICIENT_PRIVS';
			END IF;
		END IF;
	END IF;

	IF i_state IN ('accepted', 'refused') AND l_action_type NOT IN ('bump', 'autobump', 'gallery', 'gallery_1', 'gallery_30', 'weekly_bump', 'daily_bump', 'upselling_gallery', 'upselling_weekly_bump', 'upselling_daily_bump', 'label', 'upselling_label', 'upselling_bump', 'upselling_standard_cars', 'upselling_advanced_cars', 'upselling_premium_cars', 'upselling_standard_inmo', 'upselling_advanced_inmo', 'upselling_premium_inmo', 'upselling_standard_others', 'upselling_advanced_others', 'upselling_premium_others', 'autofact', 'inserting_fee', 'if_pack_car', 'if_pack_inmo', 'at_combo1', 'at_combo2', 'at_combo3', 'discount_combo1', 'discount_combo2', 'discount_combo3', 'discount_combo4', 'promo_bump', 'if_pack_car_premium') THEN
		IF NOT l_current_state IN ('reg', 'pending_review', 'locked', 'refused', 'undo_deleted') THEN
			RAISE EXCEPTION 'ERROR_INVALID_STATE';
		END IF;
	END IF;

	INSERT INTO
		action_states
		(ad_id,
		 action_id,
		 state,
		 transition,
		 timestamp,
		 remote_addr,
		 token_id)
	VALUES
		(i_ad_id,
		 i_action_id,
		 i_state,
		 i_transition,
		 now(),
		 i_remote_addr,
		 i_token_id);

	-- Update the action to point to the newly added state
	UPDATE
		ad_actions
	SET
		current_state = CURRVAL('action_states_state_id_seq'),
		state = i_state
	WHERE
		ad_id = i_ad_id AND
		action_id = i_action_id;
	
	IF NOT FOUND THEN
		RAISE EXCEPTION 'ERROR_NO_SUCH_ACTION';
	END IF;
	
	RETURN CURRVAL('action_states_state_id_seq');
END;
$$ LANGUAGE plpgsql;
