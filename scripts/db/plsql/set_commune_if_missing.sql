CREATE OR REPLACE FUNCTION set_commune_if_missing(
			i_email accounts.email%TYPE,
			i_region accounts.region%TYPE,
			i_commune accounts.commune%TYPE) RETURNS BOOLEAN AS $$
BEGIN
	UPDATE
		accounts
	SET
		commune = i_commune
	WHERE
		email = lower(i_email)
		AND region = i_region
		AND commune IS NULL;

	return FOUND;
END;
$$ LANGUAGE plpgsql;
