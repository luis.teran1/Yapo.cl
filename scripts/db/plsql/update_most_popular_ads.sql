CREATE OR REPLACE FUNCTION update_most_popular_ads(
		i_list_id integer[],
		i_quantity integer[],
		i_type varchar[],
		i_stat_time mail_log.sent_at%TYPE) RETURNS VOID AS $$
DECLARE
	l_i integer;
	l_type varchar;
	l_list_id integer;
	l_quantity integer;	
	l_adreplies integer;	
	l_rec record;
BEGIN
	IF i_type IS NOT NULL THEN 
		DELETE FROM most_popular_ads;
		l_i := 1; -- Arrays in postgresql are 1-based
		LOOP
			
			l_type := i_type[l_i];
			l_list_id := i_list_id[l_i];
			l_quantity := i_quantity[l_i];
			EXIT WHEN l_type IS NULL;

			IF NOT EXISTS (SELECT
					list_id
				   FROM
					most_popular_ads
				   WHERE
					list_id = l_list_id)
			THEN
				INSERT INTO
					most_popular_ads
					(list_id,
					quantity,
					type)
				VALUES
					(l_list_id,
					l_quantity,
					l_type::enum_most_popular_aspect);

				SELECT 
					count(*) 
				INTO 
					l_adreplies 
				FROM 
					mail_log 
				WHERE 
					mail_type='adreply' 
					AND sent_at >= i_stat_time
					AND sent_at < i_stat_time::timestamp + INTERVAL '1 day'
					AND list_id = l_list_id; 
			
				IF NOT FOUND THEN
					l_adreplies := 0;
				END IF;
	
				INSERT INTO
					most_popular_ads
					(list_id,
					quantity,
					type)
				VALUES
					(l_list_id,
					l_adreplies,
					'adreplies'::enum_most_popular_aspect);

			END IF;
			l_i := l_i + 1;
		END LOOP;
	END IF;
END;
$$ LANGUAGE plpgsql;

