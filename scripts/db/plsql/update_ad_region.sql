CREATE OR REPLACE FUNCTION update_ad_region(
    i_ad_id integer,
    i_from_region integer,
    i_to_region integer,
    i_remote_addr character varying,
    i_token_id character varying)
    RETURNS void AS $$

DECLARE
    l_action_id ad_actions.action_id%TYPE;
    l_state_id action_states.state_id%TYPE;
    l_current_state ad_actions.state%TYPE;

BEGIN
    SELECT
        o_action_id,
        o_state_id
    INTO
        l_action_id,
        l_state_id
    FROM insert_ad_action_state(
        i_ad_id::integer,
        'move_to_region'::enum_ad_actions_action_type,
        'reg'::enum_action_states_state,
        'initial'::enum_action_states_transition,
        i_remote_addr::varchar,
        i_token_id::integer);
    
    -- Insert into state params an initial record to log the action of changing from/to a new region
    INSERT INTO state_params
        VALUES (
            i_ad_id,
            l_action_id,
            l_state_id,
            'previous_region',
            i_from_region);

    l_state_id := insert_state(i_ad_id, l_action_id, 'accepted', 'adminclear', i_remote_addr, NULL);
    -- Apply ad_changes
    PERFORM insert_ad_change(i_ad_id, l_action_id, l_state_id, false, 'region'::varchar, i_to_region::varchar);                                                                                                                                                                                          
    PERFORM apply_ad_changes(i_ad_id, l_action_id, NULL);
    RAISE NOTICE 'Ad Id: (%) successfully changed', i_ad_id;
END;
$$ LANGUAGE plpgsql;
