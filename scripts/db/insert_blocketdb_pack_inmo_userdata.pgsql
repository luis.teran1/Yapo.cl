INSERT INTO users (user_id, uid, email, account, paid_total) VALUES (60, 60, 'carlos@schibsted.cl', 0, 0);
INSERT INTO users (user_id, uid, email, account, paid_total) VALUES (61, 61, 'no_pack_no_account@yapo.cl', 0, 0);

INSERT INTO accounts (account_id, name, rut, email, phone, is_company, region, salted_passwd, creation_date, activation_date, status, address, contact, lob, commune, user_id, phone_hidden) VALUES (8, 'carlos', '', 'carlos@schibsted.cl', '12312312', true, 15, '$1024$Z]Ws?DTXpU_ [4pd0f0c19cbae7972b6eb22b1dc9dbc4a5ac507f43d', '2016-02-01 11:04:05.810173', '2016-02-01 11:07:24.282953', 'active', NULL, NULL, NULL, 321, 60, false);

INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (801, 'X56af66ae1e763ee1000000001a317e2500000000', 9, '2016-02-01 11:07:41.8595', '2016-02-01 11:07:41.985755', '10.0.1.199', 'authenticate', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (802, 'X56af66ae2f5f42eb000000002973af0400000000', 9, '2016-02-01 11:07:41.985755', '2016-02-01 11:07:56.844098', '10.0.1.199', 'spamfilter', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (803, 'X56af66bd640e11c900000000c97967b00000000', 9, '2016-02-01 11:07:56.844098', '2016-02-01 11:07:59.832016', '10.0.1.199', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (804, 'X56af66c061bb53dc00000000a5f002c00000000', 9, '2016-02-01 11:07:59.832016', '2016-02-01 11:07:59.868469', '10.0.1.199', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (805, 'X56af66c04c0050e6000000001b6c96c600000000', 9, '2016-02-01 11:07:59.868469', '2016-02-01 11:07:59.871663', '10.0.1.199', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (806, 'X56af66c066507b2e000000001e28ed1400000000', 9, '2016-02-01 11:07:59.871663', '2016-02-01 11:07:59.874635', '10.0.1.199', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (807, 'X56af66c03ec7eedc0000000037fd796f00000000', 9, '2016-02-01 11:07:59.874635', '2016-02-01 11:08:04.734782', '10.0.1.199', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (808, 'X56af66c55fb12b1c00000000c289aef00000000', 9, '2016-02-01 11:08:04.734782', '2016-02-01 11:09:24.221657', '10.0.1.199', 'review', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (809, 'X56af671438be6a50000000069c7411d00000000', 9, '2016-02-01 11:09:24.221657', '2016-02-01 11:09:25.525969', '10.0.1.199', 'spamfilter', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (810, 'X56af67167b3c751c000000003bdc455700000000', 9, '2016-02-01 11:09:25.525969', '2016-02-01 11:09:28.028514', '10.0.1.199', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (811, 'X56af67186bea8a10000000006b057e4f00000000', 9, '2016-02-01 11:09:28.028514', '2016-02-01 11:09:28.073898', '10.0.1.199', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (812, 'X56af67186b43397b0000000033615e4d00000000', 9, '2016-02-01 11:09:28.073898', '2016-02-01 11:09:28.076663', '10.0.1.199', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (813, 'X56af6718494a4f830000000015a23c7800000000', 9, '2016-02-01 11:09:28.076663', '2016-02-01 11:09:28.079409', '10.0.1.199', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (814, 'X56af6718419ac757000000005cae1f3900000000', 9, '2016-02-01 11:09:28.079409', '2016-02-01 11:09:31.621654', '10.0.1.199', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (815, 'X56af671c7838e6790000000069e4030f00000000', 9, '2016-02-01 11:09:31.621654', '2016-02-01 11:09:35.630227', '10.0.1.199', 'review', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (816, 'X56af672063ce570c00000000460d13100000000', 9, '2016-02-01 11:09:35.630227', '2016-02-01 11:10:38.869821', '10.0.1.199', 'review', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (817, 'X56af675f2d16a9f4000000007beedfc00000000', 9, '2016-02-01 11:10:38.869821', '2016-02-01 11:10:40.66667', '10.0.1.199', 'spamfilter', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (818, 'X56af676173efc0c80000000074a038ef00000000', 9, '2016-02-01 11:10:40.66667', '2016-02-01 11:10:42.582072', '10.0.1.199', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (819, 'X56af67632871cd0a000000002105cf0900000000', 9, '2016-02-01 11:10:42.582072', '2016-02-01 11:10:42.602203', '10.0.1.199', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (820, 'X56af67633a257eea00000000f2330ff00000000', 9, '2016-02-01 11:10:42.602203', '2016-02-01 11:10:42.604902', '10.0.1.199', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (821, 'X56af6763274e187b00000000435e5d5400000000', 9, '2016-02-01 11:10:42.604902', '2016-02-01 11:10:42.60743', '10.0.1.199', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (822, 'X56af67631411a8550000000010997f6d00000000', 9, '2016-02-01 11:10:42.60743', '2016-02-01 11:10:45.931761', '10.0.1.199', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (823, 'X56af67665ebd4cae000000005826bc0600000000', 9, '2016-02-01 11:10:45.931761', '2016-02-01 11:12:46.24063', '10.0.1.199', 'review', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (824, 'X56af67de658d2c60000000004e6dcc5700000000', 9, '2016-02-01 11:12:46.24063', '2016-02-01 11:12:47.833115', '10.0.1.199', 'spamfilter', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (825, 'X56af67e03cca45c40000000011b61f4000000000', 9, '2016-02-01 11:12:47.833115', '2016-02-01 11:12:49.277196', '10.0.1.199', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (826, 'X56af67e141120cdf000000003e1d7fe00000000', 9, '2016-02-01 11:12:49.277196', '2016-02-01 11:12:49.308439', '10.0.1.199', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (827, 'X56af67e175a4472b0000000034fd83000000000', 9, '2016-02-01 11:12:49.308439', '2016-02-01 11:12:49.311556', '10.0.1.199', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (828, 'X56af67e16e54ab2a000000002fcd0eb300000000', 9, '2016-02-01 11:12:49.311556', '2016-02-01 11:12:49.314791', '10.0.1.199', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (829, 'X56af67e12e049537000000006e297bba00000000', 9, '2016-02-01 11:12:49.314791', '2016-02-01 11:12:53.502985', '10.0.1.199', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (830, 'X56af67e6151c025e00000000752a5be800000000', 9, '2016-02-01 11:12:53.502985', '2016-02-01 11:13:56.291784', '10.0.1.199', 'review', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (831, 'X56af682432c496370000000017f4f13e00000000', 9, '2016-02-01 11:13:56.291784', '2016-02-01 11:13:58.695392', '10.0.1.199', 'spamfilter', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (832, 'X56af682765e44eea00000000380a538a00000000', 9, '2016-02-01 11:13:58.695392', '2016-02-01 12:13:58.695392', '10.0.1.199', 'adqueues', NULL, NULL);

INSERT INTO account_actions (account_id, account_action_id, account_action_type, "timestamp", token_id) VALUES (8, 1, 'creation', '2016-02-01 11:04:05.810173', NULL);
INSERT INTO account_actions (account_id, account_action_id, account_action_type, "timestamp", token_id) VALUES (8, 2, 'activation', '2016-02-01 11:07:24.282953', NULL);
INSERT INTO account_actions (account_id, account_action_id, account_action_type, "timestamp", token_id) VALUES (8, 3, 'add_pro_category_param', '2016-02-01 11:12:53.526224', NULL);
INSERT INTO account_actions (account_id, account_action_id, account_action_type, "timestamp", token_id) VALUES (8, 4, 'add_pro_category_param', '2016-02-01 11:13:49.327566', NULL);

INSERT INTO account_params (account_id, name, value) VALUES (8, 'is_pro_for', '1220,1240');
INSERT INTO account_params (account_id, name, value) VALUES (8, 'pack_type', 'real_estate');

SELECT pg_catalog.setval('accounts_account_id_seq', 8, true);

INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (224, 8000104, '2016-02-01 11:12:53', 'active', 'sell', 'carlos', '12312312', 15, 0, 1220, 60, NULL, false, false, true, 'Casa 5', 'casa 5', 123123, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$Z]Ws?DTXpU_ [4pd0f0c19cbae7972b6eb22b1dc9dbc4a5ac507f43d', '2016-02-01 11:12:53.505434', 'es');
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (223, 8000103, '2016-02-01 11:10:45', 'active', 'sell', 'carlos', '12312312', 15, 0, 1220, 60, NULL, false, false, true, 'Casa 4', 'casa 4', 123123, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$Z]Ws?DTXpU_ [4pd0f0c19cbae7972b6eb22b1dc9dbc4a5ac507f43d', '2016-02-01 11:10:45.934089', 'es');
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (222, 8000102, '2016-02-01 11:09:35', 'active', 'sell', 'carlos', '12312312', 15, 0, 1220, 60, NULL, false, false, true, 'Casa 3', 'casa 3', 123123, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$Z]Ws?DTXpU_ [4pd0f0c19cbae7972b6eb22b1dc9dbc4a5ac507f43d', '2016-02-01 11:09:35.632126', 'es');
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (221, 8000101, '2016-02-01 11:09:31', 'active', 'sell', 'carlos', '12312312', 15, 0, 1220, 60, NULL, false, false, true, 'Casa 2', 'casa 2', 123123, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$Z]Ws?DTXpU_ [4pd0f0c19cbae7972b6eb22b1dc9dbc4a5ac507f43d', '2016-02-01 11:09:31.624146', 'es');
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (220, 8000100, '2016-02-01 11:08:04', 'active', 'sell', 'carlos', '12312312', 15, 0, 1220, 60, NULL, false, false, true, 'Casa 1', 'casa 1', 123123, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$Z]Ws?DTXpU_ [4pd0f0c19cbae7972b6eb22b1dc9dbc4a5ac507f43d', '2016-02-01 11:08:04.737026', 'es');
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (225, 8000105, '2016-02-01 11:13:49', 'disabled', 'sell', 'carlos', '12312312', 15, 0, 1220, 60, NULL, false, false, true, 'Casa 6', 'casa 6', 123123, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$Z]Ws?DTXpU_ [4pd0f0c19cbae7972b6eb22b1dc9dbc4a5ac507f43d', '2016-02-01 11:13:49.312337', 'es');
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (226, 8000106, '2016-02-01 11:13:49', 'disabled', 'sell', 'NPNA', '12312312', 15, 0, 1220, 61, NULL, false, false, true, 'Casa 666', 'casa 666', 123123, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$Z]Ws?DTXpU_ [4pd0f0c19cbae7972b6eb22b1dc9dbc4a5ac507f43d', '2016-02-01 11:13:49.312337', 'es');

INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (260, '112200000', 'verified', '2016-02-01 11:04:05.772146', NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (261, '112200001', 'verified', '2016-02-01 11:08:45.022714', NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (262, '112200002', 'verified', '2016-02-01 11:09:19.229531', NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (263, '112200003', 'verified', '2016-02-01 11:10:24.094659', NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (264, '112200004', 'verified', '2016-02-01 11:12:34.910933', NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (265, '112200005', 'verified', '2016-02-01 11:13:36.223249', NULL);

INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (220, 1, 'new', 704, 'accepted', 'first_da', 9, '2016-02-01 11:12:59.834416', 260);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (221, 1, 'new', 713, 'accepted', 'normal', 9, '2016-02-01 11:14:28.030557', 261);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (222, 1, 'new', 714, 'accepted', 'normal', 9, '2016-02-01 11:14:28.030557', 262);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (223, 1, 'new', 719, 'accepted', 'normal', 9, '2016-02-01 11:15:42.583532', 263);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (224, 1, 'new', 724, 'accepted', 'normal', 9, '2016-02-01 11:17:49.279491', 264);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (224, 2, 'account_to_pro', 726, 'accepted', 'normal', NULL, NULL, NULL);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (223, 2, 'account_to_pro', 728, 'accepted', 'normal', NULL, NULL, NULL);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (222, 2, 'account_to_pro', 730, 'accepted', 'normal', NULL, NULL, NULL);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (221, 2, 'account_to_pro', 732, 'accepted', 'normal', NULL, NULL, NULL);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (220, 2, 'account_to_pro', 34, 'accepted', 'normal', NULL, NULL, NULL);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (225, 1, 'new', 738, 'accepted', 'pack', NULL, NULL, 265);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (226, 1, 'account_to_pro', 735, 'accepted', 'normal', NULL, NULL, NULL);

INSERT INTO action_params (ad_id, action_id, name, value) VALUES (220, 1, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (220, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (221, 1, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (221, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (222, 1, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (222, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (223, 1, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (223, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (224, 1, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (224, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (225, 1, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (225, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (225, 1, 'pack_result', '3');

INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (220, 1, 700, 'reg', 'initial', '2016-02-01 11:04:05.772146', '10.0.1.199', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (220, 1, 701, 'unverified', 'verifymail', '2016-02-01 11:04:05.772146', '10.0.1.199', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (220, 1, 702, 'pending_review', 'verify', '2016-02-01 11:04:05.825712', '10.0.1.199', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (220, 1, 703, 'locked', 'checkout', '2016-02-01 11:07:59.834416', '10.0.1.199', 803);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (220, 1, 704, 'accepted', 'accept', '2016-02-01 11:08:04.737026', '10.0.1.199', 807);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (221, 1, 705, 'reg', 'initial', '2016-02-01 11:08:45.022714', '10.0.1.199', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (221, 1, 706, 'unverified', 'verifymail', '2016-02-01 11:08:45.022714', '10.0.1.199', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (221, 1, 707, 'pending_review', 'verify', '2016-02-01 11:08:45.048322', '10.0.1.199', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (222, 1, 708, 'reg', 'initial', '2016-02-01 11:09:19.229531', '10.0.1.199', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (222, 1, 709, 'unverified', 'verifymail', '2016-02-01 11:09:19.229531', '10.0.1.199', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (222, 1, 710, 'pending_review', 'verify', '2016-02-01 11:09:19.247149', '10.0.1.199', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (221, 1, 711, 'locked', 'checkout', '2016-02-01 11:09:28.030557', '10.0.1.199', 810);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (222, 1, 712, 'locked', 'checkout', '2016-02-01 11:09:28.030557', '10.0.1.199', 810);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (221, 1, 713, 'accepted', 'accept', '2016-02-01 11:09:31.624146', '10.0.1.199', 814);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (222, 1, 714, 'accepted', 'accept', '2016-02-01 11:09:35.632126', '10.0.1.199', 815);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (223, 1, 715, 'reg', 'initial', '2016-02-01 11:10:24.094659', '10.0.1.199', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (223, 1, 716, 'unverified', 'verifymail', '2016-02-01 11:10:24.094659', '10.0.1.199', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (223, 1, 717, 'pending_review', 'verify', '2016-02-01 11:10:24.110347', '10.0.1.199', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (223, 1, 718, 'locked', 'checkout', '2016-02-01 11:10:42.583532', '10.0.1.199', 818);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (223, 1, 719, 'accepted', 'accept', '2016-02-01 11:10:45.934089', '10.0.1.199', 822);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (224, 1, 720, 'reg', 'initial', '2016-02-01 11:12:34.910933', '10.0.1.199', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (224, 1, 721, 'unverified', 'verifymail', '2016-02-01 11:12:34.910933', '10.0.1.199', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (224, 1, 722, 'pending_review', 'verify', '2016-02-01 11:12:34.953531', '10.0.1.199', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (224, 1, 723, 'locked', 'checkout', '2016-02-01 11:12:49.279491', '10.0.1.199', 825);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (224, 1, 724, 'accepted', 'accept', '2016-02-01 11:12:53.505434', '10.0.1.199', 829);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (224, 2, 725, 'reg', 'initial', '2016-02-01 11:12:53.526224', '127.0.0.1', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (224, 2, 726, 'accepted', 'adminclear', '2016-02-01 11:12:53.526224', '127.0.0.1', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (223, 2, 727, 'reg', 'initial', '2016-02-01 11:12:53.526224', '127.0.0.1', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (223, 2, 728, 'accepted', 'adminclear', '2016-02-01 11:12:53.526224', '127.0.0.1', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (222, 2, 729, 'reg', 'initial', '2016-02-01 11:12:53.526224', '127.0.0.1', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (222, 2, 730, 'accepted', 'adminclear', '2016-02-01 11:12:53.526224', '127.0.0.1', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (221, 2, 731, 'reg', 'initial', '2016-02-01 11:12:53.526224', '127.0.0.1', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (221, 2, 732, 'accepted', 'adminclear', '2016-02-01 11:12:53.526224', '127.0.0.1', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (220, 2, 733, 'reg', 'initial', '2016-02-01 11:12:53.526224', '127.0.0.1', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (220, 2, 734, 'accepted', 'adminclear', '2016-02-01 11:12:53.526224', '127.0.0.1', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (225, 1, 735, 'reg', 'initial', '2016-02-01 11:13:36.223249', '10.0.1.199', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (225, 1, 736, 'unverified', 'verifymail', '2016-02-01 11:13:36.223249', '10.0.1.199', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (225, 1, 737, 'pending_review', 'verify', '2016-02-01 11:13:36.242356', '10.0.1.199', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (225, 1, 738, 'accepted', 'accept', '2016-02-01 11:13:49.312337', '10.0.1.199', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (220, 2, 739, 'accepted', 'adminclear', '2016-02-01 11:12:53.526224', '127.0.0.1', NULL);

SELECT pg_catalog.setval('action_states_state_id_seq', 739, true);

INSERT INTO ad_changes (ad_id, action_id, state_id, is_param, column_name, old_value, new_value) VALUES (225, 1, 736, true, 'pack_status', NULL, 'disabled');

INSERT INTO ad_codes (code, code_type) VALUES (112200000, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (112200001, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (112200002, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (112200003, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (112200004, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (112200005, 'verify');

INSERT INTO ad_codes (code, code_type) VALUES (135311, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (167334, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (199357, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (141380, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (173403, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (115426, 'pay');

INSERT INTO ad_image_changes (ad_id, action_id, state_id, seq_no, name, is_new) VALUES (220, 1, 704, 0, NULL, false);
INSERT INTO ad_image_changes (ad_id, action_id, state_id, seq_no, name, is_new) VALUES (221, 1, 713, 0, NULL, false);
INSERT INTO ad_image_changes (ad_id, action_id, state_id, seq_no, name, is_new) VALUES (222, 1, 714, 0, NULL, false);
INSERT INTO ad_image_changes (ad_id, action_id, state_id, seq_no, name, is_new) VALUES (223, 1, 719, 0, NULL, false);
INSERT INTO ad_image_changes (ad_id, action_id, state_id, seq_no, name, is_new) VALUES (224, 1, 724, 0, NULL, false);
INSERT INTO ad_image_changes (ad_id, action_id, state_id, seq_no, name, is_new) VALUES (225, 1, 738, 0, NULL, false);

INSERT INTO ad_params (ad_id, name, value) VALUES (220, 'rooms', '1');
INSERT INTO ad_params (ad_id, name, value) VALUES (220, 'size', '123');
INSERT INTO ad_params (ad_id, name, value) VALUES (220, 'garage_spaces', '1');
INSERT INTO ad_params (ad_id, name, value) VALUES (220, 'condominio', '1');
INSERT INTO ad_params (ad_id, name, value) VALUES (220, 'communes', '321');
INSERT INTO ad_params (ad_id, name, value) VALUES (220, 'estate_type', '1');
INSERT INTO ad_params (ad_id, name, value) VALUES (220, 'bathrooms', '1');
INSERT INTO ad_params (ad_id, name, value) VALUES (220, 'new_realestate', '1');
INSERT INTO ad_params (ad_id, name, value) VALUES (220, 'country', 'UNK');
INSERT INTO ad_params (ad_id, name, value) VALUES (221, 'rooms', '1');
INSERT INTO ad_params (ad_id, name, value) VALUES (221, 'size', '12');
INSERT INTO ad_params (ad_id, name, value) VALUES (221, 'garage_spaces', '2');
INSERT INTO ad_params (ad_id, name, value) VALUES (221, 'condominio', '123');
INSERT INTO ad_params (ad_id, name, value) VALUES (221, 'communes', '321');
INSERT INTO ad_params (ad_id, name, value) VALUES (221, 'estate_type', '1');
INSERT INTO ad_params (ad_id, name, value) VALUES (221, 'bathrooms', '1');
INSERT INTO ad_params (ad_id, name, value) VALUES (221, 'new_realestate', '1');
INSERT INTO ad_params (ad_id, name, value) VALUES (221, 'country', 'UNK');
INSERT INTO ad_params (ad_id, name, value) VALUES (222, 'rooms', '1');
INSERT INTO ad_params (ad_id, name, value) VALUES (222, 'communes', '321');
INSERT INTO ad_params (ad_id, name, value) VALUES (222, 'estate_type', '1');
INSERT INTO ad_params (ad_id, name, value) VALUES (222, 'bathrooms', '2');
INSERT INTO ad_params (ad_id, name, value) VALUES (222, 'new_realestate', '1');
INSERT INTO ad_params (ad_id, name, value) VALUES (222, 'country', 'UNK');
INSERT INTO ad_params (ad_id, name, value) VALUES (223, 'rooms', '1');
INSERT INTO ad_params (ad_id, name, value) VALUES (223, 'communes', '321');
INSERT INTO ad_params (ad_id, name, value) VALUES (223, 'estate_type', '1');
INSERT INTO ad_params (ad_id, name, value) VALUES (223, 'bathrooms', '1');
INSERT INTO ad_params (ad_id, name, value) VALUES (223, 'new_realestate', '1');
INSERT INTO ad_params (ad_id, name, value) VALUES (223, 'country', 'UNK');
INSERT INTO ad_params (ad_id, name, value) VALUES (224, 'rooms', '2');
INSERT INTO ad_params (ad_id, name, value) VALUES (224, 'communes', '321');
INSERT INTO ad_params (ad_id, name, value) VALUES (224, 'estate_type', '1');
INSERT INTO ad_params (ad_id, name, value) VALUES (224, 'bathrooms', '2');
INSERT INTO ad_params (ad_id, name, value) VALUES (224, 'new_realestate', '1');
INSERT INTO ad_params (ad_id, name, value) VALUES (224, 'country', 'UNK');
INSERT INTO ad_params (ad_id, name, value) VALUES (225, 'rooms', '1');
INSERT INTO ad_params (ad_id, name, value) VALUES (225, 'communes', '321');
INSERT INTO ad_params (ad_id, name, value) VALUES (225, 'estate_type', '1');
INSERT INTO ad_params (ad_id, name, value) VALUES (225, 'bathrooms', '1');
INSERT INTO ad_params (ad_id, name, value) VALUES (225, 'new_realestate', '1');
INSERT INTO ad_params (ad_id, name, value) VALUES (225, 'country', 'UNK');
INSERT INTO ad_params (ad_id, name, value) VALUES (225, 'pack_status', 'disabled');

SELECT pg_catalog.setval('ads_ad_id_seq', 126, true);

SELECT pg_catalog.setval('list_id_seq', 8000106, true);

SELECT pg_catalog.setval('pay_code_seq', 63, true);

INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (301, 'save', 0, '112200000', NULL, '2016-02-01 11:04:06', 260, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (302, 'verify', 0, '112200000', NULL, '2016-02-01 11:04:06', 260, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (303, 'save', 0, '112200001', NULL, '2016-02-01 11:08:45', 261, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (304, 'verify', 0, '112200001', NULL, '2016-02-01 11:08:45', 261, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (305, 'save', 0, '112200002', NULL, '2016-02-01 11:09:19', 262, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (306, 'verify', 0, '112200002', NULL, '2016-02-01 11:09:19', 262, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (307, 'save', 0, '112200003', NULL, '2016-02-01 11:10:24', 263, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (308, 'verify', 0, '112200003', NULL, '2016-02-01 11:10:24', 263, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (309, 'save', 0, '112200004', NULL, '2016-02-01 11:12:35', 264, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (310, 'verify', 0, '112200004', NULL, '2016-02-01 11:12:35', 264, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (311, 'save', 0, '112200005', NULL, '2016-02-01 11:13:36', 265, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (312, 'verify', 0, '112200005', NULL, '2016-02-01 11:13:36', 265, 'OK');

SELECT pg_catalog.setval('pay_log_pay_log_id_seq', 313, true);

INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (301, 0, 'adphone', '12312312');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (301, 1, 'remote_addr', '10.0.1.199');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (302, 0, 'remote_addr', '10.0.1.199');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (303, 0, 'adphone', '12312312');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (303, 1, 'remote_addr', '10.0.1.199');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (304, 0, 'remote_addr', '10.0.1.199');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (305, 0, 'adphone', '12312312');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (305, 1, 'remote_addr', '10.0.1.199');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (306, 0, 'remote_addr', '10.0.1.199');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (307, 0, 'adphone', '12312312');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (307, 1, 'remote_addr', '10.0.1.199');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (308, 0, 'remote_addr', '10.0.1.199');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (309, 0, 'adphone', '12312312');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (309, 1, 'remote_addr', '10.0.1.199');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (310, 0, 'remote_addr', '10.0.1.199');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (311, 0, 'adphone', '12312312');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (311, 1, 'remote_addr', '10.0.1.199');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (312, 0, 'remote_addr', '10.0.1.199');

SELECT pg_catalog.setval('payment_groups_payment_group_id_seq', 266, true);

INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (260, 'ad_action', 0, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (261, 'ad_action', 0, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (262, 'ad_action', 0, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (263, 'ad_action', 0, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (264, 'ad_action', 0, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (265, 'ad_action', 0, 0);

INSERT INTO review_log (ad_id, action_id, admin_id, review_time, queue, action_type, action, refusal_reason_text, category) VALUES (220, 1, 9, '2016-02-01 11:08:04.737026', 'first_da', 'new', 'accepted', NULL, 1220);
INSERT INTO review_log (ad_id, action_id, admin_id, review_time, queue, action_type, action, refusal_reason_text, category) VALUES (221, 1, 9, '2016-02-01 11:09:31.624146', 'normal', 'new', 'accepted', NULL, 1220);
INSERT INTO review_log (ad_id, action_id, admin_id, review_time, queue, action_type, action, refusal_reason_text, category) VALUES (222, 1, 9, '2016-02-01 11:09:35.632126', 'normal', 'new', 'accepted', NULL, 1220);
INSERT INTO review_log (ad_id, action_id, admin_id, review_time, queue, action_type, action, refusal_reason_text, category) VALUES (223, 1, 9, '2016-02-01 11:10:45.934089', 'normal', 'new', 'accepted', NULL, 1220);
INSERT INTO review_log (ad_id, action_id, admin_id, review_time, queue, action_type, action, refusal_reason_text, category) VALUES (224, 1, 9, '2016-02-01 11:12:53.505434', 'normal', 'new', 'accepted', NULL, 1220);

INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (220, 1, 702, 'queue', 'first_da');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (220, 1, 704, 'filter_name', 'first_da');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (221, 1, 707, 'queue', 'normal');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (222, 1, 710, 'queue', 'normal');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (221, 1, 713, 'filter_name', 'normal');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (222, 1, 714, 'filter_name', 'normal');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (223, 1, 717, 'queue', 'normal');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (223, 1, 719, 'filter_name', 'normal');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (224, 1, 722, 'queue', 'normal');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (224, 1, 724, 'filter_name', 'normal');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (225, 1, 737, 'queue', 'pack');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (225, 1, 738, 'filter_name', 'pack');

SELECT pg_catalog.setval('tokens_token_id_seq', 833, true);

INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (1, '2016-02-01 11:08:04.757137', '20155@jenna.schibsted.cl', NULL, NULL, NULL, NULL, NULL, NULL, 'cmd:admail
commit:1
ad_id:220
action_id:1
mail_type:accept
', NULL, 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (2, '2016-02-01 11:09:31.637394', '20155@jenna.schibsted.cl', NULL, NULL, NULL, NULL, NULL, NULL, 'cmd:admail
commit:1
ad_id:221
action_id:1
mail_type:accept
', NULL, 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (3, '2016-02-01 11:09:35.638707', '20155@jenna.schibsted.cl', NULL, NULL, NULL, NULL, NULL, NULL, 'cmd:admail
commit:1
ad_id:222
action_id:1
mail_type:accept
', NULL, 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (4, '2016-02-01 11:10:45.944412', '20155@jenna.schibsted.cl', NULL, NULL, NULL, NULL, NULL, NULL, 'cmd:admail
commit:1
ad_id:223
action_id:1
mail_type:accept
', NULL, 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (5, '2016-02-01 11:12:53.518933', '20155@jenna.schibsted.cl', NULL, NULL, NULL, NULL, NULL, NULL, 'cmd:admail
commit:1
ad_id:224
action_id:1
mail_type:accept
', NULL, 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (7, '2016-02-01 11:13:49.325849', '20155@jenna.schibsted.cl', NULL, NULL, NULL, NULL, NULL, NULL, 'cmd:admail
commit:1
ad_id:225
action_id:1
mail_type:accept
', NULL, 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (6, '2016-02-01 11:13:36.292313', '20155@jenna.schibsted.cl', '2016-02-01 11:13:46.292313', '2016-02-01 11:13:49.329582', '20155@jenna.schibsted.cl', 'TRANS_OK', '2016-02-01 11:13:49.308058', '20155@jenna.schibsted.cl', 'cmd:review
commit:1
ad_id:225
action_id:1
action:accept
filter_name:pack
remote_addr:10.0.1.199

', '220 Welcome.
status:TRANS_OK
end
', 'AT', 'review', '25');

SELECT pg_catalog.setval('trans_queue_id_seq', 8, true);

SELECT pg_catalog.setval('uid_seq', 61, true);

INSERT INTO user_params (user_id, name, value) VALUES (60, 'first_inserted_ad', '2016-02-01 11:04:05.772146-03');
INSERT INTO user_params (user_id, name, value) VALUES (60, 'first_approved_ad', '2016-02-01 11:08:04.737026-03');

SELECT pg_catalog.setval('users_user_id_seq', 61, true);

--Change all passwords to 123123123
update ads set salted_passwd =  E'$1024$2y1bQ}VYRD}zF4QCbf07a971b784888658a20e62156de64dfde5ffb6', passwd = null;
update accounts set salted_passwd =  E'$1024$2y1bQ}VYRD}zF4QCbf07a971b784888658a20e62156de64dfde5ffb6';
