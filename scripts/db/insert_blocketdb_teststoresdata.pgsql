--
-- PostgreSQL database dump
--

SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = public, pg_catalog;

--
-- Name: stores_store_id_seq; Type: SEQUENCE SET; Schema: public; Owner: pelle
--

SELECT pg_catalog.setval(pg_catalog.pg_get_serial_sequence('stores', 'store_id'), 1, false);


--
-- Data for Name: stores; Type: TABLE DATA; Schema: public; Owner: pelle
--

INSERT INTO stores VALUES (12, 'Svenska Bilgruppen AB', 'svenska_bilgruppen.se@blocket.se', '08-84 10 10', '08-84 13 00', 'Lignagatan 1', '117 34', 'STOCKHOLM', 'svb_logo.gif', 'S�LJER-K�PER-BYTER Bra begagnade bilar. MRF-Handlare', '<b>V�lkommen\ttill oss p� Svenska Bilgruppen!</b>
<p>
Bilarfirman man v�ljer n�r man k�per och s�ljer.
<p>
Vi har specialiserat oss p� bra begagnade bilar fr�n �rsmodell -90 till -2005. Bes�k v�r bilhall eller ta en titt p� v�r hemsida d�r hittar du bilder p� alla bilar vi s�ljer. Vi har alltid ett 50-tal bilar i lager till f�rm�nliga priser.
<p>
V�ra l�ga omkostnader medf�r konkurrenskraftiga priser.
Svenska Bilgruppen �r medlem i Motorbranschens riksf�rbund (MRF), vilket medf�r en trygghet f�r dig som kund.
<p>
<b>Ring eller bes�k oss, det l�nar sig alltid!</b>', 'http://www.bilgruppen.se', 'active', true, '2003-06-12 15:30:00', '2016-09-01 23:59:59', 'BMW, Alfa Romeo, Audi, Cadillac, Chevrolet, Chrysler, Ford, Mercedes, Opel, Saab, Subaru, Volkswagen, Volvo, Toyota,', '-', 'patrik', '-', '-', 'patrik', 'patrik', 11, 0, 1020);
INSERT INTO stores VALUES (31, 'Capital Car AB', 'info_capitalcar.se@blocket.se', '08 - 650 66 08', '08 - 441 09 88', 'S:t Eriksgatan 4', '112 39', 'STOCKHOLM', 'banner2.gif', 'Vi �r specialister p� exklusiva bilar och vanliga bilar.', '
<BR>
- Vi �r specialister p� exklusiva bilar och vanliga bilar.
<br> 
- Vi har ett stort urval av exempelvis Mercedes och BMW
<br> 
- K�per - S�ljer - Byter
<br> 
- F�rm�nlig finansiering - Kontantinsats 20% 
<BR>
<BR>
G�r g�rna ett bes�k i v�r bilsalong p� S:t Eriksgatan 4 i Stockholm!
<BR>
', 'http://www.capitalcar.se', 'active', false, '2003-06-13 14:00:00', '2016-05-14 23:59:59', 'Audi, BMW, Chrysler, Citroen, Ford, Mercedes, Volkswagen, Volvo', '-', 'patrik', '-', '-', 'patrik', 'patrik', 11, 0, 1020);
INSERT INTO stores VALUES (88, '�s� Bilhall AB', 'info_asobilhall.se@blocket.se', '08-641 12 24', '08-643 22 60', 'Blekingegatan 36', '118 56', 'STOCKHOLM', 'aso.gif', 'K�PER-BYTER-S�LJER', 'Vi handlar med nyare begagnade bilar.
<p>
�ppettider:<br>
<li>Vardagar: 9.00-18.00</li>
<li>L�rdagar: 9.00-13.00</li>
<p>
V�lkomna!', 'http://www.asobilhall.se', 'active', false, '2003-06-12 10:30:00', '2016-01-01 23:59:00', 'Audi, BMW, Mercedes, Saab, Volvo', '-', 'patrik', '-', '-', 'patrik', 'patrik', 11, 0, 1020);
INSERT INTO stores VALUES (123, 'Bilborgen AB', 'bilborgen_bilborgen.se@blocket.se', '08-99 10 23', '08-99 01 07', 'H�gdalens Centrum', '124 50', 'H�GDALEN', 'bilborgen.gif', 'En riktig bilfirma!', 'Bilborgen AB etablerades 1976 i nuvarande lokaler i H�gdalen av Per Nymark. Inriktningen har alltid varit b�ttre begagnade bilar. M�let �r att ha cirka 30 bilar i lager med priser fr�n 50.000 upp till 250.000:-. Vi hj�lper g�rna till med finansering. Bilborgen, en riktig bilfirma.', 'http://www.bilborgen.se
', 'active', false, '2003-06-06 00:00:00', '2016-03-01 23:59:59', 'Alfa Romeo, BMW, Chevrolet, Ford, Kia, Mitsubishi, Rover, Saab, Skoda, Volvo, Yamaha', '-', 'patrik', '-', '-', 'patrik', 'patrik', 11, 0, 1020);
INSERT INTO stores VALUES (162, 'Fittja Bil', 'fittjabil_hotmail.com@blocket.se', '08-710 18 85', '08- 710 18 86', 'Fittjav�gen 23', '145 53', 'NORSBORG', 'fittjabil.gif', 'M�rkesoberoende handlare', '�ppettider: 
<br>
m�n-fre 10-19
<br>
l�r-s�n 11-17 ', 'http://www.fittjabil.com', 'active', false, '2003-06-03 00:00:00', '2017-01-01 23:59:59', 'Audi, BMW, Chrysler, Citroen, Fiat, Ford, Honda, Kia, Mazda, Mercedes, Mitsubishi, Nissan, Opel, Renault, Toyota, Suzuki, Volkswagen, Volvo', '-', 'patrik', '-', '-', 'patrik', 'patrik', 11, 0, 1020);
INSERT INTO stores VALUES (240, 'Lennart R�nn Bil AB', 'info_ronnbil.se@blocket.se', '08-712 43 11', '08-712 73 38', 'Vinterv�gen 5', '135 21', 'TYRES�', 'ronn_logo.gif', 'Kunden i centrum - det sitter i v�ggarna hos oss', 'Med ett 15-tal anst�llda �r vi ett fullservicef�retag, stora nog att vara specialiserade, litet nog att ge v�ra kunder ett personligt bem�tande. Vi str�var efter att bli branschens b�sta servicef�retag. F�r oss betyder det kvalitet, kompetens och ett personligt engagemang.
<p>
�ppettider Bilf�rs�ljning:<br>
M�n - Torsdag 09.00 - 18.00<br>
Fredag 09.00 - 16.00<br>
L�rdag 11.00 - 14.00', 'http://www.ronnbil.se/', 'active', false, '2003-12-12 11:00:00', '2016-03-31 23:59:00', 'saab, opel, chevrolet, chrysler, ford, volvo,', '-', 'patrik', '-', '-', 'patrik', 'patrik', 11, 0, 1020);
INSERT INTO stores VALUES (296, 'Svenska Motorkompaniet AB', 'info_svenskamotorkompaniet.se@blocket.se', '08 - 449 84 00', '08 -449 84 15', 'Huddingev�gen 305', '141 37', 'HUDDINGE', 'SvenskaMotorkompaniet_logo.gif', 'M�rkesoberoende handlare', 'V�lkommen till Svenska Motorkompaniet i Huddinge. Hitta din dr�mbil h�r p� v�r webbplats eller g�r ett bes�k hos oss i v�ra trevliga lokaler. Vare sig det �r en exklusiv sportvagn eller en trygg och p�litlig familjebil du s�ker v�gar vi p�st� att du hittar det du s�ker hos oss. Och detta till mycket konkurrenskraftiga priser ackompanjerat av en personlig och trevlig service. 
', 'http://www.svenskamotorkompaniet.se
', 'active', false, '2003-08-20 11:30:00', '2016-05-01 23:59:59', 'Audi, BMW, Chrysler, Ford, Jeep, Land Rover, Mercedes, Nissan, Peugeot, Renault, Volvo, Toyota', '-', 'patrik', '-', '-', 'patrik', 'patrik', 11, 0, 1020);
INSERT INTO stores VALUES (381, 'Trucker AB', 'order_trucker.se@blocket.se', '08-552 485 99', '08-552 475 11', 'Nedre Bruket', '155 21', 'NYKVARN', 'trucker.gif', 'Import och marknadsf�ring/f�rs�ljning av delar och tillbeh�r till l�tta lastbilar', 'TRUCKER AB:s verksamhet �r import och marknadsf�ring/f�rs�ljning av delar och tillbeh�r till l�tta lastbilar, i huvudsak amerikanska. Vi �r baserade i Sverige men har �ven kontor och lager i USA varifr�n all import sker. Vi etablerades 1986 och har haft i stort samma verksamhetsomr�de sedan dess. Detta har givit oss osedvanlig expertis, produktkunskap och effektivitet. Trucker borgar f�r exceptionell service och konkurrenskraft vare sig vi handlar med Er i gossist eller konsumentled. Trucker AB har sedan starten funnits i Nykvarn utanf�r S�dert�lje, d�r finns v�r butik d�r Ni kan �ven kan handla �ver disk, dock sker huvuddelen av v�r f�rs�ljning via postorder.

', 'http://www.trucker.se
', 'active', false, '2003-10-15 00:00:00', '2016-03-03 23:00:00', 'Bildelar, Biltillbeh�r', '-', 'patrik', '-', '-', 'patrik', 'patrik', 11, 0, 1040);
INSERT INTO stores VALUES (481, 'bostart', 'info_sehlbergbil.com@blocket.se', '08-26 70 73', '08-564 717 76', 'Saldov�gen 3', '175 62', 'J�RF�LLA', 'sehlberg_logo.gif', 'Verksamma i branschen i snart 25 �r.', 'H�kan Sehlberg Bil AB startade 1979, idag har vi 3 anst�llda. Vi b�de k�per och s�ljer begagnade bilar till bra priser. Vi har alltid ett 90-tal bra bilar i lager. 
<p>
V�ra �ppettider �r:<br> 
M�ndag - Torsdag 9-18<br>
Fredag 9-17<br>
L�rdag 10-14
<p>
Kom och bes�k oss idag!', 'http://www.sehlbergbil.com/', 'active', false, '2004-03-08 16:18:00', '2016-07-01 23:59:00', 'Alfa Romeo, Audi, BMW, Chevrolet, Chrysler, Jaguar, Ferrari, Fiat, Ford, Jeep, Mercedes, Mitsubishi, Nissan, Opel, Saab, Volkswagen, Volvo,', '-', 'patrik', '-', '-', 'patrik', 'patrik', 11, 0, 1020);
INSERT INTO stores VALUES (541, 'Flipper Marin AB', 'christer.rexholt_flippermarin.se@blocket.se', '08-544 442 40', '08-756 88 05', 'Hamnv�gen 8', '183 57', 'T�BY', 'flippermarin_logo.gif', '�terf�rs�ljare f�r samtliga inom branschen ledande, nordiska b�tfabrikat.', 'Flipper Marin �r �terf�rs�ljare f�r samtliga inom branschen ledande, nordiska b�tfabrikat. V�r m�ls�ttning �r att saluf�ra ett brett utbud av b�tm�rken med h�g kvalitet, i olika storleksklasser till konkurrenskraftiga priser. Vi �r �terf�rs�ljare f�r marknadens toppfabrikat, vilka vi anser uppfyller v�ra krav p� kvalitet tillsammans med pris.
<p>
V�lkomna!', 'http://www.flippermarin.se', 'active', false, '2004-04-05 10:46:00', '2016-08-05 23:59:59', 'B�t, B�tar, Flipper, Aquador, Buster, Mercury, Yamaha,', '-', 'patrik', '-', '-', 'patrik', 'patrik', 11, 0, 1060);
INSERT INTO stores VALUES (542, 'Re/MAX', 'cartrade_telia.com@blocket.se', '08-446 19 87', '08-446 19 97', 'Reprov�gen 6', '183 77', 'VIGGBYHOLM', '-', 'M�rkesoberoende handlare', 'Brett utbud av begagnade bilar.', 'http://www.cartrade.se', 'active', false, '2003-01-01 00:00:00', '2016-01-01 23:00:00', 'Alfa Romeo, Audi, BMW, Chevrolet, Chrysler, Jaguar, Ferrari, Fiat, Ford, Jeep, Mercedes, Mitsubishi, Nissan, Opel, Saab, Volkswagen, Volvo', '-', 'patrik', '-', '-', 'patrik', 'patrik', 11, 0, 1020);
INSERT INTO stores VALUES (547, 'Persson & Widner Bil AB', 'info_viggbymotor.se@blocket.se', '08 - 732 76 76', '08 - 732 69 96', 'Flyghamnsv�gen 1', '183 64', 'T�BY', 'perssonwidner.gif', 'Auktoriserad �terf�rs�ljare f�r HONDA', 'V�lkommen till Viggbyholms Motor AB, ett av T�bys �ldsta bilf�retag. Vi har s�lt och servat bilar i T�by sedan 1959. 
<p>
�r 1994 blev vi auktoriserad �terf�rs�ljare f�r Honda i T�by d�r vi har en fullservice anl�ggning med f�rs�ljning av nya och begagnade bilar samt en komplett bilverkstad f�r reparation av pl�tskador, service, underh�ll och reservdelsf�rs�ljning. V�r verkstad �r �ven auktoriserad f�r Subaru och Daihatsu.', 'http://www.viggbymotor.se
', 'active', false, '2003-06-18 14:30:00', '2016-08-30 23:59:59', 'Honda, BMW, Citro�n, Fiat, Ford, Hyundai, Kia, Mitsubishi, Renault, Saab, Seat, Suzuki, Toyota, Volvo', '-', 'patrik', '-', '-', 'patrik', 'patrik', 11, 0, 1020);
INSERT INTO stores VALUES (588, 'Stockholms Husbil & Husvagnscenter', 'info_shhc.se@blocket.se', '08-756 67 60', '08-756 44 64', 'Saluv�gen 3', '187 66', 'T�BY', 'logo.gif', 'Vi �r ett litet f�retag med stora ambitioner och en stor portion humor!', 'Du/Ni som kunder skall k�nna trygghet att g�ra ditt k�p hos oss vare sig det �r nytt eller begagnat. Vi hoppas dessutom att v�r litenhet visar m�jlighet till genomg�ende l�gsta pris.
<p>
Historik: F�retaget grundades av Jan-Erik Olsson 1973, d� under namnet
Husvagns- Expo. 1975 �ndrades namnet till Stockholms Husvagnscenter AB, och 1998 kom till�gget Husbil med i bilden av f�rklarliga sk�l. �ter har husbilar blivit en stor del av f�retaget, dels HOBBY Husbilar som s�lts sedan starten 1984, samt sedan 1996 �ven ELNAGH Husbilar.
<p>
Genom �ren har flera husvagnsfabrikat passerat s�som Knaus, Cavalier, �ggestorp, S�vsj�, Sm�landia och icke allra minst TABBERT. Tabbert husvagnar som till v�rt stora n�je �ter finns i v�rt utbud!
<p>
Det som dock funnits med som en r�d tr�d sedan 1978 �r HOBBY Husvagnar! Vi �r s�ledes Sveriges �ldsta HOBBY handlare med m�ng�rigt kunnande.
<p>
Vi h�lsar dig varmt v�lkommen!', 'http://www.shhc.se/', 'active', false, '2004-03-11 11:00:00', '2016-03-11 23:59:59', 'Husvagnar, Husvagn, Elnagh, Tabbert, Hobby, Knaus, Solifer, Kabe,', '-', 'patrik', '-', '-', 'patrik', 'patrik', 11, 0, 1100);
INSERT INTO stores VALUES (665, 'Nissan Imperial Bil Malm�/Sverige AB', 'bil.malmo_nissanbil.com@blocket.se', '040-671 52 00', '040-671 52 10', 'Agnesfridsv�gen 190', '200 39', 'MALM�', 'butik.gif', '�terf�rs�ljare f�r Nissan', 'Nissan har idag ett modernt modell-
program. Titta in och bekanta dig med det.
<p>
Har vi ett brett urval begagnade
bilar. Dessutom kan du k�pa dem
till ett "L�gt fast pris".
<p>
Alla begagnade Nissan som du k�per av oss omfattas av garantin om bilen k�rts h�gst 150.000 km och �r yngre �n 8 �r r�knat fr�n bilens f�rsta leveransdatum.



', 'http://www.imperialbil.se

', 'active', false, '2005-05-01 00:00:00', '2016-05-15 23:59:59', 'Nissan', '-', 'patrik', '-', '-', 'patrik', 'patrik', 23, 11, 1020);
INSERT INTO stores VALUES (14184, 'L�gprisbilar AB', 'lagprisbilar_spray.se@blocket.se', '046-37 00 73', '046-37 00 74', 'Odarsl�vsv�gen 50', '225 92', 'LUND', 'butik.gif', 'M�rkesoberoende handlare', '<b>V�lkommen till L�gprisbilar AB</b><p>
H�r har ni alltid billiga bilar till billiga priser som t�l att j�mf�ras med.<p>

F�rm�nlig finansiering med r�nta fr. 6,95%<p>

�ppettider:<br>
Vardagar: 10-18<br>
L�rdagar: St�ngt<br>
S�ndagar: 11-15 <br><p>
V�lkommen ! ', 'http://www.lagprisbilar.nu
', 'active', false, '2005-09-29 00:00:00', '2015-12-31 23:59:59', 'BMW, Ford, Mitsubishi, Saab, Volkswagen, VW, Volvo', '-', 'patrik', '-', '-', 'patrik', 'patrik', 23, 10, 1020);
INSERT INTO stores VALUES (798, 'M�rtenssons Bil AB', 'info_martenssons-bil.se@blocket.se', '0415-31 14 60', '0415-31 16 37', 'Verkstadsgatan 12', '242 21', 'H�RBY', 'martensson.gif', 'Tryggt f�r dig!', 'M�rtenssons Bil AB �r ett familjef�retag som grundades 1952 
och som idag drivs av tredje generationen.
<p>
Bolaget bedriver bilf�rs�ljningsr�relse med tillh�rande verkstadsavdelning. 
F�retaget �r �terf�rs�ljare f�r Svenska Honda AB men s�ljer �ven 
begagnade bilar med tonvikten p� BMW.
', 'http://www.martenssons-bil.se', 'active', false, '2003-04-10 00:00:00', '2016-04-01 23:59:59', 'BMW, Honda, ', '-', 'patrik', '-', '-', 'patrik', 'patrik', 23, 5, 1020);
INSERT INTO stores VALUES (913, 'Sk�nebil Personbilar AB, �ngelholm', 'skanebil_skanebil.se@blocket.se', '0431-898 00', '0431-166 58', 'Kungsg�rdsleden', '262 22', '�NGELHOLM', 'skanebil.gif', 'Volvo & Renault �terf�rs�ljare', 'I �ngelholm finns person- och lastbilsf�rs�ljning med leveranskontor, person- och lastbilsverkst�der, pl�tverkstad, reservdelslager, grossistf�rs�ljning, butik, Volvia f�rs�kringar, Volvokortet och en administrativ avdelning. 

<p>I Klippan har vi personbilsf�rs�ljning, personbilsverkstad, reservdelslager och butik. 

<p>I B�stad finns en allbilsverkstad och en butik. 

<p>P� alla tre st�llena s�ljs bensin. Som representanter f�r Hertz Biluthyrning finns vi ocks� ute p� �ngelholms Flygplats. I �rkelljunga finns Keland Bil AB, v�r lejdverkstad.
', 'http://www.skanebil.se', 'active', false, '2003-06-03 15:00:00', '2015-12-01 23:59:00', 'Volvo, Renault, Alfa Romeo, Audi, Hyundai, Mazda, Mercedes, Nissan, Opel, Peugeot, Saab, Skoda, Toyota, Volkswagen', '-', 'patrik', '-', '-', 'patrik', 'patrik', 23, 16, 1020);
INSERT INTO stores VALUES (14469, 'Bilfirma Thor Nilsson AB', 'thornilssonbil_sjobo.nu@blocket.se', '041612030', '041612039', 'S�dergatan 9', '275 31', 'SJ�BO', 'butik.gif', '�terf�rs�ljare f�r Fiat, Suzuki', '<b>V�lkommen till Bilfirma Thor Nilsson AB</b>
<p>

Firman grundades 1956, Suzuki b�rjade vi s�lja 1994. I v�r bilhall finner Du nya Suzukibilar samt ett stort urval av fina och fr�scha begagnade bilar.
<p>
Vid service, bilreparationer och skadereparationer kontakta v�r verkstadspersonal som har m�nga �rs erfarenhet och kunskap, nu �ven AC-krediterad.
<p>
�ppettider:<br>   
M�ndag-fredag: 09.00-18.00<br>
L�rdag: 10.00-13.00 <p>

V�lkommen !
', 'http://www.suzuki.se/index.asp?kundnr=5019
', 'active', false, '2005-10-24 00:00:00', '2016-01-31 23:59:59', 'Audi, BMW, Ford, Mazda, Mitsubishi, Suzuki, Volkswagen, VW, Volvo  ', '-', 'patrik', '-', '-', 'patrik', 'patrik', 23, 15, 1020);
INSERT INTO stores VALUES (994, 'Newmanbil AB', 'niklas.svendsen_newmanbil.se@blocket.se', '0451-38 40 00', '0451-38 40 25', 'S. Kringelv�gen 2', '281 22', 'H�SSLEHOLM', 'Newman.gif', 'Din Saab - Opel-handlare', 'Hos Newmanbil i H�ssleholm och Osby hittar du ett av marknadens bredaste och starkaste bilsortiment. Oavsett vad du har f�r �nskem�l n�r det g�llar din bil, s� �r chansen mycket stor att vi har en modell som passar dina behov. Dessutom har vi en butik med bra tillbeh�r och en auktoriserad verkstad med kunniga mekaniker, riktiga originaldelar och l�ga priser.', 'http://www.newmanbil.se
', 'active', false, '2003-04-10 00:00:00', '2016-05-01 23:59:59', 'Saab, Opel, Chevrolet', '-', 'patrik', '-', '-', 'patrik', 'patrik', 23, 4, 1020);
INSERT INTO stores VALUES (13815, 'Toyota Center G�vle', 'toyotagavle_toyota.se@blocket.se', '026-10 72 50', '026-14 00 37', 'Ingenj�rsgatan 2', '801 31', 'G�VLE', 'logga.jpg', '�terf�rs�ljare f�r Lexus, Toyota', '<b>Vi p� Toyota G�vle h�lsar er v�lkomna till v�r fullserviceanl�ggning.</b>
<p>
Vi har en gedigen erfarenhet om bilar i allm�nhet och Toyota i synnerhet. V�ra duktiga s�ljare och mekaniker kan hj�lpa dig med alla fr�gor som g�ller ditt bilk�p.
<p>
V�lkommen p� ett bes�k i G�vles modernaste bilanl�ggning.
<p>
�ppettider<br>
M�n-Fre: 09.00-18.00 <br>
L�rdag:  10.00-13.00 <p>

', 'http://www.toyotagavle.se
', 'active', false, '2005-05-03 00:00:00', '2016-05-15 23:59:59', 'Audi, Lexus, Renault, Saab, Toyota, Volkswagen, VW, Volvo', '-', 'patrik', '-', '-', 'patrik', 'patrik', 5, 0, 1020);
INSERT INTO stores VALUES (14074, 'B�ckmans Bilar AB', 'info_backmansbilar.com@blocket.se', '0433-108 30', '0433-100 35', 'Hannabad 1365', '28591', 'MARKARYD', 'butik_logga.gif', 'M�rkesoberoende handlare', '�ppettider:<br>
M�n-Tor 13-18<br>
Fre 13-17<br>
L�rdagar och f�rmiddagar efter �verenskommelse<br>
S�n- och helgdagar st�ngt<p>

�r du intresserad av en bil eller har fr�gor s� tveka inte att kontakta oss. Vill du ha snabba svar s� rekommenderar vi dig att ringa men det g�r ocks� bra att skicka e-post.
', 'http://www.backmansbilar.se

', 'active', false, '2005-09-16 00:00:00', '2015-12-31 23:59:59', 'Audi, Chevrolet, Opel, Peugeot, Saab, Volkswagen, VW, Volvo', '-', 'patrik', '-', '-', 'patrik', 'patrik', 21, 0, 1020);
INSERT INTO stores VALUES (1047, 'Husvagnsreserven AB', 'husvagnsreserven_telia.com@blocket.se', '044-24 56 90', '044-24 44 86', 'S�vv�gen 10', '291 59', 'KRISTIANSTAD', 'logo.gif', 'Det din husvagn beh�ver.', 'Husvagnsreserven grundades 1988 av Anders Ekdahl. Under tiden 1988 -1992 importerades i huvudsak reservdelar till Tabbert husvagnar.
Under l�gkonjunkturen 1992 organiserades f�retaget om och bildade ett aktiebolag (Husvagnsreserven i Kristianstad AB). Vid denna tidpunkt b�rjade f�retaget �ven s�lja begagnade husvagnar.
<p>
Under �ren 2000-2001 s�ldes nya husvagnar av fabrikat Tabbert och Adria. Bolaget omsatte ca 8,5 miljoner kronor �r 2000.
<p>
Ist�llet f�r Adria b�rjade vi �r 2002 s�lja husvagnar av m�rken Vimara och Wilk. B�da dessa �r produkter fr�n Knaus-Tabbert-Group. Dessa lyckades vi g�ra till riktiga succ�-vagnar. Detta �ret blev vi Sverige1:a p� samtliga av v�ra representerade husvagnar TABBERT, WILK OCH VIMARA.
<p>
�r 2003, �ven detta �r lyckades vi bli Sverige1:a p� TABBERT, WILK OCH VIMARA.
<p>
�PPETTIDER:
<br>
M�ndag - Fredag 8.00 - 17.00
<br>
L�rdag St�ngt
<br>
S�ndag St�ngt', 'http://www.husvagnsreserven.se', 'active', false, '2003-12-23 14:45:00', '2016-06-23 00:00:00', 'husvagnar, tabbert, wilk,', '-', 'patrik', '-', '-', 'patrik', 'patrik', 23, 1, 1100);
INSERT INTO stores VALUES (1054, 'Bilexpo i Kristianstad HB', 'mats_bilexpo.com@blocket.se', '044-12 65 50', '044-12 49 81', 'Blekingev�gen 110', '291 50', 'KRISTIANSTAD', 'bilexpologg.gif', 'Suzuki & Fiat �terf�rs�ljare', 'Bilexpo i Kristianstad HB, st�ller dig som kund i centrum och hj�lper till med din n�sta och f�rhoppningsvis b�sta bilaff�r, antingen det g�ller ny eller begagnad bil.
<p>
Vi �r auktoriserade �terf�rs�ljare f�r Suzuki jeepar och bilar samt Fiat bilar och transportfordon.
<p>
Vi har �ven auktoriserad m�rkesverkstad.
<p>
V�ra �ppettider �r:<br>
M�nd-Torsd 9-18 <br>
Fred 9-17 <br>
L�rd 10-13<br>', 'http://www.bilexpo.com', 'active', false, '2005-08-01 00:00:00', '2016-08-15 23:59:59', 'Suzuki, Fiat, Mazda, Ford, Mitsubishi, Volkswagen, VW, Volvo', '-', 'patrik', '-', '-', 'patrik', 'patrik', 23, 8, 1020);
INSERT INTO stores VALUES (1081, 'Eddies Bil AB', 'eddiesbil_eddiesbil.se@blocket.se', '044-20 21 00', '044-20 22 00', 'R�rv�gen 11', '291 59', 'KRISTIANSTAD', 'eddies.gif', 'Bilar, verkstad & d�ck', 'V�lkommen till Eddies Bil AB i Kristianstad ! Bra bilar, Bra garantier (upp till 24m�n.Garanti!!),L�ga priser.Auktoriserad Fullserviceverkstad (Meca Carservice)D�ck och f�lgar till oslagbara priser!
<p>
Problem med din bil? <br> Ring Kenneth 044-20 21 00
<p>
Byta in / k�pa eller s�lja din bil?<br>
Ring Eddie 044-20 21 00.
<p>
V�lkomna!', 'http://www.eddiesbil.se', 'active', false, '2003-05-23 09:10:00', '2016-11-01 23:59:59', 'Audi, BMW, Ford, Hobby, Mazda, Nissan, Opel, Peugot, Saab, Seat, Toyota, Volkswagen, Volvo', '-', 'patrik', '-', '-', 'patrik', 'patrik', 23, 8, 1020);
INSERT INTO stores VALUES (14440, 'AB Varberg Bil-Depot', 'johan_varbergbil-depot.se@blocket.se', '0340-62 83 00', '0340-61 11 54', 'V�rnamov�gen 14', '432 18', 'VARBERG', 'butik.gif', '�terf�rs�ljare f�r Volvo, Renault och Ford', 'V�lkommen till Bildep�n, din kompletta �terf�rs�ljare f�r Volvo, Renault och Ford personbilar.<br>Samt Renault och Ford transportbilar och Volvo lastbilar. 
<p>

 
�ppettider:<br>
M�n-Fre 09.00-18.00 <br>
L�rd 10.00-13.00<p>', 'http://www.varbergbil-depot.se', 'active', false, '2005-10-19 00:00:00', '2015-12-31 23:59:59', 'Ford, Renault, Volvo, Opel, Saab', '-', 'patrik', '-', '-', 'patrik', 'patrik', 20, 0, 1020);
INSERT INTO stores VALUES (1099, 'Klas�n Bil', 'info_klasenbil.se@blocket.se', '035-14 42 00', '035-14 42 08', 'Bohusgatan 1', '302 38', 'HALMSTAD', 'klasen.gif', '�terf�rs�ljare f�r Chrysler, Jeep, Seat', '<B>V�lkommen till Klas�n Bil.</B>
<p>
V�stsveriges st�rsta auktoriserade �terf�rs�ljare av Chrysler, Jeep & Seat. Vi har �ven verkstad.

<p>Vi har alltid ett 60-tal fr�scha begagnade bilar till r�tt pris i lager.
<p>
Vi �r anslutna till MRF, samt samarbetar med Motorm�nnens testcenter.
<p>
V�lkommen in till oss och g�r en trygg bilaff�r!
', 'http://www.klasenbil.se
', 'active', false, '2003-10-14 00:00:00', '2016-05-15 23:59:59', 'Jeep, Chrysler, Seat, BMW, Mercedes, Saab, Volkswagen, VW', '-', 'patrik', '-', '-', 'patrik', 'patrik', 20, 0, 1020);
INSERT INTO stores VALUES (1109, 'Sannarps Bil AB', 'sannarpsbil_telia.com@blocket.se', '035-13 33 03', '035-13 33 04', 'Skyttev�gen 39', '302 44', 'HALMSTAD', 'logo.gif', 'K�p din nya Peugeot av oss!', 'Thomas Fredblad och Leif Andersson startade sommaren 2000 Sannarps Bil AB.
<br>
Aff�rsid�n �r att s�lja Peugeot�s popul�ra bilprogram.
<p>
Peugeot har idag ett av bilmarknadens mest kompletta bilprogram med s�v�l sk�p och flakbilar till fr�cka cabbar och limousiner.
<br>
Med utg�ngspunkt fr�n detta valde vi att starta en bilfirma med detta program och vi finns nu p� Sannarps industriomr�de i Halmstad i nya fr�scha lokaler.
<p>
Vi k�per och s�ljer nya och begagnade bilar och �nskar alla som g�r i bilbytartankar v�lkomna till oss. 
', 'http://www.sannarpsbil.se/', 'active', false, '2003-12-04 15:54:00', '2015-12-01 23:59:59', 'Peugeot, bil, ', '-', 'patrik', '-', '-', 'patrik', 'patrik', 20, 0, 1020);
INSERT INTO stores VALUES (1142, 'AB Laholms Bilservice', 'lars.larsson_laholmsbilservice.se@blocket.se', '0430-133 00', '0430-146 47', 'Industrigatan 24', '312 34', 'LAHOLM', 'laholmsbilservice.gif', 'Suzuki �terf�rs�ljare', 'Vi �r till f�r Dig! Tillsammans v�ljer vi ut den Suzuki som passar just Dig. Vi hj�lper till med finansiering, f�rs�kring och tar hand om eventuellt inbyte. V�lkommen!', 'http://www.laholmsbilservice.se', 'active', false, '2003-11-27 01:00:00', '2015-12-01 23:59:00', 'Suzuki', '-', 'patrik', '-', '-', 'patrik', 'patrik', 20, 0, 1020);
INSERT INTO stores VALUES (14258, 'M�rten Bil AB', 'marten.bil_telia.com@blocket.se', '0371-500 90', '0371-500 91', 'Storgatan 2', '330 26', 'BURSERYD', 'butik.gif', 'M�rkesoberoende handlare', '<b>V�lkommen till M�rten Bil AB</b>', 'http://www.martenbil.se

', 'active', false, '2005-10-11 00:00:00', '2016-01-15 23:59:59', 'Audi, BMW, Chrysler, Citro�n, Honda, Mercedes-Benz, Saab, Volkswagen, VW, Volvo', '-', 'patrik', '-', '-', 'patrik', 'patrik', 17, 0, 1020);
INSERT INTO stores VALUES (1177, 'Ingemars USA Bilar', 'fakiren_hotmail.com@blocket.se', '0371-322 00', '0371-320 28', 'Lilla Haghult', '333 91', 'SM�LANDSSTENAR', 'usabilar.gif', 'M�ngder av begegnade Amerikanska bilar', 'En kilometer fr�n v�g 26, mitt i skogen finns vi sedan 30 �r tillbaka. Vi har m�ngder av begegnade Amerikanska bilar. Allt fr�n gamla vr�l�k fr�n femtiotalet till dagens cityjeepar.

', 'http://www.usabilar-online.com', 'active', false, '2003-05-14 00:00:00', '2016-03-15 23:59:59', 'BMW, Buick, Cadillac, Chevrolet, Chrysler, Dodge, Fiat, Ford, Jeep, Mitsubishi, Mercedes, Nissan, Oldsmobile, Pontiac, Rolls-Royce, Volvo', '-', 'patrik', '-', '-', 'patrik', 'patrik', 17, 0, 1020);
INSERT INTO stores VALUES (1181, 'M�llefors Bil AB', 'info_mollefors.com@blocket.se', '0371-150 16', '0371-154 84', 'M�llefors', '334 21', 'ANDERSTORP', 'mollefors.gif', '�terf�rs�ljare f�r Nissan', 'Aukt. �terf�rs�ljare f�r NISSAN person och transportbilar. 
<p>Ring oss det l�nar sig. Tel. 0371-15016. 070-3665016. ', 'http://www.mollefors.com', 'active', false, '2005-03-31 00:00:00', '2016-04-01 23:59:59', 'BMW, Chrysler, Mercedes, Nissan, Saab, Volkswagen, Volvo', '-', 'patrik', '-', '-', 'patrik', 'patrik', 17, 0, 1020);
INSERT INTO stores VALUES (1183, 'V�stbo Bilcenter AB', 'vastbobilcenter_telia.com@blocket.se', '0371-188 30', '0371-188 31', '�gatan 11', '334 32', 'ANDERSTORP', 'vastbo.gif', 'Kunden i centrum', 'V�r aff�rsid� �r att s�tta kunden i centrum, h�gsta kvalitet, kunskap, p�litlighet
och b�sta personliga service. Vi �r totalt 7 personer med tillsammans 128 �r i
yrket. Komplett anl�ggning med service-glas-pl�tverkstad, reservdelar och tillbeh�r,
bilv�rdshall och biluthyrning, finansiering och leasing.
', 'http://www.bytbil.com/vastbobilcenter', 'active', false, '2003-05-19 00:00:00', '2016-06-01 23:59:59', 'Mazda, Suzuki, Honda, Audi, BMW, Volvo, Saab, Ford, Mazda, Mercedes, Opel, Nissan, Subaru,', '-', 'patrik', '-', '-', 'patrik', 'patrik', 17, 0, 1020);
INSERT INTO stores VALUES (1222, 'Folksam Auto AB', 'svara.inte@blocket.se', '0470-77 11 00', '0470-77 11 25', 'Ljungadalsgatan 6, Box 3015', '352 46', 'V�XJ�', 'folksamauto2.gif', 'Kvalitet och trygghet!', '<li> Bildemontering / reservdelar
<li> Service och reparationer
<li> Hjulinst�llning 
<li> Luftkonditionering, ackrediterade enligt Swedac
<li> Aluminiumsvetsning
<li> Karosseriverkstad
<li> Lackeringsverkstad
<li> Plastreparationer
<li> Utbildning
<li> Bilf�rs�ljning
<li> Medlemmar i SBR, MRF och certifierade enligt KPL 2000
<li> Kontrollerad bilverkstad
<li> Milj�certifierade enligt ISO 14001
<p>
Hos oss kan du bl.a. k�pa begagnade bildelar till mycket f�rm�nliga priser. Varje detalj kontrolleras med avseende p� funktion och kvalitet. Vi hj�lper dig att hitta det du s�ker, fyra s�ljare �r redo att ta emot din best�llning. Ring oss p� 0470-77 11 20
<p>
Vi ger originaldelen en andra chans!
', 'http://www.folksamauto.com', 'active', false, '2003-09-10 00:00:00', '2016-09-17 23:59:59', 'Bildemontering, Reservdelar, Begagnade delar, Bildelar, Originaldelar', '-', 'patrik', '-', '-', 'patrik', 'patrik', 21, 0, 1040);
INSERT INTO stores VALUES (1231, 'Auto Car Import', 'carimport_telia.com@blocket.se', '0470-232 50', '0470-232 50', 'Fagrab�cksv�gen 20', '352 40', 'V�XJ�', 'autocarimp.gif', 'Vi s�ljer i huvudsak importerade bilar fr�n tyskland.', 'Vi har specialiserat oss p� import bilar fr�n Tyskland men en och annan svensks�ld bil har vi ocks�. Audi, BMW, Volvo, Toyota, Chrysler, Jeepar, bussar, Mitsubishi, Opel, Ford och andra bilm�rken, bensin som diesel, stora och sm�. Vi har serviceverkstad som klarar av n�stan alla bilm�rken.
<p>
Mobil: 070-812 53 51 S�ljare Tobias.
<p>
Vi tar emot best�llningar ocks�.
<p>
V�RA �PPETTIDER:<br>
M�n-Fre 09:00-18:00<br>
L�r 10:00-15:00<br>
S�n St�ngt
<p>
V�lkomna in.', 'http://www.autocarimport.com', 'active', false, '2003-08-08 10:15:00', '2015-12-31 00:00:00', 'Audi, BMW, Chrysler, Ford, Honda, Volvo, Volkswagen', '-', 'patrik', '-', '-', 'patrik', 'patrik', 21, 0, 1020);
INSERT INTO stores VALUES (1240, 'Mickes & Connys Motor AB, B�tar', 'info_mickesmotor.se@blocket.se', '0470-70 08 80', '0470-70 08 89', 'Hjalmar Petris v�g 57-59', '352 46', 'V�XJ�', 'logga.gif', 'Stort utbud av nya och begagnade b�tar', 'V�lkommen till Mickes Motor. <br>
Som vanligt har vi massor av b�tar och b�tmotorer p� lager. <br>
Naturligtvis byter vi MC mot b�t och vice versa! <p>
Har Du problem att f� hem Din nya MC eller b�t s� erbjuder vi Fogelsta biltransporter och b�ttrailers.
', 'http://www.mickesmotor.se', 'active', false, '2003-01-01 00:00:00', '2017-03-31 23:59:59', 'b�t, MC, HD, Honda, biltransport, b�ttrailers, Fogelsta, motorer, ', '-', 'patrik', '-', '-', 'patrik', 'patrik', 21, 0, 1060);
INSERT INTO stores VALUES (1255, 'Fr�jds Motor AB', 'frojdsmotor_telia.com@blocket.se', '0478-342 50', '0478-342 60', 'Storgatan 35', '360 50', 'LESSEBO', 'frojdsmotor.gif', '�terf�rs�ljare av Audi & Volkswagen', '<li>F�rs�ljning av nya bilar: Audi och VW
<li>F�rs�ljning av begagnade bilar
<li>Biluthyrning
<li>Bilb�rgning
<li>Fullst�ndig service
<li>Sl�cker bilprovningens 2:or
<li>Reparationer och p�fyllning av kylmedel till AC
<li>Pl�tskadereparationer och lackering
<li>D�ckservice
<li>Bensinstation med kiosk
<li>MRF-handlare', 'http://www.frojdsmotor.se
', 'active', false, '2003-08-08 11:30:00', '2016-04-01 23:59:59', 'Audi, Volkswagen, BMW, Ford, Mercedes, Nissan, Volvo', '-', 'patrik', '-', '-', 'patrik', 'patrik', 21, 0, 1020);
INSERT INTO stores VALUES (14239, 'Nilanders Bil i Karlskrona AB', 'c.nilander_telia.com@blocket.se', '0455-578 90', '0455-168 97', 'Verkstadsgatan 6', '371 47', 'KARLSKRONA', 'butik.gif', '�terf�rs�ljare f�r Subaru', '<b>V�lkommen till Nilanders bil AB i karlskrona.</b><p>

Vi �r �terf�rs�ljare f�r Subaru och Kia och erbjuder merparten av deras modell-program f�r provk�rning. <br>
F�rutom Subarus och Kias kompletta utbud lagerf�r vi �ven ett 40-tal begagnade bilar. 
 

<p>
Vi har �ppet:<br>
M�nd - Tors 10 - 18<br>
Fred 10 - 16<br>
L�rd 11 - 14 <p>', 'http://www.nilanders.se

', 'active', false, '2005-10-10 00:00:00', '2016-01-15 23:59:59', 'Audi, BMW, Kia, Opel, Subaru, Toyota, Volkswagen, VW', '-', 'patrik', '-', '-', 'patrik', 'patrik', 22, 0, 1020);
INSERT INTO stores VALUES (1284, 'Bj�rn Lundmark AB', 'info_bjornlundmark.se@blocket.se', '0455-578 00', '0455-227 20', 'Verkstadsgatan 6', '371 47', 'KARLSKRONA', 'bjornlundmark.gif', 'Nya och begagnade bilar i Karlskrona', '<li>Nya bilar</li>
<li>Begagnade bilar</li>
<p>
�ppettider <br>
M�n-Tors 1300-1800<br>
Fre 1300-1600 <br>
L�rdagar St�ngt. <br>
Var god ring i f�rsta hand 0455-57800 <br>
', 'http://www.bjornlundmark.se', 'active', false, '2005-08-10 00:00:00', '2015-11-30 23:59:59', 'Audi, BMW, Chevrolet, Chrysler, Citro�n, Honda, Mercedes, Nissan, Porsche, Renault, Subaru, Volkswagen, VW ', '-', 'patrik', '-', '-', 'patrik', 'patrik', 22, 0, 1020);
INSERT INTO stores VALUES (1333, 'Mellstr�ms Bil AB', 'info_mellstromsbil.se@blocket.se', '0486-410 20', '0486-410 22', 'Bergkvarav�gen 485', '385 30', 'TORS�S', 'mellstroms.GIF', 'G�r ditt livs bilaff�r hos oss!!!', 'V�lkommen till Mellstr�ms Bil AB<p>
Vi tillhandah�ller Personbilar, Vans, Minibussar, Pick up�s, Husbilar och Mc.<p> 
Alla bilar levereras besiktigade med upp till 6 m�naders garanti. <p>
Vi erbjuder bra finansiering, vilkor fr�n 4.95% i sammarbete med Elcon Finans. 
', 'http://www.mellstromsbil.se
', 'active', false, '2003-08-21 11:30:00', '2016-05-01 23:59:59', 'Audi, BMW, Chevrolet, Chrysler, Citroen, Ford, Honda, Mazda, Mercedes, Mitsubishi, Nissan, Opel, Peugeot, Porsche, Renault, Rover, Saab, Volkswagen, VW, Volvo', '-', 'patrik', '-', '-', 'patrik', 'patrik', 18, 0, 1020);
INSERT INTO stores VALUES (13243, 'AG Bilar', 'info_agbilar.com@blocket.se', '0141-21 02 30', '0141-21 02 30', 'Saturnusgatan 1', '591 32', 'MOTALA', 'AgBilar_logo.gif', 'M�rkesoberoende handlare', 'Vi �r ett relativt nytt f�retag, och som s�tter kunden i centrum. V�ra tj�nster �r m�nga och oftast anpassas individuellt fr�n kund till kund, f�r att p� det s�ttet �stadkomma �msesidigt f�rtroende och b�sta service.<br> 
Vi k�per, s�ljer, f�rmedlar, och �ven tar in bilar p� best�llning.<br> 
Vi g�r polering, repborttagning, vaxning, polish, lackf�rsegling, plast och gummi rekonditionering, m.m.
<p>
Kontakta oss g�rna f�r vidare info, eller bes�k oss redan idag !!! ', 'http://www.agbilar.com
', 'active', false, '2004-10-22 13:20:00', '2016-03-01 23:59:59', 'Audi, Mercedes, Saab, Volvo', '-', 'patrik', '-', '-', 'patrik', 'patrik', 14, 0, 1020);
INSERT INTO stores VALUES (1335, 'Elmbro Bil AB', 'info_elmbrobil.se@blocket.se', '0485-301 58', '0485-349 00', 'Storgatan 51', '386 31', 'F�RJESTADEN', 'elmbro.GIF', '�terf�rs�ljare f�r Kia', 'I v�r bilhall p� Storgatan i F�rjestaden finner du alltid ett rikligt urval av begagnade personbilar och l�tta transportbilar. <p>Hos oss har 1000-tals kunder gjort goda bil-
aff�rer under 15 �r. V�rt h�llbara recept �r prisv�rd kvalitet, god service och
trygga villkor. <p>Vi �r anslutna till MRF och l�mnar 3 m�naders garanti p� samtliga fordon. Vi har �ven en v�lsorterad reservdelsbutik.<p>
V�ra �ppettider �r: <br>
M�ndag - Torsdag 08.00 - 18.00 <br>
Fredagar 08.00 - 17.00 <br>
L�rdagar 09.00 - 13.00 <p>
', 'http://www.elmbrobil.se', 'active', false, '2005-06-13 00:00:00', '2016-06-15 23:59:59', 'Audi, Ford, Kia, Hyosung, Jaguar, Kia, Mitsubishi, Mercedes, Nissan, Renault, SYM, Seat, Skoda, Suzuki, Toyota, Volkswagen, VW, Volvo,  
TOMOS YOUNGSTER  
  ', '-', 'patrik', '-', '-', 'patrik', 'patrik', 18, 0, 1020);
INSERT INTO stores VALUES (1380, 'Bek� Bil AB', 'info_bekobil.se@blocket.se', '031-68 68 00', '031-68 68 68', 'Sisj�v�gen 45', '400 95', 'G�TEBORG', '10_2_05.gif', '�terf�rs�ljare f�r Saab, Opel, Chevrolet & Subaru', 'V�lkommen till oss f�r en bilupplevelse ut�ver det vanliga.<br>
Vi erbjuder V�stkustens st�rsta utbud av nya Saab, Opel, Chevrolet och Subaru.<br>
Samt ett stort utbud av begagnade bilar.
<p>
Vi finns i Sisj� centrum strax s�der om G�teborg.
<p>
V�lkommen!
', 'http://www.bekobil.se', 'active', false, '2003-05-21 10:32:00', '2016-02-15 23:59:59', 'Saab, Subaru, Opel, Chevrolet, ', '-', 'patrik', '-', '-', 'patrik', 'patrik', 15, 0, 1020);
INSERT INTO stores VALUES (14192, 'Hageblad Bil AB', 'per_hagebladbil.com@blocket.se', '031-16 85 40', '031-16 85 05', 'Box 8762', '402 76', 'G�TEBORG', 'butik.gif', 'M�rkesoberoende handlare', 'Ring f�r visning av Bilarna.<br>
Bilarna visas endast efter �verenskommelse.<br>
Mobil: 0707-592510', 'http://www.hagebladbil.com

', 'active', false, '2005-09-30 00:00:00', '2015-12-31 23:59:59', 'Austin, Chrysler, DeSoto, Dodge, Ford, Honda, Mercedes-Benz, Packard, Plymouth, Studebaker, Volvo', '-', 'patrik', '-', '-', 'patrik', 'patrik', 15, 0, 1020);
INSERT INTO stores VALUES (14194, 'Nyttofordon i Stockholm AB', 'kjell_dintransportbil.se@blocket.se', '08-89 91 05', '-', 'Strykerskev�gen 4', '112 64', 'STOCKHOLM', 'butik2.gif', 'M�rkesoberoende handlare', '<b>V�lkommen till Nyttofordon i Stockholm AB</b>', 'http://www.dintransportbil.se

', 'active', false, '2005-09-30 00:00:00', '2015-12-31 23:59:59', 'Honda, Iveco, Mercedes-Benz, Renault, Volkswagen, VW', '-', 'patrik', '-', '-', 'patrik', 'patrik', 11, 0, 1020);
INSERT INTO stores VALUES (1484, 'Variant Bil', 'variantbil_telia.com@blocket.se', '031-254600', '031-254600', 'Gamlestadsv�gen 20', '415 02', 'G�TEBORG', 'logo.gif', 'Specialitet: V�lv�rdade begagnade bilar!', '<b>Variant Bil</b><br>
<b>Specialitet: V�lv�rdade begagnade bilar!</b><br>
Vi finns p� Gamlestadsv�gen 20.<br>
Telefon 031-25 46 00
<p>
Bes�k oss redan idag.. <p>

Variant Bil, Specialitet :<br>
V�lv�rdade begagnade bilar. �ver 10 �rs erfarenhet av kvalitetsbilar, vi ordnar det mesta.

', 'http://www.variantbil.se

', 'active', false, '2004-01-21 10:59:00', '2016-05-15 23:59:59', 'Audi, BMW, Mercedes, Peugeot, Volkswagen, VW, Volvo', '-', 'patrik', '-', '-', 'patrik', 'patrik', 15, 0, 1020);
INSERT INTO stores VALUES (1627, 'Automan Svenska AB', 'bilar_automan.se@blocket.se', '031-91 80 90', '031-97 79 35', 'Gamla Landsv�gen', '438 22', 'LANDVETTER', 'logo.gif', 'Specialisten p� svensks�lda Volvobilar.', '�r ni ute och tittar p� Volvo kan det l�na sig att h�ra med oss.
<br>
V�r specialit� �r nyare svensks�lda Volvobilar.
<p>
Du som �r intresserad av lite �ldre bilar kan titta p� v�ra �vriga objekt som vi s�ljer till nettopriser.
<p>
Ni kan �ven k�pa er bil p� avbetalning. Vi har n�gra olika alternativ att erbjuda beroende p� ert behov.', 'http://www.automan.se/', 'active', false, '2004-01-30 16:42:00', '2016-04-01 23:59:59', 'Volvo, BMW, Mercedes,', '-', 'patrik', '-', '-', 'patrik', 'patrik', 15, 0, 1020);
INSERT INTO stores VALUES (1772, 'Backamo BG:s Husvagnscenter AB', 'info_backamohusvagnscenter.se@blocket.se', '0522-234 40', '0522-234 00', 'Bastebacka 105', '459 91', 'LJUNGSKILE', 'backamo.gif', 'V�lkommen till Backamo Husvagnscenter AB', 'Vi har massor av visningsvagnar i v�r nya hall.
<br> 
Ring g�rna och boka tid om du vill komma under andra tider �n v�ra ordinarie �ppettider.
<p>
V�r moderna verkstad �r p� ca 350 kvm och med plats f�r 6 vagnar. H�r tar vi hand om service av alla m�rken, garanti�tg�rder, st�rre reparationer och f�rs�kringsskador. 
<p>
Vi hyr ocks� ut b�de till privatpersoner och f�retag.
<p>
Sj�vlklart hj�lper vi till med att finansiera ert husvagnsk�p. Vi anv�nder oss av Wasa kredit.
<p>
<b>�ppettider:</b><br>
Se v�r hemsida!', 'http://www.backamohusvagnscenter.se', 'active', false, '2003-10-06 00:00:00', '2016-03-03 23:59:00', 'Cabby, Kabe, Hobby, Knaus, Polar, Sm�landia, Solifer, Sprite, S�vsj�, Tabbert, Vimara, Dodge, Elnagh, Hymer, Iveco, Peugeot, SMC, VW', '-', 'patrik', '-', '-', 'patrik', 'patrik', 15, 0, 1100);
INSERT INTO stores VALUES (1821, 'Fritz�ns Maskin AB', 'info_fritzens.nu@blocket.se', '0530-101 10', '0530-418 19', 'Sapphultsgatan', '464 34', 'MELLERUD', 'fritzens_logo.gif', 'B�tar i Mellerud', 'F�retaget Fritz�ns grundades �r 1892 och var d� en J�rnhandel, sedan m�nga �r tillbaka �r vi ortens enda kompletta b�tbutik, vi s�ljer nya, och har ett stort urval av bra beg. b�tar & motorer.
<p>
I v�r b�tbutik kan du se nya & beg. motorer, samt tillbeh�r & reservdelar, under samma tak finns �ven en Elektrolux Home butik. 

', 'http://www.fritzens.nu', 'active', false, '2003-08-08 00:00:00', '2015-12-31 23:00:00', 'Bella, Flipper, Ryds, Linder, �rnvik, ', '-', 'patrik', '-', '-', 'patrik', 'patrik', 16, 0, 1060);
INSERT INTO stores VALUES (1855, 'Carblaze AB', 'info_carblaze.com@blocket.se', '033-150 180', '033-150 182', 'Trandaredsg. 200 (Hulta)', '507 52', 'BOR�S', 'carblaze2.gif', 'V�lkommen att dela v�r passion f�r bilar', 'Letar du efter en ny sp�nnande bil kan det vara v�rt att titta lite extra p�
v�rt utbud. Genom goda kontakter samt l�ga omkostnader har CarBlaze
m�jlighet att erbjuda kvalitetsbilar till bra priser. Bilarna har f� �gare
samt �r v�lservade och har �verlag svensk ursprung. J�mf�r g�rna v�ra priser
med konkurrenterna och f� insikt om att du kan spara pengar genom att k�pa
bilen hos Carblaze.
<p>
V�r m�ls�ttning �r att ge dig som kund trygghet i bilk�pet. Detta genom att
erbjuda alla nyblivna bil�gare upp till 12 m�naders garanti.
<p>
Finansiering:<br>
Carblaze erbjuder dig, (i samarbete med ELCON, SEB och Wasa) goda finansieringsl�sningar f�r s�v�l f�retagare som privatkund till
en f�rm�nlig r�nta. V�ljer du en kreativ finansieringsl�sning f�r din
investering, minskar ditt risktagande samtidigt som du sprider kostnaden
�ver tiden.
<p>
Alla vet vi hur viktigt det f�rsta intrycket �r, d�r en stor faktor �r din
bil. Detta �r en av orsakerna till att CarBlaze finns. Vi kan bilv�rd och
erbjuder ett brett utbud av behandlingar f�r b�de bilens insida och utsida.
<p>
Vi finns alltid p� plats f�r hj�lpa er vid bilink�p och fr�gor. (Direktnr F�rs�ljningsansvarig Tomas Lundberg: 033-150 183 / 0709-100 123)
<p>
V�lkommen till Carblaze', 'http://www.carblaze.com', 'active', false, '2003-09-10 00:00:00', '2015-11-12 00:00:00', 'Ford, Mercedes, Mitsubishi, Pontiac, Renault, Volkswagen, Volvo', '-', 'patrik', '-', '-', 'patrik', 'patrik', 16, 0, 1020);
INSERT INTO stores VALUES (1895, '7H Bil AB', 'info_7hbil.se@blocket.se', '0320-165 00', '0320-165 00', 'Ehns Gata 11', '511 56', 'KINNA', '7Hbil.gif', 'K�per, s�ljer, byter och f�rmedlar', 'V�r ambition �r att med en rationell hantering och med l�ga omkostnader kunna erbjuda     konkurrenskraftiga priser p� bilar oavsett prisklass. F�retr�desvis handplockas v�ra bilar fr�n den tyska privatmarknaden. Hittar du inte din dr�mbil i v�rt lager s� letar vi upp den �t dig. Vare sig det �r en exklusiv sportvagn eller en trygg och s�ker familjebil du s�ker, v�gar vi p�st� att vi hittar det du s�ker. Utmana oss g�rna!
<p>
Genom v�rt samarbete med Elcon Finans och Svensk Bilgaranti kan vi erbjuda finansiering och     garanti p� bilen du k�per hos oss.
<p>
Kontakta oss g�rna.<br>
Jarmo J�rvinen 0707-40 20 75<br>
Esa J�rvinen 0705-24 90 32<br>
Claes Stjernhager 0705-70 19 71', 'http://www.7hbil.se
', 'active', false, '2003-10-30 00:00:00', '2016-05-15 23:59:59', 'Audi, BMW, Chrysler, Ferrari, Ford, Jeep, Mercedes, Lexus, Saab, Volkswagen', '-', 'patrik', '-', '-', 'patrik', 'patrik', 16, 0, 1020);
INSERT INTO stores VALUES (1956, 'Svenska Euro i V�st AB', 'svenska.e_telia.com@blocket.se', '0510-144 00', '0510-144 50', 'R�da Johanslund', '531 16', 'LIDK�PING', 'svenskaeuro.GIF', 'Din n�sta & b�sta bilaff�r', 'V�lkommen till Svenska Euro i V�st f�r din n�sta o b�sta bilaff�r.
<p>
Vi har alltid intressanta objekt i lager.
<p>
Klicka p� flaggan ovan f�r v�rt hela bil-lager.
<p>
V�ra �ppettider: <br>
Vardagar: 10-18<br>
L�rdagar: 10-14
<p>
V�lkomna', 'http://www.svenskaeuro.se', 'active', false, '2003-08-26 16:20:00', '2016-04-02 23:59:00', 'Audi, BMW, Chrysler, Ford, Mercedes, Nissan, Saab, Subaru, Toyota, Volkswagen, Volvo, Chevrolet, Cadillac, Lincoln ', '-', 'patrik', '-', '-', 'patrik', 'patrik', 13, 0, 1020);
INSERT INTO stores VALUES (1970, 'H�rene Bil AB', 'claes_harenebilab.se@blocket.se', '0510-53 50 49', '0510-53 50 97', 'Karl Johansv�gen 29', '531 92', 'LIDK�PING', 'harene.GIF', '�F f�r Mitsubishi.', 'V�lkommen till det lilla f�retaget med den stora
servicen.
<p>
H�rene Bil AB �r ett litet personligt f�retag bel�get ca 9 km s�der om
Lidk�ping. F�retaget bildades 1977. Vi �r auktoriserade �terf�rs�ljare f�r
Mitsubishi, med egen verkstad och reservdelslager, och vi har �ven ett stort
utbud av begagnade bilar till salu.
<p>
V�rt motto �r, "det lilla f�retaget med den stora servicen".
<p>
Mitsubishi har ett av Sveriges starkaste garantipaket.
<p>
Alla Mitsubishi-modeller har ett omfattande trygghetspaket - MTG -
Mitsubishi Total Garanti: 3 �r eller 10.000 mil fabriksgaranti, 6 �rs
rostskyddsgaranti (Carisma och Space Star 10 �r), 3 �rs vagnskadegaranti, 10
�r eller 20.000 mil elektronikgaranti samt 3 �rs Inter Euro Service
Avbrottsgaranti.', 'http://www.harenebilab.se', 'active', false, '2003-08-18 15:00:00', '2016-04-01 23:59:59', 'Mitsubishi', '-', 'patrik', '-', '-', 'patrik', 'patrik', 13, 0, 1020);
INSERT INTO stores VALUES (1993, 'Bilkompaniet i Tr�vad AB', 'mattias_bilkompaniet.nu@blocket.se', '0512-711 11', '0512-711 77', 'Stenhaga G�rd', '534 91', 'VARA', 'butik-logga.gif', 'Vi hj�lper dig att hitta din dr�mbil!', 'Bilkompaniet AB i Tr�vad har ett stort kund och leverant�rsn�t i Sverige och �vriga Europa b�de vad g�ller privatpersoner och f�retag. 
V�r specialit� �r import av personbilar och sportbilar m.m. fr�n framf�rallt Tyskland. Vi saluf�r �ven nya och begagnade svensks�lda bilar. 
Vi kan med hj�lp av v�ra etablerade kontakter i hela Europa snabbt leverera bilar utrustade efter era krav och �nskem�l.
<p>
V�ra �ppettider:<br>
tis - fre   10.00 - 18.00<br> 
lunch 12.00 - 13.00<br>
l�r 10-13 (sommarst�ngt Juni-Aug)<br>
', 'http://www.bilkompaniet.nu', 'active', false, '2003-12-08 11:15:00', '2011-06-01 23:59:59', 'ferrari, bmw, porsche, lamborghini, mercedes, audi, jeep, range rover, jaguar,', '-', 'patrik', '-', '-', 'patrik', 'patrik', 13, 0, 1020);
INSERT INTO stores VALUES (2031, 'Bilomarin AB i Karlsborg', 'sales.bilomarin_carlsborg.net@blocket.se', '0505-306 00', '0505-305 00', 'Axtorpsv�gen 51 (utmed Riksv�g 49)', '546 72', 'M�LLTORP', 'bilomarin.gif', 'Peugeot �terf�rs�ljare', '<li>Aukt. f�rs�ljning av Peugeot person-/transportbilar
<li>Aukt. Peugeot-fullservice & service av andra m�rken
<li>Kvalitetskontroll av verkstaden genom Motorm�nnen
<li>Ackrediterade f�r att "sl�cka" 2:or fr�n besiktning
<li>Certifierade f�r arbeten p� AC-anl�ggningar i bilar
<li>D�ckfullservice med f�rs�ljning
<li>Biluthyrning
', 'http://www.bilomarin.se', 'active', false, '2003-09-01 16:00:00', '2015-12-01 00:00:00', 'Peugeot, Verkstad, Fullservice, Ackrediterad
verkstad, AC-certifiering, D�ckfullservice, Biluthyrning, Motorm�nnen, personbilar, transportbilar, serviceinredningar

', '-', 'patrik', '-', '-', 'patrik', 'patrik', 13, 0, 1020);
INSERT INTO stores VALUES (2108, 'Ostkustbilar AB', 'ostkustbilar_telia.com@blocket.se', '0491-78 44 78', '0491-78 44 77', '�sav�gen 43', '572 36', 'OSKARSHAMN', 'ostkustbilar.gif', 'S�ljer begagnade bilar', '<b>V�lkommen till Ostkustbilar AB</b><p>

Vi startade v�r verksamhet �r 2001 med att s�lja, byta och f�rmedla begagnade personbilar och l�tta transportbilar.<p>
Thomas Helmersson som startade f�retaget har jobbat i bilbranschen sedan 1980 som bilreparat�r, bilf�rs�ljare samt VD hos den lokala Volvo �terf�rs�ljaren.<br>

Thomas Helmersson �r f�retaget Ostkustbilar AB:s VD samt s�ljare.<br>
�r 2003 gjordes f�rsta anst�llningen, d� anst�lldes Patrik �strand som har jobbat i bilbranschen sedan 1987.<br>
Patrik �strand �r f�retaget Ostkustbilar AB:s ekonomiansvarige samt s�ljare.<p>

2004 var ett h�ndelserikt �r f�r Ostkustbilar som byggde egna lokaler med bilhall och verkstad, tv�tthall f�r att kunna hantera den kraftigt �kade
volymen s�lda bilar.<br>

2004 anst�lldes Jimmy Pettersson som rekonditionerare och 2005 anst�lldes Ulf Kjellgren som bilreparat�r.<br>
Vi k�per, byter, s�ljer och f�rmedlar begagnade bilar och l�tta transportbilar med b�de f�retag och privatpersoner.<p>

Ring g�rna, s� f�r du veta hur vi kan hj�lpa dig med ditt bilk�p eller med att s�lja den bil du har.<p>

V�lkommen!
<p>
\rV�ra �ppet tider<br>
M�ndag - torsdag 9.00 - 18.00<br>
Fredagar 9.00 - 17.00<br>
L�rdag & S�ndag ST�NGT<br>
men vi kan n�s p� 070-669 74 88

', 'http://www.ostkustbilar.se', 'active', false, '2003-06-18 11:00:00', '2015-12-31 23:00:00', 'Audi, BMW, Fiat, Mercedes, Peugeot, Renault, Saab, Volkswagen, Volvo', '-', 'patrik', '-', '-', 'patrik', 'patrik', 18, 0, 1020);
INSERT INTO stores VALUES (2135, 'Vitala Bil AB', 'vitala-bil_telia.com@blocket.se', '0383-132 20', '0383-168 93', '�stra Nygatan 3', '574 23', 'VETLANDA', 'logo.gif', 'Att k�pa en bil skall vara en gl�dje!', 'Att k�pa en bil skall vara en gl�dje, ett s�tt att visa sin personlighet. Vitala Bil s�ljer bilar av h�g kvalit� till marknadsm�ssiga l�ga priser. Det har vi gjort sedan b�rjan av 60 talet med m�nga n�jda kunder. Olika m�rken har s�lts genom �ren och idag �r vi �terf�rs�ljare av MG & Rover.
<p>
Vitala Bil har �ven m�ng�rig erfarenhet av f�rs�ljning av Brittiska sportbilar och salooner d�r v�ran specialitet �r Aston Martin, Daimler, Jaguar, Land Rover, Morgan & Rolls Royce. V�lkommen med din f�rfr�gan till n�gon av v�ra importspecialister.
<p>
De flesta begagnade bilarna levereras med 1 �rs Eurogaranti vilket hj�lper Dig till ett tryggt begagnat bilk�p.
<p>
V�lkommen!', 'http://www.vitala-bil.se/', 'active', false, '2004-02-12 14:00:00', '2016-02-01 23:59:59', 'MG, Rover, BMW, Honda, Jaguar, Aston Martin, Chevrolet, Jeep, Land Rover, Peugeot, Renault, Toyota, Saab, Volvo, Volkswagen, VW, Seat, Skoda, Mercedes,', '-', 'patrik', '-', '-', 'patrik', 'patrik', 17, 0, 1020);
INSERT INTO stores VALUES (2187, 'Bilbyter AB', 'david.burman_bilbyter.se@blocket.se', '013-12 34 15', '013-12 34 58', 'Bonnorpsgatan 2', '58273', 'LINK�PING', 'bilbyter.gif', 'Hyundai �terf�rs�ljare', 'BilByter har s�lt bilar sedan 1971. Vi har varit �terf�rs�ljare av olika nybilsm�rken men sedan 1992 �r vi auktoriserade �terf�rs�ljare f�r Hyundai personbilar, l�tta lastbilar och bussar. F�rutom nya bilar har vi ocks� ett brett sortiment av begagnade bilar.
<p> 

V�lkommen in till oss p� en provtur. 
', 'http://www.bilbyter.se', 'active', false, '2003-05-14 00:00:00', '2016-07-01 23:59:59', 'Hyundai, Audi, BMW, Chrysler, Ford, Jeep, Lexus, Nissan, Peugeot, Renault, Saab, Toyota, Volkswagen, Volvo', '-', 'patrik', '-', '-', 'patrik', 'patrik', 14, 0, 1020);
INSERT INTO stores VALUES (2195, '�stg�ta Camping Mantorp AB', '-', '0142-217 30', '0142-210 79', 'M�llersbrunnsv�gen 3', '590 17', 'MANTORP', 'logo.gif', 'Nordens st�rsta varuhus - f�r fritid p� hjul.', 'Det har sedan 1969, varit helt fantastiska �r. V�r egen start var visserligen blygsam, men det v�ckte ett genuint intresse f�r alla de m�jligheter som den rullande bostaden f�rde med sig. 
<p>
Vi l�rde oss �lska friheten och uppt�ckte ett nytt s�tt att leva helt enkelt. I dag har vi p� �stg�ta Camping m�nga likasinnade v�nner fr�n n�r och fj�rran. 
<p>
M�nga har under de h�r �ren bes�kt oss och tack vare alla trevliga kunder och bra leverant�rer �r vi i dag till konsument "Nordens st�rsta �terf�rs�ljare av husvagnar och husbilar". Vi oms�tter ca 160 miljoner kr och har totalt 30 medarbetare.   Det �r vi b�de stolta �ver och tacksamma f�r och lovar att vi ska f�rs�ka g�ra forts�ttningen lika trevlig. 
<p>
Vi har m�nga id�er och nyheter som vi kommer att presentera oavsett om du bes�ker oss i Mantorp, Malm�, M�rsta, Norrk�ping eller i �rebro. 
<p>
�ppettider:<br>
M�ndag 09-19<br>
Tis-Fre 09-18<br>
L�rdag 10-14<br>
S�ndag 12-16   
<p>
V�lkommen!', 'http://www.ostcamp.se', 'active', false, '2004-01-15 15:30:00', '2016-02-15 23:59:59', 'Polar, S�vsj�, Cabby, Kabe, Husvagn, Husvagnar, Husbil, Husbilar, Solifer, B�rstner, Pilot, Renault, Iveco, Hobby, Adria, ', '-', 'patrik', '-', '-', 'patrik', 'patrik', 14, 0, 1100);
INSERT INTO stores VALUES (14120, 'L�nestams Bilar AB', 'info_lonestamsbilar.com@blocket.se', '0142-201 07', '0142-36 90 17', 'Rydjav�gen 8', '59020', 'MANTORP', 'butik.jpg', '�terf�rs�ljare f�r Galloper, Mitsubishi', '<b>V�lkommen till L�nestams Bilar i Mantorp.</b><br>
I v�r nyrenoverade anl�ggning har vi, f�rutom nya & begagnade bilar.<br>
�ven service och reservdelar till din Mitsubishi.
<p>
�En mindre Mitsubishi �terf�rs�ljare med fokus p� personlig service.�
<p>
<b>�ppettider</b><br>
M�n-Fre 10.00-18.00<br>
L�rdag 10.00-13.00<p>

V�lkommen!', 'http://www.lonestamsbilar.com

', 'active', false, '2005-09-21 00:00:00', '2015-12-31 23:59:59', 'Audi, Honda, Mitsubishi, Nissan, Opel, Renault, Saab, Seat, Volkswagen, VW, Volvo, Galloper', '-', 'patrik', '-', '-', 'patrik', 'patrik', 14, 0, 1020);
INSERT INTO stores VALUES (2242, 'Bilhallen i Mj�lby AB', 'bilar_bilhallenmjolby.se@blocket.se', '0142-166 72', '0142-125 77', 'Sandkullagr�nd 1', '595 34', 'MJ�LBY', 'bilhallenmjol.gif', 'Nissan & Kia �terf�rs�ljare', 'Vi �r auktoriserade �terf�rs�ljare f�r NISSAN, KIA, GRECAV MOPEDBILAR och KYMCO SCOOTER.<br>
Det ger oss m�jligheten att tillhandah�lla allt fr�n billiga mopederscooter till l�tta motorcyklar, upp till lyxiga sedaner, fyrhjulsdrivna terr�ngkombi eller l�tta transportbilar.<p>

Vi b�de s�ljer och servar s�v�l dessa fordon som �vriga m�rken. Vi har ocks� ett brett utbud av begagnade bilar i de flesta prisl�gen. Dessa bilar har alltid genomg�tt Bildiagnos hos AB Svensk Bilprovning f�r att Du skall veta vad du k�per. Vi till�mpar nettopriss�ttning s� att Du alltid vet att Du f�r r�tt pris fr�n b�rjan.
<p>
V�r m�ls�ttning �r att l�sa Dina transporter p� ett s� f�rm�nligt s�tt att Du blir en trogen kund hos oss. 
<p>
N�r Du vill bes�ka oss hittar Du oss l�tt vid Mj�lbys v�stra infart p� Kungsh�ga (vi syns fr�n E4) och p� v�r tomt finns �ven en JET bensinstation.<p>
�ppettider:<br>
M�n-Fre: 09.00 - 18.00<br>
L�rdag(juni - augusti st�ngt): 10.00 - 13.00 ', 'http://www.bilhallenmjolby.se', 'active', false, '2005-05-09 00:00:00', '2016-10-01 23:59:59', 'Audi, Alfa Romeo, Ford, Kia, Nissan, Volkswagen, VW, Volvo', '-', 'patrik', '-', '-', 'patrik', 'patrik', 14, 0, 1020);
INSERT INTO stores VALUES (2293, 'Peugeot Norrk�ping AB', 'bilforsaljning_peugeotnorrkoping.com@blocket.se', '011-28 26 80', '011-28 26 89', 'Box 936, Moa Martinssons Gata 18', '601 19', 'NORRK�PING', 'pnorrkop.gif', '�terf�rs�ljare av Peugeot', 'Peugeot i Norrk�ping finns p� plats sedan augusti 2000.<br>
Vi erbjuder v�ra kunder en genuin upplevelse av design, komfort och personligt bem�tande.
<p>
Vi marknadsf�r Peugeot person och transportbilar.<br>
I v�r bilhall kan vi visa ett stort utbud av olika modeller och utrustningar.
<p>
Peugeot Norrk�ping �r ett komplett bilf�retag med
reservdelar/tillbeh�r och en fullutrustad serviceverkstad och pl�tverkstad.
Ackriditerad av Svensk Bilprovning och godk�nda att utf�ra AC- tillsyn/reperatur.
<p>
Vi p� Peugeot Norrk�ping h�lsar dig v�lkommen!', 'http://www.peugeotnorrkoping.com', 'active', false, '2003-05-14 11:04:00', '2016-05-16 23:59:59', 'Peugeot, Citro�n, Citroen, Ford, Skoda, Toyota, Volkswagen, VW, Saab, Volvo,', '-', 'patrik', '-', '-', 'patrik', 'patrik', 14, 0, 1020);
INSERT INTO stores VALUES (2304, 'Sluta Leta Bil AB', 'slutaletabil_telia.com@blocket.se', '011-12 10 00', '011-12 60 19', 'Exportgatan 25', '602 28', 'NORRK�PING', 'logo.gif', 'I spetsen f�r ett gott bilval', 'Hos oss f�r Du alltid ett personligt bem�tande och den h�gsta servicegraden.
<br>
Vi har mycket fina beg.bilar i lager, alla med garanti.
<p>
Vi finns �ven p�:<br>
Link�pingsv�gen 53<br>
602 36 Norrk�ping<br>
Tfn och Fax: 011-18 77 47

', 'http://www.slutaletabil.se', 'active', false, '2004-09-21 13:30:00', '2016-03-01 23:59:59', 'Alfa Romeo, Audi, BMW, Chevrolet, Chrysler, Citroen, Ford, Mercedes, Mitsubishi, Opel, Peugeot, Saab, Volkswagen, Volvo', '-', 'patrik', '-', '-', 'patrik', 'patrik', 14, 0, 1020);
INSERT INTO stores VALUES (14291, 'Westerlunds Audio Video', 'gavle_audiovideo.se@blocket.se', '026-664440', '026-664451', 'Drottninggatan 12', '803 20', 'G�VLE', 'logo_2.gif', 'Bra ljud & bild �r vanebildande...', 'Kedjan AUDIO VIDEO st�r f�r fackhandelskunskap med ett brett sortiment av v�lk�nda varum�rken. Vi finns p� mer �n 130 orter i landet och nu har vi �ven en butik p� n�tet som ger dig m�jlighet att handla av oss var i landet du �n befinner dig! 

V�lkommen att kontakta oss redan idag!
Specialitet:
Vi ombes�rjer hemk�rning och installation.

�ppettider:
M�ndag Till Fredag: 10:00 - 18:00
L�rdag: 10:00 - 15:00
S�ndag: St�ngt

', 'http://www.audiovideo.se
', 'active', false, '2005-10-14 00:00:00', '2015-11-30 23:00:00', 'audio, hifi, h�gtalare, bilstereo, dvd,', '-', 'patrik', '-', '-', 'patrik', 'patrik', 5, 0, 8040);
INSERT INTO stores VALUES (2395, 'Classic Garage AB', 'info_classic-garage.se@blocket.se', '016-13 88 80', '016-12 72 50', 'Hejargatan 17', '632 29', 'ESKILSTUNA', 'classic.gif', 'Bilar ut�ver det vanliga', 'Det man utf�r med n�je, g�r man ofta bra. D�rf�r ska Du anlita en entusiast, n�r du �r p� jakt efter en bil ut�ver det vanliga. 

', 'http://www.classic-garage.se', 'active', false, '2003-05-14 00:00:00', '2016-03-31 23:59:59', 'Alfa, Ferrari, Jaguar, Maserati, Mercedes, Porsche, Rolls-Royce, Volkswagen', '-', 'patrik', '-', '-', 'patrik', 'patrik', 12, 0, 1020);
INSERT INTO stores VALUES (2430, 'Bilgallerian i Katrineholm AB', 'andreas.bilgallerian.k_telia.com@blocket.se', '0150-126 60', '0150-122 70', 'Mossv�gen 5', '641 49', 'KATRINEHOLM', 'bilgallerian2.gif', '�terf�rs�ljare f�r Citro�n, MG, Rover', '<B>�ppettider:</B><br>
<li>M�n - fre: 10 - 18   
<li>L�rdag: 10 - 14   
<li>S�ndag: 12 � 15  
', 'http://www.bilgalleriankatrineholm.se', 'active', false, '2003-05-07 00:00:00', '2016-03-01 23:59:59', 'Citro�n, MG, Rover, BMW, Ford, Volvo', '-', 'patrik', '-', '-', 'patrik', 'patrik', 12, 0, 1020);
INSERT INTO stores VALUES (13272, 'Landrins Bil, Avesta', 'info_landrinsbil.se@blocket.se', '0226-57700', '0226-54095', 'Bergslagsv�gen 6', '774 30', 'AVESTA', 'LandrinsBil.gif', '�terf�rs�ljare f�r Mercedes, Nissan', 'Vi �r auktoriserade �terf�rs�ljare f�r Mercedes-Benz, Chrysler, Jeep, Nissan och Subaru. Landrins Bil finns b�de i V�ster�s och Avesta.
<p>
Landrins Bil �r idag exklusiva fullserviceanl�ggningar b�de i V�ster�s samt Avesta. 
<p>
Nyckeln till framg�ng ligger inte bara i att vi s�ljer n�gra av de b�sta produkterna som finns p� marknaden, vill vi infria dina f�rv�ntningar som �r v�r m�ls�ttning, m�ste vi visa att vi �r professionella b�de vid f�rs�ljningstillf�llet och p� eftermarknaden. Du skall alltid k�nna trygghet som bil�gare n�r du k�pt en bil p� Landrins Bil. 
<p>
Vi har ocks� det mesta n�r det g�ller praktiska och stilfulla original tillbeh�r till din bil, v�lkommen in f�r en demonstration och pris- information om monterat & klart p� bilen. ', 'http://www.landrinsbil.se
', 'active', false, '2004-10-29 00:00:00', '2016-05-01 23:59:59', 'Mercedes, Chrysler, Jeep, Nissan, Subaru, Audi, Renault, Saab, Toyota, Volkswagen, VW, Volvo', '-', 'patrik', '-', '-', 'patrik', 'patrik', 6, 0, 1020);
INSERT INTO stores VALUES (2432, 'Hallsten Bil AB', 'info_hallstenbil.nu@blocket.se', '0157-120 30', '0157-510 02', '�segatan 8', '642 37', 'FLEN', '-', '�terf�rs�ljare f�r Toyota', 'Bilar p� lager men - s�ker du en speciell bil, allt fr�n vanlig bil men kanske i en speciell f�rg, �rsmodell, prisklass, utrustning eller annat. Kanske s�ker du en lyxbil? - Kontakta oss om dina �nskem�l s� letar vi fram ett f�rslag!', 'http://www.hallstenbil.nu', 'active', false, '2003-05-07 00:00:00', '2016-04-01 23:59:59', 'Toyota', '-', 'patrik', '-', '-', 'patrik', 'patrik', 12, 0, 1020);
INSERT INTO stores VALUES (2443, 'Specialbilar i Str�ngn�s AB', 'info_specialbilar.com@blocket.se', '0152-134 24', '0152-13426', 'Plogstigen 4', '645 41', 'STR�NGN�S', 'logga.gif', 'M�rkesoberoende handlare', 'Specialbilar AB s�ljer, byter och k�per (amerikanska, europeiska) bilar i
b�de bensin och dieselutf�rande.
Nytt och begagnat.
<p>
Vi har varit i farten sedan -88, s� v�rt kunnande om bilar �r gott.<br>
V�lkommen att ringa eller bes�ka oss, vi �r placerade i Str�ngn�s
Industriomr�de.', 'http://www.specialbilar.com', 'active', false, '2005-04-04 00:00:00', '2016-04-15 23:59:59', 'Audi, Chevrolet, Chrysler, Ford, Mercedes, Skoda, Volvo, Toyota, Volkswagen, VW ', '-', 'patrik', '-', '-', 'patrik', 'patrik', 12, 0, 1020);
INSERT INTO stores VALUES (2496, 'Husvagnspoolen AB', 'info_husvagnspoolen.se@blocket.se', '054-52 46 66', '054-52 16 66', 'Skraggev�gen 8', '663 41', 'HAMMAR�', 'husvagnspoolen.gif', 'Husvagnar i Hammr�', 'Husvagnspoolen grundades 1983. Ni hittar oss p� Nolg�rds industriomr�de i Hammar�. Det �r inte alls sv�rt att hitta hit. F�lj bara skyltning till Hammar� fr�n E18 och k�r sedan mot Nolg�rds industriomr�de. 
<p>
Vi har idag 5 anst�llda varav tv� personer i v�r verkstad. Vi finns i moderna lokaler p� 1180 m2 med en stor hall f�r vagnar och t�lt samt butik och verkstad.
', 'http://www.husvagnspoolen.se', 'active', false, '2003-04-30 00:00:00', '2015-12-31 00:00:00', 'Polar, Solifer, Knaus, Husvagn, Husvagnar, Husbil, Husbilar
', '-', 'patrik', '-', '-', 'patrik', 'patrik', 7, 0, 1100);
INSERT INTO stores VALUES (2515, 'H�kans Bilar AB', 'info_hakansbilar.se@blocket.se', '054-87 49 49', '054-87 33 38', 'Framg�rdsv�gen 2', '667 21', 'FORSHAGA', 'hakansbilar.gif', 'Nyare begagnade bilar', 'H�kan startade verksamheten i Forshaga 1985. V�r specialit�: Nyare begagnade bilar, Vi har alltid ett femtiotal bilar i lager.', 'http://www.hakansbilar.se', 'active', false, '2005-05-11 00:00:00', '2016-06-01 23:59:59', 'Audi, BMW, Ford, Mercedes, Saab, Volkswagen, Volvo', '-', 'patrik', '-', '-', 'patrik', 'patrik', 7, 0, 1020);
INSERT INTO stores VALUES (2524, 'Olof Ericson Bil AB', 'anders.ericson_olofericson.se@blocket.se, lennart.johnsson_olofericson.se@blocket.se', '0570-844 15, 844 16', '0570-71 19 02', 'Industrigatan 4', '671 27', 'ARVIKA', 'olife.GIF', '�terf�rs�ljare f�r Volkswagen, Audi & Skoda', '<B>Olof Ericson Bil AB �r ett bilhandelsf�retag i v�stra V�rmland som verkat i branschen i mer �n 50 �r.</B>  
<p>
Vi �r �terf�rs�ljare f�r Volkswagen personbilar, lastbilar och bussar samt Skoda. V�r fullserviceverkstad �r auktoriserad f�r Volkswagen, Audi och Skoda samt ackrediterad f�r kontroll av egna reparationer.
<p>
F�retaget �r certifierat f�r kvalitet och milj� enligt ISO 9001:2000 resp. 14001  
', 'http://www.olofericson.se', 'active', false, '2003-08-22 11:00:00', '2016-02-22 23:59:59', 'Volkswagen, Audi, Skoda, Mercedes, Opel, Renault, Volvo', '-', 'patrik', '-', '-', 'patrik', 'patrik', 7, 0, 1020);
INSERT INTO stores VALUES (2552, 'JG:S Husvagnar AB', 'info_g-grahn.se@blocket.se', '0550-131 13', '0550-41 16 56', 'Box 225, Bodalsv�gen 1', '681 25', 'KRISTINEHAMN', 'cabbycenter.gif', 'Cabby Center i Kristinehamn - Sveriges n�rmaste Cabby handlare', 'Cabby och Home-Car �terf�rs�ljare, samt begagnade husvagnar.
<p>
Hos oss finns vagnen f�r dig, b�de nytt och begagnat. Vi har alltid ett stort antal begagnade vagnar i lager.
<p>
�ppettider:<br>
M�ndag-Fredag 8:00-18:00<br>
samt vissa helger


', 'http://www.jgshusvagnar.se', 'active', false, '2003-05-23 15:10:00', '2016-07-15 23:59:59', 'Cabby, Home-Car, begagnade husvagnar, nya husvagnar, f�rt�lt, Isabella, Ventura', '-', 'patrik', '-', '-', 'patrik', 'patrik', 7, 0, 1100);
INSERT INTO stores VALUES (2559, 'Nordic Husbilar AB', 'info_nordic-husbilar.se@blocket.se', '0550-834 34', '0550-831 38', 'Drevsta Ind. Omr. Hantverkargatan 10', '681 42', 'KRISTINEHAMN', 'logo.gif', 'Upplagt f�r Upplevelser!', 'Vi har under 12 verksamma �r byggt upp ett husbilsm�rke p� den Svenska marknaden, NORDIC husbilar ab, och investerat i egen industrifastighet, inomhushall, limpress, snickerimaskiner och servicehall.
<p>
NORDIC husbilar ab erbjuder idag 12 olika modeller. Vi skr�ddarsyr �ven husbilar �t f�retag och privatpersoner. V�ra husbilar �r byggda f�r �ret runt bruk med inredning i svenskhantverks tradition.
<p>
Till v�ra nya husbilsk�pare ger vi:
<br>
- 10 �rs Fuktgaranti<br>
- 3 �rs Funktionsgaranti<br>
- 2 �rs Nybilsgaranti', 'www.nordic-husbilar.se
', 'active', false, '2004-01-13 11:06:00', '2016-03-01 23:59:59', 'Husbil, Husbilar, Nordic, Hobby, Frankia, Pilote, Knaus, Swift, Weinsberg, MB ', '-', 'patrik', '-', '-', 'patrik', 'patrik', 7, 0, 1100);
INSERT INTO stores VALUES (2563, 'Husbilslandet AB', '-', '0550-293 85', '0550-293 86', 'B�ckhems G�rd', '681 91', 'KRISTINEHAMN', 'husbilslandet.gif', '�k till husbilslandet - det r�cker!', 'H�r hittar Ni nya Knaus samt alltid ett 50-tal begagnade till de absolut r�tta priserna.
<p>
V�lkomna ut till oss p� landet!', 'http://www.husbilslandet.com', 'active', false, '2003-05-20 16:07:00', '2017-01-15 23:59:59', 'Knaus, Adriatik, B�rstner, Dethleffs, Elnagh, Mercedes, Hobby, Karmann, Nordic, Pilote, Riviera, Swift, Tabbert, Weinsberg, Karman, Renault, Tripple', '-', 'patrik', '-', '-', 'patrik', 'patrik', 7, 0, 1100);
INSERT INTO stores VALUES (2566, 'Seffel Bil AB', 'goran.bilforsalj_seffelbil.se@blocket.se', '0590-152 30', '0590-102 30', 'Hertig Karlsg. 25', '682 22', 'FILIPSTAD', 'seffel.gif', '�terf�rs�ljare f�r Ford, Kia', 'Sedan 1976 s� har f�retaget varit auktoriserad Fordhandel. Vi s�ljer nya Ford och begagnade bilar s�v�l personbilar som l�tta transportfordon. Vi har biluthyrning, �ven buss. <p>
Seffel Bil �r en komplett verkstad som utf�r service och arbete med pl�t, rekond, bilrutor, d�ck, hjulinst�llningar m.m. Vi har �ven f�rs�ljning av tillbeh�r och reservdelar. Personalen p� Seffel Bil AB har sammanlagt 115 �rs erfarenhet inom bilbranschen. 
', 'http://www.seffelbil.se', 'active', false, '2005-04-04 00:00:00', '2016-07-15 23:59:59', 'BMW, Ford, KIA, ', '-', 'patrik', '-', '-', 'patrik', 'patrik', 7, 0, 1020);
INSERT INTO stores VALUES (13765, 'KS Custom & Speed Parts AB', 'bilar_vwcustomspeed.com@blocket.se', '042 - 22 59 43', '042 - 22 59 44', '�rtgatan 25', '260 61', 'HYLLINGE', 'logga.gif', 'M�rkesoberoende handlare', 'CSP Bil Hyllinge SAAB specialisten i Hyllinge!<br>V�lkommen till CSP Bil i Hyllinge.
CSP Bil hittar du i Hyllinge strax utanf�r Helsingborg,
n�ra fr�n b�de E4 och E6.
Hos oss hittar du alltid fr�scha beg. SAABar.
<p>
Vi s�ljer �ven reservdelar och tillbeh�r till VW.<br>
Dessa hittar du h�r:
<A HREF=" http://www.vwcsp.se/" TARGET="_blank">
http://www.vwcsp.se/</a>
<p>
�ppettider
M�ndag-fredag 8.00-18.00, lunchst�ngt 13.00-14.00<br>
L�rdagar �r endast butiken �ppen 10.00-13.00
', 'http://www.cspbil.se', 'active', false, '2005-04-05 00:00:00', '2016-04-15 23:59:59', 'Mercedes, Saab, Volvo ', '-', 'patrik', '-', '-', 'patrik', 'patrik', 23, 3, 1020);
INSERT INTO stores VALUES (2572, 'Gunnar Lindkvist Bil AB', '-', '0563-520 00', '0563-520 60', 'Fabriksv�gen 5', '684 23', 'MUNKFORS', 'GLindkvist.gif', '�terf�rs�ljare Audi, Volkswagen & Skoda', '<p>
Vi s�ljer prim�rt Audi, Volkswagen & Skoda.
<p>
Ni kan �ven kontakta oss p� mobil nummer : 070-33 33 600
<p>', 'http://www.gunnarlindkvistbil.se', 'active', false, '2003-08-11 15:00:00', '2016-01-15 23:59:59', 'Audi, Ford, Mercedes, Opel, Peugeot, Renault, Saab, Skoda, Volkswagen, ', '-', 'patrik', '-', '-', 'patrik', 'patrik', 7, 0, 1020);
INSERT INTO stores VALUES (2610, 'J�rgen Hamr�ns Bil', 'info_hamren.t.se@blocket.se', '019-57 30 35', '019-57 30 50', 'Gamla E3:an Stene', '692 00', 'KUMLA', 'hamren.GIF', 'Fr�scha bilar alltid med garanti!', 'Bilar i klassen 20 000 till 80 000 kr och �ven Pick-Up.
', 'http://www.hamren.t.se', 'active', false, '2003-08-14 15:00:00', '2016-03-01 23:59:00', 'Audi, BMW, Ford, Pick-Up, Chrysler, Hundai, Mercedes,Opel, Nissan
', '-', 'patrik', '-', '-', 'patrik', 'patrik', 8, 0, 1020);
INSERT INTO stores VALUES (2621, '�BAB, �rebro', 'info_obabbil.se@blocket.se', '019-10 87 20', '019-10 76 77', 'B�ckv�gen 60', '70375', '�REBRO', 'obab.gif', 'KIA och SUBARU �terf�rs�ljare', '�BAB �rebro Bilbolag AB finns p� B�ckv�gen 60 i �rebro. 
<p>Vi �r �terf�rs�ljare f�r SUBARU och KIA.
<p>Vi p� �BAB har ett stort urval begagnade bilar.

<p>Vi samarbetar �ven med ledande finansbolag f�r att du skall f� bra finansiering.
', 'http://www.obabbil.se', 'active', false, '2004-08-12 16:00:00', '2016-03-01 23:59:59', 'KIA,SUBARU, ', '-', 'patrik', '-', '-', 'patrik', 'patrik', 8, 0, 1020);
INSERT INTO stores VALUES (2643, 'Bilpunkten i �rebro AB', 'jonas_bilpunkten.com@blocket.se', '019-26 00 55', '019-36 40 55', 'Gryt Industriomr�de 5602, (Norra Bro bakom Ory)', '702 21', '�REBRO', 'bilpunkten_logo.gif', 'M�rkesoberoende handlare', '�ppettider: 
<li>Vardagar 10 - 18
<li>L�rdag 10 - 15
<br>�ven kv�llar och helger vid �verkommelse
<br>Mobilnr: 070-643 88 08

', 'http://www.bilpunkten.com', 'active', false, '2003-05-05 00:00:00', '2016-09-01 23:59:59', 'Audi, BMW, Mazda, Opel, Toyota, Volvo, Peugeot, Volkswagen, Ford, Saab, Mercedes, Nissan,', '-', 'patrik', '-', '-', 'patrik', 'patrik', 8, 0, 1020);
INSERT INTO stores VALUES (2650, 'Jonsson Bil AB', 'fredrick.hafstrom_jonssonbil.se@blocket.se', '019-20 73 03', '019-20 73 16', 'Nastagatan 18', '702 27', '�REBRO', 'jonssonbil.gif', 'Peugeot �terf�rs�ljare', 'Jonsson Bil �r ett bilhandelsf�retag som etablerades 1977 i �rebro.
<p>
Vi finns i egna lokaler p� Aspholmen. 
<p>
Vi s�ljer nya Peugeot alltifr�n lilla smidiga 206, folkbilen 307, tj�nstebilen 407, prestigebilen 607 och familjebilen 807. Vi har unika transportbilar i form av Partner (�ven som kombi) Expert samt lastbilar i form av Peugeot Boxer. 
<p>
Vi har alltid en stor sortering av begagnade bilar till bra priser, alla med VDN deklaration och ett tryggt k�p.
', 'http://www.jonssonbil.se', 'active', false, '2003-09-03 00:00:00', '2016-03-01 23:59:59', 'Peugeot, Audi, BMW, Cadillac, Chevrolet, Citro�n, Ford, Hyundai, Mercedes, Mitsubishi, Nissan, Opel, Renault, Saab, Skoda, Suzuki, Toyota, Volkswagen, Volvo', '-', 'patrik', '-', '-', 'patrik', 'patrik', 8, 0, 1020);
INSERT INTO stores VALUES (2652, 'Marieberg Bil AB', 'e-mail_mariebergbil.se@blocket.se', '019-22 39 49', '019-22 39 48', 'Firmav�gen 2', '702 31', '�REBRO', 'marieberg.gif', 'F�rs�ljning - Service - Verkstad - Import - Export', 'Marieberg bil ab startades 1999.
<p>
Vi finns i fina lokaler vid Mariebergs k�pcentrum i ca 1 mil s�der om �rebro.
<br>
Vi handlar i huvudsak med importbilar.
<br>

Vi har alltid ca 30-40 bilar i v�r utst�llningshall klara f�r leverans.
<br>

Vi utf�r service och reparationer i v�r serviceverkstad.
<br>

Vi finansierar g�rna ditt bilk�p via v�r samarbetspartner Nordea Finans.
<p>


V�lkommen till oss.

', 'http://www.mariebergbil.se', 'active', false, '2003-05-01 00:00:00', '2016-01-01 00:00:00', 'Audi, BMW, Chrysler, Jaguar, Jeep, Lincoln, Mercedes, Mitsubishi, Nissan, Pontiac, Volkswagen', '-', 'patrik', '-', '-', 'patrik', 'patrik', 8, 0, 1020);
INSERT INTO stores VALUES (2662, 'Bilhuset i �rebro AB', 'bilhusetorebro_telia.com@blocket.se', '019-678 20 90', '019-31 17 88', 'Skjutbanev�gen 4', '703 69', '�REBRO', 'bilhuset.gif', 'Ditt bilvaruhus i norra �rebro', 'Du har nu hittat till en lite annorlunda bilhandlare, Bilhuset i �rebro AB. Vi v�gar p�st� att vi bryr oss lite mer �n v�ra konkurrenter. Detta f�r att ge dig som kund en bil att trivas med. Fordonet du k�per �r ofta nyservat, inte s�llan har det �ven f�tt nya d�ck. D�rf�r kan vi garantera dig en trivsam och bekymmersfri bilupplevelse.

', 'http://www.bilhuset.org', 'active', false, '2003-05-12 00:00:00', '2016-03-01 00:00:00', 'Suzuki, Saab, Volvo, VW, Audi', '-', 'patrik', '-', '-', 'patrik', 'patrik', 8, 0, 1020);
INSERT INTO stores VALUES (14153, 'Bilpoolen �rebro AB', 'jorgen_bilpoolen.se@blocket.se', '019-23 24 23', '019-23 24 33', 'K�viv�gen 13', '703 75', '�REBRO', 'butik.gif', '�terf�rs�ljare f�r MG, Rover', '<b>V�lkommen till Bilpoolen �rebro AB</b><p>

MRF Ansluten Bilhandel<br>
S�ljer och k�per NYA och Begagnade bilar.<p>

Service och eventuella reparationer tar vi hand om p� v�r verkstad. <br>
Naturligtvis �r vi en "kontrollerad verkstad med den gr�na punkten"<p>

Reservdelar f�r v�ra m�rken �r en sj�lvklarhet, d�ck och �vriga tillbeh�r f�r din bil k�per du h�r till bra priser.
<p>

Vi st�ller upp f�r <b>DIG</b> och <b>DIN</b> bil.<p>

�ppettider:<br>
<b>Bil F�rs�ljning:</b><br>
M�-Fre 10.00 - 18.00<br>
L�rdag 10.00 - 14.00<br>
<p>
<b>Service & Reservdelar:</b><br>
M�-Tor 07.00 - 17.00<br>
Fredag OBS tiden! <br>
07.00 - 15.00<br>
L�rdag St�ngt<br>
Lunchst�ngt <br>
12.15 - 13.00<p>



V�lkommen!', 'http://www.bilpoolen.se

', 'active', false, '2005-09-28 00:00:00', '2015-12-31 23:59:59', 'Alfa Romeo, Fiat, Hyundai, MG, Rover,  Volkswagen, VW, Volvo', '-', 'patrik', '-', '-', 'patrik', 'patrik', 8, 0, 1020);
INSERT INTO stores VALUES (2677, 'Bilmagasinet i �rebro AB', 'bilmagasinet_telia.com@blocket.se', '019-31 05 40', '019-31 05 41', 'Kyrkv�gen 49, Lill�n', '703 75', '�REBRO', 'bilmagasinet.gif', 'Alltid bilar till l�gpris och i skick som t�l att j�mf�ras', 'Vi har alltid bilar till l�gpris och i skick som t�l att j�mf�ras!
<p>
Vi erbjuder �ven f�rm�nligt bill�n till bra r�nta.
<p>
V�lkommen �nskar Arne Peterson och Kurt Nylund.', 'http://www.bilmagasinet.nu', 'active', false, '2003-05-05 00:00:00', '2016-05-07 23:59:59', 'BMW, Chevrolet, Renault, Saab, Volkswagen, Volvo', '-', 'patrik', '-', '-', 'patrik', 'patrik', 8, 0, 1020);
INSERT INTO stores VALUES (2688, 'Samuel Karlssons Bilar AB', 'peter_fordbilar.com@blocket.se', '0581-64 56 60', '0581-148 87', '�rebrov�gen 34', '711 91', 'LINDESBERG', 'samkar_logo.gif', 'Som din Fordhandlare p� n�tet, s� st�r vi alltid till tj�nst f�r Er', 'Vi kan ge Er all information och r�d om Er nya och begagnade Ford.
<p>
Letar du efter ny eller begagnad bil? 
Vi kan presentera hela Fords breda modellprogram, finna den r�tta finansierings - l�sningen och ge dig s�kerhet f�r en effektiv service och v�gledning efter k�pet.
<p>
Om du �r nyfiken p� v�ra Nya Ford bilar s� �r du alltid v�lkommen till oss f�r en provk�rning och en kopp kaffe, eller navigera Er runt p� v�r hemsida.
<p>
Ni kan �ven ringa direkt till v�r bilf�rs�ljning p�<br>tel. 0581-64 56 63 alt. 070-631 29 69
<p>
Vi hyr �ven ut l�genheter.<br> Mera info hittar ni p�
http://www.bobranu.se
<p>
V�lkomna!', 'http://www.fordbilar.com', 'active', false, '2003-05-07 00:00:00', '2016-07-01 23:59:59', 'Ford', '-', 'patrik', '-', '-', 'patrik', 'patrik', 8, 0, 1020);
INSERT INTO stores VALUES (2742, 'K. �hman Bil & Maskin AB', 'stefan.ohman_mbox301.swipnet.se@blocket.se', '021-280 56', '021-280 56', 'L�ngby G�rd', '725 96', 'V�STER�S', 'kohman.gif', 'Vi s�ljer mest Volvo men �ven andra m�rken', 'Begagnade bilar fr�mst �rsmodeller 95-99. 

<p>Ni hittar oss p� L�ngbyg�rd i utkanten av V�ster�s. 

<p>F�retaget grundades f�r drygt 20 �r sedan av Kenneth �hman, nuvarande �gare �r sonen Stefan �hman som jobbat i f�retaget i 6 �r.

', 'http://www.kohman.nu', 'active', false, '2003-05-07 00:00:00', '2016-05-01 23:59:59', 'Volvo, Audi, BMW, Citroen, Dodge, Ford, Honda, Jeep, Mercedes, Hyundai, Mitsubishi, Opel, Peugeot, Renault, Porsche, Saab, Toyota, Volkswagen, Volvo', '-', 'patrik', '-', '-', 'patrik', 'patrik', 9, 0, 1020);
INSERT INTO stores VALUES (2769, 'Arn� Bil & MC AB', 'kontakt_arnobil.se@blocket.se', '0224-125 92', '0224-125 91', 'Stadsskogen R�vtorpet 100', '733 97', 'SALA', 'arno.gif', 'Handla Tryggt - 40�r i branschen', '-', 'http://www.arnobil.se', 'active', false, '2003-04-25 13:32:00', '2016-09-01 23:59:59', 'Chevrolet, Honda, KTM, Kawasaki, Opel, Saab, Suzuki, Volkswagen, Volvo, Yamaha, ', '-', 'patrik', '-', '-', 'patrik', 'patrik', 9, 0, 1140);
INSERT INTO stores VALUES (2785, 'Andtfolks Bil AB', 'andfolks_bytbil.com@blocket.se', '0223-146 20', '0223-14820', 'Brinellv�gen 7', '73740', 'FAGERSTA', 'butik_logga.gif', 'M�rkesoberoende handlare', 'V�lkommen vi hj�lper dig g�rna till en god Bilaff�r.<p>
Nu kan vi l�mna 12 m�naders garanti kom in s� ber�ttar vi mer.<p>
Egen verkstad  tel: 0223-145 30.<br>
Suomea puhutaan <p>
�ppettider: <br>
M�ndag - Torsdag. 10.00 - 18.00 <br>
Fredag.10.00 -17.00 <br>
L�rdagar. 10.00 - 14.00 <p>
', 'http://www.andtfolks.com', 'active', false, '2005-09-19 00:00:00', '2015-12-31 23:59:59', 'Audi, BMW, Ford, Mitsubishi, Volksagen, VW', '-', 'patrik', '-', '-', 'patrik', 'patrik', 9, 0, 1020);
INSERT INTO stores VALUES (2794, 'Matton Marin AB', 'mattonmarin_telia.com@blocket.se', '0173-214 00', '0173-217 10', 'Hamnpiren 2', '742 31', '�STHAMMAR', 'logo.gif', 'Din b�thandlare!', 'F�rs�ljning av Ryds & Silver b�tar med Mercury motorer.
<p>
Motorer fr�n Yanmar Marine, Tohatsu, Johnson, Evinrude & Honda.
<p>
V�lkomna!
', 'http://www.mattonmarin.se/', 'active', false, '2004-02-24 11:13:00', '2016-03-01 23:59:59', 'B�t, B�tar, Ryds, Silver, Yanmar Marine, Tohatsu, Johnson, Evinrude, Honda,', '-', 'patrik', '-', '-', 'patrik', 'patrik', 10, 0, 1060);
INSERT INTO stores VALUES (2796, 'Cassman Bil AB', 'bengt_cassmanbil.se@blocket.se', '0173-103 50', '0173-103 15', 'Industriv�gen 11', '742 34', '�STHAMMAR', 'cassman.gif', '�terf�rs�ljare Volkswagen, Audi & Skoda', 'F�retaget startades 1993 av Bengt Cassman med f�rs�ljning av Volkswagen och Audi.
        1998 kom Ronnie Andersson med som kompanjon samtidigt som vi flyttade till nya lokaler p� Industriv�gen 11.
D�rtill bildade vi dotterbolaget Bilteam i �sthammar d�r vi marknadsf�r och s�ljer Skoda.
<p>

Auktoriserad verkstad.', 'http://www.cassmanbil.se', 'active', false, '2003-09-18 00:00:00', '2016-06-15 23:59:59', 'Audi, Skoda, Volkswagen, Honda, Mitsubishi, Renault, Seat, Volvo', '-', 'patrik', '-', '-', 'patrik', 'patrik', 10, 0, 1020);
INSERT INTO stores VALUES (2819, 'Helins Bilcentrum AB', 'info_helinsbil.se@blocket.se', '0171-46 80 90', '-', 'Dragr�nnan 1', '746 50', 'B�LSTA', 'Helins.gif', 'S�tter alltid kunden i centrum', 'V�lkommen till Helins Bilcentrum. Vi startade v�r verksamhet i B�lsta 1983.<br>
V�r aff�rsid� �r att alltid s�tta kunden i centrum. Kom in och se vad vi kan erbjuda dig. 
<p>
Tel : 08-34 59 00<p>
V�lkommen 
', 'http://www.helinsbil.se', 'active', false, '2003-04-30 00:00:00', '2016-11-01 23:59:59', 'Mitsubishi, Hyundai, Fiat, Alfa Romeo', '-', 'patrik', '-', '-', 'patrik', 'patrik', 10, 0, 1020);
INSERT INTO stores VALUES (13538, 'Johans MC', 'info_johansmc.se@blocket.se', '031- 22 00 49', '031- 22 00 39', 'Gamla Tuvev�gen 4', '417 05', 'G�TEBORG', 'nya.gif', 'Nordens st�rsta Kawasaki och Triumph center', 'Nordens st�rsta Kawasaki och Triumph center, 
20 �r i branschen. Motorcyklar till tusen! 
<p> 
Ditt kompletta MC-varuhus, vi har nya och begagnade 
motorcyklar, kl�der och tillbeh�r f�r alla smaker. Vi har 
�ven erfaren verkstad och vinterf�rvaring i larmat 
varmgarage f�r din �gonsten. Vi har n�stan alla modeller 
av Kawasaki och Triumph f�r provk�rning. 
<p> 
I v�r butik har vi all t�nkbar utrustning till dig och din MC. 
V�r personal har alltid tid att hj�lpa dig att hitta just det 
du beh�ver.
<p> 
�ppettider:<br>
M�n - Tors 10:00 - 18:00<br>
Fredag      10:00 - 17:00<br>
L�rdag      10:00 - 13:00<br>
Lunch       13:00 - 14:00<p>', 'http://www.johansmc.se/
', 'active', false, '2005-02-02 00:00:00', '2016-04-15 23:59:59', 'Kawasaki, Triumph, Yamaha, Honda, Aprilia, Harley-Davidson, Suzuki, ', '-', 'patrik', '-', '-', 'patrik', 'patrik', 15, 0, 1140);
INSERT INTO stores VALUES (2841, 'ab g persson', 'roger.ericsson_abgpersson.se@blocket.se ,  johan.smedsjo_abgpersson.se@blocket.se', '018-17 17 00', '018-69 41 74', 'Kungsgatan 103', '753 18', 'UPPSALA', 'logo.gif', 'Det finns m�nga s�tt att f�rdas p�!', 'Vi p� ab g.persson f�rdas fram�t i v�r utveckling med ny teknik, nya aff�rskoncept, ut�kad kapacitet och kompetens f�r att kunna m�ta v�ra kunders framtida krav p� sitt bil�gande.
<p>
Vi vill att du som kund ska k�nna att vi �r Uppsalas ledande bilf�retag.
<p>
V�ra �ppettider:<br>
Bilf�rs�ljning m�n-fre 08.00 - 18.00<br> 
Bilf�rs�ljning l�rdagar 11.00 - 15:00<br>
Bilf�rs�ljning s�ndagar St�ngt. Kunder h�nvisas till Autlet.
<p>
CALLCENTER Service/Reservdelar 07.00 - 18.00 
<p> 
Expresservice m�n-tors 07.00 - 16.00<br>
Expresservice fredagar. 07.00 - 14.30.
<p> 
Biluthyrning - Europcar 07.00 - 18.00
<p>
V�lkommna att bes�ka v�r hemsida f�r ytterligare information eller kom in till oss direkt.', 'http://www.abgpersson.se/', 'active', false, '2004-01-15 15:00:00', '2016-09-15 23:59:59', 'Audi, Volkswagen, VW, Skoda, Seat,', '-', 'patrik', '-', '-', 'patrik', 'patrik', 10, 0, 1020);
INSERT INTO stores VALUES (2852, 'Bilvalet i Enk�ping AB', 'info_bilvalet.com@blocket.se', '0171 - 244 00', '0171 - 246 30', 'Traktorgatan 5', '745 37', 'ENK�PING', '16-12-04.gif', 'Citro�n �terf�rs�ljare', 'V�lkommen till Enk�pings b�sta Bilval!
<p>
Bilvalet i Enk�ping AB �r din Citro�n �terf�rs�ljare i Enk�ping.
Bilvalet �r en av Enk�pings st�rsta begagnat handlare v�ra specialiteter �r Volvo, Saab, Ford & Volkswagen (vi har alltid minst 60 begbilar i lager), vi garanterar att vi har r�tt pris p� r�tt bil!', 'http://www.bilvalet.com', 'active', false, '2003-05-19 14:08:00', '2016-07-01 23:59:59', 'Citroen, Alfa, Audi, BMW, Fiat, Ford, Mitsubishi, Opel, Renault, Saab, Volkswagen, Volvo', '-', 'patrik', '-', '-', 'patrik', 'patrik', 10, 0, 1020);
INSERT INTO stores VALUES (2863, 'Upplands Bilforum AB', 'ted_upplandsbilforum.se@blocket.se', '018-13 13 10', '018-13 13 20', 'Fyrislundsgatan 74', '754 50', 'UPPSALA', '-', 'Auktoriserad �terf�rs�ljare av KIA Motors.', 'KIA Dealer of the year 2004.
<p>
Vi har ett n�ra samarbete med MotorM�nnens Testcenter.
<p>
�ppet M�n-Fre 10-18 L�r 10-14
<p>
V�lkommen till oss f�r ett tryggt k�p.
<p>
<a href="mailto:ted_upplandsbilforum.se" target="_blank">ted@upplandsbilforum.se</a>
<br>
<a href="mailto:lg@upplandsbilforum.se" target="_blank">lg@upplandsbilforum.se</a>
<br>
<a href="mailto:peter@upplandsbilforum.se" target="_blank">peter@upplandsbilforum.se</a>@blocket.se', 'http://www.upplandsbilforum.se', 'active', false, '2003-05-06 00:00:00', '2016-03-01 23:59:59', 'KIA,', '-', 'patrik', '-', '-', 'patrik', 'patrik', 10, 0, 1020);
INSERT INTO stores VALUES (2892, 'Liljeblads Bil AB', 'info.liljeblad_liljebladbil.se@blocket.se', '0176-159 00', '0176-181 40', 'Stockholmsv�gen 26', '761 43', 'NORRT�LJE', 'liljebladsbil_logo.gif', '�terf�rs�ljare f�r Mitsubishi, Peugeot.', 'Vi f�r ofta h�ra v�ra kunder s�ga �Vi ska g� till Liljeblads f�r att k�pa bil�. F�r oss betyder det mycket! V�r roll �r r�dgivande, byggt p� f�rtroende. Ni ska k�nna Er trygga och N�jda som kunder.
<p>
V�r verksamhet startade redan 1969. �r 1981 b�rjade vi marknadsf�ra Peugeot och Mitsubishi, Sveriges �ldsta �terf�rs�ljare.
<p>
V�r framtidsvision �r att vara
det ledande innovativa f�retaget.
<br>
V�r aff�rsid� som ska b�ra oss dit �r att
LiljebladsBil ska vara r�dgivande
och hj�lpa v�ra kunder fatta r�tt beslut.
<br>
Vi ska upplevas som kundens f�rstahandsval
Trygghet ska k�nneteckna v�rt f�retag.
<p>
V�lkommen!', 'http://www.liljebladbil.se/', 'active', false, '2004-03-12 09:30:00', '2016-03-15 23:59:59', 'Peugeot, Mitsubishi, Bilar, Chrysler, Citro�n, Opel, Renault, Saab, Seat, Toyota, Volvo,', '-', 'patrik', '-', '-', 'patrik', 'patrik', 11, 0, 1020);
INSERT INTO store_users(store_id,email) VALUES(2841, 'roger.ericsson_abgpersson.se@blocket.se');
INSERT INTO stores VALUES (2920, 'Carnila AB', 'carnila_jobbet.utfors.se@blocket.se', '0240-144 45', '0240-144 95', 'Knipes v�g 1', '771 50', 'LUDVIKA', '-', 'F�rs�ljning av transportfordon.', 'V�lkommen till Carnila AB Vi �r ett f�retag i Ludvika som specialiserat sig att s�lja Mercedes transportbilar till f�rm�nliga priser.', 'http://www.carnila.se/', 'active', false, '2003-04-22 00:00:00', '2016-07-01 23:59:59', 'Mercedes, Sprinter, Atego', '-', 'patrik', '-', '-', 'patrik', 'patrik', 6, 0, 1020);
INSERT INTO stores VALUES (2925, 'Hans Persson Bil  AB i Avesta', 'info_hansperssonbil.se@blocket.se', '0226-530 30', '0226-536 05', 'Bergn�sv�gen 2', '774 21', 'AVESTA', 'hanspersson_logo.gif', '�terf�rs�ljare f�r Renault, Volvo', 'Ett bilk�p �r en stor investering och vi tar det h�r p� fullaste allvar. Vi vill g�ra allt f�r att hj�lpa dig v�lja r�tt. Bilar skall man naturligtvis se och uppleva i verkligheten. Kom in s� skall du f� k�nna p�, sitta i och provk�ra den bil, eller de bilar, som kan vara intressanta f�r dig.
<p>
Ni �r v�lkommen att kontakta n�gon av v�ra s�ljare:<br>
Mikael Waksam<br>
Tel: 0226-530 37<br>
E-post: <a href="mailto:mikael.waksam_hansperssonbil.se">mikael.waksam@hansperssonbil.se</a>
<p>
Joakim Enocksson<br>
Tel: 0226-530 38<br>
E-post: <a href="mailto:joakim.enocksson@hansperssonbil.se">joakim.enocksson@hansperssonbil.se</a>
<p>
�ppettider:<br>
M�ndag - fredag: 9.00 - 18.00 <br>
L�rdag: 10.00 - 14.00 <br>
@blocket.se', 'http://www.hansperssonbil.se', 'active', false, '2003-04-25 00:00:02', '2016-12-31 23:59:59', 'Volvo, Renault,', '-', 'patrik', '-', '-', 'patrik', 'patrik', 6, 0, 1020);
INSERT INTO stores VALUES (2953, 'L Rosells Bilar KB', '-', '073 - 648 13 17', '0241 - 10916', 'T�gtv�gen 12', '780 41', 'GAGNEF', '-', 'K�PER-S�LJER-BYTER', 'Vi �r specialiserade p� personbilar och l�tta transportbilar.<br>
�r m�rkesoberoende handlare

<p>
Tel: 0241-109 16<br>
Mobil: 073-648 13 17
<p>
<b>Intresseanm�lningar sker via mobil,ej via mail</b><p>
�ppettider enligt �verenskommelse, ring 073-648 13 17 f�re bes�k.
<p>
<b>Varmt v�lkommen!</b>', '-', 'active', false, '2003-04-17 00:00:00', '2016-03-15 23:59:59', 'Opel, Volvo, Seat, Volkswagen, VW, ', '-', 'patrik', '-', '-', 'patrik', 'patrik', 6, 0, 1020);
INSERT INTO stores VALUES (2972, 'Haralds B�tar AB', 'haraldsbatar_telia.com@blocket.se', '0243-130 19', '0243-22 82 70', 'Hugo Hedstr�ms v�g 12', '781 72', 'BORL�NGE', 'haralds.gif', 'Genom stora ink�p kan vi h�lla b�sta priser!', 'Haralds B�tar AB �r ett stabilt f�retag som verkat sedan sextiotalet. 
<p>

V�r aff�rsid� best�r, liksom hos stormarknader att g�ra m�ngdink�p f�r att sedan s�lja till stormarknadspriser.
<p>
Genom dessa storink�p direkt fr�n fabrik eller import�r kan vi leverera produkter till oslagbara priser. 
', 'http://www.haraldsbatar.se', 'active', false, '2003-05-05 11:00:00', '2016-06-01 23:59:59', 'B�tar, skotrar, Polaris, Lynx, Yamaha, Kawasaki', '-', 'patrik', '-', '-', 'patrik', 'patrik', 6, 0, 1060);
INSERT INTO stores VALUES (2985, 'S�ters Cykel & Motor AB', 'info_cykelomotor.se@blocket.se', '0225-525 80', '0225-525 05', 'Verkstadsgatan 4', '783 31', 'S�TER', 'saters.gif', 'Sn�skotrar i S�ter', 'F�retaget startades 1975. Har idag 4 anst�llda. S�ljer och reparerar cyklar,mopeder, motors�gar,gr�sklippare,
gr�strimmers,r�js�gar, sn�slungor,sn�skotrar,fyrhjulingar,
dinx bensin,reservdelar och tillbeh�r m.m.

', 'http://www.cykelomotor.se
', 'active', false, '2003-09-25 00:00:00', '2015-12-31 23:00:00', 'Ski-doo, Yamaha, Arctic Cat', '-', 'patrik', '-', '-', 'patrik', 'patrik', 6, 0, 1180);
INSERT INTO stores VALUES (3002, 'Rolf G Persson Bil AB', 'info_rolfgperssonbil.se@blocket.se', '023-160 50', '023-271 97', 'Krontallsv�gen 18', '791 55', 'FALUN', 'logo.gif', 'M�rkesoberoende handlare.', 'Rolf G Persson Bil AB �r ett av Dalarnas �ldsta bilf�retag och bildades redan 1976.
<p>
Vi �r MRF-medlem sedan 1976. Vi f�ljer regler som motorbranshen har uppr�ttat tillsammans med Motorbranshens RiksF�rbund.
<p>
Bilar k�pes �rsmodell -95 och nyare - g�rna Audi, BMW, Volvo och Saab. H�gsta dagspris f�r v�lsk�tta bilar. Kontakta oss f�r mer information.
<p>
V�ra �ppettider:<br>
Vardagar 10-18<br>
L�rdagar 11-14
<p>
V�lkomna!', 'http://www.rolfgperssonbil.se/', 'active', false, '2004-01-29 09:30:00', '2016-05-01 23:59:59', 'Audi, Saab, Volkswagen, Volvo, ', '-', 'patrik', '-', '-', 'patrik', 'patrik', 6, 0, 1020);
INSERT INTO stores VALUES (3051, 'Autovation AB', 'info_autovation.se@blocket.se', '026-544 420', '026-141 650', 'Str�msbrov�gen 22', '80002', 'G�VLE', 'butik_logga.gif', '�terf�rs�ljare f�r BMW', '<b>V�lkommen till Autovation AB!</b><p>
Hos oss hittar du nya och begagnade BMW bilar samt MC. <br>
Vi �r en fullserviceanl�ggning d�r vi kan erbjuda Dig som kund f�rs�ljning, reservdelar, verkstad och tillbeh�r.<p>
Allt detta samlat under samma tak ger Dig som kund ett tryggt bil och MC �gande.
<p>

�ppettider:<br>
M�n-Tors. 9-18 <br>
Fre. 9-17<br>
L�r. 11.00-14.00<br>
�vriga tider efter �verenskommelse<p>', 'http://www.autovation.se
', 'active', false, '2005-09-19 00:00:00', '2015-12-31 23:59:59', 'Audi, BMW, Mercedes-Benz, Volkswagen, VW, Volvo', '-', 'patrik', '-', '-', 'patrik', 'patrik', 5, 0, 1020);
INSERT INTO stores VALUES (3056, 'Kvalitetsbilar i G�vle AB', 'kvalitetsbilar_telia.com@blocket.se', '026-51 35 00', '026-12 15 61', 'Kryddstigen 33 C', '800 06', 'G�VLE', 'kvalitetsbilar.gif', 'M�rkesoberoende handlare', '<b>Alltid nyare begagnade bilar i lager.</b> <br>
F�rm�nlig finansiering via SEB finans.<br>
V�lkommen till oss f�r Din trygga bilaff�r.

<p>
�ppettider 
<br>
M�n-Fre 9.30-18<br>
L�r 10-14

', 'http://www.kvalitetsbilar.se', 'active', false, '2005-05-12 00:00:00', '2016-05-12 23:59:59', 'Audi, Saab, Volvo', '-', 'patrik', '-', '-', 'patrik', 'patrik', 5, 0, 1020);
INSERT INTO stores VALUES (13693, 'Bilmetro AB, G�vle', 'per.pandel_bilmetro.se@blocket.se', '026 - 65 71 00', '026 - 65 71 10', 'Bondegatan 10', '80104', 'G�VLE', '15_3_05.gif', '�terf�rs�ljare f�r Audi, Volkswagen', '<b>V�lkommen till Bilmetro.se Din B�sta Bilaff�r</b>
<p>
Bilmetro, ett av landets �ldsta bilf�retag, startades redan �r 1922 i Hudiksvall. 
<p>
Bilmetro har nu n�rmare 450 anst�llda p� 13 orter i G�vleborgs- och Dalarnas l�n. Tillsammans oms�tter vi cirka 1,3 miljarder kronor. I Avesta, Bolln�s, Borl�nge, Falun, G�vle, Hedemora, Hudiksvall, Ljusdal, Ludvika, Mora, Sandviken och S�derhamn har vi kompletta bilanl�ggningar med f�rs�ljning, service och verkstad. I R�ttvik har vi enbart bilhall. Kunder som bor p� orter d�r vi inte har egen verksamhet, kan f� hj�lp fr�n n�gon av de nio neutrala verkst�der runtom i l�nen som �r knutna till oss.
<p>
Bilmetro �r inte bara ett av Sveriges ledande bilf�retag, vi �r ocks� ett av de st�rsta privat�gda i branschen. Vi har en sj�lvklar m�ls�ttning f�r v�rt arbete, n�got som g�ller b�de kunder och medarbetare.
<p>
Vi ska alltid vara Din b�sta bilaff�r.         
<p>
�ppettider:<br>
m�ndag-fredag: 09.00-18.00  <br>
l�rdag: 10.00-13.00 <br>', 'http://www.bilmetro.se
', 'active', false, '2005-03-15 00:00:00', '2016-10-01 23:59:59', 'Audi, Chrysler, Mercedes, Saab, Seat, Skoda, Toyota, Volkswagen, VW, Volvo, ', '-', 'patrik', '-', '-', 'patrik', 'patrik', 5, 0, 1020);
INSERT INTO stores VALUES (13794, 'Olof Larsson Bil AB', 'info_oloflarssonbil.se@blocket.se', '0522-867 20', '0522-868 83', 'Smedtorpsv�gen 4', '451 75', 'UDDEVALLA', 'logga.gif', '�terf�rs�ljare f�r Peugeot', 'Olof Larsson Bil AB har l�ng och gedigen erfarenhet i bilbranschen.<br>
F�retaget grundades 1965 av Lars Olof Larsson. Firman var d� centralt bel�gen i Uddevalla, och 1968 byggdes den befintliga fastigheten p� Herrestads Industriomr�de.
<p>
Vi �r b�de glada & stolta �ver v�rt fastighetsl�ge som �r bel�get utmed 44:an strax innan Torps k�pcentrum. <br>
Kommer  Ni  fr�n G�teborg �ver Uddevallabron 
ta av mot Uddevalla s� har Ni oss strax p� h�ger sida.  
<p>
Vi �r auktoriserad �terf�rs�ljare f�r Peugeot sedan 1982. 
<p>
Med Peugeots kompletta modellprogram kan vi erbjuda en bil f�r varje smakriktning och behov. Vi hj�lper Dig att hitta en bil som passar, ny som begagnad. 
<p>
V�lkommen att bes�ka oss! 
<p>
�ppettider:<br>
M�n-Fre 9-18<br>
L�rdag 10-13<br>
Verkstad:<br>
M�nd-Fred 7-17<br>

', 'http://www.oloflarssonbil.se
', 'active', false, '2005-04-28 00:00:00', '2016-05-01 23:59:59', 'Chrysler, Citro�n, Ford, Mazda, Nissan, Opel, Peugeot, Renault, Volkswagen, VW, Volvo', '-', 'patrik', '-', '-', 'patrik', 'patrik', 15, 0, 1020);
INSERT INTO stores VALUES (3067, 'Bildax AB', 'info_bildax.se@blocket.se', '026-54 30 60', '026-14 02 75', 'Skolg�ngen 4', '802 57', 'G�VLE', 'Bildax.gif', 'Din Citro�n �terf�rs�ljare i G�vle', 'Citro�n ger dig 2 �rs nybilsgaranti utan begr�nsning i miltal.
Nu �ven med 3 �rs vagnskadegaranti. (g�ller �ven transportbilar).
P� v�ra personbilar ing�r �ven:
12 �rs rostskyddsgaranti
3 �rs lackgaranti
3 �rs assistansservice
<p>
Alltid ett stort urval av begagnade bilar.
<p>
V�lkomna till Bildax!', 'http://www.bildax.se', 'active', false, '2003-04-15 00:00:01', '2016-09-01 23:59:59', 'Citroen, Mercedes, BMW, Audi, Opel, Saab, ', '-', 'patrik', '-', '-', 'patrik', 'patrik', 5, 0, 1020);
INSERT INTO stores VALUES (3079, 'Anetorn Bil AB', 'stina.fischer_anetornbil.se@blocket.se', '026-25 95 50', '026-25 67 83', 'Box 166, Sp�ngv�gen 1', '811 22', 'SANDVIKEN', 'anetorn.gif', '�terf�rs�ljare f�r Saab & Opel', 'V�lkommen till Sten Anetorn Bil AB i Sandviken och till v�r hemsida. H�r kan du i lugn och ro titta n�rmare p� modellerna fr�n Saab.

<p>
Du ser vad vi har f�r begagnade bilar p� lager, vad vi har f�r erbjudanden f�r tillf�llet och hur du enklast n�r oss p� Anetorn Bil. Du kan l�sa allt om de kringtj�nster som finns f�r dig som k�r Saab. Vi erbjuder n�mligen en f�rm�nlig bilf�rs�kring, ett kundkort som g�r ditt bil�gande billigare, bra finansiering och l�ga priser p� de flesta verkstadsjobb. ', 'http://www.anetornbil.se', 'active', false, '2003-06-05 16:06:00', '2016-01-31 23:59:59', 'Saab, Opel', '-', 'patrik', '-', '-', 'patrik', 'patrik', 5, 0, 1020);
INSERT INTO stores VALUES (3082, 'Bilh�rnan i Sandviken AB', 'niklas.johansson_bilhornan.se@blocket.se', '026-24 90 50', '026-24 90 59', 'H�gbov�gen 47', '811 32', 'SANDVIKEN', 'logga.jpg', 'Auktoriserad �terf�rs�ljare f�r Toyota.', '* Handlar med nya och beg bilar<br>
* Alltid intressanta objekt i lager<br>
* F�rm�nlig finansiering via Skandiabanken Bilfinans<br>

<p>�ppettider: 
<br>M�ndag - Fredag 9.00-18.00, 
<br>L�rdag 10.00 - 13.00', 'http://www.bilhornan.se', 'active', false, '2003-04-25 00:00:00', '2015-12-31 23:00:00', 'Toyota', '-', 'patrik', '-', '-', 'patrik', 'patrik', 5, 0, 1020);
INSERT INTO stores VALUES (3099, 'Husvagnscenter i Valbo AB', 'husvagnscenterivalbo_telia.com@blocket.se', '026-13 02 50', '026-13 02 50', 'Ludvigsbergsv�gen 16', '818 31', 'VALBO', 'husvagnvalbo.gif', 'Personlig service och bra priser', 'Vi erbjuder personlig service och bra priser p� nya eller begagnade husvagnar och sl�pvagnar samt tillbeh�r som t.ex. reservdelar, f�rt�lt, v�rmepaket, och vitvaror. 
<p>
P� v�r verkstad utf�rs service, reparationer samt montering av t.ex. tillbeh�r till samtliga p� marknaden f�rekommande husvagnsm�rken. ', 'http://www.husvagnscenterivalbo.se
', 'active', false, '2003-05-19 11:00:00', '2016-11-01 23:59:59', 'Cabby, Eifelland, Hobby, Kabe, Knaus, Polar, LMC', '-', 'patrik', '-', '-', 'patrik', 'patrik', 5, 0, 1100);
INSERT INTO stores VALUES (3110, 'Nibo Caravan AB', 'nibo.bolln�s_telia.com@blocket.se', '0278-172 90', '0278-263 50', 'Box 223, Rehnshammarv�gen 16', '821 22', 'BOLLN�S', 'sokord_nibo_040331.gif', 'St�rst i Norrland', 'En av de modernaste husvagnsanl�ggningarna i landet. Nytt, begagnat, service och tillbeh�r.
<p>

Bes�k �ven oss i Valbo<br>
Nibo Caravan AB<br>
Ludviksbergsv�gen 12<br>
818 31 VALBO
<p>
Tel: 026-13 33 00', 'http://www.nibo.nu
', 'active', false, '2003-03-26 15:39:00', '2016-08-23 23:59:59', 'Adria, Cabby, Fj�llvagn, �ggestor, Hobby, Kabe, Knaus, Polar, Solifer, Sprite', '-', 'patrik', '-', '-', 'patrik', 'patrik', 5, 0, 1100);
INSERT INTO stores VALUES (3170, 'Nor�n & Skyttner Bil AB', 'noren.skyttner_ljusdal.se@blocket.se', '0651-131 05', '0651-131 45', 'Hybov�gen 10 A', '827 35', 'LJUSDAL', 'norenskytt.gif', 'Toyota �terf�rs�ljare', 'Nor�n & Skyttner Bil AB satsar p� kundrelationer och professionell service. Verkstaden har ett grundmurat rykte om fin service, sunda priser och ett utomordentligt hantverkskunnande. 
<p>
Ryktet �r helt v�lf�rtj�nt och m�nga �gare till andra m�rken �n Toyota nyttjar Nor�n & Skyttners serviceanl�ggning. 
Bilf�rs�ljningen satsar p� personlig service, vanliga m�nniskor emellan. Handel sker med de flesta m�rken av begagnade bilar, men n�r det g�ller nytt �r det Toyota f�r hela slanten. 

', 'http://www.noren-skyttner.se', 'active', false, '2003-06-24 16:45:00', '2015-12-31 23:59:00', 'Citro�n, Saab, Ford, Mercedes, Volkswagen, Audi, Toyota, Ford, Subaru, Mitsubishi ', '-', 'patrik', '-', '-', 'patrik', 'patrik', 5, 0, 1020);
INSERT INTO stores VALUES (3175, 'Sellmans Bil AB', 'robert_sellmans.se@blocket.se', '0651-58 99 00', '0651-335 56', 'B�ckebov�gen 4', '827 40', 'TALL�SEN', 'butik.gif', '�terf�rs�ljare Mercedes-Benz, Nissan, Jeep, Chrysler', 'V�lkommen till Sellmans Bil.<br> Den kompletta bilhandeln i H�lsingland. Vi saluf�r och reparerar Mercedes, Nissan, Jeep och Chrysler.<br> Vi har ocks� ett stort antal begagnade bilar. 
<p>
�ppettider:<br>
M�ndag 09:00-19:00 <br>
Tisdag-Torsdag 09:00-18:00<br>
Fredag: 09:00-17:00<br>
Lunch: 12:00-13:00 <p>', 'http://www.sellmans.se
', 'active', false, '2005-06-13 00:00:00', '2016-12-31 23:59:59', 'Mercedes, Nissan, Jeep, Chrysler', '-', 'patrik', '-', '-', 'patrik', 'patrik', 5, 0, 1020);
INSERT INTO stores VALUES (13896, 'Sellmans Bil AB', 'robert_sellmans.se@blocket.se', '0650-57 99 00', '0650-977 90', 'Kungsgatan 51', '824 26', 'HUDIKSVALL', 'butik.gif', '�terf�rs�ljare Mercedes-Benz, Nissan, Jeep, Chrysler', 'V�lkommen till Sellmans Bil.<br> Den kompletta bilhandeln i H�lsingland. Vi saluf�r och reparerar Mercedes, Nissan, Jeep och Chrysler.<br> Vi har ocks� ett stort antal begagnade bilar. 
<p>
�ppettider:<br>
M�ndag 09:00-19:00 <br>
Tisdag-Torsdag 09:00-18:00<br>
Fredag: 09:00-17:00<br>
Lunch: 12:00-13:00 <p>', 'http://www.sellmans.se
', 'active', false, '2005-06-13 00:00:00', '2016-12-31 23:59:59', 'Mercedes, Nissan, Jeep, Chrysler, Audi', '-', 'patrik', '-', '-', 'patrik', 'patrik', 5, 0, 1020);
INSERT INTO stores VALUES (14461, 'Ivars Bil AB', 'hoting_ivarsbil.se@blocket.se', '0671-71 30 00', '0671-71 30 01', 'V�stra Hoting 450', '830 80', 'HOTING', 'butik.gif', '�terf�rs�ljare f�r BMW, Citro�n, Nissan, Subaru', '<b>V�lkommen till Hoting</b>
<p>

Det var h�r som allt b�rjade 1960 och idag representerar vi BMW, Citroen, Nissan och Subaru. Hoting �r huvudkontor f�r orterna Sollefte� och �stersund. <p>
H�r har vi bilf�rs�ljning och ekonomifunktion med egen leasingavdelning. Dessutom har vi ett av norrlands st�sta utbud av begagnade bilar.
<p>
V�lkommen in till oss! 
<p>
�ppettider 1 jun - 31 aug<br>
M�n-Fre:   08.00-17.00 <br>
L�r:   St�ngt 
 <p> 
�ppettider 1 sep - 31 maj<br>
M�n-Fre:   08.00-17.00 <br>
L�r:   10.00-14.00 <p>

', 'http://www.ivarsbil.se

', 'active', false, '2005-10-24 00:00:00', '2016-01-31 23:59:59', 'Audi, BMW, Citro�n, Hyundai, Jaguar, Mercedes-Benz, Nissan, Mitsubishi, Peugeot, Renault, Skoda, Subaru, Volkswagen, VW, Volvo', '-', 'patrik', '-', '-', 'patrik', 'patrik', 3, 0, 1020);
INSERT INTO stores VALUES (3225, 'Bilhuset i Str�msund AB', 'roger_bilhuset.se@blocket.se', '0670-127 50', '0670-101 15', 'Str�msv�gen 47', '833 21', 'STR�MSUND', '-', 'Vi �r �terf�rs�ljare av Audi, Seat, Skoda och Volkswagen', 'Bilhuset i Str�msund AB grundades 1996 och �vertog d� Berners i Str�msund. F�retaget s�ljer nya och begagnade bilar, samt sk�ter all reparation. F�rutom dessa tj�nster har vi �ven d�ck, hyrbilar och en pl�tverkstad. Vi har ett mycket n�ra samarbete med Berners och ing�r i V.A.G kedjan.
<p>
Bilhusets s�ljomr�de �r Str�msunds stora kommun, och f�retaget ser en positiv utveckling p� alla enheter. Vi vill att v�ra kunder ska veta att vi st�ller upp f�r dom, och s�ker reda p� det vi inte redan har.

', 'http://www.bilhuset.se', 'active', false, '2003-11-20 00:00:00', '2016-03-01 23:59:59', 'Audi, BMW, Chevrolet, Chrysler, Fiat, Ford, Mitsubishi, Nissan, Opel, Volkswagen, Skoda, Volvo', '-', 'patrik', '-', '-', 'patrik', 'patrik', 3, 0, 1020);
INSERT INTO stores VALUES (3270, 'Norrlands Bilinvest AB', 'bilinvest_telia.com@blocket.se', '060-56 27 30', '060-56 27 30', 'Bj�rneborgsgatan 49', '854 61', 'SUNDSVALL', '-', 'M�rkesoberoende f�rs�ljare.', '-', 'http://www.norrlandsbilinvest.se/', 'active', false, '2003-11-18 00:00:00', '2016-03-01 23:59:59', 'Volvo, VW, BMW, SAAB, Audi, Mercedes', '-', 'patrik', '-', '-', 'patrik', 'patrik', 4, 0, 1020);
INSERT INTO stores VALUES (3277, 'Bilomobil AB', 'info_bilomobil.se@blocket.se', '060-10 11 90', '060-10 12 90', 'Axv�gen 6', '853 50', 'SUNDSVALL', 'logga.gif', 'MRF Handlare. S�ljer n�stan nya bilar - husbilar', 'Vi �r ett ungt f�retag med massor av rutin.
�garen har sysslat med bilar och aff�rer i �ver 30 �rs tid.
<p>
V�r aff�rsid� �r:<br>
- f�rs�ljning av n�stan nya personbilar.<br>
- import�r f�r s�dra och mellersta Norrland av Dethleffs husbilar och husvagnar.<br>
- import�r av Carthago husbilar.
<p>
MRF-handlare.', 'http://www.bilomobil.se', 'active', false, '2003-04-30 11:13:00', '2016-09-01 23:59:59', 'Dethleffs, Fiat, Mercedes, Saab, Toyota, Volvo, husvagnar, husvagn, husbil, husbilar,', '-', 'patrik', '-', '-', 'patrik', 'patrik', 4, 0, 1020);
INSERT INTO stores VALUES (3279, 'S�rberge Husvagnar AB', '-', '060-57 98 65', '060-57 09 60', 'Terminalv�gen 30', '861 36', 'TIMR�', 'sorberge.gif', 'Auktoriserad �terf�rs�ljare f�r Cabby och Knaus', 'Du hittar oss vid Timr� V�rdshus Ca 15 Km norr om Sundsvall, efter E4:an. 
Vi �r 5 anst�llda som utf�r service, reparationer, fukttest m.m. p� husvagnar, husbilar och sl�p. (Dock ej motorproblem p� husbilar.) Vi s�ljer nya husvagnar av m�rkena Knaus och Cabby samt husbilar av m�rket Knaus. Dessutom s�ljer vi sl�pvagnar av m�rket 4P, som �r en svensk kvalitetsvagn.
<p>
Vi har �ven uthyrning av husvagnar och husbilar. 
Hos oss finner du en v�lsorterad butik med tillbeh�r och reservdelar med allt f�r husbils- och husvagns �garen. Beh�ver du fylla p� din gasolkruka s� �r du v�lkommen in.
<p>
Vi har 15 �rs erfarenhet och �r sedan 1994 auktoriserad �terf�rs�ljare f�r Cabby och Knaus. Vi kan branschen och s�tter kunden i centrum. S� titta in och se om vi kan hj�lpa er med n�got!', 'http://www.sorbergehusvagnar.com', 'active', false, '2003-05-10 00:00:00', '2016-02-28 23:59:59', 'Cabby, Knaus, Home-car, Eifelland', '-', 'patrik', '-', '-', 'patrik', 'patrik', 4, 0, 1100);
INSERT INTO stores VALUES (3280, 'Bilhuset i Bergeforsen AB', 'info_bilhuset-bildelar.com@blocket.se', '060-57 95 97', '060-57 95 01', 'Industrigatan 1', '860 33', 'BERGEFORSEN', 'logo.gif', 'Bilreservdelar med orginalkvalitet till riktigt konkurrenskraftiga priser.', 'Bilhuset Bergeforsen AB startades 1997. Vi tillhandah�ller bilreservdelar, billack, bilpl�t, avgassystem mm via butik och postorder. V�ra kunder �r Bilverkst�der, F�retag, Kommun, Landsting och Privatpersoner.
<p>
�ppettider:<br>
M�n-Fre 08.30-17.00
<p>
V�lkomna!', 'http://www.bilhuset-bildelar.com/', 'active', false, '2004-03-11 17:00:00', '2016-07-10 23:59:59', 'Bilreservdelar, Billack, Bilpl�t, Avgassystem, Bildelar,  ', '-', 'patrik', '-', '-', 'patrik', 'patrik', 4, 0, 1040);
INSERT INTO stores VALUES (3294, 'Auto-Mobile Sweden AB', 'info_auto-mobile.se@blocket.se', '060-58 82 36', '060-55 83 30', 'Maskinistv�gen 4', '865 21', 'ALN�', 'automobile.gif', 'Samarbetspartnern vid bilk�p och transporter inom EU', 'Det naturliga valet av samarbetspartner vid bilk�p och transporter inom EU.
<li>importerar bilar fr�n EU</li>
<li>tar emot best�llningar</li>
<li>utf�r f�rs�ljningsuppdrag</li>
<li>byter in bilar</li>
<li>ger r�d och tips om bilk�p</li>
<li>erbjuder finansiering i samarbete med NORDEA BILFINANS</li>
<p>
Auktoriserad �terf�rs�ljare f�r MG och Rover.', 'http://www.auto-mobile.se', 'active', false, '2003-04-17 00:00:00', '2016-07-01 23:59:59', 'Audi, BMW, Ford, Mercedes, Mitsubishi, Nissan, Toyota, Volkswagen, Volvo, Yamaha, Polaris, MG, Rover,', '-', 'patrik', '-', '-', 'patrik', 'patrik', 4, 0, 1020);
INSERT INTO stores VALUES (3346, 'Burlin Motor i �rnsk�ldsvik AB', 'ilkka.kangas_toyota.se@blocket.se', '0660-107 20', '0660-175 05', 'Ges�llv�gen 1', '891 41', '�RNSK�LDSVIK', 'burlin_logo.gif', 'Auktoriserad Toyota �F', 'Alltid intressanta bilar i lager <br>
Fullservice verkstad <br>
�ppet: M�n - Tors 9-18, Fre 9-17
', 'http://www.burlinmotor.nu/public/dokument.php?art=37&parent01=10', 'active', false, '2003-04-23 00:00:00', '2016-06-01 23:59:59', 'Toyota', '-', 'patrik', '-', '-', 'patrik', 'patrik', 4, 0, 1020);
INSERT INTO stores VALUES (3351, 'Magnus Bil AB', 'info_magnusbil.se@blocket.se', '0660 - 120 12', '0660 - 815 62', 'J�rvhagav�gen 7', '891 61', '�RNSK�LDSVIK', 'Magnus.gif', 'Fullserviceanl�ggning p� J�rvshagav�gen 7', '
<BR>
Varmt v�lkomna till Er Peugeot-�terf�rs�ljare i �rnsk�ldsvik.
<P>
En fullserviceanl�ggning med bilf�rs�ljning, transportbilsf�rs�ljning,
serviceverkstad, bilskadeverkstad och reservdelsf�rs�ljning.
<P>
Vi har �ven ett stort lager av bra begagnade bilar.
<P>
V�r m�ls�ttning �r att s�lja bra bilar med r�tt pris till n�jda kunder.
<P>', 'http://www.magnusbil.se
', 'active', false, '2003-04-16 00:00:00', '2016-05-01 23:59:59', 'Peugeot', '-', 'patrik', '-', '-', 'patrik', 'patrik', 4, 0, 1020);
INSERT INTO stores VALUES (3365, 'Ume� Van Service AB', 'info_vanservice.se@blocket.se', '090 - 12 04 20', '090 - 12 20 02', 'F�rr�dsv�gen 8', '901 32', 'UME�', 'vanservice.gif', '�terf�rs�ljare f�r Cadillac, Chevrolet, Corvette', 'Ume� Van Service �r Norrlands mest kompletta sevicecenter f�r
alla typer av amerikanska fordon. Sedan ett par �r tillbaka kan du
�ven k�pa din amerikanska bil hos oss. I v�r bilhall hittardu GM,
Ford eller Chrysler �The big three�. Kom in och l�t digimponeras
av priserna, utrustningen, modellutbudet mm. Inget g�r upp mot
en �kta amerikanare! 
<p>
V�lkommen till oss.
<p>
<li>Medlem i MRF
<li>M-testade fordon
<li>Trygghet', 'http://www.vanservice.se', 'active', false, '2005-04-06 00:00:00', '2016-05-01 23:59:59', 'Chevrolet, Dodge, SUV, Pick Up, Minivan, 4-wheel, Vans, Sport/special, Ford, Chrysler, Jeep', '-', 'patrik', '-', '-', 'patrik', 'patrik', 2, 0, 1020);
INSERT INTO stores VALUES (3381, 'Stureson Bil AB', 'stureson.bil_nordiq.net@blocket.se', '090-18 62 50/51', '090-18 62 52', 'Formv�gen 9 B', '906 21', 'UME�', 'stureson.gif', 'Mercedes-specialisten. Utvalda, M-testade bilar av h�gsta kvalitet.', '30 �rs samlad erfarenhet av Mercedes handel

och service har gett oss kunskapen om vad Mercedes-�garen

kr�ver och har r�tt at beg�ra.
<br>

Vi erbjuder dig alla f�rdelar men med en

K�PTRYGGHET OCH BEKV�MLIGHET

som endast en lokal leverant�r kan erbjuda.

', 'http://mercedes.just.nu
', 'active', false, '2003-04-23 00:00:00', '2016-06-01 23:59:59', 'Mercedes', '-', 'patrik', '-', '-', 'patrik', 'patrik', 2, 0, 1020);
INSERT INTO stores VALUES (3387, 'Bergners Bil AB', 'ake_bergnersbil.se@blocket.se', '090-70 23 80', '090-70 23 89', 'Formv�gen 10 C', '906 21', 'UME�', 'bergners.gif', 'Ume�-f�retag med f�rs�ljning av nya och beg bilar', 'Bergners Bil AB �r ett genuint Ume�-f�retag som �gs och drivs av br�derna Bergner. Vi har varit �terf�rs�ljare f�r Mazda sedan starten 1984.
I dag �r Bergners Bil auktoriserad �terf�rs�ljare f�r Mazda, Chrysler, Jeep, Suzuki och senaste tillskottet KIA, i en fullserviceanl�ggning som efter den senaste utbyggnaden, som blev klar under v�ren 2004, omfattar 2700 kvm. Vi �r sedan 2002 ISO 14001 milj�certifierade, och �r av Svensk Bilprovning en Kontrollerad Bilverkstad.
Naturligtvis �r Bergners Bil medlemmar av Motorbranschens Riksf�rbund, MRF, f�r din trygghet i bilaff�ren.
<p>
Vi �r 14 anst�llda under ledning av VD H�kan Bergner, som hj�lper dig med din bilaff�r b�de f�re och efter k�pet.
Personligt engagemang och kunden i centrum �r v�r ledstj�rna f�r att Du skall g�ra din b�sta bilaff�r hos oss.
<p>
V�lkommen!', 'http://www.bergnersbil.se', 'active', false, '2003-04-16 00:00:00', '2016-05-01 23:59:59', 'Mazda, Chrysler, Jeep, Suzuki,', '-', 'patrik', '-', '-', 'patrik', 'patrik', 2, 0, 1020);
INSERT INTO stores VALUES (3414, 'Auto Bl� AB', 'autobla_autobla.se@blocket.se', '0950-120 81', '0950-122 84', 'Karossv�gen 3 B', '921 45', 'LYCKSELE', 'autobla.gif', 'Mercedes och Toyota �terf�rs�ljare', 'Auto Bl� AB �r ett f�retag i Lycksele som etablerades den 7 januari 1992. Vi har sedan dess s�lt och reparerat Mercedes Benz. Sedan 1998 s�ljer vi �ven Toyota. V�r verkstad �r dessutom auktoriserad f�r ovanst�ende m�rken. Vi har �ven l�ng erfarenhet av service och reparationer av Nissan bilar.', 'http://www.autobla.se', 'active', false, '2003-06-16 08:46:00', '2015-11-08 00:00:00', 'Mercedes, Toyota', '-', 'patrik', '-', '-', 'patrik', 'patrik', 2, 0, 1020);
INSERT INTO stores VALUES (3452, 'Gustafssons Bil AB', 'hakan_gustafssonsbil.se@blocket.se', '0910-137 77', '0910-534 52', 'Tj�rnv�gen 40 B', '931 61', 'SKELLEFTE�', 'gustafssons.gif', '�terf�rs�ljare f�r Mazda, Suzuki', 'F�rs�ljningen �ppen:
<p>
M�ndag-Torsdag 09.00-18.00<br>
Fredag 09.00-17.00<br>
L�rdag 11.00-14.00<br>
<p>


Reservdelar och verkstad �ppet:
<p>
M�ndag-Fredag 08.00-17.00<br>
Lunch: 12.00-13.00<br>  
<p>
Telefon reservdelar: 0910-137 88<br>
verkstad: 0910-77 63 40<br>
', 'http://www.gustafssonsbil.se', 'active', false, '2003-04-17 00:00:00', '2016-05-15 23:59:59', 'Mazda, Suzuki, Mercedes, Nissan, Suzuki ', '-', 'patrik', '-', '-', 'patrik', 'patrik', 2, 0, 1020);
INSERT INTO stores VALUES (3455, 'Hellgrens Bil AB', 'anders.hellgren_hellgrensbil.se@blocket.se', '0910-77 08 30', '0910 77 08 38', 'Tj�rnv�gen 1', '931 61', 'SKELLEFTE�', 'hellgrens.gif', '�terf�rs�ljare f�r Mitsubishi och Peugeot', 'Hellgrens Bil AB erbjuder med sin fullserviceanl�ggning de flesta av de i marknaden f�rekommande tj�nster och produkter f�r bil�garen. Nya och begagnade personbilar och l�tta lastbilar med tillh�rande finansiering, b�de avbetalning och leasing. Service och reparationer samt reservdelar och tillbeh�r till Mitsubishi och Peugeot.', 'http://www.hellgrensbil.se', 'active', false, '2003-04-22 00:00:00', '2016-06-03 23:59:59', 'Mitsubishi, Peugeot', '-', 'patrik', '-', '-', 'patrik', 'patrik', 2, 0, 1020);
INSERT INTO stores VALUES (3493, 'Sunderbyns Bil AB', 'ulf_sunderbybil.se@blocket.se', '0920-26 60 19', '0920-25 78 55', 'Gamla Bodenv�gen 315', '954 42', 'S�DRA SUNDERBYN', 'sunderbyns.gif', 'Bilar som ska passa de flesta', 'Vi �r ett f�retag som st�ller kunden fr�mst vilket inneb�r att vi har ett stort utbud av bra beg. bilar som ska passa de flesta.
<p>
V�rt f�retag befinner sig  i S�dra Sunderbyn ca 2 mil v�ster om Lule�.
<p>
V�r f�rs�ljare Ulf hj�lper er g�rna att hitta just er dr�mbil.
<p>
V�lkomna!', 'http://www.sunderbybil.se', 'active', false, '2003-04-30 00:00:00', '2016-07-01 23:59:59', 'Audi, Ford, Jeep, BMW, Dodge, Mercedes, Saab, Opel, Toyota, Volkswagen, Volvo,', '-', 'patrik', '-', '-', 'patrik', 'patrik', 1, 0, 1020);
INSERT INTO stores VALUES (12652, 'Moped och Scooter City', '-', '042-23 50 25', '042-23 50 25', 'Kropparv�gen 40', '260 34', 'M�RARP', 'mopedcity_logo.gif', 'Marknadens mest prisv�rda Mopeder & Scooters.', 'Egen MC-import till oslagbara priser.<br>
Stort urval av tillbeh�r.
<p>
<b>�vrigt:</b>
<br>
F�rs�kringsskador, EU-mopeder, 30 km/h K�rkortsfria Mopeder, R�ntefri avbetalning, Reparationer, Inbyten, Tillbeh�r, Reservdelar, 125 cc Scootrar.', '-', 'active', false, '2004-05-10 09:00:00', '2015-11-30 23:59:59', 'Yamaha, Honda, Drac, Suzuki, Moped, Scooter, Skoter, EU-moped,', '-', 'patrik', '-', '-', 'patrik', 'patrik', 23, 3, 1140);
INSERT INTO stores VALUES (13480, 'L�gprisbilar  i  Svedala HB', 'lagprisbilar.s_telia.com@blocket.se', '040 - 40 78 50', '040 - 40 78 55', 'Trelleborgsv�gen 87', '233 93', 'SVEDALA', '-', 'M�rkesoberoende handlare', '-', '-', 'active', false, '2005-01-10 00:00:00', '2016-01-15 23:59:59', 'Renault, Toyota, Volvo, ', '-', 'patrik', '-', '-', 'patrik', 'patrik', 23, 11, 1020);
INSERT INTO stores VALUES (3553, 'Bil & motortj�nst i Karlskrona', 'j.timo_telia.com@blocket.se', '0455-126 90', '0455-126 90', 'V. Kvarngatan 4, Pantarholmen', '371 38', 'KARLSKRONA', '17_12_04.gif', 'Brett utbud av begagnade bilar och service i Karlskrona.', '�ppettider 08:30-16:00 m�ndag-fredag eller enligt �verenskommelse. Telefontid 07:00-22:00 alla dagar, �ven helger. <p>
<li>Bilf�rs�ljning
<li>Serviceverkstad
<li>Reperationer
<li>Rekontidionering
<p>
Mobil:<br>0701-42 69 00<br>0708-12 69 05', 'http://www.bilochmotortjanst.se', 'active', false, '2003-01-01 00:00:00', '2016-06-01 23:59:59', 'verkstad, karlskrona, reparationer', '-', 'patrik', '-', '-', 'patrik', 'patrik', 22, 0, 1020);
INSERT INTO stores VALUES (3565, 'Bohlins i J�rbo AB', 'bohlins_swipnet.se@blocket.se', '0290-704 02', '0290-704 60', 'Norrhedsv.  2', '81170', 'J�RBO', 'bohlins.gif', 'Specialiserade p� sn�skotrar, tillbeh�r samt pistmaskiner', 'Yamaha �r v�rt m�rke men vi har �ven alltid ett stort lager av begagnade skotrar. Hoppas, att ni hittar n�got av intresse.
<p>
Vi levererar skotrar �ver hela Sverige men rent geografiskt hittar ni oss mitt i J�rbo. J�rbo ligger ca 4 mil fr�n G�vle, ca 20 mil fr�n Stockholm, ca 20 mil fr�n Sundsvall. 
 
', 'http://www.bohlinsab.se
', 'active', false, '2003-04-01 00:00:00', '2016-05-10 23:59:59', 'Yamaha, skotrar, skoteruthyrning, skoterkl�der, pistmaskiner, sl�pvagnar, eu-mopeder', '-', 'patrik', '-', '-', 'patrik', 'patrik', 5, 0, 1180);
INSERT INTO stores VALUES (3590, 'Hedmans Motor AB', 'hedmans.motor_telia.com@blocket.se', '0960-123 41', '0960-123 40', 'Norrv.  39', '933 31', 'ARVIDSJAUR', 'hedmans.gif', 'Sn�skotrar i Arvidsjaur', 'Hedmans motor startade 1985 n�r Alfons och Tommy Hedman (Far och son) k�pte ett befintligt f�retag: Granbergs motors�gservice.
I b�rjan jobbade vi uteslutande med motors�gar, fr�mst servade vi �t alla skogsbolag inom Arvidsjaurs n�romr�de. �r 1986 kom sn�skotrar och 4-hjulingar in i bilden (Polaris och Honda).
Efterhand som motors�gsepoken i skogen b�rjade ebba ut tog skotrar, ATV och tr�dg�rdsmaskiner �ver 
som dom st�rre f�dkrokarna, �ven om motors�gar och framf�rallt r�js�gar fortfarande �r en stor grej hos oss.
<p>
Under -89 flyttade vi till egna lokaler p� Norrv�gen, d�r vi fortfarande h�ller till.
<p>
Tillverkning av ATV produkter v�xte ocks� fram genom �ren, i b�rjan mest p� grund av att det inte fanns n�gra riktigt bra vagnar som var anpassade f�r anv�ndarnas behov, en annan orsak till tillverkningen �r att det �r s� j�drans kul att bygga s�nt som sedan blir uppskattat av brukarna.
<p>
I dag �r vi 4-5 personer som jobbar inom f�retaget beroende p� s�song.
<p>
Vi oms�tter idag ca 11 milj. p� Skog och Tr�dg�rdsprodukter, Sn�skotrar, ATV, MC, B�tar, B�tmotorer, Cyklar, Tillverkning av ATV-tillbeh�r (i samarbete med Arvidsjaurs maskinmekaniska) plus mycket annat.', 'http://www.hedmansmotor.com
', 'active', false, '2003-05-19 00:00:05', '2016-01-15 23:59:59', 'Polaris, Arctic Cat, Lynx, Finncat, Ockelbo, Aktiv', '-', 'patrik', '-', '-', 'patrik', 'patrik', 1, 0, 1180);
INSERT INTO stores VALUES (3597, 'Kalix Maskiner Hugo Jacobsson AB', 'butik_kalixmaskiner.se@blocket.se', '0923-122 90', '0923-107 64', 'Vikenv�gen 7', '952 27', 'KALIX', 'kalixmaskin.gif', '�terf�rs�ljare f�r Ski-doo, Lynx & Polaris', 'Hugo Jacobsson startade Kalix Maskiner i september 1969. Han b�rjade d� i en liten boda p� villatomten.

<p>
I dag har vi 2 stora, moderna  inneh�llsrika butiker p� sammanlagt 5000 m� f�rs�ljnings och lageryta. 14 anst�llda med en sammanlagd branshvana p�  200 �r!
<p>

Vi �r en servande fackhandel, vilket inneb�r att vi har en egen verkstad och ett stort reservdels lager med personal som har l�ng rerfarenhet och besitter en enorm kompetens
', 'http://www.kalixmaskiner.se', 'active', false, '2003-05-06 00:00:00', '2016-10-01 23:59:59', 'Lynx, Ski-doo, Polaris', '-', 'patrik', '-', '-', 'patrik', 'patrik', 1, 0, 1180);
INSERT INTO stores VALUES (3606, 'Lindblads Motor AB', 'perlindblad_vilhelmina.ac@blocket.se', '0940-108 15', '0940-121 56', 'Volgsj�v.  47', '91232', 'VILHELMINA', 'lindblads.gif', 'Vilhelmina f�retag med f�rs�ljning av nya och begagnade skotrar', 'Lindblads motor �r ett Vilhelmina f�retag med f�rs�ljning av nya och
begagnade skotrar. Vi har varit med i branchen sedan 1966.

<p>
Skidoo har varit med l�ngst och som har visat sig vara ett m�rke med mycket
h�g kvalitet.
<p>
Vi har �ven Sea-doo.
<p>
<b>E-marknad</b><br>
P� <A HREF=" http://lindbladsmotor.emarknad.se" TARGET="_blank">
http://lindbladsmotor.emarknad.se</a>
presenterar vi flera nyheter b�de vad g�ller maskiner och tillbeh�r.

', 'http://www.lindbladsmotor.com
', 'active', false, '2003-05-07 00:00:00', '2016-05-10 23:59:59', 'Ski-doo, Sea-doo', '-', 'patrik', '-', '-', 'patrik', 'patrik', 2, 0, 1180);
INSERT INTO stores VALUES (3621, 'Motor & Fritid Bj�rn & Rihpa AB', 'info_motorfritid.nu@blocket.se', '0684-294 50', '0684-294 50', 'R�rosv.  23', '84095', 'FUN�SDALEN', 'furnas.gif', 'Arctic Cat, Suzuki, Yamaha f�r omg�ende leverans', '<li>Yamaha Dealer of the Year
<li>F�rs�ljning av Yamaha och Arctic Cat skotrar
<li>Alltid ett 30-tal skotrar f�r omg�ende leverans
<li>Mycket stort sortiment av de b�sta skoterkl�derna
<li>Service och reservdelar av alla skoterfabrikat
<li>Uthyrning av skotrar. 10 min. till h�gfj�llet
<li>Postorder
<li>Nya Arctic Cat, Suzuki & Yamaha f�r omg�ende leverans', 'http://www.motorfritid.nu
', 'active', false, '2003-09-05 00:00:00', '2016-12-31 23:59:59', 'Arctic Cat, Suzuki, Yamaha ', '-', 'patrik', '-', '-', 'patrik', 'patrik', 3, 0, 1180);
INSERT INTO stores VALUES (12642, 'Unik Bil AB', 'unikbilorebro_hotmail.com@blocket.se', '019 - 26 53 30', '019 - 26 53 30', 'Tingslagsgatan 3', '702 17', '�REBRO', '-', 'Alltid billiga fr�scha bilar i lager.', 'Alltid billiga fr�scha bilar i lager.
Mobil nr: 0707-95 31 87
<p>
V�lkommen!', '-', 'active', false, '2004-04-30 14:00:00', '2016-05-01 23:59:59', 'Ford, Opel, Renault, Volkswagen, Toyota,  Volvo, Saab,', '-', 'patrik', '-', '-', 'patrik', 'patrik', 8, 0, 1020);
INSERT INTO stores VALUES (3633, 'Nya Maskin & Fritid i Arvidsjaur AB', 'nya.m_telia.com@blocket.se', '0960-107 70', '0960-212 19', 'J�rnv�gsg.  42', '933 31', 'ARVIDSJAUR', 'nyamaskin.gif', '�terf�rs�ljare f�r Lynx och Ski-doo', 'Vi har �ven bilverkstad.<br>
Ni kan n� oss p� 070-6270468
<p>
V�lkomna!', 'http://www.nyamaskinofritid.com', 'active', false, '2003-09-29 00:00:00', '2016-05-01 23:59:59', 'Lynx, Ski-doo, Arctic Cat,  Gilera, Polaris, Yamaha', '-', 'patrik', '-', '-', 'patrik', 'patrik', 1, 0, 1180);
INSERT INTO stores VALUES (3751, 'Guzzi Center AB', 'info_guzzicenter.se@blocket.se', '08-445 60 10', '08-445 60 11', 'Veddestav. 23', '175 62', 'J�RF�LLA', 'guzzi.GIF', '�terf�rs�ljare f�r Aprilia, CCM, Derbi, Moto Guzzi, Sachs', 'Guzzi Center AB slog upp portarna h�r i J�rf�lla mars -99. Vi bedriver handel med nya och 
begangnade motorcyklar, mopeder samt reservdelar. 
<p>
Vi �r auktoriserade �terf�rs�ljare f�r MC, Moto Guzzi, Aprilia och Sachs
<p>
Moped/Scooter: Piaggio, Gilera, Vespa, Aprilia, Sachs och TGB
<p>
Vi �r enda specialbutiken f�r Moto Guzzi i hela 08-omr�det. 
<p>
Vi har en egen butik samt egen verkstad f�r reparation och service.
<p>', 'http://www.guzzicenter.se
', 'active', false, '2005-03-09 00:00:00', '2016-03-15 23:59:59', 'Moto Guzzi, Aprilla, Piaggio, Gilera, Vespa, Stein-Dinse, MotorSpeziel, D.Agostini, Cromac, Bags & Bike', '-', 'patrik', '-', '-', 'patrik', 'patrik', 11, 0, 1140);
INSERT INTO stores VALUES (3752, 'Delta MC AB', 'info_deltamc.se@blocket.se', '0346-178 38', '0346-137 33', 'Skogstorp Industriomr�de', '31191', 'FALKENBERG', 'deltamc.gif', 'Ny- samt begagnatf�rs�ljning av motorcyklar och mopeder.', 'Hos oss p� Delta MC hittar du allt fr�n nya och begagnade motorcyklar till
tillbeh�r och d�ckservice samt service och reparationsverkstad.
<p>
H�r finner du det mesta som �r beh�vligt n�r det g�ller Mc-�kning i terr�ng
och p� landsv�g.
<p>
Vi har �ven en stor kl�davdelning f�r b�de sport och touring.
Skulle vi de inte har det hemma som just Du s�ker s� kan vi naturligtvis
best�lla hem det
<p>
Vi p� Delta hj�lper Dig g�rna med att f� lite extra fart p� Din
2-takts-motor eller med inst�llning / omshimsning av fj�dringskomponenter.
Vi har stor erfarenhet d� vi hj�lpt elitf�rare i m�nga �r.

<p>
Du kan �ven med trygghet v�nda Dig till v�r auktoriserade verkstad.

<p>
V�lkommen till Delta MC f�r en trygg aff�r.

', 'http://www.deltamc.se', 'active', false, '2003-08-08 00:00:00', '2016-12-31 23:59:59', 'Honda, Kawasaki, KTM, Suzuki, Yamaha', '-', 'patrik', '-', '-', 'patrik', 'patrik', 20, 0, 1140);
INSERT INTO stores VALUES (3765, 'Hermans Hojar', 'info_hermanshojar.com@blocket.se', '0512-168 71', '0512-168 71', 'Kungsg. 34', '534 31', 'VARA', 'hermanshoj.GIF', 'K�per och s�ljer nya och begagnade custommotorcyklar', '<p>V�lkommen till Hermans Hojar som har specialiserat sig p� att k�pa och s�lja nya och begagnade custommotorcyklar.
<p>
Jag hj�lper dig att finna din DR�MMOTORCYKEL!
<p>
Vi har alltid mellan 60-80 begagnade motorcyklar i lager.
<p>
Fullserviceverkstad.
<p>
Mobilnr 0706-81 75 75<p>
�ppettider:<br>
M�n-tors 7-18<br>
Fre 7-17<P>

', 'http://www.hermanshojar.com', 'active', false, '2003-08-20 13:29:00', '2015-12-31 23:59:59', 'BMW, Ducati, Harley Davidson, Honda, Kawasaki, Suzuki, Yamaha', '-', 'patrik', '-', '-', 'patrik', 'patrik', 13, 0, 1140);
INSERT INTO stores VALUES (3779, 'Superbike G�teborg', 'info_superbike.se@blocket.se', '031-12 94 73', '031-12 94 57', 'EA Rosengrensgata 13C', '421 31', 'V�STRA FR�LUNDA', 'superbikelogo.gif', 'KTM & Suzuki-centret i G�teborg', 'V�lkommen till Superbike - KTM & Suzuki-centret i G�teborg.
<p>
Auktoriserad �terf�rs�ljare f�r KTM & Suzuki.
<p>
V�r st�rsta inriktning �r supermotard och sportmotorcyklar och dess tillbeh�r.
<p>
Vi har allt som h�r hoj-�kningen till. S�som kl�der, reservdelar och tillbeh�r.
<p>
Du kan �ven med trygghet v�nda dig till v�r fullservice-verkstad.
<p>
V�lkommen till Superbike - KTM & Suzuki Specialisten i G�teborg.', 'http://www.superbike.se
', 'active', false, '2003-09-09 00:00:00', '2016-09-15 23:59:59', 'KTM, Supermotard, Enduro, Adventure, Spyke, Nya mc, Beg mc, Offroad, Alpinestars, Shark, Cross, Superbike, Akrapovic, Suzuki, slip-on, Duke, 990, 950 640 LC4, 625 SMC, 660 SMC, Superduke, GSX, GSXR, GSX-R,', '-', 'patrik', '-', '-', 'patrik', 'patrik', 15, 0, 1140);
INSERT INTO stores VALUES (3843, 'Bike City', 'info_bikecity.se@blocket.se', '0920-22 02 80', '0920-22 02 94', 'Handelsv. 7', '97345', 'LULE�', 'logo.gif', 'St�rsta MC-varuhuset norr om Stockholm', 'V�lkommen till BikeCity!
<p>
Vi �r auktoriserade �terf�rs�ljare av Harley-Davidson och Suzuki. Dessutom s�ljer vi mopeder, ATV, cross, enduro, trial och lekfordon. Vi har �ven ett stort lager kl�der, hj�lmar och skor.
<p>
Alltid m�ngder med begagnade MC inne. Kom in och titta.
<p>
Du hittar oss, s�derifr�n E4 mot Haparanda N�r Du passerat Lule-�lven sv�ng h�ger mot Storheden Stormarknad. Stanna vid orienteringtavlorna som finns vid alla infarter, s� hittar Du. Andra infarter... stanna vid orienteringstavlorna...', 'http://www.bikecity.se', 'active', false, '2003-08-14 14:00:00', '2016-08-24 23:59:59', 'Buell, Gold Wing, Harley, Honda, Kawasaki, Suzuki, Yamaha, Lehman Trikes, Nefiti Sidecar, Peak Trailer', '-', 'patrik', '-', '-', 'patrik', 'patrik', 1, 0, 1140);
INSERT INTO stores VALUES (3849, 'Sulas MC', 'sulasmc_swipnet.se@blocket.se', '018-39 52 80', '018-39 53 01', '�lsta', '75591', 'UPPSALA', 'sulas2.gif', 'MC-butiken f�r dig och din hoj', 'Aukt. �F f�r Triumph, Kawasaki, Suzuki, Moto Guzzi, MZ, Polaris, Peugeot
mopeder.
<p>
G� g�rna in p� v�r hemsida f�r mer info om beg.hojar och v�ra jippohelger.
<p>

1500 kvadratmeter MC. S�ljer nytt o beg samt f�rmedlar.
<p>
V�lsorterad tillbeh�rsbutik & personlig utrustning.
<p>
Aukt. verkstad f�r Triumph, Kawasaki, Suzuki, Peugeot, Polaris, Moto Guzzi.
Reservdelar & service f�r samtliga m�rken.
<p>
Vi har �ven vinterf�rvaring med varmgarage, larm och v�ktare.
<p>
N�gra av v�ra tillbeh�rsm�rken; <br>
Ledrie, Mustang sadlar, Renegade, CirCui, DPM, Triumph-, Kawasaki- & Suzuki
orginaltillbeh�r, Abus, Oxford, Mapam, Kentuckey, Givi, Motoplex, Cameron,
Mobil, Spectro, NGK, Champion K&N, SBS, DID, Kappa, m fl.
<p>
N�gra av v�ra kl�desm�rken;<br>
Rukka, Revit, Hallman, Jofama, Halvarsson, Held, MP-ASU, Triumph, HJC,
Airoh, Bieffe, Shoei, Nolan, Suomy, Caberg, Forma, Spruce, Alpinestars,
Sidi, STR, Buff
<p>
�ppettider:<br>
M�n-Fre 9,30-18<br>
Kv�lls�ppet onsdagagar till 20 (ej nov, dec & jan)<br>
L�rdag 10-14<br>
S�ndagar fr�n mars till juni, se hemsida.
<p>
Du finner oss 18km fr�n Uppsala l�ngs v�g 55 mot Enk�ping.', 'http://www.sulas.se
', 'active', false, '2003-10-03 00:00:00', '2016-10-31 23:59:59', 'Triumph, Kawasaki, Suzuki, Moto Guzzi, MZ, Peugeot, Mopeder, Polaris, Rukka,
Revit, Hallman, HJC, Jofama, Halvarssons, Airoh, Shoei, Nolan, Spruce,
Alpinstars, Sidi', '-', 'patrik', '-', '-', 'patrik', 'patrik', 10, 0, 1140);
INSERT INTO stores VALUES (3851, 'Nilssons Motor i Sk�nes Fagerhult AB', 'nilssonsmotor_telia.com@blocket.se', '0433-300 70', '0433-300 80', 'Markarydsv�gen 19', '28040', 'SK�NES FAGERHULT', 'logo.gif', 'En av Sveriges �ldsta MC-butiker!', 'Nilssons Motor i Sk�nes Fagerhult AB �r en av Sveriges �ldsta MC-butiker. Firman startades 1936 av Erich Nilsson som drev den tillsammans med sin familj.
<p>
Efter ett antal �r tog Erichs son Nils-Arne Nilsson �ver firman, efter att ha varit bosatt i USA en l�ngre tid. Han livn�rde sig d�r som proffs inom motocrossen. Nilssons Motor har alltid varit �terf�rs�ljare f�r Honda, men �ven Husqvarna s�ldes under m�nga �r.
<p>
I december 1999 s�ldes familjef�retaget Nilssons Motor till den inte helt ok�nda motocross-f�raren Fredrik Werner och hans fru Christina. MC-butiken fick lite nytt blod och ocks� nya id�er. Eftersom Fredrik Werner sj�lv varit Husqvarna troget under sin karri�r inom motocrossen, f�ll det sig naturligt att komplettera Hondas breda sortiment med Husqvarnas. En kort tid efter �vertagandet av Nilssons Motor, blev Fredrik tillsammans med sin yngre bror Henrik och Claes Halvards fr�n Eskilstuna, �ven svenska import�rer f�r Husqvarna, CW Import.
<p>
Nilssons Motor drivs idag med Fredrik sj�lv som f�rs�ljare i butiken. Till sin hj�lp har han "landsv�gsproffset" Jan Karlqvist. Patrik Thullgren tr�ffas i verkstan och Christina sk�ter ekonomin. V�gg i v�gg med Nilssons Motor sitter "Henke" och sk�ter reservdelslagret f�r CW Import.
<p>
Vad kan vara b�ttre �n en Svensk M�stare i motocross, som delar med sig av den rutin han har samlat p� sig under dryga 20 �r som motocrossf�rare, varav 10 �r p� elitniv�, b�de nationellt och internationellt.
<p>
Butiken bjuder p� hela Hondas, Husqvarnas och KTMs breda sortiment. Utrustning och reservdelar av b�sta kvalitet och till priser som passar alla. 
<p>
V�LKOMNA!', 'http://www.nilssonsmotor.com/', 'active', false, '2004-04-08 09:55:00', '2016-08-15 23:59:59', 'Honda, Husqvarna, KTM, Derbi, Scooters, Mopeder,
', '-', 'patrik', '-', '-', 'patrik', 'patrik', 23, 7, 1140);
INSERT INTO stores VALUES (3853, 'Movap AB', 'info_movap.se@blocket.se', '0143-103 08', '0143-120 50', 'Granby Oxelg�rd', '592 94', 'VADSTENA', 'movap.gif', 'Nya och begagnade MC till r�tt pris', 'Hej! Till mig kan Du ringa �ven kv�llar och helger, men efter 21.00 och f�re 08.00 �r det bra om Du v�ljer min mobiltelefon 0705-233 633. Dagtid �r jag ibland p� bilprovningen eller h�ller p� med andra v�sentliga aktiviteter som jakt, mc-�kning, segling etc. D� lyssnar jag regelbundet av min "nalle" d�r Du kan l�mna ett meddelande. 

', 'http://www.movap.se', 'active', false, '2003-06-19 11:19:00', '2016-06-15 23:59:59', 'Honda, Kawasaki, Suzuki, Yamaha, ', '-', 'patrik', '-', '-', 'patrik', 'patrik', 14, 0, 1140);
INSERT INTO stores VALUES (3877, 'FastBike', 'info_fastbike.se@blocket.se', '031-14 74 40', '031-14 74 40', '�. S�nkverksg. 2', '41327', 'G�TEBORG', 'fastbike.gif', '�F f�r Husaberg, KTM, Royal Enfield, Derbi', 'Stor sortering av tillbeh�r o kl�der.<br>
Verkstad med tillg�ng till full service och reparationer.
<p>
V�ra �ppettider �r m�n-fre 10,00-18,00<br>
Lunch 12,30-13,30

', 'http://www.fastbike.se
', 'active', false, '2003-09-18 00:00:00', '2016-01-12 23:59:59', 'Husaberg, KTM, Royal Enfield, Derbi, PGO, Daelim, Harley Davidson, Buell
', '-', 'patrik', '-', '-', 'patrik', 'patrik', 15, 0, 1140);
INSERT INTO stores VALUES (3879, 'Fottas MC', 'info_fottasmc.se@blocket.se', '0240-611 70', '0240-841 38', 'Storgatan 45', '77130', 'LUDVIKA', 'butik.gif', '�terf�rs�ljare f�r Gilera, Honda, Kymco, Piaggio, Suzuki, Vespa', '<b>V�lkommen till Fottas MC!</b><br>
H�r hittar du motorcyklar, mopeder, terr�nghjulingar och sn�skotrar. Tillbeh�r och reservdelar s�ljer vi �ver hela landet (och lite till) p� postorder. <br>
Nu kan du �ven se vad en service p� din mc kostar, direkt p� hemsidan.
', 'http://www.fottasmc.se', 'active', false, '2005-08-18 00:00:00', '2016-09-01 23:59:59', 'Honda, Kawasaki, Suzuki, Yamaha, ', '-', 'patrik', '-', '-', 'patrik', 'patrik', 6, 0, 1140);
INSERT INTO stores VALUES (13968, 'Smygehamns Bil & Fritid', 'kontakt_smygehamnsbil.com@blocket.se', '0410-290 90', '0410-290 50', 'Smyge Strandv. 32', '231 78', 'SMYGEHAMN', 'butik_logga.gif', 'M�rkesoberoende handlare', 'Smygehamns Bil & Fritid startade 1978.<br>
Har bilverkstad.', 'http://www.smygehamnsbil.com
', 'active', false, '2005-08-19 00:00:00', '2015-11-30 23:59:59', 'Audi, Buick, Honda, Mercedes, Nissan, Opel, Renault, Saab, Volkswagen, VW, Volvo ', '-', 'patrik', '-', '-', 'patrik', 'patrik', 23, 14, 1020);
INSERT INTO stores VALUES (3894, 'J�rvs� Cykel & Motor', 'info_jcmotor.se@blocket.se', '0651-41243', '0651-412 47', 'Industriv�gen 22', '820 40', 'J�RVS�', 'jarvsocykel.gif', 'Motorcyklar i J�rvs�', 'Vi har flera modeller av Harley-Davidson & Honda f�r provk�rning.<p>
Vi kan erbjuda dig m�nga olika finansieringsalternativ.
<p>
V�lkommen..', 'http://www.jcmotor.se', 'active', false, '2003-05-12 00:00:00', '2015-12-31 23:59:59', 'Harley Davidson, Honda, Suzuki, Peugeot, Yamaha, Buell, HD, Polaris', '-', 'patrik', '-', '-', 'patrik', 'patrik', 5, 0, 1140);
INSERT INTO stores VALUES (3897, 'Lindstr�ms Motor AB', 'kent.brink_lindstromsmotor.se@blocket.se', '040-15 42 97', '040-15 42 86', 'Magasinsg. 7', '21613', 'LIMHAMN', 'lindstromsmotor.gif', 'Vi �r auktoriserade �terf�rs�ljare f�r BMW, Honda och Peugeot', 'V�lkommen till Lindstr�ms Motor AB. <br>
Vi �r auktoriserade �terf�rs�ljare f�r BMW, Honda, PGO och Peugeot. V�r verkstad �r auktoriserad �ven p� Yamaha, men vi reparerar och servar de flesta m�rken.', 'http://www.lindstromsmotor.se', 'active', false, '2003-08-28 13:30:00', '2015-12-31 23:59:59', 'BMW, Honda, Kawasaki, PGO, Peugeot, Triuph, Yamaha', '-', 'patrik', '-', '-', 'patrik', 'patrik', 23, 11, 1140);
INSERT INTO stores VALUES (3923, 'P.O:s Harley Davidson Norrk�ping', 'h-d_olofssonauto.com@blocket.se', '011-14 25 47', '011-14 67 68', 'Navestadsg. 43', '60366', 'NORRK�PING', 'harleynorrk.gif', 'Designstore', 'Unna Dig n�got alldeles extra i �sterg�tlands enda �kta H-D Design Store.
<p>
V�r H-D shop inneb�r en v�lfylld butik med det senaste motorcyklarna,
originaltillbeh�ren, reservdelarna, kl�derna och presentartiklarna f�r Dig
och Din hoj. 
<p>
Vi �r auktoriserade �terf�rs�ljare f�r Harley-Davidson
motorcyklar, tillbeh�r, reservdelar, kl�der, boots och presentartiklar.
<p>
�ppettider<br>
Vardagar 9-18<br>
L�rdagar 11-14
', 'http://www.harley-davidson.se/butiker/norrkoping', 'active', false, '2003-09-11 11:00:00', '2015-11-30 23:59:59', 'Harley Davidson, Buell', '-', 'patrik', '-', '-', 'patrik', 'patrik', 14, 0, 1140);
INSERT INTO stores VALUES (3933, 'Speed i �rebro AB', 'forsaljning_speed-mc.se@blocket.se', '019-22 32 44', '019-22 32 99', 'Firmav�gen. 2', '702 31', '�REBRO', 'speedore.gif', 'APRILIA, HONDA, KAWASAKI, TRIUMPH, PIAGGIO, PEUGEOT, GILERA', 'F�rs�ljning av nya och begagnade mc samt mopeder och fyrhjulingar. Auktoriserad �terf�rs�ljare f�r APRILIA, HONDA, KAWASAKI, TRIUMPH, PIAGGIO, PEUGEOT, GILERA

<p>
Auktoriserad  serviceverkstad, Skadeverkstad �t alla f�rs�kringsbolag. D�ckverkstad. Stor reservdels och tillbeh�rsavdelning med personlig service. Godk�nd och auktoriserad av v�gverket att utbilda EU f�rarbevis.
<p>
Medlem i SMR  Sveriges Motorcykel handlar riksf�rbund, n�got som borgar f�r kvalitet och garanti.
<p>
V�r verkstad f�ljer samtliga milj�direktiv betr�ffande avfallshantering.
<p>
Vi finns vid Marieberg K�pcentrum, ca 5 km s�der om �rebro centrum alldeles vid E20,vid IKEA.
<p>
�ppettider:
<p>
BUTIK<br>
M�n-Fre: 10-18<br>
L�r: 10-14 (tiden 1/3-31/8)<br>
L�r: 10-13 (tiden 1/9-28/2)
<p>
VERKSTAD<br>
M�n-Fre: 8-17<br>
Lunch: 12-13<br>
L�r: St�ngt

', 'http://www.speed-mc.se', 'active', false, '2003-09-11 00:00:00', '2016-03-15 23:59:00', 'Motorcyklar, mopeder, scooter, Eu f�rarutbildning, beg mc,beg moped, beg scooter, Aprilia, Honda, Kawasaki, Triumph, Piaggio, Peugeot, Gilera, mc tillbeh�r, mc reservdelar, Hj�lmar, skinnst�ll, textilkl�der, mc st�vlar, mc handskar, personlig utrustning, skydd, SHOEI,AGV,YES,AIROH,PELTOR,AXO,UFO,YOKO,REV-IT,BLACKSTREET,ARLEEN NEES, OXTAR, Mc verkstad, mc d�ck, Metzeler, Pirelli
', '-', 'patrik', '-', '-', 'patrik', 'patrik', 8, 0, 1140);
INSERT INTO stores VALUES (3935, 'Stibix AB', 'info_stibix.se@blocket.se', '033-23 82 50', '033-25 76 85', 'Verkstadsgatan 10', '504 62', 'BOR�S', 'butik_ny.jpg', 'Your Bike Dealer. Sveriges b�sta priser p� motorcyklar.', '<b>Auktoriserad �terf�rs�ljare f�r Kawasaki</b><p>

V�lkommen till Stibix.<br>
MC butiken v�l v�rd en utflykt.
<p>
S�ker du motorcykel s� har vi l�sningen f�r dig.<br>
Vi har alltid ett stort urval av beg. mc av de flesta fabrikat.<br>
Hos oss f�r du alltid ett trevligt bem�tande av v�r kunniga personal.
<br>
V�lkommen till Stibix f�r din n�sta och b�sta aff�r.
', 'http://www.stibix.se', 'active', false, '2003-09-09 00:00:00', '2016-12-31 23:59:59', 'Aeom, Aprilia, Honda, KTM, Kawasaki, MV Agusta, Suzuki, Yamaha, Hoysung,
Triumph, Harley-Davidson, BMW, HD
', '-', 'patrik', '-', '-', 'patrik', 'patrik', 16, 0, 1140);
INSERT INTO stores VALUES (3941, 'West-Bike Motor & Service', 'glennon_telia.com@blocket.se', '0530-12241', '0530-122 72', 'Landsv�gsgatan 34', '464 31', 'MELLERUD', 'westbike_logo.gif', 'Auktoriserad �terf�rs�ljare f�r PGO, Derbi, Kymco, Rieju, samt Piaggio, Gilera och Vespa', 'V�lkomna till West-Bike - Din MC butik i Dalsland. 
<br>Vi finns i nya lokaler om 600kvm alldeles invid Rv45. ( Rondellen s:a Mellerud )
<p>
<b>Service och reparationer</b><br>
H�r finns m�jlighet att serva och reparera motorcyklar. 
<p>
<b>Nytt och begagnat</b><br>
Vi s�ljer �ven begagnade och nya, b�de custom och sport cyklar.
<p>
Skulle man vilja pynta sin motorcykel med lite fr�ckare ljud eller andra blinkers finns den m�jligheten med. Kort sagt, innanf�r v�ggarna p� West-Bike finns allt f�r dig o din motorcykel.
<p>
Vi n�s �ven p� mobil 0706-22 32 31 
<p>
V�lkomna

', 'http://www.west-bike.se
', 'active', false, '2003-08-28 17:05:00', '2016-09-01 23:59:59', 'Honda, Kawasaki, Suzuki, Yamaha', '-', 'patrik', '-', '-', 'patrik', 'patrik', 16, 0, 1140);
INSERT INTO stores VALUES (3947, 'Halls Bilar', 'hallsbil_algonet.se@blocket.se', '026-27 51 22', '026-27 51 22', 'H�gbov�gen 49', '811 32', 'Sandviken', 'Halls_Bilar.jpg', 'K�per  -  Byter  -  S�ljer', '30 �rs erfarenhet i branchen.
<p>
Vi k�per, byter och s�ljer begagnade bilar.<br>Bra finansiering.
<p>
Hj�rtligt v�lkommen!
', 'http://www.hallsbilar.com', 'active', false, '2003-03-12 00:00:00', '2016-01-31 23:59:59', 'F�rs�ljningsuppdrag, billiga bilar, begagnade bilar, bilar k�pes kontant, Volvo, Saab, Mazda, Audi, BMW, Ford, Toyota, Skoda, Mercedes, Hyundai, Chrysler, Renault, ', '-', 'patrik', '-', '-', 'patrik', 'patrik', 5, 0, 1020);
INSERT INTO stores VALUES (3950, 'Bilf�rmedlingen - H�gersten', 'stockholm_bilformedlingen.com@blocket.se', '08-685 60 66', '08-449 19 88', 'V�stbergav�gen 24', '126 30', 'H�GERSTEN', 'bilformedlingen.gif', 'Vi hj�lper dig till en b�ttre bilaff�r!', 'Genom m�nga �rs erfarenhet av bilbranschen har vi samlat p� oss mycket kunskap om bilaff�rer. Den kunskapen delar vi g�rna med oss av. Skall du s�lja eller k�pa en bil, mc, sl�p, pick-up eller annat fordon som rullar kan vi hj�lpa dig till en b�ttre aff�r tack vare v�r unika aff�rsid�.', 'http://www.bilformedlingen.com', 'active', false, '2003-03-19 00:00:00', '2017-02-28 23:59:59', 'Audi, BMW, Ford, Mazda, Mercedes, Mitsubishi, Peugeot, Porsche, Saab, Volkswagen, VW, Volvo,', '-', 'patrik', '-', '-', 'patrik', 'patrik', 11, 0, 1020);
INSERT INTO stores VALUES (3951, 'Bilf�rmedlingen - �ngelholm', 'engelholm_bilformedlingen.com@blocket.se', '0431-155 60', '0431-155 29', 'Lagegatan 3', '262 71', '�NGELHOLM', 'bilformedlingen.gif', 'Vi hj�lper dig till en b�ttre bilaff�r!', 'Genom m�nga �rs erfarenhet av bilbranschen har vi samlat p� oss mycket kunskap om bilaff�rer. Den kunskapen delar vi g�rna med oss av. Skall du s�lja eller k�pa en bil, mc, sl�p, pick-up eller annat fordon som rullar kan vi hj�lpa dig till en b�ttre aff�r tack vare v�r unika aff�rsid�.', 'http://www.bilformedlingen.com', 'active', false, '2003-03-19 00:00:00', '2017-02-28 23:59:59', 'Audi, BMW, Ford, Hyundai, Mercedes, Saab, Volkswagen, VW, Volvo,', '-', 'patrik', '-', '-', 'patrik', 'patrik', 23, 16, 1020);
INSERT INTO stores VALUES (3952, 'UCARS EXCLUSIVE AB', 'ucar_telia.com@blocket.se', '0413-120 40', '0413-55 46 55', 'Traktorsv�gen 3', '241 38', 'ESL�V', 'ucars_logo.gif', 'M�rkesoberoende handlare', 'Vi l�mnar garanti p� alla bilar; funktionsgaranti p� bilar som �r nyare �n 5 �r och som g�tt mindre �n 5000 mil samt trafiks�kerhetsgaranti p� �vriga bilar.', 'http://www.ucars.se', 'active', false, '2003-03-19 00:00:00', '2016-06-30 23:59:59', 'Audi, BMW, Ferrari, Ford, Mercedes, Porsche, Saab, Volkswagen, VW, Volvo, ', '-', 'patrik', '-', '-', 'patrik', 'patrik', 23, 2, 1020);
INSERT INTO stores VALUES (14235, 'GI Bil AB', 'gibilab_msn.com@blocket.se', '042-16 12 00', '042-16 12 08', 'Gev�rsgatan 17', '254 66', 'HELSINGBORG', 'butik.gif', 'M�rkesoberoende handlare', 'Vi �r ett f�retag som satsar h�rt p� att tillfredst�lla alla v�ra kunder p� b�sta m�jliga vis.<br>
Vi har ett brett sortiment av massor av prisv�rda bilar.<p>
Hos GI bil anv�nder vi oss av v�ldigt personligt bem�tande med trevlig personal som tar hand om Dig p� b�sta s�tt. Har din bil en skavank? Eller beh�ver den bara servas?<br>
V�nd dig till oss s� blir det gjort!<p>

�ppet:<br>
Vardagar 10-18<br>
L�rdag och S�ndag 10-15 eller enligt �verenskommelse.
', 'http://www.gibilab.com

', 'active', false, '2005-10-09 00:00:00', '2016-01-15 23:59:59', 'BMW, Honda, Mazda, Mercedes-Benz, Nissan, Opel, Skoda, Pontiac, Volvo ', '-', 'patrik', '-', '-', 'patrik', 'patrik', 23, 3, 1020);
INSERT INTO stores VALUES (3962, 'Codat Husbilar AB', 'info_codat.se@blocket.se', '0320-721 20', '0320-722 29', 'Box 108', '511 10', 'FRITSLA', 'Codat.gif', '�terf�rs�ljare f�r Dethleffs husbilar och husvagnar och Pilote husbilar.', 'V�lkommen till Codat Husbilar AB - Sveriges �ldsta och mest erfarna husbilsf�retag.<p>
Vi s�ljer Sveriges mest popul�raste husbil Dethleffs och Frankrikes mest s�lda husbil Pilote.
Dethleffs husvagnar, Isabella f�rt�lt, Pioneer GPS-Navigation & Audio, Alpine stereo, KCR Tuning, samt Kamas breda fritidssortiment �r n�gra av de starka varum�rken vi representerar.
<p>
V�lkommen till Din b�sta fritidsaff�r - Codat Husbilar AB - Din fritid = V�rt jobb.
<p>
�ppet- Tider<br>
Vardag 9-18<br>
Fredag 9-16<br>
L�rdag St�ngt<br>
S�ndag 12-15<br>
(under s�song)<p>', 'http://www.codat.se
', 'active', false, '2005-06-29 00:00:00', '2016-08-01 23:59:59', 'B�rstner, Dethleffs, Frankia, Hobby, Hymer, LMC, Nordic, Pilote, Polar, Solifer', '-', 'patrik', '-', '-', 'patrik', 'patrik', 16, 0, 1100);
INSERT INTO stores VALUES (3968, 'Daniels Bil AB', 'daniel.rockenson_comhem.se@blocket.se', '042-16 07 11', '042-16 25 20', 'Gev�rsgatan 9', '254 66', 'HELSINGBORG', 'Daneils.gif', 'K�per S�ljer Byter begagnade bilar', 'K�per, s�ljer, byter begagnade bilar. Garanti. Avbetalning.
<p>
Kan �ven n�s p� tel. 070-7162870
<p>
V�lkommen!', 'http://www.danielsbil.se', 'active', false, '2003-04-01 00:00:00', '2016-11-01 23:59:59', 'BMW, Audi, Mercedes, Volvo, Mazda, Toyota, Ford, Opel, Mitsubishi, Renault, Saab, Volkswagen,', '-', 'patrik', '-', '-', 'patrik', 'patrik', 23, 3, 1020);
INSERT INTO stores VALUES (3969, 'Mal� Karosseriteknik AB', 'info_mkt.se@blocket.se', '0953-102 03', '0953-102 67', 'Industriv�gen 10', '930 70', 'MAL�', '11_3_05.gif', 'Styling & tillbeh�r f�r din bil', '* VI BJUDER P� FRAKTEN VID K�P �VER 3500 KR. FRI FRAKT INOM SVERIGE<p>
* LYKTOR & BLINKERS LEV PARVIS. F�RGADE GL�DLAMPOR MEDF�LJER<p>
*STYLING OCH TILLBEH�R TILL AUDI, BMW, FORD, MB, VW, VOLVO, MM', 'http://www.mkt.se
', 'active', false, '2003-04-01 00:00:00', '2016-06-01 23:59:59', '�ndr�r, backspeglar, baklampor, crome styling, dimljus, d�rrhantag, extraljus, folia tec styling, framblink, framlyktor, �gonlock, rattar, rattnav, sidorblinkers, sportgrill, takspoiler, v�xelspak', '-', 'patrik', '-', '-', 'patrik', 'patrik', 2, 0, 1040);
INSERT INTO stores VALUES (14463, 'G�inge Bil AB i H�ssleholm', 'info_goingebil.se@blocket.se', '0451 - 436 00', '0451 - 124 50', 'Norra Kringelv�gen 70', '281 21', 'H�SSLEHOLM', 'butik.gif', '�terf�rs�ljare f�r Renault, Volvo', '<b>Din Volvo och Renault-�terf�rs�ljare i H�ssleholm, Markaryd, Osby och �lmhult.</b>
<p>
M�nga kunder kommer i f�rtroende till oss eftersom vi inte bara s�ljer bilar - vi s�ljer ett bil�gande.
<p>
En marknadsandel p� ca 30% och en total f�rs�ljning p� runt 1 700 bilar per �r har uppn�tts tack vare en engagerad och kompetent personal som st�r bakom "G�inge Bil f�r trygga mil".
<p>
V�lj oss och du g�r ett s�kert val.
<p>
�ppettider:<br>
Bilf�rs�ljning <br>
Vardagar: 9 � 18 <br>
L�rdagar: 10 � 13 <p>', 'http://www.goingebil.se
', 'active', false, '2005-10-24 00:00:00', '2016-01-31 23:59:59', 'Audi, Ford, Renault, Volkswagen, VW, Volvo', '-', 'patrik', '-', '-', 'patrik', 'patrik', 23, 4, 1020);
INSERT INTO stores VALUES (3974, 'Granuddens Bil AB', 'info_granuddensbil.se@blocket.se', '0920-944 60', '0920-944 58', 'Granuddsv�gen', '972 51', 'LULE�', 'granuddens.gif', 'K�PER - S�LJER - BYTER', 'Granuddens Bil AB �r ett f�retag som ligger 2 km norr om Lule� centrum. F�retaget �r mest inriktat p� dieselbilar. Granuddens Bil AB har �ven en egen verkstad d�r alla bilar servas och genomg�s innan f�rs�ljning. I verkstaden kan vi ta hand om alla f�rekommande verkstadsarbeten samt att vi �ven har en komplett d�ckverkstad.', 'http://www.granuddensbil.se', 'active', false, '2003-04-13 00:00:00', '2016-03-01 23:59:59', 'Mercedes, Saab, Volvo', '-', 'patrik', '-', '-', 'patrik', 'patrik', 1, 0, 1020);
INSERT INTO stores VALUES (13284, 'Auto Classica Of Sweden', 'mats_autoclassica.com@blocket.se', '0411-66666/36', '0411-66617', 'M�ssingsgatan 3', '271 39', 'YSTAD', 'AutoClassica_logo.gif', 'M�rkesoberoende handlare', 'Auto Classica har s�dra Sveriges st�rsta importerfarenhet av sportbilar, lyxbilar, 4WD, minivans, diesel och standardbilar. Vi har idag ett 30-tal leverant�rer ute i Europa och kan inom en tio dagars period s�ka upp och leverera ett fordon utrustat efter era egna �nskem�l. Vi g�r upps�kning samt hemtagning med full f�rs�kring, homologosering, uppgradering, registreringsbesiktning, M-testning samt finansiering. Dessutom har vi en verkstad med tv� erfarna mekaniker. Vi �tar oss att v�rdera och s�lja ert inbyte.
<p>     
<B>Garanti</B><br>
Auto Classica har sedan en tid tillbaks ett samarbete med AB Svensk Bilgaranti. Vi hj�lper dig att hitta den smartaste l�sningen f�r just din bil. AB Svensk Bilgaranti �r sedan m�nga �r ett av de ledande f�retagen n�r det g�ller garantier p� bilar. 
<p>  
<B>Finans</B><br>
Vi hj�lper dig �ven att hitta den smartaste l�sningen f�r finansieringen av ditt bilk�p. Detta har vi l�st genom samarbete med Nordea.
<p>
<B>F�rs�kring</B><br>
Vi hj�lper dig sj�lvklart �ven med att f� fram den r�tta f�rs�kringen till din bil. H�r har vi ett samarbete med L�nsf�rs�kringar och Folksam f�r att uppn� b�sat resultat.

 
', 'http://www.autoclassica.com
', 'active', false, '2004-11-01 11:09:00', '2016-06-15 23:59:59', 'Alfa Romeo, Audi, BMW, Chevrolet, Chrysler, Jaguar, Jeep, Mercedes, Porsche, Volvo', '-', 'patrik', '-', '-', 'patrik', 'patrik', 23, 15, 1020);
INSERT INTO stores VALUES (3975, 'Bilaff�ren i Lule�', 'info_bilaffarenlulea.com@blocket.se', '0920-23 21 10', '0920-23 21 10', 'Banv�gen 21', '973 46', 'LULE�', 'Bilaffaren.gif', 'Din Bilaff�r i LULE�', 'Hitta din dr�mbil p� v�r webbplats eller g�r ett bes�k hos oss i v�ran trevliga lokal. Vare sig det �r en exklusiv sportvagn eller en trygg och p�litlig familjebil du s�ker hittar du det hos oss. Och detta till mycket konkurrenskraftiga priser ackompanjerat av en personlig och trevlig service.', 'http://www.bilaffarenlulea.com
', 'active', false, '2003-04-14 00:00:00', '2016-02-01 23:59:59', 'Audi, Ford, Honda, Jaguar, VW, Volvo, Saab, Mercedes', '-', 'patrik', '-', '-', 'patrik', 'patrik', 1, 0, 1020);
INSERT INTO stores VALUES (3980, 'Mikaels Bilar', 'micke.sjoberg_telia.com@blocket.se', '0660-512 00', '0660-986 78', 'Transformatorv�gen 3', '892 32', 'DOMSJ�', 'Mikaelsbilar.gif', 'M�rkesoberoende handlare', 'Vi har s�lt bilar sedan -99 fr�mst Tyska bilar men �ven andra m�rken "vi byter �ven in importerade bilar" Vi s�ljer D�ck och F�lgar till kraftigt konkurrens m�ssiga priser kolla v�ra erbjudande. V�lkommna.', 'http://www.mikaelsbilar.com
', 'active', false, '2005-03-21 00:00:00', '2016-06-01 23:59:59', 'Audi, BMW, Chevrolet, Chrysler, Dodge, Jeep, Mercedes, Volkswagen, VW, Volvo, Peugeot', '-', 'patrik', '-', '-', 'patrik', 'patrik', 4, 0, 1020);
INSERT INTO stores VALUES (3981, 'Bilf�rmedlingen - Helsingborg', 'helsingborg_bilformedlingen.com@blocket.se', '042-37 93 10', '042 37 93 11', 'Florettgatan 23', '254 67', 'HELSINGBORG', 'bilformedlingen.gif', 'Vi hj�lper dig till en b�ttre bilaff�r!', 'Genom m�nga �rs erfarenhet av bilbranschen har vi samlat p� oss mycket kunskap om bilaff�rer. Den kunskapen delar vi g�rna med oss av. Skall du s�lja eller k�pa en bil, mc, sl�p, pick-up eller annat fordon som rullar kan vi hj�lpa dig till en b�ttre aff�r tack vare v�r unika aff�rsid�.', 'http://www.bilformedlingen.com', 'active', false, '2003-03-19 00:00:00', '2017-02-28 23:59:59', 'Audi, BMW, Ford, Mercedes, Mitsubishi, Nissan, Porsche, Volkswagen, VW, Volvo,', '-', 'patrik', '-', '-', 'patrik', 'patrik', 23, 3, 1020);
INSERT INTO stores VALUES (3982, 'LA Invest', 'lainvest_algonet.se@blocket.se', '0240-845 50', '-', 'Sn��v�g 159', '771 94', 'LUDVIKA', '-', 'F�rs�ljning Av Amerikanska Pickups.', 'F�rs�ljning Av Amerikanska Pickups.
<p>
Mobilnr: 070-532 99 99', '-', 'active', false, '2003-04-23 00:00:01', '2016-07-01 23:59:59', 'Chevrolet, Gmc, Dodge, Pickup', '-', 'patrik', '-', '-', 'patrik', 'patrik', 6, 0, 1020);
INSERT INTO stores VALUES (3986, 'Tomas Kaakinen Bil AB', 'tomas.kaakinen_kaakinenbil.se@blocket.se', '0243-68022', '0243-87940', 'Hugo Hedstr�msv�g 6', '781 72', 'BORL�NGE', 'kaakinen.gif', 'Din Chrysler-handlare i Dalarna', 'Chrysler & Jeep handlare
<p>
Verkstad f�r Chrysler, Jeep och Nissan<br>
Ni kan n� oss p� f�ljande Tfn:<br>
Verkstad: 0243-864 44<br>
Reservdelar: 0243-864 47<br>
<p>
V�lkommen!', 'http://www.kaakinenbil.se
', 'active', false, '2003-04-25 00:00:00', '2016-01-31 23:59:59', 'Chrysler, Jeep, Nissan, Ford, Mercedes, Peugeot, Renault, Volvo,', '-', 'patrik', '-', '-', 'patrik', 'patrik', 6, 0, 1020);
INSERT INTO stores VALUES (3993, 'Zackrisson Bil & MC AB', 'le.zackrisson_telia.com@blocket.se', '0225-127 13', '0225-77 44 45', 'Backa 12', '776 92', 'HEDEMORA', 'logo_liten.gif', 'Import av bilar, motorcyklar, husbilar, specialbilar mm.', 'V�r m�ls�ttning �r 100% helt n�jda kunder. Alla v�ra f�rs�ljningsobjekt
s�ljes med garantier. M�ng�rig erfarenhet av import fr�n Europa & USA. Tar
�ven �t oss importuppdrag f�r era speciella �nskem�l.
<p>
Mobilnr: 070-573 01 51
', 'http://www.bilomc.nu', 'active', false, '2003-04-29 00:00:00', '2016-05-01 23:59:59', 'Bil, MC', '-', 'patrik', '-', '-', 'patrik', 'patrik', 6, 0, 1020);
INSERT INTO stores VALUES (3994, 'Zackrisson Bil & MC AB', 'le.zackrisson_telia.com@blocket.se', '0225-127 13', '0225-77 44 45', 'Backa 12', '776 92', 'HEDEMORA', 'logo_liten.gif', 'Import av bilar, motorcyklar, husbilar, specialbilar mm.', 'V�r m�ls�ttning �r 100% helt n�jda kunder. Alla v�ra f�rs�ljningsobjekt
s�ljes med garantier. M�ng�rig erfarenhet av import fr�n Europa & USA. Tar
�ven �t oss importuppdrag f�r era speciella �nskem�l.
<p>
Mobilnr: 070-573 01 51
', 'http://www.customhojar.nu', 'active', false, '2003-04-29 00:00:00', '2016-05-01 23:59:59', 'Bil, MC, Kawasaki, Yamaha ', '-', 'patrik', '-', '-', 'patrik', 'patrik', 6, 0, 1140);
INSERT INTO stores VALUES (3995, 'Michaelsson & Nelin AB', 'hans.mnmotor_telia.com@blocket.se', '018-13 90 40', '018-12 86 99', 'Fyrislundsgatan 76', '754 50', 'UPPSALA', 'Nelin.gif', 'Alltid ca 225 bilar i lager av blandade fabrikat.', '<p>
Firman startades 1988 av Hans Nelin och Andreas Michaelsson och finns i Uppsala.
<p>
Oms�ttningen 2004 l�g p� 150 miljoner kronor.
Totalt s�ljer vi ca 1350 bilar per �r och har specialicerat oss p� att s�lja nyare begagnade bilar av alla m�rken. 
<p>
V�lkommen in s� hj�lps vi �t att hitta en bil som passar just DIG!
', 'http://www.mnmotor.com', 'active', false, '2003-04-29 00:00:00', '2016-06-01 23:59:59', 'Alfa, Audi, BMW, Chevrolet, Chrysler, Fiat, Ford, Jaguar, Jeep, Hyundai, Land Rover, Mitsubishi, Opel, Peugeot, Saab, Toyota, Volkswagen, Volvo', '-', 'patrik', '-', '-', 'patrik', 'patrik', 10, 0, 1020);
INSERT INTO stores VALUES (14443, 'Ruma Bil', 'rumabil_telia.com@blocket.se', '019-10 12 75', '019-23 20 36', 'Kyrkv�gen 27', '703 75', '�REBRO', 'butik.jpg', 'M�rkesoberoende handlare', '<b>V�lkommen till Ruma Bil.</b><p>
Ruma Bil �gs och drivs av Runar Fall. Vi k�per, byter och s�ljer begagnade bilar. <p>

F�r att se det aktuella bilutbudet skall du bes�ka bilhallen. <br>
D�r kan du �ven  skicka intresseanm�lan p� bilar du �r intresserad av. <p>

Vid behov ordnar vi finansiering i Nordbanken Finans till en f�rdelaktig r�nta. <br>
Avbetalning kan ske p� mellan 12 till 60 m�nader.  ', 'http://www.rumabil.se

', 'active', false, '2005-10-19 00:00:00', '2016-01-31 23:59:59', 'Audi, BMW, Ford, Hyundai, Mercedes-Benz, Saab, Seat, Volkswagen, VW, Volvo', '-', 'patrik', '-', '-', 'patrik', 'patrik', 8, 0, 1020);
INSERT INTO stores VALUES (14444, 'Tomaz Lundgren AB', 'tomaz.lundgren_telia.com@blocket.se', '0706-43 70 40', '060-250 95', '-', '-', 'MATFORS', 'butik.jpg', 'F�rs�ljning av fritidsprodukter till R�TT PRIS', 'TOLAB/Tomaz Lundgren AB s�ljer allehanda fritidsprodukter s�som; ATV/Fyrhjulingar, olika modeller av cross-motorcyklar, EU-mopeder, mini-chopprar , mini-skoter etc.
<p>
V�lkommen till oss i Matfors f�r att titta och provk�ra.
<p>

Vi har �ven ett stort sortiment av massagebadkar och olika modeller av duschar, genom v�rt samarbete med Importlagret.com.
<p>
M�jlighet till r�ntefri avbetalning p� 9 m�nader.
<p>
Vi har �ven en husbil till uthyrning, boka inf�r semestern ! .

', '-', 'active', false, '2005-10-19 00:00:00', '2016-10-24 23:59:59', 'ATV, fyrhjulingar, cross-motorcyklar, EU-mopeder, mini-chopprar , mini-skoter, mopeder, cross, mc, massagebadkar, duschar ', '-', 'patrik', '-', '-', 'patrik', 'patrik', 4, 0, 8040);
INSERT INTO stores VALUES (4004, 'Bilomobil AB', 'info_bilomobil.se@blocket.se', '060-10 11 90', '060-10 12 90', 'Axv�gen 6', '853 50', 'SUNDSVALL', 'logga.gif', 'MRF Handlare. S�ljer n�stan nya bilar - husbilar', 'Vi �r ett ungt f�retag med massor av rutin.
�garen har sysslat med bilar och aff�rer i �ver 30 �rs tid.
<p>
V�r aff�rsid� �r:<br>
- f�rs�ljning av n�stan nya personbilar.<br>
- import�r f�r s�dra och mellersta Norrland av Dethleffs husbilar och husvagnar.<br>
- import�r av Carthago husbilar.
<p>
MRF-handlare.', 'http://www.bilomobil.se', 'active', false, '2003-04-30 11:13:00', '2016-09-01 23:59:59', 'Dethleffs, Fiat, Mercedes, Saab, Toyota, Volvo, husvagnar, husvagn, husbil, husbilar,', '-', 'patrik', '-', '-', 'patrik', 'patrik', 4, 0, 1100);
INSERT INTO stores VALUES (13692, 'Bilmetro AB, Bolln�s', 'stefan.jonsson_bilmetro.se@blocket.se', '0278 - 63 66 00', '0278 - 63 66 10', 'Renshammarsv�gen 6', '82110', 'BOLLN�S', '15_3_05.gif', '�terf�rs�ljare f�r Volkswagen', '<b>V�lkommen till Bilmetro.se Din B�sta Bilaff�r</b>
<p>
Bilmetro, ett av landets �ldsta bilf�retag, startades redan �r 1922 i Hudiksvall. 
<p>
Bilmetro har nu n�rmare 450 anst�llda p� 13 orter i G�vleborgs- och Dalarnas l�n. Tillsammans oms�tter vi cirka 1,3 miljarder kronor. I Avesta, Bolln�s, Borl�nge, Falun, G�vle, Hedemora, Hudiksvall, Ljusdal, Ludvika, Mora, Sandviken och S�derhamn har vi kompletta bilanl�ggningar med f�rs�ljning, service och verkstad. I R�ttvik har vi enbart bilhall. Kunder som bor p� orter d�r vi inte har egen verksamhet, kan f� hj�lp fr�n n�gon av de nio neutrala verkst�der runtom i l�nen som �r knutna till oss.
<p>
Bilmetro �r inte bara ett av Sveriges ledande bilf�retag, vi �r ocks� ett av de st�rsta privat�gda i branschen. Vi har en sj�lvklar m�ls�ttning f�r v�rt arbete, n�got som g�ller b�de kunder och medarbetare.
<p>
Vi ska alltid vara Din b�sta bilaff�r.         
<p>
�ppettider:<br>
m�ndag-torsdag: 09.00-18.00  <br>
fredag: 9.00-17.00 <br>', 'http://www.bilmetro.se
', 'active', false, '2005-03-15 00:00:00', '2016-10-01 23:59:59', 'Audi, Skoda, Volkswagen, VW, Volvo ', '-', 'patrik', '-', '-', 'patrik', 'patrik', 5, 0, 1020);
INSERT INTO stores VALUES (4003, 'Ride AB', 'info_ride.se@blocket.se', '090-70 10 30', '090-70 10 31', 'Gr�ddv�gen 17', '906 20', 'UME�', 'ride.gif', 'MC-s�ljare i Ume�', 'Som medlem f�r ni rabatter i butiken, annonsera gratis hemsidan och ett nyhetsbrev med intressanta nyheter i branchen samt en massa specialerbjudande. ', 'http://www.ride.se

', 'active', false, '2003-04-30 10:00:00', '2016-05-15 23:59:59', 'Honda, KTM', '-', 'patrik', '-', '-', 'patrik', 'patrik', 2, 0, 1140);
INSERT INTO stores VALUES (4006, 'Haralds B�tar AB', 'haraldsbatar_telia.com@blocket.se', '0243-130 19', '0243-22 82 70', 'Hugo Hedstr�ms v�g 12', '781 72', 'BORL�NGE', 'haralds.gif', 'Genom stora ink�p kan vi h�lla b�sta priser!', 'Haralds B�tar AB �r ett stabilt f�retag som verkat sedan sextiotalet. 
<p>

V�r aff�rsid� best�r, liksom hos stormarknader att g�ra m�ngdink�p f�r att sedan s�lja till stormarknadspriser.
<p>
Genom dessa storink�p direkt fr�n fabrik eller import�r kan vi leverera produkter till oslagbara priser. 
', 'http://www.haraldsbatar.se', 'active', false, '2003-05-05 11:00:00', '2016-06-01 23:59:59', 'B�tar, skotrar, Polaris, Lynx, Yamaha, Kawasaki', '-', 'patrik', '-', '-', 'patrik', 'patrik', 6, 0, 1180);
INSERT INTO stores VALUES (4007, 'MC Huset, Lingvalls Motor', 'info_lingvalls.se@blocket.se', '026-51 75 41', '026-14 03 26', 'Kryddstigen 12', '802 92', 'G�VLE', 'butik_logga.gif', '�terf�rs�ljare f�r Honda, Kawasaki, Triumph, Yamaha', 'V�lkommen till v�ra nya o fr�scha lokaler p� kryddstigen.

<p>

K�p din MC hos oss. F�retaget med 30 �rs erfarenhet.
Alltid intressanta cyklar i lager.
<p>


Fullservice verkstad.

<p>

Stor sortering av kl�der och hj�lmar.
<p>


�ppet: M�n - Fre 10-18
<br>L�r 10-13

', 'http://www.lingvalls.se/', 'active', false, '2003-05-05 00:00:00', '2016-09-01 23:59:59', 'Honda, Kawasaki, Triumph, Yamaha', '-', 'patrik', '-', '-', 'patrik', 'patrik', 5, 0, 1140);
INSERT INTO stores VALUES (4011, 'Bromsplans Bil, �rebro', 'bromsplansbil_telia.com@blocket.se', '019-12 09 90', '019-12 00 97', 'N. Lill�strand 7', '703 65', '�REBRO', 'brommaplanslogg.gif', 'S�ljer bra bilar med l�ga marginaler', 'G�r du i bilk�partankar och funderar p� en begagnad bil? Vill du s�lja din? Eller vill du byta upp dig? B�rja g�rna ditt letande hos oss. Du kanske inte beh�ver leta mer efter ett bes�k . Vi k�per, s�ljer och byter bilar. Vi har personbilar, l�tta lastbilar och minibussar till billiga priser. Det �r v�l v�rt ett bes�k.
<p>
Ni �r v�lkommen att kontakta oss f�ljande telenr:
<br>
Rahim 0705-64 76 10
<br>
Jonas 0731-53 28 68', 'http://www.bromsplansbil.se', 'active', false, '2003-05-06 14:00:00', '2016-06-01 23:59:59', 'Audi, BMW, Ford, Mazda, Mercedes, Mitsubishi, Opel, Skoda, Toyota, Volkswagen, Golf, Passat, Volvo', '-', 'patrik', '-', '-', 'patrik', 'patrik', 8, 0, 1020);
INSERT INTO stores VALUES (4014, 'City Automobile  i �rebro AB', 'cityautomobil_yahoo.se@blocket.se', '019 - 12 98 68', '019 - 22 38 69', 'Droskv�gen 24', '702 33', '�REBRO', '4_1_05.gif', 'S�ljer bra bilar till L�ga Priser', 'Hej och varmt v�lkommen till City Automobil AB.<br>
Funderar du p� byta upp dig eller k�pa en ny bil? 
Vill du s�lja din begagnade bil, d� har du kommit r�tt. 
<p>
Kom in till oss och kolla i v�rat lager. Vi har alltid 20-30 st bilar i lager.
Du beh�ver inte leta mer efter ditt bes�k hos oss.<br>
Vi k�per, byter och s�ljer alla typer av bilar och bilm�rken tex personbilar och minibussar till bra priser.<br>
Skrotningsavgift kan tillkomma p� vissa bilar.
<p> 
Hittar du bilen du vill ha hos oss s� kan vi �ven ordna med finansiering d� vi samarbetar med flera finansbolag (nordea finans och Skandia finans).<br>OBS! Vi erbjuder alla kunder som k�per bil hos f�r 5,95% i r�nta.
<p>
1. Avbetalning<br>
2. Bank�verf�ring<br>
3. Internet �verf�ring<br> 
4. Kontant eller postv�xel <b>(OBS! EJ BETALKORT!)</b>
<p>
Det �r v�l v�rt ett bes�k hos oss i v�r bilhall 
<p>
Oss hittar ni i Mos�s, 3km fr�n IKEA k�pcenter. I samma byggnad som ITABB.
<p>
<b>�ppetider:</b>
<br>M�ndag till Fredag 10-18<br>
�vriga tider enligt �verenskommelse
<p>

<b>Mobil nummer :</b>
0708 - 88 82 90
<p>', 'http://www.cityautomobil.se', 'active', false, '2003-05-09 00:00:00', '2016-03-15 23:59:59', 'Audi, BMW, Fiat, Ford, Mercedes, Saab, Volvo', '-', 'patrik', '-', '-', 'patrik', 'patrik', 8, 0, 1020);
INSERT INTO stores VALUES (4015, 'Nyk�pings Bilmarknad', 'nk.bilmarknad_telia.com@blocket.se', '0155-28 58 50', '0155-29 46 86', 'Norra Bang�rdsgatan 1', '611 39', 'NYK�PING', 'nykopings.gif', 'K�PER-S�LJER-BYTER', 'Vi specialiserar oss p� begagnade bilar.
<p>
�ppet: M�n-fre 10.00-18.00<br>
�vrig tid:0707-974855', 'http://www.nkbilmarknad.nu', 'active', false, '2003-05-08 00:00:00', '2015-11-30 00:00:00', 'Audi, Chevrolet, Chrysler, Ford, Opel, Saab, Toyota, Volkswagen, Volvo', '-', 'patrik', '-', '-', 'patrik', 'patrik', 12, 0, 1020);
INSERT INTO stores VALUES (4020, 'Flyinge Bil', 'zoran_flyingebil.com@blocket.se', '046 - 521 11', '046 - 525 11', 'Holmbyv�gen 18', '240 32', 'FLYINGE', '2_3_05.gif', 'M�rkesoberoende handlare', 'V�lkommen till oss p� Flyinge Bil & Traktor! <p>
Flyinge Bil �r idag ett familje�gt f�retag som grundades f�r mer �n 60 �r sedan. 
<p>
Varje �r levererar vi ca 500 bilar till n�jda kunder.
<p>
N�ra samarbete med auktoriserade �terf�rs�ljare, erfarna ink�pare och egen verkstad g�r 
att v�ra bilar �r noga kontrollerade och genomg�ngna, samtidigt som vi kan h�lla ett v�ldigt f�rdelaktigt pris.
<p>
Vi har alltid ett brett sortiment av nyare begagnade bilar p� lager i v�r nya bilsalong med
varierande prisklass. Sedan-, coup�-, och kombimodeller, stora som sm�.
<p>
F�rutom bilf�rs�ljning erbjuder vi �ven service och reparationer av de flesta bilm�rken i 
v�r egen verkstad av kunnig och kompetent personal. <br>
Vi garanterar ett v�l utf�rt arbete och l�mnar fasta priser p� det mesta.
<p>
Vill du ha hj�lp eller har fr�gor ang�ende Ditt fordon �r Du v�lkommen till oss!
<p>
�ppettider:<br>
Vardagar 8.00 - 17.00<br>
Helger    10.00 - 14.00<br>', 'http://www.flyingebil.com
', 'active', false, '2005-03-02 00:00:00', '2016-06-15 00:00:00', 'Chrysler, Ford, Nissan, Opel, Renault, Toyota, ', '-', 'patrik', '-', '-', 'patrik', 'patrik', 23, 10, 1020);
INSERT INTO stores VALUES (4022, 'Ennefors Bilfirma - Grundad 1975', '-', '0733-12 51 24', '019 - 25 71 70', '-', '702 31', '�REBRO', '-', 'Bilf�rs�ljning av begagnade bilar', 'Grundat 1975<br>
Seri�sa Bilk�pare. Ring 0733-12 51 24.<p>
"SAAB-SERVICE" fr. 1350:- inkl.olja,filter,stift.<p>
Jag h�mtar och l�mnar bilen inom �rebro !!!!', '-', 'active', false, '2005-04-04 00:00:00', '2016-08-15 23:59:59', 'Saab, Volvo ', '-', 'patrik', '-', '-', 'patrik', 'patrik', 8, 0, 1020);
INSERT INTO stores VALUES (4023, 'Bilexpo.nu', 'info_bilexpo.nu@blocket.se', '046 - 14 10 80', '046 - 14 10 94', 'F�ltspatsv�gen 1B', '224 78', 'LUND', 'bilexpo_logo_ny.gif', 'Det personliga bilf�retaget med kunden i centrum.', 'V�r ambition �r att till kund erbjuda fina och v�lv�rdade bilar oftast med en �gare och full dokumentation.
<p>
Vi kan ocks� erbjuda mycket attraktiva priser p� alla v�ra bilar, utan att pruta p� kvalitet och service. P� BilExpo st�r kunden alltid i centrum, �ven efter bilk�pet.
<p>
Vi k�per din bil kontant! Restskulder l�ses.
<p>
V�lkomna!', 'http://www.bilexpo.nu', 'active', false, '2003-05-14 11:50:00', '2016-06-15 23:59:59', 'Alfa Romeo, Audi, BMW, Chrysler, Fiat, Ford, Honda, Hyundai, Jaguar, Mercedes, Mitsubishi, Opel, Peugeot, Renault, Suzuki, Volkswagen, Volvo', '-', 'patrik', '-', '-', 'patrik', 'patrik', 23, 10, 1020);
INSERT INTO stores VALUES (4024, 'Staffanstorps Bil City', 'ask_staffanstorpsbilcity.com@blocket.se', '046-25 57 20', '046-25 67 55', 'Sk�nev�gen 55', '245 38', 'STAFFANSTORP', 'staffanstorps.gif', 'M�rkesoberoende handlare', 'Staffanstorps Bilcity �r ett familjef�retag som grundades redan 1987
av Agne Ask. Sedan 1989 �r �ven Rikard Ask delaktig i f�retaget.
<p>

Vi har s�ledes en m�ng�rig erfarenhet av att utf�ra 
professionell service och att s�lja bilar.
', 'http://www.staffanstorpsbilcity.com', 'active', false, '2003-05-14 00:00:00', '2016-05-01 23:59:59', 'Audi, Ford, Honda, Renault, Toyota, Volvo', '-', 'patrik', '-', '-', 'patrik', 'patrik', 23, 10, 1020);
INSERT INTO stores VALUES (4025, 'Anders �holm Automobilf�rs�ljning', 'anders_oholmbil.com@blocket.se', '036-13 70 70', '-', 'Hakarpsv�gen 112', '561 39', 'HUSKVARNA', '-', 'M�rkesoberoende Bilf�retag', 'Volvo BMW VAG-Bilar.
<p>
Egen imoport p� kundens beg�ran.
<p>
Mobil: 070 922 92 33', '-', 'active', false, '2003-05-14 01:00:00', '2016-05-01 23:59:59', 'Volvo, BMW, VAG', '-', 'patrik', '-', '-', 'patrik', 'patrik', 17, 0, 1020);
INSERT INTO stores VALUES (4026, 'MSA Bilcenter', 'info_msabilcenter.se@blocket.se', '040-461 500', '-', 'Lundav�gen 51', '23252', '�KARP', 'msabilcenter.gif', 'M�rkesoberoende handlare', '<b>V�lkommen till MSA Bilcenter</b><p>

Flesta av v�ra bilar levereras med 12 m�naders garanti, f�r mera information bes�ka: www.autogarantiab.com <p>

�ppet:<br>
M�ndag - Fredag 11.00-18.00<br>
L�rdag - S�ndag 11.00-15.00 <p>

V�lkommen ! 
', 'http://www.msabilcenter.se
', 'active', false, '2005-09-19 00:00:00', '2015-12-31 23:59:59', 'Audi, Ford, Honda, Mazda, Opel, Volkswagen, VW, Volvo', '-', 'patrik', '-', '-', 'patrik', 'patrik', 23, 11, 1020);
INSERT INTO stores VALUES (13635, 'Helmia Bil AB, Sunne', 'malin.jakobsson_helmia.se@blocket.se, bg.thorin_helmia.se@blocket.se', '0565 - 74 40 00', '0565 - 74 40 50', 'Norrg�rdsgatan 1', '686 23', ' SUNNE', 'Helmia_logo.gif', '�terf�rs�ljare f�r Renault, Volvo', 'Helmia Bil AB �r �terf�rs�ljare f�r Volvo och Renault i V�rmland. F�retaget �r ett familjef�retag som �gs och drivs av familjen Walfridson genom moderbolaget Helmia AB. Verksamheten best�r av f�rs�ljning av nya och begagnade personbilar och l�tta transportbilar, service, reparationer, bildelar, tillbeh�r, drivmedel, finansieringstj�nster samt f�rs�kringsf�rmedling. 
', 'http://www.helmia.se
', 'active', false, '2004-11-08 09:06:00', '2016-05-15 23:59:59', 'Renault, Volvo, Mercedes, Nissan', '-', 'patrik', '-', '-', 'patrik', 'patrik', 7, 0, 1020);
INSERT INTO stores VALUES (4028, 'Lundav�gens Bil AB', 'lundavagensbil_telia.com@blocket.se', '040-29 31 31', '040-29 85 55', 'Lundav�gen 58', '200 25', 'MALM�', 'lundavagen_logo.gif', 'M�rkesoberoende handlare', '<li>Mobil: 070-776 53 56', 'http://www.bytbil.com/lundavagensbil/', 'active', false, '2003-05-19 11:00:00', '2016-01-15 23:00:00', 'Audi, BMW, Chrysler, Ford, Harley Davidson, Jaguar, Kia, Mazda, Mercedes, Nissan, Opel, Renault, Saab, Toyota, Volvo', '-', 'patrik', '-', '-', 'patrik', 'patrik', 23, 11, 1020);
INSERT INTO stores VALUES (4029, 'K�glingev�gens Bil', 'info_bilartillsalu.se@blocket.se', '040-54 54 95', '040-54 54 76', 'K�glingev�gen 109', '238 43', 'OXIE', 'kangline.gif', 'K�per, S�ljer, Byter', 'F�r bilar i prisklassen 30.000 kronor eller l�gre har vi en unik avbetalningsform. 
<li>V�lj amorteringstid 0-24 m�nader 
<li>R�ntefritt, endast en uppl�ggningsavgift om 295 kronor 
<li>Fast m�nadskostnad 
<li>L�net kan l�sas n�r Du vill - utan extra kostnader
<p>
�ppettider: 
<li>m�ndag - fredag 10-18
<li>s�ndag 10-15
<p>
V�lkommen !  
', 'http://www.bilartillsalu.se', 'active', false, '2003-05-19 16:30:00', '2016-06-30 23:00:00', 'Audi, BMW, Citroen, Ford, Honda, Hyundai, Mazda, Mitsubishi, Nissan, Opel, Peugeot, Renault, Saab, Skoda, Toyota, Volkswagen, Volvo', '-', 'patrik', '-', '-', 'patrik', 'patrik', 23, 11, 1020);
INSERT INTO stores VALUES (14129, 'NCM System Service AB', 'info_ncm-ss.se@blocket.se', '0692-200 04', '0692-200 80', 'Sillre 178', '860 41', 'LIDEN', 'butik.jpg', 'Exklusiv Bombardier-�terf�rs�ljare.', '<b>NCM SYSTEM SERVICE AB</b> �r en exklusiv Bombardier-�terf�rs�ljare, vilket inneb�r att vi s�ljer <b>SKI-DOO</b> och <b>LYNX</b> sn�skotrar, samt <b>Bombardier ATVs</b>. <p>

Dessutom driver vi en toppmodern serviceverkstad f�r att kunna ge b�sta service till dig och din skoter.<p>

�ppettider:<br>
M�ndag - fredag: 08 - 16.30 <br>
L�rdag: 10 - 14, sommartid st�ngt <br>
�vrig tid enligt �verenskommelse.<p>

V�lkommen! 
', 'http://www.ncm-ss.se

', 'active', false, '2005-09-22 00:00:00', '2016-04-01 23:59:59', 'Bombardier, Ski-doo, Lynx, Bombardier ATV, ATV, Skoter, Verkstad, Sn�skotrar, Servicverkstad', '-', 'patrik', '-', '-', 'patrik', 'patrik', 4, 0, 1180);
INSERT INTO stores VALUES (4033, 'Christells Bil AB', 'marcus.christell_skivarp.se@blocket.se', '0411-305 03', '0411-305 03', 'Norrbackav�gen 5', '274 51', 'SKIVARP', '-', 'K�PER-BYTER-S�LJER', 'Vi har �ppet vardagar 7-17.
<br>
�vrig tid efter �verenskommelse: 0705-345300
<p>
V�lkommen !
', '-', 'active', false, '2003-05-21 00:00:00', '2016-06-01 23:59:59', 'Lastbilar, Bilar, Entreprenad, Iveco, Mercedes, Mitsubishi, Nissan, Opel, Peugeot, Saab, Toyota, Volkswagen, Husvagn, Volvo', '-', 'patrik', '-', '-', 'patrik', 'patrik', 23, 15, 1020);
INSERT INTO stores VALUES (4034, 'Bara Bilar', 'bo.faltin_swipnet.se@blocket.se', '040-54 98 99', '040-54 97 99', 'V�ngav�gen 7', '238 41', 'GLOSTORP', 'logo.gif', 'M�rkesoberoende handlare', 'V�lkommen!<br>
Till ett f�retag specialiserat p� tyska bilar, MB, Mercedes, Audi och BMW. 
<p>
25 �r i branschen.
<br>
Stort kontaktn�t.
<p>
Jag har ambition att ha l�ga omkostnader och l�ga priser samt 100% n�jda kunder.
<p>
Vid best�llning skr�ddarsyr jag ert koncept betr�ffande f�rg, utrustning och utf�rande.', 'http://www.barabilar.se/', 'active', false, '2003-05-22 00:00:00', '2016-06-01 23:59:59', 'BMW, Chrysler, Jaguar, Land Rover, Mercedes, Volvo', '-', 'patrik', '-', '-', 'patrik', 'patrik', 23, 11, 1020);
INSERT INTO stores VALUES (4035, 'KC Bil & Truck AB', 'info_kcbil-truck.se@blocket.se', '036-71 56 29', '036-16 58 70', 'K�mpev�gen 17', '553 02', 'J�NK�PING', '28_12_04.jpg', 'F�rs�ljning av Amerikanska bilar', '<p>
Import och f�rs�ljning av amerikanska Bilar,Vans och Pick-Ups, M�ng�rig erfarenhet av Import av Amerikanska bilar. G�r som m�nga andra n�jda kunder, v�nd dig till oss om du vill k�ra Amerikanskt.
<p>
Serviceverkstad och F�rs�ljning av Tillbeh�r.
<p>', 'http://www.kcbil-truck.se
', 'active', false, '2003-05-22 00:00:00', '2015-12-31 23:59:59', 'Cadillac,Chevrolet,Chrysler,Buick,Dodge,Ford,Jeep,Lincoln,Pontiac
', '-', 'patrik', '-', '-', 'patrik', 'patrik', 17, 0, 1020);
INSERT INTO stores VALUES (13499, 'Kista Motorcenter AB / Peugeot Kista', 'bertan.camuz_peugeotkista.se@blocket.se', '08 - 50 52 86 23', '08 - 50 52 86 98', 'Haukadalsgatan 3', '164 40', 'KISTA', '13_1_05.gif', '�terf�rs�ljare f�r Peugeot', '<p>
Peugeot �r i dag Sveriges fj�rde st�rsta bilm�rke. V�r anl�ggning i Kista �r Nordens st�rsta. H�r finner du b�de nya person- och transportbilar samt ett flertal begagnade bilar.
<p>
Anl�ggningen i Kista startades upp mars 2004 och �r en av Europas modernaste bil anl�ggningar.
<p>
Hos Peugeot hittar du fler bilar �n hos de flesta av v�ra konkurrenter. Gemensamt f�r alla modeller �r den goda komforten, v�gh�llningen, den moderna designen och en god ekonomi. I v�r utst�llning finner du hela Peugeots modellprogram representerat. 
<p>
Med v�ra gedigna kunskaper om Peugeot och dess modellprogram kan vi ge dig en god v�gledning vid ditt val av bil. 
<p>
Sj�lvfallet erbjuder vi, om du s� �nskar f�rdelaktig billfinansiering samt f�rs�kring genom Peugeot-Finans och Peugeot-F�rs�kring. 
<p>
<b>Kontakt person</b><br>
Bertan Camuz
<p>
�ppettider<br>
m�n-fre : 09.00-19.00<br>
l�r-s�n : 11.00-16.00<br>

 
', 'http://www.peugeotkista.se

', 'active', false, '2005-01-13 00:00:00', '2016-02-01 23:59:59', 'Fiat, Ford, Mercedes, Peugeot, Volvo, Renault,  ', '-', 'patrik', '-', '-', 'patrik', 'patrik', 11, 0, 1020);
INSERT INTO stores VALUES (4037, 'Tomelilla Bil', 'jan.gustafs_spray.se@blocket.se', '0417-122 76', '0417-149 37', 'Norregatan 30', '273 30', 'TOMELILLA', '-', 'K�PER-BYTER-S�LJER', 'DET G�R �VEN BRA ATT RINGA KV�LLSTID OCH HELGER. <br>0417-122 76<br>0708-23 02 29
<p>V�lkommen !

', '-', 'active', false, '2003-05-23 00:00:00', '2016-06-01 23:59:59', 'Begagnade bilar, Ford, Nissan, Mercedes, Volvo', '-', 'patrik', '-', '-', 'patrik', 'patrik', 23, 15, 1020);
INSERT INTO stores VALUES (4038, 'Auto-C', 'info_auto-c.se@blocket.se', '0456-185 54', '0456-185 54', 'Bredgatan 1', '294 43', 'S�LVESBORG', 'autoc_logo.gif', 'Bilaff�ren i centrum', 'Genom en bilpark av importerade och svenska bilar, kan vi h�lla bra priser.<br>
Vi k�per in bilar och �tar oss �ven att s�lja er bil.<br>
Har Ni n�gra �nskem�l om bil, bara kontakta oss.
Auto-C har F-skattebevis. Finansiering vid bilk�p kan ombes�rjas.
<p>
Vi n�s �ven p� tel. 0733-76 33 80
<p>
V�lkommen!', 'http://www.auto-c.se', 'active', false, '2003-05-23 10:59:00', '2016-06-01 23:59:59', 'BMW, Honda, Porsche, Rover, Saab, Volvo', '-', 'patrik', '-', '-', 'patrik', 'patrik', 22, 0, 1020);
INSERT INTO stores VALUES (4049, 'Transport Center AB', 'info_transportcenter.se@blocket.se', '08-35 93 00', '08-35 93 55', 'Djupdalsv�gen 25', '192 51', 'SOLLENTUNA', 'transportce.gif', 'M�rkesoberoende handlare', 'Vi �r ett f�retag i bilbranschen sedan 1982 som har specialiserat oss p� alla typer av l�tta transportbilar s�som sk�p - flak - pickup - bussar och kombi.
<p>
Vi k�per s�ljer byter samt ordnar finansiering s�som leasing eller avbetalning. 
<p>
�ppettider: <br>
M�n-Tor  9-18 <br>
Fre  9-15 <br>
L�r-S�n Enligt �verenskomelse <p>

', 'http://www.transportcenter.se', 'active', false, '2005-05-16 00:00:00', '2016-05-17 23:59:59', 'BMW, Chevrolet, Citro�n, Mercedes, Peugeot, Toyota, Volkswagen, VW ', '-', 'patrik', '-', '-', 'patrik', 'patrik', 11, 0, 1020);
INSERT INTO stores VALUES (4053, 'Bike Port AB', 'mail_bikeport.se@blocket.se', '0650-142 26', '0650-977 04', 'Hamngatan 14', '824 52', 'HUDIKSVALL', 'bikeport.gif', 'Motorcyklar i Glada Hudik', '�ppettider:<br>
<li>M�nd. 15.00 - 20.00 
<li>Tisd. - Fred. 09.30 - 18.00 
<li>L�rd. 11.00 - 14.00 

', 'http://www.bikeport.se', 'active', false, '2003-06-03 13:19:00', '2016-07-01 23:59:59', 'Aprilla, KTM, Suzuki, Yamaha, Honda, Kawasaki, Kymco', '-', 'patrik', '-', '-', 'patrik', 'patrik', 5, 0, 1140);
INSERT INTO stores VALUES (4054, 'Sk�nebil Personbilar AB, Klippan', 'skanebil_skanebil.se@blocket.se', '0435 - 44 88 60', '0435-44 88 55', 'Storgatan 10', '264 35', 'KLIPPAN', 'skanebil.gif', 'Volvo & Renault �terf�rs�ljare', 'I �ngelholm finns person- och lastbilsf�rs�ljning med leveranskontor, person- och lastbilsverkst�der, pl�tverkstad, reservdelslager, grossistf�rs�ljning, butik, Volvia f�rs�kringar, Volvokortet och en administrativ avdelning. 

<p>I Klippan har vi personbilsf�rs�ljning, personbilsverkstad, reservdelslager och butik. 

<p>I B�stad finns en allbilsverkstad och en butik. 

<p>P� alla tre st�llena s�ljs bensin. Som representanter f�r Hertz Biluthyrning finns vi ocks� ute p� �ngelholms Flygplats. I �rkelljunga finns Keland Bil AB, v�r lejdverkstad.
', 'http://www.skanebil.se', 'active', false, '2003-06-03 15:00:00', '2015-12-01 23:59:00', 'Volvo, Renault, Alfa Romeo, Audi, Hyundai, Mazda, Mercedes, Nissan, Opel, Peugeot, Saab, Skoda, Toyota, Volkswagen', '-', 'patrik', '-', '-', 'patrik', 'patrik', 23, 7, 1020);
INSERT INTO stores VALUES (4059, 'Gefle Yachts AB', 'info_gefleyachts.nu@blocket.se', '026-19 59 90', '026-66 95 94', 'Box 2078, Flisk�rsvarvet', '800 02', 'G�VLE', 'gefleyachts.gif', 'Din b�tm�klare', 'Gefle Yachts �r nedre Norrlands enda b�tm�klare och finns p� Norrlands st�rsta marina, Flisk�rsvarvet i G�vle. Vi s�ljer, f�rmedlar, finansierar och f�rs�krar. Kontakter med k�pare finns. Vi har dels egen brygga f�r Dig som vill ha b�ten i vattnet och platser p� land. H�r av dig till oss s� blir du n�jd med din b�taff�r!', 'http://www.gefleyachts.nu', 'active', false, '2003-06-05 14:40:00', '2016-10-30 23:59:59', 'Storebro, Sea Ray, Nimbus, Alma, Flipper, Sunrunner, Bayliner, Nautic, Uttern', '-', 'patrik', '-', '-', 'patrik', 'patrik', 5, 0, 1060);
INSERT INTO stores VALUES (4060, 'First Invest AB', 'plc_dof.se@blocket.se', '0176-20 74 00', '0176-20 74 01', 'Box 85', '760 40', 'V�DD�', '-', 'M�rkesoberoende handlare', 'M�rkesoberoende handlare.
<p>
V�lkomna!', '-', 'active', false, '2003-06-05 00:00:00', '2015-10-31 00:00:00', 'Saab, Toyota, Volkswagen, Volvo, GP', '-', 'patrik', '-', '-', 'patrik', 'patrik', 11, 0, 1020);
INSERT INTO stores VALUES (4065, 'Auto Car Import i Kalmar', 'info_autocarimport.nu@blocket.se', '0480-696 93', '0480-696 93', 'Wismarsv�gen 10', '393 54', 'KALMAR', 'autocarimp.gif', 'S�LJER-BYTER-K�PER-F�RMEDLAR-FINANSIERAR', 'Auto Car Import �r ett v�l etablerat f�retag med snart 5 �r i branschen.
<p> 
Vi har specialiserat oss p� import bilar fr�n Tyskland men en och annan svensks�ld bil har vi ocks�. Audi, BMW, Volvo, Toyota, Chrysler, Jeepar, bussar, Mitsubishi, Opel, Ford och andra bilm�rken, bensin som diesel, stora och sm�.
Vi har serviceverkstad som klarar av n�stan alla bilm�rken.
<p>
Vi tar emot best�llningar ocks�.
<p>
V�RA �PPETTIDER:<br>
M�n-Fre 09.00 - 19.00<br>
L�r-S�n 10.00 - 16.00<br>
<p>
V�lkomna in.', 'http://www.autocarimport.se', 'active', false, '2003-06-10 00:00:00', '2016-03-01 23:00:00', 'Audi, BMW, Chrysler, Honda, Nissan, Opel, Renault, Suzuki, Toyota, Volvo, Volkswagen,', '-', 'patrik', '-', '-', 'patrik', 'patrik', 18, 0, 1020);
INSERT INTO stores VALUES (4064, 'UBBO AB', 'info_ubbo.se@blocket.se', '08-26 00 64', '08-544 989 93', 'Vendev�gen 90', '182 32', 'DANDERYD', 'butik.jpg', 'Fabriksnya bilar via internet till l�gsta pris', 'Ubbo.se �r den ledande akt�ren som s�ljer fabriksnya bilar via internet. Vi
s�ljer ett flertal bilm�rken Alfa Romeo Fiat Ford Hyundai Kia Peugeot och
Toyota. Du f�r din bil levererad direkt fr�n fabrik, generalagent eller
�terf�rs�ljare till l�gsta pris.', 'http://www.ubbo.se', 'active', false, '2003-06-06 00:00:00', '2016-06-15 23:59:59', 'Alfa Romeo, Fiat Multipla, Ford Mondeo, Hyundai Santa Fe, Kia Sorento, Peugeot 307, Toyota Rav 4, Toyota Avensis

', '-', 'patrik', '-', '-', 'patrik', 'patrik', 11, 0, 1020);
INSERT INTO stores VALUES (4066, 'Sk�nsk Bilforum AB', 'si_telia.com@blocket.se', '040-45 15 30', '', 'Falsterbov�gen 51', '236 30', 'H�LLVIKEN', '-', 'M�rkesoberoende handlare', '<ul>
<li>Mobiltel: 0768-67 37 35</li>
</ul>', 'http://www.skanskbilforum.se', 'active', false, '2003-06-10 00:00:00', '2015-12-31 23:00:00', 'Audi, BMW, Chrysler, Ford, Hyundai, Jaguar, Jeep, Land Rover, Mercedes, Nissan, Volkswagen, Volvo', '-', 'patrik', '-', '-', 'patrik', 'patrik', 23, 11, 1020);
INSERT INTO stores VALUES (4069, 'Rehnstr�ms Bil HB', 'info_rehnstromsbil.se@blocket.se', '08-742 33 25', '08-742 12 33', 'Myggdalsv�gen 2 A', '135 43', 'TYRES�', 'rehnstroms.gif', 'Vi handlar med nyare begagnade bilar av h�g kvalitet', 'V�r m�ls�ttning �r att h�lla h�g kvalitet p� bilarna och h�g kundservice. Givetvis l�mnar vi garanti p� samtliga bilar vi s�ljer. 
<p>
Vi tar �ven emot s�ljuppdrag fr�n privatpersoner och f�retag.', 'http://www.rehnstromsbil.se', 'active', false, '2004-09-16 11:00:00', '2016-02-01 23:59:59', 'Audi, BMW, Chevrolet, Ford, Rover, Smart, Saab, Volvo', '-', 'patrik', '-', '-', 'patrik', 'patrik', 11, 0, 1020);
INSERT INTO stores VALUES (14108, 'Halabsons Bil', 'halabsonsbil_swipnet.se@blocket.se', '040-43 04 30', '040-43 04 30', 'Lundav�gen 7', '23235', 'ARL�V', 'butik.gif', 'M�rkesoberoende handlare', 'V�lkommen till Halabsons Bil i Arl�v.<p>

�ppet:<br>
M�ndag-Torsdag 11.00 - 18.00<br>
Fredag St�ngt<br>
L�rdag-S�ndag 11.00 - 15.00<br>', 'http://www.halabsonsbil.se

', 'active', false, '2005-09-20 00:00:00', '2015-12-31 23:59:59', 'Audi, BMW, Ford, Mazda, Opel, Volkswagen, VW, Volvo', '-', 'patrik', '-', '-', 'patrik', 'patrik', 23, 11, 1020);
INSERT INTO stores VALUES (4072, 'Bil 2000', 'bil2000_chello.se@blocket.se', '08-19 55 92', '08-744 35 05', 'H�vdingagatan 39', '126 52', 'H�GERSTEN', 'bil2000.gif', 'K�per - Byter - S�ljer', '�ppettider:<br>
<li>M�n-tor 09.00-18.00
<li>fre 09.00-15.00


', 'http://www.biltvatusen.com', 'active', false, '2005-02-25 00:00:00', '2016-03-01 23:59:59', 'Mercedes, Toyota, Volkswagen, Volvo, Nissan, Renault, ', '-', 'patrik', '-', '-', 'patrik', 'patrik', 11, 0, 1020);
INSERT INTO stores VALUES (4077, 'A-Line AB', 'info_a-line.se@blocket.se', '08-564 606 70', '08-25 03 18', 'Ranhammarv�gen 3', '-', 'BROMMA', '-', 'Handlar med nya och begagnade bilar', '�terf�s�ljare f�r Hyundai.
<p>
�ppettider:
<li>Vardagar: 9.00-18.00
<li>L�r-S�n: 11.00-15.00
<p>
V�lkomna!

', '-', 'active', false, '2003-06-12 11:15:00', '2016-07-01 23:59:59', 'Hyundai, Ford, Fiat, Skoda, Opel, Mitsubishi, Mercedes, Renault', '-', 'patrik', '-', '-', 'patrik', 'patrik', 11, 0, 1020);
INSERT INTO stores VALUES (4078, 'Alviks MC', 'butik_alviks-mc.com@blocket.se', '060-55 70 75', '060-53 80 96', 'Fockv�gen 19', '865 21', 'ALN�', 'alviksmc.gif', 'Allt f�r dig och hojen', 'F�retaget startade 1977 som verkstad, ut�kades med butik 1992. �r sedan 1981 Auktoriserad Suzuki MC Verkstad.
<p>
Alviks MC �r ett f�retag som s�ljer och finansierar k�p av MC, personlig utrustning, tillbeh�r, reservdelar, kort sagt allt som har med hoj�kning att g�ra.
<p>
Vi har en verkstad med ett mycket gott renomm�, d�r vi bland annat utf�r service, reparationer och injusteringar i Dynojetb�nk.
<p>
Kunder under �ren har varit och �r: F�rs�kringsbolag, k�rskolor, polis, v�gverket och naturligtvis alla privata kunder.
<p>
V�rt motto �r Kvalitet, Kunskap och L�ngvariga relationer med b�de kund och leverant�rer.
<p>
Allt f�r dig och hojen hittar du hos oss p� Alviks-MC. Vi finns p� Aln�n, strax utanf�r Sundsvall. V�lkommen! 
', 'http://www.alviks-mc.com', 'active', false, '2003-06-12 14:21:00', '2015-12-31 00:00:00', 'Suzuki, Dynojet', '-', 'patrik', '-', '-', 'patrik', 'patrik', 4, 0, 1140);
INSERT INTO stores VALUES (4079, 'United Cars Of America AB', 'info_unitedcars.se@blocket.se', '08-704 18 75', '08-704 18 70', 'Karlsbodav�gen 15 A', '161 11', 'BROMMA', 'logo.gif', 'Nya och begagnade bilar', 'V�lkommen  till United Cars!
<p>
Vi importerar nya och begagnade bilar fr�mst fr�n USA, Kanada, Tyskland, Belgien och Luxemburg i samarbete med v�lk�nda och utvalda handlare i respektive land.
<p>
Bilarna levereras servade, registreringsbesiktade och helt nyckelf�rdiga till
kunden.
<p>
Garantiuppl�gg sker i samarbete med AutoConcept.
<p>
Ring eller skick ett e-mail om Du vill veta  mer.
', 'http://www.unitedcars.se', 'active', false, '2003-06-13 10:30:00', '2015-11-16 23:59:59', 'Buick, Cadillac, Chevrolet, Chrysler, Dodge, Ford, GMC, Harley Davidson, Jeep, Lincoln, Oldsmobile, Plymouth, Pontiac,', '-', 'patrik', '-', '-', 'patrik', 'patrik', 11, 0, 1020);
INSERT INTO stores VALUES (4081, 'Kryddstigens Bilcentrum', 'nordma_msn.com@blocket.se', '073-694 38 55', '026-14 15 05', 'Kryddstigen 23', '802 92', 'G�VLE', 'kryddis.gif', 'K�per-byter-s�ljer-f�rmedlar bilar', 'Kryddstigens Bilcentrum �r ett litet enmansf�retag som sedan 1996 har
specialiserat sig p� att s�lja kombibilar, pickuper och fyrdjulsdrivna bilar
av diverse m�rken.
<p>
N�r det g�ller kredit s� samarbetar vi med Wasa Kredit. Snabbt, enkelt och
personligt!
<p>
Har Ni n�gra fr�gor eller letar Ni efter n�got speciellt, tveka inte med att
ta kontakt med mig, dag som kv�ll! Har vi inte den bilen som ni �nskar s�
ordnar vi fram den.
<br>
Ni kan �ven n� oss p� 026-14 15 05.
', 'http://
', 'active', false, '2003-06-16 09:40:00', '2016-02-01 23:59:59', 'Audi, Chevrolet, Citroen, Jeep, Mercedes, Mitsubishi, Saab, Seat, Subaru, Suzuki, Toyota, Volvo', '-', 'patrik', '-', '-', 'patrik', 'patrik', 5, 0, 1020);
INSERT INTO stores VALUES (4083, 'Br�derna Odd�s Bilar AB', 'oddsbilar_swipnet.se@blocket.se', '054-56 56 70', '054-56 56 75', 'K�pv�gen 1', '653 46', 'KARLSTAD', 'odds3.gif', 'B�ttre begagnade bilar av v�lk�nda m�rken', 'Br�derna Odd�s Bilar AB �r ett familjef�retag som funnits sedan maj 1993.
<p>
V�r m�ls�ttning �r att alltid ha ett antal nyare fr�scha begagnade bilar i lager. 
<p>
Vi har �ven egen rekonditioneringshall.
<p>
V�lkommen till oss f�r ett tryggt bilk�p.', 'http://www.oddsbilar.se', 'active', false, '2003-06-16 11:56:00', '2015-12-31 23:59:00', 'Audi, BMW, Saab, Volkswagen, Volvo', '-', 'patrik', '-', '-', 'patrik', 'patrik', 7, 0, 1020);
INSERT INTO stores VALUES (4088, 'MC Mont�rerna AB', 'mcmont_telia.com@blocket.se', '011-18 18 77', '011-16 04 53', 'Malmgatan 4', '602 23', 'NORRK�PING', 'mcmontor_logo.gif', 'Allt f�r din MC', 'MC Mont�rerna grundades 1980 och har fram till �r 2000 enbart varit verkstads och service-inriktat.
I verkstaden �r vi idag auktoriserade f�r YAMAHA och HONDA.
<p>
�r 2000 fick vi m�jligheten att b�rja med f�rs�ljning av YAMAHA�s motorcyklar och strax d�refter blev vi utn�mnda av YAMAHA till "�rets Komet 2000".
<p>
Idag hj�lper vi Dig med allt ifr�n finansiering och  f�rs�ljning av motorcyklar, tillbeh�r och reservdelar, till service och reparation av b�de landsv�gs- offroad- och fyrhjuls- motorcyklar.', 'http://www.mcmontorerna.se', 'active', false, '2003-06-17 14:09:00', '2015-11-06 00:00:00', 'Honda, Kawasaki, Suzuki, Yamaha', '-', 'patrik', '-', '-', 'patrik', 'patrik', 14, 0, 1140);
INSERT INTO stores VALUES (4103, 'Bengt i �rkelljunga AB', 'info_bengtiorkelljunga.se@blocket.se', '0435-513 70', '0435-544 70', 'Hallandsv�gen 47', '286 21', '�RKELLJUNGA', 'bengt.gif', 'Alltid minst ett 100-tal beg husvagnar & husbilar i lager', 'Vi s�ljer fabriksnya husbilar av m�rkena:<br>
CI, RollerTeam, Knaus, Weinsberg samt fabriksnya husvagnar av m�rkena Knaus och Eifelland.
<p>
�ppettider:
<br>M�ndag - Torsdag 10-17.30
<br>Fredag 10-16
<br>Lunchst�ngt 12-12.45
<br>S�ndags�ppet kl. 12 - 16
<p>
<li>Auktoriserad HRF-�terf�rs�ljare
<li>35 �r i branschen!
  
', 'http://www.bengtiorkelljunga.se', 'active', false, '2003-06-17 15:45:00', '2015-11-30 23:59:59', 'CI, RollerTeam, Knaus, Weinsberg, Knaus, Eifelland', '-', 'patrik', '-', '-', 'patrik', 'patrik', 23, 16, 1100);
INSERT INTO stores VALUES (4107, 'Motorhuset i �stersund AB', 'info_motorhuset.com@blocket.se', '063-57 00 20', '063-57 00 21', 'Odenskogsv. 35', '83148', '�STERSUND', 'motorhuset.gif', 'L�nets marknadsledande handlare.', '�terf�rs�ljare f�r Polaris, Ski-Doo, Honda, Peugeot, Beta och Hymco.
<p>
Stor sortering av kl�der och reservdelar.
<p>
Fullserviceverkstad i samarbete med Motorteknik.
<p>
�ppet M�n-Fre 8-17
<p>
V�lkommen till oss f�r din n�sta och b�sta aff�r.', 'http://www.motorhuset.com', 'active', false, '2005-09-28 00:00:00', '2016-10-01 23:59:59', 'Polaris, Ski-Doo, Honda, Peugeot, Beta, Hymco', '-', 'patrik', '-', '-', 'patrik', 'patrik', 3, 0, 1180);
INSERT INTO stores VALUES (4110, 'Nestors Bil AB', 'nestors_telia.com@blocket.se', '08 - 511 808 05', '08 - 511 797 19', 'Galgbacksv�gen 10', '186 30', 'VALLENTUNA', '-', 'M�rkesoberoende handlare', '�ppettider:<br>
M�n-Tor 10-18<br>
Fredag 10-16<br>
Lunchst�ngt 12-13<br>
L�rdag 11-14<br>
V�lkomna!

', 'http://www.nestorsbil.se
', 'active', false, '2005-03-28 00:00:00', '2016-04-01 23:59:59', 'Audi, BMW, Chevrolet, Chrysler, Citro�n, Ford, Mazda, Mercedes, Mitsubishi, Nissan, Opel, Saab, ', '-', 'patrik', '-', '-', 'patrik', 'patrik', 11, 0, 1020);
INSERT INTO stores VALUES (4111, 'Solvalla Biltorg', 'info_solvallabiltorg.se@blocket.se', '08-28 01 40', '08-445 07 67', 'Sundbybergskopplet', '172 37', 'SUNDBYBERG', 'solvalla.gif', 'B�sta bilen till b�sta priset!', 'Hitta din dr�mbil hos oss.
<p>
<li>Vi lagerf�r bilar fr�n 5000 :- och upp�t.
<li>B�sta bilen till b�sta priset!
<li>Vi kan ordna b�de Leasing och Avbetalning.
<p>
V�lkommen in till oss!', 'http://www.solvallabiltorg.se', 'active', false, '2003-06-19 11:00:00', '2016-10-21 23:59:59', 'Alfa Romeo, Audi, BMW, Chevrolet, Ford, Mercedes, Saab, Volkswagen, Volvo', '-', 'patrik', '-', '-', 'patrik', 'patrik', 11, 0, 1020);
INSERT INTO stores VALUES (4116, 'Nordlunds Motor KB', 'info_nordlundsmotor.se@blocket.se', '0910-72 40 00', '0910-72 44 49', 'J�rnsv�gen 56', '934 93', 'KUSMARK', 'nordlunds_logo.gif', '�terf�rs�ljare f�r Cabby husvagnar & Home Car husvagnar o husbilar.', 'Cabby & Home Car �r v�ra m�rken som vi saluf�r med stolthet.
<p>
Hos oss finns vagnen f�r dig, b�de nytt och begagnat.
<p>
N�r det g�ller tillbeh�r s� �r det KAMA, Fiamma och Isabellas sortiment vi s�ljer.
<p>
I v�r fullservice-verkstad s� utf�r vi bl.a broms-, gasol- och fukttest.
<br>
Vi utf�r �ven service och reparation av alla f�rekommande husvagnar & husbilar.
<p>
V�lkomna till oss med trygghet f�r din n�sta aff�r.', 'http://www.nordlundsmotor.se', 'active', false, '2003-06-23 10:42:00', '2016-12-31 23:59:59', 'Cabby', '-', 'patrik', '-', '-', 'patrik', 'patrik', 2, 0, 1100);
INSERT INTO stores VALUES (4118, 'Hojgaraget i Karlstad AB', 'per_hojgaraget.com@blocket.se', '070-55 33 120', '070-55 33 023', 'Biv�gen 8', '663 41', 'HAMMAR�', '2_2_05.gif', 'MC till konkurrenskraftiga priser', 'Id�n med Hojgaraget �r att kunna erbjuda dig som kund ett billigt s�tt att f�rverkliga din dr�m om bli motorcykel�gare. Eftersom vi importerar maskinerna fr�n Tyskland och inte har s� m�nga anst�llda s� kan vi h�lla priserna p� en mycket konkurrenskraftig niv�. 
', 'http://www.hojgaraget.com', 'active', false, '2003-06-23 15:32:00', '2016-12-31 23:59:59', 'Honda, Kawasaki, Suzuki, Yamaha', '-', 'patrik', '-', '-', 'patrik', 'patrik', 7, 0, 1140);
INSERT INTO stores VALUES (4119, 'L�nna Handelshus i Sverige AB', 'tommie.edstrand_lhandelshus.se@blocket.se', '08-630 08 81', '08-630 08 82', 'M�ttbandsv�gen 3 A', '187 11', 'T�BY', 'lannahandels.gif', 'Fordon & maskiner', 'L�nna Handelshus AB �r ett f�retag som specialicerat sig p� kommissionsf�rs�ljning av fordon och maskiner �t f�retag.
LH etablerades 1990 i L�nna Industriomr�de. Sedan 1993 finns vi i Arninge industriomr�de.
', 'http://www.lhandelshus.se', 'active', false, '2003-06-24 10:30:00', '2015-10-31 00:00:00', 'Audi, BMW, Chevrolet, Chrysler, Ford, Mitsubishi, Nissan, Opel, Peugeot, Renault, Saab, Suzuki, Toyota, Volkswagen, VW, Volvo,', '-', 'patrik', '-', '-', 'patrik', 'patrik', 11, 0, 1020);
INSERT INTO stores VALUES (4120, 'Nilssons Bilhall AB', 'info_nilssonsbilhall.se@blocket.se', '0492-797 92', '0492-153 66', 'Lundgatan 5', '598 40', 'VIMMERBY', '-', '�terf�rs�ljare f�r BMW & Peugeot.', 'Nilssons Bilhall AB �r ett familjef�retag med �ver 30 �r i branschen.
BMW och Peugeot �r de nybilsm�rken som Nilssons Bilhall representerar, och serviceverkstad till dessa bilm�rken finns i samma hus som f�rs�ljningslokalerna.
Begagnatlagret best�r alltid av c:a 50 - 70 bilar.
', 'http://www.nilssonsbilhall.se', 'active', false, '2003-06-24 15:00:00', '2016-09-15 23:59:59', 'BMW, Rover, Peugeot, Honda', '-', 'patrik', '-', '-', 'patrik', 'patrik', 18, 0, 1020);
INSERT INTO stores VALUES (12799, 'Bilmix AB', 'bilmix_telia.com@blocket.se', '0910-151 55', '0910-7799 67', 'Tj�rnv�gen 7', '931 61', 'SKELLEFTE�', '-', 'M�rkesoberoende handlare', 'Bilmix har funnits sedan 1994.
Om du s�ker en bil med l�g prislapp ska du titta lite extra p� v�rt utbud.
Vi �r specialiserade p� bilar i prisklassen  20-100.000 kr. Speciellt handplockade med f�- �gare, lite mil och svensks�lda. Genom goda ink�pskanaler, 
liten organisation och l�ga omkostnader kan vi h�lla n�st intill oslagbara priser.
Vi �r inte m�rkesbundna heller, vilket inneb�r ett brett utbud.
<p>
Vi �r �ven intresserade av att k�pa in motsvarande bilar.
<p>
Vi erbjuder bra finansiering i samarbete med Handelsbanken Finans.
<p>
Vi hyr lokalen tillsammans med WM Bil AB och vi samordnar �ven bilf�rs�ljningen.
Sammanlagt har vi cirka 50 bilar i samtliga prisniv�er.
<p>
V�lkommen till oss f�r en trygg och bra bilaff�r.', '-', 'active', false, '2004-05-20 10:50:00', '2016-06-01 23:59:59', 'Audi, BMW, Ford, Nissan, Opel, Saab, Toyota, Volvo,', '-', 'patrik', '-', '-', 'patrik', 'patrik', 2, 0, 1020);
INSERT INTO stores VALUES (4124, 'M-Bilar Stockholm AB', 'info_m-bilar.se@blocket.se', '08-754 35 20', '08-754 35 21', 'Konsumentv 2', '192 68', 'SOLLENTUNA', 'mbilar.gif', 'M�rkesoberoende handlare', '�ppettider:<br>
Vardagar: 11-18', 'http://www.m-bilar.se', 'active', false, '2004-07-12 14:45:00', '2016-09-01 23:59:59', 'BMW, Opel, Volkswagen, Volvo', '-', 'patrik', '-', '-', 'patrik', 'patrik', 11, 0, 1020);
INSERT INTO stores VALUES (14130, 'Medelhults Trading', 'jansson_medelhult.nu@blocket.se', '0472-770 95', '0472-770 95', 'Medelhult 1', '340 36', 'MOHEDA', 'butik.jpg', 'M�rkesoberoende handlare', 'Vi s�ljer mest nyare Volvo, men �ven andra m�rken till bra priser. <br>
Vi utf�r ocks� service, reparationer och rekonditioneringsjobb.<p>
Vi finns p� en lantg�rd i Sm�land 10 mil s�der om J�nk�ping mellan V�rnamo och V�xj�. <p>

<b>Garantier och finansiering</b><br>
P� de flesta bilar l�mnar vi tre m�naders garanti. Vi l�mnar varudeklaration p� alla bilar �ver 15 000:- <br>
Beh�ver du finansiering av bilk�pet kan vi ordna detta p� n�stan alla bilar (till exempel genom F�reningssparbanken).<p>

Ring oss alltid f�re bes�k s� vi s�kert �r hemma.<br>
V�lkomna �nskar familjen Jansson! 
', 'http://www.medelhult.nu

', 'active', false, '2005-09-22 00:00:00', '2015-12-31 23:59:59', 'Renault, Volkswagen, VW, Volvo', '-', 'patrik', '-', '-', 'patrik', 'patrik', 21, 0, 1020);
INSERT INTO stores VALUES (4131, 'Kareby Bil', 'kareby.bil_swipnet.se@blocket.se', '0303-22 20 01', '0303-22 25 61', 'Pr�stv�gen 105', '442 93', 'KAREBY', 'kareby.GIF', 'Auktoriserad �terf�rs�ljare f�r KIA', 'Vi �r en auktoriserad �terf�rs�ljare f�r KIA, och vi s�ljer och k�per nya och beg. bilar
Kareby Bil ligger 2 km norr om Kung�lv i det lilla 
samh�llet Kareby.
<p>

Funderar du p� att k�pa en ny eller begagnad bil eller s�lja din gamla? Tveka inte, ring oss! 
Vi har finansieringsm�jligheter med Handelsbanken, GE Bilfinans eller SEB, med 20 % som l�gsta kontantinsats. 
<p>

Ta en dag och kom och provk�r v�ra nya KIA bilar eller 
n�gon begagnad s� kanske du hittar din framtida bil.
', 'http://www.karebybil.se', 'active', false, '2003-08-12 10:58:00', '2016-04-11 23:59:59', 'KIA, Volvo, SAAB', '-', 'patrik', '-', '-', 'patrik', 'patrik', 15, 0, 1020);
INSERT INTO stores VALUES (13192, 'Bogstedt Bil AB', 'info_bogstedtbil.se@blocket.se', '0703 - 40 59 30', '-', 'Bergdalsgatan 3', '504 58', 'BOR�S', 'bogstedt_logo.gif', 'M�rkesoberoende handlare', 'Vi s�ljer och f�rmedlar bilar av h�g kvalitet till r�tt pris samt ej momsbelagda bilar av m�rkena Audi och Volkswagen.
<p>
Samtliga bilar till f�rs�ljning genomg�r en komplett MR Cap behandling, b�de
lack och interi�r, v�rde 5-6000:-.
<p>
Vi l�ser �ven din finansiering eller varf�r inte modifiering av bilen.
<p>
V�lkommen att h�ra av dig om du �r intresserad av n�gon av v�ra bilar eller om du har ett objekt som du vill att vi f�rmedlar.', 'http://www.bogstedtbil.se
', 'active', false, '2004-10-08 14:30:00', '2016-04-15 23:59:59', 'Audi, BMW, Volkswagen, VW,', '-', 'patrik', '-', '-', 'patrik', 'patrik', 16, 0, 1020);
INSERT INTO stores VALUES (4133, 'Verna Bil, �rebro', 'vernabil_vernabil.com@blocket.se', '019-26 00 67', '019-26 00 67', 'Idrottsv�gen 31 B', '703 49', '�REBRO', 'verna.GIF', 'M�rkesoberoende handlare', 'Vernabil �r ett f�retag som grundades 1997 och som importerar bilar fr�n Tyskland.', 'http://www.vernabil.com', 'active', false, '2003-08-12 00:00:00', '2016-02-01 23:59:59', 'Audi, Ford, Honda, Opel, Mazda, Mercedes, Nissan, Volvo', '-', 'patrik', '-', '-', 'patrik', 'patrik', 8, 0, 1020);
INSERT INTO stores VALUES (4134, 'Lidk�pings Biltj�nst AB', 'lidkopingsbiltjanst_telia.com@blocket.se', '0510-222 66', '0510-222 02', 'Gamla K�llandsgatan 48', '531 30', 'LIDK�PING', 'lidbil.GIF', 'F�retaget f�r Dig som vill k�pa eller s�lja Din bil.', 'Vi har ett brett utbud av begagnade bilar i olika prisklasser.<p>
<b>Fler bilder p� alla bilar finns p� v�r</b> <A HREF=" http://www.lidkopingsbiltjanst.com" TARGET="_blank"><b><u>
hemsida</u></b></a>

<p>
Kontakta oss g�rna p� v�ra direktnummer;<br>
Peter: 070-342 32 03<br>
Mikael: 070-342 32 30<br>
Fredrik: 070-342 33 20<br>

', 'http://www.lidkopingsbiltjanst.com
', 'active', false, '2003-08-15 15:00:00', '2016-08-15 23:59:59', 'Audi, BMW, Chrysler, Ford, Mercedes, Nissan, Saab, Subaru, Toyota, Volkswagen, Volvo', '-', 'patrik', '-', '-', 'patrik', 'patrik', 13, 0, 1020);
INSERT INTO stores VALUES (4136, 'Yvonne Nordanstig Bil AB', 'info_yvonnenordanstigbil.se@blocket.se', '054-85 04 10', '054-85 08 95', 'Gjuterigatan 1', '652 21', 'KARLSTAD', '22_12_04.gif', '�terf�rs�ljare f�r Alfa Romeo, Kia', '<p>
Bes�k oss g�rna f�r en provtur i en ny bil fr�n FIAT eller Alfa Romeo.
<p>
Givetvis finns det �ven begagnade alternativ!
<p>
Stort utbud av transportbilar.', 'http://www.yvonnenordanstigbil.se', 'active', false, '2003-08-18 10:00:00', '2015-12-22 23:59:59', 'Fiat, Alfa Romeo, Saab, Opel, Toyota, ', '-', 'patrik', '-', '-', 'patrik', 'patrik', 7, 0, 1020);
INSERT INTO stores VALUES (4142, 'Biltec AB', 'info_biltec.com@blocket.se', '0470 - 400 65', '-', 'Linn�gatan 20', '35105', 'V�XJ�', 'biltec.GIF', 'SAAB-specialist', '1994 startade J�rgen Lundgren f�retaget som �r specialist inom bilm�rket SAAB. Vi har 20 �rs erfarenhet av SAAB-reparationer och bilf�rs�ljning. Vi har utbildat oss genom �tskilliga kurser p� SAAB-skolan I Nyk�ping. Vi har �ven deltagit i tv� riksfinaler som t�vlat om "Sveriges b�sta mekaniker" genom avancemang av lokala t�vlingar samt distriksfinal. 
Inriktningen �r att serva och reparera bilar fr�mst av m�rket SAAB samt bilf�rs�ljning av samma m�rke.
Tack vare stor kompetens och klar m�linriktning har f�retaget snabbt vuxit och �r nu v�l inarbetat. D� bilarna �r mycket v�l genomg�ngna b�de efter en service och innan en f�rs�ljning har Biltec m�nga n�jda kunder.', 'http://www.biltec.se', 'active', false, '2003-08-21 00:00:00', '2016-09-01 23:59:59', 'SAAB', '-', 'patrik', '-', '-', 'patrik', 'patrik', 21, 0, 1020);
INSERT INTO stores VALUES (14191, 'Bilpunkten AB', 'kw_bilpunktenboras.se@blocket.se', '033-24 83 88', '033-23 00 95', 'Riks 40 Br�mhult', '507 11', 'BOR�S', 'bbutik.gif', '�terf�rs�ljare f�r Chrysler, Jeep', 'V�lkommen till BilPunkten i Bor�s/WiWe:s Bilf�rs�ljning.<br>
Din kompletta Chrysler,Jeep samt Mazda o Suzuki-�terf�rs�ljare med f�rs�ljning, verkstad/reservdelar samt biluthyrning. <p>
Alltid bra priser f�r v�ra internetkunder!<p>
�ppettider:<br> 
M�n-Fre 09:00-18:00 <br>
L�rd 10:00-14:00 <br>
Sommartid L�rd St�ngt <p>

V�lkommen!', 'http://www.bilpunktenboras.se
', 'active', false, '2005-09-30 00:00:00', '2015-12-31 23:59:59', 'Chrysler, Jeep, Audi, Voyager, Jeep Cherokee, Mazda, Mercedes-Benz, Mitsubishi, Nissan, Opel, Suzuki, Volvo', '-', 'patrik', '-', '-', 'patrik', 'patrik', 16, 0, 1020);
INSERT INTO stores VALUES (4141, 'Karlstr�ms Motor', 'tommy_karlstrommotor.se@blocket.se', '0224 - 771 66', '0224 - 771 28', 'Fabriksgatan 5', '733 21', 'SALA', '10_2_05.gif', '�terf�rs�ljare f�r Honda, Husaberg, Jincheng, KTM, LEM, Peugeot', 'Nu satsar jag 100 % p� att f� min firma att bli en av de ledande i Cross o Enduro branschen i mellan Sverige. F�r att komma dit �r det mycket jobb men jag �r p� god v�g f�r det �r verkligen fullt med delar och tillbeh�r i butiken. Nu har jag verkligen snabb leverans p� order (samma dag ) eftersom lagret �r s� stort och d� menar jag stort. Och min satsning har gett bra resultat eftersom jag s�ljer m�nga fler MC runt om i landet �n tidigare. P� Husaberg tex. �r firman stor och har st�rst reserv-delslager i Sverige och det g�ller �ven KTM. 
Men det som har �kat mest �r f�rs�ljningen p� KTM som har �kat flera 100 procent. F�r att det ska g� fort och s�kert med order har jag nu hela lagret p� data, allt f�r snabb orderhantering. 
Testa oss .... 

', 'www.karlstrommotor.se
', 'active', false, '2003-08-19 14:45:00', '2016-03-01 23:59:59', 'Honda, Husqvarna, KTM, Kawasaki, Suzuki, Yamaha', '-', 'patrik', '-', '-', 'patrik', 'patrik', 9, 0, 1140);
INSERT INTO stores VALUES (5320, 'Carl Larsson & S�ner AB', 'order_cls.se@blocket.se', '031-49 63 20', '031-47 55 35', 'Kontrabasgatan 10', '421 50', 'V�STRA FR�LUNDA', 'carlarsson.gif', 'Vi s�ljer kvalitetsdelar b�de till verkst�der och g�r-det-sj�lvare', 'Carl Larsson & S�ner AB s�ljer bildelar och har det mesta som slits p� bilen.  H�r kan du hitta allt fr�n plyscht�rningar till kompletta avgassystem och till och med kall l�sk! P� sommaren kan det vara gott med en glass. 
<p>
Vi s�ljer kvalitetsdelar b�de till verkst�der och g�r-det-sj�lvare.
<p>
Vi har �ven en motorrenovering som fixar din motor.
<p>
�ppettider:  
<br>m�ndag-fredag 7.30-16.30', 'http://www.autokatalogen.se/cls', 'active', false, '2003-10-16 00:00:00', '2016-10-21 23:59:59', 'Bildelar, Reservdelar, Motorrenovering, Motordelar, Framvagnsdelar, Bilel, Bromsar, Olja, Filter, Bilv�rd, St�td�mpare, Pl�t, Generator, Startmotor', '-', 'patrik', '-', '-', 'patrik', 'patrik', 15, 0, 1040);
INSERT INTO stores VALUES (5519, 'Resten�s Bil & Bildelar AB', 'info_restenasbildelar.se@blocket.se', '0522-223 95', '0522-223 96', 'Resten�s 165', '459 93', 'LJUNGSKILE', 'reste.gif', 'Bildelar & Tillbeh�r i Ljungskile', 'Resten�s Bil & Bildelar AB ligger i Ljungskile, strax s�der om Uddevalla.
<p>
Vi har funnits sedan 1998 och lagerh�ller ett brett sortiment av b�de nya och begagnade reservdelar till framf�rallt Volvo. D�rut�ver finns ett mindre sortiment begagnade delar till opel och golf. F�rutom f�rs�ljning av bildelar utf�r vi reparationer och service p� de flesta bilm�rken.
<p>
P� startsidan kan du s�ka efter de nya volvodelar vi tillhandah�ller. Om du inte hittar det du s�ker eller om du s�ker begagnade delar kan du kontakta oss via telefon. Du �r naturligtvis ocks� v�lkommen att kontakta oss via e-post eller att bes�ka oss.
Ej lagerf�rda reservdelar tar vi hem p� best�llning.
<p>
Skickar mot postf�rskott.
<p>
F�r snabbast info, ring oss<br>
M�ndag - Fredag kl. 8.00 - 18.00<br>
L�rdag kl. 10.00 - 14.00', 'http://www.restenasbildelar.se
', 'active', false, '2003-09-19 00:00:00', '2015-11-20 00:00:00', 'Bildelar, Volvo, Opel, Reservdelar, Begagnade bildelar, Nya bildelar, Reparationer, Service, Golf,
', '-', 'patrik', '-', '-', 'patrik', 'patrik', 15, 0, 1040);
INSERT INTO stores VALUES (6326, 'Svenska Bildelsimporten AB', 'lars.lindblad_bildelsimporten.se@blocket.se', '018-50 80 80', '018-50 80 82', 'Fyrisvallsgatan 26', '750 15', 'UPPSALA', 'bildelsimp.gif', 'Bildelar BMW, Mercedes & Audi', 'Vi t�nker p� kvalitet i f�rsta hand! 
Mottot �r "En bra bil ska ha bra delar" Med dom orden st�ller vi en hel del krav p� oss sj�lva, d�rf�r importerar vi t ex delar av v�lk�nda fabrikat.
<p>
Vi etablerar hela tiden nya kontakter med bra leverant�rer �ver hela v�rlden.', 'http://www.bildelsimporten.se', 'active', false, '2003-09-17 00:00:00', '2015-12-31 23:59:00', 'BMW, Mercedes, Audi, bildelar', '-', 'patrik', '-', '-', 'patrik', 'patrik', 10, 0, 1040);
INSERT INTO stores VALUES (14085, 'Nyboms Bil & Motor', 'info_nyboms-bil.se@blocket.se', '0278-65 07 50', '0278-65 08 59', 'Riksv�g 35', '82321', 'KILAFORS', 'butik_logga.gif', '�terf�rs�ljare f�r Citro�n, Subaru', '<b>V�lkommen till Nyboms Bil & Motor!</b><p>

I tre generationer har Nyboms bil & motor tillh�rt samma familj, vilket �r unikt i Sverige. Detta borgar f�r att vi har den erfarenhet, kunskap och service som �r ett m�ste f�r att du som kund ska k�nna trygghet i ditt bilk�p. <p>

V�r policy �r att som ett av Sveriges �ldsta bilf�retag bibeh�lla den goda tradition att alltid arbeta f�r v�ra kunders b�sta. Vi ska vara lyh�rda och alltid str�va efter att uppfylla deras krav och �nskem�l. <br>
Genom att hela tiden f�rb�ttra v�rt kvalitetsledningssystem skapar vi goda f�ruts�ttningar att lyckas i v�r str�van.
<p>

Vi �r en komplett fullserviceanl�ggning d�r du kan f� hj�lp med allt som r�r personbilar och l�tta lastfordon.<p>


', 'http://www.nyboms-bil.se

', 'active', false, '2005-09-19 00:00:00', '2015-12-31 23:59:59', 'Citro�n, Subaru, Peugeot', '-', 'patrik', '-', '-', 'patrik', 'patrik', 5, 0, 1020);
INSERT INTO stores VALUES (14086, 'Vallentuna Motor AB', 'receptionen_vallentunamotor.se@blocket.se', '08-514 308 00', '08-514 308 01', 'Industriv�gen 2', '18636', 'VALLENTUNA', 'butik_logga.gif', '�terf�rs�ljare f�r Opel, Saab', '<b>V�lkommen till Vallentuna Motor AB </b><p>
H�r kan Du i lugn och ro titta n�rmare p� modellerna fr�n Saab, Opel och Chevrolet. 
<p>
Du kan �ven s�ka bland v�ra begagnade bilar, l�sa om alla kringtj�nster f�r Dig som k�r v�ra m�rken, se vilka erbjudanden vi har f�r tillf�llet samt hur Du enklast kommer i kontakt med v�r personal.<p>

V�r m�ls�ttning �r att genom h�gklassiga produkter, engagerad personal och goda kundrelationer skapa en entusiasm f�r v�ra m�rken. <p>
Vi vill hela tiden s�tta Dig som kund i fokus f�r att �vertr�ffa Dina f�rv�ntningar. 
<p>

<b>�ppettider</b><br>
Service/Verkstad<br>
M�n-Fre: 07.00-16.00 <br>
L�rdagar: St�ngt <br>
S�ndagar: St�ngt <br>
<p>
 
F�rs�ljning <br>
M�n-Fre: 09.00-18.00 <br>
L�rdagar: 11.00-16.00 <br>
S�ndagar: 11.00-16.00 <p>

Butiken<br>
M�n-Fre: 07.00-17.00 <br>
L�rdagar: St�ngt <br>
S�ndagar: St�ngt <br>
<p>
V�lkommen till v�rt f�retag<br>
Lars Noreen
', 'http://www.vallentunamotor.se

', 'active', false, '2005-09-19 00:00:00', '2015-12-31 23:59:59', 'Audi, Ford, Nissan, Opel, Renault, Saab, ', '-', 'patrik', '-', '-', 'patrik', 'patrik', 11, 0, 1020);
INSERT INTO stores VALUES (6893, 'S�dermalms Bil- & Taxiservice AB', 'sodermalms.bil_swipnet.se@blocket.se', '08-694 70 10', '08-702 08 75', 'Tullg�rdsgatan 12', '116 68', 'STOCKHOLM', 'sodermalms.gif', 'Original reservdelar och service', 'FULLSERVICEVERKSTAD
<p>
<li> Servar och reparerar personbilar
<li> Byter Glasrutor
<li> Servar och fyller AC-anl�ggningar
<li> Reparerar krock- st�ld- och brandskada
<li> L�nebilar och hyrbilar
<li> S�ljer och monterar d�ck
<p>
S�dermalms Bilservice startade sin verksamhet i BP Ringv�gens gamla lokaler 1994, d� i huvudsak som taxiverkstad. 1996 flyttade bolaget till Hammarby f�r att i september 1999 flytta tillbaka till s�der och sin nuvarande adress p� Tullg�rdsgatan.
<p>
Vi har ett bra samarbete med Bilia, n�got som st�rkts p� senare �r.
<p>
Kia har vi arbetat med sedan 1997.', 'http://www.sodermalms-bilservice.se
', 'active', false, '2003-11-03 00:00:00', '2015-12-31 00:00:00', 'Volvo, Kia', '-', 'patrik', '-', '-', 'patrik', 'patrik', 11, 0, 1040);
INSERT INTO stores VALUES (6894, 'Lagerstedts Bilservice AB', 'reservdelar.lagerstedt_swipnet.se@blocket.se', '08-714 54 65', '08-702 08 75', 'Tullg�rdsgatan 12', '116 68', 'STOCKHOLM', 'lagerstedts.gif', 'V�nd dig med f�rtroende till oss n�r det g�ller din Citro�n', 'V�lkommen till Lagerstedts Bilservice. V�ra lokaler finner du p� S�dermalm i Stockholm.
<br>
<br>
 Bilservice har vi arbetat med sedan 1983 och har alltsedan starten arbetat f�r att ha h�gsta kompetens och ta fullt ansvar f�r utf�rt arbete.
<br>
<br>
 Vi anv�nder den senaste tekniken och kvalitetss�kringssystemet ISO 9002 f�r att kunna garantera kvalitet.
<br>
<br>
 Att vi �r en MRF verkstad �r en garanti f�r att vi h�ller vad vi lovar. Vi anlitas �ven flitigt av f�rs�kringsbolag s�som Skandia, Trygg Hansa och Dial.
', 'http://www.lagerstedts-bilservice.se', 'active', false, '2003-11-03 00:00:00', '2015-12-31 00:00:00', 'Citroen, Citro�n', '-', 'patrik', '-', '-', 'patrik', 'patrik', 11, 0, 1040);
INSERT INTO stores VALUES (9163, 'D�ck-Vulkis AB', 'dvracing_spray.se@blocket.se', '013-12 30 55', '013-14 50 70', 'Sunnorpsgatan 12', '582 73', 'LINK�PING', '-', 'F�rs�ljning av d�ck till personbilar, lastbilar, bussar, traktorer och MC', '�ppettider: <br>
M�n - Fre 07.00-18.00<br>
L�rdagar 09.00-13.00, under h�gs�song
<p>
D�ck-Vulkis startades redan 1965, nuvarande �gare tog �ver f�retaget 1988.
<p>
F�rs�ljning av d�ck till personbilar, lastbilar, bussar, traktorer och MC.

<p>
Vi utf�r �ven bilreparationer.
', '-', 'active', false, '2003-10-14 00:00:00', '2015-11-15 00:00:00', 'd�ck, sommard�ck, vinterd�ck, f�lgar, aluf�lgar, aluminiumf�lgar, LMf�lgar,
Michellin, Bridgestone, Firestone, Nokian, AGI, Dunlop
', '-', 'patrik', '-', '-', 'patrik', 'patrik', 14, 0, 1040);
INSERT INTO stores VALUES (9170, 'Pelles D�ck & Bensin AB', 'lars.klefborg_spray.se@blocket.se', '013-12 14 07', '013-13 30 33', 'Gottorpsgatan 41', '582 73', 'LINK�PING', 'logga.gif', 'Kvalitet till l�gt pris!', '<p>
I v�r butik har vi ett v�l uppdaterat sortiment av f�lgar och d�ck.
H�r kan du titta och k�nna i lugn och ro, samtidigt som du f�r r�d och tips
fr�n oss.<br>
Vi har alltid det senaste - anpassat f�r alla pl�nb�cker - till sportbilen,
bruksbilen eller morfars gamla dieselmerca fr�n -69.
<p>
Vi utf�r hjulinst�llningar och har dessutom ett stort sortiment av
stylingf�lgar.
<p>
V�lkommen att bes�ka oss!
', 'http://www.pellesdack.com/', 'active', false, '2004-01-29 09:50:00', '2016-04-01 23:59:59', 'd�ck, vinterd�ck, sommard�ck, f�lgar, aluf�lgar, alu-f�lgar, alu f�lgar, mc-d�ck, styling, stylingf�lgar,Michelin, Continental, OCL Brorssons, Hankook,
Pirelli, Yokohama
', '-', 'patrik', '-', '-', 'patrik', 'patrik', 14, 0, 1040);
INSERT INTO stores VALUES (9177, 'Ahlbergs Gummiverkstad AB/DEKKPartner', 'info_ahlbergsgummi.se@blocket.se', '013-14 89 07', '013-14 46 14', 'V�ringagatan 5', '582 78', 'LINK�PING', 'ahlbergs2.gif', 'Vi s�ljer och arbetar endast med v�lk�nda produkter med h�g kvalitet', '�ppettider: M�n-Fre 07.00-17.00
<p>
Ahlbergs Gummiverkstad AB �r med mer �n 45 �rs verksamhet inom d�ckbranschen ett v�lrenommerat och stabilt f�retag. Med v�r l�nga erfarenhet och stora kunskapsniv� kan vi utf�ra d�ckservice till de flesta fordon. Allt fr�n skottk�rror till entreprenadmaskiner. Vi �r sedan m�nga �r auktoriserade av D�ckspecialisternas Riksf�rbund. Personalen vidareutbildas kontinuerligt f�r att st�ndigt vara insatta om alla f�r�ndringar och nyheter som kommer. Maskinparken �r modern f�r att motsvara dagens krav. Vi s�ljer och arbetar endast med v�lk�nda produkter med h�g kvalitet, detta ger s�kerhet, trygghet, ekonomi och minsta m�jliga milj�p�verkan. Allt detta till r�tt pris.
', 'http://www.ahlbergsgummi.se', 'active', false, '2003-10-09 00:00:00', '2016-04-21 23:59:59', 'Michelin, Continental, B.F. Goodrich, Uniroyal, Barum, Svenska F�lg, Specialf�lgar, OCL, Abozzo, d�ck, f�lgar, gummiverkstad, l�ttmetall f�lgar, hjulinst�llning, transportband
', '-', 'patrik', '-', '-', 'patrik', 'patrik', 14, 0, 1040);
INSERT INTO stores VALUES (9206, 'S�derbergs Gummiverkstad AB', 'info_soderbergsgummi.se@blocket.se', '0494-109 90', '0494-101 90', 'Vimmerbyv�gen', '590 40', 'KISA', 'dackteam.gif', 'D�ck i Kisa', 'Personbilsd�ck:
Kolla v�r kampanjsida
<p>
Lastbilsd�ck:
Vi har alltid bra erbjudande p� regummerade lastbilsd�ck. Ring oss f�r vidare information.
<p>
Traktord�ck:
Vi jobbar med traktord�ck fr�n Trelleborg och Nokian.  Ring och prata med Staffan p� tel 0494-10990.
<p>
Vi skickar g�rna Dina best�llda varor med Bilpaket om Du s� �nskar.

', 'http://www.soderbergsgummi.se
', 'active', false, '2003-10-08 00:00:00', '2016-01-31 23:59:59', 'Vinterd�ck, Sommard�ck, D�ck, F�lg, F�lgar, Aluminiumf�lgar, L�ttmetallf�lgar, Aluminiumf�lg, L�ttmetallf�lg', '-', 'patrik', '-', '-', 'patrik', 'patrik', 14, 0, 1040);
INSERT INTO stores VALUES (9848, 'Bilgruppen Enk�ping Sala AB', 'guy_fordbilgruppen.com@blocket.se,petter_mazdabilgruppen.com@blocket.se', '0171 - 333 80', '0171 - 210 44', 'L�nggatan 66', '745 22', 'ENK�PING', 'logga.gif', '�terf�rs�ljare f�r Ford, Mazda', '
<p>
V�lkommen till BilGruppen Enk�ping Sala AB!
<br>
Vi �r auktoriserade �terf�rs�ljare f�r Ford och Mazda. Vi har �ven ett 120-tal begagnade bilar till f�rs�ljning. Vi finns p� tv� f�rs�ljningst�llen, i Enk�ping och i Sala. V�ra begagnade bilar �r alltid svensks�lda om inte annat anges i informationen, och givetvis �r vi medlemmar i MRF - en trygghet f�r dig som kund.
<p>
NU ING�R GARANTIF�RS�KRING 1 �R N�R DU K�PER BEGAGNAD BIL HOS OSS. 
<p>
Garantin avser bilar som �r max 8 �r eller max 15000 mil. Garantif�rs�kringen g�ller vid finansiering via BilGruppen eller kontant betalning. ', 'http://www.fordbilgruppen.com/', 'active', false, '2004-01-23 11:45:00', '2016-07-01 23:59:59', 'Audi, Ford, Mazda, Volkswagen, VW, Volvo', '-', 'patrik', '-', '-', 'patrik', 'patrik', 10, 0, 1020);
INSERT INTO stores VALUES (10285, 'S�derhamns Bilv�rd', '-', '0270-194 89', '0270-194 89', 'Inaberg 2144', '826 91', 'S�DERHAMN', 'logo.gif', 'Vi tar hand om din bil!', 'Allt inom Bilv�rd.<br>
Solfilmsmontering till b�de fordon och fastigheter. �ven inbrottsskyddsfilm.<br> �ppettider:<br>
M�n-Fre 08.00 - 17.00', '-', 'active', false, '2003-12-05 00:00:00', '2018-01-01 00:00:00', 'Bilv�rd, Solfilm, Inbrottsfilm, H�sttransport, Sl�pvagn,', '-', 'patrik', '-', '-', 'patrik', 'patrik', 5, 0, 1040);
INSERT INTO stores VALUES (10300, 'Edsbyns Bilv�rdscenter AB', '-', '0271-217 53', '0271-214 13', 'Bruggev�gen 6', '828 32', 'EDSBYN', '-', 'Auktoriserad Toyota verkstad', 'Auktoriserad Toyota verkstad. Stort urval av delar till Volvo
orginal.
<p>
�ppettider:
<br>M�n-Tors 0700-1630
<br>Fre 0700-1500 
<br>L�r-S�n St�ngt.
', '-', 'active', false, '2003-11-27 00:00:00', '2015-12-31 23:59:59', 'Toyota, Volvo orginal, Volvo', '-', 'patrik', '-', '-', 'patrik', 'patrik', 5, 0, 1040);
INSERT INTO stores VALUES (10402, 'Sundsvalls D�ck & F�lg Service AB', 'dackofalg_hotmail.com@blocket.se', '060-12 05 65', '060-61 23 68', 'Hovgatan 1', '854 62', 'SUNDSVALL', 'logo.gif', 'Norrlands st�rsta d�ck och f�lglager!', 'Norrlands st�rsta d�ck och f�lglager.<br>
Vi hj�lper �ven till med framvagnsinst�llning.', '-', 'active', false, '2003-09-17 00:00:00', '2016-03-01 23:59:59', 'D�ck, hjulinst�llning, f�lgar, michelin, hankook, fulda, framvagnsinst�llning, aluminiumf�lgar', '-', 'patrik', '-', '-', 'patrik', 'patrik', 4, 0, 1040);
INSERT INTO stores VALUES (10668, 'Dahlqvist Bil i Norr AB', 'info_dahlqvist-bil.com@blocket.se', '0920-606 75', '0920-606 73', 'Banv�gen 16', '973 46', 'LULE�', 'toplog.gif', 'Auktoriserad �terf�rs�ljare av Hyundai!', 'V�LKOMMEN TILL DAHLQVIST BIL!
<p>
Auktoriserad �terf�rs�ljare av Hyundai.
<p>
Vi har �ven verkstad i samma byggnad som butiken.
<br>
Vi utf�r alla typer av service och reparationer.
<p>
�ppettider:<br>
M�ndag-Fredag 09.00-17.30<br>
L�rdag (sept-maj) 10.00-13.00', 'http://www.dahlqvist-bil.com/', 'active', false, '2004-04-16 11:15:00', '2016-05-01 23:59:59', 'Hyundai, Bil, Bilar, Kombi,', '-', 'patrik', '-', '-', 'patrik', 'patrik', 1, 0, 1020);
INSERT INTO stores VALUES (10703, 'GMC Karlstad AB', 'goran_g-mc.se@blocket.se', '054-85 05 55', '054-87 66 87', 'Gjuterigatan 9', '652 21', 'KARLSTAD', 'GMC-logo.gif', '�terf�rs�ljare f�r Kawasaki MC o Piaggio, Gilera o Vespa mopeder', '<B>V�lkommen till GMC i Karlstad</B><br>
Vi �r auktoriserade f�r Kawasaki i V�rmland.
Vi som arbetar p� GMC �r sj�lva flitiga mc f�rare b�de proffisionellt och privat. Vi g�r detta arbete f�r att vi tycker att motorcykel �kning �r det roligaste man kan g�ra med kl�derna p�.
<p>
K�p din MC eller moped hos oss. Vi har alltid intressanta objekt i lager. 
I v�r butik hittar du det mesta, s�som Axo & MP-Asu kl�der, samt andra fina tillbeh�r.
<p>
Du kan �ven med trygghet v�nda dig till v�r auktoriserade verkstad. 
<p>
Ta g�rna en titt in till oss om ni har v�garna f�rbi sola i Karlstad!
<p>
<B>V�ra direktnr:</B><br>
F�rs�ljning 054-85 05 55<br>
Reservdelar 054-85 08 25<br>
Verkstad 054-85 00 47<br>
<p>
V�lkommen till GMC f�r din n�sta och b�sta aff�r!
', 'http://www.g-mc.se', 'active', false, '2003-08-21 16:35:00', '2015-12-31 00:00:00', 'Kawasaki, Piaggio, Gilera, Vespa', '-', 'patrik', '-', '-', 'patrik', 'patrik', 7, 0, 1140);
INSERT INTO stores VALUES (10711, 'Larsson:s MC Racing', 'info_mc-racing.se@blocket.se', '0251-419 48', '0251-419 48', 'Dalgatan 57', '796 31', '�LVDALEN', 'larssons.gif', 'Arctic Cat i �lvdalen', 'V�lkomna!', 'http://www.mc-racing.se
', 'active', false, '2003-09-01 00:00:00', '2015-12-31 00:00:00', 'Arctic Cat, Ski doo, Kawasaki, Suzuki, Honda', '-', 'patrik', '-', '-', 'patrik', 'patrik', 6, 0, 1180);
INSERT INTO stores VALUES (10712, 'Johansson Maskin AB', 'calesi_johanssonmaskinkl.se@blocket.se', '0935-203 70', '0935-208 49', 'F�retagsv.11', '911 35', 'V�NN�SBY', 'johanssons.GIF', 'SKOTRAR, ATV, SKOG & TR�DG�RD', 'KL Johansson Maskinf�rs�ljning AB fick sin nuvarande form 1992. Men f�retaget har funnits sedan 1979. 
<p>
Vi bedriver f�rs�ljning och service med nya och begagnade lantbruks entreprenad och tr�dg�rdsmaskiner samt sn�skotrar och ATV:s. 
<p>
Vi har �ven en Butik med ett brett tillbeh�rssortiment f�r villa, tr�dg�rd, lantbruk och skoter.', 'http://www.johanssonmaskinkl.se', 'active', false, '2003-09-02 00:00:00', '2015-12-31 23:00:00', 'Lynx,Entreprenadmaskiner, Lantbruksmaskiner, tr�dg�rdsmaskiner, Masset
Ferguson, Fendt, Nokka, Tume, trejon, Pottinger, Welger, Lely, Villab,
Stihl, Viking, Husqvarna, Bombardier.

', '-', 'patrik', '-', '-', 'patrik', 'patrik', 2, 0, 1180);
INSERT INTO stores VALUES (10713, 'Gunnar Andersson Bil AB', 'gunnar.anderssonbilab_telia.com@blocket.se', '054-56 20 80', '054-56 22 85', 'Sp�rgatan 3', '65343', 'KARLSTAD', 'butik_logga.gif', '�terf�rs�ljare f�r Peugeot', 'V�lkommen till <b>GUNNAR ANDERSSON BIL AB</b> i Karlstad. <br>
H�r hittar du landets just nu hetaste bilm�rke: <b>PEUGEOT.</b><p>

F�r oss p� GUNNAR ANDERSSON BIL AB �r det extra roligt att s�lja Peugeot, inte minst nu d� PEUGEOT�r ett av Sveriges snabbast v�xande bilm�rken. 
<p>
Orsakerna till framg�ngarna �r m�nga. En av dem �r utan tvekan dagens imponerande Peugeot-program med modeller f�r alla som uppskattar elegans, sp�nnande formgivning, inspirerande k�regenskaper, trygga garantier och riktigt mycket bil f�r pengarna.
<p>
D�rf�r f�resl�r vi en n�rmare titt och en riktigt l�ng och inspirerande provtur.
<p>
V�lkommen in till oss! 


', 'http://www.gabilab.se
', 'active', false, '2005-09-20 00:00:00', '2015-12-31 23:59:59', 'Audi, BMW, Ford, Hyndai, Ferrari, Jaguar, Mercedes-Benz, Nissan, Opel, Peugeot, Renault, Saab, Volvo', '-', 'patrik', '-', '-', 'patrik', 'patrik', 7, 0, 1020);
INSERT INTO stores VALUES (10715, 'Sweden Motorshop AB', 'info_swedenmotorshop.com@blocket.se', '031-52 28 30', '031-52 28 30', 'Backa Berg�gata 9', '422 46', 'HISINGS BACKA', '8_2_05.gif', 'Begagnade och nya motorcyklar av de vanligaste m�rkena', 'P� Sweden Motorshop har vi specialiserat oss p� begagnade och nya motorcyklar av de vanligaste m�rkena, d v s Honda, Yamaha, Suzuki och Kawasaki. Vi k�per in v�ra cyklar hos seri�sa handlare i Tyskland. Tack vare det stora utbudet och de l�gre priserna d�r erbjuder vi bra motorcyklar i olika prisklasser, samtidigt till ett betydligt b�ttre pris �n m�nga m�rkeshandlare. Hos oss k�per du exakt samma cykel som hos m�rkeshandlaren, fast till ett betydligt l�gre pris. ', 'http://www.swedenmotorshop.com
', 'active', false, '2003-09-05 00:00:00', '2016-10-24 23:59:59', 'Yamaha, Suzuki, Piaggio, Peugeot, Kawasaki, Honda', '-', 'patrik', '-', '-', 'patrik', 'patrik', 15, 0, 1140);
INSERT INTO stores VALUES (10720, 'TrendAuto i �rebro AB', 'trendbil_hotmail.com@blocket.se', '019-25 00 04', '019-25 00 04', 'Prologgttan 9', '703 59', '�REBRO', '-', 'M�rkesoberoende handlare', 'K�PER-BYTER-S�LJER
<p>
Ring oss f�r snabbare kontakt!
<p>
V�lkomna!
', 'http://www.trendauto.se', 'active', false, '2003-09-10 10:00:00', '2017-07-01 23:59:59', 'Volvo, BMW, Ford, Mazda, Opel, Chrysler, Volkswagen, Audi, Renault', '-', 'patrik', '-', '-', 'patrik', 'patrik', 8, 0, 1020);
INSERT INTO stores VALUES (10729, 'Bike Trollh�ttan', 'mc_biketrollhattan.se@blocket.se', '0520-48 80 30', '0520-48 80 31', 'Lextorpsv�gen 997', '461 65', 'TROLLH�TTAN', 'biketrollhattan.gif', 'Ett b�ttre begagnatk�p med BIKE:s trygghetspaket', '<li>1 �rs garanti: N�r du k�per en begagnad MC hos
oss p� Bike f�r du 1 �rs garanti
(eller 750 mil.)

<li>Varudeklaration: Varje begagnad MC har genomg�tt v�rt
inbytestest som omfattar 48 punkter.

<li>Bytesr�tt: Hos kan du k�pa begagnad
motorcykel med r�tt att byta
<li>Auktoriserad �terf�rs�ljare och verkstad
<p>
Allt f�r att kunna ge dig b�sta m�jliga service!', 'http://www.biketrollhattan.se', 'active', false, '2003-09-22 00:00:00', '2016-05-01 23:59:59', 'Honda, Yamaha, Suzuki, Kawasaki, KTM, BMW', '-', 'patrik', '-', '-', 'patrik', 'patrik', 16, 0, 1140);
INSERT INTO stores VALUES (10723, 'Folksam Auto AB', 'info_folksamauto.com@blocket.se', '0470-77 11 00', '0470-77 11 25', 'Ljungadalsgatan 6, Box 3015', '352 46', 'V�XJ�', 'folksamauto2.gif', 'Kvalitet och trygghet!', '<li> Bilf�rs�ljning
<li> Service och reparationer
<li> Hjulinst�llning 
<li> Luftkonditionering, ackrediterade enligt Swedac
<li> Aluminiumsvetsning
<li> Karosseriverkstad
<li> Lackeringsverkstad
<li> Plastreparationer
<li> Bildemontering / reservdelar
<li> Utbildning
<li> Medlemmar i MRF, SBR och certifierade enligt KPL 2000
<li> Kontrollerad bilverkstad
<li> Milj�certifierade enligt ISO 14001
<p>
Vi har bilar av nyare �rsmodell som reparerats i v�r egen anl�ggning. Ring v�r s�ljare 0470-771130 s� hj�lper han dig.
', 'http://www.folksamauto.com', 'active', false, '2003-09-10 00:00:00', '2016-09-17 23:59:59', 'Begagnade bilar, Bilf�rs�ljning', '-', 'patrik', '-', '-', 'patrik', 'patrik', 21, 0, 1020);
INSERT INTO stores VALUES (10728, 'Sj�marks D�ck', 'sjomadack_telia.com@blocket.se', '0581-61 10 00', '0581-61 15 80', 'Skrinnargatan 4 A', '711 34', 'LINDESBERG', 'sjomark.gif', 'D�ck i Lindesberg', 'D�ck, F�lgar och Tillbeh�r
<p>
<li>D�ckia<p>
Skaffa D�ckia kortet, s� f�r du 3 m�nader r�ntefritt + fri ink�ps m�nad.<br>
Kontakta oss f�r mera information. I sammarbete med citibank.<p>
�ppettider:<br>
M�ndag-fredag 07:00-16:30<p>
', 'http://www.sjomarksdack.nu
', 'active', false, '2003-09-23 00:00:00', '2016-09-10 23:59:59', 'Yokohama, Sava, Intensa, Good Year, Mac Ripper', '-', 'patrik', '-', '-', 'patrik', 'patrik', 8, 0, 1040);
INSERT INTO stores VALUES (10732, 'Grevens MC-Delar', 'info_grevensmcdelar.com@blocket.se', '0320-414 15', '0320-414 23', 'Sl�tts�v�gen 38-40', '511 91', 'SKENE', 'grevens.gif', 'Din leverant�r av begagnade delar och motorcyklar', 'V�lkommen till Grevens MC-delar
<p>
Din leverant�r av begagnade delar och motorcyklar. Vi har ett stort lager
med delar till de flesta Japanska mc men �ven en hel del andra delar ocks�.
H�r p� sidan kan du �ven f�lja min roadracing-karri�r i form av resultat,
nyheter, bilder m.m.
<p>
Vi har alltid ett g�ng begagnade mc hemma. Klicka p� flaggan ovan f�r
aktuellt lager.
<p>
V�lkommen till Grevens n�r det g�ller MC', 'http://www.grevensmcdelar.com', 'active', false, '2003-09-25 00:00:00', '2015-12-31 23:00:00', 'Ducati, Honda, Yamaha, Suzuki, Kawasaki', '-', 'patrik', '-', '-', 'patrik', 'patrik', 16, 0, 1140);
INSERT INTO stores VALUES (10733, 'S�vsj�Sl�pet & Industrimontage AB', 'kn_ssiab.se@blocket.se', '08-4423260', '08-6690925', 'Snickarv�gen 5', '132 28', 'Saltsj�-Boo', 'savsjo.gif', 'Vi st�ller hela v�r samlade erfarenhet och kompetens till f�rfogande!', 'Vi �r ett fullsortimentstillverkarande f�retag av personbilssl�p och har l�ng erfarenhet av specialbyggnationer. Vi har ett komplett reservdelslager och s�ljer och byter in begagnade sl�p.
<p>
Ni kan n� Kjell Nordberg p� 0709-839692
', 'http://www.ssiab.se', 'active', false, '2003-09-25 11:00:00', '2015-11-27 00:00:00', 'Flakvagn, Biltransport, Sk�psl�pvagn, B�tupptagningsvagn, Djurtransportsl�p, Sl�pvagnar, sl�pvagn, sl�p,
trailer
', '-', 'patrik', '-', '-', 'patrik', 'patrik', 11, 0, 1140);
INSERT INTO stores VALUES (10734, 'S�vsj�Sl�pet & Industrimontage AB', 'kn_ssiab.se@blocket.se', '08-4423260', '08-6690925', 'Snickarv�gen 5', '132 28', 'Saltsj�-Boo', 'savsjo.gif', 'Vi st�ller hela v�r samlade erfarenhet och kompetens till f�rfogande!', 'Vi �r ett fullsortimentstillverkarande f�retag av personbilssl�p och har l�ng erfarenhet av specialbyggnationer. Vi har ett komplett reservdelslager och s�ljer och byter in begagnade sl�p.
<p>
Ni kan n� Kjell Nordberg p� 0709-839692', 'http://www.ssiab.se', 'active', false, '2003-09-25 11:00:00', '2015-11-27 00:00:00', 'Flakvagn, Biltransport, Sk�psl�pvagn, B�tupptagningsvagn, Djurtransportsl�p, Sl�pvagnar, sl�pvagn, sl�p,
trailer
', '-', 'patrik', '-', '-', 'patrik', 'patrik', 11, 0, 1040);
INSERT INTO stores VALUES (10736, 'V�xj� Bildemontering AB', 'vaxjo.bildemo_telia.com@blocket.se', '0470-70 96 50', '0470-70 96 40', 'Skir', '355 91', 'V�xj�', 'vaxjobildemo.gif', 'Pr�va n�got nytt - K�p begagnat!', 'Vi har begagnade bildelar samt d�ck och f�lgar till de flesta bilm�rken �ven fr�n nyare krockade bilar med garanti och l�ga priser. 
<p>
Vi s�ljer �ven p� postorder �ver hela Sverige se lev.villkor/fraktpris Har vi inte den del du s�ker s� kan du sj�lv enkelt s�ka den genom bildelsbaserna h�r ovan Vi har �ven nya delar till r�tt pris.
<p>
Vi �r ett auktoriserat bildemonteringsf�retag och medlem i SBR-Sveriges Bilskrotares Riksf�rbund. 
<p>
Vi �r milj�certifierade enl ISO 14001', 'http://www.bildemontering.nu', 'active', false, '2003-09-26 00:00:00', '2016-09-30 23:59:59', 'ALFA, 
AUDI, 
BMW, 
CHRYSLER, 
CITROEN, 
DAEWOO, 
DAIHATSU, 
DATSUN, 
FIAT, 
FORD, 
HONDA, 
HYUNDAI, 
ISUZU, 
IVECO, 
JAGUAR, 
KIA, 
LADA, 
LANCIA, 
MAZDA, 
MERCEDES, 
MITSUBISHI, 
NISSAN, 
OPEL, 
PEUGEOT, 
PORSCHE, 
RENAULT, 
ROVER, 
SAAB, 
SEAT, 
SIMCA, 
SKODA, 
SL�P, 
SUBARU, 
SUZUKI, 
TOYOTA, 
VAZ, 
VOLVO, 
VW, ', '-', 'patrik', '-', '-', 'patrik', 'patrik', 21, 0, 1040);
INSERT INTO stores VALUES (10738, 'N��s Maskin & Motor AB', 'maskin.o.motor_herjenet.net@blocket.se', '0684-109 80', '0684-106 34', 'R�ndalsv�gen 3365', '840 93', 'Hede', '-', '�terf�rs�ljare F�r Arctic Cat & Honda', '-', '-', 'active', false, '2003-09-26 00:00:00', '2015-12-31 00:00:00', 'Honda, ATV, Fyrhjulingar, Ski-doo, Polaris, Arctic Cat, Yamaha', '-', 'patrik', '-', '-', 'patrik', 'patrik', 3, 0, 1180);
INSERT INTO stores VALUES (10739, 'Motorcity i J�rpen AB', 'motorcityab_telia.com@blocket.se', '0647-61 11 90', '0647-61 14 60', 'Strandv�gen 42', '830 05', 'J�rpen', '-', '�terf�rs�ljare f�r Ski-doo', '�ppettider:<br>
M�n - Fre 08.00-17.00<br>
Lunch 12.00-13.00
<p>
V�lkomna!
', '-', 'active', false, '2003-09-26 00:00:00', '2015-12-31 00:00:00', 'Ski-doo, Arctic Cat, Polaris, Lynx, Yamaha', '-', 'patrik', '-', '-', 'patrik', 'patrik', 3, 0, 1180);
INSERT INTO stores VALUES (10740, 'T�gagaraget', 'info_tagagaraget.nu@blocket.se', '042-14 62 66', '042-12 81 70', 'T�gagatan 86', '254 54', 'HELSINGBORG', '28_12_04.jpg', 'S�ljer, k�per, byter samt f�rmedlar kvalitetsbilar', 'T�gagaraget �r grundat 1961 och s�ljer, k�per, byter samt f�rmedlar kvalitetsbilar av alla m�rken.
<p>
H�r p� v�r sida finner Ni samtliga bilar vi har i lager f�r tillf�llet, oftast representerade med bild.
<p>
Hittar Ni inte en bil som passar s� tar vi �ven hem bilar p� best�llning omg�ende.
<p>
V�r ambition �r att alltid ha marknadens l�gsta priser samt 100% n�jda kunder.
<p>
Vi erbjuder ocks� konkurrenskraftigt finansiering med bra r�nta. V�lkommen in till oss p� T�gagatan 86 i Helsingborg.
', 'http://www.tagagaraget.nu', 'active', false, '2003-09-26 00:00:00', '2016-01-01 23:59:59', 'Audi, BMW, Chrysler, Ford, Mercedes, Mitsubishi, Opel, Saab, Volkswagen, Volvo', '-', 'patrik', '-', '-', 'patrik', 'patrik', 23, 3, 1020);
INSERT INTO stores VALUES (10742, 'H�ggs Bildemontering AB i Ilsbo', 'delar_haggsbildem.se@blocket.se', '0652-190 15', '0652-190 01', 'Vattl�ng 5590', '820 71', 'ILSBO', 'haggs.gif', 'Kvalitets- och milj�- certifierat enligt ISO 9002 och 14001', '�PPETTIDER<br>
M�n-Tors 07.00-16.30<br>
Lunchst�ngt 12.00-13.00<br>
<p>
Fre 07.00-14.00 (ej lunchst�ngt)', 'http://www.haggsbildem.se', 'active', false, '2003-09-29 00:00:00', '2015-12-23 23:00:00', 'ALFA, AMC, 
AUDI, 
BEDFORD, 
BMC, 
BMW, 
CHRYSLER, 
CITROEN, 
DAEWOO, 
DAF, 
DAIHATSU, 
DATSUN, 
DODGE, 
FIAT, 
FORD, 
GM, 
HANOMAG, 
HONDA, 
HUSVAGNAR, 
HYUNDAI, 
ISUZU, 
IVECO, 
JEEP, 
LADA, 
LANCIA, 
LASTBILAR, 
MAZDA, 
MC, 
MERCEDES, 
MITSUBISHI, 
NISSAN, 
OPEL, 
PEUGEOT, 
PORSCHE, 
RENAULT, 
SAAB, 
SEAT, 
SIMCA, 
SKODA, 
SL�P, 
SMART, 
SN�SKOTER, 
SUBARU, 
SUZUKI, 
TOYOTA, 
TRAKTOR, 
VAUXHALL, 
VAZ, 
VOLVO, 
VW
', '-', 'patrik', '-', '-', 'patrik', 'patrik', 5, 0, 1040);
INSERT INTO stores VALUES (13083, 'Michael Andersson Bil', 'pellebabs_telia.com@blocket.se', '0650-754 00', '0650-754 00', 'Gammelbansv�gen 1', '824 40', 'HUDIKSVALL', '-', 'M�rkesoberoende handlare', 'M�rkesoberoende handlare.
<p>
Mobil : 070-313 87 28<p>
V�lkomna!', 'http://
', 'active', false, '2004-09-21 11:00:00', '2016-02-01 23:59:59', 'Audi, Ford, Mercedes, Toyota, Volvo,', '-', 'patrik', '-', '-', 'patrik', 'patrik', 5, 0, 1020);
INSERT INTO stores VALUES (10743, 'J�nk�pings Bildemontering AB', 'info_jb-bildemo.se@blocket.se', '036-39 44 50', '036-930 34', 'B�ckamarken', '555 92', 'J�NK�PING', 'jb.gif', 'Begagnade original delar', 'Begagnade original delar fr�n demonterade VOLVO & RENAULT bilar av senaste �rsmodell och �ldre.
<p>
V�r bildels-shop och ordermottagning
har f�ljande �ppettider:<br>
M�ndag 0700-1630<br>       
Tisdag 0700-1630<br>
Onsdag  0700-1630<br>
Torsdag 0700-1800<br>
Fredag 0700-1630', 'http://www.jb-bildemo.se', 'active', false, '2003-09-29 00:00:00', '2015-11-30 00:00:00', 'FIAT
FORD
PEUGEOT, 
RENAULT, 
ROVER, 
VOLVO
', '-', 'patrik', '-', '-', 'patrik', 'patrik', 17, 0, 1040);
INSERT INTO stores VALUES (10952, 'Vincents Bikeshop, Varberg', 'info_vincents.se@blocket.se', '0340-67 77 78', '0340-644 744', 'Birger Svenssons V�g 30', '432 40', 'VARBERG', 'vincents.gif', 'Vi �r en av v�stsveriges st�rsta handlare av begagnade motorcyklar.', 'Vi har inriktat oss p� att bli riktigt bra p� begagnade custom, landsv�g och offroad motorcyklar. Hos oss finner du marknadens mest popul�ra m�rken, dvs Yamaha, Suzuki, Kawasaki, Honda etc. H�r k�per du exakt samma cykel som du finner hos m�rkeshandlaren men till ett avsev�rt l�gre pris.  I och med att utbudet dessutom �r stort, �r det mycket troligt att du kan hitta din dr�mcykel till ett bra pris hos oss.   


<p>
Specialbest�llningar �r en annan av v�ra specialiteter.   Har du specifika �nskem�l betr�ffande m�rke, modell, utrustning etc.? Skicka i s� fall en f�rfr�gan s� skall vi g�ra  vad vi kan f�r att hitta din �gonsten.   
<p>
Nytt f�r i �r �r att vi har en helt ny och v�lutrustad verkstad. Under vintern �r mekanikern p� plats 10:30 - 14:30. 
<p>
I �r har vi ocks� ut�kat v�rt urval av MC kl�der och accessoarer, �ven h�r erbjuder vi f�rm�nliga priser. Kom in och prova! 


', 'http://www.vincents.se', 'active', false, '2003-10-02 00:00:00', '2016-05-15 23:59:59', 'Suzuki, Yamaha, Kawasaki, Kymco, Adler, Aprilia, Gilera, PGO, Honda, Highland, Begagnade motorcykel, MC
', '-', 'patrik', '-', '-', 'patrik', 'patrik', 20, 0, 1140);
INSERT INTO stores VALUES (10956, 'SM Bilar', 'sm-bilar_swipnet.se@blocket.se', '054-53 00 77', '054-53 00 17', 'Fj�rrviksv�gen 6', '653 50', 'KARLSTAD', 'smbilar.gif', 'Auktoriserad �terf�rs�ljare f�r Citro�n', 'SM Bilar i Karlstad �r din Citro�n �terf�rs�ljare i Karlstad.
<p>
Det viktigaste f�r oss �r att Du som kund kan k�nna dig helt trygg efter avslutat k�p.
<p>
Med Citro�ns fullmatade nybilsprogram i kombination med v�rt breda urval av begagnade bilar g�r att vi n�stan alltid kan erbjuda Dig en bil som passar just dina behov.
<p>
Du kan �ven med trygghet v�nda Dig till v�r serviceverkstad.<br>
Direkttel verkstaden: 054-53 35 77
<p>
V�lkomna till oss f�r Din b�sta bilaff�r.
', 'http://www.smbilar.com', 'active', false, '2003-10-02 00:00:00', '2016-06-15 23:59:59', 'Citro�n, Audi, Kia, Mitsubishi, Mercedes, Mazda, Nissan, Opel, Saab, Subaru, Volkswagen, Volvo', '-', 'patrik', '-', '-', 'patrik', 'patrik', 7, 0, 1020);
INSERT INTO stores VALUES (10957, 'Kungs�ra Bildemontering AB', 'info_kungsarabildemo.se@blocket.se', '021-210 08', '021-210 22', 'Karleby 16', '725 98', 'V�STER�S', 'kungsara.gif', 'Bildelar med garanti', 'Kungs�ra Bilskrot AB �r SBR-certifierat.Varje �r demonteras h�r 1200 - 1600 bilar. V�ran demonteringsyta �r 25 000 kvm. V�ra 75 000 delar finns inlagrade i en databas. S�k i lagret efter de delar Du saknar.', 'http://www.kungsarabilskrot.se', 'active', false, '2003-10-03 00:00:00', '2016-10-11 23:59:59', 'ALFA, 
AUDI, 
BMC, 
BMW, 
CHRYSLER, 
CITROEN, 
DAIHATSU, 
DATSUN, 
DODGE, 
FIAT, 
FORD, 
GM, 
HONDA, 
HUSVAGNAR, 
HYUNDAI, 
ISUZU, 
IVECO, 
JAGUAR, 
JEEP, 
KIA, 
LADA, 
LANCIA, 
MAZDA, 
MERCEDES, 
MITSUBISHI, 
NISSAN, 
OPEL, 
PEUGEOT, 
PORSCHE, 
RENAULT, 
ROVER, 
SAAB, 
SEAT, 
SIMCA, 
SKODA, 
SL�P, 
SUBARU, 
SUZUKI, 
TOYOTA, 
VAUXHALL, 
VAZ, 
VOLVO, 
VW', '-', 'patrik', '-', '-', 'patrik', 'patrik', 9, 0, 1040);
INSERT INTO stores VALUES (10961, 'M�lndals Bildemontering AB', 'info_molndalsbildemo.se@blocket.se', '031-870 110', '031-877 848', 'Kryptongatan 12', '431 53', 'M�LNDAL', 'molndals.gif', 'Bra begagnade bildelar, till bra priser', 'Hos M�lndals Bildemontering kan du k�pa bra begagnade bildelar, till bra priser.
<p>
F�r din trygghet, vi testar delarna och l�mnar alltid 30 dagars full bytesr�tt!
<p>
Postorder, Vi skickar delar �ver hela landet.
<p>
Har vi ej delen hj�lper vi dig att kostnadsfritt s�ka delen hos v�ra kollegor.
<p>
Vi lagerf�r �ven nya delar till ett stort antal bilmodeller, det som vi inte har i lager, tar vi hem till n�sta dag.  
', 'http://www.molndalsbildemo.se
', 'active', false, '2003-10-03 00:00:00', '2016-10-13 23:59:59', 'ALFA, 
AUDI, 
BEDFORD, 
BMC, 
BMW, 
CHRYSLER, 
CITROEN, 
DAF, 
DAIHATSU, 
DATSUN, 
DODGE, 
FIAT, 
FORD, 
GM, 
HONDA, 
HYUNDAI, 
ISUZU, 
JAGUAR, 
JEEP, 
LADA, 
LANCIA, 
MAZDA, 
MERCEDES, 
MITSUBISHI, 
NISSAN, 
OPEL, 
PEUGEOT, 
PIAGGIO, 
PORSCHE, 
RENAULT, 
ROVER, 
SAAB, 
SEAT, 
SIMCA, 
SKODA, 
SUBARU, 
SUZUKI, 
TOYOTA, 
TRIUMPH, 
VAUXHALL, 
VAZ, 
VOLVO, 
VW
', '-', 'patrik', '-', '-', 'patrik', 'patrik', 15, 0, 1040);
INSERT INTO stores VALUES (11325, 'Eklunds Bildelslager', 'info_eklunds.nu@blocket.se', '0500-41 38 45', '0500-41 82 80', 'Pl 3192', '541 33', 'SK�VDE', 'butik.gif', 'I branschen sedan 1932', 'Skrotning av nyare krockskadade bilar.
<p>
V�r moderna anl�ggning finns centralt bel�gen i V�stra G�talands l�n, i Sk�vde kommun.
<p>
�ppettider:<br> 
M�ndag - Fredag 7:00 - 17:00<br>
Lunch mellan 12:00 - 13:15', 'http://www.eklunds.nu', 'active', false, '2003-10-06 00:00:00', '2016-10-08 23:59:59', 'ALFA, 
AUDI, 
BMC, 
BMW, 
CHRYSLER, 
CITROEN, 
DAF, 
DAIHATSU, 
DATSUN, 
DODGE, 
FIAT, 
FORD, 
GM, 
HONDA, 
HUSVAGNAR, 
HYUNDAI, 
ISUZU, 
IVECO, 
JAGUAR, 
LADA, 
LANCIA, 
LANDROVER, 
MAZDA, 
MERCEDES, 
MITSUBISHI, 
NISSAN, 
OPEL, 
PEUGEOT, 
RENAULT, 
ROVER, 
SAAB, 
SEAT, 
SIMCA, 
SKODA, 
SUBARU, 
SUZUKI, 
TOYOTA, 
VAZ, 
VOLVO, 
VW
', '-', 'patrik', '-', '-', 'patrik', 'patrik', 13, 0, 1040);
INSERT INTO stores VALUES (11328, '�dalens Bildemontering AB', 'adalens.bildemontering_swipnet.se@blocket.se', '0612-124 20', '0612-71 17 48', 'Karossv�gen 1', '872 33', 'KRAMFORS', 'adalens2.gif', 'Bildemontering', '�ppettider:<br>
M�ndag - Torsdag 7:00 -16:30 <br>
Fredag 7:00 -15:00<br>
Lunchst�ngt 12:00 - 13:00
<p>
�dalens Bildemontering AB startade 1954. V�r huvudverksamhet �r bildemontering av f�rs�kringsskador, men vi tar �ven emot bilar fr�n privata. Dessutom demonterar vi motorcyklar och lastbilar.
<p>
Fr o m �rsskiftet 2002 har vi f�tt kontrakt med Volvia. Det inneb�r att vi k�per in alla krockskadade Volvo-bilar fr�n V�sterbotten, V�sternorrland, J�mtland och delar av G�vleborg som �r f�rs�krade i Volvia. Detta inneb�r att vi demonterar ca. 400 Volvo-bilar per �r.
<p>
Bildemonteringen bedrivs rationellt och med st�rsta m�jliga milj�h�nsyn. Kunderna �r i stor utrstr�ckning bilverkst�der. Reservdelar levereras oftast inom ett dygn. Inom f�retaget finns �ven en gren med �keriverksamhet.
<p>
Vi �r �ven medlem i assistancek�ren d�r vi har tv� l�ttb�rgare anslutna.


', 'http://www.adalensbildemontering.se', 'active', false, '2003-10-07 00:00:00', '2016-10-18 23:59:59', 'ALFA, 
AUDI, 
BEDFORD, 
BMC, 
BMW, 
CHRYSLER, 
CITROEN, 
DAEWOO, 
DAF, 
DAIHATSU, 
DATSUN, 
DODGE, 
ENTREPRENADMASKINER, 
FIAT, 
FORD, 
GINETTA, 
GM, 
HANOMAG, 
HONDA, 
HUSVAGNAR, 
HYUNDAI, 
ISUZU, 
JEEP, 
KIA, 
LADA, 
LANCIA, 
LASTBILAR, 
MAZDA, 
MC, 
MERCEDES, 
MITSUBISHI, 
NISSAN, 
OPEL, 
PEUGEOT, 
PIAGGIO, 
PORSCHE, 
RENAULT, 
ROVER, 
SAAB, 
SEAT, 
SIMCA, 
SKODA, 
SN�SKOTER, 
SSANGYONG, 
SUBARU, 
SUZUKI, 
TOYOTA, 
VAUXHALL, 
VAZ, 
VOLVO, 
VW 
', '-', 'patrik', '-', '-', 'patrik', 'patrik', 4, 0, 1040);
INSERT INTO stores VALUES (11691, 'VEGE Motorer AB', 'vege.motorer_vege.se@blocket.se', '042-16 42 45', '042-16 42 46', 'Florettgatan 39', '254 67', 'HELSINGBORG', 'vege.gif', 'Helrenoverade motorer till originalkvalitet', 'Sedan 1936 har VEGE Motorer levererat utbytesmotorer till n�stan alla bilm�rken. Till samma h�ga kvalitet som original.
<p>
Begagnade motorer direkt fr�n lager i Helsingborg: <br>
Som komplement till renoverade motorer har vi ett stort lager begagnade motorer. Alltid med l�ga miltal och av senare �rsmodeller. H�r kan du se vad vi har i lager just nu!', 'http://www.vege.se', 'active', false, '2003-10-09 00:00:00', '2015-11-15 00:00:00', 'Motor, Motorer, Topplock', '-', 'patrik', '-', '-', 'patrik', 'patrik', 23, 3, 1040);
INSERT INTO stores VALUES (11694, 'Allbildelar AB', 'allbildelar_allbildelar.se@blocket.se', '08-746 93 00', '08-746 90 93', 'H�k�rrsv�gen 125', '141 24', 'HUDDINGE', 'allbildelar.gif', 'Vi �r milj� & kvalitetscertifierade enligt BPS system sedan 2001', 'ALLBILDELAR �r ett familjef�retag som varit etablerat sedan 1964. Vi demonterar ca 4000 bilar per �r. Ca 80% av dessa �r av nyare �rsmodell och vi ser en fortsatt �kning i framtiden. Idag �r vi 28 anst�lda p� en yta av 40000 m�. Vi har en modern d�ckverkstad med b�de nytt & begagnat. 

<p>
�ppettider: <br>
M�n-Ons 08.00-17.00<br>
Torsdag 08.00-19.00<br>
Fredag 08.00-15.00<br>
<br>
Lunchst�ngt 11.00-11.45
', 'http://www.allbildelar.se', 'active', false, '2003-10-13 00:00:00', '2015-11-01 00:00:00', 'Bildelar, Biltillbeh�r, D�ck', '-', 'patrik', '-', '-', 'patrik', 'patrik', 11, 0, 1040);
INSERT INTO stores VALUES (11696, 'J&J Holmstr�ms Trading AB', 'info_mctema.com@blocket.se', '0492-100 60', '070-610 15 68', 'Pr�stg�rdsgatan 10', '598 36', 'VIMMERBY', 'mctema.gif', 'MC i Vimmerby', 'P� v�r hemsida hittar du information om oss och givetvis motorcyklarna som vi har hemma och som �r p� ing�ng. Mycket n�je!', 'http://www.mctema.com', 'active', false, '2003-10-13 00:00:00', '2016-01-30 23:59:00', 'BMW, Honda, Harley Davidson, Kawasaki, Mercedes, Mobilhome, Smart, Suzuki, Triumph, Yamaha', '-', 'patrik', '-', '-', 'patrik', 'patrik', 21, 0, 1140);
INSERT INTO stores VALUES (11697, 'J&J Holmstr�ms Trading AB', 'info_mctema.com@blocket.se', '0492-100 60', '070-610 15 68', 'Pr�stg�rdsgatan 10', '598 36', 'VIMMERBY', 'mctema.gif', 'MC i Vimmerby', 'P� v�r hemsida hittar du information om oss och givetvis motorcyklarna som vi har hemma och som �r p� ing�ng. Mycket n�je!', 'http://www.mctema.com', 'active', false, '2003-10-13 00:00:00', '2016-01-30 23:59:00', 'BMW, Honda, Harley Davidson, Kawasaki, Mercedes, Mobilhome, Smart, Suzuki, Triumph, Yamaha', '-', 'patrik', '-', '-', 'patrik', 'patrik', 14, 0, 1140);
INSERT INTO stores VALUES (11699, '�rebro Bildemo AB', 'orebro.bildemo_telia.com@blocket.se', '019-670 12 00', '019-32 15 80', 'Maskingatan 1', '700 06', '�REBRO', 'orebildemo.gif', 'Bildelar & Tillbeh�r', '�ppetTider: <br>
07:00 -17:00 Vardagar <br>
09:00 - 13:00 L�rdagar
', 'http://www.orebrobildemontering.se
', 'active', false, '2003-10-13 00:00:00', '2015-12-15 00:00:00', 'Volvo, Saab, WW, Audi, Skoda, Porsche, Seat, MC', '-', 'patrik', '-', '-', 'patrik', 'patrik', 8, 0, 1040);
INSERT INTO stores VALUES (11700, 'D�ckspecialen', 'claes.andersson_toyota.se@blocket.se', '031-750 24 70', '031-750 24 02', 'Lunnag�rdgatan 11', '431 90', 'M�LNDAL', 'dackspecialen.gif', 'D�ck i M�lndal', 'P� v�r webbplats kommer ni att finna diverse priser p� sommard�ck, vinterd�ck och aluminiumf�lgar. Ni navigerar med hj�lp av menyn. H�r kommer  ni �ven se vad vi k�r f�r erbjudande just nu.
<p>
V�l m�tt: Personalen p� D�ckspecialen
', 'http://www.dackspecialen.com', 'active', false, '2003-10-13 00:00:00', '2016-03-01 23:00:00', 'Gislaved, Hankook, Brigestone, Continental, ASA, Svenska F�lg, Specialf�lgar', '-', 'patrik', '-', '-', 'patrik', 'patrik', 15, 0, 1040);
INSERT INTO stores VALUES (11703, 'Signalen AB', 'signalen_telia.com@blocket.se', '0499-490 00', '0499-490 40', 'V�stra Okneb�cksv�gen  7', '383 36', 'M�NSTER�S', 'signalen.gif', 'Begagnade bildelar fr�n ett milj�- och kvalitetss�krat f�retag', 'Ett proffsigt s�ljteam tar hand om din reservdelsf�rfr�gan och expedierar din order oavsett vart du finns i Sverige. 
<p>
Signalen AB tar emot cirka 1000 krockskadade bilar per �r. 
De flesta �r bilar av nyare �rsmodell. 
Vi erbjuder �ven skarvpartier som exempelvis frontpartier, bakpartier, tak m.m. 
<p>
Alla delar genomg�r kvalitetskontroll i flera steg innan de registreras i datasystemet. Tack vare denna noggranna kontroll l�mnar vi upp till 3 m�naders garanti p� v�ra begagnade delar.
', 'http://www.signalen.com', 'active', false, '2003-10-15 00:00:00', '2016-10-18 23:59:59', 'ALFA, 
AUDI, 
BMC, 
BMW, 
CHRYSLER, 
CITROEN, 
DAF, 
DAIHATSU, 
DATSUN, 
DODGE, 
FIAT, 
FORD, 
GM, 
HONDA, 
HUSVAGNAR, 
HYUNDAI, 
ISUZU, 
JAGUAR, 
JEEP, 
KIA, 
LADA, 
LANCIA, 
LOTUS, 
MASERATI, 
MAZDA, 
MERCEDES, 
MITSUBISHI, 
NISSAN, 
OPEL, 
PEUGEOT, 
PORSCHE, 
RENAULT, 
ROVER, 
SAAB, 
SEAT, 
SIMCA, 
SKODA, 
SUBARU, 
SUZUKI, 
TOYOTA, 
TRIUMPH, 
VOLVO, 
VW
', '-', 'patrik', '-', '-', 'patrik', 'patrik', 18, 0, 1040);
INSERT INTO stores VALUES (11704, 'L�derups Husvagnstj�nst', 'info_loderupshusvagnstjanst.se@blocket.se', '0411-52 43 09', '0411-52 43 19', 'Storgatan 36', '276 40', 'L�DERUP', 'loderops.gif', 'Stort urval av b�de nya och begagnade husvagnar', 'L�derups Husvagnstj�nst ligger i Sk�ne
p� �sterlen, 5 km fr�n kusten mittemellan
Ystad och Simrishamn. 
H�r har vi s�lt och hyrt ut husvagnar mm
sedan 1972.
<p>
I v�r butik erbjuder vi Dig m�nga praktiska
tillbeh�r till husvagnen
<p>
Vi hj�lper �ven Dig som beh�ver reparera 
eller ha service p� husvagnen.
<p>
Hos oss hittar Du alltid ett stort urval av 
b�de nya och begagnade husvagnar med
passande f�rt�lt.', 'http://www.loderupshusvagnstjanst.se
', 'active', false, '2003-10-15 00:00:00', '2016-03-31 00:00:00', 'Cabby, F�rt�lt, Polar, Home-Car', '-', 'patrik', '-', '-', 'patrik', 'patrik', 23, 15, 1100);
INSERT INTO stores VALUES (11706, 'L�derups Husvagnstj�nst', 'info_loderupshusvagnstjanst.se@blocket.se', '0411-52 43 09', '0411-52 43 19', 'Storgatan 36', '276 40', 'L�DERUP', 'loderops.gif', 'Stort urval av b�de nya och begagnade husvagnar', 'L�derups Husvagnstj�nst ligger i Sk�ne
p� �sterlen, 5 km fr�n kusten mittemellan
Ystad och Simrishamn. 
H�r har vi s�lt och hyrt ut husvagnar mm
sedan 1972.
<p>
I v�r butik erbjuder vi Dig m�nga praktiska
tillbeh�r till husvagnen
<p>
Vi hj�lper �ven Dig som beh�ver reparera 
eller ha service p� husvagnen.
<p>
Hos oss hittar Du alltid ett stort urval av 
b�de nya och begagnade husvagnar med
passande f�rt�lt.', 'http://www.loderupshusvagnstjanst.se
', 'active', false, '2003-10-15 00:00:00', '2016-03-31 00:00:00', 'Cabby, F�rt�lt, Polar, Home-Car', '-', 'patrik', '-', '-', 'patrik', 'patrik', 23, 1, 1100);
INSERT INTO stores VALUES (11707, 'L�derups Husvagnstj�nst', 'info_loderupshusvagnstjanst.se@blocket.se', '0411-52 43 09', '0411-52 43 19', 'Storgatan 36', '276 40', 'L�DERUP', 'loderops.gif', 'Stort urval av b�de nya och begagnade husvagnar', 'L�derups Husvagnstj�nst ligger i Sk�ne
p� �sterlen, 5 km fr�n kusten mittemellan
Ystad och Simrishamn. 
H�r har vi s�lt och hyrt ut husvagnar mm
sedan 1972.
<p>
I v�r butik erbjuder vi Dig m�nga praktiska
tillbeh�r till husvagnen
<p>
Vi hj�lper �ven Dig som beh�ver reparera 
eller ha service p� husvagnen.
<p>
Hos oss hittar Du alltid ett stort urval av 
b�de nya och begagnade husvagnar med
passande f�rt�lt.', 'http://www.loderupshusvagnstjanst.se
', 'active', false, '2003-10-15 00:00:00', '2016-03-31 00:00:00', 'Cabby, F�rt�lt, Polar, Home-Car', '-', 'patrik', '-', '-', 'patrik', 'patrik', 23, 2, 1100);
INSERT INTO stores VALUES (11708, 'L�derups Husvagnstj�nst', 'info_loderupshusvagnstjanst.se@blocket.se', '0411-52 43 09', '0411-52 43 19', 'Storgatan 36', '276 40', 'L�DERUP', 'loderops.gif', 'Stort urval av b�de nya och begagnade husvagnar', 'L�derups Husvagnstj�nst ligger i Sk�ne
p� �sterlen, 5 km fr�n kusten mittemellan
Ystad och Simrishamn. 
H�r har vi s�lt och hyrt ut husvagnar mm
sedan 1972.
<p>
I v�r butik erbjuder vi Dig m�nga praktiska
tillbeh�r till husvagnen
<p>
Vi hj�lper �ven Dig som beh�ver reparera 
eller ha service p� husvagnen.
<p>
Hos oss hittar Du alltid ett stort urval av 
b�de nya och begagnade husvagnar med
passande f�rt�lt.', 'http://www.loderupshusvagnstjanst.se
', 'active', false, '2003-10-15 00:00:00', '2016-03-31 00:00:00', 'Cabby, F�rt�lt, Polar, Home-Car', '-', 'patrik', '-', '-', 'patrik', 'patrik', 23, 3, 1100);
INSERT INTO stores VALUES (11709, 'L�derups Husvagnstj�nst', 'info_loderupshusvagnstjanst.se@blocket.se', '0411-52 43 09', '0411-52 43 19', 'Storgatan 36', '276 40', 'L�DERUP', 'loderops.gif', 'Stort urval av b�de nya och begagnade husvagnar', 'L�derups Husvagnstj�nst ligger i Sk�ne
p� �sterlen, 5 km fr�n kusten mittemellan
Ystad och Simrishamn. 
H�r har vi s�lt och hyrt ut husvagnar mm
sedan 1972.
<p>
I v�r butik erbjuder vi Dig m�nga praktiska
tillbeh�r till husvagnen
<p>
Vi hj�lper �ven Dig som beh�ver reparera 
eller ha service p� husvagnen.
<p>
Hos oss hittar Du alltid ett stort urval av 
b�de nya och begagnade husvagnar med
passande f�rt�lt.', 'http://www.loderupshusvagnstjanst.se
', 'active', false, '2003-10-15 00:00:00', '2016-03-31 00:00:00', 'Cabby, F�rt�lt, Polar, Home-Car', '-', 'patrik', '-', '-', 'patrik', 'patrik', 23, 4, 1100);
INSERT INTO stores VALUES (11710, 'L�derups Husvagnstj�nst', 'info_loderupshusvagnstjanst.se@blocket.se', '0411-52 43 09', '0411-52 43 19', 'Storgatan 36', '276 40', 'L�DERUP', 'loderops.gif', 'Stort urval av b�de nya och begagnade husvagnar', 'L�derups Husvagnstj�nst ligger i Sk�ne
p� �sterlen, 5 km fr�n kusten mittemellan
Ystad och Simrishamn. 
H�r har vi s�lt och hyrt ut husvagnar mm
sedan 1972.
<p>
I v�r butik erbjuder vi Dig m�nga praktiska
tillbeh�r till husvagnen
<p>
Vi hj�lper �ven Dig som beh�ver reparera 
eller ha service p� husvagnen.
<p>
Hos oss hittar Du alltid ett stort urval av 
b�de nya och begagnade husvagnar med
passande f�rt�lt.', 'http://www.loderupshusvagnstjanst.se
', 'active', false, '2003-10-15 00:00:00', '2016-03-31 00:00:00', 'Cabby, F�rt�lt, Polar, Home-Car', '-', 'patrik', '-', '-', 'patrik', 'patrik', 23, 5, 1100);
INSERT INTO stores VALUES (11711, 'L�derups Husvagnstj�nst', 'info_loderupshusvagnstjanst.se@blocket.se', '0411-52 43 09', '0411-52 43 19', 'Storgatan 36', '276 40', 'L�DERUP', 'loderops.gif', 'Stort urval av b�de nya och begagnade husvagnar', 'L�derups Husvagnstj�nst ligger i Sk�ne
p� �sterlen, 5 km fr�n kusten mittemellan
Ystad och Simrishamn. 
H�r har vi s�lt och hyrt ut husvagnar mm
sedan 1972.
<p>
I v�r butik erbjuder vi Dig m�nga praktiska
tillbeh�r till husvagnen
<p>
Vi hj�lper �ven Dig som beh�ver reparera 
eller ha service p� husvagnen.
<p>
Hos oss hittar Du alltid ett stort urval av 
b�de nya och begagnade husvagnar med
passande f�rt�lt.', 'http://www.loderupshusvagnstjanst.se
', 'active', false, '2003-10-15 00:00:00', '2016-03-31 00:00:00', 'Cabby, F�rt�lt, Polar, Home-Car', '-', 'patrik', '-', '-', 'patrik', 'patrik', 23, 6, 1100);
INSERT INTO stores VALUES (11712, 'L�derups Husvagnstj�nst', 'info_loderupshusvagnstjanst.se@blocket.se', '0411-52 43 09', '0411-52 43 19', 'Storgatan 36', '276 40', 'L�DERUP', 'loderops.gif', 'Stort urval av b�de nya och begagnade husvagnar', 'L�derups Husvagnstj�nst ligger i Sk�ne
p� �sterlen, 5 km fr�n kusten mittemellan
Ystad och Simrishamn. 
H�r har vi s�lt och hyrt ut husvagnar mm
sedan 1972.
<p>
I v�r butik erbjuder vi Dig m�nga praktiska
tillbeh�r till husvagnen
<p>
Vi hj�lper �ven Dig som beh�ver reparera 
eller ha service p� husvagnen.
<p>
Hos oss hittar Du alltid ett stort urval av 
b�de nya och begagnade husvagnar med
passande f�rt�lt.', 'http://www.loderupshusvagnstjanst.se
', 'active', false, '2003-10-15 00:00:00', '2016-03-31 00:00:00', 'Cabby, F�rt�lt, Polar, Home-Car', '-', 'patrik', '-', '-', 'patrik', 'patrik', 23, 7, 1100);
INSERT INTO stores VALUES (11713, 'L�derups Husvagnstj�nst', 'info_loderupshusvagnstjanst.se@blocket.se', '0411-52 43 09', '0411-52 43 19', 'Storgatan 36', '276 40', 'L�DERUP', 'loderops.gif', 'Stort urval av b�de nya och begagnade husvagnar', 'L�derups Husvagnstj�nst ligger i Sk�ne
p� �sterlen, 5 km fr�n kusten mittemellan
Ystad och Simrishamn. 
H�r har vi s�lt och hyrt ut husvagnar mm
sedan 1972.
<p>
I v�r butik erbjuder vi Dig m�nga praktiska
tillbeh�r till husvagnen
<p>
Vi hj�lper �ven Dig som beh�ver reparera 
eller ha service p� husvagnen.
<p>
Hos oss hittar Du alltid ett stort urval av 
b�de nya och begagnade husvagnar med
passande f�rt�lt.', 'http://www.loderupshusvagnstjanst.se
', 'active', false, '2003-10-15 00:00:00', '2016-03-31 00:00:00', 'Cabby, F�rt�lt, Polar, Home-Car', '-', 'patrik', '-', '-', 'patrik', 'patrik', 23, 8, 1100);
INSERT INTO stores VALUES (11714, 'L�derups Husvagnstj�nst', 'info_loderupshusvagnstjanst.se@blocket.se', '0411-52 43 09', '0411-52 43 19', 'Storgatan 36', '276 40', 'L�DERUP', 'loderops.gif', 'Stort urval av b�de nya och begagnade husvagnar', 'L�derups Husvagnstj�nst ligger i Sk�ne
p� �sterlen, 5 km fr�n kusten mittemellan
Ystad och Simrishamn. 
H�r har vi s�lt och hyrt ut husvagnar mm
sedan 1972.
<p>
I v�r butik erbjuder vi Dig m�nga praktiska
tillbeh�r till husvagnen
<p>
Vi hj�lper �ven Dig som beh�ver reparera 
eller ha service p� husvagnen.
<p>
Hos oss hittar Du alltid ett stort urval av 
b�de nya och begagnade husvagnar med
passande f�rt�lt.', 'http://www.loderupshusvagnstjanst.se
', 'active', false, '2003-10-15 00:00:00', '2016-03-31 00:00:00', 'Cabby, F�rt�lt, Polar, Home-Car', '-', 'patrik', '-', '-', 'patrik', 'patrik', 23, 9, 1100);
INSERT INTO stores VALUES (11715, 'L�derups Husvagnstj�nst', 'info_loderupshusvagnstjanst.se@blocket.se', '0411-52 43 09', '0411-52 43 19', 'Storgatan 36', '276 40', 'L�DERUP', 'loderops.gif', 'Stort urval av b�de nya och begagnade husvagnar', 'L�derups Husvagnstj�nst ligger i Sk�ne
p� �sterlen, 5 km fr�n kusten mittemellan
Ystad och Simrishamn. 
H�r har vi s�lt och hyrt ut husvagnar mm
sedan 1972.
<p>
I v�r butik erbjuder vi Dig m�nga praktiska
tillbeh�r till husvagnen
<p>
Vi hj�lper �ven Dig som beh�ver reparera 
eller ha service p� husvagnen.
<p>
Hos oss hittar Du alltid ett stort urval av 
b�de nya och begagnade husvagnar med
passande f�rt�lt.', 'http://www.loderupshusvagnstjanst.se
', 'active', false, '2003-10-15 00:00:00', '2016-03-31 00:00:00', 'Cabby, F�rt�lt, Polar, Home-Car', '-', 'patrik', '-', '-', 'patrik', 'patrik', 23, 10, 1100);
INSERT INTO stores VALUES (11716, 'L�derups Husvagnstj�nst', 'info_loderupshusvagnstjanst.se@blocket.se', '0411-52 43 09', '0411-52 43 19', 'Storgatan 36', '276 40', 'L�DERUP', 'loderops.gif', 'Stort urval av b�de nya och begagnade husvagnar', 'L�derups Husvagnstj�nst ligger i Sk�ne
p� �sterlen, 5 km fr�n kusten mittemellan
Ystad och Simrishamn. 
H�r har vi s�lt och hyrt ut husvagnar mm
sedan 1972.
<p>
I v�r butik erbjuder vi Dig m�nga praktiska
tillbeh�r till husvagnen
<p>
Vi hj�lper �ven Dig som beh�ver reparera 
eller ha service p� husvagnen.
<p>
Hos oss hittar Du alltid ett stort urval av 
b�de nya och begagnade husvagnar med
passande f�rt�lt.', 'http://www.loderupshusvagnstjanst.se
', 'active', false, '2003-10-15 00:00:00', '2016-03-31 00:00:00', 'Cabby, F�rt�lt, Polar, Home-Car', '-', 'patrik', '-', '-', 'patrik', 'patrik', 23, 11, 1100);
INSERT INTO stores VALUES (11717, 'L�derups Husvagnstj�nst', 'info_loderupshusvagnstjanst.se@blocket.se', '0411-52 43 09', '0411-52 43 19', 'Storgatan 36', '276 40', 'L�DERUP', 'loderops.gif', 'Stort urval av b�de nya och begagnade husvagnar', 'L�derups Husvagnstj�nst ligger i Sk�ne
p� �sterlen, 5 km fr�n kusten mittemellan
Ystad och Simrishamn. 
H�r har vi s�lt och hyrt ut husvagnar mm
sedan 1972.
<p>
I v�r butik erbjuder vi Dig m�nga praktiska
tillbeh�r till husvagnen
<p>
Vi hj�lper �ven Dig som beh�ver reparera 
eller ha service p� husvagnen.
<p>
Hos oss hittar Du alltid ett stort urval av 
b�de nya och begagnade husvagnar med
passande f�rt�lt.', 'http://www.loderupshusvagnstjanst.se
', 'active', false, '2003-10-15 00:00:00', '2016-03-31 00:00:00', 'Cabby, F�rt�lt, Polar, Home-Car', '-', 'patrik', '-', '-', 'patrik', 'patrik', 23, 12, 1100);
INSERT INTO stores VALUES (11718, 'L�derups Husvagnstj�nst', 'info_loderupshusvagnstjanst.se@blocket.se', '0411-52 43 09', '0411-52 43 19', 'Storgatan 36', '276 40', 'L�DERUP', 'loderops.gif', 'Stort urval av b�de nya och begagnade husvagnar', 'L�derups Husvagnstj�nst ligger i Sk�ne
p� �sterlen, 5 km fr�n kusten mittemellan
Ystad och Simrishamn. 
H�r har vi s�lt och hyrt ut husvagnar mm
sedan 1972.
<p>
I v�r butik erbjuder vi Dig m�nga praktiska
tillbeh�r till husvagnen
<p>
Vi hj�lper �ven Dig som beh�ver reparera 
eller ha service p� husvagnen.
<p>
Hos oss hittar Du alltid ett stort urval av 
b�de nya och begagnade husvagnar med
passande f�rt�lt.', 'http://www.loderupshusvagnstjanst.se
', 'active', false, '2003-10-15 00:00:00', '2016-03-31 00:00:00', 'Cabby, F�rt�lt, Polar, Home-Car', '-', 'patrik', '-', '-', 'patrik', 'patrik', 23, 13, 1100);
INSERT INTO stores VALUES (11719, 'L�derups Husvagnstj�nst', 'info_loderupshusvagnstjanst.se@blocket.se', '0411-52 43 09', '0411-52 43 19', 'Storgatan 36', '276 40', 'L�DERUP', 'loderops.gif', 'Stort urval av b�de nya och begagnade husvagnar', 'L�derups Husvagnstj�nst ligger i Sk�ne
p� �sterlen, 5 km fr�n kusten mittemellan
Ystad och Simrishamn. 
H�r har vi s�lt och hyrt ut husvagnar mm
sedan 1972.
<p>
I v�r butik erbjuder vi Dig m�nga praktiska
tillbeh�r till husvagnen
<p>
Vi hj�lper �ven Dig som beh�ver reparera 
eller ha service p� husvagnen.
<p>
Hos oss hittar Du alltid ett stort urval av 
b�de nya och begagnade husvagnar med
passande f�rt�lt.', 'http://www.loderupshusvagnstjanst.se
', 'active', false, '2003-10-15 00:00:00', '2016-03-31 00:00:00', 'Cabby, F�rt�lt, Polar, Home-Car', '-', 'patrik', '-', '-', 'patrik', 'patrik', 23, 14, 1100);
INSERT INTO stores VALUES (11720, 'L�derups Husvagnstj�nst', 'info_loderupshusvagnstjanst.se@blocket.se', '0411-52 43 09', '0411-52 43 19', 'Storgatan 36', '276 40', 'L�DERUP', 'loderops.gif', 'Stort urval av b�de nya och begagnade husvagnar', 'L�derups Husvagnstj�nst ligger i Sk�ne
p� �sterlen, 5 km fr�n kusten mittemellan
Ystad och Simrishamn. 
H�r har vi s�lt och hyrt ut husvagnar mm
sedan 1972.
<p>
I v�r butik erbjuder vi Dig m�nga praktiska
tillbeh�r till husvagnen
<p>
Vi hj�lper �ven Dig som beh�ver reparera 
eller ha service p� husvagnen.
<p>
Hos oss hittar Du alltid ett stort urval av 
b�de nya och begagnade husvagnar med
passande f�rt�lt.', 'http://www.loderupshusvagnstjanst.se
', 'active', false, '2003-10-15 00:00:00', '2016-03-31 00:00:00', 'Cabby, F�rt�lt, Polar, Home-Car', '-', 'patrik', '-', '-', 'patrik', 'patrik', 23, 16, 1100);
INSERT INTO stores VALUES (11721, 'Bildelshuset AB', 'bildelshuset_telia.com@blocket.se', '0660-700 90', '0660-705 85', 'V�gersta', '894 93', '�VERH�RN�S', 'logga_butik.gif', 'Bildelar & Tillbeh�r', 'V�lkomna till Bildelshuset! F�retaget har funnits i m�nga �r, �nda sedan
b�rjan av 1970-talet. �r 2003 �vertogs det av Mikael Danielsson.
F�retaget har tv� anst�llda och vi sysslar mestadels med bildemontering d�r
vi skrotar bilar och s�ljer de begagnade delarna. Vi s�ljer �ven nya delar
till bilar, s� som pl�t, lyktor, kof�ngare och kylare. Vid behov kan vi �ven
h�mta bilar med v�r b�rgningsbil.
<p>
En annan del av v�r verksamhet �r f�rs�ljning av diverse
entreprenadmaskiner, t.ex. lastmaskiner.
<p>
Vi �r milj�certifierade och medlemmar i SBR, Sveriges Bilskrotares
Riksf�rbund.
<p>
�ppettider: <br>
M�n, Tis 08.00-17.00<br>
Ons 08.00-19.00<br>
Tor 08.00-17.00<br>
Fre 08.00-13.00
', 'http://www.bildelshuset.com
', 'active', false, '2003-10-17 00:00:00', '2016-02-28 23:59:59', 'begagnade d�ck, d�ck, bildelar, reservdelar, ekonomipl�t, pl�t, lyktor,
kof�ngare, entreprenadmaskiner

', '-', 'patrik', '-', '-', 'patrik', 'patrik', 4, 0, 1040);
INSERT INTO stores VALUES (11728, 'Hammarskrot Bildemontering AB', 'info_hammarskrot.se@blocket.se', '0290-377 22', '0290-76 60 06', 'Dals�ngsv�gen 65', '812 93', 'KUNGSG�RDEN', 'hammarskrot_logo.gif', 'Bildelar & Tillbeh�r', '<b>V�lkommen till Hammarskrot Bildemontering</b><p>

Hammarskrot Bildemontering �r en auktoriserad bildemontering. Vi har f�rs�ljning av begagnade bildelar och nya ekonomipl�tdetaljer, kylare och tankar. <br>
Vi s�ljer �ven bilbelysning. Allt detta tas hem p� best�llning.
<p>
Vi erbjuder h�mtning av skrotbilar samt bildemontering.
<p>
Vi finns p� Dals�ngsv�gen 65 i Backberg, Kungsg�rden.<br>
Ett litet samh�lle som ligger strax v�ster om Sandviken i G�strikland.
<p>
<b>Just nu betalar vi upp till 700:- f�r din skotbil!</b><p>


<b>V�ra �ppettider �r:</b><br>
M�ndag � Onsdag 9.00-17.00<br>
Torsdag 9.00 � 19.00<br>
Fredag 9.00-15.00<p>

Har du n�gra fr�gor �r du v�lkommen att kontakta oss!

', 'http://www.hammarskrot.se', 'active', false, '2003-10-22 00:00:00', '2016-10-31 23:59:59', 'Bildelar, Biltillbeh�r, Reservdelar', '-', 'patrik', '-', '-', 'patrik', 'patrik', 5, 0, 1040);
INSERT INTO stores VALUES (11730, 'LORKE-MC', 'rainer.lorke_telia.com@blocket.se', '0703-93 31 14', '-', 'Vackamo 101', '361 94', 'ERIKSM�LA', 'lorke_sok031026.gif', '50 Begagnade Mc i lager', 'Nya motorcyklar varje fredag.
<p>
Transport: Det finns m�jlighet att f� transport av MC till norra Sverige till l�gt pris. Lastbil g�r upp�t landet varje vecka. Pris exempelvis till Pite�: 1500 kronor. 
<p>
Garanti: Alla fordon har tre m�naders garanti (max 300 mils k�rst�cka). Garanti g�ller p� platsen. 
<p>
Leveranstid p� best�llda fordon
Mellan sju och fjorton dagar fr�n best�llningsdagen. 
<p>
Inga dolda kostnader: Det angivna priser avser k�rklara fordon som �r svenskregistrerade och besiktigade. Det tillkommer inga avgifter f�r k�paren s�som tull, accis eller andra ov�ntade utgifter. 

', 'http://www.lorke-mc.com
', 'active', false, '2003-10-26 00:00:00', '2015-12-31 23:59:59', 'BMW, Yamaha, Suzuki, Kawasaki, Honda', '-', 'patrik', '-', '-', 'patrik', 'patrik', 18, 0, 1140);
INSERT INTO stores VALUES (11910, 'Olofssons i Syd AB', 'ulf_olofssonsisyd.se@blocket.se', '0454-32 12 40', '0454-847 66', 'Giselbergsv. 246-8', '374 50', 'ASARUM', 'olofsyd_logo.gif', 'Det �r vi som importerar och s�ljer nya Bobcatmaskiner, redskap, reservdelar och tillbeh�r!', 'Hos oss finner Du ett komplett program av Bobcat kompaktlastare, minigr�vare, teleskoplastare, hydraulhammare, vibroplattor. Kawasaki 4-hjulingar, Mulefordon med vagnar och alla tillbeh�r, RESERVDELAR samt mycket mer!
<p>
Vi har �ven en uthyrningsverksamhet av redskap till Bobcat kompaktlastare, samt projektuthyrning av entreprenadmaskiner och terr�ngfordon.
<p>
Vi har st�ndigt behov av bra beg. maskiner, vill Du s�lja din kompaktmaskin eller 4-hjuling? �terkom med maskindata och g�rna bild s� l�mnar vi ett seri�st bud.  
<p>
�ppettider �r:<br>
Vardagar 8-17<br>
Onsdagar kv�lls�ppet till 19 eller enligt �verenskommelse.', 'http://www.bobman.se/', 'active', false, '2003-12-29 00:00:00', '2016-07-31 23:59:59', 'Bobcat, Kompaktlastare, Kompressorer, Reservdelar, Teleskoplastare, V�ltar, Vibro, Utbildning, Minigr�vare, Redskap, Uthyrning 
', '-', 'patrik', '-', '-', 'patrik', 'patrik', 22, 0, 1220);
INSERT INTO stores VALUES (11922, 'Bengt Bergs Maskinservice AB', 'jorgen_bbm.se@blocket.se', '031-25 10 55', '031-19 60 72', 'Frukthallen/Partihallarna', '400 13', 'G�TEBORG', 'logo.gif', 'Din personliga maskinleverant�r!', 'V�lkommen till Bergs Maskinservice
Vi s�ljer och markandsf�r redskapasb�raren Wille och p� senare �r �ven minidumpern Canycom som genom �ren hj�lp m�nga i tr�nga utrymmen.
<p>
Vi �r ett f�rs�ljningsbolag inom entreprenad, sk�tsel och underh�llssektorn.
<p>
V�r verksamhet best�r av att analysera Era maskinbehov och anpassa maskin och redskap efter kundes behov och �nskem�l. Bakom oss har vi en av Nordens b�sta leverant�rer av redskapsb�rare (Vilakone OY).
<p>
F�retaget ing�r �ven i Willegruppen i Sverige, vilket medf�r att vi med hj�lp av v�r gemensamma erfarenhet och Willes breda maskinprogram kan leverera �r�tt� maskin till Er som anv�ndare.
<p>
V�r aff�rsid� �r, att med en f�r v�r bransch unikt koncept med enbart f�rs�ljning av redskapsb�rare och redskap som ger oss spjutspetskompetens inom omr�det.
<p>
Service och reservdelar till v�ra maskiner tillhandah�lls av lokala serviceverkst�der som vi samarbetar med n�ra Er.
<p>
F�retaget grundades �r 1992 och har kontor i G�teborg.
<p>
Vill ni maila till oss s� �r adresserna:
<br>
<b>jorgen_bbm.se</b> och/eller <b>bengt@bbm.se</b>
<p>
Med v�nliga h�lsningar
Bengt Berg med personal@blocket.se', 'http://www.bbm.se/', 'active', false, '2004-01-30 12:00:00', '2016-04-15 23:59:59', 'Skopor, Sandspridare, Gr�sklippare, Personlift, H�gtryckstv�tt, Sn�blad, Vikplog, Tallriksspridare, Sopmaskiner. Lastmaskiner, Lundberg, Wille, Traktorer, Redskap, Minidumpers, Redskapsb�rare, Minidumper, Vilakone, Canycom, Chikusui', '-', 'patrik', '-', '-', 'patrik', 'patrik', 15, 0, 1220);
INSERT INTO stores VALUES (12038, 'AB Kristdala Maskinaff�r', 'jerker_krimamaskin.se@blocket.se', '0491-704 75', '0491-707 95', 'D�mv�gen 29', '570 91', 'KRISTDALA', 'krima2.gif', 'Maskiner f�r Jord, Skog och Tr�dg�rd.', 'Vi s�ljer maskiner f�r Jord, Skog och Tr�dg�rd. V�lsorterat reservdels- och butiksortiment. Reparationsverkstad f�r lantbruksmaskiner och bilar. Bensinstation.', 'http://www.krimamaskin.se', 'active', false, '2003-12-12 12:15:00', '2016-02-15 23:59:00', 'Traktor, Bilar, Hydro, Jonsered, S�derberg & Haak, Bombardier, New Holland, FMV, KMA, Partner, Olsson i Ell�s, Grene, Foga, Mekonomen, Skogsmaskiner, Lantbruksmaskiner, Fyrhjulingar, Fyrhjuling, Aeon, Nusun', '-', 'patrik', '-', '-', 'patrik', 'patrik', 18, 0, 1220);
INSERT INTO stores VALUES (12039, 'R�dja Mekaniska AB', 'rodja_tiscali.se@blocket.se', '0380-222 20', '0380-221 11', 'R�dja', '571 94', 'N�SSJ�', 'logo.gif', 'Hos oss �r kunden alltid i centrum!', 'R�dja Mekaniska AB har tillverkat, importerat och s�lt lantbruksmaskiner i snart 40 �r!
<p>
V�r m�ls�ttning �r att erbjuda kvalitetsmaskiner till l�ga priser.
<p>
Vi tillhandah�ller reservdelar till ett stort antal maskiner med snabba leveranser.', 'http://home.tiscali.se/rodja/', 'active', false, '2003-12-04 09:00:00', '2016-06-10 23:59:59', 'Avlastarbord, Betesputsare, Fodermixer, G�dselspridare, Spaltmixer, Silof�rdelare, Silospridare, Halmhack, Rotorsl�tter, Sl�ttermaskin, Sj�lvlastarvagn, S�maskin, Strautmann, Kaweco, R�dja, Reck,', '-', 'patrik', '-', '-', 'patrik', 'patrik', 17, 0, 1220);
INSERT INTO stores VALUES (12053, 'H�glandets Maskin F�rs�ljnings AB', 'info_hoglandetsmaskin.se@blocket.se', '0380-478 50', '0380-478 59', 'J�rnv�gsgatan 15', '578 24', 'ANEBY', 'logo.gif', 'Det naturliga valet!', 'H�glandets Maskin F�rs�ljnings AB:s aff�rsid� �r att inom ett geografiskt n�romr�de erbjuda kunder h�gklassiga produkter inom skog, lantbruk och entreprenad vilket tillsammans med en v�l utbyggd service skall borga f�r h�gsta kvalitet.
<p>
I dag finns H�glandets Maskin representerat p� fyra orter � Aneby, Huskvarna, Vetlanda och Vimmerby � med sammanlagt ett 25-tal anst�llda. P� samtliga orter finns f�rs�ljning, service och reservdelar. ', 'http://www.hoglandetsmaskin.se/', 'active', false, '2003-12-03 10:50:00', '2016-01-31 23:59:59', 'traktor, traktorer, skogsmaskiner, entreprenadmaskiner, ', '-', 'patrik', '-', '-', 'patrik', 'patrik', 17, 0, 1220);
INSERT INTO stores VALUES (12147, 'Str�mvalls Maskin', '-', '019-22 80 75', '019-22 80 03', 'T�by 316', '705 94', '�rebro', '-', '-', '-', '-', 'active', false, '2003-11-01 00:00:00', '2015-12-31 00:00:00', 'traktor, traktorer, begagnade delar, begagnade traktordelar
', '-', 'patrik', '-', '-', 'patrik', 'patrik', 8, 0, 1220);
INSERT INTO stores VALUES (12232, 'Thore Bostr�m Maskin AB', 'info_tbm.se@blocket.se', '026-101250', '026-14 00 94', 'R�lsgatan 6', '800 06', 'G�vle', 'thorbostrom.gif', 'Handel, f�rmedling, service och reparation', 'Thore Bostr�m Maskin AB �r ett f�retag vars verksamhet grundar sig i handel, f�rmedling, service och reparation av redskapsb�rare, gr�vmaskiner och begagnade inbytesmaskiner. Vi innehar spetskunskap inom omr�det och har l�ng erfarenhet av branschen.', 'http://www.tbm.se', 'active', false, '2005-10-17 00:00:00', '2015-11-30 23:00:00', 'gr�vmaskin, gr�vmaskiner, redskapsb�rare,', '-', 'patrik', '-', '-', 'patrik', 'patrik', 5, 0, 8040);
INSERT INTO stores VALUES (12239, 'Wij Maskincenter AB', 'solve.rosen_wijmaskincenter.se@blocket.se', '0297-405 94', '0297-411 97', 'Br�msv�gen 17', '816 30', 'OCKELBO', 'logo.gif', 'Din Servicepartner!', 'Wij Maskincenter �r k�nd som verkstaden som klarar det mesta. F�retaget erbjuder service och reparationer inne p� verkstad samt ute i f�lt med fullt utrustade servicebilar och v�ra h�gkompetenta mekaniker.
<p>
Vi �r auktoriserad servicel�mnare f�r Valmet skogsmaskiner. 
<p>
F�retaget startades 1999 och �gs till 100 % av S�lve Ros�n, syssels�tter 13 anst�llda och bedriver verksamheten i egen fastighet p� Br�msv�gen 17 i Ockelbo. ', 'http://www.wijmaskincenter.se', 'active', false, '2004-01-30 12:55:00', '2016-04-01 23:59:59', 'Skogsmaskin, Skogsmaskiner, Husqvarna, Viking, Industri, Entrepenad, Valmet,', '-', 'patrik', '-', '-', 'patrik', 'patrik', 5, 0, 1220);
INSERT INTO stores VALUES (12651, 'Westerdahls Husvagnscenter AB', 'jan_westerdahls.com@blocket.se', '019-25 25 20', '019-25 25 21', 'Dialoggatan 3', '703 74', '�REBRO', 'westerdahls_logo.gif', 'Vi s�ljer nya och begagnade husvagnar samt tillbeh�r.', 'Westerdahls Husvagnscenter AB startade 1982 av nuvarande �garen Jan Westerdahl. 
F�retaget och dess utst�llningslokal ligger p� Vivallaomr�det i �rebro.
<p>
Utst�llningshallen byggdes i egen regi 1992. D�r finns nya vagnar; Cabby, Polar och Hobby samt en stor sortering begagnade husvagnar. 
Vi h�ller noggrann kontroll p� alla vagnar vi hanterar, s� att vi uppfyller de trygghetskrav som finns p� marknaden. 
<p>
Vi har �ven stor sortering av f�rt�lt, reservdelar och tillbeh�r samt en verkstad i samma lokaler. 
Westerdahls hyr �ven ut husvagnar p� kort eller l�ng tid.
<p>
V�lkomna att ringa eller bes�ka oss.', 'http://www.westerdahls.com/', 'active', false, '2004-05-07 00:00:00', '2016-06-01 23:59:59', 'Husvagn, Husvagnar, Cabby, Polar, Hobby, Eifelland, Kabe, Solifer,', '-', 'patrik', '-', '-', 'patrik', 'patrik', 8, 0, 1100);
INSERT INTO stores VALUES (12281, 'Ramsele Maskin', 'info_ramselemaskin.com@blocket.se', '0620-174 00', '0620-174 40', 'L�ngatan 1', '881 33', 'SOLLEFTE�', 'RamseleMaskin_logo.gif', 'Alla maskiner finns p� plats i Sollefte�', '<li>Hjullastare
<li>Traktorgr�vare
<li>Jordbrukstraktorer
<li>Redskap
<p>
- Alla maskiner finns p� plats i Sollefte�-

', 'http://www.ramselemaskin.com', 'active', false, '2003-11-05 00:00:00', '2015-12-31 00:00:00', 'BM, Huddig, Hymas, Pel-Job, DB, Plog, Pallgaffel, Sn�blad', '-', 'patrik', '-', '-', 'patrik', 'patrik', 4, 0, 1220);
INSERT INTO stores VALUES (12648, 'Lars Levin Bil AB', 'info.levinsbil_telia.com@blocket.se', '0370 - 474 20', '0370 - 135 63', 'J�nk�pingsv�gen 65', '331 34', 'V�RNAMO', 'logga.gif', '�terf�rs�ljare f�r BMW, Mazda', '�r du intresserad av en ny eller begagnad BMW �r det h�r ett bra s�tt att b�rja. H�r hittar du information om v�rt f�retag, var vi finns och hur du kommer i kontakt med oss. 
<p>
Du kan ocks� s�ka bland de begagnade bilar vi har i lager, som i de flesta fall presenteras med bild. Hittar du n�gon bil som intresserar dig kan du enkelt anm�la ditt intresse direkt h�r p� v�r hemsida. Du har h�r ocks� m�jlighet att kontakta oss i andra fr�gor. 
<p>
F�rutom att ge dig st�d kring ditt bilk�p kan vi sj�lvklart erbjuda dig service, reservdelar och tillbeh�r. 
<p>
Ord och bild �r inte alltid tillr�ckligt f�r att beskriva en BMW. D�rf�r inbjuder vi till en personlig kontakt med bilen.', 'http://www.levinsbil.se', 'active', false, '2004-05-05 00:00:00', '2016-04-01 23:59:59', 'BMW, Audi, Ford, Mazda, Mercedes, Peugeot, Porsche, Saab, Volkswagen, Volvo, Chrysler, Nissan, Opel, Renault, ', '-', 'patrik', '-', '-', 'patrik', 'patrik', 17, 0, 1020);
INSERT INTO stores VALUES (12324, 'Bil & Husvagnshandlar�n i �rebro', 'bilohusvagn_telia.com@blocket.se', '019-23 20 80', '019-23 20 81', 'K�viv�gen 26', '703 75', '�rebro', 'handlarn.gif', 'K�per, byter, s�ljer', 'De flesta husvagnarna visas inomhus!
<p>
�ppet tillsvidare<br>
M�n - Tors 10:00 - 17:00<br>
Fre 10:00 - 16:00
<br>
Lunchst�ngt 13:00 - 14:00<br>
L�rdag 10.00 - 13.00
<br>
S�ndag  ST�NGT
<p>
V�lkomna!
', 'http://www.bilohusvagnshandlarn.com
', 'active', false, '2003-11-03 00:00:00', '2016-03-10 23:59:59', 'Adria, Cabby, Cavaljer, Hobby, KABE, Polar, Star', '-', 'patrik', '-', '-', 'patrik', 'patrik', 8, 0, 1100);
INSERT INTO stores VALUES (12325, 'Redox Bilfarm AB', 'info_redox.se@blocket.se', '0303-33 46 90', '0303-33 88 20', 'Knuts v�g 14', '446 41', 'Skepplanda', 'redox.gif', 'Bildelar, tillbeh�r & demontering', 'Vi h�ller till ca 4mil norr om G�teborg i Ale kommun p� Skepplanda industriomr�de d�r vi varit sedan 1993.
<p>
Vi �r nu 13 anst�llda som som enbart arbetar med att demontera och s�lja demonterade delar.
<p>
N�stan alla delarna i v�rt lager �r kvalitets klassade enligt SBR:s kvalitets klasser.
<p>
Redox milj�hantering best�r av tv� grenar plastreparationer och d�ckhantering.
', 'http://www.redox.se', 'active', false, '2003-11-03 00:00:00', '2015-12-31 23:00:00', 'ALFA, 
AUDI, 
BMC, 
BMW, 
CHRYSLER, 
CITROEN, 
DAEWOO, 
DAIHATSU, 
DODGE, 
FIAT, 
FORD, 
GM, 
HONDA, 
HYUNDAI, 
ISUZU, 
IVECO, 
JEEP, 
KIA, 
LADA, 
LANCIA, 
LANDROVER, 
MASERATI, 
MAZDA, 
MERCEDES, 
MITSUBISHI, 
NISSAN, 
OPEL, 
PEUGEOT, 
PIAGGIO, 
PORSCHE, 
RENAULT, 
ROVER, 
SAAB, 
SEAT, 
SKODA, 
SMART, 
SUBARU, 
SUZUKI, 
TOYOTA, 
VAZ, 
VOLVO, 
VW
', '-', 'patrik', '-', '-', 'patrik', 'patrik', 15, 0, 1040);
INSERT INTO stores VALUES (12326, 'Redox Bilfarm AB', 'info_redox.se@blocket.se', '0303-33 46 90', '0303-33 88 20', 'Knuts v�g 14', '446 41', 'Skepplanda', 'redox.gif', 'Bildelar, tillbeh�r & demontering', 'Vi h�ller till ca 4mil norr om G�teborg i Ale kommun p� Skepplanda industriomr�de d�r vi varit sedan 1993.
<p>
Vi �r nu 13 anst�llda som som enbart arbetar med att demontera och s�lja demonterade delar.
<p>
N�stan alla delarna i v�rt lager �r kvalitets klassade enligt SBR:s kvalitets klasser.
<p>
Redox milj�hantering best�r av tv� grenar plastreparationer och d�ckhantering.
', 'http://www.redox.se', 'active', false, '2003-11-03 00:00:00', '2015-12-31 23:00:00', 'ALFA, 
AUDI, 
BMC, 
BMW, 
CHRYSLER, 
CITROEN, 
DAEWOO, 
DAIHATSU, 
DODGE, 
FIAT, 
FORD, 
GM, 
HONDA, 
HYUNDAI, 
ISUZU, 
IVECO, 
JEEP, 
KIA, 
LADA, 
LANCIA, 
LANDROVER, 
MASERATI, 
MAZDA, 
MERCEDES, 
MITSUBISHI, 
NISSAN, 
OPEL, 
PEUGEOT, 
PIAGGIO, 
PORSCHE, 
RENAULT, 
ROVER, 
SAAB, 
SEAT, 
SKODA, 
SMART, 
SUBARU, 
SUZUKI, 
TOYOTA, 
VAZ, 
VOLVO, 
VW
', '-', 'patrik', '-', '-', 'patrik', 'patrik', 16, 0, 1040);
INSERT INTO stores VALUES (12333, 'Ringqvist Bil & MC', 'ringqvist.bil.mc_telia.com@blocket.se', '0501-180 40', '0501-129 89', 'F�rr�dsgatan 41', '542 35', 'Mariestad', 'ringqvist.gif', 'M�rkesoberoende handlare', 'Det �r h�r du kan g�ra ditt livs b�sta bilk�p. Hos oss f�r du den b�sta t�nkbara service, r�d och kunskap Man kan t�nka sig inom bilbranshen, nytt som begagnat.
<p>
Vi �r s�kra p� att om du en g�ng handlat hos oss kommer du inte att vilja handla av n�gon annan handlare i framtiden. Men varf�r skulle du vilja det d� vi alltid str�var efter att vara den b�sta och enda bilhandlaren f�r dig, gammal som ung!
<p>
V�nligen RING f�r snabbare kontakt <p>', 'http://www.ringqvist.nu', 'active', false, '2003-11-05 00:00:00', '2016-12-01 23:59:59', 'Aeon, Aprilla, Honda, Kawasaki, Piaggio, Suzuki, Yamaha, ATV50, ATV90T, Cobra, Silverstone, XP50, Leonardo, CB500, CBR1000F, CM400T, NS400R, NSR125, NX650, Dominator, VT600, Shadow, VT600Shadow, XRV750AfricaTwin, XRV750, AfricaTwin, VN800Vulcan, VN800, Vulcan, Typhoon, GT125X, VS800Intruder, VS800, Intruder, VZ800Marauder, VZ800, Marauder, FJ1200, XTZ660, Tenere, XTZ750SuperTenere, XTZ750, SuperTenere, XV1100Virago, XV1100, Virago, YZF750R
', '-', 'patrik', '-', '-', 'patrik', 'patrik', 13, 0, 1140);
INSERT INTO stores VALUES (12336, 'J.A Car Care', 'ja_carcare_telia.com@blocket.se', '0455-201 71', '-', 'Verkstadsgatan 6', '371 47', 'KARLSKRONA', 'jacarcare.gif', 'Helrekonditionering, lackkonservering, kl�dseltv�tt, mm.', '<ul>
<li>Ditec lackkonservering</li>
<li>Helrekonditionering</li>
<li>Kl�dseltv�tt</li>
<li>Gastec f�r rutorna</li>
<li>Protec f�r kl�dseln</li>
<li>Vindrutereparationer</li>
<li>Luktsanering</li>
<li>AC-saneringar</li>
</ul>
<p>
DITEC lackkonserveringssystem �r ett komplett l�ngtidsverkande lackskyddssystem f�r nya eller nyare bilar. DITEC �r v�l bepr�vat sedan -70 talet och �r testat av StatensProvningsanstalt. DITEC utf�res endast av utbildade yrkesm�n.
Alla produkterna �r av h�gsta klass och helt silikonfria. Med DITECS lackkonservering beh�ver bilen aldrig mer vaxas! 
<p>
<b>Vi har l�nebilar till en mindre kostnad.</b>
<p>
V�lkomna!
 ', 'http://www.ja-carcare.com', 'active', false, '2003-11-07 00:00:00', '2016-02-28 23:59:59', 'Ditec', '-', 'patrik', '-', '-', 'patrik', 'patrik', 22, 0, 1040);
INSERT INTO stores VALUES (12337, 'Ramsele Maskin', 'info_ramselemaskin.com@blocket.se', '0620-174 00', '0620-174 40', 'L�ngatan 1', '881 33', 'SOLLEFTE�', 'RamseleMaskin_logo.gif', 'Alla maskiner finns p� plats i Sollefte�', '<li>Hjullastare
<li>Traktorgr�vare
<li>Jordbrukstraktorer
<li>Redskap
<p>
- Alla maskiner finns p� plats i Sollefte�-

', 'http://www.ramselemaskin.com', 'active', false, '2003-11-05 00:00:00', '2015-12-31 00:00:00', 'BM, Huddig, Hymas, Pel-Job, DB, Plog, Pallgaffel, Sn�blad', '-', 'patrik', '-', '-', 'patrik', 'patrik', 3, 0, 1220);
INSERT INTO stores VALUES (12338, 'Ramsele Maskin', 'info_ramselemaskin.com@blocket.se', '0620-174 00', '0620-174 40', 'L�ngatan 1', '881 33', 'SOLLEFTE�', 'RamseleMaskin_logo.gif', 'Alla maskiner finns p� plats i Sollefte�', '<li>Hjullastare
<li>Traktorgr�vare
<li>Jordbrukstraktorer
<li>Redskap
<p>
- Alla maskiner finns p� plats i Sollefte�-

', 'http://www.ramselemaskin.com', 'active', false, '2003-11-05 00:00:00', '2015-12-31 00:00:00', 'BM, Huddig, Hymas, Pel-Job, DB, Plog, Pallgaffel, Sn�blad', '-', 'patrik', '-', '-', 'patrik', 'patrik', 2, 0, 1220);
INSERT INTO stores VALUES (12341, '�sthammars Reserv- & Motordelar AB', '-', '0173-104 25', '0173-171 75', 'Slottsgatan 18 A', '742 31', '�STHAMMAR', 'logo.gif', 'Reserv- och motordelar', '<li> F�rs�ljning av reservdelar och tillbeh�r till de flesta bilar. 
<li> Blandning av f�rg, �ven spray. 
<li> Tillverkning av bromsr�r 
<li> Reservdelar till de flesta sl�pvagnar. 
<li> Thule lasth�llare och boxar 
<li> Bosch och Hella belysning, extraljus, arbetsbelysning 
<li> Hagmans bilv�rdsprodukter 
<li> Walker avgassystem 
<li> Rydahls & ABR bromsdelar 
<li> Mann filter 
<li> NGK T�ndstift 
<li> VDO instrument & bilstereo 
<li> Tridon framvagn 
<li> Monroe st�td�mpare 
<li> Sachs st�td�mpare & kopplingar 
<li> Gete motordelar 
<li> Tudor batterier 
<li> Huzells tillbeh�r 
<li> Midland & Valvoline oljor 
<li> Bosch torkarblad 
<li> Kamasa verktyg 
<li> Philips gl�dlampor ', 'http://www.motorbilreservdelar.com', 'active', false, '2003-12-09 10:45:00', '2016-04-30 23:59:59', 'Thule, Hella, Hagmans, Bosal, Rydahls, Mann, NGK, VDO, Tidon, Monroe, Sachs, Gete, Banner, Huzells, Duckhamns, SWF, Kamasa, Philips', '-', 'patrik', '-', '-', 'patrik', 'patrik', 10, 0, 1040);
INSERT INTO stores VALUES (12392, 'Redskaparna AB', 'redskaparna_telia.com@blocket.se', '0270-28 73 50', '0270-28 72 50', 'Sockenv�gen 31', '826 61', 'S�DERDALA', 'redskaparna.gif', 'Av oss k�per ni kompetens, erfarenhet och design', 'Vi tillverkar redskap f�r entreprenadmaskiner, s�v�l standardredskap som specialredskap, allt efter kundens �nskem�l.
<p>
Vi f�r en st�ndig produktutveckling f�r att m�ta marknadens krav. Vi hj�lper dig g�rna med b�sta l�sningen f�r ditt behov.
<p>
De produkter vi erbjuder skall fylla de behov kunden har f�r effektiv materialhantering. V�ra produkter uppfyller krav p� anv�ndarv�nlighet, s�kerhet och kvalit�.
<p>
Kvaliteten och slitstyrkan hos v�ra produkter ger optimal drifts�kerhet och god totalekonomi. S� varf�r chansa p� annat h�ll, n�r det finns en leverant�r i en klass f�r sig.

', 'http://www.redskaparna.com', 'active', false, '2003-11-11 00:00:00', '2015-12-31 23:00:00', 'Plogar, Sn�plogar, Skopor, Gr�vskopor, Sandspridarskopor, H�gtippande
skopor, Pallgafflar, Gafflar, Kranarmar, L�ttmatrialskopor, Planeringsskopor, Grusskopor, Gr�vlastare, Gr�vutrustning', '-', 'patrik', '-', '-', 'patrik', 'patrik', 5, 0, 1220);
INSERT INTO stores VALUES (12397, 'Olofsson Auto AB', 'bilsalong_olofssonauto.com@blocket.se', '011-36 95 70', '011-14 25 08', 'Navestadsgatan 43', '-', 'NORRK�PING', 'olofsson_logo.gif', '�terf�rs�ljare f�r Chrysler, Jeep, Mercedes-Benz', 'Vissa bilar yngre �n 10 �r och som g�tt mindre �n 17000 mil kan f�s med 12 m�n elektronik- och maskinskadegaranti via AB Svensk Bilgaranti.<p>
Se f�rtydligande p� v�r hemsida www.olofssonauto.com under Bilgaranti.<p>
Vi resarverar oss f�r felskrivningar i annonserna. <p>
�ppetider vardagar 08.00-18.00 l�rdag 10.00-14.00 
', 'http://www.olofssonauto.com
', 'active', false, '2005-10-19 00:00:00', '2016-01-31 23:59:59', 'Chrysler, Jeep, Mercedes-Benz, Audi, BMW, Cadillac, Chevrolet, Ford, Jeep Cherokee, Saab, Skoda, Volkswagen, VW, Volvo', '-', 'patrik', '-', '-', 'patrik', 'patrik', 14, 0, 1020);
INSERT INTO stores VALUES (14438, 'GT Service', 'info_lada.nu@blocket.se', '0459-860 08', '0459-860 09', '�lshults Skola', '360 10', 'RYD', 'butik.jpg', '�terf�rs�ljare f�r Lada', 'S�ljer Nya Lada bilar i S�dra Sverige.<br>
F�r mer info g� in p� www.lada.nu <br>
Hemsida: www.datacafe.se/gtservice 

<p>
V�ra bilar har 2 �rs fabriksgaranti, och 10 �rs rostskyddsgaranti.<br>
Vi �r auktoriserade Lada �terf�rs�ljare. 
', 'http://www.lada.nu

', 'active', false, '2005-10-19 00:00:00', '2016-01-31 23:59:59', 'Lada, Alfa Romeo, BMW, Chrysler, Skoda, Volkswagen, VW, Volvo', '-', 'patrik', '-', '-', 'patrik', 'patrik', 21, 0, 1020);
INSERT INTO stores VALUES (12414, 'Johansson Maskin AB', 'calesi_johanssonmaskinkl.se@blocket.se', '0935-203 70', '0935-208 49', 'F�retagsv.11', '911 35', 'V�NN�SBY', 'johanssons.GIF', 'SKOTRAR, ATV, SKOG & TR�DG�RD', 'KL Johansson Maskinf�rs�ljning AB fick sin nuvarande form 1992. Men f�retaget har funnits sedan 1979. 
<p>
Vi bedriver f�rs�ljning och service med nya och begagnade lantbruks entreprenad och tr�dg�rdsmaskiner samt sn�skotrar och ATV:s. 
<p>
Vi har �ven en Butik med ett brett tillbeh�rssortiment f�r villa, tr�dg�rd, lantbruk och skoter.', 'http://www.johanssonmaskinkl.se', 'active', false, '2003-11-21 00:00:00', '2015-12-31 23:00:00', 'Lynx,Entreprenadmaskiner, Lantbruksmaskiner, tr�dg�rdsmaskiner, Masset
Ferguson, Fendt, Nokka, Tume, trejon, Pottinger, Welger, Lely, Villab,
Stihl, Viking, Husqvarna, Bombardier.

', '-', 'patrik', '-', '-', 'patrik', 'patrik', 2, 0, 1220);
INSERT INTO stores VALUES (12418, 'Halmstad Bilcity AB', 'info_halmstadbilcity.nu@blocket.se', '035-135 135', '035-135 136', 'Kristinehedsv�gen 5', '302 44', 'Halmstad', 'halmstadbilcity.gif', 'Auktoriserad �terf�rs�ljare f�r Mitsubishi', 'Vi �r auktoriserad �terf�rs�ljare f�r Mitsubishi�s breda nybilsprogram.
V�r m�ls�ttning �r att erbjuda v�ra kunder fina bilar till ett fr�n b�rjan
l�gt pris, samt en personlig service.
<p>
Vi utf�r service och reparationer i v�r serviceverkstad.
<p>
V�lkomna till Halmstad Bilcity
', 'http://www.halmstadbilcity.nu
', 'active', false, '2003-11-24 14:55:00', '2016-01-18 00:00:00', 'Mitsubishi', '-', 'patrik', '-', '-', 'patrik', 'patrik', 20, 0, 1020);
INSERT INTO stores VALUES (12419, 'Laholms Bil AB', 'info_laholmsbil.nu@blocket.se', '0430-140 00', '0430-140 01', 'Industrigatan 33', '312 22', 'Laholm', 'laholms_bil.gif', 'Auktoriserad �terf�rs�ljare f�r Mitsubishi', 'Vi �r auktoriserad �terf�rs�ljare f�r Mitsubishi�s breda nybilsprogram. Vi
finns i nya fr�scha lokaler, ett stenkast fr�n E6:an. 
<p>
V�r m�ls�ttning �r att
erbjuda v�ra kunder fina bilar till ett fr�n b�rjan l�gt pris, samt en
personlig service. Endast genom ett bes�k kan ni se om vi lever upp till
v�rt l�fte. 
<p>
V�lkomna till Laholms Bil.
', 'http://www.laholmsbil.nu', 'active', false, '2003-11-24 14:56:00', '2016-01-18 00:00:00', 'Mitsubishi', '-', 'patrik', '-', '-', 'patrik', 'patrik', 20, 0, 1020);
INSERT INTO stores VALUES (12420, 'AM Bilar AB', 'ambil_ambil.nu@blocket.se', '036 - 71 00 43', '036 - 71 05 63', 'Gev�rsgatan 23', '554 53', 'J�nk�ping', 'ambilar.gif', 'M�rkesoberoende handlare', 'Vi har inriktat oss p� specialbilar och har �ver 
30 �rs erfarenhet av branschen. 
Finner Ni inte vad Ni s�ker efter? 
Vi h�mtar �ven hem bilar p� best�llning... 
<p>
F�r att skapa trygghet hos kunden M-testas 
bilar p� Motorm�nnens Riksf�rbund om s� �nskas.
<p>
V�lkommen att kontakta oss p� AM Bilar AB.
<p>
V�ra �ppettider: <br>
M�n - Fre: 10.00 - 18.00  <br>
L�rdag: 10.00 - 13.00 <br>
�vrig tid enligt �verenskommelse <br>
<p>
Mobnr: 0708-437669', 'http://www.ambilar.nu/', 'active', false, '2003-04-01 00:00:00', '2016-04-01 23:59:59', 'Audi, BMW, Honda, Jaguar, Mercedes, Renault, Volvo', '-', 'patrik', '-', '-', 'patrik', 'patrik', 17, 0, 1020);
INSERT INTO stores VALUES (13322, 'Nilssons Bil i Pukavik AB', 'info_nilssonsbil.se@blocket.se', '0454-52251', '0454-52251', 'Gustavstorpsv�gen 691', '375 91', 'M�RRUM', 'NilssonsBilPukavik_logo.gif', 'M�rkesoberoende handlare', '<B>V�rt m�l �r</B><br>
N�jda kunder st�r f�r kvalitet & k�ptrygghet<P>
Mobilnummer 070-292 84 47.', 'http://www.nilssonsbil.se

', 'active', false, '2004-11-09 09:00:00', '2016-05-15 23:59:59', 'Audi, BMW, Ford, Jeep, Toyota, Volvo', '-', 'patrik', '-', '-', 'patrik', 'patrik', 22, 0, 1020);
INSERT INTO stores VALUES (13631, 'Bilforum Alf Johansson AB', 'christopher.forsmark_bilforumajbo.se@blocket.se', '08 - 594 408 91', '08 - 591 286 30', 'Bristagatan 16', '195 60', 'M�RSTA', '1_3_03.gif', '�terf�rs�ljare f�r Chevrolet, Daewoo, Opel, Saab', '<b>V�lkommen till Bilforum AB</b>
<p>  
H�r kan Du i lugn och ro titta n�rmare p� modellerna fr�n Saab, Opel, Chevrolet. 
<p>
Du kan �ven s�ka bland v�ra begagnade bilar, l�sa om alla kringtj�nster f�r Dig som k�r v�ra m�rken, se vilka erbjudanden vi har f�r tillf�llet samt hur Du enklast kommer i kontakt med v�r personal. 
<p>
V�r m�ls�ttning �r att genom h�gklassiga produkter, engagerad personal och goda kundrelationer skapa en entusiasm f�r v�ra m�rken. Vi vill hela tiden s�tta Dig som kund i fokus f�r att �vertr�ffa Dina f�rv�ntningar. 
<p>
V�lkommen till v�rt f�retag
<p>
Alf Johansson
<p>
<b>�ppettider</b><br>
m�n-tor 09-18 <br>
fre 09-17 <br>
l�r sommar st�ngt <br>
s�n st�ngt <br>

 


 
', 'http://www.bilforum.biz
', 'active', false, '2005-03-01 00:00:00', '2015-12-31 23:59:59', 'Audi, Chevrolet, Ford, Nissan, Opel, Saab, ', '-', 'patrik', '-', '-', 'patrik', 'patrik', 11, 0, 1020);
INSERT INTO stores VALUES (12423, 'Tornum AB', 'info_tornum.se@blocket.se', '0512-291 00', '0512-291 10', 'Skaragatan 15', '535 22', 'Kv�num', 'tornum.gif', 'Raka v�gen till r�tt kvalitet', 'TORNUM �r marknadsledande som helhetsleverant�r av spannm�lsanl�ggningar till lantbruk och spannm�lsindustri. Vi erbjuder kompletta l�sningar oavsett om det g�ller ny- eller tillbyggnad. Vi hj�lper dig med allt fr�n analys till f�rdig anl�ggning. ', 'http://www.tornum.se
', 'active', false, '2003-11-25 00:00:00', '2015-12-31 23:00:00', 'spannm�lsanl�ggningar, lantbruk, spannm�lsindustri', '-', 'patrik', '-', '-', 'patrik', 'patrik', 13, 0, 1220);
INSERT INTO stores VALUES (12427, 'MotorHuset i G�vle AB', 'sales_motorhusetgavle.se@blocket.se', '026-132060', '026-132058', 'Riav�gen 3', '818 33', 'VALBO', 'motorhusetgavle.gif', 'Allt r�rande cross, enduro & supermotard', 'MotorHuset slog upp portarna i april 2003 och vi fick genast en positiv respons fr�n v�ra kunder. 
<p> 
V�r ambition �r att med ett brett kunnande st� till tj�nst med allt r�rande cross, enduro & supermotard. V�r personal �r sj�lva aktiva eller har tidigare varit �kare och p� verkstadssidan har vi personal som servat �t �kare p� internationell niv�. 
<p>  
Vi �r idag �terf�rs�ljare f�r KTM, Kawasaki & Yamaha motorcyklar. Vi s�ljer �ven reservdelar, tillbeh�r & kl�der till alla typer av motorcyklar. Dessutom har vi reservdelar & tillbeh�r till dom allra flesta mopeder p� marknaden.
<p>  
Fram�ver har vi planer p� att bredda v�rt sortiment av motorcyklar och �ven satsa p� vinterprodukter. 
', 'http://www.motorhusetgavle.se
', 'active', false, '2003-11-26 00:00:00', '2015-12-31 23:59:59', 'KTM, Kawasaki, Yamaha, Husaberg, Gas Gas, Fox', '-', 'patrik', '-', '-', 'patrik', 'patrik', 5, 0, 1140);
INSERT INTO stores VALUES (12428, 'Per Larssons Bil HB', 'per.larssons_swipnet.se@blocket.se', '046-25 33 90', '046-25 36 90', 'Kranv�gen 10 �stra Industriomr�de', '245 34', 'Staffanstorp', 'perlarssons.gif', 'K�per, byter, s�ljer', 'ETT V�LETABLERAT BILF�RETAG I STAFFANSTORP
<p>
Vi startade bilfirman 1974 i B�rringe. 1981 flyttades firman till Staffanstorp, 1985 byggde vi nya lokaler p� �stra industriomr�det, d�r vi nu bedriver bilhandel, d�ckf�rs�ljning, Traktor och Motormuseum.
<p>
�ppettider:<br>

Vardagar 9.00-18.00  
<br>L�rd.-S�nd. st�ngt.
<br>
N�r vi ej har �ppet kan Ni prova att n� oss p� 0705-253690<br>
V�nligen l�mna telefon nummer vid intresseanm�lan.
', 'http://www.perlarssonsbil.cjb.net', 'active', false, '2003-11-27 00:00:00', '2016-04-15 23:59:59', 'BMW, Ford, Mercedes, Mitsubishi, Nissan, Opel, Seat, Skoda, Volkswagen, Volvo', '-', 'patrik', '-', '-', 'patrik', 'patrik', 23, 10, 1020);
INSERT INTO stores VALUES (13302, 'Schultzbergs Bil AB', 'klas_ksbil.com@blocket.se', '036-347077', '036-347078', 'Maskingatan 2', '554 74', 'J�NK�PING', 'Schultsberg_logo.gif', 'M�rkesoberoende handlare', '<B>V�lkommen till SCHULTZBERG BIL AB</B>
', 'http://www.ksbil.com

', 'active', false, '2004-11-04 14:00:00', '2016-03-01 23:59:59', 'BMW, Mercedes, Volvo', '-', 'patrik', '-', '-', 'patrik', 'patrik', 17, 0, 1020);
INSERT INTO stores VALUES (12432, 'TK:s Begagnade Traktordelar AB', 'info_tktraktordelar.se@blocket.se', '013 - 35 73 00', '013 - 592 79', 'V�rdsberg', '585 92', 'LINK�PING', 'tktraktor.gif', 'Begagnade och nya reservdelar', 'V�r aff�rsid� �r att k�pa in maskiner f�r att demontera dem och s�lja de begagnade delarna f�r �teranv�ndning. Vi k�per in traktorer, tr�skor, lastmaskiner och traktorgr�vare. Vi har ett stort lager av delar till de flesta modeller.', 'http://www.tktraktordelar.se', 'active', false, '2003-12-01 00:00:00', '2016-02-28 23:59:59', 'Traktordelar, entreprenaddelar, tr�skdelar, reservdelar, lastmaskiner, begagnade, entreprenad, maskiner', '-', 'patrik', '-', '-', 'patrik', 'patrik', 14, 0, 1220);
INSERT INTO stores VALUES (12440, 'Bilm�nsson i Esl�v', 'marten.persson_bilmansson.se@blocket.se', '0413-688 60', '0413-688 62', 'Vikhemsv�gen 2', '241 23', 'ESL�V', 'logo_big.gif', '�terf�rs�ljare f�r Land Rover, Renault, Volvo', 'Vi �r ett litet personligt familjef�retag med traditioner sedan bilismens barndom, vilket ger en stabilitet som gynnar v�ra kunder. G�ran M�nsson �r tredje generationen som driver f�retaget.
<p>
Vi s�ljer bilar och bilrelaterade kringtj�nster som ger ett bekv�mt bil�gande till l�g kostnad och h�g kvalite.
', 'http://www.bilmansson.se/', 'active', false, '2003-12-08 14:45:00', '2016-03-01 23:59:59', 'Volvo, Land Rover, Renault, Michelin, Mitsubishi, Opel, Saab', '-', 'patrik', '-', '-', 'patrik', 'patrik', 23, 2, 1020);
INSERT INTO stores VALUES (12441, 'LM Autoservice', 'info_lmautoservice.com@blocket.se', '0451-561 19', '0451-569 25', 'Helsingborgsv�gen 1', '282 34', 'TYRINGE', 'logo.gif', 'Oberoende m�rkeshandlare', 'V�lkomna till LM Autoservice.<br>
Vi har ett stort utbud av b�de nyare och �ldre begagnade bilar till mycket bra priser.
<p>
�PPETTIDER<br>
M�nd-Fred Kl 09.00-18.00<br>
�vrig tid efter �verenskommelse.
', 'http://www.lmautoservice.com/', 'active', false, '2003-12-08 00:00:00', '2016-09-01 23:59:59', 'Ford, Volvo, Mazda, Nissan, Opel, Peugeot, Renault, Mercedes, Jeep, ', '-', 'patrik', '-', '-', 'patrik', 'patrik', 23, 4, 1020);
INSERT INTO stores VALUES (12444, 'Motor Halland', 'janne.persson_motorhalland.se@blocket.se', '035-18 07 00', '035-18 07 40', 'Stationsgatan 74', '302 45', 'HALMSTAD', 'logo.gif', 'Auktoriserad �terf�rs�ljare av Audi ,VW och Skoda', 'Motor AB Halland har sedan 1952 varit verksamt inom bilhandel och service. 
<br>
Vi �r auktoriserad �terf�s�ljare f�r volkswagen, Audi samt Skoda.
<p>
Motor AB Halland �r certifierade f�r ISO 9001, T�V samt ISO 14001.
<p>

<b>�ppettider: </b><br>
Bilhallen:<br>
M�ndag-Fredag: 9-18, L�rdag: 10-13
<p>
Verkstad och reservdelsbutik:<br>
M�ndag-Fredag: 7-17
 
', 'http://www.motorhalland.se', 'active', false, '2003-12-09 14:07:00', '2015-12-31 23:59:00', 'Audi, VW, Volkswagen', '-', 'patrik', '-', '-', 'patrik', 'patrik', 20, 0, 1020);
INSERT INTO stores VALUES (12450, 'Svensk Bil�tervinning AB', 'svensk.bilatervinning_swipnet.se@blocket.se', '019-45 06 90/91', '019-45 06 92', 'Industriv�gen 8', '715 31', 'ODENSBACKEN', 'logga.gif', 'Beg. Bildelar', 'VI FIXAR DELEN...............
<br>
NI FIXAR FELEN!
<p>
Kontrollerade bildelar upp till �rets modell till f�rdelaktiga priser!
<p>
Beg. Bildelar
<br>
Milj� - Service - Prismedvetande
<p>
�ppettider:
<br>
M�ndag 07:00 - 17:00<br> 
Tisdag 07:00 - 17:00<br>
Onsdag 07:00 - 17:00<br>
Torsdag 07:00 - 17:00<br>
Fredag 07:00 - 15:00<br>
L�rdag St�ngt', 'http://www.fbt.se/user/170/', 'active', false, '2003-12-15 15:30:00', '2016-02-28 23:59:59', 'begagnat, bildelar, �tervinning, ', '-', 'patrik', '-', '-', 'patrik', 'patrik', 8, 0, 1040);
INSERT INTO stores VALUES (14199, 'Huges V�stervik', 'info_jocke.se@blocket.se', '0140 - 190 10', '0140 - 190 11', '�viksv�gen 8', '573 43', 'TRAN�S', 'jocke.bmp', 'Din mc-butik p� n�tet.', 'Vi har det mesta du beh�ver f�r Motocross, Supermotard, Enduro, Street, Custom, Fyrhjulingar/ATV, Moped och nu ocks� Sn�skoter.Vi finns i Tran�s och jobbar ihop med Jofrab/TWS som ocks� ligger i Tran�s. Bara Jofrab�s Cross sortiment ligger p� ca 7 000 artiklar, d�rtill kommer Moped, Sn�skoter, ATV, Street mm
<p>
Vi jobbar ocks� ihop med Carla Pro Shop, som har en m�ngd mycket bra produkter. Ny f�r �ret �r MotoSpeed, som har allt du beh�ver f�r Custom och Streethojar.
<p>
Givetvis har vi ocks� Verkstad f�r reperationer, fr�mst f�r Offroad hojar och till viss del Streethojar och Mopeder.
<p>
H�r till v�nster hittar du olika produktkategorier som fylls p� hela tiden, hittar du inte vad du s�ker ring g�rna oss. L�ngst upp hittar du ocks� information om betalning och frakt kostnader samt information om butiken.
<p>
�PPETIDER BUTIK:<br>
M�ndag-Fredag: 13.00- 18.00<br>
L�rdag, S�ndag: St�ngt
', 'http://www.jocke.se/', 'active', false, '2005-10-03 00:00:00', '2016-03-01 23:59:59', 'Motorcyklar, mc tillbeh�r, reservdelar, trimdelar, hjul, utrustning, kl�der, hj�lmar, st�vlar, slitdelar, motocross, supermotard, enduro, landsv�g, freestyle mx, axo, boyesen, braking, ceet, fmf, reikon, skin, tech, ufo, fp pipes/silencer, pro carbon racing, mc', '-', 'patrik', '-', '-', 'patrik', 'patrik', 18, 0, 1140);
INSERT INTO stores VALUES (12455, 'Husvagnsreserven AB', 'husvagnsreserven_telia.com@blocket.se', '044-24 56 90', '044-24 44 86', 'S�vv�gen 10', '291 59', 'KRISTIANSTAD', 'logo.gif', 'Det din husvagn beh�ver.', 'Husvagnsreserven grundades 1988 av Anders Ekdahl. Under tiden 1988 -1992 importerades i huvudsak reservdelar till Tabbert husvagnar.
Under l�gkonjunkturen 1992 organiserades f�retaget om och bildade ett aktiebolag (Husvagnsreserven i Kristianstad AB). Vid denna tidpunkt b�rjade f�retaget �ven s�lja begagnade husvagnar.
<p>
Under �ren 2000-2001 s�ldes nya husvagnar av fabrikat Tabbert och Adria. Bolaget omsatte ca 8,5 miljoner kronor �r 2000.
<p>
Ist�llet f�r Adria b�rjade vi �r 2002 s�lja husvagnar av m�rken Vimara och Wilk. B�da dessa �r produkter fr�n Knaus-Tabbert-Group. Dessa lyckades vi g�ra till riktiga succ�-vagnar. Detta �ret blev vi Sverige1:a p� samtliga av v�ra representerade husvagnar TABBERT, WILK OCH VIMARA.
<p>
�r 2003, �ven detta �r lyckades vi bli Sverige1:a p� TABBERT, WILK OCH VIMARA.
<p>
�PPETTIDER:
<br>
M�ndag - Fredag 8.00 - 17.00
<br>
L�rdag St�ngt
<br>
S�ndag St�ngt', 'http://www.husvagnsreserven.se', 'active', false, '2003-12-23 14:45:00', '2016-06-23 00:00:00', 'husvagnar, tabbert, wilk,', '-', 'patrik', '-', '-', 'patrik', 'patrik', 23, 2, 1100);
INSERT INTO stores VALUES (12456, 'Husvagnsreserven AB', 'husvagnsreserven_telia.com@blocket.se', '044-24 56 90', '044-24 44 86', 'S�vv�gen 10', '291 59', 'KRISTIANSTAD', 'logo.gif', 'Det din husvagn beh�ver.', 'Husvagnsreserven grundades 1988 av Anders Ekdahl. Under tiden 1988 -1992 importerades i huvudsak reservdelar till Tabbert husvagnar.
Under l�gkonjunkturen 1992 organiserades f�retaget om och bildade ett aktiebolag (Husvagnsreserven i Kristianstad AB). Vid denna tidpunkt b�rjade f�retaget �ven s�lja begagnade husvagnar.
<p>
Under �ren 2000-2001 s�ldes nya husvagnar av fabrikat Tabbert och Adria. Bolaget omsatte ca 8,5 miljoner kronor �r 2000.
<p>
Ist�llet f�r Adria b�rjade vi �r 2002 s�lja husvagnar av m�rken Vimara och Wilk. B�da dessa �r produkter fr�n Knaus-Tabbert-Group. Dessa lyckades vi g�ra till riktiga succ�-vagnar. Detta �ret blev vi Sverige1:a p� samtliga av v�ra representerade husvagnar TABBERT, WILK OCH VIMARA.
<p>
�r 2003, �ven detta �r lyckades vi bli Sverige1:a p� TABBERT, WILK OCH VIMARA.
<p>
�PPETTIDER:
<br>
M�ndag - Fredag 8.00 - 17.00
<br>
L�rdag St�ngt
<br>
S�ndag St�ngt', 'http://www.husvagnsreserven.se', 'active', false, '2003-12-23 14:45:00', '2016-06-23 00:00:00', 'husvagnar, tabbert, wilk,', '-', 'patrik', '-', '-', 'patrik', 'patrik', 23, 3, 1100);
INSERT INTO stores VALUES (12457, 'Husvagnsreserven AB', 'husvagnsreserven_telia.com@blocket.se', '044-24 56 90', '044-24 44 86', 'S�vv�gen 10', '291 59', 'KRISTIANSTAD', 'logo.gif', 'Det din husvagn beh�ver.', 'Husvagnsreserven grundades 1988 av Anders Ekdahl. Under tiden 1988 -1992 importerades i huvudsak reservdelar till Tabbert husvagnar.
Under l�gkonjunkturen 1992 organiserades f�retaget om och bildade ett aktiebolag (Husvagnsreserven i Kristianstad AB). Vid denna tidpunkt b�rjade f�retaget �ven s�lja begagnade husvagnar.
<p>
Under �ren 2000-2001 s�ldes nya husvagnar av fabrikat Tabbert och Adria. Bolaget omsatte ca 8,5 miljoner kronor �r 2000.
<p>
Ist�llet f�r Adria b�rjade vi �r 2002 s�lja husvagnar av m�rken Vimara och Wilk. B�da dessa �r produkter fr�n Knaus-Tabbert-Group. Dessa lyckades vi g�ra till riktiga succ�-vagnar. Detta �ret blev vi Sverige1:a p� samtliga av v�ra representerade husvagnar TABBERT, WILK OCH VIMARA.
<p>
�r 2003, �ven detta �r lyckades vi bli Sverige1:a p� TABBERT, WILK OCH VIMARA.
<p>
�PPETTIDER:
<br>
M�ndag - Fredag 8.00 - 17.00
<br>
L�rdag St�ngt
<br>
S�ndag St�ngt', 'http://www.husvagnsreserven.se', 'active', false, '2003-12-23 14:45:00', '2016-06-23 00:00:00', 'husvagnar, tabbert, wilk,', '-', 'patrik', '-', '-', 'patrik', 'patrik', 23, 4, 1100);
INSERT INTO stores VALUES (12458, 'Husvagnsreserven AB', 'husvagnsreserven_telia.com@blocket.se', '044-24 56 90', '044-24 44 86', 'S�vv�gen 10', '291 59', 'KRISTIANSTAD', 'logo.gif', 'Det din husvagn beh�ver.', 'Husvagnsreserven grundades 1988 av Anders Ekdahl. Under tiden 1988 -1992 importerades i huvudsak reservdelar till Tabbert husvagnar.
Under l�gkonjunkturen 1992 organiserades f�retaget om och bildade ett aktiebolag (Husvagnsreserven i Kristianstad AB). Vid denna tidpunkt b�rjade f�retaget �ven s�lja begagnade husvagnar.
<p>
Under �ren 2000-2001 s�ldes nya husvagnar av fabrikat Tabbert och Adria. Bolaget omsatte ca 8,5 miljoner kronor �r 2000.
<p>
Ist�llet f�r Adria b�rjade vi �r 2002 s�lja husvagnar av m�rken Vimara och Wilk. B�da dessa �r produkter fr�n Knaus-Tabbert-Group. Dessa lyckades vi g�ra till riktiga succ�-vagnar. Detta �ret blev vi Sverige1:a p� samtliga av v�ra representerade husvagnar TABBERT, WILK OCH VIMARA.
<p>
�r 2003, �ven detta �r lyckades vi bli Sverige1:a p� TABBERT, WILK OCH VIMARA.
<p>
�PPETTIDER:
<br>
M�ndag - Fredag 8.00 - 17.00
<br>
L�rdag St�ngt
<br>
S�ndag St�ngt', 'http://www.husvagnsreserven.se', 'active', false, '2003-12-23 14:45:00', '2016-06-23 00:00:00', 'husvagnar, tabbert, wilk,', '-', 'patrik', '-', '-', 'patrik', 'patrik', 23, 5, 1100);
INSERT INTO stores VALUES (12459, 'Husvagnsreserven AB', 'husvagnsreserven_telia.com@blocket.se', '044-24 56 90', '044-24 44 86', 'S�vv�gen 10', '291 59', 'KRISTIANSTAD', 'logo.gif', 'Det din husvagn beh�ver.', 'Husvagnsreserven grundades 1988 av Anders Ekdahl. Under tiden 1988 -1992 importerades i huvudsak reservdelar till Tabbert husvagnar.
Under l�gkonjunkturen 1992 organiserades f�retaget om och bildade ett aktiebolag (Husvagnsreserven i Kristianstad AB). Vid denna tidpunkt b�rjade f�retaget �ven s�lja begagnade husvagnar.
<p>
Under �ren 2000-2001 s�ldes nya husvagnar av fabrikat Tabbert och Adria. Bolaget omsatte ca 8,5 miljoner kronor �r 2000.
<p>
Ist�llet f�r Adria b�rjade vi �r 2002 s�lja husvagnar av m�rken Vimara och Wilk. B�da dessa �r produkter fr�n Knaus-Tabbert-Group. Dessa lyckades vi g�ra till riktiga succ�-vagnar. Detta �ret blev vi Sverige1:a p� samtliga av v�ra representerade husvagnar TABBERT, WILK OCH VIMARA.
<p>
�r 2003, �ven detta �r lyckades vi bli Sverige1:a p� TABBERT, WILK OCH VIMARA.
<p>
�PPETTIDER:
<br>
M�ndag - Fredag 8.00 - 17.00
<br>
L�rdag St�ngt
<br>
S�ndag St�ngt', 'http://www.husvagnsreserven.se', 'active', false, '2003-12-23 14:45:00', '2016-06-23 00:00:00', 'husvagnar, tabbert, wilk,', '-', 'patrik', '-', '-', 'patrik', 'patrik', 23, 6, 1100);
INSERT INTO stores VALUES (12460, 'Husvagnsreserven AB', 'husvagnsreserven_telia.com@blocket.se', '044-24 56 90', '044-24 44 86', 'S�vv�gen 10', '291 59', 'KRISTIANSTAD', 'logo.gif', 'Det din husvagn beh�ver.', 'Husvagnsreserven grundades 1988 av Anders Ekdahl. Under tiden 1988 -1992 importerades i huvudsak reservdelar till Tabbert husvagnar.
Under l�gkonjunkturen 1992 organiserades f�retaget om och bildade ett aktiebolag (Husvagnsreserven i Kristianstad AB). Vid denna tidpunkt b�rjade f�retaget �ven s�lja begagnade husvagnar.
<p>
Under �ren 2000-2001 s�ldes nya husvagnar av fabrikat Tabbert och Adria. Bolaget omsatte ca 8,5 miljoner kronor �r 2000.
<p>
Ist�llet f�r Adria b�rjade vi �r 2002 s�lja husvagnar av m�rken Vimara och Wilk. B�da dessa �r produkter fr�n Knaus-Tabbert-Group. Dessa lyckades vi g�ra till riktiga succ�-vagnar. Detta �ret blev vi Sverige1:a p� samtliga av v�ra representerade husvagnar TABBERT, WILK OCH VIMARA.
<p>
�r 2003, �ven detta �r lyckades vi bli Sverige1:a p� TABBERT, WILK OCH VIMARA.
<p>
�PPETTIDER:
<br>
M�ndag - Fredag 8.00 - 17.00
<br>
L�rdag St�ngt
<br>
S�ndag St�ngt', 'http://www.husvagnsreserven.se', 'active', false, '2003-12-23 14:45:00', '2016-06-23 00:00:00', 'husvagnar, tabbert, wilk,', '-', 'patrik', '-', '-', 'patrik', 'patrik', 23, 7, 1100);
INSERT INTO stores VALUES (12461, 'Husvagnsreserven AB', 'husvagnsreserven_telia.com@blocket.se', '044-24 56 90', '044-24 44 86', 'S�vv�gen 10', '291 59', 'KRISTIANSTAD', 'logo.gif', 'Det din husvagn beh�ver.', 'Husvagnsreserven grundades 1988 av Anders Ekdahl. Under tiden 1988 -1992 importerades i huvudsak reservdelar till Tabbert husvagnar.
Under l�gkonjunkturen 1992 organiserades f�retaget om och bildade ett aktiebolag (Husvagnsreserven i Kristianstad AB). Vid denna tidpunkt b�rjade f�retaget �ven s�lja begagnade husvagnar.
<p>
Under �ren 2000-2001 s�ldes nya husvagnar av fabrikat Tabbert och Adria. Bolaget omsatte ca 8,5 miljoner kronor �r 2000.
<p>
Ist�llet f�r Adria b�rjade vi �r 2002 s�lja husvagnar av m�rken Vimara och Wilk. B�da dessa �r produkter fr�n Knaus-Tabbert-Group. Dessa lyckades vi g�ra till riktiga succ�-vagnar. Detta �ret blev vi Sverige1:a p� samtliga av v�ra representerade husvagnar TABBERT, WILK OCH VIMARA.
<p>
�r 2003, �ven detta �r lyckades vi bli Sverige1:a p� TABBERT, WILK OCH VIMARA.
<p>
�PPETTIDER:
<br>
M�ndag - Fredag 8.00 - 17.00
<br>
L�rdag St�ngt
<br>
S�ndag St�ngt', 'http://www.husvagnsreserven.se', 'active', false, '2003-12-23 14:45:00', '2016-06-23 00:00:00', 'husvagnar, tabbert, wilk,', '-', 'patrik', '-', '-', 'patrik', 'patrik', 23, 9, 1100);
INSERT INTO stores VALUES (12462, 'Husvagnsreserven AB', 'husvagnsreserven_telia.com@blocket.se', '044-24 56 90', '044-24 44 86', 'S�vv�gen 10', '291 59', 'KRISTIANSTAD', 'logo.gif', 'Det din husvagn beh�ver.', 'Husvagnsreserven grundades 1988 av Anders Ekdahl. Under tiden 1988 -1992 importerades i huvudsak reservdelar till Tabbert husvagnar.
Under l�gkonjunkturen 1992 organiserades f�retaget om och bildade ett aktiebolag (Husvagnsreserven i Kristianstad AB). Vid denna tidpunkt b�rjade f�retaget �ven s�lja begagnade husvagnar.
<p>
Under �ren 2000-2001 s�ldes nya husvagnar av fabrikat Tabbert och Adria. Bolaget omsatte ca 8,5 miljoner kronor �r 2000.
<p>
Ist�llet f�r Adria b�rjade vi �r 2002 s�lja husvagnar av m�rken Vimara och Wilk. B�da dessa �r produkter fr�n Knaus-Tabbert-Group. Dessa lyckades vi g�ra till riktiga succ�-vagnar. Detta �ret blev vi Sverige1:a p� samtliga av v�ra representerade husvagnar TABBERT, WILK OCH VIMARA.
<p>
�r 2003, �ven detta �r lyckades vi bli Sverige1:a p� TABBERT, WILK OCH VIMARA.
<p>
�PPETTIDER:
<br>
M�ndag - Fredag 8.00 - 17.00
<br>
L�rdag St�ngt
<br>
S�ndag St�ngt', 'http://www.husvagnsreserven.se', 'active', false, '2003-12-23 14:45:00', '2016-06-23 00:00:00', 'husvagnar, tabbert, wilk,', '-', 'patrik', '-', '-', 'patrik', 'patrik', 23, 8, 1100);
INSERT INTO stores VALUES (12463, 'Husvagnsreserven AB', 'husvagnsreserven_telia.com@blocket.se', '044-24 56 90', '044-24 44 86', 'S�vv�gen 10', '291 59', 'KRISTIANSTAD', 'logo.gif', 'Det din husvagn beh�ver.', 'Husvagnsreserven grundades 1988 av Anders Ekdahl. Under tiden 1988 -1992 importerades i huvudsak reservdelar till Tabbert husvagnar.
Under l�gkonjunkturen 1992 organiserades f�retaget om och bildade ett aktiebolag (Husvagnsreserven i Kristianstad AB). Vid denna tidpunkt b�rjade f�retaget �ven s�lja begagnade husvagnar.
<p>
Under �ren 2000-2001 s�ldes nya husvagnar av fabrikat Tabbert och Adria. Bolaget omsatte ca 8,5 miljoner kronor �r 2000.
<p>
Ist�llet f�r Adria b�rjade vi �r 2002 s�lja husvagnar av m�rken Vimara och Wilk. B�da dessa �r produkter fr�n Knaus-Tabbert-Group. Dessa lyckades vi g�ra till riktiga succ�-vagnar. Detta �ret blev vi Sverige1:a p� samtliga av v�ra representerade husvagnar TABBERT, WILK OCH VIMARA.
<p>
�r 2003, �ven detta �r lyckades vi bli Sverige1:a p� TABBERT, WILK OCH VIMARA.
<p>
�PPETTIDER:
<br>
M�ndag - Fredag 8.00 - 17.00
<br>
L�rdag St�ngt
<br>
S�ndag St�ngt', 'http://www.husvagnsreserven.se', 'active', false, '2003-12-23 14:45:00', '2016-06-23 00:00:00', 'husvagnar, tabbert, wilk,', '-', 'patrik', '-', '-', 'patrik', 'patrik', 23, 10, 1100);
INSERT INTO stores VALUES (12464, 'Husvagnsreserven AB', 'husvagnsreserven_telia.com@blocket.se', '044-24 56 90', '044-24 44 86', 'S�vv�gen 10', '291 59', 'KRISTIANSTAD', 'logo.gif', 'Det din husvagn beh�ver.', 'Husvagnsreserven grundades 1988 av Anders Ekdahl. Under tiden 1988 -1992 importerades i huvudsak reservdelar till Tabbert husvagnar.
Under l�gkonjunkturen 1992 organiserades f�retaget om och bildade ett aktiebolag (Husvagnsreserven i Kristianstad AB). Vid denna tidpunkt b�rjade f�retaget �ven s�lja begagnade husvagnar.
<p>
Under �ren 2000-2001 s�ldes nya husvagnar av fabrikat Tabbert och Adria. Bolaget omsatte ca 8,5 miljoner kronor �r 2000.
<p>
Ist�llet f�r Adria b�rjade vi �r 2002 s�lja husvagnar av m�rken Vimara och Wilk. B�da dessa �r produkter fr�n Knaus-Tabbert-Group. Dessa lyckades vi g�ra till riktiga succ�-vagnar. Detta �ret blev vi Sverige1:a p� samtliga av v�ra representerade husvagnar TABBERT, WILK OCH VIMARA.
<p>
�r 2003, �ven detta �r lyckades vi bli Sverige1:a p� TABBERT, WILK OCH VIMARA.
<p>
�PPETTIDER:
<br>
M�ndag - Fredag 8.00 - 17.00
<br>
L�rdag St�ngt
<br>
S�ndag St�ngt', 'http://www.husvagnsreserven.se', 'active', false, '2003-12-23 14:45:00', '2016-06-23 00:00:00', 'husvagnar, tabbert, wilk,', '-', 'patrik', '-', '-', 'patrik', 'patrik', 23, 11, 1100);
INSERT INTO stores VALUES (12465, 'Husvagnsreserven AB', 'husvagnsreserven_telia.com@blocket.se', '044-24 56 90', '044-24 44 86', 'S�vv�gen 10', '291 59', 'KRISTIANSTAD', 'logo.gif', 'Det din husvagn beh�ver.', 'Husvagnsreserven grundades 1988 av Anders Ekdahl. Under tiden 1988 -1992 importerades i huvudsak reservdelar till Tabbert husvagnar.
Under l�gkonjunkturen 1992 organiserades f�retaget om och bildade ett aktiebolag (Husvagnsreserven i Kristianstad AB). Vid denna tidpunkt b�rjade f�retaget �ven s�lja begagnade husvagnar.
<p>
Under �ren 2000-2001 s�ldes nya husvagnar av fabrikat Tabbert och Adria. Bolaget omsatte ca 8,5 miljoner kronor �r 2000.
<p>
Ist�llet f�r Adria b�rjade vi �r 2002 s�lja husvagnar av m�rken Vimara och Wilk. B�da dessa �r produkter fr�n Knaus-Tabbert-Group. Dessa lyckades vi g�ra till riktiga succ�-vagnar. Detta �ret blev vi Sverige1:a p� samtliga av v�ra representerade husvagnar TABBERT, WILK OCH VIMARA.
<p>
�r 2003, �ven detta �r lyckades vi bli Sverige1:a p� TABBERT, WILK OCH VIMARA.
<p>
�PPETTIDER:
<br>
M�ndag - Fredag 8.00 - 17.00
<br>
L�rdag St�ngt
<br>
S�ndag St�ngt', 'http://www.husvagnsreserven.se', 'active', false, '2003-12-23 14:45:00', '2016-06-23 00:00:00', 'husvagnar, tabbert, wilk,', '-', 'patrik', '-', '-', 'patrik', 'patrik', 23, 12, 1100);
INSERT INTO stores VALUES (12466, 'Husvagnsreserven AB', 'husvagnsreserven_telia.com@blocket.se', '044-24 56 90', '044-24 44 86', 'S�vv�gen 10', '291 59', 'KRISTIANSTAD', 'logo.gif', 'Det din husvagn beh�ver.', 'Husvagnsreserven grundades 1988 av Anders Ekdahl. Under tiden 1988 -1992 importerades i huvudsak reservdelar till Tabbert husvagnar.
Under l�gkonjunkturen 1992 organiserades f�retaget om och bildade ett aktiebolag (Husvagnsreserven i Kristianstad AB). Vid denna tidpunkt b�rjade f�retaget �ven s�lja begagnade husvagnar.
<p>
Under �ren 2000-2001 s�ldes nya husvagnar av fabrikat Tabbert och Adria. Bolaget omsatte ca 8,5 miljoner kronor �r 2000.
<p>
Ist�llet f�r Adria b�rjade vi �r 2002 s�lja husvagnar av m�rken Vimara och Wilk. B�da dessa �r produkter fr�n Knaus-Tabbert-Group. Dessa lyckades vi g�ra till riktiga succ�-vagnar. Detta �ret blev vi Sverige1:a p� samtliga av v�ra representerade husvagnar TABBERT, WILK OCH VIMARA.
<p>
�r 2003, �ven detta �r lyckades vi bli Sverige1:a p� TABBERT, WILK OCH VIMARA.
<p>
�PPETTIDER:
<br>
M�ndag - Fredag 8.00 - 17.00
<br>
L�rdag St�ngt
<br>
S�ndag St�ngt', 'http://www.husvagnsreserven.se', 'active', false, '2003-12-23 14:45:00', '2016-06-23 00:00:00', 'husvagnar, tabbert, wilk,', '-', 'patrik', '-', '-', 'patrik', 'patrik', 23, 13, 1100);
INSERT INTO stores VALUES (12467, 'Husvagnsreserven AB', 'husvagnsreserven_telia.com@blocket.se', '044-24 56 90', '044-24 44 86', 'S�vv�gen 10', '291 59', 'KRISTIANSTAD', 'logo.gif', 'Det din husvagn beh�ver.', 'Husvagnsreserven grundades 1988 av Anders Ekdahl. Under tiden 1988 -1992 importerades i huvudsak reservdelar till Tabbert husvagnar.
Under l�gkonjunkturen 1992 organiserades f�retaget om och bildade ett aktiebolag (Husvagnsreserven i Kristianstad AB). Vid denna tidpunkt b�rjade f�retaget �ven s�lja begagnade husvagnar.
<p>
Under �ren 2000-2001 s�ldes nya husvagnar av fabrikat Tabbert och Adria. Bolaget omsatte ca 8,5 miljoner kronor �r 2000.
<p>
Ist�llet f�r Adria b�rjade vi �r 2002 s�lja husvagnar av m�rken Vimara och Wilk. B�da dessa �r produkter fr�n Knaus-Tabbert-Group. Dessa lyckades vi g�ra till riktiga succ�-vagnar. Detta �ret blev vi Sverige1:a p� samtliga av v�ra representerade husvagnar TABBERT, WILK OCH VIMARA.
<p>
�r 2003, �ven detta �r lyckades vi bli Sverige1:a p� TABBERT, WILK OCH VIMARA.
<p>
�PPETTIDER:
<br>
M�ndag - Fredag 8.00 - 17.00
<br>
L�rdag St�ngt
<br>
S�ndag St�ngt', 'http://www.husvagnsreserven.se', 'active', false, '2003-12-23 14:45:00', '2016-06-23 00:00:00', 'husvagnar, tabbert, wilk,', '-', 'patrik', '-', '-', 'patrik', 'patrik', 23, 14, 1100);
INSERT INTO stores VALUES (12468, 'Husvagnsreserven AB', 'husvagnsreserven_telia.com@blocket.se', '044-24 56 90', '044-24 44 86', 'S�vv�gen 10', '291 59', 'KRISTIANSTAD', 'logo.gif', 'Det din husvagn beh�ver.', 'Husvagnsreserven grundades 1988 av Anders Ekdahl. Under tiden 1988 -1992 importerades i huvudsak reservdelar till Tabbert husvagnar.
Under l�gkonjunkturen 1992 organiserades f�retaget om och bildade ett aktiebolag (Husvagnsreserven i Kristianstad AB). Vid denna tidpunkt b�rjade f�retaget �ven s�lja begagnade husvagnar.
<p>
Under �ren 2000-2001 s�ldes nya husvagnar av fabrikat Tabbert och Adria. Bolaget omsatte ca 8,5 miljoner kronor �r 2000.
<p>
Ist�llet f�r Adria b�rjade vi �r 2002 s�lja husvagnar av m�rken Vimara och Wilk. B�da dessa �r produkter fr�n Knaus-Tabbert-Group. Dessa lyckades vi g�ra till riktiga succ�-vagnar. Detta �ret blev vi Sverige1:a p� samtliga av v�ra representerade husvagnar TABBERT, WILK OCH VIMARA.
<p>
�r 2003, �ven detta �r lyckades vi bli Sverige1:a p� TABBERT, WILK OCH VIMARA.
<p>
�PPETTIDER:
<br>
M�ndag - Fredag 8.00 - 17.00
<br>
L�rdag St�ngt
<br>
S�ndag St�ngt', 'http://www.husvagnsreserven.se', 'active', false, '2003-12-23 14:45:00', '2016-06-23 00:00:00', 'husvagnar, tabbert, wilk,', '-', 'patrik', '-', '-', 'patrik', 'patrik', 23, 15, 1100);
INSERT INTO stores VALUES (12469, 'Husvagnsreserven AB', 'husvagnsreserven_telia.com@blocket.se', '044-24 56 90', '044-24 44 86', 'S�vv�gen 10', '291 59', 'KRISTIANSTAD', 'logo.gif', 'Det din husvagn beh�ver.', 'Husvagnsreserven grundades 1988 av Anders Ekdahl. Under tiden 1988 -1992 importerades i huvudsak reservdelar till Tabbert husvagnar.
Under l�gkonjunkturen 1992 organiserades f�retaget om och bildade ett aktiebolag (Husvagnsreserven i Kristianstad AB). Vid denna tidpunkt b�rjade f�retaget �ven s�lja begagnade husvagnar.
<p>
Under �ren 2000-2001 s�ldes nya husvagnar av fabrikat Tabbert och Adria. Bolaget omsatte ca 8,5 miljoner kronor �r 2000.
<p>
Ist�llet f�r Adria b�rjade vi �r 2002 s�lja husvagnar av m�rken Vimara och Wilk. B�da dessa �r produkter fr�n Knaus-Tabbert-Group. Dessa lyckades vi g�ra till riktiga succ�-vagnar. Detta �ret blev vi Sverige1:a p� samtliga av v�ra representerade husvagnar TABBERT, WILK OCH VIMARA.
<p>
�r 2003, �ven detta �r lyckades vi bli Sverige1:a p� TABBERT, WILK OCH VIMARA.
<p>
�PPETTIDER:
<br>
M�ndag - Fredag 8.00 - 17.00
<br>
L�rdag St�ngt
<br>
S�ndag St�ngt', 'http://www.husvagnsreserven.se', 'active', false, '2003-12-23 14:45:00', '2016-06-23 00:00:00', 'husvagnar, tabbert, wilk,', '-', 'patrik', '-', '-', 'patrik', 'patrik', 23, 16, 1100);
INSERT INTO stores VALUES (14426, 'Motorsalongen i Halmstad', 'info_motosalongen.com@blocket.se', '035-21 75 60', '035-21 74 16', 'Laholmsv�gen 84', '302 49', 'HALMSTAD', 'butik.gif', '�terf�rs�ljare f�r Citro�n, Hyundai', '<b>V�lkommen till Motorsalongen i Halmstad </b>', 'http://www.motorsalongen.com

', 'active', false, '2005-10-17 00:00:00', '2016-01-31 23:59:59', 'Citro�n, Hyundai, Audi, Alfa Romeo, Ford, Honda, Nissan, Peugeot, Renault, Saab, Toyota, Volkswagen, VW, Volvo', '-', 'patrik', '-', '-', 'patrik', 'patrik', 20, 0, 1020);
INSERT INTO stores VALUES (12472, 'Eurogarage', 'info_eurogarage.com@blocket.se', '0410-405 60', '0410-474 86', 'J�rnv�gsgatan 15', '231 44', 'TRELLEBORG', 'euro_logo_liten.gif', 'Vi har bilen du dr�mmer om.', 'Vi p� Eurogarage har m�nga �rs erfarenhet av bilimport. Vi kan erbjuda Er import av i princip alla typer av fordon, inregistrerade och klara att anv�ndas i Sverige.
Med v�r erfarenhet kan vi ge Er ett tryggt k�p med garantier.', 'http://www.eurogarage.com/', 'active', false, '2003-12-22 15:00:00', '2015-12-31 23:59:59', 'volvo, chrysler, audi, bmw, stylad, mercedes, porsche, peugeot, vw, suzuki, toyota,', '-', 'patrik', '-', '-', 'patrik', 'patrik', 23, 1, 1020);
INSERT INTO stores VALUES (12473, 'Eurogarage', 'info_eurogarage.com@blocket.se', '0410-405 60', '0410-474 86', 'J�rnv�gsgatan 15', '231 44', 'TRELLEBORG', 'euro_logo_liten.gif', 'Vi har bilen du dr�mmer om.', 'Vi p� Eurogarage har m�nga �rs erfarenhet av bilimport. Vi kan erbjuda Er import av i princip alla typer av fordon, inregistrerade och klara att anv�ndas i Sverige.
Med v�r erfarenhet kan vi ge Er ett tryggt k�p med garantier.', 'http://www.eurogarage.com/', 'active', false, '2003-12-22 15:00:00', '2015-12-31 23:59:59', 'volvo, chrysler, audi, bmw, stylad, mercedes, porsche, peugeot, vw, suzuki, toyota,', '-', 'patrik', '-', '-', 'patrik', 'patrik', 23, 2, 1020);
INSERT INTO stores VALUES (12474, 'Eurogarage', 'info_eurogarage.com@blocket.se', '0410-405 60', '0410-474 86', 'J�rnv�gsgatan 15', '231 44', 'TRELLEBORG', 'euro_logo_liten.gif', 'Vi har bilen du dr�mmer om.', 'Vi p� Eurogarage har m�nga �rs erfarenhet av bilimport. Vi kan erbjuda Er import av i princip alla typer av fordon, inregistrerade och klara att anv�ndas i Sverige.
Med v�r erfarenhet kan vi ge Er ett tryggt k�p med garantier.', 'http://www.eurogarage.com/', 'active', false, '2003-12-22 15:00:00', '2015-12-31 23:59:59', 'volvo, chrysler, audi, bmw, stylad, mercedes, porsche, peugeot, vw, suzuki, toyota,', '-', 'patrik', '-', '-', 'patrik', 'patrik', 23, 3, 1020);
INSERT INTO stores VALUES (12475, 'Eurogarage', 'info_eurogarage.com@blocket.se', '0410-405 60', '0410-474 86', 'J�rnv�gsgatan 15', '231 44', 'TRELLEBORG', 'euro_logo_liten.gif', 'Vi har bilen du dr�mmer om.', 'Vi p� Eurogarage har m�nga �rs erfarenhet av bilimport. Vi kan erbjuda Er import av i princip alla typer av fordon, inregistrerade och klara att anv�ndas i Sverige.
Med v�r erfarenhet kan vi ge Er ett tryggt k�p med garantier.', 'http://www.eurogarage.com/', 'active', false, '2003-12-22 15:00:00', '2015-12-31 23:59:59', 'volvo, chrysler, audi, bmw, stylad, mercedes, porsche, peugeot, vw, suzuki, toyota,', '-', 'patrik', '-', '-', 'patrik', 'patrik', 23, 4, 1020);
INSERT INTO stores VALUES (12476, 'Eurogarage', 'info_eurogarage.com@blocket.se', '0410-405 60', '0410-474 86', 'J�rnv�gsgatan 15', '231 44', 'TRELLEBORG', 'euro_logo_liten.gif', 'Vi har bilen du dr�mmer om.', 'Vi p� Eurogarage har m�nga �rs erfarenhet av bilimport. Vi kan erbjuda Er import av i princip alla typer av fordon, inregistrerade och klara att anv�ndas i Sverige.
Med v�r erfarenhet kan vi ge Er ett tryggt k�p med garantier.', 'http://www.eurogarage.com/', 'active', false, '2003-12-22 15:00:00', '2015-12-31 23:59:59', 'volvo, chrysler, audi, bmw, stylad, mercedes, porsche, peugeot, vw, suzuki, toyota,', '-', 'patrik', '-', '-', 'patrik', 'patrik', 23, 5, 1020);
INSERT INTO stores VALUES (12477, 'Eurogarage', 'info_eurogarage.com@blocket.se', '0410-405 60', '0410-474 86', 'J�rnv�gsgatan 15', '231 44', 'TRELLEBORG', 'euro_logo_liten.gif', 'Vi har bilen du dr�mmer om.', 'Vi p� Eurogarage har m�nga �rs erfarenhet av bilimport. Vi kan erbjuda Er import av i princip alla typer av fordon, inregistrerade och klara att anv�ndas i Sverige.
Med v�r erfarenhet kan vi ge Er ett tryggt k�p med garantier.', 'http://www.eurogarage.com/', 'active', false, '2003-12-22 15:00:00', '2015-12-31 23:59:59', 'volvo, chrysler, audi, bmw, stylad, mercedes, porsche, peugeot, vw, suzuki, toyota,', '-', 'patrik', '-', '-', 'patrik', 'patrik', 23, 6, 1020);
INSERT INTO stores VALUES (12478, 'Eurogarage', 'info_eurogarage.com@blocket.se', '0410-405 60', '0410-474 86', 'J�rnv�gsgatan 15', '231 44', 'TRELLEBORG', 'euro_logo_liten.gif', 'Vi har bilen du dr�mmer om.', 'Vi p� Eurogarage har m�nga �rs erfarenhet av bilimport. Vi kan erbjuda Er import av i princip alla typer av fordon, inregistrerade och klara att anv�ndas i Sverige.
Med v�r erfarenhet kan vi ge Er ett tryggt k�p med garantier.', 'http://www.eurogarage.com/', 'active', false, '2003-12-22 15:00:00', '2015-12-31 23:59:59', 'volvo, chrysler, audi, bmw, stylad, mercedes, porsche, peugeot, vw, suzuki, toyota,', '-', 'patrik', '-', '-', 'patrik', 'patrik', 23, 7, 1020);
INSERT INTO stores VALUES (12479, 'Eurogarage', 'info_eurogarage.com@blocket.se', '0410-405 60', '0410-474 86', 'J�rnv�gsgatan 15', '231 44', 'TRELLEBORG', 'euro_logo_liten.gif', 'Vi har bilen du dr�mmer om.', 'Vi p� Eurogarage har m�nga �rs erfarenhet av bilimport. Vi kan erbjuda Er import av i princip alla typer av fordon, inregistrerade och klara att anv�ndas i Sverige.
Med v�r erfarenhet kan vi ge Er ett tryggt k�p med garantier.', 'http://www.eurogarage.com/', 'active', false, '2003-12-22 15:00:00', '2015-12-31 23:59:59', 'volvo, chrysler, audi, bmw, stylad, mercedes, porsche, peugeot, vw, suzuki, toyota,', '-', 'patrik', '-', '-', 'patrik', 'patrik', 23, 8, 1020);
INSERT INTO stores VALUES (12480, 'Eurogarage', 'info_eurogarage.com@blocket.se', '0410-405 60', '0410-474 86', 'J�rnv�gsgatan 15', '231 44', 'TRELLEBORG', 'euro_logo_liten.gif', 'Vi har bilen du dr�mmer om.', 'Vi p� Eurogarage har m�nga �rs erfarenhet av bilimport. Vi kan erbjuda Er import av i princip alla typer av fordon, inregistrerade och klara att anv�ndas i Sverige.
Med v�r erfarenhet kan vi ge Er ett tryggt k�p med garantier.', 'http://www.eurogarage.com/', 'active', false, '2003-12-22 15:00:00', '2015-12-31 23:59:59', 'volvo, chrysler, audi, bmw, stylad, mercedes, porsche, peugeot, vw, suzuki, toyota,', '-', 'patrik', '-', '-', 'patrik', 'patrik', 23, 9, 1020);
INSERT INTO stores VALUES (12481, 'Eurogarage', 'info_eurogarage.com@blocket.se', '0410-405 60', '0410-474 86', 'J�rnv�gsgatan 15', '231 44', 'TRELLEBORG', 'euro_logo_liten.gif', 'Vi har bilen du dr�mmer om.', 'Vi p� Eurogarage har m�nga �rs erfarenhet av bilimport. Vi kan erbjuda Er import av i princip alla typer av fordon, inregistrerade och klara att anv�ndas i Sverige.
Med v�r erfarenhet kan vi ge Er ett tryggt k�p med garantier.', 'http://www.eurogarage.com/', 'active', false, '2003-12-22 15:00:00', '2015-12-31 23:59:59', 'volvo, chrysler, audi, bmw, stylad, mercedes, porsche, peugeot, vw, suzuki, toyota,', '-', 'patrik', '-', '-', 'patrik', 'patrik', 23, 10, 1020);
INSERT INTO stores VALUES (12482, 'Eurogarage', 'info_eurogarage.com@blocket.se', '0410-405 60', '0410-474 86', 'J�rnv�gsgatan 15', '231 44', 'TRELLEBORG', 'euro_logo_liten.gif', 'Vi har bilen du dr�mmer om.', 'Vi p� Eurogarage har m�nga �rs erfarenhet av bilimport. Vi kan erbjuda Er import av i princip alla typer av fordon, inregistrerade och klara att anv�ndas i Sverige.
Med v�r erfarenhet kan vi ge Er ett tryggt k�p med garantier.', 'http://www.eurogarage.com/', 'active', false, '2003-12-22 15:00:00', '2015-12-31 23:59:59', 'volvo, chrysler, audi, bmw, stylad, mercedes, porsche, peugeot, vw, suzuki, toyota,', '-', 'patrik', '-', '-', 'patrik', 'patrik', 23, 11, 1020);
INSERT INTO stores VALUES (12483, 'Eurogarage', 'info_eurogarage.com@blocket.se', '0410-405 60', '0410-474 86', 'J�rnv�gsgatan 15', '231 44', 'TRELLEBORG', 'euro_logo_liten.gif', 'Vi har bilen du dr�mmer om.', 'Vi p� Eurogarage har m�nga �rs erfarenhet av bilimport. Vi kan erbjuda Er import av i princip alla typer av fordon, inregistrerade och klara att anv�ndas i Sverige.
Med v�r erfarenhet kan vi ge Er ett tryggt k�p med garantier.', 'http://www.eurogarage.com/', 'active', false, '2003-12-22 15:00:00', '2015-12-31 23:59:59', 'volvo, chrysler, audi, bmw, stylad, mercedes, porsche, peugeot, vw, suzuki, toyota,', '-', 'patrik', '-', '-', 'patrik', 'patrik', 23, 12, 1020);
INSERT INTO stores VALUES (12484, 'Eurogarage', 'info_eurogarage.com@blocket.se', '0410-405 60', '0410-474 86', 'J�rnv�gsgatan 15', '231 44', 'TRELLEBORG', 'euro_logo_liten.gif', 'Vi har bilen du dr�mmer om.', 'Vi p� Eurogarage har m�nga �rs erfarenhet av bilimport. Vi kan erbjuda Er import av i princip alla typer av fordon, inregistrerade och klara att anv�ndas i Sverige.
Med v�r erfarenhet kan vi ge Er ett tryggt k�p med garantier.', 'http://www.eurogarage.com/', 'active', false, '2003-12-22 15:00:00', '2015-12-31 23:59:59', 'volvo, chrysler, audi, bmw, stylad, mercedes, porsche, peugeot, vw, suzuki, toyota,', '-', 'patrik', '-', '-', 'patrik', 'patrik', 23, 13, 1020);
INSERT INTO stores VALUES (12485, 'Eurogarage', 'info_eurogarage.com@blocket.se', '0410-405 60', '0410-474 86', 'J�rnv�gsgatan 15', '231 44', 'TRELLEBORG', 'euro_logo_liten.gif', 'Vi har bilen du dr�mmer om.', 'Vi p� Eurogarage har m�nga �rs erfarenhet av bilimport. Vi kan erbjuda Er import av i princip alla typer av fordon, inregistrerade och klara att anv�ndas i Sverige.
Med v�r erfarenhet kan vi ge Er ett tryggt k�p med garantier.', 'http://www.eurogarage.com/', 'active', false, '2003-12-22 15:00:00', '2015-12-31 23:59:59', 'volvo, chrysler, audi, bmw, stylad, mercedes, porsche, peugeot, vw, suzuki, toyota,', '-', 'patrik', '-', '-', 'patrik', 'patrik', 23, 14, 1020);
INSERT INTO stores VALUES (12486, 'Eurogarage', 'info_eurogarage.com@blocket.se', '0410-405 60', '0410-474 86', 'J�rnv�gsgatan 15', '231 44', 'TRELLEBORG', 'euro_logo_liten.gif', 'Vi har bilen du dr�mmer om.', 'Vi p� Eurogarage har m�nga �rs erfarenhet av bilimport. Vi kan erbjuda Er import av i princip alla typer av fordon, inregistrerade och klara att anv�ndas i Sverige.
Med v�r erfarenhet kan vi ge Er ett tryggt k�p med garantier.', 'http://www.eurogarage.com/', 'active', false, '2003-12-22 15:00:00', '2015-12-31 23:59:59', 'volvo, chrysler, audi, bmw, stylad, mercedes, porsche, peugeot, vw, suzuki, toyota,', '-', 'patrik', '-', '-', 'patrik', 'patrik', 23, 15, 1020);
INSERT INTO stores VALUES (12487, 'Eurogarage', 'info_eurogarage.com@blocket.se', '0410-405 60', '0410-474 86', 'J�rnv�gsgatan 15', '231 44', 'TRELLEBORG', 'euro_logo_liten.gif', 'Vi har bilen du dr�mmer om.', 'Vi p� Eurogarage har m�nga �rs erfarenhet av bilimport. Vi kan erbjuda Er import av i princip alla typer av fordon, inregistrerade och klara att anv�ndas i Sverige.
Med v�r erfarenhet kan vi ge Er ett tryggt k�p med garantier.', 'http://www.eurogarage.com/', 'active', false, '2003-12-22 15:00:00', '2015-12-31 23:59:59', 'volvo, chrysler, audi, bmw, stylad, mercedes, porsche, peugeot, vw, suzuki, toyota,', '-', 'patrik', '-', '-', 'patrik', 'patrik', 23, 16, 1020);
INSERT INTO stores VALUES (12488, 'Eurogarage', 'info_eurogarage.com@blocket.se', '0410-405 60', '0410-474 86', 'J�rnv�gsgatan 15', '231 44', 'TRELLEBORG', 'euro_logo_liten.gif', 'Vi har bilen du dr�mmer om.', 'Vi p� Eurogarage har m�nga �rs erfarenhet av bilimport. Vi kan erbjuda Er import av i princip alla typer av fordon, inregistrerade och klara att anv�ndas i Sverige.
Med v�r erfarenhet kan vi ge Er ett tryggt k�p med garantier.', 'http://www.eurogarage.com/', 'active', false, '2003-12-22 15:00:00', '2015-12-31 23:59:59', 'volvo, chrysler, audi, bmw, stylad, mercedes, porsche, peugeot, vw, suzuki, toyota,', '-', 'patrik', '-', '-', 'patrik', 'patrik', 22, 0, 1020);
INSERT INTO stores VALUES (12489, 'Eurogarage', 'info_eurogarage.com@blocket.se', '0410-405 60', '0410-474 86', 'J�rnv�gsgatan 15', '231 44', 'TRELLEBORG', 'euro_logo_liten.gif', 'Vi har bilen du dr�mmer om.', 'Vi p� Eurogarage har m�nga �rs erfarenhet av bilimport. Vi kan erbjuda Er import av i princip alla typer av fordon, inregistrerade och klara att anv�ndas i Sverige.
Med v�r erfarenhet kan vi ge Er ett tryggt k�p med garantier.', 'http://www.eurogarage.com/', 'active', false, '2003-12-22 15:00:00', '2015-12-31 23:59:59', 'volvo, chrysler, audi, bmw, stylad, mercedes, porsche, peugeot, vw, suzuki, toyota,', '-', 'patrik', '-', '-', 'patrik', 'patrik', 6, 0, 1020);
INSERT INTO stores VALUES (12490, 'Eurogarage', 'info_eurogarage.com@blocket.se', '0410-405 60', '0410-474 86', 'J�rnv�gsgatan 15', '231 44', 'TRELLEBORG', 'euro_logo_liten.gif', 'Vi har bilen du dr�mmer om.', 'Vi p� Eurogarage har m�nga �rs erfarenhet av bilimport. Vi kan erbjuda Er import av i princip alla typer av fordon, inregistrerade och klara att anv�ndas i Sverige.
Med v�r erfarenhet kan vi ge Er ett tryggt k�p med garantier.', 'http://www.eurogarage.com/', 'active', false, '2003-12-22 15:00:00', '2015-12-31 23:59:59', 'volvo, chrysler, audi, bmw, stylad, mercedes, porsche, peugeot, vw, suzuki, toyota,', '-', 'patrik', '-', '-', 'patrik', 'patrik', 19, 0, 1020);
INSERT INTO stores VALUES (12491, 'Eurogarage', 'info_eurogarage.com@blocket.se', '0410-405 60', '0410-474 86', 'J�rnv�gsgatan 15', '231 44', 'TRELLEBORG', 'euro_logo_liten.gif', 'Vi har bilen du dr�mmer om.', 'Vi p� Eurogarage har m�nga �rs erfarenhet av bilimport. Vi kan erbjuda Er import av i princip alla typer av fordon, inregistrerade och klara att anv�ndas i Sverige.
Med v�r erfarenhet kan vi ge Er ett tryggt k�p med garantier.', 'http://www.eurogarage.com/', 'active', false, '2003-12-22 15:00:00', '2015-12-31 23:59:59', 'volvo, chrysler, audi, bmw, stylad, mercedes, porsche, peugeot, vw, suzuki, toyota,', '-', 'patrik', '-', '-', 'patrik', 'patrik', 5, 0, 1020);
INSERT INTO stores VALUES (12492, 'Eurogarage', 'info_eurogarage.com@blocket.se', '0410-405 60', '0410-474 86', 'J�rnv�gsgatan 15', '231 44', 'TRELLEBORG', 'euro_logo_liten.gif', 'Vi har bilen du dr�mmer om.', 'Vi p� Eurogarage har m�nga �rs erfarenhet av bilimport. Vi kan erbjuda Er import av i princip alla typer av fordon, inregistrerade och klara att anv�ndas i Sverige.
Med v�r erfarenhet kan vi ge Er ett tryggt k�p med garantier.', 'http://www.eurogarage.com/', 'active', false, '2003-12-22 15:00:00', '2015-12-31 23:59:59', 'volvo, chrysler, audi, bmw, stylad, mercedes, porsche, peugeot, vw, suzuki, toyota,', '-', 'patrik', '-', '-', 'patrik', 'patrik', 15, 0, 1020);
INSERT INTO stores VALUES (12493, 'Eurogarage', 'info_eurogarage.com@blocket.se', '0410-405 60', '0410-474 86', 'J�rnv�gsgatan 15', '231 44', 'TRELLEBORG', 'euro_logo_liten.gif', 'Vi har bilen du dr�mmer om.', 'Vi p� Eurogarage har m�nga �rs erfarenhet av bilimport. Vi kan erbjuda Er import av i princip alla typer av fordon, inregistrerade och klara att anv�ndas i Sverige.
Med v�r erfarenhet kan vi ge Er ett tryggt k�p med garantier.', 'http://www.eurogarage.com/', 'active', false, '2003-12-22 15:00:00', '2015-12-31 23:59:59', 'volvo, chrysler, audi, bmw, stylad, mercedes, porsche, peugeot, vw, suzuki, toyota,', '-', 'patrik', '-', '-', 'patrik', 'patrik', 20, 0, 1020);
INSERT INTO stores VALUES (12494, 'Eurogarage', 'info_eurogarage.com@blocket.se', '0410-405 60', '0410-474 86', 'J�rnv�gsgatan 15', '231 44', 'TRELLEBORG', 'euro_logo_liten.gif', 'Vi har bilen du dr�mmer om.', 'Vi p� Eurogarage har m�nga �rs erfarenhet av bilimport. Vi kan erbjuda Er import av i princip alla typer av fordon, inregistrerade och klara att anv�ndas i Sverige.
Med v�r erfarenhet kan vi ge Er ett tryggt k�p med garantier.', 'http://www.eurogarage.com/', 'active', false, '2003-12-22 15:00:00', '2015-12-31 23:59:59', 'volvo, chrysler, audi, bmw, stylad, mercedes, porsche, peugeot, vw, suzuki, toyota,', '-', 'patrik', '-', '-', 'patrik', 'patrik', 3, 0, 1020);
INSERT INTO stores VALUES (12495, 'Eurogarage', 'info_eurogarage.com@blocket.se', '0410-405 60', '0410-474 86', 'J�rnv�gsgatan 15', '231 44', 'TRELLEBORG', 'euro_logo_liten.gif', 'Vi har bilen du dr�mmer om.', 'Vi p� Eurogarage har m�nga �rs erfarenhet av bilimport. Vi kan erbjuda Er import av i princip alla typer av fordon, inregistrerade och klara att anv�ndas i Sverige.
Med v�r erfarenhet kan vi ge Er ett tryggt k�p med garantier.', 'http://www.eurogarage.com/', 'active', false, '2003-12-22 15:00:00', '2015-12-31 23:59:59', 'volvo, chrysler, audi, bmw, stylad, mercedes, porsche, peugeot, vw, suzuki, toyota,', '-', 'patrik', '-', '-', 'patrik', 'patrik', 17, 0, 1020);
INSERT INTO stores VALUES (12496, 'Eurogarage', 'info_eurogarage.com@blocket.se', '0410-405 60', '0410-474 86', 'J�rnv�gsgatan 15', '231 44', 'TRELLEBORG', 'euro_logo_liten.gif', 'Vi har bilen du dr�mmer om.', 'Vi p� Eurogarage har m�nga �rs erfarenhet av bilimport. Vi kan erbjuda Er import av i princip alla typer av fordon, inregistrerade och klara att anv�ndas i Sverige.
Med v�r erfarenhet kan vi ge Er ett tryggt k�p med garantier.', 'http://www.eurogarage.com/', 'active', false, '2003-12-22 15:00:00', '2015-12-31 23:59:59', 'volvo, chrysler, audi, bmw, stylad, mercedes, porsche, peugeot, vw, suzuki, toyota,', '-', 'patrik', '-', '-', 'patrik', 'patrik', 18, 0, 1020);
INSERT INTO stores VALUES (12497, 'Eurogarage', 'info_eurogarage.com@blocket.se', '0410-405 60', '0410-474 86', 'J�rnv�gsgatan 15', '231 44', 'TRELLEBORG', 'euro_logo_liten.gif', 'Vi har bilen du dr�mmer om.', 'Vi p� Eurogarage har m�nga �rs erfarenhet av bilimport. Vi kan erbjuda Er import av i princip alla typer av fordon, inregistrerade och klara att anv�ndas i Sverige.
Med v�r erfarenhet kan vi ge Er ett tryggt k�p med garantier.', 'http://www.eurogarage.com/', 'active', false, '2003-12-22 15:00:00', '2015-12-31 23:59:59', 'volvo, chrysler, audi, bmw, stylad, mercedes, porsche, peugeot, vw, suzuki, toyota,', '-', 'patrik', '-', '-', 'patrik', 'patrik', 21, 0, 1020);
INSERT INTO stores VALUES (12498, 'Eurogarage', 'info_eurogarage.com@blocket.se', '0410-405 60', '0410-474 86', 'J�rnv�gsgatan 15', '231 44', 'TRELLEBORG', 'euro_logo_liten.gif', 'Vi har bilen du dr�mmer om.', 'Vi p� Eurogarage har m�nga �rs erfarenhet av bilimport. Vi kan erbjuda Er import av i princip alla typer av fordon, inregistrerade och klara att anv�ndas i Sverige.
Med v�r erfarenhet kan vi ge Er ett tryggt k�p med garantier.', 'http://www.eurogarage.com/', 'active', false, '2003-12-22 15:00:00', '2015-12-31 23:59:59', 'volvo, chrysler, audi, bmw, stylad, mercedes, porsche, peugeot, vw, suzuki, toyota,', '-', 'patrik', '-', '-', 'patrik', 'patrik', 1, 0, 1020);
INSERT INTO stores VALUES (12499, 'Eurogarage', 'info_eurogarage.com@blocket.se', '0410-405 60', '0410-474 86', 'J�rnv�gsgatan 15', '231 44', 'TRELLEBORG', 'euro_logo_liten.gif', 'Vi har bilen du dr�mmer om.', 'Vi p� Eurogarage har m�nga �rs erfarenhet av bilimport. Vi kan erbjuda Er import av i princip alla typer av fordon, inregistrerade och klara att anv�ndas i Sverige.
Med v�r erfarenhet kan vi ge Er ett tryggt k�p med garantier.', 'http://www.eurogarage.com/', 'active', false, '2003-12-22 15:00:00', '2015-12-31 23:59:59', 'volvo, chrysler, audi, bmw, stylad, mercedes, porsche, peugeot, vw, suzuki, toyota,', '-', 'patrik', '-', '-', 'patrik', 'patrik', 13, 0, 1020);
INSERT INTO stores VALUES (12500, 'Eurogarage', 'info_eurogarage.com@blocket.se', '0410-405 60', '0410-474 86', 'J�rnv�gsgatan 15', '231 44', 'TRELLEBORG', 'euro_logo_liten.gif', 'Vi har bilen du dr�mmer om.', 'Vi p� Eurogarage har m�nga �rs erfarenhet av bilimport. Vi kan erbjuda Er import av i princip alla typer av fordon, inregistrerade och klara att anv�ndas i Sverige.
Med v�r erfarenhet kan vi ge Er ett tryggt k�p med garantier.', 'http://www.eurogarage.com/', 'active', false, '2003-12-22 15:00:00', '2015-12-31 23:59:59', 'volvo, chrysler, audi, bmw, stylad, mercedes, porsche, peugeot, vw, suzuki, toyota,', '-', 'patrik', '-', '-', 'patrik', 'patrik', 11, 0, 1020);
INSERT INTO stores VALUES (12501, 'Eurogarage', 'info_eurogarage.com@blocket.se', '0410-405 60', '0410-474 86', 'J�rnv�gsgatan 15', '231 44', 'TRELLEBORG', 'euro_logo_liten.gif', 'Vi har bilen du dr�mmer om.', 'Vi p� Eurogarage har m�nga �rs erfarenhet av bilimport. Vi kan erbjuda Er import av i princip alla typer av fordon, inregistrerade och klara att anv�ndas i Sverige.
Med v�r erfarenhet kan vi ge Er ett tryggt k�p med garantier.', 'http://www.eurogarage.com/', 'active', false, '2003-12-22 15:00:00', '2015-12-31 23:59:59', 'volvo, chrysler, audi, bmw, stylad, mercedes, porsche, peugeot, vw, suzuki, toyota,', '-', 'patrik', '-', '-', 'patrik', 'patrik', 12, 0, 1020);
INSERT INTO stores VALUES (12502, 'Eurogarage', 'info_eurogarage.com@blocket.se', '0410-405 60', '0410-474 86', 'J�rnv�gsgatan 15', '231 44', 'TRELLEBORG', 'euro_logo_liten.gif', 'Vi har bilen du dr�mmer om.', 'Vi p� Eurogarage har m�nga �rs erfarenhet av bilimport. Vi kan erbjuda Er import av i princip alla typer av fordon, inregistrerade och klara att anv�ndas i Sverige.
Med v�r erfarenhet kan vi ge Er ett tryggt k�p med garantier.', 'http://www.eurogarage.com/', 'active', false, '2003-12-22 15:00:00', '2015-12-31 23:59:59', 'volvo, chrysler, audi, bmw, stylad, mercedes, porsche, peugeot, vw, suzuki, toyota,', '-', 'patrik', '-', '-', 'patrik', 'patrik', 10, 0, 1020);
INSERT INTO stores VALUES (12503, 'Eurogarage', 'info_eurogarage.com@blocket.se', '0410-405 60', '0410-474 86', 'J�rnv�gsgatan 15', '231 44', 'TRELLEBORG', 'euro_logo_liten.gif', 'Vi har bilen du dr�mmer om.', 'Vi p� Eurogarage har m�nga �rs erfarenhet av bilimport. Vi kan erbjuda Er import av i princip alla typer av fordon, inregistrerade och klara att anv�ndas i Sverige.
Med v�r erfarenhet kan vi ge Er ett tryggt k�p med garantier.', 'http://www.eurogarage.com/', 'active', false, '2003-12-22 15:00:00', '2015-12-31 23:59:59', 'volvo, chrysler, audi, bmw, stylad, mercedes, porsche, peugeot, vw, suzuki, toyota,', '-', 'patrik', '-', '-', 'patrik', 'patrik', 7, 0, 1020);
INSERT INTO stores VALUES (12504, 'Eurogarage', 'info_eurogarage.com@blocket.se', '0410-405 60', '0410-474 86', 'J�rnv�gsgatan 15', '231 44', 'TRELLEBORG', 'euro_logo_liten.gif', 'Vi har bilen du dr�mmer om.', 'Vi p� Eurogarage har m�nga �rs erfarenhet av bilimport. Vi kan erbjuda Er import av i princip alla typer av fordon, inregistrerade och klara att anv�ndas i Sverige.
Med v�r erfarenhet kan vi ge Er ett tryggt k�p med garantier.', 'http://www.eurogarage.com/', 'active', false, '2003-12-22 15:00:00', '2015-12-31 23:59:59', 'volvo, chrysler, audi, bmw, stylad, mercedes, porsche, peugeot, vw, suzuki, toyota,', '-', 'patrik', '-', '-', 'patrik', 'patrik', 2, 0, 1020);
INSERT INTO stores VALUES (12505, 'Eurogarage', 'info_eurogarage.com@blocket.se', '0410-405 60', '0410-474 86', 'J�rnv�gsgatan 15', '231 44', 'TRELLEBORG', 'euro_logo_liten.gif', 'Vi har bilen du dr�mmer om.', 'Vi p� Eurogarage har m�nga �rs erfarenhet av bilimport. Vi kan erbjuda Er import av i princip alla typer av fordon, inregistrerade och klara att anv�ndas i Sverige.
Med v�r erfarenhet kan vi ge Er ett tryggt k�p med garantier.', 'http://www.eurogarage.com/', 'active', false, '2003-12-22 15:00:00', '2015-12-31 23:59:59', 'volvo, chrysler, audi, bmw, stylad, mercedes, porsche, peugeot, vw, suzuki, toyota,', '-', 'patrik', '-', '-', 'patrik', 'patrik', 4, 0, 1020);
INSERT INTO stores VALUES (12506, 'Eurogarage', 'info_eurogarage.com@blocket.se', '0410-405 60', '0410-474 86', 'J�rnv�gsgatan 15', '231 44', 'TRELLEBORG', 'euro_logo_liten.gif', 'Vi har bilen du dr�mmer om.', 'Vi p� Eurogarage har m�nga �rs erfarenhet av bilimport. Vi kan erbjuda Er import av i princip alla typer av fordon, inregistrerade och klara att anv�ndas i Sverige.
Med v�r erfarenhet kan vi ge Er ett tryggt k�p med garantier.', 'http://www.eurogarage.com/', 'active', false, '2003-12-22 15:00:00', '2015-12-31 23:59:59', 'volvo, chrysler, audi, bmw, stylad, mercedes, porsche, peugeot, vw, suzuki, toyota,', '-', 'patrik', '-', '-', 'patrik', 'patrik', 9, 0, 1020);
INSERT INTO stores VALUES (12507, 'Eurogarage', 'info_eurogarage.com@blocket.se', '0410-405 60', '0410-474 86', 'J�rnv�gsgatan 15', '231 44', 'TRELLEBORG', 'euro_logo_liten.gif', 'Vi har bilen du dr�mmer om.', 'Vi p� Eurogarage har m�nga �rs erfarenhet av bilimport. Vi kan erbjuda Er import av i princip alla typer av fordon, inregistrerade och klara att anv�ndas i Sverige.
Med v�r erfarenhet kan vi ge Er ett tryggt k�p med garantier.', 'http://www.eurogarage.com/', 'active', false, '2003-12-22 15:00:00', '2015-12-31 23:59:59', 'volvo, chrysler, audi, bmw, stylad, mercedes, porsche, peugeot, vw, suzuki, toyota,', '-', 'patrik', '-', '-', 'patrik', 'patrik', 16, 0, 1020);
INSERT INTO stores VALUES (12508, 'Eurogarage', 'info_eurogarage.com@blocket.se', '0410-405 60', '0410-474 86', 'J�rnv�gsgatan 15', '231 44', 'TRELLEBORG', 'euro_logo_liten.gif', 'Vi har bilen du dr�mmer om.', 'Vi p� Eurogarage har m�nga �rs erfarenhet av bilimport. Vi kan erbjuda Er import av i princip alla typer av fordon, inregistrerade och klara att anv�ndas i Sverige.
Med v�r erfarenhet kan vi ge Er ett tryggt k�p med garantier.', 'http://www.eurogarage.com/', 'active', false, '2003-12-22 15:00:00', '2015-12-31 23:59:59', 'volvo, chrysler, audi, bmw, stylad, mercedes, porsche, peugeot, vw, suzuki, toyota,', '-', 'patrik', '-', '-', 'patrik', 'patrik', 8, 0, 1020);
INSERT INTO stores VALUES (12509, 'Eurogarage', 'info_eurogarage.com@blocket.se', '0410-405 60', '0410-474 86', 'J�rnv�gsgatan 15', '231 44', 'TRELLEBORG', 'euro_logo_liten.gif', 'Vi har bilen du dr�mmer om.', 'Vi p� Eurogarage har m�nga �rs erfarenhet av bilimport. Vi kan erbjuda Er import av i princip alla typer av fordon, inregistrerade och klara att anv�ndas i Sverige.
Med v�r erfarenhet kan vi ge Er ett tryggt k�p med garantier.', 'http://www.eurogarage.com/', 'active', false, '2003-12-22 15:00:00', '2015-12-31 23:59:59', 'volvo, chrysler, audi, bmw, stylad, mercedes, porsche, peugeot, vw, suzuki, toyota,', '-', 'patrik', '-', '-', 'patrik', 'patrik', 14, 0, 1020);
INSERT INTO stores VALUES (12516, 'Tomelilla Husvagnar', 'tomelilla.husvagnar_telia.com@blocket.se', '0417-128 10', '0417-156 22', 'Nygatan 37', '273 36', 'TOMELILLA', 'Logga.jpg', 'Vi har husvagnen som passar dig.', '�terf�rs�ljare f�r LMC Husvagnar.
<p>
Vi tillhandah�ller tillbeh�r och reservdelar fr�n bl a Kama Fritid och Isabella.
<br>
I v�r butik finner du ett brett utbud av de vanligaste tillbeh�ren.
<p>
K�per ni husvagn hos oss f�r ni m�jligheten att utnyttja Wasa Finans med 3,90 % r�nta.
<p>
V�ra �ppettider:<br>
M�n-Fre 10.00-18.00<br>
L�rdag  10.00-13.00<br>
S�ndagar st�ngt', 'http://www.tomelillahusvagnar.se/', 'active', false, '2004-01-09 09:45:00', '2016-03-01 23:59:59', 'Husvagn, Husvagnar, Cabby, Knaus, Polar, Kabe, Adria, Solifer, S�vsj�, Burstner, Gasol, Gasolkamin, LMC,', '-', 'patrik', '-', '-', 'patrik', 'patrik', 23, 15, 1100);
INSERT INTO stores VALUES (12517, 'Sets�s Silver', 'm.set_telia.com@blocket.se', '0709-602 337', '-', 'Gotland', '.', 'VISBY', 'logo.gif', 'Tillverkning och f�rs�ljning av silver och guld smycken.', 'Atelj� med Silver- Guldsmedja, handgjorda smycken i silver och guld med fornhistorisk
formgivning.<br>
Handgjorda prydnadsknivar och bruksknivar i exklusiva material.
Tenntr�dsarbeten efter Samiska f�rlagor.
<p>
V�r f�rs�ljning sker p� marknadsplatser runt om i Sverige och �vriga Europa och givetvis
p� Gotland d�r vi �r verksamma hela �ret om.
Under Tornespelsmarknaderna och Medeltidsveckan i Visby kan du bes�ka v�rat marknadsst�nd, under andra tider kontakta oss per telefon eller e-post.
<p>
Tillverkning �ven p� best�llning, Omarbeten, reparationer mm.
<p>
Ni kan �ven n� oss p� tel. 070-325 21 19', 'http://hem.passagen.se/setssilver', 'active', false, '2004-01-08 10:20:00', '2016-01-08 23:59:59', 'Silver, guld, smycken, kungal�nk, ringar, kedjor, sv�rd, stridsyxor, knivar, �delstenar, armband, Torshammare,
medeltidsveckan, tornerspelsmarknaden 
', '-', 'patrik', '-', '-', 'patrik', 'patrik', 19, 0, 4060);
INSERT INTO stores VALUES (12519, 'Spinnin� Wheel MC Demontering', 'spinninw_algohotellet.se@blocket.se', '08-530 300 15', '08-530 311 45', 'Finkmossev�gen 150', '147 34', 'TUMBA', 'logo.gif', 'St�rst p� begagnade MC-delar. Vi har �ven begagnade mc och mopeder.', 'Om du letar efter delar till din motorcykel har du kommit r�tt! 
Vi har sysslat med f�rs�ljning av begagnade mc-delar sedan 1976. 
Sedan dess har vi demonterat ca. 1500 motorcyklar och tillsammans med 
v�rat systerbolag SPININN`WHEEL A/S i Norge �r vi Nordens absolut st�rsta MC-demontering.
<p>
Vi har totalt en lageryta av 4000 kvm.fullproppade med reservdelar till 
hojar fr�n tidigt 70-tal fram till -99 �rs modell. 

Vi har �ven ett mycket stort lager av NYA delar till KAWASAKI:s 70- och 80-tals modeller.
Lagret best�r av ca 20.000 olika artikelnummer av NYA reservdelar till 
KAWASAKI:s motorcyklar, Jetski:s och 4-hjulingar. 
<p>
�PPETTIDER I BUTIKEN<br>
Vi har fasta tider f�r varuutl�mning �ver disk.
Du m�ste dock ringa innan du bes�ker oss s� att vi hinner plocka fram delarna innan du kommer.
<p>
M�ndag - Torsdag  kl. 14-18
<br>
Fredag kl. 11-15
<br>
Telefontid 09.00-18.00', 'http://www.spinnin-wheel.se', 'active', false, '2004-01-14 10:30:00', '2016-04-01 23:59:59', 'Kawasaki, Honda, Yamaha, Suzuki, Aprilia, Triumph, MC-delar, Begagnat,', '-', 'patrik', '-', '-', 'patrik', 'patrik', 11, 0, 1140);
INSERT INTO stores VALUES (12529, 'Bilfirman i Bara', 'bilfirmanibara_yahoo.se@blocket.se', '040-44 08 99', '040- 44 08 99', 'Malm�v�gen 575', '230 40', 'BARA', '-', 'M�rkesoberoende handlare.', 'F�r bilar i prisklassen 60.000 kronor eller l�gre har vi en unik avbetalningsform. R�ntefritt, endast en uppl�ggningsavgift fr�n 195kr - 395kr Fast m�nadskostnad L�net kan l�sas n�r ni vill - utan extra kostnader.
<p>
�ppettider:<br>
M�-To 12.00-18.00<br>
Fre   12.00-16.00<br>
S�n   10.00-16.00
<p>
V�lkomna!', 'http://bilfirman.se', 'active', false, '2004-01-21 13:00:00', '2016-02-01 23:59:59', 'BMW, Chevrolet, Chrysler, Ford, Mazda, Opel, Saab, Suzuki, Toyota, Volkswagen, Volvo, Kawasaki,', '-', 'patrik', '-', '-', 'patrik', 'patrik', 23, 11, 1020);
INSERT INTO stores VALUES (14193, 'Almby Bil', 'almbybil_hotmail.com@blocket.se', '019-27 04 10', '019-27 04 10', 'Tybbelund', '705 90', '�REBRO', 'butik.gif', 'M�rkesoberoende handlare', '<b>V�lkommen till Almby Bil</b><p>


Vi reparerar alla modeller till bra priser.<br>
Vi s�ljer bilar och motorcyklar med garanti till bra priser.<br>
Vi har �ver 20�rs erfarenhet inom bilbranschen.<p>

', 'http://www.almbybil.com

', 'active', false, '2005-09-30 00:00:00', '2015-12-31 23:59:59', 'BMW, Jaguar, Ford, Jeep Grand Cherokee, Mercedes-Benz, Saab, Volkswagen, VW, Volvo', '-', 'patrik', '-', '-', 'patrik', 'patrik', 8, 0, 1020);
INSERT INTO stores VALUES (12528, 'Saluvagnar i V�ster�s AB', 'info_saluvagnar.se@blocket.se', '021-30 20 75', '021-30 20 76', 'K�vsta 10', '725 93', 'V�STER�S', 'saluvagnar_logo.gif', 'Den lilla bilfirman med personlig serivce.', 'Vi �r �terf�rs�ljare av l�gprisbilen KIA.
<p>
Vi har alltid ett 50-tal nya och beg. bilar hemma till l�ga priser. 
<p>
V�ra �ppettider:<br>
M�n - Fre: 11 - 18 (lunch: 13 - 14)<br>
L�r: 11 - 14 
<p>
Ni �r varmt v�lkomna, gamla som nya kunder.
Vi talar �ven Finska Suomea Puhutaan.
<p> 
<b>H�r finns vi:</b><br>
Timmerstugan, riksv�g 66, 4 km N om Erikslund mot Surahammar
<p>
Ni kan �ven n� oss p� 070-888 08 88.', 'http://www.saluvagnar.se', 'active', false, '2004-01-20 15:40:00', '2016-02-01 23:59:59', 'BMW, Chrysler, Ford, Kia, Mazda, Mercedes, Opel, Pontiac, Renault, Saab, Toyota, Volvo, ', '-', 'patrik', '-', '-', 'patrik', 'patrik', 9, 0, 1020);
INSERT INTO stores VALUES (12534, 'Bilbutiken / Biltorget', 'bilbutiken_telia.com@blocket.se', '0303-23 08 08', '0303-23 08 09', 'Ellesbov�gen 172', '442 90', 'KUNG�LV', 'logo.gif', 'M�rkesoberoende handlare!', 'HEJ OCH V�LKOMMEN!
<p>
�ppet Kung�lv M�n-Fre 9-18, L�r-S�n 11-15.
<p>
Vi f�rs�ker att alltid ha ca 100 bilar i lager d� vi har fasta leverant�rer som f�rser oss med 25 inkommande bilar per m�nad. Som en av G�teborgs st�rsta bils�ljare levererar vi ca 25 s�lda bilar i m�naden.
<p>
V�r butik �r �ppen i H�by med gummib�tar, EU-mopeder, cross & 4-hjulingar.
N�tsidan med butiksfunktion och butiksknapp v�ntas vara klar ca: vecka 17.<p>
Hos oss har Du alltid m�jlighet till f�rm�nlig finansiering. 
Ex: L�na upp till 30,000 till 0%
Se v�r hemsida f�r mer information.
<p>
Nytt f�r i �r �r v�r fullserviceverkstad d�r vi utf�r b�de service & rep.arb.
', 'http://www.bilbutiken.se/', 'active', false, '2004-01-30 16:35:00', '2016-07-01 23:59:59', 'Audi, BMW, Ford, Opel, Volvo, Saab, Hyundai, Nissan, Mitsubishi, Mazda, Fiat, Peugeot, Renault,', '-', 'patrik', '-', '-', 'patrik', 'patrik', 15, 0, 1020);
INSERT INTO stores VALUES (12535, 'Bilbutiken / Bilhallen', 'bilbutiken_telia.com@blocket.se', '0524-235 00', '0524-235 10', 'Tradenv�gen 18', '455 91', 'H�BY', 'logo.gif', 'M�rkesoberoende handlare!', 'HEJ OCH V�LKOMMEN!
<p>
�ppet H�by M�n-Fre 9-18
<p>
Vi f�rs�ker att alltid ha ca 100 bilar i lager d� vi har fasta leverant�rer som f�rser oss med 25 inkommande bilar per m�nad. Som en av G�teborgs st�rsta bils�ljare levererar vi ca 25 s�lda bilar i m�naden.
<p>
V�r butik �r �ppen i H�by med gummib�tar, EU-mopeder, cross & 4-hjulingar.
N�tsidan med butiksfunktion och butiksknapp v�ntas vara klar ca: vecka 17.<p>
Hos oss har Du alltid m�jlighet till f�rm�nlig finansiering. 
Ex: L�na upp till 30,000 till 0%
Se v�r hemsida f�r mer information.
<p>
Nytt f�r i �r �r v�r fullserviceverkstad d�r vi utf�r b�de service & rep.arb.
', 'http://www.bilbutiken.se/', 'active', false, '2004-01-30 16:36:00', '2016-07-01 23:59:59', 'Nissan, Peugeot, Volvo,', '-', 'patrik', '-', '-', 'patrik', 'patrik', 15, 0, 1020);
INSERT INTO stores VALUES (12537, 'Tollarps Bilskrot AB', 'tollarpsskrot_telia.com@blocket.se', '044-31 08 41', '044-31 18 25', 'Tassebodav�gen 121', '290 10', 'TOLLARP', 'logo.gif', 'Milj�certifierad Bilskrot!', 'Tollarps Bilskrot �r milj�certifierade enligt ISO 14001.
<p>
Vi �r �ven anslutna till Sveriges Bilskrotares Riksf�rbund.
<p>
Demonterar nyare f�rs�kringsbilar med specialitet p� Saab, Opel, Mercdes,
VW-gruppen, USA, Japan.
<p>
Vi har alltid ca 30 bilar till salu.
<p>
�ppettider:<br>
M�n - Tor 7.00-17.00<br>
Fre 7.00-15.00<br>
Lunchst�ngt 12.00-13.00<br>', 'http://www.tollarpsbilskrot.com', 'active', false, '2004-01-30 10:55:00', '2016-04-01 23:59:59', 'Bildelar, Bilskrot, Skrot, ', '-', 'patrik', '-', '-', 'patrik', 'patrik', 23, 8, 1040);
INSERT INTO stores VALUES (12540, 'Bilmodecenter AB', 'info_bilmodecenter.se@blocket.se', '031-40 55 56', '031-40 75 81', 'Backa Berg�gata 11', '422 46', 'HISINGS BACKA', 'logo.gif', 'Finn v�gen till en roligare och mer aktiv bilk�rning.', 'Vi vill g�rna att Du med v�r hj�lp skall finna v�gen till en roligare och mer aktiv bilk�rning. 
D�rf�r erbjuder vi st�llbara chassin, cup kit, sport kit, sportfj�dersatser, st�td�mpare
spacers och kr�ngningsh�mmare fr�n H&R.', 'http://www.bilmodecenter.se', 'active', false, '2004-02-05 14:50:00', '2016-04-15 23:59:59', 'S�nkningssats, Sportfj�drar, St�td�mpare, Spacer', '-', 'patrik', '-', '-', 'patrik', 'patrik', 15, 0, 1040);
INSERT INTO stores VALUES (12541, 'Freddes Bil o Motor', 'sockenplansbil_brevet.nu@blocket.se', '08-600 25 35', '08-556 224 88', 'Enskedev�gen 83', '122 63', 'ENSKEDE', 'logo.gif', 'M�rkesoberoende bilhandlare.', 'H�r p� Sockenplan har det s�lts bilar sedan 1968 och vi har haft m�nga n�jda kunder.
<p> 
V�r l�nga erfarenhet och v�ra goda kontakter med kunder och leverant�rer g�r att vi n�stan alltid kan uppfylla Dina �nskem�l n�r det g�ller standardbilar i alla prisklasser. 
<p>
Vi kan erbjuda k�p p� avbetalning med en amorteringstid p� 12-60 m�n, kontantinsats minst 20%. 
L�gsta dagsr�nta till�mpas vid k�p p� avbetalning. Vi l�ser restskulden p� Din bil vid b�de inbyte och k�p. 
<p>
Mobil: 0702-21 50 80', 'http://www.sockenplansbil.com', 'active', false, '2004-03-01 08:45:00', '2016-05-01 23:59:59', 'Volvo, Volkswagen, Seat, Nissan,', '-', 'patrik', '-', '-', 'patrik', 'patrik', 11, 0, 1020);
INSERT INTO stores VALUES (13709, 'Kristianstads Automobil AB', 'christian.svensson_kristianstadsautomobil.se@blocket.se', '044 - 20 60 00', '044 - 12 22 75', 'Blekingev�gen 107', '291 50', 'KRISTIANSTAD', '15_3_05.gif', '�terf�rs�ljare f�r Renault, Volvo', '
 
Den 1 april 1932 fick Gunnar och Bertil Tuvesson tillsammans med Rasmus Nilsson Volvof�rs�ljningen inom ett distrikt med Kristianstad som medelpunkt. Det omsl�t ett omr�de med ca 5 mils radie. 
<p>
<b>Blygsam start</b><br>
Starten skedde i en liten l�genhet med tre sm� rum i den gamla Schenholmska fastigheten vid �stra Boulevarden i Kristianstad. Herrarna Tuvesson och Nilsson inredde ett f�rs�ljningskontor i ett rum med ett matbord som enda m�bel. Den "kamerala" avdelningen fick ett rum med ett gammalt dubbelskrivbord som arbetsbord. Lagret fick det tredje rummet, vilket i och f�r sig var rena lyxutrymmet, eftersom lagret inte var st�rre �n att det rymdes i en resv�ska. Chef var Gunnar Tuvesson, f�rs�ljare var Rasmus Nilsson lagerbevarare Bertil Tuvesson. Detta var f�retagets hela personal under 1932. 
<p>
<b>Expansion</b><br>
Sin f�rsta verkstad �ppnade f�retaget 1933 i sina nya lokaler p� S�dra Boulevarden. N�sta etapp i f�retagets utveckling blev karosseriverkstan i Hammar och dess lagerutrymmen som tillkom 1942. 1964 flyttade hela lastvagnsverksamheten ut till Hammar som ligger ca tre kilometer �ster om Kristianstad. 1972 var det dags att flytta ut resten av f�retaget till Hammar. 1932 fanns det tre anst�llda och oms�ttningen var 57000 kr. 2002 �r antalet anst�llda omkring 80 och oms�ttningen ca 300 miljoner. 
<p>
Kristianstads Automobil AB �r en av de tio �ldsta Volvo�terf�rs�ljarna i Sverige. 
<p>
<b>�ppettider:</b><br>
M�ndag 09.00-19.00 <br>
Tisdag-Fredag 09.00-18.00 <br>
L�rdag 10.00-14.00 <p>
 
', 'http://www.kristianstadsautomobil.se

', 'active', false, '2005-03-15 00:00:00', '2016-04-01 23:59:59', 'Audi, Ford, Hyundai, Mitsubishi, Opel, Peugeot, Renault, Saab, Toyota, Volkswagen, VW, Volvo,   ', '-', 'patrik', '-', '-', 'patrik', 'patrik', 23, 8, 1020);
INSERT INTO stores VALUES (12552, 'Anders Olsson Bil', 'orsa.olsson_telia.com@blocket.se', '063-811 40', '063-811 40', 'Alegatan 41', '831 45', '�STERSUND', '-', 'M�rkesoberoende Handlare.', '�r specialiserad p� nya och begagnade bilar.
<br>
Jag har �ppet M�n-Fre 09.00-17.00
<br>
�vrig tid kan ni n� mig p� 070-659 52 42
<p>
V�lkommen!', '-', 'active', false, '2004-02-20 09:00:00', '2016-07-01 23:59:59', 'Audi, Hyundai, Mercedes, Nissan, Opel, Peugeot, Renault, Volkswagen, Volvo, Saab, Toyota,', '-', 'patrik', '-', '-', 'patrik', 'patrik', 3, 0, 1020);
INSERT INTO stores VALUES (12559, 'Cycle Center AB', 'info_cyclecenter.se@blocket.se', '018-36 70 40', '018-36 78 11', 'Svista', '755 93', 'UPPSALA', 'logo.gif', 'MC-BUTIKEN V�RD EN UTFLYKT.', 'Cycle Center �r �terf�rs�ljare av HD, Aprilia, CCM & Jincheng.
<p>
Dessutom har vi alla tillbeh�ren och kl�derna du beh�ver till dig och din hoj.
<p>
Vi utf�r �ven service. Boka tid via v�r hemsida.
<p>
�ppettider:<br>
Vardagar 10 - 18<br>
Lunchst�ngt 12 -13<br>
L�rdagar 10 - 14<br>
<p>
V�lkommen!', 'http://www.cyclecenter.se/', 'active', false, '2004-02-23 15:05:00', '2016-02-23 23:59:59', 'MC, Harley Davidson, HD, Aprilia, CCM, Jincheng,', '-', 'patrik', '-', '-', 'patrik', 'patrik', 10, 0, 1140);
INSERT INTO stores VALUES (12561, 'Slagsta Biltorg', 'info_slagstabiltorg.se@blocket.se', '08 - 531 719 19', '08 - 531 719 19', 'F�gelviksv�g 5', '145 53', 'NORSBORG', '23_3_05.gif', 'M�rkesoberoende Handlare.', 'Vi har alltid ca: 60 bilar p� lager av blandade m�rken och modeller, allt fr�n sportiga bilar till familjebilar. Priser mellan 30.000 kr - 100.000 kr.
<p>
Vi ordnar �ven finansiering f�rm�nligt. Vi finns bakom Slagsta Centrum ca: tv� mil s�der om Stockholm vid E4/E20 avfart Eker�f�rjan
<p>
�ppettider:<br>
M�n-Fre 11.00-18.00<br>
L�rdag  11.00-17.00<br>
S�ndag St�ngt<br>
�vriga tider alla dagar
kan ni n� mig p� 070-686 97 16
<p>
V�lkommen!', 'http://www.slagstabiltorg.se
', 'active', false, '2005-03-22 00:00:00', '2016-06-01 23:59:59', 'Audi, BMW, Chevrolet, Chrysler, Citro�n, Ford, Honda, Jeep, Kia, Mazda, Mercedes,', '-', 'patrik', '-', '-', 'patrik', 'patrik', 11, 0, 1020);
INSERT INTO stores VALUES (12562, 'Dahlins D�ck & F�lg', 'dahlinsdackofalg_msn.com@blocket.se', '031-92 01 35', '031-92 01 36', 'Gamla Flygplatsv�gen 22', '416 60', 'G�TEBORG', 'logo.gif', 'D�ckspecialisten!', '1. Sommard�ck/Vinterd�ck, Aluminiumf�lgar, St�lf�lgar.
<br>
2. Vi f�rmedlar dina d�ck & f�lgar �t er.
<br>
3. Vi s�ljer �ven d�ck & f�lg p� postorder.
<br>
4. Tidsbest�llning/Drive-in.
<br>
5. Vi tar kort som Visa, Mastercard & Amex.
<br>
6. Fullservice inom d�ck.
<p>
�ppettider:<br>
M�ndag-Torsdag 09.00-18.00<br>
Fredag 09.00-17.00<br>
L�rdagar �ppet mars-april 10.00-14.00
<p>
V�lkomna!', 'http://www.dahlinsdack.se', 'active', false, '2004-02-24 15:35:00', '2016-05-21 23:59:59', 'D�ck, F�lg, Sommard�ck, Vinterd�ck, Aluminiumf�lgar, St�lf�lgar,', '-', 'patrik', '-', '-', 'patrik', 'patrik', 15, 0, 1040);
INSERT INTO stores VALUES (12563, 'CN Bil', 'info_cnbil.net@blocket.se', '0739-86 64 95', '-', 'Fogdegatan 54', '78334', 'S�TER', 'logo.gif', 'M�rkesoberoende Import!', '<li>Import mot best�llning</li>
<li>M�rkesoberoende Import</li>
<li>Fr�n best�llning till import, registrering, besiktning, leverans.</li>
<li>Finans via Wasa kredit.</li>
<p>
Ni kan �ven n� oss p� 0739-86 64 95.', 'http://www.cnbil.net', 'active', false, '2004-02-25 10:01:00', '2016-07-01 23:59:59', 'Audi, BMW, Fiat, Saab, Volkswagen, VW, Volvo, ', '-', 'patrik', '-', '-', 'patrik', 'patrik', 6, 0, 1020);
INSERT INTO stores VALUES (12566, 'Custom MC Tidaholm', 'info_custommc.net@blocket.se', '0708-22 01 93', '0502-598 55', 'Gransiken, Siggestorp', '522 91', 'TIDAHOLM', 'logo.gif', 'Din MC-handlare!', 'F�retaget �gs och drivs av Jan-Olof Jansson. Vi importerar och s�ljer Custom MC. F�retaget finns i Tidaholm men vi s�ljer till personer �ver hela Sverige. 
<p>
Har du n�gra fr�gor �r du v�lkommen att kontakta oss!', 'http://www.custommc.net/', 'active', false, '2004-02-27 13:54:00', '2016-07-01 23:59:59', 'MC, Kawasaki, Yamaha, Suzuki, Dragpipor, Motorb�gar, Rutor, Sissybar,', '-', 'patrik', '-', '-', 'patrik', 'patrik', 13, 0, 1140);
INSERT INTO stores VALUES (12576, 'Bj�re Bil', 'jan.bjore_tele2.se@blocket.se', '0703-19 33 05', '016-770 56', 'M�ls�terg�rd', '635 20', 'ESKILSTUNA', '-', 'K�PER-BYTER-S�LJER.', 'Vi s�ljer f�retr�desvis Svensks�lda fullservade bilar.<br>
Vi samarbetar med GE Capital Bank.
<p>
Ring f�r visning 0703-19 33 05
<p>
V�lkommen!', '-', 'active', false, '2004-03-08 15:00:00', '2016-04-01 23:59:59', 'BMW, Citro�n, Citroen, Mercedes, Seat, Volkswagen,', '-', 'patrik', '-', '-', 'patrik', 'patrik', 12, 0, 1020);
INSERT INTO stores VALUES (12577, 'MMX Racing', 'info_mmxracing.se@blocket.se', '070-28 66 103', '-', 'Djurg�rdsv�gen 10', '136 71', 'HANINGE', 'logo.gif', 'Import�r och �terf�rs�ljare av Stomp Design och Scar racing products', 'MMX Racing �r import�r och �terf�rs�ljare av Stomp Design och Scar racing products
men kommer �ven att ha fullt sortiment av de b�sta m�rken som finns p� marknaden
till alla cross hojar. F�rst och fr�mst kommer MMX att satsa p� 50cc 65cc 85cc
barn och ungdom men har �ven delar till st�rre hojar, Stomp och Scars produkter finns alltid till alla hojar.
<p>
MMX ger nu 15% rabbat p� Stomp & Scar �nda fram till 15 April som �ppningserbjudande!
<p>
MMX Racing Stefan Axelsson  070-286 61 03<br>
Webaff�r www.mmxracing.se', 'http://www.mmxracing.se', 'active', false, '2004-03-10 09:00:00', '2016-05-15 23:59:59', 'Stomp, Scar, Cross, MC, ', '-', 'patrik', '-', '-', 'patrik', 'patrik', 11, 0, 1140);
INSERT INTO stores VALUES (12594, 'Norrt�lje B�t & Motor', 'info_batochmotor.se@blocket.se', '0176-23 23 23', '0176-131 60', 'Stockholmsv�gen 45', '761 43', 'NORRT�LJE', 'logo.gif', '�TER I BRANSCHEN !', 'Vi har nu startat ny verksamhet i Norrt�lje och �nskar alla gamla och nya kunder v�lkomna till v�ra nyrustade lokaler p� Stockholmsv�gen 45 i Norrt�lje(f.d. L�ttviktsmotor/Skoda).
<p>
Vi s�ljer Yamaha-Yamarin-Buster-Tehri-ATV-4-hjulingar-Vattenleksaker-Mopeder samt Byggplast tillbeh�r och Elektronik.
<p>
Med v�r fullserviceverkstad och datadiagnos �r vi certifierade f�r service och garantiarbeten f�r samtliga v�ra produkter.
<p>
Vi k�per - byter � s�ljer � f�rmedlar begagnade b�tar.
<p>
V�lkomna tillbaka �nskar<br>
Rikard, Jan, Johan och Mattias.', 'http://www.batochmotor.se/', 'active', false, '2004-03-12 11:32:00', '2016-05-01 23:59:59', 'b�t, b�tar, terhi, buster, yamaha, yamarin,', '-', 'patrik', '-', '-', 'patrik', 'patrik', 11, 0, 1060);
INSERT INTO stores VALUES (12736, 'Palm Bilfirma', 'info_palmbilfirma.se@blocket.se', '08-86 94 93', '08-86 94 93', 'Turingev�gen 61', '125 43', '�LVSJ�', 'palm_logo.gif', 'S�ljer, Byter, och K�per begagnade bilar.', 'Vi S�ljer, Byter, och K�per begagnade bilar. Vi betalar extra f�r kombi, l�gmilare samt v�lv�rdade bilar. Genom l�ga omkostnader har vi m�jlighet att h�lla mycket konkurrenskraftiga  priser.
<p>
�ppettider:<br>
M�ndag - Torsdag 10.00-18.00<br>
Fredag 10.00-17.00<br>
L�rdag 10.00-14.00
<p>
V�lkommen!', 'http://www.palmbilfirma.se/
', 'active', false, '2004-06-01 00:00:00', '2016-10-01 23:59:59', 'Audi, Chrysler, Ford, Hyundai, Mazda, Mercedes, MB, Mitsubishi, Opel, Peugeot, Seat, Skoda, Suzuki, VW, Volkswagen, Volvo, ', '-', 'patrik', '-', '-', 'patrik', 'patrik', 11, 0, 1020);
INSERT INTO stores VALUES (12600, 'M Fritid', 'info_mfritid.se@blocket.se', '0499-106 10', '0499-444 50', '�lgerumsv�gen 29', '383 22', 'M�NSTER�S', 'logo.gif', 'Din Husvagnshandlare i s�dra Sverige!', '�terf�rs�ljare av Adria, Wilk, Tabbert och T_b.
<br>
Stort utbud av begagnade husvagnar.
<p>
Vi har �ven trailers fr�n Star Trailer samt Reko Trailer.
<p>
V�ra �ppettider:<br>
M�n - Tor 9 - 18<br>
Fre 9 - 17<br>
L�r 10 - 13<br>
<p>
V�lkomna!', 'http://www.mfritid.se', 'active', false, '2004-03-17 12:00:00', '2016-09-18 23:59:59', 'husvagn, adria, wilk, tabbert, t_b, sl�pvagn', '-', 'patrik', '-', '-', 'patrik', 'patrik', 18, 0, 1100);
INSERT INTO stores VALUES (12629, 'Sj�torps Marina AB', 'swedspin_telia.com@blocket.se', '0501 - 511 55', '0501 - 511 00', 'Sj�fallsv�gen 21', '540 66', 'Sj�torp', 'logo.gif', 'Nya och Begagnade B�tar', 'V�lkommen till Sj�torps Marina,<br>
vi ligger precis utanf�r<br>
G�ta Kanals mynning i V�nern<br>
och 19 km norr om Mariestad.<br>
Hos oss hittar ni nya b�tar fr�n HR,<br>
begagnade b�tar, b�ttrailers, tillbeh�r<br>
och service.<br>', 'http://www.sjotorpsmarina.se', 'active', false, '2004-03-31 00:00:00', '2016-06-01 23:59:59', 'B�t, B�tar, B�tservice, ', '-', 'patrik', '-', '-', 'patrik', 'patrik', 13, 0, 1060);
INSERT INTO stores VALUES (12630, 'Jezyk Bil', 'z.jezyk_telia.com@blocket.se', '019 - 22 63 61', '019 - 22 63 61', 'Hovsta H�ssleby', '705 91', '�rebro', 'logga.gif', 'M�rkesoberoende Handlare.', '�ppettider:<br>
Vardagar 10.00-18.00<br>
L�rdagar 10.00-14.00<br>
<br>
Ni kan n� mig p� mobilnr: 070-659 25 80<br>
<br>
V�lkommen!<br>
', 'http://www.bytbil.com/jezykbil/', 'active', false, '2004-03-31 00:00:00', '2016-04-01 23:59:59', 'Audi, Opel, Ford, Jeep, Mercedes, Nissan, Suzuki, Toyota, Volkswagen, Volvo,', '-', 'patrik', '-', '-', 'patrik', 'patrik', 8, 0, 1020);
INSERT INTO stores VALUES (12631, 'Bil och Fritid i J�mtland AB', 'info_bil-fritid.se@blocket.se', '0693 - 314 40', '0693 - 314 40', 'Bergsv�gen 2', '840 58', 'PILGRIMSTAD', '22_12_04.gif', 'Det mesta du beh�ver f�r din dr�msemester och fritid!', 'Levereras alltid nyckelf�rdiga.<br>
Hem till dig om du vill, i hela Sverige! <br>
Stor service gjord (Byte av olja, oljefilter,<br> luftfilter, t�ndstift samt kamrem vid behov). <br>
Kan �ven eftermontera dragkrok, stereo, <br> mobiltelefon, mm. <br>
Bygger �ven om bilen efter dina behov, tex<br> handikappanpassning. <br>', 'http://www.bil-fritid.se', 'active', false, '2004-04-01 00:00:00', '2016-04-15 23:59:59', 'oljefilter, luftfilter, t�ndstift, dragkrok, stereo, mobiltelefon, husbilar, husbil,', '-', 'patrik', '-', '-', 'patrik', 'patrik', 3, 0, 1100);
INSERT INTO stores VALUES (12634, 'MC-Proffsen i J�nk�ping', 'info_cismc.se@blocket.se', '036-705 70', '036-705 77', 'Norrahammarsv�gen 123', '556 27', 'J�NK�PING', 'logo.gif', 'Nya och begagnade motorcyklar, skoters och mopeder.', 'Ci:s MC ligger i J�nk�ping i Hovsl�tt och har funnits i 28 �r. <br>Vi s�ljer nya och begagnade motorcyklar, skoters och mopeder.
<p>
Vi servar alla MC-m�rken men �r specialiserade f�r Harley-Davidsson, Honda och Suzuki.
<p>
Vi �r anslutna till SMR - vilket st�r f�r Sveriges Motorcykelhandlares Riksf�rbund.', 'http://www.begmc.se
', 'active', false, '2004-04-01 10:30:00', '2016-06-01 23:59:59', 'Shoei, AGV, Arlen Ness, Nolan, Alpine-Stars, Halvarsson, Jofama, Lindstrand, Micron, Pirelli, NGK, DID, RK, Yoshimura, Metzeler, Dunlop och Continental, Harley-Davidsson, Honda, Suzuki, MC, Motorcykel, Moped, Skoter,', '-', 'patrik', '-', '-', 'patrik', 'patrik', 17, 0, 1140);
INSERT INTO stores VALUES (12638, 'Mickes & Connys Motor AB, MC', 'info_mickesmotor.se@blocket.se', '0470-70 08 80', '0470-70 08 89', 'Hjalmar Petris v�g 57-59', '352 46', 'V�XJ�', 'logga.gif', 'Stort utbud av nya och begagnade MC', 'V�lkommen till Mickes Motor. <br>
Alltid mer �n 100 attraktiva nya och begagnade MC i lager. <br>
Som vanligt har vi massor av b�tar och b�tmotorer p� lager. <br>
Naturligtvis byter vi MC mot b�t och vice versa! <p>
Har Du problem att f� hem Din nya MC eller b�t s� erbjuder vi Fogelsta biltransporter och b�ttrailers.
', 'http://www.mickesmotor.se', 'active', false, '2003-01-01 00:00:00', '2017-03-31 23:59:59', 'b�t, MC, HD, Honda, biltransport, b�ttrailers, Fogelsta, motorer, ', '-', 'patrik', '-', '-', 'patrik', 'patrik', 21, 0, 1140);
INSERT INTO stores VALUES (12639, 'Din Bil Stockholm AB', 'beg.sthlmsoder_dinbil.se@blocket.se', '08-503 33 242', '08-503 330 70', 'Hammarby Fabriksv�g 45', '120 08', 'STOCKHOLM', 'logo.gif', 'Sveriges st�rsta auktoriserade �terf�rs�ljare!', 'Med v�ra 14 f�rs�ljningsanl�ggningar �r Din Bil Stockholm AB Sveriges st�rsta auktoriserade �terf�rs�ljare av Audi, Volkswagen, Skoda, Seat, Porsche, Bentley och Lamborghini.
<p>
Vi har alltid ett stort antal nya och begagnade kvalitetsbilar i lager! Just nu har vi ca 800 begagnade bilar i v�ra lager.
<p>
Hos oss g�r du alltid en trygg och bra bilaff�r d� vi levererar kvalitetsbilar med garantier och bra finansieringserbjudanden till r�tt priser.
<p>
V�lkomna!', 'http://www.dinbil.se', 'active', false, '2004-04-08 14:15:00', '2016-09-01 23:59:59', 'Audi, Volkswagen, VW, Skoda, Seat, Porsche, Lamborghini, Bentley, Bil, Bilar, Transportbilar, Nya bilar, Begagnade bilar,
', '-', 'patrik', '-', '-', 'patrik', 'patrik', 11, 0, 1020);
INSERT INTO stores VALUES (12654, 'Allglas AB', 'rifum100_hotmail.com@blocket.se', '019-26 26 26', '019-26 26 26', 'Idrottsv�gen 29', '702 32', '�REBRO', 'allglas_logo.gif', 'Vi har 20 �rs erfarenhet inom bilglas.', 'F�retaget ligger i �rnsro i centrala �rebro, mitt emot �rnsro idrottsplats.
<p>
Vi har 20 �rs erfarenhet inom bilglas.<br>
P� Allglas AB �r vi 3 anst�llda med gedigen erfarenhet av bilglas.
<p>
L�mnar du bilen hos oss st�r vi med gratis l�nebil, vi har 2st Mercedes A-klass.', '-', 'active', false, '2004-05-11 11:50:00', '2016-02-01 23:59:59', 'Glas, Bilglas, Audi, BMW, Chrysler, Mercedes, Opel, Seat, Volvo,', '-', 'patrik', '-', '-', 'patrik', 'patrik', 8, 0, 1020);
INSERT INTO stores VALUES (12656, 'Carlqvist Bil AB', 'christer.johansson_carlqvistbil.se@blocket.se', '0477-195 00', '0477-195 15', 'Kanotleden 3', '362 22', 'TINGSRYD', 'carlqvist_logo.gif', 'Bra bil till r�tt pris!', 'V�lkomna in till oss p� Carlqvist Bil!<br>
H�r finns idag ca 500 nyare begagnade bilar till salu.
<p>
Carlqvist Bil AB startades 1983 och finns bel�gen i Tingsryd. F�r n�rvarande arbetar 20 personer i f�retaget.
<p>
Vi �r specialiserade p� begagnat-f�rs�ljning, av alla p� marknaden f�rekommande m�rken, den huvudsakliga delen ligger p� nyare bilar.
<p>
Vi har �ven ett antal transportfordon, arbetsfordon samt tyngre lastbilar i lager. F�r n�rvarande har vi �ver 500 fordon till f�rs�ljning.
<p>
Vi har egen nybygd modern verkstad och rekonditionering med mycket kompetent personal. 
<p>
V�ra ordinarie �ppettider �r:<br>
M�ndag � Fredag kl 10.00 � 18.00<br>
L�rdag kl 10.00 � 14.00<br>
S�ndag St�ngt', 'http://www.carlqvistbil.se/', 'active', false, '2004-05-13 12:00:00', '2017-09-30 23:59:59', 'Alfa, Alfa Romeo, Audi, BMW, Chevrolet, Chrysler, DAF, Fiat, Ford, Lexus, MG, Mazda, Mercedes, Mitsubishi, Nissan, Opel, Peugeot, Renault, Saab, Toyota, VW, Volkswagen, Volvo,', '-', 'patrik', '-', '-', 'patrik', 'patrik', 21, 0, 1020);
INSERT INTO stores VALUES (14032, 'Automix', 'automix_automix.se@blocket.se', '042-706 35', '042-706 35', 'Norra Storgatan 64', '26735', 'BJUV', 'Butik.gif', 'M�rkesoberoende handlare', 'Vi �r ett f�retag som specialiserat oss p� import och f�rs�ljning av europeiska bilar av alla sorter och storlekar, ifr�n familjebilar till sport bilar och lyx coup�.
<p>
Vi har alltid omkring 60 bilar i lager och har du speciella �nskem�l s� har vi ett brett kontaktn�t f�r att hitta din speciella bil.
<p>
Mobil: 0736-943391 Harry<br>
Ring g�rna innan ni kommer f�r bil visning.
<p>
�ppettider:<br>
M�n - Fre 11:00 - 18:00<br>
L�r: 12:00 - 16:00<p>', 'http://www.automix.se

', 'active', false, '2005-09-09 00:00:00', '2015-12-15 23:59:59', 'Audi, BMW, Chrysler, Mercedes, Honda, Ford, Volkswagen, VW, Volvo', '-', 'patrik', '-', '-', 'patrik', 'patrik', 23, 3, 1020);
INSERT INTO stores VALUES (12659, 'Skepparg�rdens Bil', 'roger.ny_skeppargardensbil.se@blocket.se', '0454-522 13', '0454-522 13', 'Bj�rken�sv�gen 79', '375 91', 'M�RRUM', 'logo.gif', '30 �rs erfarenhet av bilar!', 'Vi p� Skepparg�rdens bil arbetar mest med tyska kvalitetsbilar. Innehavaren har haft bilar som hobby i �ver 30 �r. Vi finns i Blekinge utmed E22:an i det lilla samh�llet Pukavik utanf�r M�rrum.
<p>
Finansiering i samarbete med Wasa Kredit AB
<p>
�ppettider:<br>
Vardagar 10-18<br>
L�rdagar 11-15<br>
�vriga tider enligt �verenskommelse.', 'http://www.skeppargardensbil.se', 'active', false, '2004-05-14 13:30:00', '2016-07-14 23:59:59', 'BMW, Saab, Volvo,', '-', 'patrik', '-', '-', 'patrik', 'patrik', 22, 0, 1020);
INSERT INTO stores VALUES (12661, 'Rosell & Clack Importlagret', 'info_importlagret.com@blocket.se', '0920-22 07 40', '0920-22 90 18', 'K�pmangatan 29', '972 33', 'LULE�', 'logga_importlagret.jpg', 'Import av fyrhjulingar och tv�hjulingar.', 'V�lkommen till Importlagret.com
<p>
Vi �r internetbutiken f�r dig som s�ker bra kvalit� till fabrikspriser. <br>Hos
oss kan du k�pa crosscycklar, fyrhjulingar, EU-mopeder och exklusiva massagebadkar till fantastiska priser.<p> Klicka in p� v�r hemsida f�r att se v�rt kompletta sortiment.', 'http://www.importlagret.com', 'active', false, '2004-05-14 10:00:00', '2016-06-01 23:59:59', 'Fyrhjuling, 4-hjuling, ATV, Cross, 4WD, 4x4, Import, SpeedMoto, ', '-', 'patrik', '-', '-', 'patrik', 'patrik', 1, 0, 1140);
INSERT INTO stores VALUES (12662, 'Rosell & Clack Importlagret', 'info_importlagret.com@blocket.se', '0920-22 07 40', '0920-22 90 18', 'K�pmangatan 29', '972 33', 'LULE�', 'importlagret_logo.gif', 'Import av fyrhjulingar och tv�hjulingar.', 'V�lkommen till Importlagret.com
<p>
Vi �r internetbutiken f�r dig som s�ker bra kvalit� till fabrikspriser. <br>Hos
oss kan du k�pa crosscycklar, fyrhjulingar, EU-mopeder och exklusiva massagebadkar till fantastiska priser.<p> Klicka in p� v�r hemsida f�r att se v�rt kompletta sortiment.', 'http://www.importlagret.com', 'active', false, '2004-05-14 10:00:00', '2016-06-01 23:59:59', 'Fyrhjuling, 4-hjuling, ATV, Cross, 4WD, 4x4, Import, Speedmoto', '-', 'patrik', '-', '-', 'patrik', 'patrik', 11, 0, 1140);
INSERT INTO stores VALUES (13884, '3 x Larssons Bil AB', 'info_3xlarssonbil.se@blocket.se', '040-21 12 00', '040-21 12 60', 'Agnefridsv�gen 183', '213 75', 'MALM�', 'butil_logga.gif', '�terf�rs�ljare f�r Kia, MG, Rover', '<b>3 x Larsson Bil AB</b> erbjuder stor variation n�r det g�ller modeller, motorer och inte minst prisniv�er.<br>
Vi har �ven ett stort urval p� beg. Chrysler och Jeep samt andra Amerikanska bilar.
<p>
F�r f�rm�nliga leasing och avbetalningsf�rslag - kontakta v�ra duktiga s�ljare.
<p>
V�lkommen in och provk�r just din bilmodell!<p>
�ppet:
M�ndag-Fredag 9.00 - 18.00<br>
L�rdag 10.00 - 14.00<br>
S�ndag 11.00 - 14.00 <br><p>
V�lkommen ! Bilarna �r M-Testade ', 'http://www.3xlarssonbil.se
', 'active', false, '2005-06-08 00:00:00', '2016-06-15 23:59:59', 'Kia, MG, Rover, Audi, Chrysler, Renault, Volkswagen, VW, Volvo', '-', 'patrik', '-', '-', 'patrik', 'patrik', 23, 11, 1020);
INSERT INTO stores VALUES (12663, 'Bilparken AB', 'bilparken_bilparken.se@blocket.se', '040-40 50 14', '040-40 50 58', 'F�retagsgatan 7', '233 51', 'SVEDALA', 'bilparken_logo_ny.gif', 'M�rkesoberoende bilhandlare!', 'Bilparken AB �r bel�gen i Svedala utmed E65 i Sk�ne, och sysslar med f�rs�ljning av nyare begagnade bilar.
<p>
�ppettider:<br>
M�ndag - Fredag: 11.00 - 18.00<br>
L�rdag - S�ndag: 11.00 - 15.00
<p>
V�lkomna!', 'http://www.bilparken.se/', 'active', false, '2004-05-18 15:40:00', '2016-06-01 23:59:59', 'Audi, BMW, Chevrolet, Chrysler, Citro�n, Citroen, Ford, Honda, Jaguar, Kia, Mercedes, Mitsubishi, Opel, Peugeot, Renault, Saab, Seat, Skoda, Volkswagen, VW, Volvo, ', '-', 'patrik', '-', '-', 'patrik', 'patrik', 23, 11, 1020);
INSERT INTO stores VALUES (14475, '�rsta Bil AB', 'lars.lofgren_artsabil.com@blocket.se', '08-500 055 56', '-', 'Virkesv�gen 6', '120 30', 'STOCKHOLM', 'butik.gif', 'M�rkesoberoende handlare', '<b>V�lkommen till �rsta Bil AB</b><p>

Vi finns p� Virkesv�gen 6 Hammarby Sj�stad <p>
�ppettider:<br>
M�-Tors: 09.30-18.00 <br>
Fredagar: 09.30-16.45 <br>
L�rdagar: 10.00-14.00 <br>
S�ndagar: St�ngt
<p>
F�rm�nlig finansiering, avbetalning el leasing Garantier 6-12 m�nader g�ller samtliga bilar yngre �n 8 �r som har g�tt max 15000 mil. 
<p>
V�lkommen !', 'http://www.arstabil.com

', 'active', false, '2005-10-25 00:00:00', '2016-01-31 23:59:59', 'audi, cadillac, chevrolet, ford, gmc, honda, nissan, seat, volkswagen, vw, volvo', '-', 'patrik', '-', '-', 'patrik', 'patrik', 11, 0, 1020);
INSERT INTO stores VALUES (14476, 'Knallelands Bil Center', 'knallelandsbilcenter_hotmail.com@blocket.se', '033-13 96 30', '033-13 96 30', 'Bergslengatan 50', '506 30', 'BOR�S', '-', 'M�rkesoberoende handlare', 'Bra beg bilar. <br>
<b>Alltid L�gpris! </b><br>
(Vi reserverar oss f�r felformuleringar i annonstext)<p>
�ppettider: <br>
M�ndag-Fredag 9-18<br>
L�rdagar 11-16<br>
Andra tider enligt �verenskommelse. <p>
V�lkomna! 
', '-', 'active', false, '2005-10-26 00:00:00', '2016-10-26 00:00:00', 'BMW, Chrysler, Ford, Mazda, Mercedes-Benz, Peugeot, Renault, Toyota, Volkswagen, VW, Volvo ', '-', 'patrik', '-', '-', 'patrik', 'patrik', 16, 0, 1020);
INSERT INTO stores VALUES (12672, 'Bilforum AB', 'info_bilforum.se@blocket.se', '019-31 20 50', '019-31 20 60', 'Bettorpsgatan 24', '703 69', '�REBRO', 'bilforum_logo.gif', 'Auktoriserade �terf�rs�ljare f�r Honda', 'Bilforum AB �r �rebro l�ns auktoriserade �terf�rs�ljare
f�r den japanska prestigebilen Honda.
<p>
Vi har en komplett fullserviceanl�ggning dvs. f�rs�ljning, verkstad samt reservdelar.
<p>
Bilforum har alltid ett v�lfyllt beg lager av nyare �rsmodell. Eftersom vi �lskar bilar och v�rt arbete f�rs�ker vi �ven ha  ett flertal exklusiva bilar.
<p>
G�r ett bes�k hos oss och prata bil.', 'http://www.bilforum.se
', 'active', false, '2004-05-18 14:00:00', '2015-12-01 00:00:00', 'Audi, BMW, Ford, Honda, Lexus, Mercedes, Mitsubishi, Nissan, Opel, Saab, Volvo,', '-', 'patrik', '-', '-', 'patrik', 'patrik', 8, 0, 1020);
INSERT INTO stores VALUES (12674, 'Yamaha Center Helsingborg', 'info.hbg_yamahacenter.com@blocket.se', '042-15 55 55', '042-16 18 15', 'V�lav�gen 29', '254 64', 'HELSINGBORG', 'logo.gif', 'Din B�t- & MC-handlare!', 'V�lkommen till Yamahacenter i Helsingborg!
<p>
Yamaha Center Helsingborg ligger p� V�lav�gen 29 i Helsingborg. I v�ra lokaler p� ca: 2400 kvm erbjuder vi hela Yamaha�s breda modellprogram.', 'http://www.yamahacenter.com/templates/Page.asp?id=1939', 'active', false, '2004-05-21 13:30:00', '2016-06-01 23:59:59', 'Yamaha, Skoter, Scooter, Fyrhjuling, 4-hjuling, Yamarin, Buster, B�tmotor,', '-', 'patrik', '-', '-', 'patrik', 'patrik', 23, 3, 1140);
INSERT INTO stores VALUES (12675, 'Mats Bring�s AB', 'matsbringas_hotmail.com@blocket.se', '070-362 11 32', '0240-211 39', 'Kopparbergsv�gen 45', '772 30', 'GR�NGESBERG', '-', 'M�rkesoberoende bilhandlare!', 'V�lkommen till Mats Bring�s AB.
<p>
Vi tillhandah�ller:
<li>Ett bra urval av beg. bilar.</li>
<li>F�rm�nlig finansiering.</li>
<li>6 m�naders garanti.</li>
<p>
Vi n�s �ven p� 0240-211 38.', 'http://www.shelliberget.com', 'active', false, '2004-05-18 13:31:00', '2016-06-01 23:59:59', 'BMW, Chevrolet, Mercedes, Volvo, Bil, Bilar,', '-', 'patrik', '-', '-', 'patrik', 'patrik', 6, 0, 1020);
INSERT INTO stores VALUES (14427, 'S-G Boman & Son Bil AB', 'info_bomanoson.se@blocket.se', '021-12 84 00', '021-18 86 09', 'L�ngg�ngskrogen', '725 91', 'V�STER�S', 'butik.gif', '�terf�rs�ljare f�r Nissan', '<b>V�lkommen till SG Boman & Son Bil AB.</b><p>

H�r kan du se Nissans kompletta modellprogram fr�n ekonomiska Micra till kraftfulla Nissan Patrol. <br>
Du kan �ven kolla in v�rt lager av fr�scha inbytesbilar. <br>
Givetvis kan du boka tid f�r demonstration och provk�rning hos n�gon av v�ra s�ljare.
<p>
SG Boman & Son bildades 1959 och vi har sedan 1966 varit �terf�rs�ljare f�r japanska bilar, vi �r d�rmed en av sveriges �ldsta japanbilss�ljare.
<p>
V�r verkstad och reservdelsavdelning �r fullt utrustade f�r att kunna serva och reparera din Nissanbil, vi �tar oss allt fr�n enklare service till avancerade pl�t och riktningsarbeten.
<p>
P� v�r hemsida kan du �ven boka tid f�r service eller best�lla reservdelar, ge oss ett meddelande s� kontaktar vi dig.
V�ra f�rs�ljnings- och servicelokaler ligger efter gamla K�pingsv�gen ca: 2 km v�ster om B�ckby-Sk�lby. <p>

<b>�ppettider: </b><br>
Vardagar 9-18<br>
L�rdagar 10 -14<br>
<p>
Verkstad<br>
7-16 vardagar<br>
<p>
Reservdelslager<br>
8-17 vardagar <br>
<p>
V�lkommen ! 
', 'http://www.bomanoson.se
', 'active', false, '2005-10-17 00:00:00', '2016-01-31 23:59:59', 'Audi, BMW, Chevrolet, Datsun, Ford, Hyundai, Isuzu, Land Rover, Range Rover, Mitsubishi, Mazda, Mercedes-Benz, Nissan, Renault, Seat, Suzuki, Volkswagen, VW, Volvo', '-', 'patrik', '-', '-', 'patrik', 'patrik', 9, 0, 1020);
INSERT INTO stores VALUES (12727, 'Lars Eneland AB - eurocars.se', 'info1_eurocars.se@blocket.se', '08-446 50 80', '08-446 50 81', '-', '-', '-', 'larseneland_logo.gif', 'Tr�tt p� d�liga och dyra bilar?', 'V�r id� �r enkel. Inom de prisklasser vi jobbar, f�rs�ker vi erbjuda Europas mest v�lsk�tta bilar till Sveriges b�sta nettopriser. 
<p>
V�lkommen!', 'http://www.eurocars.se/', 'active', false, '2004-05-21 10:50:00', '2016-06-01 23:59:59', 'Audi, BMW, Chrysler, Ford, Mercedes, Mitsubishi,', '-', 'patrik', '-', '-', 'patrik', 'patrik', 11, 0, 1020);
INSERT INTO stores VALUES (12729, '�rebro Nettobilar', 'nettobilar_telia.com@blocket.se', '019-33 00 05', '', 'Tull�ngsgatan 10', '702 25', '�REBRO', 'nettobilar_logo.gif', 'M�rkesoberoende bilhandlare!', 'Alltid ett 20-tal fr�scha bilar i lager!
<p>
*S�ljer
<br>
*K�per
<br>
*Byter
<p>
F�rm�nlig finansiering.
<p>
V�lkomna!', 'http://
', 'active', false, '2004-05-20 11:55:00', '2016-06-01 23:59:59', 'Audi, BMW, Ford, Hyundai, Mazda, Opel, Renault, Saab, Volkswagen, VW, Volvo, ', '-', 'patrik', '-', '-', 'patrik', 'patrik', 8, 0, 1020);
INSERT INTO stores VALUES (12734, 'Exclusive Cars AB', 'info_exclusive.se@blocket.se', '08-449 20 40', '08-449 20 59', 'Elektrav�gen 7-9', '126 30', 'H�GERSTEN', 'exclusive_logo.gif', 'Mycket mer �n exklusiva bilar!', 'Exclusive Cars har passion f�r bilar, och h�r finns ett personligt intresse i det som s�ljs. Tack vare detta �r vi snabba p� att plocka upp nya trender utomlands, f�r att sedan testa dessa p� svenska marknaden. Hummer, Corvette C5, Lincoln Navigator, Ford F-150 Lightning, Chevrolet Avalanche, Dodge Viper, Lexus RX300 & LX 470, Nissan 350Z �r alla exempel d�r vi levererat dom f�rsta bilarna i Sverige. Lexus RX300 levererades t.ex tv� �r innan den b�rjade s�ljas av den svenska generalagenten. Vi �r helt frist�ende fr�n n�gon tillverkare, och kan d�rf�r erbjuda m�rken och modeller som normalt inte s�ljs h�r, till rimliga priser.
<p>
Exclusive Cars kan �ven erbjuda service och tillbeh�r. Sedan n�gra �r tillbaka har verksamheten ut�kats med Exclusive Cars Rental, biluthyrning med betoning p� rymliga, bekv�ma och v�lutrustade bilar vilket �r helt i linje med vad vi s�ljer.
<p>
Till sist... Vi har alltid arbetat l�ngsiktigt, och vi �r �vertygade om att god service �r framtiden. V�ra kunder ska k�nna ett f�rtroende och alltid veta att de blir omh�ndertagna p� b�sta m�jliga s�tt.', 'http://www.exclusivecars.se', 'active', false, '2004-05-28 00:00:00', '2016-09-15 23:59:59', 'Alfa Romeo, Audi, BMW, Cadillac, Chevrolet, Chrysler, Dodge, Ford, Infiniti, Jaguar, Jeep, Mercedes, Nissan, ', '-', 'patrik', '-', '-', 'patrik', 'patrik', 11, 0, 1020);
INSERT INTO stores VALUES (12741, 'Arvidstorps Bil', 'info_arvidstorpsbil.com@blocket.se', '0346-104 41', '0346-127 16', 'Falkenbergsmotet E6 �stra', '311 50', 'FALKENBERG', 'arvidstorps_logo.gif', '�terf�rs�ljare f�r Honda, Hyundai, Fiat Transportbilar samt Adria Husbilar', 'V�LKOMMEN TILL ARVIDSTORPS BIL !
<p>
Vi �r �terf�rs�ljare f�r Honda, Hyundai, Fiat Transportbilar samt Adria Husbilar. <br>
V�lkommen till v�r nybyggda, ljusa och trivsamma bilhall vid Falkenbergsmotet E6 �stra!
<p>
Hos oss hittar du m�nga fina begagnade bilar som vi levererar med garanti och ansvar. Sj�lvklart hittar du ocks� stora delar av nybilsprogramet fr�n HYUNDAI, HONDA, FIAT TRANSPORTBILAR samt ADRIA Husbilar.
<p>', 'http://www.arvidstorpsbil.com', 'active', false, '2004-06-02 15:30:00', '2016-12-01 23:00:00', 'Audi, Citro�n, Citroen, Ford, Honda, Hyundai, Mazda, Mercedes, Seat, Toyota, VW, Volkswagen,', '-', 'patrik', '-', '-', 'patrik', 'patrik', 20, 0, 1020);
INSERT INTO stores VALUES (12864, 'Attento Bil AB', 'frederick.enaholo_attentobil.se@blocket.se', '042-38 69 50', '042-38 69 69', 'Bergav�gen 7', '254 66', 'HELSINGBORG', 'attento_logo.gif', '�terf�rs�ljare f�r Alfa Romeo, Fiat, Hyundai, MG, Rover', 'Hyundai, Fiat, Alfa Romeo, MG och Rover �r mycket bra bilm�rken d�r Fiat, Europas mest s�lda bil, �r ett av de �ldsta bilm�rkena i v�rlden. Hyundai har med en m�lmedveten reklamsatsning snabbt blivit ett bilm�rke med uttalad milj�profil. Vad det g�ller Alfa Romeo �r detta en utm�rkt f�retagsbil som st�r f�r sportighet, design och avancerad teknik.
<p>
�r Du intresserad av en ny eller begagnad Alfa Romeo Fiat eller Hyundai �r det h�r ett bra s�tt att b�rja med. Vi har ett brett urval begagnade bilar. H�r hittar Du information om v�rt f�retag, var vi finns och hur Du kommer i kontakt med oss. F�rutom att ge Dig st�d runt ditt bilk�p kan vi sj�lvklart erbjuda Dig service, reservdelar och tillbeh�r. Text och bild �r inte alltid tillr�ckligt s� d�rf�r inbjuder vi till en personlig kontakt med bilen.
<p>
�ppettider:<br>
M�n-Fre 09:00 - 18:00<br>
L�r-S�n 11:00 - 15:00
<p>
V�lkomna!', 'http://www.attentobil.se', 'active', false, '2004-06-24 09:00:00', '2016-07-01 23:59:59', 'Alfa Romeo, Fiat, Hyundai, MG, Rover,', '-', 'patrik', '-', '-', 'patrik', 'patrik', 23, 3, 1020);
INSERT INTO stores VALUES (12744, 'Bilcenter i V�rberg', 'bilcenterivarberg_hotmail.com@blocket.se', '08-710 36 30', '08-710 36 29', 'Fj�rdholmsgr�nd 28', '127 43', 'SK�RHOLMEN', '-', 'M�rkesoberoende handlare', 'S�ljer - Byter - K�per
<p>
Ni hittar hos i V�rbergs Centrum, bredvid tunnelbanan.
<p>
V�lkomna!', 'http://www.bilcentervarberg.com', 'active', false, '2004-06-01 09:00:00', '2015-12-31 23:00:00', 'Audi, BMW, Chrysler, Ford, Honda, Jaguar, Lancia, Mazda, Mercedes, Mitsubishi, Opel, Toyota, Volkswagen, VW, Volvo,', '-', 'patrik', '-', '-', 'patrik', 'patrik', 11, 0, 1020);
INSERT INTO stores VALUES (12745, 'Stenbergs Bil AB', 'matts.stenberg_stenbergsbil.com@blocket.se', '0155-28 59 60', '0155-28 36 97', 'Box 3035, Gasverksv�gen 32', '611 03', 'NYK�PING', 'stenbergs_logo.gif', 'Auktoriserad �terf�rs�ljare f�r Hyundai.', 'Stenbergs Bil AB �r S�dermanlands �ldsta bilf�retag med 61 �r i branschen. Stenbergs Bil AB �r verksamma i Bettna och h�r i Nyk�ping.
Vi har totalt ca: 2800 m� moderna lokaler d�r vi 23 medarbetare, alla med stor erfarenhet och bred kompetens inom bilbranschen arbetar f�r att f� �nnu fler n�jda bilkunder. M�nga av oss har arbetat p� Stenbergs hela sitt yrkesverkamma liv.
<p>
I Nyk�ping �r vi 8 medarbetare. Nybilsf�rs�ljningen representeras av Hyundai samt begagnade bilar. Vi har en modern serviceverkstad f�r Hyundai och Peugeot samt d�rtill omfattande tillbeh�r och reservdelsavdelning.', 'http://www.stenbergsbil.com', 'active', false, '2004-06-06 16:00:00', '2016-09-30 23:59:59', 'Hyundai

', '-', 'patrik', '-', '-', 'patrik', 'patrik', 12, 0, 1020);
INSERT INTO stores VALUES (13847, 'City Bil Rent AB', '-', '08-29 18 18', '08-655 49 49', 'Ranhammarsv 25', '168 67', 'BROMMA', 'logga_butik.gif', 'M�rkesoberoende handlare', '<b>V�lkommen till City Bil Rent AB</b>
<p>
H�r kan du b�de k�pa och hyra bil till bra priser.<br>
Direkt nummer bilf�rs�ljningen:<br>
08-29 18 18<p>
Direkt nummer biluthyrningen:<br>
08-655 13 13<p>
<b>�ppettider:</b><br>
Vardagar: 10.00-18.00<p>
V�lkommen', 'http://www.hyrbilrent.nu
', 'active', false, '2005-05-17 00:00:00', '2016-10-01 23:59:59', 'Fiat, Honda, Hyundai, Mazda, Opel, Toyota, Volkswagen, VW, Hyrbil', '-', 'patrik', '-', '-', 'patrik', 'patrik', 11, 0, 1020);
INSERT INTO stores VALUES (13483, 'Autotrader Bilv�rdscenter AB', 'info_autotrader.se@blocket.se', '08 - 37 25 75', '08 - 37 34 81', 'Saldov�gen 10', '175 62', 'J�RF�LLA', '10_1_05.jpg', 'M�rkesoberoende handlare', '<p>
Vi �r ett f�retag som bedriver biluthyrning i samarbete med Mabi biluthyrning i V�llingby Centrum. Vi har �ven specialiserat oss p� det mesta inom bilv�rd, bland annat ditec lackkonservering, solfilm och vindrutereparationer. 
<p>      
�ppettider
<br>Vardag 07:30-17:00
<br>L�rdag 09:00-13:00 
<p>', 'http://www.autotrader.se
', 'active', false, '2005-01-10 00:00:00', '2016-12-31 23:59:59', 'Audi, Volkswagen, VW', '-', 'patrik', '-', '-', 'patrik', 'patrik', 11, 0, 1020);
INSERT INTO stores VALUES (13484, 'Sk�ne Camp AB', '-', '040 - 29 29 12', '040 - 29 30 31', 'Segesv�ngen 1', '212 27', 'MALM�', 'skane.jpg', 'Nordens st�rsta varuhus - f�r fritid p� hjul.', 'Det har sedan 1969, varit helt fantastiska �r. V�r egen start var visserligen blygsam, men det v�ckte ett genuint intresse f�r alla de m�jligheter som den rullande bostaden f�rde med sig. 
<p>
Vi l�rde oss �lska friheten och uppt�ckte ett nytt s�tt att leva helt enkelt. I dag har vi p� �stg�ta Camping m�nga likasinnade v�nner fr�n n�r och fj�rran. 
<p>
M�nga har under de h�r �ren bes�kt oss och tack vare alla trevliga kunder och bra leverant�rer �r vi i dag till konsument "Nordens st�rsta �terf�rs�ljare av husvagnar och husbilar". Vi oms�tter ca 160 miljoner kr och har totalt 30 medarbetare.   Det �r vi b�de stolta �ver och tacksamma f�r och lovar att vi ska f�rs�ka g�ra forts�ttningen lika trevlig. 
<p>
Vi har m�nga id�er och nyheter som vi kommer att presentera oavsett om du bes�ker oss i Mantorp, Malm�, M�rsta, Norrk�ping eller i �rebro. 

<p>
�ppettider:<br>
M�ndag - Fredag - 09-17<br>
L�rdag - St�ngt<br>
S�ndag - St�ngt<br>
<p>
V�lkommen!', 'http://www.ostcamp.se', 'active', false, '2004-01-15 15:30:00', '2016-02-15 23:59:59', 'Polar, S�vsj�, Cabby, Kabe, Husvagn, Husvagnar, Husbil, Husbilar, Solifer, B�rstner, Pilot, Renault, Iveco, Hobby, Adria, ', '-', 'patrik', '-', '-', 'patrik', 'patrik', 23, 11, 1100);
INSERT INTO stores VALUES (12753, 'Asplunds Bil AB', 'bengt_asplundsbil.se@blocket.se', '033-20 45 40', '033-20 45 49', 'Sjumilagatan 4', '507 11', 'BOR�S', 'asplunds_logo.gif', 'Auktoriserad �terf�rs�ljare av Citro�n och Jaguar.', 'V�lkommen till Asplunds Bil<br>
Asplunds Bil startade 1955 och �r idag �terf�rs�ljare av Citro�n och Jaguar.
Du finner v�r fullserviceanl�ggning i Bor�s vid Br�mhultsmotet. 
Vi har ett stort utbud av begagnade bilar som du kan s�ka bland h�r p� hemsidan.
<p>
Funderar du p� att byta bil, ny eller begagnad, v�nd dig till oss. 
<p>
V�lkommen!', 'http://www.asplundsbil.se/', 'active', false, '2004-06-01 15:40:00', '2016-01-01 23:00:00', 'Audi, BMW, Chrysler, Citro�n, Citroen, Ford, Honda, Jaguar, Mercedes, Volvo,', '-', 'patrik', '-', '-', 'patrik', 'patrik', 16, 0, 1020);
INSERT INTO stores VALUES (12754, 'HB L�fstr�ms F�rs�ljning', 'hb.lofstroms_swipnet.se@blocket.se', '060-325 71', '060-325 71', 'Kornv�gen 5', '862 41', 'NJURUNDA', '-', 'M�rkesoberoende handlare', 'M�rkesoberoende handlare.
<p>
V�lkommen!', '-', 'active', false, '2004-06-01 16:00:00', '2016-06-01 23:59:59', 'Audi, Toyota, VW, Volkswagen, Volvo,', '-', 'patrik', '-', '-', 'patrik', 'patrik', 4, 0, 1020);
INSERT INTO stores VALUES (12755, 'Kabe Husvagnar Produktion AB i Tenhult', 'lennart.rosenberg_kabe.se@blocket.se', '036-39 37 00', '', 'J�nk�pingsv�gen 21, Box 14', '560 27', 'TENHULT', 'kabe-logo.gif', 'Marknadsledare sedan -93.', 'Vi har ett stort urval av marknadsledande KABE Husvagnar och KABE Travel Master Husbilar.
<p>
Tillbeh�rsbutiken �r full av sp�nnande nyheter och snygga, bekv�ma utem�bler. 
<p>
Hos oss hittar du ocks� t�lt fr�n marknadsledande Isabella och Ventura.
<p>
<b>F�rs�ljning</b><br>
Lennart 036-39 37 20<br>
Alf 036-39 37 19
<p>
<b>Tillbeh�rsbutik</b><br>
Simona 036-39 37 21
<p>
<b>�ppettider</b><br>
M�ndag - Fredag 9.00 - 18.00<br>
L�rdag 10.00 -14.00<br>
S�ndag 12.00 - 16.00<br>
<p>
V�lkommen!', 'http://www.kabe.se/tenhult/', 'active', false, '2004-06-01 11:22:00', '2016-06-03 23:59:59', 'Husvagn, Husvagnar, Husbil, Husbilar, Kabe, Kama, Isabella, Ventura, ', '-', 'patrik', '-', '-', 'patrik', 'patrik', 17, 0, 1100);
INSERT INTO stores VALUES (12756, 'AutoPerformance Sweden AB', 'info_autoperformance.se@blocket.se', '040-42 49 99', '040-42 01 82', 'Slottag�rdsgatan 3', '235 35', 'VELLINGE', 'autoperformance_logo.gif', 'M�rkesoberoende handlare', 'Auto Performance s�ljer sp�nnande lyx-
och kvalitetsbilar till s� rimliga priser
som marknaden till�ter.
<br>
Bilarna vi s�ljer �r av nyare �rsmodell men
har rullat av sig de absolut dyraste milen.
<p>
Det inneb�r att du alltid g�r en bra aff�r,
�ven om du byter bil ofta eller om du
tycker om att variera ditt bil�gande.
<br>V�r filosofi �r att man ska g�ra det mesta
av varje dag och v�rt bidrag �r att hj�lpa
till att f�rverkliga dina bildr�mmar och
�nskem�l till r�tt pengar.
<br>�ven om vi inte har just "din" bil hemma
hj�lper vi dig att hitta den.
<br>
Hos oss f�r du samma trygghet och garantier
som hos m�rkes�terf�rs�ljaren.
<p>
Vi n�s �ven p� 0709-42 01 42.
<br>
V�lkomna!', 'http://www.autoperformance.se/', 'active', false, '2004-06-02 14:01:00', '2016-11-01 23:59:59', 'Audi, BMW, Chevrolet, Jaguar, Mazda, Mercedes, Nissan, Porsche, VW, Volkswagen, Volvo', '-', 'patrik', '-', '-', 'patrik', 'patrik', 23, 11, 1020);
INSERT INTO stores VALUES (12757, 'Pia�s Bilar', 'pias.bilar_home.se@blocket.se', '031-83 23 33', '031-40 70 16', 'Pia Johansson Delsj�v�gen 7', '412 66', 'G�TEBORG', '-', 'M�rkesoberoende handlare', 'M�rkesoberoende handlare.
<p>
Bilarna s�ljes fr�n bostad = l�gre priser. <br>
Visning enbart efter avtalad tid.<br>
V�nligen ring oss innan ankomst.<br>
Mobilnr: 0762-40 45 45
<p>
V�lkommen!<p>
<b>OBS! Vi har dataproblem f�r tillf�llet, vi kan pga detta ej besvara e-post.<br>
V�nligen ring.</b>
', '-', 'active', false, '2004-06-07 08:40:00', '2016-06-01 23:59:59', 'Audi, BMW, Ford, Mazda, Nissan, Mercedes, Opel, Peugeot, Renault, Saab, Volkswagen, VW, Volvo,', '-', 'patrik', '-', '-', 'patrik', 'patrik', 15, 0, 1020);
INSERT INTO stores VALUES (14190, 'Callisma AB', 'info_callisma.se@blocket.se', '08-755 15 15', '-', 'Vendev�gen 11A', '182 69', 'DJURSHOLM', 'Logga_Callisma.jpg', 'Exklusiva bilar till bra pris.', '<b>V�lkommen till Callisma AB!</b>
<p>
Callisma AB, �Selected Cars Only�, erbjuder personlig service. <br>Vi �r en exklusiv oberoende handlare och vi har v�rt kontor p� Djursholms Torg. Genom unika samarbeten med Europas fr�msta �terf�rs�ljare av exklusiva sportbilar, kan vi f�rmedla nya bilar av f�ljande m�rken: <b>Aston Martin, Bentley, Ferrari, Lamborghini, Maserati och Rolls Royce</b>.<p> Bilarna levereras med gener�sa helt�ckande fabriksgarantier.Tack vare v�ra speciella samarbeten kan vi s�lja till l�gre priser och b�ttre leveranstider �n m�nga andra.<p> Vi s�ljer �ven l�tt begagnade bilar som vi l�mnar minst ett �rs garanti p�. <p>Har Du en nyare bil eller en speciell bil som Du vill s�lja, s� hj�lper vi ocks� g�rna till. Dra g�rna nytta av v�rt n�tverk och bredd p� marknadsf�ring.
<p>
Vi erbjuder f�rs�ljning � f�rmedling � k�p � byte - finansiering - import - export
<p>
Kontakta oss s� hj�lper vi till att hitta en bil som passar just dig. 
<p>
V�ra �ppettider �r m�ndag till fredag 09:00 till 17:00. Vi rekommenderar Er att kontakta oss innan en personlig visning.
', 'http://www.callisma.se', 'active', false, '2005-09-30 00:00:00', '2016-10-03 23:59:59', 'Audi, BMW, Bentley, Chevrolet, Chrysler, Ferrari, Ford, Jaguar, Land Rover, Maserati, Mercedes, Porsche, Saab, Volkswagen, VW, Volvo', '-', 'patrik', '-', '-', 'patrik', 'patrik', 11, 0, 1020);
INSERT INTO stores VALUES (12800, 'WM Bil AB', 'bilmix_telia.com@blocket.se', '0910-77 99 69', '0910-7799 67', 'Tj�rnv�gen 7', '931 61', 'SKELLEFTE�', '-', 'M�rkesoberoende handlare', 'WM Bil �r specialiserad p� bilar i prisklass 80.000kr och upp�t.
Hos oss kan du hitta n�stan nya f�retagsk�rda, fullservade bilar eller en inbytesbil i l�gre prisklass.
Vi handplockar bilar och genom goda ink�pskanaler, liten organisation och l�ga omkostnader kan vi h�lla n�stintill  oslagbara priser.
Det visar sig ocks� genom att vi s�ljer bilar �ver hela Norrland.
Vi �r inte m�rkesbundna heller vilket inneb�r ett brett utbud.
<p>
Vi �r �ven intresserade av att k�pa in motsvarande bilar.
<p>
Vi erbjuder bra finansiering i samarbete med Handelsbanken Finans.
<p>
Vi hyr lokalen tillsammans med Bilmix AB och vi samordnar �ven bilf�rs�ljningen.
Sammanlagt har vi cirka 50 bilar i samtliga prisniv�er.
<p>
V�lkommen till en trygg och bra bilaff�r.', '-', 'active', false, '2004-05-20 10:50:00', '2016-06-01 23:59:59', 'Audi, BMW, Ford, Nissan, Opel, Saab, Toyota, Volvo,', '-', 'patrik', '-', '-', 'patrik', 'patrik', 2, 0, 1020);
INSERT INTO stores VALUES (12809, 'Lundby Bil AB', 'info_lundbybil.se@blocket.se', '031-51 18 88', '031-23 58 62', 'Herkulesgatan 58', '417 01', 'G�TEBORG', 'lundbybil_loggo.gif', 'Mer �n 30 �r i branschen!', 'Lundby Bil AB etablerades 1973.<br>
Firman har idag 6 anst�llda, varav 2 p� egen verkstad.
Lagret best�r av ca 50 st beg. bilar av olika m�rken, genomg�ngna p� egen verkstad, vilket borgar f�r ett tryggt k�p.
<p>
�ppettider 08.00-18.00', 'http://www.lundbybil.se', 'active', false, '2004-06-09 10:00:00', '2016-01-11 23:59:59', 'Chrysler, Ford, Jaguar, Jeep, Mercedes, Volvo,', '-', 'patrik', '-', '-', 'patrik', 'patrik', 15, 0, 1020);
INSERT INTO stores VALUES (12828, 'EBN Bilf�rs�ljning', 'info_ebn.se@blocket.se', '031-28 76 10', '031-28 66 82', 'Datav�gen 2', '436 32', 'ASKIM', 'ebn_logo.gif', 'M�rkesoberoende handlare', '�ppettider:<br>
Vardagar 10.00-18.00<br>
L�rdagar 11.00-14.00
<p>
V�lkomna!', 'http://www.bytbil.com/ebnbil', 'active', false, '2004-06-10 11:00:00', '2016-12-31 23:59:59', 'Audi, GMC, Jeep, Mercedes, Porsche, Volkswagen, VW, Volvo,', '-', 'patrik', '-', '-', 'patrik', 'patrik', 15, 0, 1020);
INSERT INTO stores VALUES (12829, 'Tisnar Bil AB', 'tisnarbil_telia.com@blocket.se', '018-10 56 50', '018-60 14 60', 'Kungsgatan 109 (Kungsh�rnet)', '751 08', 'UPPSALA', 'tisnar_logo.gif', 'M�rkesoberoende handlare', 'TISNAR BIL AB hittar Ni p� Kungsgatan 109/Kungsh�rnet i Uppsala. F�rs�ljningen best�r i huvudsak av nyare, svensks�lda bilar. D� v�ra �ppettider varierar ber vi Er ringa p� telefon 018-10 56 50 f�r visning av bilarna. 
Vi byter g�rna in Er svensks�lda bil, och f�rs�ker alltid ge ett ungef�rligt mellanpris per telefon.
<p>
P� de flesta av bilarna l�mnas 3 m�naders trafiks�kerhetsgaranti och med m�jlighet att �ven teckna 1-�rs garanti (enligt garantibevis) via ett samarbete med Svensk Bilgaranti AB.
<p>
Ni kan �ven n� oss via mobil:
<p>
Peter Borghede<br>
Mob: 0708-413 999
<p>
Mikael Eriksson<br>
Mob: 070-514 6 514
<p>
Johan Skjaeret<br>
Mob: 0768-88 11 44
<p>
V�lkommen!', 'http://www.tisnarbil.se
', 'active', false, '2004-06-10 13:23:00', '2016-06-10 23:59:59', 'Audi, BMW, Opel, Porsche, Saab, Volkswagen, VW, Volvo,', '-', 'patrik', '-', '-', 'patrik', 'patrik', 10, 0, 1020);
INSERT INTO stores VALUES (13625, 'Twinmoters', 'twinmoters_hotmail.com@blocket.se', '0736-21 95 68', '0224-74 10 48', '�sterg�rdsv�gen 12 A', '730 71', 'SALBOHED', '-', 'M�rkesoberoende handlare', '-', '-', 'active', false, '2005-02-28 00:00:00', '2016-03-01 23:59:59', 'Yamaha, Kawasaki, ', '-', 'patrik', '-', '-', 'patrik', 'patrik', 9, 0, 1140);
INSERT INTO stores VALUES (12834, 'Eriksson Marine Scandinavia AB', 'info_erikssonmarine.com@blocket.se', '08-730 50 20', '08-730 50 20', 'Karlbergsstrand 4F', '171 73', 'SOLNA', 'eriksson_logo.gif', 'S�ljer, k�per, f�rmedlar, finansierar och besiktigar motorb�tar', 'Vi s�ljer, k�per, f�rmedlar, finansierar och besiktigar motorb�tar. V�r specialitet �r m�kling av b�tar i olika prisklasser och fabrikat. Vi f�renar kvalitet och valm�jlighet med kunskap och v�rt l�ge garanterar maximal exponering.
Ni hittar oss p� Pampas Marina, en av Stockholms st�rsta marinanl�ggningar. Titta f�rbi h�r finns allt till din b�t! 
', 'http://www.erikssonmarine.com', 'active', false, '2004-06-16 15:00:00', '2016-08-10 23:59:59', 'B�t, Motorb�t, Bayliner, Buster, Coronet, Flipper, ', '-', 'patrik', '-', '-', 'patrik', 'patrik', 11, 0, 1060);
INSERT INTO stores VALUES (12836, 'Hurtigs Bil AB', 'anders.carlsson_hurtigsbil.se@blocket.se', '0303-20 94 40', '0303-198 46', 'Filaregatan 23', '442 34', 'KUNG�LV', 'hurtigs_logo.gif', '�terf�rs�ljare f�r Citro�n & Nissan', 'Det skulle vara andra h�stkrafter f�r kreaturhandlarns n�st �ldste son. Efter diverse reparationer p� faderns motorcykel Blixt, visste G�sta Hurtig vad han skull �gna sin framtid �t. Bilar fick det bli.
<p>
Historien om Hurtigs Bil b�rjar i Kareby, norr om Kung�lv. Efter n�gra �r av mindre organiserad bilhandel, startar G�sta �r 1968 G�sta Hurtigs Bil AB. Till en b�rjan s�ljs Vauxhall och andra begagnade bilar. �r 1979 b�rjar man s�lja Datsun, som senare blir Nissan och 1981 ut�kas verksamheten ytterligare, Citro�n kommer in i sortimentet. Nu f�r det r�cka, t�nker G�sta men s� en morgon 1983 st�r ett par nya Mercedes utanf�r firman. Det �r Philipson som har levererat dem och nu fr�gar de G�sta om han inte kan s�lja dem ocks�. G�sta �r till en b�rjan skeptisk men de blir s�lda och sedan dess har �tskilliga Mercedes s�lts p� Hurtigs Bil.
<p>
Med Mercedes i sortimentet blir det tr�ngt p� "Appelgrens" i Kareby. S� 1984 flyttar Hurtigs Bil in till Filaregatan i Kung�lv, d�r de alltj�mt har sin verksamhet.', 'http://www.hurtigsbil.se', 'active', false, '2004-06-16 14:00:00', '2016-07-01 23:59:59', 'Citro�n, Citroen, Mercedes, Nissan, Volkswagen, VW, Volvo,', '-', 'patrik', '-', '-', 'patrik', 'patrik', 15, 0, 1020);
INSERT INTO stores VALUES (12837, 'Bilg�rden i �ngelholm AB', 'info_bilgarden.se@blocket.se', '0431-41 77 50', '0431-41 77 51', 'Helsingborgsv�gen 31', '262 72', '�NGELHOLM', 'bilgarden_logo.gif', '�terf�rs�ljare f�r Hyundai', 'Vi s�ljer succ�bilen HYUNDAI. Vi har alltid erbjudanden till Dig som k�per bil hos oss och har framf�rallt R�TT PRIS DIREKT!
<p>
V�ra s�ljare st�r naturligtvis till din tj�nst och hj�lper Dig med v�rdering och finansiering om s� �nskas.
<p>
Skall Din bil beh�va servas eller repareras hj�lper v�r kunniga verkstadpersonal Dig med detta. Vi har auktoriserad service av Hyundai och i Lund �ven av Honda. Givetvis kan vi ocks� utf�ra service p� andra bilm�rken. 
<p>
Som avslutning �nskar alla vi p� BILG�RDEN v�ra kunder, hj�rtligt v�lkomna. Har du n�gra fr�gor �r Du v�lkommen att kontakta oss.', 'http://www.bilgarden.se', 'active', false, '2004-06-17 11:40:00', '2016-10-17 23:59:59', 'Hyundai,', '-', 'patrik', '-', '-', 'patrik', 'patrik', 23, 16, 1020);
INSERT INTO stores VALUES (12838, 'Bilfirma I. R�berg', 'raberg_mbox302.swipnet.se@blocket.se', '033-26 01 28', '033-26 01 28', 'Manhem', '513 50', 'SPARS�R', 'raberg_logo.gif', 'Begagnade bilar med garanti!', 'Vi s�ljer begagnade bilar med garanti!
<p>
�ppet sju dagar i veckan, men ring g�rna f�re bes�k.<br>
Vi n�s �ven p� mobil: 070-490 01 85
<p>
V�lkomna!', 'http://www.rabergsbil.com
', 'active', false, '2004-06-17 09:00:00', '2016-10-15 23:59:59', 'Audi, Ford, Mercedes, Volvo,', '-', 'patrik', '-', '-', 'patrik', 'patrik', 16, 0, 1020);
INSERT INTO stores VALUES (12839, 'Autohaus Helsingborg AB', 'info_autohaus.se@blocket.se', '042-16 50 60', '042-16 50 68', 'Gev�rsgatan 25', '254 66', 'HELSINGBORG', 'autohaus_logo.gif', 'M�rkesoberoende handlare', 'P� samtliga bilar l�mnar 1 m�nads trafiks�kerhetsgaranti.
Dessutom kan de flesta av v�ra bilar f�s med 1 �rs garanti.
G�llande prislista hittar Ni p� v�r hemsida.<br>
NU G�LLER GARANTIN �VEN P� M-MODELLER, PORSCHE, ALPINA, AMG MM.<br>
Garantin kan f�s p� bilar yngre �n 8 �r och "rullat" mindre �n 17000 mil.
<p>
Genom samarbete med Elcon Finans har vi m�jlighet att erbjuda finansiering enligt f�ljande villkor: 
<br>
Minimum 20% kontantinsats eller inbytesbil.<br>
Uppl�ggningsavgift: 495:-<br>
Aviavgift: 45:-<br>
R�rlig r�nta: fr�n 6,65%<br>
N�gra �vriga avgifter tillkommer ej, inte heller vid f�rtidsl�sen av kontraktet.
<p>
�ppettider:<br>
M�n - Fre: 10.00 - 18.00<br>
L�rdag: 11.00 - 14.00<br>
�vrig tid enligt �verenskommelse.
<p>
V�lkommen!', 'http://www.autohaus.se', 'active', false, '2004-06-17 09:15:00', '2016-07-01 23:59:59', 'BMW, Mercedes, Saab, Volvo,', '-', 'patrik', '-', '-', 'patrik', 'patrik', 23, 3, 1020);
INSERT INTO stores VALUES (12841, 'Lindqvist Bil AB', 'thomas_lindqvistbil.nu@blocket.se', '0221-184 30', '0221-218 28', 'M�staregatan 1', '731 50', 'K�PING', '-', '�terf�rs�ljare f�r Ford', 'Vi �r ett fullservicef�retag i bilbranschen.
<p>
Vi ser st�ndigt �ver v�ra rutiner och st�ller mycket h�ga krav p� oss sj�lva n�r det g�ller att leverera perfekt kundservice. 
<p>
Vi t�rs s�ga att vi �r i t�ten n�r det g�ller att ha HELT n�jda kunder. V�r kundservice g�r inte att k�pa hos n�gon annan!
<p>
Kort sagt �r v�rt m�l att aldrig ge v�ra kunder anledning att byta leverant�r p� grund av orsaker som vi kan p�verka.
<p>
<b>L�rdagar 13-16 samt S�ndagar 12-16 n�s vi p� tel. 070-299 37 96</b>
<p>
V�lkomna!', 'http://www.lbil.com
', 'active', false, '2004-06-17 11:00:00', '2016-01-17 23:59:59', 'Ford,', '-', 'patrik', '-', '-', 'patrik', 'patrik', 9, 0, 1020);
INSERT INTO stores VALUES (12842, 'Bilh�rnan AB', 'bilhornan.solna_telia.com@blocket.se', '08-27 44 50', '08-27 57 74', 'R�sundav�gen 51', '169 57', 'SOLNA', '-', 'M�rkesoberoende handlare.', 'M�rkesoberoende handlare.', '-', 'active', false, '2004-06-17 11:30:00', '2017-01-01 23:59:59', 'Audi, Citro�n, Mazda, Mitsubishi, Renault, Toyota, Volkswagen, VW, Volvo,', '-', 'patrik', '-', '-', 'patrik', 'patrik', 11, 0, 1020);
INSERT INTO stores VALUES (12985, 'Beckers Bil AB', 'info_beckersbil.se@blocket.se', '0121-145 45', '0121-146 00', 'Norrk�pingsv�gen, BOX 76', '614 22', 'S�DERK�PING', 'beckers_logo.gif', '�terf�rs�ljare f�r Ford', 'Beckers Bil AB erbjuder ett stort urval av de vanligast f�rekommande m�rkena. Snabb oms�ttning och l�ga omkostnader kommer v�ra kunder till gagn i form av l�ga priser.
<p> 
Beckers finns sedan 1968 vid norra infarten till S�derk�ping. Bilfirman �r dock �ldre �n s�. Redan 1954 startade familjef�retaget i Valdemarsvik. Alltsedan dess har Beckers k�pt nyare begagnade bilar i parti och s�lt dem i �ndam�lsenliga lokaler.
<p>
Vi �r alltid intresserade av att k�pa in nyare svensks�lda bilar. �r du intresserad av att s�lja s� h�r av dig till Ulf Becker hos oss f�r att f� din bil v�rderad.
<p>
�ppettider:<br>
Vardagar 09.00-18.00<br>
L�rdagar 10.00-14.00
<p>
V�lkomna!', 'http://www.beckersbil.se', 'active', false, '2004-08-19 11:00:00', '2016-03-31 23:59:00', 'Ford,', '-', 'patrik', '-', '-', 'patrik', 'patrik', 14, 0, 1020);
INSERT INTO stores VALUES (12847, 'Eklunds Bil AB', 'reimond_eklundsbil.com@blocket.se', '031-744 44 90', '031-744 44 99', 'Smidesgatan 2', '417 07', 'G�TEBORG', 'eklunds_logo.gif', '�terf�rs�ljare f�r MG, Rover', 'P� Eklunds Bil m�ts du av ett personligt mottagande. Det �r s� vi har byggt upp v�r goda renomm�. Vi har arbetat upp relationer med v�ra kunder med ambitionen att vara det personliga och trygga valet f�r dig n�r du skall g�ra en bilaff�r. F�r oss �r det lika naturligt att g�ra aff�rer med s�v�l privata kunder som f�retag.
<p>
K�nn dig v�lkommen till oss p� Eklunds Bil.
<p>
�ppettider:<br>
M�ndag - Fredag 09.00-18.00<br>
L�rdag 11.00-15.00<br>
S�ndag 11.00-15.00', 'http://www.eklundsbil.com', 'active', false, '2004-06-18 09:00:00', '2016-08-31 23:59:59', 'Audi, BMW, Ford, Honda, Hyundai, MG, Mitsubishi, Nissan, Rover, Saab, Volkswagen, VW, Volvo,', '-', 'patrik', '-', '-', 'patrik', 'patrik', 15, 0, 1020);
INSERT INTO stores VALUES (12849, 'RK Bil AB', '-', '0510-800 80', '0510-800 80', 'Bustorpsv�gen 4', '530 30', 'TUN', 'rk_logo.gif', 'M�rkesoberoende handlare', 'L�t oss hj�lpa dig hitta r�tt bil, snabbt, enkelt och p�litligt. 
RK Bil �r ett litet familjef�retag med stor k�nsla f�r kvalitet och service. Varje �r levererar vi ett stort antal bilar till n�jda kunder. Vi l�gger ner stort arbete p� varje enskild bils skick och finish. Vi har egen rekonditionering och verkstad och kan d�rf�r h�lla l�ga priser.
<p>
<b>Prisinfo:</b><br>
1500kr i skrotningsavgift tillkommer.<br>
Priser utan byte.<br>
Registreringsnumren som finns p� n�tet �r bilarnas serienr i annonserna.
<p>
Maila inte. Ring ist�llet.<br>
Tel. tid 10.00- 22.00.<br>
�ppet efter �verrenskommelse alla dagar �ven p� helger.<p>

F�r fler bilder p� alla bilar, klicka <A HREF=" http://www.bytbil.com/select_car.cgi?seller=rkbiltun&sort=letter&look=blocket" TARGET="_blank">h�r</a>', 'http://www.billigbil.se
', 'active', false, '2004-06-18 10:00:00', '2016-10-01 23:59:59', 'BMW, Ford, Mazda, Mercedes, Renault, Volkswagen, VW, Volvo,', '-', 'patrik', '-', '-', 'patrik', 'patrik', 13, 0, 1020);
INSERT INTO stores VALUES (12850, 'M. T�rngren Bil AB', 'info-mtbil_telia.com@blocket.se', '033-28 25 27', '-', 'Kornellgatan 2', '507 31', 'BR�NNHULT', 'torngrens_logo.gif', 'M�rkesoberoende handlare', 'Vi �r ett bilf�retag utanf�r Bor�s som s�ljer, k�per och exporterar s�v�l importerade som svensks�lda bilar inom EU.
Alla bilar �r v�ldokumenterade och testade med varudeklaration.
<p>
F�r att du som kund skall k�nna dig trygg:<br>
1 �rs Euro-garanti kan man f� mot till�gg till de flesta bilarna.
<p>
V�r inriktning �r mestadels specialbilar, sportbilar cabbar, b�tar.
<p>
Vi kan �ven n�s p� mobil: 0706-88 60 10
<p>
V�lkomna!', 'http://www.torngrensbil.com', 'active', false, '2004-06-21 09:00:00', '2016-06-30 23:59:59', 'BMW, Porsche,', '-', 'patrik', '-', '-', 'patrik', 'patrik', 16, 0, 1020);
INSERT INTO stores VALUES (12852, 'Bil Brodd AB', 'mikael.brodd_bilbrodd.se@blocket.se', '0142-29 94 90', '0142-29 94 99', 'Borgm�stargatan 14-16, Box 85', '596 22', 'SK�NNINGE', 'bilbrodd_logo.gif', '�terf�rs�ljare f�r Nissan, Opel och Saab', '�terf�rs�ljare f�r Nissan, Opel och Saab.', 'http://www.bilbrodd.se', 'active', false, '2004-06-10 09:30:00', '2015-12-01 23:00:00', 'Nissan, Opel, Saab, ', '-', 'patrik', '-', '-', 'patrik', 'patrik', 14, 0, 1020);
INSERT INTO stores VALUES (12854, 'Orrekulla Bil AB', 'mikael_orrekullabil.se@blocket.se', '031-57 98 90', '031-57 17 90', 'Tagenev�gen 55', '425 37', 'HISINGS-K�RRA', 'orrekulla_logo.gif', 'Det lilla f�retaget med personlig service', '�ppettider:<br>
M�ndag - Fredag 9-18.
<p>
Kom och se v�ra fina bilar i v�ra trevliga lokaler p� Tagenev�gen.
<p>
V�lkommen!', 'http://www.orrekullabil.se', 'active', false, '2004-06-23 10:43:00', '2016-07-01 23:59:59', 'BMW, Mercedes, Mitsubishi,', '-', 'patrik', '-', '-', 'patrik', 'patrik', 15, 0, 1020);
INSERT INTO stores VALUES (12855, 'Hans Persson Bil  AB i Hedemora', 'info_hansperssonbil.se@blocket.se', '0225-121 50', '0225-154 10', 'Gussarvsgatan 31', '776 21', 'HEDEMORA', 'hanspersson_logo.gif', '�terf�rs�ljare f�r Renault, Volvo', 'Ett bilk�p �r en stor investering och vi tar det h�r p� fullaste allvar. Vi vill g�ra allt f�r att hj�lpa dig v�lja r�tt. Bilar skall man naturligtvis se och uppleva i verkligheten. Kom in s� skall du f� k�nna p�, sitta i och provk�ra den bil, eller de bilar, som kan vara intressanta f�r dig.
<p>
Ni �r v�lkommen att kontakta n�gon av v�ra s�ljare:<br>
Kjell Enstr�m<br>
Tel: 0225-77 40 05<br>
E-post: <a href="mailto:kjell.enstrom_hansperssonbil.se">kjell.enstrom@hansperssonbil.se</a>
<p>
Sven-Erik Eriksson<br>
Tel: 0225-77 40 04<br>
E-post: <a href="mailto:sven-erik.eriksson@hansperssonbil.se">sven-erik.eriksson@hansperssonbil.se</a>
<p>
�ppettider:<br>
M�ndag - fredag: 9.00 - 18.00 <br>
L�rdag: 10.00 - 14.00 <br>
@blocket.se', 'http://www.hansperssonbil.se', 'active', false, '2004-06-23 09:00:00', '2016-12-31 23:59:59', 'Volvo, Renault,', '-', 'patrik', '-', '-', 'patrik', 'patrik', 6, 0, 1020);
INSERT INTO stores VALUES (12856, 'Hans Persson Bil  AB i Fagersta', 'info_hansperssonbil.se@blocket.se', '0223-718 00', '0223-142 75', 'Virsbov�gen 8', '737 30', 'FAGERSTA', 'hanspersson_logo.gif', '�terf�rs�ljare f�r Renault, Volvo', 'Ett bilk�p �r en stor investering och vi tar det h�r p� fullaste allvar. Vi vill g�ra allt f�r att hj�lpa dig v�lja r�tt. Bilar skall man naturligtvis se och uppleva i verkligheten. Kom in s� skall du f� k�nna p�, sitta i och provk�ra den bil, eller de bilar, som kan vara intressanta f�r dig.
<p>
Ni �r v�lkommen att kontakta n�gon av v�ra s�ljare:<br>
Patrik Hjertqvist<br>
Tel: 0223 - 718 02<br>
E-post: <a href="mailto:patrik.hjertqvist_hansperssonbil.se">patrik.hjertqvist@hansperssonbil.se</a>
<p>
Peter Fredriksson<br>
Tel: 0223-718 03<br>
E-post: <a href="mailto:peter.fredriksson@hansperssonbil.se">peter.fredriksson@hansperssonbil.se</a>
<p>
�ppettider:<br>
M�ndag - fredag: 9.00 - 18.00 <br>
L�rdag: 10.00 - 14.00 <br>
@blocket.se', 'http://www.hansperssonbil.se', 'active', false, '2004-06-23 09:00:00', '2016-12-31 23:59:59', 'Volvo, Renault,', '-', 'patrik', '-', '-', 'patrik', 'patrik', 9, 0, 1020);
INSERT INTO stores VALUES (12857, 'Hans Persson Bil  AB i Hallstahammar', 'info_hansperssonbil.se@blocket.se', '0220-230 90', '0220-518 70', 'V�ster�sv�gen 5', '734 32', 'HALLSTAHAMMAR', 'hanspersson_logo.gif', '�terf�rs�ljare f�r Renault, Volvo', 'Ett bilk�p �r en stor investering och vi tar det h�r p� fullaste allvar. Vi vill g�ra allt f�r att hj�lpa dig v�lja r�tt. Bilar skall man naturligtvis se och uppleva i verkligheten. Kom in s� skall du f� k�nna p�, sitta i och provk�ra den bil, eller de bilar, som kan vara intressanta f�r dig.
<p>
Ni �r v�lkommen att kontakta n�gon av v�ra s�ljare:<br>
Hans Karlsson<br>
Tel: 0220-230 91<br>
E-post: <a href="mailto:hans.karlsson_hansperssonbil.se">hans.karlsson@hansperssonbil.se</a>
<p>
Jan-Erik Michelsen<br>
Tel: 0220-230 92<br>
E-post: <a href="mailto:jan-erik.michelsen@hansperssonbil.se">jan-erik.michelsen@hansperssonbil.se</a>
<p>
�ppettider:<br>
M�ndag - fredag: 9.00 - 18.00 <br>
L�rdag: 10.00 - 14.00 <br>
@blocket.se', 'http://www.hansperssonbil.se', 'active', false, '2004-06-23 09:01:00', '2016-12-31 23:59:59', 'Volvo, Renault,', '-', 'patrik', '-', '-', 'patrik', 'patrik', 9, 0, 1020);
INSERT INTO stores VALUES (12858, 'Hans Persson Bil  AB i K�ping', 'info_hansperssonbil.se@blocket.se', '0221-371 00', '0221-371 09', 'Ringv�gen 75', '731 33', 'K�PING', 'hanspersson_logo.gif', '�terf�rs�ljare f�r Renault, Volvo', 'Ett bilk�p �r en stor investering och vi tar det h�r p� fullaste allvar. Vi vill g�ra allt f�r att hj�lpa dig v�lja r�tt. Bilar skall man naturligtvis se och uppleva i verkligheten. Kom in s� skall du f� k�nna p�, sitta i och provk�ra den bil, eller de bilar, som kan vara intressanta f�r dig.
<p>
Ni �r v�lkommen att kontakta n�gon av v�ra s�ljare:<br>
Leif Nordstr�m<br>
Tel: 0221-371 11<br>
E-post: <a href="mailto:leif.nordstrom_hansperssonbil.se">leif.nordstrom@hansperssonbil.se</a>
<p>
Mats Ekberg<br>
Tel: 0221-371 14<br>
E-post: <a href="mailto:mats.ekberg@hansperssonbil.se">mats.ekberg@hansperssonbil.se</a>
<p>
Roland Larsson<br>
Tel: 0221-371 12<br>
E-post: <a href="mailto:roland.larsson@hansperssonbil.se">roland.larsson@hansperssonbil.se</a>
<p>
Ulf Thor�n<br>
Tel: 0221-371 13<br>
E-post: <a href="mailto:ulf.thoren@hansperssonbil.se">ulf.thoren@hansperssonbil.se</a>
<p>
�ppettider:<br>
M�ndag - fredag: 9.00 - 18.00 <br>
L�rdag: 10.00 - 14.00 <br>
@blocket.se', 'http://www.hansperssonbil.se', 'active', false, '2004-06-23 09:02:00', '2016-12-31 23:59:59', 'Volvo, Renault,', '-', 'patrik', '-', '-', 'patrik', 'patrik', 9, 0, 1020);
INSERT INTO stores VALUES (12859, 'Hans Persson Bil  AB i Sala', 'info_hansperssonbil.se@blocket.se', '0224 - 367 50', '0224 - 367 55', 'Berggatan 13', '733 22', 'SALA', 'hanspersson_logo.gif', '�terf�rs�ljare f�r Renault, Volvo', 'Ett bilk�p �r en stor investering och vi tar det h�r p� fullaste allvar. Vi vill g�ra allt f�r att hj�lpa dig v�lja r�tt. Bilar skall man naturligtvis se och uppleva i verkligheten. Kom in s� skall du f� k�nna p�, sitta i och provk�ra den bil, eller de bilar, som kan vara intressanta f�r dig.
<p>
Ni �r v�lkommen att kontakta n�gon av v�ra s�ljare:<br>
Bror Olsson<br>
Tel: 0224-367 53<br>
E-post: <a href="mailto:bror.olsson_hansperssonbil.se">bror.olsson@hansperssonbil.se</a>
<p>
Fredrik Undin<br>
Tel: 0224-367 51<br>
E-post: <a href="mailto:fredrik.undin@hansperssonbil.se">fredrik.undin@hansperssonbil.se</a>
<p>
Hasse Karlsson<br>
Tel: 0224-367 52<br>
E-post: <a href="mailto:hasse.karlsson@hansperssonbil.se">hasse.karlsson@hansperssonbil.se</a>
<p>
�ppettider:<br>
M�ndag - fredag: 9.00 - 18.00 <br>
L�rdag: 10.00 - 14.00 <br>
@blocket.se', 'http://www.hansperssonbil.se', 'active', false, '2004-06-23 09:03:00', '2016-12-31 23:59:59', 'Volvo, Renault,', '-', 'patrik', '-', '-', 'patrik', 'patrik', 9, 0, 1020);
INSERT INTO stores VALUES (12860, 'Hans Persson Bil  AB i V�ster�s', 'info_hansperssonbil.se@blocket.se', '021-17 88 00', '021-17 89 60', 'Str�mledningsgatan 11', '721 22', 'V�STER�S', 'hanspersson_logo.gif', '�terf�rs�ljare f�r Renault, Volvo', 'Ett bilk�p �r en stor investering och vi tar det h�r p� fullaste allvar. Vi vill g�ra allt f�r att hj�lpa dig v�lja r�tt. Bilar skall man naturligtvis se och uppleva i verkligheten. Kom in s� skall du f� k�nna p�, sitta i och provk�ra den bil, eller de bilar, som kan vara intressanta f�r dig.
<p>
Ni �r v�lkommen att kontakta n�gon av v�ra s�ljare:<br>
Anders Ros�n<br>
Tel: 021-17 88 03<br>
E-post: <a href="mailto:anders.rosen_hansperssonbil.se">anders.rosen@hansperssonbil.se</a>
<p>
Carl Berglind<br>
Tel: 021-17 88 05<br>
E-post: <a href="mailto:carl.berglind@hansperssonbil.se">carl.berglind@hansperssonbil.se</a>
<p>
Jan H�ggstr�m<br>
Tel: 021-17 88 02<br>
E-post: <a href="mailto:jan.haggstrom@hansperssonbil.se">jan.haggstrom@hansperssonbil.se</a>
<p>
Magnus Norrstr�m<br>
Tel: 021-17 89 77<br>
E-post: <a href="mailto:magnus.norrstrom@hansperssonbil.se">magnus.norrstrom@hansperssonbil.se</a>
<p>
Nils Wilander<br>
Tel: 021-17 88 43<br>
E-post: <a href="mailto:nils.wilander@hansperssonbil.se">nils.wilander@hansperssonbil.se</a>
<p>
Peter Halldin<br>
Tel: 021-17 88 45<br>
E-post: <a href="mailto:peter.halldin@hansperssonbil.se">peter.halldin@hansperssonbil.se</a>
<p>
Staffan Johansson<br>
Tel: 021-17 88 32<br>
E-post: <a href="mailto:staffan.johansson@hansperssonbil.se">staffan.johansson@hansperssonbil.se</a>
<p>
�ppettider:<br>
M�ndag - fredag: 9.00 - 18.00 <br>
L�rdag: 10.00 - 14.00 <br>
S�ndag: 11.00-15.00@blocket.se', 'http://www.hansperssonbil.se', 'active', false, '2004-06-23 08:00:00', '2016-12-31 23:59:59', 'Volvo, Renault,', '-', 'patrik', '-', '-', 'patrik', 'patrik', 9, 0, 1020);
INSERT INTO stores VALUES (12861, 'Bilia AB i Sk�vde', 'agne.axelsson_bilia.se@blocket.se', '0500-47 34 00', '0500-41 12 42', 'Vadsbov�gen/T�rebodav�gen', '541 29', 'SK�VDE', 'bilia_logo.gif', '�terf�rs�ljare f�r Renault, Volvo', 'Bilia bedriver personbilsf�rs�ljning i Sverige, Norge och Danmark. P� samtliga marknader erbjuder vi bilf�rs�ljning, finansiering, verkstadstj�nster samt tillbeh�rs- och reservdelsf�rs�ljning. I Sverige s�ljer vi ocks� drivmedel. Bilia �r Nordens st�rsta �terf�rs�ljare av Volvo och Renault. I Sverige s�ljer vi �ven Ford och i Norge Land Rover. N�r det handlar om service dominerar vi marknaden f�r nyare bilar fr�n Volvo och Renault.', 'http://www.bilia.se
', 'active', false, '2004-06-23 15:00:00', '2016-06-22 23:59:59', 'Renault, Volvo,', '-', 'patrik', '-', '-', 'patrik', 'patrik', 13, 0, 1020);
INSERT INTO stores VALUES (12862, 'City Bil i Dalarna AB, Falun', 'magnus_citybil.se@blocket.se', '023-186 50', '023-136 50', 'V�stermalmsv�gen 2', '791 25', 'FALUN', 'citybil_logo.gif', '�terf�rs�ljare f�r Citro�n, Mazda', '�terf�rs�ljare f�r Citro�n, Mazda.<br>
12 m�naders garanti p� begagnade bilar.
<p>
�ppettider:<br>
M�n-Fre: 09.00-18.00<br>
L�rdag: 11.00 - 14.00
<p>
V�lkomna!', 'http://www.citybil.se', 'active', false, '2004-06-23 16:00:00', '2016-01-01 23:59:59', 'Citro�n, Citroen, Mazda,', '-', 'patrik', '-', '-', 'patrik', 'patrik', 6, 0, 1020);
INSERT INTO stores VALUES (12863, 'City Bil i Dalarna AB, Borl�nge', 'magnus_citybil.se@blocket.se', '0243-25 36 36', '0243-25 36 37', 'Projektgatan 7', '781 70', 'BORL�NGE', 'citybil_logo.gif', '�terf�rs�ljare f�r Citro�n, Mazda', '�terf�rs�ljare f�r Citro�n, Mazda.<br>
12 m�naders garanti p� begagnade bilar.
<p>
�ppettider:<br>
M�n-Fre: 09.00-18.00<br>
L�rdag: 11.00 - 14.00
<p>
V�lkomna!', 'http://www.citybil.se', 'active', false, '2004-06-23 16:01:00', '2016-01-01 23:59:00', 'Citro�n, Citroen, Mazda,', '-', 'patrik', '-', '-', 'patrik', 'patrik', 6, 0, 1020);
INSERT INTO stores VALUES (12865, 'BilNilsson Bil', 'kenneth.nilsson_bilnilsson.se@blocket.se', '0431-821 50', '0431-834 38', 'Munka Ljungbyv�gen 26', '262 01', '�NGELHOLM', 'bilnilsson_logo.gif', '�terf�rs�ljare f�r Alfa Romeo, Fiat, Honda, MG, Rover', 'Bilnilsson har en fullserviceanl�ggning i �ngelholm med allt vad du beh�ver f�r ett bil�gande. Ett unikt brett sortiment av nya bilar, ett intressant utbud av begagnat och en kvalificerad verkstad g�r Bilnilsson till ett naturligt val f�r dig som �r ute efter ett tryggt bil�gande.
<p>
�ppettider f�rs�ljning:<br>
M�n-fre 9-18<br>
L�r-s�n 11 - 15 
<p>
�ppettider service:<br>
M�n-fred 7-16.30<br>
(lunchst�ngt 11.30 - 12.30). 
<p>
V�lkommen �nskar Kenneth och medarbetarna p� Bilnilsson.', 'http://www.bilnilsson.se', 'active', false, '2004-06-24 09:00:00', '2016-07-01 23:59:59', 'Alfa Romeo, Fiat, Honda, MG, Rover,', '-', 'patrik', '-', '-', 'patrik', 'patrik', 23, 16, 1020);
INSERT INTO stores VALUES (12866, 'Strandells Motor AB', 'info_strandells-motor.se@blocket.se', '042-14 42 00', '042-14 42 01', 'Dragaregatan 5', '254 42', 'HELSINGBORG', 'strandells_logo.gif', '�terf�rs�ljare f�r Honda', '�r Du intresserad av en ny eller begagnad HONDA �r det h�r ett bra s�tt att b�rja med. Vi har �ven ett brett urval begagnade bilar.
<p>
�ppettider:<br>
M�ndag - fredag 9 - 18<br>
L�rdag - s�ndag 11 - 15 
<p>
V�lkommen in till Sveriges st�rsta HONDA-�terf�rs�ljare!!', 'http://www.strandells-motor.se', 'active', false, '2004-06-24 09:01:00', '2016-07-01 23:59:59', 'Honda,', '-', 'patrik', '-', '-', 'patrik', 'patrik', 23, 3, 1020);
INSERT INTO stores VALUES (12867, 'FA Motor och Bil F�rs�ljning AB', 'fa_bostream.nu@blocket.se', '08-645 69 53', '08-645 69 53', 'F�gels�ngsv�gen 1', '117 68', 'STOCKHOLM', '-', 'M�rkesoberoende handlare', 'FA Motor & Bilf�rs�ljning erbjuder v�ra kunder f�rstklassig service av bilf�rs�ljning samt egen verkstad. Vi har st�ndigt intressanta bilar inne f�r f�rs�ljning.
<p>
Vi tar �ven inbyten
av b�de bilar, b�tar och mc.
<p>
Hos FA Motor & Bilf�rs�ljning kan du k�pa din bil kontant eller p�
avbetalning:
<br>
- Amorteringstid 12-60 m�nader
<br>
- Kontantinsats, minst 20% av totalpriset.
<p>
�ppettider:<br>

M�n � Fred 10.00�18.00<br>

�ven helg�ppet, ring g�rna innan ni kommer s� att vi �r p� plats.
<p>
Kontoret 08/6456953
<br>
Henrik 073/5002747
<br>
Ola 073/5002717', '-', 'active', false, '2004-06-24 10:00:00', '2017-01-31 23:59:59', 'Audi, Ford, Hyundai, Mercedes, Mitsubishi, Opel, Toyota,', '-', 'patrik', '-', '-', 'patrik', 'patrik', 11, 0, 1020);
INSERT INTO stores VALUES (12868, 'Daniels Bil AB', 'k.daniels_telia.com@blocket.se', '031-25 23 23', '031-707 51 56', 'S:t Pauligatan 33', '416 60', 'G�TEBORG', 'danielsbilgbg_logo.gif', 'Hos oss hittar du bilen du letar efter.', 'Som oberoende bilhandlare kan vi erbjuda en rad av de mest k�nda m�rkena p� marknaden.
<p>
Vi handlar i huvudsak med nyare begagnade bilar men kan naturligtvis leverera just ditt �nskem�l. S�ledes v�r arbetsfilosofi:<br>
"R�tt bil - r�tt pris".
', 'http://www.danielsbil.com', 'active', false, '2004-06-24 12:00:00', '2006-10-15 23:59:59', 'Mercedes, Opel, Saab, Volkswagen, VW, Volvo,', '-', 'patrik', '-', '-', 'patrik', 'patrik', 15, 0, 1020);
INSERT INTO stores VALUES (13537, 'LM Bil AB', 'info_lmbil.nu@blocket.se', '0563 - 125 60', '0563 - 523 00', 'Munkerudsv�gen 36', '684 32', 'MUNKFORS', '1_2_05.gif', 'V�lkommen till L- M Bil AB!', '<p>
L- M Bil bildades 1995.<br>
Vi sysslar med f�rs�ljning av bilreservdelar och tillbeh�r. Vi tillhandah�ller ett brett sortiment av delar till s� gott som alla m�rken. Vi har samarbete med de flesta stora generalagenterna s�som ABR, MBD, Gete, Bilatlas m.fl. 
<p>
Vi sysslar �ven med partik�p av t.ex. konkurslager och restpartier. Detta medf�r att vi har ett stort sortiment av delar till lite �ldre och ovanliga bilar. 
<p>
Vi har gott om bl.a. framsk�rmar till lite �ldre bilar, t.ex. Opel, Ford, Volvo m.fl. Vi f�r delar med overnightleverans
<p>
Kontakta oss g�rna f�r mer information.<p>', 'http://www.lmbil.nu

', 'active', false, '2005-02-01 00:00:00', '2016-04-10 23:59:59', 'konkurslager, framsk�rmar, sk�rmar, Opel, Ford, Volvo, bilreservdelar, tillbeh�r, Bildelar, Biltillbeh�r, ', '-', 'patrik', '-', '-', 'patrik', 'patrik', 7, 0, 1040);
INSERT INTO stores VALUES (12871, 'Gr�ndals Bil & Motor', 'info_grondalsbilmotor.se@blocket.se', '08-645 65 75', '08-645 65 75', 'Sp�ngav�gen 306', '163 46', 'SP�NGA', 'grondals_logo.gif', 'M�rkesoberoende handlare', 'Gr�ndals Bil & Motor startade sin verksamhet 1974. Med n�stan 30-�rs erfarenhet erbjuder vi v�ra kunder f�rstklassig service av bilf�rs�ljning samt egen verkstad.
<p>
Vi har st�ndigt intressanta bilar inne f�r 
f�rs�ljning.
Vi tar �ven inbyten av b�de bilar och mc.
<p>
Hos Gr�ndals Bil och Motor kan du k�pa din bil kontant eller p� avbetalning:<br>
- Amorteringstid 12-60 m�nader<br>
- Kontantinsats, minst 20% av totalpriset.<br>
Leasing kan ocks� erbjudas.
<p>
Ni n�r oss �ven p� : 070 486 88 08
<p>
V�lkommen!
', 'http://www.grondalsbilmotor.se', 'active', false, '2004-06-30 13:30:00', '2016-07-15 23:59:59', 'Audi, BMW, Fiat, Ford, Mercedes, Saab, Seat, Volvo,', '-', 'patrik', '-', '-', 'patrik', 'patrik', 11, 0, 1020);
INSERT INTO stores VALUES (12873, 'CARETA', 'johan.kempas_careta.nu@blocket.se', '060-18 08 00', '060-53 56 20', 'Terminalv�gen 4', '861 23', 'TIMR�', 'careta_logo.gif', '�terf�rs�ljare f�r Renault & Volvo', 'Careta �r en ny del av Bilbolaget d�r vi samlar hela v�r verksamhet f�r begagnade bilar. Under ett helt nytt koncept erbjuder vi dig Norrlands st�rsta utbud av begagnade bilar av olika m�rken och modeller, i alla prisklasser. I Caretas bilhall finns �ver 150 bilar att v�lja bland. Dessutom ska du som k�per en begagnad bil av oss f� samma goda service och bem�tande som de som v�ljer att k�pa en ny bil. Fr�n Bilbolaget tar vi med oss kunskapen om att varje k�pare har olika behov. Och att bilen alltid �r ny f�r kunden. �ven den begagnade. Visst l�ter det sj�lvklart men hos oss �r det mer �n bara ord.
<p>
<b>Alla priser prutade och klara</b><br>
Hos oss hittar du bilar i alla prisklasser. Det ger dig stora valm�jligheter inf�r ditt bilk�p. Du ska heller inte beh�va fundera �ver priset. P� Careta i Timr� g�ller fast pris. Prutat och klart.
<p>
<b>Alltid p� plats</b><br>
Bilhallen har �ppet fr�n sju dagar i veckan. Webbplatsen har �ppet dygnet runt och d�r hittar du alla bilar vi har till salu. Vi f�rs�ker helt enkelt att finnas p� plats n�r du beh�ver oss. Kom till oss och k�p din begagnade bil av kunniga medarbetare som hj�lper dig att hitta den bil du beh�ver.
<p>
<b>Begagnat med kvalitet</b><br>
Alla v�ra bilar �r noggrannt genomg�ngna och du f�r en varudeklaration som visar bilens kondition och s�kerhet. Vi erbjuder dessutom f�rm�nliga garantier och hj�lp med finansiering. P� Careta har du �ven tillg�ng till verkstad om din bil beh�ver service.
<p>
V�lkommen!', 'http://www.careta.nu', 'active', false, '2005-05-03 00:00:00', '2016-05-15 23:59:59', 'Audi, Chevrolet, Citro�n, Ford, Hyundai, Opel, Renault, Saab, Toyota, Volkswagen, VW, Volvo,    ', '-', 'patrik', '-', '-', 'patrik', 'patrik', 4, 0, 1020);
INSERT INTO stores VALUES (12874, '�redalen Bil AB', 'aredalenbil_telia.com@blocket.se', '0647-61 18 28', '0647-66 85 28', 'Strandv�gen 1', '830 05', 'J�RPEN', '-', 'M�rkesoberoende handlare', 'M�rkesoberoende handlare!
<p>
V�lkommen!', '-', 'active', false, '2004-07-01 15:00:00', '2016-02-15 23:59:59', 'Audi, Chrysler, Mercedes, Mitsubishi, Opel, Volkswagen, VW, ', '-', 'patrik', '-', '-', 'patrik', 'patrik', 3, 0, 1020);
INSERT INTO stores VALUES (14200, 'Forsbergs Fritidscenter AB', 'info_forsbergsfritidscenter.se@blocket.se', '0320-305 50', '0320-305 55', 'Solbacken', '510 22', 'HYSSNA', 'butik.gif', '�terf�rs�ljare f�r Concorde, Frankia, Hobby, Polar, Solifer', '<b>V�lkommen till Forsbergs Fritidscenter!</b><p>

Forsbergs Fritidcenter �r idag Nordens st�rsta
f�retag med inriktning p� f�rs�ljning av husbilar och husvagnar till konsument.<p>

Vi har l�ng erfarenhet av branschen. 
Vi har genom �ren blivit en modern fritidsanl�ggning med 6.000 kvm stor inomhusutst�llning och vi har alltid minst 150 objekt i lager.<p>

F�retaget syssel-s�tter totalt 19 personer
som har �ver 250 �rs samlad branscherfarenhet.
<p>

<b>EXTRA HELG�PPET!</b><br>

S�nd. 11-15 oktober ut.<br>
Vard. 9-18 oktober ut.<br>
D�refter 9-17.<p>

', 'http://www.forsbergsfritidscenter.se

', 'active', false, '2005-10-03 00:00:00', '2016-12-31 23:59:59', 'Concorde, Frankia, Hobby, Polar, Solifer, B�rstner, Cabby, Elnagh, Euramobil, Hobby, Hymer, Kabe, Knaus, Mirage, Niesmann, Tabbert, Volkswagen ', '-', 'patrik', '-', '-', 'patrik', 'patrik', 16, 0, 1100);
INSERT INTO stores VALUES (12879, 'F�renade Motor AB', 'mikael.lundqvist_forenademotor.se@blocket.se', '040-55 27 50', '040-55 27 59', 'Agnesfridsv�gen 185 D', '200 11', 'MALM�', 'forenademotor_logo.gif', 'Auktoriserad �terf�rs�ljare av Honda och Subaru', 'F�renade Motor AB �r �terf�rs�ljare f�r Honda och Subaru.
<p>
F�retaget �r ett fullservicef�retag inom bilbranchen och �r bel�get p� Agnesfridsv�gen 185 D i Malm�. 
<p>
I v�r bilhall finns ett stort urval av nya bilar och vi har �ven ett brett sortiment med demobilar. I v�r verkstad erbjuder vi service och reparationer f�r de m�rken vi representerar. Vi har �ven reservdelar f�r dessa m�rken. 
<p>
Vi erbjuder �ven konkurrenskraftiga finansieringsalternativ.', 'http://www.forenademotor.se', 'active', false, '2004-07-02 14:00:00', '2016-07-01 23:59:59', 'BMW, Ford, Honda, Hyundai, Nissan, Peugeot, Subaru, Volvo,', '-', 'patrik', '-', '-', 'patrik', 'patrik', 23, 11, 1020);
INSERT INTO stores VALUES (13037, 'Blennows Bil AB', 'alexander_blennowsbil.se@blocket.se', '0411-400 97', '0411-400 87', 'Norrgatan 5', '274 37', 'SKURUP', 'blennows_logo.gif', 'Auktoriserad �terf�rs�ljare och serviceverkstad f�r Nissan.', 'Blennows Bil AB �gs av Ingvar och Gert-Olle Blennow sedan 1968,
f�rutom nybils- och begagnatf�rs�ljning s� omfattar verksamheten
�ven pl�tverkstad (alla bilm�rken).
<p>
�ppettider:<br>
Vardagar: 09.00-18.00<br>
L�rdagar: 10.00-14.00
<p>
V�lkomna!', 'http://www.blennowsbil.se', 'active', false, '2004-09-10 08:00:00', '2016-04-15 23:59:59', 'Audi, BMW, Nissan, Saab, Volvo,', '-', 'patrik', '-', '-', 'patrik', 'patrik', 23, 11, 1020);
INSERT INTO stores VALUES (12881, 'Bilcenter Augustsson & Anderzon AB, Nyk�ping', 'info_bilcenter.com@blocket.se', '0155-20 51 00', '0155-28 77 90', 'Ringv�gen 58, Box 222', '611 25', 'NYK�PING', 'loggo.gif', '�terf�rs�ljare f�r Saab, Opel & Chevrolet', 'V�rt f�retag har gamla anor. �ver fyrtio �r. Trots att vi under �rens lopp haft olika namn och �gare har alltid kontinuiteten bevarats och �gandet har hela tiden varit Nyk�pinganknutet. Vi �r ett genuint Nyk�pingsf�retag.
<p>
Med hj�lp av v�rt breda modellprogram av Opel. Har vi en stora marknadsandelar i b�de Nyk�ping och Oxel�sunds omr�det. Vilket �r v�r m�ls�ttning f�r att trygga syssels�ttning f�r v�ra 44 anst�llda p� f�retaget. 
<p>
V�r ambition �r att ge b�de privat och f�retagskunder ett tryggt och ett problemfritt bil�gande med god totalekonomi.', 'http://www.bilcentergruppen.se', 'active', false, '2004-07-06 11:00:00', '2016-03-01 23:59:59', 'Daewoo, Opel, Saab,', '-', 'patrik', '-', '-', 'patrik', 'patrik', 12, 0, 1020);
INSERT INTO stores VALUES (12882, 'Bilcenter  i Eskilstuna AB', 'svara.inte@blocket.se', '016-17 43 00', '016-12 48 07', 'V�xelv�gen 8, Box 4036', '630 04', 'ESKILSTUNA', 'loggo.gif', '�terf�rs�ljare f�r Saab, Opel & Chevrolet', 'Hos Bilcenter i Eskilstuna AB hittar du ett av marknadens bredaste och starkaste bilsortiment. Oavsett vad du har f�r �nskem�l n�r det g�llar din bil, s� �r chansen mycket stor att vi har en modell som passar dina behov. Dessutom har vi en butik med bra tillbeh�r och en auktoriserad verkstad med kunniga mekaniker, riktiga originaldelar och l�ga priser.
<p>
L�s vidare p� v�r hemsida och kom sedan in till oss, s� ska vi se till att du blir riktigt n�jd med ditt bil�gande!', 'http://www.bilcentergruppen.se ', 'active', false, '2004-07-06 11:01:00', '2016-03-01 23:59:59', 'Daewoo, Opel, Saab,', '-', 'patrik', '-', '-', 'patrik', 'patrik', 12, 0, 1020);
INSERT INTO stores VALUES (12883, 'Bilcenter i Str�ngn�s', 'svara.inte@blocket.se', '0152-349 00', '0152-349 10', 'Ges�llv�gen 4', '641 45', 'STR�NGN�S', 'loggo.gif', '�terf�rs�ljare f�r Saab, Opel & Chevrolet', 'V�lkommen till Bilcenter i Str�ngn�s<P>
V�r ambition �r att ge b�de privat och f�retagskunder ett tryggt och ett problemfritt bil�gande med god totalekonomi. 
<p>
Vi p� Bilcenter hoppas att du ska finna vad du s�ker p� v�ra sidor om nya & beg bilar. Du finner information om kostnader att �ga bil. P� f�retagsbilar kan du se vad bilarna kostar med olika utrustningar och f� fram ditt f�rm�nsv�rde. 
<p>
�ven som privatkund kan du f� information om kostnader f�r att �ga bil p� Service & �gande. 
<p>
Se v�ra Erbjudande fr�n Butiken. 
<p>
Du kan best�lla tid f�r service & reparationer p� sidan service.   
 
', 'http://www.bilcentergruppen.se ', 'active', false, '2004-07-06 11:02:00', '2016-03-01 23:59:59', 'Opel, Saab,', '-', 'patrik', '-', '-', 'patrik', 'patrik', 12, 0, 1020);
INSERT INTO stores VALUES (12884, 'Bilcenter i Katrineholm', 'svara.inte@blocket.se', '0150-48 91 91', '0150-107 76', 'Starrv�gen 5', '641 22', 'KATRINEHOLM', 'loggo.gif', '�terf�rs�ljare f�r Saab, Opel & Chevrolet', 'V�lkommen till Bilcenter i Katrineholm  
<p> 
V�r ambition �r att ge b�de privat och f�retagskunder ett tryggt och ett problemfritt bil�gande med god totalekonomi. 
<p>
Vi p� Bilcenter hoppas att du ska finna vad du s�ker p� v�ra sidor om nya & beg bilar. Du finner information om kostnader att �ga bil. P� f�retagsbilar kan du se vad bilarna kostar med olika utrustningar och f� fram ditt f�rm�nsv�rde. 
<p>
�ven som privatkund kan du f� information om kostnader f�r att �ga bil p� Service & �gande. 
<p>
Se v�ra Erbjudande fr�n Butiken. 
<p>
Du kan best�lla tid f�r service & reparationer p� sidan service.  
', 'http://www.bilcentergruppen.se ', 'active', false, '2004-07-06 11:03:00', '2016-03-01 23:59:59', 'Opel, Saab,', '-', 'patrik', '-', '-', 'patrik', 'patrik', 12, 0, 1020);
INSERT INTO stores VALUES (3946, 'Ett str� vassare', '-', '0456-169 39', '0456-169 39', 'Egnahemsv�gen 122 - 4', '294 92', 'S�LVESBORG', 'ettstra.gif', 'Vi �r en seri�s bilimport�r som har m�nga �rs erfarenhet i branchen.', 'S�ker ni n�got vi inte har i lager, s� tar vi hj�lp av v�rt breda kontaktn�t i Europa.
<p>
Mobil: 0708-555 998', 'http://www.ettsv.com', 'active', false, '2003-03-11 00:00:00', '2004-04-01 00:00:00', 'Import, Tyskland', '-', 'patrik', '-', '-', 'patrik', 'patrik', 22, 0, 1020);
INSERT INTO stores VALUES (20000, 'Svenska Bilgruppen AB', 'svenska_bilgruppen.se@blocket.se', '08-84 10 10', '08-84 13 00', 'Lignagatan 1', '117 34', 'STOCKHOLM', 'svb_logo.gif', 'S�LJER-KÖPER-BYTER Bra begagnade bilar. MRF-Handlare', '<b>V�lkommen till oss p� Svenska Bilgruppen!</b>
<p>
Bilarfirman man v�ljer n�r man k�per och s�ljer.
<p>
Vi har specialiserat oss p� bra begagnade bilar fr�n �rsmodell -90 till -2005. Bes�k v�r bilhall eller ta en titt p� v�r hemsida d�r hittar du bilder p� alla bilar vi s�ljer. Vi har alltid ett 50-tal bilar i lager till f�rm�nliga priser.
<p>
V�ra l�ga omkostnader medf�r konkurrenskraftiga priser.
Svenska Bilgruppen �r medlem i Motorbranschens riksf�rbund (MRF), vilket medf�r en trygghet f�r dig som kund.
<p>
<b>Ring eller bes�k oss, det l�nar sig alltid!</b>', 'http://www.bilgruppen.se', 'active', false, '2003-06-12 15:30:00', '2006-09-01 23:59:59', 'BMW, Alfa Romeo, Audi, Cadillac, Chevrolet, Chrysler, Ford, Mercedes, Opel, Saab, Subaru, Volkswagen, Volvo, Toyota,', '-', 'patrik', '-', '-', 'patrik', 'patrik', 11, 0, 1020);
INSERT INTO stores VALUES (14661, 'Arvika Bilsmide AB', 'patrik@blocket.se', '0570-123 57', '0570-805 20', 'Mosseberg', '671 91', 'ARVIKA', 'butik.gif', 'Reparationer, skrotning och uppbyggnad av tunga fordon.', '<b>V�lkommen till Arvika Bilsmide AB</b>
<p>
F�retaget startades 1988 av 3 del�gare i hyrd lokal p� Viks industriomr�de i Arvika.
<p>
Nya verkstadslokaler byggdes 1990 p� Mossebergs industriomr�de. 
<p>
F�retaget �gs och drivs sedan 1:e juli 2004 av Jonas H�glund
<p>
Vi sysslar med reparationer, skrotning och uppbyggnad av tunga fordon. 
<br>
Vi g�r ocks� p�byggnationer och ombyggnationer.

<p>
Öppetider 07.00 - 16.00<br>
Övriga tider ring 070-338 57 88<p>

', 'http://www.arvikabilsmide.com
', 'active', true, '2005-11-28 00:00:00', '2016-11-30 23:59:59', 'reparationer, skrotning, uppbyggnad  ', '-', 'patrik', '-', '-', 'patrik', 'patrik', 7, 0, 1220);
INSERT INTO stores VALUES (14662, 'Kronofogden AB', 'blocket_kronofogden.se@blocket.se', '0570-123 57', '0570-805 20', 'Mosseberg', '671 91', 'ARVIKA', 'butik.gif', 'fordon.', '<b>V�lkommen till Arvika Bilsmide AB</b>
<p>
F�retaget startades 1988 av 3 del�gare i hyrd lokal p� Viks industriomr�de i Arvika.
<p>
Nya verkstadslokaler byggdes 1990 p� Mossebergs industriomr�de. 
<p>
F�retaget �gs och drivs sedan 1:e juli 2004 av Jonas H�glund
<p>
Vi sysslar med reparationer, skrotning och uppbyggnad av tunga fordon. 
<br>
Vi g�r ocks� p�byggnationer och ombyggnationer.

<p>
Öppetider 07.00 - 16.00<br>
Övriga tider ring 070-338 57 88<p>

', 'http://www.arvikabilsmide.com
', 'active', true, '2005-11-28 00:00:00', '2016-11-30 23:59:59', 'reparationer, skrotning, uppbyggnad  ', '-', 'patrik', '-', '-', 'patrik', 'patrik', 7, 0, 1220);
-- INSERT INTO stores VALUES (14634, 'FreshMilk', 'info@freshmilk.se', '031 3609295', NULL, 'Kastellgatan 13a', '41307', 'G�TEBORG', 'logo_14634_qav.jpg', 'FreshMilk f�r Tuffa Kids till Trendiga P�ron!!   JUST NU!! H�STNYHETER!!', '����������������������<br />JUST NU H�STNYHETER!!<br />����������������������<br /><br />Tr�tt p� de traditionella barn och mammakl�derna!? D� har ni hittat r�tt. <br /> V�r ambition �r att alltid erbjuda ett attraktivt sortiment till bra priser med mycket kort leveranstid. <br /><br /> I v�rt sortiment hittar du alltid det senaste inom barnkl�der,  mammakl�der, �kp�sar, napph�llare och sk�tv�skor mm. <br /><br /> Vi kan erbjuda kl�der fr�n Kikkid, Koeka, MiniSafari, Nanoou, Shampoodle,  Urban Elk, Varmiso, No Added Sugar, Katvig, Lodger, Ida T, Stardust, Lundmyr of Sweden, Skip*Hop och Fun Mom med fler. <br /><br /> Hos oss kan du �ven best�lla utskick av present till Jul, f�delsedag eller dop tex. vi sl�r d� in varan i exlusivt presentpapper och du v�ljer vad som skall st� p� paketet.  <br /><br /> Leveranstid 1-2 arbetsdagar efter mottagen betalning.  <br /><br />V�lkommen in i v�r webbutik alt. bes�k oss i v�r butik p� Kastellgatan 13a G�teborg.<br /><br />����������������������<br />JUST NU H�STNYHETER!!<br />����������������������<br />', 'http://www.freshmilk.se', 'active', false, '2005-11-22 00:00:00', '2008-01-01 00:00:00', 'barnkl�der,  mammakl�der, �kp�sar, napph�llare, sk�tv�skor, No Added Sugar, Katvig, Lodger, Ida T, Stardust, Lundmyr of Sweden, Skip*Hop och Fun Mom, Dop ', '770301-5029', 'Karljohansg. 59a', '41455', 'G�TEBORG', 'Anders Vesterlund', 'Johnie Dalliah', true, 11, 0, 4040);

--
-- PostgreSQL database dump complete
--

--
-- PostgreSQL database dump
--

SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = public, pg_catalog;

--
-- Data for Name: store_params; Type: TABLE DATA; Schema: public; Owner: pelle
--

INSERT INTO store_params VALUES (12, 'import_id', 'svbilgrupp');
INSERT INTO store_params VALUES (12, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (12, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=svbilgrupp&sort=letter&look=blocket');
INSERT INTO store_params VALUES (12, 'image_extra', 'mrf_logo_transp.gif');
INSERT INTO store_params VALUES (12, 'import_type', '1,3');
INSERT INTO store_params VALUES (12, 'url_path', 'svenskabil');

INSERT INTO store_params VALUES (31, 'import_id', 'capital');
INSERT INTO store_params VALUES (31, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (31, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=capital&sort=letter&look=blocket');
INSERT INTO store_params VALUES (31, 'image_extra', '-');
INSERT INTO store_params VALUES (31, 'import_type', '1,3');

INSERT INTO store_params VALUES (88, 'import_id', 'asobilhall');
INSERT INTO store_params VALUES (88, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (88, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=asobilhall&look=blocket');
INSERT INTO store_params VALUES (88, 'image_extra', '-');
INSERT INTO store_params VALUES (123, 'import_id', 'hogdalensbil');
INSERT INTO store_params VALUES (123, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (123, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=hogdalensbil&sort=letter&look=hogdalensbil');
INSERT INTO store_params VALUES (123, 'image_extra', '-');
INSERT INTO store_params VALUES (162, 'import_id', 'fittjabilnorsborg');
INSERT INTO store_params VALUES (162, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (162, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=fittjabilnorsborg&look=fittjabilnorsborg&carsperpage=20');
INSERT INTO store_params VALUES (162, 'image_extra', '-');
INSERT INTO store_params VALUES (240, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=lennartronn&sort=letter&look=hlista_2002');
INSERT INTO store_params VALUES (240, 'image_extra', '-');
INSERT INTO store_params VALUES (296, 'import_id', 'svmotorkompanietsthlm');
INSERT INTO store_params VALUES (296, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (296, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=svmotorkompanietsthlm&sort=letter&look=blocket');
INSERT INTO store_params VALUES (296, 'image_extra', '-');
INSERT INTO store_params VALUES (381, 'infopage_no_ads', 'http://www.trucker.se/cgi-bin/produkter.pl?falt=2&varde=1&unikt_falt=1&sortfalt1&sortextra=down&mall=kat_hmeny.html');
INSERT INTO store_params VALUES (381, 'image_extra', '-');
INSERT INTO store_params VALUES (481, 'import_partner', 'bostart');
INSERT INTO store_params VALUES (481, 'import_type', '5');
INSERT INTO store_params VALUES (481, 'hide_email', '1');
INSERT INTO store_params VALUES (481, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=sehlberg&sort=letter&look=blocket');
INSERT INTO store_params VALUES (481, 'image_extra', '-');
INSERT INTO store_params VALUES (541, 'import_id', 'FM');
INSERT INTO store_params VALUES (541, 'import_partner', 'interboat');
INSERT INTO store_params VALUES (541, 'infopage_no_ads', 'http://www.flippermarin.se');
INSERT INTO store_params VALUES (541, 'image_extra', '-');
INSERT INTO store_params VALUES (541, 'import_type', '2');
INSERT INTO store_params VALUES (542, 'import_id', 'wt_20215');
INSERT INTO store_params VALUES (542, 'import_partner', 'webtop');
INSERT INTO store_params VALUES (542, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=cartradetaby&look=cartradetaby');
INSERT INTO store_params VALUES (542, 'image_extra', '-');
INSERT INTO store_params VALUES (542, 'import_type', '5,6,7,8');
INSERT INTO store_params VALUES (547, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=viggbytaby&sort=letter&look=blocket');
INSERT INTO store_params VALUES (547, 'import_id', 'cap_7929');
INSERT INTO store_params VALUES (547, 'import_partner', 'capitex');
INSERT INTO store_params VALUES (547, 'image_extra', '-');
INSERT INTO store_params VALUES (547, 'import_type', '5,6,7,8');
INSERT INTO store_params VALUES (588, 'import_id', 'sthlmhusbilohusvagnsc');
INSERT INTO store_params VALUES (588, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (588, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=sthlmhusbilohusvagnsc&sort=letter&look=blocket');
INSERT INTO store_params VALUES (588, 'image_extra', '-');
INSERT INTO store_params VALUES (665, 'import_id', 'cap_7930');
INSERT INTO store_params VALUES (665, 'import_partner', 'capitex_broker');
INSERT INTO store_params VALUES (665, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=nissanmalmo&sort=letter&look=blocket');
INSERT INTO store_params VALUES (665, 'import_type', '6,7');
INSERT INTO store_params VALUES (665, 'image_extra', '-');
INSERT INTO store_params VALUES (14184, 'import_id', 'lagprisbilarlund');
INSERT INTO store_params VALUES (14184, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (14184, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=lagprisbilarlund&sort=letter&look=blocket');
INSERT INTO store_params VALUES (14184, 'image_extra', '-');
INSERT INTO store_params VALUES (798, 'import_id', 'martenssons');
INSERT INTO store_params VALUES (798, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (798, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=martenssons&sort=letter&look=martenssons&carsperpage=30
');
INSERT INTO store_params VALUES (798, 'image_extra', '-');
INSERT INTO store_params VALUES (913, 'import_id', 'skanebil');
INSERT INTO store_params VALUES (913, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (913, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=skanebil&sort=letter&look=blocket');
INSERT INTO store_params VALUES (913, 'image_extra', '-');
INSERT INTO store_params VALUES (14469, 'import_id', 'thornilsson');
INSERT INTO store_params VALUES (14469, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (14469, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=thornilsson&sort=letter&look=blocket');
INSERT INTO store_params VALUES (14469, 'image_extra', '-');
INSERT INTO store_params VALUES (994, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=newmanshm&sort=letter&look=blocket');
INSERT INTO store_params VALUES (994, 'image_extra', '-');
INSERT INTO store_params VALUES (13815, 'import_id', 'dinmotorgavle');
INSERT INTO store_params VALUES (13815, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (13815, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=dinmotorgavle&sort=letter&look=blocket');
INSERT INTO store_params VALUES (13815, 'image_extra', '-');
INSERT INTO store_params VALUES (14074, 'import_id', 'backmansmryd');
INSERT INTO store_params VALUES (14074, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (14074, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=backmansmryd&sort=letter&look=blocket');
INSERT INTO store_params VALUES (14074, 'image_extra', '-');
INSERT INTO store_params VALUES (1047, 'infopage_no_ads', 'http://www.tradecenter.se/dealer_privatelist.asp?sortorder=fabrication&dealerid=125&bgcolor=ff0000&rowcolor=ff5050');
INSERT INTO store_params VALUES (1047, 'image_extra', '-');
INSERT INTO store_params VALUES (1054, 'import_id', 'bilexpokristianstad');
INSERT INTO store_params VALUES (1054, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (1054, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=bilexpokristianstad&sort=letter&look=blocket');
INSERT INTO store_params VALUES (1054, 'image_extra', '-');
INSERT INTO store_params VALUES (1081, 'import_id', 'eddieskristianstad');
INSERT INTO store_params VALUES (1081, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (1081, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=eddieskristianstad&sort=letter&look=blocket');
INSERT INTO store_params VALUES (1081, 'image_extra', '-');
INSERT INTO store_params VALUES (14440, 'import_id', 'varbergsbildepot');
INSERT INTO store_params VALUES (14440, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (14440, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=varbergsbildepot&sort=letter&look=blocket
');
INSERT INTO store_params VALUES (14440, 'image_extra', '-');
INSERT INTO store_params VALUES (1099, 'import_id', 'klasen');
INSERT INTO store_params VALUES (1099, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (1099, 'infopage_no_ads', '-');
INSERT INTO store_params VALUES (1099, 'image_extra', '-');
INSERT INTO store_params VALUES (1109, 'import_id', 'sannarpsbilhstd');
INSERT INTO store_params VALUES (1109, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (1109, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=sannarpsbilhstd&sort=letter&look');
INSERT INTO store_params VALUES (1109, 'image_extra', '-');
INSERT INTO store_params VALUES (1142, 'import_id', 'lahbil');
INSERT INTO store_params VALUES (1142, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (1142, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=lahbil&sort=letter&look=blocket');
INSERT INTO store_params VALUES (1142, 'image_extra', '-');
INSERT INTO store_params VALUES (14258, 'import_id', 'martenbilab');
INSERT INTO store_params VALUES (14258, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (14258, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=martenbilab&sort=letter&look=blocket');
INSERT INTO store_params VALUES (14258, 'image_extra', '-');
INSERT INTO store_params VALUES (1177, 'import_id', 'ingemarsusabilar');
INSERT INTO store_params VALUES (1177, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (1177, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=ingemarsusabilar&sort=letter&look=blocket');
INSERT INTO store_params VALUES (1177, 'image_extra', '-');
INSERT INTO store_params VALUES (1181, 'import_id', 'molleforshandelshus');
INSERT INTO store_params VALUES (1181, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (1181, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=molleforshandelshus&sort=letter&look=blocket');
INSERT INTO store_params VALUES (1181, 'image_extra', '-');
INSERT INTO store_params VALUES (1183, 'import_id', 'vastboanderstorp');
INSERT INTO store_params VALUES (1183, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (1183, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=vastboanderstorp&look=blocket');
INSERT INTO store_params VALUES (1183, 'image_extra', '-');
INSERT INTO store_params VALUES (1222, 'infopage_no_ads', 'http://www.bildelsbasen.se/frames.asp?Action=Enskiltlager&kundnr=76');
INSERT INTO store_params VALUES (1222, 'image_extra', '-');
INSERT INTO store_params VALUES (1231, 'import_id', 'autocarvaxjo');
INSERT INTO store_params VALUES (1231, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (1231, 'infopage_no_ads', 'http://www.autocarimport.com/oversikt.asp');
INSERT INTO store_params VALUES (1231, 'image_extra', '-');
INSERT INTO store_params VALUES (1240, 'infopage_no_ads', 'http://www.mickesmotor.se/home/data/swe/iframe_embed.htm?embedurl=http://www.batborsen.com/bb/servlet/TraderSearch?id=39&perPage=15&color1=003399&color2=f0f0f0&rubrik=white');
INSERT INTO store_params VALUES (1240, 'image_extra', '-');
INSERT INTO store_params VALUES (1255, 'import_id', 'frojdsmotor');
INSERT INTO store_params VALUES (1255, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (1255, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=frojdsmotor&sort=letter&look=frojdsmotor');
INSERT INTO store_params VALUES (1255, 'image_extra', '-');
INSERT INTO store_params VALUES (14239, 'import_id', 'nilanderbilkarlskrona');
INSERT INTO store_params VALUES (14239, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (14239, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=nilanderbilkarlskrona&sort=letter&look=blocket');
INSERT INTO store_params VALUES (14239, 'image_extra', '-');
INSERT INTO store_params VALUES (1284, 'import_id', 'lundmark');
INSERT INTO store_params VALUES (1284, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (1284, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=lundmark&sort=letter&look=blocket');
INSERT INTO store_params VALUES (1284, 'image_extra', '-');
INSERT INTO store_params VALUES (1333, 'import_id', 'mellstorsas');
INSERT INTO store_params VALUES (1333, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (1333, 'infopage_no_ads', '-');
INSERT INTO store_params VALUES (1333, 'image_extra', '-');
INSERT INTO store_params VALUES (13243, 'import_id', 'agmotala');
INSERT INTO store_params VALUES (13243, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (13243, 'infopage_no_ads', '
');
INSERT INTO store_params VALUES (13243, 'image_extra', '-');
INSERT INTO store_params VALUES (1335, 'import_id', 'elmbrofarje');
INSERT INTO store_params VALUES (1335, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (1335, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=elmbrofarje&sort=letter&look=blocket');
INSERT INTO store_params VALUES (1335, 'image_extra', '-');
INSERT INTO store_params VALUES (1380, 'import_id', 'bekobils');
INSERT INTO store_params VALUES (1380, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (1380, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=bekobils&sort=letter&look=blocket');
INSERT INTO store_params VALUES (1380, 'image_extra', '-');
INSERT INTO store_params VALUES (14192, 'import_id', 'hageblad');
INSERT INTO store_params VALUES (14192, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (14192, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=hageblad&sort=letter&look=blocket');
INSERT INTO store_params VALUES (14192, 'image_extra', '-');
INSERT INTO store_params VALUES (14194, 'import_id', 'dintransportbilsthlm');
INSERT INTO store_params VALUES (14194, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (14194, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=dintransportbilsthlm&sort=letter&look=blocket');
INSERT INTO store_params VALUES (14194, 'image_extra', '-');
INSERT INTO store_params VALUES (1484, 'import_id', 'variantgbg');
INSERT INTO store_params VALUES (1484, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (1484, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=variantgbg&sort=letter&look=blocket');
INSERT INTO store_params VALUES (1484, 'image_extra', '-');
INSERT INTO store_params VALUES (1627, 'import_id', 'automangbg');
INSERT INTO store_params VALUES (1627, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (1627, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=automangbg&sort=letter&look=blocket');
INSERT INTO store_params VALUES (1627, 'image_extra', '-');
INSERT INTO store_params VALUES (1772, 'infopage_no_ads', 'http://www.backamohusvagnscenter.se/getUsedWagons.asp?TYPE=V');
INSERT INTO store_params VALUES (1772, 'image_extra', '-');
INSERT INTO store_params VALUES (1821, 'infopage_no_ads', 'http://www.batborsen.com/bb/servlet/TraderSearch?id=5002068&perPage=15&color1=003399&color2=f0f0f0&rubrik=white');
INSERT INTO store_params VALUES (1821, 'image_extra', '-');
INSERT INTO store_params VALUES (1855, 'import_id', 'carblazeboras');
INSERT INTO store_params VALUES (1855, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (1855, 'infopage_no_ads', 'http://www.carblaze.com/IE/bilar.htm');
INSERT INTO store_params VALUES (1855, 'image_extra', '-');
INSERT INTO store_params VALUES (1895, 'import_id', 'jarmojarvinen');
INSERT INTO store_params VALUES (1895, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (1895, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=jarmojarvinen&sort=letter&look=blocket');
INSERT INTO store_params VALUES (1895, 'image_extra', '-');
INSERT INTO store_params VALUES (1956, 'import_id', 'svenskaeurolidkoping');
INSERT INTO store_params VALUES (1956, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (1956, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=svenskaeurolidkoping&look=svenskaeurolidkoping&carsperpage=20&sort=letter');
INSERT INTO store_params VALUES (1956, 'image_extra', '-');
INSERT INTO store_params VALUES (1970, 'import_id', 'harenebil');
INSERT INTO store_params VALUES (1970, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (1970, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=harenebil&sort=letter&look=blocket');
INSERT INTO store_params VALUES (1970, 'image_extra', '-');
INSERT INTO store_params VALUES (1993, 'import_id', 'bilkompaniettravad');
INSERT INTO store_params VALUES (1993, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (1993, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=bilkompaniettravad&sort=letter&look=blocket');
INSERT INTO store_params VALUES (1993, 'image_extra', '-');
INSERT INTO store_params VALUES (2031, 'import_id', 'bilomarin');
INSERT INTO store_params VALUES (2031, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (2031, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=bilomarin&sort=letter&look=blocket');
INSERT INTO store_params VALUES (2031, 'image_extra', '-');
INSERT INTO store_params VALUES (2108, 'import_id', 'ostkustbilarohamn');
INSERT INTO store_params VALUES (2108, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (2108, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=ostkustbilarohamn&look=ostkustbilarohamn');
INSERT INTO store_params VALUES (2108, 'image_extra', '-');
INSERT INTO store_params VALUES (2135, 'import_id', 'vitalavetlanda');
INSERT INTO store_params VALUES (2135, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (2135, 'infopage_no_ads', 'http://www.vitala-bil.se/begagnat.htm');
INSERT INTO store_params VALUES (2135, 'image_extra', '-');
INSERT INTO store_params VALUES (2187, 'import_id', 'bilbyter');
INSERT INTO store_params VALUES (2187, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (2187, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=bilbyter&look=bilbyter2');
INSERT INTO store_params VALUES (2187, 'image_extra', '-');
INSERT INTO store_params VALUES (2195, 'infopage_no_ads', 'http://www.ostcamp.se/index.php?mainCatId=2&menuCatId=28&layoutId=1');
INSERT INTO store_params VALUES (2195, 'image_extra', '-');
INSERT INTO store_params VALUES (14120, 'import_id', 'lonestams');
INSERT INTO store_params VALUES (14120, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (14120, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=lonestams&sort=letter&look=blocket');
INSERT INTO store_params VALUES (14120, 'image_extra', '-');
INSERT INTO store_params VALUES (2242, 'import_id', 'bilhallenmjolby');
INSERT INTO store_params VALUES (2242, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (2242, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=bilhallenmjolby&sort=letter&look=blocket');
INSERT INTO store_params VALUES (2242, 'image_extra', '-');
INSERT INTO store_params VALUES (2293, 'import_id', 'peugeotnorrk');
INSERT INTO store_params VALUES (2293, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (2293, 'infopage_no_ads', '-');
INSERT INTO store_params VALUES (2293, 'image_extra', '-');
INSERT INTO store_params VALUES (2304, 'import_id', 'norrkbilfomed');
INSERT INTO store_params VALUES (2304, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (2304, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=norrkbilfomed&look=norrkbilfomed&carsperpage=20');
INSERT INTO store_params VALUES (2304, 'image_extra', '-');
INSERT INTO store_params VALUES (14291, 'infopage_no_ads', 'http://www.audiovideo.se');
INSERT INTO store_params VALUES (14291, 'image_extra', 'westerlunds_audiovideo.jpg');
INSERT INTO store_params VALUES (2395, 'import_id', 'classiceskilstuna');
INSERT INTO store_params VALUES (2395, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (2395, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=classiceskilstuna&sort=letter&look=blocket');
INSERT INTO store_params VALUES (2395, 'image_extra', '-');
INSERT INTO store_params VALUES (2430, 'import_id', 'bilgalleriankholm');
INSERT INTO store_params VALUES (2430, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (2430, 'infopage_no_ads', '-');
INSERT INTO store_params VALUES (2430, 'image_extra', '-');
INSERT INTO store_params VALUES (13272, 'import_id', 'landrinavesta');
INSERT INTO store_params VALUES (13272, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (13272, 'infopage_no_ads', '-');
INSERT INTO store_params VALUES (13272, 'image_extra', '-');
INSERT INTO store_params VALUES (2432, 'import_id', 'hallstenflen');
INSERT INTO store_params VALUES (2432, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (2432, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=hallstenflen&sort=letter&look=blocket');
INSERT INTO store_params VALUES (2432, 'image_extra', '-');
INSERT INTO store_params VALUES (2443, 'import_id', 'auktionshusetsodermanland');
INSERT INTO store_params VALUES (2443, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (2443, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=auktionshusetsodermanland&sort=letter&look=blocket');
INSERT INTO store_params VALUES (2443, 'image_extra', '-');
INSERT INTO store_params VALUES (2496, 'infopage_no_ads', 'http://www.kanonpris.com/husvagnspoolen/');
INSERT INTO store_params VALUES (2496, 'image_extra', '-');
INSERT INTO store_params VALUES (2515, 'import_id', 'hakans');
INSERT INTO store_params VALUES (2515, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (2515, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=hakans&sort=letter&look=blocket');
INSERT INTO store_params VALUES (2515, 'image_extra', '-');
INSERT INTO store_params VALUES (2524, 'import_id', 'olofericsonbilarvika');
INSERT INTO store_params VALUES (2524, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (2524, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?look=olofericsonbilarvika&seller=olofericsonbilarvika&carsperpage=50');
INSERT INTO store_params VALUES (2524, 'image_extra', '-');
INSERT INTO store_params VALUES (2552, 'import_id', 'cabbycenterkristinehamn');
INSERT INTO store_params VALUES (2552, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (2552, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=cabbycenterkristinehamn&sort=letter&look=blocket');
INSERT INTO store_params VALUES (2552, 'image_extra', '-');
INSERT INTO store_params VALUES (2559, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=nordichusbilarkristinehamn&look=nordichusbilarkristinehamn');
INSERT INTO store_params VALUES (2559, 'image_extra', '-');
INSERT INTO store_params VALUES (2563, 'import_id', 'husbilslandetkristinehamn');
INSERT INTO store_params VALUES (2563, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (2563, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=husbilslandetkristinehamn&look=blocket');
INSERT INTO store_params VALUES (2563, 'image_extra', '-');
INSERT INTO store_params VALUES (2566, 'import_id', 'leseffelbilfilipstad');
INSERT INTO store_params VALUES (2566, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (2566, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=leseffelbilfilipstad&sort=letter&look=blocket');
INSERT INTO store_params VALUES (2566, 'image_extra', '-');
INSERT INTO store_params VALUES (13765, 'import_id', 'customhyllinge');
INSERT INTO store_params VALUES (13765, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (13765, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=customhyllinge&sort=letter&look=blocket');
INSERT INTO store_params VALUES (13765, 'image_extra', '-');
INSERT INTO store_params VALUES (2572, 'import_id', 'gunnarl');
INSERT INTO store_params VALUES (2572, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (2572, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?look=gunnarl&seller=gunnarl');
INSERT INTO store_params VALUES (2572, 'image_extra', '-');
INSERT INTO store_params VALUES (2610, 'import_id', 'jorgenhamrenkumla');
INSERT INTO store_params VALUES (2610, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (2610, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=jorgenhamrenkumla&sort=letter&look=blocket');
INSERT INTO store_params VALUES (2610, 'image_extra', '-');
INSERT INTO store_params VALUES (2621, 'import_id', 'obab');
INSERT INTO store_params VALUES (2621, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (2621, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=obab&sort=letter&look=blocket');
INSERT INTO store_params VALUES (2621, 'image_extra', '-');
INSERT INTO store_params VALUES (2643, 'import_id', 'bilpunkteniorebro');
INSERT INTO store_params VALUES (2643, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (2643, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=bilpunkteniorebro&look=bilpunkteniorebro&carsperpage');
INSERT INTO store_params VALUES (2643, 'image_extra', '-');
INSERT INTO store_params VALUES (2650, 'import_id', 'jonsson');
INSERT INTO store_params VALUES (2650, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (2650, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=jonsson&sort=letter&look=blocket');
INSERT INTO store_params VALUES (2650, 'image_extra', '-');
INSERT INTO store_params VALUES (2652, 'import_id', 'mariebergbil');
INSERT INTO store_params VALUES (2652, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (2652, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?look=mariebergbil&seller=mariebergbil&sort=letter&carsperpage=200');
INSERT INTO store_params VALUES (2652, 'image_extra', '-');
INSERT INTO store_params VALUES (2662, 'import_id', 'tunnel');
INSERT INTO store_params VALUES (2662, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (2662, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=tunnel&sort=letter&look=blocket&carsperpage=100');
INSERT INTO store_params VALUES (2662, 'image_extra', '-');
INSERT INTO store_params VALUES (14153, 'import_id', 'bilpoolen');
INSERT INTO store_params VALUES (14153, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (14153, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=bilpoolen&sort=letter&look=blocket');
INSERT INTO store_params VALUES (14153, 'image_extra', '-');
INSERT INTO store_params VALUES (2677, 'import_id', 'bilmagasinet');
INSERT INTO store_params VALUES (2677, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (2677, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=bilmagasinet&sort=letter&look=blocket');
INSERT INTO store_params VALUES (2677, 'image_extra', '-');
INSERT INTO store_params VALUES (2688, 'import_id', 'samuelk');
INSERT INTO store_params VALUES (2688, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (2688, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=samuelk&sort=letter&look=blocket');
INSERT INTO store_params VALUES (2688, 'image_extra', '-');
INSERT INTO store_params VALUES (2742, 'import_id', 'kohmanvasteras');
INSERT INTO store_params VALUES (2742, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (2742, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=kohmanvasteras&sort=letter&look=blocket');
INSERT INTO store_params VALUES (2742, 'image_extra', '-');
INSERT INTO store_params VALUES (2769, 'import_id', 'arnobilmc');
INSERT INTO store_params VALUES (2769, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (2769, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=arnobilmc&sort=letter&look=blocket');
INSERT INTO store_params VALUES (2769, 'image_extra', '-');
INSERT INTO store_params VALUES (2785, 'import_id', 'andtfolks');
INSERT INTO store_params VALUES (2785, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (2785, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=andtfolks&sort=letter&look=blocket');
INSERT INTO store_params VALUES (2785, 'image_extra', '-');
INSERT INTO store_params VALUES (2794, 'infopage_no_ads', 'http://begmarknad.batnet.se/sok/annons_foretag.asp?fid=646');
INSERT INTO store_params VALUES (2794, 'image_extra', '-');
INSERT INTO store_params VALUES (2796, 'import_id', 'cassmanbil');
INSERT INTO store_params VALUES (2796, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (2796, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=cassmanbil&sort=letter&look=blocket');
INSERT INTO store_params VALUES (2796, 'image_extra', '-');
INSERT INTO store_params VALUES (2819, 'import_id', 'helinsbilcentrum');
INSERT INTO store_params VALUES (2819, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (2819, 'infopage_no_ads', 'http://www.helinsbil.se/begbilar.html');
INSERT INTO store_params VALUES (2819, 'image_extra', '-');
INSERT INTO store_params VALUES (13538, 'import_id', 'johansmcgbgmc');
INSERT INTO store_params VALUES (13538, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (13538, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=johansmcgbgmc&sort=letter&look=blocket');
INSERT INTO store_params VALUES (13538, 'image_extra', '-');
INSERT INTO store_params VALUES (2841, 'import_id', 'gperssonbil');
INSERT INTO store_params VALUES (2841, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (2841, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=gperssonbil&sort=letter&look=blocket');
INSERT INTO store_params VALUES (2841, 'image_extra', '-');
INSERT INTO store_params VALUES (2852, 'import_id', 'bilvalet');
INSERT INTO store_params VALUES (2852, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (2852, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=bilvalet&sort=letter&look=blocket');
INSERT INTO store_params VALUES (2852, 'image_extra', '-');
INSERT INTO store_params VALUES (2863, 'import_id', 'upplandsbilforum');
INSERT INTO store_params VALUES (2863, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (2863, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=upplandsbilforum&sort=letter&look=blocket&carsperpage=50%22');
INSERT INTO store_params VALUES (2863, 'image_extra', '-');
INSERT INTO store_params VALUES (2892, 'import_id', 'liljebladnorrtelje');
INSERT INTO store_params VALUES (2892, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (2892, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=liljebladnorrtelje&sort=letter&look=blocket');
INSERT INTO store_params VALUES (2892, 'image_extra', '-');
INSERT INTO store_params VALUES (2920, 'import_id', 'carnila');
INSERT INTO store_params VALUES (2920, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (2920, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=carnila&sort=letter&look=hlista_2002');
INSERT INTO store_params VALUES (2920, 'image_extra', '-');
INSERT INTO store_params VALUES (2925, 'import_id', 'hansperssonavesta');
INSERT INTO store_params VALUES (2925, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (2925, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=hansperssonavesta&sort=letter&look=blocket2002');
INSERT INTO store_params VALUES (2925, 'image_extra', '-');
INSERT INTO store_params VALUES (2953, 'import_id', 'rosellsborlange');
INSERT INTO store_params VALUES (2953, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (2953, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=rosellsborlange&sort=letter&look=blocket');
INSERT INTO store_params VALUES (2953, 'image_extra', '-');
INSERT INTO store_params VALUES (2972, 'infopage_no_ads', 'http://www.haraldsbatar.com/servlet/se.medialeden.market.haraldsbatar.ShowServlet');
INSERT INTO store_params VALUES (2972, 'image_extra', '-');
INSERT INTO store_params VALUES (2985, 'infopage_no_ads', 'http://www.skoter.se/scripts/begagnade_fordon.php?marke=&arsmodell=&modell=&region_af=302&fordonstyp=Sn%F6skoter&length=20&pris_grupp=
');
INSERT INTO store_params VALUES (2985, 'image_extra', '-');
INSERT INTO store_params VALUES (3002, 'import_id', 'rolfgpersson');
INSERT INTO store_params VALUES (3002, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (3002, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=rolfgpersson&look=rolfgpersson');
INSERT INTO store_params VALUES (3002, 'image_extra', '-');
INSERT INTO store_params VALUES (3051, 'import_id', 'autogavle');
INSERT INTO store_params VALUES (3051, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (3051, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=autogavle&sort=letter&look=blocket');
INSERT INTO store_params VALUES (3051, 'image_extra', '-');
INSERT INTO store_params VALUES (3056, 'import_id', 'kvalitetsbilargavle');
INSERT INTO store_params VALUES (3056, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (3056, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=kvalitetsbilargavle&sort=letter&look=blocket');
INSERT INTO store_params VALUES (3056, 'image_extra', '-');
INSERT INTO store_params VALUES (13693, 'import_id', 'bilmetrogavle');
INSERT INTO store_params VALUES (13693, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (13693, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=bilmetrogavle&sort=letter&look=blocket');
INSERT INTO store_params VALUES (13693, 'image_extra', '-');
INSERT INTO store_params VALUES (13794, 'import_id', 'peugeotuvalla');
INSERT INTO store_params VALUES (13794, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (13794, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=peugeotuvalla&sort=letter&look=blocket');
INSERT INTO store_params VALUES (13794, 'image_extra', '-');
INSERT INTO store_params VALUES (3067, 'import_id', 'bildaxgavle');
INSERT INTO store_params VALUES (3067, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (3067, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=bildaxgavle&look=blocket');
INSERT INTO store_params VALUES (3067, 'image_extra', '-');
INSERT INTO store_params VALUES (3079, 'import_id', 'anetornbilsandviken');
INSERT INTO store_params VALUES (3079, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (3079, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=anetornbilsandviken&sort=letter&look=blocket');
INSERT INTO store_params VALUES (3079, 'image_extra', '-');
INSERT INTO store_params VALUES (3082, 'import_id', 'bilhornansandviken');
INSERT INTO store_params VALUES (3082, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (3082, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=bilhornansandviken&sort=letter&look=blocket');
INSERT INTO store_params VALUES (3082, 'image_extra', '-');
INSERT INTO store_params VALUES (3099, 'import_id', 'husvagnscenterivalbo');
INSERT INTO store_params VALUES (3099, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (3099, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=husvagnscenterivalbo&sort=letter&look=blocket');
INSERT INTO store_params VALUES (3099, 'image_extra', '-');
INSERT INTO store_params VALUES (3110, 'import_id', 'nibobollnas');
INSERT INTO store_params VALUES (3110, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (3110, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=nibobollnas&sort=letter&look=blocket');
INSERT INTO store_params VALUES (3110, 'image_extra', '-');
INSERT INTO store_params VALUES (3170, 'import_id', 'norskyljusdal');
INSERT INTO store_params VALUES (3170, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (3170, 'infopage_no_ads', 'http://www.noren-skyttner.se/oldcars.asp');
INSERT INTO store_params VALUES (3170, 'image_extra', '-');
INSERT INTO store_params VALUES (3175, 'import_id', 'sellmansbil');
INSERT INTO store_params VALUES (3175, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (3175, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=sellmansbil&sort=letter&look=blocket');
INSERT INTO store_params VALUES (3175, 'image_extra', '-');
INSERT INTO store_params VALUES (13896, 'import_id', 'sellmansbilhudik');
INSERT INTO store_params VALUES (13896, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (13896, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=sellmansbilhudik&sort=letter&look=blocket');
INSERT INTO store_params VALUES (13896, 'image_extra', '-');
INSERT INTO store_params VALUES (14461, 'import_id', 'ivarshoting');
INSERT INTO store_params VALUES (14461, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (14461, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=ivarshoting&sort=letter&look=blocket');
INSERT INTO store_params VALUES (14461, 'image_extra', '-');
INSERT INTO store_params VALUES (3225, 'import_id', 'bilhusetstromsund');
INSERT INTO store_params VALUES (3225, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (3225, 'infopage_no_ads', 'http://www.bilweb.se/VehicleSearch/Default.cfm?Page=2&Section=SecUsedCar&StartRow=1&VehicleTypeID=2&StateID=&MakeID=&ModelID=&BodyID=&CategoryID=&Option=&PriceFrom=0&PriceTo=100000000&YearFrom=0&YearTo=2003&MileageFrom=0&MileageTo=100000000&EngineFrom=&EngineTo=&LatestCarsOnOff=&LatestCars=1&FreeText=&OrderBy=MakeName%2C%20ModelSortOrder%2C%20ModelName%2C%20Year&ContactID=203748&ViewType=table');
INSERT INTO store_params VALUES (3225, 'image_extra', '-');
INSERT INTO store_params VALUES (3270, 'import_id', 'bilinvestsundsvall');
INSERT INTO store_params VALUES (3270, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (3270, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=bilinvestsundsvall&sort=letter&look=blocket');
INSERT INTO store_params VALUES (3270, 'image_extra', '-');
INSERT INTO store_params VALUES (3277, 'infopage_no_ads', 'http://www.bilomobil.se/index.php?begagnat');
INSERT INTO store_params VALUES (3277, 'image_extra', '-');
INSERT INTO store_params VALUES (3279, 'import_id', 'sorbergehusvagnar');
INSERT INTO store_params VALUES (3279, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (3279, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=sorbergehusvagnar&sort=letter&look=blocket');
INSERT INTO store_params VALUES (3279, 'image_extra', '-');
INSERT INTO store_params VALUES (3280, 'infopage_no_ads', '-');
INSERT INTO store_params VALUES (3280, 'image_extra', '-');
INSERT INTO store_params VALUES (3294, 'import_id', 'automobile');
INSERT INTO store_params VALUES (3294, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (3294, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=automobile&sort=letter&look=blocket');
INSERT INTO store_params VALUES (3294, 'image_extra', '-');
INSERT INTO store_params VALUES (3346, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=bilhallenovik&sort=letter&look=blocket');
INSERT INTO store_params VALUES (3346, 'image_extra', '-');
INSERT INTO store_params VALUES (3351, 'import_id', 'magnusbil');
INSERT INTO store_params VALUES (3351, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (3351, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=magnusbil&sort=letter&look=blocket');
INSERT INTO store_params VALUES (3351, 'image_extra', '-');
INSERT INTO store_params VALUES (3365, 'import_id', 'umeavan');
INSERT INTO store_params VALUES (3365, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (3365, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=umeavan&sort=letter&look=blocket');
INSERT INTO store_params VALUES (3365, 'image_extra', '-');
INSERT INTO store_params VALUES (3381, 'import_id', 'sturesonumea');
INSERT INTO store_params VALUES (3381, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (3381, 'infopage_no_ads', 'http://www.bonetweb.com/jetski/lager/');
INSERT INTO store_params VALUES (3381, 'image_extra', '-');
INSERT INTO store_params VALUES (3387, 'import_id', 'bergnersbil');
INSERT INTO store_params VALUES (3387, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (3387, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=bergnersbil&look=bergnersbil');
INSERT INTO store_params VALUES (3387, 'image_extra', '-');
INSERT INTO store_params VALUES (3414, 'import_id', 'autobla');
INSERT INTO store_params VALUES (3414, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (3414, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=autobla&sort=letter&look=blocket');
INSERT INTO store_params VALUES (3414, 'image_extra', '-');
INSERT INTO store_params VALUES (3452, 'import_id', 'gustafssonsbil');
INSERT INTO store_params VALUES (3452, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (3452, 'infopage_no_ads', 'http://
');
INSERT INTO store_params VALUES (3452, 'image_extra', '-');
INSERT INTO store_params VALUES (3455, 'import_id', 'hellgrens');
INSERT INTO store_params VALUES (3455, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (3455, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=hellgrens&sort=letter&look=blocket');
INSERT INTO store_params VALUES (3455, 'image_extra', '-');
INSERT INTO store_params VALUES (3493, 'import_id', 'sunderbybil');
INSERT INTO store_params VALUES (3493, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (3493, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=sunderbybil&look=blocket&carsperpage=30');
INSERT INTO store_params VALUES (3493, 'image_extra', '-');
INSERT INTO store_params VALUES (12652, 'import_id', 'mcmotorcityhbg');
INSERT INTO store_params VALUES (12652, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (12652, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=mcmotorcityhbg&sort=letter&look=blocket');
INSERT INTO store_params VALUES (12652, 'image_extra', '-');
INSERT INTO store_params VALUES (13480, 'import_id', 'lagprisbilarisvedala');
INSERT INTO store_params VALUES (13480, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (13480, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=lagprisbilarisvedala&sort=letter&look=blocket');
INSERT INTO store_params VALUES (13480, 'image_extra', '-');
INSERT INTO store_params VALUES (3553, 'import_id', 'bilochmotortkkrona');
INSERT INTO store_params VALUES (3553, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (3553, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=bilochmotortkkrona&sort=letter&look=blocket');
INSERT INTO store_params VALUES (3553, 'image_extra', '-');
INSERT INTO store_params VALUES (3565, 'infopage_no_ads', 'http://skotertest.skycom.se/scripts/begagnade_fordon.php?marke=&arsmodell=&modell=&region_af=313&fordonstyp=Sn%F6skoter&length=20&pris_grupp=');
INSERT INTO store_params VALUES (3565, 'image_extra', '-');
INSERT INTO store_params VALUES (3590, 'import_id', 'hedmansmotor');
INSERT INTO store_params VALUES (3590, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (3590, 'infopage_no_ads', 'http://www.tradecenter.se/dealer_privatelist.asp?machineryid=8&sortorder=fabrication&dealerid=64');
INSERT INTO store_params VALUES (3590, 'image_extra', '-');
INSERT INTO store_params VALUES (3597, 'infopage_no_ads', 'http://www.kalixmaskiner.se/butik/default.asp?id=1');
INSERT INTO store_params VALUES (3597, 'image_extra', '-');
INSERT INTO store_params VALUES (3606, 'infopage_no_ads', 'http://www.tradecenter.se/dealer_privatelist.asp?sortorder=fabrication&dealerid=49&font=Verdana');
INSERT INTO store_params VALUES (3606, 'image_extra', '-');
INSERT INTO store_params VALUES (3621, 'infopage_no_ads', 'http://skotertest.skycom.se/scripts/begagnade_fordon.php?marke=&arsmodell=&modell=&region_af=288&fordonstyp=Sn%F6skoter&length=20&pris_grupp=');
INSERT INTO store_params VALUES (3621, 'image_extra', '-');
INSERT INTO store_params VALUES (12642, 'import_id', 'unikbilorebro');
INSERT INTO store_params VALUES (12642, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (12642, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=unikbilorebro&sort=letter&look=blocket');
INSERT INTO store_params VALUES (12642, 'image_extra', '-');
INSERT INTO store_params VALUES (3633, 'infopage_no_ads', 'http://www.tradecenter.se/dealer_privatelist.asp?sortorder=fabrication&dealerid=155');
INSERT INTO store_params VALUES (3633, 'image_extra', '-');
INSERT INTO store_params VALUES (3751, 'import_id', 'guzzicenterjarfalla');
INSERT INTO store_params VALUES (3751, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (3751, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=guzzicenterjarfalla&sort=letter&look=blocket');
INSERT INTO store_params VALUES (3751, 'image_extra', '-');
INSERT INTO store_params VALUES (3752, 'import_id', 'deltafbgmc');
INSERT INTO store_params VALUES (3752, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (3752, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=deltafbgmc&sort=letter&look=blocket');
INSERT INTO store_params VALUES (3752, 'image_extra', '-');
INSERT INTO store_params VALUES (3765, 'infopage_no_ads', 'http://www.bytmc.com/select_mc.cgi?seller=hermansvaramc&sort=letter&look=hermansvaramc');
INSERT INTO store_params VALUES (3765, 'image_extra', '-');
INSERT INTO store_params VALUES (3779, 'import_id', 'supergbgmc');
INSERT INTO store_params VALUES (3779, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (3779, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=supergbgmc&sort=letter&look=blocket');
INSERT INTO store_params VALUES (3779, 'image_extra', '-');
INSERT INTO store_params VALUES (3843, 'import_id', 'wingecenterluleamc');
INSERT INTO store_params VALUES (3843, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (3843, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=wingecenterluleamc&sort=letter&look=blocket');
INSERT INTO store_params VALUES (3843, 'image_extra', '-');
INSERT INTO store_params VALUES (3849, 'import_id', 'sulasuppsalamc');
INSERT INTO store_params VALUES (3849, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (3849, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=sulasuppsalamc&sort=letter&look=blocket');
INSERT INTO store_params VALUES (3849, 'image_extra', '-');
INSERT INTO store_params VALUES (3851, 'import_id', 'nilssonsmotorfagerhultmc');
INSERT INTO store_params VALUES (3851, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (3851, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=nilssonsmotorfagerhultmc&sort=letter&look=blocket');
INSERT INTO store_params VALUES (3851, 'image_extra', '-');
INSERT INTO store_params VALUES (3853, 'import_id', 'movapvadstenamc');
INSERT INTO store_params VALUES (3853, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (3853, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=movapvadstenamc&sort=letter&look=blocket');
INSERT INTO store_params VALUES (3853, 'image_extra', '-');
INSERT INTO store_params VALUES (3877, 'infopage_no_ads', 'http://www.fastbike.se/motorcyklar.html');
INSERT INTO store_params VALUES (3877, 'image_extra', '-');
INSERT INTO store_params VALUES (3879, 'import_id', 'fottasmcludvikamc');
INSERT INTO store_params VALUES (3879, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (3879, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=fottasmcludvikamc&sort=letter&look=blocket');
INSERT INTO store_params VALUES (3879, 'image_extra', '-');
INSERT INTO store_params VALUES (13968, 'import_id', 'smygebilochfritid');
INSERT INTO store_params VALUES (13968, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (13968, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=smygebilochfritid&sort=letter&look=blocket');
INSERT INTO store_params VALUES (13968, 'image_extra', '-');
INSERT INTO store_params VALUES (3894, 'infopage_no_ads', 'http://193.180.23.178/select_mc.cgi?seller=jarvsocykelomotormc&look=jarvsocy
kelomotormc&milesfrom=1');
INSERT INTO store_params VALUES (3894, 'image_extra', '-');
INSERT INTO store_params VALUES (3897, 'import_id', 'lindstromsmotormalmomc');
INSERT INTO store_params VALUES (3897, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (3897, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=lindstromsmotormalmomc&sort=letter&look=blocket');
INSERT INTO store_params VALUES (3897, 'image_extra', '-');
INSERT INTO store_params VALUES (3923, 'import_id', 'autoimpnorrkmc');
INSERT INTO store_params VALUES (3923, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (3923, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=autoimpnorrkmc&sort=letter&look=blocket');
INSERT INTO store_params VALUES (3923, 'image_extra', '-');
INSERT INTO store_params VALUES (3933, 'infopage_no_ads', 'http://www.speed-mc.se/motorcyklar.php?bfilter=0&u=1&PHPSESSID=5ab243343ec5ab837ce8ebbbccd8327a');
INSERT INTO store_params VALUES (3933, 'image_extra', '-');
INSERT INTO store_params VALUES (3935, 'import_id', 'stibixborasmc');
INSERT INTO store_params VALUES (3935, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (3935, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=stibixborasmc&sort=letter&look=b
locket');
INSERT INTO store_params VALUES (3935, 'image_extra', '-');
INSERT INTO store_params VALUES (3941, 'import_id', 'westbikemellerudmc');
INSERT INTO store_params VALUES (3941, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (3941, 'infopage_no_ads', 'http://www.bytmc.com/select_mc.cgi?seller=westbikemellerudmc&sort=letter&look=westbikemellerudmc');
INSERT INTO store_params VALUES (3941, 'image_extra', '-');
INSERT INTO store_params VALUES (3947, 'import_id', 'hallsbilarsandviken');
INSERT INTO store_params VALUES (3947, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (3947, 'infopage_no_ads', 'http://flnet.bilweb.se/VehicleSearch/Default.cfm?Page=2&Section=SecUsedCar&StartRow=1&VehicleTypeID=2&StateID=&MakeID=&ModelID=&BodyID=&CategoryID=&Option=&PriceFrom=0&PriceTo=100000000&YearFrom=0&YearTo=2001&MileageFrom=0&MileageTo=100000000&EngineFrom=&EngineTo=&LatestCarsOnOff=&LatestCars=1&FreeText=&OrderBy=Price%2C%20MakeName%2C%20ModelSortOrder%2C%20ModelName%2C%20Year&ContactID=58612&ViewType=table');
INSERT INTO store_params VALUES (3947, 'image_extra', '-');
INSERT INTO store_params VALUES (3950, 'import_id', 'nadior');
INSERT INTO store_params VALUES (3950, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (3950, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=nadior&sort=letter&look=blocket');
INSERT INTO store_params VALUES (3950, 'image_extra', '-');
INSERT INTO store_params VALUES (3951, 'import_id', 'bilformedlmos');
INSERT INTO store_params VALUES (3951, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (3951, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=bilformedlmos&sort=letter&look=blocket');
INSERT INTO store_params VALUES (3951, 'image_extra', '-');
INSERT INTO store_params VALUES (3952, 'import_id', 'ucars');
INSERT INTO store_params VALUES (3952, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (3952, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=ucars&sort=letter&look=blocket');
INSERT INTO store_params VALUES (3952, 'image_extra', '-');
INSERT INTO store_params VALUES (14235, 'import_id', 'gibilhbg');
INSERT INTO store_params VALUES (14235, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (14235, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=gibilhbg&sort=letter&look=blocket');
INSERT INTO store_params VALUES (14235, 'image_extra', '-');
INSERT INTO store_params VALUES (3962, 'import_id', 'codatfritsla');
INSERT INTO store_params VALUES (3962, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (3962, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=codatfritsla&sort=letter&look=blocket');
INSERT INTO store_params VALUES (3962, 'image_extra', '-');
INSERT INTO store_params VALUES (3968, 'import_id', 'danielshbg');
INSERT INTO store_params VALUES (3968, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (3968, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=danielshbg&sort=letter&look=blocket');
INSERT INTO store_params VALUES (3968, 'image_extra', '-');
INSERT INTO store_params VALUES (3969, 'infopage_no_ads', '-');
INSERT INTO store_params VALUES (3969, 'image_extra', '-');
INSERT INTO store_params VALUES (14463, 'import_id', 'goingebilhlm');
INSERT INTO store_params VALUES (14463, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (14463, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=goingebilhlm&sort=letter&look=blocket');
INSERT INTO store_params VALUES (14463, 'image_extra', '-');
INSERT INTO store_params VALUES (3974, 'import_id', 'granuddenlulea');
INSERT INTO store_params VALUES (3974, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (3974, 'infopage_no_ads', 'http://
');
INSERT INTO store_params VALUES (3974, 'image_extra', '-');
INSERT INTO store_params VALUES (13284, 'import_id', 'autoclassica');
INSERT INTO store_params VALUES (13284, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (13284, 'infopage_no_ads', '-
');
INSERT INTO store_params VALUES (13284, 'image_extra', '-');
INSERT INTO store_params VALUES (3975, 'import_id', 'bilaffarenlulea');
INSERT INTO store_params VALUES (3975, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (3975, 'infopage_no_ads', 'http://www.bilaffarenlulea.com/forsaljning.htm');
INSERT INTO store_params VALUES (3975, 'image_extra', '-');
INSERT INTO store_params VALUES (3980, 'import_id', 'mikaelsbilar');
INSERT INTO store_params VALUES (3980, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (3980, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=mikaelsbilar&sort=letter&look=blocket');
INSERT INTO store_params VALUES (3980, 'image_extra', '-');
INSERT INTO store_params VALUES (3981, 'import_id', 'carbura');
INSERT INTO store_params VALUES (3981, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (3981, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=carbura&sort=letter&look=blocket');
INSERT INTO store_params VALUES (3981, 'image_extra', '-');
INSERT INTO store_params VALUES (3982, 'import_id', 'lainvest');
INSERT INTO store_params VALUES (3982, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (3982, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=lainvest&sort=letter&look=blocket');
INSERT INTO store_params VALUES (3982, 'image_extra', '-');
INSERT INTO store_params VALUES (3986, 'import_id', 'kaakinen');
INSERT INTO store_params VALUES (3986, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (3986, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=kaakinen&sort=letter&look=blocket');
INSERT INTO store_params VALUES (3986, 'image_extra', '-');
INSERT INTO store_params VALUES (3993, 'infopage_no_ads', 'http://www.bilomc.nu');
INSERT INTO store_params VALUES (3993, 'image_extra', '-');
INSERT INTO store_params VALUES (3994, 'import_id', 'zackrissonshedemora');
INSERT INTO store_params VALUES (3994, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (3994, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=zackrissonshedemora&sort=letter&look=blocket');
INSERT INTO store_params VALUES (3994, 'image_extra', '-');
INSERT INTO store_params VALUES (3995, 'import_id', 'mnuppsala');
INSERT INTO store_params VALUES (3995, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (3995, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=mnuppsala&sort=letter&look=blocket&carsperpage=50');
INSERT INTO store_params VALUES (3995, 'image_extra', '-');
INSERT INTO store_params VALUES (14443, 'import_id', 'rumabilorebro');
INSERT INTO store_params VALUES (14443, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (14443, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=rumabilorebro&sort=letter&look=blocket');
INSERT INTO store_params VALUES (14443, 'image_extra', '-');
INSERT INTO store_params VALUES (14444, 'infopage_no_ads', '-');
INSERT INTO store_params VALUES (14444, 'image_extra', '-');
INSERT INTO store_params VALUES (4004, 'import_id', 'norravagenbil');
INSERT INTO store_params VALUES (4004, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (4004, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=norravagenbil&sort=letter&look=blocket');
INSERT INTO store_params VALUES (4004, 'image_extra', '-');
INSERT INTO store_params VALUES (13692, 'import_id', 'bilmetrobollnas');
INSERT INTO store_params VALUES (13692, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (13692, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=bilmetrobollnas&sort=letter&look=blocket');
INSERT INTO store_params VALUES (13692, 'image_extra', '-');
INSERT INTO store_params VALUES (4003, 'import_id', 'rideiumea');
INSERT INTO store_params VALUES (4003, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (4003, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=rideiumea&sort=letter&look=blocket');
INSERT INTO store_params VALUES (4003, 'image_extra', '-');
INSERT INTO store_params VALUES (4006, 'infopage_no_ads', 'http://www.haraldsbatar.com/servlet/se.medialeden.market.haraldsbatar.ShowServlet');
INSERT INTO store_params VALUES (4006, 'image_extra', '-');
INSERT INTO store_params VALUES (4007, 'import_id', 'lingvallsmotorgavlemc');
INSERT INTO store_params VALUES (4007, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (4007, 'infopage_no_ads', 'http://bytmc.bytbil.com/select_mc.cgi?seller=lingvallsmotorgavlemc&sort=letter&look=blocket

');
INSERT INTO store_params VALUES (4007, 'image_extra', '-');
INSERT INTO store_params VALUES (4011, 'import_id', 'bromsplansbilorebro');
INSERT INTO store_params VALUES (4011, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (4011, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=bromsplansbilorebro&look=bromsplansbilorebro');
INSERT INTO store_params VALUES (4011, 'image_extra', '-');
INSERT INTO store_params VALUES (4014, 'import_id', 'cityautomobileiorebro');
INSERT INTO store_params VALUES (4014, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (4014, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=cityautomobileiorebro&sort=letter&look=hlista_2002');
INSERT INTO store_params VALUES (4014, 'image_extra', '-');
INSERT INTO store_params VALUES (4015, 'import_id', 'nykopbilmark');
INSERT INTO store_params VALUES (4015, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (4015, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=nykopbilmark&look=nykopbilmark');
INSERT INTO store_params VALUES (4015, 'image_extra', '-');
INSERT INTO store_params VALUES (4020, 'import_id', 'flyingebil');
INSERT INTO store_params VALUES (4020, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (4020, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=flyingebil&sort=letter&look=blocket');
INSERT INTO store_params VALUES (4020, 'image_extra', '-');
INSERT INTO store_params VALUES (4022, 'import_id', 'enneforsbilorebro');
INSERT INTO store_params VALUES (4022, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (4022, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=enneforsbilorebro&sort=letter&look=blocket');
INSERT INTO store_params VALUES (4022, 'image_extra', '-');
INSERT INTO store_params VALUES (4023, 'import_id', 'bilexpolomma');
INSERT INTO store_params VALUES (4023, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (4023, 'infopage_no_ads', '-');
INSERT INTO store_params VALUES (4023, 'image_extra', '-');
INSERT INTO store_params VALUES (4024, 'import_id', 'bilcitystaffanstorp');
INSERT INTO store_params VALUES (4024, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (4024, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=bilcitystaffanstorp&look=blocket');
INSERT INTO store_params VALUES (4024, 'image_extra', '-');
INSERT INTO store_params VALUES (4025, 'infopage_no_ads', '-');
INSERT INTO store_params VALUES (4025, 'image_extra', '-');
INSERT INTO store_params VALUES (4026, 'import_id', 'msaarlov');
INSERT INTO store_params VALUES (4026, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (4026, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=msaarlov&sort=letter&look=blocket');
INSERT INTO store_params VALUES (4026, 'image_extra', '-');
INSERT INTO store_params VALUES (13635, 'import_id', 'helmiasunne');
INSERT INTO store_params VALUES (13635, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (13635, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=helmiasunne&sort=letter&look=blocket');
INSERT INTO store_params VALUES (13635, 'image_extra', '-');
INSERT INTO store_params VALUES (4028, 'import_id', 'lundavagenmalmo');
INSERT INTO store_params VALUES (4028, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (4028, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=lundavagenmalmo&sort=letter&look=hlista_2002');
INSERT INTO store_params VALUES (4028, 'image_extra', '-');
INSERT INTO store_params VALUES (4029, 'import_id', 'kaglingebil');
INSERT INTO store_params VALUES (4029, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (4029, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?look=blocket&seller=kaglingebil&carsperpage=30');
INSERT INTO store_params VALUES (4029, 'image_extra', '-');
INSERT INTO store_params VALUES (14129, 'infopage_no_ads', 'http://www.ncm-ss.se/beg_skotrar/beg_skotrar.htm');
INSERT INTO store_params VALUES (14129, 'image_extra', '-');
INSERT INTO store_params VALUES (4033, 'import_id', 'mcbilskivarp');
INSERT INTO store_params VALUES (4033, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (4033, 'infopage_no_ads', '-');
INSERT INTO store_params VALUES (4033, 'image_extra', '-');
INSERT INTO store_params VALUES (4034, 'import_id', 'barabilarmalmo');
INSERT INTO store_params VALUES (4034, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (4034, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=barabilarmalmo&sort=letter&look=blocket');
INSERT INTO store_params VALUES (4034, 'image_extra', '-');
INSERT INTO store_params VALUES (4035, 'import_id', 'kcbt');
INSERT INTO store_params VALUES (4035, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (4035, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=kcbt&sort=letter&look=blocket');
INSERT INTO store_params VALUES (4035, 'image_extra', '-');
INSERT INTO store_params VALUES (13499, 'import_id', 'peugeotkista');
INSERT INTO store_params VALUES (13499, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (13499, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=peugeotkista&sort=letter&look=blocket');
INSERT INTO store_params VALUES (13499, 'image_extra', '-');
INSERT INTO store_params VALUES (4037, 'import_id', 'tomelillabil');
INSERT INTO store_params VALUES (4037, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (4037, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=tomelillabil&sort=letter&look=hlista_2002');
INSERT INTO store_params VALUES (4037, 'image_extra', '-');
INSERT INTO store_params VALUES (4038, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=autocsolvesborg&sort=letter&look=blocket');
INSERT INTO store_params VALUES (4038, 'image_extra', '-');
INSERT INTO store_params VALUES (4049, 'import_id', 'transpcent');
INSERT INTO store_params VALUES (4049, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (4049, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=transpcent&sort=letter&look=blocket');
INSERT INTO store_params VALUES (4049, 'image_extra', '-');
INSERT INTO store_params VALUES (4053, 'import_id', 'bikeport');
INSERT INTO store_params VALUES (4053, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (4053, 'infopage_no_ads', 'http://www.bikeport.se/mcavd/~elt_lager.htm');
INSERT INTO store_params VALUES (4053, 'image_extra', '-');
INSERT INTO store_params VALUES (4054, 'import_id', 'skanebilkl');
INSERT INTO store_params VALUES (4054, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (4054, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=skanebilkl&sort=letter&look=blocket');
INSERT INTO store_params VALUES (4054, 'image_extra', '-');
INSERT INTO store_params VALUES (4059, 'infopage_no_ads', 'http://www.gefleyachts.nu/motorbatar.htm');
INSERT INTO store_params VALUES (4059, 'image_extra', '-');
INSERT INTO store_params VALUES (4060, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=firstinvestvaddo&sort=letter&look=blocket');
INSERT INTO store_params VALUES (4060, 'image_extra', '-');
INSERT INTO store_params VALUES (4065, 'import_id', 'autocarkalmar');
INSERT INTO store_params VALUES (4065, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (4065, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=autocarkalmar&sort=letter&look=blocket');
INSERT INTO store_params VALUES (4065, 'image_extra', '-');
INSERT INTO store_params VALUES (4064, 'import_id', 'ubbo');
INSERT INTO store_params VALUES (4064, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (4064, 'infopage_no_ads', 'http://www.ubbo.se');
INSERT INTO store_params VALUES (4064, 'image_extra', '-');
INSERT INTO store_params VALUES (4066, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?look=blocket&seller=slottsmalmo');
INSERT INTO store_params VALUES (4066, 'image_extra', '-');
INSERT INTO store_params VALUES (4069, 'import_id', 'rehnstromhb');
INSERT INTO store_params VALUES (4069, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (4069, 'infopage_no_ads', 'http://www.rehnstromsbil.se/bilar.asp');
INSERT INTO store_params VALUES (4069, 'image_extra', '-');
INSERT INTO store_params VALUES (14108, 'import_id', 'halabson');
INSERT INTO store_params VALUES (14108, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (14108, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=halabson&sort=letter&look=blocket');
INSERT INTO store_params VALUES (14108, 'image_extra', '-');
INSERT INTO store_params VALUES (4072, 'import_id', 'biltvatusen');
INSERT INTO store_params VALUES (4072, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (4072, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=biltvatusen&sort=letter&look=blocket');
INSERT INTO store_params VALUES (4072, 'image_extra', '-');
INSERT INTO store_params VALUES (4077, 'import_id', 'alinestockholm');
INSERT INTO store_params VALUES (4077, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (4077, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=alinestockholm&sort=letter&look=blocket');
INSERT INTO store_params VALUES (4077, 'image_extra', '-');
INSERT INTO store_params VALUES (4078, 'infopage_no_ads', 'http://www.alviks-mc.com/fordon/mc/beg/mc_beg.html');
INSERT INTO store_params VALUES (4078, 'image_extra', '-');
INSERT INTO store_params VALUES (4079, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=esmotor&look=blocket');
INSERT INTO store_params VALUES (4079, 'image_extra', '-');
INSERT INTO store_params VALUES (4081, 'import_id', 'kryddstigensbilcentrum');
INSERT INTO store_params VALUES (4081, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (4081, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=kryddstigensbilcentrum&sort=letter&look=blocket');
INSERT INTO store_params VALUES (4081, 'image_extra', '-');
INSERT INTO store_params VALUES (4083, 'import_id', 'odds');
INSERT INTO store_params VALUES (4083, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (4083, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=odds&sort=letter&look=blocket&carsperpage=100');
INSERT INTO store_params VALUES (4083, 'image_extra', '-');
INSERT INTO store_params VALUES (4088, 'infopage_no_ads', 'http://www.hojtorget.nu/usedbikes.asp?f=sok&sok=smr&handlare=141&layout=1&type=1,2,3,4,5,6&show=beg');
INSERT INTO store_params VALUES (4088, 'image_extra', '-');
INSERT INTO store_params VALUES (4103, 'import_id', 'bengtorkelljunga');
INSERT INTO store_params VALUES (4103, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (4103, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=bengtorkelljunga&sort=letter&look=blocket');
INSERT INTO store_params VALUES (4103, 'image_extra', '-');
INSERT INTO store_params VALUES (4107, 'infopage_no_ads', 'http://www.tradecenter.se/dealer_privatelist.asp?machineryid=8&sortorder=fabrication&dealerid=32%20');
INSERT INTO store_params VALUES (4107, 'image_extra', '-');
INSERT INTO store_params VALUES (4110, 'import_id', 'nestorsbilvallentuna');
INSERT INTO store_params VALUES (4110, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (4110, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=nestorsbilvallentuna&sort=letter&look=blocket');
INSERT INTO store_params VALUES (4110, 'image_extra', '-');
INSERT INTO store_params VALUES (4111, 'import_id', 'solbiltorg');
INSERT INTO store_params VALUES (4111, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (4111, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=solbiltorg&look=blocket');
INSERT INTO store_params VALUES (4111, 'image_extra', '-');
INSERT INTO store_params VALUES (4116, 'infopage_no_ads', 'http://www.tradecenter.se/dealer_privatelist.asp?dealerid=168&font=verdana&');
INSERT INTO store_params VALUES (4116, 'image_extra', '-');
INSERT INTO store_params VALUES (4118, 'import_id', 'hojgaragetkarlstadmc');
INSERT INTO store_params VALUES (4118, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (4118, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=hojgaragetkarlstadmc&sort=letter&look=blocket');
INSERT INTO store_params VALUES (4118, 'image_extra', '-');
INSERT INTO store_params VALUES (4119, 'import_id', 'lannahandelshus');
INSERT INTO store_params VALUES (4119, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (4119, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=lannahandelshus&sort=letter&look=blocket');
INSERT INTO store_params VALUES (4119, 'image_extra', '-');
INSERT INTO store_params VALUES (4120, 'import_id', 'nilshall');
INSERT INTO store_params VALUES (4120, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (4120, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=nilshall&sort=letter&look=blocket');
INSERT INTO store_params VALUES (4120, 'image_extra', '-');
INSERT INTO store_params VALUES (12799, 'import_id', 'bilmixskelleftea');
INSERT INTO store_params VALUES (12799, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (12799, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=bilmixskelleftea&sort=letter&look=blocket');
INSERT INTO store_params VALUES (12799, 'image_extra', '-');
INSERT INTO store_params VALUES (4124, 'import_id', 'fritidscenterstockholm');
INSERT INTO store_params VALUES (4124, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (4124, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=fritidscenterstockholm&look=fritidscenterstockholm&carsperpage=9000');
INSERT INTO store_params VALUES (4124, 'image_extra', '-');
INSERT INTO store_params VALUES (14130, 'import_id', 'tradingmedelhult');
INSERT INTO store_params VALUES (14130, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (14130, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=tradingmedelhult&sort=letter&look=blocket');
INSERT INTO store_params VALUES (14130, 'image_extra', '-');
INSERT INTO store_params VALUES (4131, 'import_id', 'karebykungalv');
INSERT INTO store_params VALUES (4131, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (4131, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=karebykungalv&look=karebykungalv');
INSERT INTO store_params VALUES (4131, 'image_extra', '-');
INSERT INTO store_params VALUES (13192, 'import_id', 'bogstedtboras');
INSERT INTO store_params VALUES (13192, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (13192, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=bogstedtboras&sort=letter&look=blocket');
INSERT INTO store_params VALUES (13192, 'image_extra', '-');
INSERT INTO store_params VALUES (4133, 'import_id', 'vernaorebro');
INSERT INTO store_params VALUES (4133, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (4133, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=vernaorebro&look=blocket');
INSERT INTO store_params VALUES (4133, 'image_extra', '-');
INSERT INTO store_params VALUES (4134, 'import_id', 'lidkopingsbiltjanst');
INSERT INTO store_params VALUES (4134, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (4134, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=lidkopingsbiltjanst&look=lidkopingsbiltjanst&carsperpage=25');
INSERT INTO store_params VALUES (4134, 'image_extra', '-');
INSERT INTO store_params VALUES (4136, 'import_id', 'yvonnenordanstigbilkarlstad');
INSERT INTO store_params VALUES (4136, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (4136, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=yvonnenordanstigbilkarlstad&sort=letter&look=blocket');
INSERT INTO store_params VALUES (4136, 'image_extra', '-');
INSERT INTO store_params VALUES (4142, 'import_id', 'biltecvaxjo');
INSERT INTO store_params VALUES (4142, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (4142, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=biltecvaxjo&sort=letter&look=blocket');
INSERT INTO store_params VALUES (4142, 'image_extra', '-');
INSERT INTO store_params VALUES (14191, 'import_id', 'bilpunktenboras');
INSERT INTO store_params VALUES (14191, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (14191, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=bilpunktenboras&sort=letter&look=blocket');
INSERT INTO store_params VALUES (14191, 'image_extra', '-');
INSERT INTO store_params VALUES (4141, 'import_id', 'karlstrommotorsalamc');
INSERT INTO store_params VALUES (4141, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (4141, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=karlstrommotorsalamc&sort=letter&look=blocket');
INSERT INTO store_params VALUES (4141, 'image_extra', '-');
INSERT INTO store_params VALUES (5320, 'infopage_no_ads', 'http://www.www.cls.se');
INSERT INTO store_params VALUES (5320, 'image_extra', '-');
INSERT INTO store_params VALUES (5519, 'infopage_no_ads', 'http://www.restenasbildelar.se');
INSERT INTO store_params VALUES (5519, 'image_extra', '-');
INSERT INTO store_params VALUES (6326, 'infopage_no_ads', 'http://www.bildelsimporten.nu');
INSERT INTO store_params VALUES (6326, 'image_extra', '-');
INSERT INTO store_params VALUES (14085, 'import_id', 'nybomsbil');
INSERT INTO store_params VALUES (14085, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (14085, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=nybomsbil&sort=letter&look=blocket');
INSERT INTO store_params VALUES (14085, 'image_extra', '-');
INSERT INTO store_params VALUES (14086, 'import_id', 'vallentunamotor');
INSERT INTO store_params VALUES (14086, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (14086, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=vallentunamotor&sort=letter&look=blocket');
INSERT INTO store_params VALUES (14086, 'image_extra', '-');
INSERT INTO store_params VALUES (6893, 'infopage_no_ads', 'http://www.sodermalms-bilservice.se/priser/priser-main.html');
INSERT INTO store_params VALUES (6893, 'image_extra', '-');
INSERT INTO store_params VALUES (6894, 'infopage_no_ads', 'http://www.lagerstedts-bilservice.se/docs/main/service_main.html');
INSERT INTO store_params VALUES (6894, 'image_extra', '-');
INSERT INTO store_params VALUES (9163, 'infopage_no_ads', '-');
INSERT INTO store_params VALUES (9163, 'image_extra', '-');
INSERT INTO store_params VALUES (9170, 'infopage_no_ads', '-');
INSERT INTO store_params VALUES (9170, 'image_extra', '-');
INSERT INTO store_params VALUES (9177, 'infopage_no_ads', '-');
INSERT INTO store_params VALUES (9177, 'image_extra', '-');
INSERT INTO store_params VALUES (9206, 'infopage_no_ads', 'http://www.soderbergsgummi.se/m3.html');
INSERT INTO store_params VALUES (9206, 'image_extra', '-');
INSERT INTO store_params VALUES (9848, 'import_id', 'bilgruppenenkopingsala');
INSERT INTO store_params VALUES (9848, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (9848, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=bilgruppenenkopingsala&sort=letter&look=blocket');
INSERT INTO store_params VALUES (9848, 'image_extra', '-');
INSERT INTO store_params VALUES (10285, 'image_extra', '-');
INSERT INTO store_params VALUES (10300, 'infopage_no_ads', '-');
INSERT INTO store_params VALUES (10300, 'image_extra', '-');
INSERT INTO store_params VALUES (10402, 'infopage_no_ads', '-');
INSERT INTO store_params VALUES (10402, 'image_extra', '-');
INSERT INTO store_params VALUES (10668, 'import_id', 'dahlqvistbil');
INSERT INTO store_params VALUES (10668, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (10668, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=dahlqvistbil&sort=letter&look=blocket');
INSERT INTO store_params VALUES (10668, 'image_extra', '-');
INSERT INTO store_params VALUES (10703, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?sessionid=15345965000&no=19&look=mc/hlista_2002&sort=letter&seller=gmckarlstadmc');
INSERT INTO store_params VALUES (10703, 'image_extra', '-');
INSERT INTO store_params VALUES (10711, 'infopage_no_ads', 'http://www.mc-racing.se/begagnade_skotrar.htm');
INSERT INTO store_params VALUES (10711, 'image_extra', '-');
INSERT INTO store_params VALUES (10712, 'infopage_no_ads', 'http://www.johanssonmaskinkl.se/fritid.html');
INSERT INTO store_params VALUES (10712, 'image_extra', '-');
INSERT INTO store_params VALUES (10713, 'import_id', 'gunnara');
INSERT INTO store_params VALUES (10713, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (10713, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=gunnara&sort=letter&look=blocket');
INSERT INTO store_params VALUES (10713, 'image_extra', '-');
INSERT INTO store_params VALUES (10715, 'import_id', 'swedenmotorshopkdmc');
INSERT INTO store_params VALUES (10715, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (10715, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=swedenmotorshopkdmc&sort=letter&look=blocket');
INSERT INTO store_params VALUES (10715, 'image_extra', '-');
INSERT INTO store_params VALUES (10720, 'import_id', 'ismetbravoorebro');
INSERT INTO store_params VALUES (10720, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (10720, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=ismetbravoorebro&sort=letter&look=blocket');
INSERT INTO store_params VALUES (10720, 'image_extra', '-');
INSERT INTO store_params VALUES (10729, 'import_id', 'bikethnmc');
INSERT INTO store_params VALUES (10729, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (10729, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=bikethnmc&sort=letter&look=blocket');
INSERT INTO store_params VALUES (10729, 'image_extra', '-');
INSERT INTO store_params VALUES (10723, 'infopage_no_ads', 'http://www.folksamauto.com/bilforsalj.html');
INSERT INTO store_params VALUES (10723, 'image_extra', '-');
INSERT INTO store_params VALUES (10728, 'infopage_no_ads', 'http://www.sjomarksdack.nu
');
INSERT INTO store_params VALUES (10728, 'image_extra', '-');
INSERT INTO store_params VALUES (10732, 'infopage_no_ads', 'http://www.grevensmcdelar.com/default.asp?cat=begmc');
INSERT INTO store_params VALUES (10732, 'image_extra', '-');
INSERT INTO store_params VALUES (10733, 'infopage_no_ads', 'http://www.ssiab.se/sve/index_begagnat.htm');
INSERT INTO store_params VALUES (10733, 'image_extra', '-');
INSERT INTO store_params VALUES (10734, 'infopage_no_ads', 'http://www.ssiab.se/sve/index_begagnat.htm');
INSERT INTO store_params VALUES (10734, 'image_extra', '-');
INSERT INTO store_params VALUES (10736, 'infopage_no_ads', 'http://nya.fbt.se/asp/partsearch/stock/?kundnr=127');
INSERT INTO store_params VALUES (10736, 'image_extra', '-');
INSERT INTO store_params VALUES (10738, 'infopage_no_ads', 'http://www.tradecenter.se/dealer_privatelist.asp?sortorder=fabrication&dealerid=281&font=verdana
');
INSERT INTO store_params VALUES (10738, 'image_extra', '-');
INSERT INTO store_params VALUES (10739, 'infopage_no_ads', 'http://www.tradecenter.se/dealer_privatelist.asp?sortorder=fabrication&dealerid=38&font=verdana
');
INSERT INTO store_params VALUES (10739, 'image_extra', '-');
INSERT INTO store_params VALUES (10740, 'import_id', 'tagagaragethbg');
INSERT INTO store_params VALUES (10740, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (10740, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=tagagaragethbg&sort=letter&look=blocket');
INSERT INTO store_params VALUES (10740, 'image_extra', '-');
INSERT INTO store_params VALUES (10742, 'infopage_no_ads', 'http://www.fbt.se/egetlager/bildelar.asp?action=24');
INSERT INTO store_params VALUES (10742, 'image_extra', '-');
INSERT INTO store_params VALUES (13083, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=michaelahudik&sort=letter&look=blocket');
INSERT INTO store_params VALUES (13083, 'image_extra', '-');
INSERT INTO store_params VALUES (10743, 'infopage_no_ads', 'http://www.bildelsbasen.se/frames.asp?Action=Enskiltlager&kundnr=67');
INSERT INTO store_params VALUES (10743, 'image_extra', '-');
INSERT INTO store_params VALUES (10952, 'import_id', 'kanvarbergmc');
INSERT INTO store_params VALUES (10952, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (10952, 'infopage_no_ads', 'http://www.bytmc.com/select_mc.cgi?seller=kanvarbergmc&look=kanvarbergmc&sort=letter');
INSERT INTO store_params VALUES (10952, 'image_extra', '-');
INSERT INTO store_params VALUES (10956, 'import_id', 'smbilar');
INSERT INTO store_params VALUES (10956, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (10956, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=smbilar&look=smbilar');
INSERT INTO store_params VALUES (10956, 'image_extra', '-');
INSERT INTO store_params VALUES (10957, 'infopage_no_ads', 'http://www.lagabasen.se/start.aspx?id=1051');
INSERT INTO store_params VALUES (10957, 'image_extra', '-');
INSERT INTO store_params VALUES (10961, 'infopage_no_ads', 'http://www.bildelsbasen.se/asp/partsearch/stock/?kundnr=151');
INSERT INTO store_params VALUES (10961, 'image_extra', '-');
INSERT INTO store_params VALUES (11325, 'infopage_no_ads', 'http://www.bildelsbasen.se/frames.asp?Action=Enskiltlager&kundnr=135');
INSERT INTO store_params VALUES (11325, 'image_extra', '-');
INSERT INTO store_params VALUES (11328, 'infopage_no_ads', 'http://www.lagabasen.se/start.aspx?id=1033');
INSERT INTO store_params VALUES (11328, 'image_extra', '-');
INSERT INTO store_params VALUES (11691, 'infopage_no_ads', 'http://fm.datarutinmedia.se/vege/FMPro?-DB=vege.fp3&-format=vege_begagnat.html&-findall');
INSERT INTO store_params VALUES (11691, 'image_extra', '-');
INSERT INTO store_params VALUES (11694, 'infopage_no_ads', 'http://www.allbildelar.se/sid1.html');
INSERT INTO store_params VALUES (11694, 'image_extra', '-');
INSERT INTO store_params VALUES (11696, 'infopage_no_ads', 'http://www.mctema.com/default.asp?cat=bikes');
INSERT INTO store_params VALUES (11696, 'image_extra', '-');
INSERT INTO store_params VALUES (11697, 'infopage_no_ads', 'http://www.mctema.com/default.asp?cat=bikes');
INSERT INTO store_params VALUES (11697, 'image_extra', '-');
INSERT INTO store_params VALUES (11699, 'infopage_no_ads', 'http://www.bildelsbasen.se/frames.asp?Action=Enskiltlager&kundnr=62');
INSERT INTO store_params VALUES (11699, 'image_extra', '-');
INSERT INTO store_params VALUES (11700, 'infopage_no_ads', 'http://www.dackspecialen.com/Erbjudanden1.htm');
INSERT INTO store_params VALUES (11700, 'image_extra', '-');
INSERT INTO store_params VALUES (11703, 'infopage_no_ads', 'http://www.bildelsbasen.se/asp/partsearch/stock/?kundnr=26');
INSERT INTO store_params VALUES (11703, 'image_extra', '-');
INSERT INTO store_params VALUES (11704, 'infopage_no_ads', 'http://www.loderupshusvagnstjanst.se/index-filer/body/body-filer/Beg_husvagnar.html');
INSERT INTO store_params VALUES (11704, 'image_extra', '-');
INSERT INTO store_params VALUES (11706, 'infopage_no_ads', 'http://www.loderupshusvagnstjanst.se/index-filer/body/body-filer/Beg_husvagnar.html');
INSERT INTO store_params VALUES (11706, 'image_extra', '-');
INSERT INTO store_params VALUES (11707, 'infopage_no_ads', 'http://www.loderupshusvagnstjanst.se/index-filer/body/body-filer/Beg_husvagnar.html');
INSERT INTO store_params VALUES (11707, 'image_extra', '-');
INSERT INTO store_params VALUES (11708, 'infopage_no_ads', 'http://www.loderupshusvagnstjanst.se/index-filer/body/body-filer/Beg_husvagnar.html');
INSERT INTO store_params VALUES (11708, 'image_extra', '-');
INSERT INTO store_params VALUES (11709, 'infopage_no_ads', 'http://www.loderupshusvagnstjanst.se/index-filer/body/body-filer/Beg_husvagnar.html');
INSERT INTO store_params VALUES (11709, 'image_extra', '-');
INSERT INTO store_params VALUES (11710, 'infopage_no_ads', 'http://www.loderupshusvagnstjanst.se/index-filer/body/body-filer/Beg_husvagnar.html');
INSERT INTO store_params VALUES (11710, 'image_extra', '-');
INSERT INTO store_params VALUES (11711, 'infopage_no_ads', 'http://www.loderupshusvagnstjanst.se/index-filer/body/body-filer/Beg_husvagnar.html');
INSERT INTO store_params VALUES (11711, 'image_extra', '-');
INSERT INTO store_params VALUES (11712, 'infopage_no_ads', 'http://www.loderupshusvagnstjanst.se/index-filer/body/body-filer/Beg_husvagnar.html');
INSERT INTO store_params VALUES (11712, 'image_extra', '-');
INSERT INTO store_params VALUES (11713, 'infopage_no_ads', 'http://www.loderupshusvagnstjanst.se/index-filer/body/body-filer/Beg_husvagnar.html');
INSERT INTO store_params VALUES (11713, 'image_extra', '-');
INSERT INTO store_params VALUES (11714, 'infopage_no_ads', 'http://www.loderupshusvagnstjanst.se/index-filer/body/body-filer/Beg_husvagnar.html');
INSERT INTO store_params VALUES (11714, 'image_extra', '-');
INSERT INTO store_params VALUES (11715, 'infopage_no_ads', 'http://www.loderupshusvagnstjanst.se/index-filer/body/body-filer/Beg_husvagnar.html');
INSERT INTO store_params VALUES (11715, 'image_extra', '-');
INSERT INTO store_params VALUES (11716, 'infopage_no_ads', 'http://www.loderupshusvagnstjanst.se/index-filer/body/body-filer/Beg_husvagnar.html');
INSERT INTO store_params VALUES (11716, 'image_extra', '-');
INSERT INTO store_params VALUES (11717, 'infopage_no_ads', 'http://www.loderupshusvagnstjanst.se/index-filer/body/body-filer/Beg_husvagnar.html');
INSERT INTO store_params VALUES (11717, 'image_extra', '-');
INSERT INTO store_params VALUES (11718, 'infopage_no_ads', 'http://www.loderupshusvagnstjanst.se/index-filer/body/body-filer/Beg_husvagnar.html');
INSERT INTO store_params VALUES (11718, 'image_extra', '-');
INSERT INTO store_params VALUES (11719, 'infopage_no_ads', 'http://www.loderupshusvagnstjanst.se/index-filer/body/body-filer/Beg_husvagnar.html');
INSERT INTO store_params VALUES (11719, 'image_extra', '-');
INSERT INTO store_params VALUES (11720, 'infopage_no_ads', 'http://www.loderupshusvagnstjanst.se/index-filer/body/body-filer/Beg_husvagnar.html');
INSERT INTO store_params VALUES (11720, 'image_extra', '-');
INSERT INTO store_params VALUES (11721, 'infopage_no_ads', 'http://www.bildelshus.se');
INSERT INTO store_params VALUES (11721, 'image_extra', '-');
INSERT INTO store_params VALUES (11728, 'infopage_no_ads', '-');
INSERT INTO store_params VALUES (11728, 'image_extra', '-');
INSERT INTO store_params VALUES (11730, 'import_id', 'lorkemc');
INSERT INTO store_params VALUES (11730, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (11730, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=lorkemc&sort=letter&look=blocket');
INSERT INTO store_params VALUES (11730, 'image_extra', '-');
INSERT INTO store_params VALUES (11910, 'infopage_no_ads', 'http://www.bobman.se/');
INSERT INTO store_params VALUES (11910, 'image_extra', '-');
INSERT INTO store_params VALUES (11922, 'infopage_no_ads', '-');
INSERT INTO store_params VALUES (11922, 'image_extra', '-');
INSERT INTO store_params VALUES (12038, 'infopage_no_ads', 'http://www.krimamaskin.se/sales.html');
INSERT INTO store_params VALUES (12038, 'image_extra', '-');
INSERT INTO store_params VALUES (12039, 'infopage_no_ads', '-');
INSERT INTO store_params VALUES (12039, 'image_extra', '-');
INSERT INTO store_params VALUES (12053, 'infopage_no_ads', 'http://www.hoglandetsmaskin.se/Assembler.asp?TreeID=915&CustomerID=317&requirepass=False&ObjectType=1');
INSERT INTO store_params VALUES (12053, 'image_extra', '-');
INSERT INTO store_params VALUES (12147, 'infopage_no_ads', '-');
INSERT INTO store_params VALUES (12147, 'image_extra', '-');
INSERT INTO store_params VALUES (12232, 'infopage_no_ads', 'http://www.tbm.se');
INSERT INTO store_params VALUES (12232, 'image_extra', 'thorebostrom_maskin.jpg');
INSERT INTO store_params VALUES (12239, 'infopage_no_ads', 'http://
');
INSERT INTO store_params VALUES (12239, 'image_extra', '-');
INSERT INTO store_params VALUES (12651, 'infopage_no_ads', 'http://www.tradecenter.se/dealer_privatelist.asp?machineryid&sortorder=fabrication&dealerid=414');
INSERT INTO store_params VALUES (12651, 'image_extra', '-');
INSERT INTO store_params VALUES (12281, 'infopage_no_ads', '-');
INSERT INTO store_params VALUES (12281, 'image_extra', '-');
INSERT INTO store_params VALUES (12648, 'import_id', 'larslevin');
INSERT INTO store_params VALUES (12648, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (12648, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=larslevin&sort=letter&look=blocket');
INSERT INTO store_params VALUES (12648, 'image_extra', '-');
INSERT INTO store_params VALUES (12324, 'infopage_no_ads', 'http://www.tradecenter.se/dealer_privatelist.asp?sortorder=fabrication&dealerid=132%20');
INSERT INTO store_params VALUES (12324, 'image_extra', '-');
INSERT INTO store_params VALUES (12325, 'infopage_no_ads', 'http://www.redox.se/nyinkommet.asp');
INSERT INTO store_params VALUES (12325, 'image_extra', '-');
INSERT INTO store_params VALUES (12326, 'infopage_no_ads', 'http://www.redox.se/nyinkommet.asp');
INSERT INTO store_params VALUES (12326, 'image_extra', '-');
INSERT INTO store_params VALUES (12333, 'import_id', 'rinqvistbilomcmariestad');
INSERT INTO store_params VALUES (12333, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (12333, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=rinqvistbilomcmariestad&sort=letter&look=blocket');
INSERT INTO store_params VALUES (12333, 'image_extra', '-');
INSERT INTO store_params VALUES (12336, 'infopage_no_ads', 'http://www.ja-carcare.com/bilder.html');
INSERT INTO store_params VALUES (12336, 'image_extra', '-');
INSERT INTO store_params VALUES (12337, 'infopage_no_ads', '-');
INSERT INTO store_params VALUES (12337, 'image_extra', '-');
INSERT INTO store_params VALUES (12338, 'infopage_no_ads', '-');
INSERT INTO store_params VALUES (12338, 'image_extra', '-');
INSERT INTO store_params VALUES (12341, 'infopage_no_ads', 'http://www.motorbilreservdelar.com');
INSERT INTO store_params VALUES (12341, 'image_extra', '-');
INSERT INTO store_params VALUES (12392, 'infopage_no_ads', 'http://w1.270.telia.com/~u27003444/reservdelar.html');
INSERT INTO store_params VALUES (12392, 'image_extra', '-');
INSERT INTO store_params VALUES (12397, 'import_id', 'autoimporten');
INSERT INTO store_params VALUES (12397, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (12397, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=autoimporten&sort=letter&look=blocket');
INSERT INTO store_params VALUES (12397, 'image_extra', '-');
INSERT INTO store_params VALUES (14438, 'import_id', 'gtryd');
INSERT INTO store_params VALUES (14438, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (14438, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=gtryd&sort=letter&look=blocket');
INSERT INTO store_params VALUES (14438, 'image_extra', '-');
INSERT INTO store_params VALUES (12414, 'infopage_no_ads', 'http://www.johanssonmaskinkl.se/beg_products.html');
INSERT INTO store_params VALUES (12414, 'image_extra', '-');
INSERT INTO store_params VALUES (12418, 'import_id', 'halmstadbilcity');
INSERT INTO store_params VALUES (12418, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (12418, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=halmstadbilcity&look=blocket');
INSERT INTO store_params VALUES (12418, 'image_extra', '-');
INSERT INTO store_params VALUES (12419, 'import_id', 'laholmsbil');
INSERT INTO store_params VALUES (12419, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (12419, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=laholmsbil&look=blocket');
INSERT INTO store_params VALUES (12419, 'image_extra', '-');
INSERT INTO store_params VALUES (12420, 'import_id', 'btbilhbg');
INSERT INTO store_params VALUES (12420, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (12420, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=btbilhbg&sort=letter&look=blocket');
INSERT INTO store_params VALUES (12420, 'image_extra', '-');
INSERT INTO store_params VALUES (13322, 'import_id', 'nilssonsbil');
INSERT INTO store_params VALUES (13322, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (13322, 'infopage_no_ads', '-');
INSERT INTO store_params VALUES (13322, 'image_extra', '-');
INSERT INTO store_params VALUES (13631, 'import_id', 'bilforummarsta');
INSERT INTO store_params VALUES (13631, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (13631, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=bilforummarsta&sort=letter&look=blocket');
INSERT INTO store_params VALUES (13631, 'image_extra', '-');
INSERT INTO store_params VALUES (12423, 'infopage_no_ads', 'http://www.tornum.se/sortiment.html');
INSERT INTO store_params VALUES (12423, 'image_extra', '-');
INSERT INTO store_params VALUES (12427, 'infopage_no_ads', 'http://www.motorhusetgavle.se/motorcyklar.htm');
INSERT INTO store_params VALUES (12427, 'image_extra', '-');
INSERT INTO store_params VALUES (12428, 'import_id', 'perlarssonstaffanstorp');
INSERT INTO store_params VALUES (12428, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (12428, 'infopage_no_ads', 'http://');
INSERT INTO store_params VALUES (12428, 'image_extra', '-');
INSERT INTO store_params VALUES (13302, 'import_id', 'schultzbergbiljkpg');
INSERT INTO store_params VALUES (13302, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (13302, 'infopage_no_ads', '-');
INSERT INTO store_params VALUES (13302, 'image_extra', '-');
INSERT INTO store_params VALUES (12432, 'infopage_no_ads', 'http://www.tktraktordelar.se/sve/index.asp?mainID=40&subID=113');
INSERT INTO store_params VALUES (12432, 'image_extra', '-');
INSERT INTO store_params VALUES (12440, 'import_id', 'bilmanssoneslov');
INSERT INTO store_params VALUES (12440, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (12440, 'infopage_no_ads', '-');
INSERT INTO store_params VALUES (12440, 'image_extra', '-');
INSERT INTO store_params VALUES (12441, 'import_id', 'lmautoservicetyringe');
INSERT INTO store_params VALUES (12441, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (12441, 'infopage_no_ads', '-');
INSERT INTO store_params VALUES (12441, 'image_extra', '-');
INSERT INTO store_params VALUES (12444, 'import_id', 'motorhalland');
INSERT INTO store_params VALUES (12444, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (12444, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=motorhalland&look=blocket&carsperpage=20');
INSERT INTO store_params VALUES (12444, 'image_extra', '-');
INSERT INTO store_params VALUES (12450, 'infopage_no_ads', 'http://www.fbt.se/egetlager/bildelar.asp?action=170');
INSERT INTO store_params VALUES (12450, 'image_extra', '-');
INSERT INTO store_params VALUES (14199, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=jocketranas&sort=letter&look=blocket
');
INSERT INTO store_params VALUES (14199, 'image_extra', '-');
INSERT INTO store_params VALUES (14199, 'import_partner', 'huges');
INSERT INTO store_params VALUES (14199, 'import_id', 'Huges V�stervik');
INSERT INTO store_params VALUES (14199, 'import_type', '2');
INSERT INTO store_params VALUES (12455, 'infopage_no_ads', 'http://www.tradecenter.se/dealer_privatelist.asp?sortorder=fabrication&dealerid=125&bgcolor=ff0000&rowcolor=ff5050');
INSERT INTO store_params VALUES (12455, 'image_extra', '-');
INSERT INTO store_params VALUES (12456, 'infopage_no_ads', 'http://www.tradecenter.se/dealer_privatelist.asp?sortorder=fabrication&dealerid=125&bgcolor=ff0000&rowcolor=ff5050');
INSERT INTO store_params VALUES (12456, 'image_extra', '-');
INSERT INTO store_params VALUES (12457, 'infopage_no_ads', 'http://www.tradecenter.se/dealer_privatelist.asp?sortorder=fabrication&dealerid=125&bgcolor=ff0000&rowcolor=ff5050');
INSERT INTO store_params VALUES (12457, 'image_extra', '-');
INSERT INTO store_params VALUES (12458, 'infopage_no_ads', 'http://www.tradecenter.se/dealer_privatelist.asp?sortorder=fabrication&dealerid=125&bgcolor=ff0000&rowcolor=ff5050');
INSERT INTO store_params VALUES (12458, 'image_extra', '-');
INSERT INTO store_params VALUES (12459, 'infopage_no_ads', 'http://www.tradecenter.se/dealer_privatelist.asp?sortorder=fabrication&dealerid=125&bgcolor=ff0000&rowcolor=ff5050');
INSERT INTO store_params VALUES (12459, 'image_extra', '-');
INSERT INTO store_params VALUES (12460, 'infopage_no_ads', 'http://www.tradecenter.se/dealer_privatelist.asp?sortorder=fabrication&dealerid=125&bgcolor=ff0000&rowcolor=ff5050');
INSERT INTO store_params VALUES (12460, 'image_extra', '-');
INSERT INTO store_params VALUES (12461, 'infopage_no_ads', 'http://www.tradecenter.se/dealer_privatelist.asp?sortorder=fabrication&dealerid=125&bgcolor=ff0000&rowcolor=ff5050');
INSERT INTO store_params VALUES (12461, 'image_extra', '-');
INSERT INTO store_params VALUES (12462, 'infopage_no_ads', 'http://www.tradecenter.se/dealer_privatelist.asp?sortorder=fabrication&dealerid=125&bgcolor=ff0000&rowcolor=ff5050');
INSERT INTO store_params VALUES (12462, 'image_extra', '-');
INSERT INTO store_params VALUES (12463, 'infopage_no_ads', 'http://www.tradecenter.se/dealer_privatelist.asp?sortorder=fabrication&dealerid=125&bgcolor=ff0000&rowcolor=ff5050');
INSERT INTO store_params VALUES (12463, 'image_extra', '-');
INSERT INTO store_params VALUES (12464, 'infopage_no_ads', 'http://www.tradecenter.se/dealer_privatelist.asp?sortorder=fabrication&dealerid=125&bgcolor=ff0000&rowcolor=ff5050');
INSERT INTO store_params VALUES (12464, 'image_extra', '-');
INSERT INTO store_params VALUES (12465, 'infopage_no_ads', 'http://www.tradecenter.se/dealer_privatelist.asp?sortorder=fabrication&dealerid=125&bgcolor=ff0000&rowcolor=ff5050');
INSERT INTO store_params VALUES (12465, 'image_extra', '-');
INSERT INTO store_params VALUES (12466, 'infopage_no_ads', 'http://www.tradecenter.se/dealer_privatelist.asp?sortorder=fabrication&dealerid=125&bgcolor=ff0000&rowcolor=ff5050');
INSERT INTO store_params VALUES (12466, 'image_extra', '-');
INSERT INTO store_params VALUES (12467, 'infopage_no_ads', 'http://www.tradecenter.se/dealer_privatelist.asp?sortorder=fabrication&dealerid=125&bgcolor=ff0000&rowcolor=ff5050');
INSERT INTO store_params VALUES (12467, 'image_extra', '-');
INSERT INTO store_params VALUES (12468, 'infopage_no_ads', 'http://www.tradecenter.se/dealer_privatelist.asp?sortorder=fabrication&dealerid=125&bgcolor=ff0000&rowcolor=ff5050');
INSERT INTO store_params VALUES (12468, 'image_extra', '-');
INSERT INTO store_params VALUES (12469, 'infopage_no_ads', 'http://www.tradecenter.se/dealer_privatelist.asp?sortorder=fabrication&dealerid=125&bgcolor=ff0000&rowcolor=ff5050');
INSERT INTO store_params VALUES (12469, 'image_extra', '-');
INSERT INTO store_params VALUES (14426, 'import_id', 'motorsalongenhstad');
INSERT INTO store_params VALUES (14426, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (14426, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=motorsalongenhstad&sort=letter&look=blocket');
INSERT INTO store_params VALUES (14426, 'image_extra', '-');
INSERT INTO store_params VALUES (12472, 'infopage_no_ads', 'http://www.eurogarage.com/e_begbil.cfm');
INSERT INTO store_params VALUES (12472, 'image_extra', '-');
INSERT INTO store_params VALUES (12473, 'infopage_no_ads', 'http://www.eurogarage.com/e_begbil.cfm');
INSERT INTO store_params VALUES (12473, 'image_extra', '-');
INSERT INTO store_params VALUES (12474, 'infopage_no_ads', 'http://www.eurogarage.com/e_begbil.cfm');
INSERT INTO store_params VALUES (12474, 'image_extra', '-');
INSERT INTO store_params VALUES (12475, 'infopage_no_ads', 'http://www.eurogarage.com/e_begbil.cfm');
INSERT INTO store_params VALUES (12475, 'image_extra', '-');
INSERT INTO store_params VALUES (12476, 'infopage_no_ads', 'http://www.eurogarage.com/e_begbil.cfm');
INSERT INTO store_params VALUES (12476, 'image_extra', '-');
INSERT INTO store_params VALUES (12477, 'infopage_no_ads', 'http://www.eurogarage.com/e_begbil.cfm');
INSERT INTO store_params VALUES (12477, 'image_extra', '-');
INSERT INTO store_params VALUES (12478, 'infopage_no_ads', 'http://www.eurogarage.com/e_begbil.cfm');
INSERT INTO store_params VALUES (12478, 'image_extra', '-');
INSERT INTO store_params VALUES (12479, 'infopage_no_ads', 'http://www.eurogarage.com/e_begbil.cfm');
INSERT INTO store_params VALUES (12479, 'image_extra', '-');
INSERT INTO store_params VALUES (12480, 'infopage_no_ads', 'http://www.eurogarage.com/e_begbil.cfm');
INSERT INTO store_params VALUES (12480, 'image_extra', '-');
INSERT INTO store_params VALUES (12481, 'infopage_no_ads', 'http://www.eurogarage.com/e_begbil.cfm');
INSERT INTO store_params VALUES (12481, 'image_extra', '-');
INSERT INTO store_params VALUES (12482, 'infopage_no_ads', 'http://www.eurogarage.com/e_begbil.cfm');
INSERT INTO store_params VALUES (12482, 'image_extra', '-');
INSERT INTO store_params VALUES (12483, 'infopage_no_ads', 'http://www.eurogarage.com/e_begbil.cfm');
INSERT INTO store_params VALUES (12483, 'image_extra', '-');
INSERT INTO store_params VALUES (12484, 'infopage_no_ads', 'http://www.eurogarage.com/e_begbil.cfm');
INSERT INTO store_params VALUES (12484, 'image_extra', '-');
INSERT INTO store_params VALUES (12485, 'import_id', 'anvika');
INSERT INTO store_params VALUES (12485, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (12485, 'infopage_no_ads', 'http://www.eurogarage.com/e_begbil.cfm');
INSERT INTO store_params VALUES (12485, 'image_extra', '-');
INSERT INTO store_params VALUES (12486, 'infopage_no_ads', 'http://www.eurogarage.com/e_begbil.cfm');
INSERT INTO store_params VALUES (12486, 'image_extra', '-');
INSERT INTO store_params VALUES (12487, 'infopage_no_ads', 'http://www.eurogarage.com/e_begbil.cfm');
INSERT INTO store_params VALUES (12487, 'image_extra', '-');
INSERT INTO store_params VALUES (12488, 'infopage_no_ads', 'http://www.eurogarage.com/e_begbil.cfm');
INSERT INTO store_params VALUES (12488, 'image_extra', '-');
INSERT INTO store_params VALUES (12489, 'infopage_no_ads', 'http://www.eurogarage.com/e_begbil.cfm');
INSERT INTO store_params VALUES (12489, 'image_extra', '-');
INSERT INTO store_params VALUES (12490, 'infopage_no_ads', 'http://www.eurogarage.com/e_begbil.cfm');
INSERT INTO store_params VALUES (12490, 'image_extra', '-');
INSERT INTO store_params VALUES (12491, 'infopage_no_ads', 'http://www.eurogarage.com/e_begbil.cfm');
INSERT INTO store_params VALUES (12491, 'image_extra', '-');
INSERT INTO store_params VALUES (12492, 'infopage_no_ads', 'http://www.eurogarage.com/e_begbil.cfm');
INSERT INTO store_params VALUES (12492, 'image_extra', '-');
INSERT INTO store_params VALUES (12493, 'infopage_no_ads', 'http://www.eurogarage.com/e_begbil.cfm');
INSERT INTO store_params VALUES (12493, 'image_extra', '-');
INSERT INTO store_params VALUES (12494, 'infopage_no_ads', 'http://www.eurogarage.com/e_begbil.cfm');
INSERT INTO store_params VALUES (12494, 'image_extra', '-');
INSERT INTO store_params VALUES (12495, 'infopage_no_ads', 'http://www.eurogarage.com/e_begbil.cfm');
INSERT INTO store_params VALUES (12495, 'image_extra', '-');
INSERT INTO store_params VALUES (12496, 'infopage_no_ads', 'http://www.eurogarage.com/e_begbil.cfm');
INSERT INTO store_params VALUES (12496, 'image_extra', '-');
INSERT INTO store_params VALUES (12497, 'infopage_no_ads', 'http://www.eurogarage.com/e_begbil.cfm');
INSERT INTO store_params VALUES (12497, 'image_extra', '-');
INSERT INTO store_params VALUES (12498, 'infopage_no_ads', 'http://www.eurogarage.com/e_begbil.cfm');
INSERT INTO store_params VALUES (12498, 'image_extra', '-');
INSERT INTO store_params VALUES (12499, 'infopage_no_ads', 'http://www.eurogarage.com/e_begbil.cfm');
INSERT INTO store_params VALUES (12499, 'image_extra', '-');
INSERT INTO store_params VALUES (12500, 'infopage_no_ads', 'http://www.eurogarage.com/e_begbil.cfm');
INSERT INTO store_params VALUES (12500, 'image_extra', '-');
INSERT INTO store_params VALUES (12501, 'infopage_no_ads', 'http://www.eurogarage.com/e_begbil.cfm');
INSERT INTO store_params VALUES (12501, 'image_extra', '-');
INSERT INTO store_params VALUES (12502, 'infopage_no_ads', 'http://www.eurogarage.com/e_begbil.cfm');
INSERT INTO store_params VALUES (12502, 'image_extra', '-');
INSERT INTO store_params VALUES (12503, 'infopage_no_ads', 'http://www.eurogarage.com/e_begbil.cfm');
INSERT INTO store_params VALUES (12503, 'image_extra', '-');
INSERT INTO store_params VALUES (12504, 'infopage_no_ads', 'http://www.eurogarage.com/e_begbil.cfm');
INSERT INTO store_params VALUES (12504, 'image_extra', '-');
INSERT INTO store_params VALUES (12505, 'infopage_no_ads', 'http://www.eurogarage.com/e_begbil.cfm');
INSERT INTO store_params VALUES (12505, 'image_extra', '-');
INSERT INTO store_params VALUES (12506, 'infopage_no_ads', 'http://www.eurogarage.com/e_begbil.cfm');
INSERT INTO store_params VALUES (12506, 'image_extra', '-');
INSERT INTO store_params VALUES (12507, 'infopage_no_ads', 'http://www.eurogarage.com/e_begbil.cfm');
INSERT INTO store_params VALUES (12507, 'image_extra', '-');
INSERT INTO store_params VALUES (12508, 'infopage_no_ads', 'http://www.eurogarage.com/e_begbil.cfm');
INSERT INTO store_params VALUES (12508, 'image_extra', '-');
INSERT INTO store_params VALUES (12509, 'infopage_no_ads', 'http://www.eurogarage.com/e_begbil.cfm');
INSERT INTO store_params VALUES (12509, 'image_extra', '-');
INSERT INTO store_params VALUES (12516, 'infopage_no_ads', 'http://www.tomelillahusvagnar.se/begagnat.htm');
INSERT INTO store_params VALUES (12516, 'image_extra', '-');
INSERT INTO store_params VALUES (12517, 'infopage_no_ads', '-');
INSERT INTO store_params VALUES (12517, 'image_extra', '-');
INSERT INTO store_params VALUES (12519, 'infopage_no_ads', 'http://www.spinnin-wheel.se/index2.html');
INSERT INTO store_params VALUES (12519, 'image_extra', '-');
INSERT INTO store_params VALUES (12529, 'import_id', 'bilfirmanibara');
INSERT INTO store_params VALUES (12529, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (12529, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=bilfirmanibara&sort=letter&look=blocket');
INSERT INTO store_params VALUES (12529, 'image_extra', '-');
INSERT INTO store_params VALUES (14193, 'import_id', 'allankakiorebro');
INSERT INTO store_params VALUES (14193, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (14193, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=allankakiorebro&sort=letter&look=blocket');
INSERT INTO store_params VALUES (14193, 'image_extra', '-');
INSERT INTO store_params VALUES (12528, 'import_id', 'bildaxvasteras');
INSERT INTO store_params VALUES (12528, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (12528, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=bildaxvasteras&sort=letter&look=blocket');
INSERT INTO store_params VALUES (12528, 'image_extra', '-');
INSERT INTO store_params VALUES (12534, 'import_id', 'bilbutikenkungalv');
INSERT INTO store_params VALUES (12534, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (12534, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=bilbutikenkungalv&look=bilbutikenkungalv&carsperpage=120');
INSERT INTO store_params VALUES (12534, 'image_extra', '-');
INSERT INTO store_params VALUES (12535, 'import_id', 'bilbutikenhaby');
INSERT INTO store_params VALUES (12535, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (12535, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=bilbutikenhaby&look=bilbutikenkungalv&carsperpage=120');
INSERT INTO store_params VALUES (12535, 'image_extra', '-');
INSERT INTO store_params VALUES (12537, 'infopage_no_ads', 'http://www.tollarpsbilskrot.com/lager/ilager.php');
INSERT INTO store_params VALUES (12537, 'image_extra', '-');
INSERT INTO store_params VALUES (12540, 'infopage_no_ads', '-');
INSERT INTO store_params VALUES (12540, 'image_extra', '-');
INSERT INTO store_params VALUES (12541, 'import_id', 'sockenplaenskede');
INSERT INTO store_params VALUES (12541, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (12541, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=sockenplaenskede&sort=letter&look=blocket');
INSERT INTO store_params VALUES (12541, 'image_extra', '-');
INSERT INTO store_params VALUES (13709, 'import_id', 'kaab');
INSERT INTO store_params VALUES (13709, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (13709, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=kaab&sort=letter&look=blocket');
INSERT INTO store_params VALUES (13709, 'image_extra', '-');
INSERT INTO store_params VALUES (12552, 'import_id', 'aolssonbil');
INSERT INTO store_params VALUES (12552, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (12552, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=aolssonbil&sort=letter&look=blocket');
INSERT INTO store_params VALUES (12552, 'image_extra', '-');
INSERT INTO store_params VALUES (12559, 'infopage_no_ads', 'http://www.cyclecenter.se/Begagnat.html');
INSERT INTO store_params VALUES (12559, 'image_extra', '-');
INSERT INTO store_params VALUES (12561, 'import_id', 'albybilalby');
INSERT INTO store_params VALUES (12561, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (12561, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=albybilalby&sort=letter&look=hlista_2002');
INSERT INTO store_params VALUES (12561, 'image_extra', '-');
INSERT INTO store_params VALUES (12562, 'infopage_no_ads', '-');
INSERT INTO store_params VALUES (12562, 'image_extra', '-');
INSERT INTO store_params VALUES (12563, 'import_id', 'cnbilborlange');
INSERT INTO store_params VALUES (12563, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (12563, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=cnbilborlange&sort=letter&look=blocket');
INSERT INTO store_params VALUES (12563, 'image_extra', '-');
INSERT INTO store_params VALUES (12566, 'import_id', 'custommctidaholmmc');
INSERT INTO store_params VALUES (12566, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (12566, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=custommctidaholmmc&sort=letter&look=blocket');
INSERT INTO store_params VALUES (12566, 'image_extra', '-');
INSERT INTO store_params VALUES (12576, 'import_id', 'bjoreetuna');
INSERT INTO store_params VALUES (12576, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (12576, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=bjoreetuna&sort=letter&look=blocket');
INSERT INTO store_params VALUES (12576, 'image_extra', '-');
INSERT INTO store_params VALUES (12577, 'infopage_no_ads', '-');
INSERT INTO store_params VALUES (12577, 'image_extra', '-');
INSERT INTO store_params VALUES (12594, 'infopage_no_ads', 'http://www.batochmotor.se/');
INSERT INTO store_params VALUES (12594, 'image_extra', '-');
INSERT INTO store_params VALUES (12736, 'import_id', 'palmsbilfirma');
INSERT INTO store_params VALUES (12736, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (12736, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=palmsbilfirma&look=blocket');
INSERT INTO store_params VALUES (12736, 'image_extra', '-');
INSERT INTO store_params VALUES (12600, 'infopage_no_ads', 'http://www.tradecenter.se/dealer_privatelist.asp?sortorder=fabrication&dealerid=177');
INSERT INTO store_params VALUES (12600, 'image_extra', '-');
INSERT INTO store_params VALUES (12629, 'infopage_no_ads', 'http://
');
INSERT INTO store_params VALUES (12629, 'image_extra', '-');
INSERT INTO store_params VALUES (12630, 'import_id', 'jezyk');
INSERT INTO store_params VALUES (12630, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (12630, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=jezyk&sort=letter&look=blocket');
INSERT INTO store_params VALUES (12630, 'image_extra', '-');
INSERT INTO store_params VALUES (12631, 'infopage_no_ads', 'http://www.tradecenter.se/dealers/bilfritid/husbilar.asp ');
INSERT INTO store_params VALUES (12631, 'image_extra', '-');
INSERT INTO store_params VALUES (12634, 'infopage_no_ads', 'http://www.begmc.se/begagnat.asp');
INSERT INTO store_params VALUES (12634, 'image_extra', '-');
INSERT INTO store_params VALUES (12638, 'import_id', 'mickesvxomc');
INSERT INTO store_params VALUES (12638, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (12638, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=mickesvxomc&sort=letter&look=blocket');
INSERT INTO store_params VALUES (12638, 'image_extra', '-');
INSERT INTO store_params VALUES (12639, 'import_id', 'dinbilhammarby');
INSERT INTO store_params VALUES (12639, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (12639, 'infopage_no_ads', 'http://www.dinbil.se');
INSERT INTO store_params VALUES (12639, 'image_extra', '-');
INSERT INTO store_params VALUES (12654, 'import_id', 'nyaallglasorebro');
INSERT INTO store_params VALUES (12654, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (12654, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=nyaallglasorebro&sort=letter&look=blocket');
INSERT INTO store_params VALUES (12654, 'image_extra', '-');
INSERT INTO store_params VALUES (12656, 'import_id', 'carlqvist');
INSERT INTO store_params VALUES (12656, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (12656, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=carlqvist&sort=letter&look=blocket');
INSERT INTO store_params VALUES (12656, 'image_extra', '-');
INSERT INTO store_params VALUES (14032, 'import_id', 'automixbjuv');
INSERT INTO store_params VALUES (14032, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (14032, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=automixbjuv&sort=letter&look=blocket');
INSERT INTO store_params VALUES (14032, 'image_extra', '-');
INSERT INTO store_params VALUES (12659, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=skeppargardensbilpukavik&sort=letter&look=blocket');
INSERT INTO store_params VALUES (12659, 'image_extra', '-');
INSERT INTO store_params VALUES (12661, 'import_id', 'importlagret');
INSERT INTO store_params VALUES (12661, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (12661, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=importlagret&sort=letter&look=blocket');
INSERT INTO store_params VALUES (12661, 'image_extra', '-');
INSERT INTO store_params VALUES (12662, 'infopage_no_ads', '-');
INSERT INTO store_params VALUES (12662, 'image_extra', '-');
INSERT INTO store_params VALUES (13884, 'import_id', '3xlarsson');
INSERT INTO store_params VALUES (13884, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=3xlarsson&sort=letter&look=blocket');
INSERT INTO store_params VALUES (13884, 'image_extra', '-');
INSERT INTO store_params VALUES (12663, 'import_id', 'bilparken-svedala');
INSERT INTO store_params VALUES (12663, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (12663, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=bilparken-svedala&look=blocket');
INSERT INTO store_params VALUES (12663, 'image_extra', '-');
INSERT INTO store_params VALUES (14475, 'import_id', 'bilhammarby');
INSERT INTO store_params VALUES (14475, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (14475, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=bilhammarby&sort=letter&look=blocket');
INSERT INTO store_params VALUES (14475, 'image_extra', '-');

INSERT INTO store_params VALUES (14476, 'import_id', 'knallemotorboras');
INSERT INTO store_params VALUES (14476, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (14476, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=knallemotorboras&sort=letter&look=blocket');
INSERT INTO store_params VALUES (14476, 'image_extra', '-');
INSERT INTO store_params VALUES (12672, 'import_id', 'bilforumorebro');
INSERT INTO store_params VALUES (12672, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (12672, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=bilforumorebro&sort=letter&look=blocket');
INSERT INTO store_params VALUES (12672, 'image_extra', '-');
INSERT INTO store_params VALUES (12674, 'import_id', 'yamahacenterhbgmc');
INSERT INTO store_params VALUES (12674, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (12674, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=yamahacenterhbgmc&sort=letter&look=blocket');
INSERT INTO store_params VALUES (12674, 'image_extra', '-');
INSERT INTO store_params VALUES (12675, 'import_id', 'matsbringasab');
INSERT INTO store_params VALUES (12675, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (12675, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=matsbringasab&sort=letter&look=blocket');
INSERT INTO store_params VALUES (12675, 'image_extra', '-');
INSERT INTO store_params VALUES (14427, 'import_id', 'bomanoson');
INSERT INTO store_params VALUES (14427, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (14427, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=bomanoson&sort=letter&look=blocket');
INSERT INTO store_params VALUES (14427, 'image_extra', '-');
INSERT INTO store_params VALUES (12727, 'import_id', 'larseneland');
INSERT INTO store_params VALUES (12727, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (12727, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=larseneland&sort=letter&look=blocket');
INSERT INTO store_params VALUES (12727, 'image_extra', '-');
INSERT INTO store_params VALUES (12729, 'import_id', 'orebronettobilar');
INSERT INTO store_params VALUES (12729, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (12729, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=orebronettobilar&sort=letter&look=blocket');
INSERT INTO store_params VALUES (12729, 'image_extra', '-');
INSERT INTO store_params VALUES (12734, 'import_id', 'exclusivecars');
INSERT INTO store_params VALUES (12734, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (12734, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=exclusivecars&look=blocket&carsperpage=50');
INSERT INTO store_params VALUES (12734, 'image_extra', '-');
INSERT INTO store_params VALUES (12741, 'import_id', 'arvidstorpsbil');
INSERT INTO store_params VALUES (12741, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (12741, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=arvidstorpsbil&sort=letter&look=blocket');
INSERT INTO store_params VALUES (12741, 'image_extra', '-');
INSERT INTO store_params VALUES (12864, 'import_id', 'attentohbg');
INSERT INTO store_params VALUES (12864, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (12864, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=attentohbg&sort=letter&look=blocket');
INSERT INTO store_params VALUES (12864, 'image_extra', '-');
INSERT INTO store_params VALUES (12744, 'import_id', 'ostvastbilvarberg');
INSERT INTO store_params VALUES (12744, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (12744, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=ostvastbilvarberg&sort=letter&look=blocket');
INSERT INTO store_params VALUES (12744, 'image_extra', '-');
INSERT INTO store_params VALUES (12745, 'import_id', 'stenbergnykop');
INSERT INTO store_params VALUES (12745, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (12745, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=stenbergnykop&sort=letter&look=blocket');
INSERT INTO store_params VALUES (12745, 'image_extra', '-');
INSERT INTO store_params VALUES (13847, 'import_id', 'citybilbromma');
INSERT INTO store_params VALUES (13847, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (13847, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=citybilbromma&sort=letter&look=blocket');
INSERT INTO store_params VALUES (13847, 'image_extra', '-');
INSERT INTO store_params VALUES (13483, 'import_id', 'atautobromma');
INSERT INTO store_params VALUES (13483, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (13483, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=atautobromma&sort=letter&look=blocket');
INSERT INTO store_params VALUES (13483, 'image_extra', '-');
INSERT INTO store_params VALUES (13484, 'infopage_no_ads', 'http://www.ostcamp.se/index.php?mainCatId=1&menuCatId=27&layoutId=1');
INSERT INTO store_params VALUES (13484, 'image_extra', '-');
INSERT INTO store_params VALUES (12753, 'import_id', 'asplunds');
INSERT INTO store_params VALUES (12753, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (12753, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=asplunds&sort=letter&look=blocket
');
INSERT INTO store_params VALUES (12753, 'image_extra', '-');
INSERT INTO store_params VALUES (12754, 'import_id', 'lofstromsbilar');
INSERT INTO store_params VALUES (12754, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (12754, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=lofstromsbilar&sort=letter&look=blocket');
INSERT INTO store_params VALUES (12754, 'image_extra', '-');
INSERT INTO store_params VALUES (12755, 'infopage_no_ads', 'http://www.kabe.se/tenhult/begagnat.html');
INSERT INTO store_params VALUES (12755, 'image_extra', '-');
INSERT INTO store_params VALUES (12756, 'import_id', 'autoimpexvellinge');
INSERT INTO store_params VALUES (12756, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (12756, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=autoimpexvellinge&sort=letter&look=blocket');
INSERT INTO store_params VALUES (12756, 'image_extra', '-');
INSERT INTO store_params VALUES (12757, 'import_id', 'piasgbg');
INSERT INTO store_params VALUES (12757, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (12757, 'infopage_no_ads', '-');
INSERT INTO store_params VALUES (12757, 'image_extra', '-');
INSERT INTO store_params VALUES (14190, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=callismaab&sort=letter&look=blocket');
INSERT INTO store_params VALUES (14190, 'image_extra', '-');
INSERT INTO store_params VALUES (12800, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=bilmixskelleftea&sort=letter&look=blocket');
INSERT INTO store_params VALUES (12800, 'image_extra', '-');
INSERT INTO store_params VALUES (12809, 'import_id', 'lundbybil');
INSERT INTO store_params VALUES (12809, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (12809, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=lundbybil&sort=letter&look=blocket');
INSERT INTO store_params VALUES (12809, 'image_extra', '-');
INSERT INTO store_params VALUES (12828, 'import_id', 'ebnbil');
INSERT INTO store_params VALUES (12828, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (12828, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=ebnbil&sort=letter&look=blocket');
INSERT INTO store_params VALUES (12828, 'image_extra', '-');
INSERT INTO store_params VALUES (12829, 'import_id', 'tisnarbil');
INSERT INTO store_params VALUES (12829, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (12829, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=tisnarbil&sort=letter&look=blocket');
INSERT INTO store_params VALUES (12829, 'image_extra', '-');
INSERT INTO store_params VALUES (13625, 'import_id', 'twinmoterssalbohed');
INSERT INTO store_params VALUES (13625, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (13625, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=twinmoterssalbohed&sort=letter&look=blocket');
INSERT INTO store_params VALUES (13625, 'image_extra', '-');
INSERT INTO store_params VALUES (12834, 'infopage_no_ads', 'http://www.erikssonmarine.com/forsale.jsp');
INSERT INTO store_params VALUES (12834, 'image_extra', '-');
INSERT INTO store_params VALUES (12836, 'import_id', 'hurtigskungalv');
INSERT INTO store_params VALUES (12836, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (12836, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=hurtigskungalv&sort=letter&look=blocket');
INSERT INTO store_params VALUES (12836, 'image_extra', '-');
INSERT INTO store_params VALUES (12837, 'import_id', 'bilgardenang');
INSERT INTO store_params VALUES (12837, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (12837, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=bilgardenang&sort=letter&look=blocket');
INSERT INTO store_params VALUES (12837, 'image_extra', '-');
INSERT INTO store_params VALUES (12838, 'import_id', 'rabergsparsor');
INSERT INTO store_params VALUES (12838, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (12838, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=rabergsparsor&sort=letter&look=blocket');
INSERT INTO store_params VALUES (12838, 'image_extra', '-');
INSERT INTO store_params VALUES (12839, 'import_id', 'autohaushbg');
INSERT INTO store_params VALUES (12839, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (12839, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=autohaushbg&sort=letter&look=blocket');
INSERT INTO store_params VALUES (12839, 'image_extra', '-');
INSERT INTO store_params VALUES (12841, 'import_id', 'lindqvistbilkoping');
INSERT INTO store_params VALUES (12841, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (12841, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=lindqvistbilkoping&sort=letter&look=blocket');
INSERT INTO store_params VALUES (12841, 'image_extra', '-');
INSERT INTO store_params VALUES (12842, 'import_id', 'bilhornansolna');
INSERT INTO store_params VALUES (12842, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (12842, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=bilhornansolna&sort=letter&look=blocket');
INSERT INTO store_params VALUES (12842, 'image_extra', '-');
INSERT INTO store_params VALUES (12985, 'import_id', 'becker');
INSERT INTO store_params VALUES (12985, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (12985, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=becker&sort=letter&look=blocket');
INSERT INTO store_params VALUES (12985, 'image_extra', '-');
INSERT INTO store_params VALUES (12847, 'import_id', 'eklunds');
INSERT INTO store_params VALUES (12847, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (12847, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=eklunds&sort=letter&look=blocket');
INSERT INTO store_params VALUES (12847, 'image_extra', '-');
INSERT INTO store_params VALUES (12849, 'import_id', 'rkbiltun');
INSERT INTO store_params VALUES (12849, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (12849, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=rkbiltun&sort=letter&look=blocket');
INSERT INTO store_params VALUES (12849, 'image_extra', '-');
INSERT INTO store_params VALUES (12850, 'import_id', 'magnustorngrenboras');
INSERT INTO store_params VALUES (12850, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (12850, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=magnustorngrenboras&sort=letter&look=blocket');
INSERT INTO store_params VALUES (12850, 'image_extra', '-');
INSERT INTO store_params VALUES (12852, 'import_id', 'bilbroddskanninge');
INSERT INTO store_params VALUES (12852, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (12852, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=bilbroddskanninge&sort=letter&look=blocket');
INSERT INTO store_params VALUES (12852, 'image_extra', '-');
INSERT INTO store_params VALUES (12854, 'import_id', 'orrekullagbg');
INSERT INTO store_params VALUES (12854, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (12854, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=orrekullagbg&sort=letter&look=blocket');
INSERT INTO store_params VALUES (12854, 'image_extra', '-');
INSERT INTO store_params VALUES (12855, 'import_id', 'hansperssonhedemora');
INSERT INTO store_params VALUES (12855, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (12855, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=hansperssonhedemora&sort=letter&look=blocket');
INSERT INTO store_params VALUES (12855, 'image_extra', '-');
INSERT INTO store_params VALUES (12856, 'import_id', 'hansperssonfagersta');
INSERT INTO store_params VALUES (12856, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (12856, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=hansperssonfagersta&sort=letter&look=blocket');
INSERT INTO store_params VALUES (12856, 'image_extra', '-');
INSERT INTO store_params VALUES (12857, 'import_id', 'hansperssonhallstahammar');
INSERT INTO store_params VALUES (12857, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (12857, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=hansperssonhallstahammar&sort=letter&look=blocket');
INSERT INTO store_params VALUES (12857, 'image_extra', '-');
INSERT INTO store_params VALUES (12858, 'import_id', 'hansperssonkoping');
INSERT INTO store_params VALUES (12858, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (12858, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=hansperssonkoping&sort=letter&look=blocket');
INSERT INTO store_params VALUES (12858, 'image_extra', '-');
INSERT INTO store_params VALUES (12859, 'import_id', 'hansperssonsala');
INSERT INTO store_params VALUES (12859, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (12859, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=hansperssonsala&sort=letter&look=blocket');
INSERT INTO store_params VALUES (12859, 'image_extra', '-');
INSERT INTO store_params VALUES (12860, 'import_id', 'hansperssonvasteras');
INSERT INTO store_params VALUES (12860, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (12860, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=hansperssonvasteras&sort=letter&look=blocket');
INSERT INTO store_params VALUES (12860, 'image_extra', '-');
INSERT INTO store_params VALUES (12861, 'import_id', 'biliaskovde');
INSERT INTO store_params VALUES (12861, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (12861, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=biliaskovde&sort=letter&look=blocket');
INSERT INTO store_params VALUES (12861, 'image_extra', '-');
INSERT INTO store_params VALUES (12862, 'import_id', 'citybilfalun');
INSERT INTO store_params VALUES (12862, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (12862, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=citybilfalun&sort=letter&look=blocket');
INSERT INTO store_params VALUES (12862, 'image_extra', '-');
INSERT INTO store_params VALUES (12863, 'import_id', 'citybilborlange');
INSERT INTO store_params VALUES (12863, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (12863, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=citybilborlange&sort=letter&look=blocket');
INSERT INTO store_params VALUES (12863, 'image_extra', '-');
INSERT INTO store_params VALUES (12865, 'import_id', 'bilnilssoneng');
INSERT INTO store_params VALUES (12865, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (12865, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=bilnilssoneng&sort=letter&look=blocket');
INSERT INTO store_params VALUES (12865, 'image_extra', '-');
INSERT INTO store_params VALUES (12866, 'import_id', 'strandellshbg');
INSERT INTO store_params VALUES (12866, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (12866, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=strandellshbg&sort=letter&look=blocket');
INSERT INTO store_params VALUES (12866, 'image_extra', '-');
INSERT INTO store_params VALUES (12867, 'import_id', 'stahaninge');
INSERT INTO store_params VALUES (12867, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (12867, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=stahaninge&sort=letter&look=blocket');
INSERT INTO store_params VALUES (12867, 'image_extra', '-');
INSERT INTO store_params VALUES (12868, 'import_id', 'danielsbilgbg');
INSERT INTO store_params VALUES (12868, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (12868, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=danielsbilgbg&sort=letter&look=blocket');
INSERT INTO store_params VALUES (12868, 'image_extra', '-');
INSERT INTO store_params VALUES (13537, 'infopage_no_ads', '-');
INSERT INTO store_params VALUES (13537, 'image_extra', '-');
INSERT INTO store_params VALUES (12871, 'import_id', 'grondalsbilsthlm');
INSERT INTO store_params VALUES (12871, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (12871, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=grondalsbilsthlm&sort=letter&look=blocket');
INSERT INTO store_params VALUES (12871, 'image_extra', '-');
INSERT INTO store_params VALUES (12873, 'import_id', 'bilbolagettimra');
INSERT INTO store_params VALUES (12873, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (12873, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=bilbolagettimra&sort=letter&look=blocket');
INSERT INTO store_params VALUES (12873, 'image_extra', '-');
INSERT INTO store_params VALUES (12874, 'import_id', 'aredalenbiljarpen');
INSERT INTO store_params VALUES (12874, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (12874, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=aredalenbiljarpen&sort=letter&look=blocket');
INSERT INTO store_params VALUES (12874, 'image_extra', '-');
INSERT INTO store_params VALUES (14200, 'import_id', 'forsberghyssna');
INSERT INTO store_params VALUES (14200, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (14200, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=forsberghyssna&sort=letter&look=blocket');
INSERT INTO store_params VALUES (14200, 'image_extra', '-');
INSERT INTO store_params VALUES (12879, 'import_id', 'forenademotormalmo');
INSERT INTO store_params VALUES (12879, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (12879, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=forenademotormalmo&sort=letter&look=blocket');
INSERT INTO store_params VALUES (12879, 'image_extra', '-');
INSERT INTO store_params VALUES (13037, 'import_id', 'blennows');
INSERT INTO store_params VALUES (13037, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (13037, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=blennows&sort=letter&look=blocket');
INSERT INTO store_params VALUES (13037, 'image_extra', '-');
INSERT INTO store_params VALUES (12881, 'import_id', 'bilcnykop');
INSERT INTO store_params VALUES (12881, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (12881, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=bilcnykop&sort=letter&look=blocket');
INSERT INTO store_params VALUES (12881, 'image_extra', '-');
INSERT INTO store_params VALUES (12882, 'import_id', 'seracoetuna');
INSERT INTO store_params VALUES (12882, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (12882, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=seracoetuna&sort=letter&look=blocket');
INSERT INTO store_params VALUES (12882, 'image_extra', '-');

INSERT INTO store_params VALUES (12883, 'import_id', 'ma_8D6A3E22-lammhult');
INSERT INTO store_params VALUES (12883, 'import_partner', 'mascus');
INSERT INTO store_params VALUES (12883, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=seracostrangnas&sort=letter&look=blocket');
INSERT INTO store_params VALUES (12883, 'image_extra', '-');
INSERT INTO store_params VALUES (12883, 'import_type', '4');


INSERT INTO store_params VALUES (12884, 'import_id', 'motorcentrumkatrineholm');
INSERT INTO store_params VALUES (12884, 'import_partner', 'bytbil');
INSERT INTO store_params VALUES (12884, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=motorcentrumkatrineholm&sort=letter&look=blocket');
INSERT INTO store_params VALUES (12884, 'image_extra', '-');
INSERT INTO store_params VALUES (3946, 'import_id', 'ab_438');
INSERT INTO store_params VALUES (3946, 'import_partner', 'annonsborsen');
INSERT INTO store_params VALUES (3946, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=ettsvsolvesborg&sort=letter&look=hlista_2002');
INSERT INTO store_params VALUES (3946, 'image_extra', '-');
INSERT INTO store_params VALUES (20000, 'import_id', 'ab_465');
INSERT INTO store_params VALUES (20000, 'import_partner', 'annonsborsen');
INSERT INTO store_params VALUES (20000, 'infopage_no_ads', 'http://www.bytbil.com/select_car.cgi?seller=svbilgrupp&sort=letter&look=blocket');
INSERT INTO store_params VALUES (20000, 'image_extra', 'mrf_logo_transp.gif');
INSERT INTO store_params VALUES (14661, 'import_id', 'ab_463');
INSERT INTO store_params VALUES (14661, 'import_partner', 'annonsborsen');
INSERT INTO store_params VALUES (14661, 'infopage_no_ads', 'http://www.arvikabilsmide.com
');
INSERT INTO store_params VALUES (14661, 'image_extra', '-');
INSERT INTO store_params VALUES (14661, 'username', 'patrik@blocket.se');
INSERT INTO store_params VALUES (14661, 'passwd', '4r5Jbv2Xkdu9VJt1272d9331e3cd8f3322b16dfb6d0feaa6e97a72c8');


INSERT INTO store_params VALUES (14662, 'import_id', 'ab_463');
INSERT INTO store_params VALUES (14662, 'import_partner', 'annonsborsen');
INSERT INTO store_params VALUES (14662, 'infopage_no_ads', 'http://www.arvikabilsmide.com');
INSERT INTO store_params VALUES (14662, 'import_type', '2');

INSERT INTO store_params VALUES (14662, 'image_extra', '-');

INSERT INTO store_users VALUES (14661, 'patrik@blocket.se');
INSERT INTO store_users VALUES (14662, 'blocket_kronofogden.se@blocket.se');
INSERT INTO store_users VALUES (1222, 'svara.inte@blocket.se');
INSERT INTO store_users VALUES (12882, 'svara.inte@blocket.se');
INSERT INTO store_users VALUES (12883, 'svara.inte@blocket.se');
INSERT INTO store_users VALUES (12884, 'svara.inte@blocket.se');


INSERT INTO store_users VALUES (12799, 'bilmix_telia.com@blocket.se');
INSERT INTO store_users VALUES (3067, 'info_bildax.se@blocket.se');
INSERT INTO store_users VALUES (2552, 'info_g-grahn.se@blocket.se');
INSERT INTO store_users VALUES (13815, 'toyotagavle_toyota.se@blocket.se');
INSERT INTO store_users VALUES (13302, 'klas_ksbil.com@blocket.se');
INSERT INTO store_users VALUES (10740, 'info_tagagaraget.nu@blocket.se');
INSERT INTO store_users VALUES (4083, 'oddsbilar_swipnet.se@blocket.se');
INSERT INTO store_users VALUES (4081, 'nordma_msn.com@blocket.se');
INSERT INTO store_users VALUES (3056, 'kvalitetsbilar_telia.com@blocket.se');
INSERT INTO store_users VALUES (2031, 'sales.bilomarin_carlsborg.net@blocket.se');
INSERT INTO store_users VALUES (4077, 'info_a-line.se@blocket.se');
INSERT INTO store_users VALUES (3051, 'info_autovation.se@blocket.se');
INSERT INTO store_users VALUES (10729, 'mc_biketrollhattan.se@blocket.se');
INSERT INTO store_users VALUES (4072, 'bil2000_chello.se@blocket.se');
INSERT INTO store_users VALUES (4069, 'info_rehnstromsbil.se@blocket.se');
INSERT INTO store_users VALUES (13284, 'mats_autoclassica.com@blocket.se');
INSERT INTO store_users VALUES (13794, 'info_oloflarssonbil.se@blocket.se');
INSERT INTO store_users VALUES (3553, 'j.timo_telia.com@blocket.se');
INSERT INTO store_users VALUES (4065, 'info_autocarimport.nu@blocket.se');
INSERT INTO store_users VALUES (4064, 'info_ubbo.se@blocket.se');
INSERT INTO store_users VALUES (10720, 'trendbil_hotmail.com@blocket.se');
INSERT INTO store_users VALUES (2524, 'anders.ericson_olofericson.se@blocket.se');
INSERT INTO store_users VALUES (10715, 'info_swedenmotorshop.com@blocket.se');
INSERT INTO store_users VALUES (10713, 'gunnar.anderssonbilab_telia.com@blocket.se');
INSERT INTO store_users VALUES (13272, 'info_landrinsbil.se@blocket.se');
INSERT INTO store_users VALUES (4054, 'skanebil_skanebil.se@blocket.se');
INSERT INTO store_users VALUES (4053, 'mail_bikeport.se@blocket.se');
INSERT INTO store_users VALUES (12757, 'pias.bilar_home.se@blocket.se');
INSERT INTO store_users VALUES (12756, 'info_autoperformance.se@blocket.se');
INSERT INTO store_users VALUES (2515, 'info_hakansbilar.se@blocket.se');
INSERT INTO store_users VALUES (11730, 'rainer.lorke_telia.com@blocket.se');
INSERT INTO store_users VALUES (12754, 'hb.lofstroms_swipnet.se@blocket.se');
INSERT INTO store_users VALUES (2769, 'kontakt_arnobil.se@blocket.se');
INSERT INTO store_users VALUES (4049, 'info_transportcenter.se@blocket.se');
INSERT INTO store_users VALUES (12753, 'bengt_asplundsbil.se@blocket.se');
INSERT INTO store_users VALUES (14032, 'automix_automix.se@blocket.se');
INSERT INTO store_users VALUES (1231, 'carimport_telia.com@blocket.se');
INSERT INTO store_users VALUES (1484, 'variantbil_telia.com@blocket.se');
INSERT INTO store_users VALUES (10956, 'sm-bilar_swipnet.se@blocket.se');
INSERT INTO store_users VALUES (1993, 'mattias_bilkompaniet.nu@blocket.se');
INSERT INTO store_users VALUES (12745, 'matts.stenberg_stenbergsbil.com@blocket.se');
INSERT INTO store_users VALUES (10952, 'info_vincents.se@blocket.se');
INSERT INTO store_users VALUES (12744, 'bilcenterivarberg_hotmail.com@blocket.se');
INSERT INTO store_users VALUES (3270, 'bilinvest_telia.com@blocket.se');
INSERT INTO store_users VALUES (4037, 'jan.gustafs_spray.se@blocket.se');
INSERT INTO store_users VALUES (12485, 'info_eurogarage.com@blocket.se');
INSERT INTO store_users VALUES (12741, 'info_arvidstorpsbil.com@blocket.se');
INSERT INTO store_users VALUES (13765, 'bilar_vwcustomspeed.com@blocket.se');
INSERT INTO store_users VALUES (3779, 'info_superbike.se@blocket.se');
INSERT INTO store_users VALUES (4035, 'info_kcbil-truck.se@blocket.se');
INSERT INTO store_users VALUES (2242, 'bilar_bilhallenmjolby.se@blocket.se');
INSERT INTO store_users VALUES (4034, 'bo.faltin_swipnet.se@blocket.se');
INSERT INTO store_users VALUES (4033, 'marcus.christell_skivarp.se@blocket.se');
INSERT INTO store_users VALUES (12736, 'info_palmbilfirma.se@blocket.se');
INSERT INTO store_users VALUES (12734, 'info_exclusive.se@blocket.se');
INSERT INTO store_users VALUES (4029, 'info_bilartillsalu.se@blocket.se');
INSERT INTO store_users VALUES (4028, 'lundavagensbil_telia.com@blocket.se');
INSERT INTO store_users VALUES (13243, 'info_agbilar.com@blocket.se');
INSERT INTO store_users VALUES (13499, 'bertan.camuz_peugeotkista.se@blocket.se');
INSERT INTO store_users VALUES (3002, 'info_rolfgperssonbil.se@blocket.se');
INSERT INTO store_users VALUES (4026, 'info_msabilcenter.se@blocket.se');
INSERT INTO store_users VALUES (12729, 'nettobilar_telia.com@blocket.se');
INSERT INTO store_users VALUES (12985, 'info_beckersbil.se@blocket.se');
INSERT INTO store_users VALUES (4024, 'ask_staffanstorpsbilcity.com@blocket.se');
INSERT INTO store_users VALUES (4023, 'info_bilexpo.nu@blocket.se');
INSERT INTO store_users VALUES (12727, 'info1_eurocars.se@blocket.se');
INSERT INTO store_users VALUES (2742, 'stefan.ohman_mbox301.swipnet.se@blocket.se');
INSERT INTO store_users VALUES (4020, 'zoran_flyingebil.com@blocket.se');
INSERT INTO store_users VALUES (1970, 'claes_harenebilab.se@blocket.se');
INSERT INTO store_users VALUES (14258, 'marten.bil_telia.com@blocket.se');
INSERT INTO store_users VALUES (4015, 'nk.bilmarknad_telia.com@blocket.se');
INSERT INTO store_users VALUES (4014, 'cityautomobil_yahoo.se@blocket.se');
INSERT INTO store_users VALUES (10668, 'info_dahlqvist-bil.com@blocket.se');
INSERT INTO store_users VALUES (4011, 'bromsplansbil_telia.com@blocket.se');
INSERT INTO store_users VALUES (13483, 'info_autotrader.se@blocket.se');
INSERT INTO store_users VALUES (3752, 'info_deltamc.se@blocket.se');
INSERT INTO store_users VALUES (13480, 'lagprisbilar.s_telia.com@blocket.se');
INSERT INTO store_users VALUES (3751, 'info_guzzicenter.se@blocket.se');
INSERT INTO store_users VALUES (4007, 'info_lingvalls.se@blocket.se');
INSERT INTO store_users VALUES (3493, 'ulf_sunderbybil.se@blocket.se');
INSERT INTO store_users VALUES (1956, 'svenska.e_telia.com@blocket.se');
INSERT INTO store_users VALUES (4004, 'info_bilomobil.se@blocket.se');
INSERT INTO store_users VALUES (4003, 'info_ride.se@blocket.se');
INSERT INTO store_users VALUES (162, 'fittjabil_hotmail.com@blocket.se');
INSERT INTO store_users VALUES (1183, 'vastbobilcenter_telia.com@blocket.se');
INSERT INTO store_users VALUES (14239, 'c.nilander_telia.com@blocket.se');
INSERT INTO store_users VALUES (1181, 'info_mollefors.com@blocket.se');
INSERT INTO store_users VALUES (12444, 'janne.persson_motorhalland.se@blocket.se');
INSERT INTO store_users VALUES (3995, 'hans.mnmotor_telia.com@blocket.se');
INSERT INTO store_users VALUES (14235, 'gibilab_msn.com@blocket.se');
INSERT INTO store_users VALUES (3994, 'le.zackrisson_telia.com@blocket.se');
INSERT INTO store_users VALUES (1177, 'fakiren_hotmail.com@blocket.se');
INSERT INTO store_users VALUES (3225, 'roger_bilhuset.se@blocket.se');
INSERT INTO store_users VALUES (12441, 'info_lmautoservice.com@blocket.se');
INSERT INTO store_users VALUES (12440, 'marten.persson_bilmansson.se@blocket.se');
INSERT INTO store_users VALUES (3986, 'tomas.kaakinen_kaakinenbil.se@blocket.se');
INSERT INTO store_users VALUES (913, 'skanebil_skanebil.se@blocket.se');
INSERT INTO store_users VALUES (13968, 'kontakt_smygehamnsbil.com@blocket.se');
INSERT INTO store_users VALUES (3982, 'lainvest_algonet.se@blocket.se');
INSERT INTO store_users VALUES (3981, 'helsingborg_bilformedlingen.com@blocket.se');
INSERT INTO store_users VALUES (13709, 'christian.svensson_kristianstadsautomobil.se@blocket.se');
INSERT INTO store_users VALUES (3980, 'micke.sjoberg_telia.com@blocket.se');
INSERT INTO store_users VALUES (12428, 'per.larssons_swipnet.se@blocket.se');
INSERT INTO store_users VALUES (14476, 'knallelandsbilcenter_hotmail.com@blocket.se');
INSERT INTO store_users VALUES (2187, 'david.burman_bilbyter.se@blocket.se');
INSERT INTO store_users VALUES (2443, 'info_specialbilar.com@blocket.se');
INSERT INTO store_users VALUES (14475, 'lars.lofgren_artsabil.com@blocket.se');
INSERT INTO store_users VALUES (2953, '-');
INSERT INTO store_users VALUES (13192, 'info_bogstedtbil.se@blocket.se');
INSERT INTO store_users VALUES (3975, 'info_bilaffarenlulea.com@blocket.se');
INSERT INTO store_users VALUES (3974, 'info_granuddensbil.se@blocket.se');
INSERT INTO store_users VALUES (14469, 'thornilssonbil_sjobo.nu@blocket.se');
INSERT INTO store_users VALUES (12420, 'ambil_ambil.nu@blocket.se');
INSERT INTO store_users VALUES (12419, 'info_laholmsbil.nu@blocket.se');
INSERT INTO store_users VALUES (12675, 'matsbringas_hotmail.com@blocket.se');
INSERT INTO store_users VALUES (12418, 'info_halmstadbilcity.nu@blocket.se');
INSERT INTO store_users VALUES (12674, 'info.hbg_yamahacenter.com@blocket.se');
INSERT INTO store_users VALUES (2432, 'info_hallstenbil.nu@blocket.se');
INSERT INTO store_users VALUES (2688, 'peter_fordbilar.com@blocket.se');
INSERT INTO store_users VALUES (3968, 'daniel.rockenson_comhem.se@blocket.se');
INSERT INTO store_users VALUES (12672, 'info_bilforum.se@blocket.se');
INSERT INTO store_users VALUES (3455, 'anders.hellgren_hellgrensbil.se@blocket.se');
INSERT INTO store_users VALUES (14463, 'info_goingebil.se@blocket.se');
INSERT INTO store_users VALUES (2430, 'andreas.bilgallerian.k_telia.com@blocket.se');
INSERT INTO store_users VALUES (13693, 'per.pandel_bilmetro.se@blocket.se');
INSERT INTO store_users VALUES (14461, 'hoting_ivarsbil.se@blocket.se');
INSERT INTO store_users VALUES (3452, 'hakan_gustafssonsbil.se@blocket.se');
INSERT INTO store_users VALUES (13692, 'stefan.jonsson_bilmetro.se@blocket.se');
INSERT INTO store_users VALUES (123, 'bilborgen_bilborgen.se@blocket.se');
INSERT INTO store_users VALUES (3962, 'info_codat.se@blocket.se');
INSERT INTO store_users VALUES (9848, 'guy_fordbilgruppen.com@blocket.se,petter_mazdabilgruppen.com@blocket.se');
INSERT INTO store_users VALUES (14200, 'info_forsbergsfritidscenter.se@blocket.se');
INSERT INTO store_users VALUES (12663, 'bilparken_bilparken.se@blocket.se');
INSERT INTO store_users VALUES (1142, 'lars.larsson_laholmsbilservice.se@blocket.se');
INSERT INTO store_users VALUES (2677, 'bilmagasinet_telia.com@blocket.se');
INSERT INTO store_users VALUES (12661, 'info_importlagret.com@blocket.se');
INSERT INTO store_users VALUES (14194, 'kjell_dintransportbil.se@blocket.se');
INSERT INTO store_users VALUES (14193, 'almbybil_hotmail.com@blocket.se');
INSERT INTO store_users VALUES (3952, 'ucar_telia.com@blocket.se');
INSERT INTO store_users VALUES (12656, 'christer.johansson_carlqvistbil.se@blocket.se');
INSERT INTO store_users VALUES (14192, 'per_hagebladbil.com@blocket.se');
INSERT INTO store_users VALUES (3951, 'engelholm_bilformedlingen.com@blocket.se');
INSERT INTO store_users VALUES (14191, 'kw_bilpunktenboras.se@blocket.se');
INSERT INTO store_users VALUES (3950, 'stockholm_bilformedlingen.com@blocket.se');
INSERT INTO store_users VALUES (12654, 'rifum100_hotmail.com@blocket.se');
INSERT INTO store_users VALUES (2925, 'info_hansperssonbil.se@blocket.se');
INSERT INTO store_users VALUES (12397, 'bilsalong_olofssonauto.com@blocket.se');
INSERT INTO store_users VALUES (12652, '-');
INSERT INTO store_users VALUES (3947, 'hallsbil_algonet.se@blocket.se');
INSERT INTO store_users VALUES (14443, 'rumabil_telia.com@blocket.se');
INSERT INTO store_users VALUES (2920, 'carnila_jobbet.utfors.se@blocket.se');
INSERT INTO store_users VALUES (12648, 'info.levinsbil_telia.com@blocket.se');
INSERT INTO store_users VALUES (14184, 'lagprisbilar_spray.se@blocket.se');
INSERT INTO store_users VALUES (14440, 'johan_varbergbil-depot.se@blocket.se');
INSERT INTO store_users VALUES (1895, 'info_7hbil.se@blocket.se');
INSERT INTO store_users VALUES (3175, 'robert_sellmans.se@blocket.se');
INSERT INTO store_users VALUES (2662, 'bilhusetorebro_telia.com@blocket.se');
INSERT INTO store_users VALUES (14438, 'info_lada.nu@blocket.se');
INSERT INTO store_users VALUES (3941, 'glennon_telia.com@blocket.se');
INSERT INTO store_users VALUES (1380, 'info_bekobil.se@blocket.se');
INSERT INTO store_users VALUES (3170, 'noren.skyttner_ljusdal.se@blocket.se');
INSERT INTO store_users VALUES (12642, 'unikbilorebro_hotmail.com@blocket.se');
INSERT INTO store_users VALUES (3935, 'info_stibix.se@blocket.se');
INSERT INTO store_users VALUES (12639, 'beg.sthlmsoder_dinbil.se@blocket.se');
INSERT INTO store_users VALUES (12638, 'info_mickesmotor.se@blocket.se');
INSERT INTO store_users VALUES (2652, 'e-mail_mariebergbil.se@blocket.se');
INSERT INTO store_users VALUES (1627, 'bilar_automan.se@blocket.se');
INSERT INTO store_users VALUES (2395, 'info_classic-garage.se@blocket.se');
INSERT INTO store_users VALUES (14427, 'info_bomanoson.se@blocket.se');
INSERT INTO store_users VALUES (2650, 'fredrick.hafstrom_jonssonbil.se@blocket.se');
INSERT INTO store_users VALUES (14426, 'info_motosalongen.com@blocket.se');
INSERT INTO store_users VALUES (88, 'info_asobilhall.se@blocket.se');
INSERT INTO store_users VALUES (2135, 'vitala-bil_telia.com@blocket.se');
INSERT INTO store_users VALUES (3414, 'autobla_autobla.se@blocket.se');
INSERT INTO store_users VALUES (12630, 'z.jezyk_telia.com@blocket.se');
INSERT INTO store_users VALUES (1109, 'sannarpsbil_telia.com@blocket.se');
INSERT INTO store_users VALUES (2643, 'jonas_bilpunkten.com@blocket.se');
INSERT INTO store_users VALUES (3923, 'h-d_olofssonauto.com@blocket.se');
INSERT INTO store_users VALUES (12881, 'info_bilcenter.com@blocket.se');
INSERT INTO store_users VALUES (12879, 'mikael.lundqvist_forenademotor.se@blocket.se');
INSERT INTO store_users VALUES (588, 'info_shhc.se@blocket.se');
INSERT INTO store_users VALUES (2892, 'info.liljeblad_liljebladbil.se@blocket.se');
INSERT INTO store_users VALUES (1099, 'info_klasenbil.se@blocket.se');
INSERT INTO store_users VALUES (12874, 'aredalenbil_telia.com@blocket.se');
INSERT INTO store_users VALUES (12873, 'johan.kempas_careta.nu@blocket.se');
INSERT INTO store_users VALUES (14153, 'jorgen_bilpoolen.se@blocket.se');
INSERT INTO store_users VALUES (13896, 'robert_sellmans.se@blocket.se');
INSERT INTO store_users VALUES (12871, 'info_grondalsbilmotor.se@blocket.se');
INSERT INTO store_users VALUES (12868, 'k.daniels_telia.com@blocket.se');
INSERT INTO store_users VALUES (12867, 'fa_bostream.nu@blocket.se');
INSERT INTO store_users VALUES (13635, 'malin.jakobsson_helmia.se@blocket.se');
INSERT INTO store_users VALUES (12866, 'info_strandells-motor.se@blocket.se');
INSERT INTO store_users VALUES (12865, 'kenneth.nilsson_bilnilsson.se@blocket.se');
INSERT INTO store_users VALUES (12864, 'frederick.enaholo_attentobil.se@blocket.se');
INSERT INTO store_users VALUES (1855, 'info_carblaze.com@blocket.se');
INSERT INTO store_users VALUES (12863, 'magnus_citybil.se@blocket.se');
INSERT INTO store_users VALUES (13631, 'christopher.forsmark_bilforumajbo.se@blocket.se');
INSERT INTO store_users VALUES (12862, 'magnus_citybil.se@blocket.se');
INSERT INTO store_users VALUES (2621, 'info_obabbil.se@blocket.se');
INSERT INTO store_users VALUES (12861, 'agne.axelsson_bilia.se@blocket.se');
INSERT INTO store_users VALUES (2108, 'ostkustbilar_telia.com@blocket.se');
INSERT INTO store_users VALUES (12860, 'info_hansperssonbil.se@blocket.se');
INSERT INTO store_users VALUES (3387, 'ake_bergnersbil.se@blocket.se');
INSERT INTO store_users VALUES (12859, 'info_hansperssonbil.se@blocket.se');
INSERT INTO store_users VALUES (12858, 'info_hansperssonbil.se@blocket.se');
INSERT INTO store_users VALUES (1081, 'eddiesbil_eddiesbil.se@blocket.se');
INSERT INTO store_users VALUES (3897, 'kent.brink_lindstromsmotor.se@blocket.se');
INSERT INTO store_users VALUES (12857, 'info_hansperssonbil.se@blocket.se');
INSERT INTO store_users VALUES (13625, 'twinmoters_hotmail.com@blocket.se');
INSERT INTO store_users VALUES (12856, 'info_hansperssonbil.se@blocket.se');
INSERT INTO store_users VALUES (1335, 'info_elmbrobil.se@blocket.se');
INSERT INTO store_users VALUES (12855, 'info_hansperssonbil.se@blocket.se');
INSERT INTO store_users VALUES (12854, 'mikael_orrekullabil.se@blocket.se');
INSERT INTO store_users VALUES (1333, 'info_mellstromsbil.se@blocket.se');
INSERT INTO store_users VALUES (3381, 'stureson.bil_nordiq.net@blocket.se');
INSERT INTO store_users VALUES (12852, 'mikael.brodd_bilbrodd.se@blocket.se');
INSERT INTO store_users VALUES (2610, 'info_hamren.t.se@blocket.se');
INSERT INTO store_users VALUES (12850, 'info-mtbil_telia.com@blocket.se');
INSERT INTO store_users VALUES (14130, 'jansson_medelhult.nu@blocket.se');
INSERT INTO store_users VALUES (2863, 'ted_upplandsbilforum.se@blocket.se');
INSERT INTO store_users VALUES (12847, 'reimond_eklundsbil.com@blocket.se');
INSERT INTO store_users VALUES (4142, 'info_biltec.com@blocket.se');
INSERT INTO store_users VALUES (4141, 'tommy_karlstrommotor.se@blocket.se');
INSERT INTO store_users VALUES (12333, 'ringqvist.bil.mc_telia.com@blocket.se');
INSERT INTO store_users VALUES (12842, 'bilhornan.solna_telia.com@blocket.se');
INSERT INTO store_users VALUES (12841, 'thomas_lindqvistbil.nu@blocket.se');
INSERT INTO store_users VALUES (296, 'info_svenskamotorkompaniet.se@blocket.se');
INSERT INTO store_users VALUES (4136, 'info_yvonnenordanstigbil.se@blocket.se');
INSERT INTO store_users VALUES (14120, 'info_lonestamsbilar.com@blocket.se');
INSERT INTO store_users VALUES (3879, 'info_fottasmc.se@blocket.se');
INSERT INTO store_users VALUES (12839, 'info_autohaus.se@blocket.se');
INSERT INTO store_users VALUES (3110, 'nibo.bolln�s_telia.com@blocket.se');
INSERT INTO store_users VALUES (4134, 'lidkopingsbiltjanst_telia.com@blocket.se');
INSERT INTO store_users VALUES (12838, 'raberg_mbox302.swipnet.se@blocket.se');
INSERT INTO store_users VALUES (3365, 'info_vanservice.se@blocket.se');
INSERT INTO store_users VALUES (4133, 'vernabil_vernabil.com@blocket.se');
INSERT INTO store_users VALUES (12837, 'info_bilgarden.se@blocket.se');
INSERT INTO store_users VALUES (2852, 'info_bilvalet.com@blocket.se');
INSERT INTO store_users VALUES (12836, 'anders.carlsson_hurtigsbil.se@blocket.se');
INSERT INTO store_users VALUES (4131, 'kareby.bil_swipnet.se@blocket.se');
INSERT INTO store_users VALUES (12576, 'jan.bjore_tele2.se@blocket.se');
INSERT INTO store_users VALUES (31, 'info_capitalcar.se@blocket.se');
INSERT INTO store_users VALUES (798, 'info_martenssons-bil.se@blocket.se');
INSERT INTO store_users VALUES (1054, 'mats_bilexpo.com@blocket.se');
INSERT INTO store_users VALUES (12829, 'tisnarbil_telia.com@blocket.se');
INSERT INTO store_users VALUES (4124, 'info_m-bilar.se@blocket.se');
INSERT INTO store_users VALUES (12828, 'info_ebn.se@blocket.se');
INSERT INTO store_users VALUES (14108, 'halabsonsbil_swipnet.se@blocket.se');
INSERT INTO store_users VALUES (3099, 'husvagnscenterivalbo_telia.com@blocket.se');
INSERT INTO store_users VALUES (2841, 'roger.ericsson_abgpersson.se@blocket.se ,  johan.smedsjo_abgpersson.se@blocket.se');
INSERT INTO store_users VALUES (4120, 'info_nilssonsbilhall.se@blocket.se');
INSERT INTO store_users VALUES (3351, 'info_magnusbil.se@blocket.se');
INSERT INTO store_users VALUES (4119, 'tommie.edstrand_lhandelshus.se@blocket.se');
INSERT INTO store_users VALUES (4118, 'per_hojgaraget.com@blocket.se');
INSERT INTO store_users VALUES (12566, 'info_custommc.net@blocket.se');
INSERT INTO store_users VALUES (12563, 'info_cnbil.net@blocket.se');
INSERT INTO store_users VALUES (12561, 'info_slagstabiltorg.se@blocket.se');
INSERT INTO store_users VALUES (4111, 'info_solvallabiltorg.se@blocket.se');
INSERT INTO store_users VALUES (4110, 'nestors_telia.com@blocket.se');
INSERT INTO store_users VALUES (3853, 'info_movap.se@blocket.se');
INSERT INTO store_users VALUES (12, 'svenska_bilgruppen.se@blocket.se');
INSERT INTO store_users VALUES (3851, 'nilssonsmotor_telia.com@blocket.se');
INSERT INTO store_users VALUES (3082, 'niklas.johansson_bilhornan.se@blocket.se');
INSERT INTO store_users VALUES (13322, 'info_nilssonsbil.se@blocket.se');
INSERT INTO store_users VALUES (3849, 'sulasmc_swipnet.se@blocket.se');
INSERT INTO store_users VALUES (12809, 'info_lundbybil.se@blocket.se');
INSERT INTO store_users VALUES (12552, 'orsa.olsson_telia.com@blocket.se');
INSERT INTO store_users VALUES (3079, 'stina.fischer_anetornbil.se@blocket.se');
INSERT INTO store_users VALUES (4103, 'info_bengtiorkelljunga.se@blocket.se');
INSERT INTO store_users VALUES (2566, 'goran.bilforsalj_seffelbil.se@blocket.se');
INSERT INTO store_users VALUES (3590, 'hedmans.motor_telia.com@blocket.se');
INSERT INTO store_users VALUES (14086, 'receptionen_vallentunamotor.se@blocket.se');
INSERT INTO store_users VALUES (14085, 'info_nyboms-bil.se@blocket.se');
INSERT INTO store_users VALUES (1284, 'info_bjornlundmark.se@blocket.se');
INSERT INTO store_users VALUES (2819, 'info_helinsbil.se@blocket.se');
INSERT INTO store_users VALUES (3843, 'info_bikecity.se@blocket.se');
INSERT INTO store_users VALUES (2304, 'slutaletabil_telia.com@blocket.se');
INSERT INTO store_users VALUES (12541, 'sockenplansbil_brevet.nu@blocket.se');
INSERT INTO store_users VALUES (14074, 'info_backmansbilar.com@blocket.se');
INSERT INTO store_users VALUES (12535, 'bilbutiken_telia.com@blocket.se');
INSERT INTO store_users VALUES (12534, 'bilbutiken_telia.com@blocket.se');
INSERT INTO store_users VALUES (2293, 'bilforsaljning_peugeotnorrkoping.com@blocket.se');
INSERT INTO store_users VALUES (12529, 'bilfirmanibara_yahoo.se@blocket.se');
INSERT INTO store_users VALUES (12528, 'info_saluvagnar.se@blocket.se');
INSERT INTO store_users VALUES (13037, 'alexander_blennowsbil.se@blocket.se');
INSERT INTO store_users VALUES (2796, 'bengt_cassmanbil.se@blocket.se');
INSERT INTO store_users VALUES (1255, 'frojdsmotor_telia.com@blocket.se');
INSERT INTO store_users VALUES (13538, 'info_johansmc.se@blocket.se');
INSERT INTO store_users VALUES (2785, 'andfolks_bytbil.com@blocket.se');
INSERT INTO store_users VALUES (3294, 'info_auto-mobile.se@blocket.se');


INSERT INTO store_params VALUES (88, 'import_type', '1,3');
INSERT INTO store_params VALUES (123, 'import_type', '1,3');
INSERT INTO store_params VALUES (162, 'import_type', '1,3');
INSERT INTO store_params VALUES (296, 'import_type', '1,3');
INSERT INTO store_params VALUES (588, 'import_type', '1,3');
INSERT INTO store_params VALUES (798, 'import_type', '1,3');
INSERT INTO store_params VALUES (913, 'import_type', '1,3');
INSERT INTO store_params VALUES (1054, 'import_type', '1,3');
INSERT INTO store_params VALUES (1081, 'import_type', '1,3');
INSERT INTO store_params VALUES (1099, 'import_type', '1,3');
INSERT INTO store_params VALUES (1109, 'import_type', '1,3');
INSERT INTO store_params VALUES (1142, 'import_type', '1,3');
INSERT INTO store_params VALUES (1177, 'import_type', '1,3');
INSERT INTO store_params VALUES (1181, 'import_type', '1,3');
INSERT INTO store_params VALUES (1183, 'import_type', '1,3');
INSERT INTO store_params VALUES (1231, 'import_type', '1,3');
INSERT INTO store_params VALUES (1255, 'import_type', '1,3');
INSERT INTO store_params VALUES (1284, 'import_type', '1,3');
INSERT INTO store_params VALUES (1333, 'import_type', '1,3');
INSERT INTO store_params VALUES (1335, 'import_type', '1,3');
INSERT INTO store_params VALUES (1380, 'import_type', '1,3');
INSERT INTO store_params VALUES (1484, 'import_type', '1,3');
INSERT INTO store_params VALUES (1627, 'import_type', '1,3');
INSERT INTO store_params VALUES (1855, 'import_type', '1,3');
INSERT INTO store_params VALUES (1895, 'import_type', '1,3');
INSERT INTO store_params VALUES (1956, 'import_type', '1,3');
INSERT INTO store_params VALUES (1970, 'import_type', '1,3');
INSERT INTO store_params VALUES (1993, 'import_type', '1,3');
INSERT INTO store_params VALUES (2031, 'import_type', '1,3');
INSERT INTO store_params VALUES (2108, 'import_type', '1,3');
INSERT INTO store_params VALUES (2135, 'import_type', '1,3');
INSERT INTO store_params VALUES (2187, 'import_type', '1,3');
INSERT INTO store_params VALUES (2242, 'import_type', '1,3');
INSERT INTO store_params VALUES (2293, 'import_type', '1,3');
INSERT INTO store_params VALUES (2304, 'import_type', '1,3');
INSERT INTO store_params VALUES (2395, 'import_type', '1,3');
INSERT INTO store_params VALUES (2430, 'import_type', '1,3');
INSERT INTO store_params VALUES (2432, 'import_type', '1,3');
INSERT INTO store_params VALUES (2443, 'import_type', '1,3');
INSERT INTO store_params VALUES (2515, 'import_type', '1,3');
INSERT INTO store_params VALUES (2524, 'import_type', '1,3');
INSERT INTO store_params VALUES (2552, 'import_type', '1,3');
INSERT INTO store_params VALUES (2563, 'import_type', '1,3');
INSERT INTO store_params VALUES (2566, 'import_type', '1,3');
INSERT INTO store_params VALUES (2572, 'import_type', '1,3');
INSERT INTO store_params VALUES (2610, 'import_type', '1,3');
INSERT INTO store_params VALUES (2621, 'import_type', '1,3');
INSERT INTO store_params VALUES (2643, 'import_type', '1,3');
INSERT INTO store_params VALUES (2650, 'import_type', '1,3');
INSERT INTO store_params VALUES (2652, 'import_type', '1,3');
INSERT INTO store_params VALUES (2662, 'import_type', '1,3');
INSERT INTO store_params VALUES (2677, 'import_type', '1,3');
INSERT INTO store_params VALUES (2688, 'import_type', '1,3');
INSERT INTO store_params VALUES (2742, 'import_type', '1,3');
INSERT INTO store_params VALUES (2769, 'import_type', '1,3');
INSERT INTO store_params VALUES (2785, 'import_type', '1,3');
INSERT INTO store_params VALUES (2796, 'import_type', '1,3');
INSERT INTO store_params VALUES (2819, 'import_type', '1,3');
INSERT INTO store_params VALUES (2841, 'import_type', '1,3');
INSERT INTO store_params VALUES (2852, 'import_type', '1,3');
INSERT INTO store_params VALUES (2863, 'import_type', '1,3');
INSERT INTO store_params VALUES (2892, 'import_type', '1,3');
INSERT INTO store_params VALUES (2920, 'import_type', '1,3');
INSERT INTO store_params VALUES (2925, 'import_type', '1,3');
INSERT INTO store_params VALUES (2953, 'import_type', '1,3');
INSERT INTO store_params VALUES (3002, 'import_type', '1,3');
INSERT INTO store_params VALUES (3051, 'import_type', '1,3');
INSERT INTO store_params VALUES (3056, 'import_type', '1,3');
INSERT INTO store_params VALUES (3067, 'import_type', '1,3');
INSERT INTO store_params VALUES (3079, 'import_type', '1,3');
INSERT INTO store_params VALUES (3082, 'import_type', '1,3');
INSERT INTO store_params VALUES (3099, 'import_type', '1,3');
INSERT INTO store_params VALUES (3110, 'import_type', '1,3');
INSERT INTO store_params VALUES (3170, 'import_type', '1,3');
INSERT INTO store_params VALUES (3175, 'import_type', '1,3');
INSERT INTO store_params VALUES (3225, 'import_type', '1,3');
INSERT INTO store_params VALUES (3270, 'import_type', '1,3');
INSERT INTO store_params VALUES (3279, 'import_type', '1,3');
INSERT INTO store_params VALUES (3294, 'import_type', '1,3');
INSERT INTO store_params VALUES (3351, 'import_type', '1,3');
INSERT INTO store_params VALUES (3365, 'import_type', '1,3');
INSERT INTO store_params VALUES (3381, 'import_type', '1,3');
INSERT INTO store_params VALUES (3387, 'import_type', '1,3');
INSERT INTO store_params VALUES (3414, 'import_type', '1,3');
INSERT INTO store_params VALUES (3452, 'import_type', '1,3');
INSERT INTO store_params VALUES (3455, 'import_type', '1,3');
INSERT INTO store_params VALUES (3493, 'import_type', '1,3');
INSERT INTO store_params VALUES (3553, 'import_type', '1,3');
INSERT INTO store_params VALUES (3590, 'import_type', '1,3');
INSERT INTO store_params VALUES (3751, 'import_type', '1,3');
INSERT INTO store_params VALUES (3752, 'import_type', '1,3');
INSERT INTO store_params VALUES (3779, 'import_type', '1,3');
INSERT INTO store_params VALUES (3843, 'import_type', '1,3');
INSERT INTO store_params VALUES (3849, 'import_type', '1,3');
INSERT INTO store_params VALUES (3851, 'import_type', '1,3');
INSERT INTO store_params VALUES (3853, 'import_type', '1,3');
INSERT INTO store_params VALUES (3879, 'import_type', '1,3');
INSERT INTO store_params VALUES (3897, 'import_type', '1,3');
INSERT INTO store_params VALUES (3923, 'import_type', '1,3');
INSERT INTO store_params VALUES (3935, 'import_type', '1,3');
INSERT INTO store_params VALUES (3941, 'import_type', '1,3');
INSERT INTO store_params VALUES (3947, 'import_type', '1,3');
INSERT INTO store_params VALUES (3950, 'import_type', '1,3');
INSERT INTO store_params VALUES (3951, 'import_type', '1,3');
INSERT INTO store_params VALUES (3952, 'import_type', '1,3');
INSERT INTO store_params VALUES (3962, 'import_type', '1,3');
INSERT INTO store_params VALUES (3968, 'import_type', '1,3');
INSERT INTO store_params VALUES (3974, 'import_type', '1,3');
INSERT INTO store_params VALUES (3975, 'import_type', '1,3');
INSERT INTO store_params VALUES (3980, 'import_type', '1,3');
INSERT INTO store_params VALUES (3981, 'import_type', '1,3');
INSERT INTO store_params VALUES (3982, 'import_type', '1,3');
INSERT INTO store_params VALUES (3986, 'import_type', '1,3');
INSERT INTO store_params VALUES (3994, 'import_type', '1,3');
INSERT INTO store_params VALUES (3995, 'import_type', '1,3');
INSERT INTO store_params VALUES (4003, 'import_type', '1,3');
INSERT INTO store_params VALUES (4004, 'import_type', '1,3');
INSERT INTO store_params VALUES (4007, 'import_type', '1,3');
INSERT INTO store_params VALUES (4011, 'import_type', '1,3');
INSERT INTO store_params VALUES (4014, 'import_type', '1,3');
INSERT INTO store_params VALUES (4015, 'import_type', '1,3');
INSERT INTO store_params VALUES (4020, 'import_type', '1,3');
INSERT INTO store_params VALUES (4022, 'import_type', '1,3');
INSERT INTO store_params VALUES (4023, 'import_type', '1,3');
INSERT INTO store_params VALUES (4024, 'import_type', '1,3');
INSERT INTO store_params VALUES (4026, 'import_type', '1,3');
INSERT INTO store_params VALUES (4028, 'import_type', '1,3');
INSERT INTO store_params VALUES (4029, 'import_type', '1,3');
INSERT INTO store_params VALUES (4033, 'import_type', '1,3');
INSERT INTO store_params VALUES (4034, 'import_type', '1,3');
INSERT INTO store_params VALUES (4035, 'import_type', '1,3');
INSERT INTO store_params VALUES (4037, 'import_type', '1,3');
INSERT INTO store_params VALUES (4049, 'import_type', '1,3');
INSERT INTO store_params VALUES (4053, 'import_type', '1,3');
INSERT INTO store_params VALUES (4054, 'import_type', '1,3');
INSERT INTO store_params VALUES (4064, 'import_type', '1,3');
INSERT INTO store_params VALUES (4065, 'import_type', '1,3');
INSERT INTO store_params VALUES (4069, 'import_type', '1,3');
INSERT INTO store_params VALUES (4072, 'import_type', '1,3');
INSERT INTO store_params VALUES (4077, 'import_type', '1,3');
INSERT INTO store_params VALUES (4081, 'import_type', '1,3');
INSERT INTO store_params VALUES (4083, 'import_type', '1,3');
INSERT INTO store_params VALUES (4103, 'import_type', '1,3');
INSERT INTO store_params VALUES (4110, 'import_type', '1,3');
INSERT INTO store_params VALUES (4111, 'import_type', '1,3');
INSERT INTO store_params VALUES (4118, 'import_type', '1,3');
INSERT INTO store_params VALUES (4119, 'import_type', '1,3');
INSERT INTO store_params VALUES (4120, 'import_type', '1,3');
INSERT INTO store_params VALUES (4124, 'import_type', '1,3');
INSERT INTO store_params VALUES (4131, 'import_type', '1,3');
INSERT INTO store_params VALUES (4133, 'import_type', '1,3');
INSERT INTO store_params VALUES (4134, 'import_type', '1,3');
INSERT INTO store_params VALUES (4136, 'import_type', '1,3');
INSERT INTO store_params VALUES (4141, 'import_type', '1,3');
INSERT INTO store_params VALUES (4142, 'import_type', '1,3');
INSERT INTO store_params VALUES (9848, 'import_type', '1,3');
INSERT INTO store_params VALUES (10668, 'import_type', '1,3');
INSERT INTO store_params VALUES (10713, 'import_type', '1,3');
INSERT INTO store_params VALUES (10715, 'import_type', '1,3');
INSERT INTO store_params VALUES (10720, 'import_type', '1,3');
INSERT INTO store_params VALUES (10729, 'import_type', '1,3');
INSERT INTO store_params VALUES (10740, 'import_type', '1,3');
INSERT INTO store_params VALUES (10952, 'import_type', '1,3');
INSERT INTO store_params VALUES (10956, 'import_type', '1,3');
INSERT INTO store_params VALUES (11730, 'import_type', '1,3');
INSERT INTO store_params VALUES (12333, 'import_type', '1,3');
INSERT INTO store_params VALUES (12397, 'import_type', '1,3');
INSERT INTO store_params VALUES (12418, 'import_type', '1,3');
INSERT INTO store_params VALUES (12419, 'import_type', '1,3');
INSERT INTO store_params VALUES (12420, 'import_type', '1,3');
INSERT INTO store_params VALUES (12428, 'import_type', '1,3');
INSERT INTO store_params VALUES (12440, 'import_type', '1,3');
INSERT INTO store_params VALUES (12441, 'import_type', '1,3');
INSERT INTO store_params VALUES (12444, 'import_type', '1,3');
INSERT INTO store_params VALUES (12485, 'import_type', '1,3');
INSERT INTO store_params VALUES (12528, 'import_type', '1,3');
INSERT INTO store_params VALUES (12529, 'import_type', '1,3');
INSERT INTO store_params VALUES (12534, 'import_type', '1,3');
INSERT INTO store_params VALUES (12535, 'import_type', '1,3');
INSERT INTO store_params VALUES (12541, 'import_type', '1,3');
INSERT INTO store_params VALUES (12552, 'import_type', '1,3');
INSERT INTO store_params VALUES (12561, 'import_type', '1,3');
INSERT INTO store_params VALUES (12563, 'import_type', '1,3');
INSERT INTO store_params VALUES (12566, 'import_type', '1,3');
INSERT INTO store_params VALUES (12576, 'import_type', '1,3');
INSERT INTO store_params VALUES (12630, 'import_type', '1,3');
INSERT INTO store_params VALUES (12638, 'import_type', '1,3');
INSERT INTO store_params VALUES (12639, 'import_type', '1,3');
INSERT INTO store_params VALUES (12642, 'import_type', '1,3');
INSERT INTO store_params VALUES (12648, 'import_type', '1,3');
INSERT INTO store_params VALUES (12652, 'import_type', '1,3');
INSERT INTO store_params VALUES (12654, 'import_type', '1,3');
INSERT INTO store_params VALUES (12656, 'import_type', '1,3');
INSERT INTO store_params VALUES (12661, 'import_type', '1,3');
INSERT INTO store_params VALUES (12663, 'import_type', '1,3');
INSERT INTO store_params VALUES (12672, 'import_type', '1,3');
INSERT INTO store_params VALUES (12674, 'import_type', '1,3');
INSERT INTO store_params VALUES (12675, 'import_type', '1,3');
INSERT INTO store_params VALUES (12727, 'import_type', '1,3');
INSERT INTO store_params VALUES (12729, 'import_type', '1,3');
INSERT INTO store_params VALUES (12734, 'import_type', '1,3');
INSERT INTO store_params VALUES (12736, 'import_type', '1,3');
INSERT INTO store_params VALUES (12741, 'import_type', '1,3');
INSERT INTO store_params VALUES (12744, 'import_type', '1,3');
INSERT INTO store_params VALUES (12745, 'import_type', '1,3');
INSERT INTO store_params VALUES (12753, 'import_type', '1,3');
INSERT INTO store_params VALUES (12754, 'import_type', '1,3');
INSERT INTO store_params VALUES (12756, 'import_type', '1,3');
INSERT INTO store_params VALUES (12757, 'import_type', '1,3');
INSERT INTO store_params VALUES (12799, 'import_type', '1,3');
INSERT INTO store_params VALUES (12809, 'import_type', '1,3');
INSERT INTO store_params VALUES (12828, 'import_type', '1,3');
INSERT INTO store_params VALUES (12829, 'import_type', '1,3');
INSERT INTO store_params VALUES (12836, 'import_type', '1,3');
INSERT INTO store_params VALUES (12837, 'import_type', '1,3');
INSERT INTO store_params VALUES (12838, 'import_type', '1,3');
INSERT INTO store_params VALUES (12839, 'import_type', '1,3');
INSERT INTO store_params VALUES (12841, 'import_type', '1,3');
INSERT INTO store_params VALUES (12842, 'import_type', '1,3');
INSERT INTO store_params VALUES (12847, 'import_type', '1,3');
INSERT INTO store_params VALUES (12849, 'import_type', '1,3');
INSERT INTO store_params VALUES (12850, 'import_type', '1,3');
INSERT INTO store_params VALUES (12852, 'import_type', '1,3');
INSERT INTO store_params VALUES (12854, 'import_type', '1,3');
INSERT INTO store_params VALUES (12855, 'import_type', '1,3');
INSERT INTO store_params VALUES (12856, 'import_type', '1,3');
INSERT INTO store_params VALUES (12857, 'import_type', '1,3');
INSERT INTO store_params VALUES (12858, 'import_type', '1,3');
INSERT INTO store_params VALUES (12859, 'import_type', '1,3');
INSERT INTO store_params VALUES (12860, 'import_type', '1,3');
INSERT INTO store_params VALUES (12861, 'import_type', '1,3');
INSERT INTO store_params VALUES (12862, 'import_type', '1,3');
INSERT INTO store_params VALUES (12863, 'import_type', '1,3');
INSERT INTO store_params VALUES (12864, 'import_type', '1,3');
INSERT INTO store_params VALUES (12865, 'import_type', '1,3');
INSERT INTO store_params VALUES (12866, 'import_type', '1,3');
INSERT INTO store_params VALUES (12867, 'import_type', '1,3');
INSERT INTO store_params VALUES (12868, 'import_type', '1,3');
INSERT INTO store_params VALUES (12871, 'import_type', '1,3');
INSERT INTO store_params VALUES (12873, 'import_type', '1,3');
INSERT INTO store_params VALUES (12874, 'import_type', '1,3');
INSERT INTO store_params VALUES (12879, 'import_type', '1,3');
INSERT INTO store_params VALUES (12881, 'import_type', '1,3');
INSERT INTO store_params VALUES (12882, 'import_type', '1,3');
INSERT INTO store_params VALUES (12884, 'import_type', '1,3');
INSERT INTO store_params VALUES (12985, 'import_type', '1,3');
INSERT INTO store_params VALUES (13037, 'import_type', '1,3');
INSERT INTO store_params VALUES (13192, 'import_type', '1,3');
INSERT INTO store_params VALUES (13243, 'import_type', '1,3');
INSERT INTO store_params VALUES (13272, 'import_type', '1,3');
INSERT INTO store_params VALUES (13284, 'import_type', '1,3');
INSERT INTO store_params VALUES (13302, 'import_type', '1,3');
INSERT INTO store_params VALUES (13322, 'import_type', '1,3');
INSERT INTO store_params VALUES (13480, 'import_type', '1,3');
INSERT INTO store_params VALUES (13483, 'import_type', '1,3');
INSERT INTO store_params VALUES (13499, 'import_type', '1,3');
INSERT INTO store_params VALUES (13538, 'import_type', '1,3');
INSERT INTO store_params VALUES (13625, 'import_type', '1,3');
INSERT INTO store_params VALUES (13631, 'import_type', '1,3');
INSERT INTO store_params VALUES (13635, 'import_type', '1,3');
INSERT INTO store_params VALUES (13692, 'import_type', '1,3');
INSERT INTO store_params VALUES (13693, 'import_type', '1,3');
INSERT INTO store_params VALUES (13709, 'import_type', '1,3');
INSERT INTO store_params VALUES (13765, 'import_type', '1,3');
INSERT INTO store_params VALUES (13794, 'import_type', '1,3');
INSERT INTO store_params VALUES (13815, 'import_type', '1,3');
INSERT INTO store_params VALUES (13847, 'import_type', '1,3');
INSERT INTO store_params VALUES (13896, 'import_type', '1,3');
INSERT INTO store_params VALUES (13968, 'import_type', '1,3');
INSERT INTO store_params VALUES (14032, 'import_type', '1,3');
INSERT INTO store_params VALUES (14074, 'import_type', '1,3');
INSERT INTO store_params VALUES (14085, 'import_type', '1,3');
INSERT INTO store_params VALUES (14086, 'import_type', '1,3');
INSERT INTO store_params VALUES (14108, 'import_type', '1,3');
INSERT INTO store_params VALUES (14120, 'import_type', '1,3');
INSERT INTO store_params VALUES (14130, 'import_type', '1,3');
INSERT INTO store_params VALUES (14153, 'import_type', '1,3');
INSERT INTO store_params VALUES (14184, 'import_type', '1,3');
INSERT INTO store_params VALUES (14191, 'import_type', '1,3');
INSERT INTO store_params VALUES (14192, 'import_type', '1,3');
INSERT INTO store_params VALUES (14193, 'import_type', '1,3');
INSERT INTO store_params VALUES (14194, 'import_type', '1,3');
INSERT INTO store_params VALUES (14200, 'import_type', '1,3');
INSERT INTO store_params VALUES (14235, 'import_type', '1,3');
INSERT INTO store_params VALUES (14239, 'import_type', '1,3');
INSERT INTO store_params VALUES (14258, 'import_type', '1,3');
INSERT INTO store_params VALUES (14426, 'import_type', '1,3');
INSERT INTO store_params VALUES (14427, 'import_type', '1,3');
INSERT INTO store_params VALUES (14438, 'import_type', '1,3');
INSERT INTO store_params VALUES (14440, 'import_type', '1,3');
INSERT INTO store_params VALUES (14443, 'import_type', '1,3');
INSERT INTO store_params VALUES (14461, 'import_type', '1,3');
INSERT INTO store_params VALUES (14463, 'import_type', '1,3');
INSERT INTO store_params VALUES (14469, 'import_type', '1,3');
INSERT INTO store_params VALUES (14475, 'import_type', '1,3');
INSERT INTO store_params VALUES (14476, 'import_type', '1,3');

-- Example voucher
INSERT INTO users (user_id, email, uid, account, paid_total) VALUES (30, 'patrik@blocket.se', 30, 0, 0);
INSERT INTO vouchers VALUES (30, 250, 14661);
INSERT INTO tokens VALUES (30, 'token_30', NULL, now(), now(), '10.11.12.13', 'voucher_refill', 14661);
INSERT INTO payment_groups VALUES (200, NULL, 'paid', now());
INSERT INTO payments (payment_group_id, payment_type, pay_amount) VALUES (200, 'voucher_refill', 250);
INSERT INTO pay_log VALUES (5000, 'voucher_save', 0, NULL, 30, now(), 200, 'SAVE');
INSERT INTO pay_log VALUES (5001, 'voucher_paycard', 250, '70200a1', NULL, now(), 200, 'OK');
INSERT INTO pay_log_references VALUES (5000, 0, 'remote_addr', '10.11.12.13');
INSERT INTO pay_log_references VALUES (5001, 0, 'payex', '200');
INSERT INTO voucher_actions VALUES (30, 1, 'fill', 2, 'paid', 250, 200);
INSERT INTO voucher_states VALUES (30, 1, 1, 'unpaid', 'initial', now(), '10.11.12.13', 30, NULL);
INSERT INTO voucher_states VALUES (30, 1, 2, 'paid', 'pay', now(), '10.11.12.13', NULL, 250);
ALTER SEQUENCE voucher_states_voucher_state_id_seq RESTART WITH 100;
ALTER SEQUENCE stores_store_id_seq RESTART WITH 15000;
ALTER SEQUENCE voucher_actions_voucher_action_id_seq RESTART WITH 100;

--
-- PostgreSQL database dump complete
--

