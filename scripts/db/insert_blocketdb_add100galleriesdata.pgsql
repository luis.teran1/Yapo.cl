INSERT INTO ad_params SELECT ad_id,'gallery',(CURRENT_TIMESTAMP + interval '1 day')::text FROM ads WHERE status='active'
 AND type IN ('sell','swap','let') AND category NOT BETWEEN 7080 AND 7099
 AND EXISTS(SELECT * FROM ad_media WHERE ad_id=ads.ad_id)
 AND ad_id IN (SELECT DISTINCT ad_id FROM ad_params EXCEPT SELECT ad_id FROM ad_params WHERE name='gallery')
 ORDER BY random() LIMIT 100;


--Migrate inmo ads
select * from bpv.migrate_ads_inmo();
