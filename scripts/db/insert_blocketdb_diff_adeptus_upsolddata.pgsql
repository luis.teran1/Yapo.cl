INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (166, '12345', 'paid', '2015-07-31 17:12:02.626539', NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (167, '65432', 'paid', '2015-07-31 17:12:11.155978', NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (169, '12345', 'paid', '2015-07-31 20:04:16.846218', NULL);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (1, 2, 'upselling_weekly_bump', 700, 'accepted', 'normal', NULL, NULL, 166);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (1, 3, 'upselling_daily_bump', 703, 'accepted', 'normal', NULL, NULL, 167);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (1, 5, 'upselling_gallery', 709, 'accepted', 'normal', NULL, NULL, 169);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (1, 2, 698, 'reg', 'initial', '2015-07-31 17:12:02.626539', '10.0.1.40', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (1, 2, 699, 'unpaid', 'pending_pay', '2015-07-31 17:12:02.626539', '10.0.1.40', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (1, 2, 700, 'accepted', 'pay', '2015-07-31 17:12:04.492719', NULL, NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (1, 3, 701, 'reg', 'initial', '2015-07-31 17:12:11.155978', '10.0.1.40', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (1, 3, 702, 'unpaid', 'pending_pay', '2015-07-31 17:12:11.155978', '10.0.1.40', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (1, 3, 703, 'accepted', 'pay', '2015-07-31 17:12:12.806816', NULL, NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (1, 5, 707, 'reg', 'initial', '2015-07-31 20:04:16.846218', '10.0.1.40', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (1, 5, 708, 'unpaid', 'pending_pay', '2015-07-31 20:04:16.846218', '10.0.1.40', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (1, 5, 709, 'accepted', 'pay', '2015-07-31 20:04:20.890293', NULL, NULL);
SELECT pg_catalog.setval('action_states_state_id_seq', 709, true);
INSERT INTO ad_changes (ad_id, action_id, state_id, is_param, column_name, old_value, new_value) VALUES (1, 2, 700, true, 'upselling_weekly_bump', NULL, '4');
INSERT INTO ad_changes (ad_id, action_id, state_id, is_param, column_name, old_value, new_value) VALUES (1, 3, 703, true, 'upselling_daily_bump', NULL, '7');
INSERT INTO ad_changes (ad_id, action_id, state_id, is_param, column_name, old_value, new_value) VALUES (1, 5, 709, true, 'gallery', NULL, '2015-07-31 20:14:20');
INSERT INTO ad_params (ad_id, name, value) VALUES (1, 'upselling_weekly_bump', '4');
INSERT INTO ad_params (ad_id, name, value) VALUES (1, 'upselling_daily_bump', '7');
INSERT INTO ad_params (ad_id, name, value) VALUES (1, 'gallery', NOW() + INTERVAL '1 day');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (213, 'save', 0, '12345', NULL, '2015-07-31 17:12:03', 166, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (214, 'paycard_save', 0, '12345', NULL, '2015-07-31 17:12:04', 166, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (215, 'paycard', 2700, '00166a', NULL, '2015-07-31 17:12:04', 166, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (216, 'paycard', 2700, '12345', NULL, '2015-07-31 17:12:05', 166, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (217, 'save', 0, '65432', NULL, '2015-07-31 17:12:11', 167, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (218, 'paycard_save', 0, '65432', NULL, '2015-07-31 17:12:13', 167, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (219, 'paycard', 4500, '00167a', NULL, '2015-07-31 17:12:13', 167, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (220, 'paycard', 4500, '65432', NULL, '2015-07-31 17:12:13', 167, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (223, 'save', 0, '12345', NULL, '2015-07-31 20:04:17', 169, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (224, 'paycard_save', 0, '12345', NULL, '2015-07-31 20:04:21', 169, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (225, 'paycard', 9200, '00169a', NULL, '2015-07-31 20:04:21', 169, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (226, 'paycard', 9200, '12345', NULL, '2015-07-31 20:04:21', 169, 'OK');
SELECT pg_catalog.setval('pay_log_pay_log_id_seq', 226, true);
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (213, 0, 'remote_addr', '10.0.1.40');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (214, 0, 'auth_code', '980791');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (214, 1, 'trans_date', '31/07/2015 17:12:04');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (214, 2, 'placements_type', 'VN');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (214, 3, 'placements_number', '1');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (214, 4, 'pay_type', 'credit');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (214, 5, 'external_response', '0');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (214, 6, 'external_id', '96734207905335683699');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (214, 7, 'remote_addr', '10.0.1.73');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (215, 0, 'webpay', '1');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (217, 0, 'remote_addr', '10.0.1.40');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (218, 0, 'auth_code', '646217');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (218, 1, 'trans_date', '31/07/2015 17:12:12');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (218, 2, 'placements_type', 'VN');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (218, 3, 'placements_number', '1');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (218, 4, 'pay_type', 'credit');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (218, 5, 'external_response', '0');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (218, 6, 'external_id', '97208044087727599994');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (218, 7, 'remote_addr', '10.0.1.73');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (219, 0, 'webpay', '1');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (223, 0, 'remote_addr', '10.0.1.40');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (224, 0, 'auth_code', '127501');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (224, 1, 'trans_date', '31/07/2015 20:04:20');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (224, 2, 'placements_type', 'VN');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (224, 3, 'placements_number', '1');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (224, 4, 'pay_type', 'credit');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (224, 5, 'external_response', '0');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (224, 6, 'external_id', '39886271580974702102');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (224, 7, 'remote_addr', '10.0.1.73');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (225, 0, 'webpay', '1');
SELECT pg_catalog.setval('payment_groups_payment_group_id_seq', 169, true);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (166, 'upselling_weekly_bump', 2700, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (167, 'upselling_daily_bump', 4500, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (169, 'upselling_gallery', 9200, 0);
INSERT INTO purchase (purchase_id, doc_type, doc_num, external_doc_id, status, receipt, delivery, rut, name, lob, address, region, communes, contact, email, tax, discount, seller_id, total_price, payment_group_id, payment_method, account_id) VALUES (2, 'invoice', NULL, NULL, 'paid', '2015-07-31 17:12:02.626539', NULL, '100000-4', 'BLOCKET', 'Technology', 'Alonso de C�rdova 5670', 15, 315, 'BLOCKET', 'prepaid5@blocket.se', 0, 0, NULL, 2700, 166, 'webpay', 1);
INSERT INTO purchase (purchase_id, doc_type, doc_num, external_doc_id, status, receipt, delivery, rut, name, lob, address, region, communes, contact, email, tax, discount, seller_id, total_price, payment_group_id, payment_method, account_id) VALUES (3, 'invoice', NULL, NULL, 'paid', '2015-07-31 17:12:11.155978', NULL, '100000-4', 'BLOCKET', 'Technology', 'Alonso de C�rdova 5670', 15, 315, 'BLOCKET', 'prepaid5@blocket.se', 0, 0, NULL, 4500, 167, 'webpay', 1);
INSERT INTO purchase (purchase_id, doc_type, doc_num, external_doc_id, status, receipt, delivery, rut, name, lob, address, region, communes, contact, email, tax, discount, seller_id, total_price, payment_group_id, payment_method, account_id) VALUES (4, 'bill', NULL, NULL, 'paid', '2015-07-31 20:04:16.846218', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'prepaid5@blocket.se', 0, 0, NULL, 9200, 169, 'webpay', NULL);
INSERT INTO purchase_detail (purchase_detail_id, purchase_id, product_id, price, action_id, ad_id, payment_group_id) VALUES (2, 2, 101, 2700, 2, 1, 166);
INSERT INTO purchase_detail (purchase_detail_id, purchase_id, product_id, price, action_id, ad_id, payment_group_id) VALUES (3, 3, 103, 4500, 3, 1, 167);
INSERT INTO purchase_detail (purchase_detail_id, purchase_id, product_id, price, action_id, ad_id, payment_group_id) VALUES (4, 4, 102, 9200, 5, 1, 169);
SELECT pg_catalog.setval('purchase_detail_purchase_detail_id_seq', 4, true);
SELECT pg_catalog.setval('purchase_purchase_id_seq', 4, true);
INSERT INTO purchase_states (purchase_id, purchase_state_id, status, "timestamp", remote_addr) VALUES (2, 1, 'pending', '2015-07-31 17:12:02.626539', '10.0.1.40');
INSERT INTO purchase_states (purchase_id, purchase_state_id, status, "timestamp", remote_addr) VALUES (3, 2, 'pending', '2015-07-31 17:12:11.155978', '10.0.1.40');
INSERT INTO purchase_states (purchase_id, purchase_state_id, status, "timestamp", remote_addr) VALUES (4, 3, 'pending', '2015-07-31 20:04:16.846218', '10.0.1.40');
SELECT pg_catalog.setval('purchase_states_purchase_state_id_seq', 3, true);
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (93, NOW(), '11694@miyako.schibsted.cl', NOW(), NULL, NULL, NULL, NULL, NULL, 'cmd:weekly_bump_ad
ad_id:1
action_id:2
counter:4
is_upselling:1
batch:1
commit:1
', NULL, 'upselling', NULL, NULL);
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (94, NOW(), '11694@miyako.schibsted.cl', NOW(), NULL, NULL, NULL, NULL, NULL, 'cmd:weekly_bump_ad
ad_id:1
action_id:3
counter:7
daily_bump:1
is_upselling:1
batch:1
commit:1
', NULL, 'upselling', NULL, NULL);
SELECT pg_catalog.setval('trans_queue_id_seq', 94, true);

--Change all passwords to 123123123
update ads set salted_passwd =  E'$1024$2y1bQ}VYRD}zF4QCbf07a971b784888658a20e62156de64dfde5ffb6', passwd = null;
update accounts set salted_passwd =  E'$1024$2y1bQ}VYRD}zF4QCbf07a971b784888658a20e62156de64dfde5ffb6';
