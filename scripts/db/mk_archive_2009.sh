#!/bin/bash
# Run this script as postgres on the master node to replicate blocket_2009
# args: ip addresses of all nodes

if [ -z "$1" -o -z "$2" ] ; then
   echo "usage $0 masternode slavenode [slave2node]"
   exit 1
fi

slonik_hnr () {
TABLES=$(pg_dump -s --schema blocket_2008 blocketdb | sed -n 's/.*CREATE TABLE \([^ ][^ ]*\)\ .*/\1/p'| grep -v '^sl_')
# there are no sequences
SETID=3
typeset -i IDCOUNT;
IDCOUNT=330;

slonik_merge_sets 1 98 99 2> /dev/null | head -5 | egrep 'cluster|conninfo'

echo "create set (id=$SETID, origin=$1, comment='archive tables 2009');"
echo "wait for event (origin = ALL, confirmed = ALL, timeout = 60, wait on = $1);"
echo "echo 'set $SETID created';"
for table in $TABLES ; do
   test -z $table && continue
   printf "set add table (set id=$SETID, origin=$1, id=$IDCOUNT, fully qualified name = 'blocket_2009.$table', comment='');\n";
   echo "wait for event (origin = ALL, confirmed = ALL, timeout = 60, wait on = $1);"
   echo "echo 'added id $IDCOUNT blocket_2009.$table';"
   IDCOUNT="$IDCOUNT + 1"
done

echo "subscribe set ( id = $SETID, provider = $1, receiver = $2, forward = yes);"
echo "wait for event (origin = ALL, confirmed = ALL, timeout = 60, wait on = $1);"
echo "echo 'set subscribed to slave node $2';"
if [ -n "$3" ] ; then
echo "subscribe set ( id = $SETID, provider = $1, receiver = $3, forward = yes);"
echo "wait for event (origin = ALL, confirmed = ALL, timeout = 60, wait on = $1);"
echo "echo 'set subscribed to slave2 node $3';"
fi
}

slonik_hnr $* | slonik

echo 'Happy new year 2009'
