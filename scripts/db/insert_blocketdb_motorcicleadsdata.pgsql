
--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: zakay
--

INSERT INTO users (user_id, uid, email, account, paid_total) VALUES (1, 1, 'user10@blocket.se', 0, 0);

-- New ad for 50 CC
INSERT INTO ads (ad_id, list_id, orig_list_time, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, old_price, image, infopage, infopage_title, store_id, salted_passwd) VALUES(12, 12, now(), now(), 'active', 'sell', 'schibsted', 1234566789, 15, 0, 2060, 1, null, 'f', 'f', 'f', '50cc', 'fewfqrfqwarfa', null, null, null, null,null,null, 'es');

INSERT INTO ad_params (ad_id, name, value) VALUES (12, 'regdate', '1999');
INSERT INTO ad_params (ad_id, name, value) VALUES (12, 'mileage', '123');
INSERT INTO ad_params (ad_id, name, value) VALUES (12, 'cubiccms', '1');

INSERT INTO payment_groups (payment_group_id, code, status, added_at) VALUES (111, 38052, 'paid', '2008-02-12 11:07:55.763321');
INSERT INTO payments (payment_group_id, payment_type, pay_amount) VALUES (111, 'ad_action', 0);
insert into pay_log values(1, 'save', 0, 38052,  null, now(), 111, 'SAVE');
insert into pay_log values(2, 'verify', 0, 38052,  null, now(), 111, 'OK');

insert into ad_actions values(12, 1, 'new',  61, 'accepted', 'normal', null, null, 111);

insert into action_states values (12, 1, 58, 'reg', 'initial', now(), '192.168.4.75', null);
insert into action_states values (12, 1, 59, 'unpaid', 'newad', now(), '192.168.4.75', null);
insert into action_states values (12, 1, 60, 'pending_review', 'pay', now(), '192.168.4.75', null);
insert into action_states values (12, 1, 61, 'accepted', 'accept', now(), '192.168.4.75', null);

insert into state_params values (12, 1, 61, 'filter_name', 'normal');

-- New ad for 125 CC
INSERT INTO ads (ad_id, list_id, orig_list_time, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, old_price, image, infopage, infopage_title, store_id, salted_passwd) VALUES(13, 13, now(), now(), 'active', 'sell', 'schibsted', 1234566789, 15, 0, 2060, 1, null, 'f', 'f', 'f', '125cc', 'fewfqrfqwarfa', null, null, null, null,null,null, 'es');

INSERT INTO ad_params (ad_id, name, value) VALUES (13, 'regdate', '1999');
INSERT INTO ad_params (ad_id, name, value) VALUES (13, 'mileage', '123');
INSERT INTO ad_params (ad_id, name, value) VALUES (13, 'cubiccms', '2');

INSERT INTO payment_groups (payment_group_id, code, status, added_at) VALUES (112, 38053, 'paid', '2008-02-12 11:07:55.763321');
INSERT INTO payments (payment_group_id, payment_type, pay_amount) VALUES (112, 'ad_action', 0);
insert into pay_log values(3, 'save', 0, 38053,  null, now(), 112, 'SAVE');
insert into pay_log values(4, 'verify', 0, 38053,  null, now(), 112, 'OK');

insert into ad_actions values(13, 1, 'new',  65, 'accepted', 'normal', null, null, 112);

insert into action_states values (13, 1, 62, 'reg', 'initial', now(), '192.168.4.75', null);
insert into action_states values (13, 1, 63, 'unpaid', 'newad', now(), '192.168.4.75', null);
insert into action_states values (13, 1, 64, 'pending_review', 'pay', now(), '192.168.4.75', null);
insert into action_states values (13, 1, 65, 'accepted', 'accept', now(), '192.168.4.75', null);

insert into state_params values (13, 1, 65, 'filter_name', 'normal');

-- New ad for 250 CC
INSERT INTO ads (ad_id, list_id, orig_list_time, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, old_price, image, infopage, infopage_title, store_id, salted_passwd) VALUES(14, 14, now(), now(), 'active', 'sell', 'schibsted', 1234566789, 15, 0, 2060, 1, null, 'f', 'f', 'f', '250cc', 'fewfqrfqwarfa', null, null, null, null,null,null, 'es');

INSERT INTO ad_params (ad_id, name, value) VALUES (14, 'regdate', '1999');
INSERT INTO ad_params (ad_id, name, value) VALUES (14, 'mileage', '123');
INSERT INTO ad_params (ad_id, name, value) VALUES (14, 'cubiccms', '3');

INSERT INTO payment_groups (payment_group_id, code, status, added_at) VALUES (113, 38054, 'paid', '2008-02-12 11:07:55.763321');
INSERT INTO payments (payment_group_id, payment_type, pay_amount) VALUES (113, 'ad_action', 0);
insert into pay_log values(5, 'save', 0, 38054,  null, now(), 113, 'SAVE');
insert into pay_log values(6, 'verify', 0, 38054,  null, now(), 113, 'OK');

insert into ad_actions values(14, 1, 'new',  69, 'accepted', 'normal', null, null, 113);

insert into action_states values (14, 1, 66, 'reg', 'initial', now(), '192.168.4.75', null);
insert into action_states values (14, 1, 67, 'unpaid', 'newad', now(), '192.168.4.75', null);
insert into action_states values (14, 1, 68, 'pending_review', 'pay', now(), '192.168.4.75', null);
insert into action_states values (14, 1, 69, 'accepted', 'accept', now(), '192.168.4.75', null);

insert into state_params values (14, 1, 69, 'filter_name', 'normal');

-- New ad for 500 CC
INSERT INTO ads (ad_id, list_id, orig_list_time, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, old_price, image, infopage, infopage_title, store_id, salted_passwd) VALUES(15, 15, now(), now(), 'active', 'sell', 'schibsted', 1234566789, 15, 0, 2060, 1, null, 'f', 'f', 'f', '500cc', 'fewfqrfqwarfa', null, null, null, null,null,null, 'es');

INSERT INTO ad_params (ad_id, name, value) VALUES (15, 'regdate', '1999');
INSERT INTO ad_params (ad_id, name, value) VALUES (15, 'mileage', '123');
INSERT INTO ad_params (ad_id, name, value) VALUES (15, 'cubiccms', '4');

INSERT INTO payment_groups (payment_group_id, code, status, added_at) VALUES (114, 38055, 'paid', '2008-02-12 11:07:55.763321');
INSERT INTO payments (payment_group_id, payment_type, pay_amount) VALUES (114, 'ad_action', 0);
insert into pay_log values(7, 'save', 0, 38055,  null, now(), 114, 'SAVE');
insert into pay_log values(8, 'verify', 0, 38055,  null, now(), 114, 'OK');

insert into ad_actions values(15, 1, 'new',  73, 'accepted', 'normal', null, null, 114);

insert into action_states values (15, 1, 70, 'reg', 'initial', now(), '192.168.4.75', null);
insert into action_states values (15, 1, 71, 'unpaid', 'newad', now(), '192.168.4.75', null);
insert into action_states values (15, 1, 72, 'pending_review', 'pay', now(), '192.168.4.75', null);
insert into action_states values (15, 1, 73, 'accepted', 'accept', now(), '192.168.4.75', null);

insert into state_params values (15, 1, 73, 'filter_name', 'normal');

-- New ad for 750 CC
INSERT INTO ads (ad_id, list_id, orig_list_time, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, old_price, image, infopage, infopage_title, store_id, salted_passwd) VALUES(16, 16, now(), now(), 'active', 'sell', 'schibsted', 1234566789, 15, 0, 2060, 1, null, 'f', 'f', 'f', '750cc', 'fewfqrfqwarfa', null, null, null, null,null,null, 'es');

INSERT INTO ad_params (ad_id, name, value) VALUES (16, 'regdate', '1999');
INSERT INTO ad_params (ad_id, name, value) VALUES (16, 'mileage', '123');
INSERT INTO ad_params (ad_id, name, value) VALUES (16, 'cubiccms', '5');

INSERT INTO payment_groups (payment_group_id, code, status, added_at) VALUES (115, 38056, 'paid', '2008-02-12 11:07:55.763321');
INSERT INTO payments (payment_group_id, payment_type, pay_amount) VALUES (115, 'ad_action', 0);
insert into pay_log values(9, 'save', 0, 38056,  null, now(), 115, 'SAVE');
insert into pay_log values(10, 'verify', 0, 38056,  null, now(), 115, 'OK');

insert into ad_actions values(16, 1, 'new',  77, 'accepted', 'normal', null, null, 115);

insert into action_states values (16, 1, 74, 'reg', 'initial', now(), '192.168.4.75', null);
insert into action_states values (16, 1, 75, 'unpaid', 'newad', now(), '192.168.4.75', null);
insert into action_states values (16, 1, 76, 'pending_review', 'pay', now(), '192.168.4.75', null);
insert into action_states values (16, 1, 77, 'accepted', 'accept', now(), '192.168.4.75', null);

insert into state_params values (16, 1, 77, 'filter_name', 'normal');

-- New ad for 1000 CC
INSERT INTO ads (ad_id, list_id, orig_list_time, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, old_price, image, infopage, infopage_title, store_id, salted_passwd) VALUES(17, 17, now(), now(), 'active', 'sell', 'schibsted', 1234566789, 15, 0, 2060, 1, null, 'f', 'f', 'f', '1000cc', 'fewfqrfqwarfa', null, null, null, null,null,null, 'es');

INSERT INTO ad_params (ad_id, name, value) VALUES (17, 'regdate', '1999');
INSERT INTO ad_params (ad_id, name, value) VALUES (17, 'mileage', '123');
INSERT INTO ad_params (ad_id, name, value) VALUES (17, 'cubiccms', '6');

INSERT INTO payment_groups (payment_group_id, code, status, added_at) VALUES (116, 38057, 'paid', '2008-02-12 11:07:55.763321');
INSERT INTO payments (payment_group_id, payment_type, pay_amount) VALUES (116, 'ad_action', 0);
insert into pay_log values(11, 'save', 0, 38057,  null, now(), 116, 'SAVE');
insert into pay_log values(12, 'verify', 0, 38057,  null, now(), 116, 'OK');

insert into ad_actions values(17, 1, 'new',  81, 'accepted', 'normal', null, null, 116);

insert into action_states values (17, 1, 78, 'reg', 'initial', now(), '192.168.4.75', null);
insert into action_states values (17, 1, 79, 'unpaid', 'newad', now(), '192.168.4.75', null);
insert into action_states values (17, 1, 80, 'pending_review', 'pay', now(), '192.168.4.75', null);
insert into action_states values (17, 1, 81, 'accepted', 'accept', now(), '192.168.4.75', null);

insert into state_params values (17, 1, 81, 'filter_name', 'normal');

-- New ad for 150 CC
INSERT INTO ads (ad_id, list_id, orig_list_time, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, old_price, image, infopage, infopage_title, store_id, salted_passwd) VALUES(18, 18, now(), now(), 'active', 'sell', 'schibsted', 1234566789, 15, 0, 2060, 1, null, 'f', 'f', 'f', '150cc', 'fewfqrfqwarfa', null, null, null, null,null,null, 'es');

INSERT INTO ad_params (ad_id, name, value) VALUES (18, 'regdate', '1999');
INSERT INTO ad_params (ad_id, name, value) VALUES (18, 'mileage', '123');
INSERT INTO ad_params (ad_id, name, value) VALUES (18, 'cubiccms', '9');

INSERT INTO payment_groups (payment_group_id, code, status, added_at) VALUES (117, 38058, 'paid', '2008-02-12 11:07:55.763321');
INSERT INTO payments (payment_group_id, payment_type, pay_amount) VALUES (117, 'ad_action', 0);
insert into pay_log values(13, 'save', 0, 38058,  null, now(), 117, 'SAVE');
insert into pay_log values(14, 'verify', 0, 38058,  null, now(), 117, 'OK');

insert into ad_actions values(18, 1, 'new',  85, 'accepted', 'normal', null, null, 117);

insert into action_states values (18, 1, 82, 'reg', 'initial', now(), '192.168.4.75', null);
insert into action_states values (18, 1, 83, 'unpaid', 'newad', now(), '192.168.4.75', null);
insert into action_states values (18, 1, 84, 'pending_review', 'pay', now(), '192.168.4.75', null);
insert into action_states values (18, 1, 85, 'accepted', 'accept', now(), '192.168.4.75', null);

insert into state_params values (18, 1, 85, 'filter_name', 'normal');

-- New ad for 200 CC
INSERT INTO ads (ad_id, list_id, orig_list_time, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, old_price, image, infopage, infopage_title, store_id, salted_passwd) VALUES(19, 19, now(), now(), 'active', 'sell', 'schibsted', 1234566789, 15, 0, 2060, 1, null, 'f', 'f', 'f', '200cc', 'fewfqrfqwarfa', null, null, null, null,null,null, 'es');

INSERT INTO ad_params (ad_id, name, value) VALUES (19, 'regdate', '1999');
INSERT INTO ad_params (ad_id, name, value) VALUES (19, 'mileage', '123');
INSERT INTO ad_params (ad_id, name, value) VALUES (19, 'cubiccms', '11');

INSERT INTO payment_groups (payment_group_id, code, status, added_at) VALUES (118, 38059, 'paid', '2008-02-12 11:07:55.763321');
INSERT INTO payments (payment_group_id, payment_type, pay_amount) VALUES (118, 'ad_action', 0);
insert into pay_log values(15, 'save', 0, 38059,  null, now(), 118, 'SAVE');
insert into pay_log values(16, 'verify', 0, 38059,  null, now(), 118, 'OK');

insert into ad_actions values(19, 1, 'new',  89, 'accepted', 'normal', null, null, 118);

insert into action_states values (19, 1, 86, 'reg', 'initial', now(), '192.168.4.75', null);
insert into action_states values (19, 1, 87, 'unpaid', 'newad', now(), '192.168.4.75', null);
insert into action_states values (19, 1, 88, 'pending_review', 'pay', now(), '192.168.4.75', null);
insert into action_states values (19, 1, 89, 'accepted', 'accept', now(), '192.168.4.75', null);

insert into state_params values (19, 1, 89, 'filter_name', 'normal');

-- New ad for 300 CC
INSERT INTO ads (ad_id, list_id, orig_list_time, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, old_price, image, infopage, infopage_title, store_id, salted_passwd) VALUES(20, 20, now(), now(), 'active', 'sell', 'schibsted', 1234566789, 15, 0, 2060, 1, null, 'f', 'f', 'f', '300cc', 'fewfqrfqwarfa', null, null, null, null,null,null, 'es');

INSERT INTO ad_params (ad_id, name, value) VALUES (20, 'regdate', '1999');
INSERT INTO ad_params (ad_id, name, value) VALUES (20, 'mileage', '123');
INSERT INTO ad_params (ad_id, name, value) VALUES (20, 'cubiccms', '12');

INSERT INTO payment_groups (payment_group_id, code, status, added_at) VALUES (119, 38060, 'paid', '2008-02-12 11:07:55.763321');
INSERT INTO payments (payment_group_id, payment_type, pay_amount) VALUES (119, 'ad_action', 0);
insert into pay_log values(17, 'save', 0, 38060,  null, now(), 119, 'SAVE');
insert into pay_log values(18, 'verify', 0, 38060,  null, now(), 119, 'OK');

insert into ad_actions values(20, 1, 'new',  93, 'accepted', 'normal', null, null, 119);

insert into action_states values (20, 1, 90, 'reg', 'initial', now(), '192.168.4.75', null);
insert into action_states values (20, 1, 91, 'unpaid', 'newad', now(), '192.168.4.75', null);
insert into action_states values (20, 1, 92, 'pending_review', 'pay', now(), '192.168.4.75', null);
insert into action_states values (20, 1, 93, 'accepted', 'accept', now(), '192.168.4.75', null);

insert into state_params values (20, 1, 93, 'filter_name', 'normal');

-- New ad for 350 CC
INSERT INTO ads (ad_id, list_id, orig_list_time, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, old_price, image, infopage, infopage_title, store_id, salted_passwd) VALUES(21, 21, now(), now(), 'active', 'sell', 'schibsted', 1234566789, 15, 0, 2060, 1, null, 'f', 'f', 'f', '350cc', 'fewfqrfqwarfa', null, null, null, null,null,null, 'es');

INSERT INTO ad_params (ad_id, name, value) VALUES (21, 'regdate', '1999');
INSERT INTO ad_params (ad_id, name, value) VALUES (21, 'mileage', '123');
INSERT INTO ad_params (ad_id, name, value) VALUES (21, 'cubiccms', '13');

INSERT INTO payment_groups (payment_group_id, code, status, added_at) VALUES (120, 38061, 'paid', '2008-02-12 11:07:55.763321');
INSERT INTO payments (payment_group_id, payment_type, pay_amount) VALUES (120, 'ad_action', 0);
insert into pay_log values(19, 'save', 0, 38061,  null, now(), 120, 'SAVE');
insert into pay_log values(20, 'verify', 0, 38061,  null, now(), 120, 'OK');

insert into ad_actions values(21, 1, 'new',  97, 'accepted', 'normal', null, null, 120);

insert into action_states values (21, 1, 94, 'reg', 'initial', now(), '192.168.4.75', null);
insert into action_states values (21, 1, 95, 'unpaid', 'newad', now(), '192.168.4.75', null);
insert into action_states values (21, 1, 96, 'pending_review', 'pay', now(), '192.168.4.75', null);
insert into action_states values (21, 1, 97, 'accepted', 'accept', now(), '192.168.4.75', null);

insert into state_params values (21, 1, 97, 'filter_name', 'normal');

-- New ad for 400 CC
INSERT INTO ads (ad_id, list_id, orig_list_time, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, old_price, image, infopage, infopage_title, store_id, salted_passwd) VALUES(22, 22, now(), now(), 'active', 'sell', 'schibsted', 1234566789, 15, 0, 2060, 1, null, 'f', 'f', 'f', '400cc', 'fewfqrfqwarfa', null, null, null, null,null,null, 'es');

INSERT INTO ad_params (ad_id, name, value) VALUES (22, 'regdate', '1999');
INSERT INTO ad_params (ad_id, name, value) VALUES (22, 'mileage', '123');
INSERT INTO ad_params (ad_id, name, value) VALUES (22, 'cubiccms', '14');

INSERT INTO payment_groups (payment_group_id, code, status, added_at) VALUES (121, 38062, 'paid', '2008-02-12 11:07:55.763321');
INSERT INTO payments (payment_group_id, payment_type, pay_amount) VALUES (121, 'ad_action', 0);
insert into pay_log values(21, 'save', 0, 38062,  null, now(), 121, 'SAVE');
insert into pay_log values(22, 'verify', 0, 38062,  null, now(), 121, 'OK');

insert into ad_actions values(22, 1, 'new',  101, 'accepted', 'normal', null, null, 121);

insert into action_states values (22, 1, 98, 'reg', 'initial', now(), '192.168.4.75', null);
insert into action_states values (22, 1, 99, 'unpaid', 'newad', now(), '192.168.4.75', null);
insert into action_states values (22, 1, 100, 'pending_review', 'pay', now(), '192.168.4.75', null);
insert into action_states values (22, 1, 101, 'accepted', 'accept', now(), '192.168.4.75', null);

insert into state_params values (22, 1, 101, 'filter_name', 'normal');

-- New ad for 450 CC
INSERT INTO ads (ad_id, list_id, orig_list_time, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, old_price, image, infopage, infopage_title, store_id, salted_passwd) VALUES(23, 23, now(), now(), 'active', 'sell', 'schibsted', 1234566789, 15, 0, 2060, 1, null, 'f', 'f', 'f', '450cc', 'fewfqrfqwarfa', null, null, null, null,null,null, 'es');

INSERT INTO ad_params (ad_id, name, value) VALUES (23, 'regdate', '1999');
INSERT INTO ad_params (ad_id, name, value) VALUES (23, 'mileage', '123');
INSERT INTO ad_params (ad_id, name, value) VALUES (23, 'cubiccms', '15');

INSERT INTO payment_groups (payment_group_id, code, status, added_at) VALUES (122, 38063, 'paid', '2008-02-12 11:07:55.763321');
INSERT INTO payments (payment_group_id, payment_type, pay_amount) VALUES (122, 'ad_action', 0);
insert into pay_log values(23, 'save', 0, 38063,  null, now(), 122, 'SAVE');
insert into pay_log values(24, 'verify', 0, 38063,  null, now(), 122, 'OK');

insert into ad_actions values(23, 1, 'new',  105, 'accepted', 'normal', null, null, 122);

insert into action_states values (23, 1, 102, 'reg', 'initial', now(), '192.168.4.75', null);
insert into action_states values (23, 1, 103, 'unpaid', 'newad', now(), '192.168.4.75', null);
insert into action_states values (23, 1, 104, 'pending_review', 'pay', now(), '192.168.4.75', null);
insert into action_states values (23, 1, 105, 'accepted', 'accept', now(), '192.168.4.75', null);

insert into state_params values (23, 1, 105, 'filter_name', 'normal');

-- New ad for 550 CC
INSERT INTO ads (ad_id, list_id, orig_list_time, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, old_price, image, infopage, infopage_title, store_id, salted_passwd) VALUES(24, 24, now(), now(), 'active', 'sell', 'schibsted', 1234566789, 15, 0, 2060, 1, null, 'f', 'f', 'f', '550cc', 'fewfqrfqwarfa', null, null, null, null,null,null, 'es');

INSERT INTO ad_params (ad_id, name, value) VALUES (24, 'regdate', '1999');
INSERT INTO ad_params (ad_id, name, value) VALUES (24, 'mileage', '123');
INSERT INTO ad_params (ad_id, name, value) VALUES (24, 'cubiccms', '16');

INSERT INTO payment_groups (payment_group_id, code, status, added_at) VALUES (123, 38064, 'paid', '2008-02-12 11:07:55.763321');
INSERT INTO payments (payment_group_id, payment_type, pay_amount) VALUES (123, 'ad_action', 0);
insert into pay_log values(25, 'save', 0, 38064,  null, now(), 123, 'SAVE');
insert into pay_log values(26, 'verify', 0, 38064,  null, now(), 123, 'OK');

insert into ad_actions values(24, 1, 'new',  109, 'accepted', 'normal', null, null, 123);

insert into action_states values (24, 1, 106, 'reg', 'initial', now(), '192.168.4.75', null);
insert into action_states values (24, 1, 107, 'unpaid', 'newad', now(), '192.168.4.75', null);
insert into action_states values (24, 1, 108, 'pending_review', 'pay', now(), '192.168.4.75', null);
insert into action_states values (24, 1, 109, 'accepted', 'accept', now(), '192.168.4.75', null);

insert into state_params values (24, 1, 109, 'filter_name', 'normal');

-- New ad for 600 CC
INSERT INTO ads (ad_id, list_id, orig_list_time, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, old_price, image, infopage, infopage_title, store_id, salted_passwd) VALUES(25, 25, now(), now(), 'active', 'sell', 'schibsted', 1234566789, 15, 0, 2060, 1, null, 'f', 'f', 'f', '600cc', 'fewfqrfqwarfa', null, null, null, null,null,null, 'es');

INSERT INTO ad_params (ad_id, name, value) VALUES (25, 'regdate', '1999');
INSERT INTO ad_params (ad_id, name, value) VALUES (25, 'mileage', '123');
INSERT INTO ad_params (ad_id, name, value) VALUES (25, 'cubiccms', '17');

INSERT INTO payment_groups (payment_group_id, code, status, added_at) VALUES (124, 38065, 'paid', '2008-02-12 11:07:55.763321');
INSERT INTO payments (payment_group_id, payment_type, pay_amount) VALUES (124, 'ad_action', 0);
insert into pay_log values(27, 'save', 0, 38065,  null, now(), 124, 'SAVE');
insert into pay_log values(28, 'verify', 0, 38065,  null, now(), 124, 'OK');

insert into ad_actions values(25, 1, 'new',  113, 'accepted', 'normal', null, null, 124);

insert into action_states values (25, 1, 110, 'reg', 'initial', now(), '192.168.4.75', null);
insert into action_states values (25, 1, 111, 'unpaid', 'newad', now(), '192.168.4.75', null);
insert into action_states values (25, 1, 112, 'pending_review', 'pay', now(), '192.168.4.75', null);
insert into action_states values (25, 1, 113, 'accepted', 'accept', now(), '192.168.4.75', null);

insert into state_params values (25, 1, 113, 'filter_name', 'normal');

-- New ad for 650 CC
INSERT INTO ads (ad_id, list_id, orig_list_time, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, old_price, image, infopage, infopage_title, store_id, salted_passwd) VALUES(26, 26, now(), now(), 'active', 'sell', 'schibsted', 1234566789, 15, 0, 2060, 1, null, 'f', 'f', 'f', '650cc', 'fewfqrfqwarfa', null, null, null, null,null,null, 'es');

INSERT INTO ad_params (ad_id, name, value) VALUES (26, 'regdate', '1999');
INSERT INTO ad_params (ad_id, name, value) VALUES (26, 'mileage', '123');
INSERT INTO ad_params (ad_id, name, value) VALUES (26, 'cubiccms', '18');

INSERT INTO payment_groups (payment_group_id, code, status, added_at) VALUES (125, 38066, 'paid', '2008-02-12 11:07:55.763321');
INSERT INTO payments (payment_group_id, payment_type, pay_amount) VALUES (125, 'ad_action', 0);
insert into pay_log values(29, 'save', 0, 38066,  null, now(), 125, 'SAVE');
insert into pay_log values(30, 'verify', 0, 38066,  null, now(), 125, 'OK');

insert into ad_actions values(26, 1, 'new',  117, 'accepted', 'normal', null, null, 125);

insert into action_states values (26, 1, 114, 'reg', 'initial', now(), '192.168.4.75', null);
insert into action_states values (26, 1, 115, 'unpaid', 'newad', now(), '192.168.4.75', null);
insert into action_states values (26, 1, 116, 'pending_review', 'pay', now(), '192.168.4.75', null);
insert into action_states values (26, 1, 117, 'accepted', 'accept', now(), '192.168.4.75', null);

insert into state_params values (26, 1, 117, 'filter_name', 'normal');

-- New ad for 700 CC
INSERT INTO ads (ad_id, list_id, orig_list_time, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, old_price, image, infopage, infopage_title, store_id, salted_passwd) VALUES(27, 27, now(), now(), 'active', 'sell', 'schibsted', 1234566789, 15, 0, 2060, 1, null, 'f', 'f', 'f', '700cc', 'fewfqrfqwarfa', null, null, null, null,null,null, 'es');

INSERT INTO ad_params (ad_id, name, value) VALUES (27, 'regdate', '1999');
INSERT INTO ad_params (ad_id, name, value) VALUES (27, 'mileage', '123');
INSERT INTO ad_params (ad_id, name, value) VALUES (27, 'cubiccms', '19');

INSERT INTO payment_groups (payment_group_id, code, status, added_at) VALUES (126, 38067, 'paid', '2008-02-12 11:07:55.763321');
INSERT INTO payments (payment_group_id, payment_type, pay_amount) VALUES (126, 'ad_action', 0);
insert into pay_log values(31, 'save', 0, 38067,  null, now(), 126, 'SAVE');
insert into pay_log values(32, 'verify', 0, 38067,  null, now(), 126, 'OK');

insert into ad_actions values(27, 1, 'new',  121, 'accepted', 'normal', null, null, 126);

insert into action_states values (27, 1, 118, 'reg', 'initial', now(), '192.168.4.75', null);
insert into action_states values (27, 1, 119, 'unpaid', 'newad', now(), '192.168.4.75', null);
insert into action_states values (27, 1, 120, 'pending_review', 'pay', now(), '192.168.4.75', null);
insert into action_states values (27, 1, 121, 'accepted', 'accept', now(), '192.168.4.75', null);

insert into state_params values (27, 1, 121, 'filter_name', 'normal');

-- New ad for 800 CC
INSERT INTO ads (ad_id, list_id, orig_list_time, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, old_price, image, infopage, infopage_title, store_id, salted_passwd) VALUES(28, 28, now(), now(), 'active', 'sell', 'schibsted', 1234566789, 15, 0, 2060, 1, null, 'f', 'f', 'f', '800cc', 'fewfqrfqwarfa', null, null, null, null,null,null, 'es');

INSERT INTO ad_params (ad_id, name, value) VALUES (28, 'regdate', '1999');
INSERT INTO ad_params (ad_id, name, value) VALUES (28, 'mileage', '123');
INSERT INTO ad_params (ad_id, name, value) VALUES (28, 'cubiccms', '20');

INSERT INTO payment_groups (payment_group_id, code, status, added_at) VALUES (127, 38068, 'paid', '2008-02-12 11:07:55.763321');
INSERT INTO payments (payment_group_id, payment_type, pay_amount) VALUES (127, 'ad_action', 0);
insert into pay_log values(33, 'save', 0, 38068,  null, now(), 127, 'SAVE');
insert into pay_log values(34, 'verify', 0, 38068,  null, now(), 127, 'OK');

insert into ad_actions values(28, 1, 'new',  125, 'accepted', 'normal', null, null, 127);

insert into action_states values (28, 1, 122, 'reg', 'initial', now(), '192.168.4.75', null);
insert into action_states values (28, 1, 123, 'unpaid', 'newad', now(), '192.168.4.75', null);
insert into action_states values (28, 1, 124, 'pending_review', 'pay', now(), '192.168.4.75', null);
insert into action_states values (28, 1, 125, 'accepted', 'accept', now(), '192.168.4.75', null);

insert into state_params values (28, 1, 125, 'filter_name', 'normal');

-- New ad for 850 CC
INSERT INTO ads (ad_id, list_id, orig_list_time, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, old_price, image, infopage, infopage_title, store_id, salted_passwd) VALUES(29, 29, now(), now(), 'active', 'sell', 'schibsted', 1234566789, 15, 0, 2060, 1, null, 'f', 'f', 'f', '850cc', 'fewfqrfqwarfa', null, null, null, null,null,null, 'es');

INSERT INTO ad_params (ad_id, name, value) VALUES (29, 'regdate', '1999');
INSERT INTO ad_params (ad_id, name, value) VALUES (29, 'mileage', '123');
INSERT INTO ad_params (ad_id, name, value) VALUES (29, 'cubiccms', '21');

INSERT INTO payment_groups (payment_group_id, code, status, added_at) VALUES (128, 38069, 'paid', '2008-02-12 11:07:55.763321');
INSERT INTO payments (payment_group_id, payment_type, pay_amount) VALUES (128, 'ad_action', 0);
insert into pay_log values(35, 'save', 0, 38069,  null, now(), 128, 'SAVE');
insert into pay_log values(36, 'verify', 0, 38069,  null, now(), 128, 'OK');

insert into ad_actions values(29, 1, 'new',  129, 'accepted', 'normal', null, null, 128);

insert into action_states values (29, 1, 126, 'reg', 'initial', now(), '192.168.4.75', null);
insert into action_states values (29, 1, 127, 'unpaid', 'newad', now(), '192.168.4.75', null);
insert into action_states values (29, 1, 128, 'pending_review', 'pay', now(), '192.168.4.75', null);
insert into action_states values (29, 1, 129, 'accepted', 'accept', now(), '192.168.4.75', null);

insert into state_params values (29, 1, 129, 'filter_name', 'normal');

-- New ad for 900 CC
INSERT INTO ads (ad_id, list_id, orig_list_time, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, old_price, image, infopage, infopage_title, store_id, salted_passwd) VALUES(30, 30, now(), now(), 'active', 'sell', 'schibsted', 1234566789, 15, 0, 2060, 1, null, 'f', 'f', 'f', '900cc', 'fewfqrfqwarfa', null, null, null, null,null,null, 'es');

INSERT INTO ad_params (ad_id, name, value) VALUES (30, 'regdate', '1999');
INSERT INTO ad_params (ad_id, name, value) VALUES (30, 'mileage', '123');
INSERT INTO ad_params (ad_id, name, value) VALUES (30, 'cubiccms', '22');

INSERT INTO payment_groups (payment_group_id, code, status, added_at) VALUES (129, 38070, 'paid', '2008-02-12 11:07:55.763321');
INSERT INTO payments (payment_group_id, payment_type, pay_amount) VALUES (129, 'ad_action', 0);
insert into pay_log values(37, 'save', 0, 38070,  null, now(), 129, 'SAVE');
insert into pay_log values(38, 'verify', 0, 38070,  null, now(), 129, 'OK');

insert into ad_actions values(30, 1, 'new',  133, 'accepted', 'normal', null, null, 129);

insert into action_states values (30, 1, 130, 'reg', 'initial', now(), '192.168.4.75', null);
insert into action_states values (30, 1, 131, 'unpaid', 'newad', now(), '192.168.4.75', null);
insert into action_states values (30, 1, 132, 'pending_review', 'pay', now(), '192.168.4.75', null);
insert into action_states values (30, 1, 133, 'accepted', 'accept', now(), '192.168.4.75', null);

insert into state_params values (30, 1, 133, 'filter_name', 'normal');

-- New ad for 950 CC
INSERT INTO ads (ad_id, list_id, orig_list_time, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, old_price, image, infopage, infopage_title, store_id, salted_passwd) VALUES(31, 31, now(), now(), 'active', 'sell', 'schibsted', 1234566789, 15, 0, 2060, 1, null, 'f', 'f', 'f', '950cc', 'fewfqrfqwarfa', null, null, null, null,null,null, 'es');

INSERT INTO ad_params (ad_id, name, value) VALUES (31, 'regdate', '1999');
INSERT INTO ad_params (ad_id, name, value) VALUES (31, 'mileage', '123');
INSERT INTO ad_params (ad_id, name, value) VALUES (31, 'cubiccms', '23');

INSERT INTO payment_groups (payment_group_id, code, status, added_at) VALUES (130, 38071, 'paid', '2008-02-12 11:07:55.763321');
INSERT INTO payments (payment_group_id, payment_type, pay_amount) VALUES (130, 'ad_action', 0);
insert into pay_log values(39, 'save', 0, 38071,  null, now(), 130, 'SAVE');
insert into pay_log values(40, 'verify', 0, 38071,  null, now(), 130, 'OK');

insert into ad_actions values(31, 1, 'new',  137, 'accepted', 'normal', null, null, 130);

insert into action_states values (31, 1, 134, 'reg', 'initial', now(), '192.168.4.75', null);
insert into action_states values (31, 1, 135, 'unpaid', 'newad', now(), '192.168.4.75', null);
insert into action_states values (31, 1, 136, 'pending_review', 'pay', now(), '192.168.4.75', null);
insert into action_states values (31, 1, 137, 'accepted', 'accept', now(), '192.168.4.75', null);

insert into state_params values (31, 1, 137, 'filter_name', 'normal');

-- New ad for Acima de 1000 CC
INSERT INTO ads (ad_id, list_id, orig_list_time, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, old_price, image, infopage, infopage_title, store_id, salted_passwd) VALUES(32, 32, now(), now(), 'active', 'sell', 'schibsted', 1234566789, 11, 0, 2060, 1, null, 'f', 'f', 'f', 'Acima de 1000cc', 'fewfqrfqwarfa', null, null, null, null,null,null, 'es');

INSERT INTO ad_params (ad_id, name, value) VALUES (32, 'regdate', '1999');
INSERT INTO ad_params (ad_id, name, value) VALUES (32, 'mileage', '123');
INSERT INTO ad_params (ad_id, name, value) VALUES (32, 'cubiccms', '7');

INSERT INTO payment_groups (payment_group_id, code, status, added_at) VALUES (131, 38072, 'paid', '2008-02-12 11:07:55.763321');
INSERT INTO payments (payment_group_id, payment_type, pay_amount) VALUES (131, 'ad_action', 0);
insert into pay_log values(41, 'save', 0, 38072,  null, now(), 131, 'SAVE');
insert into pay_log values(42, 'verify', 0, 38072,  null, now(), 131, 'OK');

insert into ad_actions values(32, 1, 'new',  141, 'accepted', 'normal', null, null, 131);

insert into action_states values (32, 1, 138, 'reg', 'initial', now(), '192.168.4.75', null);
insert into action_states values (32, 1, 139, 'unpaid', 'newad', now(), '192.168.4.75', null);
insert into action_states values (32, 1, 140, 'pending_review', 'pay', now(), '192.168.4.75', null);
insert into action_states values (32, 1, 141, 'accepted', 'accept', now(), '192.168.4.75', null);

insert into state_params values (32, 1, 141, 'filter_name', 'normal');

