
BEGIN;
SET CONSTRAINTS ALL DEFERRED;

TRUNCATE TABLE public.admins CASCADE;
TRUNCATE TABLE public.abuse_locks CASCADE;
TRUNCATE TABLE public.abuse_reporters CASCADE;
TRUNCATE TABLE public.users CASCADE;
TRUNCATE TABLE public.abuse_reports CASCADE;
TRUNCATE TABLE public.ads CASCADE;
TRUNCATE TABLE public.dashboard_ads CASCADE;
TRUNCATE TABLE public.payment_groups CASCADE;
TRUNCATE TABLE public.ad_actions CASCADE;
TRUNCATE TABLE public.action_params CASCADE;
TRUNCATE TABLE public.stores CASCADE;
TRUNCATE TABLE public.tokens CASCADE;
TRUNCATE TABLE public.action_states CASCADE;
TRUNCATE TABLE public.ad_changes CASCADE;
TRUNCATE TABLE public.ad_codes CASCADE;
TRUNCATE TABLE public.ad_image_changes CASCADE;
TRUNCATE TABLE public.ad_images CASCADE;
TRUNCATE TABLE public.ad_images_digests CASCADE;
TRUNCATE TABLE public.ad_media CASCADE;
TRUNCATE TABLE public.ad_media_changes CASCADE;
TRUNCATE TABLE public.ad_params CASCADE;
TRUNCATE TABLE public.ad_queues CASCADE;
TRUNCATE TABLE public.admin_privs CASCADE;
TRUNCATE TABLE public.bid_ads CASCADE;
TRUNCATE TABLE public.bid_bids CASCADE;
TRUNCATE TABLE public.bid_media CASCADE;
TRUNCATE TABLE public.block_lists CASCADE;
TRUNCATE TABLE public.block_rules CASCADE;
TRUNCATE TABLE public.block_rule_conditions CASCADE;
TRUNCATE TABLE public.blocked_items CASCADE;
TRUNCATE TABLE public.conf CASCADE;
TRUNCATE TABLE public.event_log CASCADE;
TRUNCATE TABLE public.example CASCADE;
TRUNCATE TABLE public.filters CASCADE;
TRUNCATE TABLE public.hold_mail_params CASCADE;
TRUNCATE TABLE public.iteminfo_items CASCADE;
TRUNCATE TABLE public.iteminfo_data CASCADE;
TRUNCATE TABLE public.mail_log CASCADE;
TRUNCATE TABLE public.mail_queue CASCADE;
TRUNCATE TABLE public.mama_attribute_wordlists CASCADE;
TRUNCATE TABLE public.mama_attribute_categories CASCADE;
TRUNCATE TABLE public.mama_main_backup CASCADE;
TRUNCATE TABLE public.mama_attribute_categories_backup CASCADE;
TRUNCATE TABLE public.mama_attribute_wordlists_backup CASCADE;
TRUNCATE TABLE public.mama_attribute_words CASCADE;
TRUNCATE TABLE public.mama_attribute_words_backup CASCADE;
TRUNCATE TABLE public.mama_exception_lists CASCADE;
TRUNCATE TABLE public.mama_exception_lists_backup CASCADE;
TRUNCATE TABLE public.mama_exception_words CASCADE;
TRUNCATE TABLE public.mama_exception_words_backup CASCADE;
TRUNCATE TABLE public.mama_wordlists CASCADE;
TRUNCATE TABLE public.mama_wordlists_backup CASCADE;
TRUNCATE TABLE public.mama_words CASCADE;
TRUNCATE TABLE public.mama_words_backup CASCADE;
TRUNCATE TABLE public.most_popular_ads CASCADE;
TRUNCATE TABLE public.notices CASCADE;
TRUNCATE TABLE public.on_call CASCADE;
TRUNCATE TABLE public.on_call_actions CASCADE;
TRUNCATE TABLE public.pageviews_per_reg_cat CASCADE;
TRUNCATE TABLE public.pay_log CASCADE;
TRUNCATE TABLE public.pay_log_references CASCADE;
TRUNCATE TABLE public.payments CASCADE;
TRUNCATE TABLE public.pricelist CASCADE;
TRUNCATE TABLE public.redir_stats CASCADE;
TRUNCATE TABLE public.review_log CASCADE;
TRUNCATE TABLE public.sms_users CASCADE;
TRUNCATE TABLE public.sms_log CASCADE;
TRUNCATE TABLE public.watch_users CASCADE;
TRUNCATE TABLE public.watch_queries CASCADE;
TRUNCATE TABLE public.sms_log_watch CASCADE;
TRUNCATE TABLE public.state_params CASCADE;
TRUNCATE TABLE public.stats_daily CASCADE;
TRUNCATE TABLE public.stats_daily_ad_actions CASCADE;
TRUNCATE TABLE public.stats_hourly CASCADE;
TRUNCATE TABLE public.store_actions CASCADE;
TRUNCATE TABLE public.store_action_states CASCADE;
TRUNCATE TABLE public.store_changes CASCADE;
TRUNCATE TABLE public.store_login_tokens CASCADE;
TRUNCATE TABLE public.store_params CASCADE;
TRUNCATE TABLE public.synonyms CASCADE;
TRUNCATE TABLE public.trans_queue CASCADE;
TRUNCATE TABLE public.unfinished_ads CASCADE;
TRUNCATE TABLE public.user_params CASCADE;
TRUNCATE TABLE public.user_testimonial CASCADE;
TRUNCATE TABLE public.visitor CASCADE;
TRUNCATE TABLE public.visits CASCADE;
TRUNCATE TABLE public.vouchers CASCADE;
TRUNCATE TABLE public.voucher_actions CASCADE;
TRUNCATE TABLE public.voucher_states CASCADE;
TRUNCATE TABLE public.watch_ads CASCADE;
TRUNCATE TABLE public.accounts CASCADE;

COMMIT;


--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET client_encoding = 'LATIN1';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;

SET search_path = public, pg_catalog;



--
-- Name: mail_queue_mail_queue_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dany
--

SELECT pg_catalog.setval('mail_queue_mail_queue_id_seq', 1, false);


--
-- Name: abuse_locks_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dany
--

SELECT pg_catalog.setval('abuse_locks_id_seq', 1, false);


--
-- Name: report_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dany
--

SELECT pg_catalog.setval('report_id_seq', 1, false);


--
-- Name: action_states_state_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dany
--

SELECT pg_catalog.setval('action_states_state_id_seq', 557, true);


--
-- Name: admins_admin_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dany
--

SELECT pg_catalog.setval('admins_admin_id_seq', 22, true);


--
-- Name: ads_ad_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dany
--

SELECT pg_catalog.setval('ads_ad_id_seq', 93, true);


--
-- Name: adwatch_code_seq; Type: SEQUENCE SET; Schema: public; Owner: dany
--

SELECT pg_catalog.setval('adwatch_code_seq', 1, false);


--
-- Name: bid_ads_bid_ad_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dany
--

SELECT pg_catalog.setval('bid_ads_bid_ad_id_seq', 1, false);


--
-- Name: bid_bids_bid_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dany
--

SELECT pg_catalog.setval('bid_bids_bid_id_seq', 1, false);


--
-- Name: block_lists_list_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dany
--

SELECT pg_catalog.setval('block_lists_list_id_seq', 100, false);


--
-- Name: block_rule_conditions_condition_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dany
--

SELECT pg_catalog.setval('block_rule_conditions_condition_id_seq', 18, true);


--
-- Name: block_rules_rule_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dany
--

SELECT pg_catalog.setval('block_rules_rule_id_seq', 100, false);


--
-- Name: blocked_items_item_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dany
--

SELECT pg_catalog.setval('blocked_items_item_id_seq', 20, true);


--
--



--
-- Name: data_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dany
--

SELECT pg_catalog.setval('data_id_seq', 1294, true);


--
-- Name: event_log_event_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dany
--

SELECT pg_catalog.setval('event_log_event_id_seq', 8, true);


--
-- Name: filters_filter_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dany
--

SELECT pg_catalog.setval('filters_filter_id_seq', 1, false);


--
-- Name: hold_mail_params_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dany
--

SELECT pg_catalog.setval('hold_mail_params_id_seq', 1, false);


--
-- Name: item_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dany
--

SELECT pg_catalog.setval('item_id_seq', 59, true);


--
-- Name: list_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dany
--

SELECT pg_catalog.setval('list_id_seq', 8000064, true);


--
-- Name: mail_log_mail_log_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dany
--

SELECT pg_catalog.setval('mail_log_mail_log_id_seq', 1, false);


--
-- Name: mama_attribute_wordlists_attribute_wordlist_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dany
--

SELECT pg_catalog.setval('mama_attribute_wordlists_attribute_wordlist_id_seq', 1, false);


--
-- Name: mama_attribute_words_word_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dany
--

SELECT pg_catalog.setval('mama_attribute_words_word_id_seq', 1, false);


--
-- Name: mama_exception_lists_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dany
--

SELECT pg_catalog.setval('mama_exception_lists_id_seq', 1, false);


--
-- Name: mama_exception_words_exception_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dany
--

SELECT pg_catalog.setval('mama_exception_words_exception_id_seq', 1, false);


--
-- Name: mama_main_backup_backup_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dany
--

SELECT pg_catalog.setval('mama_main_backup_backup_id_seq', 1, false);


--
-- Name: mama_wordlists_wordlist_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dany
--

SELECT pg_catalog.setval('mama_wordlists_wordlist_id_seq', 1, false);


--
-- Name: mama_words_word_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dany
--

SELECT pg_catalog.setval('mama_words_word_id_seq', 1, false);


--
-- Name: most_popular_ads_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dany
--

SELECT pg_catalog.setval('most_popular_ads_id_seq', 1, false);


--
-- Name: next_image_id; Type: SEQUENCE SET; Schema: public; Owner: dany
--

SELECT pg_catalog.setval('next_image_id', 1, false);


--
-- Name: notices_notice_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dany
--

SELECT pg_catalog.setval('notices_notice_id_seq', 1, false);


--
-- Name: on_call_actions_action_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dany
--

SELECT pg_catalog.setval('on_call_actions_action_id_seq', 1, false);


--
-- Name: on_call_on_call_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dany
--

SELECT pg_catalog.setval('on_call_on_call_id_seq', 1, false);


--
-- Name: order_id_suffix_seq; Type: SEQUENCE SET; Schema: public; Owner: dany
--

SELECT pg_catalog.setval('order_id_suffix_seq', 1, false);


--
-- Name: pay_code_seq; Type: SEQUENCE SET; Schema: public; Owner: dany
--

SELECT pg_catalog.setval('pay_code_seq', 130, true);


--
-- Name: pay_log_pay_log_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dany
--

SELECT pg_catalog.setval('pay_log_pay_log_id_seq', 148, true);


--
-- Name: payment_groups_payment_group_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dany
--

SELECT pg_catalog.setval('payment_groups_payment_group_id_seq', 133, true);


--
-- Name: purchase_detail_purchase_detail_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dany
--

SELECT pg_catalog.setval('purchase_detail_purchase_detail_id_seq', 1, false);


--
-- Name: purchase_purchase_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dany
--

SELECT pg_catalog.setval('purchase_purchase_id_seq', 1, false);


--
-- Name: purchase_states_purchase_state_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dany
--

SELECT pg_catalog.setval('purchase_states_purchase_state_id_seq', 1, false);


--
-- Name: redir_stats_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dany
--

SELECT pg_catalog.setval('redir_stats_id_seq', 67, true);


--
-- Name: reporter_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dany
--

SELECT pg_catalog.setval('reporter_id_seq', 1, false);


--
-- Name: sms_log_sms_log_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dany
--

SELECT pg_catalog.setval('sms_log_sms_log_id_seq', 1, false);


--
-- Name: sms_users_sms_user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dany
--

SELECT pg_catalog.setval('sms_users_sms_user_id_seq', 1, false);


--
-- Name: stats_daily_ad_actions_stat_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dany
--

SELECT pg_catalog.setval('stats_daily_ad_actions_stat_id_seq', 1, false);


--
-- Name: store_action_states_state_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dany
--

SELECT pg_catalog.setval('store_action_states_state_id_seq', 1, false);


--
-- Name: store_login_tokens_store_login_token_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dany
--

SELECT pg_catalog.setval('store_login_tokens_store_login_token_id_seq', 1, false);


--
-- Name: stores_store_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dany
--

SELECT pg_catalog.setval('stores_store_id_seq', 1, false);


--
-- Name: synonyms_syn_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dany
--

SELECT pg_catalog.setval('synonyms_syn_id_seq', 245, true);


--
-- Name: tokens_token_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dany
--

SELECT pg_catalog.setval('tokens_token_id_seq', 556, true);


--
-- Name: trans_queue_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dany
--

SELECT pg_catalog.setval('trans_queue_id_seq', 72, true);


--
-- Name: uid_seq; Type: SEQUENCE SET; Schema: public; Owner: dany
--

SELECT pg_catalog.setval('uid_seq', 51, true);


--
-- Name: unfinished_ads_unf_ad_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dany
--

SELECT pg_catalog.setval('unfinished_ads_unf_ad_id_seq', 1, false);


--
-- Name: user_testimonial_user_testimonial_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dany
--

SELECT pg_catalog.setval('user_testimonial_user_testimonial_id_seq', 1, false);


--
-- Name: users_user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dany
--

SELECT pg_catalog.setval('users_user_id_seq', 52, true);


--
-- Name: verify_code_seq; Type: SEQUENCE SET; Schema: public; Owner: dany
--

SELECT pg_catalog.setval('verify_code_seq', 90, true);


--
-- Name: voucher_actions_voucher_action_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dany
--

SELECT pg_catalog.setval('voucher_actions_voucher_action_id_seq', 1, false);


--
-- Name: voucher_states_voucher_state_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dany
--

SELECT pg_catalog.setval('voucher_states_voucher_state_id_seq', 1, false);


--
-- Name: vouchers_voucher_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dany
--

SELECT pg_catalog.setval('vouchers_voucher_id_seq', 1, false);


--
-- Name: watch_users_watch_user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dany
--

SELECT pg_catalog.setval('watch_users_watch_user_id_seq', 1, false);


SET search_path = blocket_2009, pg_catalog;

--
-- Data for Name: ads; Type: TABLE DATA; Schema: blocket_2009; Owner: dany
--



--
-- Data for Name: payment_groups; Type: TABLE DATA; Schema: blocket_2009; Owner: dany
--



--
-- Data for Name: ad_actions; Type: TABLE DATA; Schema: blocket_2009; Owner: dany
--



--
-- Data for Name: action_params; Type: TABLE DATA; Schema: blocket_2009; Owner: dany
--



--
-- Data for Name: tokens; Type: TABLE DATA; Schema: blocket_2009; Owner: dany
--



--
-- Data for Name: action_states; Type: TABLE DATA; Schema: blocket_2009; Owner: dany
--



--
-- Data for Name: ad_changes; Type: TABLE DATA; Schema: blocket_2009; Owner: dany
--



--
-- Data for Name: ad_image_changes; Type: TABLE DATA; Schema: blocket_2009; Owner: dany
--



--
-- Data for Name: ad_images; Type: TABLE DATA; Schema: blocket_2009; Owner: dany
--



--
-- Data for Name: ad_media; Type: TABLE DATA; Schema: blocket_2009; Owner: dany
--



--
-- Data for Name: ad_media_changes; Type: TABLE DATA; Schema: blocket_2009; Owner: dany
--



--
-- Data for Name: ad_params; Type: TABLE DATA; Schema: blocket_2009; Owner: dany
--



--
-- Data for Name: mail_queue; Type: TABLE DATA; Schema: blocket_2009; Owner: dany
--



--
-- Data for Name: notices; Type: TABLE DATA; Schema: blocket_2009; Owner: dany
--



--
-- Data for Name: pay_log; Type: TABLE DATA; Schema: blocket_2009; Owner: dany
--



--
-- Data for Name: pay_log_references; Type: TABLE DATA; Schema: blocket_2009; Owner: dany
--



--
-- Data for Name: payments; Type: TABLE DATA; Schema: blocket_2009; Owner: dany
--



--
-- Data for Name: purchase; Type: TABLE DATA; Schema: blocket_2009; Owner: dany
--



--
-- Data for Name: purchase_detail; Type: TABLE DATA; Schema: blocket_2009; Owner: dany
--



--
-- Data for Name: purchase_states; Type: TABLE DATA; Schema: blocket_2009; Owner: dany
--



--
-- Data for Name: state_params; Type: TABLE DATA; Schema: blocket_2009; Owner: dany
--



--
-- Data for Name: voucher_actions; Type: TABLE DATA; Schema: blocket_2009; Owner: dany
--



--
-- Data for Name: voucher_states; Type: TABLE DATA; Schema: blocket_2009; Owner: dany
--



SET search_path = blocket_2010, pg_catalog;

--
-- Data for Name: ads; Type: TABLE DATA; Schema: blocket_2010; Owner: dany
--



--
-- Data for Name: payment_groups; Type: TABLE DATA; Schema: blocket_2010; Owner: dany
--



--
-- Data for Name: ad_actions; Type: TABLE DATA; Schema: blocket_2010; Owner: dany
--



--
-- Data for Name: action_params; Type: TABLE DATA; Schema: blocket_2010; Owner: dany
--



--
-- Data for Name: tokens; Type: TABLE DATA; Schema: blocket_2010; Owner: dany
--



--
-- Data for Name: action_states; Type: TABLE DATA; Schema: blocket_2010; Owner: dany
--



--
-- Data for Name: ad_changes; Type: TABLE DATA; Schema: blocket_2010; Owner: dany
--



--
-- Data for Name: ad_image_changes; Type: TABLE DATA; Schema: blocket_2010; Owner: dany
--



--
-- Data for Name: ad_images; Type: TABLE DATA; Schema: blocket_2010; Owner: dany
--



--
-- Data for Name: ad_media; Type: TABLE DATA; Schema: blocket_2010; Owner: dany
--



--
-- Data for Name: ad_media_changes; Type: TABLE DATA; Schema: blocket_2010; Owner: dany
--



--
-- Data for Name: ad_params; Type: TABLE DATA; Schema: blocket_2010; Owner: dany
--



--
-- Data for Name: mail_queue; Type: TABLE DATA; Schema: blocket_2010; Owner: dany
--



--
-- Data for Name: notices; Type: TABLE DATA; Schema: blocket_2010; Owner: dany
--



--
-- Data for Name: pay_log; Type: TABLE DATA; Schema: blocket_2010; Owner: dany
--



--
-- Data for Name: pay_log_references; Type: TABLE DATA; Schema: blocket_2010; Owner: dany
--



--
-- Data for Name: payments; Type: TABLE DATA; Schema: blocket_2010; Owner: dany
--



--
-- Data for Name: purchase; Type: TABLE DATA; Schema: blocket_2010; Owner: dany
--



--
-- Data for Name: purchase_detail; Type: TABLE DATA; Schema: blocket_2010; Owner: dany
--



--
-- Data for Name: purchase_states; Type: TABLE DATA; Schema: blocket_2010; Owner: dany
--



--
-- Data for Name: state_params; Type: TABLE DATA; Schema: blocket_2010; Owner: dany
--



--
-- Data for Name: voucher_actions; Type: TABLE DATA; Schema: blocket_2010; Owner: dany
--



--
-- Data for Name: voucher_states; Type: TABLE DATA; Schema: blocket_2010; Owner: dany
--



SET search_path = blocket_2011, pg_catalog;

--
-- Data for Name: ads; Type: TABLE DATA; Schema: blocket_2011; Owner: dany
--



--
-- Data for Name: payment_groups; Type: TABLE DATA; Schema: blocket_2011; Owner: dany
--



--
-- Data for Name: ad_actions; Type: TABLE DATA; Schema: blocket_2011; Owner: dany
--



--
-- Data for Name: action_params; Type: TABLE DATA; Schema: blocket_2011; Owner: dany
--



--
-- Data for Name: tokens; Type: TABLE DATA; Schema: blocket_2011; Owner: dany
--



--
-- Data for Name: action_states; Type: TABLE DATA; Schema: blocket_2011; Owner: dany
--



--
-- Data for Name: ad_changes; Type: TABLE DATA; Schema: blocket_2011; Owner: dany
--



--
-- Data for Name: ad_image_changes; Type: TABLE DATA; Schema: blocket_2011; Owner: dany
--



--
-- Data for Name: ad_images; Type: TABLE DATA; Schema: blocket_2011; Owner: dany
--



--
-- Data for Name: ad_media; Type: TABLE DATA; Schema: blocket_2011; Owner: dany
--



--
-- Data for Name: ad_media_changes; Type: TABLE DATA; Schema: blocket_2011; Owner: dany
--



--
-- Data for Name: ad_params; Type: TABLE DATA; Schema: blocket_2011; Owner: dany
--



--
-- Data for Name: mail_queue; Type: TABLE DATA; Schema: blocket_2011; Owner: dany
--



--
-- Data for Name: notices; Type: TABLE DATA; Schema: blocket_2011; Owner: dany
--



--
-- Data for Name: pay_log; Type: TABLE DATA; Schema: blocket_2011; Owner: dany
--



--
-- Data for Name: pay_log_references; Type: TABLE DATA; Schema: blocket_2011; Owner: dany
--



--
-- Data for Name: payments; Type: TABLE DATA; Schema: blocket_2011; Owner: dany
--



--
-- Data for Name: purchase; Type: TABLE DATA; Schema: blocket_2011; Owner: dany
--



--
-- Data for Name: purchase_detail; Type: TABLE DATA; Schema: blocket_2011; Owner: dany
--



--
-- Data for Name: purchase_states; Type: TABLE DATA; Schema: blocket_2011; Owner: dany
--



--
-- Data for Name: state_params; Type: TABLE DATA; Schema: blocket_2011; Owner: dany
--



--
-- Data for Name: voucher_actions; Type: TABLE DATA; Schema: blocket_2011; Owner: dany
--



--
-- Data for Name: voucher_states; Type: TABLE DATA; Schema: blocket_2011; Owner: dany
--



SET search_path = blocket_2012, pg_catalog;

--
-- Data for Name: ads; Type: TABLE DATA; Schema: blocket_2012; Owner: dany
--



--
-- Data for Name: payment_groups; Type: TABLE DATA; Schema: blocket_2012; Owner: dany
--



--
-- Data for Name: ad_actions; Type: TABLE DATA; Schema: blocket_2012; Owner: dany
--



--
-- Data for Name: action_params; Type: TABLE DATA; Schema: blocket_2012; Owner: dany
--



--
-- Data for Name: tokens; Type: TABLE DATA; Schema: blocket_2012; Owner: dany
--



--
-- Data for Name: action_states; Type: TABLE DATA; Schema: blocket_2012; Owner: dany
--



--
-- Data for Name: ad_changes; Type: TABLE DATA; Schema: blocket_2012; Owner: dany
--



--
-- Data for Name: ad_image_changes; Type: TABLE DATA; Schema: blocket_2012; Owner: dany
--



--
-- Data for Name: ad_images; Type: TABLE DATA; Schema: blocket_2012; Owner: dany
--



--
-- Data for Name: ad_media; Type: TABLE DATA; Schema: blocket_2012; Owner: dany
--



--
-- Data for Name: ad_media_changes; Type: TABLE DATA; Schema: blocket_2012; Owner: dany
--



--
-- Data for Name: ad_params; Type: TABLE DATA; Schema: blocket_2012; Owner: dany
--



--
-- Data for Name: mail_queue; Type: TABLE DATA; Schema: blocket_2012; Owner: dany
--



--
-- Data for Name: notices; Type: TABLE DATA; Schema: blocket_2012; Owner: dany
--



--
-- Data for Name: pay_log; Type: TABLE DATA; Schema: blocket_2012; Owner: dany
--



--
-- Data for Name: pay_log_references; Type: TABLE DATA; Schema: blocket_2012; Owner: dany
--



--
-- Data for Name: payments; Type: TABLE DATA; Schema: blocket_2012; Owner: dany
--



--
-- Data for Name: purchase; Type: TABLE DATA; Schema: blocket_2012; Owner: dany
--



--
-- Data for Name: purchase_detail; Type: TABLE DATA; Schema: blocket_2012; Owner: dany
--



--
-- Data for Name: purchase_states; Type: TABLE DATA; Schema: blocket_2012; Owner: dany
--



--
-- Data for Name: state_params; Type: TABLE DATA; Schema: blocket_2012; Owner: dany
--



--
-- Data for Name: voucher_actions; Type: TABLE DATA; Schema: blocket_2012; Owner: dany
--



--
-- Data for Name: voucher_states; Type: TABLE DATA; Schema: blocket_2012; Owner: dany
--



SET search_path = blocket_2013, pg_catalog;

--
-- Data for Name: ads; Type: TABLE DATA; Schema: blocket_2013; Owner: dany
--



--
-- Data for Name: payment_groups; Type: TABLE DATA; Schema: blocket_2013; Owner: dany
--



--
-- Data for Name: ad_actions; Type: TABLE DATA; Schema: blocket_2013; Owner: dany
--



--
-- Data for Name: action_params; Type: TABLE DATA; Schema: blocket_2013; Owner: dany
--



--
-- Data for Name: tokens; Type: TABLE DATA; Schema: blocket_2013; Owner: dany
--



--
-- Data for Name: action_states; Type: TABLE DATA; Schema: blocket_2013; Owner: dany
--



--
-- Data for Name: ad_changes; Type: TABLE DATA; Schema: blocket_2013; Owner: dany
--



--
-- Data for Name: ad_image_changes; Type: TABLE DATA; Schema: blocket_2013; Owner: dany
--



--
-- Data for Name: ad_images; Type: TABLE DATA; Schema: blocket_2013; Owner: dany
--



--
-- Data for Name: ad_media; Type: TABLE DATA; Schema: blocket_2013; Owner: dany
--



--
-- Data for Name: ad_media_changes; Type: TABLE DATA; Schema: blocket_2013; Owner: dany
--



--
-- Data for Name: ad_params; Type: TABLE DATA; Schema: blocket_2013; Owner: dany
--



--
-- Data for Name: mail_queue; Type: TABLE DATA; Schema: blocket_2013; Owner: dany
--



--
-- Data for Name: notices; Type: TABLE DATA; Schema: blocket_2013; Owner: dany
--



--
-- Data for Name: pay_log; Type: TABLE DATA; Schema: blocket_2013; Owner: dany
--



--
-- Data for Name: pay_log_references; Type: TABLE DATA; Schema: blocket_2013; Owner: dany
--



--
-- Data for Name: payments; Type: TABLE DATA; Schema: blocket_2013; Owner: dany
--



--
-- Data for Name: purchase; Type: TABLE DATA; Schema: blocket_2013; Owner: dany
--



--
-- Data for Name: purchase_detail; Type: TABLE DATA; Schema: blocket_2013; Owner: dany
--



--
-- Data for Name: purchase_states; Type: TABLE DATA; Schema: blocket_2013; Owner: dany
--



--
-- Data for Name: state_params; Type: TABLE DATA; Schema: blocket_2013; Owner: dany
--



--
-- Data for Name: voucher_actions; Type: TABLE DATA; Schema: blocket_2013; Owner: dany
--



--
-- Data for Name: voucher_states; Type: TABLE DATA; Schema: blocket_2013; Owner: dany
--



SET search_path = public, pg_catalog;

--
-- Data for Name: admins; Type: TABLE DATA; Schema: public; Owner: dany
--

INSERT INTO admins VALUES (2, 'erik', '2d541162141d281abb9f5ba1b14f8072a1d2d29b', 'Erik', 'erik@jabber', 'erik@blocket.se', '070-111111', 'active');
INSERT INTO admins VALUES (3, 'zakay', '2d541162141d281abb9f5ba1b14f8072a1d2d29b', 'Zakay', 'zakay@jabber', 'zakay@blocket.se', '070-111111', 'active');
INSERT INTO admins VALUES (4, 'thomas', 'a94a8fe5ccb19ba61c4c0873d391e987982fbbd3', 'Thomas', 'thomas@jabber', 'test@schibstediberica.es', '070-111111', 'active');
INSERT INTO admins VALUES (5, 'kjell', '430985da851e9223368e820ebde5beaf6c6ef1cc', 'Kjell', 'kjell@jabber', 'kjell@blocket.se', '0709-6459256', 'active');
INSERT INTO admins VALUES (6, 'torsten', '430985da851e9223368e820ebde5beaf6c6ef1cc', 'Torsten Svensson', 'torsten@jabber', 'svara-inte@blocket.se', '0709-6459256', 'deleted');
INSERT INTO admins VALUES (50, 'mama', '67b757a4f76e5fda3fe86c8a9009630a5e53a053', 'M.A.M.A.', 'blocket1@jabber', 'blocket1@blocket.se', '0733555501', 'active');
INSERT INTO admins VALUES (51, 'bender00', '67b757a4f76e5fda3fe86c8a9009630a5e53a053', 'M.A.M.A. bender 00', NULL, NULL, NULL, 'active');
INSERT INTO admins VALUES (52, 'bender01', '67b757a4f76e5fda3fe86c8a9009630a5e53a053', 'M.A.M.A. bender 01', NULL, NULL, NULL, 'active');
INSERT INTO admins VALUES (53, 'bender02', '67b757a4f76e5fda3fe86c8a9009630a5e53a053', 'M.A.M.A. bender 02', NULL, NULL, NULL, 'active');
INSERT INTO admins VALUES (54, 'bender03', '67b757a4f76e5fda3fe86c8a9009630a5e53a053', 'M.A.M.A. bender 03', NULL, NULL, NULL, 'active');
INSERT INTO admins VALUES (55, 'bender04', '67b757a4f76e5fda3fe86c8a9009630a5e53a053', 'M.A.M.A. bender 04', NULL, NULL, NULL, 'active');
INSERT INTO admins VALUES (9, 'dany', '118ab5d614b5a8d4222fc7eee1609793e4014800', NULL, NULL, 'dany@tetsuo.schibsted.cl', NULL, 'active');


--
-- Data for Name: abuse_locks; Type: TABLE DATA; Schema: public; Owner: dany
--



--
-- Data for Name: abuse_reporters; Type: TABLE DATA; Schema: public; Owner: dany
--



--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: dany
--

INSERT INTO users VALUES (1, 1, 'uid1@blocket.se', 0, 0);
INSERT INTO users VALUES (2, 2, 'uid2@blocket.se', 0, 100);
INSERT INTO users VALUES (3, 3, 'prepaid3@blocket.se', 1000, 1000);
INSERT INTO users VALUES (4, 4, 'prepaid@blocket.se', 100000, 1000);
INSERT INTO users VALUES (5, 5, 'prepaid5@blocket.se', 1000, 1000);
INSERT INTO users VALUES (6, 6, 'kim@blocket.se', 1000, 1000);
INSERT INTO users VALUES (50, 50, 'android@yapo.cl', 0, 0);
INSERT INTO users VALUES (51, 51, 'susana@oria.cl', 0, 0);
INSERT INTO users VALUES (52, 51, 'alan@brito.cl', 0, 0);


--
-- Data for Name: abuse_reports; Type: TABLE DATA; Schema: public; Owner: dany
--



--
-- Data for Name: ads; Type: TABLE DATA; Schema: public; Owner: dany
--

INSERT INTO ads VALUES (1, 6000667, '2011-11-09 11:00:00', 'active', 'sell', 'Aurelio Rodr�guez', '1231231231', 2, 0, 1020, 5, '11111', false, false, false, 'Departamento Tarapac�', 'Con 2 dormitorios, en Alto Hospicio, 250m2 de espacio y 2 plazas de garaje', 45000000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'es');
INSERT INTO ads VALUES (2, NULL, NULL, 'inactive', 'sell', 'Thomas Svensson', '08-112233', 11, 0, 4100, 3, 'testb', false, false, false, 'Barncykel', 'Röd barncykel, 28 tum.', 666, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'es');
INSERT INTO ads VALUES (3, NULL, NULL, 'inactive', 'sell', 'Sven Ingvars', '08-121314', 11, 0, 4100, 5, 'testc', false, false, false, 'Damcykel', 'Damcykel med stång i mitten. 33,2 tum i plast.', 667, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'es');
INSERT INTO ads VALUES (4, NULL, NULL, 'inactive', 'sell', 'Klas Klasson', '08-121314', 11, 0, 4100, 5, 'testc', true, true, true, 'Hustomte', 'En tre fot stor hustomte.', 667, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'es');
INSERT INTO ads VALUES (5, NULL, NULL, 'inactive', 'sell', 'Dummy', '08-121314', 11, 0, 4100, 5, 'testc', false, false, false, 'Damcykel', 'Damcykel med stång i mitten. 33,2 tum i plast.', 667, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'es');
INSERT INTO ads VALUES (6, 3456789, '2006-04-05 09:21:31', 'active', 'sell', 'Bengt Bedrup', '08-121314', 10, 0, 2020, 5, '11111', true, true, false, 'Race car', 'Car with two doors ...
33.2-inch wheels made of plastic.', 11667, NULL, NULL, 'Testar länk', NULL, NULL, NULL, NULL, NULL, 'es');
INSERT INTO ads VALUES (7, NULL, NULL, 'inactive', 'sell', 'Dummy', '08-121314', 11, 0, 4100, 5, 'testc', false, false, false, 'Damcykel', 'Damcykel med stång i mitten. 33,2 tum i plast.', 667, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'es');
INSERT INTO ads VALUES (8, 6431717, '2006-04-06 09:21:31', 'active', 'sell', 'Juan P�rez', '084123456', 11, 0, 5020, 1, '11111', false, false, false, 'Una mesa', 'Y qu� mesa... menuda mesa!!', 157000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'es');
INSERT INTO ads VALUES (9, 999999, NULL, 'inactive', 'sell', 'Dummy', '08-121314', 11, 0, 4100, 4, 'testc', false, false, false, 'Damcykel med blahonga', 'Damcykel med stång rakt upp i mitten. 33,2 tum i plast.', 667, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'es');
INSERT INTO ads VALUES (10, 6000666, '2006-04-05 10:21:31', 'active', 'sell', 'Cristobal Col�n', '0812131491', 12, 0, 6020, 5, '11111', false, false, false, 'Pesas muy grandes', 'Una de 120 kg y la otra de 57, mas ligera.', 90000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'es');
INSERT INTO ads VALUES (11, 6394117, '2006-04-07 11:21:31', 'active', 'sell', 'Andrea Gonz�lez', '0987654321', 13, 0, 7060, 1, '11111', false, false, false, 'Peluquera a domicilio', 'Voy, corto el pelo, me pagas y m voy....', 12000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'es');
INSERT INTO ads VALUES (12, 6394118, '2011-11-06 11:21:31', 'active', 'let', 'Boris Felipe', '0987654000', 15, 0, 1040, 1, '11111', false, false, false, 'Arriendo mi preciosa casa en �u�oa', 'Dos habitaciones, metros cuadrados.... hect�reas dir�a yo... y plazas de garaje a tutipl�n', 500000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'es');
INSERT INTO ads VALUES (23, 8000003, NOW() - interval '1 month', 'active', 'let', 'AndroidTest', '123123', 15, 0, 1020, 50, NULL, false, false, true, 'Departamento 2 dormitorio 1 ba�o', 'Arriendo Departamento 2 dormitorio 1 ba�o ,cocina americana terraza muro a muro piso 16 , gastos comunes 35.000 aprox.
El Edificio cuenta con ;
lavander�a, sala de eventos .
Requisitos :
2 meses de garant�a 
12 cheques acreditar a�o 
comisi�n del corretaje 50% del arriendo 

3 ultimas liquidaciones de sueldo 
contrato de trabajo indefinido 
certificado de Dicom 
certificado de AFP
Fotocopia de carnet 

Contacto Gabriela 56670270', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$7N@JND@6O-$(yJ,ed345d4aa234e160f7b6a8224b013de88a793b98f', '2013-06-04 10:52:12.983778', 'es');
INSERT INTO ads VALUES (20, 8000000, NOW() - interval '1 month', 'active', 'sell', 'AndroidTest', '123123', 15, 0, 1020, 50, NULL, false, false, false, 'Depto stgo centro', 'dpto soltero

cuenta con un dormitorio, livig ,cosina americana ba�o coset .
coquimbo/ san ignacio 3 cuadras de metro . agua
caliente se paga en gastos comunes', 25000000, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$23qWk@a#"(F4S`&^0cb4e94887596a5cb0e8385ed110020d9b747277', '2013-06-04 10:51:58.847484', 'es');
INSERT INTO ads VALUES (21, 8000001, NOW() - interval '1 month', 'active', 'let', 'AndroidTest', '123123', 15, 0, 1020, 1, NULL, false, false, true, 'Departamento 3 dormitorios 2 ba�os', 'excelente departamento 3 dormitorios 2 ba�os, bodega estacionamineto, cocina amueblada y equipada con encimera , horno y campana, instalaci�n para lacadora y secadora. shower door, ba�o en suite, closets, edificio con piscina, areas verdes juegosminfnatiles, sala de eventos, a 2 cuadras de irarrazabal , cerda de supermercado y famacias', 540000, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$)1GK|kPDo/JYUJwK9741418938835db492eb3315b4a752eb518ed73a', '2013-06-04 10:52:03.060447', 'es');
INSERT INTO ads VALUES (24, 8000004, NOW() - interval '1 month', 'active', 'sell', 'AndroidTest', '123123', 15, 0, 1020, 1, NULL, false, false, false, 'Por viaje hermoso departamento comuna santiago', 'hermoso departamento en comuna de santiago cuenta con 2 dormitorios 2 ba�os living cocina americana closet en los dos dormitorios puerta d seguridad balc�n hermosa vista a la cordillera el edificio cuenta con piscina , quincho ,sala multiuso ,gimnasio ,salida metro cal y canto supermercados la vega , locomoci�n', 36000000, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$Mhz''>g;vu%_FI`GYdeca1a81062dc7f76d906838b8451051168eed4f', '2013-06-04 10:52:19.031284', 'es');
INSERT INTO ads VALUES (22, 8000002, NOW() - interval '1 month', 'active', 'buy', 'AndroidTest', '123123', 15, 0, 1020, 1, NULL, false, false, true, 'Casa o departamento', 'Busco casa o departamento en La Florida o alrededores pago maximo $190.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$b 7}MfeU!BQv_EFG3be6cb2fbe19f6e488818f91d970f13f88291240', '2013-06-04 10:52:10.494763', 'es');
INSERT INTO ads VALUES (25, 8000005, NOW() - interval '1 month', 'active', 'sell', 'AndroidTest', '123123', 15, 0, 1020, 1, NULL, false, false, false, 'Departamento maip� centro calle San Jose', 'DEPARTAMENTO SEGUNDO PISO ,ACCESO CONTROLADO 24 HORAS,CITOFONO DE PORTER�A A DPTO, SECTOR DE JUEGOS,ESTACIONAMIENTO GASTOS COMUNES APROX.$20.000 EXCELENTE ENTORNO Y ALTA PLUSVAL�A .
EL DEPARTAMENTO CUENTA CON: DOS BA�OS CON CER�MICA ,COCINA AMOBLADA ,LIVING COMEDOR TRES DORMITORIOS PRINCIPAL CON CLOSET Y BA�O, PISO FLOTANTE Y EXCELENTE ILUMINACI�N..', 40000, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$haf!W/NlAtTf4;Vp901a82e9f11e1ccc5d14a149e4523199d2bf6fdc', '2013-06-04 10:52:21.669333', 'es');
INSERT INTO ads VALUES (28, 8000008, NOW() - interval '1 month', 'active', 'let', 'Android', '457764874', 15, 0, 1020, 1, NULL, false, false, false, 'Departamento Amoblado 2 Dormitorios', 'Arriendo departamento AMOBLADO ,a�o corrido, piso 2, a�o 2009, 2 dormitorios (uno en suite ), 2 ba�os , loggia, cocina amoblada, estacionamiento. Gastos comunes $ 38.000. Edificio cuenta con hall de acceso, sala de eventos, sala de juegos para ni�os, sala de juegos para adolescentes y adultos, gimnasio, �reas verdes, piscina, quincho, amplia p�rgola , sala de lavander�a, c�maras de seguridad, estacionamientos de visita, servicio de porter�a 24/7 horas.', 320000, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$3~''GCb/C:vvHd} :9b79abb4dffd8efe7a359183ced65703b1df39e3', '2013-06-04 11:03:23.770424', 'es');
INSERT INTO ads VALUES (29, 8000009, NOW() - interval '1 month', 'active', 'let', 'Android', '457764874', 15, 0, 1020, 1, NULL, false, false, false, 'Departamentos nuevos en santiago centro', '- Metro Moneda:Morande, Dpto de 1 dormitorio amplio .,con un cano de arrirndo $190.000.
(disponible acontar de 18 junio)

- Metro Santa Isabel; Portugal ,Dpto de 1 dormitorio ,con vista Norte , con un cano de arriendo de $190.000.con inst.para la lavadora,cocina electrica ,horno y campana electrica.( disponible acontar de 8 de junio)', 190000, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$4jo/~3$''n0_wDZ;La996a4d9df79679e9e86aacedd3c37c106b0da99', '2013-06-04 11:03:26.585804', 'es');
INSERT INTO ads VALUES (47, 8000027, NOW() - interval '1 month', 'active', 'sell', 'Android', '457764874', 15, 0, 3020, 50, NULL, false, false, false, 'Deus Ex Human Revolution', 'Vendo este juego en excelente estado, sin ningun detalle en caja manuales y disco.', 10000, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$sRDfIbcEf. CCF y71bada4bd518bad85cabd7c19b6779b577f9f14f', '2013-06-04 11:59:26.342338', 'es');
INSERT INTO ads VALUES (26, 8000006, NOW() - interval '1 month', 'active', 'sell', 'AndroidTest', '123123', 15, 0, 1020, 50, NULL, false, false, false, 'Pieza para mujer que estudie o trabaje', 'Pieza amoblada con ba�o privado. Con derecho a cocina, luz, agua, gas y wifi. A 2 cuadras metro los leones y comercio; en edificio con conserje y ascensor.El depto. tiene cocina americana.
Se incluye lavado de ropa.
Se pide mes de garant�a.', 170000, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$ ;cHSd``x-B?Vylfa13d37747b4762ab4980ee4eeb96da248fc73733', '2013-06-04 11:03:15.132247', 'es');
INSERT INTO ads VALUES (27, 8000007, NOW() - interval '1 month', 'active', 'let', 'Android', '457764874', 15, 0, 1020, 50, NULL, false, false, false, 'Depto amoblado un ambiente', 'Departamento amoblado de un ambiente. Valor de arriendo incluye todos los gastos, internet y cable, lo unico que no se incluye es la calefaccion en caso de que se use.
Ubicado a cuadras de metro manquehue, cuenta con cocina americana totalmente equipada, con lavadora secadora.
Cama de dos plazas y futon. 
edificio con seguridad 24hrs, sala de eventos, piscina temperada, sauna, lavanderia, gimnasio, quincho para asados.
arriendo minimo 3 meses
garantia $250.000.
sin estacionamiento, sin bodega', 380000, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$S6a>s[q<|xTai{pRc6a4dfc6c8579232d1c54ca6cd0fac624bc89d8a', '2013-06-04 11:03:18.293219', 'es');
INSERT INTO ads VALUES (81, 8000061, NOW() - interval '1 month', 'active', 'sell', 'Android', '457764874', 15, 0, 6120, 4, NULL, false, false, false, 'Libros baldor(aritmetica, geometria,algebra)', 'Vendo los tres libros originales cada uno con cd. estan nuevos los compre hace 2 meses y no los uso.', 69000, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$<y5}m9$OBd,ku"+j814aa7bf4d9a40b09896806a9d17b064e0373978', '2013-06-04 12:32:47.537926', 'es');
INSERT INTO ads VALUES (30, 8000010, NOW() - interval '1 month', 'active', 'sell', 'Android', '457764874', 15, 0, 1020, 50, NULL, false, false, false, 'Departamento en condominio', 'Vendo departamento, ubicado en el tercer piso del Torre A, del Condominio Parque Cordillera II Lote 5 en Puente Alto. 
Tiene 3 dormitorios amplios.
Un Ba�o completo (Tina, WC, Lavamanos, Espejo y Mueble para guardar accesorios).
Living Comedor, Terraza. Vista al poniente, el sol pega Nor- Poniente.
Cocina Amoblada
Logia con calef�n.
Gas de ca�er�a Gasco, Agua Andinas, Luz EEPA.
Piso de cer�mica en Living, Comedor, Pasillo, Ba�o, Cocina y Loggia
Piso de alfombra cubre piso en Dormitorios.

Sector residencial, en Condominio, Acceso Controlado por Conserje, con Piscina para ni�os y jardines internos, en los cuales no se admiten animales.
Gastos comunes valor promedio a pago $ 25.000.
Departamento a�n con cr�dito hipotecario, sin deuda atrasada y dividendos al d�a.

Valor $ 30.000.000', 30000000, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$:+j-F=@u6A.6t"Wo6bbf5bed1e1cf71b247c343bce04993c07343fbf', '2013-06-04 11:03:31.075334', 'es');
INSERT INTO ads VALUES (32, 8000012, NOW() - interval '1 month', 'active', 'sell', 'Android', '457764874', 15, 0, 1100, 50, NULL, false, false, false, '9 hectareas de riego tranquilidad', 'Se venden 9 hectareas. Todo de riego planas muy buenas tierras con luz. Y agua! Y camino pavimentado a 1 kilometro al.interior de calle avenida valparaizo! Muy trakilo el lindo. Melipilla 
ar y muy lindo! Vende su due�o! Ojo tambien se venden x hectareas . Valor 18 millones x 1_', 18000000, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$1!Xq"IxA/i-@G/BM4b0ef94e66b6862a4412ed2da60de65e5c6e3a49', '2013-06-04 11:39:29.80389', 'es');
INSERT INTO ads VALUES (31, 8000011, NOW() - interval '1 month', 'active', 'sell', 'Android', '457764874', 15, 0, 1100, 50, NULL, false, false, false, 'Terreno Industrial', 'terreno industrial,5000 metros cuadrados, corriente trifasica, galpon 300 Metros cuadrados,con oficina ba�o y duchas para trabajadores casa 120 metros cuadrados, piscina de hormigon ,arboles frutales ,cel 9-6811733', 250000000, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$kBZo(3wYShd&mb`sd7ece65732e2a0460523f698b48fc2fe94f54db1', '2013-06-04 11:39:25.182244', 'es');
INSERT INTO ads VALUES (33, 8000018, NOW() - interval '1 month', 'active', 'sell', 'Android', '457764874', 15, 0, 1100, 50, NULL, false, false, false, 'Parcelas frente a laguna de aculeo 101 hectareas', 'dos parcelas frente a laguna de aculeo y frente a reserva natural altos de cantillana 
1 parcela de 480000 m2 lotiada en 30.
otra parcela de 530000 m2.
estas parcelas estan contiguas
valor de 350 pesos el m2
168000000 cada parcela.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$3>\\ G"c#*06*}\\jBf3faa4822c8f0c8be287b99f6be804abe68e8618', '2013-06-04 11:40:05.443704', 'es');
INSERT INTO ads VALUES (36, 8000014, NOW() - interval '1 month', 'active', 'sell', 'Android', '457764874', 15, 0, 2060, 50, NULL, false, false, false, 'Yamaha rx115 special', 'Vendo yamaha rx115 en impecable estado, con sus papeles al d�a y a mi nombre motor reci�n ajustado pintura nueva neum�ticos nuevos Michelin motor de 2 tiempos solo entendidos precio poco conversable.', 470000, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$L=+iN1ATT_jwN#Yf6ae09c245bf6b1f806f3e54e9466f27faf715635', '2013-06-04 11:39:40.617107', 'es');
INSERT INTO ads VALUES (35, 8000013, NOW() - interval '1 month', 'active', 'sell', 'Android', '457764874', 15, 0, 2020, 50, NULL, false, false, false, 'Dodge Ram 1500 V8', 'a toda prueba.

fono: 77700260 carlitos', 5500000, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$upJrj31={a''_wG*28a09861d1239a4a5b3471f47726d14e683fc1a68', '2013-06-04 11:39:37.380686', 'es');
INSERT INTO ads VALUES (37, 8000016, NOW() - interval '1 month', 'active', 'sell', 'Android', '457764874', 15, 0, 2060, 50, NULL, false, false, false, 'Moto Wolken xy 400cc,', 'Vendo mi joyita Wolken xy 400cc, 
a�o 2009 $1.550.000

esta impecable apenas 14000. Con toda su documentaci�n al d�a asta 2014 sin deudas alguna La moto tienen 6 cambios tiene mucha fuerza. frenos de disco. delantero y trasero Transferencia inmediata. se puede ver sin compromiso solo interesados conversableVendo mi joyita Wolken xy 400cc, 
a�o 2009 $1.550.000', 1550000, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$t`[]nH;xSs[YU5)R31f5f5c4441dec60f381c049b5eea5a8e34067c3', '2013-06-04 11:39:54.980674', 'es');
INSERT INTO ads VALUES (39, 8000015, NOW() - interval '1 month', 'active', 'sell', 'Android', '457764874', 15, 0, 2020, 50, NULL, false, false, false, 'Chevrolet Aveo sport', 'Auto en perfectas condiciones, cuenta con cierre centralizado, alza vidrios con interfaz, sunroof.', 3500000, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$E|u"N V=-YZcd0F:554cf741780734d363457a447993e950561c196b', '2013-06-04 11:39:47.165872', 'es');
INSERT INTO ads VALUES (38, 8000017, NOW() - interval '1 month', 'active', 'sell', 'Android', '457764874', 15, 0, 2020, 50, NULL, false, false, false, 'Suzuki Maruti 800cc 2003', 'Vendo Susuki Maruti motor 800cc color Gris 5 puertas
a�o 2003 3� due�o en excelentes condiciones.
Mantenciones al d�a, papeles al d�a, sin partes ni deudas. Precio conversable.', 1750000, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$''#|YiNN[M?BZfuGT58b9d962c8d265fbff89ba49876fcc99cc5fe872', '2013-06-04 11:39:58.627429', 'es');
INSERT INTO ads VALUES (34, 8000019, NOW() - interval '1 month', 'active', 'sell', 'Android', '457764874', 15, 0, 2060, 50, NULL, false, false, false, 'Kawasaki 650 r 2.008', 'Kawasaki Ninja 650 R , neum�ticos de competici�n con una semana de uso 
A�o 2008
Motor Refrigeraci�n por agua, 4 tiempos,
2 cilindros paralelo
Encendido Electr�nico digital
Arranque Motor el�ctrico
Embrague Multidisco en ba�o de aceite
Cambio De 6 velocidades
Transmisi�n secundaria Por cadena sellada
Tipo chasis Diamante, acero de alta resistencia
Geometr�a 25� / 106 mm
Suspensi�n delantera Horquilla de 41 mm
Suspensi�n trasera Amortiguador lateral con ajuste de precarga de muelle
Freno delantero Doble disco lobulado semiflotante de 300 mm Doble pist�n
Freno trasero Un solo disco lobulado de 220 mm Un solo pist�n', 4500000, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$>Pm-kQ3{<iHx5t1R41591e054e6c4046508c147cc467455ad832ba8a', '2013-06-04 11:40:09.295412', 'es');
INSERT INTO ads VALUES (40, 8000020, NOW() - interval '1 month', 'active', 'sell', 'Android', '457764874', 15, 0, 7040, 50, NULL, false, false, false, 'Artesano mueblista', 'busco empleo de manera urgente se hacer muebles o para cualquier trabajo pero en alerce disponibilidad inmediata tratar al 57520987', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$S#UwwO:9RB[IqR:<718b75acd44003b8fd8f91df6a012112fbb2ba98', '2013-06-04 11:58:48.056116', 'es');
INSERT INTO ads VALUES (42, 8000022, NOW() - interval '1 month', 'active', 'sell', 'Android', '457764874', 15, 0, 6080, 50, NULL, false, false, false, 'Traktor Kontrol X1 + Case Traktor', 'Producto Usado por Un A�o, Detalles, solo que apreto bi�n los Botones del Cue pero en General Todos Funcionan OK

Producto.....

TRAKTOR KONTROL X1 destaca por su fabricaci�n de calidad excepcionalmente alta. Con meticulosa atenci�n a los detalles, los mandos y botones se han dise�ado para lograr la m�xima precisi�n tanto en el aspecto como en el tacto. 

Con su dise�o fino y s�per resistente, el X1 es tu compa�ero ideal en cualquier situaci�n de pinchado de discos. La bolsa de transporte opcional no solo sirve para transportar de forma segura la unidad, sino que tambi�n se convierte en un mostrador que coloca el X1 a exactamente la misma altura que los mezcladores est�ndar y otros hardware para DJ.
TRAKTOR KONTROL X1 incorpora un total de 30 botones, 4 codificadores pulsables y 8 mandos distribuidos.', 95000, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$Ipn6@[E#FZUn^_-`b3a5923e1a3e03523604f96c23abbaf2ca273c14', '2013-06-04 11:59:01.64359', 'es');
INSERT INTO ads VALUES (44, 8000024, NOW() - interval '1 month', 'active', 'sell', 'Android', '457764874', 15, 0, 7040, 50, NULL, false, false, false, 'Ventas y atencion al cliente', 'buen C.V, variedad en experiencia laboral, responsable, y con hartas ganas de trabajar', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$>L''"m(A\\:anuEIv*22aff8d12526d000371475ef59b5d8d6e90e3824', '2013-06-04 11:59:12.342116', 'es');
INSERT INTO ads VALUES (45, 8000025, NOW() - interval '1 month', 'active', 'sell', 'Android', '457764874', 15, 0, 6080, 50, NULL, false, false, false, 'Djembe yembe jembe tambor', 'NUEVOS Y EXCELENTES DJEMBES 
SOLO 35.000 
CUERO DE CHIVO 
MADERAS NATIVAS 
EXCELENTE SONIDO 
50 CM DE ALTURA X 23 CM DE DIAMETRO
VENTASDJEMBE', 33000, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$.} 5EXAkcJKR*~N/350a24490eb7f6561d15998f46300d18bbf2e989', '2013-06-04 11:59:17.737615', 'es');
INSERT INTO ads VALUES (46, 8000026, NOW() - interval '1 month', 'active', 'sell', 'Android', '457764874', 15, 0, 3060, 50, NULL, false, false, false, 'Samsung Galaxi SIII (GT19300)', 'Posee una pantalla Super AMOLED HD 720p de 4.8 pulgadas, procesador Exynos 4 Quad de cuatro n�cleos a 1.4GHz, 1GB de RAM, 16GB de memoria externa, ranura microSD y corre Android 4.0 Ice Cream Sandwich con la interfaz TouchWiz, con solo 6 meses de uso, no tiene cargador ni manos libres, viene en su caja junto con el manual, ENTEL', 220000, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$ubb=N~N=Fx''0_E(U269f2ef54a24f7f7f7a4ad5c1f6d0454090f38b7', '2013-06-04 11:59:23.717305', 'es');
INSERT INTO ads VALUES (49, 8000029, NOW() - interval '1 month', 'active', 'sell', 'Android', '457764874', 15, 0, 3020, 50, NULL, false, false, false, 'Psp3010 excelente estado desbloqueada conaccesorio', 'esta desbloqueada trae cargador original memoria original un juego el god of war trae emulador y otros juegos en la memoria tiene wifi unico due�o esta en excelente estado cualquier cosa consulten', 74000, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$q.%)RMoa T+d6n*j51bd1692e5bb9af7078201a64ebaf700dc978649', '2013-06-04 11:59:35.454628', 'es');
INSERT INTO ads VALUES (48, 8000028, NOW() - interval '1 month', 'active', 'sell', 'Android', '457764874', 15, 0, 3060, 50, NULL, false, false, false, 'Motorola razr I xt890 (detalle)', 'El equipo est� perfect, el uNico problema es q la pantalla se fue a negro de a poco, No se que tiene ni por que pas� eso. Lo permuto o vendo. El precio es conversable todo el rato.', 90000, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$Nh;Cr==K[yk3_h54a93ca0dafda2d0b0e4357540e1f5f8e3c6aea733', '2013-06-04 11:59:31.159284', 'es');
INSERT INTO ads VALUES (50, 8000030, NOW() - interval '1 month', 'active', 'sell', 'Android', '457764874', 15, 0, 3060, 50, NULL, false, false, false, 'Alcatel Cel, Para abuelos', 'Celular Unotouch, lo trae solo entel, por lo tanto no esta liberado., 
poco uso
no incluye chip

Conversable', 8000, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$8_1/qL:RJOwF$BUfb9a4d8fffd7d782caa3369ecdacc11189c95cf2a', '2013-06-04 11:59:43.225016', 'es');
INSERT INTO ads VALUES (51, 8000031, NOW() - interval '1 month', 'active', 'sell', 'Android', '457764874', 15, 0, 3020, 50, NULL, false, false, false, 'Xbox 360 con sensor kinect y juegos', 'Vendo Consola Xbox 360,como nueva
contiene lo siguiente:

* XBOX 360 Arcade Slim 
* Memoria 4GB 
* WIFI 
* Sensor Kinect 
* Control Inalambrico 
* 3 Juegos Originales: Kinect Adventures, Kinect
* Sports Season 2, Dance Central 

si les interesa solo deben escribirnos o llamarnos', 180000, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$(#!=fW+>h;uE[6ifedd511c5739c6b736e8bd3d090c42eb262cd8454', '2013-06-04 11:59:46.011444', 'es');
INSERT INTO ads VALUES (52, 8000032, NOW() - interval '1 month', 'active', 'buy', 'Android', '457764874', 15, 0, 3060, 50, NULL, false, false, false, 'Iphone 5 blanco', 'Busco iphone 5 blanco nuevo.', 230000, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$v$Pia4<$YNm>92M51b3ca8f1612423b54a98fcdc5525e6553700d011', '2013-06-04 11:59:50.034908', 'es');
INSERT INTO ads VALUES (54, 8000034, NOW() - interval '1 month', 'active', 'sell', 'Android', '457764874', 15, 0, 3060, 50, NULL, false, false, false, 'Expectacular Blackberry 9810 o Permuta', 'En excelente estado , c�mara de 5 megapixeles con flash incluye su cargador y cable usb .se puede permutar por samsung S3 mini o samsung s2 ! cualquier consulta mail interesados.liberado a todas las compa�ias', 160000, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$?mZ<.)_sgO1rf.:R0c3994e219e8275ccdc3980bb88cb62cbfe36e30', '2013-06-04 11:59:58.805189', 'es');
INSERT INTO ads VALUES (53, 8000033, NOW() - interval '1 month', 'active', 'sell', 'Android', '457764874', 15, 0, 3020, 50, NULL, false, false, false, 'Guitarra para Play 2 Buen Estado', 'vendo guitarra para play 2 muy buen estado', 5500, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$h[Jk6CD5R8|5v"dl78b29db792c832be8d4e8502dcd75b2521acd36e', '2013-06-04 11:59:53.36316', 'es');
INSERT INTO ads VALUES (57, 8000037, NOW() - interval '1 month', 'active', 'sell', 'Android', '457764874', 15, 0, 3020, 50, NULL, false, false, false, 'Call of Duty Black ops II Xbox 360', 'Call of Duty Black ops II Xbox 360', 20000, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$KF7qNJKjwF&Z{+[Mfc0fae0f70945d19f771b3702af23a65ec11c2e4', '2013-06-04 12:00:33.275949', 'es');
INSERT INTO ads VALUES (56, 8000036, NOW() - interval '1 month', 'active', 'sell', 'Android', '457764874', 15, 0, 3020, 50, NULL, false, false, false, 'Nintendo 3ds + 7 Juegos Originales', 'Consola Nintendo 3ds color Aqua Blue en perfecto estado Con su respectiva caja y manuales.
Incluye memoria de 2GB saca fotos y graba en 3D
Contiene 7 Juegos Originales :
- Resident Evil The Mercenaries 3D
- Resident Evil Revelations
- Super Mario 3DLand
- Mario Kart 7
- Star Fox 64 3D
- Zelda Ocarina of Time 3D
- Super Street Fighter IV 3D Edition.
Ademas de el Circle Pad Pro (que se se en la foto al lado izquierdo)', 140000, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$(M}FHj\\5xt8B+4LO3690ffadca12dcec6801dc8fc23c13fc1dbdc293', '2013-06-04 12:00:17.586168', 'es');
INSERT INTO ads VALUES (41, 8000021, NOW() - interval '1 month', 'active', 'sell', 'Android', '457764874', 15, 0, 7020, 50, NULL, false, false, false, 'Asesora del hogar', 'Hola busco empleo como asesora del hogar me llamo Ana Toledo soy de Puerto Monty ago todo tipo de coda cosino lavo etc . Con exelentes recomendaciones ,me justaria trabajar de Las 09:00a Las 18:00 maximo Las 19:00 hras. Disponibilidad imediata.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$zYjEz9{f?91RcFO:3c11d1ef9dccfa2b8ffc423e1c7dc9639caa5319', '2013-06-04 11:58:51.896416', 'es');
INSERT INTO ads VALUES (43, 8000023, NOW() - interval '1 month', 'active', 'sell', 'Android', '457764874', 15, 0, 6080, 50, NULL, false, false, false, 'Guitarra electrica', 'guitarra electrica con amplificador', 70000, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$JUyb*XYG-\\>qjK3V21119dd7aae85fe1eddc7314741210cfa2f4b776', '2013-06-04 11:59:05.848706', 'es');
INSERT INTO ads VALUES (55, 8000035, NOW() - interval '1 month', 'active', 'sell', 'Android', '457764874', 15, 0, 3020, 50, NULL, false, false, false, 'Play Station 3 con 19 juegos 1 control, 1 guitarra', 'Vendo Play Station 3 Slim de 160GB con 19 juegos, 1 control y 1 guitarra. (Todo original)

Lista de juegos:
1. Fifa Soccer 10
2. Fifa Soccer 11
3. Fifa Soccer 12
4. Metal Gear Solid HD Collection
5. GTA IV
6. Killzone 3
7. Little Big Planet
8. Motorstorm Pacific Rift
9. Guitar Hero World Tour
10. Family Guy Back to the Multiverse
11. RockBand Greenday
12. Guitar Hero Van Halen (sellado)
13. Fifa Soccer 2010 World Cup South Africa
14. Assassins Creed
15. Pro Evolution Soccer 2011
16. Guitar Hero Warrior of Rock
17. RockBand The Beatles
18. Band Hero
19. Gran Turismo 5

Acepto ofertas', 280000, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$;e|(N#7j(|P:v7Y,62de3bef5c09b2fc1e9a1889beb900748e430d9b', '2013-06-04 12:00:08.524804', 'es');
INSERT INTO ads VALUES (58, 8000038, NOW() - interval '1 month', 'active', 'sell', 'Android', '457764874', 15, 0, 5100, 50, NULL, false, false, false, 'Mario y Luigi crochet', 'Tejidos a crochet .$ 4500 retiro en mi casa y $ 5000 te lo llevo a domicilio dentro de la ciudad.', 4500, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$mo8]Zd/t/(,OSGIs312e2cd4d663477bb1b0d8077ba374d53979b152', '2013-06-04 12:11:18.709302', 'es');
INSERT INTO ads VALUES (59, 8000039, NOW() - interval '1 month', 'active', 'sell', 'Android', '457764874', 15, 0, 5020, 50, NULL, false, false, false, 'Tetera y Azucarero', 'Precioso set para el t�, de tetera y azucarero de porcelana, con dise�o frutal.', 9000, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$ ctYtU21?jTXQ"Wf7d9ba91c77a50fe7907ea7472e3158f29cc95133', '2013-06-04 12:11:21.872854', 'es');
INSERT INTO ads VALUES (60, 8000040, NOW() - interval '1 month', 'active', 'sell', 'Android', '457764874', 15, 0, 5020, 50, NULL, false, false, true, 'Cama rosen modelo tfx-2', 'cama rosen modelo tfx-2
cama sellada nueva sin ningun detalle
americana base mas colchon', 110000, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$?#TEG!|cdaW]7#yj402cc1ca2a7a41ceeedba96e5483f8e5ef48903d', '2013-06-04 12:11:26.704452', 'es');
INSERT INTO ads VALUES (61, 8000041, NOW() - interval '1 month', 'active', 'sell', 'Android', '457764874', 15, 0, 5100, 50, NULL, false, false, false, 'Gateadores N� 20 Calpany', 'Hermosos zapatitos Calpany nuevos, en su caja.
Color azul-gris.
Ideales para la transici�n gateo-caminar
50% cuero-50% textil.', 7000, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$7uC8z6EvUw5Bm>fN26496e4d4e4688ac2784000c70bb7049b5d77463', '2013-06-04 12:11:30.178703', 'es');
INSERT INTO ads VALUES (65, 8000045, NOW() - interval '1 month', 'active', 'sell', 'Android', '457764874', 15, 0, 7020, 50, NULL, false, false, true, '" URGENTE" Operarios de limpieza industria', 'Distribuidora industrial del sector quilicura, requiere operarios de limpieza.

Requisitos.

Hombres
35 a 55 a�os
papel de ant5ecedentes
disponibilidad inmediata

Se ofrece:

Renta 230.000.- liquidos
beneficios legales
contrato directo empresa
lunes a viernes 8:30 a 18:30 horas
colacion
bus de acercamiento
estabilidad.

Contactar: FLAVIO HUERTA - 78183951
2- 4613408 - 2- 2438790', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$.]LO*ik):/GeO[p:027025cf7f7597dd46ae3e157e2f5da7c2cf9fd9', '2013-06-04 12:20:39.108132', 'es');
INSERT INTO ads VALUES (62, 8000042, NOW() - interval '1 month', 'active', 'sell', 'Android', '457764874', 15, 0, 5100, 50, NULL, false, false, false, 'Vestido de Novia', 'Lindo vestido de novia, solo porque necesito el dinero, es realmente lindo, llamar solo interesadas.', 200000, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$LQSvH^~>U31_V#mJ2284ac47da3b77c4aac01ede3d8112605c9958ad', '2013-06-04 12:11:34.82732', 'es');
INSERT INTO ads VALUES (64, 8000044, NOW() - interval '1 month', 'active', 'sell', 'Android', '457764874', 15, 0, 7020, 50, NULL, false, false, false, 'Control de calidad en metalurgica', 'se necesita contratar control de calidad para empresa metalurgica fabricantes de gabinetes y salas insonorizadas para generadores con servicios en corte y plegado, fabricacion y armado y pintura.
se necesita persona con:
experiencia
compromiso
trabajo en equipo
se ofrece estabilidad laboral
remuneracion acorde al mercado', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$0Aj7qxPn.K!4[Z<$7e1ad6bfb2371702690db6aa527e1111ffab9567', '2013-06-04 12:20:34.121895', 'es');
INSERT INTO ads VALUES (67, 8000047, NOW() - interval '1 month', 'active', 'sell', 'Android', '457764874', 15, 0, 7020, 50, NULL, false, false, false, 'Secretaria Recepcionista', 'Climaroca, empresa chilena de servicios de ingenier�a t�rmica busca Asistente de Servicio para hacerse cargo de la atenci�n de clientes en relaci�n a cotizaciones, entrega de informaci�n de servicios y en general, manejo de todas las situaciones relacionadas al servicio y soporte comercial.

Principales actividades:
-Atenci�n telef�nica y/o presencial de clientes
-Atenci�n de consultas v�a e-mail, entregando orientaci�n general de los servicios
-Coordinaci�n de visitas, agendamiento de reuniones
-Contacto con proveedores y contratistas
-Administraci�n de registros y documentos de gerencia
-Administraci�n de caja chica', 300000, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$.Ue^Kc+zndw$K Fce9e575159b5d7796454978e97e370b955dbd1f89', '2013-06-04 12:20:46.755825', 'es');
INSERT INTO ads VALUES (66, 8000046, NOW() - interval '1 month', 'active', 'sell', 'Android', '457764874', 15, 0, 7020, 50, NULL, false, false, true, 'Asistente RR HH', 'Para trabajar en remuneraciones debe conocer y saber trabajar con planilla exel, no se requiere conocimientos extras se entrenara en manejo de programa de remuneraciones, trabajo de lunes a viernes de 9:00 a 18:30 horas, ubicacion 18 1/2 de la comuna de La Cisterna, sueldo liquido 280.000 pesos. se contrata plazo fijo y luego indefinido.', 280000, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$/KZh3''Wz{j G*z!9388601f66691bd098e7507c9405a582625d86e23', '2013-06-04 12:20:42.041479', 'es');
INSERT INTO ads VALUES (68, 8000048, NOW() - interval '1 month', 'active', 'sell', 'Android', '457764874', 15, 0, 7020, 50, NULL, false, false, false, 'Guardias de seguridad', 'Necesitamos a la brevedad 2 guardias con curso OS10 en CERRILLOS detr�s del mall plaza oeste(fabrica) 12 hrs de $280.000 liq a $300.000 liq,ma�ana o noche.
1 guardia para COLINA con o sin curso en las BRISAS. Lunes a Sabado de 13:00 a 21:00 hrs $300.000 liq. ; 2 guardias Principe de Gales/farmacia.$280.000 liquido.Necesitamos tambien para todas las cadenas Cruz Verde,todas las comunas,siempre cerca de su domicilio $270.000 a $280.000 liq.Contacto con Francisco Alderete 65442479 o al correo.', 280000, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$ .f~si:2BS;OP",K621343a09a52811b502c6e278232a54954d2f445', '2013-06-04 12:20:49.002899', 'es');
INSERT INTO ads VALUES (76, 8000056, NOW() - interval '1 month', 'active', 'sell', 'Android', '457764874', 15, 0, 6160, 50, NULL, false, false, false, 'Ca�on Modelo Dahlgreen 1861 Nuevo De Madera Y Meta', 'Se vende replica de ca��n artiller�a Dahlgreen 1861 NUEVO DE Metal Y Madera

ENVIO A REGIONES POR TUR BUS POR CARGO

Dimensiones Ca�on :

LARGO: 24 CM.

ANCHO: 14 CM

ALTO : 12 CM

LARGO DE CA�ON 14.5 CM.

Dimensiones caja porta munici�n :

LARGO: 10 CM.

ANCHO: 14 CM

ALTO : 13.5 CM', 45000, NULL, NULL, NULL, NULL, NULL, NULL, '$1024${Xk2HE8*Ul[(50"g9abc077987fd83669a647058e9ff1dc45bc43f9e', '2013-06-04 12:32:33.488079', 'es');
INSERT INTO ads VALUES (69, 8000049, NOW() - interval '1 month', 'active', 'sell', 'Android', '457764874', 15, 0, 7020, 50, NULL, false, false, false, 'Easy requiere cajeros, vendedores y bodegueros', '�BUSCAMOS PERSONAS COMO T�! SI ERES DIN�MICO Y CON GANAS DE CRECER EN UNA GRAN EMPRESA, ESTA ES T� OPORTUNIDAD! Easy requiere para su local de QUILICURA Y LA REINA a los mejores candidatos con o sin experiencia, para asumir cargos como: Vendedores ,Operadores Log�sticos y Cajeros (as). En Jornadas Full Time (45 hrs.) y Part Time (20 hrs. S�bados, Domingos y Festivos) �S�lo debes tener disponibilidad para trabajar en turnos rotativos! Ofrecemos colaci�n, uniforme, atractivos beneficios institucionales, convenios, entre otros.

Si est�s interesado EN EL LOCAL DE QUILICURA
se realizar�n entrevistas el dia JUEVES 6 DE JUNIO desde las 9:00 hasta las 12:30 y desde las 14:30 hasta las 17:00 en la oficina de Recursos Humanos del local Easy de Quilicura, Lo Marcoleta 315, Quilicura.

Los interesados en el LOCAL DE LA REINA dirigirse a: Av Francisco Bilbao 8750, en las oficinas de recursos humanos del Easy la Reina (Portal la Reina) el d�a VIERNES 7 DE JUNIO desde las 9:00 hasta las 12:00 �Te esperamos!

Si usted postula puede ser contactado y citado a una entrevista directamente', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$s1H kHh8YSpye?t774b879e82163e0c5ceded92cdac10ea2f48752cb', '2013-06-04 12:20:54.280291', 'es');
INSERT INTO ads VALUES (73, 8000053, NOW() - interval '1 month', 'active', 'sell', 'Android', '457764874', 15, 0, 6160, 50, NULL, false, false, false, 'Juego de 7 Llaves Antiguas', '7 llaves antiguas tra�das de Francia, son de fierro forjado, la m�s grande mide 17,5 cms y la mas peque�a mide 9,5 cms', 17000, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$KoW*U]~y]ybkA2\\D747ed815ba6409b8b6d5d3f103cd151e8e9aaf20', '2013-06-04 12:32:24.019538', 'es');
INSERT INTO ads VALUES (74, 8000054, NOW() - interval '1 month', 'active', 'sell', 'Android', '457764874', 15, 0, 6160, 50, NULL, false, false, false, '3 Esculturas de Busto en piedra de "O. Plandiura"', 'Originales, maravillosas e impecables.
Tama�o id�ntico al ser humano.
Unidades: 3.
Tipos:
a). Con vista al frente.
b). Con vista al cielo.
c). Con vista a la tierra.

Precio de c/u: 380.000.', 380000, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$qlw%pL#}%!=?p:-<e55e54b400385e8b3c8359da3fb1aab99bc6df4c', '2013-06-04 12:32:28.333066', 'es');
INSERT INTO ads VALUES (75, 8000055, NOW() - interval '1 month', 'active', 'sell', 'Android', '457764874', 15, 0, 6160, 50, NULL, false, false, false, 'Lote de motos en miniatura', 'lo de motos de coleccion

consultas via e-mail', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$) #ZIh~5rBo@o=yU104fa6769f3a2412f0e775513db37fa557ffa083', '2013-06-04 12:32:29.773792', 'es');
INSERT INTO ads VALUES (63, 8000043, NOW() - interval '1 month', 'active', 'sell', 'Android', '457764874', 15, 0, 7040, 50, NULL, false, false, false, 'Empresa necesita Secretaria Recepcionista', 'Cualidades:
Responsable, ordenada, din�mica, puntual, discreta, proactiva; con buena presencia, dicci�n y gran capacidad de trabajar bajo presi�n.

Funciones a desempe�ar: Atenci�n telef�nica y clientes, Operaci�n de correspondencia administrativa. Apoyo al Departamento de Contabilidad y Ventas.

Requisitos: Manejo computacional y experiencia 2 A�os en el cargo. 

Localidad: Providencia 
Regi�n: Metropolitana
Salario: $300.000. L�quidos
Comienzo: Inmediato
Duraci�n: Indefinida
Jornada: Lunes a Viernes 8:30 a 18:30 hrs.

enviar curriculum vitae a: 

Persona de contacto: 
Eduardo St�whas M.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$|ps.0wblSt|<bRZp393dab2f2e018e47219e1bc831194d38f851e984', '2013-06-04 12:20:30.972276', 'es');
INSERT INTO ads VALUES (82, 8000062, NOW() - INTERVAL '1 week', 'active', 'sell', 'Android', '457764874', 15, 0, 6120, 3, NULL, false, false, false, 'Libro para Psicologo Manual Diagnostico', 'Vendo en Excelente estado DSM-IV-TR Manual Diagnostico y estadistico de los trastornos mentales ,,, 

Precio en Locales arriba de $200.000.-

Oferta $ 70.000', 70000, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$Rm<>x|N*gqS3qDK*4ee74798e7e5fa0f1694f3256f2c511742513132', '2013-06-04 12:32:52.558464', 'es');
INSERT INTO ads VALUES (70, 8000050, NOW() - INTERVAL '1 week', 'active', 'sell', 'Android', '457764874', 15, 0, 6160, 3, NULL, false, false, false, 'Maquina Singer antigua', 'M�quina de coser Singer antigua de 4 cajones, con su exterior en buenas condiciones, funcionanado perfecto mec�nicamente y tambi�n con su pedal el�ctrico.', 75000, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$qnfvwm?@V\\jiZ.=2fa2e919b50c4fbc5b108bf34af8865e04e823d08', '2013-06-04 12:32:14.392958', 'es');
INSERT INTO ads VALUES (79, 8000059, NOW() - INTERVAL '1 week', 'active', 'sell', 'Android', '457764874', 15, 0, 6120, 3, NULL, false, false, false, 'Cinegrama star wars - natalie portman', 'CINEGRAMA
en portada
NATALIE PORTMAN
STAR WARS

$1.200

Vendedor : Carlos Cebal
Telefono : 88344488', 1200, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$LV7.W3{F43tbKY.+6d1e90207e9e47b8a7662938f005b079fa5d8c3d', '2013-06-04 12:32:42.237431', 'es');
INSERT INTO ads VALUES (71, 8000051, NOW() - INTERVAL '1 week', 'active', 'sell', 'Android', '457764874', 15, 0, 6160, 3, NULL, false, false, false, 'El joven manos de tijera poster pelicula 54 x 41', '$1.200
EL JOVEN MANOS DE TIJERA
POSTER PELICULA
54 x 41

Vendedor : Carlos Cebal
Telefono : 88344488', 1200, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$vwn6fCS?w''[lo~GX51b0397c42c2dbdec329d446be5965d99d09dcb7', '2013-06-04 12:32:17.838751', 'es');
INSERT INTO ads VALUES (72, 8000052, NOW() - INTERVAL '1 week', 'active', 'sell', 'Android', '457764874', 15, 0, 6160, 50, NULL, false, false, false, 'Lechuza embalsamada', 'lechuza embalsamada, completa y en buen estado,
tambien la cambio por otras aves vivas, ej: (una pareja de personatas lutinos)', 40000, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$4QQ*Yg{Md%Y{#d4D38683c456261b1006bdad0d6089a2cb5acf853c3', '2013-06-04 12:32:22.166087', 'es');
INSERT INTO ads VALUES (80, 8000060, NOW() - INTERVAL '2 week', 'refused', 'sell', 'Android', '457764874', 15, 0, 6120, 4, NULL, false, false, true, 'Lego catalogo 1977 exclusivo coleccionistas', 'LEGO CATALOGO 1977
EXCLUSIVO UNICO
contiene todos los modelos Lego de la epoca

ENVIO A TODO CHILE

Vendedor: CARLOS CEBAL
Telefono : 88344488', 15000, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$61\\nMcp`*d0F:+$$fc097958c4a8eeccf54a8855dd2fb0d25169f576', '2013-06-04 12:32:45.690422', 'es');
INSERT INTO ads VALUES (77, 8000057, NOW() - INTERVAL '2 week', 'active', 'sell', 'Android', '457764874', 15, 0, 6160, 4, NULL, false, false, false, 'Lote De Cuadros Para Decoraci�n, Por Traslado', 'Lote De Cuadros Para Decoraci�n de casa o departamento .
Visitar y Ver en Providencia.
El precio indicado es por el lote.', 39000, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$e%UR?oYN5@z!(4_Q19b3fa69a59e0fe945b1c76734c374a6a78880d7', '2013-06-04 12:32:35.535407', 'es');
INSERT INTO ads VALUES (78, 8000058, NOW() - INTERVAL '2 week', 'active', 'sell', 'Android', '457764874', 15, 0, 6160, 4, NULL, false, false, false, 'Superman Returns 75 cm de altura', 'Espectacular figura de Superman, del a�o 2006, marca Mattel con capa de tela. Mide casi 80 cm, es articulada, y presenta detalles de pintura, tal como se aprecia en la foto. Ahora bien, nada terrible, para aquellos que puedan retocarla. De ah� tambi�n su insuperable precio, muy por debajo del valor comercial. �Oferta!

�Especial para coleccionistas!', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$McIc^a|9uKg!&SRc5a39ddf0bd9c974d75b640e64bc28714596dfcd2', '2013-06-04 12:32:40.754062', 'es');
INSERT INTO ads VALUES (83, 8000063, '2013-08-06 15:19:30', 'active', 'sell', 'Susana Oria', '123123123', 15, 0, 3040, 51, NULL, false, false, false, 'Foo', 'Bar', 314, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$B~w6nAp>mQ!a7dc!bd6d0e4085b8a0f4caab939c6c9a7d4781c865b3', '2013-08-06 15:19:30.588446', 'es');
INSERT INTO ads VALUES (84, 8000064, '2013-08-06 15:19:34', 'active', 'sell', 'Alan Brito', '123123123', 15, 0, 3080, 52, NULL, false, false, false, 'Foo 2', 'Bar 2', 314, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$Zq5/BtFrCm=''@z`r3565ad8294c60a34d292878dcf633134ce268a27', '2013-08-06 15:19:34.894397', 'es');
INSERT INTO ads VALUES (85, NULL, NULL, 'inactive', 'sell', 'Susana Oria', '123123', 15, 0, 6140, 51, NULL, false, false, false, 'Titulo del aviso', 'Foobar', 123, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$88g-_}C43@K3Txzqfdd049fbd37682fb8ff9b5746344db39d2e9e616', NULL, 'es');
INSERT INTO ads VALUES (86, NULL, NULL, 'inactive', 'sell', 'Prepaid5', '123123', 15, 0, 3020, 5, NULL, false, false, false, 'Aviso de prepaid5', 'foobar', 564456, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$f-i7%txAC_vD-^CO7ef9a1acedb609225d310c8b5780e6822e3f3200', NULL, 'es');
INSERT INTO ads VALUES (87, NULL, NULL, 'inactive', 'sell', 'Cristian', '12345678', 15, 0, 6120, 1, NULL, false, false, false, 'Libro 1', 'Este es el libro 1', 10000, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$cp8hvOeP;(aK]`zl42a1c5cea15a307ea474049d7479913950d37183', NULL, 'es');
INSERT INTO ads VALUES (88, NULL, NULL, 'inactive', 'sell', 'Kim', '123123', 15, 0, 5020, 6, NULL, false, false, false, 'Aviso de Kim', 'Aviso de Kim', 678, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$mCG)ZFLYxFv\\m{rU37bf417eed88c4dde85689b5fd3ebc7b121c49d6', NULL, 'es');
INSERT INTO ads VALUES (89, NULL, NULL, 'inactive', 'sell', 'Cristian2', '12345678', 15, 0, 6120, 2, NULL, false, false, false, 'Libro 2', 'Este es el libro 2', 12000, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$ha~7KP](U[j}2+(Z67dce8f73ae6a6b43a0b45b40fef6ed1a37b44d9', NULL, 'es');
INSERT INTO ads VALUES (90, NULL, NULL, 'inactive', 'sell', 'Android', '123123', 15, 0, 3060, 50, NULL, false, false, false, 'Aviso de usuario Android', 'Aviso de android', 76767, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$-|!zL];Lt.!8Yj,X2126a5304c933e6dc2a72c6e31a2b60015c3f145', NULL, 'es');
INSERT INTO ads VALUES (91, NULL, NULL, 'inactive', 'sell', 'Cristian3', '12345678', 15, 0, 6120, 3, NULL, false, false, false, 'Libro 3', 'Este es el libro 3', 14000, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$B9iv"HW.#?NWNIPC2838e4ef9e52c8a0d4d8b1c02eed0837bbd4e2c1', NULL, 'es');
INSERT INTO ads VALUES (92, NULL, NULL, 'inactive', 'sell', 'Alan', '123123', 15, 0, 3080, 52, NULL, false, false, false, 'Aviso de Alan', 'foobar', 456, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$GI\\<D`00I]>t%me"dc5e7a35b23b90789f743b32b1c21fd48d81f4bc', NULL, 'es');
INSERT INTO ads VALUES (93, NULL, NULL, 'inactive', 'sell', 'Cristian4', '12345678', 15, 0, 6120, 4, NULL, false, false, false, 'Libro 4', 'Este es el libro 4', 16000, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$/2o<)jV''dR(!8EWG2088bfcc89376d6cfa73fea5a31c346bc5945a67', NULL, 'es');


--
-- Data for Name: payment_groups; Type: TABLE DATA; Schema: public; Owner: dany
--

INSERT INTO payment_groups VALUES (0, '012345678', 'unverified', '2013-06-04 10:38:33.303857', NULL);
INSERT INTO payment_groups VALUES (1, '123456789', 'unverified', '2013-06-04 10:38:33.305798', NULL);
INSERT INTO payment_groups VALUES (2, '11223', 'unpaid', '2013-06-04 10:38:33.306167', NULL);
INSERT INTO payment_groups VALUES (3, '200000001', 'unverified', '2013-06-04 10:38:33.306429', NULL);
INSERT INTO payment_groups VALUES (50, '59876', 'cleared', '2013-06-04 10:38:33.306638', NULL);
INSERT INTO payment_groups VALUES (60, '112200000', 'verified', '2013-06-04 10:41:31.829948', NULL);
INSERT INTO payment_groups VALUES (122, '975777219', 'verified', '2013-06-04 12:31:59.414171', NULL);
INSERT INTO payment_groups VALUES (61, '112200001', 'verified', '2013-06-04 10:45:36.89995', NULL);
INSERT INTO payment_groups VALUES (62, '112200002', 'verified', '2013-06-04 10:46:56.886233', NULL);
INSERT INTO payment_groups VALUES (63, '112200003', 'verified', '2013-06-04 10:48:27.488435', NULL);
INSERT INTO payment_groups VALUES (64, '112200004', 'verified', '2013-06-04 10:50:16.097755', NULL);
INSERT INTO payment_groups VALUES (65, '112200005', 'verified', '2013-06-04 10:51:33.747232', NULL);
INSERT INTO payment_groups VALUES (66, '112200006', 'verified', '2013-06-04 10:55:01.248764', NULL);
INSERT INTO payment_groups VALUES (67, '112200007', 'verified', '2013-06-04 10:58:10.031048', NULL);
INSERT INTO payment_groups VALUES (68, '112200008', 'verified', '2013-06-04 10:59:39.870471', NULL);
INSERT INTO payment_groups VALUES (69, '112200009', 'verified', '2013-06-04 11:01:06.897025', NULL);
INSERT INTO payment_groups VALUES (70, '235392023', 'verified', '2013-06-04 11:03:03.085742', NULL);
INSERT INTO payment_groups VALUES (71, '370784046', 'verified', '2013-06-04 11:20:25.769106', NULL);
INSERT INTO payment_groups VALUES (72, '506176069', 'verified', '2013-06-04 11:21:46.226704', NULL);
INSERT INTO payment_groups VALUES (73, '641568092', 'verified', '2013-06-04 11:23:40.532448', NULL);
INSERT INTO payment_groups VALUES (74, '776960115', 'verified', '2013-06-04 11:32:13.874344', NULL);
INSERT INTO payment_groups VALUES (75, '912352138', 'verified', '2013-06-04 11:34:44.554944', NULL);
INSERT INTO payment_groups VALUES (76, '147744161', 'verified', '2013-06-04 11:35:11.862084', NULL);
INSERT INTO payment_groups VALUES (77, '283136184', 'verified', '2013-06-04 11:36:18.949759', NULL);
INSERT INTO payment_groups VALUES (78, '418528207', 'verified', '2013-06-04 11:37:20.86608', NULL);
INSERT INTO payment_groups VALUES (79, '553920230', 'verified', '2013-06-04 11:39:02.979047', NULL);
INSERT INTO payment_groups VALUES (80, '689312253', 'verified', '2013-06-04 11:52:05.993009', NULL);
INSERT INTO payment_groups VALUES (81, '824704276', 'verified', '2013-06-04 11:53:08.606456', NULL);
INSERT INTO payment_groups VALUES (82, '960096299', 'verified', '2013-06-04 11:53:13.823383', NULL);
INSERT INTO payment_groups VALUES (83, '195488322', 'verified', '2013-06-04 11:53:54.773615', NULL);
INSERT INTO payment_groups VALUES (84, '330880345', 'verified', '2013-06-04 11:54:21.822676', NULL);
INSERT INTO payment_groups VALUES (85, '466272368', 'verified', '2013-06-04 11:54:53.034857', NULL);
INSERT INTO payment_groups VALUES (86, '601664391', 'verified', '2013-06-04 11:55:40.055155', NULL);
INSERT INTO payment_groups VALUES (87, '737056414', 'verified', '2013-06-04 11:55:59.631308', NULL);
INSERT INTO payment_groups VALUES (88, '872448437', 'verified', '2013-06-04 11:56:15.422455', NULL);
INSERT INTO payment_groups VALUES (89, '107840460', 'verified', '2013-06-04 11:56:39.543316', NULL);
INSERT INTO payment_groups VALUES (90, '243232483', 'verified', '2013-06-04 11:57:00.493136', NULL);
INSERT INTO payment_groups VALUES (91, '378624506', 'verified', '2013-06-04 11:57:12.480431', NULL);
INSERT INTO payment_groups VALUES (92, '514016529', 'verified', '2013-06-04 11:57:54.844198', NULL);
INSERT INTO payment_groups VALUES (93, '649408552', 'verified', '2013-06-04 11:58:19.445929', NULL);
INSERT INTO payment_groups VALUES (94, '784800575', 'verified', '2013-06-04 11:58:32.444333', NULL);
INSERT INTO payment_groups VALUES (95, '920192598', 'verified', '2013-06-04 11:58:56.208915', NULL);
INSERT INTO payment_groups VALUES (96, '155584621', 'verified', '2013-06-04 11:59:30.351197', NULL);
INSERT INTO payment_groups VALUES (97, '290976644', 'verified', '2013-06-04 12:00:14.014412', NULL);
INSERT INTO payment_groups VALUES (98, '426368667', 'verified', '2013-06-04 12:06:49.933293', NULL);
INSERT INTO payment_groups VALUES (99, '561760690', 'verified', '2013-06-04 12:06:51.505695', NULL);
INSERT INTO payment_groups VALUES (100, '697152713', 'verified', '2013-06-04 12:07:42.721584', NULL);
INSERT INTO payment_groups VALUES (101, '832544736', 'verified', '2013-06-04 12:10:01.595201', NULL);
INSERT INTO payment_groups VALUES (102, '967936759', 'verified', '2013-06-04 12:10:55.070333', NULL);
INSERT INTO payment_groups VALUES (103, '203328782', 'verified', '2013-06-04 12:13:22.259481', NULL);
INSERT INTO payment_groups VALUES (104, '338720805', 'verified', '2013-06-04 12:14:08.083916', NULL);
INSERT INTO payment_groups VALUES (105, '474112828', 'verified', '2013-06-04 12:14:57.405542', NULL);
INSERT INTO payment_groups VALUES (106, '609504851', 'verified', '2013-06-04 12:15:46.720656', NULL);
INSERT INTO payment_groups VALUES (107, '744896874', 'verified', '2013-06-04 12:18:25.410375', NULL);
INSERT INTO payment_groups VALUES (108, '880288897', 'verified', '2013-06-04 12:19:17.40622', NULL);
INSERT INTO payment_groups VALUES (109, '115680920', 'verified', '2013-06-04 12:20:12.431433', NULL);
INSERT INTO payment_groups VALUES (110, '251072943', 'verified', '2013-06-04 12:22:45.227628', NULL);
INSERT INTO payment_groups VALUES (111, '386464966', 'verified', '2013-06-04 12:23:33.113503', NULL);
INSERT INTO payment_groups VALUES (112, '521856989', 'verified', '2013-06-04 12:23:58.691985', NULL);
INSERT INTO payment_groups VALUES (113, '657249012', 'verified', '2013-06-04 12:24:24.977517', NULL);
INSERT INTO payment_groups VALUES (114, '792641035', 'verified', '2013-06-04 12:24:59.209525', NULL);
INSERT INTO payment_groups VALUES (115, '928033058', 'verified', '2013-06-04 12:25:27.502975', NULL);
INSERT INTO payment_groups VALUES (116, '163425081', 'verified', '2013-06-04 12:26:02.414732', NULL);
INSERT INTO payment_groups VALUES (117, '298817104', 'verified', '2013-06-04 12:28:19.211592', NULL);
INSERT INTO payment_groups VALUES (118, '434209127', 'verified', '2013-06-04 12:28:41.75276', NULL);
INSERT INTO payment_groups VALUES (119, '569601150', 'verified', '2013-06-04 12:30:25.456462', NULL);
INSERT INTO payment_groups VALUES (120, '704993173', 'verified', '2013-06-04 12:30:57.353407', NULL);
INSERT INTO payment_groups VALUES (121, '840385196', 'verified', '2013-06-04 12:31:33.823614', NULL);
INSERT INTO payment_groups VALUES (123, '211169242', 'verified', '2013-08-06 15:18:24.449334', NULL);
INSERT INTO payment_groups VALUES (124, '346561265', 'verified', '2013-08-06 15:19:06.123318', NULL);
INSERT INTO payment_groups VALUES (125, '481953288', 'verified', '2013-08-06 15:35:32.585346', NULL);
INSERT INTO payment_groups VALUES (126, '617345311', 'verified', '2013-08-06 15:36:33.000271', NULL);
INSERT INTO payment_groups VALUES (127, '752737334', 'verified', '2013-08-06 15:36:39.387043', NULL);
INSERT INTO payment_groups VALUES (128, '888129357', 'verified', '2013-08-06 15:37:24.915844', NULL);
INSERT INTO payment_groups VALUES (129, '123521380', 'verified', '2013-08-06 15:37:35.262003', NULL);
INSERT INTO payment_groups VALUES (130, '258913403', 'verified', '2013-08-06 15:38:19.414708', NULL);
INSERT INTO payment_groups VALUES (131, '394305426', 'verified', '2013-08-06 15:38:39.627571', NULL);
INSERT INTO payment_groups VALUES (132, '529697449', 'verified', '2013-08-06 15:39:10.342827', NULL);
INSERT INTO payment_groups VALUES (133, '665089472', 'verified', '2013-08-06 15:39:21.644011', NULL);


--
-- Data for Name: ad_actions; Type: TABLE DATA; Schema: public; Owner: dany
--

INSERT INTO ad_actions VALUES (1, 1, 'new', 154, 'accepted', 'normal', NULL, NULL, 0);
INSERT INTO ad_actions VALUES (2, 1, 'new', 4, 'unverified', 'normal', NULL, NULL, 1);
INSERT INTO ad_actions VALUES (3, 1, 'new', 6, 'unpaid', 'normal', NULL, NULL, 2);
INSERT INTO ad_actions VALUES (4, 1, 'new', 8, 'unverified', 'normal', NULL, NULL, 3);
INSERT INTO ad_actions VALUES (8, 1, 'new', 9, 'accepted', 'normal', NULL, NULL, NULL);
INSERT INTO ad_actions VALUES (10, 1, 'new', 11, 'accepted', 'normal', NULL, NULL, NULL);
INSERT INTO ad_actions VALUES (11, 1, 'new', 12, 'accepted', 'normal', NULL, NULL, NULL);
INSERT INTO ad_actions VALUES (6, 1, 'new', 53, 'accepted', 'normal', NULL, NULL, 50);
INSERT INTO ad_actions VALUES (9, 1, 'new', 43, 'refused', 'normal', NULL, NULL, NULL);
INSERT INTO ad_actions VALUES (12, 1, 'new', 164, 'accepted', 'normal', NULL, NULL, 0);
INSERT INTO ad_actions VALUES (68, 1, 'new', 451, 'accepted', 'normal', 9, '2013-06-04 12:25:43.410987', 108);
INSERT INTO ad_actions VALUES (82, 1, 'new', 518, 'accepted', 'normal', 9, '2013-06-04 12:37:48.866543', 122);
INSERT INTO ad_actions VALUES (69, 1, 'new', 453, 'accepted', 'normal', 9, '2013-06-04 12:25:50.694259', 109);
INSERT INTO ad_actions VALUES (78, 1, 'new', 511, 'accepted', 'normal', 9, '2013-06-04 12:37:36.515351', 118);
INSERT INTO ad_actions VALUES (48, 1, 'new', 373, 'accepted', 'normal', 9, '2013-06-04 12:04:27.537222', 88);
INSERT INTO ad_actions VALUES (49, 1, 'new', 374, 'accepted', 'normal', 9, '2013-06-04 12:04:27.537222', 89);
INSERT INTO ad_actions VALUES (74, 1, 'new', 503, 'accepted', 'normal', 9, '2013-06-04 12:37:25.468453', 114);
INSERT INTO ad_actions VALUES (75, 1, 'new', 504, 'accepted', 'normal', 9, '2013-06-04 12:37:25.468453', 115);
INSERT INTO ad_actions VALUES (26, 1, 'new', 247, 'accepted', 'normal', 9, '2013-06-04 11:08:09.546316', 66);
INSERT INTO ad_actions VALUES (27, 1, 'new', 248, 'accepted', 'normal', 9, '2013-06-04 11:08:09.546316', 67);
INSERT INTO ad_actions VALUES (28, 1, 'new', 251, 'accepted', 'normal', 9, '2013-06-04 11:08:19.908236', 68);
INSERT INTO ad_actions VALUES (29, 1, 'new', 252, 'accepted', 'normal', 9, '2013-06-04 11:08:19.908236', 69);
INSERT INTO ad_actions VALUES (40, 1, 'new', 351, 'accepted', 'normal', 9, '2013-06-04 12:03:43.505048', 80);
INSERT INTO ad_actions VALUES (41, 1, 'new', 352, 'accepted', 'normal', 9, '2013-06-04 12:03:43.505048', 81);
INSERT INTO ad_actions VALUES (30, 1, 'new', 254, 'accepted', 'normal', 9, '2013-06-04 11:08:27.864857', 70);
INSERT INTO ad_actions VALUES (20, 1, 'new', 220, 'accepted', 'normal', 9, '2013-06-04 10:56:47.42484', 60);
INSERT INTO ad_actions VALUES (21, 1, 'new', 221, 'accepted', 'normal', 9, '2013-06-04 10:56:47.42484', 61);
INSERT INTO ad_actions VALUES (50, 1, 'new', 377, 'accepted', 'normal', 9, '2013-06-04 12:04:39.576395', 90);
INSERT INTO ad_actions VALUES (22, 1, 'new', 224, 'accepted', 'normal', 9, '2013-06-04 10:57:04.632691', 62);
INSERT INTO ad_actions VALUES (23, 1, 'new', 225, 'accepted', 'normal', 9, '2013-06-04 10:57:04.632691', 63);
INSERT INTO ad_actions VALUES (58, 1, 'new', 411, 'accepted', 'normal', 9, '2013-06-04 12:16:11.320805', 98);
INSERT INTO ad_actions VALUES (59, 1, 'new', 412, 'accepted', 'normal', 9, '2013-06-04 12:16:11.320805', 99);
INSERT INTO ad_actions VALUES (51, 1, 'new', 378, 'accepted', 'normal', 9, '2013-06-04 12:04:39.576395', 91);
INSERT INTO ad_actions VALUES (79, 1, 'new', 512, 'accepted', 'normal', 9, '2013-06-04 12:37:36.515351', 119);
INSERT INTO ad_actions VALUES (24, 1, 'new', 228, 'accepted', 'normal', 9, '2013-06-04 10:57:14.866453', 64);
INSERT INTO ad_actions VALUES (25, 1, 'new', 229, 'accepted', 'normal', 9, '2013-06-04 10:57:14.866453', 65);
INSERT INTO ad_actions VALUES (60, 1, 'new', 415, 'accepted', 'normal', 9, '2013-06-04 12:16:23.386507', 100);
INSERT INTO ad_actions VALUES (61, 1, 'new', 416, 'accepted', 'normal', 9, '2013-06-04 12:16:23.386507', 101);
INSERT INTO ad_actions VALUES (42, 1, 'new', 358, 'accepted', 'normal', 9, '2013-06-04 12:03:53.862787', 82);
INSERT INTO ad_actions VALUES (43, 1, 'new', 359, 'accepted', 'normal', 9, '2013-06-04 12:03:53.862787', 83);
INSERT INTO ad_actions VALUES (62, 1, 'new', 418, 'accepted', 'normal', 9, '2013-06-04 12:16:32.017175', 102);
INSERT INTO ad_actions VALUES (31, 1, 'new', 295, 'accepted', 'normal', 9, '2013-06-04 11:43:45.399338', 71);
INSERT INTO ad_actions VALUES (32, 1, 'new', 296, 'accepted', 'normal', 9, '2013-06-04 11:44:03.802708', 72);
INSERT INTO ad_actions VALUES (35, 1, 'new', 297, 'accepted', 'normal', 9, '2013-06-04 11:44:06.289955', 75);
INSERT INTO ad_actions VALUES (36, 1, 'new', 298, 'accepted', 'normal', 9, '2013-06-04 11:44:08.995533', 76);
INSERT INTO ad_actions VALUES (39, 1, 'new', 299, 'accepted', 'normal', 9, '2013-06-04 11:44:12.668836', 79);
INSERT INTO ad_actions VALUES (37, 1, 'new', 300, 'accepted', 'normal', 9, '2013-06-04 11:44:08.995533', 77);
INSERT INTO ad_actions VALUES (38, 1, 'new', 301, 'accepted', 'normal', 9, '2013-06-04 11:44:12.668836', 78);
INSERT INTO ad_actions VALUES (33, 1, 'new', 302, 'accepted', 'normal', 9, '2013-06-04 11:44:03.802708', 73);
INSERT INTO ad_actions VALUES (34, 1, 'new', 303, 'accepted', 'normal', 9, '2013-06-04 11:44:06.289955', 74);
INSERT INTO ad_actions VALUES (44, 1, 'new', 362, 'accepted', 'normal', 9, '2013-06-04 12:04:07.852271', 84);
INSERT INTO ad_actions VALUES (45, 1, 'new', 363, 'accepted', 'normal', 9, '2013-06-04 12:04:07.852271', 85);
INSERT INTO ad_actions VALUES (46, 1, 'new', 366, 'accepted', 'normal', 9, '2013-06-04 12:04:19.400463', 86);
INSERT INTO ad_actions VALUES (47, 1, 'new', 367, 'accepted', 'normal', 9, '2013-06-04 12:04:19.400463', 87);
INSERT INTO ad_actions VALUES (63, 1, 'new', 442, 'accepted', 'normal', 9, '2013-06-04 12:25:22.53332', 103);
INSERT INTO ad_actions VALUES (52, 1, 'new', 381, 'accepted', 'normal', 9, '2013-06-04 12:04:47.261721', 92);
INSERT INTO ad_actions VALUES (64, 1, 'new', 443, 'accepted', 'normal', 9, '2013-06-04 12:25:22.53332', 104);
INSERT INTO ad_actions VALUES (53, 1, 'new', 382, 'accepted', 'normal', 9, '2013-06-04 12:04:47.261721', 93);
INSERT INTO ad_actions VALUES (54, 1, 'new', 385, 'accepted', 'normal', 9, '2013-06-04 12:04:55.113344', 94);
INSERT INTO ad_actions VALUES (55, 1, 'new', 386, 'accepted', 'normal', 9, '2013-06-04 12:04:55.113344', 95);
INSERT INTO ad_actions VALUES (56, 1, 'new', 391, 'accepted', 'normal', 9, '2013-06-04 12:05:13.537556', 96);
INSERT INTO ad_actions VALUES (65, 1, 'new', 446, 'accepted', 'normal', 9, '2013-06-04 12:25:35.126261', 105);
INSERT INTO ad_actions VALUES (57, 1, 'new', 393, 'accepted', 'normal', 9, '2013-06-04 12:05:19.353946', 97);
INSERT INTO ad_actions VALUES (66, 1, 'new', 447, 'accepted', 'normal', 9, '2013-06-04 12:25:35.126261', 106);
INSERT INTO ad_actions VALUES (70, 1, 'new', 495, 'accepted', 'normal', 9, '2013-06-04 12:37:07.486473', 110);
INSERT INTO ad_actions VALUES (71, 1, 'new', 496, 'accepted', 'normal', 9, '2013-06-04 12:37:07.486473', 111);
INSERT INTO ad_actions VALUES (67, 1, 'new', 450, 'accepted', 'normal', 9, '2013-06-04 12:25:43.410987', 107);
INSERT INTO ad_actions VALUES (72, 1, 'new', 499, 'accepted', 'normal', 9, '2013-06-04 12:37:19.070434', 112);
INSERT INTO ad_actions VALUES (73, 1, 'new', 500, 'accepted', 'normal', 9, '2013-06-04 12:37:19.070434', 113);
INSERT INTO ad_actions VALUES (76, 1, 'new', 507, 'accepted', 'normal', 9, '2013-06-04 12:37:30.984453', 116);
INSERT INTO ad_actions VALUES (77, 1, 'new', 508, 'accepted', 'normal', 9, '2013-06-04 12:37:30.984453', 117);
INSERT INTO ad_actions VALUES (80, 1, 'new', 515, 'refused', 'normal', 9, '2013-06-04 12:37:43.517565', 120);
INSERT INTO ad_actions VALUES (81, 1, 'new', 516, 'accepted', 'normal', 9, '2013-06-04 12:37:43.517565', 121);
INSERT INTO ad_actions VALUES (83, 1, 'new', 527, 'accepted', 'normal', 9, '2013-08-06 15:24:26.84191', 123);
INSERT INTO ad_actions VALUES (84, 1, 'new', 528, 'accepted', 'normal', 9, '2013-08-06 15:24:26.84191', 124);
INSERT INTO ad_actions VALUES (85, 1, 'new', 531, 'pending_review', 'whitelist', NULL, NULL, 125);
INSERT INTO ad_actions VALUES (89, 1, 'new', 543, 'pending_review', 'whitelist', NULL, NULL, 129);
INSERT INTO ad_actions VALUES (86, 1, 'new', 534, 'pending_review', 'whitelist', NULL, NULL, 126);
INSERT INTO ad_actions VALUES (88, 1, 'new', 540, 'pending_review', 'whitelist', NULL, NULL, 128);
INSERT INTO ad_actions VALUES (90, 1, 'new', 546, 'pending_review', 'whitelist', NULL, NULL, 130);
INSERT INTO ad_actions VALUES (91, 1, 'new', 549, 'pending_review', 'whitelist', NULL, NULL, 131);
INSERT INTO ad_actions VALUES (93, 1, 'new', 555, 'pending_review', 'whitelist', NULL, NULL, 133);
INSERT INTO ad_actions VALUES (87, 1, 'new', 556, 'locked', 'normal', 9, '2013-08-06 15:44:34.797529', 127);
INSERT INTO ad_actions VALUES (92, 1, 'new', 557, 'locked', 'normal', 9, '2013-08-06 15:44:34.797529', 132);


--
-- Data for Name: action_params; Type: TABLE DATA; Schema: public; Owner: dany
--

INSERT INTO action_params VALUES (20, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params VALUES (21, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params VALUES (22, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params VALUES (23, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params VALUES (24, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params VALUES (25, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params VALUES (26, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params VALUES (27, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params VALUES (28, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params VALUES (29, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params VALUES (30, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params VALUES (31, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params VALUES (32, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params VALUES (33, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params VALUES (34, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params VALUES (35, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params VALUES (36, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params VALUES (37, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params VALUES (38, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params VALUES (39, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params VALUES (40, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params VALUES (41, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params VALUES (42, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params VALUES (43, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params VALUES (44, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params VALUES (45, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params VALUES (46, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params VALUES (47, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params VALUES (48, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params VALUES (49, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params VALUES (50, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params VALUES (51, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params VALUES (52, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params VALUES (53, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params VALUES (54, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params VALUES (55, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params VALUES (56, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params VALUES (57, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params VALUES (58, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params VALUES (59, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params VALUES (60, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params VALUES (61, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params VALUES (62, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params VALUES (63, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params VALUES (64, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params VALUES (65, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params VALUES (66, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params VALUES (67, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params VALUES (68, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params VALUES (69, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params VALUES (70, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params VALUES (71, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params VALUES (72, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params VALUES (73, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params VALUES (74, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params VALUES (75, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params VALUES (76, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params VALUES (77, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params VALUES (78, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params VALUES (79, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params VALUES (80, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params VALUES (81, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params VALUES (82, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params VALUES (83, 1, 'source', 'web');
INSERT INTO action_params VALUES (83, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params VALUES (84, 1, 'source', 'web');
INSERT INTO action_params VALUES (84, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params VALUES (85, 1, 'source', 'web');
INSERT INTO action_params VALUES (85, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params VALUES (86, 1, 'source', 'web');
INSERT INTO action_params VALUES (86, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params VALUES (87, 1, 'source', 'web');
INSERT INTO action_params VALUES (87, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params VALUES (88, 1, 'source', 'web');
INSERT INTO action_params VALUES (88, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params VALUES (89, 1, 'source', 'web');
INSERT INTO action_params VALUES (89, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params VALUES (90, 1, 'source', 'web');
INSERT INTO action_params VALUES (90, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params VALUES (91, 1, 'source', 'web');
INSERT INTO action_params VALUES (91, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params VALUES (92, 1, 'source', 'web');
INSERT INTO action_params VALUES (92, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params VALUES (93, 1, 'source', 'web');
INSERT INTO action_params VALUES (93, 1, 'redir', 'dW5rbm93bg==');


--
-- Data for Name: stores; Type: TABLE DATA; Schema: public; Owner: dany
--



--
-- Data for Name: tokens; Type: TABLE DATA; Schema: public; Owner: dany
--

INSERT INTO tokens VALUES (99999917, 'd12ac643ce5c39ddb765bc452d40790932d7c0f67823aaa', 5, '2006-04-06 15:19:46', '2006-04-06 16:19:46', '192.168.4.75', 'clear', NULL, NULL);
INSERT INTO tokens VALUES (99999918, 'a5e4e5b16bc9c806b1c22cfd638440034791acc68f23600', 5, '2006-04-06 16:19:31', '2006-04-06 16:19:46', '192.168.4.75', 'authenticate', NULL, NULL);
INSERT INTO tokens VALUES (99999919, 'a80cbcd0b019780754593a1ea8b28001c88086439101100', 5, '2006-04-06 16:19:46', '2006-04-06 16:19:46', '192.168.4.75', 'clear', NULL, NULL);
INSERT INTO tokens VALUES (99999992, 'b7e4e5b16bc9c806b1c22cfd638440034791acc68f23600', 5, '2011-11-06 05:19:31', '2011-11-06 05:19:46', '192.168.4.75', 'adqueues', NULL, NULL);
INSERT INTO tokens VALUES (99999993, 'b9e4e5b16bc9c806b1c22cfd638440034791acc68f23600', 5, '2011-11-06 05:19:31', '2011-11-06 05:19:46', '192.168.4.75', 'adqueues', NULL, NULL);
INSERT INTO tokens VALUES (99999998, 'b5e4e5b16bc9c806b1c22cfd638440034791acc68f23600', 5, '2006-04-06 15:19:31', '2006-04-06 15:19:46', '192.168.4.75', 'authenticate', NULL, NULL);
INSERT INTO tokens VALUES (99999999, '580cbcd0b019780754593a1ea8b28001c88086439101100', 5, '2006-04-06 15:19:46', '2006-04-06 16:19:46', '192.168.4.75', 'clear', NULL, NULL);
INSERT INTO tokens VALUES (99999994, 'b7e4e9b16bc9c806b1c22cfd638440034791acc68f23600', 5, '2011-11-06 05:19:31', '2011-11-06 05:19:46', '192.168.4.75', 'adqueues', NULL, NULL);
INSERT INTO tokens VALUES (99999995, 'b9e6e6b16bc9c806b1c22cfd638440034791acc68f23600', 5, '2011-11-06 06:19:31', '2011-11-06 06:19:46', '192.168.4.75', 'adqueues', NULL, NULL);
INSERT INTO tokens VALUES (1, 'X51adff0050db6242000000001c02644600000000', 9, '2013-06-04 10:51:44.12871', '2013-06-04 10:51:44.241718', '10.0.1.173', 'authenticate', NULL, NULL);
INSERT INTO tokens VALUES (2, 'X51adff007b9e6ab40000000033fdacfe00000000', 9, '2013-06-04 10:51:44.241718', '2013-06-04 10:51:46.07632', '10.0.1.173', 'spamfilter', NULL, NULL);
INSERT INTO tokens VALUES (3, 'X51adff025f15e630000000012a280f200000000', 9, '2013-06-04 10:51:46.07632', '2013-06-04 10:51:47.422706', '10.0.1.173', 'adqueues', NULL, NULL);
INSERT INTO tokens VALUES (4, 'X51adff036d8020c5000000004680a2ba00000000', 9, '2013-06-04 10:51:47.422706', '2013-06-04 10:51:47.47993', '10.0.1.173', 'adqueues', NULL, NULL);
INSERT INTO tokens VALUES (5, 'X51adff033a1135fd0000000019688b0700000000', 9, '2013-06-04 10:51:47.47993', '2013-06-04 10:51:47.483145', '10.0.1.173', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (6, 'X51adff032f6a23b20000000025f59b4e00000000', 9, '2013-06-04 10:51:47.483145', '2013-06-04 10:51:47.486387', '10.0.1.173', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (7, 'X51adff0381d06ae0000000030b47a9600000000', 9, '2013-06-04 10:51:47.486387', '2013-06-04 10:51:47.493302', '10.0.1.173', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (8, 'X51adff032eef6e11000000006f1efbc000000000', 9, '2013-06-04 10:51:47.493302', '2013-06-04 10:51:47.49604', '10.0.1.173', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (9, 'X51adff032d3cd71600000000167e4ebe00000000', 9, '2013-06-04 10:51:47.49604', '2013-06-04 10:51:47.499433', '10.0.1.173', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (10, 'X51adff03642c5a3b000000006495631400000000', 9, '2013-06-04 10:51:47.499433', '2013-06-04 10:51:58.844965', '10.0.1.173', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (11, 'X51adff0f4f1e5e57000000001c0589b400000000', 9, '2013-06-04 10:51:58.844965', '2013-06-04 10:52:03.055796', '10.0.1.173', 'review', NULL, NULL);
INSERT INTO tokens VALUES (12, 'X51adff133b99b5eb0000000059da236000000000', 9, '2013-06-04 10:52:03.055796', '2013-06-04 10:52:04.630596', '10.0.1.173', 'review', NULL, NULL);
INSERT INTO tokens VALUES (13, 'X51adff1577c23ae10000000079e98a8f00000000', 9, '2013-06-04 10:52:04.630596', '2013-06-04 10:52:04.667095', '10.0.1.173', 'adqueues', NULL, NULL);
INSERT INTO tokens VALUES (14, 'X51adff15d53928100000000672cd2fc00000000', 9, '2013-06-04 10:52:04.667095', '2013-06-04 10:52:04.670079', '10.0.1.173', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (15, 'X51adff15604a2d60000000046c3fe7000000000', 9, '2013-06-04 10:52:04.670079', '2013-06-04 10:52:04.673485', '10.0.1.173', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (16, 'X51adff1536e8c923000000001c20743000000000', 9, '2013-06-04 10:52:04.673485', '2013-06-04 10:52:04.680621', '10.0.1.173', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (17, 'X51adff1544abfbe0000000007c42b6500000000', 9, '2013-06-04 10:52:04.680621', '2013-06-04 10:52:04.683402', '10.0.1.173', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (18, 'X51adff153822d87600000000404a669400000000', 9, '2013-06-04 10:52:04.683402', '2013-06-04 10:52:04.686621', '10.0.1.173', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (19, 'X51adff153bc1d864000000003e1436da00000000', 9, '2013-06-04 10:52:04.686621', '2013-06-04 10:52:10.492843', '10.0.1.173', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (20, 'X51adff1a52ece787000000002941f92900000000', 9, '2013-06-04 10:52:10.492843', '2013-06-04 10:52:12.981816', '10.0.1.173', 'review', NULL, NULL);
INSERT INTO tokens VALUES (21, 'X51adff1d7fbe45bb0000000037f0383600000000', 9, '2013-06-04 10:52:12.981816', '2013-06-04 10:52:14.864428', '10.0.1.173', 'review', NULL, NULL);
INSERT INTO tokens VALUES (22, 'X51adff1f494d99400000000cfe1d8400000000', 9, '2013-06-04 10:52:14.864428', '2013-06-04 10:52:14.902946', '10.0.1.173', 'adqueues', NULL, NULL);
INSERT INTO tokens VALUES (23, 'X51adff1f42aa84310000000033fefd4700000000', 9, '2013-06-04 10:52:14.902946', '2013-06-04 10:52:14.905796', '10.0.1.173', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (24, 'X51adff1f32f3b8d2000000004ac78adf00000000', 9, '2013-06-04 10:52:14.905796', '2013-06-04 10:52:14.909073', '10.0.1.173', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (25, 'X51adff1f64b377dd0000000061e326e300000000', 9, '2013-06-04 10:52:14.909073', '2013-06-04 10:52:14.91583', '10.0.1.173', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (26, 'X51adff1f39e686a00000000011f04ef300000000', 9, '2013-06-04 10:52:14.91583', '2013-06-04 10:52:14.918589', '10.0.1.173', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (27, 'X51adff1f786175a1000000001e12e0db00000000', 9, '2013-06-04 10:52:14.918589', '2013-06-04 10:52:14.921782', '10.0.1.173', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (28, 'X51adff1f7685b20800000000477fd3f800000000', 9, '2013-06-04 10:52:14.921782', '2013-06-04 10:52:19.029251', '10.0.1.173', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (29, 'X51adff233a186a8f000000006e47ece900000000', 9, '2013-06-04 10:52:19.029251', '2013-06-04 10:52:21.667293', '10.0.1.173', 'review', NULL, NULL);
INSERT INTO tokens VALUES (30, 'X51adff267d0d55b40000000054300a7400000000', 9, '2013-06-04 10:52:21.667293', '2013-06-04 10:52:23.0104', '10.0.1.173', 'review', NULL, NULL);
INSERT INTO tokens VALUES (31, 'X51adff2741695e8700000000476bfd1100000000', 9, '2013-06-04 10:52:23.0104', '2013-06-04 10:52:23.014556', '10.0.1.173', 'adqueues', NULL, NULL);
INSERT INTO tokens VALUES (32, 'X51adff275574bfe500000000476e015e00000000', 9, '2013-06-04 10:52:23.014556', '2013-06-04 11:03:09.540648', '10.0.1.173', 'adqueues', NULL, NULL);
INSERT INTO tokens VALUES (33, 'X51ae01ae19f418c5000000006564452500000000', 9, '2013-06-04 11:03:09.540648', '2013-06-04 11:03:09.63057', '10.0.1.173', 'adqueues', NULL, NULL);
INSERT INTO tokens VALUES (34, 'X51ae01ae3333faa70000000013ac8d2d00000000', 9, '2013-06-04 11:03:09.63057', '2013-06-04 11:03:09.633577', '10.0.1.173', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (35, 'X51ae01ae10a2b34f00000000777c2cb200000000', 9, '2013-06-04 11:03:09.633577', '2013-06-04 11:03:09.637008', '10.0.1.173', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (36, 'X51ae01ae36deb766000000007ffcca4300000000', 9, '2013-06-04 11:03:09.637008', '2013-06-04 11:03:09.644009', '10.0.1.173', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (37, 'X51ae01ae3c2a0d79000000001015d7ff00000000', 9, '2013-06-04 11:03:09.644009', '2013-06-04 11:03:09.646971', '10.0.1.173', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (38, 'X51ae01ae142443b70000000059d2cdd000000000', 9, '2013-06-04 11:03:09.646971', '2013-06-04 11:03:09.65024', '10.0.1.173', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (39, 'X51ae01ae304118640000000013430c1700000000', 9, '2013-06-04 11:03:09.65024', '2013-06-04 11:03:15.130271', '10.0.1.173', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (40, 'X51ae01b3348cc8b9000000001e16f0dd00000000', 9, '2013-06-04 11:03:15.130271', '2013-06-04 11:03:18.289006', '10.0.1.173', 'review', NULL, NULL);
INSERT INTO tokens VALUES (41, 'X51ae01b659406cef0000000029a14c3500000000', 9, '2013-06-04 11:03:18.289006', '2013-06-04 11:03:19.906206', '10.0.1.173', 'review', NULL, NULL);
INSERT INTO tokens VALUES (42, 'X51ae01b870385578000000006483291300000000', 9, '2013-06-04 11:03:19.906206', '2013-06-04 11:03:19.989235', '10.0.1.173', 'adqueues', NULL, NULL);
INSERT INTO tokens VALUES (43, 'X51ae01b82f17fc5c000000004854792c00000000', 9, '2013-06-04 11:03:19.989235', '2013-06-04 11:03:19.992236', '10.0.1.173', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (44, 'X51ae01b83dbd3d400000000051bb1a4e00000000', 9, '2013-06-04 11:03:19.992236', '2013-06-04 11:03:19.995383', '10.0.1.173', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (45, 'X51ae01b847dd8e7000000005c41c48700000000', 9, '2013-06-04 11:03:19.995383', '2013-06-04 11:03:20.002233', '10.0.1.173', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (46, 'X51ae01b86d8ec8130000000057397b0e00000000', 9, '2013-06-04 11:03:20.002233', '2013-06-04 11:03:20.005052', '10.0.1.173', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (47, 'X51ae01b830f8b86700000000f6515a600000000', 9, '2013-06-04 11:03:20.005052', '2013-06-04 11:03:20.008192', '10.0.1.173', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (48, 'X51ae01b831b4e2f7000000005bbb52a200000000', 9, '2013-06-04 11:03:20.008192', '2013-06-04 11:03:23.767995', '10.0.1.173', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (49, 'X51ae01bc414bdcd8000000004ba8fbbc00000000', 9, '2013-06-04 11:03:23.767995', '2013-06-04 11:03:26.583798', '10.0.1.173', 'review', NULL, NULL);
INSERT INTO tokens VALUES (50, 'X51ae01bf31dcc6830000000055f6019a00000000', 9, '2013-06-04 11:03:26.583798', '2013-06-04 11:03:27.86224', '10.0.1.173', 'review', NULL, NULL);
INSERT INTO tokens VALUES (67, 'X51ae086f4747028000000000266299eb00000000', 9, '2013-06-04 11:31:59.077548', '2013-06-04 11:38:40.779564', '10.0.1.173', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (51, 'X51ae01c0411f97c700000000747fd77f00000000', 9, '2013-06-04 11:03:27.86224', '2013-06-04 11:03:27.922856', '10.0.1.173', 'adqueues', NULL, NULL);
INSERT INTO tokens VALUES (68, 'X51ae0a012fecd739000000001f8775b000000000', 9, '2013-06-04 11:38:40.779564', '2013-06-04 11:38:40.892469', '10.0.1.138', 'authenticate', NULL, NULL);
INSERT INTO tokens VALUES (52, 'X51ae01c05f5588ea0000000051c24b1600000000', 9, '2013-06-04 11:03:27.922856', '2013-06-04 11:03:27.926231', '10.0.1.173', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (152, 'X51ae0eb1ff47ce10000000072ba78c500000000', 9, '2013-06-04 11:58:40.699504', '2013-06-04 11:58:43.503522', '10.0.1.173', 'adqueues', NULL, NULL);
INSERT INTO tokens VALUES (53, 'X51ae01c06bfc0431000000001634405000000000', 9, '2013-06-04 11:03:27.926231', '2013-06-04 11:03:27.929532', '10.0.1.173', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (69, 'X51ae0a01711d97680000000039e268d100000000', 9, '2013-06-04 11:38:40.892469', '2013-06-04 11:38:43.007356', '10.0.1.138', 'spamfilter', NULL, NULL);
INSERT INTO tokens VALUES (54, 'X51ae01c051bf155900000000282611aa00000000', 9, '2013-06-04 11:03:27.929532', '2013-06-04 11:03:31.073338', '10.0.1.173', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (55, 'X51ae01c3264a184f0000000065e3591100000000', 9, '2013-06-04 11:03:31.073338', '2013-06-04 11:03:32.137553', '10.0.1.173', 'review', NULL, NULL);
INSERT INTO tokens VALUES (70, 'X51ae0a03778315d3000000004a901ca900000000', 9, '2013-06-04 11:38:43.007356', '2013-06-04 11:38:45.396315', '10.0.1.138', 'adqueues', NULL, NULL);
INSERT INTO tokens VALUES (56, 'X51ae01c47b611b0a00000000455d765d00000000', 9, '2013-06-04 11:03:32.137553', '2013-06-04 11:03:32.142901', '10.0.1.173', 'adqueues', NULL, NULL);
INSERT INTO tokens VALUES (57, 'X51ae01c43cf59b53000000006d9d59e700000000', 9, '2013-06-04 11:03:32.142901', '2013-06-04 11:31:51.082866', '10.0.1.173', 'adqueues', NULL, NULL);
INSERT INTO tokens VALUES (71, 'X51ae0a05f7d22c9000000002e1f526b00000000', 9, '2013-06-04 11:38:45.396315', '2013-06-04 11:38:45.499649', '10.0.1.138', 'adqueues', NULL, NULL);
INSERT INTO tokens VALUES (58, 'X51ae08675775fb1c0000000040c880e200000000', 9, '2013-06-04 11:31:51.082866', '2013-06-04 11:31:51.199263', '10.0.1.173', 'authenticate', NULL, NULL);
INSERT INTO tokens VALUES (59, 'X51ae0867498375e50000000063c1100000000000', 9, '2013-06-04 11:31:51.199263', '2013-06-04 11:31:53.486235', '10.0.1.173', 'spamfilter', NULL, NULL);
INSERT INTO tokens VALUES (72, 'X51ae0a058bd593200000000eee740400000000', 9, '2013-06-04 11:38:45.499649', '2013-06-04 11:38:45.502886', '10.0.1.138', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (60, 'X51ae0869289e887a000000007415108400000000', 9, '2013-06-04 11:31:53.486235', '2013-06-04 11:31:58.970157', '10.0.1.173', 'adqueues', NULL, NULL);
INSERT INTO tokens VALUES (61, 'X51ae086f78d6364f00000000667a869700000000', 9, '2013-06-04 11:31:58.970157', '2013-06-04 11:31:59.057829', '10.0.1.173', 'adqueues', NULL, NULL);
INSERT INTO tokens VALUES (73, 'X51ae0a06616f15eb0000000063f8f7e300000000', 9, '2013-06-04 11:38:45.502886', '2013-06-04 11:38:45.506048', '10.0.1.138', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (62, 'X51ae086fc4ad9ed000000006f58878a00000000', 9, '2013-06-04 11:31:59.057829', '2013-06-04 11:31:59.061095', '10.0.1.173', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (63, 'X51ae086f4bc49d0a00000000713d79d500000000', 9, '2013-06-04 11:31:59.061095', '2013-06-04 11:31:59.064322', '10.0.1.173', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (74, 'X51ae0a0619aefd42000000006ee2892300000000', 9, '2013-06-04 11:38:45.506048', '2013-06-04 11:38:45.517291', '10.0.1.138', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (64, 'X51ae086f5ae645020000000046d63ddd00000000', 9, '2013-06-04 11:31:59.064322', '2013-06-04 11:31:59.071271', '10.0.1.173', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (65, 'X51ae086f49527c3d000000001920f0600000000', 9, '2013-06-04 11:31:59.071271', '2013-06-04 11:31:59.074394', '10.0.1.173', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (75, 'X51ae0a0620cad6cb000000002a014e7600000000', 9, '2013-06-04 11:38:45.517291', '2013-06-04 11:38:45.523649', '10.0.1.138', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (66, 'X51ae086f2f49b0dc0000000050b9793400000000', 9, '2013-06-04 11:31:59.074394', '2013-06-04 11:31:59.077548', '10.0.1.173', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (76, 'X51ae0a06191ec5a0000000004a9226100000000', 9, '2013-06-04 11:38:45.523649', '2013-06-04 11:38:45.52837', '10.0.1.138', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (77, 'X51ae0a0671fa72710000000028027cfe00000000', 9, '2013-06-04 11:38:45.52837', '2013-06-04 11:38:58.763163', '10.0.1.138', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (78, 'X51ae0a134bd4338a000000004e39b59100000000', 9, '2013-06-04 11:38:58.763163', '2013-06-04 11:39:03.796954', '10.0.1.138', 'unlock', NULL, NULL);
INSERT INTO tokens VALUES (79, 'X51ae0a18396c7c07000000004ff8a71600000000', 9, '2013-06-04 11:39:03.796954', '2013-06-04 11:39:03.895299', '10.0.1.138', 'adqueues', NULL, NULL);
INSERT INTO tokens VALUES (80, 'X51ae0a184f87b3e000000001982665400000000', 9, '2013-06-04 11:39:03.895299', '2013-06-04 11:39:03.897758', '10.0.1.138', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (81, 'X51ae0a18229937df000000004eb719400000000', 9, '2013-06-04 11:39:03.897758', '2013-06-04 11:39:03.900029', '10.0.1.138', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (82, 'X51ae0a1829cc0c930000000053485fb000000000', 9, '2013-06-04 11:39:03.900029', '2013-06-04 11:39:03.90549', '10.0.1.138', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (83, 'X51ae0a182a4800cd000000003fb3ae1c00000000', 9, '2013-06-04 11:39:03.90549', '2013-06-04 11:39:03.90713', '10.0.1.138', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (84, 'X51ae0a188928cd000000006da8c12f00000000', 9, '2013-06-04 11:39:03.90713', '2013-06-04 11:39:03.909283', '10.0.1.138', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (85, 'X51ae0a1862c72be5000000006a16f2a800000000', 9, '2013-06-04 11:39:03.909283', '2013-06-04 11:39:06.288668', '10.0.1.138', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (86, 'X51ae0a1a7bff6a1e00000000605fa5b200000000', 9, '2013-06-04 11:39:06.288668', '2013-06-04 11:39:06.37191', '10.0.1.138', 'adqueues', NULL, NULL);
INSERT INTO tokens VALUES (87, 'X51ae0a1a5c497e49000000002c1c383e00000000', 9, '2013-06-04 11:39:06.37191', '2013-06-04 11:39:06.373726', '10.0.1.138', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (88, 'X51ae0a1a7bf52988000000007dea36f900000000', 9, '2013-06-04 11:39:06.373726', '2013-06-04 11:39:06.375688', '10.0.1.138', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (89, 'X51ae0a1a4aabe9730000000059cebfdd00000000', 9, '2013-06-04 11:39:06.375688', '2013-06-04 11:39:06.384646', '10.0.1.138', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (90, 'X51ae0a1a28cc05a9000000006d628caf00000000', 9, '2013-06-04 11:39:06.384646', '2013-06-04 11:39:06.386799', '10.0.1.138', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (91, 'X51ae0a1a1d8cb9000000000b1f97be00000000', 9, '2013-06-04 11:39:06.386799', '2013-06-04 11:39:06.389014', '10.0.1.138', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (92, 'X51ae0a1a74e75af500000000431f523400000000', 9, '2013-06-04 11:39:06.389014', '2013-06-04 11:39:08.993016', '10.0.1.138', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (93, 'X51ae0a1d4bdc1bd90000000071d69cfd00000000', 9, '2013-06-04 11:39:08.993016', '2013-06-04 11:39:09.086601', '10.0.1.138', 'adqueues', NULL, NULL);
INSERT INTO tokens VALUES (94, 'X51ae0a1d50f678f40000000054897e000000000', 9, '2013-06-04 11:39:09.086601', '2013-06-04 11:39:09.090683', '10.0.1.138', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (95, 'X51ae0a1d41cf44130000000055eef43300000000', 9, '2013-06-04 11:39:09.090683', '2013-06-04 11:39:09.094227', '10.0.1.138', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (96, 'X51ae0a1d1ecafe340000000064687bf200000000', 9, '2013-06-04 11:39:09.094227', '2013-06-04 11:39:09.099895', '10.0.1.138', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (97, 'X51ae0a1d5ada65c70000000048970ac700000000', 9, '2013-06-04 11:39:09.099895', '2013-06-04 11:39:09.101733', '10.0.1.138', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (98, 'X51ae0a1d37b0dba300000000522669400000000', 9, '2013-06-04 11:39:09.101733', '2013-06-04 11:39:09.103923', '10.0.1.138', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (99, 'X51ae0a1d84ab8e300000000383a047000000000', 9, '2013-06-04 11:39:09.103923', '2013-06-04 11:39:12.666885', '10.0.1.138', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (100, 'X51ae0a2172cb27c4000000006b11e4c800000000', 9, '2013-06-04 11:39:12.666885', '2013-06-04 11:39:12.737445', '10.0.1.138', 'adqueues', NULL, NULL);
INSERT INTO tokens VALUES (101, 'X51ae0a212250f719000000006eca91e200000000', 9, '2013-06-04 11:39:12.737445', '2013-06-04 11:39:12.740596', '10.0.1.138', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (102, 'X51ae0a214b718a7a000000007e9a756200000000', 9, '2013-06-04 11:39:12.740596', '2013-06-04 11:39:12.744117', '10.0.1.138', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (103, 'X51ae0a211ae6ca21000000004766b40200000000', 9, '2013-06-04 11:39:12.744117', '2013-06-04 11:39:12.752079', '10.0.1.138', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (104, 'X51ae0a217c84ac5c000000006592b39400000000', 9, '2013-06-04 11:39:12.752079', '2013-06-04 11:39:12.755003', '10.0.1.138', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (105, 'X51ae0a21213573df000000002550b20500000000', 9, '2013-06-04 11:39:12.755003', '2013-06-04 11:39:12.758383', '10.0.1.138', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (106, 'X51ae0a2152f5404300000000230e3f6f00000000', 9, '2013-06-04 11:39:12.758383', '2013-06-04 11:39:15.340963', '10.0.1.138', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (107, 'X51ae0a23307049c30000000047dc9b3800000000', 9, '2013-06-04 11:39:15.340963', '2013-06-04 11:39:15.451847', '10.0.1.173', 'authenticate', NULL, NULL);
INSERT INTO tokens VALUES (108, 'X51ae0a23662d91a3000000007c4c659c00000000', 9, '2013-06-04 11:39:15.451847', '2013-06-04 11:39:18.484459', '10.0.1.173', 'spamfilter', NULL, NULL);
INSERT INTO tokens VALUES (109, 'X51ae0a2639b338360000000037240a9800000000', 9, '2013-06-04 11:39:18.484459', '2013-06-04 11:39:20.488821', '10.0.1.173', 'adqueues', NULL, NULL);
INSERT INTO tokens VALUES (151, 'X51ae0a5b8dfae19000000005e6b645300000000', 9, '2013-06-04 11:40:11.253294', '2013-06-04 11:58:40.699504', '10.0.1.173', 'adqueues', NULL, NULL);
INSERT INTO tokens VALUES (110, 'X51ae0a28194fd7c000000007b827c4900000000', 9, '2013-06-04 11:39:20.488821', '2013-06-04 11:39:20.561485', '10.0.1.173', 'adqueues', NULL, NULL);
INSERT INTO tokens VALUES (265, 'X51ae13c71c34c4d700000000135246b400000000', 9, '2013-06-04 12:20:22.528252', '2013-06-04 12:20:22.765344', '10.0.1.138', 'adqueues', NULL, NULL);
INSERT INTO tokens VALUES (111, 'X51ae0a29d12fecb00000000205ffbb000000000', 9, '2013-06-04 11:39:20.561485', '2013-06-04 11:39:20.564514', '10.0.1.173', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (153, 'X51ae0eb429e4222c000000001f44c75c00000000', 9, '2013-06-04 11:58:43.503522', '2013-06-04 11:58:43.600794', '10.0.1.173', 'adqueues', NULL, NULL);
INSERT INTO tokens VALUES (112, 'X51ae0a295feaf83c0000000067ed649200000000', 9, '2013-06-04 11:39:20.564514', '2013-06-04 11:39:20.567947', '10.0.1.173', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (113, 'X51ae0a2968f7067700000000179bd3df00000000', 9, '2013-06-04 11:39:20.567947', '2013-06-04 11:39:20.574747', '10.0.1.173', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (154, 'X51ae0eb4ec815bc00000000238b240e00000000', 9, '2013-06-04 11:58:43.600794', '2013-06-04 11:58:43.602785', '10.0.1.173', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (114, 'X51ae0a296d0fcb26000000007141bf5a00000000', 9, '2013-06-04 11:39:20.574747', '2013-06-04 11:39:20.583098', '10.0.1.173', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (115, 'X51ae0a294fd5d84f000000005fdaf2ea00000000', 9, '2013-06-04 11:39:20.583098', '2013-06-04 11:39:20.588918', '10.0.1.173', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (155, 'X51ae0eb4653362b600000000218eb18800000000', 9, '2013-06-04 11:58:43.602785', '2013-06-04 11:58:43.604863', '10.0.1.173', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (116, 'X51ae0a295c53a422000000007226cf6800000000', 9, '2013-06-04 11:39:20.588918', '2013-06-04 11:39:25.180856', '10.0.1.173', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (117, 'X51ae0a2d4ea584cd0000000027c52e9d00000000', 9, '2013-06-04 11:39:25.180856', '2013-06-04 11:39:29.80252', '10.0.1.173', 'review', NULL, NULL);
INSERT INTO tokens VALUES (118, 'X51ae0a3224f68fb1000000006d1688a700000000', 9, '2013-06-04 11:39:29.80252', '2013-06-04 11:39:32.497887', '10.0.1.173', 'review', NULL, NULL);
INSERT INTO tokens VALUES (156, 'X51ae0eb47fb910b30000000020ed678000000000', 9, '2013-06-04 11:58:43.604863', '2013-06-04 11:58:43.610438', '10.0.1.173', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (119, 'X51ae0a3470c144cb00000000698c4eee00000000', 9, '2013-06-04 11:39:32.497887', '2013-06-04 11:39:32.561818', '10.0.1.173', 'adqueues', NULL, NULL);
INSERT INTO tokens VALUES (120, 'X51ae0a356f2be29f000000006d45f12700000000', 9, '2013-06-04 11:39:32.561818', '2013-06-04 11:39:32.564658', '10.0.1.173', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (157, 'X51ae0eb44811766a000000006e05b0d900000000', 9, '2013-06-04 11:58:43.610438', '2013-06-04 11:58:43.612183', '10.0.1.173', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (121, 'X51ae0a354f1f0282000000001061567f00000000', 9, '2013-06-04 11:39:32.564658', '2013-06-04 11:39:32.567851', '10.0.1.173', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (122, 'X51ae0a351296a32c00000000221442c500000000', 9, '2013-06-04 11:39:32.567851', '2013-06-04 11:39:32.574503', '10.0.1.173', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (158, 'X51ae0eb438d1e0ad00000000241feb6300000000', 9, '2013-06-04 11:58:43.612183', '2013-06-04 11:58:43.614183', '10.0.1.173', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (123, 'X51ae0a35336f95ee000000004306ecf000000000', 9, '2013-06-04 11:39:32.574503', '2013-06-04 11:39:32.577211', '10.0.1.173', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (124, 'X51ae0a3569f0ddfd00000000199d279200000000', 9, '2013-06-04 11:39:32.577211', '2013-06-04 11:39:32.580239', '10.0.1.173', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (159, 'X51ae0eb46f9bc044000000006ecfb0c100000000', 9, '2013-06-04 11:58:43.614183', '2013-06-04 11:58:48.05465', '10.0.1.173', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (125, 'X51ae0a353f53528c0000000023a4163300000000', 9, '2013-06-04 11:39:32.580239', '2013-06-04 11:39:37.378636', '10.0.1.173', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (126, 'X51ae0a3950c1322a0000000040e8500900000000', 9, '2013-06-04 11:39:37.378636', '2013-06-04 11:39:40.615191', '10.0.1.173', 'review', NULL, NULL);
INSERT INTO tokens VALUES (127, 'X51ae0a3d4f840c3b0000000014ed0bff00000000', 9, '2013-06-04 11:39:40.615191', '2013-06-04 11:39:42.667308', '10.0.1.173', 'review', NULL, NULL);
INSERT INTO tokens VALUES (160, 'X51ae0eb8653fe3b2000000001ab7d52800000000', 9, '2013-06-04 11:58:48.05465', '2013-06-04 11:58:51.892534', '10.0.1.173', 'review', NULL, NULL);
INSERT INTO tokens VALUES (128, 'X51ae0a3f1f26927d000000005dd430f500000000', 9, '2013-06-04 11:39:42.667308', '2013-06-04 11:39:42.697433', '10.0.1.173', 'adqueues', NULL, NULL);
INSERT INTO tokens VALUES (129, 'X51ae0a3f61484bb9000000007f118ab900000000', 9, '2013-06-04 11:39:42.697433', '2013-06-04 11:39:42.700383', '10.0.1.173', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (161, 'X51ae0ebc1994fc64000000004d01946200000000', 9, '2013-06-04 11:58:51.892534', '2013-06-04 11:58:53.860686', '10.0.1.173', 'review', NULL, NULL);
INSERT INTO tokens VALUES (130, 'X51ae0a3f45c19587000000004a3f523100000000', 9, '2013-06-04 11:39:42.700383', '2013-06-04 11:39:42.70361', '10.0.1.173', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (131, 'X51ae0a3f16ad5e980000000032d160ad00000000', 9, '2013-06-04 11:39:42.70361', '2013-06-04 11:39:47.163745', '10.0.1.173', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (162, 'X51ae0ebe7c91d5fd0000000011a22f7f00000000', 9, '2013-06-04 11:58:53.860686', '2013-06-04 11:58:53.995181', '10.0.1.173', 'adqueues', NULL, NULL);
INSERT INTO tokens VALUES (132, 'X51ae0a433b81118b00000000668336e700000000', 9, '2013-06-04 11:39:47.163745', '2013-06-04 11:39:50.799366', '10.0.1.173', 'review', NULL, NULL);
INSERT INTO tokens VALUES (133, 'X51ae0a475c409cf6000000006a5ea83600000000', 9, '2013-06-04 11:39:50.799366', '2013-06-04 11:39:50.890366', '10.0.1.173', 'adqueues', NULL, NULL);
INSERT INTO tokens VALUES (163, 'X51ae0ebe3ac61c0c0000000022054dbb00000000', 9, '2013-06-04 11:58:53.995181', '2013-06-04 11:58:53.998118', '10.0.1.173', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (134, 'X51ae0a4742ddaf1400000000b0d59f300000000', 9, '2013-06-04 11:39:50.890366', '2013-06-04 11:39:50.893458', '10.0.1.173', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (135, 'X51ae0a47695cb7dc0000000072ca864d00000000', 9, '2013-06-04 11:39:50.893458', '2013-06-04 11:39:50.896855', '10.0.1.173', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (164, 'X51ae0ebe7d66dbad0000000015acd56900000000', 9, '2013-06-04 11:58:53.998118', '2013-06-04 11:58:54.00134', '10.0.1.173', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (136, 'X51ae0a472a94cfa3000000005a7a4f4400000000', 9, '2013-06-04 11:39:50.896855', '2013-06-04 11:39:50.908218', '10.0.1.173', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (137, 'X51ae0a472cacef1f000000002217e57600000000', 9, '2013-06-04 11:39:50.908218', '2013-06-04 11:39:50.911157', '10.0.1.173', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (165, 'X51ae0ebe6eb6147c00000000492f6c0300000000', 9, '2013-06-04 11:58:54.00134', '2013-06-04 11:58:54.008285', '10.0.1.173', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (138, 'X51ae0a47250a6bed000000003c2a11e800000000', 9, '2013-06-04 11:39:50.911157', '2013-06-04 11:39:50.914509', '10.0.1.173', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (139, 'X51ae0a47503737e2000000002dc7c51f00000000', 9, '2013-06-04 11:39:50.914509', '2013-06-04 11:39:54.979242', '10.0.1.173', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (166, 'X51ae0ebe109e6b0900000000347526a700000000', 9, '2013-06-04 11:58:54.008285', '2013-06-04 11:58:54.011153', '10.0.1.173', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (140, 'X51ae0a4b4b1885ec0000000031a64dcd00000000', 9, '2013-06-04 11:39:54.979242', '2013-06-04 11:39:58.624619', '10.0.1.173', 'review', NULL, NULL);
INSERT INTO tokens VALUES (141, 'X51ae0a4f12ac53980000000017d4b5ae00000000', 9, '2013-06-04 11:39:58.624619', '2013-06-04 11:40:00.819185', '10.0.1.173', 'review', NULL, NULL);
INSERT INTO tokens VALUES (167, 'X51ae0ebe3269928000000007a13bf6900000000', 9, '2013-06-04 11:58:54.011153', '2013-06-04 11:58:54.014418', '10.0.1.173', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (142, 'X51ae0a5111c0bd030000000064c7832f00000000', 9, '2013-06-04 11:40:00.819185', '2013-06-04 11:40:00.915223', '10.0.1.173', 'adqueues', NULL, NULL);
INSERT INTO tokens VALUES (143, 'X51ae0a512088d6f000000000328b93ce00000000', 9, '2013-06-04 11:40:00.915223', '2013-06-04 11:40:00.918536', '10.0.1.173', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (168, 'X51ae0ebe78eb3a0900000000131b160900000000', 9, '2013-06-04 11:58:54.014418', '2013-06-04 11:59:01.641729', '10.0.1.173', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (144, 'X51ae0a51ec8d1a50000000039a79c9000000000', 9, '2013-06-04 11:40:00.918536', '2013-06-04 11:40:00.92205', '10.0.1.173', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (145, 'X51ae0a513734b63000000000c3441600000000', 9, '2013-06-04 11:40:00.92205', '2013-06-04 11:40:00.929556', '10.0.1.173', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (169, 'X51ae0ec66cce382e0000000022cf5c3500000000', 9, '2013-06-04 11:59:01.641729', '2013-06-04 11:59:05.847257', '10.0.1.173', 'review', NULL, NULL);
INSERT INTO tokens VALUES (146, 'X51ae0a5161aa198f00000000308e9ba00000000', 9, '2013-06-04 11:40:00.929556', '2013-06-04 11:40:00.932447', '10.0.1.173', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (147, 'X51ae0a514efcf9a7000000006a0a94000000000', 9, '2013-06-04 11:40:00.932447', '2013-06-04 11:40:00.93568', '10.0.1.173', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (170, 'X51ae0eca6d7545200000000334b5bb500000000', 9, '2013-06-04 11:59:05.847257', '2013-06-04 11:59:07.850807', '10.0.1.173', 'review', NULL, NULL);
INSERT INTO tokens VALUES (148, 'X51ae0a51701f7261000000001e8105e300000000', 9, '2013-06-04 11:40:00.93568', '2013-06-04 11:40:05.442026', '10.0.1.173', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (149, 'X51ae0a551b8db53f000000004c600f5800000000', 9, '2013-06-04 11:40:05.442026', '2013-06-04 11:40:09.293972', '10.0.1.173', 'review', NULL, NULL);
INSERT INTO tokens VALUES (150, 'X51ae0a5958aa0650000000006151d86500000000', 9, '2013-06-04 11:40:09.293972', '2013-06-04 11:40:11.253294', '10.0.1.173', 'review', NULL, NULL);
INSERT INTO tokens VALUES (171, 'X51ae0ecc325fdd65000000007b964dea00000000', 9, '2013-06-04 11:59:07.850807', '2013-06-04 11:59:07.924782', '10.0.1.173', 'adqueues', NULL, NULL);
INSERT INTO tokens VALUES (172, 'X51ae0ecc465a8043000000001793401c00000000', 9, '2013-06-04 11:59:07.924782', '2013-06-04 11:59:07.927534', '10.0.1.173', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (173, 'X51ae0ecc1d24ff7300000000461390f600000000', 9, '2013-06-04 11:59:07.927534', '2013-06-04 11:59:07.929568', '10.0.1.173', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (174, 'X51ae0ecc3880a79c00000000653675dd00000000', 9, '2013-06-04 11:59:07.929568', '2013-06-04 11:59:07.934764', '10.0.1.173', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (175, 'X51ae0ecc341941cf000000007152884a00000000', 9, '2013-06-04 11:59:07.934764', '2013-06-04 11:59:07.93682', '10.0.1.173', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (176, 'X51ae0ecc95661400000000023b5021400000000', 9, '2013-06-04 11:59:07.93682', '2013-06-04 11:59:07.9388', '10.0.1.173', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (177, 'X51ae0ecc6022390b000000006e9644f200000000', 9, '2013-06-04 11:59:07.9388', '2013-06-04 11:59:12.340714', '10.0.1.173', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (178, 'X51ae0ed03e6cd73c000000005cb40f0800000000', 9, '2013-06-04 11:59:12.340714', '2013-06-04 11:59:17.736097', '10.0.1.173', 'review', NULL, NULL);
INSERT INTO tokens VALUES (179, 'X51ae0ed6387471000000007932f34800000000', 9, '2013-06-04 11:59:17.736097', '2013-06-04 11:59:19.399058', '10.0.1.173', 'review', NULL, NULL);
INSERT INTO tokens VALUES (180, 'X51ae0ed77eb95cc3000000007d9f501f00000000', 9, '2013-06-04 11:59:19.399058', '2013-06-04 11:59:19.494487', '10.0.1.173', 'adqueues', NULL, NULL);
INSERT INTO tokens VALUES (181, 'X51ae0ed7edfc8b1000000006d6f714000000000', 9, '2013-06-04 11:59:19.494487', '2013-06-04 11:59:19.496679', '10.0.1.173', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (182, 'X51ae0ed746cebc22000000001f7e33bb00000000', 9, '2013-06-04 11:59:19.496679', '2013-06-04 11:59:19.498737', '10.0.1.173', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (183, 'X51ae0ed721e497e70000000049f5554a00000000', 9, '2013-06-04 11:59:19.498737', '2013-06-04 11:59:19.504359', '10.0.1.173', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (184, 'X51ae0ed81991f324000000001acfd1f000000000', 9, '2013-06-04 11:59:19.504359', '2013-06-04 11:59:19.506206', '10.0.1.173', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (185, 'X51ae0ed85d106b54000000006602b5200000000', 9, '2013-06-04 11:59:19.506206', '2013-06-04 11:59:19.508328', '10.0.1.173', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (186, 'X51ae0ed83d9f2e2600000000f7048b900000000', 9, '2013-06-04 11:59:19.508328', '2013-06-04 11:59:23.715878', '10.0.1.173', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (187, 'X51ae0edc1f6793d000000003f9ae6900000000', 9, '2013-06-04 11:59:23.715878', '2013-06-04 11:59:26.340945', '10.0.1.173', 'review', NULL, NULL);
INSERT INTO tokens VALUES (188, 'X51ae0ede270388d5000000001f1b78b000000000', 9, '2013-06-04 11:59:26.340945', '2013-06-04 11:59:27.535798', '10.0.1.173', 'review', NULL, NULL);
INSERT INTO tokens VALUES (189, 'X51ae0ee04a0d3f60000000005f84307200000000', 9, '2013-06-04 11:59:27.535798', '2013-06-04 11:59:27.635583', '10.0.1.173', 'adqueues', NULL, NULL);
INSERT INTO tokens VALUES (190, 'X51ae0ee0451ee8d000000007e26812f00000000', 9, '2013-06-04 11:59:27.635583', '2013-06-04 11:59:27.637661', '10.0.1.173', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (191, 'X51ae0ee050d6b8bc00000000da84fcd00000000', 9, '2013-06-04 11:59:27.637661', '2013-06-04 11:59:27.639726', '10.0.1.173', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (192, 'X51ae0ee021db83430000000030f8f1c700000000', 9, '2013-06-04 11:59:27.639726', '2013-06-04 11:59:27.645813', '10.0.1.173', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (193, 'X51ae0ee07c3e94c00000000060485a7f00000000', 9, '2013-06-04 11:59:27.645813', '2013-06-04 11:59:27.648059', '10.0.1.173', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (194, 'X51ae0ee0dad00cf000000007c77093100000000', 9, '2013-06-04 11:59:27.648059', '2013-06-04 11:59:27.650124', '10.0.1.173', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (195, 'X51ae0ee0597b4dc700000000c665d9300000000', 9, '2013-06-04 11:59:27.650124', '2013-06-04 11:59:31.157742', '10.0.1.173', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (196, 'X51ae0ee37a16595000000000685b167900000000', 9, '2013-06-04 11:59:31.157742', '2013-06-04 11:59:35.452786', '10.0.1.173', 'review', NULL, NULL);
INSERT INTO tokens VALUES (197, 'X51ae0ee776c713050000000043bcb0f200000000', 9, '2013-06-04 11:59:35.452786', '2013-06-04 11:59:39.574969', '10.0.1.173', 'review', NULL, NULL);
INSERT INTO tokens VALUES (198, 'X51ae0eec79d5ced30000000040e5157200000000', 9, '2013-06-04 11:59:39.574969', '2013-06-04 11:59:39.674807', '10.0.1.173', 'adqueues', NULL, NULL);
INSERT INTO tokens VALUES (199, 'X51ae0eec7d94a34000000001bba66ba00000000', 9, '2013-06-04 11:59:39.674807', '2013-06-04 11:59:39.677175', '10.0.1.173', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (200, 'X51ae0eecada6abd00000000216b3d5800000000', 9, '2013-06-04 11:59:39.677175', '2013-06-04 11:59:39.679798', '10.0.1.173', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (201, 'X51ae0eec368a38aa0000000067ead61100000000', 9, '2013-06-04 11:59:39.679798', '2013-06-04 11:59:39.685562', '10.0.1.173', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (202, 'X51ae0eec27cb68aa00000000742966d000000000', 9, '2013-06-04 11:59:39.685562', '2013-06-04 11:59:39.688109', '10.0.1.173', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (203, 'X51ae0eec775b1eca0000000029c1e1e700000000', 9, '2013-06-04 11:59:39.688109', '2013-06-04 11:59:39.690448', '10.0.1.173', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (204, 'X51ae0eec7823153a000000001e5ea7a000000000', 9, '2013-06-04 11:59:39.690448', '2013-06-04 11:59:43.223564', '10.0.1.173', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (205, 'X51ae0eef48dd5a97000000004230549a00000000', 9, '2013-06-04 11:59:43.223564', '2013-06-04 11:59:46.009972', '10.0.1.173', 'review', NULL, NULL);
INSERT INTO tokens VALUES (206, 'X51ae0ef25dd6a254000000003f9aaa9a00000000', 9, '2013-06-04 11:59:46.009972', '2013-06-04 11:59:47.260305', '10.0.1.173', 'review', NULL, NULL);
INSERT INTO tokens VALUES (207, 'X51ae0ef37de2d812000000004d2f492400000000', 9, '2013-06-04 11:59:47.260305', '2013-06-04 11:59:47.310868', '10.0.1.173', 'adqueues', NULL, NULL);
INSERT INTO tokens VALUES (208, 'X51ae0ef34056d5c9000000004eb990ce00000000', 9, '2013-06-04 11:59:47.310868', '2013-06-04 11:59:47.312933', '10.0.1.173', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (209, 'X51ae0ef35ad798f2000000006232590d00000000', 9, '2013-06-04 11:59:47.312933', '2013-06-04 11:59:47.315078', '10.0.1.173', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (210, 'X51ae0ef37fb282950000000057162db200000000', 9, '2013-06-04 11:59:47.315078', '2013-06-04 11:59:47.320299', '10.0.1.173', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (211, 'X51ae0ef3427ab38c00000000d5f836400000000', 9, '2013-06-04 11:59:47.320299', '2013-06-04 11:59:47.322009', '10.0.1.173', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (212, 'X51ae0ef3538d36e3000000001bf6015400000000', 9, '2013-06-04 11:59:47.322009', '2013-06-04 11:59:47.324', '10.0.1.173', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (213, 'X51ae0ef319c5e0f7000000004da3903400000000', 9, '2013-06-04 11:59:47.324', '2013-06-04 11:59:50.030201', '10.0.1.173', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (214, 'X51ae0ef645117cd00000000139bafca00000000', 9, '2013-06-04 11:59:50.030201', '2013-06-04 11:59:53.360519', '10.0.1.173', 'review', NULL, NULL);
INSERT INTO tokens VALUES (215, 'X51ae0ef93fea748000000000517e44e00000000', 9, '2013-06-04 11:59:53.360519', '2013-06-04 11:59:55.111906', '10.0.1.173', 'review', NULL, NULL);
INSERT INTO tokens VALUES (216, 'X51ae0efbe88a5a600000000c2a620100000000', 9, '2013-06-04 11:59:55.111906', '2013-06-04 11:59:55.271486', '10.0.1.173', 'adqueues', NULL, NULL);
INSERT INTO tokens VALUES (217, 'X51ae0efb2f561684000000001963106300000000', 9, '2013-06-04 11:59:55.271486', '2013-06-04 11:59:55.273611', '10.0.1.173', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (218, 'X51ae0efb2d959f590000000065e04f2f00000000', 9, '2013-06-04 11:59:55.273611', '2013-06-04 11:59:55.275809', '10.0.1.173', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (219, 'X51ae0efb14de674000000005561080300000000', 9, '2013-06-04 11:59:55.275809', '2013-06-04 11:59:55.281221', '10.0.1.173', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (220, 'X51ae0efb5a09b5ff0000000078a9053f00000000', 9, '2013-06-04 11:59:55.281221', '2013-06-04 11:59:55.283415', '10.0.1.173', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (221, 'X51ae0efb7f22e9eb00000000522ccb3900000000', 9, '2013-06-04 11:59:55.283415', '2013-06-04 11:59:55.285511', '10.0.1.173', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (222, 'X51ae0efb1707acdf000000004800448200000000', 9, '2013-06-04 11:59:55.285511', '2013-06-04 11:59:58.803286', '10.0.1.173', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (223, 'X51ae0eff145d1fd30000000014ea84f100000000', 9, '2013-06-04 11:59:58.803286', '2013-06-04 12:00:08.523352', '10.0.1.173', 'review', NULL, NULL);
INSERT INTO tokens VALUES (224, 'X51ae0f09106bc1f5000000003e53ab0000000000', 9, '2013-06-04 12:00:08.523352', '2013-06-04 12:00:13.536131', '10.0.1.173', 'review', NULL, NULL);
INSERT INTO tokens VALUES (225, 'X51ae0f0e152f8da70000000054b3f59d00000000', 9, '2013-06-04 12:00:13.536131', '2013-06-04 12:00:13.61931', '10.0.1.173', 'adqueues', NULL, NULL);
INSERT INTO tokens VALUES (226, 'X51ae0f0e63a415bf000000007007269900000000', 9, '2013-06-04 12:00:13.61931', '2013-06-04 12:00:13.622163', '10.0.1.173', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (227, 'X51ae0f0e36e64eaa000000006356985400000000', 9, '2013-06-04 12:00:13.622163', '2013-06-04 12:00:13.625448', '10.0.1.173', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (228, 'X51ae0f0e471d544b000000007961023600000000', 9, '2013-06-04 12:00:13.625448', '2013-06-04 12:00:17.584641', '10.0.1.173', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (229, 'X51ae0f1250e133b50000000060aa2a3000000000', 9, '2013-06-04 12:00:17.584641', '2013-06-04 12:00:19.352147', '10.0.1.173', 'review', NULL, NULL);
INSERT INTO tokens VALUES (264, 'X51ae11b927467123000000002905446c00000000', 9, '2013-06-04 12:11:37.20551', '2013-06-04 12:20:22.528252', '10.0.1.138', 'adqueues', NULL, NULL);
INSERT INTO tokens VALUES (230, 'X51ae0f1370b61bb8000000001aaa8b2e00000000', 9, '2013-06-04 12:00:19.352147', '2013-06-04 12:00:19.380167', '10.0.1.173', 'adqueues', NULL, NULL);
INSERT INTO tokens VALUES (266, 'X51ae13c758425a28000000006fe7d08700000000', 9, '2013-06-04 12:20:22.765344', '2013-06-04 12:20:22.768814', '10.0.1.138', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (231, 'X51ae0f131557038a00000000a7bfcb000000000', 9, '2013-06-04 12:00:19.380167', '2013-06-04 12:00:19.382103', '10.0.1.173', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (232, 'X51ae0f13684e1b620000000019a81b5700000000', 9, '2013-06-04 12:00:19.382103', '2013-06-04 12:00:19.384756', '10.0.1.173', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (267, 'X51ae13c744fdc0ec000000007664d50a00000000', 9, '2013-06-04 12:20:22.768814', '2013-06-04 12:20:22.772565', '10.0.1.138', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (233, 'X51ae0f131e17ac7a0000000076d6c10900000000', 9, '2013-06-04 12:00:19.384756', '2013-06-04 12:00:33.27451', '10.0.1.173', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (234, 'X51ae0f2125d27d58000000004d6dc2ff00000000', 9, '2013-06-04 12:00:33.27451', '2013-06-04 12:00:35.046484', '10.0.1.173', 'review', NULL, NULL);
INSERT INTO tokens VALUES (268, 'X51ae13c73ed478c6000000002f6fe0200000000', 9, '2013-06-04 12:20:22.772565', '2013-06-04 12:20:22.780116', '10.0.1.138', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (235, 'X51ae0f231039d16c0000000053681cb100000000', 9, '2013-06-04 12:00:35.046484', '2013-06-04 12:00:35.049496', '10.0.1.173', 'adqueues', NULL, NULL);
INSERT INTO tokens VALUES (236, 'X51ae0f23334e122e000000001187b7e100000000', 9, '2013-06-04 12:00:35.049496', '2013-06-04 12:11:07.942004', '10.0.1.173', 'adqueues', NULL, NULL);
INSERT INTO tokens VALUES (269, 'X51ae13c75a7321bf00000000782a35dd00000000', 9, '2013-06-04 12:20:22.780116', '2013-06-04 12:20:22.782846', '10.0.1.138', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (237, 'X51ae119c33b45174000000007b4c409700000000', 9, '2013-06-04 12:11:07.942004', '2013-06-04 12:11:08.049716', '10.0.1.138', 'authenticate', NULL, NULL);
INSERT INTO tokens VALUES (238, 'X51ae119cd7ab600000000006a96f8ef00000000', 9, '2013-06-04 12:11:08.049716', '2013-06-04 12:11:10.311698', '10.0.1.138', 'spamfilter', NULL, NULL);
INSERT INTO tokens VALUES (270, 'X51ae13c7cc347aa0000000041b09bfa00000000', 9, '2013-06-04 12:20:22.782846', '2013-06-04 12:20:22.786286', '10.0.1.138', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (239, 'X51ae119e4570d7200000000105dfc9200000000', 9, '2013-06-04 12:11:10.311698', '2013-06-04 12:11:11.318702', '10.0.1.138', 'adqueues', NULL, NULL);
INSERT INTO tokens VALUES (240, 'X51ae119f685837f0000000006ad8045f00000000', 9, '2013-06-04 12:11:11.318702', '2013-06-04 12:11:11.400159', '10.0.1.138', 'adqueues', NULL, NULL);
INSERT INTO tokens VALUES (271, 'X51ae13c7661c4540000000084ec8cf00000000', 9, '2013-06-04 12:20:22.786286', '2013-06-04 12:20:30.970438', '10.0.1.138', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (241, 'X51ae119f6e549e99000000005a2f5ae000000000', 9, '2013-06-04 12:11:11.400159', '2013-06-04 12:11:11.403161', '10.0.1.138', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (242, 'X51ae119f678dcebc000000003e80514d00000000', 9, '2013-06-04 12:11:11.403161', '2013-06-04 12:11:11.406485', '10.0.1.138', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (272, 'X51ae13cf5de967ec000000002a0142bf00000000', 9, '2013-06-04 12:20:30.970438', '2013-06-04 12:20:34.117821', '10.0.1.138', 'review', NULL, NULL);
INSERT INTO tokens VALUES (243, 'X51ae119f20d445530000000050c2962300000000', 9, '2013-06-04 12:11:11.406485', '2013-06-04 12:11:11.413257', '10.0.1.138', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (244, 'X51ae119f35ffceb00000000032f230f200000000', 9, '2013-06-04 12:11:11.413257', '2013-06-04 12:11:11.416062', '10.0.1.138', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (273, 'X51ae13d2430a76250000000013e48c7400000000', 9, '2013-06-04 12:20:34.117821', '2013-06-04 12:20:35.124217', '10.0.1.138', 'review', NULL, NULL);
INSERT INTO tokens VALUES (245, 'X51ae119f7df3f345000000002e3a280600000000', 9, '2013-06-04 12:11:11.416062', '2013-06-04 12:11:11.419187', '10.0.1.138', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (246, 'X51ae119f3e70a958000000003585a25d00000000', 9, '2013-06-04 12:11:11.419187', '2013-06-04 12:11:18.707338', '10.0.1.138', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (274, 'X51ae13d3dfa1ad900000000189cb66b00000000', 9, '2013-06-04 12:20:35.124217', '2013-06-04 12:20:35.308529', '10.0.1.138', 'adqueues', NULL, NULL);
INSERT INTO tokens VALUES (247, 'X51ae11a7e2dd60800000000714defa300000000', 9, '2013-06-04 12:11:18.707338', '2013-06-04 12:11:21.868441', '10.0.1.138', 'review', NULL, NULL);
INSERT INTO tokens VALUES (248, 'X51ae11aa5614699a0000000028788f5100000000', 9, '2013-06-04 12:11:21.868441', '2013-06-04 12:11:23.384808', '10.0.1.138', 'review', NULL, NULL);
INSERT INTO tokens VALUES (275, 'X51ae13d32b16566300000000a66361e00000000', 9, '2013-06-04 12:20:35.308529', '2013-06-04 12:20:35.311789', '10.0.1.138', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (249, 'X51ae11ab44c742fc000000001d8957ef00000000', 9, '2013-06-04 12:11:23.384808', '2013-06-04 12:11:23.463651', '10.0.1.138', 'adqueues', NULL, NULL);
INSERT INTO tokens VALUES (250, 'X51ae11ab7a26320000000074ec2d8d00000000', 9, '2013-06-04 12:11:23.463651', '2013-06-04 12:11:23.466671', '10.0.1.138', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (276, 'X51ae13d3e2869de000000005683b7ac00000000', 9, '2013-06-04 12:20:35.311789', '2013-06-04 12:20:35.315167', '10.0.1.138', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (251, 'X51ae11ab4ed5b583000000007c3bd49900000000', 9, '2013-06-04 12:11:23.466671', '2013-06-04 12:11:23.469897', '10.0.1.138', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (252, 'X51ae11ab3c0bd022000000006909ca5600000000', 9, '2013-06-04 12:11:23.469897', '2013-06-04 12:11:23.476913', '10.0.1.138', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (277, 'X51ae13d37a8037f80000000030cacd8400000000', 9, '2013-06-04 12:20:35.315167', '2013-06-04 12:20:35.322209', '10.0.1.138', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (253, 'X51ae11ab79c1b9c8000000006fc0219700000000', 9, '2013-06-04 12:11:23.476913', '2013-06-04 12:11:23.479758', '10.0.1.138', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (254, 'X51ae11ab64560aee0000000073c6fc900000000', 9, '2013-06-04 12:11:23.479758', '2013-06-04 12:11:23.483113', '10.0.1.138', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (278, 'X51ae13d31208a4e300000000d13a15700000000', 9, '2013-06-04 12:20:35.322209', '2013-06-04 12:20:35.325092', '10.0.1.138', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (255, 'X51ae11ab5a571a860000000068ad186000000000', 9, '2013-06-04 12:11:23.483113', '2013-06-04 12:11:26.702661', '10.0.1.138', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (256, 'X51ae11af179a6c5b0000000042af527600000000', 9, '2013-06-04 12:11:26.702661', '2013-06-04 12:11:30.176774', '10.0.1.138', 'review', NULL, NULL);
INSERT INTO tokens VALUES (279, 'X51ae13d32781e7e800000000f9e218500000000', 9, '2013-06-04 12:20:35.325092', '2013-06-04 12:20:35.328385', '10.0.1.138', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (257, 'X51ae11b231c2122a000000004052aa8900000000', 9, '2013-06-04 12:11:30.176774', '2013-06-04 12:11:32.015142', '10.0.1.138', 'review', NULL, NULL);
INSERT INTO tokens VALUES (258, 'X51ae11b453851cbf000000005ef0af500000000', 9, '2013-06-04 12:11:32.015142', '2013-06-04 12:11:32.056328', '10.0.1.138', 'adqueues', NULL, NULL);
INSERT INTO tokens VALUES (280, 'X51ae13d32deb64dd000000005a38c64900000000', 9, '2013-06-04 12:20:35.328385', '2013-06-04 12:20:39.106319', '10.0.1.138', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (259, 'X51ae11b41cdead56000000003b12eb7b00000000', 9, '2013-06-04 12:11:32.056328', '2013-06-04 12:11:32.059359', '10.0.1.138', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (260, 'X51ae11b4446f5c42000000003db2f2aa00000000', 9, '2013-06-04 12:11:32.059359', '2013-06-04 12:11:32.062612', '10.0.1.138', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (281, 'X51ae13d7563ac700000000004a2029b400000000', 9, '2013-06-04 12:20:39.106319', '2013-06-04 12:20:42.039639', '10.0.1.138', 'review', NULL, NULL);
INSERT INTO tokens VALUES (261, 'X51ae11b4bd5819e000000007a6f2af300000000', 9, '2013-06-04 12:11:32.062612', '2013-06-04 12:11:34.825363', '10.0.1.138', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (262, 'X51ae11b770a5239c000000009c974e300000000', 9, '2013-06-04 12:11:34.825363', '2013-06-04 12:11:37.200643', '10.0.1.138', 'review', NULL, NULL);
INSERT INTO tokens VALUES (282, 'X51ae13da636bc424000000004d620f5300000000', 9, '2013-06-04 12:20:42.039639', '2013-06-04 12:20:43.408985', '10.0.1.138', 'review', NULL, NULL);
INSERT INTO tokens VALUES (263, 'X51ae11b916425ccf000000001485360200000000', 9, '2013-06-04 12:11:37.200643', '2013-06-04 12:11:37.20551', '10.0.1.138', 'adqueues', NULL, NULL);
INSERT INTO tokens VALUES (283, 'X51ae13db6d8b0cfd000000002e7d212800000000', 9, '2013-06-04 12:20:43.408985', '2013-06-04 12:20:43.663693', '10.0.1.138', 'adqueues', NULL, NULL);
INSERT INTO tokens VALUES (284, 'X51ae13dc3a07fa3c000000003288cde900000000', 9, '2013-06-04 12:20:43.663693', '2013-06-04 12:20:43.666739', '10.0.1.138', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (285, 'X51ae13dc24e1f6330000000078dc730200000000', 9, '2013-06-04 12:20:43.666739', '2013-06-04 12:20:43.670151', '10.0.1.138', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (286, 'X51ae13dc357fcbeb000000007f5517f200000000', 9, '2013-06-04 12:20:43.670151', '2013-06-04 12:20:43.677649', '10.0.1.138', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (287, 'X51ae13dc7106a8df000000004243139500000000', 9, '2013-06-04 12:20:43.677649', '2013-06-04 12:20:43.680552', '10.0.1.138', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (288, 'X51ae13dc4105b3ed0000000077686d3300000000', 9, '2013-06-04 12:20:43.680552', '2013-06-04 12:20:43.683805', '10.0.1.138', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (289, 'X51ae13dc4a91dc65000000001eef1bd900000000', 9, '2013-06-04 12:20:43.683805', '2013-06-04 12:20:46.75402', '10.0.1.138', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (290, 'X51ae13df2169aff300000000588bf73e00000000', 9, '2013-06-04 12:20:46.75402', '2013-06-04 12:20:48.999501', '10.0.1.138', 'review', NULL, NULL);
INSERT INTO tokens VALUES (291, 'X51ae13e13be4ca68000000003fdd39c100000000', 9, '2013-06-04 12:20:48.999501', '2013-06-04 12:20:50.691958', '10.0.1.138', 'review', NULL, NULL);
INSERT INTO tokens VALUES (297, 'X51ae13e954e08189000000007795bab00000000', 9, '2013-06-04 12:20:57.143318', '2013-06-04 12:20:57.14814', '10.0.1.138', 'adqueues', NULL, NULL);
INSERT INTO tokens VALUES (298, 'X51ae13e97b48a15e000000007b35e53900000000', 9, '2013-06-04 12:20:57.14814', '2013-06-04 12:29:16.596472', '10.0.1.138', 'adqueues', NULL, NULL);
INSERT INTO tokens VALUES (292, 'X51ae13e3378bd245000000004c80065600000000', 9, '2013-06-04 12:20:50.691958', '2013-06-04 12:20:50.900438', '10.0.1.138', 'adqueues', NULL, NULL);
INSERT INTO tokens VALUES (293, 'X51ae13e362f22d5c0000000045b43c2300000000', 9, '2013-06-04 12:20:50.900438', '2013-06-04 12:20:50.903969', '10.0.1.138', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (294, 'X51ae13e32303be02000000005d72655400000000', 9, '2013-06-04 12:20:50.903969', '2013-06-04 12:20:50.907532', '10.0.1.138', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (295, 'X51ae13e3767f09a700000000350c62e500000000', 9, '2013-06-04 12:20:50.907532', '2013-06-04 12:20:54.278288', '10.0.1.138', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (296, 'X51ae13e66a8606ab000000001e00f18f00000000', 9, '2013-06-04 12:20:54.278288', '2013-06-04 12:20:57.143318', '10.0.1.138', 'review', NULL, NULL);
INSERT INTO tokens VALUES (299, 'X51ae15dd15a36a3e0000000047401a0700000000', 9, '2013-06-04 12:29:16.596472', '2013-06-04 12:29:18.674013', '10.0.1.138', 'spamfilter', NULL, NULL);
INSERT INTO tokens VALUES (300, 'X51ae15dfb8928620000000035aff67b00000000', 9, '2013-06-04 12:29:18.674013', '2013-06-04 12:32:05.307259', '10.0.1.138', 'adqueues', NULL, NULL);
INSERT INTO tokens VALUES (301, 'X51ae1685590e693200000000ea288e00000000', 9, '2013-06-04 12:32:05.307259', '2013-06-04 12:32:07.485075', '10.0.1.138', 'adqueues', NULL, NULL);
INSERT INTO tokens VALUES (302, 'X51ae1687333bc7720000000059f80f5500000000', 9, '2013-06-04 12:32:07.485075', '2013-06-04 12:32:07.604732', '10.0.1.138', 'adqueues', NULL, NULL);
INSERT INTO tokens VALUES (303, 'X51ae16883be1e6c50000000073e1533700000000', 9, '2013-06-04 12:32:07.604732', '2013-06-04 12:32:07.606659', '10.0.1.138', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (304, 'X51ae16882f04b3ef0000000074b9fe2f00000000', 9, '2013-06-04 12:32:07.606659', '2013-06-04 12:32:07.608638', '10.0.1.138', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (305, 'X51ae16882735137700000000759a7a7900000000', 9, '2013-06-04 12:32:07.608638', '2013-06-04 12:32:07.6147', '10.0.1.138', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (306, 'X51ae1688155723c40000000065dde31800000000', 9, '2013-06-04 12:32:07.6147', '2013-06-04 12:32:07.616552', '10.0.1.138', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (307, 'X51ae16882ac665c8000000006b67258800000000', 9, '2013-06-04 12:32:07.616552', '2013-06-04 12:32:07.618638', '10.0.1.138', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (308, 'X51ae16884c7999ec00000000688a955200000000', 9, '2013-06-04 12:32:07.618638', '2013-06-04 12:32:14.391592', '10.0.1.138', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (309, 'X51ae168e69d3a296000000003a89e9c400000000', 9, '2013-06-04 12:32:14.391592', '2013-06-04 12:32:17.835022', '10.0.1.138', 'review', NULL, NULL);
INSERT INTO tokens VALUES (310, 'X51ae1692350c716b0000000058a9b02100000000', 9, '2013-06-04 12:32:17.835022', '2013-06-04 12:32:19.068958', '10.0.1.138', 'review', NULL, NULL);
INSERT INTO tokens VALUES (311, 'X51ae1693755b9cb4000000002a80eb3200000000', 9, '2013-06-04 12:32:19.068958', '2013-06-04 12:32:19.183821', '10.0.1.138', 'adqueues', NULL, NULL);
INSERT INTO tokens VALUES (312, 'X51ae16937da28004000000007d09e9bb00000000', 9, '2013-06-04 12:32:19.183821', '2013-06-04 12:32:19.186471', '10.0.1.138', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (313, 'X51ae1693ba6adea00000000141a2a5300000000', 9, '2013-06-04 12:32:19.186471', '2013-06-04 12:32:19.188688', '10.0.1.138', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (314, 'X51ae16935c4812ac0000000027436ec500000000', 9, '2013-06-04 12:32:19.188688', '2013-06-04 12:32:19.194573', '10.0.1.138', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (315, 'X51ae16932d8ab3940000000071eb7cea00000000', 9, '2013-06-04 12:32:19.194573', '2013-06-04 12:32:19.196772', '10.0.1.138', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (316, 'X51ae16936e8388cc000000003913dbf600000000', 9, '2013-06-04 12:32:19.196772', '2013-06-04 12:32:19.199477', '10.0.1.138', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (317, 'X51ae1693279b7365000000004791f1fe00000000', 9, '2013-06-04 12:32:19.199477', '2013-06-04 12:32:22.164167', '10.0.1.138', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (318, 'X51ae169639fe0485000000005ad73ad800000000', 9, '2013-06-04 12:32:22.164167', '2013-06-04 12:32:24.018115', '10.0.1.138', 'review', NULL, NULL);
INSERT INTO tokens VALUES (319, 'X51ae169816c00ada0000000076e5292e00000000', 9, '2013-06-04 12:32:24.018115', '2013-06-04 12:32:25.467124', '10.0.1.138', 'review', NULL, NULL);
INSERT INTO tokens VALUES (320, 'X51ae1699218a01530000000075dfeb4a00000000', 9, '2013-06-04 12:32:25.467124', '2013-06-04 12:32:25.576312', '10.0.1.138', 'adqueues', NULL, NULL);
INSERT INTO tokens VALUES (321, 'X51ae169a4eb88e0f00000000508eb54300000000', 9, '2013-06-04 12:32:25.576312', '2013-06-04 12:32:25.578169', '10.0.1.138', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (322, 'X51ae169a6a99e9790000000075eda18600000000', 9, '2013-06-04 12:32:25.578169', '2013-06-04 12:32:25.580509', '10.0.1.138', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (323, 'X51ae169a46292fbc000000007ff10d3e00000000', 9, '2013-06-04 12:32:25.580509', '2013-06-04 12:32:25.586287', '10.0.1.138', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (324, 'X51ae169a5bcb849e0000000070ef958400000000', 9, '2013-06-04 12:32:25.586287', '2013-06-04 12:32:25.58822', '10.0.1.138', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (325, 'X51ae169a6b5832c60000000028451e8b00000000', 9, '2013-06-04 12:32:25.58822', '2013-06-04 12:32:25.590146', '10.0.1.138', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (326, 'X51ae169a597a2ad700000000552bd55c00000000', 9, '2013-06-04 12:32:25.590146', '2013-06-04 12:32:28.33171', '10.0.1.138', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (327, 'X51ae169c62cf084f000000004ed5c78b00000000', 9, '2013-06-04 12:32:28.33171', '2013-06-04 12:32:29.772201', '10.0.1.138', 'review', NULL, NULL);
INSERT INTO tokens VALUES (328, 'X51ae169e48196698000000001172e7be00000000', 9, '2013-06-04 12:32:29.772201', '2013-06-04 12:32:30.983014', '10.0.1.138', 'review', NULL, NULL);
INSERT INTO tokens VALUES (329, 'X51ae169f7facc08f000000006071885300000000', 9, '2013-06-04 12:32:30.983014', '2013-06-04 12:32:31.132227', '10.0.1.138', 'adqueues', NULL, NULL);
INSERT INTO tokens VALUES (330, 'X51ae169f4bdfb14700000000b536e7900000000', 9, '2013-06-04 12:32:31.132227', '2013-06-04 12:32:31.134285', '10.0.1.138', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (331, 'X51ae169f748bb2a6000000002827c3f300000000', 9, '2013-06-04 12:32:31.134285', '2013-06-04 12:32:31.136847', '10.0.1.138', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (332, 'X51ae169f3296dd3e000000002216663b00000000', 9, '2013-06-04 12:32:31.136847', '2013-06-04 12:32:31.144299', '10.0.1.138', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (333, 'X51ae169f1a1340dd00000000211a660a00000000', 9, '2013-06-04 12:32:31.144299', '2013-06-04 12:32:31.146335', '10.0.1.138', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (334, 'X51ae169f5b2a42310000000041aeb44200000000', 9, '2013-06-04 12:32:31.146335', '2013-06-04 12:32:31.14869', '10.0.1.138', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (335, 'X51ae169f68ac580800000000152846b600000000', 9, '2013-06-04 12:32:31.14869', '2013-06-04 12:32:33.486269', '10.0.1.138', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (336, 'X51ae16a11c85ef1a00000000a36595c00000000', 9, '2013-06-04 12:32:33.486269', '2013-06-04 12:32:35.533896', '10.0.1.138', 'review', NULL, NULL);
INSERT INTO tokens VALUES (337, 'X51ae16a43643fd0400000000670a94ad00000000', 9, '2013-06-04 12:32:35.533896', '2013-06-04 12:32:36.513937', '10.0.1.138', 'review', NULL, NULL);
INSERT INTO tokens VALUES (338, 'X51ae16a5b083201000000006b3e7d2900000000', 9, '2013-06-04 12:32:36.513937', '2013-06-04 12:32:36.676734', '10.0.1.138', 'adqueues', NULL, NULL);
INSERT INTO tokens VALUES (339, 'X51ae16a55ac50e9f0000000075a21b7a00000000', 9, '2013-06-04 12:32:36.676734', '2013-06-04 12:32:36.678731', '10.0.1.138', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (340, 'X51ae16a5612c1eaf0000000020ee3e5b00000000', 9, '2013-06-04 12:32:36.678731', '2013-06-04 12:32:36.680626', '10.0.1.138', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (341, 'X51ae16a5759328b8000000003cf7a34e00000000', 9, '2013-06-04 12:32:36.680626', '2013-06-04 12:32:36.686339', '10.0.1.138', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (342, 'X51ae16a511ddd3df0000000060eb5b7f00000000', 9, '2013-06-04 12:32:36.686339', '2013-06-04 12:32:36.688506', '10.0.1.138', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (343, 'X51ae16a5653cc1d9000000006b57feb600000000', 9, '2013-06-04 12:32:36.688506', '2013-06-04 12:32:36.690621', '10.0.1.138', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (344, 'X51ae16a5361730db00000000480bca2800000000', 9, '2013-06-04 12:32:36.690621', '2013-06-04 12:32:40.752566', '10.0.1.138', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (345, 'X51ae16a93a2dc6420000000035c3f16a00000000', 9, '2013-06-04 12:32:40.752566', '2013-06-04 12:32:42.236059', '10.0.1.138', 'review', NULL, NULL);
INSERT INTO tokens VALUES (346, 'X51ae16aa6504de45000000001761626200000000', 9, '2013-06-04 12:32:42.236059', '2013-06-04 12:32:43.516077', '10.0.1.138', 'review', NULL, NULL);
INSERT INTO tokens VALUES (347, 'X51ae16ac287d527b0000000060d778900000000', 9, '2013-06-04 12:32:43.516077', '2013-06-04 12:32:43.629674', '10.0.1.138', 'adqueues', NULL, NULL);
INSERT INTO tokens VALUES (348, 'X51ae16ac41175fe4000000001d09052200000000', 9, '2013-06-04 12:32:43.629674', '2013-06-04 12:32:43.631583', '10.0.1.138', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (349, 'X51ae16ac2e353b7c0000000073ae3d2200000000', 9, '2013-06-04 12:32:43.631583', '2013-06-04 12:32:43.634618', '10.0.1.138', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (350, 'X51ae16ac3f1f6b5d0000000048487c5900000000', 9, '2013-06-04 12:32:43.634618', '2013-06-04 12:32:43.640159', '10.0.1.138', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (351, 'X51ae16ac14c8a32d000000001a49ad8e00000000', 9, '2013-06-04 12:32:43.640159', '2013-06-04 12:32:43.642414', '10.0.1.138', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (352, 'X51ae16ac9f7309b000000007d74fb3500000000', 9, '2013-06-04 12:32:43.642414', '2013-06-04 12:32:43.644734', '10.0.1.138', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (353, 'X51ae16ac3a7a26460000000011bb9cdf00000000', 9, '2013-06-04 12:32:43.644734', '2013-06-04 12:32:45.688895', '10.0.1.138', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (354, 'X51ae16ae153c756b0000000069ac5f800000000', 9, '2013-06-04 12:32:45.688895', '2013-06-04 12:32:47.536527', '10.0.1.138', 'review', NULL, NULL);
INSERT INTO tokens VALUES (355, 'X51ae16b06ec5359100000000576972d400000000', 9, '2013-06-04 12:32:47.536527', '2013-06-04 12:32:48.86255', '10.0.1.138', 'review', NULL, NULL);
INSERT INTO tokens VALUES (356, 'X51ae16b17452d2e0000000004dbf55e200000000', 9, '2013-06-04 12:32:48.86255', '2013-06-04 12:32:48.941563', '10.0.1.138', 'adqueues', NULL, NULL);
INSERT INTO tokens VALUES (357, 'X51ae16b14bd615260000000021472ba500000000', 9, '2013-06-04 12:32:48.941563', '2013-06-04 12:32:48.945374', '10.0.1.138', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (358, 'X51ae16b131a1077e0000000052d0e58500000000', 9, '2013-06-04 12:32:48.945374', '2013-06-04 12:32:48.947862', '10.0.1.138', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (359, 'X51ae16b15d92a217000000007393fc3e00000000', 9, '2013-06-04 12:32:48.947862', '2013-06-04 12:32:52.556772', '10.0.1.138', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (360, 'X51ae16b52b38017e000000004963c67c00000000', 9, '2013-06-04 12:32:52.556772', '2013-06-04 12:32:53.978406', '10.0.1.138', 'review', NULL, NULL);
INSERT INTO tokens VALUES (361, 'X51ae16b67275e83e000000005a53d5900000000', 9, '2013-06-04 12:32:53.978406', '2013-06-04 12:32:53.982581', '10.0.1.138', 'adqueues', NULL, NULL);
INSERT INTO tokens VALUES (362, 'X51ae16b6273b85440000000061cdadd800000000', 9, '2013-06-04 12:32:53.982581', '2013-06-04 12:40:53.142518', '10.0.1.138', 'adqueues', NULL, NULL);
INSERT INTO tokens VALUES (363, 'X51ae1895f22d5ef00000000508e011800000000', 9, '2013-06-04 12:40:53.142518', '2013-06-04 12:40:53.249552', '10.0.1.173', 'authenticate', NULL, NULL);
INSERT INTO tokens VALUES (364, 'X51ae1895184eff3f000000005e8e8dc00000000', 9, '2013-06-04 12:40:53.249552', '2013-06-04 12:40:54.850947', '10.0.1.173', 'spamfilter', NULL, NULL);
INSERT INTO tokens VALUES (365, 'X51ae189765b63bc30000000037cc27f100000000', 9, '2013-06-04 12:40:54.850947', '2013-06-04 13:40:54.850947', '10.0.1.173', 'adqueues', NULL, NULL);
INSERT INTO tokens VALUES (366, 'X52014bd9106cd14a000000009122c2f00000000', 9, '2013-08-06 15:17:45.028921', '2013-08-06 15:17:45.419735', '10.0.1.175', 'authenticate', NULL, NULL);
INSERT INTO tokens VALUES (367, 'X52014bd9247657df000000006cca271a00000000', 9, '2013-08-06 15:17:45.419735', '2013-08-06 15:17:46.989291', '10.0.1.175', 'spamfilter', NULL, NULL);
INSERT INTO tokens VALUES (368, 'X52014bdb41d97b2e000000002483986f00000000', 9, '2013-08-06 15:17:46.989291', '2013-08-06 15:17:49.793438', '10.0.1.175', 'adqueues', NULL, NULL);
INSERT INTO tokens VALUES (369, 'X52014bde598ec8cb000000001eece0db00000000', 9, '2013-08-06 15:17:49.793438', '2013-08-06 15:17:51.725205', '10.0.1.175', 'spamfilter', NULL, NULL);
INSERT INTO tokens VALUES (370, 'X52014be020c1104c0000000066fac1d200000000', 9, '2013-08-06 15:17:51.725205', '2013-08-06 15:17:53.368986', '10.0.1.175', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (371, 'X52014be1108850fb000000007379647d00000000', 9, '2013-08-06 15:17:53.368986', '2013-08-06 15:17:55.92664', '10.0.1.175', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (372, 'X52014be477305e35000000004781fbcd00000000', 9, '2013-08-06 15:17:55.92664', '2013-08-06 15:17:55.930636', '10.0.1.175', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (373, 'X52014be417b42e1e0000000013fb68500000000', 9, '2013-08-06 15:17:55.930636', '2013-08-06 15:19:23.535361', '10.0.1.175', 'blocked_item', NULL, NULL);
INSERT INTO tokens VALUES (374, 'X52014c3c5d15ca0c0000000068ce1c4200000000', 9, '2013-08-06 15:19:23.535361', '2013-08-06 15:19:25.777208', '10.0.1.175', 'spamfilter', NULL, NULL);
INSERT INTO tokens VALUES (375, 'X52014c3e1c14bae0000000002a18750d00000000', 9, '2013-08-06 15:19:25.777208', '2013-08-06 15:19:26.840596', '10.0.1.175', 'adqueues', NULL, NULL);
INSERT INTO tokens VALUES (376, 'X52014c3f7cd92961000000005ac2285d00000000', 9, '2013-08-06 15:19:26.840596', '2013-08-06 15:19:26.869652', '10.0.1.175', 'adqueues', NULL, NULL);
INSERT INTO tokens VALUES (377, 'X52014c3f720f0c32000000005d2eb9fc00000000', 9, '2013-08-06 15:19:26.869652', '2013-08-06 15:19:26.871603', '10.0.1.175', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (378, 'X52014c3f39fc4000000004f6f7cf900000000', 9, '2013-08-06 15:19:26.871603', '2013-08-06 15:19:26.873776', '10.0.1.175', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (379, 'X52014c3f426e2e04000000007045ab7a00000000', 9, '2013-08-06 15:19:26.873776', '2013-08-06 15:19:26.922656', '10.0.1.175', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (380, 'X52014c3fea8326c0000000058df6f3d00000000', 9, '2013-08-06 15:19:26.922656', '2013-08-06 15:19:26.924404', '10.0.1.175', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (381, 'X52014c3f6893d937000000001f7008a800000000', 9, '2013-08-06 15:19:26.924404', '2013-08-06 15:19:26.92653', '10.0.1.175', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (382, 'X52014c3f52f901fd000000006947e98c00000000', 9, '2013-08-06 15:19:26.92653', '2013-08-06 15:19:30.561483', '10.0.1.175', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (383, 'X52014c432a3e5321000000005ff9c9bc00000000', 9, '2013-08-06 15:19:30.561483', '2013-08-06 15:19:34.892067', '10.0.1.175', 'review', NULL, NULL);
INSERT INTO tokens VALUES (384, 'X52014c473dc5d1a6000000006716a69e00000000', 9, '2013-08-06 15:19:34.892067', '2013-08-06 15:19:37.172766', '10.0.1.175', 'review', NULL, NULL);
INSERT INTO tokens VALUES (385, 'X52014c49f0462f9000000007a2634ba00000000', 9, '2013-08-06 15:19:37.172766', '2013-08-06 15:19:48.943259', '10.0.1.175', 'spamfilter', NULL, NULL);
INSERT INTO tokens VALUES (386, 'X52014c55551877f4000000001368abd00000000', 9, '2013-08-06 15:19:48.943259', '2013-08-06 15:19:50.724868', '10.0.1.175', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (387, 'X52014c57205607640000000013a2dcc400000000', 9, '2013-08-06 15:19:50.724868', '2013-08-06 15:19:57.038992', '10.0.1.175', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (388, 'X52014c5d5c3f2d30000000003db59dd800000000', 9, '2013-08-06 15:19:57.038992', '2013-08-06 15:19:57.04347', '10.0.1.175', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (389, 'X52014c5d368f4f38000000004b5a8e5500000000', 9, '2013-08-06 15:19:57.04347', '2013-08-06 15:19:59.303624', '10.0.1.175', 'blocked_item', NULL, NULL);
INSERT INTO tokens VALUES (390, 'X52014c5fdc9a7d0000000013a5194400000000', 9, '2013-08-06 15:19:59.303624', '2013-08-06 15:19:59.305858', '10.0.1.175', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (391, 'X52014c5f3428aa98000000001cf1555d00000000', 9, '2013-08-06 15:19:59.305858', '2013-08-06 15:19:59.520958', '10.0.1.175', 'blocked_item', NULL, NULL);
INSERT INTO tokens VALUES (392, 'X52014c603dbd8e52000000003101d3f900000000', 9, '2013-08-06 15:19:59.520958', '2013-08-06 15:19:59.522979', '10.0.1.175', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (393, 'X52014c6077b37dba000000002fcc9a8400000000', 9, '2013-08-06 15:19:59.522979', '2013-08-06 15:19:59.693169', '10.0.1.175', 'blocked_item', NULL, NULL);
INSERT INTO tokens VALUES (394, 'X52014c60e308df60000000077b71d7e00000000', 9, '2013-08-06 15:19:59.693169', '2013-08-06 15:19:59.695088', '10.0.1.175', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (395, 'X52014c607f3c177d00000000509ebbfa00000000', 9, '2013-08-06 15:19:59.695088', '2013-08-06 15:19:59.992846', '10.0.1.175', 'blocked_item', NULL, NULL);
INSERT INTO tokens VALUES (396, 'X52014c6067fcc8f800000000de449e900000000', 9, '2013-08-06 15:19:59.992846', '2013-08-06 15:20:00.005051', '10.0.1.175', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (397, 'X52014c60297e2b37000000005090a22f00000000', 9, '2013-08-06 15:20:00.005051', '2013-08-06 15:20:00.447232', '10.0.1.175', 'blocked_item', NULL, NULL);
INSERT INTO tokens VALUES (398, 'X52014c602d545292000000007c772d3400000000', 9, '2013-08-06 15:20:00.447232', '2013-08-06 15:20:00.450639', '10.0.1.175', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (399, 'X52014c6039d88bbc000000005792a5b300000000', 9, '2013-08-06 15:20:00.450639', '2013-08-06 15:20:00.786539', '10.0.1.175', 'blocked_item', NULL, NULL);
INSERT INTO tokens VALUES (400, 'X52014c615c70f6f00000000048dceeb500000000', 9, '2013-08-06 15:20:00.786539', '2013-08-06 15:20:00.789464', '10.0.1.175', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (401, 'X52014c6151b8da6d0000000031896ee400000000', 9, '2013-08-06 15:20:00.789464', '2013-08-06 15:20:01.140865', '10.0.1.175', 'blocked_item', NULL, NULL);
INSERT INTO tokens VALUES (402, 'X52014c614a13797300000000720ee1d200000000', 9, '2013-08-06 15:20:01.140865', '2013-08-06 15:20:01.143636', '10.0.1.175', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (403, 'X52014c61452c4ba9000000002652a6a300000000', 9, '2013-08-06 15:20:01.143636', '2013-08-06 15:20:01.342281', '10.0.1.175', 'blocked_item', NULL, NULL);
INSERT INTO tokens VALUES (404, 'X52014c6130a11a2700000000f60b42500000000', 9, '2013-08-06 15:20:01.342281', '2013-08-06 15:20:01.345928', '10.0.1.175', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (405, 'X52014c6165084180000000003cfd0abc00000000', 9, '2013-08-06 15:20:01.345928', '2013-08-06 15:20:02.24285', '10.0.1.175', 'blocked_item', NULL, NULL);
INSERT INTO tokens VALUES (406, 'X52014c62375f90f5000000005b9c55e600000000', 9, '2013-08-06 15:20:02.24285', '2013-08-06 15:20:02.245801', '10.0.1.175', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (407, 'X52014c62425369c80000000052b9f2e800000000', 9, '2013-08-06 15:20:02.245801', '2013-08-06 15:20:02.520574', '10.0.1.175', 'blocked_item', NULL, NULL);
INSERT INTO tokens VALUES (408, 'X52014c6339df566a000000002924f400000000', 9, '2013-08-06 15:20:02.520574', '2013-08-06 15:20:02.524787', '10.0.1.175', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (409, 'X52014c633c0919fe000000002f4f43700000000', 9, '2013-08-06 15:20:02.524787', '2013-08-06 15:20:02.793391', '10.0.1.175', 'blocked_item', NULL, NULL);
INSERT INTO tokens VALUES (410, 'X52014c63a1aa8ba0000000050d5402a00000000', 9, '2013-08-06 15:20:02.793391', '2013-08-06 15:20:02.796255', '10.0.1.175', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (411, 'X52014c632c719610000000004cdc3f6f00000000', 9, '2013-08-06 15:20:02.796255', '2013-08-06 15:20:03.255673', '10.0.1.175', 'blocked_item', NULL, NULL);
INSERT INTO tokens VALUES (412, 'X52014c63451c2fc5000000003117816e00000000', 9, '2013-08-06 15:20:03.255673', '2013-08-06 15:20:03.25763', '10.0.1.175', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (413, 'X52014c633d17456000000007cd8a60500000000', 9, '2013-08-06 15:20:03.25763', '2013-08-06 15:20:03.420095', '10.0.1.175', 'blocked_item', NULL, NULL);
INSERT INTO tokens VALUES (414, 'X52014c632757484a000000004a68ef5d00000000', 9, '2013-08-06 15:20:03.420095', '2013-08-06 15:20:03.422293', '10.0.1.175', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (415, 'X52014c634a921451000000003a216fde00000000', 9, '2013-08-06 15:20:03.422293', '2013-08-06 15:20:03.571429', '10.0.1.175', 'blocked_item', NULL, NULL);
INSERT INTO tokens VALUES (416, 'X52014c64195959000000006c1cad7500000000', 9, '2013-08-06 15:20:03.571429', '2013-08-06 15:20:03.573709', '10.0.1.175', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (417, 'X52014c646c2c17cc000000004f4c1e4700000000', 9, '2013-08-06 15:20:03.573709', '2013-08-06 15:20:03.717526', '10.0.1.175', 'blocked_item', NULL, NULL);
INSERT INTO tokens VALUES (418, 'X52014c6420215e71000000004876534b00000000', 9, '2013-08-06 15:20:03.717526', '2013-08-06 15:20:03.719566', '10.0.1.175', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (419, 'X52014c64155292ba0000000082d6a8e00000000', 9, '2013-08-06 15:20:03.719566', '2013-08-06 15:20:03.86671', '10.0.1.175', 'blocked_item', NULL, NULL);
INSERT INTO tokens VALUES (420, 'X52014c643944ebfc000000007f27760900000000', 9, '2013-08-06 15:20:03.86671', '2013-08-06 15:20:03.86933', '10.0.1.175', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (421, 'X52014c647c001c0e0000000051f4060600000000', 9, '2013-08-06 15:20:03.86933', '2013-08-06 15:20:04.01466', '10.0.1.175', 'blocked_item', NULL, NULL);
INSERT INTO tokens VALUES (422, 'X52014c641c5cf563000000007cd5badb00000000', 9, '2013-08-06 15:20:04.01466', '2013-08-06 15:20:04.016772', '10.0.1.175', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (423, 'X52014c6436f72ab9000000006cc23aa500000000', 9, '2013-08-06 15:20:04.016772', '2013-08-06 15:20:04.164027', '10.0.1.175', 'blocked_item', NULL, NULL);
INSERT INTO tokens VALUES (424, 'X52014c6458dee81a00000000661dfd5300000000', 9, '2013-08-06 15:20:04.164027', '2013-08-06 15:20:04.165956', '10.0.1.175', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (425, 'X52014c64356a1b9a00000000651be71e00000000', 9, '2013-08-06 15:20:04.165956', '2013-08-06 15:20:39.609669', '10.0.1.175', 'blocked_item', NULL, NULL);
INSERT INTO tokens VALUES (426, 'X52014c882d923a69000000007312baa400000000', 9, '2013-08-06 15:20:39.609669', '2013-08-06 15:20:39.616595', '10.0.1.175', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (427, 'X52014c887b40253200000000533045f300000000', 9, '2013-08-06 15:20:39.616595', '2013-08-06 15:20:41.180979', '10.0.1.175', 'blocked_item', NULL, NULL);
INSERT INTO tokens VALUES (428, 'X52014c895257bbfc000000007f31ebb000000000', 9, '2013-08-06 15:20:41.180979', '2013-08-06 15:20:41.182884', '10.0.1.175', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (429, 'X52014c895125f1b6000000003501b8bc00000000', 9, '2013-08-06 15:20:41.182884', '2013-08-06 15:20:41.56422', '10.0.1.175', 'blocked_item', NULL, NULL);
INSERT INTO tokens VALUES (430, 'X52014c8a31d7739700000000763d3f7200000000', 9, '2013-08-06 15:20:41.56422', '2013-08-06 15:20:41.56624', '10.0.1.175', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (431, 'X52014c8a62ff7a17000000005cf3e75200000000', 9, '2013-08-06 15:20:41.56624', '2013-08-06 15:20:41.91189', '10.0.1.175', 'blocked_item', NULL, NULL);
INSERT INTO tokens VALUES (432, 'X52014c8a4311e4a50000000059fdf40600000000', 9, '2013-08-06 15:20:41.91189', '2013-08-06 15:20:41.914167', '10.0.1.175', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (433, 'X52014c8a787c003f000000003f19db2400000000', 9, '2013-08-06 15:20:41.914167', '2013-08-06 15:20:42.140969', '10.0.1.175', 'blocked_item', NULL, NULL);
INSERT INTO tokens VALUES (434, 'X52014c8a3c3b2285000000004e077a4a00000000', 9, '2013-08-06 15:20:42.140969', '2013-08-06 15:20:42.142924', '10.0.1.175', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (435, 'X52014c8a6cac158d000000002f4ddd2900000000', 9, '2013-08-06 15:20:42.142924', '2013-08-06 15:20:42.363846', '10.0.1.175', 'blocked_item', NULL, NULL);
INSERT INTO tokens VALUES (436, 'X52014c8a7025e8d6000000002f90e2b000000000', 9, '2013-08-06 15:20:42.363846', '2013-08-06 15:20:42.366201', '10.0.1.175', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (437, 'X52014c8a2a8e025b0000000043562ec900000000', 9, '2013-08-06 15:20:42.366201', '2013-08-06 15:21:18.078389', '10.0.1.175', 'blocked_item', NULL, NULL);
INSERT INTO tokens VALUES (438, 'X52014cae4ce47253000000005f13138a00000000', 9, '2013-08-06 15:21:18.078389', '2013-08-06 15:21:18.082549', '10.0.1.175', 'blocked_item', NULL, NULL);
INSERT INTO tokens VALUES (439, 'X52014cae15adeac6000000004c165e0300000000', 9, '2013-08-06 15:21:18.082549', '2013-08-06 15:21:23.173837', '10.0.1.175', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (440, 'X52014cb350ae850c000000006405c2d100000000', 9, '2013-08-06 15:21:23.173837', '2013-08-06 15:21:23.177869', '10.0.1.175', 'blocked_item', NULL, NULL);
INSERT INTO tokens VALUES (441, 'X52014cb31d3c4fb9000000005b03dc800000000', 9, '2013-08-06 15:21:23.177869', '2013-08-06 15:21:29.296308', '10.0.1.175', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (442, 'X52014cb955b4ff3500000000abf36d200000000', 9, '2013-08-06 15:21:29.296308', '2013-08-06 15:21:29.298741', '10.0.1.175', 'blocked_item', NULL, NULL);
INSERT INTO tokens VALUES (443, 'X52014cb93787b15f000000004bf23ea700000000', 9, '2013-08-06 15:21:29.298741', '2013-08-06 15:21:34.131924', '10.0.1.175', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (444, 'X52014cbe5f3127a00000000020564fb000000000', 9, '2013-08-06 15:21:34.131924', '2013-08-06 15:21:34.134335', '10.0.1.175', 'blocked_item', NULL, NULL);
INSERT INTO tokens VALUES (445, 'X52014cbe2ef1b8be000000003c250ef200000000', 9, '2013-08-06 15:21:34.134335', '2013-08-06 15:21:38.974308', '10.0.1.175', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (446, 'X52014cc340357214000000006ad01aef00000000', 9, '2013-08-06 15:21:38.974308', '2013-08-06 15:21:38.977744', '10.0.1.175', 'blocked_item', NULL, NULL);
INSERT INTO tokens VALUES (447, 'X52014cc37f36f397000000001a33661a00000000', 9, '2013-08-06 15:21:38.977744', '2013-08-06 15:21:44.172088', '10.0.1.175', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (448, 'X52014cc86bb97d060000000077b2f3d600000000', 9, '2013-08-06 15:21:44.172088', '2013-08-06 15:21:44.175614', '10.0.1.175', 'blocked_item', NULL, NULL);
INSERT INTO tokens VALUES (449, 'X52014cc8594d413e0000000027f49f8b00000000', 9, '2013-08-06 15:21:44.175614', '2013-08-06 15:21:51.749891', '10.0.1.175', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (450, 'X52014cd045ba6e200000000045f956cb00000000', 9, '2013-08-06 15:21:51.749891', '2013-08-06 15:21:51.752369', '10.0.1.175', 'blocked_item', NULL, NULL);
INSERT INTO tokens VALUES (451, 'X52014cd057427cb40000000035e056f600000000', 9, '2013-08-06 15:21:51.752369', '2013-08-06 15:21:57.666623', '10.0.1.175', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (452, 'X52014cd6758a397b000000001d07f1000000000', 9, '2013-08-06 15:21:57.666623', '2013-08-06 15:21:57.669365', '10.0.1.175', 'blocked_item', NULL, NULL);
INSERT INTO tokens VALUES (453, 'X52014cd6793685bf00000000426eabcf00000000', 9, '2013-08-06 15:21:57.669365', '2013-08-06 15:22:00.080666', '10.0.1.175', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (454, 'X52014cd860e3929a00000000ee4708500000000', 9, '2013-08-06 15:22:00.080666', '2013-08-06 15:31:18.24761', '10.0.1.175', 'spamfilter', NULL, NULL);
INSERT INTO tokens VALUES (455, 'X52014f061a04b175000000004334780700000000', 9, '2013-08-06 15:31:18.24761', '2013-08-06 15:31:18.39018', '10.0.1.175', 'authenticate', NULL, NULL);
INSERT INTO tokens VALUES (456, 'X52014f067d91b1dc000000001391cc7f00000000', 9, '2013-08-06 15:31:18.39018', '2013-08-06 15:31:19.87261', '10.0.1.175', 'spamfilter', NULL, NULL);
INSERT INTO tokens VALUES (457, 'X52014f0827ab9c0000000006586a7b000000000', 9, '2013-08-06 15:31:19.87261', '2013-08-06 15:31:26.998445', '10.0.1.175', 'adqueues', NULL, NULL);
INSERT INTO tokens VALUES (458, 'X52014f0f7cd826ba000000007cb25e9600000000', 9, '2013-08-06 15:31:26.998445', '2013-08-06 15:31:29.364429', '10.0.1.175', 'spamfilter', NULL, NULL);
INSERT INTO tokens VALUES (459, 'X52014f117e82312d0000000035660cc900000000', 9, '2013-08-06 15:31:29.364429', '2013-08-06 15:31:30.883389', '10.0.1.175', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (460, 'X52014f1343e8eb9e000000006de53f1f00000000', 9, '2013-08-06 15:31:30.883389', '2013-08-06 15:31:33.754789', '10.0.1.175', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (461, 'X52014f16692a465d000000006946bca300000000', 9, '2013-08-06 15:31:33.754789', '2013-08-06 15:31:33.758442', '10.0.1.175', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (462, 'X52014f16f25e8610000000078c9a68800000000', 9, '2013-08-06 15:31:33.758442', '2013-08-06 15:31:41.844105', '10.0.1.175', 'blocked_item', NULL, NULL);
INSERT INTO tokens VALUES (463, 'X52014f1e765b586400000000797d3fe000000000', 9, '2013-08-06 15:31:41.844105', '2013-08-06 15:31:41.847602', '10.0.1.175', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (464, 'X52014f1e5217dec4000000003d7e4b0600000000', 9, '2013-08-06 15:31:41.847602', '2013-08-06 15:31:45.533496', '10.0.1.175', 'blocked_item', NULL, NULL);
INSERT INTO tokens VALUES (465, 'X52014f223dc7cc20000000003a0afb4200000000', 9, '2013-08-06 15:31:45.533496', '2013-08-06 15:31:45.537137', '10.0.1.175', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (466, 'X52014f2236bd59d8000000001278881000000000', 9, '2013-08-06 15:31:45.537137', '2013-08-06 15:31:58.357299', '10.0.1.175', 'blocked_item', NULL, NULL);
INSERT INTO tokens VALUES (467, 'X52014f2e47de94d9000000003a29b3b100000000', 9, '2013-08-06 15:31:58.357299', '2013-08-06 15:31:58.359476', '10.0.1.175', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (468, 'X52014f2e280ef2d00000000038ceb2e500000000', 9, '2013-08-06 15:31:58.359476', '2013-08-06 15:32:01.485136', '10.0.1.175', 'blocked_item', NULL, NULL);
INSERT INTO tokens VALUES (469, 'X52014f3122156f8800000000717d511900000000', 9, '2013-08-06 15:32:01.485136', '2013-08-06 15:39:32.439769', '10.0.1.175', 'spamfilter', NULL, NULL);
INSERT INTO tokens VALUES (470, 'X520150f43d586b26000000002c48b32d00000000', 9, '2013-08-06 15:39:32.439769', '2013-08-06 15:39:32.54337', '10.0.1.175', 'authenticate', NULL, NULL);
INSERT INTO tokens VALUES (471, 'X520150f5751fe62c000000002ec56ada00000000', 9, '2013-08-06 15:39:32.54337', '2013-08-06 15:39:33.636846', '10.0.1.175', 'spamfilter', NULL, NULL);
INSERT INTO tokens VALUES (472, 'X520150f673e704c0000000005461de5900000000', 9, '2013-08-06 15:39:33.636846', '2013-08-06 15:39:34.779918', '10.0.1.175', 'adqueues', NULL, NULL);
INSERT INTO tokens VALUES (473, 'X520150f7a5e126d00000000cd84e8600000000', 9, '2013-08-06 15:39:34.779918', '2013-08-06 15:39:34.829389', '10.0.1.175', 'adqueues', NULL, NULL);
INSERT INTO tokens VALUES (474, 'X520150f71359bcd80000000014148f3300000000', 9, '2013-08-06 15:39:34.829389', '2013-08-06 15:39:34.831923', '10.0.1.175', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (475, 'X520150f722aa2c76000000006975b7da00000000', 9, '2013-08-06 15:39:34.831923', '2013-08-06 15:39:34.834545', '10.0.1.175', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (476, 'X520150f72fe19964000000003d85910200000000', 9, '2013-08-06 15:39:34.834545', '2013-08-06 15:39:34.84003', '10.0.1.175', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (477, 'X520150f758423517000000003abeda8700000000', 9, '2013-08-06 15:39:34.84003', '2013-08-06 15:39:34.841856', '10.0.1.175', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (478, 'X520150f767078db4000000007ba0aba600000000', 9, '2013-08-06 15:39:34.841856', '2013-08-06 15:39:34.843772', '10.0.1.175', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (479, 'X520150f72a661680000000002e1ec8bf00000000', 9, '2013-08-06 15:39:34.843772', '2013-08-06 15:39:36.126394', '10.0.1.175', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (480, 'X520150f8280a718000000004fb8ed400000000', 9, '2013-08-06 15:39:36.126394', '2013-08-06 15:39:37.917397', '10.0.1.175', 'adqueues', NULL, NULL);
INSERT INTO tokens VALUES (481, 'X520150fa11d3dd5b00000000464a1ec400000000', 9, '2013-08-06 15:39:37.917397', '2013-08-06 15:39:37.939279', '10.0.1.175', 'adqueues', NULL, NULL);
INSERT INTO tokens VALUES (482, 'X520150fa5a5eadf7000000005d11884b00000000', 9, '2013-08-06 15:39:37.939279', '2013-08-06 15:39:37.941201', '10.0.1.175', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (483, 'X520150fa46874025000000007eaf893100000000', 9, '2013-08-06 15:39:37.941201', '2013-08-06 15:39:37.94315', '10.0.1.175', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (484, 'X520150fa3c351a3400000000627f236600000000', 9, '2013-08-06 15:39:37.94315', '2013-08-06 15:39:37.948143', '10.0.1.175', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (485, 'X520150fa1d3dfded000000003eb0f8f800000000', 9, '2013-08-06 15:39:37.948143', '2013-08-06 15:39:37.950034', '10.0.1.175', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (486, 'X520150fa3a51a49e0000000021ac653500000000', 9, '2013-08-06 15:39:37.950034', '2013-08-06 15:39:37.952038', '10.0.1.175', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (487, 'X520150fa4fcb2df40000000050d6cfee00000000', 9, '2013-08-06 15:39:37.952038', '2013-08-06 15:39:45.768679', '10.0.1.175', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (488, 'X5201510255d25ec3000000004746a36100000000', 9, '2013-08-06 15:39:45.768679', '2013-08-06 15:39:48.851011', '10.0.1.175', 'spamfilter', NULL, NULL);
INSERT INTO tokens VALUES (489, 'X52015105d90c225000000005d53a20d00000000', 9, '2013-08-06 15:39:48.851011', '2013-08-06 15:43:26.828443', '10.0.1.175', 'adqueues', NULL, NULL);
INSERT INTO tokens VALUES (490, 'X520151df3f3128a8000000002f565c6c00000000', 9, '2013-08-06 15:43:26.828443', '2013-08-06 15:43:27.933546', '10.0.1.175', 'adqueues', NULL, NULL);
INSERT INTO tokens VALUES (491, 'X520151e01cd3c1cd000000006ebfe1cc00000000', 9, '2013-08-06 15:43:27.933546', '2013-08-06 15:43:28.283073', '10.0.1.175', 'adqueues', NULL, NULL);
INSERT INTO tokens VALUES (492, 'X520151e05dbbdca5000000004efb327400000000', 9, '2013-08-06 15:43:28.283073', '2013-08-06 15:43:28.720406', '10.0.1.175', 'adqueues', NULL, NULL);
INSERT INTO tokens VALUES (493, 'X520151e175eceeff000000006103314400000000', 9, '2013-08-06 15:43:28.720406', '2013-08-06 15:43:32.823512', '10.0.1.175', 'adqueues', NULL, NULL);
INSERT INTO tokens VALUES (494, 'X520151e54a1a26fe000000003728c89a00000000', 9, '2013-08-06 15:43:32.823512', '2013-08-06 15:43:32.860263', '10.0.1.175', 'adqueues', NULL, NULL);
INSERT INTO tokens VALUES (495, 'X520151e54d8f6e0e000000002e6d60d400000000', 9, '2013-08-06 15:43:32.860263', '2013-08-06 15:43:32.862851', '10.0.1.175', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (496, 'X520151e5320e6ad9000000004921bf0e00000000', 9, '2013-08-06 15:43:32.862851', '2013-08-06 15:43:32.864666', '10.0.1.175', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (497, 'X520151e561f5e817000000009f293b800000000', 9, '2013-08-06 15:43:32.864666', '2013-08-06 15:43:32.869474', '10.0.1.175', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (498, 'X520151e53948f025000000001ebd84cf00000000', 9, '2013-08-06 15:43:32.869474', '2013-08-06 15:43:32.871135', '10.0.1.175', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (499, 'X520151e5d7d669b000000003d103bb600000000', 9, '2013-08-06 15:43:32.871135', '2013-08-06 15:43:32.873213', '10.0.1.175', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (500, 'X520151e5c0b6e2a00000000181309f800000000', 9, '2013-08-06 15:43:32.873213', '2013-08-06 15:43:37.99454', '10.0.1.175', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (501, 'X520151ea79163b3c0000000035510b4900000000', 9, '2013-08-06 15:43:37.99454', '2013-08-06 15:43:41.885016', '10.0.1.175', 'adqueues', NULL, NULL);
INSERT INTO tokens VALUES (502, 'X520151ee6c79d3e4000000003999b4ec00000000', 9, '2013-08-06 15:43:41.885016', '2013-08-06 15:43:41.906516', '10.0.1.175', 'adqueues', NULL, NULL);
INSERT INTO tokens VALUES (503, 'X520151ee680715c00000000048efee8700000000', 9, '2013-08-06 15:43:41.906516', '2013-08-06 15:43:41.909145', '10.0.1.175', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (504, 'X520151ee1211ad95000000001f008c7500000000', 9, '2013-08-06 15:43:41.909145', '2013-08-06 15:43:41.9111', '10.0.1.175', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (505, 'X520151ee28f3202d000000001d0e804100000000', 9, '2013-08-06 15:43:41.9111', '2013-08-06 15:43:41.916005', '10.0.1.175', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (506, 'X520151ee3bcc0510000000006464b65100000000', 9, '2013-08-06 15:43:41.916005', '2013-08-06 15:43:41.917701', '10.0.1.175', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (507, 'X520151ee2174f207000000003a098c500000000', 9, '2013-08-06 15:43:41.917701', '2013-08-06 15:43:41.919383', '10.0.1.175', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (508, 'X520151ee1bb3a2bd0000000018749fda00000000', 9, '2013-08-06 15:43:41.919383', '2013-08-06 15:43:48.164428', '10.0.1.175', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (509, 'X520151f44dc5ab24000000007d3a0cf200000000', 9, '2013-08-06 15:43:48.164428', '2013-08-06 15:43:48.184216', '10.0.1.175', 'adqueues', NULL, NULL);
INSERT INTO tokens VALUES (510, 'X520151f436d3c1de000000001b11688100000000', 9, '2013-08-06 15:43:48.184216', '2013-08-06 15:43:48.186263', '10.0.1.175', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (511, 'X520151f464015708000000007014607100000000', 9, '2013-08-06 15:43:48.186263', '2013-08-06 15:43:48.188318', '10.0.1.175', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (512, 'X520151f4f14ece60000000075aa7f9800000000', 9, '2013-08-06 15:43:48.188318', '2013-08-06 15:43:48.193577', '10.0.1.175', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (513, 'X520151f412b8ffd9000000007f4940b600000000', 9, '2013-08-06 15:43:48.193577', '2013-08-06 15:43:48.195265', '10.0.1.175', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (514, 'X520151f463adf70700000000529cdfab00000000', 9, '2013-08-06 15:43:48.195265', '2013-08-06 15:43:48.19733', '10.0.1.175', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (515, 'X520151f4563d7870000000004c511f8300000000', 9, '2013-08-06 15:43:48.19733', '2013-08-06 15:43:56.627515', '10.0.1.175', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (516, 'X520151fd64c5bf5d00000000621c069b00000000', 9, '2013-08-06 15:43:56.627515', '2013-08-06 15:43:56.662892', '10.0.1.175', 'adqueues', NULL, NULL);
INSERT INTO tokens VALUES (517, 'X520151fd5f56138d000000005eb5395c00000000', 9, '2013-08-06 15:43:56.662892', '2013-08-06 15:43:56.665296', '10.0.1.175', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (518, 'X520151fd79c6a1dd000000006113aa8300000000', 9, '2013-08-06 15:43:56.665296', '2013-08-06 15:43:56.667208', '10.0.1.175', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (519, 'X520151fd51280af5000000006df5054200000000', 9, '2013-08-06 15:43:56.667208', '2013-08-06 15:43:56.672323', '10.0.1.175', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (520, 'X520151fd639f84db000000003df597ce00000000', 9, '2013-08-06 15:43:56.672323', '2013-08-06 15:43:56.674231', '10.0.1.175', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (521, 'X520151fd3d3ed884000000003289225600000000', 9, '2013-08-06 15:43:56.674231', '2013-08-06 15:43:56.676511', '10.0.1.175', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (522, 'X520151fd5260201000000005cabfa3400000000', 9, '2013-08-06 15:43:56.676511', '2013-08-06 15:44:04.588738', '10.0.1.175', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (523, 'X5201520528fd19b70000000058b301db00000000', 9, '2013-08-06 15:44:04.588738', '2013-08-06 15:44:04.601733', '10.0.1.175', 'adqueues', NULL, NULL);
INSERT INTO tokens VALUES (524, 'X520152053acf087600000000540f8ec700000000', 9, '2013-08-06 15:44:04.601733', '2013-08-06 15:44:04.603618', '10.0.1.175', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (525, 'X5201520532c4c824000000002d192d2000000000', 9, '2013-08-06 15:44:04.603618', '2013-08-06 15:44:04.605898', '10.0.1.175', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (526, 'X52015205e2cd7a30000000065539e7a00000000', 9, '2013-08-06 15:44:04.605898', '2013-08-06 15:44:13.375533', '10.0.1.175', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (527, 'X5201520d5348a3bd000000005e2e606b00000000', 9, '2013-08-06 15:44:13.375533', '2013-08-06 15:44:13.410462', '10.0.1.175', 'adqueues', NULL, NULL);
INSERT INTO tokens VALUES (528, 'X5201520d1c23f83a0000000057d6df6300000000', 9, '2013-08-06 15:44:13.410462', '2013-08-06 15:44:13.41242', '10.0.1.175', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (529, 'X5201520da6001b900000000721e36eb00000000', 9, '2013-08-06 15:44:13.41242', '2013-08-06 15:44:13.414767', '10.0.1.175', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (530, 'X5201520d4eca311f00000000205f58e500000000', 9, '2013-08-06 15:44:13.414767', '2013-08-06 15:44:13.420825', '10.0.1.175', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (531, 'X5201520d79125ac100000000383e13cb00000000', 9, '2013-08-06 15:44:13.420825', '2013-08-06 15:44:13.422835', '10.0.1.175', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (532, 'X5201520dc4da2930000000045d8678200000000', 9, '2013-08-06 15:44:13.422835', '2013-08-06 15:44:13.425082', '10.0.1.175', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (533, 'X5201520d72f194a2000000007e5570ab00000000', 9, '2013-08-06 15:44:13.425082', '2013-08-06 15:44:17.552733', '10.0.1.175', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (534, 'X5201521263a90f260000000075f137a000000000', 9, '2013-08-06 15:44:17.552733', '2013-08-06 15:44:17.573316', '10.0.1.175', 'adqueues', NULL, NULL);
INSERT INTO tokens VALUES (535, 'X5201521260734ede0000000036f1b2e300000000', 9, '2013-08-06 15:44:17.573316', '2013-08-06 15:44:17.575677', '10.0.1.175', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (536, 'X52015212541f980b000000002005295200000000', 9, '2013-08-06 15:44:17.575677', '2013-08-06 15:44:17.577671', '10.0.1.175', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (537, 'X520152126dd9db7b000000007043904500000000', 9, '2013-08-06 15:44:17.577671', '2013-08-06 15:44:17.583131', '10.0.1.175', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (538, 'X5201521277dc08b6000000001b1a88c700000000', 9, '2013-08-06 15:44:17.583131', '2013-08-06 15:44:17.58497', '10.0.1.175', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (539, 'X5201521249a661040000000023c0a6f00000000', 9, '2013-08-06 15:44:17.58497', '2013-08-06 15:44:17.587046', '10.0.1.175', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (540, 'X52015212d38bfb20000000043f6014e00000000', 9, '2013-08-06 15:44:17.587046', '2013-08-06 15:44:20.846598', '10.0.1.175', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (541, 'X5201521511c20e2a000000005c02f0d200000000', 9, '2013-08-06 15:44:20.846598', '2013-08-06 15:44:20.869189', '10.0.1.175', 'adqueues', NULL, NULL);
INSERT INTO tokens VALUES (542, 'X5201521564555a34000000007cab8c5600000000', 9, '2013-08-06 15:44:20.869189', '2013-08-06 15:44:20.871551', '10.0.1.175', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (543, 'X5201521553ca3ba9000000005d67b4f500000000', 9, '2013-08-06 15:44:20.871551', '2013-08-06 15:44:20.873424', '10.0.1.175', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (544, 'X5201521534e9a0210000000059545f9500000000', 9, '2013-08-06 15:44:20.873424', '2013-08-06 15:44:20.879184', '10.0.1.175', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (545, 'X520152151149182c00000000413742b400000000', 9, '2013-08-06 15:44:20.879184', '2013-08-06 15:44:20.881321', '10.0.1.175', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (546, 'X520152151f2cc717000000005f4f976800000000', 9, '2013-08-06 15:44:20.881321', '2013-08-06 15:44:20.883276', '10.0.1.175', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (547, 'X5201521549ad6b00000000121e5bba00000000', 9, '2013-08-06 15:44:20.883276', '2013-08-06 15:44:27.190784', '10.0.1.175', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (548, 'X5201521b5da50813000000006be7822400000000', 9, '2013-08-06 15:44:27.190784', '2013-08-06 15:44:30.334287', '10.0.1.175', 'spamfilter', NULL, NULL);
INSERT INTO tokens VALUES (549, 'X5201521e133cc80000000000414e173900000000', 9, '2013-08-06 15:44:30.334287', '2013-08-06 15:44:33.041994', '10.0.1.175', 'adqueues', NULL, NULL);
INSERT INTO tokens VALUES (550, 'X5201522161d8b9c40000000073b016de00000000', 9, '2013-08-06 15:44:33.041994', '2013-08-06 15:44:33.065099', '10.0.1.175', 'adqueues', NULL, NULL);
INSERT INTO tokens VALUES (551, 'X52015221783fca1c0000000035f851cf00000000', 9, '2013-08-06 15:44:33.065099', '2013-08-06 15:44:33.067107', '10.0.1.175', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (552, 'X5201522113b54031000000006619a59700000000', 9, '2013-08-06 15:44:33.067107', '2013-08-06 15:44:33.06959', '10.0.1.175', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (553, 'X52015221263be21500000000b9148e700000000', 9, '2013-08-06 15:44:33.06959', '2013-08-06 15:44:33.075202', '10.0.1.175', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (554, 'X520152211342e5f000000006fe2431900000000', 9, '2013-08-06 15:44:33.075202', '2013-08-06 15:44:33.077063', '10.0.1.175', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (555, 'X52015221dcd535600000000e6cee1100000000', 9, '2013-08-06 15:44:33.077063', '2013-08-06 15:44:33.079009', '10.0.1.175', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (556, 'X5201522133d84467000000001f8f618000000000', 9, '2013-08-06 15:44:33.079009', '2013-08-06 16:44:33.079009', '10.0.1.175', 'block_list', NULL, NULL);


--
-- Data for Name: action_states; Type: TABLE DATA; Schema: public; Owner: dany
--

INSERT INTO action_states VALUES (1, 1, 150, 'reg', 'initial', '2013-06-04 10:38:33.3138', '192.168.4.75', NULL);
INSERT INTO action_states VALUES (1, 1, 151, 'unverified', 'verify', '2013-06-04 10:38:33.315886', '192.168.4.75', NULL);
INSERT INTO action_states VALUES (1, 1, 152, 'pending_review', 'verify', '2013-06-04 10:38:33.31634', '192.168.4.75', NULL);
INSERT INTO action_states VALUES (1, 1, 153, 'locked', 'checkout', '2013-06-04 10:38:33.316524', '192.168.4.75', 99999992);
INSERT INTO action_states VALUES (1, 1, 154, 'accepted', 'accept', '2013-06-04 10:38:33.316761', '192.168.4.75', 99999993);
INSERT INTO action_states VALUES (2, 1, 3, 'reg', 'initial', '2013-06-04 10:38:33.316993', '192.168.4.85', NULL);
INSERT INTO action_states VALUES (2, 1, 4, 'unverified', 'verifymail', '2013-06-04 10:38:33.31718', '192.168.4.85', NULL);
INSERT INTO action_states VALUES (3, 1, 5, 'reg', 'initial', '2013-06-04 10:38:33.317386', '192.168.4.85', NULL);
INSERT INTO action_states VALUES (3, 1, 6, 'unpaid', 'paymail', '2013-06-04 10:38:33.317545', '192.168.4.85', NULL);
INSERT INTO action_states VALUES (4, 1, 7, 'reg', 'initial', '2013-06-04 10:38:33.317704', '192.168.4.85', NULL);
INSERT INTO action_states VALUES (4, 1, 8, 'unverified', 'verifymail', '2013-06-04 10:38:33.317854', '192.168.4.85', NULL);
INSERT INTO action_states VALUES (6, 1, 50, 'reg', 'initial', '2013-06-04 10:38:33.318042', '192.168.4.75', NULL);
INSERT INTO action_states VALUES (6, 1, 51, 'unpaid', 'newad', '2013-06-04 10:38:33.318209', '192.168.4.75', NULL);
INSERT INTO action_states VALUES (6, 1, 52, 'pending_review', 'pay', '2013-06-04 10:38:33.318395', '192.168.4.75', 99999998);
INSERT INTO action_states VALUES (6, 1, 53, 'accepted', 'accept', '2013-06-04 10:38:33.318586', '192.168.4.75', 99999999);
INSERT INTO action_states VALUES (9, 1, 40, 'reg', 'initial', '2013-06-04 10:38:33.318768', '192.168.4.75', NULL);
INSERT INTO action_states VALUES (9, 1, 41, 'unpaid', 'newad', '2013-06-04 10:38:33.318922', '192.168.4.75', NULL);
INSERT INTO action_states VALUES (9, 1, 42, 'pending_review', 'pay', '2013-06-04 10:38:33.319096', '192.168.4.75', 99999918);
INSERT INTO action_states VALUES (9, 1, 43, 'refused', 'refuse', NOW() - interval '1 month', '192.168.4.75', 99999919);
INSERT INTO action_states VALUES (12, 1, 160, 'reg', 'initial', '2013-06-04 10:38:33.319445', '192.168.4.75', NULL);
INSERT INTO action_states VALUES (12, 1, 161, 'unverified', 'verify', '2013-06-04 10:38:33.319618', '192.168.4.75', NULL);
INSERT INTO action_states VALUES (12, 1, 162, 'pending_review', 'verify', '2013-06-04 10:38:33.319786', '192.168.4.75', NULL);
INSERT INTO action_states VALUES (12, 1, 163, 'locked', 'checkout', '2013-06-04 10:38:33.319937', '192.168.4.75', 99999994);
INSERT INTO action_states VALUES (12, 1, 164, 'accepted', 'accept', '2013-06-04 10:38:33.320114', '192.168.4.75', 99999995);
INSERT INTO action_states VALUES (10, 1, 54, 'reg', 'initial', '2013-06-04 11:20:00', '192.168.4.75', NULL);
INSERT INTO action_states VALUES (10, 1, 55, 'unpaid', 'newad', '2013-06-04 10:38:33.320915', '192.168.4.75', NULL);
INSERT INTO action_states VALUES (10, 1, 56, 'pending_review', 'pay', '2013-06-04 10:38:33.321104', '192.168.4.75', 99999998);
INSERT INTO action_states VALUES (10, 1, 57, 'accepted', 'accept', '2013-06-04 10:38:33.321268', '192.168.4.75', 99999999);
INSERT INTO action_states VALUES (20, 1, 200, 'reg', 'initial', '2013-06-04 10:41:31.829948', '10.0.1.173', NULL);
INSERT INTO action_states VALUES (20, 1, 201, 'unverified', 'verifymail', '2013-06-04 10:41:31.829948', '10.0.1.173', NULL);
INSERT INTO action_states VALUES (20, 1, 202, 'pending_review', 'verify', '2013-06-04 10:41:31.85624', '10.0.1.173', NULL);
INSERT INTO action_states VALUES (21, 1, 203, 'reg', 'initial', '2013-06-04 10:45:36.89995', '10.0.1.173', NULL);
INSERT INTO action_states VALUES (21, 1, 204, 'unverified', 'verifymail', '2013-06-04 10:45:36.89995', '10.0.1.173', NULL);
INSERT INTO action_states VALUES (21, 1, 205, 'pending_review', 'verify', '2013-06-04 10:45:36.923504', '10.0.1.173', NULL);
INSERT INTO action_states VALUES (22, 1, 206, 'reg', 'initial', '2013-06-04 10:46:56.886233', '10.0.1.173', NULL);
INSERT INTO action_states VALUES (22, 1, 207, 'unverified', 'verifymail', '2013-06-04 10:46:56.886233', '10.0.1.173', NULL);
INSERT INTO action_states VALUES (22, 1, 208, 'pending_review', 'verify', '2013-06-04 10:46:56.892366', '10.0.1.173', NULL);
INSERT INTO action_states VALUES (23, 1, 209, 'reg', 'initial', '2013-06-04 10:48:27.488435', '10.0.1.173', NULL);
INSERT INTO action_states VALUES (23, 1, 210, 'unverified', 'verifymail', '2013-06-04 10:48:27.488435', '10.0.1.173', NULL);
INSERT INTO action_states VALUES (23, 1, 211, 'pending_review', 'verify', '2013-06-04 10:48:27.511447', '10.0.1.173', NULL);
INSERT INTO action_states VALUES (24, 1, 212, 'reg', 'initial', '2013-06-04 10:50:16.097755', '10.0.1.173', NULL);
INSERT INTO action_states VALUES (24, 1, 213, 'unverified', 'verifymail', '2013-06-04 10:50:16.097755', '10.0.1.173', NULL);
INSERT INTO action_states VALUES (24, 1, 214, 'pending_review', 'verify', '2013-06-04 10:50:16.121221', '10.0.1.173', NULL);
INSERT INTO action_states VALUES (25, 1, 215, 'reg', 'initial', '2013-06-04 10:51:33.747232', '10.0.1.173', NULL);
INSERT INTO action_states VALUES (25, 1, 216, 'unverified', 'verifymail', '2013-06-04 10:51:33.747232', '10.0.1.173', NULL);
INSERT INTO action_states VALUES (25, 1, 217, 'pending_review', 'verify', '2013-06-04 10:51:33.770616', '10.0.1.173', NULL);
INSERT INTO action_states VALUES (20, 1, 218, 'locked', 'checkout', '2013-06-04 10:51:47.42484', '10.0.1.173', 3);
INSERT INTO action_states VALUES (21, 1, 219, 'locked', 'checkout', '2013-06-04 10:51:47.42484', '10.0.1.173', 3);
INSERT INTO action_states VALUES (20, 1, 220, 'accepted', 'accept', '2013-06-04 10:51:58.847484', '10.0.1.173', 10);
INSERT INTO action_states VALUES (21, 1, 221, 'accepted', 'accept', '2013-06-04 10:52:03.060447', '10.0.1.173', 11);
INSERT INTO action_states VALUES (22, 1, 222, 'locked', 'checkout', '2013-06-04 10:52:04.632691', '10.0.1.173', 12);
INSERT INTO action_states VALUES (23, 1, 223, 'locked', 'checkout', '2013-06-04 10:52:04.632691', '10.0.1.173', 12);
INSERT INTO action_states VALUES (22, 1, 224, 'accepted', 'accept', '2013-06-04 10:52:10.494763', '10.0.1.173', 19);
INSERT INTO action_states VALUES (23, 1, 225, 'accepted', 'accept', '2013-06-04 10:52:12.983778', '10.0.1.173', 20);
INSERT INTO action_states VALUES (24, 1, 226, 'locked', 'checkout', '2013-06-04 10:52:14.866453', '10.0.1.173', 21);
INSERT INTO action_states VALUES (25, 1, 227, 'locked', 'checkout', '2013-06-04 10:52:14.866453', '10.0.1.173', 21);
INSERT INTO action_states VALUES (24, 1, 228, 'accepted', 'accept', '2013-06-04 10:52:19.031284', '10.0.1.173', 28);
INSERT INTO action_states VALUES (25, 1, 229, 'accepted', 'accept', NOW() - interval '1 month', '10.0.1.173', 29);
INSERT INTO action_states VALUES (26, 1, 230, 'reg', 'initial', '2013-06-04 10:55:01.248764', '10.0.1.173', NULL);
INSERT INTO action_states VALUES (26, 1, 231, 'unverified', 'verifymail', '2013-06-04 10:55:01.248764', '10.0.1.173', NULL);
INSERT INTO action_states VALUES (26, 1, 232, 'pending_review', 'verify', '2013-06-04 10:55:01.264908', '10.0.1.173', NULL);
INSERT INTO action_states VALUES (27, 1, 233, 'reg', 'initial', '2013-06-04 10:58:10.031048', '10.0.1.173', NULL);
INSERT INTO action_states VALUES (27, 1, 234, 'unverified', 'verifymail', '2013-06-04 10:58:10.031048', '10.0.1.173', NULL);
INSERT INTO action_states VALUES (27, 1, 235, 'pending_review', 'verify', '2013-06-04 10:58:10.048996', '10.0.1.173', NULL);
INSERT INTO action_states VALUES (28, 1, 236, 'reg', 'initial', '2013-06-04 10:59:39.870471', '10.0.1.173', NULL);
INSERT INTO action_states VALUES (28, 1, 237, 'unverified', 'verifymail', '2013-06-04 10:59:39.870471', '10.0.1.173', NULL);
INSERT INTO action_states VALUES (28, 1, 238, 'pending_review', 'verify', '2013-06-04 10:59:39.888685', '10.0.1.173', NULL);
INSERT INTO action_states VALUES (29, 1, 239, 'reg', 'initial', '2013-06-04 11:01:06.897025', '10.0.1.173', NULL);
INSERT INTO action_states VALUES (29, 1, 240, 'unverified', 'verifymail', '2013-06-04 11:01:06.897025', '10.0.1.173', NULL);
INSERT INTO action_states VALUES (29, 1, 241, 'pending_review', 'verify', '2013-06-04 11:01:06.914734', '10.0.1.173', NULL);
INSERT INTO action_states VALUES (30, 1, 242, 'reg', 'initial', '2013-06-04 11:03:03.085742', '10.0.1.173', NULL);
INSERT INTO action_states VALUES (30, 1, 243, 'unverified', 'verifymail', '2013-06-04 11:03:03.085742', '10.0.1.173', NULL);
INSERT INTO action_states VALUES (30, 1, 244, 'pending_review', 'verify', '2013-06-04 11:03:03.109549', '10.0.1.173', NULL);
INSERT INTO action_states VALUES (26, 1, 245, 'locked', 'checkout', '2013-06-04 11:03:09.546316', '10.0.1.173', 32);
INSERT INTO action_states VALUES (27, 1, 246, 'locked', 'checkout', '2013-06-04 11:03:09.546316', '10.0.1.173', 32);
INSERT INTO action_states VALUES (26, 1, 247, 'accepted', 'accept', '2013-06-04 11:03:15.132247', '10.0.1.173', 39);
INSERT INTO action_states VALUES (27, 1, 248, 'accepted', 'accept', '2013-06-04 11:03:18.293219', '10.0.1.173', 40);
INSERT INTO action_states VALUES (28, 1, 249, 'locked', 'checkout', '2013-06-04 11:03:19.908236', '10.0.1.173', 41);
INSERT INTO action_states VALUES (29, 1, 250, 'locked', 'checkout', '2013-06-04 11:03:19.908236', '10.0.1.173', 41);
INSERT INTO action_states VALUES (28, 1, 251, 'accepted', 'accept', '2013-06-04 11:03:23.770424', '10.0.1.173', 48);
INSERT INTO action_states VALUES (29, 1, 252, 'accepted', 'accept', '2013-06-04 11:03:26.585804', '10.0.1.173', 49);
INSERT INTO action_states VALUES (30, 1, 253, 'locked', 'checkout', '2013-06-04 11:03:27.864857', '10.0.1.173', 50);
INSERT INTO action_states VALUES (30, 1, 254, 'accepted', 'accept', '2013-06-04 11:03:31.075334', '10.0.1.173', 54);
INSERT INTO action_states VALUES (31, 1, 255, 'reg', 'initial', '2013-06-04 11:20:25.769106', '10.0.1.138', NULL);
INSERT INTO action_states VALUES (31, 1, 256, 'unverified', 'verifymail', '2013-06-04 11:20:25.769106', '10.0.1.138', NULL);
INSERT INTO action_states VALUES (31, 1, 257, 'pending_review', 'verify', '2013-06-04 11:20:25.793221', '10.0.1.138', NULL);
INSERT INTO action_states VALUES (32, 1, 258, 'reg', 'initial', '2013-06-04 11:21:46.226704', '10.0.1.138', NULL);
INSERT INTO action_states VALUES (32, 1, 259, 'unverified', 'verifymail', '2013-06-04 11:21:46.226704', '10.0.1.138', NULL);
INSERT INTO action_states VALUES (32, 1, 260, 'pending_review', 'verify', '2013-06-04 11:21:46.244289', '10.0.1.138', NULL);
INSERT INTO action_states VALUES (33, 1, 261, 'reg', 'initial', '2013-06-04 11:23:40.532448', '10.0.1.138', NULL);
INSERT INTO action_states VALUES (33, 1, 262, 'unverified', 'verifymail', '2013-06-04 11:23:40.532448', '10.0.1.138', NULL);
INSERT INTO action_states VALUES (33, 1, 263, 'pending_review', 'verify', '2013-06-04 11:23:40.562334', '10.0.1.138', NULL);
INSERT INTO action_states VALUES (31, 1, 264, 'locked', 'checkout', '2013-06-04 11:31:58.972477', '10.0.1.173', 60);
INSERT INTO action_states VALUES (32, 1, 265, 'locked', 'checkout', '2013-06-04 11:31:58.972477', '10.0.1.173', 60);
INSERT INTO action_states VALUES (34, 1, 266, 'reg', 'initial', '2013-06-04 11:32:13.874344', '10.0.1.138', NULL);
INSERT INTO action_states VALUES (34, 1, 267, 'unverified', 'verifymail', '2013-06-04 11:32:13.874344', '10.0.1.138', NULL);
INSERT INTO action_states VALUES (34, 1, 268, 'pending_review', 'verify', '2013-06-04 11:32:13.885879', '10.0.1.138', NULL);
INSERT INTO action_states VALUES (35, 1, 269, 'reg', 'initial', '2013-06-04 11:34:44.554944', '10.0.1.173', NULL);
INSERT INTO action_states VALUES (35, 1, 270, 'unverified', 'verifymail', '2013-06-04 11:34:44.554944', '10.0.1.173', NULL);
INSERT INTO action_states VALUES (35, 1, 271, 'pending_review', 'verify', '2013-06-04 11:34:44.574045', '10.0.1.173', NULL);
INSERT INTO action_states VALUES (36, 1, 272, 'reg', 'initial', '2013-06-04 11:35:11.862084', '10.0.1.138', NULL);
INSERT INTO action_states VALUES (36, 1, 273, 'unverified', 'verifymail', '2013-06-04 11:35:11.862084', '10.0.1.138', NULL);
INSERT INTO action_states VALUES (36, 1, 274, 'pending_review', 'verify', '2013-06-04 11:35:11.876785', '10.0.1.138', NULL);
INSERT INTO action_states VALUES (37, 1, 275, 'reg', 'initial', '2013-06-04 11:36:18.949759', '10.0.1.138', NULL);
INSERT INTO action_states VALUES (37, 1, 276, 'unverified', 'verifymail', '2013-06-04 11:36:18.949759', '10.0.1.138', NULL);
INSERT INTO action_states VALUES (37, 1, 277, 'pending_review', 'verify', '2013-06-04 11:36:18.95672', '10.0.1.138', NULL);
INSERT INTO action_states VALUES (38, 1, 278, 'reg', 'initial', '2013-06-04 11:37:20.86608', '10.0.1.173', NULL);
INSERT INTO action_states VALUES (38, 1, 279, 'unverified', 'verifymail', '2013-06-04 11:37:20.86608', '10.0.1.173', NULL);
INSERT INTO action_states VALUES (38, 1, 280, 'pending_review', 'verify', '2013-06-04 11:37:20.873266', '10.0.1.173', NULL);
INSERT INTO action_states VALUES (31, 1, 281, 'locked', 'checkout', '2013-06-04 11:38:45.399338', '10.0.1.138', 70);
INSERT INTO action_states VALUES (32, 1, 282, 'locked', 'checkout', '2013-06-04 11:38:45.399338', '10.0.1.138', 70);
INSERT INTO action_states VALUES (32, 1, 283, 'pending_review', 'checkin', '2013-06-04 11:38:58.76451', '10.0.1.138', 77);
INSERT INTO action_states VALUES (39, 1, 284, 'reg', 'initial', '2013-06-04 11:39:02.979047', '10.0.1.173', NULL);
INSERT INTO action_states VALUES (39, 1, 285, 'unverified', 'verifymail', '2013-06-04 11:39:02.979047', '10.0.1.173', NULL);
INSERT INTO action_states VALUES (39, 1, 286, 'pending_review', 'verify', '2013-06-04 11:39:02.995565', '10.0.1.173', NULL);
INSERT INTO action_states VALUES (32, 1, 287, 'locked', 'checkout', '2013-06-04 11:39:03.802708', '10.0.1.138', 78);
INSERT INTO action_states VALUES (33, 1, 288, 'locked', 'checkout', '2013-06-04 11:39:03.802708', '10.0.1.138', 78);
INSERT INTO action_states VALUES (34, 1, 289, 'locked', 'checkout', '2013-06-04 11:39:06.289955', '10.0.1.138', 85);
INSERT INTO action_states VALUES (35, 1, 290, 'locked', 'checkout', '2013-06-04 11:39:06.289955', '10.0.1.138', 85);
INSERT INTO action_states VALUES (36, 1, 291, 'locked', 'checkout', '2013-06-04 11:39:08.995533', '10.0.1.138', 92);
INSERT INTO action_states VALUES (37, 1, 292, 'locked', 'checkout', '2013-06-04 11:39:08.995533', '10.0.1.138', 92);
INSERT INTO action_states VALUES (38, 1, 293, 'locked', 'checkout', '2013-06-04 11:39:12.668836', '10.0.1.138', 99);
INSERT INTO action_states VALUES (39, 1, 294, 'locked', 'checkout', '2013-06-04 11:39:12.668836', '10.0.1.138', 99);
INSERT INTO action_states VALUES (31, 1, 295, 'accepted', 'accept', '2013-06-04 11:39:25.182244', '10.0.1.173', 116);
INSERT INTO action_states VALUES (32, 1, 296, 'accepted', 'accept', '2013-06-04 11:39:29.80389', '10.0.1.173', 117);
INSERT INTO action_states VALUES (35, 1, 297, 'accepted', 'accept', '2013-06-04 11:39:37.380686', '10.0.1.173', 125);
INSERT INTO action_states VALUES (36, 1, 298, 'accepted', 'accept', '2013-06-04 11:39:40.617107', '10.0.1.173', 126);
INSERT INTO action_states VALUES (39, 1, 299, 'accepted', 'accept', '2013-06-04 11:39:47.165872', '10.0.1.173', 131);
INSERT INTO action_states VALUES (37, 1, 300, 'accepted', 'accept', '2013-06-04 11:39:54.980674', '10.0.1.173', 139);
INSERT INTO action_states VALUES (38, 1, 301, 'accepted', 'accept', '2013-06-04 11:39:58.627429', '10.0.1.173', 140);
INSERT INTO action_states VALUES (33, 1, 302, 'accepted', 'accept', '2013-06-04 11:40:05.443704', '10.0.1.173', 148);
INSERT INTO action_states VALUES (34, 1, 303, 'accepted', 'accept', '2013-06-04 11:40:09.295412', '10.0.1.173', 149);
INSERT INTO action_states VALUES (40, 1, 304, 'reg', 'initial', '2013-06-04 11:52:05.993009', '10.0.1.173', NULL);
INSERT INTO action_states VALUES (40, 1, 305, 'unverified', 'verifymail', '2013-06-04 11:52:05.993009', '10.0.1.173', NULL);
INSERT INTO action_states VALUES (40, 1, 306, 'pending_review', 'verify', '2013-06-04 11:52:06.011962', '10.0.1.173', NULL);
INSERT INTO action_states VALUES (41, 1, 307, 'reg', 'initial', '2013-06-04 11:53:08.606456', '10.0.1.173', NULL);
INSERT INTO action_states VALUES (41, 1, 308, 'unverified', 'verifymail', '2013-06-04 11:53:08.606456', '10.0.1.173', NULL);
INSERT INTO action_states VALUES (41, 1, 309, 'pending_review', 'verify', '2013-06-04 11:53:08.610431', '10.0.1.173', NULL);
INSERT INTO action_states VALUES (42, 1, 310, 'reg', 'initial', '2013-06-04 11:53:13.823383', '10.0.1.138', NULL);
INSERT INTO action_states VALUES (42, 1, 311, 'unverified', 'verifymail', '2013-06-04 11:53:13.823383', '10.0.1.138', NULL);
INSERT INTO action_states VALUES (42, 1, 312, 'pending_review', 'verify', '2013-06-04 11:53:13.831339', '10.0.1.138', NULL);
INSERT INTO action_states VALUES (43, 1, 313, 'reg', 'initial', '2013-06-04 11:53:54.773615', '10.0.1.138', NULL);
INSERT INTO action_states VALUES (43, 1, 314, 'unverified', 'verifymail', '2013-06-04 11:53:54.773615', '10.0.1.138', NULL);
INSERT INTO action_states VALUES (43, 1, 315, 'pending_review', 'verify', '2013-06-04 11:53:54.779322', '10.0.1.138', NULL);
INSERT INTO action_states VALUES (44, 1, 316, 'reg', 'initial', '2013-06-04 11:54:21.822676', '10.0.1.173', NULL);
INSERT INTO action_states VALUES (44, 1, 317, 'unverified', 'verifymail', '2013-06-04 11:54:21.822676', '10.0.1.173', NULL);
INSERT INTO action_states VALUES (44, 1, 318, 'pending_review', 'verify', '2013-06-04 11:54:21.827127', '10.0.1.173', NULL);
INSERT INTO action_states VALUES (45, 1, 319, 'reg', 'initial', '2013-06-04 11:54:53.034857', '10.0.1.138', NULL);
INSERT INTO action_states VALUES (45, 1, 320, 'unverified', 'verifymail', '2013-06-04 11:54:53.034857', '10.0.1.138', NULL);
INSERT INTO action_states VALUES (45, 1, 321, 'pending_review', 'verify', '2013-06-04 11:54:53.039606', '10.0.1.138', NULL);
INSERT INTO action_states VALUES (46, 1, 322, 'reg', 'initial', '2013-06-04 11:55:40.055155', '10.0.1.173', NULL);
INSERT INTO action_states VALUES (46, 1, 323, 'unverified', 'verifymail', '2013-06-04 11:55:40.055155', '10.0.1.173', NULL);
INSERT INTO action_states VALUES (46, 1, 324, 'pending_review', 'verify', '2013-06-04 11:55:40.059334', '10.0.1.173', NULL);
INSERT INTO action_states VALUES (47, 1, 325, 'reg', 'initial', '2013-06-04 11:55:59.631308', '10.0.1.138', NULL);
INSERT INTO action_states VALUES (47, 1, 326, 'unverified', 'verifymail', '2013-06-04 11:55:59.631308', '10.0.1.138', NULL);
INSERT INTO action_states VALUES (47, 1, 327, 'pending_review', 'verify', '2013-06-04 11:55:59.637708', '10.0.1.138', NULL);
INSERT INTO action_states VALUES (48, 1, 328, 'reg', 'initial', '2013-06-04 11:56:15.422455', '10.0.1.173', NULL);
INSERT INTO action_states VALUES (48, 1, 329, 'unverified', 'verifymail', '2013-06-04 11:56:15.422455', '10.0.1.173', NULL);
INSERT INTO action_states VALUES (48, 1, 330, 'pending_review', 'verify', '2013-06-04 11:56:15.42622', '10.0.1.173', NULL);
INSERT INTO action_states VALUES (49, 1, 331, 'reg', 'initial', '2013-06-04 11:56:39.543316', '10.0.1.138', NULL);
INSERT INTO action_states VALUES (49, 1, 332, 'unverified', 'verifymail', '2013-06-04 11:56:39.543316', '10.0.1.138', NULL);
INSERT INTO action_states VALUES (49, 1, 333, 'pending_review', 'verify', '2013-06-04 11:56:39.547582', '10.0.1.138', NULL);
INSERT INTO action_states VALUES (50, 1, 334, 'reg', 'initial', '2013-06-04 11:57:00.493136', '10.0.1.173', NULL);
INSERT INTO action_states VALUES (50, 1, 335, 'unverified', 'verifymail', '2013-06-04 11:57:00.493136', '10.0.1.173', NULL);
INSERT INTO action_states VALUES (50, 1, 336, 'pending_review', 'verify', '2013-06-04 11:57:00.497706', '10.0.1.173', NULL);
INSERT INTO action_states VALUES (51, 1, 337, 'reg', 'initial', '2013-06-04 11:57:12.480431', '10.0.1.138', NULL);
INSERT INTO action_states VALUES (51, 1, 338, 'unverified', 'verifymail', '2013-06-04 11:57:12.480431', '10.0.1.138', NULL);
INSERT INTO action_states VALUES (51, 1, 339, 'pending_review', 'verify', '2013-06-04 11:57:12.48441', '10.0.1.138', NULL);
INSERT INTO action_states VALUES (52, 1, 340, 'reg', 'initial', '2013-06-04 11:57:54.844198', '10.0.1.173', NULL);
INSERT INTO action_states VALUES (52, 1, 341, 'unverified', 'verifymail', '2013-06-04 11:57:54.844198', '10.0.1.173', NULL);
INSERT INTO action_states VALUES (52, 1, 342, 'pending_review', 'verify', '2013-06-04 11:57:54.848701', '10.0.1.173', NULL);
INSERT INTO action_states VALUES (53, 1, 343, 'reg', 'initial', '2013-06-04 11:58:19.445929', '10.0.1.138', NULL);
INSERT INTO action_states VALUES (53, 1, 344, 'unverified', 'verifymail', '2013-06-04 11:58:19.445929', '10.0.1.138', NULL);
INSERT INTO action_states VALUES (53, 1, 345, 'pending_review', 'verify', '2013-06-04 11:58:19.449736', '10.0.1.138', NULL);
INSERT INTO action_states VALUES (54, 1, 346, 'reg', 'initial', '2013-06-04 11:58:32.444333', '10.0.1.173', NULL);
INSERT INTO action_states VALUES (54, 1, 347, 'unverified', 'verifymail', '2013-06-04 11:58:32.444333', '10.0.1.173', NULL);
INSERT INTO action_states VALUES (54, 1, 348, 'pending_review', 'verify', '2013-06-04 11:58:32.449925', '10.0.1.173', NULL);
INSERT INTO action_states VALUES (40, 1, 349, 'locked', 'checkout', '2013-06-04 11:58:43.505048', '10.0.1.173', 152);
INSERT INTO action_states VALUES (41, 1, 350, 'locked', 'checkout', '2013-06-04 11:58:43.505048', '10.0.1.173', 152);
INSERT INTO action_states VALUES (40, 1, 351, 'accepted', 'accept', '2013-06-04 11:58:48.056116', '10.0.1.173', 159);
INSERT INTO action_states VALUES (41, 1, 352, 'accepted', 'accept', '2013-06-04 11:58:51.896416', '10.0.1.173', 160);
INSERT INTO action_states VALUES (42, 1, 353, 'locked', 'checkout', '2013-06-04 11:58:53.862787', '10.0.1.173', 161);
INSERT INTO action_states VALUES (43, 1, 354, 'locked', 'checkout', '2013-06-04 11:58:53.862787', '10.0.1.173', 161);
INSERT INTO action_states VALUES (55, 1, 355, 'reg', 'initial', '2013-06-04 11:58:56.208915', '10.0.1.138', NULL);
INSERT INTO action_states VALUES (55, 1, 356, 'unverified', 'verifymail', '2013-06-04 11:58:56.208915', '10.0.1.138', NULL);
INSERT INTO action_states VALUES (55, 1, 357, 'pending_review', 'verify', '2013-06-04 11:58:56.215115', '10.0.1.138', NULL);
INSERT INTO action_states VALUES (42, 1, 358, 'accepted', 'accept', '2013-06-04 11:59:01.64359', '10.0.1.173', 168);
INSERT INTO action_states VALUES (43, 1, 359, 'accepted', 'accept', '2013-06-04 11:59:05.848706', '10.0.1.173', 169);
INSERT INTO action_states VALUES (44, 1, 360, 'locked', 'checkout', '2013-06-04 11:59:07.852271', '10.0.1.173', 170);
INSERT INTO action_states VALUES (45, 1, 361, 'locked', 'checkout', '2013-06-04 11:59:07.852271', '10.0.1.173', 170);
INSERT INTO action_states VALUES (44, 1, 362, 'accepted', 'accept', '2013-06-04 11:59:12.342116', '10.0.1.173', 177);
INSERT INTO action_states VALUES (45, 1, 363, 'accepted', 'accept', '2013-06-04 11:59:17.737615', '10.0.1.173', 178);
INSERT INTO action_states VALUES (46, 1, 364, 'locked', 'checkout', '2013-06-04 11:59:19.400463', '10.0.1.173', 179);
INSERT INTO action_states VALUES (47, 1, 365, 'locked', 'checkout', '2013-06-04 11:59:19.400463', '10.0.1.173', 179);
INSERT INTO action_states VALUES (46, 1, 366, 'accepted', 'accept', '2013-06-04 11:59:23.717305', '10.0.1.173', 186);
INSERT INTO action_states VALUES (47, 1, 367, 'accepted', 'accept', '2013-06-04 11:59:26.342338', '10.0.1.173', 187);
INSERT INTO action_states VALUES (48, 1, 368, 'locked', 'checkout', '2013-06-04 11:59:27.537222', '10.0.1.173', 188);
INSERT INTO action_states VALUES (49, 1, 369, 'locked', 'checkout', '2013-06-04 11:59:27.537222', '10.0.1.173', 188);
INSERT INTO action_states VALUES (56, 1, 370, 'reg', 'initial', '2013-06-04 11:59:30.351197', '10.0.1.138', NULL);
INSERT INTO action_states VALUES (56, 1, 371, 'unverified', 'verifymail', '2013-06-04 11:59:30.351197', '10.0.1.138', NULL);
INSERT INTO action_states VALUES (56, 1, 372, 'pending_review', 'verify', '2013-06-04 11:59:30.358144', '10.0.1.138', NULL);
INSERT INTO action_states VALUES (48, 1, 373, 'accepted', 'accept', '2013-06-04 11:59:31.159284', '10.0.1.173', 195);
INSERT INTO action_states VALUES (49, 1, 374, 'accepted', 'accept', '2013-06-04 11:59:35.454628', '10.0.1.173', 196);
INSERT INTO action_states VALUES (50, 1, 375, 'locked', 'checkout', '2013-06-04 11:59:39.576395', '10.0.1.173', 197);
INSERT INTO action_states VALUES (51, 1, 376, 'locked', 'checkout', '2013-06-04 11:59:39.576395', '10.0.1.173', 197);
INSERT INTO action_states VALUES (50, 1, 377, 'accepted', 'accept', '2013-06-04 11:59:43.225016', '10.0.1.173', 204);
INSERT INTO action_states VALUES (51, 1, 378, 'accepted', 'accept', '2013-06-04 11:59:46.011444', '10.0.1.173', 205);
INSERT INTO action_states VALUES (52, 1, 379, 'locked', 'checkout', '2013-06-04 11:59:47.261721', '10.0.1.173', 206);
INSERT INTO action_states VALUES (53, 1, 380, 'locked', 'checkout', '2013-06-04 11:59:47.261721', '10.0.1.173', 206);
INSERT INTO action_states VALUES (52, 1, 381, 'accepted', 'accept', '2013-06-04 11:59:50.034908', '10.0.1.173', 213);
INSERT INTO action_states VALUES (53, 1, 382, 'accepted', 'accept', '2013-06-04 11:59:53.36316', '10.0.1.173', 214);
INSERT INTO action_states VALUES (54, 1, 383, 'locked', 'checkout', '2013-06-04 11:59:55.113344', '10.0.1.173', 215);
INSERT INTO action_states VALUES (55, 1, 384, 'locked', 'checkout', '2013-06-04 11:59:55.113344', '10.0.1.173', 215);
INSERT INTO action_states VALUES (54, 1, 385, 'accepted', 'accept', '2013-06-04 11:59:58.805189', '10.0.1.173', 222);
INSERT INTO action_states VALUES (56, 1, 387, 'locked', 'checkout', '2013-06-04 12:00:13.537556', '10.0.1.173', 224);
INSERT INTO action_states VALUES (57, 1, 388, 'reg', 'initial', '2013-06-04 12:00:14.014412', '10.0.1.138', NULL);
INSERT INTO action_states VALUES (57, 1, 389, 'unverified', 'verifymail', '2013-06-04 12:00:14.014412', '10.0.1.138', NULL);
INSERT INTO action_states VALUES (57, 1, 390, 'pending_review', 'verify', '2013-06-04 12:00:14.019137', '10.0.1.138', NULL);
INSERT INTO action_states VALUES (57, 1, 392, 'locked', 'checkout', '2013-06-04 12:00:19.353946', '10.0.1.173', 229);
INSERT INTO action_states VALUES (57, 1, 393, 'accepted', 'accept', '2013-06-04 12:00:33.275949', '10.0.1.173', 233);
INSERT INTO action_states VALUES (55, 1, 386, 'accepted', 'accept', '2013-06-04 12:00:08.524804', '10.0.1.173', 223);
INSERT INTO action_states VALUES (56, 1, 391, 'accepted', 'accept', '2013-06-04 12:00:17.586168', '10.0.1.173', 228);
INSERT INTO action_states VALUES (58, 1, 394, 'reg', 'initial', '2013-06-04 12:06:49.933293', '10.0.1.138', NULL);
INSERT INTO action_states VALUES (58, 1, 395, 'unverified', 'verifymail', '2013-06-04 12:06:49.933293', '10.0.1.138', NULL);
INSERT INTO action_states VALUES (58, 1, 396, 'pending_review', 'verify', '2013-06-04 12:06:49.951026', '10.0.1.138', NULL);
INSERT INTO action_states VALUES (59, 1, 397, 'reg', 'initial', '2013-06-04 12:06:51.505695', '10.0.1.173', NULL);
INSERT INTO action_states VALUES (59, 1, 398, 'unverified', 'verifymail', '2013-06-04 12:06:51.505695', '10.0.1.173', NULL);
INSERT INTO action_states VALUES (59, 1, 399, 'pending_review', 'verify', '2013-06-04 12:06:51.517471', '10.0.1.173', NULL);
INSERT INTO action_states VALUES (60, 1, 400, 'reg', 'initial', '2013-06-04 12:07:42.721584', '10.0.1.173', NULL);
INSERT INTO action_states VALUES (60, 1, 401, 'unverified', 'verifymail', '2013-06-04 12:07:42.721584', '10.0.1.173', NULL);
INSERT INTO action_states VALUES (60, 1, 402, 'pending_review', 'verify', '2013-06-04 12:07:42.726511', '10.0.1.173', NULL);
INSERT INTO action_states VALUES (61, 1, 403, 'reg', 'initial', '2013-06-04 12:10:01.595201', '10.0.1.138', NULL);
INSERT INTO action_states VALUES (61, 1, 404, 'unverified', 'verifymail', '2013-06-04 12:10:01.595201', '10.0.1.138', NULL);
INSERT INTO action_states VALUES (61, 1, 405, 'pending_review', 'verify', '2013-06-04 12:10:01.620147', '10.0.1.138', NULL);
INSERT INTO action_states VALUES (62, 1, 406, 'reg', 'initial', '2013-06-04 12:10:55.070333', '10.0.1.138', NULL);
INSERT INTO action_states VALUES (62, 1, 407, 'unverified', 'verifymail', '2013-06-04 12:10:55.070333', '10.0.1.138', NULL);
INSERT INTO action_states VALUES (62, 1, 408, 'pending_review', 'verify', '2013-06-04 12:10:55.081796', '10.0.1.138', NULL);
INSERT INTO action_states VALUES (58, 1, 409, 'locked', 'checkout', '2013-06-04 12:11:11.320805', '10.0.1.138', 239);
INSERT INTO action_states VALUES (59, 1, 410, 'locked', 'checkout', '2013-06-04 12:11:11.320805', '10.0.1.138', 239);
INSERT INTO action_states VALUES (58, 1, 411, 'accepted', 'accept', '2013-06-04 12:11:18.709302', '10.0.1.138', 246);
INSERT INTO action_states VALUES (59, 1, 412, 'accepted', 'accept', '2013-06-04 12:11:21.872854', '10.0.1.138', 247);
INSERT INTO action_states VALUES (60, 1, 413, 'locked', 'checkout', '2013-06-04 12:11:23.386507', '10.0.1.138', 248);
INSERT INTO action_states VALUES (61, 1, 414, 'locked', 'checkout', '2013-06-04 12:11:23.386507', '10.0.1.138', 248);
INSERT INTO action_states VALUES (60, 1, 415, 'accepted', 'accept', '2013-06-04 12:11:26.704452', '10.0.1.138', 255);
INSERT INTO action_states VALUES (61, 1, 416, 'accepted', 'accept', '2013-06-04 12:11:30.178703', '10.0.1.138', 256);
INSERT INTO action_states VALUES (62, 1, 417, 'locked', 'checkout', '2013-06-04 12:11:32.017175', '10.0.1.138', 257);
INSERT INTO action_states VALUES (62, 1, 418, 'accepted', 'accept', '2013-06-04 12:11:34.82732', '10.0.1.138', 261);
INSERT INTO action_states VALUES (63, 1, 419, 'reg', 'initial', '2013-06-04 12:13:22.259481', '10.0.1.138', NULL);
INSERT INTO action_states VALUES (63, 1, 420, 'unverified', 'verifymail', '2013-06-04 12:13:22.259481', '10.0.1.138', NULL);
INSERT INTO action_states VALUES (63, 1, 421, 'pending_review', 'verify', '2013-06-04 12:13:22.265967', '10.0.1.138', NULL);
INSERT INTO action_states VALUES (64, 1, 422, 'reg', 'initial', '2013-06-04 12:14:08.083916', '10.0.1.138', NULL);
INSERT INTO action_states VALUES (64, 1, 423, 'unverified', 'verifymail', '2013-06-04 12:14:08.083916', '10.0.1.138', NULL);
INSERT INTO action_states VALUES (64, 1, 424, 'pending_review', 'verify', '2013-06-04 12:14:08.091073', '10.0.1.138', NULL);
INSERT INTO action_states VALUES (65, 1, 425, 'reg', 'initial', '2013-06-04 12:14:57.405542', '10.0.1.138', NULL);
INSERT INTO action_states VALUES (65, 1, 426, 'unverified', 'verifymail', '2013-06-04 12:14:57.405542', '10.0.1.138', NULL);
INSERT INTO action_states VALUES (65, 1, 427, 'pending_review', 'verify', '2013-06-04 12:14:57.410852', '10.0.1.138', NULL);
INSERT INTO action_states VALUES (66, 1, 428, 'reg', 'initial', '2013-06-04 12:15:46.720656', '10.0.1.138', NULL);
INSERT INTO action_states VALUES (66, 1, 429, 'unverified', 'verifymail', '2013-06-04 12:15:46.720656', '10.0.1.138', NULL);
INSERT INTO action_states VALUES (66, 1, 430, 'pending_review', 'verify', '2013-06-04 12:15:46.726407', '10.0.1.138', NULL);
INSERT INTO action_states VALUES (67, 1, 431, 'reg', 'initial', '2013-06-04 12:18:25.410375', '10.0.1.138', NULL);
INSERT INTO action_states VALUES (67, 1, 432, 'unverified', 'verifymail', '2013-06-04 12:18:25.410375', '10.0.1.138', NULL);
INSERT INTO action_states VALUES (67, 1, 433, 'pending_review', 'verify', '2013-06-04 12:18:25.434027', '10.0.1.138', NULL);
INSERT INTO action_states VALUES (68, 1, 434, 'reg', 'initial', '2013-06-04 12:19:17.40622', '10.0.1.138', NULL);
INSERT INTO action_states VALUES (68, 1, 435, 'unverified', 'verifymail', '2013-06-04 12:19:17.40622', '10.0.1.138', NULL);
INSERT INTO action_states VALUES (68, 1, 436, 'pending_review', 'verify', '2013-06-04 12:19:17.418305', '10.0.1.138', NULL);
INSERT INTO action_states VALUES (69, 1, 437, 'reg', 'initial', '2013-06-04 12:20:12.431433', '10.0.1.138', NULL);
INSERT INTO action_states VALUES (69, 1, 438, 'unverified', 'verifymail', '2013-06-04 12:20:12.431433', '10.0.1.138', NULL);
INSERT INTO action_states VALUES (69, 1, 439, 'pending_review', 'verify', '2013-06-04 12:20:12.43704', '10.0.1.138', NULL);
INSERT INTO action_states VALUES (63, 1, 440, 'locked', 'checkout', '2013-06-04 12:20:22.53332', '10.0.1.138', 264);
INSERT INTO action_states VALUES (64, 1, 441, 'locked', 'checkout', '2013-06-04 12:20:22.53332', '10.0.1.138', 264);
INSERT INTO action_states VALUES (63, 1, 442, 'accepted', 'accept', '2013-06-04 12:20:30.972276', '10.0.1.138', 271);
INSERT INTO action_states VALUES (64, 1, 443, 'accepted', 'accept', '2013-06-04 12:20:34.121895', '10.0.1.138', 272);
INSERT INTO action_states VALUES (65, 1, 444, 'locked', 'checkout', '2013-06-04 12:20:35.126261', '10.0.1.138', 273);
INSERT INTO action_states VALUES (66, 1, 445, 'locked', 'checkout', '2013-06-04 12:20:35.126261', '10.0.1.138', 273);
INSERT INTO action_states VALUES (65, 1, 446, 'accepted', 'accept', '2013-06-04 12:20:39.108132', '10.0.1.138', 280);
INSERT INTO action_states VALUES (66, 1, 447, 'accepted', 'accept', '2013-06-04 12:20:42.041479', '10.0.1.138', 281);
INSERT INTO action_states VALUES (67, 1, 448, 'locked', 'checkout', '2013-06-04 12:20:43.410987', '10.0.1.138', 282);
INSERT INTO action_states VALUES (68, 1, 449, 'locked', 'checkout', '2013-06-04 12:20:43.410987', '10.0.1.138', 282);
INSERT INTO action_states VALUES (67, 1, 450, 'accepted', 'accept', '2013-06-04 12:20:46.755825', '10.0.1.138', 289);
INSERT INTO action_states VALUES (68, 1, 451, 'accepted', 'accept', '2013-06-04 12:20:49.002899', '10.0.1.138', 290);
INSERT INTO action_states VALUES (69, 1, 452, 'locked', 'checkout', '2013-06-04 12:20:50.694259', '10.0.1.138', 291);
INSERT INTO action_states VALUES (69, 1, 453, 'accepted', 'accept', '2013-06-04 12:20:54.280291', '10.0.1.138', 295);
INSERT INTO action_states VALUES (70, 1, 454, 'reg', 'initial', '2013-06-04 12:22:45.227628', '10.0.1.138', NULL);
INSERT INTO action_states VALUES (70, 1, 455, 'unverified', 'verifymail', '2013-06-04 12:22:45.227628', '10.0.1.138', NULL);
INSERT INTO action_states VALUES (70, 1, 456, 'pending_review', 'verify', '2013-06-04 12:22:45.232719', '10.0.1.138', NULL);
INSERT INTO action_states VALUES (71, 1, 457, 'reg', 'initial', '2013-06-04 12:23:33.113503', '10.0.1.138', NULL);
INSERT INTO action_states VALUES (71, 1, 458, 'unverified', 'verifymail', '2013-06-04 12:23:33.113503', '10.0.1.138', NULL);
INSERT INTO action_states VALUES (71, 1, 459, 'pending_review', 'verify', '2013-06-04 12:23:33.117867', '10.0.1.138', NULL);
INSERT INTO action_states VALUES (72, 1, 460, 'reg', 'initial', '2013-06-04 12:23:58.691985', '10.0.1.138', NULL);
INSERT INTO action_states VALUES (72, 1, 461, 'unverified', 'verifymail', '2013-06-04 12:23:58.691985', '10.0.1.138', NULL);
INSERT INTO action_states VALUES (72, 1, 462, 'pending_review', 'verify', '2013-06-04 12:23:58.699892', '10.0.1.138', NULL);
INSERT INTO action_states VALUES (73, 1, 463, 'reg', 'initial', '2013-06-04 12:24:24.977517', '10.0.1.138', NULL);
INSERT INTO action_states VALUES (73, 1, 464, 'unverified', 'verifymail', '2013-06-04 12:24:24.977517', '10.0.1.138', NULL);
INSERT INTO action_states VALUES (73, 1, 465, 'pending_review', 'verify', '2013-06-04 12:24:24.984493', '10.0.1.138', NULL);
INSERT INTO action_states VALUES (74, 1, 466, 'reg', 'initial', '2013-06-04 12:24:59.209525', '10.0.1.138', NULL);
INSERT INTO action_states VALUES (74, 1, 467, 'unverified', 'verifymail', '2013-06-04 12:24:59.209525', '10.0.1.138', NULL);
INSERT INTO action_states VALUES (74, 1, 468, 'pending_review', 'verify', '2013-06-04 12:24:59.215505', '10.0.1.138', NULL);
INSERT INTO action_states VALUES (75, 1, 469, 'reg', 'initial', '2013-06-04 12:25:27.502975', '10.0.1.138', NULL);
INSERT INTO action_states VALUES (75, 1, 470, 'unverified', 'verifymail', '2013-06-04 12:25:27.502975', '10.0.1.138', NULL);
INSERT INTO action_states VALUES (75, 1, 471, 'pending_review', 'verify', '2013-06-04 12:25:27.508014', '10.0.1.138', NULL);
INSERT INTO action_states VALUES (76, 1, 472, 'reg', 'initial', '2013-06-04 12:26:02.414732', '10.0.1.138', NULL);
INSERT INTO action_states VALUES (76, 1, 473, 'unverified', 'verifymail', '2013-06-04 12:26:02.414732', '10.0.1.138', NULL);
INSERT INTO action_states VALUES (76, 1, 474, 'pending_review', 'verify', '2013-06-04 12:26:02.419431', '10.0.1.138', NULL);
INSERT INTO action_states VALUES (77, 1, 475, 'reg', 'initial', '2013-06-04 12:28:19.211592', '10.0.1.138', NULL);
INSERT INTO action_states VALUES (77, 1, 476, 'unverified', 'verifymail', '2013-06-04 12:28:19.211592', '10.0.1.138', NULL);
INSERT INTO action_states VALUES (77, 1, 477, 'pending_review', 'verify', '2013-06-04 12:28:19.229234', '10.0.1.138', NULL);
INSERT INTO action_states VALUES (78, 1, 478, 'reg', 'initial', '2013-06-04 12:28:41.75276', '10.0.1.138', NULL);
INSERT INTO action_states VALUES (78, 1, 479, 'unverified', 'verifymail', '2013-06-04 12:28:41.75276', '10.0.1.138', NULL);
INSERT INTO action_states VALUES (78, 1, 480, 'pending_review', 'verify', '2013-06-04 12:28:41.810464', '10.0.1.138', NULL);
INSERT INTO action_states VALUES (79, 1, 481, 'reg', 'initial', '2013-06-04 12:30:25.456462', '10.0.1.138', NULL);
INSERT INTO action_states VALUES (79, 1, 482, 'unverified', 'verifymail', '2013-06-04 12:30:25.456462', '10.0.1.138', NULL);
INSERT INTO action_states VALUES (79, 1, 483, 'pending_review', 'verify', '2013-06-04 12:30:25.464976', '10.0.1.138', NULL);
INSERT INTO action_states VALUES (80, 1, 484, 'reg', 'initial', '2013-06-04 12:30:57.353407', '10.0.1.138', NULL);
INSERT INTO action_states VALUES (80, 1, 485, 'unverified', 'verifymail', '2013-06-04 12:30:57.353407', '10.0.1.138', NULL);
INSERT INTO action_states VALUES (80, 1, 486, 'pending_review', 'verify', '2013-06-04 12:30:58.313321', '10.0.1.138', NULL);
INSERT INTO action_states VALUES (81, 1, 487, 'reg', 'initial', '2013-06-04 12:31:33.823614', '10.0.1.138', NULL);
INSERT INTO action_states VALUES (81, 1, 488, 'unverified', 'verifymail', '2013-06-04 12:31:33.823614', '10.0.1.138', NULL);
INSERT INTO action_states VALUES (81, 1, 489, 'pending_review', 'verify', '2013-06-04 12:31:33.829385', '10.0.1.138', NULL);
INSERT INTO action_states VALUES (82, 1, 490, 'reg', 'initial', '2013-06-04 12:31:59.414171', '10.0.1.138', NULL);
INSERT INTO action_states VALUES (82, 1, 491, 'unverified', 'verifymail', '2013-06-04 12:31:59.414171', '10.0.1.138', NULL);
INSERT INTO action_states VALUES (82, 1, 492, 'pending_review', 'verify', '2013-06-04 12:31:59.419766', '10.0.1.138', NULL);
INSERT INTO action_states VALUES (70, 1, 493, 'locked', 'checkout', '2013-06-04 12:32:07.486473', '10.0.1.138', 301);
INSERT INTO action_states VALUES (71, 1, 494, 'locked', 'checkout', '2013-06-04 12:32:07.486473', '10.0.1.138', 301);
INSERT INTO action_states VALUES (70, 1, 495, 'accepted', 'accept', '2013-06-04 12:32:14.392958', '10.0.1.138', 308);
INSERT INTO action_states VALUES (71, 1, 496, 'accepted', 'accept', '2013-06-04 12:32:17.838751', '10.0.1.138', 309);
INSERT INTO action_states VALUES (72, 1, 497, 'locked', 'checkout', '2013-06-04 12:32:19.070434', '10.0.1.138', 310);
INSERT INTO action_states VALUES (73, 1, 498, 'locked', 'checkout', '2013-06-04 12:32:19.070434', '10.0.1.138', 310);
INSERT INTO action_states VALUES (72, 1, 499, 'accepted', 'accept', '2013-06-04 12:32:22.166087', '10.0.1.138', 317);
INSERT INTO action_states VALUES (74, 1, 501, 'locked', 'checkout', '2013-06-04 12:32:25.468453', '10.0.1.138', 319);
INSERT INTO action_states VALUES (75, 1, 502, 'locked', 'checkout', '2013-06-04 12:32:25.468453', '10.0.1.138', 319);
INSERT INTO action_states VALUES (74, 1, 503, 'accepted', 'accept', '2013-06-04 12:32:28.333066', '10.0.1.138', 326);
INSERT INTO action_states VALUES (76, 1, 505, 'locked', 'checkout', '2013-06-04 12:32:30.984453', '10.0.1.138', 328);
INSERT INTO action_states VALUES (77, 1, 506, 'locked', 'checkout', '2013-06-04 12:32:30.984453', '10.0.1.138', 328);
INSERT INTO action_states VALUES (76, 1, 507, 'accepted', 'accept', '2013-06-04 12:32:33.488079', '10.0.1.138', 335);
INSERT INTO action_states VALUES (78, 1, 509, 'locked', 'checkout', '2013-06-04 12:32:36.515351', '10.0.1.138', 337);
INSERT INTO action_states VALUES (79, 1, 510, 'locked', 'checkout', '2013-06-04 12:32:36.515351', '10.0.1.138', 337);
INSERT INTO action_states VALUES (78, 1, 511, 'accepted', 'accept', '2013-06-04 12:32:40.754062', '10.0.1.138', 344);
INSERT INTO action_states VALUES (80, 1, 513, 'locked', 'checkout', '2013-06-04 12:32:43.517565', '10.0.1.138', 346);
INSERT INTO action_states VALUES (81, 1, 514, 'locked', 'checkout', '2013-06-04 12:32:43.517565', '10.0.1.138', 346);
INSERT INTO action_states VALUES (80, 1, 515, 'refused', 'refuse', NOW() - interval '1 month', '10.0.1.138', 353);
INSERT INTO action_states VALUES (73, 1, 500, 'accepted', 'accept', '2013-06-04 12:32:24.019538', '10.0.1.138', 318);
INSERT INTO action_states VALUES (75, 1, 504, 'accepted', 'accept', '2013-06-04 12:32:29.773792', '10.0.1.138', 327);
INSERT INTO action_states VALUES (77, 1, 508, 'accepted', 'accept', '2013-06-04 12:32:35.535407', '10.0.1.138', 336);
INSERT INTO action_states VALUES (79, 1, 512, 'accepted', 'accept', '2013-06-04 12:32:42.237431', '10.0.1.138', 345);
INSERT INTO action_states VALUES (81, 1, 516, 'accepted', 'accept', '2013-06-04 12:32:47.537926', '10.0.1.138', 354);
INSERT INTO action_states VALUES (82, 1, 517, 'locked', 'checkout', '2013-06-04 12:32:48.866543', '10.0.1.138', 355);
INSERT INTO action_states VALUES (82, 1, 518, 'accepted', 'accept_w_chngs', '2013-06-04 12:32:52.558464', '10.0.1.138', 359);
INSERT INTO action_states VALUES (83, 1, 519, 'reg', 'initial', '2013-08-06 15:18:24.449334', '10.0.1.175', NULL);
INSERT INTO action_states VALUES (83, 1, 520, 'unverified', 'verifymail', '2013-08-06 15:18:24.449334', '10.0.1.175', NULL);
INSERT INTO action_states VALUES (83, 1, 521, 'pending_review', 'verify', '2013-08-06 15:18:24.467182', '10.0.1.175', NULL);
INSERT INTO action_states VALUES (84, 1, 522, 'reg', 'initial', '2013-08-06 15:19:06.123318', '10.0.1.175', NULL);
INSERT INTO action_states VALUES (84, 1, 523, 'unverified', 'verifymail', '2013-08-06 15:19:06.123318', '10.0.1.175', NULL);
INSERT INTO action_states VALUES (84, 1, 524, 'pending_review', 'verify', '2013-08-06 15:19:06.136496', '10.0.1.175', NULL);
INSERT INTO action_states VALUES (83, 1, 525, 'locked', 'checkout', '2013-08-06 15:19:26.84191', '10.0.1.175', 375);
INSERT INTO action_states VALUES (84, 1, 526, 'locked', 'checkout', '2013-08-06 15:19:26.84191', '10.0.1.175', 375);
INSERT INTO action_states VALUES (83, 1, 527, 'accepted', 'accept', '2013-08-06 15:19:30.588446', '10.0.1.175', 382);
INSERT INTO action_states VALUES (84, 1, 528, 'accepted', 'accept', '2013-08-06 15:19:34.894397', '10.0.1.175', 383);
INSERT INTO action_states VALUES (85, 1, 529, 'reg', 'initial', '2013-08-06 15:35:32.585346', '10.0.1.175', NULL);
INSERT INTO action_states VALUES (85, 1, 530, 'unverified', 'verifymail', '2013-08-06 15:35:32.585346', '10.0.1.175', NULL);
INSERT INTO action_states VALUES (85, 1, 531, 'pending_review', 'verify', '2013-08-06 15:35:32.607362', '10.0.1.175', NULL);
INSERT INTO action_states VALUES (86, 1, 532, 'reg', 'initial', '2013-08-06 15:36:33.000271', '10.0.1.175', NULL);
INSERT INTO action_states VALUES (86, 1, 533, 'unverified', 'verifymail', '2013-08-06 15:36:33.000271', '10.0.1.175', NULL);
INSERT INTO action_states VALUES (86, 1, 534, 'pending_review', 'verify', '2013-08-06 15:36:33.019516', '10.0.1.175', NULL);
INSERT INTO action_states VALUES (87, 1, 535, 'reg', 'initial', '2013-08-06 15:36:39.387043', '10.0.1.17', NULL);
INSERT INTO action_states VALUES (87, 1, 536, 'unverified', 'verifymail', '2013-08-06 15:36:39.387043', '10.0.1.17', NULL);
INSERT INTO action_states VALUES (87, 1, 537, 'pending_review', 'verify', '2013-08-06 15:36:39.394559', '10.0.1.17', NULL);
INSERT INTO action_states VALUES (88, 1, 538, 'reg', 'initial', '2013-08-06 15:37:24.915844', '10.0.1.175', NULL);
INSERT INTO action_states VALUES (88, 1, 539, 'unverified', 'verifymail', '2013-08-06 15:37:24.915844', '10.0.1.175', NULL);
INSERT INTO action_states VALUES (88, 1, 540, 'pending_review', 'verify', '2013-08-06 15:37:24.922633', '10.0.1.175', NULL);
INSERT INTO action_states VALUES (89, 1, 541, 'reg', 'initial', '2013-08-06 15:37:35.262003', '10.0.1.17', NULL);
INSERT INTO action_states VALUES (89, 1, 542, 'unverified', 'verifymail', '2013-08-06 15:37:35.262003', '10.0.1.17', NULL);
INSERT INTO action_states VALUES (89, 1, 543, 'pending_review', 'verify', '2013-08-06 15:37:35.26812', '10.0.1.17', NULL);
INSERT INTO action_states VALUES (90, 1, 544, 'reg', 'initial', '2013-08-06 15:38:19.414708', '10.0.1.175', NULL);
INSERT INTO action_states VALUES (90, 1, 545, 'unverified', 'verifymail', '2013-08-06 15:38:19.414708', '10.0.1.175', NULL);
INSERT INTO action_states VALUES (90, 1, 546, 'pending_review', 'verify', '2013-08-06 15:38:19.422433', '10.0.1.175', NULL);
INSERT INTO action_states VALUES (91, 1, 547, 'reg', 'initial', '2013-08-06 15:38:39.627571', '10.0.1.17', NULL);
INSERT INTO action_states VALUES (91, 1, 548, 'unverified', 'verifymail', '2013-08-06 15:38:39.627571', '10.0.1.17', NULL);
INSERT INTO action_states VALUES (91, 1, 549, 'pending_review', 'verify', '2013-08-06 15:38:39.632923', '10.0.1.17', NULL);
INSERT INTO action_states VALUES (92, 1, 550, 'reg', 'initial', '2013-08-06 15:39:10.342827', '10.0.1.175', NULL);
INSERT INTO action_states VALUES (92, 1, 551, 'unverified', 'verifymail', '2013-08-06 15:39:10.342827', '10.0.1.175', NULL);
INSERT INTO action_states VALUES (92, 1, 552, 'pending_review', 'verify', '2013-08-06 15:39:10.346908', '10.0.1.175', NULL);
INSERT INTO action_states VALUES (93, 1, 553, 'reg', 'initial', '2013-08-06 15:39:21.644011', '10.0.1.17', NULL);
INSERT INTO action_states VALUES (93, 1, 554, 'unverified', 'verifymail', '2013-08-06 15:39:21.644011', '10.0.1.17', NULL);
INSERT INTO action_states VALUES (93, 1, 555, 'pending_review', 'verify', '2013-08-06 15:39:21.648398', '10.0.1.17', NULL);
INSERT INTO action_states VALUES (87, 1, 556, 'locked', 'checkout', '2013-08-06 15:39:34.797529', '10.0.1.175', 472);
INSERT INTO action_states VALUES (92, 1, 557, 'locked', 'checkout', '2013-08-06 15:39:34.797529', '10.0.1.175', 472);


--
-- Data for Name: ad_changes; Type: TABLE DATA; Schema: public; Owner: dany
--

INSERT INTO ad_changes VALUES (20, 1, 220, false, 'lang', 'es', NULL);
INSERT INTO ad_changes VALUES (21, 1, 221, false, 'lang', 'es', NULL);
INSERT INTO ad_changes VALUES (22, 1, 224, false, 'lang', 'es', NULL);
INSERT INTO ad_changes VALUES (23, 1, 225, false, 'lang', 'es', NULL);
INSERT INTO ad_changes VALUES (24, 1, 228, false, 'lang', 'es', NULL);
INSERT INTO ad_changes VALUES (25, 1, 229, false, 'lang', 'es', NULL);
INSERT INTO ad_changes VALUES (26, 1, 247, false, 'lang', 'es', NULL);
INSERT INTO ad_changes VALUES (27, 1, 248, false, 'lang', 'es', NULL);
INSERT INTO ad_changes VALUES (28, 1, 251, false, 'lang', 'es', NULL);
INSERT INTO ad_changes VALUES (29, 1, 252, false, 'lang', 'es', NULL);
INSERT INTO ad_changes VALUES (30, 1, 254, false, 'lang', 'es', NULL);
INSERT INTO ad_changes VALUES (31, 1, 295, false, 'lang', 'es', NULL);
INSERT INTO ad_changes VALUES (32, 1, 296, false, 'lang', 'es', NULL);
INSERT INTO ad_changes VALUES (35, 1, 297, false, 'lang', 'es', NULL);
INSERT INTO ad_changes VALUES (36, 1, 298, false, 'lang', 'es', NULL);
INSERT INTO ad_changes VALUES (39, 1, 299, false, 'lang', 'es', NULL);
INSERT INTO ad_changes VALUES (37, 1, 300, false, 'lang', 'es', NULL);
INSERT INTO ad_changes VALUES (38, 1, 301, false, 'lang', 'es', NULL);
INSERT INTO ad_changes VALUES (33, 1, 302, false, 'lang', 'es', NULL);
INSERT INTO ad_changes VALUES (34, 1, 303, false, 'lang', 'es', NULL);
INSERT INTO ad_changes VALUES (40, 1, 351, false, 'lang', 'es', NULL);
INSERT INTO ad_changes VALUES (41, 1, 352, false, 'lang', 'es', NULL);
INSERT INTO ad_changes VALUES (42, 1, 358, false, 'lang', 'es', NULL);
INSERT INTO ad_changes VALUES (43, 1, 359, false, 'lang', 'es', NULL);
INSERT INTO ad_changes VALUES (44, 1, 362, false, 'lang', 'es', NULL);
INSERT INTO ad_changes VALUES (45, 1, 363, false, 'lang', 'es', NULL);
INSERT INTO ad_changes VALUES (46, 1, 366, false, 'lang', 'es', NULL);
INSERT INTO ad_changes VALUES (47, 1, 367, false, 'lang', 'es', NULL);
INSERT INTO ad_changes VALUES (48, 1, 373, false, 'lang', 'es', NULL);
INSERT INTO ad_changes VALUES (49, 1, 374, false, 'lang', 'es', NULL);
INSERT INTO ad_changes VALUES (50, 1, 377, false, 'lang', 'es', NULL);
INSERT INTO ad_changes VALUES (51, 1, 378, false, 'lang', 'es', NULL);
INSERT INTO ad_changes VALUES (52, 1, 381, false, 'lang', 'es', NULL);
INSERT INTO ad_changes VALUES (53, 1, 382, false, 'lang', 'es', NULL);
INSERT INTO ad_changes VALUES (54, 1, 385, false, 'lang', 'es', NULL);
INSERT INTO ad_changes VALUES (55, 1, 386, false, 'lang', 'es', NULL);
INSERT INTO ad_changes VALUES (56, 1, 391, false, 'lang', 'es', NULL);
INSERT INTO ad_changes VALUES (57, 1, 393, false, 'lang', 'es', NULL);
INSERT INTO ad_changes VALUES (58, 1, 411, false, 'lang', 'es', NULL);
INSERT INTO ad_changes VALUES (59, 1, 412, false, 'lang', 'es', NULL);
INSERT INTO ad_changes VALUES (60, 1, 415, false, 'lang', 'es', NULL);
INSERT INTO ad_changes VALUES (61, 1, 416, false, 'lang', 'es', NULL);
INSERT INTO ad_changes VALUES (62, 1, 418, false, 'lang', 'es', NULL);
INSERT INTO ad_changes VALUES (63, 1, 442, false, 'lang', 'es', NULL);
INSERT INTO ad_changes VALUES (64, 1, 443, false, 'lang', 'es', NULL);
INSERT INTO ad_changes VALUES (65, 1, 446, false, 'lang', 'es', NULL);
INSERT INTO ad_changes VALUES (66, 1, 447, false, 'lang', 'es', NULL);
INSERT INTO ad_changes VALUES (67, 1, 450, false, 'lang', 'es', NULL);
INSERT INTO ad_changes VALUES (68, 1, 451, false, 'lang', 'es', NULL);
INSERT INTO ad_changes VALUES (69, 1, 453, false, 'lang', 'es', NULL);
INSERT INTO ad_changes VALUES (70, 1, 495, false, 'lang', 'es', NULL);
INSERT INTO ad_changes VALUES (71, 1, 496, false, 'lang', 'es', NULL);
INSERT INTO ad_changes VALUES (72, 1, 499, false, 'lang', 'es', NULL);
INSERT INTO ad_changes VALUES (73, 1, 500, false, 'lang', 'es', NULL);
INSERT INTO ad_changes VALUES (74, 1, 503, false, 'lang', 'es', NULL);
INSERT INTO ad_changes VALUES (75, 1, 504, false, 'lang', 'es', NULL);
INSERT INTO ad_changes VALUES (76, 1, 507, false, 'lang', 'es', NULL);
INSERT INTO ad_changes VALUES (77, 1, 508, false, 'lang', 'es', NULL);
INSERT INTO ad_changes VALUES (78, 1, 511, false, 'lang', 'es', NULL);
INSERT INTO ad_changes VALUES (79, 1, 512, false, 'lang', 'es', NULL);
INSERT INTO ad_changes VALUES (80, 1, 515, false, 'lang', 'es', NULL);
INSERT INTO ad_changes VALUES (81, 1, 516, false, 'lang', 'es', NULL);
INSERT INTO ad_changes VALUES (82, 1, 518, false, 'lang', 'es', NULL);
INSERT INTO ad_changes VALUES (83, 1, 527, false, 'lang', 'es', NULL);
INSERT INTO ad_changes VALUES (84, 1, 528, false, 'lang', 'es', NULL);


--
-- Data for Name: ad_codes; Type: TABLE DATA; Schema: public; Owner: dany
--

INSERT INTO ad_codes VALUES (12345, 'pay');
INSERT INTO ad_codes VALUES (65432, 'pay');
INSERT INTO ad_codes VALUES (32323, 'pay');
INSERT INTO ad_codes VALUES (54545, 'pay');
INSERT INTO ad_codes VALUES (90001, 'pay');
INSERT INTO ad_codes VALUES (90002, 'pay');
INSERT INTO ad_codes VALUES (90003, 'pay');
INSERT INTO ad_codes VALUES (90004, 'pay');
INSERT INTO ad_codes VALUES (90005, 'pay');
INSERT INTO ad_codes VALUES (90006, 'pay');
INSERT INTO ad_codes VALUES (90007, 'pay');
INSERT INTO ad_codes VALUES (90008, 'pay');
INSERT INTO ad_codes VALUES (90009, 'pay');
INSERT INTO ad_codes VALUES (90010, 'pay');
INSERT INTO ad_codes VALUES (90011, 'pay');
INSERT INTO ad_codes VALUES (90012, 'pay');
INSERT INTO ad_codes VALUES (90013, 'pay');
INSERT INTO ad_codes VALUES (90014, 'pay');
INSERT INTO ad_codes VALUES (90015, 'pay');
INSERT INTO ad_codes VALUES (90016, 'pay');
INSERT INTO ad_codes VALUES (90017, 'pay');
INSERT INTO ad_codes VALUES (90018, 'pay');
INSERT INTO ad_codes VALUES (90019, 'pay');
INSERT INTO ad_codes VALUES (90020, 'pay');
INSERT INTO ad_codes VALUES (90021, 'pay');
INSERT INTO ad_codes VALUES (90022, 'pay');
INSERT INTO ad_codes VALUES (90023, 'pay');
INSERT INTO ad_codes VALUES (90024, 'pay');
INSERT INTO ad_codes VALUES (90025, 'pay');
INSERT INTO ad_codes VALUES (90026, 'pay');
INSERT INTO ad_codes VALUES (90027, 'pay');
INSERT INTO ad_codes VALUES (90028, 'pay');
INSERT INTO ad_codes VALUES (90029, 'pay');
INSERT INTO ad_codes VALUES (90030, 'pay');
INSERT INTO ad_codes VALUES (90031, 'pay');
INSERT INTO ad_codes VALUES (90032, 'pay');
INSERT INTO ad_codes VALUES (90033, 'pay');
INSERT INTO ad_codes VALUES (90034, 'pay');
INSERT INTO ad_codes VALUES (90035, 'pay');
INSERT INTO ad_codes VALUES (90036, 'pay');
INSERT INTO ad_codes VALUES (90037, 'pay');
INSERT INTO ad_codes VALUES (90038, 'pay');
INSERT INTO ad_codes VALUES (90039, 'pay');
INSERT INTO ad_codes VALUES (90040, 'pay');
INSERT INTO ad_codes VALUES (42023, 'pay');
INSERT INTO ad_codes VALUES (74046, 'pay');
INSERT INTO ad_codes VALUES (16069, 'pay');
INSERT INTO ad_codes VALUES (48092, 'pay');
INSERT INTO ad_codes VALUES (80115, 'pay');
INSERT INTO ad_codes VALUES (22138, 'pay');
INSERT INTO ad_codes VALUES (54161, 'pay');
INSERT INTO ad_codes VALUES (86184, 'pay');
INSERT INTO ad_codes VALUES (28207, 'pay');
INSERT INTO ad_codes VALUES (60230, 'pay');
INSERT INTO ad_codes VALUES (92253, 'pay');
INSERT INTO ad_codes VALUES (34276, 'pay');
INSERT INTO ad_codes VALUES (66299, 'pay');
INSERT INTO ad_codes VALUES (98322, 'pay');
INSERT INTO ad_codes VALUES (40345, 'pay');
INSERT INTO ad_codes VALUES (72368, 'pay');
INSERT INTO ad_codes VALUES (14391, 'pay');
INSERT INTO ad_codes VALUES (46414, 'pay');
INSERT INTO ad_codes VALUES (78437, 'pay');
INSERT INTO ad_codes VALUES (20460, 'pay');
INSERT INTO ad_codes VALUES (52483, 'pay');
INSERT INTO ad_codes VALUES (84506, 'pay');
INSERT INTO ad_codes VALUES (26529, 'pay');
INSERT INTO ad_codes VALUES (58552, 'pay');
INSERT INTO ad_codes VALUES (90575, 'pay');
INSERT INTO ad_codes VALUES (32598, 'pay');
INSERT INTO ad_codes VALUES (64621, 'pay');
INSERT INTO ad_codes VALUES (96644, 'pay');
INSERT INTO ad_codes VALUES (38667, 'pay');
INSERT INTO ad_codes VALUES (70690, 'pay');
INSERT INTO ad_codes VALUES (12713, 'pay');
INSERT INTO ad_codes VALUES (44736, 'pay');
INSERT INTO ad_codes VALUES (76759, 'pay');
INSERT INTO ad_codes VALUES (18782, 'pay');
INSERT INTO ad_codes VALUES (50805, 'pay');
INSERT INTO ad_codes VALUES (82828, 'pay');
INSERT INTO ad_codes VALUES (24851, 'pay');
INSERT INTO ad_codes VALUES (56874, 'pay');
INSERT INTO ad_codes VALUES (88897, 'pay');
INSERT INTO ad_codes VALUES (30920, 'pay');
INSERT INTO ad_codes VALUES (62943, 'pay');
INSERT INTO ad_codes VALUES (94966, 'pay');
INSERT INTO ad_codes VALUES (36989, 'pay');
INSERT INTO ad_codes VALUES (69012, 'pay');
INSERT INTO ad_codes VALUES (11035, 'pay');
INSERT INTO ad_codes VALUES (43058, 'pay');
INSERT INTO ad_codes VALUES (75081, 'pay');
INSERT INTO ad_codes VALUES (17104, 'pay');
INSERT INTO ad_codes VALUES (49127, 'pay');
INSERT INTO ad_codes VALUES (81150, 'pay');
INSERT INTO ad_codes VALUES (23173, 'pay');
INSERT INTO ad_codes VALUES (55196, 'pay');
INSERT INTO ad_codes VALUES (87219, 'pay');
INSERT INTO ad_codes VALUES (29242, 'pay');
INSERT INTO ad_codes VALUES (61265, 'pay');
INSERT INTO ad_codes VALUES (93288, 'pay');
INSERT INTO ad_codes VALUES (800481495, 'verify');
INSERT INTO ad_codes VALUES (935873518, 'verify');
INSERT INTO ad_codes VALUES (171265541, 'verify');
INSERT INTO ad_codes VALUES (306657564, 'verify');
INSERT INTO ad_codes VALUES (442049587, 'verify');
INSERT INTO ad_codes VALUES (577441610, 'verify');
INSERT INTO ad_codes VALUES (712833633, 'verify');
INSERT INTO ad_codes VALUES (848225656, 'verify');
INSERT INTO ad_codes VALUES (983617679, 'verify');
INSERT INTO ad_codes VALUES (219009702, 'verify');
INSERT INTO ad_codes VALUES (354401725, 'verify');
INSERT INTO ad_codes VALUES (489793748, 'verify');
INSERT INTO ad_codes VALUES (625185771, 'verify');
INSERT INTO ad_codes VALUES (760577794, 'verify');
INSERT INTO ad_codes VALUES (895969817, 'verify');
INSERT INTO ad_codes VALUES (131361840, 'verify');
INSERT INTO ad_codes VALUES (266753863, 'verify');
INSERT INTO ad_codes VALUES (402145886, 'verify');
INSERT INTO ad_codes VALUES (537537909, 'verify');
INSERT INTO ad_codes VALUES (672929932, 'verify');
INSERT INTO ad_codes VALUES (808321955, 'verify');
INSERT INTO ad_codes VALUES (943713978, 'verify');
INSERT INTO ad_codes VALUES (179106001, 'verify');
INSERT INTO ad_codes VALUES (314498024, 'verify');
INSERT INTO ad_codes VALUES (449890047, 'verify');
INSERT INTO ad_codes VALUES (585282070, 'verify');
INSERT INTO ad_codes VALUES (35311, 'pay');
INSERT INTO ad_codes VALUES (67334, 'pay');
INSERT INTO ad_codes VALUES (99357, 'pay');
INSERT INTO ad_codes VALUES (41380, 'pay');
INSERT INTO ad_codes VALUES (73403, 'pay');
INSERT INTO ad_codes VALUES (15426, 'pay');
INSERT INTO ad_codes VALUES (47449, 'pay');
INSERT INTO ad_codes VALUES (79472, 'pay');
INSERT INTO ad_codes VALUES (21495, 'pay');
INSERT INTO ad_codes VALUES (53518, 'pay');
INSERT INTO ad_codes VALUES (85541, 'pay');
INSERT INTO ad_codes VALUES (27564, 'pay');
INSERT INTO ad_codes VALUES (59587, 'pay');
INSERT INTO ad_codes VALUES (91610, 'pay');
INSERT INTO ad_codes VALUES (33633, 'pay');
INSERT INTO ad_codes VALUES (65656, 'pay');
INSERT INTO ad_codes VALUES (97679, 'pay');
INSERT INTO ad_codes VALUES (39702, 'pay');
INSERT INTO ad_codes VALUES (71725, 'pay');
INSERT INTO ad_codes VALUES (13748, 'pay');
INSERT INTO ad_codes VALUES (45771, 'pay');
INSERT INTO ad_codes VALUES (77794, 'pay');
INSERT INTO ad_codes VALUES (19817, 'pay');
INSERT INTO ad_codes VALUES (51840, 'pay');
INSERT INTO ad_codes VALUES (83863, 'pay');
INSERT INTO ad_codes VALUES (25886, 'pay');
INSERT INTO ad_codes VALUES (57909, 'pay');
INSERT INTO ad_codes VALUES (89932, 'pay');
INSERT INTO ad_codes VALUES (31955, 'pay');
INSERT INTO ad_codes VALUES (63978, 'pay');
INSERT INTO ad_codes VALUES (96001, 'pay');
INSERT INTO ad_codes VALUES (38024, 'pay');
INSERT INTO ad_codes VALUES (70047, 'pay');
INSERT INTO ad_codes VALUES (12070, 'pay');
INSERT INTO ad_codes VALUES (44093, 'pay');
INSERT INTO ad_codes VALUES (76116, 'pay');
INSERT INTO ad_codes VALUES (18139, 'pay');
INSERT INTO ad_codes VALUES (50162, 'pay');
INSERT INTO ad_codes VALUES (82185, 'pay');
INSERT INTO ad_codes VALUES (24208, 'pay');
INSERT INTO ad_codes VALUES (56231, 'pay');
INSERT INTO ad_codes VALUES (88254, 'pay');
INSERT INTO ad_codes VALUES (30277, 'pay');
INSERT INTO ad_codes VALUES (62300, 'pay');
INSERT INTO ad_codes VALUES (94323, 'pay');
INSERT INTO ad_codes VALUES (36346, 'pay');
INSERT INTO ad_codes VALUES (68369, 'pay');
INSERT INTO ad_codes VALUES (10392, 'pay');
INSERT INTO ad_codes VALUES (42415, 'pay');
INSERT INTO ad_codes VALUES (74438, 'pay');
INSERT INTO ad_codes VALUES (16461, 'pay');
INSERT INTO ad_codes VALUES (48484, 'pay');
INSERT INTO ad_codes VALUES (80507, 'pay');
INSERT INTO ad_codes VALUES (22530, 'pay');
INSERT INTO ad_codes VALUES (54553, 'pay');
INSERT INTO ad_codes VALUES (86576, 'pay');
INSERT INTO ad_codes VALUES (28599, 'pay');
INSERT INTO ad_codes VALUES (60622, 'pay');
INSERT INTO ad_codes VALUES (92645, 'pay');
INSERT INTO ad_codes VALUES (34668, 'pay');
INSERT INTO ad_codes VALUES (66691, 'pay');
INSERT INTO ad_codes VALUES (98714, 'pay');
INSERT INTO ad_codes VALUES (40737, 'pay');
INSERT INTO ad_codes VALUES (72760, 'pay');
INSERT INTO ad_codes VALUES (14783, 'pay');
INSERT INTO ad_codes VALUES (46806, 'pay');
INSERT INTO ad_codes VALUES (78829, 'pay');
INSERT INTO ad_codes VALUES (20852, 'pay');
INSERT INTO ad_codes VALUES (52875, 'pay');
INSERT INTO ad_codes VALUES (84898, 'pay');
INSERT INTO ad_codes VALUES (26921, 'pay');
INSERT INTO ad_codes VALUES (58944, 'pay');
INSERT INTO ad_codes VALUES (90967, 'pay');
INSERT INTO ad_codes VALUES (32990, 'pay');


--
-- Data for Name: ad_image_changes; Type: TABLE DATA; Schema: public; Owner: dany
--

INSERT INTO ad_image_changes VALUES (20, 1, 220, 0, NULL, false);
INSERT INTO ad_image_changes VALUES (21, 1, 221, 0, NULL, false);
INSERT INTO ad_image_changes VALUES (22, 1, 224, 0, NULL, false);
INSERT INTO ad_image_changes VALUES (23, 1, 225, 0, NULL, false);
INSERT INTO ad_image_changes VALUES (24, 1, 228, 0, NULL, false);
INSERT INTO ad_image_changes VALUES (25, 1, 229, 0, NULL, false);
INSERT INTO ad_image_changes VALUES (26, 1, 247, 0, NULL, false);
INSERT INTO ad_image_changes VALUES (27, 1, 248, 0, NULL, false);
INSERT INTO ad_image_changes VALUES (28, 1, 251, 0, NULL, false);
INSERT INTO ad_image_changes VALUES (29, 1, 252, 0, NULL, false);
INSERT INTO ad_image_changes VALUES (30, 1, 254, 0, NULL, false);
INSERT INTO ad_image_changes VALUES (31, 1, 295, 0, NULL, false);
INSERT INTO ad_image_changes VALUES (32, 1, 296, 0, NULL, false);
INSERT INTO ad_image_changes VALUES (35, 1, 297, 0, NULL, false);
INSERT INTO ad_image_changes VALUES (36, 1, 298, 0, NULL, false);
INSERT INTO ad_image_changes VALUES (39, 1, 299, 0, NULL, false);
INSERT INTO ad_image_changes VALUES (37, 1, 300, 0, NULL, false);
INSERT INTO ad_image_changes VALUES (38, 1, 301, 0, NULL, false);
INSERT INTO ad_image_changes VALUES (33, 1, 302, 0, NULL, false);
INSERT INTO ad_image_changes VALUES (34, 1, 303, 0, NULL, false);
INSERT INTO ad_image_changes VALUES (40, 1, 351, 0, NULL, false);
INSERT INTO ad_image_changes VALUES (41, 1, 352, 0, NULL, false);
INSERT INTO ad_image_changes VALUES (42, 1, 358, 0, NULL, false);
INSERT INTO ad_image_changes VALUES (43, 1, 359, 0, NULL, false);
INSERT INTO ad_image_changes VALUES (44, 1, 362, 0, NULL, false);
INSERT INTO ad_image_changes VALUES (45, 1, 363, 0, NULL, false);
INSERT INTO ad_image_changes VALUES (46, 1, 366, 0, NULL, false);
INSERT INTO ad_image_changes VALUES (47, 1, 367, 0, NULL, false);
INSERT INTO ad_image_changes VALUES (48, 1, 373, 0, NULL, false);
INSERT INTO ad_image_changes VALUES (49, 1, 374, 0, NULL, false);
INSERT INTO ad_image_changes VALUES (50, 1, 377, 0, NULL, false);
INSERT INTO ad_image_changes VALUES (51, 1, 378, 0, NULL, false);
INSERT INTO ad_image_changes VALUES (52, 1, 381, 0, NULL, false);
INSERT INTO ad_image_changes VALUES (53, 1, 382, 0, NULL, false);
INSERT INTO ad_image_changes VALUES (54, 1, 385, 0, NULL, false);
INSERT INTO ad_image_changes VALUES (55, 1, 386, 0, NULL, false);
INSERT INTO ad_image_changes VALUES (56, 1, 391, 0, NULL, false);
INSERT INTO ad_image_changes VALUES (57, 1, 393, 0, NULL, false);
INSERT INTO ad_image_changes VALUES (58, 1, 411, 0, NULL, false);
INSERT INTO ad_image_changes VALUES (59, 1, 412, 0, NULL, false);
INSERT INTO ad_image_changes VALUES (60, 1, 415, 0, NULL, false);
INSERT INTO ad_image_changes VALUES (61, 1, 416, 0, NULL, false);
INSERT INTO ad_image_changes VALUES (62, 1, 418, 0, NULL, false);
INSERT INTO ad_image_changes VALUES (63, 1, 442, 0, NULL, false);
INSERT INTO ad_image_changes VALUES (64, 1, 443, 0, NULL, false);
INSERT INTO ad_image_changes VALUES (65, 1, 446, 0, NULL, false);
INSERT INTO ad_image_changes VALUES (66, 1, 447, 0, NULL, false);
INSERT INTO ad_image_changes VALUES (67, 1, 450, 0, NULL, false);
INSERT INTO ad_image_changes VALUES (68, 1, 451, 0, NULL, false);
INSERT INTO ad_image_changes VALUES (69, 1, 453, 0, NULL, false);
INSERT INTO ad_image_changes VALUES (70, 1, 495, 0, NULL, false);
INSERT INTO ad_image_changes VALUES (71, 1, 496, 0, NULL, false);
INSERT INTO ad_image_changes VALUES (72, 1, 499, 0, NULL, false);
INSERT INTO ad_image_changes VALUES (73, 1, 500, 0, NULL, false);
INSERT INTO ad_image_changes VALUES (74, 1, 503, 0, NULL, false);
INSERT INTO ad_image_changes VALUES (75, 1, 504, 0, NULL, false);
INSERT INTO ad_image_changes VALUES (76, 1, 507, 0, NULL, false);
INSERT INTO ad_image_changes VALUES (77, 1, 508, 0, NULL, false);
INSERT INTO ad_image_changes VALUES (78, 1, 511, 0, NULL, false);
INSERT INTO ad_image_changes VALUES (79, 1, 512, 0, NULL, false);
INSERT INTO ad_image_changes VALUES (80, 1, 515, 0, NULL, false);
INSERT INTO ad_image_changes VALUES (81, 1, 516, 0, NULL, false);
INSERT INTO ad_image_changes VALUES (82, 1, 518, 0, NULL, false);
INSERT INTO ad_image_changes VALUES (83, 1, 527, 0, NULL, false);
INSERT INTO ad_image_changes VALUES (84, 1, 528, 0, NULL, false);


--
-- Data for Name: ad_images; Type: TABLE DATA; Schema: public; Owner: dany
--



--
-- Data for Name: ad_images_digests; Type: TABLE DATA; Schema: public; Owner: dany
--



--
-- Data for Name: ad_media; Type: TABLE DATA; Schema: public; Owner: dany
--

INSERT INTO ad_media VALUES (100012344, 6, 0, '2013-06-04 10:38:33.321443', 'image', NULL, NULL, NULL);
INSERT INTO ad_media VALUES (100012345, 6, 1, '2013-06-04 10:38:33.322195', 'image', NULL, NULL, NULL);
INSERT INTO ad_media VALUES (100012346, 6, 2, '2013-06-04 10:38:33.322399', 'image', NULL, NULL, NULL);


--
-- Data for Name: ad_media_changes; Type: TABLE DATA; Schema: public; Owner: dany
--



--
-- Data for Name: ad_params; Type: TABLE DATA; Schema: public; Owner: dany
--

INSERT INTO ad_params VALUES (1, 'rooms', '2');
INSERT INTO ad_params VALUES (1, 'size', '250');
INSERT INTO ad_params VALUES (1, 'garage_spaces', '2');
INSERT INTO ad_params VALUES (1, 'condominio', '400');
INSERT INTO ad_params VALUES (1, 'communes', '5');
INSERT INTO ad_params VALUES (1, 'currency', 'peso');
INSERT INTO ad_params VALUES (1, 'country', 'UNK');
INSERT INTO ad_params VALUES (6, 'regdate', '1986');
INSERT INTO ad_params VALUES (6, 'mileage', '1');
INSERT INTO ad_params VALUES (6, 'fuel', '2');
INSERT INTO ad_params VALUES (6, 'gearbox', '2');
INSERT INTO ad_params VALUES (11, 'service_type', '6');
INSERT INTO ad_params VALUES (12, 'rooms', '3');
INSERT INTO ad_params VALUES (12, 'size', '120');
INSERT INTO ad_params VALUES (12, 'garage_spaces', '1');
INSERT INTO ad_params VALUES (12, 'condominio', '400');
INSERT INTO ad_params VALUES (12, 'communes', '323');
INSERT INTO ad_params VALUES (12, 'currency', 'peso');
INSERT INTO ad_params VALUES (12, 'country', 'UNK');
INSERT INTO ad_params VALUES (20, 'rooms', '1');
INSERT INTO ad_params VALUES (20, 'size', '40');
INSERT INTO ad_params VALUES (20, 'garage_spaces', '1');
INSERT INTO ad_params VALUES (20, 'condominio', '25000');
INSERT INTO ad_params VALUES (20, 'communes', '343');
INSERT INTO ad_params VALUES (20, 'prev_currency', 'peso');
INSERT INTO ad_params VALUES (20, 'country', 'UNK');
INSERT INTO ad_params VALUES (21, 'rooms', '3');
INSERT INTO ad_params VALUES (21, 'size', '100');
INSERT INTO ad_params VALUES (21, 'garage_spaces', '1');
INSERT INTO ad_params VALUES (21, 'condominio', '45000');
INSERT INTO ad_params VALUES (21, 'communes', '323');
INSERT INTO ad_params VALUES (21, 'prev_currency', 'peso');
INSERT INTO ad_params VALUES (21, 'country', 'UNK');
INSERT INTO ad_params VALUES (22, 'rooms', '2');
INSERT INTO ad_params VALUES (22, 'communes', '310');
INSERT INTO ad_params VALUES (22, 'prev_currency', 'peso');
INSERT INTO ad_params VALUES (22, 'country', 'UNK');
INSERT INTO ad_params VALUES (23, 'rooms', '2');
INSERT INTO ad_params VALUES (23, 'size', '52');
INSERT INTO ad_params VALUES (23, 'communes', '343');
INSERT INTO ad_params VALUES (23, 'prev_currency', 'peso');
INSERT INTO ad_params VALUES (23, 'country', 'UNK');
INSERT INTO ad_params VALUES (24, 'rooms', '2');
INSERT INTO ad_params VALUES (24, 'size', '52');
INSERT INTO ad_params VALUES (24, 'condominio', '37000');
INSERT INTO ad_params VALUES (24, 'communes', '343');
INSERT INTO ad_params VALUES (24, 'prev_currency', 'peso');
INSERT INTO ad_params VALUES (24, 'country', 'UNK');
INSERT INTO ad_params VALUES (25, 'rooms', '3');
INSERT INTO ad_params VALUES (25, 'size', '60');
INSERT INTO ad_params VALUES (25, 'garage_spaces', '1');
INSERT INTO ad_params VALUES (25, 'condominio', '20000');
INSERT INTO ad_params VALUES (25, 'communes', '320');
INSERT INTO ad_params VALUES (25, 'prev_currency', 'peso');
INSERT INTO ad_params VALUES (25, 'country', 'UNK');
INSERT INTO ad_params VALUES (26, 'rooms', '1');
INSERT INTO ad_params VALUES (26, 'communes', '330');
INSERT INTO ad_params VALUES (26, 'prev_currency', 'peso');
INSERT INTO ad_params VALUES (26, 'country', 'UNK');
INSERT INTO ad_params VALUES (27, 'rooms', '1');
INSERT INTO ad_params VALUES (27, 'size', '27');
INSERT INTO ad_params VALUES (27, 'communes', '315');
INSERT INTO ad_params VALUES (27, 'prev_currency', 'peso');
INSERT INTO ad_params VALUES (27, 'country', 'UNK');
INSERT INTO ad_params VALUES (28, 'rooms', '2');
INSERT INTO ad_params VALUES (28, 'size', '60');
INSERT INTO ad_params VALUES (28, 'garage_spaces', '1');
INSERT INTO ad_params VALUES (28, 'condominio', '40000');
INSERT INTO ad_params VALUES (28, 'communes', '323');
INSERT INTO ad_params VALUES (28, 'prev_currency', 'peso');
INSERT INTO ad_params VALUES (28, 'country', 'UNK');
INSERT INTO ad_params VALUES (29, 'rooms', '1');
INSERT INTO ad_params VALUES (29, 'size', '30');
INSERT INTO ad_params VALUES (29, 'garage_spaces', '1');
INSERT INTO ad_params VALUES (29, 'communes', '343');
INSERT INTO ad_params VALUES (29, 'prev_currency', 'peso');
INSERT INTO ad_params VALUES (29, 'country', 'UNK');
INSERT INTO ad_params VALUES (30, 'rooms', '3');
INSERT INTO ad_params VALUES (30, 'size', '55');
INSERT INTO ad_params VALUES (30, 'condominio', '25000');
INSERT INTO ad_params VALUES (30, 'communes', '332');
INSERT INTO ad_params VALUES (30, 'prev_currency', 'peso');
INSERT INTO ad_params VALUES (30, 'country', 'UNK');
INSERT INTO ad_params VALUES (31, 'size', '5000');
INSERT INTO ad_params VALUES (31, 'communes', '331');
INSERT INTO ad_params VALUES (31, 'prev_currency', 'peso');
INSERT INTO ad_params VALUES (31, 'country', 'UNK');
INSERT INTO ad_params VALUES (32, 'size', '9000');
INSERT INTO ad_params VALUES (32, 'condominio', '1000000');
INSERT INTO ad_params VALUES (32, 'communes', '321');
INSERT INTO ad_params VALUES (32, 'prev_currency', 'peso');
INSERT INTO ad_params VALUES (32, 'country', 'UNK');
INSERT INTO ad_params VALUES (33, 'communes', '325');
INSERT INTO ad_params VALUES (33, 'prev_currency', 'peso');
INSERT INTO ad_params VALUES (33, 'country', 'UNK');
INSERT INTO ad_params VALUES (34, 'regdate', '2008');
INSERT INTO ad_params VALUES (34, 'mileage', '10000');
INSERT INTO ad_params VALUES (34, 'cubiccms', '4');
INSERT INTO ad_params VALUES (34, 'prev_currency', 'peso');
INSERT INTO ad_params VALUES (34, 'country', 'UNK');
INSERT INTO ad_params VALUES (35, 'regdate', '2001');
INSERT INTO ad_params VALUES (35, 'mileage', '1');
INSERT INTO ad_params VALUES (35, 'gearbox', '2');
INSERT INTO ad_params VALUES (35, 'fuel', '1');
INSERT INTO ad_params VALUES (35, 'cartype', '3');
INSERT INTO ad_params VALUES (35, 'prev_currency', 'peso');
INSERT INTO ad_params VALUES (35, 'brand', '26');
INSERT INTO ad_params VALUES (35, 'model', '17');
INSERT INTO ad_params VALUES (35, 'version', '1');
INSERT INTO ad_params VALUES (35, 'country', 'UNK');
INSERT INTO ad_params VALUES (36, 'regdate', '1998');
INSERT INTO ad_params VALUES (36, 'mileage', '150');
INSERT INTO ad_params VALUES (36, 'cubiccms', '2');
INSERT INTO ad_params VALUES (36, 'prev_currency', 'peso');
INSERT INTO ad_params VALUES (36, 'country', 'UNK');
INSERT INTO ad_params VALUES (37, 'regdate', '2009');
INSERT INTO ad_params VALUES (37, 'mileage', '14000');
INSERT INTO ad_params VALUES (37, 'cubiccms', '4');
INSERT INTO ad_params VALUES (37, 'prev_currency', 'peso');
INSERT INTO ad_params VALUES (37, 'country', 'UNK');
INSERT INTO ad_params VALUES (38, 'regdate', '2003');
INSERT INTO ad_params VALUES (38, 'mileage', '94000');
INSERT INTO ad_params VALUES (38, 'gearbox', '1');
INSERT INTO ad_params VALUES (38, 'fuel', '1');
INSERT INTO ad_params VALUES (38, 'cartype', '1');
INSERT INTO ad_params VALUES (38, 'prev_currency', 'peso');
INSERT INTO ad_params VALUES (38, 'brand', '86');
INSERT INTO ad_params VALUES (38, 'model', '23');
INSERT INTO ad_params VALUES (38, 'version', '9');
INSERT INTO ad_params VALUES (38, 'country', 'UNK');
INSERT INTO ad_params VALUES (39, 'regdate', '2006');
INSERT INTO ad_params VALUES (39, 'mileage', '140000');
INSERT INTO ad_params VALUES (39, 'gearbox', '1');
INSERT INTO ad_params VALUES (39, 'fuel', '1');
INSERT INTO ad_params VALUES (39, 'cartype', '1');
INSERT INTO ad_params VALUES (39, 'prev_currency', 'peso');
INSERT INTO ad_params VALUES (39, 'brand', '18');
INSERT INTO ad_params VALUES (39, 'model', '7');
INSERT INTO ad_params VALUES (39, 'version', '4');
INSERT INTO ad_params VALUES (39, 'country', 'UNK');
INSERT INTO ad_params VALUES (40, 'job_category', '19|20|24');
INSERT INTO ad_params VALUES (40, 'country', 'UNK');
INSERT INTO ad_params VALUES (41, 'job_category', '17');
INSERT INTO ad_params VALUES (41, 'country', 'UNK');
INSERT INTO ad_params VALUES (42, 'prev_currency', 'peso');
INSERT INTO ad_params VALUES (42, 'country', 'UNK');
INSERT INTO ad_params VALUES (43, 'prev_currency', 'peso');
INSERT INTO ad_params VALUES (43, 'country', 'UNK');
INSERT INTO ad_params VALUES (44, 'job_category', '19|20|24');
INSERT INTO ad_params VALUES (44, 'country', 'UNK');
INSERT INTO ad_params VALUES (45, 'prev_currency', 'peso');
INSERT INTO ad_params VALUES (45, 'country', 'UNK');
INSERT INTO ad_params VALUES (46, 'prev_currency', 'peso');
INSERT INTO ad_params VALUES (46, 'country', 'UNK');
INSERT INTO ad_params VALUES (47, 'prev_currency', 'peso');
INSERT INTO ad_params VALUES (47, 'country', 'UNK');
INSERT INTO ad_params VALUES (48, 'prev_currency', 'peso');
INSERT INTO ad_params VALUES (48, 'country', 'UNK');
INSERT INTO ad_params VALUES (49, 'prev_currency', 'peso');
INSERT INTO ad_params VALUES (49, 'country', 'UNK');
INSERT INTO ad_params VALUES (50, 'prev_currency', 'peso');
INSERT INTO ad_params VALUES (50, 'country', 'UNK');
INSERT INTO ad_params VALUES (51, 'prev_currency', 'peso');
INSERT INTO ad_params VALUES (51, 'country', 'UNK');
INSERT INTO ad_params VALUES (52, 'prev_currency', 'peso');
INSERT INTO ad_params VALUES (52, 'country', 'UNK');
INSERT INTO ad_params VALUES (53, 'prev_currency', 'peso');
INSERT INTO ad_params VALUES (53, 'country', 'UNK');
INSERT INTO ad_params VALUES (54, 'prev_currency', 'peso');
INSERT INTO ad_params VALUES (54, 'country', 'UNK');
INSERT INTO ad_params VALUES (55, 'prev_currency', 'peso');
INSERT INTO ad_params VALUES (55, 'country', 'UNK');
INSERT INTO ad_params VALUES (56, 'prev_currency', 'peso');
INSERT INTO ad_params VALUES (56, 'country', 'UNK');
INSERT INTO ad_params VALUES (57, 'prev_currency', 'peso');
INSERT INTO ad_params VALUES (57, 'country', 'UNK');
INSERT INTO ad_params VALUES (58, 'gender', '3');
INSERT INTO ad_params VALUES (58, 'condition', '1');
INSERT INTO ad_params VALUES (58, 'prev_currency', 'peso');
INSERT INTO ad_params VALUES (58, 'country', 'UNK');
INSERT INTO ad_params VALUES (59, 'prev_currency', 'peso');
INSERT INTO ad_params VALUES (59, 'country', 'UNK');
INSERT INTO ad_params VALUES (60, 'prev_currency', 'peso');
INSERT INTO ad_params VALUES (60, 'country', 'UNK');
INSERT INTO ad_params VALUES (61, 'condition', '1');
INSERT INTO ad_params VALUES (61, 'prev_currency', 'peso');
INSERT INTO ad_params VALUES (61, 'country', 'UNK');
INSERT INTO ad_params VALUES (62, 'condition', '2');
INSERT INTO ad_params VALUES (62, 'prev_currency', 'peso');
INSERT INTO ad_params VALUES (62, 'country', 'UNK');
INSERT INTO ad_params VALUES (63, 'job_category', '20|24');
INSERT INTO ad_params VALUES (63, 'country', 'UNK');
INSERT INTO ad_params VALUES (64, 'job_category', '21');
INSERT INTO ad_params VALUES (64, 'country', 'UNK');
INSERT INTO ad_params VALUES (65, 'job_category', '21|22');
INSERT INTO ad_params VALUES (65, 'country', 'UNK');
INSERT INTO ad_params VALUES (66, 'job_category', '20|24');
INSERT INTO ad_params VALUES (66, 'country', 'UNK');
INSERT INTO ad_params VALUES (67, 'job_category', '20|24');
INSERT INTO ad_params VALUES (67, 'country', 'UNK');
INSERT INTO ad_params VALUES (68, 'job_category', '22');
INSERT INTO ad_params VALUES (68, 'country', 'UNK');
INSERT INTO ad_params VALUES (69, 'job_category', '19');
INSERT INTO ad_params VALUES (69, 'country', 'UNK');
INSERT INTO ad_params VALUES (70, 'prev_currency', 'peso');
INSERT INTO ad_params VALUES (70, 'country', 'UNK');
INSERT INTO ad_params VALUES (71, 'prev_currency', 'peso');
INSERT INTO ad_params VALUES (71, 'country', 'UNK');
INSERT INTO ad_params VALUES (72, 'prev_currency', 'peso');
INSERT INTO ad_params VALUES (72, 'country', 'UNK');
INSERT INTO ad_params VALUES (73, 'prev_currency', 'peso');
INSERT INTO ad_params VALUES (73, 'country', 'UNK');
INSERT INTO ad_params VALUES (74, 'prev_currency', 'peso');
INSERT INTO ad_params VALUES (74, 'country', 'UNK');
INSERT INTO ad_params VALUES (75, 'prev_currency', 'peso');
INSERT INTO ad_params VALUES (75, 'country', 'UNK');
INSERT INTO ad_params VALUES (76, 'prev_currency', 'peso');
INSERT INTO ad_params VALUES (76, 'country', 'UNK');
INSERT INTO ad_params VALUES (77, 'prev_currency', 'peso');
INSERT INTO ad_params VALUES (77, 'country', 'UNK');
INSERT INTO ad_params VALUES (78, 'prev_currency', 'peso');
INSERT INTO ad_params VALUES (78, 'country', 'UNK');
INSERT INTO ad_params VALUES (79, 'prev_currency', 'peso');
INSERT INTO ad_params VALUES (79, 'country', 'UNK');
INSERT INTO ad_params VALUES (80, 'prev_currency', 'peso');
INSERT INTO ad_params VALUES (80, 'country', 'UNK');
INSERT INTO ad_params VALUES (81, 'prev_currency', 'peso');
INSERT INTO ad_params VALUES (81, 'country', 'UNK');
INSERT INTO ad_params VALUES (82, 'prev_currency', 'peso');
INSERT INTO ad_params VALUES (82, 'country', 'UNK');
INSERT INTO ad_params VALUES (83, 'prev_currency', 'peso');
INSERT INTO ad_params VALUES (83, 'country', 'UNK');
INSERT INTO ad_params VALUES (84, 'prev_currency', 'peso');
INSERT INTO ad_params VALUES (84, 'country', 'UNK');
INSERT INTO ad_params VALUES (85, 'prev_currency', 'peso');
INSERT INTO ad_params VALUES (85, 'country', 'UNK');
INSERT INTO ad_params VALUES (86, 'prev_currency', 'peso');
INSERT INTO ad_params VALUES (86, 'country', 'UNK');
INSERT INTO ad_params VALUES (87, 'prev_currency', 'peso');
INSERT INTO ad_params VALUES (87, 'country', 'UNK');
INSERT INTO ad_params VALUES (88, 'prev_currency', 'peso');
INSERT INTO ad_params VALUES (88, 'country', 'UNK');
INSERT INTO ad_params VALUES (89, 'prev_currency', 'peso');
INSERT INTO ad_params VALUES (89, 'country', 'UNK');
INSERT INTO ad_params VALUES (90, 'prev_currency', 'peso');
INSERT INTO ad_params VALUES (90, 'country', 'UNK');
INSERT INTO ad_params VALUES (91, 'prev_currency', 'peso');
INSERT INTO ad_params VALUES (91, 'country', 'UNK');
INSERT INTO ad_params VALUES (92, 'prev_currency', 'peso');
INSERT INTO ad_params VALUES (92, 'country', 'UNK');
INSERT INTO ad_params VALUES (93, 'prev_currency', 'peso');
INSERT INTO ad_params VALUES (93, 'country', 'UNK');


--
-- Data for Name: ad_queues; Type: TABLE DATA; Schema: public; Owner: dany
--

INSERT INTO ad_queues VALUES (85, 1, 'whitelist', '2013-08-06 15:35:32.607362', NULL, NULL);
INSERT INTO ad_queues VALUES (86, 1, 'whitelist', '2013-08-06 15:36:33.019516', NULL, NULL);
INSERT INTO ad_queues VALUES (88, 1, 'whitelist', '2013-08-06 15:37:24.922633', NULL, NULL);
INSERT INTO ad_queues VALUES (89, 1, 'whitelist', '2013-08-06 15:37:35.26812', NULL, NULL);
INSERT INTO ad_queues VALUES (90, 1, 'whitelist', '2013-08-06 15:38:19.422433', NULL, NULL);
INSERT INTO ad_queues VALUES (91, 1, 'whitelist', '2013-08-06 15:38:39.632923', NULL, NULL);
INSERT INTO ad_queues VALUES (93, 1, 'whitelist', '2013-08-06 15:39:21.648398', NULL, NULL);
INSERT INTO ad_queues VALUES (87, 1, 'normal', '2013-08-06 15:36:39.394559', 9, '2013-08-06 15:44:34.797529');
INSERT INTO ad_queues VALUES (92, 1, 'normal', '2013-08-06 15:39:10.346908', 9, '2013-08-06 15:44:34.797529');


--
-- Data for Name: admin_privs; Type: TABLE DATA; Schema: public; Owner: dany
--

INSERT INTO admin_privs VALUES (2, 'adqueue');
INSERT INTO admin_privs VALUES (3, 'adqueue');
INSERT INTO admin_privs VALUES (3, 'admin');
INSERT INTO admin_privs VALUES (3, 'adminad.edit_ad');
INSERT INTO admin_privs VALUES (3, 'adminad.clear_ad');
INSERT INTO admin_privs VALUES (3, 'adminad.bump');
INSERT INTO admin_privs VALUES (3, 'credit');
INSERT INTO admin_privs VALUES (3, 'config');
INSERT INTO admin_privs VALUES (3, 'adqueue.admin_queue');
INSERT INTO admin_privs VALUES (3, 'adqueue.admin_settings');
INSERT INTO admin_privs VALUES (3, 'adqueue.show_num_ads=2');
INSERT INTO admin_privs VALUES (3, 'search');
INSERT INTO admin_privs VALUES (3, 'search.search_ads');
INSERT INTO admin_privs VALUES (3, 'notice_abuse');
INSERT INTO admin_privs VALUES (4, 'admin');
INSERT INTO admin_privs VALUES (5, 'admin');
INSERT INTO admin_privs VALUES (5, 'adqueue');
INSERT INTO admin_privs VALUES (5, 'adminad');
INSERT INTO admin_privs VALUES (5, 'adminad.clear_ad');
INSERT INTO admin_privs VALUES (5, 'adminad.edit_ad');
INSERT INTO admin_privs VALUES (5, 'credit');
INSERT INTO admin_privs VALUES (5, 'config');
INSERT INTO admin_privs VALUES (5, 'adqueue.admin_queue');
INSERT INTO admin_privs VALUES (5, 'adqueue.admin_settings');
INSERT INTO admin_privs VALUES (5, 'adqueue.show_num_ads=2');
INSERT INTO admin_privs VALUES (6, 'admin');
INSERT INTO admin_privs VALUES (6, 'adqueue');
INSERT INTO admin_privs VALUES (6, 'adminad.edit_ad');
INSERT INTO admin_privs VALUES (50, 'admin');
INSERT INTO admin_privs VALUES (50, 'adqueue');
INSERT INTO admin_privs VALUES (51, 'admin');
INSERT INTO admin_privs VALUES (51, 'adqueue');
INSERT INTO admin_privs VALUES (52, 'admin');
INSERT INTO admin_privs VALUES (52, 'adqueue');
INSERT INTO admin_privs VALUES (53, 'admin');
INSERT INTO admin_privs VALUES (53, 'adqueue');
INSERT INTO admin_privs VALUES (54, 'admin');
INSERT INTO admin_privs VALUES (54, 'adqueue');
INSERT INTO admin_privs VALUES (55, 'admin');
INSERT INTO admin_privs VALUES (55, 'adqueue');
INSERT INTO admin_privs VALUES (9, 'search.paylog');
INSERT INTO admin_privs VALUES (9, 'search.reviewers');
INSERT INTO admin_privs VALUES (9, 'search.maillog');
INSERT INTO admin_privs VALUES (9, 'search.abuse_report');
INSERT INTO admin_privs VALUES (9, 'notice_abuse');
INSERT INTO admin_privs VALUES (9, 'adminad.clear_ad');
INSERT INTO admin_privs VALUES (9, 'adminad.edit_ad');
INSERT INTO admin_privs VALUES (9, 'adminad');
INSERT INTO admin_privs VALUES (9, 'admin');
INSERT INTO admin_privs VALUES (9, 'adqueue');
INSERT INTO admin_privs VALUES (9, 'stores');
INSERT INTO admin_privs VALUES (9, 'clearad');
INSERT INTO admin_privs VALUES (9, 'filter');
INSERT INTO admin_privs VALUES (9, 'popular_ads');
INSERT INTO admin_privs VALUES (9, 'search');
INSERT INTO admin_privs VALUES (9, 'credit');
INSERT INTO admin_privs VALUES (9, 'adminedit');
INSERT INTO admin_privs VALUES (9, 'config');
INSERT INTO admin_privs VALUES (9, 'adqueue.admin_settings');
INSERT INTO admin_privs VALUES (9, 'adqueue.show_num_ads=2');
INSERT INTO admin_privs VALUES (9, 'adqueue.admin_queue');
INSERT INTO admin_privs VALUES (9, 'adqueue.refusals');
INSERT INTO admin_privs VALUES (9, 'adqueue.refusals_score');
INSERT INTO admin_privs VALUES (9, 'filter.lists');
INSERT INTO admin_privs VALUES (9, 'filter.rules');
INSERT INTO admin_privs VALUES (9, 'filter.spamfilter');
INSERT INTO admin_privs VALUES (9, 'search.search_ads');
INSERT INTO admin_privs VALUES (9, 'search.uid_emails');
INSERT INTO admin_privs VALUES (9, 'search.mass_delete');
INSERT INTO admin_privs VALUES (9, 'on_call');
INSERT INTO admin_privs VALUES (9, 'Websql');
INSERT INTO admin_privs VALUES (9, 'adqueue.video');
INSERT INTO admin_privs VALUES (9, 'bids');
INSERT INTO admin_privs VALUES (9, 'on_call.duty');
INSERT INTO admin_privs VALUES (9, 'scarface');
INSERT INTO admin_privs VALUES (9, 'ais');
INSERT INTO admin_privs VALUES (9, 'landing_page');
INSERT INTO admin_privs VALUES (9, 'Adminuf');
INSERT INTO admin_privs VALUES (9, 'adqueue.approve_refused');
INSERT INTO admin_privs VALUES (9, 'scarface.warning');


--
-- Data for Name: bid_ads; Type: TABLE DATA; Schema: public; Owner: dany
--



--
-- Data for Name: bid_bids; Type: TABLE DATA; Schema: public; Owner: dany
--



--
-- Data for Name: bid_media; Type: TABLE DATA; Schema: public; Owner: dany
--



--
-- Data for Name: block_lists; Type: TABLE DATA; Schema: public; Owner: dany
--

INSERT INTO block_lists VALUES (1, 'E-post adresser - raderas', true, 'email', NULL);
INSERT INTO block_lists VALUES (2, 'E-post adresser - spamfiltret', true, 'email', NULL);
INSERT INTO block_lists VALUES (3, 'IP-adresser som - raderas', true, 'ip', NULL);
INSERT INTO block_lists VALUES (4, 'IP-adresser - spamfiltret', true, 'ip', NULL);
INSERT INTO block_lists VALUES (5, 'E-postadresser som inte f�r annonsera', false, 'email', NULL);
INSERT INTO block_lists VALUES (6, 'Telefonnummer som inte f�r annonsera', false, 'general', '-+ ');
INSERT INTO block_lists VALUES (7, 'Tipsmottagare', false, 'email', NULL);
INSERT INTO block_lists VALUES (8, 'Synonymer f�r ordet Annonsera', false, 'general', NULL);
INSERT INTO block_lists VALUES (9, 'Synonymer f�r ordet Gratis', false, 'general', NULL);
INSERT INTO block_lists VALUES (10, 'Sajter som kan spamma', false, 'general', NULL);
INSERT INTO block_lists VALUES (11, 'Ord som anv�nds av sajter som spammar', false, 'general', NULL);
INSERT INTO block_lists VALUES (12, 'Fula ord', false, 'general', NULL);
INSERT INTO block_lists VALUES (13, 'F�rbjudna rubriker i L�gg in annons', false, 'general', NULL);
INSERT INTO block_lists VALUES (14, 'Engelska fraser som anv�nds i mejlsvar', false, 'general', NULL);
INSERT INTO block_lists VALUES (15, 'Byten.se', false, 'general', NULL);
INSERT INTO block_lists VALUES (16, 'whitelist', false, 'email', NULL);
INSERT INTO block_lists VALUES (17, 'were_whitelist', false, 'email', NULL);


--
-- Data for Name: block_rules; Type: TABLE DATA; Schema: public; Owner: dany
--

INSERT INTO block_rules VALUES (1, 'Sajtspam', 'delete', 'adreply', 'and', 0, 0, '2013-06-04', NULL);
INSERT INTO block_rules VALUES (2, 'Annonsera gratis', 'delete', 'adreply', 'and', 0, 0, '2013-06-04', NULL);
INSERT INTO block_rules VALUES (3, 'Fula ord', 'stop', 'newad', 'and', 0, 0, '2013-06-04', NULL);
INSERT INTO block_rules VALUES (4, 'Fula ord', 'delete', 'adreply', 'and', 0, 0, '2013-06-04', NULL);
INSERT INTO block_rules VALUES (5, 'Blockerade epostadresser  - raderas', 'delete', 'adreply', 'and', 0, 0, '2013-06-04', NULL);
INSERT INTO block_rules VALUES (6, 'Blockerade epostadresser', 'stop', 'newad', 'and', 0, 0, '2013-06-04', NULL);
INSERT INTO block_rules VALUES (7, 'Blockerade ip-adresser - raderas', 'delete', 'adreply', 'and', 0, 0, '2013-06-04', NULL);
INSERT INTO block_rules VALUES (8, 'Blockerade telefonnummer', 'stop', 'newad', 'and', 0, 0, '2013-06-04', NULL);
INSERT INTO block_rules VALUES (9, 'Blockerade tipsmottagare', 'delete', 'sendtip', 'and', 0, 0, '2013-06-04', NULL);
INSERT INTO block_rules VALUES (10, 'Byten.se', 'delete', 'adreply', 'and', 0, 0, '2013-06-04', NULL);
INSERT INTO block_rules VALUES (11, 'Byten.se', 'delete', 'sendtip', 'and', 0, 0, '2013-06-04', NULL);
INSERT INTO block_rules VALUES (12, 'Blockerade ord i rubriken', 'stop', 'newad', 'and', 0, 0, '2013-06-04', NULL);
INSERT INTO block_rules VALUES (13, 'Blockerade engelska mail', 'spamfilter', 'adreply', 'and', 0, 0, '2013-06-04', NULL);
INSERT INTO block_rules VALUES (14, 'Blockerade epostadresser - spamfilter', 'spamfilter', 'adreply', 'and', 0, 0, '2013-06-04', NULL);
INSERT INTO block_rules VALUES (15, 'Blockerade ip-adresser - spamfilter', 'spamfilter', 'adreply', 'and', 0, 0, '2013-06-04', NULL);
INSERT INTO block_rules VALUES (16, 'Fula ord', 'delete', 'sendtip', 'or', 0, 0, '2013-06-04', NULL);
INSERT INTO block_rules VALUES (17, 'whitelist rule', 'move_to_queue', 'clear', 'and', 0, 8, '2013-08-06', 'whitelist');


--
-- Data for Name: block_rule_conditions; Type: TABLE DATA; Schema: public; Owner: dany
--

INSERT INTO block_rule_conditions VALUES (1, 1, 10, 0, '{subject,body}');
INSERT INTO block_rule_conditions VALUES (2, 1, 11, 0, '{subject,body}');
INSERT INTO block_rule_conditions VALUES (3, 2, 8, 0, '{subject,body}');
INSERT INTO block_rule_conditions VALUES (4, 2, 9, 0, '{subject,body}');
INSERT INTO block_rule_conditions VALUES (5, 3, 12, 0, '{subject,body}');
INSERT INTO block_rule_conditions VALUES (6, 4, 12, 0, '{subject,body}');
INSERT INTO block_rule_conditions VALUES (7, 5, 1, 1, '{email}');
INSERT INTO block_rule_conditions VALUES (8, 6, 5, 1, '{email}');
INSERT INTO block_rule_conditions VALUES (9, 7, 3, 1, '{remote_addr}');
INSERT INTO block_rule_conditions VALUES (10, 8, 6, 1, '{phone}');
INSERT INTO block_rule_conditions VALUES (11, 9, 7, 1, '{email}');
INSERT INTO block_rule_conditions VALUES (12, 10, 15, 0, '{name,email,subject,body}');
INSERT INTO block_rule_conditions VALUES (13, 11, 15, 0, '{name,email,subject,body}');
INSERT INTO block_rule_conditions VALUES (14, 12, 13, 0, '{subject}');
INSERT INTO block_rule_conditions VALUES (15, 13, 14, 0, '{body}');
INSERT INTO block_rule_conditions VALUES (16, 14, 2, 1, '{subject}');
INSERT INTO block_rule_conditions VALUES (17, 15, 4, 1, '{remote_addr}');
INSERT INTO block_rule_conditions VALUES (18, 16, 12, 0, '{body}');
INSERT INTO block_rule_conditions VALUES (19, 17, 16, 1, '{email}');


--
-- Data for Name: blocked_items; Type: TABLE DATA; Schema: public; Owner: dany
--

INSERT INTO blocked_items VALUES (1, 'ful@fisk.se', 1, NULL, NULL);
INSERT INTO blocked_items VALUES (2, 'ful@fisk.se', 5, NULL, NULL);
INSERT INTO blocked_items VALUES (3, '0733555501', 6, NULL, NULL);
INSERT INTO blocked_items VALUES (4, 'ful@fisk.se', 7, NULL, NULL);
INSERT INTO blocked_items VALUES (5, 'fisk.se', 10, NULL, NULL);
INSERT INTO blocked_items VALUES (6, 'vatten', 11, NULL, NULL);
INSERT INTO blocked_items VALUES (7, 'fitta', 12, NULL, NULL);
INSERT INTO blocked_items VALUES (8, 'analakrobat', 12, NULL, NULL);
INSERT INTO blocked_items VALUES (9, 'buy', 13, NULL, NULL);
INSERT INTO blocked_items VALUES (10, 'sale', 13, NULL, NULL);
INSERT INTO blocked_items VALUES (11, 'salu', 13, NULL, NULL);
INSERT INTO blocked_items VALUES (12, 'free', 14, NULL, NULL);
INSERT INTO blocked_items VALUES (13, 'uid1@blocket.se', 16, 'foo', 437);
INSERT INTO blocked_items VALUES (14, 'uid2@blocket.se', 16, 'foo', 439);
INSERT INTO blocked_items VALUES (15, 'prepaid3@blocket.se', 16, 'foo', 441);
INSERT INTO blocked_items VALUES (16, 'prepaid@blocket.se', 16, 'foo', 443);
INSERT INTO blocked_items VALUES (17, 'prepaid5@blocket.se', 16, 'foo', 445);
INSERT INTO blocked_items VALUES (18, 'kim@blocket.se', 16, 'foo', 447);
INSERT INTO blocked_items VALUES (19, 'android@yapo.cl', 16, 'foo', 449);
INSERT INTO blocked_items VALUES (20, 'susana@oria.cl', 16, 'foo', 451);


--
-- Data for Name: conf; Type: TABLE DATA; Schema: public; Owner: dany
--

INSERT INTO conf VALUES ('*.controlpanel.modules.adqueue.settings.auto_abuse', '1', '2013-06-04 10:38:32.916343', NULL);
INSERT INTO conf VALUES ('*.*.common.accepted.1.name', 'Sin fotos', '2013-06-04 10:38:32.918169', NULL);
INSERT INTO conf VALUES ('*.*.common.accepted.1.text', 'Para mejorar las posibilidades de que tu aviso sea visto, te recomendamos que agregues im�genes del producto que ofreces o buscas.', '2013-06-04 10:38:32.918323', NULL);
INSERT INTO conf VALUES ('*.*.common.accepted.2.name', 'Foto principal movida', '2013-06-04 10:38:32.91842', NULL);
INSERT INTO conf VALUES ('*.*.common.accepted.2.text', 'Para mejorar las posibilidades de que tu aviso sea visto, hemos alterado el orden de las fotograf�as', '2013-06-04 10:38:32.918511', NULL);
INSERT INTO conf VALUES ('*.*.common.accepted_order.1', '1', '2013-06-04 10:38:32.918601', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal.0.name', 'T�tulo y Descripci�n de Otros', '2013-06-04 10:38:32.918808', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal.1.name', 'T�tulo y Descripci�n del Veh�culo', '2013-06-04 10:38:32.91891', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal.2.name', 'T�tulo y Descripci�n de los Bienes Ra�ces', '2013-06-04 10:38:32.919021', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal.3.name', 'Restricciones para los Animales Dom�sticos', '2013-06-04 10:38:32.919109', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal.4.name', 'Links para otros Sitios', '2013-06-04 10:38:32.919196', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal.5.name', 'Aviso Empresa', '2013-06-04 10:38:32.919286', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal.6.name', 'Contacto', '2013-06-04 10:38:32.919392', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal.7.name', 'Varios elementos', '2013-06-04 10:38:32.919487', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal.8.name', 'Varios elementos - Empleo', '2013-06-04 10:38:32.919574', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal.9.name', 'Avisos personales', '2013-06-04 10:38:32.919665', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal.10.name', 'Varios elementos Veh�culos - Propiedad', '2013-06-04 10:38:32.919756', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal.11.name', 'Aviso doble', '2013-06-04 10:38:32.919844', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal.12.name', 'Aviso caducado', '2013-06-04 10:38:32.920015', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal.13.name', 'IP extranjero', '2013-06-04 10:38:32.92015', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal.14.name', 'Ilegal', '2013-06-04 10:38:32.920245', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal.15.name', 'Contenido de im�genes', '2013-06-04 10:38:32.920332', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal.16.name', 'Error en la imagen', '2013-06-04 10:38:32.920439', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal.17.name', 'Idioma', '2013-06-04 10:38:32.920528', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal.18.name', 'Link', '2013-06-04 10:38:32.920635', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal.19.name', 'Imagen obscena', '2013-06-04 10:38:32.920753', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal.20.name', 'Ofensivo', '2013-06-04 10:38:32.92086', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal.21.name', 'Pirater�a', '2013-06-04 10:38:32.920971', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal.22.name', 'Elementos', '2013-06-04 10:38:32.92106', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal.23.name', 'Marketing', '2013-06-04 10:38:32.921147', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal.24.name', 'Palabras de B�squeda', '2013-06-04 10:38:32.921253', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal.25.name', 'Contrase�a', '2013-06-04 10:38:32.921362', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal.26.name', 'Virus', '2013-06-04 10:38:32.92148', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal.27.name', 'Estado de origen', '2013-06-04 10:38:32.921567', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal.29.name', 'No es realista', '2013-06-04 10:38:32.921674', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal.30.name', 'Categor�a equivocada', '2013-06-04 10:38:32.921763', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal.31.name', 'Spam', '2013-06-04 10:38:32.92185', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal.32.name', 'Propiedad intelectual', '2013-06-04 10:38:32.921953', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal.33.name', 'Privacidad', '2013-06-04 10:38:32.922068', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal.34.name', 'Intercambios', '2013-06-04 10:38:32.922157', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal.35.name', 'Menores de edad', '2013-06-04 10:38:32.922245', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal.0.text', 'Por favor, comprueba que el nombre incluido / modelo / t�tulo / marca del art�culo que deseas vender, est� de acuerdo con  el t�tulo y la descripci�n del producto. El t�tulo del aviso debe describir el producto o servicio anunciado, no se permiten incluir nombres de empresas o URL (direcci�n Web). No es permitido el uso de caracteres especiales o caracteres en may�sculas en el t�tulo.', '2013-06-04 10:38:32.922336', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal.1.text', 'Por favor,  verifica el nombre y modelo (ejemplo: Honda Civic 1.6]) del veh�culo que deseas vender. En la descripci�n, incluye  informaci�n espec�fica.', '2013-06-04 10:38:32.922492', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal.2.text', 'Comprueba  que incluya el t�tulo. El t�tulo del aviso debe describir el producto o servicio anunciado, no se le permite incluir nombres de empresas o  URL (direcci�n Web). No es permitido el uso de caracteres especiales o caracteres en may�sculas en el t�tulo.', '2013-06-04 10:38:32.922594', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal.3.text', 'No permitimos avisos de animales prohibidos por las leyes chilenas de protecci�n animal.', '2013-06-04 10:38:32.922693', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal.4.text', 'No permitimos la inclusi�n de un enlace dirigido a otra p�gina web de e-mail o n�meros de tel�fono en el texto del aviso. No est� permitido hablar de otros sitios web o las subastas de avisos clasificados. Estos v�nculos y referencias deben ser eliminados antes de volver a enviar la notificaci�n.', '2013-06-04 10:38:32.922785', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal.5.text', 'Has introducido un aviso en forma individual o categor�as que no permiten avisos de empresas. Las empresas deben insertar avisos y hacer una lista como los avisos de empresas. Los avisos de empresas no est�n permitidos en las siguientes categor�as: Video Juegos, telefon�a, ropa y prendas de vestir, bolsos, mochilas y accesorios, joyas, relojes y joyas, etc.', '2013-06-04 10:38:32.922898', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal.6.text', 'No hemos podido verificar tu informaci�n de contacto. Por favor, verifica que toda la informaci�n introducida es correcta. Despu�s de la revisi�n, puedes volver a enviar tu aviso.', '2013-06-04 10:38:32.923011', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal.7.text', 'Hay muchos elementos en el mismo aviso. Por favor, escribe  cada elemento de los avisos por separado. No est� permitido introducir m�s de un veh�culo o propiedad en el mismo aviso, excepto si la transacci�n es un intercambio (por ejemplo, 2 por 1).                             ', '2013-06-04 10:38:32.923136', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal.8.text', 'Hay muchas ofertas de trabajo en el mismo aviso. Escribe una oferta por aviso.  No puedes publicar m�s de una propiedad en el mismo aviso.', '2013-06-04 10:38:32.923236', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal.9.text', 'Has introducido un aviso de una empresa. Los avisos deben ser personales.', '2013-06-04 10:38:32.923345', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal.10.text', 'Hay muchos elementos en el mismo aviso. Escribe cada elemento de la lista por separado, para que tu aviso sea m�s relevante para los compradores. No est� permitido introducir m�s de un veh�culo o propiedad en el mismo aviso, a menos que la transacci�n sea  un intercambio necesario (por  ejemplo, 2 por 1).', '2013-06-04 10:38:32.923435', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal.11.text', 'Ya existe otro aviso como el que has introducido. No puedes copiar el texto de los avisos de otros anunciantes, ya que est�n bajo la ley de derechos de autor.. Tampoco est� permitido publicar varios avisos con el mismo producto  o servicio. El aviso anterior debe ser borrado antes de publicar un nuevo  aviso. Tampoco se permite hacer publicidad del mismo art�culo, o servicio en  las diferentes categor�as de avisos en las diferentes regiones.', '2013-06-04 10:38:32.923529', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal.12.text', 'El  producto  ya no est� disponible/ha expirado/fue vendido.', '2013-06-04 10:38:32.923626', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal.13.text', 'Nuestro sitio ofrece clasificados s�lo en Chile. Los productos o servicios s�lo se  encuentran en  Chile y el aviso ser� colocado en la zona donde se encuentra. No se aceptan avisos de fuera del pa�s o en otro idioma que no sea el espa�ol.', '2013-06-04 10:38:32.923748', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal_order.21', '24', '2013-06-04 10:38:32.928584', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal_order.22', '13', '2013-06-04 10:38:32.928704', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal_order.23', '16', '2013-06-04 10:38:32.928805', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal_order.24', '23', '2013-06-04 10:38:32.928898', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal.14.text', 'Tu aviso contiene productos ilegales y/o prohibidos  para la  venta en nuestro sitio. No est� permito hacer publicidad de productos falsificados o falsos, como los productos de marca, CD / VCD / DVD, software / consola de juegos. Para que el aviso sea aceptado y para que este requisitto se aplique, el anunciante debe garantizar que los productos son originales. Los avisos que incluyen los productos para su reventa a la luz de la legislaci�n chilena no pueden ser publicados. Tenemos restricciones sobre el aviso de ciertos bienes y servicios. Lee nuestros T�rminos. Los servicios ofrecidos o solicitados deben cumplir con las leyes chilenas y reglamentarias aplicables a cada profesi�n.', '2013-06-04 10:38:32.923857', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal.15.text', 'Las im�genes deben concordar con los avisos y deben ser relevantes para el art�culo o servicio anunciado. Las im�genes deben representar el art�culo anunciado. No puedes utilizar las im�genes para art�culos del cat�logo de segunda mano, o utilizar logotipos e im�genes de la empresa, excepto en los "Servicios" y "Empleo". No se permite el uso de im�genes de otros anunciantes, sin su consentimiento previo, ni marcas de agua o logos de sitios de la competencia. Las im�genes est�n protegidas por la legislaci�n sobre derechos de autor. No se permite el uso de una imagen en m�s de un aviso.', '2013-06-04 10:38:32.924257', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal.16.text', 'Una o m�s im�genes contienen errores y no se muestran correctamente. Por favor, aseg�rate que el formato de las  im�genes  es  JPG, GIF o BMP. ', '2013-06-04 10:38:32.924411', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal.17.text', 'Su aviso no est� en espa�ol', '2013-06-04 10:38:32.924508', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal.18.text', 'Est�s utilizando enlaces que no son relevantes para el aviso y/o que no funcionan', '2013-06-04 10:38:32.9246', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal.19.text', 'No se permite  publicar im�genes obscenas que muestren a la gente desnuda, en ropa interior o traje de ba�o.', '2013-06-04 10:38:32.924706', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal.20.text', 'Tu aviso puede ser ofensivo para los grupos �tnicos / religiosos, o puede ser considerado  racista, xen�fobo o terrorista, ya que atenta contra el g�nero humano. No se permiten avisos que violen normas constitucionales y que incorporen  contenidos, mensajes o productos de naturaleza violenta o degradantes.', '2013-06-04 10:38:32.924805', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal.21.text', 'No  est� permitido hacer publicidad de productos falsificados o falsos, como los productos de marca, CD / VCD / DVD, software / consola de juegos. Para que el aviso sea aceptado, el anunciante debe garantizar que los productos son originales. Los avisos que incluyen los productos para su reventa a la luz de la legislaci�n chilena, no pueden ser publicados.', '2013-06-04 10:38:32.924933', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal.22.text', 'S�lo se  permite hacer publicidad de ventas, arriendos, empleo y servicios.', '2013-06-04 10:38:32.92504', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal.23.text', 'El aviso contiene productos o servicios que no est�n permitidos en nuestro sitio. Para m�s informaci�n sobre estos productos, visite la p�gina de las Reglas. Si usted tiene alguna pregunta p�ngase en contacto con Atenci�n al Cliente.', '2013-06-04 10:38:32.92513', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal.24.text', 'No est�  permitido el uso de avisos  que tengan como �nico prop�sito el  marketing,  lo que no implica necesariamente el suministro de bienes, trabajos o servicios.', '2013-06-04 10:38:32.92524', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal.25.text', 'No est�  permitido el uso de avisos  que tengan como �nico prop�sito el  marketing,  lo que no implica necesariamente el suministro de bienes, trabajos o servicios.', '2013-06-04 10:38:32.925348', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal.26.text', 'No se pueden  introducir o difundir en la red,  programas de datos (virus y software maliciosos) que puedan  causar da�os al proveedor de acceso, a sistemas inform�ticos  de nuestros usuarios del sitio o de terceros que utilicen la misma red.', '2013-06-04 10:38:32.925448', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal.27.text', 'No  se ha declarado la autenticidad de tu producto. Para que el aviso sea  aceptado, el anunciante debe garantizar que los productos son originales.', '2013-06-04 10:38:32.925546', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal.29.text', 'No se permite la publicaci�n de avisos que no posean ofertas cre�bles y realistas. No se permite que los avisos contengan cualquier informaci�n con contenidos falsos, ambiguos o inexactos, con el fin de inducir al error a potenciales receptores de dicha informaci�n.', '2013-06-04 10:38:32.925637', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal.30.text', 'El aviso se publicar� en la categor�a que mejor describa el art�culo o servicio. Nos reservamos el derecho, si es necesario, a mover el aviso a la categor�a m�s apropiada. Bienes y servicios que no entren en la misma categor�a ser�n publicados en diferentes avisos. Para la venta se publicar� en "Se vende" y los avisos que demanden  un producto se publicar�n en "Se compra". En algunas categor�as los avisos podr�an incluir las opciones de "Arriendo" y "Se busca  arriendo". En otras categor�as, si es necesario, los avisos de "Se arrienda"  se publicar�n en "Venta" y los avisos "Quiero  arrendar", se publicar�n en "Se compra".', '2013-06-04 10:38:32.925736', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal.31.text', 'No est� permitido enviar publicidad no solicitada o autorizada, material publicitario, "correo basura", "cartas en cadena", "marketing piramidal" o cualquier otra forma de solicitud.Tu aviso se inscribe en estas categor�as.', '2013-06-04 10:38:32.925864', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal.32.text', 'No se permite difundir, transmitir o poner a disposici�n de terceros cualquier tipo de informaci�n, elemento o contenido que implica la violaci�n de los derechos de propiedad intelectual, incluyendo derechos de autor y propiedad industrial,  marcas, derechos de autor o de propiedad de los due�os de este sitio o de terceros.', '2013-06-04 10:38:32.925992', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal.33.text', 'No se permite difundir, transmitir o poner a disposici�n de terceros, cualquier tipo de informaci�n, elemento o contenido que implique  la violaci�n del secreto de las comunicaciones y la intimidad.', '2013-06-04 10:38:32.926111', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal.34.text', 'No se permiten m�s de cinco referencias a los productos  que podr�an constituir la base del intercambio. Los intercambios est�n permitidos en el sitio, pero se debe hacer una lista de menos de cinco referencias a los productos que forman la base del intercambio.', '2013-06-04 10:38:32.926217', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal.35.text', 'Categor�a errada', '2013-06-04 10:38:32.926309', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal_order.0', '32', '2013-06-04 10:38:32.9264', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal_order.1', '33', '2013-06-04 10:38:32.926509', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal_order.2', '31', '2013-06-04 10:38:32.926606', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal_order.3', '27', '2013-06-04 10:38:32.926719', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal_order.4', '25', '2013-06-04 10:38:32.926841', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal_order.5', '0', '2013-06-04 10:38:32.926939', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal_order.6', '5', '2013-06-04 10:38:32.92704', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal_order.7', '18', '2013-06-04 10:38:32.92713', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal_order.8', '19', '2013-06-04 10:38:32.927221', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal_order.9', '3', '2013-06-04 10:38:32.927333', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal_order.10', '20', '2013-06-04 10:38:32.927435', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal_order.11', '2', '2013-06-04 10:38:32.927536', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal_order.12', '1', '2013-06-04 10:38:32.927629', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal_order.13', '12', '2013-06-04 10:38:32.92774', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal_order.14', '10', '2013-06-04 10:38:32.927834', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal_order.15', '6', '2013-06-04 10:38:32.927926', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal_order.16', '7', '2013-06-04 10:38:32.928066', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal_order.17', '9', '2013-06-04 10:38:32.928179', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal_order.18', '14', '2013-06-04 10:38:32.928274', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal_order.19', '11', '2013-06-04 10:38:32.928367', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal_order.20', '22', '2013-06-04 10:38:32.928458', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal_order.25', '29', '2013-06-04 10:38:32.929007', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal_order.26', '34', '2013-06-04 10:38:32.929182', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal_order.27', '8', '2013-06-04 10:38:32.929296', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal_order.28', '35', '2013-06-04 10:38:32.929421', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal_order.29', '21', '2013-06-04 10:38:32.929517', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal_order.30', '4', '2013-06-04 10:38:32.929611', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal_order.31', '30', '2013-06-04 10:38:32.929703', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal_order.32', '26', '2013-06-04 10:38:32.929795', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal_order.33', '25', '2013-06-04 10:38:32.929904', NULL);
INSERT INTO conf VALUES ('*.*.common.refusal_order.35', '17', '2013-06-04 10:38:32.930025', NULL);
INSERT INTO conf VALUES ('*.*.ais.synonyms.word.1.word', 'caravana', '2013-06-04 10:38:32.93013', NULL);
INSERT INTO conf VALUES ('*.*.ais.synonyms.word.1.synonyms.2', 'casa-m�vel', '2013-06-04 10:38:32.930217', NULL);
INSERT INTO conf VALUES ('*.*.ais.synonyms.word.1.synonyms.4', 'roulotte', '2013-06-04 10:38:32.930309', NULL);
INSERT INTO conf VALUES ('*.*.ais.synonyms.word.2.word', 'piso', '2013-06-04 10:38:32.930398', NULL);
INSERT INTO conf VALUES ('*.*.ais.synonyms.word.2.synonyms.1', 'apartamento', '2013-06-04 10:38:32.930506', NULL);
INSERT INTO conf VALUES ('*.*.ais.synonyms.word.2.synonyms.2', 'loft', '2013-06-04 10:38:32.930625', NULL);
INSERT INTO conf VALUES ('*.*.ais.synonyms.word.3.word', 'vw', '2013-06-04 10:38:32.930753', NULL);
INSERT INTO conf VALUES ('*.*.ais.synonyms.word.3.synonyms.1', 'volkswagen', '2013-06-04 10:38:32.930841', NULL);
INSERT INTO conf VALUES ('*.*.ais.synonyms.word.4.word', 'carro', '2013-06-04 10:38:32.93093', NULL);
INSERT INTO conf VALUES ('*.*.ais.synonyms.word.4.synonyms.1', 'autom�vel', '2013-06-04 10:38:32.931054', NULL);
INSERT INTO conf VALUES ('*.*.ais.synonyms.word.4.synonyms.2', 'buga', '2013-06-04 10:38:32.931162', NULL);
INSERT INTO conf VALUES ('*.*.ais.misspellings.word.1.misspelled.1', 'vlokswagen', '2013-06-04 10:38:32.93126', NULL);
INSERT INTO conf VALUES ('*.*.ais.misspellings.word.1.misspelled.2', 'volksbagen', '2013-06-04 10:38:32.931365', NULL);
INSERT INTO conf VALUES ('*.*.ais.misspellings.word.1.misspelled.3', 'volsvagen', '2013-06-04 10:38:32.931472', NULL);
INSERT INTO conf VALUES ('*.*.ais.misspellings.word.1.intended', 'volkswagen', '2013-06-04 10:38:32.931561', NULL);
INSERT INTO conf VALUES ('*.*.ais.misspellings.word.2.misspelled.1', 'apratamento', '2013-06-04 10:38:32.931648', NULL);
INSERT INTO conf VALUES ('*.*.ais.misspellings.word.2.misspelled.2', 'apatamento', '2013-06-04 10:38:32.931755', NULL);
INSERT INTO conf VALUES ('*.*.ais.misspellings.word.2.misspelled.3', 'apartametno', '2013-06-04 10:38:32.931908', NULL);
INSERT INTO conf VALUES ('*.*.ais.misspellings.word.2.intended', 'apartamento', '2013-06-04 10:38:32.932023', NULL);
INSERT INTO conf VALUES ('*.*.ais.misspellings.word.3.misspelled.1', 'carabana', '2013-06-04 10:38:32.932116', NULL);
INSERT INTO conf VALUES ('*.*.ais.misspellings.word.3.misspelled.2', 'cravana', '2013-06-04 10:38:32.932204', NULL);
INSERT INTO conf VALUES ('*.*.ais.misspellings.word.3.intended', 'caravana', '2013-06-04 10:38:32.93229', NULL);
INSERT INTO conf VALUES ('*.*.common.uf_conversion_factor', '22245,5290', '2013-06-04 10:38:32.932384', NULL);
INSERT INTO conf VALUES ('*.*.common.uf_conversion_updated', '2013-06-04 10:38:32.932492-04', '2013-06-04 10:38:32.932492', NULL);


--
-- Data for Name: event_log; Type: TABLE DATA; Schema: public; Owner: dany
--

INSERT INTO event_log VALUES (1, 'blocked_item', 'INSERT blocked_item uid1@blocket.se, 16, "foo"', 437, '2013-08-06 15:21:18.079253');
INSERT INTO event_log VALUES (2, 'blocked_item', 'INSERT blocked_item uid2@blocket.se, 16, "foo"', 439, '2013-08-06 15:21:23.174856');
INSERT INTO event_log VALUES (3, 'blocked_item', 'INSERT blocked_item prepaid3@blocket.se, 16, "foo"', 441, '2013-08-06 15:21:29.297166');
INSERT INTO event_log VALUES (4, 'blocked_item', 'INSERT blocked_item prepaid@blocket.se, 16, "foo"', 443, '2013-08-06 15:21:34.132843');
INSERT INTO event_log VALUES (5, 'blocked_item', 'INSERT blocked_item prepaid5@blocket.se, 16, "foo"', 445, '2013-08-06 15:21:38.97521');
INSERT INTO event_log VALUES (6, 'blocked_item', 'INSERT blocked_item kim@blocket.se, 16, "foo"', 447, '2013-08-06 15:21:44.17299');
INSERT INTO event_log VALUES (7, 'blocked_item', 'INSERT blocked_item android@yapo.cl, 16, "foo"', 449, '2013-08-06 15:21:51.750802');
INSERT INTO event_log VALUES (8, 'blocked_item', 'INSERT blocked_item susana@oria.cl, 16, "foo"', 451, '2013-08-06 15:21:57.667716');


--
-- Data for Name: example; Type: TABLE DATA; Schema: public; Owner: dany
--

INSERT INTO example VALUES (0, 12);
INSERT INTO example VALUES (1, 56);
INSERT INTO example VALUES (2, 32);
INSERT INTO example VALUES (3, 156);
INSERT INTO example VALUES (4, 2345);


--
-- Data for Name: filters; Type: TABLE DATA; Schema: public; Owner: dany
--

INSERT INTO filters VALUES (1, 'all', '', '1=1', true);
INSERT INTO filters VALUES (2, 'unpaid', '', 'ad_actions.state = ''unpaid''', false);
INSERT INTO filters VALUES (3, 'unverified', '', 'ad_actions.state = ''unverified''', false);
INSERT INTO filters VALUES (4, 'new', '', 'ad_actions.queue = ''normal'' AND ad_actions.state IN (''pending_review'', ''locked'')', false);
INSERT INTO filters VALUES (5, 'unsolved', '', 'ad_actions.queue = ''unsolved'' AND ad_actions.state IN (''pending_review'',''locked'')', false);
INSERT INTO filters VALUES (6, 'abuse', '', 'ad_actions.queue = ''abuse'' AND ad_actions.state IN (''pending_review'',''locked'')', false);
INSERT INTO filters VALUES (7, 'accepted', '', 'ad_actions.state = ''accepted''', false);
INSERT INTO filters VALUES (8, 'refused', '', 'ad_actions.state = ''refused''', true);
INSERT INTO filters VALUES (9, 'technical_error', '', 'ad_actions.queue = ''technical_error'' AND ad_actions.state NOT IN (''accepted'', ''refused'')', false);
INSERT INTO filters VALUES (10, 'published', '', 'ad_actions.state IN (''accepted'', ''published'') AND NOT EXISTS (SELECT * FROM action_states AS newer WHERE ad_id = ad_actions.ad_id AND action_id != ad_actions.action_id AND state = ''accepted'' AND timestamp > newer.timestamp) AND NOT EXISTS ( SELECT * FROM ads WHERE ads.status = ''deleted'' AND ad_id=ad_actions.ad_id)', false);
INSERT INTO filters VALUES (11, 'deleted', '', 'ad_actions.state = ''deleted''', true);
INSERT INTO filters VALUES (12, 'autoaccept', '', 'ad_actions.queue = ''autoaccept'' AND ad_actions.state IN (''pending_review'', ''locked'')', false);
INSERT INTO filters VALUES (13, 'autorefuse', '', 'ad_actions.queue = ''autorefuse'' AND ad_actions.state IN (''pending_review'', ''locked'')', false);
INSERT INTO filters VALUES (16, 'video', '', 'ad_actions.queue = ''video'' AND ad_actions.state IN (''pending_review'', ''locked'')', false);
INSERT INTO filters VALUES (17, 'spidered', '', 'ad_actions.queue = ''spidered'' AND ad_actions.state IN (''pending_review'', ''locked'')', false);


--
-- Data for Name: hold_mail_params; Type: TABLE DATA; Schema: public; Owner: dany
--



--
-- Data for Name: iteminfo_items; Type: TABLE DATA; Schema: public; Owner: dany
--

INSERT INTO iteminfo_items VALUES (1, 'root', false, NULL);
INSERT INTO iteminfo_items VALUES (2, 'primogenito', true, 1);
INSERT INTO iteminfo_items VALUES (3, 'segundon', false, 1);
INSERT INTO iteminfo_items VALUES (4, 'car_info', false, 1);
INSERT INTO iteminfo_items VALUES (5, 'location_info', false, 1);
INSERT INTO iteminfo_items VALUES (6, 'object_info', false, 1);
INSERT INTO iteminfo_items VALUES (7, 'moto_info', false, 1);
INSERT INTO iteminfo_items VALUES (8, 'sac', false, 6);
INSERT INTO iteminfo_items VALUES (9, 'chanel', false, 8);
INSERT INTO iteminfo_items VALUES (10, 'bmw', false, 7);
INSERT INTO iteminfo_items VALUES (11, 'triumph', false, 7);
INSERT INTO iteminfo_items VALUES (12, 'r100', true, 10);
INSERT INTO iteminfo_items VALUES (13, 'r1100 gs', true, 10);
INSERT INTO iteminfo_items VALUES (14, 'k100', true, 10);
INSERT INTO iteminfo_items VALUES (15, 'america', true, 11);
INSERT INTO iteminfo_items VALUES (16, 'adventurer', true, 11);
INSERT INTO iteminfo_items VALUES (17, '1000', false, 12);
INSERT INTO iteminfo_items VALUES (18, '1100', false, 13);
INSERT INTO iteminfo_items VALUES (19, '1100 75 ans', false, 13);
INSERT INTO iteminfo_items VALUES (20, '1100 abs', false, 13);
INSERT INTO iteminfo_items VALUES (21, '1100 abs ergo', false, 13);
INSERT INTO iteminfo_items VALUES (22, '1100 ergo', false, 13);
INSERT INTO iteminfo_items VALUES (23, 'k100 lt', false, 14);
INSERT INTO iteminfo_items VALUES (24, 'k100 rs', false, 14);
INSERT INTO iteminfo_items VALUES (25, '865', false, 15);
INSERT INTO iteminfo_items VALUES (26, '900', false, 16);
INSERT INTO iteminfo_items VALUES (27, 'ford', false, 4);
INSERT INTO iteminfo_items VALUES (28, 'renault', false, 4);
INSERT INTO iteminfo_items VALUES (29, 'seat', false, 4);
INSERT INTO iteminfo_items VALUES (30, 'escort', true, 27);
INSERT INTO iteminfo_items VALUES (31, 'fiesta', true, 27);
INSERT INTO iteminfo_items VALUES (32, 'laguna', true, 28);
INSERT INTO iteminfo_items VALUES (33, 'megane', true, 28);
INSERT INTO iteminfo_items VALUES (34, 'scenic', true, 28);
INSERT INTO iteminfo_items VALUES (35, 'cordoba', true, 29);
INSERT INTO iteminfo_items VALUES (36, 'ibiza', true, 29);
INSERT INTO iteminfo_items VALUES (37, 'leon', true, 29);
INSERT INTO iteminfo_items VALUES (38, 'cosworth', false, 30);
INSERT INTO iteminfo_items VALUES (39, 'rs 2000', false, 30);
INSERT INTO iteminfo_items VALUES (40, '1.8 turbo', false, 31);
INSERT INTO iteminfo_items VALUES (41, '1.4 senso 5p', false, 31);
INSERT INTO iteminfo_items VALUES (42, '1.8 16s', false, 32);
INSERT INTO iteminfo_items VALUES (43, '1.8 gpl', false, 32);
INSERT INTO iteminfo_items VALUES (44, '2.0 rxe', false, 32);
INSERT INTO iteminfo_items VALUES (45, '1.9 dti', false, 32);
INSERT INTO iteminfo_items VALUES (46, '2.2 d rt', false, 32);
INSERT INTO iteminfo_items VALUES (47, '2.2 d rxe', false, 32);
INSERT INTO iteminfo_items VALUES (48, '1.9 dci 110 authentique', false, 32);
INSERT INTO iteminfo_items VALUES (49, '2.2 dci 150 initiale', false, 32);
INSERT INTO iteminfo_items VALUES (50, '1.9 dci 120 ch expression', false, 32);
INSERT INTO iteminfo_items VALUES (51, '1.9 sdi 3p', false, 36);
INSERT INTO iteminfo_items VALUES (52, '1.9 tdi 105 reference', false, 37);
INSERT INTO iteminfo_items VALUES (53, '2.0 tdi 140 stylance', false, 37);
INSERT INTO iteminfo_items VALUES (54, 'tdi 110 signo', false, 37);
INSERT INTO iteminfo_items VALUES (55, 'tdi 150 fr', false, 37);
INSERT INTO iteminfo_items VALUES (56, 'tdi 150 sport', false, 37);
INSERT INTO iteminfo_items VALUES (57, 'rx4 1.9 dci', false, 34);
INSERT INTO iteminfo_items VALUES (58, '1.9 d rte', false, 34);
INSERT INTO iteminfo_items VALUES (59, 'd rte', false, 34);
INSERT INTO iteminfo_items VALUES (2001, '22', false, 5);
INSERT INTO iteminfo_items VALUES (2002, '01', false, 2001);
INSERT INTO iteminfo_items VALUES (2003, '01000', false, 2002);
INSERT INTO iteminfo_items VALUES (2004, 'bourg en bresse', false, 2003);
INSERT INTO iteminfo_items VALUES (2005, 'st denis les bourg', false, 2003);
INSERT INTO iteminfo_items VALUES (2006, '12', false, 5);
INSERT INTO iteminfo_items VALUES (2007, '75', false, 2006);
INSERT INTO iteminfo_items VALUES (2008, '75001', false, 2007);
INSERT INTO iteminfo_items VALUES (2009, 'paris', false, 2008);


--
-- Data for Name: iteminfo_data; Type: TABLE DATA; Schema: public; Owner: dany
--

INSERT INTO iteminfo_data VALUES (1, 'Hipoteca', 'De cojones', 1);
INSERT INTO iteminfo_data VALUES (2, 'Sueldo', 'Justico', 1);
INSERT INTO iteminfo_data VALUES (3, 'Broncas', 'Todas', 2);
INSERT INTO iteminfo_data VALUES (4, 'Bici', 'Heredada de su hermano', 3);
INSERT INTO iteminfo_data VALUES (5, 'price_logic_max_cat_2120', '10000', 6);
INSERT INTO iteminfo_data VALUES (6, 'price_logic_max_cat_3040', '20000', 6);
INSERT INTO iteminfo_data VALUES (7, 'price_logic_max_cat_3060', '10000', 6);
INSERT INTO iteminfo_data VALUES (8, 'price_logic_max_cat_8020', '20000', 6);
INSERT INTO iteminfo_data VALUES (9, 'price_logic_max_cat_8020', '560', 8);
INSERT INTO iteminfo_data VALUES (10, 'price_logic_max_cat_22', '1050', 9);
INSERT INTO iteminfo_data VALUES (11, 'price_fraud_cat_8020', '1050', 9);
INSERT INTO iteminfo_data VALUES (12, 'price_logic_max_cat_8060', '2000', 6);
INSERT INTO iteminfo_data VALUES (13, 'price_logic_max_cat_4100', '15000', 6);
INSERT INTO iteminfo_data VALUES (14, 'price_logic_max_cat_4120', '10000', 6);
INSERT INTO iteminfo_data VALUES (15, 'price_logic_max_cat_39', '10000', 6);
INSERT INTO iteminfo_data VALUES (16, 'price_logic_max_cat_41', '20000', 6);
INSERT INTO iteminfo_data VALUES (17, 'price_fraud_1976', '1600', 12);
INSERT INTO iteminfo_data VALUES (18, 'price_logic_min_1976', '2560', 12);
INSERT INTO iteminfo_data VALUES (19, 'price_logic_max_1976', '6400', 12);
INSERT INTO iteminfo_data VALUES (20, 'price_fraud_1977', '975', 12);
INSERT INTO iteminfo_data VALUES (21, 'price_logic_min_1977', '1560', 12);
INSERT INTO iteminfo_data VALUES (22, 'price_logic_max_1977', '3900', 12);
INSERT INTO iteminfo_data VALUES (23, 'price_fraud_1978', '1498.5', 12);
INSERT INTO iteminfo_data VALUES (24, 'price_logic_min_1978', '2397.6', 12);
INSERT INTO iteminfo_data VALUES (25, 'price_logic_max_1978', '5994', 12);
INSERT INTO iteminfo_data VALUES (26, 'price_fraud_1979', '1450', 12);
INSERT INTO iteminfo_data VALUES (27, 'price_logic_min_1979', '2320', 12);
INSERT INTO iteminfo_data VALUES (28, 'price_logic_max_1979', '5800', 12);
INSERT INTO iteminfo_data VALUES (29, 'price_fraud_1980', '1768', 12);
INSERT INTO iteminfo_data VALUES (30, 'price_logic_min_1980', '2828.8', 12);
INSERT INTO iteminfo_data VALUES (31, 'price_logic_max_1980', '7072', 12);
INSERT INTO iteminfo_data VALUES (32, 'price_fraud_1981', '1325', 12);
INSERT INTO iteminfo_data VALUES (33, 'price_logic_min_1981', '2120', 12);
INSERT INTO iteminfo_data VALUES (34, 'price_logic_max_1981', '5300', 12);
INSERT INTO iteminfo_data VALUES (35, 'price_fraud_1982', '1325', 12);
INSERT INTO iteminfo_data VALUES (36, 'price_logic_min_1982', '2120', 12);
INSERT INTO iteminfo_data VALUES (37, 'price_logic_max_1982', '5300', 12);
INSERT INTO iteminfo_data VALUES (38, 'price_fraud_1983', '1500', 12);
INSERT INTO iteminfo_data VALUES (39, 'price_logic_min_1983', '2400', 12);
INSERT INTO iteminfo_data VALUES (40, 'price_logic_max_1983', '6000', 12);
INSERT INTO iteminfo_data VALUES (41, 'price_fraud_1984', '1900', 12);
INSERT INTO iteminfo_data VALUES (42, 'price_logic_min_1984', '3040', 12);
INSERT INTO iteminfo_data VALUES (43, 'price_logic_max_1984', '7600', 12);
INSERT INTO iteminfo_data VALUES (44, 'price_fraud_1988', '1450', 12);
INSERT INTO iteminfo_data VALUES (45, 'price_logic_min_1988', '2320', 12);
INSERT INTO iteminfo_data VALUES (46, 'price_logic_max_1988', '5800', 12);
INSERT INTO iteminfo_data VALUES (47, 'price_fraud_1989', '1500', 12);
INSERT INTO iteminfo_data VALUES (48, 'price_logic_min_1989', '2400', 12);
INSERT INTO iteminfo_data VALUES (49, 'price_logic_max_1989', '6000', 12);
INSERT INTO iteminfo_data VALUES (50, 'price_fraud_1990', '2000', 12);
INSERT INTO iteminfo_data VALUES (51, 'price_logic_min_1990', '3200', 12);
INSERT INTO iteminfo_data VALUES (52, 'price_logic_max_1990', '8000', 12);
INSERT INTO iteminfo_data VALUES (53, 'price_fraud_1991', '1900', 12);
INSERT INTO iteminfo_data VALUES (54, 'price_logic_min_1991', '3040', 12);
INSERT INTO iteminfo_data VALUES (55, 'price_logic_max_1991', '7600', 12);
INSERT INTO iteminfo_data VALUES (56, 'price_fraud_1992', '1942.5', 12);
INSERT INTO iteminfo_data VALUES (57, 'price_logic_min_1992', '3108', 12);
INSERT INTO iteminfo_data VALUES (58, 'price_logic_max_1992', '7770', 12);
INSERT INTO iteminfo_data VALUES (59, 'price_fraud_1993', '2322.5', 12);
INSERT INTO iteminfo_data VALUES (60, 'price_logic_min_1993', '3716', 12);
INSERT INTO iteminfo_data VALUES (61, 'price_logic_max_1993', '9290', 12);
INSERT INTO iteminfo_data VALUES (62, 'price_fraud_1994', '2625', 12);
INSERT INTO iteminfo_data VALUES (63, 'price_logic_min_1994', '4200', 12);
INSERT INTO iteminfo_data VALUES (64, 'price_logic_max_1994', '10500', 12);
INSERT INTO iteminfo_data VALUES (65, 'price_fraud_1995', '1691.5', 12);
INSERT INTO iteminfo_data VALUES (66, 'price_logic_min_1995', '2706.4', 12);
INSERT INTO iteminfo_data VALUES (67, 'price_logic_max_1995', '6766', 12);
INSERT INTO iteminfo_data VALUES (68, 'price_fraud_1996', '2525', 12);
INSERT INTO iteminfo_data VALUES (69, 'price_logic_min_1996', '4040', 12);
INSERT INTO iteminfo_data VALUES (70, 'price_logic_max_1996', '10100', 12);
INSERT INTO iteminfo_data VALUES (71, 'price_fraud_1975', '2487.5', 11);
INSERT INTO iteminfo_data VALUES (72, 'price_logic_min_1975', '3980', 11);
INSERT INTO iteminfo_data VALUES (73, 'price_logic_max_1975', '9950', 11);
INSERT INTO iteminfo_data VALUES (74, 'price_fraud_1977', '2150', 11);
INSERT INTO iteminfo_data VALUES (75, 'price_logic_min_1977', '3440', 11);
INSERT INTO iteminfo_data VALUES (76, 'price_logic_max_1977', '8600', 11);
INSERT INTO iteminfo_data VALUES (77, 'price_fraud_1978', '2400', 11);
INSERT INTO iteminfo_data VALUES (78, 'price_logic_min_1978', '3840', 11);
INSERT INTO iteminfo_data VALUES (79, 'price_logic_max_1978', '9600', 11);
INSERT INTO iteminfo_data VALUES (80, 'price_fraud_1979', '2625', 11);
INSERT INTO iteminfo_data VALUES (81, 'price_logic_min_1979', '4200', 11);
INSERT INTO iteminfo_data VALUES (82, 'price_logic_max_1979', '10500', 11);
INSERT INTO iteminfo_data VALUES (83, 'price_fraud_1980', '3050', 11);
INSERT INTO iteminfo_data VALUES (84, 'price_logic_min_1980', '4880', 11);
INSERT INTO iteminfo_data VALUES (85, 'price_logic_max_1980', '12200', 11);
INSERT INTO iteminfo_data VALUES (86, 'price_fraud_1981', '2750', 11);
INSERT INTO iteminfo_data VALUES (87, 'price_logic_min_1981', '4400', 11);
INSERT INTO iteminfo_data VALUES (88, 'price_logic_max_1981', '11000', 11);
INSERT INTO iteminfo_data VALUES (89, 'price_fraud_1986', '6750', 11);
INSERT INTO iteminfo_data VALUES (90, 'price_logic_min_1986', '10800', 11);
INSERT INTO iteminfo_data VALUES (91, 'price_logic_max_1986', '27000', 11);
INSERT INTO iteminfo_data VALUES (92, 'price_fraud_1992', '1428.5', 11);
INSERT INTO iteminfo_data VALUES (93, 'price_logic_min_1992', '2285.6', 11);
INSERT INTO iteminfo_data VALUES (94, 'price_logic_max_1992', '5714', 11);
INSERT INTO iteminfo_data VALUES (95, 'price_fraud_1993', '1461', 11);
INSERT INTO iteminfo_data VALUES (96, 'price_logic_min_1993', '2337.6', 11);
INSERT INTO iteminfo_data VALUES (97, 'price_logic_max_1993', '5844', 11);
INSERT INTO iteminfo_data VALUES (98, 'price_fraud_1994', '1866.5', 11);
INSERT INTO iteminfo_data VALUES (99, 'price_logic_min_1994', '2986.4', 11);
INSERT INTO iteminfo_data VALUES (100, 'price_logic_max_1994', '7466', 11);
INSERT INTO iteminfo_data VALUES (101, 'price_fraud_1995', '1736.5', 11);
INSERT INTO iteminfo_data VALUES (102, 'price_logic_min_1995', '2778.4', 11);
INSERT INTO iteminfo_data VALUES (103, 'price_logic_max_1995', '6946', 11);
INSERT INTO iteminfo_data VALUES (104, 'price_fraud_1996', '1842.5', 11);
INSERT INTO iteminfo_data VALUES (105, 'price_logic_min_1996', '2948', 11);
INSERT INTO iteminfo_data VALUES (106, 'price_logic_max_1996', '7370', 11);
INSERT INTO iteminfo_data VALUES (107, 'price_fraud_1997', '1906', 11);
INSERT INTO iteminfo_data VALUES (108, 'price_logic_min_1997', '3049.6', 11);
INSERT INTO iteminfo_data VALUES (109, 'price_logic_max_1997', '7624', 11);
INSERT INTO iteminfo_data VALUES (110, 'price_fraud_1998', '2011.5', 11);
INSERT INTO iteminfo_data VALUES (111, 'price_logic_min_1998', '3218.4', 11);
INSERT INTO iteminfo_data VALUES (112, 'price_logic_max_1998', '8046', 11);
INSERT INTO iteminfo_data VALUES (113, 'price_fraud_1999', '2202.5', 11);
INSERT INTO iteminfo_data VALUES (114, 'price_logic_min_1999', '3524', 11);
INSERT INTO iteminfo_data VALUES (115, 'price_logic_max_1999', '8810', 11);
INSERT INTO iteminfo_data VALUES (116, 'price_fraud_2000', '2071.5', 11);
INSERT INTO iteminfo_data VALUES (117, 'price_logic_min_2000', '3314.4', 11);
INSERT INTO iteminfo_data VALUES (118, 'price_logic_max_2000', '8286', 11);
INSERT INTO iteminfo_data VALUES (119, 'price_fraud_2001', '2397.5', 11);
INSERT INTO iteminfo_data VALUES (120, 'price_logic_min_2001', '3836', 11);
INSERT INTO iteminfo_data VALUES (121, 'price_logic_max_2001', '9590', 11);
INSERT INTO iteminfo_data VALUES (122, 'price_fraud_2002', '2549', 11);
INSERT INTO iteminfo_data VALUES (123, 'price_logic_min_2002', '4078.4', 11);
INSERT INTO iteminfo_data VALUES (124, 'price_logic_max_2002', '10196', 11);
INSERT INTO iteminfo_data VALUES (125, 'price_fraud_2003', '2785', 11);
INSERT INTO iteminfo_data VALUES (126, 'price_logic_min_2003', '4456', 11);
INSERT INTO iteminfo_data VALUES (127, 'price_logic_max_2003', '11140', 11);
INSERT INTO iteminfo_data VALUES (128, 'price_fraud_2004', '3007', 11);
INSERT INTO iteminfo_data VALUES (129, 'price_logic_min_2004', '4811.2', 11);
INSERT INTO iteminfo_data VALUES (130, 'price_logic_max_2004', '12028', 11);
INSERT INTO iteminfo_data VALUES (131, 'price_fraud_2005', '4200.5', 11);
INSERT INTO iteminfo_data VALUES (132, 'price_logic_min_2005', '6720.8', 11);
INSERT INTO iteminfo_data VALUES (133, 'price_logic_max_2005', '16802', 11);
INSERT INTO iteminfo_data VALUES (134, 'price_fraud_2006', '4121.5', 11);
INSERT INTO iteminfo_data VALUES (135, 'price_logic_min_2006', '6594.4', 11);
INSERT INTO iteminfo_data VALUES (136, 'price_logic_max_2006', '16486', 11);
INSERT INTO iteminfo_data VALUES (137, 'price_fraud_2007', '4257.5', 11);
INSERT INTO iteminfo_data VALUES (138, 'price_logic_min_2007', '6812', 11);
INSERT INTO iteminfo_data VALUES (139, 'price_logic_max_2007', '17030', 11);
INSERT INTO iteminfo_data VALUES (140, 'price_fraud_2008', '4357.5', 11);
INSERT INTO iteminfo_data VALUES (141, 'price_logic_min_2008', '6972', 11);
INSERT INTO iteminfo_data VALUES (142, 'price_logic_max_2008', '17430', 11);
INSERT INTO iteminfo_data VALUES (143, 'price_fraud_2001', '2995', 15);
INSERT INTO iteminfo_data VALUES (144, 'price_logic_min_2001', '4792', 15);
INSERT INTO iteminfo_data VALUES (145, 'price_logic_max_2001', '11980', 15);
INSERT INTO iteminfo_data VALUES (146, 'price_fraud_2002', '2625', 15);
INSERT INTO iteminfo_data VALUES (147, 'price_logic_min_2002', '4200', 15);
INSERT INTO iteminfo_data VALUES (148, 'price_logic_max_2002', '10500', 15);
INSERT INTO iteminfo_data VALUES (149, 'price_fraud_2003', '2600', 15);
INSERT INTO iteminfo_data VALUES (150, 'price_logic_min_2003', '4160', 15);
INSERT INTO iteminfo_data VALUES (151, 'price_logic_max_2003', '10400', 15);
INSERT INTO iteminfo_data VALUES (152, 'price_fraud_2004', '3250', 15);
INSERT INTO iteminfo_data VALUES (153, 'price_logic_min_2004', '5200', 15);
INSERT INTO iteminfo_data VALUES (154, 'price_logic_max_2004', '13000', 15);
INSERT INTO iteminfo_data VALUES (155, 'price_fraud_2007', '3750', 15);
INSERT INTO iteminfo_data VALUES (156, 'price_logic_min_2007', '6000', 15);
INSERT INTO iteminfo_data VALUES (157, 'price_logic_max_2007', '15000', 15);
INSERT INTO iteminfo_data VALUES (158, 'price_fraud_1975', '1720.5', 10);
INSERT INTO iteminfo_data VALUES (159, 'price_logic_min_1975', '2752.8', 10);
INSERT INTO iteminfo_data VALUES (160, 'price_logic_max_1975', '6882', 10);
INSERT INTO iteminfo_data VALUES (161, 'price_fraud_1976', '1610.5', 10);
INSERT INTO iteminfo_data VALUES (162, 'price_logic_min_1976', '2576.8', 10);
INSERT INTO iteminfo_data VALUES (163, 'price_logic_max_1976', '6442', 10);
INSERT INTO iteminfo_data VALUES (164, 'price_fraud_1977', '1150', 10);
INSERT INTO iteminfo_data VALUES (165, 'price_logic_min_1977', '1840', 10);
INSERT INTO iteminfo_data VALUES (166, 'price_logic_max_1977', '4600', 10);
INSERT INTO iteminfo_data VALUES (167, 'price_fraud_1978', '1491.5', 10);
INSERT INTO iteminfo_data VALUES (168, 'price_logic_min_1978', '2386.4', 10);
INSERT INTO iteminfo_data VALUES (169, 'price_logic_max_1978', '5966', 10);
INSERT INTO iteminfo_data VALUES (170, 'price_fraud_1979', '1272.5', 10);
INSERT INTO iteminfo_data VALUES (171, 'price_logic_min_1979', '2036', 10);
INSERT INTO iteminfo_data VALUES (172, 'price_logic_max_1979', '5090', 10);
INSERT INTO iteminfo_data VALUES (173, 'price_fraud_1980', '1623', 10);
INSERT INTO iteminfo_data VALUES (174, 'price_logic_min_1980', '2596.8', 10);
INSERT INTO iteminfo_data VALUES (175, 'price_logic_max_1980', '6492', 10);
INSERT INTO iteminfo_data VALUES (176, 'price_fraud_1981', '1257', 10);
INSERT INTO iteminfo_data VALUES (177, 'price_logic_min_1981', '2011.2', 10);
INSERT INTO iteminfo_data VALUES (178, 'price_logic_max_1981', '5028', 10);
INSERT INTO iteminfo_data VALUES (179, 'price_fraud_1982', '1237.5', 10);
INSERT INTO iteminfo_data VALUES (180, 'price_logic_min_1982', '1980', 10);
INSERT INTO iteminfo_data VALUES (181, 'price_logic_max_1982', '4950', 10);
INSERT INTO iteminfo_data VALUES (182, 'price_fraud_1983', '1111', 10);
INSERT INTO iteminfo_data VALUES (183, 'price_logic_min_1983', '1777.6', 10);
INSERT INTO iteminfo_data VALUES (184, 'price_logic_max_1983', '4444', 10);
INSERT INTO iteminfo_data VALUES (185, 'price_fraud_1984', '1127', 10);
INSERT INTO iteminfo_data VALUES (186, 'price_logic_min_1984', '1803.2', 10);
INSERT INTO iteminfo_data VALUES (187, 'price_logic_max_1984', '4508', 10);
INSERT INTO iteminfo_data VALUES (188, 'price_fraud_1985', '1425', 10);
INSERT INTO iteminfo_data VALUES (189, 'price_logic_min_1985', '2280', 10);
INSERT INTO iteminfo_data VALUES (190, 'price_logic_max_1985', '5700', 10);
INSERT INTO iteminfo_data VALUES (191, 'price_fraud_1986', '1179', 10);
INSERT INTO iteminfo_data VALUES (192, 'price_logic_min_1986', '1886.4', 10);
INSERT INTO iteminfo_data VALUES (193, 'price_logic_max_1986', '4716', 10);
INSERT INTO iteminfo_data VALUES (194, 'price_fraud_1987', '1336.5', 10);
INSERT INTO iteminfo_data VALUES (195, 'price_logic_min_1987', '2138.4', 10);
INSERT INTO iteminfo_data VALUES (196, 'price_logic_max_1987', '5346', 10);
INSERT INTO iteminfo_data VALUES (197, 'price_fraud_1988', '1402', 10);
INSERT INTO iteminfo_data VALUES (198, 'price_logic_min_1988', '2243.2', 10);
INSERT INTO iteminfo_data VALUES (199, 'price_logic_max_1988', '5608', 10);
INSERT INTO iteminfo_data VALUES (200, 'price_fraud_1989', '1327.5', 10);
INSERT INTO iteminfo_data VALUES (201, 'price_logic_min_1989', '2124', 10);
INSERT INTO iteminfo_data VALUES (202, 'price_logic_max_1989', '5310', 10);
INSERT INTO iteminfo_data VALUES (203, 'price_fraud_1990', '1678', 10);
INSERT INTO iteminfo_data VALUES (204, 'price_logic_min_1990', '2684.8', 10);
INSERT INTO iteminfo_data VALUES (205, 'price_logic_max_1990', '6712', 10);
INSERT INTO iteminfo_data VALUES (206, 'price_fraud_1991', '1612', 10);
INSERT INTO iteminfo_data VALUES (207, 'price_logic_min_1991', '2579.2', 10);
INSERT INTO iteminfo_data VALUES (208, 'price_logic_max_1991', '6448', 10);
INSERT INTO iteminfo_data VALUES (209, 'price_fraud_1992', '1703', 10);
INSERT INTO iteminfo_data VALUES (210, 'price_logic_min_1992', '2724.8', 10);
INSERT INTO iteminfo_data VALUES (211, 'price_logic_max_1992', '6812', 10);
INSERT INTO iteminfo_data VALUES (212, 'price_fraud_1993', '1699.5', 10);
INSERT INTO iteminfo_data VALUES (213, 'price_logic_min_1993', '2719.2', 10);
INSERT INTO iteminfo_data VALUES (214, 'price_logic_max_1993', '6798', 10);
INSERT INTO iteminfo_data VALUES (215, 'price_fraud_1994', '1561', 10);
INSERT INTO iteminfo_data VALUES (216, 'price_logic_min_1994', '2497.6', 10);
INSERT INTO iteminfo_data VALUES (217, 'price_logic_max_1994', '6244', 10);
INSERT INTO iteminfo_data VALUES (218, 'price_fraud_1995', '1942', 10);
INSERT INTO iteminfo_data VALUES (219, 'price_logic_min_1995', '3107.2', 10);
INSERT INTO iteminfo_data VALUES (220, 'price_logic_max_1995', '7768', 10);
INSERT INTO iteminfo_data VALUES (221, 'price_fraud_1996', '1993', 10);
INSERT INTO iteminfo_data VALUES (222, 'price_logic_min_1996', '3188.8', 10);
INSERT INTO iteminfo_data VALUES (223, 'price_logic_max_1996', '7972', 10);
INSERT INTO iteminfo_data VALUES (224, 'price_fraud_1997', '2398', 10);
INSERT INTO iteminfo_data VALUES (225, 'price_logic_min_1997', '3836.8', 10);
INSERT INTO iteminfo_data VALUES (226, 'price_logic_max_1997', '9592', 10);
INSERT INTO iteminfo_data VALUES (227, 'price_fraud_1998', '2456.5', 10);
INSERT INTO iteminfo_data VALUES (228, 'price_logic_min_1998', '3930.4', 10);
INSERT INTO iteminfo_data VALUES (229, 'price_logic_max_1998', '9826', 10);
INSERT INTO iteminfo_data VALUES (230, 'price_fraud_1999', '2640', 10);
INSERT INTO iteminfo_data VALUES (231, 'price_logic_min_1999', '4224', 10);
INSERT INTO iteminfo_data VALUES (232, 'price_logic_max_1999', '10560', 10);
INSERT INTO iteminfo_data VALUES (233, 'price_fraud_2000', '2942.5', 10);
INSERT INTO iteminfo_data VALUES (234, 'price_logic_min_2000', '4708', 10);
INSERT INTO iteminfo_data VALUES (235, 'price_logic_max_2000', '11770', 10);
INSERT INTO iteminfo_data VALUES (236, 'price_fraud_2001', '3435.5', 10);
INSERT INTO iteminfo_data VALUES (237, 'price_logic_min_2001', '5496.8', 10);
INSERT INTO iteminfo_data VALUES (238, 'price_logic_max_2001', '13742', 10);
INSERT INTO iteminfo_data VALUES (239, 'price_fraud_2002', '3541', 10);
INSERT INTO iteminfo_data VALUES (240, 'price_logic_min_2002', '5665.6', 10);
INSERT INTO iteminfo_data VALUES (241, 'price_logic_max_2002', '14164', 10);
INSERT INTO iteminfo_data VALUES (242, 'price_fraud_2003', '3900', 10);
INSERT INTO iteminfo_data VALUES (243, 'price_logic_min_2003', '6240', 10);
INSERT INTO iteminfo_data VALUES (244, 'price_logic_max_2003', '15600', 10);
INSERT INTO iteminfo_data VALUES (245, 'price_fraud_2004', '4205.5', 10);
INSERT INTO iteminfo_data VALUES (246, 'price_logic_min_2004', '6728.8', 10);
INSERT INTO iteminfo_data VALUES (247, 'price_logic_max_2004', '16822', 10);
INSERT INTO iteminfo_data VALUES (248, 'price_fraud_2005', '4750.5', 10);
INSERT INTO iteminfo_data VALUES (249, 'price_logic_min_2005', '7600.8', 10);
INSERT INTO iteminfo_data VALUES (250, 'price_logic_max_2005', '19002', 10);
INSERT INTO iteminfo_data VALUES (251, 'price_fraud_2006', '5451.5', 10);
INSERT INTO iteminfo_data VALUES (252, 'price_logic_min_2006', '8722.4', 10);
INSERT INTO iteminfo_data VALUES (253, 'price_logic_max_2006', '21806', 10);
INSERT INTO iteminfo_data VALUES (254, 'price_fraud_2007', '6064', 10);
INSERT INTO iteminfo_data VALUES (255, 'price_logic_min_2007', '9702.4', 10);
INSERT INTO iteminfo_data VALUES (256, 'price_logic_max_2007', '24256', 10);
INSERT INTO iteminfo_data VALUES (257, 'price_fraud_2008', '6650.5', 10);
INSERT INTO iteminfo_data VALUES (258, 'price_logic_min_2008', '10640.8', 10);
INSERT INTO iteminfo_data VALUES (259, 'price_logic_max_2008', '26602', 10);
INSERT INTO iteminfo_data VALUES (260, 'price_fraud_1996', '1950', 26);
INSERT INTO iteminfo_data VALUES (261, 'price_logic_min_1996', '3120', 26);
INSERT INTO iteminfo_data VALUES (262, 'price_logic_max_1996', '7800', 26);
INSERT INTO iteminfo_data VALUES (263, 'price_fraud_1997', '1750', 26);
INSERT INTO iteminfo_data VALUES (264, 'price_logic_min_1997', '2800', 26);
INSERT INTO iteminfo_data VALUES (265, 'price_logic_max_1997', '7000', 26);
INSERT INTO iteminfo_data VALUES (266, 'price_fraud_2001', '2800', 26);
INSERT INTO iteminfo_data VALUES (267, 'price_logic_min_2001', '4480', 26);
INSERT INTO iteminfo_data VALUES (268, 'price_logic_max_2001', '11200', 26);
INSERT INTO iteminfo_data VALUES (269, 'price_fraud_1996', '1875', 16);
INSERT INTO iteminfo_data VALUES (270, 'price_logic_min_1996', '3000', 16);
INSERT INTO iteminfo_data VALUES (271, 'price_logic_max_1996', '7500', 16);
INSERT INTO iteminfo_data VALUES (272, 'price_fraud_1997', '1766.5', 16);
INSERT INTO iteminfo_data VALUES (273, 'price_logic_min_1997', '2826.4', 16);
INSERT INTO iteminfo_data VALUES (274, 'price_logic_max_1997', '7066', 16);
INSERT INTO iteminfo_data VALUES (275, 'price_fraud_1998', '2000', 16);
INSERT INTO iteminfo_data VALUES (276, 'price_logic_min_1998', '3200', 16);
INSERT INTO iteminfo_data VALUES (277, 'price_logic_max_1998', '8000', 16);
INSERT INTO iteminfo_data VALUES (278, 'price_fraud_2001', '2800', 16);
INSERT INTO iteminfo_data VALUES (279, 'price_logic_min_2001', '4480', 16);
INSERT INTO iteminfo_data VALUES (280, 'price_logic_max_2001', '11200', 16);
INSERT INTO iteminfo_data VALUES (281, 'price_fraud_1992', '5625', 38);
INSERT INTO iteminfo_data VALUES (282, 'price_logic_min_1992', '9000', 38);
INSERT INTO iteminfo_data VALUES (283, 'price_logic_max_1992', '22500', 38);
INSERT INTO iteminfo_data VALUES (284, 'price_fraud_1993', '9233', 38);
INSERT INTO iteminfo_data VALUES (285, 'price_logic_min_1993', '14772.8', 38);
INSERT INTO iteminfo_data VALUES (286, 'price_logic_max_1993', '36932', 38);
INSERT INTO iteminfo_data VALUES (287, 'price_fraud_1994', '10500', 38);
INSERT INTO iteminfo_data VALUES (288, 'price_logic_min_1994', '16800', 38);
INSERT INTO iteminfo_data VALUES (289, 'price_logic_max_1994', '42000', 38);
INSERT INTO iteminfo_data VALUES (290, 'price_fraud_1995', '12000', 38);
INSERT INTO iteminfo_data VALUES (291, 'price_logic_min_1995', '19200', 38);
INSERT INTO iteminfo_data VALUES (292, 'price_logic_max_1995', '48000', 38);
INSERT INTO iteminfo_data VALUES (293, 'price_fraud_1992', '1450', 39);
INSERT INTO iteminfo_data VALUES (294, 'price_logic_min_1992', '2320', 39);
INSERT INTO iteminfo_data VALUES (295, 'price_logic_max_1992', '5800', 39);
INSERT INTO iteminfo_data VALUES (296, 'price_fraud_1993', '2250', 39);
INSERT INTO iteminfo_data VALUES (297, 'price_logic_min_1993', '3600', 39);
INSERT INTO iteminfo_data VALUES (298, 'price_logic_max_1993', '9000', 39);
INSERT INTO iteminfo_data VALUES (299, 'price_fraud_1995', '1783', 39);
INSERT INTO iteminfo_data VALUES (300, 'price_logic_min_1995', '2852.8', 39);
INSERT INTO iteminfo_data VALUES (301, 'price_logic_max_1995', '7132', 39);
INSERT INTO iteminfo_data VALUES (302, 'price_fraud_1996', '1683', 39);
INSERT INTO iteminfo_data VALUES (303, 'price_logic_min_1996', '2692.8', 39);
INSERT INTO iteminfo_data VALUES (304, 'price_logic_max_1996', '6732', 39);
INSERT INTO iteminfo_data VALUES (305, 'price_fraud_1998', '6100', 39);
INSERT INTO iteminfo_data VALUES (306, 'price_logic_min_1998', '9760', 39);
INSERT INTO iteminfo_data VALUES (307, 'price_logic_max_1998', '24400', 39);
INSERT INTO iteminfo_data VALUES (308, 'price_fraud_1975', '991.5', 30);
INSERT INTO iteminfo_data VALUES (309, 'price_logic_min_1975', '1586.4', 30);
INSERT INTO iteminfo_data VALUES (310, 'price_logic_max_1975', '3966', 30);
INSERT INTO iteminfo_data VALUES (311, 'price_fraud_1976', '750', 30);
INSERT INTO iteminfo_data VALUES (312, 'price_logic_min_1976', '1200', 30);
INSERT INTO iteminfo_data VALUES (313, 'price_logic_max_1976', '3000', 30);
INSERT INTO iteminfo_data VALUES (314, 'price_fraud_1977', '1500', 30);
INSERT INTO iteminfo_data VALUES (315, 'price_logic_min_1977', '2400', 30);
INSERT INTO iteminfo_data VALUES (316, 'price_logic_max_1977', '6000', 30);
INSERT INTO iteminfo_data VALUES (317, 'price_fraud_1979', '437.5', 30);
INSERT INTO iteminfo_data VALUES (318, 'price_logic_min_1979', '700', 30);
INSERT INTO iteminfo_data VALUES (319, 'price_logic_max_1979', '1750', 30);
INSERT INTO iteminfo_data VALUES (320, 'price_fraud_1980', '1050', 30);
INSERT INTO iteminfo_data VALUES (321, 'price_logic_min_1980', '1680', 30);
INSERT INTO iteminfo_data VALUES (322, 'price_logic_max_1980', '4200', 30);
INSERT INTO iteminfo_data VALUES (323, 'price_fraud_1981', '375', 30);
INSERT INTO iteminfo_data VALUES (324, 'price_logic_min_1981', '600', 30);
INSERT INTO iteminfo_data VALUES (325, 'price_logic_max_1981', '1500', 30);
INSERT INTO iteminfo_data VALUES (326, 'price_fraud_1982', '230', 30);
INSERT INTO iteminfo_data VALUES (327, 'price_logic_min_1982', '368', 30);
INSERT INTO iteminfo_data VALUES (328, 'price_logic_max_1982', '920', 30);
INSERT INTO iteminfo_data VALUES (329, 'price_fraud_1983', '343.5', 30);
INSERT INTO iteminfo_data VALUES (330, 'price_logic_min_1983', '549.6', 30);
INSERT INTO iteminfo_data VALUES (331, 'price_logic_max_1983', '1374', 30);
INSERT INTO iteminfo_data VALUES (332, 'price_fraud_1984', '520', 30);
INSERT INTO iteminfo_data VALUES (333, 'price_logic_min_1984', '832', 30);
INSERT INTO iteminfo_data VALUES (334, 'price_logic_max_1984', '2080', 30);
INSERT INTO iteminfo_data VALUES (335, 'price_fraud_1985', '365', 30);
INSERT INTO iteminfo_data VALUES (336, 'price_logic_min_1985', '584', 30);
INSERT INTO iteminfo_data VALUES (337, 'price_logic_max_1985', '1460', 30);
INSERT INTO iteminfo_data VALUES (338, 'price_fraud_1986', '703.5', 30);
INSERT INTO iteminfo_data VALUES (339, 'price_logic_min_1986', '1125.6', 30);
INSERT INTO iteminfo_data VALUES (340, 'price_logic_max_1986', '2814', 30);
INSERT INTO iteminfo_data VALUES (341, 'price_fraud_1987', '432', 30);
INSERT INTO iteminfo_data VALUES (342, 'price_logic_min_1987', '691.2', 30);
INSERT INTO iteminfo_data VALUES (343, 'price_logic_max_1987', '1728', 30);
INSERT INTO iteminfo_data VALUES (344, 'price_fraud_1988', '526', 30);
INSERT INTO iteminfo_data VALUES (345, 'price_logic_min_1988', '841.6', 30);
INSERT INTO iteminfo_data VALUES (346, 'price_logic_max_1988', '2104', 30);
INSERT INTO iteminfo_data VALUES (347, 'price_fraud_1989', '421.5', 30);
INSERT INTO iteminfo_data VALUES (348, 'price_logic_min_1989', '674.4', 30);
INSERT INTO iteminfo_data VALUES (349, 'price_logic_max_1989', '1686', 30);
INSERT INTO iteminfo_data VALUES (350, 'price_fraud_1990', '459', 30);
INSERT INTO iteminfo_data VALUES (351, 'price_logic_min_1990', '734.4', 30);
INSERT INTO iteminfo_data VALUES (352, 'price_logic_max_1990', '1836', 30);
INSERT INTO iteminfo_data VALUES (353, 'price_fraud_1991', '460.5', 30);
INSERT INTO iteminfo_data VALUES (354, 'price_logic_min_1991', '736.8', 30);
INSERT INTO iteminfo_data VALUES (355, 'price_logic_max_1991', '1842', 30);
INSERT INTO iteminfo_data VALUES (356, 'price_fraud_1992', '614', 30);
INSERT INTO iteminfo_data VALUES (357, 'price_logic_min_1992', '982.4', 30);
INSERT INTO iteminfo_data VALUES (358, 'price_logic_max_1992', '2456', 30);
INSERT INTO iteminfo_data VALUES (359, 'price_fraud_1993', '699.5', 30);
INSERT INTO iteminfo_data VALUES (360, 'price_logic_min_1993', '1119.2', 30);
INSERT INTO iteminfo_data VALUES (361, 'price_logic_max_1993', '2798', 30);
INSERT INTO iteminfo_data VALUES (362, 'price_fraud_1994', '684', 30);
INSERT INTO iteminfo_data VALUES (363, 'price_logic_min_1994', '1094.4', 30);
INSERT INTO iteminfo_data VALUES (364, 'price_logic_max_1994', '2736', 30);
INSERT INTO iteminfo_data VALUES (365, 'price_fraud_1995', '768', 30);
INSERT INTO iteminfo_data VALUES (366, 'price_logic_min_1995', '1228.8', 30);
INSERT INTO iteminfo_data VALUES (367, 'price_logic_max_1995', '3072', 30);
INSERT INTO iteminfo_data VALUES (368, 'price_fraud_1996', '883.5', 30);
INSERT INTO iteminfo_data VALUES (369, 'price_logic_min_1996', '1413.6', 30);
INSERT INTO iteminfo_data VALUES (370, 'price_logic_max_1996', '3534', 30);
INSERT INTO iteminfo_data VALUES (371, 'price_fraud_1997', '956.5', 30);
INSERT INTO iteminfo_data VALUES (372, 'price_logic_min_1997', '1530.4', 30);
INSERT INTO iteminfo_data VALUES (373, 'price_logic_max_1997', '3826', 30);
INSERT INTO iteminfo_data VALUES (374, 'price_fraud_1998', '1153.5', 30);
INSERT INTO iteminfo_data VALUES (375, 'price_logic_min_1998', '1845.6', 30);
INSERT INTO iteminfo_data VALUES (376, 'price_logic_max_1998', '4614', 30);
INSERT INTO iteminfo_data VALUES (377, 'price_fraud_1999', '1244.5', 30);
INSERT INTO iteminfo_data VALUES (378, 'price_logic_min_1999', '1991.2', 30);
INSERT INTO iteminfo_data VALUES (379, 'price_logic_max_1999', '4978', 30);
INSERT INTO iteminfo_data VALUES (380, 'price_fraud_2000', '1298.5', 30);
INSERT INTO iteminfo_data VALUES (381, 'price_logic_min_2000', '2077.6', 30);
INSERT INTO iteminfo_data VALUES (382, 'price_logic_max_2000', '5194', 30);
INSERT INTO iteminfo_data VALUES (383, 'price_fraud_2001', '1873.5', 30);
INSERT INTO iteminfo_data VALUES (384, 'price_logic_min_2001', '2997.6', 30);
INSERT INTO iteminfo_data VALUES (385, 'price_logic_max_2001', '7494', 30);
INSERT INTO iteminfo_data VALUES (386, 'price_fraud_2002', '1600', 30);
INSERT INTO iteminfo_data VALUES (387, 'price_logic_min_2002', '2560', 30);
INSERT INTO iteminfo_data VALUES (388, 'price_logic_max_2002', '6400', 30);
INSERT INTO iteminfo_data VALUES (389, 'price_fraud_2007', '1150', 30);
INSERT INTO iteminfo_data VALUES (390, 'price_logic_min_2007', '1840', 30);
INSERT INTO iteminfo_data VALUES (391, 'price_logic_max_2007', '4600', 30);
INSERT INTO iteminfo_data VALUES (392, 'price_fraud_1993', '300', 40);
INSERT INTO iteminfo_data VALUES (393, 'price_logic_min_1993', '480', 40);
INSERT INTO iteminfo_data VALUES (394, 'price_logic_max_1993', '1200', 40);
INSERT INTO iteminfo_data VALUES (395, 'price_fraud_1996', '10000', 40);
INSERT INTO iteminfo_data VALUES (396, 'price_logic_min_1996', '16000', 40);
INSERT INTO iteminfo_data VALUES (397, 'price_logic_max_1996', '40000', 40);
INSERT INTO iteminfo_data VALUES (398, 'price_fraud_2001', '1836', 40);
INSERT INTO iteminfo_data VALUES (399, 'price_logic_min_2001', '2937.6', 40);
INSERT INTO iteminfo_data VALUES (400, 'price_logic_max_2001', '7344', 40);
INSERT INTO iteminfo_data VALUES (401, 'price_fraud_2002', '2100', 40);
INSERT INTO iteminfo_data VALUES (402, 'price_logic_min_2002', '3360', 40);
INSERT INTO iteminfo_data VALUES (403, 'price_logic_max_2002', '8400', 40);
INSERT INTO iteminfo_data VALUES (404, 'price_fraud_2004', '3930', 41);
INSERT INTO iteminfo_data VALUES (405, 'price_logic_min_2004', '6288', 41);
INSERT INTO iteminfo_data VALUES (406, 'price_logic_max_2004', '15720', 41);
INSERT INTO iteminfo_data VALUES (407, 'price_fraud_2005', '4316.5', 41);
INSERT INTO iteminfo_data VALUES (408, 'price_logic_min_2005', '6906.4', 41);
INSERT INTO iteminfo_data VALUES (409, 'price_logic_max_2005', '17266', 41);
INSERT INTO iteminfo_data VALUES (410, 'price_fraud_2006', '4622.5', 41);
INSERT INTO iteminfo_data VALUES (411, 'price_logic_min_2006', '7396', 41);
INSERT INTO iteminfo_data VALUES (412, 'price_logic_max_2006', '18490', 41);
INSERT INTO iteminfo_data VALUES (413, 'price_fraud_2007', '5154.5', 41);
INSERT INTO iteminfo_data VALUES (414, 'price_logic_min_2007', '8247.2', 41);
INSERT INTO iteminfo_data VALUES (415, 'price_logic_max_2007', '20618', 41);
INSERT INTO iteminfo_data VALUES (416, 'price_fraud_2008', '6725', 41);
INSERT INTO iteminfo_data VALUES (417, 'price_logic_min_2008', '10760', 41);
INSERT INTO iteminfo_data VALUES (418, 'price_logic_max_2008', '26900', 41);
INSERT INTO iteminfo_data VALUES (419, 'price_fraud_1975', '100', 31);
INSERT INTO iteminfo_data VALUES (420, 'price_logic_min_1975', '160', 31);
INSERT INTO iteminfo_data VALUES (421, 'price_logic_max_1975', '400', 31);
INSERT INTO iteminfo_data VALUES (422, 'price_fraud_1976', '1150', 31);
INSERT INTO iteminfo_data VALUES (423, 'price_logic_min_1976', '1840', 31);
INSERT INTO iteminfo_data VALUES (424, 'price_logic_max_1976', '4600', 31);
INSERT INTO iteminfo_data VALUES (425, 'price_fraud_1977', '220', 31);
INSERT INTO iteminfo_data VALUES (426, 'price_logic_min_1977', '352', 31);
INSERT INTO iteminfo_data VALUES (427, 'price_logic_max_1977', '880', 31);
INSERT INTO iteminfo_data VALUES (428, 'price_fraud_1978', '375', 31);
INSERT INTO iteminfo_data VALUES (429, 'price_logic_min_1978', '600', 31);
INSERT INTO iteminfo_data VALUES (430, 'price_logic_max_1978', '1500', 31);
INSERT INTO iteminfo_data VALUES (431, 'price_fraud_1980', '225', 31);
INSERT INTO iteminfo_data VALUES (432, 'price_logic_min_1980', '360', 31);
INSERT INTO iteminfo_data VALUES (433, 'price_logic_max_1980', '900', 31);
INSERT INTO iteminfo_data VALUES (434, 'price_fraud_1981', '250', 31);
INSERT INTO iteminfo_data VALUES (435, 'price_logic_min_1981', '400', 31);
INSERT INTO iteminfo_data VALUES (436, 'price_logic_max_1981', '1000', 31);
INSERT INTO iteminfo_data VALUES (437, 'price_fraud_1982', '100', 31);
INSERT INTO iteminfo_data VALUES (438, 'price_logic_min_1982', '160', 31);
INSERT INTO iteminfo_data VALUES (439, 'price_logic_max_1982', '400', 31);
INSERT INTO iteminfo_data VALUES (440, 'price_fraud_1983', '166.5', 31);
INSERT INTO iteminfo_data VALUES (441, 'price_logic_min_1983', '266.4', 31);
INSERT INTO iteminfo_data VALUES (442, 'price_logic_max_1983', '666', 31);
INSERT INTO iteminfo_data VALUES (443, 'price_fraud_1984', '184', 31);
INSERT INTO iteminfo_data VALUES (444, 'price_logic_min_1984', '294.4', 31);
INSERT INTO iteminfo_data VALUES (445, 'price_logic_max_1984', '736', 31);
INSERT INTO iteminfo_data VALUES (446, 'price_fraud_1985', '191.5', 31);
INSERT INTO iteminfo_data VALUES (447, 'price_logic_min_1985', '306.4', 31);
INSERT INTO iteminfo_data VALUES (448, 'price_logic_max_1985', '766', 31);
INSERT INTO iteminfo_data VALUES (449, 'price_fraud_1986', '316.5', 31);
INSERT INTO iteminfo_data VALUES (450, 'price_logic_min_1986', '506.4', 31);
INSERT INTO iteminfo_data VALUES (451, 'price_logic_max_1986', '1266', 31);
INSERT INTO iteminfo_data VALUES (452, 'price_fraud_1987', '285', 31);
INSERT INTO iteminfo_data VALUES (453, 'price_logic_min_1987', '456', 31);
INSERT INTO iteminfo_data VALUES (454, 'price_logic_max_1987', '1140', 31);
INSERT INTO iteminfo_data VALUES (455, 'price_fraud_1988', '323', 31);
INSERT INTO iteminfo_data VALUES (456, 'price_logic_min_1988', '516.8', 31);
INSERT INTO iteminfo_data VALUES (457, 'price_logic_max_1988', '1292', 31);
INSERT INTO iteminfo_data VALUES (458, 'price_fraud_1989', '321.5', 31);
INSERT INTO iteminfo_data VALUES (459, 'price_logic_min_1989', '514.4', 31);
INSERT INTO iteminfo_data VALUES (460, 'price_logic_max_1989', '1286', 31);
INSERT INTO iteminfo_data VALUES (461, 'price_fraud_1990', '352.5', 31);
INSERT INTO iteminfo_data VALUES (462, 'price_logic_min_1990', '564', 31);
INSERT INTO iteminfo_data VALUES (463, 'price_logic_max_1990', '1410', 31);
INSERT INTO iteminfo_data VALUES (464, 'price_fraud_1991', '395', 31);
INSERT INTO iteminfo_data VALUES (465, 'price_logic_min_1991', '632', 31);
INSERT INTO iteminfo_data VALUES (466, 'price_logic_max_1991', '1580', 31);
INSERT INTO iteminfo_data VALUES (467, 'price_fraud_1992', '475', 31);
INSERT INTO iteminfo_data VALUES (468, 'price_logic_min_1992', '760', 31);
INSERT INTO iteminfo_data VALUES (469, 'price_logic_max_1992', '1900', 31);
INSERT INTO iteminfo_data VALUES (470, 'price_fraud_1993', '487', 31);
INSERT INTO iteminfo_data VALUES (471, 'price_logic_min_1993', '779.2', 31);
INSERT INTO iteminfo_data VALUES (472, 'price_logic_max_1993', '1948', 31);
INSERT INTO iteminfo_data VALUES (473, 'price_fraud_1994', '549.5', 31);
INSERT INTO iteminfo_data VALUES (474, 'price_logic_min_1994', '879.2', 31);
INSERT INTO iteminfo_data VALUES (475, 'price_logic_max_1994', '2198', 31);
INSERT INTO iteminfo_data VALUES (476, 'price_fraud_1995', '651.5', 31);
INSERT INTO iteminfo_data VALUES (477, 'price_logic_min_1995', '1042.4', 31);
INSERT INTO iteminfo_data VALUES (478, 'price_logic_max_1995', '2606', 31);
INSERT INTO iteminfo_data VALUES (479, 'price_fraud_1996', '829.5', 31);
INSERT INTO iteminfo_data VALUES (480, 'price_logic_min_1996', '1327.2', 31);
INSERT INTO iteminfo_data VALUES (481, 'price_logic_max_1996', '3318', 31);
INSERT INTO iteminfo_data VALUES (482, 'price_fraud_1997', '1048.5', 31);
INSERT INTO iteminfo_data VALUES (483, 'price_logic_min_1997', '1677.6', 31);
INSERT INTO iteminfo_data VALUES (484, 'price_logic_max_1997', '4194', 31);
INSERT INTO iteminfo_data VALUES (485, 'price_fraud_1998', '1168', 31);
INSERT INTO iteminfo_data VALUES (486, 'price_logic_min_1998', '1868.8', 31);
INSERT INTO iteminfo_data VALUES (487, 'price_logic_max_1998', '4672', 31);
INSERT INTO iteminfo_data VALUES (488, 'price_fraud_1999', '1340', 31);
INSERT INTO iteminfo_data VALUES (489, 'price_logic_min_1999', '2144', 31);
INSERT INTO iteminfo_data VALUES (490, 'price_logic_max_1999', '5360', 31);
INSERT INTO iteminfo_data VALUES (491, 'price_fraud_2000', '1591', 31);
INSERT INTO iteminfo_data VALUES (492, 'price_logic_min_2000', '2545.6', 31);
INSERT INTO iteminfo_data VALUES (493, 'price_logic_max_2000', '6364', 31);
INSERT INTO iteminfo_data VALUES (494, 'price_fraud_2001', '1940.5', 31);
INSERT INTO iteminfo_data VALUES (495, 'price_logic_min_2001', '3104.8', 31);
INSERT INTO iteminfo_data VALUES (496, 'price_logic_max_2001', '7762', 31);
INSERT INTO iteminfo_data VALUES (497, 'price_fraud_2002', '2489', 31);
INSERT INTO iteminfo_data VALUES (498, 'price_logic_min_2002', '3982.4', 31);
INSERT INTO iteminfo_data VALUES (499, 'price_logic_max_2002', '9956', 31);
INSERT INTO iteminfo_data VALUES (500, 'price_fraud_2003', '3363', 31);
INSERT INTO iteminfo_data VALUES (501, 'price_logic_min_2003', '5380.8', 31);
INSERT INTO iteminfo_data VALUES (502, 'price_logic_max_2003', '13452', 31);
INSERT INTO iteminfo_data VALUES (503, 'price_fraud_2004', '3318.5', 31);
INSERT INTO iteminfo_data VALUES (504, 'price_logic_min_2004', '5309.6', 31);
INSERT INTO iteminfo_data VALUES (505, 'price_logic_max_2004', '13274', 31);
INSERT INTO iteminfo_data VALUES (506, 'price_fraud_2005', '3736', 31);
INSERT INTO iteminfo_data VALUES (507, 'price_logic_min_2005', '5977.6', 31);
INSERT INTO iteminfo_data VALUES (508, 'price_logic_max_2005', '14944', 31);
INSERT INTO iteminfo_data VALUES (509, 'price_fraud_2006', '4460', 31);
INSERT INTO iteminfo_data VALUES (510, 'price_logic_min_2006', '7136', 31);
INSERT INTO iteminfo_data VALUES (511, 'price_logic_max_2006', '17840', 31);
INSERT INTO iteminfo_data VALUES (512, 'price_fraud_2007', '5120.5', 31);
INSERT INTO iteminfo_data VALUES (513, 'price_logic_min_2007', '8192.8', 31);
INSERT INTO iteminfo_data VALUES (514, 'price_logic_max_2007', '20482', 31);
INSERT INTO iteminfo_data VALUES (515, 'price_fraud_2008', '5896.5', 31);
INSERT INTO iteminfo_data VALUES (516, 'price_logic_min_2008', '9434.4', 31);
INSERT INTO iteminfo_data VALUES (517, 'price_logic_max_2008', '23586', 31);
INSERT INTO iteminfo_data VALUES (518, 'price_fraud_1975', '4529.5', 27);
INSERT INTO iteminfo_data VALUES (519, 'price_logic_min_1975', '7247.2', 27);
INSERT INTO iteminfo_data VALUES (520, 'price_logic_max_1975', '18118', 27);
INSERT INTO iteminfo_data VALUES (521, 'price_fraud_1976', '1860', 27);
INSERT INTO iteminfo_data VALUES (522, 'price_logic_min_1976', '2976', 27);
INSERT INTO iteminfo_data VALUES (523, 'price_logic_max_1976', '7440', 27);
INSERT INTO iteminfo_data VALUES (524, 'price_fraud_1977', '1113.5', 27);
INSERT INTO iteminfo_data VALUES (525, 'price_logic_min_1977', '1781.6', 27);
INSERT INTO iteminfo_data VALUES (526, 'price_logic_max_1977', '4454', 27);
INSERT INTO iteminfo_data VALUES (527, 'price_fraud_1978', '699', 27);
INSERT INTO iteminfo_data VALUES (528, 'price_logic_min_1978', '1118.4', 27);
INSERT INTO iteminfo_data VALUES (529, 'price_logic_max_1978', '2796', 27);
INSERT INTO iteminfo_data VALUES (530, 'price_fraud_1979', '1120.5', 27);
INSERT INTO iteminfo_data VALUES (531, 'price_logic_min_1979', '1792.8', 27);
INSERT INTO iteminfo_data VALUES (532, 'price_logic_max_1979', '4482', 27);
INSERT INTO iteminfo_data VALUES (533, 'price_fraud_1980', '1116.5', 27);
INSERT INTO iteminfo_data VALUES (534, 'price_logic_min_1980', '1786.4', 27);
INSERT INTO iteminfo_data VALUES (535, 'price_logic_max_1980', '4466', 27);
INSERT INTO iteminfo_data VALUES (536, 'price_fraud_1981', '956.5', 27);
INSERT INTO iteminfo_data VALUES (537, 'price_logic_min_1981', '1530.4', 27);
INSERT INTO iteminfo_data VALUES (538, 'price_logic_max_1981', '3826', 27);
INSERT INTO iteminfo_data VALUES (539, 'price_fraud_1982', '561.5', 27);
INSERT INTO iteminfo_data VALUES (540, 'price_logic_min_1982', '898.4', 27);
INSERT INTO iteminfo_data VALUES (541, 'price_logic_max_1982', '2246', 27);
INSERT INTO iteminfo_data VALUES (542, 'price_fraud_1983', '526', 27);
INSERT INTO iteminfo_data VALUES (543, 'price_logic_min_1983', '841.6', 27);
INSERT INTO iteminfo_data VALUES (544, 'price_logic_max_1983', '2104', 27);
INSERT INTO iteminfo_data VALUES (545, 'price_fraud_1984', '483.5', 27);
INSERT INTO iteminfo_data VALUES (546, 'price_logic_min_1984', '773.6', 27);
INSERT INTO iteminfo_data VALUES (547, 'price_logic_max_1984', '1934', 27);
INSERT INTO iteminfo_data VALUES (548, 'price_fraud_1985', '354', 27);
INSERT INTO iteminfo_data VALUES (549, 'price_logic_min_1985', '566.4', 27);
INSERT INTO iteminfo_data VALUES (550, 'price_logic_max_1985', '1416', 27);
INSERT INTO iteminfo_data VALUES (551, 'price_fraud_1986', '473', 27);
INSERT INTO iteminfo_data VALUES (552, 'price_logic_min_1986', '756.8', 27);
INSERT INTO iteminfo_data VALUES (553, 'price_logic_max_1986', '1892', 27);
INSERT INTO iteminfo_data VALUES (554, 'price_fraud_1987', '402.5', 27);
INSERT INTO iteminfo_data VALUES (555, 'price_logic_min_1987', '644', 27);
INSERT INTO iteminfo_data VALUES (556, 'price_logic_max_1987', '1610', 27);
INSERT INTO iteminfo_data VALUES (557, 'price_fraud_1988', '430', 27);
INSERT INTO iteminfo_data VALUES (558, 'price_logic_min_1988', '688', 27);
INSERT INTO iteminfo_data VALUES (559, 'price_logic_max_1988', '1720', 27);
INSERT INTO iteminfo_data VALUES (560, 'price_fraud_1989', '410', 27);
INSERT INTO iteminfo_data VALUES (561, 'price_logic_min_1989', '656', 27);
INSERT INTO iteminfo_data VALUES (562, 'price_logic_max_1989', '1640', 27);
INSERT INTO iteminfo_data VALUES (563, 'price_fraud_1990', '454.5', 27);
INSERT INTO iteminfo_data VALUES (564, 'price_logic_min_1990', '727.2', 27);
INSERT INTO iteminfo_data VALUES (565, 'price_logic_max_1990', '1818', 27);
INSERT INTO iteminfo_data VALUES (566, 'price_fraud_1991', '442.5', 27);
INSERT INTO iteminfo_data VALUES (567, 'price_logic_min_1991', '708', 27);
INSERT INTO iteminfo_data VALUES (568, 'price_logic_max_1991', '1770', 27);
INSERT INTO iteminfo_data VALUES (569, 'price_fraud_1992', '541', 27);
INSERT INTO iteminfo_data VALUES (570, 'price_logic_min_1992', '865.6', 27);
INSERT INTO iteminfo_data VALUES (571, 'price_logic_max_1992', '2164', 27);
INSERT INTO iteminfo_data VALUES (572, 'price_fraud_1993', '645', 27);
INSERT INTO iteminfo_data VALUES (573, 'price_logic_min_1993', '1032', 27);
INSERT INTO iteminfo_data VALUES (574, 'price_logic_max_1993', '2580', 27);
INSERT INTO iteminfo_data VALUES (575, 'price_fraud_1994', '710.5', 27);
INSERT INTO iteminfo_data VALUES (576, 'price_logic_min_1994', '1136.8', 27);
INSERT INTO iteminfo_data VALUES (577, 'price_logic_max_1994', '2842', 27);
INSERT INTO iteminfo_data VALUES (578, 'price_fraud_1995', '869', 27);
INSERT INTO iteminfo_data VALUES (579, 'price_logic_min_1995', '1390.4', 27);
INSERT INTO iteminfo_data VALUES (580, 'price_logic_max_1995', '3476', 27);
INSERT INTO iteminfo_data VALUES (581, 'price_fraud_1996', '1012.5', 27);
INSERT INTO iteminfo_data VALUES (582, 'price_logic_min_1996', '1620', 27);
INSERT INTO iteminfo_data VALUES (583, 'price_logic_max_1996', '4050', 27);
INSERT INTO iteminfo_data VALUES (584, 'price_fraud_1997', '1226.5', 27);
INSERT INTO iteminfo_data VALUES (585, 'price_logic_min_1997', '1962.4', 27);
INSERT INTO iteminfo_data VALUES (586, 'price_logic_max_1997', '4906', 27);
INSERT INTO iteminfo_data VALUES (587, 'price_fraud_1998', '1405', 27);
INSERT INTO iteminfo_data VALUES (588, 'price_logic_min_1998', '2248', 27);
INSERT INTO iteminfo_data VALUES (589, 'price_logic_max_1998', '5620', 27);
INSERT INTO iteminfo_data VALUES (590, 'price_fraud_1999', '1682.5', 27);
INSERT INTO iteminfo_data VALUES (591, 'price_logic_min_1999', '2692', 27);
INSERT INTO iteminfo_data VALUES (592, 'price_logic_max_1999', '6730', 27);
INSERT INTO iteminfo_data VALUES (593, 'price_fraud_2000', '2113.5', 27);
INSERT INTO iteminfo_data VALUES (594, 'price_logic_min_2000', '3381.6', 27);
INSERT INTO iteminfo_data VALUES (595, 'price_logic_max_2000', '8454', 27);
INSERT INTO iteminfo_data VALUES (596, 'price_fraud_2001', '2815.5', 27);
INSERT INTO iteminfo_data VALUES (597, 'price_logic_min_2001', '4504.8', 27);
INSERT INTO iteminfo_data VALUES (598, 'price_logic_max_2001', '11262', 27);
INSERT INTO iteminfo_data VALUES (599, 'price_fraud_2002', '3258', 27);
INSERT INTO iteminfo_data VALUES (600, 'price_logic_min_2002', '5212.8', 27);
INSERT INTO iteminfo_data VALUES (601, 'price_logic_max_2002', '13032', 27);
INSERT INTO iteminfo_data VALUES (602, 'price_fraud_2003', '3888', 27);
INSERT INTO iteminfo_data VALUES (603, 'price_logic_min_2003', '6220.8', 27);
INSERT INTO iteminfo_data VALUES (604, 'price_logic_max_2003', '15552', 27);
INSERT INTO iteminfo_data VALUES (605, 'price_fraud_2004', '4701', 27);
INSERT INTO iteminfo_data VALUES (606, 'price_logic_min_2004', '7521.6', 27);
INSERT INTO iteminfo_data VALUES (607, 'price_logic_max_2004', '18804', 27);
INSERT INTO iteminfo_data VALUES (608, 'price_fraud_2005', '5431', 27);
INSERT INTO iteminfo_data VALUES (609, 'price_logic_min_2005', '8689.6', 27);
INSERT INTO iteminfo_data VALUES (610, 'price_logic_max_2005', '21724', 27);
INSERT INTO iteminfo_data VALUES (611, 'price_fraud_2006', '6420', 27);
INSERT INTO iteminfo_data VALUES (612, 'price_logic_min_2006', '10272', 27);
INSERT INTO iteminfo_data VALUES (613, 'price_logic_max_2006', '25680', 27);
INSERT INTO iteminfo_data VALUES (614, 'price_fraud_2007', '8082', 27);
INSERT INTO iteminfo_data VALUES (615, 'price_logic_min_2007', '12931.2', 27);
INSERT INTO iteminfo_data VALUES (616, 'price_logic_max_2007', '32328', 27);
INSERT INTO iteminfo_data VALUES (617, 'price_fraud_2008', '9657', 27);
INSERT INTO iteminfo_data VALUES (618, 'price_logic_min_2008', '15451.2', 27);
INSERT INTO iteminfo_data VALUES (619, 'price_logic_max_2008', '38628', 27);
INSERT INTO iteminfo_data VALUES (620, 'price_fraud_1998', '1700', 42);
INSERT INTO iteminfo_data VALUES (621, 'price_logic_min_1998', '2720', 42);
INSERT INTO iteminfo_data VALUES (622, 'price_logic_max_1998', '6800', 42);
INSERT INTO iteminfo_data VALUES (623, 'price_fraud_1999', '1550', 42);
INSERT INTO iteminfo_data VALUES (624, 'price_logic_min_1999', '2480', 42);
INSERT INTO iteminfo_data VALUES (625, 'price_logic_max_1999', '6200', 42);
INSERT INTO iteminfo_data VALUES (626, 'price_fraud_2001', '3800', 42);
INSERT INTO iteminfo_data VALUES (627, 'price_logic_min_2001', '6080', 42);
INSERT INTO iteminfo_data VALUES (628, 'price_logic_max_2001', '15200', 42);
INSERT INTO iteminfo_data VALUES (629, 'price_fraud_2002', '2972.5', 42);
INSERT INTO iteminfo_data VALUES (630, 'price_logic_min_2002', '4756', 42);
INSERT INTO iteminfo_data VALUES (631, 'price_logic_max_2002', '11890', 42);
INSERT INTO iteminfo_data VALUES (632, 'price_fraud_1996', '1250', 43);
INSERT INTO iteminfo_data VALUES (633, 'price_logic_min_1996', '2000', 43);
INSERT INTO iteminfo_data VALUES (634, 'price_logic_max_1996', '5000', 43);
INSERT INTO iteminfo_data VALUES (635, 'price_fraud_1998', '850', 43);
INSERT INTO iteminfo_data VALUES (636, 'price_logic_min_1998', '1360', 43);
INSERT INTO iteminfo_data VALUES (637, 'price_logic_max_1998', '3400', 43);
INSERT INTO iteminfo_data VALUES (638, 'price_fraud_1999', '1575', 43);
INSERT INTO iteminfo_data VALUES (639, 'price_logic_min_1999', '2520', 43);
INSERT INTO iteminfo_data VALUES (640, 'price_logic_max_1999', '6300', 43);
INSERT INTO iteminfo_data VALUES (641, 'price_fraud_2000', '1100', 43);
INSERT INTO iteminfo_data VALUES (642, 'price_logic_min_2000', '1760', 43);
INSERT INTO iteminfo_data VALUES (643, 'price_logic_max_2000', '4400', 43);
INSERT INTO iteminfo_data VALUES (644, 'price_fraud_1994', '831', 44);
INSERT INTO iteminfo_data VALUES (645, 'price_logic_min_1994', '1329.6', 44);
INSERT INTO iteminfo_data VALUES (646, 'price_logic_max_1994', '3324', 44);
INSERT INTO iteminfo_data VALUES (647, 'price_fraud_1995', '775', 44);
INSERT INTO iteminfo_data VALUES (648, 'price_logic_min_1995', '1240', 44);
INSERT INTO iteminfo_data VALUES (649, 'price_logic_max_1995', '3100', 44);
INSERT INTO iteminfo_data VALUES (650, 'price_fraud_1996', '1387.5', 44);
INSERT INTO iteminfo_data VALUES (651, 'price_logic_min_1996', '2220', 44);
INSERT INTO iteminfo_data VALUES (652, 'price_logic_max_1996', '5550', 44);
INSERT INTO iteminfo_data VALUES (653, 'price_fraud_1997', '1400', 44);
INSERT INTO iteminfo_data VALUES (654, 'price_logic_min_1997', '2240', 44);
INSERT INTO iteminfo_data VALUES (655, 'price_logic_max_1997', '5600', 44);
INSERT INTO iteminfo_data VALUES (656, 'price_fraud_1998', '1852.5', 45);
INSERT INTO iteminfo_data VALUES (657, 'price_logic_min_1998', '2964', 45);
INSERT INTO iteminfo_data VALUES (658, 'price_logic_max_1998', '7410', 45);
INSERT INTO iteminfo_data VALUES (659, 'price_fraud_1999', '1749.5', 45);
INSERT INTO iteminfo_data VALUES (660, 'price_logic_min_1999', '2799.2', 45);
INSERT INTO iteminfo_data VALUES (661, 'price_logic_max_1999', '6998', 45);
INSERT INTO iteminfo_data VALUES (662, 'price_fraud_2000', '2148.5', 45);
INSERT INTO iteminfo_data VALUES (663, 'price_logic_min_2000', '3437.6', 45);
INSERT INTO iteminfo_data VALUES (664, 'price_logic_max_2000', '8594', 45);
INSERT INTO iteminfo_data VALUES (665, 'price_fraud_2001', '2000', 45);
INSERT INTO iteminfo_data VALUES (666, 'price_logic_min_2001', '3200', 45);
INSERT INTO iteminfo_data VALUES (667, 'price_logic_max_2001', '8000', 45);
INSERT INTO iteminfo_data VALUES (668, 'price_fraud_1995', '1625', 46);
INSERT INTO iteminfo_data VALUES (669, 'price_logic_min_1995', '2600', 46);
INSERT INTO iteminfo_data VALUES (670, 'price_logic_max_1995', '6500', 46);
INSERT INTO iteminfo_data VALUES (671, 'price_fraud_1996', '1440', 46);
INSERT INTO iteminfo_data VALUES (672, 'price_logic_min_1996', '2304', 46);
INSERT INTO iteminfo_data VALUES (673, 'price_logic_max_1996', '5760', 46);
INSERT INTO iteminfo_data VALUES (674, 'price_fraud_1997', '1275', 46);
INSERT INTO iteminfo_data VALUES (675, 'price_logic_min_1997', '2040', 46);
INSERT INTO iteminfo_data VALUES (676, 'price_logic_max_1997', '5100', 46);
INSERT INTO iteminfo_data VALUES (677, 'price_fraud_1998', '1400', 46);
INSERT INTO iteminfo_data VALUES (678, 'price_logic_min_1998', '2240', 46);
INSERT INTO iteminfo_data VALUES (679, 'price_logic_max_1998', '5600', 46);
INSERT INTO iteminfo_data VALUES (680, 'price_fraud_2004', '4250', 46);
INSERT INTO iteminfo_data VALUES (681, 'price_logic_min_2004', '6800', 46);
INSERT INTO iteminfo_data VALUES (682, 'price_logic_max_2004', '17000', 46);
INSERT INTO iteminfo_data VALUES (683, 'price_fraud_1995', '1441.5', 47);
INSERT INTO iteminfo_data VALUES (684, 'price_logic_min_1995', '2306.4', 47);
INSERT INTO iteminfo_data VALUES (685, 'price_logic_max_1995', '5766', 47);
INSERT INTO iteminfo_data VALUES (686, 'price_fraud_1996', '1229', 47);
INSERT INTO iteminfo_data VALUES (687, 'price_logic_min_1996', '1966.4', 47);
INSERT INTO iteminfo_data VALUES (688, 'price_logic_max_1996', '4916', 47);
INSERT INTO iteminfo_data VALUES (689, 'price_fraud_1997', '1750', 47);
INSERT INTO iteminfo_data VALUES (690, 'price_logic_min_1997', '2800', 47);
INSERT INTO iteminfo_data VALUES (691, 'price_logic_max_1997', '7000', 47);
INSERT INTO iteminfo_data VALUES (692, 'price_fraud_1999', '2325', 47);
INSERT INTO iteminfo_data VALUES (693, 'price_logic_min_1999', '3720', 47);
INSERT INTO iteminfo_data VALUES (694, 'price_logic_max_1999', '9300', 47);
INSERT INTO iteminfo_data VALUES (695, 'price_fraud_2000', '1600', 47);
INSERT INTO iteminfo_data VALUES (696, 'price_logic_min_2000', '2560', 47);
INSERT INTO iteminfo_data VALUES (697, 'price_logic_max_2000', '6400', 47);
INSERT INTO iteminfo_data VALUES (698, 'price_fraud_2002', '3325', 48);
INSERT INTO iteminfo_data VALUES (699, 'price_logic_min_2002', '5320', 48);
INSERT INTO iteminfo_data VALUES (700, 'price_logic_max_2002', '13300', 48);
INSERT INTO iteminfo_data VALUES (701, 'price_fraud_2003', '4100', 48);
INSERT INTO iteminfo_data VALUES (702, 'price_logic_min_2003', '6560', 48);
INSERT INTO iteminfo_data VALUES (703, 'price_logic_max_2003', '16400', 48);
INSERT INTO iteminfo_data VALUES (704, 'price_fraud_2004', '3625', 48);
INSERT INTO iteminfo_data VALUES (705, 'price_logic_min_2004', '5800', 48);
INSERT INTO iteminfo_data VALUES (706, 'price_logic_max_2004', '14500', 48);
INSERT INTO iteminfo_data VALUES (707, 'price_fraud_2005', '4800', 48);
INSERT INTO iteminfo_data VALUES (708, 'price_logic_min_2005', '7680', 48);
INSERT INTO iteminfo_data VALUES (709, 'price_logic_max_2005', '19200', 48);
INSERT INTO iteminfo_data VALUES (710, 'price_fraud_2006', '5000', 48);
INSERT INTO iteminfo_data VALUES (711, 'price_logic_min_2006', '8000', 48);
INSERT INTO iteminfo_data VALUES (712, 'price_logic_max_2006', '20000', 48);
INSERT INTO iteminfo_data VALUES (713, 'price_fraud_2003', '5175', 49);
INSERT INTO iteminfo_data VALUES (714, 'price_logic_min_2003', '8280', 49);
INSERT INTO iteminfo_data VALUES (715, 'price_logic_max_2003', '20700', 49);
INSERT INTO iteminfo_data VALUES (716, 'price_fraud_2004', '5475', 49);
INSERT INTO iteminfo_data VALUES (717, 'price_logic_min_2004', '8760', 49);
INSERT INTO iteminfo_data VALUES (718, 'price_logic_max_2004', '21900', 49);
INSERT INTO iteminfo_data VALUES (719, 'price_fraud_2005', '6467.5', 49);
INSERT INTO iteminfo_data VALUES (720, 'price_logic_min_2005', '10348', 49);
INSERT INTO iteminfo_data VALUES (721, 'price_logic_max_2005', '25870', 49);
INSERT INTO iteminfo_data VALUES (722, 'price_fraud_2006', '8950', 49);
INSERT INTO iteminfo_data VALUES (723, 'price_logic_min_2006', '14320', 49);
INSERT INTO iteminfo_data VALUES (724, 'price_logic_max_2006', '35800', 49);
INSERT INTO iteminfo_data VALUES (725, 'price_fraud_2001', '3400', 50);
INSERT INTO iteminfo_data VALUES (726, 'price_logic_min_2001', '5440', 50);
INSERT INTO iteminfo_data VALUES (727, 'price_logic_max_2001', '13600', 50);
INSERT INTO iteminfo_data VALUES (728, 'price_fraud_2002', '3445', 50);
INSERT INTO iteminfo_data VALUES (729, 'price_logic_min_2002', '5512', 50);
INSERT INTO iteminfo_data VALUES (730, 'price_logic_max_2002', '13780', 50);
INSERT INTO iteminfo_data VALUES (731, 'price_fraud_2004', '4950', 50);
INSERT INTO iteminfo_data VALUES (732, 'price_logic_min_2004', '7920', 50);
INSERT INTO iteminfo_data VALUES (733, 'price_logic_max_2004', '19800', 50);
INSERT INTO iteminfo_data VALUES (734, 'price_fraud_2005', '4950', 50);
INSERT INTO iteminfo_data VALUES (735, 'price_logic_min_2005', '7920', 50);
INSERT INTO iteminfo_data VALUES (736, 'price_logic_max_2005', '19800', 50);
INSERT INTO iteminfo_data VALUES (737, 'price_fraud_1984', '1750', 32);
INSERT INTO iteminfo_data VALUES (738, 'price_logic_min_1984', '2800', 32);
INSERT INTO iteminfo_data VALUES (739, 'price_logic_max_1984', '7000', 32);
INSERT INTO iteminfo_data VALUES (740, 'price_fraud_1988', '1750', 32);
INSERT INTO iteminfo_data VALUES (741, 'price_logic_min_1988', '2800', 32);
INSERT INTO iteminfo_data VALUES (742, 'price_logic_max_1988', '7000', 32);
INSERT INTO iteminfo_data VALUES (743, 'price_fraud_1989', '1650', 32);
INSERT INTO iteminfo_data VALUES (744, 'price_logic_min_1989', '2640', 32);
INSERT INTO iteminfo_data VALUES (745, 'price_logic_max_1989', '6600', 32);
INSERT INTO iteminfo_data VALUES (746, 'price_fraud_1991', '2733', 32);
INSERT INTO iteminfo_data VALUES (747, 'price_logic_min_1991', '4372.8', 32);
INSERT INTO iteminfo_data VALUES (748, 'price_logic_max_1991', '10932', 32);
INSERT INTO iteminfo_data VALUES (749, 'price_fraud_1992', '1000', 32);
INSERT INTO iteminfo_data VALUES (750, 'price_logic_min_1992', '1600', 32);
INSERT INTO iteminfo_data VALUES (751, 'price_logic_max_1992', '4000', 32);
INSERT INTO iteminfo_data VALUES (752, 'price_fraud_1993', '1625', 32);
INSERT INTO iteminfo_data VALUES (753, 'price_logic_min_1993', '2600', 32);
INSERT INTO iteminfo_data VALUES (754, 'price_logic_max_1993', '6500', 32);
INSERT INTO iteminfo_data VALUES (755, 'price_fraud_1994', '828', 32);
INSERT INTO iteminfo_data VALUES (756, 'price_logic_min_1994', '1324.8', 32);
INSERT INTO iteminfo_data VALUES (757, 'price_logic_max_1994', '3312', 32);
INSERT INTO iteminfo_data VALUES (758, 'price_fraud_1995', '1057.5', 32);
INSERT INTO iteminfo_data VALUES (759, 'price_logic_min_1995', '1692', 32);
INSERT INTO iteminfo_data VALUES (760, 'price_logic_max_1995', '4230', 32);
INSERT INTO iteminfo_data VALUES (761, 'price_fraud_1996', '1136.5', 32);
INSERT INTO iteminfo_data VALUES (762, 'price_logic_min_1996', '1818.4', 32);
INSERT INTO iteminfo_data VALUES (763, 'price_logic_max_1996', '4546', 32);
INSERT INTO iteminfo_data VALUES (764, 'price_fraud_1997', '1295.5', 32);
INSERT INTO iteminfo_data VALUES (765, 'price_logic_min_1997', '2072.8', 32);
INSERT INTO iteminfo_data VALUES (766, 'price_logic_max_1997', '5182', 32);
INSERT INTO iteminfo_data VALUES (767, 'price_fraud_1998', '1616', 32);
INSERT INTO iteminfo_data VALUES (768, 'price_logic_min_1998', '2585.6', 32);
INSERT INTO iteminfo_data VALUES (769, 'price_logic_max_1998', '6464', 32);
INSERT INTO iteminfo_data VALUES (770, 'price_fraud_1999', '1857.5', 32);
INSERT INTO iteminfo_data VALUES (771, 'price_logic_min_1999', '2972', 32);
INSERT INTO iteminfo_data VALUES (772, 'price_logic_max_1999', '7430', 32);
INSERT INTO iteminfo_data VALUES (773, 'price_fraud_2000', '2170', 32);
INSERT INTO iteminfo_data VALUES (774, 'price_logic_min_2000', '3472', 32);
INSERT INTO iteminfo_data VALUES (775, 'price_logic_max_2000', '8680', 32);
INSERT INTO iteminfo_data VALUES (776, 'price_fraud_2001', '3146', 32);
INSERT INTO iteminfo_data VALUES (777, 'price_logic_min_2001', '5033.6', 32);
INSERT INTO iteminfo_data VALUES (778, 'price_logic_max_2001', '12584', 32);
INSERT INTO iteminfo_data VALUES (779, 'price_fraud_2002', '3565.5', 32);
INSERT INTO iteminfo_data VALUES (780, 'price_logic_min_2002', '5704.8', 32);
INSERT INTO iteminfo_data VALUES (781, 'price_logic_max_2002', '14262', 32);
INSERT INTO iteminfo_data VALUES (782, 'price_fraud_2003', '4029', 32);
INSERT INTO iteminfo_data VALUES (783, 'price_logic_min_2003', '6446.4', 32);
INSERT INTO iteminfo_data VALUES (784, 'price_logic_max_2003', '16116', 32);
INSERT INTO iteminfo_data VALUES (785, 'price_fraud_2004', '4702.5', 32);
INSERT INTO iteminfo_data VALUES (786, 'price_logic_min_2004', '7524', 32);
INSERT INTO iteminfo_data VALUES (787, 'price_logic_max_2004', '18810', 32);
INSERT INTO iteminfo_data VALUES (788, 'price_fraud_2005', '5904.5', 32);
INSERT INTO iteminfo_data VALUES (789, 'price_logic_min_2005', '9447.2', 32);
INSERT INTO iteminfo_data VALUES (790, 'price_logic_max_2005', '23618', 32);
INSERT INTO iteminfo_data VALUES (791, 'price_fraud_2006', '7192.5', 32);
INSERT INTO iteminfo_data VALUES (792, 'price_logic_min_2006', '11508', 32);
INSERT INTO iteminfo_data VALUES (793, 'price_logic_max_2006', '28770', 32);
INSERT INTO iteminfo_data VALUES (794, 'price_fraud_2007', '9243', 32);
INSERT INTO iteminfo_data VALUES (795, 'price_logic_min_2007', '14788.8', 32);
INSERT INTO iteminfo_data VALUES (796, 'price_logic_max_2007', '36972', 32);
INSERT INTO iteminfo_data VALUES (797, 'price_fraud_2008', '11569.5', 32);
INSERT INTO iteminfo_data VALUES (798, 'price_logic_min_2008', '18511.2', 32);
INSERT INTO iteminfo_data VALUES (799, 'price_logic_max_2008', '46278', 32);
INSERT INTO iteminfo_data VALUES (800, 'price_fraud_2000', '3866.5', 57);
INSERT INTO iteminfo_data VALUES (801, 'price_logic_min_2000', '6186.4', 57);
INSERT INTO iteminfo_data VALUES (802, 'price_logic_max_2000', '15466', 57);
INSERT INTO iteminfo_data VALUES (803, 'price_fraud_2001', '3982', 57);
INSERT INTO iteminfo_data VALUES (804, 'price_logic_min_2001', '6371.2', 57);
INSERT INTO iteminfo_data VALUES (805, 'price_logic_max_2001', '15928', 57);
INSERT INTO iteminfo_data VALUES (806, 'price_fraud_2002', '4284', 57);
INSERT INTO iteminfo_data VALUES (807, 'price_logic_min_2002', '6854.4', 57);
INSERT INTO iteminfo_data VALUES (808, 'price_logic_max_2002', '17136', 57);
INSERT INTO iteminfo_data VALUES (809, 'price_fraud_2003', '6075', 57);
INSERT INTO iteminfo_data VALUES (810, 'price_logic_min_2003', '9720', 57);
INSERT INTO iteminfo_data VALUES (811, 'price_logic_max_2003', '24300', 57);
INSERT INTO iteminfo_data VALUES (812, 'price_fraud_2007', '7950', 57);
INSERT INTO iteminfo_data VALUES (813, 'price_logic_min_2007', '12720', 57);
INSERT INTO iteminfo_data VALUES (814, 'price_logic_max_2007', '31800', 57);
INSERT INTO iteminfo_data VALUES (815, 'price_fraud_1997', '1533', 58);
INSERT INTO iteminfo_data VALUES (816, 'price_logic_min_1997', '2452.8', 58);
INSERT INTO iteminfo_data VALUES (817, 'price_logic_max_1997', '6132', 58);
INSERT INTO iteminfo_data VALUES (818, 'price_fraud_1998', '2033', 58);
INSERT INTO iteminfo_data VALUES (819, 'price_logic_min_1998', '3252.8', 58);
INSERT INTO iteminfo_data VALUES (820, 'price_logic_max_1998', '8132', 58);
INSERT INTO iteminfo_data VALUES (821, 'price_fraud_1999', '2248', 58);
INSERT INTO iteminfo_data VALUES (822, 'price_logic_min_1999', '3596.8', 58);
INSERT INTO iteminfo_data VALUES (823, 'price_logic_max_1999', '8992', 58);
INSERT INTO iteminfo_data VALUES (824, 'price_fraud_2000', '2313', 58);
INSERT INTO iteminfo_data VALUES (825, 'price_logic_min_2000', '3700.8', 58);
INSERT INTO iteminfo_data VALUES (826, 'price_logic_max_2000', '9252', 58);
INSERT INTO iteminfo_data VALUES (827, 'price_fraud_2001', '2300', 58);
INSERT INTO iteminfo_data VALUES (828, 'price_logic_min_2001', '3680', 58);
INSERT INTO iteminfo_data VALUES (829, 'price_logic_max_2001', '9200', 58);
INSERT INTO iteminfo_data VALUES (830, 'price_fraud_1997', '1816.5', 59);
INSERT INTO iteminfo_data VALUES (831, 'price_logic_min_1997', '2906.4', 59);
INSERT INTO iteminfo_data VALUES (832, 'price_logic_max_1997', '7266', 59);
INSERT INTO iteminfo_data VALUES (833, 'price_fraud_1998', '2100', 59);
INSERT INTO iteminfo_data VALUES (834, 'price_logic_min_1998', '3360', 59);
INSERT INTO iteminfo_data VALUES (835, 'price_logic_max_1998', '8400', 59);
INSERT INTO iteminfo_data VALUES (836, 'price_fraud_2001', '3450', 59);
INSERT INTO iteminfo_data VALUES (837, 'price_logic_min_2001', '5520', 59);
INSERT INTO iteminfo_data VALUES (838, 'price_logic_max_2001', '13800', 59);
INSERT INTO iteminfo_data VALUES (839, 'price_fraud_2007', '9125', 59);
INSERT INTO iteminfo_data VALUES (840, 'price_logic_min_2007', '14600', 59);
INSERT INTO iteminfo_data VALUES (841, 'price_logic_max_2007', '36500', 59);
INSERT INTO iteminfo_data VALUES (842, 'price_fraud_1988', '1500', 34);
INSERT INTO iteminfo_data VALUES (843, 'price_logic_min_1988', '2400', 34);
INSERT INTO iteminfo_data VALUES (844, 'price_logic_max_1988', '6000', 34);
INSERT INTO iteminfo_data VALUES (845, 'price_fraud_1994', '5000', 34);
INSERT INTO iteminfo_data VALUES (846, 'price_logic_min_1994', '8000', 34);
INSERT INTO iteminfo_data VALUES (847, 'price_logic_max_1994', '20000', 34);
INSERT INTO iteminfo_data VALUES (848, 'price_fraud_1995', '1600', 34);
INSERT INTO iteminfo_data VALUES (849, 'price_logic_min_1995', '2560', 34);
INSERT INTO iteminfo_data VALUES (850, 'price_logic_max_1995', '6400', 34);
INSERT INTO iteminfo_data VALUES (851, 'price_fraud_1996', '1641.5', 34);
INSERT INTO iteminfo_data VALUES (852, 'price_logic_min_1996', '2626.4', 34);
INSERT INTO iteminfo_data VALUES (853, 'price_logic_max_1996', '6566', 34);
INSERT INTO iteminfo_data VALUES (854, 'price_fraud_1997', '1526', 34);
INSERT INTO iteminfo_data VALUES (855, 'price_logic_min_1997', '2441.6', 34);
INSERT INTO iteminfo_data VALUES (856, 'price_logic_max_1997', '6104', 34);
INSERT INTO iteminfo_data VALUES (857, 'price_fraud_1998', '1791', 34);
INSERT INTO iteminfo_data VALUES (858, 'price_logic_min_1998', '2865.6', 34);
INSERT INTO iteminfo_data VALUES (859, 'price_logic_max_1998', '7164', 34);
INSERT INTO iteminfo_data VALUES (860, 'price_fraud_1999', '2001.5', 34);
INSERT INTO iteminfo_data VALUES (861, 'price_logic_min_1999', '3202.4', 34);
INSERT INTO iteminfo_data VALUES (862, 'price_logic_max_1999', '8006', 34);
INSERT INTO iteminfo_data VALUES (863, 'price_fraud_2000', '2689', 34);
INSERT INTO iteminfo_data VALUES (864, 'price_logic_min_2000', '4302.4', 34);
INSERT INTO iteminfo_data VALUES (865, 'price_logic_max_2000', '10756', 34);
INSERT INTO iteminfo_data VALUES (866, 'price_fraud_2001', '3090.5', 34);
INSERT INTO iteminfo_data VALUES (867, 'price_logic_min_2001', '4944.8', 34);
INSERT INTO iteminfo_data VALUES (868, 'price_logic_max_2001', '12362', 34);
INSERT INTO iteminfo_data VALUES (869, 'price_fraud_2002', '3618', 34);
INSERT INTO iteminfo_data VALUES (870, 'price_logic_min_2002', '5788.8', 34);
INSERT INTO iteminfo_data VALUES (871, 'price_logic_max_2002', '14472', 34);
INSERT INTO iteminfo_data VALUES (872, 'price_fraud_2003', '4496.5', 34);
INSERT INTO iteminfo_data VALUES (873, 'price_logic_min_2003', '7194.4', 34);
INSERT INTO iteminfo_data VALUES (874, 'price_logic_max_2003', '17986', 34);
INSERT INTO iteminfo_data VALUES (875, 'price_fraud_2004', '5294', 34);
INSERT INTO iteminfo_data VALUES (876, 'price_logic_min_2004', '8470.4', 34);
INSERT INTO iteminfo_data VALUES (877, 'price_logic_max_2004', '21176', 34);
INSERT INTO iteminfo_data VALUES (878, 'price_fraud_2005', '6196.5', 34);
INSERT INTO iteminfo_data VALUES (879, 'price_logic_min_2005', '9914.4', 34);
INSERT INTO iteminfo_data VALUES (880, 'price_logic_max_2005', '24786', 34);
INSERT INTO iteminfo_data VALUES (881, 'price_fraud_2006', '7274.5', 34);
INSERT INTO iteminfo_data VALUES (882, 'price_logic_min_2006', '11639.2', 34);
INSERT INTO iteminfo_data VALUES (883, 'price_logic_max_2006', '29098', 34);
INSERT INTO iteminfo_data VALUES (884, 'price_fraud_2007', '8958', 34);
INSERT INTO iteminfo_data VALUES (885, 'price_logic_min_2007', '14332.8', 34);
INSERT INTO iteminfo_data VALUES (886, 'price_logic_max_2007', '35832', 34);
INSERT INTO iteminfo_data VALUES (887, 'price_fraud_2008', '9717.5', 34);
INSERT INTO iteminfo_data VALUES (888, 'price_logic_min_2008', '15548', 34);
INSERT INTO iteminfo_data VALUES (889, 'price_logic_max_2008', '38870', 34);
INSERT INTO iteminfo_data VALUES (890, 'price_fraud_1975', '1040', 28);
INSERT INTO iteminfo_data VALUES (891, 'price_logic_min_1975', '1664', 28);
INSERT INTO iteminfo_data VALUES (892, 'price_logic_max_1975', '4160', 28);
INSERT INTO iteminfo_data VALUES (893, 'price_fraud_1976', '552', 28);
INSERT INTO iteminfo_data VALUES (894, 'price_logic_min_1976', '883.2', 28);
INSERT INTO iteminfo_data VALUES (895, 'price_logic_max_1976', '2208', 28);
INSERT INTO iteminfo_data VALUES (896, 'price_fraud_1977', '434.5', 28);
INSERT INTO iteminfo_data VALUES (897, 'price_logic_min_1977', '695.2', 28);
INSERT INTO iteminfo_data VALUES (898, 'price_logic_max_1977', '1738', 28);
INSERT INTO iteminfo_data VALUES (899, 'price_fraud_1978', '508', 28);
INSERT INTO iteminfo_data VALUES (900, 'price_logic_min_1978', '812.8', 28);
INSERT INTO iteminfo_data VALUES (901, 'price_logic_max_1978', '2032', 28);
INSERT INTO iteminfo_data VALUES (902, 'price_fraud_1979', '506.5', 28);
INSERT INTO iteminfo_data VALUES (903, 'price_logic_min_1979', '810.4', 28);
INSERT INTO iteminfo_data VALUES (904, 'price_logic_max_1979', '2026', 28);
INSERT INTO iteminfo_data VALUES (905, 'price_fraud_1980', '585.5', 28);
INSERT INTO iteminfo_data VALUES (906, 'price_logic_min_1980', '936.8', 28);
INSERT INTO iteminfo_data VALUES (907, 'price_logic_max_1980', '2342', 28);
INSERT INTO iteminfo_data VALUES (908, 'price_fraud_1981', '425', 28);
INSERT INTO iteminfo_data VALUES (909, 'price_logic_min_1981', '680', 28);
INSERT INTO iteminfo_data VALUES (910, 'price_logic_max_1981', '1700', 28);
INSERT INTO iteminfo_data VALUES (911, 'price_fraud_1982', '415', 28);
INSERT INTO iteminfo_data VALUES (912, 'price_logic_min_1982', '664', 28);
INSERT INTO iteminfo_data VALUES (913, 'price_logic_max_1982', '1660', 28);
INSERT INTO iteminfo_data VALUES (914, 'price_fraud_1983', '531.5', 28);
INSERT INTO iteminfo_data VALUES (915, 'price_logic_min_1983', '850.4', 28);
INSERT INTO iteminfo_data VALUES (916, 'price_logic_max_1983', '2126', 28);
INSERT INTO iteminfo_data VALUES (917, 'price_fraud_1984', '463', 28);
INSERT INTO iteminfo_data VALUES (918, 'price_logic_min_1984', '740.8', 28);
INSERT INTO iteminfo_data VALUES (919, 'price_logic_max_1984', '1852', 28);
INSERT INTO iteminfo_data VALUES (920, 'price_fraud_1985', '393.5', 28);
INSERT INTO iteminfo_data VALUES (921, 'price_logic_min_1985', '629.6', 28);
INSERT INTO iteminfo_data VALUES (922, 'price_logic_max_1985', '1574', 28);
INSERT INTO iteminfo_data VALUES (923, 'price_fraud_1986', '383', 28);
INSERT INTO iteminfo_data VALUES (924, 'price_logic_min_1986', '612.8', 28);
INSERT INTO iteminfo_data VALUES (925, 'price_logic_max_1986', '1532', 28);
INSERT INTO iteminfo_data VALUES (926, 'price_fraud_1987', '413', 28);
INSERT INTO iteminfo_data VALUES (927, 'price_logic_min_1987', '660.8', 28);
INSERT INTO iteminfo_data VALUES (928, 'price_logic_max_1987', '1652', 28);
INSERT INTO iteminfo_data VALUES (929, 'price_fraud_1988', '413.5', 28);
INSERT INTO iteminfo_data VALUES (930, 'price_logic_min_1988', '661.6', 28);
INSERT INTO iteminfo_data VALUES (931, 'price_logic_max_1988', '1654', 28);
INSERT INTO iteminfo_data VALUES (932, 'price_fraud_1989', '408.5', 28);
INSERT INTO iteminfo_data VALUES (933, 'price_logic_min_1989', '653.6', 28);
INSERT INTO iteminfo_data VALUES (934, 'price_logic_max_1989', '1634', 28);
INSERT INTO iteminfo_data VALUES (935, 'price_fraud_1990', '472', 28);
INSERT INTO iteminfo_data VALUES (936, 'price_logic_min_1990', '755.2', 28);
INSERT INTO iteminfo_data VALUES (937, 'price_logic_max_1990', '1888', 28);
INSERT INTO iteminfo_data VALUES (938, 'price_fraud_1991', '550.5', 28);
INSERT INTO iteminfo_data VALUES (939, 'price_logic_min_1991', '880.8', 28);
INSERT INTO iteminfo_data VALUES (940, 'price_logic_max_1991', '2202', 28);
INSERT INTO iteminfo_data VALUES (941, 'price_fraud_1992', '628.5', 28);
INSERT INTO iteminfo_data VALUES (942, 'price_logic_min_1992', '1005.6', 28);
INSERT INTO iteminfo_data VALUES (943, 'price_logic_max_1992', '2514', 28);
INSERT INTO iteminfo_data VALUES (944, 'price_fraud_1993', '751.5', 28);
INSERT INTO iteminfo_data VALUES (945, 'price_logic_min_1993', '1202.4', 28);
INSERT INTO iteminfo_data VALUES (946, 'price_logic_max_1993', '3006', 28);
INSERT INTO iteminfo_data VALUES (947, 'price_fraud_1994', '886', 28);
INSERT INTO iteminfo_data VALUES (948, 'price_logic_min_1994', '1417.6', 28);
INSERT INTO iteminfo_data VALUES (949, 'price_logic_max_1994', '3544', 28);
INSERT INTO iteminfo_data VALUES (950, 'price_fraud_1995', '1025', 28);
INSERT INTO iteminfo_data VALUES (951, 'price_logic_min_1995', '1640', 28);
INSERT INTO iteminfo_data VALUES (952, 'price_logic_max_1995', '4100', 28);
INSERT INTO iteminfo_data VALUES (953, 'price_fraud_1996', '1200.5', 28);
INSERT INTO iteminfo_data VALUES (954, 'price_logic_min_1996', '1920.8', 28);
INSERT INTO iteminfo_data VALUES (955, 'price_logic_max_1996', '4802', 28);
INSERT INTO iteminfo_data VALUES (956, 'price_fraud_1997', '1526.5', 28);
INSERT INTO iteminfo_data VALUES (957, 'price_logic_min_1997', '2442.4', 28);
INSERT INTO iteminfo_data VALUES (958, 'price_logic_max_1997', '6106', 28);
INSERT INTO iteminfo_data VALUES (959, 'price_fraud_1998', '1923.5', 28);
INSERT INTO iteminfo_data VALUES (960, 'price_logic_min_1998', '3077.6', 28);
INSERT INTO iteminfo_data VALUES (961, 'price_logic_max_1998', '7694', 28);
INSERT INTO iteminfo_data VALUES (962, 'price_fraud_1999', '2253.5', 28);
INSERT INTO iteminfo_data VALUES (963, 'price_logic_min_1999', '3605.6', 28);
INSERT INTO iteminfo_data VALUES (964, 'price_logic_max_1999', '9014', 28);
INSERT INTO iteminfo_data VALUES (965, 'price_fraud_2000', '2633.5', 28);
INSERT INTO iteminfo_data VALUES (966, 'price_logic_min_2000', '4213.6', 28);
INSERT INTO iteminfo_data VALUES (967, 'price_logic_max_2000', '10534', 28);
INSERT INTO iteminfo_data VALUES (968, 'price_fraud_2001', '3158', 28);
INSERT INTO iteminfo_data VALUES (969, 'price_logic_min_2001', '5052.8', 28);
INSERT INTO iteminfo_data VALUES (970, 'price_logic_max_2001', '12632', 28);
INSERT INTO iteminfo_data VALUES (971, 'price_fraud_2002', '3611', 28);
INSERT INTO iteminfo_data VALUES (972, 'price_logic_min_2002', '5777.6', 28);
INSERT INTO iteminfo_data VALUES (973, 'price_logic_max_2002', '14444', 28);
INSERT INTO iteminfo_data VALUES (974, 'price_fraud_2003', '4475', 28);
INSERT INTO iteminfo_data VALUES (975, 'price_logic_min_2003', '7160', 28);
INSERT INTO iteminfo_data VALUES (976, 'price_logic_max_2003', '17900', 28);
INSERT INTO iteminfo_data VALUES (977, 'price_fraud_2004', '5082.5', 28);
INSERT INTO iteminfo_data VALUES (978, 'price_logic_min_2004', '8132', 28);
INSERT INTO iteminfo_data VALUES (979, 'price_logic_max_2004', '20330', 28);
INSERT INTO iteminfo_data VALUES (980, 'price_fraud_2005', '5637', 28);
INSERT INTO iteminfo_data VALUES (981, 'price_logic_min_2005', '9019.2', 28);
INSERT INTO iteminfo_data VALUES (982, 'price_logic_max_2005', '22548', 28);
INSERT INTO iteminfo_data VALUES (983, 'price_fraud_2006', '6450', 28);
INSERT INTO iteminfo_data VALUES (984, 'price_logic_min_2006', '10320', 28);
INSERT INTO iteminfo_data VALUES (985, 'price_logic_max_2006', '25800', 28);
INSERT INTO iteminfo_data VALUES (986, 'price_fraud_2007', '7576', 28);
INSERT INTO iteminfo_data VALUES (987, 'price_logic_min_2007', '12121.6', 28);
INSERT INTO iteminfo_data VALUES (988, 'price_logic_max_2007', '30304', 28);
INSERT INTO iteminfo_data VALUES (989, 'price_fraud_2008', '8842.5', 28);
INSERT INTO iteminfo_data VALUES (990, 'price_logic_min_2008', '14148', 28);
INSERT INTO iteminfo_data VALUES (991, 'price_logic_max_2008', '35370', 28);
INSERT INTO iteminfo_data VALUES (992, 'price_fraud_1987', '750', 35);
INSERT INTO iteminfo_data VALUES (993, 'price_logic_min_1987', '1200', 35);
INSERT INTO iteminfo_data VALUES (994, 'price_logic_max_1987', '3000', 35);
INSERT INTO iteminfo_data VALUES (995, 'price_fraud_1989', '750', 35);
INSERT INTO iteminfo_data VALUES (996, 'price_logic_min_1989', '1200', 35);
INSERT INTO iteminfo_data VALUES (997, 'price_logic_max_1989', '3000', 35);
INSERT INTO iteminfo_data VALUES (998, 'price_fraud_1991', '350', 35);
INSERT INTO iteminfo_data VALUES (999, 'price_logic_min_1991', '560', 35);
INSERT INTO iteminfo_data VALUES (1000, 'price_logic_max_1991', '1400', 35);
INSERT INTO iteminfo_data VALUES (1001, 'price_fraud_1993', '775', 35);
INSERT INTO iteminfo_data VALUES (1002, 'price_logic_min_1993', '1240', 35);
INSERT INTO iteminfo_data VALUES (1003, 'price_logic_max_1993', '3100', 35);
INSERT INTO iteminfo_data VALUES (1004, 'price_fraud_1994', '856', 35);
INSERT INTO iteminfo_data VALUES (1005, 'price_logic_min_1994', '1369.6', 35);
INSERT INTO iteminfo_data VALUES (1006, 'price_logic_max_1994', '3424', 35);
INSERT INTO iteminfo_data VALUES (1007, 'price_fraud_1995', '699', 35);
INSERT INTO iteminfo_data VALUES (1008, 'price_logic_min_1995', '1118.4', 35);
INSERT INTO iteminfo_data VALUES (1009, 'price_logic_max_1995', '2796', 35);
INSERT INTO iteminfo_data VALUES (1010, 'price_fraud_1996', '825', 35);
INSERT INTO iteminfo_data VALUES (1011, 'price_logic_min_1996', '1320', 35);
INSERT INTO iteminfo_data VALUES (1012, 'price_logic_max_1996', '3300', 35);
INSERT INTO iteminfo_data VALUES (1013, 'price_fraud_1997', '1232.5', 35);
INSERT INTO iteminfo_data VALUES (1014, 'price_logic_min_1997', '1972', 35);
INSERT INTO iteminfo_data VALUES (1015, 'price_logic_max_1997', '4930', 35);
INSERT INTO iteminfo_data VALUES (1016, 'price_fraud_1998', '1326', 35);
INSERT INTO iteminfo_data VALUES (1017, 'price_logic_min_1998', '2121.6', 35);
INSERT INTO iteminfo_data VALUES (1018, 'price_logic_max_1998', '5304', 35);
INSERT INTO iteminfo_data VALUES (1019, 'price_fraud_1999', '1411', 35);
INSERT INTO iteminfo_data VALUES (1020, 'price_logic_min_1999', '2257.6', 35);
INSERT INTO iteminfo_data VALUES (1021, 'price_logic_max_1999', '5644', 35);
INSERT INTO iteminfo_data VALUES (1022, 'price_fraud_2000', '2046', 35);
INSERT INTO iteminfo_data VALUES (1023, 'price_logic_min_2000', '3273.6', 35);
INSERT INTO iteminfo_data VALUES (1024, 'price_logic_max_2000', '8184', 35);
INSERT INTO iteminfo_data VALUES (1025, 'price_fraud_2001', '2396', 35);
INSERT INTO iteminfo_data VALUES (1026, 'price_logic_min_2001', '3833.6', 35);
INSERT INTO iteminfo_data VALUES (1027, 'price_logic_max_2001', '9584', 35);
INSERT INTO iteminfo_data VALUES (1028, 'price_fraud_2002', '2957.5', 35);
INSERT INTO iteminfo_data VALUES (1029, 'price_logic_min_2002', '4732', 35);
INSERT INTO iteminfo_data VALUES (1030, 'price_logic_max_2002', '11830', 35);
INSERT INTO iteminfo_data VALUES (1031, 'price_fraud_2003', '3562', 35);
INSERT INTO iteminfo_data VALUES (1032, 'price_logic_min_2003', '5699.2', 35);
INSERT INTO iteminfo_data VALUES (1033, 'price_logic_max_2003', '14248', 35);
INSERT INTO iteminfo_data VALUES (1034, 'price_fraud_2004', '3935', 35);
INSERT INTO iteminfo_data VALUES (1035, 'price_logic_min_2004', '6296', 35);
INSERT INTO iteminfo_data VALUES (1036, 'price_logic_max_2004', '15740', 35);
INSERT INTO iteminfo_data VALUES (1037, 'price_fraud_2005', '4332', 35);
INSERT INTO iteminfo_data VALUES (1038, 'price_logic_min_2005', '6931.2', 35);
INSERT INTO iteminfo_data VALUES (1039, 'price_logic_max_2005', '17328', 35);
INSERT INTO iteminfo_data VALUES (1040, 'price_fraud_2006', '4738.5', 35);
INSERT INTO iteminfo_data VALUES (1041, 'price_logic_min_2006', '7581.6', 35);
INSERT INTO iteminfo_data VALUES (1042, 'price_logic_max_2006', '18954', 35);
INSERT INTO iteminfo_data VALUES (1043, 'price_fraud_2007', '5379', 35);
INSERT INTO iteminfo_data VALUES (1044, 'price_logic_min_2007', '8606.4', 35);
INSERT INTO iteminfo_data VALUES (1045, 'price_logic_max_2007', '21516', 35);
INSERT INTO iteminfo_data VALUES (1046, 'price_fraud_2008', '5580.5', 35);
INSERT INTO iteminfo_data VALUES (1047, 'price_logic_min_2008', '8928.8', 35);
INSERT INTO iteminfo_data VALUES (1048, 'price_logic_max_2008', '22322', 35);
INSERT INTO iteminfo_data VALUES (1049, 'price_fraud_2002', '3400', 51);
INSERT INTO iteminfo_data VALUES (1050, 'price_logic_min_2002', '5440', 51);
INSERT INTO iteminfo_data VALUES (1051, 'price_logic_max_2002', '13600', 51);
INSERT INTO iteminfo_data VALUES (1052, 'price_fraud_2003', '3200', 51);
INSERT INTO iteminfo_data VALUES (1053, 'price_logic_min_2003', '5120', 51);
INSERT INTO iteminfo_data VALUES (1054, 'price_logic_max_2003', '12800', 51);
INSERT INTO iteminfo_data VALUES (1055, 'price_fraud_2004', '3250', 51);
INSERT INTO iteminfo_data VALUES (1056, 'price_logic_min_2004', '5200', 51);
INSERT INTO iteminfo_data VALUES (1057, 'price_logic_max_2004', '13000', 51);
INSERT INTO iteminfo_data VALUES (1058, 'price_fraud_2005', '3350', 51);
INSERT INTO iteminfo_data VALUES (1059, 'price_logic_min_2005', '5360', 51);
INSERT INTO iteminfo_data VALUES (1060, 'price_logic_max_2005', '13400', 51);
INSERT INTO iteminfo_data VALUES (1061, 'price_fraud_1984', '1500', 36);
INSERT INTO iteminfo_data VALUES (1062, 'price_logic_min_1984', '2400', 36);
INSERT INTO iteminfo_data VALUES (1063, 'price_logic_max_1984', '6000', 36);
INSERT INTO iteminfo_data VALUES (1064, 'price_fraud_1985', '300', 36);
INSERT INTO iteminfo_data VALUES (1065, 'price_logic_min_1985', '480', 36);
INSERT INTO iteminfo_data VALUES (1066, 'price_logic_max_1985', '1200', 36);
INSERT INTO iteminfo_data VALUES (1067, 'price_fraud_1986', '250', 36);
INSERT INTO iteminfo_data VALUES (1068, 'price_logic_min_1986', '400', 36);
INSERT INTO iteminfo_data VALUES (1069, 'price_logic_max_1986', '1000', 36);
INSERT INTO iteminfo_data VALUES (1070, 'price_fraud_1987', '418.5', 36);
INSERT INTO iteminfo_data VALUES (1071, 'price_logic_min_1987', '669.6', 36);
INSERT INTO iteminfo_data VALUES (1072, 'price_logic_max_1987', '1674', 36);
INSERT INTO iteminfo_data VALUES (1073, 'price_fraud_1988', '246.5', 36);
INSERT INTO iteminfo_data VALUES (1074, 'price_logic_min_1988', '394.4', 36);
INSERT INTO iteminfo_data VALUES (1075, 'price_logic_max_1988', '986', 36);
INSERT INTO iteminfo_data VALUES (1076, 'price_fraud_1989', '217.5', 36);
INSERT INTO iteminfo_data VALUES (1077, 'price_logic_min_1989', '348', 36);
INSERT INTO iteminfo_data VALUES (1078, 'price_logic_max_1989', '870', 36);
INSERT INTO iteminfo_data VALUES (1079, 'price_fraud_1990', '385', 36);
INSERT INTO iteminfo_data VALUES (1080, 'price_logic_min_1990', '616', 36);
INSERT INTO iteminfo_data VALUES (1081, 'price_logic_max_1990', '1540', 36);
INSERT INTO iteminfo_data VALUES (1082, 'price_fraud_1991', '365.5', 36);
INSERT INTO iteminfo_data VALUES (1083, 'price_logic_min_1991', '584.8', 36);
INSERT INTO iteminfo_data VALUES (1084, 'price_logic_max_1991', '1462', 36);
INSERT INTO iteminfo_data VALUES (1085, 'price_fraud_1992', '418.5', 36);
INSERT INTO iteminfo_data VALUES (1086, 'price_logic_min_1992', '669.6', 36);
INSERT INTO iteminfo_data VALUES (1087, 'price_logic_max_1992', '1674', 36);
INSERT INTO iteminfo_data VALUES (1088, 'price_fraud_1993', '419', 36);
INSERT INTO iteminfo_data VALUES (1089, 'price_logic_min_1993', '670.4', 36);
INSERT INTO iteminfo_data VALUES (1090, 'price_logic_max_1993', '1676', 36);
INSERT INTO iteminfo_data VALUES (1091, 'price_fraud_1994', '684.5', 36);
INSERT INTO iteminfo_data VALUES (1092, 'price_logic_min_1994', '1095.2', 36);
INSERT INTO iteminfo_data VALUES (1093, 'price_logic_max_1994', '2738', 36);
INSERT INTO iteminfo_data VALUES (1094, 'price_fraud_1995', '826.5', 36);
INSERT INTO iteminfo_data VALUES (1095, 'price_logic_min_1995', '1322.4', 36);
INSERT INTO iteminfo_data VALUES (1096, 'price_logic_max_1995', '3306', 36);
INSERT INTO iteminfo_data VALUES (1097, 'price_fraud_1996', '813.5', 36);
INSERT INTO iteminfo_data VALUES (1098, 'price_logic_min_1996', '1301.6', 36);
INSERT INTO iteminfo_data VALUES (1099, 'price_logic_max_1996', '3254', 36);
INSERT INTO iteminfo_data VALUES (1100, 'price_fraud_1997', '1050.5', 36);
INSERT INTO iteminfo_data VALUES (1101, 'price_logic_min_1997', '1680.8', 36);
INSERT INTO iteminfo_data VALUES (1102, 'price_logic_max_1997', '4202', 36);
INSERT INTO iteminfo_data VALUES (1103, 'price_fraud_1998', '1403', 36);
INSERT INTO iteminfo_data VALUES (1104, 'price_logic_min_1998', '2244.8', 36);
INSERT INTO iteminfo_data VALUES (1105, 'price_logic_max_1998', '5612', 36);
INSERT INTO iteminfo_data VALUES (1106, 'price_fraud_1999', '1567', 36);
INSERT INTO iteminfo_data VALUES (1107, 'price_logic_min_1999', '2507.2', 36);
INSERT INTO iteminfo_data VALUES (1108, 'price_logic_max_1999', '6268', 36);
INSERT INTO iteminfo_data VALUES (1109, 'price_fraud_2000', '2024.5', 36);
INSERT INTO iteminfo_data VALUES (1110, 'price_logic_min_2000', '3239.2', 36);
INSERT INTO iteminfo_data VALUES (1111, 'price_logic_max_2000', '8098', 36);
INSERT INTO iteminfo_data VALUES (1112, 'price_fraud_2001', '2379', 36);
INSERT INTO iteminfo_data VALUES (1113, 'price_logic_min_2001', '3806.4', 36);
INSERT INTO iteminfo_data VALUES (1114, 'price_logic_max_2001', '9516', 36);
INSERT INTO iteminfo_data VALUES (1115, 'price_fraud_2002', '3110.5', 36);
INSERT INTO iteminfo_data VALUES (1116, 'price_logic_min_2002', '4976.8', 36);
INSERT INTO iteminfo_data VALUES (1117, 'price_logic_max_2002', '12442', 36);
INSERT INTO iteminfo_data VALUES (1118, 'price_fraud_2003', '3624', 36);
INSERT INTO iteminfo_data VALUES (1119, 'price_logic_min_2003', '5798.4', 36);
INSERT INTO iteminfo_data VALUES (1120, 'price_logic_max_2003', '14496', 36);
INSERT INTO iteminfo_data VALUES (1121, 'price_fraud_2004', '3941', 36);
INSERT INTO iteminfo_data VALUES (1122, 'price_logic_min_2004', '6305.6', 36);
INSERT INTO iteminfo_data VALUES (1123, 'price_logic_max_2004', '15764', 36);
INSERT INTO iteminfo_data VALUES (1124, 'price_fraud_2005', '4407', 36);
INSERT INTO iteminfo_data VALUES (1125, 'price_logic_min_2005', '7051.2', 36);
INSERT INTO iteminfo_data VALUES (1126, 'price_logic_max_2005', '17628', 36);
INSERT INTO iteminfo_data VALUES (1127, 'price_fraud_2006', '5228', 36);
INSERT INTO iteminfo_data VALUES (1128, 'price_logic_min_2006', '8364.8', 36);
INSERT INTO iteminfo_data VALUES (1129, 'price_logic_max_2006', '20912', 36);
INSERT INTO iteminfo_data VALUES (1130, 'price_fraud_2007', '6039', 36);
INSERT INTO iteminfo_data VALUES (1131, 'price_logic_min_2007', '9662.4', 36);
INSERT INTO iteminfo_data VALUES (1132, 'price_logic_max_2007', '24156', 36);
INSERT INTO iteminfo_data VALUES (1133, 'price_fraud_2008', '7001', 36);
INSERT INTO iteminfo_data VALUES (1134, 'price_logic_min_2008', '11201.6', 36);
INSERT INTO iteminfo_data VALUES (1135, 'price_logic_max_2008', '28004', 36);
INSERT INTO iteminfo_data VALUES (1136, 'price_fraud_2005', '5950', 52);
INSERT INTO iteminfo_data VALUES (1137, 'price_logic_min_2005', '9520', 52);
INSERT INTO iteminfo_data VALUES (1138, 'price_logic_max_2005', '23800', 52);
INSERT INTO iteminfo_data VALUES (1139, 'price_fraud_2006', '6500', 52);
INSERT INTO iteminfo_data VALUES (1140, 'price_logic_min_2006', '10400', 52);
INSERT INTO iteminfo_data VALUES (1141, 'price_logic_max_2006', '26000', 52);
INSERT INTO iteminfo_data VALUES (1142, 'price_fraud_2007', '7203', 52);
INSERT INTO iteminfo_data VALUES (1143, 'price_logic_min_2007', '11524.8', 52);
INSERT INTO iteminfo_data VALUES (1144, 'price_logic_max_2007', '28812', 52);
INSERT INTO iteminfo_data VALUES (1145, 'price_fraud_2008', '9750', 52);
INSERT INTO iteminfo_data VALUES (1146, 'price_logic_min_2008', '15600', 52);
INSERT INTO iteminfo_data VALUES (1147, 'price_logic_max_2008', '39000', 52);
INSERT INTO iteminfo_data VALUES (1148, 'price_fraud_2005', '6987.5', 53);
INSERT INTO iteminfo_data VALUES (1149, 'price_logic_min_2005', '11180', 53);
INSERT INTO iteminfo_data VALUES (1150, 'price_logic_max_2005', '27950', 53);
INSERT INTO iteminfo_data VALUES (1151, 'price_fraud_2006', '7982.5', 53);
INSERT INTO iteminfo_data VALUES (1152, 'price_logic_min_2006', '12772', 53);
INSERT INTO iteminfo_data VALUES (1153, 'price_logic_max_2006', '31930', 53);
INSERT INTO iteminfo_data VALUES (1154, 'price_fraud_2007', '8383.5', 53);
INSERT INTO iteminfo_data VALUES (1155, 'price_logic_min_2007', '13413.6', 53);
INSERT INTO iteminfo_data VALUES (1156, 'price_logic_max_2007', '33534', 53);
INSERT INTO iteminfo_data VALUES (1157, 'price_fraud_2008', '10250', 53);
INSERT INTO iteminfo_data VALUES (1158, 'price_logic_min_2008', '16400', 53);
INSERT INTO iteminfo_data VALUES (1159, 'price_logic_max_2008', '41000', 53);
INSERT INTO iteminfo_data VALUES (1160, 'price_fraud_2000', '2980', 54);
INSERT INTO iteminfo_data VALUES (1161, 'price_logic_min_2000', '4768', 54);
INSERT INTO iteminfo_data VALUES (1162, 'price_logic_max_2000', '11920', 54);
INSERT INTO iteminfo_data VALUES (1163, 'price_fraud_2001', '3241.5', 54);
INSERT INTO iteminfo_data VALUES (1164, 'price_logic_min_2001', '5186.4', 54);
INSERT INTO iteminfo_data VALUES (1165, 'price_logic_max_2001', '12966', 54);
INSERT INTO iteminfo_data VALUES (1166, 'price_fraud_2002', '3950', 54);
INSERT INTO iteminfo_data VALUES (1167, 'price_logic_min_2002', '6320', 54);
INSERT INTO iteminfo_data VALUES (1168, 'price_logic_max_2002', '15800', 54);
INSERT INTO iteminfo_data VALUES (1169, 'price_fraud_2003', '4100', 54);
INSERT INTO iteminfo_data VALUES (1170, 'price_logic_min_2003', '6560', 54);
INSERT INTO iteminfo_data VALUES (1171, 'price_logic_max_2003', '16400', 54);
INSERT INTO iteminfo_data VALUES (1172, 'price_fraud_2004', '5033', 54);
INSERT INTO iteminfo_data VALUES (1173, 'price_logic_min_2004', '8052.8', 54);
INSERT INTO iteminfo_data VALUES (1174, 'price_logic_max_2004', '20132', 54);
INSERT INTO iteminfo_data VALUES (1175, 'price_fraud_2002', '5250', 55);
INSERT INTO iteminfo_data VALUES (1176, 'price_logic_min_2002', '8400', 55);
INSERT INTO iteminfo_data VALUES (1177, 'price_logic_max_2002', '21000', 55);
INSERT INTO iteminfo_data VALUES (1178, 'price_fraud_2003', '5750', 55);
INSERT INTO iteminfo_data VALUES (1179, 'price_logic_min_2003', '9200', 55);
INSERT INTO iteminfo_data VALUES (1180, 'price_logic_max_2003', '23000', 55);
INSERT INTO iteminfo_data VALUES (1181, 'price_fraud_2004', '7014', 55);
INSERT INTO iteminfo_data VALUES (1182, 'price_logic_min_2004', '11222.4', 55);
INSERT INTO iteminfo_data VALUES (1183, 'price_logic_max_2004', '28056', 55);
INSERT INTO iteminfo_data VALUES (1184, 'price_fraud_2005', '7406', 55);
INSERT INTO iteminfo_data VALUES (1185, 'price_logic_min_2005', '11849.6', 55);
INSERT INTO iteminfo_data VALUES (1186, 'price_logic_max_2005', '29624', 55);
INSERT INTO iteminfo_data VALUES (1187, 'price_fraud_1999', '2250', 37);
INSERT INTO iteminfo_data VALUES (1188, 'price_logic_min_1999', '3600', 37);
INSERT INTO iteminfo_data VALUES (1189, 'price_logic_max_1999', '9000', 37);
INSERT INTO iteminfo_data VALUES (1190, 'price_fraud_2000', '3055.5', 37);
INSERT INTO iteminfo_data VALUES (1191, 'price_logic_min_2000', '4888.8', 37);
INSERT INTO iteminfo_data VALUES (1192, 'price_logic_max_2000', '12222', 37);
INSERT INTO iteminfo_data VALUES (1193, 'price_fraud_2001', '3446', 37);
INSERT INTO iteminfo_data VALUES (1194, 'price_logic_min_2001', '5513.6', 37);
INSERT INTO iteminfo_data VALUES (1195, 'price_logic_max_2001', '13784', 37);
INSERT INTO iteminfo_data VALUES (1196, 'price_fraud_2002', '4447.5', 37);
INSERT INTO iteminfo_data VALUES (1197, 'price_logic_min_2002', '7116', 37);
INSERT INTO iteminfo_data VALUES (1198, 'price_logic_max_2002', '17790', 37);
INSERT INTO iteminfo_data VALUES (1199, 'price_fraud_2003', '5235.5', 37);
INSERT INTO iteminfo_data VALUES (1200, 'price_logic_min_2003', '8376.8', 37);
INSERT INTO iteminfo_data VALUES (1201, 'price_logic_max_2003', '20942', 37);
INSERT INTO iteminfo_data VALUES (1202, 'price_fraud_2004', '5725.5', 37);
INSERT INTO iteminfo_data VALUES (1203, 'price_logic_min_2004', '9160.8', 37);
INSERT INTO iteminfo_data VALUES (1204, 'price_logic_max_2004', '22902', 37);
INSERT INTO iteminfo_data VALUES (1205, 'price_fraud_2005', '6721', 37);
INSERT INTO iteminfo_data VALUES (1206, 'price_logic_min_2005', '10753.6', 37);
INSERT INTO iteminfo_data VALUES (1207, 'price_logic_max_2005', '26884', 37);
INSERT INTO iteminfo_data VALUES (1208, 'price_fraud_2006', '7476.5', 37);
INSERT INTO iteminfo_data VALUES (1209, 'price_logic_min_2006', '11962.4', 37);
INSERT INTO iteminfo_data VALUES (1210, 'price_logic_max_2006', '29906', 37);
INSERT INTO iteminfo_data VALUES (1211, 'price_fraud_2007', '8677', 37);
INSERT INTO iteminfo_data VALUES (1212, 'price_logic_min_2007', '13883.2', 37);
INSERT INTO iteminfo_data VALUES (1213, 'price_logic_max_2007', '34708', 37);
INSERT INTO iteminfo_data VALUES (1214, 'price_fraud_2008', '10337.5', 37);
INSERT INTO iteminfo_data VALUES (1215, 'price_logic_min_2008', '16540', 37);
INSERT INTO iteminfo_data VALUES (1216, 'price_logic_max_2008', '41350', 37);
INSERT INTO iteminfo_data VALUES (1217, 'price_fraud_1983', '300', 29);
INSERT INTO iteminfo_data VALUES (1218, 'price_logic_min_1983', '480', 29);
INSERT INTO iteminfo_data VALUES (1219, 'price_logic_max_1983', '1200', 29);
INSERT INTO iteminfo_data VALUES (1220, 'price_fraud_1984', '1500', 29);
INSERT INTO iteminfo_data VALUES (1221, 'price_logic_min_1984', '2400', 29);
INSERT INTO iteminfo_data VALUES (1222, 'price_logic_max_1984', '6000', 29);
INSERT INTO iteminfo_data VALUES (1223, 'price_fraud_1985', '300', 29);
INSERT INTO iteminfo_data VALUES (1224, 'price_logic_min_1985', '480', 29);
INSERT INTO iteminfo_data VALUES (1225, 'price_logic_max_1985', '1200', 29);
INSERT INTO iteminfo_data VALUES (1226, 'price_fraud_1986', '290', 29);
INSERT INTO iteminfo_data VALUES (1227, 'price_logic_min_1986', '464', 29);
INSERT INTO iteminfo_data VALUES (1228, 'price_logic_max_1986', '1160', 29);
INSERT INTO iteminfo_data VALUES (1229, 'price_fraud_1987', '458', 29);
INSERT INTO iteminfo_data VALUES (1230, 'price_logic_min_1987', '732.8', 29);
INSERT INTO iteminfo_data VALUES (1231, 'price_logic_max_1987', '1832', 29);
INSERT INTO iteminfo_data VALUES (1232, 'price_fraud_1988', '301.5', 29);
INSERT INTO iteminfo_data VALUES (1233, 'price_logic_min_1988', '482.4', 29);
INSERT INTO iteminfo_data VALUES (1234, 'price_logic_max_1988', '1206', 29);
INSERT INTO iteminfo_data VALUES (1235, 'price_fraud_1989', '281', 29);
INSERT INTO iteminfo_data VALUES (1236, 'price_logic_min_1989', '449.6', 29);
INSERT INTO iteminfo_data VALUES (1237, 'price_logic_max_1989', '1124', 29);
INSERT INTO iteminfo_data VALUES (1238, 'price_fraud_1990', '345', 29);
INSERT INTO iteminfo_data VALUES (1239, 'price_logic_min_1990', '552', 29);
INSERT INTO iteminfo_data VALUES (1240, 'price_logic_max_1990', '1380', 29);
INSERT INTO iteminfo_data VALUES (1241, 'price_fraud_1991', '339', 29);
INSERT INTO iteminfo_data VALUES (1242, 'price_logic_min_1991', '542.4', 29);
INSERT INTO iteminfo_data VALUES (1243, 'price_logic_max_1991', '1356', 29);
INSERT INTO iteminfo_data VALUES (1244, 'price_fraud_1992', '478', 29);
INSERT INTO iteminfo_data VALUES (1245, 'price_logic_min_1992', '764.8', 29);
INSERT INTO iteminfo_data VALUES (1246, 'price_logic_max_1992', '1912', 29);
INSERT INTO iteminfo_data VALUES (1247, 'price_fraud_1993', '470', 29);
INSERT INTO iteminfo_data VALUES (1248, 'price_logic_min_1993', '752', 29);
INSERT INTO iteminfo_data VALUES (1249, 'price_logic_max_1993', '1880', 29);
INSERT INTO iteminfo_data VALUES (1250, 'price_fraud_1994', '692.5', 29);
INSERT INTO iteminfo_data VALUES (1251, 'price_logic_min_1994', '1108', 29);
INSERT INTO iteminfo_data VALUES (1252, 'price_logic_max_1994', '2770', 29);
INSERT INTO iteminfo_data VALUES (1253, 'price_fraud_1995', '758.5', 29);
INSERT INTO iteminfo_data VALUES (1254, 'price_logic_min_1995', '1213.6', 29);
INSERT INTO iteminfo_data VALUES (1255, 'price_logic_max_1995', '3034', 29);
INSERT INTO iteminfo_data VALUES (1256, 'price_fraud_1996', '837', 29);
INSERT INTO iteminfo_data VALUES (1257, 'price_logic_min_1996', '1339.2', 29);
INSERT INTO iteminfo_data VALUES (1258, 'price_logic_max_1996', '3348', 29);
INSERT INTO iteminfo_data VALUES (1259, 'price_fraud_1997', '1160', 29);
INSERT INTO iteminfo_data VALUES (1260, 'price_logic_min_1997', '1856', 29);
INSERT INTO iteminfo_data VALUES (1261, 'price_logic_max_1997', '4640', 29);
INSERT INTO iteminfo_data VALUES (1262, 'price_fraud_1998', '1414.5', 29);
INSERT INTO iteminfo_data VALUES (1263, 'price_logic_min_1998', '2263.2', 29);
INSERT INTO iteminfo_data VALUES (1264, 'price_logic_max_1998', '5658', 29);
INSERT INTO iteminfo_data VALUES (1265, 'price_fraud_1999', '1672.5', 29);
INSERT INTO iteminfo_data VALUES (1266, 'price_logic_min_1999', '2676', 29);
INSERT INTO iteminfo_data VALUES (1267, 'price_logic_max_1999', '6690', 29);
INSERT INTO iteminfo_data VALUES (1268, 'price_fraud_2000', '2244.5', 29);
INSERT INTO iteminfo_data VALUES (1269, 'price_logic_min_2000', '3591.2', 29);
INSERT INTO iteminfo_data VALUES (1270, 'price_logic_max_2000', '8978', 29);
INSERT INTO iteminfo_data VALUES (1271, 'price_fraud_2001', '2835', 29);
INSERT INTO iteminfo_data VALUES (1272, 'price_logic_min_2001', '4536', 29);
INSERT INTO iteminfo_data VALUES (1273, 'price_logic_max_2001', '11340', 29);
INSERT INTO iteminfo_data VALUES (1274, 'price_fraud_2002', '3760', 29);
INSERT INTO iteminfo_data VALUES (1275, 'price_logic_min_2002', '6016', 29);
INSERT INTO iteminfo_data VALUES (1276, 'price_logic_max_2002', '15040', 29);
INSERT INTO iteminfo_data VALUES (1277, 'price_fraud_2003', '4309.5', 29);
INSERT INTO iteminfo_data VALUES (1278, 'price_logic_min_2003', '6895.2', 29);
INSERT INTO iteminfo_data VALUES (1279, 'price_logic_max_2003', '17238', 29);
INSERT INTO iteminfo_data VALUES (1280, 'price_fraud_2004', '4872.5', 29);
INSERT INTO iteminfo_data VALUES (1281, 'price_logic_min_2004', '7796', 29);
INSERT INTO iteminfo_data VALUES (1282, 'price_logic_max_2004', '19490', 29);
INSERT INTO iteminfo_data VALUES (1283, 'price_fraud_2005', '5702.5', 29);
INSERT INTO iteminfo_data VALUES (1284, 'price_logic_min_2005', '9124', 29);
INSERT INTO iteminfo_data VALUES (1285, 'price_logic_max_2005', '22810', 29);
INSERT INTO iteminfo_data VALUES (1286, 'price_fraud_2006', '6530', 29);
INSERT INTO iteminfo_data VALUES (1287, 'price_logic_min_2006', '10448', 29);
INSERT INTO iteminfo_data VALUES (1288, 'price_logic_max_2006', '26120', 29);
INSERT INTO iteminfo_data VALUES (1289, 'price_fraud_2007', '7412.5', 29);
INSERT INTO iteminfo_data VALUES (1290, 'price_logic_min_2007', '11860', 29);
INSERT INTO iteminfo_data VALUES (1291, 'price_logic_max_2007', '29650', 29);
INSERT INTO iteminfo_data VALUES (1292, 'price_fraud_2008', '8648', 29);
INSERT INTO iteminfo_data VALUES (1293, 'price_logic_min_2008', '13836.8', 29);
INSERT INTO iteminfo_data VALUES (1294, 'price_logic_max_2008', '34592', 29);
INSERT INTO iteminfo_data VALUES (10001, 'name', 'rhone alpes', 2001);
INSERT INTO iteminfo_data VALUES (10002, 'name', 'ain', 2002);
INSERT INTO iteminfo_data VALUES (10003, 'name', 'ile de france', 2006);
INSERT INTO iteminfo_data VALUES (10004, 'name', 'paris', 2007);
INSERT INTO iteminfo_data VALUES (10005, 'is_coast', '0', 2001);
INSERT INTO iteminfo_data VALUES (10006, 'is_coast', '0', 2006);
INSERT INTO iteminfo_data VALUES (10007, 'is_mountain', '0', 2001);
INSERT INTO iteminfo_data VALUES (10008, 'is_mountain', '1', 2006);
INSERT INTO iteminfo_data VALUES (10009, 'price_fraud_cat_1080_s', '738.23986261018', 2001);
INSERT INTO iteminfo_data VALUES (10010, 'price_logic_min_cat_1080_s', '1181.1837801763', 2001);
INSERT INTO iteminfo_data VALUES (10011, 'price_logic_max_cat_1080_s', '2952.9594504407', 2001);
INSERT INTO iteminfo_data VALUES (10012, 'price_fraud_cat_1080_s', '739.23986261018', 2006);
INSERT INTO iteminfo_data VALUES (10013, 'price_logic_min_cat_1080_s', '1182.1837801763', 2006);
INSERT INTO iteminfo_data VALUES (10014, 'price_logic_max_cat_1080_s', '2953.9594504407', 2006);
INSERT INTO iteminfo_data VALUES (10015, 'price_fraud_cat_1020_s', '1154.3440838249', 2001);
INSERT INTO iteminfo_data VALUES (10016, 'price_logic_min_cat_1020_s', '1846.9505341198', 2001);
INSERT INTO iteminfo_data VALUES (10017, 'price_logic_max_cat_1020_s', '4617.3763352994', 2001);
INSERT INTO iteminfo_data VALUES (10018, 'price_fraud_cat_1020_s', '1155.3440838249', 2006);
INSERT INTO iteminfo_data VALUES (10019, 'price_logic_min_cat_1020_s', '1847.9505341198', 2006);
INSERT INTO iteminfo_data VALUES (10020, 'price_logic_max_cat_1020_s', '4618.3763352994', 2006);
INSERT INTO iteminfo_data VALUES (10021, 'price_fraud_cat_10_s', '3', 2001);
INSERT INTO iteminfo_data VALUES (10022, 'price_logic_min_cat_10_s', '4.8', 2001);
INSERT INTO iteminfo_data VALUES (10023, 'price_logic_max_cat_10_s', '12', 2001);
INSERT INTO iteminfo_data VALUES (10024, 'price_fraud_cat_10_s', '4', 2006);
INSERT INTO iteminfo_data VALUES (10025, 'price_logic_min_cat_10_s', '5.8', 2006);
INSERT INTO iteminfo_data VALUES (10026, 'price_logic_max_cat_10_s', '13', 2006);
INSERT INTO iteminfo_data VALUES (10027, 'price_fraud_cat_11_s', '1.5', 2001);
INSERT INTO iteminfo_data VALUES (10028, 'price_logic_min_cat_11_s', '2.4', 2001);
INSERT INTO iteminfo_data VALUES (10029, 'price_logic_max_cat_11_s', '6', 2001);
INSERT INTO iteminfo_data VALUES (10030, 'price_fraud_cat_11_s', '2.5', 2006);
INSERT INTO iteminfo_data VALUES (10031, 'price_logic_min_cat_11_s', '3.4', 2006);
INSERT INTO iteminfo_data VALUES (10032, 'price_logic_max_cat_11_s', '7', 2006);
INSERT INTO iteminfo_data VALUES (10033, 'price_fraud_cat_12_s', '1.8', 2001);
INSERT INTO iteminfo_data VALUES (10034, 'price_logic_min_cat_12_s', '2.88', 2001);
INSERT INTO iteminfo_data VALUES (10035, 'price_logic_max_cat_12_s', '7.2', 2001);
INSERT INTO iteminfo_data VALUES (10036, 'price_fraud_cat_12_s', '2.8', 2006);
INSERT INTO iteminfo_data VALUES (10037, 'price_logic_min_cat_12_s', '3.88', 2006);
INSERT INTO iteminfo_data VALUES (10038, 'price_logic_max_cat_12_s', '8.2', 2006);


--
-- Data for Name: mail_log; Type: TABLE DATA; Schema: public; Owner: dany
--



--
-- Data for Name: mail_queue; Type: TABLE DATA; Schema: public; Owner: dany
--



--
-- Data for Name: mama_attribute_wordlists; Type: TABLE DATA; Schema: public; Owner: dany
--



--
-- Data for Name: mama_attribute_categories; Type: TABLE DATA; Schema: public; Owner: dany
--



--
-- Data for Name: mama_main_backup; Type: TABLE DATA; Schema: public; Owner: dany
--



--
-- Data for Name: mama_attribute_categories_backup; Type: TABLE DATA; Schema: public; Owner: dany
--



--
-- Data for Name: mama_attribute_wordlists_backup; Type: TABLE DATA; Schema: public; Owner: dany
--



--
-- Data for Name: mama_attribute_words; Type: TABLE DATA; Schema: public; Owner: dany
--



--
-- Data for Name: mama_attribute_words_backup; Type: TABLE DATA; Schema: public; Owner: dany
--



--
-- Data for Name: mama_exception_lists; Type: TABLE DATA; Schema: public; Owner: dany
--



--
-- Data for Name: mama_exception_lists_backup; Type: TABLE DATA; Schema: public; Owner: dany
--



--
-- Data for Name: mama_exception_words; Type: TABLE DATA; Schema: public; Owner: dany
--



--
-- Data for Name: mama_exception_words_backup; Type: TABLE DATA; Schema: public; Owner: dany
--



--
-- Data for Name: mama_wordlists; Type: TABLE DATA; Schema: public; Owner: dany
--



--
-- Data for Name: mama_wordlists_backup; Type: TABLE DATA; Schema: public; Owner: dany
--



--
-- Data for Name: mama_words; Type: TABLE DATA; Schema: public; Owner: dany
--



--
-- Data for Name: mama_words_backup; Type: TABLE DATA; Schema: public; Owner: dany
--



--
-- Data for Name: most_popular_ads; Type: TABLE DATA; Schema: public; Owner: dany
--



--
-- Data for Name: notices; Type: TABLE DATA; Schema: public; Owner: dany
--



--
-- Data for Name: on_call; Type: TABLE DATA; Schema: public; Owner: dany
--



--
-- Data for Name: on_call_actions; Type: TABLE DATA; Schema: public; Owner: dany
--



--
-- Data for Name: pageviews_per_reg_cat; Type: TABLE DATA; Schema: public; Owner: dany
--



--
-- Data for Name: pay_log; Type: TABLE DATA; Schema: public; Owner: dany
--

INSERT INTO pay_log VALUES (0, 'adminclear', 0, '59876', 99999917, '2006-04-06 15:19:46', 50, 'OK');
INSERT INTO pay_log VALUES (1, 'save', 0, '112200000', NULL, '2013-06-04 10:41:32', 60, 'SAVE');
INSERT INTO pay_log VALUES (2, 'verify', 0, '112200000', NULL, '2013-06-04 10:41:32', 60, 'OK');
INSERT INTO pay_log VALUES (3, 'save', 0, '112200001', NULL, '2013-06-04 10:45:37', 61, 'SAVE');
INSERT INTO pay_log VALUES (4, 'verify', 0, '112200001', NULL, '2013-06-04 10:45:37', 61, 'OK');
INSERT INTO pay_log VALUES (5, 'save', 0, '112200002', NULL, '2013-06-04 10:46:57', 62, 'SAVE');
INSERT INTO pay_log VALUES (6, 'verify', 0, '112200002', NULL, '2013-06-04 10:46:57', 62, 'OK');
INSERT INTO pay_log VALUES (7, 'save', 0, '112200003', NULL, '2013-06-04 10:48:27', 63, 'SAVE');
INSERT INTO pay_log VALUES (8, 'verify', 0, '112200003', NULL, '2013-06-04 10:48:28', 63, 'OK');
INSERT INTO pay_log VALUES (9, 'save', 0, '112200004', NULL, '2013-06-04 10:50:16', 64, 'SAVE');
INSERT INTO pay_log VALUES (10, 'verify', 0, '112200004', NULL, '2013-06-04 10:50:16', 64, 'OK');
INSERT INTO pay_log VALUES (11, 'save', 0, '112200005', NULL, '2013-06-04 10:51:34', 65, 'SAVE');
INSERT INTO pay_log VALUES (12, 'verify', 0, '112200005', NULL, '2013-06-04 10:51:34', 65, 'OK');
INSERT INTO pay_log VALUES (13, 'save', 0, '112200006', NULL, '2013-06-04 10:55:01', 66, 'SAVE');
INSERT INTO pay_log VALUES (14, 'verify', 0, '112200006', NULL, '2013-06-04 10:55:01', 66, 'OK');
INSERT INTO pay_log VALUES (15, 'save', 0, '112200007', NULL, '2013-06-04 10:58:10', 67, 'SAVE');
INSERT INTO pay_log VALUES (16, 'verify', 0, '112200007', NULL, '2013-06-04 10:58:10', 67, 'OK');
INSERT INTO pay_log VALUES (17, 'save', 0, '112200008', NULL, '2013-06-04 10:59:40', 68, 'SAVE');
INSERT INTO pay_log VALUES (18, 'verify', 0, '112200008', NULL, '2013-06-04 10:59:40', 68, 'OK');
INSERT INTO pay_log VALUES (19, 'save', 0, '112200009', NULL, '2013-06-04 11:01:07', 69, 'SAVE');
INSERT INTO pay_log VALUES (20, 'verify', 0, '112200009', NULL, '2013-06-04 11:01:07', 69, 'OK');
INSERT INTO pay_log VALUES (21, 'save', 0, '235392023', NULL, '2013-06-04 11:03:03', 70, 'SAVE');
INSERT INTO pay_log VALUES (22, 'verify', 0, '235392023', NULL, '2013-06-04 11:03:03', 70, 'OK');
INSERT INTO pay_log VALUES (23, 'save', 0, '370784046', NULL, '2013-06-04 11:20:26', 71, 'SAVE');
INSERT INTO pay_log VALUES (24, 'verify', 0, '370784046', NULL, '2013-06-04 11:20:26', 71, 'OK');
INSERT INTO pay_log VALUES (25, 'save', 0, '506176069', NULL, '2013-06-04 11:21:46', 72, 'SAVE');
INSERT INTO pay_log VALUES (26, 'verify', 0, '506176069', NULL, '2013-06-04 11:21:46', 72, 'OK');
INSERT INTO pay_log VALUES (27, 'save', 0, '641568092', NULL, '2013-06-04 11:23:41', 73, 'SAVE');
INSERT INTO pay_log VALUES (28, 'verify', 0, '641568092', NULL, '2013-06-04 11:23:41', 73, 'OK');
INSERT INTO pay_log VALUES (29, 'save', 0, '776960115', NULL, '2013-06-04 11:32:14', 74, 'SAVE');
INSERT INTO pay_log VALUES (30, 'verify', 0, '776960115', NULL, '2013-06-04 11:32:14', 74, 'OK');
INSERT INTO pay_log VALUES (31, 'save', 0, '912352138', NULL, '2013-06-04 11:34:45', 75, 'SAVE');
INSERT INTO pay_log VALUES (32, 'verify', 0, '912352138', NULL, '2013-06-04 11:34:45', 75, 'OK');
INSERT INTO pay_log VALUES (33, 'save', 0, '147744161', NULL, '2013-06-04 11:35:12', 76, 'SAVE');
INSERT INTO pay_log VALUES (34, 'verify', 0, '147744161', NULL, '2013-06-04 11:35:12', 76, 'OK');
INSERT INTO pay_log VALUES (35, 'save', 0, '283136184', NULL, '2013-06-04 11:36:19', 77, 'SAVE');
INSERT INTO pay_log VALUES (36, 'verify', 0, '283136184', NULL, '2013-06-04 11:36:19', 77, 'OK');
INSERT INTO pay_log VALUES (37, 'save', 0, '418528207', NULL, '2013-06-04 11:37:21', 78, 'SAVE');
INSERT INTO pay_log VALUES (38, 'verify', 0, '418528207', NULL, '2013-06-04 11:37:21', 78, 'OK');
INSERT INTO pay_log VALUES (39, 'save', 0, '553920230', NULL, '2013-06-04 11:39:03', 79, 'SAVE');
INSERT INTO pay_log VALUES (40, 'verify', 0, '553920230', NULL, '2013-06-04 11:39:03', 79, 'OK');
INSERT INTO pay_log VALUES (41, 'save', 0, '689312253', NULL, '2013-06-04 11:52:06', 80, 'SAVE');
INSERT INTO pay_log VALUES (42, 'verify', 0, '689312253', NULL, '2013-06-04 11:52:06', 80, 'OK');
INSERT INTO pay_log VALUES (43, 'save', 0, '824704276', NULL, '2013-06-04 11:53:09', 81, 'SAVE');
INSERT INTO pay_log VALUES (44, 'verify', 0, '824704276', NULL, '2013-06-04 11:53:09', 81, 'OK');
INSERT INTO pay_log VALUES (45, 'save', 0, '960096299', NULL, '2013-06-04 11:53:14', 82, 'SAVE');
INSERT INTO pay_log VALUES (46, 'verify', 0, '960096299', NULL, '2013-06-04 11:53:14', 82, 'OK');
INSERT INTO pay_log VALUES (47, 'save', 0, '195488322', NULL, '2013-06-04 11:53:55', 83, 'SAVE');
INSERT INTO pay_log VALUES (48, 'verify', 0, '195488322', NULL, '2013-06-04 11:53:55', 83, 'OK');
INSERT INTO pay_log VALUES (49, 'save', 0, '330880345', NULL, '2013-06-04 11:54:22', 84, 'SAVE');
INSERT INTO pay_log VALUES (50, 'verify', 0, '330880345', NULL, '2013-06-04 11:54:22', 84, 'OK');
INSERT INTO pay_log VALUES (51, 'save', 0, '466272368', NULL, '2013-06-04 11:54:53', 85, 'SAVE');
INSERT INTO pay_log VALUES (52, 'verify', 0, '466272368', NULL, '2013-06-04 11:54:53', 85, 'OK');
INSERT INTO pay_log VALUES (53, 'save', 0, '601664391', NULL, '2013-06-04 11:55:40', 86, 'SAVE');
INSERT INTO pay_log VALUES (54, 'verify', 0, '601664391', NULL, '2013-06-04 11:55:40', 86, 'OK');
INSERT INTO pay_log VALUES (55, 'save', 0, '737056414', NULL, '2013-06-04 11:56:00', 87, 'SAVE');
INSERT INTO pay_log VALUES (56, 'verify', 0, '737056414', NULL, '2013-06-04 11:56:00', 87, 'OK');
INSERT INTO pay_log VALUES (57, 'save', 0, '872448437', NULL, '2013-06-04 11:56:15', 88, 'SAVE');
INSERT INTO pay_log VALUES (58, 'verify', 0, '872448437', NULL, '2013-06-04 11:56:15', 88, 'OK');
INSERT INTO pay_log VALUES (59, 'save', 0, '107840460', NULL, '2013-06-04 11:56:40', 89, 'SAVE');
INSERT INTO pay_log VALUES (60, 'verify', 0, '107840460', NULL, '2013-06-04 11:56:40', 89, 'OK');
INSERT INTO pay_log VALUES (61, 'save', 0, '243232483', NULL, '2013-06-04 11:57:00', 90, 'SAVE');
INSERT INTO pay_log VALUES (62, 'verify', 0, '243232483', NULL, '2013-06-04 11:57:00', 90, 'OK');
INSERT INTO pay_log VALUES (63, 'save', 0, '378624506', NULL, '2013-06-04 11:57:12', 91, 'SAVE');
INSERT INTO pay_log VALUES (64, 'verify', 0, '378624506', NULL, '2013-06-04 11:57:12', 91, 'OK');
INSERT INTO pay_log VALUES (65, 'save', 0, '514016529', NULL, '2013-06-04 11:57:55', 92, 'SAVE');
INSERT INTO pay_log VALUES (66, 'verify', 0, '514016529', NULL, '2013-06-04 11:57:55', 92, 'OK');
INSERT INTO pay_log VALUES (67, 'save', 0, '649408552', NULL, '2013-06-04 11:58:19', 93, 'SAVE');
INSERT INTO pay_log VALUES (68, 'verify', 0, '649408552', NULL, '2013-06-04 11:58:19', 93, 'OK');
INSERT INTO pay_log VALUES (69, 'save', 0, '784800575', NULL, '2013-06-04 11:58:32', 94, 'SAVE');
INSERT INTO pay_log VALUES (70, 'verify', 0, '784800575', NULL, '2013-06-04 11:58:32', 94, 'OK');
INSERT INTO pay_log VALUES (71, 'save', 0, '920192598', NULL, '2013-06-04 11:58:56', 95, 'SAVE');
INSERT INTO pay_log VALUES (72, 'verify', 0, '920192598', NULL, '2013-06-04 11:58:56', 95, 'OK');
INSERT INTO pay_log VALUES (73, 'save', 0, '155584621', NULL, '2013-06-04 11:59:30', 96, 'SAVE');
INSERT INTO pay_log VALUES (74, 'verify', 0, '155584621', NULL, '2013-06-04 11:59:30', 96, 'OK');
INSERT INTO pay_log VALUES (75, 'save', 0, '290976644', NULL, '2013-06-04 12:00:14', 97, 'SAVE');
INSERT INTO pay_log VALUES (76, 'verify', 0, '290976644', NULL, '2013-06-04 12:00:14', 97, 'OK');
INSERT INTO pay_log VALUES (77, 'save', 0, '426368667', NULL, '2013-06-04 12:06:50', 98, 'SAVE');
INSERT INTO pay_log VALUES (78, 'verify', 0, '426368667', NULL, '2013-06-04 12:06:50', 98, 'OK');
INSERT INTO pay_log VALUES (79, 'save', 0, '561760690', NULL, '2013-06-04 12:06:52', 99, 'SAVE');
INSERT INTO pay_log VALUES (80, 'verify', 0, '561760690', NULL, '2013-06-04 12:06:52', 99, 'OK');
INSERT INTO pay_log VALUES (81, 'save', 0, '697152713', NULL, '2013-06-04 12:07:43', 100, 'SAVE');
INSERT INTO pay_log VALUES (82, 'verify', 0, '697152713', NULL, '2013-06-04 12:07:43', 100, 'OK');
INSERT INTO pay_log VALUES (83, 'save', 0, '832544736', NULL, '2013-06-04 12:10:02', 101, 'SAVE');
INSERT INTO pay_log VALUES (84, 'verify', 0, '832544736', NULL, '2013-06-04 12:10:02', 101, 'OK');
INSERT INTO pay_log VALUES (85, 'save', 0, '967936759', NULL, '2013-06-04 12:10:55', 102, 'SAVE');
INSERT INTO pay_log VALUES (86, 'verify', 0, '967936759', NULL, '2013-06-04 12:10:55', 102, 'OK');
INSERT INTO pay_log VALUES (87, 'save', 0, '203328782', NULL, '2013-06-04 12:13:22', 103, 'SAVE');
INSERT INTO pay_log VALUES (88, 'verify', 0, '203328782', NULL, '2013-06-04 12:13:22', 103, 'OK');
INSERT INTO pay_log VALUES (89, 'save', 0, '338720805', NULL, '2013-06-04 12:14:08', 104, 'SAVE');
INSERT INTO pay_log VALUES (90, 'verify', 0, '338720805', NULL, '2013-06-04 12:14:08', 104, 'OK');
INSERT INTO pay_log VALUES (91, 'save', 0, '474112828', NULL, '2013-06-04 12:14:57', 105, 'SAVE');
INSERT INTO pay_log VALUES (92, 'verify', 0, '474112828', NULL, '2013-06-04 12:14:57', 105, 'OK');
INSERT INTO pay_log VALUES (93, 'save', 0, '609504851', NULL, '2013-06-04 12:15:47', 106, 'SAVE');
INSERT INTO pay_log VALUES (94, 'verify', 0, '609504851', NULL, '2013-06-04 12:15:47', 106, 'OK');
INSERT INTO pay_log VALUES (95, 'save', 0, '744896874', NULL, '2013-06-04 12:18:25', 107, 'SAVE');
INSERT INTO pay_log VALUES (96, 'verify', 0, '744896874', NULL, '2013-06-04 12:18:25', 107, 'OK');
INSERT INTO pay_log VALUES (97, 'save', 0, '880288897', NULL, '2013-06-04 12:19:17', 108, 'SAVE');
INSERT INTO pay_log VALUES (98, 'verify', 0, '880288897', NULL, '2013-06-04 12:19:17', 108, 'OK');
INSERT INTO pay_log VALUES (99, 'save', 0, '115680920', NULL, '2013-06-04 12:20:12', 109, 'SAVE');
INSERT INTO pay_log VALUES (100, 'verify', 0, '115680920', NULL, '2013-06-04 12:20:12', 109, 'OK');
INSERT INTO pay_log VALUES (101, 'save', 0, '251072943', NULL, '2013-06-04 12:22:45', 110, 'SAVE');
INSERT INTO pay_log VALUES (102, 'verify', 0, '251072943', NULL, '2013-06-04 12:22:45', 110, 'OK');
INSERT INTO pay_log VALUES (103, 'save', 0, '386464966', NULL, '2013-06-04 12:23:33', 111, 'SAVE');
INSERT INTO pay_log VALUES (104, 'verify', 0, '386464966', NULL, '2013-06-04 12:23:33', 111, 'OK');
INSERT INTO pay_log VALUES (105, 'save', 0, '521856989', NULL, '2013-06-04 12:23:59', 112, 'SAVE');
INSERT INTO pay_log VALUES (106, 'verify', 0, '521856989', NULL, '2013-06-04 12:23:59', 112, 'OK');
INSERT INTO pay_log VALUES (107, 'save', 0, '657249012', NULL, '2013-06-04 12:24:25', 113, 'SAVE');
INSERT INTO pay_log VALUES (108, 'verify', 0, '657249012', NULL, '2013-06-04 12:24:25', 113, 'OK');
INSERT INTO pay_log VALUES (109, 'save', 0, '792641035', NULL, '2013-06-04 12:24:59', 114, 'SAVE');
INSERT INTO pay_log VALUES (110, 'verify', 0, '792641035', NULL, '2013-06-04 12:24:59', 114, 'OK');
INSERT INTO pay_log VALUES (111, 'save', 0, '928033058', NULL, '2013-06-04 12:25:28', 115, 'SAVE');
INSERT INTO pay_log VALUES (112, 'verify', 0, '928033058', NULL, '2013-06-04 12:25:28', 115, 'OK');
INSERT INTO pay_log VALUES (113, 'save', 0, '163425081', NULL, '2013-06-04 12:26:02', 116, 'SAVE');
INSERT INTO pay_log VALUES (114, 'verify', 0, '163425081', NULL, '2013-06-04 12:26:02', 116, 'OK');
INSERT INTO pay_log VALUES (115, 'save', 0, '298817104', NULL, '2013-06-04 12:28:19', 117, 'SAVE');
INSERT INTO pay_log VALUES (116, 'verify', 0, '298817104', NULL, '2013-06-04 12:28:19', 117, 'OK');
INSERT INTO pay_log VALUES (117, 'save', 0, '434209127', NULL, '2013-06-04 12:28:42', 118, 'SAVE');
INSERT INTO pay_log VALUES (118, 'verify', 0, '434209127', NULL, '2013-06-04 12:28:42', 118, 'OK');
INSERT INTO pay_log VALUES (119, 'save', 0, '569601150', NULL, '2013-06-04 12:30:25', 119, 'SAVE');
INSERT INTO pay_log VALUES (120, 'verify', 0, '569601150', NULL, '2013-06-04 12:30:25', 119, 'OK');
INSERT INTO pay_log VALUES (121, 'save', 0, '704993173', NULL, '2013-06-04 12:30:57', 120, 'SAVE');
INSERT INTO pay_log VALUES (122, 'verify', 0, '704993173', NULL, '2013-06-04 12:30:58', 120, 'OK');
INSERT INTO pay_log VALUES (123, 'save', 0, '840385196', NULL, '2013-06-04 12:31:34', 121, 'SAVE');
INSERT INTO pay_log VALUES (124, 'verify', 0, '840385196', NULL, '2013-06-04 12:31:34', 121, 'OK');
INSERT INTO pay_log VALUES (125, 'save', 0, '975777219', NULL, '2013-06-04 12:31:59', 122, 'SAVE');
INSERT INTO pay_log VALUES (126, 'verify', 0, '975777219', NULL, '2013-06-04 12:31:59', 122, 'OK');
INSERT INTO pay_log VALUES (127, 'save', 0, '211169242', NULL, '2013-08-06 15:18:24', 123, 'SAVE');
INSERT INTO pay_log VALUES (128, 'verify', 0, '211169242', NULL, '2013-08-06 15:18:24', 123, 'OK');
INSERT INTO pay_log VALUES (129, 'save', 0, '346561265', NULL, '2013-08-06 15:19:06', 124, 'SAVE');
INSERT INTO pay_log VALUES (130, 'verify', 0, '346561265', NULL, '2013-08-06 15:19:06', 124, 'OK');
INSERT INTO pay_log VALUES (131, 'save', 0, '481953288', NULL, '2013-08-06 15:35:33', 125, 'SAVE');
INSERT INTO pay_log VALUES (132, 'verify', 0, '481953288', NULL, '2013-08-06 15:35:33', 125, 'OK');
INSERT INTO pay_log VALUES (133, 'save', 0, '617345311', NULL, '2013-08-06 15:36:33', 126, 'SAVE');
INSERT INTO pay_log VALUES (134, 'verify', 0, '617345311', NULL, '2013-08-06 15:36:33', 126, 'OK');
INSERT INTO pay_log VALUES (135, 'save', 0, '752737334', NULL, '2013-08-06 15:36:39', 127, 'SAVE');
INSERT INTO pay_log VALUES (136, 'verify', 0, '752737334', NULL, '2013-08-06 15:36:39', 127, 'OK');
INSERT INTO pay_log VALUES (137, 'save', 0, '888129357', NULL, '2013-08-06 15:37:25', 128, 'SAVE');
INSERT INTO pay_log VALUES (138, 'verify', 0, '888129357', NULL, '2013-08-06 15:37:25', 128, 'OK');
INSERT INTO pay_log VALUES (139, 'save', 0, '123521380', NULL, '2013-08-06 15:37:35', 129, 'SAVE');
INSERT INTO pay_log VALUES (140, 'verify', 0, '123521380', NULL, '2013-08-06 15:37:35', 129, 'OK');
INSERT INTO pay_log VALUES (141, 'save', 0, '258913403', NULL, '2013-08-06 15:38:19', 130, 'SAVE');
INSERT INTO pay_log VALUES (142, 'verify', 0, '258913403', NULL, '2013-08-06 15:38:19', 130, 'OK');
INSERT INTO pay_log VALUES (143, 'save', 0, '394305426', NULL, '2013-08-06 15:38:40', 131, 'SAVE');
INSERT INTO pay_log VALUES (144, 'verify', 0, '394305426', NULL, '2013-08-06 15:38:40', 131, 'OK');
INSERT INTO pay_log VALUES (145, 'save', 0, '529697449', NULL, '2013-08-06 15:39:10', 132, 'SAVE');
INSERT INTO pay_log VALUES (146, 'verify', 0, '529697449', NULL, '2013-08-06 15:39:10', 132, 'OK');
INSERT INTO pay_log VALUES (147, 'save', 0, '665089472', NULL, '2013-08-06 15:39:22', 133, 'SAVE');
INSERT INTO pay_log VALUES (148, 'verify', 0, '665089472', NULL, '2013-08-06 15:39:22', 133, 'OK');


--
-- Data for Name: pay_log_references; Type: TABLE DATA; Schema: public; Owner: dany
--

INSERT INTO pay_log_references VALUES (0, 0, 'remote_addr', '192.168.4.75');
INSERT INTO pay_log_references VALUES (1, 0, 'adphone', '123123');
INSERT INTO pay_log_references VALUES (1, 1, 'remote_addr', '10.0.1.173');
INSERT INTO pay_log_references VALUES (2, 0, 'remote_addr', '10.0.1.173');
INSERT INTO pay_log_references VALUES (3, 0, 'adphone', '123123');
INSERT INTO pay_log_references VALUES (3, 1, 'remote_addr', '10.0.1.173');
INSERT INTO pay_log_references VALUES (4, 0, 'remote_addr', '10.0.1.173');
INSERT INTO pay_log_references VALUES (5, 0, 'adphone', '123123');
INSERT INTO pay_log_references VALUES (5, 1, 'remote_addr', '10.0.1.173');
INSERT INTO pay_log_references VALUES (6, 0, 'remote_addr', '10.0.1.173');
INSERT INTO pay_log_references VALUES (7, 0, 'adphone', '123123');
INSERT INTO pay_log_references VALUES (7, 1, 'remote_addr', '10.0.1.173');
INSERT INTO pay_log_references VALUES (8, 0, 'remote_addr', '10.0.1.173');
INSERT INTO pay_log_references VALUES (9, 0, 'adphone', '123123');
INSERT INTO pay_log_references VALUES (9, 1, 'remote_addr', '10.0.1.173');
INSERT INTO pay_log_references VALUES (10, 0, 'remote_addr', '10.0.1.173');
INSERT INTO pay_log_references VALUES (11, 0, 'adphone', '123123');
INSERT INTO pay_log_references VALUES (11, 1, 'remote_addr', '10.0.1.173');
INSERT INTO pay_log_references VALUES (12, 0, 'remote_addr', '10.0.1.173');
INSERT INTO pay_log_references VALUES (13, 0, 'adphone', '123123');
INSERT INTO pay_log_references VALUES (13, 1, 'remote_addr', '10.0.1.173');
INSERT INTO pay_log_references VALUES (14, 0, 'remote_addr', '10.0.1.173');
INSERT INTO pay_log_references VALUES (15, 0, 'adphone', '457764874');
INSERT INTO pay_log_references VALUES (15, 1, 'remote_addr', '10.0.1.173');
INSERT INTO pay_log_references VALUES (16, 0, 'remote_addr', '10.0.1.173');
INSERT INTO pay_log_references VALUES (17, 0, 'adphone', '457764874');
INSERT INTO pay_log_references VALUES (17, 1, 'remote_addr', '10.0.1.173');
INSERT INTO pay_log_references VALUES (18, 0, 'remote_addr', '10.0.1.173');
INSERT INTO pay_log_references VALUES (19, 0, 'adphone', '457764874');
INSERT INTO pay_log_references VALUES (19, 1, 'remote_addr', '10.0.1.173');
INSERT INTO pay_log_references VALUES (20, 0, 'remote_addr', '10.0.1.173');
INSERT INTO pay_log_references VALUES (21, 0, 'adphone', '457764874');
INSERT INTO pay_log_references VALUES (21, 1, 'remote_addr', '10.0.1.173');
INSERT INTO pay_log_references VALUES (22, 0, 'remote_addr', '10.0.1.173');
INSERT INTO pay_log_references VALUES (23, 0, 'adphone', '457764874');
INSERT INTO pay_log_references VALUES (23, 1, 'remote_addr', '10.0.1.138');
INSERT INTO pay_log_references VALUES (24, 0, 'remote_addr', '10.0.1.138');
INSERT INTO pay_log_references VALUES (25, 0, 'adphone', '457764874');
INSERT INTO pay_log_references VALUES (25, 1, 'remote_addr', '10.0.1.138');
INSERT INTO pay_log_references VALUES (26, 0, 'remote_addr', '10.0.1.138');
INSERT INTO pay_log_references VALUES (27, 0, 'adphone', '457764874');
INSERT INTO pay_log_references VALUES (27, 1, 'remote_addr', '10.0.1.138');
INSERT INTO pay_log_references VALUES (28, 0, 'remote_addr', '10.0.1.138');
INSERT INTO pay_log_references VALUES (29, 0, 'adphone', '457764874');
INSERT INTO pay_log_references VALUES (29, 1, 'remote_addr', '10.0.1.138');
INSERT INTO pay_log_references VALUES (30, 0, 'remote_addr', '10.0.1.138');
INSERT INTO pay_log_references VALUES (31, 0, 'adphone', '457764874');
INSERT INTO pay_log_references VALUES (31, 1, 'remote_addr', '10.0.1.173');
INSERT INTO pay_log_references VALUES (32, 0, 'remote_addr', '10.0.1.173');
INSERT INTO pay_log_references VALUES (33, 0, 'adphone', '457764874');
INSERT INTO pay_log_references VALUES (33, 1, 'remote_addr', '10.0.1.138');
INSERT INTO pay_log_references VALUES (34, 0, 'remote_addr', '10.0.1.138');
INSERT INTO pay_log_references VALUES (35, 0, 'adphone', '457764874');
INSERT INTO pay_log_references VALUES (35, 1, 'remote_addr', '10.0.1.138');
INSERT INTO pay_log_references VALUES (36, 0, 'remote_addr', '10.0.1.138');
INSERT INTO pay_log_references VALUES (37, 0, 'adphone', '457764874');
INSERT INTO pay_log_references VALUES (37, 1, 'remote_addr', '10.0.1.173');
INSERT INTO pay_log_references VALUES (38, 0, 'remote_addr', '10.0.1.173');
INSERT INTO pay_log_references VALUES (39, 0, 'adphone', '457764874');
INSERT INTO pay_log_references VALUES (39, 1, 'remote_addr', '10.0.1.173');
INSERT INTO pay_log_references VALUES (40, 0, 'remote_addr', '10.0.1.173');
INSERT INTO pay_log_references VALUES (41, 0, 'adphone', '457764874');
INSERT INTO pay_log_references VALUES (41, 1, 'remote_addr', '10.0.1.173');
INSERT INTO pay_log_references VALUES (42, 0, 'remote_addr', '10.0.1.173');
INSERT INTO pay_log_references VALUES (43, 0, 'adphone', '457764874');
INSERT INTO pay_log_references VALUES (43, 1, 'remote_addr', '10.0.1.173');
INSERT INTO pay_log_references VALUES (44, 0, 'remote_addr', '10.0.1.173');
INSERT INTO pay_log_references VALUES (45, 0, 'adphone', '457764874');
INSERT INTO pay_log_references VALUES (45, 1, 'remote_addr', '10.0.1.138');
INSERT INTO pay_log_references VALUES (46, 0, 'remote_addr', '10.0.1.138');
INSERT INTO pay_log_references VALUES (47, 0, 'adphone', '457764874');
INSERT INTO pay_log_references VALUES (47, 1, 'remote_addr', '10.0.1.138');
INSERT INTO pay_log_references VALUES (48, 0, 'remote_addr', '10.0.1.138');
INSERT INTO pay_log_references VALUES (49, 0, 'adphone', '457764874');
INSERT INTO pay_log_references VALUES (49, 1, 'remote_addr', '10.0.1.173');
INSERT INTO pay_log_references VALUES (50, 0, 'remote_addr', '10.0.1.173');
INSERT INTO pay_log_references VALUES (51, 0, 'adphone', '457764874');
INSERT INTO pay_log_references VALUES (51, 1, 'remote_addr', '10.0.1.138');
INSERT INTO pay_log_references VALUES (52, 0, 'remote_addr', '10.0.1.138');
INSERT INTO pay_log_references VALUES (53, 0, 'adphone', '457764874');
INSERT INTO pay_log_references VALUES (53, 1, 'remote_addr', '10.0.1.173');
INSERT INTO pay_log_references VALUES (54, 0, 'remote_addr', '10.0.1.173');
INSERT INTO pay_log_references VALUES (55, 0, 'adphone', '457764874');
INSERT INTO pay_log_references VALUES (55, 1, 'remote_addr', '10.0.1.138');
INSERT INTO pay_log_references VALUES (56, 0, 'remote_addr', '10.0.1.138');
INSERT INTO pay_log_references VALUES (57, 0, 'adphone', '457764874');
INSERT INTO pay_log_references VALUES (57, 1, 'remote_addr', '10.0.1.173');
INSERT INTO pay_log_references VALUES (58, 0, 'remote_addr', '10.0.1.173');
INSERT INTO pay_log_references VALUES (59, 0, 'adphone', '457764874');
INSERT INTO pay_log_references VALUES (59, 1, 'remote_addr', '10.0.1.138');
INSERT INTO pay_log_references VALUES (60, 0, 'remote_addr', '10.0.1.138');
INSERT INTO pay_log_references VALUES (61, 0, 'adphone', '457764874');
INSERT INTO pay_log_references VALUES (61, 1, 'remote_addr', '10.0.1.173');
INSERT INTO pay_log_references VALUES (62, 0, 'remote_addr', '10.0.1.173');
INSERT INTO pay_log_references VALUES (63, 0, 'adphone', '457764874');
INSERT INTO pay_log_references VALUES (63, 1, 'remote_addr', '10.0.1.138');
INSERT INTO pay_log_references VALUES (64, 0, 'remote_addr', '10.0.1.138');
INSERT INTO pay_log_references VALUES (65, 0, 'adphone', '457764874');
INSERT INTO pay_log_references VALUES (65, 1, 'remote_addr', '10.0.1.173');
INSERT INTO pay_log_references VALUES (66, 0, 'remote_addr', '10.0.1.173');
INSERT INTO pay_log_references VALUES (67, 0, 'adphone', '457764874');
INSERT INTO pay_log_references VALUES (67, 1, 'remote_addr', '10.0.1.138');
INSERT INTO pay_log_references VALUES (68, 0, 'remote_addr', '10.0.1.138');
INSERT INTO pay_log_references VALUES (69, 0, 'adphone', '457764874');
INSERT INTO pay_log_references VALUES (69, 1, 'remote_addr', '10.0.1.173');
INSERT INTO pay_log_references VALUES (70, 0, 'remote_addr', '10.0.1.173');
INSERT INTO pay_log_references VALUES (71, 0, 'adphone', '457764874');
INSERT INTO pay_log_references VALUES (71, 1, 'remote_addr', '10.0.1.138');
INSERT INTO pay_log_references VALUES (72, 0, 'remote_addr', '10.0.1.138');
INSERT INTO pay_log_references VALUES (73, 0, 'adphone', '457764874');
INSERT INTO pay_log_references VALUES (73, 1, 'remote_addr', '10.0.1.138');
INSERT INTO pay_log_references VALUES (74, 0, 'remote_addr', '10.0.1.138');
INSERT INTO pay_log_references VALUES (75, 0, 'adphone', '457764874');
INSERT INTO pay_log_references VALUES (75, 1, 'remote_addr', '10.0.1.138');
INSERT INTO pay_log_references VALUES (76, 0, 'remote_addr', '10.0.1.138');
INSERT INTO pay_log_references VALUES (77, 0, 'adphone', '457764874');
INSERT INTO pay_log_references VALUES (77, 1, 'remote_addr', '10.0.1.138');
INSERT INTO pay_log_references VALUES (78, 0, 'remote_addr', '10.0.1.138');
INSERT INTO pay_log_references VALUES (79, 0, 'adphone', '457764874');
INSERT INTO pay_log_references VALUES (79, 1, 'remote_addr', '10.0.1.173');
INSERT INTO pay_log_references VALUES (80, 0, 'remote_addr', '10.0.1.173');
INSERT INTO pay_log_references VALUES (81, 0, 'adphone', '457764874');
INSERT INTO pay_log_references VALUES (81, 1, 'remote_addr', '10.0.1.173');
INSERT INTO pay_log_references VALUES (82, 0, 'remote_addr', '10.0.1.173');
INSERT INTO pay_log_references VALUES (83, 0, 'adphone', '457764874');
INSERT INTO pay_log_references VALUES (83, 1, 'remote_addr', '10.0.1.138');
INSERT INTO pay_log_references VALUES (84, 0, 'remote_addr', '10.0.1.138');
INSERT INTO pay_log_references VALUES (85, 0, 'adphone', '457764874');
INSERT INTO pay_log_references VALUES (85, 1, 'remote_addr', '10.0.1.138');
INSERT INTO pay_log_references VALUES (86, 0, 'remote_addr', '10.0.1.138');
INSERT INTO pay_log_references VALUES (87, 0, 'adphone', '457764874');
INSERT INTO pay_log_references VALUES (87, 1, 'remote_addr', '10.0.1.138');
INSERT INTO pay_log_references VALUES (88, 0, 'remote_addr', '10.0.1.138');
INSERT INTO pay_log_references VALUES (89, 0, 'adphone', '457764874');
INSERT INTO pay_log_references VALUES (89, 1, 'remote_addr', '10.0.1.138');
INSERT INTO pay_log_references VALUES (90, 0, 'remote_addr', '10.0.1.138');
INSERT INTO pay_log_references VALUES (91, 0, 'adphone', '457764874');
INSERT INTO pay_log_references VALUES (91, 1, 'remote_addr', '10.0.1.138');
INSERT INTO pay_log_references VALUES (92, 0, 'remote_addr', '10.0.1.138');
INSERT INTO pay_log_references VALUES (93, 0, 'adphone', '457764874');
INSERT INTO pay_log_references VALUES (93, 1, 'remote_addr', '10.0.1.138');
INSERT INTO pay_log_references VALUES (94, 0, 'remote_addr', '10.0.1.138');
INSERT INTO pay_log_references VALUES (95, 0, 'adphone', '457764874');
INSERT INTO pay_log_references VALUES (95, 1, 'remote_addr', '10.0.1.138');
INSERT INTO pay_log_references VALUES (96, 0, 'remote_addr', '10.0.1.138');
INSERT INTO pay_log_references VALUES (97, 0, 'adphone', '457764874');
INSERT INTO pay_log_references VALUES (97, 1, 'remote_addr', '10.0.1.138');
INSERT INTO pay_log_references VALUES (98, 0, 'remote_addr', '10.0.1.138');
INSERT INTO pay_log_references VALUES (99, 0, 'adphone', '457764874');
INSERT INTO pay_log_references VALUES (99, 1, 'remote_addr', '10.0.1.138');
INSERT INTO pay_log_references VALUES (100, 0, 'remote_addr', '10.0.1.138');
INSERT INTO pay_log_references VALUES (101, 0, 'adphone', '457764874');
INSERT INTO pay_log_references VALUES (101, 1, 'remote_addr', '10.0.1.138');
INSERT INTO pay_log_references VALUES (102, 0, 'remote_addr', '10.0.1.138');
INSERT INTO pay_log_references VALUES (103, 0, 'adphone', '457764874');
INSERT INTO pay_log_references VALUES (103, 1, 'remote_addr', '10.0.1.138');
INSERT INTO pay_log_references VALUES (104, 0, 'remote_addr', '10.0.1.138');
INSERT INTO pay_log_references VALUES (105, 0, 'adphone', '457764874');
INSERT INTO pay_log_references VALUES (105, 1, 'remote_addr', '10.0.1.138');
INSERT INTO pay_log_references VALUES (106, 0, 'remote_addr', '10.0.1.138');
INSERT INTO pay_log_references VALUES (107, 0, 'adphone', '457764874');
INSERT INTO pay_log_references VALUES (107, 1, 'remote_addr', '10.0.1.138');
INSERT INTO pay_log_references VALUES (108, 0, 'remote_addr', '10.0.1.138');
INSERT INTO pay_log_references VALUES (109, 0, 'adphone', '457764874');
INSERT INTO pay_log_references VALUES (109, 1, 'remote_addr', '10.0.1.138');
INSERT INTO pay_log_references VALUES (110, 0, 'remote_addr', '10.0.1.138');
INSERT INTO pay_log_references VALUES (111, 0, 'adphone', '457764874');
INSERT INTO pay_log_references VALUES (111, 1, 'remote_addr', '10.0.1.138');
INSERT INTO pay_log_references VALUES (112, 0, 'remote_addr', '10.0.1.138');
INSERT INTO pay_log_references VALUES (113, 0, 'adphone', '457764874');
INSERT INTO pay_log_references VALUES (113, 1, 'remote_addr', '10.0.1.138');
INSERT INTO pay_log_references VALUES (114, 0, 'remote_addr', '10.0.1.138');
INSERT INTO pay_log_references VALUES (115, 0, 'adphone', '457764874');
INSERT INTO pay_log_references VALUES (115, 1, 'remote_addr', '10.0.1.138');
INSERT INTO pay_log_references VALUES (116, 0, 'remote_addr', '10.0.1.138');
INSERT INTO pay_log_references VALUES (117, 0, 'adphone', '457764874');
INSERT INTO pay_log_references VALUES (117, 1, 'remote_addr', '10.0.1.138');
INSERT INTO pay_log_references VALUES (118, 0, 'remote_addr', '10.0.1.138');
INSERT INTO pay_log_references VALUES (119, 0, 'adphone', '457764874');
INSERT INTO pay_log_references VALUES (119, 1, 'remote_addr', '10.0.1.138');
INSERT INTO pay_log_references VALUES (120, 0, 'remote_addr', '10.0.1.138');
INSERT INTO pay_log_references VALUES (121, 0, 'adphone', '457764874');
INSERT INTO pay_log_references VALUES (121, 1, 'remote_addr', '10.0.1.138');
INSERT INTO pay_log_references VALUES (122, 0, 'remote_addr', '10.0.1.138');
INSERT INTO pay_log_references VALUES (123, 0, 'adphone', '457764874');
INSERT INTO pay_log_references VALUES (123, 1, 'remote_addr', '10.0.1.138');
INSERT INTO pay_log_references VALUES (124, 0, 'remote_addr', '10.0.1.138');
INSERT INTO pay_log_references VALUES (125, 0, 'adphone', '457764874');
INSERT INTO pay_log_references VALUES (125, 1, 'remote_addr', '10.0.1.138');
INSERT INTO pay_log_references VALUES (126, 0, 'remote_addr', '10.0.1.138');
INSERT INTO pay_log_references VALUES (127, 0, 'adphone', '123123123');
INSERT INTO pay_log_references VALUES (127, 1, 'remote_addr', '10.0.1.175');
INSERT INTO pay_log_references VALUES (128, 0, 'remote_addr', '10.0.1.175');
INSERT INTO pay_log_references VALUES (129, 0, 'adphone', '123123123');
INSERT INTO pay_log_references VALUES (129, 1, 'remote_addr', '10.0.1.175');
INSERT INTO pay_log_references VALUES (130, 0, 'remote_addr', '10.0.1.175');
INSERT INTO pay_log_references VALUES (131, 0, 'adphone', '123123');
INSERT INTO pay_log_references VALUES (131, 1, 'remote_addr', '10.0.1.175');
INSERT INTO pay_log_references VALUES (132, 0, 'remote_addr', '10.0.1.175');
INSERT INTO pay_log_references VALUES (133, 0, 'adphone', '123123');
INSERT INTO pay_log_references VALUES (133, 1, 'remote_addr', '10.0.1.175');
INSERT INTO pay_log_references VALUES (134, 0, 'remote_addr', '10.0.1.175');
INSERT INTO pay_log_references VALUES (135, 0, 'adphone', '12345678');
INSERT INTO pay_log_references VALUES (135, 1, 'remote_addr', '10.0.1.17');
INSERT INTO pay_log_references VALUES (136, 0, 'remote_addr', '10.0.1.17');
INSERT INTO pay_log_references VALUES (137, 0, 'adphone', '123123');
INSERT INTO pay_log_references VALUES (137, 1, 'remote_addr', '10.0.1.175');
INSERT INTO pay_log_references VALUES (138, 0, 'remote_addr', '10.0.1.175');
INSERT INTO pay_log_references VALUES (139, 0, 'adphone', '12345678');
INSERT INTO pay_log_references VALUES (139, 1, 'remote_addr', '10.0.1.17');
INSERT INTO pay_log_references VALUES (140, 0, 'remote_addr', '10.0.1.17');
INSERT INTO pay_log_references VALUES (141, 0, 'adphone', '123123');
INSERT INTO pay_log_references VALUES (141, 1, 'remote_addr', '10.0.1.175');
INSERT INTO pay_log_references VALUES (142, 0, 'remote_addr', '10.0.1.175');
INSERT INTO pay_log_references VALUES (143, 0, 'adphone', '12345678');
INSERT INTO pay_log_references VALUES (143, 1, 'remote_addr', '10.0.1.17');
INSERT INTO pay_log_references VALUES (144, 0, 'remote_addr', '10.0.1.17');
INSERT INTO pay_log_references VALUES (145, 0, 'adphone', '123123');
INSERT INTO pay_log_references VALUES (145, 1, 'remote_addr', '10.0.1.175');
INSERT INTO pay_log_references VALUES (146, 0, 'remote_addr', '10.0.1.175');
INSERT INTO pay_log_references VALUES (147, 0, 'adphone', '12345678');
INSERT INTO pay_log_references VALUES (147, 1, 'remote_addr', '10.0.1.17');
INSERT INTO pay_log_references VALUES (148, 0, 'remote_addr', '10.0.1.17');


--
-- Data for Name: payments; Type: TABLE DATA; Schema: public; Owner: dany
--

INSERT INTO payments VALUES (0, 'ad_action', 20, 0);
INSERT INTO payments VALUES (1, 'ad_action', 20, 0);
INSERT INTO payments VALUES (2, 'ad_action', 20, 0);
INSERT INTO payments VALUES (3, 'ad_action', 20, 0);
INSERT INTO payments VALUES (50, 'ad_action', 20, 0);
INSERT INTO payments VALUES (60, 'ad_action', 0, 0);
INSERT INTO payments VALUES (61, 'ad_action', 0, 0);
INSERT INTO payments VALUES (62, 'ad_action', 0, 0);
INSERT INTO payments VALUES (63, 'ad_action', 0, 0);
INSERT INTO payments VALUES (64, 'ad_action', 0, 0);
INSERT INTO payments VALUES (65, 'ad_action', 0, 0);
INSERT INTO payments VALUES (66, 'ad_action', 0, 0);
INSERT INTO payments VALUES (67, 'ad_action', 0, 0);
INSERT INTO payments VALUES (68, 'ad_action', 0, 0);
INSERT INTO payments VALUES (69, 'ad_action', 0, 0);
INSERT INTO payments VALUES (70, 'ad_action', 0, 0);
INSERT INTO payments VALUES (71, 'ad_action', 0, 0);
INSERT INTO payments VALUES (72, 'ad_action', 0, 0);
INSERT INTO payments VALUES (73, 'ad_action', 0, 0);
INSERT INTO payments VALUES (74, 'ad_action', 0, 0);
INSERT INTO payments VALUES (75, 'ad_action', 0, 0);
INSERT INTO payments VALUES (76, 'ad_action', 0, 0);
INSERT INTO payments VALUES (77, 'ad_action', 0, 0);
INSERT INTO payments VALUES (78, 'ad_action', 0, 0);
INSERT INTO payments VALUES (79, 'ad_action', 0, 0);
INSERT INTO payments VALUES (80, 'ad_action', 0, 0);
INSERT INTO payments VALUES (81, 'ad_action', 0, 0);
INSERT INTO payments VALUES (82, 'ad_action', 0, 0);
INSERT INTO payments VALUES (83, 'ad_action', 0, 0);
INSERT INTO payments VALUES (84, 'ad_action', 0, 0);
INSERT INTO payments VALUES (85, 'ad_action', 0, 0);
INSERT INTO payments VALUES (86, 'ad_action', 0, 0);
INSERT INTO payments VALUES (87, 'ad_action', 0, 0);
INSERT INTO payments VALUES (88, 'ad_action', 0, 0);
INSERT INTO payments VALUES (89, 'ad_action', 0, 0);
INSERT INTO payments VALUES (90, 'ad_action', 0, 0);
INSERT INTO payments VALUES (91, 'ad_action', 0, 0);
INSERT INTO payments VALUES (92, 'ad_action', 0, 0);
INSERT INTO payments VALUES (93, 'ad_action', 0, 0);
INSERT INTO payments VALUES (94, 'ad_action', 0, 0);
INSERT INTO payments VALUES (95, 'ad_action', 0, 0);
INSERT INTO payments VALUES (96, 'ad_action', 0, 0);
INSERT INTO payments VALUES (97, 'ad_action', 0, 0);
INSERT INTO payments VALUES (98, 'ad_action', 0, 0);
INSERT INTO payments VALUES (99, 'ad_action', 0, 0);
INSERT INTO payments VALUES (100, 'ad_action', 0, 0);
INSERT INTO payments VALUES (101, 'ad_action', 0, 0);
INSERT INTO payments VALUES (102, 'ad_action', 0, 0);
INSERT INTO payments VALUES (103, 'ad_action', 0, 0);
INSERT INTO payments VALUES (104, 'ad_action', 0, 0);
INSERT INTO payments VALUES (105, 'ad_action', 0, 0);
INSERT INTO payments VALUES (106, 'ad_action', 0, 0);
INSERT INTO payments VALUES (107, 'ad_action', 0, 0);
INSERT INTO payments VALUES (108, 'ad_action', 0, 0);
INSERT INTO payments VALUES (109, 'ad_action', 0, 0);
INSERT INTO payments VALUES (110, 'ad_action', 0, 0);
INSERT INTO payments VALUES (111, 'ad_action', 0, 0);
INSERT INTO payments VALUES (112, 'ad_action', 0, 0);
INSERT INTO payments VALUES (113, 'ad_action', 0, 0);
INSERT INTO payments VALUES (114, 'ad_action', 0, 0);
INSERT INTO payments VALUES (115, 'ad_action', 0, 0);
INSERT INTO payments VALUES (116, 'ad_action', 0, 0);
INSERT INTO payments VALUES (117, 'ad_action', 0, 0);
INSERT INTO payments VALUES (118, 'ad_action', 0, 0);
INSERT INTO payments VALUES (119, 'ad_action', 0, 0);
INSERT INTO payments VALUES (120, 'ad_action', 0, 0);
INSERT INTO payments VALUES (121, 'ad_action', 0, 0);
INSERT INTO payments VALUES (122, 'ad_action', 0, 0);
INSERT INTO payments VALUES (123, 'ad_action', 0, 0);
INSERT INTO payments VALUES (124, 'ad_action', 0, 0);
INSERT INTO payments VALUES (125, 'ad_action', 0, 0);
INSERT INTO payments VALUES (126, 'ad_action', 0, 0);
INSERT INTO payments VALUES (127, 'ad_action', 0, 0);
INSERT INTO payments VALUES (128, 'ad_action', 0, 0);
INSERT INTO payments VALUES (129, 'ad_action', 0, 0);
INSERT INTO payments VALUES (130, 'ad_action', 0, 0);
INSERT INTO payments VALUES (131, 'ad_action', 0, 0);
INSERT INTO payments VALUES (132, 'ad_action', 0, 0);
INSERT INTO payments VALUES (133, 'ad_action', 0, 0);


--
-- Data for Name: pricelist; Type: TABLE DATA; Schema: public; Owner: dany
--

INSERT INTO pricelist VALUES ('if', 'BL1', 1295);
INSERT INTO pricelist VALUES ('if', 'BL2', 1295);
INSERT INTO pricelist VALUES ('if', 'BL3', 995);
INSERT INTO pricelist VALUES ('if', 'BL4', 995);


--
-- Data for Name: purchase; Type: TABLE DATA; Schema: public; Owner: dany
--



--
-- Data for Name: purchase_detail; Type: TABLE DATA; Schema: public; Owner: dany
--



--
-- Data for Name: purchase_states; Type: TABLE DATA; Schema: public; Owner: dany
--



--
-- Data for Name: redir_stats; Type: TABLE DATA; Schema: public; Owner: dany
--

INSERT INTO redir_stats VALUES (1, 'unknown@unknowntime', '2013-06-04 10:40:00', 0, 0, 0, 0, 0, 0, 0, 1);
INSERT INTO redir_stats VALUES (2, 'unknown@unknowntime', '2013-06-04 10:41:00', 0, 0, 0, 0, 0, 0, 0, 3);
INSERT INTO redir_stats VALUES (3, 'unknown@unknowntime', '2013-06-04 10:43:00', 0, 0, 0, 0, 0, 0, 0, 2);
INSERT INTO redir_stats VALUES (4, 'unknown@unknowntime', '2013-06-04 10:44:00', 0, 0, 0, 0, 0, 0, 0, 2);
INSERT INTO redir_stats VALUES (5, 'unknown@unknowntime', '2013-06-04 10:45:00', 0, 0, 0, 0, 0, 0, 0, 1);
INSERT INTO redir_stats VALUES (6, 'unknown@unknowntime', '2013-06-04 10:46:00', 0, 0, 0, 0, 0, 0, 0, 2);
INSERT INTO redir_stats VALUES (7, 'unknown@unknowntime', '2013-06-04 10:48:00', 0, 0, 0, 0, 0, 0, 0, 2);
INSERT INTO redir_stats VALUES (8, 'unknown@unknowntime', '2013-06-04 10:50:00', 0, 0, 0, 0, 0, 0, 0, 2);
INSERT INTO redir_stats VALUES (9, 'unknown@unknowntime', '2013-06-04 10:51:00', 0, 0, 0, 0, 0, 0, 0, 3);
INSERT INTO redir_stats VALUES (10, 'unknown@unknowntime', '2013-06-04 10:53:00', 0, 0, 0, 0, 0, 0, 0, 1);
INSERT INTO redir_stats VALUES (11, 'unknown@unknowntime', '2013-06-04 10:54:00', 0, 0, 0, 0, 0, 0, 0, 1);
INSERT INTO redir_stats VALUES (12, 'unknown@unknowntime', '2013-06-04 10:57:00', 0, 0, 0, 0, 0, 0, 0, 3);
INSERT INTO redir_stats VALUES (13, 'unknown@unknowntime', '2013-06-04 10:58:00', 0, 0, 0, 0, 0, 0, 0, 2);
INSERT INTO redir_stats VALUES (14, 'unknown@unknowntime', '2013-06-04 10:59:00', 0, 0, 0, 0, 0, 0, 0, 2);
INSERT INTO redir_stats VALUES (15, 'unknown@unknowntime', '2013-06-04 11:00:00', 0, 0, 0, 0, 0, 0, 0, 2);
INSERT INTO redir_stats VALUES (16, 'unknown@unknowntime', '2013-06-04 11:03:00', 0, 0, 0, 0, 0, 0, 0, 3);
INSERT INTO redir_stats VALUES (17, 'unknown@unknowntime', '2013-06-04 11:04:00', 0, 0, 0, 0, 0, 0, 0, 2);
INSERT INTO redir_stats VALUES (18, 'unknown@unknowntime', '2013-06-04 11:12:00', 0, 0, 0, 0, 0, 0, 0, 4);
INSERT INTO redir_stats VALUES (19, 'unknown@unknowntime', '2013-06-04 11:20:00', 0, 0, 0, 0, 0, 0, 0, 1);
INSERT INTO redir_stats VALUES (20, 'unknown@unknowntime', '2013-06-04 11:21:00', 0, 0, 0, 0, 0, 0, 0, 2);
INSERT INTO redir_stats VALUES (21, 'unknown@unknowntime', '2013-06-04 11:23:00', 0, 0, 0, 0, 0, 0, 0, 4);
INSERT INTO redir_stats VALUES (22, 'unknown@unknowntime', '2013-06-04 11:32:00', 0, 0, 0, 0, 0, 0, 0, 2);
INSERT INTO redir_stats VALUES (23, 'unknown@unknowntime', '2013-06-04 11:33:00', 0, 0, 0, 0, 0, 0, 0, 2);
INSERT INTO redir_stats VALUES (24, 'unknown@unknowntime', '2013-06-04 11:34:00', 0, 0, 0, 0, 0, 0, 0, 2);
INSERT INTO redir_stats VALUES (25, 'unknown@unknowntime', '2013-06-04 11:36:00', 0, 0, 0, 0, 0, 0, 0, 4);
INSERT INTO redir_stats VALUES (26, 'unknown@unknowntime', '2013-06-04 11:37:00', 0, 0, 0, 0, 0, 0, 0, 2);
INSERT INTO redir_stats VALUES (27, 'unknown@unknowntime', '2013-06-04 11:39:00', 0, 0, 0, 0, 0, 0, 0, 2);
INSERT INTO redir_stats VALUES (28, 'unknown@unknowntime', '2013-06-04 11:40:00', 0, 0, 0, 0, 0, 0, 0, 2);
INSERT INTO redir_stats VALUES (29, 'unknown@unknowntime', '2013-06-04 11:41:00', 0, 0, 0, 0, 0, 0, 0, 4);
INSERT INTO redir_stats VALUES (30, 'unknown@unknowntime', '2013-06-04 11:48:00', 0, 0, 0, 0, 0, 0, 0, 1);
INSERT INTO redir_stats VALUES (31, 'unknown@unknowntime', '2013-06-04 11:51:00', 0, 0, 0, 0, 0, 0, 0, 1);
INSERT INTO redir_stats VALUES (32, 'unknown@unknowntime', '2013-06-04 11:52:00', 0, 0, 0, 0, 0, 0, 0, 1);
INSERT INTO redir_stats VALUES (33, 'unknown@unknowntime', '2013-06-04 11:53:00', 0, 0, 0, 0, 0, 0, 0, 2);
INSERT INTO redir_stats VALUES (34, 'unknown@unknowntime', '2013-06-04 11:54:00', 0, 0, 0, 0, 0, 0, 0, 7);
INSERT INTO redir_stats VALUES (35, 'unknown@unknowntime', '2013-06-04 11:55:00', 0, 0, 0, 0, 0, 0, 0, 5);
INSERT INTO redir_stats VALUES (36, 'unknown@unknowntime', '2013-06-04 11:56:00', 0, 0, 0, 0, 0, 0, 0, 7);
INSERT INTO redir_stats VALUES (37, 'unknown@unknowntime', '2013-06-04 11:57:00', 0, 0, 0, 0, 0, 0, 0, 7);
INSERT INTO redir_stats VALUES (38, 'unknown@unknowntime', '2013-06-04 11:58:00', 0, 0, 0, 0, 0, 0, 0, 6);
INSERT INTO redir_stats VALUES (39, 'unknown@unknowntime', '2013-06-04 12:00:00', 0, 0, 0, 0, 0, 0, 0, 4);
INSERT INTO redir_stats VALUES (40, 'unknown@unknowntime', '2013-06-04 12:01:00', 0, 0, 0, 0, 0, 0, 0, 3);
INSERT INTO redir_stats VALUES (41, 'unknown@unknowntime', '2013-06-04 12:06:00', 0, 0, 0, 0, 0, 0, 0, 1);
INSERT INTO redir_stats VALUES (42, 'unknown@unknowntime', '2013-06-04 12:07:00', 0, 0, 0, 0, 0, 0, 0, 6);
INSERT INTO redir_stats VALUES (43, 'unknown@unknowntime', '2013-06-04 12:09:00', 0, 0, 0, 0, 0, 0, 0, 1);
INSERT INTO redir_stats VALUES (44, 'unknown@unknowntime', '2013-06-04 12:10:00', 0, 0, 0, 0, 0, 0, 0, 2);
INSERT INTO redir_stats VALUES (45, 'unknown@unknowntime', '2013-06-04 12:11:00', 0, 0, 0, 0, 0, 0, 0, 6);
INSERT INTO redir_stats VALUES (46, 'unknown@unknowntime', '2013-06-04 12:13:00', 0, 0, 0, 0, 0, 0, 0, 3);
INSERT INTO redir_stats VALUES (47, 'unknown@unknowntime', '2013-06-04 12:14:00', 0, 0, 0, 0, 0, 0, 0, 5);
INSERT INTO redir_stats VALUES (48, 'unknown@unknowntime', '2013-06-04 12:15:00', 0, 0, 0, 0, 0, 0, 0, 3);
INSERT INTO redir_stats VALUES (49, 'unknown@unknowntime', '2013-06-04 12:18:00', 0, 0, 0, 0, 0, 0, 0, 2);
INSERT INTO redir_stats VALUES (50, 'unknown@unknowntime', '2013-06-04 12:20:00', 0, 0, 0, 0, 0, 0, 0, 4);
INSERT INTO redir_stats VALUES (51, 'unknown@unknowntime', '2013-06-04 12:21:00', 0, 0, 0, 0, 0, 0, 0, 2);
INSERT INTO redir_stats VALUES (52, 'unknown@unknowntime', '2013-06-04 12:22:00', 0, 0, 0, 0, 0, 0, 0, 3);
INSERT INTO redir_stats VALUES (53, 'unknown@unknowntime', '2013-06-04 12:23:00', 0, 0, 0, 0, 0, 0, 0, 3);
INSERT INTO redir_stats VALUES (54, 'unknown@unknowntime', '2013-06-04 12:24:00', 0, 0, 0, 0, 0, 0, 0, 4);
INSERT INTO redir_stats VALUES (55, 'unknown@unknowntime', '2013-06-04 12:26:00', 0, 0, 0, 0, 0, 0, 0, 4);
INSERT INTO redir_stats VALUES (56, 'unknown@unknowntime', '2013-06-04 12:28:00', 0, 0, 0, 0, 0, 0, 0, 2);
INSERT INTO redir_stats VALUES (57, 'unknown@unknowntime', '2013-06-04 12:29:00', 0, 0, 0, 0, 0, 0, 0, 3);
INSERT INTO redir_stats VALUES (58, 'unknown@unknowntime', '2013-06-04 12:30:00', 0, 0, 0, 0, 0, 0, 0, 3);
INSERT INTO redir_stats VALUES (59, 'unknown@unknowntime', '2013-06-04 12:31:00', 0, 0, 0, 0, 0, 0, 0, 6);
INSERT INTO redir_stats VALUES (60, 'unknown@unknowntime', '2013-06-04 12:36:00', 0, 0, 0, 0, 0, 0, 0, 4);
INSERT INTO redir_stats VALUES (61, 'unknown@unknowntime', '2013-08-06 15:17:00', 0, 0, 0, 0, 0, 0, 0, 1);
INSERT INTO redir_stats VALUES (62, 'unknown@unknowntime', '2013-08-06 15:19:00', 0, 0, 0, 0, 0, 0, 0, 4);
INSERT INTO redir_stats VALUES (63, 'unknown@unknowntime', '2013-08-06 15:34:00', 0, 0, 0, 0, 0, 0, 0, 1);
INSERT INTO redir_stats VALUES (64, 'unknown@unknowntime', '2013-08-06 15:35:00', 0, 0, 0, 0, 0, 0, 0, 5);
INSERT INTO redir_stats VALUES (65, 'unknown@unknowntime', '2013-08-06 15:36:00', 0, 0, 0, 0, 0, 0, 0, 4);
INSERT INTO redir_stats VALUES (66, 'unknown@unknowntime', '2013-08-06 15:38:00', 0, 0, 0, 0, 0, 0, 0, 6);
INSERT INTO redir_stats VALUES (67, 'unknown@unknowntime', '2013-08-06 15:39:00', 0, 0, 0, 0, 0, 0, 0, 5);


--
-- Data for Name: review_log; Type: TABLE DATA; Schema: public; Owner: dany
--

INSERT INTO review_log VALUES (20, 1, 9, '2013-06-04 10:51:58.847484', 'normal', 'new', 'accepted', NULL, 1020);
INSERT INTO review_log VALUES (21, 1, 9, '2013-06-04 10:52:03.060447', 'normal', 'new', 'accepted', NULL, 1020);
INSERT INTO review_log VALUES (22, 1, 9, '2013-06-04 10:52:10.494763', 'normal', 'new', 'accepted', NULL, 1020);
INSERT INTO review_log VALUES (23, 1, 9, '2013-06-04 10:52:12.983778', 'normal', 'new', 'accepted', NULL, 1020);
INSERT INTO review_log VALUES (24, 1, 9, '2013-06-04 10:52:19.031284', 'normal', 'new', 'accepted', NULL, 1020);
INSERT INTO review_log VALUES (25, 1, 9, '2013-06-04 10:52:21.669333', 'normal', 'new', 'accepted', NULL, 1020);
INSERT INTO review_log VALUES (26, 1, 9, '2013-06-04 11:03:15.132247', 'normal', 'new', 'accepted', NULL, 1020);
INSERT INTO review_log VALUES (27, 1, 9, '2013-06-04 11:03:18.293219', 'normal', 'new', 'accepted', NULL, 1020);
INSERT INTO review_log VALUES (28, 1, 9, '2013-06-04 11:03:23.770424', 'normal', 'new', 'accepted', NULL, 1020);
INSERT INTO review_log VALUES (29, 1, 9, '2013-06-04 11:03:26.585804', 'normal', 'new', 'accepted', NULL, 1020);
INSERT INTO review_log VALUES (30, 1, 9, '2013-06-04 11:03:31.075334', 'normal', 'new', 'accepted', NULL, 1020);
INSERT INTO review_log VALUES (31, 1, 9, '2013-06-04 11:39:25.182244', 'normal', 'new', 'accepted', NULL, 1100);
INSERT INTO review_log VALUES (32, 1, 9, '2013-06-04 11:39:29.80389', 'normal', 'new', 'accepted', NULL, 1100);
INSERT INTO review_log VALUES (35, 1, 9, '2013-06-04 11:39:37.380686', 'normal', 'new', 'accepted', NULL, 2020);
INSERT INTO review_log VALUES (36, 1, 9, '2013-06-04 11:39:40.617107', 'normal', 'new', 'accepted', NULL, 2060);
INSERT INTO review_log VALUES (39, 1, 9, '2013-06-04 11:39:47.165872', 'normal', 'new', 'accepted', NULL, 2020);
INSERT INTO review_log VALUES (37, 1, 9, '2013-06-04 11:39:54.980674', 'normal', 'new', 'accepted', NULL, 2060);
INSERT INTO review_log VALUES (38, 1, 9, '2013-06-04 11:39:58.627429', 'normal', 'new', 'accepted', NULL, 2020);
INSERT INTO review_log VALUES (33, 1, 9, '2013-06-04 11:40:05.443704', 'normal', 'new', 'accepted', NULL, 1100);
INSERT INTO review_log VALUES (34, 1, 9, '2013-06-04 11:40:09.295412', 'normal', 'new', 'accepted', NULL, 2060);
INSERT INTO review_log VALUES (40, 1, 9, '2013-06-04 11:58:48.056116', 'normal', 'new', 'accepted', NULL, 7040);
INSERT INTO review_log VALUES (41, 1, 9, '2013-06-04 11:58:51.896416', 'normal', 'new', 'accepted', NULL, 7020);
INSERT INTO review_log VALUES (42, 1, 9, '2013-06-04 11:59:01.64359', 'normal', 'new', 'accepted', NULL, 6080);
INSERT INTO review_log VALUES (43, 1, 9, '2013-06-04 11:59:05.848706', 'normal', 'new', 'accepted', NULL, 6080);
INSERT INTO review_log VALUES (44, 1, 9, '2013-06-04 11:59:12.342116', 'normal', 'new', 'accepted', NULL, 7040);
INSERT INTO review_log VALUES (45, 1, 9, '2013-06-04 11:59:17.737615', 'normal', 'new', 'accepted', NULL, 6080);
INSERT INTO review_log VALUES (46, 1, 9, '2013-06-04 11:59:23.717305', 'normal', 'new', 'accepted', NULL, 3060);
INSERT INTO review_log VALUES (47, 1, 9, '2013-06-04 11:59:26.342338', 'normal', 'new', 'accepted', NULL, 3020);
INSERT INTO review_log VALUES (48, 1, 9, '2013-06-04 11:59:31.159284', 'normal', 'new', 'accepted', NULL, 3060);
INSERT INTO review_log VALUES (49, 1, 9, '2013-06-04 11:59:35.454628', 'normal', 'new', 'accepted', NULL, 3020);
INSERT INTO review_log VALUES (50, 1, 9, '2013-06-04 11:59:43.225016', 'normal', 'new', 'accepted', NULL, 3060);
INSERT INTO review_log VALUES (51, 1, 9, '2013-06-04 11:59:46.011444', 'normal', 'new', 'accepted', NULL, 3020);
INSERT INTO review_log VALUES (52, 1, 9, '2013-06-04 11:59:50.034908', 'normal', 'new', 'accepted', NULL, 3060);
INSERT INTO review_log VALUES (53, 1, 9, '2013-06-04 11:59:53.36316', 'normal', 'new', 'accepted', NULL, 3020);
INSERT INTO review_log VALUES (54, 1, 9, '2013-06-04 11:59:58.805189', 'normal', 'new', 'accepted', NULL, 3060);
INSERT INTO review_log VALUES (55, 1, 9, '2013-06-04 12:00:08.524804', 'normal', 'new', 'accepted', NULL, 3020);
INSERT INTO review_log VALUES (56, 1, 9, '2013-06-04 12:00:17.586168', 'normal', 'new', 'accepted', NULL, 3020);
INSERT INTO review_log VALUES (57, 1, 9, '2013-06-04 12:00:33.275949', 'normal', 'new', 'accepted', NULL, 3020);
INSERT INTO review_log VALUES (58, 1, 9, '2013-06-04 12:11:18.709302', 'normal', 'new', 'accepted', NULL, 5100);
INSERT INTO review_log VALUES (59, 1, 9, '2013-06-04 12:11:21.872854', 'normal', 'new', 'accepted', NULL, 5020);
INSERT INTO review_log VALUES (60, 1, 9, '2013-06-04 12:11:26.704452', 'normal', 'new', 'accepted', NULL, 5020);
INSERT INTO review_log VALUES (61, 1, 9, '2013-06-04 12:11:30.178703', 'normal', 'new', 'accepted', NULL, 5100);
INSERT INTO review_log VALUES (62, 1, 9, '2013-06-04 12:11:34.82732', 'normal', 'new', 'accepted', NULL, 5100);
INSERT INTO review_log VALUES (63, 1, 9, '2013-06-04 12:20:30.972276', 'normal', 'new', 'accepted', NULL, 7040);
INSERT INTO review_log VALUES (64, 1, 9, '2013-06-04 12:20:34.121895', 'normal', 'new', 'accepted', NULL, 7020);
INSERT INTO review_log VALUES (65, 1, 9, '2013-06-04 12:20:39.108132', 'normal', 'new', 'accepted', NULL, 7020);
INSERT INTO review_log VALUES (66, 1, 9, '2013-06-04 12:20:42.041479', 'normal', 'new', 'accepted', NULL, 7020);
INSERT INTO review_log VALUES (67, 1, 9, '2013-06-04 12:20:46.755825', 'normal', 'new', 'accepted', NULL, 7020);
INSERT INTO review_log VALUES (68, 1, 9, '2013-06-04 12:20:49.002899', 'normal', 'new', 'accepted', NULL, 7020);
INSERT INTO review_log VALUES (69, 1, 9, '2013-06-04 12:20:54.280291', 'normal', 'new', 'accepted', NULL, 7020);
INSERT INTO review_log VALUES (70, 1, 9, '2013-06-04 12:32:14.392958', 'normal', 'new', 'accepted', NULL, 6160);
INSERT INTO review_log VALUES (71, 1, 9, '2013-06-04 12:32:17.838751', 'normal', 'new', 'accepted', NULL, 6160);
INSERT INTO review_log VALUES (72, 1, 9, '2013-06-04 12:32:22.166087', 'normal', 'new', 'accepted', NULL, 6160);
INSERT INTO review_log VALUES (73, 1, 9, '2013-06-04 12:32:24.019538', 'normal', 'new', 'accepted', NULL, 6160);
INSERT INTO review_log VALUES (74, 1, 9, '2013-06-04 12:32:28.333066', 'normal', 'new', 'accepted', NULL, 6160);
INSERT INTO review_log VALUES (75, 1, 9, '2013-06-04 12:32:29.773792', 'normal', 'new', 'accepted', NULL, 6160);
INSERT INTO review_log VALUES (76, 1, 9, '2013-06-04 12:32:33.488079', 'normal', 'new', 'accepted', NULL, 6160);
INSERT INTO review_log VALUES (77, 1, 9, '2013-06-04 12:32:35.535407', 'normal', 'new', 'accepted', NULL, 6160);
INSERT INTO review_log VALUES (78, 1, 9, '2013-06-04 12:32:40.754062', 'normal', 'new', 'accepted', NULL, 6160);
INSERT INTO review_log VALUES (79, 1, 9, '2013-06-04 12:32:42.237431', 'normal', 'new', 'accepted', NULL, 6120);
INSERT INTO review_log VALUES (80, 1, 9, '2013-06-04 12:32:45.690422', 'normal', 'new', 'accepted', NULL, 6120);
INSERT INTO review_log VALUES (81, 1, 9, '2013-06-04 12:32:47.537926', 'normal', 'new', 'accepted', NULL, 6120);
INSERT INTO review_log VALUES (82, 1, 9, '2013-06-04 12:32:52.558464', 'normal', 'new', 'accepted', 'Para mejorar las posibilidades de que tu aviso sea visto, te recomendamos que agregues im�genes del producto que ofreces o buscas.', 6120);
INSERT INTO review_log VALUES (83, 1, 9, '2013-08-06 15:19:30.588446', 'normal', 'new', 'accepted', NULL, 3040);
INSERT INTO review_log VALUES (84, 1, 9, '2013-08-06 15:19:34.894397', 'normal', 'new', 'accepted', NULL, 3080);


--
-- Data for Name: sms_users; Type: TABLE DATA; Schema: public; Owner: dany
--



--
-- Data for Name: sms_log; Type: TABLE DATA; Schema: public; Owner: dany
--



--
-- Data for Name: watch_users; Type: TABLE DATA; Schema: public; Owner: dany
--



--
-- Data for Name: watch_queries; Type: TABLE DATA; Schema: public; Owner: dany
--



--
-- Data for Name: sms_log_watch; Type: TABLE DATA; Schema: public; Owner: dany
--



--
-- Data for Name: state_params; Type: TABLE DATA; Schema: public; Owner: dany
--

INSERT INTO state_params VALUES (20, 1, 220, 'filter_name', 'normal');
INSERT INTO state_params VALUES (21, 1, 221, 'filter_name', 'normal');
INSERT INTO state_params VALUES (22, 1, 224, 'filter_name', 'normal');
INSERT INTO state_params VALUES (23, 1, 225, 'filter_name', 'normal');
INSERT INTO state_params VALUES (24, 1, 228, 'filter_name', 'normal');
INSERT INTO state_params VALUES (25, 1, 229, 'filter_name', 'normal');
INSERT INTO state_params VALUES (26, 1, 247, 'filter_name', 'normal');
INSERT INTO state_params VALUES (27, 1, 248, 'filter_name', 'normal');
INSERT INTO state_params VALUES (28, 1, 251, 'filter_name', 'normal');
INSERT INTO state_params VALUES (29, 1, 252, 'filter_name', 'normal');
INSERT INTO state_params VALUES (30, 1, 254, 'filter_name', 'normal');
INSERT INTO state_params VALUES (31, 1, 295, 'filter_name', 'normal');
INSERT INTO state_params VALUES (32, 1, 296, 'filter_name', 'normal');
INSERT INTO state_params VALUES (35, 1, 297, 'filter_name', 'normal');
INSERT INTO state_params VALUES (36, 1, 298, 'filter_name', 'normal');
INSERT INTO state_params VALUES (39, 1, 299, 'filter_name', 'normal');
INSERT INTO state_params VALUES (37, 1, 300, 'filter_name', 'normal');
INSERT INTO state_params VALUES (38, 1, 301, 'filter_name', 'normal');
INSERT INTO state_params VALUES (33, 1, 302, 'filter_name', 'normal');
INSERT INTO state_params VALUES (34, 1, 303, 'filter_name', 'normal');
INSERT INTO state_params VALUES (40, 1, 351, 'filter_name', 'normal');
INSERT INTO state_params VALUES (41, 1, 352, 'filter_name', 'normal');
INSERT INTO state_params VALUES (42, 1, 358, 'filter_name', 'normal');
INSERT INTO state_params VALUES (43, 1, 359, 'filter_name', 'normal');
INSERT INTO state_params VALUES (44, 1, 362, 'filter_name', 'normal');
INSERT INTO state_params VALUES (45, 1, 363, 'filter_name', 'normal');
INSERT INTO state_params VALUES (46, 1, 366, 'filter_name', 'normal');
INSERT INTO state_params VALUES (47, 1, 367, 'filter_name', 'normal');
INSERT INTO state_params VALUES (48, 1, 373, 'filter_name', 'normal');
INSERT INTO state_params VALUES (49, 1, 374, 'filter_name', 'normal');
INSERT INTO state_params VALUES (50, 1, 377, 'filter_name', 'normal');
INSERT INTO state_params VALUES (51, 1, 378, 'filter_name', 'normal');
INSERT INTO state_params VALUES (52, 1, 381, 'filter_name', 'normal');
INSERT INTO state_params VALUES (53, 1, 382, 'filter_name', 'normal');
INSERT INTO state_params VALUES (54, 1, 385, 'filter_name', 'normal');
INSERT INTO state_params VALUES (55, 1, 386, 'filter_name', 'normal');
INSERT INTO state_params VALUES (56, 1, 391, 'filter_name', 'normal');
INSERT INTO state_params VALUES (57, 1, 393, 'filter_name', 'normal');
INSERT INTO state_params VALUES (58, 1, 411, 'filter_name', 'normal');
INSERT INTO state_params VALUES (59, 1, 412, 'filter_name', 'normal');
INSERT INTO state_params VALUES (60, 1, 415, 'filter_name', 'normal');
INSERT INTO state_params VALUES (61, 1, 416, 'filter_name', 'normal');
INSERT INTO state_params VALUES (62, 1, 418, 'filter_name', 'normal');
INSERT INTO state_params VALUES (63, 1, 442, 'filter_name', 'normal');
INSERT INTO state_params VALUES (64, 1, 443, 'filter_name', 'normal');
INSERT INTO state_params VALUES (65, 1, 446, 'filter_name', 'normal');
INSERT INTO state_params VALUES (66, 1, 447, 'filter_name', 'normal');
INSERT INTO state_params VALUES (67, 1, 450, 'filter_name', 'normal');
INSERT INTO state_params VALUES (68, 1, 451, 'filter_name', 'normal');
INSERT INTO state_params VALUES (69, 1, 453, 'filter_name', 'normal');
INSERT INTO state_params VALUES (70, 1, 495, 'filter_name', 'normal');
INSERT INTO state_params VALUES (71, 1, 496, 'filter_name', 'normal');
INSERT INTO state_params VALUES (72, 1, 499, 'filter_name', 'normal');
INSERT INTO state_params VALUES (73, 1, 500, 'filter_name', 'normal');
INSERT INTO state_params VALUES (74, 1, 503, 'filter_name', 'normal');
INSERT INTO state_params VALUES (75, 1, 504, 'filter_name', 'normal');
INSERT INTO state_params VALUES (76, 1, 507, 'filter_name', 'normal');
INSERT INTO state_params VALUES (77, 1, 508, 'filter_name', 'normal');
INSERT INTO state_params VALUES (78, 1, 511, 'filter_name', 'normal');
INSERT INTO state_params VALUES (79, 1, 512, 'filter_name', 'normal');
INSERT INTO state_params VALUES (80, 1, 515, 'filter_name', 'normal');
INSERT INTO state_params VALUES (81, 1, 516, 'filter_name', 'normal');
INSERT INTO state_params VALUES (82, 1, 518, 'filter_name', 'normal');
INSERT INTO state_params VALUES (83, 1, 527, 'filter_name', 'normal');
INSERT INTO state_params VALUES (84, 1, 528, 'filter_name', 'normal');


--
-- Data for Name: stats_daily; Type: TABLE DATA; Schema: public; Owner: dany
--



--
-- Data for Name: stats_daily_ad_actions; Type: TABLE DATA; Schema: public; Owner: dany
--



--
-- Data for Name: stats_hourly; Type: TABLE DATA; Schema: public; Owner: dany
--



--
-- Data for Name: store_actions; Type: TABLE DATA; Schema: public; Owner: dany
--



--
-- Data for Name: store_action_states; Type: TABLE DATA; Schema: public; Owner: dany
--



--
-- Data for Name: store_changes; Type: TABLE DATA; Schema: public; Owner: dany
--



--
-- Data for Name: store_login_tokens; Type: TABLE DATA; Schema: public; Owner: dany
--



--
-- Data for Name: store_params; Type: TABLE DATA; Schema: public; Owner: dany
--



--
-- Data for Name: synonyms; Type: TABLE DATA; Schema: public; Owner: dany
--

INSERT INTO synonyms VALUES (1, 3040, 'armoire', NULL);
INSERT INTO synonyms VALUES (2, 3040, 'armoires ', 1);
INSERT INTO synonyms VALUES (3, 3040, 'banc', NULL);
INSERT INTO synonyms VALUES (4, 3040, 'bancs ', 3);
INSERT INTO synonyms VALUES (5, 3040, 'banquette ', NULL);
INSERT INTO synonyms VALUES (6, 3040, 'banquettes ', 5);
INSERT INTO synonyms VALUES (7, 3040, 'bergere', NULL);
INSERT INTO synonyms VALUES (8, 3040, 'bergeres', 7);
INSERT INTO synonyms VALUES (9, 3040, 'bibliotheque ', NULL);
INSERT INTO synonyms VALUES (10, 3040, 'biblioteque', 9);
INSERT INTO synonyms VALUES (11, 3040, 'bibliotheques', 9);
INSERT INTO synonyms VALUES (12, 3040, 'buffet ', NULL);
INSERT INTO synonyms VALUES (13, 3040, 'bufet', 12);
INSERT INTO synonyms VALUES (14, 3040, 'buffets', 12);
INSERT INTO synonyms VALUES (15, 3040, 'bureau ', NULL);
INSERT INTO synonyms VALUES (16, 3040, 'bureaux ', 15);
INSERT INTO synonyms VALUES (17, 3040, 'canape', NULL);
INSERT INTO synonyms VALUES (18, 3040, 'canaper ', 17);
INSERT INTO synonyms VALUES (19, 3040, 'canapes ', 17);
INSERT INTO synonyms VALUES (20, 3040, 'canapee', 17);
INSERT INTO synonyms VALUES (21, 3040, 'cannape', 17);
INSERT INTO synonyms VALUES (22, 3040, 'chaise ', NULL);
INSERT INTO synonyms VALUES (23, 3040, 'chaises ', 22);
INSERT INTO synonyms VALUES (24, 3040, 'chevet ', NULL);
INSERT INTO synonyms VALUES (25, 3040, 'chevets ', 24);
INSERT INTO synonyms VALUES (26, 3040, 'clic clac', NULL);
INSERT INTO synonyms VALUES (27, 3040, 'clic-clac', 26);
INSERT INTO synonyms VALUES (28, 3040, 'cliclac', 26);
INSERT INTO synonyms VALUES (29, 3040, 'commode ', NULL);
INSERT INTO synonyms VALUES (30, 3040, 'commodes ', 29);
INSERT INTO synonyms VALUES (31, 3040, 'comode', 29);
INSERT INTO synonyms VALUES (32, 3040, 'etagere ', NULL);
INSERT INTO synonyms VALUES (33, 3040, 'etageres ', 32);
INSERT INTO synonyms VALUES (34, 3040, 'fauteuil ', NULL);
INSERT INTO synonyms VALUES (35, 3040, 'fauteil ', 34);
INSERT INTO synonyms VALUES (36, 3040, 'fauteils ', 34);
INSERT INTO synonyms VALUES (37, 3040, 'fauteuille ', 34);
INSERT INTO synonyms VALUES (38, 3040, 'fauteuilles ', 34);
INSERT INTO synonyms VALUES (39, 3040, 'fauteuils ', 34);
INSERT INTO synonyms VALUES (40, 3040, 'matelas ', NULL);
INSERT INTO synonyms VALUES (41, 3040, 'matelat ', 40);
INSERT INTO synonyms VALUES (42, 3040, 'meuble ', NULL);
INSERT INTO synonyms VALUES (43, 3040, 'meubles ', 42);
INSERT INTO synonyms VALUES (44, 3040, 'mezzanine ', NULL);
INSERT INTO synonyms VALUES (45, 3040, 'mezanine ', 44);
INSERT INTO synonyms VALUES (46, 3040, 'porte-manteau', NULL);
INSERT INTO synonyms VALUES (47, 3040, 'porte-manteaux', 46);
INSERT INTO synonyms VALUES (48, 3040, 'porte manteau', 46);
INSERT INTO synonyms VALUES (49, 3040, 'porte manteaux', 46);
INSERT INTO synonyms VALUES (50, 3040, 'pouf', NULL);
INSERT INTO synonyms VALUES (51, 3040, 'poufs ', 50);
INSERT INTO synonyms VALUES (52, 3040, 'rangement ', NULL);
INSERT INTO synonyms VALUES (53, 3040, 'rangements ', 52);
INSERT INTO synonyms VALUES (54, 3040, 'rocking chair', NULL);
INSERT INTO synonyms VALUES (55, 3040, 'rocking-chair', 54);
INSERT INTO synonyms VALUES (56, 3040, 'siege ', NULL);
INSERT INTO synonyms VALUES (57, 3040, 'sieges', 56);
INSERT INTO synonyms VALUES (58, 3040, 'sommier ', NULL);
INSERT INTO synonyms VALUES (59, 3040, 'sommiers ', 58);
INSERT INTO synonyms VALUES (60, 3040, 'table ', NULL);
INSERT INTO synonyms VALUES (61, 3040, 'tables ', 60);
INSERT INTO synonyms VALUES (62, 3040, 'tabouret ', NULL);
INSERT INTO synonyms VALUES (63, 3040, 'tabourets ', 62);
INSERT INTO synonyms VALUES (64, 3040, 'tiroir ', NULL);
INSERT INTO synonyms VALUES (65, 3040, 'tiroirs ', 64);
INSERT INTO synonyms VALUES (66, 3040, 'vaisselier ', NULL);
INSERT INTO synonyms VALUES (67, 3040, 'vaissellier ', 66);
INSERT INTO synonyms VALUES (68, 3060, 'chauffe-eau', NULL);
INSERT INTO synonyms VALUES (69, 3060, 'chauffe eau', 68);
INSERT INTO synonyms VALUES (70, 3060, 'cuiseur', NULL);
INSERT INTO synonyms VALUES (71, 3060, 'cuisseur', 70);
INSERT INTO synonyms VALUES (72, 3060, 'cuit vapeur', NULL);
INSERT INTO synonyms VALUES (73, 3060, 'cuit-vapeur', 72);
INSERT INTO synonyms VALUES (74, 3060, 'gauffrer', NULL);
INSERT INTO synonyms VALUES (75, 3060, 'gauffrier', 74);
INSERT INTO synonyms VALUES (76, 3060, 'gaufrier', 74);
INSERT INTO synonyms VALUES (77, 3060, 'gaziniere', NULL);
INSERT INTO synonyms VALUES (78, 3060, 'gazinniere', 77);
INSERT INTO synonyms VALUES (79, 3060, 'grille-pain', NULL);
INSERT INTO synonyms VALUES (80, 3060, 'grille pain', 79);
INSERT INTO synonyms VALUES (81, 3060, 'lave linge', NULL);
INSERT INTO synonyms VALUES (82, 3060, 'lave-linge', 81);
INSERT INTO synonyms VALUES (83, 3060, 'lave vaisselle', NULL);
INSERT INTO synonyms VALUES (84, 3060, 'lave-vaisselle', 83);
INSERT INTO synonyms VALUES (85, 3060, 'micro onde', NULL);
INSERT INTO synonyms VALUES (86, 3060, 'micro ondes', 85);
INSERT INTO synonyms VALUES (87, 3060, 'micro-onde', 85);
INSERT INTO synonyms VALUES (88, 3060, 'micro-ondes', 85);
INSERT INTO synonyms VALUES (89, 3060, 'mixer', NULL);
INSERT INTO synonyms VALUES (90, 3060, 'mixeur', 89);
INSERT INTO synonyms VALUES (91, 3060, 'multifonction', NULL);
INSERT INTO synonyms VALUES (92, 3060, 'multifonctions', 91);
INSERT INTO synonyms VALUES (93, 3060, 'plaque a gaz', NULL);
INSERT INTO synonyms VALUES (94, 3060, 'plaque gaz', 93);
INSERT INTO synonyms VALUES (95, 3060, 'plaque de cuisson', NULL);
INSERT INTO synonyms VALUES (96, 3060, 'plaques de cuisson', 95);
INSERT INTO synonyms VALUES (97, 3060, 'plaque electrique', NULL);
INSERT INTO synonyms VALUES (98, 3060, 'plaques electriques', 97);
INSERT INTO synonyms VALUES (99, 3060, 'presse agrumes', NULL);
INSERT INTO synonyms VALUES (100, 3060, 'presse agrume', 99);
INSERT INTO synonyms VALUES (101, 3060, 'refrigerateur', NULL);
INSERT INTO synonyms VALUES (102, 3060, 'refrigirateur', 101);
INSERT INTO synonyms VALUES (103, 3060, 'refregirateur', 101);
INSERT INTO synonyms VALUES (104, 3060, 'seche cheveux', NULL);
INSERT INTO synonyms VALUES (105, 3060, 'seche-cheveux', 104);
INSERT INTO synonyms VALUES (106, 3060, 'seche linge', NULL);
INSERT INTO synonyms VALUES (107, 3060, 'seche-linge', 106);
INSERT INTO synonyms VALUES (108, 3060, 'table cuisson', NULL);
INSERT INTO synonyms VALUES (109, 3060, 'table de cuisson', 108);
INSERT INTO synonyms VALUES (110, 3060, 'vitroceramique', NULL);
INSERT INTO synonyms VALUES (111, 3060, 'vitro-ceramique', 110);
INSERT INTO synonyms VALUES (112, 3060, 'whirlpool', NULL);
INSERT INTO synonyms VALUES (113, 3060, 'whirpool', 112);
INSERT INTO synonyms VALUES (114, 3060, 'wirlpool', 112);
INSERT INTO synonyms VALUES (115, 8020, 'bouteille de gaz', NULL);
INSERT INTO synonyms VALUES (116, 8020, 'bouteille gaz', 115);
INSERT INTO synonyms VALUES (117, 8020, 'bouteilles de gaz', 115);
INSERT INTO synonyms VALUES (118, 8020, 'bouteilles gaz', 115);
INSERT INTO synonyms VALUES (119, 8020, 'brique', NULL);
INSERT INTO synonyms VALUES (120, 8020, 'briques', 119);
INSERT INTO synonyms VALUES (121, 8020, 'carrelage', NULL);
INSERT INTO synonyms VALUES (122, 8020, 'carrelages', 121);
INSERT INTO synonyms VALUES (123, 8020, 'casier a bouteille', NULL);
INSERT INTO synonyms VALUES (124, 8020, 'casier a bouteilles', 123);
INSERT INTO synonyms VALUES (125, 8020, 'chauffage', NULL);
INSERT INTO synonyms VALUES (126, 8020, 'chauffages', 125);
INSERT INTO synonyms VALUES (127, 8020, 'chauffe eau', NULL);
INSERT INTO synonyms VALUES (128, 8020, 'chauffe-eau', 127);
INSERT INTO synonyms VALUES (129, 8020, 'convecteur', NULL);
INSERT INTO synonyms VALUES (130, 8020, 'convecteurs', 129);
INSERT INTO synonyms VALUES (131, 8020, 'cuve', NULL);
INSERT INTO synonyms VALUES (132, 8020, 'cuves', 131);
INSERT INTO synonyms VALUES (133, 8020, 'disjoncteur', NULL);
INSERT INTO synonyms VALUES (134, 8020, 'disjoncteurs', 133);
INSERT INTO synonyms VALUES (135, 8020, 'fenetre', NULL);
INSERT INTO synonyms VALUES (136, 8020, 'fenetres', 135);
INSERT INTO synonyms VALUES (137, 8020, 'plante', NULL);
INSERT INTO synonyms VALUES (138, 8020, 'plantes', 137);
INSERT INTO synonyms VALUES (139, 8020, 'porte', NULL);
INSERT INTO synonyms VALUES (140, 8020, 'portes', 139);
INSERT INTO synonyms VALUES (141, 8020, 'radiateur', NULL);
INSERT INTO synonyms VALUES (142, 8020, 'radiateurs', 141);
INSERT INTO synonyms VALUES (143, 8020, 'remblai', NULL);
INSERT INTO synonyms VALUES (144, 8020, 'remblais', 143);
INSERT INTO synonyms VALUES (145, 8020, 'store', NULL);
INSERT INTO synonyms VALUES (146, 8020, 'stores', 145);
INSERT INTO synonyms VALUES (147, 8020, 'taille haie', NULL);
INSERT INTO synonyms VALUES (148, 8020, 'taille-haie', 147);
INSERT INTO synonyms VALUES (149, 8020, 'taille haies', 147);
INSERT INTO synonyms VALUES (150, 8020, 'taille-haies', 147);
INSERT INTO synonyms VALUES (151, 8020, 'tonneau', NULL);
INSERT INTO synonyms VALUES (152, 8020, 'tonneaux', 151);
INSERT INTO synonyms VALUES (153, 8020, 'tuile', NULL);
INSERT INTO synonyms VALUES (154, 8020, 'tuiles', 153);
INSERT INTO synonyms VALUES (155, 8020, 'tuyau', NULL);
INSERT INTO synonyms VALUES (156, 8020, 'tuyaux', 155);
INSERT INTO synonyms VALUES (157, 8020, 'vasque', NULL);
INSERT INTO synonyms VALUES (158, 8020, 'vasques', 157);
INSERT INTO synonyms VALUES (159, 8020, 'volet', NULL);
INSERT INTO synonyms VALUES (160, 8020, 'volets', 159);
INSERT INTO synonyms VALUES (161, 8060, 'biberon', NULL);
INSERT INTO synonyms VALUES (162, 8060, 'biberons', 161);
INSERT INTO synonyms VALUES (163, 8060, 'chaussure', NULL);
INSERT INTO synonyms VALUES (164, 8060, 'chaussures', 163);
INSERT INTO synonyms VALUES (165, 8060, 'ensemble fille', NULL);
INSERT INTO synonyms VALUES (166, 8060, 'ensemble filles', 165);
INSERT INTO synonyms VALUES (167, 8060, 'ensembles fille', 165);
INSERT INTO synonyms VALUES (168, 8060, 'ensembles filles', 165);
INSERT INTO synonyms VALUES (169, 8060, 'ensemble garcon', NULL);
INSERT INTO synonyms VALUES (170, 8060, 'ensemble garcons', 169);
INSERT INTO synonyms VALUES (171, 8060, 'ensembles garcon', 169);
INSERT INTO synonyms VALUES (172, 8060, 'ensembles garcons', 169);
INSERT INTO synonyms VALUES (173, 8060, 'gigoteuse', NULL);
INSERT INTO synonyms VALUES (174, 8060, 'gigoteuses', 173);
INSERT INTO synonyms VALUES (175, 8060, 'landau', NULL);
INSERT INTO synonyms VALUES (176, 8060, 'landeau', 175);
INSERT INTO synonyms VALUES (177, 8060, 'lit parapluie', NULL);
INSERT INTO synonyms VALUES (178, 8060, 'lit-parapluie', 177);
INSERT INTO synonyms VALUES (179, 8060, 'pantalon', NULL);
INSERT INTO synonyms VALUES (180, 8060, 'pantalons', 179);
INSERT INTO synonyms VALUES (181, 8060, 'porte bebe', NULL);
INSERT INTO synonyms VALUES (182, 8060, 'porte-bb', 181);
INSERT INTO synonyms VALUES (183, 8060, 'porte-bebe', 181);
INSERT INTO synonyms VALUES (184, 8060, 'poussette', NULL);
INSERT INTO synonyms VALUES (185, 8060, 'pousette', 184);
INSERT INTO synonyms VALUES (186, 8060, 'pyjama', NULL);
INSERT INTO synonyms VALUES (187, 8060, 'pyjamas', 186);
INSERT INTO synonyms VALUES (188, 8060, 'salopette', NULL);
INSERT INTO synonyms VALUES (189, 8060, 'salopettes', 188);
INSERT INTO synonyms VALUES (190, 8060, 'tee shirt', NULL);
INSERT INTO synonyms VALUES (191, 8060, 'tee-shirt', 190);
INSERT INTO synonyms VALUES (192, 8060, 'tshirt', 190);
INSERT INTO synonyms VALUES (193, 8060, 'vertbaudet', NULL);
INSERT INTO synonyms VALUES (194, 8060, 'vert baudet', 193);
INSERT INTO synonyms VALUES (195, 8060, 'verbaudet', 193);
INSERT INTO synonyms VALUES (196, 8060, 'vetement bebe', NULL);
INSERT INTO synonyms VALUES (197, 8060, 'vetements bebe', 196);
INSERT INTO synonyms VALUES (198, 8060, 'vetements enfant', NULL);
INSERT INTO synonyms VALUES (199, 8060, 'vetement enfant', 198);
INSERT INTO synonyms VALUES (200, 8060, 'vetements enfants', 198);
INSERT INTO synonyms VALUES (201, 8060, 'vetements fille', NULL);
INSERT INTO synonyms VALUES (202, 8060, 'vetements filles', 201);
INSERT INTO synonyms VALUES (203, 8060, 'vetements garcon', NULL);
INSERT INTO synonyms VALUES (204, 8060, 'vetements garcons', 203);
INSERT INTO synonyms VALUES (205, 41, 'babyfoot', NULL);
INSERT INTO synonyms VALUES (206, 41, 'baby-foot', 205);
INSERT INTO synonyms VALUES (207, 41, 'baby foot', 205);
INSERT INTO synonyms VALUES (208, 41, 'barbie', NULL);
INSERT INTO synonyms VALUES (209, 41, 'barbies', 208);
INSERT INTO synonyms VALUES (210, 41, 'camion de pompier', NULL);
INSERT INTO synonyms VALUES (211, 41, 'camion pompier', 210);
INSERT INTO synonyms VALUES (212, 41, 'domino', NULL);
INSERT INTO synonyms VALUES (213, 41, 'dominos', 212);
INSERT INTO synonyms VALUES (214, 41, 'dora', NULL);
INSERT INTO synonyms VALUES (215, 41, 'dora l''exploratrice', 214);
INSERT INTO synonyms VALUES (216, 41, 'figurine', NULL);
INSERT INTO synonyms VALUES (217, 41, 'figurines', 216);
INSERT INTO synonyms VALUES (218, 41, 'fisher price', NULL);
INSERT INTO synonyms VALUES (219, 41, 'fischer price', 218);
INSERT INTO synonyms VALUES (220, 41, 'fisherprice', 218);
INSERT INTO synonyms VALUES (221, 41, 'fisher-price', 218);
INSERT INTO synonyms VALUES (222, 41, 'jeu de construction', NULL);
INSERT INTO synonyms VALUES (223, 41, 'jeux de construction', 222);
INSERT INTO synonyms VALUES (224, 41, 'jeu de societe', NULL);
INSERT INTO synonyms VALUES (225, 41, 'jeux de societe', 224);
INSERT INTO synonyms VALUES (226, 41, 'jeu educatif', NULL);
INSERT INTO synonyms VALUES (227, 41, 'jeux educatifs', 226);
INSERT INTO synonyms VALUES (228, 41, 'jouet', NULL);
INSERT INTO synonyms VALUES (229, 41, 'jouets', 228);
INSERT INTO synonyms VALUES (230, 41, 'lego', NULL);
INSERT INTO synonyms VALUES (231, 41, 'legos', 230);
INSERT INTO synonyms VALUES (232, 41, 'peluche', NULL);
INSERT INTO synonyms VALUES (233, 41, 'pelluche', 232);
INSERT INTO synonyms VALUES (234, 41, 'peluches', 232);
INSERT INTO synonyms VALUES (235, 41, 'playmobil', NULL);
INSERT INTO synonyms VALUES (236, 41, 'playmobils', 235);
INSERT INTO synonyms VALUES (237, 41, 'playmobile', 235);
INSERT INTO synonyms VALUES (238, 41, 'poupee', NULL);
INSERT INTO synonyms VALUES (239, 41, 'poupees', 238);
INSERT INTO synonyms VALUES (240, 41, 'puzzle', NULL);
INSERT INTO synonyms VALUES (241, 41, 'puzzles', 240);
INSERT INTO synonyms VALUES (242, 41, 'trottinette', NULL);
INSERT INTO synonyms VALUES (243, 41, 'trotinette', 242);
INSERT INTO synonyms VALUES (244, 41, 'vtech', NULL);
INSERT INTO synonyms VALUES (245, 41, 'v-tech', 244);


--
-- Data for Name: trans_queue; Type: TABLE DATA; Schema: public; Owner: dany
--

INSERT INTO trans_queue VALUES (10, '2013-06-04 11:03:26.588884', '28283@tetsuo.schibsted.cl', NULL, '2013-06-04 11:03:42.363338', '28283@tetsuo.schibsted.cl', 'TRANS_OK', '2013-06-04 11:03:41.971196', NULL, 'cmd:admail
commit:1
ad_id:29
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue VALUES (11, '2013-06-04 11:03:31.078433', '28283@tetsuo.schibsted.cl', NULL, '2013-06-04 11:03:42.377125', '28283@tetsuo.schibsted.cl', 'TRANS_OK', '2013-06-04 11:03:41.971196', NULL, 'cmd:admail
commit:1
ad_id:30
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue VALUES (8, '2013-06-04 11:03:18.305913', '28283@tetsuo.schibsted.cl', NULL, '2013-06-04 11:03:42.382452', '28283@tetsuo.schibsted.cl', 'TRANS_OK', '2013-06-04 11:03:41.971196', NULL, 'cmd:admail
commit:1
ad_id:27
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue VALUES (6, '2013-06-04 10:52:21.672328', '28283@tetsuo.schibsted.cl', NULL, '2013-06-04 11:03:42.391287', '28283@tetsuo.schibsted.cl', 'TRANS_OK', '2013-06-04 11:03:41.971196', NULL, 'cmd:admail
commit:1
ad_id:25
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue VALUES (7, '2013-06-04 11:03:15.142291', '28283@tetsuo.schibsted.cl', NULL, '2013-06-04 11:03:42.391862', '28283@tetsuo.schibsted.cl', 'TRANS_OK', '2013-06-04 11:03:41.971196', NULL, 'cmd:admail
commit:1
ad_id:26
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue VALUES (9, '2013-06-04 11:03:23.774369', '28283@tetsuo.schibsted.cl', NULL, '2013-06-04 11:03:42.391664', '28283@tetsuo.schibsted.cl', 'TRANS_OK', '2013-06-04 11:03:41.971196', NULL, 'cmd:admail
commit:1
ad_id:28
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue VALUES (5, '2013-06-04 10:52:19.034259', '28283@tetsuo.schibsted.cl', NULL, '2013-06-04 11:03:42.393116', '28283@tetsuo.schibsted.cl', 'TRANS_OK', '2013-06-04 11:03:41.971196', NULL, 'cmd:admail
commit:1
ad_id:24
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue VALUES (3, '2013-06-04 10:52:10.498464', '28283@tetsuo.schibsted.cl', NULL, '2013-06-04 11:03:42.392058', '28283@tetsuo.schibsted.cl', 'TRANS_OK', '2013-06-04 11:03:41.971196', NULL, 'cmd:admail
commit:1
ad_id:22
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue VALUES (2, '2013-06-04 10:52:03.073485', '28283@tetsuo.schibsted.cl', NULL, '2013-06-04 11:03:42.392396', '28283@tetsuo.schibsted.cl', 'TRANS_OK', '2013-06-04 11:03:41.971196', NULL, 'cmd:admail
commit:1
ad_id:21
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue VALUES (4, '2013-06-04 10:52:12.986911', '28283@tetsuo.schibsted.cl', NULL, '2013-06-04 11:03:42.392384', '28283@tetsuo.schibsted.cl', 'TRANS_OK', '2013-06-04 11:03:41.971196', NULL, 'cmd:admail
commit:1
ad_id:23
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue VALUES (1, '2013-06-04 10:51:58.859182', '28283@tetsuo.schibsted.cl', NULL, '2013-06-04 11:03:42.443193', '28283@tetsuo.schibsted.cl', 'TRANS_OK', '2013-06-04 11:03:42.394938', NULL, 'cmd:admail
commit:1
ad_id:20
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue VALUES (20, '2013-06-04 11:40:09.298005', '28283@tetsuo.schibsted.cl', NULL, '2013-06-04 11:40:19.78124', '28283@tetsuo.schibsted.cl', 'TRANS_OK', '2013-06-04 11:40:19.431719', NULL, 'cmd:admail
commit:1
ad_id:34
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue VALUES (19, '2013-06-04 11:40:05.446878', '28283@tetsuo.schibsted.cl', NULL, '2013-06-04 11:40:19.783043', '28283@tetsuo.schibsted.cl', 'TRANS_OK', '2013-06-04 11:40:19.431719', NULL, 'cmd:admail
commit:1
ad_id:33
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue VALUES (14, '2013-06-04 11:39:37.384598', '28283@tetsuo.schibsted.cl', NULL, '2013-06-04 11:40:19.790829', '28283@tetsuo.schibsted.cl', 'TRANS_OK', '2013-06-04 11:40:19.431719', NULL, 'cmd:admail
commit:1
ad_id:35
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue VALUES (17, '2013-06-04 11:39:54.98438', '28283@tetsuo.schibsted.cl', NULL, '2013-06-04 11:40:19.790879', '28283@tetsuo.schibsted.cl', 'TRANS_OK', '2013-06-04 11:40:19.431719', NULL, 'cmd:admail
commit:1
ad_id:37
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue VALUES (18, '2013-06-04 11:39:58.63201', '28283@tetsuo.schibsted.cl', NULL, '2013-06-04 11:40:19.791259', '28283@tetsuo.schibsted.cl', 'TRANS_OK', '2013-06-04 11:40:19.431719', NULL, 'cmd:admail
commit:1
ad_id:38
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue VALUES (15, '2013-06-04 11:39:40.620251', '28283@tetsuo.schibsted.cl', NULL, '2013-06-04 11:40:19.791184', '28283@tetsuo.schibsted.cl', 'TRANS_OK', '2013-06-04 11:40:19.431719', NULL, 'cmd:admail
commit:1
ad_id:36
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue VALUES (13, '2013-06-04 11:39:29.813566', '28283@tetsuo.schibsted.cl', NULL, '2013-06-04 11:40:19.791346', '28283@tetsuo.schibsted.cl', 'TRANS_OK', '2013-06-04 11:40:19.431719', NULL, 'cmd:admail
commit:1
ad_id:32
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue VALUES (16, '2013-06-04 11:39:47.168878', '28283@tetsuo.schibsted.cl', NULL, '2013-06-04 11:40:19.791513', '28283@tetsuo.schibsted.cl', 'TRANS_OK', '2013-06-04 11:40:19.431719', NULL, 'cmd:admail
commit:1
ad_id:39
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue VALUES (12, '2013-06-04 11:39:25.191882', '28283@tetsuo.schibsted.cl', NULL, '2013-06-04 11:40:19.792021', '28283@tetsuo.schibsted.cl', 'TRANS_OK', '2013-06-04 11:40:19.431719', NULL, 'cmd:admail
commit:1
ad_id:31
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue VALUES (34, '2013-06-04 11:59:53.367729', '28283@tetsuo.schibsted.cl', NULL, '2013-06-04 12:00:42.4401', '28283@tetsuo.schibsted.cl', 'TRANS_OK', '2013-06-04 12:00:42.184247', NULL, 'cmd:admail
commit:1
ad_id:53
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue VALUES (37, '2013-06-04 12:00:17.588815', '28283@tetsuo.schibsted.cl', NULL, '2013-06-04 12:00:42.439674', '28283@tetsuo.schibsted.cl', 'TRANS_OK', '2013-06-04 12:00:42.184247', NULL, 'cmd:admail
commit:1
ad_id:56
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue VALUES (33, '2013-06-04 11:59:50.039119', '28283@tetsuo.schibsted.cl', NULL, '2013-06-04 12:00:42.439919', '28283@tetsuo.schibsted.cl', 'TRANS_OK', '2013-06-04 12:00:42.184247', NULL, 'cmd:admail
commit:1
ad_id:52
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue VALUES (35, '2013-06-04 11:59:58.808391', '28283@tetsuo.schibsted.cl', NULL, '2013-06-04 12:00:42.440968', '28283@tetsuo.schibsted.cl', 'TRANS_OK', '2013-06-04 12:00:42.184247', NULL, 'cmd:admail
commit:1
ad_id:54
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue VALUES (31, '2013-06-04 11:59:43.227599', '28283@tetsuo.schibsted.cl', NULL, '2013-06-04 12:00:42.441086', '28283@tetsuo.schibsted.cl', 'TRANS_OK', '2013-06-04 12:00:42.184247', NULL, 'cmd:admail
commit:1
ad_id:50
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue VALUES (32, '2013-06-04 11:59:46.014044', '28283@tetsuo.schibsted.cl', NULL, '2013-06-04 12:00:42.441685', '28283@tetsuo.schibsted.cl', 'TRANS_OK', '2013-06-04 12:00:42.184247', NULL, 'cmd:admail
commit:1
ad_id:51
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue VALUES (36, '2013-06-04 12:00:08.527502', '28283@tetsuo.schibsted.cl', NULL, '2013-06-04 12:00:42.441831', '28283@tetsuo.schibsted.cl', 'TRANS_OK', '2013-06-04 12:00:42.184247', NULL, 'cmd:admail
commit:1
ad_id:55
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue VALUES (38, '2013-06-04 12:00:33.278553', '28283@tetsuo.schibsted.cl', NULL, '2013-06-04 12:00:42.442148', '28283@tetsuo.schibsted.cl', 'TRANS_OK', '2013-06-04 12:00:42.184247', NULL, 'cmd:admail
commit:1
ad_id:57
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue VALUES (22, '2013-06-04 11:58:51.906999', '28283@tetsuo.schibsted.cl', NULL, '2013-06-04 12:00:42.162379', '28283@tetsuo.schibsted.cl', 'TRANS_OK', '2013-06-04 12:00:41.781527', NULL, 'cmd:admail
commit:1
ad_id:41
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue VALUES (21, '2013-06-04 11:58:48.06587', '28283@tetsuo.schibsted.cl', NULL, '2013-06-04 12:00:42.169465', '28283@tetsuo.schibsted.cl', 'TRANS_OK', '2013-06-04 12:00:41.781527', NULL, 'cmd:admail
commit:1
ad_id:40
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue VALUES (24, '2013-06-04 11:59:05.851378', '28283@tetsuo.schibsted.cl', NULL, '2013-06-04 12:00:42.173273', '28283@tetsuo.schibsted.cl', 'TRANS_OK', '2013-06-04 12:00:41.781527', NULL, 'cmd:admail
commit:1
ad_id:43
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue VALUES (28, '2013-06-04 11:59:26.344877', '28283@tetsuo.schibsted.cl', NULL, '2013-06-04 12:00:42.180853', '28283@tetsuo.schibsted.cl', 'TRANS_OK', '2013-06-04 12:00:41.781527', NULL, 'cmd:admail
commit:1
ad_id:47
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue VALUES (23, '2013-06-04 11:59:01.648975', '28283@tetsuo.schibsted.cl', NULL, '2013-06-04 12:00:42.181229', '28283@tetsuo.schibsted.cl', 'TRANS_OK', '2013-06-04 12:00:41.781527', NULL, 'cmd:admail
commit:1
ad_id:42
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue VALUES (25, '2013-06-04 11:59:12.344641', '28283@tetsuo.schibsted.cl', NULL, '2013-06-04 12:00:42.181561', '28283@tetsuo.schibsted.cl', 'TRANS_OK', '2013-06-04 12:00:41.781527', NULL, 'cmd:admail
commit:1
ad_id:44
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue VALUES (26, '2013-06-04 11:59:17.740175', '28283@tetsuo.schibsted.cl', NULL, '2013-06-04 12:00:42.181825', '28283@tetsuo.schibsted.cl', 'TRANS_OK', '2013-06-04 12:00:41.781527', NULL, 'cmd:admail
commit:1
ad_id:45
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue VALUES (30, '2013-06-04 11:59:35.457738', '28283@tetsuo.schibsted.cl', NULL, '2013-06-04 12:00:42.182106', '28283@tetsuo.schibsted.cl', 'TRANS_OK', '2013-06-04 12:00:41.781527', NULL, 'cmd:admail
commit:1
ad_id:49
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue VALUES (27, '2013-06-04 11:59:23.719844', '28283@tetsuo.schibsted.cl', NULL, '2013-06-04 12:00:42.183114', '28283@tetsuo.schibsted.cl', 'TRANS_OK', '2013-06-04 12:00:41.781527', NULL, 'cmd:admail
commit:1
ad_id:46
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue VALUES (29, '2013-06-04 11:59:31.162302', '28283@tetsuo.schibsted.cl', NULL, '2013-06-04 12:00:42.183969', '28283@tetsuo.schibsted.cl', 'TRANS_OK', '2013-06-04 12:00:41.781527', NULL, 'cmd:admail
commit:1
ad_id:48
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue VALUES (39, '2013-06-04 12:11:18.719295', '28283@tetsuo.schibsted.cl', NULL, '2013-06-04 12:37:08.635032', '28283@tetsuo.schibsted.cl', 'TRANS_OK', '2013-06-04 12:37:08.483961', NULL, 'cmd:admail
commit:1
ad_id:58
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue VALUES (41, '2013-06-04 12:11:26.707498', '28283@tetsuo.schibsted.cl', NULL, '2013-06-04 12:37:08.990889', '28283@tetsuo.schibsted.cl', 'TRANS_OK', '2013-06-04 12:37:08.483961', NULL, 'cmd:admail
commit:1
ad_id:60
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue VALUES (44, '2013-06-04 12:20:30.982239', '28283@tetsuo.schibsted.cl', NULL, '2013-06-04 12:37:08.993595', '28283@tetsuo.schibsted.cl', 'TRANS_OK', '2013-06-04 12:37:08.483961', NULL, 'cmd:admail
commit:1
ad_id:63
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue VALUES (45, '2013-06-04 12:20:34.132535', '28283@tetsuo.schibsted.cl', NULL, '2013-06-04 12:37:09.002955', '28283@tetsuo.schibsted.cl', 'TRANS_OK', '2013-06-04 12:37:08.483961', NULL, 'cmd:admail
commit:1
ad_id:64
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue VALUES (43, '2013-06-04 12:11:34.830497', '28283@tetsuo.schibsted.cl', NULL, '2013-06-04 12:37:09.003562', '28283@tetsuo.schibsted.cl', 'TRANS_OK', '2013-06-04 12:37:08.483961', NULL, 'cmd:admail
commit:1
ad_id:62
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue VALUES (47, '2013-06-04 12:20:42.04459', '28283@tetsuo.schibsted.cl', NULL, '2013-06-04 12:37:09.003767', '28283@tetsuo.schibsted.cl', 'TRANS_OK', '2013-06-04 12:37:08.483961', NULL, 'cmd:admail
commit:1
ad_id:66
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue VALUES (40, '2013-06-04 12:11:21.885225', '28283@tetsuo.schibsted.cl', NULL, '2013-06-04 12:37:09.00509', '28283@tetsuo.schibsted.cl', 'TRANS_OK', '2013-06-04 12:37:08.483961', NULL, 'cmd:admail
commit:1
ad_id:59
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue VALUES (42, '2013-06-04 12:11:30.182051', '28283@tetsuo.schibsted.cl', NULL, '2013-06-04 12:37:09.00528', '28283@tetsuo.schibsted.cl', 'TRANS_OK', '2013-06-04 12:37:08.483961', NULL, 'cmd:admail
commit:1
ad_id:61
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue VALUES (46, '2013-06-04 12:20:39.111385', '28283@tetsuo.schibsted.cl', NULL, '2013-06-04 12:37:09.005654', '28283@tetsuo.schibsted.cl', 'TRANS_OK', '2013-06-04 12:37:08.483961', NULL, 'cmd:admail
commit:1
ad_id:65
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue VALUES (48, '2013-06-04 12:20:46.758953', '28283@tetsuo.schibsted.cl', NULL, '2013-06-04 12:37:09.007507', '28283@tetsuo.schibsted.cl', 'TRANS_OK', '2013-06-04 12:37:08.483961', NULL, 'cmd:admail
commit:1
ad_id:67
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue VALUES (51, '2013-06-04 12:32:14.402225', '28283@tetsuo.schibsted.cl', NULL, '2013-06-04 12:37:09.401672', '28283@tetsuo.schibsted.cl', 'TRANS_OK', '2013-06-04 12:37:09.008152', NULL, 'cmd:admail
commit:1
ad_id:70
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue VALUES (52, '2013-06-04 12:32:17.849735', '28283@tetsuo.schibsted.cl', NULL, '2013-06-04 12:37:09.402242', '28283@tetsuo.schibsted.cl', 'TRANS_OK', '2013-06-04 12:37:09.008152', NULL, 'cmd:admail
commit:1
ad_id:71
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue VALUES (50, '2013-06-04 12:20:54.284227', '28283@tetsuo.schibsted.cl', NULL, '2013-06-04 12:37:09.402669', '28283@tetsuo.schibsted.cl', 'TRANS_OK', '2013-06-04 12:37:09.008152', NULL, 'cmd:admail
commit:1
ad_id:69
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue VALUES (49, '2013-06-04 12:20:49.007317', '28283@tetsuo.schibsted.cl', NULL, '2013-06-04 12:37:09.404101', '28283@tetsuo.schibsted.cl', 'TRANS_OK', '2013-06-04 12:37:09.008152', NULL, 'cmd:admail
commit:1
ad_id:68
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue VALUES (60, '2013-06-04 12:32:42.239929', '28283@tetsuo.schibsted.cl', NULL, '2013-06-04 12:37:09.637161', '28283@tetsuo.schibsted.cl', 'TRANS_OK', '2013-06-04 12:37:09.40667', NULL, 'cmd:admail
commit:1
ad_id:79
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue VALUES (61, '2013-06-04 12:32:45.693456', '28283@tetsuo.schibsted.cl', NULL, '2013-06-04 12:37:09.637691', '28283@tetsuo.schibsted.cl', 'TRANS_OK', '2013-06-04 12:37:09.40667', NULL, 'cmd:admail
commit:1
ad_id:80
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue VALUES (59, '2013-06-04 12:32:40.756658', '28283@tetsuo.schibsted.cl', NULL, '2013-06-04 12:37:09.638363', '28283@tetsuo.schibsted.cl', 'TRANS_OK', '2013-06-04 12:37:09.40667', NULL, 'cmd:admail
commit:1
ad_id:78
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue VALUES (63, '2013-06-04 12:32:52.563308', '28283@tetsuo.schibsted.cl', NULL, '2013-06-04 12:37:09.639485', '28283@tetsuo.schibsted.cl', 'TRANS_OK', '2013-06-04 12:37:09.40667', NULL, 'cmd:admail
commit:1
ad_id:82
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue VALUES (62, '2013-06-04 12:32:47.540445', '28283@tetsuo.schibsted.cl', NULL, '2013-06-04 12:37:09.639744', '28283@tetsuo.schibsted.cl', 'TRANS_OK', '2013-06-04 12:37:09.40667', NULL, 'cmd:admail
commit:1
ad_id:81
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue VALUES (58, '2013-06-04 12:32:35.538299', '28283@tetsuo.schibsted.cl', NULL, '2013-06-04 12:37:09.401401', '28283@tetsuo.schibsted.cl', 'TRANS_OK', '2013-06-04 12:37:09.008152', NULL, 'cmd:admail
commit:1
ad_id:77
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue VALUES (53, '2013-06-04 12:32:22.169242', '28283@tetsuo.schibsted.cl', NULL, '2013-06-04 12:37:09.402409', '28283@tetsuo.schibsted.cl', 'TRANS_OK', '2013-06-04 12:37:09.008152', NULL, 'cmd:admail
commit:1
ad_id:72
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue VALUES (54, '2013-06-04 12:32:24.022459', '28283@tetsuo.schibsted.cl', NULL, '2013-06-04 12:37:09.402925', '28283@tetsuo.schibsted.cl', 'TRANS_OK', '2013-06-04 12:37:09.008152', NULL, 'cmd:admail
commit:1
ad_id:73
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue VALUES (55, '2013-06-04 12:32:28.335622', '28283@tetsuo.schibsted.cl', NULL, '2013-06-04 12:37:09.403599', '28283@tetsuo.schibsted.cl', 'TRANS_OK', '2013-06-04 12:37:09.008152', NULL, 'cmd:admail
commit:1
ad_id:74
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue VALUES (56, '2013-06-04 12:32:29.776524', '28283@tetsuo.schibsted.cl', NULL, '2013-06-04 12:37:09.404358', '28283@tetsuo.schibsted.cl', 'TRANS_OK', '2013-06-04 12:37:09.008152', NULL, 'cmd:admail
commit:1
ad_id:75
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue VALUES (57, '2013-06-04 12:32:33.490975', '28283@tetsuo.schibsted.cl', NULL, '2013-06-04 12:37:09.405767', '28283@tetsuo.schibsted.cl', 'TRANS_OK', '2013-06-04 12:37:09.008152', NULL, 'cmd:admail
commit:1
ad_id:76
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue VALUES (64, '2013-08-06 15:19:30.60062', '2459@tetsuo.schibsted.cl', NULL, NULL, NULL, NULL, NULL, NULL, 'cmd:admail
commit:1
ad_id:83
action_id:1
mail_type:accept
', NULL, 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue VALUES (65, '2013-08-06 15:19:34.910481', '2459@tetsuo.schibsted.cl', NULL, NULL, NULL, NULL, NULL, NULL, 'cmd:admail
commit:1
ad_id:84
action_id:1
mail_type:accept
', NULL, 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue VALUES (66, '2013-08-06 15:35:32.689652', '31473@tetsuo.schibsted.cl', '2013-08-06 16:05:32.689652', NULL, NULL, NULL, NULL, NULL, 'cmd:review
commit:1
ad_id:85
action_id:1
action:accept
filter_name:whitelist
remote_addr:10.0.1.175

', NULL, 'AT', 'review', '85');
INSERT INTO trans_queue VALUES (67, '2013-08-06 15:36:33.09665', '31473@tetsuo.schibsted.cl', '2013-08-06 16:06:33.09665', NULL, NULL, NULL, NULL, NULL, 'cmd:review
commit:1
ad_id:86
action_id:1
action:accept
filter_name:whitelist
remote_addr:10.0.1.175

', NULL, 'AT', 'review', '86');
INSERT INTO trans_queue VALUES (68, '2013-08-06 15:37:24.995712', '31473@tetsuo.schibsted.cl', '2013-08-06 16:07:24.995712', NULL, NULL, NULL, NULL, NULL, 'cmd:review
commit:1
ad_id:88
action_id:1
action:accept
filter_name:whitelist
remote_addr:10.0.1.175

', NULL, 'AT', 'review', '88');
INSERT INTO trans_queue VALUES (69, '2013-08-06 15:37:35.34124', '31473@tetsuo.schibsted.cl', '2013-08-06 16:07:35.34124', NULL, NULL, NULL, NULL, NULL, 'cmd:review
commit:1
ad_id:89
action_id:1
action:accept
filter_name:whitelist
remote_addr:10.0.1.17

', NULL, 'AT', 'review', '89');
INSERT INTO trans_queue VALUES (70, '2013-08-06 15:38:19.48921', '31473@tetsuo.schibsted.cl', '2013-08-06 16:08:19.48921', NULL, NULL, NULL, NULL, NULL, 'cmd:review
commit:1
ad_id:90
action_id:1
action:accept
filter_name:whitelist
remote_addr:10.0.1.175

', NULL, 'AT', 'review', '90');
INSERT INTO trans_queue VALUES (71, '2013-08-06 15:38:39.706377', '31473@tetsuo.schibsted.cl', '2013-08-06 16:08:39.706377', NULL, NULL, NULL, NULL, NULL, 'cmd:review
commit:1
ad_id:91
action_id:1
action:accept
filter_name:whitelist
remote_addr:10.0.1.17

', NULL, 'AT', 'review', '91');
INSERT INTO trans_queue VALUES (72, '2013-08-06 15:39:21.699377', '31473@tetsuo.schibsted.cl', '2013-08-06 16:09:21.699377', NULL, NULL, NULL, NULL, NULL, 'cmd:review
commit:1
ad_id:93
action_id:1
action:accept
filter_name:whitelist
remote_addr:10.0.1.17

', NULL, 'AT', 'review', '93');


--
-- Data for Name: unfinished_ads; Type: TABLE DATA; Schema: public; Owner: dany
--



--
-- Data for Name: user_params; Type: TABLE DATA; Schema: public; Owner: dany
--

INSERT INTO user_params VALUES (50, 'first_approved_ad', '2013-06-04 10:51:58.847484-04');
INSERT INTO user_params VALUES (51, 'first_approved_ad', '2013-08-06 15:19:30.588446-04');
INSERT INTO user_params VALUES (52, 'first_approved_ad', '2013-08-06 15:19:34.894397-04');

INSERT INTO user_params VALUES (50, 'first_inserted_ad', '2013-06-04 10:51:58.847484-04');
INSERT INTO user_params VALUES (51, 'first_inserted_ad', '2013-08-06 15:19:30.588446-04');
INSERT INTO user_params VALUES (52, 'first_inserted_ad', '2013-08-06 15:19:34.894397-04');

--
-- Data for Name: user_testimonial; Type: TABLE DATA; Schema: public; Owner: dany
--



--
-- Data for Name: visitor; Type: TABLE DATA; Schema: public; Owner: dany
--

INSERT INTO visitor VALUES (1, '2013-06-04');
INSERT INTO visitor VALUES (139, '2013-06-04');


--
-- Data for Name: visits; Type: TABLE DATA; Schema: public; Owner: dany
--

INSERT INTO visits VALUES (1, '2013-06-04', 47, 22, 22, 85, 0, 0);
INSERT INTO visits VALUES (139, '2013-06-04', 54, 35, 35, 143, 0, 0);
INSERT INTO visits VALUES (1, '2013-08-06', 14, 10, 10, 29, 0, 0);


--
-- Data for Name: vouchers; Type: TABLE DATA; Schema: public; Owner: dany
--



--
-- Data for Name: voucher_actions; Type: TABLE DATA; Schema: public; Owner: dany
--



--
-- Data for Name: voucher_states; Type: TABLE DATA; Schema: public; Owner: dany
--



--
-- Data for Name: watch_ads; Type: TABLE DATA; Schema: public; Owner: dany
--

--
-- Data for Name: Accounts; Type: TABLE DATA; Schema: public; Owner: Cristi�n
--

INSERT INTO accounts (
	name,
	rut,
	email,
	phone,
	is_company,
	region,
	salted_passwd,
	creation_date,
	status,user_id) 
VALUES ('BLOCKET',
	'100000-4',
	'prepaid5@blocket.se',
	'1234567',
	't',
	15,
	'$1024$KpIwK8ku24RaCFzC23c152bd74d58e20c45a9afaa112b85684e4c9a3',
	now(),
	'active',
	5);

--
-- PostgreSQL database dump complete
--

INSERT INTO ad_params VALUES (2, 'communes', '234');
INSERT INTO ad_params VALUES (3, 'communes', '243');
INSERT INTO ad_params VALUES (4, 'communes', '243');
INSERT INTO ad_params VALUES (5, 'communes', '240');
INSERT INTO ad_params VALUES (6, 'communes', '226');
INSERT INTO ad_params VALUES (7, 'communes', '238');
INSERT INTO ad_params VALUES (8, 'communes', '239');
INSERT INTO ad_params VALUES (9, 'communes', '238');
INSERT INTO ad_params VALUES (10, 'communes', '258');
INSERT INTO ad_params VALUES (11, 'communes', '276');
INSERT INTO ad_params VALUES (47, 'communes', '322');
INSERT INTO ad_params VALUES (81, 'communes', '331');
INSERT INTO ad_params VALUES (36, 'communes', '338');
INSERT INTO ad_params VALUES (35, 'communes', '327');
INSERT INTO ad_params VALUES (37, 'communes', '340');
INSERT INTO ad_params VALUES (39, 'communes', '322');
INSERT INTO ad_params VALUES (38, 'communes', '338');
INSERT INTO ad_params VALUES (34, 'communes', '332');
INSERT INTO ad_params VALUES (40, 'communes', '335');
INSERT INTO ad_params VALUES (42, 'communes', '305');
INSERT INTO ad_params VALUES (44, 'communes', '326');
INSERT INTO ad_params VALUES (45, 'communes', '309');
INSERT INTO ad_params VALUES (46, 'communes', '328');
INSERT INTO ad_params VALUES (49, 'communes', '329');
INSERT INTO ad_params VALUES (48, 'communes', '308');
INSERT INTO ad_params VALUES (50, 'communes', '315');
INSERT INTO ad_params VALUES (51, 'communes', '334');
INSERT INTO ad_params VALUES (52, 'communes', '335');
INSERT INTO ad_params VALUES (54, 'communes', '306');
INSERT INTO ad_params VALUES (53, 'communes', '332');
INSERT INTO ad_params VALUES (57, 'communes', '346');
INSERT INTO ad_params VALUES (56, 'communes', '333');
INSERT INTO ad_params VALUES (41, 'communes', '336');
INSERT INTO ad_params VALUES (43, 'communes', '346');
INSERT INTO ad_params VALUES (55, 'communes', '314');
INSERT INTO ad_params VALUES (58, 'communes', '300');
INSERT INTO ad_params VALUES (59, 'communes', '314');
INSERT INTO ad_params VALUES (60, 'communes', '329');
INSERT INTO ad_params VALUES (61, 'communes', '307');
INSERT INTO ad_params VALUES (65, 'communes', '332');
INSERT INTO ad_params VALUES (62, 'communes', '314');
INSERT INTO ad_params VALUES (64, 'communes', '335');
INSERT INTO ad_params VALUES (67, 'communes', '339');
INSERT INTO ad_params VALUES (66, 'communes', '329');
INSERT INTO ad_params VALUES (68, 'communes', '308');
INSERT INTO ad_params VALUES (76, 'communes', '295');
INSERT INTO ad_params VALUES (69, 'communes', '313');
INSERT INTO ad_params VALUES (73, 'communes', '297');
INSERT INTO ad_params VALUES (74, 'communes', '311');
INSERT INTO ad_params VALUES (75, 'communes', '299');
INSERT INTO ad_params VALUES (63, 'communes', '323');
INSERT INTO ad_params VALUES (82, 'communes', '298');
INSERT INTO ad_params VALUES (70, 'communes', '322');
INSERT INTO ad_params VALUES (79, 'communes', '297');
INSERT INTO ad_params VALUES (71, 'communes', '299');
INSERT INTO ad_params VALUES (72, 'communes', '301');
INSERT INTO ad_params VALUES (80, 'communes', '336');
INSERT INTO ad_params VALUES (77, 'communes', '317');
INSERT INTO ad_params VALUES (78, 'communes', '323');
INSERT INTO ad_params VALUES (83, 'communes', '336');
INSERT INTO ad_params VALUES (84, 'communes', '334');
INSERT INTO ad_params VALUES (85, 'communes', '316');
INSERT INTO ad_params VALUES (86, 'communes', '321');
INSERT INTO ad_params VALUES (87, 'communes', '342');
INSERT INTO ad_params VALUES (88, 'communes', '346');
INSERT INTO ad_params VALUES (89, 'communes', '297');

--Change all passwords to 123123123
update ads set salted_passwd =  E'$1024$2y1bQ}VYRD}zF4QCbf07a971b784888658a20e62156de64dfde5ffb6', passwd = null;
update accounts set salted_passwd =  E'$1024$2y1bQ}VYRD}zF4QCbf07a971b784888658a20e62156de64dfde5ffb6';
