
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (166, '12345', 'paid', '2014-12-10 15:21:47.798026', NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (167, '65432', 'paid', '2014-12-10 15:22:46.772587', NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (168, '32323', 'paid', '2014-12-10 15:22:46.772587', 167);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (169, '54545', 'paid', '2014-12-10 15:22:46.772587', 167);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (170, '90001', 'paid', '2014-12-10 15:22:46.772587', 167);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (171, '90002', 'paid', '2014-12-10 15:23:59.27987', NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (172, '90003', 'paid', '2014-12-10 17:57:04.033434', NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (173, '90004', 'paid', '2014-12-10 17:57:28.873977', NULL);


INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (6, 2, 'gallery', 704, 'accepted', 'normal', NULL, NULL, 168);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (1, 2, 'daily_bump', 707, 'accepted', 'normal', NULL, NULL, 169);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (1, 3, 'bump', 709, 'accepted', 'normal', NULL, NULL, NULL);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (10, 2, 'daily_bump', 710, 'accepted', 'normal', NULL, NULL, 170);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (10, 3, 'bump', 712, 'accepted', 'normal', NULL, NULL, NULL);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (35, 2, 'bump', 716, 'accepted', 'normal', NULL, NULL, 171);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (120, 2, 'bump', 720, 'accepted', 'normal', NULL, NULL, 172);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (95, 2, 'bump', 724, 'accepted', 'normal', NULL, NULL, 173);


INSERT INTO stores (store_id, name, phone, address, info_text, status, date_start, date_end, region, url, hide_phone, commune, address_number, website_url, main_image, main_image_offset, logo_image, account_id) VALUES (1, NULL, NULL, NULL, NULL, 'inactive', '2014-12-10 15:21:47.777779', '2014-12-10 15:31:51.993411', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1);


INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (715, 'X5488b3ce66912d1b000000004038dd5600000000', 9, '2014-12-10 17:57:50.236517', '2014-12-10 17:57:50.41811', '10.0.1.101', 'authenticate', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (716, 'X5488b3ce702711ab000000006cc3d2fa00000000', 9, '2014-12-10 17:57:50.41811', '2014-12-10 17:58:05.581445', '10.0.1.101', 'spamfilter', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (717, 'X5488b3de669e9a230000000036412e4f00000000', 9, '2014-12-10 17:58:05.581445', '2014-12-10 18:58:05.581445', '10.0.1.101', 'Websql', NULL, NULL);


INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (6, 2, 698, 'reg', 'initial', '2014-12-10 15:22:46.772587', '10.0.1.101', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (6, 2, 699, 'unpaid', 'pending_pay', '2014-12-10 15:22:46.772587', '10.0.1.101', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (1, 2, 700, 'reg', 'initial', '2014-12-10 15:22:46.772587', '10.0.1.101', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (1, 2, 701, 'unpaid', 'pending_pay', '2014-12-10 15:22:46.772587', '10.0.1.101', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (10, 2, 702, 'reg', 'initial', '2014-12-10 15:22:46.772587', '10.0.1.101', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (10, 2, 703, 'unpaid', 'pending_pay', '2014-12-10 15:22:46.772587', '10.0.1.101', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (6, 2, 704, 'accepted', 'pay', '2014-12-10 15:22:53.136383', NULL, NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (1, 2, 705, 'accepted', 'pay', '2014-12-10 15:22:53.192537', NULL, NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (10, 2, 706, 'accepted', 'pay', '2014-12-10 15:22:53.215293', NULL, NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (1, 2, 707, 'accepted', 'updating_counter', '2014-12-10 15:22:53.320069', NULL, NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (1, 3, 708, 'reg', 'initial', '2014-12-10 15:22:53.332434', NULL, NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (1, 3, 709, 'accepted', 'bump', '2014-12-10 15:22:53.332434', NULL, NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (10, 2, 710, 'accepted', 'updating_counter', '2014-12-10 15:22:53.336385', NULL, NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (10, 3, 711, 'reg', 'initial', '2014-12-10 15:22:53.365766', NULL, NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (10, 3, 712, 'accepted', 'bump', '2014-12-10 15:22:53.365766', NULL, NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (35, 2, 713, 'reg', 'initial', '2014-12-10 15:23:59.27987', '10.0.1.101', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (35, 2, 714, 'unpaid', 'pending_pay', '2014-12-10 15:23:59.27987', '10.0.1.101', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (35, 2, 715, 'pending_bump', 'pay', '2014-12-10 15:24:01.857847', NULL, NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (35, 2, 716, 'accepted', 'bump', '2014-12-10 15:24:02.017436', NULL, NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (120, 2, 717, 'reg', 'initial', '2014-12-10 17:57:04.033434', '10.0.1.101', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (120, 2, 718, 'unpaid', 'pending_pay', '2014-12-10 17:57:04.033434', '10.0.1.101', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (120, 2, 719, 'pending_bump', 'pay', '2014-12-10 17:57:06.897036', NULL, NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (120, 2, 720, 'accepted', 'bump', '2014-12-10 17:57:07.018399', NULL, NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (95, 2, 721, 'reg', 'initial', '2014-12-10 17:57:28.873977', '10.0.1.101', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (95, 2, 722, 'unpaid', 'pending_pay', '2014-12-10 17:57:28.873977', '10.0.1.101', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (95, 2, 723, 'pending_bump', 'pay', '2014-12-10 17:57:31.014861', NULL, NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (95, 2, 724, 'accepted', 'bump', '2014-12-10 17:57:31.093404', NULL, NULL);



SELECT pg_catalog.setval('action_states_state_id_seq', 724, true);

INSERT INTO ad_changes (ad_id, action_id, state_id, is_param, column_name, old_value, new_value) VALUES (1, 2, 707, true, 'daily_bump', NULL, '6');
INSERT INTO ad_changes (ad_id, action_id, state_id, is_param, column_name, old_value, new_value) VALUES (10, 2, 710, true, 'daily_bump', NULL, '6');
INSERT INTO ad_changes (ad_id, action_id, state_id, is_param, column_name, old_value, new_value) VALUES (6, 2, 704, true, 'gallery', NULL, '2014-12-10 15:32:53');





INSERT INTO ad_params (ad_id, name, value) VALUES (1, 'daily_bump', '6');
INSERT INTO ad_params (ad_id, name, value) VALUES (10, 'daily_bump', '6');
INSERT INTO ad_params (ad_id, name, value) VALUES (6, 'gallery', '2014-12-10 15:32:53');


INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (213, 'save', 0, '12345', NULL, '2014-12-10 15:21:48', 166, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (214, 'paycard_save', 0, '12345', NULL, '2014-12-10 15:21:52', 166, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (215, 'save', 0, '32323', NULL, '2014-12-10 15:22:47', 168, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (216, 'save', 0, '54545', NULL, '2014-12-10 15:22:47', 169, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (217, 'save', 0, '90001', NULL, '2014-12-10 15:22:47', 170, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (218, 'paycard_save', 0, '65432', NULL, '2014-12-10 15:22:53', 167, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (219, 'paycard', 6400, '00168a', NULL, '2014-12-10 15:22:53', 168, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (220, 'paycard', 11500, '00169a', NULL, '2014-12-10 15:22:53', 169, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (221, 'paycard', 4500, '00170a', NULL, '2014-12-10 15:22:53', 170, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (222, 'paycard', 22400, '65432', NULL, '2014-12-10 15:22:53', 167, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (223, 'save', 0, '90002', NULL, '2014-12-10 15:23:59', 171, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (224, 'paycard_save', 0, '90002', NULL, '2014-12-10 15:24:02', 171, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (225, 'paycard', 1600, '00171a', NULL, '2014-12-10 15:24:02', 171, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (226, 'save', 0, '90003', NULL, '2014-12-10 17:57:04', 172, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (227, 'paycard_save', 0, '90003', NULL, '2014-12-10 17:57:07', 172, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (228, 'paycard', 900, '00172a', NULL, '2014-12-10 17:57:07', 172, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (229, 'save', 0, '90004', NULL, '2014-12-10 17:57:29', 173, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (230, 'paycard_save', 0, '90004', NULL, '2014-12-10 17:57:31', 173, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (231, 'paycard', 900, '00173a', NULL, '2014-12-10 17:57:31', 173, 'OK');



SELECT pg_catalog.setval('pay_log_pay_log_id_seq', 231, true);


INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (213, 0, 'remote_addr', '10.0.1.101');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (214, 0, 'auth_code', '190567');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (214, 1, 'trans_date', '10/12/2014 15:21:49');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (214, 2, 'placements_type', 'VN');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (214, 3, 'placements_number', '1');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (214, 4, 'pay_type', 'credit');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (214, 5, 'external_response', '0');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (214, 6, 'external_id', '42961290505188248246');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (214, 7, 'remote_addr', '10.0.1.208');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (215, 0, 'remote_addr', '10.0.1.101');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (216, 0, 'remote_addr', '10.0.1.101');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (217, 0, 'remote_addr', '10.0.1.101');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (218, 0, 'auth_code', '');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (218, 1, 'trans_date', '');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (218, 2, 'placements_type', '');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (218, 3, 'placements_number', '');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (218, 4, 'pay_type', '');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (218, 5, 'external_response', '1');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (218, 6, 'external_id', '');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (218, 7, 'remote_addr', '0.0.0.0');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (219, 0, 'webpay', '1');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (220, 0, 'webpay', '1');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (221, 0, 'webpay', '1');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (223, 0, 'remote_addr', '10.0.1.101');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (224, 0, 'auth_code', '939626');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (224, 1, 'trans_date', '10/12/2014 15:24:01');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (224, 2, 'placements_type', 'VN');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (224, 3, 'placements_number', '1');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (224, 4, 'pay_type', 'credit');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (224, 5, 'external_response', '0');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (224, 6, 'external_id', '38380010159530436974');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (224, 7, 'remote_addr', '10.0.1.208');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (225, 0, 'webpay', '1');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (226, 0, 'remote_addr', '10.0.1.101');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (227, 0, 'auth_code', '671081');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (227, 1, 'trans_date', '10/12/2014 17:57:06');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (227, 2, 'placements_type', 'VN');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (227, 3, 'placements_number', '1');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (227, 4, 'pay_type', 'credit');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (227, 5, 'external_response', '0');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (227, 6, 'external_id', '43934143797025864838');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (227, 7, 'remote_addr', '10.0.1.208');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (228, 0, 'webpay', '1');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (229, 0, 'remote_addr', '10.0.1.101');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (230, 0, 'auth_code', '821172');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (230, 1, 'trans_date', '10/12/2014 17:57:30');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (230, 2, 'placements_type', 'VN');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (230, 3, 'placements_number', '1');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (230, 4, 'pay_type', 'credit');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (230, 5, 'external_response', '0');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (230, 6, 'external_id', '82342992919165564508');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (230, 7, 'remote_addr', '10.0.1.208');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (231, 0, 'webpay', '1');


SELECT pg_catalog.setval('payment_groups_payment_group_id_seq', 173, true);


INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (166, 'quarterly_store', 15000, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (168, 'gallery', 6400, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (169, 'daily_bump', 11500, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (170, 'daily_bump', 4500, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (171, 'bump', 1600, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (172, 'bump', 900, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (173, 'bump', 900, 0);



INSERT INTO purchase (purchase_id, doc_type, doc_num, external_doc_id, status, receipt, delivery, rut, name, lob, address, region, communes, contact, email, tax, discount, seller_id, total_price, payment_group_id, payment_method) VALUES (2, 'invoice', 1, 1001, 'confirmed', '2014-12-10 15:21:47.798026', NULL, '100000-4', 'BLOCKET', 'Technology', 'Alonso de C�rdova 5670', 15, 315, 'BLOCKET', 'prepaid5@blocket.se', 0, 0, NULL, 15000, 166, 'webpay');
INSERT INTO purchase (purchase_id, doc_type, doc_num, external_doc_id, status, receipt, delivery, rut, name, lob, address, region, communes, contact, email, tax, discount, seller_id, total_price, payment_group_id, payment_method) VALUES (3, 'invoice', 2, 1002, 'confirmed', '2014-12-10 15:22:46.772587', NULL, '100000-4', 'BLOCKET', 'Technology', 'Alonso de C�rdova 5670', 15, 315, 'BLOCKET', 'prepaid5@blocket.se', 0, 0, NULL, 22400, 167, 'servipag');
INSERT INTO purchase (purchase_id, doc_type, doc_num, external_doc_id, status, receipt, delivery, rut, name, lob, address, region, communes, contact, email, tax, discount, seller_id, total_price, payment_group_id, payment_method) VALUES (4, 'bill', 3, 1003, 'confirmed', '2014-12-10 15:23:59.27987', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'android@yapo.cl', 0, 0, NULL, 1600, 171, 'webpay');
INSERT INTO purchase (purchase_id, doc_type, doc_num, external_doc_id, status, receipt, delivery, rut, name, lob, address, region, communes, contact, email, tax, discount, seller_id, total_price, payment_group_id, payment_method) VALUES (5, 'bill', NULL, NULL, 'paid', '2014-12-10 17:57:04.033434', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'user.01@schibsted.cl', 0, 0, NULL, 900, 172, 'webpay');
INSERT INTO purchase (purchase_id, doc_type, doc_num, external_doc_id, status, receipt, delivery, rut, name, lob, address, region, communes, contact, email, tax, discount, seller_id, total_price, payment_group_id, payment_method) VALUES (6, 'bill', NULL, NULL, 'paid', '2014-12-10 17:57:28.873977', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'prepaid5@blocket.se', 0, 0, NULL, 900, 173, 'webpay');

INSERT INTO purchase_detail (purchase_detail_id, purchase_id, product_id, price, action_id, ad_id) VALUES (2, 2, 5, 15000, NULL, NULL);
INSERT INTO purchase_detail (purchase_detail_id, purchase_id, product_id, price, action_id, ad_id) VALUES (3, 3, 3, 6400, 2, 6);
INSERT INTO purchase_detail (purchase_detail_id, purchase_id, product_id, price, action_id, ad_id) VALUES (4, 3, 8, 11500, 2, 1);
INSERT INTO purchase_detail (purchase_detail_id, purchase_id, product_id, price, action_id, ad_id) VALUES (5, 3, 8, 4500, 2, 10);
INSERT INTO purchase_detail (purchase_detail_id, purchase_id, product_id, price, action_id, ad_id) VALUES (6, 4, 1, 1600, 2, 35);
INSERT INTO purchase_detail (purchase_detail_id, purchase_id, product_id, price, action_id, ad_id) VALUES (7, 5, 1, 900, 2, 120);
INSERT INTO purchase_detail (purchase_detail_id, purchase_id, product_id, price, action_id, ad_id) VALUES (8, 6, 1, 900, 2, 95);


SELECT pg_catalog.setval('purchase_detail_purchase_detail_id_seq', 8, true);

SELECT pg_catalog.setval('purchase_purchase_id_seq', 6, true);

INSERT INTO purchase_states (purchase_id, purchase_state_id, status, "timestamp", remote_addr) VALUES (2, 1, 'pending', '2014-12-10 15:21:47.798026', '10.0.1.101');
INSERT INTO purchase_states (purchase_id, purchase_state_id, status, "timestamp", remote_addr) VALUES (3, 2, 'pending', '2014-12-10 15:22:46.772587', '10.0.1.101');
INSERT INTO purchase_states (purchase_id, purchase_state_id, status, "timestamp", remote_addr) VALUES (4, 3, 'pending', '2014-12-10 15:23:59.27987', '10.0.1.101');
INSERT INTO purchase_states (purchase_id, purchase_state_id, status, "timestamp", remote_addr) VALUES (5, 4, 'pending', '2014-12-10 17:57:04.033434', '10.0.1.101');
INSERT INTO purchase_states (purchase_id, purchase_state_id, status, "timestamp", remote_addr) VALUES (6, 5, 'pending', '2014-12-10 17:57:28.873977', '10.0.1.101');

SELECT pg_catalog.setval('purchase_states_purchase_state_id_seq', 5, true);


INSERT INTO store_actions (store_id, action_id, action_type, current_state, state, payment_group_id) VALUES (1, 1, 'new', 1, 'reg', NULL);
INSERT INTO store_actions (store_id, action_id, action_type, current_state, state, payment_group_id) VALUES (1, 2, 'new', 2, 'unpaid', NULL);
INSERT INTO store_actions (store_id, action_id, action_type, current_state, state, payment_group_id) VALUES (1, 4, 'status_change', 5, 'accepted', NULL);
INSERT INTO store_actions (store_id, action_id, action_type, current_state, state, payment_group_id) VALUES (1, 3, 'extend', 7, 'accepted', 166);

INSERT INTO store_action_states (store_id, action_id, state_id, state, "timestamp", token_id) VALUES (1, 1, 1, 'reg', '2014-12-10 15:21:47.777779', NULL);
INSERT INTO store_action_states (store_id, action_id, state_id, state, "timestamp", token_id) VALUES (1, 2, 2, 'unpaid', '2014-12-10 15:21:47.777779', NULL);
INSERT INTO store_action_states (store_id, action_id, state_id, state, "timestamp", token_id) VALUES (1, 3, 3, 'unpaid', '2014-12-10 15:21:47.798026', NULL);
INSERT INTO store_action_states (store_id, action_id, state_id, state, "timestamp", token_id) VALUES (1, 4, 4, 'reg', '2014-12-10 15:21:51.993411', NULL);
INSERT INTO store_action_states (store_id, action_id, state_id, state, "timestamp", token_id) VALUES (1, 4, 5, 'accepted', '2014-12-10 15:21:51.993411', NULL);
INSERT INTO store_action_states (store_id, action_id, state_id, state, "timestamp", token_id) VALUES (1, 3, 6, 'paid', '2014-12-10 15:21:51.993411', NULL);
INSERT INTO store_action_states (store_id, action_id, state_id, state, "timestamp", token_id) VALUES (1, 3, 7, 'accepted', '2014-12-10 15:21:51.993411', NULL);

SELECT pg_catalog.setval('store_action_states_state_id_seq', 7, true);

INSERT INTO store_changes (store_id, action_id, state_id, is_param, column_name, old_value, new_value) VALUES (1, 4, 4, false, 'status', 'pending', 'inactive');
INSERT INTO store_changes (store_id, action_id, state_id, is_param, column_name, old_value, new_value) VALUES (1, 3, 3, false, 'date_end', NULL, '2014-12-10 15:31:51.993411');
INSERT INTO store_changes (store_id, action_id, state_id, is_param, column_name, old_value, new_value) VALUES (1, 3, 3, true, 'store_type', NULL, '5');


INSERT INTO store_params (store_id, name, value) VALUES (1, 'store_type', '5');


SELECT pg_catalog.setval('stores_store_id_seq', 1, true);

SELECT pg_catalog.setval('tokens_token_id_seq', 717, true);


--Change all passwords to 123123123
update ads set salted_passwd =  E'$1024$2y1bQ}VYRD}zF4QCbf07a971b784888658a20e62156de64dfde5ffb6', passwd = null;
update accounts set salted_passwd =  E'$1024$2y1bQ}VYRD}zF4QCbf07a971b784888658a20e62156de64dfde5ffb6';
