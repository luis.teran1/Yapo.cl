INSERT INTO stores (store_id, name, phone, address, info_text, status, date_start, date_end, region, url, hide_phone, commune, address_number, website_url, main_image, main_image_offset, logo_image, account_id) VALUES (1, 'Da store', '987654321', 'Alonso de C�rdova 5670', 'La tienda de prepaid5', 'active', '2016-09-15 10:12:25.509817', '2016-09-15 10:27:27.604669', 15, 'da-store', false, 315, '', NULL, NULL, NULL, NULL, 1);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (758, 'X57d9b3f674528e5d000000001cd9a43100000000', 9, '2016-09-14 17:32:54.213435', '2016-09-14 17:32:57.499043', '10.0.1.157', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (759, 'X57d9b3f94f099b5b000000007199a3c700000000', 9, '2016-09-14 17:32:57.499043', '2016-09-14 17:33:00.009114', '10.0.1.157', 'review', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (760, 'X57d9b3fc7a8be104000000003567b4a400000000', 9, '2016-09-14 17:33:00.009114', '2016-09-14 17:33:02.503335', '10.0.1.157', 'review', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (761, 'X57d9b3ff4a70ea35000000004a574b2300000000', 9, '2016-09-14 17:33:02.503335', '2016-09-14 18:33:02.503335', '10.0.1.157', 'review', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (786, 'X57da9eed57d8710f0000000020472a7600000000', 9, '2016-09-15 10:15:24.587942', '2016-09-15 10:15:28.110895', '10.0.2.31', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (787, 'X57da9ef0e7e83ed0000000063829d00000000', 9, '2016-09-15 10:15:28.110895', '2016-09-15 11:15:28.110895', '10.0.2.31', 'review', NULL, NULL);
INSERT INTO account_actions (account_id, account_action_id, account_action_type, "timestamp", token_id) VALUES (1, 1, 'edition', '2016-09-14 17:29:17.769744', NULL);
INSERT INTO account_actions (account_id, account_action_id, account_action_type, "timestamp", token_id) VALUES (1, 2, 'edition', '2016-09-14 17:29:59.141305', NULL);
INSERT INTO account_actions (account_id, account_action_id, account_action_type, "timestamp", token_id) VALUES (1, 3, 'edition', '2016-09-14 17:31:09.754695', NULL);
INSERT INTO account_actions (account_id, account_action_id, account_action_type, "timestamp", token_id) VALUES (1, 4, 'edition', '2016-09-15 10:14:22.690465', NULL);
INSERT INTO account_actions (account_id, account_action_id, account_action_type, "timestamp", token_id) VALUES (1, 5, 'add_pro_category_param', '2016-09-15 10:15:28.17187', NULL);
INSERT INTO account_params (account_id, name, value) VALUES (1, 'is_pro_for', '1220');
INSERT INTO account_params (account_id, name, value) VALUES (1, 'pack_type', 'real_estate');
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (127, 8000086, '2016-09-14 17:33:00', 'active', 'let', 'blocket', '987654321', 15, 0, 1240, 5, NULL, false, false, true, 'Oficina central', 'Qwerty', 3155, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$2y1bQ}VYRD}zF4QCbf07a971b784888658a20e62156de64dfde5ffb6', '2016-09-14 17:33:00.022764', 'es');
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (126, 8000087, '2016-09-14 17:33:02', 'active', 'sell', 'blocket', '987654321', 15, 0, 1220, 5, NULL, false, false, true, 'Departamento central', '1 pieza con estacionamiento', 324598, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$2y1bQ}VYRD}zF4QCbf07a971b784888658a20e62156de64dfde5ffb6', '2016-09-14 17:33:02.5065', 'es');
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (128, 8000085, '2016-09-14 17:32:57', 'active', 'sell', 'blocket', '987654321', 15, 0, 5020, 5, NULL, false, false, true, 'Silla vieja', 'En buen estado', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$2y1bQ}VYRD}zF4QCbf07a971b784888658a20e62156de64dfde5ffb6', '2016-09-14 17:32:57.50206', 'es');
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (129, 8000088, '2016-09-15 10:15:28', 'active', 'sell', 'blocket', '987654321', 15, 0, 1220, 5, NULL, false, false, true, 'Depto central muy bien cuidado', 'Esta la raja, comprenlo', 85000000, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$2y1bQ}VYRD}zF4QCbf07a971b784888658a20e62156de64dfde5ffb6', '2016-09-15 10:15:28.114476', 'es');
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (166, '633026231', 'verified', '2016-09-14 17:29:17.6863', NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (167, '768418254', 'verified', '2016-09-14 17:29:59.099959', NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (168, '903810277', 'verified', '2016-09-14 17:31:09.691524', NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (169, '12345', 'paid', '2016-09-15 10:12:25.534809', NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (170, '633026231', 'verified', '2016-09-15 10:14:21.603951', NULL);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (128, 1, 'new', 709, 'accepted', 'normal', NULL, NULL, 168);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (127, 1, 'new', 710, 'accepted', 'normal', NULL, NULL, 167);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (126, 1, 'new', 711, 'accepted', 'normal', NULL, NULL, 166);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (129, 1, 'new', 715, 'accepted', 'whitelist', NULL, NULL, 170);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (1, 2, 'account_to_pro', 717, 'accepted', 'normal', NULL, NULL, NULL);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (95, 2, 'account_to_pro', 719, 'accepted', 'normal', NULL, NULL, NULL);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (86, 2, 'account_to_pro', 721, 'accepted', 'normal', NULL, NULL, NULL);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (10, 2, 'account_to_pro', 723, 'accepted', 'normal', NULL, NULL, NULL);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (6, 2, 'account_to_pro', 725, 'accepted', 'normal', NULL, NULL, NULL);
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (126, 1, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (126, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (127, 1, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (127, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (128, 1, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (128, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (129, 1, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (129, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (126, 1, 698, 'reg', 'initial', '2016-09-14 17:29:17.6863', '10.0.1.157', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (126, 1, 699, 'unverified', 'verifymail', '2016-09-14 17:29:17.6863', '10.0.1.157', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (126, 1, 700, 'pending_review', 'verify', '2016-09-14 17:29:17.817719', '10.0.1.157', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (127, 1, 701, 'reg', 'initial', '2016-09-14 17:29:59.099959', '10.0.1.157', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (127, 1, 702, 'unverified', 'verifymail', '2016-09-14 17:29:59.099959', '10.0.1.157', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (127, 1, 703, 'pending_review', 'verify', '2016-09-14 17:29:59.170941', '10.0.1.157', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (128, 1, 704, 'reg', 'initial', '2016-09-14 17:31:09.691524', '10.0.1.157', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (128, 1, 705, 'unverified', 'verifymail', '2016-09-14 17:31:09.691524', '10.0.1.157', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (128, 1, 706, 'pending_review', 'verify', '2016-09-14 17:31:09.807427', '10.0.1.157', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (128, 1, 709, 'accepted', 'accept', '2016-09-14 17:32:57.50206', '10.0.1.157', 758);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (127, 1, 710, 'accepted', 'accept', '2016-09-14 17:33:00.022764', '10.0.1.157', 759);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (126, 1, 711, 'accepted', 'accept', '2016-09-14 17:33:02.5065', '10.0.1.157', 760);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (129, 1, 712, 'reg', 'initial', '2016-09-15 10:14:21.603951', '10.0.2.31', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (129, 1, 713, 'unverified', 'verifymail', '2016-09-15 10:14:21.603951', '10.0.2.31', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (129, 1, 714, 'pending_review', 'verify', '2016-09-15 10:14:22.745273', '10.0.2.31', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (129, 1, 715, 'accepted', 'accept', '2016-09-15 10:15:28.114476', '10.0.2.31', 786);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (1, 2, 716, 'reg', 'initial', '2016-09-15 10:15:28.17187', '127.0.0.1', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (1, 2, 717, 'accepted', 'adminclear', '2016-09-15 10:15:28.17187', '127.0.0.1', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (95, 2, 718, 'reg', 'initial', '2016-09-15 10:15:28.17187', '127.0.0.1', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (95, 2, 719, 'accepted', 'adminclear', '2016-09-15 10:15:28.17187', '127.0.0.1', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (86, 2, 720, 'reg', 'initial', '2016-09-15 10:15:28.17187', '127.0.0.1', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (86, 2, 721, 'accepted', 'adminclear', '2016-09-15 10:15:28.17187', '127.0.0.1', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (10, 2, 722, 'reg', 'initial', '2016-09-15 10:15:28.17187', '127.0.0.1', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (10, 2, 723, 'accepted', 'adminclear', '2016-09-15 10:15:28.17187', '127.0.0.1', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (6, 2, 724, 'reg', 'initial', '2016-09-15 10:15:28.17187', '127.0.0.1', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (6, 2, 725, 'accepted', 'adminclear', '2016-09-15 10:15:28.17187', '127.0.0.1', NULL);
SELECT pg_catalog.setval('action_states_state_id_seq', 725, true);
INSERT INTO ad_codes (code, code_type) VALUES (24741, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (56764, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (88787, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (30810, 'pay');
INSERT INTO ad_image_changes (ad_id, action_id, state_id, seq_no, name, is_new) VALUES (128, 1, 709, 0, NULL, false);
INSERT INTO ad_image_changes (ad_id, action_id, state_id, seq_no, name, is_new) VALUES (127, 1, 710, 0, NULL, false);
INSERT INTO ad_image_changes (ad_id, action_id, state_id, seq_no, name, is_new) VALUES (126, 1, 711, 0, NULL, false);
INSERT INTO ad_image_changes (ad_id, action_id, state_id, seq_no, name, is_new) VALUES (129, 1, 715, 0, NULL, false);
INSERT INTO ad_params (ad_id, name, value) VALUES (126, 'rooms', '1');
INSERT INTO ad_params (ad_id, name, value) VALUES (126, 'size', '100');
INSERT INTO ad_params (ad_id, name, value) VALUES (126, 'garage_spaces', '1');
INSERT INTO ad_params (ad_id, name, value) VALUES (126, 'condominio', '40000');
INSERT INTO ad_params (ad_id, name, value) VALUES (126, 'communes', '315');
INSERT INTO ad_params (ad_id, name, value) VALUES (126, 'currency', 'uf');
INSERT INTO ad_params (ad_id, name, value) VALUES (126, 'estate_type', '1');
INSERT INTO ad_params (ad_id, name, value) VALUES (126, 'bathrooms', '1');
INSERT INTO ad_params (ad_id, name, value) VALUES (126, 'country', 'UNK');
INSERT INTO ad_params (ad_id, name, value) VALUES (127, 'size', '32');
INSERT INTO ad_params (ad_id, name, value) VALUES (127, 'garage_spaces', '1');
INSERT INTO ad_params (ad_id, name, value) VALUES (127, 'communes', '330');
INSERT INTO ad_params (ad_id, name, value) VALUES (127, 'currency', 'uf');
INSERT INTO ad_params (ad_id, name, value) VALUES (127, 'estate_type', '3');
INSERT INTO ad_params (ad_id, name, value) VALUES (127, 'country', 'UNK');
INSERT INTO ad_params (ad_id, name, value) VALUES (128, 'communes', '315');
INSERT INTO ad_params (ad_id, name, value) VALUES (128, 'currency', 'peso');
INSERT INTO ad_params (ad_id, name, value) VALUES (128, 'country', 'UNK');
INSERT INTO ad_params (ad_id, name, value) VALUES (128, 'prev_currency', 'peso');
INSERT INTO ad_params (ad_id, name, value) VALUES (129, 'rooms', '1');
INSERT INTO ad_params (ad_id, name, value) VALUES (129, 'size', '1500');
INSERT INTO ad_params (ad_id, name, value) VALUES (129, 'garage_spaces', '40');
INSERT INTO ad_params (ad_id, name, value) VALUES (129, 'condominio', '5000');
INSERT INTO ad_params (ad_id, name, value) VALUES (129, 'communes', '315');
INSERT INTO ad_params (ad_id, name, value) VALUES (129, 'currency', 'peso');
INSERT INTO ad_params (ad_id, name, value) VALUES (129, 'estate_type', '1');
INSERT INTO ad_params (ad_id, name, value) VALUES (129, 'bathrooms', '3');
INSERT INTO ad_params (ad_id, name, value) VALUES (129, 'country', 'UNK');

INSERT INTO ad_params (ad_id, name, value) VALUES (129, 'address_number', '1001');
INSERT INTO ad_params (ad_id, name, value) VALUES (129, 'geoposition', '-33.4256935,-70.590889');
INSERT INTO ad_params (ad_id, name, value) VALUES (129, 'geoposition_is_precise', '1');
INSERT INTO ad_params (ad_id, name, value) VALUES (129, 'address', 'Avenida Mar�ano S�nchez Fontecilla');
INSERT INTO ad_params (ad_id, name, value) VALUES (129, 'street_id', '62363');

SELECT pg_catalog.setval('ads_ad_id_seq', 129, true);
INSERT INTO blocked_items (item_id, value, list_id, notice, token_id) VALUES (22, 'prepaid5@blocket.se', 16, 'Email automatically put into Whitelist in Ad Insert.', NULL);
SELECT pg_catalog.setval('blocked_items_item_id_seq', 22, true);
SELECT pg_catalog.setval('list_id_seq', 8000088, true);
SELECT pg_catalog.setval('pay_code_seq', 470, true);
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (213, 'save', 0, '633026231', NULL, '2016-09-14 17:29:18', 166, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (214, 'verify', 0, '633026231', NULL, '2016-09-14 17:29:18', 166, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (215, 'save', 0, '768418254', NULL, '2016-09-14 17:29:59', 167, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (216, 'verify', 0, '768418254', NULL, '2016-09-14 17:29:59', 167, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (217, 'save', 0, '903810277', NULL, '2016-09-14 17:31:10', 168, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (218, 'verify', 0, '903810277', NULL, '2016-09-14 17:31:10', 168, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (219, 'save', 0, '12345', NULL, '2016-09-15 10:12:26', 169, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (220, 'paycard_save', 0, '12345', NULL, '2016-09-15 10:12:28', 169, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (221, 'paycard', 27000, '12345', NULL, '2016-09-15 10:12:28', 169, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (222, 'save', 0, '633026231', NULL, '2016-09-15 10:14:22', 170, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (223, 'verify', 0, '633026231', NULL, '2016-09-15 10:14:23', 170, 'OK');
SELECT pg_catalog.setval('pay_log_pay_log_id_seq', 223, true);
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (213, 0, 'adphone', '987654321');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (213, 1, 'remote_addr', '10.0.1.157');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (214, 0, 'remote_addr', '10.0.1.157');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (215, 0, 'adphone', '987654321');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (215, 1, 'remote_addr', '10.0.1.157');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (216, 0, 'remote_addr', '10.0.1.157');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (217, 0, 'adphone', '987654321');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (217, 1, 'remote_addr', '10.0.1.157');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (218, 0, 'remote_addr', '10.0.1.157');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (219, 0, 'remote_addr', '10.0.2.31');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (220, 0, 'auth_code', '789643');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (220, 1, 'trans_date', '15/09/2016 10:12:27');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (220, 2, 'placements_type', 'VN');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (220, 3, 'placements_number', '1');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (220, 4, 'pay_type', 'credit');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (220, 5, 'external_response', '0');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (220, 6, 'external_id', '53173174051530434392');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (220, 7, 'cc_number', '7665');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (220, 8, 'remote_addr', '10.0.1.76');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (222, 0, 'adphone', '987654321');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (222, 1, 'remote_addr', '10.0.2.31');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (223, 0, 'remote_addr', '10.0.2.31');
SELECT pg_catalog.setval('payment_groups_payment_group_id_seq', 170, true);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (166, 'ad_action', 0, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (167, 'ad_action', 0, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (168, 'ad_action', 0, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (169, 'biannual_store', 27000, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (170, 'ad_action', 0, 0);
INSERT INTO purchase (purchase_id, doc_type, doc_num, external_doc_id, status, receipt, delivery, rut, name, lob, address, region, communes, contact, email, tax, discount, seller_id, total_price, payment_group_id, payment_method, payment_platform, account_id) VALUES (2, 'invoice', NULL, NULL, 'paid', '2016-09-15 10:12:25.534809', NULL, '100000-4', 'BLOCKET', 'Technology', 'Alonso de C�rdova 5670', 15, 315, 'BLOCKET', 'prepaid5@blocket.se', 0, 0, NULL, 27000, 169, 'webpay', 'desktop', 1);
INSERT INTO purchase_detail (purchase_detail_id, purchase_id, product_id, price, action_id, ad_id, payment_group_id) VALUES (2, 2, 6, 27000, NULL, NULL, 169);
SELECT pg_catalog.setval('purchase_detail_purchase_detail_id_seq', 2, true);
SELECT pg_catalog.setval('purchase_purchase_id_seq', 2, true);
INSERT INTO purchase_states (purchase_id, purchase_state_id, status, "timestamp", remote_addr) VALUES (2, 1, 'pending', '2016-09-15 10:12:25.534809', '10.0.2.31');
SELECT pg_catalog.setval('purchase_states_purchase_state_id_seq', 1, true);
INSERT INTO review_log (ad_id, action_id, admin_id, review_time, queue, action_type, action, refusal_reason_text, category) VALUES (128, 1, 9, '2016-09-14 17:32:57.50206', 'all', 'new', 'accepted', NULL, 5020);
INSERT INTO review_log (ad_id, action_id, admin_id, review_time, queue, action_type, action, refusal_reason_text, category) VALUES (127, 1, 9, '2016-09-14 17:33:00.022764', 'all', 'new', 'accepted', NULL, 1240);
INSERT INTO review_log (ad_id, action_id, admin_id, review_time, queue, action_type, action, refusal_reason_text, category) VALUES (126, 1, 9, '2016-09-14 17:33:02.5065', 'all', 'new', 'accepted', NULL, 1220);
INSERT INTO review_log (ad_id, action_id, admin_id, review_time, queue, action_type, action, refusal_reason_text, category) VALUES (129, 1, 9, '2016-09-15 10:15:28.114476', 'whitelist', 'new', 'accepted', NULL, 1220);
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (126, 1, 700, 'queue', 'normal');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (127, 1, 703, 'queue', 'normal');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (128, 1, 706, 'queue', 'normal');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (128, 1, 709, 'filter_name', 'all');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (127, 1, 710, 'filter_name', 'all');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (126, 1, 711, 'filter_name', 'all');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (129, 1, 714, 'queue', 'whitelist');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (129, 1, 715, 'filter_name', 'whitelist');
INSERT INTO store_actions (store_id, action_id, action_type, current_state, state, payment_group_id) VALUES (1, 1, 'new', 1, 'reg', NULL);
INSERT INTO store_actions (store_id, action_id, action_type, current_state, state, payment_group_id) VALUES (1, 2, 'new', 2, 'unpaid', NULL);
INSERT INTO store_actions (store_id, action_id, action_type, current_state, state, payment_group_id) VALUES (1, 4, 'status_change', 5, 'accepted', NULL);
INSERT INTO store_actions (store_id, action_id, action_type, current_state, state, payment_group_id) VALUES (1, 3, 'extend', 7, 'accepted', 169);
INSERT INTO store_actions (store_id, action_id, action_type, current_state, state, payment_group_id) VALUES (1, 5, 'edit', 9, 'accepted', NULL);
INSERT INTO store_actions (store_id, action_id, action_type, current_state, state, payment_group_id) VALUES (1, 6, 'status_change', 11, 'accepted', NULL);
INSERT INTO store_action_states (store_id, action_id, state_id, state, "timestamp", token_id) VALUES (1, 1, 1, 'reg', '2016-09-15 10:12:25.509817', NULL);
INSERT INTO store_action_states (store_id, action_id, state_id, state, "timestamp", token_id) VALUES (1, 2, 2, 'unpaid', '2016-09-15 10:12:25.509817', NULL);
INSERT INTO store_action_states (store_id, action_id, state_id, state, "timestamp", token_id) VALUES (1, 3, 3, 'unpaid', '2016-09-15 10:12:25.534809', NULL);
INSERT INTO store_action_states (store_id, action_id, state_id, state, "timestamp", token_id) VALUES (1, 4, 4, 'reg', '2016-09-15 10:12:27.604669', NULL);
INSERT INTO store_action_states (store_id, action_id, state_id, state, "timestamp", token_id) VALUES (1, 4, 5, 'accepted', '2016-09-15 10:12:27.604669', NULL);
INSERT INTO store_action_states (store_id, action_id, state_id, state, "timestamp", token_id) VALUES (1, 3, 6, 'paid', '2016-09-15 10:12:27.604669', NULL);
INSERT INTO store_action_states (store_id, action_id, state_id, state, "timestamp", token_id) VALUES (1, 3, 7, 'accepted', '2016-09-15 10:12:27.604669', NULL);
INSERT INTO store_action_states (store_id, action_id, state_id, state, "timestamp", token_id) VALUES (1, 5, 8, 'reg', '2016-09-15 10:12:53.933095', NULL);
INSERT INTO store_action_states (store_id, action_id, state_id, state, "timestamp", token_id) VALUES (1, 5, 9, 'accepted', '2016-09-15 10:12:53.933095', NULL);
INSERT INTO store_action_states (store_id, action_id, state_id, state, "timestamp", token_id) VALUES (1, 6, 10, 'reg', '2016-09-15 10:12:53.933095', NULL);
INSERT INTO store_action_states (store_id, action_id, state_id, state, "timestamp", token_id) VALUES (1, 6, 11, 'accepted', '2016-09-15 10:12:53.933095', NULL);
SELECT pg_catalog.setval('store_action_states_state_id_seq', 11, true);
INSERT INTO store_changes (store_id, action_id, state_id, is_param, column_name, old_value, new_value) VALUES (1, 4, 4, false, 'status', 'pending', 'inactive');
INSERT INTO store_changes (store_id, action_id, state_id, is_param, column_name, old_value, new_value) VALUES (1, 3, 3, false, 'date_end', NULL, '2016-09-15 10:27:27.604669');
INSERT INTO store_changes (store_id, action_id, state_id, is_param, column_name, old_value, new_value) VALUES (1, 3, 3, true, 'store_type', NULL, '6');
INSERT INTO store_changes (store_id, action_id, state_id, is_param, column_name, old_value, new_value) VALUES (1, 5, 8, false, 'name', NULL, 'Da store');
INSERT INTO store_changes (store_id, action_id, state_id, is_param, column_name, old_value, new_value) VALUES (1, 5, 8, false, 'info_text', NULL, 'La tienda de prepaid5');
INSERT INTO store_changes (store_id, action_id, state_id, is_param, column_name, old_value, new_value) VALUES (1, 5, 8, false, 'url', NULL, 'da-store');
INSERT INTO store_changes (store_id, action_id, state_id, is_param, column_name, old_value, new_value) VALUES (1, 5, 8, false, 'phone', NULL, '987654321');
INSERT INTO store_changes (store_id, action_id, state_id, is_param, column_name, old_value, new_value) VALUES (1, 5, 8, false, 'hide_phone', NULL, '0');
INSERT INTO store_changes (store_id, action_id, state_id, is_param, column_name, old_value, new_value) VALUES (1, 5, 8, false, 'address', NULL, 'Alonso de C�rdova 5670');
INSERT INTO store_changes (store_id, action_id, state_id, is_param, column_name, old_value, new_value) VALUES (1, 5, 8, false, 'address_number', NULL, '');
INSERT INTO store_changes (store_id, action_id, state_id, is_param, column_name, old_value, new_value) VALUES (1, 5, 8, false, 'region', NULL, '15');
INSERT INTO store_changes (store_id, action_id, state_id, is_param, column_name, old_value, new_value) VALUES (1, 5, 8, false, 'commune', NULL, '315');
INSERT INTO store_changes (store_id, action_id, state_id, is_param, column_name, old_value, new_value) VALUES (1, 6, 10, false, 'status', 'inactive', 'active');
INSERT INTO store_params (store_id, name, value) VALUES (1, 'store_type', '6');
SELECT pg_catalog.setval('stores_store_id_seq', 1, true);
SELECT pg_catalog.setval('tokens_token_id_seq', 787, true);
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (93, '2016-09-14 17:32:57.531264', '22990@sasha.schibsted.cl', NULL, '2016-09-14 17:33:06.671484', '22990@sasha.schibsted.cl', 'TRANS_OK', '2016-09-14 17:33:06.618888', NULL, 'cmd:admail
commit:1
ad_id:128
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (94, '2016-09-14 17:33:00.051978', '22990@sasha.schibsted.cl', NULL, '2016-09-14 17:33:06.731523', '22990@sasha.schibsted.cl', 'TRANS_OK', '2016-09-14 17:33:06.618888', NULL, 'cmd:admail
commit:1
ad_id:127
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (95, '2016-09-14 17:33:02.517011', '22990@sasha.schibsted.cl', NULL, '2016-09-14 17:33:06.731622', '22990@sasha.schibsted.cl', 'TRANS_OK', '2016-09-14 17:33:06.618888', NULL, 'cmd:admail
commit:1
ad_id:126
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (96, '2016-09-15 10:14:22.801868', '24182@sasha.schibsted.cl', '2016-09-15 10:44:22.801868', NULL, NULL, NULL, NULL, NULL, 'cmd:review
commit:1
ad_id:129
action_id:1
action:accept
filter_name:whitelist
remote_addr:10.0.2.31

', NULL, 'AT', 'review', '129');
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (97, '2016-09-15 10:15:28.148021', '24182@sasha.schibsted.cl', NULL, '2016-09-15 10:15:34.229203', '24182@sasha.schibsted.cl', 'TRANS_OK', '2016-09-15 10:15:34.186718', NULL, 'cmd:admail
commit:1
ad_id:129
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
SELECT pg_catalog.setval('trans_queue_id_seq', 97, true);
