
INSERT INTO accounts VALUES (8, 'android', '', 'android@yapo.cl', '987654321', false, 15, '$1024$Xz9fObnE6R=?;WF3dedc5f856ce5f31bf35015ac0f38cd5edddf155a', '2014-10-03 13:01:34.712883', '2014-10-03 13:01:46.51965', 'active', NULL, NULL, NULL, NULL, 50);
SELECT pg_catalog.setval('accounts_account_id_seq', 8, true);
INSERT INTO payment_groups VALUES (166, '12345', 'paid', '2014-10-03 12:55:35.146251', NULL);
INSERT INTO payment_groups VALUES (167, '65432', 'paid', '2014-10-03 12:58:32.959003', NULL);
INSERT INTO payment_groups VALUES (168, '32323', 'paid', '2014-10-03 13:02:04.522927', NULL);
INSERT INTO payment_groups VALUES (169, '12345', 'paid', '2014-10-08 16:09:14.283816', NULL);

INSERT INTO stores VALUES (1, 'Tienda test', '1234567', 'Alonso de C�rdova 5670', 'tienda test
tienda test
tienda test', 'active', '2014-10-03 12:55:35.123377', '2014-10-03 13:15:39.052913', 15, 'tienda-test', NULL, 315, '', NULL, NULL, NULL, NULL, 1);
INSERT INTO stores VALUES (2, 'Tienda no-ads', '6666667', '', 'tienda no-ads
tienda no-ads
tienda no-ads', 'active', '2014-10-03 12:58:32.940385', '2014-10-03 13:18:36.723602', 14, 'tienda-no-ads', NULL, NULL, '', NULL, NULL, NULL, NULL, 2);
INSERT INTO stores VALUES (3, 'Tienda android', '98798798787', 'mi calle', 'tienda android
tienda android
tienda android', 'active', '2014-10-03 13:02:04.505442', '2014-10-03 13:22:10.264963', 15, 'tienda-android', NULL, NULL, '123', 'www.yapo.cl', NULL, NULL, NULL, 8);
INSERT INTO stores VALUES (4, 'Usuario01', '123123123', 'mi calle', 'Esta tienda est� expirada', 'active', '2014-09-08 16:09:14.273294', '2014-09-08 16:14:16.867522', 15, 'usuario01', NULL, NULL, '123', NULL, NULL, NULL, NULL, 7);

INSERT INTO pay_log VALUES (213, 'save', 0, '12345', NULL, '2014-10-03 12:55:35', 166, 'SAVE');
INSERT INTO pay_log VALUES (214, 'paycard_save', 0, '12345', NULL, '2014-10-03 12:55:39', 166, 'SAVE');
INSERT INTO pay_log VALUES (215, 'save', 0, '65432', NULL, '2014-10-03 12:58:33', 167, 'SAVE');
INSERT INTO pay_log VALUES (216, 'paycard_save', 0, '65432', NULL, '2014-10-03 12:58:36', 167, 'SAVE');
INSERT INTO pay_log VALUES (217, 'save', 0, '32323', NULL, '2014-10-03 13:02:05', 168, 'SAVE');
INSERT INTO pay_log VALUES (218, 'paycard_save', 0, '32323', NULL, '2014-10-03 13:02:10', 168, 'SAVE');
INSERT INTO pay_log VALUES (219, 'save', 0, '12345', NULL, '2014-10-08 16:09:14', 169, 'SAVE');
INSERT INTO pay_log VALUES (220, 'paycard_save', 0, '12345', NULL, '2014-10-08 16:09:17', 169, 'SAVE');

SELECT pg_catalog.setval('pay_log_pay_log_id_seq', 220, true);

INSERT INTO pay_log_references VALUES (213, 0, 'remote_addr', '10.0.1.101');
INSERT INTO pay_log_references VALUES (214, 0, 'auth_code', '837647');
INSERT INTO pay_log_references VALUES (214, 1, 'trans_date', '03/10/2014 12:55:37');
INSERT INTO pay_log_references VALUES (214, 2, 'placements_type', 'VN');
INSERT INTO pay_log_references VALUES (214, 3, 'placements_number', '1');
INSERT INTO pay_log_references VALUES (214, 4, 'pay_type', 'credit');
INSERT INTO pay_log_references VALUES (214, 5, 'external_response', '0');
INSERT INTO pay_log_references VALUES (214, 6, 'external_id', '63030545542274120344');
INSERT INTO pay_log_references VALUES (214, 7, 'remote_addr', '127.0.0.1');
INSERT INTO pay_log_references VALUES (215, 0, 'remote_addr', '10.0.1.101');
INSERT INTO pay_log_references VALUES (216, 0, 'auth_code', '164331');
INSERT INTO pay_log_references VALUES (216, 1, 'trans_date', '03/10/2014 12:58:36');
INSERT INTO pay_log_references VALUES (216, 2, 'placements_type', 'VN');
INSERT INTO pay_log_references VALUES (216, 3, 'placements_number', '1');
INSERT INTO pay_log_references VALUES (216, 4, 'pay_type', 'credit');
INSERT INTO pay_log_references VALUES (216, 5, 'external_response', '0');
INSERT INTO pay_log_references VALUES (216, 6, 'external_id', '16249145192211773015');
INSERT INTO pay_log_references VALUES (216, 7, 'remote_addr', '127.0.0.1');
INSERT INTO pay_log_references VALUES (217, 0, 'remote_addr', '10.0.1.101');
INSERT INTO pay_log_references VALUES (218, 0, 'auth_code', '695355');
INSERT INTO pay_log_references VALUES (218, 1, 'trans_date', '03/10/2014 13:02:09');
INSERT INTO pay_log_references VALUES (218, 2, 'placements_type', 'VN');
INSERT INTO pay_log_references VALUES (218, 3, 'placements_number', '1');
INSERT INTO pay_log_references VALUES (218, 4, 'pay_type', 'credit');
INSERT INTO pay_log_references VALUES (218, 5, 'external_response', '0');
INSERT INTO pay_log_references VALUES (218, 6, 'external_id', '78891681539584114152');
INSERT INTO pay_log_references VALUES (218, 7, 'remote_addr', '127.0.0.1');
INSERT INTO pay_log_references VALUES (219, 0, 'remote_addr', '10.0.1.167');
INSERT INTO pay_log_references VALUES (220, 0, 'auth_code', '312010');
INSERT INTO pay_log_references VALUES (220, 1, 'trans_date', '08/10/2014 16:09:16');
INSERT INTO pay_log_references VALUES (220, 2, 'placements_type', 'VN');
INSERT INTO pay_log_references VALUES (220, 3, 'placements_number', '1');
INSERT INTO pay_log_references VALUES (220, 4, 'pay_type', 'credit');
INSERT INTO pay_log_references VALUES (220, 5, 'external_response', '0');
INSERT INTO pay_log_references VALUES (220, 6, 'external_id', '66086918233006813512');
INSERT INTO pay_log_references VALUES (220, 7, 'remote_addr', '10.0.1.209');

SELECT pg_catalog.setval('payment_groups_payment_group_id_seq', 169, true);

INSERT INTO payments VALUES (166, 'annual_store', 48000, 0);
INSERT INTO payments VALUES (167, 'annual_store', 48000, 0);
INSERT INTO payments VALUES (168, 'annual_store', 48000, 0);
INSERT INTO payments VALUES (169, 'monthly_store', 6000, 0);


INSERT INTO purchase VALUES (2, 'invoice', NULL, NULL, 'paid', '2014-10-03 12:55:35.146251', NULL, '100000-4', 'BLOCKET', 'Technology', 'Alonso de C�rdova 5670', 15, 315, 'BLOCKET', 'prepaid5@blocket.se', 0, 0, NULL, 48000, 166);
INSERT INTO purchase VALUES (3, 'bill', NULL, NULL, 'paid', '2014-10-03 12:58:32.959003', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no-ads@yapo.cl', 0, 0, NULL, 48000, 167);
INSERT INTO purchase VALUES (4, 'bill', NULL, NULL, 'paid', '2014-10-03 13:02:04.522927', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'android@yapo.cl', 0, 0, NULL, 48000, 168);
INSERT INTO purchase VALUES (5, 'bill', NULL, NULL, 'paid', '2014-10-08 16:09:14.283816', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'user.01@schibsted.cl', 0, 0, NULL, 6000, 169);

INSERT INTO purchase_detail VALUES (2, 2, 7, 48000, NULL, NULL);
INSERT INTO purchase_detail VALUES (3, 3, 7, 48000, NULL, NULL);
INSERT INTO purchase_detail VALUES (4, 4, 7, 48000, NULL, NULL);
INSERT INTO purchase_detail VALUES (5, 5, 4, 6000, NULL, NULL);

SELECT pg_catalog.setval('purchase_detail_purchase_detail_id_seq', 5, true);

SELECT pg_catalog.setval('purchase_purchase_id_seq', 5, true);

INSERT INTO purchase_states VALUES (2, 1, 'pending', '2014-10-03 12:55:35.146251', '10.0.1.101');
INSERT INTO purchase_states VALUES (3, 2, 'pending', '2014-10-03 12:58:32.959003', '10.0.1.101');
INSERT INTO purchase_states VALUES (4, 3, 'pending', '2014-10-03 13:02:04.522927', '10.0.1.101');
INSERT INTO purchase_states VALUES (5, 4, 'pending', '2014-10-08 16:09:14.283816', '10.0.1.167');

SELECT pg_catalog.setval('purchase_states_purchase_state_id_seq', 4, true);

INSERT INTO store_actions VALUES (1, 1, 'new', 1, 'reg', NULL);
INSERT INTO store_actions VALUES (1, 2, 'new', 2, 'unpaid', NULL);
INSERT INTO store_actions VALUES (1, 4, 'status_change', 5, 'accepted', NULL);
INSERT INTO store_actions VALUES (1, 3, 'extend', 7, 'accepted', 166);
INSERT INTO store_actions VALUES (1, 5, 'edit', 9, 'accepted', NULL);
INSERT INTO store_actions VALUES (1, 6, 'status_change', 11, 'accepted', NULL);
INSERT INTO store_actions VALUES (2, 1, 'new', 12, 'reg', NULL);
INSERT INTO store_actions VALUES (2, 2, 'new', 13, 'unpaid', NULL);
INSERT INTO store_actions VALUES (2, 4, 'status_change', 16, 'accepted', NULL);
INSERT INTO store_actions VALUES (2, 3, 'extend', 18, 'accepted', 167);
INSERT INTO store_actions VALUES (2, 5, 'edit', 20, 'accepted', NULL);
INSERT INTO store_actions VALUES (2, 6, 'status_change', 22, 'accepted', NULL);
INSERT INTO store_actions VALUES (3, 1, 'new', 23, 'reg', NULL);
INSERT INTO store_actions VALUES (3, 2, 'new', 24, 'unpaid', NULL);
INSERT INTO store_actions VALUES (3, 4, 'status_change', 27, 'accepted', NULL);
INSERT INTO store_actions VALUES (3, 3, 'extend', 29, 'accepted', 168);
INSERT INTO store_actions VALUES (3, 5, 'edit', 31, 'accepted', NULL);
INSERT INTO store_actions VALUES (3, 6, 'status_change', 33, 'accepted', NULL);
INSERT INTO store_actions VALUES (4, 1, 'new', 34, 'reg', NULL);
INSERT INTO store_actions VALUES (4, 2, 'new', 35, 'unpaid', NULL);
INSERT INTO store_actions VALUES (4, 4, 'status_change', 38, 'accepted', NULL);
INSERT INTO store_actions VALUES (4, 3, 'extend', 40, 'accepted', 169);
INSERT INTO store_actions VALUES (4, 5, 'edit', 42, 'accepted', NULL);
INSERT INTO store_actions VALUES (4, 6, 'status_change', 44, 'accepted', NULL);

INSERT INTO store_action_states VALUES (1, 1, 1, 'reg', '2014-10-03 12:55:35.123377', NULL);
INSERT INTO store_action_states VALUES (1, 2, 2, 'unpaid', '2014-10-03 12:55:35.123377', NULL);
INSERT INTO store_action_states VALUES (1, 3, 3, 'unpaid', '2014-10-03 12:55:35.146251', NULL);
INSERT INTO store_action_states VALUES (1, 4, 4, 'reg', '2014-10-03 12:55:39.052913', NULL);
INSERT INTO store_action_states VALUES (1, 4, 5, 'accepted', '2014-10-03 12:55:39.052913', NULL);
INSERT INTO store_action_states VALUES (1, 3, 6, 'paid', '2014-10-03 12:55:39.052913', NULL);
INSERT INTO store_action_states VALUES (1, 3, 7, 'accepted', '2014-10-03 12:55:39.052913', NULL);
INSERT INTO store_action_states VALUES (1, 5, 8, 'reg', '2014-10-03 12:56:05.226327', NULL);
INSERT INTO store_action_states VALUES (1, 5, 9, 'accepted', '2014-10-03 12:56:05.226327', NULL);
INSERT INTO store_action_states VALUES (1, 6, 10, 'reg', '2014-10-03 12:56:05.226327', NULL);
INSERT INTO store_action_states VALUES (1, 6, 11, 'accepted', '2014-10-03 12:56:05.226327', NULL);
INSERT INTO store_action_states VALUES (2, 1, 12, 'reg', '2014-10-03 12:58:32.940385', NULL);
INSERT INTO store_action_states VALUES (2, 2, 13, 'unpaid', '2014-10-03 12:58:32.940385', NULL);
INSERT INTO store_action_states VALUES (2, 3, 14, 'unpaid', '2014-10-03 12:58:32.959003', NULL);
INSERT INTO store_action_states VALUES (2, 4, 15, 'reg', '2014-10-03 12:58:36.723602', NULL);
INSERT INTO store_action_states VALUES (2, 4, 16, 'accepted', '2014-10-03 12:58:36.723602', NULL);
INSERT INTO store_action_states VALUES (2, 3, 17, 'paid', '2014-10-03 12:58:36.723602', NULL);
INSERT INTO store_action_states VALUES (2, 3, 18, 'accepted', '2014-10-03 12:58:36.723602', NULL);
INSERT INTO store_action_states VALUES (2, 5, 19, 'reg', '2014-10-03 12:59:21.776845', NULL);
INSERT INTO store_action_states VALUES (2, 5, 20, 'accepted', '2014-10-03 12:59:21.776845', NULL);
INSERT INTO store_action_states VALUES (2, 6, 21, 'reg', '2014-10-03 12:59:21.776845', NULL);
INSERT INTO store_action_states VALUES (2, 6, 22, 'accepted', '2014-10-03 12:59:21.776845', NULL);
INSERT INTO store_action_states VALUES (3, 1, 23, 'reg', '2014-10-03 13:02:04.505442', NULL);
INSERT INTO store_action_states VALUES (3, 2, 24, 'unpaid', '2014-10-03 13:02:04.505442', NULL);
INSERT INTO store_action_states VALUES (3, 3, 25, 'unpaid', '2014-10-03 13:02:04.522927', NULL);
INSERT INTO store_action_states VALUES (3, 4, 26, 'reg', '2014-10-03 13:02:10.264963', NULL);
INSERT INTO store_action_states VALUES (3, 4, 27, 'accepted', '2014-10-03 13:02:10.264963', NULL);
INSERT INTO store_action_states VALUES (3, 3, 28, 'paid', '2014-10-03 13:02:10.264963', NULL);
INSERT INTO store_action_states VALUES (3, 3, 29, 'accepted', '2014-10-03 13:02:10.264963', NULL);
INSERT INTO store_action_states VALUES (3, 5, 30, 'reg', '2014-10-03 13:02:51.452688', NULL);
INSERT INTO store_action_states VALUES (3, 5, 31, 'accepted', '2014-10-03 13:02:51.452688', NULL);
INSERT INTO store_action_states VALUES (3, 6, 32, 'reg', '2014-10-03 13:02:51.452688', NULL);
INSERT INTO store_action_states VALUES (3, 6, 33, 'accepted', '2014-10-03 13:02:51.452688', NULL);
INSERT INTO store_action_states VALUES (4, 1, 34, 'reg', '2014-10-08 16:09:14.273294', NULL);
INSERT INTO store_action_states VALUES (4, 2, 35, 'unpaid', '2014-10-08 16:09:14.273294', NULL);
INSERT INTO store_action_states VALUES (4, 3, 36, 'unpaid', '2014-10-08 16:09:14.283816', NULL);
INSERT INTO store_action_states VALUES (4, 4, 37, 'reg', '2014-10-08 16:09:16.867522', NULL);
INSERT INTO store_action_states VALUES (4, 4, 38, 'accepted', '2014-10-08 16:09:16.867522', NULL);
INSERT INTO store_action_states VALUES (4, 3, 39, 'paid', '2014-10-08 16:09:16.867522', NULL);
INSERT INTO store_action_states VALUES (4, 3, 40, 'accepted', '2014-10-08 16:09:16.867522', NULL);
INSERT INTO store_action_states VALUES (4, 5, 41, 'reg', '2014-10-08 16:10:59.503921', NULL);
INSERT INTO store_action_states VALUES (4, 5, 42, 'accepted', '2014-10-08 16:10:59.503921', NULL);
INSERT INTO store_action_states VALUES (4, 6, 43, 'reg', '2014-10-08 16:10:59.503921', NULL);
INSERT INTO store_action_states VALUES (4, 6, 44, 'accepted', '2014-10-08 16:10:59.503921', NULL);

SELECT pg_catalog.setval('store_action_states_state_id_seq', 44, true);

INSERT INTO store_changes VALUES (1, 4, 4, false, 'status', 'pending', 'inactive');
INSERT INTO store_changes VALUES (1, 5, 8, false, 'name', NULL, 'Tienda test');
INSERT INTO store_changes VALUES (1, 5, 8, false, 'info_text', NULL, 'tienda test
tienda test
tienda test');
INSERT INTO store_changes VALUES (1, 5, 8, false, 'url', NULL, 'tienda-test');
INSERT INTO store_changes VALUES (1, 5, 8, false, 'phone', NULL, '1234567');
INSERT INTO store_changes VALUES (1, 5, 8, false, 'address', NULL, 'Alonso de C�rdova 5670');
INSERT INTO store_changes VALUES (1, 5, 8, false, 'address_number', NULL, '');
INSERT INTO store_changes VALUES (1, 5, 8, false, 'region', NULL, '15');
INSERT INTO store_changes VALUES (1, 5, 8, false, 'commune', NULL, '315');
INSERT INTO store_changes VALUES (1, 6, 10, false, 'status', 'inactive', 'active');
INSERT INTO store_changes VALUES (2, 4, 15, false, 'status', 'pending', 'inactive');
INSERT INTO store_changes VALUES (2, 5, 19, false, 'name', NULL, 'Tienda no-ads');
INSERT INTO store_changes VALUES (2, 5, 19, false, 'info_text', NULL, 'tienda no-ads
tienda no-ads
tienda no-ads');
INSERT INTO store_changes VALUES (2, 5, 19, false, 'url', NULL, 'tienda-no-ads');
INSERT INTO store_changes VALUES (2, 5, 19, false, 'phone', NULL, '6666667');
INSERT INTO store_changes VALUES (2, 5, 19, false, 'address', NULL, '');
INSERT INTO store_changes VALUES (2, 5, 19, false, 'address_number', NULL, '');
INSERT INTO store_changes VALUES (2, 5, 19, false, 'region', NULL, '14');
INSERT INTO store_changes VALUES (2, 6, 21, false, 'status', 'inactive', 'active');
INSERT INTO store_changes VALUES (3, 4, 26, false, 'status', 'pending', 'inactive');
INSERT INTO store_changes VALUES (3, 5, 30, false, 'name', NULL, 'Tienda android');
INSERT INTO store_changes VALUES (3, 5, 30, false, 'info_text', NULL, 'tienda android
tienda android
tienda android');
INSERT INTO store_changes VALUES (3, 5, 30, false, 'url', NULL, 'tienda-android');
INSERT INTO store_changes VALUES (3, 5, 30, false, 'phone', NULL, '98798798787');
INSERT INTO store_changes VALUES (3, 5, 30, false, 'address', NULL, 'mi calle');
INSERT INTO store_changes VALUES (3, 5, 30, false, 'address_number', NULL, '123');
INSERT INTO store_changes VALUES (3, 5, 30, false, 'region', NULL, '15');
INSERT INTO store_changes VALUES (3, 5, 30, false, 'website_url', NULL, 'www.yapo.cl');
INSERT INTO store_changes VALUES (3, 6, 32, false, 'status', 'inactive', 'active');
INSERT INTO store_changes VALUES (4, 4, 37, false, 'status', 'pending', 'inactive');
INSERT INTO store_changes VALUES (4, 5, 41, false, 'name', NULL, 'Usuario01');
INSERT INTO store_changes VALUES (4, 5, 41, false, 'info_text', NULL, 'Esta tienda est� expirada');
INSERT INTO store_changes VALUES (4, 5, 41, false, 'url', NULL, 'usuario01');
INSERT INTO store_changes VALUES (4, 5, 41, false, 'phone', NULL, '123123123');
INSERT INTO store_changes VALUES (4, 5, 41, false, 'address', NULL, 'mi calle');
INSERT INTO store_changes VALUES (4, 5, 41, false, 'address_number', NULL, '123');
INSERT INTO store_changes VALUES (4, 5, 41, false, 'region', NULL, '15');
INSERT INTO store_changes VALUES (4, 6, 43, false, 'status', 'inactive', 'active');

SELECT pg_catalog.setval('stores_store_id_seq', 4, true);


--Change all passwords to 123123123
update ads set salted_passwd =  E'$1024$2y1bQ}VYRD}zF4QCbf07a971b784888658a20e62156de64dfde5ffb6', passwd = null;
update accounts set salted_passwd =  E'$1024$2y1bQ}VYRD}zF4QCbf07a971b784888658a20e62156de64dfde5ffb6';
