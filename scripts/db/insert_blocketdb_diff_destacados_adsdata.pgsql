INSERT INTO ad_params (ad_id, name, value) VALUES (35, 'link_type', 'destacados_cl');
INSERT INTO ad_params (ad_id, name, value) VALUES (35, 'external_ad_id', '1000001');
INSERT INTO ad_params (ad_id, name, value) VALUES (38, 'link_type', 'destacados_cl');
INSERT INTO ad_params (ad_id, name, value) VALUES (38, 'external_ad_id', '1000002');
INSERT INTO ad_params (ad_id, name, value) VALUES (39, 'link_type', 'destacados_cl');
INSERT INTO ad_params (ad_id, name, value) VALUES (39, 'external_ad_id', '1000003');
INSERT INTO ad_params (ad_id, name, value) VALUES (87, 'link_type', 'destacados_cl');
INSERT INTO ad_params (ad_id, name, value) VALUES (87, 'external_ad_id', '1000004');
INSERT INTO ad_params (ad_id, name, value) VALUES (88, 'link_type', 'destacados_cl');
INSERT INTO ad_params (ad_id, name, value) VALUES (88, 'external_ad_id', '1000005');

UPDATE action_states SET timestamp = NOW() WHERE ad_id IN (87, 88) AND state = 'accepted';
UPDATE action_states SET timestamp = NOW() - INTERVAL '4 month' WHERE ad_id = 38 AND state = 'accepted';


--Change all passwords to 123123123
update ads set salted_passwd =  E'$1024$2y1bQ}VYRD}zF4QCbf07a971b784888658a20e62156de64dfde5ffb6', passwd = null;
update accounts set salted_passwd =  E'$1024$2y1bQ}VYRD}zF4QCbf07a971b784888658a20e62156de64dfde5ffb6';
