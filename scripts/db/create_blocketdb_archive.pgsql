
-- XXX Remember to replace any serial types with integer

CREATE TABLE tokens (
  token_id integer,
  token varchar(50) NOT NULL UNIQUE,
  admin_id integer,
  created_at timestamp default NULL,
  valid_to timestamp default NULL,
  remote_addr varchar(20) NOT NULL,
  info varchar(255),
  store_id integer,
  email varchar(60),
  PRIMARY KEY  (token_id)
);

GRANT select,insert,delete,update ON tokens to bwriters;
GRANT select on tokens TO breaders;

CREATE INDEX index_tokens_valid_to ON tokens (valid_to);
CREATE INDEX index_tokens_email ON tokens (email);
CREATE INDEX index_tokens_store_id ON tokens (store_id);
CREATE INDEX index_tokens_admin_id ON tokens (admin_id);

CREATE TABLE payment_groups (
  payment_group_id integer PRIMARY KEY,
  code varchar(20),
  status public.enum_payment_groups_status NOT NULL,
  added_at timestamp NOT NULL,
  parent_payment_group_id integer REFERENCES payment_groups (payment_group_id)
);
CREATE INDEX index_payment_group_code ON payment_groups(code);
CREATE INDEX index_payment_groups_added_at ON payment_groups(added_at);
CREATE INDEX index_payment_groups_status ON payment_groups (status);
CREATE INDEX index_payment_groups_parent_payment_group_id ON payment_groups(parent_payment_group_id);

GRANT select,insert,delete,update ON payment_groups to bwriters;
GRANT select on payment_groups TO breaders;

CREATE TABLE payments (
  payment_group_id integer NOT NULL REFERENCES payment_groups (payment_group_id),
  payment_type public.enum_payments_payment_type NOT NULL,
  pay_amount integer NOT NULL,
  discount integer NOT NULL DEFAULT 0,
  PRIMARY KEY (payment_group_id, payment_type)
);
GRANT select,insert,delete,update ON payments TO bwriters;
GRANT select ON payments TO breaders;

CREATE INDEX index_payments_payment_group ON payments(payment_group_id);
CREATE INDEX index_payment_payment_type ON payments(payment_type);
CREATE INDEX index_payments_pay_amount ON payments(pay_amount);

CREATE TABLE pay_log (
        pay_log_id integer PRIMARY KEY,
        pay_type public.enum_pay_log_pay_type NOT NULL,
        amount integer NOT NULL default 0,
        code varchar(30),
        token_id integer REFERENCES tokens (token_id),
        paid_at timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP,
        payment_group_id integer REFERENCES payment_groups (payment_group_id),
        status public.enum_pay_log_status NOT NULL
);
CREATE INDEX index_pay_log_status ON pay_log (status);
CREATE INDEX index_pay_log_status_paid_at ON pay_log (status, paid_at);
CREATE INDEX index_pay_log_pay_type ON pay_log USING btree (pay_type);
CREATE UNIQUE INDEX unique_pay_log_pay_type_code_paid_at ON pay_log (pay_type, code, paid_at, COALESCE(payment_group_id, 0));
CREATE INDEX index_pay_log_code ON pay_log (code varchar_pattern_ops);
CREATE INDEX index_pay_log_code_1 ON pay_log (code);
CREATE INDEX index_pay_log_paid_at ON pay_log (paid_at);
CREATE INDEX index_pay_log_payment_group_id ON pay_log (payment_group_id);
CREATE INDEX index_pay_log_amount ON pay_log USING btree (amount);
CREATE INDEX index_pay_log_token_id ON pay_log (token_id);

GRANT SELECT, INSERT, DELETE, UPDATE ON pay_log TO bwriters;
GRANT SELECT ON pay_log TO breaders;

CREATE TABLE pay_log_references (
        pay_log_id integer NOT NULL REFERENCES pay_log (pay_log_id),
        ref_num integer NOT NULL DEFAULT 0,
        ref_type public.enum_pay_log_references_ref_type NOT NULL,
        reference varchar(60) NOT NULL,
        PRIMARY KEY (pay_log_id, ref_num)
);
GRANT select,insert,delete,update ON pay_log_references TO bwriters;
GRANT select ON pay_log_references TO breaders;

CREATE INDEX index_pay_log_references_ref_type_reference ON pay_log_references (ref_type, reference varchar_pattern_ops);
CREATE INDEX index_pay_log_references_ref_type_reference_1 ON pay_log_references (ref_type, reference);
CREATE INDEX index_pay_log_references_pay_log_id ON pay_log_references (pay_log_id);

CREATE TABLE voucher_actions (
        voucher_id integer NOT NULL,
        voucher_action_id integer NOT NULL,
        action_type public.enum_voucher_actions_action_type NOT NULL,
        current_state integer,
        state public.enum_voucher_states_state,
        amount integer NOT NULL,
        payment_group_id integer REFERENCES payment_groups (payment_group_id),
        PRIMARY KEY (voucher_id, voucher_action_id)
);
GRANT select,insert,delete,update ON voucher_actions to bwriters;
GRANT select on voucher_actions TO breaders;
CREATE INDEX index_voucher_actions_payment_group_id ON voucher_actions (payment_group_id);
CREATE INDEX index_voucher_actions_voucher_id ON voucher_actions(voucher_id);

CREATE TABLE voucher_states (
        voucher_id integer NOT NULL,
        voucher_action_id integer NOT NULL,
        voucher_state_id integer NOT NULL,
        state public.enum_voucher_states_state NOT NULL,
        transition public.enum_voucher_states_transition NOT NULL,
        timestamp timestamp NOT NULL default CURRENT_TIMESTAMP,
        remote_addr varchar(80),
        token_id integer REFERENCES tokens (token_id),
        resulting_balance integer,
        PRIMARY KEY (voucher_id, voucher_action_id, voucher_state_id),
        FOREIGN KEY (voucher_id, voucher_action_id) REFERENCES voucher_actions (voucher_id, voucher_action_id)
);
GRANT select,insert,delete,update ON voucher_states to bwriters;
GRANT select on voucher_states TO breaders;

CREATE INDEX index_voucher_states_token_id ON voucher_states (token_id);
CREATE INDEX index_voucher_states_voucher_id_voucher_action_id ON voucher_states(voucher_id, voucher_action_id);

CREATE TABLE ads (
        ad_id integer PRIMARY KEY,
        list_id integer UNIQUE,
        list_time timestamp default NULL,
        status public.enum_ads_status NOT NULL,
        type public.enum_ads_type NOT NULL,
        name varchar(100) NOT NULL default '',
        phone varchar(100) NOT NULL default '',
        region smallint NOT NULL,
        city smallint NOT NULL default 0,
        category integer NOT NULL default 0,
        user_id integer NOT NULL default 0,
        passwd varchar(30) default NULL,
        phone_hidden bool NOT NULL default 'f',
        no_salesmen bool NOT NULL default 'f',
        company_ad bool NOT NULL default 'f',
        subject varchar(50) NOT NULL default '',
        body text,
        price bigint default NULL,
        image varchar(20) default NULL,
        infopage text default NULL,
        infopage_title varchar(50) default NULL,
        orig_list_time timestamp default NULL,
        old_price bigint default NULL,
        store_id integer default NULL,
        salted_passwd varchar(100) default NULL,
        modified_at timestamp default NULL,
        lang varchar NOT NULL default 'es'
);

GRANT select,insert,delete,update ON ads to bwriters;
GRANT select on ads TO breaders;

CREATE INDEX index_ads_category ON ads (category);
CREATE INDEX index_ads_category_region ON ads (category, region);
CREATE INDEX index_ads_company_ad ON ads (company_ad);
CREATE INDEX index_ads_status ON ads (status);
CREATE INDEX index_ads_status_ad_id ON ads (status, ad_id);
CREATE INDEX index_ads_status_list_time ON ads (status, list_time);
CREATE INDEX index_ads_list_time ON ads (list_time);
CREATE INDEX index_ads_user_id ON ads (user_id);
CREATE INDEX index_ads_name ON ads (lower(name));
CREATE INDEX index_ads_passwd ON ads (lower(passwd));
CREATE INDEX index_ads_phone ON ads (phone varchar_pattern_ops);
CREATE INDEX index_ads_phone_repl ON ads (replace(replace(phone, '-', ''), ' ', '') varchar_pattern_ops);
CREATE INDEX index_store_id ON ads (store_id);
CREATE INDEX index_ads_subject ON ads (lower(subject) varchar_pattern_ops);
CREATE INDEX index_ads_price ON ads (price);

CREATE TABLE ad_images (
  ad_id integer NOT NULL,
  seq_no smallint default 0 NOT NULL,
  name varchar(20) NOT NULL default '',
  PRIMARY KEY (ad_id, seq_no),
  FOREIGN KEY (ad_id) REFERENCES ads (ad_id)
);
GRANT select,insert,delete,update ON ad_images to bwriters;
GRANT select on ad_images TO breaders;

CREATE TABLE ad_params (
  ad_id integer NOT NULL,
  name public.enum_ad_params_name NOT NULL,
  value text NOT NULL,
  PRIMARY KEY  (ad_id,name),
  FOREIGN KEY (ad_id) REFERENCES ads (ad_id)
);
GRANT select,insert,delete,update ON ad_params to bwriters;
GRANT select on ad_params TO breaders;

CREATE INDEX index_ad_params_name_value ON ad_params (name, value);
CREATE INDEX index_ad_params_ad_id ON ad_params (ad_id);

CREATE TABLE ad_actions (
  ad_id integer NOT NULL,
  action_id integer default 0 NOT NULL,
  action_type public.enum_ad_actions_action_type NOT NULL,
  current_state integer,
  state varchar(16) not null default 'reg',
  queue public.enum_ad_actions_queue NOT NULL default 'normal',
  locked_by integer default NULL,
  locked_until timestamp default NULL,
  payment_group_id integer REFERENCES payment_groups (payment_group_id),
  PRIMARY KEY (ad_id, action_id),
  FOREIGN KEY (ad_id) REFERENCES ads (ad_id)
);

CREATE INDEX index_ad_actions_state_queue ON ad_actions (state, queue);
CREATE INDEX index_ad_actions_queue ON ad_actions (queue);
CREATE INDEX index_ad_actions_state ON ad_actions (state);
CREATE INDEX index_payment_group_id ON ad_actions (payment_group_id);
CREATE INDEX index_ad_actions_action_type ON ad_actions (action_type);
CREATE INDEX index_ad_actions_action_type_ad_id ON ad_actions (action_type, ad_id);
CREATE INDEX index_ad_actions_ad_id ON ad_actions(ad_id);
CREATE INDEX index_ad_actions_locked_by ON ad_actions(locked_by);

GRANT select,insert,delete,update ON ad_actions to bwriters;
GRANT select on ad_actions TO breaders;

CREATE TABLE action_params (
  ad_id integer NOT NULL,
  action_id integer NOT NULL,
  name public.enum_action_params_name NOT NULL,
  value text NOT NULL,
  PRIMARY KEY (ad_id, action_id, name),
  FOREIGN KEY (ad_id, action_id) REFERENCES ad_actions (ad_id, action_id)
);

GRANT select,insert,delete,update ON action_params to bwriters;
GRANT select on action_params TO breaders;

CREATE INDEX index_action_params_name_value ON action_params (name, value);
CREATE INDEX index_action_params_ad_id_action_id ON action_params (ad_id, action_id);

CREATE TABLE action_states (
  ad_id integer NOT NULL,
  action_id integer NOT NULL,
  state_id integer UNIQUE NOT NULL,
  state public.enum_action_states_state NOT NULL default 'reg',
  transition public.enum_action_states_transition NOT NULL default 'initial',
  timestamp timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  remote_addr varchar(20) default '',
  token_id integer REFERENCES tokens (token_id),
  PRIMARY KEY  (ad_id,action_id,state_id),
  FOREIGN KEY (ad_id, action_id) REFERENCES ad_actions (ad_id, action_id)
);
GRANT select,insert,delete,update ON action_states to bwriters;
GRANT select on action_states TO breaders;

CREATE INDEX index_action_states_transition_timestamp ON action_states (transition, timestamp);
CREATE INDEX index_action_states_state ON action_states (state);
CREATE INDEX index_remote_addr ON action_states(remote_addr);
CREATE INDEX index_action_states_token_id ON action_states (token_id);
CREATE INDEX index_action_states_state_ad_id ON action_states (state, ad_id);
CREATE INDEX index_action_states_state_timestamp ON action_states (state, "timestamp");
CREATE INDEX index_action_states_ad_id_action_id_state ON action_states (ad_id, action_id, state);
CREATE INDEX index_action_states_state_reg_timestamp ON action_states ("timestamp", (state = 'reg'));
CREATE INDEX index_action_states_ad_id_action_id ON action_states (ad_id, action_id);

CREATE TABLE state_params (
  ad_id integer NOT NULL,
  action_id integer NOT NULL,
  state_id integer NOT NULL,
  name public.enum_state_params_name NOT NULL,
  value text NOT NULL,
  PRIMARY KEY  (ad_id, action_id, state_id, name),
  FOREIGN KEY (ad_id, action_id, state_id) REFERENCES action_states (ad_id, action_id, state_id)
);
GRANT select,insert,delete,update ON state_params to bwriters;
GRANT select on state_params TO breaders;

CREATE INDEX index_state_params_name_value ON state_params (name, value);
CREATE INDEX index_state_params_ad_id_action_id_state_id ON state_params (ad_id, action_id, state_id);

CREATE TABLE ad_changes (
  ad_id integer NOT NULL,
  action_id integer NOT NULL,
  state_id integer NOT NULL,
  is_param bool NOT NULL,
  column_name varchar(50) NOT NULL,
  old_value text,
  new_value text,
  PRIMARY KEY (ad_id, action_id, state_id, is_param, column_name),
  FOREIGN KEY (ad_id, action_id, state_id) REFERENCES action_states (ad_id, action_id, state_id)
);
GRANT select,insert,delete,update ON ad_changes to bwriters;
GRANT select on ad_changes TO breaders;

CREATE INDEX index_ad_changes_ad_id_action_id_state_id ON ad_changes(ad_id, action_id, state_id);

CREATE TABLE ad_image_changes (
  ad_id integer NOT NULL,
  action_id integer NOT NULL,
  state_id integer NOT NULL,
  seq_no integer NOT NULL,
  name varchar(20),
  is_new bool NOT NULL default 'f',
  PRIMARY KEY (ad_id, action_id, state_id, seq_no),
  FOREIGN KEY (ad_id, action_id, state_id) REFERENCES action_states (ad_id, action_id, state_id)
);
GRANT select,insert,delete,update ON ad_image_changes TO bwriters;
GRANT select ON ad_image_changes TO breaders;

CREATE INDEX index_ad_images_changes_ad_id_action_id_state_id ON ad_image_changes (ad_id, action_id, state_id);

CREATE TABLE ad_media_changes (
  ad_id integer NOT NULL,
  action_id integer NOT NULL,
  state_id integer NOT NULL,
  seq_no integer NOT NULL,
  ad_media_id bigint,
  is_new bool NOT NULL default 'f',
  media_type public.enum_ad_media_media_type NOT NULL DEFAULT 'image',
  PRIMARY KEY (ad_id, action_id, state_id, seq_no),
  FOREIGN KEY (ad_id, action_id, state_id) REFERENCES action_states (ad_id, action_id, state_id),
        CHECK (seq_no = 0 OR ad_media_id IS NOT NULL)
);
GRANT select,insert,delete,update ON ad_media_changes TO bwriters;
GRANT select ON ad_media_changes TO breaders;

CREATE INDEX index_ad_media_changes_ad_id_action_id_state_id ON ad_media_changes (ad_id, action_id, state_id);

CREATE TABLE notices (
  notice_id integer PRIMARY KEY,
  body text NOT NULL,
  uid integer default NULL UNIQUE,
  user_id integer default NULL UNIQUE,
  ad_id integer default NULL UNIQUE REFERENCES ads(ad_id) UNIQUE,
  abuse bool NOT NULL DEFAULT 'f',
  created_at timestamp NOT NULL default CURRENT_TIMESTAMP,
  token_id integer NOT NULL REFERENCES tokens(token_id)
);
GRANT select,insert,delete,update ON notices to bwriters;
GRANT select on notices TO breaders;

CREATE INDEX index_notices_token_id ON notices (token_id);
CREATE INDEX index_notices_user_id ON notices(user_id);
CREATE INDEX index_notices_ad_id ON notices(ad_id);

CREATE TABLE mail_queue (
        mail_queue_id integer NOT NULL PRIMARY KEY DEFAULT NEXTVAL('public.mail_queue_mail_queue_id_seq'),
        state public.enum_mail_queue_state NOT NULL,
        template_name varchar(60) NOT NULL,
        added_at timestamp NOT NULL default CURRENT_TIMESTAMP,
        remote_addr varchar(100) NOT NULL,
        sender_email varchar(200) NOT NULL,
        sender_name varchar(200) NOT NULL,
        receipient_email varchar(200) NOT NULL,
        receipient_name varchar(100) NOT NULL,
        subject varchar(100) NOT NULL,
        body text NOT NULL,
        list_id integer,
        rule_id integer default NULL,
        reason varchar(255) default NULL,
        sender_phone varchar(100) default NULL
);
CREATE INDEX mail_queue_remote_addr_state ON mail_queue (remote_addr, state);

CREATE INDEX index_mail_queue_added_at ON mail_queue (added_at);
CREATE INDEX index_mail_queue_sender_name ON mail_queue (lower(sender_name) varchar_pattern_ops);
CREATE INDEX index_mail_queue_sender_email ON mail_queue (lower(sender_email) varchar_pattern_ops);
CREATE INDEX index_mail_queue_receipient_name ON mail_queue (lower(receipient_name) varchar_pattern_ops);
CREATE INDEX index_mail_queue_receipient_email ON mail_queue (lower(receipient_email) varchar_pattern_ops);
CREATE INDEX index_mail_queue_state_spam ON mail_queue (state)  WHERE state = ANY (ARRAY['manual'::public.enum_mail_queue_state, 'abuse'::public.enum_mail_queue_state]);

CREATE INDEX mail_queue_list_id ON mail_queue (list_id);

GRANT select,insert,delete,update ON mail_queue TO bwriters;
GRANT select ON mail_queue TO breaders;

-- ad_media is not archived, but table is created for dump_ads 
CREATE TABLE ad_media (
        ad_media_id bigint PRIMARY KEY,
        ad_id INTEGER REFERENCES ads(ad_id),
        seq_no SMALLINT DEFAULT 0 NOT NULL,
        upload_time TIMESTAMP NOT NULL,
        media_type public.enum_ad_media_media_type NOT NULL DEFAULT 'image',
        width integer,
        height integer,
        length integer
);
CREATE INDEX index_ad_media_ad_id ON ad_media (ad_id);

GRANT select,insert,delete,update ON ad_media TO bwriters;
GRANT select ON ad_media TO breaders;

CREATE TABLE purchase (
	purchase_id	 	integer NOT NULL,
	doc_type		public.enum_purchase_doc_type NOT NULL,
	doc_num			bigint,
	external_doc_id		bigint,
	status			public.enum_purchase_status NOT NULL,
	receipt			timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	delivery		timestamp,
        rut                     character varying(12),
        name                    character varying(200),
        lob                     character varying(200),
        address                 character varying(500),
        region                  smallint,
        communes                smallint,
        contact                 character varying(200),
        email                   character varying(80) NOT NULL,
	tax			integer NOT NULL,
	discount		integer NOT NULL default(0),
	seller_id               integer default NULL,
	total_price		integer NOT NULL,
        payment_group_id        integer,
	payment_method	public.enum_purchase_payment_method DEFAULT	'webpay'::public.enum_purchase_payment_method NOT NULL,
	payment_platform	public.enum_purchase_payment_platform DEFAULT 'unknown'::public.enum_purchase_payment_platform NOT NULL,
	account_id 				integer,
	PRIMARY KEY (purchase_id),
	FOREIGN KEY (payment_group_id) REFERENCES payment_groups(payment_group_id)
);
CREATE INDEX index_purchase_id ON purchase(purchase_id);
CREATE INDEX index_purchase_doc ON purchase(doc_type, doc_num) WHERE doc_num is not null;
CREATE INDEX index_purchase_rut ON purchase(rut) where rut is not null and doc_num is not null;
CREATE INDEX index_purchase_email ON purchase(email);

GRANT select,insert,delete,update ON purchase TO bwriters;
GRANT select ON purchase TO breaders;

CREATE TABLE purchase_detail (
	purchase_detail_id	integer NOT NULL,
	purchase_id 		integer NOT NULL,
	product_id 		integer NOT NULL,
	price			integer NOT NULL,
	action_id		integer,
	ad_id			integer NOT NULL,
	payment_group_id integer not null default 0,
	FOREIGN KEY(purchase_id) REFERENCES purchase (purchase_id) ON DELETE CASCADE,
	PRIMARY KEY(purchase_id, purchase_detail_id)
);
CREATE INDEX index_purchase_detail_id on purchase_detail(purchase_id,purchase_detail_id);
CREATE INDEX index_puchase_detail_ad_product on purchase_detail(ad_id, product_id);

GRANT select,insert,delete,update ON purchase_detail TO bwriters;
GRANT select ON purchase_detail TO breaders;

CREATE TABLE purchase_states (
	purchase_id 		integer NOT NULL,
	purchase_state_id	integer NOT NULL,
	status			public.enum_purchase_status NOT NULL,
	timestamp 		timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	remote_addr 		varchar(20) default '',
	FOREIGN KEY(purchase_id) REFERENCES purchase (purchase_id) on DELETE CASCADE,
	PRIMARY KEY(purchase_id, purchase_state_id)
);
CREATE INDEX index_purchase_states ON purchase_states (purchase_id, purchase_state_id);

GRANT select,insert,delete,update ON purchase_states TO bwriters;
GRANT select ON purchase_states TO breaders;

-- Stats -- for redis stats dumped data
CREATE TABLE stats(
	stat_id 	serial,
	stat_type 	public.enum_stats_type NOT NULL,
	ext_id 		integer NOT NULL,
	total  		integer NOT NULL DEFAULT 0,
	PRIMARY KEY(stat_id)
);

CREATE INDEX index_stat_id on stats(stat_id);
CREATE INDEX index_stat_search on stats(stat_type,ext_id);

GRANT select,insert,delete,update ON stats TO bwriters;
GRANT select ON stats TO breaders;

CREATE TABLE stats_detail(
	stat_id 	integer NOT NULL,
	reg_date 	date NOT NULL,
	reg_hour 	time NOT NULL,
	value 		integer NOT NULL,
	FOREIGN KEY(stat_id) REFERENCES stats(stat_id) on DELETE CASCADE,
	PRIMARY KEY(stat_id,reg_date,reg_hour)
);

CREATE INDEX stats_detail_id on stats_detail(stat_id,reg_date,reg_hour);

GRANT select,insert,delete,update ON stats_detail TO bwriters;
GRANT select ON stats_detail TO breaders;
-- vim:syntax=sql
