BEGIN;
SET CONSTRAINTS ALL DEFERRED;

TRUNCATE TABLE public.admins CASCADE;
TRUNCATE TABLE public.abuse_locks CASCADE;
TRUNCATE TABLE public.abuse_reporters CASCADE;
TRUNCATE TABLE public.users CASCADE;
TRUNCATE TABLE public.abuse_reports CASCADE;
TRUNCATE TABLE public.ads CASCADE;
TRUNCATE TABLE public.dashboard_ads CASCADE;
TRUNCATE TABLE public.payment_groups CASCADE;
TRUNCATE TABLE public.ad_actions CASCADE;
TRUNCATE TABLE public.action_params CASCADE;
TRUNCATE TABLE public.stores CASCADE;
TRUNCATE TABLE public.tokens CASCADE;
TRUNCATE TABLE public.action_states CASCADE;
TRUNCATE TABLE public.ad_changes CASCADE;
TRUNCATE TABLE public.ad_codes CASCADE;
TRUNCATE TABLE public.ad_image_changes CASCADE;
TRUNCATE TABLE public.ad_images CASCADE;
TRUNCATE TABLE public.ad_images_digests CASCADE;
TRUNCATE TABLE public.ad_media CASCADE;
TRUNCATE TABLE public.ad_media_changes CASCADE;
TRUNCATE TABLE public.ad_params CASCADE;
TRUNCATE TABLE public.ad_queues CASCADE;
TRUNCATE TABLE public.admin_privs CASCADE;
TRUNCATE TABLE public.bid_ads CASCADE;
TRUNCATE TABLE public.bid_bids CASCADE;
TRUNCATE TABLE public.bid_media CASCADE;
TRUNCATE TABLE public.block_lists CASCADE;
TRUNCATE TABLE public.block_rules CASCADE;
TRUNCATE TABLE public.block_rule_conditions CASCADE;
TRUNCATE TABLE public.blocked_items CASCADE;
TRUNCATE TABLE public.conf CASCADE;
TRUNCATE TABLE public.event_log CASCADE;
TRUNCATE TABLE public.example CASCADE;
TRUNCATE TABLE public.filters CASCADE;
TRUNCATE TABLE public.hold_mail_params CASCADE;
TRUNCATE TABLE public.iteminfo_items CASCADE;
TRUNCATE TABLE public.iteminfo_data CASCADE;
TRUNCATE TABLE public.mail_log CASCADE;
TRUNCATE TABLE public.mail_queue CASCADE;
TRUNCATE TABLE public.mama_attribute_wordlists CASCADE;
TRUNCATE TABLE public.mama_attribute_categories CASCADE;
TRUNCATE TABLE public.mama_main_backup CASCADE;
TRUNCATE TABLE public.mama_attribute_categories_backup CASCADE;
TRUNCATE TABLE public.mama_attribute_wordlists_backup CASCADE;
TRUNCATE TABLE public.mama_attribute_words CASCADE;
TRUNCATE TABLE public.mama_attribute_words_backup CASCADE;
TRUNCATE TABLE public.mama_exception_lists CASCADE;
TRUNCATE TABLE public.mama_exception_lists_backup CASCADE;
TRUNCATE TABLE public.mama_exception_words CASCADE;
TRUNCATE TABLE public.mama_exception_words_backup CASCADE;
TRUNCATE TABLE public.mama_wordlists CASCADE;
TRUNCATE TABLE public.mama_wordlists_backup CASCADE;
TRUNCATE TABLE public.mama_words CASCADE;
TRUNCATE TABLE public.mama_words_backup CASCADE;
TRUNCATE TABLE public.most_popular_ads CASCADE;
TRUNCATE TABLE public.notices CASCADE;
TRUNCATE TABLE public.on_call CASCADE;
TRUNCATE TABLE public.on_call_actions CASCADE;
TRUNCATE TABLE public.pageviews_per_reg_cat CASCADE;
TRUNCATE TABLE public.pay_log CASCADE;
TRUNCATE TABLE public.pay_log_references CASCADE;
TRUNCATE TABLE public.payments CASCADE;
TRUNCATE TABLE public.purchase CASCADE;
TRUNCATE TABLE public.pricelist CASCADE;
TRUNCATE TABLE public.redir_stats CASCADE;
TRUNCATE TABLE public.review_log CASCADE;
TRUNCATE TABLE public.sms_users CASCADE;
TRUNCATE TABLE public.sms_log CASCADE;
TRUNCATE TABLE public.watch_users CASCADE;
TRUNCATE TABLE public.watch_queries CASCADE;
TRUNCATE TABLE public.sms_log_watch CASCADE;
TRUNCATE TABLE public.state_params CASCADE;
TRUNCATE TABLE public.stats_daily CASCADE;
TRUNCATE TABLE public.stats_daily_ad_actions CASCADE;
TRUNCATE TABLE public.stats_hourly CASCADE;
TRUNCATE TABLE public.store_actions CASCADE;
TRUNCATE TABLE public.store_action_states CASCADE;
TRUNCATE TABLE public.store_changes CASCADE;
TRUNCATE TABLE public.store_login_tokens CASCADE;
TRUNCATE TABLE public.store_params CASCADE;
TRUNCATE TABLE public.synonyms CASCADE;
TRUNCATE TABLE public.trans_queue CASCADE;
TRUNCATE TABLE public.unfinished_ads CASCADE;
TRUNCATE TABLE public.user_params CASCADE;
TRUNCATE TABLE public.user_testimonial CASCADE;
TRUNCATE TABLE public.visitor CASCADE;
TRUNCATE TABLE public.visits CASCADE;
TRUNCATE TABLE public.vouchers CASCADE;
TRUNCATE TABLE public.voucher_actions CASCADE;
TRUNCATE TABLE public.voucher_states CASCADE;
TRUNCATE TABLE public.watch_ads CASCADE;
TRUNCATE TABLE public.accounts CASCADE;
TRUNCATE TABLE public.account_params CASCADE;
TRUNCATE TABLE public.packs CASCADE;
TRUNCATE TABLE blocket_2015.stats CASCADE;

COMMIT;

--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'LATIN1';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;


SET search_path = public, pg_catalog;

--
-- Data for Name: admins; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO admins (admin_id, username, passwd, fullname, jabbername, email, mobile, status) VALUES (2, 'erik', '2d541162141d281abb9f5ba1b14f8072a1d2d29b', 'Erik', 'erik@jabber', 'erik@blocket.se', '070-111111', 'active');
INSERT INTO admins (admin_id, username, passwd, fullname, jabbername, email, mobile, status) VALUES (3, 'zakay', '2d541162141d281abb9f5ba1b14f8072a1d2d29b', 'Zakay', 'zakay@jabber', 'zakay@blocket.se', '070-111111', 'active');
INSERT INTO admins (admin_id, username, passwd, fullname, jabbername, email, mobile, status) VALUES (4, 'thomas', 'a94a8fe5ccb19ba61c4c0873d391e987982fbbd3', 'Thomas', 'thomas@jabber', 'test@schibstediberica.es', '070-111111', 'active');
INSERT INTO admins (admin_id, username, passwd, fullname, jabbername, email, mobile, status) VALUES (5, 'kjell', '430985da851e9223368e820ebde5beaf6c6ef1cc', 'Kjell', 'kjell@jabber', 'kjell@blocket.se', '0709-6459256', 'active');
INSERT INTO admins (admin_id, username, passwd, fullname, jabbername, email, mobile, status) VALUES (6, 'torsten', '430985da851e9223368e820ebde5beaf6c6ef1cc', 'Torsten Svensson', 'torsten@jabber', 'svara-inte@blocket.se', '0709-6459256', 'deleted');
INSERT INTO admins (admin_id, username, passwd, fullname, jabbername, email, mobile, status) VALUES (50, 'mama', '67b757a4f76e5fda3fe86c8a9009630a5e53a053', 'M.A.M.A.', 'blocket1@jabber', 'blocket1@blocket.se', '0733555501', 'active');
INSERT INTO admins (admin_id, username, passwd, fullname, jabbername, email, mobile, status) VALUES (51, 'bender00', '67b757a4f76e5fda3fe86c8a9009630a5e53a053', 'M.A.M.A. bender 00', NULL, NULL, NULL, 'active');
INSERT INTO admins (admin_id, username, passwd, fullname, jabbername, email, mobile, status) VALUES (52, 'bender01', '67b757a4f76e5fda3fe86c8a9009630a5e53a053', 'M.A.M.A. bender 01', NULL, NULL, NULL, 'active');
INSERT INTO admins (admin_id, username, passwd, fullname, jabbername, email, mobile, status) VALUES (53, 'bender02', '67b757a4f76e5fda3fe86c8a9009630a5e53a053', 'M.A.M.A. bender 02', NULL, NULL, NULL, 'active');
INSERT INTO admins (admin_id, username, passwd, fullname, jabbername, email, mobile, status) VALUES (54, 'bender03', '67b757a4f76e5fda3fe86c8a9009630a5e53a053', 'M.A.M.A. bender 03', NULL, NULL, NULL, 'active');
INSERT INTO admins (admin_id, username, passwd, fullname, jabbername, email, mobile, status) VALUES (55, 'bender04', '67b757a4f76e5fda3fe86c8a9009630a5e53a053', 'M.A.M.A. bender 04', NULL, NULL, NULL, 'active');
INSERT INTO admins (admin_id, username, passwd, fullname, jabbername, email, mobile, status) VALUES (9, 'dany', '118ab5d614b5a8d4222fc7eee1609793e4014800', NULL, NULL, 'dany@mazaru.schibsted.cl', NULL, 'active');
INSERT INTO admins (admin_id, username, passwd, fullname, jabbername, email, mobile, status) VALUES (23, 'blocket', '430985da851e9223368e820ebde5beaf6c6ef1cc', 'Blocket', NULL, 'blocket@yapo.cl', NULL, 'active');




--
-- Name: abuse_locks_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('abuse_locks_id_seq', 1, false);


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO users (user_id, uid, email, account, paid_total) VALUES (1, 1, 'uid1@blocket.se', 0, 0);
INSERT INTO users (user_id, uid, email, account, paid_total) VALUES (2, 2, 'uid2@blocket.se', 0, 100);
INSERT INTO users (user_id, uid, email, account, paid_total) VALUES (3, 3, 'prepaid3@blocket.se', 1000, 1000);
INSERT INTO users (user_id, uid, email, account, paid_total) VALUES (4, 4, 'prepaid@blocket.se', 100000, 1000);
INSERT INTO users (user_id, uid, email, account, paid_total) VALUES (5, 5, 'prepaid5@blocket.se', 1000, 1000);
INSERT INTO users (user_id, uid, email, account, paid_total) VALUES (6, 6, 'kim@blocket.se', 1000, 1000);
INSERT INTO users (user_id, uid, email, account, paid_total) VALUES (50, 50, 'noupselling@yapo.cl', 0, 0);
INSERT INTO users (user_id, uid, email, account, paid_total) VALUES (51, 50, 'gupselling@yapo.cl', 0, 3600);
INSERT INTO users (user_id, uid, email, account, paid_total) VALUES (52, 50, 'wupselling@yapo.cl', 0, 2700);
INSERT INTO users (user_id, uid, email, account, paid_total) VALUES (53, 50, 'dupselling@yapo.cl', 0, 4500);
INSERT INTO users (user_id, uid, email, account, paid_total) VALUES (54, 50, 'editupselling@yapo.cl', 0, 4500);
INSERT INTO users (user_id, uid, email, account, paid_total) VALUES (55, 50, 'renewupselling@yapo.cl', 0, 2700);

INSERT INTO accounts VALUES (1, 'BLOCKET', '100000-4', 'prepaid5@blocket.se', '987654321', true, 15, '$1024$2y1bQ}VYRD}zF4QCbf07a971b784888658a20e62156de64dfde5ffb6', '2014-01-14 15:49:48.005114', NULL, 'active', 'Alonso de C�rdova 5670', 'BLOCKET', 'Technology', 315, 5);


--
-- Data for Name: tokens; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (99999917, 'd12ac643ce5c39ddb765bc452d40790932d7c0f67823aaa', 5, '2006-04-06 15:19:46', '2006-04-06 16:19:46', '192.168.4.75', 'clear', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (99999918, 'a5e4e5b16bc9c806b1c22cfd638440034791acc68f23600', 5, '2006-04-06 16:19:31', '2006-04-06 16:19:46', '192.168.4.75', 'authenticate', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (99999919, 'a80cbcd0b019780754593a1ea8b28001c88086439101100', 5, '2006-04-06 16:19:46', '2006-04-06 16:19:46', '192.168.4.75', 'clear', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (99999992, 'b7e4e5b16bc9c806b1c22cfd638440034791acc68f23600', 5, '2011-11-06 05:19:31', '2011-11-06 05:19:46', '192.168.4.75', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (99999993, 'b9e4e5b16bc9c806b1c22cfd638440034791acc68f23600', 5, '2011-11-06 05:19:31', '2011-11-06 05:19:46', '192.168.4.75', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (99999998, 'b5e4e5b16bc9c806b1c22cfd638440034791acc68f23600', 5, '2006-04-06 15:19:31', '2006-04-06 15:19:46', '192.168.4.75', 'authenticate', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (99999999, '580cbcd0b019780754593a1ea8b28001c88086439101100', 5, '2006-04-06 15:19:46', '2006-04-06 16:19:46', '192.168.4.75', 'clear', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (99999994, 'b7e4e9b16bc9c806b1c22cfd638440034791acc68f23600', 5, '2011-11-06 05:19:31', '2011-11-06 05:19:46', '192.168.4.75', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (99999995, 'b9e6e6b16bc9c806b1c22cfd638440034791acc68f23600', 5, '2011-11-06 06:19:31', '2011-11-06 06:19:46', '192.168.4.75', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (1, 'X5593f81f6548499f000000006cb8990100000000', 9, '2015-07-01 11:24:31.325152', '2015-07-01 11:24:31.448534', '10.0.1.101', 'authenticate', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (2, 'X5593f81f5ce9a204000000005a9a3c1a00000000', 9, '2015-07-01 11:24:31.448534', '2015-07-01 11:24:37.461718', '10.0.1.101', 'spamfilter', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (3, 'X5593f8255db42895000000002b343c2300000000', 9, '2015-07-01 11:24:37.461718', '2015-07-01 11:24:42.867048', '10.0.1.101', 'listadmins', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (4, 'X5593f82b24a9e7db000000002eaa75cd00000000', 9, '2015-07-01 11:24:42.867048', '2015-07-01 11:24:42.876819', '10.0.1.101', 'listadmins', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (5, 'X5593f82b1d4354aa0000000059e4db0200000000', 9, '2015-07-01 11:24:42.876819', '2015-07-01 11:24:45.248208', '10.0.1.101', 'search_ads', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (6, 'X5593f82d41aba7f1000000004958fc9000000000', 9, '2015-07-01 11:24:45.248208', '2015-07-01 11:24:45.308533', '10.0.1.101', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (7, 'X5593f82d7b3944570000000019a3f3ab00000000', 9, '2015-07-01 11:24:45.308533', '2015-07-01 11:24:45.31419', '10.0.1.101', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (8, 'X5593f82d470833b3000000004ef0c08100000000', 9, '2015-07-01 11:24:45.31419', '2015-07-01 11:24:45.318718', '10.0.1.101', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (9, 'X5593f82dd0527ea0000000019d1d59d00000000', 9, '2015-07-01 11:24:45.318718', '2015-07-01 11:27:01.4584', '10.0.1.101', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (10, 'X5593f8b56614cfee000000001ff7bbe800000000', 9, '2015-07-01 11:27:01.4584', '2015-07-01 11:27:01.614082', '10.0.1.101', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (11, 'X5593f8b62d01e6c300000000184349c900000000', 9, '2015-07-01 11:27:01.614082', '2015-07-01 11:27:01.623762', '10.0.1.101', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (12, 'X5593f8b668fe3e990000000072583ae200000000', 9, '2015-07-01 11:27:01.623762', '2015-07-01 11:27:01.631994', '10.0.1.101', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (13, 'X5593f8b6132905960000000050f730c00000000', 9, '2015-07-01 11:27:01.631994', '2015-07-01 11:27:38.744517', '10.0.1.101', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (14, 'X5593f8db6a2a96da00000000acf4f3900000000', 9, '2015-07-01 11:27:38.744517', '2015-07-01 11:27:38.802088', '10.0.1.101', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (15, 'X5593f8db5acadbf3000000004e6b561c00000000', 9, '2015-07-01 11:27:38.802088', '2015-07-01 11:27:38.810954', '10.0.1.101', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (16, 'X5593f8db735277ff00000000695dd66700000000', 9, '2015-07-01 11:27:38.810954', '2015-07-01 11:27:38.818014', '10.0.1.101', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (17, 'X5593f8db6c973a440000000025acf29800000000', 9, '2015-07-01 11:27:38.818014', '2015-07-01 11:28:04.732832', '10.0.1.101', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (18, 'X5593f8f54b3a1918000000006bbed4ce00000000', 9, '2015-07-01 11:28:04.732832', '2015-07-01 11:28:04.78132', '10.0.1.101', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (19, 'X5593f8f56fa793400000000020e06f2f00000000', 9, '2015-07-01 11:28:04.78132', '2015-07-01 11:28:04.788967', '10.0.1.101', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (20, 'X5593f8f591b34ef0000000061556ef700000000', 9, '2015-07-01 11:28:04.788967', '2015-07-01 11:28:04.79629', '10.0.1.101', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (21, 'X5593f8f51ba1189a0000000051e699c300000000', 9, '2015-07-01 11:28:04.79629', '2015-07-01 11:29:05.014851', '10.0.1.101', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (22, 'X5593f9316cc228f5000000006da88fad00000000', 9, '2015-07-01 11:29:05.014851', '2015-07-01 11:29:05.070631', '10.0.1.101', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (23, 'X5593f93125036be40000000078262f3c00000000', 9, '2015-07-01 11:29:05.070631', '2015-07-01 11:29:05.078903', '10.0.1.101', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (24, 'X5593f931c953238000000001482764300000000', 9, '2015-07-01 11:29:05.078903', '2015-07-01 11:29:05.086617', '10.0.1.101', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (25, 'X5593f93116a206af0000000072aa022700000000', 9, '2015-07-01 11:29:05.086617', '2015-07-01 11:29:40.555516', '10.0.1.101', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (26, 'X5593f955347a322c0000000043a3ed7200000000', 9, '2015-07-01 11:29:40.555516', '2015-07-01 11:29:40.587305', '10.0.1.101', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (27, 'X5593f955aed4bf0000000001d7870c500000000', 9, '2015-07-01 11:29:40.587305', '2015-07-01 11:29:40.591293', '10.0.1.101', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (28, 'X5593f95535fc2855000000001e16518600000000', 9, '2015-07-01 11:29:40.591293', '2015-07-01 11:29:40.595425', '10.0.1.101', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (29, 'X5593f9552287e3d2000000002026bf2f00000000', 9, '2015-07-01 11:29:40.595425', '2015-07-01 11:30:11.68685', '10.0.1.101', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (30, 'X5593f97428e5a0bf000000007d52bfc500000000', 9, '2015-07-01 11:30:11.68685', '2015-07-01 11:30:11.732205', '10.0.1.101', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (31, 'X5593f9746e92154b000000001c3818bf00000000', 9, '2015-07-01 11:30:11.732205', '2015-07-01 11:30:11.738644', '10.0.1.101', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (32, 'X5593f97466b0962d000000005b294f9000000000', 9, '2015-07-01 11:30:11.738644', '2015-07-01 11:30:11.744546', '10.0.1.101', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (33, 'X5593f97441e50b570000000031eaaf4500000000', 9, '2015-07-01 11:30:11.744546', '2015-07-01 11:30:43.290472', '10.0.1.101', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (34, 'X5593f99346e8245e00000000318c9e9700000000', 9, '2015-07-01 11:30:43.290472', '2015-07-01 11:30:43.333678', '10.0.1.101', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (35, 'X5593f99352cb1e74000000005003594d00000000', 9, '2015-07-01 11:30:43.333678', '2015-07-01 11:30:43.339998', '10.0.1.101', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (36, 'X5593f99312e20d8f000000006e6c370f00000000', 9, '2015-07-01 11:30:43.339998', '2015-07-01 11:30:43.345806', '10.0.1.101', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (37, 'X5593f99321e9f310000000007fa4368400000000', 9, '2015-07-01 11:30:43.345806', '2015-07-01 11:30:59.35749', '10.0.1.101', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (38, 'X5593f9a35c14c6bc0000000046ed5ef500000000', 9, '2015-07-01 11:30:59.35749', '2015-07-01 11:30:59.398559', '10.0.1.101', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (39, 'X5593f9a377ca65c00000000068a9f8f500000000', 9, '2015-07-01 11:30:59.398559', '2015-07-01 11:30:59.40502', '10.0.1.101', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (40, 'X5593f9a35b6fd53800000000e6c6c6f00000000', 9, '2015-07-01 11:30:59.40502', '2015-07-01 11:30:59.412336', '10.0.1.101', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (41, 'X5593f9a35b53fb1c00000000fea076400000000', 9, '2015-07-01 11:30:59.412336', '2015-07-01 11:32:14.790503', '10.0.1.101', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (42, 'X5593f9ef619f058c000000003b02a53000000000', 9, '2015-07-01 11:32:14.790503', '2015-07-01 11:32:14.903392', '10.0.1.101', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (43, 'X5593f9ef4933bc64000000007d79837400000000', 9, '2015-07-01 11:32:14.903392', '2015-07-01 11:32:14.91085', '10.0.1.101', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (44, 'X5593f9ef69342be6000000005a420d9d00000000', 9, '2015-07-01 11:32:14.91085', '2015-07-01 11:32:14.91837', '10.0.1.101', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (45, 'X5593f9ef26ae058900000000460e14be00000000', 9, '2015-07-01 11:32:14.91837', '2015-07-01 11:32:33.084534', '10.0.1.101', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (46, 'X5593fa0147e3a5ae000000007ead0edb00000000', 9, '2015-07-01 11:32:33.084534', '2015-07-01 11:32:33.141116', '10.0.1.101', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (47, 'X5593fa014087d0db000000006b2910f500000000', 9, '2015-07-01 11:32:33.141116', '2015-07-01 11:32:33.149763', '10.0.1.101', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (48, 'X5593fa01619d96d900000000582a135e00000000', 9, '2015-07-01 11:32:33.149763', '2015-07-01 11:32:33.156707', '10.0.1.101', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (49, 'X5593fa01123d3f75000000007cdef7d200000000', 9, '2015-07-01 11:32:33.156707', '2015-07-01 11:33:38.342646', '10.0.1.101', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (50, 'X5593fa4253429b470000000078b03bfc00000000', 9, '2015-07-01 11:33:38.342646', '2015-07-01 11:33:38.372206', '10.0.1.101', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (62, 'X5593fafb651ee15000000007a61158d00000000', 9, '2015-07-01 11:36:42.512678', '2015-07-01 11:38:58.388402', '10.0.1.101', 'review', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (51, 'X5593fa424bf83efc000000003c3f8a100000000', 9, '2015-07-01 11:33:38.372206', '2015-07-01 11:33:38.377588', '10.0.1.101', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (63, 'X5593fb8274efbae20000000049cb2bfd00000000', 9, '2015-07-01 11:38:58.388402', '2015-07-01 11:39:01.245163', '10.0.1.101', 'spamfilter', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (52, 'X5593fa4215465b3c000000007b85568a00000000', 9, '2015-07-01 11:33:38.377588', '2015-07-01 11:33:38.382288', '10.0.1.101', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (53, 'X5593fa4254ea287d0000000077add23b00000000', 9, '2015-07-01 11:33:38.382288', '2015-07-01 11:34:06.241897', '10.0.1.101', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (64, 'X5593fb853edfd11e0000000061c58a5d00000000', 9, '2015-07-01 11:39:01.245163', '2015-07-01 11:39:06.712708', '10.0.1.101', 'listadmins', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (54, 'X5593fa5e48cd0820000000002a0f7a4100000000', 9, '2015-07-01 11:34:06.241897', '2015-07-01 11:34:06.288734', '10.0.1.101', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (55, 'X5593fa5e546985bb000000001e000c8200000000', 9, '2015-07-01 11:34:06.288734', '2015-07-01 11:34:06.296428', '10.0.1.101', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (65, 'X5593fb8b3b7157b700000000694544d500000000', 9, '2015-07-01 11:39:06.712708', '2015-07-01 11:39:06.722091', '10.0.1.101', 'listadmins', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (56, 'X5593fa5e3e1cbe40000000050a61e8400000000', 9, '2015-07-01 11:34:06.296428', '2015-07-01 11:34:06.302784', '10.0.1.101', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (57, 'X5593fa5ef592762000000006580d17000000000', 9, '2015-07-01 11:34:06.302784', '2015-07-01 11:34:42.696569', '10.0.1.101', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (66, 'X5593fb8b2db3cdd600000000476779d600000000', 9, '2015-07-01 11:39:06.722091', '2015-07-01 11:39:09.450823', '10.0.1.101', 'search_ads', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (58, 'X5593fa83ba8c3b500000000588ce3c600000000', 9, '2015-07-01 11:34:42.696569', '2015-07-01 11:34:42.727253', '10.0.1.101', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (59, 'X5593fa8362fa54e40000000074dcef9b00000000', 9, '2015-07-01 11:34:42.727253', '2015-07-01 11:34:42.731592', '10.0.1.101', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (67, 'X5593fb8d6c3bc7020000000079617afd00000000', 9, '2015-07-01 11:39:09.450823', '2015-07-01 11:39:09.504859', '10.0.1.101', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (60, 'X5593fa8332cef164000000009a85a6d00000000', 9, '2015-07-01 11:34:42.731592', '2015-07-01 11:34:42.735646', '10.0.1.101', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (61, 'X5593fa833aeb0459000000007ab2971200000000', 9, '2015-07-01 11:34:42.735646', '2015-07-01 11:36:42.512678', '10.0.1.101', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (68, 'X5593fb8e47182317000000005ffd1c4000000000', 9, '2015-07-01 11:39:09.504859', '2015-07-01 11:39:09.509851', '10.0.1.101', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (69, 'X5593fb8e5ef8afe50000000064e2ab2d00000000', 9, '2015-07-01 11:39:09.509851', '2015-07-01 11:39:09.513736', '10.0.1.101', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (70, 'X5593fb8e344d114100000000396ab4fc00000000', 9, '2015-07-01 11:39:09.513736', '2015-07-01 11:39:12.930625', '10.0.1.101', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (71, 'X5593fb916d1f3308000000005ab1d4ff00000000', 9, '2015-07-01 11:39:12.930625', '2015-07-01 11:39:20.261606', '10.0.1.101', 'review', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (72, 'X5593fb98637f69f00000000010f7928d00000000', 9, '2015-07-01 11:39:20.261606', '2015-07-01 11:39:20.288006', '10.0.1.101', 'listadmins', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (73, 'X5593fb9844aa0ded000000002f0dcca500000000', 9, '2015-07-01 11:39:20.288006', '2015-07-01 12:39:20.288006', '10.0.1.101', 'search_ads', NULL, NULL);


--
-- Data for Name: account_actions; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Data for Name: account_params; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Name: accounts_account_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('accounts_account_id_seq', 2, false);


--
-- Data for Name: ads; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (1, 6000667, '2011-11-09 11:00:00', 'active', 'sell', 'Aurelio Rodr�guez', '1231231231', 2, 0, 1020, 5, '11111', false, false, false, 'Departamento Tarapac�', 'Con 2 dormitorios, en Alto Hospicio, 250m2 de espacio y 2 plazas de garaje', 45000000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'es');
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (2, NULL, NULL, 'inactive', 'sell', 'Thomas Svensson', '08-112233', 11, 0, 4100, 3, 'testb', false, false, false, 'Barncykel', 'Röd barncykel, 28 tum.', 666, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'es');
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (3, NULL, NULL, 'inactive', 'sell', 'Sven Ingvars', '08-121314', 11, 0, 4100, 5, 'testc', false, false, false, 'Damcykel', 'Damcykel med stång i mitten. 33,2 tum i plast.', 667, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'es');
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (4, NULL, NULL, 'inactive', 'sell', 'Klas Klasson', '08-121314', 11, 0, 4100, 5, 'testc', true, true, true, 'Hustomte', 'En tre fot stor hustomte.', 667, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'es');
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (5, NULL, NULL, 'inactive', 'sell', 'Dummy', '08-121314', 11, 0, 4100, 5, 'testc', false, false, false, 'Damcykel', 'Damcykel med stång i mitten. 33,2 tum i plast.', 667, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'es');
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (6, 3456789, '2006-04-05 09:21:31', 'active', 'sell', 'Bengt Bedrup', '08-121314', 10, 0, 2020, 5, '11111', true, true, false, 'Race car', 'Car with two doors ...
33.2-inch wheels made of plastic.', 11667, NULL, NULL, 'Testar länk', NULL, NULL, NULL, NULL, NULL, 'es');
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (7, NULL, NULL, 'inactive', 'sell', 'Dummy', '08-121314', 11, 0, 4100, 5, 'testc', false, false, false, 'Damcykel', 'Damcykel med stång i mitten. 33,2 tum i plast.', 667, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'es');
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (8, 6431717, '2006-04-06 09:21:31', 'active', 'sell', 'Juan P�rez', '084123456', 11, 0, 5020, 1, '11111', false, false, false, 'Una mesa', 'Y qu� mesa... menuda mesa!!', 157000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'es');
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (9, 999999, NULL, 'inactive', 'sell', 'Dummy', '08-121314', 11, 0, 4100, 1, 'testc', false, false, false, 'Damcykel med blahonga', 'Damcykel med stång rakt upp i mitten. 33,2 tum i plast.', 667, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'es');
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (10, 6000666, '2006-04-05 10:21:31', 'active', 'sell', 'Cristobal Col�n', '0812131491', 12, 0, 6020, 5, '11111', false, false, false, 'Pesas muy grandes', 'Una de 120 kg y la otra de 57, mas ligera.', 90000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'es');
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (11, 6394117, '2006-04-07 11:21:31', 'active', 'sell', 'Andrea Gonz�lez', '0987654321', 13, 0, 7060, 1, '11111', false, false, false, 'Peluquera a domicilio', 'Voy, corto el pelo, me pagas y m voy....', 12000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'es');
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (12, 6394118, '2011-11-06 11:21:31', 'active', 'let', 'Boris Felipe', '0987654000', 15, 0, 1040, 1, '11111', false, false, false, 'Arriendo mi preciosa casa en �u�oa', 'Dos habitaciones, metros cuadrados.... hect�reas dir�a yo... y plazas de garaje a tutipl�n', 500000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'es');
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (20, NULL, NULL, 'inactive', 'sell', 'noupselling', '98765465', 15, 0, 6140, 50, NULL, false, false, false, 'Aviso sin upselling', 'descripcion', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$-*l(tGZ{S5RBE9#Afd2549d0f3f8d8b6d3be3fdf94f6c506111ba6d9', NULL, 'es');
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (21, NULL, NULL, 'inactive', 'sell', 'upsellinggallery', '98765465', 15, 0, 6020, 51, NULL, false, false, false, 'Aviso upselling gallery', 'desc', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$xd{E$,V.*~_&\\Q:0d3a91bac5a7d98504d3e4d0cc34435e1834a82a6', NULL, 'es');
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (23, NULL, NULL, 'inactive', 'sell', 'upsellingdaily', '98765465', 15, 0, 6180, 53, NULL, false, false, false, 'Aviso upselling daily', 'desc', 124444, NULL, NULL, NULL, '2015-07-01 11:36:49.271978', NULL, NULL, '$1024$_UVt{/`a~%:`;aB|0e91e5b90dfe73e7f11ff6bea675dce864f6b3e7', '2015-07-01 11:36:49.271978', 'es');
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (22, NULL, NULL, 'inactive', 'sell', 'upsellingweekly', '98765465', 15, 0, 6060, 52, NULL, false, false, false, 'Aviso upselling weekly', 'desc', 12000, NULL, NULL, NULL, '2015-07-01 11:36:49.284192', NULL, NULL, '$1024$}).$,''A,zaYcR+HZ28a3a066436c418bb36381e941656d7f5cbf233e', '2015-07-01 11:36:49.284192', 'es');
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (24, 8000000, '2015-07-01 11:36:42', 'active', 'sell', 'upselling on edit', '98765465', 15, 0, 6080, 54, NULL, false, false, false, 'Aviso upselling on edit', 'to edit', 100, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$"{Jn>$o^sh>9E;PNb502eae088998c39bd7cb890886e2ef99a0f79d0', '2015-07-01 11:36:42.542847', 'es');
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (25, 8000001, '2015-07-01 11:39:12', 'active', 'sell', 'upselling on edit', '98765465', 15, 0, 6100, 55, NULL, false, false, false, 'Aviso upselling on renew', 'to renew', 12, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$CmiR#8G~XDxY/n>G73dcfccef73610f2e78e19da138bb27da6b0caa5', '2015-07-01 11:39:12.935439', 'es');


--
-- Data for Name: payment_groups; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (0, '012345678', 'unverified', '2015-07-01 10:28:04.891614', NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (1, '123456789', 'unverified', '2015-07-01 10:28:04.893794', NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (2, '11223', 'unpaid', '2015-07-01 10:28:04.894484', NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (3, '200000001', 'unverified', '2015-07-01 10:28:04.895282', NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (50, '59876', 'cleared', '2015-07-01 10:28:04.895965', NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (60, '112200000', 'verified', '2015-07-01 11:19:08.422669', NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (61, '112200001', 'verified', '2015-07-01 11:20:34.874016', NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (62, '12345', 'paid', '2015-07-01 11:20:43.021459', NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (63, '112200002', 'verified', '2015-07-01 11:21:57.885356', NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (64, '65432', 'paid', '2015-07-01 11:22:05.56416', NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (65, '112200003', 'verified', '2015-07-01 11:23:03.871419', NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (66, '32323', 'paid', '2015-07-01 11:23:13.029125', NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (67, '112200004', 'verified', '2015-07-01 11:24:20.139513', NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (68, '112200005', 'verified', '2015-07-01 11:37:26.576402', NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (69, '54545', 'paid', '2015-07-01 11:37:31.815333', NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (70, '112200006', 'verified', '2015-07-01 11:38:51.925208', NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (71, '112200007', 'verified', '2015-07-01 11:41:29.292392', NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (72, '90001', 'paid', '2015-07-01 11:41:33.769528', NULL);


--
-- Data for Name: ad_actions; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (1, 1, 'new', 154, 'accepted', 'normal', NULL, NULL, 0);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (2, 1, 'new', 4, 'unverified', 'normal', NULL, NULL, 1);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (3, 1, 'new', 6, 'unpaid', 'normal', NULL, NULL, 2);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (4, 1, 'new', 8, 'unverified', 'normal', NULL, NULL, 3);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (8, 1, 'new', 9, 'accepted', 'normal', NULL, NULL, NULL);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (10, 1, 'new', 11, 'accepted', 'normal', NULL, NULL, NULL);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (11, 1, 'new', 12, 'accepted', 'normal', NULL, NULL, NULL);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (6, 1, 'new', 53, 'accepted', 'normal', NULL, NULL, 50);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (9, 1, 'new', 43, 'refused', 'normal', NULL, NULL, NULL);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (12, 1, 'new', 164, 'accepted', 'normal', NULL, NULL, 0);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (25, 2, 'half_time_mail', 244, 'reg', 'normal', NULL, NULL, NULL);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (20, 1, 'new', 202, 'pending_review', 'normal', NULL, NULL, 60);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (25, 3, 'renew', 247, 'pending_review', 'normal', NULL, NULL, 71);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (21, 1, 'new', 205, 'pending_review', 'normal', NULL, NULL, 61);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (21, 2, 'upselling_gallery', 208, 'accepted', 'normal', NULL, NULL, 62);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (25, 4, 'upselling_weekly_bump', 250, 'accepted', 'normal', NULL, NULL, 72);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (22, 1, 'new', 211, 'pending_review', 'normal', NULL, NULL, 63);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (23, 1, 'new', 217, 'pending_review', 'normal', NULL, NULL, 65);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (24, 1, 'new', 224, 'accepted', 'normal', NULL, NULL, 67);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (21, 3, 'remove_gallery', 226, 'accepted', 'normal', NULL, NULL, NULL);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (21, 4, 'gal_exp_mail', 227, 'reg', 'normal', NULL, NULL, NULL);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (23, 2, 'upselling_daily_bump', 228, 'accepted', 'normal', NULL, NULL, 66);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (23, 3, 'bump', 231, 'accepted', 'normal', NULL, NULL, NULL);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (22, 2, 'upselling_weekly_bump', 230, 'accepted', 'normal', NULL, NULL, 64);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (22, 3, 'bump', 233, 'accepted', 'normal', NULL, NULL, NULL);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (24, 2, 'edit', 236, 'pending_review', 'edit', NULL, NULL, 68);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (24, 3, 'upselling_daily_bump', 239, 'accepted', 'normal', NULL, NULL, 69);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (25, 1, 'new', 243, 'accepted', 'normal', NULL, NULL, 70);


--
-- Data for Name: action_params; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO action_params (ad_id, action_id, name, value) VALUES (20, 1, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (20, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (21, 1, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (21, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (22, 1, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (22, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (23, 1, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (23, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (24, 1, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (24, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (24, 2, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (24, 2, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (25, 1, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (25, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (25, 3, 'source', 'half_time_mail');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (25, 3, 'redir', 'dW5rbm93bg==');


--
-- Data for Name: action_states; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (1, 1, 150, 'reg', 'initial', '2015-07-01 10:28:04.906741', '192.168.4.75', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (1, 1, 151, 'unverified', 'verify', '2015-07-01 10:28:04.908815', '192.168.4.75', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (1, 1, 152, 'pending_review', 'verify', '2015-07-01 10:28:04.909528', '192.168.4.75', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (1, 1, 153, 'locked', 'checkout', '2015-07-01 10:28:04.910056', '192.168.4.75', 99999992);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (1, 1, 154, 'accepted', 'accept', '2015-07-01 10:28:04.910701', '192.168.4.75', 99999993);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (2, 1, 3, 'reg', 'initial', '2015-07-01 10:28:04.911308', '192.168.4.85', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (2, 1, 4, 'unverified', 'verifymail', '2015-07-01 10:28:04.911776', '192.168.4.85', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (3, 1, 5, 'reg', 'initial', '2015-07-01 10:28:04.912178', '192.168.4.85', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (3, 1, 6, 'unpaid', 'paymail', '2015-07-01 10:28:04.912546', '192.168.4.85', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (4, 1, 7, 'reg', 'initial', '2015-07-01 10:28:04.913016', '192.168.4.85', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (4, 1, 8, 'unverified', 'verifymail', '2015-07-01 10:28:04.913451', '192.168.4.85', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (6, 1, 50, 'reg', 'initial', '2015-07-01 10:28:04.913959', '192.168.4.75', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (6, 1, 51, 'unpaid', 'newad', '2015-07-01 10:28:04.91433', '192.168.4.75', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (6, 1, 52, 'pending_review', 'pay', '2015-07-01 10:28:04.914734', '192.168.4.75', 99999998);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (6, 1, 53, 'accepted', 'accept', '2015-07-01 10:28:04.915296', '192.168.4.75', 99999999);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (9, 1, 40, 'reg', 'initial', '2015-07-01 10:28:04.915891', '192.168.4.75', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (9, 1, 41, 'unpaid', 'newad', '2015-07-01 10:28:04.9163', '192.168.4.75', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (9, 1, 42, 'pending_review', 'pay', '2015-07-01 10:28:04.916662', '192.168.4.75', 99999918);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (9, 1, 43, 'refused', 'refuse', '2015-07-01 10:28:04.917182', '192.168.4.75', 99999919);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (12, 1, 160, 'reg', 'initial', '2015-07-01 10:28:04.917666', '192.168.4.75', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (12, 1, 161, 'unverified', 'verify', '2015-07-01 10:28:04.918069', '192.168.4.75', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (12, 1, 162, 'pending_review', 'verify', '2015-07-01 10:28:04.918428', '192.168.4.75', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (12, 1, 163, 'locked', 'checkout', '2015-07-01 10:28:04.918765', '192.168.4.75', 99999994);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (12, 1, 164, 'accepted', 'accept', '2015-07-01 10:28:04.9192', '192.168.4.75', 99999995);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (10, 1, 54, 'reg', 'initial', '2015-07-01 11:20:00', '192.168.4.75', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (10, 1, 55, 'unpaid', 'newad', '2015-07-01 10:28:04.920793', '192.168.4.75', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (10, 1, 56, 'pending_review', 'pay', '2015-07-01 10:28:04.921208', '192.168.4.75', 99999998);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (10, 1, 57, 'accepted', 'accept', '2015-07-01 10:28:04.921612', '192.168.4.75', 99999999);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (20, 1, 200, 'reg', 'initial', '2015-07-01 11:19:08.422669', '10.0.1.101', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (20, 1, 201, 'unverified', 'verifymail', '2015-07-01 11:19:08.422669', '10.0.1.101', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (20, 1, 202, 'pending_review', 'verify', '2015-07-01 11:19:08.512616', '10.0.1.101', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (21, 1, 203, 'reg', 'initial', '2015-07-01 11:20:34.874016', '10.0.1.101', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (21, 1, 204, 'unverified', 'verifymail', '2015-07-01 11:20:34.874016', '10.0.1.101', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (21, 1, 205, 'pending_review', 'verify', '2015-07-01 11:20:34.916586', '10.0.1.101', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (21, 2, 206, 'reg', 'initial', '2015-07-01 11:20:43.021459', '10.0.1.101', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (21, 2, 207, 'unpaid', 'pending_pay', '2015-07-01 11:20:43.021459', '10.0.1.101', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (21, 2, 208, 'accepted', 'pay', '2015-07-01 11:20:46.976137', NULL, NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (22, 1, 209, 'reg', 'initial', '2015-07-01 11:21:57.885356', '10.0.1.101', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (22, 1, 210, 'unverified', 'verifymail', '2015-07-01 11:21:57.885356', '10.0.1.101', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (22, 1, 211, 'pending_review', 'verify', '2015-07-01 11:21:57.955771', '10.0.1.101', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (22, 2, 212, 'reg', 'initial', '2015-07-01 11:22:05.56416', '10.0.1.101', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (22, 2, 213, 'unpaid', 'pending_pay', '2015-07-01 11:22:05.56416', '10.0.1.101', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (22, 2, 214, 'accepted', 'pay', '2015-07-01 11:22:07.665155', NULL, NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (23, 1, 215, 'reg', 'initial', '2015-07-01 11:23:03.871419', '10.0.1.101', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (23, 1, 216, 'unverified', 'verifymail', '2015-07-01 11:23:03.871419', '10.0.1.101', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (23, 1, 217, 'pending_review', 'verify', '2015-07-01 11:23:03.93621', '10.0.1.101', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (23, 2, 218, 'reg', 'initial', '2015-07-01 11:23:13.029125', '10.0.1.101', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (23, 2, 219, 'unpaid', 'pending_pay', '2015-07-01 11:23:13.029125', '10.0.1.101', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (23, 2, 220, 'accepted', 'pay', '2015-07-01 11:23:14.880356', NULL, NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (24, 1, 221, 'reg', 'initial', '2015-07-01 11:24:20.139513', '10.0.1.101', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (24, 1, 222, 'unverified', 'verifymail', '2015-07-01 11:24:20.139513', '10.0.1.101', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (24, 1, 223, 'pending_review', 'verify', '2015-07-01 11:24:20.180569', '10.0.1.101', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (24, 1, 224, 'accepted', 'accept', '2015-07-01 11:36:42.542847', '10.0.1.101', 61);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (21, 3, 225, 'reg', 'initial', '2015-07-01 11:36:48.209417', '127.0.0.1', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (21, 3, 226, 'accepted', 'accept', '2015-07-01 11:36:48.209417', '127.0.0.1', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (21, 4, 227, 'reg', 'initial', '2015-07-01 11:36:48.291462', '127.0.0.1', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (23, 2, 228, 'accepted', 'updating_counter', '2015-07-01 11:36:49.264648', NULL, NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (23, 3, 229, 'reg', 'initial', '2015-07-01 11:36:49.271978', NULL, NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (22, 2, 230, 'accepted', 'updating_counter', '2015-07-01 11:36:49.265631', NULL, NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (23, 3, 231, 'accepted', 'bump', '2015-07-01 11:36:49.271978', NULL, NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (22, 3, 232, 'reg', 'initial', '2015-07-01 11:36:49.284192', NULL, NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (22, 3, 233, 'accepted', 'bump', '2015-07-01 11:36:49.284192', NULL, NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (24, 2, 234, 'reg', 'initial', '2015-07-01 11:37:26.576402', '10.0.1.101', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (24, 2, 235, 'unverified', 'verifymail', '2015-07-01 11:37:26.576402', '10.0.1.101', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (24, 2, 236, 'pending_review', 'verify', '2015-07-01 11:37:26.624435', '10.0.1.101', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (24, 3, 237, 'reg', 'initial', '2015-07-01 11:37:31.815333', '10.0.1.101', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (24, 3, 238, 'unpaid', 'pending_pay', '2015-07-01 11:37:31.815333', '10.0.1.101', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (24, 3, 239, 'accepted', 'pay', '2015-07-01 11:37:33.797975', NULL, NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (25, 1, 240, 'reg', 'initial', '2015-07-01 11:38:51.925208', '10.0.1.101', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (25, 1, 241, 'unverified', 'verifymail', '2015-07-01 11:38:51.925208', '10.0.1.101', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (25, 1, 242, 'pending_review', 'verify', '2015-07-01 11:38:51.96162', '10.0.1.101', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (25, 1, 243, 'accepted', 'accept', '2015-07-01 11:39:12.935439', '10.0.1.101', 70);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (25, 2, 244, 'reg', 'initial', '2015-07-01 11:41:04.809876', '127.0.0.1', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (25, 3, 245, 'reg', 'initial', '2015-07-01 11:41:29.292392', '10.0.1.101', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (25, 3, 246, 'unverified', 'verifymail', '2015-07-01 11:41:29.292392', '10.0.1.101', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (25, 3, 247, 'pending_review', 'verify', '2015-07-01 11:41:29.376804', '10.0.1.101', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (25, 4, 248, 'reg', 'initial', '2015-07-01 11:41:33.769528', '10.0.1.101', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (25, 4, 249, 'unpaid', 'pending_pay', '2015-07-01 11:41:33.769528', '10.0.1.101', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (25, 4, 250, 'accepted', 'pay', '2015-07-01 11:41:35.716471', NULL, NULL);


--
-- Name: action_states_state_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('action_states_state_id_seq', 250, true);


--
-- Data for Name: ad_changes; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO ad_changes (ad_id, action_id, state_id, is_param, column_name, old_value, new_value) VALUES (21, 2, 208, true, 'gallery', NULL, '2015-07-01 11:30:47');
INSERT INTO ad_changes (ad_id, action_id, state_id, is_param, column_name, old_value, new_value) VALUES (21, 3, 226, true, 'gallery', '2015-07-01 11:30:47', NULL);
INSERT INTO ad_changes (ad_id, action_id, state_id, is_param, column_name, old_value, new_value) VALUES (23, 2, 228, true, 'upselling_daily_bump', NULL, '3');
INSERT INTO ad_changes (ad_id, action_id, state_id, is_param, column_name, old_value, new_value) VALUES (22, 2, 230, true, 'upselling_weekly_bump', NULL, '3');
INSERT INTO ad_changes (ad_id, action_id, state_id, is_param, column_name, old_value, new_value) VALUES (24, 2, 234, false, 'body', 'to edit', 'edited');
INSERT INTO ad_changes (ad_id, action_id, state_id, is_param, column_name, old_value, new_value) VALUES (25, 3, 245, false, 'body', 'to renew', 'renewed');


--
-- Data for Name: ad_codes; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO ad_codes (code, code_type) VALUES (112200008, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (112200009, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (90002, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (90003, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (90004, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (90005, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (90006, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (90007, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (90008, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (90009, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (90010, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (90011, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (90012, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (90013, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (90014, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (90015, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (90016, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (90017, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (90018, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (90019, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (90020, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (90021, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (90022, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (90023, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (90024, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (90025, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (90026, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (90027, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (90028, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (90029, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (90030, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (90031, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (90032, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (90033, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (90034, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (90035, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (90036, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (90037, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (90038, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (90039, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (90040, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (42023, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (74046, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (16069, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (48092, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (80115, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (22138, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (54161, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (86184, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (28207, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (60230, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (92253, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (34276, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (66299, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (98322, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (40345, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (72368, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (14391, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (46414, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (78437, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (20460, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (52483, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (84506, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (26529, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (58552, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (90575, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (32598, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (64621, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (96644, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (38667, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (70690, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (12713, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (44736, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (76759, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (18782, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (50805, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (82828, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (24851, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (56874, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (88897, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (30920, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (62943, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (94966, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (36989, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (69012, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (11035, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (43058, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (75081, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (17104, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (49127, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (81150, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (23173, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (55196, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (87219, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (29242, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (61265, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (93288, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (235392023, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (370784046, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (506176069, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (641568092, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (776960115, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (912352138, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (147744161, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (283136184, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (418528207, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (553920230, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (689312253, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (824704276, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (960096299, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (195488322, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (330880345, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (466272368, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (601664391, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (737056414, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (872448437, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (107840460, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (243232483, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (378624506, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (514016529, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (649408552, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (784800575, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (920192598, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (155584621, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (290976644, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (426368667, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (561760690, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (697152713, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (832544736, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (967936759, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (203328782, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (338720805, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (474112828, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (609504851, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (744896874, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (880288897, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (115680920, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (251072943, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (386464966, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (521856989, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (657249012, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (792641035, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (928033058, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (163425081, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (298817104, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (434209127, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (569601150, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (704993173, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (840385196, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (975777219, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (211169242, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (346561265, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (481953288, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (617345311, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (752737334, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (888129357, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (123521380, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (258913403, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (394305426, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (529697449, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (665089472, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (800481495, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (935873518, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (171265541, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (306657564, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (442049587, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (577441610, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (712833633, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (848225656, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (983617679, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (219009702, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (354401725, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (489793748, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (625185771, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (760577794, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (895969817, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (131361840, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (266753863, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (402145886, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (537537909, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (672929932, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (808321955, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (943713978, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (179106001, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (314498024, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (449890047, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (585282070, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (35311, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (67334, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (99357, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (41380, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (73403, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (15426, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (47449, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (79472, 'pay');


--
-- Data for Name: ad_image_changes; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO ad_image_changes (ad_id, action_id, state_id, seq_no, name, is_new) VALUES (24, 1, 224, 0, NULL, false);
INSERT INTO ad_image_changes (ad_id, action_id, state_id, seq_no, name, is_new) VALUES (24, 2, 234, 0, NULL, false);
INSERT INTO ad_image_changes (ad_id, action_id, state_id, seq_no, name, is_new) VALUES (25, 1, 243, 0, NULL, false);
INSERT INTO ad_image_changes (ad_id, action_id, state_id, seq_no, name, is_new) VALUES (25, 3, 245, 0, NULL, false);


--
-- Data for Name: ad_images; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Data for Name: ad_images_digests; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO ad_images_digests (ad_id, name, digest, uid, previously_inserted) VALUES (21, '1708523319.jpg', '9cfb0c7201db3f4bf2fe9c5e6f377a15', 50, false);


--
-- Data for Name: ad_media; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO ad_media (ad_media_id, ad_id, seq_no, upload_time, media_type, width, height, length) VALUES (100012344, 6, 0, '2015-07-01 10:28:04.922085', 'image', NULL, NULL, NULL);
INSERT INTO ad_media (ad_media_id, ad_id, seq_no, upload_time, media_type, width, height, length) VALUES (100012345, 6, 1, '2015-07-01 10:28:04.923058', 'image', NULL, NULL, NULL);
INSERT INTO ad_media (ad_media_id, ad_id, seq_no, upload_time, media_type, width, height, length) VALUES (100012346, 6, 2, '2015-07-01 10:28:04.923568', 'image', NULL, NULL, NULL);
INSERT INTO ad_media (ad_media_id, ad_id, seq_no, upload_time, media_type, width, height, length) VALUES (1708523319, 21, 0, '2015-07-01 11:19:57.661891', 'image', NULL, NULL, NULL);


--
-- Data for Name: ad_media_changes; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Data for Name: ad_params; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO ad_params (ad_id, name, value) VALUES (1, 'rooms', '2');
INSERT INTO ad_params (ad_id, name, value) VALUES (1, 'size', '250');
INSERT INTO ad_params (ad_id, name, value) VALUES (1, 'garage_spaces', '2');
INSERT INTO ad_params (ad_id, name, value) VALUES (1, 'condominio', '400');
INSERT INTO ad_params (ad_id, name, value) VALUES (1, 'communes', '5');
INSERT INTO ad_params (ad_id, name, value) VALUES (1, 'currency', 'peso');
INSERT INTO ad_params (ad_id, name, value) VALUES (1, 'country', 'UNK');
INSERT INTO ad_params (ad_id, name, value) VALUES (6, 'regdate', '1986');
INSERT INTO ad_params (ad_id, name, value) VALUES (6, 'mileage', '1');
INSERT INTO ad_params (ad_id, name, value) VALUES (6, 'fuel', '2');
INSERT INTO ad_params (ad_id, name, value) VALUES (6, 'gearbox', '2');
INSERT INTO ad_params (ad_id, name, value) VALUES (11, 'service_type', '6');
INSERT INTO ad_params (ad_id, name, value) VALUES (12, 'rooms', '3');
INSERT INTO ad_params (ad_id, name, value) VALUES (12, 'size', '120');
INSERT INTO ad_params (ad_id, name, value) VALUES (12, 'garage_spaces', '1');
INSERT INTO ad_params (ad_id, name, value) VALUES (12, 'condominio', '400');
INSERT INTO ad_params (ad_id, name, value) VALUES (12, 'communes', '323');
INSERT INTO ad_params (ad_id, name, value) VALUES (12, 'currency', 'peso');
INSERT INTO ad_params (ad_id, name, value) VALUES (12, 'country', 'UNK');
INSERT INTO ad_params (ad_id, name, value) VALUES (20, 'prev_currency', 'peso');
INSERT INTO ad_params (ad_id, name, value) VALUES (20, 'country', 'UNK');
INSERT INTO ad_params (ad_id, name, value) VALUES (21, 'prev_currency', 'peso');
INSERT INTO ad_params (ad_id, name, value) VALUES (21, 'country', 'UNK');
INSERT INTO ad_params (ad_id, name, value) VALUES (22, 'prev_currency', 'peso');
INSERT INTO ad_params (ad_id, name, value) VALUES (22, 'country', 'UNK');
INSERT INTO ad_params (ad_id, name, value) VALUES (23, 'prev_currency', 'peso');
INSERT INTO ad_params (ad_id, name, value) VALUES (23, 'country', 'UNK');
INSERT INTO ad_params (ad_id, name, value) VALUES (24, 'prev_currency', 'peso');
INSERT INTO ad_params (ad_id, name, value) VALUES (24, 'country', 'UNK');
INSERT INTO ad_params (ad_id, name, value) VALUES (23, 'upselling_daily_bump', '3');
INSERT INTO ad_params (ad_id, name, value) VALUES (22, 'upselling_weekly_bump', '3');
INSERT INTO ad_params (ad_id, name, value) VALUES (25, 'prev_currency', 'peso');
INSERT INTO ad_params (ad_id, name, value) VALUES (25, 'country', 'UNK');


--
-- Data for Name: ad_queues; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO ad_queues (ad_id, action_id, queue, queued_at, locked_by, locked_until) VALUES (20, 1, 'normal', '2015-07-01 11:19:08.512616', NULL, NULL);
INSERT INTO ad_queues (ad_id, action_id, queue, queued_at, locked_by, locked_until) VALUES (21, 1, 'normal', '2015-07-01 11:20:34.916586', NULL, NULL);
INSERT INTO ad_queues (ad_id, action_id, queue, queued_at, locked_by, locked_until) VALUES (22, 1, 'normal', '2015-07-01 11:21:57.955771', NULL, NULL);
INSERT INTO ad_queues (ad_id, action_id, queue, queued_at, locked_by, locked_until) VALUES (23, 1, 'normal', '2015-07-01 11:23:03.93621', NULL, NULL);
INSERT INTO ad_queues (ad_id, action_id, queue, queued_at, locked_by, locked_until) VALUES (24, 2, 'edit', '2015-07-01 11:37:26.624435', NULL, NULL);
INSERT INTO ad_queues (ad_id, action_id, queue, queued_at, locked_by, locked_until) VALUES (25, 3, 'normal', '2015-07-01 11:41:29.376804', NULL, NULL);


--
-- Data for Name: admin_privs; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO admin_privs (admin_id, priv_name) VALUES (2, 'adqueue');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (3, 'adqueue');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (3, 'admin');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (3, 'adminad.edit_ad');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (3, 'adminad.clear_ad');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (3, 'adminad.bump');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (3, 'credit');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (3, 'config');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (3, 'adqueue.admin_queue');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (3, 'adqueue.admin_settings');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (3, 'adqueue.show_num_ads=2');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (3, 'search');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (3, 'search.search_ads');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (3, 'notice_abuse');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (4, 'admin');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (5, 'admin');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (5, 'adqueue');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (5, 'adminad');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (5, 'adminad.clear_ad');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (5, 'adminad.edit_ad');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (5, 'adminad.bump');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (5, 'credit');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (5, 'config');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (5, 'adqueue.admin_queue');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (5, 'adqueue.admin_settings');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (5, 'adqueue.show_num_ads=2');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (6, 'admin');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (6, 'adqueue');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (6, 'adminad.edit_ad');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (50, 'admin');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (50, 'adqueue');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (51, 'admin');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (51, 'adqueue');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (52, 'admin');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (52, 'adqueue');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (53, 'admin');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (53, 'adqueue');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (54, 'admin');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (54, 'adqueue');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (55, 'admin');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (55, 'adqueue');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'ais');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'notice_abuse');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'adqueue');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'adminad');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'Adminuf');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'admin');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'api');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'config');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'filter');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'landing_page');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'mama');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'popular_ads');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'scarface');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'search');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'Websql');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'adqueue.accepted');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'adqueue.admin_settings');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'adqueue.show_num_ads=2');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'adqueue.approve_refused');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'adqueue.video');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'adqueue.mama_queues');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'adqueue.mamawork_queues');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'adqueue.refusals');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'adqueue.admin_queue');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'adminad.bump');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'adminad.clear_ad');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'adminad.edit_ad');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'filter.lists');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'filter.rules');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'filter.spamfilter');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'mama.backup_lists');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'mama.show_lists');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'scarface.warning');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'search.abuse_report');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'search.uid_emails');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'search.maillog');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'search.mass_delete');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'search.paylog');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'search.reviewers');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'search.search_ads');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'search.undo_delete');
INSERT INTO admin_privs VALUES (23, 'ais');
INSERT INTO admin_privs VALUES (23, 'notice_abuse');
INSERT INTO admin_privs VALUES (23, 'adqueue');
INSERT INTO admin_privs VALUES (23, 'adminad');
INSERT INTO admin_privs VALUES (23, 'Adminuf');
INSERT INTO admin_privs VALUES (23, 'admin');
INSERT INTO admin_privs VALUES (23, 'api');
INSERT INTO admin_privs VALUES (23, 'config');
INSERT INTO admin_privs VALUES (23, 'filter');
INSERT INTO admin_privs VALUES (23, 'landing_page');
INSERT INTO admin_privs VALUES (23, 'mama');
INSERT INTO admin_privs VALUES (23, 'popular_ads');
INSERT INTO admin_privs VALUES (23, 'scarface');
INSERT INTO admin_privs VALUES (23, 'search');
INSERT INTO admin_privs VALUES (23, 'Websql');
INSERT INTO admin_privs VALUES (23, 'adqueue.accepted');
INSERT INTO admin_privs VALUES (23, 'adqueue.admin_settings');
INSERT INTO admin_privs VALUES (23, 'adqueue.show_num_ads=10');
INSERT INTO admin_privs VALUES (23, 'adqueue.approve_refused');
INSERT INTO admin_privs VALUES (23, 'adqueue.video');
INSERT INTO admin_privs VALUES (23, 'adqueue.mama_queues');
INSERT INTO admin_privs VALUES (23, 'adqueue.mamawork_queues');
INSERT INTO admin_privs VALUES (23, 'adqueue.refusals');
INSERT INTO admin_privs VALUES (23, 'adqueue.admin_queue');
INSERT INTO admin_privs VALUES (23, 'adqueue.whitelist');
INSERT INTO admin_privs VALUES (23, 'adminad.bump');
INSERT INTO admin_privs VALUES (23, 'adminad.clear_ad');
INSERT INTO admin_privs VALUES (23, 'adminad.edit_ad');
INSERT INTO admin_privs VALUES (23, 'filter.lists');
INSERT INTO admin_privs VALUES (23, 'filter.rules');
INSERT INTO admin_privs VALUES (23, 'filter.spamfilter');
INSERT INTO admin_privs VALUES (23, 'mama.backup_lists');
INSERT INTO admin_privs VALUES (23, 'mama.show_lists');
INSERT INTO admin_privs VALUES (23, 'scarface.warning');
INSERT INTO admin_privs VALUES (23, 'search.abuse_report');
INSERT INTO admin_privs VALUES (23, 'search.uid_emails');
INSERT INTO admin_privs VALUES (23, 'search.maillog');
INSERT INTO admin_privs VALUES (23, 'search.mass_delete');
INSERT INTO admin_privs VALUES (23, 'search.paylog');
INSERT INTO admin_privs VALUES (23, 'search.reviewers');
INSERT INTO admin_privs VALUES (23, 'search.search_ads');
INSERT INTO admin_privs VALUES (23, 'search.undo_delete');


--
-- Name: admins_admin_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('admins_admin_id_seq', 22, true);


--
-- Name: ads_ad_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('ads_ad_id_seq', 25, true);


--
-- Name: adwatch_code_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('adwatch_code_seq', 1, false);


--
-- Data for Name: archive_cache; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Data for Name: bid_ads; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Name: bid_ads_bid_ad_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('bid_ads_bid_ad_id_seq', 1, false);


--
-- Data for Name: bid_bids; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Name: bid_bids_bid_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('bid_bids_bid_id_seq', 1, false);


--
-- Data for Name: bid_media; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Data for Name: block_lists; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO block_lists (list_id, list_name, base_list, list_type, ignore) VALUES (1, 'E-post adresser - raderas', true, 'email', NULL);
INSERT INTO block_lists (list_id, list_name, base_list, list_type, ignore) VALUES (2, 'E-post adresser - spamfiltret', true, 'email', NULL);
INSERT INTO block_lists (list_id, list_name, base_list, list_type, ignore) VALUES (3, 'IP-adresser som - raderas', true, 'ip', NULL);
INSERT INTO block_lists (list_id, list_name, base_list, list_type, ignore) VALUES (4, 'IP-adresser - spamfiltret', true, 'ip', NULL);
INSERT INTO block_lists (list_id, list_name, base_list, list_type, ignore) VALUES (5, 'E-postadresser som inte f�r annonsera', false, 'email', NULL);
INSERT INTO block_lists (list_id, list_name, base_list, list_type, ignore) VALUES (6, 'Telefonnummer som inte f�r annonsera', false, 'general', '-+ ');
INSERT INTO block_lists (list_id, list_name, base_list, list_type, ignore) VALUES (7, 'Tipsmottagare', false, 'email', NULL);
INSERT INTO block_lists (list_id, list_name, base_list, list_type, ignore) VALUES (8, 'Synonymer f�r ordet Annonsera', false, 'general', NULL);
INSERT INTO block_lists (list_id, list_name, base_list, list_type, ignore) VALUES (9, 'Synonymer f�r ordet Gratis', false, 'general', NULL);
INSERT INTO block_lists (list_id, list_name, base_list, list_type, ignore) VALUES (10, 'Sajter som kan spamma', false, 'general', NULL);
INSERT INTO block_lists (list_id, list_name, base_list, list_type, ignore) VALUES (11, 'Ord som anv�nds av sajter som spammar', false, 'general', NULL);
INSERT INTO block_lists (list_id, list_name, base_list, list_type, ignore) VALUES (12, 'Fula ord', false, 'general', NULL);
INSERT INTO block_lists (list_id, list_name, base_list, list_type, ignore) VALUES (13, 'F�rbjudna rubriker i L�gg in annons', false, 'general', NULL);
INSERT INTO block_lists (list_id, list_name, base_list, list_type, ignore) VALUES (14, 'Engelska fraser som anv�nds i mejlsvar', false, 'general', NULL);
INSERT INTO block_lists (list_id, list_name, base_list, list_type, ignore) VALUES (15, 'Byten.se', false, 'general', NULL);
INSERT INTO block_lists (list_id, list_name, base_list, list_type, ignore) VALUES (16, 'whitelist', false, 'email', NULL);
INSERT INTO block_lists (list_id, list_name, base_list, list_type, ignore) VALUES (17, 'were_whitelist', false, 'email', NULL);


--
-- Name: block_lists_list_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('block_lists_list_id_seq', 100, false);


--
-- Data for Name: block_rules; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO block_rules (rule_id, rule_name, action, application, combine_rule, count_yesterday, count_today, count_date, target) VALUES (1, 'Sajtspam', 'delete', 'adreply', 'and', 0, 0, '2015-07-01', NULL);
INSERT INTO block_rules (rule_id, rule_name, action, application, combine_rule, count_yesterday, count_today, count_date, target) VALUES (2, 'Annonsera gratis', 'delete', 'adreply', 'and', 0, 0, '2015-07-01', NULL);
INSERT INTO block_rules (rule_id, rule_name, action, application, combine_rule, count_yesterday, count_today, count_date, target) VALUES (3, 'Fula ord', 'stop', 'newad', 'and', 0, 0, '2015-07-01', NULL);
INSERT INTO block_rules (rule_id, rule_name, action, application, combine_rule, count_yesterday, count_today, count_date, target) VALUES (4, 'Fula ord', 'delete', 'adreply', 'and', 0, 0, '2015-07-01', NULL);
INSERT INTO block_rules (rule_id, rule_name, action, application, combine_rule, count_yesterday, count_today, count_date, target) VALUES (5, 'Blockerade epostadresser  - raderas', 'delete', 'adreply', 'and', 0, 0, '2015-07-01', NULL);
INSERT INTO block_rules (rule_id, rule_name, action, application, combine_rule, count_yesterday, count_today, count_date, target) VALUES (6, 'Blockerade epostadresser', 'stop', 'newad', 'and', 0, 0, '2015-07-01', NULL);
INSERT INTO block_rules (rule_id, rule_name, action, application, combine_rule, count_yesterday, count_today, count_date, target) VALUES (7, 'Blockerade ip-adresser - raderas', 'delete', 'adreply', 'and', 0, 0, '2015-07-01', NULL);
INSERT INTO block_rules (rule_id, rule_name, action, application, combine_rule, count_yesterday, count_today, count_date, target) VALUES (8, 'Blockerade telefonnummer', 'stop', 'newad', 'and', 0, 0, '2015-07-01', NULL);
INSERT INTO block_rules (rule_id, rule_name, action, application, combine_rule, count_yesterday, count_today, count_date, target) VALUES (9, 'Blockerade tipsmottagare', 'delete', 'sendtip', 'and', 0, 0, '2015-07-01', NULL);
INSERT INTO block_rules (rule_id, rule_name, action, application, combine_rule, count_yesterday, count_today, count_date, target) VALUES (10, 'Byten.se', 'delete', 'adreply', 'and', 0, 0, '2015-07-01', NULL);
INSERT INTO block_rules (rule_id, rule_name, action, application, combine_rule, count_yesterday, count_today, count_date, target) VALUES (11, 'Byten.se', 'delete', 'sendtip', 'and', 0, 0, '2015-07-01', NULL);
INSERT INTO block_rules (rule_id, rule_name, action, application, combine_rule, count_yesterday, count_today, count_date, target) VALUES (12, 'Blockerade ord i rubriken', 'stop', 'newad', 'and', 0, 0, '2015-07-01', NULL);
INSERT INTO block_rules (rule_id, rule_name, action, application, combine_rule, count_yesterday, count_today, count_date, target) VALUES (13, 'Blockerade engelska mail', 'spamfilter', 'adreply', 'and', 0, 0, '2015-07-01', NULL);
INSERT INTO block_rules (rule_id, rule_name, action, application, combine_rule, count_yesterday, count_today, count_date, target) VALUES (14, 'Blockerade epostadresser - spamfilter', 'spamfilter', 'adreply', 'and', 0, 0, '2015-07-01', NULL);
INSERT INTO block_rules (rule_id, rule_name, action, application, combine_rule, count_yesterday, count_today, count_date, target) VALUES (15, 'Blockerade ip-adresser - spamfilter', 'spamfilter', 'adreply', 'and', 0, 0, '2015-07-01', NULL);
INSERT INTO block_rules (rule_id, rule_name, action, application, combine_rule, count_yesterday, count_today, count_date, target) VALUES (16, 'Fula ord', 'delete', 'sendtip', 'or', 0, 0, '2015-07-01', NULL);
INSERT INTO block_rules (rule_id, rule_name, action, application, combine_rule, count_yesterday, count_today, count_date, target) VALUES (17, 'whitelist rule', 'move_to_queue', 'clear', 'and', 0, 0, '2015-07-01', 'whitelist');


--
-- Data for Name: block_rule_conditions; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO block_rule_conditions (condition_id, rule_id, list_id, whole_word, fields) VALUES (1, 1, 10, 0, '{subject,body}');
INSERT INTO block_rule_conditions (condition_id, rule_id, list_id, whole_word, fields) VALUES (2, 1, 11, 0, '{subject,body}');
INSERT INTO block_rule_conditions (condition_id, rule_id, list_id, whole_word, fields) VALUES (3, 2, 8, 0, '{subject,body}');
INSERT INTO block_rule_conditions (condition_id, rule_id, list_id, whole_word, fields) VALUES (4, 2, 9, 0, '{subject,body}');
INSERT INTO block_rule_conditions (condition_id, rule_id, list_id, whole_word, fields) VALUES (5, 3, 12, 0, '{subject,body}');
INSERT INTO block_rule_conditions (condition_id, rule_id, list_id, whole_word, fields) VALUES (6, 4, 12, 0, '{subject,body}');
INSERT INTO block_rule_conditions (condition_id, rule_id, list_id, whole_word, fields) VALUES (7, 5, 1, 1, '{email}');
INSERT INTO block_rule_conditions (condition_id, rule_id, list_id, whole_word, fields) VALUES (8, 6, 5, 1, '{email}');
INSERT INTO block_rule_conditions (condition_id, rule_id, list_id, whole_word, fields) VALUES (9, 7, 3, 1, '{remote_addr}');
INSERT INTO block_rule_conditions (condition_id, rule_id, list_id, whole_word, fields) VALUES (10, 8, 6, 1, '{phone}');
INSERT INTO block_rule_conditions (condition_id, rule_id, list_id, whole_word, fields) VALUES (11, 9, 7, 1, '{email}');
INSERT INTO block_rule_conditions (condition_id, rule_id, list_id, whole_word, fields) VALUES (12, 10, 15, 0, '{name,email,subject,body}');
INSERT INTO block_rule_conditions (condition_id, rule_id, list_id, whole_word, fields) VALUES (13, 11, 15, 0, '{name,email,subject,body}');
INSERT INTO block_rule_conditions (condition_id, rule_id, list_id, whole_word, fields) VALUES (14, 12, 13, 0, '{subject}');
INSERT INTO block_rule_conditions (condition_id, rule_id, list_id, whole_word, fields) VALUES (15, 13, 14, 0, '{body}');
INSERT INTO block_rule_conditions (condition_id, rule_id, list_id, whole_word, fields) VALUES (16, 14, 2, 1, '{subject}');
INSERT INTO block_rule_conditions (condition_id, rule_id, list_id, whole_word, fields) VALUES (17, 15, 4, 1, '{remote_addr}');
INSERT INTO block_rule_conditions (condition_id, rule_id, list_id, whole_word, fields) VALUES (18, 16, 12, 0, '{body}');
INSERT INTO block_rule_conditions (condition_id, rule_id, list_id, whole_word, fields) VALUES (19, 17, 16, 1, '{email}');


--
-- Name: block_rule_conditions_condition_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('block_rule_conditions_condition_id_seq', 18, true);


--
-- Name: block_rules_rule_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('block_rules_rule_id_seq', 100, false);


--
-- Data for Name: blocked_items; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO blocked_items (item_id, value, list_id, notice, token_id) VALUES (1, 'ful@fisk.se', 1, NULL, NULL);
INSERT INTO blocked_items (item_id, value, list_id, notice, token_id) VALUES (2, 'ful@fisk.se', 5, NULL, NULL);
INSERT INTO blocked_items (item_id, value, list_id, notice, token_id) VALUES (3, '0733555501', 6, NULL, NULL);
INSERT INTO blocked_items (item_id, value, list_id, notice, token_id) VALUES (4, 'ful@fisk.se', 7, NULL, NULL);
INSERT INTO blocked_items (item_id, value, list_id, notice, token_id) VALUES (5, 'fisk.se', 10, NULL, NULL);
INSERT INTO blocked_items (item_id, value, list_id, notice, token_id) VALUES (6, 'vatten', 11, NULL, NULL);
INSERT INTO blocked_items (item_id, value, list_id, notice, token_id) VALUES (7, 'fitta', 12, NULL, NULL);
INSERT INTO blocked_items (item_id, value, list_id, notice, token_id) VALUES (8, 'analakrobat', 12, NULL, NULL);
INSERT INTO blocked_items (item_id, value, list_id, notice, token_id) VALUES (9, 'buy', 13, NULL, NULL);
INSERT INTO blocked_items (item_id, value, list_id, notice, token_id) VALUES (10, 'sale', 13, NULL, NULL);
INSERT INTO blocked_items (item_id, value, list_id, notice, token_id) VALUES (11, 'salu', 13, NULL, NULL);
INSERT INTO blocked_items (item_id, value, list_id, notice, token_id) VALUES (12, 'free', 14, NULL, NULL);


--
-- Name: blocked_items_item_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('blocked_items_item_id_seq', 12, true);


--
-- Data for Name: conf; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.controlpanel.modules.adqueue.settings.auto_abuse', '1', '2015-07-01 10:28:04.418375', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.accepted.1.name', 'Sin fotos', '2015-07-01 10:28:04.422578', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.accepted.1.text', 'Para mejorar las posibilidades de que tu aviso sea visto, te recomendamos que agregues im�genes del producto que ofreces o buscas.', '2015-07-01 10:28:04.42313', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.accepted.22.name', 'Foto principal movida', '2015-07-01 10:28:04.423568', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.accepted.22.text', 'Para mejorar las posibilidades de que tu aviso sea visto, hemos alterado el orden de las fotograf�as', '2015-07-01 10:28:04.424033', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.accepted.3.name', 'BORRAMOS TODAS LAS IM�GENES', '2015-07-01 10:28:04.424555', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.accepted.3.text', 'Hemos borrado las im�genes que subiste por no cumplir las reglas de Yapo.cl. Sube nuevas fotograf�as para tener m�s visitas y vender�s antes!', '2015-07-01 10:28:04.424976', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.accepted.4.name', 'BORRAMOS ALGUNAS DE LAS IM�GENES', '2015-07-01 10:28:04.425517', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.accepted.4.text', 'Hemos borrado algunas de las im�genes que subiste por no cumplir las reglas de Yapo.cl. Sube nuevas fotograf�as para tener m�s visitas y vender�s antes!', '2015-07-01 10:28:04.426016', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.accepted.6.name', 'BORRAMOS CORREOS', '2015-07-01 10:28:04.426505', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.accepted.6.text', 'Hemos borrado el email de la descripci�n de tu aviso por tu seguridad. Las personas interesadas pueden contactarte a trav�s del formulario y evitar�s recibir spam o emails no deseados. Recuerda que Yapo.cl es 100% gratis.', '2015-07-01 10:28:04.426904', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.accepted.8.name', 'MODIFICAMOS T�TULO', '2015-07-01 10:28:04.427378', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.accepted.8.text', 'Hemos modificado el t�tulo de tu aviso para que describa mejor lo que ofreces. Te sugerimos editar tu aviso para hacerlo a�n mejor y vender antes.', '2015-07-01 10:28:04.427769', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.accepted.9.name', 'MODIFICAMOS DESCRIPCI�N', '2015-07-01 10:28:04.428223', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.accepted.9.text', 'Hemos modificado la descripci�n de tu aviso para explicar mejor lo que ofreces. Te sugerimos editar tu aviso para hacerlo a�n mejor y vender antes.', '2015-07-01 10:28:04.42868', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.accepted.10.name', 'PALABRAS DE B�SQUEDA', '2015-07-01 10:28:04.429048', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.accepted.10.text', 'Hemos modificado la descripci�n de tu aviso para ajustarla a las reglas de Yapo.cl. Recuerda que tu aviso debe hacer referencia a un solo producto o servicio.', '2015-07-01 10:28:04.429521', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.accepted.15.name', 'MEJORA LA DESCRIPCI�N', '2015-07-01 10:28:04.429923', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.accepted.15.text', 'Hemos revisado la descripci�n de tu aviso y te recomendamos que detalles m�s el producto o servicio que est�s ofreciendo, tu aviso ser� m�s interesante y vender�s antes', '2015-07-01 10:28:04.4304', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.accepted.16.name', 'CATEGORIA FUE CAMBIADA', '2015-07-01 10:28:04.430783', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.accepted.16.text', 'Hemos cambiado la categor�a que elegiste por la correcta. Ahora tu aviso ser� encontrado m�s f�cil y vender�s antes. Recuerda que Yapo.cl es 100% gratis.', '2015-07-01 10:28:04.431151', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.accepted.12.name', 'COMPLETAMOS EL CAMPO PRECIO', '2015-07-01 10:28:04.431561', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.accepted.12.text', 'Hemos completado el precio de tu producto o servicio. Puedes editarlo de nuevo si crees que nos hemos equivocado.', '2015-07-01 10:28:04.43193', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.accepted.13.name', 'BORRAMOS ENLACES', '2015-07-01 10:28:04.432377', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.accepted.13.text', 'Hemos borrado enlaces de la descripci�n de tu aviso ya que no est� permitido en las reglas de nuestra web. Recuerda que no hay l�mite de datos para ingresar y que Yapo.cl es 100% gratis.', '2015-07-01 10:28:04.432802', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.accepted.17.name', 'TIPO DE VENDEDOR DE PERSONA A EMPRESA', '2015-07-01 10:28:04.433183', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.accepted.17.text', 'Hemos cambiado el tipo de vendedor de persona a empresa porque consideramos que te dedicas profesionalmente al producto/servicio que ofreces. Recuerda que Yapo.cl es 100% gratis tambi�n para empresas.', '2015-07-01 10:28:04.433632', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.accepted.18.name', 'TIPO DE VENDEDOR DE EMPRESA A PERSONA', '2015-07-01 10:28:04.434108', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.accepted.18.text', 'Hemos cambiado el tipo de vendedor de empresa a persona porque consideramos que fue mal seleccionado. Recuerda que Yapo.cl es 100% gratis.', '2015-07-01 10:28:04.434693', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.accepted.19.name', 'MEJORA EL T�TULO', '2015-07-01 10:28:04.435112', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.accepted.19.text', 'Hemos revisado el t�tulo de tu aviso y te recomendamos que detalles m�s el producto o servicio que est�s ofreciendo, tu aviso ser� m�s interesante y vender�s antes', '2015-07-01 10:28:04.435568', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.accepted.20.name', 'INGRESA COMUNA (De NO UF a UF)', '2015-07-01 10:28:04.435983', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.accepted.20.text', 'Hemos cambiado la categor�a que elegiste por una de inmobiliaria. Para completar la publicaci�n debes editarlo e ingresar la comuna, de esta forma tu aviso ser� encontrado m�s f�cil y vender�s antes. Recuerda que Yapo.cl es 100% gratis.', '2015-07-01 10:28:04.436383', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.accepted.21.name', 'VEHICULOS: AGREGA MODELO Y VERSION', '2015-07-01 10:28:04.436802', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.accepted.21.text', 'Hemos revisado tu aviso y te recomendamos que detalles la marca y modelo del veh�culo que est�s ofreciendo, tu aviso ser� encontrado m�s f�cil y vender�s antes. Recuerda que Yapo.cl es 100% gratis.', '2015-07-01 10:28:04.437184', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.accepted.5.name', 'BORRAMOS LINKS Y CORREOS', '2015-07-01 10:28:04.437636', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.accepted.5.text', 'Por tu seguridad, hemos borrado emails y enlaces de la descripci�n de tu aviso. Las personas interesadas pueden contactarte a trav�s del formulario "Contacta con el anunciante" el cual se encuentra a la derecha de cada uno de los avisos. As� evitar�s recibir spam o emails no deseados. Adem�s, puedes incluir tantos detalles como quieras en tu aviso. Recuerda que Yapo.cl es 100% gratis.', '2015-07-01 10:28:04.438457', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.accepted.11.name', 'BORRAMOS CAMPO PRECIO', '2015-07-01 10:28:04.438854', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.accepted.11.text', 'Hemos borrado el campo precio porque consideramos que es err�neo. Por favor verifica que tu precio sea correcto. Recuerda que debes insertar el precio por el total de productos � servicios ofrecidos.', '2015-07-01 10:28:04.439235', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.accepted.2.name', 'SIN IM�GENES', '2015-07-01 10:28:04.439681', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.accepted.2.text', '�Sab�as que un aviso con im�genes tiene m�s visitas y es mucho m�s interesante? �Incluye im�genes y vender�s antes!', '2015-07-01 10:28:04.440084', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.accepted_order.0', '3', '2015-07-01 10:28:04.44055', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.accepted_order.1', '2', '2015-07-01 10:28:04.440944', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.accepted_order.2', '4', '2015-07-01 10:28:04.441338', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.accepted_order.3', '5', '2015-07-01 10:28:04.441739', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.accepted_order.4', '13', '2015-07-01 10:28:04.44215', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.accepted_order.5', '6', '2015-07-01 10:28:04.442594', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.accepted_order.6', '8', '2015-07-01 10:28:04.442992', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.accepted_order.7', '9', '2015-07-01 10:28:04.443363', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.accepted_order.8', '10', '2015-07-01 10:28:04.443775', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.accepted_order.9', '11', '2015-07-01 10:28:04.444231', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.accepted_order.10', '12', '2015-07-01 10:28:04.444687', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.accepted_order.11', '19', '2015-07-01 10:28:04.445093', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.accepted_order.12', '15', '2015-07-01 10:28:04.445521', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.accepted_order.13', '21', '2015-07-01 10:28:04.445915', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.accepted_order.14', '16', '2015-07-01 10:28:04.44631', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.accepted_order.15', '20', '2015-07-01 10:28:04.446725', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.accepted_order.16', '17', '2015-07-01 10:28:04.447122', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.accepted_order.17', '18', '2015-07-01 10:28:04.447553', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.accepted_order.18', '1', '2015-07-01 10:28:04.447982', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.accepted_order.19', '22', '2015-07-01 10:28:04.44838', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal.0.name', 'T�tulo y Descripci�n de Otros', '2015-07-01 10:28:04.448819', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal.1.name', 'T�tulo y Descripci�n del Veh�culo', '2015-07-01 10:28:04.449539', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal.2.name', 'T�tulo y Descripci�n de los Bienes Ra�ces', '2015-07-01 10:28:04.450025', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal.3.name', 'Restricciones para los Animales Dom�sticos', '2015-07-01 10:28:04.450608', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal.4.name', 'Links para otros Sitios', '2015-07-01 10:28:04.451014', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal.5.name', 'Aviso Empresa', '2015-07-01 10:28:04.451437', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal.6.name', 'Contacto', '2015-07-01 10:28:04.451972', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal.7.name', 'Varios elementos', '2015-07-01 10:28:04.452364', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal.8.name', 'Varios elementos - Empleo', '2015-07-01 10:28:04.45283', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal.9.name', 'Avisos personales', '2015-07-01 10:28:04.453206', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal.10.name', 'Varios elementos Veh�culos - Propiedad', '2015-07-01 10:28:04.453687', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal.11.name', 'Aviso doble', '2015-07-01 10:28:04.454162', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal.12.name', 'Aviso caducado', '2015-07-01 10:28:04.454642', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal.13.name', 'IP extranjero', '2015-07-01 10:28:04.455013', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal.14.name', 'Ilegal', '2015-07-01 10:28:04.455423', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal.15.name', 'Contenido de im�genes', '2015-07-01 10:28:04.455805', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal.16.name', 'Error en la imagen', '2015-07-01 10:28:04.456168', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal.17.name', 'Idioma', '2015-07-01 10:28:04.456584', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal.18.name', 'Link', '2015-07-01 10:28:04.45696', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal.19.name', 'Imagen obscena', '2015-07-01 10:28:04.457344', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal.20.name', 'Ofensivo', '2015-07-01 10:28:04.457712', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal.21.name', 'Pirater�a', '2015-07-01 10:28:04.458241', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal.22.name', 'Elementos', '2015-07-01 10:28:04.458696', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal.23.name', 'Marketing', '2015-07-01 10:28:04.459061', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal.24.name', 'Palabras de B�squeda', '2015-07-01 10:28:04.459452', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal.25.name', 'Contrase�a', '2015-07-01 10:28:04.459831', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal.26.name', 'Virus', '2015-07-01 10:28:04.460198', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal.27.name', 'Estado de origen', '2015-07-01 10:28:04.460582', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal.29.name', 'No es realista', '2015-07-01 10:28:04.460951', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal.30.name', 'Categor�a equivocada', '2015-07-01 10:28:04.461355', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal.31.name', 'Spam', '2015-07-01 10:28:04.461727', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal.32.name', 'Propiedad intelectual', '2015-07-01 10:28:04.462085', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal.33.name', 'Privacidad', '2015-07-01 10:28:04.462474', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal.34.name', 'Intercambios', '2015-07-01 10:28:04.462873', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal.35.name', 'Menores de edad', '2015-07-01 10:28:04.46328', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal.0.text', 'Por favor, comprueba que el nombre incluido / modelo / t�tulo / marca del art�culo que deseas vender, est� de acuerdo con  el t�tulo y la descripci�n del producto. El t�tulo del aviso debe describir el producto o servicio anunciado, no se permiten incluir nombres de empresas o URL (direcci�n Web). No es permitido el uso de caracteres especiales o caracteres en may�sculas en el t�tulo.', '2015-07-01 10:28:04.463721', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal.1.text', 'Por favor,  verifica el nombre y modelo (ejemplo: Honda Civic 1.6]) del veh�culo que deseas vender. En la descripci�n, incluye  informaci�n espec�fica.', '2015-07-01 10:28:04.464334', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal.2.text', 'Comprueba  que incluya el t�tulo. El t�tulo del aviso debe describir el producto o servicio anunciado, no se le permite incluir nombres de empresas o  URL (direcci�n Web). No es permitido el uso de caracteres especiales o caracteres en may�sculas en el t�tulo.', '2015-07-01 10:28:04.464901', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal.3.text', 'No permitimos avisos de animales prohibidos por las leyes chilenas de protecci�n animal.', '2015-07-01 10:28:04.465371', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal.4.text', 'No permitimos la inclusi�n de un enlace dirigido a otra p�gina web de e-mail o n�meros de tel�fono en el texto del aviso. No est� permitido hablar de otros sitios web o las subastas de avisos clasificados. Estos v�nculos y referencias deben ser eliminados antes de volver a enviar la notificaci�n.', '2015-07-01 10:28:04.465926', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal.5.text', 'Has introducido un aviso en forma individual o categor�as que no permiten avisos de empresas. Las empresas deben insertar avisos y hacer una lista como los avisos de empresas. Los avisos de empresas no est�n permitidos en las siguientes categor�as: Video Juegos, telefon�a, ropa y prendas de vestir, bolsos, mochilas y accesorios, joyas, relojes y joyas, etc.', '2015-07-01 10:28:04.466357', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal.6.text', 'No hemos podido verificar tu informaci�n de contacto. Por favor, verifica que toda la informaci�n introducida es correcta. Despu�s de la revisi�n, puedes volver a enviar tu aviso.', '2015-07-01 10:28:04.46681', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal.7.text', 'Hay muchos elementos en el mismo aviso. Por favor, escribe  cada elemento de los avisos por separado. No est� permitido introducir m�s de un veh�culo o propiedad en el mismo aviso, excepto si la transacci�n es un intercambio (por ejemplo, 2 por 1).                             ', '2015-07-01 10:28:04.467269', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal.8.text', 'Hay muchas ofertas de trabajo en el mismo aviso. Escribe una oferta por aviso.  No puedes publicar m�s de una propiedad en el mismo aviso.', '2015-07-01 10:28:04.467702', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal.9.text', 'Has introducido un aviso de una empresa. Los avisos deben ser personales.', '2015-07-01 10:28:04.468111', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal.10.text', 'Hay muchos elementos en el mismo aviso. Escribe cada elemento de la lista por separado, para que tu aviso sea m�s relevante para los compradores. No est� permitido introducir m�s de un veh�culo o propiedad en el mismo aviso, a menos que la transacci�n sea  un intercambio necesario (por  ejemplo, 2 por 1).', '2015-07-01 10:28:04.468544', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal.11.text', 'Ya existe otro aviso como el que has introducido. No puedes copiar el texto de los avisos de otros anunciantes, ya que est�n bajo la ley de derechos de autor.. Tampoco est� permitido publicar varios avisos con el mismo producto  o servicio. El aviso anterior debe ser borrado antes de publicar un nuevo  aviso. Tampoco se permite hacer publicidad del mismo art�culo, o servicio en  las diferentes categor�as de avisos en las diferentes regiones.', '2015-07-01 10:28:04.46899', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal.12.text', 'El  producto  ya no est� disponible/ha expirado/fue vendido.', '2015-07-01 10:28:04.469422', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal.13.text', 'Nuestro sitio ofrece clasificados s�lo en Chile. Los productos o servicios s�lo se  encuentran en  Chile y el aviso ser� colocado en la zona donde se encuentra. No se aceptan avisos de fuera del pa�s o en otro idioma que no sea el espa�ol.', '2015-07-01 10:28:04.469857', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal.14.text', 'Tu aviso contiene productos ilegales y/o prohibidos  para la  venta en nuestro sitio. No est� permito hacer publicidad de productos falsificados o falsos, como los productos de marca, CD / VCD / DVD, software / consola de juegos. Para que el aviso sea aceptado y para que este requisitto se aplique, el anunciante debe garantizar que los productos son originales. Los avisos que incluyen los productos para su reventa a la luz de la legislaci�n chilena no pueden ser publicados. Tenemos restricciones sobre el aviso de ciertos bienes y servicios. Lee nuestros T�rminos. Los servicios ofrecidos o solicitados deben cumplir con las leyes chilenas y reglamentarias aplicables a cada profesi�n.', '2015-07-01 10:28:04.470274', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal_order.32', '26', '2015-07-01 10:28:04.492994', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal_order.33', '25', '2015-07-01 10:28:04.493423', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal_order.35', '17', '2015-07-01 10:28:04.493807', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal.15.text', 'Las im�genes deben concordar con los avisos y deben ser relevantes para el art�culo o servicio anunciado. Las im�genes deben representar el art�culo anunciado. No puedes utilizar las im�genes para art�culos del cat�logo de segunda mano, o utilizar logotipos e im�genes de la empresa, excepto en los "Servicios" y "Empleo". No se permite el uso de im�genes de otros anunciantes, sin su consentimiento previo, ni marcas de agua o logos de sitios de la competencia. Las im�genes est�n protegidas por la legislaci�n sobre derechos de autor. No se permite el uso de una imagen en m�s de un aviso.', '2015-07-01 10:28:04.470698', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal.16.text', 'Una o m�s im�genes contienen errores y no se muestran correctamente. Por favor, aseg�rate que el formato de las  im�genes  es  JPG, GIF o BMP. ', '2015-07-01 10:28:04.471171', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal.17.text', 'Su aviso no est� en espa�ol', '2015-07-01 10:28:04.47158', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal.18.text', 'Est�s utilizando enlaces que no son relevantes para el aviso y/o que no funcionan', '2015-07-01 10:28:04.471995', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal.19.text', 'No se permite  publicar im�genes obscenas que muestren a la gente desnuda, en ropa interior o traje de ba�o.', '2015-07-01 10:28:04.472384', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal.20.text', 'Tu aviso puede ser ofensivo para los grupos �tnicos / religiosos, o puede ser considerado  racista, xen�fobo o terrorista, ya que atenta contra el g�nero humano. No se permiten avisos que violen normas constitucionales y que incorporen  contenidos, mensajes o productos de naturaleza violenta o degradantes.', '2015-07-01 10:28:04.472806', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal.21.text', 'No  est� permitido hacer publicidad de productos falsificados o falsos, como los productos de marca, CD / VCD / DVD, software / consola de juegos. Para que el aviso sea aceptado, el anunciante debe garantizar que los productos son originales. Los avisos que incluyen los productos para su reventa a la luz de la legislaci�n chilena, no pueden ser publicados.', '2015-07-01 10:28:04.473268', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal.22.text', 'S�lo se  permite hacer publicidad de ventas, arriendos, empleo y servicios.', '2015-07-01 10:28:04.473671', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal.23.text', 'El aviso contiene productos o servicios que no est�n permitidos en nuestro sitio. Para m�s informaci�n sobre estos productos, visite la p�gina de las Reglas. Si usted tiene alguna pregunta p�ngase en contacto con Atenci�n al Cliente.', '2015-07-01 10:28:04.47409', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal.24.text', 'No est�  permitido el uso de avisos  que tengan como �nico prop�sito el  marketing,  lo que no implica necesariamente el suministro de bienes, trabajos o servicios.', '2015-07-01 10:28:04.474512', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal.25.text', 'No est�  permitido el uso de avisos  que tengan como �nico prop�sito el  marketing,  lo que no implica necesariamente el suministro de bienes, trabajos o servicios.', '2015-07-01 10:28:04.474938', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal.26.text', 'No se pueden  introducir o difundir en la red,  programas de datos (virus y software maliciosos) que puedan  causar da�os al proveedor de acceso, a sistemas inform�ticos  de nuestros usuarios del sitio o de terceros que utilicen la misma red.', '2015-07-01 10:28:04.475378', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal.27.text', 'No  se ha declarado la autenticidad de tu producto. Para que el aviso sea  aceptado, el anunciante debe garantizar que los productos son originales.', '2015-07-01 10:28:04.475792', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal.29.text', 'No se permite la publicaci�n de avisos que no posean ofertas cre�bles y realistas. No se permite que los avisos contengan cualquier informaci�n con contenidos falsos, ambiguos o inexactos, con el fin de inducir al error a potenciales receptores de dicha informaci�n.', '2015-07-01 10:28:04.476179', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal.30.text', 'El aviso se publicar� en la categor�a que mejor describa el art�culo o servicio. Nos reservamos el derecho, si es necesario, a mover el aviso a la categor�a m�s apropiada. Bienes y servicios que no entren en la misma categor�a ser�n publicados en diferentes avisos. Para la venta se publicar� en "Se vende" y los avisos que demanden  un producto se publicar�n en "Se compra". En algunas categor�as los avisos podr�an incluir las opciones de "Arriendo" y "Se busca  arriendo". En otras categor�as, si es necesario, los avisos de "Se arrienda"  se publicar�n en "Venta" y los avisos "Quiero  arrendar", se publicar�n en "Se compra".', '2015-07-01 10:28:04.476593', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal.31.text', 'No est� permitido enviar publicidad no solicitada o autorizada, material publicitario, "correo basura", "cartas en cadena", "marketing piramidal" o cualquier otra forma de solicitud.Tu aviso se inscribe en estas categor�as.', '2015-07-01 10:28:04.477037', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal.32.text', 'No se permite difundir, transmitir o poner a disposici�n de terceros cualquier tipo de informaci�n, elemento o contenido que implica la violaci�n de los derechos de propiedad intelectual, incluyendo derechos de autor y propiedad industrial,  marcas, derechos de autor o de propiedad de los due�os de este sitio o de terceros.', '2015-07-01 10:28:04.477455', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal.33.text', 'No se permite difundir, transmitir o poner a disposici�n de terceros, cualquier tipo de informaci�n, elemento o contenido que implique  la violaci�n del secreto de las comunicaciones y la intimidad.', '2015-07-01 10:28:04.477866', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal.34.text', 'No se permiten m�s de cinco referencias a los productos  que podr�an constituir la base del intercambio. Los intercambios est�n permitidos en el sitio, pero se debe hacer una lista de menos de cinco referencias a los productos que forman la base del intercambio.', '2015-07-01 10:28:04.478305', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal.35.text', 'Categor�a errada', '2015-07-01 10:28:04.478682', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal_order.0', '32', '2015-07-01 10:28:04.479097', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal_order.1', '33', '2015-07-01 10:28:04.479698', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal_order.2', '31', '2015-07-01 10:28:04.480128', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal_order.3', '27', '2015-07-01 10:28:04.480555', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal_order.4', '25', '2015-07-01 10:28:04.480979', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal_order.5', '0', '2015-07-01 10:28:04.481443', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal_order.6', '5', '2015-07-01 10:28:04.481859', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal_order.7', '18', '2015-07-01 10:28:04.482305', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal_order.8', '19', '2015-07-01 10:28:04.482691', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal_order.9', '3', '2015-07-01 10:28:04.483115', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal_order.10', '20', '2015-07-01 10:28:04.483538', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal_order.11', '2', '2015-07-01 10:28:04.483967', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal_order.12', '1', '2015-07-01 10:28:04.484389', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal_order.13', '12', '2015-07-01 10:28:04.484828', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal_order.14', '10', '2015-07-01 10:28:04.485351', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal_order.15', '6', '2015-07-01 10:28:04.485751', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal_order.16', '7', '2015-07-01 10:28:04.486177', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal_order.17', '9', '2015-07-01 10:28:04.486593', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal_order.18', '14', '2015-07-01 10:28:04.487015', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal_order.19', '11', '2015-07-01 10:28:04.487437', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal_order.20', '22', '2015-07-01 10:28:04.487895', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal_order.21', '24', '2015-07-01 10:28:04.488365', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal_order.22', '13', '2015-07-01 10:28:04.488752', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal_order.23', '16', '2015-07-01 10:28:04.489172', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal_order.24', '23', '2015-07-01 10:28:04.489588', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal_order.25', '29', '2015-07-01 10:28:04.490018', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal_order.26', '34', '2015-07-01 10:28:04.490447', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal_order.27', '8', '2015-07-01 10:28:04.490882', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal_order.28', '35', '2015-07-01 10:28:04.49134', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal_order.29', '21', '2015-07-01 10:28:04.491739', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal_order.30', '4', '2015-07-01 10:28:04.492173', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal_order.31', '30', '2015-07-01 10:28:04.49259', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.ais.synonyms.word.1.word', 'caravana', '2015-07-01 10:28:04.494233', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.ais.synonyms.word.1.synonyms.2', 'casa-m�vel', '2015-07-01 10:28:04.494686', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.ais.synonyms.word.1.synonyms.4', 'roulotte', '2015-07-01 10:28:04.495103', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.ais.synonyms.word.2.word', 'piso', '2015-07-01 10:28:04.495537', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.ais.synonyms.word.2.synonyms.1', 'apartamento', '2015-07-01 10:28:04.49594', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.ais.synonyms.word.2.synonyms.2', 'loft', '2015-07-01 10:28:04.496366', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.ais.synonyms.word.3.word', 'vw', '2015-07-01 10:28:04.496745', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.ais.synonyms.word.3.synonyms.1', 'volkswagen', '2015-07-01 10:28:04.497162', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.ais.synonyms.word.4.word', 'carro', '2015-07-01 10:28:04.497565', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.ais.synonyms.word.4.synonyms.1', 'autom�vel', '2015-07-01 10:28:04.497969', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.ais.synonyms.word.4.synonyms.2', 'buga', '2015-07-01 10:28:04.498373', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.ais.misspellings.word.1.misspelled.1', 'vlokswagen', '2015-07-01 10:28:04.498749', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.ais.misspellings.word.1.misspelled.2', 'volksbagen', '2015-07-01 10:28:04.499183', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.ais.misspellings.word.1.misspelled.3', 'volsvagen', '2015-07-01 10:28:04.499583', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.ais.misspellings.word.1.intended', 'volkswagen', '2015-07-01 10:28:04.500015', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.ais.misspellings.word.2.misspelled.1', 'apratamento', '2015-07-01 10:28:04.500441', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.ais.misspellings.word.2.misspelled.2', 'apatamento', '2015-07-01 10:28:04.501122', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.ais.misspellings.word.2.misspelled.3', 'apartametno', '2015-07-01 10:28:04.501619', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.ais.misspellings.word.2.intended', 'apartamento', '2015-07-01 10:28:04.502128', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.ais.misspellings.word.3.misspelled.1', 'carabana', '2015-07-01 10:28:04.502546', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.ais.misspellings.word.3.misspelled.2', 'cravana', '2015-07-01 10:28:04.502913', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.ais.misspellings.word.3.intended', 'caravana', '2015-07-01 10:28:04.503343', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.uf_conversion_factor', '22245,5290', '2015-07-01 10:28:04.503715', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.uf_conversion_updated', '2015-07-01 10:28:04.504307-03', '2015-07-01 10:28:04.504307', NULL);


--
-- Name: data_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('data_id_seq', 1294, true);


--
-- Data for Name: event_log; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Name: event_log_event_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('event_log_event_id_seq', 1, false);


--
-- Data for Name: example; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO example (id, col) VALUES (0, 12);
INSERT INTO example (id, col) VALUES (1, 56);
INSERT INTO example (id, col) VALUES (2, 32);
INSERT INTO example (id, col) VALUES (3, 156);
INSERT INTO example (id, col) VALUES (4, 2345);


--
-- Data for Name: filters; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO filters (filter_id, name, tables, condition, use_archive) VALUES (1, 'all', '', '1=1', true);
INSERT INTO filters (filter_id, name, tables, condition, use_archive) VALUES (2, 'unpaid', '', 'ad_actions.state = ''unpaid''', false);
INSERT INTO filters (filter_id, name, tables, condition, use_archive) VALUES (3, 'unverified', '', 'ad_actions.state = ''unverified''', false);
INSERT INTO filters (filter_id, name, tables, condition, use_archive) VALUES (4, 'new', '', 'ad_actions.queue = ''normal'' AND ad_actions.state IN (''pending_review'', ''locked'')', false);
INSERT INTO filters (filter_id, name, tables, condition, use_archive) VALUES (5, 'unsolved', '', 'ad_actions.queue = ''unsolved'' AND ad_actions.state IN (''pending_review'',''locked'')', false);
INSERT INTO filters (filter_id, name, tables, condition, use_archive) VALUES (6, 'abuse', '', 'ad_actions.queue = ''abuse'' AND ad_actions.state IN (''pending_review'',''locked'')', false);
INSERT INTO filters (filter_id, name, tables, condition, use_archive) VALUES (7, 'accepted', '', 'ad_actions.state = ''accepted''', false);
INSERT INTO filters (filter_id, name, tables, condition, use_archive) VALUES (8, 'refused', '', 'ad_actions.state = ''refused''', true);
INSERT INTO filters (filter_id, name, tables, condition, use_archive) VALUES (9, 'technical_error', '', 'ad_actions.queue = ''technical_error'' AND ad_actions.state NOT IN (''accepted'', ''refused'')', false);
INSERT INTO filters (filter_id, name, tables, condition, use_archive) VALUES (10, 'published', '', 'ad_actions.state IN (''accepted'', ''published'') AND NOT EXISTS (SELECT * FROM action_states AS newer WHERE ad_id = ad_actions.ad_id AND action_id != ad_actions.action_id AND state = ''accepted'' AND timestamp > newer.timestamp) AND NOT EXISTS ( SELECT * FROM ads WHERE ads.status = ''deleted'' AND ad_id=ad_actions.ad_id)', false);
INSERT INTO filters (filter_id, name, tables, condition, use_archive) VALUES (11, 'deleted', '', 'ad_actions.state = ''deleted''', true);
INSERT INTO filters (filter_id, name, tables, condition, use_archive) VALUES (12, 'autoaccept', '', 'ad_actions.queue = ''autoaccept'' AND ad_actions.state IN (''pending_review'', ''locked'')', false);
INSERT INTO filters (filter_id, name, tables, condition, use_archive) VALUES (13, 'autorefuse', '', 'ad_actions.queue = ''autorefuse'' AND ad_actions.state IN (''pending_review'', ''locked'')', false);
INSERT INTO filters (filter_id, name, tables, condition, use_archive) VALUES (16, 'video', '', 'ad_actions.queue = ''video'' AND ad_actions.state IN (''pending_review'', ''locked'')', false);
INSERT INTO filters (filter_id, name, tables, condition, use_archive) VALUES (17, 'spidered', '', 'ad_actions.queue = ''spidered'' AND ad_actions.state IN (''pending_review'', ''locked'')', false);


--
-- Name: filters_filter_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('filters_filter_id_seq', 1, false);


--
-- Data for Name: hold_mail_params; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Name: hold_mail_params_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('hold_mail_params_id_seq', 1, false);


--
-- Name: item_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('item_id_seq', 59, true);


--
-- Data for Name: iteminfo_items; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO iteminfo_items (item_id, word, defining, parent_id) VALUES (1, 'root', false, NULL);
INSERT INTO iteminfo_items (item_id, word, defining, parent_id) VALUES (2, 'primogenito', true, 1);
INSERT INTO iteminfo_items (item_id, word, defining, parent_id) VALUES (3, 'segundon', false, 1);
INSERT INTO iteminfo_items (item_id, word, defining, parent_id) VALUES (4, 'car_info', false, 1);
INSERT INTO iteminfo_items (item_id, word, defining, parent_id) VALUES (5, 'location_info', false, 1);
INSERT INTO iteminfo_items (item_id, word, defining, parent_id) VALUES (6, 'object_info', false, 1);
INSERT INTO iteminfo_items (item_id, word, defining, parent_id) VALUES (7, 'moto_info', false, 1);
INSERT INTO iteminfo_items (item_id, word, defining, parent_id) VALUES (8, 'sac', false, 6);
INSERT INTO iteminfo_items (item_id, word, defining, parent_id) VALUES (9, 'chanel', false, 8);
INSERT INTO iteminfo_items (item_id, word, defining, parent_id) VALUES (10, 'bmw', false, 7);
INSERT INTO iteminfo_items (item_id, word, defining, parent_id) VALUES (11, 'triumph', false, 7);
INSERT INTO iteminfo_items (item_id, word, defining, parent_id) VALUES (12, 'r100', true, 10);
INSERT INTO iteminfo_items (item_id, word, defining, parent_id) VALUES (13, 'r1100 gs', true, 10);
INSERT INTO iteminfo_items (item_id, word, defining, parent_id) VALUES (14, 'k100', true, 10);
INSERT INTO iteminfo_items (item_id, word, defining, parent_id) VALUES (15, 'america', true, 11);
INSERT INTO iteminfo_items (item_id, word, defining, parent_id) VALUES (16, 'adventurer', true, 11);
INSERT INTO iteminfo_items (item_id, word, defining, parent_id) VALUES (17, '1000', false, 12);
INSERT INTO iteminfo_items (item_id, word, defining, parent_id) VALUES (18, '1100', false, 13);
INSERT INTO iteminfo_items (item_id, word, defining, parent_id) VALUES (19, '1100 75 ans', false, 13);
INSERT INTO iteminfo_items (item_id, word, defining, parent_id) VALUES (20, '1100 abs', false, 13);
INSERT INTO iteminfo_items (item_id, word, defining, parent_id) VALUES (21, '1100 abs ergo', false, 13);
INSERT INTO iteminfo_items (item_id, word, defining, parent_id) VALUES (22, '1100 ergo', false, 13);
INSERT INTO iteminfo_items (item_id, word, defining, parent_id) VALUES (23, 'k100 lt', false, 14);
INSERT INTO iteminfo_items (item_id, word, defining, parent_id) VALUES (24, 'k100 rs', false, 14);
INSERT INTO iteminfo_items (item_id, word, defining, parent_id) VALUES (25, '865', false, 15);
INSERT INTO iteminfo_items (item_id, word, defining, parent_id) VALUES (26, '900', false, 16);
INSERT INTO iteminfo_items (item_id, word, defining, parent_id) VALUES (27, 'ford', false, 4);
INSERT INTO iteminfo_items (item_id, word, defining, parent_id) VALUES (28, 'renault', false, 4);
INSERT INTO iteminfo_items (item_id, word, defining, parent_id) VALUES (29, 'seat', false, 4);
INSERT INTO iteminfo_items (item_id, word, defining, parent_id) VALUES (30, 'escort', true, 27);
INSERT INTO iteminfo_items (item_id, word, defining, parent_id) VALUES (31, 'fiesta', true, 27);
INSERT INTO iteminfo_items (item_id, word, defining, parent_id) VALUES (32, 'laguna', true, 28);
INSERT INTO iteminfo_items (item_id, word, defining, parent_id) VALUES (33, 'megane', true, 28);
INSERT INTO iteminfo_items (item_id, word, defining, parent_id) VALUES (34, 'scenic', true, 28);
INSERT INTO iteminfo_items (item_id, word, defining, parent_id) VALUES (35, 'cordoba', true, 29);
INSERT INTO iteminfo_items (item_id, word, defining, parent_id) VALUES (36, 'ibiza', true, 29);
INSERT INTO iteminfo_items (item_id, word, defining, parent_id) VALUES (37, 'leon', true, 29);
INSERT INTO iteminfo_items (item_id, word, defining, parent_id) VALUES (38, 'cosworth', false, 30);
INSERT INTO iteminfo_items (item_id, word, defining, parent_id) VALUES (39, 'rs 2000', false, 30);
INSERT INTO iteminfo_items (item_id, word, defining, parent_id) VALUES (40, '1.8 turbo', false, 31);
INSERT INTO iteminfo_items (item_id, word, defining, parent_id) VALUES (41, '1.4 senso 5p', false, 31);
INSERT INTO iteminfo_items (item_id, word, defining, parent_id) VALUES (42, '1.8 16s', false, 32);
INSERT INTO iteminfo_items (item_id, word, defining, parent_id) VALUES (43, '1.8 gpl', false, 32);
INSERT INTO iteminfo_items (item_id, word, defining, parent_id) VALUES (44, '2.0 rxe', false, 32);
INSERT INTO iteminfo_items (item_id, word, defining, parent_id) VALUES (45, '1.9 dti', false, 32);
INSERT INTO iteminfo_items (item_id, word, defining, parent_id) VALUES (46, '2.2 d rt', false, 32);
INSERT INTO iteminfo_items (item_id, word, defining, parent_id) VALUES (47, '2.2 d rxe', false, 32);
INSERT INTO iteminfo_items (item_id, word, defining, parent_id) VALUES (48, '1.9 dci 110 authentique', false, 32);
INSERT INTO iteminfo_items (item_id, word, defining, parent_id) VALUES (49, '2.2 dci 150 initiale', false, 32);
INSERT INTO iteminfo_items (item_id, word, defining, parent_id) VALUES (50, '1.9 dci 120 ch expression', false, 32);
INSERT INTO iteminfo_items (item_id, word, defining, parent_id) VALUES (51, '1.9 sdi 3p', false, 36);
INSERT INTO iteminfo_items (item_id, word, defining, parent_id) VALUES (52, '1.9 tdi 105 reference', false, 37);
INSERT INTO iteminfo_items (item_id, word, defining, parent_id) VALUES (53, '2.0 tdi 140 stylance', false, 37);
INSERT INTO iteminfo_items (item_id, word, defining, parent_id) VALUES (54, 'tdi 110 signo', false, 37);
INSERT INTO iteminfo_items (item_id, word, defining, parent_id) VALUES (55, 'tdi 150 fr', false, 37);
INSERT INTO iteminfo_items (item_id, word, defining, parent_id) VALUES (56, 'tdi 150 sport', false, 37);
INSERT INTO iteminfo_items (item_id, word, defining, parent_id) VALUES (57, 'rx4 1.9 dci', false, 34);
INSERT INTO iteminfo_items (item_id, word, defining, parent_id) VALUES (58, '1.9 d rte', false, 34);
INSERT INTO iteminfo_items (item_id, word, defining, parent_id) VALUES (59, 'd rte', false, 34);
INSERT INTO iteminfo_items (item_id, word, defining, parent_id) VALUES (2001, '22', false, 5);
INSERT INTO iteminfo_items (item_id, word, defining, parent_id) VALUES (2002, '01', false, 2001);
INSERT INTO iteminfo_items (item_id, word, defining, parent_id) VALUES (2003, '01000', false, 2002);
INSERT INTO iteminfo_items (item_id, word, defining, parent_id) VALUES (2004, 'bourg en bresse', false, 2003);
INSERT INTO iteminfo_items (item_id, word, defining, parent_id) VALUES (2005, 'st denis les bourg', false, 2003);
INSERT INTO iteminfo_items (item_id, word, defining, parent_id) VALUES (2006, '12', false, 5);
INSERT INTO iteminfo_items (item_id, word, defining, parent_id) VALUES (2007, '75', false, 2006);
INSERT INTO iteminfo_items (item_id, word, defining, parent_id) VALUES (2008, '75001', false, 2007);
INSERT INTO iteminfo_items (item_id, word, defining, parent_id) VALUES (2009, 'paris', false, 2008);


--
-- Data for Name: iteminfo_data; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1, 'Hipoteca', 'De cojones', 1);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (2, 'Sueldo', 'Justico', 1);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (3, 'Broncas', 'Todas', 2);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (4, 'Bici', 'Heredada de su hermano', 3);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (5, 'price_logic_max_cat_2120', '10000', 6);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (6, 'price_logic_max_cat_3040', '20000', 6);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (7, 'price_logic_max_cat_3060', '10000', 6);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (8, 'price_logic_max_cat_8020', '20000', 6);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (9, 'price_logic_max_cat_8020', '560', 8);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (10, 'price_logic_max_cat_22', '1050', 9);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (11, 'price_fraud_cat_8020', '1050', 9);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (12, 'price_logic_max_cat_8060', '2000', 6);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (13, 'price_logic_max_cat_4100', '15000', 6);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (14, 'price_logic_max_cat_4120', '10000', 6);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (15, 'price_logic_max_cat_39', '10000', 6);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (16, 'price_logic_max_cat_41', '20000', 6);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (17, 'price_fraud_1976', '1600', 12);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (18, 'price_logic_min_1976', '2560', 12);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (19, 'price_logic_max_1976', '6400', 12);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (20, 'price_fraud_1977', '975', 12);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (21, 'price_logic_min_1977', '1560', 12);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (22, 'price_logic_max_1977', '3900', 12);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (23, 'price_fraud_1978', '1498.5', 12);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (24, 'price_logic_min_1978', '2397.6', 12);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (25, 'price_logic_max_1978', '5994', 12);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (26, 'price_fraud_1979', '1450', 12);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (27, 'price_logic_min_1979', '2320', 12);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (28, 'price_logic_max_1979', '5800', 12);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (29, 'price_fraud_1980', '1768', 12);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (30, 'price_logic_min_1980', '2828.8', 12);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (31, 'price_logic_max_1980', '7072', 12);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (32, 'price_fraud_1981', '1325', 12);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (33, 'price_logic_min_1981', '2120', 12);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (34, 'price_logic_max_1981', '5300', 12);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (35, 'price_fraud_1982', '1325', 12);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (36, 'price_logic_min_1982', '2120', 12);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (37, 'price_logic_max_1982', '5300', 12);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (38, 'price_fraud_1983', '1500', 12);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (39, 'price_logic_min_1983', '2400', 12);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (40, 'price_logic_max_1983', '6000', 12);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (41, 'price_fraud_1984', '1900', 12);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (42, 'price_logic_min_1984', '3040', 12);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (43, 'price_logic_max_1984', '7600', 12);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (44, 'price_fraud_1988', '1450', 12);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (45, 'price_logic_min_1988', '2320', 12);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (46, 'price_logic_max_1988', '5800', 12);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (47, 'price_fraud_1989', '1500', 12);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (48, 'price_logic_min_1989', '2400', 12);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (49, 'price_logic_max_1989', '6000', 12);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (50, 'price_fraud_1990', '2000', 12);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (51, 'price_logic_min_1990', '3200', 12);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (52, 'price_logic_max_1990', '8000', 12);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (53, 'price_fraud_1991', '1900', 12);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (54, 'price_logic_min_1991', '3040', 12);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (55, 'price_logic_max_1991', '7600', 12);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (56, 'price_fraud_1992', '1942.5', 12);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (57, 'price_logic_min_1992', '3108', 12);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (58, 'price_logic_max_1992', '7770', 12);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (59, 'price_fraud_1993', '2322.5', 12);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (60, 'price_logic_min_1993', '3716', 12);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (61, 'price_logic_max_1993', '9290', 12);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (62, 'price_fraud_1994', '2625', 12);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (63, 'price_logic_min_1994', '4200', 12);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (64, 'price_logic_max_1994', '10500', 12);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (65, 'price_fraud_1995', '1691.5', 12);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (66, 'price_logic_min_1995', '2706.4', 12);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (67, 'price_logic_max_1995', '6766', 12);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (68, 'price_fraud_1996', '2525', 12);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (69, 'price_logic_min_1996', '4040', 12);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (70, 'price_logic_max_1996', '10100', 12);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (71, 'price_fraud_1975', '2487.5', 11);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (72, 'price_logic_min_1975', '3980', 11);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (73, 'price_logic_max_1975', '9950', 11);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (74, 'price_fraud_1977', '2150', 11);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (75, 'price_logic_min_1977', '3440', 11);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (76, 'price_logic_max_1977', '8600', 11);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (77, 'price_fraud_1978', '2400', 11);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (78, 'price_logic_min_1978', '3840', 11);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (79, 'price_logic_max_1978', '9600', 11);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (80, 'price_fraud_1979', '2625', 11);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (81, 'price_logic_min_1979', '4200', 11);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (82, 'price_logic_max_1979', '10500', 11);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (83, 'price_fraud_1980', '3050', 11);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (84, 'price_logic_min_1980', '4880', 11);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (85, 'price_logic_max_1980', '12200', 11);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (86, 'price_fraud_1981', '2750', 11);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (87, 'price_logic_min_1981', '4400', 11);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (88, 'price_logic_max_1981', '11000', 11);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (89, 'price_fraud_1986', '6750', 11);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (90, 'price_logic_min_1986', '10800', 11);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (91, 'price_logic_max_1986', '27000', 11);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (92, 'price_fraud_1992', '1428.5', 11);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (93, 'price_logic_min_1992', '2285.6', 11);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (94, 'price_logic_max_1992', '5714', 11);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (95, 'price_fraud_1993', '1461', 11);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (96, 'price_logic_min_1993', '2337.6', 11);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (97, 'price_logic_max_1993', '5844', 11);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (98, 'price_fraud_1994', '1866.5', 11);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (99, 'price_logic_min_1994', '2986.4', 11);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (100, 'price_logic_max_1994', '7466', 11);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (101, 'price_fraud_1995', '1736.5', 11);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (102, 'price_logic_min_1995', '2778.4', 11);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (103, 'price_logic_max_1995', '6946', 11);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (104, 'price_fraud_1996', '1842.5', 11);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (105, 'price_logic_min_1996', '2948', 11);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (106, 'price_logic_max_1996', '7370', 11);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (107, 'price_fraud_1997', '1906', 11);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (108, 'price_logic_min_1997', '3049.6', 11);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (109, 'price_logic_max_1997', '7624', 11);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (110, 'price_fraud_1998', '2011.5', 11);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (111, 'price_logic_min_1998', '3218.4', 11);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (112, 'price_logic_max_1998', '8046', 11);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (113, 'price_fraud_1999', '2202.5', 11);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (114, 'price_logic_min_1999', '3524', 11);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (115, 'price_logic_max_1999', '8810', 11);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (116, 'price_fraud_2000', '2071.5', 11);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (117, 'price_logic_min_2000', '3314.4', 11);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (118, 'price_logic_max_2000', '8286', 11);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (119, 'price_fraud_2001', '2397.5', 11);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (120, 'price_logic_min_2001', '3836', 11);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (121, 'price_logic_max_2001', '9590', 11);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (122, 'price_fraud_2002', '2549', 11);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (123, 'price_logic_min_2002', '4078.4', 11);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (124, 'price_logic_max_2002', '10196', 11);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (125, 'price_fraud_2003', '2785', 11);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (126, 'price_logic_min_2003', '4456', 11);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (127, 'price_logic_max_2003', '11140', 11);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (128, 'price_fraud_2004', '3007', 11);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (129, 'price_logic_min_2004', '4811.2', 11);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (130, 'price_logic_max_2004', '12028', 11);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (131, 'price_fraud_2005', '4200.5', 11);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (132, 'price_logic_min_2005', '6720.8', 11);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (133, 'price_logic_max_2005', '16802', 11);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (134, 'price_fraud_2006', '4121.5', 11);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (135, 'price_logic_min_2006', '6594.4', 11);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (136, 'price_logic_max_2006', '16486', 11);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (137, 'price_fraud_2007', '4257.5', 11);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (138, 'price_logic_min_2007', '6812', 11);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (139, 'price_logic_max_2007', '17030', 11);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (140, 'price_fraud_2008', '4357.5', 11);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (141, 'price_logic_min_2008', '6972', 11);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (142, 'price_logic_max_2008', '17430', 11);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (143, 'price_fraud_2001', '2995', 15);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (144, 'price_logic_min_2001', '4792', 15);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (145, 'price_logic_max_2001', '11980', 15);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (146, 'price_fraud_2002', '2625', 15);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (147, 'price_logic_min_2002', '4200', 15);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (148, 'price_logic_max_2002', '10500', 15);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (149, 'price_fraud_2003', '2600', 15);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (150, 'price_logic_min_2003', '4160', 15);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (151, 'price_logic_max_2003', '10400', 15);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (152, 'price_fraud_2004', '3250', 15);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (153, 'price_logic_min_2004', '5200', 15);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (154, 'price_logic_max_2004', '13000', 15);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (155, 'price_fraud_2007', '3750', 15);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (156, 'price_logic_min_2007', '6000', 15);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (157, 'price_logic_max_2007', '15000', 15);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (158, 'price_fraud_1975', '1720.5', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (159, 'price_logic_min_1975', '2752.8', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (160, 'price_logic_max_1975', '6882', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (161, 'price_fraud_1976', '1610.5', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (162, 'price_logic_min_1976', '2576.8', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (163, 'price_logic_max_1976', '6442', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (164, 'price_fraud_1977', '1150', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (165, 'price_logic_min_1977', '1840', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (166, 'price_logic_max_1977', '4600', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (167, 'price_fraud_1978', '1491.5', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (168, 'price_logic_min_1978', '2386.4', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (169, 'price_logic_max_1978', '5966', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (170, 'price_fraud_1979', '1272.5', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (171, 'price_logic_min_1979', '2036', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (172, 'price_logic_max_1979', '5090', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (173, 'price_fraud_1980', '1623', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (174, 'price_logic_min_1980', '2596.8', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (175, 'price_logic_max_1980', '6492', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (176, 'price_fraud_1981', '1257', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (177, 'price_logic_min_1981', '2011.2', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (178, 'price_logic_max_1981', '5028', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (179, 'price_fraud_1982', '1237.5', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (180, 'price_logic_min_1982', '1980', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (181, 'price_logic_max_1982', '4950', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (182, 'price_fraud_1983', '1111', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (183, 'price_logic_min_1983', '1777.6', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (184, 'price_logic_max_1983', '4444', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (185, 'price_fraud_1984', '1127', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (186, 'price_logic_min_1984', '1803.2', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (187, 'price_logic_max_1984', '4508', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (188, 'price_fraud_1985', '1425', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (189, 'price_logic_min_1985', '2280', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (190, 'price_logic_max_1985', '5700', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (191, 'price_fraud_1986', '1179', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (192, 'price_logic_min_1986', '1886.4', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (193, 'price_logic_max_1986', '4716', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (194, 'price_fraud_1987', '1336.5', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (195, 'price_logic_min_1987', '2138.4', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (196, 'price_logic_max_1987', '5346', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (197, 'price_fraud_1988', '1402', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (198, 'price_logic_min_1988', '2243.2', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (199, 'price_logic_max_1988', '5608', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (200, 'price_fraud_1989', '1327.5', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (201, 'price_logic_min_1989', '2124', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (202, 'price_logic_max_1989', '5310', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (203, 'price_fraud_1990', '1678', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (204, 'price_logic_min_1990', '2684.8', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (205, 'price_logic_max_1990', '6712', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (206, 'price_fraud_1991', '1612', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (207, 'price_logic_min_1991', '2579.2', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (208, 'price_logic_max_1991', '6448', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (209, 'price_fraud_1992', '1703', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (210, 'price_logic_min_1992', '2724.8', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (211, 'price_logic_max_1992', '6812', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (212, 'price_fraud_1993', '1699.5', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (213, 'price_logic_min_1993', '2719.2', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (214, 'price_logic_max_1993', '6798', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (215, 'price_fraud_1994', '1561', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (216, 'price_logic_min_1994', '2497.6', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (217, 'price_logic_max_1994', '6244', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (218, 'price_fraud_1995', '1942', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (219, 'price_logic_min_1995', '3107.2', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (220, 'price_logic_max_1995', '7768', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (221, 'price_fraud_1996', '1993', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (222, 'price_logic_min_1996', '3188.8', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (223, 'price_logic_max_1996', '7972', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (224, 'price_fraud_1997', '2398', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (225, 'price_logic_min_1997', '3836.8', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (226, 'price_logic_max_1997', '9592', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (227, 'price_fraud_1998', '2456.5', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (228, 'price_logic_min_1998', '3930.4', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (229, 'price_logic_max_1998', '9826', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (230, 'price_fraud_1999', '2640', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (231, 'price_logic_min_1999', '4224', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (232, 'price_logic_max_1999', '10560', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (233, 'price_fraud_2000', '2942.5', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (234, 'price_logic_min_2000', '4708', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (235, 'price_logic_max_2000', '11770', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (236, 'price_fraud_2001', '3435.5', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (237, 'price_logic_min_2001', '5496.8', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (238, 'price_logic_max_2001', '13742', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (239, 'price_fraud_2002', '3541', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (240, 'price_logic_min_2002', '5665.6', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (241, 'price_logic_max_2002', '14164', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (242, 'price_fraud_2003', '3900', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (243, 'price_logic_min_2003', '6240', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (244, 'price_logic_max_2003', '15600', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (245, 'price_fraud_2004', '4205.5', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (246, 'price_logic_min_2004', '6728.8', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (247, 'price_logic_max_2004', '16822', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (248, 'price_fraud_2005', '4750.5', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (249, 'price_logic_min_2005', '7600.8', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (250, 'price_logic_max_2005', '19002', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (251, 'price_fraud_2006', '5451.5', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (252, 'price_logic_min_2006', '8722.4', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (253, 'price_logic_max_2006', '21806', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (254, 'price_fraud_2007', '6064', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (255, 'price_logic_min_2007', '9702.4', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (256, 'price_logic_max_2007', '24256', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (257, 'price_fraud_2008', '6650.5', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (258, 'price_logic_min_2008', '10640.8', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (259, 'price_logic_max_2008', '26602', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (260, 'price_fraud_1996', '1950', 26);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (261, 'price_logic_min_1996', '3120', 26);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (262, 'price_logic_max_1996', '7800', 26);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (263, 'price_fraud_1997', '1750', 26);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (264, 'price_logic_min_1997', '2800', 26);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (265, 'price_logic_max_1997', '7000', 26);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (266, 'price_fraud_2001', '2800', 26);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (267, 'price_logic_min_2001', '4480', 26);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (268, 'price_logic_max_2001', '11200', 26);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (269, 'price_fraud_1996', '1875', 16);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (270, 'price_logic_min_1996', '3000', 16);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (271, 'price_logic_max_1996', '7500', 16);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (272, 'price_fraud_1997', '1766.5', 16);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (273, 'price_logic_min_1997', '2826.4', 16);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (274, 'price_logic_max_1997', '7066', 16);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (275, 'price_fraud_1998', '2000', 16);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (276, 'price_logic_min_1998', '3200', 16);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (277, 'price_logic_max_1998', '8000', 16);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (278, 'price_fraud_2001', '2800', 16);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (279, 'price_logic_min_2001', '4480', 16);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (280, 'price_logic_max_2001', '11200', 16);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (281, 'price_fraud_1992', '5625', 38);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (282, 'price_logic_min_1992', '9000', 38);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (283, 'price_logic_max_1992', '22500', 38);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (284, 'price_fraud_1993', '9233', 38);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (285, 'price_logic_min_1993', '14772.8', 38);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (286, 'price_logic_max_1993', '36932', 38);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (287, 'price_fraud_1994', '10500', 38);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (288, 'price_logic_min_1994', '16800', 38);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (289, 'price_logic_max_1994', '42000', 38);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (290, 'price_fraud_1995', '12000', 38);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (291, 'price_logic_min_1995', '19200', 38);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (292, 'price_logic_max_1995', '48000', 38);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (293, 'price_fraud_1992', '1450', 39);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (294, 'price_logic_min_1992', '2320', 39);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (295, 'price_logic_max_1992', '5800', 39);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (296, 'price_fraud_1993', '2250', 39);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (297, 'price_logic_min_1993', '3600', 39);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (298, 'price_logic_max_1993', '9000', 39);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (299, 'price_fraud_1995', '1783', 39);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (300, 'price_logic_min_1995', '2852.8', 39);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (301, 'price_logic_max_1995', '7132', 39);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (302, 'price_fraud_1996', '1683', 39);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (303, 'price_logic_min_1996', '2692.8', 39);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (304, 'price_logic_max_1996', '6732', 39);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (305, 'price_fraud_1998', '6100', 39);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (306, 'price_logic_min_1998', '9760', 39);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (307, 'price_logic_max_1998', '24400', 39);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (308, 'price_fraud_1975', '991.5', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (309, 'price_logic_min_1975', '1586.4', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (310, 'price_logic_max_1975', '3966', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (311, 'price_fraud_1976', '750', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (312, 'price_logic_min_1976', '1200', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (313, 'price_logic_max_1976', '3000', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (314, 'price_fraud_1977', '1500', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (315, 'price_logic_min_1977', '2400', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (316, 'price_logic_max_1977', '6000', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (317, 'price_fraud_1979', '437.5', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (318, 'price_logic_min_1979', '700', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (319, 'price_logic_max_1979', '1750', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (320, 'price_fraud_1980', '1050', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (321, 'price_logic_min_1980', '1680', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (322, 'price_logic_max_1980', '4200', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (323, 'price_fraud_1981', '375', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (324, 'price_logic_min_1981', '600', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (325, 'price_logic_max_1981', '1500', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (326, 'price_fraud_1982', '230', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (327, 'price_logic_min_1982', '368', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (328, 'price_logic_max_1982', '920', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (329, 'price_fraud_1983', '343.5', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (330, 'price_logic_min_1983', '549.6', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (331, 'price_logic_max_1983', '1374', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (332, 'price_fraud_1984', '520', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (333, 'price_logic_min_1984', '832', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (334, 'price_logic_max_1984', '2080', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (335, 'price_fraud_1985', '365', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (336, 'price_logic_min_1985', '584', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (337, 'price_logic_max_1985', '1460', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (338, 'price_fraud_1986', '703.5', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (339, 'price_logic_min_1986', '1125.6', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (340, 'price_logic_max_1986', '2814', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (341, 'price_fraud_1987', '432', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (342, 'price_logic_min_1987', '691.2', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (343, 'price_logic_max_1987', '1728', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (344, 'price_fraud_1988', '526', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (345, 'price_logic_min_1988', '841.6', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (346, 'price_logic_max_1988', '2104', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (347, 'price_fraud_1989', '421.5', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (348, 'price_logic_min_1989', '674.4', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (349, 'price_logic_max_1989', '1686', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (350, 'price_fraud_1990', '459', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (351, 'price_logic_min_1990', '734.4', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (352, 'price_logic_max_1990', '1836', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (353, 'price_fraud_1991', '460.5', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (354, 'price_logic_min_1991', '736.8', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (355, 'price_logic_max_1991', '1842', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (356, 'price_fraud_1992', '614', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (357, 'price_logic_min_1992', '982.4', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (358, 'price_logic_max_1992', '2456', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (359, 'price_fraud_1993', '699.5', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (360, 'price_logic_min_1993', '1119.2', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (361, 'price_logic_max_1993', '2798', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (362, 'price_fraud_1994', '684', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (363, 'price_logic_min_1994', '1094.4', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (364, 'price_logic_max_1994', '2736', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (365, 'price_fraud_1995', '768', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (366, 'price_logic_min_1995', '1228.8', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (367, 'price_logic_max_1995', '3072', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (368, 'price_fraud_1996', '883.5', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (369, 'price_logic_min_1996', '1413.6', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (370, 'price_logic_max_1996', '3534', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (371, 'price_fraud_1997', '956.5', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (372, 'price_logic_min_1997', '1530.4', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (373, 'price_logic_max_1997', '3826', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (374, 'price_fraud_1998', '1153.5', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (375, 'price_logic_min_1998', '1845.6', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (376, 'price_logic_max_1998', '4614', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (377, 'price_fraud_1999', '1244.5', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (378, 'price_logic_min_1999', '1991.2', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (379, 'price_logic_max_1999', '4978', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (380, 'price_fraud_2000', '1298.5', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (381, 'price_logic_min_2000', '2077.6', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (382, 'price_logic_max_2000', '5194', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (383, 'price_fraud_2001', '1873.5', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (384, 'price_logic_min_2001', '2997.6', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (385, 'price_logic_max_2001', '7494', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (386, 'price_fraud_2002', '1600', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (387, 'price_logic_min_2002', '2560', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (388, 'price_logic_max_2002', '6400', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (389, 'price_fraud_2007', '1150', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (390, 'price_logic_min_2007', '1840', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (391, 'price_logic_max_2007', '4600', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (392, 'price_fraud_1993', '300', 40);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (393, 'price_logic_min_1993', '480', 40);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (394, 'price_logic_max_1993', '1200', 40);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (395, 'price_fraud_1996', '10000', 40);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (396, 'price_logic_min_1996', '16000', 40);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (397, 'price_logic_max_1996', '40000', 40);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (398, 'price_fraud_2001', '1836', 40);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (399, 'price_logic_min_2001', '2937.6', 40);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (400, 'price_logic_max_2001', '7344', 40);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (401, 'price_fraud_2002', '2100', 40);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (402, 'price_logic_min_2002', '3360', 40);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (403, 'price_logic_max_2002', '8400', 40);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (404, 'price_fraud_2004', '3930', 41);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (405, 'price_logic_min_2004', '6288', 41);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (406, 'price_logic_max_2004', '15720', 41);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (407, 'price_fraud_2005', '4316.5', 41);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (408, 'price_logic_min_2005', '6906.4', 41);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (409, 'price_logic_max_2005', '17266', 41);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (410, 'price_fraud_2006', '4622.5', 41);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (411, 'price_logic_min_2006', '7396', 41);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (412, 'price_logic_max_2006', '18490', 41);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (413, 'price_fraud_2007', '5154.5', 41);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (414, 'price_logic_min_2007', '8247.2', 41);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (415, 'price_logic_max_2007', '20618', 41);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (416, 'price_fraud_2008', '6725', 41);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (417, 'price_logic_min_2008', '10760', 41);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (418, 'price_logic_max_2008', '26900', 41);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (419, 'price_fraud_1975', '100', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (420, 'price_logic_min_1975', '160', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (421, 'price_logic_max_1975', '400', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (422, 'price_fraud_1976', '1150', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (423, 'price_logic_min_1976', '1840', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (424, 'price_logic_max_1976', '4600', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (425, 'price_fraud_1977', '220', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (426, 'price_logic_min_1977', '352', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (427, 'price_logic_max_1977', '880', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (428, 'price_fraud_1978', '375', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (429, 'price_logic_min_1978', '600', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (430, 'price_logic_max_1978', '1500', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (431, 'price_fraud_1980', '225', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (432, 'price_logic_min_1980', '360', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (433, 'price_logic_max_1980', '900', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (434, 'price_fraud_1981', '250', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (435, 'price_logic_min_1981', '400', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (436, 'price_logic_max_1981', '1000', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (437, 'price_fraud_1982', '100', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (438, 'price_logic_min_1982', '160', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (439, 'price_logic_max_1982', '400', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (440, 'price_fraud_1983', '166.5', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (441, 'price_logic_min_1983', '266.4', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (442, 'price_logic_max_1983', '666', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (443, 'price_fraud_1984', '184', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (444, 'price_logic_min_1984', '294.4', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (445, 'price_logic_max_1984', '736', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (446, 'price_fraud_1985', '191.5', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (447, 'price_logic_min_1985', '306.4', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (448, 'price_logic_max_1985', '766', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (449, 'price_fraud_1986', '316.5', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (450, 'price_logic_min_1986', '506.4', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (451, 'price_logic_max_1986', '1266', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (452, 'price_fraud_1987', '285', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (453, 'price_logic_min_1987', '456', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (454, 'price_logic_max_1987', '1140', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (455, 'price_fraud_1988', '323', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (456, 'price_logic_min_1988', '516.8', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (457, 'price_logic_max_1988', '1292', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (458, 'price_fraud_1989', '321.5', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (459, 'price_logic_min_1989', '514.4', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (460, 'price_logic_max_1989', '1286', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (461, 'price_fraud_1990', '352.5', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (462, 'price_logic_min_1990', '564', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (463, 'price_logic_max_1990', '1410', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (464, 'price_fraud_1991', '395', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (465, 'price_logic_min_1991', '632', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (466, 'price_logic_max_1991', '1580', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (467, 'price_fraud_1992', '475', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (468, 'price_logic_min_1992', '760', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (469, 'price_logic_max_1992', '1900', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (470, 'price_fraud_1993', '487', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (471, 'price_logic_min_1993', '779.2', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (472, 'price_logic_max_1993', '1948', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (473, 'price_fraud_1994', '549.5', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (474, 'price_logic_min_1994', '879.2', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (475, 'price_logic_max_1994', '2198', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (476, 'price_fraud_1995', '651.5', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (477, 'price_logic_min_1995', '1042.4', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (478, 'price_logic_max_1995', '2606', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (479, 'price_fraud_1996', '829.5', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (480, 'price_logic_min_1996', '1327.2', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (481, 'price_logic_max_1996', '3318', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (482, 'price_fraud_1997', '1048.5', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (483, 'price_logic_min_1997', '1677.6', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (484, 'price_logic_max_1997', '4194', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (485, 'price_fraud_1998', '1168', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (486, 'price_logic_min_1998', '1868.8', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (487, 'price_logic_max_1998', '4672', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (488, 'price_fraud_1999', '1340', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (489, 'price_logic_min_1999', '2144', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (490, 'price_logic_max_1999', '5360', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (491, 'price_fraud_2000', '1591', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (492, 'price_logic_min_2000', '2545.6', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (493, 'price_logic_max_2000', '6364', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (494, 'price_fraud_2001', '1940.5', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (495, 'price_logic_min_2001', '3104.8', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (496, 'price_logic_max_2001', '7762', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (497, 'price_fraud_2002', '2489', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (498, 'price_logic_min_2002', '3982.4', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (499, 'price_logic_max_2002', '9956', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (500, 'price_fraud_2003', '3363', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (501, 'price_logic_min_2003', '5380.8', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (502, 'price_logic_max_2003', '13452', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (503, 'price_fraud_2004', '3318.5', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (504, 'price_logic_min_2004', '5309.6', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (505, 'price_logic_max_2004', '13274', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (506, 'price_fraud_2005', '3736', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (507, 'price_logic_min_2005', '5977.6', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (508, 'price_logic_max_2005', '14944', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (509, 'price_fraud_2006', '4460', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (510, 'price_logic_min_2006', '7136', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (511, 'price_logic_max_2006', '17840', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (512, 'price_fraud_2007', '5120.5', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (513, 'price_logic_min_2007', '8192.8', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (514, 'price_logic_max_2007', '20482', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (515, 'price_fraud_2008', '5896.5', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (516, 'price_logic_min_2008', '9434.4', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (517, 'price_logic_max_2008', '23586', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (518, 'price_fraud_1975', '4529.5', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (519, 'price_logic_min_1975', '7247.2', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (520, 'price_logic_max_1975', '18118', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (521, 'price_fraud_1976', '1860', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (522, 'price_logic_min_1976', '2976', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (523, 'price_logic_max_1976', '7440', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (524, 'price_fraud_1977', '1113.5', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (525, 'price_logic_min_1977', '1781.6', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (526, 'price_logic_max_1977', '4454', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (527, 'price_fraud_1978', '699', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (528, 'price_logic_min_1978', '1118.4', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (529, 'price_logic_max_1978', '2796', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (530, 'price_fraud_1979', '1120.5', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (531, 'price_logic_min_1979', '1792.8', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (532, 'price_logic_max_1979', '4482', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (533, 'price_fraud_1980', '1116.5', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (534, 'price_logic_min_1980', '1786.4', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (535, 'price_logic_max_1980', '4466', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (536, 'price_fraud_1981', '956.5', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (537, 'price_logic_min_1981', '1530.4', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (538, 'price_logic_max_1981', '3826', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (539, 'price_fraud_1982', '561.5', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (540, 'price_logic_min_1982', '898.4', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (541, 'price_logic_max_1982', '2246', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (542, 'price_fraud_1983', '526', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (543, 'price_logic_min_1983', '841.6', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (544, 'price_logic_max_1983', '2104', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (545, 'price_fraud_1984', '483.5', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (546, 'price_logic_min_1984', '773.6', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (547, 'price_logic_max_1984', '1934', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (548, 'price_fraud_1985', '354', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (549, 'price_logic_min_1985', '566.4', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (550, 'price_logic_max_1985', '1416', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (551, 'price_fraud_1986', '473', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (552, 'price_logic_min_1986', '756.8', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (553, 'price_logic_max_1986', '1892', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (554, 'price_fraud_1987', '402.5', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (555, 'price_logic_min_1987', '644', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (556, 'price_logic_max_1987', '1610', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (557, 'price_fraud_1988', '430', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (558, 'price_logic_min_1988', '688', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (559, 'price_logic_max_1988', '1720', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (560, 'price_fraud_1989', '410', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (561, 'price_logic_min_1989', '656', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (562, 'price_logic_max_1989', '1640', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (563, 'price_fraud_1990', '454.5', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (564, 'price_logic_min_1990', '727.2', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (565, 'price_logic_max_1990', '1818', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (566, 'price_fraud_1991', '442.5', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (567, 'price_logic_min_1991', '708', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (568, 'price_logic_max_1991', '1770', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (569, 'price_fraud_1992', '541', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (570, 'price_logic_min_1992', '865.6', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (571, 'price_logic_max_1992', '2164', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (572, 'price_fraud_1993', '645', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (573, 'price_logic_min_1993', '1032', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (574, 'price_logic_max_1993', '2580', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (575, 'price_fraud_1994', '710.5', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (576, 'price_logic_min_1994', '1136.8', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (577, 'price_logic_max_1994', '2842', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (578, 'price_fraud_1995', '869', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (579, 'price_logic_min_1995', '1390.4', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (580, 'price_logic_max_1995', '3476', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (581, 'price_fraud_1996', '1012.5', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (582, 'price_logic_min_1996', '1620', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (583, 'price_logic_max_1996', '4050', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (584, 'price_fraud_1997', '1226.5', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (585, 'price_logic_min_1997', '1962.4', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (586, 'price_logic_max_1997', '4906', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (587, 'price_fraud_1998', '1405', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (588, 'price_logic_min_1998', '2248', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (589, 'price_logic_max_1998', '5620', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (590, 'price_fraud_1999', '1682.5', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (591, 'price_logic_min_1999', '2692', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (592, 'price_logic_max_1999', '6730', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (593, 'price_fraud_2000', '2113.5', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (594, 'price_logic_min_2000', '3381.6', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (595, 'price_logic_max_2000', '8454', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (596, 'price_fraud_2001', '2815.5', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (597, 'price_logic_min_2001', '4504.8', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (598, 'price_logic_max_2001', '11262', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (599, 'price_fraud_2002', '3258', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (600, 'price_logic_min_2002', '5212.8', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (601, 'price_logic_max_2002', '13032', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (602, 'price_fraud_2003', '3888', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (603, 'price_logic_min_2003', '6220.8', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (604, 'price_logic_max_2003', '15552', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (605, 'price_fraud_2004', '4701', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (606, 'price_logic_min_2004', '7521.6', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (607, 'price_logic_max_2004', '18804', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (608, 'price_fraud_2005', '5431', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (609, 'price_logic_min_2005', '8689.6', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (610, 'price_logic_max_2005', '21724', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (611, 'price_fraud_2006', '6420', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (612, 'price_logic_min_2006', '10272', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (613, 'price_logic_max_2006', '25680', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (614, 'price_fraud_2007', '8082', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (615, 'price_logic_min_2007', '12931.2', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (616, 'price_logic_max_2007', '32328', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (617, 'price_fraud_2008', '9657', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (618, 'price_logic_min_2008', '15451.2', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (619, 'price_logic_max_2008', '38628', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (620, 'price_fraud_1998', '1700', 42);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (621, 'price_logic_min_1998', '2720', 42);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (622, 'price_logic_max_1998', '6800', 42);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (623, 'price_fraud_1999', '1550', 42);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (624, 'price_logic_min_1999', '2480', 42);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (625, 'price_logic_max_1999', '6200', 42);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (626, 'price_fraud_2001', '3800', 42);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (627, 'price_logic_min_2001', '6080', 42);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (628, 'price_logic_max_2001', '15200', 42);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (629, 'price_fraud_2002', '2972.5', 42);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (630, 'price_logic_min_2002', '4756', 42);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (631, 'price_logic_max_2002', '11890', 42);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (632, 'price_fraud_1996', '1250', 43);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (633, 'price_logic_min_1996', '2000', 43);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (634, 'price_logic_max_1996', '5000', 43);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (635, 'price_fraud_1998', '850', 43);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (636, 'price_logic_min_1998', '1360', 43);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (637, 'price_logic_max_1998', '3400', 43);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (638, 'price_fraud_1999', '1575', 43);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (639, 'price_logic_min_1999', '2520', 43);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (640, 'price_logic_max_1999', '6300', 43);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (641, 'price_fraud_2000', '1100', 43);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (642, 'price_logic_min_2000', '1760', 43);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (643, 'price_logic_max_2000', '4400', 43);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (644, 'price_fraud_1994', '831', 44);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (645, 'price_logic_min_1994', '1329.6', 44);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (646, 'price_logic_max_1994', '3324', 44);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (647, 'price_fraud_1995', '775', 44);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (648, 'price_logic_min_1995', '1240', 44);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (649, 'price_logic_max_1995', '3100', 44);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (650, 'price_fraud_1996', '1387.5', 44);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (651, 'price_logic_min_1996', '2220', 44);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (652, 'price_logic_max_1996', '5550', 44);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (653, 'price_fraud_1997', '1400', 44);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (654, 'price_logic_min_1997', '2240', 44);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (655, 'price_logic_max_1997', '5600', 44);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (656, 'price_fraud_1998', '1852.5', 45);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (657, 'price_logic_min_1998', '2964', 45);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (658, 'price_logic_max_1998', '7410', 45);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (659, 'price_fraud_1999', '1749.5', 45);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (660, 'price_logic_min_1999', '2799.2', 45);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (661, 'price_logic_max_1999', '6998', 45);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (662, 'price_fraud_2000', '2148.5', 45);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (663, 'price_logic_min_2000', '3437.6', 45);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (664, 'price_logic_max_2000', '8594', 45);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (665, 'price_fraud_2001', '2000', 45);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (666, 'price_logic_min_2001', '3200', 45);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (667, 'price_logic_max_2001', '8000', 45);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (668, 'price_fraud_1995', '1625', 46);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (669, 'price_logic_min_1995', '2600', 46);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (670, 'price_logic_max_1995', '6500', 46);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (671, 'price_fraud_1996', '1440', 46);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (672, 'price_logic_min_1996', '2304', 46);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (673, 'price_logic_max_1996', '5760', 46);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (674, 'price_fraud_1997', '1275', 46);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (675, 'price_logic_min_1997', '2040', 46);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (676, 'price_logic_max_1997', '5100', 46);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (677, 'price_fraud_1998', '1400', 46);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (678, 'price_logic_min_1998', '2240', 46);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (679, 'price_logic_max_1998', '5600', 46);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (680, 'price_fraud_2004', '4250', 46);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (681, 'price_logic_min_2004', '6800', 46);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (682, 'price_logic_max_2004', '17000', 46);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (683, 'price_fraud_1995', '1441.5', 47);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (684, 'price_logic_min_1995', '2306.4', 47);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (685, 'price_logic_max_1995', '5766', 47);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (686, 'price_fraud_1996', '1229', 47);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (687, 'price_logic_min_1996', '1966.4', 47);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (688, 'price_logic_max_1996', '4916', 47);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (689, 'price_fraud_1997', '1750', 47);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (690, 'price_logic_min_1997', '2800', 47);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (691, 'price_logic_max_1997', '7000', 47);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (692, 'price_fraud_1999', '2325', 47);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (693, 'price_logic_min_1999', '3720', 47);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (694, 'price_logic_max_1999', '9300', 47);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (695, 'price_fraud_2000', '1600', 47);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (696, 'price_logic_min_2000', '2560', 47);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (697, 'price_logic_max_2000', '6400', 47);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (698, 'price_fraud_2002', '3325', 48);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (699, 'price_logic_min_2002', '5320', 48);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (700, 'price_logic_max_2002', '13300', 48);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (701, 'price_fraud_2003', '4100', 48);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (702, 'price_logic_min_2003', '6560', 48);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (703, 'price_logic_max_2003', '16400', 48);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (704, 'price_fraud_2004', '3625', 48);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (705, 'price_logic_min_2004', '5800', 48);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (706, 'price_logic_max_2004', '14500', 48);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (707, 'price_fraud_2005', '4800', 48);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (708, 'price_logic_min_2005', '7680', 48);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (709, 'price_logic_max_2005', '19200', 48);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (710, 'price_fraud_2006', '5000', 48);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (711, 'price_logic_min_2006', '8000', 48);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (712, 'price_logic_max_2006', '20000', 48);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (713, 'price_fraud_2003', '5175', 49);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (714, 'price_logic_min_2003', '8280', 49);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (715, 'price_logic_max_2003', '20700', 49);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (716, 'price_fraud_2004', '5475', 49);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (717, 'price_logic_min_2004', '8760', 49);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (718, 'price_logic_max_2004', '21900', 49);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (719, 'price_fraud_2005', '6467.5', 49);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (720, 'price_logic_min_2005', '10348', 49);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (721, 'price_logic_max_2005', '25870', 49);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (722, 'price_fraud_2006', '8950', 49);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (723, 'price_logic_min_2006', '14320', 49);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (724, 'price_logic_max_2006', '35800', 49);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (725, 'price_fraud_2001', '3400', 50);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (726, 'price_logic_min_2001', '5440', 50);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (727, 'price_logic_max_2001', '13600', 50);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (728, 'price_fraud_2002', '3445', 50);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (729, 'price_logic_min_2002', '5512', 50);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (730, 'price_logic_max_2002', '13780', 50);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (731, 'price_fraud_2004', '4950', 50);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (732, 'price_logic_min_2004', '7920', 50);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (733, 'price_logic_max_2004', '19800', 50);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (734, 'price_fraud_2005', '4950', 50);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (735, 'price_logic_min_2005', '7920', 50);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (736, 'price_logic_max_2005', '19800', 50);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (737, 'price_fraud_1984', '1750', 32);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (738, 'price_logic_min_1984', '2800', 32);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (739, 'price_logic_max_1984', '7000', 32);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (740, 'price_fraud_1988', '1750', 32);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (741, 'price_logic_min_1988', '2800', 32);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (742, 'price_logic_max_1988', '7000', 32);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (743, 'price_fraud_1989', '1650', 32);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (744, 'price_logic_min_1989', '2640', 32);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (745, 'price_logic_max_1989', '6600', 32);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (746, 'price_fraud_1991', '2733', 32);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (747, 'price_logic_min_1991', '4372.8', 32);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (748, 'price_logic_max_1991', '10932', 32);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (749, 'price_fraud_1992', '1000', 32);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (750, 'price_logic_min_1992', '1600', 32);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (751, 'price_logic_max_1992', '4000', 32);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (752, 'price_fraud_1993', '1625', 32);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (753, 'price_logic_min_1993', '2600', 32);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (754, 'price_logic_max_1993', '6500', 32);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (755, 'price_fraud_1994', '828', 32);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (756, 'price_logic_min_1994', '1324.8', 32);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (757, 'price_logic_max_1994', '3312', 32);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (758, 'price_fraud_1995', '1057.5', 32);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (759, 'price_logic_min_1995', '1692', 32);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (760, 'price_logic_max_1995', '4230', 32);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (761, 'price_fraud_1996', '1136.5', 32);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (762, 'price_logic_min_1996', '1818.4', 32);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (763, 'price_logic_max_1996', '4546', 32);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (764, 'price_fraud_1997', '1295.5', 32);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (765, 'price_logic_min_1997', '2072.8', 32);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (766, 'price_logic_max_1997', '5182', 32);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (767, 'price_fraud_1998', '1616', 32);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (768, 'price_logic_min_1998', '2585.6', 32);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (769, 'price_logic_max_1998', '6464', 32);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (770, 'price_fraud_1999', '1857.5', 32);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (771, 'price_logic_min_1999', '2972', 32);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (772, 'price_logic_max_1999', '7430', 32);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (773, 'price_fraud_2000', '2170', 32);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (774, 'price_logic_min_2000', '3472', 32);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (775, 'price_logic_max_2000', '8680', 32);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (776, 'price_fraud_2001', '3146', 32);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (777, 'price_logic_min_2001', '5033.6', 32);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (778, 'price_logic_max_2001', '12584', 32);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (779, 'price_fraud_2002', '3565.5', 32);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (780, 'price_logic_min_2002', '5704.8', 32);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (781, 'price_logic_max_2002', '14262', 32);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (782, 'price_fraud_2003', '4029', 32);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (783, 'price_logic_min_2003', '6446.4', 32);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (784, 'price_logic_max_2003', '16116', 32);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (785, 'price_fraud_2004', '4702.5', 32);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (786, 'price_logic_min_2004', '7524', 32);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (787, 'price_logic_max_2004', '18810', 32);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (788, 'price_fraud_2005', '5904.5', 32);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (789, 'price_logic_min_2005', '9447.2', 32);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (790, 'price_logic_max_2005', '23618', 32);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (791, 'price_fraud_2006', '7192.5', 32);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (792, 'price_logic_min_2006', '11508', 32);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (793, 'price_logic_max_2006', '28770', 32);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (794, 'price_fraud_2007', '9243', 32);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (795, 'price_logic_min_2007', '14788.8', 32);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (796, 'price_logic_max_2007', '36972', 32);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (797, 'price_fraud_2008', '11569.5', 32);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (798, 'price_logic_min_2008', '18511.2', 32);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (799, 'price_logic_max_2008', '46278', 32);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (800, 'price_fraud_2000', '3866.5', 57);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (801, 'price_logic_min_2000', '6186.4', 57);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (802, 'price_logic_max_2000', '15466', 57);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (803, 'price_fraud_2001', '3982', 57);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (804, 'price_logic_min_2001', '6371.2', 57);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (805, 'price_logic_max_2001', '15928', 57);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (806, 'price_fraud_2002', '4284', 57);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (807, 'price_logic_min_2002', '6854.4', 57);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (808, 'price_logic_max_2002', '17136', 57);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (809, 'price_fraud_2003', '6075', 57);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (810, 'price_logic_min_2003', '9720', 57);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (811, 'price_logic_max_2003', '24300', 57);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (812, 'price_fraud_2007', '7950', 57);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (813, 'price_logic_min_2007', '12720', 57);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (814, 'price_logic_max_2007', '31800', 57);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (815, 'price_fraud_1997', '1533', 58);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (816, 'price_logic_min_1997', '2452.8', 58);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (817, 'price_logic_max_1997', '6132', 58);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (818, 'price_fraud_1998', '2033', 58);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (819, 'price_logic_min_1998', '3252.8', 58);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (820, 'price_logic_max_1998', '8132', 58);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (821, 'price_fraud_1999', '2248', 58);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (822, 'price_logic_min_1999', '3596.8', 58);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (823, 'price_logic_max_1999', '8992', 58);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (824, 'price_fraud_2000', '2313', 58);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (825, 'price_logic_min_2000', '3700.8', 58);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (826, 'price_logic_max_2000', '9252', 58);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (827, 'price_fraud_2001', '2300', 58);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (828, 'price_logic_min_2001', '3680', 58);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (829, 'price_logic_max_2001', '9200', 58);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (830, 'price_fraud_1997', '1816.5', 59);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (831, 'price_logic_min_1997', '2906.4', 59);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (832, 'price_logic_max_1997', '7266', 59);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (833, 'price_fraud_1998', '2100', 59);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (834, 'price_logic_min_1998', '3360', 59);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (835, 'price_logic_max_1998', '8400', 59);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (836, 'price_fraud_2001', '3450', 59);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (837, 'price_logic_min_2001', '5520', 59);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (838, 'price_logic_max_2001', '13800', 59);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (839, 'price_fraud_2007', '9125', 59);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (840, 'price_logic_min_2007', '14600', 59);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (841, 'price_logic_max_2007', '36500', 59);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (842, 'price_fraud_1988', '1500', 34);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (843, 'price_logic_min_1988', '2400', 34);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (844, 'price_logic_max_1988', '6000', 34);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (845, 'price_fraud_1994', '5000', 34);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (846, 'price_logic_min_1994', '8000', 34);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (847, 'price_logic_max_1994', '20000', 34);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (848, 'price_fraud_1995', '1600', 34);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (849, 'price_logic_min_1995', '2560', 34);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (850, 'price_logic_max_1995', '6400', 34);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (851, 'price_fraud_1996', '1641.5', 34);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (852, 'price_logic_min_1996', '2626.4', 34);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (853, 'price_logic_max_1996', '6566', 34);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (854, 'price_fraud_1997', '1526', 34);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (855, 'price_logic_min_1997', '2441.6', 34);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (856, 'price_logic_max_1997', '6104', 34);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (857, 'price_fraud_1998', '1791', 34);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (858, 'price_logic_min_1998', '2865.6', 34);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (859, 'price_logic_max_1998', '7164', 34);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (860, 'price_fraud_1999', '2001.5', 34);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (861, 'price_logic_min_1999', '3202.4', 34);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (862, 'price_logic_max_1999', '8006', 34);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (863, 'price_fraud_2000', '2689', 34);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (864, 'price_logic_min_2000', '4302.4', 34);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (865, 'price_logic_max_2000', '10756', 34);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (866, 'price_fraud_2001', '3090.5', 34);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (867, 'price_logic_min_2001', '4944.8', 34);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (868, 'price_logic_max_2001', '12362', 34);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (869, 'price_fraud_2002', '3618', 34);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (870, 'price_logic_min_2002', '5788.8', 34);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (871, 'price_logic_max_2002', '14472', 34);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (872, 'price_fraud_2003', '4496.5', 34);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (873, 'price_logic_min_2003', '7194.4', 34);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (874, 'price_logic_max_2003', '17986', 34);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (875, 'price_fraud_2004', '5294', 34);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (876, 'price_logic_min_2004', '8470.4', 34);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (877, 'price_logic_max_2004', '21176', 34);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (878, 'price_fraud_2005', '6196.5', 34);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (879, 'price_logic_min_2005', '9914.4', 34);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (880, 'price_logic_max_2005', '24786', 34);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (881, 'price_fraud_2006', '7274.5', 34);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (882, 'price_logic_min_2006', '11639.2', 34);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (883, 'price_logic_max_2006', '29098', 34);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (884, 'price_fraud_2007', '8958', 34);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (885, 'price_logic_min_2007', '14332.8', 34);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (886, 'price_logic_max_2007', '35832', 34);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (887, 'price_fraud_2008', '9717.5', 34);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (888, 'price_logic_min_2008', '15548', 34);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (889, 'price_logic_max_2008', '38870', 34);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (890, 'price_fraud_1975', '1040', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (891, 'price_logic_min_1975', '1664', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (892, 'price_logic_max_1975', '4160', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (893, 'price_fraud_1976', '552', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (894, 'price_logic_min_1976', '883.2', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (895, 'price_logic_max_1976', '2208', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (896, 'price_fraud_1977', '434.5', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (897, 'price_logic_min_1977', '695.2', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (898, 'price_logic_max_1977', '1738', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (899, 'price_fraud_1978', '508', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (900, 'price_logic_min_1978', '812.8', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (901, 'price_logic_max_1978', '2032', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (902, 'price_fraud_1979', '506.5', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (903, 'price_logic_min_1979', '810.4', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (904, 'price_logic_max_1979', '2026', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (905, 'price_fraud_1980', '585.5', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (906, 'price_logic_min_1980', '936.8', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (907, 'price_logic_max_1980', '2342', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (908, 'price_fraud_1981', '425', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (909, 'price_logic_min_1981', '680', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (910, 'price_logic_max_1981', '1700', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (911, 'price_fraud_1982', '415', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (912, 'price_logic_min_1982', '664', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (913, 'price_logic_max_1982', '1660', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (914, 'price_fraud_1983', '531.5', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (915, 'price_logic_min_1983', '850.4', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (916, 'price_logic_max_1983', '2126', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (917, 'price_fraud_1984', '463', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (918, 'price_logic_min_1984', '740.8', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (919, 'price_logic_max_1984', '1852', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (920, 'price_fraud_1985', '393.5', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (921, 'price_logic_min_1985', '629.6', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (922, 'price_logic_max_1985', '1574', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (923, 'price_fraud_1986', '383', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (924, 'price_logic_min_1986', '612.8', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (925, 'price_logic_max_1986', '1532', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (926, 'price_fraud_1987', '413', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (927, 'price_logic_min_1987', '660.8', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (928, 'price_logic_max_1987', '1652', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (929, 'price_fraud_1988', '413.5', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (930, 'price_logic_min_1988', '661.6', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (931, 'price_logic_max_1988', '1654', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (932, 'price_fraud_1989', '408.5', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (933, 'price_logic_min_1989', '653.6', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (934, 'price_logic_max_1989', '1634', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (935, 'price_fraud_1990', '472', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (936, 'price_logic_min_1990', '755.2', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (937, 'price_logic_max_1990', '1888', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (938, 'price_fraud_1991', '550.5', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (939, 'price_logic_min_1991', '880.8', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (940, 'price_logic_max_1991', '2202', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (941, 'price_fraud_1992', '628.5', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (942, 'price_logic_min_1992', '1005.6', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (943, 'price_logic_max_1992', '2514', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (944, 'price_fraud_1993', '751.5', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (945, 'price_logic_min_1993', '1202.4', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (946, 'price_logic_max_1993', '3006', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (947, 'price_fraud_1994', '886', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (948, 'price_logic_min_1994', '1417.6', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (949, 'price_logic_max_1994', '3544', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (950, 'price_fraud_1995', '1025', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (951, 'price_logic_min_1995', '1640', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (952, 'price_logic_max_1995', '4100', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (953, 'price_fraud_1996', '1200.5', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (954, 'price_logic_min_1996', '1920.8', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (955, 'price_logic_max_1996', '4802', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (956, 'price_fraud_1997', '1526.5', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (957, 'price_logic_min_1997', '2442.4', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (958, 'price_logic_max_1997', '6106', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (959, 'price_fraud_1998', '1923.5', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (960, 'price_logic_min_1998', '3077.6', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (961, 'price_logic_max_1998', '7694', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (962, 'price_fraud_1999', '2253.5', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (963, 'price_logic_min_1999', '3605.6', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (964, 'price_logic_max_1999', '9014', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (965, 'price_fraud_2000', '2633.5', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (966, 'price_logic_min_2000', '4213.6', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (967, 'price_logic_max_2000', '10534', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (968, 'price_fraud_2001', '3158', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (969, 'price_logic_min_2001', '5052.8', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (970, 'price_logic_max_2001', '12632', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (971, 'price_fraud_2002', '3611', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (972, 'price_logic_min_2002', '5777.6', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (973, 'price_logic_max_2002', '14444', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (974, 'price_fraud_2003', '4475', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (975, 'price_logic_min_2003', '7160', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (976, 'price_logic_max_2003', '17900', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (977, 'price_fraud_2004', '5082.5', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (978, 'price_logic_min_2004', '8132', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (979, 'price_logic_max_2004', '20330', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (980, 'price_fraud_2005', '5637', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (981, 'price_logic_min_2005', '9019.2', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (982, 'price_logic_max_2005', '22548', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (983, 'price_fraud_2006', '6450', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (984, 'price_logic_min_2006', '10320', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (985, 'price_logic_max_2006', '25800', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (986, 'price_fraud_2007', '7576', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (987, 'price_logic_min_2007', '12121.6', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (988, 'price_logic_max_2007', '30304', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (989, 'price_fraud_2008', '8842.5', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (990, 'price_logic_min_2008', '14148', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (991, 'price_logic_max_2008', '35370', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (992, 'price_fraud_1987', '750', 35);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (993, 'price_logic_min_1987', '1200', 35);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (994, 'price_logic_max_1987', '3000', 35);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (995, 'price_fraud_1989', '750', 35);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (996, 'price_logic_min_1989', '1200', 35);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (997, 'price_logic_max_1989', '3000', 35);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (998, 'price_fraud_1991', '350', 35);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (999, 'price_logic_min_1991', '560', 35);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1000, 'price_logic_max_1991', '1400', 35);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1001, 'price_fraud_1993', '775', 35);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1002, 'price_logic_min_1993', '1240', 35);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1003, 'price_logic_max_1993', '3100', 35);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1004, 'price_fraud_1994', '856', 35);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1005, 'price_logic_min_1994', '1369.6', 35);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1006, 'price_logic_max_1994', '3424', 35);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1007, 'price_fraud_1995', '699', 35);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1008, 'price_logic_min_1995', '1118.4', 35);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1009, 'price_logic_max_1995', '2796', 35);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1010, 'price_fraud_1996', '825', 35);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1011, 'price_logic_min_1996', '1320', 35);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1012, 'price_logic_max_1996', '3300', 35);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1013, 'price_fraud_1997', '1232.5', 35);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1014, 'price_logic_min_1997', '1972', 35);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1015, 'price_logic_max_1997', '4930', 35);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1016, 'price_fraud_1998', '1326', 35);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1017, 'price_logic_min_1998', '2121.6', 35);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1018, 'price_logic_max_1998', '5304', 35);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1019, 'price_fraud_1999', '1411', 35);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1020, 'price_logic_min_1999', '2257.6', 35);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1021, 'price_logic_max_1999', '5644', 35);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1022, 'price_fraud_2000', '2046', 35);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1023, 'price_logic_min_2000', '3273.6', 35);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1024, 'price_logic_max_2000', '8184', 35);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1025, 'price_fraud_2001', '2396', 35);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1026, 'price_logic_min_2001', '3833.6', 35);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1027, 'price_logic_max_2001', '9584', 35);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1028, 'price_fraud_2002', '2957.5', 35);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1029, 'price_logic_min_2002', '4732', 35);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1030, 'price_logic_max_2002', '11830', 35);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1031, 'price_fraud_2003', '3562', 35);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1032, 'price_logic_min_2003', '5699.2', 35);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1033, 'price_logic_max_2003', '14248', 35);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1034, 'price_fraud_2004', '3935', 35);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1035, 'price_logic_min_2004', '6296', 35);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1036, 'price_logic_max_2004', '15740', 35);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1037, 'price_fraud_2005', '4332', 35);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1038, 'price_logic_min_2005', '6931.2', 35);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1039, 'price_logic_max_2005', '17328', 35);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1040, 'price_fraud_2006', '4738.5', 35);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1041, 'price_logic_min_2006', '7581.6', 35);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1042, 'price_logic_max_2006', '18954', 35);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1043, 'price_fraud_2007', '5379', 35);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1044, 'price_logic_min_2007', '8606.4', 35);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1045, 'price_logic_max_2007', '21516', 35);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1046, 'price_fraud_2008', '5580.5', 35);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1047, 'price_logic_min_2008', '8928.8', 35);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1048, 'price_logic_max_2008', '22322', 35);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1049, 'price_fraud_2002', '3400', 51);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1050, 'price_logic_min_2002', '5440', 51);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1051, 'price_logic_max_2002', '13600', 51);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1052, 'price_fraud_2003', '3200', 51);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1053, 'price_logic_min_2003', '5120', 51);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1054, 'price_logic_max_2003', '12800', 51);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1055, 'price_fraud_2004', '3250', 51);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1056, 'price_logic_min_2004', '5200', 51);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1057, 'price_logic_max_2004', '13000', 51);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1058, 'price_fraud_2005', '3350', 51);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1059, 'price_logic_min_2005', '5360', 51);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1060, 'price_logic_max_2005', '13400', 51);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1061, 'price_fraud_1984', '1500', 36);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1062, 'price_logic_min_1984', '2400', 36);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1063, 'price_logic_max_1984', '6000', 36);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1064, 'price_fraud_1985', '300', 36);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1065, 'price_logic_min_1985', '480', 36);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1066, 'price_logic_max_1985', '1200', 36);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1067, 'price_fraud_1986', '250', 36);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1068, 'price_logic_min_1986', '400', 36);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1069, 'price_logic_max_1986', '1000', 36);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1070, 'price_fraud_1987', '418.5', 36);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1071, 'price_logic_min_1987', '669.6', 36);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1072, 'price_logic_max_1987', '1674', 36);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1073, 'price_fraud_1988', '246.5', 36);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1074, 'price_logic_min_1988', '394.4', 36);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1075, 'price_logic_max_1988', '986', 36);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1076, 'price_fraud_1989', '217.5', 36);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1077, 'price_logic_min_1989', '348', 36);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1078, 'price_logic_max_1989', '870', 36);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1079, 'price_fraud_1990', '385', 36);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1080, 'price_logic_min_1990', '616', 36);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1081, 'price_logic_max_1990', '1540', 36);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1082, 'price_fraud_1991', '365.5', 36);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1083, 'price_logic_min_1991', '584.8', 36);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1084, 'price_logic_max_1991', '1462', 36);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1085, 'price_fraud_1992', '418.5', 36);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1086, 'price_logic_min_1992', '669.6', 36);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1087, 'price_logic_max_1992', '1674', 36);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1088, 'price_fraud_1993', '419', 36);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1089, 'price_logic_min_1993', '670.4', 36);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1090, 'price_logic_max_1993', '1676', 36);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1091, 'price_fraud_1994', '684.5', 36);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1092, 'price_logic_min_1994', '1095.2', 36);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1093, 'price_logic_max_1994', '2738', 36);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1094, 'price_fraud_1995', '826.5', 36);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1095, 'price_logic_min_1995', '1322.4', 36);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1096, 'price_logic_max_1995', '3306', 36);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1097, 'price_fraud_1996', '813.5', 36);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1098, 'price_logic_min_1996', '1301.6', 36);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1099, 'price_logic_max_1996', '3254', 36);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1100, 'price_fraud_1997', '1050.5', 36);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1101, 'price_logic_min_1997', '1680.8', 36);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1102, 'price_logic_max_1997', '4202', 36);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1103, 'price_fraud_1998', '1403', 36);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1104, 'price_logic_min_1998', '2244.8', 36);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1105, 'price_logic_max_1998', '5612', 36);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1106, 'price_fraud_1999', '1567', 36);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1107, 'price_logic_min_1999', '2507.2', 36);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1108, 'price_logic_max_1999', '6268', 36);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1109, 'price_fraud_2000', '2024.5', 36);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1110, 'price_logic_min_2000', '3239.2', 36);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1111, 'price_logic_max_2000', '8098', 36);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1112, 'price_fraud_2001', '2379', 36);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1113, 'price_logic_min_2001', '3806.4', 36);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1114, 'price_logic_max_2001', '9516', 36);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1115, 'price_fraud_2002', '3110.5', 36);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1116, 'price_logic_min_2002', '4976.8', 36);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1117, 'price_logic_max_2002', '12442', 36);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1118, 'price_fraud_2003', '3624', 36);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1119, 'price_logic_min_2003', '5798.4', 36);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1120, 'price_logic_max_2003', '14496', 36);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1121, 'price_fraud_2004', '3941', 36);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1122, 'price_logic_min_2004', '6305.6', 36);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1123, 'price_logic_max_2004', '15764', 36);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1124, 'price_fraud_2005', '4407', 36);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1125, 'price_logic_min_2005', '7051.2', 36);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1126, 'price_logic_max_2005', '17628', 36);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1127, 'price_fraud_2006', '5228', 36);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1128, 'price_logic_min_2006', '8364.8', 36);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1129, 'price_logic_max_2006', '20912', 36);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1130, 'price_fraud_2007', '6039', 36);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1131, 'price_logic_min_2007', '9662.4', 36);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1132, 'price_logic_max_2007', '24156', 36);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1133, 'price_fraud_2008', '7001', 36);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1134, 'price_logic_min_2008', '11201.6', 36);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1135, 'price_logic_max_2008', '28004', 36);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1136, 'price_fraud_2005', '5950', 52);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1137, 'price_logic_min_2005', '9520', 52);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1138, 'price_logic_max_2005', '23800', 52);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1139, 'price_fraud_2006', '6500', 52);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1140, 'price_logic_min_2006', '10400', 52);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1141, 'price_logic_max_2006', '26000', 52);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1142, 'price_fraud_2007', '7203', 52);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1143, 'price_logic_min_2007', '11524.8', 52);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1144, 'price_logic_max_2007', '28812', 52);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1145, 'price_fraud_2008', '9750', 52);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1146, 'price_logic_min_2008', '15600', 52);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1147, 'price_logic_max_2008', '39000', 52);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1148, 'price_fraud_2005', '6987.5', 53);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1149, 'price_logic_min_2005', '11180', 53);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1150, 'price_logic_max_2005', '27950', 53);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1151, 'price_fraud_2006', '7982.5', 53);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1152, 'price_logic_min_2006', '12772', 53);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1153, 'price_logic_max_2006', '31930', 53);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1154, 'price_fraud_2007', '8383.5', 53);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1155, 'price_logic_min_2007', '13413.6', 53);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1156, 'price_logic_max_2007', '33534', 53);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1157, 'price_fraud_2008', '10250', 53);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1158, 'price_logic_min_2008', '16400', 53);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1159, 'price_logic_max_2008', '41000', 53);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1160, 'price_fraud_2000', '2980', 54);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1161, 'price_logic_min_2000', '4768', 54);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1162, 'price_logic_max_2000', '11920', 54);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1163, 'price_fraud_2001', '3241.5', 54);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1164, 'price_logic_min_2001', '5186.4', 54);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1165, 'price_logic_max_2001', '12966', 54);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1166, 'price_fraud_2002', '3950', 54);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1167, 'price_logic_min_2002', '6320', 54);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1168, 'price_logic_max_2002', '15800', 54);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1169, 'price_fraud_2003', '4100', 54);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1170, 'price_logic_min_2003', '6560', 54);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1171, 'price_logic_max_2003', '16400', 54);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1172, 'price_fraud_2004', '5033', 54);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1173, 'price_logic_min_2004', '8052.8', 54);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1174, 'price_logic_max_2004', '20132', 54);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1175, 'price_fraud_2002', '5250', 55);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1176, 'price_logic_min_2002', '8400', 55);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1177, 'price_logic_max_2002', '21000', 55);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1178, 'price_fraud_2003', '5750', 55);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1179, 'price_logic_min_2003', '9200', 55);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1180, 'price_logic_max_2003', '23000', 55);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1181, 'price_fraud_2004', '7014', 55);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1182, 'price_logic_min_2004', '11222.4', 55);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1183, 'price_logic_max_2004', '28056', 55);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1184, 'price_fraud_2005', '7406', 55);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1185, 'price_logic_min_2005', '11849.6', 55);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1186, 'price_logic_max_2005', '29624', 55);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1187, 'price_fraud_1999', '2250', 37);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1188, 'price_logic_min_1999', '3600', 37);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1189, 'price_logic_max_1999', '9000', 37);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1190, 'price_fraud_2000', '3055.5', 37);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1191, 'price_logic_min_2000', '4888.8', 37);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1192, 'price_logic_max_2000', '12222', 37);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1193, 'price_fraud_2001', '3446', 37);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1194, 'price_logic_min_2001', '5513.6', 37);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1195, 'price_logic_max_2001', '13784', 37);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1196, 'price_fraud_2002', '4447.5', 37);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1197, 'price_logic_min_2002', '7116', 37);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1198, 'price_logic_max_2002', '17790', 37);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1199, 'price_fraud_2003', '5235.5', 37);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1200, 'price_logic_min_2003', '8376.8', 37);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1201, 'price_logic_max_2003', '20942', 37);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1202, 'price_fraud_2004', '5725.5', 37);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1203, 'price_logic_min_2004', '9160.8', 37);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1204, 'price_logic_max_2004', '22902', 37);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1205, 'price_fraud_2005', '6721', 37);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1206, 'price_logic_min_2005', '10753.6', 37);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1207, 'price_logic_max_2005', '26884', 37);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1208, 'price_fraud_2006', '7476.5', 37);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1209, 'price_logic_min_2006', '11962.4', 37);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1210, 'price_logic_max_2006', '29906', 37);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1211, 'price_fraud_2007', '8677', 37);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1212, 'price_logic_min_2007', '13883.2', 37);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1213, 'price_logic_max_2007', '34708', 37);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1214, 'price_fraud_2008', '10337.5', 37);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1215, 'price_logic_min_2008', '16540', 37);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1216, 'price_logic_max_2008', '41350', 37);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1217, 'price_fraud_1983', '300', 29);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1218, 'price_logic_min_1983', '480', 29);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1219, 'price_logic_max_1983', '1200', 29);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1220, 'price_fraud_1984', '1500', 29);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1221, 'price_logic_min_1984', '2400', 29);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1222, 'price_logic_max_1984', '6000', 29);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1223, 'price_fraud_1985', '300', 29);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1224, 'price_logic_min_1985', '480', 29);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1225, 'price_logic_max_1985', '1200', 29);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1226, 'price_fraud_1986', '290', 29);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1227, 'price_logic_min_1986', '464', 29);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1228, 'price_logic_max_1986', '1160', 29);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1229, 'price_fraud_1987', '458', 29);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1230, 'price_logic_min_1987', '732.8', 29);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1231, 'price_logic_max_1987', '1832', 29);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1232, 'price_fraud_1988', '301.5', 29);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1233, 'price_logic_min_1988', '482.4', 29);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1234, 'price_logic_max_1988', '1206', 29);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1235, 'price_fraud_1989', '281', 29);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1236, 'price_logic_min_1989', '449.6', 29);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1237, 'price_logic_max_1989', '1124', 29);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1238, 'price_fraud_1990', '345', 29);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1239, 'price_logic_min_1990', '552', 29);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1240, 'price_logic_max_1990', '1380', 29);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1241, 'price_fraud_1991', '339', 29);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1242, 'price_logic_min_1991', '542.4', 29);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1243, 'price_logic_max_1991', '1356', 29);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1244, 'price_fraud_1992', '478', 29);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1245, 'price_logic_min_1992', '764.8', 29);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1246, 'price_logic_max_1992', '1912', 29);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1247, 'price_fraud_1993', '470', 29);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1248, 'price_logic_min_1993', '752', 29);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1249, 'price_logic_max_1993', '1880', 29);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1250, 'price_fraud_1994', '692.5', 29);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1251, 'price_logic_min_1994', '1108', 29);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1252, 'price_logic_max_1994', '2770', 29);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1253, 'price_fraud_1995', '758.5', 29);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1254, 'price_logic_min_1995', '1213.6', 29);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1255, 'price_logic_max_1995', '3034', 29);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1256, 'price_fraud_1996', '837', 29);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1257, 'price_logic_min_1996', '1339.2', 29);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1258, 'price_logic_max_1996', '3348', 29);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1259, 'price_fraud_1997', '1160', 29);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1260, 'price_logic_min_1997', '1856', 29);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1261, 'price_logic_max_1997', '4640', 29);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1262, 'price_fraud_1998', '1414.5', 29);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1263, 'price_logic_min_1998', '2263.2', 29);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1264, 'price_logic_max_1998', '5658', 29);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1265, 'price_fraud_1999', '1672.5', 29);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1266, 'price_logic_min_1999', '2676', 29);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1267, 'price_logic_max_1999', '6690', 29);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1268, 'price_fraud_2000', '2244.5', 29);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1269, 'price_logic_min_2000', '3591.2', 29);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1270, 'price_logic_max_2000', '8978', 29);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1271, 'price_fraud_2001', '2835', 29);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1272, 'price_logic_min_2001', '4536', 29);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1273, 'price_logic_max_2001', '11340', 29);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1274, 'price_fraud_2002', '3760', 29);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1275, 'price_logic_min_2002', '6016', 29);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1276, 'price_logic_max_2002', '15040', 29);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1277, 'price_fraud_2003', '4309.5', 29);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1278, 'price_logic_min_2003', '6895.2', 29);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1279, 'price_logic_max_2003', '17238', 29);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1280, 'price_fraud_2004', '4872.5', 29);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1281, 'price_logic_min_2004', '7796', 29);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1282, 'price_logic_max_2004', '19490', 29);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1283, 'price_fraud_2005', '5702.5', 29);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1284, 'price_logic_min_2005', '9124', 29);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1285, 'price_logic_max_2005', '22810', 29);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1286, 'price_fraud_2006', '6530', 29);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1287, 'price_logic_min_2006', '10448', 29);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1288, 'price_logic_max_2006', '26120', 29);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1289, 'price_fraud_2007', '7412.5', 29);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1290, 'price_logic_min_2007', '11860', 29);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1291, 'price_logic_max_2007', '29650', 29);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1292, 'price_fraud_2008', '8648', 29);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1293, 'price_logic_min_2008', '13836.8', 29);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1294, 'price_logic_max_2008', '34592', 29);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (10001, 'name', 'rhone alpes', 2001);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (10002, 'name', 'ain', 2002);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (10003, 'name', 'ile de france', 2006);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (10004, 'name', 'paris', 2007);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (10005, 'is_coast', '0', 2001);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (10006, 'is_coast', '0', 2006);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (10007, 'is_mountain', '0', 2001);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (10008, 'is_mountain', '1', 2006);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (10009, 'price_fraud_cat_1080_s', '738.23986261018', 2001);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (10010, 'price_logic_min_cat_1080_s', '1181.1837801763', 2001);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (10011, 'price_logic_max_cat_1080_s', '2952.9594504407', 2001);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (10012, 'price_fraud_cat_1080_s', '739.23986261018', 2006);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (10013, 'price_logic_min_cat_1080_s', '1182.1837801763', 2006);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (10014, 'price_logic_max_cat_1080_s', '2953.9594504407', 2006);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (10015, 'price_fraud_cat_1020_s', '1154.3440838249', 2001);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (10016, 'price_logic_min_cat_1020_s', '1846.9505341198', 2001);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (10017, 'price_logic_max_cat_1020_s', '4617.3763352994', 2001);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (10018, 'price_fraud_cat_1020_s', '1155.3440838249', 2006);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (10019, 'price_logic_min_cat_1020_s', '1847.9505341198', 2006);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (10020, 'price_logic_max_cat_1020_s', '4618.3763352994', 2006);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (10021, 'price_fraud_cat_10_s', '3', 2001);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (10022, 'price_logic_min_cat_10_s', '4.8', 2001);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (10023, 'price_logic_max_cat_10_s', '12', 2001);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (10024, 'price_fraud_cat_10_s', '4', 2006);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (10025, 'price_logic_min_cat_10_s', '5.8', 2006);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (10026, 'price_logic_max_cat_10_s', '13', 2006);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (10027, 'price_fraud_cat_11_s', '1.5', 2001);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (10028, 'price_logic_min_cat_11_s', '2.4', 2001);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (10029, 'price_logic_max_cat_11_s', '6', 2001);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (10030, 'price_fraud_cat_11_s', '2.5', 2006);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (10031, 'price_logic_min_cat_11_s', '3.4', 2006);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (10032, 'price_logic_max_cat_11_s', '7', 2006);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (10033, 'price_fraud_cat_12_s', '1.8', 2001);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (10034, 'price_logic_min_cat_12_s', '2.88', 2001);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (10035, 'price_logic_max_cat_12_s', '7.2', 2001);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (10036, 'price_fraud_cat_12_s', '2.8', 2006);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (10037, 'price_logic_min_cat_12_s', '3.88', 2006);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (10038, 'price_logic_max_cat_12_s', '8.2', 2006);


--
-- Name: list_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('list_id_seq', 8000001, true);


--
-- Data for Name: mail_log; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Name: mail_log_mail_log_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('mail_log_mail_log_id_seq', 1, false);


--
-- Data for Name: mail_queue; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Name: mail_queue_mail_queue_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('mail_queue_mail_queue_id_seq', 1, false);


--
-- Data for Name: mama_attribute_wordlists; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Data for Name: mama_attribute_categories; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Data for Name: mama_main_backup; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Data for Name: mama_attribute_categories_backup; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Name: mama_attribute_wordlists_attribute_wordlist_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('mama_attribute_wordlists_attribute_wordlist_id_seq', 1, false);


--
-- Data for Name: mama_attribute_wordlists_backup; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Data for Name: mama_attribute_words; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Data for Name: mama_attribute_words_backup; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Name: mama_attribute_words_word_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('mama_attribute_words_word_id_seq', 1, false);


--
-- Data for Name: mama_exception_lists; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Data for Name: mama_exception_lists_backup; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Name: mama_exception_lists_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('mama_exception_lists_id_seq', 1, false);


--
-- Data for Name: mama_exception_words; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Data for Name: mama_exception_words_backup; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Name: mama_exception_words_exception_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('mama_exception_words_exception_id_seq', 1, false);


--
-- Name: mama_main_backup_backup_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('mama_main_backup_backup_id_seq', 1, false);


--
-- Data for Name: mama_wordlists; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Data for Name: mama_wordlists_backup; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Name: mama_wordlists_wordlist_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('mama_wordlists_wordlist_id_seq', 1, false);


--
-- Data for Name: mama_words; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Data for Name: mama_words_backup; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Name: mama_words_word_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('mama_words_word_id_seq', 1, false);


--
-- Data for Name: most_popular_ads; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Name: most_popular_ads_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('most_popular_ads_id_seq', 1, false);


--
-- Name: next_image_id; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('next_image_id', 1, true);


--
-- Data for Name: notices; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Name: notices_notice_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('notices_notice_id_seq', 1, false);


--
-- Data for Name: on_call; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Data for Name: on_call_actions; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Name: on_call_actions_action_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('on_call_actions_action_id_seq', 1, false);


--
-- Name: on_call_on_call_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('on_call_on_call_id_seq', 1, false);


--
-- Name: order_id_suffix_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('order_id_suffix_seq', 1, false);


--
-- Data for Name: packs; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Name: packs_pack_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('packs_pack_id_seq', 1, false);


--
-- Data for Name: pageviews_per_reg_cat; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Name: pay_code_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('pay_code_seq', 64, true);


--
-- Data for Name: pay_log; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (0, 'adminclear', 0, '59876', 99999917, '2006-04-06 15:19:46', 50, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (1, 'save', 0, '112200000', NULL, '2015-07-01 11:19:08', 60, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (2, 'verify', 0, '112200000', NULL, '2015-07-01 11:19:09', 60, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (3, 'save', 0, '112200001', NULL, '2015-07-01 11:20:35', 61, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (4, 'verify', 0, '112200001', NULL, '2015-07-01 11:20:35', 61, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (5, 'save', 0, '12345', NULL, '2015-07-01 11:20:43', 62, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (6, 'paycard_save', 0, '12345', NULL, '2015-07-01 11:20:46', 62, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (7, 'paycard', 3600, '0062a', NULL, '2015-07-01 11:20:47', 62, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (8, 'paycard', 3600, '12345', NULL, '2015-07-01 11:20:47', 62, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (9, 'save', 0, '112200002', NULL, '2015-07-01 11:21:58', 63, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (10, 'verify', 0, '112200002', NULL, '2015-07-01 11:21:58', 63, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (11, 'save', 0, '65432', NULL, '2015-07-01 11:22:06', 64, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (12, 'paycard_save', 0, '65432', NULL, '2015-07-01 11:22:08', 64, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (13, 'paycard', 2700, '0064a', NULL, '2015-07-01 11:22:08', 64, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (14, 'paycard', 2700, '65432', NULL, '2015-07-01 11:22:08', 64, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (15, 'save', 0, '112200003', NULL, '2015-07-01 11:23:04', 65, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (16, 'verify', 0, '112200003', NULL, '2015-07-01 11:23:04', 65, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (17, 'save', 0, '32323', NULL, '2015-07-01 11:23:13', 66, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (18, 'paycard_save', 0, '32323', NULL, '2015-07-01 11:23:15', 66, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (19, 'paycard', 4500, '0066a', NULL, '2015-07-01 11:23:15', 66, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (20, 'paycard', 4500, '32323', NULL, '2015-07-01 11:23:15', 66, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (21, 'save', 0, '112200004', NULL, '2015-07-01 11:24:20', 67, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (22, 'verify', 0, '112200004', NULL, '2015-07-01 11:24:20', 67, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (23, 'save', 0, '112200005', NULL, '2015-07-01 11:37:27', 68, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (24, 'verify', 0, '112200005', NULL, '2015-07-01 11:37:27', 68, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (25, 'save', 0, '54545', NULL, '2015-07-01 11:37:32', 69, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (26, 'paycard_save', 0, '54545', NULL, '2015-07-01 11:37:34', 69, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (27, 'paycard', 4500, '0069a', NULL, '2015-07-01 11:37:34', 69, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (28, 'paycard', 4500, '54545', NULL, '2015-07-01 11:37:34', 69, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (29, 'save', 0, '112200006', NULL, '2015-07-01 11:38:52', 70, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (30, 'verify', 0, '112200006', NULL, '2015-07-01 11:38:52', 70, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (31, 'save', 0, '112200007', NULL, '2015-07-01 11:41:29', 71, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (32, 'verify', 0, '112200007', NULL, '2015-07-01 11:41:29', 71, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (33, 'save', 0, '90001', NULL, '2015-07-01 11:41:34', 72, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (34, 'paycard_save', 0, '90001', NULL, '2015-07-01 11:41:36', 72, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (35, 'paycard', 2700, '0072a', NULL, '2015-07-01 11:41:36', 72, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (36, 'paycard', 2700, '90001', NULL, '2015-07-01 11:41:36', 72, 'OK');


--
-- Name: pay_log_pay_log_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('pay_log_pay_log_id_seq', 36, true);


--
-- Data for Name: pay_log_references; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (0, 0, 'remote_addr', '192.168.4.75');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (1, 0, 'adphone', '98765465');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (1, 1, 'remote_addr', '10.0.1.101');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (2, 0, 'remote_addr', '10.0.1.101');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (3, 0, 'adphone', '98765465');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (3, 1, 'remote_addr', '10.0.1.101');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (4, 0, 'remote_addr', '10.0.1.101');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (5, 0, 'remote_addr', '10.0.1.101');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (6, 0, 'auth_code', '804800');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (6, 1, 'trans_date', '01/07/2015 11:20:44');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (6, 2, 'placements_type', 'VN');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (6, 3, 'placements_number', '1');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (6, 4, 'pay_type', 'credit');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (6, 5, 'external_response', '0');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (6, 6, 'external_id', '45182670035613873202');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (6, 7, 'remote_addr', '10.0.1.208');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (7, 0, 'webpay', '1');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (9, 0, 'adphone', '98765465');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (9, 1, 'remote_addr', '10.0.1.101');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (10, 0, 'remote_addr', '10.0.1.101');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (11, 0, 'remote_addr', '10.0.1.101');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (12, 0, 'auth_code', '219858');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (12, 1, 'trans_date', '01/07/2015 11:22:07');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (12, 2, 'placements_type', 'VN');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (12, 3, 'placements_number', '1');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (12, 4, 'pay_type', 'credit');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (12, 5, 'external_response', '0');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (12, 6, 'external_id', '72520265213210794111');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (12, 7, 'remote_addr', '10.0.1.208');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (13, 0, 'webpay', '1');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (15, 0, 'adphone', '98765465');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (15, 1, 'remote_addr', '10.0.1.101');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (16, 0, 'remote_addr', '10.0.1.101');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (17, 0, 'remote_addr', '10.0.1.101');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (18, 0, 'auth_code', '961666');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (18, 1, 'trans_date', '01/07/2015 11:23:14');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (18, 2, 'placements_type', 'VN');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (18, 3, 'placements_number', '1');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (18, 4, 'pay_type', 'credit');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (18, 5, 'external_response', '0');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (18, 6, 'external_id', '49407955091446462014');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (18, 7, 'remote_addr', '10.0.1.208');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (19, 0, 'webpay', '1');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (21, 0, 'adphone', '98765465');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (21, 1, 'remote_addr', '10.0.1.101');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (22, 0, 'remote_addr', '10.0.1.101');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (23, 0, 'adphone', '98765465');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (23, 1, 'remote_addr', '10.0.1.101');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (24, 0, 'remote_addr', '10.0.1.101');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (25, 0, 'remote_addr', '10.0.1.101');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (26, 0, 'auth_code', '687852');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (26, 1, 'trans_date', '01/07/2015 11:37:33');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (26, 2, 'placements_type', 'VN');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (26, 3, 'placements_number', '1');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (26, 4, 'pay_type', 'credit');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (26, 5, 'external_response', '0');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (26, 6, 'external_id', '57432915185690163087');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (26, 7, 'remote_addr', '10.0.1.208');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (27, 0, 'webpay', '1');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (29, 0, 'adphone', '98765465');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (29, 1, 'remote_addr', '10.0.1.101');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (30, 0, 'remote_addr', '10.0.1.101');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (31, 0, 'adphone', '98765465');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (31, 1, 'remote_addr', '10.0.1.101');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (32, 0, 'remote_addr', '10.0.1.101');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (33, 0, 'remote_addr', '10.0.1.101');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (34, 0, 'auth_code', '688480');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (34, 1, 'trans_date', '01/07/2015 11:41:35');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (34, 2, 'placements_type', 'VN');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (34, 3, 'placements_number', '1');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (34, 4, 'pay_type', 'credit');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (34, 5, 'external_response', '0');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (34, 6, 'external_id', '32462168163922191973');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (34, 7, 'remote_addr', '10.0.1.208');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (35, 0, 'webpay', '1');


--
-- Name: payment_groups_payment_group_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('payment_groups_payment_group_id_seq', 72, true);


--
-- Data for Name: payments; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (0, 'ad_action', 20, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (1, 'ad_action', 20, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (2, 'ad_action', 20, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (3, 'ad_action', 20, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (50, 'ad_action', 20, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (60, 'ad_action', 0, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (61, 'ad_action', 0, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (62, 'upselling_gallery', 3600, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (63, 'ad_action', 0, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (64, 'upselling_weekly_bump', 2700, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (65, 'ad_action', 0, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (66, 'upselling_daily_bump', 4500, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (67, 'ad_action', 0, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (68, 'ad_action', 0, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (69, 'upselling_daily_bump', 4500, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (70, 'ad_action', 0, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (71, 'ad_action', 0, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (72, 'upselling_weekly_bump', 2700, 0);


--
-- Data for Name: pricelist; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO pricelist (list, item, price) VALUES ('if', 'BL1', 1295);
INSERT INTO pricelist (list, item, price) VALUES ('if', 'BL2', 1295);
INSERT INTO pricelist (list, item, price) VALUES ('if', 'BL3', 995);
INSERT INTO pricelist (list, item, price) VALUES ('if', 'BL4', 995);


--
-- Data for Name: purchase; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO purchase (purchase_id, doc_type, doc_num, external_doc_id, status, receipt, delivery, rut, name, lob, address, region, communes, contact, email, tax, discount, seller_id, total_price, payment_group_id, payment_method, account_id) VALUES (1, 'bill', NULL, NULL, 'paid', '2015-07-01 11:20:43.021459', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'gupselling@yapo.cl', 0, 0, NULL, 3600, 62, 'webpay', NULL);
INSERT INTO purchase (purchase_id, doc_type, doc_num, external_doc_id, status, receipt, delivery, rut, name, lob, address, region, communes, contact, email, tax, discount, seller_id, total_price, payment_group_id, payment_method, account_id) VALUES (2, 'bill', NULL, NULL, 'paid', '2015-07-01 11:22:05.56416', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'wupselling@yapo.cl', 0, 0, NULL, 2700, 64, 'webpay', NULL);
INSERT INTO purchase (purchase_id, doc_type, doc_num, external_doc_id, status, receipt, delivery, rut, name, lob, address, region, communes, contact, email, tax, discount, seller_id, total_price, payment_group_id, payment_method, account_id) VALUES (3, 'bill', NULL, NULL, 'paid', '2015-07-01 11:23:13.029125', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'dupselling@yapo.cl', 0, 0, NULL, 4500, 66, 'webpay', NULL);
INSERT INTO purchase (purchase_id, doc_type, doc_num, external_doc_id, status, receipt, delivery, rut, name, lob, address, region, communes, contact, email, tax, discount, seller_id, total_price, payment_group_id, payment_method, account_id) VALUES (4, 'bill', NULL, NULL, 'paid', '2015-07-01 11:37:31.815333', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'editupselling@yapo.cl', 0, 0, NULL, 4500, 69, 'webpay', NULL);
INSERT INTO purchase (purchase_id, doc_type, doc_num, external_doc_id, status, receipt, delivery, rut, name, lob, address, region, communes, contact, email, tax, discount, seller_id, total_price, payment_group_id, payment_method, account_id) VALUES (5, 'bill', NULL, NULL, 'paid', '2015-07-01 11:41:33.769528', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'renewupselling@yapo.cl', 0, 0, NULL, 2700, 72, 'webpay', NULL);


--
-- Data for Name: purchase_detail; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO purchase_detail (purchase_detail_id, purchase_id, product_id, price, action_id, ad_id, payment_group_id) VALUES (1, 1, 102, 3600, 2, 21, 62);
INSERT INTO purchase_detail (purchase_detail_id, purchase_id, product_id, price, action_id, ad_id, payment_group_id) VALUES (2, 2, 101, 2700, 2, 22, 64);
INSERT INTO purchase_detail (purchase_detail_id, purchase_id, product_id, price, action_id, ad_id, payment_group_id) VALUES (3, 3, 103, 4500, 2, 23, 66);
INSERT INTO purchase_detail (purchase_detail_id, purchase_id, product_id, price, action_id, ad_id, payment_group_id) VALUES (4, 4, 103, 4500, 3, 24, 69);
INSERT INTO purchase_detail (purchase_detail_id, purchase_id, product_id, price, action_id, ad_id, payment_group_id) VALUES (5, 5, 101, 2700, 4, 25, 72);


--
-- Name: purchase_detail_purchase_detail_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('purchase_detail_purchase_detail_id_seq', 5, true);


--
-- Name: purchase_purchase_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('purchase_purchase_id_seq', 5, true);


--
-- Data for Name: purchase_states; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO purchase_states (purchase_id, purchase_state_id, status, "timestamp", remote_addr) VALUES (1, 1, 'pending', '2015-07-01 11:20:43.021459', '10.0.1.101');
INSERT INTO purchase_states (purchase_id, purchase_state_id, status, "timestamp", remote_addr) VALUES (2, 2, 'pending', '2015-07-01 11:22:05.56416', '10.0.1.101');
INSERT INTO purchase_states (purchase_id, purchase_state_id, status, "timestamp", remote_addr) VALUES (3, 3, 'pending', '2015-07-01 11:23:13.029125', '10.0.1.101');
INSERT INTO purchase_states (purchase_id, purchase_state_id, status, "timestamp", remote_addr) VALUES (4, 4, 'pending', '2015-07-01 11:37:31.815333', '10.0.1.101');
INSERT INTO purchase_states (purchase_id, purchase_state_id, status, "timestamp", remote_addr) VALUES (5, 5, 'pending', '2015-07-01 11:41:33.769528', '10.0.1.101');


--
-- Name: purchase_states_purchase_state_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('purchase_states_purchase_state_id_seq', 5, true);


--
-- Data for Name: redir_stats; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Name: redir_stats_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('redir_stats_id_seq', 1, false);


--
-- Data for Name: refunds; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Name: refunds_refund_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('refunds_refund_id_seq', 1, false);


--
-- Name: report_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('report_id_seq', 1, false);


--
-- Name: reporter_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('reporter_id_seq', 1, false);


--
-- Data for Name: review_log; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO review_log (ad_id, action_id, admin_id, review_time, queue, action_type, action, refusal_reason_text, category) VALUES (24, 1, 9, '2015-07-01 11:36:42.542847', 'all', 'new', 'accepted', NULL, 6080);
INSERT INTO review_log (ad_id, action_id, admin_id, review_time, queue, action_type, action, refusal_reason_text, category) VALUES (25, 1, 9, '2015-07-01 11:39:12.935439', 'all', 'new', 'accepted', NULL, 6100);


--
-- Data for Name: sms_users; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Data for Name: sms_log; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Name: sms_log_sms_log_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('sms_log_sms_log_id_seq', 1, false);


--
-- Data for Name: watch_users; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Data for Name: watch_queries; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Data for Name: sms_log_watch; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Name: sms_users_sms_user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('sms_users_sms_user_id_seq', 1, false);


--
-- Data for Name: state_params; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (20, 1, 202, 'queue', 'normal');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (21, 1, 205, 'queue', 'normal');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (22, 1, 211, 'queue', 'normal');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (23, 1, 217, 'queue', 'normal');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (24, 1, 223, 'queue', 'normal');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (24, 1, 224, 'filter_name', 'all');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (24, 2, 236, 'queue', 'edit');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (25, 1, 242, 'queue', 'normal');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (25, 1, 243, 'filter_name', 'all');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (25, 3, 247, 'queue', 'normal');


--
-- Data for Name: stats_daily; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Data for Name: stats_daily_ad_actions; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Name: stats_daily_ad_actions_stat_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('stats_daily_ad_actions_stat_id_seq', 1, false);


--
-- Data for Name: stats_hourly; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Data for Name: store_actions; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Data for Name: store_action_states; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Name: store_action_states_state_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('store_action_states_state_id_seq', 1, false);


--
-- Data for Name: store_changes; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Name: store_image_id; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('store_image_id', 1, false);


--
-- Data for Name: store_login_tokens; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Name: store_login_tokens_store_login_token_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('store_login_tokens_store_login_token_id_seq', 1, false);


--
-- Data for Name: store_media; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Data for Name: store_params; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Data for Name: store_users; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Name: stores_store_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('stores_store_id_seq', 1, false);


--
-- Data for Name: synonyms; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (1, 3040, 'armoire', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (2, 3040, 'armoires ', 1);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (3, 3040, 'banc', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (4, 3040, 'bancs ', 3);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (5, 3040, 'banquette ', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (6, 3040, 'banquettes ', 5);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (7, 3040, 'bergere', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (8, 3040, 'bergeres', 7);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (9, 3040, 'bibliotheque ', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (10, 3040, 'biblioteque', 9);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (11, 3040, 'bibliotheques', 9);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (12, 3040, 'buffet ', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (13, 3040, 'bufet', 12);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (14, 3040, 'buffets', 12);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (15, 3040, 'bureau ', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (16, 3040, 'bureaux ', 15);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (17, 3040, 'canape', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (18, 3040, 'canaper ', 17);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (19, 3040, 'canapes ', 17);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (20, 3040, 'canapee', 17);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (21, 3040, 'cannape', 17);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (22, 3040, 'chaise ', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (23, 3040, 'chaises ', 22);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (24, 3040, 'chevet ', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (25, 3040, 'chevets ', 24);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (26, 3040, 'clic clac', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (27, 3040, 'clic-clac', 26);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (28, 3040, 'cliclac', 26);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (29, 3040, 'commode ', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (30, 3040, 'commodes ', 29);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (31, 3040, 'comode', 29);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (32, 3040, 'etagere ', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (33, 3040, 'etageres ', 32);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (34, 3040, 'fauteuil ', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (35, 3040, 'fauteil ', 34);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (36, 3040, 'fauteils ', 34);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (37, 3040, 'fauteuille ', 34);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (38, 3040, 'fauteuilles ', 34);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (39, 3040, 'fauteuils ', 34);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (40, 3040, 'matelas ', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (41, 3040, 'matelat ', 40);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (42, 3040, 'meuble ', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (43, 3040, 'meubles ', 42);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (44, 3040, 'mezzanine ', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (45, 3040, 'mezanine ', 44);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (46, 3040, 'porte-manteau', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (47, 3040, 'porte-manteaux', 46);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (48, 3040, 'porte manteau', 46);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (49, 3040, 'porte manteaux', 46);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (50, 3040, 'pouf', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (51, 3040, 'poufs ', 50);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (52, 3040, 'rangement ', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (53, 3040, 'rangements ', 52);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (54, 3040, 'rocking chair', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (55, 3040, 'rocking-chair', 54);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (56, 3040, 'siege ', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (57, 3040, 'sieges', 56);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (58, 3040, 'sommier ', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (59, 3040, 'sommiers ', 58);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (60, 3040, 'table ', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (61, 3040, 'tables ', 60);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (62, 3040, 'tabouret ', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (63, 3040, 'tabourets ', 62);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (64, 3040, 'tiroir ', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (65, 3040, 'tiroirs ', 64);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (66, 3040, 'vaisselier ', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (67, 3040, 'vaissellier ', 66);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (68, 3060, 'chauffe-eau', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (69, 3060, 'chauffe eau', 68);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (70, 3060, 'cuiseur', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (71, 3060, 'cuisseur', 70);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (72, 3060, 'cuit vapeur', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (73, 3060, 'cuit-vapeur', 72);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (74, 3060, 'gauffrer', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (75, 3060, 'gauffrier', 74);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (76, 3060, 'gaufrier', 74);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (77, 3060, 'gaziniere', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (78, 3060, 'gazinniere', 77);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (79, 3060, 'grille-pain', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (80, 3060, 'grille pain', 79);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (81, 3060, 'lave linge', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (82, 3060, 'lave-linge', 81);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (83, 3060, 'lave vaisselle', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (84, 3060, 'lave-vaisselle', 83);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (85, 3060, 'micro onde', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (86, 3060, 'micro ondes', 85);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (87, 3060, 'micro-onde', 85);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (88, 3060, 'micro-ondes', 85);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (89, 3060, 'mixer', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (90, 3060, 'mixeur', 89);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (91, 3060, 'multifonction', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (92, 3060, 'multifonctions', 91);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (93, 3060, 'plaque a gaz', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (94, 3060, 'plaque gaz', 93);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (95, 3060, 'plaque de cuisson', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (96, 3060, 'plaques de cuisson', 95);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (97, 3060, 'plaque electrique', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (98, 3060, 'plaques electriques', 97);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (99, 3060, 'presse agrumes', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (100, 3060, 'presse agrume', 99);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (101, 3060, 'refrigerateur', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (102, 3060, 'refrigirateur', 101);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (103, 3060, 'refregirateur', 101);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (104, 3060, 'seche cheveux', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (105, 3060, 'seche-cheveux', 104);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (106, 3060, 'seche linge', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (107, 3060, 'seche-linge', 106);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (108, 3060, 'table cuisson', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (109, 3060, 'table de cuisson', 108);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (110, 3060, 'vitroceramique', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (111, 3060, 'vitro-ceramique', 110);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (112, 3060, 'whirlpool', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (113, 3060, 'whirpool', 112);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (114, 3060, 'wirlpool', 112);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (115, 8020, 'bouteille de gaz', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (116, 8020, 'bouteille gaz', 115);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (117, 8020, 'bouteilles de gaz', 115);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (118, 8020, 'bouteilles gaz', 115);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (119, 8020, 'brique', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (120, 8020, 'briques', 119);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (121, 8020, 'carrelage', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (122, 8020, 'carrelages', 121);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (123, 8020, 'casier a bouteille', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (124, 8020, 'casier a bouteilles', 123);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (125, 8020, 'chauffage', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (126, 8020, 'chauffages', 125);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (127, 8020, 'chauffe eau', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (128, 8020, 'chauffe-eau', 127);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (129, 8020, 'convecteur', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (130, 8020, 'convecteurs', 129);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (131, 8020, 'cuve', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (132, 8020, 'cuves', 131);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (133, 8020, 'disjoncteur', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (134, 8020, 'disjoncteurs', 133);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (135, 8020, 'fenetre', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (136, 8020, 'fenetres', 135);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (137, 8020, 'plante', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (138, 8020, 'plantes', 137);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (139, 8020, 'porte', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (140, 8020, 'portes', 139);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (141, 8020, 'radiateur', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (142, 8020, 'radiateurs', 141);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (143, 8020, 'remblai', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (144, 8020, 'remblais', 143);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (145, 8020, 'store', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (146, 8020, 'stores', 145);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (147, 8020, 'taille haie', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (148, 8020, 'taille-haie', 147);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (149, 8020, 'taille haies', 147);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (150, 8020, 'taille-haies', 147);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (151, 8020, 'tonneau', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (152, 8020, 'tonneaux', 151);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (153, 8020, 'tuile', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (154, 8020, 'tuiles', 153);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (155, 8020, 'tuyau', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (156, 8020, 'tuyaux', 155);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (157, 8020, 'vasque', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (158, 8020, 'vasques', 157);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (159, 8020, 'volet', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (160, 8020, 'volets', 159);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (161, 8060, 'biberon', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (162, 8060, 'biberons', 161);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (163, 8060, 'chaussure', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (164, 8060, 'chaussures', 163);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (165, 8060, 'ensemble fille', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (166, 8060, 'ensemble filles', 165);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (167, 8060, 'ensembles fille', 165);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (168, 8060, 'ensembles filles', 165);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (169, 8060, 'ensemble garcon', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (170, 8060, 'ensemble garcons', 169);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (171, 8060, 'ensembles garcon', 169);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (172, 8060, 'ensembles garcons', 169);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (173, 8060, 'gigoteuse', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (174, 8060, 'gigoteuses', 173);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (175, 8060, 'landau', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (176, 8060, 'landeau', 175);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (177, 8060, 'lit parapluie', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (178, 8060, 'lit-parapluie', 177);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (179, 8060, 'pantalon', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (180, 8060, 'pantalons', 179);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (181, 8060, 'porte bebe', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (182, 8060, 'porte-bb', 181);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (183, 8060, 'porte-bebe', 181);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (184, 8060, 'poussette', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (185, 8060, 'pousette', 184);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (186, 8060, 'pyjama', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (187, 8060, 'pyjamas', 186);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (188, 8060, 'salopette', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (189, 8060, 'salopettes', 188);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (190, 8060, 'tee shirt', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (191, 8060, 'tee-shirt', 190);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (192, 8060, 'tshirt', 190);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (193, 8060, 'vertbaudet', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (194, 8060, 'vert baudet', 193);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (195, 8060, 'verbaudet', 193);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (196, 8060, 'vetement bebe', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (197, 8060, 'vetements bebe', 196);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (198, 8060, 'vetements enfant', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (199, 8060, 'vetement enfant', 198);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (200, 8060, 'vetements enfants', 198);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (201, 8060, 'vetements fille', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (202, 8060, 'vetements filles', 201);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (203, 8060, 'vetements garcon', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (204, 8060, 'vetements garcons', 203);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (205, 41, 'babyfoot', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (206, 41, 'baby-foot', 205);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (207, 41, 'baby foot', 205);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (208, 41, 'barbie', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (209, 41, 'barbies', 208);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (210, 41, 'camion de pompier', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (211, 41, 'camion pompier', 210);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (212, 41, 'domino', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (213, 41, 'dominos', 212);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (214, 41, 'dora', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (215, 41, 'dora l''exploratrice', 214);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (216, 41, 'figurine', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (217, 41, 'figurines', 216);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (218, 41, 'fisher price', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (219, 41, 'fischer price', 218);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (220, 41, 'fisherprice', 218);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (221, 41, 'fisher-price', 218);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (222, 41, 'jeu de construction', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (223, 41, 'jeux de construction', 222);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (224, 41, 'jeu de societe', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (225, 41, 'jeux de societe', 224);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (226, 41, 'jeu educatif', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (227, 41, 'jeux educatifs', 226);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (228, 41, 'jouet', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (229, 41, 'jouets', 228);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (230, 41, 'lego', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (231, 41, 'legos', 230);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (232, 41, 'peluche', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (233, 41, 'pelluche', 232);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (234, 41, 'peluches', 232);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (235, 41, 'playmobil', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (236, 41, 'playmobils', 235);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (237, 41, 'playmobile', 235);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (238, 41, 'poupee', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (239, 41, 'poupees', 238);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (240, 41, 'puzzle', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (241, 41, 'puzzles', 240);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (242, 41, 'trottinette', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (243, 41, 'trotinette', 242);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (244, 41, 'vtech', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (245, 41, 'v-tech', 244);


--
-- Name: synonyms_syn_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('synonyms_syn_id_seq', 245, true);


--
-- Name: tokens_token_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('tokens_token_id_seq', 73, true);


--
-- Data for Name: trans_queue; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (6, '2015-07-01 11:36:42.609065', '20527@mazaru.schibsted.cl', NULL, '2015-07-01 11:36:49.218299', '20527@mazaru.schibsted.cl', 'TRANS_OK', '2015-07-01 11:36:49.182644', NULL, 'cmd:admail
commit:1
ad_id:24
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (5, '2015-07-01 11:23:14.990182', '20527@mazaru.schibsted.cl', NULL, '2015-07-01 11:36:49.241298', '20527@mazaru.schibsted.cl', 'TRANS_OK', '2015-07-01 11:36:49.224431', NULL, '
cmd:multi_product_mail
to:dupselling@yapo.cl
doc_type:bill
product_ids:103
ad_names:Aviso upselling daily
commit:1', '220 Welcome.
status:TRANS_OK
end
', 'bump_mail', NULL, NULL);
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (3, '2015-07-01 11:22:07.810197', '20527@mazaru.schibsted.cl', NULL, '2015-07-01 11:36:49.241476', '20527@mazaru.schibsted.cl', 'TRANS_OK', '2015-07-01 11:36:49.224431', NULL, '
cmd:multi_product_mail
to:wupselling@yapo.cl
doc_type:bill
product_ids:101
ad_names:Aviso upselling weekly
commit:1', '220 Welcome.
status:TRANS_OK
end
', 'bump_mail', NULL, NULL);
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (1, '2015-07-01 11:20:47.118908', '20527@mazaru.schibsted.cl', NULL, '2015-07-01 11:36:49.24162', '20527@mazaru.schibsted.cl', 'TRANS_OK', '2015-07-01 11:36:49.224431', NULL, '
cmd:multi_product_mail
to:gupselling@yapo.cl
doc_type:bill
product_ids:102
ad_names:Aviso upselling gallery
commit:1', '220 Welcome.
status:TRANS_OK
end
', 'bump_mail', NULL, NULL);
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (4, '2015-07-01 11:23:14.986137', '20527@mazaru.schibsted.cl', '2015-07-01 11:25:14.986137', '2015-07-01 11:36:49.271441', '20527@mazaru.schibsted.cl', 'TRANS_OK', '2015-07-01 11:36:49.256271', NULL, '
cmd:weekly_bump_ad
ad_id:23
action_id:2
counter:4
batch:1
daily_bump:1
do_not_send_mail:1
is_upselling:1
commit:1', '220 Welcome.
status:TRANS_OK
end
', 'upselling', NULL, NULL);
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (7, '2015-07-01 11:36:49.275966', '20527@mazaru.schibsted.cl', '2015-07-01 11:38:49.275966', NULL, NULL, NULL, NULL, NULL, 'cmd:weekly_bump_ad
ad_id:23
action_id:2
counter:3
daily_bump:1
is_upselling:1
batch:1
commit:1
', NULL, 'upselling', NULL, NULL);
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (2, '2015-07-01 11:22:07.804702', '20527@mazaru.schibsted.cl', '2015-07-01 11:24:07.804702', '2015-07-01 11:36:49.283703', '20527@mazaru.schibsted.cl', 'TRANS_OK', '2015-07-01 11:36:49.256271', NULL, '
cmd:weekly_bump_ad
ad_id:22
action_id:2
counter:4
batch:1
do_not_send_mail:1
is_upselling:1
commit:1', '220 Welcome.
status:TRANS_OK
end
', 'upselling', NULL, NULL);
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (8, '2015-07-01 11:36:49.284664', '20527@mazaru.schibsted.cl', '2015-07-01 11:37:49.284664', NULL, NULL, NULL, NULL, NULL, 'cmd:weekly_bump_ad
ad_id:22
action_id:2
counter:3
is_upselling:1
batch:1
commit:1
', NULL, 'upselling', NULL, NULL);
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (9, '2015-07-01 11:37:33.881811', '20527@mazaru.schibsted.cl', '2015-07-01 11:39:33.881811', NULL, NULL, NULL, NULL, NULL, '
cmd:weekly_bump_ad
ad_id:24
action_id:3
counter:4
batch:1
daily_bump:1
do_not_send_mail:1
is_upselling:1
commit:1', NULL, 'upselling', NULL, NULL);
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (10, '2015-07-01 11:37:33.884342', '20527@mazaru.schibsted.cl', NULL, NULL, NULL, NULL, NULL, NULL, '
cmd:multi_product_mail
to:editupselling@yapo.cl
doc_type:bill
product_ids:103
ad_names:Aviso upselling on edit
commit:1', NULL, 'bump_mail', NULL, NULL);
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (11, '2015-07-01 11:39:12.971903', '20527@mazaru.schibsted.cl', NULL, NULL, NULL, NULL, NULL, NULL, 'cmd:admail
commit:1
ad_id:25
action_id:1
mail_type:accept
', NULL, 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (12, '2015-07-01 11:41:35.82146', '20527@mazaru.schibsted.cl', '2015-07-01 11:43:35.82146', NULL, NULL, NULL, NULL, NULL, '
cmd:weekly_bump_ad
ad_id:25
action_id:4
counter:4
batch:1
do_not_send_mail:1
is_upselling:1
commit:1', NULL, 'upselling', NULL, NULL);
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (13, '2015-07-01 11:41:35.827274', '20527@mazaru.schibsted.cl', NULL, NULL, NULL, NULL, NULL, NULL, '
cmd:multi_product_mail
to:renewupselling@yapo.cl
doc_type:bill
product_ids:101
ad_names:Aviso upselling on renew
commit:1', NULL, 'bump_mail', NULL, NULL);


--
-- Name: trans_queue_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('trans_queue_id_seq', 13, true);


--
-- Data for Name: transbank_log; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Name: transbank_log_log_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('transbank_log_log_id_seq', 1, false);


--
-- Name: uid_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('uid_seq', 50, true);


--
-- Data for Name: unfinished_ads; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Name: unfinished_ads_unf_ad_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('unfinished_ads_unf_ad_id_seq', 1, false);


--
-- Data for Name: user_params; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO user_params (user_id, name, value) VALUES (54, 'first_approved_ad', '2015-07-01 11:36:42.542847-03');
INSERT INTO user_params (user_id, name, value) VALUES (55, 'first_approved_ad', '2015-07-01 11:39:12.935439-03');

INSERT INTO user_params (user_id, name, value) VALUES (54, 'first_inserted_ad', '2015-07-01 11:36:42.542847-03');
INSERT INTO user_params (user_id, name, value) VALUES (55, 'first_inserted_ad', '2015-07-01 11:39:12.935439-03');

--
-- Data for Name: user_testimonial; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Name: user_testimonial_user_testimonial_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('user_testimonial_user_testimonial_id_seq', 1, false);


--
-- Name: users_user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('users_user_id_seq', 55, true);


--
-- Name: verify_code_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('verify_code_seq', 90, true);


--
-- Data for Name: visitor; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Data for Name: visits; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Data for Name: vouchers; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Data for Name: voucher_actions; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Name: voucher_actions_voucher_action_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('voucher_actions_voucher_action_id_seq', 1, false);


--
-- Data for Name: voucher_states; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Name: voucher_states_voucher_state_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('voucher_states_voucher_state_id_seq', 1, false);


--
-- Name: vouchers_voucher_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('vouchers_voucher_id_seq', 1, false);


--
-- Data for Name: watch_ads; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Name: watch_users_watch_user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('watch_users_watch_user_id_seq', 1, false);


--
-- PostgreSQL database dump complete
--

