INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (137, 8000096, NOW(), 'active', 'sell', 'OtherRegionAccount', '12312312', 10, 0, 2040, 3, NULL, false, false, true, 'Furgon Suzuki APV', 'Est� como nuevo', 12312312, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$Xz9fObnE6R=?;WF3dedc5f856ce5f31bf35015ac0f38cd5edddf155a', NOW(), 'es');
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (179, '633026231', 'verified', '2015-04-15 16:23:34.967025', NULL);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (137, 1, 'new', 747, 'accepted', 'pack', NULL, NULL, 179);
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (137, 1, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (137, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (137, 1, 'pack_result', '0');
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (137, 1, 744, 'reg', 'initial', '2015-04-15 16:23:34.967025', '10.0.1.149', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (137, 1, 745, 'unverified', 'verifymail', '2015-04-15 16:23:34.967025', '10.0.1.149', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (137, 1, 746, 'pending_review', 'verify', '2015-04-15 16:23:35.124594', '10.0.1.149', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (137, 1, 747, 'accepted', 'accept', '2015-04-15 16:23:47.306942', '10.0.1.149', NULL);
SELECT pg_catalog.setval('action_states_state_id_seq', 757, true);
INSERT INTO ad_changes (ad_id, action_id, state_id, is_param, column_name, old_value, new_value) VALUES (137, 1, 745, true, 'pack_status', NULL, 'active');
INSERT INTO ad_codes (code, code_type) VALUES (16994, 'pay');
INSERT INTO ad_image_changes (ad_id, action_id, state_id, seq_no, name, is_new) VALUES (137, 1, 747, 0, NULL, false);
INSERT INTO ad_params (ad_id, name, value) VALUES (137, 'regdate', '2015');
INSERT INTO ad_params (ad_id, name, value) VALUES (137, 'mileage', '121212');
INSERT INTO ad_params (ad_id, name, value) VALUES (137, 'gearbox', '1');
INSERT INTO ad_params (ad_id, name, value) VALUES (137, 'fuel', '1');
INSERT INTO ad_params (ad_id, name, value) VALUES (137, 'prev_currency', 'peso');
INSERT INTO ad_params (ad_id, name, value) VALUES (137, 'country', 'UNK');
INSERT INTO ad_params (ad_id, name, value) VALUES (137, 'pack_status', 'active');
SELECT pg_catalog.setval('ads_ad_id_seq', 137, true);
SELECT pg_catalog.setval('list_id_seq', 8000096, true);
SELECT pg_catalog.setval('pay_code_seq', 478, true);
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (241, 'save', 0, '633026231', NULL, '2015-04-15 16:23:35', 179, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (242, 'verify', 0, '633026231', NULL, '2015-04-15 16:23:35', 179, 'OK');
SELECT pg_catalog.setval('pay_log_pay_log_id_seq', 242, true);
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (241, 0, 'adphone', '12312312');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (241, 1, 'remote_addr', '10.0.1.149');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (242, 0, 'remote_addr', '10.0.1.149');
SELECT pg_catalog.setval('payment_groups_payment_group_id_seq', 179, true);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (179, 'ad_action', 0, 0);
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (137, 1, 746, 'queue', 'pack');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (137, 1, 747, 'filter_name', 'pack');
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (94, '2015-04-15 16:23:47.346482', '22531@mazaru.schibsted.cl', NULL, NULL, NULL, NULL, NULL, NULL, 'cmd:admail
commit:1
ad_id:137
action_id:1
mail_type:accept
', NULL, 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (93, '2015-04-15 16:23:35.279652', '22531@mazaru.schibsted.cl', '2015-04-15 16:23:45.279652', '2015-04-15 16:23:47.427899', '22531@mazaru.schibsted.cl', 'TRANS_OK', '2015-04-15 16:23:47.29584', '22531@mazaru.schibsted.cl', 'cmd:review
commit:1
ad_id:137
action_id:1
action:accept
filter_name:pack
remote_addr:10.0.1.149

', '220 Welcome.
status:TRANS_OK
end
', 'AT', 'review', '137');
SELECT pg_catalog.setval('trans_queue_id_seq', 94, true);
INSERT INTO ad_media VALUES (100012353, 137, 0, NOW(), 'image', NULL, NULL, NULL);

--Change all passwords to 123123123
update ads set salted_passwd =  E'$1024$2y1bQ}VYRD}zF4QCbf07a971b784888658a20e62156de64dfde5ffb6', passwd = null;
update accounts set salted_passwd =  E'$1024$2y1bQ}VYRD}zF4QCbf07a971b784888658a20e62156de64dfde5ffb6';
