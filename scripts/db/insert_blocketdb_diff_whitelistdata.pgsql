INSERT INTO users (user_id, uid, email, account, paid_total) VALUES (59, 50, 'border_user@yapo.cl', 0, 0);
INSERT INTO users (user_id, uid, email, account, paid_total) VALUES (60, 50, 'user_without_ads_not_in_whitelist@yapo.cl', 0, 0);
INSERT INTO users (user_id, uid, email, account, paid_total) VALUES (61, 50, '1_rejected_other_moved_review_queue@yapo.cl', 0, 0);
INSERT INTO users (user_id, uid, email, account, paid_total) VALUES (62, 50, '1_forbiddenwords_other_whitelist_queue@yapo.cl', 0, 0);
INSERT INTO users (user_id, uid, email, account, paid_total) VALUES (63, 50, '1_duplicatewarning_other_whitelist_queue@yapo.cl', 0, 0);
INSERT INTO users (user_id, uid, email, account, paid_total) VALUES (64, 50, '1_without_greenprice_other_whitelist_queue@yapo.cl', 0, 0);
INSERT INTO users (user_id, uid, email, account, paid_total) VALUES (65, 50, '1_foreignip_to_review_other_whitelistqueue@yapo.cl', 0, 0);
INSERT INTO users (user_id, uid, email, account, paid_total) VALUES (66, 50, '1_withnote_to_review_other_whitelist_queue@yapo.cl', 0, 0);
INSERT INTO users (user_id, uid, email, account, paid_total) VALUES (67, 50, '1_under1000_to_review_other_whitelistqueue@yapo.cl', 0, 0);
INSERT INTO users (user_id, uid, email, account, paid_total) VALUES (68, 50, 'user_to_reject_ad@yapo.cl', 0, 0);
INSERT INTO users (user_id, uid, email, account, paid_total) VALUES (69, 50, '1_inserted_keep_whitelist@yapo.cl', 0, 0);
INSERT INTO users (user_id, uid, email, account, paid_total) VALUES (70, 50, 'user_doesnt_3accepted_in_3m_other_whitelist_queue@yapo.cl', 0, 0);
INSERT INTO users (user_id, uid, email, account, paid_total) VALUES (71, 50, 'user_to_insert_ad_foreign_ip@yapo.cl', 0, 0);
INSERT INTO users (user_id, uid, email, account, paid_total) VALUES (72, 50, 'user_to_insert_ad_notes@yapo.cl', 0, 0);
INSERT INTO users (user_id, uid, email, account, paid_total) VALUES (73, 50, 'user_to_price_under_1000@yapo.cl', 0, 0);
INSERT INTO users (user_id, uid, email, account, paid_total) VALUES (74, 50, 'user_to_ad_forbidden_words@yapo.cl', 0, 0);
INSERT INTO users (user_id, uid, email, account, paid_total) VALUES (75, 50, 'user_to_ad_duplicate_warning@yapo.cl', 0, 0);
INSERT INTO users (user_id, uid, email, account, paid_total) VALUES (76, 50, 'user_with_ad_without_green_price@yapo.cl', 0, 0);
INSERT INTO users (user_id, uid, email, account, paid_total) VALUES (77, 50, 'user_with_approved_deleted_ads@yapo.cl', 0, 0);
INSERT INTO accounts (account_id, name, rut, email, phone, is_company, region, salted_passwd, creation_date, activation_date, status, address, contact, lob, commune, user_id) VALUES (8, 'Border User', '', 'border_user@yapo.cl', '226437660', false, 15, '$1024$$R;x<_@iBEWlZHVY2b5f37e384b6d125f8a3a278dc0f8d6db707d7bf', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 'active', NULL, NULL, NULL, NULL, 59);
INSERT INTO accounts (account_id, name, rut, email, phone, is_company, region, salted_passwd, creation_date, activation_date, status, address, contact, lob, commune, user_id) VALUES (9, 'user_without_ads', '', 'user_without_ads_not_in_whitelist@yapo.cl', '962184762', false, 15, '$1024$|@}4@P9UY[nd=j"[9f04daed26ca08ad116ebaefe29e6d5ddb959b9c', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 'active', NULL, NULL, NULL, NULL, 60);
INSERT INTO accounts (account_id, name, rut, email, phone, is_company, region, salted_passwd, creation_date, activation_date, status, address, contact, lob, commune, user_id) VALUES (10, '1_rejected_other_moved_review_queue@yapo.cl', '', '1_rejected_other_moved_review_queue@yapo.cl', '962184762', false, 15, '$1024$ovxcOe<"M?r-#L+`c4790807e5179e6ba1a98531682893ce25d911bf', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 'active', NULL, NULL, NULL, NULL, 61);
INSERT INTO accounts (account_id, name, rut, email, phone, is_company, region, salted_passwd, creation_date, activation_date, status, address, contact, lob, commune, user_id) VALUES (11, '1_forbiddenwords_other_whitelist_queue@yapo.cl', '', '1_forbiddenwords_other_whitelist_queue@yapo.cl', '962273874', false, 15, '$1024$X5/$XsE9.SX?$Ka1ca52243003dffd21aaa80017128da236214e8a53', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 'active', NULL, NULL, NULL, NULL, 62);
INSERT INTO accounts (account_id, name, rut, email, phone, is_company, region, salted_passwd, creation_date, activation_date, status, address, contact, lob, commune, user_id) VALUES (12, '1_duplicatewarning_other_whitelist_queue@yapo.cl', '', '1_duplicatewarning_other_whitelist_queue@yapo.cl', '912312312', false, 15, '$1024$+Kttc-a''?Dxb%4#y90be928116122033cfa0531b0a1e5ede1de9d269', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 'active', NULL, NULL, NULL, NULL, 63);
INSERT INTO accounts (account_id, name, rut, email, phone, is_company, region, salted_passwd, creation_date, activation_date, status, address, contact, lob, commune, user_id) VALUES (13, '1_without_greenprice_other_whitelist_queue@yapo.cl', '', '1_without_greenprice_other_whitelist_queue@yapo.cl', '962184762', false, 15, '$1024$0Idi[#D #s*\\9)XU6c0f7994dc5f78fe2e25db6f6038516d242373f1', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 'active', NULL, NULL, NULL, NULL, 64);
INSERT INTO accounts (account_id, name, rut, email, phone, is_company, region, salted_passwd, creation_date, activation_date, status, address, contact, lob, commune, user_id) VALUES (14, '1_foreignip_to_review_other_whitelistqueue@yapo.cl', '', '1_foreignip_to_review_other_whitelistqueue@yapo.cl', '962184762', false, 15, '$1024$:<Gi"_v`q :T80sede66f5addff9234204a778c8ba97aae7ecc7f02b', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 'active', NULL, NULL, NULL, NULL, 65);
INSERT INTO accounts (account_id, name, rut, email, phone, is_company, region, salted_passwd, creation_date, activation_date, status, address, contact, lob, commune, user_id) VALUES (15, '1_withnote_to_review_other_whitelist_queue@yapo.cl', '', '1_withnote_to_review_other_whitelist_queue@yapo.cl', '962273874', false, 15, '$1024$m."%9UKLA(6D8$~Scb837a52ff6b17b5dafeb6c87e4aa6d6ede1e9a1', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 'active', NULL, NULL, NULL, NULL, 66);
INSERT INTO accounts (account_id, name, rut, email, phone, is_company, region, salted_passwd, creation_date, activation_date, status, address, contact, lob, commune, user_id) VALUES (16, '1_under1000_to_review_other_whitelistqueue@yapo.cl', '', '1_under1000_to_review_other_whitelistqueue@yapo.cl', '962273874', false, 15, '$1024$P>%r>.R_%3XS*|zO20749518d79f3dd33b6e599b207d7c9d8add62d6', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 'active', NULL, NULL, NULL, NULL, 67);
INSERT INTO accounts (account_id, name, rut, email, phone, is_company, region, salted_passwd, creation_date, activation_date, status, address, contact, lob, commune, user_id) VALUES (17, 'user_to_reject_ad', '', 'user_to_reject_ad@yapo.cl', '962184762', false, 15, '$1024$hAxe\\hRgdS_pq3$n701b3605f7feb988c2430c5d1e6975f229ed2679', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 'active', NULL, NULL, NULL, NULL, 68);
INSERT INTO accounts (account_id, name, rut, email, phone, is_company, region, salted_passwd, creation_date, activation_date, status, address, contact, lob, commune, user_id) VALUES (18, '1_inserted_keep_whitelist@yapo.cl', '', '1_inserted_keep_whitelist@yapo.cl', '962184762', false, 15, '$1024$GO3P) `|ToBQxb(,e5be7a081673e186fe379dc9bfef6a683b5eca42', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 'active', NULL, NULL, NULL, NULL, 69);
INSERT INTO accounts (account_id, name, rut, email, phone, is_company, region, salted_passwd, creation_date, activation_date, status, address, contact, lob, commune, user_id) VALUES (19, 'user_doesnt_3accepted_in_3m_other_whitelist_queue', '', 'user_doesnt_3accepted_in_3m_other_whitelist_queue@yapo.cl', '962184762', false, 15, '$1024$Iw;uiv-{A{mcVu.0111bb4a9bed80e489944a6b27dba8b70272d79f2', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 'active', NULL, NULL, NULL, NULL, 70);
INSERT INTO accounts (account_id, name, rut, email, phone, is_company, region, salted_passwd, creation_date, activation_date, status, address, contact, lob, commune, user_id) VALUES (20, 'user_to_insert_ad_foreign_ip', '', 'user_to_insert_ad_foreign_ip@yapo.cl', '962184762', false, 15, '$1024$}$W:Nrn8?:o)[q!n07fd7fdcc3cd9455a46d7370965655ea676042dd', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 'active', NULL, NULL, NULL, NULL, 71);
INSERT INTO accounts (account_id, name, rut, email, phone, is_company, region, salted_passwd, creation_date, activation_date, status, address, contact, lob, commune, user_id) VALUES (21, 'user_to_insert_ad_notes', '', 'user_to_insert_ad_notes@yapo.cl', '962184762', false, 15, '$1024$*~5+JoEz[/WvP_5oc9d47ded91449bf26d1cbd93f43d804e022a6cda', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 'active', NULL, NULL, NULL, NULL, 72);
INSERT INTO accounts (account_id, name, rut, email, phone, is_company, region, salted_passwd, creation_date, activation_date, status, address, contact, lob, commune, user_id) VALUES (22, 'user_to_price_under_1000', '', 'user_to_price_under_1000@yapo.cl', '226437660', false, 15, '$1024$S#isc\\I7}d;}[\\''_a8c5f0f274392a65cf60f327dc1df5194de35568', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 'active', NULL, NULL, NULL, NULL, 73);
INSERT INTO accounts (account_id, name, rut, email, phone, is_company, region, salted_passwd, creation_date, activation_date, status, address, contact, lob, commune, user_id) VALUES (23, 'user_to_ad_forbidden_words', '', 'user_to_ad_forbidden_words@yapo.cl', '962184762', false, 15, '$1024$r}J/nEpe.:_EQ(;we95687b92ded99698bf3ce75d7dfa01aba2e12b4', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 'active', NULL, NULL, NULL, NULL, 74);
INSERT INTO accounts (account_id, name, rut, email, phone, is_company, region, salted_passwd, creation_date, activation_date, status, address, contact, lob, commune, user_id) VALUES (24, 'user_to_ad_duplicate_warning', '', 'user_to_ad_duplicate_warning@yapo.cl', '226437660', false, 15, '$1024$J6pj]`&''pg"U*qn@ea9ea4aa891625afb2b207dfd7382965d407bf38', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 'active', NULL, NULL, NULL, NULL, 75);
INSERT INTO accounts (account_id, name, rut, email, phone, is_company, region, salted_passwd, creation_date, activation_date, status, address, contact, lob, commune, user_id) VALUES (25, 'user_with_ad_without_green_price', '', 'user_with_ad_without_green_price@yapo.cl', '226437660', false, 15, '$1024$)PA}}<n%k0QqN3Q34c7317e002c1095bac98650337af16f58f4e27b3', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 'active', NULL, NULL, NULL, NULL, 76);
INSERT INTO accounts (account_id, name, rut, email, phone, is_company, region, salted_passwd, creation_date, activation_date, status, address, contact, lob, commune, user_id) VALUES (26, 'user_with_approved_deleted_ads', '', 'user_with_approved_deleted_ads@yapo.cl', '962184762', false, 15, '$1024$*[``m_q1J"$*EX+;32acd85623ee351db34af9872814c467955903b1', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 'active', NULL, NULL, NULL, NULL, 77);
SELECT pg_catalog.setval('accounts_account_id_seq', 26, true);
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (128, 8000085, CURRENT_TIMESTAMP, 'active', 'sell', 'Border User', '226437660', 15, 0, 5160, 59, NULL, false, false, false, 'Sopapo usado', 'asd', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$$R;x<_@iBEWlZHVY2b5f37e384b6d125f8a3a278dc0f8d6db707d7bf', CURRENT_TIMESTAMP, 'es');
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (137, 8000094, CURRENT_TIMESTAMP, 'active', 'sell', 'user_to_insert_ad_notes', '962184762', 15, 0, 4060, 72, NULL, false, false, false, 'Crema de hombre', 'Exfoliante natural', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$*~5+JoEz[/WvP_5oc9d47ded91449bf26d1cbd93f43d804e022a6cda', CURRENT_TIMESTAMP, 'es');
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (146, 8000103, CURRENT_TIMESTAMP, 'active', 'sell', 'user_to_ad_duplicate_warning', '226437660', 15, 0, 3020, 75, NULL, false, false, false, 'Magnavox oddisey', 'Se va por kilo', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$J6pj]`&''pg"U*qn@ea9ea4aa891625afb2b207dfd7382965d407bf38', CURRENT_TIMESTAMP, 'es');
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (126, 8000087, CURRENT_TIMESTAMP - interval '4 months', 'active', 'sell', 'Border User', '226437660', 15, 0, 5020, 59, NULL, false, false, false, 'Mueble bonito', 'asd', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$$R;x<_@iBEWlZHVY2b5f37e384b6d125f8a3a278dc0f8d6db707d7bf', CURRENT_TIMESTAMP, 'es');
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (136, 8000095, CURRENT_TIMESTAMP, 'active', 'sell', 'user_to_insert_ad_notes', '962184762', 15, 0, 9040, 72, NULL, false, false, false, 'Mu�eca inflable pal diego', 'especial para esas tardes solitarias', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$*~5+JoEz[/WvP_5oc9d47ded91449bf26d1cbd93f43d804e022a6cda', CURRENT_TIMESTAMP, 'es');
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (131, 8000088, CURRENT_TIMESTAMP, 'active', 'sell', 'user_to_reject_ad', '962184762', 15, 0, 5040, 68, NULL, false, false, false, 'Licuadora de piedras', 'Ultra resistente, muele hasta diamantes', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$hAxe\\hRgdS_pq3$n701b3605f7feb988c2430c5d1e6975f229ed2679', CURRENT_TIMESTAMP, 'es');
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (143, 8000100, CURRENT_TIMESTAMP, 'active', 'sell', 'user_to_ad_forbidden_words', '962184762', 15, 0, 6180, 74, NULL, false, false, false, 'Picoyo para hacer fuego', 'Se incluye un pedernal', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$r}J/nEpe.:_EQ(;we95687b92ded99698bf3ce75d7dfa01aba2e12b4', CURRENT_TIMESTAMP, 'es');
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (130, 8000089, CURRENT_TIMESTAMP, 'active', 'sell', 'user_to_reject_ad', '962184762', 15, 0, 5060, 68, NULL, false, false, false, 'Manguera de manguaco', 'Es buena para regar', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$hAxe\\hRgdS_pq3$n701b3605f7feb988c2430c5d1e6975f229ed2679', CURRENT_TIMESTAMP, 'es');
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (135, 8000096, CURRENT_TIMESTAMP, 'active', 'sell', 'user_to_insert_ad_notes', '962184762', 15, 0, 6140, 72, NULL, false, false, false, 'Gatito con pulgas', 'precio por kilo', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$*~5+JoEz[/WvP_5oc9d47ded91449bf26d1cbd93f43d804e022a6cda', CURRENT_TIMESTAMP, 'es');
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (129, NULL, NULL, 'refused', 'sell', 'user_to_reject_ad', '962184762', 15, 0, 5020, 68, NULL, false, false, false, 'Mesa para cortar', 'Sirve para cortar muchas cosas, tiene poco uso', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$hAxe\\hRgdS_pq3$n701b3605f7feb988c2430c5d1e6975f229ed2679', NULL, 'es');
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (134, 8000091, CURRENT_TIMESTAMP, 'active', 'sell', 'user_to_insert_ad_foreign_ip', '962184762', 15, 0, 9040, 71, NULL, false, false, false, 'Mu�eco de Alf', 'Alf volvio!! en forma de mu�eco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$}$W:Nrn8?:o)[q!n07fd7fdcc3cd9455a46d7370965655ea676042dd', CURRENT_TIMESTAMP, 'es');
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (133, 8000092, CURRENT_TIMESTAMP, 'active', 'sell', 'user_to_insert_ad_foreign_ip', '962184762', 15, 0, 5060, 71, NULL, false, false, false, 'Manguera de manguaco', 'Permite maguerear cosas', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$}$W:Nrn8?:o)[q!n07fd7fdcc3cd9455a46d7370965655ea676042dd', CURRENT_TIMESTAMP, 'es');
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (132, 8000093, CURRENT_TIMESTAMP, 'active', 'sell', 'user_to_insert_ad_foreign_ip', '962184762', 15, 0, 5020, 71, NULL, false, false, false, 'Licuadora de piedras', 'Permite licuar varias cosas, como pi�ones', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$}$W:Nrn8?:o)[q!n07fd7fdcc3cd9455a46d7370965655ea676042dd', CURRENT_TIMESTAMP, 'es');
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (140, 8000097, CURRENT_TIMESTAMP, 'active', 'sell', 'user_to_price_under_1000', '226437660', 15, 0, 4040, 73, NULL, false, false, false, 'Bolse de cuero de tiburon', 'permite afilar cuchillos y navajas', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$S#isc\\I7}d;}[\\''_a8c5f0f274392a65cf60f327dc1df5194de35568', CURRENT_TIMESTAMP, 'es');
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (142, 8000101, CURRENT_TIMESTAMP, 'active', 'sell', 'user_to_ad_forbidden_words', '962184762', 15, 0, 6120, 74, NULL, false, false, false, 'Condorito de oro', 'los mejores chistes del pajarraco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$r}J/nEpe.:_EQ(;we95687b92ded99698bf3ce75d7dfa01aba2e12b4', CURRENT_TIMESTAMP, 'es');
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (139, 8000098, CURRENT_TIMESTAMP, 'active', 'sell', 'user_to_price_under_1000', '226437660', 15, 0, 9020, 73, NULL, false, false, false, 'Faja para embarazo', 'util para evitar desbordamientos', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$S#isc\\I7}d;}[\\''_a8c5f0f274392a65cf60f327dc1df5194de35568', CURRENT_TIMESTAMP, 'es');
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (145, 8000104, CURRENT_TIMESTAMP, 'active', 'sell', 'user_to_ad_duplicate_warning', '226437660', 15, 0, 5020, 75, NULL, false, false, false, 'Silla de descanso', 'para reposar la raja', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$J6pj]`&''pg"U*qn@ea9ea4aa891625afb2b207dfd7382965d407bf38', CURRENT_TIMESTAMP, 'es');
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (138, 8000099, CURRENT_TIMESTAMP, 'active', 'sell', 'user_to_price_under_1000', '226437660', 15, 0, 5160, 73, NULL, false, false, false, 'Rastrillo', 'especial para el oto�o', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$S#isc\\I7}d;}[\\''_a8c5f0f274392a65cf60f327dc1df5194de35568', CURRENT_TIMESTAMP, 'es');
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (141, 8000102, CURRENT_TIMESTAMP, 'active', 'sell', 'user_to_ad_forbidden_words', '962184762', 15, 0, 3060, 74, NULL, false, false, false, 'One Plus One', 'exclusivo telefono para gente con dinero', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$r}J/nEpe.:_EQ(;we95687b92ded99698bf3ce75d7dfa01aba2e12b4', CURRENT_TIMESTAMP, 'es');
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (148, 8000107, CURRENT_TIMESTAMP, 'active', 'sell', 'user_with_ad_without_green_price', '226437660', 15, 0, 6020, 76, NULL, false, false, false, 'Pelota de pilates', 'Permite realizar ejecicios chinos', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$)PA}}<n%k0QqN3Q34c7317e002c1095bac98650337af16f58f4e27b3', CURRENT_TIMESTAMP, 'es');
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (144, 8000105, CURRENT_TIMESTAMP, 'active', 'sell', 'user_to_ad_duplicate_warning', '226437660', 15, 0, 6020, 75, NULL, false, false, false, 'Force ball', 'para ejercicios de fap', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$J6pj]`&''pg"U*qn@ea9ea4aa891625afb2b207dfd7382965d407bf38', CURRENT_TIMESTAMP, 'es');
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (149, 8000106, CURRENT_TIMESTAMP, 'active', 'sell', 'user_with_ad_without_green_price', '226437660', 15, 0, 9060, 76, NULL, false, false, false, 'Palanca de cambio para bebe', 'desarrolla facultados ideales para un uturo camionero', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$)PA}}<n%k0QqN3Q34c7317e002c1095bac98650337af16f58f4e27b3', CURRENT_TIMESTAMP, 'es');
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (147, 8000108, CURRENT_TIMESTAMP, 'active', 'sell', 'user_with_ad_without_green_price', '226437660', 15, 0, 4040, 76, NULL, false, false, false, 'Aros de almeja', 'ideales para tener las orejas pasadas a marisco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$)PA}}<n%k0QqN3Q34c7317e002c1095bac98650337af16f58f4e27b3', CURRENT_TIMESTAMP, 'es');
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (152, 8000109, CURRENT_TIMESTAMP, 'deleted', 'sell', 'user_with_approved_deleted_ads', '962184762', 15, 0, 5020, 77, NULL, false, false, false, 'Estante de libros', 'es para guadar libros', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$*[``m_q1J"$*EX+;32acd85623ee351db34af9872814c467955903b1', CURRENT_TIMESTAMP, 'es');
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (150, 8000111, CURRENT_TIMESTAMP, 'active', 'sell', 'user_with_approved_deleted_ads', '962184762', 15, 0, 5040, 77, NULL, false, false, false, 'Microondas de pollitos', 'permite comer frescos pollitos rostizados', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$*[``m_q1J"$*EX+;32acd85623ee351db34af9872814c467955903b1', CURRENT_TIMESTAMP, 'es');
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (151, 8000110, CURRENT_TIMESTAMP, 'deleted', 'sell', 'user_with_approved_deleted_ads', '962184762', 15, 0, 5060, 77, NULL, false, false, false, 'Pistola de rigo avanzado', 'cualquier cosa', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$*[``m_q1J"$*EX+;32acd85623ee351db34af9872814c467955903b1', CURRENT_TIMESTAMP, 'es');
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (166, '633026231', 'verified', CURRENT_TIMESTAMP, NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (167, '768418254', 'verified', CURRENT_TIMESTAMP, NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (168, '903810277', 'verified', CURRENT_TIMESTAMP, NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (169, '139202300', 'verified', CURRENT_TIMESTAMP, NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (170, '274594323', 'verified', CURRENT_TIMESTAMP, NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (171, '409986346', 'verified', CURRENT_TIMESTAMP, NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (172, '545378369', 'verified', CURRENT_TIMESTAMP, NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (173, '680770392', 'verified', CURRENT_TIMESTAMP, NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (174, '816162415', 'verified', CURRENT_TIMESTAMP, NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (175, '951554438', 'verified', CURRENT_TIMESTAMP, NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (176, '186946461', 'verified', CURRENT_TIMESTAMP, NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (177, '322338484', 'verified', CURRENT_TIMESTAMP, NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (178, '457730507', 'verified', CURRENT_TIMESTAMP, NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (179, '593122530', 'verified', CURRENT_TIMESTAMP, NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (180, '728514553', 'verified', CURRENT_TIMESTAMP, NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (181, '863906576', 'verified', CURRENT_TIMESTAMP, NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (182, '999298599', 'verified', CURRENT_TIMESTAMP, NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (183, '234690622', 'verified', CURRENT_TIMESTAMP, NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (184, '370082645', 'verified', CURRENT_TIMESTAMP, NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (185, '505474668', 'verified', CURRENT_TIMESTAMP, NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (186, '640866691', 'verified', CURRENT_TIMESTAMP, NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (187, '776258714', 'verified', CURRENT_TIMESTAMP, NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (188, '911650737', 'verified', CURRENT_TIMESTAMP, NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (189, '147042760', 'verified', CURRENT_TIMESTAMP, NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (190, '282434783', 'verified', CURRENT_TIMESTAMP, NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (191, '417826806', 'verified', CURRENT_TIMESTAMP, NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (192, '553218829', 'verified', CURRENT_TIMESTAMP, NULL);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (128, 1, 'new', 707, 'accepted', 'normal', NULL, NULL, 168);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (126, 1, 'new', 709, 'accepted', 'normal', NULL, NULL, 166);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (131, 1, 'new', 719, 'accepted', 'normal', NULL, NULL, 171);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (130, 1, 'new', 720, 'accepted', 'normal', NULL, NULL, 170);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (129, 1, 'new', 721, 'refused', 'normal', NULL, NULL, 169);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (140, 1, 'new', 755, 'accepted', 'normal', NULL, NULL, 180);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (139, 1, 'new', 756, 'accepted', 'normal', NULL, NULL, 179);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (138, 1, 'new', 757, 'accepted', 'normal', NULL, NULL, 178);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (134, 1, 'new', 731, 'accepted', 'normal', NULL, NULL, 174);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (133, 1, 'new', 732, 'accepted', 'normal', NULL, NULL, 173);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (132, 1, 'new', 733, 'accepted', 'normal', NULL, NULL, 172);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (149, 1, 'new', 791, 'accepted', 'normal', NULL, NULL, 189);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (137, 1, 'new', 743, 'accepted', 'normal', NULL, NULL, 177);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (136, 1, 'new', 744, 'accepted', 'normal', NULL, NULL, 176);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (135, 1, 'new', 745, 'accepted', 'normal', NULL, NULL, 175);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (148, 1, 'new', 792, 'accepted', 'normal', NULL, NULL, 188);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (147, 1, 'new', 793, 'accepted', 'normal', NULL, NULL, 187);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (143, 1, 'new', 767, 'accepted', 'normal', NULL, NULL, 183);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (142, 1, 'new', 768, 'accepted', 'normal', NULL, NULL, 182);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (141, 1, 'new', 769, 'accepted', 'normal', NULL, NULL, 181);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (146, 1, 'new', 779, 'accepted', 'normal', NULL, NULL, 186);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (145, 1, 'new', 780, 'accepted', 'normal', NULL, NULL, 185);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (144, 1, 'new', 781, 'accepted', 'normal', NULL, NULL, 184);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (152, 1, 'new', 803, 'accepted', 'normal', NULL, NULL, 192);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (151, 1, 'new', 804, 'accepted', 'normal', NULL, NULL, 191);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (150, 1, 'new', 805, 'accepted', 'normal', NULL, NULL, 190);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (152, 2, 'delete', 806, 'deleted', 'normal', NULL, NULL, NULL);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (151, 2, 'delete', 807, 'deleted', 'normal', NULL, NULL, NULL);
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (126, 1, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (126, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (128, 1, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (128, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (129, 1, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (129, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (130, 1, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (130, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (131, 1, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (131, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (132, 1, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (132, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (133, 1, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (133, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (134, 1, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (134, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (135, 1, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (135, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (136, 1, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (136, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (137, 1, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (137, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (138, 1, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (138, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (139, 1, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (139, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (140, 1, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (140, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (141, 1, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (141, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (142, 1, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (142, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (143, 1, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (143, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (144, 1, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (144, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (145, 1, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (145, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (146, 1, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (146, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (147, 1, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (147, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (148, 1, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (148, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (149, 1, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (149, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (150, 1, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (150, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (151, 1, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (151, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (152, 1, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (152, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (152, 2, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (152, 2, 'deletion_reason', '1');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (152, 2, 'deletion_timer', '1');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (152, 2, 'event_view', '0');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (152, 2, 'event_mail', '0');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (152, 2, 'event_galleryview', '0');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (151, 2, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (151, 2, 'deletion_reason', '1');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (151, 2, 'deletion_timer', '2');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (151, 2, 'event_view', '0');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (151, 2, 'event_mail', '0');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (151, 2, 'event_galleryview', '0');
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (715, 'X5523de807b58bb6200000000121b303400000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'authenticate', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (716, 'X5523de804c51b0e6000000006b43a6b500000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'spamfilter', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (717, 'X5523de8264f74b20000000005ce97a00000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'listadmins', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (718, 'X5523de8e424a630000000000344ba51e00000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'listadmins', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (719, 'X5523de8e21c42d1e000000006db22cf400000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'search_ads', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (720, 'X5523de92795e04c100000000525abde100000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (721, 'X5523de925af83f380000000064d4ce1900000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (722, 'X5523de922ef28b2300000000298294400000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (723, 'X5523de926c19b6aa000000001ef394f000000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (724, 'X5523de9314373ebf000000006b29243e00000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (725, 'X5523de93498626cb0000000072a1870b00000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (726, 'X5523de937daacfe8000000005283ab7100000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (727, 'X5523de9360303d4f0000000038dda79500000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (728, 'X5523de955d4f16210000000016344c6300000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (729, 'X5523de955395a98b0000000050f09ced00000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (730, 'X5523de9568cb445200000000632c9e0a00000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (731, 'X5523de9537fffd6e0000000053e3d5f100000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (732, 'X5523deac156214c7000000003cba348b00000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'review', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (733, 'X5523deb21f85b5de000000004251060000000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'review', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (734, 'X5523deb8737e8c1b0000000025dd51100000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'review', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (735, 'X5523dece60f2dbd8000000003ee2546500000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'listadmins', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (736, 'X5523dece60af277500000000c1955bb00000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'search_ads', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (737, 'X5523e42514d7a609000000006a3cd3b700000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'listadmins', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (738, 'X5523e4258eea68700000000770dd9300000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'search_ads', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (739, 'X5523e42a5beaaf41000000002ab0525800000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (740, 'X5523e42a418841e0000000091c7c1e00000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (741, 'X5523e42a53198b2c000000005fd7473b00000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (742, 'X5523e42a36710f61000000006d31bbd000000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (743, 'X5523e42f465ab6610000000059dd031000000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'review', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (744, 'X5523e43349e713cb000000004aacb1dc00000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (745, 'X5523e43322bbe6e8000000004d8e800100000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (746, 'X5523e433490a96180000000074895f000000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (747, 'X5523e43353d9e9600000000058b7859600000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (748, 'X5523e4381570b1ef000000003fc0412100000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'review', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (749, 'X5523e43ed85bb36000000007eeda2fc00000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (750, 'X5523e43e5126a8bf00000000658a274c00000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (751, 'X5523e43e33e5e2fb000000005b76163600000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (752, 'X5523e43e3a15f721000000003d0cb8e00000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (753, 'X5523e44230ea36f60000000027a89db700000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'review', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (754, 'X5523e44666f49f920000000024e1969100000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'listadmins', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (755, 'X5523e446ccfdd3e00000000522d5ce200000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'search_ads', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (756, 'X5523e4803200bdf80000000027467df100000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.164', 'authenticate', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (757, 'X5523e4805b232a7f0000000031f99cf400000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.164', 'spamfilter', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (758, 'X5523e48a6e953ad10000000079a70f5500000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.164', 'spamfilter', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (759, 'X5523e4bb3f814939000000003748fad800000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.164', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (760, 'X5523e4c6fab804a000000007127ffe100000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.164', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (761, 'X5523e4f74cfd9ee2000000005095234800000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.164', 'blocked_item', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (762, 'X5523e4f755e74bf2000000001965d52f00000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.164', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (763, 'X5523e50334c087fd0000000076ae1c4e00000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.164', 'blocked_item', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (764, 'X5523e5036e62a505000000005947e09e00000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.164', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (765, 'X5523e5084f25413b00000000bf2edcb00000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.164', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (766, 'X5523e50821096730000000001168bc8100000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.164', 'blocked_item', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (767, 'X5523e5185e665b630000000066c8b0d300000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.164', 'blocked_item', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (768, 'X5523e5182bd46ac4000000003daa78e700000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.164', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (769, 'X5523e51b47bc354b000000001bbb6b600000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.164', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (770, 'X5523e51b57104e16000000005ba3e3b900000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.164', 'blocked_item', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (771, 'X5523e52a132723d6000000005f292f9300000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.164', 'blocked_item', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (772, 'X5523e52a7e89809c0000000047e7abd300000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.164', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (773, 'X5523e52d55d74be1000000001b162cd500000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.164', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (774, 'X5523e52d4f6e0aa6000000006f9de34600000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.164', 'blocked_item', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (775, 'X5523e53c6da74b8b000000003dd0afab00000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.164', 'blocked_item', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (776, 'X5523e53c48e5c3e4000000007781651900000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.164', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (777, 'X5523e53e3dd0f71c0000000035436f3600000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.164', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (778, 'X5523e53e46a6a6540000000049c3e4e700000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.164', 'blocked_item', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (779, 'X5523e54d314c9ad3000000006f3e77100000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.164', 'blocked_item', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (780, 'X5523e54d6acd4c170000000042b5575400000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.164', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (781, 'X5523e5501d2774fc0000000053d0886700000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.164', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (782, 'X5523e550211bb2b7000000003f025cf00000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.164', 'blocked_item', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (783, 'X5523e55f749d4f57000000002623748400000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.164', 'blocked_item', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (784, 'X5523e55f6d8a17e500000000397696a00000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.164', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (785, 'X5523e56157766b11000000007289a3900000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.164', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (786, 'X5523e5619cd67e0000000003c081a2600000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.164', 'blocked_item', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (787, 'X5523e5715a91783e0000000067aafdd400000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.164', 'blocked_item', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (788, 'X5523e5712aaa4d150000000012ea769500000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.164', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (789, 'X5523e573ca6b8db000000003b90115b00000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.164', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (790, 'X5523e5733cb7a422000000007cde839f00000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.164', 'blocked_item', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (791, 'X5523e583534f50480000000037889a1e00000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.164', 'blocked_item', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (792, 'X5523e5835e140b4500000000f892fc600000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.164', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (793, 'X5523e585102b906e000000007830b88300000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.164', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (794, 'X5523e5851abb2e560000000054dac58b00000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.164', 'blocked_item', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (795, 'X5523e59958eee6b1000000006322861000000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.164', 'blocked_item', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (796, 'X5523e599232c7e27000000006dc8e3b00000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.164', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (797, 'X5523e59c12e8970d00000000d9010d400000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.164', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (798, 'X5523e59c3385a79a00000000785e66400000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.164', 'blocked_item', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (799, 'X5523e6825e40969b000000008d429e500000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'authenticate', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (800, 'X5523e68242b6c5960000000054a4858700000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'spamfilter', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (801, 'X5523e68421d780e0000000042d03d1400000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'listadmins', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (802, 'X5523e6895258bc060000000028d0230e00000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'listadmins', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (803, 'X5523e68968aef881000000006bff425300000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'search_ads', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (804, 'X5523e68d53d03a220000000028d93dc100000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (805, 'X5523e68d41d96344000000003d67d12800000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (806, 'X5523e68d1c1316b0000000006e004a5e00000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (807, 'X5523e68d7a7dda29000000006d24945a00000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (808, 'X5523e68e56c09ede000000005f26be5100000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (809, 'X5523e68e300b263000000005479790200000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (810, 'X5523e68e4ad6a0ae000000003931729300000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (811, 'X5523e68e3b7e922a00000000327587fd00000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (812, 'X5523e68f1ebb2a47000000005bb24d6b00000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (813, 'X5523e68f61c52190000000066c88a8700000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (814, 'X5523e68f1d71956800000000645ce8b400000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (815, 'X5523e68f6f9cb46c0000000060285aff00000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (816, 'X5523e69439016e3c0000000071ba2c7a00000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'review', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (817, 'X5523e6b145eb8423000000005d2e392b00000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'review', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (818, 'X5523e6b722f8981300000000b5a2a4200000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'review', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (819, 'X5523e7b6f9c215a000000001ba81adb00000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'listadmins', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (820, 'X5523e7b640a37378000000004864ae8600000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'search_ads', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (821, 'X5523e7ba701cb4030000000070c49c6c00000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (822, 'X5523e7ba2065567700000000649d2f900000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (823, 'X5523e7ba7ee21b30000000093a8bde00000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (824, 'X5523e7ba26a7d9a5000000003db0d8e600000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (825, 'X5523e7bb6e9d3ad70000000095d59300000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (826, 'X5523e7bb7f56515f000000002a29bcd200000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (827, 'X5523e7bb6cce7f1b000000005d44aac400000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (828, 'X5523e7bb4642aebc000000006523071200000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (829, 'X5523e7bc1317a7bd0000000028ccdccd00000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (830, 'X5523e7bc4cb2a23700000000679a875800000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (831, 'X5523e7bc7736a8b300000000234926f300000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (832, 'X5523e7bc1365daf6000000007a6fd200000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (833, 'X5523e7c0868918500000000693b398e00000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'review', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (834, 'X5523e7c5395a6df9000000005343385300000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'review', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (835, 'X5523e7c957d874660000000040ed5d2800000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'review', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (836, 'X5523e7cd4b205a640000000056c177ce00000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'listadmins', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (837, 'X5523e7cd61a29220000000006bcaba2f00000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'search_ads', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (838, 'X5523e8961090d2a200000000703f5aa300000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'listadmins', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (839, 'X5523e896858c754000000007f1bf53600000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'search_ads', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (840, 'X5523e89a61130e42000000004abe3cba00000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (841, 'X5523e89ac5763ba000000007f15594200000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (842, 'X5523e89a75c72589000000007d8ca18b00000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (843, 'X5523e89a545056900000000044177b3600000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (844, 'X5523e89b7e01d2e900000000506356c200000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (845, 'X5523e89b437b1d5a0000000059808fd000000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (846, 'X5523e89b49bfea74000000004b74edf900000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (847, 'X5523e89b4a90e330000000005f0a06b700000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (848, 'X5523e89c29c8437100000000d81dff400000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (849, 'X5523e89cc973937000000006d77cfd400000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (850, 'X5523e89c6b04715f00000000482790d000000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (851, 'X5523e89cc3f0c06000000007d69097800000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (852, 'X5523e8a14dcc603a00000000da34dec00000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'review', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (853, 'X5523e8a6d6cb70d000000002646348200000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'review', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (854, 'X5523e8ab6723ddbc000000003481079700000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'review', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (855, 'X5523e8af262dea9a0000000014afb8dd00000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'listadmins', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (856, 'X5523e8af4c5b05d700000000318c82d00000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'search_ads', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (857, 'X5523e965452bb84f00000000663df36f00000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'listadmins', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (858, 'X5523e9653b936c840000000033dfeaf00000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'search_ads', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (859, 'X5523e98b30a82f860000000043d3f0d200000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'listadmins', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (860, 'X5523e98b7dd6ba73000000007f2d659000000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'search_ads', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (861, 'X5523e98e52709de30000000039f0333200000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (862, 'X5523e98e10b1ab000000000069fb60c400000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (863, 'X5523e98e55c61af40000000046b80bd700000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (864, 'X5523e98e6cfe4059000000001afb490200000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (865, 'X5523e98f2fab01df0000000045295bc000000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (866, 'X5523e98f484223ed000000001105b7ef00000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (867, 'X5523e98f78ea53730000000054d9a8c100000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (868, 'X5523e98f3761bae8000000004858f63700000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (869, 'X5523e99052b0633500000000368f207800000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (870, 'X5523e99011ad54ff000000009e18ba100000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (871, 'X5523e9908ffbe5b000000004b9d883100000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (872, 'X5523e990650c90eb000000002690b6a300000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (873, 'X5523e994e7bd2e1000000004ea3705200000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'review', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (874, 'X5523e99945223b10000000001abdafd700000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'review', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (875, 'X5523e99e29f25ac200000000800dea500000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'review', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (876, 'X5523e9a23bb7f303000000006c336a0000000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'listadmins', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (877, 'X5523e9a2391692e4000000003235fa4a00000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'search_ads', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (878, 'X5523eaa841f42ba10000000074ab99fa00000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'listadmins', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (879, 'X5523eaa84b4ae24d000000006c750bec00000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'search_ads', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (880, 'X5523eaad2fa197ac00000000751409ac00000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (881, 'X5523eaad61a2ade900000000af0b87f00000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (882, 'X5523eaad287c29ec0000000044f9155600000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (883, 'X5523eaadced3aac0000000056068a9700000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (884, 'X5523eaae4f2a0308000000007b070f1c00000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (885, 'X5523eaae57802a0b000000004f97db1300000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (886, 'X5523eaae2b32bc840000000055ef0b5b00000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (887, 'X5523eaae56a25dd00000000616939fa00000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (888, 'X5523eaae3f79cfbd000000005959a3f800000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (889, 'X5523eaaf21c3ab800000000015dfc12300000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (890, 'X5523eaaf5e5bca37000000004c063bbc00000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (891, 'X5523eaaf5e1a60ca00000000739defd700000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (892, 'X5523eab3172e933700000000513fad7b00000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'review', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (893, 'X5523eab85898ecb800000000442ca94800000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'review', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (894, 'X5523ead05a74d7ea0000000043902d7500000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'review', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (895, 'X5523ead56b104813000000006159c52c00000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'listadmins', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (896, 'X5523ead519192a80000000052796ecf00000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'search_ads', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (897, 'X5523ebd3623b95b3000000004281dcc200000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'listadmins', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (898, 'X5523ebd36b04196a00000000739a00900000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'search_ads', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (899, 'X5523ebd610f77926000000001a90519d00000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (900, 'X5523ebd63a79408b000000003e7018800000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (901, 'X5523ebd645438faf0000000046f22b0500000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (902, 'X5523ebd65fff56ca00000000572809bf00000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (903, 'X5523ebd7249502880000000096bd89100000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (904, 'X5523ebd72b96d430000000002f60da5f00000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (905, 'X5523ebd76cb0d7a7000000002473c3af00000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (906, 'X5523ebd76f8434b2000000007b4da1aa00000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (907, 'X5523ebd87f49292d00000000763a8b4d00000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (908, 'X5523ebd8667f2d5d00000000239a6c8a00000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (909, 'X5523ebd87e9f7528000000001ed223ba00000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (910, 'X5523ebd8589d263c000000006e6c2e6a00000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (911, 'X5523ebdc3482740a0000000065aca8e200000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'review', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (912, 'X5523ebe033145940000000006650bff200000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'review', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (913, 'X5523ebe87d41b44400000000460b2d4e00000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'review', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (914, 'X5523ebf17cce30eb00000000203b3aa000000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'listadmins', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (915, 'X5523ebf14e73fa680000000011a4334300000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'search_ads', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (916, 'X5523ece12a0105210000000050ebeee200000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'listadmins', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (917, 'X5523ece177781ac90000000024d1485a00000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'search_ads', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (918, 'X5523ece675978cf90000000053fe727c00000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (919, 'X5523ece63616c4030000000043504ec600000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (920, 'X5523ece64e6159f9000000003be399500000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (921, 'X5523ece63baf8bc3000000004972c6a00000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (922, 'X5523ece77b6f020700000000216e975600000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (923, 'X5523ece763f549d000000000410f9e7d00000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (924, 'X5523ece72b79417f00000000144b696700000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (925, 'X5523ece725dd40070000000050005cc600000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (926, 'X5523ece8135ee5f700000000172d817b00000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (927, 'X5523ece83a2990f0000000002ebb6d8a00000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (928, 'X5523ece8502a04e00000000030387b400000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (929, 'X5523ece8534c0ac700000000a9d944500000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (930, 'X5523ecee37ba3bd2000000003ae65a4600000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'review', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (931, 'X5523ecf27ff8bfa3000000007542f1fb00000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'review', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (932, 'X5523ecf74f31c3ad0000000066f621da00000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'review', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (1007, 'X5524367e61ee34c100000000336b6d6aa0000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.164', 'blocked_item', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (1008, 'X5524367e61ee34c100000000336b6d6ab0000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.164', 'blocked_item', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (1009, 'X5524367e61ee34c100000000336b6d6ac0000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.164', 'blocked_item', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (1010, 'X5524367e61ee34c100000000336b6d6ad0000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.164', 'blocked_item', NULL, NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (126, 1, 698, 'reg', 'initial', CURRENT_TIMESTAMP, '10.0.1.198', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (126, 1, 699, 'unverified', 'verifymail', CURRENT_TIMESTAMP, '10.0.1.198', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (126, 1, 700, 'pending_review', 'verify', CURRENT_TIMESTAMP, '10.0.1.198', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (128, 1, 704, 'reg', 'initial', CURRENT_TIMESTAMP, '10.0.1.198', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (128, 1, 705, 'unverified', 'verifymail', CURRENT_TIMESTAMP, '10.0.1.198', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (128, 1, 706, 'pending_review', 'verify', CURRENT_TIMESTAMP, '10.0.1.198', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (128, 1, 707, 'accepted', 'accept', CURRENT_TIMESTAMP, '10.0.1.198', 731);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (126, 1, 709, 'accepted', 'accept', CURRENT_TIMESTAMP, '10.0.1.198', 733);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (129, 1, 710, 'reg', 'initial', CURRENT_TIMESTAMP, '10.0.1.198', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (129, 1, 711, 'unverified', 'verifymail', CURRENT_TIMESTAMP, '10.0.1.198', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (129, 1, 712, 'pending_review', 'verify', CURRENT_TIMESTAMP, '10.0.1.198', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (130, 1, 713, 'reg', 'initial', CURRENT_TIMESTAMP, '10.0.1.198', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (130, 1, 714, 'unverified', 'verifymail', CURRENT_TIMESTAMP, '10.0.1.198', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (130, 1, 715, 'pending_review', 'verify', CURRENT_TIMESTAMP, '10.0.1.198', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (131, 1, 716, 'reg', 'initial', CURRENT_TIMESTAMP, '10.0.1.198', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (131, 1, 717, 'unverified', 'verifymail', CURRENT_TIMESTAMP, '10.0.1.198', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (131, 1, 718, 'pending_review', 'verify', CURRENT_TIMESTAMP, '10.0.1.198', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (131, 1, 719, 'accepted', 'accept', CURRENT_TIMESTAMP, '10.0.1.198', 742);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (130, 1, 720, 'accepted', 'accept', CURRENT_TIMESTAMP, '10.0.1.198', 747);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (129, 1, 721, 'refused', 'refuse', CURRENT_TIMESTAMP, '10.0.1.198', 752);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (132, 1, 722, 'reg', 'initial', CURRENT_TIMESTAMP, '10.0.1.198', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (132, 1, 723, 'unverified', 'verifymail', CURRENT_TIMESTAMP, '10.0.1.198', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (132, 1, 724, 'pending_review', 'verify', CURRENT_TIMESTAMP, '10.0.1.198', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (133, 1, 725, 'reg', 'initial', CURRENT_TIMESTAMP, '10.0.1.198', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (133, 1, 726, 'unverified', 'verifymail', CURRENT_TIMESTAMP, '10.0.1.198', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (133, 1, 727, 'pending_review', 'verify', CURRENT_TIMESTAMP, '10.0.1.198', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (134, 1, 728, 'reg', 'initial', CURRENT_TIMESTAMP, '10.0.1.198', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (134, 1, 729, 'unverified', 'verifymail', CURRENT_TIMESTAMP, '10.0.1.198', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (134, 1, 730, 'pending_review', 'verify', CURRENT_TIMESTAMP, '10.0.1.198', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (134, 1, 731, 'accepted', 'accept', CURRENT_TIMESTAMP, '10.0.1.198', 815);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (133, 1, 732, 'accepted', 'accept', CURRENT_TIMESTAMP, '10.0.1.198', 816);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (132, 1, 733, 'accepted', 'accept', CURRENT_TIMESTAMP, '10.0.1.198', 817);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (135, 1, 734, 'reg', 'initial', CURRENT_TIMESTAMP, '10.0.1.198', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (135, 1, 735, 'unverified', 'verifymail', CURRENT_TIMESTAMP, '10.0.1.198', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (135, 1, 736, 'pending_review', 'verify', CURRENT_TIMESTAMP, '10.0.1.198', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (136, 1, 737, 'reg', 'initial', CURRENT_TIMESTAMP, '10.0.1.198', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (136, 1, 738, 'unverified', 'verifymail', CURRENT_TIMESTAMP, '10.0.1.198', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (136, 1, 739, 'pending_review', 'verify', CURRENT_TIMESTAMP, '10.0.1.198', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (137, 1, 740, 'reg', 'initial', CURRENT_TIMESTAMP, '10.0.1.198', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (137, 1, 741, 'unverified', 'verifymail', CURRENT_TIMESTAMP, '10.0.1.198', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (137, 1, 742, 'pending_review', 'verify', CURRENT_TIMESTAMP, '10.0.1.198', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (137, 1, 743, 'accepted', 'accept', CURRENT_TIMESTAMP, '10.0.1.198', 832);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (136, 1, 744, 'accepted', 'accept', CURRENT_TIMESTAMP, '10.0.1.198', 833);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (135, 1, 745, 'accepted', 'accept', CURRENT_TIMESTAMP, '10.0.1.198', 834);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (138, 1, 746, 'reg', 'initial', CURRENT_TIMESTAMP, '10.0.1.198', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (138, 1, 747, 'unverified', 'verifymail', CURRENT_TIMESTAMP, '10.0.1.198', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (138, 1, 748, 'pending_review', 'verify', CURRENT_TIMESTAMP, '10.0.1.198', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (139, 1, 749, 'reg', 'initial', CURRENT_TIMESTAMP, '10.0.1.198', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (139, 1, 750, 'unverified', 'verifymail', CURRENT_TIMESTAMP, '10.0.1.198', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (139, 1, 751, 'pending_review', 'verify', CURRENT_TIMESTAMP, '10.0.1.198', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (140, 1, 752, 'reg', 'initial', CURRENT_TIMESTAMP, '10.0.1.198', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (140, 1, 753, 'unverified', 'verifymail', CURRENT_TIMESTAMP, '10.0.1.198', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (140, 1, 754, 'pending_review', 'verify', CURRENT_TIMESTAMP, '10.0.1.198', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (140, 1, 755, 'accepted', 'accept', CURRENT_TIMESTAMP, '10.0.1.198', 851);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (139, 1, 756, 'accepted', 'accept', CURRENT_TIMESTAMP, '10.0.1.198', 852);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (138, 1, 757, 'accepted', 'accept', CURRENT_TIMESTAMP, '10.0.1.198', 853);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (141, 1, 758, 'reg', 'initial', CURRENT_TIMESTAMP, '10.0.1.198', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (141, 1, 759, 'unverified', 'verifymail', CURRENT_TIMESTAMP, '10.0.1.198', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (141, 1, 760, 'pending_review', 'verify', CURRENT_TIMESTAMP, '10.0.1.198', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (142, 1, 761, 'reg', 'initial', CURRENT_TIMESTAMP, '10.0.1.198', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (142, 1, 762, 'unverified', 'verifymail', CURRENT_TIMESTAMP, '10.0.1.198', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (142, 1, 763, 'pending_review', 'verify', CURRENT_TIMESTAMP, '10.0.1.198', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (143, 1, 764, 'reg', 'initial', CURRENT_TIMESTAMP, '10.0.1.198', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (143, 1, 765, 'unverified', 'verifymail', CURRENT_TIMESTAMP, '10.0.1.198', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (143, 1, 766, 'pending_review', 'verify', CURRENT_TIMESTAMP, '10.0.1.198', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (143, 1, 767, 'accepted', 'accept', CURRENT_TIMESTAMP, '10.0.1.198', 872);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (142, 1, 768, 'accepted', 'accept', CURRENT_TIMESTAMP, '10.0.1.198', 873);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (141, 1, 769, 'accepted', 'accept', CURRENT_TIMESTAMP, '10.0.1.198', 874);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (144, 1, 770, 'reg', 'initial', CURRENT_TIMESTAMP, '10.0.1.198', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (144, 1, 771, 'unverified', 'verifymail', CURRENT_TIMESTAMP, '10.0.1.198', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (144, 1, 772, 'pending_review', 'verify', CURRENT_TIMESTAMP, '10.0.1.198', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (145, 1, 773, 'reg', 'initial', CURRENT_TIMESTAMP, '10.0.1.198', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (145, 1, 774, 'unverified', 'verifymail', CURRENT_TIMESTAMP, '10.0.1.198', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (145, 1, 775, 'pending_review', 'verify', CURRENT_TIMESTAMP, '10.0.1.198', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (146, 1, 776, 'reg', 'initial', CURRENT_TIMESTAMP, '10.0.1.198', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (146, 1, 777, 'unverified', 'verifymail', CURRENT_TIMESTAMP, '10.0.1.198', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (146, 1, 778, 'pending_review', 'verify', CURRENT_TIMESTAMP, '10.0.1.198', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (146, 1, 779, 'accepted', 'accept', CURRENT_TIMESTAMP, '10.0.1.198', 891);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (145, 1, 780, 'accepted', 'accept', CURRENT_TIMESTAMP, '10.0.1.198', 892);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (144, 1, 781, 'accepted', 'accept', CURRENT_TIMESTAMP, '10.0.1.198', 893);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (147, 1, 782, 'reg', 'initial', CURRENT_TIMESTAMP, '10.0.1.198', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (147, 1, 783, 'unverified', 'verifymail', CURRENT_TIMESTAMP, '10.0.1.198', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (147, 1, 784, 'pending_review', 'verify', CURRENT_TIMESTAMP, '10.0.1.198', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (148, 1, 785, 'reg', 'initial', CURRENT_TIMESTAMP, '10.0.1.198', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (148, 1, 786, 'unverified', 'verifymail', CURRENT_TIMESTAMP, '10.0.1.198', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (148, 1, 787, 'pending_review', 'verify', CURRENT_TIMESTAMP, '10.0.1.198', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (149, 1, 788, 'reg', 'initial', CURRENT_TIMESTAMP, '10.0.1.198', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (149, 1, 789, 'unverified', 'verifymail', CURRENT_TIMESTAMP, '10.0.1.198', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (149, 1, 790, 'pending_review', 'verify', CURRENT_TIMESTAMP, '10.0.1.198', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (149, 1, 791, 'accepted', 'accept', CURRENT_TIMESTAMP, '10.0.1.198', 910);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (148, 1, 792, 'accepted', 'accept', CURRENT_TIMESTAMP, '10.0.1.198', 911);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (147, 1, 793, 'accepted', 'accept', CURRENT_TIMESTAMP, '10.0.1.198', 912);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (150, 1, 794, 'reg', 'initial', CURRENT_TIMESTAMP, '10.0.1.198', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (150, 1, 795, 'unverified', 'verifymail', CURRENT_TIMESTAMP, '10.0.1.198', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (150, 1, 796, 'pending_review', 'verify', CURRENT_TIMESTAMP, '10.0.1.198', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (151, 1, 797, 'reg', 'initial', CURRENT_TIMESTAMP, '10.0.1.198', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (151, 1, 798, 'unverified', 'verifymail', CURRENT_TIMESTAMP, '10.0.1.198', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (151, 1, 799, 'pending_review', 'verify', CURRENT_TIMESTAMP, '10.0.1.198', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (152, 1, 800, 'reg', 'initial', CURRENT_TIMESTAMP, '10.0.1.198', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (152, 1, 801, 'unverified', 'verifymail', CURRENT_TIMESTAMP, '10.0.1.198', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (152, 1, 802, 'pending_review', 'verify', CURRENT_TIMESTAMP, '10.0.1.198', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (152, 1, 803, 'accepted', 'accept', CURRENT_TIMESTAMP, '10.0.1.198', 929);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (151, 1, 804, 'accepted', 'accept', CURRENT_TIMESTAMP, '10.0.1.198', 930);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (150, 1, 805, 'accepted', 'accept', CURRENT_TIMESTAMP, '10.0.1.198', 931);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (152, 2, 806, 'deleted', 'user_deleted', CURRENT_TIMESTAMP, '10.0.1.198', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (151, 2, 807, 'deleted', 'user_deleted', CURRENT_TIMESTAMP, '10.0.1.198', NULL);
SELECT pg_catalog.setval('action_states_state_id_seq', 807, true);
INSERT INTO ad_codes (code, code_type) VALUES (24741, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (56764, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (88787, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (30810, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (62833, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (94856, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (36879, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (68902, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (10925, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (42948, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (74971, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (16994, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (49017, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (81040, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (23063, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (55086, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (87109, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (29132, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (61155, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (93178, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (35201, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (67224, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (99247, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (41270, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (73293, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (15316, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (47339, 'pay');
INSERT INTO ad_image_changes (ad_id, action_id, state_id, seq_no, name, is_new) VALUES (128, 1, 707, 0, NULL, false);
INSERT INTO ad_image_changes (ad_id, action_id, state_id, seq_no, name, is_new) VALUES (126, 1, 709, 0, NULL, false);
INSERT INTO ad_image_changes (ad_id, action_id, state_id, seq_no, name, is_new) VALUES (131, 1, 719, 0, NULL, false);
INSERT INTO ad_image_changes (ad_id, action_id, state_id, seq_no, name, is_new) VALUES (130, 1, 720, 0, NULL, false);
INSERT INTO ad_image_changes (ad_id, action_id, state_id, seq_no, name, is_new) VALUES (129, 1, 721, 0, NULL, false);
INSERT INTO ad_image_changes (ad_id, action_id, state_id, seq_no, name, is_new) VALUES (134, 1, 731, 0, NULL, false);
INSERT INTO ad_image_changes (ad_id, action_id, state_id, seq_no, name, is_new) VALUES (133, 1, 732, 0, NULL, false);
INSERT INTO ad_image_changes (ad_id, action_id, state_id, seq_no, name, is_new) VALUES (132, 1, 733, 0, NULL, false);
INSERT INTO ad_image_changes (ad_id, action_id, state_id, seq_no, name, is_new) VALUES (137, 1, 743, 0, NULL, false);
INSERT INTO ad_image_changes (ad_id, action_id, state_id, seq_no, name, is_new) VALUES (136, 1, 744, 0, NULL, false);
INSERT INTO ad_image_changes (ad_id, action_id, state_id, seq_no, name, is_new) VALUES (135, 1, 745, 0, NULL, false);
INSERT INTO ad_image_changes (ad_id, action_id, state_id, seq_no, name, is_new) VALUES (140, 1, 755, 0, NULL, false);
INSERT INTO ad_image_changes (ad_id, action_id, state_id, seq_no, name, is_new) VALUES (139, 1, 756, 0, NULL, false);
INSERT INTO ad_image_changes (ad_id, action_id, state_id, seq_no, name, is_new) VALUES (138, 1, 757, 0, NULL, false);
INSERT INTO ad_image_changes (ad_id, action_id, state_id, seq_no, name, is_new) VALUES (143, 1, 767, 0, NULL, false);
INSERT INTO ad_image_changes (ad_id, action_id, state_id, seq_no, name, is_new) VALUES (142, 1, 768, 0, NULL, false);
INSERT INTO ad_image_changes (ad_id, action_id, state_id, seq_no, name, is_new) VALUES (141, 1, 769, 0, NULL, false);
INSERT INTO ad_image_changes (ad_id, action_id, state_id, seq_no, name, is_new) VALUES (146, 1, 779, 0, NULL, false);
INSERT INTO ad_image_changes (ad_id, action_id, state_id, seq_no, name, is_new) VALUES (145, 1, 780, 0, NULL, false);
INSERT INTO ad_image_changes (ad_id, action_id, state_id, seq_no, name, is_new) VALUES (144, 1, 781, 0, NULL, false);
INSERT INTO ad_image_changes (ad_id, action_id, state_id, seq_no, name, is_new) VALUES (149, 1, 791, 0, NULL, false);
INSERT INTO ad_image_changes (ad_id, action_id, state_id, seq_no, name, is_new) VALUES (148, 1, 792, 0, NULL, false);
INSERT INTO ad_image_changes (ad_id, action_id, state_id, seq_no, name, is_new) VALUES (147, 1, 793, 0, NULL, false);
INSERT INTO ad_image_changes (ad_id, action_id, state_id, seq_no, name, is_new) VALUES (152, 1, 803, 0, NULL, false);
INSERT INTO ad_image_changes (ad_id, action_id, state_id, seq_no, name, is_new) VALUES (151, 1, 804, 0, NULL, false);
INSERT INTO ad_image_changes (ad_id, action_id, state_id, seq_no, name, is_new) VALUES (150, 1, 805, 0, NULL, false);
INSERT INTO ad_params (ad_id, name, value) VALUES (126, 'prev_currency', 'peso');
INSERT INTO ad_params (ad_id, name, value) VALUES (126, 'country', 'UNK');
INSERT INTO ad_params (ad_id, name, value) VALUES (128, 'country', 'UNK');
INSERT INTO ad_params (ad_id, name, value) VALUES (129, 'prev_currency', 'peso');
INSERT INTO ad_params (ad_id, name, value) VALUES (129, 'country', 'UNK');
INSERT INTO ad_params (ad_id, name, value) VALUES (130, 'prev_currency', 'peso');
INSERT INTO ad_params (ad_id, name, value) VALUES (130, 'country', 'UNK');
INSERT INTO ad_params (ad_id, name, value) VALUES (131, 'prev_currency', 'peso');
INSERT INTO ad_params (ad_id, name, value) VALUES (131, 'country', 'UNK');
INSERT INTO ad_params (ad_id, name, value) VALUES (132, 'prev_currency', 'peso');
INSERT INTO ad_params (ad_id, name, value) VALUES (132, 'country', 'UNK');
INSERT INTO ad_params (ad_id, name, value) VALUES (133, 'prev_currency', 'peso');
INSERT INTO ad_params (ad_id, name, value) VALUES (133, 'country', 'UNK');
INSERT INTO ad_params (ad_id, name, value) VALUES (134, 'condition', '1');
INSERT INTO ad_params (ad_id, name, value) VALUES (134, 'country', 'UNK');
INSERT INTO ad_params (ad_id, name, value) VALUES (135, 'prev_currency', 'peso');
INSERT INTO ad_params (ad_id, name, value) VALUES (135, 'country', 'UNK');
INSERT INTO ad_params (ad_id, name, value) VALUES (136, 'condition', '1');
INSERT INTO ad_params (ad_id, name, value) VALUES (136, 'country', 'UNK');
INSERT INTO ad_params (ad_id, name, value) VALUES (137, 'gender', '1');
INSERT INTO ad_params (ad_id, name, value) VALUES (137, 'prev_currency', 'peso');
INSERT INTO ad_params (ad_id, name, value) VALUES (137, 'country', 'UNK');
INSERT INTO ad_params (ad_id, name, value) VALUES (138, 'country', 'UNK');
INSERT INTO ad_params (ad_id, name, value) VALUES (139, 'condition', '2');
INSERT INTO ad_params (ad_id, name, value) VALUES (139, 'country', 'UNK');
INSERT INTO ad_params (ad_id, name, value) VALUES (140, 'gender', '2');
INSERT INTO ad_params (ad_id, name, value) VALUES (140, 'condition', '1');
INSERT INTO ad_params (ad_id, name, value) VALUES (140, 'prev_currency', 'peso');
INSERT INTO ad_params (ad_id, name, value) VALUES (140, 'country', 'UNK');
INSERT INTO ad_params (ad_id, name, value) VALUES (141, 'prev_currency', 'peso');
INSERT INTO ad_params (ad_id, name, value) VALUES (141, 'internal_memory', '64');
INSERT INTO ad_params (ad_id, name, value) VALUES (141, 'country', 'UNK');
INSERT INTO ad_params (ad_id, name, value) VALUES (142, 'prev_currency', 'peso');
INSERT INTO ad_params (ad_id, name, value) VALUES (142, 'country', 'UNK');
INSERT INTO ad_params (ad_id, name, value) VALUES (143, 'prev_currency', 'peso');
INSERT INTO ad_params (ad_id, name, value) VALUES (143, 'country', 'UNK');
INSERT INTO ad_params (ad_id, name, value) VALUES (144, 'prev_currency', 'peso');
INSERT INTO ad_params (ad_id, name, value) VALUES (144, 'country', 'UNK');
INSERT INTO ad_params (ad_id, name, value) VALUES (145, 'prev_currency', 'peso');
INSERT INTO ad_params (ad_id, name, value) VALUES (145, 'country', 'UNK');
INSERT INTO ad_params (ad_id, name, value) VALUES (146, 'prev_currency', 'peso');
INSERT INTO ad_params (ad_id, name, value) VALUES (146, 'country', 'UNK');
INSERT INTO ad_params (ad_id, name, value) VALUES (147, 'gender', '1');
INSERT INTO ad_params (ad_id, name, value) VALUES (147, 'condition', '1');
INSERT INTO ad_params (ad_id, name, value) VALUES (147, 'prev_currency', 'peso');
INSERT INTO ad_params (ad_id, name, value) VALUES (147, 'country', 'UNK');
INSERT INTO ad_params (ad_id, name, value) VALUES (148, 'prev_currency', 'peso');
INSERT INTO ad_params (ad_id, name, value) VALUES (148, 'country', 'UNK');
INSERT INTO ad_params (ad_id, name, value) VALUES (149, 'condition', '1');
INSERT INTO ad_params (ad_id, name, value) VALUES (149, 'country', 'UNK');
INSERT INTO ad_params (ad_id, name, value) VALUES (150, 'prev_currency', 'peso');
INSERT INTO ad_params (ad_id, name, value) VALUES (150, 'country', 'UNK');
INSERT INTO ad_params (ad_id, name, value) VALUES (151, 'prev_currency', 'peso');
INSERT INTO ad_params (ad_id, name, value) VALUES (151, 'country', 'UNK');
INSERT INTO ad_params (ad_id, name, value) VALUES (152, 'prev_currency', 'peso');
INSERT INTO ad_params (ad_id, name, value) VALUES (152, 'country', 'UNK');
SELECT pg_catalog.setval('ads_ad_id_seq', 152, true);
TRUNCATE block_rules CASCADE;
TRUNCATE block_lists CASCADE;
TRUNCATE block_rule_conditions CASCADE;
INSERT INTO block_lists (list_id, list_name, base_list, list_type, ignore) VALUES (1, 'E-mail addresses - deleted', true, 'email', NULL);
INSERT INTO block_lists (list_id, list_name, base_list, list_type, ignore) VALUES (2, 'E-mail addresses - spam filter', true, 'email', NULL);
INSERT INTO block_lists (list_id, list_name, base_list, list_type, ignore) VALUES (3, 'IP addresses - deleted', true, 'ip', NULL);
INSERT INTO block_lists (list_id, list_name, base_list, list_type, ignore) VALUES (4, 'IP addresses - spam filter', true, 'ip', NULL);
INSERT INTO block_lists (list_id, list_name, base_list, list_type, ignore) VALUES (11, 'Palabras no aceptadas por whitelist', false, 'general', NULL);
INSERT INTO block_lists (list_id, list_name, base_list, list_type, ignore) VALUES (19, 'KEYWORDS (Forbidden)-AI_sub=>Stop', false, 'general', NULL);
INSERT INTO block_lists (list_id, list_name, base_list, list_type, ignore) VALUES (29, 'UID-AI =>Stop', false, 'general', NULL);
INSERT INTO block_lists (list_id, list_name, base_list, list_type, ignore) VALUES (30, 'KEYWORDS (Forbidden)-AI_sub_decr=>Stop, TAF=>Stop', false, 'general', NULL);
INSERT INTO block_lists (list_id, list_name, base_list, list_type, ignore) VALUES (32, 'KEYWORDS (Cursing)-AR=>Spamfilter', false, 'general', NULL);
INSERT INTO block_lists (list_id, list_name, base_list, list_type, ignore) VALUES (33, 'KEYWORDS (Fraud)-AI=>Abuse,AR=>Spamfilter,TAF=>Stop', false, 'general', NULL);
INSERT INTO block_lists (list_id, list_name, base_list, list_type, ignore) VALUES (34, 'EMAIL(Part)-AI=>Abuso', false, 'email', NULL);
INSERT INTO block_lists (list_id, list_name, base_list, list_type, ignore) VALUES (35, 'PHONENUMBER-AI=>Stop', false, 'general', NULL);
INSERT INTO block_lists (list_id, list_name, base_list, list_type, ignore) VALUES (36, 'IP(Chilena)-AI(Fraud)=>Abuse', false, 'ip', NULL);
INSERT INTO block_lists (list_id, list_name, base_list, list_type, ignore) VALUES (37, 'KEYWORDS (Fraud)-AR=>Delete', false, 'general', NULL);
INSERT INTO block_lists (list_id, list_name, base_list, list_type, ignore) VALUES (39, 'whitelist', false, 'email', NULL);
INSERT INTO block_lists (list_id, list_name, base_list, list_type, ignore) VALUES (40, 'were_whitelist', false, 'email', NULL);
INSERT INTO block_lists (list_id, list_name, base_list, list_type, ignore) VALUES (41, 'store_bad_words', false, 'general', NULL);
INSERT INTO block_lists (list_id, list_name, base_list, list_type, ignore) VALUES (48, 'KEYWORDS-AI=>Abuse', false, 'general', NULL);
INSERT INTO block_lists (list_id, list_name, base_list, list_type, ignore) VALUES (50, 'Categor�as auto aceptadas', false, 'category', NULL);
INSERT INTO block_rules (rule_id, rule_name, action, application, combine_rule, count_yesterday, count_today, count_date, target) VALUES (1, 'EMAIL-AI=>Stop', 'stop', 'newad', 'and', 70, 68, '2015-04-06', NULL);
INSERT INTO block_rules (rule_id, rule_name, action, application, combine_rule, count_yesterday, count_today, count_date, target) VALUES (2, 'KEYWORDS (Forbidden)-AI_sub_decr=>Stop', 'stop', 'newad', 'or', 27, 12, '2015-04-06', NULL);
INSERT INTO block_rules (rule_id, rule_name, action, application, combine_rule, count_yesterday, count_today, count_date, target) VALUES (4, 'IP-AI=>Stop', 'stop', 'newad', 'or', 73, 75, '2015-04-06', NULL);
INSERT INTO block_rules (rule_id, rule_name, action, application, combine_rule, count_yesterday, count_today, count_date, target) VALUES (5, 'KEYWORDS (Forbidden)-AI_sub=>Stop', 'stop', 'newad', 'and', 3216, 2001, '2015-04-06', NULL);
INSERT INTO block_rules (rule_id, rule_name, action, application, combine_rule, count_yesterday, count_today, count_date, target) VALUES (6, 'IP-AR=>Delete', 'delete', 'adreply', 'and', 46, 39, '2015-04-06', NULL);
INSERT INTO block_rules (rule_id, rule_name, action, application, combine_rule, count_yesterday, count_today, count_date, target) VALUES (7, 'EMAIL-AR=>Delete', 'delete', 'adreply', 'and', 79, 143, '2015-04-06', NULL);
INSERT INTO block_rules (rule_id, rule_name, action, application, combine_rule, count_yesterday, count_today, count_date, target) VALUES (10, 'UID-AI =>Stop', 'stop', 'newad', 'and', 7, 60, '2015-04-06', NULL);
INSERT INTO block_rules (rule_id, rule_name, action, application, combine_rule, count_yesterday, count_today, count_date, target) VALUES (11, 'IP-AR=>Spamfilter', 'spamfilter', 'adreply', 'and', 14, 38, '2015-04-06', NULL);
INSERT INTO block_rules (rule_id, rule_name, action, application, combine_rule, count_yesterday, count_today, count_date, target) VALUES (12, 'EMAIL-AR=>Spamfilter', 'spamfilter', 'adreply', 'and', 740, 625, '2015-04-06', NULL);
INSERT INTO block_rules (rule_id, rule_name, action, application, combine_rule, count_yesterday, count_today, count_date, target) VALUES (13, 'KEYWORDS-AI=>Abuse', 'move_to_queue', 'clear', 'and', 28, 16, '2014-09-10', 'abuse');
INSERT INTO block_rules (rule_id, rule_name, action, application, combine_rule, count_yesterday, count_today, count_date, target) VALUES (14, 'KEYWORDS (Cursing)-AR=>spamfilter', 'spamfilter', 'adreply', 'or', 350, 390, '2015-04-06', NULL);
INSERT INTO block_rules (rule_id, rule_name, action, application, combine_rule, count_yesterday, count_today, count_date, target) VALUES (16, 'KEYWORDS (fraud)-TAF=>Stop', 'delete', 'sendtip', 'or', 0, 1, '2015-04-05', NULL);
INSERT INTO block_rules (rule_id, rule_name, action, application, combine_rule, count_yesterday, count_today, count_date, target) VALUES (17, 'KEYWORDS (Fraud)-AI=>Abuse', 'move_to_queue', 'clear', 'or', 67, 3, '2014-09-11', 'abuse');
INSERT INTO block_rules (rule_id, rule_name, action, application, combine_rule, count_yesterday, count_today, count_date, target) VALUES (18, 'KEYWORDS (Fraud)-AR=>spamfilter', 'spamfilter', 'adreply', 'or', 299, 11, '2015-04-06', NULL);
INSERT INTO block_rules (rule_id, rule_name, action, application, combine_rule, count_yesterday, count_today, count_date, target) VALUES (19, 'EMAIL(Part)-AI=>Abuso', 'move_to_queue', 'clear', 'and', 63, 5, '2014-09-11', 'abuse');
INSERT INTO block_rules (rule_id, rule_name, action, application, combine_rule, count_yesterday, count_today, count_date, target) VALUES (20, 'PHONENUMBER-AI=>Stop', 'stop', 'newad', 'and', 66, 87, '2015-04-06', NULL);
INSERT INTO block_rules (rule_id, rule_name, action, application, combine_rule, count_yesterday, count_today, count_date, target) VALUES (21, 'KEYWORDS (Forbidden)-TAF=>Stop', 'delete', 'sendtip', 'and', 0, 1, '2015-04-01', NULL);
INSERT INTO block_rules (rule_id, rule_name, action, application, combine_rule, count_yesterday, count_today, count_date, target) VALUES (22, 'IP(Chilena)-AI(fraud)=>Abuse', 'move_to_queue', 'clear', 'and', 34, 41, '2014-09-10', 'abuse');
INSERT INTO block_rules (rule_id, rule_name, action, application, combine_rule, count_yesterday, count_today, count_date, target) VALUES (23, 'KEYWORDS (Fraud)-AR=>Delete', 'delete', 'adreply', 'and', 303, 3, '2015-04-06', NULL);
INSERT INTO block_rules (rule_id, rule_name, action, application, combine_rule, count_yesterday, count_today, count_date, target) VALUES (25, 'Whitelist', 'move_to_queue', 'clear', 'and', 0, 0, '2014-10-01', 'whitelist');
INSERT INTO block_rules (rule_id, rule_name, action, application, combine_rule, count_yesterday, count_today, count_date, target) VALUES (28, 'categor�as auto aceptadas', 'move_to_queue', 'clear', 'and', 0, 0, '2015-03-31', 'autoaccept');
INSERT INTO block_rule_conditions (condition_id, rule_id, list_id, whole_word, fields) VALUES (26, 11, 4, 1, '{remote_addr}');
INSERT INTO block_rule_conditions (condition_id, rule_id, list_id, whole_word, fields) VALUES (27, 12, 2, 1, '{email}');
INSERT INTO block_rule_conditions (condition_id, rule_id, list_id, whole_word, fields) VALUES (42, 14, 32, 1, '{email,body}');
INSERT INTO block_rule_conditions (condition_id, rule_id, list_id, whole_word, fields) VALUES (46, 17, 33, 1, '{name,email,subject,body}');
INSERT INTO block_rule_conditions (condition_id, rule_id, list_id, whole_word, fields) VALUES (47, 18, 33, 1, '{name,email,body}');
INSERT INTO block_rule_conditions (condition_id, rule_id, list_id, whole_word, fields) VALUES (50, 21, 30, 1, '{name,email,body}');
INSERT INTO block_rule_conditions (condition_id, rule_id, list_id, whole_word, fields) VALUES (51, 16, 33, 1, '{name,email,body}');
INSERT INTO block_rule_conditions (condition_id, rule_id, list_id, whole_word, fields) VALUES (57, 4, 3, 1, '{remote_addr}');
INSERT INTO block_rule_conditions (condition_id, rule_id, list_id, whole_word, fields) VALUES (60, 1, 1, 1, '{email}');
INSERT INTO block_rule_conditions (condition_id, rule_id, list_id, whole_word, fields) VALUES (61, 22, 36, 1, '{remote_addr}');
INSERT INTO block_rule_conditions (condition_id, rule_id, list_id, whole_word, fields) VALUES (63, 2, 30, 1, '{subject,body}');
INSERT INTO block_rule_conditions (condition_id, rule_id, list_id, whole_word, fields) VALUES (65, 19, 34, 0, '{email}');
INSERT INTO block_rule_conditions (condition_id, rule_id, list_id, whole_word, fields) VALUES (66, 7, 1, 1, '{email}');
INSERT INTO block_rule_conditions (condition_id, rule_id, list_id, whole_word, fields) VALUES (67, 6, 3, 1, '{remote_addr}');
INSERT INTO block_rule_conditions (condition_id, rule_id, list_id, whole_word, fields) VALUES (68, 5, 19, 1, '{subject}');
INSERT INTO block_rule_conditions (condition_id, rule_id, list_id, whole_word, fields) VALUES (69, 23, 37, 1, '{email,body}');
INSERT INTO block_rule_conditions (condition_id, rule_id, list_id, whole_word, fields) VALUES (73, 25, 39, 1, '{email}');
INSERT INTO block_rule_conditions (condition_id, rule_id, list_id, whole_word, fields) VALUES (79, 13, 48, 1, '{name,email,subject,body}');
INSERT INTO block_rule_conditions (condition_id, rule_id, list_id, whole_word, fields) VALUES (80, 10, 29, 2, '{uid}');
INSERT INTO block_rule_conditions (condition_id, rule_id, list_id, whole_word, fields) VALUES (81, 20, 35, 2, '{phone}');
INSERT INTO block_rule_conditions (condition_id, rule_id, list_id, whole_word, fields) VALUES (83, 28, 50, 1, '{category}');
SELECT pg_catalog.setval('block_rule_conditions_condition_id_seq', 84, true);
INSERT INTO blocked_items (item_id, value, list_id, notice, token_id) VALUES (22, '1_inserted_keep_whitelist@yapo.cl', 39, '1era', 762);
INSERT INTO blocked_items (item_id, value, list_id, notice, token_id) VALUES (23, 'user_doesnt_3accepted_in_3m_other_whitelist_queue@yapo.cl', 39, '2da', 766);
INSERT INTO blocked_items (item_id, value, list_id, notice, token_id) VALUES (24, '1_rejected_other_moved_review_queue@yapo.cl', 39, '3er', 770);
INSERT INTO blocked_items (item_id, value, list_id, notice, token_id) VALUES (25, '1_forbiddenwords_other_whitelist_queue@yapo.cl', 39, '4ta', 774);
INSERT INTO blocked_items (item_id, value, list_id, notice, token_id) VALUES (26, '1_duplicatewarning_other_whitelist_queue@yapo.cl', 39, '5ta', 778);
INSERT INTO blocked_items (item_id, value, list_id, notice, token_id) VALUES (27, '1_without_greenprice_other_whitelist_queue@yapo.cl', 39, '6ta', 782);
INSERT INTO blocked_items (item_id, value, list_id, notice, token_id) VALUES (28, '1_foreignip_to_review_other_whitelistqueue@yapo.cl', 39, '7ma', 786);
INSERT INTO blocked_items (item_id, value, list_id, notice, token_id) VALUES (29, '1_withnote_to_review_other_whitelist_queue@yapo.cl', 39, '8va', 790);
INSERT INTO blocked_items (item_id, value, list_id, notice, token_id) VALUES (30, '1_under1000_to_review_other_whitelistqueue@yapo.cl', 39, '9na', 794);
INSERT INTO blocked_items (item_id, value, list_id, notice, token_id) VALUES (31, 'analakrobat', 11, 'analabrobat', 1007);
INSERT INTO blocked_items (item_id, value, list_id, notice, token_id) VALUES (32, 'mamahuevo', 11, 'mamahuevo', 1008);
INSERT INTO blocked_items (item_id, value, list_id, notice, token_id) VALUES (33, 'conshesumare', 11, 'conshesumare', 1009);
SELECT pg_catalog.setval('blocked_items_item_id_seq', 33, true);
INSERT INTO event_log (event_id, event_name, event, token_id, "time") VALUES (9, 'blocked_item', 'INSERT blocked_item 1_inserted_keep_whitelist@yapo.cl, 39, "1era"', 762, CURRENT_TIMESTAMP);
INSERT INTO event_log (event_id, event_name, event, token_id, "time") VALUES (10, 'blocked_item', 'INSERT blocked_item user_doesnt_3accepted_in_3m_other_whitelist_queue@yapo.cl, 39, "2da"', 766, CURRENT_TIMESTAMP);
INSERT INTO event_log (event_id, event_name, event, token_id, "time") VALUES (11, 'blocked_item', 'INSERT blocked_item 1_rejected_other_moved_review_queue@yapo.cl, 39, "3er"', 770, CURRENT_TIMESTAMP);
INSERT INTO event_log (event_id, event_name, event, token_id, "time") VALUES (12, 'blocked_item', 'INSERT blocked_item 1_forbiddenwords_other_whitelist_queue@yapo.cl, 39, "4ta"', 774, CURRENT_TIMESTAMP);
INSERT INTO event_log (event_id, event_name, event, token_id, "time") VALUES (13, 'blocked_item', 'INSERT blocked_item 1_duplicatewarning_other_whitelist_queue@yapo.cl, 39, "5ta"', 778, CURRENT_TIMESTAMP);
INSERT INTO event_log (event_id, event_name, event, token_id, "time") VALUES (14, 'blocked_item', 'INSERT blocked_item 1_without_greenprice_other_whitelist_queue@yapo.cl, 39, "6ta"', 782, CURRENT_TIMESTAMP);
INSERT INTO event_log (event_id, event_name, event, token_id, "time") VALUES (15, 'blocked_item', 'INSERT blocked_item 1_foreignip_to_review_other_whitelistqueue@yapo.cl, 39, "7ma"', 786, CURRENT_TIMESTAMP);
INSERT INTO event_log (event_id, event_name, event, token_id, "time") VALUES (16, 'blocked_item', 'INSERT blocked_item 1_withnote_to_review_other_whitelist_queue@yapo.cl, 39, "8va"', 790, CURRENT_TIMESTAMP);
INSERT INTO event_log (event_id, event_name, event, token_id, "time") VALUES (17, 'blocked_item', 'INSERT blocked_item 1_under1000_to_review_other_whitelistqueue@yapo.cl, 39, "9na"', 794, CURRENT_TIMESTAMP);
SELECT pg_catalog.setval('event_log_event_id_seq', 17, true);
SELECT pg_catalog.setval('list_id_seq', 8000111, true);
SELECT pg_catalog.setval('pay_code_seq', 493, true);
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (213, 'save', 0, '633026231', NULL, CURRENT_TIMESTAMP, 166, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (214, 'verify', 0, '633026231', NULL, CURRENT_TIMESTAMP, 166, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (215, 'save', 0, '768418254', NULL, CURRENT_TIMESTAMP, 167, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (216, 'verify', 0, '768418254', NULL, CURRENT_TIMESTAMP, 167, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (217, 'save', 0, '903810277', NULL, CURRENT_TIMESTAMP, 168, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (218, 'verify', 0, '903810277', NULL, CURRENT_TIMESTAMP, 168, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (219, 'save', 0, '139202300', NULL, CURRENT_TIMESTAMP, 169, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (220, 'verify', 0, '139202300', NULL, CURRENT_TIMESTAMP, 169, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (221, 'save', 0, '274594323', NULL, CURRENT_TIMESTAMP, 170, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (222, 'verify', 0, '274594323', NULL, CURRENT_TIMESTAMP, 170, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (223, 'save', 0, '409986346', NULL, CURRENT_TIMESTAMP, 171, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (224, 'verify', 0, '409986346', NULL, CURRENT_TIMESTAMP, 171, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (225, 'save', 0, '545378369', NULL, CURRENT_TIMESTAMP, 172, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (226, 'verify', 0, '545378369', NULL, CURRENT_TIMESTAMP, 172, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (227, 'save', 0, '680770392', NULL, CURRENT_TIMESTAMP, 173, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (228, 'verify', 0, '680770392', NULL, CURRENT_TIMESTAMP, 173, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (229, 'save', 0, '816162415', NULL, CURRENT_TIMESTAMP, 174, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (230, 'verify', 0, '816162415', NULL, CURRENT_TIMESTAMP, 174, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (231, 'save', 0, '951554438', NULL, CURRENT_TIMESTAMP, 175, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (232, 'verify', 0, '951554438', NULL, CURRENT_TIMESTAMP, 175, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (233, 'save', 0, '186946461', NULL, CURRENT_TIMESTAMP, 176, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (234, 'verify', 0, '186946461', NULL, CURRENT_TIMESTAMP, 176, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (235, 'save', 0, '322338484', NULL, CURRENT_TIMESTAMP, 177, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (236, 'verify', 0, '322338484', NULL, CURRENT_TIMESTAMP, 177, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (237, 'save', 0, '457730507', NULL, CURRENT_TIMESTAMP, 178, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (238, 'verify', 0, '457730507', NULL, CURRENT_TIMESTAMP, 178, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (239, 'save', 0, '593122530', NULL, CURRENT_TIMESTAMP, 179, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (240, 'verify', 0, '593122530', NULL, CURRENT_TIMESTAMP, 179, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (242, 'verify', 0, '728514553', NULL, CURRENT_TIMESTAMP, 180, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (241, 'save', 0, '728514553', NULL, CURRENT_TIMESTAMP, 180, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (243, 'save', 0, '863906576', NULL, CURRENT_TIMESTAMP, 181, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (244, 'verify', 0, '863906576', NULL, CURRENT_TIMESTAMP, 181, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (245, 'save', 0, '999298599', NULL, CURRENT_TIMESTAMP, 182, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (246, 'verify', 0, '999298599', NULL, CURRENT_TIMESTAMP, 182, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (247, 'save', 0, '234690622', NULL, CURRENT_TIMESTAMP, 183, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (248, 'verify', 0, '234690622', NULL, CURRENT_TIMESTAMP, 183, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (249, 'save', 0, '370082645', NULL, CURRENT_TIMESTAMP, 184, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (250, 'verify', 0, '370082645', NULL, CURRENT_TIMESTAMP, 184, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (251, 'save', 0, '505474668', NULL, CURRENT_TIMESTAMP, 185, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (252, 'verify', 0, '505474668', NULL, CURRENT_TIMESTAMP, 185, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (253, 'save', 0, '640866691', NULL, CURRENT_TIMESTAMP, 186, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (254, 'verify', 0, '640866691', NULL, CURRENT_TIMESTAMP, 186, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (255, 'save', 0, '776258714', NULL, CURRENT_TIMESTAMP, 187, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (256, 'verify', 0, '776258714', NULL, CURRENT_TIMESTAMP, 187, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (257, 'save', 0, '911650737', NULL, CURRENT_TIMESTAMP, 188, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (258, 'verify', 0, '911650737', NULL, CURRENT_TIMESTAMP, 188, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (259, 'save', 0, '147042760', NULL, CURRENT_TIMESTAMP, 189, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (260, 'verify', 0, '147042760', NULL, CURRENT_TIMESTAMP, 189, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (261, 'save', 0, '282434783', NULL, CURRENT_TIMESTAMP, 190, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (262, 'verify', 0, '282434783', NULL, CURRENT_TIMESTAMP, 190, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (263, 'save', 0, '417826806', NULL, CURRENT_TIMESTAMP, 191, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (264, 'verify', 0, '417826806', NULL, CURRENT_TIMESTAMP, 191, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (265, 'save', 0, '553218829', NULL, CURRENT_TIMESTAMP, 192, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (266, 'verify', 0, '553218829', NULL, CURRENT_TIMESTAMP, 192, 'OK');
SELECT pg_catalog.setval('pay_log_pay_log_id_seq', 266, true);
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (213, 0, 'adphone', '226437660');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (213, 1, 'remote_addr', '10.0.1.198');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (214, 0, 'remote_addr', '10.0.1.198');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (215, 0, 'adphone', '226437660');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (215, 1, 'remote_addr', '10.0.1.198');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (216, 0, 'remote_addr', '10.0.1.198');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (217, 0, 'adphone', '226437660');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (217, 1, 'remote_addr', '10.0.1.198');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (218, 0, 'remote_addr', '10.0.1.198');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (219, 0, 'adphone', '962184762');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (219, 1, 'remote_addr', '10.0.1.198');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (220, 0, 'remote_addr', '10.0.1.198');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (221, 0, 'adphone', '962184762');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (221, 1, 'remote_addr', '10.0.1.198');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (222, 0, 'remote_addr', '10.0.1.198');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (223, 0, 'adphone', '962184762');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (223, 1, 'remote_addr', '10.0.1.198');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (224, 0, 'remote_addr', '10.0.1.198');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (225, 0, 'adphone', '962184762');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (225, 1, 'remote_addr', '10.0.1.198');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (226, 0, 'remote_addr', '10.0.1.198');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (227, 0, 'adphone', '962184762');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (227, 1, 'remote_addr', '10.0.1.198');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (228, 0, 'remote_addr', '10.0.1.198');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (229, 0, 'adphone', '962184762');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (229, 1, 'remote_addr', '10.0.1.198');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (230, 0, 'remote_addr', '10.0.1.198');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (231, 0, 'adphone', '962184762');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (231, 1, 'remote_addr', '10.0.1.198');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (232, 0, 'remote_addr', '10.0.1.198');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (233, 0, 'adphone', '962184762');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (233, 1, 'remote_addr', '10.0.1.198');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (234, 0, 'remote_addr', '10.0.1.198');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (235, 0, 'adphone', '962184762');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (235, 1, 'remote_addr', '10.0.1.198');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (236, 0, 'remote_addr', '10.0.1.198');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (237, 0, 'adphone', '226437660');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (237, 1, 'remote_addr', '10.0.1.198');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (238, 0, 'remote_addr', '10.0.1.198');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (239, 0, 'adphone', '226437660');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (239, 1, 'remote_addr', '10.0.1.198');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (240, 0, 'remote_addr', '10.0.1.198');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (241, 0, 'adphone', '226437660');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (241, 1, 'remote_addr', '10.0.1.198');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (242, 0, 'remote_addr', '10.0.1.198');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (243, 0, 'adphone', '962184762');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (243, 1, 'remote_addr', '10.0.1.198');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (244, 0, 'remote_addr', '10.0.1.198');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (245, 0, 'adphone', '962184762');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (245, 1, 'remote_addr', '10.0.1.198');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (246, 0, 'remote_addr', '10.0.1.198');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (247, 0, 'adphone', '962184762');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (247, 1, 'remote_addr', '10.0.1.198');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (248, 0, 'remote_addr', '10.0.1.198');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (249, 0, 'adphone', '226437660');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (249, 1, 'remote_addr', '10.0.1.198');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (250, 0, 'remote_addr', '10.0.1.198');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (251, 0, 'adphone', '226437660');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (251, 1, 'remote_addr', '10.0.1.198');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (252, 0, 'remote_addr', '10.0.1.198');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (253, 0, 'adphone', '226437660');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (253, 1, 'remote_addr', '10.0.1.198');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (254, 0, 'remote_addr', '10.0.1.198');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (255, 0, 'adphone', '226437660');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (255, 1, 'remote_addr', '10.0.1.198');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (256, 0, 'remote_addr', '10.0.1.198');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (257, 0, 'adphone', '226437660');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (257, 1, 'remote_addr', '10.0.1.198');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (258, 0, 'remote_addr', '10.0.1.198');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (259, 0, 'adphone', '226437660');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (259, 1, 'remote_addr', '10.0.1.198');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (260, 0, 'remote_addr', '10.0.1.198');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (261, 0, 'adphone', '962184762');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (261, 1, 'remote_addr', '10.0.1.198');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (262, 0, 'remote_addr', '10.0.1.198');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (263, 0, 'adphone', '962184762');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (263, 1, 'remote_addr', '10.0.1.198');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (264, 0, 'remote_addr', '10.0.1.198');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (265, 0, 'adphone', '962184762');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (265, 1, 'remote_addr', '10.0.1.198');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (266, 0, 'remote_addr', '10.0.1.198');
SELECT pg_catalog.setval('payment_groups_payment_group_id_seq', 192, true);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (166, 'ad_action', 0, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (167, 'ad_action', 0, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (168, 'ad_action', 0, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (169, 'ad_action', 0, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (170, 'ad_action', 0, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (171, 'ad_action', 0, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (172, 'ad_action', 0, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (173, 'ad_action', 0, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (174, 'ad_action', 0, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (175, 'ad_action', 0, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (176, 'ad_action', 0, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (177, 'ad_action', 0, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (178, 'ad_action', 0, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (179, 'ad_action', 0, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (180, 'ad_action', 0, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (181, 'ad_action', 0, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (182, 'ad_action', 0, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (183, 'ad_action', 0, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (184, 'ad_action', 0, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (185, 'ad_action', 0, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (186, 'ad_action', 0, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (187, 'ad_action', 0, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (188, 'ad_action', 0, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (189, 'ad_action', 0, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (190, 'ad_action', 0, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (191, 'ad_action', 0, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (192, 'ad_action', 0, 0);
INSERT INTO review_log (ad_id, action_id, admin_id, review_time, queue, action_type, action, refusal_reason_text, category) VALUES (128, 1, 9, CURRENT_TIMESTAMP, 'all', 'new', 'accepted', NULL, 5160);
INSERT INTO review_log (ad_id, action_id, admin_id, review_time, queue, action_type, action, refusal_reason_text, category) VALUES (126, 1, 9, CURRENT_TIMESTAMP, 'all', 'new', 'accepted', NULL, 5020);
INSERT INTO review_log (ad_id, action_id, admin_id, review_time, queue, action_type, action, refusal_reason_text, category) VALUES (131, 1, 9, CURRENT_TIMESTAMP, 'all', 'new', 'accepted', NULL, 5040);
INSERT INTO review_log (ad_id, action_id, admin_id, review_time, queue, action_type, action, refusal_reason_text, category) VALUES (130, 1, 9, CURRENT_TIMESTAMP, 'all', 'new', 'accepted', NULL, 5060);
INSERT INTO review_log (ad_id, action_id, admin_id, review_time, queue, action_type, action, refusal_reason_text, category) VALUES (129, 1, 9, CURRENT_TIMESTAMP, 'all', 'new', 'refused', 'Virus', 5020);
INSERT INTO review_log (ad_id, action_id, admin_id, review_time, queue, action_type, action, refusal_reason_text, category) VALUES (134, 1, 9, CURRENT_TIMESTAMP, 'all', 'new', 'accepted', NULL, 9040);
INSERT INTO review_log (ad_id, action_id, admin_id, review_time, queue, action_type, action, refusal_reason_text, category) VALUES (133, 1, 9, CURRENT_TIMESTAMP, 'all', 'new', 'accepted', NULL, 5060);
INSERT INTO review_log (ad_id, action_id, admin_id, review_time, queue, action_type, action, refusal_reason_text, category) VALUES (132, 1, 9, CURRENT_TIMESTAMP, 'all', 'new', 'accepted', NULL, 5020);
INSERT INTO review_log (ad_id, action_id, admin_id, review_time, queue, action_type, action, refusal_reason_text, category) VALUES (137, 1, 9, CURRENT_TIMESTAMP, 'all', 'new', 'accepted', NULL, 4060);
INSERT INTO review_log (ad_id, action_id, admin_id, review_time, queue, action_type, action, refusal_reason_text, category) VALUES (136, 1, 9, CURRENT_TIMESTAMP, 'all', 'new', 'accepted', NULL, 9040);
INSERT INTO review_log (ad_id, action_id, admin_id, review_time, queue, action_type, action, refusal_reason_text, category) VALUES (135, 1, 9, CURRENT_TIMESTAMP, 'all', 'new', 'accepted', NULL, 6140);
INSERT INTO review_log (ad_id, action_id, admin_id, review_time, queue, action_type, action, refusal_reason_text, category) VALUES (140, 1, 9, CURRENT_TIMESTAMP, 'all', 'new', 'accepted', NULL, 4040);
INSERT INTO review_log (ad_id, action_id, admin_id, review_time, queue, action_type, action, refusal_reason_text, category) VALUES (138, 1, 9, CURRENT_TIMESTAMP, 'all', 'new', 'accepted', NULL, 5160);
INSERT INTO review_log (ad_id, action_id, admin_id, review_time, queue, action_type, action, refusal_reason_text, category) VALUES (139, 1, 9, CURRENT_TIMESTAMP, 'all', 'new', 'accepted', NULL, 9020);
INSERT INTO review_log (ad_id, action_id, admin_id, review_time, queue, action_type, action, refusal_reason_text, category) VALUES (143, 1, 9, CURRENT_TIMESTAMP, 'all', 'new', 'accepted', NULL, 6180);
INSERT INTO review_log (ad_id, action_id, admin_id, review_time, queue, action_type, action, refusal_reason_text, category) VALUES (142, 1, 9, CURRENT_TIMESTAMP, 'all', 'new', 'accepted', NULL, 6120);
INSERT INTO review_log (ad_id, action_id, admin_id, review_time, queue, action_type, action, refusal_reason_text, category) VALUES (141, 1, 9, CURRENT_TIMESTAMP, 'all', 'new', 'accepted', NULL, 3060);
INSERT INTO review_log (ad_id, action_id, admin_id, review_time, queue, action_type, action, refusal_reason_text, category) VALUES (146, 1, 9, CURRENT_TIMESTAMP, 'all', 'new', 'accepted', NULL, 3020);
INSERT INTO review_log (ad_id, action_id, admin_id, review_time, queue, action_type, action, refusal_reason_text, category) VALUES (145, 1, 9, CURRENT_TIMESTAMP, 'all', 'new', 'accepted', NULL, 5020);
INSERT INTO review_log (ad_id, action_id, admin_id, review_time, queue, action_type, action, refusal_reason_text, category) VALUES (144, 1, 9, CURRENT_TIMESTAMP, 'all', 'new', 'accepted', NULL, 6020);
INSERT INTO review_log (ad_id, action_id, admin_id, review_time, queue, action_type, action, refusal_reason_text, category) VALUES (149, 1, 9, CURRENT_TIMESTAMP, 'all', 'new', 'accepted', NULL, 9060);
INSERT INTO review_log (ad_id, action_id, admin_id, review_time, queue, action_type, action, refusal_reason_text, category) VALUES (148, 1, 9, CURRENT_TIMESTAMP, 'all', 'new', 'accepted', NULL, 6020);
INSERT INTO review_log (ad_id, action_id, admin_id, review_time, queue, action_type, action, refusal_reason_text, category) VALUES (147, 1, 9, CURRENT_TIMESTAMP, 'all', 'new', 'accepted', NULL, 4040);
INSERT INTO review_log (ad_id, action_id, admin_id, review_time, queue, action_type, action, refusal_reason_text, category) VALUES (152, 1, 9, CURRENT_TIMESTAMP, 'all', 'new', 'accepted', NULL, 5020);
INSERT INTO review_log (ad_id, action_id, admin_id, review_time, queue, action_type, action, refusal_reason_text, category) VALUES (151, 1, 9, CURRENT_TIMESTAMP, 'all', 'new', 'accepted', NULL, 5060);
INSERT INTO review_log (ad_id, action_id, admin_id, review_time, queue, action_type, action, refusal_reason_text, category) VALUES (150, 1, 9, CURRENT_TIMESTAMP, 'all', 'new', 'accepted', NULL, 5040);
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (126, 1, 700, 'queue', 'normal');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (128, 1, 706, 'queue', 'normal');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (128, 1, 707, 'filter_name', 'all');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (126, 1, 709, 'filter_name', 'all');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (129, 1, 712, 'queue', 'normal');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (130, 1, 715, 'queue', 'normal');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (131, 1, 718, 'queue', 'normal');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (131, 1, 719, 'filter_name', 'all');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (130, 1, 720, 'filter_name', 'all');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (129, 1, 721, 'filter_name', 'all');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (132, 1, 724, 'queue', 'normal');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (133, 1, 727, 'queue', 'normal');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (134, 1, 730, 'queue', 'normal');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (134, 1, 731, 'filter_name', 'all');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (133, 1, 732, 'filter_name', 'all');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (132, 1, 733, 'filter_name', 'all');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (135, 1, 736, 'queue', 'normal');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (136, 1, 739, 'queue', 'normal');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (137, 1, 742, 'queue', 'normal');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (137, 1, 743, 'filter_name', 'all');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (136, 1, 744, 'filter_name', 'all');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (135, 1, 745, 'filter_name', 'all');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (138, 1, 748, 'queue', 'normal');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (139, 1, 751, 'queue', 'normal');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (140, 1, 754, 'queue', 'normal');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (140, 1, 755, 'filter_name', 'all');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (139, 1, 756, 'filter_name', 'all');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (138, 1, 757, 'filter_name', 'all');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (141, 1, 760, 'queue', 'normal');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (142, 1, 763, 'queue', 'normal');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (143, 1, 766, 'queue', 'normal');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (143, 1, 767, 'filter_name', 'all');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (142, 1, 768, 'filter_name', 'all');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (141, 1, 769, 'filter_name', 'all');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (144, 1, 772, 'queue', 'normal');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (145, 1, 775, 'queue', 'normal');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (146, 1, 778, 'queue', 'normal');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (146, 1, 779, 'filter_name', 'all');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (145, 1, 780, 'filter_name', 'all');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (144, 1, 781, 'filter_name', 'all');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (147, 1, 784, 'queue', 'normal');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (148, 1, 787, 'queue', 'normal');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (149, 1, 790, 'queue', 'normal');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (149, 1, 791, 'filter_name', 'all');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (148, 1, 792, 'filter_name', 'all');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (147, 1, 793, 'filter_name', 'all');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (150, 1, 796, 'queue', 'normal');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (151, 1, 799, 'queue', 'normal');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (152, 1, 802, 'queue', 'normal');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (152, 1, 803, 'filter_name', 'all');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (151, 1, 804, 'filter_name', 'all');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (150, 1, 805, 'filter_name', 'all');
SELECT pg_catalog.setval('tokens_token_id_seq', 932, true);
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (119, CURRENT_TIMESTAMP, '30038@mazaru.schibsted.cl', NULL, CURRENT_TIMESTAMP, '30038@mazaru.schibsted.cl', 'TRANS_OK', CURRENT_TIMESTAMP, NULL, 'cmd:admail
commit:1
ad_id:150
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (118, CURRENT_TIMESTAMP, '30038@mazaru.schibsted.cl', NULL, CURRENT_TIMESTAMP, '30038@mazaru.schibsted.cl', 'TRANS_OK', CURRENT_TIMESTAMP, NULL, 'cmd:admail
commit:1
ad_id:151
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (117, CURRENT_TIMESTAMP, '30038@mazaru.schibsted.cl', NULL, CURRENT_TIMESTAMP, '30038@mazaru.schibsted.cl', 'TRANS_OK', CURRENT_TIMESTAMP, NULL, 'cmd:admail
commit:1
ad_id:152
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (116, CURRENT_TIMESTAMP, '30038@mazaru.schibsted.cl', NULL, CURRENT_TIMESTAMP, '30038@mazaru.schibsted.cl', 'TRANS_OK', CURRENT_TIMESTAMP, NULL, 'cmd:admail
commit:1
ad_id:147
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (115, CURRENT_TIMESTAMP, '30038@mazaru.schibsted.cl', NULL, CURRENT_TIMESTAMP, '30038@mazaru.schibsted.cl', 'TRANS_OK', CURRENT_TIMESTAMP, NULL, 'cmd:admail
commit:1
ad_id:148
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (114, CURRENT_TIMESTAMP, '30038@mazaru.schibsted.cl', NULL, CURRENT_TIMESTAMP, '30038@mazaru.schibsted.cl', 'TRANS_OK', CURRENT_TIMESTAMP, NULL, 'cmd:admail
commit:1
ad_id:149
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (113, CURRENT_TIMESTAMP, '30038@mazaru.schibsted.cl', NULL, CURRENT_TIMESTAMP, '30038@mazaru.schibsted.cl', 'TRANS_OK', CURRENT_TIMESTAMP, NULL, 'cmd:admail
commit:1
ad_id:144
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (112, CURRENT_TIMESTAMP, '30038@mazaru.schibsted.cl', NULL, CURRENT_TIMESTAMP, '30038@mazaru.schibsted.cl', 'TRANS_OK', CURRENT_TIMESTAMP, NULL, 'cmd:admail
commit:1
ad_id:145
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (110, CURRENT_TIMESTAMP, '30038@mazaru.schibsted.cl', NULL, CURRENT_TIMESTAMP, '30038@mazaru.schibsted.cl', 'TRANS_OK', CURRENT_TIMESTAMP, NULL, 'cmd:admail
commit:1
ad_id:141
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (111, CURRENT_TIMESTAMP, '30038@mazaru.schibsted.cl', NULL, CURRENT_TIMESTAMP, '30038@mazaru.schibsted.cl', 'TRANS_OK', CURRENT_TIMESTAMP, NULL, 'cmd:admail
commit:1
ad_id:146
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (107, CURRENT_TIMESTAMP, '30038@mazaru.schibsted.cl', NULL, CURRENT_TIMESTAMP, '30038@mazaru.schibsted.cl', 'TRANS_OK', CURRENT_TIMESTAMP, NULL, 'cmd:admail
commit:1
ad_id:138
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (100, CURRENT_TIMESTAMP, '30038@mazaru.schibsted.cl', NULL, CURRENT_TIMESTAMP, '30038@mazaru.schibsted.cl', 'TRANS_OK', CURRENT_TIMESTAMP, NULL, 'cmd:admail
commit:1
ad_id:133
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (102, CURRENT_TIMESTAMP, '30038@mazaru.schibsted.cl', NULL, CURRENT_TIMESTAMP, '30038@mazaru.schibsted.cl', 'TRANS_OK', CURRENT_TIMESTAMP, NULL, 'cmd:admail
commit:1
ad_id:137
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (103, CURRENT_TIMESTAMP, '30038@mazaru.schibsted.cl', NULL, CURRENT_TIMESTAMP, '30038@mazaru.schibsted.cl', 'TRANS_OK', CURRENT_TIMESTAMP, NULL, 'cmd:admail
commit:1
ad_id:136
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (108, CURRENT_TIMESTAMP, '30038@mazaru.schibsted.cl', NULL, CURRENT_TIMESTAMP, '30038@mazaru.schibsted.cl', 'TRANS_OK', CURRENT_TIMESTAMP, NULL, 'cmd:admail
commit:1
ad_id:143
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (104, CURRENT_TIMESTAMP, '30038@mazaru.schibsted.cl', NULL, CURRENT_TIMESTAMP, '30038@mazaru.schibsted.cl', 'TRANS_OK', CURRENT_TIMESTAMP, NULL, 'cmd:admail
commit:1
ad_id:135
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (105, CURRENT_TIMESTAMP, '30038@mazaru.schibsted.cl', NULL, CURRENT_TIMESTAMP, '30038@mazaru.schibsted.cl', 'TRANS_OK', CURRENT_TIMESTAMP, NULL, 'cmd:admail
commit:1
ad_id:140
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (106, CURRENT_TIMESTAMP, '30038@mazaru.schibsted.cl', NULL, CURRENT_TIMESTAMP, '30038@mazaru.schibsted.cl', 'TRANS_OK', CURRENT_TIMESTAMP, NULL, 'cmd:admail
commit:1
ad_id:139
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (109, CURRENT_TIMESTAMP, '30038@mazaru.schibsted.cl', NULL, CURRENT_TIMESTAMP, '30038@mazaru.schibsted.cl', 'TRANS_OK', CURRENT_TIMESTAMP, NULL, 'cmd:admail
commit:1
ad_id:142
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (101, CURRENT_TIMESTAMP, '30038@mazaru.schibsted.cl', NULL, CURRENT_TIMESTAMP, '30038@mazaru.schibsted.cl', 'TRANS_OK', CURRENT_TIMESTAMP, NULL, 'cmd:admail
commit:1
ad_id:132
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (99, CURRENT_TIMESTAMP, '30038@mazaru.schibsted.cl', NULL, CURRENT_TIMESTAMP, '30038@mazaru.schibsted.cl', 'TRANS_OK', CURRENT_TIMESTAMP, NULL, 'cmd:admail
commit:1
ad_id:134
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (96, CURRENT_TIMESTAMP, '30038@mazaru.schibsted.cl', NULL, CURRENT_TIMESTAMP, '30038@mazaru.schibsted.cl', 'TRANS_OK', CURRENT_TIMESTAMP, NULL, 'cmd:admail
commit:1
ad_id:131
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (98, CURRENT_TIMESTAMP, '30038@mazaru.schibsted.cl', NULL, CURRENT_TIMESTAMP, '30038@mazaru.schibsted.cl', 'TRANS_OK', CURRENT_TIMESTAMP, NULL, 'cmd:admail
commit:1
ad_id:129
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (93, CURRENT_TIMESTAMP, '30038@mazaru.schibsted.cl', NULL, CURRENT_TIMESTAMP, '30038@mazaru.schibsted.cl', 'TRANS_OK', CURRENT_TIMESTAMP, NULL, 'cmd:admail
commit:1
ad_id:128
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (95, CURRENT_TIMESTAMP, '30038@mazaru.schibsted.cl', NULL, CURRENT_TIMESTAMP, '30038@mazaru.schibsted.cl', 'TRANS_OK', CURRENT_TIMESTAMP, NULL, 'cmd:admail
commit:1
ad_id:126
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (97, CURRENT_TIMESTAMP, '30038@mazaru.schibsted.cl', NULL, CURRENT_TIMESTAMP, '30038@mazaru.schibsted.cl', 'TRANS_OK', CURRENT_TIMESTAMP, NULL, 'cmd:admail
commit:1
ad_id:130
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
SELECT pg_catalog.setval('trans_queue_id_seq', 119, true);
INSERT INTO user_params (user_id, name, value) VALUES (59, 'first_approved_ad', CURRENT_TIMESTAMP);
INSERT INTO user_params (user_id, name, value) VALUES (68, 'first_approved_ad', CURRENT_TIMESTAMP);
INSERT INTO user_params (user_id, name, value) VALUES (69, 'first_approved_ad', CURRENT_TIMESTAMP);
INSERT INTO user_params (user_id, name, value) VALUES (71, 'first_approved_ad', CURRENT_TIMESTAMP);
INSERT INTO user_params (user_id, name, value) VALUES (72, 'first_approved_ad', CURRENT_TIMESTAMP);
INSERT INTO user_params (user_id, name, value) VALUES (73, 'first_approved_ad', CURRENT_TIMESTAMP);
INSERT INTO user_params (user_id, name, value) VALUES (74, 'first_approved_ad', CURRENT_TIMESTAMP);
INSERT INTO user_params (user_id, name, value) VALUES (75, 'first_approved_ad', CURRENT_TIMESTAMP);
INSERT INTO user_params (user_id, name, value) VALUES (76, 'first_approved_ad', CURRENT_TIMESTAMP);
INSERT INTO user_params (user_id, name, value) VALUES (77, 'first_approved_ad', CURRENT_TIMESTAMP);
INSERT INTO user_params (user_id, name, value) VALUES (59, 'first_inserted_ad', CURRENT_TIMESTAMP);
INSERT INTO user_params (user_id, name, value) VALUES (68, 'first_inserted_ad', CURRENT_TIMESTAMP);
INSERT INTO user_params (user_id, name, value) VALUES (69, 'first_inserted_ad', CURRENT_TIMESTAMP);
INSERT INTO user_params (user_id, name, value) VALUES (71, 'first_inserted_ad', CURRENT_TIMESTAMP);
INSERT INTO user_params (user_id, name, value) VALUES (72, 'first_inserted_ad', CURRENT_TIMESTAMP);
INSERT INTO user_params (user_id, name, value) VALUES (73, 'first_inserted_ad', CURRENT_TIMESTAMP);
INSERT INTO user_params (user_id, name, value) VALUES (74, 'first_inserted_ad', CURRENT_TIMESTAMP);
INSERT INTO user_params (user_id, name, value) VALUES (75, 'first_inserted_ad', CURRENT_TIMESTAMP);
INSERT INTO user_params (user_id, name, value) VALUES (76, 'first_inserted_ad', CURRENT_TIMESTAMP);
INSERT INTO user_params (user_id, name, value) VALUES (77, 'first_inserted_ad', CURRENT_TIMESTAMP);
SELECT pg_catalog.setval('users_user_id_seq', 77, true);
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (155, 8000112, CURRENT_TIMESTAMP, 'active', 'sell', '1_inserted_keep_whitelist@yapo.cl', '962184762', 15, 0, 4040, 69, NULL, false, false, false, 'Peluca de loro con sabores', 'Especial para juegos con desarrolladores :D', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$GO3P) `|ToBQxb(,e5be7a081673e186fe379dc9bfef6a683b5eca42', CURRENT_TIMESTAMP, 'es');
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (154, 8000113, CURRENT_TIMESTAMP, 'active', 'sell', '1_inserted_keep_whitelist@yapo.cl', '962184762', 15, 0, 3020, 69, NULL, false, false, false, 'Xbox 720', 'consola de nueva generacion que te mata de verdad :D', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$GO3P) `|ToBQxb(,e5be7a081673e186fe379dc9bfef6a683b5eca42', CURRENT_TIMESTAMP, 'es');
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (153, 8000114, CURRENT_TIMESTAMP, 'active', 'sell', '1_inserted_keep_whitelist@yapo.cl', '962184762', 15, 0, 5020, 69, NULL, false, false, false, 'Mesa de centro pedro picapiedra', 'Es de la era prehistorica y esas cosas', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$GO3P) `|ToBQxb(,e5be7a081673e186fe379dc9bfef6a683b5eca42', CURRENT_TIMESTAMP, 'es');
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (193, '633026231', 'verified', CURRENT_TIMESTAMP, NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (194, '768418254', 'verified', CURRENT_TIMESTAMP, NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (195, '903810277', 'verified', CURRENT_TIMESTAMP, NULL);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (155, 1, 'new', 817, 'accepted', 'normal', NULL, NULL, 195);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (154, 1, 'new', 818, 'accepted', 'normal', NULL, NULL, 194);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (153, 1, 'new', 819, 'accepted', 'normal', NULL, NULL, 193);
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (153, 1, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (153, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (154, 1, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (154, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (155, 1, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (155, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (933, 'X55242baf5d908383000000006960e38300000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.164', 'authenticate', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (934, 'X55242bafaf29749000000006900722800000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.164', 'spamfilter', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (935, 'X55242bb16715ebfc0000000014446a8500000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.164', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (936, 'X55242bb374ceb0a50000000013e3e51c00000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.164', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (937, 'X55242bb54aa6d042000000005bfd390500000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.164', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (938, 'X55242bb534e851c20000000077b2c00900000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.164', 'blocked_item', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (939, 'X55242bc7541f2d1300000000391fac6c00000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.164', 'spamfilter', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (940, 'X55242bc87e46c2c7000000001a7d4d0200000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.164', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (941, 'X55242bca56c71d3a000000004064e32800000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.164', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (942, 'X55242bcb1d52b5fb0000000059f0a1b400000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.164', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (943, 'X55242bcb37acd06c0000000076987fd000000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.164', 'blocked_item', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (944, 'X55242bcd709e1c340000000027aee84200000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.164', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (945, 'X55242bcdc85beb700000000f3865f000000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.164', 'blocked_item', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (946, 'X55242bea20378c1d0000000074151dd600000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.164', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (947, 'X55242bea23673fc00000000010fa44a700000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.164', 'blocked_item', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (948, 'X55242bee10173eb800000000f7c34300000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.164', 'spamfilter', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (949, 'X55242bf17a5b282a000000001b09d60100000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.164', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (950, 'X55242bf369f8356b000000006171142600000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.164', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (951, 'X55242bff2f4e4086000000005ec6e61000000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.164', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (952, 'X55242bff7554f9430000000079f510c800000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.164', 'blocked_item', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (953, 'X55242c013ac41f16000000002a3d4b0500000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.164', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (954, 'X55242c0171a7d0d100000000ee34c2900000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.164', 'blocked_item', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (955, 'X55242c08635cf771000000006fee939800000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.164', 'spamfilter', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (956, 'X55242c0a2960992b000000003a2414ab00000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.164', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (957, 'X55242c0d305376c00000000046b34f2600000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.164', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (958, 'X55242c111414b660000000006800472d00000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.164', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (959, 'X55242c113d4bcef6000000004b2d29400000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.164', 'blocked_item', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (960, 'X55242ff370cb2a9d0000000069ba6aba00000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'authenticate', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (961, 'X55242ff4c533bbb00000000241084b000000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'spamfilter', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (962, 'X552430343face1090000000046e41c8900000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.198', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (963, 'X552433877089111b000000004a249dfe00000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.164', 'authenticate', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (964, 'X5524338856f7029b0000000012544d6500000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.164', 'spamfilter', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (965, 'X5524338a191680c6000000004e2df68700000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.164', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (966, 'X5524338c73756595000000002ade81f200000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.164', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (967, 'X552433a476f3bb380000000079f8c0d900000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.164', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (968, 'X552433a424a7e609000000009adeb2700000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.164', 'blocked_item', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (969, 'X552433b84eda92540000000060768cdc00000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.164', 'spamfilter', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (970, 'X552433ba7441d65600000000196be6d600000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.164', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (971, 'X552433bc7695a91800000000da64de200000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.164', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (972, 'X552433c0318fbc300000000332f02e200000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.164', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (973, 'X552433c02be955d10000000014d6a0f500000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.164', 'blocked_item', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (974, 'X55243593201a8ee9000000001747821800000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.164', 'spamfilter', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (975, 'X552435963b576e770000000058b3ecb300000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.164', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (976, 'X5524359c11afe7cb000000001da6163600000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.164', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (977, 'X552435a218a1ebea0000000038ea738f00000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.164', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (978, 'X552435a27e0bbbf700000000175cecfa00000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.164', 'blocked_item', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (979, 'X552435aa474b90e40000000017f7955100000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.164', 'blocked_item', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (980, 'X552435aa46b536b7000000002d5f64a600000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.164', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (981, 'X552435af7120b981000000004e51fa2e00000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.164', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (982, 'X552435af6e6c8917000000007d5ea5a700000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.164', 'blocked_item', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (983, 'X552435b95612925a0000000068e6d3c100000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.164', 'spamfilter', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (984, 'X552435ba68ce9f7000000001a78eb600000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.164', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (985, 'X552435bc3a920245000000001ab7599700000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.164', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (986, 'X552435c132144692000000001faab2fe00000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.164', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (987, 'X552435c137a2484f00000000307cc6600000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.164', 'blocked_item', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (988, 'X5524365e7da04c27000000006b4bf05200000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.164', 'spamfilter', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (989, 'X55243661767cf7d60000000039a94c4100000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.164', 'listadmins', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (990, 'X5524366a432e60bb00000000355a428800000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.164', 'listadmins', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (991, 'X5524366ad9599fb0000000072e4ae4800000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.164', 'search_ads', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (992, 'X5524366c2a2bff67000000006d48d4f000000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.164', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (993, 'X5524366c6c29fa20000000004b48265c00000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.164', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (994, 'X5524366c15f4b400000000004404ac1200000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.164', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (995, 'X5524366c5eb23d7700000000f545d4700000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.164', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (996, 'X5524366d7aa04d990000000029714b4100000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.164', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (997, 'X5524366d631a978200000000138cf17100000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.164', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (998, 'X5524366d48e733f9000000004937446500000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.164', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (999, 'X5524366d3c1bf2ad00000000494b721500000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.164', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (1000, 'X5524366e36944706000000005bb1719100000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.164', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (1001, 'X5524366e26f997ee000000002f8d6e5700000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.164', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (1002, 'X5524366e73921a690000000012b8641a00000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.164', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (1003, 'X5524366e220cc161000000003c37049900000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.164', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (1004, 'X55243674dca546a0000000048c2e75d00000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.164', 'review', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (1005, 'X55243679521fd84f000000002ea55200000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.164', 'review', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (1006, 'X5524367e61ee34c100000000336b6d6a00000000', 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '10.0.1.164', 'review', NULL, NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (153, 1, 808, 'reg', 'initial', CURRENT_TIMESTAMP, '10.0.1.198', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (153, 1, 809, 'unverified', 'verifymail', CURRENT_TIMESTAMP, '10.0.1.198', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (153, 1, 810, 'pending_review', 'verify', CURRENT_TIMESTAMP, '10.0.1.198', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (154, 1, 811, 'reg', 'initial', CURRENT_TIMESTAMP, '10.0.1.198', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (154, 1, 812, 'unverified', 'verifymail', CURRENT_TIMESTAMP, '10.0.1.198', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (154, 1, 813, 'pending_review', 'verify', CURRENT_TIMESTAMP, '10.0.1.198', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (155, 1, 814, 'reg', 'initial', CURRENT_TIMESTAMP, '10.0.1.198', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (155, 1, 815, 'unverified', 'verifymail', CURRENT_TIMESTAMP, '10.0.1.198', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (155, 1, 816, 'pending_review', 'verify', CURRENT_TIMESTAMP, '10.0.1.198', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (155, 1, 817, 'accepted', 'accept', CURRENT_TIMESTAMP, '10.0.1.164', 1003);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (154, 1, 818, 'accepted', 'accept', CURRENT_TIMESTAMP, '10.0.1.164', 1004);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (153, 1, 819, 'accepted', 'accept', CURRENT_TIMESTAMP, '10.0.1.164', 1005);
SELECT pg_catalog.setval('action_states_state_id_seq', 819, true);
INSERT INTO ad_codes (code, code_type) VALUES (79362, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (21385, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (53408, 'pay');
INSERT INTO ad_image_changes (ad_id, action_id, state_id, seq_no, name, is_new) VALUES (155, 1, 817, 0, NULL, false);
INSERT INTO ad_image_changes (ad_id, action_id, state_id, seq_no, name, is_new) VALUES (154, 1, 818, 0, NULL, false);
INSERT INTO ad_image_changes (ad_id, action_id, state_id, seq_no, name, is_new) VALUES (153, 1, 819, 0, NULL, false);
INSERT INTO ad_params (ad_id, name, value) VALUES (153, 'prev_currency', 'peso');
INSERT INTO ad_params (ad_id, name, value) VALUES (153, 'country', 'UNK');
INSERT INTO ad_params (ad_id, name, value) VALUES (154, 'prev_currency', 'peso');
INSERT INTO ad_params (ad_id, name, value) VALUES (154, 'country', 'UNK');
INSERT INTO ad_params (ad_id, name, value) VALUES (155, 'gender', '1');
INSERT INTO ad_params (ad_id, name, value) VALUES (155, 'condition', '1');
INSERT INTO ad_params (ad_id, name, value) VALUES (155, 'prev_currency', 'peso');
INSERT INTO ad_params (ad_id, name, value) VALUES (155, 'country', 'UNK');
SELECT pg_catalog.setval('ads_ad_id_seq', 155, true);
INSERT INTO event_log (event_id, event_name, event, token_id, "time") VALUES (18, 'blocked_item', 'DELETE list ', 978, CURRENT_TIMESTAMP);
SELECT pg_catalog.setval('event_log_event_id_seq', 18, true);
SELECT pg_catalog.setval('list_id_seq', 8000114, true);
SELECT pg_catalog.setval('pay_code_seq', 496, true);
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (267, 'save', 0, '633026231', NULL, CURRENT_TIMESTAMP, 193, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (268, 'verify', 0, '633026231', NULL, CURRENT_TIMESTAMP, 193, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (269, 'save', 0, '768418254', NULL, CURRENT_TIMESTAMP, 194, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (270, 'verify', 0, '768418254', NULL, CURRENT_TIMESTAMP, 194, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (271, 'save', 0, '903810277', NULL, CURRENT_TIMESTAMP, 195, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (272, 'verify', 0, '903810277', NULL, CURRENT_TIMESTAMP, 195, 'OK');
SELECT pg_catalog.setval('pay_log_pay_log_id_seq', 272, true);
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (267, 0, 'adphone', '962184762');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (267, 1, 'remote_addr', '10.0.1.198');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (268, 0, 'remote_addr', '10.0.1.198');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (269, 0, 'adphone', '962184762');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (269, 1, 'remote_addr', '10.0.1.198');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (270, 0, 'remote_addr', '10.0.1.198');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (271, 0, 'adphone', '962184762');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (271, 1, 'remote_addr', '10.0.1.198');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (272, 0, 'remote_addr', '10.0.1.198');
SELECT pg_catalog.setval('payment_groups_payment_group_id_seq', 195, true);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (193, 'ad_action', 0, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (194, 'ad_action', 0, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (195, 'ad_action', 0, 0);
INSERT INTO review_log (ad_id, action_id, admin_id, review_time, queue, action_type, action, refusal_reason_text, category) VALUES (155, 1, 9, CURRENT_TIMESTAMP, 'all', 'new', 'accepted', NULL, 4040);
INSERT INTO review_log (ad_id, action_id, admin_id, review_time, queue, action_type, action, refusal_reason_text, category) VALUES (154, 1, 9, CURRENT_TIMESTAMP, 'all', 'new', 'accepted', NULL, 3020);
INSERT INTO review_log (ad_id, action_id, admin_id, review_time, queue, action_type, action, refusal_reason_text, category) VALUES (153, 1, 9, CURRENT_TIMESTAMP, 'all', 'new', 'accepted', NULL, 5020);
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (153, 1, 810, 'queue', 'normal');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (154, 1, 813, 'queue', 'normal');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (155, 1, 816, 'queue', 'normal');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (155, 1, 817, 'filter_name', 'all');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (154, 1, 818, 'filter_name', 'all');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (153, 1, 819, 'filter_name', 'all');
SELECT pg_catalog.setval('tokens_token_id_seq', 9999, true);
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (120, CURRENT_TIMESTAMP, '1137@mazaru.schibsted.cl', NULL, CURRENT_TIMESTAMP, '1137@mazaru.schibsted.cl', 'TRANS_OK', CURRENT_TIMESTAMP, NULL, 'cmd:admail
commit:1
ad_id:155
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (122, CURRENT_TIMESTAMP, '1137@mazaru.schibsted.cl', NULL, CURRENT_TIMESTAMP, '1137@mazaru.schibsted.cl', 'TRANS_OK', CURRENT_TIMESTAMP, NULL, 'cmd:admail
commit:1
ad_id:153
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (121, CURRENT_TIMESTAMP, '1137@mazaru.schibsted.cl', NULL, CURRENT_TIMESTAMP, '1137@mazaru.schibsted.cl', 'TRANS_OK', CURRENT_TIMESTAMP, NULL, 'cmd:admail
commit:1
ad_id:154
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
SELECT pg_catalog.setval('trans_queue_id_seq', 122, true);
INSERT INTO users (user_id, uid, email, account, paid_total) VALUES (78, 5, 'edit_with_badwords@yapo.cl', 0, 0);
INSERT INTO users (user_id, uid, email, account, paid_total) VALUES (79, 5, 'edit_add_image@yapo.cl', 0, 0);
INSERT INTO users (user_id, uid, email, account, paid_total) VALUES (80, 5, 'edit_nothing@yapo.cl', 0, 0);
INSERT INTO users (user_id, uid, email, account, paid_total) VALUES (81, 5, 'edit_nothing_free_car@yapo.cl', 0, 0);
INSERT INTO users (user_id, uid, email, account, paid_total) VALUES (82, 9, 'edit_nothing_regular_car@yapo.cl', 0, 0);
INSERT INTO users (user_id, uid, email, account, paid_total) VALUES (83, 5, 'edit_lower_car_price_still_green@yapo.cl', 0, 0);
INSERT INTO users (user_id, uid, email, account, paid_total) VALUES (84, 5, 'edit_lower_car_price_not_green@yapo.cl', 0, 0);
INSERT INTO users (user_id, uid, email, account, paid_total) VALUES (85, 5, 'edit_refuse@yapo.cl', 0, 0);
INSERT INTO accounts (account_id, name, rut, email, phone, is_company, region, salted_passwd, creation_date, activation_date, status, address, contact, lob, commune, user_id) VALUES (27, 'usuario orinario', '', 'edit_with_badwords@yapo.cl', '962184762', false, 15, '$1024$#[jcKeL8?F$q*W9I1467edc2984428e432c08339d6df4df9b57ee6d9', '2015-04-28 11:32:04.149067', NULL, 'active', NULL, NULL, NULL, NULL, 78);
INSERT INTO accounts (account_id, name, rut, email, phone, is_company, region, salted_passwd, creation_date, activation_date, status, address, contact, lob, commune, user_id) VALUES (28, 'usuario imaginativo', '', 'edit_add_image@yapo.cl', '962184762', false, 15, '$1024$39DCq;bhQ}K--.ihbcec1fa4d928b0d9b46625bd89b9b87978bf0269', '2015-04-28 13:08:41.011176', NULL, 'active', NULL, NULL, NULL, NULL, 79);
INSERT INTO accounts (account_id, name, rut, email, phone, is_company, region, salted_passwd, creation_date, activation_date, status, address, contact, lob, commune, user_id) VALUES (29, 'usuario imaginativo', '', 'edit_nothing@yapo.cl', '962184762', false, 15, '$1024$o.n$"RqIX4Bz,n.j21a0601d7cdd5d547fe32301d44d3ddd3ff3a5ed', '2015-04-28 13:09:30.524483', NULL, 'active', NULL, NULL, NULL, NULL, 80);
INSERT INTO accounts (account_id, name, rut, email, phone, is_company, region, salted_passwd, creation_date, activation_date, status, address, contact, lob, commune, user_id) VALUES (30, 'usuario auto gratis', '', 'edit_nothing_free_car@yapo.cl', '962184762', false, 15, '$1024$,);j>SU`I&qUc2@dd8a1b68398847c470771ed2cf8d3bfcc1ec45193', '2015-04-28 13:10:44.336179', NULL, 'active', NULL, NULL, NULL, NULL, 81);
INSERT INTO accounts (account_id, name, rut, email, phone, is_company, region, salted_passwd, creation_date, activation_date, status, address, contact, lob, commune, user_id) VALUES (31, 'usuario auto gratis', '', 'edit_nothing_regular_car@yapo.cl', '962184762', false, 15, '$1024$YU7t=2gS2YjwI>Lkdf1c51cb98a4f89f5653239fa662fa930494b349', '2015-04-28 13:12:03.575711', NULL, 'active', NULL, NULL, NULL, NULL, 82);
INSERT INTO accounts (account_id, name, rut, email, phone, is_company, region, salted_passwd, creation_date, activation_date, status, address, contact, lob, commune, user_id) VALUES (32, 'usuario auto gratis', '', 'edit_lower_car_price_still_green@yapo.cl', '962184762', false, 15, '$1024$ghg2n=yE"1x9jF./cd7811b7102cd360ad0f9a3402817522b093baef', '2015-04-28 13:13:33.787315', NULL, 'active', NULL, NULL, NULL, NULL, 83);
INSERT INTO accounts (account_id, name, rut, email, phone, is_company, region, salted_passwd, creation_date, activation_date, status, address, contact, lob, commune, user_id) VALUES (33, 'usuario auto green to ng', '', 'edit_lower_car_price_not_green@yapo.cl', '962184762', false, 15, '$1024$QO-Kd@8Ow$Rg7''u@eb880fb04644dc97af72c6e66fa63e72afdf8b6b', '2015-04-28 13:15:58.596535', NULL, 'active', NULL, NULL, NULL, NULL, 84);
INSERT INTO accounts (account_id, name, rut, email, phone, is_company, region, salted_passwd, creation_date, activation_date, status, address, contact, lob, commune, user_id) VALUES (34, 'usuario auto green to ng', '', 'edit_refuse@yapo.cl', '962184762', false, 15, '$1024$DHEj EcSW/tJPNYze930904ed9ae15b365a2da32d33d5847f7820780', '2015-04-28 13:16:45.120783', NULL, 'active', NULL, NULL, NULL, NULL, 85);
SELECT pg_catalog.setval('accounts_account_id_seq', 34, true);
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (156, 8000115, '2015-04-28 13:28:19', 'active', 'sell', 'usuario orinario', '962184762', 15, 0, 5020, 78, NULL, false, false, false, 'Aviso para badwords', 'Aviso oridionario inserte insulto aqui:', 13000, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$#[jcKeL8?F$q*W9I1467edc2984428e432c08339d6df4df9b57ee6d9', '2015-04-28 13:28:19.817803', 'es');
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (157, 8000116, '2015-04-28 13:28:30', 'active', 'sell', 'usuario imaginativo', '962184762', 15, 0, 5020, 79, NULL, false, false, false, 'Aviso to add image', 'agregando imagen', 13000, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$39DCq;bhQ}K--.ihbcec1fa4d928b0d9b46625bd89b9b87978bf0269', '2015-04-28 13:28:30.447528', 'es');
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (158, 8000117, '2015-04-28 13:28:40', 'active', 'sell', 'usuario imaginativo', '962184762', 15, 0, 5020, 80, NULL, false, false, false, 'Aviso para hacer nada', 'nada de nada', 13000, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$o.n$"RqIX4Bz,n.j21a0601d7cdd5d547fe32301d44d3ddd3ff3a5ed', '2015-04-28 13:28:40.290236', 'es');
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (159, 8000118, '2015-04-28 13:28:49', 'active', 'sell', 'usuario auto gratis', '962184762', 15, 0, 2020, 81, NULL, false, false, false, 'Auto gratis sin cambios', 'gratis', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$,);j>SU`I&qUc2@dd8a1b68398847c470771ed2cf8d3bfcc1ec45193', '2015-04-28 13:28:49.063185', 'es');
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (160, 8000119, '2015-04-28 13:29:13', 'active', 'sell', 'usuario auto gratis', '962184762', 15, 0, 2020, 82, NULL, false, false, false, 'Auto normal no edit', 'auto normal', 8000000, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$YU7t=2gS2YjwI>Lkdf1c51cb98a4f89f5653239fa662fa930494b349', '2015-04-28 13:29:13.395206', 'es');
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (161, 8000120, '2015-04-28 13:29:20', 'active', 'sell', 'usuario auto gratis', '962184762', 15, 0, 2020, 83, NULL, false, false, false, 'Auto verde lower price', 'verde to verde', 8000000, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$ghg2n=yE"1x9jF./cd7811b7102cd360ad0f9a3402817522b093baef', '2015-04-28 13:29:20.944876', 'es');
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (162, 8000121, '2015-04-28 13:29:27', 'active', 'sell', 'usuario auto green to ng', '962184762', 15, 0, 2020, 84, NULL, false, false, false, 'Auto green para no green', 'lower to not green', 8000000, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$QO-Kd@8Ow$Rg7''u@eb880fb04644dc97af72c6e66fa63e72afdf8b6b', '2015-04-28 13:29:27.726325', 'es');
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (163, 8000122, '2015-04-28 13:29:35', 'active', 'sell', 'usuario auto green to ng', '962184762', 15, 0, 5020, 85, NULL, false, false, false, 'Aviso to edit refuse', 'hola', 123000, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$DHEj EcSW/tJPNYze930904ed9ae15b365a2da32d33d5847f7820780', '2015-04-28 13:29:35.407876', 'es');
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (196, '633026231', 'verified', '2015-04-28 11:32:04.070554', NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (197, '768418254', 'verified', '2015-04-28 13:08:40.915794', NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (198, '903810277', 'verified', '2015-04-28 13:09:30.4803', NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (199, '139202300', 'verified', '2015-04-28 13:10:44.287547', NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (200, '274594323', 'verified', '2015-04-28 13:12:03.483474', NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (201, '409986346', 'verified', '2015-04-28 13:13:33.695264', NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (202, '545378369', 'verified', '2015-04-28 13:15:58.531462', NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (203, '680770392', 'verified', '2015-04-28 13:16:45.080055', NULL);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (163, 1, 'new', 851, 'accepted', 'normal', NULL, NULL, 203);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (156, 1, 'new', 844, 'accepted', 'normal', NULL, NULL, 196);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (157, 1, 'new', 845, 'accepted', 'normal', NULL, NULL, 197);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (158, 1, 'new', 846, 'accepted', 'normal', NULL, NULL, 198);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (159, 1, 'new', 847, 'accepted', 'normal', NULL, NULL, 199);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (160, 1, 'new', 848, 'accepted', 'normal', NULL, NULL, 200);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (161, 1, 'new', 849, 'accepted', 'normal', NULL, NULL, 201);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (162, 1, 'new', 850, 'accepted', 'normal', NULL, NULL, 202);
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (156, 1, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (156, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (157, 1, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (157, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (158, 1, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (158, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (159, 1, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (159, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (160, 1, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (160, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (161, 1, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (161, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (162, 1, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (162, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (163, 1, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (163, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (1011, 'X553fc2f168db36f5000000005ffb64ad00000000', 9, '2015-04-28 13:27:13.369257', '2015-04-28 13:27:13.560113', '10.0.1.117', 'authenticate', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (1012, 'X553fc2f2737f8d2100000000758d6a1900000000', 9, '2015-04-28 13:27:13.560113', '2015-04-28 13:27:15.116519', '10.0.1.117', 'spamfilter', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (1013, 'X553fc2f33db765d2000000009fa543a00000000', 9, '2015-04-28 13:27:15.116519', '2015-04-28 13:27:22.992637', '10.0.1.117', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (1014, 'X553fc2fb6b7df5cf000000007f61e3af00000000', 9, '2015-04-28 13:27:22.992637', '2015-04-28 13:27:25.973673', '10.0.1.117', 'spamfilter', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (1015, 'X553fc2fe945f709000000002d274b7e00000000', 9, '2015-04-28 13:27:25.973673', '2015-04-28 13:27:32.199058', '10.0.1.117', 'listadmins', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (1016, 'X553fc304390ecaf1000000007cef78dd00000000', 9, '2015-04-28 13:27:32.199058', '2015-04-28 13:27:32.220167', '10.0.1.117', 'listadmins', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (1017, 'X553fc3042ffc5d62000000003693634f00000000', 9, '2015-04-28 13:27:32.220167', '2015-04-28 13:27:40.515154', '10.0.1.117', 'search_ads', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (1018, 'X553fc30d62c85ec2000000005f5f195800000000', 9, '2015-04-28 13:27:40.515154', '2015-04-28 13:27:40.53505', '10.0.1.117', 'listadmins', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (1019, 'X553fc30d483a504e0000000043b687f200000000', 9, '2015-04-28 13:27:40.53505', '2015-04-28 13:28:01.202644', '10.0.1.117', 'search_ads', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (1020, 'X553fc3213943f20b0000000063b2cab800000000', 9, '2015-04-28 13:28:01.202644', '2015-04-28 13:28:01.216573', '10.0.1.117', 'listadmins', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (1021, 'X553fc3216dad1ef2000000002c27b4ec00000000', 9, '2015-04-28 13:28:01.216573', '2015-04-28 13:28:14.216461', '10.0.1.117', 'search_ads', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (1022, 'X553fc32e2b89989b00000000fa1067b00000000', 9, '2015-04-28 13:28:14.216461', '2015-04-28 13:28:14.323478', '10.0.1.117', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (1023, 'X553fc32e3cc851fa000000005eb071b900000000', 9, '2015-04-28 13:28:14.323478', '2015-04-28 13:28:14.332175', '10.0.1.117', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (1024, 'X553fc32e5b9fea96000000001f4f597100000000', 9, '2015-04-28 13:28:14.332175', '2015-04-28 13:28:14.339239', '10.0.1.117', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (1025, 'X553fc32e55e2bcc000000000155338f200000000', 9, '2015-04-28 13:28:14.339239', '2015-04-28 13:28:19.811424', '10.0.1.117', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (1026, 'X553fc33474b2524b000000002d0c054400000000', 9, '2015-04-28 13:28:19.811424', '2015-04-28 13:28:23.966083', '10.0.1.117', 'review', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (1027, 'X553fc338588ce50c0000000063a2a76700000000', 9, '2015-04-28 13:28:23.966083', '2015-04-28 13:28:24.044139', '10.0.1.117', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (1028, 'X553fc338472058d6000000006a5d348300000000', 9, '2015-04-28 13:28:24.044139', '2015-04-28 13:28:24.05507', '10.0.1.117', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (1029, 'X553fc3383f963df4000000003522ff200000000', 9, '2015-04-28 13:28:24.05507', '2015-04-28 13:28:24.06517', '10.0.1.117', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (1030, 'X553fc3387ebf1319000000004afee0d700000000', 9, '2015-04-28 13:28:24.06517', '2015-04-28 13:28:30.438978', '10.0.1.117', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (1031, 'X553fc33e5b099b5e000000001bad3e8d00000000', 9, '2015-04-28 13:28:30.438978', '2015-04-28 13:28:35.959121', '10.0.1.117', 'review', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (1032, 'X553fc34470c28d360000000018bd102800000000', 9, '2015-04-28 13:28:35.959121', '2015-04-28 13:28:36.023658', '10.0.1.117', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (1033, 'X553fc3447c6fdae0000000007df0268900000000', 9, '2015-04-28 13:28:36.023658', '2015-04-28 13:28:36.030864', '10.0.1.117', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (1034, 'X553fc3442a17db75000000005b6fe6dc00000000', 9, '2015-04-28 13:28:36.030864', '2015-04-28 13:28:36.038508', '10.0.1.117', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (1035, 'X553fc3446b10ed5800000000568982b300000000', 9, '2015-04-28 13:28:36.038508', '2015-04-28 13:28:40.284758', '10.0.1.117', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (1036, 'X553fc3483539f46c0000000024569eeb00000000', 9, '2015-04-28 13:28:40.284758', '2015-04-28 13:28:44.030515', '10.0.1.117', 'review', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (1037, 'X553fc34c17ccecad000000001e74db1500000000', 9, '2015-04-28 13:28:44.030515', '2015-04-28 13:28:44.097653', '10.0.1.117', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (1038, 'X553fc34c6960231200000000343bc08700000000', 9, '2015-04-28 13:28:44.097653', '2015-04-28 13:28:44.105042', '10.0.1.117', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (1039, 'X553fc34c414b9cb2000000007003165f00000000', 9, '2015-04-28 13:28:44.105042', '2015-04-28 13:28:44.113114', '10.0.1.117', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (1040, 'X553fc34c53a5bdc6000000002370b21300000000', 9, '2015-04-28 13:28:44.113114', '2015-04-28 13:28:49.05766', '10.0.1.117', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (1041, 'X553fc351dcde6960000000040ff4c4100000000', 9, '2015-04-28 13:28:49.05766', '2015-04-28 13:28:55.163483', '10.0.1.117', 'review', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (1052, 'X553fc36e44517c340000000058c70eb600000000', 9, '2015-04-28 13:29:17.602176', '2015-04-28 13:29:17.677227', '10.0.1.117', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (1053, 'X553fc36e23c5ef8d00000000706be32000000000', 9, '2015-04-28 13:29:17.677227', '2015-04-28 13:29:17.686004', '10.0.1.117', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (1054, 'X553fc36ec1921ad000000002e6c6b9000000000', 9, '2015-04-28 13:29:17.686004', '2015-04-28 13:29:17.694121', '10.0.1.117', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (1055, 'X553fc36e4ce146a5000000007710438200000000', 9, '2015-04-28 13:29:17.694121', '2015-04-28 13:29:20.93919', '10.0.1.117', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (1056, 'X553fc3712b4c040a00000000552b37af00000000', 9, '2015-04-28 13:29:20.93919', '2015-04-28 13:29:24.437351', '10.0.1.117', 'review', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (1062, 'X553fc37c452e4e0e000000001eafbd3200000000', 9, '2015-04-28 13:29:32.007673', '2015-04-28 13:29:32.071865', '10.0.1.117', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (1063, 'X553fc37c42206f460000000037df5c8f00000000', 9, '2015-04-28 13:29:32.071865', '2015-04-28 13:29:32.078773', '10.0.1.117', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (1064, 'X553fc37c78dea8d100000000445e6be100000000', 9, '2015-04-28 13:29:32.078773', '2015-04-28 13:29:32.086366', '10.0.1.117', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (1065, 'X553fc37c1d257a970000000061e219af00000000', 9, '2015-04-28 13:29:32.086366', '2015-04-28 13:29:35.402486', '10.0.1.117', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (1066, 'X553fc37f40eb6a2400000000524dfcd000000000', 9, '2015-04-28 13:29:35.402486', '2015-04-28 13:29:39.833646', '10.0.1.117', 'review', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (1042, 'X553fc35743a5f85c00000000361cd94500000000', 9, '2015-04-28 13:28:55.163483', '2015-04-28 13:28:55.233376', '10.0.1.117', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (1043, 'X553fc3574b7012370000000065c708fa00000000', 9, '2015-04-28 13:28:55.233376', '2015-04-28 13:28:55.243026', '10.0.1.117', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (1044, 'X553fc35712d30e3e000000002936302700000000', 9, '2015-04-28 13:28:55.243026', '2015-04-28 13:28:55.251358', '10.0.1.117', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (1045, 'X553fc35741f3404f0000000075b5872400000000', 9, '2015-04-28 13:28:55.251358', '2015-04-28 13:29:01.744219', '10.0.1.117', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (1046, 'X553fc35e73a5adad000000004e9df1d700000000', 9, '2015-04-28 13:29:01.744219', '2015-04-28 13:29:07.731954', '10.0.1.117', 'review', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (1047, 'X553fc3642a0dd8b3000000006bc8239600000000', 9, '2015-04-28 13:29:07.731954', '2015-04-28 13:29:07.802605', '10.0.1.117', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (1048, 'X553fc3644251a649000000007dd000e900000000', 9, '2015-04-28 13:29:07.802605', '2015-04-28 13:29:07.807421', '10.0.1.117', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (1049, 'X553fc36422269fd4000000005aa8619a00000000', 9, '2015-04-28 13:29:07.807421', '2015-04-28 13:29:07.811939', '10.0.1.117', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (1050, 'X553fc36410c53adf0000000024dba0b400000000', 9, '2015-04-28 13:29:07.811939', '2015-04-28 13:29:13.388979', '10.0.1.117', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (1051, 'X553fc369aa2a9af0000000051891dec00000000', 9, '2015-04-28 13:29:13.388979', '2015-04-28 13:29:17.602176', '10.0.1.117', 'review', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (1057, 'X553fc3747abf4e14000000001966b8b500000000', 9, '2015-04-28 13:29:24.437351', '2015-04-28 13:29:24.503665', '10.0.1.117', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (1058, 'X553fc375f1c3fd9000000004ea76e1900000000', 9, '2015-04-28 13:29:24.503665', '2015-04-28 13:29:24.510752', '10.0.1.117', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (1059, 'X553fc3751d455ff0000000002395be9500000000', 9, '2015-04-28 13:29:24.510752', '2015-04-28 13:29:24.518642', '10.0.1.117', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (1060, 'X553fc375f5de22b0000000013de73dc00000000', 9, '2015-04-28 13:29:24.518642', '2015-04-28 13:29:27.723134', '10.0.1.117', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (1061, 'X553fc37811ae74c500000000196a040d00000000', 9, '2015-04-28 13:29:27.723134', '2015-04-28 13:29:32.007673', '10.0.1.117', 'review', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (1067, 'X553fc384741265a70000000051a09ce300000000', 9, '2015-04-28 13:29:39.833646', '2015-04-28 13:29:39.853897', '10.0.1.117', 'listadmins', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (1068, 'X553fc38419a1acca000000004d7a08700000000', 9, '2015-04-28 13:29:39.853897', '2015-04-28 14:29:39.853897', '10.0.1.117', 'search_ads', NULL, NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (156, 1, 820, 'reg', 'initial', '2015-04-28 11:32:04.070554', '10.0.1.117', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (156, 1, 821, 'unverified', 'verifymail', '2015-04-28 11:32:04.070554', '10.0.1.117', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (156, 1, 822, 'pending_review', 'verify', '2015-04-28 11:32:04.19121', '10.0.1.117', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (157, 1, 823, 'reg', 'initial', '2015-04-28 13:08:40.915794', '10.0.1.117', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (157, 1, 824, 'unverified', 'verifymail', '2015-04-28 13:08:40.915794', '10.0.1.117', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (157, 1, 825, 'pending_review', 'verify', '2015-04-28 13:08:41.054665', '10.0.1.117', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (158, 1, 826, 'reg', 'initial', '2015-04-28 13:09:30.4803', '10.0.1.117', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (158, 1, 827, 'unverified', 'verifymail', '2015-04-28 13:09:30.4803', '10.0.1.117', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (158, 1, 828, 'pending_review', 'verify', '2015-04-28 13:09:30.558276', '10.0.1.117', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (159, 1, 829, 'reg', 'initial', '2015-04-28 13:10:44.287547', '10.0.1.117', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (159, 1, 830, 'unverified', 'verifymail', '2015-04-28 13:10:44.287547', '10.0.1.117', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (159, 1, 831, 'pending_review', 'verify', '2015-04-28 13:10:44.357674', '10.0.1.117', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (160, 1, 832, 'reg', 'initial', '2015-04-28 13:12:03.483474', '10.0.1.117', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (160, 1, 833, 'unverified', 'verifymail', '2015-04-28 13:12:03.483474', '10.0.1.117', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (160, 1, 834, 'pending_review', 'verify', '2015-04-28 13:12:03.611718', '10.0.1.117', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (161, 1, 835, 'reg', 'initial', '2015-04-28 13:13:33.695264', '10.0.1.117', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (161, 1, 836, 'unverified', 'verifymail', '2015-04-28 13:13:33.695264', '10.0.1.117', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (161, 1, 837, 'pending_review', 'verify', '2015-04-28 13:13:33.825455', '10.0.1.117', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (162, 1, 838, 'reg', 'initial', '2015-04-28 13:15:58.531462', '10.0.1.117', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (162, 1, 839, 'unverified', 'verifymail', '2015-04-28 13:15:58.531462', '10.0.1.117', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (162, 1, 840, 'pending_review', 'verify', '2015-04-28 13:15:58.632409', '10.0.1.117', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (163, 1, 841, 'reg', 'initial', '2015-04-28 13:16:45.080055', '10.0.1.117', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (163, 1, 842, 'unverified', 'verifymail', '2015-04-28 13:16:45.080055', '10.0.1.117', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (163, 1, 843, 'pending_review', 'verify', '2015-04-28 13:16:45.152425', '10.0.1.117', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (156, 1, 844, 'accepted', 'accept', '2015-04-28 13:28:19.817803', '10.0.1.117', 1025);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (157, 1, 845, 'accepted', 'accept', '2015-04-28 13:28:30.447528', '10.0.1.117', 1030);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (158, 1, 846, 'accepted', 'accept', '2015-04-28 13:28:40.290236', '10.0.1.117', 1035);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (159, 1, 847, 'accepted', 'accept', '2015-04-28 13:28:49.063185', '10.0.1.117', 1040);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (160, 1, 848, 'accepted', 'accept', '2015-04-28 13:29:13.395206', '10.0.1.117', 1050);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (161, 1, 849, 'accepted', 'accept', '2015-04-28 13:29:20.944876', '10.0.1.117', 1055);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (162, 1, 850, 'accepted', 'accept', '2015-04-28 13:29:27.726325', '10.0.1.117', 1060);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (163, 1, 851, 'accepted', 'accept', '2015-04-28 13:29:35.407876', '10.0.1.117', 1065);
SELECT pg_catalog.setval('action_states_state_id_seq', 851, true);
INSERT INTO ad_codes (code, code_type) VALUES (85431, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (27454, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (59477, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (91500, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (33523, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (65546, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (97569, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (39592, 'pay');
INSERT INTO ad_image_changes (ad_id, action_id, state_id, seq_no, name, is_new) VALUES (156, 1, 844, 0, NULL, false);
INSERT INTO ad_image_changes (ad_id, action_id, state_id, seq_no, name, is_new) VALUES (157, 1, 845, 0, NULL, false);
INSERT INTO ad_image_changes (ad_id, action_id, state_id, seq_no, name, is_new) VALUES (158, 1, 846, 0, NULL, false);
INSERT INTO ad_image_changes (ad_id, action_id, state_id, seq_no, name, is_new) VALUES (159, 1, 847, 0, NULL, false);
INSERT INTO ad_image_changes (ad_id, action_id, state_id, seq_no, name, is_new) VALUES (160, 1, 848, 0, NULL, false);
INSERT INTO ad_image_changes (ad_id, action_id, state_id, seq_no, name, is_new) VALUES (161, 1, 849, 0, NULL, false);
INSERT INTO ad_image_changes (ad_id, action_id, state_id, seq_no, name, is_new) VALUES (162, 1, 850, 0, NULL, false);
INSERT INTO ad_image_changes (ad_id, action_id, state_id, seq_no, name, is_new) VALUES (163, 1, 851, 0, NULL, false);
INSERT INTO ad_params (ad_id, name, value) VALUES (156, 'prev_currency', 'peso');
INSERT INTO ad_params (ad_id, name, value) VALUES (156, 'country', 'UNK');
INSERT INTO ad_params (ad_id, name, value) VALUES (157, 'prev_currency', 'peso');
INSERT INTO ad_params (ad_id, name, value) VALUES (157, 'country', 'UNK');
INSERT INTO ad_params (ad_id, name, value) VALUES (158, 'prev_currency', 'peso');
INSERT INTO ad_params (ad_id, name, value) VALUES (158, 'country', 'UNK');
INSERT INTO ad_params (ad_id, name, value) VALUES (159, 'regdate', '2007');
INSERT INTO ad_params (ad_id, name, value) VALUES (159, 'mileage', '13000');
INSERT INTO ad_params (ad_id, name, value) VALUES (159, 'gearbox', '1');
INSERT INTO ad_params (ad_id, name, value) VALUES (159, 'fuel', '1');
INSERT INTO ad_params (ad_id, name, value) VALUES (159, 'cartype', '2');
INSERT INTO ad_params (ad_id, name, value) VALUES (159, 'prev_currency', 'peso');
INSERT INTO ad_params (ad_id, name, value) VALUES (159, 'brand', '10');
INSERT INTO ad_params (ad_id, name, value) VALUES (159, 'model', '1');
INSERT INTO ad_params (ad_id, name, value) VALUES (159, 'version', '2');
INSERT INTO ad_params (ad_id, name, value) VALUES (159, 'country', 'UNK');
INSERT INTO ad_params (ad_id, name, value) VALUES (160, 'regdate', '2007');
INSERT INTO ad_params (ad_id, name, value) VALUES (160, 'mileage', '130000');
INSERT INTO ad_params (ad_id, name, value) VALUES (160, 'gearbox', '1');
INSERT INTO ad_params (ad_id, name, value) VALUES (160, 'fuel', '1');
INSERT INTO ad_params (ad_id, name, value) VALUES (160, 'cartype', '2');
INSERT INTO ad_params (ad_id, name, value) VALUES (160, 'prev_currency', 'peso');
INSERT INTO ad_params (ad_id, name, value) VALUES (160, 'brand', '10');
INSERT INTO ad_params (ad_id, name, value) VALUES (160, 'model', '1');
INSERT INTO ad_params (ad_id, name, value) VALUES (160, 'version', '2');
INSERT INTO ad_params (ad_id, name, value) VALUES (160, 'country', 'UNK');
INSERT INTO ad_params (ad_id, name, value) VALUES (161, 'regdate', '2007');
INSERT INTO ad_params (ad_id, name, value) VALUES (161, 'mileage', '130000');
INSERT INTO ad_params (ad_id, name, value) VALUES (161, 'gearbox', '1');
INSERT INTO ad_params (ad_id, name, value) VALUES (161, 'fuel', '1');
INSERT INTO ad_params (ad_id, name, value) VALUES (161, 'cartype', '2');
INSERT INTO ad_params (ad_id, name, value) VALUES (161, 'prev_currency', 'peso');
INSERT INTO ad_params (ad_id, name, value) VALUES (161, 'brand', '10');
INSERT INTO ad_params (ad_id, name, value) VALUES (161, 'model', '1');
INSERT INTO ad_params (ad_id, name, value) VALUES (161, 'version', '2');
INSERT INTO ad_params (ad_id, name, value) VALUES (161, 'country', 'UNK');
INSERT INTO ad_params (ad_id, name, value) VALUES (162, 'regdate', '2007');
INSERT INTO ad_params (ad_id, name, value) VALUES (162, 'mileage', '133000');
INSERT INTO ad_params (ad_id, name, value) VALUES (162, 'gearbox', '1');
INSERT INTO ad_params (ad_id, name, value) VALUES (162, 'fuel', '1');
INSERT INTO ad_params (ad_id, name, value) VALUES (162, 'cartype', '2');
INSERT INTO ad_params (ad_id, name, value) VALUES (162, 'prev_currency', 'peso');
INSERT INTO ad_params (ad_id, name, value) VALUES (162, 'brand', '10');
INSERT INTO ad_params (ad_id, name, value) VALUES (162, 'model', '1');
INSERT INTO ad_params (ad_id, name, value) VALUES (162, 'version', '2');
INSERT INTO ad_params (ad_id, name, value) VALUES (162, 'country', 'UNK');
INSERT INTO ad_params (ad_id, name, value) VALUES (162, 'communes', '311');
INSERT INTO ad_params (ad_id, name, value) VALUES (163, 'prev_currency', 'peso');
INSERT INTO ad_params (ad_id, name, value) VALUES (163, 'country', 'UNK');
INSERT INTO ad_params (ad_id, name, value) VALUES (163, 'communes', '315');
SELECT pg_catalog.setval('ads_ad_id_seq', 163, true);
SELECT pg_catalog.setval('list_id_seq', 8000122, true);
SELECT pg_catalog.setval('pay_code_seq', 504, true);
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (273, 'save', 0, '633026231', NULL, '2015-04-28 11:32:04', 196, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (274, 'verify', 0, '633026231', NULL, '2015-04-28 11:32:04', 196, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (275, 'save', 0, '768418254', NULL, '2015-04-28 13:08:41', 197, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (276, 'verify', 0, '768418254', NULL, '2015-04-28 13:08:41', 197, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (277, 'save', 0, '903810277', NULL, '2015-04-28 13:09:30', 198, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (278, 'verify', 0, '903810277', NULL, '2015-04-28 13:09:31', 198, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (279, 'save', 0, '139202300', NULL, '2015-04-28 13:10:44', 199, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (280, 'verify', 0, '139202300', NULL, '2015-04-28 13:10:44', 199, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (281, 'save', 0, '274594323', NULL, '2015-04-28 13:12:03', 200, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (282, 'verify', 0, '274594323', NULL, '2015-04-28 13:12:04', 200, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (283, 'save', 0, '409986346', NULL, '2015-04-28 13:13:34', 201, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (284, 'verify', 0, '409986346', NULL, '2015-04-28 13:13:34', 201, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (285, 'save', 0, '545378369', NULL, '2015-04-28 13:15:59', 202, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (286, 'verify', 0, '545378369', NULL, '2015-04-28 13:15:59', 202, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (287, 'save', 0, '680770392', NULL, '2015-04-28 13:16:45', 203, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (288, 'verify', 0, '680770392', NULL, '2015-04-28 13:16:45', 203, 'OK');
SELECT pg_catalog.setval('pay_log_pay_log_id_seq', 288, true);
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (273, 0, 'adphone', '962184762');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (273, 1, 'remote_addr', '10.0.1.117');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (274, 0, 'remote_addr', '10.0.1.117');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (275, 0, 'adphone', '962184762');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (275, 1, 'remote_addr', '10.0.1.117');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (276, 0, 'remote_addr', '10.0.1.117');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (277, 0, 'adphone', '962184762');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (277, 1, 'remote_addr', '10.0.1.117');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (278, 0, 'remote_addr', '10.0.1.117');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (279, 0, 'adphone', '962184762');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (279, 1, 'remote_addr', '10.0.1.117');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (280, 0, 'remote_addr', '10.0.1.117');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (281, 0, 'adphone', '962184762');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (281, 1, 'remote_addr', '10.0.1.117');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (282, 0, 'remote_addr', '10.0.1.117');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (283, 0, 'adphone', '962184762');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (283, 1, 'remote_addr', '10.0.1.117');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (284, 0, 'remote_addr', '10.0.1.117');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (285, 0, 'adphone', '962184762');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (285, 1, 'remote_addr', '10.0.1.117');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (286, 0, 'remote_addr', '10.0.1.117');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (287, 0, 'adphone', '962184762');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (287, 1, 'remote_addr', '10.0.1.117');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (288, 0, 'remote_addr', '10.0.1.117');
SELECT pg_catalog.setval('payment_groups_payment_group_id_seq', 203, true);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (196, 'ad_action', 0, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (197, 'ad_action', 0, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (198, 'ad_action', 0, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (199, 'ad_action', 0, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (200, 'ad_action', 0, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (201, 'ad_action', 0, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (202, 'ad_action', 0, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (203, 'ad_action', 0, 0);
INSERT INTO review_log (ad_id, action_id, admin_id, review_time, queue, action_type, action, refusal_reason_text, category) VALUES (156, 1, 9, '2015-04-28 13:28:19.817803', 'all', 'new', 'accepted', NULL, 5020);
INSERT INTO review_log (ad_id, action_id, admin_id, review_time, queue, action_type, action, refusal_reason_text, category) VALUES (157, 1, 9, '2015-04-28 13:28:30.447528', 'all', 'new', 'accepted', NULL, 5020);
INSERT INTO review_log (ad_id, action_id, admin_id, review_time, queue, action_type, action, refusal_reason_text, category) VALUES (158, 1, 9, '2015-04-28 13:28:40.290236', 'all', 'new', 'accepted', NULL, 5020);
INSERT INTO review_log (ad_id, action_id, admin_id, review_time, queue, action_type, action, refusal_reason_text, category) VALUES (159, 1, 9, '2015-04-28 13:28:49.063185', 'all', 'new', 'accepted', NULL, 2020);
INSERT INTO review_log (ad_id, action_id, admin_id, review_time, queue, action_type, action, refusal_reason_text, category) VALUES (160, 1, 9, '2015-04-28 13:29:13.395206', 'all', 'new', 'accepted', NULL, 2020);
INSERT INTO review_log (ad_id, action_id, admin_id, review_time, queue, action_type, action, refusal_reason_text, category) VALUES (161, 1, 9, '2015-04-28 13:29:20.944876', 'all', 'new', 'accepted', NULL, 2020);
INSERT INTO review_log (ad_id, action_id, admin_id, review_time, queue, action_type, action, refusal_reason_text, category) VALUES (162, 1, 9, '2015-04-28 13:29:27.726325', 'all', 'new', 'accepted', NULL, 2020);
INSERT INTO review_log (ad_id, action_id, admin_id, review_time, queue, action_type, action, refusal_reason_text, category) VALUES (163, 1, 9, '2015-04-28 13:29:35.407876', 'all', 'new', 'accepted', NULL, 5020);
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (156, 1, 822, 'queue', 'normal');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (157, 1, 825, 'queue', 'normal');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (158, 1, 828, 'queue', 'normal');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (159, 1, 831, 'queue', 'normal');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (160, 1, 834, 'queue', 'normal');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (161, 1, 837, 'queue', 'normal');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (162, 1, 840, 'queue', 'normal');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (163, 1, 843, 'queue', 'normal');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (156, 1, 844, 'filter_name', 'all');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (157, 1, 845, 'filter_name', 'all');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (158, 1, 846, 'filter_name', 'all');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (159, 1, 847, 'filter_name', 'all');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (160, 1, 848, 'filter_name', 'all');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (161, 1, 849, 'filter_name', 'all');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (163, 1, 851, 'filter_name', 'all');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (162, 1, 850, 'filter_name', 'all');
SELECT pg_catalog.setval('tokens_token_id_seq', 1068, true);
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (123, '2015-04-28 13:28:19.880506', '8204@kiyoko.schibsted.cl', NULL, NULL, NULL, NULL, NULL, NULL, 'cmd:admail
commit:1
ad_id:156
action_id:1
mail_type:accept
', NULL, 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (124, '2015-04-28 13:28:30.504777', '8204@kiyoko.schibsted.cl', NULL, NULL, NULL, NULL, NULL, NULL, 'cmd:admail
commit:1
ad_id:157
action_id:1
mail_type:accept
', NULL, 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (125, '2015-04-28 13:28:40.306753', '8204@kiyoko.schibsted.cl', NULL, NULL, NULL, NULL, NULL, NULL, 'cmd:admail
commit:1
ad_id:158
action_id:1
mail_type:accept
', NULL, 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (126, '2015-04-28 13:28:49.080961', '8204@kiyoko.schibsted.cl', NULL, NULL, NULL, NULL, NULL, NULL, 'cmd:admail
commit:1
ad_id:159
action_id:1
mail_type:accept
', NULL, 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (127, '2015-04-28 13:29:13.412044', '8204@kiyoko.schibsted.cl', NULL, NULL, NULL, NULL, NULL, NULL, 'cmd:admail
commit:1
ad_id:160
action_id:1
mail_type:accept
', NULL, 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (128, '2015-04-28 13:29:20.962934', '8204@kiyoko.schibsted.cl', NULL, NULL, NULL, NULL, NULL, NULL, 'cmd:admail
commit:1
ad_id:161
action_id:1
mail_type:accept
', NULL, 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (129, '2015-04-28 13:29:27.73614', '8204@kiyoko.schibsted.cl', NULL, NULL, NULL, NULL, NULL, NULL, 'cmd:admail
commit:1
ad_id:162
action_id:1
mail_type:accept
', NULL, 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (130, '2015-04-28 13:29:35.424533', '8204@kiyoko.schibsted.cl', NULL, NULL, NULL, NULL, NULL, NULL, 'cmd:admail
commit:1
ad_id:163
action_id:1
mail_type:accept
', NULL, 'accepted_mail', NULL, NULL);
SELECT pg_catalog.setval('trans_queue_id_seq', 130, true);
INSERT INTO user_params (user_id, name, value) VALUES (78, 'first_approved_ad', '2015-04-28 13:28:19.817803-04');
INSERT INTO user_params (user_id, name, value) VALUES (79, 'first_approved_ad', '2015-04-28 13:28:30.447528-04');
INSERT INTO user_params (user_id, name, value) VALUES (80, 'first_approved_ad', '2015-04-28 13:28:40.290236-04');
INSERT INTO user_params (user_id, name, value) VALUES (81, 'first_approved_ad', '2015-04-28 13:28:49.063185-04');
INSERT INTO user_params (user_id, name, value) VALUES (82, 'first_approved_ad', '2015-04-28 13:29:13.395206-04');
INSERT INTO user_params (user_id, name, value) VALUES (83, 'first_approved_ad', '2015-04-28 13:29:20.944876-04');
INSERT INTO user_params (user_id, name, value) VALUES (84, 'first_approved_ad', '2015-04-28 13:29:27.726325-04');
INSERT INTO user_params (user_id, name, value) VALUES (85, 'first_approved_ad', '2015-04-28 13:29:35.407876-04');
INSERT INTO user_params (user_id, name, value) VALUES (78, 'first_inserted_ad', '2015-04-28 13:28:19.817803-04');
INSERT INTO user_params (user_id, name, value) VALUES (79, 'first_inserted_ad', '2015-04-28 13:28:30.447528-04');
INSERT INTO user_params (user_id, name, value) VALUES (80, 'first_inserted_ad', '2015-04-28 13:28:40.290236-04');
INSERT INTO user_params (user_id, name, value) VALUES (81, 'first_inserted_ad', '2015-04-28 13:28:49.063185-04');
INSERT INTO user_params (user_id, name, value) VALUES (82, 'first_inserted_ad', '2015-04-28 13:29:13.395206-04');
INSERT INTO user_params (user_id, name, value) VALUES (83, 'first_inserted_ad', '2015-04-28 13:29:20.944876-04');
INSERT INTO user_params (user_id, name, value) VALUES (84, 'first_inserted_ad', '2015-04-28 13:29:27.726325-04');
INSERT INTO user_params (user_id, name, value) VALUES (85, 'first_inserted_ad', '2015-04-28 13:29:35.407876-04');
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (1069, 'X553fddb15e856e060000000075d7372e00000000', 9, '2015-04-28 15:21:20.778777', '2015-04-28 15:21:21.373625', '10.0.1.117', 'authenticate', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (1070, 'X553fddb13f71f7700000000024987f6300000000', 9, '2015-04-28 15:21:21.373625', '2015-04-28 15:21:26.287281', '10.0.1.117', 'spamfilter', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (1071, 'X553fddb6a0e6170000000004f836d3700000000', 9, '2015-04-28 15:21:26.287281', '2015-04-28 15:21:32.079686', '10.0.1.117', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (1072, 'X553fddbc50fed34c0000000025b5511400000000', 9, '2015-04-28 15:21:32.079686', '2015-04-28 15:21:47.269617', '10.0.1.117', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (1073, 'X553fddcb527e80430000000070fa293600000000', 9, '2015-04-28 15:21:47.269617', '2015-04-28 15:21:47.314905', '10.0.1.117', 'blocked_item', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (1074, 'X553fddcb486722480000000073bdfa3600000000', 9, '2015-04-28 15:21:47.314905', '2015-04-28 15:22:19.606536', '10.0.1.117', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (1075, 'X553fddec808beb300000000352fed0c00000000', 9, '2015-04-28 15:22:19.606536', '2015-04-28 15:22:19.644246', '10.0.1.117', 'blocked_item', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (1076, 'X553fddec2a050be40000000030982fcd00000000', 9, '2015-04-28 15:22:19.644246', '2015-04-28 15:22:27.782962', '10.0.1.117', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (1077, 'X553fddf4f1d9dd30000000013fd04c400000000', 9, '2015-04-28 15:22:27.782962', '2015-04-28 15:22:27.821808', '10.0.1.117', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (1078, 'X553fddf4389584270000000014d81e9800000000', 9, '2015-04-28 15:22:27.821808', '2015-04-28 15:22:57.87562', '10.0.1.117', 'blocked_item', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (1079, 'X553fde12645b8bcf000000002426d96f00000000', 9, '2015-04-28 15:22:57.87562', '2015-04-28 15:22:57.901993', '10.0.1.117', 'blocked_item', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (1080, 'X553fde1249dc2a830000000057cdf25d00000000', 9, '2015-04-28 15:22:57.901993', '2015-04-28 15:23:03.830752', '10.0.1.117', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (1081, 'X553fde1848c81b93000000001556cacc00000000', 9, '2015-04-28 15:23:03.830752', '2015-04-28 15:23:03.841622', '10.0.1.117', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (1082, 'X553fde18914c5020000000011b4b0fd00000000', 9, '2015-04-28 15:23:03.841622', '2015-04-28 15:23:05.305068', '10.0.1.117', 'blocked_item', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (1083, 'X553fde1946e49e09000000007d5b764f00000000', 9, '2015-04-28 15:23:05.305068', '2015-04-28 15:23:05.318615', '10.0.1.117', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (1084, 'X553fde192df3a61c00000000449c5fae00000000', 9, '2015-04-28 15:23:05.318615', '2015-04-28 15:23:20.223419', '10.0.1.117', 'blocked_item', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (1085, 'X553fde2858996472000000007ea518e600000000', 9, '2015-04-28 15:23:20.223419', '2015-04-28 15:23:20.237681', '10.0.1.117', 'blocked_item', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (1086, 'X553fde28137d377e000000009c58e4200000000', 9, '2015-04-28 15:23:20.237681', '2015-04-28 15:23:34.092695', '10.0.1.117', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (1087, 'X553fde362dec67b1000000001c24793600000000', 9, '2015-04-28 15:23:34.092695', '2015-04-28 15:23:34.103248', '10.0.1.117', 'blocked_item', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (1088, 'X553fde3673f26b9400000000310320f800000000', 9, '2015-04-28 15:23:34.103248', '2015-04-28 15:23:47.91048', '10.0.1.117', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (1089, 'X553fde444659ebc500000000c15ee4700000000', 9, '2015-04-28 15:23:47.91048', '2015-04-28 15:23:47.986906', '10.0.1.117', 'blocked_item', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (1090, 'X553fde441dca9f440000000044288ae800000000', 9, '2015-04-28 15:23:47.986906', '2015-04-28 15:24:00.635543', '10.0.1.117', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (1091, 'X553fde514184013700000000702c69500000000', 9, '2015-04-28 15:24:00.635543', '2015-04-28 15:24:00.649478', '10.0.1.117', 'blocked_item', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (1092, 'X553fde514b9f26430000000019e1a15a00000000', 9, '2015-04-28 15:24:00.649478', '2015-04-28 15:24:16.855205', '10.0.1.117', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (1093, 'X553fde611886ba40000000003829c47400000000', 9, '2015-04-28 15:24:16.855205', '2015-04-28 15:24:16.865931', '10.0.1.117', 'blocked_item', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (1094, 'X553fde6141ef52b70000000093a418000000000', 9, '2015-04-28 15:24:16.865931', '2015-04-28 15:24:29.067725', '10.0.1.117', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (1095, 'X553fde6d255ebab600000000510fd77700000000', 9, '2015-04-28 15:24:29.067725', '2015-04-28 15:24:29.077732', '10.0.1.117', 'blocked_item', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (1096, 'X553fde6d212f86f000000007bc7899900000000', 9, '2015-04-28 15:24:29.077732', '2015-04-28 15:24:42.024623', '10.0.1.117', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (1097, 'X553fde7a7dd77e00000000023f5727500000000', 9, '2015-04-28 15:24:42.024623', '2015-04-28 15:24:42.035047', '10.0.1.117', 'blocked_item', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (1098, 'X553fde7a681dfd5d000000007867c97900000000', 9, '2015-04-28 15:24:42.035047', '2015-04-28 15:24:49.939004', '10.0.1.117', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (1099, 'X553fde827f6a900e000000005ff03bac00000000', 9, '2015-04-28 15:24:49.939004', '2015-04-28 15:24:49.945281', '10.0.1.117', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (1100, 'X553fde8279d1dd060000000045a2912900000000', 9, '2015-04-28 15:24:49.945281', '2015-04-28 16:24:49.945281', '10.0.1.117', 'blocked_item', NULL, NULL);
INSERT INTO blocked_items (item_id, value, list_id, notice, token_id) VALUES (34, 'edit_with_badwords@yapo.cl', 39, 'Badwords', 1074);
INSERT INTO blocked_items (item_id, value, list_id, notice, token_id) VALUES (35, 'edit_add_image@yapo.cl', 39, 'prueba', 1084);
INSERT INTO blocked_items (item_id, value, list_id, notice, token_id) VALUES (36, 'edit_nothing@yapo.cl', 39, 'nothing', 1086);
INSERT INTO blocked_items (item_id, value, list_id, notice, token_id) VALUES (37, 'edit_nothing_free_car@yapo.cl', 39, 'edit free car', 1088);
INSERT INTO blocked_items (item_id, value, list_id, notice, token_id) VALUES (38, 'edit_nothing_regular_car@yapo.cl', 39, 'edit nothing car', 1090);
INSERT INTO blocked_items (item_id, value, list_id, notice, token_id) VALUES (39, 'edit_lower_car_price_still_green@yapo.cl', 39, 'edit lower car green', 1092);
INSERT INTO blocked_items (item_id, value, list_id, notice, token_id) VALUES (40, 'edit_lower_car_price_not_green@yapo.cl', 39, 'edit lower car not green', 1094);
INSERT INTO blocked_items (item_id, value, list_id, notice, token_id) VALUES (41, 'edit_refuse@yapo.cl', 39, 'edit refuse', 1096);
SELECT pg_catalog.setval('blocked_items_item_id_seq', 41, true);
INSERT INTO event_log (event_id, event_name, event, token_id, "time") VALUES (19, 'blocked_item', 'INSERT blocked_item edit_with_badwords@yapo.cl, 39, "Badwords"', 1074, '2015-04-28 15:22:19.614053');
INSERT INTO event_log (event_id, event_name, event, token_id, "time") VALUES (20, 'blocked_item', 'INSERT blocked_item edit_add_image@yapo.cl, 39, "prueba"', 1084, '2015-04-28 15:23:20.228195');
INSERT INTO event_log (event_id, event_name, event, token_id, "time") VALUES (21, 'blocked_item', 'INSERT blocked_item edit_nothing@yapo.cl, 39, "nothing"', 1086, '2015-04-28 15:23:34.095889');
INSERT INTO event_log (event_id, event_name, event, token_id, "time") VALUES (22, 'blocked_item', 'INSERT blocked_item edit_nothing_free_car@yapo.cl, 39, "edit free car"', 1088, '2015-04-28 15:23:47.914706');
INSERT INTO event_log (event_id, event_name, event, token_id, "time") VALUES (23, 'blocked_item', 'INSERT blocked_item edit_nothing_regular_car@yapo.cl, 39, "edit nothing car"', 1090, '2015-04-28 15:24:00.638566');
INSERT INTO event_log (event_id, event_name, event, token_id, "time") VALUES (24, 'blocked_item', 'INSERT blocked_item edit_lower_car_price_still_green@yapo.cl, 39, "edit lower car green"', 1092, '2015-04-28 15:24:16.858522');
INSERT INTO event_log (event_id, event_name, event, token_id, "time") VALUES (25, 'blocked_item', 'INSERT blocked_item edit_lower_car_price_not_green@yapo.cl, 39, "edit lower car not green"', 1094, '2015-04-28 15:24:29.071546');
INSERT INTO event_log (event_id, event_name, event, token_id, "time") VALUES (26, 'blocked_item', 'INSERT blocked_item edit_refuse@yapo.cl, 39, "edit refuse"', 1096, '2015-04-28 15:24:42.028544');
SELECT pg_catalog.setval('event_log_event_id_seq', 26, true);
SELECT pg_catalog.setval('tokens_token_id_seq', 1100, true);
SELECT pg_catalog.setval('users_user_id_seq', 85, true);
INSERT INTO users (user_id, uid, email, account, paid_total) VALUES (86, 50, 'edit_totaly_not_green_user@yapo.cl', 0, 0);
INSERT INTO accounts (account_id, name, rut, email, phone, is_company, region, salted_passwd, creation_date, activation_date, status, address, contact, lob, commune, user_id) VALUES (35, 'totaly not green', '', 'edit_totaly_not_green_user@yapo.cl', '12344123', false, 15, '$1024$eDVIQ>e*1Uo%vriI87611fa34658a9792a403fbeb225053678fcb1ba', '2015-04-29 09:12:50.683725', NULL, 'active', NULL, NULL, NULL, NULL, 86);
SELECT pg_catalog.setval('accounts_account_id_seq', 35, true);
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (164, 8000123, '2015-04-29 09:13:26', 'active', 'sell', 'totaly not green', '12344123', 15, 0, 2020, 86, NULL, false, false, false, 'Car to price not green anywhere', 'Auto to ungreen in whitelist and autoaccept.', 8000000, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$eDVIQ>e*1Uo%vriI87611fa34658a9792a403fbeb225053678fcb1ba', '2015-04-29 09:13:26.06422', 'es');
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (204, '633026231', 'verified', '2015-04-29 09:12:50.586807', NULL);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (164, 1, 'new', 855, 'accepted', 'normal', NULL, NULL, 204);
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (164, 1, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (164, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (1101, 'X5540d8dd609ae9da0000000053e19ba100000000', 9, '2015-04-29 09:13:01.292009', '2015-04-29 09:13:01.482836', '10.0.1.117', 'authenticate', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (1102, 'X5540d8ddb0daef800000000499c7b4f00000000', 9, '2015-04-29 09:13:01.482836', '2015-04-29 09:13:05.577373', '10.0.1.117', 'spamfilter', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (1103, 'X5540d8e22e334e950000000065617ba700000000', 9, '2015-04-29 09:13:05.577373', '2015-04-29 09:13:12.856911', '10.0.1.117', 'listadmins', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (1104, 'X5540d8e952a884a30000000052edbd1e00000000', 9, '2015-04-29 09:13:12.856911', '2015-04-29 09:13:12.878927', '10.0.1.117', 'listadmins', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (1105, 'X5540d8e946f38b7b000000002cef93e700000000', 9, '2015-04-29 09:13:12.878927', '2015-04-29 09:13:21.369574', '10.0.1.117', 'search_ads', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (1106, 'X5540d8f164d4b0a400000000507eb82f00000000', 9, '2015-04-29 09:13:21.369574', '2015-04-29 09:13:21.537294', '10.0.1.117', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (1107, 'X5540d8f215e04053000000002f6e02c900000000', 9, '2015-04-29 09:13:21.537294', '2015-04-29 09:13:21.54475', '10.0.1.117', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (1108, 'X5540d8f253719b0700000000388dfbe300000000', 9, '2015-04-29 09:13:21.54475', '2015-04-29 09:13:21.551872', '10.0.1.117', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (1109, 'X5540d8f22a5d07c700000000210288cb00000000', 9, '2015-04-29 09:13:21.551872', '2015-04-29 09:13:26.058848', '10.0.1.117', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (1110, 'X5540d8f67f6ecc7b000000007f8360ea00000000', 9, '2015-04-29 09:13:26.058848', '2015-04-29 10:13:26.058848', '10.0.1.117', 'review', NULL, NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (164, 1, 852, 'reg', 'initial', '2015-04-29 09:12:50.586807', '10.0.1.117', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (164, 1, 853, 'unverified', 'verifymail', '2015-04-29 09:12:50.586807', '10.0.1.117', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (164, 1, 854, 'pending_review', 'verify', '2015-04-29 09:12:50.732971', '10.0.1.117', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (164, 1, 855, 'accepted', 'accept', '2015-04-29 09:13:26.06422', '10.0.1.117', 1109);
SELECT pg_catalog.setval('action_states_state_id_seq', 855, true);
INSERT INTO ad_codes (code, code_type) VALUES (71615, 'pay');
INSERT INTO ad_image_changes (ad_id, action_id, state_id, seq_no, name, is_new) VALUES (164, 1, 855, 0, NULL, false);
INSERT INTO ad_params (ad_id, name, value) VALUES (164, 'regdate', '2007');
INSERT INTO ad_params (ad_id, name, value) VALUES (164, 'mileage', '13000');
INSERT INTO ad_params (ad_id, name, value) VALUES (164, 'gearbox', '1');
INSERT INTO ad_params (ad_id, name, value) VALUES (164, 'fuel', '1');
INSERT INTO ad_params (ad_id, name, value) VALUES (164, 'cartype', '2');
INSERT INTO ad_params (ad_id, name, value) VALUES (164, 'prev_currency', 'peso');
INSERT INTO ad_params (ad_id, name, value) VALUES (164, 'brand', '10');
INSERT INTO ad_params (ad_id, name, value) VALUES (164, 'model', '1');
INSERT INTO ad_params (ad_id, name, value) VALUES (164, 'version', '2');
INSERT INTO ad_params (ad_id, name, value) VALUES (164, 'country', 'UNK');
INSERT INTO ad_params (ad_id, name, value) VALUES (164, 'communes', '311');
SELECT pg_catalog.setval('ads_ad_id_seq', 164, true);
SELECT pg_catalog.setval('list_id_seq', 8000123, true);
SELECT pg_catalog.setval('pay_code_seq', 505, true);
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (289, 'save', 0, '633026231', NULL, '2015-04-29 09:12:51', 204, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (290, 'verify', 0, '633026231', NULL, '2015-04-29 09:12:51', 204, 'OK');
SELECT pg_catalog.setval('pay_log_pay_log_id_seq', 290, true);
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (289, 0, 'adphone', '12344123');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (289, 1, 'remote_addr', '10.0.1.117');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (290, 0, 'remote_addr', '10.0.1.117');
SELECT pg_catalog.setval('payment_groups_payment_group_id_seq', 204, true);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (204, 'ad_action', 0, 0);
INSERT INTO review_log (ad_id, action_id, admin_id, review_time, queue, action_type, action, refusal_reason_text, category) VALUES (164, 1, 9, '2015-04-29 09:13:26.06422', 'all', 'new', 'accepted', NULL, 2020);
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (164, 1, 854, 'queue', 'normal');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (164, 1, 855, 'filter_name', 'all');
SELECT pg_catalog.setval('tokens_token_id_seq', 1110, true);
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (131, '2015-04-29 09:13:26.102879', '10765@kiyoko.schibsted.cl', NULL, NULL, NULL, NULL, NULL, NULL, 'cmd:admail
commit:1
ad_id:164
action_id:1
mail_type:accept
', NULL, 'accepted_mail', NULL, NULL);
SELECT pg_catalog.setval('trans_queue_id_seq', 131, true);
INSERT INTO user_params (user_id, name, value) VALUES (86, 'first_approved_ad', '2015-04-29 09:13:26.06422-04');
INSERT INTO user_params (user_id, name, value) VALUES (86, 'first_inserted_ad', '2015-04-29 09:13:26.06422-04');
SELECT pg_catalog.setval('users_user_id_seq', 86, true);
INSERT INTO users (user_id, uid, email, account, paid_total) VALUES (87, 50, 'edit_to_refuse_user@yapo.cl', 0, 0);
INSERT INTO accounts (account_id, name, rut, email, phone, is_company, region, salted_passwd, creation_date, activation_date, status, address, contact, lob, commune, user_id) VALUES (36, 'User to edit refuse', '', 'edit_to_refuse_user@yapo.cl', '962184762', false, 15, '$1024$@aupDnQy&:2$u<z^005b20d60b23e4ae8b5fe10affa49e78e8fbd327', '2015-04-29 14:43:30.801697', NULL, 'active', NULL, NULL, NULL, NULL, 87);
SELECT pg_catalog.setval('accounts_account_id_seq', 36, true);
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (165, 8000124, '2015-04-29 14:45:14', 'active', 'sell', 'User to edit refuse', '962184762', 15, 0, 6160, 87, NULL, false, false, false, 'Aviso to edit refuse', 'to edit refuse', 13000, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$@aupDnQy&:2$u<z^005b20d60b23e4ae8b5fe10affa49e78e8fbd327', '2015-04-29 14:45:14.078915', 'es');
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (205, '633026231', 'verified', '2015-04-29 14:43:30.71248', NULL);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (165, 1, 'new', 859, 'accepted', 'normal', NULL, NULL, 205);
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (165, 1, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (165, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (1111, 'X554125fb303aee720000000043652d1e00000000', 9, '2015-04-29 14:42:03.263021', '2015-04-29 14:42:03.483533', '10.0.1.117', 'authenticate', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (1112, 'X554125fb5606e377000000006b4dc0e100000000', 9, '2015-04-29 14:42:03.483533', '2015-04-29 14:42:05.985467', '10.0.1.117', 'spamfilter', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (1113, 'X554125fe5b5b801c000000007513f7400000000', 9, '2015-04-29 14:42:05.985467', '2015-04-29 14:42:08.026484', '10.0.1.117', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (1114, 'X554126001e620293000000001798e72000000000', 9, '2015-04-29 14:42:08.026484', '2015-04-29 14:42:19.268788', '10.0.1.117', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (1115, 'X5541260b2f4da703000000003a39d19100000000', 9, '2015-04-29 14:42:19.268788', '2015-04-29 14:42:19.284625', '10.0.1.117', 'blocked_item', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (1116, 'X5541260b18f348800000000024470f4000000000', 9, '2015-04-29 14:42:19.284625', '2015-04-29 14:43:36.630838', '10.0.1.117', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (1117, 'X554126596d03c46b00000000226a13e300000000', 9, '2015-04-29 14:43:36.630838', '2015-04-29 14:43:41.156323', '10.0.1.117', 'listadmins', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (1118, 'X5541265d993a2c00000000078d4a21a00000000', 9, '2015-04-29 14:43:41.156323', '2015-04-29 14:43:41.169166', '10.0.1.117', 'blocked_item', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (1119, 'X5541265d6455f3b8000000003588121600000000', 9, '2015-04-29 14:43:41.169166', '2015-04-29 14:44:45.638146', '10.0.1.117', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (1120, 'X5541269e3e2018f8000000003c60d42000000000', 9, '2015-04-29 14:44:45.638146', '2015-04-29 14:44:45.66847', '10.0.1.117', 'blocked_item', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (1121, 'X5541269e2f71276500000000253ad6f00000000', 9, '2015-04-29 14:44:45.66847', '2015-04-29 14:44:47.756196', '10.0.1.117', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (1122, 'X554126a0244d69cb0000000044cf8f8600000000', 9, '2015-04-29 14:44:47.756196', '2015-04-29 14:44:47.764753', '10.0.1.117', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (1123, 'X554126a0326e1f80000000004f62ff6200000000', 9, '2015-04-29 14:44:47.764753', '2015-04-29 14:45:00.02927', '10.0.1.117', 'blocked_item', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (1124, 'X554126ac2c14d2be0000000040fccdd700000000', 9, '2015-04-29 14:45:00.02927', '2015-04-29 14:45:04.405272', '10.0.1.117', 'listadmins', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (1125, 'X554126b023241816000000004abf25d000000000', 9, '2015-04-29 14:45:04.405272', '2015-04-29 14:45:04.427173', '10.0.1.117', 'listadmins', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (1126, 'X554126b0215db35b000000003d9aed0000000000', 9, '2015-04-29 14:45:04.427173', '2015-04-29 14:45:07.40367', '10.0.1.117', 'search_ads', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (1127, 'X554126b379c59ca6000000003bdd88e800000000', 9, '2015-04-29 14:45:07.40367', '2015-04-29 14:45:07.572113', '10.0.1.117', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (1128, 'X554126b44f180a29000000001b70fcfc00000000', 9, '2015-04-29 14:45:07.572113', '2015-04-29 14:45:07.579818', '10.0.1.117', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (1129, 'X554126b4360cef310000000077105f2600000000', 9, '2015-04-29 14:45:07.579818', '2015-04-29 14:45:07.587398', '10.0.1.117', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (1130, 'X554126b422b2d25700000000363601500000000', 9, '2015-04-29 14:45:07.587398', '2015-04-29 14:45:14.072099', '10.0.1.117', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (1131, 'X554126ba6d9b0af80000000064845ba400000000', 9, '2015-04-29 14:45:14.072099', '2015-04-29 15:45:14.072099', '10.0.1.117', 'review', NULL, NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (165, 1, 856, 'reg', 'initial', '2015-04-29 14:43:30.71248', '10.0.1.117', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (165, 1, 857, 'unverified', 'verifymail', '2015-04-29 14:43:30.71248', '10.0.1.117', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (165, 1, 858, 'pending_review', 'verify', '2015-04-29 14:43:30.842165', '10.0.1.117', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (165, 1, 859, 'accepted', 'accept', '2015-04-29 14:45:14.078915', '10.0.1.117', 1130);
SELECT pg_catalog.setval('action_states_state_id_seq', 859, true);
INSERT INTO ad_codes (code, code_type) VALUES (13638, 'pay');
INSERT INTO ad_image_changes (ad_id, action_id, state_id, seq_no, name, is_new) VALUES (165, 1, 859, 0, NULL, false);
INSERT INTO ad_params (ad_id, name, value) VALUES (165, 'prev_currency', 'peso');
INSERT INTO ad_params (ad_id, name, value) VALUES (165, 'country', 'UNK');
INSERT INTO ad_params (ad_id, name, value) VALUES (165, 'communes', '311');
SELECT pg_catalog.setval('ads_ad_id_seq', 165, true);
INSERT INTO blocked_items (item_id, value, list_id, notice, token_id) VALUES (42, 'edit_totaly_not_green_user@yapo.cl', 39, 'used to be green', 1114);
INSERT INTO blocked_items (item_id, value, list_id, notice, token_id) VALUES (43, 'edit_to_refuse_user@yapo.cl', 39, 'edit to refuse', 1119);
INSERT INTO blocked_items (item_id, value, list_id, notice, token_id) VALUES (44, 'For Sale', 11, 'For Sale', 1010);
SELECT pg_catalog.setval('blocked_items_item_id_seq', 44, true);
INSERT INTO event_log (event_id, event_name, event, token_id, "time") VALUES (27, 'blocked_item', 'INSERT blocked_item edit_totaly_not_green_user@yapo.cl, 39, "used to be green"', 1114, '2015-04-29 14:42:19.273983');
INSERT INTO event_log (event_id, event_name, event, token_id, "time") VALUES (28, 'blocked_item', 'INSERT blocked_item edit_totaly_not_green_user@yapo.cl, 39, "used to be green"', 1117, '2015-04-29 14:43:41.161341');
INSERT INTO event_log (event_id, event_name, event, token_id, "time") VALUES (29, 'blocked_item', 'INSERT blocked_item edit_to_refuse_user@yapo.cl, 39, "edit to refuse"', 1119, '2015-04-29 14:44:45.657922');
SELECT pg_catalog.setval('event_log_event_id_seq', 29, true);
SELECT pg_catalog.setval('list_id_seq', 8000124, true);
SELECT pg_catalog.setval('pay_code_seq', 506, true);
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (291, 'save', 0, '633026231', NULL, '2015-04-29 14:43:31', 205, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (292, 'verify', 0, '633026231', NULL, '2015-04-29 14:43:31', 205, 'OK');
SELECT pg_catalog.setval('pay_log_pay_log_id_seq', 292, true);
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (291, 0, 'adphone', '962184762');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (291, 1, 'remote_addr', '10.0.1.117');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (292, 0, 'remote_addr', '10.0.1.117');
SELECT pg_catalog.setval('payment_groups_payment_group_id_seq', 205, true);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (205, 'ad_action', 0, 0);
INSERT INTO review_log (ad_id, action_id, admin_id, review_time, queue, action_type, action, refusal_reason_text, category) VALUES (165, 1, 9, '2015-04-29 14:45:14.078915', 'all', 'new', 'accepted', NULL, 6160);
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (165, 1, 858, 'queue', 'normal');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (165, 1, 859, 'filter_name', 'all');
SELECT pg_catalog.setval('tokens_token_id_seq', 1131, true);
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (132, '2015-04-29 14:45:14.139848', '567@kiyoko.schibsted.cl', NULL, NULL, NULL, NULL, NULL, NULL, 'cmd:admail
commit:1
ad_id:165
action_id:1
mail_type:accept
', NULL, 'accepted_mail', NULL, NULL);
SELECT pg_catalog.setval('trans_queue_id_seq', 132, true);
INSERT INTO user_params (user_id, name, value) VALUES (87, 'first_approved_ad', '2015-04-29 14:45:14.078915-04');
INSERT INTO user_params (user_id, name, value) VALUES (87, 'first_inserted_ad', '2015-04-29 14:45:14.078915-04');
SELECT pg_catalog.setval('users_user_id_seq', 87, true);
INSERT INTO ad_params VALUES (128, 'communes', '336');
INSERT INTO ad_params VALUES (137, 'communes', '316');
INSERT INTO ad_params VALUES (146, 'communes', '334');
INSERT INTO ad_params VALUES (126, 'communes', '329');
INSERT INTO ad_params VALUES (136, 'communes', '312');
INSERT INTO ad_params VALUES (131, 'communes', '345');
INSERT INTO ad_params VALUES (143, 'communes', '321');
INSERT INTO ad_params VALUES (130, 'communes', '318');
INSERT INTO ad_params VALUES (135, 'communes', '313');
INSERT INTO ad_params VALUES (129, 'communes', '303');
INSERT INTO ad_params VALUES (134, 'communes', '304');
INSERT INTO ad_params VALUES (133, 'communes', '324');
INSERT INTO ad_params VALUES (132, 'communes', '323');
INSERT INTO ad_params VALUES (140, 'communes', '345');
INSERT INTO ad_params VALUES (142, 'communes', '330');
INSERT INTO ad_params VALUES (139, 'communes', '298');
INSERT INTO ad_params VALUES (145, 'communes', '331');
INSERT INTO ad_params VALUES (138, 'communes', '308');
INSERT INTO ad_params VALUES (141, 'communes', '314');
INSERT INTO ad_params VALUES (148, 'communes', '302');
INSERT INTO ad_params VALUES (144, 'communes', '326');
INSERT INTO ad_params VALUES (149, 'communes', '300');
INSERT INTO ad_params VALUES (147, 'communes', '335');
INSERT INTO ad_params VALUES (152, 'communes', '313');
INSERT INTO ad_params VALUES (150, 'communes', '326');
INSERT INTO ad_params VALUES (151, 'communes', '343');
INSERT INTO ad_params VALUES (155, 'communes', '338');
INSERT INTO ad_params VALUES (154, 'communes', '329');
INSERT INTO ad_params VALUES (153, 'communes', '346');
INSERT INTO ad_params VALUES (156, 'communes', '310');
INSERT INTO ad_params VALUES (157, 'communes', '307');
INSERT INTO ad_params VALUES (158, 'communes', '306');
INSERT INTO ad_params VALUES (159, 'communes', '343');
INSERT INTO ad_params VALUES (160, 'communes', '331');
INSERT INTO ad_params VALUES (161, 'communes', '325');

--Change all passwords to 123123123
update ads set salted_passwd =  E'$1024$2y1bQ}VYRD}zF4QCbf07a971b784888658a20e62156de64dfde5ffb6', passwd = null;
update accounts set salted_passwd =  E'$1024$2y1bQ}VYRD}zF4QCbf07a971b784888658a20e62156de64dfde5ffb6';

--Rule to move ads to jobs queue
insert into block_lists (list_id, list_name, base_list, list_type) values (55, 'Cola Empleos', 'f', 'category');
insert into block_rules (rule_id, rule_name, action, application, combine_rule, target) values (55, 'Avisos a cola Empleos', 'move_to_queue', 'clear', 'and', 'jobs');
insert into block_rule_conditions (condition_id, rule_id, list_id, whole_word, fields) values (85, 55, 55, 2, '{category}');
insert into blocked_items (value, list_id, notice) values ('7020', 55, 'Avisos a cola Empleos');
insert into blocked_items (value, list_id, notice) values ('bad_jobs', 33, 'Palabra abusiva');
