CREATE TYPE public.enum_ad_queues_queue AS ENUM (
    'special',
    'vehicles',
    'normal',
    'abuse',
    'unsolved',
    'technical_error',
    'autoaccept',
    'autorefuse',
    'video',
    'mm_double_ref',
    'mm_insuftext_ref',
    'mm_rudewords_ref',
    'mm_del_dup_acc',
    'mm_fewchange_acc',
    'mm_mass_acc',
    'mm_forbidwrd_ref',
    'mm_fraud_ref',
    'modified',
    'mama_normal',
    'mama_abuse',
    'mama_vehicles',
    'mama_special',
    'mama_modified',
    'redirect',
    'prio_store',
    'mama_prio_store',
    'serenity_fault',
    'serenity_accept',
    'serenity_refuse',
    'serenity_human_review'
);


CREATE TYPE public.enum_state_params_name AS ENUM (
    'abuse',
    'cred_card_id',
    'credit_trans_ref',
    'delete_reason',
    'filter_name',
    'gallery',
    'mama_info',
    'queue',
    'reason',
    'special',
    'sub_toplist',
    'technical_error',
    'top_list',
    'unsolved',
    'urgent',
    'vehicles',
    'gallery30',
    'daily_bump',
    'daily_bump30',
    'source',
    'photosup',
    'ldv',
    'detach_store_id',
    'detach_email',
    'detach_old_email',
    'detach_duplicate_state_id',
    'tlv_id',
    'minimal_review_description',
    'detailed_review_description',
    'serenity_wrong_verdict',
    'name_of_refusing_module',
    'booster'
);

CREATE TABLE tokens (
    token_id bigserial,
    token varchar(50) NOT NULL UNIQUE,
    admin_id integer REFERENCES admins (admin_id),
    created_at timestamp default NULL,
    valid_to timestamp default NULL,
    remote_addr varchar(20) NOT NULL,
    remote_browser varchar(255),
    info varchar(255),
    store_id integer REFERENCES stores (store_id),
    email varchar(60),
    PRIMARY KEY  (token_id),
    CHECK ((admin_id IS NOT NULL OR store_id IS NOT NULL) AND (admin_id IS NULL OR email IS NULL))
);
GRANT select,insert,delete,update ON tokens to bwriters;
GRANT select on tokens TO breaders;
GRANT select,update ON tokens_token_id_seq to bwriters;
GRANT select on tokens_token_id_seq TO breaders;

CREATE INDEX index_tokens_valid_to ON tokens (valid_to);
