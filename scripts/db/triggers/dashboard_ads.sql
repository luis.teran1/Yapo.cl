-- Triggers to keep updated dashboard ads table

DROP TRIGGER IF EXISTS dashboard_ads_relay_from_ad_actions ON ad_actions;
CREATE TRIGGER dashboard_ads_relay_from_ad_actions
AFTER INSERT OR UPDATE ON ad_actions
FOR EACH ROW
EXECUTE PROCEDURE dashboard_ads_relay_from_ad_actions();

DROP TRIGGER IF EXISTS dashboard_ads_relay_from_ads ON ad_actions;
CREATE TRIGGER dashboard_ads_relay_from_ads
AFTER INSERT OR UPDATE OR DELETE ON ads
FOR EACH ROW
EXECUTE PROCEDURE dashboard_ads_relay_from_ads();

DROP TRIGGER IF EXISTS dashboard_ads_relay_from_ad_params ON ad_params;
CREATE TRIGGER dashboard_ads_relay_from_ad_params
AFTER INSERT OR UPDATE OR DELETE ON ad_params
FOR EACH ROW
EXECUTE PROCEDURE dashboard_ads_relay_from_ad_params();

DROP TRIGGER IF EXISTS dashboard_ads_relay_from_ad_media ON ad_media;
CREATE TRIGGER dashboard_ads_relay_from_ad_media
AFTER INSERT OR UPDATE OR DELETE ON ad_media
FOR EACH ROW
EXECUTE PROCEDURE dashboard_ads_relay_from_ad_media();

DROP TRIGGER IF EXISTS dashboard_ads_relay_from_ad_changes ON ad_changes;
CREATE TRIGGER dashboard_ads_relay_from_ad_changes
AFTER INSERT OR UPDATE OR DELETE ON ad_changes
FOR EACH ROW
EXECUTE PROCEDURE dashboard_ads_relay_from_ad_changes();

DROP TRIGGER IF EXISTS dashboard_ads_relay_from_autobump ON autobump;
CREATE TRIGGER dashboard_ads_relay_from_autobump
AFTER INSERT OR UPDATE OR DELETE ON autobump
FOR EACH ROW
EXECUTE PROCEDURE dashboard_ads_relay_from_autobump();
-- vim:syntax=sql
