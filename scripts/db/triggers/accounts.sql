-- Triggers to keep updated available_slots on account_params
DROP TRIGGER IF EXISTS account_params_relay_from_packs ON packs;
CREATE TRIGGER account_params_relay_from_packs
AFTER INSERT OR UPDATE OR DELETE ON packs
FOR EACH ROW
EXECUTE PROCEDURE account_params_relay_from_packs();
-- vim:syntax=sql
