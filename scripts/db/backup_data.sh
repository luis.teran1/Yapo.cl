#!/bin/bash

test -z "$1" && exit 1
test -z "$2" && exit 1
PGDIR=`cd $1; pwd`
# Create a snapshot of blocketdb
pg_dump --format=c --compress=0 -h $PGDIR blocketdb > $2.dmp

test -n "$3" && exit 0

# Restore an empty schema
pg_restore -c -s $2.dmp | psql -h $PGDIR blocketdb >/dev/null

# Populate with base-data
DSNARGS="-h $PGDIR"
env PGOPTIONS="-c search_path=$BPVSCHEMA,public" /usr/bin/psql $DSNARGS blocketdb < insert_blocketdb_data.pgsql > /dev/null 2>> .psql.stderr
