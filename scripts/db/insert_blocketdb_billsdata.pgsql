SELECT pg_catalog.setval('action_states_state_id_seq', 227, true);
SELECT pg_catalog.setval('pay_log_pay_log_id_seq', 21, true);
SELECT pg_catalog.setval('payment_groups_payment_group_id_seq', 66, true);
SELECT pg_catalog.setval('purchase_detail_purchase_detail_id_seq', 7, true);
SELECT pg_catalog.setval('purchase_purchase_id_seq', 7, true);
SELECT pg_catalog.setval('purchase_states_purchase_state_id_seq', 7, true);
INSERT INTO payment_groups VALUES (66, '90003', 'paid', '2014-04-06 10:52:06.324331', NULL);
INSERT INTO ad_actions VALUES (11, 3, 'bump', 227, 'accepted', 'normal', NULL, NULL, 66);
INSERT INTO ad_actions VALUES (12, 3, 'bump', 231, 'accepted', 'normal', NULL, NULL, 64);
INSERT INTO ad_actions VALUES (10, 3, 'bump', 235, 'accepted', 'normal', NULL, NULL, 64);
INSERT INTO action_states VALUES (11, 3, 224, 'reg', 'initial', '2014-04-06 10:52:06.324331', '10.0.1.188', NULL);
INSERT INTO action_states VALUES (11, 3, 225, 'unpaid', 'pending_pay', '2014-04-06 10:52:06.324331', '10.0.1.188', NULL);
INSERT INTO action_states VALUES (11, 3, 226, 'pending_bump', 'pay', '2014-04-06 10:52:11.623367', NULL, NULL);
INSERT INTO action_states VALUES (11, 3, 227, 'accepted', 'bump', '2014-04-06 10:52:11.710101', NULL, NULL);
INSERT INTO action_states VALUES (12, 3, 228, 'reg', 'initial', '2014-04-06 10:52:06.324331', '10.0.1.188', NULL);
INSERT INTO action_states VALUES (12, 3, 229, 'unpaid', 'pending_pay', '2014-04-07 10:52:06.324331', '10.0.1.188', NULL);
INSERT INTO action_states VALUES (12, 3, 230, 'pending_bump', 'pay', '2014-04-07 10:52:11.623367', NULL, NULL);
INSERT INTO action_states VALUES (12, 3, 231, 'accepted', 'bump', '2014-04-07 10:52:11.710101', NULL, NULL);
INSERT INTO action_states VALUES (10, 3, 232, 'reg', 'initial', '2014-04-06 10:52:06.324331', '10.0.1.188', NULL);
INSERT INTO action_states VALUES (10, 3, 233, 'unpaid', 'pending_pay', '2014-04-07 10:52:06.324331', '10.0.1.188', NULL);
INSERT INTO action_states VALUES (10, 3, 234, 'pending_bump', 'pay', '2014-04-07 10:52:11.623367', NULL, NULL);
INSERT INTO action_states VALUES (10, 3, 235, 'accepted', 'bump', '2014-04-07 10:52:11.710101', NULL, NULL);
INSERT INTO pay_log VALUES (19, 'save', 0, '90003', NULL, '2014-04-06 10:52:06', 66, 'SAVE');
INSERT INTO pay_log VALUES (20, 'paycard_save', 0, '90003', NULL, '2014-04-06 10:52:11', 66, 'SAVE');
INSERT INTO pay_log VALUES (21, 'paycard', 1000, '0066a', NULL, '2014-04-06 10:52:12', 66, 'OK');
INSERT INTO pay_log_references VALUES (19, 0, 'remote_addr', '10.0.1.188');
INSERT INTO pay_log_references VALUES (20, 0, 'auth_code', '439786');
INSERT INTO pay_log_references VALUES (20, 1, 'trans_date', '07/04/2014 10:52:09');
INSERT INTO pay_log_references VALUES (20, 2, 'placements_type', 'VN');
INSERT INTO pay_log_references VALUES (20, 3, 'placements_number', '1');
INSERT INTO pay_log_references VALUES (20, 4, 'pay_type', 'credit');
INSERT INTO pay_log_references VALUES (20, 5, 'external_response', '0');
INSERT INTO pay_log_references VALUES (20, 6, 'external_id', '18532758753082616683');
INSERT INTO pay_log_references VALUES (20, 7, 'remote_addr', '127.0.0.1');
INSERT INTO pay_log_references VALUES (21, 0, 'webpay', '1');
INSERT INTO payments VALUES (66, 'bump', 1000, 0);
INSERT INTO purchase VALUES (7, 'invoice', NULL, NULL, 'paid', '2014-04-06 10:52:06.324331', NULL, '11111111-1', 'Andrea Gonz�lez', 'Limpieza y Mantenci�n &#8211;', 'O&#180;Higgins', 13, 275, 'M�rc��l�g�', 'uid1@blocket.se', 0, 0, NULL, 1000, 66);
INSERT INTO purchase VALUES (8, 'invoice', NULL, NULL, 'paid', '2014-04-07 10:52:06.324331', NULL, '11111111-1', 'Strnage characters 1', '�����������������������������', 'O&#180;Higgins', 13, 275, 'M�rc��l�g�', 'uid1@blocket.se', 0, 0, NULL, 1000, 66);
INSERT INTO purchase VALUES (9, 'invoice', NULL, NULL, 'paid', '2014-04-07 10:52:06.324331', NULL, '11111111-1', 'Strnage characters 2', '����������������������������������', 'O&#180;Higgins', 13, 275, 'M�rc��l�g�', 'uid1@blocket.se', 0, 0, NULL, 1000, 66);
INSERT INTO purchase_detail VALUES (7, 7, 1, 1000, 3, 11);
INSERT INTO purchase_detail VALUES (8, 8, 1, 1000, 3, 12);
INSERT INTO purchase_detail VALUES (9, 9, 1, 1000, 3, 12);
INSERT INTO purchase_states VALUES (7, 7, 'pending', '2014-04-06 10:52:06.324331', '10.0.1.188');
INSERT INTO purchase_states VALUES (8, 8, 'pending', '2014-04-07 10:52:06.324331', '10.0.1.188');
INSERT INTO purchase_states VALUES (9, 9, 'pending', '2014-04-07 10:52:06.324331', '10.0.1.188');
INSERT INTO trans_queue VALUES (7, '2014-04-06 10:52:11.719431', '21085@mazaru.schibsted.cl', NULL, NULL, NULL, NULL, NULL, NULL, '
cmd:multi_product_mail
to:uid1@blocket.se
doc_type:invoice
product_ids:1
ad_names:Peluquera a domicilio
commit:1', NULL, 'bump_mail', NULL, NULL);
-- update old payments
UPDATE purchase SET status = 'paid' WHERE purchase_id IN (1,2);

--Migrate inmo ads
select * from bpv.migrate_ads_inmo();
