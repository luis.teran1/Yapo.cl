
update ads set status = 'refused' where ad_id in (92,117,95,87,123,118);

INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (166, '12345', 'paid', '2015-07-13 11:20:53.192971', NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (167, '65432', 'paid', '2015-07-13 11:21:40.068128', NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (168, '32323', 'paid', '2015-07-13 11:21:56.850531', NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (169, '54545', 'paid', '2015-07-13 11:22:37.18336', NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (170, '90001', 'paid', '2015-07-13 11:23:12.520326', NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (171, '633026231', 'verified', '2015-07-13 11:29:54.882918', NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (172, '90002', 'paid', '2015-07-13 11:30:03.024541', NULL);


 INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (123, 2, 'upselling_weekly_bump', 718, 'refused', 'normal', NULL, NULL, 166);
 INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (118, 2, 'upselling_gallery', 720, 'refused', 'normal', NULL, NULL, 167);
 INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (118, 3, 'remove_gallery', 722, 'accepted', 'normal', NULL, NULL, NULL);
 INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (117, 2, 'upselling_daily_bump', 724, 'refused', 'normal', NULL, NULL, 168);
 INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (92, 2, 'upselling_weekly_bump', 726, 'refused', 'normal', NULL, NULL, 169);
 INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (87, 2, 'upselling_gallery', 728, 'refused', 'normal', NULL, NULL, 170);
 INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (87, 3, 'remove_gallery', 730, 'accepted', 'normal', NULL, NULL, NULL);
 INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (95, 2, 'edit', 737, 'refused', 'edit', NULL, NULL, 171);
 INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (95, 3, 'upselling_weekly_bump', 738, 'refused', 'normal', NULL, NULL, 172);

 INSERT INTO action_params (ad_id, action_id, name, value) VALUES (95, 2, 'source', 'web');
 INSERT INTO action_params (ad_id, action_id, name, value) VALUES (95, 2, 'redir', 'dW5rbm93bg==');

INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (715, 'X55a3d5c759f55693000000005305cce400000000', 9, '2015-07-13 11:14:15.427639', '2015-07-13 11:14:15.50661', '10.0.1.101', 'authenticate', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (716, 'X55a3d5c8738932310000000011b5583b00000000', 9, '2015-07-13 11:14:15.50661', '2015-07-13 11:14:16.628302', '10.0.1.101', 'spamfilter', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (717, 'X55a3d5c976b3b8ad000000003a2ea98700000000', 9, '2015-07-13 11:14:16.628302', '2015-07-13 11:14:18.054986', '10.0.1.101', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (718, 'X55a3d5ca3a9fd163000000004295ccd800000000', 9, '2015-07-13 11:14:18.054986', '2015-07-13 11:14:18.142891', '10.0.1.101', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (719, 'X55a3d5ca1d37a858000000004ca0a50800000000', 9, '2015-07-13 11:14:18.142891', '2015-07-13 11:14:18.146931', '10.0.1.101', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (720, 'X55a3d5ca769ea2f0000000001a3d707900000000', 9, '2015-07-13 11:14:18.146931', '2015-07-13 11:14:18.150945', '10.0.1.101', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (721, 'X55a3d5ca3c0154040000000060cc6f8f00000000', 9, '2015-07-13 11:14:18.150945', '2015-07-13 11:14:23.903335', '10.0.1.101', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (722, 'X55a3d5d057448dc0000000002d354b6600000000', 9, '2015-07-13 11:14:23.903335', '2015-07-13 11:14:37.754117', '10.0.1.101', 'unlock', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (723, 'X55a3d5de375b5a0200000000743baadd00000000', 9, '2015-07-13 11:14:37.754117', '2015-07-13 11:14:39.146472', '10.0.1.101', 'unlock', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (724, 'X55a3d5df5511c6f300000000162d42cd00000000', 9, '2015-07-13 11:14:39.146472', '2015-07-13 11:14:41.673318', '10.0.1.101', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (725, 'X55a3d5e21eb41aff00000000274095c000000000', 9, '2015-07-13 11:14:41.673318', '2015-07-13 11:14:41.738978', '10.0.1.101', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (726, 'X55a3d5e255b193d00000000033d5e34800000000', 9, '2015-07-13 11:14:41.738978', '2015-07-13 11:14:41.742063', '10.0.1.101', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (727, 'X55a3d5e2541e76c7000000002db44fd700000000', 9, '2015-07-13 11:14:41.742063', '2015-07-13 11:14:41.745716', '10.0.1.101', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (728, 'X55a3d5e2514059490000000037e6366500000000', 9, '2015-07-13 11:14:41.745716', '2015-07-13 11:23:22.45814', '10.0.1.101', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (729, 'X55a3d7ea33f985b200000000597ceeef00000000', 9, '2015-07-13 11:23:22.45814', '2015-07-13 11:23:26.024249', '10.0.1.101', 'spamfilter', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (730, 'X55a3d7ee61a00f8900000000b7d49ec00000000', 9, '2015-07-13 11:23:26.024249', '2015-07-13 11:23:35.342074', '10.0.1.101', 'listadmins', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (731, 'X55a3d7f7571d574f0000000040f8756700000000', 9, '2015-07-13 11:23:35.342074', '2015-07-13 11:23:35.353623', '10.0.1.101', 'listadmins', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (732, 'X55a3d7f71983ec300000000a3e05d100000000', 9, '2015-07-13 11:23:35.353623', '2015-07-13 11:23:40.659062', '10.0.1.101', 'search_ads', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (733, 'X55a3d7fd7e580df50000000024f22d9000000000', 9, '2015-07-13 11:23:40.659062', '2015-07-13 11:23:40.670646', '10.0.1.101', 'listadmins', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (734, 'X55a3d7fd3fa2f255000000006d7f1a8f00000000', 9, '2015-07-13 11:23:40.670646', '2015-07-13 11:23:54.456974', '10.0.1.101', 'search_ads', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (735, 'X55a3d80a720c455f000000002a7f554f00000000', 9, '2015-07-13 11:23:54.456974', '2015-07-13 11:23:54.504648', '10.0.1.101', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (736, 'X55a3d80b5aa2cb1a000000007b2ddee500000000', 9, '2015-07-13 11:23:54.504648', '2015-07-13 11:23:54.508978', '10.0.1.101', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (737, 'X55a3d80b54aacdd4000000005dfd110b00000000', 9, '2015-07-13 11:23:54.508978', '2015-07-13 11:23:54.512977', '10.0.1.101', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (738, 'X55a3d80b697a5af7000000002c14d03300000000', 9, '2015-07-13 11:23:54.512977', '2015-07-13 11:24:28.336169', '10.0.1.101', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (739, 'X55a3d82c6d0d459a0000000025419f3300000000', 9, '2015-07-13 11:24:28.336169', '2015-07-13 11:24:28.366988', '10.0.1.101', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (740, 'X55a3d82c2f7fa505000000001799b52e00000000', 9, '2015-07-13 11:24:28.366988', '2015-07-13 11:24:28.370154', '10.0.1.101', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (741, 'X55a3d82c3c8be2bf00000000536e2e5a00000000', 9, '2015-07-13 11:24:28.370154', '2015-07-13 11:24:28.373556', '10.0.1.101', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (742, 'X55a3d82c40ed48e9000000003e63910500000000', 9, '2015-07-13 11:24:28.373556', '2015-07-13 11:24:43.918157', '10.0.1.101', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (743, 'X55a3d83c68e2e654000000007534aac00000000', 9, '2015-07-13 11:24:43.918157', '2015-07-13 11:25:20.966527', '10.0.1.101', 'review', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (744, 'X55a3d861281299100000000594c2a1a00000000', 9, '2015-07-13 11:25:20.966527', '2015-07-13 11:25:20.996382', '10.0.1.101', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (745, 'X55a3d86137493b25000000005b9bf45400000000', 9, '2015-07-13 11:25:20.996382', '2015-07-13 11:25:20.999968', '10.0.1.101', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (746, 'X55a3d8617b0c4870000000037c16be400000000', 9, '2015-07-13 11:25:20.999968', '2015-07-13 11:25:21.003626', '10.0.1.101', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (747, 'X55a3d8615d030b1700000000318f680000000000', 9, '2015-07-13 11:25:21.003626', '2015-07-13 11:25:30.787883', '10.0.1.101', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (748, 'X55a3d86b49291d2f0000000054ca802800000000', 9, '2015-07-13 11:25:30.787883', '2015-07-13 11:26:08.672214', '10.0.1.101', 'review', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (749, 'X55a3d8912838ae8200000000f76646200000000', 9, '2015-07-13 11:26:08.672214', '2015-07-13 11:26:08.705027', '10.0.1.101', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (750, 'X55a3d8914dd9f567000000001a3d3bbd00000000', 9, '2015-07-13 11:26:08.705027', '2015-07-13 11:26:08.70856', '10.0.1.101', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (751, 'X55a3d8917a428d2b000000002411affa00000000', 9, '2015-07-13 11:26:08.70856', '2015-07-13 11:26:08.712514', '10.0.1.101', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (752, 'X55a3d891280083bb000000008b47c2100000000', 9, '2015-07-13 11:26:08.712514', '2015-07-13 11:26:18.766506', '10.0.1.101', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (753, 'X55a3d89b246f5263000000001d78f31d00000000', 9, '2015-07-13 11:26:18.766506', '2015-07-13 11:27:03.863455', '10.0.1.101', 'review', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (754, 'X55a3d8c85c9d6109000000006456065000000000', 9, '2015-07-13 11:27:03.863455', '2015-07-13 11:27:11.548774', '10.0.1.101', 'spamfilter', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (755, 'X55a3d8d06dfc06f70000000041a6601a00000000', 9, '2015-07-13 11:27:11.548774', '2015-07-13 11:27:13.470005', '10.0.1.101', 'listadmins', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (756, 'X55a3d8d14eee2ca30000000072a5c90100000000', 9, '2015-07-13 11:27:13.470005', '2015-07-13 11:27:23.835119', '10.0.1.101', 'listadmins', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (757, 'X55a3d8dc4274fbec00000000670adc2300000000', 9, '2015-07-13 11:27:23.835119', '2015-07-13 11:27:23.844513', '10.0.1.101', 'saveadmin', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (758, 'X55a3d8dc10277af6000000003b2b76c800000000', 9, '2015-07-13 11:27:23.844513', '2015-07-13 11:27:25.610761', '10.0.1.101', 'listadmins', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (759, 'X55a3d8e129ea5825000000004371986700000000', 9, '2015-07-13 11:27:28.631587', '2015-07-13 11:27:28.706922', '10.0.1.101', 'authenticate', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (760, 'X55a3d8e11d49d63e00000000126dcf3600000000', 9, '2015-07-13 11:27:28.706922', '2015-07-13 11:27:34.685563', '10.0.1.101', 'spamfilter', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (761, 'X55a3d8e727054fcf000000006b77888500000000', 9, '2015-07-13 11:27:34.685563', '2015-07-13 11:27:50.433508', '10.0.1.101', 'refund_search', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (762, 'X55a3d8f615b2571f000000001af97b2700000000', 9, '2015-07-13 11:27:50.433508', '2015-07-13 11:27:55.162736', '10.0.1.101', 'listadmins', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (763, 'X55a3d8fb517be6160000000063c18d5d00000000', 9, '2015-07-13 11:27:55.162736', '2015-07-13 11:27:55.173582', '10.0.1.101', 'listadmins', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (764, 'X55a3d8fb32a8e68700000000687c839e00000000', 9, '2015-07-13 11:27:55.173582', '2015-07-13 11:28:16.819743', '10.0.1.101', 'search_ads', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (765, 'X55a3d9117a4a0183000000004937f24400000000', 9, '2015-07-13 11:28:16.819743', '2015-07-13 11:28:16.830813', '10.0.1.101', 'listadmins', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (766, 'X55a3d9113167833c0000000019c91e500000000', 9, '2015-07-13 11:28:16.830813', '2015-07-13 11:28:20.082296', '10.0.1.101', 'search_ads', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (767, 'X55a3d91453e2b3500000000780be8cc00000000', 9, '2015-07-13 11:28:20.082296', '2015-07-13 11:28:20.091136', '10.0.1.101', 'listadmins', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (768, 'X55a3d9147cf10841000000001ce3f97a00000000', 9, '2015-07-13 11:28:20.091136', '2015-07-13 11:28:22.844213', '10.0.1.101', 'search_ads', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (769, 'X55a3d91744b54eeb000000003ca174b300000000', 9, '2015-07-13 11:28:22.844213', '2015-07-13 11:28:22.853313', '10.0.1.101', 'listadmins', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (770, 'X55a3d917332126e200000000137eb4e300000000', 9, '2015-07-13 11:28:22.853313', '2015-07-13 11:28:28.956678', '10.0.1.101', 'search_ads', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (771, 'X55a3d91d763c164000000001e8d5c9400000000', 9, '2015-07-13 11:28:28.956678', '2015-07-13 11:28:29.012176', '10.0.1.101', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (772, 'X55a3d91d27a3480d000000001f9932cf00000000', 9, '2015-07-13 11:28:29.012176', '2015-07-13 11:28:29.015797', '10.0.1.101', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (773, 'X55a3d91d29f456a5000000001243c29900000000', 9, '2015-07-13 11:28:29.015797', '2015-07-13 11:28:29.020063', '10.0.1.101', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (774, 'X55a3d91d4461e66a00000000164d114700000000', 9, '2015-07-13 11:28:29.020063', '2015-07-13 11:28:53.370977', '10.0.1.101', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (775, 'X55a3d93558b5ed7d0000000033208ef000000000', 9, '2015-07-13 11:28:53.370977', '2015-07-13 11:28:53.380206', '10.0.1.101', 'listadmins', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (776, 'X55a3d9355ff3734000000004982168700000000', 9, '2015-07-13 11:28:53.380206', '2015-07-13 11:28:55.163582', '10.0.1.101', 'search_ads', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (777, 'X55a3d93773727879000000005c66a3b000000000', 9, '2015-07-13 11:28:55.163582', '2015-07-13 11:28:55.173894', '10.0.1.101', 'listadmins', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (778, 'X55a3d9377ebf306700000000559c07a00000000', 9, '2015-07-13 11:28:55.173894', '2015-07-13 11:28:57.743836', '10.0.1.101', 'search_ads', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (779, 'X55a3d93a19347cda000000002671b6600000000', 9, '2015-07-13 11:28:57.743836', '2015-07-13 11:28:57.799735', '10.0.1.101', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (780, 'X55a3d93a5019f4410000000013b1e4d400000000', 9, '2015-07-13 11:28:57.799735', '2015-07-13 11:28:57.803225', '10.0.1.101', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (781, 'X55a3d93a34576225000000005bc00d5500000000', 9, '2015-07-13 11:28:57.803225', '2015-07-13 11:28:57.806809', '10.0.1.101', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (782, 'X55a3d93a5d306a23000000001abe94ce00000000', 9, '2015-07-13 11:28:57.806809', '2015-07-13 11:29:06.842335', '10.0.1.101', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (783, 'X55a3d94374e999b1000000002ce28ff300000000', 9, '2015-07-13 11:29:06.842335', '2015-07-13 11:29:18.330101', '10.0.1.101', 'review', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (784, 'X55a3d94e1814cb26000000004d149c5800000000', 9, '2015-07-13 11:29:18.330101', '2015-07-13 11:29:18.339014', '10.0.1.101', 'listadmins', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (785, 'X55a3d94e397149920000000053b6f7700000000', 9, '2015-07-13 11:29:18.339014', '2015-07-13 11:29:21.364792', '10.0.1.101', 'search_ads', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (786, 'X55a3d95132ca31100000000538902300000000', 9, '2015-07-13 11:29:21.364792', '2015-07-13 11:29:21.375066', '10.0.1.101', 'listadmins', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (787, 'X55a3d95156cdda65000000001dff5c9e00000000', 9, '2015-07-13 11:29:21.375066', '2015-07-13 11:29:23.996687', '10.0.1.101', 'search_ads', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (788, 'X55a3d95473651e9200000000314c9b0e00000000', 9, '2015-07-13 11:29:23.996687', '2015-07-13 11:29:24.047638', '10.0.1.101', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (789, 'X55a3d954341a8285000000002714b77200000000', 9, '2015-07-13 11:29:24.047638', '2015-07-13 11:29:24.051546', '10.0.1.101', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (790, 'X55a3d954729f4999000000004949575a00000000', 9, '2015-07-13 11:29:24.051546', '2015-07-13 11:29:24.055299', '10.0.1.101', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (791, 'X55a3d9546944a7ce000000009b5c62c00000000', 9, '2015-07-13 11:29:24.055299', '2015-07-13 11:29:32.831619', '10.0.1.101', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (792, 'X55a3d95d1477132400000000731f317b00000000', 9, '2015-07-13 11:29:32.831619', '2015-07-13 11:30:26.664956', '10.0.1.101', 'review', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (793, 'X55a3d993585127ea0000000056d31ab400000000', 9, '2015-07-13 11:30:26.664956', '2015-07-13 11:30:26.676577', '10.0.1.101', 'listadmins', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (794, 'X55a3d99351f2437e00000000490bc45900000000', 9, '2015-07-13 11:30:26.676577', '2015-07-13 11:30:29.072483', '10.0.1.101', 'search_ads', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (795, 'X55a3d9955a2ddf2f0000000030b801f000000000', 9, '2015-07-13 11:30:29.072483', '2015-07-13 11:30:29.08391', '10.0.1.101', 'listadmins', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (796, 'X55a3d9955611c5980000000073f52cca00000000', 9, '2015-07-13 11:30:29.08391', '2015-07-13 11:30:31.667034', '10.0.1.101', 'search_ads', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (797, 'X55a3d99865d4d09e000000005f1e4b6300000000', 9, '2015-07-13 11:30:31.667034', '2015-07-13 11:30:31.711131', '10.0.1.101', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (798, 'X55a3d9986ec21e140000000069049a7200000000', 9, '2015-07-13 11:30:31.711131', '2015-07-13 11:30:31.715535', '10.0.1.101', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (799, 'X55a3d9984a8379400000000049e4456e00000000', 9, '2015-07-13 11:30:31.715535', '2015-07-13 11:30:31.718839', '10.0.1.101', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (800, 'X55a3d9987ee447a00000000027bc3ea600000000', 9, '2015-07-13 11:30:31.718839', '2015-07-13 11:30:37.367725', '10.0.1.101', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (801, 'X55a3d99d4494641b0000000053b79d3400000000', 9, '2015-07-13 11:30:37.367725', '2015-07-13 11:31:30.01883', '10.0.1.101', 'review', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (802, 'X55a3d9d23d2dcefb00000000328debfe00000000', 9, '2015-07-13 11:31:30.01883', '2015-07-13 11:31:30.028974', '10.0.1.101', 'listadmins', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (803, 'X55a3d9d26e643cef000000007e2a961500000000', 9, '2015-07-13 11:31:30.028974', '2015-07-13 11:32:35.977082', '10.0.1.101', 'search_ads', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (804, 'X55a3da14102684e200000000561cf45700000000', 9, '2015-07-13 11:32:35.977082', '2015-07-13 11:32:45.665276', '10.0.1.101', 'refund_search', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (805, 'X55a3da1e63c1a87100000000d44bd2b00000000', 9, '2015-07-13 11:32:45.665276', '2015-07-13 11:32:45.669258', '10.0.1.101', 'refund_update', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (806, 'X55a3da1e52f3ff37000000002c486e100000000', 9, '2015-07-13 11:32:45.669258', '2015-07-13 11:33:03.451616', '10.0.1.101', 'refund_search', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (807, 'X55a3da2f41fe448a00000000398cbf9800000000', 9, '2015-07-13 11:33:03.451616', '2015-07-13 11:33:12.169799', '10.0.1.101', 'refund_search', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (808, 'X55a3da3878ed419d000000001930557700000000', 9, '2015-07-13 11:33:12.169799', '2015-07-13 11:33:12.173856', '10.0.1.101', 'refund_update', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (809, 'X55a3da38581a90f20000000043e9830700000000', 9, '2015-07-13 11:33:12.173856', '2015-07-13 11:33:18.040696', '10.0.1.101', 'refund_search', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (810, 'X55a3da3e20510c4400000000446016f00000000', 9, '2015-07-13 11:33:18.040696', '2015-07-13 11:33:18.044248', '10.0.1.101', 'refund_update', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (811, 'X55a3da3e201954fb00000000cbb446900000000', 9, '2015-07-13 11:33:18.044248', '2015-07-13 12:33:18.044248', '10.0.1.101', 'refund_search', NULL, NULL);


 INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (87, 1, 698, 'locked', 'checkout', '2015-07-13 11:14:18.058835', '10.0.1.101', 717);
 INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (92, 1, 699, 'locked', 'checkout', '2015-07-13 11:14:18.058835', '10.0.1.101', 717);
 INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (87, 1, 700, 'pending_review', 'checkin', '2015-07-13 11:14:23.904799', '10.0.1.101', 721);
 INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (92, 1, 701, 'pending_review', 'checkin', '2015-07-13 11:14:37.755518', '10.0.1.101', 722);
 INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (123, 2, 702, 'reg', 'initial', '2015-07-13 11:20:53.192971', '10.0.1.101', NULL);
 INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (123, 2, 703, 'unpaid', 'pending_pay', '2015-07-13 11:20:53.192971', '10.0.1.101', NULL);
 INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (123, 2, 704, 'accepted', 'pay', '2015-07-13 11:20:57.510467', NULL, NULL);
 INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (118, 2, 705, 'reg', 'initial', '2015-07-13 11:21:40.068128', '10.0.1.101', NULL);
 INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (118, 2, 706, 'unpaid', 'pending_pay', '2015-07-13 11:21:40.068128', '10.0.1.101', NULL);
 INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (118, 2, 707, 'accepted', 'pay', '2015-07-13 11:21:41.526607', NULL, NULL);
 INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (117, 2, 708, 'reg', 'initial', '2015-07-13 11:21:56.850531', '10.0.1.101', NULL);
 INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (117, 2, 709, 'unpaid', 'pending_pay', '2015-07-13 11:21:56.850531', '10.0.1.101', NULL);
 INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (117, 2, 710, 'accepted', 'pay', '2015-07-13 11:22:00.822258', NULL, NULL);
 INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (92, 2, 711, 'reg', 'initial', '2015-07-13 11:22:37.18336', '10.0.1.101', NULL);
 INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (92, 2, 712, 'unpaid', 'pending_pay', '2015-07-13 11:22:37.18336', '10.0.1.101', NULL);
 INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (92, 2, 713, 'accepted', 'pay', '2015-07-13 11:22:38.831467', NULL, NULL);
 INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (87, 2, 714, 'reg', 'initial', '2015-07-13 11:23:12.520326', '10.0.1.101', NULL);
 INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (87, 2, 715, 'unpaid', 'pending_pay', '2015-07-13 11:23:12.520326', '10.0.1.101', NULL);
 INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (87, 2, 716, 'accepted', 'pay', '2015-07-13 11:23:14.542545', NULL, NULL);
 INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (123, 1, 717, 'refused', 'refuse', '2015-07-13 11:24:43.920119', '10.0.1.101', 742);
 INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (123, 2, 718, 'refused', 'refuse', '2015-07-13 11:24:43.920119', '10.0.1.101', 742);
 INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (118, 1, 719, 'refused', 'refuse', '2015-07-13 11:25:30.790041', '10.0.1.101', 747);
 INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (118, 2, 720, 'refused', 'refuse', '2015-07-13 11:25:30.790041', '10.0.1.101', 747);
 INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (118, 3, 721, 'reg', 'initial', '2015-07-13 11:25:30.790041', '10.0.1.101', NULL);
 INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (118, 3, 722, 'accepted', 'accept', '2015-07-13 11:25:30.790041', '10.0.1.101', NULL);
 INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (117, 1, 723, 'refused', 'refuse', '2015-07-13 11:26:18.768895', '10.0.1.101', 752);
 INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (117, 2, 724, 'refused', 'refuse', '2015-07-13 11:26:18.768895', '10.0.1.101', 752);
 INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (92, 1, 725, 'refused', 'refuse', '2015-07-13 11:29:06.845623', '10.0.1.101', 782);
 INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (92, 2, 726, 'refused', 'refuse', '2015-07-13 11:29:06.845623', '10.0.1.101', 782);
 INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (87, 1, 727, 'refused', 'refuse', '2015-07-13 11:29:32.833879', '10.0.1.101', 791);
 INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (87, 2, 728, 'refused', 'refuse', '2015-07-13 11:29:32.833879', '10.0.1.101', 791);
 INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (87, 3, 729, 'reg', 'initial', '2015-07-13 11:29:32.833879', '10.0.1.101', NULL);
 INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (87, 3, 730, 'accepted', 'accept', '2015-07-13 11:29:32.833879', '10.0.1.101', NULL);
 INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (95, 2, 731, 'reg', 'initial', '2015-07-13 11:29:54.882918', '10.0.1.101', NULL);
 INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (95, 2, 732, 'unverified', 'verifymail', '2015-07-13 11:29:54.882918', '10.0.1.101', NULL);
 INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (95, 2, 733, 'pending_review', 'verify', '2015-07-13 11:29:54.923718', '10.0.1.101', NULL);
 INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (95, 3, 734, 'reg', 'initial', '2015-07-13 11:30:03.024541', '10.0.1.101', NULL);
 INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (95, 3, 735, 'unpaid', 'pending_pay', '2015-07-13 11:30:03.024541', '10.0.1.101', NULL);
 INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (95, 3, 736, 'accepted', 'pay', '2015-07-13 11:30:04.707483', NULL, NULL);
 INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (95, 2, 737, 'refused', 'refuse', '2015-07-13 11:30:37.369841', '10.0.1.101', 800);
 INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (95, 3, 738, 'refused', 'refuse', '2015-07-13 11:30:37.369841', '10.0.1.101', 800);

 SELECT pg_catalog.setval('action_states_state_id_seq', 738, true);

 INSERT INTO ad_changes (ad_id, action_id, state_id, is_param, column_name, old_value, new_value) VALUES (118, 2, 707, true, 'gallery', NULL, '2015-07-13 11:31:41');
 INSERT INTO ad_changes (ad_id, action_id, state_id, is_param, column_name, old_value, new_value) VALUES (87, 2, 716, true, 'gallery', NULL, '2015-07-13 11:33:14');
 INSERT INTO ad_changes (ad_id, action_id, state_id, is_param, column_name, old_value, new_value) VALUES (118, 3, 722, true, 'gallery', '2015-07-13 11:31:41', NULL);
 INSERT INTO ad_changes (ad_id, action_id, state_id, is_param, column_name, old_value, new_value) VALUES (87, 3, 730, true, 'gallery', '2015-07-13 11:33:14', NULL);
 INSERT INTO ad_changes (ad_id, action_id, state_id, is_param, column_name, old_value, new_value) VALUES (95, 2, 731, false, 'subject', 'Adeptus Astartes Codex', 'Adeptus Astartes Codex edited');
 INSERT INTO ad_changes (ad_id, action_id, state_id, is_param, column_name, old_value, new_value) VALUES (95, 2, 731, false, 'company_ad', 'false', 'true');

 INSERT INTO ad_image_changes (ad_id, action_id, state_id, seq_no, name, is_new) VALUES (95, 2, 731, 0, NULL, false);

DELETE FROM admin_privs WHERE admin_id = 9;
 INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'ais');
 INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'notice_abuse');
 INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'adqueue');
 INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'adminad');
 INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'Adminuf');
 INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'admin');
 INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'config');
 INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'Refund');
 INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'filter');
 INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'landing_page');
 INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'popular_ads');
 INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'pack_autos');
 INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'scarface');
 INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'search');
 INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'telesales');
 INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'stores');
 INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'Websql');
 INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'adqueue.admin_settings');
 INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'adqueue.show_num_ads=2');
 INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'adqueue.approve_refused');
 INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'adqueue.refusals');
 INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'adqueue.admin_queue');
 INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'adminad.clear_ad');
 INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'adminad.edit_ad');
 INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'Refund.searchRefund');
 INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'filter.lists');
 INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'filter.rules');
 INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'filter.spamfilter');
 INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'pack_autos.searchandassign');
 INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'pack_autos.update_pro_param');
 INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'scarface.warning');
 INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'search.abuse_report');
 INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'search.uid_emails');
 INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'search.maillog');
 INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'search.mass_delete');
 INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'search.paylog');
 INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'search.reviewers');
 INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'search.search_ads');
 INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'search.account');
 INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'search.undo_delete');
 INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'telesales.edit_account');
 INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'telesales.sell_products');
 INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'stores.create_store');
 INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'stores.edit_store');
 INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'stores.extend_store');

 SELECT pg_catalog.setval('pay_code_seq', 467, true);

 INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (213, 'save', 0, '12345', NULL, '2015-07-13 11:20:53', 166, 'SAVE');
 INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (214, 'paycard_save', 0, '12345', NULL, '2015-07-13 11:20:57', 166, 'SAVE');
 INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (215, 'paycard', 2700, '00166a', NULL, '2015-07-13 11:20:58', 166, 'OK');
 INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (216, 'paycard', 2700, '12345', NULL, '2015-07-13 11:20:58', 166, 'OK');
 INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (217, 'save', 0, '65432', NULL, '2015-07-13 11:21:40', 167, 'SAVE');
 INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (218, 'paycard_save', 0, '65432', NULL, '2015-07-13 11:21:41', 167, 'SAVE');
 INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (219, 'paycard', 3600, '00167a', NULL, '2015-07-13 11:21:42', 167, 'OK');
 INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (220, 'paycard', 3600, '65432', NULL, '2015-07-13 11:21:42', 167, 'OK');
 INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (221, 'save', 0, '32323', NULL, '2015-07-13 11:21:57', 168, 'SAVE');
 INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (222, 'paycard_save', 0, '32323', NULL, '2015-07-13 11:22:01', 168, 'SAVE');
 INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (223, 'paycard', 4500, '00168a', NULL, '2015-07-13 11:22:01', 168, 'OK');
 INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (224, 'paycard', 4500, '32323', NULL, '2015-07-13 11:22:01', 168, 'OK');
 INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (225, 'save', 0, '54545', NULL, '2015-07-13 11:22:37', 169, 'SAVE');
 INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (226, 'paycard_save', 0, '54545', NULL, '2015-07-13 11:22:39', 169, 'SAVE');
 INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (227, 'paycard', 2700, '00169a', NULL, '2015-07-13 11:22:39', 169, 'OK');
 INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (228, 'paycard', 2700, '54545', NULL, '2015-07-13 11:22:39', 169, 'OK');
 INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (229, 'save', 0, '90001', NULL, '2015-07-13 11:23:13', 170, 'SAVE');
 INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (230, 'paycard_save', 0, '90001', NULL, '2015-07-13 11:23:14', 170, 'SAVE');
 INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (231, 'paycard', 3600, '00170a', NULL, '2015-07-13 11:23:15', 170, 'OK');
 INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (232, 'paycard', 3600, '90001', NULL, '2015-07-13 11:23:15', 170, 'OK');
 INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (233, 'save', 0, '633026231', NULL, '2015-07-13 11:29:55', 171, 'SAVE');
 INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (234, 'verify', 0, '633026231', NULL, '2015-07-13 11:29:55', 171, 'OK');
 INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (235, 'save', 0, '90002', NULL, '2015-07-13 11:30:03', 172, 'SAVE');
 INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (236, 'paycard_save', 0, '90002', NULL, '2015-07-13 11:30:05', 172, 'SAVE');
 INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (237, 'paycard', 2700, '00172a', NULL, '2015-07-13 11:30:05', 172, 'OK');
 INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (238, 'paycard', 2700, '90002', NULL, '2015-07-13 11:30:05', 172, 'OK');

 SELECT pg_catalog.setval('pay_log_pay_log_id_seq', 238, true);

 INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (213, 0, 'remote_addr', '10.0.1.101');
 INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (214, 0, 'auth_code', '568995');
 INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (214, 1, 'trans_date', '13/07/2015 11:20:54');
 INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (214, 2, 'placements_type', 'VN');
 INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (214, 3, 'placements_number', '1');
 INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (214, 4, 'pay_type', 'credit');
 INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (214, 5, 'external_response', '0');
 INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (214, 6, 'external_id', '66477661954035095428');
 INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (214, 7, 'remote_addr', '10.0.1.72');
 INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (215, 0, 'webpay', '1');
 INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (217, 0, 'remote_addr', '10.0.1.101');
 INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (218, 0, 'auth_code', '664494');
 INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (218, 1, 'trans_date', '13/07/2015 11:21:41');
 INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (218, 2, 'placements_type', 'VN');
 INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (218, 3, 'placements_number', '1');
 INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (218, 4, 'pay_type', 'credit');
 INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (218, 5, 'external_response', '0');
 INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (218, 6, 'external_id', '12597099063829179619');
 INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (218, 7, 'remote_addr', '10.0.1.72');
 INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (219, 0, 'webpay', '1');
 INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (221, 0, 'remote_addr', '10.0.1.101');
 INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (222, 0, 'auth_code', '246768');
 INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (222, 1, 'trans_date', '13/07/2015 11:22:00');
 INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (222, 2, 'placements_type', 'VN');
 INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (222, 3, 'placements_number', '1');
 INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (222, 4, 'pay_type', 'credit');
 INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (222, 5, 'external_response', '0');
 INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (222, 6, 'external_id', '68265446457411434240');
 INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (222, 7, 'remote_addr', '10.0.1.72');
 INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (223, 0, 'webpay', '1');
 INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (225, 0, 'remote_addr', '10.0.1.101');
 INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (226, 0, 'auth_code', '528214');
 INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (226, 1, 'trans_date', '13/07/2015 11:22:38');
 INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (226, 2, 'placements_type', 'VN');
 INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (226, 3, 'placements_number', '1');
 INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (226, 4, 'pay_type', 'credit');
 INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (226, 5, 'external_response', '0');
 INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (226, 6, 'external_id', '14617745964633739687');
 INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (226, 7, 'remote_addr', '10.0.1.72');
 INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (227, 0, 'webpay', '1');
 INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (229, 0, 'remote_addr', '10.0.1.101');
 INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (230, 0, 'auth_code', '256706');
 INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (230, 1, 'trans_date', '13/07/2015 11:23:14');
 INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (230, 2, 'placements_type', 'VN');
 INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (230, 3, 'placements_number', '1');
 INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (230, 4, 'pay_type', 'credit');
 INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (230, 5, 'external_response', '0');
 INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (230, 6, 'external_id', '47877401130941298631');
 INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (230, 7, 'remote_addr', '10.0.1.72');
 INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (231, 0, 'webpay', '1');
 INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (233, 0, 'adphone', '26437660');
 INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (233, 1, 'remote_addr', '10.0.1.101');
 INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (234, 0, 'remote_addr', '10.0.1.101');
 INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (235, 0, 'remote_addr', '10.0.1.101');
 INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (236, 0, 'auth_code', '367255');
 INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (236, 1, 'trans_date', '13/07/2015 11:30:04');
 INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (236, 2, 'placements_type', 'VN');
 INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (236, 3, 'placements_number', '1');
 INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (236, 4, 'pay_type', 'credit');
 INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (236, 5, 'external_response', '0');
 INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (236, 6, 'external_id', '43116539473536228596');
 INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (236, 7, 'remote_addr', '10.0.1.72');
 INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (237, 0, 'webpay', '1');

 SELECT pg_catalog.setval('payment_groups_payment_group_id_seq', 172, true);

 INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (166, 'upselling_weekly_bump', 2700, 0);
 INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (167, 'upselling_gallery', 3600, 0);
 INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (168, 'upselling_daily_bump', 4500, 0);
 INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (169, 'upselling_weekly_bump', 2700, 0);
 INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (170, 'upselling_gallery', 3600, 0);
 INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (171, 'ad_action', 0, 0);
 INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (172, 'upselling_weekly_bump', 2700, 0);

 INSERT INTO purchase (purchase_id, doc_type, doc_num, external_doc_id, status, receipt, delivery, rut, name, lob, address, region, communes, contact, email, tax, discount, seller_id, total_price, payment_group_id, payment_method, account_id) VALUES (2, 'bill', 1, 1001, 'confirmed', '2015-07-13 11:20:53.192971', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'user.01@schibsted.cl', 0, 0, NULL, 2700, 166, 'webpay', NULL);
 INSERT INTO purchase (purchase_id, doc_type, doc_num, external_doc_id, status, receipt, delivery, rut, name, lob, address, region, communes, contact, email, tax, discount, seller_id, total_price, payment_group_id, payment_method, account_id) VALUES (3, 'bill', 2, 1002, 'confirmed', '2015-07-13 11:21:40.068128', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'user.01@schibsted.cl', 0, 0, NULL, 3600, 167, 'webpay', NULL);
 INSERT INTO purchase (purchase_id, doc_type, doc_num, external_doc_id, status, receipt, delivery, rut, name, lob, address, region, communes, contact, email, tax, discount, seller_id, total_price, payment_group_id, payment_method, account_id) VALUES (4, 'bill', 3, 1003, 'confirmed', '2015-07-13 11:21:56.850531', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'user.01@schibsted.cl', 0, 0, NULL, 4500, 168, 'webpay', NULL);
 INSERT INTO purchase (purchase_id, doc_type, doc_num, external_doc_id, status, receipt, delivery, rut, name, lob, address, region, communes, contact, email, tax, discount, seller_id, total_price, payment_group_id, payment_method, account_id) VALUES (5, 'bill', 4, 1004, 'confirmed', '2015-07-13 11:22:37.18336', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'alan@brito.cl', 0, 0, NULL, 2700, 169, 'webpay', NULL);
 INSERT INTO purchase (purchase_id, doc_type, doc_num, external_doc_id, status, receipt, delivery, rut, name, lob, address, region, communes, contact, email, tax, discount, seller_id, total_price, payment_group_id, payment_method, account_id) VALUES (6, 'bill', 5, 1005, 'confirmed', '2015-07-13 11:23:12.520326', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'uid1@blocket.se', 0, 0, NULL, 3600, 170, 'webpay', NULL);
 INSERT INTO purchase (purchase_id, doc_type, doc_num, external_doc_id, status, receipt, delivery, rut, name, lob, address, region, communes, contact, email, tax, discount, seller_id, total_price, payment_group_id, payment_method, account_id) VALUES (7, 'bill', 6, 1006, 'confirmed', '2015-07-13 11:30:03.024541', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'prepaid5@blocket.se', 0, 0, NULL, 2700, 172, 'webpay', 1);

 INSERT INTO purchase_detail (purchase_detail_id, purchase_id, product_id, price, action_id, ad_id, payment_group_id) VALUES (2, 2, 101, 2700, 2, 123, 166);
 INSERT INTO purchase_detail (purchase_detail_id, purchase_id, product_id, price, action_id, ad_id, payment_group_id) VALUES (3, 3, 102, 3600, 2, 118, 167);
 INSERT INTO purchase_detail (purchase_detail_id, purchase_id, product_id, price, action_id, ad_id, payment_group_id) VALUES (4, 4, 103, 4500, 2, 117, 168);
 INSERT INTO purchase_detail (purchase_detail_id, purchase_id, product_id, price, action_id, ad_id, payment_group_id) VALUES (5, 5, 101, 2700, 2, 92, 169);
 INSERT INTO purchase_detail (purchase_detail_id, purchase_id, product_id, price, action_id, ad_id, payment_group_id) VALUES (6, 6, 102, 3600, 2, 87, 170);
 INSERT INTO purchase_detail (purchase_detail_id, purchase_id, product_id, price, action_id, ad_id, payment_group_id) VALUES (7, 7, 101, 2700, 3, 95, 172);

 SELECT pg_catalog.setval('purchase_detail_purchase_detail_id_seq', 7, true);
 SELECT pg_catalog.setval('purchase_purchase_id_seq', 7, true);

 INSERT INTO purchase_states (purchase_id, purchase_state_id, status, "timestamp", remote_addr) VALUES (2, 1, 'pending', '2015-07-13 11:20:53.192971', '10.0.1.101');
 INSERT INTO purchase_states (purchase_id, purchase_state_id, status, "timestamp", remote_addr) VALUES (3, 2, 'pending', '2015-07-13 11:21:40.068128', '10.0.1.101');
 INSERT INTO purchase_states (purchase_id, purchase_state_id, status, "timestamp", remote_addr) VALUES (4, 3, 'pending', '2015-07-13 11:21:56.850531', '10.0.1.101');
 INSERT INTO purchase_states (purchase_id, purchase_state_id, status, "timestamp", remote_addr) VALUES (5, 4, 'pending', '2015-07-13 11:22:37.18336', '10.0.1.101');
 INSERT INTO purchase_states (purchase_id, purchase_state_id, status, "timestamp", remote_addr) VALUES (6, 5, 'pending', '2015-07-13 11:23:12.520326', '10.0.1.101');
 INSERT INTO purchase_states (purchase_id, purchase_state_id, status, "timestamp", remote_addr) VALUES (7, 6, 'pending', '2015-07-13 11:30:03.024541', '10.0.1.101');
 SELECT pg_catalog.setval('purchase_states_purchase_state_id_seq', 6, true);

 INSERT INTO refunds (refund_id, purchase_id, ad_id, action_id, first_name, last_name, rut, email, bank_account_type, bank, bank_account_number, creation_date, status) VALUES (3, 3, 118, 2, 'User2', 'One2', '100000-4', 'user.01@schibsted.cl', 2, 3, '5151515151551', '2015-07-13 11:26:02.848486', 'pending');
 INSERT INTO refunds (refund_id, purchase_id, ad_id, action_id, first_name, last_name, rut, email, bank_account_type, bank, bank_account_number, creation_date, status) VALUES (5, 7, 95, 3, 'Blocketa', 'Five', '100000-4', 'prepaid5@blocket.se', 3, 10, '123124-124124-124', '2015-07-13 11:31:14.86915', 'pending');
 INSERT INTO refunds (refund_id, purchase_id, ad_id, action_id, first_name, last_name, rut, email, bank_account_type, bank, bank_account_number, creation_date, status) VALUES (4, 4, 117, 2, 'User3', 'One3', '100000-4', 'user.01@schibsted.cl', 2, 16, '55666996665522', '2015-07-13 11:26:49.799949', 'refunded');
 INSERT INTO refunds (refund_id, purchase_id, ad_id, action_id, first_name, last_name, rut, email, bank_account_type, bank, bank_account_number, creation_date, status) VALUES (2, 2, 123, 2, 'User', 'One', '100000-4', 'user.01@schibsted.cl', 1, 16, '12424-89877', '2015-07-13 11:25:14.165126', 'failed');

 INSERT INTO review_log (ad_id, action_id, admin_id, review_time, queue, action_type, action, refusal_reason_text, category) VALUES (123, 1, 9, '2015-07-13 11:24:43.920119', 'all', 'new', 'refused', 'T�tulo y Descripci�n del Veh�culo', 8020);
 INSERT INTO review_log (ad_id, action_id, admin_id, review_time, queue, action_type, action, refusal_reason_text, category) VALUES (118, 1, 9, '2015-07-13 11:25:30.790041', 'all', 'new', 'refused', 'Varios elementos Veh�culos - Propiedad', 8020);
 INSERT INTO review_log (ad_id, action_id, admin_id, review_time, queue, action_type, action, refusal_reason_text, category) VALUES (117, 1, 9, '2015-07-13 11:26:18.768895', 'all', 'new', 'refused', 'T�tulo y Descripci�n del Veh�culo', 8020);
 INSERT INTO review_log (ad_id, action_id, admin_id, review_time, queue, action_type, action, refusal_reason_text, category) VALUES (92, 1, 9, '2015-07-13 11:29:06.845623', 'all', 'new', 'refused', 'T�tulo y Descripci�n de Otros', 3080);
 INSERT INTO review_log (ad_id, action_id, admin_id, review_time, queue, action_type, action, refusal_reason_text, category) VALUES (87, 1, 9, '2015-07-13 11:29:32.833879', 'all', 'new', 'refused', 'Restricciones para los Animales Dom�sticos', 6120);
 INSERT INTO review_log (ad_id, action_id, admin_id, review_time, queue, action_type, action, refusal_reason_text, category) VALUES (95, 2, 9, '2015-07-13 11:30:37.369841', 'all', 'edit', 'refused', 'Contrase�a', 6120);

 INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (123, 1, 717, 'reason', '1');
 INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (123, 1, 717, 'filter_name', 'all');
 INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (118, 1, 719, 'reason', '10');
 INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (118, 1, 719, 'filter_name', 'all');
 INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (117, 1, 723, 'reason', '1');
 INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (117, 1, 723, 'filter_name', 'all');
 INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (92, 1, 725, 'reason', '0');
 INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (92, 1, 725, 'filter_name', 'all');
 INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (87, 1, 727, 'reason', '3');
 INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (87, 1, 727, 'filter_name', 'all');
 INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (95, 2, 733, 'queue', 'edit');
 INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (95, 2, 737, 'reason', '25');
 INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (95, 2, 737, 'filter_name', 'all');

--
 SELECT pg_catalog.setval('tokens_token_id_seq', 811, true);

 INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (94, '2015-07-13 11:20:57.606381', '22987@takashi.schibsted.cl', NULL, NULL, NULL, NULL, NULL, NULL, '
 cmd:multi_product_mail
 to:user.01@schibsted.cl
 doc_type:bill
 product_ids:101
 ad_names:Test 07
 commit:1', NULL, 'bump_mail', NULL, NULL);
 INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (95, '2015-07-13 11:21:41.608429', '22987@takashi.schibsted.cl', NULL, NULL, NULL, NULL, NULL, NULL, '
 cmd:multi_product_mail
 to:user.01@schibsted.cl
 doc_type:bill
 product_ids:102
 ad_names:Test 03
 commit:1', NULL, 'bump_mail', NULL, NULL);
 INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (97, '2015-07-13 11:22:00.87904', '22987@takashi.schibsted.cl', NULL, NULL, NULL, NULL, NULL, NULL, '
 cmd:multi_product_mail
 to:user.01@schibsted.cl
 doc_type:bill
 product_ids:103
 ad_names:Test 02
 commit:1', NULL, 'bump_mail', NULL, NULL);
 INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (99, '2015-07-13 11:22:38.882269', '22987@takashi.schibsted.cl', NULL, NULL, NULL, NULL, NULL, NULL, '
 cmd:multi_product_mail
 to:alan@brito.cl
 doc_type:bill
 product_ids:101
 ad_names:Aviso de Alan
 commit:1', NULL, 'bump_mail', NULL, NULL);
 INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (100, '2015-07-13 11:23:14.60652', '22987@takashi.schibsted.cl', NULL, NULL, NULL, NULL, NULL, NULL, '
 cmd:multi_product_mail
 to:uid1@blocket.se
 doc_type:bill
 product_ids:102
 ad_names:Libro 1
 commit:1', NULL, 'bump_mail', NULL, NULL);
 INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (93, '2015-07-13 11:20:57.603762', '22987@takashi.schibsted.cl', '2015-07-13 11:22:57.603762', '2015-07-13 11:24:43.920119', NULL, 'CANCELED', NULL, NULL, '
 cmd:weekly_bump_ad
 ad_id:123
 action_id:2
 counter:4
 batch:1
 do_not_send_mail:1
 is_upselling:1
 commit:1', NULL, 'upselling', NULL, NULL);
 INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (96, '2015-07-13 11:22:00.876602', '22987@takashi.schibsted.cl', '2015-07-13 11:24:00.876602', '2015-07-13 11:26:18.768895', NULL, 'CANCELED', NULL, NULL, '
 cmd:weekly_bump_ad
 ad_id:117
 action_id:2
 counter:7
 batch:1
 daily_bump:1
 do_not_send_mail:1
 is_upselling:1
 commit:1', NULL, 'upselling', NULL, NULL);
 INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (98, '2015-07-13 11:22:38.880465', '22987@takashi.schibsted.cl', '2015-07-13 11:24:38.880465', '2015-07-13 11:29:06.845623', NULL, 'CANCELED', NULL, NULL, '
 cmd:weekly_bump_ad
 ad_id:92
 action_id:2
 counter:4
 batch:1
 do_not_send_mail:1
 is_upselling:1
 commit:1', NULL, 'upselling', NULL, NULL);
 INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (102, '2015-07-13 11:30:04.768126', '22987@takashi.schibsted.cl', NULL, NULL, NULL, NULL, NULL, NULL, '
 cmd:multi_product_mail
 to:prepaid5@blocket.se
 doc_type:bill
 product_ids:101
 ad_names:Adeptus Astartes Codex
 commit:1', NULL, 'bump_mail', NULL, NULL);
 INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (101, '2015-07-13 11:30:04.76576', '22987@takashi.schibsted.cl', '2015-07-13 11:32:04.76576', '2015-07-13 11:30:37.369841', NULL, 'CANCELED', NULL, NULL, '
 cmd:weekly_bump_ad
 ad_id:95
 action_id:3
 counter:4
 batch:1
 do_not_send_mail:1
 is_upselling:1
 commit:1', NULL, 'upselling', NULL, NULL);
--
 SELECT pg_catalog.setval('trans_queue_id_seq', 102, true);

--Change all passwords to 123123123
update ads set salted_passwd =  E'$1024$2y1bQ}VYRD}zF4QCbf07a971b784888658a20e62156de64dfde5ffb6', passwd = null;
update accounts set salted_passwd =  E'$1024$2y1bQ}VYRD}zF4QCbf07a971b784888658a20e62156de64dfde5ffb6';
