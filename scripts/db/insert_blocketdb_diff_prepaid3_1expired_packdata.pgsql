INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (179, '12345', 'paid', NOW() - INTERVAL '31 days', NULL);
INSERT INTO packs (pack_id, account_id, status, type, slots, used, date_start, date_end, product_id, payment_group_id, token_id) VALUES (4, 5, 'expired', 'car', 10, 0, NOW() - INTERVAL '31 days', NOW() - INTERVAL '1 days', 14010, 179, NULL);
SELECT pg_catalog.setval('packs_pack_id_seq', 4, true);
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (241, 'save', 0, '12345', NULL, NOW() - INTERVAL '31 days', 179, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (242, 'paycard_save', 0, '12345', NULL, NOW() - INTERVAL '31 days', 179, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (243, 'paycard', 235000, '12345', NULL, NOW() - INTERVAL '31 days', 179, 'OK');
SELECT pg_catalog.setval('pay_log_pay_log_id_seq', 243, true);
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (241, 0, 'remote_addr', '10.0.1.149');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (242, 0, 'auth_code', '313829');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (242, 1, 'trans_date', '01/04/2015 15:27:16');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (242, 2, 'placements_type', 'VN');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (242, 3, 'placements_number', '1');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (242, 4, 'pay_type', 'credit');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (242, 5, 'external_response', '0');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (242, 6, 'external_id', '88042884205508799759');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (242, 7, 'remote_addr', '10.0.1.208');
SELECT pg_catalog.setval('payment_groups_payment_group_id_seq', 179, true);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (179, 'PCAR_010_4', 235000, 0);
INSERT INTO purchase (purchase_id, doc_type, doc_num, external_doc_id, status, receipt, delivery, rut, name, lob, address, region, communes, contact, email, tax, discount, seller_id, total_price, payment_group_id, payment_method, account_id) VALUES (4, 'invoice', NULL, NULL, 'paid', NOW() - INTERVAL '31 days', NULL, '100000-4', 'OtherRegionAccount', 'Technology', 'Rudecindo Ortega 500', 10, 225, 'OTHER_BLOCKET', 'prepaid3@blocket.se', 0, 0, NULL, 235000, 179, 'webpay', 5);
INSERT INTO purchase_detail (purchase_detail_id, purchase_id, product_id, price, action_id, ad_id, payment_group_id) VALUES (4, 4, 14010, 235000, NULL, NULL, 179);
SELECT pg_catalog.setval('purchase_detail_purchase_detail_id_seq', 4, true);
SELECT pg_catalog.setval('purchase_purchase_id_seq', 4, true);
INSERT INTO purchase_states (purchase_id, purchase_state_id, status, "timestamp", remote_addr) VALUES (4, 3, 'pending', NOW() - INTERVAL '31 days', '10.0.1.149');
SELECT pg_catalog.setval('purchase_states_purchase_state_id_seq', 3, true);

--Change all passwords to 123123123
update ads set salted_passwd =  E'$1024$2y1bQ}VYRD}zF4QCbf07a971b784888658a20e62156de64dfde5ffb6', passwd = null;
update accounts set salted_passwd =  E'$1024$2y1bQ}VYRD}zF4QCbf07a971b784888658a20e62156de64dfde5ffb6';
