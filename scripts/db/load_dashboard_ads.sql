TRUNCATE dashboard_ads;

-- copy from ads and ad_params
INSERT INTO
	dashboard_ads (
		ad_id, user_id, list_id, list_time, company_ad, type, category, subject, body, price,
		daily_bump, weekly_bump, label, gallery_date, ad_pack_status
	)
	SELECT
		ads.ad_id, user_id, list_id, list_time, company_ad, type, category, subject, body, price,
		CASE WHEN ap1.name = 'daily_bump' THEN ap1.value
			WHEN ap1.name = 'upselling_daily_bump' THEN (ap1.value::INTEGER - 1)::VARCHAR
		END as daily_bump,
		CASE WHEN ap2.name = 'weekly_bump' THEN ap2.value
			WHEN ap2.name = 'upselling_weekly_bump' THEN (ap2.value::INTEGER - 1)::VARCHAR
		END as weekly_bump,
		ap5.value as label,
		ap3.value as gallery_date,
		ap4.value as pack_status
	FROM
		ads
		LEFT JOIN ad_params ap1 ON (ads.ad_id = ap1.ad_id AND (ap1.name = 'daily_bump' OR ap1.name = 'upselling_daily_bump'))
		LEFT JOIN ad_params ap2 ON (ads.ad_id = ap2.ad_id AND (ap2.name = 'weekly_bump' OR ap2.name = 'upselling_weekly_bump'))
		LEFT JOIN ad_params ap3 ON (ads.ad_id = ap3.ad_id AND ap3.name = 'gallery')
		LEFT JOIN ad_params ap4 ON (ads.ad_id = ap4.ad_id AND ap4.name = 'pack_status')
		LEFT JOIN ad_params ap5 ON (ads.ad_id = ap5.ad_id AND ap5.name = 'label');

-- ad status
UPDATE
	dashboard_ads da
SET
	ad_pack_status = t.ad_pack_status,
	ad_status = CASE
		WHEN t.ad_status = 'active' THEN
			CASE
				WHEN t.ad_pack_status = '1'
					THEN 'pack_show'::enum_dashboard_status
				WHEN t.action_type = 'status_change' AND t.action_state = 'accepted' AND t.ad_pack_status != '1'
					THEN 'admin_show'::enum_dashboard_status
				WHEN t.action_type = 'undo_delete' AND t.action_state = 'undo_deleted'
					THEN 'undo_deleted'::enum_dashboard_status
				WHEN t.action_type = 'editrefused' AND t.action_state = 'undo_deleted'
					THEN 'undo_deleted'::enum_dashboard_status
				WHEN EXISTS (SELECT 'edited' FROM ad_actions WHERE state = 'pending_review' AND ad_id = t.ad_id LIMIT 1)
					THEN 'review-edit'::enum_dashboard_status
				ELSE
					t.ad_status
			END
		WHEN t.ad_status = 'disabled' THEN
			CASE
				WHEN EXISTS (SELECT 'edited' FROM ad_actions WHERE state = 'pending_review' AND ad_id = t.ad_id LIMIT 1)
					THEN 'review-edit-disabled'::enum_dashboard_status
				ELSE
					t.ad_status
			END
		ELSE
			t.ad_status
	END
FROM (
	SELECT
		aac.action_type,
		aac.state as action_state,
		ads.ad_id,
		CASE
			WHEN aac.action_type = 'editrefused' AND NOT (ads.status = 'disabled' and aac.state = 'accepted') THEN
				CASE
					WHEN ads.status = 'refused' AND aac.state = 'refused' THEN
						'refused'::enum_dashboard_status
					ELSE
						aac.action_type::varchar::enum_dashboard_status
				END
			ELSE
				ads.status::varchar::enum_dashboard_status
		END as ad_status,
		CASE
			WHEN ap_pack_status.value IS NOT NULL THEN
				CASE
					WHEN ap_pack_status.value = 'active' THEN '1'
					ELSE '0'
				END
			WHEN acs.new_value IS NOT NULL THEN
				CASE
					WHEN new_value = 'deactivated' THEN '0'
					WHEN new_value = 'active' THEN '1'
					WHEN new_value = 'disabled' THEN '0'
				END
			ELSE ''
		END as ad_pack_status
	FROM
		ads
		INNER JOIN ad_actions aac USING (ad_id)
		LEFT JOIN ad_changes acs ON (acs.ad_id = aac.ad_id AND acs.action_id = aac.action_id AND acs.state_id = aac.current_state AND acs.column_name = 'pack_status')
		LEFT JOIN ad_params ap_pack_status ON (ads.ad_id = ap_pack_status.ad_id AND ap_pack_status.name = 'pack_status')
	WHERE
		aac.action_id = (SELECT MAX(action_id) FROM ad_actions WHERE ad_id = ads.ad_id)
	) t
WHERE
   da.ad_id = t.ad_id;

-- image_count
UPDATE
   dashboard_ads da
SET
	image_count = t.count,
	ad_media_id = t.ad_media_id
FROM (
	SELECT
		ad_id,
		COUNT(*) as count,
		MIN(case when seq_no = 0 then ad_media_id end) as ad_media_id
	FROM
		ad_media
	GROUP BY
		ad_id
	) t
WHERE
	da.ad_id = t.ad_id;

UPDATE
	dashboard_ads da
SET
	image_count = t.count
FROM (
	SELECT
		ads.ad_id,
		count(*) as count
	FROM
		ad_image_changes aic
		INNER JOIN ad_actions aac ON (aic.ad_id = aac.ad_id and aic.action_id = aac.action_id)
		INNER JOIN ads ON (ads.ad_id = aic.ad_id)
	WHERE
		aac.action_id = (SELECT MAX(action_id) FROM ad_actions WHERE ad_id = ads.ad_id)
	AND aac.action_type = 'editrefused' AND NOT (ads.status = 'disabled' and aac.state = 'accepted')
	AND aic.name IS NOT NULL
	AND aic.name != ''
	GROUP BY
		ads.ad_id, aic.action_id
	) t
WHERE
	da.ad_id = t.ad_id;

-- other ad changes
UPDATE
	dashboard_ads da
SET
	subject = t.subject,
	category = t.category,
	price = t.price
FROM (
	SELECT
		ads.ad_id,
		COALESCE(ac1.new_value, ads.subject) as subject,
		COALESCE(ac2.new_value::integer, ads.category) as category,
		COALESCE(ac3.new_value::integer, ads.price) as price
	FROM
		ads
		INNER JOIN ad_actions aac USING (ad_id)
		LEFT JOIN ad_changes ac1 ON (ac1.ad_id = aac.ad_id and ac1.action_id = aac.action_id and ac1.state_id = aac.current_state and ac1.column_name = 'subject')
		LEFT JOIN ad_changes ac2 ON (ac2.ad_id = aac.ad_id and ac2.action_id = aac.action_id and ac2.state_id = aac.current_state and ac2.column_name = 'category')
		LEFT JOIN ad_changes ac3 ON (ac3.ad_id = aac.ad_id and ac3.action_id = aac.action_id and ac3.state_id = aac.current_state and ac3.column_name = 'price')
	WHERE
		aac.action_id = (SELECT MAX(action_id) FROM ad_actions WHERE ad_id = ads.ad_id)
	AND aac.action_type = 'editrefused' AND NOT (ads.status = 'disabled' and aac.state = 'accepted')
	) t
WHERE
	da.ad_id = t.ad_id;


-- NGA ad params
UPDATE
	dashboard_ads da
SET
	ad_params_list = t.list
FROM (
	SELECT
		ad_id,
		STRING_AGG(name || ',' || REPLACE(value, E'\n', '<br>'), '|&|') list
	FROM
		ad_params
	GROUP BY
		ad_id
	) t
WHERE
	da.ad_id = t.ad_id;

-- last_change
UPDATE
	dashboard_ads da
SET
	last_change = t.timestamp
FROM (
	SELECT
		aa.ad_id as ad_id,
		timestamp
	FROM
		ad_actions aa
		JOIN action_states on (state_id = current_state)
	WHERE
		aa.action_id = (
			SELECT
				MAX(action_id)
			FROM
				ad_actions
			WHERE
				ad_id = aa.ad_id
			AND 
				action_type IN (
					'new', 'edit', 'renew', 'delete', 'editrefused', 'import', 'adminedit',
					'undo_delete', 'post_refusal', 'bump', 'status_change', 'scarface_edit',
					'move_to_category', 'deactivate', 'activate', 'disable'
				)
			AND
				state IN ('accepted', 'pending_review', 'locked')
		)
	) t
WHERE
	da.ad_id = t.ad_id;
