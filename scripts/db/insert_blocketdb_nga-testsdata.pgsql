INSERT INTO admins (admin_id, username, passwd, fullname, email, mobile, status) SELECT 9999, 'dev', 'da39a3ee5e6b4b0d3255bfef95601890afd80709', 'dev', 'dev@dev.com', '0709-6459256' , 'active'  WHERE NOT EXISTS (SELECT 1 FROM admins WHERE admin_id = 9999);
INSERT INTO admin_privs(admin_id, priv_name) VALUES (9999, 'adqueue');
INSERT INTO block_rules(rule_id, rule_name, action, application, combine_rule, count_yesterday, count_today, count_date, target) VALUES (13, 'Forbidden buy in ads', 'stop', 'newad', 'and', 0, 0, '2011-08-29', NULL);
INSERT INTO block_lists(list_id, list_name, base_list, list_type, ignore) VALUES (13, 'Forbidden buy in ads', false, 'general', NULL);
INSERT INTO blocked_items (list_id, value) VALUES (13, 'buy');
INSERT INTO block_rule_conditions(condition_id, rule_id, list_id, whole_word, fields) VALUES(13, 13, 13, 0, '{subject}');
SELECT generate_ad_codes('pay', 144) FROM (VALUES (1),(2),(3),(4),(5),(6),(7),(8),(9),(10),(11),(12)) AS a CROSS JOIN (VALUES (1),(2),(3),(4),(5),(6),(7),(8),(9),(10),(11),(12)) AS b;
SELECT generate_ad_codes('verify', 144) FROM (VALUES (1),(2),(3),(4),(5),(6),(7),(8),(9),(10),(11),(12)) AS a CROSS JOIN (VALUES (1),(2),(3),(4),(5),(6),(7),(8),(9),(10),(11),(12)) AS b;
