#!/usr/bin/perl
#
# pgsqlschemacollator
#
#
# Purpose:
#
#   Allow us to sanely compare pgsql schema dumps even when statements move around, which confuse diff.
#
#   Given a pgsql schema dump on stdin or in a file on the cmdline, output a collated, sorted output on stdout.
#   Ny collated we mean that multiple related lines have been collected into one.
#
# Usage:
#   pgsqlschemacollator filename | <filename
#
use strict;

sub line_collate
{
	my $filename = shift;
	my @lines = ();
	my $workline = "";

	if( $filename ) {
		open(FILE, "<", $filename) or die("Couldn't open file '.$filename.'");
	} else {
		open(FILE, "<&=0");
	}

	while( my $line = <FILE> ) {

		chomp $line;

		if( $line eq ");" || $line =~ m/^\s+\S+/ ) # Collate indented and clause terminating lines.
		{
			$workline .= $line;
		} else { # Save the collated line
			push(@lines, $workline."\n") if $workline ne "";
			$workline = $line;
		}

	}
	# We mustn't forget to flush the workline buffer:
	push(@lines, $workline."\n") if $workline ne "";

	print sort @lines;
	close(FILE) if $filename;
	return 0;
}

exit line_collate($ARGV[0]);

