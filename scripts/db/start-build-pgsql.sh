#!/bin/bash

if [ -n "$TOPDIR" ] ; then
   PGDIR=$TOPDIR/pgsql
else
   PGDIR=pgsql
fi
export PGDIR

if [ -z "$existing_db" ]; then
	test -z "$1" && exit 1
	test "${1%data*}" = "$1" && exit 1
	rm -rf $1/* 
	mkdir $1 >/dev/null 2>&1
	cd $1 || exit 1
	PGDATA=`pwd`
	cd -
	export PGDATA
else
	cd $PGDATA || exit 1
	PGDIR=`pwd`
	cd -
fi

PGVERSION=$2

#TZ=`date +%Z`
TIMEZONE=`date +%Z`
if [ "$TIMEZONE" = "CLT" ] || [ "$TIMEZONE" = "CLST" ] ; then
	TZ=America/Santiago
	export TZ
else
	TZ=`date +%Z`
	export TZ
fi

PGBINROOT=/usr/bin
if [ -d /usr/pgsql-${PGVERSION}/bin ];
then
	PGBINROOT=/usr/pgsql-${PGVERSION}/bin
fi

PSQL=$PGBINROOT/psql

if [ -z "$existing_db" ]; then
	echo "Installing modules"
	env TOPDIR="$(cd $TOPDIR ; pwd)" make -C ${TOPDIR}/modules depend install BUILD_STAGE=REGRESS INSTALLROOT=${TOPDIR}/regress_final PSQL_ONLY=1

	echo "Starting Postgresql ($PGVERSION)"
	echo "PGDATA $PGDATA"
	echo "$PGBINROOT/initdb --pgdata=$PGDATA --auth='ident'"
	if [ "$PGVERSION" = "8.4" -o $(expr "$PGVERSION" ">=" 9) = 1 ] ;  then
	    $PGBINROOT/initdb --pgdata=$PGDATA --auth="ident"
	else
	    $PGBINROOT/initdb --pgdata=$PGDATA --auth="ident sameuser"
	fi
	echo "$PGDIR/postgresql.conf"
	cat $PGDIR/postgresql.conf >> $PGDATA/postgresql.conf
#	cat ${TOPDIR}/build/modules/psql_*/*.pgconfig 2> /dev/null >> $PGDATA/postgresql.conf
	
	POSTMASTER=$PGBINROOT/postmaster

	echo " RUNNING: $POSTMASTER -h '' -k $PGDATA -D $PGDATA 1> $PGDIR/pgsql.log"
	$POSTMASTER -h '' -k $PGDATA -D $PGDATA 1> $PGDIR/pgsql.log < /dev/null 2>&1 &
	echo $! > $PGDIR/postmaster.pid

	DSNARGS="-h $PGDATA"
	while ! psql $DSNARGS -c "select current_timestamp" template1 > /dev/null 2>&1; do
	/bin/sleep 1
	echo -n "."
	done
fi

$PGBINROOT/createdb $DSNARGS blocketdb

echo "Connect with psql $DSNARGS blocketdb or export PGHOST=$PGDATA PGDATABASE=blocketdb"

rm -f .psql.stderr

echo "Adding module procs"
echo "Schema $BPVSCHEMA"
cat ${TOPDIR}/build/modules/psql_*/*.sql | env PGOPTIONS="-c search_path=$BPVSCHEMA,public" $PSQL $DSNARGS blocketdb > /dev/null 2>> .psql.stderr
$PSQL $DSNARGS blocketdb < extensions.pgsql > /dev/null 2>> .psql.stderr
cat ${TOPDIR}/ninja_build/regress/modules/*.sql | env PGOPTIONS="-c search_path=$BPVSCHEMA,public" $PSQL $DSNARGS blocketdb > /dev/null 2>> .psql.stderr

echo "Creating schemas"
$PSQL $DSNARGS blocketdb < create_blocketdb.pgsql > /dev/null 2>> .psql.stderr
echo "COPY streets FROM '${TOPDIR}/scripts/db/streets.csv' DELIMITER ';' CSV;" | $PSQL $DSNARGS blocketdb > /dev/null 2>> .psql.stderr

# Create schema and procedures
$PSQL $DSNARGS blocketdb < ${TOPDIR}/ninja_build/regress/bin/create_proschema.pgsql > /dev/null 2>> .psql.stderr

# Create statistics tables
$PSQL $DSNARGS blocketdb < create_blocketdb_stats1x1.pgsql > /dev/null 2>> .psql.stderr

if [ -n "$DBPATCH" ]; then
       echo "$DBPATCH" | $PSQL $DSNARGS blocketdb > /dev/null 2>> .psql.stderr
fi

# create archives (dependent on enums)
for year in $(seq 2009 $(($(date +%Y) +1)))
do
	(echo "create schema blocket_$year; set search_path=blocket_$year;" && cat create_blocketdb_archive.pgsql) | $PSQL $DSNARGS blocketdb > /dev/null 2>> .psql.stderr
done

echo "Adding views"
# create views (dependent on archives)
for view in views/*.sql
do
	echo "FILE $view" >> .psql.stderr
	env PGOPTIONS="-c search_path=$BPVSCHEMA,public" $PSQL $DSNARGS blocketdb < $view > /dev/null 2>> .psql.stderr
done

echo "Adding plsqlprocs"
# plsqlprocs
for proc in plsql/*.sql
do
	echo "FILE $proc" >> .psql.stderr
	env PGOPTIONS="-c search_path=$BPVSCHEMA,public" $PSQL $DSNARGS blocketdb < $proc > /dev/null 2>> .psql.stderr
done
#plsqlprocs platform
for proc in ${TOPDIR}/platform/scripts/db/plsql/*.sql
do
	echo "FILE $proc" >> .psql.stderr
	env PGOPTIONS="-c search_path=$BPVSCHEMA,public" $PSQL $DSNARGS blocketdb < $proc > /dev/null 2>> .psql.stderr
done

echo "Creating triggers"
for trig in triggers/*.sql
do
	echo "FILE $trig" >> .psql.stderr
	echo "trigger: $trig"
	env PGOPTIONS="-c search_path=$BPVSCHEMA,public" $PSQL $DSNARGS blocketdb < $trig > /dev/null 2>> .psql.stderr
done

echo "Inserting base data"
env PGOPTIONS="-c search_path=$BPVSCHEMA,public" $PSQL $DSNARGS blocketdb < insert_blocketdb_sitedata.pgsql > /dev/null 2>> .psql.stderr

$PSQL $DSNARGS postgres -c "CREATE DATABASE blocketdb_pristine TEMPLATE blocketdb;" > /dev/null 2>> .psql.stderr

if [ -z "$existing_db" ]; then
    env PGOPTIONS="-c search_path=$BPVSCHEMA,public" $PSQL $DSNARGS blocketdb < insert_blocketdb_testdata.pgsql > /dev/null 2>> .psql.stderr
fi

if grep -C1 '^ERROR:' .psql.stderr ; then
    exit 1
fi
rm -f .psql.stderr
