SELECT pg_catalog.setval('action_states_state_id_seq', 240, true);
SELECT pg_catalog.setval('ads_ad_id_seq', 24, true);
SELECT pg_catalog.setval('list_id_seq', 8000004, true);
SELECT pg_catalog.setval('pay_code_seq', 64, true);
SELECT pg_catalog.setval('pay_log_pay_log_id_seq', 16, true);
SELECT pg_catalog.setval('payment_groups_payment_group_id_seq', 67, true);
SELECT pg_catalog.setval('tokens_token_id_seq', 74, true);
SELECT pg_catalog.setval('trans_queue_id_seq', 6, true);
SELECT pg_catalog.setval('users_user_id_seq', 50, true);
INSERT INTO admins (admin_id, username, passwd, fullname, jabbername, email, mobile, status) VALUES (9, 'marcos', 'dfadc855249b015fd2bb015c0b099b2189c58748', NULL, NULL, 'marcos@mazaru.schibsted.cl', NULL, 'active');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'ais');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'notice_abuse');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'adqueue');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'adminad');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'Adminuf');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'admin');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'api');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'config');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'filter');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'landing_page');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'mama');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'popular_ads');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'scarface');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'search');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'Websql');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'adqueue.accepted');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'adqueue.admin_settings');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'adqueue.show_num_ads=2');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'adqueue.approve_refused');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'adqueue.video');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'adqueue.mama_queues');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'adqueue.mamawork_queues');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'adqueue.refusals');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'adqueue.admin_queue');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'adminad.bump');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'adminad.clear_ad');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'adminad.edit_ad');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'filter.lists');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'filter.rules');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'filter.spamfilter');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'mama.backup_lists');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'mama.show_lists');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'scarface.warning');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'search.abuse_report');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'search.uid_emails');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'search.maillog');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'search.mass_delete');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'search.paylog');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'search.reviewers');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'search.search_ads');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'search.undo_delete');
INSERT INTO users (user_id, uid, email, account, paid_total) VALUES (50, 3, 'moniacas@hell.cl', 0, 0);
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (20, 8000000, '2014-02-19 17:57:47', 'active', 'sell', 'Moniacas', '1231231', 5, 0, 5040, 50, NULL, false, false, false, 'Secador de pelo', 'Que te seca el pelo', 777999, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$|?#?=j`CU3NG|lUOe4332fb3726ffb80bc6afa05d0a52f1853ebea47', '2014-02-19 17:57:47.300556', 'es');
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (21, 8000001, '2014-02-19 17:57:52', 'active', 'sell', 'Moniacas', '1231231', 5, 0, 5020, 50, NULL, false, false, false, 'Otra Mesa', 'Me la rob� durante las protestas.', 123123, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$ppF''{^*>O^Ln}I.C3dc973d4a5afd1f11d67fde3c1f7eef7893d343c', '2014-02-19 17:57:52.267024', 'es');
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (23, 8000002, '2014-02-19 18:07:49', 'refused', 'sell', 'Moniacas', '1231231', 6, 0, 3060, 50, NULL, false, false, false, 'Telefono Tash', 'Con la media pantalla', 343453, NULL, NULL, NULL, NULL, NULL, NULL, '$1024${<&VZ};,{@|OA >k9fa76af8bb60c8d35479b724f529f88296b834e4', '2014-02-19 18:07:49.311382', 'es');
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (22, 8000003, '2014-02-19 18:10:01', 'active', 'sell', 'Moniacas', '1231231', 6, 0, 6120, 50, NULL, false, false, false, 'Nintendo Power', 'wololo ahora si', 555, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$4P9{<WHqVhpWXiZCb89ac06ab992899cce3246c9ddee385d8994f7e7', '2014-02-19 18:10:01.537652', 'es');
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (24, 8000004, '2014-02-19 18:13:49', 'active', 'sell', 'Moniacas', '1231231', 15, 0, 5040, 50, NULL, false, false, false, 'Tele pulenta', 'Tele de infinito + pulgadas y 7 dimensiones', 6677, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$;A@-`PgT7X,u#YPL28465af3af20f6ab946497673cdbf404ff37b7a8', '2014-02-19 18:13:49.094708', 'es');
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (60, '112200000', 'verified', '2014-02-19 17:52:37.101702', NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (61, '112200001', 'verified', '2014-02-19 17:53:25.895868', NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (62, '112200002', 'verified', '2014-02-19 17:54:29.382861', NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (63, '112200003', 'verified', '2014-02-19 17:55:33.448477', NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (64, '112200004', 'verified', '2014-02-19 17:59:39.284238', NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (65, '112200005', 'verified', '2014-02-19 18:03:47.575196', NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (66, '112200006', 'verified', '2014-02-19 18:13:38.901059', NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (67, '112200007', 'verified', '2014-02-19 18:17:23.266125', NULL);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (21, 2, 'edit', 236, 'locked', 'normal', 9, '2014-02-19 18:21:09.131202', 65);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (24, 2, 'half_time_mail', 237, 'reg', 'normal', NULL, NULL, NULL);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (24, 3, 'renew', 240, 'pending_review', 'autoaccept', NULL, NULL, 67);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (20, 1, 'new', 214, 'accepted', 'normal', 9, '2014-02-19 18:02:33.944179', 60);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (21, 1, 'new', 215, 'accepted', 'normal', 9, '2014-02-19 18:02:33.944179', 61);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (22, 1, 'new', 218, 'refused', 'normal', 9, '2014-02-19 18:03:00.275722', 62);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (23, 1, 'new', 225, 'accepted', 'normal', 9, '2014-02-19 18:03:00.275722', 63);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (23, 2, 'post_refusal', 226, 'refused', 'normal', NULL, NULL, NULL);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (22, 2, 'editrefused', 229, 'accepted', 'normal', 9, '2014-02-19 18:14:43.415441', 64);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (22, 3, 'half_time_mail', 230, 'reg', 'normal', NULL, NULL, NULL);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (24, 1, 'new', 235, 'accepted', 'normal', 9, '2014-02-19 18:18:45.183353', 66);
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (20, 1, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (20, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (21, 1, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (21, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (22, 1, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (22, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (23, 1, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (23, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (22, 2, 'prev_action_id', '1');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (22, 2, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (22, 2, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (21, 2, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (21, 2, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (24, 1, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (24, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (24, 3, 'source', 'half_time_mail');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (24, 3, 'redir', 'dW5rbm93bg==');
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (74, 'X530520271c7435f800000000a40fec500000000', 9, '2014-02-19 18:20:38.624699', '2014-02-19 19:20:38.624699', '10.0.1.138', 'Websql', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (1, 'X53051a5d6611bfc5000000003e850eff00000000', 9, '2014-02-19 17:55:57.446847', '2014-02-19 17:55:57.661184', '10.0.1.138', 'authenticate', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (2, 'X53051a5e65f269e7000000007353c2fd00000000', 9, '2014-02-19 17:55:57.661184', '2014-02-19 17:56:28.149213', '10.0.1.138', 'spamfilter', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (3, 'X53051a7c72171fcc0000000073bd3bf500000000', 9, '2014-02-19 17:56:28.149213', '2014-02-19 17:56:58.302527', '10.0.1.138', 'Websql', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (4, 'X53051a9a5b9de640000000005bc46d2a00000000', 9, '2014-02-19 17:56:58.302527', '2014-02-19 17:57:00.086531', '10.0.1.138', 'Websql', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (5, 'X53051a9c47925a39000000006e3942aa00000000', 9, '2014-02-19 17:57:00.086531', '2014-02-19 17:57:01.429471', '10.0.1.138', 'Websql', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (6, 'X53051a9d6b14c2da000000006230d5f800000000', 9, '2014-02-19 17:57:01.429471', '2014-02-19 17:57:05.473864', '10.0.1.138', 'Websql', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (7, 'X53051aa1609fcf50000000071b7da500000000', 9, '2014-02-19 17:57:05.473864', '2014-02-19 17:57:06.941945', '10.0.1.138', 'Websql', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (8, 'X53051aa3197efac0000000006c1b95c700000000', 9, '2014-02-19 17:57:06.941945', '2014-02-19 17:57:32.442829', '10.0.1.138', 'Websql', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (9, 'X53051abc47ce7f20000000003450ca6200000000', 9, '2014-02-19 17:57:32.442829', '2014-02-19 17:57:33.940751', '10.0.1.138', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (10, 'X53051abe7a95846f000000002267dd4900000000', 9, '2014-02-19 17:57:33.940751', '2014-02-19 17:57:34.027627', '10.0.1.138', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (11, 'X53051abe5a2eabae0000000035dd42ca00000000', 9, '2014-02-19 17:57:34.027627', '2014-02-19 17:57:34.034454', '10.0.1.138', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (12, 'X53051abe276d00a0000000003710689100000000', 9, '2014-02-19 17:57:34.034454', '2014-02-19 17:57:34.041633', '10.0.1.138', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (13, 'X53051abe596be5da000000006790f38400000000', 9, '2014-02-19 17:57:34.041633', '2014-02-19 17:57:34.060525', '10.0.1.138', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (14, 'X53051abe383252700000000030524d600000000', 9, '2014-02-19 17:57:34.060525', '2014-02-19 17:57:34.067698', '10.0.1.138', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (15, 'X53051abe4c7be31400000000468eb14f00000000', 9, '2014-02-19 17:57:34.067698', '2014-02-19 17:57:34.074596', '10.0.1.138', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (16, 'X53051abe352d47aa00000000328da2da00000000', 9, '2014-02-19 17:57:34.074596', '2014-02-19 17:57:47.297243', '10.0.1.138', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (17, 'X53051acb513c04e000000001b1fb19100000000', 9, '2014-02-19 17:57:47.297243', '2014-02-19 17:57:52.253223', '10.0.1.138', 'review', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (18, 'X53051ad043c0a6cb000000003947d5ed00000000', 9, '2014-02-19 17:57:52.253223', '2014-02-19 17:58:00.270598', '10.0.1.138', 'review', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (19, 'X53051ad825e165d700000000772ae01a00000000', 9, '2014-02-19 17:58:00.270598', '2014-02-19 17:58:00.352302', '10.0.1.138', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (20, 'X53051ad8edced860000000017f4c1700000000', 9, '2014-02-19 17:58:00.352302', '2014-02-19 17:58:00.359385', '10.0.1.138', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (21, 'X53051ad852ef4d4400000000566f47bf00000000', 9, '2014-02-19 17:58:00.359385', '2014-02-19 17:58:00.365781', '10.0.1.138', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (22, 'X53051ad86fb88ec1000000003e04101e00000000', 9, '2014-02-19 17:58:00.365781', '2014-02-19 17:58:00.387914', '10.0.1.138', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (23, 'X53051ad838a01db70000000075c28bb700000000', 9, '2014-02-19 17:58:00.387914', '2014-02-19 17:58:00.395656', '10.0.1.138', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (24, 'X53051ad8451f8dc300000000521f187700000000', 9, '2014-02-19 17:58:00.395656', '2014-02-19 17:58:00.403351', '10.0.1.138', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (25, 'X53051ad861de217e00000000cee0ce400000000', 9, '2014-02-19 17:58:00.403351', '2014-02-19 17:58:49.772789', '10.0.1.138', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (26, 'X53051b0a66fe2d9000000005c73a5ed00000000', 9, '2014-02-19 17:58:49.772789', '2014-02-19 17:58:53.886952', '10.0.1.138', 'review', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (27, 'X53051b0e2f55ea2d00000000609e8e8700000000', 9, '2014-02-19 17:58:53.886952', '2014-02-19 17:59:44.363258', '10.0.1.138', 'Websql', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (28, 'X53051b40382c4eb200000000486af58900000000', 9, '2014-02-19 17:59:44.363258', '2014-02-19 18:03:56.419682', '10.0.1.138', 'Websql', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (29, 'X53051c3c205eb793000000002827f44800000000', 9, '2014-02-19 18:03:56.419682', '2014-02-19 18:07:49.288572', '10.0.1.138', 'Websql', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (30, 'X53051d256bf714a9000000003002a67400000000', 9, '2014-02-19 18:07:49.288572', '2014-02-19 18:08:11.528831', '10.0.1.138', 'review', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (31, 'X53051d3c1f47e3bc00000000707f425b00000000', 9, '2014-02-19 18:08:11.528831', '2014-02-19 18:08:12.243214', '10.0.1.138', 'block_rule', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (32, 'X53051d3c5700708b0000000042b1af4000000000', 9, '2014-02-19 18:08:12.243214', '2014-02-19 18:08:19.335279', '10.0.1.138', 'listadmins', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (33, 'X53051d43495995aa00000000789525b700000000', 9, '2014-02-19 18:08:19.335279', '2014-02-19 18:08:19.346204', '10.0.1.138', 'listadmins', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (34, 'X53051d4321d29760000000002841d24a00000000', 9, '2014-02-19 18:08:19.346204', '2014-02-19 18:08:27.315347', '10.0.1.138', 'search_ads', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (35, 'X53051d4b73f68f8400000000538a008a00000000', 9, '2014-02-19 18:08:27.315347', '2014-02-19 18:08:27.39675', '10.0.1.138', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (36, 'X53051d4b65a9d442000000005d6734d800000000', 9, '2014-02-19 18:08:27.39675', '2014-02-19 18:08:27.404225', '10.0.1.138', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (37, 'X53051d4b241818700000000033b21b4100000000', 9, '2014-02-19 18:08:27.404225', '2014-02-19 18:08:27.411004', '10.0.1.138', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (38, 'X53051d4b124074d2000000004d0cc1aa00000000', 9, '2014-02-19 18:08:27.411004', '2014-02-19 18:09:03.901411', '10.0.1.138', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (39, 'X53051d705103a34100000000568f418400000000', 9, '2014-02-19 18:09:03.901411', '2014-02-19 18:09:08.715063', '10.0.1.138', 'review', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (40, 'X53051d756b8f23d00000000020e2a4c800000000', 9, '2014-02-19 18:09:08.715063', '2014-02-19 18:09:43.412654', '10.0.1.138', 'Websql', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (41, 'X53051d97218039d90000000030b6d71a00000000', 9, '2014-02-19 18:09:43.412654', '2014-02-19 18:09:43.471316', '10.0.1.138', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (42, 'X53051d976c967ea800000000209fd41700000000', 9, '2014-02-19 18:09:43.471316', '2014-02-19 18:09:43.477438', '10.0.1.138', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (43, 'X53051d97f58da03000000005cc2082300000000', 9, '2014-02-19 18:09:43.477438', '2014-02-19 18:09:43.484203', '10.0.1.138', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (44, 'X53051d977356d2340000000057ad9d9400000000', 9, '2014-02-19 18:09:43.484203', '2014-02-19 18:09:43.500064', '10.0.1.138', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (45, 'X53051d98639de92c000000007228740100000000', 9, '2014-02-19 18:09:43.500064', '2014-02-19 18:09:43.505332', '10.0.1.138', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (46, 'X53051d9841fa0d38000000005e2558500000000', 9, '2014-02-19 18:09:43.505332', '2014-02-19 18:09:43.510879', '10.0.1.138', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (47, 'X53051d98287e59160000000076477caa00000000', 9, '2014-02-19 18:09:43.510879', '2014-02-19 18:10:01.533543', '10.0.1.138', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (48, 'X53051daa2c6eae2000000004004ace00000000', 9, '2014-02-19 18:10:01.533543', '2014-02-19 18:13:43.806008', '10.0.1.138', 'review', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (49, 'X53051e886ec0710300000000a8ff5e200000000', 9, '2014-02-19 18:13:43.806008', '2014-02-19 18:13:45.179156', '10.0.1.138', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (50, 'X53051e89bcd77dc0000000070f4841b00000000', 9, '2014-02-19 18:13:45.179156', '2014-02-19 18:13:45.240435', '10.0.1.138', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (73, 'X53051f486cf061d5000000002955f7f300000000', 9, '2014-02-19 18:16:56.317926', '2014-02-19 18:20:38.624699', '10.0.1.138', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (51, 'X53051e89466a7cdc000000004cd967bb00000000', 9, '2014-02-19 18:13:45.240435', '2014-02-19 18:13:45.246802', '10.0.1.138', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (52, 'X53051e892a9148120000000069a8929500000000', 9, '2014-02-19 18:13:45.246802', '2014-02-19 18:13:45.254156', '10.0.1.138', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (53, 'X53051e894ff87b2500000000687e7c2f00000000', 9, '2014-02-19 18:13:45.254156', '2014-02-19 18:13:49.089795', '10.0.1.138', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (54, 'X53051e8d167a5401000000003aa859b100000000', 9, '2014-02-19 18:13:49.089795', '2014-02-19 18:13:51.610559', '10.0.1.138', 'review', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (55, 'X53051e902365bcb9000000003a9ce5cc00000000', 9, '2014-02-19 18:13:51.610559', '2014-02-19 18:13:52.188553', '10.0.1.138', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (56, 'X53051e907807023200000000647d13600000000', 9, '2014-02-19 18:13:52.188553', '2014-02-19 18:13:55.793896', '10.0.1.138', 'Websql', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (57, 'X53051e9472d6f8f2000000005809e19800000000', 9, '2014-02-19 18:13:55.793896', '2014-02-19 18:16:06.334395', '10.0.1.138', 'Websql', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (58, 'X53051f164dc19dc30000000071f26e5d00000000', 9, '2014-02-19 18:16:06.334395', '2014-02-19 18:16:09.126724', '10.0.1.138', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (59, 'X53051f19208db4c2000000006708995900000000', 9, '2014-02-19 18:16:09.126724', '2014-02-19 18:16:09.197301', '10.0.1.138', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (60, 'X53051f19472c68ff000000007fdfcc0600000000', 9, '2014-02-19 18:16:09.197301', '2014-02-19 18:16:09.205184', '10.0.1.138', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (61, 'X53051f19339ddf8800000000df05f3400000000', 9, '2014-02-19 18:16:09.205184', '2014-02-19 18:16:09.212336', '10.0.1.138', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (62, 'X53051f194ab73aef000000007912e73700000000', 9, '2014-02-19 18:16:09.212336', '2014-02-19 18:16:21.001879', '10.0.1.138', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (63, 'X53051f254922915200000000386bae3400000000', 9, '2014-02-19 18:16:21.001879', '2014-02-19 18:16:27.73409', '10.0.1.138', 'listadmins', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (64, 'X53051f2c4fd5b97300000000d84f28700000000', 9, '2014-02-19 18:16:27.73409', '2014-02-19 18:16:27.748223', '10.0.1.138', 'listadmins', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (65, 'X53051f2c1717cec4000000009b2e85000000000', 9, '2014-02-19 18:16:27.748223', '2014-02-19 18:16:45.524997', '10.0.1.138', 'search_ads', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (66, 'X53051f3e6a9cc655000000007674c80300000000', 9, '2014-02-19 18:16:45.524997', '2014-02-19 18:16:45.547556', '10.0.1.138', 'listadmins', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (67, 'X53051f3e6b4d417d0000000026cc8beb00000000', 9, '2014-02-19 18:16:45.547556', '2014-02-19 18:16:50.435579', '10.0.1.138', 'search_ads', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (68, 'X53051f424f3f9a740000000034289d5a00000000', 9, '2014-02-19 18:16:50.435579', '2014-02-19 18:16:50.461665', '10.0.1.138', 'listadmins', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (69, 'X53051f4220bac159000000003d277f1500000000', 9, '2014-02-19 18:16:50.461665', '2014-02-19 18:16:55.43859', '10.0.1.138', 'search_ads', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (70, 'X53051f47657fb12c0000000029dacb5a00000000', 9, '2014-02-19 18:16:55.43859', '2014-02-19 18:16:56.299065', '10.0.1.138', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (71, 'X53051f48e710ca5000000005b9459cd00000000', 9, '2014-02-19 18:16:56.299065', '2014-02-19 18:16:56.309371', '10.0.1.138', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (72, 'X53051f48f8aa28900000000b80f32a00000000', 9, '2014-02-19 18:16:56.309371', '2014-02-19 18:16:56.317926', '10.0.1.138', 'block_list', NULL, NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (20, 1, 200, 'reg', 'initial', '2014-02-19 17:52:37.101702', '10.0.1.138', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (20, 1, 201, 'unverified', 'verifymail', '2014-02-19 17:52:37.101702', '10.0.1.138', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (20, 1, 202, 'pending_review', 'verify', '2014-02-19 17:52:37.164438', '10.0.1.138', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (21, 1, 203, 'reg', 'initial', '2014-02-19 17:53:25.895868', '10.0.1.138', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (21, 1, 204, 'unverified', 'verifymail', '2014-02-19 17:53:25.895868', '10.0.1.138', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (21, 1, 205, 'pending_review', 'verify', '2014-02-19 17:53:25.934876', '10.0.1.138', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (22, 1, 206, 'reg', 'initial', '2014-02-19 17:54:29.382861', '10.0.1.138', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (22, 1, 207, 'unverified', 'verifymail', '2014-02-19 17:54:29.382861', '10.0.1.138', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (22, 1, 208, 'pending_review', 'verify', '2014-02-19 17:54:29.445418', '10.0.1.138', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (23, 1, 209, 'reg', 'initial', '2014-02-19 17:55:33.448477', '10.0.1.138', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (23, 1, 210, 'unverified', 'verifymail', '2014-02-19 17:55:33.448477', '10.0.1.138', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (23, 1, 211, 'pending_review', 'verify', '2014-02-19 17:55:33.489493', '10.0.1.138', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (20, 1, 212, 'locked', 'checkout', '2014-02-19 17:57:33.944179', '10.0.1.138', 9);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (21, 1, 213, 'locked', 'checkout', '2014-02-19 17:57:33.944179', '10.0.1.138', 9);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (20, 1, 214, 'accepted', 'accept', '2014-02-19 17:57:47.300556', '10.0.1.138', 16);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (21, 1, 215, 'accepted', 'accept', '2014-02-19 17:57:52.267024', '10.0.1.138', 17);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (22, 1, 216, 'locked', 'checkout', '2014-02-19 17:58:00.275722', '10.0.1.138', 18);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (23, 1, 217, 'locked', 'checkout', '2014-02-19 17:58:00.275722', '10.0.1.138', 18);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (22, 1, 218, 'refused', 'refuse', '2014-02-19 17:58:49.775878', '10.0.1.138', 25);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (22, 2, 219, 'reg', 'initial', '2014-02-19 17:59:39.284238', '10.0.1.138', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (22, 2, 220, 'unverified', 'verifymail', '2014-02-19 17:59:39.284238', '10.0.1.138', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (22, 2, 221, 'pending_review', 'verify', '2014-02-19 17:59:39.311679', '10.0.1.138', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (21, 2, 222, 'reg', 'initial', '2014-02-19 18:03:47.575196', '10.0.1.138', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (21, 2, 223, 'unverified', 'verifymail', '2014-02-19 18:03:47.575196', '10.0.1.138', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (21, 2, 224, 'pending_review', 'verify', '2014-02-19 18:03:47.649802', '10.0.1.138', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (23, 1, 225, 'accepted', 'accept', '2014-02-19 18:07:49.311382', '10.0.1.138', 29);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (23, 2, 226, 'refused', 'refuse', '2014-02-19 18:09:03.905695', '10.0.1.138', 38);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (22, 2, 227, 'locked', 'checkout', '2014-02-19 18:09:43.415441', '10.0.1.138', 40);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (21, 2, 228, 'locked', 'checkout', '2014-02-19 18:09:43.415441', '10.0.1.138', 40);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (22, 2, 229, 'accepted', 'accept', '2014-02-19 18:10:01.537652', '10.0.1.138', 47);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (22, 3, 230, 'reg', 'initial', '2014-02-19 18:11:38.185664', '127.0.0.1', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (24, 1, 231, 'reg', 'initial', '2014-02-19 18:13:38.901059', '10.0.1.138', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (24, 1, 232, 'unverified', 'verifymail', '2014-02-19 18:13:38.901059', '10.0.1.138', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (24, 1, 233, 'pending_review', 'verify', '2014-02-19 18:13:38.958381', '10.0.1.138', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (24, 1, 234, 'locked', 'checkout', '2014-02-19 18:13:45.183353', '10.0.1.138', 49);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (24, 1, 235, 'accepted', 'accept', '2014-02-19 18:13:49.094708', '10.0.1.138', 53);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (21, 2, 236, 'locked', 'checkout', '2014-02-19 18:16:09.131202', '10.0.1.138', 58);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (24, 2, 237, 'reg', 'initial', '2014-02-19 18:17:11.858103', '127.0.0.1', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (24, 3, 238, 'reg', 'initial', '2014-02-19 18:17:23.266125', '10.0.1.138', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (24, 3, 239, 'unverified', 'verifymail', '2014-02-19 18:17:23.266125', '10.0.1.138', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (24, 3, 240, 'pending_review', 'verify', '2014-02-19 18:17:23.343336', '10.0.1.138', NULL);
INSERT INTO ad_changes (ad_id, action_id, state_id, is_param, column_name, old_value, new_value) VALUES (22, 2, 219, false, 'body', 'wololo', 'wololo ahora si');
INSERT INTO ad_changes (ad_id, action_id, state_id, is_param, column_name, old_value, new_value) VALUES (21, 2, 222, false, 'body', 'Me la rob� durante las protestas.', 'Me la rob� durante las protestas.
edited');
INSERT INTO ad_codes (code, code_type) VALUES (35311, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (67334, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (99357, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (41380, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (73403, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (15426, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (47449, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (79472, 'pay');
INSERT INTO ad_image_changes (ad_id, action_id, state_id, seq_no, name, is_new) VALUES (20, 1, 214, 0, NULL, false);
INSERT INTO ad_image_changes (ad_id, action_id, state_id, seq_no, name, is_new) VALUES (21, 1, 215, 0, NULL, false);
INSERT INTO ad_image_changes (ad_id, action_id, state_id, seq_no, name, is_new) VALUES (22, 2, 219, 0, NULL, false);
INSERT INTO ad_image_changes (ad_id, action_id, state_id, seq_no, name, is_new) VALUES (21, 2, 222, 0, NULL, false);
INSERT INTO ad_image_changes (ad_id, action_id, state_id, seq_no, name, is_new) VALUES (23, 1, 225, 0, NULL, false);
INSERT INTO ad_image_changes (ad_id, action_id, state_id, seq_no, name, is_new) VALUES (22, 2, 229, 0, NULL, false);
INSERT INTO ad_image_changes (ad_id, action_id, state_id, seq_no, name, is_new) VALUES (24, 1, 235, 0, NULL, false);
INSERT INTO ad_image_changes (ad_id, action_id, state_id, seq_no, name, is_new) VALUES (24, 3, 238, 0, NULL, false);
INSERT INTO ad_params (ad_id, name, value) VALUES (20, 'prev_currency', 'peso');
INSERT INTO ad_params (ad_id, name, value) VALUES (20, 'country', 'UNK');
INSERT INTO ad_params (ad_id, name, value) VALUES (21, 'prev_currency', 'peso');
INSERT INTO ad_params (ad_id, name, value) VALUES (21, 'country', 'UNK');
INSERT INTO ad_params (ad_id, name, value) VALUES (22, 'prev_currency', 'peso');
INSERT INTO ad_params (ad_id, name, value) VALUES (22, 'country', 'UNK');
INSERT INTO ad_params (ad_id, name, value) VALUES (23, 'prev_currency', 'peso');
INSERT INTO ad_params (ad_id, name, value) VALUES (23, 'internal_memory', '666');
INSERT INTO ad_params (ad_id, name, value) VALUES (23, 'country', 'UNK');
INSERT INTO ad_params (ad_id, name, value) VALUES (24, 'prev_currency', 'peso');
INSERT INTO ad_params (ad_id, name, value) VALUES (24, 'country', 'UNK');
INSERT INTO ad_queues (ad_id, action_id, queue, queued_at, locked_by, locked_until) VALUES (21, 2, 'normal', '2014-02-19 18:03:47.649802', 9, '2014-02-19 18:21:09.131202');
INSERT INTO ad_queues (ad_id, action_id, queue, queued_at, locked_by, locked_until) VALUES (24, 3, 'autoaccept', '2014-02-19 18:17:23.343336', NULL, NULL);
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (1, 'save', 0, '112200000', NULL, '2014-02-19 17:52:37', 60, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (2, 'verify', 0, '112200000', NULL, '2014-02-19 17:52:37', 60, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (3, 'save', 0, '112200001', NULL, '2014-02-19 17:53:26', 61, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (4, 'verify', 0, '112200001', NULL, '2014-02-19 17:53:26', 61, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (5, 'save', 0, '112200002', NULL, '2014-02-19 17:54:29', 62, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (6, 'verify', 0, '112200002', NULL, '2014-02-19 17:54:29', 62, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (7, 'save', 0, '112200003', NULL, '2014-02-19 17:55:33', 63, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (8, 'verify', 0, '112200003', NULL, '2014-02-19 17:55:33', 63, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (9, 'save', 0, '112200004', NULL, '2014-02-19 17:59:39', 64, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (10, 'verify', 0, '112200004', NULL, '2014-02-19 17:59:39', 64, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (11, 'save', 0, '112200005', NULL, '2014-02-19 18:03:48', 65, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (12, 'verify', 0, '112200005', NULL, '2014-02-19 18:03:48', 65, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (13, 'save', 0, '112200006', NULL, '2014-02-19 18:13:39', 66, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (14, 'verify', 0, '112200006', NULL, '2014-02-19 18:13:39', 66, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (15, 'save', 0, '112200007', NULL, '2014-02-19 18:17:23', 67, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (16, 'verify', 0, '112200007', NULL, '2014-02-19 18:17:23', 67, 'OK');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (1, 0, 'adphone', '1231231');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (1, 1, 'remote_addr', '10.0.1.138');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (2, 0, 'remote_addr', '10.0.1.138');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (3, 0, 'adphone', '1231231');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (3, 1, 'remote_addr', '10.0.1.138');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (4, 0, 'remote_addr', '10.0.1.138');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (5, 0, 'adphone', '1231231');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (5, 1, 'remote_addr', '10.0.1.138');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (6, 0, 'remote_addr', '10.0.1.138');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (7, 0, 'adphone', '1231231');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (7, 1, 'remote_addr', '10.0.1.138');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (8, 0, 'remote_addr', '10.0.1.138');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (9, 0, 'adphone', '1231231');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (9, 1, 'remote_addr', '10.0.1.138');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (10, 0, 'remote_addr', '10.0.1.138');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (11, 0, 'adphone', '1231231');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (11, 1, 'remote_addr', '10.0.1.138');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (12, 0, 'remote_addr', '10.0.1.138');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (13, 0, 'adphone', '1231231');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (13, 1, 'remote_addr', '10.0.1.138');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (14, 0, 'remote_addr', '10.0.1.138');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (15, 0, 'adphone', '1231231');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (15, 1, 'remote_addr', '10.0.1.138');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (16, 0, 'remote_addr', '10.0.1.138');
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (60, 'ad_action', 0, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (61, 'ad_action', 0, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (62, 'ad_action', 0, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (63, 'ad_action', 0, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (64, 'ad_action', 0, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (65, 'ad_action', 0, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (66, 'ad_action', 0, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (67, 'ad_action', 0, 0);
INSERT INTO review_log (ad_id, action_id, admin_id, review_time, queue, action_type, action, refusal_reason_text, category) VALUES (20, 1, 9, '2014-02-19 17:57:47.300556', 'normal', 'new', 'accepted', NULL, 5040);
INSERT INTO review_log (ad_id, action_id, admin_id, review_time, queue, action_type, action, refusal_reason_text, category) VALUES (21, 1, 9, '2014-02-19 17:57:52.267024', 'normal', 'new', 'accepted', NULL, 5020);
INSERT INTO review_log (ad_id, action_id, admin_id, review_time, queue, action_type, action, refusal_reason_text, category) VALUES (22, 1, 9, '2014-02-19 17:58:49.775878', 'normal', 'new', 'refused', 'Imagen obscena', 6120);
INSERT INTO review_log (ad_id, action_id, admin_id, review_time, queue, action_type, action, refusal_reason_text, category) VALUES (23, 1, 9, '2014-02-19 18:07:49.311382', 'normal', 'new', 'accepted', NULL, 3060);
INSERT INTO review_log (ad_id, action_id, admin_id, review_time, queue, action_type, action, refusal_reason_text, category) VALUES (23, 1, 9, '2014-02-19 18:09:03.905695', 'all', 'new', 'refused', 'Virus', 3060);
INSERT INTO review_log (ad_id, action_id, admin_id, review_time, queue, action_type, action, refusal_reason_text, category) VALUES (22, 2, 9, '2014-02-19 18:10:01.537652', 'normal', 'editrefused', 'accepted', NULL, 6120);
INSERT INTO review_log (ad_id, action_id, admin_id, review_time, queue, action_type, action, refusal_reason_text, category) VALUES (24, 1, 9, '2014-02-19 18:13:49.094708', 'normal', 'new', 'accepted', NULL, 5040);
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (20, 1, 214, 'filter_name', 'normal');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (21, 1, 215, 'filter_name', 'normal');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (22, 1, 218, 'reason', '19');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (22, 1, 218, 'filter_name', 'normal');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (23, 1, 225, 'filter_name', 'normal');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (23, 2, 226, 'reason', '26');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (23, 2, 226, 'filter_name', 'all');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (22, 2, 229, 'filter_name', 'normal');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (24, 1, 235, 'filter_name', 'normal');
INSERT INTO stats_daily (stat_time, name, value) VALUES ('2014-02-19', 'websql_query_75', '13');
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (1, '2014-02-19 17:57:47.325563', '27906@mazaru.schibsted.cl', NULL, NULL, NULL, NULL, NULL, NULL, 'cmd:admail
commit:1
ad_id:20
action_id:1
mail_type:accept
', NULL, 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (2, '2014-02-19 17:57:52.296028', '27906@mazaru.schibsted.cl', NULL, NULL, NULL, NULL, NULL, NULL, 'cmd:admail
commit:1
ad_id:21
action_id:1
mail_type:accept
', NULL, 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (3, '2014-02-19 18:07:49.363151', '27906@mazaru.schibsted.cl', NULL, NULL, NULL, NULL, NULL, NULL, 'cmd:admail
commit:1
ad_id:23
action_id:1
mail_type:accept
', NULL, 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (4, '2014-02-19 18:10:01.551244', '27906@mazaru.schibsted.cl', NULL, NULL, NULL, NULL, NULL, NULL, 'cmd:admail
commit:1
ad_id:22
action_id:2
mail_type:edit_accept
', NULL, 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (5, '2014-02-19 18:13:49.125178', '27906@mazaru.schibsted.cl', NULL, NULL, NULL, NULL, NULL, NULL, 'cmd:admail
commit:1
ad_id:24
action_id:1
mail_type:accept
', NULL, 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (6, '2014-02-19 18:17:23.412696', '27906@mazaru.schibsted.cl', '2014-02-19 18:37:23.412696', NULL, NULL, NULL, NULL, NULL, 'cmd:review
commit:1
ad_id:24
action_id:3
action:accept
filter_name:autoaccept
remote_addr:10.0.1.138

', NULL, 'AT', 'review', '24');
INSERT INTO user_params (user_id, name, value) VALUES (50, 'first_approved_ad', '2014-02-19 17:57:47.300556-03');
INSERT INTO user_params (user_id, name, value) VALUES (50, 'first_inserted_ad', '2014-02-19 17:57:47.300556-03');
