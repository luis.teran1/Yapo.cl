#!/bin/bash

if [ -n "$TOPDIR" ] ; then
   PGDIR=$TOPDIR/pgsql
else
   PGDIR=pgsql
fi
export PGDIR

test -z "$1" && exit 1
test ${1%data*} = $1 && exit 1
cd $1 || exit 1
export PGDATA=`pwd`
cd -

while kill -INT $(<$PGDIR/postmaster.pid) 2>/dev/null; do echo "stopping postmaster"; sleep 1; done

rm -rf $PGDATA
rm -f $PGDIR/.s.PGSQL.*
rm -f $PGDIR/pgsql.log
rm -f $PGDIR/postmaster.pid
