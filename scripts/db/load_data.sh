#!/bin/bash

test -z "$1" && exit 1
test -z "$2" && exit 1
PGDIR=`cd $1; pwd`
# Load the example data
psql -e --variable ON_ERROR_STOP=yes -h $PGDIR blocketdb < insert_blocketdb_${2}data.pgsql >.sql.log 2>&1

# Check result
res=$?

# Dump result in case of error
if [ $res != 0 ] ; then
  cat .sql.log
  echo Error in file insert_blocketdb_${2}data.pgsql
  exit $res
fi
