#!/usr/bin/perl -w

my $dollardollaroptional = shift;

while (<>) {
	/CREATE OR REPLACE VIEW/ and do {
		s/.*CREATE OR REPLACE/DROP/;
		s/ AS.*$/;/;
		print;
	};
	/CREATE RULE / and do {
		s/.*CREATE /DROP /;
		s/ AS ON .* TO/ ON /;
		s/$/;/;
		print;
	};
	/CREATE OR REPLACE FUNC/ and $print = 1;
	s/.*CREATE OR REPLACE/DROP/;
	if ($dollardollaroptional) {
		s/(RETURNS .*)* AS( \$\$)?/ CASCADE;/ and print and $print = 0;
	} else {
		s/(RETURNS .*)* AS \$\$/ CASCADE;/ and print and $print = 0;
	}
	$print and print;
}
