INSERT INTO users (user_id, uid, email, account, paid_total) VALUES (59, 5, 'with_account_and_no_commune@maps.cl', 0, 0);
INSERT INTO users (user_id, uid, email, account, paid_total) VALUES (60, 5, 'with_account_and_commune@maps.cl', 0, 0);
INSERT INTO users (user_id, uid, email, account, paid_total) VALUES (61, 5, 'without_account@maps.cl', 0, 0);
INSERT INTO users (user_id, uid, email, account, paid_total) VALUES (62, 5, 'without_account_2@maps.cl', 0, 0);

INSERT INTO accounts (account_id, name, rut, email, phone, is_company, region, salted_passwd, creation_date, activation_date, status, address, contact, lob, commune, user_id) VALUES (8, ' with_account_and_no_commune', '19565635-5', 'with_account_and_no_commune@maps.cl', '62184762', true, 1, '$1024$FzcbB[j!;t(sMn(yaebb74b99fa983dd2008e0c941bf65b375b5459d', '2015-05-28 09:06:07.499209', NULL, 'active', NULL, NULL, NULL, NULL, 59);
INSERT INTO accounts (account_id, name, rut, email, phone, is_company, region, salted_passwd, creation_date, activation_date, status, address, contact, lob, commune, user_id) VALUES (9, 'with_account_and_commune', NULL, 'with_account_and_commune@maps.cl', '962184762', false, 6, '$1024$`]''PZ;eo&G3$3$K%9fcdad5b647664c099e3af06ad387279f0cd2d39', '2015-05-28 09:19:37.97391', NULL, 'active', 'mi direccion en zapallar', NULL, NULL, 82, 60);

SELECT pg_catalog.setval('accounts_account_id_seq', 9, true);

INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (126, 8000085, '2015-05-28 09:51:04', 'active', 'sell', 'with_account_and_commune', '962184762', 6, 0, 5020, 60, NULL, false, false, false, 'Ad whitout maps and whitout commune', 'ad whitout maps and whitout commune body', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$`]''PZ;eo&G3$3$K%9fcdad5b647664c099e3af06ad387279f0cd2d39', '2015-05-28 09:51:04.58617', 'es');
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (127, 8000086, '2015-05-28 09:59:08', 'active', 'sell', 'without_account', '962184762', 2, 0, 7060, 61, NULL, false, false, false, 'Ad without map and with commune', 'ad without map and with commune (user without account)', 10000, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$(jY[Y=+HK91GI8d]07045085f3ac7270da09dffc68df19e8c014179d', '2015-05-28 09:59:08.217609', 'es');
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (138, 8000097, '2015-05-28 10:57:08', 'active', 'buy', 'with_account_and_no_commune', '62184762', 4, 0, 2020, 59, NULL, false, false, true, 'With map and precise marker(user with account)', 'with map and precise marker(user with account without commune)', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$FzcbB[j!;t(sMn(yaebb74b99fa983dd2008e0c941bf65b375b5459d', '2015-05-28 10:57:08.829112', 'es');
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (128, 8000087, '2015-05-28 10:01:26', 'active', 'sell', 'without_account', '962184762', 10, 0, 4040, 61, NULL, false, false, false, 'Ad without map and without commune', 'ad without map and without commune (user without account)', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$p/LL*&tE9s.3u4)+29ee866b5978e59efdccd8987e9168537601ff61', '2015-05-28 10:01:26.994731', 'es');
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (134, 8000093, '2015-05-28 10:44:16', 'active', 'buy', 'with_account_and_commune', '962184762', 15, 0, 9040, 60, NULL, false, false, false, 'With map and aprox marker (user with account )', 'with map and aprox marker (user with account and commune)

Ad in commune different at the account.', 3232, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$`]''PZ;eo&G3$3$K%9fcdad5b647664c099e3af06ad387279f0cd2d39', '2015-05-28 10:44:16.578315', 'es');
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (129, 8000088, '2015-05-28 10:15:18', 'active', 'sell', 'without_account', '962184762', 15, 0, 1040, 61, NULL, false, false, false, 'With map and with commune and aprox marker', 'with map and with commune and aprox marker (user without account)', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$x.7A%*F ${7Pl``(9485362bd9cef7b7351786cf20dc6a0a5d1777e8', '2015-05-28 10:15:18.098606', 'es');
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (130, 8000089, '2015-05-28 10:22:56', 'active', 'rent', 'without_account', '962184762', 15, 0, 1060, 61, NULL, false, false, false, 'With map and with commune and precise marker', 'with map and with commune and precise marker (user without account)', 10000000, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$R0@xvuEq!z>RKjG"a5e936c56dbabbd93d4d2d4c17f90a43a94caed3', '2015-05-28 10:22:56.144317', 'es');
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (135, 8000094, '2015-05-28 10:44:26', 'active', 'sell', 'with_account_and_commune', '962184762', 6, 0, 6020, 60, NULL, false, false, false, 'With map and precise marker (user with account )', 'With map and precise marker (user with account  and commune)', 32, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$`]''PZ;eo&G3$3$K%9fcdad5b647664c099e3af06ad387279f0cd2d39', '2015-05-28 10:44:26.616185', 'es');
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (131, 8000090, '2015-05-28 10:23:05', 'active', 'rent', 'without_account', '962184762', 15, 0, 1080, 61, NULL, false, false, false, 'With map and commune and aprox marker(near border)', 'with map and commune and aprox marker(near border)(user wihtout account)', 321213321, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$z1k{TP=M$r{A@Q4}9626a775ff4c38e031800aaf233d87e8cffbb1f6', '2015-05-28 10:23:05.010654', 'es');
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (132, 8000091, '2015-05-28 10:32:13', 'active', 'buy', 'without_account', '962184762', 15, 0, 5020, 61, NULL, false, false, false, 'With map and precise marker(near border)', 'with map and precise marker(near border) (user without account)', 2121, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$8V8Z##gj^3zR5q<H2bfa5da2c2a00978e2fd1e79bf449ab15cd25da8', '2015-05-28 10:32:13.495143', 'es');
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (133, 8000092, '2015-05-28 10:36:47', 'active', 'sell', 'with_account_and_commune', '962184762', 10, 0, 2060, 60, NULL, false, false, false, 'Without map and with commune', 'without map and with commune (with_account_and_commune)', 32312321, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$`]''PZ;eo&G3$3$K%9fcdad5b647664c099e3af06ad387279f0cd2d39', '2015-05-28 10:36:47.491486', 'es');
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (136, 8000095, '2015-05-28 10:50:30', 'active', 'buy', 'with_account_and_no_commune', '62184762', 14, 0, 1080, 59, NULL, false, false, true, 'With map and aprox marker (user with account)', 'with map and aprox marker center (user with account and without commune)', 2121, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$FzcbB[j!;t(sMn(yaebb74b99fa983dd2008e0c941bf65b375b5459d', '2015-05-28 10:50:30.183213', 'es');
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (139, 8000098, '2015-05-28 11:00:41', 'active', 'sell', 'with_account_and_no_commune', '62184762', 7, 0, 7020, 59, NULL, false, false, true, 'Ad without map and with commune (user with account', 'ad without map and with commune (user with account and without commune)', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$FzcbB[j!;t(sMn(yaebb74b99fa983dd2008e0c941bf65b375b5459d', '2015-05-28 11:00:41.599527', 'es');
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (137, 8000096, '2015-05-28 10:54:38', 'active', 'buy', 'with_account_and_no_commune', '62184762', 11, 0, 2080, 59, NULL, false, false, true, 'Ad without map and without commune', 'ad without map and without commune ( user with account )', 2121, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$FzcbB[j!;t(sMn(yaebb74b99fa983dd2008e0c941bf65b375b5459d', '2015-05-28 10:54:38.693566', 'es');
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (140, 8000099, '2015-05-28 11:21:21', 'active', 'let', 'with_account_and_no_commune', '62184762', 15, 0, 1020, 62, NULL, false, false, false, 'Ad with map with street Mar�ano S�nchez Fontecilla', 'ad with map with street Mar�ano S�nchez Fontecilla (user without account)', 321300, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$1pW LP0+fBnvvwoi5eeebb6bad7a3b0b4eff19547427049f896de2c9', '2015-05-28 11:21:21.398089', 'es');
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (141, 8000100, '2015-05-28 11:22:34', 'active', 'sell', 'with_account_and_no_commune', '62184762', 15, 0, 7020, 62, NULL, false, false, false, 'Ad with map street Mar�ano S�nchez Fontecilla2', 'ad with map with street Mar�ano S�nchez Fontecilla (user without account)', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$pS&fI`;uFI$a.gb(c03bde42d64668a023c881a217b2f95c3e91716d', '2015-05-28 11:22:34.617492', 'es');
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (142, 8000101, '2015-05-28 11:22:34', 'active', 'sell', 'without_account', '62184762', 13, 0, 1040, 62, NULL, false, false, false, 'Ad in Aisen', 'Ad in Aisen body', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$pS&fI`;uFI$a.gb(c03bde42d64668a023c881a217b2f95c3e91716d', '2015-05-28 11:22:34.617492', 'es');
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (143, 8000102, '2015-05-28 11:22:34', 'active', 'sell', 'without_account', '62184762', 13, 0, 2020, 62, NULL, false, false, false, 'Ad in Aisen autos brand 0', 'Ad in Aisen autos brand 0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$pS&fI`;uFI$a.gb(c03bde42d64668a023c881a217b2f95c3e91716d', '2015-05-28 11:22:34.617492', 'es');

INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (166, '633026231', 'verified', '2015-05-28 09:47:03.737456', NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (167, '768418254', 'verified', '2015-05-28 09:58:34.676127', NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (168, '903810277', 'verified', '2015-05-28 10:01:15.779341', NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (169, '139202300', 'verified', '2015-05-28 10:15:03.886209', NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (170, '274594323', 'verified', '2015-05-28 10:18:08.255035', NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (171, '409986346', 'verified', '2015-05-28 10:22:27.513455', NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (172, '545378369', 'verified', '2015-05-28 10:31:58.293625', NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (173, '680770392', 'verified', '2015-05-28 10:34:48.697571', NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (174, '816162415', 'verified', '2015-05-28 10:40:14.782191', NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (175, '951554438', 'verified', '2015-05-28 10:44:12.063365', NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (176, '186946461', 'verified', '2015-05-28 10:49:56.614073', NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (177, '322338484', 'verified', '2015-05-28 10:52:54.212361', NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (178, '457730507', 'verified', '2015-05-28 10:56:55.861276', NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (179, '593122530', 'verified', '2015-05-28 11:00:32.974347', NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (180, '728514553', 'verified', '2015-05-28 11:19:12.154001', NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (181, '863906576', 'verified', '2015-05-28 11:20:32.49815', NULL);

INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (126, 1, 'new', 701, 'accepted', 'normal', NULL, NULL, 166);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (138, 1, 'new', 749, 'accepted', 'normal', NULL, NULL, 178);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (127, 1, 'new', 705, 'accepted', 'normal', NULL, NULL, 167);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (128, 1, 'new', 709, 'accepted', 'normal', NULL, NULL, 168);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (129, 1, 'new', 713, 'accepted', 'normal', NULL, NULL, 169);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (139, 1, 'new', 753, 'accepted', 'normal', NULL, NULL, 179);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (130, 1, 'new', 720, 'accepted', 'normal', NULL, NULL, 170);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (131, 1, 'new', 721, 'accepted', 'normal', NULL, NULL, 171);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (132, 1, 'new', 725, 'accepted', 'normal', NULL, NULL, 172);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (133, 1, 'new', 729, 'accepted', 'normal', NULL, NULL, 173);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (134, 1, 'new', 736, 'accepted', 'normal', NULL, NULL, 174);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (135, 1, 'new', 737, 'accepted', 'normal', NULL, NULL, 175);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (136, 1, 'new', 741, 'accepted', 'normal', NULL, NULL, 176);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (140, 1, 'new', 760, 'accepted', 'normal', NULL, NULL, 180);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (141, 1, 'new', 761, 'accepted', 'normal', NULL, NULL, 181);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (137, 1, 'new', 745, 'accepted', 'normal', NULL, NULL, 177);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (142, 1, 'new', 765, 'accepted', 'normal', NULL, NULL, 177);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (143, 1, 'new', 769, 'accepted', 'normal', NULL, NULL, 177);


INSERT INTO action_params (ad_id, action_id, name, value) VALUES (126, 1, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (126, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (127, 1, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (127, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (128, 1, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (128, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (129, 1, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (129, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (130, 1, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (130, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (131, 1, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (131, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (132, 1, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (132, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (133, 1, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (133, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (134, 1, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (134, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (135, 1, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (135, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (136, 1, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (136, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (137, 1, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (137, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (138, 1, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (138, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (139, 1, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (139, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (140, 1, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (140, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (141, 1, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (141, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (142, 1, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (142, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (143, 1, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (143, 1, 'redir', 'dW5rbm93bg==');

INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (715, 'X55671d32530e1681000000005e0814e400000000', 9, '2015-05-28 09:50:41.788014', '2015-05-28 09:50:41.997485', '10.0.1.170', 'authenticate', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (716, 'X55671d326a94668f000000005073a0f400000000', 9, '2015-05-28 09:50:41.997485', '2015-05-28 09:50:43.793014', '10.0.1.170', 'spamfilter', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (717, 'X55671d347705c99500000000523c25100000000', 9, '2015-05-28 09:50:43.793014', '2015-05-28 09:50:55.828882', '10.0.1.170', 'listadmins', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (718, 'X55671d407aa020ee00000000508ac4a400000000', 9, '2015-05-28 09:50:55.828882', '2015-05-28 09:50:55.850564', '10.0.1.170', 'listadmins', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (719, 'X55671d405925b11c000000004aea19ad00000000', 9, '2015-05-28 09:50:55.850564', '2015-05-28 09:50:59.77154', '10.0.1.170', 'search_ads', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (720, 'X55671d443a0117d3000000007f099f6e00000000', 9, '2015-05-28 09:50:59.77154', '2015-05-28 09:50:59.879956', '10.0.1.170', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (721, 'X55671d443b39dc58000000003d3b336500000000', 9, '2015-05-28 09:50:59.879956', '2015-05-28 09:50:59.891672', '10.0.1.170', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (722, 'X55671d4469104e25000000003caceb8700000000', 9, '2015-05-28 09:50:59.891672', '2015-05-28 09:50:59.899386', '10.0.1.170', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (723, 'X55671d447c927897000000001fd1a6cb00000000', 9, '2015-05-28 09:50:59.899386', '2015-05-28 09:51:04.578772', '10.0.1.170', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (724, 'X55671d49261d2bb90000000047163f500000000', 9, '2015-05-28 09:51:04.578772', '2015-05-28 09:58:48.759987', '10.0.1.170', 'review', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (725, 'X55671f193d48c86a0000000056261d0200000000', 9, '2015-05-28 09:58:48.759987', '2015-05-28 09:58:48.792', '10.0.1.170', 'listadmins', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (726, 'X55671f19160de62e0000000058d94b1b00000000', 9, '2015-05-28 09:58:48.792', '2015-05-28 09:58:54.351759', '10.0.1.170', 'search_ads', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (727, 'X55671f1e29f1f165000000008c15baa00000000', 9, '2015-05-28 09:58:54.351759', '2015-05-28 09:58:54.451876', '10.0.1.170', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (728, 'X55671f1e38ecb754000000007584e70700000000', 9, '2015-05-28 09:58:54.451876', '2015-05-28 09:58:54.460945', '10.0.1.170', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (729, 'X55671f1e4cc48abe0000000059e9249c00000000', 9, '2015-05-28 09:58:54.460945', '2015-05-28 09:58:54.47097', '10.0.1.170', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (730, 'X55671f1e7d66c2a3000000008435d3000000000', 9, '2015-05-28 09:58:54.47097', '2015-05-28 09:59:08.211862', '10.0.1.170', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (731, 'X55671f2c3a62a541000000001dc3e98d00000000', 9, '2015-05-28 09:59:08.211862', '2015-05-28 10:01:19.984295', '10.0.1.170', 'review', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (732, 'X55671fb026fe94a50000000018b3fb9c00000000', 9, '2015-05-28 10:01:19.984295', '2015-05-28 10:01:20.002235', '10.0.1.170', 'listadmins', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (733, 'X55671fb07c96f88f000000003c013fe800000000', 9, '2015-05-28 10:01:20.002235', '2015-05-28 10:01:22.623585', '10.0.1.170', 'search_ads', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (734, 'X55671fb34369b68f0000000061196f9400000000', 9, '2015-05-28 10:01:22.623585', '2015-05-28 10:01:22.705005', '10.0.1.170', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (735, 'X55671fb31410469a00000000674e159c00000000', 9, '2015-05-28 10:01:22.705005', '2015-05-28 10:01:22.712386', '10.0.1.170', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (736, 'X55671fb3312d09750000000074cb41fc00000000', 9, '2015-05-28 10:01:22.712386', '2015-05-28 10:01:22.721298', '10.0.1.170', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (737, 'X55671fb325226e32000000004cbfec2e00000000', 9, '2015-05-28 10:01:22.721298', '2015-05-28 10:01:26.991224', '10.0.1.170', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (738, 'X55671fb75d95c6bc000000006d62bc5900000000', 9, '2015-05-28 10:01:26.991224', '2015-05-28 10:15:09.137416', '10.0.1.170', 'review', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (739, 'X556722ed6b26d1c7000000007a2c364e00000000', 9, '2015-05-28 10:15:09.137416', '2015-05-28 10:15:09.170523', '10.0.1.170', 'listadmins', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (740, 'X556722ed535a34720000000042fbdbee00000000', 9, '2015-05-28 10:15:09.170523', '2015-05-28 10:15:12.655144', '10.0.1.170', 'search_ads', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (741, 'X556722f15057d028000000006b8ea89200000000', 9, '2015-05-28 10:15:12.655144', '2015-05-28 10:15:12.720625', '10.0.1.170', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (742, 'X556722f17f1e1e90000000004007f79400000000', 9, '2015-05-28 10:15:12.720625', '2015-05-28 10:15:12.727108', '10.0.1.170', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (743, 'X556722f1502a1030000000061fcb5db00000000', 9, '2015-05-28 10:15:12.727108', '2015-05-28 10:15:12.733528', '10.0.1.170', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (744, 'X556722f168f504af000000002700f4f300000000', 9, '2015-05-28 10:15:12.733528', '2015-05-28 10:15:18.095291', '10.0.1.170', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (745, 'X556722f65586fbf6000000003e7b6f900000000', 9, '2015-05-28 10:15:18.095291', '2015-05-28 10:22:31.141515', '10.0.1.170', 'review', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (746, 'X556724a734452fa5000000005f4ff1d300000000', 9, '2015-05-28 10:22:31.141515', '2015-05-28 10:22:31.160161', '10.0.1.170', 'listadmins', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (747, 'X556724a7296ba18e000000007f22941d00000000', 9, '2015-05-28 10:22:31.160161', '2015-05-28 10:22:38.65739', '10.0.1.170', 'search_ads', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (748, 'X556724af56508883000000003e919fb700000000', 9, '2015-05-28 10:22:38.65739', '2015-05-28 10:22:38.771738', '10.0.1.170', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (749, 'X556724af296f4c95000000001ea1333900000000', 9, '2015-05-28 10:22:38.771738', '2015-05-28 10:22:38.783087', '10.0.1.170', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (750, 'X556724af17b9e81a000000003c3e1bbe00000000', 9, '2015-05-28 10:22:38.783087', '2015-05-28 10:22:38.792681', '10.0.1.170', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (751, 'X556724af75e69a3f00000000617fde6b00000000', 9, '2015-05-28 10:22:38.792681', '2015-05-28 10:22:56.138558', '10.0.1.170', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (752, 'X556724c02fb3bba3000000003795155100000000', 9, '2015-05-28 10:22:56.138558', '2015-05-28 10:23:00.348779', '10.0.1.170', 'review', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (753, 'X556724c42ad6bacf000000001069d7ba00000000', 9, '2015-05-28 10:23:00.348779', '2015-05-28 10:23:00.388965', '10.0.1.170', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (754, 'X556724c41f2951ea000000002a3ff0ef00000000', 9, '2015-05-28 10:23:00.388965', '2015-05-28 10:23:00.394649', '10.0.1.170', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (755, 'X556724c44dc78f580000000031afadf700000000', 9, '2015-05-28 10:23:00.394649', '2015-05-28 10:23:00.40026', '10.0.1.170', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (756, 'X556724c475e5a9a000000000f14d67100000000', 9, '2015-05-28 10:23:00.40026', '2015-05-28 10:23:05.000343', '10.0.1.170', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (757, 'X556724c9182dda96000000002003b3aa00000000', 9, '2015-05-28 10:23:05.000343', '2015-05-28 10:23:08.499753', '10.0.1.170', 'review', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (758, 'X556724cc3292dcc600000000625ac18200000000', 9, '2015-05-28 10:23:08.499753', '2015-05-28 10:23:08.52132', '10.0.1.170', 'listadmins', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (759, 'X556724cd3a4b259b0000000055c977ae00000000', 9, '2015-05-28 10:23:08.52132', '2015-05-28 10:32:02.344458', '10.0.1.170', 'search_ads', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (760, 'X556726e23bcda581000000007f32706900000000', 9, '2015-05-28 10:32:02.344458', '2015-05-28 10:32:02.377802', '10.0.1.170', 'listadmins', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (761, 'X556726e26cc58f350000000036e3052c00000000', 9, '2015-05-28 10:32:02.377802', '2015-05-28 10:32:06.356026', '10.0.1.170', 'search_ads', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (762, 'X556726e64602f647000000001b7f4ece00000000', 9, '2015-05-28 10:32:06.356026', '2015-05-28 10:32:06.456343', '10.0.1.170', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (763, 'X556726e64ac46b6d000000004878fd9700000000', 9, '2015-05-28 10:32:06.456343', '2015-05-28 10:32:06.461447', '10.0.1.170', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (764, 'X556726e6145b76590000000034f63f6e00000000', 9, '2015-05-28 10:32:06.461447', '2015-05-28 10:32:06.466727', '10.0.1.170', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (765, 'X556726e635624b7f000000003a58f49100000000', 9, '2015-05-28 10:32:06.466727', '2015-05-28 10:32:13.490255', '10.0.1.170', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (766, 'X556726ed1976ef6e000000002b5d63ec00000000', 9, '2015-05-28 10:32:13.490255', '2015-05-28 10:34:57.35904', '10.0.1.170', 'review', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (767, 'X556727915cc87932000000003531969c00000000', 9, '2015-05-28 10:34:57.35904', '2015-05-28 10:34:57.378464', '10.0.1.170', 'listadmins', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (768, 'X556727911ff6eb840000000038282b1300000000', 9, '2015-05-28 10:34:57.378464', '2015-05-28 10:35:11.260815', '10.0.1.170', 'search_ads', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (769, 'X5567279f797392b5000000002b528a4800000000', 9, '2015-05-28 10:35:11.260815', '2015-05-28 10:35:11.280059', '10.0.1.170', 'listadmins', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (770, 'X5567279fabb859700000000288068f900000000', 9, '2015-05-28 10:35:11.280059', '2015-05-28 10:36:39.330641', '10.0.1.170', 'search_ads', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (771, 'X556727f75c2a27730000000012c9ee9c00000000', 9, '2015-05-28 10:36:39.330641', '2015-05-28 10:36:39.473214', '10.0.1.170', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (772, 'X556727f72489f44a000000001eb5484b00000000', 9, '2015-05-28 10:36:39.473214', '2015-05-28 10:36:39.482514', '10.0.1.170', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (773, 'X556727f72c8eca930000000032ca97bd00000000', 9, '2015-05-28 10:36:39.482514', '2015-05-28 10:36:39.491307', '10.0.1.170', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (774, 'X556727f744398713000000002b1fc90f00000000', 9, '2015-05-28 10:36:39.491307', '2015-05-28 10:36:47.486294', '10.0.1.170', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (775, 'X556727ff320581a4000000007020dcd000000000', 9, '2015-05-28 10:36:47.486294', '2015-05-28 10:40:18.785904', '10.0.1.170', 'review', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (776, 'X556728d315b006860000000031a82b5b00000000', 9, '2015-05-28 10:40:18.785904', '2015-05-28 10:40:18.812848', '10.0.1.170', 'listadmins', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (777, 'X556728d3510cb1c6000000003a8a1dfa00000000', 9, '2015-05-28 10:40:18.812848', '2015-05-28 10:40:21.27045', '10.0.1.170', 'search_ads', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (778, 'X556728d517adbcdb00000000161af9c600000000', 9, '2015-05-28 10:40:21.27045', '2015-05-28 10:40:21.39273', '10.0.1.170', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (779, 'X556728d549780150000000019bd69be00000000', 9, '2015-05-28 10:40:21.39273', '2015-05-28 10:40:21.40293', '10.0.1.170', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (780, 'X556728d551e41c3100000000235629f500000000', 9, '2015-05-28 10:40:21.40293', '2015-05-28 10:40:21.412546', '10.0.1.170', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (781, 'X556728d51e31a0790000000038878f9300000000', 9, '2015-05-28 10:40:21.412546', '2015-05-28 10:44:16.565618', '10.0.1.170', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (782, 'X556729c17662229d0000000025248c6000000000', 9, '2015-05-28 10:44:16.565618', '2015-05-28 10:44:19.658933', '10.0.1.170', 'review', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (783, 'X556729c45030ae120000000062704f9200000000', 9, '2015-05-28 10:44:19.658933', '2015-05-28 10:44:19.689566', '10.0.1.170', 'listadmins', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (784, 'X556729c439c2e4d4000000001b62aecd00000000', 9, '2015-05-28 10:44:19.689566', '2015-05-28 10:44:22.400126', '10.0.1.170', 'search_ads', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (785, 'X556729c659cd2545000000006108ab2f00000000', 9, '2015-05-28 10:44:22.400126', '2015-05-28 10:44:22.466039', '10.0.1.170', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (786, 'X556729c63220bee20000000016d9c14100000000', 9, '2015-05-28 10:44:22.466039', '2015-05-28 10:44:22.47434', '10.0.1.170', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (787, 'X556729c617f240c8000000003244f4f00000000', 9, '2015-05-28 10:44:22.47434', '2015-05-28 10:44:22.482317', '10.0.1.170', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (788, 'X556729c656ec463c00000000796340d00000000', 9, '2015-05-28 10:44:22.482317', '2015-05-28 10:44:26.609472', '10.0.1.170', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (789, 'X556729cb4165767a0000000061cc8e100000000', 9, '2015-05-28 10:44:26.609472', '2015-05-28 10:45:25.816303', '10.0.1.170', 'review', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (790, 'X55672a063ec018b3000000003840d17400000000', 9, '2015-05-28 10:45:25.816303', '2015-05-28 10:45:25.845496', '10.0.1.170', 'listadmins', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (791, 'X55672a0643ddb5120000000043145a1a00000000', 9, '2015-05-28 10:45:25.845496', '2015-05-28 10:50:01.13136', '10.0.1.170', 'search_ads', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (792, 'X55672b1958efdecc0000000015c875a700000000', 9, '2015-05-28 10:50:01.13136', '2015-05-28 10:50:01.164145', '10.0.1.170', 'listadmins', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (793, 'X55672b197bf11df9000000004132896900000000', 9, '2015-05-28 10:50:01.164145', '2015-05-28 10:50:04.333679', '10.0.1.170', 'search_ads', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (794, 'X55672b1c73eddb4100000000495c24d900000000', 9, '2015-05-28 10:50:04.333679', '2015-05-28 10:50:04.349434', '10.0.1.170', 'listadmins', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (795, 'X55672b1ccb97fd100000000398035d800000000', 9, '2015-05-28 10:50:04.349434', '2015-05-28 10:50:05.616069', '10.0.1.170', 'search_ads', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (796, 'X55672b1e5471bd1b000000005000729c00000000', 9, '2015-05-28 10:50:05.616069', '2015-05-28 10:50:05.635341', '10.0.1.170', 'listadmins', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (797, 'X55672b1e53ec62130000000019b902d000000000', 9, '2015-05-28 10:50:05.635341', '2015-05-28 10:50:10.944069', '10.0.1.170', 'search_ads', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (798, 'X55672b235bf8bb9d0000000055bebb1b00000000', 9, '2015-05-28 10:50:10.944069', '2015-05-28 10:50:10.960252', '10.0.1.170', 'listadmins', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (799, 'X55672b2368f3fd51000000002ce10e8100000000', 9, '2015-05-28 10:50:10.960252', '2015-05-28 10:50:19.526544', '10.0.1.170', 'search_ads', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (800, 'X55672b2c610a7f5400000000414374a700000000', 9, '2015-05-28 10:50:19.526544', '2015-05-28 10:50:19.537516', '10.0.1.170', 'listadmins', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (801, 'X55672b2c73a0adc6000000004c76af7800000000', 9, '2015-05-28 10:50:19.537516', '2015-05-28 10:50:23.653452', '10.0.1.170', 'search_ads', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (802, 'X55672b307fc9c1b3000000006353a42a00000000', 9, '2015-05-28 10:50:23.653452', '2015-05-28 10:50:23.786471', '10.0.1.170', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (803, 'X55672b304cac40f800000000d760dcb00000000', 9, '2015-05-28 10:50:23.786471', '2015-05-28 10:50:23.79896', '10.0.1.170', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (804, 'X55672b3010216211000000002b03dc8600000000', 9, '2015-05-28 10:50:23.79896', '2015-05-28 10:50:23.81017', '10.0.1.170', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (805, 'X55672b306df8a89f0000000056db4f0b00000000', 9, '2015-05-28 10:50:23.81017', '2015-05-28 10:50:30.177474', '10.0.1.170', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (806, 'X55672b36753d52d200000000d8595f000000000', 9, '2015-05-28 10:50:30.177474', '2015-05-28 10:50:49.303672', '10.0.1.170', 'review', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (807, 'X55672b497b04d6bd00000000750c436500000000', 9, '2015-05-28 10:50:49.303672', '2015-05-28 10:50:49.35936', '10.0.1.170', 'listadmins', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (808, 'X55672b491b0b7df3000000007702b51900000000', 9, '2015-05-28 10:50:49.35936', '2015-05-28 10:52:57.444577', '10.0.1.170', 'search_ads', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (809, 'X55672bc926117160000000001a1784fd00000000', 9, '2015-05-28 10:52:57.444577', '2015-05-28 10:52:57.464085', '10.0.1.170', 'listadmins', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (810, 'X55672bc92a3eb37e000000005b00cd8d00000000', 9, '2015-05-28 10:52:57.464085', '2015-05-28 10:52:59.77454', '10.0.1.170', 'search_ads', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (811, 'X55672bcc73d7113f000000002fd80f6500000000', 9, '2015-05-28 10:52:59.77454', '2015-05-28 10:52:59.89038', '10.0.1.170', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (812, 'X55672bcc304a46aa0000000076b6ff2600000000', 9, '2015-05-28 10:52:59.89038', '2015-05-28 10:52:59.899787', '10.0.1.170', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (813, 'X55672bcc1d9907a60000000066542b4100000000', 9, '2015-05-28 10:52:59.899787', '2015-05-28 10:52:59.908784', '10.0.1.170', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (814, 'X55672bcc3e05b68a0000000032b96e2400000000', 9, '2015-05-28 10:52:59.908784', '2015-05-28 10:54:33.486191', '10.0.1.170', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (815, 'X55672c2945ffbbff000000004c4f1a9300000000', 9, '2015-05-28 10:54:33.486191', '2015-05-28 10:54:33.526418', '10.0.1.170', 'listadmins', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (816, 'X55672c2a611e09ad00000000105709ff00000000', 9, '2015-05-28 10:54:33.526418', '2015-05-28 10:54:35.981916', '10.0.1.170', 'search_ads', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (817, 'X55672c2c6beb1423000000002f764c3a00000000', 9, '2015-05-28 10:54:35.981916', '2015-05-28 10:54:36.081881', '10.0.1.170', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (818, 'X55672c2cd28a9de000000003013f45a00000000', 9, '2015-05-28 10:54:36.081881', '2015-05-28 10:54:36.087924', '10.0.1.170', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (819, 'X55672c2c259a1ae1000000001857717b00000000', 9, '2015-05-28 10:54:36.087924', '2015-05-28 10:54:36.094743', '10.0.1.170', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (820, 'X55672c2cc96ecb600000000431006dd00000000', 9, '2015-05-28 10:54:36.094743', '2015-05-28 10:54:38.689235', '10.0.1.170', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (821, 'X55672c2f44d9dfb0000000003009f11e00000000', 9, '2015-05-28 10:54:38.689235', '2015-05-28 10:57:01.209654', '10.0.1.170', 'review', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (822, 'X55672cbd48d5992e000000004dcc091900000000', 9, '2015-05-28 10:57:01.209654', '2015-05-28 10:57:01.236798', '10.0.1.170', 'listadmins', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (823, 'X55672cbd26524859000000006ddc791200000000', 9, '2015-05-28 10:57:01.236798', '2015-05-28 10:57:04.043872', '10.0.1.170', 'search_ads', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (824, 'X55672cc03c57eecd000000005cd2433500000000', 9, '2015-05-28 10:57:04.043872', '2015-05-28 10:57:04.162321', '10.0.1.170', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (825, 'X55672cc07f0c3f20000000065ebbb1d00000000', 9, '2015-05-28 10:57:04.162321', '2015-05-28 10:57:04.17074', '10.0.1.170', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (826, 'X55672cc01886ec4900000000150832f700000000', 9, '2015-05-28 10:57:04.17074', '2015-05-28 10:57:04.17682', '10.0.1.170', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (827, 'X55672cc0207776b00000000446ad91600000000', 9, '2015-05-28 10:57:04.17682', '2015-05-28 10:57:08.824438', '10.0.1.170', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (828, 'X55672cc517f64b52000000007000a18d00000000', 9, '2015-05-28 10:57:08.824438', '2015-05-28 10:57:29.941355', '10.0.1.170', 'review', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (829, 'X55672cda3450ac000000000e1b3f6900000000', 9, '2015-05-28 10:57:29.941355', '2015-05-28 10:57:29.97189', '10.0.1.170', 'listadmins', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (830, 'X55672cda2f529eff000000006f4a5e9800000000', 9, '2015-05-28 10:57:29.97189', '2015-05-28 11:00:35.926791', '10.0.1.170', 'search_ads', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (831, 'X55672d946c2310a000000000246302f500000000', 9, '2015-05-28 11:00:35.926791', '2015-05-28 11:00:35.954265', '10.0.1.170', 'listadmins', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (832, 'X55672d9445532b480000000078c8699600000000', 9, '2015-05-28 11:00:35.954265', '2015-05-28 11:00:38.794843', '10.0.1.170', 'search_ads', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (833, 'X55672d97608fcb8700000000142bfc4300000000', 9, '2015-05-28 11:00:38.794843', '2015-05-28 11:00:38.877114', '10.0.1.170', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (834, 'X55672d9736e0785a00000000503dcb9800000000', 9, '2015-05-28 11:00:38.877114', '2015-05-28 11:00:38.883655', '10.0.1.170', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (835, 'X55672d9747a9af5c000000003424b18100000000', 9, '2015-05-28 11:00:38.883655', '2015-05-28 11:00:38.889595', '10.0.1.170', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (836, 'X55672d975241cbf5000000003ad63f9b00000000', 9, '2015-05-28 11:00:38.889595', '2015-05-28 11:00:41.596305', '10.0.1.170', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (837, 'X55672d9a658b93730000000027cbfb9e00000000', 9, '2015-05-28 11:00:41.596305', '2015-05-28 11:20:54.560806', '10.0.1.170', 'review', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (838, 'X556732574d0da6db0000000041ba89c00000000', 9, '2015-05-28 11:20:54.560806', '2015-05-28 11:20:54.59499', '10.0.1.170', 'listadmins', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (839, 'X5567325759490b72000000003c075d3800000000', 9, '2015-05-28 11:20:54.59499', '2015-05-28 11:21:01.574005', '10.0.1.170', 'search_ads', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (840, 'X5567325e208b1dd200000000542fea2000000000', 9, '2015-05-28 11:21:01.574005', '2015-05-28 11:21:01.637284', '10.0.1.170', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (841, 'X5567325e213d13bd00000000573e852d00000000', 9, '2015-05-28 11:21:01.637284', '2015-05-28 11:21:01.642798', '10.0.1.170', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (842, 'X5567325e30c2ae6a000000003b9bf51d00000000', 9, '2015-05-28 11:21:01.642798', '2015-05-28 11:21:01.648602', '10.0.1.170', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (843, 'X5567325e2a3ef520000000005b75a39c00000000', 9, '2015-05-28 11:21:01.648602', '2015-05-28 11:21:02.859016', '10.0.1.170', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (844, 'X5567325f69692f13000000005dcd913300000000', 9, '2015-05-28 11:21:02.859016', '2015-05-28 11:21:02.938994', '10.0.1.170', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (845, 'X5567325f5f7e1171000000004b09b2e300000000', 9, '2015-05-28 11:21:02.938994', '2015-05-28 11:21:02.946066', '10.0.1.170', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (846, 'X5567325f256e84650000000059e5f57300000000', 9, '2015-05-28 11:21:02.946066', '2015-05-28 11:21:02.953024', '10.0.1.170', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (847, 'X5567325f200ac05d00000000363a2afa00000000', 9, '2015-05-28 11:21:02.953024', '2015-05-28 11:21:21.39368', '10.0.1.170', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (848, 'X556732711a33229000000008c22c3700000000', 9, '2015-05-28 11:21:21.39368', '2015-05-28 11:21:45.642737', '10.0.1.170', 'review', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (849, 'X5567328a2e1e595c000000001423554d00000000', 9, '2015-05-28 11:21:45.642737', '2015-05-28 11:21:55.496537', '10.0.1.170', 'review', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (850, 'X55673293c01cb28000000005e94aacf00000000', 9, '2015-05-28 11:21:55.496537', '2015-05-28 11:21:55.522431', '10.0.1.170', 'listadmins', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (851, 'X5567329479cc37a0000000005ea8261e00000000', 9, '2015-05-28 11:21:55.522431', '2015-05-28 11:22:08.095633', '10.0.1.170', 'search_ads', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (852, 'X556732a04b8df393000000006e57f66900000000', 9, '2015-05-28 11:22:08.095633', '2015-05-28 11:22:08.204382', '10.0.1.170', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (853, 'X556732a05ea2aa7000000000416532f400000000', 9, '2015-05-28 11:22:08.204382', '2015-05-28 11:22:08.211972', '10.0.1.170', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (854, 'X556732a05688a5b9000000002ee448900000000', 9, '2015-05-28 11:22:08.211972', '2015-05-28 11:22:08.219002', '10.0.1.170', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (855, 'X556732a04e1accfb00000000200eff4600000000', 9, '2015-05-28 11:22:08.219002', '2015-05-28 11:22:34.613436', '10.0.1.170', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (856, 'X556732bb4033674500000000532b486f00000000', 9, '2015-05-28 11:22:34.613436', '2015-05-28 11:22:39.857304', '10.0.1.170', 'review', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (857, 'X556732c014e29f1a000000007fb2f74e00000000', 9, '2015-05-28 11:22:39.857304', '2015-05-28 11:22:39.881655', '10.0.1.170', 'listadmins', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (858, 'X556732c07286331100000000294b8a600000000', 9, '2015-05-28 11:22:39.881655', '2015-05-28 12:22:39.881655', '10.0.1.170', 'search_ads', NULL, NULL);

INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (126, 1, 698, 'reg', 'initial', '2015-05-28 09:47:03.737456', '10.0.1.170', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (126, 1, 699, 'unverified', 'verifymail', '2015-05-28 09:47:03.737456', '10.0.1.170', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (126, 1, 700, 'pending_review', 'verify', '2015-05-28 09:47:03.827674', '10.0.1.170', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (126, 1, 701, 'accepted', 'accept', '2015-05-28 09:51:04.58617', '10.0.1.170', 723);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (127, 1, 702, 'reg', 'initial', '2015-05-28 09:58:34.676127', '10.0.1.170', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (127, 1, 703, 'unverified', 'verifymail', '2015-05-28 09:58:34.676127', '10.0.1.170', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (127, 1, 704, 'pending_review', 'verify', '2015-05-28 09:58:34.775392', '10.0.1.170', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (127, 1, 705, 'accepted', 'accept_w_chngs', '2015-05-28 09:59:08.217609', '10.0.1.170', 730);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (128, 1, 706, 'reg', 'initial', '2015-05-28 10:01:15.779341', '10.0.1.170', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (128, 1, 707, 'unverified', 'verifymail', '2015-05-28 10:01:15.779341', '10.0.1.170', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (128, 1, 708, 'pending_review', 'verify', '2015-05-28 10:01:15.852915', '10.0.1.170', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (128, 1, 709, 'accepted', 'accept_w_chngs', '2015-05-28 10:01:26.994731', '10.0.1.170', 737);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (129, 1, 710, 'reg', 'initial', '2015-05-28 10:15:03.886209', '10.0.1.170', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (129, 1, 711, 'unverified', 'verifymail', '2015-05-28 10:15:03.886209', '10.0.1.170', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (129, 1, 712, 'pending_review', 'verify', '2015-05-28 10:15:03.96908', '10.0.1.170', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (129, 1, 713, 'accepted', 'accept', '2015-05-28 10:15:18.098606', '10.0.1.170', 744);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (130, 1, 714, 'reg', 'initial', '2015-05-28 10:18:08.255035', '10.0.1.170', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (130, 1, 715, 'unverified', 'verifymail', '2015-05-28 10:18:08.255035', '10.0.1.170', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (130, 1, 716, 'pending_review', 'verify', '2015-05-28 10:18:08.415371', '10.0.1.170', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (131, 1, 717, 'reg', 'initial', '2015-05-28 10:22:27.513455', '10.0.1.170', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (131, 1, 718, 'unverified', 'verifymail', '2015-05-28 10:22:27.513455', '10.0.1.170', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (131, 1, 719, 'pending_review', 'verify', '2015-05-28 10:22:27.69314', '10.0.1.170', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (130, 1, 720, 'accepted', 'accept_w_chngs', '2015-05-28 10:22:56.144317', '10.0.1.170', 751);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (131, 1, 721, 'accepted', 'accept_w_chngs', '2015-05-28 10:23:05.010654', '10.0.1.170', 756);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (132, 1, 722, 'reg', 'initial', '2015-05-28 10:31:58.293625', '10.0.1.170', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (132, 1, 723, 'unverified', 'verifymail', '2015-05-28 10:31:58.293625', '10.0.1.170', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (132, 1, 724, 'pending_review', 'verify', '2015-05-28 10:31:58.424534', '10.0.1.170', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (132, 1, 725, 'accepted', 'accept_w_chngs', '2015-05-28 10:32:13.495143', '10.0.1.170', 765);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (133, 1, 726, 'reg', 'initial', '2015-05-28 10:34:48.697571', '10.0.1.170', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (133, 1, 727, 'unverified', 'verifymail', '2015-05-28 10:34:48.697571', '10.0.1.170', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (133, 1, 728, 'pending_review', 'verify', '2015-05-28 10:34:48.779304', '10.0.1.170', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (133, 1, 729, 'accepted', 'accept_w_chngs', '2015-05-28 10:36:47.491486', '10.0.1.170', 774);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (134, 1, 730, 'reg', 'initial', '2015-05-28 10:40:14.782191', '10.0.1.170', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (134, 1, 731, 'unverified', 'verifymail', '2015-05-28 10:40:14.782191', '10.0.1.170', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (134, 1, 732, 'pending_review', 'verify', '2015-05-28 10:40:14.845169', '10.0.1.170', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (135, 1, 733, 'reg', 'initial', '2015-05-28 10:44:12.063365', '10.0.1.170', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (135, 1, 734, 'unverified', 'verifymail', '2015-05-28 10:44:12.063365', '10.0.1.170', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (135, 1, 735, 'pending_review', 'verify', '2015-05-28 10:44:12.148992', '10.0.1.170', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (134, 1, 736, 'accepted', 'accept', '2015-05-28 10:44:16.578315', '10.0.1.170', 781);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (135, 1, 737, 'accepted', 'accept_w_chngs', '2015-05-28 10:44:26.616185', '10.0.1.170', 788);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (136, 1, 738, 'reg', 'initial', '2015-05-28 10:49:56.614073', '10.0.1.170', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (136, 1, 739, 'unverified', 'verifymail', '2015-05-28 10:49:56.614073', '10.0.1.170', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (136, 1, 740, 'pending_review', 'verify', '2015-05-28 10:49:56.6917', '10.0.1.170', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (136, 1, 741, 'accepted', 'accept', '2015-05-28 10:50:30.183213', '10.0.1.170', 805);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (137, 1, 742, 'reg', 'initial', '2015-05-28 10:52:54.212361', '10.0.1.170', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (137, 1, 743, 'unverified', 'verifymail', '2015-05-28 10:52:54.212361', '10.0.1.170', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (137, 1, 744, 'pending_review', 'verify', '2015-05-28 10:52:54.280407', '10.0.1.170', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (137, 1, 745, 'accepted', 'accept', '2015-05-28 10:54:38.693566', '10.0.1.170', 820);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (138, 1, 746, 'reg', 'initial', '2015-05-28 10:56:55.861276', '10.0.1.170', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (138, 1, 747, 'unverified', 'verifymail', '2015-05-28 10:56:55.861276', '10.0.1.170', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (138, 1, 748, 'pending_review', 'verify', '2015-05-28 10:56:55.928603', '10.0.1.170', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (138, 1, 749, 'accepted', 'accept_w_chngs', '2015-05-28 10:57:08.829112', '10.0.1.170', 827);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (139, 1, 750, 'reg', 'initial', '2015-05-28 11:00:32.974347', '10.0.1.170', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (139, 1, 751, 'unverified', 'verifymail', '2015-05-28 11:00:32.974347', '10.0.1.170', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (139, 1, 752, 'pending_review', 'verify', '2015-05-28 11:00:33.112008', '10.0.1.170', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (139, 1, 753, 'accepted', 'accept', '2015-05-28 11:00:41.599527', '10.0.1.170', 836);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (140, 1, 754, 'reg', 'initial', '2015-05-28 11:19:12.154001', '10.0.1.170', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (140, 1, 755, 'unverified', 'verifymail', '2015-05-28 11:19:12.154001', '10.0.1.170', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (140, 1, 756, 'pending_review', 'verify', '2015-05-28 11:19:12.24497', '10.0.1.170', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (141, 1, 757, 'reg', 'initial', '2015-05-28 11:20:32.49815', '10.0.1.170', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (141, 1, 758, 'unverified', 'verifymail', '2015-05-28 11:20:32.49815', '10.0.1.170', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (141, 1, 759, 'pending_review', 'verify', '2015-05-28 11:20:32.580274', '10.0.1.170', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (140, 1, 760, 'accepted', 'accept', '2015-05-28 11:21:21.398089', '10.0.1.170', 847);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (141, 1, 761, 'accepted', 'accept', '2015-05-28 11:22:34.617492', '10.0.1.170', 855);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (142, 1, 762, 'reg', 'initial', '2015-05-28 10:22:27.513455', '10.0.1.170', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (142, 1, 763, 'unverified', 'verifymail', '2015-05-28 10:22:27.513455', '10.0.1.170', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (142, 1, 764, 'pending_review', 'verify', '2015-05-28 10:22:27.69314', '10.0.1.170', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (142, 1, 765, 'accepted', 'accept_w_chngs', '2015-05-28 10:23:05.010654', '10.0.1.170', 756);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (143, 1, 766, 'reg', 'initial', '2015-05-28 10:22:27.513455', '10.0.1.170', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (143, 1, 767, 'unverified', 'verifymail', '2015-05-28 10:22:27.513455', '10.0.1.170', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (143, 1, 768, 'pending_review', 'verify', '2015-05-28 10:22:27.69314', '10.0.1.170', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (143, 1, 769, 'accepted', 'accept_w_chngs', '2015-05-28 10:23:05.010654', '10.0.1.170', 756);

SELECT pg_catalog.setval('action_states_state_id_seq', 769, true);

INSERT INTO ad_changes (ad_id, action_id, state_id, is_param, column_name, old_value, new_value) VALUES (140, 1, 760, false, 'subject', 'Ad with map with street mario sanches fontecilla', 'Ad with map with street Mar�ano S�nchez Fontecilla');
INSERT INTO ad_changes (ad_id, action_id, state_id, is_param, column_name, old_value, new_value) VALUES (140, 1, 760, false, 'body', 'ad with map with street mario sanches fontecilla (user without account)', 'ad with map with street Mar�ano S�nchez Fontecilla (user without account)');
INSERT INTO ad_changes (ad_id, action_id, state_id, is_param, column_name, old_value, new_value) VALUES (141, 1, 761, false, 'subject', 'Ad with map with street mario sanches fontecilla 2', 'Ad with map street Mar�ano S�nchez Fontecilla2');
INSERT INTO ad_changes (ad_id, action_id, state_id, is_param, column_name, old_value, new_value) VALUES (141, 1, 761, false, 'body', 'ad with map with street mario sanches fontecilla (user without account)', 'ad with map with street Mar�ano S�nchez Fontecilla (user without account)');

INSERT INTO ad_codes (code, code_type) VALUES (24741, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (56764, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (88787, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (30810, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (62833, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (94856, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (36879, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (68902, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (10925, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (42948, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (74971, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (16994, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (49017, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (81040, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (23063, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (55086, 'pay');

INSERT INTO ad_image_changes (ad_id, action_id, state_id, seq_no, name, is_new) VALUES (126, 1, 701, 0, NULL, false);
INSERT INTO ad_image_changes (ad_id, action_id, state_id, seq_no, name, is_new) VALUES (127, 1, 705, 0, NULL, false);
INSERT INTO ad_image_changes (ad_id, action_id, state_id, seq_no, name, is_new) VALUES (128, 1, 709, 0, NULL, false);
INSERT INTO ad_image_changes (ad_id, action_id, state_id, seq_no, name, is_new) VALUES (129, 1, 713, 0, NULL, false);
INSERT INTO ad_image_changes (ad_id, action_id, state_id, seq_no, name, is_new) VALUES (130, 1, 720, 0, NULL, false);
INSERT INTO ad_image_changes (ad_id, action_id, state_id, seq_no, name, is_new) VALUES (131, 1, 721, 0, NULL, false);
INSERT INTO ad_image_changes (ad_id, action_id, state_id, seq_no, name, is_new) VALUES (132, 1, 725, 0, NULL, false);
INSERT INTO ad_image_changes (ad_id, action_id, state_id, seq_no, name, is_new) VALUES (133, 1, 729, 0, NULL, false);
INSERT INTO ad_image_changes (ad_id, action_id, state_id, seq_no, name, is_new) VALUES (134, 1, 736, 0, NULL, false);
INSERT INTO ad_image_changes (ad_id, action_id, state_id, seq_no, name, is_new) VALUES (135, 1, 737, 0, NULL, false);
INSERT INTO ad_image_changes (ad_id, action_id, state_id, seq_no, name, is_new) VALUES (136, 1, 741, 0, NULL, false);
INSERT INTO ad_image_changes (ad_id, action_id, state_id, seq_no, name, is_new) VALUES (137, 1, 745, 0, NULL, false);
INSERT INTO ad_image_changes (ad_id, action_id, state_id, seq_no, name, is_new) VALUES (138, 1, 749, 0, NULL, false);
INSERT INTO ad_image_changes (ad_id, action_id, state_id, seq_no, name, is_new) VALUES (139, 1, 753, 0, NULL, false);
INSERT INTO ad_image_changes (ad_id, action_id, state_id, seq_no, name, is_new) VALUES (140, 1, 760, 0, NULL, false);
INSERT INTO ad_image_changes (ad_id, action_id, state_id, seq_no, name, is_new) VALUES (141, 1, 761, 0, NULL, false);

INSERT INTO ad_params (ad_id, name, value) VALUES (126, 'prev_currency', 'peso');
INSERT INTO ad_params (ad_id, name, value) VALUES (126, 'country', 'UNK');
INSERT INTO ad_params (ad_id, name, value) VALUES (127, 'service_type', '2');
INSERT INTO ad_params (ad_id, name, value) VALUES (127, 'communes', '11');
INSERT INTO ad_params (ad_id, name, value) VALUES (127, 'prev_currency', 'peso');
INSERT INTO ad_params (ad_id, name, value) VALUES (127, 'country', 'UNK');
INSERT INTO ad_params (ad_id, name, value) VALUES (128, 'gender', '3');
INSERT INTO ad_params (ad_id, name, value) VALUES (128, 'condition', '2');
INSERT INTO ad_params (ad_id, name, value) VALUES (128, 'prev_currency', 'peso');
INSERT INTO ad_params (ad_id, name, value) VALUES (128, 'country', 'UNK');
INSERT INTO ad_params (ad_id, name, value) VALUES (129, 'rooms', '2');
INSERT INTO ad_params (ad_id, name, value) VALUES (129, 'size', '1313');
INSERT INTO ad_params (ad_id, name, value) VALUES (129, 'garage_spaces', '323');
INSERT INTO ad_params (ad_id, name, value) VALUES (129, 'condominio', '3232');
INSERT INTO ad_params (ad_id, name, value) VALUES (129, 'communes', '315');
INSERT INTO ad_params (ad_id, name, value) VALUES (129, 'prev_currency', 'peso');
INSERT INTO ad_params (ad_id, name, value) VALUES (129, 'geoposition', '-33.4279289,-70.5882797');
INSERT INTO ad_params (ad_id, name, value) VALUES (129, 'address', 'Avenida Mar�ano S�nchez Fontecilla');
INSERT INTO ad_params (ad_id, name, value) VALUES (129, 'street_id', '62363');
INSERT INTO ad_params (ad_id, name, value) VALUES (129, 'country', 'UNK');
INSERT INTO ad_params (ad_id, name, value) VALUES (130, 'size', '3213');
INSERT INTO ad_params (ad_id, name, value) VALUES (130, 'garage_spaces', '323');
INSERT INTO ad_params (ad_id, name, value) VALUES (130, 'communes', '343');
INSERT INTO ad_params (ad_id, name, value) VALUES (130, 'prev_currency', 'peso');
INSERT INTO ad_params (ad_id, name, value) VALUES (130, 'address_number', '123');
INSERT INTO ad_params (ad_id, name, value) VALUES (130, 'geoposition', '-33.4663506,-70.6611862');
INSERT INTO ad_params (ad_id, name, value) VALUES (130, 'geoposition_is_precise', '1');
INSERT INTO ad_params (ad_id, name, value) VALUES (130, 'address', 'Club de Tenis Municipal de Santiago');
INSERT INTO ad_params (ad_id, name, value) VALUES (130, 'street_id', '92409');
INSERT INTO ad_params (ad_id, name, value) VALUES (130, 'country', 'UNK');
INSERT INTO ad_params (ad_id, name, value) VALUES (131, 'size', '3232');
INSERT INTO ad_params (ad_id, name, value) VALUES (131, 'garage_spaces', '323');
INSERT INTO ad_params (ad_id, name, value) VALUES (131, 'communes', '315');
INSERT INTO ad_params (ad_id, name, value) VALUES (131, 'prev_currency', 'peso');
INSERT INTO ad_params (ad_id, name, value) VALUES (131, 'address_number', '1001');
INSERT INTO ad_params (ad_id, name, value) VALUES (131, 'geoposition', '-33.417902280002465,-70.60380935668944');
INSERT INTO ad_params (ad_id, name, value) VALUES (131, 'address', 'Avenida Mar�ano S�nchez Fontecilla');
INSERT INTO ad_params (ad_id, name, value) VALUES (131, 'street_id', '62363');
INSERT INTO ad_params (ad_id, name, value) VALUES (131, 'country', 'UNK');
INSERT INTO ad_params (ad_id, name, value) VALUES (132, 'communes', '334');
INSERT INTO ad_params (ad_id, name, value) VALUES (132, 'prev_currency', 'peso');
INSERT INTO ad_params (ad_id, name, value) VALUES (132, 'address_number', '123');
INSERT INTO ad_params (ad_id, name, value) VALUES (132, 'geoposition', '-33.4094482540654,-70.69384574890137');
INSERT INTO ad_params (ad_id, name, value) VALUES (132, 'geoposition_is_precise', '1');
INSERT INTO ad_params (ad_id, name, value) VALUES (132, 'address', 'Calle Lastenia Z��iga');
INSERT INTO ad_params (ad_id, name, value) VALUES (132, 'street_id', '84260');
INSERT INTO ad_params (ad_id, name, value) VALUES (132, 'country', 'UNK');
INSERT INTO ad_params (ad_id, name, value) VALUES (133, 'regdate', '2015');
INSERT INTO ad_params (ad_id, name, value) VALUES (133, 'mileage', '12');
INSERT INTO ad_params (ad_id, name, value) VALUES (133, 'cubiccms', '6');
INSERT INTO ad_params (ad_id, name, value) VALUES (133, 'communes', '220');
INSERT INTO ad_params (ad_id, name, value) VALUES (133, 'prev_currency', 'peso');
INSERT INTO ad_params (ad_id, name, value) VALUES (133, 'country', 'UNK');
INSERT INTO ad_params (ad_id, name, value) VALUES (134, 'condition', '2');
INSERT INTO ad_params (ad_id, name, value) VALUES (134, 'communes', '326');
INSERT INTO ad_params (ad_id, name, value) VALUES (134, 'geoposition', '-33.5021706,-70.6881409');
INSERT INTO ad_params (ad_id, name, value) VALUES (134, 'address', 'Calle Maestranza');
INSERT INTO ad_params (ad_id, name, value) VALUES (134, 'street_id', '72862');
INSERT INTO ad_params (ad_id, name, value) VALUES (134, 'country', 'UNK');
INSERT INTO ad_params (ad_id, name, value) VALUES (135, 'communes', '82');
INSERT INTO ad_params (ad_id, name, value) VALUES (135, 'prev_currency', 'peso');
INSERT INTO ad_params (ad_id, name, value) VALUES (135, 'address_number', '12');
INSERT INTO ad_params (ad_id, name, value) VALUES (135, 'geoposition', '-32.5823212,-71.4504013');
INSERT INTO ad_params (ad_id, name, value) VALUES (135, 'geoposition_is_precise', '1');
INSERT INTO ad_params (ad_id, name, value) VALUES (135, 'address', 'Calle Federico Kohnenkampf');
INSERT INTO ad_params (ad_id, name, value) VALUES (135, 'street_id', '113409');
INSERT INTO ad_params (ad_id, name, value) VALUES (135, 'country', 'UNK');
INSERT INTO ad_params (ad_id, name, value) VALUES (136, 'size', '3213');
INSERT INTO ad_params (ad_id, name, value) VALUES (136, 'garage_spaces', '321');
INSERT INTO ad_params (ad_id, name, value) VALUES (136, 'communes', '294');
INSERT INTO ad_params (ad_id, name, value) VALUES (136, 'prev_currency', 'peso');
INSERT INTO ad_params (ad_id, name, value) VALUES (136, 'geoposition', '-51.2580299,-72.3421173');
INSERT INTO ad_params (ad_id, name, value) VALUES (136, 'address', 'Camino Hist�rico Las Carretas');
INSERT INTO ad_params (ad_id, name, value) VALUES (136, 'street_id', '47855');
INSERT INTO ad_params (ad_id, name, value) VALUES (136, 'country', 'UNK');
INSERT INTO ad_params (ad_id, name, value) VALUES (137, 'prev_currency', 'peso');
INSERT INTO ad_params (ad_id, name, value) VALUES (137, 'country', 'UNK');
INSERT INTO ad_params (ad_id, name, value) VALUES (138, 'regdate', '2007');
INSERT INTO ad_params (ad_id, name, value) VALUES (138, 'mileage', '212121');
INSERT INTO ad_params (ad_id, name, value) VALUES (138, 'gearbox', '2');
INSERT INTO ad_params (ad_id, name, value) VALUES (138, 'fuel', '1');
INSERT INTO ad_params (ad_id, name, value) VALUES (138, 'cartype', '1');
INSERT INTO ad_params (ad_id, name, value) VALUES (138, 'communes', '24');
INSERT INTO ad_params (ad_id, name, value) VALUES (138, 'prev_currency', 'peso');
INSERT INTO ad_params (ad_id, name, value) VALUES (138, 'brand', '2');
INSERT INTO ad_params (ad_id, name, value) VALUES (138, 'model', '2');
INSERT INTO ad_params (ad_id, name, value) VALUES (138, 'version', '1');
INSERT INTO ad_params (ad_id, name, value) VALUES (138, 'address_number', '122');
INSERT INTO ad_params (ad_id, name, value) VALUES (138, 'geoposition', '-27.3563004,-70.3104324');
INSERT INTO ad_params (ad_id, name, value) VALUES (138, 'geoposition_is_precise', '1');
INSERT INTO ad_params (ad_id, name, value) VALUES (138, 'address', 'Rafael Ruiz Mar�n Escalera');
INSERT INTO ad_params (ad_id, name, value) VALUES (138, 'street_id', '38380');
INSERT INTO ad_params (ad_id, name, value) VALUES (138, 'country', 'UNK');
INSERT INTO ad_params (ad_id, name, value) VALUES (139, 'job_category', '19|20|21|24');
INSERT INTO ad_params (ad_id, name, value) VALUES (139, 'communes', '84');
INSERT INTO ad_params (ad_id, name, value) VALUES (139, 'country', 'UNK');
INSERT INTO ad_params (ad_id, name, value) VALUES (140, 'rooms', '4');
INSERT INTO ad_params (ad_id, name, value) VALUES (140, 'size', '2323');
INSERT INTO ad_params (ad_id, name, value) VALUES (140, 'garage_spaces', '31');
INSERT INTO ad_params (ad_id, name, value) VALUES (140, 'condominio', '3213123');
INSERT INTO ad_params (ad_id, name, value) VALUES (140, 'communes', '315');
INSERT INTO ad_params (ad_id, name, value) VALUES (140, 'prev_currency', 'peso');
INSERT INTO ad_params (ad_id, name, value) VALUES (140, 'address_number', '1001');
INSERT INTO ad_params (ad_id, name, value) VALUES (140, 'geoposition', '-33.4256935,-70.590889');
INSERT INTO ad_params (ad_id, name, value) VALUES (140, 'address', 'Avenida Mar�ano S�nchez Fontecilla');
INSERT INTO ad_params (ad_id, name, value) VALUES (140, 'street_id', '62363');
INSERT INTO ad_params (ad_id, name, value) VALUES (140, 'country', 'UNK');
INSERT INTO ad_params (ad_id, name, value) VALUES (141, 'job_category', '19|22');
INSERT INTO ad_params (ad_id, name, value) VALUES (141, 'communes', '315');
INSERT INTO ad_params (ad_id, name, value) VALUES (141, 'address_number', '1001');
INSERT INTO ad_params (ad_id, name, value) VALUES (141, 'geoposition', '-33.4256935,-70.590889');
INSERT INTO ad_params (ad_id, name, value) VALUES (141, 'geoposition_is_precise', '1');
INSERT INTO ad_params (ad_id, name, value) VALUES (141, 'address', 'Avenida Mar�ano S�nchez Fontecilla');
INSERT INTO ad_params (ad_id, name, value) VALUES (141, 'street_id', '62363');
INSERT INTO ad_params (ad_id, name, value) VALUES (141, 'country', 'UNK');
INSERT INTO ad_params (ad_id, name, value) VALUES (142, 'size', '3232');
INSERT INTO ad_params (ad_id, name, value) VALUES (142, 'garage_spaces', '323');
INSERT INTO ad_params (ad_id, name, value) VALUES (142, 'communes', '315');
INSERT INTO ad_params (ad_id, name, value) VALUES (142, 'prev_currency', 'peso');
INSERT INTO ad_params (ad_id, name, value) VALUES (142, 'address_number', '1001');
INSERT INTO ad_params (ad_id, name, value) VALUES (142, 'geoposition', '-33.417902280002465,-70.60380935668944');
INSERT INTO ad_params (ad_id, name, value) VALUES (142, 'address', 'Avenida Mar�ano S�nchez Fontecilla');
INSERT INTO ad_params (ad_id, name, value) VALUES (142, 'street_id', '62363');
INSERT INTO ad_params (ad_id, name, value) VALUES (142, 'country', 'UNK');
INSERT INTO ad_params (ad_id, name, value) VALUES (143, 'regdate', '2007');
INSERT INTO ad_params (ad_id, name, value) VALUES (143, 'mileage', '212121');
INSERT INTO ad_params (ad_id, name, value) VALUES (143, 'gearbox', '2');
INSERT INTO ad_params (ad_id, name, value) VALUES (143, 'fuel', '1');
INSERT INTO ad_params (ad_id, name, value) VALUES (143, 'cartype', '1');
INSERT INTO ad_params (ad_id, name, value) VALUES (143, 'communes', '274');
INSERT INTO ad_params (ad_id, name, value) VALUES (143, 'prev_currency', 'peso');
INSERT INTO ad_params (ad_id, name, value) VALUES (143, 'brand', '0');
INSERT INTO ad_params (ad_id, name, value) VALUES (143, 'model', '0');
INSERT INTO ad_params (ad_id, name, value) VALUES (143, 'version', '0');
INSERT INTO ad_params (ad_id, name, value) VALUES (143, 'address_number', '122');
INSERT INTO ad_params (ad_id, name, value) VALUES (143, 'geoposition', '-27.3563004,-70.3104324');
INSERT INTO ad_params (ad_id, name, value) VALUES (143, 'geoposition_is_precise', '1');
INSERT INTO ad_params (ad_id, name, value) VALUES (143, 'address', 'Rafael Ruiz Mar�n Escalera');
INSERT INTO ad_params (ad_id, name, value) VALUES (143, 'street_id', '38380');
INSERT INTO ad_params (ad_id, name, value) VALUES (143, 'country', 'UNK');

SELECT pg_catalog.setval('ads_ad_id_seq', 143, true);

SELECT pg_catalog.setval('list_id_seq', 8000102, true);

SELECT pg_catalog.setval('pay_code_seq', 482, true);

INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (213, 'save', 0, '633026231', NULL, '2015-05-28 09:47:04', 166, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (214, 'verify', 0, '633026231', NULL, '2015-05-28 09:47:04', 166, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (215, 'save', 0, '768418254', NULL, '2015-05-28 09:58:35', 167, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (216, 'verify', 0, '768418254', NULL, '2015-05-28 09:58:35', 167, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (217, 'save', 0, '903810277', NULL, '2015-05-28 10:01:16', 168, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (218, 'verify', 0, '903810277', NULL, '2015-05-28 10:01:16', 168, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (219, 'save', 0, '139202300', NULL, '2015-05-28 10:15:04', 169, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (220, 'verify', 0, '139202300', NULL, '2015-05-28 10:15:04', 169, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (221, 'save', 0, '274594323', NULL, '2015-05-28 10:18:08', 170, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (222, 'verify', 0, '274594323', NULL, '2015-05-28 10:18:08', 170, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (223, 'save', 0, '409986346', NULL, '2015-05-28 10:22:28', 171, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (224, 'verify', 0, '409986346', NULL, '2015-05-28 10:22:28', 171, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (225, 'save', 0, '545378369', NULL, '2015-05-28 10:31:58', 172, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (226, 'verify', 0, '545378369', NULL, '2015-05-28 10:31:58', 172, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (227, 'save', 0, '680770392', NULL, '2015-05-28 10:34:49', 173, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (228, 'verify', 0, '680770392', NULL, '2015-05-28 10:34:49', 173, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (229, 'save', 0, '816162415', NULL, '2015-05-28 10:40:15', 174, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (230, 'verify', 0, '816162415', NULL, '2015-05-28 10:40:15', 174, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (231, 'save', 0, '951554438', NULL, '2015-05-28 10:44:12', 175, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (232, 'verify', 0, '951554438', NULL, '2015-05-28 10:44:12', 175, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (233, 'save', 0, '186946461', NULL, '2015-05-28 10:49:57', 176, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (234, 'verify', 0, '186946461', NULL, '2015-05-28 10:49:57', 176, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (235, 'save', 0, '322338484', NULL, '2015-05-28 10:52:54', 177, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (236, 'verify', 0, '322338484', NULL, '2015-05-28 10:52:54', 177, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (237, 'save', 0, '457730507', NULL, '2015-05-28 10:56:56', 178, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (238, 'verify', 0, '457730507', NULL, '2015-05-28 10:56:56', 178, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (239, 'save', 0, '593122530', NULL, '2015-05-28 11:00:33', 179, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (240, 'verify', 0, '593122530', NULL, '2015-05-28 11:00:33', 179, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (241, 'save', 0, '728514553', NULL, '2015-05-28 11:19:12', 180, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (242, 'verify', 0, '728514553', NULL, '2015-05-28 11:19:12', 180, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (243, 'save', 0, '863906576', NULL, '2015-05-28 11:20:32', 181, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (244, 'verify', 0, '863906576', NULL, '2015-05-28 11:20:33', 181, 'OK');

SELECT pg_catalog.setval('pay_log_pay_log_id_seq', 244, true);

INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (213, 0, 'adphone', '962184762');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (213, 1, 'remote_addr', '10.0.1.170');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (214, 0, 'remote_addr', '10.0.1.170');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (215, 0, 'adphone', '962184762');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (215, 1, 'remote_addr', '10.0.1.170');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (216, 0, 'remote_addr', '10.0.1.170');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (217, 0, 'adphone', '962184762');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (217, 1, 'remote_addr', '10.0.1.170');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (218, 0, 'remote_addr', '10.0.1.170');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (219, 0, 'adphone', '962184762');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (219, 1, 'remote_addr', '10.0.1.170');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (220, 0, 'remote_addr', '10.0.1.170');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (221, 0, 'adphone', '962184762');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (221, 1, 'remote_addr', '10.0.1.170');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (222, 0, 'remote_addr', '10.0.1.170');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (223, 0, 'adphone', '962184762');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (223, 1, 'remote_addr', '10.0.1.170');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (224, 0, 'remote_addr', '10.0.1.170');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (225, 0, 'adphone', '962184762');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (225, 1, 'remote_addr', '10.0.1.170');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (226, 0, 'remote_addr', '10.0.1.170');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (227, 0, 'adphone', '962184762');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (227, 1, 'remote_addr', '10.0.1.170');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (228, 0, 'remote_addr', '10.0.1.170');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (229, 0, 'adphone', '962184762');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (229, 1, 'remote_addr', '10.0.1.170');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (230, 0, 'remote_addr', '10.0.1.170');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (231, 0, 'adphone', '962184762');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (231, 1, 'remote_addr', '10.0.1.170');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (232, 0, 'remote_addr', '10.0.1.170');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (233, 0, 'adphone', '62184762');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (233, 1, 'remote_addr', '10.0.1.170');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (234, 0, 'remote_addr', '10.0.1.170');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (235, 0, 'adphone', '62184762');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (235, 1, 'remote_addr', '10.0.1.170');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (236, 0, 'remote_addr', '10.0.1.170');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (237, 0, 'adphone', '62184762');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (237, 1, 'remote_addr', '10.0.1.170');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (238, 0, 'remote_addr', '10.0.1.170');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (239, 0, 'adphone', '62184762');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (239, 1, 'remote_addr', '10.0.1.170');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (240, 0, 'remote_addr', '10.0.1.170');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (241, 0, 'adphone', '62184762');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (241, 1, 'remote_addr', '10.0.1.170');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (242, 0, 'remote_addr', '10.0.1.170');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (243, 0, 'adphone', '62184762');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (243, 1, 'remote_addr', '10.0.1.170');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (244, 0, 'remote_addr', '10.0.1.170');

SELECT pg_catalog.setval('payment_groups_payment_group_id_seq', 181, true);

INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (166, 'ad_action', 0, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (167, 'ad_action', 0, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (168, 'ad_action', 0, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (169, 'ad_action', 0, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (170, 'ad_action', 0, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (171, 'ad_action', 0, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (172, 'ad_action', 0, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (173, 'ad_action', 0, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (174, 'ad_action', 0, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (175, 'ad_action', 0, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (176, 'ad_action', 0, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (177, 'ad_action', 0, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (178, 'ad_action', 0, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (179, 'ad_action', 0, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (180, 'ad_action', 0, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (181, 'ad_action', 0, 0);

INSERT INTO review_log (ad_id, action_id, admin_id, review_time, queue, action_type, action, refusal_reason_text, category) VALUES (126, 1, 9, '2015-05-28 09:51:04.58617', 'all', 'new', 'accepted', NULL, 5020);
INSERT INTO review_log (ad_id, action_id, admin_id, review_time, queue, action_type, action, refusal_reason_text, category) VALUES (127, 1, 9, '2015-05-28 09:59:08.217609', 'all', 'new', 'accept_ch', 'Foto principal movida', 7060);
INSERT INTO review_log (ad_id, action_id, admin_id, review_time, queue, action_type, action, refusal_reason_text, category) VALUES (128, 1, 9, '2015-05-28 10:01:26.994731', 'all', 'new', 'accept_ch', '6', 4040);
INSERT INTO review_log (ad_id, action_id, admin_id, review_time, queue, action_type, action, refusal_reason_text, category) VALUES (129, 1, 9, '2015-05-28 10:15:18.098606', 'all', 'new', 'accepted', NULL, 1040);
INSERT INTO review_log (ad_id, action_id, admin_id, review_time, queue, action_type, action, refusal_reason_text, category) VALUES (130, 1, 9, '2015-05-28 10:22:56.144317', 'all', 'new', 'accept_ch', 'Foto principal movida', 1060);
INSERT INTO review_log (ad_id, action_id, admin_id, review_time, queue, action_type, action, refusal_reason_text, category) VALUES (131, 1, 9, '2015-05-28 10:23:05.010654', 'all', 'new', 'accept_ch', 'Foto principal movida', 1080);
INSERT INTO review_log (ad_id, action_id, admin_id, review_time, queue, action_type, action, refusal_reason_text, category) VALUES (132, 1, 9, '2015-05-28 10:32:13.495143', 'all', 'new', 'accept_ch', 'Foto principal movida', 5020);
INSERT INTO review_log (ad_id, action_id, admin_id, review_time, queue, action_type, action, refusal_reason_text, category) VALUES (133, 1, 9, '2015-05-28 10:36:47.491486', 'all', 'new', 'accept_ch', 'Foto principal movida', 2060);
INSERT INTO review_log (ad_id, action_id, admin_id, review_time, queue, action_type, action, refusal_reason_text, category) VALUES (134, 1, 9, '2015-05-28 10:44:16.578315', 'all', 'new', 'accepted', NULL, 9040);
INSERT INTO review_log (ad_id, action_id, admin_id, review_time, queue, action_type, action, refusal_reason_text, category) VALUES (135, 1, 9, '2015-05-28 10:44:26.616185', 'all', 'new', 'accept_ch', 'Foto principal movida', 6020);
INSERT INTO review_log (ad_id, action_id, admin_id, review_time, queue, action_type, action, refusal_reason_text, category) VALUES (136, 1, 9, '2015-05-28 10:50:30.183213', 'all', 'new', 'accepted', NULL, 1080);
INSERT INTO review_log (ad_id, action_id, admin_id, review_time, queue, action_type, action, refusal_reason_text, category) VALUES (137, 1, 9, '2015-05-28 10:54:38.693566', 'all', 'new', 'accepted', NULL, 2080);
INSERT INTO review_log (ad_id, action_id, admin_id, review_time, queue, action_type, action, refusal_reason_text, category) VALUES (138, 1, 9, '2015-05-28 10:57:08.829112', 'all', 'new', 'accept_ch', 'Foto principal movida', 2020);
INSERT INTO review_log (ad_id, action_id, admin_id, review_time, queue, action_type, action, refusal_reason_text, category) VALUES (139, 1, 9, '2015-05-28 11:00:41.599527', 'all', 'new', 'accepted', NULL, 7020);
INSERT INTO review_log (ad_id, action_id, admin_id, review_time, queue, action_type, action, refusal_reason_text, category) VALUES (140, 1, 9, '2015-05-28 11:21:21.398089', 'all', 'new', 'accepted', NULL, 1020);
INSERT INTO review_log (ad_id, action_id, admin_id, review_time, queue, action_type, action, refusal_reason_text, category) VALUES (141, 1, 9, '2015-05-28 11:22:34.617492', 'all', 'new', 'accepted', NULL, 7020);

INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (126, 1, 700, 'queue', 'normal');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (126, 1, 701, 'filter_name', 'all');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (127, 1, 704, 'queue', 'normal');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (127, 1, 705, 'filter_name', 'all');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (128, 1, 708, 'queue', 'normal');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (128, 1, 709, 'filter_name', 'all');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (129, 1, 712, 'queue', 'normal');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (129, 1, 713, 'filter_name', 'all');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (130, 1, 716, 'queue', 'normal');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (131, 1, 719, 'queue', 'normal');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (130, 1, 720, 'filter_name', 'all');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (131, 1, 721, 'filter_name', 'all');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (132, 1, 724, 'queue', 'normal');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (132, 1, 725, 'filter_name', 'all');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (133, 1, 728, 'queue', 'normal');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (133, 1, 729, 'filter_name', 'all');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (134, 1, 732, 'queue', 'normal');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (135, 1, 735, 'queue', 'normal');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (134, 1, 736, 'filter_name', 'all');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (135, 1, 737, 'filter_name', 'all');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (136, 1, 740, 'queue', 'normal');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (136, 1, 741, 'filter_name', 'all');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (137, 1, 744, 'queue', 'normal');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (137, 1, 745, 'filter_name', 'all');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (138, 1, 748, 'queue', 'normal');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (138, 1, 749, 'filter_name', 'all');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (139, 1, 752, 'queue', 'normal');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (139, 1, 753, 'filter_name', 'all');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (140, 1, 756, 'queue', 'normal');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (141, 1, 759, 'queue', 'normal');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (140, 1, 760, 'filter_name', 'all');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (141, 1, 761, 'filter_name', 'all');

SELECT pg_catalog.setval('tokens_token_id_seq', 858, true);

INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (93, '2015-05-28 09:51:04.644253', '30819@kiyoko.schibsted.cl', NULL, '2015-05-28 09:51:32.840645', '30819@kiyoko.schibsted.cl', 'TRANS_OK', '2015-05-28 09:51:32.751401', NULL, 'cmd:admail
commit:1
ad_id:126
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (94, '2015-05-28 09:59:08.258791', '30819@kiyoko.schibsted.cl', NULL, '2015-05-28 09:59:20.017578', '30819@kiyoko.schibsted.cl', 'TRANS_OK', '2015-05-28 09:59:19.931688', NULL, 'cmd:admail
commit:1
ad_id:127
action_id:1
mail_type:accept_with_changes
reason:2
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (95, '2015-05-28 10:01:27.020398', '30819@kiyoko.schibsted.cl', NULL, '2015-05-28 10:02:10.062787', '30819@kiyoko.schibsted.cl', 'TRANS_OK', '2015-05-28 10:02:09.982476', NULL, 'cmd:admail
commit:1
ad_id:128
action_id:1
mail_type:accept_with_changes
reason:6
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (96, '2015-05-28 10:15:18.120318', '30819@kiyoko.schibsted.cl', NULL, '2015-05-28 10:15:26.586654', '30819@kiyoko.schibsted.cl', 'TRANS_OK', '2015-05-28 10:15:26.510868', NULL, 'cmd:admail
commit:1
ad_id:129
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (98, '2015-05-28 10:23:05.072871', '30819@kiyoko.schibsted.cl', NULL, '2015-05-28 11:01:01.610249', '30819@kiyoko.schibsted.cl', 'TRANS_OK', '2015-05-28 11:01:01.513575', NULL, 'cmd:admail
commit:1
ad_id:131
action_id:1
mail_type:accept_with_changes
reason:2
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (97, '2015-05-28 10:22:56.182446', '30819@kiyoko.schibsted.cl', NULL, '2015-05-28 11:01:01.610598', '30819@kiyoko.schibsted.cl', 'TRANS_OK', '2015-05-28 11:01:01.513575', NULL, 'cmd:admail
commit:1
ad_id:130
action_id:1
mail_type:accept_with_changes
reason:2
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (99, '2015-05-28 10:32:13.52391', '30819@kiyoko.schibsted.cl', NULL, '2015-05-28 11:01:01.657721', '30819@kiyoko.schibsted.cl', 'TRANS_OK', '2015-05-28 11:01:01.513575', NULL, 'cmd:admail
commit:1
ad_id:132
action_id:1
mail_type:accept_with_changes
reason:2
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (100, '2015-05-28 10:36:47.53264', '30819@kiyoko.schibsted.cl', NULL, '2015-05-28 11:01:01.664101', '30819@kiyoko.schibsted.cl', 'TRANS_OK', '2015-05-28 11:01:01.513575', NULL, 'cmd:admail
commit:1
ad_id:133
action_id:1
mail_type:accept_with_changes
reason:2
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (102, '2015-05-28 10:44:26.659995', '30819@kiyoko.schibsted.cl', NULL, '2015-05-28 11:01:01.672711', '30819@kiyoko.schibsted.cl', 'TRANS_OK', '2015-05-28 11:01:01.513575', NULL, 'cmd:admail
commit:1
ad_id:135
action_id:1
mail_type:accept_with_changes
reason:2
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (103, '2015-05-28 10:50:30.222239', '30819@kiyoko.schibsted.cl', NULL, '2015-05-28 11:01:01.685179', '30819@kiyoko.schibsted.cl', 'TRANS_OK', '2015-05-28 11:01:01.513575', NULL, 'cmd:admail
commit:1
ad_id:136
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (101, '2015-05-28 10:44:16.613715', '30819@kiyoko.schibsted.cl', NULL, '2015-05-28 11:01:01.684923', '30819@kiyoko.schibsted.cl', 'TRANS_OK', '2015-05-28 11:01:01.513575', NULL, 'cmd:admail
commit:1
ad_id:134
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (104, '2015-05-28 10:54:38.731033', '30819@kiyoko.schibsted.cl', NULL, '2015-05-28 11:01:01.697286', '30819@kiyoko.schibsted.cl', 'TRANS_OK', '2015-05-28 11:01:01.513575', NULL, 'cmd:admail
commit:1
ad_id:137
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (106, '2015-05-28 11:00:41.622448', '30819@kiyoko.schibsted.cl', NULL, '2015-05-28 11:01:01.697147', '30819@kiyoko.schibsted.cl', 'TRANS_OK', '2015-05-28 11:01:01.513575', NULL, 'cmd:admail
commit:1
ad_id:139
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (105, '2015-05-28 10:57:08.863774', '30819@kiyoko.schibsted.cl', NULL, '2015-05-28 11:01:01.697409', '30819@kiyoko.schibsted.cl', 'TRANS_OK', '2015-05-28 11:01:01.513575', NULL, 'cmd:admail
commit:1
ad_id:138
action_id:1
mail_type:accept_with_changes
reason:2
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (108, '2015-05-28 11:22:34.656961', '30819@kiyoko.schibsted.cl', NULL, '2015-05-28 11:23:13.837601', '30819@kiyoko.schibsted.cl', 'TRANS_OK', '2015-05-28 11:23:13.754401', NULL, 'cmd:admail
commit:1
ad_id:141
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (107, '2015-05-28 11:21:21.43202', '30819@kiyoko.schibsted.cl', NULL, '2015-05-28 11:23:13.837947', '30819@kiyoko.schibsted.cl', 'TRANS_OK', '2015-05-28 11:23:13.754401', NULL, 'cmd:admail
commit:1
ad_id:140
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);

SELECT pg_catalog.setval('trans_queue_id_seq', 108, true);

INSERT INTO user_params (user_id, name, value) VALUES (60, 'first_approved_ad', '2015-05-28 09:51:04.58617-04');
INSERT INTO user_params (user_id, name, value) VALUES (61, 'first_approved_ad', '2015-05-28 09:59:08.217609-04');
INSERT INTO user_params (user_id, name, value) VALUES (59, 'first_approved_ad', '2015-05-28 10:50:30.183213-04');
INSERT INTO user_params (user_id, name, value) VALUES (62, 'first_approved_ad', '2015-05-28 11:21:21.398089-04');

INSERT INTO user_params (user_id, name, value) VALUES (60, 'first_inserted_ad', '2015-05-28 09:51:04.58617-04');
INSERT INTO user_params (user_id, name, value) VALUES (61, 'first_inserted_ad', '2015-05-28 09:59:08.217609-04');
INSERT INTO user_params (user_id, name, value) VALUES (59, 'first_inserted_ad', '2015-05-28 10:50:30.183213-04');
INSERT INTO user_params (user_id, name, value) VALUES (62, 'first_inserted_ad', '2015-05-28 11:21:21.398089-04');

SELECT pg_catalog.setval('users_user_id_seq', 62, true);
--Migrate inmo ads
select * from bpv.migrate_ads_inmo();

--Change all passwords to 123123123
update ads set salted_passwd =  E'$1024$2y1bQ}VYRD}zF4QCbf07a971b784888658a20e62156de64dfde5ffb6', passwd = null;
update accounts set salted_passwd =  E'$1024$2y1bQ}VYRD}zF4QCbf07a971b784888658a20e62156de64dfde5ffb6';
