INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (166, '12345', 'paid', '2015-07-09 15:01:37.376944', NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (167, '65432', 'paid', '2015-07-09 15:01:37.376944', 166);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (168, '32323', 'paid', '2015-07-09 15:01:37.376944', 166);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (169, '54545', 'paid', '2015-07-09 15:01:37.376944', 166);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (1, 2, 'upselling_weekly_bump', 704, 'accepted', 'normal', NULL, NULL, 167);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (95, 2, 'upselling_gallery', 705, 'accepted', 'normal', NULL, NULL, 168);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (6, 2, 'upselling_daily_bump', 706, 'accepted', 'normal', NULL, NULL, 169);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (1, 2, 698, 'reg', 'initial', '2015-07-09 15:01:37.376944', '10.0.1.149', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (1, 2, 699, 'unpaid', 'pending_pay', '2015-07-09 15:01:37.376944', '10.0.1.149', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (95, 2, 700, 'reg', 'initial', '2015-07-09 15:01:37.376944', '10.0.1.149', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (95, 2, 701, 'unpaid', 'pending_pay', '2015-07-09 15:01:37.376944', '10.0.1.149', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (6, 2, 702, 'reg', 'initial', '2015-07-09 15:01:37.376944', '10.0.1.149', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (6, 2, 703, 'unpaid', 'pending_pay', '2015-07-09 15:01:37.376944', '10.0.1.149', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (1, 2, 704, 'accepted', 'pay', '2015-07-09 15:01:41.059627', NULL, NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (95, 2, 705, 'accepted', 'pay', '2015-07-09 15:01:41.088893', NULL, NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (6, 2, 706, 'accepted', 'pay', '2015-07-09 15:01:41.102988', NULL, NULL);
SELECT pg_catalog.setval('action_states_state_id_seq', 706, true);
INSERT INTO ad_changes (ad_id, action_id, state_id, is_param, column_name, old_value, new_value) VALUES (95, 2, 705, true, 'gallery', NULL, '2015-07-09 15:11:41');
INSERT INTO ad_params (ad_id, name, value) VALUES (95, 'gallery', '2015-07-09 15:11:41');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (213, 'save', 0, '65432', NULL, '2015-07-09 15:01:37', 167, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (214, 'save', 0, '32323', NULL, '2015-07-09 15:01:37', 168, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (215, 'save', 0, '54545', NULL, '2015-07-09 15:01:37', 169, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (216, 'paycard_save', 0, '12345', NULL, '2015-07-09 15:01:41', 166, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (217, 'paycard', 6900, '00167a', NULL, '2015-07-09 15:01:41', 167, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (218, 'paycard', 3600, '00168a', NULL, '2015-07-09 15:01:41', 168, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (219, 'paycard', 8000, '00169a', NULL, '2015-07-09 15:01:41', 169, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (220, 'paycard', 18500, '12345', NULL, '2015-07-09 15:01:41', 166, 'OK');
SELECT pg_catalog.setval('pay_log_pay_log_id_seq', 220, true);
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (213, 0, 'remote_addr', '10.0.1.149');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (214, 0, 'remote_addr', '10.0.1.149');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (215, 0, 'remote_addr', '10.0.1.149');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (216, 0, 'auth_code', '237963');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (216, 1, 'trans_date', '09/07/2015 15:01:39');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (216, 2, 'placements_type', 'VN');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (216, 3, 'placements_number', '1');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (216, 4, 'pay_type', 'credit');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (216, 5, 'external_response', '0');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (216, 6, 'external_id', '29011925694853908966');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (216, 7, 'remote_addr', '10.0.1.73');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (217, 0, 'webpay', '1');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (218, 0, 'webpay', '1');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (219, 0, 'webpay', '1');
SELECT pg_catalog.setval('payment_groups_payment_group_id_seq', 169, true);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (167, 'upselling_weekly_bump', 6900, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (168, 'upselling_gallery', 3600, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (169, 'upselling_daily_bump', 8000, 0);
INSERT INTO purchase (purchase_id, doc_type, doc_num, external_doc_id, status, receipt, delivery, rut, name, lob, address, region, communes, contact, email, tax, discount, seller_id, total_price, payment_group_id, payment_method, account_id) VALUES (2, 'invoice', NULL, NULL, 'paid', '2015-07-09 15:01:37.376944', NULL, '100000-4', 'BLOCKET', 'Technology', 'Alonso de C�rdova 5670', 15, 315, 'BLOCKET', 'prepaid5@blocket.se', 0, 0, NULL, 18500, 166, 'webpay', 1);
INSERT INTO purchase_detail (purchase_detail_id, purchase_id, product_id, price, action_id, ad_id, payment_group_id) VALUES (2, 2, 101, 6900, 2, 1, 167);
INSERT INTO purchase_detail (purchase_detail_id, purchase_id, product_id, price, action_id, ad_id, payment_group_id) VALUES (3, 2, 102, 3600, 2, 95, 168);
INSERT INTO purchase_detail (purchase_detail_id, purchase_id, product_id, price, action_id, ad_id, payment_group_id) VALUES (4, 2, 103, 8000, 2, 6, 169);
SELECT pg_catalog.setval('purchase_detail_purchase_detail_id_seq', 4, true);
SELECT pg_catalog.setval('purchase_purchase_id_seq', 2, true);
INSERT INTO purchase_states (purchase_id, purchase_state_id, status, "timestamp", remote_addr) VALUES (2, 1, 'pending', '2015-07-09 15:01:37.376944', '10.0.1.149');
SELECT pg_catalog.setval('purchase_states_purchase_state_id_seq', 1, true);
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (93, '2015-07-09 15:01:41.1638', '7101@miyako.schibsted.cl', NOW(), NULL, NULL, NULL, NULL, NULL, '
cmd:weekly_bump_ad
ad_id:6
action_id:2
counter:7
batch:1
daily_bump:1
do_not_send_mail:1
is_upselling:1
commit:1', NULL, 'upselling', NULL, NULL);
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (94, '2015-07-09 15:01:41.167071', '7101@miyako.schibsted.cl', NOW(), NULL, NULL, NULL, NULL, NULL, '
cmd:weekly_bump_ad
ad_id:1
action_id:2
counter:4
batch:1
do_not_send_mail:1
is_upselling:1
commit:1', NULL, 'upselling', NULL, NULL);
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (95, '2015-07-09 15:01:41.192533', '7101@miyako.schibsted.cl', NULL, NULL, NULL, NULL, NULL, NULL, '
cmd:multi_product_mail
to:prepaid5@blocket.se
doc_type:invoice
product_ids:103|101|102
ad_names:Race car|Departamento Tarapac�|Adeptus Astartes Codex
commit:1', NULL, 'bump_mail', NULL, NULL);
SELECT pg_catalog.setval('trans_queue_id_seq', 95, true);

--Change all passwords to 123123123
update ads set salted_passwd =  E'$1024$2y1bQ}VYRD}zF4QCbf07a971b784888658a20e62156de64dfde5ffb6', passwd = null;
update accounts set salted_passwd =  E'$1024$2y1bQ}VYRD}zF4QCbf07a971b784888658a20e62156de64dfde5ffb6';
