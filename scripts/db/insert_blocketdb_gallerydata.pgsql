--
-- PostgreSQL database dump
--

BEGIN;
SET CONSTRAINTS ALL DEFERRED;

TRUNCATE TABLE public.admins CASCADE;
TRUNCATE TABLE public.abuse_locks CASCADE;
TRUNCATE TABLE public.abuse_reporters CASCADE;
TRUNCATE TABLE public.users CASCADE;
TRUNCATE TABLE public.abuse_reports CASCADE;
TRUNCATE TABLE public.ads CASCADE;
TRUNCATE TABLE public.dashboard_ads CASCADE;
TRUNCATE TABLE public.payment_groups CASCADE;
TRUNCATE TABLE public.ad_actions CASCADE;
TRUNCATE TABLE public.action_params CASCADE;
TRUNCATE TABLE public.stores CASCADE;
TRUNCATE TABLE public.tokens CASCADE;
TRUNCATE TABLE public.action_states CASCADE;
TRUNCATE TABLE public.ad_changes CASCADE;
TRUNCATE TABLE public.ad_codes CASCADE;
TRUNCATE TABLE public.ad_image_changes CASCADE;
TRUNCATE TABLE public.ad_images CASCADE;
TRUNCATE TABLE public.ad_images_digests CASCADE;
TRUNCATE TABLE public.ad_media CASCADE;
TRUNCATE TABLE public.ad_media_changes CASCADE;
TRUNCATE TABLE public.ad_params CASCADE;
TRUNCATE TABLE public.ad_queues CASCADE;
TRUNCATE TABLE public.admin_privs CASCADE;
TRUNCATE TABLE public.bid_ads CASCADE;
TRUNCATE TABLE public.bid_bids CASCADE;
TRUNCATE TABLE public.bid_media CASCADE;
TRUNCATE TABLE public.block_lists CASCADE;
TRUNCATE TABLE public.block_rules CASCADE;
TRUNCATE TABLE public.block_rule_conditions CASCADE;
TRUNCATE TABLE public.blocked_items CASCADE;
TRUNCATE TABLE public.conf CASCADE;
TRUNCATE TABLE public.event_log CASCADE;
TRUNCATE TABLE public.example CASCADE;
TRUNCATE TABLE public.filters CASCADE;
TRUNCATE TABLE public.hold_mail_params CASCADE;
TRUNCATE TABLE public.iteminfo_items CASCADE;
TRUNCATE TABLE public.iteminfo_data CASCADE;
TRUNCATE TABLE public.mail_log CASCADE;
TRUNCATE TABLE public.mail_queue CASCADE;
TRUNCATE TABLE public.mama_attribute_wordlists CASCADE;
TRUNCATE TABLE public.mama_attribute_categories CASCADE;
TRUNCATE TABLE public.mama_main_backup CASCADE;
TRUNCATE TABLE public.mama_attribute_categories_backup CASCADE;
TRUNCATE TABLE public.mama_attribute_wordlists_backup CASCADE;
TRUNCATE TABLE public.mama_attribute_words CASCADE;
TRUNCATE TABLE public.mama_attribute_words_backup CASCADE;
TRUNCATE TABLE public.mama_exception_lists CASCADE;
TRUNCATE TABLE public.mama_exception_lists_backup CASCADE;
TRUNCATE TABLE public.mama_exception_words CASCADE;
TRUNCATE TABLE public.mama_exception_words_backup CASCADE;
TRUNCATE TABLE public.mama_wordlists CASCADE;
TRUNCATE TABLE public.mama_wordlists_backup CASCADE;
TRUNCATE TABLE public.mama_words CASCADE;
TRUNCATE TABLE public.mama_words_backup CASCADE;
TRUNCATE TABLE public.most_popular_ads CASCADE;
TRUNCATE TABLE public.notices CASCADE;
TRUNCATE TABLE public.on_call CASCADE;
TRUNCATE TABLE public.on_call_actions CASCADE;
TRUNCATE TABLE public.pageviews_per_reg_cat CASCADE;
TRUNCATE TABLE public.pay_log CASCADE;
TRUNCATE TABLE public.pay_log_references CASCADE;
TRUNCATE TABLE public.payments CASCADE;
TRUNCATE TABLE public.purchase CASCADE;
TRUNCATE TABLE public.pricelist CASCADE;
TRUNCATE TABLE public.redir_stats CASCADE;
TRUNCATE TABLE public.review_log CASCADE;
TRUNCATE TABLE public.sms_users CASCADE;
TRUNCATE TABLE public.sms_log CASCADE;
TRUNCATE TABLE public.watch_users CASCADE;
TRUNCATE TABLE public.watch_queries CASCADE;
TRUNCATE TABLE public.sms_log_watch CASCADE;
TRUNCATE TABLE public.state_params CASCADE;
TRUNCATE TABLE public.stats_daily CASCADE;
TRUNCATE TABLE public.stats_daily_ad_actions CASCADE;
TRUNCATE TABLE public.stats_hourly CASCADE;
TRUNCATE TABLE public.store_actions CASCADE;
TRUNCATE TABLE public.store_action_states CASCADE;
TRUNCATE TABLE public.store_changes CASCADE;
TRUNCATE TABLE public.store_login_tokens CASCADE;
TRUNCATE TABLE public.store_params CASCADE;
TRUNCATE TABLE public.synonyms CASCADE;
TRUNCATE TABLE public.trans_queue CASCADE;
TRUNCATE TABLE public.unfinished_ads CASCADE;
TRUNCATE TABLE public.user_params CASCADE;
TRUNCATE TABLE public.user_testimonial CASCADE;
TRUNCATE TABLE public.visitor CASCADE;
TRUNCATE TABLE public.visits CASCADE;
TRUNCATE TABLE public.vouchers CASCADE;
TRUNCATE TABLE public.voucher_actions CASCADE;
TRUNCATE TABLE public.voucher_states CASCADE;
TRUNCATE TABLE public.watch_ads CASCADE;
TRUNCATE TABLE public.accounts CASCADE;
TRUNCATE TABLE public.account_params CASCADE;
TRUNCATE TABLE public.packs CASCADE;
TRUNCATE TABLE public.refunds CASCADE;
TRUNCATE TABLE blocket_2015.stats CASCADE;

COMMIT;
--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET client_encoding = 'LATIN1';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;

SET search_path = public, pg_catalog;

--
-- Name: mail_queue_mail_queue_id_seq; Type: SEQUENCE SET; Schema: public; Owner: joaquin
--

SELECT pg_catalog.setval('mail_queue_mail_queue_id_seq', 1, false);


--
-- Name: abuse_locks_id_seq; Type: SEQUENCE SET; Schema: public; Owner: joaquin
--

SELECT pg_catalog.setval('abuse_locks_id_seq', 1, false);


--
-- Name: report_id_seq; Type: SEQUENCE SET; Schema: public; Owner: joaquin
--

SELECT pg_catalog.setval('report_id_seq', 1, false);


--
-- Name: accounts_account_id_seq; Type: SEQUENCE SET; Schema: public; Owner: joaquin
--

SELECT pg_catalog.setval('accounts_account_id_seq', 1, false);


--
-- Name: action_states_state_id_seq; Type: SEQUENCE SET; Schema: public; Owner: joaquin
--

SELECT pg_catalog.setval('action_states_state_id_seq', 318, true);


--
-- Name: admins_admin_id_seq; Type: SEQUENCE SET; Schema: public; Owner: joaquin
--

SELECT pg_catalog.setval('admins_admin_id_seq', 22, true);


--
-- Name: ads_ad_id_seq; Type: SEQUENCE SET; Schema: public; Owner: joaquin
--

SELECT pg_catalog.setval('ads_ad_id_seq', 36, true);


--
-- Name: adwatch_code_seq; Type: SEQUENCE SET; Schema: public; Owner: joaquin
--

SELECT pg_catalog.setval('adwatch_code_seq', 1, false);


--
-- Name: bid_ads_bid_ad_id_seq; Type: SEQUENCE SET; Schema: public; Owner: joaquin
--

SELECT pg_catalog.setval('bid_ads_bid_ad_id_seq', 1, false);


--
-- Name: bid_bids_bid_id_seq; Type: SEQUENCE SET; Schema: public; Owner: joaquin
--

SELECT pg_catalog.setval('bid_bids_bid_id_seq', 1, false);


--
-- Name: block_lists_list_id_seq; Type: SEQUENCE SET; Schema: public; Owner: joaquin
--

SELECT pg_catalog.setval('block_lists_list_id_seq', 100, false);


--
-- Name: block_rule_conditions_condition_id_seq; Type: SEQUENCE SET; Schema: public; Owner: joaquin
--

SELECT pg_catalog.setval('block_rule_conditions_condition_id_seq', 18, true);


--
-- Name: block_rules_rule_id_seq; Type: SEQUENCE SET; Schema: public; Owner: joaquin
--

SELECT pg_catalog.setval('block_rules_rule_id_seq', 100, false);


--
-- Name: blocked_items_item_id_seq; Type: SEQUENCE SET; Schema: public; Owner: joaquin
--

SELECT pg_catalog.setval('blocked_items_item_id_seq', 12, true);


--
-- Name: data_id_seq; Type: SEQUENCE SET; Schema: public; Owner: joaquin
--

SELECT pg_catalog.setval('data_id_seq', 1294, true);


--
-- Name: event_log_event_id_seq; Type: SEQUENCE SET; Schema: public; Owner: joaquin
--

SELECT pg_catalog.setval('event_log_event_id_seq', 1, false);


--
-- Name: filters_filter_id_seq; Type: SEQUENCE SET; Schema: public; Owner: joaquin
--

SELECT pg_catalog.setval('filters_filter_id_seq', 1, false);


--
-- Name: hold_mail_params_id_seq; Type: SEQUENCE SET; Schema: public; Owner: joaquin
--

SELECT pg_catalog.setval('hold_mail_params_id_seq', 1, false);


--
-- Name: item_id_seq; Type: SEQUENCE SET; Schema: public; Owner: joaquin
--

SELECT pg_catalog.setval('item_id_seq', 59, true);


--
-- Name: list_id_seq; Type: SEQUENCE SET; Schema: public; Owner: joaquin
--

SELECT pg_catalog.setval('list_id_seq', 8000016, true);


--
-- Name: mail_log_mail_log_id_seq; Type: SEQUENCE SET; Schema: public; Owner: joaquin
--

SELECT pg_catalog.setval('mail_log_mail_log_id_seq', 1, false);


--
-- Name: mama_attribute_wordlists_attribute_wordlist_id_seq; Type: SEQUENCE SET; Schema: public; Owner: joaquin
--

SELECT pg_catalog.setval('mama_attribute_wordlists_attribute_wordlist_id_seq', 1, false);


--
-- Name: mama_attribute_words_word_id_seq; Type: SEQUENCE SET; Schema: public; Owner: joaquin
--

SELECT pg_catalog.setval('mama_attribute_words_word_id_seq', 1, false);


--
-- Name: mama_exception_lists_id_seq; Type: SEQUENCE SET; Schema: public; Owner: joaquin
--

SELECT pg_catalog.setval('mama_exception_lists_id_seq', 1, false);


--
-- Name: mama_exception_words_exception_id_seq; Type: SEQUENCE SET; Schema: public; Owner: joaquin
--

SELECT pg_catalog.setval('mama_exception_words_exception_id_seq', 1, false);


--
-- Name: mama_main_backup_backup_id_seq; Type: SEQUENCE SET; Schema: public; Owner: joaquin
--

SELECT pg_catalog.setval('mama_main_backup_backup_id_seq', 1, false);


--
-- Name: mama_wordlists_wordlist_id_seq; Type: SEQUENCE SET; Schema: public; Owner: joaquin
--

SELECT pg_catalog.setval('mama_wordlists_wordlist_id_seq', 1, false);


--
-- Name: mama_words_word_id_seq; Type: SEQUENCE SET; Schema: public; Owner: joaquin
--

SELECT pg_catalog.setval('mama_words_word_id_seq', 1, false);


--
-- Name: most_popular_ads_id_seq; Type: SEQUENCE SET; Schema: public; Owner: joaquin
--

SELECT pg_catalog.setval('most_popular_ads_id_seq', 1, false);


--
-- Name: next_image_id; Type: SEQUENCE SET; Schema: public; Owner: joaquin
--

SELECT pg_catalog.setval('next_image_id', 1, true);


--
-- Name: notices_notice_id_seq; Type: SEQUENCE SET; Schema: public; Owner: joaquin
--

SELECT pg_catalog.setval('notices_notice_id_seq', 1, false);


--
-- Name: on_call_actions_action_id_seq; Type: SEQUENCE SET; Schema: public; Owner: joaquin
--

SELECT pg_catalog.setval('on_call_actions_action_id_seq', 1, false);


--
-- Name: on_call_on_call_id_seq; Type: SEQUENCE SET; Schema: public; Owner: joaquin
--

SELECT pg_catalog.setval('on_call_on_call_id_seq', 1, false);


--
-- Name: order_id_suffix_seq; Type: SEQUENCE SET; Schema: public; Owner: joaquin
--

SELECT pg_catalog.setval('order_id_suffix_seq', 1, false);


--
-- Name: pay_code_seq; Type: SEQUENCE SET; Schema: public; Owner: joaquin
--

SELECT pg_catalog.setval('pay_code_seq', 73, true);


--
-- Name: pay_log_pay_log_id_seq; Type: SEQUENCE SET; Schema: public; Owner: joaquin
--

SELECT pg_catalog.setval('pay_log_pay_log_id_seq', 34, true);


--
-- Name: payment_groups_payment_group_id_seq; Type: SEQUENCE SET; Schema: public; Owner: joaquin
--

SELECT pg_catalog.setval('payment_groups_payment_group_id_seq', 76, true);


--
-- Name: purchase_detail_purchase_detail_id_seq; Type: SEQUENCE SET; Schema: public; Owner: joaquin
--

SELECT pg_catalog.setval('purchase_detail_purchase_detail_id_seq', 1, false);


--
-- Name: purchase_purchase_id_seq; Type: SEQUENCE SET; Schema: public; Owner: joaquin
--

SELECT pg_catalog.setval('purchase_purchase_id_seq', 1, false);


--
-- Name: purchase_states_purchase_state_id_seq; Type: SEQUENCE SET; Schema: public; Owner: joaquin
--

SELECT pg_catalog.setval('purchase_states_purchase_state_id_seq', 1, false);


--
-- Name: redir_stats_id_seq; Type: SEQUENCE SET; Schema: public; Owner: joaquin
--

SELECT pg_catalog.setval('redir_stats_id_seq', 1, false);


--
-- Name: reporter_id_seq; Type: SEQUENCE SET; Schema: public; Owner: joaquin
--

SELECT pg_catalog.setval('reporter_id_seq', 1, false);


--
-- Name: sms_log_sms_log_id_seq; Type: SEQUENCE SET; Schema: public; Owner: joaquin
--

SELECT pg_catalog.setval('sms_log_sms_log_id_seq', 1, false);


--
-- Name: sms_users_sms_user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: joaquin
--

SELECT pg_catalog.setval('sms_users_sms_user_id_seq', 1, false);


--
-- Name: stats_daily_ad_actions_stat_id_seq; Type: SEQUENCE SET; Schema: public; Owner: joaquin
--

SELECT pg_catalog.setval('stats_daily_ad_actions_stat_id_seq', 1, false);


--
-- Name: store_action_states_state_id_seq; Type: SEQUENCE SET; Schema: public; Owner: joaquin
--

SELECT pg_catalog.setval('store_action_states_state_id_seq', 1, false);


--
-- Name: store_login_tokens_store_login_token_id_seq; Type: SEQUENCE SET; Schema: public; Owner: joaquin
--

SELECT pg_catalog.setval('store_login_tokens_store_login_token_id_seq', 1, false);


--
-- Name: stores_store_id_seq; Type: SEQUENCE SET; Schema: public; Owner: joaquin
--

SELECT pg_catalog.setval('stores_store_id_seq', 1, false);


--
-- Name: synonyms_syn_id_seq; Type: SEQUENCE SET; Schema: public; Owner: joaquin
--

SELECT pg_catalog.setval('synonyms_syn_id_seq', 245, true);


--
-- Name: tokens_token_id_seq; Type: SEQUENCE SET; Schema: public; Owner: joaquin
--

SELECT pg_catalog.setval('tokens_token_id_seq', 87, true);


--
-- Name: trans_queue_id_seq; Type: SEQUENCE SET; Schema: public; Owner: joaquin
--

SELECT pg_catalog.setval('trans_queue_id_seq', 17, true);


--
-- Name: uid_seq; Type: SEQUENCE SET; Schema: public; Owner: joaquin
--

SELECT pg_catalog.setval('uid_seq', 50, true);


--
-- Name: unfinished_ads_unf_ad_id_seq; Type: SEQUENCE SET; Schema: public; Owner: joaquin
--

SELECT pg_catalog.setval('unfinished_ads_unf_ad_id_seq', 1, false);


--
-- Name: user_testimonial_user_testimonial_id_seq; Type: SEQUENCE SET; Schema: public; Owner: joaquin
--

SELECT pg_catalog.setval('user_testimonial_user_testimonial_id_seq', 1, false);


--
-- Name: users_user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: joaquin
--

SELECT pg_catalog.setval('users_user_id_seq', 54, true);


--
-- Name: verify_code_seq; Type: SEQUENCE SET; Schema: public; Owner: joaquin
--

SELECT pg_catalog.setval('verify_code_seq', 90, true);


--
-- Name: voucher_actions_voucher_action_id_seq; Type: SEQUENCE SET; Schema: public; Owner: joaquin
--

SELECT pg_catalog.setval('voucher_actions_voucher_action_id_seq', 1, false);


--
-- Name: voucher_states_voucher_state_id_seq; Type: SEQUENCE SET; Schema: public; Owner: joaquin
--

SELECT pg_catalog.setval('voucher_states_voucher_state_id_seq', 1, false);


--
-- Name: vouchers_voucher_id_seq; Type: SEQUENCE SET; Schema: public; Owner: joaquin
--

SELECT pg_catalog.setval('vouchers_voucher_id_seq', 1, false);


--
-- Name: watch_users_watch_user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: joaquin
--

SELECT pg_catalog.setval('watch_users_watch_user_id_seq', 1, false);


SET search_path = blocket_2009, pg_catalog;

--
-- Data for Name: ads; Type: TABLE DATA; Schema: blocket_2009; Owner: joaquin
--



--
-- Data for Name: payment_groups; Type: TABLE DATA; Schema: blocket_2009; Owner: joaquin
--



--
-- Data for Name: ad_actions; Type: TABLE DATA; Schema: blocket_2009; Owner: joaquin
--



--
-- Data for Name: action_params; Type: TABLE DATA; Schema: blocket_2009; Owner: joaquin
--



--
-- Data for Name: tokens; Type: TABLE DATA; Schema: blocket_2009; Owner: joaquin
--



--
-- Data for Name: action_states; Type: TABLE DATA; Schema: blocket_2009; Owner: joaquin
--



--
-- Data for Name: ad_changes; Type: TABLE DATA; Schema: blocket_2009; Owner: joaquin
--



--
-- Data for Name: ad_image_changes; Type: TABLE DATA; Schema: blocket_2009; Owner: joaquin
--



--
-- Data for Name: ad_images; Type: TABLE DATA; Schema: blocket_2009; Owner: joaquin
--



--
-- Data for Name: ad_media; Type: TABLE DATA; Schema: blocket_2009; Owner: joaquin
--



--
-- Data for Name: ad_media_changes; Type: TABLE DATA; Schema: blocket_2009; Owner: joaquin
--



--
-- Data for Name: ad_params; Type: TABLE DATA; Schema: blocket_2009; Owner: joaquin
--



--
-- Data for Name: mail_queue; Type: TABLE DATA; Schema: blocket_2009; Owner: joaquin
--



--
-- Data for Name: notices; Type: TABLE DATA; Schema: blocket_2009; Owner: joaquin
--



--
-- Data for Name: pay_log; Type: TABLE DATA; Schema: blocket_2009; Owner: joaquin
--



--
-- Data for Name: pay_log_references; Type: TABLE DATA; Schema: blocket_2009; Owner: joaquin
--



--
-- Data for Name: payments; Type: TABLE DATA; Schema: blocket_2009; Owner: joaquin
--



--
-- Data for Name: purchase; Type: TABLE DATA; Schema: blocket_2009; Owner: joaquin
--



--
-- Data for Name: purchase_detail; Type: TABLE DATA; Schema: blocket_2009; Owner: joaquin
--



--
-- Data for Name: purchase_states; Type: TABLE DATA; Schema: blocket_2009; Owner: joaquin
--



--
-- Data for Name: state_params; Type: TABLE DATA; Schema: blocket_2009; Owner: joaquin
--



--
-- Data for Name: voucher_actions; Type: TABLE DATA; Schema: blocket_2009; Owner: joaquin
--



--
-- Data for Name: voucher_states; Type: TABLE DATA; Schema: blocket_2009; Owner: joaquin
--



SET search_path = blocket_2010, pg_catalog;

--
-- Data for Name: ads; Type: TABLE DATA; Schema: blocket_2010; Owner: joaquin
--



--
-- Data for Name: payment_groups; Type: TABLE DATA; Schema: blocket_2010; Owner: joaquin
--



--
-- Data for Name: ad_actions; Type: TABLE DATA; Schema: blocket_2010; Owner: joaquin
--



--
-- Data for Name: action_params; Type: TABLE DATA; Schema: blocket_2010; Owner: joaquin
--



--
-- Data for Name: tokens; Type: TABLE DATA; Schema: blocket_2010; Owner: joaquin
--



--
-- Data for Name: action_states; Type: TABLE DATA; Schema: blocket_2010; Owner: joaquin
--



--
-- Data for Name: ad_changes; Type: TABLE DATA; Schema: blocket_2010; Owner: joaquin
--



--
-- Data for Name: ad_image_changes; Type: TABLE DATA; Schema: blocket_2010; Owner: joaquin
--



--
-- Data for Name: ad_images; Type: TABLE DATA; Schema: blocket_2010; Owner: joaquin
--



--
-- Data for Name: ad_media; Type: TABLE DATA; Schema: blocket_2010; Owner: joaquin
--



--
-- Data for Name: ad_media_changes; Type: TABLE DATA; Schema: blocket_2010; Owner: joaquin
--



--
-- Data for Name: ad_params; Type: TABLE DATA; Schema: blocket_2010; Owner: joaquin
--



--
-- Data for Name: mail_queue; Type: TABLE DATA; Schema: blocket_2010; Owner: joaquin
--



--
-- Data for Name: notices; Type: TABLE DATA; Schema: blocket_2010; Owner: joaquin
--



--
-- Data for Name: pay_log; Type: TABLE DATA; Schema: blocket_2010; Owner: joaquin
--



--
-- Data for Name: pay_log_references; Type: TABLE DATA; Schema: blocket_2010; Owner: joaquin
--



--
-- Data for Name: payments; Type: TABLE DATA; Schema: blocket_2010; Owner: joaquin
--



--
-- Data for Name: purchase; Type: TABLE DATA; Schema: blocket_2010; Owner: joaquin
--



--
-- Data for Name: purchase_detail; Type: TABLE DATA; Schema: blocket_2010; Owner: joaquin
--



--
-- Data for Name: purchase_states; Type: TABLE DATA; Schema: blocket_2010; Owner: joaquin
--



--
-- Data for Name: state_params; Type: TABLE DATA; Schema: blocket_2010; Owner: joaquin
--



--
-- Data for Name: voucher_actions; Type: TABLE DATA; Schema: blocket_2010; Owner: joaquin
--



--
-- Data for Name: voucher_states; Type: TABLE DATA; Schema: blocket_2010; Owner: joaquin
--



SET search_path = blocket_2011, pg_catalog;

--
-- Data for Name: ads; Type: TABLE DATA; Schema: blocket_2011; Owner: joaquin
--



--
-- Data for Name: payment_groups; Type: TABLE DATA; Schema: blocket_2011; Owner: joaquin
--



--
-- Data for Name: ad_actions; Type: TABLE DATA; Schema: blocket_2011; Owner: joaquin
--



--
-- Data for Name: action_params; Type: TABLE DATA; Schema: blocket_2011; Owner: joaquin
--



--
-- Data for Name: tokens; Type: TABLE DATA; Schema: blocket_2011; Owner: joaquin
--



--
-- Data for Name: action_states; Type: TABLE DATA; Schema: blocket_2011; Owner: joaquin
--



--
-- Data for Name: ad_changes; Type: TABLE DATA; Schema: blocket_2011; Owner: joaquin
--



--
-- Data for Name: ad_image_changes; Type: TABLE DATA; Schema: blocket_2011; Owner: joaquin
--



--
-- Data for Name: ad_images; Type: TABLE DATA; Schema: blocket_2011; Owner: joaquin
--



--
-- Data for Name: ad_media; Type: TABLE DATA; Schema: blocket_2011; Owner: joaquin
--



--
-- Data for Name: ad_media_changes; Type: TABLE DATA; Schema: blocket_2011; Owner: joaquin
--



--
-- Data for Name: ad_params; Type: TABLE DATA; Schema: blocket_2011; Owner: joaquin
--



--
-- Data for Name: mail_queue; Type: TABLE DATA; Schema: blocket_2011; Owner: joaquin
--



--
-- Data for Name: notices; Type: TABLE DATA; Schema: blocket_2011; Owner: joaquin
--



--
-- Data for Name: pay_log; Type: TABLE DATA; Schema: blocket_2011; Owner: joaquin
--



--
-- Data for Name: pay_log_references; Type: TABLE DATA; Schema: blocket_2011; Owner: joaquin
--



--
-- Data for Name: payments; Type: TABLE DATA; Schema: blocket_2011; Owner: joaquin
--



--
-- Data for Name: purchase; Type: TABLE DATA; Schema: blocket_2011; Owner: joaquin
--



--
-- Data for Name: purchase_detail; Type: TABLE DATA; Schema: blocket_2011; Owner: joaquin
--



--
-- Data for Name: purchase_states; Type: TABLE DATA; Schema: blocket_2011; Owner: joaquin
--



--
-- Data for Name: state_params; Type: TABLE DATA; Schema: blocket_2011; Owner: joaquin
--



--
-- Data for Name: voucher_actions; Type: TABLE DATA; Schema: blocket_2011; Owner: joaquin
--



--
-- Data for Name: voucher_states; Type: TABLE DATA; Schema: blocket_2011; Owner: joaquin
--



SET search_path = blocket_2012, pg_catalog;

--
-- Data for Name: ads; Type: TABLE DATA; Schema: blocket_2012; Owner: joaquin
--



--
-- Data for Name: payment_groups; Type: TABLE DATA; Schema: blocket_2012; Owner: joaquin
--



--
-- Data for Name: ad_actions; Type: TABLE DATA; Schema: blocket_2012; Owner: joaquin
--



--
-- Data for Name: action_params; Type: TABLE DATA; Schema: blocket_2012; Owner: joaquin
--



--
-- Data for Name: tokens; Type: TABLE DATA; Schema: blocket_2012; Owner: joaquin
--



--
-- Data for Name: action_states; Type: TABLE DATA; Schema: blocket_2012; Owner: joaquin
--



--
-- Data for Name: ad_changes; Type: TABLE DATA; Schema: blocket_2012; Owner: joaquin
--



--
-- Data for Name: ad_image_changes; Type: TABLE DATA; Schema: blocket_2012; Owner: joaquin
--



--
-- Data for Name: ad_images; Type: TABLE DATA; Schema: blocket_2012; Owner: joaquin
--



--
-- Data for Name: ad_media; Type: TABLE DATA; Schema: blocket_2012; Owner: joaquin
--



--
-- Data for Name: ad_media_changes; Type: TABLE DATA; Schema: blocket_2012; Owner: joaquin
--



--
-- Data for Name: ad_params; Type: TABLE DATA; Schema: blocket_2012; Owner: joaquin
--



--
-- Data for Name: mail_queue; Type: TABLE DATA; Schema: blocket_2012; Owner: joaquin
--



--
-- Data for Name: notices; Type: TABLE DATA; Schema: blocket_2012; Owner: joaquin
--



--
-- Data for Name: pay_log; Type: TABLE DATA; Schema: blocket_2012; Owner: joaquin
--



--
-- Data for Name: pay_log_references; Type: TABLE DATA; Schema: blocket_2012; Owner: joaquin
--



--
-- Data for Name: payments; Type: TABLE DATA; Schema: blocket_2012; Owner: joaquin
--



--
-- Data for Name: purchase; Type: TABLE DATA; Schema: blocket_2012; Owner: joaquin
--



--
-- Data for Name: purchase_detail; Type: TABLE DATA; Schema: blocket_2012; Owner: joaquin
--



--
-- Data for Name: purchase_states; Type: TABLE DATA; Schema: blocket_2012; Owner: joaquin
--



--
-- Data for Name: state_params; Type: TABLE DATA; Schema: blocket_2012; Owner: joaquin
--



--
-- Data for Name: voucher_actions; Type: TABLE DATA; Schema: blocket_2012; Owner: joaquin
--



--
-- Data for Name: voucher_states; Type: TABLE DATA; Schema: blocket_2012; Owner: joaquin
--



SET search_path = blocket_2013, pg_catalog;

--
-- Data for Name: ads; Type: TABLE DATA; Schema: blocket_2013; Owner: joaquin
--



--
-- Data for Name: payment_groups; Type: TABLE DATA; Schema: blocket_2013; Owner: joaquin
--



--
-- Data for Name: ad_actions; Type: TABLE DATA; Schema: blocket_2013; Owner: joaquin
--



--
-- Data for Name: action_params; Type: TABLE DATA; Schema: blocket_2013; Owner: joaquin
--



--
-- Data for Name: tokens; Type: TABLE DATA; Schema: blocket_2013; Owner: joaquin
--



--
-- Data for Name: action_states; Type: TABLE DATA; Schema: blocket_2013; Owner: joaquin
--



--
-- Data for Name: ad_changes; Type: TABLE DATA; Schema: blocket_2013; Owner: joaquin
--



--
-- Data for Name: ad_image_changes; Type: TABLE DATA; Schema: blocket_2013; Owner: joaquin
--



--
-- Data for Name: ad_images; Type: TABLE DATA; Schema: blocket_2013; Owner: joaquin
--



--
-- Data for Name: ad_media; Type: TABLE DATA; Schema: blocket_2013; Owner: joaquin
--



--
-- Data for Name: ad_media_changes; Type: TABLE DATA; Schema: blocket_2013; Owner: joaquin
--



--
-- Data for Name: ad_params; Type: TABLE DATA; Schema: blocket_2013; Owner: joaquin
--



--
-- Data for Name: mail_queue; Type: TABLE DATA; Schema: blocket_2013; Owner: joaquin
--



--
-- Data for Name: notices; Type: TABLE DATA; Schema: blocket_2013; Owner: joaquin
--



--
-- Data for Name: pay_log; Type: TABLE DATA; Schema: blocket_2013; Owner: joaquin
--



--
-- Data for Name: pay_log_references; Type: TABLE DATA; Schema: blocket_2013; Owner: joaquin
--



--
-- Data for Name: payments; Type: TABLE DATA; Schema: blocket_2013; Owner: joaquin
--



--
-- Data for Name: purchase; Type: TABLE DATA; Schema: blocket_2013; Owner: joaquin
--



--
-- Data for Name: purchase_detail; Type: TABLE DATA; Schema: blocket_2013; Owner: joaquin
--



--
-- Data for Name: purchase_states; Type: TABLE DATA; Schema: blocket_2013; Owner: joaquin
--



--
-- Data for Name: state_params; Type: TABLE DATA; Schema: blocket_2013; Owner: joaquin
--



--
-- Data for Name: voucher_actions; Type: TABLE DATA; Schema: blocket_2013; Owner: joaquin
--



--
-- Data for Name: voucher_states; Type: TABLE DATA; Schema: blocket_2013; Owner: joaquin
--



SET search_path = blocket_2014, pg_catalog;

--
-- Data for Name: ads; Type: TABLE DATA; Schema: blocket_2014; Owner: joaquin
--



--
-- Data for Name: payment_groups; Type: TABLE DATA; Schema: blocket_2014; Owner: joaquin
--



--
-- Data for Name: ad_actions; Type: TABLE DATA; Schema: blocket_2014; Owner: joaquin
--



--
-- Data for Name: action_params; Type: TABLE DATA; Schema: blocket_2014; Owner: joaquin
--



--
-- Data for Name: tokens; Type: TABLE DATA; Schema: blocket_2014; Owner: joaquin
--



--
-- Data for Name: action_states; Type: TABLE DATA; Schema: blocket_2014; Owner: joaquin
--



--
-- Data for Name: ad_changes; Type: TABLE DATA; Schema: blocket_2014; Owner: joaquin
--



--
-- Data for Name: ad_image_changes; Type: TABLE DATA; Schema: blocket_2014; Owner: joaquin
--



--
-- Data for Name: ad_images; Type: TABLE DATA; Schema: blocket_2014; Owner: joaquin
--



--
-- Data for Name: ad_media; Type: TABLE DATA; Schema: blocket_2014; Owner: joaquin
--



--
-- Data for Name: ad_media_changes; Type: TABLE DATA; Schema: blocket_2014; Owner: joaquin
--



--
-- Data for Name: ad_params; Type: TABLE DATA; Schema: blocket_2014; Owner: joaquin
--



--
-- Data for Name: mail_queue; Type: TABLE DATA; Schema: blocket_2014; Owner: joaquin
--



--
-- Data for Name: notices; Type: TABLE DATA; Schema: blocket_2014; Owner: joaquin
--



--
-- Data for Name: pay_log; Type: TABLE DATA; Schema: blocket_2014; Owner: joaquin
--



--
-- Data for Name: pay_log_references; Type: TABLE DATA; Schema: blocket_2014; Owner: joaquin
--



--
-- Data for Name: payments; Type: TABLE DATA; Schema: blocket_2014; Owner: joaquin
--



--
-- Data for Name: purchase; Type: TABLE DATA; Schema: blocket_2014; Owner: joaquin
--



--
-- Data for Name: purchase_detail; Type: TABLE DATA; Schema: blocket_2014; Owner: joaquin
--



--
-- Data for Name: purchase_states; Type: TABLE DATA; Schema: blocket_2014; Owner: joaquin
--



--
-- Data for Name: state_params; Type: TABLE DATA; Schema: blocket_2014; Owner: joaquin
--



--
-- Data for Name: voucher_actions; Type: TABLE DATA; Schema: blocket_2014; Owner: joaquin
--



--
-- Data for Name: voucher_states; Type: TABLE DATA; Schema: blocket_2014; Owner: joaquin
--



SET search_path = blocket_2015, pg_catalog;

--
-- Data for Name: ads; Type: TABLE DATA; Schema: blocket_2015; Owner: joaquin
--



--
-- Data for Name: payment_groups; Type: TABLE DATA; Schema: blocket_2015; Owner: joaquin
--



--
-- Data for Name: ad_actions; Type: TABLE DATA; Schema: blocket_2015; Owner: joaquin
--



--
-- Data for Name: action_params; Type: TABLE DATA; Schema: blocket_2015; Owner: joaquin
--



--
-- Data for Name: tokens; Type: TABLE DATA; Schema: blocket_2015; Owner: joaquin
--



--
-- Data for Name: action_states; Type: TABLE DATA; Schema: blocket_2015; Owner: joaquin
--



--
-- Data for Name: ad_changes; Type: TABLE DATA; Schema: blocket_2015; Owner: joaquin
--



--
-- Data for Name: ad_image_changes; Type: TABLE DATA; Schema: blocket_2015; Owner: joaquin
--



--
-- Data for Name: ad_images; Type: TABLE DATA; Schema: blocket_2015; Owner: joaquin
--



--
-- Data for Name: ad_media; Type: TABLE DATA; Schema: blocket_2015; Owner: joaquin
--



--
-- Data for Name: ad_media_changes; Type: TABLE DATA; Schema: blocket_2015; Owner: joaquin
--



--
-- Data for Name: ad_params; Type: TABLE DATA; Schema: blocket_2015; Owner: joaquin
--



--
-- Data for Name: mail_queue; Type: TABLE DATA; Schema: blocket_2015; Owner: joaquin
--



--
-- Data for Name: notices; Type: TABLE DATA; Schema: blocket_2015; Owner: joaquin
--



--
-- Data for Name: pay_log; Type: TABLE DATA; Schema: blocket_2015; Owner: joaquin
--



--
-- Data for Name: pay_log_references; Type: TABLE DATA; Schema: blocket_2015; Owner: joaquin
--



--
-- Data for Name: payments; Type: TABLE DATA; Schema: blocket_2015; Owner: joaquin
--



--
-- Data for Name: purchase; Type: TABLE DATA; Schema: blocket_2015; Owner: joaquin
--



--
-- Data for Name: purchase_detail; Type: TABLE DATA; Schema: blocket_2015; Owner: joaquin
--



--
-- Data for Name: purchase_states; Type: TABLE DATA; Schema: blocket_2015; Owner: joaquin
--



--
-- Data for Name: state_params; Type: TABLE DATA; Schema: blocket_2015; Owner: joaquin
--



--
-- Data for Name: voucher_actions; Type: TABLE DATA; Schema: blocket_2015; Owner: joaquin
--



--
-- Data for Name: voucher_states; Type: TABLE DATA; Schema: blocket_2015; Owner: joaquin
--



SET search_path = public, pg_catalog;

--
-- Data for Name: admins; Type: TABLE DATA; Schema: public; Owner: joaquin
--

INSERT INTO admins VALUES (2, 'erik', '2d541162141d281abb9f5ba1b14f8072a1d2d29b', 'Erik', 'erik@jabber', 'erik@blocket.se', '070-111111', 'active');
INSERT INTO admins VALUES (3, 'zakay', '2d541162141d281abb9f5ba1b14f8072a1d2d29b', 'Zakay', 'zakay@jabber', 'zakay@blocket.se', '070-111111', 'active');
INSERT INTO admins VALUES (4, 'thomas', 'a94a8fe5ccb19ba61c4c0873d391e987982fbbd3', 'Thomas', 'thomas@jabber', 'test@schibstediberica.es', '070-111111', 'active');
INSERT INTO admins VALUES (5, 'kjell', '430985da851e9223368e820ebde5beaf6c6ef1cc', 'Kjell', 'kjell@jabber', 'kjell@blocket.se', '0709-6459256', 'active');
INSERT INTO admins VALUES (6, 'torsten', '430985da851e9223368e820ebde5beaf6c6ef1cc', 'Torsten Svensson', 'torsten@jabber', 'svara-inte@blocket.se', '0709-6459256', 'deleted');
INSERT INTO admins VALUES (50, 'mama', '67b757a4f76e5fda3fe86c8a9009630a5e53a053', 'M.A.M.A.', 'blocket1@jabber', 'blocket1@blocket.se', '0733555501', 'active');
INSERT INTO admins VALUES (51, 'bender00', '67b757a4f76e5fda3fe86c8a9009630a5e53a053', 'M.A.M.A. bender 00', NULL, NULL, NULL, 'active');
INSERT INTO admins VALUES (52, 'bender01', '67b757a4f76e5fda3fe86c8a9009630a5e53a053', 'M.A.M.A. bender 01', NULL, NULL, NULL, 'active');
INSERT INTO admins VALUES (53, 'bender02', '67b757a4f76e5fda3fe86c8a9009630a5e53a053', 'M.A.M.A. bender 02', NULL, NULL, NULL, 'active');
INSERT INTO admins VALUES (54, 'bender03', '67b757a4f76e5fda3fe86c8a9009630a5e53a053', 'M.A.M.A. bender 03', NULL, NULL, NULL, 'active');
INSERT INTO admins VALUES (55, 'bender04', '67b757a4f76e5fda3fe86c8a9009630a5e53a053', 'M.A.M.A. bender 04', NULL, NULL, NULL, 'active');
INSERT INTO admins VALUES (9, 'joaquin', '0a00ae03c9edd21d50a229f9fa1f98d9b3912b9e', NULL, NULL, 'joaquin@mazaru.schibsted.cl', NULL, 'active');


--
-- Data for Name: abuse_locks; Type: TABLE DATA; Schema: public; Owner: joaquin
--



--
-- Data for Name: abuse_reporters; Type: TABLE DATA; Schema: public; Owner: joaquin
--



--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: joaquin
--

INSERT INTO users VALUES (1, 1, 'uid1@blocket.se', 0, 0);
INSERT INTO users VALUES (2, 2, 'uid2@blocket.se', 0, 100);
INSERT INTO users VALUES (3, 3, 'prepaid3@blocket.se', 1000, 1000);
INSERT INTO users VALUES (4, 4, 'prepaid@blocket.se', 100000, 1000);
INSERT INTO users VALUES (5, 5, 'prepaid5@blocket.se', 1000, 1000);
INSERT INTO users VALUES (6, 6, 'kim@blocket.se', 1000, 1000);
INSERT INTO users VALUES (50, 50, 'seller1@schibsted.cl', 0, 0);
INSERT INTO users VALUES (51, 50, 'seller2@schibsted.cl', 0, 0);
INSERT INTO users VALUES (52, 50, 'seller3@schibsted.cl', 0, 0);
INSERT INTO users VALUES (53, 50, 'seller4@schibsted.cl', 0, 0);
INSERT INTO users VALUES (54, 50, 'joaquin@schibsted.cl', 0, 0);


--
-- Data for Name: abuse_reports; Type: TABLE DATA; Schema: public; Owner: joaquin
--



--
-- Data for Name: accounts; Type: TABLE DATA; Schema: public; Owner: joaquin
--



--
-- Data for Name: ads; Type: TABLE DATA; Schema: public; Owner: joaquin
--

INSERT INTO ads VALUES (1, 6000667, '2011-11-09 11:00:00', 'active', 'sell', 'Aurelio Rodr�guez', '1231231231', 2, 0, 1020, 5, '11111', false, false, false, 'Departamento Tarapac�', 'Con 2 dormitorios, en Alto Hospicio, 250m2 de espacio y 2 plazas de garaje', 45000000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'es');
INSERT INTO ads VALUES (2, NULL, NULL, 'inactive', 'sell', 'Thomas Svensson', '08-112233', 11, 0, 4100, 3, 'testb', false, false, false, 'Barncykel', 'Röd barncykel, 28 tum.', 666, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'es');
INSERT INTO ads VALUES (3, NULL, NULL, 'inactive', 'sell', 'Sven Ingvars', '08-121314', 11, 0, 4100, 5, 'testc', false, false, false, 'Damcykel', 'Damcykel med stång i mitten. 33,2 tum i plast.', 667, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'es');
INSERT INTO ads VALUES (4, NULL, NULL, 'inactive', 'sell', 'Klas Klasson', '08-121314', 11, 0, 4100, 5, 'testc', true, true, true, 'Hustomte', 'En tre fot stor hustomte.', 667, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'es');
INSERT INTO ads VALUES (5, NULL, NULL, 'inactive', 'sell', 'Dummy', '08-121314', 11, 0, 4100, 5, 'testc', false, false, false, 'Damcykel', 'Damcykel med stång i mitten. 33,2 tum i plast.', 667, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'es');
INSERT INTO ads VALUES (6, 3456789, '2006-04-05 09:21:31', 'active', 'sell', 'Bengt Bedrup', '08-121314', 10, 0, 2020, 5, '11111', true, true, false, 'Race car', 'Car with two doors ...
33.2-inch wheels made of plastic.', 11667, NULL, NULL, 'Testar länk', NULL, NULL, NULL, NULL, NULL, 'es');
INSERT INTO ads VALUES (7, NULL, NULL, 'inactive', 'sell', 'Dummy', '08-121314', 11, 0, 4100, 5, 'testc', false, false, false, 'Damcykel', 'Damcykel med stång i mitten. 33,2 tum i plast.', 667, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'es');
INSERT INTO ads VALUES (8, 6431717, '2006-04-06 09:21:31', 'active', 'sell', 'Juan P�rez', '084123456', 11, 0, 5020, 1, '11111', false, false, false, 'Una mesa', 'Y qu� mesa... menuda mesa!!', 157000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'es');
INSERT INTO ads VALUES (9, 999999, NULL, 'inactive', 'sell', 'Dummy', '08-121314', 11, 0, 4100, 1, 'testc', false, false, false, 'Damcykel med blahonga', 'Damcykel med stång rakt upp i mitten. 33,2 tum i plast.', 667, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'es');
INSERT INTO ads VALUES (10, 6000666, '2006-04-05 10:21:31', 'active', 'sell', 'Cristobal Col�n', '0812131491', 12, 0, 6020, 5, '11111', false, false, false, 'Pesas muy grandes', 'Una de 120 kg y la otra de 57, mas ligera.', 90000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'es');
INSERT INTO ads VALUES (11, 6394117, '2006-04-07 11:21:31', 'active', 'sell', 'Andrea Gonz�lez', '0987654321', 13, 0, 7060, 1, '11111', false, false, false, 'Peluquera a domicilio', 'Voy, corto el pelo, me pagas y m voy....', 12000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'es');
INSERT INTO ads VALUES (12, 6394118, '2011-11-06 11:21:31', 'active', 'let', 'Boris Felipe', '0987654000', 15, 0, 1040, 1, '11111', false, false, false, 'Arriendo mi preciosa casa en �u�oa', 'Dos habitaciones, metros cuadrados.... hect�reas dir�a yo... y plazas de garaje a tutipl�n', 500000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'es');
INSERT INTO ads VALUES (21, 8000001, '2014-03-25 14:22:25', 'active', 'sell', 'Seller 1', '962184762', 15, 0, 1020, 50, NULL, false, false, false, 'Apartment in Pedro Aguirre Cerda', 'Lorem ipsum dolor sit amet', 400000, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$}JL(ofeIEO*wJO)>2131333b02cd692ab495d5c800b549aa6f640506', '2014-03-25 14:22:25.746218', 'es');
INSERT INTO ads VALUES (20, 8000000, '2014-03-25 14:22:22', 'active', 'sell', 'Seller 1', '962184762', 15, 0, 1020, 50, NULL, false, false, false, 'Apartment in Providencia', 'Lorem ipsum dolor sit amet', 500000, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$\\izsM~$t[ ;,4\\!Z4c5de8c4daf8da389685d9c7136ddb0ae2584686', '2014-03-25 14:22:22.424495', 'es');
INSERT INTO ads VALUES (22, 8000002, '2014-03-25 14:22:30', 'active', 'sell', 'Seller 1', '962184762', 15, 0, 1020, 50, NULL, false, false, false, 'Apartment in Huechuraba', 'Lorem ipsum dolor sit amet', 123456000, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$zerImwXC''q~|U,Ll1f720830f211f2e14400f7a1f7e3bc7126455987', '2014-03-25 14:22:30.561154', 'es');
INSERT INTO ads VALUES (23, 8000003, '2014-03-25 14:22:32', 'active', 'sell', 'Seller 2', '962184762', 15, 0, 1020, 51, NULL, false, false, false, 'Apartment in Las Condes', 'Quisque porta neque ullamcorper, tristique est quis, sagittis dolor. Fusce et porta est, at egestas magna.', 98907678, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$;v]p7yvOgipTVNNl5bf56e14518ca79db3d9e8f75364e94cdc1addcc', '2014-03-25 14:22:32.972419', 'es');
INSERT INTO ads VALUES (24, 8000004, '2014-03-25 14:22:38', 'active', 'sell', 'Seller 2', '962184762', 15, 0, 1020, 51, NULL, false, false, false, 'Apartment in �u�oa', 'Fusce vitae sollicitudin orci. Duis fringilla metus ac congue venenatis. Nam tristique in velit sed sodales. Praesent dictum odio vitae sem consequat, at ullamcorper massa fringilla.', 150000000, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$(WV|z=oi&n,Cvp3;5f56fab5eef39790322ef145dbc39b74179c86e7', '2014-03-25 14:22:38.839343', 'es');
INSERT INTO ads VALUES (25, 8000005, '2014-03-25 14:22:41', 'active', 'sell', 'Seller 2', '962184762', 15, 0, 1040, 51, NULL, false, false, false, 'House in La Pintana', 'Morbi cursus tincidunt dictum. Sed sit amet turpis nec ipsum cursus posuere. Vestibulum sed congue dui. Vestibulum id arcu porttitor,', 150000, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$dpcpVuqsCpGnfomq2140dfb814bef56778e416dd5206c9de9594078b', '2014-03-25 14:22:41.15456', 'es');
INSERT INTO ads VALUES (26, 8000006, '2014-03-25 14:22:46', 'active', 'sell', 'Seller 2', '962184762', 15, 0, 1040, 51, NULL, false, false, false, 'House in Puente Alto', 'Morbi cursus tincidunt dictum. Sed sit amet turpis nec ipsum cursus posuere. Vestibulum sed congue dui. Vestibulum id arcu porttitor,', 400000, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$(Fe-+Dg}*@Jv.:oD450cfbe6eb6a00312d38910cb0a63a4a063a7600', '2014-03-25 14:22:46.028443', 'es');
INSERT INTO ads VALUES (27, 8000007, '2014-03-25 14:22:48', 'active', 'sell', 'Seller 2', '962184762', 15, 0, 1040, 51, NULL, false, false, false, 'House in Renca', 'Donec cursus velit ut tincidunt ullamcorper. Nullam lacus nisl, adipiscing eu vestibulum eget, ultricies vitae eros.', 12345000, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$0%6''#2Invvk9*.*-dda4a61b15cb6045f4fbfe3bd3387b79938311f2', '2014-03-25 14:22:48.491856', 'es');
INSERT INTO ads VALUES (28, 8000008, '2014-03-25 14:22:53', 'active', 'sell', 'Seller 2', '962184762', 15, 0, 1040, 51, NULL, false, false, false, 'House in San Bernardo', 'Donec cursus velit ut tincidunt ullamcorper. Nullam lacus nisl, adipiscing eu vestibulum eget, ultricies vitae eros.', 908000000, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$bddxxu4m)ROk;o-48def413f267768aa51b7416a8498ba9738e3e435', '2014-03-25 14:22:53.141107', 'es');
INSERT INTO ads VALUES (29, 8000009, '2014-03-25 14:22:55', 'active', 'sell', 'Seller 3', '962184762', 6, 0, 1100, 52, NULL, false, false, false, 'Land in Isla de Pascua', 'Duis fringilla metus ac congue venenatis. Nam tristique in velit sed sodales. Praesent dictum odio vitae sem consequat, at ullamcorper massa fringilla.', 500000000, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$-J|5v.$,VXO8IVk6feef72b1644609d53308c154d9df3398fb05d837', '2014-03-25 14:22:55.807858', 'es');
INSERT INTO ads VALUES (30, 8000010, '2014-03-25 14:23:00', 'active', 'sell', 'Seller 3', '962184762', 6, 0, 1100, 52, NULL, false, false, false, 'Land in La Cruz', 'Duis fringilla metus ac congue venenatis. Nam tristique in velit sed sodales. Praesent dictum odio vitae sem consequat, at ullamcorper massa fringilla.', 9000000, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$]VfkYr.q ,Retp+F6a6ac9b67641214478d68b86aa6a5dbcee78243c', '2014-03-25 14:23:00.806579', 'es');
INSERT INTO ads VALUES (31, 8000011, '2014-03-25 14:23:03', 'active', 'sell', 'Seller 3', '962184762', 6, 0, 1100, 52, NULL, false, false, false, 'Land in Olmu�', 'Duis fringilla metus ac congue venenatis. Nam tristique in velit sed sodales. Praesent dictum odio vitae sem consequat, at ullamcorper massa fringilla.', 90000000, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$scd,W="tWg:>^kFh78aaa370f7e398fb5504df2577e4fbcfc11fc880', '2014-03-25 14:23:03.294265', 'es');
INSERT INTO ads VALUES (32, 8000012, '2014-03-25 14:23:10', 'active', 'sell', 'Seller 4', '962184762', 3, 0, 7020, 53, NULL, false, false, false, 'Trabalho in Antofagasta', 'Proin eu lacinia tortor. In at augue sem. Vivamus id arcu dolor. Quisque ac nisi felis.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$:xHb@T;P6sv 0^>b6b0380005c949218a4e02bc7be55b389b4804c98', '2014-03-25 14:23:10.740153', 'es');
INSERT INTO ads VALUES (33, 8000013, '2014-03-25 14:23:12', 'active', 'sell', 'Seller 4', '962184762', 3, 0, 7020, 53, NULL, false, false, false, 'Trabajo in Antofagasta II', 'Proin eu lacinia tortor. In at augue sem. Vivamus id arcu dolor. Quisque ac nisi felis.', 1234000, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$Z#9m`H M66Q&''O/Qb8e1c3ed53a2a5f8e81b485fb796e248abe0993c', '2014-03-25 14:23:12.928341', 'es');
INSERT INTO ads VALUES (34, 8000014, '2014-03-25 14:23:18', 'active', 'sell', 'Seller 4', '962184762', 9, 0, 6140, 53, NULL, false, false, false, 'Animals ad in BioBio', 'Proin eu lacinia tortor. In at augue sem. Vivamus id arcu dolor. Quisque ac nisi felis.', 100000, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$b92OEL''1k1~ux$911d22505efb06b236d5893a7ec3cf13f01d6ef0e9', '2014-03-25 14:23:18.307154', 'es');
INSERT INTO ads VALUES (35, 8000015, '2014-03-26 09:49:29', 'active', 'sell', 'Joaco', '962184762', 6, 0, 5020, 54, NULL, false, false, false, 'Os meus moveis', 'Mauris ac leo at quam egestas laoreet eget vitae ligula. Nunc vitae libero at nunc porta rutrum. Integer aliquam, tortor et porttitor scelerisque, n', 100000, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$"Gx&yT)9y%,RCX<l1d72af271e84ba24d7c2105c472db47ec32b4fbe', '2014-03-26 09:49:29.791808', 'es');
INSERT INTO ads VALUES (36, 8000016, '2014-03-26 09:49:33', 'active', 'sell', 'Joaco', '962184762', 5, 0, 1100, 54, NULL, false, false, false, 'Land in Combarbal�', 'Mauris ac leo at quam egestas laoreet eget vitae ligula. Nunc vitae libero at nunc porta rutrum. Integer aliquam, tortor et porttitor scelerisque, n', 89876000, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$->da,o2/ZPln6 r<172d5baf4974a2a72e78faad7f4b3c1b2d9053d0', '2014-03-26 09:49:33.725943', 'es');


--
-- Data for Name: payment_groups; Type: TABLE DATA; Schema: public; Owner: joaquin
--

INSERT INTO payment_groups VALUES (0, '012345678', 'unverified', '2014-03-25 12:27:22.650156', NULL);
INSERT INTO payment_groups VALUES (1, '123456789', 'unverified', '2014-03-25 12:27:22.652188', NULL);
INSERT INTO payment_groups VALUES (2, '11223', 'unpaid', '2014-03-25 12:27:22.652963', NULL);
INSERT INTO payment_groups VALUES (3, '200000001', 'unverified', '2014-03-25 12:27:22.653578', NULL);
INSERT INTO payment_groups VALUES (50, '59876', 'cleared', '2014-03-25 12:27:22.654175', NULL);
INSERT INTO payment_groups VALUES (60, '112200000', 'verified', '2014-03-25 12:45:07.473968', NULL);
INSERT INTO payment_groups VALUES (61, '112200001', 'verified', '2014-03-25 12:47:06.699678', NULL);
INSERT INTO payment_groups VALUES (62, '112200002', 'verified', '2014-03-25 12:49:15.012072', NULL);
INSERT INTO payment_groups VALUES (63, '112200003', 'verified', '2014-03-25 12:50:36.73864', NULL);
INSERT INTO payment_groups VALUES (64, '112200004', 'verified', '2014-03-25 12:51:24.215573', NULL);
INSERT INTO payment_groups VALUES (65, '112200005', 'verified', '2014-03-25 12:53:47.06352', NULL);
INSERT INTO payment_groups VALUES (66, '112200006', 'verified', '2014-03-25 12:54:25.262888', NULL);
INSERT INTO payment_groups VALUES (67, '112200007', 'verified', '2014-03-25 12:55:12.66257', NULL);
INSERT INTO payment_groups VALUES (68, '112200008', 'verified', '2014-03-25 12:55:57.057476', NULL);
INSERT INTO payment_groups VALUES (69, '112200009', 'verified', '2014-03-25 12:58:21.703978', NULL);
INSERT INTO payment_groups VALUES (70, '235392023', 'verified', '2014-03-25 12:59:07.160838', NULL);
INSERT INTO payment_groups VALUES (71, '370784046', 'verified', '2014-03-25 12:59:41.258944', NULL);
INSERT INTO payment_groups VALUES (72, '506176069', 'verified', '2014-03-25 14:20:13.64879', NULL);
INSERT INTO payment_groups VALUES (73, '641568092', 'verified', '2014-03-25 14:20:52.529498', NULL);
INSERT INTO payment_groups VALUES (74, '776960115', 'verified', '2014-03-25 14:21:40.986298', NULL);
INSERT INTO payment_groups VALUES (75, '912352138', 'verified', '2014-03-26 09:48:21.196134', NULL);
INSERT INTO payment_groups VALUES (76, '147744161', 'verified', '2014-03-26 09:49:02.146434', NULL);


--
-- Data for Name: ad_actions; Type: TABLE DATA; Schema: public; Owner: joaquin
--

INSERT INTO ad_actions VALUES (1, 1, 'new', 154, 'accepted', 'normal', NULL, NULL, 0);
INSERT INTO ad_actions VALUES (2, 1, 'new', 4, 'unverified', 'normal', NULL, NULL, 1);
INSERT INTO ad_actions VALUES (3, 1, 'new', 6, 'unpaid', 'normal', NULL, NULL, 2);
INSERT INTO ad_actions VALUES (4, 1, 'new', 8, 'unverified', 'normal', NULL, NULL, 3);
INSERT INTO ad_actions VALUES (8, 1, 'new', 9, 'accepted', 'normal', NULL, NULL, NULL);
INSERT INTO ad_actions VALUES (10, 1, 'new', 11, 'accepted', 'normal', NULL, NULL, NULL);
INSERT INTO ad_actions VALUES (11, 1, 'new', 12, 'accepted', 'normal', NULL, NULL, NULL);
INSERT INTO ad_actions VALUES (6, 1, 'new', 53, 'accepted', 'normal', NULL, NULL, 50);
INSERT INTO ad_actions VALUES (9, 1, 'new', 43, 'refused', 'normal', NULL, NULL, NULL);
INSERT INTO ad_actions VALUES (12, 1, 'new', 164, 'accepted', 'normal', NULL, NULL, 0);
INSERT INTO ad_actions VALUES (20, 2, 'gallery', 276, 'accepted', 'normal', NULL, NULL, NULL);
INSERT INTO ad_actions VALUES (21, 2, 'gallery', 278, 'accepted', 'normal', NULL, NULL, NULL);
INSERT INTO ad_actions VALUES (22, 2, 'gallery', 280, 'accepted', 'normal', NULL, NULL, NULL);
INSERT INTO ad_actions VALUES (23, 2, 'gallery', 282, 'accepted', 'normal', NULL, NULL, NULL);
INSERT INTO ad_actions VALUES (24, 2, 'gallery', 284, 'accepted', 'normal', NULL, NULL, NULL);
INSERT INTO ad_actions VALUES (25, 2, 'gallery', 286, 'accepted', 'normal', NULL, NULL, NULL);
INSERT INTO ad_actions VALUES (26, 2, 'gallery', 288, 'accepted', 'normal', NULL, NULL, NULL);
INSERT INTO ad_actions VALUES (27, 2, 'gallery', 290, 'accepted', 'normal', NULL, NULL, NULL);
INSERT INTO ad_actions VALUES (28, 2, 'gallery', 292, 'accepted', 'normal', NULL, NULL, NULL);
INSERT INTO ad_actions VALUES (29, 2, 'gallery', 294, 'accepted', 'normal', NULL, NULL, NULL);
INSERT INTO ad_actions VALUES (30, 2, 'gallery', 296, 'accepted', 'normal', NULL, NULL, NULL);
INSERT INTO ad_actions VALUES (31, 2, 'gallery', 298, 'accepted', 'normal', NULL, NULL, NULL);
INSERT INTO ad_actions VALUES (32, 2, 'gallery', 300, 'accepted', 'normal', NULL, NULL, NULL);
INSERT INTO ad_actions VALUES (33, 2, 'gallery', 302, 'accepted', 'normal', NULL, NULL, NULL);
INSERT INTO ad_actions VALUES (20, 1, 'new', 247, 'accepted', 'normal', 9, '2014-03-25 14:27:17.982753', 60);
INSERT INTO ad_actions VALUES (21, 1, 'new', 248, 'accepted', 'normal', 9, '2014-03-25 14:27:17.982753', 61);
INSERT INTO ad_actions VALUES (34, 2, 'gallery', 304, 'accepted', 'normal', NULL, NULL, NULL);
INSERT INTO ad_actions VALUES (22, 1, 'new', 251, 'accepted', 'normal', 9, '2014-03-25 14:27:27.471215', 62);
INSERT INTO ad_actions VALUES (23, 1, 'new', 252, 'accepted', 'normal', 9, '2014-03-25 14:27:27.471215', 63);
INSERT INTO ad_actions VALUES (24, 1, 'new', 255, 'accepted', 'normal', 9, '2014-03-25 14:27:35.004493', 64);
INSERT INTO ad_actions VALUES (25, 1, 'new', 256, 'accepted', 'normal', 9, '2014-03-25 14:27:35.004493', 65);
INSERT INTO ad_actions VALUES (26, 1, 'new', 259, 'accepted', 'normal', 9, '2014-03-25 14:27:42.403915', 66);
INSERT INTO ad_actions VALUES (27, 1, 'new', 260, 'accepted', 'normal', 9, '2014-03-25 14:27:42.403915', 67);
INSERT INTO ad_actions VALUES (28, 1, 'new', 263, 'accepted', 'normal', 9, '2014-03-25 14:27:49.918724', 68);
INSERT INTO ad_actions VALUES (29, 1, 'new', 264, 'accepted', 'normal', 9, '2014-03-25 14:27:49.918724', 69);
INSERT INTO ad_actions VALUES (30, 1, 'new', 267, 'accepted', 'normal', 9, '2014-03-25 14:27:57.722281', 70);
INSERT INTO ad_actions VALUES (31, 1, 'new', 268, 'accepted', 'normal', 9, '2014-03-25 14:27:57.722281', 71);
INSERT INTO ad_actions VALUES (32, 1, 'new', 271, 'accepted', 'normal', 9, '2014-03-25 14:28:04.959752', 72);
INSERT INTO ad_actions VALUES (33, 1, 'new', 272, 'accepted', 'normal', 9, '2014-03-25 14:28:04.959752', 73);
INSERT INTO ad_actions VALUES (34, 1, 'new', 274, 'accepted', 'normal', 9, '2014-03-25 14:28:15.524971', 74);
INSERT INTO ad_actions VALUES (35, 1, 'new', 313, 'accepted', 'normal', 9, '2014-03-26 09:54:24.627781', 75);
INSERT INTO ad_actions VALUES (36, 1, 'new', 314, 'accepted', 'normal', 9, '2014-03-26 09:54:24.627781', 76);
INSERT INTO ad_actions VALUES (35, 2, 'gallery', 316, 'accepted', 'normal', NULL, NULL, NULL);
INSERT INTO ad_actions VALUES (36, 2, 'gallery', 318, 'accepted', 'normal', NULL, NULL, NULL);


--
-- Data for Name: action_params; Type: TABLE DATA; Schema: public; Owner: joaquin
--

INSERT INTO action_params VALUES (20, 1, 'source', 'web');
INSERT INTO action_params VALUES (20, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params VALUES (21, 1, 'source', 'web');
INSERT INTO action_params VALUES (21, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params VALUES (22, 1, 'source', 'web');
INSERT INTO action_params VALUES (22, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params VALUES (23, 1, 'source', 'web');
INSERT INTO action_params VALUES (23, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params VALUES (24, 1, 'source', 'web');
INSERT INTO action_params VALUES (24, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params VALUES (25, 1, 'source', 'web');
INSERT INTO action_params VALUES (25, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params VALUES (26, 1, 'source', 'web');
INSERT INTO action_params VALUES (26, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params VALUES (27, 1, 'source', 'web');
INSERT INTO action_params VALUES (27, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params VALUES (28, 1, 'source', 'web');
INSERT INTO action_params VALUES (28, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params VALUES (29, 1, 'source', 'web');
INSERT INTO action_params VALUES (29, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params VALUES (30, 1, 'source', 'web');
INSERT INTO action_params VALUES (30, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params VALUES (31, 1, 'source', 'web');
INSERT INTO action_params VALUES (31, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params VALUES (32, 1, 'source', 'web');
INSERT INTO action_params VALUES (32, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params VALUES (33, 1, 'source', 'web');
INSERT INTO action_params VALUES (33, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params VALUES (34, 1, 'source', 'web');
INSERT INTO action_params VALUES (34, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params VALUES (35, 1, 'source', 'web');
INSERT INTO action_params VALUES (35, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params VALUES (36, 1, 'source', 'web');
INSERT INTO action_params VALUES (36, 1, 'redir', 'dW5rbm93bg==');


--
-- Data for Name: stores; Type: TABLE DATA; Schema: public; Owner: joaquin
--



--
-- Data for Name: tokens; Type: TABLE DATA; Schema: public; Owner: joaquin
--

INSERT INTO tokens VALUES (99999917, 'd12ac643ce5c39ddb765bc452d40790932d7c0f67823aaa', 5, '2006-04-06 15:19:46', '2006-04-06 16:19:46', '192.168.4.75', 'clear', NULL, NULL);
INSERT INTO tokens VALUES (99999918, 'a5e4e5b16bc9c806b1c22cfd638440034791acc68f23600', 5, '2006-04-06 16:19:31', '2006-04-06 16:19:46', '192.168.4.75', 'authenticate', NULL, NULL);
INSERT INTO tokens VALUES (99999919, 'a80cbcd0b019780754593a1ea8b28001c88086439101100', 5, '2006-04-06 16:19:46', '2006-04-06 16:19:46', '192.168.4.75', 'clear', NULL, NULL);
INSERT INTO tokens VALUES (99999992, 'b7e4e5b16bc9c806b1c22cfd638440034791acc68f23600', 5, '2011-11-06 05:19:31', '2011-11-06 05:19:46', '192.168.4.75', 'adqueues', NULL, NULL);
INSERT INTO tokens VALUES (99999993, 'b9e4e5b16bc9c806b1c22cfd638440034791acc68f23600', 5, '2011-11-06 05:19:31', '2011-11-06 05:19:46', '192.168.4.75', 'adqueues', NULL, NULL);
INSERT INTO tokens VALUES (99999998, 'b5e4e5b16bc9c806b1c22cfd638440034791acc68f23600', 5, '2006-04-06 15:19:31', '2006-04-06 15:19:46', '192.168.4.75', 'authenticate', NULL, NULL);
INSERT INTO tokens VALUES (99999999, '580cbcd0b019780754593a1ea8b28001c88086439101100', 5, '2006-04-06 15:19:46', '2006-04-06 16:19:46', '192.168.4.75', 'clear', NULL, NULL);
INSERT INTO tokens VALUES (99999994, 'b7e4e9b16bc9c806b1c22cfd638440034791acc68f23600', 5, '2011-11-06 05:19:31', '2011-11-06 05:19:46', '192.168.4.75', 'adqueues', NULL, NULL);
INSERT INTO tokens VALUES (99999995, 'b9e6e6b16bc9c806b1c22cfd638440034791acc68f23600', 5, '2011-11-06 06:19:31', '2011-11-06 06:19:46', '192.168.4.75', 'adqueues', NULL, NULL);
INSERT INTO tokens VALUES (1, 'X5331bb4546b4cbad000000003286403a00000000', 9, '2014-03-25 14:22:13.407836', '2014-03-25 14:22:13.679265', '10.0.1.188', 'authenticate', NULL, NULL);
INSERT INTO tokens VALUES (2, 'X5331bb464369beb20000000026517e6200000000', 9, '2014-03-25 14:22:13.679265', '2014-03-25 14:22:15.353873', '10.0.1.188', 'spamfilter', NULL, NULL);
INSERT INTO tokens VALUES (3, 'X5331bb47644a083a0000000028bd94b300000000', 9, '2014-03-25 14:22:15.353873', '2014-03-25 14:22:17.978329', '10.0.1.188', 'adqueues', NULL, NULL);
INSERT INTO tokens VALUES (4, 'X5331bb4a41ae0262000000003e55e49900000000', 9, '2014-03-25 14:22:17.978329', '2014-03-25 14:22:18.107518', '10.0.1.188', 'adqueues', NULL, NULL);
INSERT INTO tokens VALUES (5, 'X5331bb4a14660df9000000006679078e00000000', 9, '2014-03-25 14:22:18.107518', '2014-03-25 14:22:18.114957', '10.0.1.188', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (6, 'X5331bb4a19f97abe000000004fdaf3ac00000000', 9, '2014-03-25 14:22:18.114957', '2014-03-25 14:22:18.122823', '10.0.1.188', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (7, 'X5331bb4a7a0d93f200000000684e27900000000', 9, '2014-03-25 14:22:18.122823', '2014-03-25 14:22:18.143956', '10.0.1.188', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (8, 'X5331bb4a43f6cdf600000000c15cfec00000000', 9, '2014-03-25 14:22:18.143956', '2014-03-25 14:22:18.150955', '10.0.1.188', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (9, 'X5331bb4a1f1766720000000039bd8dc400000000', 9, '2014-03-25 14:22:18.150955', '2014-03-25 14:22:18.158119', '10.0.1.188', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (10, 'X5331bb4a35191dfc0000000025e9b60200000000', 9, '2014-03-25 14:22:18.158119', '2014-03-25 14:22:22.41884', '10.0.1.188', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (11, 'X5331bb4e66bd7ca000000006e22650000000000', 9, '2014-03-25 14:22:22.41884', '2014-03-25 14:22:25.731807', '10.0.1.188', 'review', NULL, NULL);
INSERT INTO tokens VALUES (12, 'X5331bb52c0bd01000000000391ef6b400000000', 9, '2014-03-25 14:22:25.731807', '2014-03-25 14:22:27.467171', '10.0.1.188', 'review', NULL, NULL);
INSERT INTO tokens VALUES (13, 'X5331bb536e7a4cbf00000000215d0d5600000000', 9, '2014-03-25 14:22:27.467171', '2014-03-25 14:22:27.58368', '10.0.1.188', 'adqueues', NULL, NULL);
INSERT INTO tokens VALUES (14, 'X5331bb543bdbc7a0000000001ff9fcca00000000', 9, '2014-03-25 14:22:27.58368', '2014-03-25 14:22:27.591534', '10.0.1.188', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (15, 'X5331bb54803205c00000000fad6ffc00000000', 9, '2014-03-25 14:22:27.591534', '2014-03-25 14:22:27.599159', '10.0.1.188', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (16, 'X5331bb54262a85bc000000002fba311f00000000', 9, '2014-03-25 14:22:27.599159', '2014-03-25 14:22:27.619912', '10.0.1.188', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (17, 'X5331bb547bfa3d32000000006cdf516900000000', 9, '2014-03-25 14:22:27.619912', '2014-03-25 14:22:27.627018', '10.0.1.188', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (18, 'X5331bb546240715a000000003f63fbe400000000', 9, '2014-03-25 14:22:27.627018', '2014-03-25 14:22:27.634527', '10.0.1.188', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (19, 'X5331bb541330cfcb00000000468a799400000000', 9, '2014-03-25 14:22:27.634527', '2014-03-25 14:22:30.555508', '10.0.1.188', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (20, 'X5331bb57682190980000000054ded22d00000000', 9, '2014-03-25 14:22:30.555508', '2014-03-25 14:22:32.966908', '10.0.1.188', 'review', NULL, NULL);
INSERT INTO tokens VALUES (21, 'X5331bb5940c1323f00000000326223bb00000000', 9, '2014-03-25 14:22:32.966908', '2014-03-25 14:22:35.000434', '10.0.1.188', 'review', NULL, NULL);
INSERT INTO tokens VALUES (22, 'X5331bb5b4e05e2e000000007c879e9100000000', 9, '2014-03-25 14:22:35.000434', '2014-03-25 14:22:35.122808', '10.0.1.188', 'adqueues', NULL, NULL);
INSERT INTO tokens VALUES (23, 'X5331bb5b3b57d9bc000000001ed9d8ec00000000', 9, '2014-03-25 14:22:35.122808', '2014-03-25 14:22:35.130268', '10.0.1.188', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (24, 'X5331bb5b4c62923d0000000035656dae00000000', 9, '2014-03-25 14:22:35.130268', '2014-03-25 14:22:35.137846', '10.0.1.188', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (25, 'X5331bb5b255ebb65000000001059603300000000', 9, '2014-03-25 14:22:35.137846', '2014-03-25 14:22:35.159618', '10.0.1.188', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (26, 'X5331bb5b417b3d9a00000000447621d700000000', 9, '2014-03-25 14:22:35.159618', '2014-03-25 14:22:35.166774', '10.0.1.188', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (27, 'X5331bb5b4a16edf80000000076945b9700000000', 9, '2014-03-25 14:22:35.166774', '2014-03-25 14:22:35.17417', '10.0.1.188', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (28, 'X5331bb5b6a5fd7d9000000005082c5c200000000', 9, '2014-03-25 14:22:35.17417', '2014-03-25 14:22:38.833755', '10.0.1.188', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (29, 'X5331bb5f64b6c0970000000058da249900000000', 9, '2014-03-25 14:22:38.833755', '2014-03-25 14:22:41.149343', '10.0.1.188', 'review', NULL, NULL);
INSERT INTO tokens VALUES (30, 'X5331bb614df8aed9000000004753daf900000000', 9, '2014-03-25 14:22:41.149343', '2014-03-25 14:22:42.399619', '10.0.1.188', 'review', NULL, NULL);
INSERT INTO tokens VALUES (31, 'X5331bb6271dfd318000000002092883700000000', 9, '2014-03-25 14:22:42.399619', '2014-03-25 14:22:42.518271', '10.0.1.188', 'adqueues', NULL, NULL);
INSERT INTO tokens VALUES (32, 'X5331bb6378d421630000000079e2f37400000000', 9, '2014-03-25 14:22:42.518271', '2014-03-25 14:22:42.526299', '10.0.1.188', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (33, 'X5331bb63303ff834000000001efea72000000000', 9, '2014-03-25 14:22:42.526299', '2014-03-25 14:22:42.534067', '10.0.1.188', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (34, 'X5331bb63299d2493000000002c3a356600000000', 9, '2014-03-25 14:22:42.534067', '2014-03-25 14:22:42.555384', '10.0.1.188', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (35, 'X5331bb63bddf88900000000bdd95ed00000000', 9, '2014-03-25 14:22:42.555384', '2014-03-25 14:22:42.562607', '10.0.1.188', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (36, 'X5331bb636b9e314b000000001f0ec85500000000', 9, '2014-03-25 14:22:42.562607', '2014-03-25 14:22:42.56996', '10.0.1.188', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (37, 'X5331bb6352680f820000000053bfc1e300000000', 9, '2014-03-25 14:22:42.56996', '2014-03-25 14:22:46.023278', '10.0.1.188', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (38, 'X5331bb6673ed9a820000000057486db000000000', 9, '2014-03-25 14:22:46.023278', '2014-03-25 14:22:48.486942', '10.0.1.188', 'review', NULL, NULL);
INSERT INTO tokens VALUES (39, 'X5331bb6843b43a230000000056888fd600000000', 9, '2014-03-25 14:22:48.486942', '2014-03-25 14:22:49.914592', '10.0.1.188', 'review', NULL, NULL);
INSERT INTO tokens VALUES (40, 'X5331bb6a50476074000000002f45743e00000000', 9, '2014-03-25 14:22:49.914592', '2014-03-25 14:22:50.034004', '10.0.1.188', 'adqueues', NULL, NULL);
INSERT INTO tokens VALUES (41, 'X5331bb6a7622469c000000001ca9f2b200000000', 9, '2014-03-25 14:22:50.034004', '2014-03-25 14:22:50.041467', '10.0.1.188', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (42, 'X5331bb6a64aae1ed000000001b81020100000000', 9, '2014-03-25 14:22:50.041467', '2014-03-25 14:22:50.048867', '10.0.1.188', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (43, 'X5331bb6a2d0352e50000000026261f8700000000', 9, '2014-03-25 14:22:50.048867', '2014-03-25 14:22:50.069616', '10.0.1.188', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (44, 'X5331bb6a5ff723d800000000771a40dd00000000', 9, '2014-03-25 14:22:50.069616', '2014-03-25 14:22:50.076735', '10.0.1.188', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (45, 'X5331bb6a1cba7b1e000000004a56fbb100000000', 9, '2014-03-25 14:22:50.076735', '2014-03-25 14:22:50.084269', '10.0.1.188', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (46, 'X5331bb6a479d069f000000001713bb600000000', 9, '2014-03-25 14:22:50.084269', '2014-03-25 14:22:53.135495', '10.0.1.188', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (47, 'X5331bb6d2331204a00000000397cd9b700000000', 9, '2014-03-25 14:22:53.135495', '2014-03-25 14:22:55.803022', '10.0.1.188', 'review', NULL, NULL);
INSERT INTO tokens VALUES (48, 'X5331bb7043239a13000000006cf492600000000', 9, '2014-03-25 14:22:55.803022', '2014-03-25 14:22:57.718222', '10.0.1.188', 'review', NULL, NULL);
INSERT INTO tokens VALUES (49, 'X5331bb722203c3ed000000001c0541ae00000000', 9, '2014-03-25 14:22:57.718222', '2014-03-25 14:22:57.836643', '10.0.1.188', 'adqueues', NULL, NULL);
INSERT INTO tokens VALUES (50, 'X5331bb72335fcd2b000000005243bc2100000000', 9, '2014-03-25 14:22:57.836643', '2014-03-25 14:22:57.843669', '10.0.1.188', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (51, 'X5331bb723b03e8ce000000005cfcf1bf00000000', 9, '2014-03-25 14:22:57.843669', '2014-03-25 14:22:57.852765', '10.0.1.188', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (52, 'X5331bb727e7df1880000000046e1e15700000000', 9, '2014-03-25 14:22:57.852765', '2014-03-25 14:22:57.876857', '10.0.1.188', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (53, 'X5331bb7268da87ac000000006a1c22d300000000', 9, '2014-03-25 14:22:57.876857', '2014-03-25 14:22:57.884067', '10.0.1.188', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (54, 'X5331bb7265f0a9ac000000003b42972e00000000', 9, '2014-03-25 14:22:57.884067', '2014-03-25 14:22:57.891867', '10.0.1.188', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (55, 'X5331bb723ddbe4b60000000059de442f00000000', 9, '2014-03-25 14:22:57.891867', '2014-03-25 14:23:00.801214', '10.0.1.188', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (56, 'X5331bb75128b04de00000000e23452a00000000', 9, '2014-03-25 14:23:00.801214', '2014-03-25 14:23:03.286396', '10.0.1.188', 'review', NULL, NULL);
INSERT INTO tokens VALUES (57, 'X5331bb776cb09eb800000000536b3ebc00000000', 9, '2014-03-25 14:23:03.286396', '2014-03-25 14:23:04.955643', '10.0.1.188', 'review', NULL, NULL);
INSERT INTO tokens VALUES (58, 'X5331bb79923b86d000000008ad4b7a00000000', 9, '2014-03-25 14:23:04.955643', '2014-03-25 14:23:05.075167', '10.0.1.188', 'adqueues', NULL, NULL);
INSERT INTO tokens VALUES (59, 'X5331bb792acd37dc000000006dce9a5a00000000', 9, '2014-03-25 14:23:05.075167', '2014-03-25 14:23:05.083003', '10.0.1.188', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (60, 'X5331bb79242e4d7b0000000057d08ac200000000', 9, '2014-03-25 14:23:05.083003', '2014-03-25 14:23:05.090593', '10.0.1.188', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (61, 'X5331bb7913f4b9e200000000425715300000000', 9, '2014-03-25 14:23:05.090593', '2014-03-25 14:23:05.111331', '10.0.1.188', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (62, 'X5331bb794eeacb9f0000000030af350000000000', 9, '2014-03-25 14:23:05.111331', '2014-03-25 14:23:05.118512', '10.0.1.188', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (63, 'X5331bb794e7c6d05000000001687d23f00000000', 9, '2014-03-25 14:23:05.118512', '2014-03-25 14:23:05.126712', '10.0.1.188', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (64, 'X5331bb79322070b60000000071ad8d4f00000000', 9, '2014-03-25 14:23:05.126712', '2014-03-25 14:23:10.733354', '10.0.1.188', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (65, 'X5331bb7f5004abf600000000542434a400000000', 9, '2014-03-25 14:23:10.733354', '2014-03-25 14:23:12.92242', '10.0.1.188', 'review', NULL, NULL);
INSERT INTO tokens VALUES (66, 'X5331bb8170d7397f00000000564272fb00000000', 9, '2014-03-25 14:23:12.92242', '2014-03-25 14:23:15.520614', '10.0.1.188', 'review', NULL, NULL);
INSERT INTO tokens VALUES (67, 'X5331bb84db2cefd00000000364792200000000', 9, '2014-03-25 14:23:15.520614', '2014-03-25 14:23:15.579489', '10.0.1.188', 'adqueues', NULL, NULL);
INSERT INTO tokens VALUES (68, 'X5331bb842667f0c50000000048b6b7cb00000000', 9, '2014-03-25 14:23:15.579489', '2014-03-25 14:23:15.585599', '10.0.1.188', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (69, 'X5331bb8460616ae10000000024e5e24d00000000', 9, '2014-03-25 14:23:15.585599', '2014-03-25 14:23:15.591612', '10.0.1.188', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (70, 'X5331bb84f98992300000000493bf28d00000000', 9, '2014-03-25 14:23:15.591612', '2014-03-25 14:23:18.301827', '10.0.1.188', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (71, 'X5331bb86f02052000000000758942cf00000000', 9, '2014-03-25 14:23:18.301827', '2014-03-25 14:23:19.155897', '10.0.1.188', 'review', NULL, NULL);
INSERT INTO tokens VALUES (72, 'X5331bb8742d8d6ee00000000640a22da00000000', 9, '2014-03-25 14:23:19.155897', '2014-03-25 14:23:19.174331', '10.0.1.188', 'adqueues', NULL, NULL);
INSERT INTO tokens VALUES (73, 'X5331bb8712b4249000000005b5e1a3600000000', 9, '2014-03-25 14:23:19.174331', '2014-03-25 15:23:19.174331', '10.0.1.188', 'adqueues', NULL, NULL);
INSERT INTO tokens VALUES (74, 'X5332ccce28863e8d000000002f97057900000000', 9, '2014-03-26 09:49:18.222891', '2014-03-26 09:49:18.52193', '10.0.1.188', 'authenticate', NULL, NULL);
INSERT INTO tokens VALUES (75, 'X5332cccf15366a7300000000e8a54c600000000', 9, '2014-03-26 09:49:18.52193', '2014-03-26 09:49:23.0903', '10.0.1.188', 'spamfilter', NULL, NULL);
INSERT INTO tokens VALUES (76, 'X5332ccd3b6e945d000000002a8beb8b00000000', 9, '2014-03-26 09:49:23.0903', '2014-03-26 09:49:24.623242', '10.0.1.188', 'adqueues', NULL, NULL);
INSERT INTO tokens VALUES (77, 'X5332ccd55129d179000000002045dcc400000000', 9, '2014-03-26 09:49:24.623242', '2014-03-26 09:49:24.769058', '10.0.1.188', 'adqueues', NULL, NULL);
INSERT INTO tokens VALUES (78, 'X5332ccd573fc87e00000000027e45db300000000', 9, '2014-03-26 09:49:24.769058', '2014-03-26 09:49:24.776182', '10.0.1.188', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (79, 'X5332ccd568bbf736000000006681679f00000000', 9, '2014-03-26 09:49:24.776182', '2014-03-26 09:49:24.783552', '10.0.1.188', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (80, 'X5332ccd56f493203000000001ef92f4200000000', 9, '2014-03-26 09:49:24.783552', '2014-03-26 09:49:24.804164', '10.0.1.188', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (81, 'X5332ccd5664b4a0c000000003dd3cb6200000000', 9, '2014-03-26 09:49:24.804164', '2014-03-26 09:49:24.810822', '10.0.1.188', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (82, 'X5332ccd534e076240000000060ca513a00000000', 9, '2014-03-26 09:49:24.810822', '2014-03-26 09:49:24.81806', '10.0.1.188', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (83, 'X5332ccd55ebedde50000000058dba12b00000000', 9, '2014-03-26 09:49:24.81806', '2014-03-26 09:49:29.786882', '10.0.1.188', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (84, 'X5332ccda7dfde00c0000000041a6c8d00000000', 9, '2014-03-26 09:49:29.786882', '2014-03-26 09:49:33.7133', '10.0.1.188', 'review', NULL, NULL);
INSERT INTO tokens VALUES (85, 'X5332ccde3726a3950000000046f667600000000', 9, '2014-03-26 09:49:33.7133', '2014-03-26 09:49:36.191849', '10.0.1.188', 'review', NULL, NULL);
INSERT INTO tokens VALUES (86, 'X5332cce060863c22000000004b79b67000000000', 9, '2014-03-26 09:49:36.191849', '2014-03-26 09:49:36.210874', '10.0.1.188', 'adqueues', NULL, NULL);
INSERT INTO tokens VALUES (87, 'X5332cce0433d62b100000000703b2f200000000', 9, '2014-03-26 09:49:36.210874', '2014-03-26 10:49:36.210874', '10.0.1.188', 'adqueues', NULL, NULL);


--
-- Data for Name: action_states; Type: TABLE DATA; Schema: public; Owner: joaquin
--

INSERT INTO action_states VALUES (1, 1, 150, 'reg', 'initial', '2014-03-25 12:27:22.663812', '192.168.4.75', NULL);
INSERT INTO action_states VALUES (1, 1, 151, 'unverified', 'verify', '2014-03-25 12:27:22.665735', '192.168.4.75', NULL);
INSERT INTO action_states VALUES (1, 1, 152, 'pending_review', 'verify', '2014-03-25 12:27:22.666692', '192.168.4.75', NULL);
INSERT INTO action_states VALUES (1, 1, 153, 'locked', 'checkout', '2014-03-25 12:27:22.667114', '192.168.4.75', 99999992);
INSERT INTO action_states VALUES (1, 1, 154, 'accepted', 'accept', '2014-03-25 12:27:22.667676', '192.168.4.75', 99999993);
INSERT INTO action_states VALUES (2, 1, 3, 'reg', 'initial', '2014-03-25 12:27:22.668105', '192.168.4.85', NULL);
INSERT INTO action_states VALUES (2, 1, 4, 'unverified', 'verifymail', '2014-03-25 12:27:22.668441', '192.168.4.85', NULL);
INSERT INTO action_states VALUES (3, 1, 5, 'reg', 'initial', '2014-03-25 12:27:22.668892', '192.168.4.85', NULL);
INSERT INTO action_states VALUES (3, 1, 6, 'unpaid', 'paymail', '2014-03-25 12:27:22.669243', '192.168.4.85', NULL);
INSERT INTO action_states VALUES (4, 1, 7, 'reg', 'initial', '2014-03-25 12:27:22.669594', '192.168.4.85', NULL);
INSERT INTO action_states VALUES (4, 1, 8, 'unverified', 'verifymail', '2014-03-25 12:27:22.670012', '192.168.4.85', NULL);
INSERT INTO action_states VALUES (6, 1, 50, 'reg', 'initial', '2014-03-25 12:27:22.670335', '192.168.4.75', NULL);
INSERT INTO action_states VALUES (6, 1, 51, 'unpaid', 'newad', '2014-03-25 12:27:22.670729', '192.168.4.75', NULL);
INSERT INTO action_states VALUES (6, 1, 52, 'pending_review', 'pay', '2014-03-25 12:27:22.671089', '192.168.4.75', 99999998);
INSERT INTO action_states VALUES (6, 1, 53, 'accepted', 'accept', '2014-03-25 12:27:22.671454', '192.168.4.75', 99999999);
INSERT INTO action_states VALUES (9, 1, 40, 'reg', 'initial', '2014-03-25 12:27:22.67192', '192.168.4.75', NULL);
INSERT INTO action_states VALUES (9, 1, 41, 'unpaid', 'newad', '2014-03-25 12:27:22.672264', '192.168.4.75', NULL);
INSERT INTO action_states VALUES (9, 1, 42, 'pending_review', 'pay', '2014-03-25 12:27:22.672592', '192.168.4.75', 99999918);
INSERT INTO action_states VALUES (9, 1, 43, 'refused', 'refuse', '2014-03-25 12:27:22.673037', '192.168.4.75', 99999919);
INSERT INTO action_states VALUES (12, 1, 160, 'reg', 'initial', '2014-03-25 12:27:22.67341', '192.168.4.75', NULL);
INSERT INTO action_states VALUES (12, 1, 161, 'unverified', 'verify', '2014-03-25 12:27:22.673821', '192.168.4.75', NULL);
INSERT INTO action_states VALUES (12, 1, 162, 'pending_review', 'verify', '2014-03-25 12:27:22.674141', '192.168.4.75', NULL);
INSERT INTO action_states VALUES (12, 1, 163, 'locked', 'checkout', '2014-03-25 12:27:22.674453', '192.168.4.75', 99999994);
INSERT INTO action_states VALUES (12, 1, 164, 'accepted', 'accept', '2014-03-25 12:27:22.674902', '192.168.4.75', 99999995);
INSERT INTO action_states VALUES (10, 1, 54, 'reg', 'initial', '2014-03-25 11:20:00', '192.168.4.75', NULL);
INSERT INTO action_states VALUES (10, 1, 55, 'unpaid', 'newad', '2014-03-25 12:27:22.676392', '192.168.4.75', NULL);
INSERT INTO action_states VALUES (10, 1, 56, 'pending_review', 'pay', '2014-03-25 12:27:22.676812', '192.168.4.75', 99999998);
INSERT INTO action_states VALUES (10, 1, 57, 'accepted', 'accept', '2014-03-25 12:27:22.677183', '192.168.4.75', 99999999);
INSERT INTO action_states VALUES (20, 1, 200, 'reg', 'initial', '2014-03-25 12:45:07.473968', '10.0.1.188', NULL);
INSERT INTO action_states VALUES (20, 1, 201, 'unverified', 'verifymail', '2014-03-25 12:45:07.473968', '10.0.1.188', NULL);
INSERT INTO action_states VALUES (20, 1, 202, 'pending_review', 'verify', '2014-03-25 12:45:07.537222', '10.0.1.188', NULL);
INSERT INTO action_states VALUES (21, 1, 203, 'reg', 'initial', '2014-03-25 12:47:06.699678', '10.0.1.188', NULL);
INSERT INTO action_states VALUES (21, 1, 204, 'unverified', 'verifymail', '2014-03-25 12:47:06.699678', '10.0.1.188', NULL);
INSERT INTO action_states VALUES (21, 1, 205, 'pending_review', 'verify', '2014-03-25 12:47:06.761654', '10.0.1.188', NULL);
INSERT INTO action_states VALUES (22, 1, 206, 'reg', 'initial', '2014-03-25 12:49:15.012072', '10.0.1.188', NULL);
INSERT INTO action_states VALUES (22, 1, 207, 'unverified', 'verifymail', '2014-03-25 12:49:15.012072', '10.0.1.188', NULL);
INSERT INTO action_states VALUES (22, 1, 208, 'pending_review', 'verify', '2014-03-25 12:49:15.075333', '10.0.1.188', NULL);
INSERT INTO action_states VALUES (23, 1, 209, 'reg', 'initial', '2014-03-25 12:50:36.73864', '10.0.1.188', NULL);
INSERT INTO action_states VALUES (23, 1, 210, 'unverified', 'verifymail', '2014-03-25 12:50:36.73864', '10.0.1.188', NULL);
INSERT INTO action_states VALUES (23, 1, 211, 'pending_review', 'verify', '2014-03-25 12:50:36.798354', '10.0.1.188', NULL);
INSERT INTO action_states VALUES (24, 1, 212, 'reg', 'initial', '2014-03-25 12:51:24.215573', '10.0.1.188', NULL);
INSERT INTO action_states VALUES (24, 1, 213, 'unverified', 'verifymail', '2014-03-25 12:51:24.215573', '10.0.1.188', NULL);
INSERT INTO action_states VALUES (24, 1, 214, 'pending_review', 'verify', '2014-03-25 12:51:24.238567', '10.0.1.188', NULL);
INSERT INTO action_states VALUES (25, 1, 215, 'reg', 'initial', '2014-03-25 12:53:47.06352', '10.0.1.188', NULL);
INSERT INTO action_states VALUES (25, 1, 216, 'unverified', 'verifymail', '2014-03-25 12:53:47.06352', '10.0.1.188', NULL);
INSERT INTO action_states VALUES (25, 1, 217, 'pending_review', 'verify', '2014-03-25 12:53:47.12196', '10.0.1.188', NULL);
INSERT INTO action_states VALUES (26, 1, 218, 'reg', 'initial', '2014-03-25 12:54:25.262888', '10.0.1.188', NULL);
INSERT INTO action_states VALUES (26, 1, 219, 'unverified', 'verifymail', '2014-03-25 12:54:25.262888', '10.0.1.188', NULL);
INSERT INTO action_states VALUES (26, 1, 220, 'pending_review', 'verify', '2014-03-25 12:54:25.286434', '10.0.1.188', NULL);
INSERT INTO action_states VALUES (27, 1, 221, 'reg', 'initial', '2014-03-25 12:55:12.66257', '10.0.1.188', NULL);
INSERT INTO action_states VALUES (27, 1, 222, 'unverified', 'verifymail', '2014-03-25 12:55:12.66257', '10.0.1.188', NULL);
INSERT INTO action_states VALUES (27, 1, 223, 'pending_review', 'verify', '2014-03-25 12:55:12.700001', '10.0.1.188', NULL);
INSERT INTO action_states VALUES (28, 1, 224, 'reg', 'initial', '2014-03-25 12:55:57.057476', '10.0.1.188', NULL);
INSERT INTO action_states VALUES (28, 1, 225, 'unverified', 'verifymail', '2014-03-25 12:55:57.057476', '10.0.1.188', NULL);
INSERT INTO action_states VALUES (28, 1, 226, 'pending_review', 'verify', '2014-03-25 12:55:57.083389', '10.0.1.188', NULL);
INSERT INTO action_states VALUES (29, 1, 227, 'reg', 'initial', '2014-03-25 12:58:21.703978', '10.0.1.188', NULL);
INSERT INTO action_states VALUES (29, 1, 228, 'unverified', 'verifymail', '2014-03-25 12:58:21.703978', '10.0.1.188', NULL);
INSERT INTO action_states VALUES (29, 1, 229, 'pending_review', 'verify', '2014-03-25 12:58:21.768657', '10.0.1.188', NULL);
INSERT INTO action_states VALUES (30, 1, 230, 'reg', 'initial', '2014-03-25 12:59:07.160838', '10.0.1.188', NULL);
INSERT INTO action_states VALUES (30, 1, 231, 'unverified', 'verifymail', '2014-03-25 12:59:07.160838', '10.0.1.188', NULL);
INSERT INTO action_states VALUES (30, 1, 232, 'pending_review', 'verify', '2014-03-25 12:59:07.186389', '10.0.1.188', NULL);
INSERT INTO action_states VALUES (31, 1, 233, 'reg', 'initial', '2014-03-25 12:59:41.258944', '10.0.1.188', NULL);
INSERT INTO action_states VALUES (31, 1, 234, 'unverified', 'verifymail', '2014-03-25 12:59:41.258944', '10.0.1.188', NULL);
INSERT INTO action_states VALUES (31, 1, 235, 'pending_review', 'verify', '2014-03-25 12:59:41.292567', '10.0.1.188', NULL);
INSERT INTO action_states VALUES (32, 1, 236, 'reg', 'initial', '2014-03-25 14:20:13.64879', '10.0.1.188', NULL);
INSERT INTO action_states VALUES (32, 1, 237, 'unverified', 'verifymail', '2014-03-25 14:20:13.64879', '10.0.1.188', NULL);
INSERT INTO action_states VALUES (32, 1, 238, 'pending_review', 'verify', '2014-03-25 14:20:13.714161', '10.0.1.188', NULL);
INSERT INTO action_states VALUES (33, 1, 239, 'reg', 'initial', '2014-03-25 14:20:52.529498', '10.0.1.188', NULL);
INSERT INTO action_states VALUES (33, 1, 240, 'unverified', 'verifymail', '2014-03-25 14:20:52.529498', '10.0.1.188', NULL);
INSERT INTO action_states VALUES (33, 1, 241, 'pending_review', 'verify', '2014-03-25 14:20:52.551122', '10.0.1.188', NULL);
INSERT INTO action_states VALUES (34, 1, 242, 'reg', 'initial', '2014-03-25 14:21:40.986298', '10.0.1.188', NULL);
INSERT INTO action_states VALUES (34, 1, 243, 'unverified', 'verifymail', '2014-03-25 14:21:40.986298', '10.0.1.188', NULL);
INSERT INTO action_states VALUES (34, 1, 244, 'pending_review', 'verify', '2014-03-25 14:21:41.011638', '10.0.1.188', NULL);
INSERT INTO action_states VALUES (20, 1, 245, 'locked', 'checkout', '2014-03-25 14:22:17.982753', '10.0.1.188', 3);
INSERT INTO action_states VALUES (21, 1, 246, 'locked', 'checkout', '2014-03-25 14:22:17.982753', '10.0.1.188', 3);
INSERT INTO action_states VALUES (20, 1, 247, 'accepted', 'accept', '2014-03-25 14:22:22.424495', '10.0.1.188', 10);
INSERT INTO action_states VALUES (21, 1, 248, 'accepted', 'accept', '2014-03-25 14:22:25.746218', '10.0.1.188', 11);
INSERT INTO action_states VALUES (22, 1, 249, 'locked', 'checkout', '2014-03-25 14:22:27.471215', '10.0.1.188', 12);
INSERT INTO action_states VALUES (23, 1, 250, 'locked', 'checkout', '2014-03-25 14:22:27.471215', '10.0.1.188', 12);
INSERT INTO action_states VALUES (22, 1, 251, 'accepted', 'accept', '2014-03-25 14:22:30.561154', '10.0.1.188', 19);
INSERT INTO action_states VALUES (23, 1, 252, 'accepted', 'accept', '2014-03-25 14:22:32.972419', '10.0.1.188', 20);
INSERT INTO action_states VALUES (24, 1, 253, 'locked', 'checkout', '2014-03-25 14:22:35.004493', '10.0.1.188', 21);
INSERT INTO action_states VALUES (25, 1, 254, 'locked', 'checkout', '2014-03-25 14:22:35.004493', '10.0.1.188', 21);
INSERT INTO action_states VALUES (24, 1, 255, 'accepted', 'accept', '2014-03-25 14:22:38.839343', '10.0.1.188', 28);
INSERT INTO action_states VALUES (25, 1, 256, 'accepted', 'accept', '2014-03-25 14:22:41.15456', '10.0.1.188', 29);
INSERT INTO action_states VALUES (26, 1, 257, 'locked', 'checkout', '2014-03-25 14:22:42.403915', '10.0.1.188', 30);
INSERT INTO action_states VALUES (27, 1, 258, 'locked', 'checkout', '2014-03-25 14:22:42.403915', '10.0.1.188', 30);
INSERT INTO action_states VALUES (26, 1, 259, 'accepted', 'accept', '2014-03-25 14:22:46.028443', '10.0.1.188', 37);
INSERT INTO action_states VALUES (27, 1, 260, 'accepted', 'accept', '2014-03-25 14:22:48.491856', '10.0.1.188', 38);
INSERT INTO action_states VALUES (28, 1, 261, 'locked', 'checkout', '2014-03-25 14:22:49.918724', '10.0.1.188', 39);
INSERT INTO action_states VALUES (29, 1, 262, 'locked', 'checkout', '2014-03-25 14:22:49.918724', '10.0.1.188', 39);
INSERT INTO action_states VALUES (28, 1, 263, 'accepted', 'accept', '2014-03-25 14:22:53.141107', '10.0.1.188', 46);
INSERT INTO action_states VALUES (29, 1, 264, 'accepted', 'accept', '2014-03-25 14:22:55.807858', '10.0.1.188', 47);
INSERT INTO action_states VALUES (30, 1, 265, 'locked', 'checkout', '2014-03-25 14:22:57.722281', '10.0.1.188', 48);
INSERT INTO action_states VALUES (31, 1, 266, 'locked', 'checkout', '2014-03-25 14:22:57.722281', '10.0.1.188', 48);
INSERT INTO action_states VALUES (30, 1, 267, 'accepted', 'accept', '2014-03-25 14:23:00.806579', '10.0.1.188', 55);
INSERT INTO action_states VALUES (31, 1, 268, 'accepted', 'accept', '2014-03-25 14:23:03.294265', '10.0.1.188', 56);
INSERT INTO action_states VALUES (32, 1, 269, 'locked', 'checkout', '2014-03-25 14:23:04.959752', '10.0.1.188', 57);
INSERT INTO action_states VALUES (33, 1, 270, 'locked', 'checkout', '2014-03-25 14:23:04.959752', '10.0.1.188', 57);
INSERT INTO action_states VALUES (32, 1, 271, 'accepted', 'accept', '2014-03-25 14:23:10.740153', '10.0.1.188', 64);
INSERT INTO action_states VALUES (33, 1, 272, 'accepted', 'accept', '2014-03-25 14:23:12.928341', '10.0.1.188', 65);
INSERT INTO action_states VALUES (34, 1, 273, 'locked', 'checkout', '2014-03-25 14:23:15.524971', '10.0.1.188', 66);
INSERT INTO action_states VALUES (34, 1, 274, 'accepted', 'accept', '2014-03-25 14:23:18.307154', '10.0.1.188', 70);
INSERT INTO action_states VALUES (20, 2, 275, 'reg', 'initial', '2014-03-25 14:28:12.730485', '127.0.0.1', NULL);
INSERT INTO action_states VALUES (20, 2, 276, 'accepted', 'accept', '2014-03-25 14:28:12.730485', '127.0.0.1', NULL);
INSERT INTO action_states VALUES (21, 2, 277, 'reg', 'initial', '2014-03-25 14:28:12.769973', '127.0.0.1', NULL);
INSERT INTO action_states VALUES (21, 2, 278, 'accepted', 'accept', '2014-03-25 14:28:12.769973', '127.0.0.1', NULL);
INSERT INTO action_states VALUES (22, 2, 279, 'reg', 'initial', '2014-03-25 14:28:12.780896', '127.0.0.1', NULL);
INSERT INTO action_states VALUES (22, 2, 280, 'accepted', 'accept', '2014-03-25 14:28:12.780896', '127.0.0.1', NULL);
INSERT INTO action_states VALUES (23, 2, 281, 'reg', 'initial', '2014-03-25 14:28:12.791657', '127.0.0.1', NULL);
INSERT INTO action_states VALUES (23, 2, 282, 'accepted', 'accept', '2014-03-25 14:28:12.791657', '127.0.0.1', NULL);
INSERT INTO action_states VALUES (24, 2, 283, 'reg', 'initial', '2014-03-25 14:28:12.80113', '127.0.0.1', NULL);
INSERT INTO action_states VALUES (24, 2, 284, 'accepted', 'accept', '2014-03-25 14:28:12.80113', '127.0.0.1', NULL);
INSERT INTO action_states VALUES (25, 2, 285, 'reg', 'initial', '2014-03-25 14:28:12.810612', '127.0.0.1', NULL);
INSERT INTO action_states VALUES (25, 2, 286, 'accepted', 'accept', '2014-03-25 14:28:12.810612', '127.0.0.1', NULL);
INSERT INTO action_states VALUES (26, 2, 287, 'reg', 'initial', '2014-03-25 14:28:12.820152', '127.0.0.1', NULL);
INSERT INTO action_states VALUES (26, 2, 288, 'accepted', 'accept', '2014-03-25 14:28:12.820152', '127.0.0.1', NULL);
INSERT INTO action_states VALUES (27, 2, 289, 'reg', 'initial', '2014-03-25 14:28:12.829219', '127.0.0.1', NULL);
INSERT INTO action_states VALUES (27, 2, 290, 'accepted', 'accept', '2014-03-25 14:28:12.829219', '127.0.0.1', NULL);
INSERT INTO action_states VALUES (28, 2, 291, 'reg', 'initial', '2014-03-25 14:28:12.83808', '127.0.0.1', NULL);
INSERT INTO action_states VALUES (28, 2, 292, 'accepted', 'accept', '2014-03-25 14:28:12.83808', '127.0.0.1', NULL);
INSERT INTO action_states VALUES (29, 2, 293, 'reg', 'initial', '2014-03-25 14:28:12.847228', '127.0.0.1', NULL);
INSERT INTO action_states VALUES (29, 2, 294, 'accepted', 'accept', '2014-03-25 14:28:12.847228', '127.0.0.1', NULL);
INSERT INTO action_states VALUES (30, 2, 295, 'reg', 'initial', '2014-03-25 14:28:12.856903', '127.0.0.1', NULL);
INSERT INTO action_states VALUES (30, 2, 296, 'accepted', 'accept', '2014-03-25 14:28:12.856903', '127.0.0.1', NULL);
INSERT INTO action_states VALUES (31, 2, 297, 'reg', 'initial', '2014-03-25 14:28:12.868763', '127.0.0.1', NULL);
INSERT INTO action_states VALUES (31, 2, 298, 'accepted', 'accept', '2014-03-25 14:28:12.868763', '127.0.0.1', NULL);
INSERT INTO action_states VALUES (32, 2, 299, 'reg', 'initial', '2014-03-25 14:28:12.878292', '127.0.0.1', NULL);
INSERT INTO action_states VALUES (32, 2, 300, 'accepted', 'accept', '2014-03-25 14:28:12.878292', '127.0.0.1', NULL);
INSERT INTO action_states VALUES (33, 2, 301, 'reg', 'initial', '2014-03-25 14:28:12.887509', '127.0.0.1', NULL);
INSERT INTO action_states VALUES (33, 2, 302, 'accepted', 'accept', '2014-03-25 14:28:12.887509', '127.0.0.1', NULL);
INSERT INTO action_states VALUES (34, 2, 303, 'reg', 'initial', '2014-03-25 14:28:12.897723', '127.0.0.1', NULL);
INSERT INTO action_states VALUES (34, 2, 304, 'accepted', 'accept', '2014-03-25 14:28:12.897723', '127.0.0.1', NULL);
INSERT INTO action_states VALUES (35, 1, 305, 'reg', 'initial', '2014-03-26 09:48:21.196134', '10.0.1.188', NULL);
INSERT INTO action_states VALUES (35, 1, 306, 'unverified', 'verifymail', '2014-03-26 09:48:21.196134', '10.0.1.188', NULL);
INSERT INTO action_states VALUES (35, 1, 307, 'pending_review', 'verify', '2014-03-26 09:48:21.259811', '10.0.1.188', NULL);
INSERT INTO action_states VALUES (36, 1, 308, 'reg', 'initial', '2014-03-26 09:49:02.146434', '10.0.1.188', NULL);
INSERT INTO action_states VALUES (36, 1, 309, 'unverified', 'verifymail', '2014-03-26 09:49:02.146434', '10.0.1.188', NULL);
INSERT INTO action_states VALUES (36, 1, 310, 'pending_review', 'verify', '2014-03-26 09:49:02.201949', '10.0.1.188', NULL);
INSERT INTO action_states VALUES (35, 1, 311, 'locked', 'checkout', '2014-03-26 09:49:24.627781', '10.0.1.188', 76);
INSERT INTO action_states VALUES (36, 1, 312, 'locked', 'checkout', '2014-03-26 09:49:24.627781', '10.0.1.188', 76);
INSERT INTO action_states VALUES (35, 1, 313, 'accepted', 'accept', '2014-03-26 09:49:29.791808', '10.0.1.188', 83);
INSERT INTO action_states VALUES (36, 1, 314, 'accepted', 'accept', '2014-03-26 09:49:33.725943', '10.0.1.188', 84);
INSERT INTO action_states VALUES (35, 2, 315, 'reg', 'initial', '2014-03-26 09:50:01.744409', '127.0.0.1', NULL);
INSERT INTO action_states VALUES (35, 2, 316, 'accepted', 'accept', '2014-03-26 09:50:01.744409', '127.0.0.1', NULL);
INSERT INTO action_states VALUES (36, 2, 317, 'reg', 'initial', '2014-03-26 09:50:01.758191', '127.0.0.1', NULL);
INSERT INTO action_states VALUES (36, 2, 318, 'accepted', 'accept', '2014-03-26 09:50:01.758191', '127.0.0.1', NULL);


--
-- Data for Name: ad_changes; Type: TABLE DATA; Schema: public; Owner: joaquin
--

INSERT INTO ad_changes VALUES (20, 2, 276, true, 'gallery', NULL, '2014-04-01 14:28:12');
INSERT INTO ad_changes VALUES (21, 2, 278, true, 'gallery', NULL, '2014-04-01 14:28:12');
INSERT INTO ad_changes VALUES (22, 2, 280, true, 'gallery', NULL, '2014-04-01 14:28:12');
INSERT INTO ad_changes VALUES (23, 2, 282, true, 'gallery', NULL, '2014-04-01 14:28:12');
INSERT INTO ad_changes VALUES (24, 2, 284, true, 'gallery', NULL, '2014-04-01 14:28:12');
INSERT INTO ad_changes VALUES (25, 2, 286, true, 'gallery', NULL, '2014-04-01 14:28:12');
INSERT INTO ad_changes VALUES (26, 2, 288, true, 'gallery', NULL, '2014-04-01 14:28:12');
INSERT INTO ad_changes VALUES (27, 2, 290, true, 'gallery', NULL, '2014-04-01 14:28:12');
INSERT INTO ad_changes VALUES (28, 2, 292, true, 'gallery', NULL, '2014-04-01 14:28:12');
INSERT INTO ad_changes VALUES (29, 2, 294, true, 'gallery', NULL, '2014-04-01 14:28:12');
INSERT INTO ad_changes VALUES (30, 2, 296, true, 'gallery', NULL, '2014-04-01 14:28:12');
INSERT INTO ad_changes VALUES (31, 2, 298, true, 'gallery', NULL, '2014-04-01 14:28:12');
INSERT INTO ad_changes VALUES (32, 2, 300, true, 'gallery', NULL, '2014-04-01 14:28:12');
INSERT INTO ad_changes VALUES (33, 2, 302, true, 'gallery', NULL, '2014-04-01 14:28:12');
INSERT INTO ad_changes VALUES (34, 2, 304, true, 'gallery', NULL, '2014-04-01 14:28:12');
INSERT INTO ad_changes VALUES (35, 2, 316, true, 'gallery', NULL, '2014-04-02 09:50:01');
INSERT INTO ad_changes VALUES (36, 2, 318, true, 'gallery', NULL, '2014-04-02 09:50:01');


--
-- Data for Name: ad_codes; Type: TABLE DATA; Schema: public; Owner: joaquin
--

INSERT INTO ad_codes VALUES (12345, 'pay');
INSERT INTO ad_codes VALUES (65432, 'pay');
INSERT INTO ad_codes VALUES (32323, 'pay');
INSERT INTO ad_codes VALUES (54545, 'pay');
INSERT INTO ad_codes VALUES (90001, 'pay');
INSERT INTO ad_codes VALUES (90002, 'pay');
INSERT INTO ad_codes VALUES (90003, 'pay');
INSERT INTO ad_codes VALUES (90004, 'pay');
INSERT INTO ad_codes VALUES (90005, 'pay');
INSERT INTO ad_codes VALUES (90006, 'pay');
INSERT INTO ad_codes VALUES (90007, 'pay');
INSERT INTO ad_codes VALUES (90008, 'pay');
INSERT INTO ad_codes VALUES (90009, 'pay');
INSERT INTO ad_codes VALUES (90010, 'pay');
INSERT INTO ad_codes VALUES (90011, 'pay');
INSERT INTO ad_codes VALUES (90012, 'pay');
INSERT INTO ad_codes VALUES (90013, 'pay');
INSERT INTO ad_codes VALUES (90014, 'pay');
INSERT INTO ad_codes VALUES (90015, 'pay');
INSERT INTO ad_codes VALUES (90016, 'pay');
INSERT INTO ad_codes VALUES (90017, 'pay');
INSERT INTO ad_codes VALUES (90018, 'pay');
INSERT INTO ad_codes VALUES (90019, 'pay');
INSERT INTO ad_codes VALUES (90020, 'pay');
INSERT INTO ad_codes VALUES (90021, 'pay');
INSERT INTO ad_codes VALUES (90022, 'pay');
INSERT INTO ad_codes VALUES (90023, 'pay');
INSERT INTO ad_codes VALUES (90024, 'pay');
INSERT INTO ad_codes VALUES (90025, 'pay');
INSERT INTO ad_codes VALUES (90026, 'pay');
INSERT INTO ad_codes VALUES (90027, 'pay');
INSERT INTO ad_codes VALUES (90028, 'pay');
INSERT INTO ad_codes VALUES (90029, 'pay');
INSERT INTO ad_codes VALUES (90030, 'pay');
INSERT INTO ad_codes VALUES (90031, 'pay');
INSERT INTO ad_codes VALUES (90032, 'pay');
INSERT INTO ad_codes VALUES (90033, 'pay');
INSERT INTO ad_codes VALUES (90034, 'pay');
INSERT INTO ad_codes VALUES (90035, 'pay');
INSERT INTO ad_codes VALUES (90036, 'pay');
INSERT INTO ad_codes VALUES (90037, 'pay');
INSERT INTO ad_codes VALUES (90038, 'pay');
INSERT INTO ad_codes VALUES (90039, 'pay');
INSERT INTO ad_codes VALUES (90040, 'pay');
INSERT INTO ad_codes VALUES (42023, 'pay');
INSERT INTO ad_codes VALUES (74046, 'pay');
INSERT INTO ad_codes VALUES (16069, 'pay');
INSERT INTO ad_codes VALUES (48092, 'pay');
INSERT INTO ad_codes VALUES (80115, 'pay');
INSERT INTO ad_codes VALUES (22138, 'pay');
INSERT INTO ad_codes VALUES (54161, 'pay');
INSERT INTO ad_codes VALUES (86184, 'pay');
INSERT INTO ad_codes VALUES (28207, 'pay');
INSERT INTO ad_codes VALUES (60230, 'pay');
INSERT INTO ad_codes VALUES (92253, 'pay');
INSERT INTO ad_codes VALUES (34276, 'pay');
INSERT INTO ad_codes VALUES (66299, 'pay');
INSERT INTO ad_codes VALUES (98322, 'pay');
INSERT INTO ad_codes VALUES (40345, 'pay');
INSERT INTO ad_codes VALUES (72368, 'pay');
INSERT INTO ad_codes VALUES (14391, 'pay');
INSERT INTO ad_codes VALUES (46414, 'pay');
INSERT INTO ad_codes VALUES (78437, 'pay');
INSERT INTO ad_codes VALUES (20460, 'pay');
INSERT INTO ad_codes VALUES (52483, 'pay');
INSERT INTO ad_codes VALUES (84506, 'pay');
INSERT INTO ad_codes VALUES (26529, 'pay');
INSERT INTO ad_codes VALUES (58552, 'pay');
INSERT INTO ad_codes VALUES (90575, 'pay');
INSERT INTO ad_codes VALUES (32598, 'pay');
INSERT INTO ad_codes VALUES (64621, 'pay');
INSERT INTO ad_codes VALUES (96644, 'pay');
INSERT INTO ad_codes VALUES (38667, 'pay');
INSERT INTO ad_codes VALUES (70690, 'pay');
INSERT INTO ad_codes VALUES (12713, 'pay');
INSERT INTO ad_codes VALUES (44736, 'pay');
INSERT INTO ad_codes VALUES (76759, 'pay');
INSERT INTO ad_codes VALUES (18782, 'pay');
INSERT INTO ad_codes VALUES (50805, 'pay');
INSERT INTO ad_codes VALUES (82828, 'pay');
INSERT INTO ad_codes VALUES (24851, 'pay');
INSERT INTO ad_codes VALUES (56874, 'pay');
INSERT INTO ad_codes VALUES (88897, 'pay');
INSERT INTO ad_codes VALUES (30920, 'pay');
INSERT INTO ad_codes VALUES (62943, 'pay');
INSERT INTO ad_codes VALUES (94966, 'pay');
INSERT INTO ad_codes VALUES (36989, 'pay');
INSERT INTO ad_codes VALUES (69012, 'pay');
INSERT INTO ad_codes VALUES (11035, 'pay');
INSERT INTO ad_codes VALUES (43058, 'pay');
INSERT INTO ad_codes VALUES (75081, 'pay');
INSERT INTO ad_codes VALUES (17104, 'pay');
INSERT INTO ad_codes VALUES (49127, 'pay');
INSERT INTO ad_codes VALUES (81150, 'pay');
INSERT INTO ad_codes VALUES (23173, 'pay');
INSERT INTO ad_codes VALUES (55196, 'pay');
INSERT INTO ad_codes VALUES (87219, 'pay');
INSERT INTO ad_codes VALUES (29242, 'pay');
INSERT INTO ad_codes VALUES (61265, 'pay');
INSERT INTO ad_codes VALUES (93288, 'pay');
INSERT INTO ad_codes VALUES (912352138, 'verify');
INSERT INTO ad_codes VALUES (147744161, 'verify');
INSERT INTO ad_codes VALUES (283136184, 'verify');
INSERT INTO ad_codes VALUES (418528207, 'verify');
INSERT INTO ad_codes VALUES (553920230, 'verify');
INSERT INTO ad_codes VALUES (689312253, 'verify');
INSERT INTO ad_codes VALUES (824704276, 'verify');
INSERT INTO ad_codes VALUES (960096299, 'verify');
INSERT INTO ad_codes VALUES (195488322, 'verify');
INSERT INTO ad_codes VALUES (330880345, 'verify');
INSERT INTO ad_codes VALUES (466272368, 'verify');
INSERT INTO ad_codes VALUES (601664391, 'verify');
INSERT INTO ad_codes VALUES (737056414, 'verify');
INSERT INTO ad_codes VALUES (872448437, 'verify');
INSERT INTO ad_codes VALUES (107840460, 'verify');
INSERT INTO ad_codes VALUES (243232483, 'verify');
INSERT INTO ad_codes VALUES (378624506, 'verify');
INSERT INTO ad_codes VALUES (514016529, 'verify');
INSERT INTO ad_codes VALUES (649408552, 'verify');
INSERT INTO ad_codes VALUES (784800575, 'verify');
INSERT INTO ad_codes VALUES (920192598, 'verify');
INSERT INTO ad_codes VALUES (155584621, 'verify');
INSERT INTO ad_codes VALUES (290976644, 'verify');
INSERT INTO ad_codes VALUES (426368667, 'verify');
INSERT INTO ad_codes VALUES (561760690, 'verify');
INSERT INTO ad_codes VALUES (697152713, 'verify');
INSERT INTO ad_codes VALUES (832544736, 'verify');
INSERT INTO ad_codes VALUES (967936759, 'verify');
INSERT INTO ad_codes VALUES (203328782, 'verify');
INSERT INTO ad_codes VALUES (338720805, 'verify');
INSERT INTO ad_codes VALUES (474112828, 'verify');
INSERT INTO ad_codes VALUES (609504851, 'verify');
INSERT INTO ad_codes VALUES (744896874, 'verify');
INSERT INTO ad_codes VALUES (880288897, 'verify');
INSERT INTO ad_codes VALUES (115680920, 'verify');
INSERT INTO ad_codes VALUES (251072943, 'verify');
INSERT INTO ad_codes VALUES (386464966, 'verify');
INSERT INTO ad_codes VALUES (521856989, 'verify');
INSERT INTO ad_codes VALUES (657249012, 'verify');
INSERT INTO ad_codes VALUES (792641035, 'verify');
INSERT INTO ad_codes VALUES (928033058, 'verify');
INSERT INTO ad_codes VALUES (163425081, 'verify');
INSERT INTO ad_codes VALUES (298817104, 'verify');
INSERT INTO ad_codes VALUES (434209127, 'verify');
INSERT INTO ad_codes VALUES (569601150, 'verify');
INSERT INTO ad_codes VALUES (704993173, 'verify');
INSERT INTO ad_codes VALUES (840385196, 'verify');
INSERT INTO ad_codes VALUES (975777219, 'verify');
INSERT INTO ad_codes VALUES (211169242, 'verify');
INSERT INTO ad_codes VALUES (346561265, 'verify');
INSERT INTO ad_codes VALUES (481953288, 'verify');
INSERT INTO ad_codes VALUES (617345311, 'verify');
INSERT INTO ad_codes VALUES (752737334, 'verify');
INSERT INTO ad_codes VALUES (888129357, 'verify');
INSERT INTO ad_codes VALUES (123521380, 'verify');
INSERT INTO ad_codes VALUES (258913403, 'verify');
INSERT INTO ad_codes VALUES (394305426, 'verify');
INSERT INTO ad_codes VALUES (529697449, 'verify');
INSERT INTO ad_codes VALUES (665089472, 'verify');
INSERT INTO ad_codes VALUES (800481495, 'verify');
INSERT INTO ad_codes VALUES (935873518, 'verify');
INSERT INTO ad_codes VALUES (171265541, 'verify');
INSERT INTO ad_codes VALUES (306657564, 'verify');
INSERT INTO ad_codes VALUES (442049587, 'verify');
INSERT INTO ad_codes VALUES (577441610, 'verify');
INSERT INTO ad_codes VALUES (712833633, 'verify');
INSERT INTO ad_codes VALUES (848225656, 'verify');
INSERT INTO ad_codes VALUES (983617679, 'verify');
INSERT INTO ad_codes VALUES (219009702, 'verify');
INSERT INTO ad_codes VALUES (354401725, 'verify');
INSERT INTO ad_codes VALUES (489793748, 'verify');
INSERT INTO ad_codes VALUES (625185771, 'verify');
INSERT INTO ad_codes VALUES (760577794, 'verify');
INSERT INTO ad_codes VALUES (895969817, 'verify');
INSERT INTO ad_codes VALUES (131361840, 'verify');
INSERT INTO ad_codes VALUES (266753863, 'verify');
INSERT INTO ad_codes VALUES (402145886, 'verify');
INSERT INTO ad_codes VALUES (537537909, 'verify');
INSERT INTO ad_codes VALUES (672929932, 'verify');
INSERT INTO ad_codes VALUES (808321955, 'verify');
INSERT INTO ad_codes VALUES (943713978, 'verify');
INSERT INTO ad_codes VALUES (179106001, 'verify');
INSERT INTO ad_codes VALUES (314498024, 'verify');
INSERT INTO ad_codes VALUES (449890047, 'verify');
INSERT INTO ad_codes VALUES (585282070, 'verify');
INSERT INTO ad_codes VALUES (35311, 'pay');
INSERT INTO ad_codes VALUES (67334, 'pay');
INSERT INTO ad_codes VALUES (99357, 'pay');
INSERT INTO ad_codes VALUES (41380, 'pay');
INSERT INTO ad_codes VALUES (73403, 'pay');
INSERT INTO ad_codes VALUES (15426, 'pay');
INSERT INTO ad_codes VALUES (47449, 'pay');
INSERT INTO ad_codes VALUES (79472, 'pay');
INSERT INTO ad_codes VALUES (21495, 'pay');
INSERT INTO ad_codes VALUES (53518, 'pay');
INSERT INTO ad_codes VALUES (85541, 'pay');
INSERT INTO ad_codes VALUES (27564, 'pay');
INSERT INTO ad_codes VALUES (59587, 'pay');
INSERT INTO ad_codes VALUES (91610, 'pay');
INSERT INTO ad_codes VALUES (33633, 'pay');
INSERT INTO ad_codes VALUES (65656, 'pay');
INSERT INTO ad_codes VALUES (97679, 'pay');


--
-- Data for Name: ad_image_changes; Type: TABLE DATA; Schema: public; Owner: joaquin
--

INSERT INTO ad_image_changes VALUES (20, 1, 247, 0, '5452331900.jpg', false);
INSERT INTO ad_image_changes VALUES (21, 1, 248, 0, '5460855219.jpg', false);
INSERT INTO ad_image_changes VALUES (22, 1, 251, 0, '5469378538.jpg', false);
INSERT INTO ad_image_changes VALUES (23, 1, 252, 0, '5477901857.jpg', false);
INSERT INTO ad_image_changes VALUES (24, 1, 255, 0, '5486425176.jpg', false);
INSERT INTO ad_image_changes VALUES (25, 1, 256, 0, '5494948495.jpg', false);
INSERT INTO ad_image_changes VALUES (26, 1, 259, 0, NULL, false);
INSERT INTO ad_image_changes VALUES (27, 1, 260, 0, '5403471814.jpg', false);
INSERT INTO ad_image_changes VALUES (28, 1, 263, 0, NULL, false);
INSERT INTO ad_image_changes VALUES (29, 1, 264, 0, '5411995133.jpg', false);
INSERT INTO ad_image_changes VALUES (30, 1, 267, 0, NULL, false);
INSERT INTO ad_image_changes VALUES (31, 1, 268, 0, '5420518452.jpg', false);
INSERT INTO ad_image_changes VALUES (32, 1, 271, 0, NULL, false);
INSERT INTO ad_image_changes VALUES (33, 1, 272, 0, NULL, false);
INSERT INTO ad_image_changes VALUES (34, 1, 274, 0, '5429041771.jpg', false);
INSERT INTO ad_image_changes VALUES (35, 1, 313, 0, NULL, false);
INSERT INTO ad_image_changes VALUES (36, 1, 314, 0, NULL, false);


--
-- Data for Name: ad_images; Type: TABLE DATA; Schema: public; Owner: joaquin
--



--
-- Data for Name: ad_images_digests; Type: TABLE DATA; Schema: public; Owner: joaquin
--

INSERT INTO ad_images_digests VALUES (20, '5452331900.jpg', '09f3bacfab986cd262d8ff3f511a7f44', 50, false);
INSERT INTO ad_images_digests VALUES (21, '5460855219.jpg', '09f3bacfab986cd262d8ff3f511a7f44', 50, false);
INSERT INTO ad_images_digests VALUES (22, '5469378538.jpg', '09f3bacfab986cd262d8ff3f511a7f44', 50, false);
INSERT INTO ad_images_digests VALUES (23, '5477901857.jpg', '09f3bacfab986cd262d8ff3f511a7f44', 50, false);
INSERT INTO ad_images_digests VALUES (24, '5486425176.jpg', '09f3bacfab986cd262d8ff3f511a7f44', 50, false);
INSERT INTO ad_images_digests VALUES (25, '5494948495.jpg', '09f3bacfab986cd262d8ff3f511a7f44', 50, false);
INSERT INTO ad_images_digests VALUES (27, '5403471814.jpg', '09f3bacfab986cd262d8ff3f511a7f44', 50, false);
INSERT INTO ad_images_digests VALUES (29, '5411995133.jpg', '09f3bacfab986cd262d8ff3f511a7f44', 50, false);
INSERT INTO ad_images_digests VALUES (31, '5420518452.jpg', '09f3bacfab986cd262d8ff3f511a7f44', 50, false);
INSERT INTO ad_images_digests VALUES (34, '5429041771.jpg', '09f3bacfab986cd262d8ff3f511a7f44', 50, false);


--
-- Data for Name: ad_media; Type: TABLE DATA; Schema: public; Owner: joaquin
--

INSERT INTO ad_media VALUES (100012344, 6, 0, '2014-03-25 12:27:22.677517', 'image', NULL, NULL, NULL);
INSERT INTO ad_media VALUES (100012345, 6, 1, '2014-03-25 12:27:22.678568', 'image', NULL, NULL, NULL);
INSERT INTO ad_media VALUES (100012346, 6, 2, '2014-03-25 12:27:22.6791', 'image', NULL, NULL, NULL);
INSERT INTO ad_media VALUES (5452331900, 20, 0, '2014-03-25 12:44:42.144285', 'image', NULL, NULL, NULL);
INSERT INTO ad_media VALUES (5460855219, 21, 0, '2014-03-25 12:46:48.177247', 'image', NULL, NULL, NULL);
INSERT INTO ad_media VALUES (5469378538, 22, 0, '2014-03-25 12:49:05.663727', 'image', NULL, NULL, NULL);
INSERT INTO ad_media VALUES (5477901857, 23, 0, '2014-03-25 12:50:21.209046', 'image', NULL, NULL, NULL);
INSERT INTO ad_media VALUES (5486425176, 24, 0, '2014-03-25 12:51:19.613402', 'image', NULL, NULL, NULL);
INSERT INTO ad_media VALUES (5494948495, 25, 0, '2014-03-25 12:53:41.875211', 'image', NULL, NULL, NULL);
INSERT INTO ad_media VALUES (5403471814, 27, 0, '2014-03-25 12:55:07.926676', 'image', NULL, NULL, NULL);
INSERT INTO ad_media VALUES (5411995133, 29, 0, '2014-03-25 12:58:09.411584', 'image', NULL, NULL, NULL);
INSERT INTO ad_media VALUES (5420518452, 31, 0, '2014-03-25 12:59:37.574732', 'image', NULL, NULL, NULL);
INSERT INTO ad_media VALUES (5429041771, 34, 0, '2014-03-25 14:21:37.980146', 'image', NULL, NULL, NULL);


--
-- Data for Name: ad_media_changes; Type: TABLE DATA; Schema: public; Owner: joaquin
--



--
-- Data for Name: ad_params; Type: TABLE DATA; Schema: public; Owner: joaquin
--

INSERT INTO ad_params VALUES (1, 'rooms', '2');
INSERT INTO ad_params VALUES (1, 'size', '250');
INSERT INTO ad_params VALUES (1, 'garage_spaces', '2');
INSERT INTO ad_params VALUES (1, 'condominio', '400');
INSERT INTO ad_params VALUES (1, 'communes', '5');
INSERT INTO ad_params VALUES (1, 'currency', 'peso');
INSERT INTO ad_params VALUES (1, 'country', 'UNK');
INSERT INTO ad_params VALUES (6, 'regdate', '1986');
INSERT INTO ad_params VALUES (6, 'mileage', '1');
INSERT INTO ad_params VALUES (6, 'fuel', '2');
INSERT INTO ad_params VALUES (6, 'gearbox', '2');
INSERT INTO ad_params VALUES (11, 'service_type', '6');
INSERT INTO ad_params VALUES (12, 'rooms', '3');
INSERT INTO ad_params VALUES (12, 'size', '120');
INSERT INTO ad_params VALUES (12, 'garage_spaces', '1');
INSERT INTO ad_params VALUES (12, 'condominio', '400');
INSERT INTO ad_params VALUES (12, 'communes', '323');
INSERT INTO ad_params VALUES (12, 'currency', 'peso');
INSERT INTO ad_params VALUES (12, 'country', 'UNK');
INSERT INTO ad_params VALUES (20, 'rooms', '2');
INSERT INTO ad_params VALUES (20, 'size', '80');
INSERT INTO ad_params VALUES (20, 'garage_spaces', '1');
INSERT INTO ad_params VALUES (20, 'condominio', '70000');
INSERT INTO ad_params VALUES (20, 'communes', '330');
INSERT INTO ad_params VALUES (20, 'prev_currency', 'peso');
INSERT INTO ad_params VALUES (20, 'country', 'UNK');
INSERT INTO ad_params VALUES (21, 'rooms', '2');
INSERT INTO ad_params VALUES (21, 'size', '80');
INSERT INTO ad_params VALUES (21, 'garage_spaces', '1');
INSERT INTO ad_params VALUES (21, 'condominio', '60000');
INSERT INTO ad_params VALUES (21, 'communes', '326');
INSERT INTO ad_params VALUES (21, 'prev_currency', 'peso');
INSERT INTO ad_params VALUES (21, 'country', 'UNK');
INSERT INTO ad_params VALUES (22, 'rooms', '2');
INSERT INTO ad_params VALUES (22, 'size', '123');
INSERT INTO ad_params VALUES (22, 'garage_spaces', '123');
INSERT INTO ad_params VALUES (22, 'condominio', '123');
INSERT INTO ad_params VALUES (22, 'communes', '306');
INSERT INTO ad_params VALUES (22, 'prev_currency', 'peso');
INSERT INTO ad_params VALUES (22, 'country', 'UNK');
INSERT INTO ad_params VALUES (23, 'rooms', '2');
INSERT INTO ad_params VALUES (23, 'size', '675');
INSERT INTO ad_params VALUES (23, 'garage_spaces', '3');
INSERT INTO ad_params VALUES (23, 'condominio', '76000');
INSERT INTO ad_params VALUES (23, 'communes', '315');
INSERT INTO ad_params VALUES (23, 'prev_currency', 'peso');
INSERT INTO ad_params VALUES (23, 'country', 'UNK');
INSERT INTO ad_params VALUES (24, 'rooms', '4');
INSERT INTO ad_params VALUES (24, 'size', '100');
INSERT INTO ad_params VALUES (24, 'garage_spaces', '2');
INSERT INTO ad_params VALUES (24, 'condominio', '80000');
INSERT INTO ad_params VALUES (24, 'communes', '323');
INSERT INTO ad_params VALUES (24, 'prev_currency', 'peso');
INSERT INTO ad_params VALUES (24, 'country', 'UNK');
INSERT INTO ad_params VALUES (25, 'rooms', '4');
INSERT INTO ad_params VALUES (25, 'size', '80');
INSERT INTO ad_params VALUES (25, 'garage_spaces', '2');
INSERT INTO ad_params VALUES (25, 'condominio', '90000');
INSERT INTO ad_params VALUES (25, 'communes', '312');
INSERT INTO ad_params VALUES (25, 'prev_currency', 'peso');
INSERT INTO ad_params VALUES (25, 'country', 'UNK');
INSERT INTO ad_params VALUES (26, 'rooms', '3');
INSERT INTO ad_params VALUES (26, 'size', '100');
INSERT INTO ad_params VALUES (26, 'garage_spaces', '1');
INSERT INTO ad_params VALUES (26, 'communes', '332');
INSERT INTO ad_params VALUES (26, 'prev_currency', 'peso');
INSERT INTO ad_params VALUES (26, 'country', 'UNK');
INSERT INTO ad_params VALUES (27, 'rooms', '2');
INSERT INTO ad_params VALUES (27, 'size', '400');
INSERT INTO ad_params VALUES (27, 'garage_spaces', '2');
INSERT INTO ad_params VALUES (27, 'communes', '336');
INSERT INTO ad_params VALUES (27, 'prev_currency', 'peso');
INSERT INTO ad_params VALUES (27, 'country', 'UNK');
INSERT INTO ad_params VALUES (28, 'rooms', '3');
INSERT INTO ad_params VALUES (28, 'size', '123');
INSERT INTO ad_params VALUES (28, 'garage_spaces', '1');
INSERT INTO ad_params VALUES (28, 'condominio', '123000');
INSERT INTO ad_params VALUES (28, 'communes', '337');
INSERT INTO ad_params VALUES (28, 'prev_currency', 'peso');
INSERT INTO ad_params VALUES (28, 'country', 'UNK');
INSERT INTO ad_params VALUES (29, 'size', '5000');
INSERT INTO ad_params VALUES (29, 'communes', '56');
INSERT INTO ad_params VALUES (29, 'prev_currency', 'peso');
INSERT INTO ad_params VALUES (29, 'country', 'UNK');
INSERT INTO ad_params VALUES (30, 'size', '5000');
INSERT INTO ad_params VALUES (30, 'condominio', '123');
INSERT INTO ad_params VALUES (30, 'communes', '58');
INSERT INTO ad_params VALUES (30, 'prev_currency', 'peso');
INSERT INTO ad_params VALUES (30, 'country', 'UNK');
INSERT INTO ad_params VALUES (31, 'size', '67887');
INSERT INTO ad_params VALUES (31, 'communes', '64');
INSERT INTO ad_params VALUES (31, 'prev_currency', 'peso');
INSERT INTO ad_params VALUES (31, 'country', 'UNK');
INSERT INTO ad_params VALUES (32, 'job_category', '18|20|24');
INSERT INTO ad_params VALUES (32, 'country', 'UNK');
INSERT INTO ad_params VALUES (33, 'job_category', '19|20|24');
INSERT INTO ad_params VALUES (33, 'country', 'UNK');
INSERT INTO ad_params VALUES (34, 'prev_currency', 'peso');
INSERT INTO ad_params VALUES (34, 'country', 'UNK');
INSERT INTO ad_params VALUES (20, 'gallery', NOW() + INTERVAL '1 day');
INSERT INTO ad_params VALUES (21, 'gallery', NOW() + INTERVAL '1 day');
INSERT INTO ad_params VALUES (22, 'gallery', NOW() + INTERVAL '1 day');
INSERT INTO ad_params VALUES (23, 'gallery', NOW() + INTERVAL '1 day');
INSERT INTO ad_params VALUES (24, 'gallery', NOW() + INTERVAL '1 day');
INSERT INTO ad_params VALUES (25, 'gallery', NOW() + INTERVAL '1 day');
INSERT INTO ad_params VALUES (26, 'gallery', NOW() + INTERVAL '1 day');
INSERT INTO ad_params VALUES (27, 'gallery', NOW() + INTERVAL '1 day');
INSERT INTO ad_params VALUES (28, 'gallery', NOW() + INTERVAL '1 day');
INSERT INTO ad_params VALUES (29, 'gallery', NOW() + INTERVAL '1 day');
INSERT INTO ad_params VALUES (30, 'gallery', NOW() + INTERVAL '1 day');
INSERT INTO ad_params VALUES (31, 'gallery', NOW() + INTERVAL '1 day');
INSERT INTO ad_params VALUES (32, 'gallery', NOW() + INTERVAL '1 day');
INSERT INTO ad_params VALUES (33, 'gallery', NOW() + INTERVAL '1 day');
INSERT INTO ad_params VALUES (34, 'gallery', NOW() + INTERVAL '1 day');
INSERT INTO ad_params VALUES (35, 'prev_currency', 'peso');
INSERT INTO ad_params VALUES (35, 'country', 'UNK');
INSERT INTO ad_params VALUES (36, 'size', '10000');
INSERT INTO ad_params VALUES (36, 'condominio', '123');
INSERT INTO ad_params VALUES (36, 'communes', '32');
INSERT INTO ad_params VALUES (36, 'prev_currency', 'peso');
INSERT INTO ad_params VALUES (36, 'country', 'UNK');
INSERT INTO ad_params VALUES (35, 'gallery', '2014-04-02 09:50:01');
INSERT INTO ad_params VALUES (36, 'gallery', '2014-04-02 09:50:01');


--
-- Data for Name: ad_queues; Type: TABLE DATA; Schema: public; Owner: joaquin
--



--
-- Data for Name: admin_privs; Type: TABLE DATA; Schema: public; Owner: joaquin
--

INSERT INTO admin_privs VALUES (2, 'adqueue');
INSERT INTO admin_privs VALUES (3, 'adqueue');
INSERT INTO admin_privs VALUES (3, 'admin');
INSERT INTO admin_privs VALUES (3, 'adminad.edit_ad');
INSERT INTO admin_privs VALUES (3, 'adminad.clear_ad');
INSERT INTO admin_privs VALUES (3, 'adminad.bump');
INSERT INTO admin_privs VALUES (3, 'credit');
INSERT INTO admin_privs VALUES (3, 'config');
INSERT INTO admin_privs VALUES (3, 'adqueue.admin_queue');
INSERT INTO admin_privs VALUES (3, 'adqueue.admin_settings');
INSERT INTO admin_privs VALUES (3, 'adqueue.show_num_ads=2');
INSERT INTO admin_privs VALUES (3, 'search');
INSERT INTO admin_privs VALUES (3, 'search.search_ads');
INSERT INTO admin_privs VALUES (3, 'notice_abuse');
INSERT INTO admin_privs VALUES (4, 'admin');
INSERT INTO admin_privs VALUES (5, 'admin');
INSERT INTO admin_privs VALUES (5, 'adqueue');
INSERT INTO admin_privs VALUES (5, 'adminad');
INSERT INTO admin_privs VALUES (5, 'adminad.clear_ad');
INSERT INTO admin_privs VALUES (5, 'adminad.edit_ad');
INSERT INTO admin_privs VALUES (5, 'adminad.bump');
INSERT INTO admin_privs VALUES (5, 'credit');
INSERT INTO admin_privs VALUES (5, 'config');
INSERT INTO admin_privs VALUES (5, 'adqueue.admin_queue');
INSERT INTO admin_privs VALUES (5, 'adqueue.admin_settings');
INSERT INTO admin_privs VALUES (5, 'adqueue.show_num_ads=2');
INSERT INTO admin_privs VALUES (6, 'admin');
INSERT INTO admin_privs VALUES (6, 'adqueue');
INSERT INTO admin_privs VALUES (6, 'adminad.edit_ad');
INSERT INTO admin_privs VALUES (50, 'admin');
INSERT INTO admin_privs VALUES (50, 'adqueue');
INSERT INTO admin_privs VALUES (51, 'admin');
INSERT INTO admin_privs VALUES (51, 'adqueue');
INSERT INTO admin_privs VALUES (52, 'admin');
INSERT INTO admin_privs VALUES (52, 'adqueue');
INSERT INTO admin_privs VALUES (53, 'admin');
INSERT INTO admin_privs VALUES (53, 'adqueue');
INSERT INTO admin_privs VALUES (54, 'admin');
INSERT INTO admin_privs VALUES (54, 'adqueue');
INSERT INTO admin_privs VALUES (55, 'admin');
INSERT INTO admin_privs VALUES (55, 'adqueue');
INSERT INTO admin_privs VALUES (9, 'ais');
INSERT INTO admin_privs VALUES (9, 'notice_abuse');
INSERT INTO admin_privs VALUES (9, 'adqueue');
INSERT INTO admin_privs VALUES (9, 'adminad');
INSERT INTO admin_privs VALUES (9, 'Adminuf');
INSERT INTO admin_privs VALUES (9, 'admin');
INSERT INTO admin_privs VALUES (9, 'api');
INSERT INTO admin_privs VALUES (9, 'config');
INSERT INTO admin_privs VALUES (9, 'filter');
INSERT INTO admin_privs VALUES (9, 'landing_page');
INSERT INTO admin_privs VALUES (9, 'mama');
INSERT INTO admin_privs VALUES (9, 'popular_ads');
INSERT INTO admin_privs VALUES (9, 'scarface');
INSERT INTO admin_privs VALUES (9, 'search');
INSERT INTO admin_privs VALUES (9, 'Websql');
INSERT INTO admin_privs VALUES (9, 'adqueue.accepted');
INSERT INTO admin_privs VALUES (9, 'adqueue.admin_settings');
INSERT INTO admin_privs VALUES (9, 'adqueue.show_num_ads=2');
INSERT INTO admin_privs VALUES (9, 'adqueue.approve_refused');
INSERT INTO admin_privs VALUES (9, 'adqueue.video');
INSERT INTO admin_privs VALUES (9, 'adqueue.mama_queues');
INSERT INTO admin_privs VALUES (9, 'adqueue.mamawork_queues');
INSERT INTO admin_privs VALUES (9, 'adqueue.refusals');
INSERT INTO admin_privs VALUES (9, 'adqueue.admin_queue');
INSERT INTO admin_privs VALUES (9, 'adminad.bump');
INSERT INTO admin_privs VALUES (9, 'adminad.clear_ad');
INSERT INTO admin_privs VALUES (9, 'adminad.edit_ad');
INSERT INTO admin_privs VALUES (9, 'filter.lists');
INSERT INTO admin_privs VALUES (9, 'filter.rules');
INSERT INTO admin_privs VALUES (9, 'filter.spamfilter');
INSERT INTO admin_privs VALUES (9, 'mama.backup_lists');
INSERT INTO admin_privs VALUES (9, 'mama.show_lists');
INSERT INTO admin_privs VALUES (9, 'scarface.warning');
INSERT INTO admin_privs VALUES (9, 'search.abuse_report');
INSERT INTO admin_privs VALUES (9, 'search.uid_emails');
INSERT INTO admin_privs VALUES (9, 'search.maillog');
INSERT INTO admin_privs VALUES (9, 'search.mass_delete');
INSERT INTO admin_privs VALUES (9, 'search.paylog');
INSERT INTO admin_privs VALUES (9, 'search.reviewers');
INSERT INTO admin_privs VALUES (9, 'search.search_ads');
INSERT INTO admin_privs VALUES (9, 'search.undo_delete');


--
-- Data for Name: bid_ads; Type: TABLE DATA; Schema: public; Owner: joaquin
--



--
-- Data for Name: bid_bids; Type: TABLE DATA; Schema: public; Owner: joaquin
--



--
-- Data for Name: bid_media; Type: TABLE DATA; Schema: public; Owner: joaquin
--



--
-- Data for Name: block_lists; Type: TABLE DATA; Schema: public; Owner: joaquin
--

INSERT INTO block_lists VALUES (1, 'E-post adresser - raderas', true, 'email', NULL);
INSERT INTO block_lists VALUES (2, 'E-post adresser - spamfiltret', true, 'email', NULL);
INSERT INTO block_lists VALUES (3, 'IP-adresser som - raderas', true, 'ip', NULL);
INSERT INTO block_lists VALUES (4, 'IP-adresser - spamfiltret', true, 'ip', NULL);
INSERT INTO block_lists VALUES (5, 'E-postadresser som inte f�r annonsera', false, 'email', NULL);
INSERT INTO block_lists VALUES (6, 'Telefonnummer som inte f�r annonsera', false, 'general', '-+ ');
INSERT INTO block_lists VALUES (7, 'Tipsmottagare', false, 'email', NULL);
INSERT INTO block_lists VALUES (8, 'Synonymer f�r ordet Annonsera', false, 'general', NULL);
INSERT INTO block_lists VALUES (9, 'Synonymer f�r ordet Gratis', false, 'general', NULL);
INSERT INTO block_lists VALUES (10, 'Sajter som kan spamma', false, 'general', NULL);
INSERT INTO block_lists VALUES (11, 'Ord som anv�nds av sajter som spammar', false, 'general', NULL);
INSERT INTO block_lists VALUES (12, 'Fula ord', false, 'general', NULL);
INSERT INTO block_lists VALUES (13, 'F�rbjudna rubriker i L�gg in annons', false, 'general', NULL);
INSERT INTO block_lists VALUES (14, 'Engelska fraser som anv�nds i mejlsvar', false, 'general', NULL);
INSERT INTO block_lists VALUES (15, 'Byten.se', false, 'general', NULL);
INSERT INTO block_lists VALUES (16, 'whitelist', false, 'email', NULL);
INSERT INTO block_lists VALUES (17, 'were_whitelist', false, 'email', NULL);


--
-- Data for Name: block_rules; Type: TABLE DATA; Schema: public; Owner: joaquin
--

INSERT INTO block_rules VALUES (1, 'Sajtspam', 'delete', 'adreply', 'and', 0, 0, '2014-03-25', NULL);
INSERT INTO block_rules VALUES (2, 'Annonsera gratis', 'delete', 'adreply', 'and', 0, 0, '2014-03-25', NULL);
INSERT INTO block_rules VALUES (3, 'Fula ord', 'stop', 'newad', 'and', 0, 0, '2014-03-25', NULL);
INSERT INTO block_rules VALUES (4, 'Fula ord', 'delete', 'adreply', 'and', 0, 0, '2014-03-25', NULL);
INSERT INTO block_rules VALUES (5, 'Blockerade epostadresser  - raderas', 'delete', 'adreply', 'and', 0, 0, '2014-03-25', NULL);
INSERT INTO block_rules VALUES (6, 'Blockerade epostadresser', 'stop', 'newad', 'and', 0, 0, '2014-03-25', NULL);
INSERT INTO block_rules VALUES (7, 'Blockerade ip-adresser - raderas', 'delete', 'adreply', 'and', 0, 0, '2014-03-25', NULL);
INSERT INTO block_rules VALUES (8, 'Blockerade telefonnummer', 'stop', 'newad', 'and', 0, 0, '2014-03-25', NULL);
INSERT INTO block_rules VALUES (9, 'Blockerade tipsmottagare', 'delete', 'sendtip', 'and', 0, 0, '2014-03-25', NULL);
INSERT INTO block_rules VALUES (10, 'Byten.se', 'delete', 'adreply', 'and', 0, 0, '2014-03-25', NULL);
INSERT INTO block_rules VALUES (11, 'Byten.se', 'delete', 'sendtip', 'and', 0, 0, '2014-03-25', NULL);
INSERT INTO block_rules VALUES (12, 'Blockerade ord i rubriken', 'stop', 'newad', 'and', 0, 0, '2014-03-25', NULL);
INSERT INTO block_rules VALUES (13, 'Blockerade engelska mail', 'spamfilter', 'adreply', 'and', 0, 0, '2014-03-25', NULL);
INSERT INTO block_rules VALUES (14, 'Blockerade epostadresser - spamfilter', 'spamfilter', 'adreply', 'and', 0, 0, '2014-03-25', NULL);
INSERT INTO block_rules VALUES (15, 'Blockerade ip-adresser - spamfilter', 'spamfilter', 'adreply', 'and', 0, 0, '2014-03-25', NULL);
INSERT INTO block_rules VALUES (16, 'Fula ord', 'delete', 'sendtip', 'or', 0, 0, '2014-03-25', NULL);
INSERT INTO block_rules VALUES (17, 'whitelist rule', 'move_to_queue', 'clear', 'and', 0, 0, '2014-03-25', 'whitelist');


--
-- Data for Name: block_rule_conditions; Type: TABLE DATA; Schema: public; Owner: joaquin
--

INSERT INTO block_rule_conditions VALUES (1, 1, 10, 0, '{subject,body}');
INSERT INTO block_rule_conditions VALUES (2, 1, 11, 0, '{subject,body}');
INSERT INTO block_rule_conditions VALUES (3, 2, 8, 0, '{subject,body}');
INSERT INTO block_rule_conditions VALUES (4, 2, 9, 0, '{subject,body}');
INSERT INTO block_rule_conditions VALUES (5, 3, 12, 0, '{subject,body}');
INSERT INTO block_rule_conditions VALUES (6, 4, 12, 0, '{subject,body}');
INSERT INTO block_rule_conditions VALUES (7, 5, 1, 1, '{email}');
INSERT INTO block_rule_conditions VALUES (8, 6, 5, 1, '{email}');
INSERT INTO block_rule_conditions VALUES (9, 7, 3, 1, '{remote_addr}');
INSERT INTO block_rule_conditions VALUES (10, 8, 6, 1, '{phone}');
INSERT INTO block_rule_conditions VALUES (11, 9, 7, 1, '{email}');
INSERT INTO block_rule_conditions VALUES (12, 10, 15, 0, '{name,email,subject,body}');
INSERT INTO block_rule_conditions VALUES (13, 11, 15, 0, '{name,email,subject,body}');
INSERT INTO block_rule_conditions VALUES (14, 12, 13, 0, '{subject}');
INSERT INTO block_rule_conditions VALUES (15, 13, 14, 0, '{body}');
INSERT INTO block_rule_conditions VALUES (16, 14, 2, 1, '{subject}');
INSERT INTO block_rule_conditions VALUES (17, 15, 4, 1, '{remote_addr}');
INSERT INTO block_rule_conditions VALUES (18, 16, 12, 0, '{body}');
INSERT INTO block_rule_conditions VALUES (19, 17, 16, 1, '{email}');


--
-- Data for Name: blocked_items; Type: TABLE DATA; Schema: public; Owner: joaquin
--

INSERT INTO blocked_items VALUES (1, 'ful@fisk.se', 1, NULL, NULL);
INSERT INTO blocked_items VALUES (2, 'ful@fisk.se', 5, NULL, NULL);
INSERT INTO blocked_items VALUES (3, '0733555501', 6, NULL, NULL);
INSERT INTO blocked_items VALUES (4, 'ful@fisk.se', 7, NULL, NULL);
INSERT INTO blocked_items VALUES (5, 'fisk.se', 10, NULL, NULL);
INSERT INTO blocked_items VALUES (6, 'vatten', 11, NULL, NULL);
INSERT INTO blocked_items VALUES (7, 'fitta', 12, NULL, NULL);
INSERT INTO blocked_items VALUES (8, 'analakrobat', 12, NULL, NULL);
INSERT INTO blocked_items VALUES (9, 'buy', 13, NULL, NULL);
INSERT INTO blocked_items VALUES (10, 'sale', 13, NULL, NULL);
INSERT INTO blocked_items VALUES (11, 'salu', 13, NULL, NULL);
INSERT INTO blocked_items VALUES (12, 'free', 14, NULL, NULL);


--
-- Data for Name: conf; Type: TABLE DATA; Schema: public; Owner: joaquin
--

-- INSERT INTO conf VALUES ('*.controlpanel.modules.adqueue.settings.auto_abuse', '1', '2014-03-25 12:27:22.255575', NULL);
-- INSERT INTO conf VALUES ('*.*.common.accepted.1.name', 'Sin fotos', '2014-03-25 12:27:22.258963', NULL);
-- INSERT INTO conf VALUES ('*.*.common.accepted.1.text', 'Para mejorar las posibilidades de que tu aviso sea visto, te recomendamos que agregues im�genes del producto que ofreces o buscas.', '2014-03-25 12:27:22.259572', NULL);
-- INSERT INTO conf VALUES ('*.*.common.accepted.22.name', 'Foto principal movida', '2014-03-25 12:27:22.260076', NULL);
-- INSERT INTO conf VALUES ('*.*.common.accepted.22.text', 'Para mejorar las posibilidades de que tu aviso sea visto, hemos alterado el orden de las fotograf�as', '2014-03-25 12:27:22.260532', NULL);
-- INSERT INTO conf VALUES ('*.*.common.accepted.3.name', 'BORRAMOS TODAS LAS IM�GENES', '2014-03-25 12:27:22.26102', NULL);
-- INSERT INTO conf VALUES ('*.*.common.accepted.3.text', 'Hemos borrado las im�genes que subiste por no cumplir las reglas de Yapo.cl. Sube nuevas fotograf�as para tener m�s visitas y vender�s antes!', '2014-03-25 12:27:22.261468', NULL);
-- INSERT INTO conf VALUES ('*.*.common.accepted.4.name', 'BORRAMOS ALGUNAS DE LAS IM�GENES', '2014-03-25 12:27:22.262086', NULL);
-- INSERT INTO conf VALUES ('*.*.common.accepted.4.text', 'Hemos borrado algunas de las im�genes que subiste por no cumplir las reglas de Yapo.cl. Sube nuevas fotograf�as para tener m�s visitas y vender�s antes!', '2014-03-25 12:27:22.262513', NULL);
-- INSERT INTO conf VALUES ('*.*.common.accepted.6.name', 'BORRAMOS CORREOS', '2014-03-25 12:27:22.263031', NULL);
-- INSERT INTO conf VALUES ('*.*.common.accepted.6.text', 'Hemos borrado el email de la descripci�n de tu aviso por tu seguridad. Las personas interesadas pueden contactarte a trav�s del formulario y evitar�s recibir spam o emails no deseados. Recuerda que Yapo.cl es 100% gratis.', '2014-03-25 12:27:22.263398', NULL);
-- INSERT INTO conf VALUES ('*.*.common.accepted.8.name', 'MODIFICAMOS T�TULO', '2014-03-25 12:27:22.263781', NULL);
-- INSERT INTO conf VALUES ('*.*.common.accepted.8.text', 'Hemos modificado el t�tulo de tu aviso para que describa mejor lo que ofreces. Te sugerimos editar tu aviso para hacerlo a�n mejor y vender antes.', '2014-03-25 12:27:22.264222', NULL);
-- INSERT INTO conf VALUES ('*.*.common.accepted.9.name', 'MODIFICAMOS DESCRIPCI�N', '2014-03-25 12:27:22.264562', NULL);
-- INSERT INTO conf VALUES ('*.*.common.accepted.9.text', 'Hemos modificado la descripci�n de tu aviso para explicar mejor lo que ofreces. Te sugerimos editar tu aviso para hacerlo a�n mejor y vender antes.', '2014-03-25 12:27:22.265033', NULL);
-- INSERT INTO conf VALUES ('*.*.common.accepted.10.name', 'PALABRAS DE B�SQUEDA', '2014-03-25 12:27:22.265358', NULL);
-- INSERT INTO conf VALUES ('*.*.common.accepted.10.text', 'Hemos modificado la descripci�n de tu aviso para ajustarla a las reglas de Yapo.cl. Recuerda que tu aviso debe hacer referencia a un solo producto o servicio.', '2014-03-25 12:27:22.265655', NULL);
-- INSERT INTO conf VALUES ('*.*.common.accepted.15.name', 'MEJORA LA DESCRIPCI�N', '2014-03-25 12:27:22.266132', NULL);
-- INSERT INTO conf VALUES ('*.*.common.accepted.15.text', 'Hemos revisado la descripci�n de tu aviso y te recomendamos que detalles m�s el producto o servicio que est�s ofreciendo, tu aviso ser� m�s interesante y vender�s antes', '2014-03-25 12:27:22.266464', NULL);
-- INSERT INTO conf VALUES ('*.*.common.accepted.16.name', 'CATEGORIA FUE CAMBIADA', '2014-03-25 12:27:22.266976', NULL);
-- INSERT INTO conf VALUES ('*.*.common.accepted.16.text', 'Hemos cambiado la categor�a que elegiste por la correcta. Ahora tu aviso ser� encontrado m�s f�cil y vender�s antes. Recuerda que Yapo.cl es 100% gratis.', '2014-03-25 12:27:22.267371', NULL);
-- INSERT INTO conf VALUES ('*.*.common.accepted.12.name', 'COMPLETAMOS EL CAMPO PRECIO', '2014-03-25 12:27:22.267692', NULL);
-- INSERT INTO conf VALUES ('*.*.common.accepted.12.text', 'Hemos completado el precio de tu producto o servicio. Puedes editarlo de nuevo si crees que nos hemos equivocado.', '2014-03-25 12:27:22.268228', NULL);
-- INSERT INTO conf VALUES ('*.*.common.accepted.13.name', 'BORRAMOS ENLACES', '2014-03-25 12:27:22.26858', NULL);
-- INSERT INTO conf VALUES ('*.*.common.accepted.13.text', 'Hemos borrado enlaces de la descripci�n de tu aviso ya que no est� permitido en las reglas de nuestra web. Recuerda que no hay l�mite de datos para ingresar y que Yapo.cl es 100% gratis.', '2014-03-25 12:27:22.269006', NULL);
-- INSERT INTO conf VALUES ('*.*.common.accepted.17.name', 'TIPO DE VENDEDOR DE PERSONA A EMPRESA', '2014-03-25 12:27:22.269366', NULL);
-- INSERT INTO conf VALUES ('*.*.common.accepted.17.text', 'Hemos cambiado el tipo de vendedor de persona a empresa porque consideramos que te dedicas profesionalmente al producto/servicio que ofreces. Recuerda que Yapo.cl es 100% gratis tambi�n para empresas.', '2014-03-25 12:27:22.269666', NULL);
-- INSERT INTO conf VALUES ('*.*.common.accepted.18.name', 'TIPO DE VENDEDOR DE EMPRESA A PERSONA', '2014-03-25 12:27:22.270144', NULL);
-- INSERT INTO conf VALUES ('*.*.common.accepted.18.text', 'Hemos cambiado el tipo de vendedor de empresa a persona porque consideramos que fue mal seleccionado. Recuerda que Yapo.cl es 100% gratis.', '2014-03-25 12:27:22.270492', NULL);
-- INSERT INTO conf VALUES ('*.*.common.accepted.19.name', 'MEJORA EL T�TULO', '2014-03-25 12:27:22.271022', NULL);
-- INSERT INTO conf VALUES ('*.*.common.accepted.19.text', 'Hemos revisado el t�tulo de tu aviso y te recomendamos que detalles m�s el producto o servicio que est�s ofreciendo, tu aviso ser� m�s interesante y vender�s antes', '2014-03-25 12:27:22.271357', NULL);
-- INSERT INTO conf VALUES ('*.*.common.accepted.20.name', 'INGRESA COMUNA (De NO UF a UF)', '2014-03-25 12:27:22.27165', NULL);
-- INSERT INTO conf VALUES ('*.*.common.accepted.20.text', 'Hemos cambiado la categor�a que elegiste por una de inmobiliaria. Para completar la publicaci�n debes editarlo e ingresar la comuna, de esta forma tu aviso ser� encontrado m�s f�cil y vender�s antes. Recuerda que Yapo.cl es 100% gratis.', '2014-03-25 12:27:22.272157', NULL);
-- INSERT INTO conf VALUES ('*.*.common.accepted.21.name', 'VEHICULOS: AGREGA MODELO Y VERSION', '2014-03-25 12:27:22.272515', NULL);
-- INSERT INTO conf VALUES ('*.*.common.accepted.21.text', 'Hemos revisado tu aviso y te recomendamos que detalles la marca y modelo del veh�culo que est�s ofreciendo, tu aviso ser� encontrado m�s f�cil y vender�s antes. Recuerda que Yapo.cl es 100% gratis.', '2014-03-25 12:27:22.272944', NULL);
-- INSERT INTO conf VALUES ('*.*.common.accepted.5.name', 'BORRAMOS LINKS Y CORREOS', '2014-03-25 12:27:22.273387', NULL);
-- INSERT INTO conf VALUES ('*.*.common.accepted.5.text', 'Por tu seguridad, hemos borrado emails y enlaces de la descripci�n de tu aviso. Las personas interesadas pueden contactarte a trav�s del formulario "Contacta con el anunciante" el cual se encuentra a la derecha de cada uno de los avisos. As� evitar�s recibir spam o emails no deseados. Adem�s, puedes incluir tantos detalles como quieras en tu aviso. Recuerda que Yapo.cl es 100% gratis.', '2014-03-25 12:27:22.273735', NULL);
-- INSERT INTO conf VALUES ('*.*.common.accepted.11.name', 'BORRAMOS CAMPO PRECIO', '2014-03-25 12:27:22.274239', NULL);
-- INSERT INTO conf VALUES ('*.*.common.accepted.11.text', 'Hemos borrado el campo precio porque consideramos que es err�neo. Por favor verifica que tu precio sea correcto. Recuerda que debes insertar el precio por el total de productos � servicios ofrecidos.', '2014-03-25 12:27:22.274532', NULL);
-- INSERT INTO conf VALUES ('*.*.common.accepted.2.name', 'SIN IM�GENES', '2014-03-25 12:27:22.274838', NULL);
-- INSERT INTO conf VALUES ('*.*.common.accepted.2.text', '�Sab�as que un aviso con im�genes tiene m�s visitas y es mucho m�s interesante? �Incluye im�genes y vender�s antes!', '2014-03-25 12:27:22.275228', NULL);
-- INSERT INTO conf VALUES ('*.*.common.accepted_order.0', '3', '2014-03-25 12:27:22.275538', NULL);
-- INSERT INTO conf VALUES ('*.*.common.accepted_order.1', '2', '2014-03-25 12:27:22.275834', NULL);
-- INSERT INTO conf VALUES ('*.*.common.accepted_order.2', '4', '2014-03-25 12:27:22.276279', NULL);
-- INSERT INTO conf VALUES ('*.*.common.accepted_order.3', '5', '2014-03-25 12:27:22.27656', NULL);
-- INSERT INTO conf VALUES ('*.*.common.accepted_order.4', '13', '2014-03-25 12:27:22.276849', NULL);
-- INSERT INTO conf VALUES ('*.*.common.accepted_order.5', '6', '2014-03-25 12:27:22.278948', NULL);
-- INSERT INTO conf VALUES ('*.*.common.accepted_order.6', '8', '2014-03-25 12:27:22.279407', NULL);
-- INSERT INTO conf VALUES ('*.*.common.accepted_order.7', '9', '2014-03-25 12:27:22.279755', NULL);
-- INSERT INTO conf VALUES ('*.*.common.accepted_order.8', '10', '2014-03-25 12:27:22.280226', NULL);
-- INSERT INTO conf VALUES ('*.*.common.accepted_order.9', '11', '2014-03-25 12:27:22.280582', NULL);
-- INSERT INTO conf VALUES ('*.*.common.accepted_order.10', '12', '2014-03-25 12:27:22.281134', NULL);
-- INSERT INTO conf VALUES ('*.*.common.accepted_order.11', '19', '2014-03-25 12:27:22.2815', NULL);
-- INSERT INTO conf VALUES ('*.*.common.accepted_order.12', '15', '2014-03-25 12:27:22.281851', NULL);
-- INSERT INTO conf VALUES ('*.*.common.accepted_order.13', '21', '2014-03-25 12:27:22.282338', NULL);
-- INSERT INTO conf VALUES ('*.*.common.accepted_order.14', '16', '2014-03-25 12:27:22.282679', NULL);
-- INSERT INTO conf VALUES ('*.*.common.accepted_order.15', '20', '2014-03-25 12:27:22.283133', NULL);
-- INSERT INTO conf VALUES ('*.*.common.accepted_order.16', '17', '2014-03-25 12:27:22.28354', NULL);
-- INSERT INTO conf VALUES ('*.*.common.accepted_order.17', '18', '2014-03-25 12:27:22.283881', NULL);
-- INSERT INTO conf VALUES ('*.*.common.accepted_order.18', '1', '2014-03-25 12:27:22.284427', NULL);
-- INSERT INTO conf VALUES ('*.*.common.accepted_order.19', '22', '2014-03-25 12:27:22.284779', NULL);
-- INSERT INTO conf VALUES ('*.*.common.refusal.0.name', 'T�tulo y Descripci�n de Otros', '2014-03-25 12:27:22.285276', NULL);
-- INSERT INTO conf VALUES ('*.*.common.refusal.1.name', 'T�tulo y Descripci�n del Veh�culo', '2014-03-25 12:27:22.285748', NULL);
-- INSERT INTO conf VALUES ('*.*.common.refusal.2.name', 'T�tulo y Descripci�n de los Bienes Ra�ces', '2014-03-25 12:27:22.286413', NULL);
-- INSERT INTO conf VALUES ('*.*.common.refusal.3.name', 'Restricciones para los Animales Dom�sticos', '2014-03-25 12:27:22.286721', NULL);
-- INSERT INTO conf VALUES ('*.*.common.refusal.4.name', 'Links para otros Sitios', '2014-03-25 12:27:22.287175', NULL);
-- INSERT INTO conf VALUES ('*.*.common.refusal.5.name', 'Aviso Empresa', '2014-03-25 12:27:22.287552', NULL);
-- INSERT INTO conf VALUES ('*.*.common.refusal.6.name', 'Contacto', '2014-03-25 12:27:22.287838', NULL);
-- INSERT INTO conf VALUES ('*.*.common.refusal.7.name', 'Varios elementos', '2014-03-25 12:27:22.288388', NULL);
-- INSERT INTO conf VALUES ('*.*.common.refusal.8.name', 'Varios elementos - Empleo', '2014-03-25 12:27:22.288754', NULL);
-- INSERT INTO conf VALUES ('*.*.common.refusal.9.name', 'Avisos personales', '2014-03-25 12:27:22.289231', NULL);
-- INSERT INTO conf VALUES ('*.*.common.refusal.10.name', 'Varios elementos Veh�culos - Propiedad', '2014-03-25 12:27:22.289543', NULL);
-- INSERT INTO conf VALUES ('*.*.common.refusal.11.name', 'Aviso doble', '2014-03-25 12:27:22.289831', NULL);
-- INSERT INTO conf VALUES ('*.*.common.refusal.12.name', 'Aviso caducado', '2014-03-25 12:27:22.290272', NULL);
-- INSERT INTO conf VALUES ('*.*.common.refusal.13.name', 'IP extranjero', '2014-03-25 12:27:22.290561', NULL);
-- INSERT INTO conf VALUES ('*.*.common.refusal.14.name', 'Ilegal', '2014-03-25 12:27:22.290847', NULL);
-- INSERT INTO conf VALUES ('*.*.common.refusal.15.name', 'Contenido de im�genes', '2014-03-25 12:27:22.291268', NULL);
-- INSERT INTO conf VALUES ('*.*.common.refusal.16.name', 'Error en la imagen', '2014-03-25 12:27:22.291608', NULL);
-- INSERT INTO conf VALUES ('*.*.common.refusal.17.name', 'Idioma', '2014-03-25 12:27:22.291905', NULL);
-- INSERT INTO conf VALUES ('*.*.common.refusal.18.name', 'Link', '2014-03-25 12:27:22.292389', NULL);
-- INSERT INTO conf VALUES ('*.*.common.refusal.19.name', 'Imagen obscena', '2014-03-25 12:27:22.292693', NULL);
-- INSERT INTO conf VALUES ('*.*.common.refusal.20.name', 'Ofensivo', '2014-03-25 12:27:22.293151', NULL);
-- INSERT INTO conf VALUES ('*.*.common.refusal.21.name', 'Pirater�a', '2014-03-25 12:27:22.293545', NULL);
-- INSERT INTO conf VALUES ('*.*.common.refusal.22.name', 'Elementos', '2014-03-25 12:27:22.293845', NULL);
-- INSERT INTO conf VALUES ('*.*.common.refusal.23.name', 'Marketing', '2014-03-25 12:27:22.294303', NULL);
-- INSERT INTO conf VALUES ('*.*.common.refusal.24.name', 'Palabras de B�squeda', '2014-03-25 12:27:22.294594', NULL);
-- INSERT INTO conf VALUES ('*.*.common.refusal.25.name', 'Contrase�a', '2014-03-25 12:27:22.294886', NULL);
-- INSERT INTO conf VALUES ('*.*.common.refusal.26.name', 'Virus', '2014-03-25 12:27:22.295301', NULL);
-- INSERT INTO conf VALUES ('*.*.common.refusal.27.name', 'Estado de origen', '2014-03-25 12:27:22.295661', NULL);
-- INSERT INTO conf VALUES ('*.*.common.refusal.29.name', 'No es realista', '2014-03-25 12:27:22.295937', NULL);
-- INSERT INTO conf VALUES ('*.*.common.refusal.30.name', 'Categor�a equivocada', '2014-03-25 12:27:22.296378', NULL);
-- INSERT INTO conf VALUES ('*.*.common.refusal.31.name', 'Spam', '2014-03-25 12:27:22.296659', NULL);
-- INSERT INTO conf VALUES ('*.*.common.refusal.32.name', 'Propiedad intelectual', '2014-03-25 12:27:22.29693', NULL);
-- INSERT INTO conf VALUES ('*.*.common.refusal.33.name', 'Privacidad', '2014-03-25 12:27:22.297353', NULL);
-- INSERT INTO conf VALUES ('*.*.common.refusal.34.name', 'Intercambios', '2014-03-25 12:27:22.297636', NULL);
-- INSERT INTO conf VALUES ('*.*.common.refusal.35.name', 'Menores de edad', '2014-03-25 12:27:22.297908', NULL);
-- INSERT INTO conf VALUES ('*.*.common.refusal.0.text', 'Por favor, comprueba que el nombre incluido / modelo / t�tulo / marca del art�culo que deseas vender, est� de acuerdo con  el t�tulo y la descripci�n del producto. El t�tulo del aviso debe describir el producto o servicio anunciado, no se permiten incluir nombres de empresas o URL (direcci�n Web). No es permitido el uso de caracteres especiales o caracteres en may�sculas en el t�tulo.', '2014-03-25 12:27:22.298361', NULL);
-- INSERT INTO conf VALUES ('*.*.common.refusal.1.text', 'Por favor,  verifica el nombre y modelo (ejemplo: Honda Civic 1.6]) del veh�culo que deseas vender. En la descripci�n, incluye  informaci�n espec�fica.', '2014-03-25 12:27:22.298742', NULL);
-- INSERT INTO conf VALUES ('*.*.common.refusal.2.text', 'Comprueba  que incluya el t�tulo. El t�tulo del aviso debe describir el producto o servicio anunciado, no se le permite incluir nombres de empresas o  URL (direcci�n Web). No es permitido el uso de caracteres especiales o caracteres en may�sculas en el t�tulo.', '2014-03-25 12:27:22.299197', NULL);
-- INSERT INTO conf VALUES ('*.*.common.refusal.3.text', 'No permitimos avisos de animales prohibidos por las leyes chilenas de protecci�n animal.', '2014-03-25 12:27:22.299606', NULL);
-- INSERT INTO conf VALUES ('*.*.common.refusal.4.text', 'No permitimos la inclusi�n de un enlace dirigido a otra p�gina web de e-mail o n�meros de tel�fono en el texto del aviso. No est� permitido hablar de otros sitios web o las subastas de avisos clasificados. Estos v�nculos y referencias deben ser eliminados antes de volver a enviar la notificaci�n.', '2014-03-25 12:27:22.299952', NULL);
-- INSERT INTO conf VALUES ('*.*.common.refusal.5.text', 'Has introducido un aviso en forma individual o categor�as que no permiten avisos de empresas. Las empresas deben insertar avisos y hacer una lista como los avisos de empresas. Los avisos de empresas no est�n permitidos en las siguientes categor�as: Video Juegos, telefon�a, ropa y prendas de vestir, bolsos, mochilas y accesorios, joyas, relojes y joyas, etc.', '2014-03-25 12:27:22.300475', NULL);
-- INSERT INTO conf VALUES ('*.*.common.refusal.6.text', 'No hemos podido verificar tu informaci�n de contacto. Por favor, verifica que toda la informaci�n introducida es correcta. Despu�s de la revisi�n, puedes volver a enviar tu aviso.', '2014-03-25 12:27:22.300833', NULL);
-- INSERT INTO conf VALUES ('*.*.common.refusal.7.text', 'Hay muchos elementos en el mismo aviso. Por favor, escribe  cada elemento de los avisos por separado. No est� permitido introducir m�s de un veh�culo o propiedad en el mismo aviso, excepto si la transacci�n es un intercambio (por ejemplo, 2 por 1).                             ', '2014-03-25 12:27:22.301253', NULL);
-- INSERT INTO conf VALUES ('*.*.common.refusal.8.text', 'Hay muchas ofertas de trabajo en el mismo aviso. Escribe una oferta por aviso.  No puedes publicar m�s de una propiedad en el mismo aviso.', '2014-03-25 12:27:22.301665', NULL);
-- INSERT INTO conf VALUES ('*.*.common.refusal.9.text', 'Has introducido un aviso de una empresa. Los avisos deben ser personales.', '2014-03-25 12:27:22.301999', NULL);
-- INSERT INTO conf VALUES ('*.*.common.refusal.10.text', 'Hay muchos elementos en el mismo aviso. Escribe cada elemento de la lista por separado, para que tu aviso sea m�s relevante para los compradores. No est� permitido introducir m�s de un veh�culo o propiedad en el mismo aviso, a menos que la transacci�n sea  un intercambio necesario (por  ejemplo, 2 por 1).', '2014-03-25 12:27:22.302509', NULL);
-- INSERT INTO conf VALUES ('*.*.common.refusal.11.text', 'Ya existe otro aviso como el que has introducido. No puedes copiar el texto de los avisos de otros anunciantes, ya que est�n bajo la ley de derechos de autor.. Tampoco est� permitido publicar varios avisos con el mismo producto  o servicio. El aviso anterior debe ser borrado antes de publicar un nuevo  aviso. Tampoco se permite hacer publicidad del mismo art�culo, o servicio en  las diferentes categor�as de avisos en las diferentes regiones.', '2014-03-25 12:27:22.302865', NULL);
-- INSERT INTO conf VALUES ('*.*.common.refusal.12.text', 'El  producto  ya no est� disponible/ha expirado/fue vendido.', '2014-03-25 12:27:22.303351', NULL);
-- INSERT INTO conf VALUES ('*.*.common.refusal.13.text', 'Nuestro sitio ofrece clasificados s�lo en Chile. Los productos o servicios s�lo se  encuentran en  Chile y el aviso ser� colocado en la zona donde se encuentra. No se aceptan avisos de fuera del pa�s o en otro idioma que no sea el espa�ol.', '2014-03-25 12:27:22.303745', NULL);
-- INSERT INTO conf VALUES ('*.*.common.refusal.14.text', 'Tu aviso contiene productos ilegales y/o prohibidos  para la  venta en nuestro sitio. No est� permito hacer publicidad de productos falsificados o falsos, como los productos de marca, CD / VCD / DVD, software / consola de juegos. Para que el aviso sea aceptado y para que este requisitto se aplique, el anunciante debe garantizar que los productos son originales. Los avisos que incluyen los productos para su reventa a la luz de la legislaci�n chilena no pueden ser publicados. Tenemos restricciones sobre el aviso de ciertos bienes y servicios. Lee nuestros T�rminos. Los servicios ofrecidos o solicitados deben cumplir con las leyes chilenas y reglamentarias aplicables a cada profesi�n.', '2014-03-25 12:27:22.304224', NULL);
-- INSERT INTO conf VALUES ('*.*.common.refusal_order.32', '26', '2014-03-25 12:27:22.328888', NULL);
-- INSERT INTO conf VALUES ('*.*.common.refusal_order.33', '25', '2014-03-25 12:27:22.329363', NULL);
-- INSERT INTO conf VALUES ('*.*.common.refusal_order.35', '17', '2014-03-25 12:27:22.329924', NULL);
-- INSERT INTO conf VALUES ('*.*.common.refusal.15.text', 'Las im�genes deben concordar con los avisos y deben ser relevantes para el art�culo o servicio anunciado. Las im�genes deben representar el art�culo anunciado. No puedes utilizar las im�genes para art�culos del cat�logo de segunda mano, o utilizar logotipos e im�genes de la empresa, excepto en los "Servicios" y "Empleo". No se permite el uso de im�genes de otros anunciantes, sin su consentimiento previo, ni marcas de agua o logos de sitios de la competencia. Las im�genes est�n protegidas por la legislaci�n sobre derechos de autor. No se permite el uso de una imagen en m�s de un aviso.', '2014-03-25 12:27:22.304616', NULL);
-- INSERT INTO conf VALUES ('*.*.common.refusal.16.text', 'Una o m�s im�genes contienen errores y no se muestran correctamente. Por favor, aseg�rate que el formato de las  im�genes  es  JPG, GIF o BMP. ', '2014-03-25 12:27:22.305043', NULL);
-- INSERT INTO conf VALUES ('*.*.common.refusal.17.text', 'Su aviso no est� en espa�ol', '2014-03-25 12:27:22.305695', NULL);
-- INSERT INTO conf VALUES ('*.*.common.refusal.18.text', 'Est�s utilizando enlaces que no son relevantes para el aviso y/o que no funcionan', '2014-03-25 12:27:22.306094', NULL);
-- INSERT INTO conf VALUES ('*.*.common.refusal.19.text', 'No se permite  publicar im�genes obscenas que muestren a la gente desnuda, en ropa interior o traje de ba�o.', '2014-03-25 12:27:22.306622', NULL);
-- INSERT INTO conf VALUES ('*.*.common.refusal.20.text', 'Tu aviso puede ser ofensivo para los grupos �tnicos / religiosos, o puede ser considerado  racista, xen�fobo o terrorista, ya que atenta contra el g�nero humano. No se permiten avisos que violen normas constitucionales y que incorporen  contenidos, mensajes o productos de naturaleza violenta o degradantes.', '2014-03-25 12:27:22.306944', NULL);
-- INSERT INTO conf VALUES ('*.*.common.refusal.21.text', 'No  est� permitido hacer publicidad de productos falsificados o falsos, como los productos de marca, CD / VCD / DVD, software / consola de juegos. Para que el aviso sea aceptado, el anunciante debe garantizar que los productos son originales. Los avisos que incluyen los productos para su reventa a la luz de la legislaci�n chilena, no pueden ser publicados.', '2014-03-25 12:27:22.307436', NULL);
-- INSERT INTO conf VALUES ('*.*.common.refusal.22.text', 'S�lo se  permite hacer publicidad de ventas, arriendos, empleo y servicios.', '2014-03-25 12:27:22.307767', NULL);
-- INSERT INTO conf VALUES ('*.*.common.refusal.23.text', 'El aviso contiene productos o servicios que no est�n permitidos en nuestro sitio. Para m�s informaci�n sobre estos productos, visite la p�gina de las Reglas. Si usted tiene alguna pregunta p�ngase en contacto con Atenci�n al Cliente.', '2014-03-25 12:27:22.308084', NULL);
-- INSERT INTO conf VALUES ('*.*.common.refusal.24.text', 'No est�  permitido el uso de avisos  que tengan como �nico prop�sito el  marketing,  lo que no implica necesariamente el suministro de bienes, trabajos o servicios.', '2014-03-25 12:27:22.308527', NULL);
-- INSERT INTO conf VALUES ('*.*.common.refusal.25.text', 'No est�  permitido el uso de avisos  que tengan como �nico prop�sito el  marketing,  lo que no implica necesariamente el suministro de bienes, trabajos o servicios.', '2014-03-25 12:27:22.308911', NULL);
-- INSERT INTO conf VALUES ('*.*.common.refusal.26.text', 'No se pueden  introducir o difundir en la red,  programas de datos (virus y software maliciosos) que puedan  causar da�os al proveedor de acceso, a sistemas inform�ticos  de nuestros usuarios del sitio o de terceros que utilicen la misma red.', '2014-03-25 12:27:22.309418', NULL);
-- INSERT INTO conf VALUES ('*.*.common.refusal.27.text', 'No  se ha declarado la autenticidad de tu producto. Para que el aviso sea  aceptado, el anunciante debe garantizar que los productos son originales.', '2014-03-25 12:27:22.309836', NULL);
-- INSERT INTO conf VALUES ('*.*.common.refusal.29.text', 'No se permite la publicaci�n de avisos que no posean ofertas cre�bles y realistas. No se permite que los avisos contengan cualquier informaci�n con contenidos falsos, ambiguos o inexactos, con el fin de inducir al error a potenciales receptores de dicha informaci�n.', '2014-03-25 12:27:22.310374', NULL);
-- INSERT INTO conf VALUES ('*.*.common.refusal.30.text', 'El aviso se publicar� en la categor�a que mejor describa el art�culo o servicio. Nos reservamos el derecho, si es necesario, a mover el aviso a la categor�a m�s apropiada. Bienes y servicios que no entren en la misma categor�a ser�n publicados en diferentes avisos. Para la venta se publicar� en "Se vende" y los avisos que demanden  un producto se publicar�n en "Se compra". En algunas categor�as los avisos podr�an incluir las opciones de "Arriendo" y "Se busca  arriendo". En otras categor�as, si es necesario, los avisos de "Se arrienda"  se publicar�n en "Venta" y los avisos "Quiero  arrendar", se publicar�n en "Se compra".', '2014-03-25 12:27:22.310778', NULL);
-- INSERT INTO conf VALUES ('*.*.common.refusal.31.text', 'No est� permitido enviar publicidad no solicitada o autorizada, material publicitario, "correo basura", "cartas en cadena", "marketing piramidal" o cualquier otra forma de solicitud.Tu aviso se inscribe en estas categor�as.', '2014-03-25 12:27:22.311195', NULL);
-- INSERT INTO conf VALUES ('*.*.common.refusal.32.text', 'No se permite difundir, transmitir o poner a disposici�n de terceros cualquier tipo de informaci�n, elemento o contenido que implica la violaci�n de los derechos de propiedad intelectual, incluyendo derechos de autor y propiedad industrial,  marcas, derechos de autor o de propiedad de los due�os de este sitio o de terceros.', '2014-03-25 12:27:22.311642', NULL);
-- INSERT INTO conf VALUES ('*.*.common.refusal.33.text', 'No se permite difundir, transmitir o poner a disposici�n de terceros, cualquier tipo de informaci�n, elemento o contenido que implique  la violaci�n del secreto de las comunicaciones y la intimidad.', '2014-03-25 12:27:22.31219', NULL);
-- INSERT INTO conf VALUES ('*.*.common.refusal.34.text', 'No se permiten m�s de cinco referencias a los productos  que podr�an constituir la base del intercambio. Los intercambios est�n permitidos en el sitio, pero se debe hacer una lista de menos de cinco referencias a los productos que forman la base del intercambio.', '2014-03-25 12:27:22.312702', NULL);
-- INSERT INTO conf VALUES ('*.*.common.refusal.35.text', 'Categor�a errada', '2014-03-25 12:27:22.313415', NULL);
-- INSERT INTO conf VALUES ('*.*.common.refusal_order.0', '32', '2014-03-25 12:27:22.313963', NULL);
-- INSERT INTO conf VALUES ('*.*.common.refusal_order.1', '33', '2014-03-25 12:27:22.314553', NULL);
-- INSERT INTO conf VALUES ('*.*.common.refusal_order.2', '31', '2014-03-25 12:27:22.3149', NULL);
-- INSERT INTO conf VALUES ('*.*.common.refusal_order.3', '27', '2014-03-25 12:27:22.315405', NULL);
-- INSERT INTO conf VALUES ('*.*.common.refusal_order.4', '25', '2014-03-25 12:27:22.315804', NULL);
-- INSERT INTO conf VALUES ('*.*.common.refusal_order.5', '0', '2014-03-25 12:27:22.316464', NULL);
-- INSERT INTO conf VALUES ('*.*.common.refusal_order.6', '5', '2014-03-25 12:27:22.316936', NULL);
-- INSERT INTO conf VALUES ('*.*.common.refusal_order.7', '18', '2014-03-25 12:27:22.317549', NULL);
-- INSERT INTO conf VALUES ('*.*.common.refusal_order.8', '19', '2014-03-25 12:27:22.3179', NULL);
-- INSERT INTO conf VALUES ('*.*.common.refusal_order.9', '3', '2014-03-25 12:27:22.318502', NULL);
-- INSERT INTO conf VALUES ('*.*.common.refusal_order.10', '20', '2014-03-25 12:27:22.318918', NULL);
-- INSERT INTO conf VALUES ('*.*.common.refusal_order.11', '2', '2014-03-25 12:27:22.319467', NULL);
-- INSERT INTO conf VALUES ('*.*.common.refusal_order.12', '1', '2014-03-25 12:27:22.320004', NULL);
-- INSERT INTO conf VALUES ('*.*.common.refusal_order.13', '12', '2014-03-25 12:27:22.320592', NULL);
-- INSERT INTO conf VALUES ('*.*.common.refusal_order.14', '10', '2014-03-25 12:27:22.32101', NULL);
-- INSERT INTO conf VALUES ('*.*.common.refusal_order.15', '6', '2014-03-25 12:27:22.321588', NULL);
-- INSERT INTO conf VALUES ('*.*.common.refusal_order.16', '7', '2014-03-25 12:27:22.322015', NULL);
-- INSERT INTO conf VALUES ('*.*.common.refusal_order.17', '9', '2014-03-25 12:27:22.322512', NULL);
-- INSERT INTO conf VALUES ('*.*.common.refusal_order.18', '14', '2014-03-25 12:27:22.3229', NULL);
-- INSERT INTO conf VALUES ('*.*.common.refusal_order.19', '11', '2014-03-25 12:27:22.32325', NULL);
-- INSERT INTO conf VALUES ('*.*.common.refusal_order.20', '22', '2014-03-25 12:27:22.323777', NULL);
-- INSERT INTO conf VALUES ('*.*.common.refusal_order.21', '24', '2014-03-25 12:27:22.324168', NULL);
-- INSERT INTO conf VALUES ('*.*.common.refusal_order.22', '13', '2014-03-25 12:27:22.324626', NULL);
-- INSERT INTO conf VALUES ('*.*.common.refusal_order.23', '16', '2014-03-25 12:27:22.325', NULL);
-- INSERT INTO conf VALUES ('*.*.common.refusal_order.24', '23', '2014-03-25 12:27:22.325443', NULL);
-- INSERT INTO conf VALUES ('*.*.common.refusal_order.25', '29', '2014-03-25 12:27:22.325792', NULL);
-- INSERT INTO conf VALUES ('*.*.common.refusal_order.26', '34', '2014-03-25 12:27:22.326116', NULL);
-- INSERT INTO conf VALUES ('*.*.common.refusal_order.27', '8', '2014-03-25 12:27:22.326633', NULL);
-- INSERT INTO conf VALUES ('*.*.common.refusal_order.28', '35', '2014-03-25 12:27:22.32707', NULL);
-- INSERT INTO conf VALUES ('*.*.common.refusal_order.29', '21', '2014-03-25 12:27:22.327638', NULL);
-- INSERT INTO conf VALUES ('*.*.common.refusal_order.30', '4', '2014-03-25 12:27:22.328001', NULL);
-- INSERT INTO conf VALUES ('*.*.common.refusal_order.31', '30', '2014-03-25 12:27:22.328449', NULL);
-- INSERT INTO conf VALUES ('*.*.ais.synonyms.word.1.word', 'caravana', '2014-03-25 12:27:22.330618', NULL);
-- INSERT INTO conf VALUES ('*.*.ais.synonyms.word.1.synonyms.2', 'casa-m�vel', '2014-03-25 12:27:22.331232', NULL);
-- INSERT INTO conf VALUES ('*.*.ais.synonyms.word.1.synonyms.4', 'roulotte', '2014-03-25 12:27:22.331736', NULL);
-- INSERT INTO conf VALUES ('*.*.ais.synonyms.word.2.word', 'piso', '2014-03-25 12:27:22.332087', NULL);
-- INSERT INTO conf VALUES ('*.*.ais.synonyms.word.2.synonyms.1', 'apartamento', '2014-03-25 12:27:22.332576', NULL);
-- INSERT INTO conf VALUES ('*.*.ais.synonyms.word.2.synonyms.2', 'loft', '2014-03-25 12:27:22.332905', NULL);
-- INSERT INTO conf VALUES ('*.*.ais.synonyms.word.3.word', 'vw', '2014-03-25 12:27:22.333228', NULL);
-- INSERT INTO conf VALUES ('*.*.ais.synonyms.word.3.synonyms.1', 'volkswagen', '2014-03-25 12:27:22.333733', NULL);
-- INSERT INTO conf VALUES ('*.*.ais.synonyms.word.4.word', 'carro', '2014-03-25 12:27:22.334092', NULL);
-- INSERT INTO conf VALUES ('*.*.ais.synonyms.word.4.synonyms.1', 'autom�vel', '2014-03-25 12:27:22.334432', NULL);
-- INSERT INTO conf VALUES ('*.*.ais.synonyms.word.4.synonyms.2', 'buga', '2014-03-25 12:27:22.334816', NULL);
-- INSERT INTO conf VALUES ('*.*.ais.misspellings.word.1.misspelled.1', 'vlokswagen', '2014-03-25 12:27:22.335135', NULL);
-- INSERT INTO conf VALUES ('*.*.ais.misspellings.word.1.misspelled.2', 'volksbagen', '2014-03-25 12:27:22.335579', NULL);
-- INSERT INTO conf VALUES ('*.*.ais.misspellings.word.1.misspelled.3', 'volsvagen', '2014-03-25 12:27:22.335928', NULL);
-- INSERT INTO conf VALUES ('*.*.ais.misspellings.word.1.intended', 'volkswagen', '2014-03-25 12:27:22.336229', NULL);
-- INSERT INTO conf VALUES ('*.*.ais.misspellings.word.2.misspelled.1', 'apratamento', '2014-03-25 12:27:22.336688', NULL);
-- INSERT INTO conf VALUES ('*.*.ais.misspellings.word.2.misspelled.2', 'apatamento', '2014-03-25 12:27:22.337145', NULL);
-- INSERT INTO conf VALUES ('*.*.ais.misspellings.word.2.misspelled.3', 'apartametno', '2014-03-25 12:27:22.337761', NULL);
-- INSERT INTO conf VALUES ('*.*.ais.misspellings.word.2.intended', 'apartamento', '2014-03-25 12:27:22.338082', NULL);
-- INSERT INTO conf VALUES ('*.*.ais.misspellings.word.3.misspelled.1', 'carabana', '2014-03-25 12:27:22.338382', NULL);
-- INSERT INTO conf VALUES ('*.*.ais.misspellings.word.3.misspelled.2', 'cravana', '2014-03-25 12:27:22.33885', NULL);
-- INSERT INTO conf VALUES ('*.*.ais.misspellings.word.3.intended', 'caravana', '2014-03-25 12:27:22.339215', NULL);
-- INSERT INTO conf VALUES ('*.*.common.uf_conversion_factor', '22245,5290', '2014-03-25 12:27:22.339666', NULL);
-- INSERT INTO conf VALUES ('*.*.common.uf_conversion_updated', '2014-03-25 12:27:22.34001-03', '2014-03-25 12:27:22.34001', NULL);


--
-- Data for Name: event_log; Type: TABLE DATA; Schema: public; Owner: joaquin
--



--
-- Data for Name: example; Type: TABLE DATA; Schema: public; Owner: joaquin
--

INSERT INTO example VALUES (0, 12);
INSERT INTO example VALUES (1, 56);
INSERT INTO example VALUES (2, 32);
INSERT INTO example VALUES (3, 156);
INSERT INTO example VALUES (4, 2345);


--
-- Data for Name: filters; Type: TABLE DATA; Schema: public; Owner: joaquin
--

-- INSERT INTO filters VALUES (1, 'all', '', '1=1', true);
-- INSERT INTO filters VALUES (2, 'unpaid', '', 'ad_actions.state = ''unpaid''', false);
-- INSERT INTO filters VALUES (3, 'unverified', '', 'ad_actions.state = ''unverified''', false);
-- INSERT INTO filters VALUES (4, 'new', '', 'ad_actions.queue = ''normal'' AND ad_actions.state IN (''pending_review'', ''locked'')', false);
-- INSERT INTO filters VALUES (5, 'unsolved', '', 'ad_actions.queue = ''unsolved'' AND ad_actions.state IN (''pending_review'',''locked'')', false);
-- INSERT INTO filters VALUES (6, 'abuse', '', 'ad_actions.queue = ''abuse'' AND ad_actions.state IN (''pending_review'',''locked'')', false);
-- INSERT INTO filters VALUES (7, 'accepted', '', 'ad_actions.state = ''accepted''', false);
-- INSERT INTO filters VALUES (8, 'refused', '', 'ad_actions.state = ''refused''', true);
-- INSERT INTO filters VALUES (9, 'technical_error', '', 'ad_actions.queue = ''technical_error'' AND ad_actions.state NOT IN (''accepted'', ''refused'')', false);
-- INSERT INTO filters VALUES (10, 'published', '', 'ad_actions.state IN (''accepted'', ''published'') AND NOT EXISTS (SELECT * FROM action_states AS newer WHERE ad_id = ad_actions.ad_id AND action_id != ad_actions.action_id AND state = ''accepted'' AND timestamp > newer.timestamp) AND NOT EXISTS ( SELECT * FROM ads WHERE ads.status = ''deleted'' AND ad_id=ad_actions.ad_id)', false);
-- INSERT INTO filters VALUES (11, 'deleted', '', 'ad_actions.state = ''deleted''', true);
-- INSERT INTO filters VALUES (12, 'autoaccept', '', 'ad_actions.queue = ''autoaccept'' AND ad_actions.state IN (''pending_review'', ''locked'')', false);
-- INSERT INTO filters VALUES (13, 'autorefuse', '', 'ad_actions.queue = ''autorefuse'' AND ad_actions.state IN (''pending_review'', ''locked'')', false);
-- INSERT INTO filters VALUES (16, 'video', '', 'ad_actions.queue = ''video'' AND ad_actions.state IN (''pending_review'', ''locked'')', false);
-- INSERT INTO filters VALUES (17, 'spidered', '', 'ad_actions.queue = ''spidered'' AND ad_actions.state IN (''pending_review'', ''locked'')', false);


--
-- Data for Name: hold_mail_params; Type: TABLE DATA; Schema: public; Owner: joaquin
--



--
-- Data for Name: iteminfo_items; Type: TABLE DATA; Schema: public; Owner: joaquin
--

INSERT INTO iteminfo_items VALUES (1, 'root', false, NULL);
INSERT INTO iteminfo_items VALUES (2, 'primogenito', true, 1);
INSERT INTO iteminfo_items VALUES (3, 'segundon', false, 1);
INSERT INTO iteminfo_items VALUES (4, 'car_info', false, 1);
INSERT INTO iteminfo_items VALUES (5, 'location_info', false, 1);
INSERT INTO iteminfo_items VALUES (6, 'object_info', false, 1);
INSERT INTO iteminfo_items VALUES (7, 'moto_info', false, 1);
INSERT INTO iteminfo_items VALUES (8, 'sac', false, 6);
INSERT INTO iteminfo_items VALUES (9, 'chanel', false, 8);
INSERT INTO iteminfo_items VALUES (10, 'bmw', false, 7);
INSERT INTO iteminfo_items VALUES (11, 'triumph', false, 7);
INSERT INTO iteminfo_items VALUES (12, 'r100', true, 10);
INSERT INTO iteminfo_items VALUES (13, 'r1100 gs', true, 10);
INSERT INTO iteminfo_items VALUES (14, 'k100', true, 10);
INSERT INTO iteminfo_items VALUES (15, 'america', true, 11);
INSERT INTO iteminfo_items VALUES (16, 'adventurer', true, 11);
INSERT INTO iteminfo_items VALUES (17, '1000', false, 12);
INSERT INTO iteminfo_items VALUES (18, '1100', false, 13);
INSERT INTO iteminfo_items VALUES (19, '1100 75 ans', false, 13);
INSERT INTO iteminfo_items VALUES (20, '1100 abs', false, 13);
INSERT INTO iteminfo_items VALUES (21, '1100 abs ergo', false, 13);
INSERT INTO iteminfo_items VALUES (22, '1100 ergo', false, 13);
INSERT INTO iteminfo_items VALUES (23, 'k100 lt', false, 14);
INSERT INTO iteminfo_items VALUES (24, 'k100 rs', false, 14);
INSERT INTO iteminfo_items VALUES (25, '865', false, 15);
INSERT INTO iteminfo_items VALUES (26, '900', false, 16);
INSERT INTO iteminfo_items VALUES (27, 'ford', false, 4);
INSERT INTO iteminfo_items VALUES (28, 'renault', false, 4);
INSERT INTO iteminfo_items VALUES (29, 'seat', false, 4);
INSERT INTO iteminfo_items VALUES (30, 'escort', true, 27);
INSERT INTO iteminfo_items VALUES (31, 'fiesta', true, 27);
INSERT INTO iteminfo_items VALUES (32, 'laguna', true, 28);
INSERT INTO iteminfo_items VALUES (33, 'megane', true, 28);
INSERT INTO iteminfo_items VALUES (34, 'scenic', true, 28);
INSERT INTO iteminfo_items VALUES (35, 'cordoba', true, 29);
INSERT INTO iteminfo_items VALUES (36, 'ibiza', true, 29);
INSERT INTO iteminfo_items VALUES (37, 'leon', true, 29);
INSERT INTO iteminfo_items VALUES (38, 'cosworth', false, 30);
INSERT INTO iteminfo_items VALUES (39, 'rs 2000', false, 30);
INSERT INTO iteminfo_items VALUES (40, '1.8 turbo', false, 31);
INSERT INTO iteminfo_items VALUES (41, '1.4 senso 5p', false, 31);
INSERT INTO iteminfo_items VALUES (42, '1.8 16s', false, 32);
INSERT INTO iteminfo_items VALUES (43, '1.8 gpl', false, 32);
INSERT INTO iteminfo_items VALUES (44, '2.0 rxe', false, 32);
INSERT INTO iteminfo_items VALUES (45, '1.9 dti', false, 32);
INSERT INTO iteminfo_items VALUES (46, '2.2 d rt', false, 32);
INSERT INTO iteminfo_items VALUES (47, '2.2 d rxe', false, 32);
INSERT INTO iteminfo_items VALUES (48, '1.9 dci 110 authentique', false, 32);
INSERT INTO iteminfo_items VALUES (49, '2.2 dci 150 initiale', false, 32);
INSERT INTO iteminfo_items VALUES (50, '1.9 dci 120 ch expression', false, 32);
INSERT INTO iteminfo_items VALUES (51, '1.9 sdi 3p', false, 36);
INSERT INTO iteminfo_items VALUES (52, '1.9 tdi 105 reference', false, 37);
INSERT INTO iteminfo_items VALUES (53, '2.0 tdi 140 stylance', false, 37);
INSERT INTO iteminfo_items VALUES (54, 'tdi 110 signo', false, 37);
INSERT INTO iteminfo_items VALUES (55, 'tdi 150 fr', false, 37);
INSERT INTO iteminfo_items VALUES (56, 'tdi 150 sport', false, 37);
INSERT INTO iteminfo_items VALUES (57, 'rx4 1.9 dci', false, 34);
INSERT INTO iteminfo_items VALUES (58, '1.9 d rte', false, 34);
INSERT INTO iteminfo_items VALUES (59, 'd rte', false, 34);
INSERT INTO iteminfo_items VALUES (2001, '22', false, 5);
INSERT INTO iteminfo_items VALUES (2002, '01', false, 2001);
INSERT INTO iteminfo_items VALUES (2003, '01000', false, 2002);
INSERT INTO iteminfo_items VALUES (2004, 'bourg en bresse', false, 2003);
INSERT INTO iteminfo_items VALUES (2005, 'st denis les bourg', false, 2003);
INSERT INTO iteminfo_items VALUES (2006, '12', false, 5);
INSERT INTO iteminfo_items VALUES (2007, '75', false, 2006);
INSERT INTO iteminfo_items VALUES (2008, '75001', false, 2007);
INSERT INTO iteminfo_items VALUES (2009, 'paris', false, 2008);


--
-- Data for Name: iteminfo_data; Type: TABLE DATA; Schema: public; Owner: joaquin
--

INSERT INTO iteminfo_data VALUES (1, 'Hipoteca', 'De cojones', 1);
INSERT INTO iteminfo_data VALUES (2, 'Sueldo', 'Justico', 1);
INSERT INTO iteminfo_data VALUES (3, 'Broncas', 'Todas', 2);
INSERT INTO iteminfo_data VALUES (4, 'Bici', 'Heredada de su hermano', 3);
INSERT INTO iteminfo_data VALUES (5, 'price_logic_max_cat_2120', '10000', 6);
INSERT INTO iteminfo_data VALUES (6, 'price_logic_max_cat_3040', '20000', 6);
INSERT INTO iteminfo_data VALUES (7, 'price_logic_max_cat_3060', '10000', 6);
INSERT INTO iteminfo_data VALUES (8, 'price_logic_max_cat_8020', '20000', 6);
INSERT INTO iteminfo_data VALUES (9, 'price_logic_max_cat_8020', '560', 8);
INSERT INTO iteminfo_data VALUES (10, 'price_logic_max_cat_22', '1050', 9);
INSERT INTO iteminfo_data VALUES (11, 'price_fraud_cat_8020', '1050', 9);
INSERT INTO iteminfo_data VALUES (12, 'price_logic_max_cat_8060', '2000', 6);
INSERT INTO iteminfo_data VALUES (13, 'price_logic_max_cat_4100', '15000', 6);
INSERT INTO iteminfo_data VALUES (14, 'price_logic_max_cat_4120', '10000', 6);
INSERT INTO iteminfo_data VALUES (15, 'price_logic_max_cat_39', '10000', 6);
INSERT INTO iteminfo_data VALUES (16, 'price_logic_max_cat_41', '20000', 6);
INSERT INTO iteminfo_data VALUES (17, 'price_fraud_1976', '1600', 12);
INSERT INTO iteminfo_data VALUES (18, 'price_logic_min_1976', '2560', 12);
INSERT INTO iteminfo_data VALUES (19, 'price_logic_max_1976', '6400', 12);
INSERT INTO iteminfo_data VALUES (20, 'price_fraud_1977', '975', 12);
INSERT INTO iteminfo_data VALUES (21, 'price_logic_min_1977', '1560', 12);
INSERT INTO iteminfo_data VALUES (22, 'price_logic_max_1977', '3900', 12);
INSERT INTO iteminfo_data VALUES (23, 'price_fraud_1978', '1498.5', 12);
INSERT INTO iteminfo_data VALUES (24, 'price_logic_min_1978', '2397.6', 12);
INSERT INTO iteminfo_data VALUES (25, 'price_logic_max_1978', '5994', 12);
INSERT INTO iteminfo_data VALUES (26, 'price_fraud_1979', '1450', 12);
INSERT INTO iteminfo_data VALUES (27, 'price_logic_min_1979', '2320', 12);
INSERT INTO iteminfo_data VALUES (28, 'price_logic_max_1979', '5800', 12);
INSERT INTO iteminfo_data VALUES (29, 'price_fraud_1980', '1768', 12);
INSERT INTO iteminfo_data VALUES (30, 'price_logic_min_1980', '2828.8', 12);
INSERT INTO iteminfo_data VALUES (31, 'price_logic_max_1980', '7072', 12);
INSERT INTO iteminfo_data VALUES (32, 'price_fraud_1981', '1325', 12);
INSERT INTO iteminfo_data VALUES (33, 'price_logic_min_1981', '2120', 12);
INSERT INTO iteminfo_data VALUES (34, 'price_logic_max_1981', '5300', 12);
INSERT INTO iteminfo_data VALUES (35, 'price_fraud_1982', '1325', 12);
INSERT INTO iteminfo_data VALUES (36, 'price_logic_min_1982', '2120', 12);
INSERT INTO iteminfo_data VALUES (37, 'price_logic_max_1982', '5300', 12);
INSERT INTO iteminfo_data VALUES (38, 'price_fraud_1983', '1500', 12);
INSERT INTO iteminfo_data VALUES (39, 'price_logic_min_1983', '2400', 12);
INSERT INTO iteminfo_data VALUES (40, 'price_logic_max_1983', '6000', 12);
INSERT INTO iteminfo_data VALUES (41, 'price_fraud_1984', '1900', 12);
INSERT INTO iteminfo_data VALUES (42, 'price_logic_min_1984', '3040', 12);
INSERT INTO iteminfo_data VALUES (43, 'price_logic_max_1984', '7600', 12);
INSERT INTO iteminfo_data VALUES (44, 'price_fraud_1988', '1450', 12);
INSERT INTO iteminfo_data VALUES (45, 'price_logic_min_1988', '2320', 12);
INSERT INTO iteminfo_data VALUES (46, 'price_logic_max_1988', '5800', 12);
INSERT INTO iteminfo_data VALUES (47, 'price_fraud_1989', '1500', 12);
INSERT INTO iteminfo_data VALUES (48, 'price_logic_min_1989', '2400', 12);
INSERT INTO iteminfo_data VALUES (49, 'price_logic_max_1989', '6000', 12);
INSERT INTO iteminfo_data VALUES (50, 'price_fraud_1990', '2000', 12);
INSERT INTO iteminfo_data VALUES (51, 'price_logic_min_1990', '3200', 12);
INSERT INTO iteminfo_data VALUES (52, 'price_logic_max_1990', '8000', 12);
INSERT INTO iteminfo_data VALUES (53, 'price_fraud_1991', '1900', 12);
INSERT INTO iteminfo_data VALUES (54, 'price_logic_min_1991', '3040', 12);
INSERT INTO iteminfo_data VALUES (55, 'price_logic_max_1991', '7600', 12);
INSERT INTO iteminfo_data VALUES (56, 'price_fraud_1992', '1942.5', 12);
INSERT INTO iteminfo_data VALUES (57, 'price_logic_min_1992', '3108', 12);
INSERT INTO iteminfo_data VALUES (58, 'price_logic_max_1992', '7770', 12);
INSERT INTO iteminfo_data VALUES (59, 'price_fraud_1993', '2322.5', 12);
INSERT INTO iteminfo_data VALUES (60, 'price_logic_min_1993', '3716', 12);
INSERT INTO iteminfo_data VALUES (61, 'price_logic_max_1993', '9290', 12);
INSERT INTO iteminfo_data VALUES (62, 'price_fraud_1994', '2625', 12);
INSERT INTO iteminfo_data VALUES (63, 'price_logic_min_1994', '4200', 12);
INSERT INTO iteminfo_data VALUES (64, 'price_logic_max_1994', '10500', 12);
INSERT INTO iteminfo_data VALUES (65, 'price_fraud_1995', '1691.5', 12);
INSERT INTO iteminfo_data VALUES (66, 'price_logic_min_1995', '2706.4', 12);
INSERT INTO iteminfo_data VALUES (67, 'price_logic_max_1995', '6766', 12);
INSERT INTO iteminfo_data VALUES (68, 'price_fraud_1996', '2525', 12);
INSERT INTO iteminfo_data VALUES (69, 'price_logic_min_1996', '4040', 12);
INSERT INTO iteminfo_data VALUES (70, 'price_logic_max_1996', '10100', 12);
INSERT INTO iteminfo_data VALUES (71, 'price_fraud_1975', '2487.5', 11);
INSERT INTO iteminfo_data VALUES (72, 'price_logic_min_1975', '3980', 11);
INSERT INTO iteminfo_data VALUES (73, 'price_logic_max_1975', '9950', 11);
INSERT INTO iteminfo_data VALUES (74, 'price_fraud_1977', '2150', 11);
INSERT INTO iteminfo_data VALUES (75, 'price_logic_min_1977', '3440', 11);
INSERT INTO iteminfo_data VALUES (76, 'price_logic_max_1977', '8600', 11);
INSERT INTO iteminfo_data VALUES (77, 'price_fraud_1978', '2400', 11);
INSERT INTO iteminfo_data VALUES (78, 'price_logic_min_1978', '3840', 11);
INSERT INTO iteminfo_data VALUES (79, 'price_logic_max_1978', '9600', 11);
INSERT INTO iteminfo_data VALUES (80, 'price_fraud_1979', '2625', 11);
INSERT INTO iteminfo_data VALUES (81, 'price_logic_min_1979', '4200', 11);
INSERT INTO iteminfo_data VALUES (82, 'price_logic_max_1979', '10500', 11);
INSERT INTO iteminfo_data VALUES (83, 'price_fraud_1980', '3050', 11);
INSERT INTO iteminfo_data VALUES (84, 'price_logic_min_1980', '4880', 11);
INSERT INTO iteminfo_data VALUES (85, 'price_logic_max_1980', '12200', 11);
INSERT INTO iteminfo_data VALUES (86, 'price_fraud_1981', '2750', 11);
INSERT INTO iteminfo_data VALUES (87, 'price_logic_min_1981', '4400', 11);
INSERT INTO iteminfo_data VALUES (88, 'price_logic_max_1981', '11000', 11);
INSERT INTO iteminfo_data VALUES (89, 'price_fraud_1986', '6750', 11);
INSERT INTO iteminfo_data VALUES (90, 'price_logic_min_1986', '10800', 11);
INSERT INTO iteminfo_data VALUES (91, 'price_logic_max_1986', '27000', 11);
INSERT INTO iteminfo_data VALUES (92, 'price_fraud_1992', '1428.5', 11);
INSERT INTO iteminfo_data VALUES (93, 'price_logic_min_1992', '2285.6', 11);
INSERT INTO iteminfo_data VALUES (94, 'price_logic_max_1992', '5714', 11);
INSERT INTO iteminfo_data VALUES (95, 'price_fraud_1993', '1461', 11);
INSERT INTO iteminfo_data VALUES (96, 'price_logic_min_1993', '2337.6', 11);
INSERT INTO iteminfo_data VALUES (97, 'price_logic_max_1993', '5844', 11);
INSERT INTO iteminfo_data VALUES (98, 'price_fraud_1994', '1866.5', 11);
INSERT INTO iteminfo_data VALUES (99, 'price_logic_min_1994', '2986.4', 11);
INSERT INTO iteminfo_data VALUES (100, 'price_logic_max_1994', '7466', 11);
INSERT INTO iteminfo_data VALUES (101, 'price_fraud_1995', '1736.5', 11);
INSERT INTO iteminfo_data VALUES (102, 'price_logic_min_1995', '2778.4', 11);
INSERT INTO iteminfo_data VALUES (103, 'price_logic_max_1995', '6946', 11);
INSERT INTO iteminfo_data VALUES (104, 'price_fraud_1996', '1842.5', 11);
INSERT INTO iteminfo_data VALUES (105, 'price_logic_min_1996', '2948', 11);
INSERT INTO iteminfo_data VALUES (106, 'price_logic_max_1996', '7370', 11);
INSERT INTO iteminfo_data VALUES (107, 'price_fraud_1997', '1906', 11);
INSERT INTO iteminfo_data VALUES (108, 'price_logic_min_1997', '3049.6', 11);
INSERT INTO iteminfo_data VALUES (109, 'price_logic_max_1997', '7624', 11);
INSERT INTO iteminfo_data VALUES (110, 'price_fraud_1998', '2011.5', 11);
INSERT INTO iteminfo_data VALUES (111, 'price_logic_min_1998', '3218.4', 11);
INSERT INTO iteminfo_data VALUES (112, 'price_logic_max_1998', '8046', 11);
INSERT INTO iteminfo_data VALUES (113, 'price_fraud_1999', '2202.5', 11);
INSERT INTO iteminfo_data VALUES (114, 'price_logic_min_1999', '3524', 11);
INSERT INTO iteminfo_data VALUES (115, 'price_logic_max_1999', '8810', 11);
INSERT INTO iteminfo_data VALUES (116, 'price_fraud_2000', '2071.5', 11);
INSERT INTO iteminfo_data VALUES (117, 'price_logic_min_2000', '3314.4', 11);
INSERT INTO iteminfo_data VALUES (118, 'price_logic_max_2000', '8286', 11);
INSERT INTO iteminfo_data VALUES (119, 'price_fraud_2001', '2397.5', 11);
INSERT INTO iteminfo_data VALUES (120, 'price_logic_min_2001', '3836', 11);
INSERT INTO iteminfo_data VALUES (121, 'price_logic_max_2001', '9590', 11);
INSERT INTO iteminfo_data VALUES (122, 'price_fraud_2002', '2549', 11);
INSERT INTO iteminfo_data VALUES (123, 'price_logic_min_2002', '4078.4', 11);
INSERT INTO iteminfo_data VALUES (124, 'price_logic_max_2002', '10196', 11);
INSERT INTO iteminfo_data VALUES (125, 'price_fraud_2003', '2785', 11);
INSERT INTO iteminfo_data VALUES (126, 'price_logic_min_2003', '4456', 11);
INSERT INTO iteminfo_data VALUES (127, 'price_logic_max_2003', '11140', 11);
INSERT INTO iteminfo_data VALUES (128, 'price_fraud_2004', '3007', 11);
INSERT INTO iteminfo_data VALUES (129, 'price_logic_min_2004', '4811.2', 11);
INSERT INTO iteminfo_data VALUES (130, 'price_logic_max_2004', '12028', 11);
INSERT INTO iteminfo_data VALUES (131, 'price_fraud_2005', '4200.5', 11);
INSERT INTO iteminfo_data VALUES (132, 'price_logic_min_2005', '6720.8', 11);
INSERT INTO iteminfo_data VALUES (133, 'price_logic_max_2005', '16802', 11);
INSERT INTO iteminfo_data VALUES (134, 'price_fraud_2006', '4121.5', 11);
INSERT INTO iteminfo_data VALUES (135, 'price_logic_min_2006', '6594.4', 11);
INSERT INTO iteminfo_data VALUES (136, 'price_logic_max_2006', '16486', 11);
INSERT INTO iteminfo_data VALUES (137, 'price_fraud_2007', '4257.5', 11);
INSERT INTO iteminfo_data VALUES (138, 'price_logic_min_2007', '6812', 11);
INSERT INTO iteminfo_data VALUES (139, 'price_logic_max_2007', '17030', 11);
INSERT INTO iteminfo_data VALUES (140, 'price_fraud_2008', '4357.5', 11);
INSERT INTO iteminfo_data VALUES (141, 'price_logic_min_2008', '6972', 11);
INSERT INTO iteminfo_data VALUES (142, 'price_logic_max_2008', '17430', 11);
INSERT INTO iteminfo_data VALUES (143, 'price_fraud_2001', '2995', 15);
INSERT INTO iteminfo_data VALUES (144, 'price_logic_min_2001', '4792', 15);
INSERT INTO iteminfo_data VALUES (145, 'price_logic_max_2001', '11980', 15);
INSERT INTO iteminfo_data VALUES (146, 'price_fraud_2002', '2625', 15);
INSERT INTO iteminfo_data VALUES (147, 'price_logic_min_2002', '4200', 15);
INSERT INTO iteminfo_data VALUES (148, 'price_logic_max_2002', '10500', 15);
INSERT INTO iteminfo_data VALUES (149, 'price_fraud_2003', '2600', 15);
INSERT INTO iteminfo_data VALUES (150, 'price_logic_min_2003', '4160', 15);
INSERT INTO iteminfo_data VALUES (151, 'price_logic_max_2003', '10400', 15);
INSERT INTO iteminfo_data VALUES (152, 'price_fraud_2004', '3250', 15);
INSERT INTO iteminfo_data VALUES (153, 'price_logic_min_2004', '5200', 15);
INSERT INTO iteminfo_data VALUES (154, 'price_logic_max_2004', '13000', 15);
INSERT INTO iteminfo_data VALUES (155, 'price_fraud_2007', '3750', 15);
INSERT INTO iteminfo_data VALUES (156, 'price_logic_min_2007', '6000', 15);
INSERT INTO iteminfo_data VALUES (157, 'price_logic_max_2007', '15000', 15);
INSERT INTO iteminfo_data VALUES (158, 'price_fraud_1975', '1720.5', 10);
INSERT INTO iteminfo_data VALUES (159, 'price_logic_min_1975', '2752.8', 10);
INSERT INTO iteminfo_data VALUES (160, 'price_logic_max_1975', '6882', 10);
INSERT INTO iteminfo_data VALUES (161, 'price_fraud_1976', '1610.5', 10);
INSERT INTO iteminfo_data VALUES (162, 'price_logic_min_1976', '2576.8', 10);
INSERT INTO iteminfo_data VALUES (163, 'price_logic_max_1976', '6442', 10);
INSERT INTO iteminfo_data VALUES (164, 'price_fraud_1977', '1150', 10);
INSERT INTO iteminfo_data VALUES (165, 'price_logic_min_1977', '1840', 10);
INSERT INTO iteminfo_data VALUES (166, 'price_logic_max_1977', '4600', 10);
INSERT INTO iteminfo_data VALUES (167, 'price_fraud_1978', '1491.5', 10);
INSERT INTO iteminfo_data VALUES (168, 'price_logic_min_1978', '2386.4', 10);
INSERT INTO iteminfo_data VALUES (169, 'price_logic_max_1978', '5966', 10);
INSERT INTO iteminfo_data VALUES (170, 'price_fraud_1979', '1272.5', 10);
INSERT INTO iteminfo_data VALUES (171, 'price_logic_min_1979', '2036', 10);
INSERT INTO iteminfo_data VALUES (172, 'price_logic_max_1979', '5090', 10);
INSERT INTO iteminfo_data VALUES (173, 'price_fraud_1980', '1623', 10);
INSERT INTO iteminfo_data VALUES (174, 'price_logic_min_1980', '2596.8', 10);
INSERT INTO iteminfo_data VALUES (175, 'price_logic_max_1980', '6492', 10);
INSERT INTO iteminfo_data VALUES (176, 'price_fraud_1981', '1257', 10);
INSERT INTO iteminfo_data VALUES (177, 'price_logic_min_1981', '2011.2', 10);
INSERT INTO iteminfo_data VALUES (178, 'price_logic_max_1981', '5028', 10);
INSERT INTO iteminfo_data VALUES (179, 'price_fraud_1982', '1237.5', 10);
INSERT INTO iteminfo_data VALUES (180, 'price_logic_min_1982', '1980', 10);
INSERT INTO iteminfo_data VALUES (181, 'price_logic_max_1982', '4950', 10);
INSERT INTO iteminfo_data VALUES (182, 'price_fraud_1983', '1111', 10);
INSERT INTO iteminfo_data VALUES (183, 'price_logic_min_1983', '1777.6', 10);
INSERT INTO iteminfo_data VALUES (184, 'price_logic_max_1983', '4444', 10);
INSERT INTO iteminfo_data VALUES (185, 'price_fraud_1984', '1127', 10);
INSERT INTO iteminfo_data VALUES (186, 'price_logic_min_1984', '1803.2', 10);
INSERT INTO iteminfo_data VALUES (187, 'price_logic_max_1984', '4508', 10);
INSERT INTO iteminfo_data VALUES (188, 'price_fraud_1985', '1425', 10);
INSERT INTO iteminfo_data VALUES (189, 'price_logic_min_1985', '2280', 10);
INSERT INTO iteminfo_data VALUES (190, 'price_logic_max_1985', '5700', 10);
INSERT INTO iteminfo_data VALUES (191, 'price_fraud_1986', '1179', 10);
INSERT INTO iteminfo_data VALUES (192, 'price_logic_min_1986', '1886.4', 10);
INSERT INTO iteminfo_data VALUES (193, 'price_logic_max_1986', '4716', 10);
INSERT INTO iteminfo_data VALUES (194, 'price_fraud_1987', '1336.5', 10);
INSERT INTO iteminfo_data VALUES (195, 'price_logic_min_1987', '2138.4', 10);
INSERT INTO iteminfo_data VALUES (196, 'price_logic_max_1987', '5346', 10);
INSERT INTO iteminfo_data VALUES (197, 'price_fraud_1988', '1402', 10);
INSERT INTO iteminfo_data VALUES (198, 'price_logic_min_1988', '2243.2', 10);
INSERT INTO iteminfo_data VALUES (199, 'price_logic_max_1988', '5608', 10);
INSERT INTO iteminfo_data VALUES (200, 'price_fraud_1989', '1327.5', 10);
INSERT INTO iteminfo_data VALUES (201, 'price_logic_min_1989', '2124', 10);
INSERT INTO iteminfo_data VALUES (202, 'price_logic_max_1989', '5310', 10);
INSERT INTO iteminfo_data VALUES (203, 'price_fraud_1990', '1678', 10);
INSERT INTO iteminfo_data VALUES (204, 'price_logic_min_1990', '2684.8', 10);
INSERT INTO iteminfo_data VALUES (205, 'price_logic_max_1990', '6712', 10);
INSERT INTO iteminfo_data VALUES (206, 'price_fraud_1991', '1612', 10);
INSERT INTO iteminfo_data VALUES (207, 'price_logic_min_1991', '2579.2', 10);
INSERT INTO iteminfo_data VALUES (208, 'price_logic_max_1991', '6448', 10);
INSERT INTO iteminfo_data VALUES (209, 'price_fraud_1992', '1703', 10);
INSERT INTO iteminfo_data VALUES (210, 'price_logic_min_1992', '2724.8', 10);
INSERT INTO iteminfo_data VALUES (211, 'price_logic_max_1992', '6812', 10);
INSERT INTO iteminfo_data VALUES (212, 'price_fraud_1993', '1699.5', 10);
INSERT INTO iteminfo_data VALUES (213, 'price_logic_min_1993', '2719.2', 10);
INSERT INTO iteminfo_data VALUES (214, 'price_logic_max_1993', '6798', 10);
INSERT INTO iteminfo_data VALUES (215, 'price_fraud_1994', '1561', 10);
INSERT INTO iteminfo_data VALUES (216, 'price_logic_min_1994', '2497.6', 10);
INSERT INTO iteminfo_data VALUES (217, 'price_logic_max_1994', '6244', 10);
INSERT INTO iteminfo_data VALUES (218, 'price_fraud_1995', '1942', 10);
INSERT INTO iteminfo_data VALUES (219, 'price_logic_min_1995', '3107.2', 10);
INSERT INTO iteminfo_data VALUES (220, 'price_logic_max_1995', '7768', 10);
INSERT INTO iteminfo_data VALUES (221, 'price_fraud_1996', '1993', 10);
INSERT INTO iteminfo_data VALUES (222, 'price_logic_min_1996', '3188.8', 10);
INSERT INTO iteminfo_data VALUES (223, 'price_logic_max_1996', '7972', 10);
INSERT INTO iteminfo_data VALUES (224, 'price_fraud_1997', '2398', 10);
INSERT INTO iteminfo_data VALUES (225, 'price_logic_min_1997', '3836.8', 10);
INSERT INTO iteminfo_data VALUES (226, 'price_logic_max_1997', '9592', 10);
INSERT INTO iteminfo_data VALUES (227, 'price_fraud_1998', '2456.5', 10);
INSERT INTO iteminfo_data VALUES (228, 'price_logic_min_1998', '3930.4', 10);
INSERT INTO iteminfo_data VALUES (229, 'price_logic_max_1998', '9826', 10);
INSERT INTO iteminfo_data VALUES (230, 'price_fraud_1999', '2640', 10);
INSERT INTO iteminfo_data VALUES (231, 'price_logic_min_1999', '4224', 10);
INSERT INTO iteminfo_data VALUES (232, 'price_logic_max_1999', '10560', 10);
INSERT INTO iteminfo_data VALUES (233, 'price_fraud_2000', '2942.5', 10);
INSERT INTO iteminfo_data VALUES (234, 'price_logic_min_2000', '4708', 10);
INSERT INTO iteminfo_data VALUES (235, 'price_logic_max_2000', '11770', 10);
INSERT INTO iteminfo_data VALUES (236, 'price_fraud_2001', '3435.5', 10);
INSERT INTO iteminfo_data VALUES (237, 'price_logic_min_2001', '5496.8', 10);
INSERT INTO iteminfo_data VALUES (238, 'price_logic_max_2001', '13742', 10);
INSERT INTO iteminfo_data VALUES (239, 'price_fraud_2002', '3541', 10);
INSERT INTO iteminfo_data VALUES (240, 'price_logic_min_2002', '5665.6', 10);
INSERT INTO iteminfo_data VALUES (241, 'price_logic_max_2002', '14164', 10);
INSERT INTO iteminfo_data VALUES (242, 'price_fraud_2003', '3900', 10);
INSERT INTO iteminfo_data VALUES (243, 'price_logic_min_2003', '6240', 10);
INSERT INTO iteminfo_data VALUES (244, 'price_logic_max_2003', '15600', 10);
INSERT INTO iteminfo_data VALUES (245, 'price_fraud_2004', '4205.5', 10);
INSERT INTO iteminfo_data VALUES (246, 'price_logic_min_2004', '6728.8', 10);
INSERT INTO iteminfo_data VALUES (247, 'price_logic_max_2004', '16822', 10);
INSERT INTO iteminfo_data VALUES (248, 'price_fraud_2005', '4750.5', 10);
INSERT INTO iteminfo_data VALUES (249, 'price_logic_min_2005', '7600.8', 10);
INSERT INTO iteminfo_data VALUES (250, 'price_logic_max_2005', '19002', 10);
INSERT INTO iteminfo_data VALUES (251, 'price_fraud_2006', '5451.5', 10);
INSERT INTO iteminfo_data VALUES (252, 'price_logic_min_2006', '8722.4', 10);
INSERT INTO iteminfo_data VALUES (253, 'price_logic_max_2006', '21806', 10);
INSERT INTO iteminfo_data VALUES (254, 'price_fraud_2007', '6064', 10);
INSERT INTO iteminfo_data VALUES (255, 'price_logic_min_2007', '9702.4', 10);
INSERT INTO iteminfo_data VALUES (256, 'price_logic_max_2007', '24256', 10);
INSERT INTO iteminfo_data VALUES (257, 'price_fraud_2008', '6650.5', 10);
INSERT INTO iteminfo_data VALUES (258, 'price_logic_min_2008', '10640.8', 10);
INSERT INTO iteminfo_data VALUES (259, 'price_logic_max_2008', '26602', 10);
INSERT INTO iteminfo_data VALUES (260, 'price_fraud_1996', '1950', 26);
INSERT INTO iteminfo_data VALUES (261, 'price_logic_min_1996', '3120', 26);
INSERT INTO iteminfo_data VALUES (262, 'price_logic_max_1996', '7800', 26);
INSERT INTO iteminfo_data VALUES (263, 'price_fraud_1997', '1750', 26);
INSERT INTO iteminfo_data VALUES (264, 'price_logic_min_1997', '2800', 26);
INSERT INTO iteminfo_data VALUES (265, 'price_logic_max_1997', '7000', 26);
INSERT INTO iteminfo_data VALUES (266, 'price_fraud_2001', '2800', 26);
INSERT INTO iteminfo_data VALUES (267, 'price_logic_min_2001', '4480', 26);
INSERT INTO iteminfo_data VALUES (268, 'price_logic_max_2001', '11200', 26);
INSERT INTO iteminfo_data VALUES (269, 'price_fraud_1996', '1875', 16);
INSERT INTO iteminfo_data VALUES (270, 'price_logic_min_1996', '3000', 16);
INSERT INTO iteminfo_data VALUES (271, 'price_logic_max_1996', '7500', 16);
INSERT INTO iteminfo_data VALUES (272, 'price_fraud_1997', '1766.5', 16);
INSERT INTO iteminfo_data VALUES (273, 'price_logic_min_1997', '2826.4', 16);
INSERT INTO iteminfo_data VALUES (274, 'price_logic_max_1997', '7066', 16);
INSERT INTO iteminfo_data VALUES (275, 'price_fraud_1998', '2000', 16);
INSERT INTO iteminfo_data VALUES (276, 'price_logic_min_1998', '3200', 16);
INSERT INTO iteminfo_data VALUES (277, 'price_logic_max_1998', '8000', 16);
INSERT INTO iteminfo_data VALUES (278, 'price_fraud_2001', '2800', 16);
INSERT INTO iteminfo_data VALUES (279, 'price_logic_min_2001', '4480', 16);
INSERT INTO iteminfo_data VALUES (280, 'price_logic_max_2001', '11200', 16);
INSERT INTO iteminfo_data VALUES (281, 'price_fraud_1992', '5625', 38);
INSERT INTO iteminfo_data VALUES (282, 'price_logic_min_1992', '9000', 38);
INSERT INTO iteminfo_data VALUES (283, 'price_logic_max_1992', '22500', 38);
INSERT INTO iteminfo_data VALUES (284, 'price_fraud_1993', '9233', 38);
INSERT INTO iteminfo_data VALUES (285, 'price_logic_min_1993', '14772.8', 38);
INSERT INTO iteminfo_data VALUES (286, 'price_logic_max_1993', '36932', 38);
INSERT INTO iteminfo_data VALUES (287, 'price_fraud_1994', '10500', 38);
INSERT INTO iteminfo_data VALUES (288, 'price_logic_min_1994', '16800', 38);
INSERT INTO iteminfo_data VALUES (289, 'price_logic_max_1994', '42000', 38);
INSERT INTO iteminfo_data VALUES (290, 'price_fraud_1995', '12000', 38);
INSERT INTO iteminfo_data VALUES (291, 'price_logic_min_1995', '19200', 38);
INSERT INTO iteminfo_data VALUES (292, 'price_logic_max_1995', '48000', 38);
INSERT INTO iteminfo_data VALUES (293, 'price_fraud_1992', '1450', 39);
INSERT INTO iteminfo_data VALUES (294, 'price_logic_min_1992', '2320', 39);
INSERT INTO iteminfo_data VALUES (295, 'price_logic_max_1992', '5800', 39);
INSERT INTO iteminfo_data VALUES (296, 'price_fraud_1993', '2250', 39);
INSERT INTO iteminfo_data VALUES (297, 'price_logic_min_1993', '3600', 39);
INSERT INTO iteminfo_data VALUES (298, 'price_logic_max_1993', '9000', 39);
INSERT INTO iteminfo_data VALUES (299, 'price_fraud_1995', '1783', 39);
INSERT INTO iteminfo_data VALUES (300, 'price_logic_min_1995', '2852.8', 39);
INSERT INTO iteminfo_data VALUES (301, 'price_logic_max_1995', '7132', 39);
INSERT INTO iteminfo_data VALUES (302, 'price_fraud_1996', '1683', 39);
INSERT INTO iteminfo_data VALUES (303, 'price_logic_min_1996', '2692.8', 39);
INSERT INTO iteminfo_data VALUES (304, 'price_logic_max_1996', '6732', 39);
INSERT INTO iteminfo_data VALUES (305, 'price_fraud_1998', '6100', 39);
INSERT INTO iteminfo_data VALUES (306, 'price_logic_min_1998', '9760', 39);
INSERT INTO iteminfo_data VALUES (307, 'price_logic_max_1998', '24400', 39);
INSERT INTO iteminfo_data VALUES (308, 'price_fraud_1975', '991.5', 30);
INSERT INTO iteminfo_data VALUES (309, 'price_logic_min_1975', '1586.4', 30);
INSERT INTO iteminfo_data VALUES (310, 'price_logic_max_1975', '3966', 30);
INSERT INTO iteminfo_data VALUES (311, 'price_fraud_1976', '750', 30);
INSERT INTO iteminfo_data VALUES (312, 'price_logic_min_1976', '1200', 30);
INSERT INTO iteminfo_data VALUES (313, 'price_logic_max_1976', '3000', 30);
INSERT INTO iteminfo_data VALUES (314, 'price_fraud_1977', '1500', 30);
INSERT INTO iteminfo_data VALUES (315, 'price_logic_min_1977', '2400', 30);
INSERT INTO iteminfo_data VALUES (316, 'price_logic_max_1977', '6000', 30);
INSERT INTO iteminfo_data VALUES (317, 'price_fraud_1979', '437.5', 30);
INSERT INTO iteminfo_data VALUES (318, 'price_logic_min_1979', '700', 30);
INSERT INTO iteminfo_data VALUES (319, 'price_logic_max_1979', '1750', 30);
INSERT INTO iteminfo_data VALUES (320, 'price_fraud_1980', '1050', 30);
INSERT INTO iteminfo_data VALUES (321, 'price_logic_min_1980', '1680', 30);
INSERT INTO iteminfo_data VALUES (322, 'price_logic_max_1980', '4200', 30);
INSERT INTO iteminfo_data VALUES (323, 'price_fraud_1981', '375', 30);
INSERT INTO iteminfo_data VALUES (324, 'price_logic_min_1981', '600', 30);
INSERT INTO iteminfo_data VALUES (325, 'price_logic_max_1981', '1500', 30);
INSERT INTO iteminfo_data VALUES (326, 'price_fraud_1982', '230', 30);
INSERT INTO iteminfo_data VALUES (327, 'price_logic_min_1982', '368', 30);
INSERT INTO iteminfo_data VALUES (328, 'price_logic_max_1982', '920', 30);
INSERT INTO iteminfo_data VALUES (329, 'price_fraud_1983', '343.5', 30);
INSERT INTO iteminfo_data VALUES (330, 'price_logic_min_1983', '549.6', 30);
INSERT INTO iteminfo_data VALUES (331, 'price_logic_max_1983', '1374', 30);
INSERT INTO iteminfo_data VALUES (332, 'price_fraud_1984', '520', 30);
INSERT INTO iteminfo_data VALUES (333, 'price_logic_min_1984', '832', 30);
INSERT INTO iteminfo_data VALUES (334, 'price_logic_max_1984', '2080', 30);
INSERT INTO iteminfo_data VALUES (335, 'price_fraud_1985', '365', 30);
INSERT INTO iteminfo_data VALUES (336, 'price_logic_min_1985', '584', 30);
INSERT INTO iteminfo_data VALUES (337, 'price_logic_max_1985', '1460', 30);
INSERT INTO iteminfo_data VALUES (338, 'price_fraud_1986', '703.5', 30);
INSERT INTO iteminfo_data VALUES (339, 'price_logic_min_1986', '1125.6', 30);
INSERT INTO iteminfo_data VALUES (340, 'price_logic_max_1986', '2814', 30);
INSERT INTO iteminfo_data VALUES (341, 'price_fraud_1987', '432', 30);
INSERT INTO iteminfo_data VALUES (342, 'price_logic_min_1987', '691.2', 30);
INSERT INTO iteminfo_data VALUES (343, 'price_logic_max_1987', '1728', 30);
INSERT INTO iteminfo_data VALUES (344, 'price_fraud_1988', '526', 30);
INSERT INTO iteminfo_data VALUES (345, 'price_logic_min_1988', '841.6', 30);
INSERT INTO iteminfo_data VALUES (346, 'price_logic_max_1988', '2104', 30);
INSERT INTO iteminfo_data VALUES (347, 'price_fraud_1989', '421.5', 30);
INSERT INTO iteminfo_data VALUES (348, 'price_logic_min_1989', '674.4', 30);
INSERT INTO iteminfo_data VALUES (349, 'price_logic_max_1989', '1686', 30);
INSERT INTO iteminfo_data VALUES (350, 'price_fraud_1990', '459', 30);
INSERT INTO iteminfo_data VALUES (351, 'price_logic_min_1990', '734.4', 30);
INSERT INTO iteminfo_data VALUES (352, 'price_logic_max_1990', '1836', 30);
INSERT INTO iteminfo_data VALUES (353, 'price_fraud_1991', '460.5', 30);
INSERT INTO iteminfo_data VALUES (354, 'price_logic_min_1991', '736.8', 30);
INSERT INTO iteminfo_data VALUES (355, 'price_logic_max_1991', '1842', 30);
INSERT INTO iteminfo_data VALUES (356, 'price_fraud_1992', '614', 30);
INSERT INTO iteminfo_data VALUES (357, 'price_logic_min_1992', '982.4', 30);
INSERT INTO iteminfo_data VALUES (358, 'price_logic_max_1992', '2456', 30);
INSERT INTO iteminfo_data VALUES (359, 'price_fraud_1993', '699.5', 30);
INSERT INTO iteminfo_data VALUES (360, 'price_logic_min_1993', '1119.2', 30);
INSERT INTO iteminfo_data VALUES (361, 'price_logic_max_1993', '2798', 30);
INSERT INTO iteminfo_data VALUES (362, 'price_fraud_1994', '684', 30);
INSERT INTO iteminfo_data VALUES (363, 'price_logic_min_1994', '1094.4', 30);
INSERT INTO iteminfo_data VALUES (364, 'price_logic_max_1994', '2736', 30);
INSERT INTO iteminfo_data VALUES (365, 'price_fraud_1995', '768', 30);
INSERT INTO iteminfo_data VALUES (366, 'price_logic_min_1995', '1228.8', 30);
INSERT INTO iteminfo_data VALUES (367, 'price_logic_max_1995', '3072', 30);
INSERT INTO iteminfo_data VALUES (368, 'price_fraud_1996', '883.5', 30);
INSERT INTO iteminfo_data VALUES (369, 'price_logic_min_1996', '1413.6', 30);
INSERT INTO iteminfo_data VALUES (370, 'price_logic_max_1996', '3534', 30);
INSERT INTO iteminfo_data VALUES (371, 'price_fraud_1997', '956.5', 30);
INSERT INTO iteminfo_data VALUES (372, 'price_logic_min_1997', '1530.4', 30);
INSERT INTO iteminfo_data VALUES (373, 'price_logic_max_1997', '3826', 30);
INSERT INTO iteminfo_data VALUES (374, 'price_fraud_1998', '1153.5', 30);
INSERT INTO iteminfo_data VALUES (375, 'price_logic_min_1998', '1845.6', 30);
INSERT INTO iteminfo_data VALUES (376, 'price_logic_max_1998', '4614', 30);
INSERT INTO iteminfo_data VALUES (377, 'price_fraud_1999', '1244.5', 30);
INSERT INTO iteminfo_data VALUES (378, 'price_logic_min_1999', '1991.2', 30);
INSERT INTO iteminfo_data VALUES (379, 'price_logic_max_1999', '4978', 30);
INSERT INTO iteminfo_data VALUES (380, 'price_fraud_2000', '1298.5', 30);
INSERT INTO iteminfo_data VALUES (381, 'price_logic_min_2000', '2077.6', 30);
INSERT INTO iteminfo_data VALUES (382, 'price_logic_max_2000', '5194', 30);
INSERT INTO iteminfo_data VALUES (383, 'price_fraud_2001', '1873.5', 30);
INSERT INTO iteminfo_data VALUES (384, 'price_logic_min_2001', '2997.6', 30);
INSERT INTO iteminfo_data VALUES (385, 'price_logic_max_2001', '7494', 30);
INSERT INTO iteminfo_data VALUES (386, 'price_fraud_2002', '1600', 30);
INSERT INTO iteminfo_data VALUES (387, 'price_logic_min_2002', '2560', 30);
INSERT INTO iteminfo_data VALUES (388, 'price_logic_max_2002', '6400', 30);
INSERT INTO iteminfo_data VALUES (389, 'price_fraud_2007', '1150', 30);
INSERT INTO iteminfo_data VALUES (390, 'price_logic_min_2007', '1840', 30);
INSERT INTO iteminfo_data VALUES (391, 'price_logic_max_2007', '4600', 30);
INSERT INTO iteminfo_data VALUES (392, 'price_fraud_1993', '300', 40);
INSERT INTO iteminfo_data VALUES (393, 'price_logic_min_1993', '480', 40);
INSERT INTO iteminfo_data VALUES (394, 'price_logic_max_1993', '1200', 40);
INSERT INTO iteminfo_data VALUES (395, 'price_fraud_1996', '10000', 40);
INSERT INTO iteminfo_data VALUES (396, 'price_logic_min_1996', '16000', 40);
INSERT INTO iteminfo_data VALUES (397, 'price_logic_max_1996', '40000', 40);
INSERT INTO iteminfo_data VALUES (398, 'price_fraud_2001', '1836', 40);
INSERT INTO iteminfo_data VALUES (399, 'price_logic_min_2001', '2937.6', 40);
INSERT INTO iteminfo_data VALUES (400, 'price_logic_max_2001', '7344', 40);
INSERT INTO iteminfo_data VALUES (401, 'price_fraud_2002', '2100', 40);
INSERT INTO iteminfo_data VALUES (402, 'price_logic_min_2002', '3360', 40);
INSERT INTO iteminfo_data VALUES (403, 'price_logic_max_2002', '8400', 40);
INSERT INTO iteminfo_data VALUES (404, 'price_fraud_2004', '3930', 41);
INSERT INTO iteminfo_data VALUES (405, 'price_logic_min_2004', '6288', 41);
INSERT INTO iteminfo_data VALUES (406, 'price_logic_max_2004', '15720', 41);
INSERT INTO iteminfo_data VALUES (407, 'price_fraud_2005', '4316.5', 41);
INSERT INTO iteminfo_data VALUES (408, 'price_logic_min_2005', '6906.4', 41);
INSERT INTO iteminfo_data VALUES (409, 'price_logic_max_2005', '17266', 41);
INSERT INTO iteminfo_data VALUES (410, 'price_fraud_2006', '4622.5', 41);
INSERT INTO iteminfo_data VALUES (411, 'price_logic_min_2006', '7396', 41);
INSERT INTO iteminfo_data VALUES (412, 'price_logic_max_2006', '18490', 41);
INSERT INTO iteminfo_data VALUES (413, 'price_fraud_2007', '5154.5', 41);
INSERT INTO iteminfo_data VALUES (414, 'price_logic_min_2007', '8247.2', 41);
INSERT INTO iteminfo_data VALUES (415, 'price_logic_max_2007', '20618', 41);
INSERT INTO iteminfo_data VALUES (416, 'price_fraud_2008', '6725', 41);
INSERT INTO iteminfo_data VALUES (417, 'price_logic_min_2008', '10760', 41);
INSERT INTO iteminfo_data VALUES (418, 'price_logic_max_2008', '26900', 41);
INSERT INTO iteminfo_data VALUES (419, 'price_fraud_1975', '100', 31);
INSERT INTO iteminfo_data VALUES (420, 'price_logic_min_1975', '160', 31);
INSERT INTO iteminfo_data VALUES (421, 'price_logic_max_1975', '400', 31);
INSERT INTO iteminfo_data VALUES (422, 'price_fraud_1976', '1150', 31);
INSERT INTO iteminfo_data VALUES (423, 'price_logic_min_1976', '1840', 31);
INSERT INTO iteminfo_data VALUES (424, 'price_logic_max_1976', '4600', 31);
INSERT INTO iteminfo_data VALUES (425, 'price_fraud_1977', '220', 31);
INSERT INTO iteminfo_data VALUES (426, 'price_logic_min_1977', '352', 31);
INSERT INTO iteminfo_data VALUES (427, 'price_logic_max_1977', '880', 31);
INSERT INTO iteminfo_data VALUES (428, 'price_fraud_1978', '375', 31);
INSERT INTO iteminfo_data VALUES (429, 'price_logic_min_1978', '600', 31);
INSERT INTO iteminfo_data VALUES (430, 'price_logic_max_1978', '1500', 31);
INSERT INTO iteminfo_data VALUES (431, 'price_fraud_1980', '225', 31);
INSERT INTO iteminfo_data VALUES (432, 'price_logic_min_1980', '360', 31);
INSERT INTO iteminfo_data VALUES (433, 'price_logic_max_1980', '900', 31);
INSERT INTO iteminfo_data VALUES (434, 'price_fraud_1981', '250', 31);
INSERT INTO iteminfo_data VALUES (435, 'price_logic_min_1981', '400', 31);
INSERT INTO iteminfo_data VALUES (436, 'price_logic_max_1981', '1000', 31);
INSERT INTO iteminfo_data VALUES (437, 'price_fraud_1982', '100', 31);
INSERT INTO iteminfo_data VALUES (438, 'price_logic_min_1982', '160', 31);
INSERT INTO iteminfo_data VALUES (439, 'price_logic_max_1982', '400', 31);
INSERT INTO iteminfo_data VALUES (440, 'price_fraud_1983', '166.5', 31);
INSERT INTO iteminfo_data VALUES (441, 'price_logic_min_1983', '266.4', 31);
INSERT INTO iteminfo_data VALUES (442, 'price_logic_max_1983', '666', 31);
INSERT INTO iteminfo_data VALUES (443, 'price_fraud_1984', '184', 31);
INSERT INTO iteminfo_data VALUES (444, 'price_logic_min_1984', '294.4', 31);
INSERT INTO iteminfo_data VALUES (445, 'price_logic_max_1984', '736', 31);
INSERT INTO iteminfo_data VALUES (446, 'price_fraud_1985', '191.5', 31);
INSERT INTO iteminfo_data VALUES (447, 'price_logic_min_1985', '306.4', 31);
INSERT INTO iteminfo_data VALUES (448, 'price_logic_max_1985', '766', 31);
INSERT INTO iteminfo_data VALUES (449, 'price_fraud_1986', '316.5', 31);
INSERT INTO iteminfo_data VALUES (450, 'price_logic_min_1986', '506.4', 31);
INSERT INTO iteminfo_data VALUES (451, 'price_logic_max_1986', '1266', 31);
INSERT INTO iteminfo_data VALUES (452, 'price_fraud_1987', '285', 31);
INSERT INTO iteminfo_data VALUES (453, 'price_logic_min_1987', '456', 31);
INSERT INTO iteminfo_data VALUES (454, 'price_logic_max_1987', '1140', 31);
INSERT INTO iteminfo_data VALUES (455, 'price_fraud_1988', '323', 31);
INSERT INTO iteminfo_data VALUES (456, 'price_logic_min_1988', '516.8', 31);
INSERT INTO iteminfo_data VALUES (457, 'price_logic_max_1988', '1292', 31);
INSERT INTO iteminfo_data VALUES (458, 'price_fraud_1989', '321.5', 31);
INSERT INTO iteminfo_data VALUES (459, 'price_logic_min_1989', '514.4', 31);
INSERT INTO iteminfo_data VALUES (460, 'price_logic_max_1989', '1286', 31);
INSERT INTO iteminfo_data VALUES (461, 'price_fraud_1990', '352.5', 31);
INSERT INTO iteminfo_data VALUES (462, 'price_logic_min_1990', '564', 31);
INSERT INTO iteminfo_data VALUES (463, 'price_logic_max_1990', '1410', 31);
INSERT INTO iteminfo_data VALUES (464, 'price_fraud_1991', '395', 31);
INSERT INTO iteminfo_data VALUES (465, 'price_logic_min_1991', '632', 31);
INSERT INTO iteminfo_data VALUES (466, 'price_logic_max_1991', '1580', 31);
INSERT INTO iteminfo_data VALUES (467, 'price_fraud_1992', '475', 31);
INSERT INTO iteminfo_data VALUES (468, 'price_logic_min_1992', '760', 31);
INSERT INTO iteminfo_data VALUES (469, 'price_logic_max_1992', '1900', 31);
INSERT INTO iteminfo_data VALUES (470, 'price_fraud_1993', '487', 31);
INSERT INTO iteminfo_data VALUES (471, 'price_logic_min_1993', '779.2', 31);
INSERT INTO iteminfo_data VALUES (472, 'price_logic_max_1993', '1948', 31);
INSERT INTO iteminfo_data VALUES (473, 'price_fraud_1994', '549.5', 31);
INSERT INTO iteminfo_data VALUES (474, 'price_logic_min_1994', '879.2', 31);
INSERT INTO iteminfo_data VALUES (475, 'price_logic_max_1994', '2198', 31);
INSERT INTO iteminfo_data VALUES (476, 'price_fraud_1995', '651.5', 31);
INSERT INTO iteminfo_data VALUES (477, 'price_logic_min_1995', '1042.4', 31);
INSERT INTO iteminfo_data VALUES (478, 'price_logic_max_1995', '2606', 31);
INSERT INTO iteminfo_data VALUES (479, 'price_fraud_1996', '829.5', 31);
INSERT INTO iteminfo_data VALUES (480, 'price_logic_min_1996', '1327.2', 31);
INSERT INTO iteminfo_data VALUES (481, 'price_logic_max_1996', '3318', 31);
INSERT INTO iteminfo_data VALUES (482, 'price_fraud_1997', '1048.5', 31);
INSERT INTO iteminfo_data VALUES (483, 'price_logic_min_1997', '1677.6', 31);
INSERT INTO iteminfo_data VALUES (484, 'price_logic_max_1997', '4194', 31);
INSERT INTO iteminfo_data VALUES (485, 'price_fraud_1998', '1168', 31);
INSERT INTO iteminfo_data VALUES (486, 'price_logic_min_1998', '1868.8', 31);
INSERT INTO iteminfo_data VALUES (487, 'price_logic_max_1998', '4672', 31);
INSERT INTO iteminfo_data VALUES (488, 'price_fraud_1999', '1340', 31);
INSERT INTO iteminfo_data VALUES (489, 'price_logic_min_1999', '2144', 31);
INSERT INTO iteminfo_data VALUES (490, 'price_logic_max_1999', '5360', 31);
INSERT INTO iteminfo_data VALUES (491, 'price_fraud_2000', '1591', 31);
INSERT INTO iteminfo_data VALUES (492, 'price_logic_min_2000', '2545.6', 31);
INSERT INTO iteminfo_data VALUES (493, 'price_logic_max_2000', '6364', 31);
INSERT INTO iteminfo_data VALUES (494, 'price_fraud_2001', '1940.5', 31);
INSERT INTO iteminfo_data VALUES (495, 'price_logic_min_2001', '3104.8', 31);
INSERT INTO iteminfo_data VALUES (496, 'price_logic_max_2001', '7762', 31);
INSERT INTO iteminfo_data VALUES (497, 'price_fraud_2002', '2489', 31);
INSERT INTO iteminfo_data VALUES (498, 'price_logic_min_2002', '3982.4', 31);
INSERT INTO iteminfo_data VALUES (499, 'price_logic_max_2002', '9956', 31);
INSERT INTO iteminfo_data VALUES (500, 'price_fraud_2003', '3363', 31);
INSERT INTO iteminfo_data VALUES (501, 'price_logic_min_2003', '5380.8', 31);
INSERT INTO iteminfo_data VALUES (502, 'price_logic_max_2003', '13452', 31);
INSERT INTO iteminfo_data VALUES (503, 'price_fraud_2004', '3318.5', 31);
INSERT INTO iteminfo_data VALUES (504, 'price_logic_min_2004', '5309.6', 31);
INSERT INTO iteminfo_data VALUES (505, 'price_logic_max_2004', '13274', 31);
INSERT INTO iteminfo_data VALUES (506, 'price_fraud_2005', '3736', 31);
INSERT INTO iteminfo_data VALUES (507, 'price_logic_min_2005', '5977.6', 31);
INSERT INTO iteminfo_data VALUES (508, 'price_logic_max_2005', '14944', 31);
INSERT INTO iteminfo_data VALUES (509, 'price_fraud_2006', '4460', 31);
INSERT INTO iteminfo_data VALUES (510, 'price_logic_min_2006', '7136', 31);
INSERT INTO iteminfo_data VALUES (511, 'price_logic_max_2006', '17840', 31);
INSERT INTO iteminfo_data VALUES (512, 'price_fraud_2007', '5120.5', 31);
INSERT INTO iteminfo_data VALUES (513, 'price_logic_min_2007', '8192.8', 31);
INSERT INTO iteminfo_data VALUES (514, 'price_logic_max_2007', '20482', 31);
INSERT INTO iteminfo_data VALUES (515, 'price_fraud_2008', '5896.5', 31);
INSERT INTO iteminfo_data VALUES (516, 'price_logic_min_2008', '9434.4', 31);
INSERT INTO iteminfo_data VALUES (517, 'price_logic_max_2008', '23586', 31);
INSERT INTO iteminfo_data VALUES (518, 'price_fraud_1975', '4529.5', 27);
INSERT INTO iteminfo_data VALUES (519, 'price_logic_min_1975', '7247.2', 27);
INSERT INTO iteminfo_data VALUES (520, 'price_logic_max_1975', '18118', 27);
INSERT INTO iteminfo_data VALUES (521, 'price_fraud_1976', '1860', 27);
INSERT INTO iteminfo_data VALUES (522, 'price_logic_min_1976', '2976', 27);
INSERT INTO iteminfo_data VALUES (523, 'price_logic_max_1976', '7440', 27);
INSERT INTO iteminfo_data VALUES (524, 'price_fraud_1977', '1113.5', 27);
INSERT INTO iteminfo_data VALUES (525, 'price_logic_min_1977', '1781.6', 27);
INSERT INTO iteminfo_data VALUES (526, 'price_logic_max_1977', '4454', 27);
INSERT INTO iteminfo_data VALUES (527, 'price_fraud_1978', '699', 27);
INSERT INTO iteminfo_data VALUES (528, 'price_logic_min_1978', '1118.4', 27);
INSERT INTO iteminfo_data VALUES (529, 'price_logic_max_1978', '2796', 27);
INSERT INTO iteminfo_data VALUES (530, 'price_fraud_1979', '1120.5', 27);
INSERT INTO iteminfo_data VALUES (531, 'price_logic_min_1979', '1792.8', 27);
INSERT INTO iteminfo_data VALUES (532, 'price_logic_max_1979', '4482', 27);
INSERT INTO iteminfo_data VALUES (533, 'price_fraud_1980', '1116.5', 27);
INSERT INTO iteminfo_data VALUES (534, 'price_logic_min_1980', '1786.4', 27);
INSERT INTO iteminfo_data VALUES (535, 'price_logic_max_1980', '4466', 27);
INSERT INTO iteminfo_data VALUES (536, 'price_fraud_1981', '956.5', 27);
INSERT INTO iteminfo_data VALUES (537, 'price_logic_min_1981', '1530.4', 27);
INSERT INTO iteminfo_data VALUES (538, 'price_logic_max_1981', '3826', 27);
INSERT INTO iteminfo_data VALUES (539, 'price_fraud_1982', '561.5', 27);
INSERT INTO iteminfo_data VALUES (540, 'price_logic_min_1982', '898.4', 27);
INSERT INTO iteminfo_data VALUES (541, 'price_logic_max_1982', '2246', 27);
INSERT INTO iteminfo_data VALUES (542, 'price_fraud_1983', '526', 27);
INSERT INTO iteminfo_data VALUES (543, 'price_logic_min_1983', '841.6', 27);
INSERT INTO iteminfo_data VALUES (544, 'price_logic_max_1983', '2104', 27);
INSERT INTO iteminfo_data VALUES (545, 'price_fraud_1984', '483.5', 27);
INSERT INTO iteminfo_data VALUES (546, 'price_logic_min_1984', '773.6', 27);
INSERT INTO iteminfo_data VALUES (547, 'price_logic_max_1984', '1934', 27);
INSERT INTO iteminfo_data VALUES (548, 'price_fraud_1985', '354', 27);
INSERT INTO iteminfo_data VALUES (549, 'price_logic_min_1985', '566.4', 27);
INSERT INTO iteminfo_data VALUES (550, 'price_logic_max_1985', '1416', 27);
INSERT INTO iteminfo_data VALUES (551, 'price_fraud_1986', '473', 27);
INSERT INTO iteminfo_data VALUES (552, 'price_logic_min_1986', '756.8', 27);
INSERT INTO iteminfo_data VALUES (553, 'price_logic_max_1986', '1892', 27);
INSERT INTO iteminfo_data VALUES (554, 'price_fraud_1987', '402.5', 27);
INSERT INTO iteminfo_data VALUES (555, 'price_logic_min_1987', '644', 27);
INSERT INTO iteminfo_data VALUES (556, 'price_logic_max_1987', '1610', 27);
INSERT INTO iteminfo_data VALUES (557, 'price_fraud_1988', '430', 27);
INSERT INTO iteminfo_data VALUES (558, 'price_logic_min_1988', '688', 27);
INSERT INTO iteminfo_data VALUES (559, 'price_logic_max_1988', '1720', 27);
INSERT INTO iteminfo_data VALUES (560, 'price_fraud_1989', '410', 27);
INSERT INTO iteminfo_data VALUES (561, 'price_logic_min_1989', '656', 27);
INSERT INTO iteminfo_data VALUES (562, 'price_logic_max_1989', '1640', 27);
INSERT INTO iteminfo_data VALUES (563, 'price_fraud_1990', '454.5', 27);
INSERT INTO iteminfo_data VALUES (564, 'price_logic_min_1990', '727.2', 27);
INSERT INTO iteminfo_data VALUES (565, 'price_logic_max_1990', '1818', 27);
INSERT INTO iteminfo_data VALUES (566, 'price_fraud_1991', '442.5', 27);
INSERT INTO iteminfo_data VALUES (567, 'price_logic_min_1991', '708', 27);
INSERT INTO iteminfo_data VALUES (568, 'price_logic_max_1991', '1770', 27);
INSERT INTO iteminfo_data VALUES (569, 'price_fraud_1992', '541', 27);
INSERT INTO iteminfo_data VALUES (570, 'price_logic_min_1992', '865.6', 27);
INSERT INTO iteminfo_data VALUES (571, 'price_logic_max_1992', '2164', 27);
INSERT INTO iteminfo_data VALUES (572, 'price_fraud_1993', '645', 27);
INSERT INTO iteminfo_data VALUES (573, 'price_logic_min_1993', '1032', 27);
INSERT INTO iteminfo_data VALUES (574, 'price_logic_max_1993', '2580', 27);
INSERT INTO iteminfo_data VALUES (575, 'price_fraud_1994', '710.5', 27);
INSERT INTO iteminfo_data VALUES (576, 'price_logic_min_1994', '1136.8', 27);
INSERT INTO iteminfo_data VALUES (577, 'price_logic_max_1994', '2842', 27);
INSERT INTO iteminfo_data VALUES (578, 'price_fraud_1995', '869', 27);
INSERT INTO iteminfo_data VALUES (579, 'price_logic_min_1995', '1390.4', 27);
INSERT INTO iteminfo_data VALUES (580, 'price_logic_max_1995', '3476', 27);
INSERT INTO iteminfo_data VALUES (581, 'price_fraud_1996', '1012.5', 27);
INSERT INTO iteminfo_data VALUES (582, 'price_logic_min_1996', '1620', 27);
INSERT INTO iteminfo_data VALUES (583, 'price_logic_max_1996', '4050', 27);
INSERT INTO iteminfo_data VALUES (584, 'price_fraud_1997', '1226.5', 27);
INSERT INTO iteminfo_data VALUES (585, 'price_logic_min_1997', '1962.4', 27);
INSERT INTO iteminfo_data VALUES (586, 'price_logic_max_1997', '4906', 27);
INSERT INTO iteminfo_data VALUES (587, 'price_fraud_1998', '1405', 27);
INSERT INTO iteminfo_data VALUES (588, 'price_logic_min_1998', '2248', 27);
INSERT INTO iteminfo_data VALUES (589, 'price_logic_max_1998', '5620', 27);
INSERT INTO iteminfo_data VALUES (590, 'price_fraud_1999', '1682.5', 27);
INSERT INTO iteminfo_data VALUES (591, 'price_logic_min_1999', '2692', 27);
INSERT INTO iteminfo_data VALUES (592, 'price_logic_max_1999', '6730', 27);
INSERT INTO iteminfo_data VALUES (593, 'price_fraud_2000', '2113.5', 27);
INSERT INTO iteminfo_data VALUES (594, 'price_logic_min_2000', '3381.6', 27);
INSERT INTO iteminfo_data VALUES (595, 'price_logic_max_2000', '8454', 27);
INSERT INTO iteminfo_data VALUES (596, 'price_fraud_2001', '2815.5', 27);
INSERT INTO iteminfo_data VALUES (597, 'price_logic_min_2001', '4504.8', 27);
INSERT INTO iteminfo_data VALUES (598, 'price_logic_max_2001', '11262', 27);
INSERT INTO iteminfo_data VALUES (599, 'price_fraud_2002', '3258', 27);
INSERT INTO iteminfo_data VALUES (600, 'price_logic_min_2002', '5212.8', 27);
INSERT INTO iteminfo_data VALUES (601, 'price_logic_max_2002', '13032', 27);
INSERT INTO iteminfo_data VALUES (602, 'price_fraud_2003', '3888', 27);
INSERT INTO iteminfo_data VALUES (603, 'price_logic_min_2003', '6220.8', 27);
INSERT INTO iteminfo_data VALUES (604, 'price_logic_max_2003', '15552', 27);
INSERT INTO iteminfo_data VALUES (605, 'price_fraud_2004', '4701', 27);
INSERT INTO iteminfo_data VALUES (606, 'price_logic_min_2004', '7521.6', 27);
INSERT INTO iteminfo_data VALUES (607, 'price_logic_max_2004', '18804', 27);
INSERT INTO iteminfo_data VALUES (608, 'price_fraud_2005', '5431', 27);
INSERT INTO iteminfo_data VALUES (609, 'price_logic_min_2005', '8689.6', 27);
INSERT INTO iteminfo_data VALUES (610, 'price_logic_max_2005', '21724', 27);
INSERT INTO iteminfo_data VALUES (611, 'price_fraud_2006', '6420', 27);
INSERT INTO iteminfo_data VALUES (612, 'price_logic_min_2006', '10272', 27);
INSERT INTO iteminfo_data VALUES (613, 'price_logic_max_2006', '25680', 27);
INSERT INTO iteminfo_data VALUES (614, 'price_fraud_2007', '8082', 27);
INSERT INTO iteminfo_data VALUES (615, 'price_logic_min_2007', '12931.2', 27);
INSERT INTO iteminfo_data VALUES (616, 'price_logic_max_2007', '32328', 27);
INSERT INTO iteminfo_data VALUES (617, 'price_fraud_2008', '9657', 27);
INSERT INTO iteminfo_data VALUES (618, 'price_logic_min_2008', '15451.2', 27);
INSERT INTO iteminfo_data VALUES (619, 'price_logic_max_2008', '38628', 27);
INSERT INTO iteminfo_data VALUES (620, 'price_fraud_1998', '1700', 42);
INSERT INTO iteminfo_data VALUES (621, 'price_logic_min_1998', '2720', 42);
INSERT INTO iteminfo_data VALUES (622, 'price_logic_max_1998', '6800', 42);
INSERT INTO iteminfo_data VALUES (623, 'price_fraud_1999', '1550', 42);
INSERT INTO iteminfo_data VALUES (624, 'price_logic_min_1999', '2480', 42);
INSERT INTO iteminfo_data VALUES (625, 'price_logic_max_1999', '6200', 42);
INSERT INTO iteminfo_data VALUES (626, 'price_fraud_2001', '3800', 42);
INSERT INTO iteminfo_data VALUES (627, 'price_logic_min_2001', '6080', 42);
INSERT INTO iteminfo_data VALUES (628, 'price_logic_max_2001', '15200', 42);
INSERT INTO iteminfo_data VALUES (629, 'price_fraud_2002', '2972.5', 42);
INSERT INTO iteminfo_data VALUES (630, 'price_logic_min_2002', '4756', 42);
INSERT INTO iteminfo_data VALUES (631, 'price_logic_max_2002', '11890', 42);
INSERT INTO iteminfo_data VALUES (632, 'price_fraud_1996', '1250', 43);
INSERT INTO iteminfo_data VALUES (633, 'price_logic_min_1996', '2000', 43);
INSERT INTO iteminfo_data VALUES (634, 'price_logic_max_1996', '5000', 43);
INSERT INTO iteminfo_data VALUES (635, 'price_fraud_1998', '850', 43);
INSERT INTO iteminfo_data VALUES (636, 'price_logic_min_1998', '1360', 43);
INSERT INTO iteminfo_data VALUES (637, 'price_logic_max_1998', '3400', 43);
INSERT INTO iteminfo_data VALUES (638, 'price_fraud_1999', '1575', 43);
INSERT INTO iteminfo_data VALUES (639, 'price_logic_min_1999', '2520', 43);
INSERT INTO iteminfo_data VALUES (640, 'price_logic_max_1999', '6300', 43);
INSERT INTO iteminfo_data VALUES (641, 'price_fraud_2000', '1100', 43);
INSERT INTO iteminfo_data VALUES (642, 'price_logic_min_2000', '1760', 43);
INSERT INTO iteminfo_data VALUES (643, 'price_logic_max_2000', '4400', 43);
INSERT INTO iteminfo_data VALUES (644, 'price_fraud_1994', '831', 44);
INSERT INTO iteminfo_data VALUES (645, 'price_logic_min_1994', '1329.6', 44);
INSERT INTO iteminfo_data VALUES (646, 'price_logic_max_1994', '3324', 44);
INSERT INTO iteminfo_data VALUES (647, 'price_fraud_1995', '775', 44);
INSERT INTO iteminfo_data VALUES (648, 'price_logic_min_1995', '1240', 44);
INSERT INTO iteminfo_data VALUES (649, 'price_logic_max_1995', '3100', 44);
INSERT INTO iteminfo_data VALUES (650, 'price_fraud_1996', '1387.5', 44);
INSERT INTO iteminfo_data VALUES (651, 'price_logic_min_1996', '2220', 44);
INSERT INTO iteminfo_data VALUES (652, 'price_logic_max_1996', '5550', 44);
INSERT INTO iteminfo_data VALUES (653, 'price_fraud_1997', '1400', 44);
INSERT INTO iteminfo_data VALUES (654, 'price_logic_min_1997', '2240', 44);
INSERT INTO iteminfo_data VALUES (655, 'price_logic_max_1997', '5600', 44);
INSERT INTO iteminfo_data VALUES (656, 'price_fraud_1998', '1852.5', 45);
INSERT INTO iteminfo_data VALUES (657, 'price_logic_min_1998', '2964', 45);
INSERT INTO iteminfo_data VALUES (658, 'price_logic_max_1998', '7410', 45);
INSERT INTO iteminfo_data VALUES (659, 'price_fraud_1999', '1749.5', 45);
INSERT INTO iteminfo_data VALUES (660, 'price_logic_min_1999', '2799.2', 45);
INSERT INTO iteminfo_data VALUES (661, 'price_logic_max_1999', '6998', 45);
INSERT INTO iteminfo_data VALUES (662, 'price_fraud_2000', '2148.5', 45);
INSERT INTO iteminfo_data VALUES (663, 'price_logic_min_2000', '3437.6', 45);
INSERT INTO iteminfo_data VALUES (664, 'price_logic_max_2000', '8594', 45);
INSERT INTO iteminfo_data VALUES (665, 'price_fraud_2001', '2000', 45);
INSERT INTO iteminfo_data VALUES (666, 'price_logic_min_2001', '3200', 45);
INSERT INTO iteminfo_data VALUES (667, 'price_logic_max_2001', '8000', 45);
INSERT INTO iteminfo_data VALUES (668, 'price_fraud_1995', '1625', 46);
INSERT INTO iteminfo_data VALUES (669, 'price_logic_min_1995', '2600', 46);
INSERT INTO iteminfo_data VALUES (670, 'price_logic_max_1995', '6500', 46);
INSERT INTO iteminfo_data VALUES (671, 'price_fraud_1996', '1440', 46);
INSERT INTO iteminfo_data VALUES (672, 'price_logic_min_1996', '2304', 46);
INSERT INTO iteminfo_data VALUES (673, 'price_logic_max_1996', '5760', 46);
INSERT INTO iteminfo_data VALUES (674, 'price_fraud_1997', '1275', 46);
INSERT INTO iteminfo_data VALUES (675, 'price_logic_min_1997', '2040', 46);
INSERT INTO iteminfo_data VALUES (676, 'price_logic_max_1997', '5100', 46);
INSERT INTO iteminfo_data VALUES (677, 'price_fraud_1998', '1400', 46);
INSERT INTO iteminfo_data VALUES (678, 'price_logic_min_1998', '2240', 46);
INSERT INTO iteminfo_data VALUES (679, 'price_logic_max_1998', '5600', 46);
INSERT INTO iteminfo_data VALUES (680, 'price_fraud_2004', '4250', 46);
INSERT INTO iteminfo_data VALUES (681, 'price_logic_min_2004', '6800', 46);
INSERT INTO iteminfo_data VALUES (682, 'price_logic_max_2004', '17000', 46);
INSERT INTO iteminfo_data VALUES (683, 'price_fraud_1995', '1441.5', 47);
INSERT INTO iteminfo_data VALUES (684, 'price_logic_min_1995', '2306.4', 47);
INSERT INTO iteminfo_data VALUES (685, 'price_logic_max_1995', '5766', 47);
INSERT INTO iteminfo_data VALUES (686, 'price_fraud_1996', '1229', 47);
INSERT INTO iteminfo_data VALUES (687, 'price_logic_min_1996', '1966.4', 47);
INSERT INTO iteminfo_data VALUES (688, 'price_logic_max_1996', '4916', 47);
INSERT INTO iteminfo_data VALUES (689, 'price_fraud_1997', '1750', 47);
INSERT INTO iteminfo_data VALUES (690, 'price_logic_min_1997', '2800', 47);
INSERT INTO iteminfo_data VALUES (691, 'price_logic_max_1997', '7000', 47);
INSERT INTO iteminfo_data VALUES (692, 'price_fraud_1999', '2325', 47);
INSERT INTO iteminfo_data VALUES (693, 'price_logic_min_1999', '3720', 47);
INSERT INTO iteminfo_data VALUES (694, 'price_logic_max_1999', '9300', 47);
INSERT INTO iteminfo_data VALUES (695, 'price_fraud_2000', '1600', 47);
INSERT INTO iteminfo_data VALUES (696, 'price_logic_min_2000', '2560', 47);
INSERT INTO iteminfo_data VALUES (697, 'price_logic_max_2000', '6400', 47);
INSERT INTO iteminfo_data VALUES (698, 'price_fraud_2002', '3325', 48);
INSERT INTO iteminfo_data VALUES (699, 'price_logic_min_2002', '5320', 48);
INSERT INTO iteminfo_data VALUES (700, 'price_logic_max_2002', '13300', 48);
INSERT INTO iteminfo_data VALUES (701, 'price_fraud_2003', '4100', 48);
INSERT INTO iteminfo_data VALUES (702, 'price_logic_min_2003', '6560', 48);
INSERT INTO iteminfo_data VALUES (703, 'price_logic_max_2003', '16400', 48);
INSERT INTO iteminfo_data VALUES (704, 'price_fraud_2004', '3625', 48);
INSERT INTO iteminfo_data VALUES (705, 'price_logic_min_2004', '5800', 48);
INSERT INTO iteminfo_data VALUES (706, 'price_logic_max_2004', '14500', 48);
INSERT INTO iteminfo_data VALUES (707, 'price_fraud_2005', '4800', 48);
INSERT INTO iteminfo_data VALUES (708, 'price_logic_min_2005', '7680', 48);
INSERT INTO iteminfo_data VALUES (709, 'price_logic_max_2005', '19200', 48);
INSERT INTO iteminfo_data VALUES (710, 'price_fraud_2006', '5000', 48);
INSERT INTO iteminfo_data VALUES (711, 'price_logic_min_2006', '8000', 48);
INSERT INTO iteminfo_data VALUES (712, 'price_logic_max_2006', '20000', 48);
INSERT INTO iteminfo_data VALUES (713, 'price_fraud_2003', '5175', 49);
INSERT INTO iteminfo_data VALUES (714, 'price_logic_min_2003', '8280', 49);
INSERT INTO iteminfo_data VALUES (715, 'price_logic_max_2003', '20700', 49);
INSERT INTO iteminfo_data VALUES (716, 'price_fraud_2004', '5475', 49);
INSERT INTO iteminfo_data VALUES (717, 'price_logic_min_2004', '8760', 49);
INSERT INTO iteminfo_data VALUES (718, 'price_logic_max_2004', '21900', 49);
INSERT INTO iteminfo_data VALUES (719, 'price_fraud_2005', '6467.5', 49);
INSERT INTO iteminfo_data VALUES (720, 'price_logic_min_2005', '10348', 49);
INSERT INTO iteminfo_data VALUES (721, 'price_logic_max_2005', '25870', 49);
INSERT INTO iteminfo_data VALUES (722, 'price_fraud_2006', '8950', 49);
INSERT INTO iteminfo_data VALUES (723, 'price_logic_min_2006', '14320', 49);
INSERT INTO iteminfo_data VALUES (724, 'price_logic_max_2006', '35800', 49);
INSERT INTO iteminfo_data VALUES (725, 'price_fraud_2001', '3400', 50);
INSERT INTO iteminfo_data VALUES (726, 'price_logic_min_2001', '5440', 50);
INSERT INTO iteminfo_data VALUES (727, 'price_logic_max_2001', '13600', 50);
INSERT INTO iteminfo_data VALUES (728, 'price_fraud_2002', '3445', 50);
INSERT INTO iteminfo_data VALUES (729, 'price_logic_min_2002', '5512', 50);
INSERT INTO iteminfo_data VALUES (730, 'price_logic_max_2002', '13780', 50);
INSERT INTO iteminfo_data VALUES (731, 'price_fraud_2004', '4950', 50);
INSERT INTO iteminfo_data VALUES (732, 'price_logic_min_2004', '7920', 50);
INSERT INTO iteminfo_data VALUES (733, 'price_logic_max_2004', '19800', 50);
INSERT INTO iteminfo_data VALUES (734, 'price_fraud_2005', '4950', 50);
INSERT INTO iteminfo_data VALUES (735, 'price_logic_min_2005', '7920', 50);
INSERT INTO iteminfo_data VALUES (736, 'price_logic_max_2005', '19800', 50);
INSERT INTO iteminfo_data VALUES (737, 'price_fraud_1984', '1750', 32);
INSERT INTO iteminfo_data VALUES (738, 'price_logic_min_1984', '2800', 32);
INSERT INTO iteminfo_data VALUES (739, 'price_logic_max_1984', '7000', 32);
INSERT INTO iteminfo_data VALUES (740, 'price_fraud_1988', '1750', 32);
INSERT INTO iteminfo_data VALUES (741, 'price_logic_min_1988', '2800', 32);
INSERT INTO iteminfo_data VALUES (742, 'price_logic_max_1988', '7000', 32);
INSERT INTO iteminfo_data VALUES (743, 'price_fraud_1989', '1650', 32);
INSERT INTO iteminfo_data VALUES (744, 'price_logic_min_1989', '2640', 32);
INSERT INTO iteminfo_data VALUES (745, 'price_logic_max_1989', '6600', 32);
INSERT INTO iteminfo_data VALUES (746, 'price_fraud_1991', '2733', 32);
INSERT INTO iteminfo_data VALUES (747, 'price_logic_min_1991', '4372.8', 32);
INSERT INTO iteminfo_data VALUES (748, 'price_logic_max_1991', '10932', 32);
INSERT INTO iteminfo_data VALUES (749, 'price_fraud_1992', '1000', 32);
INSERT INTO iteminfo_data VALUES (750, 'price_logic_min_1992', '1600', 32);
INSERT INTO iteminfo_data VALUES (751, 'price_logic_max_1992', '4000', 32);
INSERT INTO iteminfo_data VALUES (752, 'price_fraud_1993', '1625', 32);
INSERT INTO iteminfo_data VALUES (753, 'price_logic_min_1993', '2600', 32);
INSERT INTO iteminfo_data VALUES (754, 'price_logic_max_1993', '6500', 32);
INSERT INTO iteminfo_data VALUES (755, 'price_fraud_1994', '828', 32);
INSERT INTO iteminfo_data VALUES (756, 'price_logic_min_1994', '1324.8', 32);
INSERT INTO iteminfo_data VALUES (757, 'price_logic_max_1994', '3312', 32);
INSERT INTO iteminfo_data VALUES (758, 'price_fraud_1995', '1057.5', 32);
INSERT INTO iteminfo_data VALUES (759, 'price_logic_min_1995', '1692', 32);
INSERT INTO iteminfo_data VALUES (760, 'price_logic_max_1995', '4230', 32);
INSERT INTO iteminfo_data VALUES (761, 'price_fraud_1996', '1136.5', 32);
INSERT INTO iteminfo_data VALUES (762, 'price_logic_min_1996', '1818.4', 32);
INSERT INTO iteminfo_data VALUES (763, 'price_logic_max_1996', '4546', 32);
INSERT INTO iteminfo_data VALUES (764, 'price_fraud_1997', '1295.5', 32);
INSERT INTO iteminfo_data VALUES (765, 'price_logic_min_1997', '2072.8', 32);
INSERT INTO iteminfo_data VALUES (766, 'price_logic_max_1997', '5182', 32);
INSERT INTO iteminfo_data VALUES (767, 'price_fraud_1998', '1616', 32);
INSERT INTO iteminfo_data VALUES (768, 'price_logic_min_1998', '2585.6', 32);
INSERT INTO iteminfo_data VALUES (769, 'price_logic_max_1998', '6464', 32);
INSERT INTO iteminfo_data VALUES (770, 'price_fraud_1999', '1857.5', 32);
INSERT INTO iteminfo_data VALUES (771, 'price_logic_min_1999', '2972', 32);
INSERT INTO iteminfo_data VALUES (772, 'price_logic_max_1999', '7430', 32);
INSERT INTO iteminfo_data VALUES (773, 'price_fraud_2000', '2170', 32);
INSERT INTO iteminfo_data VALUES (774, 'price_logic_min_2000', '3472', 32);
INSERT INTO iteminfo_data VALUES (775, 'price_logic_max_2000', '8680', 32);
INSERT INTO iteminfo_data VALUES (776, 'price_fraud_2001', '3146', 32);
INSERT INTO iteminfo_data VALUES (777, 'price_logic_min_2001', '5033.6', 32);
INSERT INTO iteminfo_data VALUES (778, 'price_logic_max_2001', '12584', 32);
INSERT INTO iteminfo_data VALUES (779, 'price_fraud_2002', '3565.5', 32);
INSERT INTO iteminfo_data VALUES (780, 'price_logic_min_2002', '5704.8', 32);
INSERT INTO iteminfo_data VALUES (781, 'price_logic_max_2002', '14262', 32);
INSERT INTO iteminfo_data VALUES (782, 'price_fraud_2003', '4029', 32);
INSERT INTO iteminfo_data VALUES (783, 'price_logic_min_2003', '6446.4', 32);
INSERT INTO iteminfo_data VALUES (784, 'price_logic_max_2003', '16116', 32);
INSERT INTO iteminfo_data VALUES (785, 'price_fraud_2004', '4702.5', 32);
INSERT INTO iteminfo_data VALUES (786, 'price_logic_min_2004', '7524', 32);
INSERT INTO iteminfo_data VALUES (787, 'price_logic_max_2004', '18810', 32);
INSERT INTO iteminfo_data VALUES (788, 'price_fraud_2005', '5904.5', 32);
INSERT INTO iteminfo_data VALUES (789, 'price_logic_min_2005', '9447.2', 32);
INSERT INTO iteminfo_data VALUES (790, 'price_logic_max_2005', '23618', 32);
INSERT INTO iteminfo_data VALUES (791, 'price_fraud_2006', '7192.5', 32);
INSERT INTO iteminfo_data VALUES (792, 'price_logic_min_2006', '11508', 32);
INSERT INTO iteminfo_data VALUES (793, 'price_logic_max_2006', '28770', 32);
INSERT INTO iteminfo_data VALUES (794, 'price_fraud_2007', '9243', 32);
INSERT INTO iteminfo_data VALUES (795, 'price_logic_min_2007', '14788.8', 32);
INSERT INTO iteminfo_data VALUES (796, 'price_logic_max_2007', '36972', 32);
INSERT INTO iteminfo_data VALUES (797, 'price_fraud_2008', '11569.5', 32);
INSERT INTO iteminfo_data VALUES (798, 'price_logic_min_2008', '18511.2', 32);
INSERT INTO iteminfo_data VALUES (799, 'price_logic_max_2008', '46278', 32);
INSERT INTO iteminfo_data VALUES (800, 'price_fraud_2000', '3866.5', 57);
INSERT INTO iteminfo_data VALUES (801, 'price_logic_min_2000', '6186.4', 57);
INSERT INTO iteminfo_data VALUES (802, 'price_logic_max_2000', '15466', 57);
INSERT INTO iteminfo_data VALUES (803, 'price_fraud_2001', '3982', 57);
INSERT INTO iteminfo_data VALUES (804, 'price_logic_min_2001', '6371.2', 57);
INSERT INTO iteminfo_data VALUES (805, 'price_logic_max_2001', '15928', 57);
INSERT INTO iteminfo_data VALUES (806, 'price_fraud_2002', '4284', 57);
INSERT INTO iteminfo_data VALUES (807, 'price_logic_min_2002', '6854.4', 57);
INSERT INTO iteminfo_data VALUES (808, 'price_logic_max_2002', '17136', 57);
INSERT INTO iteminfo_data VALUES (809, 'price_fraud_2003', '6075', 57);
INSERT INTO iteminfo_data VALUES (810, 'price_logic_min_2003', '9720', 57);
INSERT INTO iteminfo_data VALUES (811, 'price_logic_max_2003', '24300', 57);
INSERT INTO iteminfo_data VALUES (812, 'price_fraud_2007', '7950', 57);
INSERT INTO iteminfo_data VALUES (813, 'price_logic_min_2007', '12720', 57);
INSERT INTO iteminfo_data VALUES (814, 'price_logic_max_2007', '31800', 57);
INSERT INTO iteminfo_data VALUES (815, 'price_fraud_1997', '1533', 58);
INSERT INTO iteminfo_data VALUES (816, 'price_logic_min_1997', '2452.8', 58);
INSERT INTO iteminfo_data VALUES (817, 'price_logic_max_1997', '6132', 58);
INSERT INTO iteminfo_data VALUES (818, 'price_fraud_1998', '2033', 58);
INSERT INTO iteminfo_data VALUES (819, 'price_logic_min_1998', '3252.8', 58);
INSERT INTO iteminfo_data VALUES (820, 'price_logic_max_1998', '8132', 58);
INSERT INTO iteminfo_data VALUES (821, 'price_fraud_1999', '2248', 58);
INSERT INTO iteminfo_data VALUES (822, 'price_logic_min_1999', '3596.8', 58);
INSERT INTO iteminfo_data VALUES (823, 'price_logic_max_1999', '8992', 58);
INSERT INTO iteminfo_data VALUES (824, 'price_fraud_2000', '2313', 58);
INSERT INTO iteminfo_data VALUES (825, 'price_logic_min_2000', '3700.8', 58);
INSERT INTO iteminfo_data VALUES (826, 'price_logic_max_2000', '9252', 58);
INSERT INTO iteminfo_data VALUES (827, 'price_fraud_2001', '2300', 58);
INSERT INTO iteminfo_data VALUES (828, 'price_logic_min_2001', '3680', 58);
INSERT INTO iteminfo_data VALUES (829, 'price_logic_max_2001', '9200', 58);
INSERT INTO iteminfo_data VALUES (830, 'price_fraud_1997', '1816.5', 59);
INSERT INTO iteminfo_data VALUES (831, 'price_logic_min_1997', '2906.4', 59);
INSERT INTO iteminfo_data VALUES (832, 'price_logic_max_1997', '7266', 59);
INSERT INTO iteminfo_data VALUES (833, 'price_fraud_1998', '2100', 59);
INSERT INTO iteminfo_data VALUES (834, 'price_logic_min_1998', '3360', 59);
INSERT INTO iteminfo_data VALUES (835, 'price_logic_max_1998', '8400', 59);
INSERT INTO iteminfo_data VALUES (836, 'price_fraud_2001', '3450', 59);
INSERT INTO iteminfo_data VALUES (837, 'price_logic_min_2001', '5520', 59);
INSERT INTO iteminfo_data VALUES (838, 'price_logic_max_2001', '13800', 59);
INSERT INTO iteminfo_data VALUES (839, 'price_fraud_2007', '9125', 59);
INSERT INTO iteminfo_data VALUES (840, 'price_logic_min_2007', '14600', 59);
INSERT INTO iteminfo_data VALUES (841, 'price_logic_max_2007', '36500', 59);
INSERT INTO iteminfo_data VALUES (842, 'price_fraud_1988', '1500', 34);
INSERT INTO iteminfo_data VALUES (843, 'price_logic_min_1988', '2400', 34);
INSERT INTO iteminfo_data VALUES (844, 'price_logic_max_1988', '6000', 34);
INSERT INTO iteminfo_data VALUES (845, 'price_fraud_1994', '5000', 34);
INSERT INTO iteminfo_data VALUES (846, 'price_logic_min_1994', '8000', 34);
INSERT INTO iteminfo_data VALUES (847, 'price_logic_max_1994', '20000', 34);
INSERT INTO iteminfo_data VALUES (848, 'price_fraud_1995', '1600', 34);
INSERT INTO iteminfo_data VALUES (849, 'price_logic_min_1995', '2560', 34);
INSERT INTO iteminfo_data VALUES (850, 'price_logic_max_1995', '6400', 34);
INSERT INTO iteminfo_data VALUES (851, 'price_fraud_1996', '1641.5', 34);
INSERT INTO iteminfo_data VALUES (852, 'price_logic_min_1996', '2626.4', 34);
INSERT INTO iteminfo_data VALUES (853, 'price_logic_max_1996', '6566', 34);
INSERT INTO iteminfo_data VALUES (854, 'price_fraud_1997', '1526', 34);
INSERT INTO iteminfo_data VALUES (855, 'price_logic_min_1997', '2441.6', 34);
INSERT INTO iteminfo_data VALUES (856, 'price_logic_max_1997', '6104', 34);
INSERT INTO iteminfo_data VALUES (857, 'price_fraud_1998', '1791', 34);
INSERT INTO iteminfo_data VALUES (858, 'price_logic_min_1998', '2865.6', 34);
INSERT INTO iteminfo_data VALUES (859, 'price_logic_max_1998', '7164', 34);
INSERT INTO iteminfo_data VALUES (860, 'price_fraud_1999', '2001.5', 34);
INSERT INTO iteminfo_data VALUES (861, 'price_logic_min_1999', '3202.4', 34);
INSERT INTO iteminfo_data VALUES (862, 'price_logic_max_1999', '8006', 34);
INSERT INTO iteminfo_data VALUES (863, 'price_fraud_2000', '2689', 34);
INSERT INTO iteminfo_data VALUES (864, 'price_logic_min_2000', '4302.4', 34);
INSERT INTO iteminfo_data VALUES (865, 'price_logic_max_2000', '10756', 34);
INSERT INTO iteminfo_data VALUES (866, 'price_fraud_2001', '3090.5', 34);
INSERT INTO iteminfo_data VALUES (867, 'price_logic_min_2001', '4944.8', 34);
INSERT INTO iteminfo_data VALUES (868, 'price_logic_max_2001', '12362', 34);
INSERT INTO iteminfo_data VALUES (869, 'price_fraud_2002', '3618', 34);
INSERT INTO iteminfo_data VALUES (870, 'price_logic_min_2002', '5788.8', 34);
INSERT INTO iteminfo_data VALUES (871, 'price_logic_max_2002', '14472', 34);
INSERT INTO iteminfo_data VALUES (872, 'price_fraud_2003', '4496.5', 34);
INSERT INTO iteminfo_data VALUES (873, 'price_logic_min_2003', '7194.4', 34);
INSERT INTO iteminfo_data VALUES (874, 'price_logic_max_2003', '17986', 34);
INSERT INTO iteminfo_data VALUES (875, 'price_fraud_2004', '5294', 34);
INSERT INTO iteminfo_data VALUES (876, 'price_logic_min_2004', '8470.4', 34);
INSERT INTO iteminfo_data VALUES (877, 'price_logic_max_2004', '21176', 34);
INSERT INTO iteminfo_data VALUES (878, 'price_fraud_2005', '6196.5', 34);
INSERT INTO iteminfo_data VALUES (879, 'price_logic_min_2005', '9914.4', 34);
INSERT INTO iteminfo_data VALUES (880, 'price_logic_max_2005', '24786', 34);
INSERT INTO iteminfo_data VALUES (881, 'price_fraud_2006', '7274.5', 34);
INSERT INTO iteminfo_data VALUES (882, 'price_logic_min_2006', '11639.2', 34);
INSERT INTO iteminfo_data VALUES (883, 'price_logic_max_2006', '29098', 34);
INSERT INTO iteminfo_data VALUES (884, 'price_fraud_2007', '8958', 34);
INSERT INTO iteminfo_data VALUES (885, 'price_logic_min_2007', '14332.8', 34);
INSERT INTO iteminfo_data VALUES (886, 'price_logic_max_2007', '35832', 34);
INSERT INTO iteminfo_data VALUES (887, 'price_fraud_2008', '9717.5', 34);
INSERT INTO iteminfo_data VALUES (888, 'price_logic_min_2008', '15548', 34);
INSERT INTO iteminfo_data VALUES (889, 'price_logic_max_2008', '38870', 34);
INSERT INTO iteminfo_data VALUES (890, 'price_fraud_1975', '1040', 28);
INSERT INTO iteminfo_data VALUES (891, 'price_logic_min_1975', '1664', 28);
INSERT INTO iteminfo_data VALUES (892, 'price_logic_max_1975', '4160', 28);
INSERT INTO iteminfo_data VALUES (893, 'price_fraud_1976', '552', 28);
INSERT INTO iteminfo_data VALUES (894, 'price_logic_min_1976', '883.2', 28);
INSERT INTO iteminfo_data VALUES (895, 'price_logic_max_1976', '2208', 28);
INSERT INTO iteminfo_data VALUES (896, 'price_fraud_1977', '434.5', 28);
INSERT INTO iteminfo_data VALUES (897, 'price_logic_min_1977', '695.2', 28);
INSERT INTO iteminfo_data VALUES (898, 'price_logic_max_1977', '1738', 28);
INSERT INTO iteminfo_data VALUES (899, 'price_fraud_1978', '508', 28);
INSERT INTO iteminfo_data VALUES (900, 'price_logic_min_1978', '812.8', 28);
INSERT INTO iteminfo_data VALUES (901, 'price_logic_max_1978', '2032', 28);
INSERT INTO iteminfo_data VALUES (902, 'price_fraud_1979', '506.5', 28);
INSERT INTO iteminfo_data VALUES (903, 'price_logic_min_1979', '810.4', 28);
INSERT INTO iteminfo_data VALUES (904, 'price_logic_max_1979', '2026', 28);
INSERT INTO iteminfo_data VALUES (905, 'price_fraud_1980', '585.5', 28);
INSERT INTO iteminfo_data VALUES (906, 'price_logic_min_1980', '936.8', 28);
INSERT INTO iteminfo_data VALUES (907, 'price_logic_max_1980', '2342', 28);
INSERT INTO iteminfo_data VALUES (908, 'price_fraud_1981', '425', 28);
INSERT INTO iteminfo_data VALUES (909, 'price_logic_min_1981', '680', 28);
INSERT INTO iteminfo_data VALUES (910, 'price_logic_max_1981', '1700', 28);
INSERT INTO iteminfo_data VALUES (911, 'price_fraud_1982', '415', 28);
INSERT INTO iteminfo_data VALUES (912, 'price_logic_min_1982', '664', 28);
INSERT INTO iteminfo_data VALUES (913, 'price_logic_max_1982', '1660', 28);
INSERT INTO iteminfo_data VALUES (914, 'price_fraud_1983', '531.5', 28);
INSERT INTO iteminfo_data VALUES (915, 'price_logic_min_1983', '850.4', 28);
INSERT INTO iteminfo_data VALUES (916, 'price_logic_max_1983', '2126', 28);
INSERT INTO iteminfo_data VALUES (917, 'price_fraud_1984', '463', 28);
INSERT INTO iteminfo_data VALUES (918, 'price_logic_min_1984', '740.8', 28);
INSERT INTO iteminfo_data VALUES (919, 'price_logic_max_1984', '1852', 28);
INSERT INTO iteminfo_data VALUES (920, 'price_fraud_1985', '393.5', 28);
INSERT INTO iteminfo_data VALUES (921, 'price_logic_min_1985', '629.6', 28);
INSERT INTO iteminfo_data VALUES (922, 'price_logic_max_1985', '1574', 28);
INSERT INTO iteminfo_data VALUES (923, 'price_fraud_1986', '383', 28);
INSERT INTO iteminfo_data VALUES (924, 'price_logic_min_1986', '612.8', 28);
INSERT INTO iteminfo_data VALUES (925, 'price_logic_max_1986', '1532', 28);
INSERT INTO iteminfo_data VALUES (926, 'price_fraud_1987', '413', 28);
INSERT INTO iteminfo_data VALUES (927, 'price_logic_min_1987', '660.8', 28);
INSERT INTO iteminfo_data VALUES (928, 'price_logic_max_1987', '1652', 28);
INSERT INTO iteminfo_data VALUES (929, 'price_fraud_1988', '413.5', 28);
INSERT INTO iteminfo_data VALUES (930, 'price_logic_min_1988', '661.6', 28);
INSERT INTO iteminfo_data VALUES (931, 'price_logic_max_1988', '1654', 28);
INSERT INTO iteminfo_data VALUES (932, 'price_fraud_1989', '408.5', 28);
INSERT INTO iteminfo_data VALUES (933, 'price_logic_min_1989', '653.6', 28);
INSERT INTO iteminfo_data VALUES (934, 'price_logic_max_1989', '1634', 28);
INSERT INTO iteminfo_data VALUES (935, 'price_fraud_1990', '472', 28);
INSERT INTO iteminfo_data VALUES (936, 'price_logic_min_1990', '755.2', 28);
INSERT INTO iteminfo_data VALUES (937, 'price_logic_max_1990', '1888', 28);
INSERT INTO iteminfo_data VALUES (938, 'price_fraud_1991', '550.5', 28);
INSERT INTO iteminfo_data VALUES (939, 'price_logic_min_1991', '880.8', 28);
INSERT INTO iteminfo_data VALUES (940, 'price_logic_max_1991', '2202', 28);
INSERT INTO iteminfo_data VALUES (941, 'price_fraud_1992', '628.5', 28);
INSERT INTO iteminfo_data VALUES (942, 'price_logic_min_1992', '1005.6', 28);
INSERT INTO iteminfo_data VALUES (943, 'price_logic_max_1992', '2514', 28);
INSERT INTO iteminfo_data VALUES (944, 'price_fraud_1993', '751.5', 28);
INSERT INTO iteminfo_data VALUES (945, 'price_logic_min_1993', '1202.4', 28);
INSERT INTO iteminfo_data VALUES (946, 'price_logic_max_1993', '3006', 28);
INSERT INTO iteminfo_data VALUES (947, 'price_fraud_1994', '886', 28);
INSERT INTO iteminfo_data VALUES (948, 'price_logic_min_1994', '1417.6', 28);
INSERT INTO iteminfo_data VALUES (949, 'price_logic_max_1994', '3544', 28);
INSERT INTO iteminfo_data VALUES (950, 'price_fraud_1995', '1025', 28);
INSERT INTO iteminfo_data VALUES (951, 'price_logic_min_1995', '1640', 28);
INSERT INTO iteminfo_data VALUES (952, 'price_logic_max_1995', '4100', 28);
INSERT INTO iteminfo_data VALUES (953, 'price_fraud_1996', '1200.5', 28);
INSERT INTO iteminfo_data VALUES (954, 'price_logic_min_1996', '1920.8', 28);
INSERT INTO iteminfo_data VALUES (955, 'price_logic_max_1996', '4802', 28);
INSERT INTO iteminfo_data VALUES (956, 'price_fraud_1997', '1526.5', 28);
INSERT INTO iteminfo_data VALUES (957, 'price_logic_min_1997', '2442.4', 28);
INSERT INTO iteminfo_data VALUES (958, 'price_logic_max_1997', '6106', 28);
INSERT INTO iteminfo_data VALUES (959, 'price_fraud_1998', '1923.5', 28);
INSERT INTO iteminfo_data VALUES (960, 'price_logic_min_1998', '3077.6', 28);
INSERT INTO iteminfo_data VALUES (961, 'price_logic_max_1998', '7694', 28);
INSERT INTO iteminfo_data VALUES (962, 'price_fraud_1999', '2253.5', 28);
INSERT INTO iteminfo_data VALUES (963, 'price_logic_min_1999', '3605.6', 28);
INSERT INTO iteminfo_data VALUES (964, 'price_logic_max_1999', '9014', 28);
INSERT INTO iteminfo_data VALUES (965, 'price_fraud_2000', '2633.5', 28);
INSERT INTO iteminfo_data VALUES (966, 'price_logic_min_2000', '4213.6', 28);
INSERT INTO iteminfo_data VALUES (967, 'price_logic_max_2000', '10534', 28);
INSERT INTO iteminfo_data VALUES (968, 'price_fraud_2001', '3158', 28);
INSERT INTO iteminfo_data VALUES (969, 'price_logic_min_2001', '5052.8', 28);
INSERT INTO iteminfo_data VALUES (970, 'price_logic_max_2001', '12632', 28);
INSERT INTO iteminfo_data VALUES (971, 'price_fraud_2002', '3611', 28);
INSERT INTO iteminfo_data VALUES (972, 'price_logic_min_2002', '5777.6', 28);
INSERT INTO iteminfo_data VALUES (973, 'price_logic_max_2002', '14444', 28);
INSERT INTO iteminfo_data VALUES (974, 'price_fraud_2003', '4475', 28);
INSERT INTO iteminfo_data VALUES (975, 'price_logic_min_2003', '7160', 28);
INSERT INTO iteminfo_data VALUES (976, 'price_logic_max_2003', '17900', 28);
INSERT INTO iteminfo_data VALUES (977, 'price_fraud_2004', '5082.5', 28);
INSERT INTO iteminfo_data VALUES (978, 'price_logic_min_2004', '8132', 28);
INSERT INTO iteminfo_data VALUES (979, 'price_logic_max_2004', '20330', 28);
INSERT INTO iteminfo_data VALUES (980, 'price_fraud_2005', '5637', 28);
INSERT INTO iteminfo_data VALUES (981, 'price_logic_min_2005', '9019.2', 28);
INSERT INTO iteminfo_data VALUES (982, 'price_logic_max_2005', '22548', 28);
INSERT INTO iteminfo_data VALUES (983, 'price_fraud_2006', '6450', 28);
INSERT INTO iteminfo_data VALUES (984, 'price_logic_min_2006', '10320', 28);
INSERT INTO iteminfo_data VALUES (985, 'price_logic_max_2006', '25800', 28);
INSERT INTO iteminfo_data VALUES (986, 'price_fraud_2007', '7576', 28);
INSERT INTO iteminfo_data VALUES (987, 'price_logic_min_2007', '12121.6', 28);
INSERT INTO iteminfo_data VALUES (988, 'price_logic_max_2007', '30304', 28);
INSERT INTO iteminfo_data VALUES (989, 'price_fraud_2008', '8842.5', 28);
INSERT INTO iteminfo_data VALUES (990, 'price_logic_min_2008', '14148', 28);
INSERT INTO iteminfo_data VALUES (991, 'price_logic_max_2008', '35370', 28);
INSERT INTO iteminfo_data VALUES (992, 'price_fraud_1987', '750', 35);
INSERT INTO iteminfo_data VALUES (993, 'price_logic_min_1987', '1200', 35);
INSERT INTO iteminfo_data VALUES (994, 'price_logic_max_1987', '3000', 35);
INSERT INTO iteminfo_data VALUES (995, 'price_fraud_1989', '750', 35);
INSERT INTO iteminfo_data VALUES (996, 'price_logic_min_1989', '1200', 35);
INSERT INTO iteminfo_data VALUES (997, 'price_logic_max_1989', '3000', 35);
INSERT INTO iteminfo_data VALUES (998, 'price_fraud_1991', '350', 35);
INSERT INTO iteminfo_data VALUES (999, 'price_logic_min_1991', '560', 35);
INSERT INTO iteminfo_data VALUES (1000, 'price_logic_max_1991', '1400', 35);
INSERT INTO iteminfo_data VALUES (1001, 'price_fraud_1993', '775', 35);
INSERT INTO iteminfo_data VALUES (1002, 'price_logic_min_1993', '1240', 35);
INSERT INTO iteminfo_data VALUES (1003, 'price_logic_max_1993', '3100', 35);
INSERT INTO iteminfo_data VALUES (1004, 'price_fraud_1994', '856', 35);
INSERT INTO iteminfo_data VALUES (1005, 'price_logic_min_1994', '1369.6', 35);
INSERT INTO iteminfo_data VALUES (1006, 'price_logic_max_1994', '3424', 35);
INSERT INTO iteminfo_data VALUES (1007, 'price_fraud_1995', '699', 35);
INSERT INTO iteminfo_data VALUES (1008, 'price_logic_min_1995', '1118.4', 35);
INSERT INTO iteminfo_data VALUES (1009, 'price_logic_max_1995', '2796', 35);
INSERT INTO iteminfo_data VALUES (1010, 'price_fraud_1996', '825', 35);
INSERT INTO iteminfo_data VALUES (1011, 'price_logic_min_1996', '1320', 35);
INSERT INTO iteminfo_data VALUES (1012, 'price_logic_max_1996', '3300', 35);
INSERT INTO iteminfo_data VALUES (1013, 'price_fraud_1997', '1232.5', 35);
INSERT INTO iteminfo_data VALUES (1014, 'price_logic_min_1997', '1972', 35);
INSERT INTO iteminfo_data VALUES (1015, 'price_logic_max_1997', '4930', 35);
INSERT INTO iteminfo_data VALUES (1016, 'price_fraud_1998', '1326', 35);
INSERT INTO iteminfo_data VALUES (1017, 'price_logic_min_1998', '2121.6', 35);
INSERT INTO iteminfo_data VALUES (1018, 'price_logic_max_1998', '5304', 35);
INSERT INTO iteminfo_data VALUES (1019, 'price_fraud_1999', '1411', 35);
INSERT INTO iteminfo_data VALUES (1020, 'price_logic_min_1999', '2257.6', 35);
INSERT INTO iteminfo_data VALUES (1021, 'price_logic_max_1999', '5644', 35);
INSERT INTO iteminfo_data VALUES (1022, 'price_fraud_2000', '2046', 35);
INSERT INTO iteminfo_data VALUES (1023, 'price_logic_min_2000', '3273.6', 35);
INSERT INTO iteminfo_data VALUES (1024, 'price_logic_max_2000', '8184', 35);
INSERT INTO iteminfo_data VALUES (1025, 'price_fraud_2001', '2396', 35);
INSERT INTO iteminfo_data VALUES (1026, 'price_logic_min_2001', '3833.6', 35);
INSERT INTO iteminfo_data VALUES (1027, 'price_logic_max_2001', '9584', 35);
INSERT INTO iteminfo_data VALUES (1028, 'price_fraud_2002', '2957.5', 35);
INSERT INTO iteminfo_data VALUES (1029, 'price_logic_min_2002', '4732', 35);
INSERT INTO iteminfo_data VALUES (1030, 'price_logic_max_2002', '11830', 35);
INSERT INTO iteminfo_data VALUES (1031, 'price_fraud_2003', '3562', 35);
INSERT INTO iteminfo_data VALUES (1032, 'price_logic_min_2003', '5699.2', 35);
INSERT INTO iteminfo_data VALUES (1033, 'price_logic_max_2003', '14248', 35);
INSERT INTO iteminfo_data VALUES (1034, 'price_fraud_2004', '3935', 35);
INSERT INTO iteminfo_data VALUES (1035, 'price_logic_min_2004', '6296', 35);
INSERT INTO iteminfo_data VALUES (1036, 'price_logic_max_2004', '15740', 35);
INSERT INTO iteminfo_data VALUES (1037, 'price_fraud_2005', '4332', 35);
INSERT INTO iteminfo_data VALUES (1038, 'price_logic_min_2005', '6931.2', 35);
INSERT INTO iteminfo_data VALUES (1039, 'price_logic_max_2005', '17328', 35);
INSERT INTO iteminfo_data VALUES (1040, 'price_fraud_2006', '4738.5', 35);
INSERT INTO iteminfo_data VALUES (1041, 'price_logic_min_2006', '7581.6', 35);
INSERT INTO iteminfo_data VALUES (1042, 'price_logic_max_2006', '18954', 35);
INSERT INTO iteminfo_data VALUES (1043, 'price_fraud_2007', '5379', 35);
INSERT INTO iteminfo_data VALUES (1044, 'price_logic_min_2007', '8606.4', 35);
INSERT INTO iteminfo_data VALUES (1045, 'price_logic_max_2007', '21516', 35);
INSERT INTO iteminfo_data VALUES (1046, 'price_fraud_2008', '5580.5', 35);
INSERT INTO iteminfo_data VALUES (1047, 'price_logic_min_2008', '8928.8', 35);
INSERT INTO iteminfo_data VALUES (1048, 'price_logic_max_2008', '22322', 35);
INSERT INTO iteminfo_data VALUES (1049, 'price_fraud_2002', '3400', 51);
INSERT INTO iteminfo_data VALUES (1050, 'price_logic_min_2002', '5440', 51);
INSERT INTO iteminfo_data VALUES (1051, 'price_logic_max_2002', '13600', 51);
INSERT INTO iteminfo_data VALUES (1052, 'price_fraud_2003', '3200', 51);
INSERT INTO iteminfo_data VALUES (1053, 'price_logic_min_2003', '5120', 51);
INSERT INTO iteminfo_data VALUES (1054, 'price_logic_max_2003', '12800', 51);
INSERT INTO iteminfo_data VALUES (1055, 'price_fraud_2004', '3250', 51);
INSERT INTO iteminfo_data VALUES (1056, 'price_logic_min_2004', '5200', 51);
INSERT INTO iteminfo_data VALUES (1057, 'price_logic_max_2004', '13000', 51);
INSERT INTO iteminfo_data VALUES (1058, 'price_fraud_2005', '3350', 51);
INSERT INTO iteminfo_data VALUES (1059, 'price_logic_min_2005', '5360', 51);
INSERT INTO iteminfo_data VALUES (1060, 'price_logic_max_2005', '13400', 51);
INSERT INTO iteminfo_data VALUES (1061, 'price_fraud_1984', '1500', 36);
INSERT INTO iteminfo_data VALUES (1062, 'price_logic_min_1984', '2400', 36);
INSERT INTO iteminfo_data VALUES (1063, 'price_logic_max_1984', '6000', 36);
INSERT INTO iteminfo_data VALUES (1064, 'price_fraud_1985', '300', 36);
INSERT INTO iteminfo_data VALUES (1065, 'price_logic_min_1985', '480', 36);
INSERT INTO iteminfo_data VALUES (1066, 'price_logic_max_1985', '1200', 36);
INSERT INTO iteminfo_data VALUES (1067, 'price_fraud_1986', '250', 36);
INSERT INTO iteminfo_data VALUES (1068, 'price_logic_min_1986', '400', 36);
INSERT INTO iteminfo_data VALUES (1069, 'price_logic_max_1986', '1000', 36);
INSERT INTO iteminfo_data VALUES (1070, 'price_fraud_1987', '418.5', 36);
INSERT INTO iteminfo_data VALUES (1071, 'price_logic_min_1987', '669.6', 36);
INSERT INTO iteminfo_data VALUES (1072, 'price_logic_max_1987', '1674', 36);
INSERT INTO iteminfo_data VALUES (1073, 'price_fraud_1988', '246.5', 36);
INSERT INTO iteminfo_data VALUES (1074, 'price_logic_min_1988', '394.4', 36);
INSERT INTO iteminfo_data VALUES (1075, 'price_logic_max_1988', '986', 36);
INSERT INTO iteminfo_data VALUES (1076, 'price_fraud_1989', '217.5', 36);
INSERT INTO iteminfo_data VALUES (1077, 'price_logic_min_1989', '348', 36);
INSERT INTO iteminfo_data VALUES (1078, 'price_logic_max_1989', '870', 36);
INSERT INTO iteminfo_data VALUES (1079, 'price_fraud_1990', '385', 36);
INSERT INTO iteminfo_data VALUES (1080, 'price_logic_min_1990', '616', 36);
INSERT INTO iteminfo_data VALUES (1081, 'price_logic_max_1990', '1540', 36);
INSERT INTO iteminfo_data VALUES (1082, 'price_fraud_1991', '365.5', 36);
INSERT INTO iteminfo_data VALUES (1083, 'price_logic_min_1991', '584.8', 36);
INSERT INTO iteminfo_data VALUES (1084, 'price_logic_max_1991', '1462', 36);
INSERT INTO iteminfo_data VALUES (1085, 'price_fraud_1992', '418.5', 36);
INSERT INTO iteminfo_data VALUES (1086, 'price_logic_min_1992', '669.6', 36);
INSERT INTO iteminfo_data VALUES (1087, 'price_logic_max_1992', '1674', 36);
INSERT INTO iteminfo_data VALUES (1088, 'price_fraud_1993', '419', 36);
INSERT INTO iteminfo_data VALUES (1089, 'price_logic_min_1993', '670.4', 36);
INSERT INTO iteminfo_data VALUES (1090, 'price_logic_max_1993', '1676', 36);
INSERT INTO iteminfo_data VALUES (1091, 'price_fraud_1994', '684.5', 36);
INSERT INTO iteminfo_data VALUES (1092, 'price_logic_min_1994', '1095.2', 36);
INSERT INTO iteminfo_data VALUES (1093, 'price_logic_max_1994', '2738', 36);
INSERT INTO iteminfo_data VALUES (1094, 'price_fraud_1995', '826.5', 36);
INSERT INTO iteminfo_data VALUES (1095, 'price_logic_min_1995', '1322.4', 36);
INSERT INTO iteminfo_data VALUES (1096, 'price_logic_max_1995', '3306', 36);
INSERT INTO iteminfo_data VALUES (1097, 'price_fraud_1996', '813.5', 36);
INSERT INTO iteminfo_data VALUES (1098, 'price_logic_min_1996', '1301.6', 36);
INSERT INTO iteminfo_data VALUES (1099, 'price_logic_max_1996', '3254', 36);
INSERT INTO iteminfo_data VALUES (1100, 'price_fraud_1997', '1050.5', 36);
INSERT INTO iteminfo_data VALUES (1101, 'price_logic_min_1997', '1680.8', 36);
INSERT INTO iteminfo_data VALUES (1102, 'price_logic_max_1997', '4202', 36);
INSERT INTO iteminfo_data VALUES (1103, 'price_fraud_1998', '1403', 36);
INSERT INTO iteminfo_data VALUES (1104, 'price_logic_min_1998', '2244.8', 36);
INSERT INTO iteminfo_data VALUES (1105, 'price_logic_max_1998', '5612', 36);
INSERT INTO iteminfo_data VALUES (1106, 'price_fraud_1999', '1567', 36);
INSERT INTO iteminfo_data VALUES (1107, 'price_logic_min_1999', '2507.2', 36);
INSERT INTO iteminfo_data VALUES (1108, 'price_logic_max_1999', '6268', 36);
INSERT INTO iteminfo_data VALUES (1109, 'price_fraud_2000', '2024.5', 36);
INSERT INTO iteminfo_data VALUES (1110, 'price_logic_min_2000', '3239.2', 36);
INSERT INTO iteminfo_data VALUES (1111, 'price_logic_max_2000', '8098', 36);
INSERT INTO iteminfo_data VALUES (1112, 'price_fraud_2001', '2379', 36);
INSERT INTO iteminfo_data VALUES (1113, 'price_logic_min_2001', '3806.4', 36);
INSERT INTO iteminfo_data VALUES (1114, 'price_logic_max_2001', '9516', 36);
INSERT INTO iteminfo_data VALUES (1115, 'price_fraud_2002', '3110.5', 36);
INSERT INTO iteminfo_data VALUES (1116, 'price_logic_min_2002', '4976.8', 36);
INSERT INTO iteminfo_data VALUES (1117, 'price_logic_max_2002', '12442', 36);
INSERT INTO iteminfo_data VALUES (1118, 'price_fraud_2003', '3624', 36);
INSERT INTO iteminfo_data VALUES (1119, 'price_logic_min_2003', '5798.4', 36);
INSERT INTO iteminfo_data VALUES (1120, 'price_logic_max_2003', '14496', 36);
INSERT INTO iteminfo_data VALUES (1121, 'price_fraud_2004', '3941', 36);
INSERT INTO iteminfo_data VALUES (1122, 'price_logic_min_2004', '6305.6', 36);
INSERT INTO iteminfo_data VALUES (1123, 'price_logic_max_2004', '15764', 36);
INSERT INTO iteminfo_data VALUES (1124, 'price_fraud_2005', '4407', 36);
INSERT INTO iteminfo_data VALUES (1125, 'price_logic_min_2005', '7051.2', 36);
INSERT INTO iteminfo_data VALUES (1126, 'price_logic_max_2005', '17628', 36);
INSERT INTO iteminfo_data VALUES (1127, 'price_fraud_2006', '5228', 36);
INSERT INTO iteminfo_data VALUES (1128, 'price_logic_min_2006', '8364.8', 36);
INSERT INTO iteminfo_data VALUES (1129, 'price_logic_max_2006', '20912', 36);
INSERT INTO iteminfo_data VALUES (1130, 'price_fraud_2007', '6039', 36);
INSERT INTO iteminfo_data VALUES (1131, 'price_logic_min_2007', '9662.4', 36);
INSERT INTO iteminfo_data VALUES (1132, 'price_logic_max_2007', '24156', 36);
INSERT INTO iteminfo_data VALUES (1133, 'price_fraud_2008', '7001', 36);
INSERT INTO iteminfo_data VALUES (1134, 'price_logic_min_2008', '11201.6', 36);
INSERT INTO iteminfo_data VALUES (1135, 'price_logic_max_2008', '28004', 36);
INSERT INTO iteminfo_data VALUES (1136, 'price_fraud_2005', '5950', 52);
INSERT INTO iteminfo_data VALUES (1137, 'price_logic_min_2005', '9520', 52);
INSERT INTO iteminfo_data VALUES (1138, 'price_logic_max_2005', '23800', 52);
INSERT INTO iteminfo_data VALUES (1139, 'price_fraud_2006', '6500', 52);
INSERT INTO iteminfo_data VALUES (1140, 'price_logic_min_2006', '10400', 52);
INSERT INTO iteminfo_data VALUES (1141, 'price_logic_max_2006', '26000', 52);
INSERT INTO iteminfo_data VALUES (1142, 'price_fraud_2007', '7203', 52);
INSERT INTO iteminfo_data VALUES (1143, 'price_logic_min_2007', '11524.8', 52);
INSERT INTO iteminfo_data VALUES (1144, 'price_logic_max_2007', '28812', 52);
INSERT INTO iteminfo_data VALUES (1145, 'price_fraud_2008', '9750', 52);
INSERT INTO iteminfo_data VALUES (1146, 'price_logic_min_2008', '15600', 52);
INSERT INTO iteminfo_data VALUES (1147, 'price_logic_max_2008', '39000', 52);
INSERT INTO iteminfo_data VALUES (1148, 'price_fraud_2005', '6987.5', 53);
INSERT INTO iteminfo_data VALUES (1149, 'price_logic_min_2005', '11180', 53);
INSERT INTO iteminfo_data VALUES (1150, 'price_logic_max_2005', '27950', 53);
INSERT INTO iteminfo_data VALUES (1151, 'price_fraud_2006', '7982.5', 53);
INSERT INTO iteminfo_data VALUES (1152, 'price_logic_min_2006', '12772', 53);
INSERT INTO iteminfo_data VALUES (1153, 'price_logic_max_2006', '31930', 53);
INSERT INTO iteminfo_data VALUES (1154, 'price_fraud_2007', '8383.5', 53);
INSERT INTO iteminfo_data VALUES (1155, 'price_logic_min_2007', '13413.6', 53);
INSERT INTO iteminfo_data VALUES (1156, 'price_logic_max_2007', '33534', 53);
INSERT INTO iteminfo_data VALUES (1157, 'price_fraud_2008', '10250', 53);
INSERT INTO iteminfo_data VALUES (1158, 'price_logic_min_2008', '16400', 53);
INSERT INTO iteminfo_data VALUES (1159, 'price_logic_max_2008', '41000', 53);
INSERT INTO iteminfo_data VALUES (1160, 'price_fraud_2000', '2980', 54);
INSERT INTO iteminfo_data VALUES (1161, 'price_logic_min_2000', '4768', 54);
INSERT INTO iteminfo_data VALUES (1162, 'price_logic_max_2000', '11920', 54);
INSERT INTO iteminfo_data VALUES (1163, 'price_fraud_2001', '3241.5', 54);
INSERT INTO iteminfo_data VALUES (1164, 'price_logic_min_2001', '5186.4', 54);
INSERT INTO iteminfo_data VALUES (1165, 'price_logic_max_2001', '12966', 54);
INSERT INTO iteminfo_data VALUES (1166, 'price_fraud_2002', '3950', 54);
INSERT INTO iteminfo_data VALUES (1167, 'price_logic_min_2002', '6320', 54);
INSERT INTO iteminfo_data VALUES (1168, 'price_logic_max_2002', '15800', 54);
INSERT INTO iteminfo_data VALUES (1169, 'price_fraud_2003', '4100', 54);
INSERT INTO iteminfo_data VALUES (1170, 'price_logic_min_2003', '6560', 54);
INSERT INTO iteminfo_data VALUES (1171, 'price_logic_max_2003', '16400', 54);
INSERT INTO iteminfo_data VALUES (1172, 'price_fraud_2004', '5033', 54);
INSERT INTO iteminfo_data VALUES (1173, 'price_logic_min_2004', '8052.8', 54);
INSERT INTO iteminfo_data VALUES (1174, 'price_logic_max_2004', '20132', 54);
INSERT INTO iteminfo_data VALUES (1175, 'price_fraud_2002', '5250', 55);
INSERT INTO iteminfo_data VALUES (1176, 'price_logic_min_2002', '8400', 55);
INSERT INTO iteminfo_data VALUES (1177, 'price_logic_max_2002', '21000', 55);
INSERT INTO iteminfo_data VALUES (1178, 'price_fraud_2003', '5750', 55);
INSERT INTO iteminfo_data VALUES (1179, 'price_logic_min_2003', '9200', 55);
INSERT INTO iteminfo_data VALUES (1180, 'price_logic_max_2003', '23000', 55);
INSERT INTO iteminfo_data VALUES (1181, 'price_fraud_2004', '7014', 55);
INSERT INTO iteminfo_data VALUES (1182, 'price_logic_min_2004', '11222.4', 55);
INSERT INTO iteminfo_data VALUES (1183, 'price_logic_max_2004', '28056', 55);
INSERT INTO iteminfo_data VALUES (1184, 'price_fraud_2005', '7406', 55);
INSERT INTO iteminfo_data VALUES (1185, 'price_logic_min_2005', '11849.6', 55);
INSERT INTO iteminfo_data VALUES (1186, 'price_logic_max_2005', '29624', 55);
INSERT INTO iteminfo_data VALUES (1187, 'price_fraud_1999', '2250', 37);
INSERT INTO iteminfo_data VALUES (1188, 'price_logic_min_1999', '3600', 37);
INSERT INTO iteminfo_data VALUES (1189, 'price_logic_max_1999', '9000', 37);
INSERT INTO iteminfo_data VALUES (1190, 'price_fraud_2000', '3055.5', 37);
INSERT INTO iteminfo_data VALUES (1191, 'price_logic_min_2000', '4888.8', 37);
INSERT INTO iteminfo_data VALUES (1192, 'price_logic_max_2000', '12222', 37);
INSERT INTO iteminfo_data VALUES (1193, 'price_fraud_2001', '3446', 37);
INSERT INTO iteminfo_data VALUES (1194, 'price_logic_min_2001', '5513.6', 37);
INSERT INTO iteminfo_data VALUES (1195, 'price_logic_max_2001', '13784', 37);
INSERT INTO iteminfo_data VALUES (1196, 'price_fraud_2002', '4447.5', 37);
INSERT INTO iteminfo_data VALUES (1197, 'price_logic_min_2002', '7116', 37);
INSERT INTO iteminfo_data VALUES (1198, 'price_logic_max_2002', '17790', 37);
INSERT INTO iteminfo_data VALUES (1199, 'price_fraud_2003', '5235.5', 37);
INSERT INTO iteminfo_data VALUES (1200, 'price_logic_min_2003', '8376.8', 37);
INSERT INTO iteminfo_data VALUES (1201, 'price_logic_max_2003', '20942', 37);
INSERT INTO iteminfo_data VALUES (1202, 'price_fraud_2004', '5725.5', 37);
INSERT INTO iteminfo_data VALUES (1203, 'price_logic_min_2004', '9160.8', 37);
INSERT INTO iteminfo_data VALUES (1204, 'price_logic_max_2004', '22902', 37);
INSERT INTO iteminfo_data VALUES (1205, 'price_fraud_2005', '6721', 37);
INSERT INTO iteminfo_data VALUES (1206, 'price_logic_min_2005', '10753.6', 37);
INSERT INTO iteminfo_data VALUES (1207, 'price_logic_max_2005', '26884', 37);
INSERT INTO iteminfo_data VALUES (1208, 'price_fraud_2006', '7476.5', 37);
INSERT INTO iteminfo_data VALUES (1209, 'price_logic_min_2006', '11962.4', 37);
INSERT INTO iteminfo_data VALUES (1210, 'price_logic_max_2006', '29906', 37);
INSERT INTO iteminfo_data VALUES (1211, 'price_fraud_2007', '8677', 37);
INSERT INTO iteminfo_data VALUES (1212, 'price_logic_min_2007', '13883.2', 37);
INSERT INTO iteminfo_data VALUES (1213, 'price_logic_max_2007', '34708', 37);
INSERT INTO iteminfo_data VALUES (1214, 'price_fraud_2008', '10337.5', 37);
INSERT INTO iteminfo_data VALUES (1215, 'price_logic_min_2008', '16540', 37);
INSERT INTO iteminfo_data VALUES (1216, 'price_logic_max_2008', '41350', 37);
INSERT INTO iteminfo_data VALUES (1217, 'price_fraud_1983', '300', 29);
INSERT INTO iteminfo_data VALUES (1218, 'price_logic_min_1983', '480', 29);
INSERT INTO iteminfo_data VALUES (1219, 'price_logic_max_1983', '1200', 29);
INSERT INTO iteminfo_data VALUES (1220, 'price_fraud_1984', '1500', 29);
INSERT INTO iteminfo_data VALUES (1221, 'price_logic_min_1984', '2400', 29);
INSERT INTO iteminfo_data VALUES (1222, 'price_logic_max_1984', '6000', 29);
INSERT INTO iteminfo_data VALUES (1223, 'price_fraud_1985', '300', 29);
INSERT INTO iteminfo_data VALUES (1224, 'price_logic_min_1985', '480', 29);
INSERT INTO iteminfo_data VALUES (1225, 'price_logic_max_1985', '1200', 29);
INSERT INTO iteminfo_data VALUES (1226, 'price_fraud_1986', '290', 29);
INSERT INTO iteminfo_data VALUES (1227, 'price_logic_min_1986', '464', 29);
INSERT INTO iteminfo_data VALUES (1228, 'price_logic_max_1986', '1160', 29);
INSERT INTO iteminfo_data VALUES (1229, 'price_fraud_1987', '458', 29);
INSERT INTO iteminfo_data VALUES (1230, 'price_logic_min_1987', '732.8', 29);
INSERT INTO iteminfo_data VALUES (1231, 'price_logic_max_1987', '1832', 29);
INSERT INTO iteminfo_data VALUES (1232, 'price_fraud_1988', '301.5', 29);
INSERT INTO iteminfo_data VALUES (1233, 'price_logic_min_1988', '482.4', 29);
INSERT INTO iteminfo_data VALUES (1234, 'price_logic_max_1988', '1206', 29);
INSERT INTO iteminfo_data VALUES (1235, 'price_fraud_1989', '281', 29);
INSERT INTO iteminfo_data VALUES (1236, 'price_logic_min_1989', '449.6', 29);
INSERT INTO iteminfo_data VALUES (1237, 'price_logic_max_1989', '1124', 29);
INSERT INTO iteminfo_data VALUES (1238, 'price_fraud_1990', '345', 29);
INSERT INTO iteminfo_data VALUES (1239, 'price_logic_min_1990', '552', 29);
INSERT INTO iteminfo_data VALUES (1240, 'price_logic_max_1990', '1380', 29);
INSERT INTO iteminfo_data VALUES (1241, 'price_fraud_1991', '339', 29);
INSERT INTO iteminfo_data VALUES (1242, 'price_logic_min_1991', '542.4', 29);
INSERT INTO iteminfo_data VALUES (1243, 'price_logic_max_1991', '1356', 29);
INSERT INTO iteminfo_data VALUES (1244, 'price_fraud_1992', '478', 29);
INSERT INTO iteminfo_data VALUES (1245, 'price_logic_min_1992', '764.8', 29);
INSERT INTO iteminfo_data VALUES (1246, 'price_logic_max_1992', '1912', 29);
INSERT INTO iteminfo_data VALUES (1247, 'price_fraud_1993', '470', 29);
INSERT INTO iteminfo_data VALUES (1248, 'price_logic_min_1993', '752', 29);
INSERT INTO iteminfo_data VALUES (1249, 'price_logic_max_1993', '1880', 29);
INSERT INTO iteminfo_data VALUES (1250, 'price_fraud_1994', '692.5', 29);
INSERT INTO iteminfo_data VALUES (1251, 'price_logic_min_1994', '1108', 29);
INSERT INTO iteminfo_data VALUES (1252, 'price_logic_max_1994', '2770', 29);
INSERT INTO iteminfo_data VALUES (1253, 'price_fraud_1995', '758.5', 29);
INSERT INTO iteminfo_data VALUES (1254, 'price_logic_min_1995', '1213.6', 29);
INSERT INTO iteminfo_data VALUES (1255, 'price_logic_max_1995', '3034', 29);
INSERT INTO iteminfo_data VALUES (1256, 'price_fraud_1996', '837', 29);
INSERT INTO iteminfo_data VALUES (1257, 'price_logic_min_1996', '1339.2', 29);
INSERT INTO iteminfo_data VALUES (1258, 'price_logic_max_1996', '3348', 29);
INSERT INTO iteminfo_data VALUES (1259, 'price_fraud_1997', '1160', 29);
INSERT INTO iteminfo_data VALUES (1260, 'price_logic_min_1997', '1856', 29);
INSERT INTO iteminfo_data VALUES (1261, 'price_logic_max_1997', '4640', 29);
INSERT INTO iteminfo_data VALUES (1262, 'price_fraud_1998', '1414.5', 29);
INSERT INTO iteminfo_data VALUES (1263, 'price_logic_min_1998', '2263.2', 29);
INSERT INTO iteminfo_data VALUES (1264, 'price_logic_max_1998', '5658', 29);
INSERT INTO iteminfo_data VALUES (1265, 'price_fraud_1999', '1672.5', 29);
INSERT INTO iteminfo_data VALUES (1266, 'price_logic_min_1999', '2676', 29);
INSERT INTO iteminfo_data VALUES (1267, 'price_logic_max_1999', '6690', 29);
INSERT INTO iteminfo_data VALUES (1268, 'price_fraud_2000', '2244.5', 29);
INSERT INTO iteminfo_data VALUES (1269, 'price_logic_min_2000', '3591.2', 29);
INSERT INTO iteminfo_data VALUES (1270, 'price_logic_max_2000', '8978', 29);
INSERT INTO iteminfo_data VALUES (1271, 'price_fraud_2001', '2835', 29);
INSERT INTO iteminfo_data VALUES (1272, 'price_logic_min_2001', '4536', 29);
INSERT INTO iteminfo_data VALUES (1273, 'price_logic_max_2001', '11340', 29);
INSERT INTO iteminfo_data VALUES (1274, 'price_fraud_2002', '3760', 29);
INSERT INTO iteminfo_data VALUES (1275, 'price_logic_min_2002', '6016', 29);
INSERT INTO iteminfo_data VALUES (1276, 'price_logic_max_2002', '15040', 29);
INSERT INTO iteminfo_data VALUES (1277, 'price_fraud_2003', '4309.5', 29);
INSERT INTO iteminfo_data VALUES (1278, 'price_logic_min_2003', '6895.2', 29);
INSERT INTO iteminfo_data VALUES (1279, 'price_logic_max_2003', '17238', 29);
INSERT INTO iteminfo_data VALUES (1280, 'price_fraud_2004', '4872.5', 29);
INSERT INTO iteminfo_data VALUES (1281, 'price_logic_min_2004', '7796', 29);
INSERT INTO iteminfo_data VALUES (1282, 'price_logic_max_2004', '19490', 29);
INSERT INTO iteminfo_data VALUES (1283, 'price_fraud_2005', '5702.5', 29);
INSERT INTO iteminfo_data VALUES (1284, 'price_logic_min_2005', '9124', 29);
INSERT INTO iteminfo_data VALUES (1285, 'price_logic_max_2005', '22810', 29);
INSERT INTO iteminfo_data VALUES (1286, 'price_fraud_2006', '6530', 29);
INSERT INTO iteminfo_data VALUES (1287, 'price_logic_min_2006', '10448', 29);
INSERT INTO iteminfo_data VALUES (1288, 'price_logic_max_2006', '26120', 29);
INSERT INTO iteminfo_data VALUES (1289, 'price_fraud_2007', '7412.5', 29);
INSERT INTO iteminfo_data VALUES (1290, 'price_logic_min_2007', '11860', 29);
INSERT INTO iteminfo_data VALUES (1291, 'price_logic_max_2007', '29650', 29);
INSERT INTO iteminfo_data VALUES (1292, 'price_fraud_2008', '8648', 29);
INSERT INTO iteminfo_data VALUES (1293, 'price_logic_min_2008', '13836.8', 29);
INSERT INTO iteminfo_data VALUES (1294, 'price_logic_max_2008', '34592', 29);
INSERT INTO iteminfo_data VALUES (10001, 'name', 'rhone alpes', 2001);
INSERT INTO iteminfo_data VALUES (10002, 'name', 'ain', 2002);
INSERT INTO iteminfo_data VALUES (10003, 'name', 'ile de france', 2006);
INSERT INTO iteminfo_data VALUES (10004, 'name', 'paris', 2007);
INSERT INTO iteminfo_data VALUES (10005, 'is_coast', '0', 2001);
INSERT INTO iteminfo_data VALUES (10006, 'is_coast', '0', 2006);
INSERT INTO iteminfo_data VALUES (10007, 'is_mountain', '0', 2001);
INSERT INTO iteminfo_data VALUES (10008, 'is_mountain', '1', 2006);
INSERT INTO iteminfo_data VALUES (10009, 'price_fraud_cat_1080_s', '738.23986261018', 2001);
INSERT INTO iteminfo_data VALUES (10010, 'price_logic_min_cat_1080_s', '1181.1837801763', 2001);
INSERT INTO iteminfo_data VALUES (10011, 'price_logic_max_cat_1080_s', '2952.9594504407', 2001);
INSERT INTO iteminfo_data VALUES (10012, 'price_fraud_cat_1080_s', '739.23986261018', 2006);
INSERT INTO iteminfo_data VALUES (10013, 'price_logic_min_cat_1080_s', '1182.1837801763', 2006);
INSERT INTO iteminfo_data VALUES (10014, 'price_logic_max_cat_1080_s', '2953.9594504407', 2006);
INSERT INTO iteminfo_data VALUES (10015, 'price_fraud_cat_1020_s', '1154.3440838249', 2001);
INSERT INTO iteminfo_data VALUES (10016, 'price_logic_min_cat_1020_s', '1846.9505341198', 2001);
INSERT INTO iteminfo_data VALUES (10017, 'price_logic_max_cat_1020_s', '4617.3763352994', 2001);
INSERT INTO iteminfo_data VALUES (10018, 'price_fraud_cat_1020_s', '1155.3440838249', 2006);
INSERT INTO iteminfo_data VALUES (10019, 'price_logic_min_cat_1020_s', '1847.9505341198', 2006);
INSERT INTO iteminfo_data VALUES (10020, 'price_logic_max_cat_1020_s', '4618.3763352994', 2006);
INSERT INTO iteminfo_data VALUES (10021, 'price_fraud_cat_10_s', '3', 2001);
INSERT INTO iteminfo_data VALUES (10022, 'price_logic_min_cat_10_s', '4.8', 2001);
INSERT INTO iteminfo_data VALUES (10023, 'price_logic_max_cat_10_s', '12', 2001);
INSERT INTO iteminfo_data VALUES (10024, 'price_fraud_cat_10_s', '4', 2006);
INSERT INTO iteminfo_data VALUES (10025, 'price_logic_min_cat_10_s', '5.8', 2006);
INSERT INTO iteminfo_data VALUES (10026, 'price_logic_max_cat_10_s', '13', 2006);
INSERT INTO iteminfo_data VALUES (10027, 'price_fraud_cat_11_s', '1.5', 2001);
INSERT INTO iteminfo_data VALUES (10028, 'price_logic_min_cat_11_s', '2.4', 2001);
INSERT INTO iteminfo_data VALUES (10029, 'price_logic_max_cat_11_s', '6', 2001);
INSERT INTO iteminfo_data VALUES (10030, 'price_fraud_cat_11_s', '2.5', 2006);
INSERT INTO iteminfo_data VALUES (10031, 'price_logic_min_cat_11_s', '3.4', 2006);
INSERT INTO iteminfo_data VALUES (10032, 'price_logic_max_cat_11_s', '7', 2006);
INSERT INTO iteminfo_data VALUES (10033, 'price_fraud_cat_12_s', '1.8', 2001);
INSERT INTO iteminfo_data VALUES (10034, 'price_logic_min_cat_12_s', '2.88', 2001);
INSERT INTO iteminfo_data VALUES (10035, 'price_logic_max_cat_12_s', '7.2', 2001);
INSERT INTO iteminfo_data VALUES (10036, 'price_fraud_cat_12_s', '2.8', 2006);
INSERT INTO iteminfo_data VALUES (10037, 'price_logic_min_cat_12_s', '3.88', 2006);
INSERT INTO iteminfo_data VALUES (10038, 'price_logic_max_cat_12_s', '8.2', 2006);


--
-- Data for Name: mail_log; Type: TABLE DATA; Schema: public; Owner: joaquin
--



--
-- Data for Name: mail_queue; Type: TABLE DATA; Schema: public; Owner: joaquin
--



--
-- Data for Name: mama_attribute_wordlists; Type: TABLE DATA; Schema: public; Owner: joaquin
--



--
-- Data for Name: mama_attribute_categories; Type: TABLE DATA; Schema: public; Owner: joaquin
--



--
-- Data for Name: mama_main_backup; Type: TABLE DATA; Schema: public; Owner: joaquin
--



--
-- Data for Name: mama_attribute_categories_backup; Type: TABLE DATA; Schema: public; Owner: joaquin
--



--
-- Data for Name: mama_attribute_wordlists_backup; Type: TABLE DATA; Schema: public; Owner: joaquin
--



--
-- Data for Name: mama_attribute_words; Type: TABLE DATA; Schema: public; Owner: joaquin
--



--
-- Data for Name: mama_attribute_words_backup; Type: TABLE DATA; Schema: public; Owner: joaquin
--



--
-- Data for Name: mama_exception_lists; Type: TABLE DATA; Schema: public; Owner: joaquin
--



--
-- Data for Name: mama_exception_lists_backup; Type: TABLE DATA; Schema: public; Owner: joaquin
--



--
-- Data for Name: mama_exception_words; Type: TABLE DATA; Schema: public; Owner: joaquin
--



--
-- Data for Name: mama_exception_words_backup; Type: TABLE DATA; Schema: public; Owner: joaquin
--



--
-- Data for Name: mama_wordlists; Type: TABLE DATA; Schema: public; Owner: joaquin
--



--
-- Data for Name: mama_wordlists_backup; Type: TABLE DATA; Schema: public; Owner: joaquin
--



--
-- Data for Name: mama_words; Type: TABLE DATA; Schema: public; Owner: joaquin
--



--
-- Data for Name: mama_words_backup; Type: TABLE DATA; Schema: public; Owner: joaquin
--



--
-- Data for Name: most_popular_ads; Type: TABLE DATA; Schema: public; Owner: joaquin
--



--
-- Data for Name: notices; Type: TABLE DATA; Schema: public; Owner: joaquin
--



--
-- Data for Name: on_call; Type: TABLE DATA; Schema: public; Owner: joaquin
--



--
-- Data for Name: on_call_actions; Type: TABLE DATA; Schema: public; Owner: joaquin
--



--
-- Data for Name: pageviews_per_reg_cat; Type: TABLE DATA; Schema: public; Owner: joaquin
--



--
-- Data for Name: pay_log; Type: TABLE DATA; Schema: public; Owner: joaquin
--

INSERT INTO pay_log VALUES (0, 'adminclear', 0, '59876', 99999917, '2006-04-06 15:19:46', 50, 'OK');
INSERT INTO pay_log VALUES (1, 'save', 0, '112200000', NULL, '2014-03-25 12:45:07', 60, 'SAVE');
INSERT INTO pay_log VALUES (2, 'verify', 0, '112200000', NULL, '2014-03-25 12:45:08', 60, 'OK');
INSERT INTO pay_log VALUES (3, 'save', 0, '112200001', NULL, '2014-03-25 12:47:07', 61, 'SAVE');
INSERT INTO pay_log VALUES (4, 'verify', 0, '112200001', NULL, '2014-03-25 12:47:07', 61, 'OK');
INSERT INTO pay_log VALUES (5, 'save', 0, '112200002', NULL, '2014-03-25 12:49:15', 62, 'SAVE');
INSERT INTO pay_log VALUES (6, 'verify', 0, '112200002', NULL, '2014-03-25 12:49:15', 62, 'OK');
INSERT INTO pay_log VALUES (7, 'save', 0, '112200003', NULL, '2014-03-25 12:50:37', 63, 'SAVE');
INSERT INTO pay_log VALUES (8, 'verify', 0, '112200003', NULL, '2014-03-25 12:50:37', 63, 'OK');
INSERT INTO pay_log VALUES (9, 'save', 0, '112200004', NULL, '2014-03-25 12:51:24', 64, 'SAVE');
INSERT INTO pay_log VALUES (10, 'verify', 0, '112200004', NULL, '2014-03-25 12:51:24', 64, 'OK');
INSERT INTO pay_log VALUES (11, 'save', 0, '112200005', NULL, '2014-03-25 12:53:47', 65, 'SAVE');
INSERT INTO pay_log VALUES (12, 'verify', 0, '112200005', NULL, '2014-03-25 12:53:47', 65, 'OK');
INSERT INTO pay_log VALUES (13, 'save', 0, '112200006', NULL, '2014-03-25 12:54:25', 66, 'SAVE');
INSERT INTO pay_log VALUES (14, 'verify', 0, '112200006', NULL, '2014-03-25 12:54:25', 66, 'OK');
INSERT INTO pay_log VALUES (15, 'save', 0, '112200007', NULL, '2014-03-25 12:55:13', 67, 'SAVE');
INSERT INTO pay_log VALUES (16, 'verify', 0, '112200007', NULL, '2014-03-25 12:55:13', 67, 'OK');
INSERT INTO pay_log VALUES (17, 'save', 0, '112200008', NULL, '2014-03-25 12:55:57', 68, 'SAVE');
INSERT INTO pay_log VALUES (18, 'verify', 0, '112200008', NULL, '2014-03-25 12:55:57', 68, 'OK');
INSERT INTO pay_log VALUES (19, 'save', 0, '112200009', NULL, '2014-03-25 12:58:22', 69, 'SAVE');
INSERT INTO pay_log VALUES (20, 'verify', 0, '112200009', NULL, '2014-03-25 12:58:22', 69, 'OK');
INSERT INTO pay_log VALUES (21, 'save', 0, '235392023', NULL, '2014-03-25 12:59:07', 70, 'SAVE');
INSERT INTO pay_log VALUES (22, 'verify', 0, '235392023', NULL, '2014-03-25 12:59:07', 70, 'OK');
INSERT INTO pay_log VALUES (23, 'save', 0, '370784046', NULL, '2014-03-25 12:59:41', 71, 'SAVE');
INSERT INTO pay_log VALUES (24, 'verify', 0, '370784046', NULL, '2014-03-25 12:59:41', 71, 'OK');
INSERT INTO pay_log VALUES (25, 'save', 0, '506176069', NULL, '2014-03-25 14:20:14', 72, 'SAVE');
INSERT INTO pay_log VALUES (26, 'verify', 0, '506176069', NULL, '2014-03-25 14:20:14', 72, 'OK');
INSERT INTO pay_log VALUES (27, 'save', 0, '641568092', NULL, '2014-03-25 14:20:53', 73, 'SAVE');
INSERT INTO pay_log VALUES (28, 'verify', 0, '641568092', NULL, '2014-03-25 14:20:53', 73, 'OK');
INSERT INTO pay_log VALUES (29, 'save', 0, '776960115', NULL, '2014-03-25 14:21:41', 74, 'SAVE');
INSERT INTO pay_log VALUES (30, 'verify', 0, '776960115', NULL, '2014-03-25 14:21:41', 74, 'OK');
INSERT INTO pay_log VALUES (31, 'save', 0, '912352138', NULL, '2014-03-26 09:48:21', 75, 'SAVE');
INSERT INTO pay_log VALUES (32, 'verify', 0, '912352138', NULL, '2014-03-26 09:48:21', 75, 'OK');
INSERT INTO pay_log VALUES (33, 'save', 0, '147744161', NULL, '2014-03-26 09:49:02', 76, 'SAVE');
INSERT INTO pay_log VALUES (34, 'verify', 0, '147744161', NULL, '2014-03-26 09:49:02', 76, 'OK');


--
-- Data for Name: pay_log_references; Type: TABLE DATA; Schema: public; Owner: joaquin
--

INSERT INTO pay_log_references VALUES (0, 0, 'remote_addr', '192.168.4.75');
INSERT INTO pay_log_references VALUES (1, 0, 'adphone', '962184762');
INSERT INTO pay_log_references VALUES (1, 1, 'remote_addr', '10.0.1.188');
INSERT INTO pay_log_references VALUES (2, 0, 'remote_addr', '10.0.1.188');
INSERT INTO pay_log_references VALUES (3, 0, 'adphone', '962184762');
INSERT INTO pay_log_references VALUES (3, 1, 'remote_addr', '10.0.1.188');
INSERT INTO pay_log_references VALUES (4, 0, 'remote_addr', '10.0.1.188');
INSERT INTO pay_log_references VALUES (5, 0, 'adphone', '962184762');
INSERT INTO pay_log_references VALUES (5, 1, 'remote_addr', '10.0.1.188');
INSERT INTO pay_log_references VALUES (6, 0, 'remote_addr', '10.0.1.188');
INSERT INTO pay_log_references VALUES (7, 0, 'adphone', '962184762');
INSERT INTO pay_log_references VALUES (7, 1, 'remote_addr', '10.0.1.188');
INSERT INTO pay_log_references VALUES (8, 0, 'remote_addr', '10.0.1.188');
INSERT INTO pay_log_references VALUES (9, 0, 'adphone', '962184762');
INSERT INTO pay_log_references VALUES (9, 1, 'remote_addr', '10.0.1.188');
INSERT INTO pay_log_references VALUES (10, 0, 'remote_addr', '10.0.1.188');
INSERT INTO pay_log_references VALUES (11, 0, 'adphone', '962184762');
INSERT INTO pay_log_references VALUES (11, 1, 'remote_addr', '10.0.1.188');
INSERT INTO pay_log_references VALUES (12, 0, 'remote_addr', '10.0.1.188');
INSERT INTO pay_log_references VALUES (13, 0, 'adphone', '962184762');
INSERT INTO pay_log_references VALUES (13, 1, 'remote_addr', '10.0.1.188');
INSERT INTO pay_log_references VALUES (14, 0, 'remote_addr', '10.0.1.188');
INSERT INTO pay_log_references VALUES (15, 0, 'adphone', '962184762');
INSERT INTO pay_log_references VALUES (15, 1, 'remote_addr', '10.0.1.188');
INSERT INTO pay_log_references VALUES (16, 0, 'remote_addr', '10.0.1.188');
INSERT INTO pay_log_references VALUES (17, 0, 'adphone', '962184762');
INSERT INTO pay_log_references VALUES (17, 1, 'remote_addr', '10.0.1.188');
INSERT INTO pay_log_references VALUES (18, 0, 'remote_addr', '10.0.1.188');
INSERT INTO pay_log_references VALUES (19, 0, 'adphone', '962184762');
INSERT INTO pay_log_references VALUES (19, 1, 'remote_addr', '10.0.1.188');
INSERT INTO pay_log_references VALUES (20, 0, 'remote_addr', '10.0.1.188');
INSERT INTO pay_log_references VALUES (21, 0, 'adphone', '962184762');
INSERT INTO pay_log_references VALUES (21, 1, 'remote_addr', '10.0.1.188');
INSERT INTO pay_log_references VALUES (22, 0, 'remote_addr', '10.0.1.188');
INSERT INTO pay_log_references VALUES (23, 0, 'adphone', '962184762');
INSERT INTO pay_log_references VALUES (23, 1, 'remote_addr', '10.0.1.188');
INSERT INTO pay_log_references VALUES (24, 0, 'remote_addr', '10.0.1.188');
INSERT INTO pay_log_references VALUES (25, 0, 'adphone', '962184762');
INSERT INTO pay_log_references VALUES (25, 1, 'remote_addr', '10.0.1.188');
INSERT INTO pay_log_references VALUES (26, 0, 'remote_addr', '10.0.1.188');
INSERT INTO pay_log_references VALUES (27, 0, 'adphone', '962184762');
INSERT INTO pay_log_references VALUES (27, 1, 'remote_addr', '10.0.1.188');
INSERT INTO pay_log_references VALUES (28, 0, 'remote_addr', '10.0.1.188');
INSERT INTO pay_log_references VALUES (29, 0, 'adphone', '962184762');
INSERT INTO pay_log_references VALUES (29, 1, 'remote_addr', '10.0.1.188');
INSERT INTO pay_log_references VALUES (30, 0, 'remote_addr', '10.0.1.188');
INSERT INTO pay_log_references VALUES (31, 0, 'adphone', '962184762');
INSERT INTO pay_log_references VALUES (31, 1, 'remote_addr', '10.0.1.188');
INSERT INTO pay_log_references VALUES (32, 0, 'remote_addr', '10.0.1.188');
INSERT INTO pay_log_references VALUES (33, 0, 'adphone', '962184762');
INSERT INTO pay_log_references VALUES (33, 1, 'remote_addr', '10.0.1.188');
INSERT INTO pay_log_references VALUES (34, 0, 'remote_addr', '10.0.1.188');


--
-- Data for Name: payments; Type: TABLE DATA; Schema: public; Owner: joaquin
--

INSERT INTO payments VALUES (0, 'ad_action', 20, 0);
INSERT INTO payments VALUES (1, 'ad_action', 20, 0);
INSERT INTO payments VALUES (2, 'ad_action', 20, 0);
INSERT INTO payments VALUES (3, 'ad_action', 20, 0);
INSERT INTO payments VALUES (50, 'ad_action', 20, 0);
INSERT INTO payments VALUES (60, 'ad_action', 0, 0);
INSERT INTO payments VALUES (61, 'ad_action', 0, 0);
INSERT INTO payments VALUES (62, 'ad_action', 0, 0);
INSERT INTO payments VALUES (63, 'ad_action', 0, 0);
INSERT INTO payments VALUES (64, 'ad_action', 0, 0);
INSERT INTO payments VALUES (65, 'ad_action', 0, 0);
INSERT INTO payments VALUES (66, 'ad_action', 0, 0);
INSERT INTO payments VALUES (67, 'ad_action', 0, 0);
INSERT INTO payments VALUES (68, 'ad_action', 0, 0);
INSERT INTO payments VALUES (69, 'ad_action', 0, 0);
INSERT INTO payments VALUES (70, 'ad_action', 0, 0);
INSERT INTO payments VALUES (71, 'ad_action', 0, 0);
INSERT INTO payments VALUES (72, 'ad_action', 0, 0);
INSERT INTO payments VALUES (73, 'ad_action', 0, 0);
INSERT INTO payments VALUES (74, 'ad_action', 0, 0);
INSERT INTO payments VALUES (75, 'ad_action', 0, 0);
INSERT INTO payments VALUES (76, 'ad_action', 0, 0);

--
-- Data for Name: pricelist; Type: TABLE DATA; Schema: public; Owner: joaquin
--

-- INSERT INTO pricelist VALUES ('if', 'BL1', 1295);
-- INSERT INTO pricelist VALUES ('if', 'BL2', 1295);
-- INSERT INTO pricelist VALUES ('if', 'BL3', 995);
-- INSERT INTO pricelist VALUES ('if', 'BL4', 995);


--
-- Data for Name: purchase; Type: TABLE DATA; Schema: public; Owner: joaquin
--



--
-- Data for Name: purchase_detail; Type: TABLE DATA; Schema: public; Owner: joaquin
--



--
-- Data for Name: purchase_states; Type: TABLE DATA; Schema: public; Owner: joaquin
--



--
-- Data for Name: redir_stats; Type: TABLE DATA; Schema: public; Owner: joaquin
--



--
-- Data for Name: review_log; Type: TABLE DATA; Schema: public; Owner: joaquin
--

INSERT INTO review_log VALUES (20, 1, 9, '2014-03-25 14:22:22.424495', 'normal', 'new', 'accepted', NULL, 1020);
INSERT INTO review_log VALUES (21, 1, 9, '2014-03-25 14:22:25.746218', 'normal', 'new', 'accepted', NULL, 1020);
INSERT INTO review_log VALUES (22, 1, 9, '2014-03-25 14:22:30.561154', 'normal', 'new', 'accepted', NULL, 1020);
INSERT INTO review_log VALUES (23, 1, 9, '2014-03-25 14:22:32.972419', 'normal', 'new', 'accepted', NULL, 1020);
INSERT INTO review_log VALUES (24, 1, 9, '2014-03-25 14:22:38.839343', 'normal', 'new', 'accepted', NULL, 1020);
INSERT INTO review_log VALUES (25, 1, 9, '2014-03-25 14:22:41.15456', 'normal', 'new', 'accepted', NULL, 1040);
INSERT INTO review_log VALUES (26, 1, 9, '2014-03-25 14:22:46.028443', 'normal', 'new', 'accepted', NULL, 1040);
INSERT INTO review_log VALUES (27, 1, 9, '2014-03-25 14:22:48.491856', 'normal', 'new', 'accepted', NULL, 1040);
INSERT INTO review_log VALUES (28, 1, 9, '2014-03-25 14:22:53.141107', 'normal', 'new', 'accepted', NULL, 1040);
INSERT INTO review_log VALUES (29, 1, 9, '2014-03-25 14:22:55.807858', 'normal', 'new', 'accepted', NULL, 1100);
INSERT INTO review_log VALUES (30, 1, 9, '2014-03-25 14:23:00.806579', 'normal', 'new', 'accepted', NULL, 1100);
INSERT INTO review_log VALUES (31, 1, 9, '2014-03-25 14:23:03.294265', 'normal', 'new', 'accepted', NULL, 1100);
INSERT INTO review_log VALUES (32, 1, 9, '2014-03-25 14:23:10.740153', 'normal', 'new', 'accepted', NULL, 7020);
INSERT INTO review_log VALUES (33, 1, 9, '2014-03-25 14:23:12.928341', 'normal', 'new', 'accepted', NULL, 7020);
INSERT INTO review_log VALUES (34, 1, 9, '2014-03-25 14:23:18.307154', 'normal', 'new', 'accepted', NULL, 6140);
INSERT INTO review_log VALUES (35, 1, 9, '2014-03-26 09:49:29.791808', 'normal', 'new', 'accepted', NULL, 5020);
INSERT INTO review_log VALUES (36, 1, 9, '2014-03-26 09:49:33.725943', 'normal', 'new', 'accepted', NULL, 1100);


--
-- Data for Name: sms_users; Type: TABLE DATA; Schema: public; Owner: joaquin
--



--
-- Data for Name: sms_log; Type: TABLE DATA; Schema: public; Owner: joaquin
--



--
-- Data for Name: watch_users; Type: TABLE DATA; Schema: public; Owner: joaquin
--



--
-- Data for Name: watch_queries; Type: TABLE DATA; Schema: public; Owner: joaquin
--



--
-- Data for Name: sms_log_watch; Type: TABLE DATA; Schema: public; Owner: joaquin
--



--
-- Data for Name: state_params; Type: TABLE DATA; Schema: public; Owner: joaquin
--

INSERT INTO state_params VALUES (20, 1, 247, 'filter_name', 'normal');
INSERT INTO state_params VALUES (21, 1, 248, 'filter_name', 'normal');
INSERT INTO state_params VALUES (22, 1, 251, 'filter_name', 'normal');
INSERT INTO state_params VALUES (23, 1, 252, 'filter_name', 'normal');
INSERT INTO state_params VALUES (24, 1, 255, 'filter_name', 'normal');
INSERT INTO state_params VALUES (25, 1, 256, 'filter_name', 'normal');
INSERT INTO state_params VALUES (26, 1, 259, 'filter_name', 'normal');
INSERT INTO state_params VALUES (27, 1, 260, 'filter_name', 'normal');
INSERT INTO state_params VALUES (28, 1, 263, 'filter_name', 'normal');
INSERT INTO state_params VALUES (29, 1, 264, 'filter_name', 'normal');
INSERT INTO state_params VALUES (30, 1, 267, 'filter_name', 'normal');
INSERT INTO state_params VALUES (31, 1, 268, 'filter_name', 'normal');
INSERT INTO state_params VALUES (32, 1, 271, 'filter_name', 'normal');
INSERT INTO state_params VALUES (33, 1, 272, 'filter_name', 'normal');
INSERT INTO state_params VALUES (34, 1, 274, 'filter_name', 'normal');
INSERT INTO state_params VALUES (35, 1, 313, 'filter_name', 'normal');
INSERT INTO state_params VALUES (36, 1, 314, 'filter_name', 'normal');


--
-- Data for Name: stats_daily; Type: TABLE DATA; Schema: public; Owner: joaquin
--



--
-- Data for Name: stats_daily_ad_actions; Type: TABLE DATA; Schema: public; Owner: joaquin
--



--
-- Data for Name: stats_hourly; Type: TABLE DATA; Schema: public; Owner: joaquin
--



--
-- Data for Name: store_actions; Type: TABLE DATA; Schema: public; Owner: joaquin
--



--
-- Data for Name: store_action_states; Type: TABLE DATA; Schema: public; Owner: joaquin
--



--
-- Data for Name: store_changes; Type: TABLE DATA; Schema: public; Owner: joaquin
--



--
-- Data for Name: store_login_tokens; Type: TABLE DATA; Schema: public; Owner: joaquin
--



--
-- Data for Name: store_params; Type: TABLE DATA; Schema: public; Owner: joaquin
--



--
-- Data for Name: store_users; Type: TABLE DATA; Schema: public; Owner: joaquin
--



--
-- Data for Name: synonyms; Type: TABLE DATA; Schema: public; Owner: joaquin
--

INSERT INTO synonyms VALUES (1, 3040, 'armoire', NULL);
INSERT INTO synonyms VALUES (2, 3040, 'armoires ', 1);
INSERT INTO synonyms VALUES (3, 3040, 'banc', NULL);
INSERT INTO synonyms VALUES (4, 3040, 'bancs ', 3);
INSERT INTO synonyms VALUES (5, 3040, 'banquette ', NULL);
INSERT INTO synonyms VALUES (6, 3040, 'banquettes ', 5);
INSERT INTO synonyms VALUES (7, 3040, 'bergere', NULL);
INSERT INTO synonyms VALUES (8, 3040, 'bergeres', 7);
INSERT INTO synonyms VALUES (9, 3040, 'bibliotheque ', NULL);
INSERT INTO synonyms VALUES (10, 3040, 'biblioteque', 9);
INSERT INTO synonyms VALUES (11, 3040, 'bibliotheques', 9);
INSERT INTO synonyms VALUES (12, 3040, 'buffet ', NULL);
INSERT INTO synonyms VALUES (13, 3040, 'bufet', 12);
INSERT INTO synonyms VALUES (14, 3040, 'buffets', 12);
INSERT INTO synonyms VALUES (15, 3040, 'bureau ', NULL);
INSERT INTO synonyms VALUES (16, 3040, 'bureaux ', 15);
INSERT INTO synonyms VALUES (17, 3040, 'canape', NULL);
INSERT INTO synonyms VALUES (18, 3040, 'canaper ', 17);
INSERT INTO synonyms VALUES (19, 3040, 'canapes ', 17);
INSERT INTO synonyms VALUES (20, 3040, 'canapee', 17);
INSERT INTO synonyms VALUES (21, 3040, 'cannape', 17);
INSERT INTO synonyms VALUES (22, 3040, 'chaise ', NULL);
INSERT INTO synonyms VALUES (23, 3040, 'chaises ', 22);
INSERT INTO synonyms VALUES (24, 3040, 'chevet ', NULL);
INSERT INTO synonyms VALUES (25, 3040, 'chevets ', 24);
INSERT INTO synonyms VALUES (26, 3040, 'clic clac', NULL);
INSERT INTO synonyms VALUES (27, 3040, 'clic-clac', 26);
INSERT INTO synonyms VALUES (28, 3040, 'cliclac', 26);
INSERT INTO synonyms VALUES (29, 3040, 'commode ', NULL);
INSERT INTO synonyms VALUES (30, 3040, 'commodes ', 29);
INSERT INTO synonyms VALUES (31, 3040, 'comode', 29);
INSERT INTO synonyms VALUES (32, 3040, 'etagere ', NULL);
INSERT INTO synonyms VALUES (33, 3040, 'etageres ', 32);
INSERT INTO synonyms VALUES (34, 3040, 'fauteuil ', NULL);
INSERT INTO synonyms VALUES (35, 3040, 'fauteil ', 34);
INSERT INTO synonyms VALUES (36, 3040, 'fauteils ', 34);
INSERT INTO synonyms VALUES (37, 3040, 'fauteuille ', 34);
INSERT INTO synonyms VALUES (38, 3040, 'fauteuilles ', 34);
INSERT INTO synonyms VALUES (39, 3040, 'fauteuils ', 34);
INSERT INTO synonyms VALUES (40, 3040, 'matelas ', NULL);
INSERT INTO synonyms VALUES (41, 3040, 'matelat ', 40);
INSERT INTO synonyms VALUES (42, 3040, 'meuble ', NULL);
INSERT INTO synonyms VALUES (43, 3040, 'meubles ', 42);
INSERT INTO synonyms VALUES (44, 3040, 'mezzanine ', NULL);
INSERT INTO synonyms VALUES (45, 3040, 'mezanine ', 44);
INSERT INTO synonyms VALUES (46, 3040, 'porte-manteau', NULL);
INSERT INTO synonyms VALUES (47, 3040, 'porte-manteaux', 46);
INSERT INTO synonyms VALUES (48, 3040, 'porte manteau', 46);
INSERT INTO synonyms VALUES (49, 3040, 'porte manteaux', 46);
INSERT INTO synonyms VALUES (50, 3040, 'pouf', NULL);
INSERT INTO synonyms VALUES (51, 3040, 'poufs ', 50);
INSERT INTO synonyms VALUES (52, 3040, 'rangement ', NULL);
INSERT INTO synonyms VALUES (53, 3040, 'rangements ', 52);
INSERT INTO synonyms VALUES (54, 3040, 'rocking chair', NULL);
INSERT INTO synonyms VALUES (55, 3040, 'rocking-chair', 54);
INSERT INTO synonyms VALUES (56, 3040, 'siege ', NULL);
INSERT INTO synonyms VALUES (57, 3040, 'sieges', 56);
INSERT INTO synonyms VALUES (58, 3040, 'sommier ', NULL);
INSERT INTO synonyms VALUES (59, 3040, 'sommiers ', 58);
INSERT INTO synonyms VALUES (60, 3040, 'table ', NULL);
INSERT INTO synonyms VALUES (61, 3040, 'tables ', 60);
INSERT INTO synonyms VALUES (62, 3040, 'tabouret ', NULL);
INSERT INTO synonyms VALUES (63, 3040, 'tabourets ', 62);
INSERT INTO synonyms VALUES (64, 3040, 'tiroir ', NULL);
INSERT INTO synonyms VALUES (65, 3040, 'tiroirs ', 64);
INSERT INTO synonyms VALUES (66, 3040, 'vaisselier ', NULL);
INSERT INTO synonyms VALUES (67, 3040, 'vaissellier ', 66);
INSERT INTO synonyms VALUES (68, 3060, 'chauffe-eau', NULL);
INSERT INTO synonyms VALUES (69, 3060, 'chauffe eau', 68);
INSERT INTO synonyms VALUES (70, 3060, 'cuiseur', NULL);
INSERT INTO synonyms VALUES (71, 3060, 'cuisseur', 70);
INSERT INTO synonyms VALUES (72, 3060, 'cuit vapeur', NULL);
INSERT INTO synonyms VALUES (73, 3060, 'cuit-vapeur', 72);
INSERT INTO synonyms VALUES (74, 3060, 'gauffrer', NULL);
INSERT INTO synonyms VALUES (75, 3060, 'gauffrier', 74);
INSERT INTO synonyms VALUES (76, 3060, 'gaufrier', 74);
INSERT INTO synonyms VALUES (77, 3060, 'gaziniere', NULL);
INSERT INTO synonyms VALUES (78, 3060, 'gazinniere', 77);
INSERT INTO synonyms VALUES (79, 3060, 'grille-pain', NULL);
INSERT INTO synonyms VALUES (80, 3060, 'grille pain', 79);
INSERT INTO synonyms VALUES (81, 3060, 'lave linge', NULL);
INSERT INTO synonyms VALUES (82, 3060, 'lave-linge', 81);
INSERT INTO synonyms VALUES (83, 3060, 'lave vaisselle', NULL);
INSERT INTO synonyms VALUES (84, 3060, 'lave-vaisselle', 83);
INSERT INTO synonyms VALUES (85, 3060, 'micro onde', NULL);
INSERT INTO synonyms VALUES (86, 3060, 'micro ondes', 85);
INSERT INTO synonyms VALUES (87, 3060, 'micro-onde', 85);
INSERT INTO synonyms VALUES (88, 3060, 'micro-ondes', 85);
INSERT INTO synonyms VALUES (89, 3060, 'mixer', NULL);
INSERT INTO synonyms VALUES (90, 3060, 'mixeur', 89);
INSERT INTO synonyms VALUES (91, 3060, 'multifonction', NULL);
INSERT INTO synonyms VALUES (92, 3060, 'multifonctions', 91);
INSERT INTO synonyms VALUES (93, 3060, 'plaque a gaz', NULL);
INSERT INTO synonyms VALUES (94, 3060, 'plaque gaz', 93);
INSERT INTO synonyms VALUES (95, 3060, 'plaque de cuisson', NULL);
INSERT INTO synonyms VALUES (96, 3060, 'plaques de cuisson', 95);
INSERT INTO synonyms VALUES (97, 3060, 'plaque electrique', NULL);
INSERT INTO synonyms VALUES (98, 3060, 'plaques electriques', 97);
INSERT INTO synonyms VALUES (99, 3060, 'presse agrumes', NULL);
INSERT INTO synonyms VALUES (100, 3060, 'presse agrume', 99);
INSERT INTO synonyms VALUES (101, 3060, 'refrigerateur', NULL);
INSERT INTO synonyms VALUES (102, 3060, 'refrigirateur', 101);
INSERT INTO synonyms VALUES (103, 3060, 'refregirateur', 101);
INSERT INTO synonyms VALUES (104, 3060, 'seche cheveux', NULL);
INSERT INTO synonyms VALUES (105, 3060, 'seche-cheveux', 104);
INSERT INTO synonyms VALUES (106, 3060, 'seche linge', NULL);
INSERT INTO synonyms VALUES (107, 3060, 'seche-linge', 106);
INSERT INTO synonyms VALUES (108, 3060, 'table cuisson', NULL);
INSERT INTO synonyms VALUES (109, 3060, 'table de cuisson', 108);
INSERT INTO synonyms VALUES (110, 3060, 'vitroceramique', NULL);
INSERT INTO synonyms VALUES (111, 3060, 'vitro-ceramique', 110);
INSERT INTO synonyms VALUES (112, 3060, 'whirlpool', NULL);
INSERT INTO synonyms VALUES (113, 3060, 'whirpool', 112);
INSERT INTO synonyms VALUES (114, 3060, 'wirlpool', 112);
INSERT INTO synonyms VALUES (115, 8020, 'bouteille de gaz', NULL);
INSERT INTO synonyms VALUES (116, 8020, 'bouteille gaz', 115);
INSERT INTO synonyms VALUES (117, 8020, 'bouteilles de gaz', 115);
INSERT INTO synonyms VALUES (118, 8020, 'bouteilles gaz', 115);
INSERT INTO synonyms VALUES (119, 8020, 'brique', NULL);
INSERT INTO synonyms VALUES (120, 8020, 'briques', 119);
INSERT INTO synonyms VALUES (121, 8020, 'carrelage', NULL);
INSERT INTO synonyms VALUES (122, 8020, 'carrelages', 121);
INSERT INTO synonyms VALUES (123, 8020, 'casier a bouteille', NULL);
INSERT INTO synonyms VALUES (124, 8020, 'casier a bouteilles', 123);
INSERT INTO synonyms VALUES (125, 8020, 'chauffage', NULL);
INSERT INTO synonyms VALUES (126, 8020, 'chauffages', 125);
INSERT INTO synonyms VALUES (127, 8020, 'chauffe eau', NULL);
INSERT INTO synonyms VALUES (128, 8020, 'chauffe-eau', 127);
INSERT INTO synonyms VALUES (129, 8020, 'convecteur', NULL);
INSERT INTO synonyms VALUES (130, 8020, 'convecteurs', 129);
INSERT INTO synonyms VALUES (131, 8020, 'cuve', NULL);
INSERT INTO synonyms VALUES (132, 8020, 'cuves', 131);
INSERT INTO synonyms VALUES (133, 8020, 'disjoncteur', NULL);
INSERT INTO synonyms VALUES (134, 8020, 'disjoncteurs', 133);
INSERT INTO synonyms VALUES (135, 8020, 'fenetre', NULL);
INSERT INTO synonyms VALUES (136, 8020, 'fenetres', 135);
INSERT INTO synonyms VALUES (137, 8020, 'plante', NULL);
INSERT INTO synonyms VALUES (138, 8020, 'plantes', 137);
INSERT INTO synonyms VALUES (139, 8020, 'porte', NULL);
INSERT INTO synonyms VALUES (140, 8020, 'portes', 139);
INSERT INTO synonyms VALUES (141, 8020, 'radiateur', NULL);
INSERT INTO synonyms VALUES (142, 8020, 'radiateurs', 141);
INSERT INTO synonyms VALUES (143, 8020, 'remblai', NULL);
INSERT INTO synonyms VALUES (144, 8020, 'remblais', 143);
INSERT INTO synonyms VALUES (145, 8020, 'store', NULL);
INSERT INTO synonyms VALUES (146, 8020, 'stores', 145);
INSERT INTO synonyms VALUES (147, 8020, 'taille haie', NULL);
INSERT INTO synonyms VALUES (148, 8020, 'taille-haie', 147);
INSERT INTO synonyms VALUES (149, 8020, 'taille haies', 147);
INSERT INTO synonyms VALUES (150, 8020, 'taille-haies', 147);
INSERT INTO synonyms VALUES (151, 8020, 'tonneau', NULL);
INSERT INTO synonyms VALUES (152, 8020, 'tonneaux', 151);
INSERT INTO synonyms VALUES (153, 8020, 'tuile', NULL);
INSERT INTO synonyms VALUES (154, 8020, 'tuiles', 153);
INSERT INTO synonyms VALUES (155, 8020, 'tuyau', NULL);
INSERT INTO synonyms VALUES (156, 8020, 'tuyaux', 155);
INSERT INTO synonyms VALUES (157, 8020, 'vasque', NULL);
INSERT INTO synonyms VALUES (158, 8020, 'vasques', 157);
INSERT INTO synonyms VALUES (159, 8020, 'volet', NULL);
INSERT INTO synonyms VALUES (160, 8020, 'volets', 159);
INSERT INTO synonyms VALUES (161, 8060, 'biberon', NULL);
INSERT INTO synonyms VALUES (162, 8060, 'biberons', 161);
INSERT INTO synonyms VALUES (163, 8060, 'chaussure', NULL);
INSERT INTO synonyms VALUES (164, 8060, 'chaussures', 163);
INSERT INTO synonyms VALUES (165, 8060, 'ensemble fille', NULL);
INSERT INTO synonyms VALUES (166, 8060, 'ensemble filles', 165);
INSERT INTO synonyms VALUES (167, 8060, 'ensembles fille', 165);
INSERT INTO synonyms VALUES (168, 8060, 'ensembles filles', 165);
INSERT INTO synonyms VALUES (169, 8060, 'ensemble garcon', NULL);
INSERT INTO synonyms VALUES (170, 8060, 'ensemble garcons', 169);
INSERT INTO synonyms VALUES (171, 8060, 'ensembles garcon', 169);
INSERT INTO synonyms VALUES (172, 8060, 'ensembles garcons', 169);
INSERT INTO synonyms VALUES (173, 8060, 'gigoteuse', NULL);
INSERT INTO synonyms VALUES (174, 8060, 'gigoteuses', 173);
INSERT INTO synonyms VALUES (175, 8060, 'landau', NULL);
INSERT INTO synonyms VALUES (176, 8060, 'landeau', 175);
INSERT INTO synonyms VALUES (177, 8060, 'lit parapluie', NULL);
INSERT INTO synonyms VALUES (178, 8060, 'lit-parapluie', 177);
INSERT INTO synonyms VALUES (179, 8060, 'pantalon', NULL);
INSERT INTO synonyms VALUES (180, 8060, 'pantalons', 179);
INSERT INTO synonyms VALUES (181, 8060, 'porte bebe', NULL);
INSERT INTO synonyms VALUES (182, 8060, 'porte-bb', 181);
INSERT INTO synonyms VALUES (183, 8060, 'porte-bebe', 181);
INSERT INTO synonyms VALUES (184, 8060, 'poussette', NULL);
INSERT INTO synonyms VALUES (185, 8060, 'pousette', 184);
INSERT INTO synonyms VALUES (186, 8060, 'pyjama', NULL);
INSERT INTO synonyms VALUES (187, 8060, 'pyjamas', 186);
INSERT INTO synonyms VALUES (188, 8060, 'salopette', NULL);
INSERT INTO synonyms VALUES (189, 8060, 'salopettes', 188);
INSERT INTO synonyms VALUES (190, 8060, 'tee shirt', NULL);
INSERT INTO synonyms VALUES (191, 8060, 'tee-shirt', 190);
INSERT INTO synonyms VALUES (192, 8060, 'tshirt', 190);
INSERT INTO synonyms VALUES (193, 8060, 'vertbaudet', NULL);
INSERT INTO synonyms VALUES (194, 8060, 'vert baudet', 193);
INSERT INTO synonyms VALUES (195, 8060, 'verbaudet', 193);
INSERT INTO synonyms VALUES (196, 8060, 'vetement bebe', NULL);
INSERT INTO synonyms VALUES (197, 8060, 'vetements bebe', 196);
INSERT INTO synonyms VALUES (198, 8060, 'vetements enfant', NULL);
INSERT INTO synonyms VALUES (199, 8060, 'vetement enfant', 198);
INSERT INTO synonyms VALUES (200, 8060, 'vetements enfants', 198);
INSERT INTO synonyms VALUES (201, 8060, 'vetements fille', NULL);
INSERT INTO synonyms VALUES (202, 8060, 'vetements filles', 201);
INSERT INTO synonyms VALUES (203, 8060, 'vetements garcon', NULL);
INSERT INTO synonyms VALUES (204, 8060, 'vetements garcons', 203);
INSERT INTO synonyms VALUES (205, 41, 'babyfoot', NULL);
INSERT INTO synonyms VALUES (206, 41, 'baby-foot', 205);
INSERT INTO synonyms VALUES (207, 41, 'baby foot', 205);
INSERT INTO synonyms VALUES (208, 41, 'barbie', NULL);
INSERT INTO synonyms VALUES (209, 41, 'barbies', 208);
INSERT INTO synonyms VALUES (210, 41, 'camion de pompier', NULL);
INSERT INTO synonyms VALUES (211, 41, 'camion pompier', 210);
INSERT INTO synonyms VALUES (212, 41, 'domino', NULL);
INSERT INTO synonyms VALUES (213, 41, 'dominos', 212);
INSERT INTO synonyms VALUES (214, 41, 'dora', NULL);
INSERT INTO synonyms VALUES (215, 41, 'dora l''exploratrice', 214);
INSERT INTO synonyms VALUES (216, 41, 'figurine', NULL);
INSERT INTO synonyms VALUES (217, 41, 'figurines', 216);
INSERT INTO synonyms VALUES (218, 41, 'fisher price', NULL);
INSERT INTO synonyms VALUES (219, 41, 'fischer price', 218);
INSERT INTO synonyms VALUES (220, 41, 'fisherprice', 218);
INSERT INTO synonyms VALUES (221, 41, 'fisher-price', 218);
INSERT INTO synonyms VALUES (222, 41, 'jeu de construction', NULL);
INSERT INTO synonyms VALUES (223, 41, 'jeux de construction', 222);
INSERT INTO synonyms VALUES (224, 41, 'jeu de societe', NULL);
INSERT INTO synonyms VALUES (225, 41, 'jeux de societe', 224);
INSERT INTO synonyms VALUES (226, 41, 'jeu educatif', NULL);
INSERT INTO synonyms VALUES (227, 41, 'jeux educatifs', 226);
INSERT INTO synonyms VALUES (228, 41, 'jouet', NULL);
INSERT INTO synonyms VALUES (229, 41, 'jouets', 228);
INSERT INTO synonyms VALUES (230, 41, 'lego', NULL);
INSERT INTO synonyms VALUES (231, 41, 'legos', 230);
INSERT INTO synonyms VALUES (232, 41, 'peluche', NULL);
INSERT INTO synonyms VALUES (233, 41, 'pelluche', 232);
INSERT INTO synonyms VALUES (234, 41, 'peluches', 232);
INSERT INTO synonyms VALUES (235, 41, 'playmobil', NULL);
INSERT INTO synonyms VALUES (236, 41, 'playmobils', 235);
INSERT INTO synonyms VALUES (237, 41, 'playmobile', 235);
INSERT INTO synonyms VALUES (238, 41, 'poupee', NULL);
INSERT INTO synonyms VALUES (239, 41, 'poupees', 238);
INSERT INTO synonyms VALUES (240, 41, 'puzzle', NULL);
INSERT INTO synonyms VALUES (241, 41, 'puzzles', 240);
INSERT INTO synonyms VALUES (242, 41, 'trottinette', NULL);
INSERT INTO synonyms VALUES (243, 41, 'trotinette', 242);
INSERT INTO synonyms VALUES (244, 41, 'vtech', NULL);
INSERT INTO synonyms VALUES (245, 41, 'v-tech', 244);


--
-- Data for Name: trans_queue; Type: TABLE DATA; Schema: public; Owner: joaquin
--

INSERT INTO trans_queue VALUES (14, '2014-03-25 14:23:12.938253', '24258@mazaru.schibsted.cl', NULL, '2014-03-25 14:23:29.73159', '24258@mazaru.schibsted.cl', 'TRANS_OK', '2014-03-25 14:23:29.642079', NULL, 'cmd:admail
commit:1
ad_id:33
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue VALUES (15, '2014-03-25 14:23:18.316039', '24258@mazaru.schibsted.cl', NULL, '2014-03-25 14:23:29.738806', '24258@mazaru.schibsted.cl', 'TRANS_OK', '2014-03-25 14:23:29.642079', NULL, 'cmd:admail
commit:1
ad_id:34
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue VALUES (13, '2014-03-25 14:23:10.749077', '24258@mazaru.schibsted.cl', NULL, '2014-03-25 14:23:29.739077', '24258@mazaru.schibsted.cl', 'TRANS_OK', '2014-03-25 14:23:29.642079', NULL, 'cmd:admail
commit:1
ad_id:32
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue VALUES (10, '2014-03-25 14:22:55.816988', '24258@mazaru.schibsted.cl', NULL, '2014-03-25 14:23:29.819628', '24258@mazaru.schibsted.cl', 'TRANS_OK', '2014-03-25 14:23:29.642079', NULL, 'cmd:admail
commit:1
ad_id:29
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue VALUES (9, '2014-03-25 14:22:53.149757', '24258@mazaru.schibsted.cl', NULL, '2014-03-25 14:23:29.825417', '24258@mazaru.schibsted.cl', 'TRANS_OK', '2014-03-25 14:23:29.642079', NULL, 'cmd:admail
commit:1
ad_id:28
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue VALUES (7, '2014-03-25 14:22:46.037151', '24258@mazaru.schibsted.cl', NULL, '2014-03-25 14:23:29.826121', '24258@mazaru.schibsted.cl', 'TRANS_OK', '2014-03-25 14:23:29.642079', NULL, 'cmd:admail
commit:1
ad_id:26
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue VALUES (8, '2014-03-25 14:22:48.501542', '24258@mazaru.schibsted.cl', NULL, '2014-03-25 14:23:29.825586', '24258@mazaru.schibsted.cl', 'TRANS_OK', '2014-03-25 14:23:29.642079', NULL, 'cmd:admail
commit:1
ad_id:27
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue VALUES (11, '2014-03-25 14:23:00.816751', '24258@mazaru.schibsted.cl', NULL, '2014-03-25 14:23:29.826007', '24258@mazaru.schibsted.cl', 'TRANS_OK', '2014-03-25 14:23:29.642079', NULL, 'cmd:admail
commit:1
ad_id:30
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue VALUES (6, '2014-03-25 14:22:41.163195', '24258@mazaru.schibsted.cl', NULL, '2014-03-25 14:23:29.82574', '24258@mazaru.schibsted.cl', 'TRANS_OK', '2014-03-25 14:23:29.642079', NULL, 'cmd:admail
commit:1
ad_id:25
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue VALUES (12, '2014-03-25 14:23:03.304295', '24258@mazaru.schibsted.cl', NULL, '2014-03-25 14:23:29.825834', '24258@mazaru.schibsted.cl', 'TRANS_OK', '2014-03-25 14:23:29.642079', NULL, 'cmd:admail
commit:1
ad_id:31
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue VALUES (4, '2014-03-25 14:22:32.98196', '24258@mazaru.schibsted.cl', NULL, '2014-03-25 14:23:29.906257', '24258@mazaru.schibsted.cl', 'TRANS_OK', '2014-03-25 14:23:29.829697', NULL, 'cmd:admail
commit:1
ad_id:23
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue VALUES (3, '2014-03-25 14:22:30.570721', '24258@mazaru.schibsted.cl', NULL, '2014-03-25 14:23:29.906511', '24258@mazaru.schibsted.cl', 'TRANS_OK', '2014-03-25 14:23:29.829697', NULL, 'cmd:admail
commit:1
ad_id:22
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue VALUES (1, '2014-03-25 14:22:22.46022', '24258@mazaru.schibsted.cl', NULL, '2014-03-25 14:23:29.906784', '24258@mazaru.schibsted.cl', 'TRANS_OK', '2014-03-25 14:23:29.829697', NULL, 'cmd:admail
commit:1
ad_id:20
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue VALUES (5, '2014-03-25 14:22:38.848071', '24258@mazaru.schibsted.cl', NULL, '2014-03-25 14:23:29.90711', '24258@mazaru.schibsted.cl', 'TRANS_OK', '2014-03-25 14:23:29.829697', NULL, 'cmd:admail
commit:1
ad_id:24
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue VALUES (2, '2014-03-25 14:22:25.78462', '24258@mazaru.schibsted.cl', NULL, '2014-03-25 14:23:29.907282', '24258@mazaru.schibsted.cl', 'TRANS_OK', '2014-03-25 14:23:29.829697', NULL, 'cmd:admail
commit:1
ad_id:21
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);


--
-- Data for Name: unfinished_ads; Type: TABLE DATA; Schema: public; Owner: joaquin
--



--
-- Data for Name: user_params; Type: TABLE DATA; Schema: public; Owner: joaquin
--

INSERT INTO user_params VALUES (50, 'first_approved_ad', '2014-03-25 14:22:22.424495-03');
INSERT INTO user_params VALUES (51, 'first_approved_ad', '2014-03-25 14:22:32.972419-03');
INSERT INTO user_params VALUES (52, 'first_approved_ad', '2014-03-25 14:22:55.807858-03');
INSERT INTO user_params VALUES (53, 'first_approved_ad', '2014-03-25 14:23:10.740153-03');
INSERT INTO user_params VALUES (54, 'first_approved_ad', '2014-03-26 09:49:29.791808-03');

INSERT INTO user_params VALUES (50, 'first_inserted_ad', '2014-03-25 14:22:22.424495-03');
INSERT INTO user_params VALUES (51, 'first_inserted_ad', '2014-03-25 14:22:32.972419-03');
INSERT INTO user_params VALUES (52, 'first_inserted_ad', '2014-03-25 14:22:55.807858-03');
INSERT INTO user_params VALUES (53, 'first_inserted_ad', '2014-03-25 14:23:10.740153-03');
INSERT INTO user_params VALUES (54, 'first_inserted_ad', '2014-03-26 09:49:29.791808-03');

--
-- Data for Name: user_testimonial; Type: TABLE DATA; Schema: public; Owner: joaquin
--



--
-- Data for Name: visitor; Type: TABLE DATA; Schema: public; Owner: joaquin
--



--
-- Data for Name: visits; Type: TABLE DATA; Schema: public; Owner: joaquin
--



--
-- Data for Name: vouchers; Type: TABLE DATA; Schema: public; Owner: joaquin
--



--
-- Data for Name: voucher_actions; Type: TABLE DATA; Schema: public; Owner: joaquin
--



--
-- Data for Name: voucher_states; Type: TABLE DATA; Schema: public; Owner: joaquin
--



--
-- Data for Name: watch_ads; Type: TABLE DATA; Schema: public; Owner: joaquin
--



--
-- PostgreSQL database dump complete
--

--Migrate inmo ads
select * from bpv.migrate_ads_inmo();
