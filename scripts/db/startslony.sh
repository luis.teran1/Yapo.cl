#!/bin/bash

[ -z "$PGHOST" -o -z "$USER" ] && ( echo "PGHOST and USER must be set" ; exit 1 )

export CLUSTERNAME=bcluster
export MASTERDBNAME=blocketdb
export SLAVEDBNAME=repl
export MASTERHOST=$PGHOST
export SLAVEHOST=$PGHOST
export REPLICATIONUSER=$USER

psql -h "$MASTERHOST" -c "CREATE DATABASE $SLAVEDBNAME;"
psql -h "$SLAVEHOST" "$SLAVEDBNAME" -c "CREATE LANGUAGE plpgsql;"
pg_dump -s -U $REPLICATIONUSER -h $MASTERHOST $MASTERDBNAME | psql -U $REPLICATIONUSER -h $SLAVEHOST $SLAVEDBNAME

tables=''
i=1
for t in `psql -At -h $MASTERHOST $MASTERDBNAME -c "select schemaname || '.' || tablename from pg_tables where schemaname = 'public'"`; do
	tables="$tables
	set add table (set id = 1, origin = 1, id = $i, fully qualified name = '$t', comment = '$t table');"
	(( i++ ))
done
j=1
seqs=''
for s in `psql -At -h $MASTERHOST $MASTERDBNAME -c "SELECT n.nspname || '.' || c.relname FROM pg_catalog.pg_class AS c JOIN pg_catalog.pg_namespace n ON n.oid = c.relnamespace WHERE c.relkind IN ('S','') AND n.nspname = 'public'"`; do
	seqs="$seqs
	set add sequence (set id = 1, origin = 1, id = $j, fully qualified name = '$s', comment = '$s sequence');"
	(( j++ ))
done

slonik <<_EOF_
	cluster name = $CLUSTERNAME;

	node 1 admin conninfo = 'dbname=$MASTERDBNAME host=$MASTERHOST user=$REPLICATIONUSER';
	node 2 admin conninfo = 'dbname=$SLAVEDBNAME host=$SLAVEHOST user=$REPLICATIONUSER';

	init cluster ( id=1, comment = 'Master Node');

	create set (id=1, origin=1, comment='All public tables');
	$tables
	$seqs

	store node (id=2, comment = 'Slave node', event node=1);
	store path (server = 1, client = 2, conninfo='dbname=$MASTERDBNAME host=$MASTERHOST user=$REPLICATIONUSER');
	store path (server = 2, client = 1, conninfo='dbname=$SLAVEDBNAME host=$SLAVEHOST user=$REPLICATIONUSER');
_EOF_

slon $CLUSTERNAME "dbname=$MASTERDBNAME user=$REPLICATIONUSER host=$MASTERHOST" < /dev/null > $MASTERHOST/pg_log/slony_master 2>&1 &
slon $CLUSTERNAME "dbname=$SLAVEDBNAME user=$REPLICATIONUSER host=$SLAVEHOST" < /dev/null > $SLAVEHOST/pg_log/slony_slave 2>&1 &

slonik <<_EOF_
	 cluster name = $CLUSTERNAME;

	 node 1 admin conninfo = 'dbname=$MASTERDBNAME host=$MASTERHOST user=$REPLICATIONUSER';
	 node 2 admin conninfo = 'dbname=$SLAVEDBNAME host=$SLAVEHOST user=$REPLICATIONUSER';

	 subscribe set ( id = 1, provider = 1, receiver = 2, forward = no);
_EOF_

