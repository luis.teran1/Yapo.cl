UPDATE ads SET status='deactivated' WHERE ad_id IN (95, 86, 94);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (95, 2, 'deactivate', 698, 'deactivated', 'normal', NULL, NULL, NULL);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (86, 2, 'deactivate', 699, 'deactivated', 'normal', NULL, NULL, NULL);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (94, 2, 'deactivate', 700, 'deactivated', 'normal', NULL, NULL, NULL);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (33, 2, 'deactivate', 701, 'deactivated', 'normal', NULL, NULL, NULL);
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (95, 2, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (86, 2, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (94, 2, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (33, 2, 'source', 'web');
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (95, 2, 698, 'deactivated', 'user_deactivated', '2015-10-08 12:02:02.51915', '10.0.1.131', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (86, 2, 699, 'deactivated', 'user_deactivated', '2015-10-08 12:02:21.021585', '10.0.1.131', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (94, 2, 700, 'deactivated', 'user_deactivated', '2015-10-08 12:03:42.021585', '10.0.1.131', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (33, 2, 701, 'deactivated', 'user_deactivated', '2016-09-23 12:03:42.021585', '10.0.1.131', NULL);
SELECT pg_catalog.setval('action_states_state_id_seq', 701, true);
UPDATE ads SET status='deactivated' WHERE ad_id = 33;
INSERT INTO ad_params (ad_id, name, value) VALUES (33, 'currency', 'uf');
UPDATE ads SET price='450032' WHERE ad_id = 33;

--Change all passwords to 123123123
update ads set salted_passwd =  E'$1024$2y1bQ}VYRD}zF4QCbf07a971b784888658a20e62156de64dfde5ffb6', passwd = null;
update accounts set salted_passwd =  E'$1024$2y1bQ}VYRD}zF4QCbf07a971b784888658a20e62156de64dfde5ffb6';
