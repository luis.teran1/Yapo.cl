--
-- Data for Name: account_actions; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.account_actions (account_id, account_action_id, account_action_type, "timestamp", token_id) VALUES (6, 1, 'edition', '2020-01-22 15:48:22.809109', NULL);
INSERT INTO public.account_actions (account_id, account_action_id, account_action_type, "timestamp", token_id) VALUES (6, 2, 'edition', '2020-01-31 17:47:54.521919', NULL);


--
-- Data for Name: ads; Type: TABLE DATA; Schema: public; Owner: -
--


INSERT INTO public.ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (126, 8000086, '2020-02-05 14:15:04', 'active', 'sell', 'ManyAdsAccount', '987654321', 15, 0, 2020, 57, NULL, false, false, false, 'Toyota corolla 2012', 'poco uso', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$2y1bQ}VYRD}zF4QCbf07a971b784888658a20e62156de64dfde5ffb6', '2020-02-05 14:15:04.27932', 'es');
INSERT INTO public.ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (127, 8000085, '2020-02-05 14:14:57', 'active', 'sell', 'ManyAdsAccount', '987654321', 15, 0, 2100, 57, NULL, false, false, false, 'Parachoques para toyota autana 2012', 'origina importado', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$2y1bQ}VYRD}zF4QCbf07a971b784888658a20e62156de64dfde5ffb6', '2020-02-05 14:14:57.353203', 'es');


INSERT INTO public.payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (166, '492270960', 'verified', '2020-01-22 15:48:22.735265', NULL);
INSERT INTO public.payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (167, '1549492866', 'verified', '2020-01-31 17:47:54.454552', NULL);
--
-- Data for Name: ad_actions; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (126, 1, 'new', 707, 'accepted', 'normal', NULL, NULL, 166);
INSERT INTO public.ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (127, 1, 'new', 706, 'accepted', 'normal', NULL, NULL, 167);

--
-- Data for Name: action_params; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.action_params (ad_id, action_id, name, value) VALUES (126, 1, 'source', 'web');
INSERT INTO public.action_params (ad_id, action_id, name, value) VALUES (126, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO public.action_params (ad_id, action_id, name, value) VALUES (127, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO public.action_params (ad_id, action_id, name, value) VALUES (127, 1, 'source', 'web');



--
-- Data for Name: action_states; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (126, 1, 698, 'reg', 'initial', '2020-01-22 15:48:22.735265', '172.16.0.1', NULL);
INSERT INTO public.action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (126, 1, 699, 'unverified', 'verifymail', '2020-01-22 15:48:22.735265', '172.16.0.1', NULL);
INSERT INTO public.action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (126, 1, 700, 'pending_review', 'verify', '2020-01-22 15:48:22.849034', '172.16.0.1', NULL);
INSERT INTO public.action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (126, 1, 701, 'pending_review', 'newqueue', '2020-01-22 15:48:22.873325', NULL, NULL);
INSERT INTO public.action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (126, 1, 707, 'accepted', 'accept', '2020-02-05 14:15:04.27932', '172.16.0.1', 706);
INSERT INTO public.action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (127, 1, 702, 'reg', 'initial', '2020-01-31 17:47:54.454552', '172.16.0.1', NULL);
INSERT INTO public.action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (127, 1, 703, 'unverified', 'verifymail', '2020-01-31 17:47:54.454552', '172.16.0.1', NULL);
INSERT INTO public.action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (127, 1, 704, 'pending_review', 'verify', '2020-01-31 17:47:54.559156', '172.16.0.1', NULL);
INSERT INTO public.action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (127, 1, 705, 'pending_review', 'newqueue', '2020-01-31 17:47:54.580969', NULL, NULL);
INSERT INTO public.action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (127, 1, 706, 'accepted', 'accept', '2020-02-05 14:14:57.353203', '172.16.0.1', 706);

--
-- Name: action_states_state_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.action_states_state_id_seq', 707, true);



SELECT pg_catalog.setval('public.list_id_seq', 8000086, true);


INSERT INTO public.ad_image_changes (ad_id, action_id, state_id, seq_no, name, is_new) VALUES (126, 1, 707, 0, '8308523319.jpg', false);
INSERT INTO public.ad_image_changes (ad_id, action_id, state_id, seq_no, name, is_new) VALUES (127, 1, 706, 0, NULL, false);

--
-- Data for Name: ad_changes; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.ad_codes (code, code_type) VALUES (56764, 'pay');

--
-- Data for Name: ad_images_digests; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.ad_images_digests (ad_id, name, digest, uid, previously_inserted) VALUES (126, '8308523319.jpg', '99610d2053efb2b9252707d5439a4735', 51, false);


--
-- Data for Name: ad_media; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.ad_media (ad_media_id, ad_id, seq_no, upload_time, media_type, width, height, length) VALUES (8308523319, 126, 0, '2020-01-22 15:48:19.007057', 'image', NULL, NULL, NULL);


--
-- Data for Name: ad_media_changes; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.ad_params (ad_id, name, value) VALUES (126, 'regdate', '2012');
INSERT INTO public.ad_params (ad_id, name, value) VALUES (126, 'mileage', '100000');
INSERT INTO public.ad_params (ad_id, name, value) VALUES (126, 'gearbox', '2');
INSERT INTO public.ad_params (ad_id, name, value) VALUES (126, 'fuel', '1');
INSERT INTO public.ad_params (ad_id, name, value) VALUES (126, 'cartype', '1');
INSERT INTO public.ad_params (ad_id, name, value) VALUES (126, 'communes', '315');
INSERT INTO public.ad_params (ad_id, name, value) VALUES (126, 'currency', 'peso');
INSERT INTO public.ad_params (ad_id, name, value) VALUES (126, 'brand', '88');
INSERT INTO public.ad_params (ad_id, name, value) VALUES (126, 'model', '8');
INSERT INTO public.ad_params (ad_id, name, value) VALUES (126, 'version', '17');
INSERT INTO public.ad_params (ad_id, name, value) VALUES (126, 'geoposition_is_precise', '0');
INSERT INTO public.ad_params (ad_id, name, value) VALUES (126, 'country', 'UNK');
INSERT INTO public.ad_params (ad_id, name, value) VALUES (126, 'prev_currency', 'peso');
INSERT INTO public.ad_params (ad_id, name, value) VALUES (127, 'communes', '315');
INSERT INTO public.ad_params (ad_id, name, value) VALUES (127, 'country', 'UNK');
INSERT INTO public.ad_params (ad_id, name, value) VALUES (127, 'currency', 'peso');
INSERT INTO public.ad_params (ad_id, name, value) VALUES (127, 'geoposition_is_precise', '0');
INSERT INTO public.ad_params (ad_id, name, value) VALUES (127, 'prev_currency', 'peso');


--
-- Data for Name: ad_queues; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.ad_queues (ad_id, action_id, queue, queued_at, locked_by, locked_until) VALUES (126, 1, 'normal', '2020-01-22 15:48:22.849034', NULL, NULL);
INSERT INTO public.ad_queues (ad_id, action_id, queue, queued_at, locked_by, locked_until) VALUES (127, 1, 'normal', '2020-01-31 17:47:54.559156', NULL, NULL);

--
-- Name: ads_ad_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.ads_ad_id_seq', 127, true);

--
-- Data for Name: email_uuid; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.email_uuid (email, uuid, name) VALUES ('many@ads.cl', '7ccb3a8b-29be-4d37-8ba9-b27b7493b540', '');


--
-- Name: next_image_id; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.next_image_id', 1, true);

--
-- Name: pay_code_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.pay_code_seq', 468, true);


--
-- Data for Name: pay_log; Type: TABLE DATA; Schema: public; Owner: -
--
INSERT INTO public.pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (213, 'save', 0, '492270960', NULL, '2020-01-22 15:48:23', 166, 'SAVE');
INSERT INTO public.pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (214, 'verify', 0, '492270960', NULL, '2020-01-22 15:48:23', 166, 'OK');
INSERT INTO public.pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (215, 'save', 0, '1549492866', NULL, '2020-01-31 17:47:54', 167, 'SAVE');
INSERT INTO public.pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (216, 'verify', 0, '1549492866', NULL, '2020-01-31 17:47:55', 167, 'OK');

--
-- Name: pay_log_pay_log_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.pay_log_pay_log_id_seq', 216, true);


--
-- Data for Name: pay_log_references; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (213, 0, 'adphone', '987654321');
INSERT INTO public.pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (213, 1, 'remote_addr', '172.16.0.1');
INSERT INTO public.pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (214, 0, 'remote_addr', '172.16.0.1');
INSERT INTO public.pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (215, 0, 'adphone', '987654321');
INSERT INTO public.pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (215, 1, 'remote_addr', '172.16.0.1');
INSERT INTO public.pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (216, 0, 'remote_addr', '172.16.0.1');


--
-- Name: payment_groups_payment_group_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.payment_groups_payment_group_id_seq', 167, true);


--
-- Data for Name: payments; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.payments (payment_group_id, payment_type, pay_amount, discount) VALUES (166, 'ad_action', 0, 0);
INSERT INTO public.payments (payment_group_id, payment_type, pay_amount, discount) VALUES (167, 'ad_action', 0, 0);



--
-- Data for Name: state_params; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.state_params (ad_id, action_id, state_id, name, value) VALUES (126, 1, 700, 'queue', 'preprocessing');
INSERT INTO public.state_params (ad_id, action_id, state_id, name, value) VALUES (126, 1, 701, 'queue', 'normal');
INSERT INTO public.state_params (ad_id, action_id, state_id, name, value) VALUES (126, 1, 707, 'filter_name', 'all');
INSERT INTO public.state_params (ad_id, action_id, state_id, name, value) VALUES (127, 1, 706, 'filter_name', 'all');


INSERT INTO public.review_log (ad_id, action_id, admin_id, review_time, queue, action_type, action, refusal_reason_text, category) VALUES (126, 1, 9, '2020-02-05 14:15:04.27932', 'all', 'new', 'accepted', NULL, 2020);
INSERT INTO public.review_log (ad_id, action_id, admin_id, review_time, queue, action_type, action, refusal_reason_text, category) VALUES (127, 1, 9, '2020-02-05 14:14:57.353203', 'all', 'new', 'accepted', NULL, 2100);


