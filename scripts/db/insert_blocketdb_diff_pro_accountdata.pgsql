insert into account_params (account_id, name, value) values (1,'is_pro_for','1260,7020');
INSERT INTO account_params (account_id, name, value) VALUES (1, 'pack_type', 'real_estate');
INSERT INTO account_params (account_id, name, value) VALUES (7, 'is_pro_for', '7020');

--Change all passwords to 123123123
update ads set salted_passwd =  E'$1024$2y1bQ}VYRD}zF4QCbf07a971b784888658a20e62156de64dfde5ffb6', passwd = null;
update accounts set salted_passwd =  E'$1024$2y1bQ}VYRD}zF4QCbf07a971b784888658a20e62156de64dfde5ffb6';

update accounts set is_company = 't' where account_id = 7;
