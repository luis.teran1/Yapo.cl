INSERT INTO block_lists VALUES (1, 'E-mail addresses - erase', true, 'email', NULL);
INSERT INTO block_lists VALUES (2, 'E-mail addresses - spamfilter', true, 'email', NULL);
INSERT INTO block_lists VALUES (3, 'IP addresses - erase', true, 'ip', NULL);
INSERT INTO block_lists VALUES (4, 'IP addresses - spamfilter', true, 'ip', NULL);
INSERT INTO block_lists VALUES (5, 'Blocked E-mail addresses', false, 'email', NULL);
INSERT INTO block_lists VALUES (6, 'Phone numbers in insert ad', false, 'general', '-+ ');
INSERT INTO block_lists VALUES (7, 'Send tip', false, 'email', NULL);
INSERT INTO block_lists VALUES (8, 'Synonyms for word Advertising (DO NOT USE)', false, 'general', NULL);
INSERT INTO block_lists VALUES (9, 'Synonyms for word Gratis (DO NOT USE)', false, 'general', NULL);
INSERT INTO block_lists VALUES (10, 'Sites that may be spam', false, 'general', NULL);
INSERT INTO block_lists VALUES (11, 'Words used by sites such as spam', false, 'general', NULL);
INSERT INTO block_lists VALUES (12, 'Ugly Words', false, 'general', NULL);
INSERT INTO block_lists VALUES (13, 'Forbidden words in ad subjects', false, 'general', NULL);
INSERT INTO block_lists VALUES (14, 'English terms used in reply mail form', false, 'general', NULL);
INSERT INTO block_lists VALUES (15, 'Byten.se (DO NOT USE)', false, 'general', NULL);
INSERT INTO block_lists VALUES (16, 'whitelist', false, 'email', NULL);
INSERT INTO block_lists VALUES (17, 'were_whitelist', false, 'email', NULL);

ALTER SEQUENCE block_lists_list_id_seq RESTART WITH 90;

INSERT INTO block_rules VALUES (1, 'Site spam', 'delete', 'adreply', 'and', 0, 0, '2010-03-30', NULL);
INSERT INTO block_rules VALUES (2, 'Advertise for free', 'delete', 'adreply', 'and', 0, 0, '2010-03-30', NULL);
INSERT INTO block_rules VALUES (3, 'Ugly Words', 'stop', 'newad', 'and', 0, 0, '2010-03-30', NULL);
INSERT INTO block_rules VALUES (4, 'Ugly Words', 'delete', 'adreply', 'and', 0, 0, '2010-03-30', NULL);
INSERT INTO block_rules VALUES (5, 'Blocked e-mail addresses  - erased', 'delete', 'adreply', 'and', 0, 0, '2010-03-30', NULL);
INSERT INTO block_rules VALUES (6, 'Blocked e-mail addresses', 'stop', 'newad', 'and', 0, 0, '2010-03-30', NULL);
INSERT INTO block_rules VALUES (7, 'Blocked IP addresses - erased', 'delete', 'adreply', 'and', 0, 0, '2010-03-30', NULL);
INSERT INTO block_rules VALUES (8, 'Blocked Phone Numbers', 'stop', 'newad', 'and', 0, 0, '2010-03-30', NULL);
INSERT INTO block_rules VALUES (9, 'Blocked tips receiver', 'delete', 'sendtip', 'and', 0, 0, '2010-03-30', NULL);
INSERT INTO block_rules VALUES (10, 'Byten.se', 'delete', 'adreply', 'and', 0, 0, '2010-03-30', NULL);
INSERT INTO block_rules VALUES (11, 'Byten.se', 'delete', 'sendtip', 'and', 0, 0, '2010-03-30', NULL);
INSERT INTO block_rules VALUES (12, 'Blocked words in headlines', 'stop', 'newad', 'and', 0, 0, '2010-03-30', NULL);
INSERT INTO block_rules VALUES (13, 'Blocked English e-mail', 'spamfilter', 'adreply', 'and', 0, 0, '2010-03-30', NULL);
INSERT INTO block_rules VALUES (14, 'Blocked e-mail addresses - spamfilter', 'spamfilter', 'adreply', 'and', 0, 0, '2010-03-30', NULL);
INSERT INTO block_rules VALUES (15, 'Blocked IP addresses - spamfilter', 'spamfilter', 'adreply', 'and', 0, 0, '2010-03-30', NULL);
INSERT INTO block_rules VALUES (16, 'Ugly Words', 'delete', 'sendtip', 'or', 0, 0, '2010-03-30', NULL);

INSERT INTO block_rules (rule_id, rule_name, action, application, combine_rule, target) VALUES (17, 'whitelist rule','move_to_queue','clear','and','whitelist');
ALTER SEQUENCE block_rules_rule_id_seq RESTART WITH 90;

INSERT INTO block_rule_conditions VALUES (1, 1, 10, 0, '{subject,body}');
INSERT INTO block_rule_conditions VALUES (2, 1, 11, 0, '{subject,body}');
INSERT INTO block_rule_conditions VALUES (3, 2, 8, 0, '{subject,body}');
INSERT INTO block_rule_conditions VALUES (4, 2, 9, 0, '{subject,body}');
INSERT INTO block_rule_conditions VALUES (5, 3, 12, 0, '{subject,body}');
INSERT INTO block_rule_conditions VALUES (6, 4, 12, 0, '{subject,body}');
INSERT INTO block_rule_conditions VALUES (7, 5, 1, 1, '{email}');
INSERT INTO block_rule_conditions VALUES (8, 6, 5, 1, '{email}');
INSERT INTO block_rule_conditions VALUES (9, 7, 3, 1, '{remote_addr}');
INSERT INTO block_rule_conditions VALUES (10, 8, 6, 1, '{phone}');
INSERT INTO block_rule_conditions VALUES (11, 9, 7, 1, '{email}');
INSERT INTO block_rule_conditions VALUES (12, 10, 15, 0, '{name,email,subject,body}');
INSERT INTO block_rule_conditions VALUES (13, 11, 15, 0, '{name,email,subject,body}');
INSERT INTO block_rule_conditions VALUES (14, 12, 13, 0, '{subject}');
INSERT INTO block_rule_conditions VALUES (15, 13, 14, 0, '{body}');
INSERT INTO block_rule_conditions VALUES (16, 14, 2, 1, '{subject}');
INSERT INTO block_rule_conditions VALUES (17, 15, 4, 1, '{remote_addr}');
INSERT INTO block_rule_conditions VALUES (18, 16, 12, 0, '{body}');

INSERT INTO block_rule_conditions VALUES (19, 17,16,1,'{email}');
ALTER SEQUENCE block_rule_conditions_condition_id_seq RESTART WITH 90;

INSERT INTO blocked_items VALUES (1, 'ful@fisk.se', 1, NULL, NULL);
INSERT INTO blocked_items VALUES (2, 'ful@fisk.se', 5, NULL, NULL);
INSERT INTO blocked_items VALUES (3, '0733555501', 6, NULL, NULL);
INSERT INTO blocked_items VALUES (4, 'ful@fisk.se', 7, NULL, NULL);
INSERT INTO blocked_items VALUES (5, 'fisk.se', 10, NULL, NULL);
INSERT INTO blocked_items VALUES (6, 'vatten', 11, NULL, NULL);
INSERT INTO blocked_items VALUES (7, 'fitta', 12, NULL, NULL);
INSERT INTO blocked_items VALUES (8, 'analakrobat', 12, NULL, NULL);
INSERT INTO blocked_items VALUES (9, 'vendo', 13, NULL, NULL);
INSERT INTO blocked_items VALUES (10, 'compro', 13, NULL, NULL);
INSERT INTO blocked_items VALUES (11, 'free', 14, NULL, NULL);

ALTER SEQUENCE blocked_items_item_id_seq RESTART WITH 90;

