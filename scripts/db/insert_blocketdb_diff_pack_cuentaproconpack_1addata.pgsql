INSERT INTO users (user_id, uid, email, account, paid_total) VALUES (62, 56, 'cuentaproconpack@yapo.cl', 0, 0);

INSERT INTO accounts (account_id, name, rut, email, phone, is_company, region, salted_passwd, creation_date, activation_date, status, address, contact, lob, commune, user_id) VALUES (10, 'cuentaproconpack', '17324400-2', 'cuentaproconpack@yapo.cl', '67867876', true, 15, '$1024$g$wt>iy8b-x5cf[!cbb7e993ffd7e885227f96dd4ff382f416e36d6b', '2015-03-09 18:13:53.72158', NULL, 'active', NULL, NULL, NULL, NULL, 62);

INSERT INTO account_params (account_id, name, value) VALUES (10, 'is_pro_for', '2020');
INSERT INTO account_params (account_id, name, value) VALUES (10, 'pack_type', 'car');

SELECT pg_catalog.setval('accounts_account_id_seq', 10, true);

INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (134, 8000093, '2015-03-09 18:19:41', 'active', 'sell', 'cuentaproconpack', '67867876', 15, 0, 2020, 62, NULL, false, false, true, 'Audi A4 2011 ', 'aviso para Luis', 11399000, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$g$wt>iy8b-x5cf[!cbb7e993ffd7e885227f96dd4ff382f416e36d6b', '2015-03-09 18:19:41.039424', 'es');

INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (174, '816162415', 'verified', '2015-03-09 18:18:37.743266', NULL);

INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (134, 1, 'new', 745, 'accepted', 'pack', NULL, NULL, 174);

INSERT INTO action_params (ad_id, action_id, name, value) VALUES (134, 1, 'source', 'msite');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (134, 1, 'redir', 'dW5rbm93bg==');


INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (134, 1, 742, 'reg', 'initial', '2015-03-09 18:18:37.743266', '10.0.1.101', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (134, 1, 743, 'unverified', 'verifymail', '2015-03-09 18:18:37.743266', '10.0.1.101', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (134, 1, 744, 'pending_review', 'verify', '2015-03-09 18:18:37.805994', '10.0.1.101', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (134, 1, 745, 'accepted', 'accept', '2015-03-09 18:19:41.039424', '10.0.1.101', NULL);

SELECT pg_catalog.setval('action_states_state_id_seq', 745, true);

INSERT INTO ad_changes (ad_id, action_id, state_id, is_param, column_name, old_value, new_value) VALUES (134, 1, 743, true, 'pack_status', NULL, 'active');

INSERT INTO ad_codes (code, code_type) VALUES (10925, 'pay');

INSERT INTO ad_image_changes (ad_id, action_id, state_id, seq_no, name, is_new) VALUES (134, 1, 745, 0, NULL, false);

INSERT INTO ad_params (ad_id, name, value) VALUES (134, 'regdate', '2011');
INSERT INTO ad_params (ad_id, name, value) VALUES (134, 'mileage', '10');
INSERT INTO ad_params (ad_id, name, value) VALUES (134, 'gearbox', '1');
INSERT INTO ad_params (ad_id, name, value) VALUES (134, 'fuel', '2');
INSERT INTO ad_params (ad_id, name, value) VALUES (134, 'cartype', '1');
INSERT INTO ad_params (ad_id, name, value) VALUES (134, 'brand', '8');
INSERT INTO ad_params (ad_id, name, value) VALUES (134, 'model', '6');
INSERT INTO ad_params (ad_id, name, value) VALUES (134, 'version', '12');
INSERT INTO ad_params (ad_id, name, value) VALUES (134, 'country', 'UNK');
INSERT INTO ad_params (ad_id, name, value) VALUES (134, 'pack_status', 'active');

SELECT pg_catalog.setval('ads_ad_id_seq', 134, true);
SELECT pg_catalog.setval('list_id_seq', 8000093, true);

INSERT INTO packs (pack_id, account_id, status, type, slots, used, date_start, date_end, product_id) VALUES (2, 10, 'active', 'car', 10, 1, NOW() - INTERVAL '31 days', NOW() - INTERVAL '1 day', 11010);

SELECT pg_catalog.setval('pay_code_seq', 475, true);

INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (229, 'save', 0, '816162415', NULL, '2015-03-09 18:18:38', 174, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (230, 'verify', 0, '816162415', NULL, '2015-03-09 18:18:38', 174, 'OK');

SELECT pg_catalog.setval('pay_log_pay_log_id_seq', 230, true);

INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (229, 0, 'adphone', '67867876');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (229, 1, 'remote_addr', '10.0.1.101');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (230, 0, 'remote_addr', '10.0.1.101');

SELECT pg_catalog.setval('payment_groups_payment_group_id_seq', 174, true);

INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (174, 'ad_action', 0, 0);

INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (134, 1, 745, 'filter_name', 'pack');

SELECT pg_catalog.setval('tokens_token_id_seq', 766, true);
SELECT pg_catalog.setval('trans_queue_id_seq', 103, true);
SELECT pg_catalog.setval('uid_seq', 56, true);

INSERT INTO user_params (user_id, name, value) VALUES (62, 'first_approved_ad', '2015-03-09 18:19:41.039424-03');
INSERT INTO user_params (user_id, name, value) VALUES (62, 'first_inserted_ad', '2015-03-09 18:19:41.039424-03');

SELECT pg_catalog.setval('users_user_id_seq', 62, true);

--Change all passwords to 123123123
update ads set salted_passwd =  E'$1024$2y1bQ}VYRD}zF4QCbf07a971b784888658a20e62156de64dfde5ffb6', passwd = null;
update accounts set salted_passwd =  E'$1024$2y1bQ}VYRD}zF4QCbf07a971b784888658a20e62156de64dfde5ffb6';
