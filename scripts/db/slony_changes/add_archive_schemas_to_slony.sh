#!/bin/bash
#
# BhEaN(2013)
# Schibsted Iberica
#
# Run this script as postgres on the master node to add all the archive schemas to the slony cluster
# args: nothing
#
# The results of this script should be redirected to SLONIK with:
#   ./add_archive_schemas_to_slony.sh | slonik
# or
#  ././add_archive_schemas_to_slony.sh > /tmp/add_archive_schemas.slonik && cat /tmp/add_archive_schemas.slonik | slonik
#

# Check user
if [ $(whoami) != "postgres" ] ; then
	echo 'Only user postgres can execute this script!'
	echo
	exit 1;
fi

DBNAME="blocketdb"
TIMEOUT=0;
CLUSTERNAME="replication"
SQLCONECTION="psql -U postgres --no-align --tuples-only $DBNAME"

# Get slony servers
SLONY_NODES=$($SQLCONECTION -c "SELECT replace(pa_conninfo, E'\ ', '_SPACE_') FROM _$CLUSTERNAME.sl_path ORDER BY pa_server ASC;" 2> /dev/null);
if [ $? -ne 0 ]; then
        echo "ERROR: Cannot get slony servers from DB (conection error?)"
	echo
	exit 1
fi

# Get max ID set in the cluster
SET_ID=$($SQLCONECTION -c "SELECT MAX(set_id) FROM _$CLUSTERNAME.sl_set;");
if [ $? -ne 0 ]; then
        echo "ERROR: Cannot get max ID set in the slony cluster from DB (conection error?)"
	echo
	exit 1
fi
# Get max ID table in the cluster
TABLE_ID=$($SQLCONECTION -c "SELECT MAX(tab_id) FROM _$CLUSTERNAME.sl_table;");
if [ $? -ne 0 ]; then
        echo "ERROR: Cannot get max ID table in the slony cluster from DB (conection error?)"
	echo
	exit 1
fi
# Get max ID sequence in the cluster
SEQUENCE_ID=$($SQLCONECTION -c "SELECT MAX(seq_id) FROM _$CLUSTERNAME.sl_sequence;");
if [ $? -ne 0 ]; then
        echo "ERROR: Cannot get max ID sequence in the slony cluster from DB (conection error?)"
	echo
	exit 1
fi

#echo "Main database: $DBNAME"
#echo "Slony replication cluster name: $CLUSTERNAME"
#echo "Slony nodes: $SLONY_NODES"
#echo "Current maximum slony set ID: $MAX_SET_ID"
#exit

echo "cluster name = $CLUSTERNAME;"

# Prepare nodes of the slonmy cluster
COUNT_NODES=1
for slony_node in $SLONY_NODES; do
	slony_node=${slony_node//_SPACE_/ }
	echo "  node $COUNT_NODES admin conninfo='$slony_node';"
	let COUNT_NODES=COUNT_NODES+1
done

# Get the list of archive schemas
SCHEMAS=$($SQLCONECTION -c "SELECT schema_name FROM information_schema.schemata WHERE schema_name LIKE 'blocket_2%' order by schema_name;");
if [ $? -ne 0 ]; then
        echo "ERROR: Cannot get the schemas of the slony cluster from DB (conection error?)"
	echo
	exit 1
fi

for schema in $SCHEMAS; do

	echo 
	echo "echo '> Schema: $schema';";

	# Check if the schema is already in the slony cluster
	ALREADY_IN_SLONY=$($SQLCONECTION -c "SELECT replace(tab_nspname, E'\ ', '_SPACE_') FROM _$CLUSTERNAME.sl_table WHERE tab_nspname = '$schema' LIMIT 1;");
	if [ $? -ne 0 ]; then
		echo "ERROR: Cannot check if the schema [$schema] already exists in the DB cluster (conection error?)";
		echo
		exit 1
	fi

	if [ "$ALREADY_IN_SLONY" == "$schema" ]; then
		echo "echo '  Skiping schema [$schema] because already exists in the DB cluster';";
	else

		echo "echo '  This can take a veeeery long time if the tables are bigs... so please, be patient! (the timeout is set to $TIMEOUT)';";
		echo 

	        # Create a new replication set
		let SET_ID=SET_ID+1

		echo "echo '  Creating new replication set (ID [$SET_ID])...';";

		echo "create set (id=$SET_ID, origin=1, comment='Replication set for archive $schema');"
		echo "wait for event (origin = ALL, confirmed = ALL, timeout = $TIMEOUT, wait on = 1);"
		echo

		# Get the tables of the current schema
		TABLES=$($SQLCONECTION -c "SELECT table_name FROM information_schema.tables WHERE table_catalog = '$DBNAME' and table_schema = '$schema' ORDER BY table_name;");
		if [ $? -ne 0 ]; then
		        echo "ERROR: Cannot get the tables of the schema $schema from DB (conection error?)"
			echo
			exit 1
		fi	

		for table in $TABLES; do
	
			echo "echo '  Adding table [$table] to the replication schema...';";

			# Add the tables to the slony
			let TABLE_ID=TABLE_ID+1
			echo "set add table (set id=$SET_ID, origin=1, id=$TABLE_ID, fully qualified name = '$schema.$table', comment='Table $schema.$table');"
			echo "wait for event (origin = ALL, confirmed = ALL, timeout = $TIMEOUT, wait on = 1);"
	
		done

		# Get the sequences of the current schema
		SEQUENCES=$($SQLCONECTION -c "SELECT sequence_name FROM information_schema.sequences WHERE sequence_catalog = '$DBNAME' and sequence_schema = '$schema' ORDER BY sequence_name;");
		if [ $? -ne 0 ]; then
		        echo "ERROR: Cannot get the sequences of the schema $schema from DB (conection error?)"
			echo
			exit 1
		fi	

		for sequence in $SEQUENCES; do

			echo "echo '  Adding sequence [$sequence] to the replication schema...';";

			# Add the sequence to te slony
			let SEQUENCE_ID=SEQUENCE_ID+1
			echo "set add sequence (set id=$SET_ID, origin=1, id=$SEQUENCE_ID, fully qualified name = '$schema.$sequence', comment='Sequence $schema.$sequence');"
			echo "wait for event (origin = ALL, confirmed = ALL, timeout = $TIMEOUT, wait on = 1);"
	
		done

		echo "echo '  Subscribing set to the cluster...';";

		echo
		echo "subscribe set (id = $SET_ID, provider = 1, receiver = 2, forward = yes);"
		echo "wait for event (origin = ALL, confirmed = ALL, timeout = $TIMEOUT, wait on = 1);"
	fi;

done

echo
echo "echo 'Process finished succesfully!';";
echo
