#!/bin/bash
#
# BhEaN(2013)
# Schibsted Iberica
#
# Run this script as postgres on the master node to add all the archive schemas to the slony cluster
# args: nothing
#

# Check user
if [ $(whoami) != "postgres" ] ; then
	echo 'Only user postgres can execute this script!'
	echo
	exit 1;
fi

DBNAME="blocketdb"
TIMEOUT=0;
CLUSTERNAME="replication"
SQLCONECTION="psql -U postgres --no-align --tuples-only $DBNAME"

TABLES="purchase purchase_detail purchase_states"
SEQUENCES="purchase_purchase_id_seq purchase_detail_purchase_detail_id_seq purchase_states_purchase_state_id_seq"

# Get slony servers
SLONY_NODES=$($SQLCONECTION -c "SELECT replace(pa_conninfo, E'\ ', '_SPACE_') FROM _$CLUSTERNAME.sl_path ORDER BY pa_server ASC;" 2> /dev/null);
if [ $? -ne 0 ]; then
        echo "ERROR: Cannot get slony servers from DB (conection error?)"
	echo
	exit 1
fi

# Get max ID set in the cluster
SET_ID=$($SQLCONECTION -c "SELECT MAX(set_id) FROM _$CLUSTERNAME.sl_set;");
if [ $? -ne 0 ]; then
        echo "ERROR: Cannot get max ID set in the slony cluster from DB (conection error?)"
	echo
	exit 1
fi
# Get max ID table in the cluster
TABLE_ID=$($SQLCONECTION -c "SELECT MAX(tab_id) FROM _$CLUSTERNAME.sl_table;");
if [ $? -ne 0 ]; then
        echo "ERROR: Cannot get max ID table in the slony cluster from DB (conection error?)"
	echo
	exit 1
fi
# Get max ID sequence in the cluster
SEQUENCE_ID=$($SQLCONECTION -c "SELECT MAX(seq_id) FROM _$CLUSTERNAME.sl_sequence;");
if [ $? -ne 0 ]; then
        echo "ERROR: Cannot get max ID sequence in the slony cluster from DB (conection error?)"
	echo
	exit 1
fi

#echo "Main database: $DBNAME"
#echo "Slony replication cluster name: $CLUSTERNAME"
#echo "Slony nodes: $SLONY_NODES"
#echo "Current maximum slony set ID: $MAX_SET_ID"
#exit

echo "cluster name = $CLUSTERNAME;"

# Prepare nodes of the slonmy cluster
COUNT_NODES=1
for slony_node in $SLONY_NODES; do
	slony_node=${slony_node//_SPACE_/ }
	echo "  node $COUNT_NODES admin conninfo='$slony_node';"
	let COUNT_NODES=COUNT_NODES+1
done

echo "echo '  This can take a veeeery long time if the tables are bigs... so please, be patient! (the timeout is set to $TIMEOUT)';";
echo 

# Create a new replication set
let SET_ID=SET_ID+1

echo "echo '  Creating new replication set (ID [$SET_ID])...';";

echo "create set (id=$SET_ID, origin=1, comment='Replication set for cardata tables');"
echo "wait for event (origin = ALL, confirmed = ALL, timeout = $TIMEOUT, wait on = 1);"
echo

for table in $TABLES; do

	echo "echo '  Adding table [$table] to the replication ...';";

	# Add the tables to the slony
	let TABLE_ID=TABLE_ID+1
	echo "set add table (set id=$SET_ID, origin=1, id=$TABLE_ID, fully qualified name = 'public.$table', comment='Table public.$table');"
	echo "wait for event (origin = ALL, confirmed = ALL, timeout = $TIMEOUT, wait on = 1);"
	
done

echo 

for sequence in $SEQUENCES; do

	echo "echo '  Adding sequence [$sequence] to the replication ...';";

	# Add the sequence to te slony
	let SEQUENCE_ID=SEQUENCE_ID+1
	echo "set add sequence (set id=$SET_ID, origin=1, id=$SEQUENCE_ID, fully qualified name = 'public.$sequence', comment='Sequence public.$sequence');"
	echo "wait for event (origin = ALL, confirmed = ALL, timeout = $TIMEOUT, wait on = 1);"
	
done

echo
echo "echo '  Subscribing set to the cluster...';";

echo
echo "subscribe set (id = $SET_ID, provider = 1, receiver = 2, forward = yes);"
echo "wait for event (origin = ALL, confirmed = ALL, timeout = $TIMEOUT, wait on = 1);"

echo
echo "echo 'Process finished succesfully!';";
echo
