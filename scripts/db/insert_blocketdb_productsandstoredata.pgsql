UPDATE users SET (user_id, uid, email, account, paid_total) = (57, 51, 'many@ads.cl', 0, 3990) WHERE user_id = 57;
UPDATE accounts SET (account_id, name, rut, email, phone, is_company, region, salted_passwd, creation_date, activation_date, status, address, contact, lob, commune, user_id) = (6, 'ManyAdsAccount', '12312312-3', 'many@ads.cl', '12312312', true, 15, '$1024$2y1bQ}VYRD}zF4QCbf07a971b784888658a20e62156de64dfde5ffb6', '2014-01-14 16:00:35.786299', NULL, 'active', NULL, NULL, NULL, NULL, 57) WHERE account_id = 6;
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (126, 8000085, '2014-11-06 19:23:50.113648', 'active', 'sell', 'ManyAdsAccountPro', '12312312', 15, 0, 5020, 57, NULL, false, false, true, 'Aviso Pro', 'super pro', 123123, NULL, NULL, NULL, '2014-11-06 19:22:56', NULL, NULL, '$1024$?GwUK#I6g8+M8*1t3c3ce592ffe8a96552cb33ee1916f9b9d16daf2e', '2014-11-06 19:23:50.113648', 'es');
UPDATE ads SET (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) = (100, 8000078, '2014-11-06 19:23:50.139865', 'active', 'sell', 'ManyAdsAccount', '12312312', 15, 0, 8020, 57, NULL, false, false, false, 'Aviso de prueba 5', 'Esto es la descripción del aviso de prueba 5', 5000, NULL, NULL, NULL, '2014-01-15 10:44:46', NULL, NULL, '$1024$?GwUK#I6g8+M8*1t3c3ce592ffe8a96552cb33ee1916f9b9d16daf2e', '2014-11-06 19:23:50.139865', 'es') WHERE ad_id = 100;
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (166, '633026231', 'verified', '2014-11-06 19:22:19.214099', NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (167, '12345', 'paid', '2014-11-06 19:23:46.953789', NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (168, '65432', 'paid', '2014-11-06 19:23:46.953789', 167);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (169, '32323', 'paid', '2014-11-06 19:23:46.953789', 167);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (170, '54545', 'paid', '2014-11-06 19:24:38.882037', NULL);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (126, 1, 'new', 701, 'accepted', 'normal', NULL, NULL, 166);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (126, 2, 'bump', 708, 'accepted', 'normal', NULL, NULL, 168);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (100, 2, 'weekly_bump', 709, 'accepted', 'normal', NULL, NULL, 169);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (100, 3, 'bump', 711, 'accepted', 'normal', NULL, NULL, NULL);
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (126, 1, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (126, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO stores (store_id, name, phone, address, info_text, status, date_start, date_end, region, url, hide_phone, commune, address_number, website_url, main_image, main_image_offset, logo_image, account_id) VALUES (1, NULL, NULL, NULL, NULL, 'inactive', '2014-11-06 19:24:38.853784', '2014-11-06 19:39:41.858208', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (715, 'X545bf4b053edd7cd000000006ac0238d00000000', 9, '2014-11-06 19:22:40.375046', '2014-11-06 19:22:40.524778', '10.0.1.133', 'authenticate', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (716, 'X545bf4b15c290b79000000002f60c71f00000000', 9, '2014-11-06 19:22:40.524778', '2014-11-06 19:22:42.509224', '10.0.1.133', 'spamfilter', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (717, 'X545bf4b35eae628900000000202d7e3800000000', 9, '2014-11-06 19:22:42.509224', '2014-11-06 19:22:49.776005', '10.0.1.133', 'listadmins', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (718, 'X545bf4ba246c7d3d0000000020e46bb900000000', 9, '2014-11-06 19:22:49.776005', '2014-11-06 19:22:49.795526', '10.0.1.133', 'listadmins', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (719, 'X545bf4ba765d6af70000000059c61f8400000000', 9, '2014-11-06 19:22:49.795526', '2014-11-06 19:22:53.823304', '10.0.1.133', 'search_ads', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (720, 'X545bf4be5315ff710000000071cb6ee500000000', 9, '2014-11-06 19:22:53.823304', '2014-11-06 19:22:53.941829', '10.0.1.133', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (721, 'X545bf4be47e1d7eb000000006d0ea9e200000000', 9, '2014-11-06 19:22:53.941829', '2014-11-06 19:22:53.950922', '10.0.1.133', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (722, 'X545bf4be66abf3e30000000012c3301300000000', 9, '2014-11-06 19:22:53.950922', '2014-11-06 19:22:53.959399', '10.0.1.133', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (723, 'X545bf4be42a7997d000000003988e03100000000', 9, '2014-11-06 19:22:53.959399', '2014-11-06 19:22:56.964597', '10.0.1.133', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (724, 'X545bf4c11fbebe4000000006e95b0f700000000', 9, '2014-11-06 19:22:56.964597', '2014-11-06 20:22:56.964597', '10.0.1.133', 'review', NULL, NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (126, 1, 698, 'reg', 'initial', '2014-11-06 19:22:19.214099', '10.0.1.133', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (126, 1, 699, 'unverified', 'verifymail', '2014-11-06 19:22:19.214099', '10.0.1.133', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (126, 1, 700, 'pending_review', 'verify', '2014-11-06 19:22:19.286994', '10.0.1.133', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (126, 1, 701, 'accepted', 'accept', '2014-11-06 19:22:56.972387', '10.0.1.133', 723);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (126, 2, 702, 'reg', 'initial', '2014-11-06 19:23:46.953789', '10.0.1.133', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (126, 2, 703, 'unpaid', 'pending_pay', '2014-11-06 19:23:46.953789', '10.0.1.133', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (100, 2, 704, 'reg', 'initial', '2014-11-06 19:23:46.953789', '10.0.1.133', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (100, 2, 705, 'unpaid', 'pending_pay', '2014-11-06 19:23:46.953789', '10.0.1.133', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (126, 2, 706, 'pending_bump', 'pay', '2014-11-06 19:23:49.980339', NULL, NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (100, 2, 707, 'accepted', 'pay', '2014-11-06 19:23:50.008879', NULL, NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (126, 2, 708, 'accepted', 'bump', '2014-11-06 19:23:50.113648', NULL, NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (100, 2, 709, 'accepted', 'updating_counter', '2014-11-06 19:23:50.128508', NULL, NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (100, 3, 710, 'reg', 'initial', '2014-11-06 19:23:50.139865', NULL, NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (100, 3, 711, 'accepted', 'bump', '2014-11-06 19:23:50.139865', NULL, NULL);
SELECT pg_catalog.setval('action_states_state_id_seq', 711, true);
INSERT INTO ad_changes (ad_id, action_id, state_id, is_param, column_name, old_value, new_value) VALUES (100, 2, 709, true, 'weekly_bump', NULL, '3');
INSERT INTO ad_codes (code, code_type) VALUES (24741, 'pay');
INSERT INTO ad_image_changes (ad_id, action_id, state_id, seq_no, name, is_new) VALUES (126, 1, 701, 0, NULL, false);
INSERT INTO ad_params (ad_id, name, value) VALUES (126, 'prev_currency', 'peso');
INSERT INTO ad_params (ad_id, name, value) VALUES (126, 'country', 'UNK');
INSERT INTO ad_params (ad_id, name, value) VALUES (100, 'weekly_bump', '3');
SELECT pg_catalog.setval('ads_ad_id_seq', 126, true);
SELECT pg_catalog.setval('list_id_seq', 8000085, true);
SELECT pg_catalog.setval('pay_code_seq', 467, true);
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (213, 'save', 0, '633026231', NULL, '2014-11-06 19:22:19', 166, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (214, 'verify', 0, '633026231', NULL, '2014-11-06 19:22:19', 166, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (215, 'save', 0, '65432', NULL, '2014-11-06 19:23:47', 168, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (216, 'save', 0, '32323', NULL, '2014-11-06 19:23:47', 169, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (217, 'paycard_save', 0, '12345', NULL, '2014-11-06 19:23:50', 167, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (218, 'paycard', 1000, '00168a', NULL, '2014-11-06 19:23:50', 168, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (219, 'paycard', 2990, '00169a', NULL, '2014-11-06 19:23:50', 169, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (220, 'paycard', 3990, '12345', NULL, '2014-11-06 19:23:50', 167, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (221, 'save', 0, '54545', NULL, '2014-11-06 19:24:39', 170, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (222, 'paycard_save', 0, '54545', NULL, '2014-11-06 19:24:42', 170, 'SAVE');
SELECT pg_catalog.setval('pay_log_pay_log_id_seq', 222, true);
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (213, 0, 'adphone', '12312312');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (213, 1, 'remote_addr', '10.0.1.133');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (214, 0, 'remote_addr', '10.0.1.133');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (215, 0, 'remote_addr', '10.0.1.133');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (216, 0, 'remote_addr', '10.0.1.133');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (217, 0, 'auth_code', '457246');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (217, 1, 'trans_date', '06/11/2014 19:23:49');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (217, 2, 'placements_type', 'VN');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (217, 3, 'placements_number', '1');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (217, 4, 'pay_type', 'credit');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (217, 5, 'external_response', '0');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (217, 6, 'external_id', '51038988763639838300');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (217, 7, 'remote_addr', '10.0.1.208');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (218, 0, 'webpay', '1');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (219, 0, 'webpay', '1');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (221, 0, 'remote_addr', '10.0.1.133');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (222, 0, 'auth_code', '492354');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (222, 1, 'trans_date', '06/11/2014 19:24:41');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (222, 2, 'placements_type', 'VN');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (222, 3, 'placements_number', '1');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (222, 4, 'pay_type', 'credit');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (222, 5, 'external_response', '0');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (222, 6, 'external_id', '73125377752343713733');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (222, 7, 'remote_addr', '10.0.1.208');
SELECT pg_catalog.setval('payment_groups_payment_group_id_seq', 170, true);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (166, 'ad_action', 0, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (168, 'bump', 1000, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (169, 'weekly_bump', 2990, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (170, 'biannual_store', 27000, 0);
INSERT INTO purchase (purchase_id, doc_type, doc_num, external_doc_id, status, receipt, delivery, rut, name, lob, address, region, communes, contact, email, tax, discount, seller_id, total_price, payment_group_id, payment_platform) VALUES (2, 'bill', NULL, NULL, 'paid', '2014-11-06 19:23:46.953789', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'many@ads.cl', 0, 0, NULL, 3990, 167, 'desktop');
INSERT INTO purchase (purchase_id, doc_type, doc_num, external_doc_id, status, receipt, delivery, rut, name, lob, address, region, communes, contact, email, tax, discount, seller_id, total_price, payment_group_id, payment_platform) VALUES (3, 'invoice', NULL, NULL, 'paid', '2014-11-06 19:24:38.882037', NULL, '12312312-3', 'ManyAdsAccount', 'Giru Giru', 'ManyHouses', 15, 302, '', 'many@ads.cl', 0, 0, NULL, 27000, 170, 'msite');
INSERT INTO purchase_detail (purchase_detail_id, purchase_id, product_id, price, action_id, ad_id) VALUES (2, 2, 1, 1000, 2, 126);
INSERT INTO purchase_detail (purchase_detail_id, purchase_id, product_id, price, action_id, ad_id) VALUES (3, 2, 2, 2990, 2, 100);
INSERT INTO purchase_detail (purchase_detail_id, purchase_id, product_id, price, action_id, ad_id) VALUES (4, 3, 6, 27000, NULL, NULL);
SELECT pg_catalog.setval('purchase_detail_purchase_detail_id_seq', 4, true);
SELECT pg_catalog.setval('purchase_purchase_id_seq', 3, true);
INSERT INTO purchase_states (purchase_id, purchase_state_id, status, "timestamp", remote_addr) VALUES (2, 1, 'pending', '2014-11-06 19:23:46.953789', '10.0.1.133');
INSERT INTO purchase_states (purchase_id, purchase_state_id, status, "timestamp", remote_addr) VALUES (3, 2, 'pending', '2014-11-06 19:24:38.882037', '10.0.1.133');
SELECT pg_catalog.setval('purchase_states_purchase_state_id_seq', 2, true);
INSERT INTO review_log (ad_id, action_id, admin_id, review_time, queue, action_type, action, refusal_reason_text, category) VALUES (126, 1, 9, '2014-11-06 19:22:56.972387', 'all', 'new', 'accepted', NULL, 5020);
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (126, 1, 701, 'filter_name', 'all');
INSERT INTO store_actions (store_id, action_id, action_type, current_state, state, payment_group_id) VALUES (1, 1, 'new', 1, 'reg', NULL);
INSERT INTO store_actions (store_id, action_id, action_type, current_state, state, payment_group_id) VALUES (1, 2, 'new', 2, 'unpaid', NULL);
INSERT INTO store_actions (store_id, action_id, action_type, current_state, state, payment_group_id) VALUES (1, 4, 'status_change', 5, 'accepted', NULL);
INSERT INTO store_actions (store_id, action_id, action_type, current_state, state, payment_group_id) VALUES (1, 3, 'extend', 7, 'accepted', 170);
INSERT INTO store_action_states (store_id, action_id, state_id, state, "timestamp", token_id) VALUES (1, 1, 1, 'reg', '2014-11-06 19:24:38.853784', NULL);
INSERT INTO store_action_states (store_id, action_id, state_id, state, "timestamp", token_id) VALUES (1, 2, 2, 'unpaid', '2014-11-06 19:24:38.853784', NULL);
INSERT INTO store_action_states (store_id, action_id, state_id, state, "timestamp", token_id) VALUES (1, 3, 3, 'unpaid', '2014-11-06 19:24:38.882037', NULL);
INSERT INTO store_action_states (store_id, action_id, state_id, state, "timestamp", token_id) VALUES (1, 4, 4, 'reg', '2014-11-06 19:24:41.858208', NULL);
INSERT INTO store_action_states (store_id, action_id, state_id, state, "timestamp", token_id) VALUES (1, 4, 5, 'accepted', '2014-11-06 19:24:41.858208', NULL);
INSERT INTO store_action_states (store_id, action_id, state_id, state, "timestamp", token_id) VALUES (1, 3, 6, 'paid', '2014-11-06 19:24:41.858208', NULL);
INSERT INTO store_action_states (store_id, action_id, state_id, state, "timestamp", token_id) VALUES (1, 3, 7, 'accepted', '2014-11-06 19:24:41.858208', NULL);
SELECT pg_catalog.setval('store_action_states_state_id_seq', 7, true);
INSERT INTO store_changes (store_id, action_id, state_id, is_param, column_name, old_value, new_value) VALUES (1, 4, 4, false, 'status', 'pending', 'inactive');
INSERT INTO store_changes (store_id, action_id, state_id, is_param, column_name, old_value, new_value) VALUES (1, 3, 3, false, 'date_end', NULL, '2014-11-06 19:39:41.858208');
INSERT INTO store_changes (store_id, action_id, state_id, is_param, column_name, old_value, new_value) VALUES (1, 3, 3, true, 'store_type', NULL, '6');
INSERT INTO store_params (store_id, name, value) VALUES (1, 'store_type', '6');
SELECT pg_catalog.setval('stores_store_id_seq', 1, true);
SELECT pg_catalog.setval('tokens_token_id_seq', 724, true);
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (93, '2014-11-06 19:22:57.017817', '29374@mazaru.schibsted.cl', NULL, '2014-11-06 19:23:05.714434', '29374@mazaru.schibsted.cl', 'TRANS_OK', '2014-11-06 19:23:05.633698', NULL, 'cmd:admail
commit:1
ad_id:126
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (94, '2014-11-06 19:23:50.14122', '29374@mazaru.schibsted.cl', '2014-11-06 19:24:50.14122', NULL, NULL, NULL, NULL, NULL, 'cmd:weekly_bump_ad
counter:3
ad_id:100
batch:1
commit:1
', NULL, 'weekly_bump', NULL, NULL);
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (95, '2014-11-06 19:23:50.148318', '29374@mazaru.schibsted.cl', NULL, NULL, NULL, NULL, NULL, NULL, '
cmd:multi_product_mail
to:many@ads.cl
doc_type:bill
product_ids:1|2
ad_names:Aviso Pro|Aviso de prueba 5
commit:1', NULL, 'bump_mail', NULL, NULL);
SELECT pg_catalog.setval('trans_queue_id_seq', 95, true);
