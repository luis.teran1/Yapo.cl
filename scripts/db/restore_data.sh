#!/bin/bash

test -z "$1" && exit 1
test -z "$2" && exit 1
PGDIR=`cd $1; pwd`
# Restore the snapshot
pg_restore -c -h $PGDIR -d blocketdb $2.dmp >.sql.log  2>&1

# Check result
res=$?

# Dump result in case of error
if [ $res != 0 ] ; then
  cat .sql.log
  echo Error in file $2.dmp
  exit $res
fi

