#!/bin/bash

if [ -n "$TOPDIR" ] ; then
   PGDIR=$TOPDIR/pgsql
else
   PGDIR=pgsql
fi
export PGDIR

test -z "$1" && exit 1
test "${1%data*}" = "$1" && exit 1
cd $1 || exit 1
PGDATA=`pwd`
cd -
export PGDATA

DSNARGS="-h $PGDATA"

rm -f .psql.stderr
# trans plsqlprocs
for proc in plsql/*.sql
do
	echo "FILE $proc" >> .psql.stderr
	env PGOPTIONS="-c search_path=$BPVSCHEMA,public" /usr/bin/psql $DSNARGS blocketdb < $proc > /dev/null 2>> .psql.stderr
done

rm -f .psql.stderr
