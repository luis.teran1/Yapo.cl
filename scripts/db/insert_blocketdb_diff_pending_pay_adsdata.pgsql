INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (715, 'X5c82805c2a37b26300000000703b26a200000000', 9, '2019-03-08 11:46:52.43593', '2019-03-08 11:46:52.66691', '172.24.0.21', 'authenticate', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (716, 'X5c82805df9ed54c000000006887c1de00000000', 9, '2019-03-08 11:46:52.66691', '2019-03-08 11:46:55.012481', '172.24.0.21', 'spamfilter', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (717, 'X5c82805f1dc61517000000004c7921d500000000', 9, '2019-03-08 11:46:55.012481', '2019-03-08 11:46:55.169554', '172.24.0.21', 'listadmins', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (718, 'X5c82805f2f23aa8300000000500debb500000000', 9, '2019-03-08 11:46:55.169554', '2019-03-08 11:47:01.142152', '172.24.0.21', 'listadmins', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (719, 'X5c82806560c5c1e7000000006fea794800000000', 9, '2019-03-08 11:47:01.142152', '2019-03-08 11:47:01.15651', '172.24.0.21', 'listadmins', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (720, 'X5c828065c2003b80000000047e817f100000000', 9, '2019-03-08 11:47:01.15651', '2019-03-08 11:47:08.954601', '172.24.0.21', 'search_ads', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (721, 'X5c82806d6b598c5000000000a553a3c00000000', 9, '2019-03-08 11:47:08.954601', '2019-03-08 11:47:08.966439', '172.24.0.21', 'listadmins', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (722, 'X5c82806d22252daa000000006635426900000000', 9, '2019-03-08 11:47:08.966439', '2019-03-08 11:47:14.110357', '172.24.0.21', 'search_ads', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (723, 'X5c828072145fd50b000000001b746a5700000000', 9, '2019-03-08 11:47:14.110357', '2019-03-08 11:47:14.216784', '172.24.0.21', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (724, 'X5c82807242ffaf17000000003387b45a00000000', 9, '2019-03-08 11:47:14.216784', '2019-03-08 11:47:14.224905', '172.24.0.21', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (725, 'X5c82807252aad439000000007d9edf0c00000000', 9, '2019-03-08 11:47:14.224905', '2019-03-08 11:47:14.232292', '172.24.0.21', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (726, 'X5c8280725d2bcc200000000020de5d4c00000000', 9, '2019-03-08 11:47:14.232292', '2019-03-08 11:47:17.460573', '172.24.0.21', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (727, 'X5c82807526b090a0000000073448abe00000000', 9, '2019-03-08 11:47:17.460573', '2019-03-08 11:47:22.700835', '172.24.0.21', 'review', NULL, NULL);

INSERT INTO account_actions (account_id, account_action_id, account_action_type, "timestamp", token_id) VALUES (6, 1, 'edition', '2019-03-08 11:46:35.630941', NULL);

INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (126, 8000085, '2019-03-08 11:47:17', 'deleted', 'sell', 'ManyAdsAccount', '987654321', 15, 0, 3060, 57, NULL, false, false, false, 'Telefono', 'asndionasodi', 120000, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$2y1bQ}VYRD}zF4QCbf07a971b784888658a20e62156de64dfde5ffb6', '2018-03-08 11:47:17.465822', 'es');

INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (166, '1410134242', 'verified', '2019-03-08 11:46:35.476052', NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (167, '12345', 'unverified', '2019-03-08 11:49:04.105187', NULL);

INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (126, 1, 'new', 701, 'accepted', 'normal', NULL, NULL, 166);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (126, 2, 'autobump', 703, 'unpaid', 'normal', NULL, NULL, 167);

INSERT INTO action_params (ad_id, action_id, name, value) VALUES (126, 1, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (126, 1, 'redir', 'dW5rbm93bg==');

INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (126, 1, 698, 'reg', 'initial', '2018-03-08 11:46:35.476052', '172.24.0.21', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (126, 1, 699, 'unverified', 'verifymail', '2018-03-08 11:46:35.476052', '172.24.0.21', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (126, 1, 700, 'pending_review', 'verify', '2018-03-08 11:46:35.705671', '172.24.0.21', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (126, 1, 701, 'accepted', 'accept', '2018-03-08 11:47:17.465822', '172.24.0.21', 726);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (126, 2, 702, 'reg', 'initial', '2018-03-08 11:49:04.105187', '172.24.0.21', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (126, 2, 703, 'unpaid', 'pending_pay', '2018-03-08 11:49:04.105187', '172.24.0.21', NULL);

INSERT INTO ad_codes (code, code_type) VALUES (24741, 'pay');

INSERT INTO ad_image_changes (ad_id, action_id, state_id, seq_no, name, is_new) VALUES (126, 1, 701, 0, NULL, false);

INSERT INTO ad_params (ad_id, name, value) VALUES (126, 'communes', '315');
INSERT INTO ad_params (ad_id, name, value) VALUES (126, 'currency', 'peso');
INSERT INTO ad_params (ad_id, name, value) VALUES (126, 'internal_memory', '220');
INSERT INTO ad_params (ad_id, name, value) VALUES (126, 'geoposition_is_precise', '0');
INSERT INTO ad_params (ad_id, name, value) VALUES (126, 'country', 'UNK');

INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (213, 'save', 0, '1410134242', NULL, '2019-03-08 11:46:35', 166, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (214, 'verify', 0, '1410134242', NULL, '2019-03-08 11:46:36', 166, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (215, 'save', 0, '12345', NULL, '2019-03-08 11:49:04', 167, 'SAVE');

INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (213, 0, 'adphone', '987654321');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (213, 1, 'remote_addr', '172.24.0.21');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (214, 0, 'remote_addr', '172.24.0.21');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (215, 0, 'remote_addr', '172.24.0.21');

INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (166, 'ad_action', 0, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (167, 'autobump', 168300, 0);

INSERT INTO purchase (purchase_id, doc_type, doc_num, external_doc_id, status, receipt, delivery, rut, name, lob, address, region, communes, contact, email, tax, discount, seller_id, total_price, payment_group_id, payment_method, payment_platform, account_id) VALUES (2, 'bill', NULL, NULL, 'pending', '2019-03-08 11:49:04.105187', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'many@ads.cl', 0, 0, NULL, 168300, 167, 'webpay', 'desktop', 6);

INSERT INTO purchase_detail (purchase_detail_id, purchase_id, product_id, price, action_id, ad_id, payment_group_id) VALUES (2, 2, 10, 168300, 2, 126, 167);

INSERT INTO purchase_detail_params (purchase_detail_id, name, value) VALUES (2, 'frequency', '2');
INSERT INTO purchase_detail_params (purchase_detail_id, name, value) VALUES (2, 'num_days', '17');
INSERT INTO purchase_detail_params (purchase_detail_id, name, value) VALUES (2, 'use_night', '0');

INSERT INTO purchase_states (purchase_id, purchase_state_id, status, "timestamp", remote_addr) VALUES (2, 1, 'pending', '2019-03-08 11:49:04.105187', '172.24.0.21');

INSERT INTO review_log (ad_id, action_id, admin_id, review_time, queue, action_type, action, refusal_reason_text, category) VALUES (126, 1, 9, '2019-03-08 11:47:17.465822', 'all', 'new', 'accepted', NULL, 3060);

INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (126, 1, 700, 'queue', 'normal');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (126, 1, 701, 'filter_name', 'all');
