--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'LATIN1';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;

SET search_path = public, pg_catalog;


--
-- Data for Name: tokens; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (767, 'X5924a0f5430a66c000000002dc852200000000', 9, '2017-05-23 16:52:04.552143', '2017-05-23 16:52:04.664456', '10.0.10.151', 'authenticate', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (768, 'X5924a0f518ab909a000000006b0d1c1b00000000', 9, '2017-05-23 16:52:04.664456', '2017-05-23 16:52:22.538429', '10.0.10.151', 'spamfilter', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (769, 'X5924a1071a2e3ffd0000000034bdda9600000000', 9, '2017-05-23 16:52:22.538429', '2017-05-23 16:52:37.968101', '10.0.10.151', 'accounts_change_pro_status', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (770, 'X5924a11649bd714200000000480cb90500000000', 9, '2017-05-23 16:52:37.968101', '2017-05-23 16:52:50.93815', '10.0.10.151', 'accounts_change_pro_status', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (771, 'X5924a123111929160000000074cc986d00000000', 23, '2017-05-23 16:52:50.93815', '2017-05-23 16:53:14.223697', '10.0.10.151', 'accounts_change_pro_status', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (772, 'X5924a13a3430fa30000000021aad0a500000000', 24, '2017-05-23 16:53:14.223697', '2017-05-23 16:53:30.929789', '10.0.10.151', 'accounts_change_pro_status', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (773, 'X5924a14b6aaa0dda000000003d32e66200000000', 9, '2017-05-23 16:53:30.929789', '2017-05-23 17:05:49.29097', '10.0.10.151', 'Websql', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (774, 'X5924a42d69286c16000000002bed5b1900000000', 9, '2017-05-23 17:05:49.29097', '2017-05-23 17:08:34.66267', '10.0.10.151', 'Websql', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (775, 'X5924a4d3f88e5d6000000003503b8f800000000', 9, '2017-05-23 17:08:34.66267', '2017-05-23 17:08:36.703684', '10.0.10.151', 'Websql', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (776, 'X5924a4d55aa3591700000000343502dd00000000', 9, '2017-05-23 17:08:36.703684', '2017-05-23 17:08:59.018009', '10.0.10.151', 'Websql', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (777, 'X5924a4eb1e089f9c0000000042f8b0dc00000000', 9, '2017-05-23 17:08:59.018009', '2017-05-23 17:11:07.137344', '10.0.10.151', 'Websql', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (778, 'X5924a56b385e14a3000000004ab1553100000000', 9, '2017-05-23 17:11:07.137344', '2017-05-23 18:11:07.137344', '10.0.10.151', 'Websql', NULL, NULL);


--
-- Name: tokens_token_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('tokens_token_id_seq', 778, true);

--
-- Data for Name: account_actions; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO account_actions (account_id, account_action_id, account_action_type, "timestamp", token_id) VALUES (6, 1, 'add_pro_category_param', '2017-05-23 16:52:22.540302', 768);
INSERT INTO account_actions (account_id, account_action_id, account_action_type, "timestamp", token_id) VALUES (1, 1, 'add_pro_category_param', '2017-05-22 16:52:37.970021', 769);
INSERT INTO account_actions (account_id, account_action_id, account_action_type, "timestamp", token_id) VALUES (1, 2, 'add_pro_category_param', '2017-05-22 16:52:37.970021', 769);
INSERT INTO account_actions (account_id, account_action_id, account_action_type, "timestamp", token_id) VALUES (10, 1, 'add_pro_category_param', '2017-05-23 16:52:50.939999', 770);
INSERT INTO account_actions (account_id, account_action_id, account_action_type, "timestamp", token_id) VALUES (10, 2, 'add_pro_category_param', '2017-05-23 16:52:50.939999', 770);
INSERT INTO account_actions (account_id, account_action_id, account_action_type, "timestamp", token_id) VALUES (5, 1, 'remove_pro_category_param', '2017-05-21 16:53:14.22559', 771);
INSERT INTO account_actions (account_id, account_action_id, account_action_type, "timestamp", token_id) VALUES (5, 2, 'add_pro_category_param', '2017-05-21 16:53:14.22559', 771);

--
-- Data for Name: account_changes; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO account_changes (account_id, account_action_id, is_param, column_name, old_value, new_value) VALUES (1, 1, true, 'pack_type', '', 'car');
INSERT INTO account_changes (account_id, account_action_id, is_param, column_name, old_value, new_value) VALUES (1, 2, true, 'is_pro_for', '', '2020,2040');

INSERT INTO account_changes (account_id, account_action_id, is_param, column_name, old_value, new_value) VALUES (10, 1, true, 'is_pro_for', '2020,2040', '2020,2040,1220,1240');
INSERT INTO account_changes (account_id, account_action_id, is_param, column_name, old_value, new_value) VALUES (10, 2, true, 'is_pro_for', '2020,2040,1220,1240', '2020,2040,1220,1240,7020');

INSERT INTO account_changes (account_id, account_action_id, is_param, column_name, old_value, new_value) VALUES (5, 1, true, 'is_pro_for', '2020,2040', '');
INSERT INTO account_changes (account_id, account_action_id, is_param, column_name, old_value, new_value) VALUES (5, 1, true, 'pack_type', 'car', '');
INSERT INTO account_changes (account_id, account_action_id, is_param, column_name, old_value, new_value) VALUES (5, 2, true, 'pack_type', '', 'real_estate');
INSERT INTO account_changes (account_id, account_action_id, is_param, column_name, old_value, new_value) VALUES (5, 2, true, 'is_pro_for', '', '1220,1240');

INSERT INTO account_changes (account_id, account_action_id, is_param, column_name, old_value, new_value) VALUES (6, 1, true, 'is_pro_for', '', '7020');


--
-- Data for Name: account_params; Type: TABLE DATA; Schema: public; Owner: -
--
delete from account_params;
INSERT INTO account_params (account_id, name, value) VALUES (8, 'is_pro_for', '2020');
INSERT INTO account_params (account_id, name, value) VALUES (8, 'pack_type', 'car');
INSERT INTO account_params (account_id, name, value) VALUES (9, 'is_pro_for', '2020');
INSERT INTO account_params (account_id, name, value) VALUES (9, 'available_slots', '0');
INSERT INTO account_params (account_id, name, value) VALUES (10, 'available_slots', '9');
INSERT INTO account_params (account_id, name, value) VALUES (9, 'pack_type', 'car');
INSERT INTO account_params (account_id, name, value) VALUES (6, 'is_pro_for', '7020');
INSERT INTO account_params (account_id, name, value) VALUES (1, 'is_pro_for', '2020,2040');
INSERT INTO account_params (account_id, name, value) VALUES (1, 'pack_type', 'car');
INSERT INTO account_params (account_id, name, value) VALUES (10, 'is_pro_for', '2020,1220,1240,7020');
INSERT INTO account_params (account_id, name, value) VALUES (10, 'pack_type', 'car');
INSERT INTO account_params (account_id, name, value) VALUES (5, 'is_pro_for', '1220,1240');
INSERT INTO account_params (account_id, name, value) VALUES (5, 'pack_type', 'real_estate');



--
-- PostgreSQL database dump complete
--

