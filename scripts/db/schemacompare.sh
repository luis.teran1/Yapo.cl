#!/bin/bash

if [ -z "$1" -o "$1" = "-h" ] ; then
   echo usage: schemacompare.sh server1 server2 server3
   echo run as user postgres on a server that can access all db:s
   exit 1
fi
dumpdir=/tmp/schc.$$
mkdir $dumpdir || exit 1

for host in $* ; do
pg_dump -s -n public -n blocket_2009  -h $host --no-acl blocketdb | egrep -v 'TRIGGER|_bcluster > $dumpdir/$host.schema
done

echo 'cd '$dumpdir' vimdiff *.schema'
exit 0
