INSERT INTO users (user_id, uid, email, account, paid_total) VALUES (60, 50, 'piroca@salvaje.com', 0, 0);
INSERT INTO users (user_id, uid, email, account, paid_total) VALUES (59, 50, 'kako@suyolito.com', 0, 0);
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (129, NULL, NULL, 'inactive', 'sell', 'Pablo', '123123123', 15, 0, 8020, 59, NULL, false, false, false, 'Test 004', 'asd ads adasda sd', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$m_=3_p^^<mzs!`<kd4662b7baf299cfb5cd028d27ff8aa51d6aba542', NULL, 'es');
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (130, NULL, NULL, 'inactive', 'sell', 'Pablo', '123123123', 15, 0, 8020, 60, NULL, false, false, false, 'Test 005', 'asda dasdas dasd', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$yP%$_LWG"Y}`gxd$c37a4c88c9a2fcb33a35891cddceaf3d5d8b6129', NULL, 'es');
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (131, NULL, NULL, 'inactive', 'sell', 'Pablo', '123123123', 15, 0, 8020, 60, NULL, false, false, false, 'Test 006', 'asd adasda sdasd a', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$<[$Op7!$%zEEXn$P61e986e136ca423f41fde171f47e1861d308f55e', NULL, 'es');
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (127, NULL, NULL, 'refused', 'sell', 'Pablo', '123123123', 15, 0, 8020, 59, NULL, false, false, false, 'Test 002', 'asd asd ad asd asd', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$os6Oz*iIdiU9o0M-a966c7b7204403925d353d95316052fe46769892', NULL, 'es');
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (128, 8000086, '2015-04-06 16:39:29', 'refused', 'sell', 'Pablo', '123123123', 15, 0, 8020, 59, NULL, false, false, false, 'Test 003', 'asd asdasd asd', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$os6Oz*iIdiU9o0M-a966c7b7204403925d353d95316052fe46769892', '2015-04-06 16:39:29.543281', 'es');
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (126, 8000085, '2015-04-06 16:38:22', 'active', 'sell', 'Pablo', '123123123', 15, 0, 8020, 59, NULL, false, false, false, 'Test 001', 'asdasd asdasd sd s
asd asda sd asda sd
asd ada dad asda', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$Ed^4n.*q]9)=:Dnjd474a756b69f5d85ba774fba17e26049081eef28', '2015-04-06 16:42:36.510083', 'es');

INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (166, '633026231', 'verified', '2015-04-06 16:31:53.363461', NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (167, '768418254', 'verified', '2015-04-06 16:32:12.588644', NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (168, '903810277', 'verified', '2015-04-06 16:32:32.206541', NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (169, '139202300', 'verified', '2015-04-06 16:32:51.213136', NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (170, '274594323', 'verified', '2015-04-06 16:37:14.905821', NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (171, '409986346', 'verified', '2015-04-06 16:37:38.939734', NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (172, '545378369', 'verified', '2015-04-06 16:40:20.906506', NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (173, '680770392', 'verified', '2015-04-06 16:42:03.521802', NULL);

INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (130, 1, 'new', 712, 'pending_review', 'abuse', NULL, NULL, 170);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (131, 1, 'new', 715, 'pending_review', 'abuse', NULL, NULL, 171);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (126, 1, 'new', 718, 'accepted', 'normal', NULL, NULL, 166);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (127, 1, 'new', 719, 'refused', 'normal', NULL, NULL, 167);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (128, 1, 'new', 720, 'accepted', 'normal', NULL, NULL, 168);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (128, 2, 'post_refusal', 724, 'refused', 'normal', NULL, NULL, NULL);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (126, 2, 'edit', 725, 'refused', 'edit', NULL, NULL, 172);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (126, 3, 'editrefused', 729, 'accepted', 'normal', NULL, NULL, 173);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (129, 1, 'new', 730, 'pending_review', 'abuse', NULL, NULL, 169);

INSERT INTO action_params (ad_id, action_id, name, value) VALUES (126, 1, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (126, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (127, 1, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (127, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (128, 1, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (128, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (129, 1, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (129, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (130, 1, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (130, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (131, 1, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (131, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (126, 2, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (126, 2, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (126, 3, 'prev_action_id', '2');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (126, 3, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (126, 3, 'redir', 'dW5rbm93bg==');

INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (715, 'X5522df6c65d40c2100000000857127700000000', 9, '2015-04-06 16:33:00.217168', '2015-04-06 16:33:00.391523', '10.0.1.122', 'authenticate', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (716, 'X5522df6c4e3cffba000000005240b31000000000', 9, '2015-04-06 16:33:00.391523', '2015-04-06 16:33:03.967866', '10.0.1.122', 'spamfilter', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (717, 'X5522df70794322f1000000005cf8ad9100000000', 9, '2015-04-06 16:33:03.967866', '2015-04-06 16:33:41.777722', '10.0.1.122', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (718, 'X5522df96dc0421c0000000064878b4600000000', 9, '2015-04-06 16:33:41.777722', '2015-04-06 16:33:41.905069', '10.0.1.122', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (719, 'X5522df9634e62f69000000005444dc2000000000', 9, '2015-04-06 16:33:41.905069', '2015-04-06 16:33:46.872669', '10.0.1.122', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (720, 'X5522df9b79e74027000000007ec459be00000000', 9, '2015-04-06 16:33:46.872669', '2015-04-06 16:34:49.98695', '10.0.1.122', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (721, 'X5522dfda213fe179000000007f1134af00000000', 9, '2015-04-06 16:34:49.98695', '2015-04-06 16:34:50.015395', '10.0.1.122', 'blocked_item', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (722, 'X5522dfda816ff8a000000007992682400000000', 9, '2015-04-06 16:34:50.015395', '2015-04-06 16:34:58.87905', '10.0.1.122', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (723, 'X5522dfe363d83f88000000003b8d339100000000', 9, '2015-04-06 16:34:58.87905', '2015-04-06 16:34:58.889243', '10.0.1.122', 'blocked_item', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (724, 'X5522dfe31eabce8b0000000060061a4100000000', 9, '2015-04-06 16:34:58.889243', '2015-04-06 16:35:00.311998', '10.0.1.122', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (725, 'X5522dfe470bcff1f000000002b6c754600000000', 9, '2015-04-06 16:35:00.311998', '2015-04-06 16:35:00.317393', '10.0.1.122', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (726, 'X5522dfe42ab998e60000000032ed675a00000000', 9, '2015-04-06 16:35:00.317393', '2015-04-06 16:35:06.631728', '10.0.1.122', 'blocked_item', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (727, 'X5522dfeb58eafb9d000000006a1938e100000000', 9, '2015-04-06 16:35:06.631728', '2015-04-06 16:35:09.675565', '10.0.1.122', 'block_rule', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (728, 'X5522dfee7058de7f00000000e268eff00000000', 9, '2015-04-06 16:35:09.675565', '2015-04-06 16:35:41.47338', '10.0.1.122', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (729, 'X5522e00d769406b0000000004a2fb6cc00000000', 9, '2015-04-06 16:35:41.47338', '2015-04-06 16:35:41.608332', '10.0.1.122', 'block_rule', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (730, 'X5522e00e4e48e29000000002b25633300000000', 9, '2015-04-06 16:35:41.608332', '2015-04-06 16:35:48.850354', '10.0.1.122', 'block_rule', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (731, 'X5522e0151b4b1885000000001ea6649200000000', 9, '2015-04-06 16:35:48.850354', '2015-04-06 16:35:48.86141', '10.0.1.122', 'block_rule', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (732, 'X5522e015658f0fd80000000039d77d4100000000', 9, '2015-04-06 16:35:48.86141', '2015-04-06 16:36:03.81499', '10.0.1.122', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (733, 'X5522e02447662e37000000006cef15100000000', 9, '2015-04-06 16:36:03.81499', '2015-04-06 16:36:38.316733', '10.0.1.122', 'spamfilter', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (734, 'X5522e04638e8b1f000000000551a8b2300000000', 9, '2015-04-06 16:36:38.316733', '2015-04-06 16:36:40.886005', '10.0.1.122', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (735, 'X5522e0496e449c070000000040ffb17a00000000', 9, '2015-04-06 16:36:40.886005', '2015-04-06 16:36:43.049812', '10.0.1.122', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (736, 'X5522e04b4eacf347000000002c3edfd800000000', 9, '2015-04-06 16:36:43.049812', '2015-04-06 16:36:43.054865', '10.0.1.122', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (737, 'X5522e04b235e6a7800000000328532d000000000', 9, '2015-04-06 16:36:43.054865', '2015-04-06 16:37:49.788614', '10.0.1.122', 'blocked_item', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (738, 'X5522e08e43f33607000000003b68eace00000000', 9, '2015-04-06 16:37:49.788614', '2015-04-06 16:37:51.114644', '10.0.1.122', 'spamfilter', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (739, 'X5522e08f7c7203f90000000077d07ac900000000', 9, '2015-04-06 16:37:51.114644', '2015-04-06 16:37:52.947959', '10.0.1.122', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (740, 'X5522e091234ed94d0000000078b003cb00000000', 9, '2015-04-06 16:37:52.947959', '2015-04-06 16:37:53.062494', '10.0.1.122', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (741, 'X5522e0911cac6b830000000068c9496500000000', 9, '2015-04-06 16:37:53.062494', '2015-04-06 16:37:53.071647', '10.0.1.122', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (742, 'X5522e091646bd4030000000049f7870900000000', 9, '2015-04-06 16:37:53.071647', '2015-04-06 16:37:53.080178', '10.0.1.122', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (743, 'X5522e09115cc1e590000000027cc818500000000', 9, '2015-04-06 16:37:53.080178', '2015-04-06 16:37:57.802355', '10.0.1.122', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (744, 'X5522e09618212130000000007738eae00000000', 9, '2015-04-06 16:37:57.802355', '2015-04-06 16:38:00.52863', '10.0.1.122', 'spamfilter', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (745, 'X5522e09963fdb36f0000000036b085bb00000000', 9, '2015-04-06 16:38:00.52863', '2015-04-06 16:38:07.688895', '10.0.1.122', 'listadmins', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (746, 'X5522e0a072197089000000001813ec0d00000000', 9, '2015-04-06 16:38:07.688895', '2015-04-06 16:38:07.704185', '10.0.1.122', 'listadmins', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (747, 'X5522e0a0fe466d70000000069193c7800000000', 9, '2015-04-06 16:38:07.704185', '2015-04-06 16:38:14.830071', '10.0.1.122', 'search_ads', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (748, 'X5522e0a761c94043000000005d1861aa00000000', 9, '2015-04-06 16:38:14.830071', '2015-04-06 16:38:14.919169', '10.0.1.122', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (749, 'X5522e0a745e1ab1000000000f43d2b200000000', 9, '2015-04-06 16:38:14.919169', '2015-04-06 16:38:14.925874', '10.0.1.122', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (750, 'X5522e0a7593b59bc000000006e75db1600000000', 9, '2015-04-06 16:38:14.925874', '2015-04-06 16:38:14.93287', '10.0.1.122', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (751, 'X5522e0a716425c9b000000001482438d00000000', 9, '2015-04-06 16:38:14.93287', '2015-04-06 16:38:22.117517', '10.0.1.122', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (752, 'X5522e0ae1bf5d23c000000001f2a167200000000', 9, '2015-04-06 16:38:22.117517', '2015-04-06 16:38:30.210216', '10.0.1.122', 'review', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (753, 'X5522e0b6335f580000000000677e1f400000000', 9, '2015-04-06 16:38:30.210216', '2015-04-06 16:38:30.230926', '10.0.1.122', 'listadmins', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (754, 'X5522e0b631e4573a00000000514aa35600000000', 9, '2015-04-06 16:38:30.230926', '2015-04-06 16:38:41.845847', '10.0.1.122', 'search_ads', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (755, 'X5522e0c23b63dc380000000047629cfa00000000', 9, '2015-04-06 16:38:41.845847', '2015-04-06 16:38:41.861378', '10.0.1.122', 'listadmins', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (756, 'X5522e0c2119253c6000000001dda6fff00000000', 9, '2015-04-06 16:38:41.861378', '2015-04-06 16:38:44.291038', '10.0.1.122', 'search_ads', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (757, 'X5522e0c43c80d4920000000042ac55f400000000', 9, '2015-04-06 16:38:44.291038', '2015-04-06 16:38:44.452537', '10.0.1.122', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (758, 'X5522e0c4497b4745000000001f9d9b7c00000000', 9, '2015-04-06 16:38:44.452537', '2015-04-06 16:38:44.460887', '10.0.1.122', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (759, 'X5522e0c4609d4cf60000000027ae813300000000', 9, '2015-04-06 16:38:44.460887', '2015-04-06 16:38:44.468189', '10.0.1.122', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (760, 'X5522e0c45a33b403000000004bcad68d00000000', 9, '2015-04-06 16:38:44.468189', '2015-04-06 16:38:54.712686', '10.0.1.122', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (761, 'X5522e0cf5242b881000000007117b20e00000000', 9, '2015-04-06 16:38:54.712686', '2015-04-06 16:39:08.854265', '10.0.1.122', 'review', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (762, 'X5522e0dd42625564000000008b9219e00000000', 9, '2015-04-06 16:39:08.854265', '2015-04-06 16:39:08.871918', '10.0.1.122', 'listadmins', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (763, 'X5522e0dd501bbe980000000027ec446900000000', 9, '2015-04-06 16:39:08.871918', '2015-04-06 16:39:19.922882', '10.0.1.122', 'search_ads', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (764, 'X5522e0e845c6b46800000000cdb5aa000000000', 9, '2015-04-06 16:39:19.922882', '2015-04-06 16:39:20.078531', '10.0.1.122', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (765, 'X5522e0e84f87b094000000007540683500000000', 9, '2015-04-06 16:39:20.078531', '2015-04-06 16:39:20.086855', '10.0.1.122', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (766, 'X5522e0e814de03b1000000005f3019af00000000', 9, '2015-04-06 16:39:20.086855', '2015-04-06 16:39:20.094695', '10.0.1.122', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (767, 'X5522e0e86de9ae30000000034c19e4b00000000', 9, '2015-04-06 16:39:20.094695', '2015-04-06 16:39:29.538189', '10.0.1.122', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (768, 'X5522e0f28c74d80000000043fdafb300000000', 9, '2015-04-06 16:39:29.538189', '2015-04-06 16:40:39.009679', '10.0.1.122', 'review', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (769, 'X5522e137799101e50000000061d6424200000000', 9, '2015-04-06 16:40:39.009679', '2015-04-06 16:40:39.035299', '10.0.1.122', 'listadmins', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (770, 'X5522e13765488a430000000078555a8800000000', 9, '2015-04-06 16:40:39.035299', '2015-04-06 16:40:44.370827', '10.0.1.122', 'search_ads', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (771, 'X5522e13c33b90c3a0000000043b7b0e00000000', 9, '2015-04-06 16:40:44.370827', '2015-04-06 16:40:44.472037', '10.0.1.122', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (772, 'X5522e13c350fdd2a0000000013f41d2200000000', 9, '2015-04-06 16:40:44.472037', '2015-04-06 16:40:44.480086', '10.0.1.122', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (773, 'X5522e13c393d36a400000000730fd2ed00000000', 9, '2015-04-06 16:40:44.480086', '2015-04-06 16:40:44.489463', '10.0.1.122', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (774, 'X5522e13c444b592b000000003e3acfdf00000000', 9, '2015-04-06 16:40:44.489463', '2015-04-06 16:40:53.87206', '10.0.1.122', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (775, 'X5522e14669428b6d00000000787cf68400000000', 9, '2015-04-06 16:40:53.87206', '2015-04-06 16:41:02.494564', '10.0.1.122', 'review', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (776, 'X5522e14e6ac4ec600000000de09b9700000000', 9, '2015-04-06 16:41:02.494564', '2015-04-06 16:41:02.512413', '10.0.1.122', 'listadmins', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (777, 'X5522e14f6fb6ddd9000000004c7a302800000000', 9, '2015-04-06 16:41:02.512413', '2015-04-06 16:41:06.921016', '10.0.1.122', 'search_ads', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (778, 'X5522e15376e3d20c0000000054ff681c00000000', 9, '2015-04-06 16:41:06.921016', '2015-04-06 16:41:07.035619', '10.0.1.122', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (779, 'X5522e15344cf8ab100000000440cf1ad00000000', 9, '2015-04-06 16:41:07.035619', '2015-04-06 16:41:07.043129', '10.0.1.122', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (780, 'X5522e15360d0d0c700000000788896eb00000000', 9, '2015-04-06 16:41:07.043129', '2015-04-06 16:41:07.050161', '10.0.1.122', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (781, 'X5522e15348486cbb0000000037fa6a1600000000', 9, '2015-04-06 16:41:07.050161', '2015-04-06 16:41:15.057827', '10.0.1.122', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (782, 'X5522e15b2a62b61a000000007d5849e600000000', 9, '2015-04-06 16:41:15.057827', '2015-04-06 16:42:22.068758', '10.0.1.122', 'review', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (783, 'X5522e19e142c62b00000000061c5b6000000000', 9, '2015-04-06 16:42:22.068758', '2015-04-06 16:42:22.097394', '10.0.1.122', 'listadmins', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (784, 'X5522e19e4ac08a1a000000006e820bd000000000', 9, '2015-04-06 16:42:22.097394', '2015-04-06 16:42:26.107681', '10.0.1.122', 'search_ads', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (785, 'X5522e1a2482d1f30000000028b86cc600000000', 9, '2015-04-06 16:42:26.107681', '2015-04-06 16:42:26.241716', '10.0.1.122', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (786, 'X5522e1a22c1132300000000069d99c5e00000000', 9, '2015-04-06 16:42:26.241716', '2015-04-06 16:42:26.248446', '10.0.1.122', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (787, 'X5522e1a22bd6bf4b000000003a5da09f00000000', 9, '2015-04-06 16:42:26.248446', '2015-04-06 16:42:26.254951', '10.0.1.122', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (788, 'X5522e1a21b3beab0000000002dcc452400000000', 9, '2015-04-06 16:42:26.254951', '2015-04-06 16:42:36.506246', '10.0.1.122', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (789, 'X5522e1ad66b0b8a100000000529c398d00000000', 9, '2015-04-06 16:42:36.506246', '2015-04-06 16:42:45.76552', '10.0.1.122', 'review', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (790, 'X5522e1b64bee873800000000ac77db200000000', 9, '2015-04-06 16:42:45.76552', '2015-04-06 16:42:45.776358', '10.0.1.122', 'listadmins', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (791, 'X5522e1b6120559d20000000052bbddc00000000', 9, '2015-04-06 16:42:45.776358', '2015-04-06 16:42:48.793256', '10.0.1.122', 'search_ads', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (792, 'X5522e1b97dd7509f00000000534e981f00000000', 9, '2015-04-06 16:42:48.793256', '2015-04-06 16:42:48.942225', '10.0.1.122', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (793, 'X5522e1b94a3c1400000000004222a9ca00000000', 9, '2015-04-06 16:42:48.942225', '2015-04-06 16:42:48.949462', '10.0.1.122', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (794, 'X5522e1b9118967ff0000000044bf70fc00000000', 9, '2015-04-06 16:42:48.949462', '2015-04-06 16:42:48.956343', '10.0.1.122', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (795, 'X5522e1b948ad1955000000007acbf36c00000000', 9, '2015-04-06 16:42:48.956343', '2015-04-06 16:42:54.115796', '10.0.1.122', 'block_list', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (796, 'X5522e1be3d3c678000000000461ff59100000000', 9, '2015-04-06 16:42:54.115796', '2015-04-06 16:43:57.593438', '10.0.1.122', 'unlock', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (797, 'X5522e1fe62b307f10000000043e8b64700000000', 9, '2015-04-06 16:43:57.593438', '2015-04-06 16:44:18.553264', '10.0.1.122', 'spamfilter', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (798, 'X5522e21354009129000000007152cd0b00000000', 9, '2015-04-06 16:44:18.553264', '2015-04-06 17:44:18.553264', '10.0.1.122', 'Websql', NULL, NULL);

INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (126, 1, 698, 'reg', 'initial', '2015-04-06 16:31:53.363461', '10.0.1.122', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (126, 1, 699, 'unverified', 'verifymail', '2015-04-06 16:31:53.363461', '10.0.1.122', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (126, 1, 700, 'pending_review', 'verify', '2015-04-06 16:31:53.428374', '10.0.1.122', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (127, 1, 701, 'reg', 'initial', '2015-04-06 16:32:12.588644', '10.0.1.122', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (127, 1, 702, 'unverified', 'verifymail', '2015-04-06 16:32:12.588644', '10.0.1.122', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (127, 1, 703, 'pending_review', 'verify', '2015-04-06 16:32:12.615885', '10.0.1.122', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (128, 1, 704, 'reg', 'initial', '2015-04-06 16:32:32.206541', '10.0.1.122', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (128, 1, 705, 'unverified', 'verifymail', '2015-04-06 16:32:32.206541', '10.0.1.122', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (128, 1, 706, 'pending_review', 'verify', '2015-04-06 16:32:32.258543', '10.0.1.122', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (129, 1, 707, 'reg', 'initial', '2015-04-06 16:32:51.213136', '10.0.1.122', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (129, 1, 708, 'unverified', 'verifymail', '2015-04-06 16:32:51.213136', '10.0.1.122', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (129, 1, 709, 'pending_review', 'verify', '2015-04-06 16:32:51.238413', '10.0.1.122', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (130, 1, 710, 'reg', 'initial', '2015-04-06 16:37:14.905821', '10.0.1.122', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (130, 1, 711, 'unverified', 'verifymail', '2015-04-06 16:37:14.905821', '10.0.1.122', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (130, 1, 712, 'pending_review', 'verify', '2015-04-06 16:37:14.989761', '10.0.1.122', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (131, 1, 713, 'reg', 'initial', '2015-04-06 16:37:38.939734', '10.0.1.122', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (131, 1, 714, 'unverified', 'verifymail', '2015-04-06 16:37:38.939734', '10.0.1.122', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (131, 1, 715, 'pending_review', 'verify', '2015-04-06 16:37:38.98041', '10.0.1.122', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (87, 1, 716, 'locked', 'checkout', '2015-04-06 16:37:52.951865', '10.0.1.122', 739);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (92, 1, 717, 'locked', 'checkout', '2015-04-06 16:37:52.951865', '10.0.1.122', 739);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (126, 1, 718, 'accepted', 'accept', '2015-04-06 16:38:22.120981', '10.0.1.122', 751);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (127, 1, 719, 'refused', 'refuse', '2015-04-06 16:38:54.718948', '10.0.1.122', 760);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (128, 1, 720, 'accepted', 'accept', '2015-04-06 16:39:29.543281', '10.0.1.122', 767);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (126, 2, 721, 'reg', 'initial', '2015-04-06 16:40:20.906506', '10.0.1.122', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (126, 2, 722, 'unverified', 'verifymail', '2015-04-06 16:40:20.906506', '10.0.1.122', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (126, 2, 723, 'pending_review', 'verify', '2015-04-06 16:40:20.950266', '10.0.1.122', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (128, 2, 724, 'refused', 'refuse', '2015-04-06 16:40:53.876251', '10.0.1.122', 774);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (126, 2, 725, 'refused', 'refuse', '2015-04-06 16:41:15.061055', '10.0.1.122', 781);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (126, 3, 726, 'reg', 'initial', '2015-04-06 16:42:03.521802', '10.0.1.122', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (126, 3, 727, 'unverified', 'verifymail', '2015-04-06 16:42:03.521802', '10.0.1.122', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (126, 3, 728, 'pending_review', 'verify', '2015-04-06 16:42:03.559025', '10.0.1.122', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (126, 3, 729, 'accepted', 'accept', '2015-04-06 16:42:36.510083', '10.0.1.122', 788);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (129, 1, 730, 'pending_review', 'newqueue', '2015-04-06 16:42:54.117623', '10.0.1.122', 795);

SELECT pg_catalog.setval('action_states_state_id_seq', 730, true);

INSERT INTO ad_changes (ad_id, action_id, state_id, is_param, column_name, old_value, new_value) VALUES (126, 2, 721, false, 'body', 'asdasd asdasd sd s', 'asdasd asdasd sd s
asd asda sd asda sd');
INSERT INTO ad_changes (ad_id, action_id, state_id, is_param, column_name, old_value, new_value) VALUES (126, 3, 726, false, 'body', 'asdasd asdasd sd s', 'asdasd asdasd sd s
asd asda sd asda sd
asd ada dad asda');

INSERT INTO ad_codes (code, code_type) VALUES (24741, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (56764, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (88787, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (30810, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (62833, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (94856, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (36879, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (68902, 'pay');

INSERT INTO ad_image_changes (ad_id, action_id, state_id, seq_no, name, is_new) VALUES (126, 1, 718, 0, NULL, false);
INSERT INTO ad_image_changes (ad_id, action_id, state_id, seq_no, name, is_new) VALUES (128, 1, 720, 0, NULL, false);
INSERT INTO ad_image_changes (ad_id, action_id, state_id, seq_no, name, is_new) VALUES (126, 2, 721, 0, NULL, false);
INSERT INTO ad_image_changes (ad_id, action_id, state_id, seq_no, name, is_new) VALUES (126, 3, 726, 0, NULL, false);
INSERT INTO ad_image_changes (ad_id, action_id, state_id, seq_no, name, is_new) VALUES (126, 3, 729, 0, NULL, false);

INSERT INTO ad_params (ad_id, name, value) VALUES (126, 'prev_currency', 'peso');
INSERT INTO ad_params (ad_id, name, value) VALUES (126, 'country', 'UNK');
INSERT INTO ad_params (ad_id, name, value) VALUES (127, 'prev_currency', 'peso');
INSERT INTO ad_params (ad_id, name, value) VALUES (127, 'country', 'UNK');
INSERT INTO ad_params (ad_id, name, value) VALUES (128, 'prev_currency', 'peso');
INSERT INTO ad_params (ad_id, name, value) VALUES (128, 'country', 'UNK');
INSERT INTO ad_params (ad_id, name, value) VALUES (129, 'prev_currency', 'peso');
INSERT INTO ad_params (ad_id, name, value) VALUES (129, 'country', 'UNK');
INSERT INTO ad_params (ad_id, name, value) VALUES (130, 'prev_currency', 'peso');
INSERT INTO ad_params (ad_id, name, value) VALUES (130, 'country', 'UNK');
INSERT INTO ad_params (ad_id, name, value) VALUES (131, 'prev_currency', 'peso');
INSERT INTO ad_params (ad_id, name, value) VALUES (131, 'country', 'UNK');

INSERT INTO ad_queues (ad_id, action_id, queue, queued_at, locked_by, locked_until) VALUES (130, 1, 'abuse', '2015-04-06 16:37:14.989761', NULL, NULL);
INSERT INTO ad_queues (ad_id, action_id, queue, queued_at, locked_by, locked_until) VALUES (131, 1, 'abuse', '2015-04-06 16:37:38.98041', NULL, NULL);
INSERT INTO ad_queues (ad_id, action_id, queue, queued_at, locked_by, locked_until) VALUES (129, 1, 'abuse', '2015-04-06 16:32:51.238413', NULL, NULL);

SELECT pg_catalog.setval('ads_ad_id_seq', 131, true);

INSERT INTO block_lists (list_id, list_name, base_list, list_type, ignore) VALUES (100, 'emails to block', false, 'email', NULL);

SELECT pg_catalog.setval('block_lists_list_id_seq', 100, true);

INSERT INTO block_rules (rule_id, rule_name, action, application, combine_rule, count_yesterday, count_today, count_date, target) VALUES (100, 'email blocker', 'move_to_queue', 'clear', 'and', 0, 0, '2015-04-06', 'abuse');

INSERT INTO block_rule_conditions (condition_id, rule_id, list_id, whole_word, fields) VALUES (20, 100, 100, 1, '{email}');

SELECT pg_catalog.setval('block_rule_conditions_condition_id_seq', 20, true);

SELECT pg_catalog.setval('block_rules_rule_id_seq', 100, true);

INSERT INTO blocked_items (item_id, value, list_id, notice, token_id) VALUES (22, 'piroca@salvaje.com', 100, 'test', 722);

SELECT pg_catalog.setval('blocked_items_item_id_seq', 22, true);

INSERT INTO event_log (event_id, event_name, event, token_id, "time") VALUES (9, 'block_list', 'INSERT list emails to block (email)', 717, '2015-04-06 16:33:41.78098');
INSERT INTO event_log (event_id, event_name, event, token_id, "time") VALUES (10, 'blocked_item', 'INSERT blocked_item piroca@salvaje.com, 100, "test"', 722, '2015-04-06 16:34:58.882266');
INSERT INTO event_log (event_id, event_name, event, token_id, "time") VALUES (11, 'block_rule', 'INSERT Rule: email blocker', 728, '2015-04-06 16:35:41.476505');

SELECT pg_catalog.setval('event_log_event_id_seq', 11, true);

SELECT pg_catalog.setval('list_id_seq', 8000086, true);

SELECT pg_catalog.setval('pay_code_seq', 474, true);

INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (213, 'save', 0, '633026231', NULL, '2015-04-06 16:31:53', 166, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (214, 'verify', 0, '633026231', NULL, '2015-04-06 16:31:53', 166, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (215, 'save', 0, '768418254', NULL, '2015-04-06 16:32:13', 167, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (216, 'verify', 0, '768418254', NULL, '2015-04-06 16:32:13', 167, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (217, 'save', 0, '903810277', NULL, '2015-04-06 16:32:32', 168, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (218, 'verify', 0, '903810277', NULL, '2015-04-06 16:32:32', 168, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (219, 'save', 0, '139202300', NULL, '2015-04-06 16:32:51', 169, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (220, 'verify', 0, '139202300', NULL, '2015-04-06 16:32:51', 169, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (221, 'save', 0, '274594323', NULL, '2015-04-06 16:37:15', 170, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (222, 'verify', 0, '274594323', NULL, '2015-04-06 16:37:15', 170, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (223, 'save', 0, '409986346', NULL, '2015-04-06 16:37:39', 171, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (224, 'verify', 0, '409986346', NULL, '2015-04-06 16:37:39', 171, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (225, 'save', 0, '545378369', NULL, '2015-04-06 16:40:21', 172, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (226, 'verify', 0, '545378369', NULL, '2015-04-06 16:40:21', 172, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (227, 'save', 0, '680770392', NULL, '2015-04-06 16:42:04', 173, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (228, 'verify', 0, '680770392', NULL, '2015-04-06 16:42:04', 173, 'OK');

SELECT pg_catalog.setval('pay_log_pay_log_id_seq', 228, true);

INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (213, 0, 'adphone', '123123123');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (213, 1, 'remote_addr', '10.0.1.122');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (214, 0, 'remote_addr', '10.0.1.122');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (215, 0, 'adphone', '123123123');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (215, 1, 'remote_addr', '10.0.1.122');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (216, 0, 'remote_addr', '10.0.1.122');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (217, 0, 'adphone', '123123123');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (217, 1, 'remote_addr', '10.0.1.122');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (218, 0, 'remote_addr', '10.0.1.122');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (219, 0, 'adphone', '123123123');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (219, 1, 'remote_addr', '10.0.1.122');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (220, 0, 'remote_addr', '10.0.1.122');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (221, 0, 'adphone', '123123123');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (221, 1, 'remote_addr', '10.0.1.122');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (222, 0, 'remote_addr', '10.0.1.122');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (223, 0, 'adphone', '123123123');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (223, 1, 'remote_addr', '10.0.1.122');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (224, 0, 'remote_addr', '10.0.1.122');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (225, 0, 'adphone', '123123123');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (225, 1, 'remote_addr', '10.0.1.122');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (226, 0, 'remote_addr', '10.0.1.122');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (227, 0, 'adphone', '123123123');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (227, 1, 'remote_addr', '10.0.1.122');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (228, 0, 'remote_addr', '10.0.1.122');

SELECT pg_catalog.setval('payment_groups_payment_group_id_seq', 173, true);

INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (166, 'ad_action', 0, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (167, 'ad_action', 0, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (168, 'ad_action', 0, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (169, 'ad_action', 0, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (170, 'ad_action', 0, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (171, 'ad_action', 0, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (172, 'ad_action', 0, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (173, 'ad_action', 0, 0);

INSERT INTO review_log (ad_id, action_id, admin_id, review_time, queue, action_type, action, refusal_reason_text, category) VALUES (126, 1, 9, '2015-04-06 16:38:22.120981', 'all', 'new', 'accepted', NULL, 8020);
INSERT INTO review_log (ad_id, action_id, admin_id, review_time, queue, action_type, action, refusal_reason_text, category) VALUES (127, 1, 9, '2015-04-06 16:38:54.718948', 'all', 'new', 'refused', 'Ilegal', 8020);
INSERT INTO review_log (ad_id, action_id, admin_id, review_time, queue, action_type, action, refusal_reason_text, category) VALUES (128, 1, 9, '2015-04-06 16:39:29.543281', 'all', 'new', 'accepted', NULL, 8020);
INSERT INTO review_log (ad_id, action_id, admin_id, review_time, queue, action_type, action, refusal_reason_text, category) VALUES (128, 1, 9, '2015-04-06 16:40:53.876251', 'all', 'new', 'refused', 'Ilegal', 8020);
INSERT INTO review_log (ad_id, action_id, admin_id, review_time, queue, action_type, action, refusal_reason_text, category) VALUES (126, 2, 9, '2015-04-06 16:41:15.061055', 'all', 'edit', 'refused', 'Ilegal', 8020);
INSERT INTO review_log (ad_id, action_id, admin_id, review_time, queue, action_type, action, refusal_reason_text, category) VALUES (126, 3, 9, '2015-04-06 16:42:36.510083', 'all', 'editrefused', 'accepted', NULL, 8020);

INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (126, 1, 700, 'queue', 'normal');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (127, 1, 703, 'queue', 'normal');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (128, 1, 706, 'queue', 'normal');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (129, 1, 709, 'queue', 'normal');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (130, 1, 712, 'queue', 'abuse');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (131, 1, 715, 'queue', 'abuse');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (126, 1, 718, 'filter_name', 'all');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (127, 1, 719, 'reason', '14');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (127, 1, 719, 'filter_name', 'all');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (128, 1, 720, 'filter_name', 'all');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (126, 2, 723, 'queue', 'edit');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (128, 2, 724, 'reason', '14');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (128, 2, 724, 'filter_name', 'all');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (126, 2, 725, 'reason', '14');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (126, 2, 725, 'filter_name', 'all');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (126, 3, 728, 'queue', 'normal');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (126, 3, 729, 'filter_name', 'all');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (129, 1, 730, 'queue', 'abuse');

INSERT INTO stats_daily (stat_time, name, value) VALUES ('2015-04-06', 'websql_query_75', '1');

SELECT pg_catalog.setval('tokens_token_id_seq', 798, true);

INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (93, '2015-04-06 16:38:22.14613', '30572@kiyoko.schibsted.cl', NULL, '2015-04-06 16:39:54.673324', '30572@kiyoko.schibsted.cl', 'TRANS_OK', '2015-04-06 16:39:54.613602', NULL, 'cmd:admail
commit:1
ad_id:126
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (94, '2015-04-06 16:39:29.587163', '30572@kiyoko.schibsted.cl', NULL, '2015-04-06 16:39:54.702999', '30572@kiyoko.schibsted.cl', 'TRANS_OK', '2015-04-06 16:39:54.613602', NULL, 'cmd:admail
commit:1
ad_id:128
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (95, '2015-04-06 16:42:36.542202', '30572@kiyoko.schibsted.cl', NULL, NULL, NULL, NULL, NULL, NULL, 'cmd:admail
commit:1
ad_id:126
action_id:3
mail_type:edit_accept
', NULL, 'accepted_mail', NULL, NULL);

SELECT pg_catalog.setval('trans_queue_id_seq', 95, true);

INSERT INTO user_params (user_id, name, value) VALUES (59, 'first_approved_ad', '2015-04-06 16:38:22.120981-03');
INSERT INTO user_params (user_id, name, value) VALUES (59, 'first_inserted_ad', '2015-04-06 16:38:22.120981-03');

SELECT pg_catalog.setval('users_user_id_seq', 60, true);
INSERT INTO ad_params VALUES (129, 'communes', '331');
INSERT INTO ad_params VALUES (130, 'communes', '302');

--Change all passwords to 123123123
update ads set salted_passwd =  E'$1024$2y1bQ}VYRD}zF4QCbf07a971b784888658a20e62156de64dfde5ffb6', passwd = null;
update accounts set salted_passwd =  E'$1024$2y1bQ}VYRD}zF4QCbf07a971b784888658a20e62156de64dfde5ffb6';
