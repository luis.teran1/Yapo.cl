
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (170, '12345', 'paid', '2015-10-20 11:59:16.772704', NULL);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (6, 2, 'bump', 701, 'accepted', 'normal', NULL, NULL, 170);

INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (6, 2, 698, 'reg', 'initial', '2015-10-20 11:59:16.772704', '10.0.1.117', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (6, 2, 699, 'unpaid', 'pending_pay', '2015-10-20 11:59:16.772704', '10.0.1.117', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (6, 2, 700, 'pending_bump', 'pay', '2015-10-20 11:59:20.194822', NULL, NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (6, 2, 701, 'accepted', 'bump', '2015-10-20 11:59:20.284804', NULL, NULL);

SELECT pg_catalog.setval('action_states_state_id_seq', 701, true);

INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (221, 'save', 0, '12345', NULL, '2015-10-20 11:59:17', 170, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (222, 'paycard_save', 0, '12345', NULL, '2015-10-20 11:59:19', 170, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (223, 'paycard', 1600, '00170a', NULL, '2015-10-20 11:59:20', 170, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (224, 'paycard', 1600, '12345', NULL, '2015-10-20 11:59:20', 170, 'OK');


SELECT pg_catalog.setval('pay_log_pay_log_id_seq', 224, true);

INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (221, 0, 'remote_addr', '10.0.1.117');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (222, 0, 'auth_code', '852450');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (222, 1, 'trans_date', '20/10/2015 11:59:18');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (222, 2, 'placements_type', 'VN');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (222, 3, 'placements_number', '1');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (222, 4, 'pay_type', 'credit');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (222, 5, 'external_response', '0');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (222, 6, 'external_id', '07929666700582084538');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (222, 7, 'remote_addr', '10.0.1.74');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (223, 0, 'webpay', '1');

SELECT pg_catalog.setval('payment_groups_payment_group_id_seq', 170, true);


INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (170, 'bump', 1600, 0);

INSERT INTO purchase (purchase_id, doc_type, doc_num, external_doc_id, status, receipt, delivery, rut, name, lob, address, region, communes, contact, email, tax, discount, seller_id, total_price, payment_group_id, payment_method, payment_platform, account_id) VALUES (6, 'bill', 998877, 665544, 'failed', '2015-10-20 11:59:16.772704', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'prepaid5@blocket.se', 0, 0, NULL, 1600, 170, 'webpay', 'desktop', NULL);
INSERT INTO purchase_detail (purchase_detail_id, purchase_id, product_id, price, action_id, ad_id, payment_group_id) VALUES (6, 6, 1, 1600, 2, 6, 170);
SELECT pg_catalog.setval('purchase_detail_purchase_detail_id_seq', 6, true);
SELECT pg_catalog.setval('purchase_purchase_id_seq', 6, true);
INSERT INTO purchase_states (purchase_id, purchase_state_id, status, "timestamp", remote_addr) VALUES (6, 5, 'pending', '2015-10-20 11:59:16.772704', '10.0.1.117');
SELECT pg_catalog.setval('purchase_states_purchase_state_id_seq', 5, true);
SELECT pg_catalog.setval('trans_queue_id_seq', 93, true);



--Change all passwords to 123123123
update ads set salted_passwd =  E'$1024$2y1bQ}VYRD}zF4QCbf07a971b784888658a20e62156de64dfde5ffb6', passwd = null;
update accounts set salted_passwd =  E'$1024$2y1bQ}VYRD}zF4QCbf07a971b784888658a20e62156de64dfde5ffb6';
