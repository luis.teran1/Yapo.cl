-- DIFF to CHECK sales report with stores
update users set paid_total = 1000 where uid = 58;
update users set paid_total = 4990 where uid = 58;
update ads set list_time = now(), modified_at = now() where ad_id in(6,95,124);

INSERT INTO payment_groups VALUES (166, '12345', 'paid', now(), NULL);
INSERT INTO payment_groups VALUES (167, '65432', 'paid', now(), NULL);
INSERT INTO payment_groups VALUES (168, '32323', 'paid', now(), NULL);
INSERT INTO payment_groups VALUES (169, '54545', 'paid', now(), 168);
INSERT INTO payment_groups VALUES (170, '90001', 'paid', now(), 168);
INSERT INTO payment_groups VALUES (171, '90002', 'paid', now(), NULL);

INSERT INTO ad_actions VALUES (95, 2, 'bump', 701, 'accepted', 'normal', NULL, NULL, 167);
INSERT INTO ad_actions VALUES (124, 2, 'bump', 708, 'accepted', 'normal', NULL, NULL, 169);
INSERT INTO ad_actions VALUES (6, 2, 'weekly_bump', 709, 'accepted', 'normal', NULL, NULL, 170);
INSERT INTO ad_actions VALUES (6, 3, 'bump', 711, 'accepted', 'normal', NULL, NULL, NULL);

INSERT INTO stores VALUES (1, NULL, NULL, NULL, NULL, 'inactive', '2014-10-01 11:17:14.61853', '2014-10-01 11:37:17.751376', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO stores VALUES (2, NULL, NULL, NULL, NULL, 'inactive', '2014-10-01 11:23:02.80213', '2014-10-01 11:33:06.018514', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6);

INSERT INTO tokens VALUES (715, 'X542c0d251c80bfab000000002db6e5af00000000', 9, '2014-10-01 11:18:12.701514', '2014-10-01 11:18:12.895543', '10.0.1.101', 'authenticate', NULL, NULL);
INSERT INTO tokens VALUES (716, 'X542c0d25feec39c000000004c27e08f00000000', 9, '2014-10-01 11:18:12.895543', '2014-10-01 11:18:29.223141', '10.0.1.101', 'spamfilter', NULL, NULL);
INSERT INTO tokens VALUES (717, 'X542c0d354e91495a000000002059d8b800000000', 9, '2014-10-01 11:18:29.223141', '2014-10-01 11:21:40.81489', '10.0.1.101', 'Websql', NULL, NULL);
INSERT INTO tokens VALUES (718, 'X542c0df5551be4e000000004916677600000000', 9, '2014-10-01 11:21:40.81489', '2014-10-01 11:23:23.206732', '10.0.1.101', 'Websql', NULL, NULL);
INSERT INTO tokens VALUES (719, 'X542c0e5b5e4ab76d000000001fec364300000000', 9, '2014-10-01 11:23:23.206732', '2014-10-01 11:23:38.903556', '10.0.1.101', 'Websql', NULL, NULL);
INSERT INTO tokens VALUES (720, 'X542c0e6b2f6532de0000000031a8292b00000000', 9, '2014-10-01 11:23:38.903556', '2014-10-01 11:25:56.493525', '10.0.1.101', 'Websql', NULL, NULL);
INSERT INTO tokens VALUES (721, 'X542c0ef46fcfeab0000000003fcdf3ad00000000', 9, '2014-10-01 11:25:56.493525', '2014-10-01 11:25:56.766089', '10.0.1.174', 'authenticate', NULL, NULL);
INSERT INTO tokens VALUES (722, 'X542c0ef55f3b874900000000448c7de400000000', 9, '2014-10-01 11:25:56.766089', '2014-10-01 11:26:10.465953', '10.0.1.174', 'spamfilter', NULL, NULL);
INSERT INTO tokens VALUES (723, 'X542c0f024d4aaa5300000000245bea1400000000', 9, '2014-10-01 11:26:10.465953', '2014-10-01 12:26:10.465953', '10.0.1.174', 'Websql', NULL, NULL);

INSERT INTO action_states VALUES (95, 2, 698, 'reg', 'initial', '2014-10-01 11:17:32.797197', '10.0.1.101', NULL);
INSERT INTO action_states VALUES (95, 2, 699, 'unpaid', 'pending_pay', '2014-10-01 11:17:32.797197', '10.0.1.101', NULL);
INSERT INTO action_states VALUES (95, 2, 700, 'pending_bump', 'pay', '2014-10-01 11:17:35.883814', NULL, NULL);
INSERT INTO action_states VALUES (95, 2, 701, 'accepted', 'bump', '2014-10-01 11:17:35.982565', NULL, NULL);
INSERT INTO action_states VALUES (124, 2, 702, 'reg', 'initial', '2014-10-01 11:22:29.094796', '10.0.1.101', NULL);
INSERT INTO action_states VALUES (124, 2, 703, 'unpaid', 'pending_pay', '2014-10-01 11:22:29.094796', '10.0.1.101', NULL);
INSERT INTO action_states VALUES (6, 2, 704, 'reg', 'initial', '2014-10-01 11:22:29.094796', '10.0.1.101', NULL);
INSERT INTO action_states VALUES (6, 2, 705, 'unpaid', 'pending_pay', '2014-10-01 11:22:29.094796', '10.0.1.101', NULL);
INSERT INTO action_states VALUES (124, 2, 706, 'pending_bump', 'pay', '2014-10-01 11:22:33.32062', NULL, NULL);
INSERT INTO action_states VALUES (6, 2, 707, 'accepted', 'pay', '2014-10-01 11:22:33.361065', NULL, NULL);
INSERT INTO action_states VALUES (124, 2, 708, 'accepted', 'bump', '2014-10-01 11:22:33.471209', NULL, NULL);
INSERT INTO action_states VALUES (6, 2, 709, 'accepted', 'updating_counter', '2014-10-01 11:22:33.482561', NULL, NULL);
INSERT INTO action_states VALUES (6, 3, 710, 'reg', 'initial', '2014-10-01 11:22:33.493263', NULL, NULL);
INSERT INTO action_states VALUES (6, 3, 711, 'accepted', 'bump', '2014-10-01 11:22:33.493263', NULL, NULL);

SELECT pg_catalog.setval('action_states_state_id_seq', 711, true);

INSERT INTO ad_changes VALUES (6, 2, 709, true, 'weekly_bump', NULL, '3');

INSERT INTO ad_params VALUES (6, 'weekly_bump', '3');

INSERT INTO pay_log VALUES (214, 'paycard_save', 0, '12345', NULL, '2014-10-01 11:17:18', 166, 'SAVE');
INSERT INTO pay_log VALUES (215, 'save', 0, '65432', NULL, '2014-10-01 11:17:33', 167, 'SAVE');
INSERT INTO pay_log VALUES (216, 'paycard_save', 0, '65432', NULL, '2014-10-01 11:17:36', 167, 'SAVE');
INSERT INTO pay_log VALUES (217, 'paycard', 1000, '00167a', NULL, '2014-10-01 11:17:36', 167, 'OK');
INSERT INTO pay_log VALUES (218, 'save', 0, '54545', NULL, '2014-10-01 11:22:29', 169, 'SAVE');
INSERT INTO pay_log VALUES (219, 'save', 0, '90001', NULL, '2014-10-01 11:22:29', 170, 'SAVE');
INSERT INTO pay_log VALUES (220, 'paycard_save', 0, '32323', NULL, '2014-10-01 11:22:33', 168, 'SAVE');
INSERT INTO pay_log VALUES (221, 'paycard', 1000, '00169a', NULL, '2014-10-01 11:22:33', 169, 'OK');
INSERT INTO pay_log VALUES (222, 'paycard', 2990, '00170a', NULL, '2014-10-01 11:22:33', 170, 'OK');
INSERT INTO pay_log VALUES (223, 'paycard', 3990, '32323', NULL, '2014-10-01 11:22:33', 168, 'OK');
INSERT INTO pay_log VALUES (224, 'save', 0, '90002', NULL, '2014-10-01 11:23:03', 171, 'SAVE');
INSERT INTO pay_log VALUES (225, 'paycard_save', 0, '90002', NULL, '2014-10-01 11:23:06', 171, 'SAVE');

SELECT pg_catalog.setval('pay_log_pay_log_id_seq', 225, true);

INSERT INTO pay_log_references VALUES (214, 0, 'auth_code', '622032');
INSERT INTO pay_log_references VALUES (214, 1, 'trans_date', '01/10/2014 11:17:16');
INSERT INTO pay_log_references VALUES (214, 2, 'placements_type', 'VN');
INSERT INTO pay_log_references VALUES (214, 3, 'placements_number', '1');
INSERT INTO pay_log_references VALUES (214, 4, 'pay_type', 'credit');
INSERT INTO pay_log_references VALUES (214, 5, 'external_response', '0');
INSERT INTO pay_log_references VALUES (214, 6, 'external_id', '07694148661956987672');
INSERT INTO pay_log_references VALUES (214, 7, 'remote_addr', '10.0.1.208');
INSERT INTO pay_log_references VALUES (215, 0, 'remote_addr', '10.0.1.101');
INSERT INTO pay_log_references VALUES (216, 0, 'auth_code', '801068');
INSERT INTO pay_log_references VALUES (216, 1, 'trans_date', '01/10/2014 11:17:35');
INSERT INTO pay_log_references VALUES (216, 2, 'placements_type', 'VN');
INSERT INTO pay_log_references VALUES (216, 3, 'placements_number', '1');
INSERT INTO pay_log_references VALUES (216, 4, 'pay_type', 'credit');
INSERT INTO pay_log_references VALUES (216, 5, 'external_response', '0');
INSERT INTO pay_log_references VALUES (216, 6, 'external_id', '06880423432576527080');
INSERT INTO pay_log_references VALUES (216, 7, 'remote_addr', '10.0.1.208');
INSERT INTO pay_log_references VALUES (217, 0, 'webpay', '1');
INSERT INTO pay_log_references VALUES (218, 0, 'remote_addr', '10.0.1.101');
INSERT INTO pay_log_references VALUES (219, 0, 'remote_addr', '10.0.1.101');
INSERT INTO pay_log_references VALUES (220, 0, 'auth_code', '782306');
INSERT INTO pay_log_references VALUES (220, 1, 'trans_date', '01/10/2014 11:22:31');
INSERT INTO pay_log_references VALUES (220, 2, 'placements_type', 'VN');
INSERT INTO pay_log_references VALUES (220, 3, 'placements_number', '1');
INSERT INTO pay_log_references VALUES (220, 4, 'pay_type', 'credit');
INSERT INTO pay_log_references VALUES (220, 5, 'external_response', '0');
INSERT INTO pay_log_references VALUES (220, 6, 'external_id', '25618774303258125357');
INSERT INTO pay_log_references VALUES (220, 7, 'remote_addr', '10.0.1.208');
INSERT INTO pay_log_references VALUES (221, 0, 'webpay', '1');
INSERT INTO pay_log_references VALUES (222, 0, 'webpay', '1');
INSERT INTO pay_log_references VALUES (224, 0, 'remote_addr', '10.0.1.101');
INSERT INTO pay_log_references VALUES (225, 0, 'auth_code', '813872');
INSERT INTO pay_log_references VALUES (225, 1, 'trans_date', '01/10/2014 11:23:05');
INSERT INTO pay_log_references VALUES (225, 2, 'placements_type', 'VN');
INSERT INTO pay_log_references VALUES (225, 3, 'placements_number', '1');
INSERT INTO pay_log_references VALUES (225, 4, 'pay_type', 'credit');
INSERT INTO pay_log_references VALUES (225, 5, 'external_response', '0');
INSERT INTO pay_log_references VALUES (225, 6, 'external_id', '39986951810593928421');
INSERT INTO pay_log_references VALUES (225, 7, 'remote_addr', '10.0.1.208');

SELECT pg_catalog.setval('payment_groups_payment_group_id_seq', 171, true);

INSERT INTO payments VALUES (166, 'annual_store', 48000, 0);
INSERT INTO payments VALUES (167, 'bump', 1000, 0);
INSERT INTO payments VALUES (169, 'bump', 1000, 0);
INSERT INTO payments VALUES (170, 'weekly_bump', 2990, 0);
INSERT INTO payments VALUES (171, 'quarterly_store', 15000, 0);

INSERT INTO purchase VALUES (2, 'invoice', 1, 1001, 'confirmed', '2014-10-01 11:17:14.647363', NULL, '100000-4', 'BLOCKET', 'Technology', 'Alonso de C�rdova 5670', 15, 315, 'BLOCKET', 'prepaid5@blocket.se', 0, 0, NULL, 48000, 166);
INSERT INTO purchase VALUES (3, 'bill', 2, 1002, 'confirmed', '2014-10-01 11:17:32.797197', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'prepaid5@blocket.se', 0, 0, NULL, 1000, 167);
INSERT INTO purchase VALUES (4, 'invoice', 3, 1003, 'confirmed', '2014-10-01 11:22:29.094796', NULL, '100000-4', 'BLOCKET', 'Technology', 'Alonso de C�rdova 5670', 15, 315, 'BLOCKET', 'prepaid5@blocket.se', 0, 0, NULL, 3990, 168);
INSERT INTO purchase VALUES (5, 'bill', 4, 1004, 'confirmed', '2014-10-01 11:23:02.819287', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'many@ads.cl', 0, 0, NULL, 15000, 171);

INSERT INTO purchase_detail VALUES (2, 2, 7, 48000, NULL, NULL);
INSERT INTO purchase_detail VALUES (3, 3, 1, 1000, 2, 95);
INSERT INTO purchase_detail VALUES (4, 4, 1, 1000, 2, 124);
INSERT INTO purchase_detail VALUES (5, 4, 2, 2990, 2, 6);
INSERT INTO purchase_detail VALUES (6, 5, 5, 15000, NULL, NULL);


SELECT pg_catalog.setval('purchase_detail_purchase_detail_id_seq', 6, true);

SELECT pg_catalog.setval('purchase_purchase_id_seq', 5, true);

INSERT INTO purchase_states VALUES (2, 1, 'pending', '2014-10-01 11:17:14.647363', '10.0.1.101');
INSERT INTO purchase_states VALUES (3, 2, 'pending', '2014-10-01 11:17:32.797197', '10.0.1.101');
INSERT INTO purchase_states VALUES (4, 3, 'pending', '2014-10-01 11:22:29.094796', '10.0.1.101');
INSERT INTO purchase_states VALUES (5, 4, 'pending', '2014-10-01 11:23:02.819287', '10.0.1.101');

SELECT pg_catalog.setval('purchase_states_purchase_state_id_seq', 4, true);

INSERT INTO stats_daily VALUES ('2014-10-01', 'websql_query_80', '5');

INSERT INTO store_actions VALUES (1, 1, 'new', 1, 'reg', NULL);
INSERT INTO store_actions VALUES (1, 2, 'new', 2, 'unpaid', NULL);
INSERT INTO store_actions VALUES (1, 4, 'status_change', 5, 'accepted', NULL);
INSERT INTO store_actions VALUES (1, 3, 'extend', 7, 'accepted', 166);
INSERT INTO store_actions VALUES (2, 1, 'new', 8, 'reg', NULL);
INSERT INTO store_actions VALUES (2, 2, 'new', 9, 'unpaid', NULL);
INSERT INTO store_actions VALUES (2, 4, 'status_change', 12, 'accepted', NULL);
INSERT INTO store_actions VALUES (2, 3, 'extend', 14, 'accepted', 171);

INSERT INTO store_action_states VALUES (1, 1, 1, 'reg', '2014-10-01 11:17:14.61853', NULL);
INSERT INTO store_action_states VALUES (1, 2, 2, 'unpaid', '2014-10-01 11:17:14.61853', NULL);
INSERT INTO store_action_states VALUES (1, 3, 3, 'unpaid', '2014-10-01 11:17:14.647363', NULL);
INSERT INTO store_action_states VALUES (1, 4, 4, 'reg', '2014-10-01 11:17:17.751376', NULL);
INSERT INTO store_action_states VALUES (1, 4, 5, 'accepted', '2014-10-01 11:17:17.751376', NULL);
INSERT INTO store_action_states VALUES (1, 3, 6, 'paid', '2014-10-01 11:17:17.751376', NULL);
INSERT INTO store_action_states VALUES (1, 3, 7, 'accepted', '2014-10-01 11:17:17.751376', NULL);
INSERT INTO store_action_states VALUES (2, 1, 8, 'reg', '2014-10-01 11:23:02.80213', NULL);
INSERT INTO store_action_states VALUES (2, 2, 9, 'unpaid', '2014-10-01 11:23:02.80213', NULL);
INSERT INTO store_action_states VALUES (2, 3, 10, 'unpaid', '2014-10-01 11:23:02.819287', NULL);
INSERT INTO store_action_states VALUES (2, 4, 11, 'reg', '2014-10-01 11:23:06.018514', NULL);
INSERT INTO store_action_states VALUES (2, 4, 12, 'accepted', '2014-10-01 11:23:06.018514', NULL);
INSERT INTO store_action_states VALUES (2, 3, 13, 'paid', '2014-10-01 11:23:06.018514', NULL);
INSERT INTO store_action_states VALUES (2, 3, 14, 'accepted', '2014-10-01 11:23:06.018514', NULL);

SELECT pg_catalog.setval('store_action_states_state_id_seq', 14, true);

INSERT INTO store_changes VALUES (1, 4, 4, false, 'status', 'pending', 'inactive');
INSERT INTO store_changes VALUES (2, 4, 11, false, 'status', 'pending', 'inactive');

SELECT pg_catalog.setval('stores_store_id_seq', 2, true);

SELECT pg_catalog.setval('tokens_token_id_seq', 723, true);












--Change all passwords to 123123123
update ads set salted_passwd =  E'$1024$2y1bQ}VYRD}zF4QCbf07a971b784888658a20e62156de64dfde5ffb6', passwd = null;
update accounts set salted_passwd =  E'$1024$2y1bQ}VYRD}zF4QCbf07a971b784888658a20e62156de64dfde5ffb6';
