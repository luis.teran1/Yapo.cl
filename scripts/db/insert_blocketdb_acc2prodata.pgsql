--
-- PostgreSQL database dump
--

BEGIN;
SET CONSTRAINTS ALL DEFERRED;

TRUNCATE TABLE public.admins CASCADE;
TRUNCATE TABLE public.abuse_locks CASCADE;
TRUNCATE TABLE public.abuse_reporters CASCADE;
TRUNCATE TABLE public.users CASCADE;
TRUNCATE TABLE public.abuse_reports CASCADE;
TRUNCATE TABLE public.ads CASCADE;
TRUNCATE TABLE public.dashboard_ads CASCADE;
TRUNCATE TABLE public.payment_groups CASCADE;
TRUNCATE TABLE public.ad_actions CASCADE;
TRUNCATE TABLE public.action_params CASCADE;
TRUNCATE TABLE public.stores CASCADE;
TRUNCATE TABLE public.tokens CASCADE;
TRUNCATE TABLE public.action_states CASCADE;
TRUNCATE TABLE public.ad_changes CASCADE;
TRUNCATE TABLE public.ad_codes CASCADE;
TRUNCATE TABLE public.ad_image_changes CASCADE;
TRUNCATE TABLE public.ad_images CASCADE;
TRUNCATE TABLE public.ad_images_digests CASCADE;
TRUNCATE TABLE public.ad_media CASCADE;
TRUNCATE TABLE public.ad_media_changes CASCADE;
TRUNCATE TABLE public.ad_params CASCADE;
TRUNCATE TABLE public.ad_queues CASCADE;
TRUNCATE TABLE public.admin_privs CASCADE;
TRUNCATE TABLE public.bid_ads CASCADE;
TRUNCATE TABLE public.bid_bids CASCADE;
TRUNCATE TABLE public.bid_media CASCADE;
TRUNCATE TABLE public.block_lists CASCADE;
TRUNCATE TABLE public.block_rules CASCADE;
TRUNCATE TABLE public.block_rule_conditions CASCADE;
TRUNCATE TABLE public.blocked_items CASCADE;
TRUNCATE TABLE public.conf CASCADE;
TRUNCATE TABLE public.event_log CASCADE;
TRUNCATE TABLE public.example CASCADE;
TRUNCATE TABLE public.filters CASCADE;
TRUNCATE TABLE public.hold_mail_params CASCADE;
TRUNCATE TABLE public.iteminfo_items CASCADE;
TRUNCATE TABLE public.iteminfo_data CASCADE;
TRUNCATE TABLE public.mail_log CASCADE;
TRUNCATE TABLE public.mail_queue CASCADE;
TRUNCATE TABLE public.mama_attribute_wordlists CASCADE;
TRUNCATE TABLE public.mama_attribute_categories CASCADE;
TRUNCATE TABLE public.mama_main_backup CASCADE;
TRUNCATE TABLE public.mama_attribute_categories_backup CASCADE;
TRUNCATE TABLE public.mama_attribute_wordlists_backup CASCADE;
TRUNCATE TABLE public.mama_attribute_words CASCADE;
TRUNCATE TABLE public.mama_attribute_words_backup CASCADE;
TRUNCATE TABLE public.mama_exception_lists CASCADE;
TRUNCATE TABLE public.mama_exception_lists_backup CASCADE;
TRUNCATE TABLE public.mama_exception_words CASCADE;
TRUNCATE TABLE public.mama_exception_words_backup CASCADE;
TRUNCATE TABLE public.mama_wordlists CASCADE;
TRUNCATE TABLE public.mama_wordlists_backup CASCADE;
TRUNCATE TABLE public.mama_words CASCADE;
TRUNCATE TABLE public.mama_words_backup CASCADE;
TRUNCATE TABLE public.most_popular_ads CASCADE;
TRUNCATE TABLE public.notices CASCADE;
TRUNCATE TABLE public.on_call CASCADE;
TRUNCATE TABLE public.on_call_actions CASCADE;
TRUNCATE TABLE public.pageviews_per_reg_cat CASCADE;
TRUNCATE TABLE public.pay_log CASCADE;
TRUNCATE TABLE public.pay_log_references CASCADE;
TRUNCATE TABLE public.payments CASCADE;
TRUNCATE TABLE public.purchase CASCADE;
TRUNCATE TABLE public.pricelist CASCADE;
TRUNCATE TABLE public.redir_stats CASCADE;
TRUNCATE TABLE public.review_log CASCADE;
TRUNCATE TABLE public.sms_users CASCADE;
TRUNCATE TABLE public.sms_log CASCADE;
TRUNCATE TABLE public.watch_users CASCADE;
TRUNCATE TABLE public.watch_queries CASCADE;
TRUNCATE TABLE public.sms_log_watch CASCADE;
TRUNCATE TABLE public.state_params CASCADE;
TRUNCATE TABLE public.stats_daily CASCADE;
TRUNCATE TABLE public.stats_daily_ad_actions CASCADE;
TRUNCATE TABLE public.stats_hourly CASCADE;
TRUNCATE TABLE public.store_actions CASCADE;
TRUNCATE TABLE public.store_action_states CASCADE;
TRUNCATE TABLE public.store_changes CASCADE;
TRUNCATE TABLE public.store_login_tokens CASCADE;
TRUNCATE TABLE public.store_params CASCADE;
TRUNCATE TABLE public.synonyms CASCADE;
TRUNCATE TABLE public.trans_queue CASCADE;
TRUNCATE TABLE public.unfinished_ads CASCADE;
TRUNCATE TABLE public.user_params CASCADE;
TRUNCATE TABLE public.user_testimonial CASCADE;
TRUNCATE TABLE public.visitor CASCADE;
TRUNCATE TABLE public.visits CASCADE;
TRUNCATE TABLE public.vouchers CASCADE;
TRUNCATE TABLE public.voucher_actions CASCADE;
TRUNCATE TABLE public.voucher_states CASCADE;
TRUNCATE TABLE public.watch_ads CASCADE;
TRUNCATE TABLE public.accounts CASCADE;
TRUNCATE TABLE public.account_params CASCADE;
COMMIT;


--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'LATIN1';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;

SET search_path = blocket_2009, pg_catalog;

--
-- Data for Name: ads; Type: TABLE DATA; Schema: blocket_2009; Owner: -
--



--
-- Data for Name: payment_groups; Type: TABLE DATA; Schema: blocket_2009; Owner: -
--



--
-- Data for Name: ad_actions; Type: TABLE DATA; Schema: blocket_2009; Owner: -
--



--
-- Data for Name: action_params; Type: TABLE DATA; Schema: blocket_2009; Owner: -
--



--
-- Data for Name: tokens; Type: TABLE DATA; Schema: blocket_2009; Owner: -
--



--
-- Data for Name: action_states; Type: TABLE DATA; Schema: blocket_2009; Owner: -
--



--
-- Data for Name: ad_changes; Type: TABLE DATA; Schema: blocket_2009; Owner: -
--



--
-- Data for Name: ad_image_changes; Type: TABLE DATA; Schema: blocket_2009; Owner: -
--



--
-- Data for Name: ad_images; Type: TABLE DATA; Schema: blocket_2009; Owner: -
--



--
-- Data for Name: ad_media; Type: TABLE DATA; Schema: blocket_2009; Owner: -
--



--
-- Data for Name: ad_media_changes; Type: TABLE DATA; Schema: blocket_2009; Owner: -
--



--
-- Data for Name: ad_params; Type: TABLE DATA; Schema: blocket_2009; Owner: -
--



--
-- Data for Name: mail_queue; Type: TABLE DATA; Schema: blocket_2009; Owner: -
--



--
-- Data for Name: notices; Type: TABLE DATA; Schema: blocket_2009; Owner: -
--



--
-- Data for Name: pay_log; Type: TABLE DATA; Schema: blocket_2009; Owner: -
--



--
-- Data for Name: pay_log_references; Type: TABLE DATA; Schema: blocket_2009; Owner: -
--



--
-- Data for Name: payments; Type: TABLE DATA; Schema: blocket_2009; Owner: -
--



--
-- Data for Name: purchase; Type: TABLE DATA; Schema: blocket_2009; Owner: -
--



--
-- Data for Name: purchase_detail; Type: TABLE DATA; Schema: blocket_2009; Owner: -
--



--
-- Data for Name: purchase_states; Type: TABLE DATA; Schema: blocket_2009; Owner: -
--



--
-- Data for Name: state_params; Type: TABLE DATA; Schema: blocket_2009; Owner: -
--



--
-- Data for Name: stats; Type: TABLE DATA; Schema: blocket_2009; Owner: -
--



--
-- Data for Name: stats_detail; Type: TABLE DATA; Schema: blocket_2009; Owner: -
--



--
-- Name: stats_stat_id_seq; Type: SEQUENCE SET; Schema: blocket_2009; Owner: -
--

SELECT pg_catalog.setval('stats_stat_id_seq', 1, false);


--
-- Data for Name: voucher_actions; Type: TABLE DATA; Schema: blocket_2009; Owner: -
--



--
-- Data for Name: voucher_states; Type: TABLE DATA; Schema: blocket_2009; Owner: -
--



SET search_path = blocket_2010, pg_catalog;

--
-- Data for Name: ads; Type: TABLE DATA; Schema: blocket_2010; Owner: -
--



--
-- Data for Name: payment_groups; Type: TABLE DATA; Schema: blocket_2010; Owner: -
--



--
-- Data for Name: ad_actions; Type: TABLE DATA; Schema: blocket_2010; Owner: -
--



--
-- Data for Name: action_params; Type: TABLE DATA; Schema: blocket_2010; Owner: -
--



--
-- Data for Name: tokens; Type: TABLE DATA; Schema: blocket_2010; Owner: -
--



--
-- Data for Name: action_states; Type: TABLE DATA; Schema: blocket_2010; Owner: -
--



--
-- Data for Name: ad_changes; Type: TABLE DATA; Schema: blocket_2010; Owner: -
--



--
-- Data for Name: ad_image_changes; Type: TABLE DATA; Schema: blocket_2010; Owner: -
--



--
-- Data for Name: ad_images; Type: TABLE DATA; Schema: blocket_2010; Owner: -
--



--
-- Data for Name: ad_media; Type: TABLE DATA; Schema: blocket_2010; Owner: -
--



--
-- Data for Name: ad_media_changes; Type: TABLE DATA; Schema: blocket_2010; Owner: -
--



--
-- Data for Name: ad_params; Type: TABLE DATA; Schema: blocket_2010; Owner: -
--



--
-- Data for Name: mail_queue; Type: TABLE DATA; Schema: blocket_2010; Owner: -
--



--
-- Data for Name: notices; Type: TABLE DATA; Schema: blocket_2010; Owner: -
--



--
-- Data for Name: pay_log; Type: TABLE DATA; Schema: blocket_2010; Owner: -
--



--
-- Data for Name: pay_log_references; Type: TABLE DATA; Schema: blocket_2010; Owner: -
--



--
-- Data for Name: payments; Type: TABLE DATA; Schema: blocket_2010; Owner: -
--



--
-- Data for Name: purchase; Type: TABLE DATA; Schema: blocket_2010; Owner: -
--



--
-- Data for Name: purchase_detail; Type: TABLE DATA; Schema: blocket_2010; Owner: -
--



--
-- Data for Name: purchase_states; Type: TABLE DATA; Schema: blocket_2010; Owner: -
--



--
-- Data for Name: state_params; Type: TABLE DATA; Schema: blocket_2010; Owner: -
--



--
-- Data for Name: stats; Type: TABLE DATA; Schema: blocket_2010; Owner: -
--



--
-- Data for Name: stats_detail; Type: TABLE DATA; Schema: blocket_2010; Owner: -
--



--
-- Name: stats_stat_id_seq; Type: SEQUENCE SET; Schema: blocket_2010; Owner: -
--

SELECT pg_catalog.setval('stats_stat_id_seq', 1, false);


--
-- Data for Name: voucher_actions; Type: TABLE DATA; Schema: blocket_2010; Owner: -
--



--
-- Data for Name: voucher_states; Type: TABLE DATA; Schema: blocket_2010; Owner: -
--



SET search_path = blocket_2011, pg_catalog;

--
-- Data for Name: ads; Type: TABLE DATA; Schema: blocket_2011; Owner: -
--



--
-- Data for Name: payment_groups; Type: TABLE DATA; Schema: blocket_2011; Owner: -
--



--
-- Data for Name: ad_actions; Type: TABLE DATA; Schema: blocket_2011; Owner: -
--



--
-- Data for Name: action_params; Type: TABLE DATA; Schema: blocket_2011; Owner: -
--



--
-- Data for Name: tokens; Type: TABLE DATA; Schema: blocket_2011; Owner: -
--



--
-- Data for Name: action_states; Type: TABLE DATA; Schema: blocket_2011; Owner: -
--



--
-- Data for Name: ad_changes; Type: TABLE DATA; Schema: blocket_2011; Owner: -
--



--
-- Data for Name: ad_image_changes; Type: TABLE DATA; Schema: blocket_2011; Owner: -
--



--
-- Data for Name: ad_images; Type: TABLE DATA; Schema: blocket_2011; Owner: -
--



--
-- Data for Name: ad_media; Type: TABLE DATA; Schema: blocket_2011; Owner: -
--



--
-- Data for Name: ad_media_changes; Type: TABLE DATA; Schema: blocket_2011; Owner: -
--



--
-- Data for Name: ad_params; Type: TABLE DATA; Schema: blocket_2011; Owner: -
--



--
-- Data for Name: mail_queue; Type: TABLE DATA; Schema: blocket_2011; Owner: -
--



--
-- Data for Name: notices; Type: TABLE DATA; Schema: blocket_2011; Owner: -
--



--
-- Data for Name: pay_log; Type: TABLE DATA; Schema: blocket_2011; Owner: -
--



--
-- Data for Name: pay_log_references; Type: TABLE DATA; Schema: blocket_2011; Owner: -
--



--
-- Data for Name: payments; Type: TABLE DATA; Schema: blocket_2011; Owner: -
--



--
-- Data for Name: purchase; Type: TABLE DATA; Schema: blocket_2011; Owner: -
--



--
-- Data for Name: purchase_detail; Type: TABLE DATA; Schema: blocket_2011; Owner: -
--



--
-- Data for Name: purchase_states; Type: TABLE DATA; Schema: blocket_2011; Owner: -
--



--
-- Data for Name: state_params; Type: TABLE DATA; Schema: blocket_2011; Owner: -
--



--
-- Data for Name: stats; Type: TABLE DATA; Schema: blocket_2011; Owner: -
--



--
-- Data for Name: stats_detail; Type: TABLE DATA; Schema: blocket_2011; Owner: -
--



--
-- Name: stats_stat_id_seq; Type: SEQUENCE SET; Schema: blocket_2011; Owner: -
--

SELECT pg_catalog.setval('stats_stat_id_seq', 1, false);


--
-- Data for Name: voucher_actions; Type: TABLE DATA; Schema: blocket_2011; Owner: -
--



--
-- Data for Name: voucher_states; Type: TABLE DATA; Schema: blocket_2011; Owner: -
--



SET search_path = blocket_2012, pg_catalog;

--
-- Data for Name: ads; Type: TABLE DATA; Schema: blocket_2012; Owner: -
--



--
-- Data for Name: payment_groups; Type: TABLE DATA; Schema: blocket_2012; Owner: -
--



--
-- Data for Name: ad_actions; Type: TABLE DATA; Schema: blocket_2012; Owner: -
--



--
-- Data for Name: action_params; Type: TABLE DATA; Schema: blocket_2012; Owner: -
--



--
-- Data for Name: tokens; Type: TABLE DATA; Schema: blocket_2012; Owner: -
--



--
-- Data for Name: action_states; Type: TABLE DATA; Schema: blocket_2012; Owner: -
--



--
-- Data for Name: ad_changes; Type: TABLE DATA; Schema: blocket_2012; Owner: -
--



--
-- Data for Name: ad_image_changes; Type: TABLE DATA; Schema: blocket_2012; Owner: -
--



--
-- Data for Name: ad_images; Type: TABLE DATA; Schema: blocket_2012; Owner: -
--



--
-- Data for Name: ad_media; Type: TABLE DATA; Schema: blocket_2012; Owner: -
--



--
-- Data for Name: ad_media_changes; Type: TABLE DATA; Schema: blocket_2012; Owner: -
--



--
-- Data for Name: ad_params; Type: TABLE DATA; Schema: blocket_2012; Owner: -
--



--
-- Data for Name: mail_queue; Type: TABLE DATA; Schema: blocket_2012; Owner: -
--



--
-- Data for Name: notices; Type: TABLE DATA; Schema: blocket_2012; Owner: -
--



--
-- Data for Name: pay_log; Type: TABLE DATA; Schema: blocket_2012; Owner: -
--



--
-- Data for Name: pay_log_references; Type: TABLE DATA; Schema: blocket_2012; Owner: -
--



--
-- Data for Name: payments; Type: TABLE DATA; Schema: blocket_2012; Owner: -
--



--
-- Data for Name: purchase; Type: TABLE DATA; Schema: blocket_2012; Owner: -
--



--
-- Data for Name: purchase_detail; Type: TABLE DATA; Schema: blocket_2012; Owner: -
--



--
-- Data for Name: purchase_states; Type: TABLE DATA; Schema: blocket_2012; Owner: -
--



--
-- Data for Name: state_params; Type: TABLE DATA; Schema: blocket_2012; Owner: -
--



--
-- Data for Name: stats; Type: TABLE DATA; Schema: blocket_2012; Owner: -
--



--
-- Data for Name: stats_detail; Type: TABLE DATA; Schema: blocket_2012; Owner: -
--



--
-- Name: stats_stat_id_seq; Type: SEQUENCE SET; Schema: blocket_2012; Owner: -
--

SELECT pg_catalog.setval('stats_stat_id_seq', 1, false);


--
-- Data for Name: voucher_actions; Type: TABLE DATA; Schema: blocket_2012; Owner: -
--



--
-- Data for Name: voucher_states; Type: TABLE DATA; Schema: blocket_2012; Owner: -
--



SET search_path = blocket_2013, pg_catalog;

--
-- Data for Name: ads; Type: TABLE DATA; Schema: blocket_2013; Owner: -
--



--
-- Data for Name: payment_groups; Type: TABLE DATA; Schema: blocket_2013; Owner: -
--



--
-- Data for Name: ad_actions; Type: TABLE DATA; Schema: blocket_2013; Owner: -
--



--
-- Data for Name: action_params; Type: TABLE DATA; Schema: blocket_2013; Owner: -
--



--
-- Data for Name: tokens; Type: TABLE DATA; Schema: blocket_2013; Owner: -
--



--
-- Data for Name: action_states; Type: TABLE DATA; Schema: blocket_2013; Owner: -
--



--
-- Data for Name: ad_changes; Type: TABLE DATA; Schema: blocket_2013; Owner: -
--



--
-- Data for Name: ad_image_changes; Type: TABLE DATA; Schema: blocket_2013; Owner: -
--



--
-- Data for Name: ad_images; Type: TABLE DATA; Schema: blocket_2013; Owner: -
--



--
-- Data for Name: ad_media; Type: TABLE DATA; Schema: blocket_2013; Owner: -
--



--
-- Data for Name: ad_media_changes; Type: TABLE DATA; Schema: blocket_2013; Owner: -
--



--
-- Data for Name: ad_params; Type: TABLE DATA; Schema: blocket_2013; Owner: -
--



--
-- Data for Name: mail_queue; Type: TABLE DATA; Schema: blocket_2013; Owner: -
--



--
-- Data for Name: notices; Type: TABLE DATA; Schema: blocket_2013; Owner: -
--



--
-- Data for Name: pay_log; Type: TABLE DATA; Schema: blocket_2013; Owner: -
--



--
-- Data for Name: pay_log_references; Type: TABLE DATA; Schema: blocket_2013; Owner: -
--



--
-- Data for Name: payments; Type: TABLE DATA; Schema: blocket_2013; Owner: -
--



--
-- Data for Name: purchase; Type: TABLE DATA; Schema: blocket_2013; Owner: -
--



--
-- Data for Name: purchase_detail; Type: TABLE DATA; Schema: blocket_2013; Owner: -
--



--
-- Data for Name: purchase_states; Type: TABLE DATA; Schema: blocket_2013; Owner: -
--



--
-- Data for Name: state_params; Type: TABLE DATA; Schema: blocket_2013; Owner: -
--



--
-- Data for Name: stats; Type: TABLE DATA; Schema: blocket_2013; Owner: -
--



--
-- Data for Name: stats_detail; Type: TABLE DATA; Schema: blocket_2013; Owner: -
--



--
-- Name: stats_stat_id_seq; Type: SEQUENCE SET; Schema: blocket_2013; Owner: -
--

SELECT pg_catalog.setval('stats_stat_id_seq', 1, false);


--
-- Data for Name: voucher_actions; Type: TABLE DATA; Schema: blocket_2013; Owner: -
--



--
-- Data for Name: voucher_states; Type: TABLE DATA; Schema: blocket_2013; Owner: -
--



SET search_path = blocket_2014, pg_catalog;

--
-- Data for Name: ads; Type: TABLE DATA; Schema: blocket_2014; Owner: -
--



--
-- Data for Name: payment_groups; Type: TABLE DATA; Schema: blocket_2014; Owner: -
--



--
-- Data for Name: ad_actions; Type: TABLE DATA; Schema: blocket_2014; Owner: -
--



--
-- Data for Name: action_params; Type: TABLE DATA; Schema: blocket_2014; Owner: -
--



--
-- Data for Name: tokens; Type: TABLE DATA; Schema: blocket_2014; Owner: -
--



--
-- Data for Name: action_states; Type: TABLE DATA; Schema: blocket_2014; Owner: -
--



--
-- Data for Name: ad_changes; Type: TABLE DATA; Schema: blocket_2014; Owner: -
--



--
-- Data for Name: ad_image_changes; Type: TABLE DATA; Schema: blocket_2014; Owner: -
--



--
-- Data for Name: ad_images; Type: TABLE DATA; Schema: blocket_2014; Owner: -
--



--
-- Data for Name: ad_media; Type: TABLE DATA; Schema: blocket_2014; Owner: -
--



--
-- Data for Name: ad_media_changes; Type: TABLE DATA; Schema: blocket_2014; Owner: -
--



--
-- Data for Name: ad_params; Type: TABLE DATA; Schema: blocket_2014; Owner: -
--



--
-- Data for Name: mail_queue; Type: TABLE DATA; Schema: blocket_2014; Owner: -
--



--
-- Data for Name: notices; Type: TABLE DATA; Schema: blocket_2014; Owner: -
--



--
-- Data for Name: pay_log; Type: TABLE DATA; Schema: blocket_2014; Owner: -
--



--
-- Data for Name: pay_log_references; Type: TABLE DATA; Schema: blocket_2014; Owner: -
--



--
-- Data for Name: payments; Type: TABLE DATA; Schema: blocket_2014; Owner: -
--



--
-- Data for Name: purchase; Type: TABLE DATA; Schema: blocket_2014; Owner: -
--



--
-- Data for Name: purchase_detail; Type: TABLE DATA; Schema: blocket_2014; Owner: -
--



--
-- Data for Name: purchase_states; Type: TABLE DATA; Schema: blocket_2014; Owner: -
--



--
-- Data for Name: state_params; Type: TABLE DATA; Schema: blocket_2014; Owner: -
--



--
-- Data for Name: stats; Type: TABLE DATA; Schema: blocket_2014; Owner: -
--



--
-- Data for Name: stats_detail; Type: TABLE DATA; Schema: blocket_2014; Owner: -
--



--
-- Name: stats_stat_id_seq; Type: SEQUENCE SET; Schema: blocket_2014; Owner: -
--

SELECT pg_catalog.setval('stats_stat_id_seq', 1, false);


--
-- Data for Name: voucher_actions; Type: TABLE DATA; Schema: blocket_2014; Owner: -
--



--
-- Data for Name: voucher_states; Type: TABLE DATA; Schema: blocket_2014; Owner: -
--



SET search_path = blocket_2015, pg_catalog;

--
-- Data for Name: ads; Type: TABLE DATA; Schema: blocket_2015; Owner: -
--



--
-- Data for Name: payment_groups; Type: TABLE DATA; Schema: blocket_2015; Owner: -
--



--
-- Data for Name: ad_actions; Type: TABLE DATA; Schema: blocket_2015; Owner: -
--



--
-- Data for Name: action_params; Type: TABLE DATA; Schema: blocket_2015; Owner: -
--



--
-- Data for Name: tokens; Type: TABLE DATA; Schema: blocket_2015; Owner: -
--



--
-- Data for Name: action_states; Type: TABLE DATA; Schema: blocket_2015; Owner: -
--



--
-- Data for Name: ad_changes; Type: TABLE DATA; Schema: blocket_2015; Owner: -
--



--
-- Data for Name: ad_image_changes; Type: TABLE DATA; Schema: blocket_2015; Owner: -
--



--
-- Data for Name: ad_images; Type: TABLE DATA; Schema: blocket_2015; Owner: -
--



--
-- Data for Name: ad_media; Type: TABLE DATA; Schema: blocket_2015; Owner: -
--



--
-- Data for Name: ad_media_changes; Type: TABLE DATA; Schema: blocket_2015; Owner: -
--



--
-- Data for Name: ad_params; Type: TABLE DATA; Schema: blocket_2015; Owner: -
--



--
-- Data for Name: mail_queue; Type: TABLE DATA; Schema: blocket_2015; Owner: -
--



--
-- Data for Name: notices; Type: TABLE DATA; Schema: blocket_2015; Owner: -
--



--
-- Data for Name: pay_log; Type: TABLE DATA; Schema: blocket_2015; Owner: -
--



--
-- Data for Name: pay_log_references; Type: TABLE DATA; Schema: blocket_2015; Owner: -
--



--
-- Data for Name: payments; Type: TABLE DATA; Schema: blocket_2015; Owner: -
--



--
-- Data for Name: purchase; Type: TABLE DATA; Schema: blocket_2015; Owner: -
--



--
-- Data for Name: purchase_detail; Type: TABLE DATA; Schema: blocket_2015; Owner: -
--



--
-- Data for Name: purchase_states; Type: TABLE DATA; Schema: blocket_2015; Owner: -
--



--
-- Data for Name: state_params; Type: TABLE DATA; Schema: blocket_2015; Owner: -
--



--
-- Data for Name: stats; Type: TABLE DATA; Schema: blocket_2015; Owner: -
--



--
-- Data for Name: stats_detail; Type: TABLE DATA; Schema: blocket_2015; Owner: -
--



--
-- Name: stats_stat_id_seq; Type: SEQUENCE SET; Schema: blocket_2015; Owner: -
--

SELECT pg_catalog.setval('stats_stat_id_seq', 1, false);


--
-- Data for Name: voucher_actions; Type: TABLE DATA; Schema: blocket_2015; Owner: -
--



--
-- Data for Name: voucher_states; Type: TABLE DATA; Schema: blocket_2015; Owner: -
--



SET search_path = blocket_2016, pg_catalog;

--
-- Data for Name: ads; Type: TABLE DATA; Schema: blocket_2016; Owner: -
--



--
-- Data for Name: payment_groups; Type: TABLE DATA; Schema: blocket_2016; Owner: -
--



--
-- Data for Name: ad_actions; Type: TABLE DATA; Schema: blocket_2016; Owner: -
--



--
-- Data for Name: action_params; Type: TABLE DATA; Schema: blocket_2016; Owner: -
--



--
-- Data for Name: tokens; Type: TABLE DATA; Schema: blocket_2016; Owner: -
--



--
-- Data for Name: action_states; Type: TABLE DATA; Schema: blocket_2016; Owner: -
--



--
-- Data for Name: ad_changes; Type: TABLE DATA; Schema: blocket_2016; Owner: -
--



--
-- Data for Name: ad_image_changes; Type: TABLE DATA; Schema: blocket_2016; Owner: -
--



--
-- Data for Name: ad_images; Type: TABLE DATA; Schema: blocket_2016; Owner: -
--



--
-- Data for Name: ad_media; Type: TABLE DATA; Schema: blocket_2016; Owner: -
--



--
-- Data for Name: ad_media_changes; Type: TABLE DATA; Schema: blocket_2016; Owner: -
--



--
-- Data for Name: ad_params; Type: TABLE DATA; Schema: blocket_2016; Owner: -
--



--
-- Data for Name: mail_queue; Type: TABLE DATA; Schema: blocket_2016; Owner: -
--



--
-- Data for Name: notices; Type: TABLE DATA; Schema: blocket_2016; Owner: -
--



--
-- Data for Name: pay_log; Type: TABLE DATA; Schema: blocket_2016; Owner: -
--



--
-- Data for Name: pay_log_references; Type: TABLE DATA; Schema: blocket_2016; Owner: -
--



--
-- Data for Name: payments; Type: TABLE DATA; Schema: blocket_2016; Owner: -
--



--
-- Data for Name: purchase; Type: TABLE DATA; Schema: blocket_2016; Owner: -
--



--
-- Data for Name: purchase_detail; Type: TABLE DATA; Schema: blocket_2016; Owner: -
--



--
-- Data for Name: purchase_states; Type: TABLE DATA; Schema: blocket_2016; Owner: -
--



--
-- Data for Name: state_params; Type: TABLE DATA; Schema: blocket_2016; Owner: -
--



--
-- Data for Name: stats; Type: TABLE DATA; Schema: blocket_2016; Owner: -
--



--
-- Data for Name: stats_detail; Type: TABLE DATA; Schema: blocket_2016; Owner: -
--



--
-- Name: stats_stat_id_seq; Type: SEQUENCE SET; Schema: blocket_2016; Owner: -
--

SELECT pg_catalog.setval('stats_stat_id_seq', 1, false);


--
-- Data for Name: voucher_actions; Type: TABLE DATA; Schema: blocket_2016; Owner: -
--



--
-- Data for Name: voucher_states; Type: TABLE DATA; Schema: blocket_2016; Owner: -
--



SET search_path = public, pg_catalog;

--
-- Data for Name: admins; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO admins (admin_id, username, passwd, fullname, jabbername, email, mobile, status) VALUES (2, 'erik', '2d541162141d281abb9f5ba1b14f8072a1d2d29b', 'Erik', 'erik@jabber', 'erik@blocket.se', '070-111111', 'active');
INSERT INTO admins (admin_id, username, passwd, fullname, jabbername, email, mobile, status) VALUES (3, 'zakay', '2d541162141d281abb9f5ba1b14f8072a1d2d29b', 'Zakay', 'zakay@jabber', 'zakay@blocket.se', '070-111111', 'active');
INSERT INTO admins (admin_id, username, passwd, fullname, jabbername, email, mobile, status) VALUES (4, 'thomas', 'a94a8fe5ccb19ba61c4c0873d391e987982fbbd3', 'Thomas', 'thomas@jabber', 'test@schibstediberica.es', '070-111111', 'active');
INSERT INTO admins (admin_id, username, passwd, fullname, jabbername, email, mobile, status) VALUES (5, 'kjell', '430985da851e9223368e820ebde5beaf6c6ef1cc', 'Kjell', 'kjell@jabber', 'kjell@blocket.se', '0709-6459256', 'active');
INSERT INTO admins (admin_id, username, passwd, fullname, jabbername, email, mobile, status) VALUES (6, 'torsten', '430985da851e9223368e820ebde5beaf6c6ef1cc', 'Torsten Svensson', 'torsten@jabber', 'svara-inte@blocket.se', '0709-6459256', 'deleted');
INSERT INTO admins (admin_id, username, passwd, fullname, jabbername, email, mobile, status) VALUES (50, 'mama', '67b757a4f76e5fda3fe86c8a9009630a5e53a053', 'M.A.M.A.', 'blocket1@jabber', 'blocket1@blocket.se', '0733555501', 'active');
INSERT INTO admins (admin_id, username, passwd, fullname, jabbername, email, mobile, status) VALUES (51, 'bender00', '67b757a4f76e5fda3fe86c8a9009630a5e53a053', 'M.A.M.A. bender 00', NULL, NULL, NULL, 'active');
INSERT INTO admins (admin_id, username, passwd, fullname, jabbername, email, mobile, status) VALUES (52, 'bender01', '67b757a4f76e5fda3fe86c8a9009630a5e53a053', 'M.A.M.A. bender 01', NULL, NULL, NULL, 'active');
INSERT INTO admins (admin_id, username, passwd, fullname, jabbername, email, mobile, status) VALUES (53, 'bender02', '67b757a4f76e5fda3fe86c8a9009630a5e53a053', 'M.A.M.A. bender 02', NULL, NULL, NULL, 'active');
INSERT INTO admins (admin_id, username, passwd, fullname, jabbername, email, mobile, status) VALUES (54, 'bender03', '67b757a4f76e5fda3fe86c8a9009630a5e53a053', 'M.A.M.A. bender 03', NULL, NULL, NULL, 'active');
INSERT INTO admins (admin_id, username, passwd, fullname, jabbername, email, mobile, status) VALUES (55, 'bender04', '67b757a4f76e5fda3fe86c8a9009630a5e53a053', 'M.A.M.A. bender 04', NULL, NULL, NULL, 'active');
INSERT INTO admins (admin_id, username, passwd, fullname, jabbername, email, mobile, status) VALUES (9, 'dany', '118ab5d614b5a8d4222fc7eee1609793e4014800', NULL, NULL, 'dany@mazaru.schibsted.cl', NULL, 'active');
INSERT INTO admins (admin_id, username, passwd, fullname, jabbername, email, mobile, status) VALUES (10, 'admin', '601f1889667efaebb33b8c12572835da3f027f78', 'admin', 'admin', 'admin@admin.cl', NULL, 'active');
INSERT INTO admins (admin_id, username, passwd, fullname, jabbername, email, mobile, status) VALUES (23, 'blocket', '430985da851e9223368e820ebde5beaf6c6ef1cc', 'Blocket', NULL, 'admin@admin.cl', NULL, 'active');


--
-- Data for Name: abuse_locks; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Name: abuse_locks_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('abuse_locks_id_seq', 1, false);


--
-- Data for Name: abuse_reporters; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO users (user_id, uid, email, account, paid_total) VALUES (1, 1, 'uid1@blocket.se', 0, 0);
INSERT INTO users (user_id, uid, email, account, paid_total) VALUES (2, 2, 'uid2@blocket.se', 0, 100);
INSERT INTO users (user_id, uid, email, account, paid_total) VALUES (3, 3, 'prepaid3@blocket.se', 1000, 1000);
INSERT INTO users (user_id, uid, email, account, paid_total) VALUES (4, 4, 'prepaid@blocket.se', 100000, 1000);
INSERT INTO users (user_id, uid, email, account, paid_total) VALUES (5, 5, 'prepaid5@blocket.se', 1000, 1000);
INSERT INTO users (user_id, uid, email, account, paid_total) VALUES (6, 6, 'kim@blocket.se', 1000, 1000);
INSERT INTO users (user_id, uid, email, account, paid_total) VALUES (50, 50, 'test@cuenta.com', 0, 0);
INSERT INTO users (user_id, uid, email, account, paid_total) VALUES (51, 51, 'usuario01@schibsted.cl', 0, 0);
INSERT INTO users (user_id, uid, email, account, paid_total) VALUES (52, 50, 'test2@cuenta.com', 0, 0);
INSERT INTO users (user_id, uid, email, account, paid_total) VALUES (53, 51, 'usuario02@schibsted.cl', 0, 0);
INSERT INTO users (user_id, uid, email, account, paid_total) VALUES (54, 50, 'test3@cuenta.com', 0, 0);
INSERT INTO users (user_id, uid, email, account, paid_total) VALUES (55, 51, 'usuario03@schibsted.cl', 0, 0);
INSERT INTO users (user_id, uid, email, account, paid_total) VALUES (56, 50, 'test4@cuenta.com', 0, 0);
INSERT INTO users (user_id, uid, email, account, paid_total) VALUES (57, 51, 'usuario04@schibsted.cl', 0, 0);
INSERT INTO users (user_id, uid, email, account, paid_total) VALUES (58, 51, 'usuario05@schibsted.cl', 0, 0);
INSERT INTO users (user_id, uid, email, account, paid_total) VALUES (59, 50, 'test5@cuenta.com', 0, 0);
INSERT INTO users (user_id, uid, email, account, paid_total) VALUES (60, 50, 'test6@cuenta.com', 0, 0);
INSERT INTO users (user_id, uid, email, account, paid_total) VALUES (61, 51, 'usuario07@schibsted.cl', 0, 0);


--
-- Data for Name: abuse_reports; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Data for Name: accounts; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO accounts (account_id, name, rut, email, phone, is_company, region, salted_passwd, creation_date, activation_date, status, address, contact, lob, commune, user_id) VALUES (1, 'cuenta privada', '', 'test@cuenta.com', '12321312', false, 15, '$1024$M%8h$#n>\\yU8qI7Lb36d8e10ee903c87cf5de09d413589509a856181', '2014-05-23 10:31:40.784965', NULL, 'active', NULL, NULL, NULL, NULL, 50);
INSERT INTO accounts (account_id, name, rut, email, phone, is_company, region, salted_passwd, creation_date, activation_date, status, address, contact, lob, commune, user_id) VALUES (2, 'test2 CUENTA', '', 'test2@cuenta.com', '1232134', false, 15, '$1024$`Qzo-[xSj?3j''`~N865bac2ed9d068a33bd7473e720a5d33e4d063e1', '2014-05-23 10:50:53.860955', NULL, 'active', NULL, NULL, NULL, NULL, 52);
INSERT INTO accounts (account_id, name, rut, email, phone, is_company, region, salted_passwd, creation_date, activation_date, status, address, contact, lob, commune, user_id) VALUES (3, 'test 3 cuenta', '', 'test3@cuenta.com', '123123412', false, 15, '$1024$>?QpUn_4Ip2cMz2\\c45902ec1cc345169b5b99d3a43ca61d07b4afd1', '2014-05-23 11:00:49.622153', NULL, 'active', NULL, NULL, NULL, NULL, 54);
INSERT INTO accounts (account_id, name, rut, email, phone, is_company, region, salted_passwd, creation_date, activation_date, status, address, contact, lob, commune, user_id) VALUES (4, 'test 4 cuenta', '', 'test4@cuenta.com', '12312312', false, 15, '$1024$f8:.N+4Z''c;R8gSB8ade7c8cdeced310d1c321937f8ff1888d1b34d5', '2014-05-23 11:17:39.278238', NULL, 'active', NULL, NULL, NULL, NULL, 56);
INSERT INTO accounts (account_id, name, rut, email, phone, is_company, region, salted_passwd, creation_date, activation_date, status, address, contact, lob, commune, user_id) VALUES (5, 'test 5 cuenta', '', 'test5@cuenta.com', '123123412', false, 15, '$1024$9t}[fV^Ep;8yENk)3f814c18c43462bf99d42180e1c6e38ecd09be66', '2014-05-23 11:28:28.820645', NULL, 'active', NULL, NULL, NULL, NULL, 59);
INSERT INTO accounts (account_id, name, rut, email, phone, is_company, region, salted_passwd, creation_date, activation_date, status, address, contact, lob, commune, user_id) VALUES (6, 'test 6 cuenta', '', 'test6@cuenta.com', '123123', false, 15, '$1024$<*TZQgi/=9'',PLN"0dd36b819cae1633ab85795dcd309555f0c93563', '2014-05-23 11:39:51.300478', NULL, 'active', NULL, NULL, NULL, NULL, 60);


--
-- Data for Name: account_params; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Name: accounts_account_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('accounts_account_id_seq', 6, true);


--
-- Data for Name: ads; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (1, 6000667, '2011-11-09 11:00:00', 'active', 'sell', 'Aurelio Rodr�guez', '1231231231', 2, 0, 1020, 5, '11111', false, false, false, 'Departamento Tarapac�', 'Con 2 dormitorios, en Alto Hospicio, 250m2 de espacio y 2 plazas de garaje', 45000000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'es');
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (2, NULL, NULL, 'inactive', 'sell', 'Thomas Svensson', '08-112233', 11, 0, 4100, 3, 'testb', false, false, false, 'Barncykel', 'Röd barncykel, 28 tum.', 666, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'es');
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (3, NULL, NULL, 'inactive', 'sell', 'Sven Ingvars', '08-121314', 11, 0, 4100, 5, 'testc', false, false, false, 'Damcykel', 'Damcykel med stång i mitten. 33,2 tum i plast.', 667, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'es');
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (4, NULL, NULL, 'inactive', 'sell', 'Klas Klasson', '08-121314', 11, 0, 4100, 5, 'testc', true, true, true, 'Hustomte', 'En tre fot stor hustomte.', 667, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'es');
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (5, NULL, NULL, 'inactive', 'sell', 'Dummy', '08-121314', 11, 0, 4100, 5, 'testc', false, false, false, 'Damcykel', 'Damcykel med stång i mitten. 33,2 tum i plast.', 667, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'es');
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (6, 3456789, '2006-04-05 09:21:31', 'active', 'sell', 'Bengt Bedrup', '08-121314', 10, 0, 2020, 5, '11111', true, true, false, 'Race car', 'Car with two doors ...
33.2-inch wheels made of plastic.', 11667, NULL, NULL, 'Testar länk', NULL, NULL, NULL, NULL, NULL, 'es');
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (7, NULL, NULL, 'inactive', 'sell', 'Dummy', '08-121314', 11, 0, 4100, 5, 'testc', false, false, false, 'Damcykel', 'Damcykel med stång i mitten. 33,2 tum i plast.', 667, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'es');
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (8, 6431717, '2006-04-06 09:21:31', 'active', 'sell', 'Juan P�rez', '084123456', 11, 0, 5020, 1, '11111', false, false, false, 'Una mesa', 'Y qu� mesa... menuda mesa!!', 157000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'es');
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (9, 999999, NULL, 'inactive', 'sell', 'Dummy', '08-121314', 11, 0, 4100, 1, 'testc', false, false, false, 'Damcykel med blahonga', 'Damcykel med stång rakt upp i mitten. 33,2 tum i plast.', 667, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'es');
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (10, 6000666, '2006-04-05 10:21:31', 'active', 'sell', 'Cristobal Col�n', '0812131491', 12, 0, 6020, 5, '11111', false, false, false, 'Pesas muy grandes', 'Una de 120 kg y la otra de 57, mas ligera.', 90000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'es');
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (11, 6394117, '2006-04-07 11:21:31', 'active', 'sell', 'Andrea Gonz�lez', '0987654321', 13, 0, 7060, 1, '11111', false, false, false, 'Peluquera a domicilio', 'Voy, corto el pelo, me pagas y m voy....', 12000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'es');
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (12, 6394118, '2011-11-06 11:21:31', 'active', 'let', 'Boris Felipe', '0987654000', 15, 0, 1040, 1, '11111', false, false, false, 'Arriendo mi preciosa casa en �u�oa', 'Dos habitaciones, metros cuadrados.... hect�reas dir�a yo... y plazas de garaje a tutipl�n', 500000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'es');
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (27, 8000007, '2014-05-23 11:00:34', 'active', 'let', 'test2 CUENTA', '1232134', 15, 0, 1040, 52, NULL, false, false, false, 'Casa', 'arriendo casaaaa', 20000, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$`Qzo-[xSj?3j''`~N865bac2ed9d068a33bd7473e720a5d33e4d063e1', '2014-05-23 11:00:34.141472', 'es');
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (20, 8000000, '2014-05-23 10:40:18', 'active', 'sell', 'Usuario 01', '55554444', 15, 0, 6200, 51, NULL, false, false, false, 'Cremas', 'Para la piel', 10000, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$c,5k|{vSCz!m:",Vfd35df761024f0d61f1b81a045269ce6a73f3a78', '2014-05-23 10:40:18.736328', 'es');
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (36, 8000016, '2014-05-23 11:05:36', 'active', 'buy', 'test 3 cuenta', '123123412', 15, 0, 1040, 54, NULL, false, false, false, 'Casaaa', 'dasda', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$>?QpUn_4Ip2cMz2\\c45902ec1cc345169b5b99d3a43ca61d07b4afd1', '2014-05-23 11:05:36.404043', 'es');
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (21, 8000001, '2014-05-23 10:40:21', 'active', 'sell', 'Usuario 01', '55554444', 15, 0, 6080, 51, NULL, false, false, false, 'Saxofon', 'Lorem ipsum dolor sit amet', 9000, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$(l]l]''#(b{Sa`m ^7c3ced395db1f3554a90a03a6c1bfba1e376bd77', '2014-05-23 10:40:21.127437', 'es');
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (28, 8000008, '2014-05-23 11:00:39', 'active', 'sell', 'test2 CUENTA', '1232134', 15, 0, 2060, 52, NULL, false, false, false, 'Super moto', 'anti golpes.', 20000, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$`Qzo-[xSj?3j''`~N865bac2ed9d068a33bd7473e720a5d33e4d063e1', '2014-05-23 11:00:39.332816', 'es');
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (22, 8000002, '2014-05-23 10:40:28', 'active', 'sell', 'cuenta privada', '12321312', 15, 0, 7020, 50, NULL, false, false, false, 'Empleo', 'test', 11111, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$M%8h$#n>\\yU8qI7Lb36d8e10ee903c87cf5de09d413589509a856181', '2014-05-23 10:40:28.211644', 'es');
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (33, 8000012, '2014-05-23 11:04:43', 'active', 'buy', 'Usuario 03', '55554444', 15, 0, 2080, 55, NULL, false, false, false, 'Barco', 'lorem ipsum dolor sit amet', 123123, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$hJGyBC_,WQgOON0H3084a01d4ecf07c09c45d5f2a6a715b635289483', '2014-05-23 11:04:43.312292', 'es');
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (23, 8000003, '2014-05-23 10:40:36', 'active', 'sell', 'cuenta privada', '12321312', 15, 0, 8020, 50, NULL, false, false, false, 'Otros', 'test', 23232, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$M%8h$#n>\\yU8qI7Lb36d8e10ee903c87cf5de09d413589509a856181', '2014-05-23 10:40:36.136996', 'es');
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (29, 8000009, '2014-05-23 11:00:42', 'active', 'sell', 'Usuario 02', '55554444', 15, 0, 1020, 53, NULL, false, false, false, 'Departamento', 'lorem ipsum dolor sit amet', 123123, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$~]gq{vH#Zr6ie>s#e678dfab5356ebdc75b708c6dcbe9415b21ebc8b', '2014-05-23 11:00:42.490199', 'es');
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (24, 8000004, '2014-05-23 11:00:20', 'active', 'sell', 'test2 CUENTA', '1232134', 15, 0, 1020, 52, NULL, false, false, false, 'Depto', 'depto en venta', 1000, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$`Qzo-[xSj?3j''`~N865bac2ed9d068a33bd7473e720a5d33e4d063e1', '2014-05-23 11:00:20.258291', 'es');
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (25, 8000005, '2014-05-23 11:00:22', 'active', 'sell', 'test2 CUENTA', '1232134', 15, 0, 2020, 52, NULL, false, false, false, 'Acura Legend 2012', 'vendo', 123123, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$`Qzo-[xSj?3j''`~N865bac2ed9d068a33bd7473e720a5d33e4d063e1', '2014-05-23 11:00:22.804056', 'es');
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (30, 8000010, '2014-05-23 11:00:46', 'active', 'sell', 'Usuario 02', '55554444', 15, 0, 1040, 53, NULL, false, false, false, 'Casa', 'lorem ipsum dolor sit amet', 123123, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$$T#`Faj& T6[eP-+23f05b625b0a9df4a0156e5e865701d81965b012', '2014-05-23 11:00:46.511128', 'es');
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (26, 8000006, '2014-05-23 11:00:31', 'active', 'sell', 'test2 CUENTA', '1232134', 15, 0, 7020, 52, NULL, false, false, false, 'Ingeniero', 'se necesita ingeniero de software que programe por comida.', 3000, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$`Qzo-[xSj?3j''`~N865bac2ed9d068a33bd7473e720a5d33e4d063e1', '2014-05-23 11:00:31.272646', 'es');
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (32, 8000013, '2014-05-23 11:05:23', 'active', 'buy', 'Usuario 03', '55554444', 15, 0, 2040, 55, NULL, false, false, false, 'Camion', 'lorem ipsum', 123123, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$"^Cwo0tvc\\F:m#gee45f5dd4f3866228c06ae8d0f637cd18a791ad2c', '2014-05-23 11:05:23.311501', 'es');
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (37, 8000017, '2014-05-23 11:05:41', 'active', 'buy', 'test 3 cuenta', '123123412', 15, 0, 1080, 54, NULL, false, false, false, 'Local', 'local', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$>?QpUn_4Ip2cMz2\\c45902ec1cc345169b5b99d3a43ca61d07b4afd1', '2014-05-23 11:05:41.839998', 'es');
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (31, 8000011, '2014-05-23 11:00:48', 'active', 'sell', 'Usuario 02', '55554444', 15, 0, 1060, 53, NULL, false, false, false, 'Oficina', '123123', 123, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$h@:z?<4iT6w$)O?ye109a9482e9134b680f92202eae9c0dfd8e9edc9', '2014-05-23 11:00:48.871817', 'es');
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (34, 8000014, '2014-05-23 11:05:26', 'active', 'rent', 'test 3 cuenta', '123123412', 15, 0, 1020, 54, NULL, false, false, false, 'Necesito depto', 'sfsdfsd', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$>?QpUn_4Ip2cMz2\\c45902ec1cc345169b5b99d3a43ca61d07b4afd1', '2014-05-23 11:05:26.602346', 'es');
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (35, 8000015, '2014-05-23 11:05:31', 'active', 'buy', 'Usuario 03', '55554444', 15, 0, 2060, 55, NULL, false, false, false, 'Moto moto', 'Busco moto moto', 213123, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$/E<Oq{"C[TL&5}2@fc70549dd7215a9aa38dd86e5d3d5aad26c2ee27', '2014-05-23 11:05:31.766461', 'es');
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (39, 8000019, '2014-05-23 11:07:52', 'active', 'buy', 'test 3 cuenta', '123123412', 15, 0, 1020, 54, NULL, false, false, false, 'Depto', 'asdas', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$>?QpUn_4Ip2cMz2\\c45902ec1cc345169b5b99d3a43ca61d07b4afd1', '2014-05-23 11:07:52.153954', 'es');
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (38, 8000018, '2014-05-23 11:06:12', 'active', 'buy', 'test 3 cuenta', '123123412', 15, 0, 1060, 54, NULL, false, false, false, 'Oficina', 'aoaods', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$>?QpUn_4Ip2cMz2\\c45902ec1cc345169b5b99d3a43ca61d07b4afd1', '2014-05-23 11:06:12.22863', 'es');
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (41, 8000021, '2014-05-23 11:08:02', 'active', 'rent', 'test 3 cuenta', '123123412', 15, 0, 1080, 54, NULL, false, false, false, 'Local en cnavia', 'asdas', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$>?QpUn_4Ip2cMz2\\c45902ec1cc345169b5b99d3a43ca61d07b4afd1', '2014-05-23 11:08:02.325334', 'es');
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (40, 8000020, '2014-05-23 11:07:56', 'active', 'buy', 'Usuario 03', '55554444', 15, 0, 2120, 55, NULL, false, false, false, 'Bici', 'electrica', 123, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$}^e.*D2}$c''O1XKt94bd1df73fe6845a68503a6893c9c700bbd716b7', '2014-05-23 11:07:56.397469', 'es');
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (42, 8000022, '2014-05-23 11:10:15', 'active', 'buy', 'Usuario 03', '55554444', 15, 0, 2060, 55, NULL, false, false, false, 'Otra moto moto', '123123', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$ie{7WJ-0(TQe2JGk9d625e9a0a3b91292aa0bdad5af3a25292c6f8ba', '2014-05-23 11:10:15.442648', 'es');
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (43, 8000023, '2014-05-23 11:15:16', 'active', 'buy', 'Usuario 03', '55554444', 15, 0, 2080, 55, NULL, false, false, false, 'Lancha', 'lorem', 23, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$4Y1X(a;y<3GU'')NSb6d650181e47a634bf6b42821aeecdb6c0eeb09c', '2014-05-23 11:15:16.730681', 'es');
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (44, 8000024, '2014-05-23 11:20:48', 'active', 'sell', 'test 4 cuenta', '12312312', 15, 0, 1020, 56, NULL, false, false, false, 'Depto 1 test4', 'depto', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$f8:.N+4Z''c;R8gSB8ade7c8cdeced310d1c321937f8ff1888d1b34d5', '2014-05-23 11:20:48.013186', 'es');
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (46, 8000026, '2014-05-23 11:21:05', 'active', 'sell', 'test 4 cuenta', '12312312', 15, 0, 1020, 56, NULL, false, false, false, 'Depto 2 test4', 'depto', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$f8:.N+4Z''c;R8gSB8ade7c8cdeced310d1c321937f8ff1888d1b34d5', '2014-05-23 11:21:05.984052', 'es');
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (47, 8000027, '2014-05-23 11:21:09', 'active', 'sell', 'test 4 cuenta', '12312312', 15, 0, 1020, 56, NULL, false, false, false, 'Depto 3 test4', 'depto', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$f8:.N+4Z''c;R8gSB8ade7c8cdeced310d1c321937f8ff1888d1b34d5', '2014-05-23 11:21:09.944108', 'es');
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (45, 8000025, '2014-05-23 11:20:59', 'active', 'sell', 'Usuario 04', '55554444', 15, 0, 7060, 57, NULL, false, false, false, 'Limpiamos su Windows', 'De virus y cosas raras', 123123, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$=rmqLHh~Hp%FK9w50947523b35f2c08f7560351bfbe0cfb2d787b758', '2014-05-23 11:20:59.773138', 'es');
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (75, 8000055, '2014-05-23 17:41:06', 'active', 'sell', 'test 5 cuenta', '123123412', 15, 0, 2020, 59, NULL, false, false, false, 'Aro Standard 2012', 'fdsfsdf', 23242, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$9t}[fV^Ep;8yENk)3f814c18c43462bf99d42180e1c6e38ecd09be66', '2014-05-23 17:41:06.971216', 'es');
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (48, 8000028, '2014-05-23 11:22:17', 'active', 'sell', 'test 4 cuenta', '12312312', 15, 0, 1020, 56, NULL, false, false, false, 'Depto 4 test4', 'depto', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$f8:.N+4Z''c;R8gSB8ade7c8cdeced310d1c321937f8ff1888d1b34d5', '2014-05-23 11:22:17.904531', 'es');
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (49, 8000029, '2014-05-23 11:22:23', 'active', 'sell', 'Usuario 04', '55554444', 15, 0, 7060, 57, NULL, false, false, false, 'Aceleramos su Mac', 'para que vuele', 123, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$byyyolI>pt|''POcRbf0b4cb3763f5f45fc28dd26895385fc81e6e160', '2014-05-23 11:22:23.364362', 'es');
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (50, 8000030, '2014-05-23 11:22:28', 'active', 'sell', 'test 4 cuenta', '12312312', 15, 0, 1020, 56, NULL, false, false, false, 'Depto 5 test4', 'depto 5', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$f8:.N+4Z''c;R8gSB8ade7c8cdeced310d1c321937f8ff1888d1b34d5', '2014-05-23 11:22:28.039458', 'es');
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (67, 8000047, '2014-05-23 11:42:57', 'active', 'sell', 'test 6 cuenta', '123123', 15, 0, 1080, 60, NULL, false, false, false, 'Local test6', 'local', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$<*TZQgi/=9'',PLN"0dd36b819cae1633ab85795dcd309555f0c93563', '2014-05-23 11:42:57.256163', 'es');
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (51, 8000031, '2014-05-23 11:23:11', 'active', 'sell', 'Usuario 04', '55554444', 15, 0, 7060, 57, NULL, false, false, false, 'Para cuidar a los ni�os', 'Lorem', 123123, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$)LnVOX\\QT!:q/d1n2550453937ec740123733a9192792ab5dbd64036', '2014-05-23 11:23:11.653611', 'es');
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (60, 8000040, '2014-05-23 11:34:59', 'active', 'sell', 'test 5 cuenta', '123123412', 15, 0, 2060, 59, NULL, false, false, false, 'Moto bkn', 'bkn', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$9t}[fV^Ep;8yENk)3f814c18c43462bf99d42180e1c6e38ecd09be66', '2014-05-23 11:34:59.239232', 'es');
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (52, 8000032, '2014-05-23 11:24:43', 'active', 'sell', 'Usuario 04', '55554444', 15, 0, 7060, 57, NULL, false, false, false, 'Fiesta de despedida', 'Adios.', 23, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$.Ko%?d#Bl$CCS5>t81922d5072b4cf32d387a4bee37f3e048dda1e79', '2014-05-23 11:24:43.416475', 'es');
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (72, 8000052, '2014-05-23 11:44:24', 'active', 'sell', 'test 6 cuenta', '123123', 15, 0, 1100, 60, NULL, false, false, true, 'Dedp test6', 'edede', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$<*TZQgi/=9'',PLN"0dd36b819cae1633ab85795dcd309555f0c93563', '2014-05-23 11:44:24.999002', 'es');
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (53, 8000033, '2014-05-23 11:24:45', 'active', 'sell', 'Usuario 04', '55554444', 15, 0, 7060, 57, NULL, false, false, false, 'We speak english', 'this is the fifth ad', 123, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$sc!usy0\\yOji+DaF2ff2057d6a56067e817588747d1a5071c01f8cb1', '2014-05-23 11:24:45.909116', 'es');
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (61, 8000041, '2014-05-23 11:35:02', 'active', 'sell', 'test 5 cuenta', '123123412', 15, 0, 2040, 59, NULL, false, false, false, 'Camion', 'asdas', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$9t}[fV^Ep;8yENk)3f814c18c43462bf99d42180e1c6e38ecd09be66', '2014-05-23 11:35:02.638209', 'es');
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (54, 8000034, '2014-05-23 11:27:44', 'active', 'let', 'Usuario 05', '55554444', 15, 0, 1020, 58, NULL, false, false, false, 'Dpto en arriendo usuario05', 'lorem ipsum', 98776, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$Xk?!~N]mr0(dNFUa81705d1211ceaef6596a90f83c6862464f4b9857', '2014-05-23 11:27:44.679114', 'es');
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (55, 8000035, '2014-05-23 11:27:50', 'active', 'sell', 'Usuario 05', '55554444', 15, 0, 1040, 58, NULL, false, false, false, 'Casa usuario05', 'vendo casa', 123, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$Ka9fiZ~X25 cSD1n73cfc577e78a8a6fa853d223d1a25c6e1e405471', '2014-05-23 11:27:50.537479', 'es');
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (62, 8000042, '2014-05-23 11:35:09', 'active', 'sell', 'test 5 cuenta', '123123412', 15, 0, 2100, 59, NULL, false, false, false, 'Parachoque', 'vendo parachoque', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$9t}[fV^Ep;8yENk)3f814c18c43462bf99d42180e1c6e38ecd09be66', '2014-05-23 11:35:09.75119', 'es');
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (56, 8000036, '2014-05-23 11:29:31', 'active', 'sell', 'Usuario 05', '55554444', 15, 0, 1060, 58, NULL, false, false, false, 'En ciudad empresarial usuario05', 'no se donde queda eso, pero debe ser cool', 123, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$wt~Y7}~Ot=;|Ne!off858e314d6a3c0b52d9685a6170ee170c90e55a', '2014-05-23 11:29:31.913321', 'es');
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (57, 8000037, '2014-05-23 11:29:36', 'active', 'let', 'Usuario 05', '55554444', 15, 0, 1080, 58, NULL, false, false, false, 'Bodega de almacenaje usuario05', 'lorem ipsum dolor sit amet', 12323, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$FkF'';&3;rYzl,:6E045c8f82c369bc075146f167bc314be4967c6828', '2014-05-23 11:29:36.949231', 'es');
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (63, 8000043, '2014-05-23 11:35:12', 'active', 'sell', 'test 5 cuenta', '123123412', 15, 0, 2080, 59, NULL, false, false, false, 'Yate digo', 'yate digo', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$9t}[fV^Ep;8yENk)3f814c18c43462bf99d42180e1c6e38ecd09be66', '2014-05-23 11:35:12.026057', 'es');
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (58, 8000038, '2014-05-23 11:29:43', 'active', 'sell', 'Usuario 05', '55554444', 15, 0, 1100, 58, NULL, false, false, false, 'Predio agricola usuario05', 'lorem', 123, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$}ATd6BBunJowst5I25d180713ed4f041566e597093e31fe9643edb5b', '2014-05-23 11:29:43.811495', 'es');
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (59, 8000039, '2014-05-23 11:29:47', 'active', 'sell', 'test 5 cuenta', '123123412', 15, 0, 2020, 59, NULL, false, false, false, 'Acura Legend 2012', 'asds', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$9t}[fV^Ep;8yENk)3f814c18c43462bf99d42180e1c6e38ecd09be66', '2014-05-23 11:29:47.267284', 'es');
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (68, 8000048, '2014-05-23 11:43:02', 'active', 'sell', 'Usuario 07', '55554444', 15, 0, 1060, 61, NULL, false, false, true, 'Oficina', 'lorem ipsum dolor sit amet', 123123, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$7+\\r1H''@hqq|y)x!6b2d9fd7554c077991f15dd4334df11fdad0e009', '2014-05-23 11:43:02.534327', 'es');
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (69, 8000049, '2014-05-23 11:43:51', 'active', 'sell', 'test 6 cuenta', '123123', 15, 0, 1040, 60, NULL, false, false, false, 'Depto test6', 'deoto', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$<*TZQgi/=9'',PLN"0dd36b819cae1633ab85795dcd309555f0c93563', '2014-05-23 11:43:51.921979', 'es');
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (73, 8000053, '2014-05-23 11:47:11', 'active', 'sell', 'test 6 cuenta', '123123', 15, 0, 1040, 60, NULL, false, false, true, 'Casaaa test6', 'asdas', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$<*TZQgi/=9'',PLN"0dd36b819cae1633ab85795dcd309555f0c93563', '2014-05-23 11:47:11.486736', 'es');
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (70, 8000050, '2014-05-23 11:43:57', 'active', 'sell', 'test 6 cuenta', '123123', 15, 0, 1020, 60, NULL, false, false, false, 'Depto 3 test6', 'fsdf', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$<*TZQgi/=9'',PLN"0dd36b819cae1633ab85795dcd309555f0c93563', '2014-05-23 11:43:57.246393', 'es');
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (64, 8000044, '2014-05-23 11:41:27', 'active', 'sell', 'Usuario 07', '55554444', 15, 0, 1120, 61, NULL, false, false, true, 'Bodega', 'lorem ipsum dolor sit amet', 123123, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$^zluVu;wz8ih)z~d908086fb2d7e733c7b7c6c018adc74ce0c6d5b12', '2014-05-23 11:41:27.073637', 'es');
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (65, 8000045, '2014-05-23 11:41:31', 'active', 'sell', 'Usuario 07', '55554444', 15, 0, 1100, 61, NULL, false, false, true, 'Terreno', 'Lorem ipsum dolor sit amet', 123132, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$] t>Bx~j!ZX''j=#F00ae3f390cc1a5771daee964f76af4e56cc955f1', '2014-05-23 11:41:31.41624', 'es');
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (66, 8000046, '2014-05-23 11:42:52', 'active', 'sell', 'Usuario 07', '55554444', 15, 0, 1080, 61, NULL, false, false, true, 'Industria del vino', 'lorem', 123123, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$1QA<ejro#D>b|*,Hd038eddeba25942bfc2b5021e82b06b482f86c64', '2014-05-23 11:42:52.817063', 'es');
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (71, 8000051, '2014-05-23 11:44:02', 'active', 'sell', 'Usuario 07', '55554444', 15, 0, 1040, 61, NULL, false, false, true, 'Casa bkn', 'lorem ipsum dolor sit amet', 123123, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$+B{X-E3B!#!q^NJj30b67c14ddee02b439450cc4b3172e0b5e39f14e', '2014-05-23 11:44:02.876137', 'es');
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (74, 8000054, '2014-05-23 11:49:51', 'active', 'sell', 'Usuario 07', '55554444', 15, 0, 1020, 61, NULL, false, false, true, 'The sixth ad', 'I am a pro', 123123, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$28,ac[t6i^1i+zaC6752481f837bd440e2d44feb818c8578947d50e2', '2014-05-23 11:49:51.747734', 'es');


--
-- Data for Name: payment_groups; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (0, '012345678', 'unverified', '2015-02-20 15:02:25.758146', NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (1, '123456789', 'unverified', '2015-02-20 15:02:25.761441', NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (2, '11223', 'unpaid', '2015-02-20 15:02:25.76274', NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (3, '200000001', 'unverified', '2015-02-20 15:02:25.764215', NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (50, '59876', 'cleared', '2015-02-20 15:02:25.765519', NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (60, '112200000', 'verified', '2014-05-23 10:32:45.317842', NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (61, '112200001', 'verified', '2014-05-23 10:34:31.188281', NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (62, '112200002', 'verified', '2014-05-23 10:36:37.437651', NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (63, '112200003', 'verified', '2014-05-23 10:37:58.488344', NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (64, '112200004', 'verified', '2014-05-23 10:53:16.089876', NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (65, '112200005', 'verified', '2014-05-23 10:53:58.34091', NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (66, '112200006', 'verified', '2014-05-23 10:55:28.906747', NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (67, '112200007', 'verified', '2014-05-23 10:56:08.078074', NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (68, '112200008', 'verified', '2014-05-23 10:57:50.097853', NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (69, '112200009', 'verified', '2014-05-23 10:58:43.60664', NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (70, '235392023', 'verified', '2014-05-23 10:59:25.358339', NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (71, '370784046', 'verified', '2014-05-23 11:00:09.575982', NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (72, '506176069', 'verified', '2014-05-23 11:02:58.842354', NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (73, '641568092', 'verified', '2014-05-23 11:03:34.025011', NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (74, '776960115', 'verified', '2014-05-23 11:03:49.993978', NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (75, '912352138', 'verified', '2014-05-23 11:04:17.162061', NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (76, '147744161', 'verified', '2014-05-23 11:04:17.402456', NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (77, '283136184', 'verified', '2014-05-23 11:05:28.878947', NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (78, '418528207', 'verified', '2014-05-23 11:05:51.614044', NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (79, '553920230', 'verified', '2014-05-23 11:06:18.665366', NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (80, '689312253', 'verified', '2014-05-23 11:07:45.111576', NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (81, '824704276', 'verified', '2014-05-23 11:07:45.787557', NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (82, '960096299', 'verified', '2014-05-23 11:09:23.167989', NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (83, '195488322', 'verified', '2014-05-23 11:09:48.828694', NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (84, '330880345', 'verified', '2014-05-23 11:19:19.455645', NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (85, '466272368', 'verified', '2014-05-23 11:20:15.225995', NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (86, '601664391', 'verified', '2014-05-23 11:20:22.043844', NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (87, '737056414', 'verified', '2014-05-23 11:20:55.220588', NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (88, '872448437', 'verified', '2014-05-23 11:21:16.749539', NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (89, '107840460', 'verified', '2014-05-23 11:22:07.228201', NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (90, '243232483', 'verified', '2014-05-23 11:22:20.891643', NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (91, '378624506', 'verified', '2014-05-23 11:22:55.121254', NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (92, '514016529', 'verified', '2014-05-23 11:23:41.626963', NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (93, '649408552', 'verified', '2014-05-23 11:24:33.819852', NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (94, '784800575', 'verified', '2014-05-23 11:26:08.686185', NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (95, '920192598', 'verified', '2014-05-23 11:27:36.401735', NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (96, '155584621', 'verified', '2014-05-23 11:28:24.310768', NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (97, '290976644', 'verified', '2014-05-23 11:28:56.557671', NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (98, '426368667', 'verified', '2014-05-23 11:29:24.559797', NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (99, '561760690', 'verified', '2014-05-23 11:29:33.116539', NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (100, '697152713', 'verified', '2014-05-23 11:29:54.625049', NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (101, '832544736', 'verified', '2014-05-23 11:30:22.888075', NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (102, '967936759', 'verified', '2014-05-23 11:30:54.818169', NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (103, '203328782', 'verified', '2014-05-23 11:31:30.026841', NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (104, '338720805', 'verified', '2014-05-23 11:40:13.438414', NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (105, '474112828', 'verified', '2014-05-23 11:41:16.086591', NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (106, '609504851', 'verified', '2014-05-23 11:42:16.478715', NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (107, '744896874', 'verified', '2014-05-23 11:42:42.151048', NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (108, '880288897', 'verified', '2014-05-23 11:42:47.785329', NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (109, '115680920', 'verified', '2014-05-23 11:43:05.069415', NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (110, '251072943', 'verified', '2014-05-23 11:43:36.119927', NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (111, '386464966', 'verified', '2014-05-23 11:43:41.185603', NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (112, '521856989', 'verified', '2014-05-23 11:44:18.652111', NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (113, '657249012', 'verified', '2014-05-23 11:44:41.644178', NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (114, '792641035', 'verified', '2014-05-23 11:48:26.364845', NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (115, '928033058', 'verified', '2014-05-23 17:40:25.077355', NULL);


--
-- Data for Name: ad_actions; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (1, 1, 'new', 154, 'accepted', 'normal', NULL, NULL, 0);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (2, 1, 'new', 4, 'unverified', 'normal', NULL, NULL, 1);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (3, 1, 'new', 6, 'unpaid', 'normal', NULL, NULL, 2);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (4, 1, 'new', 8, 'unverified', 'normal', NULL, NULL, 3);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (8, 1, 'new', 9, 'accepted', 'normal', NULL, NULL, NULL);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (10, 1, 'new', 11, 'accepted', 'normal', NULL, NULL, NULL);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (11, 1, 'new', 12, 'accepted', 'normal', NULL, NULL, NULL);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (6, 1, 'new', 53, 'accepted', 'normal', NULL, NULL, 50);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (9, 1, 'new', 43, 'refused', 'normal', NULL, NULL, NULL);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (12, 1, 'new', 164, 'accepted', 'normal', NULL, NULL, 0);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (72, 1, 'new', 466, 'accepted', 'normal', 5, '2014-05-23 11:49:20.256385', 112);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (33, 1, 'new', 278, 'accepted', 'normal', 5, '2014-05-23 11:09:27.356829', 73);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (71, 2, 'account_to_pro', 485, 'accepted', 'normal', NULL, NULL, NULL);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (48, 1, 'new', 350, 'accepted', 'normal', 5, '2014-05-23 11:27:11.759579', 88);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (56, 1, 'new', 393, 'accepted', 'normal', 5, '2014-05-23 11:34:28.915929', 96);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (32, 1, 'new', 281, 'accepted', 'normal', 5, '2014-05-23 11:10:11.146703', 72);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (34, 1, 'new', 282, 'accepted', 'normal', 5, '2014-05-23 11:10:11.146703', 74);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (73, 1, 'new', 471, 'accepted', 'normal', 5, '2014-05-23 11:52:08.787071', 113);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (60, 1, 'new', 416, 'accepted', 'normal', 5, '2014-05-23 11:39:55.736242', 100);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (61, 1, 'new', 417, 'accepted', 'normal', 5, '2014-05-23 11:39:55.736242', 101);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (42, 1, 'new', 320, 'accepted', 'normal', 5, '2014-05-23 11:14:56.769766', 82);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (43, 1, 'new', 321, 'accepted', 'normal', 5, '2014-05-23 11:14:56.769766', 83);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (52, 1, 'new', 370, 'accepted', 'normal', 5, '2014-05-23 11:29:39.088999', 92);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (35, 1, 'new', 288, 'accepted', 'normal', 5, '2014-05-23 11:10:27.769772', 75);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (36, 1, 'new', 289, 'accepted', 'normal', 5, '2014-05-23 11:10:27.769772', 76);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (53, 1, 'new', 371, 'accepted', 'normal', 5, '2014-05-23 11:29:39.088999', 93);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (37, 1, 'new', 291, 'accepted', 'normal', 5, '2014-05-23 11:10:38.23555', 77);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (20, 1, 'new', 214, 'accepted', 'normal', 5, '2014-05-23 10:45:15.332884', 60);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (21, 1, 'new', 215, 'accepted', 'normal', 5, '2014-05-23 10:45:15.332884', 61);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (24, 1, 'new', 246, 'accepted', 'normal', 5, '2014-05-23 11:05:14.839237', 64);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (25, 1, 'new', 247, 'accepted', 'normal', 5, '2014-05-23 11:05:14.839237', 65);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (62, 1, 'new', 420, 'accepted', 'normal', 5, '2014-05-23 11:40:04.605242', 102);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (63, 1, 'new', 421, 'accepted', 'normal', 5, '2014-05-23 11:40:04.605242', 103);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (22, 1, 'new', 218, 'accepted', 'normal', 5, '2014-05-23 10:45:23.43254', 62);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (23, 1, 'new', 219, 'accepted', 'normal', 5, '2014-05-23 10:45:23.43254', 63);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (26, 1, 'new', 250, 'accepted', 'normal', 5, '2014-05-23 11:05:25.731504', 66);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (27, 1, 'new', 251, 'accepted', 'normal', 5, '2014-05-23 11:05:25.731504', 67);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (74, 1, 'new', 475, 'accepted', 'whitelist', NULL, NULL, 114);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (68, 2, 'account_to_pro', 477, 'accepted', 'normal', NULL, NULL, NULL);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (38, 1, 'new', 296, 'accepted', 'normal', 5, '2014-05-23 11:11:06.786665', 78);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (64, 2, 'account_to_pro', 479, 'accepted', 'normal', NULL, NULL, NULL);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (28, 1, 'new', 254, 'accepted', 'normal', 5, '2014-05-23 11:05:36.113746', 68);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (29, 1, 'new', 255, 'accepted', 'normal', 5, '2014-05-23 11:05:36.113746', 69);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (49, 1, 'new', 354, 'accepted', 'normal', 5, '2014-05-23 11:27:11.759579', 89);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (65, 2, 'account_to_pro', 481, 'accepted', 'normal', NULL, NULL, NULL);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (54, 1, 'new', 380, 'accepted', 'normal', 5, '2014-05-23 11:32:40.480284', 94);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (30, 1, 'new', 258, 'accepted', 'normal', 5, '2014-05-23 11:05:43.631244', 70);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (31, 1, 'new', 259, 'accepted', 'normal', 5, '2014-05-23 11:05:43.631244', 71);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (55, 1, 'new', 381, 'accepted', 'normal', 5, '2014-05-23 11:32:40.480284', 95);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (44, 1, 'new', 333, 'accepted', 'normal', 5, '2014-05-23 11:25:41.456755', 84);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (66, 2, 'account_to_pro', 483, 'accepted', 'normal', NULL, NULL, NULL);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (64, 1, 'new', 430, 'accepted', 'normal', 5, '2014-05-23 11:46:23.71262', 104);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (65, 1, 'new', 431, 'accepted', 'normal', 5, '2014-05-23 11:46:23.71262', 105);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (45, 1, 'new', 337, 'accepted', 'normal', 5, '2014-05-23 11:25:41.456755', 85);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (74, 2, 'account_to_pro', 487, 'accepted', 'normal', NULL, NULL, NULL);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (57, 1, 'new', 397, 'accepted', 'normal', 5, '2014-05-23 11:34:28.915929', 97);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (46, 1, 'new', 340, 'accepted', 'normal', 5, '2014-05-23 11:26:02.946637', 86);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (47, 1, 'new', 341, 'accepted', 'normal', 5, '2014-05-23 11:26:02.946637', 87);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (50, 1, 'new', 356, 'accepted', 'normal', 5, '2014-05-23 11:27:24.93418', 90);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (39, 1, 'new', 308, 'accepted', 'normal', 5, '2014-05-23 11:12:48.656672', 79);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (40, 1, 'new', 309, 'accepted', 'normal', 5, '2014-05-23 11:12:48.656672', 80);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (41, 1, 'new', 311, 'accepted', 'normal', 5, '2014-05-23 11:12:57.807416', 81);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (75, 1, 'new', 492, 'accepted', 'normal', 5, '2014-05-23 17:46:00.699117', 115);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (66, 1, 'new', 443, 'accepted', 'normal', 5, '2014-05-23 11:47:49.623455', 106);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (67, 1, 'new', 444, 'accepted', 'normal', 5, '2014-05-23 11:47:49.623455', 107);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (58, 1, 'new', 400, 'accepted', 'normal', 5, '2014-05-23 11:34:38.45016', 98);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (59, 1, 'new', 401, 'accepted', 'normal', 5, '2014-05-23 11:34:38.45016', 99);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (68, 1, 'new', 446, 'accepted', 'normal', 5, '2014-05-23 11:47:58.381755', 108);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (51, 1, 'new', 361, 'accepted', 'normal', 5, '2014-05-23 11:28:06.984382', 91);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (69, 1, 'new', 458, 'accepted', 'normal', 5, '2014-05-23 11:48:49.153109', 109);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (70, 1, 'new', 459, 'accepted', 'normal', 5, '2014-05-23 11:48:49.153109', 110);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (71, 1, 'new', 461, 'accepted', 'normal', 5, '2014-05-23 11:48:57.709605', 111);


--
-- Data for Name: action_params; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO action_params (ad_id, action_id, name, value) VALUES (20, 1, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (20, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (21, 1, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (21, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (22, 1, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (22, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (23, 1, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (23, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (24, 1, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (24, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (25, 1, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (25, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (26, 1, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (26, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (27, 1, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (27, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (28, 1, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (28, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (29, 1, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (29, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (30, 1, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (30, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (31, 1, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (31, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (32, 1, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (32, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (33, 1, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (33, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (34, 1, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (34, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (35, 1, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (35, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (36, 1, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (36, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (37, 1, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (37, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (38, 1, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (38, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (39, 1, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (39, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (40, 1, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (40, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (41, 1, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (41, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (42, 1, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (42, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (43, 1, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (43, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (44, 1, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (44, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (45, 1, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (45, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (46, 1, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (46, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (47, 1, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (47, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (48, 1, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (48, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (49, 1, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (49, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (50, 1, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (50, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (51, 1, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (51, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (52, 1, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (52, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (53, 1, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (53, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (54, 1, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (54, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (55, 1, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (55, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (56, 1, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (56, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (57, 1, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (57, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (58, 1, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (58, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (59, 1, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (59, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (60, 1, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (60, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (61, 1, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (61, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (62, 1, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (62, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (63, 1, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (63, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (64, 1, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (64, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (65, 1, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (65, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (66, 1, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (66, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (67, 1, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (67, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (68, 1, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (68, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (69, 1, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (69, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (70, 1, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (70, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (71, 1, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (71, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (72, 1, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (72, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (73, 1, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (73, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (74, 1, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (74, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (75, 1, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (75, 1, 'redir', 'dW5rbm93bg==');


--
-- Data for Name: stores; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Data for Name: tokens; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (99999917, 'd12ac643ce5c39ddb765bc452d40790932d7c0f67823aaa', 5, '2006-04-06 15:19:46', '2006-04-06 16:19:46', '192.168.4.75', 'clear', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (99999918, 'a5e4e5b16bc9c806b1c22cfd638440034791acc68f23600', 5, '2006-04-06 16:19:31', '2006-04-06 16:19:46', '192.168.4.75', 'authenticate', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (99999919, 'a80cbcd0b019780754593a1ea8b28001c88086439101100', 5, '2006-04-06 16:19:46', '2006-04-06 16:19:46', '192.168.4.75', 'clear', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (99999992, 'b7e4e5b16bc9c806b1c22cfd638440034791acc68f23600', 5, '2011-11-06 05:19:31', '2011-11-06 05:19:46', '192.168.4.75', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (99999993, 'b9e4e5b16bc9c806b1c22cfd638440034791acc68f23600', 5, '2011-11-06 05:19:31', '2011-11-06 05:19:46', '192.168.4.75', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (99999998, 'b5e4e5b16bc9c806b1c22cfd638440034791acc68f23600', 5, '2006-04-06 15:19:31', '2006-04-06 15:19:46', '192.168.4.75', 'authenticate', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (99999999, '580cbcd0b019780754593a1ea8b28001c88086439101100', 5, '2006-04-06 15:19:46', '2006-04-06 16:19:46', '192.168.4.75', 'clear', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (99999994, 'b7e4e9b16bc9c806b1c22cfd638440034791acc68f23600', 5, '2011-11-06 05:19:31', '2011-11-06 05:19:46', '192.168.4.75', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (99999995, 'b9e6e6b16bc9c806b1c22cfd638440034791acc68f23600', 5, '2011-11-06 06:19:31', '2011-11-06 06:19:46', '192.168.4.75', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (1, 'X537f5dc9e9aa61000000005fa40eab00000000', 5, '2014-05-23 10:40:08.835055', '2014-05-23 10:40:14.158611', '10.0.1.188', 'authenticate', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (2, 'X537f5dce352c86950000000084fd5c600000000', 5, '2014-05-23 10:40:14.158611', '2014-05-23 10:40:15.329111', '10.0.1.188', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (3, 'X537f5dcf5d112be90000000047e4a05900000000', 5, '2014-05-23 10:40:15.329111', '2014-05-23 10:40:18.73076', '10.0.1.188', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (4, 'X537f5dd3556439cb000000007d7d266100000000', 5, '2014-05-23 10:40:18.73076', '2014-05-23 10:40:21.1136', '10.0.1.188', 'review', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (5, 'X537f5dd566d590c1000000007d97d19000000000', 5, '2014-05-23 10:40:21.1136', '2014-05-23 10:40:23.428877', '10.0.1.188', 'review', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (6, 'X537f5dd73354f0fd000000004784db0600000000', 5, '2014-05-23 10:40:23.428877', '2014-05-23 10:40:28.208617', '10.0.1.188', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (7, 'X537f5ddc53cf8ec60000000012e5c15a00000000', 5, '2014-05-23 10:40:28.208617', '2014-05-23 10:40:36.133226', '10.0.1.188', 'review', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (8, 'X537f5de4447f0ac00000000ffa367700000000', 5, '2014-05-23 10:40:36.133226', '2014-05-23 10:40:41.837083', '10.0.1.188', 'review', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (9, 'X537f5dea39a6edba000000004a8e00100000000', 5, '2014-05-23 10:40:41.837083', '2014-05-23 10:40:41.861902', '10.0.1.188', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (10, 'X537f5dea1ec0bcf20000000031d996dd00000000', 5, '2014-05-23 10:40:41.861902', '2014-05-23 10:40:58.660202', '10.0.1.188', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (11, 'X537f5dfb536048c7000000006ff7056200000000', 5, '2014-05-23 10:40:58.660202', '2014-05-23 10:40:58.680957', '10.0.1.188', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (12, 'X537f5dfb22751a70000000003be6485400000000', 5, '2014-05-23 10:40:58.680957', '2014-05-23 11:00:14.83266', '10.0.1.188', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (13, 'X537f627f56624351000000007d8a5fe700000000', 5, '2014-05-23 11:00:14.83266', '2014-05-23 11:00:20.252276', '10.0.1.188', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (14, 'X537f62843dc5598800000000480fdc9a00000000', 5, '2014-05-23 11:00:20.252276', '2014-05-23 11:00:22.795706', '10.0.1.188', 'review', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (15, 'X537f62871a37722d000000004bd727c300000000', 5, '2014-05-23 11:00:22.795706', '2014-05-23 11:00:25.728966', '10.0.1.188', 'review', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (16, 'X537f628a65506a92000000006e0b9bf600000000', 5, '2014-05-23 11:00:25.728966', '2014-05-23 11:00:31.268248', '10.0.1.188', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (17, 'X537f628f11dde2f9000000007a83e99100000000', 5, '2014-05-23 11:00:31.268248', '2014-05-23 11:00:34.138597', '10.0.1.188', 'review', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (18, 'X537f629224d4c8ef000000003f31780200000000', 5, '2014-05-23 11:00:34.138597', '2014-05-23 11:00:36.110202', '10.0.1.188', 'review', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (19, 'X537f629430336610000000058005b4b00000000', 5, '2014-05-23 11:00:36.110202', '2014-05-23 11:00:39.328304', '10.0.1.188', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (20, 'X537f629766bb505c000000007f3e41a100000000', 5, '2014-05-23 11:00:39.328304', '2014-05-23 11:00:42.487131', '10.0.1.188', 'review', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (21, 'X537f629a3d905342000000007b44b48000000000', 5, '2014-05-23 11:00:42.487131', '2014-05-23 11:00:43.627948', '10.0.1.188', 'review', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (22, 'X537f629c498ca8c900000000745501ff00000000', 5, '2014-05-23 11:00:43.627948', '2014-05-23 11:00:46.50619', '10.0.1.188', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (23, 'X537f629fe83a723000000007b01d69c00000000', 5, '2014-05-23 11:00:46.50619', '2014-05-23 11:00:48.868667', '10.0.1.188', 'review', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (24, 'X537f62a13a856bfd000000005268355700000000', 5, '2014-05-23 11:00:48.868667', '2014-05-23 11:00:50.043912', '10.0.1.188', 'review', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (25, 'X537f62a22351405000000001aaa6e5600000000', 5, '2014-05-23 11:00:50.043912', '2014-05-23 11:00:50.067646', '10.0.1.188', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (26, 'X537f62a22feef63f0000000039deb2f800000000', 5, '2014-05-23 11:00:50.067646', '2014-05-23 11:00:52.080729', '10.0.1.188', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (27, 'X537f62a442f0b8270000000021285b0000000000', 5, '2014-05-23 11:00:52.080729', '2014-05-23 11:00:52.102255', '10.0.1.188', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (28, 'X537f62a41d15810800000000516ce10600000000', 5, '2014-05-23 11:00:52.102255', '2014-05-23 11:04:26.440256', '10.0.1.188', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (29, 'X537f637a72c2b8fb000000002413697500000000', 5, '2014-05-23 11:04:26.440256', '2014-05-23 11:04:27.351863', '10.0.1.188', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (30, 'X537f637b6421574000000007214070300000000', 5, '2014-05-23 11:04:27.351863', '2014-05-23 11:04:38.8008', '10.0.1.188', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (31, 'X537f638727ff874900000000328b261b00000000', 5, '2014-05-23 11:04:38.8008', '2014-05-23 11:04:43.306578', '10.0.1.188', 'unlock', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (32, 'X537f638b40b0ab9200000000202fddeb00000000', 5, '2014-05-23 11:04:43.306578', '2014-05-23 11:05:11.13515', '10.0.1.188', 'review', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (33, 'X537f63a74002cf000000005417f26400000000', 5, '2014-05-23 11:05:11.13515', '2014-05-23 11:05:23.304592', '10.0.1.188', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (34, 'X537f63b322d9c6f90000000047332ea500000000', 5, '2014-05-23 11:05:23.304592', '2014-05-23 11:05:26.596325', '10.0.1.188', 'review', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (35, 'X537f63b7488a2d38000000006d1f9ed500000000', 5, '2014-05-23 11:05:26.596325', '2014-05-23 11:05:27.765985', '10.0.1.188', 'review', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (36, 'X537f63b8230a3fea000000003d56998800000000', 5, '2014-05-23 11:05:27.765985', '2014-05-23 11:05:31.760265', '10.0.1.188', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (37, 'X537f63bc170c96f000000004be8a1b300000000', 5, '2014-05-23 11:05:31.760265', '2014-05-23 11:05:36.398737', '10.0.1.188', 'review', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (38, 'X537f63c0c8b25c100000000100a164000000000', 5, '2014-05-23 11:05:36.398737', '2014-05-23 11:05:38.231243', '10.0.1.188', 'review', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (39, 'X537f63c258eeae8a00000000f1188b300000000', 5, '2014-05-23 11:05:38.231243', '2014-05-23 11:05:41.835548', '10.0.1.188', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (40, 'X537f63c659f4c263000000003de71d4100000000', 5, '2014-05-23 11:05:41.835548', '2014-05-23 11:05:43.420144', '10.0.1.188', 'review', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (41, 'X537f63c75e1c0b650000000073a857d800000000', 5, '2014-05-23 11:05:43.420144', '2014-05-23 11:05:43.441048', '10.0.1.188', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (42, 'X537f63c71f19048a00000000665278ec00000000', 5, '2014-05-23 11:05:43.441048', '2014-05-23 11:06:05.397031', '10.0.1.188', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (43, 'X537f63dd7d5af05000000001b4b992600000000', 5, '2014-05-23 11:06:05.397031', '2014-05-23 11:06:06.782731', '10.0.1.188', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (44, 'X537f63df6e88b943000000004d05d5e600000000', 5, '2014-05-23 11:06:06.782731', '2014-05-23 11:06:12.222754', '10.0.1.188', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (45, 'X537f63e47782324e000000007458feeb00000000', 5, '2014-05-23 11:06:12.222754', '2014-05-23 11:06:13.489277', '10.0.1.188', 'review', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (46, 'X537f63e5559c2bef0000000078816ad000000000', 5, '2014-05-23 11:06:13.489277', '2014-05-23 11:06:13.51392', '10.0.1.188', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (47, 'X537f63e67854238f00000000609d11cd00000000', 5, '2014-05-23 11:06:13.51392', '2014-05-23 11:07:48.649388', '10.0.1.188', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (48, 'X537f6445e1b2cdb000000003823cb9300000000', 5, '2014-05-23 11:07:48.649388', '2014-05-23 11:07:52.149884', '10.0.1.188', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (49, 'X537f64481de22c31000000005752b0fe00000000', 5, '2014-05-23 11:07:52.149884', '2014-05-23 11:07:56.385165', '10.0.1.188', 'review', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (50, 'X537f644c63fdcdd2000000004ee7e01800000000', 5, '2014-05-23 11:07:56.385165', '2014-05-23 11:07:57.803159', '10.0.1.188', 'review', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (69, 'X537f67685117e93800000000682d9f5c00000000', 5, '2014-05-23 11:21:11.643523', '2014-05-23 11:22:11.753242', '10.0.1.188', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (51, 'X537f644e402ef3e500000000710952c400000000', 5, '2014-05-23 11:07:57.803159', '2014-05-23 11:08:02.320486', '10.0.1.188', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (70, 'X537f67a452b4a9a1000000002fa8d01100000000', 5, '2014-05-23 11:22:11.753242', '2014-05-23 11:22:17.901475', '10.0.1.188', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (52, 'X537f645211ad5706000000006b65fa6800000000', 5, '2014-05-23 11:08:02.320486', '2014-05-23 11:08:03.439085', '10.0.1.188', 'review', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (53, 'X537f6453419c00e70000000072f4a8f400000000', 5, '2014-05-23 11:08:03.439085', '2014-05-23 11:08:03.467492', '10.0.1.188', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (71, 'X537f67aa67efe98a000000006caf2a6000000000', 5, '2014-05-23 11:22:17.901475', '2014-05-23 11:22:23.358755', '10.0.1.188', 'review', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (54, 'X537f645356389e80000000024ddbad700000000', 5, '2014-05-23 11:08:03.467492', '2014-05-23 11:09:55.754299', '10.0.1.188', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (55, 'X537f64c44e95fed1000000007ec6f3e400000000', 5, '2014-05-23 11:09:55.754299', '2014-05-23 11:09:56.765319', '10.0.1.188', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (72, 'X537f67afdfd353b000000001623aef400000000', 5, '2014-05-23 11:22:23.358755', '2014-05-23 11:22:24.930653', '10.0.1.188', 'review', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (56, 'X537f64c566ff0682000000007568228900000000', 5, '2014-05-23 11:09:56.765319', '2014-05-23 11:10:15.437692', '10.0.1.188', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (57, 'X537f64d734d36180000000012cde20100000000', 5, '2014-05-23 11:10:15.437692', '2014-05-23 11:15:16.707403', '10.0.1.188', 'review', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (73, 'X537f67b11dc9400c0000000036599a8200000000', 5, '2014-05-23 11:22:24.930653', '2014-05-23 11:22:28.034098', '10.0.1.188', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (58, 'X537f660525c92c2d0000000031cb11f500000000', 5, '2014-05-23 11:15:16.707403', '2014-05-23 11:15:19.368874', '10.0.1.188', 'review', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (59, 'X537f660744a854040000000085889dc00000000', 5, '2014-05-23 11:15:19.368874', '2014-05-23 11:15:19.393379', '10.0.1.188', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (74, 'X537f67b42d1fedf000000001d833f7500000000', 5, '2014-05-23 11:22:28.034098', '2014-05-23 11:23:06.979226', '10.0.1.188', 'review', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (60, 'X537f660739dd56fc000000004b7da90800000000', 5, '2014-05-23 11:15:19.393379', '2014-05-23 11:20:40.530871', '10.0.1.188', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (61, 'X537f67498298fbf00000000678e5f6d00000000', 5, '2014-05-23 11:20:40.530871', '2014-05-23 11:20:41.453833', '10.0.1.188', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (75, 'X537f67db5be4af530000000072d8750e00000000', 5, '2014-05-23 11:23:06.979226', '2014-05-23 11:23:11.6504', '10.0.1.188', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (62, 'X537f67493ee458c40000000023a47bd000000000', 5, '2014-05-23 11:20:41.453833', '2014-05-23 11:20:48.008088', '10.0.1.188', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (63, 'X537f67505cf066d2000000006ea97f9e00000000', 5, '2014-05-23 11:20:48.008088', '2014-05-23 11:20:59.761536', '10.0.1.188', 'review', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (76, 'X537f67e0217e87240000000040aacc800000000', 5, '2014-05-23 11:23:11.6504', '2014-05-23 11:23:15.964047', '10.0.1.188', 'review', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (64, 'X537f675c53a79e41000000005a5a658200000000', 5, '2014-05-23 11:20:59.761536', '2014-05-23 11:21:02.938955', '10.0.1.188', 'review', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (65, 'X537f675f4ab24d28000000004c66b2ef00000000', 5, '2014-05-23 11:21:02.938955', '2014-05-23 11:21:05.980207', '10.0.1.188', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (77, 'X537f67e45695848b00000000413b404100000000', 5, '2014-05-23 11:23:15.964047', '2014-05-23 11:23:15.979911', '10.0.1.188', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (66, 'X537f67627afdd70000000027bbcd8b00000000', 5, '2014-05-23 11:21:05.980207', '2014-05-23 11:21:09.938428', '10.0.1.188', 'review', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (67, 'X537f67661359cda8000000004f2b4fc200000000', 5, '2014-05-23 11:21:09.938428', '2014-05-23 11:21:11.619658', '10.0.1.188', 'review', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (78, 'X537f67e470dbe936000000007bc323ee00000000', 5, '2014-05-23 11:23:15.979911', '2014-05-23 11:24:39.086194', '10.0.1.188', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (68, 'X537f67686fae805000000000a50d51d00000000', 5, '2014-05-23 11:21:11.619658', '2014-05-23 11:21:11.643523', '10.0.1.188', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (79, 'X537f68377b399cfa0000000065b526ad00000000', 5, '2014-05-23 11:24:39.086194', '2014-05-23 11:24:43.412579', '10.0.1.188', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (80, 'X537f683b31df02ae000000005ad7689500000000', 5, '2014-05-23 11:24:43.412579', '2014-05-23 11:24:45.904355', '10.0.1.188', 'review', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (81, 'X537f683e236008d000000004cd90a1100000000', 5, '2014-05-23 11:24:45.904355', '2014-05-23 11:24:47.485263', '10.0.1.188', 'review', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (82, 'X537f683f3e815a29000000003a08926d00000000', 5, '2014-05-23 11:24:47.485263', '2014-05-23 11:24:47.499546', '10.0.1.188', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (83, 'X537f683f4265c802000000007d65b2ed00000000', 5, '2014-05-23 11:24:47.499546', '2014-05-23 11:27:40.469071', '10.0.1.188', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (84, 'X537f68ec1d7c95f30000000017223e4500000000', 5, '2014-05-23 11:27:40.469071', '2014-05-23 11:27:44.673135', '10.0.1.188', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (85, 'X537f68f197c5c09000000003cb9bf2100000000', 5, '2014-05-23 11:27:44.673135', '2014-05-23 11:27:50.522271', '10.0.1.188', 'review', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (86, 'X537f68f742f981a200000000676dfb6b00000000', 5, '2014-05-23 11:27:50.522271', '2014-05-23 11:27:51.86091', '10.0.1.188', 'review', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (87, 'X537f68f84f9e49bb0000000047b51be200000000', 5, '2014-05-23 11:27:51.86091', '2014-05-23 11:27:51.885538', '10.0.1.188', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (88, 'X537f68f829900b2000000000c0cc6ae00000000', 5, '2014-05-23 11:27:51.885538', '2014-05-23 11:29:28.911241', '10.0.1.188', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (89, 'X537f69597e20adf000000006ea9874000000000', 5, '2014-05-23 11:29:28.911241', '2014-05-23 11:29:31.908303', '10.0.1.188', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (90, 'X537f695c3342c1c40000000034c34b2e00000000', 5, '2014-05-23 11:29:31.908303', '2014-05-23 11:29:36.943718', '10.0.1.188', 'review', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (91, 'X537f6961106abf5700000000711366df00000000', 5, '2014-05-23 11:29:36.943718', '2014-05-23 11:29:38.445794', '10.0.1.188', 'review', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (92, 'X537f69622fab8e90000000003659e86000000000', 5, '2014-05-23 11:29:38.445794', '2014-05-23 11:29:43.806083', '10.0.1.188', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (93, 'X537f696855e467bb000000002571127100000000', 5, '2014-05-23 11:29:43.806083', '2014-05-23 11:29:47.261548', '10.0.1.188', 'review', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (94, 'X537f696b17d1255400000000667c287700000000', 5, '2014-05-23 11:29:47.261548', '2014-05-23 11:29:48.871059', '10.0.1.188', 'review', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (95, 'X537f696d791b7b6000000002e8933a600000000', 5, '2014-05-23 11:29:48.871059', '2014-05-23 11:29:48.895455', '10.0.1.188', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (96, 'X537f696d46ee402e000000006deaa25d00000000', 5, '2014-05-23 11:29:48.895455', '2014-05-23 11:34:55.71725', '10.0.1.188', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (97, 'X537f6aa03c22b49d000000004b42982000000000', 5, '2014-05-23 11:34:55.71725', '2014-05-23 11:34:59.233579', '10.0.1.188', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (98, 'X537f6aa3749f971600000000625f117b00000000', 5, '2014-05-23 11:34:59.233579', '2014-05-23 11:35:02.615643', '10.0.1.188', 'review', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (99, 'X537f6aa77fde9415000000002f2a6f5700000000', 5, '2014-05-23 11:35:02.615643', '2014-05-23 11:35:04.601451', '10.0.1.188', 'review', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (100, 'X537f6aa9596dc459000000007eb7407d00000000', 5, '2014-05-23 11:35:04.601451', '2014-05-23 11:35:09.747708', '10.0.1.188', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (101, 'X537f6aae1ffb30bd0000000040761a9600000000', 5, '2014-05-23 11:35:09.747708', '2014-05-23 11:35:12.022415', '10.0.1.188', 'review', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (102, 'X537f6ab0619376c70000000058a632fc00000000', 5, '2014-05-23 11:35:12.022415', '2014-05-23 11:35:14.462988', '10.0.1.188', 'review', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (103, 'X537f6ab24fc0187f000000007d98de6e00000000', 5, '2014-05-23 11:35:14.462988', '2014-05-23 11:35:14.482258', '10.0.1.188', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (104, 'X537f6ab25833c3af00000000360dc69300000000', 5, '2014-05-23 11:35:14.482258', '2014-05-23 11:41:22.920668', '10.0.1.188', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (105, 'X537f6c235dc514a00000000419d48d00000000', 5, '2014-05-23 11:41:22.920668', '2014-05-23 11:41:23.709378', '10.0.1.188', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (106, 'X537f6c24669a8bcb000000007097567f00000000', 5, '2014-05-23 11:41:23.709378', '2014-05-23 11:41:27.069206', '10.0.1.188', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (107, 'X537f6c277d7705ad000000009deb71600000000', 5, '2014-05-23 11:41:27.069206', '2014-05-23 11:41:31.404064', '10.0.1.188', 'review', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (108, 'X537f6c2b2716f2a4000000007e660af000000000', 5, '2014-05-23 11:41:31.404064', '2014-05-23 11:41:33.260664', '10.0.1.188', 'review', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (109, 'X537f6c2d69a8a19300000000160d49ee00000000', 5, '2014-05-23 11:41:33.260664', '2014-05-23 11:41:33.280423', '10.0.1.188', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (110, 'X537f6c2d20270978000000007b8cccd000000000', 5, '2014-05-23 11:41:33.280423', '2014-05-23 11:42:49.618789', '10.0.1.188', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (113, 'X537f6c81717e60ec000000005e32e30600000000', 5, '2014-05-23 11:42:57.250502', '2014-05-23 11:42:58.377534', '10.0.1.188', 'review', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (116, 'X537f6c8923072b46000000007dabff7c00000000', 5, '2014-05-23 11:43:05.081767', '2014-05-23 11:43:05.103613', '10.0.1.188', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (117, 'X537f6c8941d48652000000007cfea6ec00000000', 5, '2014-05-23 11:43:05.103613', '2014-05-23 11:43:48.139128', '10.0.1.188', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (121, 'X537f6cbd488eade100000000429b11d400000000', 5, '2014-05-23 11:43:57.240848', '2014-05-23 11:43:57.706109', '10.0.1.188', 'review', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (124, 'X537f6cc4539001e100000000249edd3700000000', 5, '2014-05-23 11:44:03.620995', '2014-05-23 11:44:03.643312', '10.0.1.188', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (125, 'X537f6cc421128006000000007526d08300000000', 5, '2014-05-23 11:44:03.643312', '2014-05-23 11:44:20.25168', '10.0.1.188', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (126, 'X537f6cd439960a970000000017a86d8e00000000', 5, '2014-05-23 11:44:20.25168', '2014-05-23 11:44:24.982928', '10.0.1.188', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (127, 'X537f6cd93a65cbf600000000396fd49500000000', 5, '2014-05-23 11:44:24.982928', '2014-05-23 11:44:26.091362', '10.0.1.188', 'review', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (111, 'X537f6c7a22b8729c000000004793bae800000000', 5, '2014-05-23 11:42:49.618789', '2014-05-23 11:42:52.811454', '10.0.1.188', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (112, 'X537f6c7d541d862c000000007a929dba00000000', 5, '2014-05-23 11:42:52.811454', '2014-05-23 11:42:57.250502', '10.0.1.188', 'review', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (114, 'X537f6c823256b7d2000000006694904300000000', 5, '2014-05-23 11:42:58.377534', '2014-05-23 11:43:02.531185', '10.0.1.188', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (115, 'X537f6c876d3187580000000034c62a1200000000', 5, '2014-05-23 11:43:02.531185', '2014-05-23 11:43:05.081767', '10.0.1.188', 'review', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (118, 'X537f6cb418a34e72000000007dc3545400000000', 5, '2014-05-23 11:43:48.139128', '2014-05-23 11:43:49.149651', '10.0.1.188', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (119, 'X537f6cb51986e9d1000000007bebd58100000000', 5, '2014-05-23 11:43:49.149651', '2014-05-23 11:43:51.917844', '10.0.1.188', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (120, 'X537f6cb87d19c875000000006644a85600000000', 5, '2014-05-23 11:43:51.917844', '2014-05-23 11:43:57.240848', '10.0.1.188', 'review', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (122, 'X537f6cbe4165ac3e0000000062bafbba00000000', 5, '2014-05-23 11:43:57.706109', '2014-05-23 11:44:02.871734', '10.0.1.188', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (123, 'X537f6cc36e4e41c90000000062f0f3d200000000', 5, '2014-05-23 11:44:02.871734', '2014-05-23 11:44:03.620995', '10.0.1.188', 'review', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (128, 'X537f6cda177c37620000000053ffaa0100000000', 5, '2014-05-23 11:44:26.091362', '2014-05-23 11:44:26.115331', '10.0.1.188', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (129, 'X537f6cda5a5037e600000000225b974a00000000', 5, '2014-05-23 11:44:26.115331', '2014-05-23 11:47:08.77529', '10.0.1.188', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (130, 'X537f6d7d6caba6a5000000004b42e50b00000000', 5, '2014-05-23 11:47:08.77529', '2014-05-23 11:47:11.48209', '10.0.1.188', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (131, 'X537f6d7fcf6775a000000002d3afebd00000000', 5, '2014-05-23 11:47:11.48209', '2014-05-23 11:47:12.804847', '10.0.1.188', 'review', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (132, 'X537f6d814b9981e1000000003989d05e00000000', 5, '2014-05-23 11:47:12.804847', '2014-05-23 11:47:12.830306', '10.0.1.188', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (133, 'X537f6d815784b6e600000000cb36a3600000000', 5, '2014-05-23 11:47:12.830306', '2014-05-23 11:48:29.40263', '10.0.1.188', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (134, 'X537f6dcd760385e3000000006990b03400000000', 5, '2014-05-23 11:48:29.40263', '2014-05-23 11:48:29.429726', '10.0.1.188', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (135, 'X537f6dcd3647f470000000051999fb500000000', 5, '2014-05-23 11:48:29.429726', '2014-05-23 11:48:40.494456', '10.0.1.188', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (136, 'X537f6dd85f0ce1be00000000517643d00000000', 5, '2014-05-23 11:48:40.494456', '2014-05-23 11:48:42.349076', '10.0.1.188', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (137, 'X537f6dda6b9069a000000007b3ab38a00000000', 5, '2014-05-23 11:48:42.349076', '2014-05-23 11:48:42.363216', '10.0.1.188', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (138, 'X537f6dda41b4eaac00000000478f63bd00000000', 5, '2014-05-23 11:48:42.363216', '2014-05-23 11:49:35.335694', '10.0.1.188', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (139, 'X537f6e0f5c2565740000000020195c7c00000000', 5, '2014-05-23 11:49:35.335694', '2014-05-23 11:49:51.718609', '10.0.1.188', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (140, 'X537f6e206fae07c000000006f38d59c00000000', 5, '2014-05-23 11:49:51.718609', '2014-05-23 11:50:43.255259', '10.0.1.188', 'review', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (141, 'X537f6e53591fcb1b000000005e0a039100000000', 5, '2014-05-23 11:50:43.255259', '2014-05-23 12:50:43.255259', '10.0.1.188', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (142, 'X537f94ab1967fe5c000000003f0639e00000000', 5, '2014-05-23 14:34:18.554688', '2014-05-23 15:34:18.554688', '10.0.1.188', 'authenticate', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (143, 'X537fc06a303820300000000062e1650b00000000', 5, '2014-05-23 17:40:57.545071', '2014-05-23 17:40:59.630072', '10.0.1.113', 'authenticate', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (144, 'X537fc06cba489bc00000000696ad9a100000000', 5, '2014-05-23 17:40:59.630072', '2014-05-23 17:41:00.694706', '10.0.1.113', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (145, 'X537fc06d4c0bd41f00000000583f290300000000', 5, '2014-05-23 17:41:00.694706', '2014-05-23 17:41:06.965998', '10.0.1.113', 'adqueues', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (146, 'X537fc07329c790c2000000004d7ef81f00000000', 5, '2014-05-23 17:41:06.965998', '2014-05-23 17:49:29.333566', '10.0.1.113', 'review', NULL, NULL);
INSERT INTO tokens (token_id, token, admin_id, created_at, valid_to, remote_addr, info, store_id, email) VALUES (147, 'X537fc269893f480000000024c84ba400000000', 5, '2014-05-23 17:49:29.333566', '2014-05-23 18:49:29.333566', '10.0.1.113', 'adqueues', NULL, NULL);


--
-- Data for Name: action_states; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (1, 1, 150, 'reg', 'initial', '2015-02-20 15:02:25.783079', '192.168.4.75', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (1, 1, 151, 'unverified', 'verify', '2015-02-20 15:02:25.786454', '192.168.4.75', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (1, 1, 152, 'pending_review', 'verify', '2015-02-20 15:02:25.787623', '192.168.4.75', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (1, 1, 153, 'locked', 'checkout', '2015-02-20 15:02:25.788439', '192.168.4.75', 99999992);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (1, 1, 154, 'accepted', 'accept', '2015-02-20 15:02:25.789595', '192.168.4.75', 99999993);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (2, 1, 3, 'reg', 'initial', '2015-02-20 15:02:25.790615', '192.168.4.85', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (2, 1, 4, 'unverified', 'verifymail', '2015-02-20 15:02:25.791378', '192.168.4.85', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (3, 1, 5, 'reg', 'initial', '2015-02-20 15:02:25.792012', '192.168.4.85', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (3, 1, 6, 'unpaid', 'paymail', '2015-02-20 15:02:25.792683', '192.168.4.85', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (4, 1, 7, 'reg', 'initial', '2015-02-20 15:02:25.793399', '192.168.4.85', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (4, 1, 8, 'unverified', 'verifymail', '2015-02-20 15:02:25.794017', '192.168.4.85', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (6, 1, 50, 'reg', 'initial', '2015-02-20 15:02:25.794688', '192.168.4.75', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (6, 1, 51, 'unpaid', 'newad', '2015-02-20 15:02:25.795305', '192.168.4.75', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (6, 1, 52, 'pending_review', 'pay', '2015-02-20 15:02:25.795955', '192.168.4.75', 99999998);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (6, 1, 53, 'accepted', 'accept', '2015-02-20 15:02:25.796881', '192.168.4.75', 99999999);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (9, 1, 40, 'reg', 'initial', '2015-02-20 15:02:25.797759', '192.168.4.75', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (9, 1, 41, 'unpaid', 'newad', '2015-02-20 15:02:25.798384', '192.168.4.75', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (9, 1, 42, 'pending_review', 'pay', '2015-02-20 15:02:25.799018', '192.168.4.75', 99999918);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (9, 1, 43, 'refused', 'refuse', '2015-02-20 15:02:25.80005', '192.168.4.75', 99999919);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (12, 1, 160, 'reg', 'initial', '2015-02-20 15:02:25.800923', '192.168.4.75', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (12, 1, 161, 'unverified', 'verify', '2015-02-20 15:02:25.801536', '192.168.4.75', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (12, 1, 162, 'pending_review', 'verify', '2015-02-20 15:02:25.8021', '192.168.4.75', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (12, 1, 163, 'locked', 'checkout', '2015-02-20 15:02:25.802719', '192.168.4.75', 99999994);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (12, 1, 164, 'accepted', 'accept', '2015-02-20 15:02:25.803385', '192.168.4.75', 99999995);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (10, 1, 54, 'reg', 'initial', '2015-02-20 11:20:00', '192.168.4.75', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (10, 1, 55, 'unpaid', 'newad', '2015-02-20 15:02:25.806059', '192.168.4.75', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (10, 1, 56, 'pending_review', 'pay', '2015-02-20 15:02:25.806715', '192.168.4.75', 99999998);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (10, 1, 57, 'accepted', 'accept', '2015-02-20 15:02:25.807419', '192.168.4.75', 99999999);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (20, 1, 200, 'reg', 'initial', '2014-05-23 10:32:45.317842', '10.0.1.188', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (20, 1, 201, 'unverified', 'verifymail', '2014-05-23 10:32:45.317842', '10.0.1.188', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (20, 1, 202, 'pending_review', 'verify', '2014-05-23 10:32:45.381597', '10.0.1.188', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (21, 1, 203, 'reg', 'initial', '2014-05-23 10:34:31.188281', '10.0.1.188', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (21, 1, 204, 'unverified', 'verifymail', '2014-05-23 10:34:31.188281', '10.0.1.188', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (21, 1, 205, 'pending_review', 'verify', '2014-05-23 10:34:31.250691', '10.0.1.188', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (22, 1, 206, 'reg', 'initial', '2014-05-23 10:36:37.437651', '10.0.1.113', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (22, 1, 207, 'unverified', 'verifymail', '2014-05-23 10:36:37.437651', '10.0.1.113', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (22, 1, 208, 'pending_review', 'verify', '2014-05-23 10:36:37.490258', '10.0.1.113', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (23, 1, 209, 'reg', 'initial', '2014-05-23 10:37:58.488344', '10.0.1.113', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (23, 1, 210, 'unverified', 'verifymail', '2014-05-23 10:37:58.488344', '10.0.1.113', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (23, 1, 211, 'pending_review', 'verify', '2014-05-23 10:37:58.54872', '10.0.1.113', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (20, 1, 212, 'locked', 'checkout', '2014-05-23 10:40:15.332884', '10.0.1.188', 2);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (21, 1, 213, 'locked', 'checkout', '2014-05-23 10:40:15.332884', '10.0.1.188', 2);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (20, 1, 214, 'accepted', 'accept', '2014-05-23 10:40:18.736328', '10.0.1.188', 3);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (21, 1, 215, 'accepted', 'accept', '2014-05-23 10:40:21.127437', '10.0.1.188', 4);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (22, 1, 216, 'locked', 'checkout', '2014-05-23 10:40:23.43254', '10.0.1.188', 5);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (23, 1, 217, 'locked', 'checkout', '2014-05-23 10:40:23.43254', '10.0.1.188', 5);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (22, 1, 218, 'accepted', 'accept', '2014-05-23 10:40:28.211644', '10.0.1.188', 6);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (23, 1, 219, 'accepted', 'accept', '2014-05-23 10:40:36.136996', '10.0.1.188', 7);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (24, 1, 220, 'reg', 'initial', '2014-05-23 10:53:16.089876', '10.0.1.113', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (24, 1, 221, 'unverified', 'verifymail', '2014-05-23 10:53:16.089876', '10.0.1.113', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (24, 1, 222, 'pending_review', 'verify', '2014-05-23 10:53:16.151421', '10.0.1.113', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (25, 1, 223, 'reg', 'initial', '2014-05-23 10:53:58.34091', '10.0.1.113', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (25, 1, 224, 'unverified', 'verifymail', '2014-05-23 10:53:58.34091', '10.0.1.113', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (25, 1, 225, 'pending_review', 'verify', '2014-05-23 10:53:58.367732', '10.0.1.113', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (26, 1, 226, 'reg', 'initial', '2014-05-23 10:55:28.906747', '10.0.1.113', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (26, 1, 227, 'unverified', 'verifymail', '2014-05-23 10:55:28.906747', '10.0.1.113', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (26, 1, 228, 'pending_review', 'verify', '2014-05-23 10:55:28.97121', '10.0.1.113', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (27, 1, 229, 'reg', 'initial', '2014-05-23 10:56:08.078074', '10.0.1.113', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (27, 1, 230, 'unverified', 'verifymail', '2014-05-23 10:56:08.078074', '10.0.1.113', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (27, 1, 231, 'pending_review', 'verify', '2014-05-23 10:56:08.119746', '10.0.1.113', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (28, 1, 232, 'reg', 'initial', '2014-05-23 10:57:50.097853', '10.0.1.113', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (28, 1, 233, 'unverified', 'verifymail', '2014-05-23 10:57:50.097853', '10.0.1.113', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (28, 1, 234, 'pending_review', 'verify', '2014-05-23 10:57:50.161638', '10.0.1.113', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (29, 1, 235, 'reg', 'initial', '2014-05-23 10:58:43.60664', '10.0.1.188', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (29, 1, 236, 'unverified', 'verifymail', '2014-05-23 10:58:43.60664', '10.0.1.188', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (29, 1, 237, 'pending_review', 'verify', '2014-05-23 10:58:43.633507', '10.0.1.188', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (30, 1, 238, 'reg', 'initial', '2014-05-23 10:59:25.358339', '10.0.1.188', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (30, 1, 239, 'unverified', 'verifymail', '2014-05-23 10:59:25.358339', '10.0.1.188', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (30, 1, 240, 'pending_review', 'verify', '2014-05-23 10:59:25.375997', '10.0.1.188', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (31, 1, 241, 'reg', 'initial', '2014-05-23 11:00:09.575982', '10.0.1.188', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (31, 1, 242, 'unverified', 'verifymail', '2014-05-23 11:00:09.575982', '10.0.1.188', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (31, 1, 243, 'pending_review', 'verify', '2014-05-23 11:00:09.589702', '10.0.1.188', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (24, 1, 244, 'locked', 'checkout', '2014-05-23 11:00:14.839237', '10.0.1.188', 12);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (25, 1, 245, 'locked', 'checkout', '2014-05-23 11:00:14.839237', '10.0.1.188', 12);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (24, 1, 246, 'accepted', 'accept', '2014-05-23 11:00:20.258291', '10.0.1.188', 13);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (25, 1, 247, 'accepted', 'accept', '2014-05-23 11:00:22.804056', '10.0.1.188', 14);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (26, 1, 248, 'locked', 'checkout', '2014-05-23 11:00:25.731504', '10.0.1.188', 15);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (27, 1, 249, 'locked', 'checkout', '2014-05-23 11:00:25.731504', '10.0.1.188', 15);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (26, 1, 250, 'accepted', 'accept', '2014-05-23 11:00:31.272646', '10.0.1.188', 16);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (27, 1, 251, 'accepted', 'accept', '2014-05-23 11:00:34.141472', '10.0.1.188', 17);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (28, 1, 252, 'locked', 'checkout', '2014-05-23 11:00:36.113746', '10.0.1.188', 18);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (29, 1, 253, 'locked', 'checkout', '2014-05-23 11:00:36.113746', '10.0.1.188', 18);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (28, 1, 254, 'accepted', 'accept', '2014-05-23 11:00:39.332816', '10.0.1.188', 19);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (29, 1, 255, 'accepted', 'accept', '2014-05-23 11:00:42.490199', '10.0.1.188', 20);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (30, 1, 256, 'locked', 'checkout', '2014-05-23 11:00:43.631244', '10.0.1.188', 21);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (31, 1, 257, 'locked', 'checkout', '2014-05-23 11:00:43.631244', '10.0.1.188', 21);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (30, 1, 258, 'accepted', 'accept', '2014-05-23 11:00:46.511128', '10.0.1.188', 22);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (31, 1, 259, 'accepted', 'accept', '2014-05-23 11:00:48.871817', '10.0.1.188', 23);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (32, 1, 260, 'reg', 'initial', '2014-05-23 11:02:58.842354', '10.0.1.188', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (32, 1, 261, 'unverified', 'verifymail', '2014-05-23 11:02:58.842354', '10.0.1.188', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (32, 1, 262, 'pending_review', 'verify', '2014-05-23 11:02:58.902864', '10.0.1.188', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (33, 1, 263, 'reg', 'initial', '2014-05-23 11:03:34.025011', '10.0.1.188', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (33, 1, 264, 'unverified', 'verifymail', '2014-05-23 11:03:34.025011', '10.0.1.188', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (33, 1, 265, 'pending_review', 'verify', '2014-05-23 11:03:34.076472', '10.0.1.188', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (34, 1, 266, 'reg', 'initial', '2014-05-23 11:03:49.993978', '10.0.1.113', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (34, 1, 267, 'unverified', 'verifymail', '2014-05-23 11:03:49.993978', '10.0.1.113', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (34, 1, 268, 'pending_review', 'verify', '2014-05-23 11:03:50.015853', '10.0.1.113', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (35, 1, 269, 'reg', 'initial', '2014-05-23 11:04:17.162061', '10.0.1.188', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (35, 1, 270, 'unverified', 'verifymail', '2014-05-23 11:04:17.162061', '10.0.1.188', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (35, 1, 271, 'pending_review', 'verify', '2014-05-23 11:04:17.179985', '10.0.1.188', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (36, 1, 272, 'reg', 'initial', '2014-05-23 11:04:17.402456', '10.0.1.113', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (36, 1, 273, 'unverified', 'verifymail', '2014-05-23 11:04:17.402456', '10.0.1.113', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (36, 1, 274, 'pending_review', 'verify', '2014-05-23 11:04:17.425223', '10.0.1.113', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (32, 1, 275, 'locked', 'checkout', '2014-05-23 11:04:27.356829', '10.0.1.188', 29);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (33, 1, 276, 'locked', 'checkout', '2014-05-23 11:04:27.356829', '10.0.1.188', 29);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (32, 1, 277, 'pending_review', 'checkin', '2014-05-23 11:04:38.804156', '10.0.1.188', 30);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (33, 1, 278, 'accepted', 'accept', '2014-05-23 11:04:43.312292', '10.0.1.188', 31);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (32, 1, 279, 'locked', 'checkout', '2014-05-23 11:05:11.146703', '10.0.1.188', 32);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (34, 1, 280, 'locked', 'checkout', '2014-05-23 11:05:11.146703', '10.0.1.188', 32);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (32, 1, 281, 'accepted', 'accept', '2014-05-23 11:05:23.311501', '10.0.1.188', 33);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (34, 1, 282, 'accepted', 'accept', '2014-05-23 11:05:26.602346', '10.0.1.188', 34);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (35, 1, 283, 'locked', 'checkout', '2014-05-23 11:05:27.769772', '10.0.1.188', 35);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (36, 1, 284, 'locked', 'checkout', '2014-05-23 11:05:27.769772', '10.0.1.188', 35);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (37, 1, 285, 'reg', 'initial', '2014-05-23 11:05:28.878947', '10.0.1.113', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (37, 1, 286, 'unverified', 'verifymail', '2014-05-23 11:05:28.878947', '10.0.1.113', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (37, 1, 287, 'pending_review', 'verify', '2014-05-23 11:05:28.900894', '10.0.1.113', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (35, 1, 288, 'accepted', 'accept', '2014-05-23 11:05:31.766461', '10.0.1.188', 36);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (36, 1, 289, 'accepted', 'accept', '2014-05-23 11:05:36.404043', '10.0.1.188', 37);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (37, 1, 290, 'locked', 'checkout', '2014-05-23 11:05:38.23555', '10.0.1.188', 38);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (37, 1, 291, 'accepted', 'accept', '2014-05-23 11:05:41.839998', '10.0.1.188', 39);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (38, 1, 292, 'reg', 'initial', '2014-05-23 11:05:51.614044', '10.0.1.113', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (38, 1, 293, 'unverified', 'verifymail', '2014-05-23 11:05:51.614044', '10.0.1.113', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (38, 1, 294, 'pending_review', 'verify', '2014-05-23 11:05:51.647097', '10.0.1.113', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (38, 1, 295, 'locked', 'checkout', '2014-05-23 11:06:06.786665', '10.0.1.188', 43);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (38, 1, 296, 'accepted', 'accept', '2014-05-23 11:06:12.22863', '10.0.1.188', 44);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (39, 1, 297, 'reg', 'initial', '2014-05-23 11:06:18.665366', '10.0.1.113', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (39, 1, 298, 'unverified', 'verifymail', '2014-05-23 11:06:18.665366', '10.0.1.113', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (39, 1, 299, 'pending_review', 'verify', '2014-05-23 11:06:18.696732', '10.0.1.113', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (40, 1, 300, 'reg', 'initial', '2014-05-23 11:07:45.111576', '10.0.1.188', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (40, 1, 301, 'unverified', 'verifymail', '2014-05-23 11:07:45.111576', '10.0.1.188', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (40, 1, 302, 'pending_review', 'verify', '2014-05-23 11:07:45.192475', '10.0.1.188', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (41, 1, 303, 'reg', 'initial', '2014-05-23 11:07:45.787557', '10.0.1.113', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (41, 1, 304, 'unverified', 'verifymail', '2014-05-23 11:07:45.787557', '10.0.1.113', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (41, 1, 305, 'pending_review', 'verify', '2014-05-23 11:07:45.816011', '10.0.1.113', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (39, 1, 306, 'locked', 'checkout', '2014-05-23 11:07:48.656672', '10.0.1.188', 47);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (40, 1, 307, 'locked', 'checkout', '2014-05-23 11:07:48.656672', '10.0.1.188', 47);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (39, 1, 308, 'accepted', 'accept', '2014-05-23 11:07:52.153954', '10.0.1.188', 48);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (40, 1, 309, 'accepted', 'accept', '2014-05-23 11:07:56.397469', '10.0.1.188', 49);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (41, 1, 310, 'locked', 'checkout', '2014-05-23 11:07:57.807416', '10.0.1.188', 50);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (41, 1, 311, 'accepted', 'accept', '2014-05-23 11:08:02.325334', '10.0.1.188', 51);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (42, 1, 312, 'reg', 'initial', '2014-05-23 11:09:23.167989', '10.0.1.188', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (42, 1, 313, 'unverified', 'verifymail', '2014-05-23 11:09:23.167989', '10.0.1.188', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (42, 1, 314, 'pending_review', 'verify', '2014-05-23 11:09:23.251035', '10.0.1.188', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (43, 1, 315, 'reg', 'initial', '2014-05-23 11:09:48.828694', '10.0.1.188', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (43, 1, 316, 'unverified', 'verifymail', '2014-05-23 11:09:48.828694', '10.0.1.188', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (43, 1, 317, 'pending_review', 'verify', '2014-05-23 11:09:48.887385', '10.0.1.188', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (42, 1, 318, 'locked', 'checkout', '2014-05-23 11:09:56.769766', '10.0.1.188', 55);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (43, 1, 319, 'locked', 'checkout', '2014-05-23 11:09:56.769766', '10.0.1.188', 55);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (42, 1, 320, 'accepted', 'accept', '2014-05-23 11:10:15.442648', '10.0.1.188', 56);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (43, 1, 321, 'accepted', 'accept', '2014-05-23 11:15:16.730681', '10.0.1.188', 57);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (44, 1, 322, 'reg', 'initial', '2014-05-23 11:19:19.455645', '10.0.1.113', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (44, 1, 323, 'unverified', 'verifymail', '2014-05-23 11:19:19.455645', '10.0.1.113', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (44, 1, 324, 'pending_review', 'verify', '2014-05-23 11:19:19.504951', '10.0.1.113', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (45, 1, 325, 'reg', 'initial', '2014-05-23 11:20:15.225995', '10.0.1.188', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (45, 1, 326, 'unverified', 'verifymail', '2014-05-23 11:20:15.225995', '10.0.1.188', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (45, 1, 327, 'pending_review', 'verify', '2014-05-23 11:20:15.246294', '10.0.1.188', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (46, 1, 328, 'reg', 'initial', '2014-05-23 11:20:22.043844', '10.0.1.113', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (46, 1, 329, 'unverified', 'verifymail', '2014-05-23 11:20:22.043844', '10.0.1.113', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (46, 1, 330, 'pending_review', 'verify', '2014-05-23 11:20:22.066179', '10.0.1.113', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (44, 1, 331, 'locked', 'checkout', '2014-05-23 11:20:41.456755', '10.0.1.188', 61);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (45, 1, 332, 'locked', 'checkout', '2014-05-23 11:20:41.456755', '10.0.1.188', 61);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (44, 1, 333, 'accepted', 'accept', '2014-05-23 11:20:48.013186', '10.0.1.188', 62);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (47, 1, 334, 'reg', 'initial', '2014-05-23 11:20:55.220588', '10.0.1.113', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (47, 1, 335, 'unverified', 'verifymail', '2014-05-23 11:20:55.220588', '10.0.1.113', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (47, 1, 336, 'pending_review', 'verify', '2014-05-23 11:20:55.239002', '10.0.1.113', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (45, 1, 337, 'accepted', 'accept', '2014-05-23 11:20:59.773138', '10.0.1.188', 63);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (46, 1, 338, 'locked', 'checkout', '2014-05-23 11:21:02.946637', '10.0.1.188', 64);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (47, 1, 339, 'locked', 'checkout', '2014-05-23 11:21:02.946637', '10.0.1.188', 64);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (46, 1, 340, 'accepted', 'accept', '2014-05-23 11:21:05.984052', '10.0.1.188', 65);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (47, 1, 341, 'accepted', 'accept', '2014-05-23 11:21:09.944108', '10.0.1.188', 66);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (48, 1, 342, 'reg', 'initial', '2014-05-23 11:21:16.749539', '10.0.1.113', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (48, 1, 343, 'unverified', 'verifymail', '2014-05-23 11:21:16.749539', '10.0.1.113', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (48, 1, 344, 'pending_review', 'verify', '2014-05-23 11:21:16.786901', '10.0.1.113', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (49, 1, 345, 'reg', 'initial', '2014-05-23 11:22:07.228201', '10.0.1.188', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (49, 1, 346, 'unverified', 'verifymail', '2014-05-23 11:22:07.228201', '10.0.1.188', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (49, 1, 347, 'pending_review', 'verify', '2014-05-23 11:22:07.260533', '10.0.1.188', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (48, 1, 348, 'locked', 'checkout', '2014-05-23 11:22:11.759579', '10.0.1.188', 69);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (49, 1, 349, 'locked', 'checkout', '2014-05-23 11:22:11.759579', '10.0.1.188', 69);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (48, 1, 350, 'accepted', 'accept', '2014-05-23 11:22:17.904531', '10.0.1.188', 70);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (50, 1, 351, 'reg', 'initial', '2014-05-23 11:22:20.891643', '10.0.1.113', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (50, 1, 352, 'unverified', 'verifymail', '2014-05-23 11:22:20.891643', '10.0.1.113', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (50, 1, 353, 'pending_review', 'verify', '2014-05-23 11:22:20.915028', '10.0.1.113', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (49, 1, 354, 'accepted', 'accept', '2014-05-23 11:22:23.364362', '10.0.1.188', 71);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (50, 1, 355, 'locked', 'checkout', '2014-05-23 11:22:24.93418', '10.0.1.188', 72);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (50, 1, 356, 'accepted', 'accept', '2014-05-23 11:22:28.039458', '10.0.1.188', 73);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (51, 1, 357, 'reg', 'initial', '2014-05-23 11:22:55.121254', '10.0.1.188', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (51, 1, 358, 'unverified', 'verifymail', '2014-05-23 11:22:55.121254', '10.0.1.188', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (51, 1, 359, 'pending_review', 'verify', '2014-05-23 11:22:55.14269', '10.0.1.188', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (51, 1, 360, 'locked', 'checkout', '2014-05-23 11:23:06.984382', '10.0.1.188', 74);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (51, 1, 361, 'accepted', 'accept', '2014-05-23 11:23:11.653611', '10.0.1.188', 75);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (52, 1, 362, 'reg', 'initial', '2014-05-23 11:23:41.626963', '10.0.1.188', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (52, 1, 363, 'unverified', 'verifymail', '2014-05-23 11:23:41.626963', '10.0.1.188', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (52, 1, 364, 'pending_review', 'verify', '2014-05-23 11:23:41.657981', '10.0.1.188', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (53, 1, 365, 'reg', 'initial', '2014-05-23 11:24:33.819852', '10.0.1.188', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (53, 1, 366, 'unverified', 'verifymail', '2014-05-23 11:24:33.819852', '10.0.1.188', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (53, 1, 367, 'pending_review', 'verify', '2014-05-23 11:24:33.835947', '10.0.1.188', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (52, 1, 368, 'locked', 'checkout', '2014-05-23 11:24:39.088999', '10.0.1.188', 78);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (53, 1, 369, 'locked', 'checkout', '2014-05-23 11:24:39.088999', '10.0.1.188', 78);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (52, 1, 370, 'accepted', 'accept', '2014-05-23 11:24:43.416475', '10.0.1.188', 79);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (53, 1, 371, 'accepted', 'accept', '2014-05-23 11:24:45.909116', '10.0.1.188', 80);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (54, 1, 372, 'reg', 'initial', '2014-05-23 11:26:08.686185', '10.0.1.188', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (54, 1, 373, 'unverified', 'verifymail', '2014-05-23 11:26:08.686185', '10.0.1.188', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (54, 1, 374, 'pending_review', 'verify', '2014-05-23 11:26:08.748383', '10.0.1.188', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (55, 1, 375, 'reg', 'initial', '2014-05-23 11:27:36.401735', '10.0.1.188', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (55, 1, 376, 'unverified', 'verifymail', '2014-05-23 11:27:36.401735', '10.0.1.188', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (55, 1, 377, 'pending_review', 'verify', '2014-05-23 11:27:36.461705', '10.0.1.188', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (54, 1, 378, 'locked', 'checkout', '2014-05-23 11:27:40.480284', '10.0.1.188', 83);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (55, 1, 379, 'locked', 'checkout', '2014-05-23 11:27:40.480284', '10.0.1.188', 83);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (54, 1, 380, 'accepted', 'accept', '2014-05-23 11:27:44.679114', '10.0.1.188', 84);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (55, 1, 381, 'accepted', 'accept', '2014-05-23 11:27:50.537479', '10.0.1.188', 85);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (56, 1, 382, 'reg', 'initial', '2014-05-23 11:28:24.310768', '10.0.1.188', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (56, 1, 383, 'unverified', 'verifymail', '2014-05-23 11:28:24.310768', '10.0.1.188', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (56, 1, 384, 'pending_review', 'verify', '2014-05-23 11:28:24.336731', '10.0.1.188', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (57, 1, 385, 'reg', 'initial', '2014-05-23 11:28:56.557671', '10.0.1.188', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (57, 1, 386, 'unverified', 'verifymail', '2014-05-23 11:28:56.557671', '10.0.1.188', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (57, 1, 387, 'pending_review', 'verify', '2014-05-23 11:28:56.580059', '10.0.1.188', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (58, 1, 388, 'reg', 'initial', '2014-05-23 11:29:24.559797', '10.0.1.188', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (58, 1, 389, 'unverified', 'verifymail', '2014-05-23 11:29:24.559797', '10.0.1.188', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (58, 1, 390, 'pending_review', 'verify', '2014-05-23 11:29:24.586769', '10.0.1.188', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (56, 1, 391, 'locked', 'checkout', '2014-05-23 11:29:28.915929', '10.0.1.188', 88);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (57, 1, 392, 'locked', 'checkout', '2014-05-23 11:29:28.915929', '10.0.1.188', 88);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (56, 1, 393, 'accepted', 'accept', '2014-05-23 11:29:31.913321', '10.0.1.188', 89);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (59, 1, 394, 'reg', 'initial', '2014-05-23 11:29:33.116539', '10.0.1.113', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (59, 1, 395, 'unverified', 'verifymail', '2014-05-23 11:29:33.116539', '10.0.1.113', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (59, 1, 396, 'pending_review', 'verify', '2014-05-23 11:29:33.143105', '10.0.1.113', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (57, 1, 397, 'accepted', 'accept', '2014-05-23 11:29:36.949231', '10.0.1.188', 90);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (58, 1, 398, 'locked', 'checkout', '2014-05-23 11:29:38.45016', '10.0.1.188', 91);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (59, 1, 399, 'locked', 'checkout', '2014-05-23 11:29:38.45016', '10.0.1.188', 91);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (58, 1, 400, 'accepted', 'accept', '2014-05-23 11:29:43.811495', '10.0.1.188', 92);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (59, 1, 401, 'accepted', 'accept', '2014-05-23 11:29:47.267284', '10.0.1.188', 93);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (60, 1, 402, 'reg', 'initial', '2014-05-23 11:29:54.625049', '10.0.1.113', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (60, 1, 403, 'unverified', 'verifymail', '2014-05-23 11:29:54.625049', '10.0.1.113', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (60, 1, 404, 'pending_review', 'verify', '2014-05-23 11:29:54.643781', '10.0.1.113', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (61, 1, 405, 'reg', 'initial', '2014-05-23 11:30:22.888075', '10.0.1.113', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (61, 1, 406, 'unverified', 'verifymail', '2014-05-23 11:30:22.888075', '10.0.1.113', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (61, 1, 407, 'pending_review', 'verify', '2014-05-23 11:30:22.91408', '10.0.1.113', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (62, 1, 408, 'reg', 'initial', '2014-05-23 11:30:54.818169', '10.0.1.113', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (62, 1, 409, 'unverified', 'verifymail', '2014-05-23 11:30:54.818169', '10.0.1.113', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (62, 1, 410, 'pending_review', 'verify', '2014-05-23 11:30:54.837994', '10.0.1.113', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (63, 1, 411, 'reg', 'initial', '2014-05-23 11:31:30.026841', '10.0.1.113', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (63, 1, 412, 'unverified', 'verifymail', '2014-05-23 11:31:30.026841', '10.0.1.113', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (63, 1, 413, 'pending_review', 'verify', '2014-05-23 11:31:30.053002', '10.0.1.113', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (60, 1, 414, 'locked', 'checkout', '2014-05-23 11:34:55.736242', '10.0.1.188', 96);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (61, 1, 415, 'locked', 'checkout', '2014-05-23 11:34:55.736242', '10.0.1.188', 96);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (60, 1, 416, 'accepted', 'accept', '2014-05-23 11:34:59.239232', '10.0.1.188', 97);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (61, 1, 417, 'accepted', 'accept', '2014-05-23 11:35:02.638209', '10.0.1.188', 98);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (62, 1, 418, 'locked', 'checkout', '2014-05-23 11:35:04.605242', '10.0.1.188', 99);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (63, 1, 419, 'locked', 'checkout', '2014-05-23 11:35:04.605242', '10.0.1.188', 99);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (62, 1, 420, 'accepted', 'accept', '2014-05-23 11:35:09.75119', '10.0.1.188', 100);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (63, 1, 421, 'accepted', 'accept', '2014-05-23 11:35:12.026057', '10.0.1.188', 101);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (64, 1, 422, 'reg', 'initial', '2014-05-23 11:40:13.438414', '10.0.1.188', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (64, 1, 423, 'unverified', 'verifymail', '2014-05-23 11:40:13.438414', '10.0.1.188', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (64, 1, 424, 'pending_review', 'verify', '2014-05-23 11:40:13.502611', '10.0.1.188', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (65, 1, 425, 'reg', 'initial', '2014-05-23 11:41:16.086591', '10.0.1.188', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (65, 1, 426, 'unverified', 'verifymail', '2014-05-23 11:41:16.086591', '10.0.1.188', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (65, 1, 427, 'pending_review', 'verify', '2014-05-23 11:41:16.108298', '10.0.1.188', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (64, 1, 428, 'locked', 'checkout', '2014-05-23 11:41:23.71262', '10.0.1.188', 105);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (65, 1, 429, 'locked', 'checkout', '2014-05-23 11:41:23.71262', '10.0.1.188', 105);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (64, 1, 430, 'accepted', 'accept', '2014-05-23 11:41:27.073637', '10.0.1.188', 106);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (65, 1, 431, 'accepted', 'accept', '2014-05-23 11:41:31.41624', '10.0.1.188', 107);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (66, 1, 432, 'reg', 'initial', '2014-05-23 11:42:16.478715', '10.0.1.188', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (66, 1, 433, 'unverified', 'verifymail', '2014-05-23 11:42:16.478715', '10.0.1.188', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (66, 1, 434, 'pending_review', 'verify', '2014-05-23 11:42:16.516306', '10.0.1.188', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (67, 1, 435, 'reg', 'initial', '2014-05-23 11:42:42.151048', '10.0.1.113', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (67, 1, 436, 'unverified', 'verifymail', '2014-05-23 11:42:42.151048', '10.0.1.113', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (67, 1, 437, 'pending_review', 'verify', '2014-05-23 11:42:42.170115', '10.0.1.113', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (68, 1, 438, 'reg', 'initial', '2014-05-23 11:42:47.785329', '10.0.1.188', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (68, 1, 439, 'unverified', 'verifymail', '2014-05-23 11:42:47.785329', '10.0.1.188', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (68, 1, 440, 'pending_review', 'verify', '2014-05-23 11:42:47.805944', '10.0.1.188', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (66, 1, 441, 'locked', 'checkout', '2014-05-23 11:42:49.623455', '10.0.1.188', 110);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (67, 1, 442, 'locked', 'checkout', '2014-05-23 11:42:49.623455', '10.0.1.188', 110);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (66, 1, 443, 'accepted', 'accept', '2014-05-23 11:42:52.817063', '10.0.1.188', 111);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (67, 1, 444, 'accepted', 'accept', '2014-05-23 11:42:57.256163', '10.0.1.188', 112);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (68, 1, 445, 'locked', 'checkout', '2014-05-23 11:42:58.381755', '10.0.1.188', 113);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (68, 1, 446, 'accepted', 'accept', '2014-05-23 11:43:02.534327', '10.0.1.188', 114);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (69, 1, 447, 'reg', 'initial', '2014-05-23 11:43:05.069415', '10.0.1.113', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (69, 1, 448, 'unverified', 'verifymail', '2014-05-23 11:43:05.069415', '10.0.1.113', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (69, 1, 449, 'pending_review', 'verify', '2014-05-23 11:43:05.11272', '10.0.1.113', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (70, 1, 450, 'reg', 'initial', '2014-05-23 11:43:36.119927', '10.0.1.113', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (70, 1, 451, 'unverified', 'verifymail', '2014-05-23 11:43:36.119927', '10.0.1.113', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (70, 1, 452, 'pending_review', 'verify', '2014-05-23 11:43:36.144256', '10.0.1.113', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (71, 1, 453, 'reg', 'initial', '2014-05-23 11:43:41.185603', '10.0.1.188', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (71, 1, 454, 'unverified', 'verifymail', '2014-05-23 11:43:41.185603', '10.0.1.188', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (71, 1, 455, 'pending_review', 'verify', '2014-05-23 11:43:41.26679', '10.0.1.188', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (69, 1, 456, 'locked', 'checkout', '2014-05-23 11:43:49.153109', '10.0.1.188', 118);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (70, 1, 457, 'locked', 'checkout', '2014-05-23 11:43:49.153109', '10.0.1.188', 118);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (69, 1, 458, 'accepted', 'accept', '2014-05-23 11:43:51.921979', '10.0.1.188', 119);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (70, 1, 459, 'accepted', 'accept', '2014-05-23 11:43:57.246393', '10.0.1.188', 120);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (71, 1, 460, 'locked', 'checkout', '2014-05-23 11:43:57.709605', '10.0.1.188', 121);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (71, 1, 461, 'accepted', 'accept', '2014-05-23 11:44:02.876137', '10.0.1.188', 122);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (72, 1, 462, 'reg', 'initial', '2014-05-23 11:44:18.652111', '10.0.1.113', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (72, 1, 463, 'unverified', 'verifymail', '2014-05-23 11:44:18.652111', '10.0.1.113', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (72, 1, 464, 'pending_review', 'verify', '2014-05-23 11:44:18.734215', '10.0.1.113', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (72, 1, 465, 'locked', 'checkout', '2014-05-23 11:44:20.256385', '10.0.1.188', 125);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (72, 1, 466, 'accepted', 'accept', '2014-05-23 11:44:24.999002', '10.0.1.188', 126);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (73, 1, 467, 'reg', 'initial', '2014-05-23 11:44:41.644178', '10.0.1.113', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (73, 1, 468, 'unverified', 'verifymail', '2014-05-23 11:44:41.644178', '10.0.1.113', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (73, 1, 469, 'pending_review', 'verify', '2014-05-23 11:44:41.693763', '10.0.1.113', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (73, 1, 470, 'locked', 'checkout', '2014-05-23 11:47:08.787071', '10.0.1.188', 129);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (73, 1, 471, 'accepted', 'accept', '2014-05-23 11:47:11.486736', '10.0.1.188', 130);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (74, 1, 472, 'reg', 'initial', '2014-05-23 11:48:26.364845', '10.0.1.188', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (74, 1, 473, 'unverified', 'verifymail', '2014-05-23 11:48:26.364845', '10.0.1.188', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (74, 1, 474, 'pending_review', 'verify', '2014-05-23 11:48:26.443903', '10.0.1.188', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (74, 1, 475, 'accepted', 'accept', '2014-05-23 11:49:51.747734', '10.0.1.188', 139);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (68, 2, 476, 'reg', 'initial', '2014-05-23 11:49:51.788273', '127.0.0.1', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (68, 2, 477, 'accepted', 'adminclear', '2014-05-23 11:49:51.788273', '127.0.0.1', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (64, 2, 478, 'reg', 'initial', '2014-05-23 11:49:51.788273', '127.0.0.1', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (64, 2, 479, 'accepted', 'adminclear', '2014-05-23 11:49:51.788273', '127.0.0.1', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (65, 2, 480, 'reg', 'initial', '2014-05-23 11:49:51.788273', '127.0.0.1', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (65, 2, 481, 'accepted', 'adminclear', '2014-05-23 11:49:51.788273', '127.0.0.1', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (66, 2, 482, 'reg', 'initial', '2014-05-23 11:49:51.788273', '127.0.0.1', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (66, 2, 483, 'accepted', 'adminclear', '2014-05-23 11:49:51.788273', '127.0.0.1', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (71, 2, 484, 'reg', 'initial', '2014-05-23 11:49:51.788273', '127.0.0.1', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (71, 2, 485, 'accepted', 'adminclear', '2014-05-23 11:49:51.788273', '127.0.0.1', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (74, 2, 486, 'reg', 'initial', '2014-05-23 11:49:51.788273', '127.0.0.1', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (74, 2, 487, 'accepted', 'adminclear', '2014-05-23 11:49:51.788273', '127.0.0.1', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (75, 1, 488, 'reg', 'initial', '2014-05-23 17:40:25.077355', '10.0.1.113', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (75, 1, 489, 'unverified', 'verifymail', '2014-05-23 17:40:25.077355', '10.0.1.113', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (75, 1, 490, 'pending_review', 'verify', '2014-05-23 17:40:25.204814', '10.0.1.113', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (75, 1, 491, 'locked', 'checkout', '2014-05-23 17:41:00.699117', '10.0.1.113', 144);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (75, 1, 492, 'accepted', 'accept', '2014-05-23 17:41:06.971216', '10.0.1.113', 145);


--
-- Name: action_states_state_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('action_states_state_id_seq', 492, true);


--
-- Data for Name: ad_changes; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO ad_changes (ad_id, action_id, state_id, is_param, column_name, old_value, new_value) VALUES (32, 1, 281, false, 'type', 'sell', 'buy');


--
-- Data for Name: ad_codes; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO ad_codes (code, code_type) VALUES (12345, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (65432, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (32323, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (54545, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (112200000, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (112200001, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (112200002, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (112200003, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (112200004, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (112200005, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (112200006, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (112200007, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (112200008, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (112200009, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (90001, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (90002, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (90003, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (90004, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (90005, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (90006, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (90007, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (90008, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (90009, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (90010, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (90011, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (90012, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (90013, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (90014, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (90015, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (90016, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (90017, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (90018, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (90019, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (90020, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (90021, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (90022, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (90023, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (90024, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (90025, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (90026, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (90027, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (90028, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (90029, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (90030, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (90031, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (90032, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (90033, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (90034, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (90035, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (90036, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (90037, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (90038, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (90039, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (90040, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (42023, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (74046, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (16069, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (48092, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (80115, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (22138, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (54161, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (86184, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (28207, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (60230, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (92253, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (34276, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (66299, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (98322, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (40345, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (72368, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (14391, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (46414, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (78437, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (20460, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (52483, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (84506, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (26529, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (58552, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (90575, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (32598, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (64621, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (96644, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (38667, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (70690, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (12713, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (44736, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (76759, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (18782, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (50805, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (82828, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (24851, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (56874, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (88897, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (30920, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (62943, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (94966, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (36989, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (69012, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (11035, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (43058, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (75081, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (17104, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (49127, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (81150, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (23173, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (55196, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (87219, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (29242, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (61265, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (93288, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (235392023, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (370784046, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (506176069, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (641568092, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (776960115, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (912352138, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (147744161, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (283136184, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (418528207, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (553920230, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (689312253, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (824704276, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (960096299, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (195488322, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (330880345, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (466272368, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (601664391, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (737056414, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (872448437, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (107840460, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (243232483, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (378624506, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (514016529, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (649408552, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (784800575, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (920192598, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (155584621, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (290976644, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (426368667, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (561760690, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (697152713, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (832544736, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (967936759, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (203328782, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (338720805, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (474112828, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (609504851, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (744896874, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (880288897, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (115680920, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (251072943, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (386464966, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (521856989, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (657249012, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (792641035, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (928033058, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (163425081, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (298817104, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (434209127, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (569601150, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (704993173, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (840385196, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (975777219, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (211169242, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (346561265, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (481953288, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (617345311, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (752737334, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (888129357, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (123521380, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (258913403, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (394305426, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (529697449, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (665089472, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (800481495, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (935873518, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (171265541, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (306657564, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (442049587, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (577441610, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (712833633, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (848225656, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (983617679, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (219009702, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (354401725, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (489793748, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (625185771, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (760577794, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (895969817, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (131361840, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (266753863, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (402145886, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (537537909, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (672929932, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (808321955, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (943713978, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (179106001, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (314498024, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (449890047, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (585282070, 'verify');
INSERT INTO ad_codes (code, code_type) VALUES (22530, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (86576, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (35311, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (67334, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (99357, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (41380, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (73403, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (15426, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (47449, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (79472, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (21495, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (53518, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (85541, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (27564, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (59587, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (91610, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (33633, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (65656, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (97679, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (39702, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (71725, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (13748, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (45771, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (77794, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (19817, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (51840, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (83863, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (25886, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (57909, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (89932, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (31955, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (63978, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (96001, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (38024, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (70047, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (12070, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (44093, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (76116, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (18139, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (50162, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (82185, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (24208, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (56231, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (88254, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (30277, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (62300, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (94323, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (36346, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (68369, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (10392, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (42415, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (74438, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (16461, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (48484, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (80507, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (54553, 'pay');


--
-- Data for Name: ad_image_changes; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO ad_image_changes (ad_id, action_id, state_id, seq_no, name, is_new) VALUES (20, 1, 214, 0, NULL, false);
INSERT INTO ad_image_changes (ad_id, action_id, state_id, seq_no, name, is_new) VALUES (21, 1, 215, 0, NULL, false);
INSERT INTO ad_image_changes (ad_id, action_id, state_id, seq_no, name, is_new) VALUES (22, 1, 218, 0, NULL, false);
INSERT INTO ad_image_changes (ad_id, action_id, state_id, seq_no, name, is_new) VALUES (23, 1, 219, 0, NULL, false);
INSERT INTO ad_image_changes (ad_id, action_id, state_id, seq_no, name, is_new) VALUES (24, 1, 246, 0, NULL, false);
INSERT INTO ad_image_changes (ad_id, action_id, state_id, seq_no, name, is_new) VALUES (25, 1, 247, 0, NULL, false);
INSERT INTO ad_image_changes (ad_id, action_id, state_id, seq_no, name, is_new) VALUES (26, 1, 250, 0, NULL, false);
INSERT INTO ad_image_changes (ad_id, action_id, state_id, seq_no, name, is_new) VALUES (27, 1, 251, 0, NULL, false);
INSERT INTO ad_image_changes (ad_id, action_id, state_id, seq_no, name, is_new) VALUES (28, 1, 254, 0, NULL, false);
INSERT INTO ad_image_changes (ad_id, action_id, state_id, seq_no, name, is_new) VALUES (29, 1, 255, 0, NULL, false);
INSERT INTO ad_image_changes (ad_id, action_id, state_id, seq_no, name, is_new) VALUES (30, 1, 258, 0, NULL, false);
INSERT INTO ad_image_changes (ad_id, action_id, state_id, seq_no, name, is_new) VALUES (31, 1, 259, 0, NULL, false);
INSERT INTO ad_image_changes (ad_id, action_id, state_id, seq_no, name, is_new) VALUES (33, 1, 278, 0, NULL, false);
INSERT INTO ad_image_changes (ad_id, action_id, state_id, seq_no, name, is_new) VALUES (32, 1, 281, 0, NULL, false);
INSERT INTO ad_image_changes (ad_id, action_id, state_id, seq_no, name, is_new) VALUES (34, 1, 282, 0, NULL, false);
INSERT INTO ad_image_changes (ad_id, action_id, state_id, seq_no, name, is_new) VALUES (35, 1, 288, 0, NULL, false);
INSERT INTO ad_image_changes (ad_id, action_id, state_id, seq_no, name, is_new) VALUES (36, 1, 289, 0, NULL, false);
INSERT INTO ad_image_changes (ad_id, action_id, state_id, seq_no, name, is_new) VALUES (37, 1, 291, 0, NULL, false);
INSERT INTO ad_image_changes (ad_id, action_id, state_id, seq_no, name, is_new) VALUES (38, 1, 296, 0, NULL, false);
INSERT INTO ad_image_changes (ad_id, action_id, state_id, seq_no, name, is_new) VALUES (39, 1, 308, 0, NULL, false);
INSERT INTO ad_image_changes (ad_id, action_id, state_id, seq_no, name, is_new) VALUES (40, 1, 309, 0, NULL, false);
INSERT INTO ad_image_changes (ad_id, action_id, state_id, seq_no, name, is_new) VALUES (41, 1, 311, 0, NULL, false);
INSERT INTO ad_image_changes (ad_id, action_id, state_id, seq_no, name, is_new) VALUES (42, 1, 320, 0, NULL, false);
INSERT INTO ad_image_changes (ad_id, action_id, state_id, seq_no, name, is_new) VALUES (43, 1, 321, 0, NULL, false);
INSERT INTO ad_image_changes (ad_id, action_id, state_id, seq_no, name, is_new) VALUES (44, 1, 333, 0, NULL, false);
INSERT INTO ad_image_changes (ad_id, action_id, state_id, seq_no, name, is_new) VALUES (45, 1, 337, 0, NULL, false);
INSERT INTO ad_image_changes (ad_id, action_id, state_id, seq_no, name, is_new) VALUES (46, 1, 340, 0, NULL, false);
INSERT INTO ad_image_changes (ad_id, action_id, state_id, seq_no, name, is_new) VALUES (47, 1, 341, 0, NULL, false);
INSERT INTO ad_image_changes (ad_id, action_id, state_id, seq_no, name, is_new) VALUES (48, 1, 350, 0, NULL, false);
INSERT INTO ad_image_changes (ad_id, action_id, state_id, seq_no, name, is_new) VALUES (49, 1, 354, 0, NULL, false);
INSERT INTO ad_image_changes (ad_id, action_id, state_id, seq_no, name, is_new) VALUES (50, 1, 356, 0, NULL, false);
INSERT INTO ad_image_changes (ad_id, action_id, state_id, seq_no, name, is_new) VALUES (51, 1, 361, 0, NULL, false);
INSERT INTO ad_image_changes (ad_id, action_id, state_id, seq_no, name, is_new) VALUES (52, 1, 370, 0, NULL, false);
INSERT INTO ad_image_changes (ad_id, action_id, state_id, seq_no, name, is_new) VALUES (53, 1, 371, 0, NULL, false);
INSERT INTO ad_image_changes (ad_id, action_id, state_id, seq_no, name, is_new) VALUES (54, 1, 380, 0, NULL, false);
INSERT INTO ad_image_changes (ad_id, action_id, state_id, seq_no, name, is_new) VALUES (55, 1, 381, 0, NULL, false);
INSERT INTO ad_image_changes (ad_id, action_id, state_id, seq_no, name, is_new) VALUES (56, 1, 393, 0, NULL, false);
INSERT INTO ad_image_changes (ad_id, action_id, state_id, seq_no, name, is_new) VALUES (57, 1, 397, 0, NULL, false);
INSERT INTO ad_image_changes (ad_id, action_id, state_id, seq_no, name, is_new) VALUES (58, 1, 400, 0, NULL, false);
INSERT INTO ad_image_changes (ad_id, action_id, state_id, seq_no, name, is_new) VALUES (59, 1, 401, 0, NULL, false);
INSERT INTO ad_image_changes (ad_id, action_id, state_id, seq_no, name, is_new) VALUES (60, 1, 416, 0, NULL, false);
INSERT INTO ad_image_changes (ad_id, action_id, state_id, seq_no, name, is_new) VALUES (61, 1, 417, 0, NULL, false);
INSERT INTO ad_image_changes (ad_id, action_id, state_id, seq_no, name, is_new) VALUES (62, 1, 420, 0, NULL, false);
INSERT INTO ad_image_changes (ad_id, action_id, state_id, seq_no, name, is_new) VALUES (63, 1, 421, 0, NULL, false);
INSERT INTO ad_image_changes (ad_id, action_id, state_id, seq_no, name, is_new) VALUES (64, 1, 430, 0, NULL, false);
INSERT INTO ad_image_changes (ad_id, action_id, state_id, seq_no, name, is_new) VALUES (65, 1, 431, 0, NULL, false);
INSERT INTO ad_image_changes (ad_id, action_id, state_id, seq_no, name, is_new) VALUES (66, 1, 443, 0, NULL, false);
INSERT INTO ad_image_changes (ad_id, action_id, state_id, seq_no, name, is_new) VALUES (67, 1, 444, 0, NULL, false);
INSERT INTO ad_image_changes (ad_id, action_id, state_id, seq_no, name, is_new) VALUES (68, 1, 446, 0, NULL, false);
INSERT INTO ad_image_changes (ad_id, action_id, state_id, seq_no, name, is_new) VALUES (69, 1, 458, 0, NULL, false);
INSERT INTO ad_image_changes (ad_id, action_id, state_id, seq_no, name, is_new) VALUES (70, 1, 459, 0, NULL, false);
INSERT INTO ad_image_changes (ad_id, action_id, state_id, seq_no, name, is_new) VALUES (71, 1, 461, 0, NULL, false);
INSERT INTO ad_image_changes (ad_id, action_id, state_id, seq_no, name, is_new) VALUES (72, 1, 466, 0, NULL, false);
INSERT INTO ad_image_changes (ad_id, action_id, state_id, seq_no, name, is_new) VALUES (73, 1, 471, 0, NULL, false);
INSERT INTO ad_image_changes (ad_id, action_id, state_id, seq_no, name, is_new) VALUES (74, 1, 475, 0, NULL, false);
INSERT INTO ad_image_changes (ad_id, action_id, state_id, seq_no, name, is_new) VALUES (75, 1, 492, 0, NULL, false);


--
-- Data for Name: ad_images; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Data for Name: ad_images_digests; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Data for Name: ad_media; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO ad_media (ad_media_id, ad_id, seq_no, upload_time, media_type, width, height, length) VALUES (100012344, 6, 0, '2015-02-20 15:02:25.80809', 'image', NULL, NULL, NULL);
INSERT INTO ad_media (ad_media_id, ad_id, seq_no, upload_time, media_type, width, height, length) VALUES (100012345, 6, 1, '2015-02-20 15:02:25.809793', 'image', NULL, NULL, NULL);
INSERT INTO ad_media (ad_media_id, ad_id, seq_no, upload_time, media_type, width, height, length) VALUES (100012346, 6, 2, '2015-02-20 15:02:25.810907', 'image', NULL, NULL, NULL);


--
-- Data for Name: ad_media_changes; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Data for Name: ad_params; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO ad_params (ad_id, name, value) VALUES (1, 'rooms', '2');
INSERT INTO ad_params (ad_id, name, value) VALUES (1, 'size', '250');
INSERT INTO ad_params (ad_id, name, value) VALUES (1, 'garage_spaces', '2');
INSERT INTO ad_params (ad_id, name, value) VALUES (1, 'condominio', '400');
INSERT INTO ad_params (ad_id, name, value) VALUES (1, 'communes', '5');
INSERT INTO ad_params (ad_id, name, value) VALUES (1, 'currency', 'peso');
INSERT INTO ad_params (ad_id, name, value) VALUES (1, 'country', 'UNK');
INSERT INTO ad_params (ad_id, name, value) VALUES (6, 'regdate', '1986');
INSERT INTO ad_params (ad_id, name, value) VALUES (6, 'mileage', '1');
INSERT INTO ad_params (ad_id, name, value) VALUES (6, 'fuel', '2');
INSERT INTO ad_params (ad_id, name, value) VALUES (6, 'gearbox', '2');
INSERT INTO ad_params (ad_id, name, value) VALUES (11, 'service_type', '6');
INSERT INTO ad_params (ad_id, name, value) VALUES (12, 'rooms', '3');
INSERT INTO ad_params (ad_id, name, value) VALUES (12, 'size', '120');
INSERT INTO ad_params (ad_id, name, value) VALUES (12, 'garage_spaces', '1');
INSERT INTO ad_params (ad_id, name, value) VALUES (12, 'condominio', '400');
INSERT INTO ad_params (ad_id, name, value) VALUES (12, 'communes', '323');
INSERT INTO ad_params (ad_id, name, value) VALUES (12, 'currency', 'peso');
INSERT INTO ad_params (ad_id, name, value) VALUES (12, 'country', 'UNK');
INSERT INTO ad_params (ad_id, name, value) VALUES (20, 'prev_currency', 'peso');
INSERT INTO ad_params (ad_id, name, value) VALUES (20, 'country', 'UNK');
INSERT INTO ad_params (ad_id, name, value) VALUES (21, 'prev_currency', 'peso');
INSERT INTO ad_params (ad_id, name, value) VALUES (21, 'country', 'UNK');
INSERT INTO ad_params (ad_id, name, value) VALUES (22, 'job_category', '19');
INSERT INTO ad_params (ad_id, name, value) VALUES (22, 'country', 'UNK');
INSERT INTO ad_params (ad_id, name, value) VALUES (23, 'prev_currency', 'peso');
INSERT INTO ad_params (ad_id, name, value) VALUES (23, 'country', 'UNK');
INSERT INTO ad_params (ad_id, name, value) VALUES (24, 'rooms', '1');
INSERT INTO ad_params (ad_id, name, value) VALUES (24, 'communes', '296');
INSERT INTO ad_params (ad_id, name, value) VALUES (24, 'prev_currency', 'peso');
INSERT INTO ad_params (ad_id, name, value) VALUES (24, 'country', 'UNK');
INSERT INTO ad_params (ad_id, name, value) VALUES (25, 'regdate', '2012');
INSERT INTO ad_params (ad_id, name, value) VALUES (25, 'mileage', '10000');
INSERT INTO ad_params (ad_id, name, value) VALUES (25, 'gearbox', '2');
INSERT INTO ad_params (ad_id, name, value) VALUES (25, 'fuel', '3');
INSERT INTO ad_params (ad_id, name, value) VALUES (25, 'cartype', '2');
INSERT INTO ad_params (ad_id, name, value) VALUES (25, 'prev_currency', 'peso');
INSERT INTO ad_params (ad_id, name, value) VALUES (25, 'brand', '2');
INSERT INTO ad_params (ad_id, name, value) VALUES (25, 'model', '2');
INSERT INTO ad_params (ad_id, name, value) VALUES (25, 'version', '2');
INSERT INTO ad_params (ad_id, name, value) VALUES (25, 'country', 'UNK');
INSERT INTO ad_params (ad_id, name, value) VALUES (26, 'job_category', '20');
INSERT INTO ad_params (ad_id, name, value) VALUES (26, 'country', 'UNK');
INSERT INTO ad_params (ad_id, name, value) VALUES (27, 'rooms', '3');
INSERT INTO ad_params (ad_id, name, value) VALUES (27, 'communes', '297');
INSERT INTO ad_params (ad_id, name, value) VALUES (27, 'prev_currency', 'peso');
INSERT INTO ad_params (ad_id, name, value) VALUES (27, 'country', 'UNK');
INSERT INTO ad_params (ad_id, name, value) VALUES (28, 'regdate', '2011');
INSERT INTO ad_params (ad_id, name, value) VALUES (28, 'mileage', '3000');
INSERT INTO ad_params (ad_id, name, value) VALUES (28, 'cubiccms', '6');
INSERT INTO ad_params (ad_id, name, value) VALUES (28, 'prev_currency', 'peso');
INSERT INTO ad_params (ad_id, name, value) VALUES (28, 'country', 'UNK');
INSERT INTO ad_params (ad_id, name, value) VALUES (29, 'rooms', '1');
INSERT INTO ad_params (ad_id, name, value) VALUES (29, 'size', '123');
INSERT INTO ad_params (ad_id, name, value) VALUES (29, 'garage_spaces', '123');
INSERT INTO ad_params (ad_id, name, value) VALUES (29, 'condominio', '123');
INSERT INTO ad_params (ad_id, name, value) VALUES (29, 'communes', '296');
INSERT INTO ad_params (ad_id, name, value) VALUES (29, 'prev_currency', 'peso');
INSERT INTO ad_params (ad_id, name, value) VALUES (29, 'country', 'UNK');
INSERT INTO ad_params (ad_id, name, value) VALUES (30, 'rooms', '3');
INSERT INTO ad_params (ad_id, name, value) VALUES (30, 'size', '123');
INSERT INTO ad_params (ad_id, name, value) VALUES (30, 'garage_spaces', '123');
INSERT INTO ad_params (ad_id, name, value) VALUES (30, 'condominio', '123');
INSERT INTO ad_params (ad_id, name, value) VALUES (30, 'communes', '297');
INSERT INTO ad_params (ad_id, name, value) VALUES (30, 'prev_currency', 'peso');
INSERT INTO ad_params (ad_id, name, value) VALUES (30, 'country', 'UNK');
INSERT INTO ad_params (ad_id, name, value) VALUES (31, 'size', '1231');
INSERT INTO ad_params (ad_id, name, value) VALUES (31, 'garage_spaces', '211');
INSERT INTO ad_params (ad_id, name, value) VALUES (31, 'communes', '300');
INSERT INTO ad_params (ad_id, name, value) VALUES (31, 'prev_currency', 'peso');
INSERT INTO ad_params (ad_id, name, value) VALUES (31, 'country', 'UNK');
INSERT INTO ad_params (ad_id, name, value) VALUES (32, 'regdate', '2012');
INSERT INTO ad_params (ad_id, name, value) VALUES (32, 'mileage', '123');
INSERT INTO ad_params (ad_id, name, value) VALUES (32, 'gearbox', '1');
INSERT INTO ad_params (ad_id, name, value) VALUES (32, 'fuel', '1');
INSERT INTO ad_params (ad_id, name, value) VALUES (32, 'prev_currency', 'peso');
INSERT INTO ad_params (ad_id, name, value) VALUES (32, 'country', 'UNK');
INSERT INTO ad_params (ad_id, name, value) VALUES (33, 'prev_currency', 'peso');
INSERT INTO ad_params (ad_id, name, value) VALUES (33, 'country', 'UNK');
INSERT INTO ad_params (ad_id, name, value) VALUES (34, 'rooms', '2');
INSERT INTO ad_params (ad_id, name, value) VALUES (34, 'communes', '296');
INSERT INTO ad_params (ad_id, name, value) VALUES (34, 'prev_currency', 'peso');
INSERT INTO ad_params (ad_id, name, value) VALUES (34, 'country', 'UNK');
INSERT INTO ad_params (ad_id, name, value) VALUES (35, 'regdate', '2013');
INSERT INTO ad_params (ad_id, name, value) VALUES (35, 'mileage', '123');
INSERT INTO ad_params (ad_id, name, value) VALUES (35, 'cubiccms', '1');
INSERT INTO ad_params (ad_id, name, value) VALUES (35, 'prev_currency', 'peso');
INSERT INTO ad_params (ad_id, name, value) VALUES (35, 'country', 'UNK');
INSERT INTO ad_params (ad_id, name, value) VALUES (36, 'rooms', '3');
INSERT INTO ad_params (ad_id, name, value) VALUES (36, 'communes', '300');
INSERT INTO ad_params (ad_id, name, value) VALUES (36, 'prev_currency', 'peso');
INSERT INTO ad_params (ad_id, name, value) VALUES (36, 'country', 'UNK');
INSERT INTO ad_params (ad_id, name, value) VALUES (37, 'communes', '299');
INSERT INTO ad_params (ad_id, name, value) VALUES (37, 'prev_currency', 'peso');
INSERT INTO ad_params (ad_id, name, value) VALUES (37, 'country', 'UNK');
INSERT INTO ad_params (ad_id, name, value) VALUES (38, 'communes', '301');
INSERT INTO ad_params (ad_id, name, value) VALUES (38, 'prev_currency', 'peso');
INSERT INTO ad_params (ad_id, name, value) VALUES (38, 'country', 'UNK');
INSERT INTO ad_params (ad_id, name, value) VALUES (39, 'rooms', '3');
INSERT INTO ad_params (ad_id, name, value) VALUES (39, 'communes', '297');
INSERT INTO ad_params (ad_id, name, value) VALUES (39, 'prev_currency', 'peso');
INSERT INTO ad_params (ad_id, name, value) VALUES (39, 'country', 'UNK');
INSERT INTO ad_params (ad_id, name, value) VALUES (40, 'prev_currency', 'peso');
INSERT INTO ad_params (ad_id, name, value) VALUES (40, 'country', 'UNK');
INSERT INTO ad_params (ad_id, name, value) VALUES (41, 'communes', '299');
INSERT INTO ad_params (ad_id, name, value) VALUES (41, 'prev_currency', 'peso');
INSERT INTO ad_params (ad_id, name, value) VALUES (41, 'country', 'UNK');
INSERT INTO ad_params (ad_id, name, value) VALUES (42, 'regdate', '2012');
INSERT INTO ad_params (ad_id, name, value) VALUES (42, 'mileage', '123');
INSERT INTO ad_params (ad_id, name, value) VALUES (42, 'cubiccms', '3');
INSERT INTO ad_params (ad_id, name, value) VALUES (42, 'prev_currency', 'peso');
INSERT INTO ad_params (ad_id, name, value) VALUES (42, 'country', 'UNK');
INSERT INTO ad_params (ad_id, name, value) VALUES (43, 'prev_currency', 'peso');
INSERT INTO ad_params (ad_id, name, value) VALUES (43, 'country', 'UNK');
INSERT INTO ad_params (ad_id, name, value) VALUES (44, 'rooms', '3');
INSERT INTO ad_params (ad_id, name, value) VALUES (44, 'communes', '297');
INSERT INTO ad_params (ad_id, name, value) VALUES (44, 'prev_currency', 'peso');
INSERT INTO ad_params (ad_id, name, value) VALUES (44, 'country', 'UNK');
INSERT INTO ad_params (ad_id, name, value) VALUES (45, 'service_type', '7');
INSERT INTO ad_params (ad_id, name, value) VALUES (45, 'prev_currency', 'peso');
INSERT INTO ad_params (ad_id, name, value) VALUES (45, 'country', 'UNK');
INSERT INTO ad_params (ad_id, name, value) VALUES (46, 'rooms', '2');
INSERT INTO ad_params (ad_id, name, value) VALUES (46, 'communes', '298');
INSERT INTO ad_params (ad_id, name, value) VALUES (46, 'prev_currency', 'peso');
INSERT INTO ad_params (ad_id, name, value) VALUES (46, 'country', 'UNK');
INSERT INTO ad_params (ad_id, name, value) VALUES (47, 'rooms', '2');
INSERT INTO ad_params (ad_id, name, value) VALUES (47, 'communes', '304');
INSERT INTO ad_params (ad_id, name, value) VALUES (47, 'prev_currency', 'peso');
INSERT INTO ad_params (ad_id, name, value) VALUES (47, 'country', 'UNK');
INSERT INTO ad_params (ad_id, name, value) VALUES (48, 'rooms', '2');
INSERT INTO ad_params (ad_id, name, value) VALUES (48, 'communes', '311');
INSERT INTO ad_params (ad_id, name, value) VALUES (48, 'prev_currency', 'peso');
INSERT INTO ad_params (ad_id, name, value) VALUES (48, 'country', 'UNK');
INSERT INTO ad_params (ad_id, name, value) VALUES (49, 'service_type', '7');
INSERT INTO ad_params (ad_id, name, value) VALUES (49, 'prev_currency', 'peso');
INSERT INTO ad_params (ad_id, name, value) VALUES (49, 'country', 'UNK');
INSERT INTO ad_params (ad_id, name, value) VALUES (50, 'rooms', '2');
INSERT INTO ad_params (ad_id, name, value) VALUES (50, 'communes', '304');
INSERT INTO ad_params (ad_id, name, value) VALUES (50, 'prev_currency', 'peso');
INSERT INTO ad_params (ad_id, name, value) VALUES (50, 'country', 'UNK');
INSERT INTO ad_params (ad_id, name, value) VALUES (51, 'service_type', '3');
INSERT INTO ad_params (ad_id, name, value) VALUES (51, 'prev_currency', 'peso');
INSERT INTO ad_params (ad_id, name, value) VALUES (51, 'country', 'UNK');
INSERT INTO ad_params (ad_id, name, value) VALUES (52, 'service_type', '4');
INSERT INTO ad_params (ad_id, name, value) VALUES (52, 'prev_currency', 'peso');
INSERT INTO ad_params (ad_id, name, value) VALUES (52, 'country', 'UNK');
INSERT INTO ad_params (ad_id, name, value) VALUES (53, 'service_type', '8');
INSERT INTO ad_params (ad_id, name, value) VALUES (53, 'prev_currency', 'peso');
INSERT INTO ad_params (ad_id, name, value) VALUES (53, 'country', 'UNK');
INSERT INTO ad_params (ad_id, name, value) VALUES (54, 'rooms', '2');
INSERT INTO ad_params (ad_id, name, value) VALUES (54, 'size', '290');
INSERT INTO ad_params (ad_id, name, value) VALUES (54, 'garage_spaces', '2');
INSERT INTO ad_params (ad_id, name, value) VALUES (54, 'condominio', '90000');
INSERT INTO ad_params (ad_id, name, value) VALUES (54, 'communes', '300');
INSERT INTO ad_params (ad_id, name, value) VALUES (54, 'prev_currency', 'peso');
INSERT INTO ad_params (ad_id, name, value) VALUES (54, 'country', 'UNK');
INSERT INTO ad_params (ad_id, name, value) VALUES (55, 'rooms', '2');
INSERT INTO ad_params (ad_id, name, value) VALUES (55, 'size', '123');
INSERT INTO ad_params (ad_id, name, value) VALUES (55, 'garage_spaces', '123');
INSERT INTO ad_params (ad_id, name, value) VALUES (55, 'condominio', '123');
INSERT INTO ad_params (ad_id, name, value) VALUES (55, 'communes', '298');
INSERT INTO ad_params (ad_id, name, value) VALUES (55, 'prev_currency', 'peso');
INSERT INTO ad_params (ad_id, name, value) VALUES (55, 'country', 'UNK');
INSERT INTO ad_params (ad_id, name, value) VALUES (56, 'size', '123');
INSERT INTO ad_params (ad_id, name, value) VALUES (56, 'garage_spaces', '123');
INSERT INTO ad_params (ad_id, name, value) VALUES (56, 'communes', '302');
INSERT INTO ad_params (ad_id, name, value) VALUES (56, 'prev_currency', 'peso');
INSERT INTO ad_params (ad_id, name, value) VALUES (56, 'country', 'UNK');
INSERT INTO ad_params (ad_id, name, value) VALUES (57, 'size', '213');
INSERT INTO ad_params (ad_id, name, value) VALUES (57, 'garage_spaces', '123');
INSERT INTO ad_params (ad_id, name, value) VALUES (57, 'communes', '297');
INSERT INTO ad_params (ad_id, name, value) VALUES (57, 'prev_currency', 'peso');
INSERT INTO ad_params (ad_id, name, value) VALUES (57, 'country', 'UNK');
INSERT INTO ad_params (ad_id, name, value) VALUES (58, 'size', '12123');
INSERT INTO ad_params (ad_id, name, value) VALUES (58, 'condominio', '123123');
INSERT INTO ad_params (ad_id, name, value) VALUES (58, 'communes', '295');
INSERT INTO ad_params (ad_id, name, value) VALUES (58, 'prev_currency', 'peso');
INSERT INTO ad_params (ad_id, name, value) VALUES (58, 'country', 'UNK');
INSERT INTO ad_params (ad_id, name, value) VALUES (59, 'regdate', '2012');
INSERT INTO ad_params (ad_id, name, value) VALUES (59, 'mileage', '12312');
INSERT INTO ad_params (ad_id, name, value) VALUES (59, 'gearbox', '2');
INSERT INTO ad_params (ad_id, name, value) VALUES (59, 'fuel', '1');
INSERT INTO ad_params (ad_id, name, value) VALUES (59, 'cartype', '1');
INSERT INTO ad_params (ad_id, name, value) VALUES (59, 'prev_currency', 'peso');
INSERT INTO ad_params (ad_id, name, value) VALUES (59, 'brand', '2');
INSERT INTO ad_params (ad_id, name, value) VALUES (59, 'model', '2');
INSERT INTO ad_params (ad_id, name, value) VALUES (59, 'version', '1');
INSERT INTO ad_params (ad_id, name, value) VALUES (59, 'country', 'UNK');
INSERT INTO ad_params (ad_id, name, value) VALUES (60, 'regdate', '2012');
INSERT INTO ad_params (ad_id, name, value) VALUES (60, 'mileage', '234');
INSERT INTO ad_params (ad_id, name, value) VALUES (60, 'cubiccms', '2');
INSERT INTO ad_params (ad_id, name, value) VALUES (60, 'prev_currency', 'peso');
INSERT INTO ad_params (ad_id, name, value) VALUES (60, 'country', 'UNK');
INSERT INTO ad_params (ad_id, name, value) VALUES (61, 'regdate', '2010');
INSERT INTO ad_params (ad_id, name, value) VALUES (61, 'mileage', '534534');
INSERT INTO ad_params (ad_id, name, value) VALUES (61, 'gearbox', '2');
INSERT INTO ad_params (ad_id, name, value) VALUES (61, 'fuel', '4');
INSERT INTO ad_params (ad_id, name, value) VALUES (61, 'prev_currency', 'peso');
INSERT INTO ad_params (ad_id, name, value) VALUES (61, 'country', 'UNK');
INSERT INTO ad_params (ad_id, name, value) VALUES (62, 'prev_currency', 'peso');
INSERT INTO ad_params (ad_id, name, value) VALUES (62, 'country', 'UNK');
INSERT INTO ad_params (ad_id, name, value) VALUES (63, 'prev_currency', 'peso');
INSERT INTO ad_params (ad_id, name, value) VALUES (63, 'country', 'UNK');
INSERT INTO ad_params (ad_id, name, value) VALUES (64, 'size', '100');
INSERT INTO ad_params (ad_id, name, value) VALUES (64, 'garage_spaces', '1');
INSERT INTO ad_params (ad_id, name, value) VALUES (64, 'condominio', '100000');
INSERT INTO ad_params (ad_id, name, value) VALUES (64, 'communes', '297');
INSERT INTO ad_params (ad_id, name, value) VALUES (64, 'prev_currency', 'peso');
INSERT INTO ad_params (ad_id, name, value) VALUES (64, 'country', 'UNK');
INSERT INTO ad_params (ad_id, name, value) VALUES (65, 'size', '123');
INSERT INTO ad_params (ad_id, name, value) VALUES (65, 'condominio', '123');
INSERT INTO ad_params (ad_id, name, value) VALUES (65, 'communes', '298');
INSERT INTO ad_params (ad_id, name, value) VALUES (65, 'prev_currency', 'peso');
INSERT INTO ad_params (ad_id, name, value) VALUES (65, 'country', 'UNK');
INSERT INTO ad_params (ad_id, name, value) VALUES (66, 'size', '987');
INSERT INTO ad_params (ad_id, name, value) VALUES (66, 'garage_spaces', '654');
INSERT INTO ad_params (ad_id, name, value) VALUES (66, 'communes', '301');
INSERT INTO ad_params (ad_id, name, value) VALUES (66, 'prev_currency', 'peso');
INSERT INTO ad_params (ad_id, name, value) VALUES (66, 'country', 'UNK');
INSERT INTO ad_params (ad_id, name, value) VALUES (67, 'communes', '297');
INSERT INTO ad_params (ad_id, name, value) VALUES (67, 'prev_currency', 'peso');
INSERT INTO ad_params (ad_id, name, value) VALUES (67, 'country', 'UNK');
INSERT INTO ad_params (ad_id, name, value) VALUES (68, 'size', '1000');
INSERT INTO ad_params (ad_id, name, value) VALUES (68, 'garage_spaces', '3');
INSERT INTO ad_params (ad_id, name, value) VALUES (68, 'communes', '299');
INSERT INTO ad_params (ad_id, name, value) VALUES (68, 'prev_currency', 'peso');
INSERT INTO ad_params (ad_id, name, value) VALUES (68, 'country', 'UNK');
INSERT INTO ad_params (ad_id, name, value) VALUES (69, 'rooms', '2');
INSERT INTO ad_params (ad_id, name, value) VALUES (69, 'communes', '297');
INSERT INTO ad_params (ad_id, name, value) VALUES (69, 'prev_currency', 'peso');
INSERT INTO ad_params (ad_id, name, value) VALUES (69, 'country', 'UNK');
INSERT INTO ad_params (ad_id, name, value) VALUES (70, 'rooms', '3');
INSERT INTO ad_params (ad_id, name, value) VALUES (70, 'communes', '299');
INSERT INTO ad_params (ad_id, name, value) VALUES (70, 'prev_currency', 'peso');
INSERT INTO ad_params (ad_id, name, value) VALUES (70, 'country', 'UNK');
INSERT INTO ad_params (ad_id, name, value) VALUES (71, 'rooms', '2');
INSERT INTO ad_params (ad_id, name, value) VALUES (71, 'size', '87');
INSERT INTO ad_params (ad_id, name, value) VALUES (71, 'garage_spaces', '876');
INSERT INTO ad_params (ad_id, name, value) VALUES (71, 'condominio', '678');
INSERT INTO ad_params (ad_id, name, value) VALUES (71, 'communes', '299');
INSERT INTO ad_params (ad_id, name, value) VALUES (71, 'prev_currency', 'peso');
INSERT INTO ad_params (ad_id, name, value) VALUES (71, 'country', 'UNK');
INSERT INTO ad_params (ad_id, name, value) VALUES (72, 'communes', '298');
INSERT INTO ad_params (ad_id, name, value) VALUES (72, 'prev_currency', 'peso');
INSERT INTO ad_params (ad_id, name, value) VALUES (72, 'country', 'UNK');
INSERT INTO ad_params (ad_id, name, value) VALUES (73, 'rooms', '4');
INSERT INTO ad_params (ad_id, name, value) VALUES (73, 'communes', '297');
INSERT INTO ad_params (ad_id, name, value) VALUES (73, 'prev_currency', 'peso');
INSERT INTO ad_params (ad_id, name, value) VALUES (73, 'country', 'UNK');
INSERT INTO ad_params (ad_id, name, value) VALUES (74, 'rooms', '1');
INSERT INTO ad_params (ad_id, name, value) VALUES (74, 'size', '9887');
INSERT INTO ad_params (ad_id, name, value) VALUES (74, 'garage_spaces', '687');
INSERT INTO ad_params (ad_id, name, value) VALUES (74, 'condominio', '786');
INSERT INTO ad_params (ad_id, name, value) VALUES (74, 'communes', '299');
INSERT INTO ad_params (ad_id, name, value) VALUES (74, 'prev_currency', 'peso');
INSERT INTO ad_params (ad_id, name, value) VALUES (74, 'country', 'UNK');
INSERT INTO ad_params (ad_id, name, value) VALUES (75, 'regdate', '2012');
INSERT INTO ad_params (ad_id, name, value) VALUES (75, 'mileage', '2342');
INSERT INTO ad_params (ad_id, name, value) VALUES (75, 'gearbox', '1');
INSERT INTO ad_params (ad_id, name, value) VALUES (75, 'fuel', '1');
INSERT INTO ad_params (ad_id, name, value) VALUES (75, 'cartype', '3');
INSERT INTO ad_params (ad_id, name, value) VALUES (75, 'prev_currency', 'peso');
INSERT INTO ad_params (ad_id, name, value) VALUES (75, 'brand', '5');
INSERT INTO ad_params (ad_id, name, value) VALUES (75, 'model', '2');
INSERT INTO ad_params (ad_id, name, value) VALUES (75, 'version', '1');
INSERT INTO ad_params (ad_id, name, value) VALUES (75, 'country', 'UNK');


--
-- Data for Name: ad_queues; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Data for Name: admin_privs; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO admin_privs (admin_id, priv_name) VALUES (2, 'adqueue');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (3, 'adqueue');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (3, 'admin');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (3, 'adminad.edit_ad');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (3, 'adminad.clear_ad');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (3, 'adminad.bump');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (3, 'credit');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (3, 'config');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (3, 'adqueue.admin_queue');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (3, 'adqueue.admin_settings');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (3, 'adqueue.show_num_ads=2');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (3, 'search');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (3, 'search.search_ads');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (3, 'notice_abuse');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (4, 'admin');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (5, 'admin');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (5, 'adqueue');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (5, 'adminad');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (5, 'adminad.clear_ad');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (5, 'adminad.edit_ad');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (5, 'adminad.bump');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (5, 'credit');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (5, 'config');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (5, 'adqueue.admin_queue');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (5, 'adqueue.admin_settings');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (5, 'adqueue.show_num_ads=2');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (6, 'admin');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (6, 'adqueue');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (6, 'adminad.edit_ad');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (50, 'admin');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (50, 'adqueue');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (51, 'admin');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (51, 'adqueue');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (52, 'admin');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (52, 'adqueue');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (53, 'admin');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (53, 'adqueue');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (54, 'admin');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (54, 'adqueue');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (55, 'admin');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (55, 'adqueue');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'ais');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'notice_abuse');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'adqueue');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'adminad');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'Adminuf');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'admin');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'api');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'config');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'filter');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'landing_page');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'mama');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'popular_ads');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'scarface');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'search');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'Websql');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'adqueue.accepted');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'adqueue.admin_settings');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'adqueue.show_num_ads=2');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'adqueue.approve_refused');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'adqueue.video');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'adqueue.mama_queues');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'adqueue.mamawork_queues');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'adqueue.refusals');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'adqueue.admin_queue');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'adminad.bump');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'adminad.clear_ad');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'adminad.edit_ad');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'filter.lists');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'filter.rules');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'filter.spamfilter');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'mama.backup_lists');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'mama.show_lists');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'scarface.warning');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'search.abuse_report');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'search.uid_emails');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'search.maillog');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'search.mass_delete');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'search.paylog');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'search.reviewers');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'search.search_ads');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (9, 'search.undo_delete');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (10, 'ais');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (10, 'notice_abuse');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (10, 'adqueue');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (10, 'adminad');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (10, 'Adminuf');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (10, 'admin');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (10, 'api');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (10, 'config');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (10, 'filter');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (10, 'landing_page');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (10, 'mama');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (10, 'popular_ads');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (10, 'scarface');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (10, 'search');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (10, 'Websql');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (10, 'adqueue.accepted');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (10, 'adqueue.admin_settings');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (10, 'adqueue.show_num_ads=10');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (10, 'adqueue.approve_refused');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (10, 'adqueue.video');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (10, 'adqueue.mama_queues');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (10, 'adqueue.mamawork_queues');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (10, 'adqueue.refusals');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (10, 'adqueue.admin_queue');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (10, 'adqueue.whitelist');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (10, 'adminad.bump');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (10, 'adminad.clear_ad');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (10, 'adminad.edit_ad');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (10, 'filter.lists');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (10, 'filter.rules');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (10, 'filter.spamfilter');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (10, 'mama.backup_lists');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (10, 'mama.show_lists');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (10, 'scarface.warning');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (10, 'search.abuse_report');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (10, 'search.uid_emails');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (10, 'search.maillog');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (10, 'search.mass_delete');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (10, 'search.paylog');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (10, 'search.reviewers');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (10, 'search.search_ads');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (10, 'search.account');
INSERT INTO admin_privs (admin_id, priv_name) VALUES (10, 'search.undo_delete');
INSERT INTO admin_privs VALUES (23, 'ais');
INSERT INTO admin_privs VALUES (23, 'notice_abuse');
INSERT INTO admin_privs VALUES (23, 'adqueue');
INSERT INTO admin_privs VALUES (23, 'adminad');
INSERT INTO admin_privs VALUES (23, 'Adminuf');
INSERT INTO admin_privs VALUES (23, 'admin');
INSERT INTO admin_privs VALUES (23, 'api');
INSERT INTO admin_privs VALUES (23, 'config');
INSERT INTO admin_privs VALUES (23, 'filter');
INSERT INTO admin_privs VALUES (23, 'landing_page');
INSERT INTO admin_privs VALUES (23, 'mama');
INSERT INTO admin_privs VALUES (23, 'popular_ads');
INSERT INTO admin_privs VALUES (23, 'scarface');
INSERT INTO admin_privs VALUES (23, 'search');
INSERT INTO admin_privs VALUES (23, 'Websql');
INSERT INTO admin_privs VALUES (23, 'adqueue.accepted');
INSERT INTO admin_privs VALUES (23, 'adqueue.admin_settings');
INSERT INTO admin_privs VALUES (23, 'adqueue.show_num_ads=10');
INSERT INTO admin_privs VALUES (23, 'adqueue.approve_refused');
INSERT INTO admin_privs VALUES (23, 'adqueue.video');
INSERT INTO admin_privs VALUES (23, 'adqueue.mama_queues');
INSERT INTO admin_privs VALUES (23, 'adqueue.mamawork_queues');
INSERT INTO admin_privs VALUES (23, 'adqueue.refusals');
INSERT INTO admin_privs VALUES (23, 'adqueue.admin_queue');
INSERT INTO admin_privs VALUES (23, 'adqueue.whitelist');
INSERT INTO admin_privs VALUES (23, 'adminad.bump');
INSERT INTO admin_privs VALUES (23, 'adminad.clear_ad');
INSERT INTO admin_privs VALUES (23, 'adminad.edit_ad');
INSERT INTO admin_privs VALUES (23, 'filter.lists');
INSERT INTO admin_privs VALUES (23, 'filter.rules');
INSERT INTO admin_privs VALUES (23, 'filter.spamfilter');
INSERT INTO admin_privs VALUES (23, 'mama.backup_lists');
INSERT INTO admin_privs VALUES (23, 'mama.show_lists');
INSERT INTO admin_privs VALUES (23, 'scarface.warning');
INSERT INTO admin_privs VALUES (23, 'search.abuse_report');
INSERT INTO admin_privs VALUES (23, 'search.uid_emails');
INSERT INTO admin_privs VALUES (23, 'search.maillog');
INSERT INTO admin_privs VALUES (23, 'search.mass_delete');
INSERT INTO admin_privs VALUES (23, 'search.paylog');
INSERT INTO admin_privs VALUES (23, 'search.reviewers');
INSERT INTO admin_privs VALUES (23, 'search.search_ads');
INSERT INTO admin_privs VALUES (23, 'search.undo_delete');


--
-- Name: admins_admin_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('admins_admin_id_seq', 22, true);


--
-- Name: ads_ad_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('ads_ad_id_seq', 75, true);


--
-- Name: adwatch_code_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('adwatch_code_seq', 1, false);


--
-- Data for Name: archive_cache; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Data for Name: bid_ads; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Name: bid_ads_bid_ad_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('bid_ads_bid_ad_id_seq', 1, false);


--
-- Data for Name: bid_bids; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Name: bid_bids_bid_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('bid_bids_bid_id_seq', 1, false);


--
-- Data for Name: bid_media; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Data for Name: block_lists; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO block_lists (list_id, list_name, base_list, list_type, ignore) VALUES (1, 'E-post adresser - raderas', true, 'email', NULL);
INSERT INTO block_lists (list_id, list_name, base_list, list_type, ignore) VALUES (2, 'E-post adresser - spamfiltret', true, 'email', NULL);
INSERT INTO block_lists (list_id, list_name, base_list, list_type, ignore) VALUES (3, 'IP-adresser som - raderas', true, 'ip', NULL);
INSERT INTO block_lists (list_id, list_name, base_list, list_type, ignore) VALUES (4, 'IP-adresser - spamfiltret', true, 'ip', NULL);
INSERT INTO block_lists (list_id, list_name, base_list, list_type, ignore) VALUES (5, 'E-postadresser som inte f�r annonsera', false, 'email', NULL);
INSERT INTO block_lists (list_id, list_name, base_list, list_type, ignore) VALUES (6, 'Telefonnummer som inte f�r annonsera', false, 'general', '-+ ');
INSERT INTO block_lists (list_id, list_name, base_list, list_type, ignore) VALUES (7, 'Tipsmottagare', false, 'email', NULL);
INSERT INTO block_lists (list_id, list_name, base_list, list_type, ignore) VALUES (8, 'Synonymer f�r ordet Annonsera', false, 'general', NULL);
INSERT INTO block_lists (list_id, list_name, base_list, list_type, ignore) VALUES (9, 'Synonymer f�r ordet Gratis', false, 'general', NULL);
INSERT INTO block_lists (list_id, list_name, base_list, list_type, ignore) VALUES (10, 'Sajter som kan spamma', false, 'general', NULL);
INSERT INTO block_lists (list_id, list_name, base_list, list_type, ignore) VALUES (11, 'Ord som anv�nds av sajter som spammar', false, 'general', NULL);
INSERT INTO block_lists (list_id, list_name, base_list, list_type, ignore) VALUES (12, 'Fula ord', false, 'general', NULL);
INSERT INTO block_lists (list_id, list_name, base_list, list_type, ignore) VALUES (13, 'F�rbjudna rubriker i L�gg in annons', false, 'general', NULL);
INSERT INTO block_lists (list_id, list_name, base_list, list_type, ignore) VALUES (14, 'Engelska fraser som anv�nds i mejlsvar', false, 'general', NULL);
INSERT INTO block_lists (list_id, list_name, base_list, list_type, ignore) VALUES (15, 'Byten.se', false, 'general', NULL);
INSERT INTO block_lists (list_id, list_name, base_list, list_type, ignore) VALUES (16, 'whitelist', false, 'email', NULL);
INSERT INTO block_lists (list_id, list_name, base_list, list_type, ignore) VALUES (17, 'were_whitelist', false, 'email', NULL);


--
-- Name: block_lists_list_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('block_lists_list_id_seq', 100, false);


--
-- Data for Name: block_rules; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO block_rules (rule_id, rule_name, action, application, combine_rule, count_yesterday, count_today, count_date, target) VALUES (1, 'Sajtspam', 'delete', 'adreply', 'and', 0, 0, '2015-02-20', NULL);
INSERT INTO block_rules (rule_id, rule_name, action, application, combine_rule, count_yesterday, count_today, count_date, target) VALUES (2, 'Annonsera gratis', 'delete', 'adreply', 'and', 0, 0, '2015-02-20', NULL);
INSERT INTO block_rules (rule_id, rule_name, action, application, combine_rule, count_yesterday, count_today, count_date, target) VALUES (3, 'Fula ord', 'stop', 'newad', 'and', 0, 0, '2015-02-20', NULL);
INSERT INTO block_rules (rule_id, rule_name, action, application, combine_rule, count_yesterday, count_today, count_date, target) VALUES (4, 'Fula ord', 'delete', 'adreply', 'and', 0, 0, '2015-02-20', NULL);
INSERT INTO block_rules (rule_id, rule_name, action, application, combine_rule, count_yesterday, count_today, count_date, target) VALUES (5, 'Blockerade epostadresser  - raderas', 'delete', 'adreply', 'and', 0, 0, '2015-02-20', NULL);
INSERT INTO block_rules (rule_id, rule_name, action, application, combine_rule, count_yesterday, count_today, count_date, target) VALUES (6, 'Blockerade epostadresser', 'stop', 'newad', 'and', 0, 0, '2015-02-20', NULL);
INSERT INTO block_rules (rule_id, rule_name, action, application, combine_rule, count_yesterday, count_today, count_date, target) VALUES (7, 'Blockerade ip-adresser - raderas', 'delete', 'adreply', 'and', 0, 0, '2015-02-20', NULL);
INSERT INTO block_rules (rule_id, rule_name, action, application, combine_rule, count_yesterday, count_today, count_date, target) VALUES (8, 'Blockerade telefonnummer', 'stop', 'newad', 'and', 0, 0, '2015-02-20', NULL);
INSERT INTO block_rules (rule_id, rule_name, action, application, combine_rule, count_yesterday, count_today, count_date, target) VALUES (9, 'Blockerade tipsmottagare', 'delete', 'sendtip', 'and', 0, 0, '2015-02-20', NULL);
INSERT INTO block_rules (rule_id, rule_name, action, application, combine_rule, count_yesterday, count_today, count_date, target) VALUES (10, 'Byten.se', 'delete', 'adreply', 'and', 0, 0, '2015-02-20', NULL);
INSERT INTO block_rules (rule_id, rule_name, action, application, combine_rule, count_yesterday, count_today, count_date, target) VALUES (11, 'Byten.se', 'delete', 'sendtip', 'and', 0, 0, '2015-02-20', NULL);
INSERT INTO block_rules (rule_id, rule_name, action, application, combine_rule, count_yesterday, count_today, count_date, target) VALUES (12, 'Blockerade ord i rubriken', 'stop', 'newad', 'and', 0, 0, '2015-02-20', NULL);
INSERT INTO block_rules (rule_id, rule_name, action, application, combine_rule, count_yesterday, count_today, count_date, target) VALUES (13, 'Blockerade engelska mail', 'spamfilter', 'adreply', 'and', 0, 0, '2015-02-20', NULL);
INSERT INTO block_rules (rule_id, rule_name, action, application, combine_rule, count_yesterday, count_today, count_date, target) VALUES (14, 'Blockerade epostadresser - spamfilter', 'spamfilter', 'adreply', 'and', 0, 0, '2015-02-20', NULL);
INSERT INTO block_rules (rule_id, rule_name, action, application, combine_rule, count_yesterday, count_today, count_date, target) VALUES (15, 'Blockerade ip-adresser - spamfilter', 'spamfilter', 'adreply', 'and', 0, 0, '2015-02-20', NULL);
INSERT INTO block_rules (rule_id, rule_name, action, application, combine_rule, count_yesterday, count_today, count_date, target) VALUES (16, 'Fula ord', 'delete', 'sendtip', 'or', 0, 0, '2015-02-20', NULL);
INSERT INTO block_rules (rule_id, rule_name, action, application, combine_rule, count_yesterday, count_today, count_date, target) VALUES (17, 'whitelist rule', 'move_to_queue', 'clear', 'and', 0, 0, '2015-02-20', 'whitelist');


--
-- Data for Name: block_rule_conditions; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO block_rule_conditions (condition_id, rule_id, list_id, whole_word, fields) VALUES (1, 1, 10, 0, '{subject,body}');
INSERT INTO block_rule_conditions (condition_id, rule_id, list_id, whole_word, fields) VALUES (2, 1, 11, 0, '{subject,body}');
INSERT INTO block_rule_conditions (condition_id, rule_id, list_id, whole_word, fields) VALUES (3, 2, 8, 0, '{subject,body}');
INSERT INTO block_rule_conditions (condition_id, rule_id, list_id, whole_word, fields) VALUES (4, 2, 9, 0, '{subject,body}');
INSERT INTO block_rule_conditions (condition_id, rule_id, list_id, whole_word, fields) VALUES (5, 3, 12, 0, '{subject,body}');
INSERT INTO block_rule_conditions (condition_id, rule_id, list_id, whole_word, fields) VALUES (6, 4, 12, 0, '{subject,body}');
INSERT INTO block_rule_conditions (condition_id, rule_id, list_id, whole_word, fields) VALUES (7, 5, 1, 1, '{email}');
INSERT INTO block_rule_conditions (condition_id, rule_id, list_id, whole_word, fields) VALUES (8, 6, 5, 1, '{email}');
INSERT INTO block_rule_conditions (condition_id, rule_id, list_id, whole_word, fields) VALUES (9, 7, 3, 1, '{remote_addr}');
INSERT INTO block_rule_conditions (condition_id, rule_id, list_id, whole_word, fields) VALUES (10, 8, 6, 1, '{phone}');
INSERT INTO block_rule_conditions (condition_id, rule_id, list_id, whole_word, fields) VALUES (11, 9, 7, 1, '{email}');
INSERT INTO block_rule_conditions (condition_id, rule_id, list_id, whole_word, fields) VALUES (12, 10, 15, 0, '{name,email,subject,body}');
INSERT INTO block_rule_conditions (condition_id, rule_id, list_id, whole_word, fields) VALUES (13, 11, 15, 0, '{name,email,subject,body}');
INSERT INTO block_rule_conditions (condition_id, rule_id, list_id, whole_word, fields) VALUES (14, 12, 13, 0, '{subject}');
INSERT INTO block_rule_conditions (condition_id, rule_id, list_id, whole_word, fields) VALUES (15, 13, 14, 0, '{body}');
INSERT INTO block_rule_conditions (condition_id, rule_id, list_id, whole_word, fields) VALUES (16, 14, 2, 1, '{subject}');
INSERT INTO block_rule_conditions (condition_id, rule_id, list_id, whole_word, fields) VALUES (17, 15, 4, 1, '{remote_addr}');
INSERT INTO block_rule_conditions (condition_id, rule_id, list_id, whole_word, fields) VALUES (18, 16, 12, 0, '{body}');
INSERT INTO block_rule_conditions (condition_id, rule_id, list_id, whole_word, fields) VALUES (19, 17, 16, 1, '{email}');


--
-- Name: block_rule_conditions_condition_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('block_rule_conditions_condition_id_seq', 18, true);


--
-- Name: block_rules_rule_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('block_rules_rule_id_seq', 100, false);


--
-- Data for Name: blocked_items; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO blocked_items (item_id, value, list_id, notice, token_id) VALUES (1, 'ful@fisk.se', 1, NULL, NULL);
INSERT INTO blocked_items (item_id, value, list_id, notice, token_id) VALUES (2, 'ful@fisk.se', 5, NULL, NULL);
INSERT INTO blocked_items (item_id, value, list_id, notice, token_id) VALUES (3, '0733555501', 6, NULL, NULL);
INSERT INTO blocked_items (item_id, value, list_id, notice, token_id) VALUES (4, 'ful@fisk.se', 7, NULL, NULL);
INSERT INTO blocked_items (item_id, value, list_id, notice, token_id) VALUES (5, 'fisk.se', 10, NULL, NULL);
INSERT INTO blocked_items (item_id, value, list_id, notice, token_id) VALUES (6, 'vatten', 11, NULL, NULL);
INSERT INTO blocked_items (item_id, value, list_id, notice, token_id) VALUES (7, 'fitta', 12, NULL, NULL);
INSERT INTO blocked_items (item_id, value, list_id, notice, token_id) VALUES (8, 'analakrobat', 12, NULL, NULL);
INSERT INTO blocked_items (item_id, value, list_id, notice, token_id) VALUES (9, 'buy', 13, NULL, NULL);
INSERT INTO blocked_items (item_id, value, list_id, notice, token_id) VALUES (10, 'sale', 13, NULL, NULL);
INSERT INTO blocked_items (item_id, value, list_id, notice, token_id) VALUES (11, 'salu', 13, NULL, NULL);
INSERT INTO blocked_items (item_id, value, list_id, notice, token_id) VALUES (12, 'free', 14, NULL, NULL);
INSERT INTO blocked_items (item_id, value, list_id, notice, token_id) VALUES (13, 'usuario07@schibsted.cl', 16, 'Email automatically put into the White List in Ad Insert.', NULL);


--
-- Name: blocked_items_item_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('blocked_items_item_id_seq', 13, true);


--
-- Data for Name: conf; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.controlpanel.modules.adqueue.settings.auto_abuse', '1', '2015-02-20 15:02:25.260309', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.accepted.1.name', 'Sin fotos', '2015-02-20 15:02:25.264865', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.accepted.1.text', 'Para mejorar las posibilidades de que tu aviso sea visto, te recomendamos que agregues im�genes del producto que ofreces o buscas.', '2015-02-20 15:02:25.265452', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.accepted.22.name', 'Foto principal movida', '2015-02-20 15:02:25.265889', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.accepted.22.text', 'Para mejorar las posibilidades de que tu aviso sea visto, hemos alterado el orden de las fotograf�as', '2015-02-20 15:02:25.266297', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.accepted.3.name', 'BORRAMOS TODAS LAS IM�GENES', '2015-02-20 15:02:25.266726', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.accepted.3.text', 'Hemos borrado las im�genes que subiste por no cumplir las reglas de Yapo.cl. Sube nuevas fotograf�as para tener m�s visitas y vender�s antes!', '2015-02-20 15:02:25.267098', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.accepted.4.name', 'BORRAMOS ALGUNAS DE LAS IM�GENES', '2015-02-20 15:02:25.267549', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.accepted.4.text', 'Hemos borrado algunas de las im�genes que subiste por no cumplir las reglas de Yapo.cl. Sube nuevas fotograf�as para tener m�s visitas y vender�s antes!', '2015-02-20 15:02:25.267982', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.accepted.6.name', 'BORRAMOS CORREOS', '2015-02-20 15:02:25.268391', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.accepted.6.text', 'Hemos borrado el email de la descripci�n de tu aviso por tu seguridad. Las personas interesadas pueden contactarte a trav�s del formulario y evitar�s recibir spam o emails no deseados. Recuerda que Yapo.cl es 100% gratis.', '2015-02-20 15:02:25.26883', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.accepted.8.name', 'MODIFICAMOS T�TULO', '2015-02-20 15:02:25.269258', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.accepted.8.text', 'Hemos modificado el t�tulo de tu aviso para que describa mejor lo que ofreces. Te sugerimos editar tu aviso para hacerlo a�n mejor y vender antes.', '2015-02-20 15:02:25.269722', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.accepted.9.name', 'MODIFICAMOS DESCRIPCI�N', '2015-02-20 15:02:25.270094', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.accepted.9.text', 'Hemos modificado la descripci�n de tu aviso para explicar mejor lo que ofreces. Te sugerimos editar tu aviso para hacerlo a�n mejor y vender antes.', '2015-02-20 15:02:25.270486', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.accepted.10.name', 'PALABRAS DE B�SQUEDA', '2015-02-20 15:02:25.270938', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.accepted.10.text', 'Hemos modificado la descripci�n de tu aviso para ajustarla a las reglas de Yapo.cl. Recuerda que tu aviso debe hacer referencia a un solo producto o servicio.', '2015-02-20 15:02:25.271362', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.accepted.15.name', 'MEJORA LA DESCRIPCI�N', '2015-02-20 15:02:25.271941', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.accepted.15.text', 'Hemos revisado la descripci�n de tu aviso y te recomendamos que detalles m�s el producto o servicio que est�s ofreciendo, tu aviso ser� m�s interesante y vender�s antes', '2015-02-20 15:02:25.272601', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.accepted.16.name', 'CATEGORIA FUE CAMBIADA', '2015-02-20 15:02:25.273052', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.accepted.16.text', 'Hemos cambiado la categor�a que elegiste por la correcta. Ahora tu aviso ser� encontrado m�s f�cil y vender�s antes. Recuerda que Yapo.cl es 100% gratis.', '2015-02-20 15:02:25.273686', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.accepted.12.name', 'COMPLETAMOS EL CAMPO PRECIO', '2015-02-20 15:02:25.274161', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.accepted.12.text', 'Hemos completado el precio de tu producto o servicio. Puedes editarlo de nuevo si crees que nos hemos equivocado.', '2015-02-20 15:02:25.274637', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.accepted.13.name', 'BORRAMOS ENLACES', '2015-02-20 15:02:25.275036', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.accepted.13.text', 'Hemos borrado enlaces de la descripci�n de tu aviso ya que no est� permitido en las reglas de nuestra web. Recuerda que no hay l�mite de datos para ingresar y que Yapo.cl es 100% gratis.', '2015-02-20 15:02:25.27552', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.accepted.17.name', 'TIPO DE VENDEDOR DE PERSONA A EMPRESA', '2015-02-20 15:02:25.276016', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.accepted.17.text', 'Hemos cambiado el tipo de vendedor de persona a empresa porque consideramos que te dedicas profesionalmente al producto/servicio que ofreces. Recuerda que Yapo.cl es 100% gratis tambi�n para empresas.', '2015-02-20 15:02:25.27644', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.accepted.18.name', 'TIPO DE VENDEDOR DE EMPRESA A PERSONA', '2015-02-20 15:02:25.276892', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.accepted.18.text', 'Hemos cambiado el tipo de vendedor de empresa a persona porque consideramos que fue mal seleccionado. Recuerda que Yapo.cl es 100% gratis.', '2015-02-20 15:02:25.277309', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.accepted.19.name', 'MEJORA EL T�TULO', '2015-02-20 15:02:25.277749', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.accepted.19.text', 'Hemos revisado el t�tulo de tu aviso y te recomendamos que detalles m�s el producto o servicio que est�s ofreciendo, tu aviso ser� m�s interesante y vender�s antes', '2015-02-20 15:02:25.278132', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.accepted.20.name', 'INGRESA COMUNA (De NO UF a UF)', '2015-02-20 15:02:25.278597', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.accepted.20.text', 'Hemos cambiado la categor�a que elegiste por una de inmobiliaria. Para completar la publicaci�n debes editarlo e ingresar la comuna, de esta forma tu aviso ser� encontrado m�s f�cil y vender�s antes. Recuerda que Yapo.cl es 100% gratis.', '2015-02-20 15:02:25.278995', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.accepted.21.name', 'VEHICULOS: AGREGA MODELO Y VERSION', '2015-02-20 15:02:25.279409', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.accepted.21.text', 'Hemos revisado tu aviso y te recomendamos que detalles la marca y modelo del veh�culo que est�s ofreciendo, tu aviso ser� encontrado m�s f�cil y vender�s antes. Recuerda que Yapo.cl es 100% gratis.', '2015-02-20 15:02:25.279831', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.accepted.5.name', 'BORRAMOS LINKS Y CORREOS', '2015-02-20 15:02:25.280309', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.accepted.5.text', 'Por tu seguridad, hemos borrado emails y enlaces de la descripci�n de tu aviso. Las personas interesadas pueden contactarte a trav�s del formulario "Contacta con el anunciante" el cual se encuentra a la derecha de cada uno de los avisos. As� evitar�s recibir spam o emails no deseados. Adem�s, puedes incluir tantos detalles como quieras en tu aviso. Recuerda que Yapo.cl es 100% gratis.', '2015-02-20 15:02:25.28076', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.accepted.11.name', 'BORRAMOS CAMPO PRECIO', '2015-02-20 15:02:25.281155', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.accepted.11.text', 'Hemos borrado el campo precio porque consideramos que es err�neo. Por favor verifica que tu precio sea correcto. Recuerda que debes insertar el precio por el total de productos � servicios ofrecidos.', '2015-02-20 15:02:25.281578', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.accepted.2.name', 'SIN IM�GENES', '2015-02-20 15:02:25.281962', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.accepted.2.text', '�Sab�as que un aviso con im�genes tiene m�s visitas y es mucho m�s interesante? �Incluye im�genes y vender�s antes!', '2015-02-20 15:02:25.282371', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.accepted_order.0', '3', '2015-02-20 15:02:25.282859', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.accepted_order.1', '2', '2015-02-20 15:02:25.28331', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.accepted_order.2', '4', '2015-02-20 15:02:25.283837', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.accepted_order.3', '5', '2015-02-20 15:02:25.284333', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.accepted_order.4', '13', '2015-02-20 15:02:25.284787', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.accepted_order.5', '6', '2015-02-20 15:02:25.28518', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.accepted_order.6', '8', '2015-02-20 15:02:25.285685', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.accepted_order.7', '9', '2015-02-20 15:02:25.286065', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.accepted_order.8', '10', '2015-02-20 15:02:25.286517', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.accepted_order.9', '11', '2015-02-20 15:02:25.28699', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.accepted_order.10', '12', '2015-02-20 15:02:25.288004', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.accepted_order.11', '19', '2015-02-20 15:02:25.288599', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.accepted_order.12', '15', '2015-02-20 15:02:25.289051', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.accepted_order.13', '21', '2015-02-20 15:02:25.289459', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.accepted_order.14', '16', '2015-02-20 15:02:25.289951', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.accepted_order.15', '20', '2015-02-20 15:02:25.290445', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.accepted_order.16', '17', '2015-02-20 15:02:25.290931', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.accepted_order.17', '18', '2015-02-20 15:02:25.291312', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.accepted_order.18', '1', '2015-02-20 15:02:25.291756', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.accepted_order.19', '22', '2015-02-20 15:02:25.292136', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal.0.name', 'T�tulo y Descripci�n de Otros', '2015-02-20 15:02:25.29262', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal.1.name', 'T�tulo y Descripci�n del Veh�culo', '2015-02-20 15:02:25.293172', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal.2.name', 'T�tulo y Descripci�n de los Bienes Ra�ces', '2015-02-20 15:02:25.293631', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal.3.name', 'Restricciones para los Animales Dom�sticos', '2015-02-20 15:02:25.293997', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal.4.name', 'Links para otros Sitios', '2015-02-20 15:02:25.294516', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal.5.name', 'Aviso Empresa', '2015-02-20 15:02:25.294891', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal.6.name', 'Contacto', '2015-02-20 15:02:25.295308', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal.7.name', 'Varios elementos', '2015-02-20 15:02:25.295817', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal.8.name', 'Varios elementos - Empleo', '2015-02-20 15:02:25.29619', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal.9.name', 'Avisos personales', '2015-02-20 15:02:25.296624', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal.10.name', 'Varios elementos Veh�culos - Propiedad', '2015-02-20 15:02:25.296997', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal.11.name', 'Aviso doble', '2015-02-20 15:02:25.297365', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal.12.name', 'Aviso caducado', '2015-02-20 15:02:25.297825', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal.13.name', 'IP extranjero', '2015-02-20 15:02:25.2982', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal.14.name', 'Ilegal', '2015-02-20 15:02:25.298725', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal.15.name', 'Contenido de im�genes', '2015-02-20 15:02:25.299117', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal.16.name', 'Error en la imagen', '2015-02-20 15:02:25.299535', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal.17.name', 'Idioma', '2015-02-20 15:02:25.29991', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal.18.name', 'Link', '2015-02-20 15:02:25.300645', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal.19.name', 'Imagen obscena', '2015-02-20 15:02:25.301043', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal.20.name', 'Ofensivo', '2015-02-20 15:02:25.301731', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal.21.name', 'Pirater�a', '2015-02-20 15:02:25.302167', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal.22.name', 'Elementos', '2015-02-20 15:02:25.302611', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal.23.name', 'Marketing', '2015-02-20 15:02:25.302985', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal.24.name', 'Palabras de B�squeda', '2015-02-20 15:02:25.303349', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal.25.name', 'Contrase�a', '2015-02-20 15:02:25.303823', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal.26.name', 'Virus', '2015-02-20 15:02:25.304231', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal.27.name', 'Estado de origen', '2015-02-20 15:02:25.304648', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal.29.name', 'No es realista', '2015-02-20 15:02:25.305069', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal.30.name', 'Categor�a equivocada', '2015-02-20 15:02:25.305509', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal.31.name', 'Spam', '2015-02-20 15:02:25.306034', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal.32.name', 'Propiedad intelectual', '2015-02-20 15:02:25.306447', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal.33.name', 'Privacidad', '2015-02-20 15:02:25.306921', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal.34.name', 'Intercambios', '2015-02-20 15:02:25.307331', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal.35.name', 'Menores de edad', '2015-02-20 15:02:25.307819', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal.0.text', 'Por favor, comprueba que el nombre incluido / modelo / t�tulo / marca del art�culo que deseas vender, est� de acuerdo con  el t�tulo y la descripci�n del producto. El t�tulo del aviso debe describir el producto o servicio anunciado, no se permiten incluir nombres de empresas o URL (direcci�n Web). No es permitido el uso de caracteres especiales o caracteres en may�sculas en el t�tulo.', '2015-02-20 15:02:25.308199', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal.1.text', 'Por favor,  verifica el nombre y modelo (ejemplo: Honda Civic 1.6]) del veh�culo que deseas vender. En la descripci�n, incluye  informaci�n espec�fica.', '2015-02-20 15:02:25.308677', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal.2.text', 'Comprueba  que incluya el t�tulo. El t�tulo del aviso debe describir el producto o servicio anunciado, no se le permite incluir nombres de empresas o  URL (direcci�n Web). No es permitido el uso de caracteres especiales o caracteres en may�sculas en el t�tulo.', '2015-02-20 15:02:25.309066', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal.3.text', 'No permitimos avisos de animales prohibidos por las leyes chilenas de protecci�n animal.', '2015-02-20 15:02:25.309448', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal.4.text', 'No permitimos la inclusi�n de un enlace dirigido a otra p�gina web de e-mail o n�meros de tel�fono en el texto del aviso. No est� permitido hablar de otros sitios web o las subastas de avisos clasificados. Estos v�nculos y referencias deben ser eliminados antes de volver a enviar la notificaci�n.', '2015-02-20 15:02:25.309895', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal.5.text', 'Has introducido un aviso en forma individual o categor�as que no permiten avisos de empresas. Las empresas deben insertar avisos y hacer una lista como los avisos de empresas. Los avisos de empresas no est�n permitidos en las siguientes categor�as: Video Juegos, telefon�a, ropa y prendas de vestir, bolsos, mochilas y accesorios, joyas, relojes y joyas, etc.', '2015-02-20 15:02:25.310294', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal.6.text', 'No hemos podido verificar tu informaci�n de contacto. Por favor, verifica que toda la informaci�n introducida es correcta. Despu�s de la revisi�n, puedes volver a enviar tu aviso.', '2015-02-20 15:02:25.310773', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal.7.text', 'Hay muchos elementos en el mismo aviso. Por favor, escribe  cada elemento de los avisos por separado. No est� permitido introducir m�s de un veh�culo o propiedad en el mismo aviso, excepto si la transacci�n es un intercambio (por ejemplo, 2 por 1).                             ', '2015-02-20 15:02:25.31116', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal.8.text', 'Hay muchas ofertas de trabajo en el mismo aviso. Escribe una oferta por aviso.  No puedes publicar m�s de una propiedad en el mismo aviso.', '2015-02-20 15:02:25.311717', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal.9.text', 'Has introducido un aviso de una empresa. Los avisos deben ser personales.', '2015-02-20 15:02:25.312116', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal.10.text', 'Hay muchos elementos en el mismo aviso. Escribe cada elemento de la lista por separado, para que tu aviso sea m�s relevante para los compradores. No est� permitido introducir m�s de un veh�culo o propiedad en el mismo aviso, a menos que la transacci�n sea  un intercambio necesario (por  ejemplo, 2 por 1).', '2015-02-20 15:02:25.312526', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal.11.text', 'Ya existe otro aviso como el que has introducido. No puedes copiar el texto de los avisos de otros anunciantes, ya que est�n bajo la ley de derechos de autor.. Tampoco est� permitido publicar varios avisos con el mismo producto  o servicio. El aviso anterior debe ser borrado antes de publicar un nuevo  aviso. Tampoco se permite hacer publicidad del mismo art�culo, o servicio en  las diferentes categor�as de avisos en las diferentes regiones.', '2015-02-20 15:02:25.31302', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal.12.text', 'El  producto  ya no est� disponible/ha expirado/fue vendido.', '2015-02-20 15:02:25.31341', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal.13.text', 'Nuestro sitio ofrece clasificados s�lo en Chile. Los productos o servicios s�lo se  encuentran en  Chile y el aviso ser� colocado en la zona donde se encuentra. No se aceptan avisos de fuera del pa�s o en otro idioma que no sea el espa�ol.', '2015-02-20 15:02:25.313867', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal.14.text', 'Tu aviso contiene productos ilegales y/o prohibidos  para la  venta en nuestro sitio. No est� permito hacer publicidad de productos falsificados o falsos, como los productos de marca, CD / VCD / DVD, software / consola de juegos. Para que el aviso sea aceptado y para que este requisitto se aplique, el anunciante debe garantizar que los productos son originales. Los avisos que incluyen los productos para su reventa a la luz de la legislaci�n chilena no pueden ser publicados. Tenemos restricciones sobre el aviso de ciertos bienes y servicios. Lee nuestros T�rminos. Los servicios ofrecidos o solicitados deben cumplir con las leyes chilenas y reglamentarias aplicables a cada profesi�n.', '2015-02-20 15:02:25.314278', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal_order.32', '26', '2015-02-20 15:02:25.33755', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal_order.33', '25', '2015-02-20 15:02:25.337986', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal_order.35', '17', '2015-02-20 15:02:25.338392', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal.15.text', 'Las im�genes deben concordar con los avisos y deben ser relevantes para el art�culo o servicio anunciado. Las im�genes deben representar el art�culo anunciado. No puedes utilizar las im�genes para art�culos del cat�logo de segunda mano, o utilizar logotipos e im�genes de la empresa, excepto en los "Servicios" y "Empleo". No se permite el uso de im�genes de otros anunciantes, sin su consentimiento previo, ni marcas de agua o logos de sitios de la competencia. Las im�genes est�n protegidas por la legislaci�n sobre derechos de autor. No se permite el uso de una imagen en m�s de un aviso.', '2015-02-20 15:02:25.31488', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal.16.text', 'Una o m�s im�genes contienen errores y no se muestran correctamente. Por favor, aseg�rate que el formato de las  im�genes  es  JPG, GIF o BMP. ', '2015-02-20 15:02:25.315518', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal.17.text', 'Su aviso no est� en espa�ol', '2015-02-20 15:02:25.315962', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal.18.text', 'Est�s utilizando enlaces que no son relevantes para el aviso y/o que no funcionan', '2015-02-20 15:02:25.316354', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal.19.text', 'No se permite  publicar im�genes obscenas que muestren a la gente desnuda, en ropa interior o traje de ba�o.', '2015-02-20 15:02:25.316793', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal.20.text', 'Tu aviso puede ser ofensivo para los grupos �tnicos / religiosos, o puede ser considerado  racista, xen�fobo o terrorista, ya que atenta contra el g�nero humano. No se permiten avisos que violen normas constitucionales y que incorporen  contenidos, mensajes o productos de naturaleza violenta o degradantes.', '2015-02-20 15:02:25.317176', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal.21.text', 'No  est� permitido hacer publicidad de productos falsificados o falsos, como los productos de marca, CD / VCD / DVD, software / consola de juegos. Para que el aviso sea aceptado, el anunciante debe garantizar que los productos son originales. Los avisos que incluyen los productos para su reventa a la luz de la legislaci�n chilena, no pueden ser publicados.', '2015-02-20 15:02:25.317614', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal.22.text', 'S�lo se  permite hacer publicidad de ventas, arriendos, empleo y servicios.', '2015-02-20 15:02:25.318031', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal.23.text', 'El aviso contiene productos o servicios que no est�n permitidos en nuestro sitio. Para m�s informaci�n sobre estos productos, visite la p�gina de las Reglas. Si usted tiene alguna pregunta p�ngase en contacto con Atenci�n al Cliente.', '2015-02-20 15:02:25.318406', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal.24.text', 'No est�  permitido el uso de avisos  que tengan como �nico prop�sito el  marketing,  lo que no implica necesariamente el suministro de bienes, trabajos o servicios.', '2015-02-20 15:02:25.31886', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal.25.text', 'No est�  permitido el uso de avisos  que tengan como �nico prop�sito el  marketing,  lo que no implica necesariamente el suministro de bienes, trabajos o servicios.', '2015-02-20 15:02:25.319241', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal.26.text', 'No se pueden  introducir o difundir en la red,  programas de datos (virus y software maliciosos) que puedan  causar da�os al proveedor de acceso, a sistemas inform�ticos  de nuestros usuarios del sitio o de terceros que utilicen la misma red.', '2015-02-20 15:02:25.319663', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal.27.text', 'No  se ha declarado la autenticidad de tu producto. Para que el aviso sea  aceptado, el anunciante debe garantizar que los productos son originales.', '2015-02-20 15:02:25.320078', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal.29.text', 'No se permite la publicaci�n de avisos que no posean ofertas cre�bles y realistas. No se permite que los avisos contengan cualquier informaci�n con contenidos falsos, ambiguos o inexactos, con el fin de inducir al error a potenciales receptores de dicha informaci�n.', '2015-02-20 15:02:25.320461', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal.30.text', 'El aviso se publicar� en la categor�a que mejor describa el art�culo o servicio. Nos reservamos el derecho, si es necesario, a mover el aviso a la categor�a m�s apropiada. Bienes y servicios que no entren en la misma categor�a ser�n publicados en diferentes avisos. Para la venta se publicar� en "Se vende" y los avisos que demanden  un producto se publicar�n en "Se compra". En algunas categor�as los avisos podr�an incluir las opciones de "Arriendo" y "Se busca  arriendo". En otras categor�as, si es necesario, los avisos de "Se arrienda"  se publicar�n en "Venta" y los avisos "Quiero  arrendar", se publicar�n en "Se compra".', '2015-02-20 15:02:25.320923', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal.31.text', 'No est� permitido enviar publicidad no solicitada o autorizada, material publicitario, "correo basura", "cartas en cadena", "marketing piramidal" o cualquier otra forma de solicitud.Tu aviso se inscribe en estas categor�as.', '2015-02-20 15:02:25.32133', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal.32.text', 'No se permite difundir, transmitir o poner a disposici�n de terceros cualquier tipo de informaci�n, elemento o contenido que implica la violaci�n de los derechos de propiedad intelectual, incluyendo derechos de autor y propiedad industrial,  marcas, derechos de autor o de propiedad de los due�os de este sitio o de terceros.', '2015-02-20 15:02:25.321777', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal.33.text', 'No se permite difundir, transmitir o poner a disposici�n de terceros, cualquier tipo de informaci�n, elemento o contenido que implique  la violaci�n del secreto de las comunicaciones y la intimidad.', '2015-02-20 15:02:25.322171', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal.34.text', 'No se permiten m�s de cinco referencias a los productos  que podr�an constituir la base del intercambio. Los intercambios est�n permitidos en el sitio, pero se debe hacer una lista de menos de cinco referencias a los productos que forman la base del intercambio.', '2015-02-20 15:02:25.322591', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal.35.text', 'Categor�a errada', '2015-02-20 15:02:25.323012', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal_order.0', '32', '2015-02-20 15:02:25.323411', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal_order.1', '33', '2015-02-20 15:02:25.323995', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal_order.2', '31', '2015-02-20 15:02:25.324385', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal_order.3', '27', '2015-02-20 15:02:25.32495', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal_order.4', '25', '2015-02-20 15:02:25.325339', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal_order.5', '0', '2015-02-20 15:02:25.325791', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal_order.6', '5', '2015-02-20 15:02:25.326215', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal_order.7', '18', '2015-02-20 15:02:25.326648', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal_order.8', '19', '2015-02-20 15:02:25.327068', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal_order.9', '3', '2015-02-20 15:02:25.327449', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal_order.10', '20', '2015-02-20 15:02:25.327881', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal_order.11', '2', '2015-02-20 15:02:25.32827', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal_order.12', '1', '2015-02-20 15:02:25.328685', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal_order.13', '12', '2015-02-20 15:02:25.329106', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal_order.14', '10', '2015-02-20 15:02:25.329514', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal_order.15', '6', '2015-02-20 15:02:25.329931', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal_order.16', '7', '2015-02-20 15:02:25.33032', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal_order.17', '9', '2015-02-20 15:02:25.330743', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal_order.18', '14', '2015-02-20 15:02:25.331166', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal_order.19', '11', '2015-02-20 15:02:25.331576', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal_order.20', '22', '2015-02-20 15:02:25.331999', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal_order.21', '24', '2015-02-20 15:02:25.332381', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal_order.22', '13', '2015-02-20 15:02:25.332839', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal_order.23', '16', '2015-02-20 15:02:25.333228', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal_order.24', '23', '2015-02-20 15:02:25.333641', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal_order.25', '29', '2015-02-20 15:02:25.334089', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal_order.26', '34', '2015-02-20 15:02:25.334498', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal_order.27', '8', '2015-02-20 15:02:25.33509', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal_order.28', '35', '2015-02-20 15:02:25.335606', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal_order.29', '21', '2015-02-20 15:02:25.336113', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal_order.30', '4', '2015-02-20 15:02:25.336607', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.refusal_order.31', '30', '2015-02-20 15:02:25.337104', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.ais.synonyms.word.1.word', 'caravana', '2015-02-20 15:02:25.338853', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.ais.synonyms.word.1.synonyms.2', 'casa-m�vel', '2015-02-20 15:02:25.339319', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.ais.synonyms.word.1.synonyms.4', 'roulotte', '2015-02-20 15:02:25.339739', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.ais.synonyms.word.2.word', 'piso', '2015-02-20 15:02:25.340167', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.ais.synonyms.word.2.synonyms.1', 'apartamento', '2015-02-20 15:02:25.34057', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.ais.synonyms.word.2.synonyms.2', 'loft', '2015-02-20 15:02:25.340978', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.ais.synonyms.word.3.word', 'vw', '2015-02-20 15:02:25.341359', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.ais.synonyms.word.3.synonyms.1', 'volkswagen', '2015-02-20 15:02:25.341766', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.ais.synonyms.word.4.word', 'carro', '2015-02-20 15:02:25.342181', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.ais.synonyms.word.4.synonyms.1', 'autom�vel', '2015-02-20 15:02:25.342591', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.ais.synonyms.word.4.synonyms.2', 'buga', '2015-02-20 15:02:25.342995', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.ais.misspellings.word.1.misspelled.1', 'vlokswagen', '2015-02-20 15:02:25.343377', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.ais.misspellings.word.1.misspelled.2', 'volksbagen', '2015-02-20 15:02:25.343785', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.ais.misspellings.word.1.misspelled.3', 'volsvagen', '2015-02-20 15:02:25.344199', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.ais.misspellings.word.1.intended', 'volkswagen', '2015-02-20 15:02:25.344603', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.ais.misspellings.word.2.misspelled.1', 'apratamento', '2015-02-20 15:02:25.345251', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.ais.misspellings.word.2.misspelled.2', 'apatamento', '2015-02-20 15:02:25.3459', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.ais.misspellings.word.2.misspelled.3', 'apartametno', '2015-02-20 15:02:25.346474', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.ais.misspellings.word.2.intended', 'apartamento', '2015-02-20 15:02:25.347046', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.ais.misspellings.word.3.misspelled.1', 'carabana', '2015-02-20 15:02:25.347475', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.ais.misspellings.word.3.misspelled.2', 'cravana', '2015-02-20 15:02:25.348003', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.ais.misspellings.word.3.intended', 'caravana', '2015-02-20 15:02:25.348472', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.uf_conversion_factor', '22245,5290', '2015-02-20 15:02:25.348892', NULL);
INSERT INTO conf (key, value, modified_at, token_id) VALUES ('*.*.common.uf_conversion_updated', '2015-02-20 15:02:25.349365-03', '2015-02-20 15:02:25.349365', NULL);


--
-- Name: data_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('data_id_seq', 1294, true);


--
-- Data for Name: event_log; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Name: event_log_event_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('event_log_event_id_seq', 1, false);


--
-- Data for Name: example; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO example (id, col) VALUES (0, 12);
INSERT INTO example (id, col) VALUES (1, 56);
INSERT INTO example (id, col) VALUES (2, 32);
INSERT INTO example (id, col) VALUES (3, 156);
INSERT INTO example (id, col) VALUES (4, 2345);


--
-- Data for Name: filters; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO filters (filter_id, name, tables, condition, use_archive) VALUES (1, 'all', '', '1=1', true);
INSERT INTO filters (filter_id, name, tables, condition, use_archive) VALUES (2, 'unpaid', '', 'ad_actions.state = ''unpaid''', false);
INSERT INTO filters (filter_id, name, tables, condition, use_archive) VALUES (3, 'unverified', '', 'ad_actions.state = ''unverified''', false);
INSERT INTO filters (filter_id, name, tables, condition, use_archive) VALUES (4, 'new', '', 'ad_actions.queue = ''normal'' AND ad_actions.state IN (''pending_review'', ''locked'')', false);
INSERT INTO filters (filter_id, name, tables, condition, use_archive) VALUES (5, 'unsolved', '', 'ad_actions.queue = ''unsolved'' AND ad_actions.state IN (''pending_review'',''locked'')', false);
INSERT INTO filters (filter_id, name, tables, condition, use_archive) VALUES (6, 'abuse', '', 'ad_actions.queue = ''abuse'' AND ad_actions.state IN (''pending_review'',''locked'')', false);
INSERT INTO filters (filter_id, name, tables, condition, use_archive) VALUES (7, 'accepted', '', 'ad_actions.state = ''accepted''', false);
INSERT INTO filters (filter_id, name, tables, condition, use_archive) VALUES (8, 'refused', '', 'ad_actions.state = ''refused''', true);
INSERT INTO filters (filter_id, name, tables, condition, use_archive) VALUES (9, 'technical_error', '', 'ad_actions.queue = ''technical_error'' AND ad_actions.state NOT IN (''accepted'', ''refused'')', false);
INSERT INTO filters (filter_id, name, tables, condition, use_archive) VALUES (10, 'published', '', 'ad_actions.state IN (''accepted'', ''published'') AND NOT EXISTS (SELECT * FROM action_states AS newer WHERE ad_id = ad_actions.ad_id AND action_id != ad_actions.action_id AND state = ''accepted'' AND timestamp > newer.timestamp) AND NOT EXISTS ( SELECT * FROM ads WHERE ads.status = ''deleted'' AND ad_id=ad_actions.ad_id)', false);
INSERT INTO filters (filter_id, name, tables, condition, use_archive) VALUES (11, 'deleted', '', 'ad_actions.state = ''deleted''', true);
INSERT INTO filters (filter_id, name, tables, condition, use_archive) VALUES (12, 'autoaccept', '', 'ad_actions.queue = ''autoaccept'' AND ad_actions.state IN (''pending_review'', ''locked'')', false);
INSERT INTO filters (filter_id, name, tables, condition, use_archive) VALUES (13, 'autorefuse', '', 'ad_actions.queue = ''autorefuse'' AND ad_actions.state IN (''pending_review'', ''locked'')', false);
INSERT INTO filters (filter_id, name, tables, condition, use_archive) VALUES (16, 'video', '', 'ad_actions.queue = ''video'' AND ad_actions.state IN (''pending_review'', ''locked'')', false);
INSERT INTO filters (filter_id, name, tables, condition, use_archive) VALUES (17, 'spidered', '', 'ad_actions.queue = ''spidered'' AND ad_actions.state IN (''pending_review'', ''locked'')', false);


--
-- Name: filters_filter_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('filters_filter_id_seq', 1, false);


--
-- Data for Name: hold_mail_params; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Name: hold_mail_params_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('hold_mail_params_id_seq', 1, false);


--
-- Name: item_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('item_id_seq', 59, true);


--
-- Data for Name: iteminfo_items; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO iteminfo_items (item_id, word, defining, parent_id) VALUES (1, 'root', false, NULL);
INSERT INTO iteminfo_items (item_id, word, defining, parent_id) VALUES (2, 'primogenito', true, 1);
INSERT INTO iteminfo_items (item_id, word, defining, parent_id) VALUES (3, 'segundon', false, 1);
INSERT INTO iteminfo_items (item_id, word, defining, parent_id) VALUES (4, 'car_info', false, 1);
INSERT INTO iteminfo_items (item_id, word, defining, parent_id) VALUES (5, 'location_info', false, 1);
INSERT INTO iteminfo_items (item_id, word, defining, parent_id) VALUES (6, 'object_info', false, 1);
INSERT INTO iteminfo_items (item_id, word, defining, parent_id) VALUES (7, 'moto_info', false, 1);
INSERT INTO iteminfo_items (item_id, word, defining, parent_id) VALUES (8, 'sac', false, 6);
INSERT INTO iteminfo_items (item_id, word, defining, parent_id) VALUES (9, 'chanel', false, 8);
INSERT INTO iteminfo_items (item_id, word, defining, parent_id) VALUES (10, 'bmw', false, 7);
INSERT INTO iteminfo_items (item_id, word, defining, parent_id) VALUES (11, 'triumph', false, 7);
INSERT INTO iteminfo_items (item_id, word, defining, parent_id) VALUES (12, 'r100', true, 10);
INSERT INTO iteminfo_items (item_id, word, defining, parent_id) VALUES (13, 'r1100 gs', true, 10);
INSERT INTO iteminfo_items (item_id, word, defining, parent_id) VALUES (14, 'k100', true, 10);
INSERT INTO iteminfo_items (item_id, word, defining, parent_id) VALUES (15, 'america', true, 11);
INSERT INTO iteminfo_items (item_id, word, defining, parent_id) VALUES (16, 'adventurer', true, 11);
INSERT INTO iteminfo_items (item_id, word, defining, parent_id) VALUES (17, '1000', false, 12);
INSERT INTO iteminfo_items (item_id, word, defining, parent_id) VALUES (18, '1100', false, 13);
INSERT INTO iteminfo_items (item_id, word, defining, parent_id) VALUES (19, '1100 75 ans', false, 13);
INSERT INTO iteminfo_items (item_id, word, defining, parent_id) VALUES (20, '1100 abs', false, 13);
INSERT INTO iteminfo_items (item_id, word, defining, parent_id) VALUES (21, '1100 abs ergo', false, 13);
INSERT INTO iteminfo_items (item_id, word, defining, parent_id) VALUES (22, '1100 ergo', false, 13);
INSERT INTO iteminfo_items (item_id, word, defining, parent_id) VALUES (23, 'k100 lt', false, 14);
INSERT INTO iteminfo_items (item_id, word, defining, parent_id) VALUES (24, 'k100 rs', false, 14);
INSERT INTO iteminfo_items (item_id, word, defining, parent_id) VALUES (25, '865', false, 15);
INSERT INTO iteminfo_items (item_id, word, defining, parent_id) VALUES (26, '900', false, 16);
INSERT INTO iteminfo_items (item_id, word, defining, parent_id) VALUES (27, 'ford', false, 4);
INSERT INTO iteminfo_items (item_id, word, defining, parent_id) VALUES (28, 'renault', false, 4);
INSERT INTO iteminfo_items (item_id, word, defining, parent_id) VALUES (29, 'seat', false, 4);
INSERT INTO iteminfo_items (item_id, word, defining, parent_id) VALUES (30, 'escort', true, 27);
INSERT INTO iteminfo_items (item_id, word, defining, parent_id) VALUES (31, 'fiesta', true, 27);
INSERT INTO iteminfo_items (item_id, word, defining, parent_id) VALUES (32, 'laguna', true, 28);
INSERT INTO iteminfo_items (item_id, word, defining, parent_id) VALUES (33, 'megane', true, 28);
INSERT INTO iteminfo_items (item_id, word, defining, parent_id) VALUES (34, 'scenic', true, 28);
INSERT INTO iteminfo_items (item_id, word, defining, parent_id) VALUES (35, 'cordoba', true, 29);
INSERT INTO iteminfo_items (item_id, word, defining, parent_id) VALUES (36, 'ibiza', true, 29);
INSERT INTO iteminfo_items (item_id, word, defining, parent_id) VALUES (37, 'leon', true, 29);
INSERT INTO iteminfo_items (item_id, word, defining, parent_id) VALUES (38, 'cosworth', false, 30);
INSERT INTO iteminfo_items (item_id, word, defining, parent_id) VALUES (39, 'rs 2000', false, 30);
INSERT INTO iteminfo_items (item_id, word, defining, parent_id) VALUES (40, '1.8 turbo', false, 31);
INSERT INTO iteminfo_items (item_id, word, defining, parent_id) VALUES (41, '1.4 senso 5p', false, 31);
INSERT INTO iteminfo_items (item_id, word, defining, parent_id) VALUES (42, '1.8 16s', false, 32);
INSERT INTO iteminfo_items (item_id, word, defining, parent_id) VALUES (43, '1.8 gpl', false, 32);
INSERT INTO iteminfo_items (item_id, word, defining, parent_id) VALUES (44, '2.0 rxe', false, 32);
INSERT INTO iteminfo_items (item_id, word, defining, parent_id) VALUES (45, '1.9 dti', false, 32);
INSERT INTO iteminfo_items (item_id, word, defining, parent_id) VALUES (46, '2.2 d rt', false, 32);
INSERT INTO iteminfo_items (item_id, word, defining, parent_id) VALUES (47, '2.2 d rxe', false, 32);
INSERT INTO iteminfo_items (item_id, word, defining, parent_id) VALUES (48, '1.9 dci 110 authentique', false, 32);
INSERT INTO iteminfo_items (item_id, word, defining, parent_id) VALUES (49, '2.2 dci 150 initiale', false, 32);
INSERT INTO iteminfo_items (item_id, word, defining, parent_id) VALUES (50, '1.9 dci 120 ch expression', false, 32);
INSERT INTO iteminfo_items (item_id, word, defining, parent_id) VALUES (51, '1.9 sdi 3p', false, 36);
INSERT INTO iteminfo_items (item_id, word, defining, parent_id) VALUES (52, '1.9 tdi 105 reference', false, 37);
INSERT INTO iteminfo_items (item_id, word, defining, parent_id) VALUES (53, '2.0 tdi 140 stylance', false, 37);
INSERT INTO iteminfo_items (item_id, word, defining, parent_id) VALUES (54, 'tdi 110 signo', false, 37);
INSERT INTO iteminfo_items (item_id, word, defining, parent_id) VALUES (55, 'tdi 150 fr', false, 37);
INSERT INTO iteminfo_items (item_id, word, defining, parent_id) VALUES (56, 'tdi 150 sport', false, 37);
INSERT INTO iteminfo_items (item_id, word, defining, parent_id) VALUES (57, 'rx4 1.9 dci', false, 34);
INSERT INTO iteminfo_items (item_id, word, defining, parent_id) VALUES (58, '1.9 d rte', false, 34);
INSERT INTO iteminfo_items (item_id, word, defining, parent_id) VALUES (59, 'd rte', false, 34);
INSERT INTO iteminfo_items (item_id, word, defining, parent_id) VALUES (2001, '22', false, 5);
INSERT INTO iteminfo_items (item_id, word, defining, parent_id) VALUES (2002, '01', false, 2001);
INSERT INTO iteminfo_items (item_id, word, defining, parent_id) VALUES (2003, '01000', false, 2002);
INSERT INTO iteminfo_items (item_id, word, defining, parent_id) VALUES (2004, 'bourg en bresse', false, 2003);
INSERT INTO iteminfo_items (item_id, word, defining, parent_id) VALUES (2005, 'st denis les bourg', false, 2003);
INSERT INTO iteminfo_items (item_id, word, defining, parent_id) VALUES (2006, '12', false, 5);
INSERT INTO iteminfo_items (item_id, word, defining, parent_id) VALUES (2007, '75', false, 2006);
INSERT INTO iteminfo_items (item_id, word, defining, parent_id) VALUES (2008, '75001', false, 2007);
INSERT INTO iteminfo_items (item_id, word, defining, parent_id) VALUES (2009, 'paris', false, 2008);


--
-- Data for Name: iteminfo_data; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1, 'Hipoteca', 'De cojones', 1);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (2, 'Sueldo', 'Justico', 1);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (3, 'Broncas', 'Todas', 2);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (4, 'Bici', 'Heredada de su hermano', 3);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (5, 'price_logic_max_cat_2120', '10000', 6);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (6, 'price_logic_max_cat_3040', '20000', 6);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (7, 'price_logic_max_cat_3060', '10000', 6);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (8, 'price_logic_max_cat_8020', '20000', 6);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (9, 'price_logic_max_cat_8020', '560', 8);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (10, 'price_logic_max_cat_22', '1050', 9);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (11, 'price_fraud_cat_8020', '1050', 9);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (12, 'price_logic_max_cat_8060', '2000', 6);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (13, 'price_logic_max_cat_4100', '15000', 6);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (14, 'price_logic_max_cat_4120', '10000', 6);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (15, 'price_logic_max_cat_39', '10000', 6);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (16, 'price_logic_max_cat_41', '20000', 6);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (17, 'price_fraud_1976', '1600', 12);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (18, 'price_logic_min_1976', '2560', 12);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (19, 'price_logic_max_1976', '6400', 12);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (20, 'price_fraud_1977', '975', 12);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (21, 'price_logic_min_1977', '1560', 12);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (22, 'price_logic_max_1977', '3900', 12);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (23, 'price_fraud_1978', '1498.5', 12);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (24, 'price_logic_min_1978', '2397.6', 12);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (25, 'price_logic_max_1978', '5994', 12);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (26, 'price_fraud_1979', '1450', 12);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (27, 'price_logic_min_1979', '2320', 12);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (28, 'price_logic_max_1979', '5800', 12);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (29, 'price_fraud_1980', '1768', 12);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (30, 'price_logic_min_1980', '2828.8', 12);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (31, 'price_logic_max_1980', '7072', 12);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (32, 'price_fraud_1981', '1325', 12);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (33, 'price_logic_min_1981', '2120', 12);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (34, 'price_logic_max_1981', '5300', 12);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (35, 'price_fraud_1982', '1325', 12);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (36, 'price_logic_min_1982', '2120', 12);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (37, 'price_logic_max_1982', '5300', 12);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (38, 'price_fraud_1983', '1500', 12);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (39, 'price_logic_min_1983', '2400', 12);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (40, 'price_logic_max_1983', '6000', 12);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (41, 'price_fraud_1984', '1900', 12);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (42, 'price_logic_min_1984', '3040', 12);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (43, 'price_logic_max_1984', '7600', 12);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (44, 'price_fraud_1988', '1450', 12);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (45, 'price_logic_min_1988', '2320', 12);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (46, 'price_logic_max_1988', '5800', 12);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (47, 'price_fraud_1989', '1500', 12);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (48, 'price_logic_min_1989', '2400', 12);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (49, 'price_logic_max_1989', '6000', 12);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (50, 'price_fraud_1990', '2000', 12);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (51, 'price_logic_min_1990', '3200', 12);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (52, 'price_logic_max_1990', '8000', 12);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (53, 'price_fraud_1991', '1900', 12);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (54, 'price_logic_min_1991', '3040', 12);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (55, 'price_logic_max_1991', '7600', 12);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (56, 'price_fraud_1992', '1942.5', 12);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (57, 'price_logic_min_1992', '3108', 12);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (58, 'price_logic_max_1992', '7770', 12);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (59, 'price_fraud_1993', '2322.5', 12);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (60, 'price_logic_min_1993', '3716', 12);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (61, 'price_logic_max_1993', '9290', 12);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (62, 'price_fraud_1994', '2625', 12);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (63, 'price_logic_min_1994', '4200', 12);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (64, 'price_logic_max_1994', '10500', 12);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (65, 'price_fraud_1995', '1691.5', 12);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (66, 'price_logic_min_1995', '2706.4', 12);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (67, 'price_logic_max_1995', '6766', 12);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (68, 'price_fraud_1996', '2525', 12);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (69, 'price_logic_min_1996', '4040', 12);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (70, 'price_logic_max_1996', '10100', 12);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (71, 'price_fraud_1975', '2487.5', 11);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (72, 'price_logic_min_1975', '3980', 11);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (73, 'price_logic_max_1975', '9950', 11);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (74, 'price_fraud_1977', '2150', 11);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (75, 'price_logic_min_1977', '3440', 11);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (76, 'price_logic_max_1977', '8600', 11);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (77, 'price_fraud_1978', '2400', 11);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (78, 'price_logic_min_1978', '3840', 11);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (79, 'price_logic_max_1978', '9600', 11);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (80, 'price_fraud_1979', '2625', 11);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (81, 'price_logic_min_1979', '4200', 11);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (82, 'price_logic_max_1979', '10500', 11);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (83, 'price_fraud_1980', '3050', 11);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (84, 'price_logic_min_1980', '4880', 11);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (85, 'price_logic_max_1980', '12200', 11);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (86, 'price_fraud_1981', '2750', 11);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (87, 'price_logic_min_1981', '4400', 11);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (88, 'price_logic_max_1981', '11000', 11);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (89, 'price_fraud_1986', '6750', 11);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (90, 'price_logic_min_1986', '10800', 11);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (91, 'price_logic_max_1986', '27000', 11);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (92, 'price_fraud_1992', '1428.5', 11);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (93, 'price_logic_min_1992', '2285.6', 11);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (94, 'price_logic_max_1992', '5714', 11);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (95, 'price_fraud_1993', '1461', 11);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (96, 'price_logic_min_1993', '2337.6', 11);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (97, 'price_logic_max_1993', '5844', 11);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (98, 'price_fraud_1994', '1866.5', 11);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (99, 'price_logic_min_1994', '2986.4', 11);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (100, 'price_logic_max_1994', '7466', 11);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (101, 'price_fraud_1995', '1736.5', 11);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (102, 'price_logic_min_1995', '2778.4', 11);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (103, 'price_logic_max_1995', '6946', 11);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (104, 'price_fraud_1996', '1842.5', 11);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (105, 'price_logic_min_1996', '2948', 11);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (106, 'price_logic_max_1996', '7370', 11);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (107, 'price_fraud_1997', '1906', 11);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (108, 'price_logic_min_1997', '3049.6', 11);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (109, 'price_logic_max_1997', '7624', 11);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (110, 'price_fraud_1998', '2011.5', 11);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (111, 'price_logic_min_1998', '3218.4', 11);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (112, 'price_logic_max_1998', '8046', 11);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (113, 'price_fraud_1999', '2202.5', 11);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (114, 'price_logic_min_1999', '3524', 11);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (115, 'price_logic_max_1999', '8810', 11);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (116, 'price_fraud_2000', '2071.5', 11);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (117, 'price_logic_min_2000', '3314.4', 11);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (118, 'price_logic_max_2000', '8286', 11);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (119, 'price_fraud_2001', '2397.5', 11);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (120, 'price_logic_min_2001', '3836', 11);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (121, 'price_logic_max_2001', '9590', 11);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (122, 'price_fraud_2002', '2549', 11);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (123, 'price_logic_min_2002', '4078.4', 11);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (124, 'price_logic_max_2002', '10196', 11);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (125, 'price_fraud_2003', '2785', 11);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (126, 'price_logic_min_2003', '4456', 11);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (127, 'price_logic_max_2003', '11140', 11);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (128, 'price_fraud_2004', '3007', 11);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (129, 'price_logic_min_2004', '4811.2', 11);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (130, 'price_logic_max_2004', '12028', 11);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (131, 'price_fraud_2005', '4200.5', 11);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (132, 'price_logic_min_2005', '6720.8', 11);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (133, 'price_logic_max_2005', '16802', 11);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (134, 'price_fraud_2006', '4121.5', 11);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (135, 'price_logic_min_2006', '6594.4', 11);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (136, 'price_logic_max_2006', '16486', 11);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (137, 'price_fraud_2007', '4257.5', 11);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (138, 'price_logic_min_2007', '6812', 11);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (139, 'price_logic_max_2007', '17030', 11);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (140, 'price_fraud_2008', '4357.5', 11);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (141, 'price_logic_min_2008', '6972', 11);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (142, 'price_logic_max_2008', '17430', 11);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (143, 'price_fraud_2001', '2995', 15);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (144, 'price_logic_min_2001', '4792', 15);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (145, 'price_logic_max_2001', '11980', 15);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (146, 'price_fraud_2002', '2625', 15);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (147, 'price_logic_min_2002', '4200', 15);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (148, 'price_logic_max_2002', '10500', 15);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (149, 'price_fraud_2003', '2600', 15);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (150, 'price_logic_min_2003', '4160', 15);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (151, 'price_logic_max_2003', '10400', 15);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (152, 'price_fraud_2004', '3250', 15);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (153, 'price_logic_min_2004', '5200', 15);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (154, 'price_logic_max_2004', '13000', 15);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (155, 'price_fraud_2007', '3750', 15);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (156, 'price_logic_min_2007', '6000', 15);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (157, 'price_logic_max_2007', '15000', 15);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (158, 'price_fraud_1975', '1720.5', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (159, 'price_logic_min_1975', '2752.8', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (160, 'price_logic_max_1975', '6882', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (161, 'price_fraud_1976', '1610.5', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (162, 'price_logic_min_1976', '2576.8', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (163, 'price_logic_max_1976', '6442', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (164, 'price_fraud_1977', '1150', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (165, 'price_logic_min_1977', '1840', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (166, 'price_logic_max_1977', '4600', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (167, 'price_fraud_1978', '1491.5', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (168, 'price_logic_min_1978', '2386.4', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (169, 'price_logic_max_1978', '5966', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (170, 'price_fraud_1979', '1272.5', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (171, 'price_logic_min_1979', '2036', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (172, 'price_logic_max_1979', '5090', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (173, 'price_fraud_1980', '1623', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (174, 'price_logic_min_1980', '2596.8', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (175, 'price_logic_max_1980', '6492', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (176, 'price_fraud_1981', '1257', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (177, 'price_logic_min_1981', '2011.2', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (178, 'price_logic_max_1981', '5028', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (179, 'price_fraud_1982', '1237.5', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (180, 'price_logic_min_1982', '1980', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (181, 'price_logic_max_1982', '4950', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (182, 'price_fraud_1983', '1111', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (183, 'price_logic_min_1983', '1777.6', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (184, 'price_logic_max_1983', '4444', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (185, 'price_fraud_1984', '1127', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (186, 'price_logic_min_1984', '1803.2', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (187, 'price_logic_max_1984', '4508', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (188, 'price_fraud_1985', '1425', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (189, 'price_logic_min_1985', '2280', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (190, 'price_logic_max_1985', '5700', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (191, 'price_fraud_1986', '1179', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (192, 'price_logic_min_1986', '1886.4', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (193, 'price_logic_max_1986', '4716', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (194, 'price_fraud_1987', '1336.5', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (195, 'price_logic_min_1987', '2138.4', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (196, 'price_logic_max_1987', '5346', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (197, 'price_fraud_1988', '1402', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (198, 'price_logic_min_1988', '2243.2', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (199, 'price_logic_max_1988', '5608', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (200, 'price_fraud_1989', '1327.5', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (201, 'price_logic_min_1989', '2124', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (202, 'price_logic_max_1989', '5310', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (203, 'price_fraud_1990', '1678', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (204, 'price_logic_min_1990', '2684.8', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (205, 'price_logic_max_1990', '6712', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (206, 'price_fraud_1991', '1612', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (207, 'price_logic_min_1991', '2579.2', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (208, 'price_logic_max_1991', '6448', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (209, 'price_fraud_1992', '1703', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (210, 'price_logic_min_1992', '2724.8', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (211, 'price_logic_max_1992', '6812', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (212, 'price_fraud_1993', '1699.5', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (213, 'price_logic_min_1993', '2719.2', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (214, 'price_logic_max_1993', '6798', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (215, 'price_fraud_1994', '1561', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (216, 'price_logic_min_1994', '2497.6', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (217, 'price_logic_max_1994', '6244', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (218, 'price_fraud_1995', '1942', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (219, 'price_logic_min_1995', '3107.2', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (220, 'price_logic_max_1995', '7768', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (221, 'price_fraud_1996', '1993', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (222, 'price_logic_min_1996', '3188.8', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (223, 'price_logic_max_1996', '7972', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (224, 'price_fraud_1997', '2398', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (225, 'price_logic_min_1997', '3836.8', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (226, 'price_logic_max_1997', '9592', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (227, 'price_fraud_1998', '2456.5', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (228, 'price_logic_min_1998', '3930.4', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (229, 'price_logic_max_1998', '9826', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (230, 'price_fraud_1999', '2640', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (231, 'price_logic_min_1999', '4224', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (232, 'price_logic_max_1999', '10560', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (233, 'price_fraud_2000', '2942.5', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (234, 'price_logic_min_2000', '4708', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (235, 'price_logic_max_2000', '11770', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (236, 'price_fraud_2001', '3435.5', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (237, 'price_logic_min_2001', '5496.8', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (238, 'price_logic_max_2001', '13742', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (239, 'price_fraud_2002', '3541', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (240, 'price_logic_min_2002', '5665.6', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (241, 'price_logic_max_2002', '14164', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (242, 'price_fraud_2003', '3900', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (243, 'price_logic_min_2003', '6240', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (244, 'price_logic_max_2003', '15600', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (245, 'price_fraud_2004', '4205.5', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (246, 'price_logic_min_2004', '6728.8', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (247, 'price_logic_max_2004', '16822', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (248, 'price_fraud_2005', '4750.5', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (249, 'price_logic_min_2005', '7600.8', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (250, 'price_logic_max_2005', '19002', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (251, 'price_fraud_2006', '5451.5', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (252, 'price_logic_min_2006', '8722.4', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (253, 'price_logic_max_2006', '21806', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (254, 'price_fraud_2007', '6064', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (255, 'price_logic_min_2007', '9702.4', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (256, 'price_logic_max_2007', '24256', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (257, 'price_fraud_2008', '6650.5', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (258, 'price_logic_min_2008', '10640.8', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (259, 'price_logic_max_2008', '26602', 10);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (260, 'price_fraud_1996', '1950', 26);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (261, 'price_logic_min_1996', '3120', 26);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (262, 'price_logic_max_1996', '7800', 26);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (263, 'price_fraud_1997', '1750', 26);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (264, 'price_logic_min_1997', '2800', 26);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (265, 'price_logic_max_1997', '7000', 26);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (266, 'price_fraud_2001', '2800', 26);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (267, 'price_logic_min_2001', '4480', 26);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (268, 'price_logic_max_2001', '11200', 26);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (269, 'price_fraud_1996', '1875', 16);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (270, 'price_logic_min_1996', '3000', 16);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (271, 'price_logic_max_1996', '7500', 16);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (272, 'price_fraud_1997', '1766.5', 16);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (273, 'price_logic_min_1997', '2826.4', 16);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (274, 'price_logic_max_1997', '7066', 16);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (275, 'price_fraud_1998', '2000', 16);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (276, 'price_logic_min_1998', '3200', 16);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (277, 'price_logic_max_1998', '8000', 16);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (278, 'price_fraud_2001', '2800', 16);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (279, 'price_logic_min_2001', '4480', 16);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (280, 'price_logic_max_2001', '11200', 16);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (281, 'price_fraud_1992', '5625', 38);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (282, 'price_logic_min_1992', '9000', 38);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (283, 'price_logic_max_1992', '22500', 38);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (284, 'price_fraud_1993', '9233', 38);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (285, 'price_logic_min_1993', '14772.8', 38);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (286, 'price_logic_max_1993', '36932', 38);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (287, 'price_fraud_1994', '10500', 38);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (288, 'price_logic_min_1994', '16800', 38);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (289, 'price_logic_max_1994', '42000', 38);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (290, 'price_fraud_1995', '12000', 38);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (291, 'price_logic_min_1995', '19200', 38);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (292, 'price_logic_max_1995', '48000', 38);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (293, 'price_fraud_1992', '1450', 39);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (294, 'price_logic_min_1992', '2320', 39);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (295, 'price_logic_max_1992', '5800', 39);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (296, 'price_fraud_1993', '2250', 39);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (297, 'price_logic_min_1993', '3600', 39);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (298, 'price_logic_max_1993', '9000', 39);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (299, 'price_fraud_1995', '1783', 39);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (300, 'price_logic_min_1995', '2852.8', 39);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (301, 'price_logic_max_1995', '7132', 39);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (302, 'price_fraud_1996', '1683', 39);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (303, 'price_logic_min_1996', '2692.8', 39);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (304, 'price_logic_max_1996', '6732', 39);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (305, 'price_fraud_1998', '6100', 39);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (306, 'price_logic_min_1998', '9760', 39);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (307, 'price_logic_max_1998', '24400', 39);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (308, 'price_fraud_1975', '991.5', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (309, 'price_logic_min_1975', '1586.4', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (310, 'price_logic_max_1975', '3966', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (311, 'price_fraud_1976', '750', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (312, 'price_logic_min_1976', '1200', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (313, 'price_logic_max_1976', '3000', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (314, 'price_fraud_1977', '1500', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (315, 'price_logic_min_1977', '2400', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (316, 'price_logic_max_1977', '6000', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (317, 'price_fraud_1979', '437.5', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (318, 'price_logic_min_1979', '700', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (319, 'price_logic_max_1979', '1750', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (320, 'price_fraud_1980', '1050', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (321, 'price_logic_min_1980', '1680', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (322, 'price_logic_max_1980', '4200', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (323, 'price_fraud_1981', '375', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (324, 'price_logic_min_1981', '600', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (325, 'price_logic_max_1981', '1500', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (326, 'price_fraud_1982', '230', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (327, 'price_logic_min_1982', '368', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (328, 'price_logic_max_1982', '920', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (329, 'price_fraud_1983', '343.5', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (330, 'price_logic_min_1983', '549.6', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (331, 'price_logic_max_1983', '1374', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (332, 'price_fraud_1984', '520', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (333, 'price_logic_min_1984', '832', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (334, 'price_logic_max_1984', '2080', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (335, 'price_fraud_1985', '365', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (336, 'price_logic_min_1985', '584', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (337, 'price_logic_max_1985', '1460', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (338, 'price_fraud_1986', '703.5', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (339, 'price_logic_min_1986', '1125.6', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (340, 'price_logic_max_1986', '2814', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (341, 'price_fraud_1987', '432', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (342, 'price_logic_min_1987', '691.2', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (343, 'price_logic_max_1987', '1728', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (344, 'price_fraud_1988', '526', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (345, 'price_logic_min_1988', '841.6', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (346, 'price_logic_max_1988', '2104', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (347, 'price_fraud_1989', '421.5', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (348, 'price_logic_min_1989', '674.4', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (349, 'price_logic_max_1989', '1686', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (350, 'price_fraud_1990', '459', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (351, 'price_logic_min_1990', '734.4', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (352, 'price_logic_max_1990', '1836', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (353, 'price_fraud_1991', '460.5', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (354, 'price_logic_min_1991', '736.8', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (355, 'price_logic_max_1991', '1842', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (356, 'price_fraud_1992', '614', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (357, 'price_logic_min_1992', '982.4', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (358, 'price_logic_max_1992', '2456', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (359, 'price_fraud_1993', '699.5', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (360, 'price_logic_min_1993', '1119.2', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (361, 'price_logic_max_1993', '2798', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (362, 'price_fraud_1994', '684', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (363, 'price_logic_min_1994', '1094.4', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (364, 'price_logic_max_1994', '2736', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (365, 'price_fraud_1995', '768', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (366, 'price_logic_min_1995', '1228.8', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (367, 'price_logic_max_1995', '3072', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (368, 'price_fraud_1996', '883.5', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (369, 'price_logic_min_1996', '1413.6', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (370, 'price_logic_max_1996', '3534', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (371, 'price_fraud_1997', '956.5', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (372, 'price_logic_min_1997', '1530.4', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (373, 'price_logic_max_1997', '3826', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (374, 'price_fraud_1998', '1153.5', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (375, 'price_logic_min_1998', '1845.6', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (376, 'price_logic_max_1998', '4614', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (377, 'price_fraud_1999', '1244.5', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (378, 'price_logic_min_1999', '1991.2', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (379, 'price_logic_max_1999', '4978', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (380, 'price_fraud_2000', '1298.5', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (381, 'price_logic_min_2000', '2077.6', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (382, 'price_logic_max_2000', '5194', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (383, 'price_fraud_2001', '1873.5', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (384, 'price_logic_min_2001', '2997.6', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (385, 'price_logic_max_2001', '7494', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (386, 'price_fraud_2002', '1600', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (387, 'price_logic_min_2002', '2560', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (388, 'price_logic_max_2002', '6400', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (389, 'price_fraud_2007', '1150', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (390, 'price_logic_min_2007', '1840', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (391, 'price_logic_max_2007', '4600', 30);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (392, 'price_fraud_1993', '300', 40);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (393, 'price_logic_min_1993', '480', 40);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (394, 'price_logic_max_1993', '1200', 40);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (395, 'price_fraud_1996', '10000', 40);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (396, 'price_logic_min_1996', '16000', 40);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (397, 'price_logic_max_1996', '40000', 40);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (398, 'price_fraud_2001', '1836', 40);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (399, 'price_logic_min_2001', '2937.6', 40);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (400, 'price_logic_max_2001', '7344', 40);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (401, 'price_fraud_2002', '2100', 40);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (402, 'price_logic_min_2002', '3360', 40);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (403, 'price_logic_max_2002', '8400', 40);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (404, 'price_fraud_2004', '3930', 41);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (405, 'price_logic_min_2004', '6288', 41);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (406, 'price_logic_max_2004', '15720', 41);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (407, 'price_fraud_2005', '4316.5', 41);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (408, 'price_logic_min_2005', '6906.4', 41);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (409, 'price_logic_max_2005', '17266', 41);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (410, 'price_fraud_2006', '4622.5', 41);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (411, 'price_logic_min_2006', '7396', 41);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (412, 'price_logic_max_2006', '18490', 41);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (413, 'price_fraud_2007', '5154.5', 41);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (414, 'price_logic_min_2007', '8247.2', 41);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (415, 'price_logic_max_2007', '20618', 41);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (416, 'price_fraud_2008', '6725', 41);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (417, 'price_logic_min_2008', '10760', 41);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (418, 'price_logic_max_2008', '26900', 41);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (419, 'price_fraud_1975', '100', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (420, 'price_logic_min_1975', '160', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (421, 'price_logic_max_1975', '400', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (422, 'price_fraud_1976', '1150', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (423, 'price_logic_min_1976', '1840', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (424, 'price_logic_max_1976', '4600', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (425, 'price_fraud_1977', '220', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (426, 'price_logic_min_1977', '352', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (427, 'price_logic_max_1977', '880', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (428, 'price_fraud_1978', '375', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (429, 'price_logic_min_1978', '600', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (430, 'price_logic_max_1978', '1500', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (431, 'price_fraud_1980', '225', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (432, 'price_logic_min_1980', '360', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (433, 'price_logic_max_1980', '900', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (434, 'price_fraud_1981', '250', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (435, 'price_logic_min_1981', '400', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (436, 'price_logic_max_1981', '1000', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (437, 'price_fraud_1982', '100', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (438, 'price_logic_min_1982', '160', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (439, 'price_logic_max_1982', '400', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (440, 'price_fraud_1983', '166.5', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (441, 'price_logic_min_1983', '266.4', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (442, 'price_logic_max_1983', '666', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (443, 'price_fraud_1984', '184', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (444, 'price_logic_min_1984', '294.4', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (445, 'price_logic_max_1984', '736', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (446, 'price_fraud_1985', '191.5', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (447, 'price_logic_min_1985', '306.4', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (448, 'price_logic_max_1985', '766', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (449, 'price_fraud_1986', '316.5', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (450, 'price_logic_min_1986', '506.4', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (451, 'price_logic_max_1986', '1266', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (452, 'price_fraud_1987', '285', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (453, 'price_logic_min_1987', '456', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (454, 'price_logic_max_1987', '1140', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (455, 'price_fraud_1988', '323', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (456, 'price_logic_min_1988', '516.8', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (457, 'price_logic_max_1988', '1292', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (458, 'price_fraud_1989', '321.5', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (459, 'price_logic_min_1989', '514.4', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (460, 'price_logic_max_1989', '1286', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (461, 'price_fraud_1990', '352.5', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (462, 'price_logic_min_1990', '564', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (463, 'price_logic_max_1990', '1410', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (464, 'price_fraud_1991', '395', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (465, 'price_logic_min_1991', '632', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (466, 'price_logic_max_1991', '1580', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (467, 'price_fraud_1992', '475', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (468, 'price_logic_min_1992', '760', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (469, 'price_logic_max_1992', '1900', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (470, 'price_fraud_1993', '487', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (471, 'price_logic_min_1993', '779.2', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (472, 'price_logic_max_1993', '1948', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (473, 'price_fraud_1994', '549.5', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (474, 'price_logic_min_1994', '879.2', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (475, 'price_logic_max_1994', '2198', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (476, 'price_fraud_1995', '651.5', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (477, 'price_logic_min_1995', '1042.4', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (478, 'price_logic_max_1995', '2606', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (479, 'price_fraud_1996', '829.5', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (480, 'price_logic_min_1996', '1327.2', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (481, 'price_logic_max_1996', '3318', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (482, 'price_fraud_1997', '1048.5', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (483, 'price_logic_min_1997', '1677.6', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (484, 'price_logic_max_1997', '4194', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (485, 'price_fraud_1998', '1168', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (486, 'price_logic_min_1998', '1868.8', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (487, 'price_logic_max_1998', '4672', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (488, 'price_fraud_1999', '1340', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (489, 'price_logic_min_1999', '2144', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (490, 'price_logic_max_1999', '5360', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (491, 'price_fraud_2000', '1591', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (492, 'price_logic_min_2000', '2545.6', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (493, 'price_logic_max_2000', '6364', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (494, 'price_fraud_2001', '1940.5', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (495, 'price_logic_min_2001', '3104.8', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (496, 'price_logic_max_2001', '7762', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (497, 'price_fraud_2002', '2489', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (498, 'price_logic_min_2002', '3982.4', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (499, 'price_logic_max_2002', '9956', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (500, 'price_fraud_2003', '3363', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (501, 'price_logic_min_2003', '5380.8', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (502, 'price_logic_max_2003', '13452', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (503, 'price_fraud_2004', '3318.5', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (504, 'price_logic_min_2004', '5309.6', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (505, 'price_logic_max_2004', '13274', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (506, 'price_fraud_2005', '3736', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (507, 'price_logic_min_2005', '5977.6', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (508, 'price_logic_max_2005', '14944', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (509, 'price_fraud_2006', '4460', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (510, 'price_logic_min_2006', '7136', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (511, 'price_logic_max_2006', '17840', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (512, 'price_fraud_2007', '5120.5', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (513, 'price_logic_min_2007', '8192.8', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (514, 'price_logic_max_2007', '20482', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (515, 'price_fraud_2008', '5896.5', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (516, 'price_logic_min_2008', '9434.4', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (517, 'price_logic_max_2008', '23586', 31);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (518, 'price_fraud_1975', '4529.5', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (519, 'price_logic_min_1975', '7247.2', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (520, 'price_logic_max_1975', '18118', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (521, 'price_fraud_1976', '1860', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (522, 'price_logic_min_1976', '2976', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (523, 'price_logic_max_1976', '7440', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (524, 'price_fraud_1977', '1113.5', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (525, 'price_logic_min_1977', '1781.6', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (526, 'price_logic_max_1977', '4454', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (527, 'price_fraud_1978', '699', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (528, 'price_logic_min_1978', '1118.4', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (529, 'price_logic_max_1978', '2796', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (530, 'price_fraud_1979', '1120.5', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (531, 'price_logic_min_1979', '1792.8', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (532, 'price_logic_max_1979', '4482', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (533, 'price_fraud_1980', '1116.5', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (534, 'price_logic_min_1980', '1786.4', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (535, 'price_logic_max_1980', '4466', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (536, 'price_fraud_1981', '956.5', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (537, 'price_logic_min_1981', '1530.4', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (538, 'price_logic_max_1981', '3826', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (539, 'price_fraud_1982', '561.5', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (540, 'price_logic_min_1982', '898.4', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (541, 'price_logic_max_1982', '2246', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (542, 'price_fraud_1983', '526', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (543, 'price_logic_min_1983', '841.6', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (544, 'price_logic_max_1983', '2104', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (545, 'price_fraud_1984', '483.5', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (546, 'price_logic_min_1984', '773.6', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (547, 'price_logic_max_1984', '1934', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (548, 'price_fraud_1985', '354', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (549, 'price_logic_min_1985', '566.4', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (550, 'price_logic_max_1985', '1416', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (551, 'price_fraud_1986', '473', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (552, 'price_logic_min_1986', '756.8', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (553, 'price_logic_max_1986', '1892', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (554, 'price_fraud_1987', '402.5', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (555, 'price_logic_min_1987', '644', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (556, 'price_logic_max_1987', '1610', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (557, 'price_fraud_1988', '430', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (558, 'price_logic_min_1988', '688', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (559, 'price_logic_max_1988', '1720', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (560, 'price_fraud_1989', '410', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (561, 'price_logic_min_1989', '656', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (562, 'price_logic_max_1989', '1640', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (563, 'price_fraud_1990', '454.5', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (564, 'price_logic_min_1990', '727.2', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (565, 'price_logic_max_1990', '1818', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (566, 'price_fraud_1991', '442.5', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (567, 'price_logic_min_1991', '708', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (568, 'price_logic_max_1991', '1770', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (569, 'price_fraud_1992', '541', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (570, 'price_logic_min_1992', '865.6', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (571, 'price_logic_max_1992', '2164', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (572, 'price_fraud_1993', '645', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (573, 'price_logic_min_1993', '1032', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (574, 'price_logic_max_1993', '2580', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (575, 'price_fraud_1994', '710.5', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (576, 'price_logic_min_1994', '1136.8', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (577, 'price_logic_max_1994', '2842', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (578, 'price_fraud_1995', '869', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (579, 'price_logic_min_1995', '1390.4', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (580, 'price_logic_max_1995', '3476', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (581, 'price_fraud_1996', '1012.5', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (582, 'price_logic_min_1996', '1620', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (583, 'price_logic_max_1996', '4050', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (584, 'price_fraud_1997', '1226.5', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (585, 'price_logic_min_1997', '1962.4', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (586, 'price_logic_max_1997', '4906', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (587, 'price_fraud_1998', '1405', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (588, 'price_logic_min_1998', '2248', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (589, 'price_logic_max_1998', '5620', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (590, 'price_fraud_1999', '1682.5', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (591, 'price_logic_min_1999', '2692', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (592, 'price_logic_max_1999', '6730', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (593, 'price_fraud_2000', '2113.5', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (594, 'price_logic_min_2000', '3381.6', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (595, 'price_logic_max_2000', '8454', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (596, 'price_fraud_2001', '2815.5', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (597, 'price_logic_min_2001', '4504.8', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (598, 'price_logic_max_2001', '11262', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (599, 'price_fraud_2002', '3258', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (600, 'price_logic_min_2002', '5212.8', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (601, 'price_logic_max_2002', '13032', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (602, 'price_fraud_2003', '3888', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (603, 'price_logic_min_2003', '6220.8', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (604, 'price_logic_max_2003', '15552', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (605, 'price_fraud_2004', '4701', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (606, 'price_logic_min_2004', '7521.6', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (607, 'price_logic_max_2004', '18804', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (608, 'price_fraud_2005', '5431', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (609, 'price_logic_min_2005', '8689.6', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (610, 'price_logic_max_2005', '21724', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (611, 'price_fraud_2006', '6420', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (612, 'price_logic_min_2006', '10272', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (613, 'price_logic_max_2006', '25680', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (614, 'price_fraud_2007', '8082', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (615, 'price_logic_min_2007', '12931.2', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (616, 'price_logic_max_2007', '32328', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (617, 'price_fraud_2008', '9657', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (618, 'price_logic_min_2008', '15451.2', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (619, 'price_logic_max_2008', '38628', 27);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (620, 'price_fraud_1998', '1700', 42);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (621, 'price_logic_min_1998', '2720', 42);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (622, 'price_logic_max_1998', '6800', 42);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (623, 'price_fraud_1999', '1550', 42);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (624, 'price_logic_min_1999', '2480', 42);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (625, 'price_logic_max_1999', '6200', 42);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (626, 'price_fraud_2001', '3800', 42);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (627, 'price_logic_min_2001', '6080', 42);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (628, 'price_logic_max_2001', '15200', 42);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (629, 'price_fraud_2002', '2972.5', 42);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (630, 'price_logic_min_2002', '4756', 42);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (631, 'price_logic_max_2002', '11890', 42);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (632, 'price_fraud_1996', '1250', 43);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (633, 'price_logic_min_1996', '2000', 43);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (634, 'price_logic_max_1996', '5000', 43);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (635, 'price_fraud_1998', '850', 43);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (636, 'price_logic_min_1998', '1360', 43);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (637, 'price_logic_max_1998', '3400', 43);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (638, 'price_fraud_1999', '1575', 43);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (639, 'price_logic_min_1999', '2520', 43);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (640, 'price_logic_max_1999', '6300', 43);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (641, 'price_fraud_2000', '1100', 43);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (642, 'price_logic_min_2000', '1760', 43);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (643, 'price_logic_max_2000', '4400', 43);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (644, 'price_fraud_1994', '831', 44);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (645, 'price_logic_min_1994', '1329.6', 44);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (646, 'price_logic_max_1994', '3324', 44);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (647, 'price_fraud_1995', '775', 44);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (648, 'price_logic_min_1995', '1240', 44);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (649, 'price_logic_max_1995', '3100', 44);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (650, 'price_fraud_1996', '1387.5', 44);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (651, 'price_logic_min_1996', '2220', 44);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (652, 'price_logic_max_1996', '5550', 44);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (653, 'price_fraud_1997', '1400', 44);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (654, 'price_logic_min_1997', '2240', 44);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (655, 'price_logic_max_1997', '5600', 44);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (656, 'price_fraud_1998', '1852.5', 45);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (657, 'price_logic_min_1998', '2964', 45);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (658, 'price_logic_max_1998', '7410', 45);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (659, 'price_fraud_1999', '1749.5', 45);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (660, 'price_logic_min_1999', '2799.2', 45);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (661, 'price_logic_max_1999', '6998', 45);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (662, 'price_fraud_2000', '2148.5', 45);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (663, 'price_logic_min_2000', '3437.6', 45);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (664, 'price_logic_max_2000', '8594', 45);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (665, 'price_fraud_2001', '2000', 45);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (666, 'price_logic_min_2001', '3200', 45);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (667, 'price_logic_max_2001', '8000', 45);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (668, 'price_fraud_1995', '1625', 46);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (669, 'price_logic_min_1995', '2600', 46);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (670, 'price_logic_max_1995', '6500', 46);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (671, 'price_fraud_1996', '1440', 46);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (672, 'price_logic_min_1996', '2304', 46);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (673, 'price_logic_max_1996', '5760', 46);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (674, 'price_fraud_1997', '1275', 46);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (675, 'price_logic_min_1997', '2040', 46);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (676, 'price_logic_max_1997', '5100', 46);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (677, 'price_fraud_1998', '1400', 46);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (678, 'price_logic_min_1998', '2240', 46);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (679, 'price_logic_max_1998', '5600', 46);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (680, 'price_fraud_2004', '4250', 46);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (681, 'price_logic_min_2004', '6800', 46);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (682, 'price_logic_max_2004', '17000', 46);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (683, 'price_fraud_1995', '1441.5', 47);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (684, 'price_logic_min_1995', '2306.4', 47);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (685, 'price_logic_max_1995', '5766', 47);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (686, 'price_fraud_1996', '1229', 47);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (687, 'price_logic_min_1996', '1966.4', 47);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (688, 'price_logic_max_1996', '4916', 47);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (689, 'price_fraud_1997', '1750', 47);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (690, 'price_logic_min_1997', '2800', 47);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (691, 'price_logic_max_1997', '7000', 47);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (692, 'price_fraud_1999', '2325', 47);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (693, 'price_logic_min_1999', '3720', 47);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (694, 'price_logic_max_1999', '9300', 47);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (695, 'price_fraud_2000', '1600', 47);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (696, 'price_logic_min_2000', '2560', 47);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (697, 'price_logic_max_2000', '6400', 47);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (698, 'price_fraud_2002', '3325', 48);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (699, 'price_logic_min_2002', '5320', 48);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (700, 'price_logic_max_2002', '13300', 48);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (701, 'price_fraud_2003', '4100', 48);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (702, 'price_logic_min_2003', '6560', 48);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (703, 'price_logic_max_2003', '16400', 48);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (704, 'price_fraud_2004', '3625', 48);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (705, 'price_logic_min_2004', '5800', 48);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (706, 'price_logic_max_2004', '14500', 48);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (707, 'price_fraud_2005', '4800', 48);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (708, 'price_logic_min_2005', '7680', 48);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (709, 'price_logic_max_2005', '19200', 48);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (710, 'price_fraud_2006', '5000', 48);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (711, 'price_logic_min_2006', '8000', 48);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (712, 'price_logic_max_2006', '20000', 48);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (713, 'price_fraud_2003', '5175', 49);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (714, 'price_logic_min_2003', '8280', 49);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (715, 'price_logic_max_2003', '20700', 49);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (716, 'price_fraud_2004', '5475', 49);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (717, 'price_logic_min_2004', '8760', 49);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (718, 'price_logic_max_2004', '21900', 49);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (719, 'price_fraud_2005', '6467.5', 49);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (720, 'price_logic_min_2005', '10348', 49);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (721, 'price_logic_max_2005', '25870', 49);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (722, 'price_fraud_2006', '8950', 49);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (723, 'price_logic_min_2006', '14320', 49);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (724, 'price_logic_max_2006', '35800', 49);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (725, 'price_fraud_2001', '3400', 50);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (726, 'price_logic_min_2001', '5440', 50);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (727, 'price_logic_max_2001', '13600', 50);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (728, 'price_fraud_2002', '3445', 50);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (729, 'price_logic_min_2002', '5512', 50);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (730, 'price_logic_max_2002', '13780', 50);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (731, 'price_fraud_2004', '4950', 50);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (732, 'price_logic_min_2004', '7920', 50);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (733, 'price_logic_max_2004', '19800', 50);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (734, 'price_fraud_2005', '4950', 50);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (735, 'price_logic_min_2005', '7920', 50);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (736, 'price_logic_max_2005', '19800', 50);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (737, 'price_fraud_1984', '1750', 32);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (738, 'price_logic_min_1984', '2800', 32);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (739, 'price_logic_max_1984', '7000', 32);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (740, 'price_fraud_1988', '1750', 32);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (741, 'price_logic_min_1988', '2800', 32);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (742, 'price_logic_max_1988', '7000', 32);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (743, 'price_fraud_1989', '1650', 32);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (744, 'price_logic_min_1989', '2640', 32);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (745, 'price_logic_max_1989', '6600', 32);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (746, 'price_fraud_1991', '2733', 32);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (747, 'price_logic_min_1991', '4372.8', 32);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (748, 'price_logic_max_1991', '10932', 32);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (749, 'price_fraud_1992', '1000', 32);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (750, 'price_logic_min_1992', '1600', 32);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (751, 'price_logic_max_1992', '4000', 32);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (752, 'price_fraud_1993', '1625', 32);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (753, 'price_logic_min_1993', '2600', 32);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (754, 'price_logic_max_1993', '6500', 32);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (755, 'price_fraud_1994', '828', 32);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (756, 'price_logic_min_1994', '1324.8', 32);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (757, 'price_logic_max_1994', '3312', 32);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (758, 'price_fraud_1995', '1057.5', 32);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (759, 'price_logic_min_1995', '1692', 32);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (760, 'price_logic_max_1995', '4230', 32);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (761, 'price_fraud_1996', '1136.5', 32);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (762, 'price_logic_min_1996', '1818.4', 32);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (763, 'price_logic_max_1996', '4546', 32);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (764, 'price_fraud_1997', '1295.5', 32);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (765, 'price_logic_min_1997', '2072.8', 32);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (766, 'price_logic_max_1997', '5182', 32);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (767, 'price_fraud_1998', '1616', 32);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (768, 'price_logic_min_1998', '2585.6', 32);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (769, 'price_logic_max_1998', '6464', 32);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (770, 'price_fraud_1999', '1857.5', 32);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (771, 'price_logic_min_1999', '2972', 32);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (772, 'price_logic_max_1999', '7430', 32);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (773, 'price_fraud_2000', '2170', 32);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (774, 'price_logic_min_2000', '3472', 32);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (775, 'price_logic_max_2000', '8680', 32);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (776, 'price_fraud_2001', '3146', 32);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (777, 'price_logic_min_2001', '5033.6', 32);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (778, 'price_logic_max_2001', '12584', 32);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (779, 'price_fraud_2002', '3565.5', 32);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (780, 'price_logic_min_2002', '5704.8', 32);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (781, 'price_logic_max_2002', '14262', 32);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (782, 'price_fraud_2003', '4029', 32);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (783, 'price_logic_min_2003', '6446.4', 32);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (784, 'price_logic_max_2003', '16116', 32);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (785, 'price_fraud_2004', '4702.5', 32);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (786, 'price_logic_min_2004', '7524', 32);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (787, 'price_logic_max_2004', '18810', 32);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (788, 'price_fraud_2005', '5904.5', 32);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (789, 'price_logic_min_2005', '9447.2', 32);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (790, 'price_logic_max_2005', '23618', 32);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (791, 'price_fraud_2006', '7192.5', 32);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (792, 'price_logic_min_2006', '11508', 32);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (793, 'price_logic_max_2006', '28770', 32);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (794, 'price_fraud_2007', '9243', 32);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (795, 'price_logic_min_2007', '14788.8', 32);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (796, 'price_logic_max_2007', '36972', 32);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (797, 'price_fraud_2008', '11569.5', 32);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (798, 'price_logic_min_2008', '18511.2', 32);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (799, 'price_logic_max_2008', '46278', 32);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (800, 'price_fraud_2000', '3866.5', 57);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (801, 'price_logic_min_2000', '6186.4', 57);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (802, 'price_logic_max_2000', '15466', 57);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (803, 'price_fraud_2001', '3982', 57);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (804, 'price_logic_min_2001', '6371.2', 57);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (805, 'price_logic_max_2001', '15928', 57);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (806, 'price_fraud_2002', '4284', 57);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (807, 'price_logic_min_2002', '6854.4', 57);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (808, 'price_logic_max_2002', '17136', 57);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (809, 'price_fraud_2003', '6075', 57);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (810, 'price_logic_min_2003', '9720', 57);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (811, 'price_logic_max_2003', '24300', 57);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (812, 'price_fraud_2007', '7950', 57);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (813, 'price_logic_min_2007', '12720', 57);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (814, 'price_logic_max_2007', '31800', 57);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (815, 'price_fraud_1997', '1533', 58);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (816, 'price_logic_min_1997', '2452.8', 58);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (817, 'price_logic_max_1997', '6132', 58);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (818, 'price_fraud_1998', '2033', 58);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (819, 'price_logic_min_1998', '3252.8', 58);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (820, 'price_logic_max_1998', '8132', 58);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (821, 'price_fraud_1999', '2248', 58);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (822, 'price_logic_min_1999', '3596.8', 58);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (823, 'price_logic_max_1999', '8992', 58);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (824, 'price_fraud_2000', '2313', 58);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (825, 'price_logic_min_2000', '3700.8', 58);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (826, 'price_logic_max_2000', '9252', 58);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (827, 'price_fraud_2001', '2300', 58);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (828, 'price_logic_min_2001', '3680', 58);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (829, 'price_logic_max_2001', '9200', 58);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (830, 'price_fraud_1997', '1816.5', 59);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (831, 'price_logic_min_1997', '2906.4', 59);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (832, 'price_logic_max_1997', '7266', 59);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (833, 'price_fraud_1998', '2100', 59);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (834, 'price_logic_min_1998', '3360', 59);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (835, 'price_logic_max_1998', '8400', 59);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (836, 'price_fraud_2001', '3450', 59);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (837, 'price_logic_min_2001', '5520', 59);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (838, 'price_logic_max_2001', '13800', 59);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (839, 'price_fraud_2007', '9125', 59);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (840, 'price_logic_min_2007', '14600', 59);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (841, 'price_logic_max_2007', '36500', 59);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (842, 'price_fraud_1988', '1500', 34);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (843, 'price_logic_min_1988', '2400', 34);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (844, 'price_logic_max_1988', '6000', 34);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (845, 'price_fraud_1994', '5000', 34);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (846, 'price_logic_min_1994', '8000', 34);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (847, 'price_logic_max_1994', '20000', 34);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (848, 'price_fraud_1995', '1600', 34);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (849, 'price_logic_min_1995', '2560', 34);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (850, 'price_logic_max_1995', '6400', 34);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (851, 'price_fraud_1996', '1641.5', 34);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (852, 'price_logic_min_1996', '2626.4', 34);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (853, 'price_logic_max_1996', '6566', 34);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (854, 'price_fraud_1997', '1526', 34);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (855, 'price_logic_min_1997', '2441.6', 34);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (856, 'price_logic_max_1997', '6104', 34);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (857, 'price_fraud_1998', '1791', 34);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (858, 'price_logic_min_1998', '2865.6', 34);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (859, 'price_logic_max_1998', '7164', 34);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (860, 'price_fraud_1999', '2001.5', 34);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (861, 'price_logic_min_1999', '3202.4', 34);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (862, 'price_logic_max_1999', '8006', 34);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (863, 'price_fraud_2000', '2689', 34);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (864, 'price_logic_min_2000', '4302.4', 34);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (865, 'price_logic_max_2000', '10756', 34);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (866, 'price_fraud_2001', '3090.5', 34);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (867, 'price_logic_min_2001', '4944.8', 34);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (868, 'price_logic_max_2001', '12362', 34);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (869, 'price_fraud_2002', '3618', 34);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (870, 'price_logic_min_2002', '5788.8', 34);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (871, 'price_logic_max_2002', '14472', 34);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (872, 'price_fraud_2003', '4496.5', 34);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (873, 'price_logic_min_2003', '7194.4', 34);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (874, 'price_logic_max_2003', '17986', 34);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (875, 'price_fraud_2004', '5294', 34);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (876, 'price_logic_min_2004', '8470.4', 34);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (877, 'price_logic_max_2004', '21176', 34);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (878, 'price_fraud_2005', '6196.5', 34);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (879, 'price_logic_min_2005', '9914.4', 34);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (880, 'price_logic_max_2005', '24786', 34);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (881, 'price_fraud_2006', '7274.5', 34);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (882, 'price_logic_min_2006', '11639.2', 34);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (883, 'price_logic_max_2006', '29098', 34);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (884, 'price_fraud_2007', '8958', 34);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (885, 'price_logic_min_2007', '14332.8', 34);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (886, 'price_logic_max_2007', '35832', 34);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (887, 'price_fraud_2008', '9717.5', 34);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (888, 'price_logic_min_2008', '15548', 34);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (889, 'price_logic_max_2008', '38870', 34);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (890, 'price_fraud_1975', '1040', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (891, 'price_logic_min_1975', '1664', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (892, 'price_logic_max_1975', '4160', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (893, 'price_fraud_1976', '552', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (894, 'price_logic_min_1976', '883.2', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (895, 'price_logic_max_1976', '2208', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (896, 'price_fraud_1977', '434.5', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (897, 'price_logic_min_1977', '695.2', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (898, 'price_logic_max_1977', '1738', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (899, 'price_fraud_1978', '508', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (900, 'price_logic_min_1978', '812.8', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (901, 'price_logic_max_1978', '2032', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (902, 'price_fraud_1979', '506.5', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (903, 'price_logic_min_1979', '810.4', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (904, 'price_logic_max_1979', '2026', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (905, 'price_fraud_1980', '585.5', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (906, 'price_logic_min_1980', '936.8', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (907, 'price_logic_max_1980', '2342', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (908, 'price_fraud_1981', '425', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (909, 'price_logic_min_1981', '680', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (910, 'price_logic_max_1981', '1700', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (911, 'price_fraud_1982', '415', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (912, 'price_logic_min_1982', '664', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (913, 'price_logic_max_1982', '1660', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (914, 'price_fraud_1983', '531.5', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (915, 'price_logic_min_1983', '850.4', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (916, 'price_logic_max_1983', '2126', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (917, 'price_fraud_1984', '463', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (918, 'price_logic_min_1984', '740.8', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (919, 'price_logic_max_1984', '1852', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (920, 'price_fraud_1985', '393.5', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (921, 'price_logic_min_1985', '629.6', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (922, 'price_logic_max_1985', '1574', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (923, 'price_fraud_1986', '383', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (924, 'price_logic_min_1986', '612.8', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (925, 'price_logic_max_1986', '1532', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (926, 'price_fraud_1987', '413', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (927, 'price_logic_min_1987', '660.8', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (928, 'price_logic_max_1987', '1652', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (929, 'price_fraud_1988', '413.5', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (930, 'price_logic_min_1988', '661.6', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (931, 'price_logic_max_1988', '1654', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (932, 'price_fraud_1989', '408.5', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (933, 'price_logic_min_1989', '653.6', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (934, 'price_logic_max_1989', '1634', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (935, 'price_fraud_1990', '472', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (936, 'price_logic_min_1990', '755.2', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (937, 'price_logic_max_1990', '1888', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (938, 'price_fraud_1991', '550.5', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (939, 'price_logic_min_1991', '880.8', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (940, 'price_logic_max_1991', '2202', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (941, 'price_fraud_1992', '628.5', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (942, 'price_logic_min_1992', '1005.6', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (943, 'price_logic_max_1992', '2514', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (944, 'price_fraud_1993', '751.5', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (945, 'price_logic_min_1993', '1202.4', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (946, 'price_logic_max_1993', '3006', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (947, 'price_fraud_1994', '886', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (948, 'price_logic_min_1994', '1417.6', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (949, 'price_logic_max_1994', '3544', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (950, 'price_fraud_1995', '1025', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (951, 'price_logic_min_1995', '1640', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (952, 'price_logic_max_1995', '4100', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (953, 'price_fraud_1996', '1200.5', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (954, 'price_logic_min_1996', '1920.8', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (955, 'price_logic_max_1996', '4802', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (956, 'price_fraud_1997', '1526.5', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (957, 'price_logic_min_1997', '2442.4', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (958, 'price_logic_max_1997', '6106', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (959, 'price_fraud_1998', '1923.5', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (960, 'price_logic_min_1998', '3077.6', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (961, 'price_logic_max_1998', '7694', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (962, 'price_fraud_1999', '2253.5', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (963, 'price_logic_min_1999', '3605.6', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (964, 'price_logic_max_1999', '9014', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (965, 'price_fraud_2000', '2633.5', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (966, 'price_logic_min_2000', '4213.6', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (967, 'price_logic_max_2000', '10534', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (968, 'price_fraud_2001', '3158', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (969, 'price_logic_min_2001', '5052.8', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (970, 'price_logic_max_2001', '12632', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (971, 'price_fraud_2002', '3611', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (972, 'price_logic_min_2002', '5777.6', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (973, 'price_logic_max_2002', '14444', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (974, 'price_fraud_2003', '4475', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (975, 'price_logic_min_2003', '7160', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (976, 'price_logic_max_2003', '17900', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (977, 'price_fraud_2004', '5082.5', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (978, 'price_logic_min_2004', '8132', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (979, 'price_logic_max_2004', '20330', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (980, 'price_fraud_2005', '5637', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (981, 'price_logic_min_2005', '9019.2', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (982, 'price_logic_max_2005', '22548', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (983, 'price_fraud_2006', '6450', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (984, 'price_logic_min_2006', '10320', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (985, 'price_logic_max_2006', '25800', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (986, 'price_fraud_2007', '7576', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (987, 'price_logic_min_2007', '12121.6', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (988, 'price_logic_max_2007', '30304', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (989, 'price_fraud_2008', '8842.5', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (990, 'price_logic_min_2008', '14148', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (991, 'price_logic_max_2008', '35370', 28);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (992, 'price_fraud_1987', '750', 35);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (993, 'price_logic_min_1987', '1200', 35);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (994, 'price_logic_max_1987', '3000', 35);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (995, 'price_fraud_1989', '750', 35);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (996, 'price_logic_min_1989', '1200', 35);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (997, 'price_logic_max_1989', '3000', 35);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (998, 'price_fraud_1991', '350', 35);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (999, 'price_logic_min_1991', '560', 35);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1000, 'price_logic_max_1991', '1400', 35);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1001, 'price_fraud_1993', '775', 35);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1002, 'price_logic_min_1993', '1240', 35);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1003, 'price_logic_max_1993', '3100', 35);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1004, 'price_fraud_1994', '856', 35);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1005, 'price_logic_min_1994', '1369.6', 35);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1006, 'price_logic_max_1994', '3424', 35);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1007, 'price_fraud_1995', '699', 35);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1008, 'price_logic_min_1995', '1118.4', 35);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1009, 'price_logic_max_1995', '2796', 35);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1010, 'price_fraud_1996', '825', 35);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1011, 'price_logic_min_1996', '1320', 35);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1012, 'price_logic_max_1996', '3300', 35);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1013, 'price_fraud_1997', '1232.5', 35);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1014, 'price_logic_min_1997', '1972', 35);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1015, 'price_logic_max_1997', '4930', 35);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1016, 'price_fraud_1998', '1326', 35);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1017, 'price_logic_min_1998', '2121.6', 35);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1018, 'price_logic_max_1998', '5304', 35);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1019, 'price_fraud_1999', '1411', 35);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1020, 'price_logic_min_1999', '2257.6', 35);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1021, 'price_logic_max_1999', '5644', 35);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1022, 'price_fraud_2000', '2046', 35);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1023, 'price_logic_min_2000', '3273.6', 35);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1024, 'price_logic_max_2000', '8184', 35);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1025, 'price_fraud_2001', '2396', 35);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1026, 'price_logic_min_2001', '3833.6', 35);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1027, 'price_logic_max_2001', '9584', 35);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1028, 'price_fraud_2002', '2957.5', 35);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1029, 'price_logic_min_2002', '4732', 35);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1030, 'price_logic_max_2002', '11830', 35);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1031, 'price_fraud_2003', '3562', 35);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1032, 'price_logic_min_2003', '5699.2', 35);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1033, 'price_logic_max_2003', '14248', 35);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1034, 'price_fraud_2004', '3935', 35);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1035, 'price_logic_min_2004', '6296', 35);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1036, 'price_logic_max_2004', '15740', 35);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1037, 'price_fraud_2005', '4332', 35);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1038, 'price_logic_min_2005', '6931.2', 35);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1039, 'price_logic_max_2005', '17328', 35);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1040, 'price_fraud_2006', '4738.5', 35);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1041, 'price_logic_min_2006', '7581.6', 35);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1042, 'price_logic_max_2006', '18954', 35);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1043, 'price_fraud_2007', '5379', 35);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1044, 'price_logic_min_2007', '8606.4', 35);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1045, 'price_logic_max_2007', '21516', 35);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1046, 'price_fraud_2008', '5580.5', 35);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1047, 'price_logic_min_2008', '8928.8', 35);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1048, 'price_logic_max_2008', '22322', 35);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1049, 'price_fraud_2002', '3400', 51);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1050, 'price_logic_min_2002', '5440', 51);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1051, 'price_logic_max_2002', '13600', 51);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1052, 'price_fraud_2003', '3200', 51);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1053, 'price_logic_min_2003', '5120', 51);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1054, 'price_logic_max_2003', '12800', 51);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1055, 'price_fraud_2004', '3250', 51);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1056, 'price_logic_min_2004', '5200', 51);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1057, 'price_logic_max_2004', '13000', 51);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1058, 'price_fraud_2005', '3350', 51);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1059, 'price_logic_min_2005', '5360', 51);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1060, 'price_logic_max_2005', '13400', 51);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1061, 'price_fraud_1984', '1500', 36);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1062, 'price_logic_min_1984', '2400', 36);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1063, 'price_logic_max_1984', '6000', 36);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1064, 'price_fraud_1985', '300', 36);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1065, 'price_logic_min_1985', '480', 36);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1066, 'price_logic_max_1985', '1200', 36);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1067, 'price_fraud_1986', '250', 36);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1068, 'price_logic_min_1986', '400', 36);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1069, 'price_logic_max_1986', '1000', 36);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1070, 'price_fraud_1987', '418.5', 36);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1071, 'price_logic_min_1987', '669.6', 36);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1072, 'price_logic_max_1987', '1674', 36);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1073, 'price_fraud_1988', '246.5', 36);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1074, 'price_logic_min_1988', '394.4', 36);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1075, 'price_logic_max_1988', '986', 36);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1076, 'price_fraud_1989', '217.5', 36);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1077, 'price_logic_min_1989', '348', 36);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1078, 'price_logic_max_1989', '870', 36);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1079, 'price_fraud_1990', '385', 36);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1080, 'price_logic_min_1990', '616', 36);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1081, 'price_logic_max_1990', '1540', 36);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1082, 'price_fraud_1991', '365.5', 36);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1083, 'price_logic_min_1991', '584.8', 36);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1084, 'price_logic_max_1991', '1462', 36);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1085, 'price_fraud_1992', '418.5', 36);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1086, 'price_logic_min_1992', '669.6', 36);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1087, 'price_logic_max_1992', '1674', 36);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1088, 'price_fraud_1993', '419', 36);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1089, 'price_logic_min_1993', '670.4', 36);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1090, 'price_logic_max_1993', '1676', 36);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1091, 'price_fraud_1994', '684.5', 36);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1092, 'price_logic_min_1994', '1095.2', 36);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1093, 'price_logic_max_1994', '2738', 36);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1094, 'price_fraud_1995', '826.5', 36);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1095, 'price_logic_min_1995', '1322.4', 36);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1096, 'price_logic_max_1995', '3306', 36);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1097, 'price_fraud_1996', '813.5', 36);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1098, 'price_logic_min_1996', '1301.6', 36);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1099, 'price_logic_max_1996', '3254', 36);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1100, 'price_fraud_1997', '1050.5', 36);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1101, 'price_logic_min_1997', '1680.8', 36);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1102, 'price_logic_max_1997', '4202', 36);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1103, 'price_fraud_1998', '1403', 36);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1104, 'price_logic_min_1998', '2244.8', 36);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1105, 'price_logic_max_1998', '5612', 36);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1106, 'price_fraud_1999', '1567', 36);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1107, 'price_logic_min_1999', '2507.2', 36);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1108, 'price_logic_max_1999', '6268', 36);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1109, 'price_fraud_2000', '2024.5', 36);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1110, 'price_logic_min_2000', '3239.2', 36);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1111, 'price_logic_max_2000', '8098', 36);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1112, 'price_fraud_2001', '2379', 36);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1113, 'price_logic_min_2001', '3806.4', 36);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1114, 'price_logic_max_2001', '9516', 36);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1115, 'price_fraud_2002', '3110.5', 36);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1116, 'price_logic_min_2002', '4976.8', 36);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1117, 'price_logic_max_2002', '12442', 36);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1118, 'price_fraud_2003', '3624', 36);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1119, 'price_logic_min_2003', '5798.4', 36);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1120, 'price_logic_max_2003', '14496', 36);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1121, 'price_fraud_2004', '3941', 36);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1122, 'price_logic_min_2004', '6305.6', 36);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1123, 'price_logic_max_2004', '15764', 36);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1124, 'price_fraud_2005', '4407', 36);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1125, 'price_logic_min_2005', '7051.2', 36);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1126, 'price_logic_max_2005', '17628', 36);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1127, 'price_fraud_2006', '5228', 36);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1128, 'price_logic_min_2006', '8364.8', 36);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1129, 'price_logic_max_2006', '20912', 36);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1130, 'price_fraud_2007', '6039', 36);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1131, 'price_logic_min_2007', '9662.4', 36);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1132, 'price_logic_max_2007', '24156', 36);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1133, 'price_fraud_2008', '7001', 36);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1134, 'price_logic_min_2008', '11201.6', 36);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1135, 'price_logic_max_2008', '28004', 36);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1136, 'price_fraud_2005', '5950', 52);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1137, 'price_logic_min_2005', '9520', 52);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1138, 'price_logic_max_2005', '23800', 52);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1139, 'price_fraud_2006', '6500', 52);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1140, 'price_logic_min_2006', '10400', 52);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1141, 'price_logic_max_2006', '26000', 52);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1142, 'price_fraud_2007', '7203', 52);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1143, 'price_logic_min_2007', '11524.8', 52);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1144, 'price_logic_max_2007', '28812', 52);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1145, 'price_fraud_2008', '9750', 52);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1146, 'price_logic_min_2008', '15600', 52);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1147, 'price_logic_max_2008', '39000', 52);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1148, 'price_fraud_2005', '6987.5', 53);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1149, 'price_logic_min_2005', '11180', 53);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1150, 'price_logic_max_2005', '27950', 53);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1151, 'price_fraud_2006', '7982.5', 53);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1152, 'price_logic_min_2006', '12772', 53);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1153, 'price_logic_max_2006', '31930', 53);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1154, 'price_fraud_2007', '8383.5', 53);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1155, 'price_logic_min_2007', '13413.6', 53);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1156, 'price_logic_max_2007', '33534', 53);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1157, 'price_fraud_2008', '10250', 53);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1158, 'price_logic_min_2008', '16400', 53);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1159, 'price_logic_max_2008', '41000', 53);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1160, 'price_fraud_2000', '2980', 54);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1161, 'price_logic_min_2000', '4768', 54);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1162, 'price_logic_max_2000', '11920', 54);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1163, 'price_fraud_2001', '3241.5', 54);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1164, 'price_logic_min_2001', '5186.4', 54);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1165, 'price_logic_max_2001', '12966', 54);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1166, 'price_fraud_2002', '3950', 54);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1167, 'price_logic_min_2002', '6320', 54);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1168, 'price_logic_max_2002', '15800', 54);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1169, 'price_fraud_2003', '4100', 54);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1170, 'price_logic_min_2003', '6560', 54);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1171, 'price_logic_max_2003', '16400', 54);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1172, 'price_fraud_2004', '5033', 54);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1173, 'price_logic_min_2004', '8052.8', 54);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1174, 'price_logic_max_2004', '20132', 54);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1175, 'price_fraud_2002', '5250', 55);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1176, 'price_logic_min_2002', '8400', 55);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1177, 'price_logic_max_2002', '21000', 55);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1178, 'price_fraud_2003', '5750', 55);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1179, 'price_logic_min_2003', '9200', 55);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1180, 'price_logic_max_2003', '23000', 55);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1181, 'price_fraud_2004', '7014', 55);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1182, 'price_logic_min_2004', '11222.4', 55);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1183, 'price_logic_max_2004', '28056', 55);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1184, 'price_fraud_2005', '7406', 55);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1185, 'price_logic_min_2005', '11849.6', 55);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1186, 'price_logic_max_2005', '29624', 55);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1187, 'price_fraud_1999', '2250', 37);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1188, 'price_logic_min_1999', '3600', 37);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1189, 'price_logic_max_1999', '9000', 37);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1190, 'price_fraud_2000', '3055.5', 37);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1191, 'price_logic_min_2000', '4888.8', 37);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1192, 'price_logic_max_2000', '12222', 37);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1193, 'price_fraud_2001', '3446', 37);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1194, 'price_logic_min_2001', '5513.6', 37);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1195, 'price_logic_max_2001', '13784', 37);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1196, 'price_fraud_2002', '4447.5', 37);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1197, 'price_logic_min_2002', '7116', 37);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1198, 'price_logic_max_2002', '17790', 37);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1199, 'price_fraud_2003', '5235.5', 37);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1200, 'price_logic_min_2003', '8376.8', 37);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1201, 'price_logic_max_2003', '20942', 37);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1202, 'price_fraud_2004', '5725.5', 37);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1203, 'price_logic_min_2004', '9160.8', 37);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1204, 'price_logic_max_2004', '22902', 37);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1205, 'price_fraud_2005', '6721', 37);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1206, 'price_logic_min_2005', '10753.6', 37);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1207, 'price_logic_max_2005', '26884', 37);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1208, 'price_fraud_2006', '7476.5', 37);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1209, 'price_logic_min_2006', '11962.4', 37);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1210, 'price_logic_max_2006', '29906', 37);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1211, 'price_fraud_2007', '8677', 37);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1212, 'price_logic_min_2007', '13883.2', 37);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1213, 'price_logic_max_2007', '34708', 37);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1214, 'price_fraud_2008', '10337.5', 37);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1215, 'price_logic_min_2008', '16540', 37);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1216, 'price_logic_max_2008', '41350', 37);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1217, 'price_fraud_1983', '300', 29);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1218, 'price_logic_min_1983', '480', 29);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1219, 'price_logic_max_1983', '1200', 29);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1220, 'price_fraud_1984', '1500', 29);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1221, 'price_logic_min_1984', '2400', 29);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1222, 'price_logic_max_1984', '6000', 29);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1223, 'price_fraud_1985', '300', 29);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1224, 'price_logic_min_1985', '480', 29);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1225, 'price_logic_max_1985', '1200', 29);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1226, 'price_fraud_1986', '290', 29);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1227, 'price_logic_min_1986', '464', 29);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1228, 'price_logic_max_1986', '1160', 29);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1229, 'price_fraud_1987', '458', 29);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1230, 'price_logic_min_1987', '732.8', 29);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1231, 'price_logic_max_1987', '1832', 29);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1232, 'price_fraud_1988', '301.5', 29);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1233, 'price_logic_min_1988', '482.4', 29);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1234, 'price_logic_max_1988', '1206', 29);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1235, 'price_fraud_1989', '281', 29);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1236, 'price_logic_min_1989', '449.6', 29);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1237, 'price_logic_max_1989', '1124', 29);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1238, 'price_fraud_1990', '345', 29);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1239, 'price_logic_min_1990', '552', 29);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1240, 'price_logic_max_1990', '1380', 29);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1241, 'price_fraud_1991', '339', 29);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1242, 'price_logic_min_1991', '542.4', 29);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1243, 'price_logic_max_1991', '1356', 29);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1244, 'price_fraud_1992', '478', 29);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1245, 'price_logic_min_1992', '764.8', 29);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1246, 'price_logic_max_1992', '1912', 29);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1247, 'price_fraud_1993', '470', 29);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1248, 'price_logic_min_1993', '752', 29);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1249, 'price_logic_max_1993', '1880', 29);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1250, 'price_fraud_1994', '692.5', 29);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1251, 'price_logic_min_1994', '1108', 29);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1252, 'price_logic_max_1994', '2770', 29);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1253, 'price_fraud_1995', '758.5', 29);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1254, 'price_logic_min_1995', '1213.6', 29);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1255, 'price_logic_max_1995', '3034', 29);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1256, 'price_fraud_1996', '837', 29);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1257, 'price_logic_min_1996', '1339.2', 29);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1258, 'price_logic_max_1996', '3348', 29);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1259, 'price_fraud_1997', '1160', 29);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1260, 'price_logic_min_1997', '1856', 29);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1261, 'price_logic_max_1997', '4640', 29);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1262, 'price_fraud_1998', '1414.5', 29);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1263, 'price_logic_min_1998', '2263.2', 29);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1264, 'price_logic_max_1998', '5658', 29);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1265, 'price_fraud_1999', '1672.5', 29);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1266, 'price_logic_min_1999', '2676', 29);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1267, 'price_logic_max_1999', '6690', 29);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1268, 'price_fraud_2000', '2244.5', 29);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1269, 'price_logic_min_2000', '3591.2', 29);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1270, 'price_logic_max_2000', '8978', 29);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1271, 'price_fraud_2001', '2835', 29);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1272, 'price_logic_min_2001', '4536', 29);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1273, 'price_logic_max_2001', '11340', 29);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1274, 'price_fraud_2002', '3760', 29);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1275, 'price_logic_min_2002', '6016', 29);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1276, 'price_logic_max_2002', '15040', 29);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1277, 'price_fraud_2003', '4309.5', 29);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1278, 'price_logic_min_2003', '6895.2', 29);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1279, 'price_logic_max_2003', '17238', 29);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1280, 'price_fraud_2004', '4872.5', 29);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1281, 'price_logic_min_2004', '7796', 29);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1282, 'price_logic_max_2004', '19490', 29);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1283, 'price_fraud_2005', '5702.5', 29);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1284, 'price_logic_min_2005', '9124', 29);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1285, 'price_logic_max_2005', '22810', 29);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1286, 'price_fraud_2006', '6530', 29);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1287, 'price_logic_min_2006', '10448', 29);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1288, 'price_logic_max_2006', '26120', 29);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1289, 'price_fraud_2007', '7412.5', 29);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1290, 'price_logic_min_2007', '11860', 29);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1291, 'price_logic_max_2007', '29650', 29);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1292, 'price_fraud_2008', '8648', 29);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1293, 'price_logic_min_2008', '13836.8', 29);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (1294, 'price_logic_max_2008', '34592', 29);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (10001, 'name', 'rhone alpes', 2001);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (10002, 'name', 'ain', 2002);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (10003, 'name', 'ile de france', 2006);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (10004, 'name', 'paris', 2007);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (10005, 'is_coast', '0', 2001);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (10006, 'is_coast', '0', 2006);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (10007, 'is_mountain', '0', 2001);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (10008, 'is_mountain', '1', 2006);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (10009, 'price_fraud_cat_1080_s', '738.23986261018', 2001);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (10010, 'price_logic_min_cat_1080_s', '1181.1837801763', 2001);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (10011, 'price_logic_max_cat_1080_s', '2952.9594504407', 2001);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (10012, 'price_fraud_cat_1080_s', '739.23986261018', 2006);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (10013, 'price_logic_min_cat_1080_s', '1182.1837801763', 2006);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (10014, 'price_logic_max_cat_1080_s', '2953.9594504407', 2006);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (10015, 'price_fraud_cat_1020_s', '1154.3440838249', 2001);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (10016, 'price_logic_min_cat_1020_s', '1846.9505341198', 2001);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (10017, 'price_logic_max_cat_1020_s', '4617.3763352994', 2001);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (10018, 'price_fraud_cat_1020_s', '1155.3440838249', 2006);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (10019, 'price_logic_min_cat_1020_s', '1847.9505341198', 2006);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (10020, 'price_logic_max_cat_1020_s', '4618.3763352994', 2006);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (10021, 'price_fraud_cat_10_s', '3', 2001);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (10022, 'price_logic_min_cat_10_s', '4.8', 2001);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (10023, 'price_logic_max_cat_10_s', '12', 2001);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (10024, 'price_fraud_cat_10_s', '4', 2006);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (10025, 'price_logic_min_cat_10_s', '5.8', 2006);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (10026, 'price_logic_max_cat_10_s', '13', 2006);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (10027, 'price_fraud_cat_11_s', '1.5', 2001);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (10028, 'price_logic_min_cat_11_s', '2.4', 2001);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (10029, 'price_logic_max_cat_11_s', '6', 2001);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (10030, 'price_fraud_cat_11_s', '2.5', 2006);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (10031, 'price_logic_min_cat_11_s', '3.4', 2006);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (10032, 'price_logic_max_cat_11_s', '7', 2006);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (10033, 'price_fraud_cat_12_s', '1.8', 2001);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (10034, 'price_logic_min_cat_12_s', '2.88', 2001);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (10035, 'price_logic_max_cat_12_s', '7.2', 2001);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (10036, 'price_fraud_cat_12_s', '2.8', 2006);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (10037, 'price_logic_min_cat_12_s', '3.88', 2006);
INSERT INTO iteminfo_data (data_id, name, value, item_id) VALUES (10038, 'price_logic_max_cat_12_s', '8.2', 2006);


--
-- Name: list_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('list_id_seq', 8000055, true);


--
-- Data for Name: mail_log; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Name: mail_log_mail_log_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('mail_log_mail_log_id_seq', 1, false);


--
-- Data for Name: mail_queue; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Name: mail_queue_mail_queue_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('mail_queue_mail_queue_id_seq', 1, false);


--
-- Data for Name: mama_attribute_wordlists; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Data for Name: mama_attribute_categories; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Data for Name: mama_main_backup; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Data for Name: mama_attribute_categories_backup; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Name: mama_attribute_wordlists_attribute_wordlist_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('mama_attribute_wordlists_attribute_wordlist_id_seq', 1, false);


--
-- Data for Name: mama_attribute_wordlists_backup; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Data for Name: mama_attribute_words; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Data for Name: mama_attribute_words_backup; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Name: mama_attribute_words_word_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('mama_attribute_words_word_id_seq', 1, false);


--
-- Data for Name: mama_exception_lists; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Data for Name: mama_exception_lists_backup; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Name: mama_exception_lists_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('mama_exception_lists_id_seq', 1, false);


--
-- Data for Name: mama_exception_words; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Data for Name: mama_exception_words_backup; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Name: mama_exception_words_exception_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('mama_exception_words_exception_id_seq', 1, false);


--
-- Name: mama_main_backup_backup_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('mama_main_backup_backup_id_seq', 1, false);


--
-- Data for Name: mama_wordlists; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Data for Name: mama_wordlists_backup; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Name: mama_wordlists_wordlist_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('mama_wordlists_wordlist_id_seq', 1, false);


--
-- Data for Name: mama_words; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Data for Name: mama_words_backup; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Name: mama_words_word_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('mama_words_word_id_seq', 1, false);


--
-- Data for Name: most_popular_ads; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Name: most_popular_ads_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('most_popular_ads_id_seq', 1, false);


--
-- Name: next_image_id; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('next_image_id', 1, false);


--
-- Data for Name: notices; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Name: notices_notice_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('notices_notice_id_seq', 1, false);


--
-- Data for Name: on_call; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Data for Name: on_call_actions; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Name: on_call_actions_action_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('on_call_actions_action_id_seq', 1, false);


--
-- Name: on_call_on_call_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('on_call_on_call_id_seq', 1, false);


--
-- Name: order_id_suffix_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('order_id_suffix_seq', 1, false);


--
-- Data for Name: pageviews_per_reg_cat; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Name: pay_code_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('pay_code_seq', 112, true);


--
-- Data for Name: pay_log; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (0, 'adminclear', 0, '59876', 99999917, '2006-04-06 15:19:46', 50, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (1, 'save', 0, '112200000', NULL, '2014-05-23 10:32:45', 60, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (2, 'verify', 0, '112200000', NULL, '2014-05-23 10:32:45', 60, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (3, 'save', 0, '112200001', NULL, '2014-05-23 10:34:31', 61, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (4, 'verify', 0, '112200001', NULL, '2014-05-23 10:34:31', 61, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (5, 'save', 0, '112200002', NULL, '2014-05-23 10:36:37', 62, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (6, 'verify', 0, '112200002', NULL, '2014-05-23 10:36:37', 62, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (7, 'save', 0, '112200003', NULL, '2014-05-23 10:37:58', 63, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (8, 'verify', 0, '112200003', NULL, '2014-05-23 10:37:59', 63, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (9, 'save', 0, '112200004', NULL, '2014-05-23 10:53:16', 64, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (10, 'verify', 0, '112200004', NULL, '2014-05-23 10:53:16', 64, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (11, 'save', 0, '112200005', NULL, '2014-05-23 10:53:58', 65, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (12, 'verify', 0, '112200005', NULL, '2014-05-23 10:53:58', 65, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (13, 'save', 0, '112200006', NULL, '2014-05-23 10:55:29', 66, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (14, 'verify', 0, '112200006', NULL, '2014-05-23 10:55:29', 66, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (15, 'save', 0, '112200007', NULL, '2014-05-23 10:56:08', 67, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (16, 'verify', 0, '112200007', NULL, '2014-05-23 10:56:08', 67, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (17, 'save', 0, '112200008', NULL, '2014-05-23 10:57:50', 68, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (18, 'verify', 0, '112200008', NULL, '2014-05-23 10:57:50', 68, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (19, 'save', 0, '112200009', NULL, '2014-05-23 10:58:44', 69, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (20, 'verify', 0, '112200009', NULL, '2014-05-23 10:58:44', 69, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (21, 'save', 0, '235392023', NULL, '2014-05-23 10:59:25', 70, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (22, 'verify', 0, '235392023', NULL, '2014-05-23 10:59:25', 70, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (23, 'save', 0, '370784046', NULL, '2014-05-23 11:00:10', 71, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (24, 'verify', 0, '370784046', NULL, '2014-05-23 11:00:10', 71, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (25, 'save', 0, '506176069', NULL, '2014-05-23 11:02:59', 72, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (26, 'verify', 0, '506176069', NULL, '2014-05-23 11:02:59', 72, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (27, 'save', 0, '641568092', NULL, '2014-05-23 11:03:34', 73, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (28, 'verify', 0, '641568092', NULL, '2014-05-23 11:03:34', 73, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (29, 'save', 0, '776960115', NULL, '2014-05-23 11:03:50', 74, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (30, 'verify', 0, '776960115', NULL, '2014-05-23 11:03:50', 74, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (31, 'save', 0, '912352138', NULL, '2014-05-23 11:04:17', 75, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (32, 'verify', 0, '912352138', NULL, '2014-05-23 11:04:17', 75, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (33, 'save', 0, '147744161', NULL, '2014-05-23 11:04:17', 76, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (34, 'verify', 0, '147744161', NULL, '2014-05-23 11:04:17', 76, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (35, 'save', 0, '283136184', NULL, '2014-05-23 11:05:29', 77, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (36, 'verify', 0, '283136184', NULL, '2014-05-23 11:05:29', 77, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (37, 'save', 0, '418528207', NULL, '2014-05-23 11:05:52', 78, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (38, 'verify', 0, '418528207', NULL, '2014-05-23 11:05:52', 78, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (39, 'save', 0, '553920230', NULL, '2014-05-23 11:06:19', 79, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (40, 'verify', 0, '553920230', NULL, '2014-05-23 11:06:19', 79, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (41, 'save', 0, '689312253', NULL, '2014-05-23 11:07:45', 80, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (42, 'verify', 0, '689312253', NULL, '2014-05-23 11:07:45', 80, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (43, 'save', 0, '824704276', NULL, '2014-05-23 11:07:46', 81, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (44, 'verify', 0, '824704276', NULL, '2014-05-23 11:07:46', 81, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (45, 'save', 0, '960096299', NULL, '2014-05-23 11:09:23', 82, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (46, 'verify', 0, '960096299', NULL, '2014-05-23 11:09:23', 82, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (47, 'save', 0, '195488322', NULL, '2014-05-23 11:09:49', 83, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (48, 'verify', 0, '195488322', NULL, '2014-05-23 11:09:49', 83, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (49, 'save', 0, '330880345', NULL, '2014-05-23 11:19:19', 84, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (50, 'verify', 0, '330880345', NULL, '2014-05-23 11:19:20', 84, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (51, 'save', 0, '466272368', NULL, '2014-05-23 11:20:15', 85, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (52, 'verify', 0, '466272368', NULL, '2014-05-23 11:20:15', 85, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (53, 'save', 0, '601664391', NULL, '2014-05-23 11:20:22', 86, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (54, 'verify', 0, '601664391', NULL, '2014-05-23 11:20:22', 86, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (55, 'save', 0, '737056414', NULL, '2014-05-23 11:20:55', 87, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (56, 'verify', 0, '737056414', NULL, '2014-05-23 11:20:55', 87, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (57, 'save', 0, '872448437', NULL, '2014-05-23 11:21:17', 88, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (58, 'verify', 0, '872448437', NULL, '2014-05-23 11:21:17', 88, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (59, 'save', 0, '107840460', NULL, '2014-05-23 11:22:07', 89, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (60, 'verify', 0, '107840460', NULL, '2014-05-23 11:22:07', 89, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (61, 'save', 0, '243232483', NULL, '2014-05-23 11:22:21', 90, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (62, 'verify', 0, '243232483', NULL, '2014-05-23 11:22:21', 90, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (63, 'save', 0, '378624506', NULL, '2014-05-23 11:22:55', 91, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (64, 'verify', 0, '378624506', NULL, '2014-05-23 11:22:55', 91, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (65, 'save', 0, '514016529', NULL, '2014-05-23 11:23:42', 92, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (66, 'verify', 0, '514016529', NULL, '2014-05-23 11:23:42', 92, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (67, 'save', 0, '649408552', NULL, '2014-05-23 11:24:34', 93, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (68, 'verify', 0, '649408552', NULL, '2014-05-23 11:24:34', 93, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (69, 'save', 0, '784800575', NULL, '2014-05-23 11:26:09', 94, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (70, 'verify', 0, '784800575', NULL, '2014-05-23 11:26:09', 94, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (71, 'save', 0, '920192598', NULL, '2014-05-23 11:27:36', 95, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (72, 'verify', 0, '920192598', NULL, '2014-05-23 11:27:36', 95, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (73, 'save', 0, '155584621', NULL, '2014-05-23 11:28:24', 96, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (74, 'verify', 0, '155584621', NULL, '2014-05-23 11:28:24', 96, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (75, 'save', 0, '290976644', NULL, '2014-05-23 11:28:57', 97, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (76, 'verify', 0, '290976644', NULL, '2014-05-23 11:28:57', 97, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (77, 'save', 0, '426368667', NULL, '2014-05-23 11:29:25', 98, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (78, 'verify', 0, '426368667', NULL, '2014-05-23 11:29:25', 98, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (79, 'save', 0, '561760690', NULL, '2014-05-23 11:29:33', 99, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (80, 'verify', 0, '561760690', NULL, '2014-05-23 11:29:33', 99, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (81, 'save', 0, '697152713', NULL, '2014-05-23 11:29:55', 100, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (82, 'verify', 0, '697152713', NULL, '2014-05-23 11:29:55', 100, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (83, 'save', 0, '832544736', NULL, '2014-05-23 11:30:23', 101, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (84, 'verify', 0, '832544736', NULL, '2014-05-23 11:30:23', 101, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (85, 'save', 0, '967936759', NULL, '2014-05-23 11:30:55', 102, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (86, 'verify', 0, '967936759', NULL, '2014-05-23 11:30:55', 102, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (87, 'save', 0, '203328782', NULL, '2014-05-23 11:31:30', 103, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (88, 'verify', 0, '203328782', NULL, '2014-05-23 11:31:30', 103, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (89, 'save', 0, '338720805', NULL, '2014-05-23 11:40:13', 104, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (90, 'verify', 0, '338720805', NULL, '2014-05-23 11:40:14', 104, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (91, 'save', 0, '474112828', NULL, '2014-05-23 11:41:16', 105, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (92, 'verify', 0, '474112828', NULL, '2014-05-23 11:41:16', 105, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (93, 'save', 0, '609504851', NULL, '2014-05-23 11:42:16', 106, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (94, 'verify', 0, '609504851', NULL, '2014-05-23 11:42:17', 106, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (95, 'save', 0, '744896874', NULL, '2014-05-23 11:42:42', 107, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (96, 'verify', 0, '744896874', NULL, '2014-05-23 11:42:42', 107, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (97, 'save', 0, '880288897', NULL, '2014-05-23 11:42:48', 108, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (98, 'verify', 0, '880288897', NULL, '2014-05-23 11:42:48', 108, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (99, 'save', 0, '115680920', NULL, '2014-05-23 11:43:05', 109, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (100, 'verify', 0, '115680920', NULL, '2014-05-23 11:43:05', 109, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (101, 'save', 0, '251072943', NULL, '2014-05-23 11:43:36', 110, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (102, 'verify', 0, '251072943', NULL, '2014-05-23 11:43:36', 110, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (103, 'save', 0, '386464966', NULL, '2014-05-23 11:43:41', 111, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (104, 'verify', 0, '386464966', NULL, '2014-05-23 11:43:41', 111, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (105, 'save', 0, '521856989', NULL, '2014-05-23 11:44:19', 112, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (106, 'verify', 0, '521856989', NULL, '2014-05-23 11:44:19', 112, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (107, 'save', 0, '657249012', NULL, '2014-05-23 11:44:42', 113, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (108, 'verify', 0, '657249012', NULL, '2014-05-23 11:44:42', 113, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (109, 'save', 0, '792641035', NULL, '2014-05-23 11:48:26', 114, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (110, 'verify', 0, '792641035', NULL, '2014-05-23 11:48:26', 114, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (111, 'save', 0, '928033058', NULL, '2014-05-23 17:40:25', 115, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (112, 'verify', 0, '928033058', NULL, '2014-05-23 17:40:25', 115, 'OK');


--
-- Name: pay_log_pay_log_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('pay_log_pay_log_id_seq', 112, true);


--
-- Data for Name: pay_log_references; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (0, 0, 'remote_addr', '192.168.4.75');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (1, 0, 'adphone', '55554444');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (1, 1, 'remote_addr', '10.0.1.188');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (2, 0, 'remote_addr', '10.0.1.188');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (3, 0, 'adphone', '55554444');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (3, 1, 'remote_addr', '10.0.1.188');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (4, 0, 'remote_addr', '10.0.1.188');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (5, 0, 'adphone', '12321312');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (5, 1, 'remote_addr', '10.0.1.113');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (6, 0, 'remote_addr', '10.0.1.113');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (7, 0, 'adphone', '12321312');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (7, 1, 'remote_addr', '10.0.1.113');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (8, 0, 'remote_addr', '10.0.1.113');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (9, 0, 'adphone', '1232134');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (9, 1, 'remote_addr', '10.0.1.113');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (10, 0, 'remote_addr', '10.0.1.113');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (11, 0, 'adphone', '1232134');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (11, 1, 'remote_addr', '10.0.1.113');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (12, 0, 'remote_addr', '10.0.1.113');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (13, 0, 'adphone', '1232134');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (13, 1, 'remote_addr', '10.0.1.113');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (14, 0, 'remote_addr', '10.0.1.113');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (15, 0, 'adphone', '1232134');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (15, 1, 'remote_addr', '10.0.1.113');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (16, 0, 'remote_addr', '10.0.1.113');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (17, 0, 'adphone', '1232134');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (17, 1, 'remote_addr', '10.0.1.113');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (18, 0, 'remote_addr', '10.0.1.113');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (19, 0, 'adphone', '55554444');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (19, 1, 'remote_addr', '10.0.1.188');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (20, 0, 'remote_addr', '10.0.1.188');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (21, 0, 'adphone', '55554444');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (21, 1, 'remote_addr', '10.0.1.188');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (22, 0, 'remote_addr', '10.0.1.188');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (23, 0, 'adphone', '55554444');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (23, 1, 'remote_addr', '10.0.1.188');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (24, 0, 'remote_addr', '10.0.1.188');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (25, 0, 'adphone', '55554444');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (25, 1, 'remote_addr', '10.0.1.188');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (26, 0, 'remote_addr', '10.0.1.188');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (27, 0, 'adphone', '55554444');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (27, 1, 'remote_addr', '10.0.1.188');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (28, 0, 'remote_addr', '10.0.1.188');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (29, 0, 'adphone', '123123412');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (29, 1, 'remote_addr', '10.0.1.113');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (30, 0, 'remote_addr', '10.0.1.113');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (31, 0, 'adphone', '55554444');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (31, 1, 'remote_addr', '10.0.1.188');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (32, 0, 'remote_addr', '10.0.1.188');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (33, 0, 'adphone', '123123412');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (33, 1, 'remote_addr', '10.0.1.113');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (34, 0, 'remote_addr', '10.0.1.113');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (35, 0, 'adphone', '123123412');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (35, 1, 'remote_addr', '10.0.1.113');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (36, 0, 'remote_addr', '10.0.1.113');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (37, 0, 'adphone', '123123412');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (37, 1, 'remote_addr', '10.0.1.113');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (38, 0, 'remote_addr', '10.0.1.113');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (39, 0, 'adphone', '123123412');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (39, 1, 'remote_addr', '10.0.1.113');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (40, 0, 'remote_addr', '10.0.1.113');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (41, 0, 'adphone', '55554444');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (41, 1, 'remote_addr', '10.0.1.188');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (42, 0, 'remote_addr', '10.0.1.188');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (43, 0, 'adphone', '123123412');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (43, 1, 'remote_addr', '10.0.1.113');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (44, 0, 'remote_addr', '10.0.1.113');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (45, 0, 'adphone', '55554444');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (45, 1, 'remote_addr', '10.0.1.188');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (46, 0, 'remote_addr', '10.0.1.188');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (47, 0, 'adphone', '55554444');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (47, 1, 'remote_addr', '10.0.1.188');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (48, 0, 'remote_addr', '10.0.1.188');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (49, 0, 'adphone', '12312312');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (49, 1, 'remote_addr', '10.0.1.113');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (50, 0, 'remote_addr', '10.0.1.113');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (51, 0, 'adphone', '55554444');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (51, 1, 'remote_addr', '10.0.1.188');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (52, 0, 'remote_addr', '10.0.1.188');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (53, 0, 'adphone', '12312312');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (53, 1, 'remote_addr', '10.0.1.113');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (54, 0, 'remote_addr', '10.0.1.113');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (55, 0, 'adphone', '12312312');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (55, 1, 'remote_addr', '10.0.1.113');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (56, 0, 'remote_addr', '10.0.1.113');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (57, 0, 'adphone', '12312312');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (57, 1, 'remote_addr', '10.0.1.113');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (58, 0, 'remote_addr', '10.0.1.113');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (59, 0, 'adphone', '55554444');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (59, 1, 'remote_addr', '10.0.1.188');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (60, 0, 'remote_addr', '10.0.1.188');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (61, 0, 'adphone', '12312312');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (61, 1, 'remote_addr', '10.0.1.113');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (62, 0, 'remote_addr', '10.0.1.113');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (63, 0, 'adphone', '55554444');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (63, 1, 'remote_addr', '10.0.1.188');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (64, 0, 'remote_addr', '10.0.1.188');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (65, 0, 'adphone', '55554444');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (65, 1, 'remote_addr', '10.0.1.188');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (66, 0, 'remote_addr', '10.0.1.188');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (67, 0, 'adphone', '55554444');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (67, 1, 'remote_addr', '10.0.1.188');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (68, 0, 'remote_addr', '10.0.1.188');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (69, 0, 'adphone', '55554444');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (69, 1, 'remote_addr', '10.0.1.188');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (70, 0, 'remote_addr', '10.0.1.188');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (71, 0, 'adphone', '55554444');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (71, 1, 'remote_addr', '10.0.1.188');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (72, 0, 'remote_addr', '10.0.1.188');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (73, 0, 'adphone', '55554444');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (73, 1, 'remote_addr', '10.0.1.188');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (74, 0, 'remote_addr', '10.0.1.188');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (75, 0, 'adphone', '55554444');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (75, 1, 'remote_addr', '10.0.1.188');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (76, 0, 'remote_addr', '10.0.1.188');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (77, 0, 'adphone', '55554444');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (77, 1, 'remote_addr', '10.0.1.188');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (78, 0, 'remote_addr', '10.0.1.188');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (79, 0, 'adphone', '123123412');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (79, 1, 'remote_addr', '10.0.1.113');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (80, 0, 'remote_addr', '10.0.1.113');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (81, 0, 'adphone', '123123412');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (81, 1, 'remote_addr', '10.0.1.113');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (82, 0, 'remote_addr', '10.0.1.113');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (83, 0, 'adphone', '123123412');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (83, 1, 'remote_addr', '10.0.1.113');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (84, 0, 'remote_addr', '10.0.1.113');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (85, 0, 'adphone', '123123412');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (85, 1, 'remote_addr', '10.0.1.113');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (86, 0, 'remote_addr', '10.0.1.113');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (87, 0, 'adphone', '123123412');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (87, 1, 'remote_addr', '10.0.1.113');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (88, 0, 'remote_addr', '10.0.1.113');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (89, 0, 'adphone', '55554444');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (89, 1, 'remote_addr', '10.0.1.188');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (90, 0, 'remote_addr', '10.0.1.188');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (91, 0, 'adphone', '55554444');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (91, 1, 'remote_addr', '10.0.1.188');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (92, 0, 'remote_addr', '10.0.1.188');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (93, 0, 'adphone', '55554444');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (93, 1, 'remote_addr', '10.0.1.188');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (94, 0, 'remote_addr', '10.0.1.188');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (95, 0, 'adphone', '123123');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (95, 1, 'remote_addr', '10.0.1.113');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (96, 0, 'remote_addr', '10.0.1.113');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (97, 0, 'adphone', '55554444');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (97, 1, 'remote_addr', '10.0.1.188');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (98, 0, 'remote_addr', '10.0.1.188');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (99, 0, 'adphone', '123123');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (99, 1, 'remote_addr', '10.0.1.113');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (100, 0, 'remote_addr', '10.0.1.113');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (101, 0, 'adphone', '123123');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (101, 1, 'remote_addr', '10.0.1.113');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (102, 0, 'remote_addr', '10.0.1.113');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (103, 0, 'adphone', '55554444');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (103, 1, 'remote_addr', '10.0.1.188');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (104, 0, 'remote_addr', '10.0.1.188');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (105, 0, 'adphone', '123123');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (105, 1, 'remote_addr', '10.0.1.113');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (106, 0, 'remote_addr', '10.0.1.113');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (107, 0, 'adphone', '123123');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (107, 1, 'remote_addr', '10.0.1.113');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (108, 0, 'remote_addr', '10.0.1.113');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (109, 0, 'adphone', '55554444');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (109, 1, 'remote_addr', '10.0.1.188');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (110, 0, 'remote_addr', '10.0.1.188');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (111, 0, 'adphone', '123123412');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (111, 1, 'remote_addr', '10.0.1.113');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (112, 0, 'remote_addr', '10.0.1.113');


--
-- Name: payment_groups_payment_group_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('payment_groups_payment_group_id_seq', 115, true);


--
-- Data for Name: payments; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (0, 'ad_action', 20, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (1, 'ad_action', 20, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (2, 'ad_action', 20, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (3, 'ad_action', 20, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (50, 'ad_action', 20, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (60, 'ad_action', 0, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (61, 'ad_action', 0, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (62, 'ad_action', 0, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (63, 'ad_action', 0, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (64, 'ad_action', 0, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (65, 'ad_action', 0, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (66, 'ad_action', 0, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (67, 'ad_action', 0, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (68, 'ad_action', 0, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (69, 'ad_action', 0, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (70, 'ad_action', 0, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (71, 'ad_action', 0, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (72, 'ad_action', 0, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (73, 'ad_action', 0, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (74, 'ad_action', 0, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (75, 'ad_action', 0, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (76, 'ad_action', 0, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (77, 'ad_action', 0, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (78, 'ad_action', 0, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (79, 'ad_action', 0, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (80, 'ad_action', 0, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (81, 'ad_action', 0, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (82, 'ad_action', 0, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (83, 'ad_action', 0, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (84, 'ad_action', 0, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (85, 'ad_action', 0, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (86, 'ad_action', 0, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (87, 'ad_action', 0, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (88, 'ad_action', 0, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (89, 'ad_action', 0, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (90, 'ad_action', 0, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (91, 'ad_action', 0, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (92, 'ad_action', 0, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (93, 'ad_action', 0, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (94, 'ad_action', 0, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (95, 'ad_action', 0, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (96, 'ad_action', 0, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (97, 'ad_action', 0, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (98, 'ad_action', 0, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (99, 'ad_action', 0, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (100, 'ad_action', 0, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (101, 'ad_action', 0, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (102, 'ad_action', 0, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (103, 'ad_action', 0, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (104, 'ad_action', 0, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (105, 'ad_action', 0, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (106, 'ad_action', 0, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (107, 'ad_action', 0, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (108, 'ad_action', 0, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (109, 'ad_action', 0, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (110, 'ad_action', 0, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (111, 'ad_action', 0, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (112, 'ad_action', 0, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (113, 'ad_action', 0, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (114, 'ad_action', 0, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (115, 'ad_action', 0, 0);


--
-- Data for Name: pricelist; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO pricelist (list, item, price) VALUES ('if', 'BL1', 1295);
INSERT INTO pricelist (list, item, price) VALUES ('if', 'BL2', 1295);
INSERT INTO pricelist (list, item, price) VALUES ('if', 'BL3', 995);
INSERT INTO pricelist (list, item, price) VALUES ('if', 'BL4', 995);


--
-- Data for Name: purchase; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Data for Name: purchase_detail; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Name: purchase_detail_purchase_detail_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('purchase_detail_purchase_detail_id_seq', 1, false);


--
-- Name: purchase_purchase_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('purchase_purchase_id_seq', 1, false);


--
-- Data for Name: purchase_states; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Name: purchase_states_purchase_state_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('purchase_states_purchase_state_id_seq', 1, false);


--
-- Data for Name: redir_stats; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Name: redir_stats_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('redir_stats_id_seq', 1, false);


--
-- Name: report_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('report_id_seq', 1, false);


--
-- Name: reporter_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('reporter_id_seq', 1, false);


--
-- Data for Name: review_log; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO review_log (ad_id, action_id, admin_id, review_time, queue, action_type, action, refusal_reason_text, category) VALUES (20, 1, 5, '2014-05-23 10:40:18.736328', 'normal', 'new', 'accepted', NULL, 6200);
INSERT INTO review_log (ad_id, action_id, admin_id, review_time, queue, action_type, action, refusal_reason_text, category) VALUES (21, 1, 5, '2014-05-23 10:40:21.127437', 'normal', 'new', 'accepted', NULL, 6080);
INSERT INTO review_log (ad_id, action_id, admin_id, review_time, queue, action_type, action, refusal_reason_text, category) VALUES (22, 1, 5, '2014-05-23 10:40:28.211644', 'normal', 'new', 'accepted', NULL, 7020);
INSERT INTO review_log (ad_id, action_id, admin_id, review_time, queue, action_type, action, refusal_reason_text, category) VALUES (23, 1, 5, '2014-05-23 10:40:36.136996', 'normal', 'new', 'accepted', NULL, 8020);
INSERT INTO review_log (ad_id, action_id, admin_id, review_time, queue, action_type, action, refusal_reason_text, category) VALUES (24, 1, 5, '2014-05-23 11:00:20.258291', 'normal', 'new', 'accepted', NULL, 1020);
INSERT INTO review_log (ad_id, action_id, admin_id, review_time, queue, action_type, action, refusal_reason_text, category) VALUES (25, 1, 5, '2014-05-23 11:00:22.804056', 'normal', 'new', 'accepted', NULL, 2020);
INSERT INTO review_log (ad_id, action_id, admin_id, review_time, queue, action_type, action, refusal_reason_text, category) VALUES (26, 1, 5, '2014-05-23 11:00:31.272646', 'normal', 'new', 'accepted', NULL, 7020);
INSERT INTO review_log (ad_id, action_id, admin_id, review_time, queue, action_type, action, refusal_reason_text, category) VALUES (27, 1, 5, '2014-05-23 11:00:34.141472', 'normal', 'new', 'accepted', NULL, 1040);
INSERT INTO review_log (ad_id, action_id, admin_id, review_time, queue, action_type, action, refusal_reason_text, category) VALUES (28, 1, 5, '2014-05-23 11:00:39.332816', 'normal', 'new', 'accepted', NULL, 2060);
INSERT INTO review_log (ad_id, action_id, admin_id, review_time, queue, action_type, action, refusal_reason_text, category) VALUES (29, 1, 5, '2014-05-23 11:00:42.490199', 'normal', 'new', 'accepted', NULL, 1020);
INSERT INTO review_log (ad_id, action_id, admin_id, review_time, queue, action_type, action, refusal_reason_text, category) VALUES (30, 1, 5, '2014-05-23 11:00:46.511128', 'normal', 'new', 'accepted', NULL, 1040);
INSERT INTO review_log (ad_id, action_id, admin_id, review_time, queue, action_type, action, refusal_reason_text, category) VALUES (31, 1, 5, '2014-05-23 11:00:48.871817', 'normal', 'new', 'accepted', NULL, 1060);
INSERT INTO review_log (ad_id, action_id, admin_id, review_time, queue, action_type, action, refusal_reason_text, category) VALUES (33, 1, 5, '2014-05-23 11:04:43.312292', 'normal', 'new', 'accepted', NULL, 2080);
INSERT INTO review_log (ad_id, action_id, admin_id, review_time, queue, action_type, action, refusal_reason_text, category) VALUES (32, 1, 5, '2014-05-23 11:05:23.311501', 'normal', 'new', 'accepted', NULL, 2040);
INSERT INTO review_log (ad_id, action_id, admin_id, review_time, queue, action_type, action, refusal_reason_text, category) VALUES (34, 1, 5, '2014-05-23 11:05:26.602346', 'normal', 'new', 'accepted', NULL, 1020);
INSERT INTO review_log (ad_id, action_id, admin_id, review_time, queue, action_type, action, refusal_reason_text, category) VALUES (35, 1, 5, '2014-05-23 11:05:31.766461', 'normal', 'new', 'accepted', NULL, 2060);
INSERT INTO review_log (ad_id, action_id, admin_id, review_time, queue, action_type, action, refusal_reason_text, category) VALUES (36, 1, 5, '2014-05-23 11:05:36.404043', 'normal', 'new', 'accepted', NULL, 1040);
INSERT INTO review_log (ad_id, action_id, admin_id, review_time, queue, action_type, action, refusal_reason_text, category) VALUES (37, 1, 5, '2014-05-23 11:05:41.839998', 'normal', 'new', 'accepted', NULL, 1080);
INSERT INTO review_log (ad_id, action_id, admin_id, review_time, queue, action_type, action, refusal_reason_text, category) VALUES (38, 1, 5, '2014-05-23 11:06:12.22863', 'normal', 'new', 'accepted', NULL, 1060);
INSERT INTO review_log (ad_id, action_id, admin_id, review_time, queue, action_type, action, refusal_reason_text, category) VALUES (39, 1, 5, '2014-05-23 11:07:52.153954', 'normal', 'new', 'accepted', NULL, 1020);
INSERT INTO review_log (ad_id, action_id, admin_id, review_time, queue, action_type, action, refusal_reason_text, category) VALUES (40, 1, 5, '2014-05-23 11:07:56.397469', 'normal', 'new', 'accepted', NULL, 2120);
INSERT INTO review_log (ad_id, action_id, admin_id, review_time, queue, action_type, action, refusal_reason_text, category) VALUES (41, 1, 5, '2014-05-23 11:08:02.325334', 'normal', 'new', 'accepted', NULL, 1080);
INSERT INTO review_log (ad_id, action_id, admin_id, review_time, queue, action_type, action, refusal_reason_text, category) VALUES (42, 1, 5, '2014-05-23 11:10:15.442648', 'normal', 'new', 'accepted', NULL, 2060);
INSERT INTO review_log (ad_id, action_id, admin_id, review_time, queue, action_type, action, refusal_reason_text, category) VALUES (43, 1, 5, '2014-05-23 11:15:16.730681', 'normal', 'new', 'accepted', NULL, 2080);
INSERT INTO review_log (ad_id, action_id, admin_id, review_time, queue, action_type, action, refusal_reason_text, category) VALUES (44, 1, 5, '2014-05-23 11:20:48.013186', 'normal', 'new', 'accepted', NULL, 1020);
INSERT INTO review_log (ad_id, action_id, admin_id, review_time, queue, action_type, action, refusal_reason_text, category) VALUES (45, 1, 5, '2014-05-23 11:20:59.773138', 'normal', 'new', 'accepted', NULL, 7060);
INSERT INTO review_log (ad_id, action_id, admin_id, review_time, queue, action_type, action, refusal_reason_text, category) VALUES (46, 1, 5, '2014-05-23 11:21:05.984052', 'normal', 'new', 'accepted', NULL, 1020);
INSERT INTO review_log (ad_id, action_id, admin_id, review_time, queue, action_type, action, refusal_reason_text, category) VALUES (47, 1, 5, '2014-05-23 11:21:09.944108', 'normal', 'new', 'accepted', NULL, 1020);
INSERT INTO review_log (ad_id, action_id, admin_id, review_time, queue, action_type, action, refusal_reason_text, category) VALUES (48, 1, 5, '2014-05-23 11:22:17.904531', 'normal', 'new', 'accepted', NULL, 1020);
INSERT INTO review_log (ad_id, action_id, admin_id, review_time, queue, action_type, action, refusal_reason_text, category) VALUES (49, 1, 5, '2014-05-23 11:22:23.364362', 'normal', 'new', 'accepted', NULL, 7060);
INSERT INTO review_log (ad_id, action_id, admin_id, review_time, queue, action_type, action, refusal_reason_text, category) VALUES (50, 1, 5, '2014-05-23 11:22:28.039458', 'normal', 'new', 'accepted', NULL, 1020);
INSERT INTO review_log (ad_id, action_id, admin_id, review_time, queue, action_type, action, refusal_reason_text, category) VALUES (51, 1, 5, '2014-05-23 11:23:11.653611', 'normal', 'new', 'accepted', NULL, 7060);
INSERT INTO review_log (ad_id, action_id, admin_id, review_time, queue, action_type, action, refusal_reason_text, category) VALUES (52, 1, 5, '2014-05-23 11:24:43.416475', 'normal', 'new', 'accepted', NULL, 7060);
INSERT INTO review_log (ad_id, action_id, admin_id, review_time, queue, action_type, action, refusal_reason_text, category) VALUES (53, 1, 5, '2014-05-23 11:24:45.909116', 'normal', 'new', 'accepted', NULL, 7060);
INSERT INTO review_log (ad_id, action_id, admin_id, review_time, queue, action_type, action, refusal_reason_text, category) VALUES (54, 1, 5, '2014-05-23 11:27:44.679114', 'normal', 'new', 'accepted', NULL, 1020);
INSERT INTO review_log (ad_id, action_id, admin_id, review_time, queue, action_type, action, refusal_reason_text, category) VALUES (55, 1, 5, '2014-05-23 11:27:50.537479', 'normal', 'new', 'accepted', NULL, 1040);
INSERT INTO review_log (ad_id, action_id, admin_id, review_time, queue, action_type, action, refusal_reason_text, category) VALUES (56, 1, 5, '2014-05-23 11:29:31.913321', 'normal', 'new', 'accepted', NULL, 1060);
INSERT INTO review_log (ad_id, action_id, admin_id, review_time, queue, action_type, action, refusal_reason_text, category) VALUES (57, 1, 5, '2014-05-23 11:29:36.949231', 'normal', 'new', 'accepted', NULL, 1080);
INSERT INTO review_log (ad_id, action_id, admin_id, review_time, queue, action_type, action, refusal_reason_text, category) VALUES (58, 1, 5, '2014-05-23 11:29:43.811495', 'normal', 'new', 'accepted', NULL, 1100);
INSERT INTO review_log (ad_id, action_id, admin_id, review_time, queue, action_type, action, refusal_reason_text, category) VALUES (59, 1, 5, '2014-05-23 11:29:47.267284', 'normal', 'new', 'accepted', NULL, 2020);
INSERT INTO review_log (ad_id, action_id, admin_id, review_time, queue, action_type, action, refusal_reason_text, category) VALUES (60, 1, 5, '2014-05-23 11:34:59.239232', 'normal', 'new', 'accepted', NULL, 2060);
INSERT INTO review_log (ad_id, action_id, admin_id, review_time, queue, action_type, action, refusal_reason_text, category) VALUES (61, 1, 5, '2014-05-23 11:35:02.638209', 'normal', 'new', 'accepted', NULL, 2040);
INSERT INTO review_log (ad_id, action_id, admin_id, review_time, queue, action_type, action, refusal_reason_text, category) VALUES (62, 1, 5, '2014-05-23 11:35:09.75119', 'normal', 'new', 'accepted', NULL, 2100);
INSERT INTO review_log (ad_id, action_id, admin_id, review_time, queue, action_type, action, refusal_reason_text, category) VALUES (63, 1, 5, '2014-05-23 11:35:12.026057', 'normal', 'new', 'accepted', NULL, 2080);
INSERT INTO review_log (ad_id, action_id, admin_id, review_time, queue, action_type, action, refusal_reason_text, category) VALUES (64, 1, 5, '2014-05-23 11:41:27.073637', 'normal', 'new', 'accepted', NULL, 1120);
INSERT INTO review_log (ad_id, action_id, admin_id, review_time, queue, action_type, action, refusal_reason_text, category) VALUES (65, 1, 5, '2014-05-23 11:41:31.41624', 'normal', 'new', 'accepted', NULL, 1100);
INSERT INTO review_log (ad_id, action_id, admin_id, review_time, queue, action_type, action, refusal_reason_text, category) VALUES (66, 1, 5, '2014-05-23 11:42:52.817063', 'normal', 'new', 'accepted', NULL, 1080);
INSERT INTO review_log (ad_id, action_id, admin_id, review_time, queue, action_type, action, refusal_reason_text, category) VALUES (67, 1, 5, '2014-05-23 11:42:57.256163', 'normal', 'new', 'accepted', NULL, 1080);
INSERT INTO review_log (ad_id, action_id, admin_id, review_time, queue, action_type, action, refusal_reason_text, category) VALUES (68, 1, 5, '2014-05-23 11:43:02.534327', 'normal', 'new', 'accepted', NULL, 1060);
INSERT INTO review_log (ad_id, action_id, admin_id, review_time, queue, action_type, action, refusal_reason_text, category) VALUES (69, 1, 5, '2014-05-23 11:43:51.921979', 'normal', 'new', 'accepted', NULL, 1040);
INSERT INTO review_log (ad_id, action_id, admin_id, review_time, queue, action_type, action, refusal_reason_text, category) VALUES (70, 1, 5, '2014-05-23 11:43:57.246393', 'normal', 'new', 'accepted', NULL, 1020);
INSERT INTO review_log (ad_id, action_id, admin_id, review_time, queue, action_type, action, refusal_reason_text, category) VALUES (71, 1, 5, '2014-05-23 11:44:02.876137', 'normal', 'new', 'accepted', NULL, 1040);
INSERT INTO review_log (ad_id, action_id, admin_id, review_time, queue, action_type, action, refusal_reason_text, category) VALUES (72, 1, 5, '2014-05-23 11:44:24.999002', 'normal', 'new', 'accepted', NULL, 1100);
INSERT INTO review_log (ad_id, action_id, admin_id, review_time, queue, action_type, action, refusal_reason_text, category) VALUES (73, 1, 5, '2014-05-23 11:47:11.486736', 'normal', 'new', 'accepted', NULL, 1040);
INSERT INTO review_log (ad_id, action_id, admin_id, review_time, queue, action_type, action, refusal_reason_text, category) VALUES (74, 1, 5, '2014-05-23 11:49:51.747734', 'whitelist', 'new', 'accepted', NULL, 1020);
INSERT INTO review_log (ad_id, action_id, admin_id, review_time, queue, action_type, action, refusal_reason_text, category) VALUES (75, 1, 5, '2014-05-23 17:41:06.971216', 'normal', 'new', 'accepted', NULL, 2020);


--
-- Data for Name: sms_users; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Data for Name: sms_log; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Name: sms_log_sms_log_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('sms_log_sms_log_id_seq', 1, false);


--
-- Data for Name: watch_users; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Data for Name: watch_queries; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Data for Name: sms_log_watch; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Name: sms_users_sms_user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('sms_users_sms_user_id_seq', 1, false);


--
-- Data for Name: state_params; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (20, 1, 214, 'filter_name', 'normal');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (21, 1, 215, 'filter_name', 'normal');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (22, 1, 218, 'filter_name', 'normal');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (23, 1, 219, 'filter_name', 'normal');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (24, 1, 246, 'filter_name', 'normal');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (25, 1, 247, 'filter_name', 'normal');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (26, 1, 250, 'filter_name', 'normal');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (27, 1, 251, 'filter_name', 'normal');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (28, 1, 254, 'filter_name', 'normal');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (29, 1, 255, 'filter_name', 'normal');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (30, 1, 258, 'filter_name', 'normal');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (31, 1, 259, 'filter_name', 'normal');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (33, 1, 278, 'filter_name', 'normal');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (32, 1, 281, 'filter_name', 'normal');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (34, 1, 282, 'filter_name', 'normal');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (35, 1, 288, 'filter_name', 'normal');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (36, 1, 289, 'filter_name', 'normal');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (37, 1, 291, 'filter_name', 'normal');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (38, 1, 296, 'filter_name', 'normal');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (39, 1, 308, 'filter_name', 'normal');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (40, 1, 309, 'filter_name', 'normal');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (41, 1, 311, 'filter_name', 'normal');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (42, 1, 320, 'filter_name', 'normal');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (43, 1, 321, 'filter_name', 'normal');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (44, 1, 333, 'filter_name', 'normal');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (45, 1, 337, 'filter_name', 'normal');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (46, 1, 340, 'filter_name', 'normal');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (47, 1, 341, 'filter_name', 'normal');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (48, 1, 350, 'filter_name', 'normal');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (49, 1, 354, 'filter_name', 'normal');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (50, 1, 356, 'filter_name', 'normal');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (51, 1, 361, 'filter_name', 'normal');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (52, 1, 370, 'filter_name', 'normal');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (53, 1, 371, 'filter_name', 'normal');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (54, 1, 380, 'filter_name', 'normal');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (55, 1, 381, 'filter_name', 'normal');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (56, 1, 393, 'filter_name', 'normal');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (57, 1, 397, 'filter_name', 'normal');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (58, 1, 400, 'filter_name', 'normal');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (59, 1, 401, 'filter_name', 'normal');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (60, 1, 416, 'filter_name', 'normal');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (61, 1, 417, 'filter_name', 'normal');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (62, 1, 420, 'filter_name', 'normal');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (63, 1, 421, 'filter_name', 'normal');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (64, 1, 430, 'filter_name', 'normal');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (65, 1, 431, 'filter_name', 'normal');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (66, 1, 443, 'filter_name', 'normal');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (67, 1, 444, 'filter_name', 'normal');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (68, 1, 446, 'filter_name', 'normal');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (69, 1, 458, 'filter_name', 'normal');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (70, 1, 459, 'filter_name', 'normal');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (71, 1, 461, 'filter_name', 'normal');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (72, 1, 466, 'filter_name', 'normal');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (73, 1, 471, 'filter_name', 'normal');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (74, 1, 475, 'filter_name', 'whitelist');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (75, 1, 492, 'filter_name', 'normal');


--
-- Data for Name: stats_daily; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Data for Name: stats_daily_ad_actions; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Name: stats_daily_ad_actions_stat_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('stats_daily_ad_actions_stat_id_seq', 1, false);


--
-- Data for Name: stats_hourly; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Data for Name: store_actions; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Data for Name: store_action_states; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Name: store_action_states_state_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('store_action_states_state_id_seq', 1, false);


--
-- Data for Name: store_changes; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Name: store_image_id; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('store_image_id', 1, false);


--
-- Data for Name: store_login_tokens; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Name: store_login_tokens_store_login_token_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('store_login_tokens_store_login_token_id_seq', 1, false);


--
-- Data for Name: store_media; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Data for Name: store_params; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Data for Name: store_users; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Name: stores_store_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('stores_store_id_seq', 1, false);


--
-- Data for Name: synonyms; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (1, 3040, 'armoire', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (2, 3040, 'armoires ', 1);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (3, 3040, 'banc', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (4, 3040, 'bancs ', 3);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (5, 3040, 'banquette ', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (6, 3040, 'banquettes ', 5);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (7, 3040, 'bergere', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (8, 3040, 'bergeres', 7);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (9, 3040, 'bibliotheque ', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (10, 3040, 'biblioteque', 9);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (11, 3040, 'bibliotheques', 9);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (12, 3040, 'buffet ', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (13, 3040, 'bufet', 12);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (14, 3040, 'buffets', 12);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (15, 3040, 'bureau ', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (16, 3040, 'bureaux ', 15);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (17, 3040, 'canape', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (18, 3040, 'canaper ', 17);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (19, 3040, 'canapes ', 17);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (20, 3040, 'canapee', 17);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (21, 3040, 'cannape', 17);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (22, 3040, 'chaise ', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (23, 3040, 'chaises ', 22);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (24, 3040, 'chevet ', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (25, 3040, 'chevets ', 24);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (26, 3040, 'clic clac', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (27, 3040, 'clic-clac', 26);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (28, 3040, 'cliclac', 26);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (29, 3040, 'commode ', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (30, 3040, 'commodes ', 29);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (31, 3040, 'comode', 29);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (32, 3040, 'etagere ', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (33, 3040, 'etageres ', 32);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (34, 3040, 'fauteuil ', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (35, 3040, 'fauteil ', 34);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (36, 3040, 'fauteils ', 34);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (37, 3040, 'fauteuille ', 34);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (38, 3040, 'fauteuilles ', 34);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (39, 3040, 'fauteuils ', 34);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (40, 3040, 'matelas ', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (41, 3040, 'matelat ', 40);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (42, 3040, 'meuble ', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (43, 3040, 'meubles ', 42);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (44, 3040, 'mezzanine ', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (45, 3040, 'mezanine ', 44);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (46, 3040, 'porte-manteau', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (47, 3040, 'porte-manteaux', 46);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (48, 3040, 'porte manteau', 46);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (49, 3040, 'porte manteaux', 46);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (50, 3040, 'pouf', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (51, 3040, 'poufs ', 50);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (52, 3040, 'rangement ', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (53, 3040, 'rangements ', 52);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (54, 3040, 'rocking chair', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (55, 3040, 'rocking-chair', 54);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (56, 3040, 'siege ', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (57, 3040, 'sieges', 56);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (58, 3040, 'sommier ', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (59, 3040, 'sommiers ', 58);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (60, 3040, 'table ', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (61, 3040, 'tables ', 60);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (62, 3040, 'tabouret ', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (63, 3040, 'tabourets ', 62);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (64, 3040, 'tiroir ', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (65, 3040, 'tiroirs ', 64);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (66, 3040, 'vaisselier ', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (67, 3040, 'vaissellier ', 66);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (68, 3060, 'chauffe-eau', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (69, 3060, 'chauffe eau', 68);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (70, 3060, 'cuiseur', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (71, 3060, 'cuisseur', 70);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (72, 3060, 'cuit vapeur', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (73, 3060, 'cuit-vapeur', 72);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (74, 3060, 'gauffrer', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (75, 3060, 'gauffrier', 74);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (76, 3060, 'gaufrier', 74);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (77, 3060, 'gaziniere', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (78, 3060, 'gazinniere', 77);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (79, 3060, 'grille-pain', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (80, 3060, 'grille pain', 79);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (81, 3060, 'lave linge', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (82, 3060, 'lave-linge', 81);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (83, 3060, 'lave vaisselle', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (84, 3060, 'lave-vaisselle', 83);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (85, 3060, 'micro onde', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (86, 3060, 'micro ondes', 85);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (87, 3060, 'micro-onde', 85);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (88, 3060, 'micro-ondes', 85);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (89, 3060, 'mixer', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (90, 3060, 'mixeur', 89);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (91, 3060, 'multifonction', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (92, 3060, 'multifonctions', 91);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (93, 3060, 'plaque a gaz', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (94, 3060, 'plaque gaz', 93);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (95, 3060, 'plaque de cuisson', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (96, 3060, 'plaques de cuisson', 95);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (97, 3060, 'plaque electrique', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (98, 3060, 'plaques electriques', 97);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (99, 3060, 'presse agrumes', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (100, 3060, 'presse agrume', 99);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (101, 3060, 'refrigerateur', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (102, 3060, 'refrigirateur', 101);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (103, 3060, 'refregirateur', 101);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (104, 3060, 'seche cheveux', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (105, 3060, 'seche-cheveux', 104);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (106, 3060, 'seche linge', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (107, 3060, 'seche-linge', 106);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (108, 3060, 'table cuisson', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (109, 3060, 'table de cuisson', 108);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (110, 3060, 'vitroceramique', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (111, 3060, 'vitro-ceramique', 110);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (112, 3060, 'whirlpool', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (113, 3060, 'whirpool', 112);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (114, 3060, 'wirlpool', 112);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (115, 8020, 'bouteille de gaz', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (116, 8020, 'bouteille gaz', 115);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (117, 8020, 'bouteilles de gaz', 115);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (118, 8020, 'bouteilles gaz', 115);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (119, 8020, 'brique', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (120, 8020, 'briques', 119);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (121, 8020, 'carrelage', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (122, 8020, 'carrelages', 121);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (123, 8020, 'casier a bouteille', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (124, 8020, 'casier a bouteilles', 123);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (125, 8020, 'chauffage', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (126, 8020, 'chauffages', 125);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (127, 8020, 'chauffe eau', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (128, 8020, 'chauffe-eau', 127);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (129, 8020, 'convecteur', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (130, 8020, 'convecteurs', 129);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (131, 8020, 'cuve', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (132, 8020, 'cuves', 131);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (133, 8020, 'disjoncteur', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (134, 8020, 'disjoncteurs', 133);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (135, 8020, 'fenetre', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (136, 8020, 'fenetres', 135);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (137, 8020, 'plante', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (138, 8020, 'plantes', 137);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (139, 8020, 'porte', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (140, 8020, 'portes', 139);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (141, 8020, 'radiateur', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (142, 8020, 'radiateurs', 141);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (143, 8020, 'remblai', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (144, 8020, 'remblais', 143);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (145, 8020, 'store', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (146, 8020, 'stores', 145);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (147, 8020, 'taille haie', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (148, 8020, 'taille-haie', 147);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (149, 8020, 'taille haies', 147);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (150, 8020, 'taille-haies', 147);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (151, 8020, 'tonneau', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (152, 8020, 'tonneaux', 151);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (153, 8020, 'tuile', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (154, 8020, 'tuiles', 153);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (155, 8020, 'tuyau', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (156, 8020, 'tuyaux', 155);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (157, 8020, 'vasque', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (158, 8020, 'vasques', 157);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (159, 8020, 'volet', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (160, 8020, 'volets', 159);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (161, 8060, 'biberon', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (162, 8060, 'biberons', 161);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (163, 8060, 'chaussure', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (164, 8060, 'chaussures', 163);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (165, 8060, 'ensemble fille', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (166, 8060, 'ensemble filles', 165);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (167, 8060, 'ensembles fille', 165);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (168, 8060, 'ensembles filles', 165);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (169, 8060, 'ensemble garcon', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (170, 8060, 'ensemble garcons', 169);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (171, 8060, 'ensembles garcon', 169);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (172, 8060, 'ensembles garcons', 169);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (173, 8060, 'gigoteuse', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (174, 8060, 'gigoteuses', 173);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (175, 8060, 'landau', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (176, 8060, 'landeau', 175);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (177, 8060, 'lit parapluie', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (178, 8060, 'lit-parapluie', 177);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (179, 8060, 'pantalon', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (180, 8060, 'pantalons', 179);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (181, 8060, 'porte bebe', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (182, 8060, 'porte-bb', 181);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (183, 8060, 'porte-bebe', 181);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (184, 8060, 'poussette', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (185, 8060, 'pousette', 184);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (186, 8060, 'pyjama', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (187, 8060, 'pyjamas', 186);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (188, 8060, 'salopette', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (189, 8060, 'salopettes', 188);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (190, 8060, 'tee shirt', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (191, 8060, 'tee-shirt', 190);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (192, 8060, 'tshirt', 190);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (193, 8060, 'vertbaudet', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (194, 8060, 'vert baudet', 193);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (195, 8060, 'verbaudet', 193);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (196, 8060, 'vetement bebe', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (197, 8060, 'vetements bebe', 196);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (198, 8060, 'vetements enfant', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (199, 8060, 'vetement enfant', 198);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (200, 8060, 'vetements enfants', 198);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (201, 8060, 'vetements fille', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (202, 8060, 'vetements filles', 201);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (203, 8060, 'vetements garcon', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (204, 8060, 'vetements garcons', 203);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (205, 41, 'babyfoot', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (206, 41, 'baby-foot', 205);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (207, 41, 'baby foot', 205);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (208, 41, 'barbie', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (209, 41, 'barbies', 208);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (210, 41, 'camion de pompier', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (211, 41, 'camion pompier', 210);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (212, 41, 'domino', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (213, 41, 'dominos', 212);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (214, 41, 'dora', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (215, 41, 'dora l''exploratrice', 214);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (216, 41, 'figurine', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (217, 41, 'figurines', 216);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (218, 41, 'fisher price', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (219, 41, 'fischer price', 218);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (220, 41, 'fisherprice', 218);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (221, 41, 'fisher-price', 218);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (222, 41, 'jeu de construction', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (223, 41, 'jeux de construction', 222);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (224, 41, 'jeu de societe', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (225, 41, 'jeux de societe', 224);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (226, 41, 'jeu educatif', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (227, 41, 'jeux educatifs', 226);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (228, 41, 'jouet', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (229, 41, 'jouets', 228);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (230, 41, 'lego', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (231, 41, 'legos', 230);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (232, 41, 'peluche', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (233, 41, 'pelluche', 232);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (234, 41, 'peluches', 232);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (235, 41, 'playmobil', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (236, 41, 'playmobils', 235);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (237, 41, 'playmobile', 235);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (238, 41, 'poupee', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (239, 41, 'poupees', 238);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (240, 41, 'puzzle', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (241, 41, 'puzzles', 240);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (242, 41, 'trottinette', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (243, 41, 'trotinette', 242);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (244, 41, 'vtech', NULL);
INSERT INTO synonyms (syn_id, category, word, seme_id) VALUES (245, 41, 'v-tech', 244);


--
-- Name: synonyms_syn_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('synonyms_syn_id_seq', 245, true);


--
-- Name: tokens_token_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('tokens_token_id_seq', 147, true);


--
-- Data for Name: trans_queue; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (55, '2014-05-23 11:48:26.512676', '2089@mazaru.schibsted.cl', '2014-05-23 12:18:26.512676', '2014-05-23 12:18:29.588494', '2089@mazaru.schibsted.cl', 'TRANS_ERROR', '2014-05-23 12:18:29.535769', '2089@mazaru.schibsted.cl', 'cmd:review
commit:1
ad_id:74
action_id:1
action:accept
filter_name:whitelist
remote_addr:10.0.1.188

', '220 Welcome.
error:ERROR_INVALID_STATE
status:TRANS_ERROR
end
', 'AT', 'review', '74');
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (4, '2014-05-23 10:40:36.143523', '2089@mazaru.schibsted.cl', NULL, '2014-05-23 10:41:12.713831', '2089@mazaru.schibsted.cl', 'TRANS_OK', '2014-05-23 10:41:12.650417', NULL, 'cmd:admail
commit:1
ad_id:23
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (3, '2014-05-23 10:40:28.217987', '2089@mazaru.schibsted.cl', NULL, '2014-05-23 10:41:12.731165', '2089@mazaru.schibsted.cl', 'TRANS_OK', '2014-05-23 10:41:12.650417', NULL, 'cmd:admail
commit:1
ad_id:22
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (2, '2014-05-23 10:40:21.159008', '2089@mazaru.schibsted.cl', NULL, '2014-05-23 10:41:12.768245', '2089@mazaru.schibsted.cl', 'TRANS_OK', '2014-05-23 10:41:12.650417', NULL, 'cmd:admail
commit:1
ad_id:21
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (1, '2014-05-23 10:40:18.774926', '2089@mazaru.schibsted.cl', NULL, '2014-05-23 10:41:12.76852', '2089@mazaru.schibsted.cl', 'TRANS_OK', '2014-05-23 10:41:12.650417', NULL, 'cmd:admail
commit:1
ad_id:20
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (11, '2014-05-23 11:00:46.518707', '2089@mazaru.schibsted.cl', NULL, '2014-05-23 11:01:04.085336', '2089@mazaru.schibsted.cl', 'TRANS_OK', '2014-05-23 11:01:04.024973', NULL, 'cmd:admail
commit:1
ad_id:30
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (12, '2014-05-23 11:00:48.877224', '2089@mazaru.schibsted.cl', NULL, '2014-05-23 11:01:04.085552', '2089@mazaru.schibsted.cl', 'TRANS_OK', '2014-05-23 11:01:04.024973', NULL, 'cmd:admail
commit:1
ad_id:31
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (10, '2014-05-23 11:00:42.49534', '2089@mazaru.schibsted.cl', NULL, '2014-05-23 11:01:04.129751', '2089@mazaru.schibsted.cl', 'TRANS_OK', '2014-05-23 11:01:04.024973', NULL, 'cmd:admail
commit:1
ad_id:29
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (9, '2014-05-23 11:00:39.340222', '2089@mazaru.schibsted.cl', NULL, '2014-05-23 11:01:04.134258', '2089@mazaru.schibsted.cl', 'TRANS_OK', '2014-05-23 11:01:04.024973', NULL, 'cmd:admail
commit:1
ad_id:28
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (7, '2014-05-23 11:00:31.280758', '2089@mazaru.schibsted.cl', NULL, '2014-05-23 11:01:04.135387', '2089@mazaru.schibsted.cl', 'TRANS_OK', '2014-05-23 11:01:04.024973', NULL, 'cmd:admail
commit:1
ad_id:26
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (8, '2014-05-23 11:00:34.146382', '2089@mazaru.schibsted.cl', NULL, '2014-05-23 11:01:04.135152', '2089@mazaru.schibsted.cl', 'TRANS_OK', '2014-05-23 11:01:04.024973', NULL, 'cmd:admail
commit:1
ad_id:27
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (5, '2014-05-23 11:00:20.291738', '2089@mazaru.schibsted.cl', NULL, '2014-05-23 11:01:04.169573', '2089@mazaru.schibsted.cl', 'TRANS_OK', '2014-05-23 11:01:04.024973', NULL, 'cmd:admail
commit:1
ad_id:24
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (6, '2014-05-23 11:00:22.823062', '2089@mazaru.schibsted.cl', NULL, '2014-05-23 11:01:04.169357', '2089@mazaru.schibsted.cl', 'TRANS_OK', '2014-05-23 11:01:04.024973', NULL, 'cmd:admail
commit:1
ad_id:25
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (19, '2014-05-23 11:06:12.237852', '2089@mazaru.schibsted.cl', NULL, '2014-05-23 11:08:16.156434', '2089@mazaru.schibsted.cl', 'TRANS_OK', '2014-05-23 11:08:16.039609', NULL, 'cmd:admail
commit:1
ad_id:38
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (16, '2014-05-23 11:05:31.77469', '2089@mazaru.schibsted.cl', NULL, '2014-05-23 11:08:16.175548', '2089@mazaru.schibsted.cl', 'TRANS_OK', '2014-05-23 11:08:16.039609', NULL, 'cmd:admail
commit:1
ad_id:35
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (18, '2014-05-23 11:05:41.845461', '2089@mazaru.schibsted.cl', NULL, '2014-05-23 11:08:16.256629', '2089@mazaru.schibsted.cl', 'TRANS_OK', '2014-05-23 11:08:16.039609', NULL, 'cmd:admail
commit:1
ad_id:37
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (14, '2014-05-23 11:05:23.344113', '2089@mazaru.schibsted.cl', NULL, '2014-05-23 11:08:16.257034', '2089@mazaru.schibsted.cl', 'TRANS_OK', '2014-05-23 11:08:16.039609', NULL, 'cmd:admail
commit:1
ad_id:32
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (13, '2014-05-23 11:04:43.345142', '2089@mazaru.schibsted.cl', NULL, '2014-05-23 11:08:16.257955', '2089@mazaru.schibsted.cl', 'TRANS_OK', '2014-05-23 11:08:16.039609', NULL, 'cmd:admail
commit:1
ad_id:33
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (15, '2014-05-23 11:05:26.612621', '2089@mazaru.schibsted.cl', NULL, '2014-05-23 11:08:16.257387', '2089@mazaru.schibsted.cl', 'TRANS_OK', '2014-05-23 11:08:16.039609', NULL, 'cmd:admail
commit:1
ad_id:34
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (17, '2014-05-23 11:05:36.412916', '2089@mazaru.schibsted.cl', NULL, '2014-05-23 11:08:16.257621', '2089@mazaru.schibsted.cl', 'TRANS_OK', '2014-05-23 11:08:16.039609', NULL, 'cmd:admail
commit:1
ad_id:36
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (56, '2014-05-23 11:49:51.782969', '2089@mazaru.schibsted.cl', NULL, '2014-05-23 11:50:38.89626', '2089@mazaru.schibsted.cl', 'TRANS_OK', '2014-05-23 11:50:38.80289', NULL, 'cmd:admail
commit:1
ad_id:74
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (21, '2014-05-23 11:07:56.430796', '2089@mazaru.schibsted.cl', NULL, '2014-05-23 11:08:16.124036', '2089@mazaru.schibsted.cl', 'TRANS_OK', '2014-05-23 11:08:16.039609', NULL, 'cmd:admail
commit:1
ad_id:40
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (22, '2014-05-23 11:08:02.331066', '2089@mazaru.schibsted.cl', NULL, '2014-05-23 11:08:16.124328', '2089@mazaru.schibsted.cl', 'TRANS_OK', '2014-05-23 11:08:16.039609', NULL, 'cmd:admail
commit:1
ad_id:41
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (20, '2014-05-23 11:07:52.172493', '2089@mazaru.schibsted.cl', NULL, '2014-05-23 11:08:16.247499', '2089@mazaru.schibsted.cl', 'TRANS_OK', '2014-05-23 11:08:16.039609', NULL, 'cmd:admail
commit:1
ad_id:39
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (24, '2014-05-23 11:15:16.786763', '2089@mazaru.schibsted.cl', NULL, '2014-05-23 11:47:26.060667', '2089@mazaru.schibsted.cl', 'TRANS_OK', '2014-05-23 11:47:26.00172', NULL, 'cmd:admail
commit:1
ad_id:43
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (26, '2014-05-23 11:20:59.805412', '2089@mazaru.schibsted.cl', NULL, '2014-05-23 11:47:26.200295', '2089@mazaru.schibsted.cl', 'TRANS_OK', '2014-05-23 11:47:26.00172', NULL, 'cmd:admail
commit:1
ad_id:45
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (23, '2014-05-23 11:10:15.473402', '2089@mazaru.schibsted.cl', NULL, '2014-05-23 11:47:26.204954', '2089@mazaru.schibsted.cl', 'TRANS_OK', '2014-05-23 11:47:26.00172', NULL, 'cmd:admail
commit:1
ad_id:42
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (30, '2014-05-23 11:22:23.372361', '2089@mazaru.schibsted.cl', NULL, '2014-05-23 11:47:26.211739', '2089@mazaru.schibsted.cl', 'TRANS_OK', '2014-05-23 11:47:26.00172', NULL, 'cmd:admail
commit:1
ad_id:49
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (28, '2014-05-23 11:21:09.955212', '2089@mazaru.schibsted.cl', NULL, '2014-05-23 11:47:26.21317', '2089@mazaru.schibsted.cl', 'TRANS_OK', '2014-05-23 11:47:26.00172', NULL, 'cmd:admail
commit:1
ad_id:47
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (25, '2014-05-23 11:20:48.041832', '2089@mazaru.schibsted.cl', NULL, '2014-05-23 11:47:26.212029', '2089@mazaru.schibsted.cl', 'TRANS_OK', '2014-05-23 11:47:26.00172', NULL, 'cmd:admail
commit:1
ad_id:44
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (27, '2014-05-23 11:21:05.991527', '2089@mazaru.schibsted.cl', NULL, '2014-05-23 11:47:26.212262', '2089@mazaru.schibsted.cl', 'TRANS_OK', '2014-05-23 11:47:26.00172', NULL, 'cmd:admail
commit:1
ad_id:46
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (29, '2014-05-23 11:22:17.911296', '2089@mazaru.schibsted.cl', NULL, '2014-05-23 11:47:26.212412', '2089@mazaru.schibsted.cl', 'TRANS_OK', '2014-05-23 11:47:26.00172', NULL, 'cmd:admail
commit:1
ad_id:48
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (39, '2014-05-23 11:29:43.821761', '2089@mazaru.schibsted.cl', NULL, '2014-05-23 11:47:26.368987', '2089@mazaru.schibsted.cl', 'TRANS_OK', '2014-05-23 11:47:26.217079', NULL, 'cmd:admail
commit:1
ad_id:58
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (38, '2014-05-23 11:29:36.960063', '2089@mazaru.schibsted.cl', NULL, '2014-05-23 11:47:26.369399', '2089@mazaru.schibsted.cl', 'TRANS_OK', '2014-05-23 11:47:26.217079', NULL, 'cmd:admail
commit:1
ad_id:57
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (33, '2014-05-23 11:24:43.424138', '2089@mazaru.schibsted.cl', NULL, '2014-05-23 11:47:26.370799', '2089@mazaru.schibsted.cl', 'TRANS_OK', '2014-05-23 11:47:26.217079', NULL, 'cmd:admail
commit:1
ad_id:52
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (34, '2014-05-23 11:24:45.919737', '2089@mazaru.schibsted.cl', NULL, '2014-05-23 11:47:26.370992', '2089@mazaru.schibsted.cl', 'TRANS_OK', '2014-05-23 11:47:26.217079', NULL, 'cmd:admail
commit:1
ad_id:53
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (35, '2014-05-23 11:27:44.712401', '2089@mazaru.schibsted.cl', NULL, '2014-05-23 11:47:26.371324', '2089@mazaru.schibsted.cl', 'TRANS_OK', '2014-05-23 11:47:26.217079', NULL, 'cmd:admail
commit:1
ad_id:54
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (36, '2014-05-23 11:27:50.577965', '2089@mazaru.schibsted.cl', NULL, '2014-05-23 11:47:26.371633', '2089@mazaru.schibsted.cl', 'TRANS_OK', '2014-05-23 11:47:26.217079', NULL, 'cmd:admail
commit:1
ad_id:55
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (37, '2014-05-23 11:29:31.92328', '2089@mazaru.schibsted.cl', NULL, '2014-05-23 11:47:26.371839', '2089@mazaru.schibsted.cl', 'TRANS_OK', '2014-05-23 11:47:26.217079', NULL, 'cmd:admail
commit:1
ad_id:56
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (46, '2014-05-23 11:41:31.449888', '2089@mazaru.schibsted.cl', NULL, '2014-05-23 11:47:26.528862', '2089@mazaru.schibsted.cl', 'TRANS_OK', '2014-05-23 11:47:26.373865', NULL, 'cmd:admail
commit:1
ad_id:65
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (48, '2014-05-23 11:42:57.26493', '2089@mazaru.schibsted.cl', NULL, '2014-05-23 11:47:26.528979', '2089@mazaru.schibsted.cl', 'TRANS_OK', '2014-05-23 11:47:26.373865', NULL, 'cmd:admail
commit:1
ad_id:67
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (49, '2014-05-23 11:43:02.539379', '2089@mazaru.schibsted.cl', NULL, '2014-05-23 11:47:26.529433', '2089@mazaru.schibsted.cl', 'TRANS_OK', '2014-05-23 11:47:26.373865', NULL, 'cmd:admail
commit:1
ad_id:68
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (43, '2014-05-23 11:35:09.75637', '2089@mazaru.schibsted.cl', NULL, '2014-05-23 11:47:26.529771', '2089@mazaru.schibsted.cl', 'TRANS_OK', '2014-05-23 11:47:26.373865', NULL, 'cmd:admail
commit:1
ad_id:62
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (44, '2014-05-23 11:35:12.031727', '2089@mazaru.schibsted.cl', NULL, '2014-05-23 11:47:26.530021', '2089@mazaru.schibsted.cl', 'TRANS_OK', '2014-05-23 11:47:26.373865', NULL, 'cmd:admail
commit:1
ad_id:63
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (45, '2014-05-23 11:41:27.093277', '2089@mazaru.schibsted.cl', NULL, '2014-05-23 11:47:26.530293', '2089@mazaru.schibsted.cl', 'TRANS_OK', '2014-05-23 11:47:26.373865', NULL, 'cmd:admail
commit:1
ad_id:64
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (51, '2014-05-23 11:43:57.257002', '2089@mazaru.schibsted.cl', NULL, '2014-05-23 11:47:26.530972', '2089@mazaru.schibsted.cl', 'TRANS_OK', '2014-05-23 11:47:26.373865', NULL, 'cmd:admail
commit:1
ad_id:70
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (50, '2014-05-23 11:43:51.951712', '2089@mazaru.schibsted.cl', NULL, '2014-05-23 11:47:26.531196', '2089@mazaru.schibsted.cl', 'TRANS_OK', '2014-05-23 11:47:26.373865', NULL, 'cmd:admail
commit:1
ad_id:69
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (47, '2014-05-23 11:42:52.825859', '2089@mazaru.schibsted.cl', NULL, '2014-05-23 11:47:26.531391', '2089@mazaru.schibsted.cl', 'TRANS_OK', '2014-05-23 11:47:26.373865', NULL, 'cmd:admail
commit:1
ad_id:66
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (53, '2014-05-23 11:44:25.015856', '2089@mazaru.schibsted.cl', NULL, '2014-05-23 11:47:26.580181', '2089@mazaru.schibsted.cl', 'TRANS_OK', '2014-05-23 11:47:26.532859', NULL, 'cmd:admail
commit:1
ad_id:72
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (54, '2014-05-23 11:47:11.515436', '2089@mazaru.schibsted.cl', NULL, '2014-05-23 11:47:26.588489', '2089@mazaru.schibsted.cl', 'TRANS_OK', '2014-05-23 11:47:26.532859', NULL, 'cmd:admail
commit:1
ad_id:73
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (32, '2014-05-23 11:23:11.658484', '2089@mazaru.schibsted.cl', NULL, '2014-05-23 11:47:26.213419', '2089@mazaru.schibsted.cl', 'TRANS_OK', '2014-05-23 11:47:26.00172', NULL, 'cmd:admail
commit:1
ad_id:51
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (31, '2014-05-23 11:22:28.047621', '2089@mazaru.schibsted.cl', NULL, '2014-05-23 11:47:26.21375', '2089@mazaru.schibsted.cl', 'TRANS_OK', '2014-05-23 11:47:26.00172', NULL, 'cmd:admail
commit:1
ad_id:50
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (57, '2014-05-23 17:41:07.001848', '2089@mazaru.schibsted.cl', NULL, '2014-05-23 17:41:28.15431', '2089@mazaru.schibsted.cl', 'TRANS_OK', '2014-05-23 17:41:28.07871', NULL, 'cmd:admail
commit:1
ad_id:75
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (42, '2014-05-23 11:35:02.688143', '2089@mazaru.schibsted.cl', NULL, '2014-05-23 11:47:26.369535', '2089@mazaru.schibsted.cl', 'TRANS_OK', '2014-05-23 11:47:26.217079', NULL, 'cmd:admail
commit:1
ad_id:61
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (40, '2014-05-23 11:29:47.275919', '2089@mazaru.schibsted.cl', NULL, '2014-05-23 11:47:26.370556', '2089@mazaru.schibsted.cl', 'TRANS_OK', '2014-05-23 11:47:26.217079', NULL, 'cmd:admail
commit:1
ad_id:59
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (41, '2014-05-23 11:34:59.278412', '2089@mazaru.schibsted.cl', NULL, '2014-05-23 11:47:26.372627', '2089@mazaru.schibsted.cl', 'TRANS_OK', '2014-05-23 11:47:26.217079', NULL, 'cmd:admail
commit:1
ad_id:60
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (52, '2014-05-23 11:44:02.884782', '2089@mazaru.schibsted.cl', NULL, '2014-05-23 11:47:26.529243', '2089@mazaru.schibsted.cl', 'TRANS_OK', '2014-05-23 11:47:26.373865', NULL, 'cmd:admail
commit:1
ad_id:71
action_id:1
mail_type:accept
', '220 Welcome.
status:TRANS_OK
end
', 'accepted_mail', NULL, NULL);


--
-- Name: trans_queue_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('trans_queue_id_seq', 57, true);


--
-- Data for Name: transbank_log; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Name: transbank_log_log_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('transbank_log_log_id_seq', 1, false);


--
-- Name: uid_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('uid_seq', 51, true);


--
-- Data for Name: unfinished_ads; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Name: unfinished_ads_unf_ad_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('unfinished_ads_unf_ad_id_seq', 1, false);


--
-- Data for Name: user_params; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO user_params (user_id, name, value) VALUES (51, 'first_approved_ad', '2014-05-23 10:40:18.736328-04');
INSERT INTO user_params (user_id, name, value) VALUES (50, 'first_approved_ad', '2014-05-23 10:40:28.211644-04');
INSERT INTO user_params (user_id, name, value) VALUES (52, 'first_approved_ad', '2014-05-23 11:00:20.258291-04');
INSERT INTO user_params (user_id, name, value) VALUES (53, 'first_approved_ad', '2014-05-23 11:00:42.490199-04');
INSERT INTO user_params (user_id, name, value) VALUES (55, 'first_approved_ad', '2014-05-23 11:04:43.312292-04');
INSERT INTO user_params (user_id, name, value) VALUES (54, 'first_approved_ad', '2014-05-23 11:05:26.602346-04');
INSERT INTO user_params (user_id, name, value) VALUES (56, 'first_approved_ad', '2014-05-23 11:20:48.013186-04');
INSERT INTO user_params (user_id, name, value) VALUES (57, 'first_approved_ad', '2014-05-23 11:20:59.773138-04');
INSERT INTO user_params (user_id, name, value) VALUES (58, 'first_approved_ad', '2014-05-23 11:27:44.679114-04');
INSERT INTO user_params (user_id, name, value) VALUES (59, 'first_approved_ad', '2014-05-23 11:29:47.267284-04');
INSERT INTO user_params (user_id, name, value) VALUES (61, 'first_approved_ad', '2014-05-23 11:41:27.073637-04');
INSERT INTO user_params (user_id, name, value) VALUES (60, 'first_approved_ad', '2014-05-23 11:42:57.256163-04');

INSERT INTO user_params (user_id, name, value) VALUES (51, 'first_inserted_ad', '2014-05-23 10:40:18.736328-04');
INSERT INTO user_params (user_id, name, value) VALUES (50, 'first_inserted_ad', '2014-05-23 10:40:28.211644-04');
INSERT INTO user_params (user_id, name, value) VALUES (52, 'first_inserted_ad', '2014-05-23 11:00:20.258291-04');
INSERT INTO user_params (user_id, name, value) VALUES (53, 'first_inserted_ad', '2014-05-23 11:00:42.490199-04');
INSERT INTO user_params (user_id, name, value) VALUES (55, 'first_inserted_ad', '2014-05-23 11:04:43.312292-04');
INSERT INTO user_params (user_id, name, value) VALUES (54, 'first_inserted_ad', '2014-05-23 11:05:26.602346-04');
INSERT INTO user_params (user_id, name, value) VALUES (56, 'first_inserted_ad', '2014-05-23 11:20:48.013186-04');
INSERT INTO user_params (user_id, name, value) VALUES (57, 'first_inserted_ad', '2014-05-23 11:20:59.773138-04');
INSERT INTO user_params (user_id, name, value) VALUES (58, 'first_inserted_ad', '2014-05-23 11:27:44.679114-04');
INSERT INTO user_params (user_id, name, value) VALUES (59, 'first_inserted_ad', '2014-05-23 11:29:47.267284-04');
INSERT INTO user_params (user_id, name, value) VALUES (61, 'first_inserted_ad', '2014-05-23 11:41:27.073637-04');
INSERT INTO user_params (user_id, name, value) VALUES (60, 'first_inserted_ad', '2014-05-23 11:42:57.256163-04');

--
-- Data for Name: user_testimonial; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Name: user_testimonial_user_testimonial_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('user_testimonial_user_testimonial_id_seq', 1, false);


--
-- Name: users_user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('users_user_id_seq', 61, true);


--
-- Name: verify_code_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('verify_code_seq', 90, true);


--
-- Data for Name: visitor; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Data for Name: visits; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Data for Name: vouchers; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Data for Name: voucher_actions; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Name: voucher_actions_voucher_action_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('voucher_actions_voucher_action_id_seq', 1, false);


--
-- Data for Name: voucher_states; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Name: voucher_states_voucher_state_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('voucher_states_voucher_state_id_seq', 1, false);


--
-- Name: vouchers_voucher_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('vouchers_voucher_id_seq', 1, false);


--
-- Data for Name: watch_ads; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Name: watch_users_watch_user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('watch_users_watch_user_id_seq', 1, false);


--
-- PostgreSQL database dump complete
--

INSERT INTO ad_params VALUES (2, 'communes', '242');
INSERT INTO ad_params VALUES (3, 'communes', '238');
INSERT INTO ad_params VALUES (4, 'communes', '238');
INSERT INTO ad_params VALUES (5, 'communes', '242');
INSERT INTO ad_params VALUES (6, 'communes', '217');
INSERT INTO ad_params VALUES (7, 'communes', '232');
INSERT INTO ad_params VALUES (8, 'communes', '232');
INSERT INTO ad_params VALUES (9, 'communes', '232');
INSERT INTO ad_params VALUES (10, 'communes', '273');
INSERT INTO ad_params VALUES (11, 'communes', '283');
INSERT INTO ad_params VALUES (20, 'communes', '334');
INSERT INTO ad_params VALUES (21, 'communes', '337');
INSERT INTO ad_params VALUES (28, 'communes', '315');
INSERT INTO ad_params VALUES (22, 'communes', '333');
INSERT INTO ad_params VALUES (33, 'communes', '329');
INSERT INTO ad_params VALUES (23, 'communes', '327');
INSERT INTO ad_params VALUES (25, 'communes', '334');
INSERT INTO ad_params VALUES (26, 'communes', '338');
INSERT INTO ad_params VALUES (32, 'communes', '342');
INSERT INTO ad_params VALUES (35, 'communes', '338');
INSERT INTO ad_params VALUES (40, 'communes', '334');
INSERT INTO ad_params VALUES (42, 'communes', '308');
INSERT INTO ad_params VALUES (43, 'communes', '336');
INSERT INTO ad_params VALUES (45, 'communes', '296');
INSERT INTO ad_params VALUES (75, 'communes', '337');
INSERT INTO ad_params VALUES (49, 'communes', '311');
INSERT INTO ad_params VALUES (51, 'communes', '319');
INSERT INTO ad_params VALUES (60, 'communes', '327');
INSERT INTO ad_params VALUES (52, 'communes', '336');
INSERT INTO ad_params VALUES (53, 'communes', '336');

--Migrate inmo ads
select * from bpv.migrate_ads_inmo();

--Change all passwords to 123123123
update ads set salted_passwd =  E'$1024$2y1bQ}VYRD}zF4QCbf07a971b784888658a20e62156de64dfde5ffb6', passwd = null;
update accounts set salted_passwd =  E'$1024$2y1bQ}VYRD}zF4QCbf07a971b784888658a20e62156de64dfde5ffb6';
