--
-- SQL Script to create database users and permissions
-- -----------
--

-- App. users
-- ----------------------------
CREATE ROLE yapo;
ALTER ROLE yapo WITH NOSUPERUSER INHERIT NOCREATEROLE NOCREATEDB LOGIN PASSWORD 'passYapoApplicationPW' VALID UNTIL 'infinity';

CREATE USER slony;
ALTER ROLE slony WITH SUPERUSER NOCREATEROLE NOCREATEDB PASSWORD 'putoslony!';

CREATE USER trans;
ALTER ROLE trans WITH NOSUPERUSER INHERIT NOCREATEROLE NOCREATEDB PASSWORD 'transPass';

CREATE USER bsearch;
ALTER ROLE bsearch WITH NOSUPERUSER INHERIT NOCREATEROLE NOCREATEDB PASSWORD 'iujuyhgyt3s';

CREATE USER batch;
ALTER ROLE batch WITH NOSUPERUSER INHERIT NOCREATEROLE NOCREATEDB PASSWORD 'paSworDInt';

CREATE USER bconfd;
ALTER ROLE bconfd WITH NOSUPERUSER INHERIT NOCREATEROLE NOCREATEDB PASSWORD 'bconfdPass';

-- Lamers
-- ----------------------------
CREATE ROLE julio;
ALTER ROLE julio WITH SUPERUSER INHERIT CREATEROLE CREATEDB LOGIN PASSWORD 'md523fc6ada97f3877f3fae5711aa810fc9' VALID UNTIL 'infinity';


-- Sysadmins
-- ----------------------------
-- BhEaN
CREATE ROLE bhean;
ALTER ROLE bhean WITH SUPERUSER INHERIT CREATEROLE CREATEDB LOGIN PASSWORD 'md5993d03d85eedd531389787280a6806ea' VALID UNTIL 'infinity';


-- Permissions
-- ----------------------------
GRANT bwriters TO trans;
GRANT breaders TO trans;

GRANT bwriters TO batch;
GRANT breaders TO batch;

GRANT breaders TO bsearch;
GRANT breaders TO bconfd;
GRANT breaders TO filterd;
