#!/bin/bash

# Source function library.
INITD=/etc/rc.d/init.d
. $INITD/functions

PWD_THIS_SCRIPT=`pwd`
PWD_SQL_SCRIPTS="$PWD_THIS_SCRIPT/.."
ERROR_LOG="error.log"

DATABASE="blocketdb"
PGDATA="/opt/psql"
PORT=5432
ENCODING="ISO-8859-1"

error () {
	echo_failure
	echo
	echo "> Script DON'T succesfully finished! See the logfile: $ERROR_LOG"
	echo 
	exit -1
}

# Files needed by the script
FILES_NEEDED[0]="$PWD_THIS_SCRIPT/db_postgresql.conf"
FILES_NEEDED[1]="$PWD_THIS_SCRIPT/db_pg_hba.conf"
FILES_NEEDED[2]="$PWD_THIS_SCRIPT/db_create_users.pgsql"
FILES_NEEDED[3]="$PWD_SQL_SCRIPTS/create_proschema.pgsql"
FILES_NEEDED[4]="$PWD_SQL_SCRIPTS/create_blocketdb.pgsql"
FILES_NEEDED[5]="$PWD_SQL_SCRIPTS/create_blocketdb_stats1x1.pgsql"
FILES_NEEDED[6]="$PWD_SQL_SCRIPTS/insert_blocketdb_ruleslistfiltersdata.pgsql"
FILES_NEEDED[7]="$PWD_SQL_SCRIPTS/insert_blocketdb_sitedata.pgsql"
FILES_NEEDED[8]="$PWD_SQL_SCRIPTS/create_blocketdb_archive.pgsql"
FILES_NEEDED[9]="$PWD_THIS_SCRIPT/db_postgresql_init.d"
FILES_NEEDED[10]="/usr/share/pgsql/contrib/fuzzystrmatch.sql"
FILES_NEEDED[11]="$PWD_SQL_SCRIPTS/create_blocketdb_serenity.pgsql"

SETCOLOR_GREEN="echo -en \\033[1;32m"
SETCOLOR_RED="echo -en \\033[1;31m"
SETCOLOR_YELLOW="echo -en \\033[1;33m"
SETCOLOR_NORMAL="echo -en \\033[0;39m"

for filename in ${FILES_NEEDED[@]}
do
	if [ ! -f $filename ]
	then
		echo "The file $filename not exists!"
		echo
		exit -1
	fi
done

# Check user
if [ $(id -u) -ne 0 ] ; then
	echo 'Only root can execute this script.'
	echo 'Try again with superuser, or use sudo.'
	echo
	exit -1;
fi

echo
echo -n "This process "
$SETCOLOR_RED
echo -n "DESTROY ALL POSTGRESQL DATABASES "
$SETCOLOR_NORMAL
echo "on current host,"
echo "and then create the initial 'Yapo Database' again..."
read -n1 -p "> Are you sure? (y/N) "
if [ "$REPLY" != 'y' ]; then
	echo
	echo
	$SETCOLOR_GREEN
	echo "Ok! Don't worry!"
	$SETCOLOR_NORMAL
	echo "Cancelling process...";
	echo
	exit -1
fi
echo
echo

# Truncate log
> $ERROR_LOG

# Check if postgres is running
echo 'Checking current postgresql instance: '
echo -n '  > '
status -p $PGDATA/postmaster.pid postmaster
if [ $? -ne 3 ] ; then
	echo -n '    Stopping instance: '
	su - postgres -c "/usr/bin/pg_ctl stop -D '$PGDATA' -s -m fast" 1> /dev/null 2>> $ERROR_LOG
	if [ $? == 0 ]; then
		echo_success
		echo
	else
		error
	fi
fi

# Create dir (if dont exists)
echo 'Checking postgresql directory: '
if [ ! -d $PGDATA ]; then
	# Creating directory
	echo -n '  > Creating directory (if exists): '
	mkdir $PGDATA 1> /dev/null 2>> $ERROR_LOG
	if [ $? == 0 ]; then
		echo_success
		echo
	else
		error
	fi
	# Setting permissions
	echo -n '  > Settings permissions: '
	chown postgres:root $PGDATA 1> /dev/null 2>> $ERROR_LOG
	if [ $? == 0 ]; then
		echo_success
		echo
	else
		error
	fi
else
	# Remove content of the directory
	echo -n '  > Deleting old content'
	rm -rf $PGDATA/* 1> /dev/null 2>> $ERROR_LOG
	if [ $? == 0 ]; then
                echo_success
                echo
        else
                error
        fi
fi

echo -n "Initializing postgresql: "
su - postgres -c "/usr/bin/initdb $PGDATA --encoding='$ENCODING' --auth='ident'" 1> /dev/null 2>> $ERROR_LOG
if [ $? == 0 ]; then
	echo_success
	echo
else
	error
fi

echo -n "Copying configuration files: "
cp $PWD_THIS_SCRIPT/db_postgresql.conf $PGDATA/postgresql.conf 1> /dev/null 2>> $ERROR_LOG
if [ $? != 0 ]; then
        error
fi
cp $PWD_THIS_SCRIPT/db_pg_hba.conf $PGDATA/pg_hba.conf 1> /dev/null 2>> $ERROR_LOG
if [ $? != 0 ]; then
	error
fi
cp $PWD_THIS_SCRIPT/db_postgresql_init.d /etc/init.d/postgresql 1> /dev/null 2>> $ERROR_LOG
if [ $? != 0 ]; then
	error
fi
chmod +x /etc/init.d/postgresql 1> /dev/null 2>> $ERROR_LOG
if [ $? == 0 ]; then
        echo_success
        echo
else
	error
fi

/etc/init.d/postgresql start
if [ $? != 0 ]; then
        exit -1
fi

echo -n "Creating database: "
su - postgres -c "/usr/bin/createdb --port=$PORT $DATABASE" 1> /dev/null 2>> $ERROR_LOG
RES=$?
if [ $RES == 0 ]; then
        echo_success
        echo
else
	error
fi

echo "Creating schemas: "

echo -n "  > Creating PUBLIC schema: "
# Copy table to temp_file to avoid permissions errors
TEMP_FILE=`mktemp`
cp $PWD_SQL_SCRIPTS/create_blocketdb.pgsql $TEMP_FILE
chmod 0777 $TEMP_FILE
su - postgres -c "/usr/bin/psql --port=$PORT $DATABASE < $TEMP_FILE" 1> /dev/null 2>> $ERROR_LOG
RES=$?
rm $TEMP_FILE
if [ $RES == 0 ]; then
        echo_success
        echo
else
	error
fi

echo -n "  > Creating BPV schema: "
# Copy table to temp_file to avoid permissions errors
TEMP_FILE=`mktemp`
cp $PWD_SQL_SCRIPTS/create_proschema.pgsql $TEMP_FILE
chmod 0777 $TEMP_FILE
su - postgres -c "/usr/bin/psql --port=$PORT $DATABASE < $TEMP_FILE" 1> /dev/null 2>> $ERROR_LOG
RES=$?
rm $TEMP_FILE
if [ $RES == 0 ]; then
	echo_success
	echo
else
	error
fi

echo -n "  > Creating STATS1x1 tables: "
# Copy table to temp_file to avoid permissions errors
TEMP_FILE=`mktemp`
cp $PWD_SQL_SCRIPTS/create_blocketdb_stats1x1.pgsql $TEMP_FILE
chmod 0777 $TEMP_FILE
su - postgres -c "/usr/bin/psql --port=$PORT $DATABASE < $TEMP_FILE" 1> /dev/null 2>> $ERROR_LOG
RES=$?
rm $TEMP_FILE
if [ $RES == 0 ]; then
        echo_success
        echo
else
	error
fi

echo -n "Creating DB users: "
cp $PWD_THIS_SCRIPT/db_create_users.pgsql /tmp
su - postgres -c "/usr/bin/psql --port=$PORT $DATABASE < /tmp/db_create_users.pgsql" 1> /dev/null 2>> $ERROR_LOG
RES=$?
rm /tmp/db_create_users.pgsql
if [ $RES == 0 ]; then
        echo_success
        echo
else
	error
fi

echo "  > Creating ARCHIVE ads schemas: "
# Copy table to temp_file to avoid permissions errors
TEMP_FILE=`mktemp`
cp $PWD_SQL_SCRIPTS/create_blocketdb_archive.pgsql $TEMP_FILE
chmod 0777 $TEMP_FILE
for year in 2009 2010 2011 2012 2013
do
	echo -n "    Schema $year: "
	su - postgres -c "(echo 'CREATE SCHEMA blocket_$year; SET search_path=blocket_$year;' && cat $TEMP_FILE) | /usr/bin/psql --port=$PORT $DATABASE" 1> /dev/null 2>> $ERROR_LOG
	RES=$?
	if [ $RES == 0 ]; then
		echo_success
		echo
	else
		rm $TEMP_FILE
		error
	fi
	echo -n "    - Permission for user [trans]: "
	su - postgres -c "(echo 'GRANT USAGE ON SCHEMA blocket_$year TO trans;' && cat $TEMP_FILE) | /usr/bin/psql --port=$PORT $DATABASE" 1> /dev/null 2>> $ERROR_LOG
	RES=$?
	if [ $RES == 0 ]; then
		echo_success
		echo
	else
		rm $TEMP_FILE
		error
	fi

done
rm $TEMP_FILE

echo -n "Inserting initial site data: "
# Copy table to temp_file to avoid permissions errors
TEMP_FILE=`mktemp`
cp $PWD_SQL_SCRIPTS/insert_blocketdb_sitedata.pgsql $TEMP_FILE
chmod 0777 $TEMP_FILE
su - postgres -c "/usr/bin/psql --port=$PORT $DATABASE < $TEMP_FILE" 1> /dev/null 2>> $ERROR_LOG
RES=$?
rm $TEMP_FILE
if [ $RES == 0 ]; then
        echo_success
        echo
else
	error
fi

echo -n "Inserting rules & filters: "
# Copy table to temp_file to avoid permissions errors
TEMP_FILE=`mktemp`
cp $PWD_SQL_SCRIPTS/insert_blocketdb_ruleslistfiltersdata.pgsql $TEMP_FILE
chmod 0777 $TEMP_FILE
su - postgres -c "/usr/bin/psql --port=$PORT $DATABASE < $TEMP_FILE" 1> /dev/null 2>> $ERROR_LOG
RES=$?
rm $TEMP_FILE
if [ $RES == 0 ]; then
        echo_success
        echo
else
	error
fi

echo -n "Creating CONTROLPANEL users: "
cp $PWD_THIS_SCRIPT/db_create_controlpanel_admins.pgsql /tmp
su - postgres -c "/usr/bin/psql --port=$PORT $DATABASE < /tmp/db_create_controlpanel_admins.pgsql" 1> /dev/null 2>> $ERROR_LOG
RES=$?
rm /tmp/db_create_controlpanel_admins.pgsql
if [ $RES == 0 ]; then
        echo_success
        echo
else
	error
fi

echo -n "Creating FUNCTIONS (fuzzy): "
su - postgres -c "/usr/bin/psql --port=$PORT $DATABASE < /usr/share/pgsql/contrib/fuzzystrmatch.sql" 1> /dev/null 2>> $ERROR_LOG
RES=$?
if [ $RES == 0 ]; then
        echo_success
        echo
else
	error
fi

echo
echo -n "> "
$SETCOLOR_GREEN
echo "Database created succesfully and ready to use in $PGDATA..."
$SETCOLOR_NORMAL
echo "Check the error.log file (if exists) before continue."
echo

