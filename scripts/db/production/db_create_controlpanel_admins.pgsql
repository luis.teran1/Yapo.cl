--
-- SQL Script to create controlpanel users and permissions
-- -----------
--

SET search_path = public, pg_catalog;

INSERT INTO admins (username, passwd, fullname, jabbername, email, mobile, status) 
	VALUES('bhean', '56aee96394a61a6033d5f755da0fb1bc133bc387', 'BhEaN', '', 'bhean@schibstediberica.es', '', 'active');

INSERT INTO admin_privs (admin_id, priv_name) VALUES((SELECT admin_id FROM admins WHERE username = 'bhean'),'search.paylog');
INSERT INTO admin_privs (admin_id, priv_name) VALUES((SELECT admin_id FROM admins WHERE username = 'bhean'),'search.maillog');
INSERT INTO admin_privs (admin_id, priv_name) VALUES((SELECT admin_id FROM admins WHERE username = 'bhean'),'search.abuse_report');
INSERT INTO admin_privs (admin_id, priv_name) VALUES((SELECT admin_id FROM admins WHERE username = 'bhean'),'notice_abuse');
INSERT INTO admin_privs (admin_id, priv_name) VALUES((SELECT admin_id FROM admins WHERE username = 'bhean'),'adminad.clear_ad');
INSERT INTO admin_privs (admin_id, priv_name) VALUES((SELECT admin_id FROM admins WHERE username = 'bhean'),'adminad.edit_ad');
INSERT INTO admin_privs (admin_id, priv_name) VALUES((SELECT admin_id FROM admins WHERE username = 'bhean'),'adminad');
INSERT INTO admin_privs (admin_id, priv_name) VALUES((SELECT admin_id FROM admins WHERE username = 'bhean'),'admin');
INSERT INTO admin_privs (admin_id, priv_name) VALUES((SELECT admin_id FROM admins WHERE username = 'bhean'),'adqueue');
INSERT INTO admin_privs (admin_id, priv_name) VALUES((SELECT admin_id FROM admins WHERE username = 'bhean'),'stores');
INSERT INTO admin_privs (admin_id, priv_name) VALUES((SELECT admin_id FROM admins WHERE username = 'bhean'),'clearad');
INSERT INTO admin_privs (admin_id, priv_name) VALUES((SELECT admin_id FROM admins WHERE username = 'bhean'),'filter');
INSERT INTO admin_privs (admin_id, priv_name) VALUES((SELECT admin_id FROM admins WHERE username = 'bhean'),'popular_ads');
INSERT INTO admin_privs (admin_id, priv_name) VALUES((SELECT admin_id FROM admins WHERE username = 'bhean'),'search');
INSERT INTO admin_privs (admin_id, priv_name) VALUES((SELECT admin_id FROM admins WHERE username = 'bhean'),'credit');
INSERT INTO admin_privs (admin_id, priv_name) VALUES((SELECT admin_id FROM admins WHERE username = 'bhean'),'adminedit');
INSERT INTO admin_privs (admin_id, priv_name) VALUES((SELECT admin_id FROM admins WHERE username = 'bhean'),'config');
INSERT INTO admin_privs (admin_id, priv_name) VALUES((SELECT admin_id FROM admins WHERE username = 'bhean'),'adqueue.admin_settings');
INSERT INTO admin_privs (admin_id, priv_name) VALUES((SELECT admin_id FROM admins WHERE username = 'bhean'),'adqueue.show_num_ads=2');
INSERT INTO admin_privs (admin_id, priv_name) VALUES((SELECT admin_id FROM admins WHERE username = 'bhean'),'adqueue.admin_queue');
INSERT INTO admin_privs (admin_id, priv_name) VALUES((SELECT admin_id FROM admins WHERE username = 'bhean'),'adqueue.refusals');
INSERT INTO admin_privs (admin_id, priv_name) VALUES((SELECT admin_id FROM admins WHERE username = 'bhean'),'filter.lists');
INSERT INTO admin_privs (admin_id, priv_name) VALUES((SELECT admin_id FROM admins WHERE username = 'bhean'),'filter.rules');
INSERT INTO admin_privs (admin_id, priv_name) VALUES((SELECT admin_id FROM admins WHERE username = 'bhean'),'filter.spamfilter');
INSERT INTO admin_privs (admin_id, priv_name) VALUES((SELECT admin_id FROM admins WHERE username = 'bhean'),'search.search_ads');
INSERT INTO admin_privs (admin_id, priv_name) VALUES((SELECT admin_id FROM admins WHERE username = 'bhean'),'search.uid_emails');
INSERT INTO admin_privs (admin_id, priv_name) VALUES((SELECT admin_id FROM admins WHERE username = 'bhean'),'on_call');
INSERT INTO admin_privs (admin_id, priv_name) VALUES((SELECT admin_id FROM admins WHERE username = 'bhean'),'websql');
INSERT INTO admin_privs (admin_id, priv_name) VALUES((SELECT admin_id FROM admins WHERE username = 'bhean'),'adqueue.video');
INSERT INTO admin_privs (admin_id, priv_name) VALUES((SELECT admin_id FROM admins WHERE username = 'bhean'),'bids');
INSERT INTO admin_privs (admin_id, priv_name) VALUES((SELECT admin_id FROM admins WHERE username = 'bhean'),'on_call.duty');
INSERT INTO admin_privs (admin_id, priv_name) VALUES((SELECT admin_id FROM admins WHERE username = 'bhean'),'scarface');
INSERT INTO admin_privs (admin_id, priv_name) VALUES((SELECT admin_id FROM admins WHERE username = 'bhean'),'ais');
