
--
-- Name: action_states_state_id_seq; Type: SEQUENCE SET; Schema: public; Owner: pablo
--

SELECT pg_catalog.setval('action_states_state_id_seq', 705, true);


--
-- Name: pay_code_seq; Type: SEQUENCE SET; Schema: public; Owner: pablo
--

SELECT pg_catalog.setval('pay_code_seq', 468, true);


--
-- Name: pay_log_pay_log_id_seq; Type: SEQUENCE SET; Schema: public; Owner: pablo
--

SELECT pg_catalog.setval('pay_log_pay_log_id_seq', 216, true);


--
-- Name: payment_groups_payment_group_id_seq; Type: SEQUENCE SET; Schema: public; Owner: pablo
--

SELECT pg_catalog.setval('payment_groups_payment_group_id_seq', 167, true);


--
-- Name: tokens_token_id_seq; Type: SEQUENCE SET; Schema: public; Owner: pablo
--

SELECT pg_catalog.setval('tokens_token_id_seq', 733, true);


INSERT INTO payment_groups VALUES (166, '633026231', 'verified', '2015-06-04 14:34:38.631623', NULL);
INSERT INTO payment_groups VALUES (167, '768418254', 'verified', '2015-06-04 14:42:45.757961', NULL);


INSERT INTO ad_actions VALUES (95, 2, 'edit', 701, 'refused', 'edit', NULL, NULL, 166);
INSERT INTO ad_actions VALUES (95, 3, 'editrefused', 705, 'refused', 'normal', NULL, NULL, 167);


INSERT INTO action_params VALUES (95, 2, 'source', 'web');
INSERT INTO action_params VALUES (95, 2, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params VALUES (95, 3, 'prev_action_id', '2');
INSERT INTO action_params VALUES (95, 3, 'source', 'web');
INSERT INTO action_params VALUES (95, 3, 'redir', 'dW5rbm93bg==');


INSERT INTO tokens VALUES (715, 'X55709a6a692fcaab0000000055b0eb5c00000000', 9, '2015-06-04 14:35:21.842284', '2015-06-04 14:35:22.049219', '10.0.1.192', 'authenticate', NULL, NULL);
INSERT INTO tokens VALUES (716, 'X55709a6a46414ffd000000005613a59400000000', 9, '2015-06-04 14:35:22.049219', '2015-06-04 14:35:23.58971', '10.0.1.192', 'spamfilter', NULL, NULL);
INSERT INTO tokens VALUES (717, 'X55709a6c5f0f27290000000027fa8db400000000', 9, '2015-06-04 14:35:23.58971', '2015-06-04 14:35:25.845363', '10.0.1.192', 'adqueues', NULL, NULL);
INSERT INTO tokens VALUES (718, 'X55709a6e623b1cb000000001252e0fe00000000', 9, '2015-06-04 14:35:25.845363', '2015-06-04 14:35:28.251628', '10.0.1.192', 'spamfilter', NULL, NULL);
INSERT INTO tokens VALUES (719, 'X55709a703f4001e800000000724a36f500000000', 9, '2015-06-04 14:35:28.251628', '2015-06-04 14:35:36.456126', '10.0.1.192', 'listadmins', NULL, NULL);
INSERT INTO tokens VALUES (720, 'X55709a7864d2450a000000003b83080c00000000', 9, '2015-06-04 14:35:36.456126', '2015-06-04 14:35:36.475152', '10.0.1.192', 'listadmins', NULL, NULL);
INSERT INTO tokens VALUES (721, 'X55709a783dcb067000000005775436600000000', 9, '2015-06-04 14:35:36.475152', '2015-06-04 14:35:44.840744', '10.0.1.192', 'search_ads', NULL, NULL);
INSERT INTO tokens VALUES (722, 'X55709a8132efc7900000000b897ed400000000', 9, '2015-06-04 14:35:44.840744', '2015-06-04 14:35:44.893581', '10.0.1.192', 'adqueues', NULL, NULL);
INSERT INTO tokens VALUES (723, 'X55709a8174730d6d00000000283de0e00000000', 9, '2015-06-04 14:35:44.893581', '2015-06-04 14:35:44.898216', '10.0.1.192', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (724, 'X55709a816bc6a61900000000454b316c00000000', 9, '2015-06-04 14:35:44.898216', '2015-06-04 14:35:44.902492', '10.0.1.192', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (725, 'X55709a815c28bd3a00000000460df4b100000000', 9, '2015-06-04 14:35:44.902492', '2015-06-04 14:35:56.566389', '10.0.1.192', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (726, 'X55709a8d2ae039bc00000000664067a900000000', 9, '2015-06-04 14:35:56.566389', '2015-06-04 14:42:56.85115', '10.0.1.192', 'review', NULL, NULL);
INSERT INTO tokens VALUES (727, 'X55709c31635262b4000000005575807a00000000', 9, '2015-06-04 14:42:56.85115', '2015-06-04 14:42:56.87959', '10.0.1.192', 'listadmins', NULL, NULL);
INSERT INTO tokens VALUES (728, 'X55709c316ba1e1520000000018d6cb4e00000000', 9, '2015-06-04 14:42:56.87959', '2015-06-04 14:43:03.669542', '10.0.1.192', 'search_ads', NULL, NULL);
INSERT INTO tokens VALUES (729, 'X55709c38bcc10b0000000038a791b00000000', 9, '2015-06-04 14:43:03.669542', '2015-06-04 14:43:03.724874', '10.0.1.192', 'adqueues', NULL, NULL);
INSERT INTO tokens VALUES (730, 'X55709c3845dc411c0000000019f21e0d00000000', 9, '2015-06-04 14:43:03.724874', '2015-06-04 14:43:03.730361', '10.0.1.192', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (731, 'X55709c383cc56bc8000000001ba8bd2200000000', 9, '2015-06-04 14:43:03.730361', '2015-06-04 14:43:03.736782', '10.0.1.192', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (732, 'X55709c38316e05fe00000000311acec200000000', 9, '2015-06-04 14:43:03.736782', '2015-06-04 14:43:32.263829', '10.0.1.192', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (733, 'X55709c54625111f0000000034e69a5c00000000', 9, '2015-06-04 14:43:32.263829', '2015-06-04 15:43:32.263829', '10.0.1.192', 'review', NULL, NULL);


INSERT INTO action_states VALUES (95, 2, 698, 'reg', 'initial', '2015-06-04 14:34:38.631623', '10.0.1.192', NULL);
INSERT INTO action_states VALUES (95, 2, 699, 'unverified', 'verifymail', '2015-06-04 14:34:38.631623', '10.0.1.192', NULL);
INSERT INTO action_states VALUES (95, 2, 700, 'pending_review', 'verify', '2015-06-04 14:34:38.763307', '10.0.1.192', NULL);
INSERT INTO action_states VALUES (95, 2, 701, 'refused', 'refuse', '2015-06-04 14:35:56.571665', '10.0.1.192', 725);
INSERT INTO action_states VALUES (95, 3, 702, 'reg', 'initial', '2015-06-04 14:42:45.757961', '10.0.1.192', NULL);
INSERT INTO action_states VALUES (95, 3, 703, 'unverified', 'verifymail', '2015-06-04 14:42:45.757961', '10.0.1.192', NULL);
INSERT INTO action_states VALUES (95, 3, 704, 'pending_review', 'verify', '2015-06-04 14:42:45.849933', '10.0.1.192', NULL);
INSERT INTO action_states VALUES (95, 3, 705, 'refused', 'refuse', '2015-06-04 14:43:32.268319', '10.0.1.192', 732);


INSERT INTO ad_changes VALUES (95, 2, 698, false, 'subject', 'Adeptus Astartes Codex', 'Adeptus Astartes edition 1');
INSERT INTO ad_changes VALUES (95, 2, 698, false, 'price', '778889', '887889');
INSERT INTO ad_changes VALUES (95, 2, 698, false, 'company_ad', 'false', 'true');
INSERT INTO ad_changes VALUES (95, 2, 698, false, 'category', '6120', '6100');
INSERT INTO ad_changes VALUES (95, 3, 702, false, 'subject', 'Adeptus Astartes Codex', 'Adeptus Astartes edition 2');
INSERT INTO ad_changes VALUES (95, 3, 702, false, 'price', '778889', '997889');
INSERT INTO ad_changes VALUES (95, 3, 702, false, 'company_ad', 'false', 'true');
INSERT INTO ad_changes VALUES (95, 3, 702, false, 'category', '6120', '6100');


INSERT INTO ad_codes VALUES (24741, 'pay');
INSERT INTO ad_codes VALUES (56764, 'pay');
--INSERT INTO ad_codes VALUES (633026231, 'verify');
--INSERT INTO ad_codes VALUES (768418254, 'verify');


INSERT INTO ad_image_changes VALUES (95, 2, 698, 0, NULL, false);
INSERT INTO ad_image_changes VALUES (95, 3, 702, 0, NULL, false);


INSERT INTO pay_log VALUES (213, 'save', 0, '633026231', NULL, '2015-06-04 14:34:39', 166, 'SAVE');
INSERT INTO pay_log VALUES (214, 'verify', 0, '633026231', NULL, '2015-06-04 14:34:39', 166, 'OK');
INSERT INTO pay_log VALUES (215, 'save', 0, '768418254', NULL, '2015-06-04 14:42:46', 167, 'SAVE');
INSERT INTO pay_log VALUES (216, 'verify', 0, '768418254', NULL, '2015-06-04 14:42:46', 167, 'OK');


INSERT INTO pay_log_references VALUES (213, 0, 'adphone', '26437660');
INSERT INTO pay_log_references VALUES (213, 1, 'remote_addr', '10.0.1.192');
INSERT INTO pay_log_references VALUES (214, 0, 'remote_addr', '10.0.1.192');
INSERT INTO pay_log_references VALUES (215, 0, 'adphone', '26437660');
INSERT INTO pay_log_references VALUES (215, 1, 'remote_addr', '10.0.1.192');
INSERT INTO pay_log_references VALUES (216, 0, 'remote_addr', '10.0.1.192');


INSERT INTO payments VALUES (166, 'ad_action', 0, 0);
INSERT INTO payments VALUES (167, 'ad_action', 0, 0);


INSERT INTO review_log VALUES (95, 2, 9, '2015-06-04 14:35:56.571665', 'all', 'edit', 'refused', 'Propiedad intelectual', 6120);
INSERT INTO review_log VALUES (95, 3, 9, '2015-06-04 14:43:32.268319', 'all', 'edit', 'refused', 'Spam', 6120);


INSERT INTO state_params VALUES (95, 2, 700, 'queue', 'edit');
INSERT INTO state_params VALUES (95, 2, 701, 'reason', '32');
INSERT INTO state_params VALUES (95, 2, 701, 'filter_name', 'all');
INSERT INTO state_params VALUES (95, 3, 704, 'queue', 'normal');
INSERT INTO state_params VALUES (95, 3, 705, 'reason', '31');
INSERT INTO state_params VALUES (95, 3, 705, 'filter_name', 'all');


--Change all passwords to 123123123
update ads set salted_passwd =  E'$1024$2y1bQ}VYRD}zF4QCbf07a971b784888658a20e62156de64dfde5ffb6', passwd = null;
update accounts set salted_passwd =  E'$1024$2y1bQ}VYRD}zF4QCbf07a971b784888658a20e62156de64dfde5ffb6';
