INSERT INTO ads VALUES (135, 8000094, '2018-01-04 18:07:27', 'active', 'sell', 'cuentaprosinpack', '876767877', 15, 0, 2020, 60, NULL, false, false, true, 'Austin mini 2009', 'bueno bonito y barato', 500000, NULL, NULL, NULL, '2018-01-04 18:06:36', NULL, NULL, '$1024$2y1bQ}VYRD}zF4QCbf07a971b784888658a20e62156de64dfde5ffb6', '2018-01-04 18:07:27.464437', 'es');
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (175, '633026231', 'verified', '2018-01-04 18:06:23.400953', NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (176, '12345', 'paid', '2018-01-04 18:07:27.262492', NULL);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (135, 1, 'new', 749, 'accepted', 'pack', NULL, NULL, 175);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (135, 2, 'deactivate', 750, 'deactivated', 'normal', NULL, NULL, NULL);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (135, 3, 'disable', 751, 'disabled', 'normal', NULL, NULL, NULL);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (135, 4, 'if_pack_car', 754, 'accepted', 'normal', NULL, NULL, 176);
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (135, 1, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (135, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (135, 1, 'pack_result', '3');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (135, 2, 'source', 'web');
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (135, 1, 746, 'reg', 'initial', '2018-01-04 18:06:23.400953', '10.0.40.130', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (135, 1, 747, 'unverified', 'verifymail', '2018-01-04 18:06:23.400953', '10.0.40.130', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (135, 1, 748, 'pending_review', 'verify', '2018-01-04 18:06:23.506935', '10.0.40.130', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (135, 1, 749, 'accepted', 'accept', '2018-01-04 18:06:36.58434', '10.0.40.130', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (135, 2, 750, 'deactivated', 'user_deactivated', '2018-01-04 18:06:49.071941', '10.0.40.130', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (135, 3, 751, 'disabled', 'user_disabled', '2018-01-04 18:07:03.564541', '10.0.40.130', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (135, 4, 752, 'reg', 'initial', '2018-01-04 18:07:27.262492', '10.0.40.130', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (135, 4, 753, 'unpaid', 'pending_pay', '2018-01-04 18:07:27.262492', '10.0.40.130', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (135, 4, 754, 'accepted', 'pay', '2018-01-04 18:07:27.433436', NULL, NULL);
SELECT pg_catalog.setval('action_states_state_id_seq', 754, true);
INSERT INTO ad_changes (ad_id, action_id, state_id, is_param, column_name, old_value, new_value) VALUES (135, 1, 747, true, 'pack_status', NULL, 'disabled');
INSERT INTO ad_changes (ad_id, action_id, state_id, is_param, column_name, old_value, new_value) VALUES (135, 4, 754, true, 'pack_status', 'disabled', NULL);
INSERT INTO ad_changes (ad_id, action_id, state_id, is_param, column_name, old_value, new_value) VALUES (135, 4, 754, true, 'inserting_fee', NULL, '1');
INSERT INTO ad_codes (code, code_type) VALUES (42948, 'pay');
INSERT INTO ad_image_changes (ad_id, action_id, state_id, seq_no, name, is_new) VALUES (135, 1, 749, 0, NULL, false);
INSERT INTO ad_params (ad_id, name, value) VALUES (135, 'regdate', '2009');
INSERT INTO ad_params (ad_id, name, value) VALUES (135, 'mileage', '150000');
INSERT INTO ad_params (ad_id, name, value) VALUES (135, 'gearbox', '1');
INSERT INTO ad_params (ad_id, name, value) VALUES (135, 'fuel', '1');
INSERT INTO ad_params (ad_id, name, value) VALUES (135, 'cartype', '1');
INSERT INTO ad_params (ad_id, name, value) VALUES (135, 'communes', '306');
INSERT INTO ad_params (ad_id, name, value) VALUES (135, 'currency', 'peso');
INSERT INTO ad_params (ad_id, name, value) VALUES (135, 'brand', '9');
INSERT INTO ad_params (ad_id, name, value) VALUES (135, 'model', '9');
INSERT INTO ad_params (ad_id, name, value) VALUES (135, 'version', '7');
INSERT INTO ad_params (ad_id, name, value) VALUES (135, 'geoposition_is_precise', '0');
INSERT INTO ad_params (ad_id, name, value) VALUES (135, 'country', 'UNK');
INSERT INTO ad_params (ad_id, name, value) VALUES (135, 'inserting_fee', '1');
SELECT pg_catalog.setval('ads_ad_id_seq', 135, true);
SELECT pg_catalog.setval('list_id_seq', 8000094, true);
SELECT pg_catalog.setval('pay_code_seq', 476, true);
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (231, 'save', 0, '633026231', NULL, '2018-01-04 18:06:23', 175, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (232, 'verify', 0, '633026231', NULL, '2018-01-04 18:06:24', 175, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (233, 'save', 0, '12345', NULL, '2018-01-04 18:07:27', 176, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (234, 'paycard_save', 0, '12345', NULL, '2018-01-04 18:07:27', 176, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (235, 'paycard', 6900, '00176a', NULL, '2018-01-04 18:07:27', 176, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (236, 'paycard', 6900, '12345', NULL, '2018-01-04 18:07:27', 176, 'OK');
SELECT pg_catalog.setval('pay_log_pay_log_id_seq', 236, true);
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (231, 0, 'adphone', '876767877');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (231, 1, 'remote_addr', '10.0.40.130');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (232, 0, 'remote_addr', '10.0.40.130');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (233, 0, 'remote_addr', '10.0.40.130');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (234, 0, 'auth_code', '123123123');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (234, 1, 'trans_date', '');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (234, 2, 'placements_type', 'VN');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (234, 3, 'placements_number', '');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (234, 4, 'pay_type', 'credit');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (234, 5, 'external_response', '1');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (234, 6, 'external_id', '');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (234, 7, 'cc_number', '');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (234, 8, 'remote_addr', '0.0.0.0');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (235, 0, 'webpay', '1');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (235, 1, 'webpay', '1');
SELECT pg_catalog.setval('payment_groups_payment_group_id_seq', 176, true);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (175, 'ad_action', 0, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (176, 'if_pack_car', 6900, 0);
INSERT INTO purchase (purchase_id, doc_type, doc_num, external_doc_id, status, receipt, delivery, rut, name, lob, address, region, communes, contact, email, tax, discount, seller_id, total_price, payment_group_id, payment_method, payment_platform, account_id) VALUES (2, 'voucher', NULL, NULL, 'paid', '2018-01-04 18:07:27.262492', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'cuentaprosinpack@yapo.cl', 0, 0, NULL, 6900, 176, 'credits', 'desktop', 8);
INSERT INTO purchase_detail (purchase_detail_id, purchase_id, product_id, price, action_id, ad_id, payment_group_id) VALUES (2, 2, 61, 6900, 4, 135, 176);
SELECT pg_catalog.setval('purchase_detail_purchase_detail_id_seq', 2, true);
SELECT pg_catalog.setval('purchase_purchase_id_seq', 2, true);
INSERT INTO purchase_states (purchase_id, purchase_state_id, status, "timestamp", remote_addr) VALUES (2, 1, 'pending', '2018-01-04 18:07:27.262492', '10.0.40.130');
SELECT pg_catalog.setval('purchase_states_purchase_state_id_seq', 1, true);
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (135, 1, 748, 'queue', 'pack');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (135, 1, 749, 'filter_name', 'pack');
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (106, '2018-01-04 18:06:49.134975', '26424@lisa.schibsted.cl', '2018-01-04 18:07:49.134975', NULL, NULL, NULL, NULL, NULL, 'cmd:queued_delete
action_id:2
id:8000094
passwd:$1024$2y1bQ}VYRD}zF4QCbf07a971b784888658a20e62156de64dfde5ffb6
remote_addr:10.0.40.130
remote_browser:Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.84 Safari/537.36
reason:user_deleted
deletion_reason:4
source:web
', NULL, 'deletead', NULL, NULL);
SELECT pg_catalog.setval('trans_queue_id_seq', 106, true);
