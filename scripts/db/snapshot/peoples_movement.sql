-- ads inserted
DROP TABLE ads_inserted;
CREATE TEMP TABLE ads_inserted AS
SELECT
        q1.user_id,
        SUM(q1.value) AS total
FROM (
	SELECT a.user_id, COUNT(a.user_id) AS value
	FROM users
	INNER JOIN blocket_2011.ads AS a USING(user_id)
	WHERE COALESCE(a.orig_list_time, a.list_time) >= '2011-01-01'
	GROUP BY a.user_id
	UNION ALL
	SELECT a.user_id, COUNT(a.user_id) AS value
	FROM users
	INNER JOIN blocket_2012.ads AS a USING(user_id)
	WHERE COALESCE(a.orig_list_time, a.list_time) >= '2011-01-01'
	GROUP BY a.user_id
	UNION ALL
	SELECT a.user_id, COUNT(a.user_id) AS value
	FROM users
	INNER JOIN blocket_2013.ads AS a USING(user_id)
	WHERE COALESCE(a.orig_list_time, a.list_time) >= '2011-01-01'
	AND COALESCE(a.orig_list_time, a.list_time) < '2013-07-01'
	GROUP BY a.user_id
	UNION ALL
	SELECT a.user_id, COUNT(a.user_id) AS value
	FROM users
	INNER JOIN public.ads AS a USING(user_id)
	WHERE COALESCE(a.orig_list_time, a.list_time) >= '2011-01-01'
	AND COALESCE(a.orig_list_time, a.list_time) < '2013-07-01'
	GROUP BY a.user_id) AS q1
GROUP BY q1.user_id
;

-- ads marked as sold
DROP TABLE ads_marked_as_sold;
CREATE TEMP TABLE ads_marked_as_sold AS
SELECT
        q1.user_id,
        SUM(q1.value) AS total
FROM (
	-- using the action_states timestamp to filter when the ad was actually deleted
	SELECT a.user_id, COUNT(a.user_id) AS value
	FROM users
	INNER JOIN blocket_2011.ads AS a USING(user_id)
	INNER JOIN blocket_2011.ad_actions AS aa USING(ad_id)
	INNER JOIN blocket_2011.action_params AS ap USING(ad_id)
	INNER JOIN blocket_2011.action_states AS ast USING(ad_id)
	WHERE ap.name='deletion_reason' AND ap.value IN ('1', '2')
    AND aa.action_type = 'delete' AND aa.state = 'deleted'
	AND ast.state = 'deleted' AND ast.transition = 'user_deleted'
	AND ast.timestamp >= '2011-01-01'
	AND COALESCE(a.orig_list_time, a.list_time) >= '2011-01-01'
	GROUP BY a.user_id
	UNION ALL
	SELECT a.user_id, COUNT(a.user_id) AS value
	FROM users
	INNER JOIN blocket_2012.ads AS a USING(user_id)
	INNER JOIN blocket_2012.ad_actions AS aa USING(ad_id)
	INNER JOIN blocket_2012.action_params AS ap USING(ad_id)
	INNER JOIN blocket_2012.action_states AS ast USING(ad_id)
	WHERE ap.name='deletion_reason' AND ap.value IN ('1', '2')
    AND aa.action_type = 'delete' AND aa.state = 'deleted'
	AND ast.state = 'deleted' AND ast.transition = 'user_deleted'
	AND ast.timestamp >= '2011-01-01'
	AND COALESCE(a.orig_list_time, a.list_time) >= '2011-01-01'
	GROUP BY a.user_id
	UNION ALL
	SELECT a.user_id, COUNT(a.user_id) AS value
	FROM users
	INNER JOIN blocket_2013.ads AS a USING(user_id)
	INNER JOIN blocket_2013.ad_actions AS aa USING(ad_id)
	INNER JOIN blocket_2013.action_params AS ap USING(ad_id)
	INNER JOIN blocket_2013.action_states AS ast USING(ad_id)
	WHERE ap.name='deletion_reason' AND ap.value IN ('1', '2')
    AND aa.action_type = 'delete' AND aa.state = 'deleted'
	AND ast.state = 'deleted' AND ast.transition = 'user_deleted'
	AND ast.timestamp >= '2011-01-01'
	AND ast.timestamp < '2013-07-01'
	AND COALESCE(a.orig_list_time, a.list_time) >= '2011-01-01'
	AND COALESCE(a.orig_list_time, a.list_time) < '2013-07-01'
	GROUP BY a.user_id
	UNION ALL
	-- using the action_states timestamp to filter when the ad was actually deleted
	SELECT a.user_id, COUNT(a.user_id) AS value
	FROM users
	INNER JOIN ads AS a USING(user_id)
	INNER JOIN ad_actions AS aa USING(ad_id)
	INNER JOIN action_params AS ap USING(ad_id)
	INNER JOIN action_states AS ast USING(ad_id)
	WHERE ap.name='deletion_reason' AND ap.value IN ('1', '2')
    AND aa.action_type = 'delete' AND aa.state = 'deleted'
	AND ast.state = 'deleted' AND ast.transition = 'user_deleted'
	AND ast.timestamp >= '2011-01-01'
	AND ast.timestamp < '2013-07-01'
	AND COALESCE(a.orig_list_time, a.list_time) >= '2011-01-01'
	AND COALESCE(a.orig_list_time, a.list_time) < '2013-07-01'
	GROUP BY a.user_id) AS q1
GROUP BY q1.user_id
;

-- ad replies
DROP TABLE ad_replies;
CREATE TEMP TABLE ad_replies AS
SELECT
        q1.user_id,
        SUM(q1.value) AS total
FROM (
	SELECT
		u.user_id,
		COUNT(u.user_id) AS value
	FROM blocket_2011.mail_queue AS mq
	INNER JOIN users AS u ON u.email=mq.sender_email
	WHERE mq.state = 'sent' AND
	mq.added_at  >= '2011-01-01'
	GROUP BY u.user_id
	UNION ALL
	SELECT
		u.user_id,
		COUNT(u.user_id) AS value
	FROM blocket_2012.mail_queue AS mq
	INNER JOIN users AS u ON u.email=mq.sender_email
	WHERE mq.state = 'sent'
	GROUP BY u.user_id
	UNION ALL
	SELECT
		u.user_id,
		COUNT(u.user_id) AS value
	FROM blocket_2013.mail_queue AS mq
	INNER JOIN users AS u ON u.email=mq.sender_email
	WHERE mq.state = 'sent' AND
	mq.added_at < '2013-07-01'
	GROUP BY u.user_id) AS q1
GROUP BY q1.user_id
;


-- first and last ad insert date
DROP TABLE first_and_last_ad_inserted_date;
CREATE TEMP TABLE first_and_last_ad_inserted_date AS
SELECT
	q1.user_id,
	MIN(first_ad_insert_date) AS first_ad_insert_date,
	MAX(last_ad_insert_date) AS last_ad_insert_date
FROM (
	SELECT
		a.user_id,
		MIN(COALESCE(a.orig_list_time, a.list_time)) AS first_ad_insert_date,
		MAX(COALESCE(a.orig_list_time, a.list_time)) AS last_ad_insert_date
	FROM users
	INNER JOIN blocket_2011.ads AS a USING(user_id)
	WHERE COALESCE(a.orig_list_time, a.list_time) >= '2011-01-01'
	GROUP BY a.user_id
	UNION ALL
	SELECT
		a.user_id,
		MIN(COALESCE(a.orig_list_time, a.list_time)) AS first_ad_insert_date,
		MAX(COALESCE(a.orig_list_time, a.list_time)) AS last_ad_insert_date
	FROM users
	INNER JOIN blocket_2012.ads AS a USING(user_id)
	WHERE COALESCE(a.orig_list_time, a.list_time) >= '2011-01-01'
	GROUP BY a.user_id
	UNION ALL
	SELECT
		a.user_id,
		MIN(COALESCE(a.orig_list_time, a.list_time)) AS first_ad_insert_date,
		MAX(COALESCE(a.orig_list_time, a.list_time)) AS last_ad_insert_date
	FROM users
	INNER JOIN blocket_2013.ads AS a USING(user_id)
	WHERE COALESCE(a.orig_list_time, a.list_time) >= '2011-01-01'
	AND COALESCE(a.orig_list_time, a.list_time) < '2013-07-01'
	GROUP BY a.user_id
	UNION ALL
	SELECT
		a.user_id,
		MIN(COALESCE(a.orig_list_time, a.list_time)) AS first_ad_insert_date,
		MAX(COALESCE(a.orig_list_time, a.list_time)) AS last_ad_insert_date
	FROM users
	INNER JOIN public.ads AS a USING(user_id)
	WHERE COALESCE(a.orig_list_time, a.list_time) >= '2011-01-01'
	AND COALESCE(a.orig_list_time, a.list_time) < '2013-07-01'
	GROUP BY a.user_id) AS q1
GROUP BY q1.user_id
;


-- first and last ad reply dates
DROP TABLE first_and_last_ad_reply_date;
CREATE TEMP TABLE first_and_last_ad_reply_date AS
SELECT
        q1.user_id,
		MIN(first_adreply_date) AS first_adreply_date,
		MAX(last_adreply_date) AS last_adreply_date
FROM (
	SELECT
		u.user_id,
		MIN(mq.added_at) AS first_adreply_date,
		MAX(mq.added_at) AS last_adreply_date
	FROM blocket_2011.mail_queue AS mq
	INNER JOIN users AS u ON u.email=mq.sender_email
	WHERE mq.state = 'sent' AND
	mq.added_at >= '2011-01-01'
	GROUP BY u.user_id
	UNION ALL
	SELECT
		u.user_id,
		MIN(mq.added_at) AS first_adreply_date,
		MAX(mq.added_at) AS last_adreply_date
	FROM blocket_2012.mail_queue AS mq
	INNER JOIN users AS u ON u.email=mq.sender_email
	WHERE mq.state = 'sent'
	GROUP BY u.user_id
	UNION ALL
	SELECT
		u.user_id,
		MIN(mq.added_at) AS first_adreply_date,
		MAX(mq.added_at) AS last_adreply_date
	FROM blocket_2013.mail_queue AS mq
	INNER JOIN users AS u ON u.email=mq.sender_email
	WHERE mq.state = 'sent' AND
	mq.added_at < '2013-07-01'
	GROUP BY u.user_id) AS q1
GROUP BY q1.user_id
;


-- unique ad insert days
DROP TABLE unique_ad_insert_days;
CREATE TEMP TABLE unique_ad_insert_days AS
SELECT
        q1.user_id,
        COUNT(DISTINCT(q1.value)) AS total
FROM (
	SELECT
		a.user_id,
		DATE(DATE_TRUNC('day', COALESCE(a.orig_list_time, a.list_time))) AS value
		FROM blocket_2011.ads AS a
		WHERE COALESCE(a.orig_list_time, a.list_time) >= '2011-01-01'
	UNION ALL
	SELECT
		a.user_id,
		DATE(DATE_TRUNC('day', COALESCE(a.orig_list_time, a.list_time))) AS value
		FROM blocket_2012.ads AS a
		WHERE COALESCE(a.orig_list_time, a.list_time) >= '2011-01-01'
	UNION ALL
	SELECT
		a.user_id,
		DATE(DATE_TRUNC('day', COALESCE(a.orig_list_time, a.list_time))) AS value
		FROM blocket_2013.ads AS a
		WHERE COALESCE(a.orig_list_time, a.list_time) >= '2011-01-01'
		AND COALESCE(a.orig_list_time, a.list_time) < '2013-07-01'
	UNION ALL
	SELECT
		a.user_id,
		DATE(DATE_TRUNC('day', COALESCE(a.orig_list_time, a.list_time))) AS value
		FROM public.ads AS a
		WHERE COALESCE(a.orig_list_time, a.list_time) >= '2011-01-01'
		AND COALESCE(a.orig_list_time, a.list_time) < '2013-07-01') as q1
GROUP BY q1.user_id
;


-- unique ad insert weeks
DROP TABLE unique_ad_insert_weeks;
CREATE TEMP TABLE unique_ad_insert_weeks AS
SELECT
        q1.user_id,
        COUNT(DISTINCT(q1.value)) AS total
FROM (
	SELECT
		a.user_id,
		TO_CHAR(COALESCE(a.orig_list_time, a.list_time), 'IYYY_IW') AS value
		FROM blocket_2011.ads AS a
		WHERE COALESCE(a.orig_list_time, a.list_time) >= '2011-01-01'
	UNION ALL
	SELECT
		a.user_id,
		TO_CHAR(COALESCE(a.orig_list_time, a.list_time), 'IYYY_IW') AS value
		FROM blocket_2012.ads AS a
		WHERE COALESCE(a.orig_list_time, a.list_time) >= '2011-01-01'
	UNION ALL
	SELECT
		a.user_id,
		TO_CHAR(COALESCE(a.orig_list_time, a.list_time), 'IYYY_IW') AS value
		FROM blocket_2013.ads AS a
		WHERE COALESCE(a.orig_list_time, a.list_time) >= '2011-01-01'
		AND COALESCE(a.orig_list_time, a.list_time) < '2013-07-01'
	UNION ALL
	SELECT
		a.user_id,
		TO_CHAR(COALESCE(a.orig_list_time, a.list_time), 'IYYY_IW') AS value
		FROM public.ads AS a
		WHERE COALESCE(a.orig_list_time, a.list_time) >= '2011-01-01' 
		AND COALESCE(a.orig_list_time, a.list_time) < '2013-07-01' ) as q1
GROUP BY q1.user_id
;

-- unique ad insert months
DROP TABLE unique_ad_insert_months;
CREATE TEMP TABLE unique_ad_insert_months AS
SELECT
        q1.user_id,
        COUNT(DISTINCT(q1.value)) AS total
FROM (
	SELECT
		a.user_id,
		TO_CHAR(COALESCE(a.orig_list_time, a.list_time), 'IYYY_MM') AS value
		FROM blocket_2011.ads AS a
		WHERE COALESCE(a.orig_list_time, a.list_time) >= '2011-01-01'
	UNION ALL
	SELECT
		a.user_id,
		TO_CHAR(COALESCE(a.orig_list_time, a.list_time), 'IYYY_MM') AS value
		FROM blocket_2012.ads AS a
		WHERE COALESCE(a.orig_list_time, a.list_time) >= '2011-01-01'
	UNION ALL
	SELECT
		a.user_id,
		TO_CHAR(COALESCE(a.orig_list_time, a.list_time), 'IYYY_MM') AS value
		FROM blocket_2013.ads AS a
		WHERE COALESCE(a.orig_list_time, a.list_time) >= '2011-01-01'
		AND COALESCE(a.orig_list_time, a.list_time) < '2013-07-01'
	UNION ALL
	SELECT
		a.user_id,
		TO_CHAR(COALESCE(a.orig_list_time, a.list_time), 'IYYY_MM') AS value
		FROM public.ads AS a
		WHERE COALESCE(a.orig_list_time, a.list_time) >= '2011-01-01'
		AND COALESCE(a.orig_list_time, a.list_time) < '2013-07-01') as q1
GROUP BY q1.user_id
;

-- unique ad reply days
DROP TABLE unique_ad_reply_days;
CREATE TEMP TABLE unique_ad_reply_days AS
SELECT
        q1.user_id,
        SUM(q1.value) AS total
FROM (
	SELECT
	u.user_id,
	COUNT(DISTINCT DATE(DATE_TRUNC('day', mq.added_at))) AS value
	FROM blocket_2011.mail_queue AS mq
	INNER JOIN users AS u ON u.email=mq.sender_email
	WHERE mq.state = 'sent' AND
	mq.added_at >= '2011-01-01'
	GROUP BY u.user_id
	UNION ALL
	SELECT
	u.user_id,
	COUNT(DISTINCT DATE(DATE_TRUNC('day', mq.added_at))) AS value
	FROM blocket_2012.mail_queue AS mq
	INNER JOIN users AS u ON u.email=mq.sender_email
	WHERE mq.state = 'sent'
	GROUP BY u.user_id
	UNION ALL
	SELECT
	u.user_id,
	COUNT(DISTINCT DATE(DATE_TRUNC('day', mq.added_at))) AS value
	FROM blocket_2013.mail_queue AS mq
	INNER JOIN users AS u ON u.email=mq.sender_email
	WHERE mq.state = 'sent' AND
	mq.added_at < '2013-07-01'
	GROUP BY u.user_id) AS q1
GROUP BY q1.user_id
;

-- unique ad reply weeks
DROP TABLE unique_ad_reply_weeks;
CREATE TEMP TABLE unique_ad_reply_weeks AS
SELECT
        q1.user_id,
        SUM(q1.value) AS total
FROM (
        SELECT
        u.user_id,
	COUNT(DISTINCT TO_CHAR(mq.added_at, 'IYYY_IW')) AS value
        FROM blocket_2011.mail_queue AS mq
        INNER JOIN users AS u ON u.email=mq.sender_email
        WHERE mq.state = 'sent' AND
        mq.added_at >= '2011-01-01'
        GROUP BY u.user_id
        UNION ALL
        SELECT
        u.user_id,
	COUNT(DISTINCT TO_CHAR(mq.added_at, 'IYYY_IW')) AS value
        FROM blocket_2012.mail_queue AS mq
        INNER JOIN users AS u ON u.email=mq.sender_email
        WHERE mq.state = 'sent'
        GROUP BY u.user_id
        UNION ALL
        SELECT
        u.user_id,
	COUNT(DISTINCT TO_CHAR(mq.added_at, 'IYYY_IW')) AS value
        FROM blocket_2013.mail_queue AS mq
        INNER JOIN users AS u ON u.email=mq.sender_email
        WHERE mq.state = 'sent' AND
        mq.added_at < '2013-07-01'
        GROUP BY u.user_id) AS q1
GROUP BY q1.user_id
;

-- unique ad reply months
DROP TABLE unique_ad_reply_months;
CREATE TEMP TABLE unique_ad_reply_months AS
SELECT
        q1.user_id,
        SUM(q1.value) AS total
FROM (
        SELECT
        u.user_id,
	COUNT(DISTINCT TO_CHAR(mq.added_at, 'IYYY_MM')) AS value
        FROM blocket_2011.mail_queue AS mq
        INNER JOIN users AS u ON u.email=mq.sender_email
        WHERE mq.state = 'sent' AND
        mq.added_at >= '2011-01-01'
        GROUP BY u.user_id
        UNION ALL
        SELECT
        u.user_id,
	COUNT(DISTINCT TO_CHAR(mq.added_at, 'IYYY_MM')) AS value
        FROM blocket_2012.mail_queue AS mq
        INNER JOIN users AS u ON u.email=mq.sender_email
        WHERE mq.state = 'sent'
        GROUP BY u.user_id
        UNION ALL
        SELECT
        u.user_id,
	COUNT(DISTINCT TO_CHAR(mq.added_at, 'IYYY_MM')) AS value
        FROM blocket_2013.mail_queue AS mq
        INNER JOIN users AS u ON u.email=mq.sender_email
        WHERE mq.state = 'sent' AND
        mq.added_at < '2013-07-01'
        GROUP BY u.user_id) AS q1
GROUP BY q1.user_id
;

-- unique main categories inserted ads in
DROP TABLE unique_main_categories_inserted_ads_in;
CREATE TEMP TABLE unique_main_categories_inserted_ads_in AS
SELECT
        q1.user_id,
        COUNT(DISTINCT q1.main_category) AS total
FROM (
	SELECT
		a.user_id,
		a.category/1000 AS main_category
	FROM blocket_2011.ads AS a
	WHERE COALESCE(a.orig_list_time, a.list_time) >= '2011-01-01'
	UNION ALL
	SELECT
		a.user_id,
		a.category/1000 AS main_category
	FROM blocket_2012.ads AS a
	WHERE COALESCE(a.orig_list_time, a.list_time) >= '2011-01-01'
	UNION ALL
	SELECT
		a.user_id,
		a.category/1000 AS main_category
	FROM blocket_2013.ads AS a
	WHERE COALESCE(a.orig_list_time, a.list_time) >= '2011-01-01'
	UNION ALL
	SELECT
		a.user_id,
		a.category/1000 AS main_category
	FROM public.ads AS a
	WHERE COALESCE(a.orig_list_time, a.list_time) >= '2011-01-01'
	AND COALESCE(a.orig_list_time, a.list_time) < '2013-07-01'
) AS q1
GROUP BY q1.user_id
;

-- unique categories inserted ads in
DROP TABLE unique_categories_inserted_ads_in;
CREATE TEMP TABLE unique_categories_inserted_ads_in AS
SELECT
        q1.user_id,
        COUNT(DISTINCT q1.category) AS total
FROM (
	SELECT
		a.user_id,
		a.category
	FROM blocket_2011.ads AS a
	WHERE COALESCE(a.orig_list_time, a.list_time) >= '2011-01-01'
	UNION ALL
	SELECT
		a.user_id,
		a.category
	FROM blocket_2012.ads AS a
	WHERE COALESCE(a.orig_list_time, a.list_time) >= '2011-01-01'
	UNION ALL
	SELECT
		a.user_id,
		a.category
	FROM blocket_2013.ads AS a
	WHERE COALESCE(a.orig_list_time, a.list_time) >= '2011-01-01'
	AND COALESCE(a.orig_list_time, a.list_time) < '2013-07-01'
	UNION ALL
	SELECT
		a.user_id,
		a.category
	FROM public.ads AS a
	WHERE COALESCE(a.orig_list_time, a.list_time) >= '2011-01-01'
	AND COALESCE(a.orig_list_time, a.list_time) < '2013-07-01'
) AS q1
GROUP BY q1.user_id
;

-- main categories replied to ads in
DROP TABLE main_categories_replied;
CREATE TEMP TABLE main_categories_replied AS
SELECT
        q1.user_id,
        COUNT(DISTINCT q1.value) AS total
FROM (
SELECT
        a.user_id,
        a.category/1000 AS value
        FROM blocket_2011.mail_queue AS mq
        INNER JOIN users AS u ON u.email=mq.sender_email
	INNER JOIN blocket_2011.ads AS a ON (mq.list_id = a.list_id)
        WHERE mq.state = 'sent' AND
        mq.added_at >= '2011-01-01'
	UNION ALL
SELECT
        a.user_id,
        a.category/1000 AS value
        FROM blocket_2011.mail_queue AS mq
        INNER JOIN users AS u ON u.email=mq.sender_email
	INNER JOIN blocket_2012.ads AS a ON (mq.list_id = a.list_id)
        WHERE mq.state = 'sent' AND
        mq.added_at >= '2011-01-01'
	UNION ALL
SELECT
        a.user_id,
        a.category/1000 AS value
        FROM blocket_2011.mail_queue AS mq
        INNER JOIN users AS u ON u.email=mq.sender_email
	INNER JOIN blocket_2013.ads AS a ON (mq.list_id = a.list_id)
        WHERE mq.state = 'sent' AND
        mq.added_at >= '2011-01-01'
	UNION ALL
SELECT
        a.user_id,
        a.category/1000 AS value
        FROM blocket_2011.mail_queue AS mq
        INNER JOIN users AS u ON u.email=mq.sender_email
	INNER JOIN ads AS a ON (mq.list_id = a.list_id)
        WHERE mq.state = 'sent' AND
        mq.added_at >= '2011-01-01'
	UNION ALL
SELECT
        a.user_id,
        a.category/1000 AS value
        FROM blocket_2012.mail_queue AS mq
        INNER JOIN users AS u ON u.email=mq.sender_email
	INNER JOIN blocket_2012.ads AS a ON (mq.list_id = a.list_id)
        WHERE mq.state = 'sent'
	UNION ALL
SELECT
        a.user_id,
        a.category/1000 AS value
        FROM blocket_2012.mail_queue AS mq
        INNER JOIN users AS u ON u.email=mq.sender_email
	INNER JOIN blocket_2013.ads AS a ON (mq.list_id = a.list_id)
        WHERE mq.state = 'sent'
	UNION ALL
SELECT
        a.user_id,
        a.category/1000 AS value
        FROM blocket_2012.mail_queue AS mq
        INNER JOIN users AS u ON u.email=mq.sender_email
	INNER JOIN ads AS a ON (mq.list_id = a.list_id)
        WHERE mq.state = 'sent'
	UNION ALL
SELECT
        a.user_id,
        a.category/1000 AS value
        FROM blocket_2013.mail_queue AS mq
        INNER JOIN users AS u ON u.email=mq.sender_email
	INNER JOIN blocket_2013.ads AS a ON (mq.list_id = a.list_id)
        WHERE mq.state = 'sent'
		AND mq.added_at < '2013-07-01'
	UNION ALL
SELECT
        a.user_id,
        a.category/1000 AS value
        FROM blocket_2013.mail_queue AS mq
        INNER JOIN users AS u ON u.email=mq.sender_email
	INNER JOIN ads AS a ON (mq.list_id = a.list_id)
        WHERE mq.state = 'sent'
        AND mq.added_at < '2013-07-01') as q1
GROUP BY q1.user_id
;

-- categories replied to ads in
DROP TABLE categories_replied;
CREATE TEMP TABLE categories_replied AS
SELECT
        q1.user_id,
        COUNT(DISTINCT q1.value) AS total
FROM (
SELECT
        a.user_id,
        a.category AS value
        FROM blocket_2011.mail_queue AS mq
        INNER JOIN users AS u ON u.email=mq.sender_email
	INNER JOIN blocket_2011.ads AS a ON (mq.list_id = a.list_id)
        WHERE mq.state = 'sent' AND
        mq.added_at >= '2011-01-01'
	UNION ALL
SELECT
        a.user_id,
        a.category AS value
        FROM blocket_2011.mail_queue AS mq
        INNER JOIN users AS u ON u.email=mq.sender_email
	INNER JOIN blocket_2012.ads AS a ON (mq.list_id = a.list_id)
        WHERE mq.state = 'sent' AND
        mq.added_at >= '2011-01-01'
	UNION ALL
SELECT
        a.user_id,
        a.category AS value
        FROM blocket_2011.mail_queue AS mq
        INNER JOIN users AS u ON u.email=mq.sender_email
	INNER JOIN blocket_2013.ads AS a ON (mq.list_id = a.list_id)
        WHERE mq.state = 'sent' AND
        mq.added_at >= '2011-01-01'
	UNION ALL
SELECT
        a.user_id,
        a.category/1000 AS value
        FROM blocket_2011.mail_queue AS mq
        INNER JOIN users AS u ON u.email=mq.sender_email
	INNER JOIN ads AS a ON (mq.list_id = a.list_id)
        WHERE mq.state = 'sent' AND
        mq.added_at >= '2011-01-01'
	UNION ALL
SELECT
        a.user_id,
        a.category AS value
        FROM blocket_2012.mail_queue AS mq
        INNER JOIN users AS u ON u.email=mq.sender_email
	INNER JOIN blocket_2012.ads AS a ON (mq.list_id = a.list_id)
        WHERE mq.state = 'sent'
	UNION ALL
SELECT
        a.user_id,
        a.category AS value
        FROM blocket_2012.mail_queue AS mq
        INNER JOIN users AS u ON u.email=mq.sender_email
	INNER JOIN blocket_2013.ads AS a ON (mq.list_id = a.list_id)
        WHERE mq.state = 'sent'
	UNION ALL
SELECT
        a.user_id,
        a.category/1000 AS value
        FROM blocket_2012.mail_queue AS mq
        INNER JOIN users AS u ON u.email=mq.sender_email
	INNER JOIN ads AS a ON (mq.list_id = a.list_id)
        WHERE mq.state = 'sent'
	UNION ALL
SELECT
        a.user_id,
        a.category AS value
        FROM blocket_2013.mail_queue AS mq
        INNER JOIN users AS u ON u.email=mq.sender_email
	INNER JOIN blocket_2013.ads AS a ON (mq.list_id = a.list_id)
        WHERE mq.state = 'sent'
		AND mq.added_at < '2013-07-01'
	UNION ALL
SELECT
        a.user_id,
        a.category AS value
        FROM blocket_2013.mail_queue AS mq
        INNER JOIN users AS u ON u.email=mq.sender_email
	INNER JOIN ads AS a ON (mq.list_id = a.list_id)
        WHERE mq.state = 'sent'
        AND mq.added_at < '2013-07-01') as q1
GROUP BY q1.user_id
;

-- Main category of the 1st ad inserted
DROP TABLE first_ad_inserted_category;
CREATE TEMP TABLE first_ad_inserted_category AS
SELECT
	DISTINCT ON (user_id) q1.user_id,
	(q1.category/1000)*1000 AS first_main_category_published_ad,
	q1.category AS first_category_published_ad
FROM (
	SELECT
		a.user_id,
		a.category
	FROM blocket_2011.ads a
	INNER JOIN first_and_last_ad_inserted_date aid USING(user_id)
	WHERE aid.first_ad_insert_date = COALESCE(a.orig_list_time, a.list_time)
	UNION
	SELECT
		a.user_id,
		a.category
	FROM blocket_2012.ads a
	INNER JOIN first_and_last_ad_inserted_date aid USING(user_id)
	WHERE aid.first_ad_insert_date = COALESCE(a.orig_list_time, a.list_time)
	UNION
	SELECT
		a.user_id,
		a.category
	FROM blocket_2013.ads a
	INNER JOIN first_and_last_ad_inserted_date aid USING(user_id)
	WHERE aid.first_ad_insert_date = COALESCE(a.orig_list_time, a.list_time)
	UNION
	SELECT
		a.user_id,
		a.category
	FROM ads a
	INNER JOIN first_and_last_ad_inserted_date aid USING(user_id)
	WHERE aid.first_ad_insert_date = COALESCE(a.orig_list_time, a.list_time)
	) AS q1
	ORDER BY q1.user_id
;

DROP TABLE snapshot_users;
CREATE TEMP TABLE snapshot_users AS
SELECT q1.user_id FROM (
	SELECT user_id FROM ads_inserted
	UNION
	SELECT user_id FROM ads_marked_as_sold
	UNION
	SELECT user_id FROM ad_replies
	UNION
	SELECT user_id FROM first_and_last_ad_inserted_date
	UNION
	SELECT user_id FROM first_and_last_ad_reply_date
	UNION
	SELECT user_id FROM unique_ad_insert_days
	UNION
	SELECT user_id FROM unique_ad_insert_weeks
	UNION
	SELECT user_id FROM unique_ad_insert_months
	UNION
	SELECT user_id FROM unique_ad_reply_days
	UNION
	SELECT user_id FROM unique_ad_reply_weeks
	UNION
	SELECT user_id FROM unique_ad_reply_months
	UNION
	SELECT user_id FROM unique_main_categories_inserted_ads_in
	UNION
	SELECT user_id FROM unique_categories_inserted_ads_in
	UNION
	SELECT user_id FROM main_categories_replied
	UNION
	SELECT user_id FROM categories_replied) AS q1
WHERE user_id NOT IN (
	-- XXX this should be filtered in the previous queries, but here is faster to do it now, sorry guys
	SELECT DISTINCT user_id FROM blocket_2011.ads WHERE company_ad = 't' UNION
	SELECT DISTINCT user_id FROM blocket_2012.ads WHERE company_ad = 't' UNION
	SELECT DISTINCT user_id FROM blocket_2013.ads WHERE company_ad = 't' UNION
	SELECT DISTINCT user_id FROM ads WHERE company_ad = 't'
);

DROP TABLE db_snap_0;
CREATE TEMP TABLE db_snap_0 AS
SELECT
    u.user_id AS user_id,
    COALESCE(ads_inserted.total, 0) AS ads_inserted,
    COALESCE(ads_marked_as_sold.total, 0) AS ads_marked_as_sold,
    COALESCE(ad_replies.total, 0) AS ads_replies
FROM
    snapshot_users u
    LEFT JOIN ads_inserted USING (user_id)
    LEFT JOIN ads_marked_as_sold USING (user_id)
    LEFT JOIN ad_replies USING (user_id)
;

DROP TABLE db_snap_1;
CREATE TEMP TABLE db_snap_1 AS
SELECT
    u.*,
    COALESCE(to_char(first_and_last_ad_inserted_date.first_ad_insert_date, 'YYYY-MM-DD'), '') AS first_ad_insert_date,
	COALESCE(to_char(first_and_last_ad_inserted_date.last_ad_insert_date, 'YYYY-MM-DD'), '') AS last_ad_insert_date,
    COALESCE(to_char(first_and_last_ad_reply_date.first_adreply_date, 'YYYY-MM-DD'), '') AS first_adreply_date,
    COALESCE(to_char(first_and_last_ad_reply_date.last_adreply_date, 'YYYY-MM-DD'), '') AS last_adreply_date
FROM
    db_snap_0 u
    LEFT JOIN first_and_last_ad_inserted_date USING (user_id)
    LEFT JOIN first_and_last_ad_reply_date USING (user_id)
;

DROP TABLE db_snap_2;
CREATE TEMP TABLE db_snap_2 AS
SELECT
    u.*,
    COALESCE(unique_ad_insert_days.total, 0) AS unique_ad_insert_days,
    COALESCE(unique_ad_insert_weeks.total, 0) AS unique_ad_insert_weeks,
    COALESCE(unique_ad_insert_months.total, 0) AS unique_ad_insert_months
FROM
    db_snap_1 u
    LEFT JOIN unique_ad_insert_days USING (user_id)
    LEFT JOIN unique_ad_insert_weeks USING (user_id)
    LEFT JOIN unique_ad_insert_months USING (user_id)
;

DROP TABLE db_snap_3;
CREATE TEMP TABLE db_snap_3 AS
SELECT
    u.*,
    COALESCE(unique_ad_reply_days.total, 0) AS unique_ad_reply_days,
    COALESCE(unique_ad_reply_weeks.total, 0) AS unique_ad_reply_weeks,
    COALESCE(unique_ad_reply_months.total, 0) AS unique_ad_reply_months
FROM
    db_snap_2 u
    LEFT JOIN unique_ad_reply_days USING (user_id)
    LEFT JOIN unique_ad_reply_weeks USING (user_id)
    LEFT JOIN unique_ad_reply_months USING (user_id)
;

DROP TABLE db_snap_4;
CREATE TEMP TABLE db_snap_4 AS
SELECT
    u.*,
    COALESCE(unique_main_categories_inserted_ads_in.total, 0) AS unique_main_categories_inserted_ads_in,
    COALESCE(unique_categories_inserted_ads_in.total, 0) AS unique_categories_inserted_ads_in,
    COALESCE(main_categories_replied.total, 0) AS main_categories_replied
FROM
    db_snap_3 u
    LEFT JOIN unique_main_categories_inserted_ads_in USING (user_id)
    LEFT JOIN unique_categories_inserted_ads_in USING (user_id)
    LEFT JOIN main_categories_replied USING (user_id)
;

DROP TABLE db_snap_5;
CREATE TEMP TABLE db_snap_5 AS
SELECT
    u.*,
    COALESCE(categories_replied.total, 0) AS categories_replied,
    COALESCE(first_ad_inserted_category.first_main_category_published_ad, 0) AS first_main_category_inserted_ad,
    COALESCE(first_ad_inserted_category.first_category_published_ad, 0) AS first_category_inserted_ad,
    0 AS has_account
FROM
    db_snap_4 u
    LEFT JOIN categories_replied USING (user_id)
    LEFT JOIN first_ad_inserted_category USING (user_id)
;

DROP TABLE db_snap_6;
CREATE TEMP TABLE db_snap_6 AS
SELECT
    md5(u.email) AS user_id,
	db.ads_inserted,
	db.ads_marked_as_sold,
	db.ads_replies,
	db.first_ad_insert_date,
	db.last_ad_insert_date,
	db.first_adreply_date,
	db.last_adreply_date,
	db.unique_ad_insert_days,
	db.unique_ad_insert_weeks,
	db.unique_ad_insert_months,
	db.unique_ad_reply_days,
	db.unique_ad_reply_weeks,
	db.unique_ad_reply_months,
	db.unique_main_categories_inserted_ads_in,
	db.unique_categories_inserted_ads_in,
	db.main_categories_replied,
	db.categories_replied,
	db.first_main_category_inserted_ad,
	db.first_category_inserted_ad,
	db.has_account
FROM
    db_snap_5 db
    INNER JOIN users u USING (user_id)
;

COPY db_snap_6 TO '/tmp/people_movement_snapshot.csv' DELIMITER ',' CSV HEADER;
