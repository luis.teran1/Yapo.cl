INSERT INTO account_actions (account_id, account_action_id, account_action_type, "timestamp", token_id) VALUES (7, 1, 'edition', '2016-08-19 14:53:47.59508', NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (166, '633026231', 'verified', '2016-08-19 14:52:04.088465', NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (167, '12345', 'paid', '2016-08-19 14:52:17.972807', NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (168, '768418254', 'verified', '2016-08-19 14:52:37.909204', NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (169, '65432', 'paid', '2016-08-19 14:52:39.891233', NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (170, '903810277', 'verified', '2016-08-19 14:53:47.553694', NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (171, '32323', 'paid', '2016-08-19 14:53:49.753559', NULL);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (30, 2, 'edit', 700, 'pending_review', 'autoaccept', NULL, NULL, 166);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (30, 3, 'upselling_advanced_inmo', 703, 'accepted', 'normal', NULL, NULL, 167);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (38, 2, 'edit', 706, 'pending_review', 'autoaccept', NULL, NULL, 168);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (38, 3, 'upselling_standard_cars', 709, 'accepted', 'normal', NULL, NULL, 169);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (63, 2, 'edit', 710, 'pending_review', 'autoaccept', NULL, NULL, 168);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (63, 3, 'upselling_standard_cars', 711, 'accepted', 'normal', NULL, NULL, 169);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (122, 2, 'edit', 712, 'pending_review', 'autoaccept', NULL, NULL, 170);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (122, 3, 'upselling_premium_others', 715, 'accepted', 'normal', NULL, NULL, 171);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (122, 4, 'gallery', 717, 'accepted', 'normal', NULL, NULL, NULL);
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (30, 2, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (30, 2, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (30, 3, 'label_type', 'little_use');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (38, 2, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (38, 2, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (38, 2, 'force_review', 'true');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (38, 2, 'label_type', 'new');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (63, 2, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (63, 2, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (63, 2, 'force_review', 'true');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (63, 2, 'label_type', 'opportunity');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (122, 2, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (122, 2, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (122, 3, 'label_type', 'urgent');
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (30, 2, 698, 'reg', 'initial', '2016-08-19 14:52:04.088465', '10.0.1.89', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (30, 2, 699, 'unverified', 'verifymail', '2016-08-19 14:52:04.088465', '10.0.1.89', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (30, 2, 700, 'pending_review', 'verify', '2016-08-19 14:52:05.202756', '10.0.1.89', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (30, 3, 701, 'reg', 'initial', '2016-08-19 14:52:17.972807', '10.0.1.89', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (30, 3, 702, 'unpaid', 'pending_pay', '2016-08-19 14:52:17.972807', '10.0.1.89', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (30, 3, 703, 'accepted', 'pay', '2016-08-19 14:52:20.841106', NULL, NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (38, 2, 704, 'reg', 'initial', '2016-08-19 14:52:37.909204', '10.0.1.89', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (38, 2, 705, 'unverified', 'verifymail', '2016-08-19 14:52:37.909204', '10.0.1.89', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (38, 2, 706, 'pending_review', 'verify', '2016-08-19 14:52:38.001436', '10.0.1.89', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (38, 3, 707, 'reg', 'initial', '2016-08-19 14:52:39.891233', '10.0.1.89', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (38, 3, 708, 'unpaid', 'pending_pay', '2016-08-19 14:52:39.891233', '10.0.1.89', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (38, 3, 709, 'accepted', 'pay', '2016-08-19 14:52:41.217801', NULL, NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (63, 2, 720, 'reg', 'initial', '2016-08-19 14:52:37.909204', '10.0.1.89', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (63, 2, 721, 'unverified', 'verifymail', '2016-08-19 14:52:37.909204', '10.0.1.89', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (63, 2, 722, 'pending_review', 'verify', '2016-08-19 14:52:38.001436', '10.0.1.89', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (63, 3, 723, 'reg', 'initial', '2016-08-19 14:52:39.891233', '10.0.1.89', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (63, 3, 724, 'unpaid', 'pending_pay', '2016-08-19 14:52:39.891233', '10.0.1.89', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (63, 3, 725, 'accepted', 'pay', '2016-08-19 14:52:41.217801', NULL, NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (122, 2, 710, 'reg', 'initial', '2016-08-19 14:53:47.553694', '10.0.1.89', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (122, 2, 711, 'unverified', 'verifymail', '2016-08-19 14:53:47.553694', '10.0.1.89', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (122, 2, 712, 'pending_review', 'verify', '2016-08-19 14:53:47.62841', '10.0.1.89', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (122, 3, 713, 'reg', 'initial', '2016-08-19 14:53:49.753559', '10.0.1.89', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (122, 3, 714, 'unpaid', 'pending_pay', '2016-08-19 14:53:49.753559', '10.0.1.89', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (122, 3, 715, 'accepted', 'pay', '2016-08-19 14:53:51.877741', NULL, NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (122, 4, 716, 'reg', 'initial', '2016-08-19 14:53:51.935176', '10.0.1.89', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (122, 4, 717, 'accepted', 'adminclear', '2016-08-19 14:53:51.935176', '10.0.1.89', NULL);
SELECT pg_catalog.setval('action_states_state_id_seq', 740, true);
INSERT INTO ad_changes (ad_id, action_id, state_id, is_param, column_name, old_value, new_value) VALUES (30, 2, 698, true, 'prev_currency', 'peso', NULL);
INSERT INTO ad_changes (ad_id, action_id, state_id, is_param, column_name, old_value, new_value) VALUES (30, 3, 703, true, 'upselling_weekly_bump', NULL, '4');
INSERT INTO ad_changes (ad_id, action_id, state_id, is_param, column_name, old_value, new_value) VALUES (30, 3, 703, true, 'label', NULL, 'little_use');
INSERT INTO ad_changes (ad_id, action_id, state_id, is_param, column_name, old_value, new_value) VALUES (38, 3, 709, true, 'upselling_daily_bump', NULL, '7');
INSERT INTO ad_changes (ad_id, action_id, state_id, is_param, column_name, old_value, new_value) VALUES (38, 3, 709, true, 'label', NULL, 'new');
INSERT INTO ad_changes (ad_id, action_id, state_id, is_param, column_name, old_value, new_value) VALUES (63, 3, 725, true, 'upselling_daily_bump', NULL, '7');
INSERT INTO ad_changes (ad_id, action_id, state_id, is_param, column_name, old_value, new_value) VALUES (63, 3, 725, true, 'label', NULL, 'opportunity');
INSERT INTO ad_changes (ad_id, action_id, state_id, is_param, column_name, old_value, new_value) VALUES (122, 3, 715, true, 'label', NULL, 'urgent');
INSERT INTO ad_changes (ad_id, action_id, state_id, is_param, column_name, old_value, new_value) VALUES (122, 4, 717, true, 'gallery', NULL, '2016-08-19 15:03:51');
INSERT INTO ad_codes (code, code_type) VALUES (24741, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (56764, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (88787, 'pay');
INSERT INTO ad_image_changes (ad_id, action_id, state_id, seq_no, name, is_new) VALUES (30, 2, 698, 0, NULL, false);
INSERT INTO ad_image_changes (ad_id, action_id, state_id, seq_no, name, is_new) VALUES (38, 2, 704, 0, NULL, false);
INSERT INTO ad_image_changes (ad_id, action_id, state_id, seq_no, name, is_new) VALUES (122, 2, 710, 0, '0100012349.jpg', false);
INSERT INTO ad_params (ad_id, name, value) VALUES (30, 'label', 'little_use');
INSERT INTO ad_params (ad_id, name, value) VALUES (30, 'upselling_weekly_bump', '4');
INSERT INTO ad_params (ad_id, name, value) VALUES (38, 'upselling_daily_bump', '7');
INSERT INTO ad_params (ad_id, name, value) VALUES (38, 'label', 'new');
INSERT INTO ad_params (ad_id, name, value) VALUES (63, 'upselling_daily_bump', '7');
INSERT INTO ad_params (ad_id, name, value) VALUES (63, 'label', 'opportunity');
INSERT INTO ad_params (ad_id, name, value) VALUES (122, 'label', 'urgent');
INSERT INTO ad_params (ad_id, name, value) VALUES (122, 'gallery', '2016-08-19 15:03:51');
INSERT INTO ad_queues (ad_id, action_id, queue, queued_at, locked_by, locked_until) VALUES (30, 2, 'autoaccept', '2016-08-19 14:52:05.202756', NULL, NULL);
INSERT INTO ad_queues (ad_id, action_id, queue, queued_at, locked_by, locked_until) VALUES (38, 2, 'autoaccept', '2016-08-19 14:52:38.001436', NULL, NULL);
INSERT INTO ad_queues (ad_id, action_id, queue, queued_at, locked_by, locked_until) VALUES (122, 2, 'autoaccept', '2016-08-19 14:53:47.62841', NULL, NULL);
SELECT pg_catalog.setval('pay_code_seq', 469, true);
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (213, 'save', 0, '633026231', NULL, '2016-08-19 14:52:04', 166, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (214, 'verify', 0, '633026231', NULL, '2016-08-19 14:52:05', 166, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (215, 'save', 0, '12345', NULL, '2016-08-19 14:52:18', 167, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (216, 'paycard_save', 0, '12345', NULL, '2016-08-19 14:52:20', 167, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (217, 'paycard', 9890, '00167a', NULL, '2016-08-19 14:52:21', 167, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (218, 'paycard', 9890, '12345', NULL, '2016-08-19 14:52:21', 167, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (219, 'save', 0, '768418254', NULL, '2016-08-19 14:52:38', 168, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (220, 'verify', 0, '768418254', NULL, '2016-08-19 14:52:38', 168, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (221, 'save', 0, '65432', NULL, '2016-08-19 14:52:40', 169, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (222, 'paycard_save', 0, '65432', NULL, '2016-08-19 14:52:41', 169, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (223, 'paycard', 8000, '00169a', NULL, '2016-08-19 14:52:41', 169, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (224, 'paycard', 8000, '65432', NULL, '2016-08-19 14:52:41', 169, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (225, 'save', 0, '903810277', NULL, '2016-08-19 14:53:48', 170, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (226, 'verify', 0, '903810277', NULL, '2016-08-19 14:53:48', 170, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (227, 'save', 0, '32323', NULL, '2016-08-19 14:53:50', 171, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (228, 'paycard_save', 0, '32323', NULL, '2016-08-19 14:53:52', 171, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (229, 'paycard', 6490, '00171a', NULL, '2016-08-19 14:53:52', 171, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (230, 'paycard', 6490, '32323', NULL, '2016-08-19 14:53:52', 171, 'OK');
SELECT pg_catalog.setval('pay_log_pay_log_id_seq', 230, true);
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (213, 0, 'adphone', '457764874');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (213, 1, 'remote_addr', '10.0.1.89');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (214, 0, 'remote_addr', '10.0.1.89');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (215, 0, 'remote_addr', '10.0.1.89');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (216, 0, 'auth_code', '840840');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (216, 1, 'trans_date', '19/08/2016 14:52:19');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (216, 2, 'placements_type', 'VN');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (216, 3, 'placements_number', '1');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (216, 4, 'pay_type', 'credit');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (216, 5, 'external_response', '0');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (216, 6, 'external_id', '74238303050345015874');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (216, 7, 'cc_number', '1530');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (216, 8, 'remote_addr', '10.0.1.76');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (217, 0, 'webpay', '1');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (217, 1, 'webpay', '1');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (219, 0, 'adphone', '457764874');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (219, 1, 'remote_addr', '10.0.1.89');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (220, 0, 'remote_addr', '10.0.1.89');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (221, 0, 'remote_addr', '10.0.1.89');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (222, 0, 'auth_code', '139468');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (222, 1, 'trans_date', '19/08/2016 14:52:41');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (222, 2, 'placements_type', 'VN');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (222, 3, 'placements_number', '1');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (222, 4, 'pay_type', 'credit');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (222, 5, 'external_response', '0');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (222, 6, 'external_id', '44147117303882974993');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (222, 7, 'cc_number', '8190');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (222, 8, 'remote_addr', '10.0.1.76');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (223, 0, 'webpay', '1');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (223, 1, 'webpay', '1');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (225, 0, 'adphone', '987654321');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (225, 1, 'remote_addr', '10.0.1.89');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (226, 0, 'remote_addr', '10.0.1.89');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (227, 0, 'remote_addr', '10.0.1.89');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (228, 0, 'auth_code', '993994');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (228, 1, 'trans_date', '19/08/2016 14:53:51');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (228, 2, 'placements_type', 'VN');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (228, 3, 'placements_number', '1');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (228, 4, 'pay_type', 'credit');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (228, 5, 'external_response', '0');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (228, 6, 'external_id', '83018554664209972960');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (228, 7, 'cc_number', '4108');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (228, 8, 'remote_addr', '10.0.1.76');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (229, 0, 'webpay', '1');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (229, 1, 'webpay', '1');
SELECT pg_catalog.setval('payment_groups_payment_group_id_seq', 171, true);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (166, 'ad_action', 0, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (167, 'upselling_advanced_inmo', 9890, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (168, 'ad_action', 0, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (169, 'upselling_standard_cars', 8000, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (170, 'ad_action', 0, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (171, 'upselling_premium_others', 6490, 0);
INSERT INTO purchase (purchase_id, doc_type, doc_num, external_doc_id, status, receipt, delivery, rut, name, lob, address, region, communes, contact, email, tax, discount, seller_id, total_price, payment_group_id, payment_method, payment_platform, account_id) VALUES (2, 'bill', NULL, NULL, 'paid', '2016-08-19 14:52:17.972807', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'android@yapo.cl', 0, 0, NULL, 9890, 167, 'webpay', 'desktop', NULL);
INSERT INTO purchase (purchase_id, doc_type, doc_num, external_doc_id, status, receipt, delivery, rut, name, lob, address, region, communes, contact, email, tax, discount, seller_id, total_price, payment_group_id, payment_method, payment_platform, account_id) VALUES (3, 'bill', NULL, NULL, 'paid', '2016-08-19 14:52:39.891233', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'android@yapo.cl', 0, 0, NULL, 8000, 169, 'webpay', 'desktop', NULL);
INSERT INTO purchase (purchase_id, doc_type, doc_num, external_doc_id, status, receipt, delivery, rut, name, lob, address, region, communes, contact, email, tax, discount, seller_id, total_price, payment_group_id, payment_method, payment_platform, account_id) VALUES (4, 'bill', NULL, NULL, 'paid', '2016-08-19 14:53:49.753559', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'user.01@schibsted.cl', 0, 0, NULL, 6490, 171, 'webpay', 'desktop', 7);
INSERT INTO purchase_detail (purchase_detail_id, purchase_id, product_id, price, action_id, ad_id, payment_group_id) VALUES (2, 2, 1022, 9890, 3, 30, 167);
INSERT INTO purchase_detail (purchase_detail_id, purchase_id, product_id, price, action_id, ad_id, payment_group_id) VALUES (3, 3, 1011, 8000, 3, 38, 169);
INSERT INTO purchase_detail (purchase_detail_id, purchase_id, product_id, price, action_id, ad_id, payment_group_id) VALUES (4, 4, 1033, 6490, 3, 122, 171);
SELECT pg_catalog.setval('purchase_detail_purchase_detail_id_seq', 4, true);
SELECT pg_catalog.setval('purchase_purchase_id_seq', 4, true);
INSERT INTO purchase_states (purchase_id, purchase_state_id, status, "timestamp", remote_addr) VALUES (2, 1, 'pending', '2016-08-19 14:52:17.972807', '10.0.1.89');
INSERT INTO purchase_states (purchase_id, purchase_state_id, status, "timestamp", remote_addr) VALUES (3, 2, 'pending', '2016-08-19 14:52:39.891233', '10.0.1.89');
INSERT INTO purchase_states (purchase_id, purchase_state_id, status, "timestamp", remote_addr) VALUES (4, 3, 'pending', '2016-08-19 14:53:49.753559', '10.0.1.89');
SELECT pg_catalog.setval('purchase_states_purchase_state_id_seq', 3, true);
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (30, 2, 700, 'queue', 'autoaccept');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (38, 2, 706, 'queue', 'autoaccept');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (63, 2, 722, 'queue', 'autoaccept');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (122, 2, 712, 'queue', 'autoaccept');
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (93, '2016-08-19 14:52:05.259417', '15749@sasha.schibsted.cl', '2016-08-19 15:22:05.259417', NULL, NULL, NULL, NULL, NULL, 'cmd:review
commit:1
ad_id:30
action_id:2
action:accept
filter_name:autoaccept
remote_addr:10.0.1.89

', NULL, 'AT', 'review', '30');
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (94, '2016-08-19 14:52:20.914782', '15749@sasha.schibsted.cl', '2016-08-19 14:54:20.914782', NULL, NULL, NULL, NULL, NULL, 'cmd:weekly_bump_ad
ad_id:30
action_id:3
counter:4
is_upselling:1
batch:1
commit:1
', NULL, 'upselling', NULL, NULL);
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (95, '2016-08-19 14:52:38.057942', '15749@sasha.schibsted.cl', '2016-08-19 15:22:38.057942', NULL, NULL, NULL, NULL, NULL, 'cmd:review
commit:1
ad_id:38
action_id:2
action:accept
filter_name:autoaccept
remote_addr:10.0.1.89

', NULL, 'AT', 'review', '38');
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (96, '2016-08-19 14:52:41.275135', '15749@sasha.schibsted.cl', '2016-08-19 14:54:41.275135', NULL, NULL, NULL, NULL, NULL, 'cmd:weekly_bump_ad
ad_id:38
action_id:3
counter:7
daily_bump:1
is_upselling:1
batch:1
commit:1
', NULL, 'upselling', NULL, NULL);
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (97, '2016-08-19 14:53:47.68126', '15749@sasha.schibsted.cl', '2016-08-19 15:23:47.68126', NULL, NULL, NULL, NULL, NULL, 'cmd:review
commit:1
ad_id:122
action_id:2
action:accept
filter_name:autoaccept
remote_addr:10.0.1.89

', NULL, 'AT', 'review', '122');
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (98, '2016-08-19 14:53:51.928924', '15749@sasha.schibsted.cl', '2016-08-19 14:55:51.928924', NULL, NULL, NULL, NULL, NULL, 'cmd:bump_ad
batch:0
commit:1
ad_id:122
remote_addr:10.0.1.89
', NULL, 'upselling', NULL, NULL);
SELECT pg_catalog.setval('trans_queue_id_seq', 98, true);
