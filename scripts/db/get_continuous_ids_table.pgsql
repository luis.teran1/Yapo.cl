CREATE OR REPLACE FUNCTION get_continuous_ids_table(x INTEGER, y INTEGER)
RETURNS TABLE (id INTEGER) AS $$
BEGIN
        CREATE TEMP TABLE tmp_get_continuous_ids_table(z INTEGER) ON COMMIT DROP ;
        FOR i IN x..y LOOP
                INSERT INTO tmp_get_continuous_ids_table VALUES (i);
        END LOOP;
        RETURN QUERY SELECT * FROM tmp_get_continuous_ids_table;
END;
$$LANGUAGE plpgsql;
