-- Name: accounts_account_id_seq; Type: SEQUENCE SET; Schema: public; Owner: boris
--

SELECT pg_catalog.setval('accounts_account_id_seq', 2, true);


--
SELECT pg_catalog.setval('action_states_state_id_seq', 272, true);
SELECT pg_catalog.setval('ads_ad_id_seq', 24, true);
SELECT pg_catalog.setval('list_id_seq', 8000004, true);
SELECT pg_catalog.setval('pay_code_seq', 61, true);
SELECT pg_catalog.setval('pay_log_pay_log_id_seq', 45, true);
SELECT pg_catalog.setval('payment_groups_payment_group_id_seq', 78, true);

SELECT pg_catalog.setval('purchase_detail_purchase_detail_id_seq', 12, true);
SELECT pg_catalog.setval('purchase_purchase_id_seq', 9, true);
SELECT pg_catalog.setval('purchase_states_purchase_state_id_seq', 9, true);
SELECT pg_catalog.setval('tokens_token_id_seq', 28, true);
SELECT pg_catalog.setval('trans_queue_id_seq', 14, true);
SELECT pg_catalog.setval('uid_seq', 52, true);
SELECT pg_catalog.setval('users_user_id_seq', 52, true);
SET search_path = bpv, public;


INSERT INTO users VALUES (51, 51, 'usuario1@schibsted.cl', 0, 2000);
INSERT INTO users VALUES (50, 50, 'diego@schibsted.cl', 0, 4000);
-- Data for Name: accounts; Type: TABLE DATA; Schema: public; Owner: boris
--

INSERT INTO accounts VALUES (1, 'diego', '', 'diego@schibsted.cl', '123123', false, 15, '$1024$O-xXY<{ZiW)k^oa+cfef3337ff2e844d9f8de6d431e92d6ce253732e', '2014-03-18 18:54:03.859889', NULL, 'active', NULL, NULL, NULL, NULL, 50);
INSERT INTO accounts VALUES (2, 'Usuario 1', '', 'usuario1@schibsted.cl', '786678', false, 15, '$1024$JAxztg0a7r?~pQI1045b07d2b6cc9d205a03bc0d85eb770697e308b3', '2014-03-18 18:54:27.746922', NULL, 'active', NULL, NULL, NULL, NULL, 51);


--
INSERT INTO ads VALUES (21, 8000000, '2014-03-18 18:58:26.684496', 'active', 'sell', 'diego', '123123', 15, 0, 5020, 50, NULL, false, false, false, 'Que pasaria si trabaj�semos con una se�orita', 'Oh my god!', 123123, NULL, NULL, NULL, '2014-03-18 18:56:46', NULL, NULL, '$1024$O-xXY<{ZiW)k^oa+cfef3337ff2e844d9f8de6d431e92d6ce253732e', '2014-03-18 18:58:26.684496', 'es');
INSERT INTO ads VALUES (22, 8000002, '2014-03-18 18:58:26.696785', 'active', 'sell', 'diego', '123123', 15, 0, 5020, 50, NULL, false, false, false, 'Lorem ipsum', 'ara parafisti some meracaliz', 13123, NULL, NULL, NULL, '2014-03-18 18:56:56', NULL, NULL, '$1024$O-xXY<{ZiW)k^oa+cfef3337ff2e844d9f8de6d431e92d6ce253732e', '2014-03-18 18:58:26.696785', 'es');
INSERT INTO ads VALUES (20, 8000001, '2014-03-18 18:59:33.202527', 'active', 'sell', 'Usuario 1', '786678', 15, 0, 6140, 51, NULL, false, false, false, 'Aviso 0', 'Lorem ipsum dolor sit amet', 123000, NULL, NULL, NULL, '2014-03-18 18:56:49', NULL, NULL, '$1024$JAxztg0a7r?~pQI1045b07d2b6cc9d205a03bc0d85eb770697e308b3', '2014-03-18 18:59:33.202527', 'es');
INSERT INTO ads VALUES (24, 8000004, '2014-03-18 18:59:33.207885', 'active', 'sell', 'Usuario 1', '786678', 15, 0, 3040, 51, NULL, false, false, false, 'Aviso de computador', 'Lorem ipsum dolor sit amet', 123000, NULL, NULL, NULL, '2014-03-18 18:57:20', NULL, NULL, '$1024$JAxztg0a7r?~pQI1045b07d2b6cc9d205a03bc0d85eb770697e308b3', '2014-03-18 18:59:33.207885', 'es');
INSERT INTO ads VALUES (23, 8000003, '2014-03-18 18:59:43.673901', 'active', 'sell', 'diego', '123123', 15, 0, 5020, 50, NULL, false, false, false, 'Parangar� cutirimicuaro', 'chipiris pipiris tripa de pollo guares dei pikis rokis tumei tumei', 123123, NULL, NULL, NULL, '2014-03-18 18:56:59', NULL, NULL, '$1024$O-xXY<{ZiW)k^oa+cfef3337ff2e844d9f8de6d431e92d6ce253732e', '2014-03-18 18:59:43.673901', 'es');
INSERT INTO payment_groups VALUES (66, '112200000', 'verified', '2014-03-18 18:54:27.6933', NULL);
INSERT INTO payment_groups VALUES (67, '112200001', 'verified', '2014-03-18 18:54:42.643552', NULL);
INSERT INTO payment_groups VALUES (68, '112200002', 'verified', '2014-03-18 18:55:14.065877', NULL);
INSERT INTO payment_groups VALUES (69, '112200003', 'verified', '2014-03-18 18:55:55.22032', NULL);
INSERT INTO payment_groups VALUES (70, '112200004', 'verified', '2014-03-18 18:56:47.734374', NULL);
INSERT INTO payment_groups VALUES (71, '90003', 'paid', '2014-03-18 18:58:24.543932', NULL);
INSERT INTO payment_groups VALUES (72, '90004', 'paid', '2014-03-18 18:58:24.543932', 71);
INSERT INTO payment_groups VALUES (73, '90005', 'paid', '2014-03-18 18:58:24.543932', 71);
INSERT INTO payment_groups VALUES (74, '90006', 'paid', '2014-03-18 18:58:24.543932', 71);
INSERT INTO payment_groups VALUES (75, '90007', 'paid', '2014-03-18 18:59:30.152061', NULL);
INSERT INTO payment_groups VALUES (76, '90008', 'paid', '2014-03-18 18:59:30.152061', 75);
INSERT INTO payment_groups VALUES (77, '90009', 'paid', '2014-03-18 18:59:30.152061', 75);
INSERT INTO payment_groups VALUES (78, '90010', 'paid', '2014-03-18 18:59:36.184888', NULL);
INSERT INTO ad_actions VALUES (20, 2, 'bump', 267, 'accepted', 'normal', NULL, NULL, 77);
INSERT INTO ad_actions VALUES (24, 2, 'bump', 268, 'accepted', 'normal', NULL, NULL, 76);
INSERT INTO ad_actions VALUES (23, 3, 'bump', 272, 'accepted', 'normal', NULL, NULL, 78);
INSERT INTO ad_actions VALUES (21, 1, 'new', 238, 'accepted', 'normal', 9, '2014-03-18 19:01:33.864628', 67);
INSERT INTO ad_actions VALUES (20, 1, 'new', 242, 'accepted', 'normal', 9, '2014-03-18 19:01:33.864628', 66);
INSERT INTO ad_actions VALUES (22, 1, 'new', 245, 'accepted', 'normal', 9, '2014-03-18 19:01:52.661223', 68);
INSERT INTO ad_actions VALUES (23, 1, 'new', 246, 'accepted', 'normal', 9, '2014-03-18 19:01:52.661223', 69);
INSERT INTO ad_actions VALUES (24, 1, 'new', 248, 'accepted', 'normal', 9, '2014-03-18 19:02:01.236312', 70);
INSERT INTO ad_actions VALUES (21, 2, 'bump', 258, 'accepted', 'normal', NULL, NULL, 74);
INSERT INTO ad_actions VALUES (22, 2, 'bump', 259, 'accepted', 'normal', NULL, NULL, 73);
INSERT INTO ad_actions VALUES (23, 2, 'bump', 260, 'accepted', 'normal', NULL, NULL, 72);
INSERT INTO action_params VALUES (20, 1, 'source', 'web');
INSERT INTO action_params VALUES (20, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params VALUES (21, 1, 'source', 'web');
INSERT INTO action_params VALUES (21, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params VALUES (22, 1, 'source', 'web');
INSERT INTO action_params VALUES (22, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params VALUES (23, 1, 'source', 'web');
INSERT INTO action_params VALUES (23, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params VALUES (24, 1, 'source', 'web');
INSERT INTO action_params VALUES (24, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO tokens VALUES (1, 'X5328c10c67f87cc2000000001135a46900000000', 9, '2014-03-18 18:56:28.33148', '2014-03-18 18:56:28.570823', '10.0.1.167', 'authenticate', NULL, NULL);
INSERT INTO tokens VALUES (2, 'X5328c10dacd0f7c0000000077fc562b00000000', 9, '2014-03-18 18:56:28.570823', '2014-03-18 18:56:31.829306', '10.0.1.167', 'spamfilter', NULL, NULL);
INSERT INTO tokens VALUES (3, 'X5328c1105b5ee453000000007ef7d70d00000000', 9, '2014-03-18 18:56:31.829306', '2014-03-18 18:56:33.859867', '10.0.1.167', 'adqueues', NULL, NULL);
INSERT INTO tokens VALUES (4, 'X5328c11246e81841000000004a16170f00000000', 9, '2014-03-18 18:56:33.859867', '2014-03-18 18:56:33.970052', '10.0.1.167', 'adqueues', NULL, NULL);
INSERT INTO tokens VALUES (5, 'X5328c11267a59bea000000007d0410c000000000', 9, '2014-03-18 18:56:33.970052', '2014-03-18 18:56:33.97771', '10.0.1.167', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (6, 'X5328c112627a1f5000000006bfacbc000000000', 9, '2014-03-18 18:56:33.97771', '2014-03-18 18:56:33.98595', '10.0.1.167', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (7, 'X5328c1124654b0d3000000004b40d58000000000', 9, '2014-03-18 18:56:33.98595', '2014-03-18 18:56:34.00494', '10.0.1.167', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (8, 'X5328c112104ce9490000000030bff57b00000000', 9, '2014-03-18 18:56:34.00494', '2014-03-18 18:56:34.012457', '10.0.1.167', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (9, 'X5328c11213ae402800000000b17d6fd00000000', 9, '2014-03-18 18:56:34.012457', '2014-03-18 18:56:34.020065', '10.0.1.167', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (10, 'X5328c11222e1e3f300000000469b93da00000000', 9, '2014-03-18 18:56:34.020065', '2014-03-18 18:56:46.521049', '10.0.1.167', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (11, 'X5328c11f1fa654a0000000006ac285ec00000000', 9, '2014-03-18 18:56:46.521049', '2014-03-18 18:56:49.009264', '10.0.1.167', 'review', NULL, NULL);
INSERT INTO tokens VALUES (12, 'X5328c1211bfdcef0000000071a7738800000000', 9, '2014-03-18 18:56:49.009264', '2014-03-18 18:56:52.65041', '10.0.1.167', 'review', NULL, NULL);
INSERT INTO tokens VALUES (13, 'X5328c1256d32cf9500000000c5a623900000000', 9, '2014-03-18 18:56:52.65041', '2014-03-18 18:56:52.755517', '10.0.1.167', 'adqueues', NULL, NULL);
INSERT INTO tokens VALUES (14, 'X5328c125b93a3d50000000073b6a76a00000000', 9, '2014-03-18 18:56:52.755517', '2014-03-18 18:56:52.763457', '10.0.1.167', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (15, 'X5328c1255a9b735a000000002c994a2a00000000', 9, '2014-03-18 18:56:52.763457', '2014-03-18 18:56:52.772039', '10.0.1.167', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (16, 'X5328c1253de220a60000000051d6a39400000000', 9, '2014-03-18 18:56:52.772039', '2014-03-18 18:56:52.793307', '10.0.1.167', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (17, 'X5328c12561d1e36b00000000e75e30e00000000', 9, '2014-03-18 18:56:52.793307', '2014-03-18 18:56:52.800888', '10.0.1.167', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (18, 'X5328c125635cd0a800000000a3b48d500000000', 9, '2014-03-18 18:56:52.800888', '2014-03-18 18:56:52.809166', '10.0.1.167', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (19, 'X5328c125f12630400000000450cc2f100000000', 9, '2014-03-18 18:56:52.809166', '2014-03-18 18:56:56.541108', '10.0.1.167', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (20, 'X5328c12969cca4ed000000005975523400000000', 9, '2014-03-18 18:56:56.541108', '2014-03-18 18:56:59.347403', '10.0.1.167', 'review', NULL, NULL);
INSERT INTO tokens VALUES (21, 'X5328c12b693cc790000000005b4b2d0500000000', 9, '2014-03-18 18:56:59.347403', '2014-03-18 18:57:01.231865', '10.0.1.167', 'review', NULL, NULL);
INSERT INTO tokens VALUES (22, 'X5328c12d4546ff28000000003aabc5d000000000', 9, '2014-03-18 18:57:01.231865', '2014-03-18 18:57:01.278955', '10.0.1.167', 'adqueues', NULL, NULL);
INSERT INTO tokens VALUES (23, 'X5328c12d4f7f639200000000767a66c600000000', 9, '2014-03-18 18:57:01.278955', '2014-03-18 18:57:01.28667', '10.0.1.167', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (24, 'X5328c12d14e83f51000000004812e57300000000', 9, '2014-03-18 18:57:01.28667', '2014-03-18 18:57:01.294961', '10.0.1.167', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (25, 'X5328c12d5ce92e600000000672471f600000000', 9, '2014-03-18 18:57:01.294961', '2014-03-18 18:57:20.88745', '10.0.1.167', 'block_list', NULL, NULL);
INSERT INTO tokens VALUES (26, 'X5328c1412cea6ae800000000306c740f00000000', 9, '2014-03-18 18:57:20.88745', '2014-03-18 18:57:23.100257', '10.0.1.167', 'review', NULL, NULL);
INSERT INTO tokens VALUES (27, 'X5328c14327594cc400000000621e2d1e00000000', 9, '2014-03-18 18:57:23.100257', '2014-03-18 18:57:23.121089', '10.0.1.167', 'adqueues', NULL, NULL);
INSERT INTO tokens VALUES (28, 'X5328c1431a0d8440000000024fd6f4700000000', 9, '2014-03-18 18:57:23.121089', '2014-03-18 19:57:23.121089', '10.0.1.167', 'adqueues', NULL, NULL);
INSERT INTO action_states VALUES (20, 1, 224, 'reg', 'initial', '2014-03-18 18:54:27.6933', '10.0.1.188', NULL);
INSERT INTO action_states VALUES (20, 1, 225, 'unverified', 'verifymail', '2014-03-18 18:54:27.6933', '10.0.1.188', NULL);
INSERT INTO action_states VALUES (20, 1, 226, 'pending_review', 'verify', '2014-03-18 18:54:27.811511', '10.0.1.188', NULL);
INSERT INTO action_states VALUES (21, 1, 227, 'reg', 'initial', '2014-03-18 18:54:42.643552', '10.0.1.167', NULL);
INSERT INTO action_states VALUES (21, 1, 228, 'unverified', 'verifymail', '2014-03-18 18:54:42.643552', '10.0.1.167', NULL);
INSERT INTO action_states VALUES (21, 1, 229, 'pending_review', 'verify', '2014-03-18 18:54:42.705902', '10.0.1.167', NULL);
INSERT INTO action_states VALUES (22, 1, 230, 'reg', 'initial', '2014-03-18 18:55:14.065877', '10.0.1.167', NULL);
INSERT INTO action_states VALUES (22, 1, 231, 'unverified', 'verifymail', '2014-03-18 18:55:14.065877', '10.0.1.167', NULL);
INSERT INTO action_states VALUES (22, 1, 232, 'pending_review', 'verify', '2014-03-18 18:55:14.083794', '10.0.1.167', NULL);
INSERT INTO action_states VALUES (23, 1, 233, 'reg', 'initial', '2014-03-18 18:55:55.22032', '10.0.1.167', NULL);
INSERT INTO action_states VALUES (23, 1, 234, 'unverified', 'verifymail', '2014-03-18 18:55:55.22032', '10.0.1.167', NULL);
INSERT INTO action_states VALUES (23, 1, 235, 'pending_review', 'verify', '2014-03-18 18:55:55.245879', '10.0.1.167', NULL);
INSERT INTO action_states VALUES (20, 1, 236, 'locked', 'checkout', '2014-03-18 18:56:33.864628', '10.0.1.167', 3);
INSERT INTO action_states VALUES (21, 1, 237, 'locked', 'checkout', '2014-03-18 18:56:33.864628', '10.0.1.167', 3);
INSERT INTO action_states VALUES (21, 1, 238, 'accepted', 'accept', '2014-03-18 18:56:46.524565', '10.0.1.167', 10);
INSERT INTO action_states VALUES (24, 1, 239, 'reg', 'initial', '2014-03-18 18:56:47.734374', '10.0.1.188', NULL);
INSERT INTO action_states VALUES (24, 1, 240, 'unverified', 'verifymail', '2014-03-18 18:56:47.734374', '10.0.1.188', NULL);
INSERT INTO action_states VALUES (24, 1, 241, 'pending_review', 'verify', '2014-03-18 18:56:47.754047', '10.0.1.188', NULL);
INSERT INTO action_states VALUES (20, 1, 242, 'accepted', 'accept', '2014-03-18 18:56:49.013701', '10.0.1.167', 11);
INSERT INTO action_states VALUES (22, 1, 243, 'locked', 'checkout', '2014-03-18 18:56:52.661223', '10.0.1.167', 12);
INSERT INTO action_states VALUES (23, 1, 244, 'locked', 'checkout', '2014-03-18 18:56:52.661223', '10.0.1.167', 12);
INSERT INTO action_states VALUES (22, 1, 245, 'accepted', 'accept', '2014-03-18 18:56:56.547427', '10.0.1.167', 19);
INSERT INTO action_states VALUES (23, 1, 246, 'accepted', 'accept', '2014-03-18 18:56:59.353539', '10.0.1.167', 20);
INSERT INTO action_states VALUES (24, 1, 247, 'locked', 'checkout', '2014-03-18 18:57:01.236312', '10.0.1.167', 21);
INSERT INTO action_states VALUES (24, 1, 248, 'accepted', 'accept', '2014-03-18 18:57:20.89315', '10.0.1.167', 25);
INSERT INTO action_states VALUES (23, 2, 249, 'reg', 'initial', '2014-03-18 18:58:24.543932', '10.0.1.167', NULL);
INSERT INTO action_states VALUES (23, 2, 250, 'unpaid', 'pending_pay', '2014-03-18 18:58:24.543932', '10.0.1.167', NULL);
INSERT INTO action_states VALUES (22, 2, 251, 'reg', 'initial', '2014-03-18 18:58:24.543932', '10.0.1.167', NULL);
INSERT INTO action_states VALUES (22, 2, 252, 'unpaid', 'pending_pay', '2014-03-18 18:58:24.543932', '10.0.1.167', NULL);
INSERT INTO action_states VALUES (21, 2, 253, 'reg', 'initial', '2014-03-18 18:58:24.543932', '10.0.1.167', NULL);
INSERT INTO action_states VALUES (21, 2, 254, 'unpaid', 'pending_pay', '2014-03-18 18:58:24.543932', '10.0.1.167', NULL);
INSERT INTO action_states VALUES (23, 2, 255, 'pending_bump', 'pay', '2014-03-18 18:58:26.547038', NULL, NULL);
INSERT INTO action_states VALUES (22, 2, 256, 'pending_bump', 'pay', '2014-03-18 18:58:26.575657', NULL, NULL);
INSERT INTO action_states VALUES (21, 2, 257, 'pending_bump', 'pay', '2014-03-18 18:58:26.588119', NULL, NULL);
INSERT INTO action_states VALUES (21, 2, 258, 'accepted', 'bump', '2014-03-18 18:58:26.684496', NULL, NULL);
INSERT INTO action_states VALUES (22, 2, 259, 'accepted', 'bump', '2014-03-18 18:58:26.696785', NULL, NULL);
INSERT INTO action_states VALUES (23, 2, 260, 'accepted', 'bump', '2014-03-18 18:58:26.702802', NULL, NULL);
INSERT INTO action_states VALUES (24, 2, 261, 'reg', 'initial', '2014-03-18 18:59:30.152061', '10.0.1.188', NULL);
INSERT INTO action_states VALUES (24, 2, 262, 'unpaid', 'pending_pay', '2014-03-18 18:59:30.152061', '10.0.1.188', NULL);
INSERT INTO action_states VALUES (20, 2, 263, 'reg', 'initial', '2014-03-18 18:59:30.152061', '10.0.1.188', NULL);
INSERT INTO action_states VALUES (20, 2, 264, 'unpaid', 'pending_pay', '2014-03-18 18:59:30.152061', '10.0.1.188', NULL);
INSERT INTO action_states VALUES (24, 2, 265, 'pending_bump', 'pay', '2014-03-18 18:59:33.10823', NULL, NULL);
INSERT INTO action_states VALUES (20, 2, 266, 'pending_bump', 'pay', '2014-03-18 18:59:33.131586', NULL, NULL);
INSERT INTO action_states VALUES (20, 2, 267, 'accepted', 'bump', '2014-03-18 18:59:33.202527', NULL, NULL);
INSERT INTO action_states VALUES (24, 2, 268, 'accepted', 'bump', '2014-03-18 18:59:33.207885', NULL, NULL);
INSERT INTO action_states VALUES (23, 3, 269, 'reg', 'initial', '2014-03-18 18:59:36.184888', '10.0.1.167', NULL);
INSERT INTO action_states VALUES (23, 3, 270, 'unpaid', 'pending_pay', '2014-03-18 18:59:36.184888', '10.0.1.167', NULL);
INSERT INTO action_states VALUES (23, 3, 271, 'pending_bump', 'pay', '2014-03-18 18:59:43.56514', NULL, NULL);
INSERT INTO action_states VALUES (23, 3, 272, 'accepted', 'bump', '2014-03-18 18:59:43.673901', NULL, NULL);
INSERT INTO ad_codes VALUES (35311, 'pay');
INSERT INTO ad_codes VALUES (67334, 'pay');
INSERT INTO ad_codes VALUES (99357, 'pay');
INSERT INTO ad_codes VALUES (41380, 'pay');
INSERT INTO ad_codes VALUES (73403, 'pay');
INSERT INTO ad_image_changes VALUES (21, 1, 238, 0, NULL, false);
INSERT INTO ad_image_changes VALUES (20, 1, 242, 0, NULL, false);
INSERT INTO ad_image_changes VALUES (22, 1, 245, 0, NULL, false);
INSERT INTO ad_image_changes VALUES (23, 1, 246, 0, NULL, false);
INSERT INTO ad_image_changes VALUES (24, 1, 248, 0, NULL, false);
INSERT INTO ad_params VALUES (20, 'prev_currency', 'peso');
INSERT INTO ad_params VALUES (20, 'country', 'UNK');
INSERT INTO ad_params VALUES (21, 'prev_currency', 'peso');
INSERT INTO ad_params VALUES (21, 'country', 'UNK');
INSERT INTO ad_params VALUES (22, 'prev_currency', 'peso');
INSERT INTO ad_params VALUES (22, 'country', 'UNK');
INSERT INTO ad_params VALUES (23, 'prev_currency', 'peso');
INSERT INTO ad_params VALUES (23, 'country', 'UNK');
INSERT INTO ad_params VALUES (24, 'prev_currency', 'peso');
INSERT INTO ad_params VALUES (24, 'country', 'UNK');
INSERT INTO pay_log VALUES (19, 'save', 0, '112200000', NULL, '2014-03-18 18:54:28', 66, 'SAVE');
INSERT INTO pay_log VALUES (20, 'verify', 0, '112200000', NULL, '2014-03-18 18:54:28', 66, 'OK');
INSERT INTO pay_log VALUES (21, 'save', 0, '112200001', NULL, '2014-03-18 18:54:43', 67, 'SAVE');
INSERT INTO pay_log VALUES (22, 'verify', 0, '112200001', NULL, '2014-03-18 18:54:43', 67, 'OK');
INSERT INTO pay_log VALUES (23, 'save', 0, '112200002', NULL, '2014-03-18 18:55:14', 68, 'SAVE');
INSERT INTO pay_log VALUES (24, 'verify', 0, '112200002', NULL, '2014-03-18 18:55:14', 68, 'OK');
INSERT INTO pay_log VALUES (25, 'save', 0, '112200003', NULL, '2014-03-18 18:55:55', 69, 'SAVE');
INSERT INTO pay_log VALUES (26, 'verify', 0, '112200003', NULL, '2014-03-18 18:55:55', 69, 'OK');
INSERT INTO pay_log VALUES (27, 'save', 0, '112200004', NULL, '2014-03-18 18:56:48', 70, 'SAVE');
INSERT INTO pay_log VALUES (28, 'verify', 0, '112200004', NULL, '2014-03-18 18:56:48', 70, 'OK');
INSERT INTO pay_log VALUES (29, 'save', 0, '90004', NULL, '2014-03-18 18:58:25', 72, 'SAVE');
INSERT INTO pay_log VALUES (30, 'save', 0, '90005', NULL, '2014-03-18 18:58:25', 73, 'SAVE');
INSERT INTO pay_log VALUES (31, 'save', 0, '90006', NULL, '2014-03-18 18:58:25', 74, 'SAVE');
INSERT INTO pay_log VALUES (32, 'paycard_save', 0, '90003', NULL, '2014-03-18 18:58:26', 71, 'SAVE');
INSERT INTO pay_log VALUES (33, 'paycard', 1000, '0072a', NULL, '2014-03-18 18:58:27', 72, 'OK');
INSERT INTO pay_log VALUES (34, 'paycard', 1000, '0073a', NULL, '2014-03-18 18:58:27', 73, 'OK');
INSERT INTO pay_log VALUES (35, 'paycard', 1000, '0074a', NULL, '2014-03-18 18:58:27', 74, 'OK');
INSERT INTO pay_log VALUES (36, 'paycard', 3000, '90003', NULL, '2014-03-18 18:58:27', 71, 'OK');
INSERT INTO pay_log VALUES (37, 'save', 0, '90008', NULL, '2014-03-18 18:59:30', 76, 'SAVE');
INSERT INTO pay_log VALUES (38, 'save', 0, '90009', NULL, '2014-03-18 18:59:30', 77, 'SAVE');
INSERT INTO pay_log VALUES (39, 'paycard_save', 0, '90007', NULL, '2014-03-18 18:59:33', 75, 'SAVE');
INSERT INTO pay_log VALUES (40, 'paycard', 1000, '0076a', NULL, '2014-03-18 18:59:33', 76, 'OK');
INSERT INTO pay_log VALUES (41, 'paycard', 1000, '0077a', NULL, '2014-03-18 18:59:33', 77, 'OK');
INSERT INTO pay_log VALUES (42, 'paycard', 2000, '90007', NULL, '2014-03-18 18:59:33', 75, 'OK');
INSERT INTO pay_log VALUES (43, 'save', 0, '90010', NULL, '2014-03-18 18:59:36', 78, 'SAVE');
INSERT INTO pay_log VALUES (44, 'paycard_save', 0, '90010', NULL, '2014-03-18 18:59:43', 78, 'SAVE');
INSERT INTO pay_log VALUES (45, 'paycard', 1000, '0078a', NULL, '2014-03-18 18:59:44', 78, 'OK');
INSERT INTO pay_log_references VALUES (19, 0, 'adphone', '786678');
INSERT INTO pay_log_references VALUES (19, 1, 'remote_addr', '10.0.1.188');
INSERT INTO pay_log_references VALUES (20, 0, 'remote_addr', '10.0.1.188');
INSERT INTO pay_log_references VALUES (21, 0, 'adphone', '123123');
INSERT INTO pay_log_references VALUES (21, 1, 'remote_addr', '10.0.1.167');
INSERT INTO pay_log_references VALUES (22, 0, 'remote_addr', '10.0.1.167');
INSERT INTO pay_log_references VALUES (23, 0, 'adphone', '123123');
INSERT INTO pay_log_references VALUES (23, 1, 'remote_addr', '10.0.1.167');
INSERT INTO pay_log_references VALUES (24, 0, 'remote_addr', '10.0.1.167');
INSERT INTO pay_log_references VALUES (25, 0, 'adphone', '123123');
INSERT INTO pay_log_references VALUES (25, 1, 'remote_addr', '10.0.1.167');
INSERT INTO pay_log_references VALUES (26, 0, 'remote_addr', '10.0.1.167');
INSERT INTO pay_log_references VALUES (27, 0, 'adphone', '786678');
INSERT INTO pay_log_references VALUES (27, 1, 'remote_addr', '10.0.1.188');
INSERT INTO pay_log_references VALUES (28, 0, 'remote_addr', '10.0.1.188');
INSERT INTO pay_log_references VALUES (29, 0, 'remote_addr', '10.0.1.167');
INSERT INTO pay_log_references VALUES (30, 0, 'remote_addr', '10.0.1.167');
INSERT INTO pay_log_references VALUES (31, 0, 'remote_addr', '10.0.1.167');
INSERT INTO pay_log_references VALUES (32, 0, 'auth_code', '166175');
INSERT INTO pay_log_references VALUES (32, 1, 'trans_date', '18/03/2014 18:58:25');
INSERT INTO pay_log_references VALUES (32, 2, 'placements_type', 'VN');
INSERT INTO pay_log_references VALUES (32, 3, 'placements_number', '1');
INSERT INTO pay_log_references VALUES (32, 4, 'pay_type', 'credit');
INSERT INTO pay_log_references VALUES (32, 5, 'external_response', '0');
INSERT INTO pay_log_references VALUES (32, 6, 'external_id', '09012209163906863729');
INSERT INTO pay_log_references VALUES (32, 7, 'remote_addr', '127.0.0.1');
INSERT INTO pay_log_references VALUES (33, 0, 'webpay', '1');
INSERT INTO pay_log_references VALUES (34, 0, 'webpay', '1');
INSERT INTO pay_log_references VALUES (35, 0, 'webpay', '1');
INSERT INTO pay_log_references VALUES (37, 0, 'remote_addr', '10.0.1.188');
INSERT INTO pay_log_references VALUES (38, 0, 'remote_addr', '10.0.1.188');
INSERT INTO pay_log_references VALUES (39, 0, 'auth_code', '413335');
INSERT INTO pay_log_references VALUES (39, 1, 'trans_date', '18/03/2014 18:59:32');
INSERT INTO pay_log_references VALUES (39, 2, 'placements_type', 'VN');
INSERT INTO pay_log_references VALUES (39, 3, 'placements_number', '1');
INSERT INTO pay_log_references VALUES (39, 4, 'pay_type', 'credit');
INSERT INTO pay_log_references VALUES (39, 5, 'external_response', '0');
INSERT INTO pay_log_references VALUES (39, 6, 'external_id', '20622327169597102663');
INSERT INTO pay_log_references VALUES (39, 7, 'remote_addr', '127.0.0.1');
INSERT INTO pay_log_references VALUES (40, 0, 'webpay', '1');
INSERT INTO pay_log_references VALUES (41, 0, 'webpay', '1');
INSERT INTO pay_log_references VALUES (43, 0, 'remote_addr', '10.0.1.167');
INSERT INTO pay_log_references VALUES (44, 0, 'auth_code', '189364');
INSERT INTO pay_log_references VALUES (44, 1, 'trans_date', '18/03/2014 18:59:42');
INSERT INTO pay_log_references VALUES (44, 2, 'placements_type', 'VN');
INSERT INTO pay_log_references VALUES (44, 3, 'placements_number', '1');
INSERT INTO pay_log_references VALUES (44, 4, 'pay_type', 'credit');
INSERT INTO pay_log_references VALUES (44, 5, 'external_response', '0');
INSERT INTO pay_log_references VALUES (44, 6, 'external_id', '41644512800897827209');
INSERT INTO pay_log_references VALUES (44, 7, 'remote_addr', '127.0.0.1');
INSERT INTO pay_log_references VALUES (45, 0, 'webpay', '1');
INSERT INTO payments VALUES (66, 'ad_action', 0, 0);
INSERT INTO payments VALUES (67, 'ad_action', 0, 0);
INSERT INTO payments VALUES (68, 'ad_action', 0, 0);
INSERT INTO payments VALUES (69, 'ad_action', 0, 0);
INSERT INTO payments VALUES (70, 'ad_action', 0, 0);
INSERT INTO payments VALUES (72, 'bump', 1000, 0);
INSERT INTO payments VALUES (73, 'bump', 1000, 0);
INSERT INTO payments VALUES (74, 'bump', 1000, 0);
INSERT INTO payments VALUES (76, 'bump', 1000, 0);
INSERT INTO payments VALUES (77, 'bump', 1000, 0);
INSERT INTO payments VALUES (78, 'bump', 1000, 0);


INSERT INTO purchase VALUES (7, 'bill', 5, NULL, 'confirmed', '2014-03-18 18:58:24.543932', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'diego@schibsted.cl', 0, 0, NULL, 3000, 71, 'oneclick');
INSERT INTO purchase VALUES (8, 'bill', 6, NULL, 'confirmed', '2014-03-18 18:59:30.152061', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'usuario1@schibsted.cl', 0, 0, NULL, 2000, 75, 'oneclick');
INSERT INTO purchase VALUES (9, 'bill', NULL, NULL, 'confirmed', '2014-03-20 18:59:36.184888', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'diego@schibsted.cl', 0, 0, NULL, 1000, 78, 'oneclick');
INSERT INTO purchase_detail VALUES (7, 7, 1, 1000, 2, 23);
INSERT INTO purchase_detail VALUES (8, 7, 1, 1000, 2, 22);
INSERT INTO purchase_detail VALUES (9, 7, 1, 1000, 2, 21);
INSERT INTO purchase_detail VALUES (10, 8, 1, 1000, 2, 24);
INSERT INTO purchase_detail VALUES (11, 8, 1, 1000, 2, 20);
INSERT INTO purchase_detail VALUES (12, 9, 1, 1000, 3, 23);
INSERT INTO purchase_states VALUES (7, 7, 'pending', '2014-03-18 18:58:24.543932', '10.0.1.167');
INSERT INTO purchase_states VALUES (8, 8, 'pending', '2014-03-18 18:59:30.152061', '10.0.1.188');
INSERT INTO purchase_states VALUES (9, 9, 'pending', '2014-03-18 18:59:36.184888', '10.0.1.167');
INSERT INTO review_log VALUES (21, 1, 9, '2014-03-18 18:56:46.524565', 'normal', 'new', 'accepted', NULL, 5020);
INSERT INTO review_log VALUES (20, 1, 9, '2014-03-18 18:56:49.013701', 'normal', 'new', 'accepted', NULL, 6140);
INSERT INTO review_log VALUES (22, 1, 9, '2014-03-18 18:56:56.547427', 'normal', 'new', 'accepted', NULL, 5020);
INSERT INTO review_log VALUES (23, 1, 9, '2014-03-18 18:56:59.353539', 'normal', 'new', 'accepted', NULL, 5020);
INSERT INTO review_log VALUES (24, 1, 9, '2014-03-18 18:57:20.89315', 'normal', 'new', 'accepted', NULL, 3040);
INSERT INTO state_params VALUES (21, 1, 238, 'filter_name', 'normal');
INSERT INTO state_params VALUES (20, 1, 242, 'filter_name', 'normal');
INSERT INTO state_params VALUES (22, 1, 245, 'filter_name', 'normal');
INSERT INTO state_params VALUES (23, 1, 246, 'filter_name', 'normal');
INSERT INTO state_params VALUES (24, 1, 248, 'filter_name', 'normal');
INSERT INTO user_params VALUES (50, 'first_approved_ad', '2014-03-18 18:56:46.524565-03');
INSERT INTO user_params VALUES (51, 'first_approved_ad', '2014-03-18 18:56:49.013701-03');
INSERT INTO user_params VALUES (50, 'first_inserted_ad', '2014-03-18 18:56:46.524565-03');
INSERT INTO user_params VALUES (51, 'first_inserted_ad', '2014-03-18 18:56:49.013701-03');
INSERT INTO ad_params VALUES (21, 'communes', '344');
