INSERT INTO account_actions (account_id, account_action_id, account_action_type, "timestamp", token_id) VALUES (7, 1, 'edition', '2016-07-27 11:51:03.362755', NULL);
INSERT INTO account_actions (account_id, account_action_id, account_action_type, "timestamp", token_id) VALUES (7, 2, 'edition', '2016-07-27 11:52:22.572948', NULL);
INSERT INTO account_actions (account_id, account_action_id, account_action_type, "timestamp", token_id) VALUES (7, 3, 'edition', '2016-07-27 11:53:17.052026', NULL);
INSERT INTO account_actions (account_id, account_action_id, account_action_type, "timestamp", token_id) VALUES (7, 4, 'edition', '2016-07-27 11:54:33.581369', NULL);

INSERT INTO ads VALUES (126, NULL, NULL, 'inactive', 'sell', 'Usuario 01', '987654321', 15, 0, 8020, 58, NULL, false, false, false, 'Aviso upselling b�sico', 'asdasd ads asd', 123123, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$2y1bQ}VYRD}zF4QCbf07a971b784888658a20e62156de64dfde5ffb6', NULL, 'es');
INSERT INTO ads VALUES (127, NULL, NULL, 'inactive', 'sell', 'Usuario 01', '987654321', 15, 0, 8020, 58, NULL, false, false, false, 'Aviso con upselling estandar', 'asdasd asd ad as', 123123, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$2y1bQ}VYRD}zF4QCbf07a971b784888658a20e62156de64dfde5ffb6', NULL, 'es');
INSERT INTO ads VALUES (128, NULL, NULL, 'inactive', 'sell', 'Usuario 01', '987654321', 15, 0, 8020, 58, NULL, false, false, false, 'Aviso con upselling avanzado', 'asd asd asd as', 123123, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$2y1bQ}VYRD}zF4QCbf07a971b784888658a20e62156de64dfde5ffb6', NULL, 'es');
INSERT INTO ads VALUES (129, NULL, NULL, 'inactive', 'sell', 'Usuario 01', '987654321', 15, 0, 8020, 58, NULL, false, false, false, 'Aviso con upselling premium', 'asdasd asad s', 123123, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$2y1bQ}VYRD}zF4QCbf07a971b784888658a20e62156de64dfde5ffb6', NULL, 'es');

INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (166, '633026231', 'verified', '2016-07-27 11:51:03.312454', NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (167, '768418254', 'verified', '2016-07-27 11:52:22.547416', NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (168, '12345', 'paid', '2016-07-27 11:52:26.204342', NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (169, '903810277', 'verified', '2016-07-27 11:53:17.028697', NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (170, '65432', 'paid', '2016-07-27 11:53:22.98944', NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (171, '139202300', 'verified', '2016-07-27 11:54:33.554909', NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (172, '32323', 'paid', '2016-07-27 11:54:36.401652', NULL);

INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (126, 1, 'new', 700, 'pending_review', 'normal', NULL, NULL, 166);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (127, 1, 'new', 703, 'pending_review', 'normal', NULL, NULL, 167);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (127, 2, 'upselling_standard_others', 706, 'accepted', 'normal', NULL, NULL, 168);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (128, 1, 'new', 709, 'pending_review', 'normal', NULL, NULL, 169);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (128, 2, 'upselling_advanced_others', 712, 'accepted', 'normal', NULL, NULL, 170);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (128, 3, 'gallery', 714, 'accepted', 'normal', NULL, NULL, NULL);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (129, 1, 'new', 717, 'pending_review', 'normal', NULL, NULL, 171);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (129, 2, 'upselling_premium_others', 720, 'accepted', 'normal', NULL, NULL, 172);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (129, 3, 'gallery', 722, 'accepted', 'normal', NULL, NULL, NULL);


INSERT INTO action_params (ad_id, action_id, name, value) VALUES (126, 1, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (126, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (127, 1, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (127, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (128, 1, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (128, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (129, 1, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (129, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (129, 2, 'label_type', 'urgent');


INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (126, 1, 698, 'reg', 'initial', '2016-07-27 11:51:03.312454', '10.0.2.118', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (126, 1, 699, 'unverified', 'verifymail', '2016-07-27 11:51:03.312454', '10.0.2.118', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (126, 1, 700, 'pending_review', 'verify', '2016-07-27 11:51:03.399911', '10.0.2.118', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (127, 1, 701, 'reg', 'initial', '2016-07-27 11:52:22.547416', '10.0.2.118', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (127, 1, 702, 'unverified', 'verifymail', '2016-07-27 11:52:22.547416', '10.0.2.118', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (127, 1, 703, 'pending_review', 'verify', '2016-07-27 11:52:22.594213', '10.0.2.118', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (127, 2, 704, 'reg', 'initial', '2016-07-27 11:52:26.204342', '10.0.2.118', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (127, 2, 705, 'unpaid', 'pending_pay', '2016-07-27 11:52:26.204342', '10.0.2.118', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (127, 2, 706, 'accepted', 'pay', '2016-07-27 11:52:28.068606', NULL, NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (128, 1, 707, 'reg', 'initial', '2016-07-27 11:53:17.028697', '10.0.2.118', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (128, 1, 708, 'unverified', 'verifymail', '2016-07-27 11:53:17.028697', '10.0.2.118', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (128, 1, 709, 'pending_review', 'verify', '2016-07-27 11:53:17.075147', '10.0.2.118', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (128, 2, 710, 'reg', 'initial', '2016-07-27 11:53:22.98944', '10.0.2.118', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (128, 2, 711, 'unpaid', 'pending_pay', '2016-07-27 11:53:22.98944', '10.0.2.118', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (128, 2, 712, 'accepted', 'pay', '2016-07-27 11:53:24.765253', NULL, NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (128, 3, 713, 'reg', 'initial', '2016-07-27 11:53:24.79453', '10.0.2.118', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (128, 3, 714, 'accepted', 'adminclear', '2016-07-27 11:53:24.79453', '10.0.2.118', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (129, 1, 715, 'reg', 'initial', '2016-07-27 11:54:33.554909', '10.0.2.118', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (129, 1, 716, 'unverified', 'verifymail', '2016-07-27 11:54:33.554909', '10.0.2.118', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (129, 1, 717, 'pending_review', 'verify', '2016-07-27 11:54:33.60065', '10.0.2.118', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (129, 2, 718, 'reg', 'initial', '2016-07-27 11:54:36.401652', '10.0.2.118', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (129, 2, 719, 'unpaid', 'pending_pay', '2016-07-27 11:54:36.401652', '10.0.2.118', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (129, 2, 720, 'accepted', 'pay', '2016-07-27 11:54:38.203878', NULL, NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (129, 3, 721, 'reg', 'initial', '2016-07-27 11:54:38.246882', '10.0.2.118', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (129, 3, 722, 'accepted', 'adminclear', '2016-07-27 11:54:38.246882', '10.0.2.118', NULL);

SELECT pg_catalog.setval('action_states_state_id_seq', 722, true);
INSERT INTO ad_changes (ad_id, action_id, state_id, is_param, column_name, old_value, new_value) VALUES (128, 3, 714, true, 'gallery', NULL, '2016-07-27 12:03:24');
INSERT INTO ad_changes (ad_id, action_id, state_id, is_param, column_name, old_value, new_value) VALUES (129, 2, 720, true, 'label', NULL, 'urgent');
INSERT INTO ad_changes (ad_id, action_id, state_id, is_param, column_name, old_value, new_value) VALUES (129, 3, 722, true, 'gallery', NULL, '2016-07-27 12:04:38');

INSERT INTO ad_codes (code, code_type) VALUES (24741, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (56764, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (88787, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (30810, 'pay');

INSERT INTO ad_images_digests (ad_id, name, digest, uid, previously_inserted) VALUES (128, '0908523319.jpg', '86f3d3f10a85226fc9d2770eaae09da7', 54, false);
INSERT INTO ad_images_digests (ad_id, name, digest, uid, previously_inserted) VALUES (129, '0917046638.jpg', '86f3d3f10a85226fc9d2770eaae09da7', 54, false);

INSERT INTO ad_media (ad_media_id, ad_id, seq_no, upload_time, media_type, width, height, length) VALUES (908523319, 128, 0, '2016-07-27 11:53:11.342103', 'image', NULL, NULL, NULL);
INSERT INTO ad_media (ad_media_id, ad_id, seq_no, upload_time, media_type, width, height, length) VALUES (917046638, 129, 0, '2016-07-27 11:54:26.630997', 'image', NULL, NULL, NULL);

INSERT INTO ad_params (ad_id, name, value) VALUES (126, 'communes', '315');
INSERT INTO ad_params (ad_id, name, value) VALUES (126, 'country', 'UNK');
INSERT INTO ad_params (ad_id, name, value) VALUES (127, 'communes', '315');
INSERT INTO ad_params (ad_id, name, value) VALUES (127, 'country', 'UNK');
INSERT INTO ad_params (ad_id, name, value) VALUES (128, 'communes', '315');
INSERT INTO ad_params (ad_id, name, value) VALUES (128, 'country', 'UNK');
INSERT INTO ad_params (ad_id, name, value) VALUES (128, 'gallery', '2016-07-27 12:03:24');
INSERT INTO ad_params (ad_id, name, value) VALUES (129, 'communes', '315');
INSERT INTO ad_params (ad_id, name, value) VALUES (129, 'country', 'UNK');
INSERT INTO ad_params (ad_id, name, value) VALUES (129, 'label', 'urgent');
INSERT INTO ad_params (ad_id, name, value) VALUES (129, 'gallery', '2016-07-27 12:04:38');

INSERT INTO ad_queues (ad_id, action_id, queue, queued_at, locked_by, locked_until) VALUES (126, 1, 'normal', '2016-07-27 11:51:03.399911', NULL, NULL);
INSERT INTO ad_queues (ad_id, action_id, queue, queued_at, locked_by, locked_until) VALUES (127, 1, 'normal', '2016-07-27 11:52:22.594213', NULL, NULL);
INSERT INTO ad_queues (ad_id, action_id, queue, queued_at, locked_by, locked_until) VALUES (128, 1, 'normal', '2016-07-27 11:53:17.075147', NULL, NULL);
INSERT INTO ad_queues (ad_id, action_id, queue, queued_at, locked_by, locked_until) VALUES (129, 1, 'normal', '2016-07-27 11:54:33.60065', NULL, NULL);

SELECT pg_catalog.setval('ads_ad_id_seq', 129, true);

SELECT pg_catalog.setval('next_image_id', 2, true);
SELECT pg_catalog.setval('pay_code_seq', 470, true);

INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (213, 'save', 0, '633026231', NULL, '2016-07-27 11:51:03', 166, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (214, 'verify', 0, '633026231', NULL, '2016-07-27 11:51:03', 166, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (215, 'save', 0, '768418254', NULL, '2016-07-27 11:52:23', 167, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (216, 'verify', 0, '768418254', NULL, '2016-07-27 11:52:23', 167, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (217, 'save', 0, '12345', NULL, '2016-07-27 11:52:26', 168, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (218, 'paycard_save', 0, '12345', NULL, '2016-07-27 11:52:28', 168, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (219, 'paycard', 900, '00168a', NULL, '2016-07-27 11:52:28', 168, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (220, 'paycard', 900, '12345', NULL, '2016-07-27 11:52:28', 168, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (221, 'save', 0, '903810277', NULL, '2016-07-27 11:53:17', 169, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (222, 'verify', 0, '903810277', NULL, '2016-07-27 11:53:17', 169, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (223, 'save', 0, '65432', NULL, '2016-07-27 11:53:23', 170, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (224, 'paycard_save', 0, '65432', NULL, '2016-07-27 11:53:25', 170, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (225, 'paycard', 4500, '00170a', NULL, '2016-07-27 11:53:25', 170, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (226, 'paycard', 4500, '65432', NULL, '2016-07-27 11:53:25', 170, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (227, 'save', 0, '139202300', NULL, '2016-07-27 11:54:34', 171, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (228, 'verify', 0, '139202300', NULL, '2016-07-27 11:54:34', 171, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (229, 'save', 0, '32323', NULL, '2016-07-27 11:54:36', 172, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (230, 'paycard_save', 0, '32323', NULL, '2016-07-27 11:54:38', 172, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (231, 'paycard', 6490, '00172a', NULL, '2016-07-27 11:54:38', 172, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (232, 'paycard', 6490, '32323', NULL, '2016-07-27 11:54:38', 172, 'OK');

SELECT pg_catalog.setval('pay_log_pay_log_id_seq', 232, true);

INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (213, 0, 'adphone', '987654321');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (213, 1, 'remote_addr', '10.0.2.118');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (214, 0, 'remote_addr', '10.0.2.118');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (215, 0, 'adphone', '987654321');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (215, 1, 'remote_addr', '10.0.2.118');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (216, 0, 'remote_addr', '10.0.2.118');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (217, 0, 'remote_addr', '10.0.2.118');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (218, 0, 'auth_code', '757487');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (218, 1, 'trans_date', '27/07/2016 11:52:28');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (218, 2, 'placements_type', 'VN');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (218, 3, 'placements_number', '1');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (218, 4, 'pay_type', 'credit');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (218, 5, 'external_response', '0');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (218, 6, 'external_id', '38287493047110642196');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (218, 7, 'cc_number', '5894');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (218, 8, 'remote_addr', '10.0.1.76');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (219, 0, 'webpay', '1');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (219, 1, 'webpay', '1');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (221, 0, 'adphone', '987654321');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (221, 1, 'remote_addr', '10.0.2.118');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (222, 0, 'remote_addr', '10.0.2.118');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (223, 0, 'remote_addr', '10.0.2.118');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (224, 0, 'auth_code', '359340');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (224, 1, 'trans_date', '27/07/2016 11:53:24');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (224, 2, 'placements_type', 'VN');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (224, 3, 'placements_number', '1');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (224, 4, 'pay_type', 'credit');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (224, 5, 'external_response', '0');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (224, 6, 'external_id', '50438786015067997414');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (224, 7, 'cc_number', '2780');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (224, 8, 'remote_addr', '10.0.1.76');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (225, 0, 'webpay', '1');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (225, 1, 'webpay', '1');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (227, 0, 'adphone', '987654321');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (227, 1, 'remote_addr', '10.0.2.118');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (228, 0, 'remote_addr', '10.0.2.118');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (229, 0, 'remote_addr', '10.0.2.118');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (230, 0, 'auth_code', '181382');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (230, 1, 'trans_date', '27/07/2016 11:54:38');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (230, 2, 'placements_type', 'VN');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (230, 3, 'placements_number', '1');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (230, 4, 'pay_type', 'credit');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (230, 5, 'external_response', '0');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (230, 6, 'external_id', '19657263248803223786');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (230, 7, 'cc_number', '3532');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (230, 8, 'remote_addr', '10.0.1.76');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (231, 0, 'webpay', '1');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (231, 1, 'webpay', '1');

SELECT pg_catalog.setval('payment_groups_payment_group_id_seq', 172, true);

INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (166, 'ad_action', 0, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (167, 'ad_action', 0, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (168, 'upselling_standard_others', 900, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (169, 'ad_action', 0, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (170, 'upselling_advanced_others', 4500, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (171, 'ad_action', 0, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (172, 'upselling_premium_others', 6490, 0);

INSERT INTO purchase (purchase_id, doc_type, doc_num, external_doc_id, status, receipt, delivery, rut, name, lob, address, region, communes, contact, email, tax, discount, seller_id, total_price, payment_group_id, payment_method, payment_platform, account_id) VALUES (2, 'bill', NULL, NULL, 'paid', '2016-07-27 11:52:26.204342', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'user.01@schibsted.cl', 0, 0, NULL, 900, 168, 'webpay', 'desktop', 7);
INSERT INTO purchase (purchase_id, doc_type, doc_num, external_doc_id, status, receipt, delivery, rut, name, lob, address, region, communes, contact, email, tax, discount, seller_id, total_price, payment_group_id, payment_method, payment_platform, account_id) VALUES (3, 'bill', NULL, NULL, 'paid', '2016-07-27 11:53:22.98944', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'user.01@schibsted.cl', 0, 0, NULL, 4500, 170, 'webpay', 'desktop', 7);
INSERT INTO purchase (purchase_id, doc_type, doc_num, external_doc_id, status, receipt, delivery, rut, name, lob, address, region, communes, contact, email, tax, discount, seller_id, total_price, payment_group_id, payment_method, payment_platform, account_id) VALUES (4, 'bill', NULL, NULL, 'paid', '2016-07-27 11:54:36.401652', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'user.01@schibsted.cl', 0, 0, NULL, 6490, 172, 'webpay', 'desktop', 7);

INSERT INTO purchase_detail (purchase_detail_id, purchase_id, product_id, price, action_id, ad_id, payment_group_id) VALUES (2, 2, 1031, 900, 2, 127, 168);
INSERT INTO purchase_detail (purchase_detail_id, purchase_id, product_id, price, action_id, ad_id, payment_group_id) VALUES (3, 3, 1032, 4500, 2, 128, 170);
INSERT INTO purchase_detail (purchase_detail_id, purchase_id, product_id, price, action_id, ad_id, payment_group_id) VALUES (4, 4, 1033, 6490, 2, 129, 172);

SELECT pg_catalog.setval('purchase_detail_purchase_detail_id_seq', 4, true);

SELECT pg_catalog.setval('purchase_purchase_id_seq', 4, true);

INSERT INTO purchase_states (purchase_id, purchase_state_id, status, "timestamp", remote_addr) VALUES (2, 1, 'pending', '2016-07-27 11:52:26.204342', '10.0.2.118');
INSERT INTO purchase_states (purchase_id, purchase_state_id, status, "timestamp", remote_addr) VALUES (3, 2, 'pending', '2016-07-27 11:53:22.98944', '10.0.2.118');
INSERT INTO purchase_states (purchase_id, purchase_state_id, status, "timestamp", remote_addr) VALUES (4, 3, 'pending', '2016-07-27 11:54:36.401652', '10.0.2.118');

SELECT pg_catalog.setval('purchase_states_purchase_state_id_seq', 3, true);

INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (126, 1, 700, 'queue', 'normal');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (127, 1, 703, 'queue', 'normal');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (128, 1, 709, 'queue', 'normal');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (129, 1, 717, 'queue', 'normal');

INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (93, '2016-07-27 11:52:28.111578', '27944@sasha.schibsted.cl', '2016-07-27 11:54:28.111578', NULL, NULL, NULL, NULL, NULL, 'cmd:bump_ad
batch:0
commit:1
ad_id:127
doc_type:bill
remote_addr:10.0.2.118
', NULL, 'upselling', NULL, NULL);
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (94, '2016-07-27 11:53:24.793026', '27944@sasha.schibsted.cl', '2016-07-27 11:55:24.793026', NULL, NULL, NULL, NULL, NULL, 'cmd:bump_ad
batch:0
commit:1
ad_id:128
doc_type:bill
remote_addr:10.0.2.118
', NULL, 'upselling', NULL, NULL);
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (95, '2016-07-27 11:54:38.239205', '27944@sasha.schibsted.cl', '2016-07-27 11:56:38.239205', NULL, NULL, NULL, NULL, NULL, 'cmd:bump_ad
batch:0
commit:1
ad_id:129
doc_type:bill
remote_addr:10.0.2.118
', NULL, 'upselling', NULL, NULL);

SELECT pg_catalog.setval('trans_queue_id_seq', 95, true);

----------------------------------------------------------

INSERT INTO account_actions (account_id, account_action_id, account_action_type, "timestamp", token_id) VALUES (7, 5, 'edition', '2016-07-28 12:50:56.966785', NULL);
INSERT INTO account_actions (account_id, account_action_id, account_action_type, "timestamp", token_id) VALUES (7, 6, 'edition', '2016-07-28 12:51:21.878577', NULL);
INSERT INTO account_actions (account_id, account_action_id, account_action_type, "timestamp", token_id) VALUES (7, 7, 'edition', '2016-07-28 12:52:00.298631', NULL);
INSERT INTO account_actions (account_id, account_action_id, account_action_type, "timestamp", token_id) VALUES (7, 8, 'edition', '2016-07-28 12:52:31.401342', NULL);

INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (130, NULL, NULL, 'inactive', 'sell', 'Usuario 01', '987654321', 15, 0, 8020, 58, NULL, false, false, false, 'Test upselling - basico', 'asdasd asd ad', 123123, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$2y1bQ}VYRD}zF4QCbf07a971b784888658a20e62156de64dfde5ffb6', NULL, 'es');
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (131, NULL, NULL, 'inactive', 'sell', 'Usuario 01', '987654321', 15, 0, 8020, 58, NULL, false, false, false, 'Test upselling - estandar', 'asd asd asd', 123123, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$2y1bQ}VYRD}zF4QCbf07a971b784888658a20e62156de64dfde5ffb6', NULL, 'es');
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (132, NULL, NULL, 'inactive', 'sell', 'Usuario 01', '987654321', 15, 0, 8020, 58, NULL, false, false, false, 'Test upselling - avanzado', 'asd assd asd', 123123, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$2y1bQ}VYRD}zF4QCbf07a971b784888658a20e62156de64dfde5ffb6', NULL, 'es');
INSERT INTO ads (ad_id, list_id, list_time, status, type, name, phone, region, city, category, user_id, passwd, phone_hidden, no_salesmen, company_ad, subject, body, price, image, infopage, infopage_title, orig_list_time, old_price, store_id, salted_passwd, modified_at, lang) VALUES (133, NULL, NULL, 'inactive', 'sell', 'Usuario 01', '987654321', 15, 0, 8020, 58, NULL, false, false, false, 'Test upselling - premium', 'asd asd asd asd', 123123, NULL, NULL, NULL, NULL, NULL, NULL, '$1024$2y1bQ}VYRD}zF4QCbf07a971b784888658a20e62156de64dfde5ffb6', NULL, 'es');

INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (173, '633026231', 'verified', '2016-07-28 12:50:56.930105', NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (174, '768418254', 'verified', '2016-07-28 12:51:21.853295', NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (175, '903810277', 'verified', '2016-07-28 12:52:00.271683', NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (176, '139202300', 'verified', '2016-07-28 12:52:31.37865', NULL);

INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (130, 1, 'new', 737, 'pending_review', 'normal', NULL, NULL, 173);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (131, 1, 'new', 740, 'pending_review', 'normal', NULL, NULL, 174);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (132, 1, 'new', 743, 'pending_review', 'normal', NULL, NULL, 175);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (133, 1, 'new', 746, 'pending_review', 'normal', NULL, NULL, 176);

INSERT INTO action_params (ad_id, action_id, name, value) VALUES (130, 1, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (130, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (131, 1, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (131, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (132, 1, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (132, 1, 'redir', 'dW5rbm93bg==');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (133, 1, 'source', 'web');
INSERT INTO action_params (ad_id, action_id, name, value) VALUES (133, 1, 'redir', 'dW5rbm93bg==');

INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (130, 1, 735, 'reg', 'initial', '2016-07-28 12:50:56.930105', '10.0.2.118', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (130, 1, 736, 'unverified', 'verifymail', '2016-07-28 12:50:56.930105', '10.0.2.118', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (130, 1, 737, 'pending_review', 'verify', '2016-07-28 12:50:57.00191', '10.0.2.118', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (131, 1, 738, 'reg', 'initial', '2016-07-28 12:51:21.853295', '10.0.2.118', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (131, 1, 739, 'unverified', 'verifymail', '2016-07-28 12:51:21.853295', '10.0.2.118', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (131, 1, 740, 'pending_review', 'verify', '2016-07-28 12:51:21.900211', '10.0.2.118', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (132, 1, 741, 'reg', 'initial', '2016-07-28 12:52:00.271683', '10.0.2.118', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (132, 1, 742, 'unverified', 'verifymail', '2016-07-28 12:52:00.271683', '10.0.2.118', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (132, 1, 743, 'pending_review', 'verify', '2016-07-28 12:52:00.321953', '10.0.2.118', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (133, 1, 744, 'reg', 'initial', '2016-07-28 12:52:31.37865', '10.0.2.118', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (133, 1, 745, 'unverified', 'verifymail', '2016-07-28 12:52:31.37865', '10.0.2.118', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (133, 1, 746, 'pending_review', 'verify', '2016-07-28 12:52:31.421059', '10.0.2.118', NULL);

SELECT pg_catalog.setval('action_states_state_id_seq', 746, true);

INSERT INTO ad_codes (code, code_type) VALUES (62833, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (94856, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (36879, 'pay');
INSERT INTO ad_codes (code, code_type) VALUES (68902, 'pay');

INSERT INTO ad_images_digests (ad_id, name, digest, uid, previously_inserted) VALUES (132, '1025569957.jpg', '86f3d3f10a85226fc9d2770eaae09da7', 54, false);
INSERT INTO ad_images_digests (ad_id, name, digest, uid, previously_inserted) VALUES (133, '1034093276.jpg', '86f3d3f10a85226fc9d2770eaae09da7', 54, false);

INSERT INTO ad_media (ad_media_id, ad_id, seq_no, upload_time, media_type, width, height, length) VALUES (1025569957, 132, 0, '2016-07-28 12:51:51.682648', 'image', NULL, NULL, NULL);
INSERT INTO ad_media (ad_media_id, ad_id, seq_no, upload_time, media_type, width, height, length) VALUES (1034093276, 133, 0, '2016-07-28 12:52:23.254119', 'image', NULL, NULL, NULL);

INSERT INTO ad_params (ad_id, name, value) VALUES (130, 'communes', '315');
INSERT INTO ad_params (ad_id, name, value) VALUES (130, 'country', 'UNK');
INSERT INTO ad_params (ad_id, name, value) VALUES (131, 'communes', '315');
INSERT INTO ad_params (ad_id, name, value) VALUES (131, 'country', 'UNK');
INSERT INTO ad_params (ad_id, name, value) VALUES (132, 'communes', '315');
INSERT INTO ad_params (ad_id, name, value) VALUES (132, 'country', 'UNK');
INSERT INTO ad_params (ad_id, name, value) VALUES (133, 'communes', '315');
INSERT INTO ad_params (ad_id, name, value) VALUES (133, 'country', 'UNK');

INSERT INTO ad_queues (ad_id, action_id, queue, queued_at, locked_by, locked_until) VALUES (130, 1, 'normal', '2016-07-28 12:50:57.00191', NULL, NULL);
INSERT INTO ad_queues (ad_id, action_id, queue, queued_at, locked_by, locked_until) VALUES (131, 1, 'normal', '2016-07-28 12:51:21.900211', NULL, NULL);
INSERT INTO ad_queues (ad_id, action_id, queue, queued_at, locked_by, locked_until) VALUES (132, 1, 'normal', '2016-07-28 12:52:00.321953', NULL, NULL);
INSERT INTO ad_queues (ad_id, action_id, queue, queued_at, locked_by, locked_until) VALUES (133, 1, 'normal', '2016-07-28 12:52:31.421059', NULL, NULL);

SELECT pg_catalog.setval('ads_ad_id_seq', 133, true);

SELECT pg_catalog.setval('next_image_id', 4, true);

SELECT pg_catalog.setval('pay_code_seq', 474, true);

INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (233, 'save', 0, '633026231', NULL, '2016-07-28 12:50:57', 173, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (234, 'verify', 0, '633026231', NULL, '2016-07-28 12:50:57', 173, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (235, 'save', 0, '768418254', NULL, '2016-07-28 12:51:22', 174, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (236, 'verify', 0, '768418254', NULL, '2016-07-28 12:51:22', 174, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (237, 'save', 0, '903810277', NULL, '2016-07-28 12:52:00', 175, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (238, 'verify', 0, '903810277', NULL, '2016-07-28 12:52:00', 175, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (239, 'save', 0, '139202300', NULL, '2016-07-28 12:52:31', 176, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (240, 'verify', 0, '139202300', NULL, '2016-07-28 12:52:31', 176, 'OK');

SELECT pg_catalog.setval('pay_log_pay_log_id_seq', 240, true);

INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (233, 0, 'adphone', '987654321');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (233, 1, 'remote_addr', '10.0.2.118');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (234, 0, 'remote_addr', '10.0.2.118');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (235, 0, 'adphone', '987654321');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (235, 1, 'remote_addr', '10.0.2.118');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (236, 0, 'remote_addr', '10.0.2.118');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (237, 0, 'adphone', '987654321');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (237, 1, 'remote_addr', '10.0.2.118');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (238, 0, 'remote_addr', '10.0.2.118');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (239, 0, 'adphone', '987654321');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (239, 1, 'remote_addr', '10.0.2.118');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (240, 0, 'remote_addr', '10.0.2.118');

SELECT pg_catalog.setval('payment_groups_payment_group_id_seq', 176, true);

INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (173, 'ad_action', 0, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (174, 'ad_action', 0, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (175, 'ad_action', 0, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (176, 'ad_action', 0, 0);

INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (130, 1, 737, 'queue', 'normal');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (131, 1, 740, 'queue', 'normal');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (132, 1, 743, 'queue', 'normal');
INSERT INTO state_params (ad_id, action_id, state_id, name, value) VALUES (133, 1, 746, 'queue', 'normal');
