--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET client_encoding = 'LATIN1';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;

SET search_path = public, pg_catalog;

--
-- Name: mama_main_backup_backup_id_seq; Type: SEQUENCE SET; Schema: public; Owner: pablo.paso
--

SELECT pg_catalog.setval('mama_main_backup_backup_id_seq', 1, true);


--
-- Data for Name: mama_main_backup; Type: TABLE DATA; Schema: public; Owner: pablo.paso
--


DELETE FROM mama_attribute_words_backup;
DELETE FROM mama_attribute_categories_backup;
DELETE FROM mama_attribute_wordlists_backup;
DELETE FROM mama_exception_words_backup;
DELETE FROM mama_exception_lists_backup;
DELETE FROM mama_words_backup;
DELETE FROM mama_wordlists_backup;
DELETE FROM mama_main_backup;

INSERT INTO mama_main_backup VALUES (1, '2009-08-25 15:48:11.088911', 2);


--
-- Data for Name: mama_attribute_categories_backup; Type: TABLE DATA; Schema: public; Owner: pablo.paso
--

INSERT INTO mama_attribute_categories_backup VALUES (1, 1, 2100);
INSERT INTO mama_attribute_categories_backup VALUES (1, 2, 2100);
INSERT INTO mama_attribute_categories_backup VALUES (1, 3, 2100);
INSERT INTO mama_attribute_categories_backup VALUES (1, 4, 2100);
INSERT INTO mama_attribute_categories_backup VALUES (1, 5, 2100);
INSERT INTO mama_attribute_categories_backup VALUES (1, 6, 2100);

--
-- Data for Name: mama_attribute_wordlists_backup; Type: TABLE DATA; Schema: public; Owner: pablo.paso
--

INSERT INTO mama_attribute_wordlists_backup VALUES (1, 1, 'brand_2100', 0);
INSERT INTO mama_attribute_wordlists_backup VALUES (1, 2, 'model_2100', 0);
INSERT INTO mama_attribute_wordlists_backup VALUES (1, 3, 'motor_2100', 0);
INSERT INTO mama_attribute_wordlists_backup VALUES (1, 4, 'power_2100', 0);
INSERT INTO mama_attribute_wordlists_backup VALUES (1, 5, 'size_2100', 0);
INSERT INTO mama_attribute_wordlists_backup VALUES (1, 6, 'type_2100', 0);

--
-- Data for Name: mama_attribute_words_backup; Type: TABLE DATA; Schema: public; Owner: pablo.paso
--

INSERT INTO mama_attribute_words_backup VALUES (1, 1, 'adria', 1, 'active', 0, 0);
INSERT INTO mama_attribute_words_backup VALUES (1, 2, 'adriatik', 1, 'active', 0, 0);
INSERT INTO mama_attribute_words_backup VALUES (1, 3, 'autoroller', 1, 'active', 0, 0);
INSERT INTO mama_attribute_words_backup VALUES (1, 4, 'autostar', 1, 'active', 0, 0);
INSERT INTO mama_attribute_words_backup VALUES (1, 5, 'axxor', 1, 'active', 0, 0);
INSERT INTO mama_attribute_words_backup VALUES (1, 6, 'barbot', 1, 'active', 0, 0);

--
-- Data for Name: mama_exception_lists_backup; Type: TABLE DATA; Schema: public; Owner: pablo.paso
--

INSERT INTO mama_exception_lists_backup VALUES (1, 1, 'embedded', 2020, 'gun');
INSERT INTO mama_exception_lists_backup VALUES (1, 2, 'embedded', 2020, 'lefa');
INSERT INTO mama_exception_lists_backup VALUES (1, 3, 'embedded', 1020, 'puta');
INSERT INTO mama_exception_lists_backup VALUES (1, 4, 'combined', 5080, 'gun');
INSERT INTO mama_exception_lists_backup VALUES (1, 5, 'embedded', 5080, 'transpondedor');


--
-- Data for Name: mama_exception_words_backup; Type: TABLE DATA; Schema: public; Owner: pablo.paso
--

INSERT INTO mama_exception_words_backup VALUES (1, 1, 1, 'laguna');
INSERT INTO mama_exception_words_backup VALUES (1, 2, 1, 'torrelaguna');
INSERT INTO mama_exception_words_backup VALUES (1, 3, 2, 'calefaccion');


--
-- Data for Name: mama_wordlists_backup; Type: TABLE DATA; Schema: public; Owner: pablo.paso
--

INSERT INTO mama_wordlists_backup VALUES (1, 'rude_words', 0, 0, 1);
INSERT INTO mama_wordlists_backup VALUES (1, 'rude_words.only_isolated', 0, 0, 2);
INSERT INTO mama_wordlists_backup VALUES (1, 'empty_words', 0, 0, 3);

--
-- Data for Name: mama_words_backup; Type: TABLE DATA; Schema: public; Owner: pablo.paso
--

INSERT INTO mama_words_backup VALUES (1, 1, 'shit', 1, 'active', 0, 0);
INSERT INTO mama_words_backup VALUES (1, 2, 'fuck', 1, 'active', 0, 0);
INSERT INTO mama_words_backup VALUES (1, 3, 'wtf', 1, 'active', 0, 0);
INSERT INTO mama_words_backup VALUES (1, 4, 'douchebag', 1, 'active', 0, 0);
INSERT INTO mama_words_backup VALUES (1, 5, 'bite', 1, 'active', 0, 0);
INSERT INTO mama_words_backup VALUES (1, 6, 'chier', 1, 'active', 0, 0);
INSERT INTO mama_words_backup VALUES (1, 7, 'connard', 1, 'active', 0, 0);
INSERT INTO mama_words_backup VALUES (1, 8, 'connasse', 1, 'active', 0, 0);

--
-- PostgreSQL database dump complete
--

