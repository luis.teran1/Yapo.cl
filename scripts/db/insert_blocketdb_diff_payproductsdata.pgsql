INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (166, '12345', 'paid', '2015-07-28 17:58:09.258317', NULL);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (167, '65432', 'paid', '2015-07-28 17:58:09.258317', 166);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (168, '32323', 'paid', '2015-07-28 17:58:09.258317', 166);
INSERT INTO payment_groups (payment_group_id, code, status, added_at, parent_payment_group_id) VALUES (169, '54545', 'paid', '2015-07-28 17:58:27.291868', NULL);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (122, 2, 'gallery', 702, 'accepted', 'normal', NULL, NULL, 167);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (122, 3, 'daily_bump', 704, 'accepted', 'normal', NULL, NULL, 168);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (122, 4, 'bump', 706, 'accepted', 'normal', NULL, NULL, NULL);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (122, 5, 'weekly_bump', 710, 'accepted', 'normal', NULL, NULL, 169);
INSERT INTO ad_actions (ad_id, action_id, action_type, current_state, state, queue, locked_by, locked_until, payment_group_id) VALUES (122, 6, 'bump', 712, 'accepted', 'normal', NULL, NULL, NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (122, 2, 698, 'reg', 'initial', '2015-07-28 17:58:09.258317', '10.0.1.101', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (122, 2, 699, 'unpaid', 'pending_pay', '2015-07-28 17:58:09.258317', '10.0.1.101', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (122, 3, 700, 'reg', 'initial', '2015-07-28 17:58:09.258317', '10.0.1.101', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (122, 3, 701, 'unpaid', 'pending_pay', '2015-07-28 17:58:09.258317', '10.0.1.101', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (122, 2, 702, 'accepted', 'pay', '2015-07-28 17:58:11.996682', NULL, NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (122, 3, 703, 'accepted', 'pay', '2015-07-28 17:58:12.052155', NULL, NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (122, 3, 704, 'accepted', 'updating_counter', '2015-07-28 17:58:12.138175', NULL, NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (122, 4, 705, 'reg', 'initial', '2015-07-28 17:58:12.150806', NULL, NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (122, 4, 706, 'accepted', 'bump', '2015-07-28 17:58:12.150806', NULL, NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (122, 5, 707, 'reg', 'initial', '2015-07-28 17:58:27.291868', '10.0.1.101', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (122, 5, 708, 'unpaid', 'pending_pay', '2015-07-28 17:58:27.291868', '10.0.1.101', NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (122, 5, 709, 'accepted', 'pay', '2015-07-28 17:58:29.268257', NULL, NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (122, 5, 710, 'accepted', 'updating_counter', '2015-07-28 17:58:29.39055', NULL, NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (122, 6, 711, 'reg', 'initial', '2015-07-28 17:58:29.40325', NULL, NULL);
INSERT INTO action_states (ad_id, action_id, state_id, state, transition, "timestamp", remote_addr, token_id) VALUES (122, 6, 712, 'accepted', 'bump', '2015-07-28 17:58:29.40325', NULL, NULL);
SELECT pg_catalog.setval('action_states_state_id_seq', 712, true);
INSERT INTO ad_changes (ad_id, action_id, state_id, is_param, column_name, old_value, new_value) VALUES (122, 3, 704, true, 'daily_bump', NULL, '6');
INSERT INTO ad_changes (ad_id, action_id, state_id, is_param, column_name, old_value, new_value) VALUES (122, 2, 702, true, 'gallery', NULL, '2015-07-28 18:08:12');
INSERT INTO ad_changes (ad_id, action_id, state_id, is_param, column_name, old_value, new_value) VALUES (122, 5, 710, true, 'weekly_bump', NULL, '3');
INSERT INTO ad_params (ad_id, name, value) VALUES (122, 'daily_bump', '6');
INSERT INTO ad_params (ad_id, name, value) VALUES (122, 'gallery', '2015-07-28 18:08:12');
INSERT INTO ad_params (ad_id, name, value) VALUES (122, 'weekly_bump', '3');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (213, 'save', 0, '65432', NULL, '2015-07-28 17:58:09', 167, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (214, 'save', 0, '32323', NULL, '2015-07-28 17:58:09', 168, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (215, 'paycard_save', 0, '12345', NULL, '2015-07-28 17:58:12', 166, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (216, 'paycard', 3600, '00167a', NULL, '2015-07-28 17:58:12', 167, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (217, 'paycard', 4500, '00168a', NULL, '2015-07-28 17:58:12', 168, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (218, 'paycard', 8100, '12345', NULL, '2015-07-28 17:58:12', 166, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (219, 'save', 0, '54545', NULL, '2015-07-28 17:58:27', 169, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (220, 'paycard_save', 0, '54545', NULL, '2015-07-28 17:58:29', 169, 'SAVE');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (221, 'paycard', 2700, '00169a', NULL, '2015-07-28 17:58:29', 169, 'OK');
INSERT INTO pay_log (pay_log_id, pay_type, amount, code, token_id, paid_at, payment_group_id, status) VALUES (222, 'paycard', 2700, '54545', NULL, '2015-07-28 17:58:29', 169, 'OK');
SELECT pg_catalog.setval('pay_log_pay_log_id_seq', 222, true);
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (213, 0, 'remote_addr', '10.0.1.101');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (214, 0, 'remote_addr', '10.0.1.101');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (215, 0, 'auth_code', '844946');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (215, 1, 'trans_date', '28/07/2015 17:58:11');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (215, 2, 'placements_type', 'VN');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (215, 3, 'placements_number', '1');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (215, 4, 'pay_type', 'credit');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (215, 5, 'external_response', '0');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (215, 6, 'external_id', '26032805589351155768');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (215, 7, 'remote_addr', '10.0.1.208');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (216, 0, 'webpay', '1');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (217, 0, 'webpay', '1');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (219, 0, 'remote_addr', '10.0.1.101');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (220, 0, 'auth_code', '174436');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (220, 1, 'trans_date', '28/07/2015 17:58:28');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (220, 2, 'placements_type', 'VN');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (220, 3, 'placements_number', '1');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (220, 4, 'pay_type', 'credit');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (220, 5, 'external_response', '0');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (220, 6, 'external_id', '02995099244195586387');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (220, 7, 'remote_addr', '10.0.1.208');
INSERT INTO pay_log_references (pay_log_id, ref_num, ref_type, reference) VALUES (221, 0, 'webpay', '1');
SELECT pg_catalog.setval('payment_groups_payment_group_id_seq', 169, true);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (167, 'gallery', 3600, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (168, 'daily_bump', 4500, 0);
INSERT INTO payments (payment_group_id, payment_type, pay_amount, discount) VALUES (169, 'weekly_bump', 2700, 0);
INSERT INTO purchase (purchase_id, doc_type, doc_num, external_doc_id, status, receipt, delivery, rut, name, lob, address, region, communes, contact, email, tax, discount, seller_id, total_price, payment_group_id, payment_method, account_id) VALUES (2, 'bill', NULL, NULL, 'paid', '2015-07-28 17:58:09.258317', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'user.01@schibsted.cl', 0, 0, NULL, 8100, 166, 'webpay', 7);
INSERT INTO purchase (purchase_id, doc_type, doc_num, external_doc_id, status, receipt, delivery, rut, name, lob, address, region, communes, contact, email, tax, discount, seller_id, total_price, payment_group_id, payment_method, account_id) VALUES (3, 'bill', NULL, NULL, 'paid', '2015-07-28 17:58:27.291868', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'user.01@schibsted.cl', 0, 0, NULL, 2700, 169, 'webpay', 7);
INSERT INTO purchase_detail (purchase_detail_id, purchase_id, product_id, price, action_id, ad_id, payment_group_id) VALUES (2, 2, 3, 3600, 2, 122, 167);
INSERT INTO purchase_detail (purchase_detail_id, purchase_id, product_id, price, action_id, ad_id, payment_group_id) VALUES (3, 2, 8, 4500, 3, 122, 168);
INSERT INTO purchase_detail (purchase_detail_id, purchase_id, product_id, price, action_id, ad_id, payment_group_id) VALUES (4, 3, 2, 2700, 5, 122, 169);
SELECT pg_catalog.setval('purchase_detail_purchase_detail_id_seq', 4, true);
SELECT pg_catalog.setval('purchase_purchase_id_seq', 3, true);
INSERT INTO purchase_states (purchase_id, purchase_state_id, status, "timestamp", remote_addr) VALUES (2, 1, 'pending', '2015-07-28 17:58:09.258317', '10.0.1.101');
INSERT INTO purchase_states (purchase_id, purchase_state_id, status, "timestamp", remote_addr) VALUES (3, 2, 'pending', '2015-07-28 17:58:27.291868', '10.0.1.101');
SELECT pg_catalog.setval('purchase_states_purchase_state_id_seq', 2, true);
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (93, '2015-07-28 17:58:12.155277', '3758@mazaru.schibsted.cl', '2015-07-28 18:00:12.155277', NULL, NULL, NULL, NULL, NULL, 'cmd:weekly_bump_ad
ad_id:122
action_id:3
counter:6
daily_bump:1
batch:1
commit:1
', NULL, 'weekly_bump', NULL, NULL);
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (94, '2015-07-28 17:58:12.162317', '3758@mazaru.schibsted.cl', NULL, NULL, NULL, NULL, NULL, NULL, 'cmd:admail
commit:1
ad_id:122
mail_type:weekly_bump_ok
remaining_bumps:6
doc_type:bill
', NULL, 'bump_mail', NULL, NULL);
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (95, '2015-07-28 17:58:29.403981', '3758@mazaru.schibsted.cl', '2015-07-28 17:59:29.403981', NULL, NULL, NULL, NULL, NULL, 'cmd:weekly_bump_ad
ad_id:122
action_id:5
counter:3
batch:1
commit:1
', NULL, 'weekly_bump', NULL, NULL);
INSERT INTO trans_queue (trans_queue_id, added_at, added_by, execute_at, executed_at, executed_by, status, locked_at, locked_by, command, response, queue, sub_queue, info) VALUES (96, '2015-07-28 17:58:29.414728', '3758@mazaru.schibsted.cl', NULL, NULL, NULL, NULL, NULL, NULL, 'cmd:admail
commit:1
ad_id:122
mail_type:weekly_bump_ok
remaining_bumps:3
doc_type:bill
', NULL, 'bump_mail', NULL, NULL);
SELECT pg_catalog.setval('trans_queue_id_seq', 96, true);

--Change all passwords to 123123123
update ads set salted_passwd =  E'$1024$2y1bQ}VYRD}zF4QCbf07a971b784888658a20e62156de64dfde5ffb6', passwd = null;
update accounts set salted_passwd =  E'$1024$2y1bQ}VYRD}zF4QCbf07a971b784888658a20e62156de64dfde5ffb6';
