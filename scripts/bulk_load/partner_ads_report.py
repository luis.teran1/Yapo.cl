#!/usr/bin/python3
import argparse
import datetime
import os
import urllib.request
import json
import ssl

def read_args():
    def date(date_string):
        return datetime.datetime.strptime(date_string, '%Y-%m-%d %H:%M:%S')

    parser = argparse.ArgumentParser(description='Partner report of inserted and modified ads')
    parser.add_argument('-u', '--url', required=True, help='Url to list active Partners')
    parser.add_argument('-i', '--init_date', required=True, type=date, help='Init date. Format: YYYY-MM-DD H:M:S')
    parser.add_argument('-e', '--end_date', required=True, type=date, help='End date. Format: YYYY-MM-DD H:M:S')
    parser.add_argument('-p', '--port', required=True, help='Port to execute trans')
    parser.add_argument('-t', '--trans_host', required=True, help='Host to execute trans')
    return parser.parse_args()

def send_trans_report(partner_data, init_date, end_date, port, trans_host):
    cmd = "cmd:send_partner_ads_report\n"
    cmd += "commit:1\n"
    cmd += "partner_name:{}\n".format(partner_data['KeyName'])
    cmd += "partner_pretty_name:{}\n".format(partner_data['Name'])
    cmd += "email:{}\n".format(partner_data['MailTo'].replace(" ",""))
    cmd += "init_date:{}\n".format(init_date)
    cmd += "end_date:{}\n".format(end_date)
    cmd += "end\n"
    print('exec: printf "{query}" | nc {trans_host} {port}'.format(query=cmd, port=port, trans_host=trans_host))
    str_trans = os.popen('printf "{query}" | nc {trans_host} {port}'.format(query=cmd, port=port, trans_host=trans_host)).read()
    print(str_trans)
    return "TRANS_OK" in str_trans

def main():
    args = read_args()

    #Get active partners
    response = urllib.request.urlopen(
        args.url,
        context=ssl._create_unverified_context()).read()
    content = json.loads(response.decode('utf-8'))
    active_partners = content['Active']

    for _, partner_data in active_partners.items():
        result = send_trans_report(partner_data, args.init_date, args.end_date, args.port, args.trans_host)
        if not result:
            print('Trans send_partner_ads_report error!')

if __name__ == "__main__":
    main()
