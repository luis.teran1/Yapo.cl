<?
ini_set("max_execution_time", "160");
function find_bad_rows($file = NULL) {
	if ($file === NULL) {
		$stdin = fopen('php://stdin', 'r');
		$content = '';

		while (!feof($stdin))
			$content .= fread($stdin, 1024);

		fclose($stdin);
		$rows = explode("\n", $content);
	} else {
		$rows = file($file);
	}
	$BCONF = array();

	foreach ($rows as $row) {
		$row = trim($row);

		if (ereg("^[%#]", $row))
			continue;
		if (empty($row))
			continue;

		list ($name, $value) = explode('=', $row, 2);

		$config_tree = explode('.', $name);
		$node = array_shift($config_tree);

		$conf =& $BCONF[$node];
		foreach ($config_tree as $node) {
			if (is_scalar($conf))
				die("Error node type: $row\n");
			$conf =& $conf[$node];
		}

		if (is_array($conf))
			die("Error node type: $row\n");

		if (strlen($conf) && !ereg(".in$", $file))
			die("Error duplicate: $row\n");

		$conf = $value;
	}

}

find_bad_rows();
?>
