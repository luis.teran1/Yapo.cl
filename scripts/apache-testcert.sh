#!/bin/bash

DESTDIR=$1
CACHEDIR=$2
HOSTHTTPDCONF=/etc/httpd/conf

if [ -r $HOSTHTTPDCONF/ssl.crt/server.crt -a -r $HOSTHTTPDCONF/ssl.key/server.key ] ; then
   if [ -r $HOSTHTTPDCONF/ssl.crt/ca.crt ] ; then
	cp $HOSTHTTPDCONF/ssl.crt/ca.crt $DESTDIR/conf/
   fi
   cp $HOSTHTTPDCONF/ssl.crt/server.crt $DESTDIR/conf/
   cp $HOSTHTTPDCONF/ssl.key/server.key $DESTDIR/conf/

else 
    if [ -n "$CACHEDIR" ] && [ -r $CACHEDIR/server.key -a -r $CACHEDIR/server.crt -a "$(date -d "`openssl x509 -noout -in $CACHEDIR/server.crt -enddate | cut -d = -f 2`" +'%s')" -gt "$(date -d 'now +30 min' +'%s')" ] ; then
	echo "Reusing old cert"
	cp $CACHEDIR/server.key $CACHEDIR/server.crt $DESTDIR/conf/
    else
       
	echo "There is no host certificate on this server, generating my own"

	#IP_NUMBER_ETH0=$(grep `hostname` /etc/hosts | awk '{ print $1; }')
	DOMAIN_NAME=`hostname -f`
	ISSUER=$USER.$(date +%s)
	
	openssl genrsa -out $DESTDIR/conf/server.key 1024
	openssl req -new -key $DESTDIR/conf/server.key -out $DESTDIR/conf/server.csr <<EOF
ES
Madrid
Vaciamadrid
$ISSUER
Risk Enterprises
$DOMAIN_NAME
risk@risk.ri

yapo.cl
EOF
	openssl x509 -req -signkey $DESTDIR/conf/server.key -days 30 -in $DESTDIR/conf/server.csr -out $DESTDIR/conf/server.crt
	if [ -n "$CACHEDIR" ] && [ -d "$CACHEDIR" ]; then
	    cp $DESTDIR/conf/server.crt $DESTDIR/conf/server.key $CACHEDIR
	fi
    fi
fi
hashfile=$DESTDIR/conf/`openssl x509 -hash -in $DESTDIR/conf/server.crt -noout`.0
rm -f $hashfile
ln -s server.crt $hashfile
