#!/bin/bash

SU=`which su`
RSYNC=`which rsync`
QUIET='FALSE'

IMAGES_LOCAL_PATH='/opt/images'
TARGET_SERVER_HOST='schbstd.dyndns.org'
TARGET_SERVER_PORT='122'
TARGET_SERVER_USER='backup'
TARGET_PATH='/opt/backups/images'

SETCOLOR_GREEN="echo -en \\033[1;32m"
SETCOLOR_RED="echo -en \\033[1;31m"
SETCOLOR_YELLOW="echo -en \\033[1;33m"
SETCOLOR_NORMAL="echo -en \\033[0;39m"
SETCOLOR_BOLD="echo -en \\033[1;39m"

# Check user
if [ $(id -u) -ne 0 ] ; then
	$SETCOLOR_RED;
	echo 'Only root can execute this script'
	$SETCOLOR_NORMAL;
	exit -1;
fi

errorParameter() {
	echo "\t--quiet\t\t\tOnly display message errors (for cron executions)" 
	echo 
	echo "Example: $0 --quiet" 
	echo
	exit -1
}

while (( $# ))
do
	case $1 in
	--quiet )
		QUIET='TRUE'
	;;
	* )
		$SETCOLOR_RED;
		echo 'Incorrect parameters.'
		$SETCOLOR_NORMAL;
		echo 'Please, check the parameters.'
		echo 
		errorParameter
	esac
	shift
done

if test "$QUIET" = 'FALSE'; then
	echo -n "Syncronizing images in [$IMAGES_LOCAL_PATH] with [$TARGET_SERVER_USER@$TARGET_SERVER_HOST:$TARGET_SERVER_PORT/$TARGET_PATH]: "
fi

if test "$QUIET" = 'FALSE'; then
	$RSYNC --verbose --recursive --links --perms --owner --group --times --rsh="ssh -p$TARGET_SERVER_PORT" --delete $IMAGES_LOCAL_PATH/* $TARGET_SERVER_USER@$TARGET_SERVER_HOST:$TARGET_PATH
else
	$RSYNC --quiet --recursive --links --perms --owner --group --times --rsh="ssh -p$TARGET_SERVER_PORT" --delete $IMAGES_LOCAL_PATH/* $TARGET_SERVER_USER@$TARGET_SERVER_HOST:$TARGET_PATH
fi

if (( $? != 0 )); then
	if test "$QUIET" = 'FALSE'; then
		$SETCOLOR_RED;
	fi
	echo 'ERROR syncronizing images!'
	if test "$QUIET" = 'FALSE'; then
		$SETCOLOR_NORMAL;
	fi
	exit -1
fi

if test "$QUIET" = 'FALSE'; then
	$SETCOLOR_GREEN;
	echo "Images sync succesfully"
	$SETCOLOR_NORMAL;
fi

exit 0
