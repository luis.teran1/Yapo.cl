#!/bin/bash

SU=`which su`
PGDUMP=`which pg_dump`
SCP=`which scp`
BZIP=`which bzip2`
QUIET='FALSE'
COMPRESS='TRUE'
SEND_DUMP='TRUE'

DB_NAME='blocketdb'
BACKUP_TMP_PATH='/opt/backups/psql'
TARGET_SERVER_HOST='schbstd.dyndns.org'
TARGET_SERVER_PORT='122'
TARGET_SERVER_USER='backup'
TARGET_PATH='/opt/backups/db'

SETCOLOR_GREEN="echo -en \\033[1;32m"
SETCOLOR_RED="echo -en \\033[1;31m"
SETCOLOR_YELLOW="echo -en \\033[1;33m"
SETCOLOR_NORMAL="echo -en \\033[0;39m"
SETCOLOR_BOLD="echo -en \\033[1;39m"

# Check user
if [ $(id -u) -ne 0 ] ; then
	$SETCOLOR_RED;
	echo 'Only root can execute this script'
	$SETCOLOR_NORMAL;
        exit -1;
fi

errorParameter() { 
	echo "\t--no-compress\t\tDont compress the database dump with BZIP2" 
	echo "\t--dont-send\t\tDont send the dump file to $TARGET_SERVER_HOST:$TARGET_SERVER_PORT/$TARGET_PATH"
	echo "\t--quiet\t\t\tOnly display message errors (for cron executions)" 
	echo 
	echo "Example: $0 --no-compress --dont-send --quiet" 
	echo
        exit -1 
}

while (( $# ))
do
        case $1 in
        --quiet )
                QUIET='TRUE'
        ;;
        --dont-send )
                SEND_DUMP='FALSE'
        ;;
	--no-compress )
		COMPRESS='FALSE'
	;;
        * )
		$SETCOLOR_RED;
                echo 'Incorrect parameters.'
		$SETCOLOR_NORMAL;
                echo 'Please, check the parameters.'
                echo 
                errorParameter
        esac
        shift
done

if test "$QUIET" = 'FALSE'; then
	echo -n "Creating backup of [$DB_NAME] database: "
fi

OUTPUT_FILE="$BACKUP_TMP_PATH/db.$DB_NAME.`date +%Y_%m_%d`.sql"
$SU - postgres -c "$PGDUMP --format=p --create --encoding=UTF8 $DB_NAME > $OUTPUT_FILE"
if (( $? != 0 )); then
	if test "$QUIET" = 'FALSE'; then
		$SETCOLOR_RED;
	fi
	echo 'ERROR exporting database!'
	if test "$QUIET" = 'FALSE'; then
		$SETCOLOR_NORMAL;
	fi
	exit -1
fi

if test "$QUIET" = 'FALSE'; then
	$SETCOLOR_BOLD;
	echo 'Dump complete!'
	$SETCOLOR_NORMAL;
fi

if test "$COMPRESS" = 'TRUE'; then
	if test "$QUIET" = 'FALSE'; then
		echo -n "Compresing file: "
	fi
	$BZIP $OUTPUT_FILE
	if (( $? != 0 )); then
		if test "$QUIET" = 'FALSE'; then
			$SETCOLOR_RED;
		fi
		echo "ERROR compressing file [$OUTPUT_FILE]!"
		if test "$QUIET" = 'FALSE'; then
			$SETCOLOR_NORMAL;
		fi
	        exit -1
	fi
	if test "$QUIET" = 'FALSE'; then
		$SETCOLOR_BOLD;
		echo 'Compression complete!'
		$SETCOLOR_NORMAL;
	fi
	OUTPUT_FILE="$OUTPUT_FILE.bz2"
fi

if test "$SEND_DUMP" = 'TRUE'; then
	if test "$QUIET" = 'FALSE'; then
		echo -n "Sending dump file to [$TARGET_SERVER_HOST:$TARGET_SERVER_PORT]: "
	fi

	$SCP -P$TARGET_SERVER_PORT $OUTPUT_FILE $TARGET_SERVER_USER@$TARGET_SERVER_HOST:$TARGET_PATH 1> /dev/null
	if (( $? != 0 )); then
		if test "$QUIET" = 'FALSE'; then
			$SETCOLOR_RED;
		fi
		echo 'ERROR sending the dump file!'
		if test "$QUIET" = 'FALSE'; then
			$SETCOLOR_NORMAL;
		fi
		exit -1
	fi

	if test "$QUIET" = 'FALSE'; then
		$SETCOLOR_BOLD;
		echo 'Sending complete!'
		$SETCOLOR_NORMAL;
	fi

	if test "$QUIET" = 'FALSE'; then
                echo -n "Removing database dump file [$OUTPUT_FILE]: "
        fi

	rm -f $OUTPUT_FILE 1> /dev/null
	if (( $? != 0 )); then
		if test "$QUIET" = 'FALSE'; then
			$SETCOLOR_RED;
		fi
		echo 'ERROR deleting the dump file!'
		if test "$QUIET" = 'FALSE'; then
			$SETCOLOR_NORMAL;
		fi
		exit -1
	fi
fi

if test "$QUIET" = 'FALSE'; then
	$SETCOLOR_GREEN;
	if test "$COMPRESS" = 'TRUE'; then
		echo "Database $DB_NAME dumped and compressed in [$OUTPUT_FILE] succesfully"
	else
		echo "Database $DB_NAME dumped in [$OUTPUT_FILE] succesfully"
	fi
	$SETCOLOR_NORMAL;
fi

exit 0
