#!/bin/bash

WORKDIR=tmp
SPLITAT=100
PREFIXLEN=5
NPROCS=14
LIMIT=1000

# Database variables.
DBHOST=
DBPORT=
DBNAME=
DBUSER=
DBPASSFILE=

# Trans variables.
TRANSHOST=
TRANSPORT=

# Describes the usage of the script
function usage() {
	local scriptname=`basename "$0"`
	echo "usage: ${scriptname} -h DATABASE HOST -p DATABASE PORT -n DATABASE NAME"
	echo "                     -u DATABASE USER [-f DATABASE PASSFILE] -T TRANS HOST -P TRANS PORT"
	echo "                     -w WORKING DIR [-s EMAILS PER FILE] [-n PROCESSES] [-l LIMIT]"	
	echo "DESCRIPTION:"
	echo " -h"
	echo "	Database hostname."
	echo " -p"
	echo "	Database port."
	echo " -u"
	echo "	Database username."
	echo " -f"
	echo "	Database passfile. Use if the database requires authentication"
	echo " -d"
	echo "	Database name."
	echo " -T"
	echo "	Trans host."
	echo " -P"
	echo "	Trans port."
	echo " -w"
	echo "	Working directory to store the temporary files."
	echo " -s"
	echo "	Number of emails on each temporary file. Default: 100"
	echo " -l"
	echo "	Number of emails to retrieve from the database. Default: 1000"
	echo ' -n'
	echo "	Number of process to use for refreshing the emails. Default: 14"
	echo ""
	exit 0	
}

# Calls the refresh_account_cache with the given email, trans host, and trans port
function refresh_account() {
	local email=$1
	local host=$2
	local port=$3
	local command="cmd:refresh_account_cache\nemail:${email}\ncommit:1\nend\n"
	local result=$(printf ${command} | nc ${host} ${port} | grep "status:TRANS_OK" | grep -c -v "error")
	if [[ $result -eq 1 ]]; then
		echo "OK"
	else
		echo "ERROR ${email}"
	fi
}

export refresh_account

# Gets the list of emails from the database, split it on multiple files, and refresh the accounts.
# The refresh returns a list with OK or ERROR and the email. We use tee to send this list to 2 commands:
#     - stats function returns the total of emails processed
#     - a grep command to get all the ERROR messages.
# We also send the output to /dev/null to avoid printing the result from refresh account
function refresh_from_db() {
	get_email_list | split_emails && refresh_files ${WORKDIR}/* | tee >(stats) >(grep "ERROR") > /dev/null
}

# Refresh the the emails given in a list from a file
function refresh_account_file() {	
	xargs -n 1 -P ${NPROCS} -i \
	bash -c "$(declare -f refresh_account); refresh_account {} ${TRANSHOST} ${TRANSPORT} "
}

# Refresh the emails in a list of files
function refresh_files() {
	for file in $@
	do
		cat ${file} | refresh_account_file
		rm ${file}
	done
}

# Gets the all the emails from all active accounts in the database
function get_email_list() {
	local limit_arg=""
	if [[ "${LIMIT}" -gt "0" ]]; then
		limit_arg=" LIMIT ${LIMIT}"
	fi
	local query="SELECT email FROM accounts WHERE status = 'active'${limit_arg}"
	
	env PGHOST=${DBHOST} \
		PGPORT=${DBPORT} \
		PGUSER=${DBUSER} \
		PGPASSFILE=${DBPASSFILE} \
		PGDATABASE=${DBNAME} \
		psql -t -c "${query}" | sed "/^\s*$/ d" 
}

# Splits a given list of emails in multiple files
function split_emails() {
	split -a ${PREFIXLEN} -d -l ${SPLITAT} - ${WORKDIR}/emails.
}

function stats() {
	local total=$(wc -l $1)
	echo "TOTAL:${total}"
}


while getopts ":h:p:u:f:d:T:P:w:s:l:n:" opt; do
	case "${opt}" in
		h)
			if [[ -n "$OPTARG" ]]; then
				DBHOST=$OPTARG
			else
				echo "Invalid database host"
				usage
			fi
			;;
		p)
			if [[ $OPTARG =~ ^[0-9]+$ ]]; then
				DBPORT=$OPTARG
			else
				echo "Invalid database port"
				usage
			fi
			;;
		d)
			if [[ -n "$OPTARG" ]]; then
				DBNAME=$OPTARG
			else
				echo "Invalid database name"
				usage
			fi
			;;
		u)
			if [[ -n "$OPTARG" ]]; then
				DBUSER=$OPTARG
			else
				echo "Invalid user"
				usage
			fi
			;;
		f)
			if [[ -n "$OPTARG" ]]; then
				DBPASSFILE=$OPTARG
			else
				echo "Invalid passfile"
				usage
				fi
				;;
		T)
			if [[ -n "$OPTARG" ]]; then
				TRANSHOST=$OPTARG
			else
				echo "Invalid trans host"
				usage
			fi
			;;
		P)
			if [[ $OPTARG =~ ^[0-9]+$ ]]; then
				TRANSPORT=$OPTARG
			else
				echo "Invalid trans port"
				usage
			fi
			;;
		w)
			if [[ -n "$OPTARG" ]]; then
				WORKDIR=$OPTARG
			else
				echo "Empty working dir"
				usage
			fi
			;;
		s)
			if [[ $OPTARG =~ ^[0-9]+$ ]]; then
				SPLITAT=$OPTARG
			fi
			;;
		l)
			if [[ $OPTARG =~ ^[0-9]+$ ]]; then
				LIMIT=$OPTARG
			fi
			;;
		n)
			if [[ $OPTARG =~ ^[0-9]+$ ]]; then
				NPROCS=$OPTARG
			fi
			;;
		\?)
			usage
			;;
	esac
done
shift $((OPTIND-1))

[ "${1:-}" = "--" ] && shift

if [[ -z "$DBHOST" || -z "$DBPORT" || -z "$DBNAME" || -z "$DBUSER" || \
	-z "$TRANSHOST" || -z "$TRANSPORT" || -z "$WORKDIR" ]]; then
	usage
fi
time refresh_from_db
