#!/bin/bash

REDIS=/usr/bin/redis-cli
REDIS_HOST=ch36
REDIS_ACCOUNTS_PORT=6398
EMAIL_REGEX="^[a-z0-9!#\$%&'*+/=?^_\`{|}~-]+(\.[a-z0-9!#$%&'*+/=?^_\`{|}~-]+)*@([a-z0-9]([a-z0-9-]*[a-z0-9])?\.)+[a-z0-9]([a-z0-9-]*[a-z0-9])?\$"

if [[ $1 =~ ${EMAIL_REGEX} ]] 
then
	echo "Deleting ad list cache for $1"
	${REDIS} -h ${REDIS_HOST} -p ${REDIS_ACCOUNTS_PORT} DEL rad1:ads_list:$1
else
	echo -e "Usage: $0 <email>\nDeletes the ad list cache for given email"
fi
