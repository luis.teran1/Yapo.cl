#!/bin/bash

REDIS=/usr/bin/redis-cli
REDIS_HOST=ch36
REDIS_ACCOUNTS_PORT=6398
REDIS_DB=2
EMAIL_REGEX="^[a-z0-9!#\$%&'*+/=?^_\`{|}~-]+(\.[a-z0-9!#$%&'*+/=?^_\`{|}~-]+)*@([a-z0-9]([a-z0-9-]*[a-z0-9])?\.)+[a-z0-9]([a-z0-9-]*[a-z0-9])?\$"

# Test whether an ad is published or not and add it to INACTIVE_ADS
test_ad() {
	ad=$1
	echo -n "$ad - "
	if [[ $(printf "id:$ad\n" | nc ch2 9080 | wc -l) -eq 5 ]]
	then
		echo "published"
	else
		echo "inactive"
		INACTIVE_ADS+="$ad "
	fi
}

# clear_ads(email, ad1[, adn]*) deletes given ads from cart for that email
clear_ads() {
	echo "Clearing ads..."
	email=$1
	shift
	for ad in $*
	do
		echo -n "$ad - "
		for i in $(seq 1 8)
		do
			${REDIS} -h ${REDIS_HOST} -p ${REDIS_ACCOUNTS_PORT} -n ${REDIS_DB} HDEL rad1:cart_info:$email:$i $ad
		done
	done
}

# Ask for permission to delete inactive
clear_inactive() {
	email=$1
	if [ -n "$INACTIVE_ADS" ]
	then
		while true; do
		    read -p "Do you want to clear inactive ad products from user cart? [y/n] " yn
		    case $yn in
			[Yy]* ) clear_ads $email $INACTIVE_ADS; break;;
			* ) exit;;
		    esac
		done
	else
		echo "Cart looks good to me, babe"
	fi
}

if [[ $1 =~ ${EMAIL_REGEX} ]] 
then
	AD_LIST=
	for i in $(seq 1 8)
	do
		AD_LIST+=" "
		AD_LIST+=$(${REDIS} -h ${REDIS_HOST} -p ${REDIS_ACCOUNTS_PORT} -n ${REDIS_DB} HKEYS rad1:cart_info:$1:$i)
	done
	for ad in $AD_LIST
	do
		test_ad $ad
	done
else
	echo -e "Usage: $0 <email>\nChecks cart contents for the given email and offers to clean it up"
	exit 1
fi

clear_inactive $1
