#!/bin/bash

REDIS=/usr/bin/redis-cli
REDIS_HOST=10.45.1.147
REDIS_SESSION_PORT=6350
EMAIL_REGEX="^[a-z0-9!#\$%&'*+/=?^_\`{|}~-]+(\.[a-z0-9!#$%&'*+/=?^_\`{|}~-]+)*@([a-z0-9]([a-z0-9-]*[a-z0-9])?\.)+[a-z0-9]([a-z0-9-]*[a-z0-9])?\$"
SESSION_REGEX="^[a-z0-9]{40}\$"

# clear_ads(email, ad1[, adn]*) deletes given ads from cart for that email
steal_session() {
	session=$1
	email=$2
	echo "Stealing session..."
	${REDIS} -h ${REDIS_HOST} -p ${REDIS_SESSION_PORT} HSET rsd1x_session_${session} email $email
}

# Ask for permission to delete inactive
shall_we_replace() {
	old_email=$1
	new_email=$2
	session=$3
	while true; do
	    read -p "Do you want to change the session from $old_email to $new_email ? [y/n] " yn
	    case $yn in
		[Yy]* ) steal_session $session $new_email; break;;
		* ) exit;;
	    esac
	done
}

if [[ $1 =~ ${SESSION_REGEX} ]] && [[ $2 =~ ${EMAIL_REGEX} ]] 
then
	OLD_EMAIL=$(${REDIS} -h ${REDIS_HOST} -p ${REDIS_SESSION_PORT} HGET rsd1x_session_${1} email)
	shall_we_replace $OLD_EMAIL $2 $1
else
	echo -e "Usage: $0 <session> <email>\nReplaces session email with given email (steal an user's session)"
	exit 1
fi

