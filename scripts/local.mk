TOPDIR?=..

INSTALLDIR=/sbin

INSTALL_TARGETS=blocket_config.pl

IN_FILES=blocket_config.pl.in

include ${TOPDIR}/mk/generate.mk
include ${TOPDIR}/mk/in.mk
include ${TOPDIR}/mk/install.mk
include ${TOPDIR}/mk/defvars.mk
