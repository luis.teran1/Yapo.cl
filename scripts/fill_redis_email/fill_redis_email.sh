#!/bin/bash

WORKDIR=tmp
NPROCS=14
LIMIT=1000
USERPREFIX=reu1x
ACCOUNTPREFIX=rea1x

# Database variables.
DBHOST=
DBPORT=
DBNAME=
DBUSER=

# Redis variables.
REDISHOST=
REDISPORT=

# Describes the usage of the script
function usage() {
	local scriptname=`basename "$0"`
	echo "usage: ${scriptname} -h DATABASE HOST -p DATABASE PORT -n DATABASE NAME"
	echo "                     -u DATABASE USER -R REDIS HOST -P REDIS PORT"
	echo "                     -w WORKING DIR [-n PROCESSES] [-l LIMIT]"	
	echo "DESCRIPTION:"
	echo " -h"
	echo "	Database hostname."
	echo " -p"
	echo "	Database port."
	echo " -u"
	echo "	Database username."
	echo " -d"
	echo "	Database name."
	echo " -R"
	echo "	Redis host."
	echo " -P"
	echo "	Redis port."
	echo " -w"
	echo "	Working directory to store the temporary files."
	echo " -l"
	echo "	Number of emails to retrieve from the database. Default: 1000"
	echo ' -n'
	echo "	Number of process to use for refreshing the emails. Default: 14"
	echo ""
	exit 0	
}

# Gets the list of email, account_id and user_id from the database, split it on multiple files, and populate the email redis.
# We also send the output to /dev/null to avoid printing the result from refresh account
function fill_redis_email_db() {
	rm ${WORKDIR}/commands.tmp 2> /dev/null
	get_parameters_list | generate_commands >> ${WORKDIR}/commands.tmp
	$(sed -i -e 's/\r*$/\r/' ${WORKDIR}/commands.tmp)
	cat ${WORKDIR}/commands.tmp | redis-cli -h ${REDISHOST} -p ${REDISPORT} --pipe
	rm ${WORKDIR}/commands.tmp
}

# Gets the all the emails, user_id's and account_id's from all active and pending  accounts in the database
function get_parameters_list() {
	local limit_arg=""
	if [[ "${LIMIT}" -gt "0" ]]; then
		limit_arg=" LIMIT ${LIMIT}"
	fi
	local query="SELECT user_id, account_id, email FROM accounts WHERE status IN ('active', 'pending_confirmation')${limit_arg}"
	
	env PGHOST=${DBHOST} \
		PGPORT=${DBPORT} \
		PGUSER=${DBUSER} \
		PGDATABASE=${DBNAME} \
		psql -t -c "${query}" -A -F " " | sed "/^\s*$/ d" 
}

function generate_commands() {
	xargs -n 3 -L 1 -P ${NPROCS} -i \
	bash -c "$(declare -f generate_command); generate_command {} ${USERPREFIX} ${ACCOUNTPREFIX}"
}

# parses the parameters and generate the corresponding redis commands
function generate_command() {
	local user_id=$1
	local account_id=$2
	local email=$3
	local user_prefix=$4
	local account_prefix=$5
	local user_hash=$(echo -n $user_id | sha1sum | awk '{ print $1 }')
	local account_hash=$(echo -n $account_id | sha1sum | awk '{ print $1 }')
	local user_key="${user_prefix}${user_hash}"
	local account_key="${account_prefix}${account_hash}"
	echo "SET \"${user_key}\" \"${email}\""
	echo "SET \"${account_key}\" \"${email}\""

}

export generate_commands

while getopts ":h:p:u:d:R:P:w:l:n:" opt; do
	case "${opt}" in
		h)
			if [[ -n "$OPTARG" ]]; then
				DBHOST=$OPTARG
			else
				echo "Invalid database host"
				usage
			fi
			;;
		p)
			if [[ $OPTARG =~ ^[0-9]+$ ]]; then
				DBPORT=$OPTARG
			else
				echo "Invalid database port"
				usage
			fi
			;;
		d)
			if [[ -n "$OPTARG" ]]; then
				DBNAME=$OPTARG
			else
				echo "Invalid database name"
				usage
			fi
			;;
		u)
			if [[ -n "$OPTARG" ]]; then
				DBUSER=$OPTARG
			else
				echo "Invalid user"
				usage
			fi
			;;
		R)
			if [[ -n "$OPTARG" ]]; then
				REDISHOST=$OPTARG
			else
				echo "Invalid redis host"
				usage
			fi
			;;
		P)
			if [[ $OPTARG =~ ^[0-9]+$ ]]; then
				REDISPORT=$OPTARG
			else
				echo "Invalid redis port"
				usage
			fi
			;;
		w)
			if [[ -n "$OPTARG" ]]; then
				WORKDIR=$OPTARG
			else
				echo "Empty working dir"
				usage
			fi
			;;
		l)
			if [[ $OPTARG =~ ^[0-9]+$ ]]; then
				LIMIT=$OPTARG
			fi
			;;
		n)
			if [[ $OPTARG =~ ^[0-9]+$ ]]; then
				NPROCS=$OPTARG
			fi
			;;
		\?)
			usage
			;;
	esac
done
shift $((OPTIND-1))

[ "${1:-}" = "--" ] && shift

if [[ -z "$DBHOST" || -z "$DBPORT" || -z "$DBNAME" || \
	-z "$REDISHOST" || -z "$REDISPORT" || -z "$WORKDIR" ]]; then
	usage
fi
time fill_redis_email_db
