#!/bin/bash

# Database variables.
DBHOST=
DBPORT=
DBNAME=
DBUSER=
DBPASSFILE=

# Trans variables.
TRANSHOST=
TRANSPORT=

# Implio variables.
IMPLIOENDPOINT=""


# Describes the usage of the script
function usage() {
	local scriptname=`basename "$0"`
	echo "usage: ${scriptname} -h DATABASE HOST -p DATABASE PORT -n DATABASE NAME"
	echo "                     -u DATABASE USER -I IMPLIO ENDPOINT"
	echo "DESCRIPTION:"
	echo " -h"
	echo "	Database hostname."
	echo " -p"
	echo "	Database port."
	echo " -u"
	echo "	Database username."
	echo " -d"
	echo "	Database name."
	echo " -I"
	echo "	Implio endpoint for adding or deleting refusal reasons."
	echo ""
	exit 0	
}

function send_refusal_reason() {
	local host=$1
	local id_name=${@:2}
	IFS='|' read -r id name <<< "$id_name"
	echo "REFUSAL REASON: id: $id name: $name"
	echo "create refusal reason"
	echo $(curl -s -X POST -H "Content-Type: application/json" -d "{\"reason\":{\"reasonID\":${id}, \"reasonName\":\"${name}\"}}" $host)
}

export send_refusal_reason

function send_refusal_reasons() {
	xargs -n 2 -L 1 -i \
	bash -c "$(declare -f send_refusal_reason); send_refusal_reason ${IMPLIOENDPOINT} \"{}\""
}


# Gets the id and name of all the refusal reasons in the database
function get_refusal_reasons() {
	local query="
		SET client_encoding = 'UTF8';
		SELECT s.token::integer as token, value FROM conf c, substring(c.key FROM '[0-9]+') s(token) WHERE key LIKE '%*.*.common.refusal.%.name%' order by cast(token as integer) asc;
	"
	env PGHOST=${DBHOST} \
		PGPORT=${DBPORT} \
		PGUSER=${DBUSER} \
		PGDATABASE=${DBNAME} \
		psql -A -t -c "${query}" | sed "/^\s*$/ d"
}

function push_refusal_reasons() {
	get_refusal_reasons | send_refusal_reasons
}


while getopts ":h:p:u:d:I:" opt; do
	case "${opt}" in
		h)
			if [[ -n "$OPTARG" ]]; then
				DBHOST=$OPTARG
			else
				echo "Invalid database host"
				usage
			fi
			;;
		p)
			if [[ $OPTARG =~ ^[0-9]+$ ]]; then
				DBPORT=$OPTARG
			else
				echo "Invalid database port"
				usage
			fi
			;;
		d)
			if [[ -n "$OPTARG" ]]; then
				DBNAME=$OPTARG
			else
				echo "Invalid database name"
				usage
			fi
			;;
		u)
			if [[ -n "$OPTARG" ]]; then
				DBUSER=$OPTARG
			else
				echo "Invalid user"
				usage
			fi
			;;
		I)
			if [[ -n "$OPTARG" ]]; then
				IMPLIOENDPOINT=$OPTARG
			else
				echo "Invalid implio endpoint"
				usage
			fi
			;;
		\?)
			usage
			;;
	esac
done
shift $((OPTIND-1))

[ "${1:-}" = "--" ] && shift

if [[ -z "$DBHOST" || -z "$DBPORT" || -z "$DBNAME" || -z "$DBUSER" || -z "$IMPLIOENDPOINT" ]]; then
	usage
fi
push_refusal_reasons
