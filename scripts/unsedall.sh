find templates -type f | grep -E '\.tmpl$|\.tmpl\.in$' | while read file; do
	if head -n 1 "$file" | egrep "TEMPLATE_TRACE" >/dev/null; then
		echo "Deleting traces on $file..."
		sed -i -e "1d" "$file" 
	fi
done
