
usage() {
	echo
	echo "Usage: $0 <old-packages> <new-packages>"
	echo "Each argument will be searched for on /tmp in the following way: "
	echo "given '8.02.1' it will look for '/tmp/8.02.1.tgz'"
	echo "Sample usage: $0 8.02.0 8.02.1"
	echo
	exit 1
}

if [[ $# -lt 2 ]]
then
	usage
fi

old=$1
new=$2

echo "Comparing ${new} to ${old}"

if ! test -r /tmp/${old}.tgz
then
	echo "file /tmp/${old}.tgz does not exist!"
	usage
fi

if ! test -r /tmp/${new}.tgz
then
	echo "file /tmp/${new}.tgz does not exist!"
	usage
fi

# setup environment
rm -rf ~/.changed
mkdir -p ~/.changed/${old}
mkdir -p ~/.changed/${new}
tar xfz /tmp/${old}.tgz -C ~/.changed/${old}
tar xfz /tmp/${new}.tgz -C ~/.changed/${new}

# truncate version numbers and leave package name only
for target in ~/.changed/${old} ~/.changed/${new}
do
	for dir in $(ls ${target})
	do
		for file in $(ls ${target}/${dir})
		do
			mv ${target}/${dir}/${file} ${target}/${dir}/${file%-*-*}
		done
	done
done

# compare each rpm individually
for rpm in $(find ~/.changed/${new} -type f)
do
	if file ${rpm} | grep RPM &> /dev/null
	then
#		echo "comparing ${rpm} and ${rpm/${new}/${old}}"
		(cd ~/.changed/${old} && rpm2cpio $rpm | cpio -idm 2> /dev/null)
		(cd ~/.changed/${new} && rpm2cpio ${rpm/${new}/${old}} | cpio -idm 2> /dev/null)
		diff -qr ~/.changed/${new}/opt ~/.changed/${old}/opt
		if [ $? -ne 0 ]
		then
			echo Update `basename $rpm`
		fi
		rm -Rf ~/.changed/${new}/opt ~/.changed/${old}/opt
	fi
done
