local output = {}
	for n, key in pairs(KEYS) do
		local views = redis.call('ZSCORE','VIEW:total',key)
		local mails = redis.call('ZSCORE','MAIL:total',key)
		table.insert(output, ''..key)
		if views then
			table.insert(output,''..views)
		else
			table.insert(output,'0')
		end
		if mails then
			table.insert(output, ''..mails)
		else
			table.insert(output,'0')
		end
	end
return output
