local output = {}
local val
for n, key in pairs(KEYS) do
	if redis.call('EXISTS', key) == 1 then
		val = redis.call('GET', key)
	else
		val = 'null'
	end
	table.insert(output, ''..val)
end
return output
