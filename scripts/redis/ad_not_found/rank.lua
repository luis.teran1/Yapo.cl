local words_key = KEYS[1]
local words = string.gmatch(words_key, '%w+')
local rank
local bword
local result

if redis.call('EXISTS', words_key) == 1 then
	for word in words do
		if redis.call('HEXISTS', 'bwords', word) == 1 then
			redis.call('ZREM', words_key, word)
		end
	end
	return redis.call('ZREVRANGE', words_key, 0, ARGV[1]-1)
end

for word in words do
    rank = redis.call('ZRANK', 'words', word)
	if not rank then
		rank = 0
	end
	if redis.call('HEXISTS', 'bwords', word) == 0 then
		redis.call('ZADD', words_key, rank, word)
	end
end
redis.call('EXPIRE', words_key, 3600)
return redis.call('ZREVRANGE', words_key, 0, ARGV[1]-1)
