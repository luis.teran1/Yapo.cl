#!/usr/bin/env perl

#
# fetch_new_images_url.pl
#
# Finds all the images with pattern A in the listing page and request this image's url replacing pattern A for pattern B. Used for pre-populating Varnish
#
# Author: Rodolfo Caldeira and Jaime Moreira
# base on Boris Cruchet get_ads_url.pl script
#
# History:
#
# 2012-07-18 - Rodolfo Caldeira / Jaime Moreira - Begining development.
#

use strict;
use warnings;

# Libraries
use POSIX qw(strftime);
use HTML::LinkExtractor;
use Log::Handler;
use Time::HiRes qw(gettimeofday tv_interval);
use WWW::Curl;
use WWW::Curl::Easy;

# Initialization of variables
my $base_url = "https://www.dev09.yapo.cl";
my $path = "/";
my $debug = 0;
my $old_path = '';
my $new_path = '';
my $from_page = 1;
my $to_page = 1;
my $secs_to_sleep = 2;
my $interval_to_sleep = 20;
my $log_file = "./new_images.log";
my $log = Log::Handler->new(
	screen => {
            log_to   => "STDERR",
            maxlevel => "warning",
            minlevel => "emergency",
	}
);

# help
my $help =
	"Usage: fetch_new_images_url.pl [flags]\n" .
	"\n" .
	"\t-p <path>: Path to test (default is /).\n" .
	"\t-b <base_url>: Base url (default is http://www.yapo.cl).\n" .
	"\t-o <old_path>: url part that will be replaced (can not be empty).\n" .
	"\t-n <new_path>: url part that will replace the old one (default is '').\n" .
	"\t-f <from page>: start page at listing page (default is 1).\n" .
	"\t-t <to page>: end page at listing page (default is 1).\n" .
	"\t-l <log_file>: Log file (default is ./webmeasure.log).\n" .
	"\t-d: Enable debug mode.\n" .
	"\n" .
	"Examples:\n" .
	"\t./fetch_new_images_url.pl -b https://www.dev09.yapo.cl -p /chile?ca=15_s -l ./new_images.log -o thumbs -n thumbsli -f 1 -t 1\n";

my $error = 0;
my $session_id = strftime "%Y%m%d%H%M", localtime;

for(my $i = 0; $i <= $#ARGV; $i++) {
	my $arg = $ARGV[$i];
	if($arg =~ s/^-//) {
		if($arg =~ m/p/ && $i < $#ARGV) {
			$path = $ARGV[++$i];
			if($path =~ m/^http[s]{0,1}:\/\//) {
				$error = 1;
			} elsif($path !~ m/^\//) {
				$path = '/' . $path;
			}
		} elsif($arg =~ m/b/ && $i < $#ARGV) {
			$base_url = $ARGV[++$i];
			if($base_url !~ m/^http[s]{0,1}:\/\//) {
				$error = 1;
			} elsif($base_url =~ m/^.*\/$/) {
				$base_url = substr $base_url, 0, -1;
			}
		} elsif($arg eq 'f' && $i < $#ARGV) {
			$from_page = $ARGV[++$i];
			if ($from_page !~ m/(\d*)/) {
				$error = 1;
			}
		} elsif($arg eq 't' && $i < $#ARGV) {
			$to_page = $ARGV[++$i];
			if ($to_page !~ m/(\d*)/) {
				$error = 1;
			}
		} elsif($arg =~ m/o/ && $i < $#ARGV) {
			$old_path = $ARGV[++$i];
		} elsif($arg =~ m/n/ && $i < $#ARGV) {
			$new_path = $ARGV[++$i];
		} elsif($arg =~ m/l/ && $i < $#ARGV) {
			$log_file = $ARGV[++$i];
		} elsif($arg =~ m/d/) {
			$debug = 1;
		} else {
			$error = 1;
		}
	} elsif($arg =~ m/h/) {
		$error = 1;
	} else {
		$error = 1;
	}
}

if($old_path eq '') {	
	$error = 1;
} 
# In case of error we show the help information.
if($error) {
    print $help;
    exit();
}

# Adding the log file to $log
if($debug) {
	$log->add(
		file => {
			filename => $log_file,
			maxlevel => "debug",
			minlevel => "emergency",
		}
	);
} else {
	$log->add(
		file => {
			filename => $log_file,
			maxlevel => "info",
			minlevel => "emergency",
		}
	);
}

$log->debug($session_id." We are working with curl $WWW::Curl::VERSION.\n");

my $curl = WWW::Curl::Easy->new;
my $curl_img = WWW::Curl::Easy->new;

# A filehandle, reference to a scalar or reference to a typeglob can be used here.
my $response_body;
my $response_body_img;

$curl->setopt(CURLOPT_HEADER, 1);
$curl->setopt(CURLOPT_FOLLOWLOCATION, 1);
$curl_img->setopt(CURLOPT_HEADER, 1);
$curl_img->setopt(CURLOPT_FOLLOWLOCATION, 1);

# start the session
$log->info($session_id." begin transaction $session_id for $base_url$path");

my $count_img = 0;
for(my $i = $from_page; $i <= $to_page; $i++) {

	my $url = $base_url . $path . "&o=$i";	
	$curl->setopt(CURLOPT_URL, $url);
	$log->info("On page: $i");
	$log->info("URL: $url");
	open (my $fileb, ">", \$response_body);
	$curl->setopt(CURLOPT_WRITEDATA, \$fileb);

	# Make the request
	my $retcode = $curl->perform;

	# Looking at the results...
	if($retcode != 0) {
		# Error code, type of error, error message
		print("An error happened: $retcode ".$curl->strerror($retcode)." ".$curl->errbuf."\n");
		exit();
	}

	# Getting from the html file all the files to download
	my $response_code = $curl->getinfo(CURLINFO_HTTP_CODE);
	$log->debug($session_id. " reponse code ".  $response_code . "\n");
	if($response_code == 200) {
		my $extractor = new HTML::LinkExtractor();
		my $new_img_url = '';
		my $list_ids = '';

		$extractor->parse(\$response_body);

		for my $Link(@{$extractor->links}) {
			if ($$Link{tag} eq 'img' && $$Link{src} =~ m/.*$old_path.*/) {
					
				$new_img_url = $$Link{src};
				$new_img_url =~ s!$old_path!$new_path!;
			
				$curl_img->setopt(CURLOPT_URL, $new_img_url);
				$log->info("Requesting img: $new_img_url");
				open (my $filec, ">", \$response_body_img);
				$curl_img->setopt(CURLOPT_WRITEDATA, \$filec);

				# Make the request
				my $retcode_img = $curl_img->perform;

				# Looking at the results...
				if($retcode_img != 0) {
					# Error code, type of error, error message
					print("An error happened: $retcode_img ".$curl_img->strerror($retcode_img)." ".$curl_img->errbuf."\n");
					exit();
				}
				
				if(($interval_to_sleep > 0 ) && (++$count_img % $interval_to_sleep ) == 0 ) {
					$log->info("Total images so far: $count_img");
					$log->info("Sleeping $secs_to_sleep seconds...");
					sleep($secs_to_sleep);
				}

			}
		}
	}
}
$log->info("Total images processed: $count_img");
$log->info($session_id." end transaction $session_id for $base_url$path");
