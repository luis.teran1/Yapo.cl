#!/bin/bash

IAM=`whoami`
CWD="/home/"$IAM"/selenium"

THE_PATH="/home/"$IAM"/selenium"
TEST_PATH=$THE_PATH"/tests"
RESULT_LOG=$TEST_PATH"/result_python_mail.log";

prevTime=`date +%F_%H:%M -d "1 hour ago"`

cd $CWD

echo "Reset result log"
echo -n "" > $RESULT_LOG

echo "Starting mail tests"
./run_tests_mail.sh

echo "Checking results"
./check_result.php tests/result_python_mail.log

echo "End mail tests"

echo "Moving log"
mv log.log logs/log.log.$prevTime
