#!/bin/bash

IAM=`whoami`
ID=`id -u`
THE_PATH="/home/"$IAM"/selenium"
TEST_PATH=$THE_PATH"/tests"

JAVA="/usr/bin/java"
SELENIUM_SERVER=$THE_PATH"/selenium-server-standalone-2.20.0.jar"
SELENIUM_SERVER_PORT=10$ID

PYTHON="/usr/bin/python26"

RESULT_LOG=$TEST_PATH"/result_python.log";

function run_test {
	s=$1 
	test=`echo $s | sed "s-$TEST_PATH/--" `;
	echo ""
	echo $test;
	result=`$PYTHON $s 2>&1 | head -n 1`
	echo $result;

	if [ $result == "." ]; then
		STATUS="OK"
	else
		STATUS="ERROR"
	fi

	echo "$test:$STATUS"
	echo "$test:$STATUS" >> $RESULT_LOG
}	

run_test $TEST_PATH"/test_connection_www.py"
run_test $TEST_PATH"/test_connection_www2.py"

export DISPLAY=:$ID
cp $TEST_PATH"/TryOutBg.jpg" /tmp

$JAVA -jar $SELENIUM_SERVER -port $SELENIUM_SERVER_PORT &

# waiting for selenium server
echo "Waiting for selenium server. Please wait, (please!!!)^2 "
http_req=""
while [ "$http_req" == "" ] ; do
	http_req=`wget 127.0.0.1:$SELENIUM_SERVER_PORT/wd/hub/static/resource/hub.html -O /dev/null 2>&1 | grep "200 OK"`
	sleep 2
	echo -n "."
done

rm $RESULT_LOG

for s in `ls -1 $TEST_PATH/sel_*.py`; do
	run_test $s
done

echo "Killing Selenium server ... (trying):D"
pkill -U $ID java

