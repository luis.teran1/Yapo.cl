#!/bin/bash

IAM=`whoami`
CWD="/home/"$IAM"/selenium"

cd $CWD

echo "Start environment"
./graphic_env.sh start

echo "Runnning HTML tests"
./run_tests_html.sh

echo "Running Python tests"
./run_tests_python.sh

echo "Checking the results"
./check_result.php tests/result_html.log tests/result_python.log

echo "Stop environment"
./graphic_env.sh stop

echo "End"
