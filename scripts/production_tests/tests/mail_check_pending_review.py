#!/usr/bin/python

import getpass, imaplib, sys
import syslog
import logging
from array import *
from datetime import datetime, date, time

def getPartDate():
        months = ["", "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]
        dt = datetime.now()
        dd = dt.day
        hh = dt.hour
        mm = dt.month
        yy = dt.year

	if hh < 10:
		hh="0"+str(hh)

        d = str(dd)+" "+months[mm]+" "+str(yy)+" "+str(hh)
        return d

def cutImportantPartOfDate(s):
        ss = s.split(",")
        ss = ss[1].split("-")
        ss = ss[0].split(":")
        return ss[0].strip()


M = imaplib.IMAP4_SSL("smtp.gmail.com", 993)
M.login("selenium@yapo.cl", "poya2012")
M.select()

emailSubject = ""
emailDate = ""

sub1 = 0
emailsData = []

typ, data = M.search(None, 'ALL')
for i in data[0].split():
	typ, data = M.fetch(i, '(RFC822)')
	for j in data[0][1].split('\r\n'):

		# get the subject
		if len(j) > 0 and (j[0:7] == 'Subject' or sub1 == 1):
			if sub1 == 1:
				sub1 = 0
				emailSubject = emailSubject + j
				continue
			
			emailSubject = j
			sub1 = 1;

		# get the date
		if len(j) > 0 and j[0:4] == 'Date':
			emailDate = j

	emailData = {'id':i, 'subject':emailSubject, 'date':emailDate}
	emailsData.append(emailData)

testSubject = 'ERROR'
testDate = 'ERROR'

for e in emailsData:
	# check the subject
	syslog.syslog("s1: "+e['subject'])
	if e['subject'] == 'Subject: =?ISO-8859-1?Q?Tu_aviso_"Aviso_de_prueba_con_imagen"_en_Yapo.cl_?= =?ISO-8859-1?Q?est=E1_siendo_revisado?=':
		testSubject = 'OK'

		# now check the date
		datePartMail = cutImportantPartOfDate(e['date'])
		datePartNow  = getPartDate()

		syslog.syslog("dPM: -"+datePartMail+"-");
		syslog.syslog("dPN: -"+datePartNow+"-");

		if datePartNow == datePartMail:
			testDate = 'OK'
			M.store(e['id'], '+FLAGS', '\\Deleted')


if testSubject == 'OK':
	if testDate == 'OK':
		print "pending review mail:OK"
	else:
		print "pending review mail (date):ERROR"
else:
	print "pending review mail (subj):ERROR"

M.expunge
M.close()
M.logout()
