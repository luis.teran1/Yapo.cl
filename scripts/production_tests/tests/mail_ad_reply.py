#!/usr/bin/env python2

import getpass, imaplib, sys
import syslog
import logging
from array import *
from datetime import datetime, date, time


#logger = logging.getLogger('myapp')
#hdlr = logging.FileHandler('/home/mrdemo/selenium/LOG_TESTS')
#formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
#hdlr.setFormatter(formatter)
#logger.addHandler(hdlr) 
#logger.setLevel(logging.WARNING)
#logger.error('El log funciona')


def getPartDate():
        months = ["", "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]
        dt = datetime.now()
        dd = dt.day
        hh = dt.hour
        mm = dt.month
        yy = dt.year

	if hh < 10:
		hh="0"+str(hh)

        d = str(dd)+" "+months[mm]+" "+str(yy)+" "+str(hh)
        return d

def cutImportantPartOfDate(s):
        ss = s.split(",")
        ss = ss[1].split("-")
        ss = ss[0].split(":")
        return ss[0].strip()


M = imaplib.IMAP4_SSL("smtp.gmail.com", 993)
M.login("selenium@yapo.cl", "poya2012")
M.select()

emailSubject = ""
emailDate = ""

sub1 = 0
emailsData = []

typ, data = M.search(None, 'ALL')
for i in data[0].split():
	typ, data = M.fetch(i, '(RFC822)')
	for j in data[0][1].split('\r\n'):

		# get the subject
		if len(j) > 0 and (j[0:7] == 'Subject' or sub1 == 1):
#			if sub1 == 1:
#				sub1 = 0
#				emailSubject = emailSubject + j
#				continue
			
			emailSubject = j.strip()
#			sub1 = 1;

		# get the date
		if len(j) > 0 and j[0:4] == 'Date':
			emailDate = j

	emailData = {'id':i, 'subject':emailSubject, 'date':emailDate}
	emailsData.append(emailData)

testSubject = 'ERROR'
testDate = 'ERROR'

for e in emailsData:
	# check the subject
	syslog.syslog("s1: "+e['subject'])
	if e['subject'] == 'Subject: =?ISO-8859-1?Q?Consulta_a_tu_aviso_"Exclusivo_taz=F3n_=FAnico_en?=':
	
		testSubject = 'OK'

		# now check the date
		# e-mail date
		datePartMail = cutImportantPartOfDate(e['date'])
		# now date
		datePartNow  = getPartDate()

		syslog.syslog("dPM: -"+datePartMail+"-")
		syslog.syslog("dPN: -"+datePartNow+"-")

		if datePartNow == datePartMail:
			testDate = 'OK'
			M.store(e['id'], '+FLAGS', '\\Deleted')


if testSubject == 'OK':
	if testDate == 'OK':
		print "ad reply mail:OK"
	else:
		print "ad reply mail (date):ERROR"
else:
	print "ad reply mail (subj):ERROR"

M.expunge
M.close()
M.logout()
