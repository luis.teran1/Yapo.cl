# -*- coding: iso-8859-1 -*-

#from selenium import webdriver
from selenium import selenium
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.keys import Keys
import unittest, time, re


class ad_insertion(unittest.TestCase):

	def setUp(self):
		self.verificationErrors = []
		self.selenium = selenium("localhost", 10511, "*firefox", "http://www2.yapo.cl")
		self.selenium.start()

	def execute_ai_with_image(self):
		sel = self.selenium
		sel.delete_all_visible_cookies();
		sel.open("/")
		sel.click(u"link=Publicar aviso gratis")
		sel.wait_for_page_to_load("30000")
		sel.select("id=region", u"Región Metropolitana")
		sel.fire_event("id=region", "blur")
		sel.select("id=category_group", u"Electrodomésticos")
		sel.fire_event("id=category_group", "blur")
		sel.type("//*[@id='subject']", u"Aviso de prueba con imagen");
		sel.type("id=body", u"Test generado automaticamente.")
		sel.mouse_over("id=image_upload_button");
		sel.type("xpath=//input[@name='file']", "/tmp/TryOutBg.jpg")
		sel.wait_for_condition("selenium.isElementPresent(\"//img[contains(@src, 'thumbs')]\")", "10000")
		sel.click("id=p_ad")
		sel.type("id=name", "Test")
		sel.type("id=email", "selenium@yapo.cl")
		sel.type("id=phone", "69191353")
		time.sleep(1)
		sel.click("name=validate")
		sel.wait_for_page_to_load("30000")
		sel.type("id=email_confirm", "selenium@yapo.cl")
		sel.type("id=passwd", "11111")
		sel.type("name=passwd_ver", "11111")
		sel.click("name=ai_accept_conditions")
		sel.click("name=create")
		sel.wait_for_page_to_load("90000")
		self.failUnless(sel.is_text_present("Gracias!"))

	def test_ad_insertion(self):
		#execute the ad insertion with image
		self.execute_ai_with_image();


        def tearDown(self):
                self.selenium.stop()
                self.assertEqual([], self.verificationErrors)

	def tearDown(self):
		#browser.close()
		self.selenium.stop()
		self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
	unittest.main()
