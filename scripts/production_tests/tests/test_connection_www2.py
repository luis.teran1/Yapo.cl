# -*- coding: iso-8859-1 -*-

#from selenium import webdriver
import unittest, time, re
import os

class test_connection(unittest.TestCase):

	def test_01_connection_www2(self):
		host = 'www2.yapo.cl'
		response = os.system("ping -q -c 1 " + host + " > /dev/null 2>&1")
		self.assertEqual(response, 0)

if __name__ == "__main__":
	unittest.main()
