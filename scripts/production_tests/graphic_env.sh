#!/bin/bash

XVNC="/usr/bin/Xvnc"
BLACKBOX="/usr/bin/blackbox"
ID=`id -u`
IAM=`whoami`
LOG="/tmp/selenium_production_site_"$IAM".log"

case "$1" in

	start)
		export DISPLAY=:$ID
		$XVNC -ac -SecurityTypes None :$ID >> $LOG 2> /dev/null &
		$BLACKBOX -display :$ID > $LOG 2>&1 > $LOG &
		;;
	stop)
		pkill -U $ID Xvnc || true;
		;;
esac

