#!/bin/bash

ID=`id -u`
IAM=`whoami`
JAVA="/usr/bin/java"
THE_PATH="/home/"$IAM"/selenium"
TEST_PATH=$THE_PATH"/tests"
SELENIUM_SERVER=$THE_PATH"/selenium-server-standalone-2.20.0.jar"

RESULT_LOG=$TEST_PATH"/result_html.log";

export DISPLAY=:$ID

$JAVA -jar $SELENIUM_SERVER -ensureCleanSession -htmlSuite "*firefox" "http://www2.yapo.cl/" $TEST_PATH"/main.html" $TEST_PATH"/result.html"

grep "\#testresult" $TEST_PATH"/result.html" | \
	awk -F\" '{print $2" "$5}' | \
	sed "s/<\/a.*//" | \
	sed "s/ >/:/" | \
	sed "s/  //" | \
	awk -F : '{print $2":"$1}' | \
	sed "s/status_passed/OK/" | \
	sed "s/status_failed/ERROR/" > $RESULT_LOG
