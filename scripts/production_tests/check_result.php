#!/usr/bin/php
<?php

$log_files = array();

if ($argc > 1) {
        for ($i = 1; $i < count($argv); $i++) {
                if (!is_file($argv[$i])) {
                        echo "\"$argv[$i]\": No such file\n";
                        exit(1);
                }
                $log_files[] = $argv[$i];
        }
}
else {
	echo "You must specify a log file\n";
	exit(0);
}


$TEST_OK = 0;
$TEST_FAIL = 0;
$STATUS = "OK";

$all_tests = "";

$bgcolor=array("OK" => "D4FFD4",
	"ERROR" => "FFD4D4");


foreach ($log_files as $log) {
	$lines = file($log);

	foreach ($lines as $line) {
		$line = trim($line);
		list($test, $status) = explode(":", $line);

		$all_test[]=array("name" => $test, "status" => $status);

		if ($status == "OK") {
			$TEST_OK++;
		}
		else {
			$TEST_FAIL++;
		}
	}
}

if ($TEST_FAIL > 0) {
	$STATUS="ERROR";
}

$TOTAL_TEST = $TEST_OK + $TEST_FAIL;

$message = "<html><body>"
."<h1>Test result</h1>"
."<table>"
."<tr><td>Total tests</td><td>: $TOTAL_TEST</td></tr>"
."<tr><td>Tests ok</td><td>: $TEST_OK</td></tr>"
."<tr><td>Tests failed</td><td>: $TEST_FAIL</td></tr>\n"
."</table>"
."<h2>Test execution detail</h2>"

."<table>"
."<tr>"
."<th>Test Name</th>"
."<th>Status</th>"
."</tr>";

foreach($all_test as $test) {

	$message .= "<tr style=\"background: #".$bgcolor[$test["status"]]."\">"
		."<td>$test[name]</td>"
		."<td>$test[status]</td>"
		."</tr>";

	syslog(LOG_INFO, "test result: $test[name], $test[status]");
}

syslog(LOG_INFO, "Total test: $TOTAL_TEST - Test ok: $TEST_OK - Test fail: $TEST_FAIL");

$message .= "</table>"
."</body></html>";

$now = getdate();
$today = date("Y-m-d h");

/*
Only send email when the test fail or 1 time at 10 hours.
*/
if ($STATUS == "ERROR" || $now["hours"] == "10" ) {
	mail("todos@schibsted.cl", "$STATUS: Selenium production test $today", $message, "MIME-Version: 1.0".PHP_EOL
	."Content-Type: text/html; charset=ISO-8859-1".PHP_EOL
	."Content-Transfer-Encoding: 7bit".PHP_EOL
	."Content-Disposition: inline".PHP_EOL
	."From: kaneda@schibsted.cl");
}

