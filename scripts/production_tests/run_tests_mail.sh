#!/bin/bash

IAM=`whoami`
ID=`id -u`
THE_PATH="/home/"$IAM"/selenium"
TEST_PATH=$THE_PATH"/tests"

PYTHON="/usr/bin/python26"

GLOBAL_LOG=$THE_PATH"/log.log"
RESULT_LOG=$TEST_PATH"/result_python_mail.log";


for s in `ls -1 $TEST_PATH/mail_*.py`; do
	result=`$PYTHON $s`
	echo "" >> $GLOBAL_LOG
	date >> $GLOBAL_LOG
	echo "$s" >> $GLOBAL_LOG
	echo $result >> $GLOBAL_LOG

	echo $result >> $RESULT_LOG
done
