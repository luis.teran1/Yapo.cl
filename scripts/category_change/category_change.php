<?php
/*
 * Should read ad_id from a file
 * Should then execute a transaction to get the ad data for each ad_id
 * Should parse this data and prepare them like trans params
 * Send an newad trans to edit the ad adding spaces in the body in order not to be send to autoreview queue
 * Execute (if executed without the -c argument then it only validates):
 * php -c ${path_to_blocket_build}/conf/php.ini category_change.php <-i filename> <-h trans_host> <-p trans_port> <-v verbose> <-c send_commits> <-r remote_addr> <-l limit>
 */

$config = parse_ini_file('./cc_config.ini', true);
require_once 'bImage.php';
require_once 'bTransaction.php';

/* cmd options */
$shortopts = 'i:h:p:vcl:a:r:'; 
$options = getopt($shortopts);


if (isset($options['i'])) {
	$filename = $options['i'];
} else {
	echo "No filename provided\n";
	echo "Usage: php -c \${path_to_blocket_build}/conf/php.ini category_change.php <-i filename> <-h trans_host> <-p trans_port> <-v verbose> <-c send_commits> <-r remote_addr> <-l limit>\n";
	exit();
}

if (isset($options['h']))
	$TRANS_HOST = $options['h'];
else {
	$TRANS_HOST = $config['trans_ip'];
	if (!$TRANS_HOST) {
		$TRANS_HOST = 'localhost';
	}
}

if (isset($options['p'])) {
	$TRANS_PORT = $options['p'];
} else {
	$TRANS_PORT = $config['trans_port'];
	if (!$TRANS_PORT) {
		print "Please specify trans port: -p PORT\n";
		die;
	}
}

if (isset($options['r']))
	$remote_addr = $options['r'];
else {
	$remote_addr = $config['remote_addr'];
	if (!$remote_addr) {
		print "Please specify remote address (the same one used on the filter for the category change queue: -r remote_adderss\n";
		die;
	}
}

$verbose = FALSE;
if (isset($options['v'])) 
	$verbose = TRUE;

$commit = FALSE;
if (isset($options['c'])) 
	$commit = TRUE;

$limit = FALSE;
if (isset($options['l']))
	$limit = $options['l'];

$exclude_params = array();
if (isset($config['exclude_params'])) 
	$exclude_params = explode(',', $config['exclude_params']);

$multiple = array();
if (isset($config['multiple'])) 
	$multiple = explode(',', $config['multiple']);

$condition = array(5080,5100,5120,5140,5160);



/* I know it is ugly I will fix when I can, I promise */
$types = array('sell' => 's', 'buy' => 'k', 'rent' => 'h', 'let' => 'u');

/*
 * Here we should parse the file with the id's
 */
$ads = split("\n", file_get_contents($filename));

$no_ads = 0;
while ($ad_id = @$ads[$no_ads]) {
	$no_ads++;

	print("<<in: $ad_id\n");

	$transaction = new bTransaction();
	$transaction->set_connect_info ($TRANS_HOST, $TRANS_PORT);
	$transaction->add_data('ad_id', $ad_id);
	$reply = $transaction->send_command('get_ad_info');

	$string_result = @implode($reply);
	if (strpos($string_result, 'TRANS_ERROR') || (strpos($string_result, 'TRANS_DATABASE_ERROR'))) {
		echo "get_ad_info failed for ad $ad_id" . PHP_EOL;
		if ($verbose)
			print_r($reply);
		continue;

	}
	unset($transaction);
	
	/* We should also check if the ad is currently active */
	if (@$reply['status'][0] != "active") {
		print ">>status: {$reply['status'][0]}" . PHP_EOL;
		continue;
	}

	if (isset($ad))
		unset($ad);

	foreach ($reply as $key => $val) {
		if (empty($val) || in_array($key, $exclude_params)) {
			continue;
		} else if (in_array($key, $multiple)) { 
			/* some params like status or uid might appear more than once
			 * so we have to exclude them */
			if (is_array($val)) {
				foreach ($val as $k => $v) {
					$data = explode(':', $v);
					if (!empty($data[1]))
						$ad[$data[0]] = $data[1];
				}
			} else {
				$data = explode(':', $val);
				if (!empty($data[1]))
					$ad[$data[0]] = $data[1];
			}
			continue;
		}
		$ad[$key] = $val;
	}

	// ugly hack 
	$c = array(" ", "+", "/", "-");
	$ad["phone"] = str_replace($c, "", $ad["phone"]);

	if (strlen($ad["phone"]) < 6) {
		while (strlen($ad["phone"]) < 6) {
			$ad["phone"] = "0".$ad["phone"];
		}
	}

	if (isset($images)) {
		unset($images);
		unset($digest);
		unset($thumbnail_digest);
	}

	if (isset($reply['ad_image'])) {
		
		if ($verbose)
			echo "Adding image data to trans\n"; 
		$i = 0;
		while (isset($reply['ad_image'][$i])) {
			$index = $i;
			$name = explode(':', $reply['ad_image'][$i++]);
			$digest = explode(':', $reply['ad_image'][$i++]);
			$images[] = $name[1];
			$thumbnail_digest[] = $digest[1];
			$digest_present[] = 0;
		}

		if ($verbose) {
			echo "Finished parsing image data\n";
			print_r($images);
			print_r($digest);
		}
	}

	unset($reply);

	/* Ugly as fxxk but functional at the moment */
	$ad['type'] = $types[$ad['type']];
	$ad['do_not_send_mail'] = '1';
	$ad['remote_addr'] = $remote_addr;
	$ad['remote_browser'] = 'vi';
	$ad['pay_type'] = 'free';

	if ($ad['category'] < 2000) {
		if (isset($ad['price']))
			$ad['price'] /= 100;
		if (isset($ad['old_price']))
			$ad['old_price'] /= 100;
	} else {
		if (isset($ad['communes'])) {
			unset($ad['communes']);
		}
		if (isset($ad['rooms'])) {
			unset($ad['rooms']);
		}
		if (isset($ad['address'])) {
			unset($ad['address']);
		}
		if (isset($ad['size'])) {
			unset($ad['size']);
		}
	}

	/* remove words like vendo, compro, arriendo */
	$words = array("se vende", "vendo", "compro", "arriendo", "ofertas", "oferta");
	$ad["subject"] = str_ireplace($words, "", $ad["subject"]);

	if ($verbose) {
		echo "Finished parsing data\n";
		print_r($ad);
	}

	/* Now we can send the edit to trans */
	$transaction = new bTransaction();
	$transaction->set_connect_info ($TRANS_HOST, $TRANS_PORT);
	
	foreach($ad as $k => $v) {
		/* Paranoia: in case a param has been inserted into $ad by mistake */
		if (!in_array($k, $exclude_params))
			$transaction->add_data($k, $v);
	}

	/* Add image data if present */
	if (isset($images)) {
		$transaction->add_data('image', $images);
		$transaction->add_data("thumbnail_digest", $thumbnail_digest);
		$transaction->add_data("digest_present", $digest_present);
		if ($i > 2) 
			$transaction->add_data("image_set", 0);
	}

	$transaction->add_data('ad_id', $ad_id);
	$transaction->add_data('ad_type', 'edit');

	if (in_array($ad['category'], $condition) && !isset($ad['condition'])) {
		if (isset($matches))
			unset($matches);
		$ad_text = $ad['subject'] . " " . $ad['body'];
		preg_match('/\b(nuevo|nuevos|nueva|nuevas)/', $ad_text, $matches);
		if (isset($matches) && !empty($matches)) {
			$transaction->add_data("condition", "1"); // nuevo
		} else {
			$transaction->add_data("condition", "2"); // condition
		}
	}

	if ($commit)
		$reply = $transaction->send_command('newad');
	else
		$reply = $transaction->validate_command('newad');

	if ( $reply['status'] != "TRANS_OK") {
		print "ERROR NEWAD: $ad_id\n";
	}

	/* Now is the time to clear */
	if ($commit && $reply['status'] == "TRANS_OK") {
		unset($transaction);
		$transaction = New bTransaction();
		$transaction->set_connect_info ($TRANS_HOST, $TRANS_PORT);
		$transaction->add_data('remote_addr', $remote_addr);
		$transaction->add_data('remote_browser', 'vi');
		$transaction->add_data('ad_id', $reply['ad_id']);
		$transaction->add_data('action_id', $reply['action_id']);
		$clear_reply = $transaction->send_command('clear');
		if ($clear_reply['status'] != 'TRANS_OK') {
			echo "Exit clear ad failed for ad {$reply['ad_id']}\n";

			if ($verbose)
				print_r($clear_reply);
		}
		else {
			echo ">>out:$ad_id\n";
		}
	}
	
	if ($verbose) {
		echo "Newad reply\n";
		print_r($reply);
		echo "Clearad reply\n";
		print_r(@$clear_reply);
	}
	unset($ad_id);

	if ($limit && $no_ads == $limit)
		exit();		
}

?>
