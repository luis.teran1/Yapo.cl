#!/usr/bin/perl -w

use DBI;
use Getopt::Long;
use Data::Dumper;
use blocket::bconf;
use perl_trans;
use Time::Local;
use POSIX qw(strftime);

my $input_file ;
my $bconffile ;
my $debug = 0 ;

sub toUnixTime
{
	my ($y, $m, $d) = split("-", $_[0]);
	$m--;
	my $time = timelocal(0, 0, 0, $d, $m, $y);
	return $time;
}

# This script parses a given ad_id log file, corresponding to migrated ads which have
# changed its category due to a heuristic decision.
# Only send emails to not spidered ads with list_time less than 6 months old (180 days)

# Modify this to suit your needs, in order to run the script...
# export BASEDIR=/home/$USER/yapo.cl/trunk
# export PERL5LIB=$BASEDIR/lib/perl:$BASEDIR/modules/perl_bconf/lib:$BASEDIR/modules/perl_bconf/blib/arch

GetOptions("input_file=s" => \$input_file,
	"debug" => \$debug,
	"bconffile=s"  => \$bconffile);

$debug = 1 if $debug;
die "I need a bconf.conf file in order to know trans host location...\n" if (!$bconffile);
die "Cannot find " . $input_file . "\n" if (! -e $input_file);

my $bconf = blocket::bconf::init_conf($bconffile);
my $transhost = $$bconf{'*'}{'common'}{'transaction'}{'host'};

my $today = strftime "%Y-%m-%d", localtime;
my $diff_in_sec = 180 * 24 * 60 * 60;
my $threshold = toUnixTime($today) - $diff_in_sec;

# Parse input file, return list of ad_id's
open LOG_FILE, '<', $input_file;
my $successful_mails = 0;
while (<LOG_FILE>) {
	/^([0-9]+)$/ or next;

	my %transcmd;
	my $ad_id = $1;

	$transcmd{'cmd'} = 'get_ad_info';
	$transcmd{'commit'} = '1';
	$transcmd{'ad_id'} = $ad_id;

	my %resp = bconf_trans($transhost, %transcmd);
	my @status = @{$resp{'status'}};

	if ($status[0] ne 'active' || $status[1] ne 'TRANS_OK') {
		print Dumper(%resp) if $debug;
		next;
	}

	# Now check to see if ad is newer than 6 months and not spidered
	my $list_time = $resp{'list_time'};
	$list_time =~ s/ ..:..:..//;
	$unix_list_time = toUnixTime($list_time);
	if (($threshold - $unix_list_time) > 0) {
		print "Skipped email for ad (too old) $ad_id\n";
		next;
	}

	foreach (@{$resp{'ad_param'}}) {
		/link_type/ or next;
		print "Skipped email for ad (spidered) $ad_id\n";
		next;
	}

	print "Send category_changed_mail for ".$ad_id."\n" if $debug;
	my %trans_ad_cmd;
	$trans_ad_cmd{'cmd'} = "admail";
	$trans_ad_cmd{'commit'} = "1";
	$trans_ad_cmd{'ad_id'} = $ad_id;
	$trans_ad_cmd{'mail_type'} = "category_changed";

	my %ad_resp = bconf_trans($transhost, %trans_ad_cmd);
	my $ad_status = $ad_resp{'status'};

	if ($ad_status ne "TRANS_OK") {
		print Dumper(%ad_resp) if $debug;
		print "Failed to send mail to $ad_id, moving on...\n";
	} else {
		$successful_mails++;
		print "Mail for " . $ad_id . " sent successfully, moving on...\n" if $debug;
	}
}
if ($successful_mails > 0) {
	print "Sent $successful_mails correctly!\n";
} else {
	print "No emails sent\n";
}
