#!/usr/bin/perl

use strict;
use warnings;

my $res = "";
my $failed = 0;
chomp (my $testsuite = <>);

while (<>) {
	chomp (my $testname = $_);
	chomp (my $start_time = <>);
	chomp (my $status = <>);
	chomp (my $end_time = <>);
	my $time = ($end_time - $start_time) / 1000000000;
	$res .= "  <testcase classname='$testsuite' name='$testname' time='$time'>\n";
	$res .= "    <failure/>\n" if $status ne "OK";
	++$failed if $status ne "OK";
	$res .= "  </testcase>\n";
}

open FD, ">$testsuite.xml";

if ($res eq "") {
	$res =  "  <testcase classname='$testsuite' name='test_missing'>\n";
	$res .= "    <failure/>\n";
	$res .= "  </testcase>\n";
	$failed = 1;
}

print FD "<testsuite failures='$failed' errors='0'>\n";
print FD $res;
print FD "</testsuite>\n";

close FD;
