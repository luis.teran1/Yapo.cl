COPY(
	SELECT 
		ad_id, 
		subject, 
		category AS current_category, 
		'http://www.yapo.cl/vi/' || list_id::text || '.htm' AS url,
		'1240' as new_category,
		'6' as new_param
	FROM 
		ads 
	WHERE 
		category = 1120 
		AND status in ('active', 'deactivated', 'refused') 
		AND type = 'let'
) To '/tmp/cat_1120_u_1240_6_ads.csv' With CSV HEADER;

