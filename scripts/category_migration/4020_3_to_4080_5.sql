COPY(
	SELECT * FROM 
	(
		SELECT
			ads.ad_id,
			price,
			category AS current_category,
			type as current_type,
			'http://www.yapo.cl/vi/' || list_id::text || '.htm' AS url,
			'4080' as new_category,
			CASE when adp.value = '3' THEN '5' WHEN adp.value = '2' THEN '2' WHEN adp.value = '1' THEN '1' ELSE '0' END AS new_gender
		FROM
			ads
		LEFT JOIN ad_params adp on ads.ad_id = adp.ad_id and adp.name = 'gender'
		WHERE
			category = 4020
			AND status in ('active')
			AND (
				subject ~* '[[:<:]]Zapatos[[:>:]]' OR
				subject ~* '[[:<:]]zapato[[:>:]]' OR
				subject ~* '[[:<:]]zapatillas[[:>:]]' OR
				subject ~* '[[:<:]]botines[[:>:]]' OR
				subject ~* '[[:<:]]botas[[:>:]]' OR
				subject ~* '[[:<:]]chalas[[:>:]]' OR
				subject ~* '[[:<:]]sandalias[[:>:]]' OR
				subject ~* '[[:<:]]bototos[[:>:]]' OR
				subject ~* '[[:<:]]tacones[[:>:]]' OR
				subject ~* '[[:<:]]alpargatas[[:>:]]' OR
				subject ~* '[[:<:]]bototo[[:>:]]' OR
				subject ~* '[[:<:]]mocasin[[:>:]]' OR
				subject ~* '[[:<:]]sapatillas[[:>:]]' OR
				subject ~* '[[:<:]]hawaianas[[:>:]]' OR
				subject ~* '[[:<:]]ballerinas[[:>:]]' OR
				subject ~* '[[:<:]]pantuflas[[:>:]]' OR
				subject ~* '[[:<:]]sapatos[[:>:]]' OR
				subject ~* '[[:<:]]tillas[[:>:]]' OR
				subject ~* '[[:<:]]bailarinas[[:>:]]' OR
				subject ~* '[[:<:]]votas[[:>:]]' OR
				subject ~* '[[:<:]]gualetas[[:>:]]' OR
				subject ~* '[[:<:]]Bota[[:>:]]' OR
				subject ~* '[[:<:]]plantillas[[:>:]]' OR
				subject ~* '[[:<:]]lote zapatos[[:>:]]' OR
				subject ~* '[[:<:]]lote zapatillas[[:>:]]' OR
				body ~* '[[:<:]]Zapatos[[:>:]]' OR
				body ~* '[[:<:]]zapatillas[[:>:]]' OR
				body ~* '[[:<:]]botines[[:>:]]' OR
				body ~* '[[:<:]]botas[[:>:]]' OR
				body ~* '[[:<:]]chalas[[:>:]]' OR
				body ~* '[[:<:]]sandalias[[:>:]]' OR
				body ~* '[[:<:]]bototo[[:>:]]' OR
				body ~* '[[:<:]]mocasin[[:>:]]' OR
				body ~* '[[:<:]]sapatillas[[:>:]]' OR
				body ~* '[[:<:]]hawaianas[[:>:]]' OR
				body ~* '[[:<:]]ballerinas[[:>:]]' OR
				body ~* '[[:<:]]pantuflas[[:>:]]' OR
				body ~* '[[:<:]]sapatos[[:>:]]' OR
				body ~* '[[:<:]]tillas[[:>:]]' OR
				body ~* '[[:<:]]bailarinas[[:>:]]' OR
				body ~* '[[:<:]]votas[[:>:]]' OR
				body ~* '[[:<:]]gualetas[[:>:]]' OR
				body ~* '[[:<:]]Bota[[:>:]]' OR
				body ~* '[[:<:]]plantillas[[:>:]]' OR
				body ~* '[[:<:]]lote zapatos[[:>:]]' OR
				body ~* '[[:<:]]lote zapatillas[[:>:]]' 
		   )
			AND NOT (
				subject ~* '[[:<:]]Vestido[[:>:]]' OR
				subject ~* '[[:<:]]ropa[[:>:]]' OR
				subject ~* '[[:<:]]ropita[[:>:]]' OR
				subject ~* '[[:<:]]disfraz[[:>:]]' OR
				subject ~* '[[:<:]]terno[[:>:]]' OR
				subject ~* '[[:<:]]traje[[:>:]]' OR
				subject ~* '[[:<:]]uniforme[[:>:]]'
			)
		) as t
		WHERE new_gender = '5'
) To '/tmp/cat_4020_3_4080_5_ads.csv' With CSV HEADER;

