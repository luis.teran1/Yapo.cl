COPY(
	SELECT * FROM 
	(
		SELECT 
			ads.ad_id, 
			price,
			category AS current_category, 
			type as current_type, 
			'http://www.yapo.cl/vi/' || list_id::text || '.htm' AS url,
			'4080' as new_category,
			CASE when adp.value = '3' THEN '5' WHEN adp.value = '2' THEN '2' WHEN adp.value = '1' THEN '1' ELSE '0' END AS new_gender
		FROM 
			ads
		LEFT JOIN ad_params adp on ads.ad_id = adp.ad_id and adp.name = 'gender'
		WHERE 
			category = 4040 
			AND status in ('active') 
			AND (subject ~* '[[:<:]]zapatos[[:>:]]' AND body  ~* '[[:<:]]zapatos[[:>:]]')
	) as t 
	WHERE new_gender = '0'
) To '/tmp/cat_4040_0_4080_0_ads.csv' With CSV HEADER;
