COPY(
	SELECT 
		ad_id, 
		price,
		category AS current_category, 
		type as current_type, 
		'http://www.yapo.cl/vi/' || list_id::text || '.htm' AS url,
		'4080' as new_category
	FROM 
		ads
	WHERE 
		category >= 7000 AND category < 8000 
		AND status in ('active') 
		AND (subject ~* '[[:<:]]zapatos[[:>:]]' AND body  ~* '[[:<:]]zapatos[[:>:]]')
) To '/tmp/cat_7000_4080_ads.csv' With CSV HEADER;
