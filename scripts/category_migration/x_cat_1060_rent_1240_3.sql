COPY(
	SELECT 
		ad_id, 
		subject, 
		category AS current_category, 
		'http://www.yapo.cl/vi/' || list_id::text || '.htm' AS url,
		'1240' as new_category,
		'3' as new_param
	FROM 
		ads 
	WHERE 
		category = 1060 
		AND status in ('active', 'deactivated', 'refused') 
		AND type = 'rent'
) To '/tmp/x_cat_1060_rent_1240_3_ads.csv' With CSV HEADER;

