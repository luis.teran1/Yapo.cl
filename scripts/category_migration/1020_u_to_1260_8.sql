COPY(
	SELECT ad_id, subject, price, current_category, current_type, new_category, new_param FROM 
		(SELECT 
			ad_id, 
			subject, 
			body,
			price,
			category AS current_category, 
			type as current_type, 
			'http://www.yapo.cl/vi/' || list_id::text || '.htm' AS url,
			'1260' as new_category,
			'8' as new_param
		FROM 
			ads
		WHERE 
			category = 1020 
			AND type ='let' 
			AND status in ('active','deactivated', 'refused') 
			AND price >= 200000
			AND NOT (
				subject ~* '[[:<:]]desayuno[[:>:]]' OR
				subject ~* '[[:<:]]desalluno[[:>:]]' OR
				subject ~* '[[:<:]]desayunos[[:>:]]' OR
				subject ~* '[[:<:]]desallunos[[:>:]]' OR
				subject ~* '[[:<:]]sabanas[[:>:]]' OR
				subject ~* '[[:<:]]s�banas[[:>:]]' OR
				subject ~* '[[:<:]]savanas[[:>:]]' OR
				subject ~* '[[:<:]]sabana[[:>:]]' OR
				subject ~* '[[:<:]]savana[[:>:]]' OR
				subject ~* '[[:<:]]toalla[[:>:]]' OR
				subject ~* '[[:<:]]toallas[[:>:]]' OR
				subject ~* '[[:<:]]toaya[[:>:]]' OR
				subject ~* '[[:<:]]olla[[:>:]]' OR
				subject ~* '[[:<:]]internet[[:>:]]' OR
				subject ~* '[[:<:]]privado[[:>:]]' OR
				subject ~* '[[:<:]]estudiantes[[:>:]]' OR
				subject ~* '[[:<:]]compartido[[:>:]]' OR
				subject ~* '[[:<:]]ambiente[[:>:]]' OR
				subject ~* '[[:<:]]residencial[[:>:]]' OR
				subject ~* '[[:<:]]estudiante[[:>:]]' OR
				subject ~* '[[:<:]]cajonera[[:>:]]' OR
				subject ~* '[[:<:]]estudie[[:>:]]' OR
				subject ~* '[[:<:]]arriendo pieza[[:>:]]' OR
				subject ~* '[[:<:]]pension[[:>:]]' OR
				subject ~* '[[:<:]]pensi�n[[:>:]]' OR
				subject ~* '[[:<:]]penci�n[[:>:]]' OR
				subject ~* '[[:<:]]pencion[[:>:]]' OR
				subject ~* '[[:<:]]hospedaje[[:>:]]' OR
				subject ~* '[[:<:]]hombre[[:>:]]' OR
				body ~* '[[:<:]]desayuno[[:>:]]' OR
				body ~* '[[:<:]]desalluno[[:>:]]' OR
				body ~* '[[:<:]]desayunos[[:>:]]' OR
				body ~* '[[:<:]]desallunos[[:>:]]' OR
				body ~* '[[:<:]]sabanas[[:>:]]' OR
				body ~* '[[:<:]]s�banas[[:>:]]' OR
				body ~* '[[:<:]]savanas[[:>:]]' OR
				body ~* '[[:<:]]sabana[[:>:]]' OR
				body ~* '[[:<:]]savana[[:>:]]' OR
				body ~* '[[:<:]]toalla[[:>:]]' OR
				body ~* '[[:<:]]toallas[[:>:]]' OR
				body ~* '[[:<:]]toaya[[:>:]]' OR
				body ~* '[[:<:]]olla[[:>:]]' OR
				body ~* '[[:<:]]internet[[:>:]]' OR
				body ~* '[[:<:]]privado[[:>:]]' OR
				body ~* '[[:<:]]estudiantes[[:>:]]' OR
				body ~* '[[:<:]]compartido[[:>:]]' OR
				body ~* '[[:<:]]ambiente[[:>:]]' OR
				body ~* '[[:<:]]residencial[[:>:]]' OR
				body ~* '[[:<:]]estudiante[[:>:]]' OR
				body ~* '[[:<:]]cajonera[[:>:]]' OR
				body ~* '[[:<:]]estudie[[:>:]]' OR
				body ~* '[[:<:]]arriendo pieza[[:>:]]' OR
				body ~* '[[:<:]]pension[[:>:]]' OR
				body ~* '[[:<:]]pensi�n[[:>:]]' OR
				body ~* '[[:<:]]penci�n[[:>:]]' OR
				body ~* '[[:<:]]pencion[[:>:]]' OR
				body ~* '[[:<:]]hospedaje[[:>:]]' OR
				body ~* '[[:<:]]hombre[[:>:]]'
			)
		) as piezas_hab
		WHERE 
			price < 400000 
			AND (
				subject ~* '[[:<:]]estad�a[[:>:]]' OR
				subject ~* '[[:<:]]estadia[[:>:]]' OR
				subject ~* '[[:<:]]mucama[[:>:]]' OR
				subject ~* '[[:<:]]wifi[[:>:]]' OR
				subject ~* '[[:<:]]temporada[[:>:]]' OR
				subject ~* '[[:<:]]confort[[:>:]]' OR
				subject ~* '[[:<:]]diario[[:>:]]' OR
				subject ~* '[[:<:]]verano[[:>:]]' OR
				subject ~* '[[:<:]]vacaciones[[:>:]]' OR
				subject ~* '[[:<:]]vacacional[[:>:]]' OR
				subject ~* '[[:<:]]d�a[[:>:]]' OR
				subject ~* '[[:<:]]diarios[[:>:]]' OR
				subject ~* '[[:<:]]playa[[:>:]]' OR
				subject ~* '[[:<:]]cable[[:>:]]' OR
				subject ~* '[[:<:]]camarote[[:>:]]' OR
				subject ~* '[[:<:]]sabanas[[:>:]]' OR
				subject ~* '[[:<:]]traer[[:>:]]' OR
				subject ~* '[[:<:]]resortes[[:>:]]' OR
				subject ~* '[[:<:]]colchon[[:>:]]' OR
				subject ~* '[[:<:]]colch�n[[:>:]]' OR
				subject ~* '[[:<:]]colchones[[:>:]]' OR
				subject ~* '[[:<:]]cubrecamas[[:>:]]' OR
				subject ~* '[[:<:]]cubrecama[[:>:]]' OR
				subject ~* '[[:<:]]wiffi[[:>:]]' OR
				subject ~* '[[:<:]]feriados[[:>:]]' OR
				subject ~* '[[:<:]]mucama[[:>:]]' OR
				subject ~* '[[:<:]]personas[[:>:]]' OR
				subject ~* '[[:<:]]persona[[:>:]]' OR
				subject ~* '[[:<:]]direct tv[[:>:]]' OR
				subject ~* '[[:<:]]turismo[[:>:]]' OR
				body ~* '[[:<:]]estad�a[[:>:]]' OR
				body ~* '[[:<:]]estadia[[:>:]]' OR
				body ~* '[[:<:]]mucama[[:>:]]' OR
				body ~* '[[:<:]]wifi[[:>:]]' OR
				body ~* '[[:<:]]temporada[[:>:]]' OR
				body ~* '[[:<:]]confort[[:>:]]' OR
				body ~* '[[:<:]]diario[[:>:]]' OR
				body ~* '[[:<:]]verano[[:>:]]' OR
				body ~* '[[:<:]]vacaciones[[:>:]]' OR
				body ~* '[[:<:]]vacacional[[:>:]]' OR
				body ~* '[[:<:]]d�a[[:>:]]' OR
				body ~* '[[:<:]]diarios[[:>:]]' OR
				body ~* '[[:<:]]playa[[:>:]]' OR
				body ~* '[[:<:]]cable[[:>:]]' OR
				body ~* '[[:<:]]camarote[[:>:]]' OR
				body ~* '[[:<:]]sabanas[[:>:]]' OR
				body ~* '[[:<:]]traer[[:>:]]' OR
				body ~* '[[:<:]]resortes[[:>:]]' OR
				body ~* '[[:<:]]colchon[[:>:]]' OR
				body ~* '[[:<:]]colch�n[[:>:]]' OR
				body ~* '[[:<:]]colchones[[:>:]]' OR
				body ~* '[[:<:]]cubrecamas[[:>:]]' OR
				body ~* '[[:<:]]cubrecama[[:>:]]' OR
				body ~* '[[:<:]]wiffi[[:>:]]' OR
				body ~* '[[:<:]]feriados[[:>:]]' OR
				body ~* '[[:<:]]mucama[[:>:]]' OR
				body ~* '[[:<:]]personas[[:>:]]' OR
				body ~* '[[:<:]]persona[[:>:]]' OR
				body ~* '[[:<:]]direct tv[[:>:]]' OR
				body ~* '[[:<:]]turismo[[:>:]]'
			)
			AND (
				subject ~* '[[:<:]]cabana[[:>:]]' OR
				subject ~* '[[:<:]]caba�a[[:>:]]' OR
				body ~* '[[:<:]]cabana[[:>:]]' OR
				body ~* '[[:<:]]caba�a[[:>:]]'
			)
) To '/tmp/cat_1020_u_1260_8_ads.csv' With CSV HEADER;
