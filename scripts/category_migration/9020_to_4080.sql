COPY(
	SELECT 
		ad_id, 
		price,
		category AS current_category, 
		type as current_type, 
		'http://www.yapo.cl/vi/' || list_id::text || '.htm' AS url,
		'4080' as new_category
	FROM 
		ads
	WHERE 
		category = 9020 
		AND status in ('active') 
		AND (
			(
				subject ~* '[[:<:]]Zapato[[:>:]]' OR
				subject ~* '[[:<:]]Zapatos[[:>:]]' OR
				subject ~* '[[:<:]]Zapatillas[[:>:]]' OR
				subject ~* '[[:<:]]Calzado[[:>:]]' OR
				subject ~* '[[:<:]]Pantuflas[[:>:]]'
			)
			OR (
				(subject ~* '[[:<:]]Botas[[:>:]]' AND body  ~* '[[:<:]]Botas[[:>:]]')
				OR
				(subject ~* '[[:<:]]Chalas[[:>:]]' AND body  ~* '[[:<:]]Chalas[[:>:]]')
				OR
				(subject ~* '[[:<:]]Botin[[:>:]]' AND body  ~* '[[:<:]]Botin[[:>:]]')
				OR
				(subject ~* '[[:<:]]botines[[:>:]]' AND body  ~* '[[:<:]]botines[[:>:]]')
			)
		)
) To '/tmp/cat_9020_4080_ads.csv' With CSV HEADER;
