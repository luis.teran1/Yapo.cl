COPY(
	SELECT 
		ad_id, 
		subject, 
		category AS current_category, 
		'http://www.yapo.cl/vi/' || list_id::text || '.htm' AS url,
		'1240' as new_category,
		'1' as new_param
	FROM 
		ads 
	WHERE 
		category = 1020 
		AND status in ('active', 'deactivated', 'refused') 
		AND type = 'rent'
) To '/tmp/x_cat_1020_rent_1240_1_ads.csv' With CSV HEADER;

