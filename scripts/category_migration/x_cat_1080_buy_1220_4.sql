COPY(
	SELECT 
		ad_id, 
		subject, 
		category AS current_category, 
		'http://www.yapo.cl/vi/' || list_id::text || '.htm' AS url,
		'1220' as new_category,
		'4' as new_param
	FROM 
		ads 
	WHERE 
		category = 1080 
		AND status in ('active', 'deactivated', 'refused') 
		AND type = 'buy'
) To '/tmp/x_cat_1080_buy_1220_4_ads.csv' With CSV HEADER;

