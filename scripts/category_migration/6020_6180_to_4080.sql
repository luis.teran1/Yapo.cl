COPY(
	SELECT 
		ad_id, 
		price,
		category AS current_category, 
		type as current_type, 
		'http://www.yapo.cl/vi/' || list_id::text || '.htm' AS url,
		'4080' as new_category
	FROM 
		ads
	WHERE 
		category in (6020, 6180) 
		AND status in ('active') 
		AND (
			(
				subject ~* '[[:<:]]Zapatos[[:>:]]' OR
				subject ~* '[[:<:]]Zapatillas[[:>:]]' OR
				body ~* '[[:<:]]zapatillas[[:>:]]' OR
				body ~* '[[:<:]]zapatos[[:>:]]'
			)
			OR (
				(subject ~* '[[:<:]]Botas[[:>:]]' AND body  ~* '[[:<:]]Botas[[:>:]]')
				OR
				(subject ~* '[[:<:]]Botines[[:>:]]' AND body  ~* '[[:<:]]Botines[[:>:]]')
			)
		)
) To '/tmp/cat_6020_and_6180_4080_ads.csv' With CSV HEADER;
