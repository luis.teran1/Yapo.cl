COPY(
	SELECT 
		ad_id, 
		subject, 
		category AS current_category, 
		'http://www.yapo.cl/vi/' || list_id::text || '.htm' AS url,
		'1220' as new_category,
		'1' as new_param
	FROM 
		ads 
	WHERE 
		category = 1020 
		AND status in ('active', 'deactivated', 'refused') 
		AND type = 'sell'
) To '/tmp/cat_1020_s_1220_1_ads.csv' With CSV HEADER;

