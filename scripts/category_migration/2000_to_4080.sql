COPY(
	SELECT 
		ad_id, 
		price,
		category AS current_category, 
		type as current_type, 
		'http://www.yapo.cl/vi/' || list_id::text || '.htm' AS url,
		'4080' as new_category
	FROM 
		ads
	WHERE 
		category >= 2000 AND category < 3000 
		AND status in ('active') 
		AND 'pack_status' not in (select name from ad_params where ad_id = ads.ad_id)
		AND (
			(subject ~* '[[:<:]]Botas[[:>:]]' AND body  ~* '[[:<:]]Botas[[:>:]]')
			OR
			(subject ~* '[[:<:]]Zapatos[[:>:]]' AND body  ~* '[[:<:]]Zapatos[[:>:]]')
		)
) To '/tmp/cat_2000_4080_ads.csv' With CSV HEADER;
