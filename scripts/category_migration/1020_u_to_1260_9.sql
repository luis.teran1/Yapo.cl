COPY(
	SELECT ad_id, subject, price, current_category, current_type, new_category, new_param FROM 
		(SELECT 
			ad_id, 
			subject, 
			body,
			price,
			category AS current_category, 
			type as current_type, 
			'http://www.yapo.cl/vi/' || list_id::text || '.htm' AS url,
			'1260' as new_category, 
			'9' as new_param 
		FROM 
			ads
		WHERE 
			category = 1020 
			AND type ='let' 
			AND status in ('active','deactivated', 'refused') 
			AND (price < 200000 OR price IS NULL)
			AND (
				subject ~* '[[:<:]]desayuno[[:>:]]' OR
				subject ~* '[[:<:]]desalluno[[:>:]]' OR
				subject ~* '[[:<:]]desayunos[[:>:]]' OR
				subject ~* '[[:<:]]desallunos[[:>:]]' OR
				subject ~* '[[:<:]]sabanas[[:>:]]' OR
				subject ~* '[[:<:]]s�banas[[:>:]]' OR
				subject ~* '[[:<:]]savanas[[:>:]]' OR
				subject ~* '[[:<:]]sabana[[:>:]]' OR
				subject ~* '[[:<:]]savana[[:>:]]' OR
				subject ~* '[[:<:]]toalla[[:>:]]' OR
				subject ~* '[[:<:]]toallas[[:>:]]' OR
				subject ~* '[[:<:]]toaya[[:>:]]' OR
				subject ~* '[[:<:]]olla[[:>:]]' OR
				subject ~* '[[:<:]]internet[[:>:]]' OR
				subject ~* '[[:<:]]privado[[:>:]]' OR
				subject ~* '[[:<:]]estudiantes[[:>:]]' OR
				subject ~* '[[:<:]]compartido[[:>:]]' OR
				subject ~* '[[:<:]]ambiente[[:>:]]' OR
				subject ~* '[[:<:]]residencial[[:>:]]' OR
				subject ~* '[[:<:]]estudiante[[:>:]]' OR
				subject ~* '[[:<:]]cajonera[[:>:]]' OR
				subject ~* '[[:<:]]estudie[[:>:]]' OR
				subject ~* '[[:<:]]arriendo pieza[[:>:]]' OR
				subject ~* '[[:<:]]pension[[:>:]]' OR
				subject ~* '[[:<:]]pensi�n[[:>:]]' OR
				subject ~* '[[:<:]]penci�n[[:>:]]' OR
				subject ~* '[[:<:]]pencion[[:>:]]' OR
				subject ~* '[[:<:]]hospedaje[[:>:]]' OR
				subject ~* '[[:<:]]hombre[[:>:]]' OR
				body  ~* '[[:<:]]desayuno[[:>:]]' OR
				body  ~* '[[:<:]]desalluno[[:>:]]' OR
				body  ~* '[[:<:]]desayunos[[:>:]]' OR
				body  ~* '[[:<:]]desallunos[[:>:]]' OR
				body  ~* '[[:<:]]sabanas[[:>:]]' OR
				body  ~* '[[:<:]]s�banas[[:>:]]' OR
				body  ~* '[[:<:]]savanas[[:>:]]' OR
				body  ~* '[[:<:]]sabana[[:>:]]' OR
				body  ~* '[[:<:]]savana[[:>:]]' OR
				body  ~* '[[:<:]]toalla[[:>:]]' OR
				body  ~* '[[:<:]]toallas[[:>:]]' OR
				body  ~* '[[:<:]]toaya[[:>:]]' OR
				body  ~* '[[:<:]]olla[[:>:]]' OR
				body  ~* '[[:<:]]internet[[:>:]]' OR
				body  ~* '[[:<:]]privado[[:>:]]' OR
				body  ~* '[[:<:]]estudiantes[[:>:]]' OR
				body  ~* '[[:<:]]compartido[[:>:]]' OR
				body  ~* '[[:<:]]ambiente[[:>:]]' OR
				body  ~* '[[:<:]]residencial[[:>:]]' OR
				body  ~* '[[:<:]]estudiante[[:>:]]' OR
				body  ~* '[[:<:]]cajonera[[:>:]]' OR
				body  ~* '[[:<:]]estudie[[:>:]]' OR
				body  ~* '[[:<:]]arriendo pieza[[:>:]]' OR
				body  ~* '[[:<:]]pension[[:>:]]' OR
				body  ~* '[[:<:]]pensi�n[[:>:]]' OR
				body  ~* '[[:<:]]penci�n[[:>:]]' OR
				body  ~* '[[:<:]]pencion[[:>:]]' OR
				body  ~* '[[:<:]]hospedaje[[:>:]]' OR
				body  ~* '[[:<:]]hombre[[:>:]]'
			)
		) as piezas_hab
		WHERE 
			(price < 150000 OR price IS NULL)
			AND (
				subject ~* '[[:<:]]estad�a[[:>:]]' OR
				subject ~* '[[:<:]]estadia[[:>:]]' OR
				subject ~* '[[:<:]]mucama[[:>:]]' OR
				subject ~* '[[:<:]]temporada[[:>:]]' OR
				subject ~* '[[:<:]]confort[[:>:]]' OR
				subject ~* '[[:<:]]diario[[:>:]]' OR
				subject ~* '[[:<:]]verano[[:>:]]' OR
				subject ~* '[[:<:]]vacaciones[[:>:]]' OR
				subject ~* '[[:<:]]vacacional[[:>:]]' OR
				subject ~* '[[:<:]]d�a[[:>:]]' OR
				subject ~* '[[:<:]]diarios[[:>:]]' OR
				subject ~* '[[:<:]]playa[[:>:]]' OR
				subject ~* '[[:<:]]camarote[[:>:]]' OR
				subject ~* '[[:<:]]traer[[:>:]]' OR
				subject ~* '[[:<:]]resortes[[:>:]]' OR
				subject ~* '[[:<:]]feriados[[:>:]]' OR
				subject ~* '[[:<:]]mucama[[:>:]]' OR
				body ~* '[[:<:]]estad�a[[:>:]]' OR
				body ~* '[[:<:]]estadia[[:>:]]' OR
				body ~* '[[:<:]]mucama[[:>:]]' OR
				body ~* '[[:<:]]temporada[[:>:]]' OR
				body ~* '[[:<:]]confort[[:>:]]' OR
				body ~* '[[:<:]]diario[[:>:]]' OR
				body ~* '[[:<:]]verano[[:>:]]' OR
				body ~* '[[:<:]]vacaciones[[:>:]]' OR
				body ~* '[[:<:]]vacacional[[:>:]]' OR
				body ~* '[[:<:]]d�a[[:>:]]' OR
				body ~* '[[:<:]]diarios[[:>:]]' OR
				body ~* '[[:<:]]playa[[:>:]]' OR
				body ~* '[[:<:]]camarote[[:>:]]' OR
				body ~* '[[:<:]]traer[[:>:]]' OR
				body ~* '[[:<:]]resortes[[:>:]]' OR
				body ~* '[[:<:]]feriados[[:>:]]' OR
				body ~* '[[:<:]]mucama[[:>:]]'
		)
) To '/tmp/cat_1020_u_1260_9_ads.csv' With CSV HEADER;
