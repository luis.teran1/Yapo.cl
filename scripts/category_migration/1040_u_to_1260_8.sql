COPY(
	SELECT 
		ad_id, 
		subject, 
		price,
		category AS current_category, 
		type as current_type, 
		'http://www.yapo.cl/vi/' || list_id::text || '.htm' AS url,
		'1240' as new_category,
		'1' as new_param
	FROM 
		ads
	WHERE 
		category = 1040 
		AND type ='let' 
		AND status in ('active','deactivated', 'refused') 
		AND (price < 400000 OR price IS NULL)
		AND (
			subject ~* '[[:<:]]estad�a[[:>:]]' OR
			subject ~* '[[:<:]]estadia[[:>:]]' OR
			subject ~* '[[:<:]]mucama[[:>:]]' OR
			subject ~* '[[:<:]]wifi[[:>:]]' OR
			subject ~* '[[:<:]]temporada[[:>:]]' OR
			subject ~* '[[:<:]]confort[[:>:]]' OR
			subject ~* '[[:<:]]diario[[:>:]]' OR
			subject ~* '[[:<:]]verano[[:>:]]' OR
			subject ~* '[[:<:]]vacaciones[[:>:]]' OR
			subject ~* '[[:<:]]vacacional[[:>:]]' OR
			subject ~* '[[:<:]]d�a[[:>:]]' OR
			subject ~* '[[:<:]]diarios[[:>:]]' OR
			subject ~* '[[:<:]]playa[[:>:]]' OR
			subject ~* '[[:<:]]cable[[:>:]]' OR
			subject ~* '[[:<:]]camarote[[:>:]]' OR
			subject ~* '[[:<:]]sabanas[[:>:]]' OR
			subject ~* '[[:<:]]traer[[:>:]]' OR
			subject ~* '[[:<:]]resortes[[:>:]]' OR
			subject ~* '[[:<:]]colchon[[:>:]]' OR
			subject ~* '[[:<:]]colch�n[[:>:]]' OR
			subject ~* '[[:<:]]colchones[[:>:]]' OR
			subject ~* '[[:<:]]cubrecamas[[:>:]]' OR
			subject ~* '[[:<:]]cubrecama[[:>:]]' OR
			subject ~* '[[:<:]]wiffi[[:>:]]' OR
			subject ~* '[[:<:]]feriados[[:>:]]' OR
			subject ~* '[[:<:]]mucama[[:>:]]' OR
			subject ~* '[[:<:]]personas[[:>:]]' OR
			subject ~* '[[:<:]]persona[[:>:]]' OR
			subject ~* '[[:<:]]direct tv[[:>:]]' OR
			subject ~* '[[:<:]]turismo[[:>:]]' OR
			body ~* '[[:<:]]estad�a[[:>:]]' OR
			body ~* '[[:<:]]estadia[[:>:]]' OR
			body ~* '[[:<:]]mucama[[:>:]]' OR
			body ~* '[[:<:]]wifi[[:>:]]' OR
			body ~* '[[:<:]]temporada[[:>:]]' OR
			body ~* '[[:<:]]confort[[:>:]]' OR
			body ~* '[[:<:]]diario[[:>:]]' OR
			body ~* '[[:<:]]verano[[:>:]]' OR
			body ~* '[[:<:]]vacaciones[[:>:]]' OR
			body ~* '[[:<:]]vacacional[[:>:]]' OR
			body ~* '[[:<:]]d�a[[:>:]]' OR
			body ~* '[[:<:]]diarios[[:>:]]' OR
			body ~* '[[:<:]]playa[[:>:]]' OR
			body ~* '[[:<:]]cable[[:>:]]' OR
			body ~* '[[:<:]]camarote[[:>:]]' OR
			body ~* '[[:<:]]sabanas[[:>:]]' OR
			body ~* '[[:<:]]traer[[:>:]]' OR
			body ~* '[[:<:]]resortes[[:>:]]' OR
			body ~* '[[:<:]]colchon[[:>:]]' OR
			body ~* '[[:<:]]colch�n[[:>:]]' OR
			body ~* '[[:<:]]colchones[[:>:]]' OR
			body ~* '[[:<:]]cubrecamas[[:>:]]' OR
			body ~* '[[:<:]]cubrecama[[:>:]]' OR
			body ~* '[[:<:]]wiffi[[:>:]]' OR
			body ~* '[[:<:]]feriados[[:>:]]' OR
			body ~* '[[:<:]]mucama[[:>:]]' OR
			body ~* '[[:<:]]personas[[:>:]]' OR
			body ~* '[[:<:]]persona[[:>:]]' OR
			body ~* '[[:<:]]direct tv[[:>:]]' OR
			body ~* '[[:<:]]turismo[[:>:]]'
		)
		AND (
			subject ~* '[[:<:]]cabana[[:>:]]' OR
			subject ~* '[[:<:]]caba�a[[:>:]]' OR
			body ~* '[[:<:]]cabana[[:>:]]' OR
			body ~* '[[:<:]]caba�a[[:>:]]'
		)
) To '/tmp/cat_1040_u_1260_8_ads.csv' With CSV HEADER;

