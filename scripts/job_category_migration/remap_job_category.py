from remap import remap
import csv
import sys


def remap_file(file):
    with open(file) as csvfile:
        with open(file+'.newcats', 'w') as outfile:
            reader = csv.reader(csvfile, delimiter=',')
            for row in reader:
                row[2] = remap(row[2])
                outfile.write('{0}\n'.format(','.join(row)))


if __name__ == '__main__':
    for file in sys.argv[1:]:
        remap_file(file)
