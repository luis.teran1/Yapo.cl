COPY(
	SELECT
		a.ad_id,
		a.category,
		ap.value as job_category
	FROM 
		ads a
		INNER JOIN
		ad_params ap
	ON
		ap.ad_id = a.ad_id
	WHERE
		a.category IN ('7020', '7040')
		AND ap.name = 'job_category'
) To '/tmp/from_job_category.csv' With CSV HEADER;
