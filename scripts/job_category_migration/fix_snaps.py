from remap import remap
import os
import re
import shutil


regex = r"^(INSERT INTO ad_params.*\(.*'job_category',\s*')(\d+(\|\d+)*)('\);\s*)$"
snap_dir = '/home/miguel/yapo_cl/scripts/db'
for file in os.listdir(snap_dir):
    if file.startswith('insert_blocketdb') and file.endswith('.pgsql'):
        path = snap_dir+'/'+file
        with open(path) as snapfile:
            with open(path+'.new', 'w') as newfile:
                for line in snapfile:
                    m = re.match(regex, line)
                    if m:
                        line = re.sub(regex, m.group(1) + remap(m.group(2)) + m.group(4), line)
                    newfile.write(line)

        shutil.move(path+'.new', path)
