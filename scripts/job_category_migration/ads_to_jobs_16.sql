COPY(
	SELECT
		a.ad_id,
		a.category,
		16 as job_category
	FROM
		ads a
		INNER JOIN
		ad_params ap
	ON
		ap.ad_id = a.ad_id
	WHERE
		a.category in ('7020','7040')
		AND (
			a.subject ~* '[[:<:]]Guardias?[[:>:]]'
			OR
			a.subject ~* '[[:<:]]Seguridad[[:>:]]'
			OR
			a.subject ~* '[[:<:]]Porteros?[[:>:]]'
			OR
			a.subject ~* '[[:<:]]os.?10[[:>:]]'
		)
		AND ap.name = 'job_category'
) To '/tmp/to_job_category_16.csv' With CSV HEADER;
