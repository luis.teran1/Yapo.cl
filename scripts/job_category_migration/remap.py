
def remap(job_category):
    tr = {
        '1':  [20, 24],
        '2':  [19],
        '3':  [20],
        '4':  [20, 24],
        '5':  [20, 24],
        '6':  [19],
        '7':  [20, 24],
        '8':  [22],
        '9':  [23],
        '10': [20, 24],
        '11': [19, 24],
        '12': [17],
        '13': [21],
        '14': [18],
        '15': [20, 24],
    }

    jcs = set()
    for cat in job_category.split('|'):
        if cat in tr:
            for c in tr[cat]:
                jcs.add(c)
        else:
            jcs.add(cat)
    return '|'.join(map(str, sorted(list(jcs))))
