#!/usr/bin/perl -w

use strict;
use DBI;
use Getopt::Long;
use Data::Dumper;
use blocket::bconf;
use perl_trans;

my $transhost = {};

sub update_ad_params {
	my ($ad_id, $category, $job_category) = @_;
	print "${ad_id} - ${category} - ${job_category}\n";

	my %trans;
	$trans{'cmd'} = 'update_ad_params';
	$trans{'ad_id'} = $ad_id;
	$trans{'category'} = $category;
	$trans{'job_category'} = $job_category;
	$trans{'pay_type'} = 'free';
	$trans{'commit'} = 1;

	my %resp = bconf_trans($transhost, %trans);
	if (!%resp) {
		print STDERR "(CRIT) Failed to communicate with trans.\n";
	} elsif ($resp{'status'} ne "TRANS_OK") {
		print STDERR "(CRIT) trans returned status: $resp{status}\n";
	}
}

my $bconffile = "";
my $remote_addr = "";

GetOptions(
   #"remote_addr=s" => \$remote_addr,
   "bconffile=s"  => \$bconffile
);

die "bconf not found" if (!$bconffile);

my $bconf = blocket::bconf::init_conf($bconffile);
$transhost = $$bconf{'*'}{'common'}{'transaction'}{'host'};

my @ads = ();
while(<>) {
	/^(\d+),(\d+),(\d+(\|\d+)*)$/ or next;
	update_ad_params $1, $2, $3;
}
