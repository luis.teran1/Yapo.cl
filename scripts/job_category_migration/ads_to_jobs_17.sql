COPY(
	SELECT
		a.ad_id,
		a.category,
		17 as job_category
	FROM
		ads a
		INNER JOIN
		ad_params ap
	ON
		ap.ad_id = a.ad_id
	WHERE
		a.category IN ('7020','7040')
		AND (
			a.subject ~* '[[:<:]]Aseo[[:>:]]'
			OR
			a.subject ~* '[[:<:]]Limpieza[[:>:]]'
			OR
			a.subject ~* E'[[:<:]]Asesoras?\\s+del?\\s+hogar[[:>:]]'
			OR
			a.subject ~* '[[:<:]]Nanas?[[:>:]]'
			OR
			a.subject ~* '[[:<:]]Mucamas?[[:>:]]'
			OR
			a.subject ~* '[[:<:]]Ni�eras?[[:>:]]'
			OR
			a.subject ~* '[[:<:]]Aseadoras?[[:>:]]'
		)
		AND ap.name = 'job_category'
) To '/tmp/to_job_category_17.csv' With CSV HEADER;
