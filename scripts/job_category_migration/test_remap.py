from remap import remap
from remap_job_category import remap_file
import os
import unittest

class TestRemap(unittest.TestCase):

    def test_single(self):
        data = {
            '1':  '20|24',
            '2':  '19',
            '3':  '20',
            '4':  '20|24',
            '5':  '20|24',
            '6':  '19',
            '7':  '20|24',
            '8':  '22',
            '9':  '23',
            '10': '20|24',
            '11': '19|24',
            '12': '17',
            '13': '21',
            '14': '18',
            '15': '20|24',
        }
        for cat, expected in data.items():
            self.assertEqual(remap(cat), expected)

    def test_ignore_new(self):
        data = map(str, range(16, 25))
        for cat in data:
            self.assertEqual(remap(cat), cat)

    def test_non_repetition(self):
        data = {
            '1|4|5|7':  '20|24',
            '3|10|11|15':  '19|20|24',
            '1|2|3|4|5|6|7|8|9|10|11|12|13|14|15':  '17|18|19|20|21|22|23|24',
        }
        for cat, expected in data.items():
            self.assertEqual(remap(cat), expected)

    def test_file(self):
        data = {
            '1|4|5|7':  '20|24',
            '3|10|11|15':  '19|20|24',
            '1|2|3|4|5|6|7|8|9|10|11|12|13|14|15':  '17|18|19|20|21|22|23|24',
        }
        file = '.dummy.csv'
        expected = input = "ad_id,category,job_category\n"

        for cat, exp in data.items():
            input += '123,7020,{0}\n'.format(cat)
            expected += '123,7020,{0}\n'.format(exp)

        if os.path.exists(file):
            os.remove(file)

        with open(file, 'w') as f:
            f.write(input.strip())

        remap_file(file)
        self.assertTrue(os.path.exists(file+'.newcats'))
        
        with open(file+'.newcats') as f:
            output = f.read()

        self.assertEqual(output, expected)

        os.remove(file+'.newcats')
        os.remove(file)
