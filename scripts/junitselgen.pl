#!/usr/bin/perl

use strict;
use warnings;

while (<>) {
	if (/<tr time="(\d+)".*bgcolor="#(......)".*href="(.*)\.html"/) {
		my ($time, $color, $name) = ($1, $2, $3);
		my $failed = $color ne "ccffcc";
		print "$name\n0\n" . ($failed ? "FAILED" : "OK") . "\n${time}000000\n";
	}
}
