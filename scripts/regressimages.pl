#!/usr/bin/perl -w
#
# This skunkworked script will scan the images/thumbs available in our repo and
# distribute them over the ads in the database using symlinks.
#
# It's a holy mess, I know.
#
# to-do:
#  * move image/thumb symlink code into a sub.
#
use strict;
use DBI;
use Getopt::Long;
use Cwd 'abs_path';

my $pghost = "";
my $pgname = "blocketdb";
my $pguser = $ENV{'USER'};
my $pgpwd = "";
my $pgoptions = "";

my $limit = 2000;
my $quiet = 0;
my $debug = 0;

my $imagesdir_name = "images";
my $thumbsdir_name = "thumbs";
my $regressdir = $ENV{'HOME'}."/src/head/regress";

		
GetOptions ("pghost=s" => \$pghost,
            "pgoptions=s" => \$pgoptions,
            "regressdir=s" => \$regressdir,
	    "debug" => \$debug,
	    "quiet" => \$quiet,
	    "limit=n" => \$limit);

# Need absolute path or symlinking won't work correctly.
$regressdir = abs_path($regressdir);

if (! -d $regressdir) {
	print $regressdir." not found. Must supply --regressdir, should point to your up and running ./regress\n";
	exit;
}

my $mediabasedir = $regressdir."/scratch/upload";
my $mediasrcdir = $mediabasedir."/".$imagesdir_name."/01";
my $thumbsrcdir = $mediabasedir."/".$thumbsdir_name."/01";

print "Scanning ".$mediasrcdir." for images to distribute over database..." unless $quiet;
opendir DIR, $mediasrcdir;
my @image_files_raw = grep { $_ =~ /\.jpg$/ } readdir DIR;
closedir DIR;
print "\n".scalar @image_files_raw." files found.\n" unless $quiet;

# remove images without corresponding thumbs
my @image_files;
print "Matching against ".$thumbsrcdir." for thumbnails...\n" unless $quiet;
foreach my $image (@image_files_raw) {
	if ( ! -f $thumbsrcdir."/".$image) {
		print "Warning: ".$image." is missing thumbnail, excluding.\n" unless $quiet;
	} else {
		push(@image_files, $image);
	}
}
my $num_image_files = scalar @image_files;
print $num_image_files." images /w thumbnails left.\n" unless $quiet;

if ($num_image_files < 1) {
	print "regressimages has nothing to do.\n";
	exit;
}

my $pgdsn = "DBI:Pg:dbname=$pgname;host=$pghost";
if ($pgoptions) {
    $pgdsn .= ";options='$pgoptions'";
}
my $dbh = DBI->connect($pgdsn, $pguser, $pgpwd) or die("Could not connect to database.");

my $mediaid_query = "SELECT ad_media_id,media_type FROM ad_media JOIN ads USING (ad_id) WHERE ads.status='active'";
my $sel = $dbh->prepare($mediaid_query);

my $num_media = $sel->execute();

if ($num_media eq "0E0") {
	print "No media files found in db.\n" unless $quiet;
} else {
	print $num_media ." media files to create.\nProcessing file system (limit=".$limit.")\n" unless $quiet;
}

while (my $row = $sel->fetchrow_hashref) {

	if($limit-- < 1) {
		print "Limit reached.\n" unless $quiet;
		last;
	}

	if ($row->{'media_type'} eq 'image') {
		my $fn = $image_files[rand($num_image_files)];
		my $dn = sprintf("%0*d", 10, $row->{'ad_media_id'});

		# Image
		my $ddir = $mediabasedir."/".$imagesdir_name."/".substr($dn,0,2);
		my $link_from = $mediabasedir."/".$imagesdir_name."/01/".$fn;
		my $link_to = $ddir."/".$dn.".jpg";
		if ( ! -d $ddir ) {
			print "Creating directory ".$ddir."\n" if $debug;
			mkdir($ddir, 0755) || die "Could not create directory: $@\n";
		}
		print "linking ".$link_from." to ".$link_to."\n" if $debug;
		symlink($link_from, $link_to); # || die "Error linking: $@\n";

		# Thumb
		$ddir = $mediabasedir."/".$thumbsdir_name."/".substr($dn,0,2);
		$link_from = $mediabasedir."/".$thumbsdir_name."/01/".$fn;
		$link_to = $ddir."/".$dn.".jpg";
		if ( ! -d $ddir ) {
			print "Creating directory ".$ddir."\n" if $debug;
			mkdir($ddir, 0755) || die "Could not create directory: $@\n";
		}
		print "linking ".$link_from." to ".$link_to." (thumb)\n" if $debug;
		symlink($link_from, $link_to); # || die "Error linking: $@\n";
	}

}

$sel->finish;

print "Symlink creation completed..\n" unless $quiet;

