#include <bconf.h>
#include <ctemplates.h>
#include <logging.h>

#include "database.h"

extern struct bconf_node *filterd_config;

int
database_call(struct bconf_node *dbconf, const char *tmpl, void (*cb)(struct bpapi *, void *), void *cbarg) {

	struct bconf_node *filter_conf = NULL;
	struct shadow_vtree shadow_conf = { {0} };
	struct bpapi_vtree_chain tvt = {0};
	struct bpapi ba = {};

	filter_state_bconf_init_all(&filter_conf);
	default_filter_state = filter_state_bconf;

	null_output_init(&ba.ochain);
	hash_simplevars_init(&ba.schain);
	ba.filter_state = filter_state_bconf;

	if (!dbconf) 
		dbconf = bconf_get(filterd_config, "database");

	bconf_vtree(&shadow_conf.vtree, filter_conf);
	bconf_vtree(&tvt, dbconf);
	shadow_vtree_init(&ba.vchain, &shadow_conf, &tvt);

	log_printf(LOG_DEBUG, "database: calling template [%s]", tmpl);

	call_template(&ba, tmpl);

	int ret = 0;
	if (bpapi_has_key(&ba, "connerr")) {
		log_printf(LOG_CRIT, "database: Failed to connect db: %s", bpapi_get_element(&ba, "connerr", 0));
		ret = -1;
	} else {
		cb(&ba, cbarg);
	}

	bpapi_free(&ba);
	filter_state_bconf_free(&filter_conf);
	return ret;
}
