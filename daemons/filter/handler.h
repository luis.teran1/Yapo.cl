#ifndef HANDLER_H
#define HANDLER_H

#include "linker_set.h"

struct bconf_node;
struct filter_daemon_state;
struct storage;

struct handler {
	char *name;
	struct filter_daemon_state *filter_daemon;
	void *state;
	int (*start)(struct handler *);	/* returns 0 on success */
	void (*stop)(struct handler **);
	void (*storage_updated)(struct handler *);
};

struct handler_type {
        const char *name;
        struct handler *(*create)(const char *, struct filter_daemon_state *, struct bconf_node *);
};

#define ADD_HANDLER_TYPE(name, createfunc)                                          \
        static struct handler_type name = {#name, createfunc};                      \
        LINKER_SET_ADD_DATA(handler_types, name)

#endif
