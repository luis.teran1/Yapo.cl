#include <errno.h>
#include <pthread.h>
#include <stdbool.h>
#include <stdlib.h>
#include <signal.h>
#include <string.h>

#include <bconfig.h>
#include <controller.h>
#include <daemon.h>
#include <logging.h>
#include <memalloc_functions.h>
#include "cached_regex.h"
#include <ps_status.h>
#include <queue.h>
#include <stat_messages.h>

#include "dbpoller.h"
#include "handler.h"
#include "bconf_daemon.h"
#include "bconf_overwrite.h"

extern struct bconf_node *bconfd_config;

static bool stop_signalled = false;

static void
term_handler(int signo) {
	stop_signalled = true;
}

struct handler_instance {
	struct handler *handler;
	SLIST_ENTRY(handler_instance) list;
};

struct neighbour {
	const char *name;
	const char *url;
	SLIST_ENTRY(neighbour) list;
};

struct bconf_daemon_state {
	volatile bool running;
	volatile bool storage_updated;
	bool fatal;

	const char *bconf_path;
	struct storage *db_storage;
	struct storage *file_storage;
	struct storage *overwrite_storage;
	struct storage *merged_storage;
	struct dbpoller_state *dbpoller;
	SLIST_HEAD(, handler_instance) handler_list;

	pthread_mutex_t lock;
	pthread_cond_t signal;

	struct stat_message *status;
	struct ctrl_thread *controller;

	SLIST_HEAD(, neighbour) neighbours;
};

static void rebuild_merged_storage(struct bconf_daemon_state *ds);

int
valid_host_appl(const char *name) {
	static struct cached_regex regex = {
		"^([0-9a-z_]+)$", PCRE_CASELESS | PCRE_DOLLAR_ENDONLY | PCRE_DOTALL | PCRE_NO_AUTO_CAPTURE
	};
	return cached_regex_match(&regex, name, NULL, 0);
}

bool
valid_type(const char *value) {
	if (!value)
		return false;

	return !(strcmp(value, "conf") && strcmp(value, "bconf"));
}

static int
bconf_daemon_reload_bconf(struct bconf_daemon_state *ds) {
	log_printf(LOG_DEBUG, "bconf_daemon: Reload called");
	if (!storage_reload_from_file(ds->file_storage, ds->bconf_path)) {
		log_printf(LOG_WARNING, "bconf_daemon: Failed to load new bconf. Keeping old.");
		return 1;
	}
	if (bconf_overwrite) {
		struct bconf_node *overwritten = storage_swap_db(ds->overwrite_storage, NULL);
		if (overwritten)
			bconf_free(&overwritten);
	}
	bconf_daemon_storage_updated(ds, true);

	return 0;
}

static void
stop_command(struct ctrl_req *cr, struct bpapi *ba, void *v) {
	struct bconf_daemon_state *bs = v;
	bconf_daemon_stop(bs, false);
}

static void
reload_command(struct ctrl_req *cr, struct bpapi *ba, void *v) {
	struct bconf_daemon_state *bs = v;
	struct bconf_node **bc = ctrl_get_bconfp(cr);
	char buf[STORAGE_DIGEST_STRING_LENGTH];

	ctrl_set_content_type(cr, "application/json");
	if (!bconf_daemon_reload_bconf(bs)) {
		storage_get_digest(bs->merged_storage, buf, sizeof(buf));
		ctrl_set_custom_headers(cr, CHECKSUM_HEADER_NAME, buf);
		bconf_add_data(bc, "reload.status", "success");
		ctrl_output_json(cr, ba, "reload");
	} else {
		ctrl_error(cr, 500, "Configuration not loaded, check your logs");
	}
}

static void
checksum_command(struct ctrl_req *cr, struct bpapi *ba, void *v) {
	struct bconf_daemon_state *bs = v;
	struct bconf_node **bc = ctrl_get_bconfp(cr);

	char buf[STORAGE_DIGEST_STRING_LENGTH];

	storage_get_digest(bs->file_storage, buf, sizeof(buf));
	bconf_add_data(bc, "checksums.file", buf);
	storage_get_digest(bs->db_storage, buf, sizeof(buf));
	bconf_add_data(bc, "checksums.db", buf);
	storage_get_digest(bs->merged_storage, buf, sizeof(buf));
	bconf_add_data(bc, "checksums.merged", buf);

	ctrl_output_json(cr, ba, "checksums");
}

static int
setup_neighbours(struct bconf_node *cfg, struct bconf_daemon_state *ds) {
	struct bconf_node *nbc = bconf_get(cfg, "neighbours");

	for (int i = 0; i < bconf_count(nbc); i++) {
		struct bconf_node *bn = bconf_byindex(nbc, i);
		struct neighbour *n = xmalloc(sizeof(*n));

		n->name = bconf_get_string(bn, "name");
		n->url = bconf_get_string(bn, "update_url");

		log_printf(LOG_DEBUG, "bconf_daemon: Adding neighbour %s with url %s", n->name, n->url);
		SLIST_INSERT_HEAD(&ds->neighbours, n, list);
	}

	return 0;
}

static void
free_neighbours(struct bconf_daemon_state *ds) {
	struct neighbour *n;
	while ((n = SLIST_FIRST(&ds->neighbours)) != NULL) {
		SLIST_REMOVE_HEAD(&ds->neighbours, list);

		free(n);
	}
}

static int
setup_controllers(struct bconf_node *cfg, struct bconf_daemon_state *ds) {
	pthread_t *threads;
	int count;
	struct ctrl_handler handlers[] = {
		ctrl_stats_handler,
		{
			.url = "/stop",
			.finish = stop_command,
			.cb_data = ds
		},
		{
			.url = "/reload",
			.finish = reload_command,
			.cb_data = ds
		},
		{
			.url = "/checksum",
			.finish = checksum_command,
			.cb_data = ds
		}
	};
	if ((ds->controller = ctrl_thread_setup(bconf_get(cfg, "stats_controller"), NULL, &threads, &count, handlers, sizeof(handlers) / sizeof(handlers[0]), -1)) == NULL) {
		log_printf(LOG_CRIT, "Failed to start controllers (%m)");
		return -1;
	}
	return 0;
}

static int
setup_storage(struct bconf_node *cfg, struct bconf_daemon_state *ds) {
	const char *path = bconf_get_string(cfg, "bconf");
	
	if (!path) {
		log_printf(LOG_CRIT, "bconf_daemon: bconf entry missing in configuration file");
		return -1;
	}

	if ((ds->file_storage = storage_create_from_file(path)) == NULL) {
		log_printf(LOG_CRIT, "bconf_daemon: failed to create storage");
		return -1;
	}
	ds->bconf_path = path;
	
	ds->db_storage = storage_create_from_bconf(NULL);
	ds->merged_storage = storage_create_from_bconf(NULL);

	if (bconf_overwrite)
		ds->overwrite_storage = storage_create_from_bconf(NULL);

	rebuild_merged_storage(ds);

	return 0;
}

static void
release_storage(struct bconf_daemon_state *ds) {
	storage_free(&ds->db_storage);
	storage_free(&ds->file_storage);
	storage_free(&ds->merged_storage);
	if (bconf_overwrite)
		storage_free(&ds->overwrite_storage);
}

static int
setup_handlers(struct bconf_node *cfg, struct bconf_daemon_state *ds) {
	struct handler_type * const * htypepp;
	struct handler_type * htypep;
	struct bconf_node *handler_config;
	LINKER_SET_DECLARE(handler_types, struct handler_type);
	
	if ((handler_config = bconf_get(cfg, "handlers")) == NULL || bconf_count(handler_config) == 0) {
		log_printf(LOG_CRIT, "bconf_daemon: no handlers configured");
		return -1;
	}

	for (int i = 0; i < bconf_count(handler_config); i++) {
		struct bconf_node *hc = bconf_byindex(handler_config, i);
		const char *name = bconf_key(hc);
		const char *type = bconf_get_string(hc, "type") ?: name;
		struct handler_instance *hi = NULL;

		LINKER_SET_FOREACH(htypepp, handler_types) {
			htypep = *htypepp;
			if (strcmp(htypep->name, type) == 0) {
				log_printf(LOG_DEBUG, "bconf_daemon: creating handler \"%s\" of type \"%s\"", name, type);
				hi = xmalloc(sizeof(*hi));
				if ((hi->handler = htypep->create(name, ds, bconf_get(hc, "config"))) == NULL) {
					free(hi);
					log_printf(LOG_CRIT, "bconf_daemon: could not create handler \"%s\" of type \"%s\"", name, type);
					return -1;
				}
			}
		}

		if (!hi) {
			log_printf(LOG_CRIT, "bconf_daemon: could not find type \"%s\" for handler \"%s\"", type, name);
			return -1;
		}

		SLIST_INSERT_HEAD(&ds->handler_list, hi, list);
	}

	return 0;
}

static void
start_handlers(struct bconf_daemon_state *ds) {
	struct handler_instance *hi;

	SLIST_FOREACH(hi, &ds->handler_list, list) {
		hi->handler->start(hi->handler);
	}
}

static void
stop_handlers(struct bconf_daemon_state *ds) {
	struct handler_instance *hi;
	
	while ((hi = SLIST_FIRST(&ds->handler_list)) != NULL) {
		SLIST_REMOVE_HEAD(&ds->handler_list, list);

		hi->handler->stop(&hi->handler);
		free(hi);
	}
}

static int
setup_dbpoller(struct bconf_node *cfg, struct bconf_daemon_state *ds) {
	struct bconf_node *dbpcfg = bconf_get(cfg, "dbpoller");

	if (!dbpcfg) {
		log_printf(LOG_WARNING, "bconf_daemon: No configuration found for database polling. Set dbpoller in config file.");
		log_printf(LOG_WARNING, "bconf_daemon: Not enabling database polling.");
		ds->dbpoller = NULL;
		return 0;
	}

	if ((ds->dbpoller = dbpoller_create(dbpcfg, ds, ds->db_storage)) == NULL) {
		log_printf(LOG_CRIT, "bconf_daemon: Failed to create dbpoller.");
		return -1;
	}

	return 0;
}

static void
notify_handlers_storage_updated(struct bconf_daemon_state *ds) {
	struct handler_instance *hi;

	SLIST_FOREACH(hi, &ds->handler_list, list) {
		hi->handler->storage_updated(hi->handler);
	}
}

struct storage * bconf_daemon_get_storage(struct bconf_daemon_state *ds, enum storage_id id) {

	switch(id) {
		case STORAGE_STATIC:
			return ds->file_storage;
		case STORAGE_DYNAMIC:
			return ds->db_storage;
		case STORAGE_ALL:
			return ds->merged_storage;
		case STORAGE_OVERWRITE:
			if (bconf_overwrite)
				return ds->overwrite_storage;
			/* Itentional fallthrough */
	};

	log_printf(LOG_ERR, "(ERROR) bconf_daemon: Request for undefined storage id %d", id);
	return NULL;
}

static void
rebuild_merged_storage(struct bconf_daemon_state *ds) {
	struct bconf_node *src;
	struct bconf_node *temp = NULL;

	log_printf(LOG_DEBUG, "bconf_daemon: Merging static and dynamic configuration");

	src = storage_get_reader(ds->file_storage);
	bconf_merge(&temp, src);
	storage_put(ds->file_storage);

	src = storage_get_reader(ds->db_storage);
	bconf_merge(&temp, src);
	storage_put(ds->db_storage);

	if (bconf_overwrite) {
		src = storage_get_reader(ds->overwrite_storage);
		bconf_merge(&temp, src);
		storage_put(ds->overwrite_storage);
	}

	struct bconf_node *old = storage_swap_db(ds->merged_storage, temp);
	if (old)
		bconf_free(&old);

	log_printf(LOG_DEBUG, "bconf_daemon: Merged configuration swapped in to place.");
}

void
bconf_daemon_storage_updated(struct bconf_daemon_state *ds, bool wait) {
	pthread_mutex_lock(&ds->lock);
	rebuild_merged_storage(ds);
	ds->storage_updated = true;
	pthread_cond_broadcast(&ds->signal);
	if (wait) {
		while (ds->storage_updated)
			pthread_cond_wait(&ds->signal, &ds->lock);
	}
	pthread_mutex_unlock(&ds->lock);
}

void
bconf_daemon_stop(struct bconf_daemon_state *ds, bool fatal) {
	pthread_mutex_lock(&ds->lock);
	ds->fatal = fatal;
	ds->running = false;
	pthread_cond_broadcast(&ds->signal);
	pthread_mutex_unlock(&ds->lock);
}

int
bconf_daemon(void) {
	struct bconf_node *cfg = bconfd_config;
	struct sigaction sa;

	int retval;
	struct bconf_daemon_state ds = {
		.running = true,
		.storage_updated = false,
		.fatal = false,
		.lock = PTHREAD_MUTEX_INITIALIZER,
		.signal = PTHREAD_COND_INITIALIZER,
		.status = stat_message_dynamic_alloc(2, "bconfd", "status"),
	};
	SLIST_INIT(&ds.handler_list);
	SLIST_INIT(&ds.neighbours);
	memset(&sa, 0, sizeof(sa));

	sa.sa_handler = term_handler;
	sigaction(SIGTERM, &sa, NULL);

	set_ps_display("starting up");
	stat_message_printf(ds.status, "starting");
	if ((retval = setup_controllers(cfg, &ds)) ||
		(retval = setup_neighbours(cfg, &ds)) ||
		(retval = setup_storage(cfg, &ds)) ||
		(retval = setup_handlers(cfg, &ds)) ||
		(retval = setup_dbpoller(cfg, &ds))) {
		goto out;
	}

	if (dbpoller_start(ds.dbpoller)) {
		struct timespec interval = { 0 };
		interval.tv_nsec = 900000;

		log_printf(LOG_DEBUG, "Waiting for db_storage to be populated");
		while (!ds.dbpoller->visited) 
			nanosleep(&interval, NULL);

		log_printf(LOG_DEBUG, "Done waiting for db_storage to be populated");
	}

	start_handlers(&ds);

	set_ps_display("running");
	stat_message_printf(ds.status, "running");
	pthread_mutex_lock(&ds.lock);
	while (ds.running) {
		struct timespec abstime;
		clock_gettime(CLOCK_REALTIME, &abstime);
		abstime.tv_sec++;
		pthread_cond_timedwait(&ds.signal, &ds.lock, &abstime);
		if (stop_signalled) {
			ds.running = false;
			continue;
		}
		if (ds.storage_updated) {
			log_printf(LOG_DEBUG, "Storage has been updated");
			notify_handlers_storage_updated(&ds);
			ds.storage_updated = false;
			pthread_cond_broadcast(&ds.signal);
		}
	}
	ds.storage_updated = false;
	pthread_cond_broadcast(&ds.signal);
	pthread_mutex_unlock(&ds.lock);

	if (ds.fatal)
		retval = -1;

out:
	log_printf(LOG_INFO, "bconf_daemon: stopping");
	stat_message_printf(ds.status, "stopping handlers");
	stop_handlers(&ds);	
	
	stat_message_printf(ds.status, "stopping dbpoller");
	dbpoller_destroy(&ds.dbpoller);

	if (ds.controller)
		ctrl_thread_quit(ds.controller);

	release_storage(&ds);
	free_neighbours(&ds);
	stat_message_dynamic_free(ds.status);
	log_printf(LOG_INFO, "bconf_daemon: stopped");
	return retval;
}
