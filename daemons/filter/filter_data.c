#include <memalloc_functions.h>
#include <stdio.h>
#include <stdlib.h>

#include "data_model.h"
#include "filter_data.h"

void do_filter_data(const struct input *input, void (*filter_data_cb)(struct match *, void *), void *data) {

	char buf[16];
	struct match m = { 0 };
	int n_matches = 2;
	static int count = 0;

	for (int i = 0; i < n_matches; ++i) {
		snprintf(buf, 16, "%d", ++count);
		m.rule_id = buf;
		filter_data_cb(&m, data);
	}
}
