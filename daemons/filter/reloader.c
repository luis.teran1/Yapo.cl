#include <pthread.h>
#include <time.h>
#include <stdlib.h>
#include <stat_messages.h>
#include <stat_counters.h>

#include <logging.h>
#include <memalloc_functions.h>
#include <ctemplates.h>
#include <bpapi.h>
#include <timer.h>

#include "reloader.h"
#include "filter_daemon.h"

int
reloader_need_reload(struct reloader_state *rs, struct timespec *ref) {
	if (!rs)	return -1;
	if (ref)
		clock_gettime(CLOCK_MONOTONIC, ref);
	pthread_mutex_lock(&rs->mutex);
	rs->dirty = true;
	pthread_mutex_unlock(&rs->mutex);
	return 0;
}

void
reloader_wait_for(struct reloader_state *rs, struct timespec *ref) {
	pthread_mutex_lock(&rs->mutex);
	while (timespeccmp(&rs->last_reload, ref, <))
		pthread_cond_wait(&rs->reloaded, &rs->mutex);
	pthread_mutex_unlock(&rs->mutex);
}

static void
reloader_completed(struct reloader_state *rs, struct timespec *start) {
	pthread_mutex_lock(&rs->mutex);
	rs->last_reload = *start;
	rs->dirty = false;
	pthread_cond_signal(&rs->reloaded);
	pthread_mutex_unlock(&rs->mutex);
}

static void *
reloader_thread(void *arg) {
	struct reloader_state *rps = arg;

	log_printf(LOG_INFO, "reloader: starting");
	rps->running = true;

	while (rps->running) {
		/* this should be a pthread condition... just in case people specify a really long interval. OR set specific cancellation points. */
		if (rps->dirty) {
			log_printf(LOG_INFO, "reloader: reloading");
			struct timespec start = {0};
			clock_gettime(CLOCK_MONOTONIC, &start);
			filter_daemon_reload_storage(rps->ds);
			reloader_completed(rps, &start);
		}
		nanosleep(&rps->interval, NULL);
	}

	log_printf(LOG_INFO, "reloader: stopping");
	return NULL;
}

bool
reloader_start(struct reloader_state *rps) {
	if (!rps)
		return false;

	int res = pthread_create(&rps->reloader_thread, NULL, reloader_thread, rps);
	if (res != 0) {
		log_printf(LOG_CRIT, "Failed to start reloader thread.");
		return false;
	}

	return true;
}

void
reloader_stop(struct reloader_state *rps) {
	if (!rps)
		return;

	if (rps->running) {
		rps->running = false;
		pthread_join(rps->reloader_thread, NULL);
	}
}

struct reloader_state *
reloader_create(struct bconf_node *cfg, struct filter_daemon_state *ds, struct storage *db) {
	struct reloader_state *rps = zmalloc(sizeof(*rps));
	int interval = bconf_get_int_default(cfg, "interval_ms", 1000); /* Poll every second per default */

	rps->cfg = cfg;
	rps->ds = ds;
	rps->db = db;
	rps->interval.tv_sec = interval / 1000;
	rps->interval.tv_nsec = (interval % 1000) * 1000000;
	pthread_mutex_init(&rps->mutex, NULL);
	pthread_cond_init(&rps->reloaded, NULL);

	return rps;
}

void
reloader_destroy(struct reloader_state **rps) {
	if (!rps || !*rps)
		return;

	if ((*rps)->running)
		reloader_stop(*rps);

	free(*rps);
	*rps = NULL;
}
