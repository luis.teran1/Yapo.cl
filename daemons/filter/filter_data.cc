#include <algorithm>
#include <iostream>
#include <memalloc_functions.h>
#include <set>
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <unordered_map>
#include <vector>

#include "filter_data.h"
#include "trie.h"

static const char *
field_int_to_str(int field) {
	switch(field) {
		case FIELD_SUBJECT:                    return "subject";
		case FIELD_BODY:                       return "body";
		case FIELD_UID:                        return "uid";
		case FIELD_EMAIL:                      return "email";
		case FIELD_NAME:                       return "name";
		case FIELD_COUNTRY:                    return "country";
		case FIELD_PHONE:                      return "phone";
		case FIELD_REMOTE_ADDR:                return "remote_addr";
		case FIELD_CATEGORY:                   return "category";
		case FIELD_EXTERNAL_EVALUATION:        return "external_evaluation";
		case FIELD_AD_PARAM:                   return "ad_param";
		case FIELD_ACTION_PARAM:               return "action_param";
		case FIELD_EXTERNAL_EVALUATION_REASON: return "external_evaluation_reason";
		default:                               break;
	}
	return "";
}

class BlockRuleCondition {
	public:
		int condition_id;
		int rule_id;
		int list_id;
		int whole_word;
		int fields;

		BlockRuleCondition(int condition_id, int rule_id, int list_id, int whole_word, int fields)
			: condition_id(condition_id), rule_id(rule_id), list_id(list_id), whole_word(whole_word), fields(fields) {}
};

class BlockRule {
	public:
		int rule_id;
		string rule_name;
		string action;
		enum application application;
		enum combine_rule combine_rule;
		string target;
		vector<int> conditions;

		BlockRule(int rule_id, const char *rule_name, const char *action, enum application application, enum combine_rule combine_rule, const char *target)
			: rule_id(rule_id), rule_name(rule_name), action(action), application(application), combine_rule(combine_rule), target(target), conditions() {}
		void add_condition(int condition_id) { conditions.emplace_back(condition_id); }
		int priority();
};

int BlockRule::priority() {
	enum rule_action actn = lookup_rule_action(action.c_str(), action.length());
	switch(actn) {
		case RA_STOP:		return 3;
		case RA_DELETE:		return 2;
		case RA_SPAMFILTER:	return 1;
		default:			break;
	}
	return 0;
}

class BlockList {
	public:
		int size;
		int list_id;
		int whole_word;
		string list_name;
		enum list_type list_type;
		Trie *trie;
		set<int> conditions;

		BlockList(int list_id, int whole_word, int condition_id, const char *list_name, enum list_type list_type)
			: size(0), list_id(list_id), whole_word(whole_word), list_name(list_name), list_type(list_type), trie(new Trie()), conditions() {
				conditions.insert(condition_id);
			}
		~BlockList() { delete trie; }

		void add_item(const char *value);
		void add_condition(int condition_id) { conditions.insert(condition_id); }
		void clear_trie();
};

void BlockList::add_item(const char *value) {
	string item;
	string s(value);
	if (list_type == LT_IP) {
		item = START_OF_STRING + s;
	} else if (whole_word == 0) {
		item = s;
	} else if (list_type == LT_EMAIL || whole_word == 2) {
		string lc;
		lc.resize(s.size());
		std::transform(s.begin(), s.end(), lc.begin(), ::tolower);
		item = START_OF_STRING + lc + END_OF_STRING;
	} else {
		item = WORD_BOUNDARY + s + WORD_BOUNDARY;
	}
	trie->add(item, tolower, this);
	size++;
}

void BlockList::clear_trie() {
	cout << "deleting unused trie for list:" << list_id << " whole_word:" << whole_word << "\n";
	delete trie;
	trie = NULL;
}

class FilterData;

class FDMatchData : public MatchData {
	public:
		int field;
		FDMatchData(const MatchData *md, int field) : MatchData(md), field(field) {}
};

class MatchResult {
	public:
		FilterData *fd;
		enum application appl;
		void (*callback)(const struct match *, void *);
		void *cbdata;
		std::unordered_map<int, FDMatchData *> matches;
	private:
		void test_rule(BlockRule *);
		void got_match(BlockRule *, FDMatchData *);
	public:
		MatchResult(FilterData *fd, enum application appl, void (*filter_data_cb)(const struct match *, void *), void *data)
			: fd(fd), appl(appl), callback(filter_data_cb), cbdata(data), matches() {}
		~MatchResult();
		void update(int field, const char *value);
		void finish();
};

class FilterData {
	public:
		int ready;
		std::unordered_map<int, BlockRuleCondition *> conditions;
		std::unordered_map<int, BlockRule *> rules;
		std::unordered_map<int, std::unordered_map<int, BlockList *> > lists;
		std::unordered_map<int, std::vector<int> > field_conditions;
		std::unordered_map<int, Trie *> field_merged_list;
		std::vector<BlockRule *> ordered_rules;
	private:
		BlockList* get_list(int list_id, int whole_word);
		void match(MatchResult &, int field, const char *value);
	public:
		~FilterData();
		BlockRule* get_rule(int rule_id);
		BlockRuleCondition* get_condition(int condition_id);
		void add_condition(int condition_id, int rule_id, int list_id, int whole_word, int fields);
		void add_rule(int rule_id, const char *rule_name, const char *action, enum application application, enum combine_rule combine_rule, const char *target);
		void add_list(int list_id, const char *list_name, enum list_type list_type);
		void add_item(int list_id, const char *value);
		void ready_up();
		void match(const struct input *input, void (*filter_data_cb)(const struct match *, void *), void *data);
		void get_counters(int *lists, int *rules, int *conditions, int *items);
};

FilterData::~FilterData() {
	for (auto&& it : conditions)
		delete it.second;
	for (auto&& it : rules)
		delete it.second;
	for (auto&& it : lists)
		for (auto&& it2 : it.second)
			delete it2.second;
	for (auto&& it : field_merged_list)
		delete it.second;
}

BlockRule* FilterData::get_rule(int rule_id) {
	auto it = rules.find(rule_id);
	if (it != rules.end())
		return it->second;
	return NULL;
}

BlockList* FilterData::get_list(int list_id, int whole_word) {
	auto it = lists.find(list_id);
	if (it != lists.end()) {
		auto it2 = it->second.find(whole_word);
		if (it2 != it->second.end()) {
			return it2->second;
		}
	}
	return NULL;
}

BlockRuleCondition* FilterData::get_condition(int condition_id) {
	auto it = conditions.find(condition_id);
	if (it != conditions.end())
		return it->second;
	return NULL;
}

void FilterData::add_condition(int condition_id, int rule_id, int list_id, int whole_word, int fields) {
	if (ready)	return;
	BlockRuleCondition *condition = new BlockRuleCondition(condition_id, rule_id, list_id, whole_word, fields);
	conditions.emplace(condition_id, condition);
	/* Map the fields the condition requires */
	for (int k = 0x1; k <= FIELD_SENTINEL; k <<= 1) {
		if (fields & k) {
			auto it = field_conditions.find(k);
			if (it == field_conditions.end()) {
				vector<int> v;
				v.emplace_back(condition_id);
				field_conditions.emplace(k, v);
			} else {
				it->second.emplace_back(condition_id);
			}
		}
	}
	BlockRule *rule = get_rule(rule_id);
	if (rule) {
		rule->add_condition(condition_id);
	}
}

void FilterData::add_rule(int rule_id, const char *rule_name, const char *action, enum application application, enum combine_rule combine_rule, const char *target) {
	if (ready)	return;
	BlockRule *rule = new BlockRule(rule_id, rule_name, action, application, combine_rule, target);
	rules.emplace(rule_id, rule);
	ordered_rules.emplace_back(rule);
}

void FilterData::add_list(int list_id, const char *list_name, enum list_type list_type) {
	if (ready)	return;
	/* For every condition */
	for (auto&& cond : conditions) {
		/* That matches against the new list */
		if (cond.second->list_id == list_id) {
			/* Create a new list with corresponding whole_word value */
			auto it = lists.find(list_id);
			if (it == lists.end()) {
				/* Create new */
				cout << "Creating list:" << list_id << " ww:" << cond.second->whole_word << " condition_id:" << cond.second->condition_id << "\n";
				BlockList *list = new BlockList(list_id, cond.second->whole_word, cond.second->condition_id, list_name, list_type);
				unordered_map<int, BlockList *> match_to_list;
				match_to_list.emplace(cond.second->whole_word, list);
				lists.emplace(list_id, match_to_list);
			} else {
				/* list_id exists, check for whole_word */
				auto it2 = it->second.find(cond.second->whole_word);
				if (it2 == it->second.end()) {
					/* Whole word does not exist, create new entry */
					BlockList *list = new BlockList(list_id, cond.second->whole_word, cond.second->condition_id, list_name, list_type);
					cout << "Creating different list:" << list_id << " ww:" << cond.second->whole_word << " condition_id:" << cond.second->condition_id << "\n";
					it->second.emplace(cond.second->whole_word, list);
				} else {
					/* Same list, add extra condition */
					cout << "Add condition to list:" << list_id << " ww:" << cond.second->whole_word << " condition_id:" << cond.second->condition_id << "\n";
					it2->second->add_condition(cond.second->condition_id);
				}
			}
		}
	}
}

void FilterData::add_item(int list_id, const char *value) {
	if (ready)	return;
	auto it = lists.find(list_id);
	if (it != lists.end()) {
		for (auto&& mt : it->second) {
			mt.second->add_item(value);
		}
	}
}

void FilterData::ready_up() {
	/* Sort conditions in order of priority */
	std::sort(ordered_rules.begin(), ordered_rules.end(), [](BlockRule *a, BlockRule *b) {
		return a->priority() > b->priority();
	});

	/* For every field */
	for (auto&& it : field_conditions) {
		Trie *t = new Trie();
		int field = it.first;
		/* For every condition with that field */
		cout << "Starting merges for field:" << field_int_to_str(field) << "\n";
		for (int condition_id : it.second) {
			cout << "Starting merges for condition_id:" << condition_id << "\n";
			auto it2 = conditions.find(condition_id);
			if (it2 != conditions.end()) {
				/* Merge corresponding lists */
				BlockList *list = get_list(it2->second->list_id, it2->second->whole_word);
				cout << "Merging list:" << list->list_id << " ww:" << list->whole_word << " conditions:";
				for (int condition_id : list->conditions) {
					cout << condition_id << ",";
				}
				cout << "\n";
				t->merge(*list->trie);
			}
		}
		/* Store the merged list */
		field_merged_list.emplace(field, t);
	}
	/* Release unused memory */
	for (auto&& it : lists)
		for (auto&& it2 : it.second)
			it2.second->clear_trie();
	ready = true;
}

void FilterData::match(const struct input *input, void (*filter_data_cb)(const struct match *, void *), void *data) {
	if (!input->application) return;

	enum application appl = lookup_application(input->application, -1);
	if (appl == A_NONE)
		return;

	MatchResult mr(this, appl, filter_data_cb, data);
	mr.update(FIELD_SUBJECT,                    input->subject                   );
	mr.update(FIELD_BODY,                       input->body                      );
	mr.update(FIELD_UID,                        input->uid                       );
	mr.update(FIELD_EMAIL,                      input->email                     );
	mr.update(FIELD_NAME,                       input->name                      );
	mr.update(FIELD_COUNTRY,                    input->country                   );
	mr.update(FIELD_PHONE,                      input->phone                     );
	mr.update(FIELD_REMOTE_ADDR,                input->remote_addr               );
	mr.update(FIELD_CATEGORY,                   input->category                  );
	mr.update(FIELD_EXTERNAL_EVALUATION,        input->external_evaluation       );
	mr.update(FIELD_AD_PARAM,                   input->ad_param                  );
	mr.update(FIELD_ACTION_PARAM,               input->action_param              );
	mr.update(FIELD_EXTERNAL_EVALUATION_REASON, input->external_evaluation_reason);
	mr.finish();
}

void FilterData::get_counters(int *ls, int *rs, int *cs, int *is) {
	*ls = lists.size();
	*rs = rules.size();
	*cs = conditions.size();

	int i = 0;
	for (auto&& it : lists) {
		i += it.second.begin()->second->size;
	}
	*is = i;
}

MatchResult::~MatchResult() {
	for (auto &&it : matches)
		delete it.second;
}

void MatchResult::update(int field, const char *value) {
	if (!value)	return;
	/* Get the merged list for input field */
	auto it = fd->field_merged_list.find(field);
	if (it != fd->field_merged_list.end()) {
		/* Get the string on lower case */
		string s(value);
		string lc;
		lc.resize(s.size());
		std::transform(s.begin(), s.end(), lc.begin(), ::tolower);
		/* Do the actual matching */
		vector<const MatchData *> found = it->second->match(lc);
		for (const MatchData *md : found) {
			for (void *m : md->get_data()) {
				BlockList *list = static_cast<BlockList *>(m);
				for (int condition_id : list->conditions) {
					/* We need to be sure that the match is for the field we are matching */
					BlockRuleCondition *cond = fd->get_condition(condition_id);
					if (cond->fields & field) {
						cout << "Matched word:" << md->word << " list_id:" << list->list_id << " condition_id:" << condition_id << "\n";
						if (matches.find(condition_id) == matches.end()) {
							FDMatchData *match = new FDMatchData(md, field);
							matches.emplace(condition_id, match);
						}
					}
				}
			}
		}
	}
}

struct combine {
	bool start;
	bool (*combine)(bool, bool);
};

struct combine AND = {
	.start = true,
	.combine = [](bool a, bool b) { return a && b; },
};

struct combine OR = {
	.start = false,
	.combine = [](bool a, bool b) { return a || b; },
};

void MatchResult::got_match(BlockRule *rule, FDMatchData *md) {
	char rule_id[32];
	snprintf(rule_id, 32, "%d", rule->rule_id);
	struct match m = {
		.rule_id = rule_id,
		.rule_name = rule->rule_name.c_str(),
		.action = rule->action.c_str(),
		.target = rule->target.c_str(),
		.combi_match = rule->conditions.size() > 1 && rule->combine_rule == CR_AND ? "true" : "false",
		.matched_column = field_int_to_str(md->field),
		.matched_value = md->word.c_str(),
	};
	callback(&m, cbdata);
}

void MatchResult::test_rule(BlockRule *rule) {
	struct combine cr = rule->combine_rule == CR_AND ? AND : OR;
	bool ok = cr.start;
	FDMatchData *last = NULL;
	cout << "Seeing if rule:" << rule->rule_id << " matches\n";
	for (int condition_id : rule->conditions) {
		cout << "Seeing if cond:" << condition_id << " matches\n";
		BlockRuleCondition *cond = fd->get_condition(condition_id);
		auto it = matches.find(condition_id);
		bool r = false;
		if (it != matches.end() && cond->whole_word != -1) {
			/* found a match */
			cout << "It matched!\n";
			last = it->second;
			r = true;
		} else if (it == matches.end() && cond->whole_word == -1) {
			/* Check for "not-in" matches */
			cout << "Non match is ok!\n";
			r = true;
		}
		ok = cr.combine(ok, r);
		if (ok != cr.start)
			break;
	}
	if (ok) {
		cout << "rule:" << rule->rule_id << " matched!\n";
		got_match(rule, last);
	}
}

void MatchResult::finish() {
	for (BlockRule *rule : fd->ordered_rules) {
		if (rule->application != appl)
			continue;
		test_rule(rule);
	}
}

/*
 * C API
 */

void *
filter_data_new() {
	FilterData *fdi = new FilterData();
	return fdi;
}

void
filter_data_free(void **fdi) {
	if (fdi && *fdi) {
		FilterData *fd = static_cast<FilterData *>(*fdi);
		delete fd;
		*fdi = NULL;
	}
}

void
filter_data_add_condition(void *fdi, int condition_id, int rule_id, int list_id, int whole_word, int fields) {
	FilterData *fd = static_cast<FilterData *>(fdi);
	fd->add_condition(condition_id, rule_id, list_id, whole_word, fields);
}

void
filter_data_add_rule(void *fdi, int rule_id, const char *rule_name, const char *action, enum application application, enum combine_rule combine_rule, const char *target) {
	FilterData *fd = static_cast<FilterData *>(fdi);
	fd->add_rule(rule_id, rule_name, action, application, combine_rule, target);
}

void
filter_data_add_list(void *fdi, int list_id, const char *list_name, enum list_type list_type) {
	FilterData *fd = static_cast<FilterData *>(fdi);
	fd->add_list(list_id, list_name, list_type);
}

void
filter_data_add_item(void *fdi, int list_id, const char *value) {
	FilterData *fd = static_cast<FilterData *>(fdi);
	fd->add_item(list_id, value);
}

void
filter_data_done_loading(void *fdi) {
	FilterData *fd = static_cast<FilterData *>(fdi);
	fd->ready_up();
}

void do_filter_data(void *fdi, const struct input *input, void (*filter_data_cb)(const struct match *, void *), void *data) {
	FilterData *fd = static_cast<FilterData *>(fdi);
	fd->match(input, filter_data_cb, data);
}

void filter_data_get_counters(void *fdi, int *lists, int *rules, int *conditions, int *items) {
	FilterData *fd = static_cast<FilterData *>(fdi);
	fd->get_counters(lists, rules, conditions, items);
}
