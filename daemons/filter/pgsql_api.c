#include <stdlib.h>
#include <string.h>
#include "pgsql_api.h"
#include "util.h"

int
pgsql_split_array (char *text, int len, char ***elems) {
	char *end;
	int nelems = 0;
	int max_elems;
	char *cp;
	enum {
		st_start,
		st_middle,
		st_end,
		st_quoted
	} state = st_start;

	if (len < 0)
		len = strlen(text);

	if (!len) {
		*elems = NULL;
		return 0;
	}

	if (text[0] != '{' || text[len - 1] != '}')
		return -1;

	max_elems = 0;
	end = text + len - 1;
	for (cp = text; cp; cp = memchr (cp + 1, ',', end - cp))
		max_elems++;

	*elems = xmalloc (max_elems * sizeof (**elems));

	for (text++ ; text < end ; ) {
		switch (state) {
		case st_start:
			if (*text == '"') {
				state = st_quoted;
				text++;
			} else
				state = st_middle;
			if (nelems >= max_elems) {
				free(*elems);
				return -1; /* XXX do something better here. */
			}
			(*elems)[nelems++] = text;
			break;
		case st_middle:
			if (*text == ',') {
				*text = '\0';
				state = st_start;
			} else if (*text == '\\')
				memmove(text, text + 1, --end - text);
			text++;
			break;
		case st_end:
			if (*text++ != ',') {
				free(*elems);
				return -1;
			}
			state = st_start;
			break;
		case st_quoted:
			if (*text == '"') {
				*text = '\0';
				state = st_end;
			} else if (*text == '\\')
				memmove(text, text + 1, --end - text);
			text++;
			break;
		}
	}
	*end = '\0';
	return nelems;
}

