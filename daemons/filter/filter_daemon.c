#include <errno.h>
#include <pthread.h>
#include <stdbool.h>
#include <stdlib.h>
#include <signal.h>
#include <string.h>

#include <bconfig.h>
#include <controller.h>
#include <daemon.h>
#include <logging.h>
#include <memalloc_functions.h>
#include "cached_regex.h"
#include <ps_status.h>
#include <queue.h>
#include <stat_messages.h>

#include "filter_daemon.h"
#include "handler.h"
#include "reloader.h"
#include "storage.h"

extern struct bconf_node *filterd_config;

static bool stop_signalled = false;

static void
term_handler(int signo) {
	stop_signalled = true;
}

struct handler_instance {
	struct handler *handler;
	SLIST_ENTRY(handler_instance) list;
};

struct filter_daemon_state {
	volatile bool running;
	volatile bool storage_updated;
	bool fatal;

	struct bconf_node *dbconf;
	struct storage *storage;
	struct reloader_state *reloader;
	SLIST_HEAD(, handler_instance) handler_list;

	pthread_mutex_t lock;
	pthread_cond_t signal;

	struct stat_message *status;
	struct ctrl_thread *controller;
};

static bool rebuild_storage(struct filter_daemon_state *ds);

int
filter_daemon_reload_storage(struct filter_daemon_state *ds) {
	log_printf(LOG_DEBUG, "filter_daemon: Reload called");

	pthread_mutex_lock(&ds->lock);
	/* do the actual update */
	bool ok = rebuild_storage(ds);
	if (!ok) {
		log_printf(LOG_WARNING, "filter_daemon: Failed to load new data. Keeping old.");
		pthread_mutex_unlock(&ds->lock);
		return 1;
	}
	ds->storage_updated = true;
	pthread_cond_broadcast(&ds->signal);
	while (ds->storage_updated)
		pthread_cond_wait(&ds->signal, &ds->lock);
	pthread_mutex_unlock(&ds->lock);
	return 0;
}

static void
stop_command(struct ctrl_req *cr, struct bpapi *ba, void *v) {
	struct filter_daemon_state *bs = v;
	filter_daemon_stop(bs, false);
}

static void
reload_command(struct ctrl_req *cr, struct bpapi *ba, void *v) {
	struct filter_daemon_state *bs = v;
	struct bconf_node **bc = ctrl_get_bconfp(cr);

	ctrl_set_content_type(cr, "application/json");

	struct timespec ref = {0};
	int err = reloader_need_reload(bs->reloader, &ref);

	if (!err) {
		/* async reload started, let's wait for it */
		reloader_wait_for(bs->reloader, &ref);
		bconf_add_data(bc, "reload.status", "success");
		ctrl_output_json(cr, ba, "reload");
	} else if (!filter_daemon_reload_storage(bs)) {
		/* async reload is not available, we went with sync */
		bconf_add_data(bc, "reload.status", "success");
		ctrl_output_json(cr, ba, "reload");
	} else {
		/* no reload at all, oops */
		ctrl_error(cr, 500, "{\"status\": \"failed\"}");
	}

}

static void
reload_async_command(struct ctrl_req *cr, struct bpapi *ba, void *v) {
	struct filter_daemon_state *bs = v;
	struct bconf_node **bc = ctrl_get_bconfp(cr);
	int err = reloader_need_reload(bs->reloader, NULL);

	if (!err) {
		ctrl_status(cr, 202);
		bconf_add_data(bc, "reload.status", "accepted");
	} else {
		ctrl_status(cr, 412);
		bconf_add_data(bc, "reload.status", "not possible");
	}

	ctrl_output_json(cr, ba, "reload");
}

static int
setup_controllers(struct bconf_node *cfg, struct filter_daemon_state *ds) {
	pthread_t *threads;
	int count;
	struct ctrl_handler handlers[] = {
		ctrl_stats_handler,
		{
			.url = "/stop",
			.finish = stop_command,
			.cb_data = ds
		},
		{
			.url = "/reload",
			.finish = reload_command,
			.cb_data = ds
		},
		{
			.url = "/async_reload",
			.finish = reload_async_command,
			.cb_data = ds
		}
	};
	if ((ds->controller = ctrl_thread_setup(bconf_get(cfg, "stats_controller"), NULL, &threads, &count, handlers, sizeof(handlers) / sizeof(handlers[0]), -1)) == NULL) {
		log_printf(LOG_CRIT, "Failed to start controllers (%m)");
		return -1;
	}
	return 0;
}

static int
setup_storage(struct bconf_node *cfg, struct filter_daemon_state *ds) {
	struct bconf_node *dbconf = bconf_get(cfg, "database");

	if (!dbconf) {
		log_printf(LOG_CRIT, "filter_daemon: database entry missing in configuration file");
		return -1;
	}
	
	if ((ds->storage = storage_create_from_database(dbconf)) == NULL) {
		log_printf(LOG_CRIT, "filter_daemon: failed to create storage");
		return -1;
	}
	ds->dbconf = dbconf;

	return 0;
}

static void
release_storage(struct filter_daemon_state *ds) {
	storage_free(&ds->storage);
}

static int
setup_handlers(struct bconf_node *cfg, struct filter_daemon_state *ds) {
	struct handler_type * const * htypepp;
	struct handler_type * htypep;
	struct bconf_node *handler_config;
	LINKER_SET_DECLARE(handler_types, struct handler_type);
	
	if ((handler_config = bconf_get(cfg, "handlers")) == NULL || bconf_count(handler_config) == 0) {
		log_printf(LOG_CRIT, "filter_daemon: no handlers configured");
		return -1;
	}

	for (int i = 0; i < bconf_count(handler_config); i++) {
		struct bconf_node *hc = bconf_byindex(handler_config, i);
		const char *name = bconf_key(hc);
		const char *type = bconf_get_string(hc, "type") ?: name;
		struct handler_instance *hi = NULL;

		LINKER_SET_FOREACH(htypepp, handler_types) {
			htypep = *htypepp;
			if (strcmp(htypep->name, type) == 0) {
				log_printf(LOG_DEBUG, "filter_daemon: creating handler \"%s\" of type \"%s\"", name, type);
				hi = xmalloc(sizeof(*hi));
				if ((hi->handler = htypep->create(name, ds, bconf_get(hc, "config"))) == NULL) {
					free(hi);
					log_printf(LOG_CRIT, "filter_daemon: could not create handler \"%s\" of type \"%s\"", name, type);
					return -1;
				}
			}
		}

		if (!hi) {
			log_printf(LOG_CRIT, "filter_daemon: could not find type \"%s\" for handler \"%s\"", type, name);
			return -1;
		}

		SLIST_INSERT_HEAD(&ds->handler_list, hi, list);
	}

	return 0;
}

static void
start_handlers(struct filter_daemon_state *ds) {
	struct handler_instance *hi;

	SLIST_FOREACH(hi, &ds->handler_list, list) {
		hi->handler->start(hi->handler);
	}
}

static void
stop_handlers(struct filter_daemon_state *ds) {
	struct handler_instance *hi;
	
	while ((hi = SLIST_FIRST(&ds->handler_list)) != NULL) {
		SLIST_REMOVE_HEAD(&ds->handler_list, list);

		hi->handler->stop(&hi->handler);
		free(hi);
	}
}

static int
setup_reloader(struct bconf_node *cfg, struct filter_daemon_state *ds) {
	struct bconf_node *rcfg = bconf_get(cfg, "reloader");

	if (!rcfg) {
		log_printf(LOG_WARNING, "filter_daemon: No configuration found for async reloading. Set reloader in config file.");
		log_printf(LOG_WARNING, "filter_daemon: Not enabling async reloading.");
		ds->reloader = NULL;
		return 0;
	}

	if ((ds->reloader = reloader_create(rcfg, ds, ds->storage)) == NULL) {
		log_printf(LOG_CRIT, "filter_daemon: Failed to create reloader.");
		return -1;
	}

	return 0;
}

static void
notify_handlers_storage_updated(struct filter_daemon_state *ds) {
	struct handler_instance *hi;

	SLIST_FOREACH(hi, &ds->handler_list, list) {
		hi->handler->storage_updated(hi->handler);
	}
}

struct storage *
filter_daemon_get_storage(struct filter_daemon_state *ds) {
	return ds->storage;
}

static bool
rebuild_storage(struct filter_daemon_state *ds) {
	log_printf(LOG_DEBUG, "filter_daemon: Reloading storage from database");
	return storage_reload_from_database(ds->storage, ds->dbconf);
}

void
filter_daemon_stop(struct filter_daemon_state *ds, bool fatal) {
	pthread_mutex_lock(&ds->lock);
	ds->fatal = fatal;
	ds->running = false;
	pthread_cond_broadcast(&ds->signal);
	pthread_mutex_unlock(&ds->lock);
}

int
filter_daemon(void) {
	struct bconf_node *cfg = filterd_config;
	struct sigaction sa;

	int retval;
	struct filter_daemon_state ds = {
		.running = true,
		.storage_updated = false,
		.fatal = false,
		.lock = PTHREAD_MUTEX_INITIALIZER,
		.signal = PTHREAD_COND_INITIALIZER,
		.status = stat_message_dynamic_alloc(2, "filterd", "status"),
	};
	SLIST_INIT(&ds.handler_list);
	memset(&sa, 0, sizeof(sa));

	sa.sa_handler = term_handler;
	sigaction(SIGTERM, &sa, NULL);

	set_ps_display("starting up");
	stat_message_printf(ds.status, "starting");
	if ((retval = setup_controllers(cfg, &ds)) ||
		(retval = setup_storage(cfg, &ds)) ||
		(retval = setup_handlers(cfg, &ds)) ||
		(retval = setup_reloader(cfg, &ds))) {
		goto out;
	}

	reloader_start(ds.reloader);
	start_handlers(&ds);

	set_ps_display("running");
	stat_message_printf(ds.status, "running");
	pthread_mutex_lock(&ds.lock);
	while (ds.running) {
		struct timespec abstime;
		clock_gettime(CLOCK_REALTIME, &abstime);
		abstime.tv_sec++;
		pthread_cond_timedwait(&ds.signal, &ds.lock, &abstime);
		if (stop_signalled) {
			ds.running = false;
			continue;
		}
		if (ds.storage_updated) {
			log_printf(LOG_DEBUG, "Storage has been updated");
			notify_handlers_storage_updated(&ds);
			ds.storage_updated = false;
			pthread_cond_broadcast(&ds.signal);
		}
	}
	ds.storage_updated = false;
	pthread_cond_broadcast(&ds.signal);
	pthread_mutex_unlock(&ds.lock);

	if (ds.fatal)
		retval = -1;

out:
	log_printf(LOG_INFO, "filter_daemon: stopping");
	stat_message_printf(ds.status, "stopping handlers");
	stop_handlers(&ds);	

	stat_message_printf(ds.status, "stopping reloader");
	reloader_destroy(&ds.reloader);
	
	if (ds.controller)
		ctrl_thread_quit(ds.controller);

	release_storage(&ds);
	stat_message_dynamic_free(ds.status);
	log_printf(LOG_INFO, "filter_daemon: stopped");
	return retval;
}
