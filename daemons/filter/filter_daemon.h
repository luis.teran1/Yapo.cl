#ifndef FILTER_DAEMON_H
#define FILTER_DAEMON_H
#include <bconf.h>
#include <stdbool.h>

struct filter_daemon_state;
struct bconf_node *filterd_config;
void filter_daemon_stop(struct filter_daemon_state *, bool);
int filter_daemon(void);

struct storage *filter_daemon_get_storage(struct filter_daemon_state *);
int filter_daemon_reload_storage(struct filter_daemon_state *ds);

#endif
