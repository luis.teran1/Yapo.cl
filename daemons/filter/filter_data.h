#ifndef FILTER_DATA_H
#define FILTER_DATA_H

struct input {
	const char *application;
	const char *subject;
	const char *body;
	const char *uid;
	const char *email;
	const char *name;
	const char *country;
	const char *phone;
	const char *remote_addr;
	const char *category;
	const char *external_evaluation;
	const char *ad_param;
	const char *action_param;
	const char *external_evaluation_reason;
};

struct match {
	const char *rule_id;
	const char *rule_name;
	const char *action;
	const char *target;
	const char *combi_match;
	const char *matched_column;
	const char *matched_value;
};

struct bconf_node;

#define FIELD_NONE                          0x0
#define FIELD_SUBJECT                       0x1
#define FIELD_BODY                          0x2
#define FIELD_UID                           0x4
#define FIELD_EMAIL                         0x8
#define FIELD_NAME                          0x10
#define FIELD_COUNTRY                       0x20
#define FIELD_PHONE                         0x40
#define FIELD_REMOTE_ADDR                   0x80
#define FIELD_CATEGORY                      0x100
#define FIELD_EXTERNAL_EVALUATION           0x200
#define FIELD_AD_PARAM                      0x400
#define FIELD_ACTION_PARAM                  0x800
#define FIELD_EXTERNAL_EVALUATION_REASON    0x1000
#define FIELD_SENTINEL                      0x1000

#ifdef __cplusplus
using namespace std;
extern "C" {
#endif

/* gperf enums */
#include <string.h>
#include "application.h"
#include "combine_rule.h"
#include "list_type.h"
#include "rule_action.h"

/*
 * Allocation/Deallocation
 */
void *filter_data_new();
void filter_data_free(void **);

/*
 * Functions to add data to the instance.
 * Please note that they can't be called in arbitrary order.
 * The required order is: rules, conditions, lists, items.
 * After all data has been loaded. You must call filter_data_done_loading()
 * That call may take some time, as it will generate all the inner structures.
 */
void filter_data_add_condition(void *fdi, int condition_id, int rule_id, int list_id, int whole_word, int fields);
void filter_data_add_rule(void *fdi, int rule_id, const char *rule_name, const char *action, enum application, enum combine_rule, const char *target);
void filter_data_add_list(void *fdi, int list_id, const char *list_name, enum list_type);
void filter_data_add_item(void *fdi, int list_id, const char *value);
void filter_data_done_loading(void *fdi);

/*
 * Do the heavy lifting.
 * NULL parameters will be considered absent on the matching stage.
 * Callback function will be invoked on every match with data as last argument
*/
void do_filter_data(void *fdi, const struct input *input, void (*filter_data_cb)(const struct match *, void *), void *data);

void filter_data_get_counters(void *fdi, int *lists, int *rules, int *conditions, int *items);

#ifdef __cplusplus
}
#endif

#endif
