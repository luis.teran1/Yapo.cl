#ifndef TRIE_H
#define TRIE_H

#include <iostream>
#include <unordered_map>
#include <vector>

extern unsigned char START_OF_STRING_CHAR;
extern unsigned char END_OF_STRING_CHAR;
extern unsigned char WORD_BOUNDARY_CHAR;

extern std::string START_OF_STRING;
extern std::string END_OF_STRING;
extern std::string WORD_BOUNDARY;

class MatchData {
	public:
		std::string word;
		std::vector<void *> userdata;

		MatchData(std::string w, void *data)
			: word(w), userdata() { add(data); }
		MatchData(const MatchData *md)
			: word(md->word), userdata(md->userdata) {}

		void add(void *moredata) { userdata.emplace_back(moredata); }
		void add_all(MatchData *other) { userdata.insert(userdata.end(), other->userdata.begin(), other->userdata.end()); }
		const std::vector<void *> get_data() const { return userdata; }
};

class Trie;

class Trie {
	private:
		bool word_end;
		MatchData *data;
		std::unordered_map<unsigned char, Trie *> t;

		Trie(const std::string &s, int p, int (*transform)(int c), void *data)
			: word_end(false), data(NULL), t() { this->add(s, p, transform, data); }

		void add(const std::string &s, size_t p, int (*transform)(int c), void *data);
		bool find(std::string &s, size_t p) const;
		void print(int d) const;
		void add_match_data(std::string s, void *data);
	public:
		Trie() : word_end(false), data(NULL), t() { std::cout << "Trie basic constructor\n"; }
		Trie(const Trie &);
		Trie(const Trie &&);
		~Trie();

		void merge(const Trie &);
		void add(const std::string &s) { this->add(s, 0, NULL, NULL); }
		void add(const std::string &s, int (*transform)(int c), void *data) { this->add(s, 0, transform, data); }
		bool find(std::string &s) const { return this->find(s, 0); }
		std::vector<const MatchData *> match(const std::string &s) const;
		Trie* findc(unsigned char c) const;
		bool word_ends_here() const { return this->word_end; }
		std::string matched_word() const { return this->data ? this->data->word : ""; }
		const MatchData *match_data() const { return this->data; }
		void print() const { return this->print(0); }
};

#endif
