#include <algorithm>
#include <fstream>
#include <iostream>
#include <iterator>
#include <memory>
#include <sstream>

#include "trie.h"

unsigned char START_OF_STRING_CHAR = 0x01;
unsigned char END_OF_STRING_CHAR = 0x02;
unsigned char WORD_BOUNDARY_CHAR = 0x03;

std::string START_OF_STRING = "\x01";
std::string END_OF_STRING = "\x02";
std::string WORD_BOUNDARY = "\x03";

using namespace std;

static bool
is_invalid_char(const char c) {
	return c > 0 && c < 4;
}

static bool
is_word_char(unsigned char c) {
	return (c >= '0' && c <= '9')
		|| (c >= 'A' && c <= 'Z')
		|| c == '_'
		|| (c >= 'a' && c <= 'z')
		|| (c >= (unsigned char) '�' && c <= (unsigned char) '�')
		|| (c >= (unsigned char) '�' && c <= (unsigned char) '�')
		|| (c >= (unsigned char) '�' && c <= (unsigned char) '�')
		|| (c >= (unsigned char) '�' && c <= (unsigned char) '�');
}

static bool
is_word_boundary(unsigned char prev, unsigned char next) {
	return is_word_char(prev) ^ is_word_char(next);
}

Trie::Trie(const Trie &t) : word_end(t.word_end), data(NULL), t() {
	/*cout << "Trie copy constructor\n";*/
	if (t.data)
		data = new MatchData(t.data);
	for (auto&& it : t.t) {
		this->t[it.first] = new Trie(*it.second);
	}
}

Trie::Trie(const Trie &&t) : word_end(t.word_end), data(t.data), t(std::move(t.t)) { 
	/*cout << "Trie move constructor\n";*/
}

Trie::~Trie() {
	/*cout << "Trie destructor\n";*/
	for (auto&& it : t) {
		delete it.second;
	}
	delete data;
}

static string 
clean_string(const string &s) {
	string result;
	result.reserve(s.length());
	remove_copy_if(s.begin(), s.end(), back_inserter(result), is_invalid_char);

	cout << "clean [" << s << "] -> [" << result << "]" << "\n";

	return result;
}

void Trie::add_match_data(std::string s, void *userdata) {
	word_end = true;
	//cout << "added word: " << s << "\n";
	if (data)
		data->add(userdata);
	else
		data = new MatchData(clean_string(s), userdata);
}

void Trie::add(const string &s, size_t p, int (*transform)(int c), void *data) {
	/*
	static int call_count = 0;
	cout << "add:" << ++call_count << endl;
	cerr << "adding [" << s.substr(p) << "]" << "\n";
	*/
	if (s.length() == p) {
		/* done */
		add_match_data(s, data);
		return;
	}
	unsigned char c = transform ? transform(s[p]) : s[p];
	auto it = t.find(c);
	if (it == t.end()) {
		/* not found */
		t[c] = new Trie(s, p+1, transform, data);
	} else {
		it->second->add(s, p+1, transform, data);
	}
}

bool Trie::find(string &s, size_t p) const {
	//cerr << "matching [" << s.substr(p) << "]" << "\n";
	if (s.length() == p) {
		/* fully consumed */
		return true;
	}
	auto it = this->t.find(s[p]);
	if (it == this->t.end()) {
		/* not found */
		return false;
	}
	return it->second->find(s, p+1);
}

void Trie::print(int d) const {
	string s = "";
	for (int i = 0 ; i < d; ++i)
		s += " ";
	if (this->word_ends_here()) {
		cout << "*" << " -> " << this->matched_word();
	}
	cout << "\n";
    for (auto&& it : t) {
		cout << s;
		if (is_invalid_char(it.first))
			cout << "0x" << +it.first;
		else
			cout << it.first;
		it.second->print(d+1);
	}
}

vector<const MatchData *> Trie::match(const string &s) const {
	vector<const MatchData *> closed;
	vector<Trie *> open;
	Trie *t;

	auto check_match = [&](Trie *m) mutable {
		if (m->word_ends_here()) {
			/* got a match */
			//cout << "match! " << m->matched_word() << "\n";
			closed.emplace_back(m->match_data());
		}
	};

	auto match_char = [&](unsigned char c) mutable {
		//cout << c << "\n";
		cout << "match char " << c << "(" << (int) c << ")\n";
		/* try to advance open matches */
		vector <Trie *> next;
		for (size_t i = 0; i < open.size(); ++i) {
			Trie *m = open[i];
			check_match(m);
			t = m->findc(c);
			if (t)
				next.emplace_back(t);
			if (c == WORD_BOUNDARY_CHAR)
				next.emplace_back(m);
		}
		/* try to start a new match */
		t = findc(c); 
		if (t)
			next.emplace_back(t);
		open = next;
		//cout << open.size() << " open matches" << "\n";
	};

	match_char(START_OF_STRING_CHAR);

	/* for every char on input string */
	unsigned char p = '\0';
	for (unsigned char c : s) {
		if (is_word_boundary(p, c))
			match_char(WORD_BOUNDARY_CHAR);
		match_char(c);
		p = c;
	}
	/* end of the input */
	if (is_word_boundary(p, '\0'))
		match_char(WORD_BOUNDARY_CHAR);
	match_char(END_OF_STRING_CHAR);

	/* Open matches that already matched also count */
	for (Trie *m : open) {
		check_match(m);
	}

	return closed;
}

void Trie::merge(const Trie &t) {
	word_end |= t.word_end;
	if (t.data) {
		if (data) {
			data->add_all(t.data);
		} else {
			data = new MatchData(t.data);
		}
	}
	for (auto&& it : t.t) {
		Trie *tt = findc(it.first);
		if (tt)
			tt->merge(*it.second);
		else
			this->t[it.first] = new Trie(*it.second);
	}
}

Trie* Trie::findc(unsigned char c) const {
	//static int call_count = 0;
	//cout << "findc:" << ++call_count << endl;
	auto it = this->t.find(c);
	if (it == this->t.end())
		return NULL;
	return it->second;
}
