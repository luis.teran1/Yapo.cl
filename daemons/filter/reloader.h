#ifndef RELOADER_H
#define RELOADER_H
#include <inttypes.h>
#include "filter_daemon.h"
#include "storage.h"

struct reloader_state {
	pthread_t reloader_thread;
	pthread_mutex_t mutex;
	pthread_cond_t reloaded;
	struct filter_daemon_state *ds;
	volatile bool running;
	volatile bool dirty;

	struct timespec interval;
	struct timespec last_reload;

	struct bconf_node *cfg;

	struct storage *db;
};

int reloader_need_reload(struct reloader_state *rs, struct timespec *ref);
void reloader_wait_for(struct reloader_state *rs, struct timespec *ref);

bool reloader_start(struct reloader_state *rs);
void reloader_stop(struct reloader_state *rs);
struct reloader_state * reloader_create(struct bconf_node *cfg, struct filter_daemon_state *ds, struct storage *db);
void reloader_destroy(struct reloader_state **rs);

#endif
