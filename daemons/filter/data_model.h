#ifndef DATA_MODEL_H
#define DATA_MODEL_H

struct block_list;
struct block_rule;
struct block_rule_condition;

enum list_type {
	LIST_TYPE_IP,
	LIST_TYPE_EMAIL,
	LIST_TYPE_CATEGORY,
	LIST_TYPE_GENERAL,
	LIST_TYPE_COUNTRY
};

struct block_list {
	int id;
	enum list_type type;
	/* Not sure if add blocked_items here or leave that to the implementation */
};

enum combine {
	COMBINE_OR,
	COMBINE_AND
};

struct block_rule {
	int id;
	const char *name;
	const char *application;
	const char *action;
	const char *target;
	enum combine combine_rule;
};

enum match_type {
	MATCH_ANYWHERE,
	MATCH_WORD,
	MATCH_EXACT,
	MATCH_PREFIX
};

struct block_rule_condition {
	int id;
	int list_id;
	int rule_id;
	int fields;
	enum match_type whole_word;
};

#endif
