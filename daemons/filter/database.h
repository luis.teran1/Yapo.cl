#ifndef DATABASE_H
#define DATABASE_H

struct bconf_node;
struct bpapi;

/*
 * Execute the requested template with the given dbconf node.
 * The callback will be invoked after the query has returned.
 */
int database_call(struct bconf_node *dbconf, const char *tmpl, void (*)(struct bpapi *, void *), void *cbarg);

#endif
