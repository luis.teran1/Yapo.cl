#ifndef STORAGE_H
#define STORAGE_H
#include <stdbool.h>
#include <bconf.h>

struct storage;

/*
 * Request read or write permissions for the inner structures
 * When done using it, it must be put back
 */
void *storage_get_reader(struct storage *s);
void *storage_get_writer(struct storage *s);
void storage_put(struct storage *s);

/*
 * Do the best effort to load storage from database.
 * Errors will be reported as NULL or false, respectively
 */
struct storage *storage_create_from_database(struct bconf_node *conf);
bool storage_reload_from_database(struct storage *s, struct bconf_node *conf);

/*
 * Release resources associated to the storage
 */
void storage_free(struct storage **);
#endif
