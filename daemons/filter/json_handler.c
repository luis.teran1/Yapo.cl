#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <bconf.h>
#include <bpapi.h>
#include <controller.h>
#include <ctemplates.h>
#include <logging.h>
#include <memalloc_functions.h>

#include "json_vtree.h"
#include "filter_daemon.h"
#include "handler.h"
#include "lru.h"
#include "storage.h"
#include "timer.h"

#include "filter_data.h"

#define MIN_MAX_CACHE_SIZE_MB 512

struct json_state {
	struct handler *handler;
	int num_workers;
	struct bconf_node *cfg;

	struct {
		struct bconf_node *config;
		struct ctrl_thread *thread;
	} controller;
};

struct cbdata {
	int matches;
	struct bconf_node **bc;
};

struct reqdata {
	struct json_state *s;
	struct buf_string bs;
};

static void
filter_data_cb(const struct match *match, void *v) {
	struct cbdata *data = v;

	/* Prepare the output */
	char buf[32];
	snprintf(buf, 32, "result.%d", data->matches++);
	struct bconf_node *node = bconf_add_listnode(data->bc, buf);

	bconf_add_data(&node, "o_rule_id", match->rule_id ?: "");
	bconf_add_data(&node, "o_rule_name", match->rule_name ?: "");
	bconf_add_data(&node, "o_action", match->action ?: "");
	bconf_add_data(&node, "o_target", match->target ?: "");
	bconf_add_data(&node, "o_combi_match", match->combi_match ?: "");
	bconf_add_data(&node, "o_matched_column", match->matched_column ?: "");
	bconf_add_data(&node, "o_matched_value", match->matched_value ?: "");
}

static void
filter_command(struct ctrl_req *cr, struct bpapi *ba, void *v) {
	struct reqdata *rd = v;
	struct bconf_node **bc = ctrl_get_bconfp(cr);

	if (rd->bs.pos == 0 || rd->bs.buf == NULL) {
		log_printf(LOG_WARNING, "filter_command: received empty request");
		bconf_add_data(bc, "filter.error", "EMPTY_REQUEST");
		ctrl_output_json(cr, ba, "filter");
		ctrl_status(cr, 400);
		return;
	}

	/* Fetch input parameters */
	struct bconf_node *bn = NULL;
	json_bconf(&bn, NULL, rd->bs.buf, rd->bs.pos, 0);
	struct input input = {
		bconf_get_string(bn, "application"),
		bconf_get_string(bn, "subject"),
		bconf_get_string(bn, "body"),
		bconf_get_string(bn, "uid"),
		bconf_get_string(bn, "email"),
		bconf_get_string(bn, "name"),
		bconf_get_string(bn, "country"),
		bconf_get_string(bn, "phone"),
		bconf_get_string(bn, "remote_addr"),
		bconf_get_string(bn, "category"),
		bconf_get_string(bn, "external_evaluation"),
		bconf_get_string(bn, "ad_param"),
		bconf_get_string(bn, "action_param"),
		bconf_get_string(bn, "external_evaluation_reason"),
	};

	/* Do the harlem shake */
	struct cbdata data = { 0, bc };
	bconf_add_listnode(bc, "result");

	struct storage *storage = filter_daemon_get_storage(rd->s->handler->filter_daemon);
	void *fdi = storage_get_reader(storage);
	do_filter_data(fdi, &input, filter_data_cb, &data);
	storage_put(storage);

	/* The actual output sending */
	bpapi_insert(ba, "vtree_root", "result");
	ctrl_set_content_type(cr, "application/json");
	render_template_cb(cr, ba, (char *)"json_array");

	/* Free all the things */
	bconf_free(&bn);
	free(rd->bs.buf);
	free(rd);
}

static void
filter_command_start(struct ctrl_req *cr, void *v) {
	struct reqdata *rd = zmalloc(sizeof(*rd));
	rd->s = v;
	ctrl_set_handler_data(cr, rd);
}

static void
check_command(struct ctrl_req *cr, struct bpapi *ba, void *v) {
	struct json_state *s = v;
	struct bconf_node **bc = ctrl_get_bconfp(cr);

	struct storage *storage = filter_daemon_get_storage(s->handler->filter_daemon);
	void *fdi = storage_get_reader(storage);

	int lists, rules, conditions, items;
	filter_data_get_counters(fdi, &lists, &rules, &conditions, &items);
	storage_put(storage);

	char buf[32];
	struct bconf_node *node = bconf_add_listnode(bc, "counters");

	snprintf(buf, 32, "%d", lists);
	bconf_add_data(&node, "lists", buf);
	snprintf(buf, 32, "%d", rules);
	bconf_add_data(&node, "rules", buf);
	snprintf(buf, 32, "%d", conditions);
	bconf_add_data(&node, "conditions", buf);
	snprintf(buf, 32, "%d", items);
	bconf_add_data(&node, "items", buf);

	bpapi_insert(ba, "vtree_root", "counters");
	ctrl_set_content_type(cr, "application/json");
	render_template_cb(cr, ba, (char *)"json_array");
}

static int
filter_post_data(struct ctrl_req *cr, struct bpapi *ba, size_t totlen, const char *data, size_t len, void *v) {
	struct reqdata *rd = v;
	bswrite(&rd->bs, data, len);
	return 0;
}

static int
start(struct handler *h) {
	struct json_state *s = h->state;
	pthread_t *threads;
	int num_threads;

	struct ctrl_handler handlers[] = {
		{
			.url = "/filter",
			.start = filter_command_start,
			.finish = filter_command,
			.consume_post = filter_post_data,
			.cb_data = s,
		},
		{
			.url = "/check",
			.finish = check_command,
			.cb_data = s,
		},
		ctrl_stats_handler,
	};

	log_printf(LOG_INFO, "handler %s: starting", h->name);
	if ((s->controller.thread = ctrl_thread_setup(s->controller.config, NULL, &threads, &num_threads, handlers, sizeof(handlers) / sizeof(handlers[0]), -1)) == NULL) {
		log_printf(LOG_CRIT, "handler %s: Failed to start controllers (%m)", h->name);
		return -1;
	}
	return 0;
}

static void
stop(struct handler **h) {
	struct json_state *s = (*h)->state;

	log_printf(LOG_INFO, "handler %s: stopping", (*h)->name);
	ctrl_thread_quit(s->controller.thread);

	free(s);
	free((*h)->name);
	free(*h);
	*h = NULL;
}

static void
storage_updated(struct handler *h) {

}

static struct handler *
create(const char *name, struct filter_daemon_state *ds, struct bconf_node *cfg) {
	struct handler *h = xmalloc(sizeof(*h));
	struct json_state *s = xmalloc(sizeof(*s));

	log_printf(LOG_DEBUG, "creating handler of type json with name %s", name);

	h->filter_daemon = ds;
	h->name = xstrdup(name);
	h->start = start;
	h->stop = stop;
	h->storage_updated = storage_updated;
	h->state = s;

	s->handler = h;
	s->controller.config = bconf_get(cfg, "controller");
	s->cfg = cfg;

	if (!s->controller.config) {
		log_printf(LOG_CRIT, "handler %s: Missing configuration for controller.", h->name);
		free(s);
		free(h);
		return NULL;
	}

	return h;
}

ADD_HANDLER_TYPE(json, create);
