#include <pthread.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <limits.h>
#include <memalloc_functions.h>
#include <logging.h>
#include <bconf.h>
#include <bconfig.h>
#include "database.h"
#include "field_type.h"
#include "filter_data.h"
#include "storage.h"
#include "util.h"

struct storage {
	pthread_rwlock_t rwlock;
	void *fdi;	/* Pointer to FilterData C++ class */
};

#include <bpapi.h>
#include <pgsql_api.h>

static int
field_str_to_int(const char *field) {
	switch(lookup_field_type(field, -1)) {
		case FT_SUBJECT:                       return FIELD_SUBJECT;
		case FT_BODY:                          return FIELD_BODY;
		case FT_UID:                           return FIELD_UID;
		case FT_EMAIL:                         return FIELD_EMAIL;
		case FT_NAME:                          return FIELD_NAME;
		case FT_COUNTRY:                       return FIELD_COUNTRY;
		case FT_PHONE:                         return FIELD_PHONE;
		case FT_REMOTE_ADDR:                   return FIELD_REMOTE_ADDR;
		case FT_CATEGORY:                      return FIELD_CATEGORY;
		case FT_EXTERNAL_EVALUATION:           return FIELD_EXTERNAL_EVALUATION;
		case FT_AD_PARAM:                      return FIELD_AD_PARAM;
		case FT_ACTION_PARAM:                  return FIELD_ACTION_PARAM;
		case FT_EXTERNAL_EVALUATION_REASON:    return FIELD_EXTERNAL_EVALUATION_REASON;
		default:                               break;
	}
	return FT_NONE;
}

static void
select_conditions_cb(struct bpapi *ba, void *fdi) {
	for (int i = 0; i < bpapi_length(ba, "condition_id"); i++) {
		int condition_id = atoi(bpapi_get_element(ba, "condition_id", i));
		int rule_id = atoi(bpapi_get_element(ba, "rule_id", i));
		int list_id = atoi(bpapi_get_element(ba, "list_id", i));
		int whole_word = atoi(bpapi_get_element(ba, "whole_word", i));
		char *arr = xstrdup(bpapi_get_element(ba, "fields", i));

		char **fieldstr;
		int n_fields = pgsql_split_array(arr, strlen(arr), &fieldstr);

		int fields = 0;
		for (int j = 0; j < n_fields; ++j)
			fields |= field_str_to_int(fieldstr[j]);

		log_printf(LOG_DEBUG, "storage: cond:%d rule:%d list:%d ww:%d fields:%d", condition_id, rule_id, list_id, whole_word, fields);
		filter_data_add_condition(fdi, condition_id, rule_id, list_id, whole_word, fields);

		free(fieldstr);
		free(arr);
	}
}

static void
select_rules_cb(struct bpapi *ba, void *fdi) {
	for (int i = 0; i < bpapi_length(ba, "rule_id"); ++i) {
		int rule_id = atoi(bpapi_get_element(ba, "rule_id", i));
		const char *rule_name = bpapi_get_element(ba, "rule_name", i);
		const char *action = bpapi_get_element(ba, "action", i);
		const char *application = bpapi_get_element(ba, "application", i);
		const char *combine_rule = bpapi_get_element(ba, "combine_rule", i);
		const char *target = bpapi_get_element(ba, "target", i);

		log_printf(LOG_DEBUG, "storage: rule:%d appl:%s action:%s target:%s combine:%s name:%s", rule_id, application, action, target, combine_rule, rule_name);
		filter_data_add_rule(fdi, rule_id, rule_name, action, lookup_application(application, -1), lookup_combine_rule(combine_rule, -1), target);
	}
}

static void
select_lists_cb(struct bpapi *ba, void *fdi) {
	for (int i = 0; i < bpapi_length(ba, "list_id"); ++i) {
		int list_id = atoi(bpapi_get_element(ba, "list_id", i));
		const char *list_name = bpapi_get_element(ba, "list_name", i);
		const char *list_type = bpapi_get_element(ba, "list_type", i);

		log_printf(LOG_DEBUG, "storage: list:%d type:%s name:%s", list_id, list_type, list_name);
		filter_data_add_list(fdi, list_id, list_name, lookup_list_type(list_type, -1));
	}
}

static void
select_items_cb(struct bpapi *ba, void *fdi) {
	for (int i = 0; i < bpapi_length(ba, "list_id"); ++i) {
		int list_id = atoi(bpapi_get_element(ba, "list_id", i));
		const char *value = bpapi_get_element(ba, "value", i);

		log_printf(LOG_DEBUG, "storage: list:%d value:%s", list_id, value);
		filter_data_add_item(fdi, list_id, value);
	}
}

static bool
load_config(void **dst, struct bconf_node *conf) {

	void *fdi = filter_data_new();
	if (!fdi)	return false;

	struct load {
		const char *message;
		const char *tmpl;
		void (*callback)(struct bpapi *, void *);
	};

	struct load to_load[] = {
		{ "fetching rules",      "select_rules",      select_rules_cb      },
		{ "fetching conditions", "select_conditions", select_conditions_cb },
		{ "fetching lists",      "select_lists",      select_lists_cb      },
		{ "fetching items",      "select_items",      select_items_cb      },
		{ NULL, NULL, NULL },
	};

	int ret;
	bool ok = true;
	struct load *next = to_load;

	while (ok && next->message) {
		log_printf(LOG_INFO, "storage: %s", next->message);
		ret = database_call(conf, next->tmpl, next->callback, fdi);
		ok &= ret == 0;
		next++;
	}

	int lists, rules, conditions, items;
	filter_data_get_counters(fdi, &lists, &rules, &conditions, &items);

	log_printf(LOG_INFO, "storage: loaded %d lists, %d rules, %d, conditions, %d items",
		lists, rules, conditions, items
	);

	if (!lists || !rules || !conditions || !items)
		ok = false;

	if (ok) {
		log_printf(LOG_INFO, "storage: finishing load process");
		filter_data_done_loading(fdi);
		log_printf(LOG_INFO, "storage: ready");
	}

	*dst = fdi;
	return ok;
}

static void *
storage_swap_db(struct storage *s, void *new) {
	void *old;

	pthread_rwlock_wrlock(&s->rwlock);
	old = s->fdi;
	s->fdi = new;
	pthread_rwlock_unlock(&s->rwlock);

	return old;
}

void *
storage_get_reader(struct storage *s) {
	pthread_rwlock_rdlock(&s->rwlock);
	return s->fdi;
}

void *
storage_get_writer(struct storage *s) {
	pthread_rwlock_wrlock(&s->rwlock);
	return &s->fdi;
}

void
storage_put(struct storage *s) {
	pthread_rwlock_unlock(&s->rwlock);
}

struct storage *
storage_create_from_database(struct bconf_node *conf) {
	struct storage *s = zmalloc(sizeof(*s));
	void *new = NULL;

	if (!load_config(&new, conf)) {
		log_printf(LOG_CRIT, "storage: failed to load from database");
		filter_data_free(&new);
		free(s);
		return NULL;
	}

	pthread_rwlock_init(&s->rwlock, NULL);
	storage_swap_db(s, new);

	return s;
}

bool
storage_reload_from_database(struct storage *s, struct bconf_node *conf) {
	void *new = NULL;

	if (!load_config(&new, conf)) {
		log_printf(LOG_WARNING, "storage: failed to reload from database");
		filter_data_free(&new);
		return false;
	}

	void *old = storage_swap_db(s, new);
	if (old)
		filter_data_free(&old);

	return true;
}

void
storage_free(struct storage **s) {
	if (!s || !*s)
		return;
	filter_data_free(&(*s)->fdi);
	free(*s);
	*s = NULL;
}
