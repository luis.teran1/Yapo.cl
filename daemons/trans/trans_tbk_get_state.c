#include "factory.h" 

static struct c_param parameters[] = {
	{"payment_group_id",	P_REQUIRED,	"v_integer"},
	{NULL, 0}
};

static struct factory_data data = {
        "tbk_get_state",
        FACTORY_NO_RES_CNTR | FACTORY_RES_SHORT_FORM
};

static const struct factory_action actions[] = {
        FA_SQL(NULL, "pgsql.master", "sql/tbk_get_state.sql", FATRANS_OUT),
        {0}
};

FACTORY_TRANS(tbk_get_state, parameters, data, actions);
