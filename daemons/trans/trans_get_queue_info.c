#include "factory.h"

static struct c_param parameters[] = {
        {"queue",    P_REQUIRED,  "v_queue_queue"},
       {NULL, 0}
};

FACTORY_TRANSACTION(get_queue_info, &config.pg_slave, "sql/get_queue_info.sql", NULL, parameters, FACTORY_EMPTY_RESULT_OK | FACTORY_RES_SHORT_FORM);
