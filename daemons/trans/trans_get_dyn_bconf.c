#include "bconfd_client.h"
#include "factory.h"

static struct c_param parameters[] = {
	{"key", P_REQUIRED, "v_string_isprint"},			
	{NULL}
};

extern struct bconf_node *bconf_root;


static int
get_dyn_bconf(struct cmd *cs, struct bpapi *ba) {

	const char *key = cmd_getval(cs, "key");

	struct bconfd_call bc = {0};
	bc.proto = PROTO_TRANS;
	bconfd_add_request_param(&bc, "type", "conf");

	/* Get the local bconf value */
	const char *source = "local";
	const char *value = bconf_get_string(bconf_root, key);

	int r = bconfd_load(&bc, bconf_get(bconf_root, "*.*.service_bconfd"));
	if (r != 0) {
		transaction_logprintf(cs->ts, D_WARNING, "get_dyn_bconf: problems with bconfd: %s", bc.errstr);
	} else if (bconf_get(bc.bconf, key)) {
		/* If we can reach bconfd, use its result instead */
		value = bconf_get_string(bc.bconf, key);
		source = "bconfd";
	}

	transaction_printf(cs->ts, "%s:%s\n", "key", key);
	transaction_printf(cs->ts, "%s:%s\n", "value", value);
	transaction_printf(cs->ts, "%s:%s\n", "source", source);
	bconf_free(&bc.bconf);

	return 0;
}

static struct factory_data data = {
	"controlpanel",
	FACTORY_NO_RES_CNTR
};

static const struct factory_action actions[] = {
	FA_FUNC(get_dyn_bconf),
	FA_DONE()
};

FACTORY_TRANS(get_dyn_bconf, parameters, data, actions);
