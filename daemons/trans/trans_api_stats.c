#include "factory.h"

struct validator v_api_external_ad_id[] = {
	VALIDATOR_REGEX("^[a-zA-Z0-9]*$", REG_EXTENDED, "ERROR_EXTERNAL_AD_ID_INVALID"),
	{0}
};

ADD_VALIDATOR(v_api_external_ad_id);

static struct c_param parameters[] = {
	{"link_type", P_REQUIRED, "v_link_type"},
	{"email", P_REQUIRED, "v_email"},
	{"external_ad_id", P_REQUIRED, "v_api_external_ad_id"},
	{NULL, 0}
};


static struct factory_data data = {
	"api_stats",
	FACTORY_EMPTY_RESULT_OK
};

static const struct factory_action actions[] = {
	FA_SQL("", "pgsql.slave", "sql/api_partner_slots.sql", FASQL_OUT),
	FA_SQL("", "pgsql.slave", "sql/api_user_slots.sql", FASQL_OUT),
	FA_SQL("", "pgsql.slave", "sql/api_get_ad_id.sql", FASQL_OUT),
	FA_SQL("", "pgsql.slave", "sql/api_ad_status.sql", FASQL_OUT),
	FA_SQL("", "pgsql.slave", "sql/api_refusal_reason.sql", FASQL_OUT),
	{0}
};

FACTORY_TRANS(api_stats, parameters, data, actions);
