#include "factory.h"

static struct c_param parameters[] = {
	{"event_name",    P_REQUIRED,  "v_event_name"},
	{"event",         P_REQUIRED,  "v_none"},
	{"token",         0,  "v_token"},
	{NULL, 0}
};

FACTORY_TRANSACTION(log_event, &config.pg_master, "sql/call_log_event.sql", NULL, parameters, FACTORY_NO_RES_CNTR);

