#include "factory.h"

static struct c_param parameters[] = {
	{"remote_addr",         P_REQUIRED, "v_ip"},
	{"remote_browser",      P_REQUIRED, "v_remote_browser"},
	{"watch_unique_id",	P_REQUIRED, "v_uuid"},
	{NULL, 0}
};

FACTORY_TRANSACTION(delete_watch_queries, &config.pg_master, "sql/call_delete_watch_queries.sql", NULL, parameters, FACTORY_NO_RES_CNTR|FACTORY_RES_SHORT_FORM);
