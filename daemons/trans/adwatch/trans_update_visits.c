#include "factory.h"

static struct c_param parameters[] = {
	{"day",      P_REQUIRED | P_MULTI, "v_string_isprint"   },
	{"vid",        P_REQUIRED | P_MULTI, "v_integer"     },
	{"n_visits",   P_REQUIRED | P_MULTI, "v_integer"     },
	{"n_entering_visits",   P_REQUIRED | P_MULTI, "v_integer"     },
	{"n_free_entering_visits",   P_REQUIRED | P_MULTI, "v_integer"     },
	{"n_entering_visits_from_click"   ,P_REQUIRED | P_MULTI, "v_integer"     },
	{"n_visits_from_click",   P_REQUIRED | P_MULTI, "v_integer"     },
	{"n_seconds_spent",   P_REQUIRED | P_MULTI, "v_integer"     },
	{NULL, 0}
};

FACTORY_TRANSACTION(update_visits, &config.pg_master, "sql/update_visits.sql", NULL, parameters, FACTORY_NO_RES_CNTR | FACTORY_EMPTY_RESULT_OK | FACTORY_RES_SHORT_FORM );

