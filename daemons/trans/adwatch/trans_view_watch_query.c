#include "factory.h"

static struct c_param parameters[] = {
	{"remote_addr",         P_REQUIRED, "v_ip"},
	{"remote_browser",      P_REQUIRED, "v_remote_browser"},
	{"watch_unique_id",	P_REQUIRED, "v_uuid"},
	{"watch_query_id",	P_REQUIRED, "v_id"},
	{NULL, 0}
};

FACTORY_TRANSACTION(view_watch_query, &config.pg_master, "sql/call_view_watch_query.sql", NULL, parameters, FACTORY_NO_RES_CNTR);
