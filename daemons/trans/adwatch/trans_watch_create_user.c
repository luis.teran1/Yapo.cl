#include "factory.h"

/* XXX This transaction is meant to be use for migrating from old adwatch only. */

static struct c_param parameters[] = {
	{"remote_addr",         P_REQUIRED, "v_ip"},
	{"remote_browser",      P_REQUIRED, "v_remote_browser"},
	{NULL, 0}
};

FACTORY_TRANSACTION_BCONF(watch_create_user, &config.pg_master, "sql/call_create_watch_user.sql", NULL, parameters, FACTORY_NO_RES_CNTR, "adwatch");
