#include "factory.h"

struct c_param parameters[] = {
	{"watch_unique_id", P_REQUIRED, "v_uuid"},
	{"watch_query_id", P_REQUIRED, "v_id"},
	{"remote_addr", P_REQUIRED, "v_ip"},
	{"remote_browser", 0, "v_remote_browser"},
	{NULL, 0}
};

FACTORY_TRANSACTION_BCONF(reserve_adwatch_activation_code, &config.pg_master, "sql/call_reserve_adwatch_activation_code.sql",
			  NULL, parameters, FACTORY_NO_RES_CNTR, "adwatch");
