#include "factory.h"

static struct c_param parameters[] = {
	{NULL, 0}
};

FACTORY_TRANSACTION_BCONF(delete_inactive_watch_users, &config.pg_master, "sql/call_delete_inactive_watch_users.sql",
		    NULL, parameters, (FACTORY_NO_RES_CNTR | FACTORY_EMPTY_RESULT_OK), "adwatch");
