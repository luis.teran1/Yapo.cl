#include "factory.h"

static struct c_param parameters[] = {
	{"remote_addr",         P_REQUIRED, "v_ip"},
	{"remote_browser",      P_REQUIRED, "v_remote_browser"},
	{"watch_unique_id",	0, "v_uuid"},
	{"list_id",		P_REQUIRED|P_MULTI, "v_id"},
	{NULL, 0}
};

FACTORY_TRANSACTION_BCONF(watch_ad, &config.pg_master, "sql/call_watch_ad.sql", NULL, parameters, FACTORY_NO_RES_CNTR, "adwatch");
