#include "factory.h"

struct validator v_watch_query_string[] = {
	{0}
};
ADD_VALIDATOR(v_watch_query_string);

static struct c_param parameters[] = {
	{"remote_addr",         P_REQUIRED, "v_ip"},
	{"remote_browser",      P_REQUIRED, "v_remote_browser"},
	{"watch_unique_id",	0, "v_uuid"},
	{"watch_query_id",	0, "v_id"},
	{"query_string",	P_REQUIRED, "v_watch_query_string"},
	{NULL, 0}
};

FACTORY_TRANSACTION_BCONF(watch_query, &config.pg_master, "sql/call_watch_query.sql", NULL, parameters, FACTORY_NO_RES_CNTR, "adwatch");
