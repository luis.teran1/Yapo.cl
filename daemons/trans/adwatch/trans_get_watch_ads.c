#include "factory.h"

static struct c_param parameters[] = {
	{"remote_addr",         P_REQUIRED, "v_ip"},
	{"remote_browser",      0, "v_remote_browser"},
	{"watch_unique_id",	P_REQUIRED, "v_uuid"},
	{NULL, 0}
};

FACTORY_TRANSACTION_BCONF(get_watch_ads, &config.pg_master, "sql/call_get_watch_ads.sql", NULL, parameters, FACTORY_NO_RES_CNTR | FACTORY_RES_NO_LOG, "adwatch");
