#include "factory.h"

static struct c_param parameters[] = {
	{NULL, 0}
};

static struct factory_data data = {
        "stores",
        FACTORY_NO_RES_CNTR | FACTORY_RES_SHORT_FORM
};

static const struct factory_action actions[]= {
	FA_SQL("", "pgsql.master", "sql/call_expire_stores.sql", FA_OUT),
	{0}
};

FACTORY_TRANS(expire_stores, parameters, data, actions);
