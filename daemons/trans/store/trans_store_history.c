#include "factory.h"

static struct c_param parameters[] = {
	{"store_id", P_REQUIRED, "v_store_id"},
	{NULL, 0}
};

static struct factory_data data = {
        "controlpanel",
        FACTORY_NO_RES_CNTR
};

static const struct factory_action actions[]= {
	FA_SQL("", "pgsql.slave", "sql/store_history.sql", FASQL_OUT),
	{0}
};

FACTORY_TRANS(store_history, parameters, data, actions);
