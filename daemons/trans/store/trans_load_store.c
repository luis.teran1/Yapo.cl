#include "factory.h"

static struct c_param parameters[] = {
	{"account_id;store_id;email", P_XOR, "v_account_id;v_store_id;v_email"},
	{NULL, 0}
};

static struct factory_data data = {
        "load_store",
        FACTORY_NO_RES_CNTR | FACTORY_RES_SHORT_FORM
};

static const struct factory_action actions[]= {
	FA_SQL("", "pgsql.master", "sql/load_store.sql", FA_OUT),
	{0}
};

FACTORY_TRANS(load_store, parameters, data, actions);
