#include "factory.h"
#include "bpapi.h"
#include <bconf.h>

extern struct bconf_node *bconf_root;

static struct c_param parameters[] = {
	{"name",	P_REQUIRED,		"v_string_isprint"},
	{"email",	P_REQUIRED,		"v_email_basic"},
	{"store_id",	P_REQUIRED,	"v_integer"},
	{"store_name",	P_REQUIRED,	"v_string_isprint"},
	{"date_start",	P_REQUIRED,	"v_date_long"},
	{"days",	P_REQUIRED,		"v_integer"},
	{NULL, 0}
};


static struct factory_data data = {
	"store_reminder_mail"
};

static const struct factory_action actions[] = {
        FA_MAIL("mail/store_reminder.mime"),
        {0}
};

FACTORY_TRANS(store_reminder_mail, parameters, data, actions);
