#include "factory.h"
#include "utils/refresh_session.h"

static struct c_param parameters[] = {
	{"account_id", P_REQUIRED, "v_account_id"},
	{"payment_group_id", 0, "v_integer"},
	{"token",P_OPTIONAL, "v_token"},
	{"remote_addr",  P_OPTIONAL, "v_ip"},
	{"remote_browser", P_OPTIONAL, "v_remote_browser"},
	{NULL, 0}
};

static struct factory_data data = {
        "create_store",
        FACTORY_NO_RES_CNTR | FACTORY_RES_SHORT_FORM
};

static const struct factory_action actions[]= {
	FA_SQL("", "pgsql.master", "sql/call_insert_store.sql", FASQL_OUT),
	FA_FUNC(refresh_account),
	{0}
};

FACTORY_TRANS(create_store, parameters, data, actions);
