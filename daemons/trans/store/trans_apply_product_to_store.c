#include "factory.h"
#include "utils/refresh_session.h"

static struct c_param parameters[] = {
	{"store_id", 		P_REQUIRED, "v_store_id"},
	{"expire_time", 	P_REQUIRED, "v_product_expire_time"},
	{"payment_group_id",P_OPTIONAL, "v_integer"},
	{"token",			P_OPTIONAL, "v_token"},
	{"remote_addr",  	P_OPTIONAL, "v_ip"},
	{"remote_browser", 	P_OPTIONAL, "v_remote_browser"},
	{NULL, 0}
};

static struct factory_data data = {
        "apply_product_to_store",
        FACTORY_NO_RES_CNTR | FACTORY_RES_SHORT_FORM
};

static const struct factory_action actions[]= {
	FA_SQL("", "pgsql.master", "sql/call_apply_product_to_store.sql", 0),
	FA_FUNC(refresh_account),
	{0}
};

FACTORY_TRANS(apply_product_to_store, parameters, data, actions);
