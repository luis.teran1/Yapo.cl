#include "factory.h"

static struct c_param parameters[] = {
	{"store_id", 		P_REQUIRED, "v_store_id"},
	{"status", 			P_REQUIRED, "v_store_states"}, /* there is another validator called v_store_status, I dont want to touch it */
	{"payment_group_id",P_OPTIONAL, "v_integer"},
	{"token",			P_OPTIONAL, "v_token"},
	{"remote_addr",  	P_OPTIONAL, "v_ip"},
	{"remote_browser", 	P_OPTIONAL, "v_remote_browser"},
	{NULL, 0}
};

static struct factory_data data = {
        "update_store_status",
        FACTORY_NO_RES_CNTR | FACTORY_RES_SHORT_FORM
};

static const struct factory_action actions[]= {
	FA_SQL("", "pgsql.master", "sql/call_update_store_status.sql", FASQL_OUT),
	{0}
};

FACTORY_TRANS(update_store_status, parameters, data, actions);
