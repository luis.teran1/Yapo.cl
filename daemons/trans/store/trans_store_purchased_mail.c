#include "factory.h"
#include "bpapi.h"
#include <bconf.h>

extern struct bconf_node *bconf_root;

static struct c_param parameters[] = {
	{"from",		0,			"v_string_isprint"},
	{"to",			P_REQUIRED,	"v_email_basic"},
	{"product_id",	P_REQUIRED,	"v_multiple_product_ids"},
	{"name",		P_REQUIRED,	"v_name"},
	{"store_id",	P_OPTIONAL,	"v_integer"},
	{"date_start",	P_OPTIONAL,	"v_date_long"},
	{NULL, 0}
};


static struct factory_data data = {
	"mail"
};

static const struct factory_action actions[] = {
        FA_MAIL("mail/store_ok.mime"),
        {0}
};

FACTORY_TRANS(store_purchased_mail, parameters, data, actions);
