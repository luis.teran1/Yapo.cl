#include "factory.h"

static struct c_param parameters[] = {
	{"store_id", P_REQUIRED, "v_store_id"},
	{"name", P_REQUIRED, "v_store_name"},
	{"info_text", P_REQUIRED, "v_info_text"},
	{"url", P_REQUIRED, "v_store_url"},
	{"phone", P_OPTIONAL, "v_phone_basic"},
	{"hide_phone", P_OPTIONAL, "v_bool"},
	{"address", P_OPTIONAL, "v_address_optional"},
	{"address_number", P_OPTIONAL, "v_store_address_number"},
	{"main_image", P_OPTIONAL, "v_string_isprint"},
	{"main_image_offset", P_OPTIONAL, "v_integer"},
	{"logo_image", P_OPTIONAL, "v_integer"},
	{"region", P_REQUIRED, "v_region"},
	{"commune", P_OPTIONAL, "v_commune_optional"},
	{"website_url", P_OPTIONAL, "v_store_external_url"},
	{"token", P_OPTIONAL, "v_token"},
	{"remote_addr",  	P_OPTIONAL, "v_ip"},
	{"remote_browser", 	P_OPTIONAL, "v_remote_browser"},
	{NULL, 0}
};

static struct factory_data data = {
        "edit_store",
        FACTORY_RES_SHORT_FORM
};

static const struct factory_action actions[]= {
	FA_SQL("", "pgsql.master", "sql/call_edit_store.sql", FA_OUT),
	{0}
};

FACTORY_TRANS(edit_store, parameters, data, actions);
