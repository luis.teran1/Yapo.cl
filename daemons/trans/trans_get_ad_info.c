#include "factory.h"

static struct c_param parameters[] = {
	{"ad_id;list_id",	P_XOR,	"v_ad_id;v_list_id"},
	{NULL, 0, 0}
};

FACTORY_TRANSACTION(get_ad_info, &config.pg_slave, "sql/get_ad_info.sql", NULL, parameters, FACTORY_RES_SHORT_FORM);
