#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <time.h>

#include "trans.h"
#include "util.h"
#include <ctemplates.h>
#include "validators.h"
#include <bconf.h>
#include <event.h>
#include "mail_log.h"
#include "trans_mail.h"
#include "filter.h"
#include "garb.h"
#include <log_event.h>

#define FLUSHMAIL_DONE  0
#define FLUSHMAIL_ERROR 1
#define FLUSHMAIL_INIT  2
#define FLUSHMAIL_GETDATA 3
#define FLUSHMAIL_SETSTATE_SPAM 4
#define FLUSHMAIL_SPAMCHECK 5
#define FLUSHMAIL_SPAM 6
#define FLUSHMAIL_RULE_SPAM 7
#define FLUSHMAIL_RULE_DELETE 8
#define FLUSHMAIL_SETSTATE_DONE 9
#define FLUSHMAIL_SEND 10
#define FLUSHMAIL_SENT 11
#define FLUSHMAIL_MSG_CENTER_SEND 12
#define FLUSHMAIL_PUBLISH_RMQ 13

struct flushmail_state {
	int state;
	int rule_id;
	struct cmd *cs;
	struct mail_data *md;
	char *matched_value;
	int is_blocked;
};

static struct c_param parameters[] = {
	{"application", P_REQUIRED, "v_flushmail_app"},
	{"mail_id",     P_REQUIRED, "v_integer"},
	{"copy",     	0, "v_integer"},
	{"nofilter",    0, "v_integer"},
	{"email",       0, "v_email_basic"},
	{"lang",	0, "v_lang"},
	{NULL, 0}
};

struct validator v_flushmail_app[] = {
	VALIDATOR_REGEX("^adreply|adreply_cc$", REG_EXTENDED, "ERROR_APPLICATION_INVALID"),
	{0}
};
ADD_VALIDATOR(v_flushmail_app);

static const char *flushmail_fsm(void *, struct sql_worker *, const char *);

extern struct bconf_node *bconf_root;

/*****************************************************************************
 * flushmail_done
 * Exit function.
 **********/
static void
flushmail_done(void *v) {
	struct flushmail_state *state = v;
	struct cmd *cs = state->cs;
	if (state->md)
		mail_free(state->md);
	if (state->matched_value)
		free(state->matched_value);
	free(state);
	cmd_done(cs);

	garb_table("mail_log");
}

/*****************************************************************************
 * bind_insert
 * Initialise template API before a call to sql_worker_template_query().
 **********/
static void
bind_insert(struct bpapi *ba, struct flushmail_state *state) {
	struct c_param *param;
	time_t now = time(NULL);
	struct tm tm;

	for (param = parameters; param->name; param++) {
		if (cmd_haskey(state->cs, param->name))
			bpapi_insert(ba, param->name, cmd_getval(state->cs, param->name));
	}

	localtime_r(&now, &tm);
	if (tm.tm_mon == 0 && tm.tm_mday <= 14) {
		/* Search in last year for some days in january. */
		bpapi_set_printf(ba, "schemas", "blocket_%d", tm.tm_year + 1900 - 1);
	}
	bpapi_set_printf(ba, "schemas", "blocket_%d", tm.tm_year + 1900);
}

static void
flushmail_mail_log_cb(struct mail_log *log, int mail_id, const char *errstr) {
	struct flushmail_state *state = log->data;

	if (errstr && !strcmp(errstr, "ERROR_SPAM")) {
		if(!state->is_blocked)
			state->state = FLUSHMAIL_SPAM;

		errstr = NULL;
	}

	free(log);
	flushmail_fsm(state, NULL, errstr);
}

static void
flushmail_send_cb(struct mail_data *md, void *arg, int status) {
	struct flushmail_state *state = arg;
	const char *errstr = NULL;

	transaction_logprintf(state->cs->ts, D_DEBUG,"Ad reply status: %d", status);
	if (status != 0) {
		errstr = "TRANS_MAIL_ERROR";
	} else if ( strcmp(cmd_getval(state->cs, "application"), "adreply") == 0 ) {

		//add ad reply stats logging
		char *strtok_state = NULL;
		char *to = xstrdup(mail_get_param(state->md, "to"));
		strtok_r(to, "@", &strtok_state);
		const char *domain = strtok_r(NULL,"@", &strtok_state);
		const char *safe_enabled = bconf_value(bconf_vget(bconf_root, "*.*.safe_settings.safe.1", domain, "value", NULL));

		if(!safe_enabled || strcmp("enable:true", safe_enabled) != 0){

			struct bpapi_vtree_chain vtree;
			bconf_vtree(&vtree, bconf_get(bconf_root, "*.*.common.statpoints"));
			stat_log(&vtree, "MAIL", mail_get_param(md, "list_id"));
			transaction_logprintf(state->cs->ts, D_DEBUG,"Ad reply counted | list_id:%s", mail_get_param(md, "list_id"));
			vtree_free(&vtree);
		}
		free(to);
	}
	syslog_ident("mail_queue", "SENT: [%s] (%s), %s, %s, %s, %s, %s", mail_get_param(state->md, "list_id"),
			mail_get_param(state->md, "to"), mail_get_param(state->md, "name"),
			mail_get_param(state->md, "from"), mail_get_param(state->md, "remote_addr"),
			mail_get_param(state->md, "subject"), mail_get_param(state->md, "body"));
	state->md = NULL;

	flushmail_fsm(state, NULL, errstr);
}

static void
flushmail_filter_cb(struct filter_data *fdata, 
		    int status, 
		    int rule_id, 
		    const char *rule_name, 
		    const char *action, 
		    const char *target, 
		    const char *matched_column,
		    const char *matched_value,
		    int combi_match,
		    const char *errstr) {
	struct flushmail_state *state = fdata->data;
	
	if (status == FILTER_ERROR || status == FILTER_DONE) {
		free(fdata);
		flushmail_fsm(state, NULL, errstr);
	} else {
		if (action && !errstr) {
			if (!state->rule_id) {
				state->rule_id = rule_id;
				state->matched_value = xstrdup(matched_value);
			}
			if (strcmp(action, "spamfilter") == 0) {
				if (state->state != FLUSHMAIL_RULE_DELETE) {
					state->state = FLUSHMAIL_RULE_SPAM;
					syslog_ident("mail_queue", "mail %s (ad %s) sent to manual queue",
							cmd_getval(state->cs, "mail_id"),
							mail_get_param(state->md, "list_id"));
				}
			} else {
				syslog_ident("mail_queue", "BLOCKED: (rule %d, word \"%s\", id %s): %s, %s, %s, %s, %s", 
					     rule_id, matched_value,
					     mail_get_param(state->md, "list_id"), mail_get_param(state->md, "name"), 
					     mail_get_param(state->md, "from"), mail_get_param(state->md, "remote_addr"),
					     mail_get_param(state->md, "subject"), mail_get_param(state->md, "body"));
				state->state = FLUSHMAIL_RULE_DELETE;
			}
		}
	}

}


/*****************************************************************************
 * flushmail_fsm
 * State machine
 **********/
static const char *
flushmail_fsm(void *v, struct sql_worker *worker, const char *errstr) {
	struct flushmail_state *state = v;
	struct cmd *cs = state->cs;
	struct bpapi ba;

	/* 
	 * General error handling for db errors.
	 */
	if (errstr) {
			transaction_logprintf(cs->ts, D_ERROR, "flushmail_fsm statement failed (%d, %s)", 
                                      state->state, errstr);
		if (strstr(errstr, "TRANS_MAIL_ERROR"))
			cs->status = "TRANS_MAIL_ERROR";
		else
			cs->status = "TRANS_DATABASE_ERROR";
		cs->message = xstrdup(errstr);
		state->state = FLUSHMAIL_ERROR;
		if (worker) {
			/* Turn of final callback, if set. */
			sql_worker_set_wflags(worker, 0);
			return "ROLLBACK";
		}
	}

	while (1) {
		transaction_logprintf(cs->ts, D_DEBUG, "flushmail_fsm(%p, %p, %p), state=%d", 
				      v, worker, errstr, state->state);

		switch (state->state) {
		case FLUSHMAIL_INIT:
			/* next state */
			state->state = FLUSHMAIL_GETDATA;

			/* Initialise the templateparse structure */
			BPAPI_INIT(&ba, NULL, pgsql);
			bind_insert(&ba, state);

			/* Register SQL query with our flushmail_fsm() as callback function */
			/* Make sure to switch to slave if read-only query. */
			sql_worker_template_query(&config.pg_master, 
						  "sql/flushmail_getmail.sql", &ba, 
						  flushmail_fsm, state, cs->ts->log_string);

			/* Clean-up */
			bpapi_output_free(&ba);
			bpapi_simplevars_free(&ba);
			bpapi_vtree_free(&ba);

			/* Return to libevent */
			return NULL;

		case FLUSHMAIL_GETDATA:
		{
			int cols;
			int i;
			struct filter_data *fdata = NULL;
			const char *filter_app;

			if (!worker->next_row(worker)) {
				state->state = FLUSHMAIL_ERROR;
				state->cs->error = "ERROR_MAIL_NOT_FOUND";
				continue;
			}

			state->md = zmalloc(sizeof (*state->md));

			cols = worker->fields(worker);
			for (i = 0; i < cols; i++) {
				if (!worker->is_null(worker, i)) {
					if (cmd_haskey(cs, "email") && 
					    strcmp(worker->field_name(worker, i), "to") == 0) {
						mail_insert_param(state->md, "to",
								  cmd_getval(cs, "email"));
					} else if (cmd_haskey(cs, "copy") && 
						   strcmp(worker->field_name(worker, i), "from") == 0) {
						
						/* WTF ?? what happen here ?  nothing ? */

					} else {
						mail_insert_param(state->md, worker->field_name(worker, i),
								  worker->get_value(worker, i));
					}
				}
			}

			if (cmd_haskey(cs, "nofilter")) {
				state->state = FLUSHMAIL_SPAMCHECK;
				continue;
			}

			if (strcmp(worker->get_value_byname(worker, "state"), "reg") != 0) {
				state->state = FLUSHMAIL_ERROR;
				cs->error = "ERROR_ALREADY_FLUSHED";
				continue;
			}

			/* Filter data */
			fdata = filter_init(fdata);

			filter_add_element(fdata, "name", worker->get_value_byname(worker, "name"));
			filter_add_element(fdata, "email", worker->get_value_byname(worker, "from"));
			filter_add_element(fdata, "subject", worker->get_value_byname(worker, "subject"));
			filter_add_element(fdata, "body", worker->get_value_byname(worker, "body"));
			filter_add_element(fdata, "remote_addr", worker->get_value_byname(worker, "remote_addr"));

			fdata->data = state;
			fdata->cb = flushmail_filter_cb;

			state->state = FLUSHMAIL_SPAMCHECK;

			filter_app = bconf_value(bconf_vget(bconf_root, "*", "*", "common",
							    "filter_app_remaps", cmd_getval(cs, "application"), NULL));

			if (!filter_app)
				filter_app = cmd_getval(cs, "application");

			filter_data(fdata, filter_app);

			return NULL;
		}
		case FLUSHMAIL_RULE_SPAM:
			BPAPI_INIT(&ba, NULL, pgsql);
			bind_insert(&ba, state);
			bpapi_insert(&ba, "state", "manual");
			if (state->rule_id)
				bpapi_set_int(&ba, "rule_id", state->rule_id);
			if (state->matched_value)
				bpapi_insert(&ba, "reason", state->matched_value);
			sql_worker_template_query_flags(&config.pg_master, SQLW_FINAL_CALLBACK,
						  "sql/flushmail_setstate.sql", &ba,
						  flushmail_fsm, state, cs->ts->log_string);

			/* Clean-up */
			bpapi_output_free(&ba);
			bpapi_simplevars_free(&ba);
			bpapi_vtree_free(&ba);

			transaction_printf(cs->ts, "manual:rule %d:%s\n", state->rule_id, state->matched_value);

                        state->state = FLUSHMAIL_SETSTATE_SPAM;
			return NULL;

		case FLUSHMAIL_RULE_DELETE:
		{
			BPAPI_INIT(&ba, NULL, pgsql);
			bind_insert(&ba, state);
			bpapi_insert(&ba, "state", "blocked");
			bpapi_set_int(&ba, "rule_id", state->rule_id);
			bpapi_insert(&ba, "reason", state->matched_value);
			sql_worker_template_query_flags(&config.pg_master, SQLW_FINAL_CALLBACK,
						  "sql/flushmail_setstate.sql", &ba, 
						  flushmail_fsm, state, cs->ts->log_string);

			/* Clean-up */
			bpapi_output_free(&ba);
			bpapi_simplevars_free(&ba);
			bpapi_vtree_free(&ba);

			transaction_printf(cs->ts, "rejected:rule %d:%s\n", state->rule_id, state->matched_value);

                        state->state = FLUSHMAIL_SETSTATE_SPAM;
			state->is_blocked = 1;
			return NULL;
		}

		case FLUSHMAIL_SETSTATE_SPAM:
		{
			if (worker->sw_state == RESULT_DONE) {
				state->state = FLUSHMAIL_SPAMCHECK;
				continue;
			}
			return NULL;
		}

		case FLUSHMAIL_SPAMCHECK:
		{
			struct mail_log *log;
			struct bconf_node *node;

			/* If matched a rule already, skip the send/reject. */
			if (state->rule_id) {
				state->state = FLUSHMAIL_DONE;
			} else {
				/* Get the local bconf value */
				const char *msg_center_conf = "*.trans.flushmail.msg_center.send_enabled";
				char *res = transaction_internal_output_printf("cmd:get_dyn_bconf\nkey:%s\ncommit:1\n", msg_center_conf);

				/* Get Msg center skip behaviour for Partners */
				const char *partner = mail_get_param(state->md, "partner_name");
				int skip_msg_center = 0;

				if (partner != NULL && strcmp(partner, "") != 0) {
					skip_msg_center = bconf_vget_int(bconf_root, "*", "*", "partners", partner, "skip_msg_center", NULL);
				}

				if (res && strstr(res, "value:1") && !skip_msg_center) {
					transaction_logprintf(state->cs->ts, D_INFO, "FLUSHMAIL_MSG_CENTER_SEND IS ACTIVE");
					state->state = FLUSHMAIL_MSG_CENTER_SEND;
				} else {
					transaction_logprintf(state->cs->ts, D_INFO, "FLUSHMAIL_MSG_CENTER_SEND IS NOT ACTIVE, USING DEFAULT BEHAVIOR");
					state->state = FLUSHMAIL_SEND;
				}
				free(res);
			}
			log = zmalloc(sizeof (*log));
			log->type = cmd_getval(cs, "application");
			log->email = mail_get_param(state->md, "to");
			log->remote_addr = mail_get_param(state->md, "remote_addr");
			log->list_id = atoi(mail_get_param(state->md, "list_id"));
			if (!cmd_haskey(cs, "copy"))
				log->check_only = 1;
			log->cb = flushmail_mail_log_cb;
			log->data = state;

			node = bconf_vget(bconf_root, "*", log->type, "spam", NULL);
			log->spam_counts[MAIL_SPAM_SEND] = bconf_get_int(node, "send_count");
			log->spam_counts[MAIL_SPAM_RECV] = bconf_get_int(node, "recv_count");
			log->spam_counts[MAIL_SPAM_AD] = bconf_get_int(node, "ad_count");
			log->spam_minutes = bconf_get_int(node, "minutes");

			log_mail(log);

			return NULL;
		}

		case FLUSHMAIL_MSG_CENTER_SEND:
		{
			//if there is a copy email, the email copy will be send inmediatly
			if (strcmp("adreply_cc", cmd_getval(cs, "application")) == 0) {
				transaction_logprintf(state->cs->ts, D_DEBUG, "FLUSHMAIL_MSG_CENTER_SEND: Email copy will be sent to the remitent");
				state->state = FLUSHMAIL_SEND;
				continue;
			} else {
				state->state = FLUSHMAIL_PUBLISH_RMQ;
			}

			transaction_logprintf(state->cs->ts, D_DEBUG, "FLUSHMAIL_MSG_CENTER_SEND");
			{
				transaction_logprintf(state->cs->ts, D_DEBUG, "FLUSHMAIL_MSG_CENTER_SEND: getting email data.");
				const char *to = mail_get_param(state->md, "to");
				const char *name = mail_get_param(state->md, "name");
				const char *from = mail_get_param(state->md, "from");
				const char *item_id = mail_get_param(state->md, "list_id");

				if (!to || !name || !from || !item_id) {
					state->cs->error = "FLUSHMAIL_MSG_CENTER_SEND: Error getting email data.";
					state->state = FLUSHMAIL_DONE;
					continue;
				}
				
				struct bpapi ba;
				BPAPI_INIT(&ba, NULL, pgsql);
				bpapi_insert(&ba, "to", to);
				bpapi_insert(&ba, "name", name);
				bpapi_insert(&ba, "reply_to", from);
				bpapi_insert(&ba, "list_id", item_id);

				transaction_logprintf(state->cs->ts, D_DEBUG, "FLUSHMAIL_MSG_CENTER_SEND: Calling sql query.");
				sql_worker_template_query(&config.pg_master, "sql/call_get_and_set_uuids_data.sql", &ba, flushmail_fsm, state, cs->ts->log_string);

				bpapi_output_free(&ba);
				bpapi_simplevars_free(&ba);
				bpapi_vtree_free(&ba);
            }	
			return NULL;
		}

		case FLUSHMAIL_PUBLISH_RMQ:

			transaction_logprintf(state->cs->ts, D_DEBUG, "FLUSHMAIL_PUBLISH_RMQ: Getting sql query values.");
			const char *sender_id = NULL;
			const char *recipient_id = NULL;
			const char *partner_name = NULL;

			if (worker->next_row(worker)) {
				sender_id = worker->get_value_byname(worker, "sender_uuid");
				recipient_id = worker->get_value_byname(worker, "recipient_uuid");
				partner_name = worker->get_value_byname(worker, "recipient_name");
				state->cs->status = "TRANS_OK";
			}

			if (recipient_id == NULL) {
				transaction_logprintf(state->cs->ts, D_INFO, "FLUSHMAIL_PUBLISH_RMQ: Recipient id not found.");
				state->cs->status = "TRANS_ERROR";
				state->cs->error = "ERROR_FLUSHMAIL_PUBLISH_RMQ_RECEIVER_ACCOUNT_NOT_FOUND";
			}

			if (sender_id == NULL) {
				transaction_logprintf(state->cs->ts, D_INFO, "FLUSHMAIL_PUBLISH_RMQ: Sender id not found.");
				state->cs->status = "TRANS_ERROR";
				state->cs->error = "ERROR_FLUSHMAIL_PUBLISH_RMQ_SENDER_ACCOUNT_NOT_FOUND";
			}
			if (partner_name == NULL) {
				transaction_logprintf(state->cs->ts, D_INFO, "FLUSHMAIL_PUBLISH_RMQ: Partner name not found.");
				state->cs->status = "TRANS_ERROR";
				state->cs->error = "ERROR_FLUSHMAIL_PUBLISH_RMQ_RECEIVER_NAME_NOT_FOUND";
			}
			if (strcmp(state->cs->status, "TRANS_OK") == 0) {
				struct command_bpapi jsoncba;
				command_bpapi_init(&jsoncba, cs, CMD_BPAPI_HOST, NULL);
				struct buf_string *msg_center_json = BPAPI_OUTPUT_DATA(&jsoncba.ba);

				transaction_logprintf(state->cs->ts, D_DEBUG, "FLUSHMAIL_PUBLISH_RMQ: Inserting msg-center  params");
				
				const char *item_id = mail_get_param(state->md, "list_id");
				const char *message_text = mail_get_param(state->md, "body");
				const char *body = mail_get_param(state->md, "body");
				const char *subject = mail_get_param(state->md, "subject");
				const char *user_name = mail_get_param(state->md, "name");
				const char *user_email = mail_get_param(state->md, "from");
				const char *partner_email = mail_get_param(state->md, "to");
				const char *phone = mail_get_param(state->md, "phone");
				const char *user_social_id = mail_get_param(state->md, "user_social_id");
				const char *partner_social_id = mail_get_param(state->md, "partner_social_id");

				if (!item_id || !message_text || !body || !subject || !user_name || !user_email || !partner_name || !partner_email) {
					transaction_logprintf(state->cs->ts, D_ERROR, "FLUSHMAIL_PUBLISH_RMQ: Missing email data, message not sended to RabbitMQ.");
					continue;
				}

				char user_profile[100] = "";
				if (user_social_id)
					snprintf(user_profile, 100, "https://graph.facebook.com/%s/picture?width=200&height=200", user_social_id);

				char partner_profile[100] = "";
				if (partner_social_id)
					snprintf(partner_profile, 100, "https://graph.facebook.com/%s/picture?width=200&height=200", partner_social_id);

				bpapi_insert(&jsoncba.ba, "senderId", sender_id);
				bpapi_insert(&jsoncba.ba, "itemId", item_id);
				bpapi_insert(&jsoncba.ba, "recipientId", recipient_id);
				bpapi_insert(&jsoncba.ba, "messageText", message_text);
				bpapi_insert(&jsoncba.ba, "body", body);
				bpapi_insert(&jsoncba.ba, "subject", subject);
				bpapi_insert(&jsoncba.ba, "userName", user_name);
				bpapi_insert(&jsoncba.ba, "userEmail", user_email);
				//this variable is currently not in use, but needed to avoid the call to "user_info".
				bpapi_insert(&jsoncba.ba, "userProfile", user_profile);
				bpapi_insert(&jsoncba.ba, "partnerName", partner_name);
				bpapi_insert(&jsoncba.ba, "partnerEmail", partner_email);
				//this variable is currently not in use, but needed to avoid the call to "user_info".
				bpapi_insert(&jsoncba.ba, "partnerProfile", partner_profile);
				
				if (phone)
					bpapi_insert(&jsoncba.ba, "phone", phone);

				call_template(&jsoncba.ba, "trans/adreply_msg_center.json");
				transaction_logprintf(state->cs->ts, D_DEBUG, "finished Calling json template");
		
				const char *exchange = bconf_get_string(bconf_root, "*.*.common.msg_center.rabbitmq.exchange");
				const char *routing_key = bconf_get_string(bconf_root, "*.*.common.msg_center.rabbitmq.routing_key");
				const char *virtual_host = bconf_get_string(bconf_root, "*.*.common.msg_center.rabbitmq.vhost");

				transaction_logprintf(state->cs->ts, D_DEBUG, "FLUSHMAIL_PUBLISH_RMQ: Loading RabbitMQ seetings exchange=%s, routing key=%s, vhost=%s", exchange,routing_key,virtual_host);

				char *res = transaction_internal_output_printf(
					"cmd:rabbitmq_send\n"
					"exchange:%s\n"
					"routing_key:%s\n"
					"virtual_host:%s\n"
					"message:%s\n"
					"commit:1\n",
					exchange,
					routing_key,
					virtual_host,
					msg_center_json->buf
				);

				command_bpapi_free(&jsoncba);

				if (!res || strstr(res, "TRANS_ERROR")) {
					transaction_logprintf(state->cs->ts, D_ERROR,"FLUSHMAIL_PUBLISH_RMQ: TRANS ERROR, CHANGING STATE TO FLUSHMAIL_ERROR.");
					state->state = FLUSHMAIL_ERROR;
					state->cs->status = "TRANS_ERROR";
				} else {
					transaction_logprintf(state->cs->ts, D_DEBUG,"FLUSHMAIL_PUBLISH_RMQ: Message successfully sended to RabbitMQ.");
					state->state = FLUSHMAIL_SENT;

					transaction_logprintf(state->cs->ts, D_DEBUG,"FLUSHMAIL_PUBLISH_RMQ: Adreply counter update.");
					struct bpapi_vtree_chain vtree;
					bconf_vtree(&vtree, bconf_get(bconf_root, "*.*.common.statpoints"));
					stat_log(&vtree, "MAIL", mail_get_param(state->md, "list_id"));
					transaction_logprintf(state->cs->ts, D_DEBUG,"Ad reply counted | list_id:%s", mail_get_param(state->md, "list_id"));
					vtree_free(&vtree);
				}
				free(res);
			} else {
				transaction_logprintf(state->cs->ts, D_ERROR,"FLUSHMAIL_PUBLISH_RMQ: TRANS ERROR, CHANGING STATE TO FLUSHMAIL_ERROR.");
				state->state = FLUSHMAIL_ERROR;
			}
			continue;

		case FLUSHMAIL_SPAM:
			BPAPI_INIT(&ba, NULL, pgsql);
			bind_insert(&ba, state);
			bpapi_insert(&ba, "state", "manual");
			/* Set all emails to manual from the same IP */
			bpapi_insert(&ba, "flood", "1");
			bpapi_insert(&ba, "remote_addr", mail_get_param(state->md, "remote_addr"));
			sql_worker_template_query_flags(&config.pg_master, SQLW_FINAL_CALLBACK,
						  "sql/flushmail_setstate.sql", &ba, 
						  flushmail_fsm, state, cs->ts->log_string);

			/* Clean-up */
			bpapi_output_free(&ba);
			bpapi_simplevars_free(&ba);
			bpapi_vtree_free(&ba);

			transaction_printf(cs->ts, "manual:flood spam\n");

                        state->state = FLUSHMAIL_SETSTATE_DONE;
			return NULL;


		case FLUSHMAIL_SEND:
			transaction_logprintf(state->cs->ts, D_DEBUG, "FLUSHMAIL_SEND");
			state->md->command = state;
			state->md->callback = flushmail_send_cb;
			state->md->log_string = cs->ts->log_string;

			if (cmd_haskey(cs, "application"))
				mail_insert_param(state->md, "application", cmd_getval(cs, "application"));

			/* CHUS228: ad mail id to mail template */
			mail_insert_param(state->md, "mail_queue_id", cmd_getval(state->cs, "mail_id"));
			if (!cmd_haskey(cs, "copy")) {
				state->state = FLUSHMAIL_SENT;
			} else {
				mail_insert_param(state->md, "copy", "1");
				state->state = FLUSHMAIL_DONE;
			}

			mail_send(state->md, mail_get_param(state->md, "template_name"), cmd_getval(cs, "lang"));
			return NULL;

		case FLUSHMAIL_SENT:
			BPAPI_INIT(&ba, NULL, pgsql);
			bind_insert(&ba, state);
			bpapi_insert(&ba, "state", "sent");
			sql_worker_template_query_flags(&config.pg_master, SQLW_FINAL_CALLBACK,
						  "sql/flushmail_setstate.sql", &ba, 
						  flushmail_fsm, state, cs->ts->log_string);
			/* Clean-up */
			bpapi_output_free(&ba);
			bpapi_simplevars_free(&ba);
			bpapi_vtree_free(&ba);

			state->state = FLUSHMAIL_SETSTATE_DONE;
			return NULL;

		case FLUSHMAIL_SETSTATE_DONE:
			if (worker->sw_state == RESULT_DONE) {
				state->state = FLUSHMAIL_DONE;
				continue;
			}
			return NULL;

		case FLUSHMAIL_DONE:
			flushmail_done(state);
			return NULL;

		case FLUSHMAIL_ERROR:
			transaction_logprintf(cs->ts, D_ERROR, "Error state");
			flushmail_done(state);
			return NULL;

		default:
			/* MAJOR STATE ERROR */
			cs->status = "TRANS_ERROR";
			transaction_logprintf(cs->ts, D_ERROR, "Unknown state (%d)", 
					      state->state);
			flushmail_done(state);
			return NULL;

		}
	}
}

/*****************************************************************************
 * flushmail
 * Main function that initialise a state-structure and calls the state machine.
 **********/
static void
flushmail(struct cmd *cs) {
	transaction_logprintf(cs->ts, D_DEBUG, "Start Flushmail transaction");
	struct flushmail_state *state;
	state = zmalloc(sizeof(*state));
	state->cs = cs;
	state->state = FLUSHMAIL_INIT;

	flushmail_fsm(state, NULL, NULL);
}

ADD_COMMAND(flushmail,		/* Name of command */
	NULL,
	flushmail,				/* Main function name */
	parameters,				/* Parameters struct */
	"Flushmail command"		/* Command description */
);
