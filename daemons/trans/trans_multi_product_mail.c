#include "factory.h"
#include "bpapi.h"
#include <bconf.h>

extern struct bconf_node *bconf_root;

static struct c_param parameters[] = {
	{"from",		0,		"v_string_isprint"},
	{"to",			0,		"v_string_isprint"},
	{"doc_type",		P_REQUIRED,	"v_string_isprint"},
	{"product_ids",		P_REQUIRED,	"v_multiple_product_ids"},
	{"ad_prices",		P_REQUIRED,	"v_multiple_product_prices"},
	{"creds_balance",	P_OPTIONAL,	"v_integer"},
	{"creds_added",		P_OPTIONAL,	"v_integer"},
	{NULL, 0}
};


static struct factory_data data = {
	"mail"
};

static const struct factory_action actions[] = {
	FA_MAIL("mail/products_ok.mime"),
	{0}
};

FACTORY_TRANS(multi_product_mail, parameters, data, actions);
