#include "factory.h"
#include "bpapi.h"
#include <bconf.h>
#include "similar_text.h"

extern struct bconf_node *bconf_root;

static int find_duplicates(struct cmd *, struct bpapi *);

static struct c_param parameters[] = {
	{"ad_id",  P_REQUIRED, "v_id"},
	{"ad_subject",  P_REQUIRED, "v_subject"},
	{"ad_body",  P_REQUIRED, "v_body"},
	{NULL, 0}
};

static struct factory_data data = {
	"factory_test",
	FACTORY_RES_CNTR | FACTORY_EMPTY_RESULT_OK
};

static const struct factory_action actions[] = {
	FA_SQL("", "pgsql.master", "sql/call_find_associated_ads.sql", 0),
	FA_FUNC(find_duplicates),
	{0}
};

static int is_duplicate(const char *adsubject, const char *adbody, const char *suspect_subject, const char *suspect_body, struct cmd *cs) {

	int mindupperc = bconf_get_int(bconf_root, "*.controlpanel.modules.adqueue.settings.duplicate_min_percent");
	int subjperc = similar_text(adsubject, suspect_subject, 0);
	int bodyperc = similar_text(adbody, suspect_body, 0);
	int totperc = (subjperc + bodyperc) / 2;

	transaction_logprintf(cs->ts, D_INFO, "SIMILAR: adsubject : %s - adbody : %s", adsubject, adbody);
	transaction_logprintf(cs->ts, D_INFO, "SIMILAR: suspect_subject : %s - suspect_subject : %s", suspect_subject, suspect_body);
	transaction_logprintf(cs->ts, D_INFO, "SIMILAR: mindupperc : %d - subjperj %d - boduperc: %d - totperc : %d", mindupperc, subjperc, bodyperc, totperc);

	if (subjperc == 100) {
		transaction_logprintf(cs->ts, D_INFO, "SIMILAR: subjects are equal");
		return 1;
	} else {
		if (totperc >= mindupperc) {
			transaction_logprintf(cs->ts, D_INFO, "SIMILAR: total percentage is higher than the minimum duplicate percentage");
			return 1;
		}else{
			transaction_logprintf(cs->ts, D_INFO, "SIMILAR: total percentage is lower than the minimum duplicate percentage");
			return 0;
		}
	}
}

static int find_duplicates(struct cmd *cs, struct bpapi *ba) {
	int length = bpapi_length(ba, "o_ad_id");
	const char *subject = cmd_getval(cs, "ad_subject");
	const char *body = cmd_getval(cs, "ad_body");
	int i, has_duplicates = 0;

	transaction_logprintf(cs->ts, D_INFO, "SIMILAR: The count of similar ads is: %d", length);

	for (i = 0; i < length && !has_duplicates; i++) {
		const char *asubject = bpapi_get_element(ba, "o_subject", i);
		const char *abody = bpapi_get_element(ba, "o_body", i);
		has_duplicates = is_duplicate(subject, body, asubject, abody, cs);
	}

	transaction_printf(cs->ts, "has_duplicates:%d\n", has_duplicates);
	return 0;
}


/*****************************************************************************
 * adqueues
 * Main function that initialise a state-structure and calls the state machine.
 **********/

FACTORY_TRANS(similar_ads, parameters, data, actions);
