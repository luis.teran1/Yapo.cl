#include <stdio.h>
#include <time.h>

#include "command.h"
#include "trans.h"
#include "util.h"
#include "bconf.h"
#include "validators.h"
#include "trans_mail.h"
#include "db.h"
#include "hash.h"
#include <ctemplates.h>
#include "trans_newad.h"
#include "log_event.h"
#include "trans_at.h"

extern struct bconf_node *bconf_root;

typedef enum {
	REJECTED_DONE,
	REJECTED_ERROR,
	REJECTED_INIT,
	REJECTED_CHECK,
	REJECTED_MAIL,
	REJECTED_UNLOAD,
	REJECTED_REVIEW,
	REJECTED_REMOVE_UPSELLING_PRODUCTS,
} state_t;

static const char *state_lut[] = {
	"REJECTED_DONE",
	"REJECTED_ERROR",
	"REJECTED_INIT",
	"REJECTED_CHECK",
	"REJECTED_MAIL",
	"REJECTED_UNLOAD",
	"REJECTED_REVIEW",
	"REJECTED_REMOVE_UPSELLING_PRODUCTS",
};

static const char * rejected_fsm(void *, struct sql_worker *, const char *);

struct media_list_entry {
	const char *name;
	TAILQ_ENTRY (media_list_entry) list;
};

struct rejected_state {
	state_t state;
	struct cmd *cs;
	char *sql;
	struct bpapi ba;
	int state_cnt;
	struct timer_instance *ti;
	struct hash_table *params;
	TAILQ_HEAD(,media_list_entry) media;
	int media_checks_pending;
	int media_check_errors;
	char *action_id;
	char *action_type;
	int category;
	char *type;
	int company_ad;
	char *upselling_action_id;
};

static struct c_param parameters[] = {
	{"ad_id",          0,          "v_ad_id"},
	{"action_id",      P_REQUIRED, "v_action_id"},
	{"remote_addr",    P_REQUIRED, "v_ip"},
	{"remote_browser", 0,          "v_remote_browser"},
	{"token",          P_REQUIRED, "v_token:adqueue"},
	{NULL}
};


static void rejected_done(struct rejected_state *state) {
	struct cmd *cs = state->cs;

	bpapi_output_free(&state->ba);
	bpapi_simplevars_free(&state->ba);
	bpapi_vtree_free(&state->ba);

	if (state->params)
		hash_table_free(state->params);

	free(state->action_id);
	free(state->action_type);
	free(state->type);
	free(state->upselling_action_id);
	free(state);

	cmd_done(cs);
}

static int
send_mail(struct cmd *cs, struct rejected_state *state) {
	struct internal_cmd *cmd = zmalloc(sizeof (*cmd));
	const char *mail_type = "rejected_wrong_category";
	transaction_logprintf(cs->ts, D_DEBUG, "Sending mailtype(%s)", mail_type);

	/* Mail */
	char *mailcmd;
	xasprintf(&mailcmd,
		"cmd:admail\n"
		"commit:1\n"
		"ad_id:%s\n"
		"action_id:%s\n"
		"mail_type:%s\n",
		cmd_getval(cs, "ad_id"),
		state->action_id ?: cmd_getval(cs, "action_id"),
		mail_type
	);

	transaction_logprintf(cs->ts, D_DEBUG, "Send mail now");
	cmd->cmd_string = mailcmd;

	return transaction_internal(cmd);
}

static void
bind_rejected(struct bpapi *ba, struct rejected_state *state) {
	struct cmd *cs = state->cs;
	struct c_param *param;

	for (param = parameters; param->name; param++) {
		if (cmd_haskey (cs, param->name))
			bpapi_insert(ba, param->name, cmd_getval(cs, param->name));
	}
	if (cmd_haskey(cs, "token"))
		bpapi_insert(ba, "token", cmd_getval(cs, "token"));
	bpapi_insert(ba, "action", "refuse");
	bpapi_insert(ba, "filter_name", "scarface_rejected");
	bpapi_insert(ba, "reason", "scarface_rejected");
	bpapi_insert(ba, "refusal_text", "Categor�a incorrecta");
}

static const char *
rejected_fsm(void *v, struct sql_worker *worker, const char *errstr) {
	struct rejected_state *state = v;
	struct cmd *cs = state->cs;
	struct bpapi ba;

	transaction_logprintf(cs->ts, D_DEBUG, "rejected_fsm state = %s (%d)", state_lut[state->state], state->state);

	if (state->sql) {
		free(state->sql);
		state->sql = NULL;
	}

	if (state->state == REJECTED_ERROR) {
		rejected_done(state);
		return NULL;
	}
	/*
	 * General error handling
	 */
	if (errstr) {
		if (strstr(errstr, "ERROR_INVALID_STATE")) {
			cs->status = "TRANS_ERROR";
			cs->error = "ERROR_INVALID_STATE";
		} else if (strstr(errstr, "ERROR_NO_SUCH_ACTION")) {
			cs->status = "TRANS_ERROR";
			cs->error = "ERROR_NO_SUCH_ACTION";
		} else {
			cs->status = "TRANS_DATABASE_ERROR";
			cs->message = xstrdup(errstr);
		}
		state->state = REJECTED_ERROR;
	}

	while (state->state != REJECTED_DONE) {
#define TIMER_STATE(st) if (state->ti != NULL) timer_end(state->ti, NULL); state->ti = timer_start(state->cs->ti, "REJECTED_"#st)
		if (state->ti) {
			timer_end(state->ti, NULL);
			state->ti = NULL;
		}

		switch ((state_t)state->state) {
		case REJECTED_INIT:
			TIMER_STATE(INIT);
			state->state = REJECTED_UNLOAD;

			/* Modify price, in case it comes in UFs, before sending it to the DB */
			format_price(cs, NULL);

			BPAPI_INIT(&ba, NULL, pgsql);
			bpapi_insert(&ba, "ad_id", cmd_getval(cs, "ad_id"));
			bpapi_insert(&ba, "action_id", cmd_getval(cs, "action_id"));

			sql_worker_template_query_flags(&config.pg_master, SQLW_FINAL_CALLBACK,
							"sql/load_adparams.sql", &ba,
							rejected_fsm, state, cs->ts->log_string);

			bpapi_output_free(&ba);
			bpapi_simplevars_free(&ba);
			bpapi_vtree_free(&ba);
			return NULL;

		case REJECTED_UNLOAD:
			TIMER_STATE(UNLOAD);
			if (worker->sw_state == RESULT_DONE) {
				state->state = REJECTED_REVIEW;
				sql_worker_set_wflags(worker, 0);
				continue;
			}

			if (!state->params) {
				state->params = hash_table_create(16, free);
				hash_table_free_keys(state->params, 1);
			}

			while (worker->next_row(worker)) {
				int cols = worker->fields(worker);
				if (cols == 2) {
					hash_table_insert(state->params, xstrdup(worker->get_value(worker, 0)), -1, xstrdup(worker->get_value(worker, 1)));
					transaction_logprintf(cs->ts, D_DEBUG, "TRANS_REVIEW: param %s = %s ",
						worker->get_value(worker, 0),
						worker->get_value(worker, 1)
					);
				}
			}

			return NULL;

		case REJECTED_REVIEW:
			TIMER_STATE(REVIEW);
			BPAPI_INIT(&ba, NULL, pgsql);

			bind_rejected(&ba, state);
			if (state->category && !cmd_haskey(cs, "category")) {
				bpapi_set_int(&ba, "category", state->category);
			}
			state->state = REJECTED_CHECK;
			sql_worker_template_query(&config.pg_master,
						  "sql/review.sql", &ba,
						  rejected_fsm, state, cs->ts->log_string);

			bpapi_output_free(&ba);
			bpapi_simplevars_free(&ba);
			bpapi_vtree_free(&ba);
			return NULL;

		case REJECTED_CHECK:
			TIMER_STATE(CHECK);
			state->state = REJECTED_REMOVE_UPSELLING_PRODUCTS;
			
			/* Redir info */
			if (worker->next_row(worker)) {
				const char *redir_code = worker->get_value_byname(worker, "redir_code");
				const char *remote_addr = worker->get_value_byname(worker, "remote_addr");
				const char *action_id = worker->get_value_byname(worker, "action_id");
				const char *action_type = worker->get_value_byname(worker, "action_type");
				const char *upselling_action_id = worker->get_value_byname(worker, "upselling_action_id");
				const char *remove_upselling_products = worker->get_value_byname(worker, "remove_upselling_products");

				transaction_logprintf(state->cs->ts, D_INFO, "QUEUE upselling_action_id: %s", upselling_action_id);
				transaction_logprintf(state->cs->ts, D_INFO, "QUEUE remove_upselling_products: %s", remove_upselling_products);

				state->company_ad = worker->is_true_byname(worker, "company_ad");

				if (remove_upselling_products && !strcmp(remove_upselling_products, "t"))
					state->upselling_action_id = xstrdup(upselling_action_id);

				if (action_type)
			 		state->action_type = xstrdup(action_type);

				syslog_ident("redir", "%s approved_ad %s",
					     redir_code ?: bconf_get_string(host_bconf, "trans.redir_unknown_code"),
					     remote_addr);
				transaction_logprintf(state->cs->ts, D_INFO, "REDIR: %s approved_ad %s",
					     redir_code ?: bconf_get_string(host_bconf, "trans.redir_unknown_code"),
					     remote_addr);
				if (action_id && action_id[0])
					state->action_id = xstrdup(action_id);
			}
			continue;

		case REJECTED_REMOVE_UPSELLING_PRODUCTS:
			TIMER_STATE(REMOVE_UPSELLING_PRODUCTS);
			state->state = REJECTED_MAIL;

			if (state->upselling_action_id) {
				BPAPI_INIT(&ba, NULL, pgsql);
				bpapi_insert(&ba, "ad_id", cmd_getval(cs, "ad_id"));
				bpapi_insert(&ba, "upselling_action_id", state->upselling_action_id);

				sql_worker_template_query("pgsql.queue",
					"sql/cancel_upselling_on_refuse.sql", &ba,
					rejected_fsm, state, cs->ts->log_string
				);

				bpapi_output_free(&ba);
				bpapi_simplevars_free(&ba);
				bpapi_vtree_free(&ba);
				return NULL;
			}
			continue;

		case REJECTED_MAIL:
		{
			char *link_type;
			TIMER_STATE(MAIL);

			// Drain older queries
			while (worker->next_row(worker));

			if (state->params &&
			    (link_type = hash_table_search(state->params, "link_type", -1, NULL)) &&
			    strncmp(link_type, "spidered", 8) == 0) {
				state->state = REJECTED_DONE;
				continue;
			}

			if (state->params &&
				(link_type = hash_table_search(state->params, "link_type", -1, NULL)) &&
				!strcmp(link_type, "spidered_balcao")) {
				state->state = REJECTED_DONE;
				continue;
			}

			if (cmd_haskey(cs, "do_not_send_mail") && atoi(cmd_getval(cs, "do_not_send_mail")) == 1) {
				state->state = REJECTED_DONE;
				continue;
			}

			if (send_mail(cs, state)) {
				transaction_logprintf(cs->ts, D_ERROR, "Failed to send email");
				state->state = REJECTED_ERROR;
				continue;
			}

			state->state = REJECTED_DONE;
			continue;
		}

		case REJECTED_ERROR:
			TIMER_STATE(ERROR);
			transaction_logprintf(cs->ts, D_ERROR, "Error in review fsm");
			rejected_done(state);
			return NULL;

		default:
			transaction_logprintf(cs->ts, D_ERROR, "Major state error in review fsm (%d)", state->state);
			rejected_done(state);
			return NULL;
		}
	}

	if (state->ti) {
		timer_end(state->ti, NULL);
		state->ti = NULL;
	}

	rejected_done(state);
	return NULL;
}

static void
scarface_rejected(struct cmd *cs) {
	struct rejected_state *state = zmalloc(sizeof (*state));
	state->cs = cs;
	state->state = REJECTED_INIT;
	rejected_fsm(state, NULL, NULL);
}

ADD_COMMAND(scarface_rejected, NULL, scarface_rejected, parameters, NULL);
