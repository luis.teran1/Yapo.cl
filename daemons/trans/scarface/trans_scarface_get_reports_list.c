#include "factory.h"

static struct c_param parameters[] = {
	{"report_type",	0,	"v_string_isprint"},
	{"list_id", 0, "v_integer"},
	{"admin_id",0, "v_integer"},
	{"skip_ad",0, "v_integer"},
	{"ad_id", 0, "v_integer"},
	{NULL, 0, 0}
};

FACTORY_TRANSACTION(scarface_get_reports_list, &config.pg_master, "sql/scarface_get_reports_list.sql", NULL, parameters, FACTORY_EMPTY_RESULT_OK);

