#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <time.h>

#include "trans.h"
#include "trans_at.h"
#include "util.h"
#include <ctemplates.h>
#include "validators.h"
#include <bconf.h>
#include <event.h>

#define SCARFACE_MULTIPLE_RESOLVE_DONE  0
#define SCARFACE_MULTIPLE_RESOLVE_ERROR 1
#define SCARFACE_MULTIPLE_RESOLVE_INIT  2
#define SCARFACE_MULTIPLE_RESOLVE_SENDMAIL_BEGIN 3
#define SCARFACE_MULTIPLE_RESOLVE_SENDMAIL 4
#define SCARFACE_MULTIPLE_RESOLVE_GET_USERS_TO_WARN 5
#define SCARFACE_MULTIPLE_RESOLVE_SEND_WARNING 6
#define SCARFACE_MULTIPLE_RESOLVE_NOTIFY_OWNER 7
#define SCARFACE_MULTIPLE_RESOLVE_SEND_NOTIFY_OWNER 8
#define SCARFACE_MULTIPLE_RESOLVE_SENDMAIL_BEGIN_CUSTOM_MESSAGE 9
#define SCARFACE_MULTIPLE_RESOLVE_SENDMAIL_CUSTOM_MESSAGE 10


extern struct bconf_node* bconf_root;

struct report {
        TAILQ_ENTRY(report) entry;
        char *report_id;
        char *abuse_type;
};

struct scarface_multiple_resolve_state {
	int state;
	TAILQ_HEAD(,report) reports_list;
        struct report *current_report;
        char *abuse_type;
	char *emails_ptr;
	char *langs_ptr;
	char *report_ids_ptr;
	char *reporter_ids_ptr;
	struct cmd *cs;
};


static struct c_param parameters[] = {
	{"list_id",	 	P_REQUIRED,	"v_integer"},	
	{"report_id",	 	P_REQUIRED,	"v_integer"},	
	{"token",	 	P_REQUIRED,  	"v_token"},	
	{"resolution",		0,		"v_resolution"},
	{"notify_type",		0,		"v_integer"},
	{"remote_addr",		P_REQUIRED,	"v_ip"},
	{"remote_browser",		P_REQUIRED,	"v_remote_browser"},
	{"score",		0,		"v_score"},
	{NULL, 0}
};


static const char *scarface_multiple_resolve_fsm(void *, struct sql_worker *, const char *);

/*****************************************************************************
 * scarface_multiple_resolve_done
 * Exit function.
 **********/
static void
scarface_multiple_resolve_done(void *v) {
	struct scarface_multiple_resolve_state *state = v;
	struct cmd *cs = state->cs;


	struct report *ad_report;
        while ((ad_report = TAILQ_FIRST(&state->reports_list)) != NULL) {
                TAILQ_REMOVE(&state->reports_list, ad_report, entry);

                free(ad_report->report_id);
                free(ad_report->abuse_type);
                free(ad_report);
        }

	free(state);
	cmd_done(cs);
}

/*****************************************************************************
 * bind_insert
 * Initialise template API before a call to sql_worker_template_query().
 **********/
static void
bind_insert(struct bpapi *ba, struct scarface_multiple_resolve_state *state) {
	struct c_param *param;

	for (param = parameters; param->name; param++) {
		if (cmd_haskey (state->cs, param->name) ) {
			bpapi_insert(ba, param->name, cmd_getval(state->cs, param->name));
		}
	}
}

/*****************************************************************************
 * scarface_multiple_resolve_fsm
 * State machine
 **********/
static const char *
scarface_multiple_resolve_fsm(void *v, struct sql_worker *worker, const char *errstr) {
	struct scarface_multiple_resolve_state *state = v;
	struct cmd *cs = state->cs;
	struct bpapi ba;
	char *email, *report_id, *lang, *reporter_id;
	struct internal_cmd *cmd;

	/*
	 * General error handling for db errors.
	 */
	if (errstr) {
                transaction_logprintf(cs->ts, D_ERROR, "scarface_multiple_resolve_fsm statement failed (%d, %s)",
                                      state->state,
                                      errstr);
		sql_worker_set_wflags(worker, 0);
					
                cs->status = "TRANS_DATABASE_ERROR";
                cs->message = xstrdup(errstr);
		state->state = SCARFACE_MULTIPLE_RESOLVE_ERROR;
        }

	while (1) {
		transaction_logprintf(cs->ts, D_DEBUG, "scarface_multiple_resolve_fsm(%p, %p, %p), state=%d",
				      v, worker, errstr, state->state);

		switch (state->state) {

		case SCARFACE_MULTIPLE_RESOLVE_INIT:

			if ((strcmp(cmd_getval(state->cs, "resolution"), "accept_and_notify_owner") == 0 || strcmp(cmd_getval(state->cs, "resolution"), "reject_and_notify_reporter") == 0) && !cmd_getval(state->cs, "notify_type")) {
				state->state = SCARFACE_MULTIPLE_RESOLVE_ERROR;
				continue;
			}
			if (strcmp(cmd_getval(state->cs, "resolution"), "reject_and_notify_reporter") == 0) {
				state->state = SCARFACE_MULTIPLE_RESOLVE_SENDMAIL_BEGIN_CUSTOM_MESSAGE;
			} else {
				state->state = SCARFACE_MULTIPLE_RESOLVE_SENDMAIL_BEGIN;
			}
			BPAPI_INIT(&ba, NULL, pgsql);
			bind_insert(&ba, state);

			sql_worker_template_query(&config.pg_master,
						"sql/call_scarface_multiple_resolve.sql", &ba,
						scarface_multiple_resolve_fsm, state, cs->ts->log_string);

			bpapi_output_free(&ba);
			bpapi_simplevars_free(&ba);
			bpapi_vtree_free(&ba);

			state->emails_ptr = NULL;
			state->langs_ptr = NULL;
			state->report_ids_ptr = NULL;
			state->reporter_ids_ptr = NULL;

			return NULL;

		case SCARFACE_MULTIPLE_RESOLVE_SENDMAIL_BEGIN:

			if (worker->next_row(worker)) {
				state->state = SCARFACE_MULTIPLE_RESOLVE_SENDMAIL;
			} else {
				//Check if we should warn the users who has replied the ad
				if (strcmp(cmd_getval(state->cs, "resolution"), "accept_delete_and_warn") == 0) {
					state->state = SCARFACE_MULTIPLE_RESOLVE_GET_USERS_TO_WARN;
				} else if (strcmp(cmd_getval(state->cs, "resolution"), "accept_and_notify_owner") == 0) {
					state->state = SCARFACE_MULTIPLE_RESOLVE_NOTIFY_OWNER;
				} else {
					state->state = SCARFACE_MULTIPLE_RESOLVE_DONE;
				}
			}

			continue;


		case SCARFACE_MULTIPLE_RESOLVE_SENDMAIL_BEGIN_CUSTOM_MESSAGE:
			if (worker->next_row(worker)) {
				state->state = SCARFACE_MULTIPLE_RESOLVE_SENDMAIL_CUSTOM_MESSAGE;
			} else {
				state->state = SCARFACE_MULTIPLE_RESOLVE_DONE;
			}
			continue;

		case SCARFACE_MULTIPLE_RESOLVE_SENDMAIL:
		
			cmd = zmalloc(sizeof (*cmd));

			if (!state->emails_ptr) {
				email = strtok_r((char *)worker->get_value_byname(worker, "o_emails"), ",", &state->emails_ptr);
				lang = strtok_r((char *)worker->get_value_byname(worker, "o_langs"), ",", &state->langs_ptr);
			} else {
				lang = strtok_r(NULL, ",", &state->langs_ptr);
				email = strtok_r(NULL, ",", &state->emails_ptr);
			}

			if (!state->report_ids_ptr)
				report_id = strtok_r((char *)worker->get_value_byname(worker, "o_report_ids"), ",", &state->report_ids_ptr);
			else
				report_id = strtok_r(NULL, ",", &state->report_ids_ptr);

			if (email && report_id) {
				state->state = SCARFACE_MULTIPLE_RESOLVE_SENDMAIL;
			} else {
				// Check if we should warn the users who has replied the ad
				if (strcmp(cmd_getval(state->cs, "resolution"), "accept_delete_and_warn") == 0) {
					state->state = SCARFACE_MULTIPLE_RESOLVE_GET_USERS_TO_WARN;
				} else if (strcmp(cmd_getval(state->cs, "resolution"), "accept_and_notify_owner") == 0) {
					state->state = SCARFACE_MULTIPLE_RESOLVE_NOTIFY_OWNER;
				} else {
					state->state = SCARFACE_MULTIPLE_RESOLVE_DONE;
				}
				continue;
			}

			xasprintf(&cmd->cmd_string,
					"cmd:admail\n"
					"commit:1\n"
					"mail_type:scarface_report\n"
					"reporter_email:%s\n"
					"list_id:%s\n"
					"abuse_type:%s\n"
					"resolution:%s\n"
					"report_id:%s\n"
			  		"report_lang:%s\n"
					"ad_id:%s\n",
					email,
					cmd_getval(state->cs, "list_id"),
					worker->get_value(worker, 0),
					cmd_getval(state->cs, "resolution"),
					report_id,
					lang,
					worker->get_value(worker, 2));

			transaction_internal(cmd);

			continue;


		case SCARFACE_MULTIPLE_RESOLVE_SENDMAIL_CUSTOM_MESSAGE:
		
			cmd = zmalloc(sizeof (*cmd));

			if (!state->emails_ptr) {
				email = strtok_r((char *)worker->get_value_byname(worker, "o_emails"), ",", &state->emails_ptr);
				lang = strtok_r((char *)worker->get_value_byname(worker, "o_langs"), ",", &state->langs_ptr);
				reporter_id = strtok_r((char *)worker->get_value_byname(worker, "o_reporter_ids"), ",", &state->reporter_ids_ptr);
			} else {
				lang = strtok_r(NULL, ",", &state->langs_ptr);
				email = strtok_r(NULL, ",", &state->emails_ptr);
				reporter_id = strtok_r(NULL, ",", &state->reporter_ids_ptr);
			}

			if (!state->report_ids_ptr)
				report_id = strtok_r((char *)worker->get_value_byname(worker, "o_report_ids"), ",", &state->report_ids_ptr);
			else
				report_id = strtok_r(NULL, ",", &state->report_ids_ptr);

			if (email && report_id) {
				state->state = SCARFACE_MULTIPLE_RESOLVE_SENDMAIL_CUSTOM_MESSAGE;
			} else {
				state->state = SCARFACE_MULTIPLE_RESOLVE_DONE;
				continue;
			}

			xasprintf(&cmd->cmd_string,
					"cmd:admail\n"
					"commit:1\n"
					"mail_type:scarface_reject_custom\n"
					"reporter_email:%s\n"
					"list_id:%s\n"
					"abuse_type:%s\n"
					"resolution:%s\n"
					"report_id:%s\n"
			  		"report_lang:%s\n"
			  		"notify_type:%s\n"
					"ad_id:%s\n"
					"uid:%s\n",
					email,
					cmd_getval(state->cs, "list_id"),
					worker->get_value(worker, 0),
					cmd_getval(state->cs, "resolution"),
					report_id,
					lang,
					cmd_getval(state->cs, "notify_type"),
					worker->get_value(worker, 2),
					reporter_id);

			transaction_internal(cmd);

			continue;

		
		case SCARFACE_MULTIPLE_RESOLVE_GET_USERS_TO_WARN:
			state->state = SCARFACE_MULTIPLE_RESOLVE_SEND_WARNING;

			BPAPI_INIT(&ba, NULL, pgsql);
			bpapi_insert(&ba, "list_id", cmd_getval(state->cs, "list_id"));

			sql_worker_template_query(&config.pg_slave,
						"sql/call_scarface_multiple_resolve_get_users_to_warn.sql", &ba,
						scarface_multiple_resolve_fsm, state, cs->ts->log_string);

			bpapi_output_free(&ba);
			bpapi_simplevars_free(&ba);
			bpapi_vtree_free(&ba);

			return NULL;

		case SCARFACE_MULTIPLE_RESOLVE_SEND_WARNING:
			state->state = SCARFACE_MULTIPLE_RESOLVE_DONE;

			while(worker->next_row(worker)) {
				transaction_internal_printf(TRANSACTION_INTERNAL_SYNC_CB, NULL,
						"cmd:admail\n"
						"mail_type:scarface_warn\n"
						"commit:1\n"
						"list_id:%s\n"
						"email:%s\n"
						"ad_id:%s\n",
						cmd_getval(state->cs, "list_id"),
						worker->get_value(worker, 0),
						worker->get_value(worker, 1)
					);
			}
			continue;

		case SCARFACE_MULTIPLE_RESOLVE_NOTIFY_OWNER:
			state->state = SCARFACE_MULTIPLE_RESOLVE_SEND_NOTIFY_OWNER;

			BPAPI_INIT(&ba, NULL, pgsql);
			bpapi_insert(&ba, "list_id", cmd_getval(state->cs, "list_id"));

			sql_worker_template_query(&config.pg_slave,
						"sql/scarface_multiple_resolve_get_owner.sql", &ba,
						scarface_multiple_resolve_fsm, state, cs->ts->log_string);

			bpapi_output_free(&ba);
			bpapi_simplevars_free(&ba);
			bpapi_vtree_free(&ba);

			return NULL;

		case SCARFACE_MULTIPLE_RESOLVE_SEND_NOTIFY_OWNER:
			state->state = SCARFACE_MULTIPLE_RESOLVE_DONE;

			while(worker->next_row(worker)) {
				transaction_internal_printf(NULL, NULL,
						"cmd:admail\n"
						"mail_type:scarface_owner\n"
						"commit:1\n"
						"list_id:%s\n"
						"subject:%s\n"
						"name:%s\n"
						"email:%s\n"
						"notify_type:%s\n"
						"ad_id:%s\n",
						cmd_getval(state->cs, "list_id"),
						worker->get_value(worker, 0),
						worker->get_value(worker, 1),
						worker->get_value(worker, 2),
						cmd_getval(state->cs, "notify_type"),
						worker->get_value(worker, 3)
					);
			}
			continue;

		case SCARFACE_MULTIPLE_RESOLVE_DONE:
			scarface_multiple_resolve_done(state);
			return NULL;

		case SCARFACE_MULTIPLE_RESOLVE_ERROR:
			transaction_logprintf(cs->ts, D_ERROR, "Error state");
			scarface_multiple_resolve_done(state);
			return NULL;

		default:
			/* MAJOR STATE ERROR */
			cs->status = "TRANS_ERROR";
			transaction_logprintf(cs->ts, D_ERROR, "Unknown state (%d)",
					      state->state);
			return NULL;

		}
	}
}

/*****************************************************************************
 * scarface_multiple_resolve
 * Main function that initialise a state-structure and calls the state machine.
 **********/
static void
scarface_multiple_resolve(struct cmd *cs) {
	struct scarface_multiple_resolve_state *state;
	state = zmalloc(sizeof(*state));
	state->cs = cs;
	state->state = SCARFACE_MULTIPLE_RESOLVE_INIT;

	scarface_multiple_resolve_fsm(state, NULL, NULL);
}

ADD_COMMAND(scarface_multiple_resolve,     /* Name of command */
		NULL,
            scarface_multiple_resolve,     /* Main function name */
            parameters,  /* Parameters struct */
            "Resolve several scarface reports");/* Command description */

