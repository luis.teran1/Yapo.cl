#include "factory.h"
#include "validators.h"

static struct c_param parameters[] = {
	{"currency",	0,	"v_string_isprint"},
	{"ad_id",    P_REQUIRED,  "v_id"},
        {"category",P_REQUIRED, "v_category_no_params"},
        {"company_ad",P_REQUIRED, "v_integer"},
        {"type",P_REQUIRED, "v_type"},
        {"price",P_REQUIRED, "v_currency_check"},
        {"remote_addr",P_REQUIRED, "v_ip"},
        {"remote_browser",0, "v_remote_browser"},
        {"token",P_REQUIRED, "v_token"},
        {NULL, 0, 0}
};

static const struct factory_data data = {
	"scarface_edit",
	FACTORY_EMPTY_RESULT_OK
};

static const struct factory_action actions[] = {
	FA_FUNC(format_price),
	FA_SQL("", &config.pg_master, "sql/call_scarface_edit.sql", FASQL_OUT),
	FA_DONE()
};

FACTORY_TRANS(scarface_edit, parameters, data, actions);
