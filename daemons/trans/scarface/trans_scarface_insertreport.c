#include "trans.h"
#include "validators.h"
#include <ctemplates.h>
#include <bconf.h>
#include <sha1.h>
#include "trans_mail.h"

#define SCARFACE_INSERTREPORT_DONE  0
#define SCARFACE_INSERTREPORT_ERROR 1
#define SCARFACE_INSERTREPORT_INIT  2
#define SCARFACE_INSERTREPORT_INSERT 3
#define SCARFACE_INSERTREPORT_RESOLVE 4
#define SCARFACE_INSERTREPORT_MAIL 5
#define SCARFACE_INSERTREPORT_ASKINFO 6
#define SCARFACE_INSERTREPORT_GETINFO 7
#define SCARFACE_INSERTREPORT_ASKINFO_SAVE 8

struct scarface_insertreport_state {
	int state;
	struct cmd *cs;
	struct sql_worker *sw;
	const char *reporter_email;
	const char *abuse_type;
	const char *resolution;
	const char *ad_id;
	const char *uid;
	const char *report_id;
	const char *lang;
};

extern struct bconf_node *bconf_root;

static struct c_param insert_params[] = {
	{"abuse_type",	 	P_REQUIRED,  	"v_integer"},	
	{"lang",		P_REQUIRED,	"v_none"},
	{"uid", 		0,		"v_uid"},
	{NULL, 0}
};

static struct c_param getinfo_params[] = {
	{"report_id",	 	P_REQUIRED,  	"v_integer"},	
	{"email",		0,		"v_email_not_required"},
	{"lang",		0,	"v_none"},
	{NULL, 0}
};

static struct c_param update_params[] = {
	{"report_id",	 	0,	  	"v_integer"},	
	{"abuse_type",	 	P_REQUIRED,  	"v_integer"},	
	{"uid", 		P_REQUIRED,	"v_uid"},
	{"email",	 	0,  	 	"v_email_not_required"},	
	{"message",	 	P_REQUIRED,	"v_abusemessage"},	
	{NULL, 0}
};

static struct c_param resolve_params[] = {
	{"report_id",	 	P_REQUIRED,	  	"v_integer"},	
	{"resolution",		0,		"v_resolution"},
	{"score",		0,		"v_score"},
	{"token",	 	P_REQUIRED,	  	"v_token"},	
	{"remote_addr",		P_REQUIRED,		"v_none"},
	{"remote_browser",	P_REQUIRED,		"v_none"},
	{NULL, 0}
};

static struct c_param askinfo_params[] = {
	{"report_id",	 	P_REQUIRED,	  	"v_integer"},	
	{"token",	 	P_REQUIRED,	  	"v_token"},	
	{"remote_addr",		P_REQUIRED,		"v_none"},
	{"remote_browser",	P_REQUIRED,		"v_none"},
	{"admin_name",	 	P_REQUIRED,		"v_none"},	
	{"admin_message",	P_REQUIRED,		"v_none"},	
	{NULL, 0}
};
static int
v_action_report_func(struct cmd_param *param, const char *arg) {
	struct c_param *extra_params = NULL;

	if (strcmp(param->value, "insert") == 0)
		extra_params = insert_params;
	else if (strcmp(param->value, "update") == 0)
		extra_params = update_params;
	else if (strcmp(param->value, "resolve") == 0)
		extra_params = resolve_params;
	else if (strcmp(param->value, "askinfo") == 0)
		extra_params = askinfo_params;
	else if (strcmp(param->value, "getinfo") == 0)
		extra_params = getinfo_params;

	if (extra_params) {
		while (extra_params->name)
			cmd_extra_param(param->cs, extra_params++);
	}
	return 0;
}

struct validator v_abusemessage[] = {
	VALIDATOR_REQUIRED("ERROR_MESSAGE_MISSING"),
	VALIDATOR_LEN(-1, 600, "ERROR_BODY_TOO_LONG"),
	{0}
};
ADD_VALIDATOR(v_abusemessage);

struct validator v_action_report[] = {
	VALIDATOR_REQUIRED("ERROR_ACTION_TYPE_MISSING"),
	VALIDATOR_REGEX("^insert$|^update$|^resolve$|^askinfo$|^getinfo$", REG_EXTENDED|REG_NOSUB, "ERROR_ACTION"),
	VALIDATOR_FUNC(v_action_report_func),
	{0}
};
ADD_VALIDATOR(v_action_report);

struct validator v_score[] = {
	VALIDATOR_REGEX("^1$", REG_EXTENDED|REG_NOSUB, "ERROR_SCORE_PARAM"),
	{0}
};
ADD_VALIDATOR(v_score);

struct validator v_resolution[] = {
	VALIDATOR_REQUIRED("ERROR_RESOLUTION_MISSING"),
	VALIDATOR_REGEX("^accept$|^reject$|^accept_and_delete$|^accept_delete_and_warn|^accept_and_notify_owner|^reject_and_notify_reporter$", REG_EXTENDED|REG_NOSUB, "ERROR_RESOLUTION"),
	{0}
};
ADD_VALIDATOR(v_resolution);


static struct c_param parameters[] = {
	{"action",	 	P_REQUIRED,	"v_action_report"},	
	{"list_id",	 	P_REQUIRED,	"v_integer"},	
	{NULL, 0}
};

//static const char *scarface_insertreport_fsm(void *, struct sql_worker *, const char *);

static void
bind_insert(struct bpapi *ba, struct scarface_insertreport_state *state) {
	const char *action;
	struct c_param *param;
	for (param = parameters; param->name; param++) {
		if (cmd_haskey (state->cs, param->name))
			bpapi_insert(ba, param->name, cmd_getval(state->cs, param->name));
	}

	action = cmd_getval(state->cs, "action");
	if(strcmp(action,"insert") == 0) {
		for (param = insert_params; param->name; param++) {
			if (cmd_haskey (state->cs, param->name))
				bpapi_insert(ba, param->name, cmd_getval(state->cs, param->name));
		}	
	} else if(strcmp(action,"update") == 0) {
		for (param = update_params; param->name; param++) {
			if (cmd_haskey (state->cs, param->name))
				bpapi_insert(ba, param->name, cmd_getval(state->cs, param->name));
		}

	} else if(strcmp(action,"resolve") == 0) {
		for (param = resolve_params; param->name; param++) {
			if (cmd_haskey (state->cs, param->name))
				bpapi_insert(ba, param->name, cmd_getval(state->cs, param->name));
		}
	} else if(strcmp(action,"askinfo") == 0) {
		for (param = askinfo_params; param->name; param++) {
			if (cmd_haskey (state->cs, param->name))
				bpapi_insert(ba, param->name, cmd_getval(state->cs, param->name));
		}

	} else if(strcmp(action,"getinfo") == 0) {
		for (param = getinfo_params; param->name; param++) {
			if (cmd_haskey (state->cs, param->name))
				bpapi_insert(ba, param->name, cmd_getval(state->cs, param->name));
		}

	}
}


static int  
send_mail(struct cmd *cs, struct scarface_insertreport_state *state) {

	struct internal_cmd *cmd = zmalloc(sizeof (*cmd));

	/* Mail */
	transaction_logprintf(cs->ts, D_DEBUG, "Creating email to send");

	/*
	 * Don't send refuse mail with no reason.
	 */

	const char *action = cmd_getval(state->cs, "action");
	const char *list_id = cmd_getval(state->cs, "list_id");
	const char *report_id = cmd_getval(state->cs, "report_id");
	const char *admin_message = cmd_getval(state->cs, "admin_message");

	if (strcmp(action, "resolve") == 0) {
		xasprintf(&cmd->cmd_string,
			  "cmd:admail\n"
			  "commit:1\n"
			  "ad_id:%s\n"
			  "list_id:%s\n"
			  "resolution:%s\n"
			  "reporter_email:%s\n"
			  "abuse_type:%s\n"
			  "report_id:%s\n"
			  "report_lang:%s\n"
			  "uid:%s\n"
			  "mail_type:scarface_report\n",
			  state->ad_id,
			  list_id,	
			  state->resolution,
			  state->reporter_email,
			  state->abuse_type,
		          state->report_id,
			  state->lang,
			  state->uid);

	} else if (strcmp(action, "askinfo") == 0) {
		char * admin_message_final = NULL;

		if (admin_message) {
			if (strchr(admin_message, '\n'))
				xasprintf(&admin_message_final, "blob:%zu:admin_message\n%s\n", strlen(admin_message), admin_message);
			else
				xasprintf(&admin_message_final, "admin_message:%s\n", admin_message);
		}

		xasprintf(&cmd->cmd_string,
			  "cmd:admail\n"
			  "commit:1\n"
			  "ad_id:%s\n"
			  "reporter_email:%s\n"
			  "abuse_type:%s\n"
			  "report_lang:%s\n"
			  "uid:%s\n"
			  "mail_type:scarface_askinfo\n"
			  "report_id:%s\n"
			  "%s"
			  "list_id:%s\n",
			  state->ad_id,
			  state->reporter_email,
			  state->abuse_type,
			  state->lang,
			  state->uid,
			  report_id,
			  admin_message_final?admin_message_final:"",
			  list_id);

		if (admin_message_final)
			free(admin_message_final);
	} else {
		return 1;
	}
	return transaction_internal(cmd);
}

static void
scarface_insertreport_done(void *v) {
	struct scarface_insertreport_state *state = v;
	struct cmd *cs = state->cs;
	free(state);
	cmd_done(cs);
}

static const char *
scarface_insertreport_fsm(void *v, struct sql_worker *worker, const char *errstr) {
	struct scarface_insertreport_state *state = v;
	struct cmd *cs = state->cs;
        struct bpapi ba;

	/* 
	 * General error handling for db errors.
	 */
	if (errstr) {
                transaction_logprintf(cs->ts, D_ERROR, "scarface_insertreport_fsm statement failed (%d, %s)", 
                                      state->state,
                                      errstr);
                cs->status = "TRANS_DATABASE_ERROR";
                cs->message = xstrdup(errstr);
		state->state = SCARFACE_INSERTREPORT_ERROR;
                if (state->sw) {
			/* Turn of final callback, if set. */
			sql_worker_set_wflags(state->sw, 0);
                        sql_worker_newquery(state->sw, QUERY_ROLLBACK, NULL);
                        return NULL;
                } else if (worker) {
			/* Turn of final callback, if set. */
			sql_worker_set_wflags(worker, 0);
			return "ROLLBACK";
		}
        }

	while (1) {
		transaction_logprintf(cs->ts, D_DEBUG, "scarface_insertreport_fsm(%p, %p, %p), state=%d", 
				      v, worker, errstr, state->state);

		switch (state->state) {
		case SCARFACE_INSERTREPORT_INIT:
			/* next state */

			/* Initialise the templateparse structure */
			BPAPI_INIT(&ba, NULL, pgsql);
			bind_insert(&ba, state);

			/* Register SQL query with our scarface_insertreport_fsm() as callback function */
			/* Make sure to switch to slave if read-only query. */
			
			const char *action = cmd_getval(state->cs, "action");

			if( strcmp(action,"insert") == 0 || strcmp(action,"update") == 0 || strcmp(action,"askinfo") == 0) {
				if (strcmp(action,"askinfo") == 0)
					state->state = SCARFACE_INSERTREPORT_ASKINFO_SAVE;
				else
					state->state = SCARFACE_INSERTREPORT_INSERT;

				sql_worker_template_query(&config.pg_master, 
							  "sql/call_insertreport.sql", &ba, 
							  scarface_insertreport_fsm, state, cs->ts->log_string);

			} else if( strcmp(action, "resolve") == 0 ) {
				state->state = SCARFACE_INSERTREPORT_RESOLVE;
				sql_worker_template_query(&config.pg_master, 
							  "sql/call_resolvereport.sql", &ba, 
							  scarface_insertreport_fsm, state, cs->ts->log_string);

			} else if (strcmp(action, "getinfo") == 0) {
				state->state = SCARFACE_INSERTREPORT_GETINFO;
				sql_worker_template_query(&config.pg_master, 
							  "sql/queryreport.sql", &ba, 
							  scarface_insertreport_fsm, state, cs->ts->log_string);

			} else {
				state->state = SCARFACE_INSERTREPORT_ERROR;
				continue;
			}
			/* Clean-up */
			bpapi_output_free(&ba);
			bpapi_simplevars_free(&ba);
			bpapi_vtree_free(&ba);

			/* Return to libevent */
			return NULL;

		case SCARFACE_INSERTREPORT_INSERT:
			state->state = SCARFACE_INSERTREPORT_DONE;

		 	if (worker->next_row(worker)) {
				int report_id = atoi(worker->get_value_byname(worker, "o_report_id"));
				transaction_printf(cs->ts, "report_id:%d\n", report_id);
				int uid = atoi(worker->get_value_byname(worker, "o_uid"));
				transaction_printf(cs->ts, "uid:%d\n", uid);

			} else {
				state->state = SCARFACE_INSERTREPORT_ERROR;
			}
			continue;

		case SCARFACE_INSERTREPORT_RESOLVE:
		 	if (worker->next_row(worker)) {
				state->reporter_email = worker->get_value_byname(worker, "o_email");
				state->ad_id = worker->get_value_byname(worker, "o_ad_id");
				state->abuse_type = worker->get_value_byname(worker, "o_abuse_type");
				state->report_id = worker->get_value_byname(worker, "o_report_id");
				state->uid = worker->get_value_byname(worker, "o_reporter_id");
				state->lang = worker->get_value_byname(worker, "o_lang");
				state->resolution = cmd_getval(state->cs, "resolution");

				if ( state->reporter_email && strlen(state->reporter_email) > 0) {
					state->state = SCARFACE_INSERTREPORT_MAIL;
				} else {
					state->state = SCARFACE_INSERTREPORT_DONE;
				}

			} else {
				state->state = SCARFACE_INSERTREPORT_ERROR;
			}
			continue;

		case SCARFACE_INSERTREPORT_ASKINFO_SAVE:

			state->state = SCARFACE_INSERTREPORT_ASKINFO;

			BPAPI_INIT(&ba, NULL, pgsql);
			bind_insert(&ba, state);

			sql_worker_template_query(&config.pg_master, 
					  "sql/queryreport.sql", &ba, 
					  scarface_insertreport_fsm, state, cs->ts->log_string);

			bpapi_output_free(&ba);
			bpapi_simplevars_free(&ba);
			bpapi_vtree_free(&ba);
			return NULL;

		case SCARFACE_INSERTREPORT_ASKINFO:
		 	if (worker->next_row(worker)) {
				state->reporter_email = worker->get_value_byname(worker, "o_email");
				state->ad_id = worker->get_value_byname(worker, "o_ad_id");
				state->abuse_type= worker->get_value_byname(worker, "o_report_type");
				state->lang= worker->get_value_byname(worker, "o_lang");
				state->uid = worker->get_value_byname(worker, "o_uid");

				if (state->reporter_email )
					state->state = SCARFACE_INSERTREPORT_MAIL;
				else {
					cs->status = "NOT_VALID_EMAIL";
					state->state = SCARFACE_INSERTREPORT_DONE;
				}
			} else {
				state->state = SCARFACE_INSERTREPORT_ERROR;
			}
			continue;

		case SCARFACE_INSERTREPORT_GETINFO:
		 	if (worker->next_row(worker)) {
				const char *message = worker->get_value_byname(worker, "o_message");
				const char *uid = worker->get_value_byname(worker, "o_uid");
				const char *abuse_type= worker->get_value_byname(worker, "o_report_type");
				const char *email = worker->get_value_byname(worker, "o_email");
				char * blob_message = NULL;

				transaction_printf(cs->ts, "uid:%s\n", uid);
				transaction_printf(cs->ts, "email:%s\n", email);
				if (strchr(message, '\n')) {
					xasprintf(&blob_message, "blob:%zu:message\n%s", strlen(message), message);
					transaction_printf(cs->ts, "%s\n", blob_message);
					if (blob_message)
						free(blob_message);
				} else {
					transaction_printf(cs->ts, "message:%s\n", message);
				}
				transaction_printf(cs->ts, "abuse_type:%s\n", abuse_type);
				state->state = SCARFACE_INSERTREPORT_DONE;
			} else {
				cs->status = "ERROR_NOT_VALID_REPORT_OR_AD";
				state->state = SCARFACE_INSERTREPORT_ERROR;
			}
			continue;

		case SCARFACE_INSERTREPORT_MAIL:
			state->state = SCARFACE_INSERTREPORT_DONE;
			if (send_mail(cs, state)) {
				transaction_logprintf(cs->ts, D_ERROR, "Failed to send email");
				cs->status = "ERROR_SENDING_MAIL";
				state->state = SCARFACE_INSERTREPORT_ERROR;
			}
			continue;

		case SCARFACE_INSERTREPORT_DONE:
			scarface_insertreport_done(state);
			return NULL;

		case SCARFACE_INSERTREPORT_ERROR:
			transaction_logprintf(cs->ts, D_ERROR, "Error state");
			scarface_insertreport_done(state);
			return NULL;

		default:
			/* MAJOR STATE ERROR */
			cs->status = "TRANS_ERROR";
			transaction_logprintf(cs->ts, D_ERROR, "Unknown state (%d)", 
					      state->state);
			return NULL;

		}
	}
}

static void
scarface_insertreport(struct cmd *cs) {
	transaction_logprintf(cs->ts, D_DEBUG, "Start Abusereports transaction");
	struct scarface_insertreport_state *state;
	state = zmalloc(sizeof(*state));
	state->cs = cs;
	state->state = SCARFACE_INSERTREPORT_INIT;

	scarface_insertreport_fsm(state, NULL, NULL);
}

ADD_COMMAND(scarface_insertreport,     /* Name of command */
            NULL,/* Called during the initalisation phase */
            scarface_insertreport,     /* Main function name */
            parameters,  /* Parameters struct */
            "Scarface insert report command");/* Command description */

