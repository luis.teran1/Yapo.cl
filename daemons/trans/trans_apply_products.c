#include "factory.h"
#include "bpapi.h"
#include "trans.h"
#include <bconf.h>
#include <ctemplates.h>

extern struct bconf_node *bconf_root;

static struct c_param parameters[] = {
	{"payment_group_id", P_REQUIRED, "v_apply_products_payment_group_id"},
	{"remote_browser",   P_OPTIONAL, "v_remote_browser"},
	{NULL, 0}
};

static struct factory_data data = {
	"apply_products"
};

static const char *
get_category_type(const char *category) {
	struct bconf_node *product_group = bconf_get(trans_bconf, "upselling_settings.product_group");
	int n = bconf_count(product_group);
	for (int i = 0; i < n; ++i) {
		struct bconf_node *pg = bconf_byindex(product_group, i);
		const char *categories = bconf_get_string(pg, "categories");
		if (strstr(categories, category)) {
			const char *type = bconf_key(pg);
			return type;
		}
	}

	return "default";
}

static const char *
is_pro_for_category(struct bpapi *ba, const char *category) {
	const char *is_pro_for = bpapi_get_element(ba, "is_pro_for", 0);
	return strstr(is_pro_for, category) ? "1" : "0";
}

static int
find_previous_combo(struct cmd *cs, const char *ad_id, const char *combo_name) {
	struct bpapi ba;
	struct sql_worker *worker = NULL;
	const char *errstr = NULL;
	if (!combo_name) {
		transaction_logprintf(cs->ts, D_ERROR,"Combo name missing");
		return 1;
	}

	BPAPI_INIT(&ba, NULL, pgsql);
	bpapi_insert(&ba, "ad_id", ad_id);
	bpapi_insert(&ba, "combo_name", combo_name);
	worker = sql_blocked_template_query(
		"pgsql.master",
		0,
		"sql/find_previous_combo.sql",
		&ba,
		cs->ts->log_string,
		&errstr
	);
	bpapi_free(&ba);

	if (errstr) {
		transaction_logprintf(cs->ts, D_ERROR,"Error in query: %s ", errstr);
		if (worker)
			sql_worker_put(worker);
		return 1;
	}

	int counter = 0;
	if (worker) {
		while (sql_blocked_next_result(worker, &errstr) == 1) {
			while (worker->next_row(worker)) {
				transaction_logprintf(cs->ts, D_DEBUG,
					"Previous combo was found: %s, %s (%d)",
					worker->get_value_byname(worker, "action_id"),
					worker->get_value_byname(worker, "action_type"),
					counter++
				);
			}
		}
		sql_worker_put(worker);
	}
	transaction_logprintf(cs->ts, D_DEBUG,"Validate previous returning: %d and found %d", counter > 1, counter);
	return counter > 1;
}


static int
read_config_and_apply(struct cmd *cs, struct bpapi *ba) {
	// verifying that combo products are applied to the proper categories
	const int detail_len = bpapi_length(ba, "purchase_detail_id");
	const int skip_commit = bpapi_length(ba, "skip_commit");

	for (int i = 0; i < detail_len; i++) {
		const char *product = bpapi_get_element(ba, "product_id", i);
		const char *category = bpapi_get_element(ba, "category", i);
		const char *ad_id = bpapi_get_element(ba, "ad_id", i);
		const char *product_type = bconf_vget_string(trans_bconf, "payment.product", product, "group", NULL);
		if (product_type && (!strcmp("COMBO_UPSELLING", product_type) || !strcmp("PROMO", product_type))) {
			const char *category_type = get_category_type(category);
			// find previous combo upselling
			const char *prod_codename = bconf_vget_string(trans_bconf, "upselling_settings.product.description", category_type, category, product, "codename", NULL);
			transaction_logprintf(cs->ts, D_DEBUG,"Codename:%s Category_type:%s Category:%s, Product:%s", prod_codename, category_type, category, product);
			if (prod_codename && find_previous_combo(cs, ad_id, prod_codename)) {
				cs->status = "TRANS_ERROR";
				cs->error = "FOUND_PREVIOUS_COMBO_FOR_AD";
				return -1;
			}

			const char *pro_for_category = is_pro_for_category(ba, category);
			// if category is in the category group, verify that we have config for that product inside category group config
			struct bconf_node *specific_config = bconf_vget(trans_bconf, "upselling_settings.product.description", category_type, pro_for_category, NULL);
			// Check category_type - pro_fo_category match
			if (specific_config) {
				// Only products on the next level can be applied
				struct bconf_node *product_config = bconf_vget(specific_config, product, NULL);
				if (product_config)
					continue;
			} else {
				// We must check default configuration
				struct bconf_node *default_config = bconf_vget(trans_bconf, "upselling_settings.product.description", "default", pro_for_category, product, NULL);
				if (default_config)
					continue;
			}

			// We only reach here if the product is invalid
			transaction_logprintf(cs->ts, D_ERROR,
				"Found combo product %s "
				"for category %s, "
				"which is type %s, "
				"user pro for this category = %s, "
				"a type not allowed to have this kind of product applied.",
				product,
				category,
				category_type,
				pro_for_category
			);
			cs->status = "TRANS_ERROR";
			cs->error = "FOUND_COMBO_PRODUCT_FOR_WRONG_CATEGORY";
			return -1;
		}
		if (product_type && (!strcmp("INSERTING_FEE_PREMIUM", product_type))) {
			const char *categories = bconf_vget_string(trans_bconf, "payment.product", product, "for_categories", NULL);
			if (!strstr(categories, category)) {
				// We only reach here if the product is invalid
				transaction_logprintf(cs->ts, D_ERROR,
					"Found product %s "
					"with ad category %s, "
					"this product cannot be applied to this ad.",
					product,
					category
				);
				cs->status = "TRANS_ERROR";
				cs->error = "FOUND_IF_PREMIUM_FOR_WRONG_CATEGORY";
				return -1;
			}
		}
	}
	for (int i = 0; i < detail_len; i++) {

		const char *product_id = bpapi_get_element(ba, "product_id", i);
		struct bconf_node *product = bconf_vget(trans_bconf, "upselling_settings.product", product_id, NULL);
		int product_count = bconf_count(product);

		if (!product) {
			// Uncomment the following when every product is applied through this transaction - Jaime
			// transaction_logprintf(cs->ts, D_ERROR, "No configuration found for product %s !", product_id);
			continue;
		}

		for (int j = 0; j < product_count; j++) {
			struct buf_string bs = { 0 };
			struct bconf_node *sub_product = bconf_byindex(product, j);
			const char *subproduct_id = bconf_get_string(sub_product, "product_id");
			int delay = bconf_get_int(sub_product, "delay");
			char *res;

			// look for trans command to execute (as internal) and its parameters
			struct bconf_node *command = bconf_vget(trans_bconf, "upselling_settings.command", subproduct_id, NULL);
			struct bconf_node *def_params = bconf_get(command, "default_params");
			struct bconf_node *given_params = bconf_get(command, "provided_params");

			bscat(&bs, "cmd:%s\n", bconf_get_string(command, "cmd"));

			int def_params_len = bconf_count(def_params);
			for (int k = 0; k < def_params_len; k++) {
				struct bconf_node *def_param = bconf_byindex(def_params, k);
				const char *value = bconf_get_string(def_param, "value");
				if (skip_commit && strstr(value, "commit:1"))
					continue;
				bscat(&bs, "%s\n", value);
			}
			int given_params_len = bconf_count(given_params);
			for (int k = 0; k < given_params_len; k++) {
				struct bconf_node *given_param = bconf_byindex(given_params, k);
				const char *param_name = bconf_get_string(given_param, "value");
				const char *provided = bpapi_get_element(ba, param_name, i);
				if (provided) {
					bscat(&bs, "%s:%s\n", param_name, provided);
				} else {
					const char *conf_provided = bconf_get_string(sub_product, param_name);
					if (conf_provided) {
						bscat(&bs, "%s:%s\n", param_name, conf_provided);
					}
				}
			}

			if (delay && !skip_commit) {
				res = transaction_internal_output_printf(
					"cmd:queue\n"
					"commit:1\n"
					"queue:%s\n"
					"timeout:%d\n"
					"blob:%d:command\n"
					"%s\n",
					"upselling",
					delay,
					bs.pos,
					bs.buf);
			} else {
				res = transaction_internal_output_printf(bs.buf, NULL);
			}
			free(bs.buf);
			char *ok = strstr(res, "status:TRANS_OK");
			if (!ok) {
				transaction_logprintf(cs->ts, D_ERROR,
					"Could not apply purchase, product %s. "
					"Result from failed trans: %s",
					product_id,
					res
				);
				free(res);
				cs->status = "TRANS_ERROR";
				cs->error = "FAILED_TO_APPLY_PRODUCT_FROM_PURCHASE";
				return -1;
			}
			free(res);
		}
	}
	return 0;
}

static int
vf_apply_products_verification(struct cmd_param *param, const char *arg) {

	struct bpapi ba;
	struct sql_worker *worker = NULL;
	const char *errstr = NULL;
	static int check = 0;

	BPAPI_INIT(&ba, NULL, pgsql);

	bpapi_insert(&ba, "payment_group_id", param->value);

	worker = sql_blocked_template_query(
		"pgsql.master",
		0,
		"sql/get_items_by_payment_to_apply.sql",
		&ba,
		param->cs->ts->log_string,
		&errstr
	);
	bpapi_free(&ba);

	if (errstr) {
		free(param->cs->message);
		param->cs->status = "TRANS_DATABASE_ERROR";
		param->cs->message = xstrdup(errstr);
		sql_worker_put(worker);
		return 1;
	}
	if (worker) {
		/* we have data from data_template */
		BPAPI_INIT(&ba, NULL, pgsql);
		while (sql_blocked_next_result(worker, &errstr) == 1) {
			if (worker->next_row(worker)) {
				for (int i = 0; i < worker->fields(worker); i++) {
					if (worker->get_value(worker, i)[0])
						bpapi_insert(&ba, worker->field_name(worker, i), worker->get_value(worker, i));
					else
						bpapi_insert(&ba, worker->field_name(worker, i), "NULL");
				}
			}
		}

		sql_worker_put(worker);
		bpapi_insert(&ba, "skip_commit", "1");
		check = read_config_and_apply(param->cs, &ba);
		bpapi_free(&ba);
	}
	return check;
}

struct validator v_apply_products_payment_group_id[] = {
	VALIDATOR_INT(1, -1, "ERROR_AD_ID_INVALID"),
	VALIDATOR_LEN(1, 15, "ERROR__INVALID"),
	VALIDATOR_FUNC(vf_apply_products_verification),
	{0}
};
ADD_VALIDATOR(v_apply_products_payment_group_id);

static const struct factory_action actions[] = {
	FA_SQL(NULL, "pgsql.master", "sql/get_items_by_payment_to_apply.sql", FACTORY_RES_SHORT_FORM),
	FA_FUNC(read_config_and_apply),
	FA_DONE()
};

FACTORY_TRANS(apply_products, parameters, data, actions);
