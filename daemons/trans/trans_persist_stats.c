#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <time.h>
#include <ctemplates.h>
#include <hiredis.h>
#include "trans.h"
#include "util.h"
#include "validators.h"
#include <bconf.h>
#include <event.h>
#include "utils/redis_connect.h"

#define DAILY_HOURS 24

extern struct bconf_node *bconf_root;

static struct c_param parameters[] = {
	{"date", P_REQUIRED, "v_date"}, // format yyyy-mm-dd
	{"stat", P_REQUIRED, "v_stat"}, // accepted param STORE_VIEW
	{NULL, 0}
};

/* The zset that contains the store views on redis */
struct daily_visits {
	int hourly_visits[DAILY_HOURS];
	int total_visits;
};

/* To not ask twice to db for stores_qtty we save it in this struct
 * plus the daily_visits array of structures that contains all stores */
struct full_visits {
	struct daily_visits *dv;
	int stores_qtty;
};

/* Do a synchronous query to get MAX(store_id) (the stores quantity) */
static int
get_stores_qtty(struct cmd *cs) {
	struct sql_worker *worker = NULL;
	const char *errstr = NULL;
	int max_stores = 0;

	worker = sql_blocked_template_query("pgsql.master", 0,
		"sql/count_stores.sql",	NULL, cs->ts->log_string, &errstr);

	if (worker && sql_blocked_next_result(worker, &errstr) == 1) {
		if (worker->rows(worker) > 0 && worker->next_row(worker)) {
			max_stores = atoi((const char *)worker->get_value(worker, 0));
			transaction_logprintf(cs->ts, D_DEBUG, "store quantity: %d", max_stores);
		}
	}

	if (errstr) {
		cs->message = xstrdup(errstr);
		cs->status = "TRANS_DATABASE_ERROR";
	}

	if (worker)
		sql_worker_put(worker);

	return max_stores;
}

/* get daily visits from redis */
static struct full_visits *
get_visits(struct cmd *cs) {
	redisContext *redisstats = rs_connect(cs, "*.*.common.redisstatdb1");
	if (!redisstats)
		return NULL;

	const char * date = cmd_getval(cs, "date");
	const char * stat = cmd_getval(cs, "stat");
	char redis_cmd[256];
	char redis_cmd_total[256];
	int stores_qtty = get_stores_qtty(cs) + 1;
	struct daily_visits *all_visits = zmalloc(sizeof(struct daily_visits) * stores_qtty);

	/* hourly stats */
	for (int store_id = 0; store_id < stores_qtty; store_id++) {
		for (int hour = 0; hour < 24; hour++) {
			sprintf(redis_cmd, "ZSCORE %s:%s %d:%.2d", stat, date, store_id, hour);
			redisReply *reply = redisCommand(redisstats, redis_cmd);

			switch (reply->type) {

				case REDIS_REPLY_STRING:
					transaction_logprintf(cs->ts, D_DEBUG, "Number of visits: %s  store_id: %d  hour: %d", reply->str, store_id, hour);
					all_visits[store_id].hourly_visits[hour] = atoi(reply->str);
					break;

				case REDIS_REPLY_NIL:
					transaction_logprintf(cs->ts, D_DEBUG, "Number of visits: 0");
					all_visits[store_id].hourly_visits[hour] = 0;
					break;

				case REDIS_REPLY_ERROR:
					transaction_logprintf(cs->ts, D_ERROR, "Something went wrong: %s", reply->str);
					all_visits[store_id].hourly_visits[hour] = 0;
					break;

				default:
					transaction_logprintf(cs->ts, D_ERROR, "Something unknown happen");
					break;
			}
			freeReplyObject(reply);
		}
	}

	/* total stats */
	for (int store_id = 0; store_id < stores_qtty; store_id++) {
		sprintf(redis_cmd_total, "ZSCORE %s:%s %d:total", stat, date, store_id);
		redisReply *reply = redisCommand(redisstats, redis_cmd_total);
		switch (reply->type) {

			case REDIS_REPLY_STRING:
				transaction_logprintf(cs->ts, D_DEBUG, "Number of total visits: %s  store_id: %d", reply->str, store_id);
				all_visits[store_id].total_visits = atoi(reply->str);
				break;

			case REDIS_REPLY_NIL:
				transaction_logprintf(cs->ts, D_DEBUG, "Number of total visits: 0");
				all_visits[store_id].total_visits = 0;
				break;

			case REDIS_REPLY_ERROR:
				transaction_logprintf(cs->ts, D_ERROR, "Something went wrong: %s", reply->str);
				all_visits[store_id].total_visits = 0;
				break;

			default:
				transaction_logprintf(cs->ts, D_ERROR, "Something unknown happen");
				break;
		}
		freeReplyObject(reply);
	}

	struct full_visits *fv = malloc(sizeof(struct full_visits));
	fv->dv = all_visits;
	fv->stores_qtty = stores_qtty;
	redisFree(redisstats);

	return fv;
}

/* go to db and save the whole structure of store visits */
static void
persist_stores_visits(struct cmd *cs, struct full_visits *fv) {
	int stores_qtty = fv->stores_qtty;
	const char * date = cmd_getval(cs, "date");

	for (int store_id = 1; store_id < stores_qtty; store_id++) {
		for (int hour = 0; hour <= 23; hour++) {
			/* only persist the non zero records */
			if (fv->dv[store_id].hourly_visits[hour]) {
				transaction_internal_printf(TRANSACTION_INTERNAL_SYNC_CB, NULL,
					"cmd:save_stats\n"
					"stat_type:store_view\n"
					"ext_id:%d\n"
					"reg_date:%s\n"
					"reg_hour:%d:00\n"
					"visits:%d\n"
					"commit:1\n"
					"end\n",
					store_id,
					date,
					hour,
					fv->dv[store_id].hourly_visits[hour]
				);
			}
		}
	}
}

/* "main" function of the transaction */
static void
persist_stats(struct cmd *cs) {
	transaction_logprintf(cs->ts, D_DEBUG, "Dumping store views from redis stats to postgres db");
	struct full_visits *fv = get_visits(cs);
	if (fv) {
		persist_stores_visits(cs, fv);
		free(fv->dv);
		free(fv);
	}
	cmd_done(cs);
}

ADD_COMMAND(persist_stats,     			/* Name of command */
			NULL,						/* I don't know add it if you know */
            persist_stats,				/* Main function name */
            parameters,					/* Parameters struct */
            "persist_stats command");	/* Command description */
