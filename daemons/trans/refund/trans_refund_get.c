#include "factory.h"

static struct c_param parameters[] = {
	{"ad_id", P_REQUIRED, "v_integer"},
	{"action_id", P_REQUIRED, "v_integer"},
	{"purchase_id", P_REQUIRED, "v_integer"},
	{NULL, 0}
};

static struct factory_data data = {
	"refund",
	FACTORY_RES_SHORT_FORM 
};

static const struct factory_action actions[]= {
	FA_SQL("", "pgsql.master", "sql/refund_get.sql", FA_OUT),
	{0}
};

FACTORY_TRANS(refund_get, parameters, data, actions);
