#include <factory.h>
#include "bpapi.h"

static struct c_param parameters[] = {
	{"days", P_OPTIONAL, "v_integer"},
	{NULL, 0}
};

static struct factory_data data = {
	"refund",
	FACTORY_RES_SHORT_FORM 
};

static const struct factory_action actions[]= {
	FA_SQL("", "pgsql.master", "sql/call_refund_delete_old.sql", FA_OUT),
	FA_DONE()
};

FACTORY_TRANS(refund_delete_old, parameters, data, actions);
