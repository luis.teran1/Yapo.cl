#include <factory.h>
#include "bpapi.h"

struct validator v_bank_id[] = {
	VALIDATOR_LEN(1, -1, "ERROR_BANK_ID_MISSING"),
	VALIDATOR_INT(0, 100, "ERROR_INVALID_BANK_ID"),
	VALIDATOR_BCONF_LIST_KEY("refund.bank_name", "ERROR_INVALID_BANK_ID"),
	{0}
};
ADD_VALIDATOR(v_bank_id);

struct validator v_bank_acc_type[] = {
	VALIDATOR_LEN(1, -1, "ERROR_BANK_ACCOUNT_TYPE_MISSING"),
	VALIDATOR_INT(0, 20, "ERROR_INVALID_BANK_ACCOUNT_TYPE"),
	VALIDATOR_BCONF_LIST_KEY("refund.bank_account_type", "ERROR_INVALID_BANK_ACCOUNT_TYPE"),
	{0}
};
ADD_VALIDATOR(v_bank_acc_type);

struct validator v_bank_acc_number[] = {
	VALIDATOR_LEN(5, -1, "ERROR_BANK_ACCOUNT_TOO_SHORT"),
	VALIDATOR_LEN(-1,20, "ERROR_BANK_ACCOUNT_TOO_LONG"),
	VALIDATOR_REGEX("^([[:digit:]-]{1,20})$", REG_EXTENDED, "ERROR_INVALID_BANK_ACCOUNT"),
	{0}
};
ADD_VALIDATOR(v_bank_acc_number);

static struct c_param parameters[] = {
	{"ad_id", P_REQUIRED, "v_ad_id"},
	{"action_id", P_REQUIRED, "v_integer"},
	{"purchase_id", P_REQUIRED, "v_integer"},
	{"first_name", P_REQUIRED, "v_name"},
	{"last_name", P_REQUIRED, "v_name"},
	{"rut", P_REQUIRED, "v_pay_rut"},
	{"email", P_REQUIRED, "v_email_basic"},
	{"bank", P_REQUIRED,"v_bank_id"},
	{"bank_account_type", P_REQUIRED, "v_bank_acc_type"},
	{"bank_account_number", P_REQUIRED, "v_bank_acc_number"},
	{NULL, 0}
};

static struct factory_data data = {
	"refund",
	FACTORY_RES_SHORT_FORM | FACTORY_EMPTY_RESULT_OK
};

static const struct factory_action actions[]= {
	FA_SQL("", "pgsql.master", "sql/call_refund_insert.sql", 0),
	FA_MAIL("mail/refund.mime"),
	FA_DONE()
};

FACTORY_TRANS(refund_insert, parameters, data, actions);
