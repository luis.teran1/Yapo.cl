#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <time.h>

#include "trans.h"
#include "util.h"
#include <ctemplates.h>
#include "validators.h"
#include <bconf.h>
#include <event.h>

#define LIST_ADCODES_DONE  0
#define LIST_ADCODES_ERROR 1
#define LIST_ADCODES_INIT  2
#define LIST_ADCODES_PAYMENTS 3
#define LIST_ADCODES_PAYMENTS_RESULT 4
#define LIST_ADCODES_ADCODES 5
#define LIST_ADCODES_ADCODES_RESULT 6

struct list_adcodes_state {
	int state;
	struct cmd *cs;
        int inuse;
        int free;
        struct bpapi ba;
};

static struct validator v_list_adcodes_type[] = {
	VALIDATOR_REGEX("^(all|paycodes|verifycodes)$", REG_EXTENDED, "ERROR_UNKNOWN_TYPE"),
	{0}
};
ADD_VALIDATOR(v_list_adcodes_type);

static struct validator v_list_adcodes_filter[] = {
	VALIDATOR_REGEX("^(all|inuse|free)$", REG_EXTENDED, "ERROR_UNKNOWN_FILTER"),
	{0}
};
ADD_VALIDATOR(v_list_adcodes_filter);

static struct c_param parameters[] = {
	{"type",	0,	"v_list_adcodes_type"},
	{"filter",	0,	"v_list_adcodes_filter"},
	{NULL, 0}
};

static const char *list_adcodes_fsm(void *, struct sql_worker *, const char *);


/*****************************************************************************
 * list_adcodes_done
 * Exit function.
 **********/
static void
list_adcodes_done(void *v) {
	struct list_adcodes_state *state = v;
	struct cmd *cs = state->cs;

	bpapi_output_free(&state->ba);
	bpapi_simplevars_free(&state->ba);
	bpapi_vtree_free(&state->ba);
	free(state);
	cmd_done(cs);
}

/*****************************************************************************
 * bind_insert
 * Initialise template API before a call to sql_worker_template_query().
 **********/
static void
bind_insert(struct bpapi *ba, struct list_adcodes_state *state) {
        const char *v;
        
        if (cmd_haskey(state->cs, "type")) {
                v = cmd_getval(state->cs, "type");
                if (!strcmp(v, "all")) {
                        v = NULL;
		}
        } else {
                v = NULL;
	}
        if (!v || !strcmp(v, "paycodes")) {
                bpapi_insert(ba, "status", "unpaid");
                bpapi_insert(ba, "status", "paid");
                bpapi_insert(ba, "type", "pay");
        }
        if (!v || !strcmp(v, "verifycodes")) {
                bpapi_insert(ba, "status", "unverified");
                bpapi_insert(ba, "status", "verified");
                bpapi_insert(ba, "type", "verify");
        }
        
        if (cmd_haskey(state->cs, "filter")) {
                v = cmd_getval(state->cs, "filter");
                if (!strcmp(v, "all"))
                        v = NULL;
        } else  {
                v = NULL;
	}
        if (!v || !strcmp(v, "inuse"))
                state->inuse = 1;
        if (!v || !strcmp(v, "free"))
                state->free = 1;
}

/*****************************************************************************
 * list_adcodes_fsm
 * State machine
 **********/
static const char *
list_adcodes_fsm(void *v, struct sql_worker *worker, const char *errstr) {
	struct list_adcodes_state *state = v;
	struct cmd *cs = state->cs;

	transaction_logprintf(cs->ts, D_DEBUG, "list_adcodes_fsm(%p, %p, %p), state=%d", 
                              v, worker, errstr, state->state);

	/* 
	 * General error handling for db errors.
	 */
	if (errstr) {
                transaction_logprintf(cs->ts, D_ERROR, "Statement failed (%d, %s)", 
                                      state->state,
                                      errstr);
                cs->status = "TRANS_DATABASE_ERROR";
                cs->message = xstrdup(errstr);
		state->state = LIST_ADCODES_ERROR;
        }

	while (1) {
		switch (state->state) {
		case LIST_ADCODES_INIT:
                        /* Initialise the templateparse structure */
			BPAPI_INIT(&state->ba, NULL, pgsql);
			bind_insert(&state->ba, state);
			/* next state */
			state->state = LIST_ADCODES_PAYMENTS;
                        break;

                case LIST_ADCODES_PAYMENTS:
                        if (state->inuse) {
				/* Use master for up to date data */
                                sql_worker_template_query(&config.pg_master, 
                                                          "sql/list_adcodes_payments.sql", &state->ba, 
                                                          list_adcodes_fsm, state, cs->ts->log_string);
                                state->state = LIST_ADCODES_PAYMENTS_RESULT;
                                return NULL;
                        }
                        state->state = LIST_ADCODES_ADCODES;
                        break;

                case LIST_ADCODES_PAYMENTS_RESULT:
			transaction_logoff(cs->ts);
                        while (worker->next_row(worker)) {
                                transaction_printf(state->cs->ts, "%s:%s\n", worker->get_value(worker, 0),
                                                    worker->get_value(worker, 1));
                        }
			transaction_logon(cs->ts);
                        state->state = LIST_ADCODES_ADCODES;
                        break;
                        
                case LIST_ADCODES_ADCODES:
                        if (state->free) {
				/* Use master for up to date data */
                                sql_worker_template_query(&config.pg_master, 
                                                          "sql/list_adcodes_free.sql", &state->ba, 
                                                          list_adcodes_fsm, state, cs->ts->log_string);
                                state->state = LIST_ADCODES_ADCODES_RESULT;

                                return NULL;
                        }
                        state->state = LIST_ADCODES_DONE;
                        break;
                        
                case LIST_ADCODES_ADCODES_RESULT:
			transaction_logoff(cs->ts);
                        while (worker->next_row(worker)) {
                                transaction_printf(state->cs->ts, "%s:%s\n", worker->get_value(worker, 0),
                                                    worker->get_value(worker, 1));
                        }
			transaction_logon(cs->ts);
                        state->state = LIST_ADCODES_DONE;
                        break;

		case LIST_ADCODES_DONE:
			list_adcodes_done(state);
			return NULL;

		case LIST_ADCODES_ERROR:
			transaction_logprintf(cs->ts, D_ERROR, "Error state");
			list_adcodes_done(state);
			return NULL;

		default:
			/* MAJOR STATE ERROR */
			cs->status = "TRANS_ERROR";
			transaction_logprintf(cs->ts, D_ERROR, "Unknown state (%d)", 
					      state->state);
			list_adcodes_done(state);
			return NULL;

		}
	}
}

/*****************************************************************************
 * list_adcodes
 * Main function that initialise a state-structure and calls the state machine.
 **********/
static void
list_adcodes(struct cmd *cs) {
	transaction_logprintf(cs->ts, D_DEBUG, "Start List_Adcodes transaction");
	struct list_adcodes_state *state;
	state = zmalloc(sizeof(*state));
	state->cs = cs;
	state->state = LIST_ADCODES_INIT;

	list_adcodes_fsm(state, NULL, NULL);
}

ADD_COMMAND(list_adcodes,     /* Name of command */
		NULL,
            list_adcodes,     /* Main function name */
            parameters,  /* Parameters struct */
            "List_Adcodes command");/* Command description */
