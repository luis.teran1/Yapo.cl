#include "factory.h"

static struct validator v_unf_status[] = {
	VALIDATOR_REGEX("^(unpublished|active|deleted)$", REG_EXTENDED, "ERROR_STATUS_INVALID"),
	{0}
};
ADD_VALIDATOR(v_unf_status);

static struct c_param params[] = {
	{"unf_ad_id", P_REQUIRED, "v_integer"},
	{"unf_status", P_REQUIRED, "v_unf_status"},
	{NULL,0} 
};

FACTORY_TRANSACTION(status_unfinished_ad, &config.pg_master, "sql/call_status_unfinished_ad.sql", NULL, params, FACTORY_EMPTY_RESULT_OK);

