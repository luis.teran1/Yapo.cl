#include "factory.h"

static struct validator v_data_unfinished[] = {
	VALIDATOR_LEN(1, 10000, "ERROR_DATA_TOO_LONG"),
	{0}
};
ADD_VALIDATOR(v_data_unfinished);

static struct validator v_session_id[] = {
	VALIDATOR_LEN(1, 60, "ERROR_SESSION_ID_TOO_LONG"),
	VALIDATOR_REGEX("^[a-zA-Z0-9-]+$", REG_EXTENDED, "ERROR_SESSION_ID_INVALID"),
	{0}
};
ADD_VALIDATOR(v_session_id);

static struct c_param params[] = {
	{"unf_ad_id", 0, "v_integer"},
	{"session_id", P_REQUIRED, "v_session_id"},
	{"email", P_REQUIRED, "v_email_basic"},
	{"data", P_REQUIRED, "v_data_unfinished"},
	{NULL,0} 
} ;

FACTORY_TRANSACTION(save_unfinished_ad, &config.pg_master, "sql/call_save_unfinished_ad.sql", NULL, params, FACTORY_EMPTY_RESULT_OK);

