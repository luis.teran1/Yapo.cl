#include "factory.h" 

static struct c_param parameters[] = {
	{"tbk_purchase_order",		P_REQUIRED,	"v_integer"},
	{"tbk_state",			P_REQUIRED,	"v_array:init,authorized,refused,payment_ok,ERR_DUP_ORDER,ERR_CANCELLED,ERR_PRICE_INCORRECT,ERR_ORDER_INVALID,ERR_MAC_INVALID"},
	{"tbk_response",		0,		"v_string_isprint_limited"},
	{"remote_addr",			0,		"v_string_isprint_limited"},
	{"remote_browser",		0,		"v_remote_browser"},
	{"tbk_ref_auth_code", 	    	0,		"v_string_isprint_limited"},
	{"tbk_ref_trans_date", 	    	0,		"v_string_isprint_limited"},
	{"tbk_ref_placements_type",   	0,		"v_string_isprint_limited"},
	{"tbk_ref_placements_number", 	0,		"v_string_isprint_limited"},
	{"tbk_ref_pay_type", 		0,		"v_string_isprint_limited"},
	{"tbk_ref_external_id", 	0,		"v_string_isprint_limited"},
	{"tbk_ref_cc_number", 	0,		"v_string_isprint_limited"},
	{NULL, 0}
};

FACTORY_TRANSACTION(tbk_add_state, &config.pg_master, "sql/call_tbk_add_state.sql", NULL, parameters, FACTORY_NO_RES_CNTR | FACTORY_RES_SHORT_FORM);
