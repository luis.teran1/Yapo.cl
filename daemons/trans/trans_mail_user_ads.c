#include "factory.h"

static struct c_param parameters[] = {
	{"email",		P_REQUIRED,  	"v_email_basic"},
	{"lst_id",		P_REQUIRED,  	"v_id_list"},
	{NULL, 0, NULL}
};

static struct factory_data data = {
	"mail_user_ads",
	FACTORY_EMPTY_RESULT_OK | FACTORY_RES_SHORT_FORM
};

static const struct factory_action actions[] = {
	FA_SQL("tab_", "pgsql.slave", "sql/load_my_ads.sql", 0),
	FA_OUT("trans/my_ads_extra_check.txt"),
	FA_MAIL("mail/user_ads.mime"),
	{0}
};

FACTORY_TRANS(mail_user_ads, parameters, data, actions);
