#include "factory.h"

extern struct bconf_node *bconf_root;

static struct c_param parameters[] = {
	{NULL, 0}
};

static struct factory_data data = {
	"destacados",
	FACTORY_NO_RES_CNTR | FACTORY_EMPTY_RESULT_OK | FACTORY_RES_SHORT_FORM
};

static const struct factory_action actions[]= {
	FA_SQL(NULL, "pgsql.master", "sql/destacados_report.sql", 0),
	FA_MAIL("mail/destacados_report.mime"),
	{0}
};

FACTORY_TRANS(destacados_report, parameters, data, actions);
