#include "factory.h"

static struct validator v_external_ad_id_nolink[] = {
	VALIDATOR_LEN(1, 50, "ERROR_EXTERNAL_ID_INVALID"),
	VALIDATOR_REGEX("^[A-Za-z0-9_{}-]+$", REG_EXTENDED, "ERROR_EXTERNAL_ID_INVALID"),
	{0}
};
ADD_VALIDATOR(v_external_ad_id_nolink);

static struct c_param parameters[] = {
	{"import_partner",	P_REQUIRED,	"v_import_id"},
	{"ad_id",		0,		"v_id"},
	{"store_id",		0,		"v_integer"},
	{"external_ad_id",	0,		"v_external_ad_id_nolink"},
	{NULL, 0, 0}
};

FACTORY_TRANSACTION(partner_get_ads_status, &config.pg_slave, "sql/partner_get_ads_status.sql", NULL, parameters, FACTORY_NO_RES_CNTR | FACTORY_EMPTY_RESULT_OK);

