#include "factory.h"
#include "bpapi.h"
#include <bconf.h>

static struct c_param parameters[] = {
	{"region", P_REQUIRED, "v_integer"},
	{NULL, 0}
};

static struct factory_data data = {
	"phone_standarization_ads",
	FACTORY_NO_RES_CNTR | FACTORY_EMPTY_RESULT_OK | FACTORY_RES_SHORT_FORM
};

static const struct factory_action actions[] = {
	FA_SQL(NULL, "pgsql.master", "sql/call_phone_standarization_ads.sql", FASQL_OUT),
	FA_DONE()
};

FACTORY_TRANS(phone_standarization_ads, parameters, data, actions);
