#include "factory.h" 
static struct c_param parameters[] = {
	{"category",         P_REQUIRED, "v_new_category"},
	{"interval",         P_OPTIONAL, "v_integer"},
	{NULL, 0}
};

FACTORY_TRANSACTION(
	bump_target_advertisement,
	&config.pg_slave,
	"sql/bump_target_advertisement.sql",
	NULL,
	parameters,
	FACTORY_EMPTY_RESULT_OK
);
