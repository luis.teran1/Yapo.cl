#include "factory.h"

static struct c_param parameters[] = {
	{"ad_id",       P_MULTI | P_REQUIRED, "v_ad_id"},
	{"category",    P_REQUIRED,           "v_category_text_warning:subject,body"},
	{"_params",     P_OPTIONAL,           "v_newad_params"},
	{"remote_addr", P_OPTIONAL,           "v_ip"},
	{NULL, 0}
};

static struct factory_data data = {
        "controlpanel",
        FACTORY_NO_RES_CNTR | FACTORY_RES_SHORT_FORM
};

static const struct factory_action actions[]= {
	FA_SQL("", "pgsql.master", "sql/call_update_ad_params.sql", 0),
	FA_DONE()
};

FACTORY_TRANS(update_ad_params, parameters, data, actions);
