#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdarg.h>
#include <unistd.h>
#include <sys/socket.h>

#include "command.h"
#include "trans.h"
#include "util.h"
#include <ctemplates.h>
#include "trans_mail.h"


static struct c_param parameters[] = {
	{"to",		0,	"v_string_isprint"},
	{"body",	0,	"v_blob"},
	{"error_id",	0,	"v_string_isprint"},
	{"lang",	0,	"v_lang"},
	{NULL, 0}
};

static void
senderror_done(struct mail_data *md, void *arg, int status) {
	struct cmd *cs = arg;

	if (status != 0) {
                cs->status = "TRANS_MAIL_ERROR";
                cmd_done(cs);	
		return;
	}

	cmd_done(cs);
}

static void
senderror(struct cmd *cs) {
	struct mail_data *md;
	md = zmalloc(sizeof (*md));
	md->command = cs;
	md->callback = senderror_done;
	md->log_string = cs->ts->log_string;

	mail_insert_param(md, "to", cmd_getval(cs, "to"));
	if (cmd_haskey(cs, "error_id"))
		mail_insert_param(md, "error_id", cmd_getval(cs, "error_id"));
	mail_insert_param(md, "body", cmd_getval(cs, "body"));

	mail_send(md, "mail/senderror_mail.txt", cmd_getval(cs, "lang"));
}

ADD_COMMAND(senderror, NULL, senderror, parameters, NULL);
