#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <time.h>

#include "trans.h"
#include "util.h"
#include <ctemplates.h>
#include "validators.h"
#include <bconf.h>
#include <event.h>

#define CREATE_ADCODES_DONE  0
#define CREATE_ADCODES_ERROR 1
#define CREATE_ADCODES_INIT  2
#define CREATE_ADCODES_CREATE 3
#define CREATE_ADCODES_RESULTS 4


const char * const types[] = {"pay", "verify", "adwatch"};

struct create_adcodes_state {
	int state;
	struct cmd *cs;
	int type;
	int num_gen[sizeof (types) / sizeof (types[0])];
};

static struct c_param parameters[] = {
	{"paylimit", 0, "v_integer"},
	{"verifylimit", 0, "v_integer"},
	{"adwatchlimit", 0, "v_integer"},
	{"pay_gen", P_OPTIONAL, "v_integer"},
	{"verify_gen", P_OPTIONAL, "v_integer"},
	{"adwatch_gen", P_OPTIONAL, "v_integer"},
	{NULL, 0}
};

static const char *create_adcodes_fsm(void *, struct sql_worker *, const char *);


/*****************************************************************************
 * create_adcodes_done
 * Exit function.
 **********/
static void
create_adcodes_done(void *v) {
	struct create_adcodes_state *state = v;
	struct cmd *cs = state->cs;
	free(state);
	cmd_done(cs);
}

/*****************************************************************************
 * create_adcodes_fsm
 * State machine
 **********/
static const char *
create_adcodes_fsm(void *v, struct sql_worker *worker, const char *errstr) {
	struct create_adcodes_state *state = v;
	struct cmd *cs = state->cs;
        struct bpapi ba;
	/* XXX
	   Worst.
	   Transaction.
	   Ever.
	*/

	transaction_logprintf(cs->ts, D_DEBUG, "create_adcodes_fsm(%p, %p, %p), state=%d", 
                              v, worker, errstr, state->state);

	/* 
	 * General error handling for db errors.
	 */
	if (errstr) {
                transaction_logprintf(cs->ts, D_ERROR, "Statement failed (%d, %s)", 
                                      state->state,
                                      errstr);
                cs->status = "TRANS_DATABASE_ERROR";
                cs->message = xstrdup(errstr);
		state->state = CREATE_ADCODES_ERROR;
        }

	while (1) {
		switch (state->state) {
		case CREATE_ADCODES_INIT:
			state->state = CREATE_ADCODES_CREATE;
			break;
			
		case CREATE_ADCODES_CREATE:
			if (!parameters[state->type].name || parameters[state->type].type == P_OPTIONAL) {
				state->state = CREATE_ADCODES_DONE;
				break;
			}
			if (cmd_haskey(cs, parameters[state->type].name)) {
				
				/* Initialise the templateparse structure */
				BPAPI_INIT(&ba, NULL, pgsql);
				
				bpapi_insert(&ba, "type", types[state->type]);
				bpapi_insert(&ba, "limit", cmd_getval(cs, parameters[state->type].name));
				
				/* Register SQL query with our create_adcodes_fsm() as callback function */
				sql_worker_template_query(&config.pg_master, "sql/call_generate_adcodes.sql", &ba,
							  create_adcodes_fsm, state, cs->ts->log_string);
				
				/* Clean-up */
				bpapi_output_free(&ba);
				bpapi_simplevars_free(&ba);
				bpapi_vtree_free(&ba);
				
				/* Return to libevent */
				state->state = CREATE_ADCODES_RESULTS;
				return NULL;
			}
			else
				state->type++;
			break;
			
		case CREATE_ADCODES_RESULTS:
			state->state = CREATE_ADCODES_CREATE;
			/* XXX please do this better */
			if (worker->next_row(worker)) {
				int num_tot = atoi(worker->get_value_byname(worker, "num_codes"));
				state->num_gen[state->type] += atoi(worker->get_value_byname(worker, "num_generated"));

				if (cmd_haskey(cs, parameters[state->type + sizeof (types)/sizeof (types[0])].name) && 
				    state->num_gen[state->type] >= atoi(cmd_getval(cs, parameters[state->type + sizeof (types)/sizeof (types[0])].name)))
					;
				else if (num_tot < atoi(cmd_getval(cs, parameters[state->type].name)))
					continue;
			}
			transaction_printf(cs->ts, "%s_num_generated:%d\n", types[state->type], state->num_gen[state->type]);
			state->type++;
			break;

		case CREATE_ADCODES_DONE:
			create_adcodes_done(state);
			return NULL;

		case CREATE_ADCODES_ERROR:
			transaction_logprintf(cs->ts, D_ERROR, "Error state");
			create_adcodes_done(state);
			return NULL;

		default:
			/* MAJOR STATE ERROR */
			cs->status = "TRANS_ERROR";
			transaction_logprintf(cs->ts, D_ERROR, "Unknown state (%d)", 
					      state->state);
			create_adcodes_done(state);
			return NULL;

		}
	}
}

/*****************************************************************************
 * create_adcodes
 * Main function that initialise a state-structure and calls the state machine.
 **********/
static void
create_adcodes(struct cmd *cs) {
	transaction_logprintf(cs->ts, D_DEBUG, "Start Create_Adcodes transaction");
	struct create_adcodes_state *state;
	state = zmalloc(sizeof(*state));
	state->cs = cs;
	state->state = CREATE_ADCODES_INIT;

	create_adcodes_fsm(state, NULL, NULL);
}

ADD_COMMAND(create_adcodes,     /* Name of command */
		NULL,
            create_adcodes,     /* Main function name */
            parameters,  /* Parameters struct */
            "Create_Adcodes command");/* Command description */
