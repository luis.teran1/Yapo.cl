#ifndef _FILTER_H
#define _FILTER_H

#include <queue.h>

struct filter_elem {
	SLIST_ENTRY(filter_elem) slist;
	char *key;
	char *value;
};

struct filter_data {
	SLIST_HEAD(,filter_elem) filter_params;

	void (*cb)(struct filter_data *,
		   int status,
		   int rule_id,
		   const char *rule_name,
		   const char *action,
		   const char *target,
		   const char *matched_column,
		   const char *matched_value,
		   int combi_match,
		   const char *errstr);
	void *data;
};

struct filter_data* filter_init(struct filter_data*);
void filter_data(struct filter_data *data, const char *appl);
void filter_add_element(struct filter_data *data, const char *key, const char *value);

enum {FILTER_ERROR, FILTER_MATCH, FILTER_DONE};

#endif /*_FILTER_H*/
