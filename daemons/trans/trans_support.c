#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <time.h>

#include "trans.h"
#include "util.h"
#include <ctemplates.h>
#include "validators.h"
#include <bconf.h>
#include <event.h>
#include <unistd.h>
#include "trans_mail.h"
#include "mail_log.h"
#include <errno.h>
#include <fcntl.h>

#define SUPPORT_DONE  0
#define SUPPORT_ERROR 1
#define SUPPORT_INIT  2
#define SUPPORT_SEND 3

#define SUPPORT_MAX_FILES 3

struct support_state {
	int state;
	struct cmd *cs;
	char *filename[SUPPORT_MAX_FILES];
	int num_files;
};

extern struct bconf_node *bconf_root;

/* subject validation */
static int
v_support_subject_conf(struct cmd_param *param, const char *arg) {
	if (bconf_vget(bconf_root, "*", "*", "support", "subject", param->value, NULL) == NULL) {
		param->error = "ERROR_SUPPORT_SUBJECT_MISSING";
		param->cs->status = "TRANS_ERROR";
	}
	return 0;
}

struct validator v_support_title[] = {
	VALIDATOR_LEN(0, 255, "ERROR_SUPPORT_TITLE_INVALID"),
	{0}
};
ADD_VALIDATOR(v_support_title);

struct validator v_support_zipcode[] = {
	VALIDATOR_REGEX("^[[:blank:][:digit:]]*$", REG_EXTENDED, "ERROR_ZIPCODE_INVALID"),
	{0}
};
ADD_VALIDATOR(v_support_zipcode);

struct validator v_support_employer[] = {
	{0}
};
ADD_VALIDATOR(v_support_employer);

struct validator v_support_date[] = {
	VALIDATOR_LEN(-1, 50, "ERROR_DATE_TOO_LONG"),
        VALIDATOR_NREGEX("[%^/\\()=#+]", REG_EXTENDED, "ERROR_DATE_INVALID"),
	{0}
};
ADD_VALIDATOR(v_support_date);

struct validator v_support_body[] = {
	VALIDATOR_LEN(3, -1, "ERROR_ADREPLY_BODY_TOO_SHORT"),
	{0}
};
ADD_VALIDATOR(v_support_body);

struct validator v_support_comment[] = {
	VALIDATOR_LEN(3, -1, "ERROR_ADREPLY_BODY_TOO_SHORT"),
	{0}
};
ADD_VALIDATOR(v_support_comment);

struct validator v_answer[] = {
	VALIDATOR_LEN(1, 255, "ERROR_ANSWER_INVALID"),
	{0}
}; 
ADD_VALIDATOR(v_answer);

static int
v_support_extra_params(struct cmd_param *param, const char *arg) {
	const char *subject_id = param->value;


	struct bconf_node *extraparam = bconf_vget(bconf_root, "*", "*", "support", "subject", subject_id, "extraparam", NULL);

	if (extraparam) {
		int i;
		const char *paramname;
		for (i = 0; (paramname = bconf_value(bconf_byindex(extraparam, i))); i++) {
			struct bconf_node *eparam = bconf_vget(bconf_root, "*", "*", "support", "param", paramname, NULL);

			if (eparam) {
				struct d_param *dparam = zmalloc(sizeof(*dparam));

				transaction_logprintf(param->cs->ts, D_DEBUG, "Adding extra param %s.", paramname);

				/* Create a dynamic parameter. */
				dparam->name = xstrdup(paramname);
				dparam->flags = P_DYN_STATIC_VALIDATOR;
				if (bconf_get_int(eparam, "required"))
					dparam->flags |= P_REQUIRED;
				dparam->validators = load_validator(bconf_get_string(eparam, "validator"), NULL, 0);

				cmd_dyn_param(param->cs, dparam);
			}
		}
	}
	return 0;
}

struct validator v_support_subject[] = {
	VALIDATOR_INT(0, -1, "ERROR_SUPPORT_SUBJECT_MISSING"),
	VALIDATOR_FUNC(v_support_subject_conf),
	VALIDATOR_FUNC(v_support_extra_params),
	{0}
};
ADD_VALIDATOR(v_support_subject);




/* support file upload */
struct validator v_support_file[] = {
	VALIDATOR_LEN(-1, 10 * 1024 * 1024, "ERROR_FILE_TOO_BIG"),
	{0}
};
ADD_VALIDATOR(v_support_file);

static struct c_param parameters[] = {
	{"name",		P_REQUIRED, "v_name"},
	{"email",		P_REQUIRED, "v_email"},
	{"support_subject",		P_REQUIRED, "v_support_subject"},
	{"file",		0, "v_support_file"},
	{"filename_file",	0, "v_string_isprint"},
	{"title",		0, "v_support_title"},
	{"remote_addr",		P_REQUIRED, "v_ip"},
	{"remote_browser",	0, "v_remote_browser"},
	{"lang",		0, "v_lang"},
	{NULL, 0}
};

static char *support_fsm(void *, const char *);

/*****************************************************************************
 * support_done
 * Exit function.
 **********/
static void
support_done(void *v) {
	struct support_state *state = v;
	struct cmd *cs = state->cs;
	int i;

	for (i = 0; i < state->num_files; i++) {
		unlink(state->filename[i]);
		free(state->filename[i]);
	}
	free(state);
	cmd_done(cs);
}

/*
 * Upload a file to /tmp.
 */
static int
upload_file_save(struct support_state *state, const char *name) {
	struct cmd *cs = state->cs;
	const char *tmp_dir;
	ssize_t sz;
	int fd = -1;
	char errbuf[1024];
	int tries;
	const char *file;
	char *tp;
	char *filename;
	const char *up;

	if (!cmd_haskey(cs, name))
		return 0;

	if (state->num_files >= SUPPORT_MAX_FILES)
		return 1;

	up = strchr(name, '_');
	xasprintf(&filename, "filename_%s", up ? up + 1 : name);
	if (!cmd_haskey(cs, filename)) {
		transaction_printf(cs->ts, "%s:ERROR_SUPPORT_FILE_FAILED\n", name);
		cs->status = "TRANS_ERROR";
		free(filename);
		return 1;
	}

	file = cmd_getval(cs, filename);
	free(filename);

	tmp_dir = bconf_get_string(bconf_root, "*.*.support.tmpdir");
	state->filename[state->num_files] = xmalloc(strlen(tmp_dir) + 5 + strlen(file) + 1);
	tp = stpcpy(state->filename[state->num_files], tmp_dir);
	*tp++ = '/';
	strcpy(tp + 4, file);
	for (tries = 0; tries < 20 && fd == -1; tries++)  {
		int i;

		for (i = 0; i < 4; i++)
			tp[i] = 'a' + (unsigned)random() % 26;
		fd = open(state->filename[state->num_files], O_WRONLY | O_CREAT | O_EXCL, 0600);
	}
	if (fd < 0) {
		transaction_logprintf(cs->ts, D_ERROR, "mkstemp(%s) failed: %s", state->filename[state->num_files], strerror_r(errno, errbuf, sizeof(errbuf)));
		free(state->filename[state->num_files]);
		transaction_printf(cs->ts, "%s:ERROR_SUPPORT_FILE_FAILED\n", name);
		cs->status = "TRANS_ERROR";
		return 1;
	}

	sz = cmd_getsize(cs, name);
	if (write(fd, cmd_getval(cs, name), sz) < sz) {
		transaction_logprintf(cs->ts, D_ERROR, "write(%s) failed: %s", state->filename[state->num_files], strerror_r(errno, errbuf, sizeof(errbuf)));
		close(fd);
		unlink(state->filename[state->num_files]);
		free(state->filename[state->num_files]);
		transaction_printf(cs->ts, "%s:ERROR_SUPPORT_FILE_FAILED\n", name);
		cs->status = "TRANS_ERROR";
		return 1;
	}
		
	close(fd);
	state->num_files++;
	return 0;
}

static void
support_mail_cb(struct mail_data *md, void *v, int status) {
	struct support_state *state = v;
	struct cmd *cs = state->cs;

        if (status != 0) {
		transaction_logprintf(cs->ts, D_ERROR, "SUPPORT-SENDMAIL ERROR: Unable to send mail");
		support_fsm(state, "TRANS_MAIL_ERROR");
                return;
        }

        transaction_logprintf(cs->ts, D_INFO, "SUPPORT-SENDMAIL: 200 Mail sent OK");
	support_fsm(state, NULL);
}


static void
support_log_cb(struct mail_log *log, int id, const char *errstr) {
	struct support_state *state = log->data;
	free(log);
	support_fsm(state, errstr);
}

/*****************************************************************************
 * support_fsm
 * State machine
 **********/
static char *
support_fsm(void *v, const char *errstr) {
	struct support_state *state = v;
	struct cmd *cs = state->cs;

	/* 
	 * General error handling for db errors.
	 */
	if (errstr) {
                transaction_logprintf(cs->ts, D_ERROR, "support_fsm statement failed (%d, %s)", 
                                      state->state,
                                      errstr);
		if (strstr(errstr, "TRANS_MAIL_ERROR")) {
			cs->status = "TRANS_MAIL_ERROR";
		} else {
			cs->status = "TRANS_DATABASE_ERROR";
			cs->message = xstrdup(errstr);
		}
		state->state = SUPPORT_ERROR;
        }

	while (1) {
		transaction_logprintf(cs->ts, D_DEBUG, "support_fsm(%p, %p), state=%d", 
				      v, errstr, state->state);

		switch (state->state) {
		case SUPPORT_INIT:
		{
			struct mail_log *log = zmalloc(sizeof(*log));

			log->type = "support";
			log->remote_addr = cmd_getval(cs, "remote_addr");
			log->email = cmd_getval(cs, "email");
			log->spam_minutes = bconf_get_int(bconf_root, "*.*.support.spam.minutes");
			log->spam_counts[MAIL_SPAM_SEND] = bconf_get_int(bconf_root, "*.*.support.spam.send_count");
			log->spam_counts[MAIL_SPAM_RECV] = bconf_get_int(bconf_root, "*.*.support.spam.recv_count");
			log->cb = support_log_cb;
			log->data = state;

			log_mail(log);

			state->state = SUPPORT_SEND;
			return NULL;
		}

		case SUPPORT_SEND:
		{
			struct mail_data *md;
			struct bconf_node *node = bconf_vget(bconf_root, "*", "*", "support", "subject", cmd_getval(cs, "support_subject"), NULL);
			const char *mail_to = bconf_get_string(node, "email");
			const char *subject = bconf_get_string(node, "name");
			int i;
			struct cmd_param *cp;

			if (upload_file_save(state, "file")) {
				state->state = SUPPORT_ERROR;
				continue;
			}

			/* SEND THE MAIL */

			/* Send email */
			md = zmalloc(sizeof (*md));
			md->command = state;
			md->callback = support_mail_cb;
			md->log_string = cs->ts->log_string;

			/* Add email paramaters */
			mail_insert_param(md, "from", cmd_getval(cs, "email"));
			mail_insert_param(md, "subject", subject);
			mail_insert_param(md, "to", mail_to);
			mail_insert_param(md, "body", cmd_getval(cs, "support_body"));

			// Params below are already added on the next foreach
			//mail_insert_param(md, "name", cmd_getval(cs, "name"));
			//mail_insert_param(md, "remote_addr", cmd_getval(cs, "remote_addr"));

			TAILQ_FOREACH(cp, &cs->params, tq) {
				if (!strcmp(cp->key, "file"))
					continue;
				if (!strncmp(cp->key, "file_", 5)) {
					if (upload_file_save(state, cp->key)) {
						state->state = SUPPORT_ERROR;
						break;
					}
					continue;
				}
				mail_insert_param(md, cp->key, cp->value);
			}
			if (state->state == SUPPORT_ERROR) {
				mail_free(md);
				continue;
			}

			for (i = 0; i < state->num_files; i++) {
				mail_insert_param(md, "attachment", state->filename[i]);
			}

			/* Send mail */
			state->state = SUPPORT_DONE;
			const char *tmpl;
			if (!(tmpl = bconf_value(bconf_vget(bconf_root, "*", "*", "support", "subject", cmd_getval(cs, "support_subject"), "template", NULL))))
				tmpl = "mail/support_mail.txt";
			const char *default_lang = bconf_get_string(bconf_root, "*.*.common.default.lang");

			mail_insert_param(md, "wrapper_template", "mail/qp_wrapper.mime");
			mail_send(md, tmpl, cmd_haskey(cs, "lang")? cmd_getval(cs, "lang") : default_lang);
			transaction_logprintf(cs->ts, D_DEBUG, "Sending email with template \"%s\"", tmpl);

                        return NULL;
		}

		case SUPPORT_DONE:
			transaction_logprintf(cs->ts, D_DEBUG, "Support done");
			support_done(state);
			return NULL;

		case SUPPORT_ERROR:
			transaction_logprintf(cs->ts, D_ERROR, "Error state");
			support_done(state);
			return NULL;

		default:
			/* MAJOR STATE ERROR */
			cs->status = "TRANS_ERROR";
			transaction_logprintf(cs->ts, D_ERROR, "Unknown state (%d)", 
					      state->state);
			return NULL;

		}
	}
}

/*****************************************************************************
 * support
 * Main function that initialise a state-structure and calls the state machine.
 **********/
static void
support(struct cmd *cs) {
	transaction_logprintf(cs->ts, D_DEBUG, "Start Support transaction");
	struct support_state *state;
	state = zmalloc(sizeof(*state));
	state->cs = cs;
	state->state = SUPPORT_INIT;

	support_fsm(state, NULL);
}

ADD_COMMAND(support,     /* Name of command */
		NULL,
            support,     /* Main function name */
            parameters,  /* Parameters struct */
            "Support command");/* Command description */
