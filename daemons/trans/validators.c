#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include <ctype.h>
#include <time.h>

#include "trans.h"
#include "util.h"
#include <ctemplates.h>
#include <bconf.h>
#include "validators.h"
#include "settings_utils.h"
#include "trans_authenticate.h"
#include "filter.h"

#include "fd_pool.h"
#include "sredisapi.h"
#include "utils/redis_trans_context.h"

#define VAL_TOKEN_INIT          0
#define VAL_TOKEN_RESULT        1
#define VAL_TOKEN_DONE          2
#define VAL_TOKEN_ERROR         3

#define VBCONF_DATA_TYPE_INTEGER 0
#define VBCONF_DATA_TYPE_STRING  1

#define PRODUCT_MUST_NOT_EXIST  0
#define PRODUCT_MUST_EXIST 1

/*
 * void validate_on_db
 * Executes a query on the db and does basic error handling.
 * param: Structure passed along on validators
 * arg: Argument supplied to the validator function
 * key, value: Used to populate the bpapi. If you need more than 1, keep reading
 * config: Database configuration to use (pgsql.master|pgsql.slave)
 * sqltmpl: Template to perform the query
 * populate_bpapi: Function to add complex values to the bpapi. Overrides key and value.
 * on_error: Function executed if an error is returned from DB. If NULL, simple error handling will occur
 * validate_result: Callback to be executed after retrieving query results
 */
void
validate_on_db(struct cmd_param *param, const char *arg,
               const char *key, const char *value,
               const char *config, const char *sqltmpl,
               void (*populate_bpapi)(struct cmd_param *param, const char *arg, struct bpapi *ba),
               void (*on_error)(struct cmd_param *param, struct sql_worker *worker, const char *errstr),
               void (*validate_result)(struct cmd_param *param, struct sql_worker *worker, const char *arg))
{
	struct bpapi ba;
	struct sql_worker *worker;
	const char *errstr = NULL;

	BPAPI_INIT(&ba, NULL, pgsql);

	if (populate_bpapi)
		populate_bpapi(param, arg, &ba);
	else
		bpapi_insert(&ba, key, value);

	worker = sql_blocked_template_query(config, 0, sqltmpl, &ba, param->cs->ts->log_string, &errstr);
	bpapi_free(&ba);

	if (worker && sql_blocked_next_result(worker, &errstr) == 1)
		validate_result(param, worker, arg);

	if (errstr) {
		if (on_error) {
			on_error(param, worker, errstr);
		} else {
			free(param->cs->message);
			param->cs->status = "TRANS_DATABASE_ERROR";
			param->cs->message = xstrdup(errstr);
		}
	}

	if (worker)
		sql_worker_put(worker);
}

/*
 * static int validate_list
 * Runs a validation function for every item of a delimited list.
 * param: Structure passed along on validators
 * arg: Argument supplied to the validator function
 * delimiter: Set of characters considered as delimiters (see: strtok_r)
 * empty_value_error: If set and the value is empty, param->error will be set to this value
 * validate_item: Validation function to call on every item. Non zero return causes validation to fail.
 *                It is expected for this function to set the param->error before returning.
 */
static int
validate_list(struct cmd_param *param, const char *arg,
              const char *delimiter, const char *empty_value_error,
              int (*validate_item)(struct cmd_param *param, const char *arg, char *value))
{
	struct cmd *cs = param->cs;

	if (empty_value_error && (!param->value || param->value == '\0')) {
		param->error = empty_value_error;
		cs->status = "TRANS_ERROR";
		return 1;
	}

	char *value_cpy = xstrdup(param->value);
	char *freeptr = value_cpy;
	char *saveptr = NULL;
	char *token;

	while ((token = strtok_r(value_cpy, delimiter, &saveptr))) {
		value_cpy = NULL;
		int result = validate_item(param, arg, token);
		if (result) {
			cs->status = "TRANS_ERROR";
			free(freeptr);
			return 1;
		}
	}
	free(freeptr);
	return 0;
}

const char*
trans_key_lookup(const char *setting, const char *key, void *cbdata) {
	struct trans_key_lookup_data *data = cbdata;
	struct cmd_param *param = data->param;

	if (!strcmp(key, "parent")) {
		return bconf_value(bconf_vget(trans_bconf, "cat", cmd_getval(param->cs, "category"), "parent", NULL));
	}

	return cmd_getval(param->cs, key);
}

struct validator v_action_id[] = {
	VALIDATOR_INT(1, -1, "ERROR_ACTION_ID_INVALID"),
	{0}
};

ADD_VALIDATOR(v_action_id);

/*
 * Sell and subscription validation
 */
static int
vf_cat_car(struct cmd_param *param, const char *arg) {
	int ff_subscription = has_feature(param, "company_subscription");

	if (ff_subscription) {
		/*
		 * If the email is valid for overriding company_subscription validation then skip error
		 */
		const char *email = cmd_getval(param->cs, "email");
		if (email) {
			struct bconf_node *subscription_override = bconf_get(host_bconf, "*.common.subscription.override");

			if (subscription_override) {
				int i;
				const char *override_email;
				for (i = 0; (override_email = bconf_value(bconf_byindex(subscription_override, i))); i++) {
					if (strcasecmp(override_email, email) == 0)
						return 0;
				}
			}

			if (!cmd_haskey(param->cs, "external_ad_id")) {
				param->error = "ERROR_CATEGORY_SUBSCRIPTION";
				param->cs->status = "TRANS_ERROR";
			}
		}
        }

        return 0;
}

struct c_param car_param  = {"category", 0, "v_car"};

static int
vf_type_sell_subscription(struct cmd_param *param, const char *arg) {
        char type = param->value[0];

        if (type == 's') {
                cmd_extra_param(param->cs, &car_param);
        }

        return 0;
}

static void
vf_type_cb(const char *setting, const char *key, const char *value, void *cbdata) {
	struct trans_key_lookup_data *cb = cbdata;

	if (!strcmp(key, cb->has_param))
		cb->result = 1;
}


/*
 * Type check
 */
static int
vf_type_invalid(struct cmd_param *param, const char *arg) {
	struct trans_key_lookup_data cbdata;
	struct bpapi_vtree_chain vtree;

	cbdata.param = param;
	cbdata.has_param = param->value;
	cbdata.result = 0;

	get_settings(bconf_vtree(&vtree, bconf_get(trans_bconf, "category_settings")),
			"types", trans_key_lookup, vf_type_cb, &cbdata);
	vtree_free(&vtree);

	if (!cbdata.result) {
		param->error = "ERROR_TYPE_INVALID";
                param->cs->status = "TRANS_ERROR";
	}

	return 0;
}

/*
 * Region / City
 */
struct c_param city_param = {"city", 0, "v_city"};

/*
static int
vf_region_cities(struct cmd_param *param, const char *arg) {
	if (bconf_get(bconf_byname(bconf_root, "*.*.common.region", param->value), "cities") != NULL) {
		cmd_extra_param(param->cs, &city_param);
	}

	return 0;
}
*/

static int
vf_region_bconf(struct cmd_param *param, const char *arg) {
	if (bconf_vget(trans_bconf, "common", "region", param->value, "name", NULL) == NULL) {
		param->error = "ERROR_REGION_INVALID";
		param->cs->status = "TRANS_ERROR";
		return 1;
	}
	return 0;
}

static int
vf_commune_bconf(struct cmd_param *param, const char *arg) {
	if (strlen(param->value)==0)
		return 0;
	if (cmd_haskey(param->cs,"region")) {
		if (bconf_vget(trans_bconf, "common", "region",cmd_getval(param->cs,"region"), "commune", param->value, "name", NULL) == NULL) {
			const char *move2region = bconf_get_string(bconf_vget(trans_bconf, "common", "move2region", NULL), param->value);
			if (move2region) {
				char *m2region = xstrdup(move2region);
				// cmd_setval execute free on m2region
				transaction_logprintf(param->cs->ts, D_INFO, "Move2Region: %s", m2region);
				cmd_setval(param->cs, "region", m2region);
				return 0;
			}
			param->error = "ERROR_COMMUNE_INVALID";
			param->cs->status = "TRANS_ERROR";
			return 1;
		}
	} else {
		param->error = "ERROR_REGION_INVALID";
		param->cs->status = "TRANS_ERROR";
		return 1;
	}
	return 0;

}

struct category_param_data {
	struct  cmd_param *param;
	const char *param_name;
	int list_key_index;
	char *value;
};

struct validator_data {
	struct  cmd_param *param;
	const char *param_name;
	const char *key_name;
	const char *validator_type;
	char *regex;
	char *reqerrmsg;
	char *errmsg;
	char *nregex;
	char *nerrmsg;
	char *lerrmsg;
	char *require_or;
	char *require_if;
	char *optional_if;
	char *max_length;
	char *min_length;
	char *exists_if_has_key;
	struct bconf_node *list_path;
	int has_list_path;
	int popt;
	int list_from;
	int list_to;
	int v_flags;
	int list_key_index;
	char *sub_validator;
};

/**
	Frees all the dynamically allocated strings of the validator_data structure.
*/
static void
free_validator_data(struct validator_data *v_data) {
		free(v_data->regex);
		free(v_data->min_length);
		free(v_data->max_length);
		free(v_data->nregex);
		free(v_data->require_or);
		free(v_data->require_if);
		free(v_data->optional_if);
		free(v_data->reqerrmsg);
		free(v_data->nerrmsg);
		free(v_data->lerrmsg);
		free(v_data->errmsg);
		free(v_data->exists_if_has_key);
		free(v_data->sub_validator);
}

static const char*
category_param_key_lookup(const char *setting, const char *key, void *cbdata) {
	struct category_param_data *data = cbdata;
	char key_name[128];

	if (data->list_key_index) {
		snprintf(key_name, sizeof(key_name), "%s%d", key, data->list_key_index);
		if (cmd_getval(data->param->cs, key_name))
			return cmd_getval(data->param->cs, key_name);
	}

	if (!strcmp(key, "parent")) {
		return bconf_value(bconf_vget(trans_bconf, "cat", cmd_getval(data->param->cs, "category"), "parent", NULL));
	} else if (cmd_getval(data->param->cs, key)) {
		return cmd_getval(data->param->cs, key);
	} else if (key && strcmp("param", key) == 0) {
		return data->param_name;
	}

	return NULL;
}


static const char*
key_validator_lookup(const char *setting, const char *key, void *cbdata) {
	struct validator_data *v_data = (struct validator_data *) cbdata;
	char key_name[128];

	if (v_data->list_key_index) {
		snprintf(key_name, sizeof(key_name), "%s%d", key, v_data->list_key_index);
		if (cmd_getval(v_data->param->cs, key_name))
			return cmd_getval(v_data->param->cs, key_name);
	}

	if (!strcmp(key, "parent")) {
		return bconf_value(bconf_vget(trans_bconf, "cat", cmd_getval(v_data->param->cs, "category"), "parent", NULL));
	} else if (cmd_getval(v_data->param->cs, key)) {
		return cmd_getval(v_data->param->cs, key);
	} else if (key && strcmp("param", key) == 0) {
		return v_data->param_name;
	}

	return v_data->validator_type;
}

static int
is_param_optional(struct cmd_param *param, const char* param_name) {
	if (cmd_haskey(param->cs, "token") || strcmp(param->cs->command->name, "review") == 0) {
		if (cmd_haskey(param->cs, "auth_type") && strcmp(cmd_getval(param->cs, "auth_type"), "store") == 0) {
			return 0;
		} else {
			return 1;
		}
	} else if (cmd_haskey(param->cs, "addimages")
			|| (cmd_haskey(param->cs, "ad_type") && strcmp(cmd_getval(param->cs, "ad_type"), "gallery") == 0)) {
		return 1;
	} else {
		return 0;
	}

}

/*
 * Extra ad options
 */

static void
run_param_validators(struct validator_data *v_data) {
	struct validator *rval;
	struct validator *val;
	struct validator *rsubval = NULL;
	char *vname;
	int nval;
	int nrval;
	int optional = is_param_optional(v_data->param, v_data->param_name);
	struct d_param *dparam;
	int res;
	int nsubval = 0;

	/* No need for validation if key doesn't exists */
	if (v_data->exists_if_has_key && !cmd_haskey(v_data->param->cs, v_data->exists_if_has_key))
		return;

	/* Create a dynamic parameter. */
	dparam = zmalloc(sizeof(*dparam));
	dparam->name = xstrdup(v_data->key_name);

	/* Check if there's an available validator. */
	ALLOCA_PRINTF(res, vname, "v_%s", v_data->param_name);
	rval = load_validator(vname, NULL, 1);

	nval = 0;
	if (rval) {
		for (val = rval; val->type; val++)
			nval++;
	}
	nrval = nval;

	/* Load sub validator */
	if (v_data->sub_validator) {
		rsubval = load_validator(v_data->sub_validator, NULL, 1);
		if (rsubval) {
			for (val = rsubval; val->type; val++) {
				nval++;
				nsubval++;
			}
		}
	}
	/* Create "required or" */
	if (v_data->require_or && !optional){
		struct d_param *reqor_param = zmalloc(sizeof (*reqor_param));

		xasprintf(&reqor_param->name, "%s;%s", v_data->param_name, v_data->require_or);
		reqor_param->flags = P_XOR;
		reqor_param->validators = NULL;
		transaction_logprintf(v_data->param->cs->ts, D_DEBUG, "Dynamic parameter added required-or validator (%s)", reqor_param->name);
		cmd_dyn_param(v_data->param->cs, reqor_param);
	} else if (v_data->require_if && cmd_haskey(v_data->param->cs, v_data->require_if))
		v_data->popt = 0;
	else if (v_data->optional_if && cmd_haskey(v_data->param->cs, v_data->optional_if))
		v_data->popt = 1;

	if (!optional && !v_data->popt)
		nval++;
	if (v_data->nregex)
		nval++;
	if (v_data->regex)
		nval++;
	if (v_data->has_list_path)
		nval++;
	if (v_data->min_length && v_data->max_length)
		nval++;

	/* v_data string members are free'd later on, so strings need to be strdup'ed and the STR_ALLOCED flag set */
	if (nval) {
		val = zmalloc(sizeof(*val) * (nval + 1));
		nval = 0;
		if (!optional && !v_data->popt) {
			struct validator req_val = VALIDATOR_REQUIRED(xstrdup(v_data->reqerrmsg ? v_data->reqerrmsg : "ERROR_REQUIRED"));
			req_val.v_flags |= v_data->v_flags | STR_ALLOCED;
			val[nval++] = req_val;
		}
		if (v_data->nregex) {
			struct validator nregex_val = VALIDATOR_NREGEX(xstrdup(v_data->nregex), REG_EXTENDED|REG_NOSUB, xstrdup(v_data->nerrmsg ? v_data->nerrmsg : "ERROR_INVALID"));
			nregex_val.v_flags |= v_data->v_flags | STR_ALLOCED;
			val[nval++] = nregex_val;
		}
		if (v_data->regex) {
			struct validator regex_val = VALIDATOR_REGEX(xstrdup(v_data->regex), REG_EXTENDED|REG_NOSUB, xstrdup(v_data->errmsg ? v_data->errmsg : "ERROR_INVALID"));
			regex_val.v_flags |= v_data->v_flags | STR_ALLOCED;
			val[nval++] = regex_val;
		}
		if (v_data->min_length && v_data->max_length) {
			struct validator length_val = VALIDATOR_LEN(atoi(v_data->min_length), atoi(v_data->max_length), xstrdup(v_data->lerrmsg ? v_data->lerrmsg : "ERROR_INVALID"));
			length_val.v_flags |= v_data->v_flags | STR_ALLOCED;
			val[nval++] = length_val;
		}
		if (v_data->has_list_path) {
			struct validator list_path_val = VALIDATOR_BCONF_KEY(v_data->list_path, xstrdup(v_data->errmsg ? v_data->errmsg : "ERROR_INVALID"));
			list_path_val.v_flags |= v_data->v_flags | STR_ALLOCED;
			val[nval++] = list_path_val;
		}

		if (rsubval) {
			memcpy(val + nval, rsubval, sizeof(*val) * nsubval);
			nval++;
		}

		if (rval)
			memcpy(val + nval, rval, sizeof(*val) * nrval);
		dparam->validators = val;
	}

	cmd_dyn_param(v_data->param->cs, dparam);
}

static void
get_param_value_cb(const char *setting, const char *key, const char *value, void *cbdata) {
	struct category_param_data *data = cbdata;

	if (!key)
		return;

	if (strcmp(key, data->param_name) != 0)
		return;

	if (value)
		data->value = xstrdup(value);
}

/**
	Don't forget to update free_validator_data() if you add entries here.
*/
static void
create_settings_validator_cb(const char *setting, const char *key, const char *value, void *cbdata) {
	struct validator_data *v_data = (struct validator_data *) cbdata;

	if (!key)
		return;

	if ((strcmp(v_data->validator_type, "optional") == 0))
		v_data->popt = atoi(key);
	else if ((strcmp(v_data->validator_type, "regex") == 0))
		v_data->regex = xstrdup(key);
	else if ((strcmp(v_data->validator_type, "min_length") == 0))
		v_data->min_length = xstrdup(key);
	else if ((strcmp(v_data->validator_type, "max_length") == 0))
		v_data->max_length = xstrdup(key);
	else if ((strcmp(v_data->validator_type, "nregex") == 0))
		v_data->nregex = xstrdup(key);
	else if ((strcmp(v_data->validator_type, "require_or") == 0))
		v_data->require_or = xstrdup(key);
	else if ((strcmp(v_data->validator_type, "require_if") == 0))
		v_data->require_if = xstrdup(key);
	else if ((strcmp(v_data->validator_type, "optional_if") == 0))
		v_data->optional_if = xstrdup(key);
	else if ((strcmp(v_data->validator_type, "req_error") == 0))
		v_data->reqerrmsg = xstrdup(key);
	else if ((strcmp(v_data->validator_type, "nerror") == 0))
		v_data->nerrmsg = xstrdup(key);
	else if ((strcmp(v_data->validator_type, "lerror") == 0))
		v_data->lerrmsg = xstrdup(key);
	else if ((strcmp(v_data->validator_type, "error") == 0))
		v_data->errmsg = xstrdup(key);
	else if ((strcmp(v_data->validator_type, "list_path") == 0)) {
		struct bconf_node *root = v_data->has_list_path ? v_data->list_path : trans_bconf;
		if (strcmp(key, "param") == 0) {
			v_data->list_path = bconf_get(root, cmd_getval(v_data->param->cs, value));
		} else
			v_data->list_path = bconf_get(root, key);
		if (!v_data->has_list_path)
			v_data->has_list_path = 1;
	} else if ((strcmp(v_data->validator_type, "exists_if_has_key") == 0))
		v_data->exists_if_has_key = xstrdup(key);
	else if ((strcmp(v_data->validator_type, "list_from") == 0))
		v_data->list_from = atoi(key);
	else if ((strcmp(v_data->validator_type, "list_to") == 0))
		v_data->list_to = atoi(key);
	else if ((strcmp(v_data->validator_type, "list_path_is_key") == 0))
		v_data->v_flags |= VFLAG_BCONF_ISKEY;
	else if ((strcmp(v_data->validator_type, "sub_validator") == 0) && key[0])
		v_data->sub_validator = xstrdup(key);
}


static char *
validate_category_params_cb(void *v, int optional, int add_default_params, const char *features, const char *list_idx) {
	struct cmd_param *param = (struct cmd_param*)v;
	struct bconf_node *adproot = bconf_get(trans_bconf, "common.ad_params");
	struct bconf_node *adparam;
	struct bconf_node *valroot = bconf_get(trans_bconf, "common.validator");
	struct bconf_node *validator_type;
	int i, j;

	for (i = 0; (adparam = bconf_byindex(adproot, i)); i++) {
		const char *name = bconf_get_string(adparam, "name");
		const char *default_param = bconf_get_string(adparam, "default");
		struct bconf_node *list = bconf_get(adparam, "list");
		const char *type = bconf_get_string(adparam, "type");

		const char *endname;

		if (!name)
			continue;

		endname = name + strlen(name);

		/* Check that param is in the feature list for this category.
		   Or if the param is a default enabled param. */
		if ((add_default_params && default_param && !strcmp(default_param,"1")) || (features && strstrptrs(features, name, endname, ","))) {
			int from;
			int to;
			int i;
			char namebuf[endname - name + 3];
			struct validator_data v_data = {0};

			v_data.param = param;
			v_data.param_name = name;

			/* Get from and to if list */
			if (list) {
				struct bpapi_vtree_chain vtree;

				v_data.validator_type = "list_from";
				get_settings(bconf_vtree(&vtree, bconf_get(trans_bconf, "validator_settings")), name, key_validator_lookup,
					     create_settings_validator_cb, &v_data);
				vtree_free(&vtree);
				v_data.validator_type = "list_to";
				get_settings(bconf_vtree(&vtree, bconf_get(trans_bconf, "validator_settings")), name, key_validator_lookup,
					     create_settings_validator_cb, &v_data);
				vtree_free(&vtree);
				if (v_data.list_from)
					from = v_data.list_from;
				else
					from = bconf_get_int(list, "from");
				if (v_data.list_to)
					to = v_data.list_to;
				else
					to = bconf_get_int(list, "to");
			} else
				from = to = 0;

			for (i = from; i <= to; i++) {
				memset(&v_data, 0, sizeof(v_data));
				struct bpapi_vtree_chain vtree;

				v_data.param = param;
				v_data.param_name = name;
				v_data.list_key_index = i;

				/* Look for a sub validator */
				v_data.validator_type = "sub_validator";
				get_settings(bconf_vtree(&vtree, bconf_get(trans_bconf, "validator_settings")), name, key_validator_lookup,
					     create_settings_validator_cb, &v_data);
				vtree_free(&vtree);


				for (j = 0; (validator_type = bconf_byindex(valroot, j)); j++) {
					v_data.validator_type = bconf_get_string(validator_type, "data");
					get_settings(bconf_vtree(&vtree, bconf_get(trans_bconf, "validator_settings")), name, key_validator_lookup,
						     create_settings_validator_cb, &v_data);
					vtree_free(&vtree);
				}

				if (v_data.list_path && list_idx)
					v_data.list_path = bconf_get(v_data.list_path, list_idx);
				else if (v_data.list_path) {
					struct category_param_data cpd = {0};
					cpd.list_key_index = i;
					cpd.param = param;
					cpd.param_name = name;
					get_settings(bconf_vtree(&vtree, bconf_get(trans_bconf, "category_settings")), "params", category_param_key_lookup, get_param_value_cb, &cpd);
					vtree_free(&vtree);
					if (cpd.value) {
						v_data.list_path = bconf_get(v_data.list_path, cpd.value);
						free(cpd.value);
					}
				}


				if (type && strcmp(type, "checklist") == 0)
					v_data.v_flags |= VFLAG_COMMALIST;

				/* Require if is automatically set to the corresponding list key for list params */
				char *require_if = v_data.require_if;
				int require_if_len = require_if ? strlen(require_if) : 0;
				char require_if_buf[require_if_len + 3];

				if (i) {
					snprintf(namebuf, endname - name + 3, "%s%d", name, i);
					if (i > from && bconf_get_int(list, "check_order") && !cmd_haskey(param->cs, namebuf))
						break;

					v_data.key_name = namebuf;

					if (require_if) {
						snprintf(require_if_buf, require_if_len + 3, "%s%d", require_if, i);
						v_data.require_if = require_if_buf;
					}

				} else
					v_data.key_name = name;

				run_param_validators(&v_data);
				v_data.require_if = require_if;
				free_validator_data(&v_data);
			}
		}
	}

	return NULL;
}

static struct c_param pay_type_param = {"pay_type", 0, "v_pay_type"};

static void
add_settings_validator_cb(const char *setting, const char *key, const char *value, void *cbdata) {
	struct trans_key_lookup_data *data = cbdata;
	struct cmd_param *param = data->param;

	transaction_logprintf(param->cs->ts, D_DEBUG, "Adding param %s", key);
	validate_category_params_cb(param, data->optional, 1, key, value);
}


static void
has_param_validator_cb(const char *setting, const char *key, const char *value, void *cbdata) {
	struct trans_key_lookup_data *data = cbdata;


	if (!strcmp(key, data->has_param))
		data->result = 1;
}

static void
new_extra_params(struct cmd_param *param, int optional) {
	struct trans_key_lookup_data cbdata;
	struct bpapi_vtree_chain vtree;

	/*
	 * XXX Find some other way
	 */
	if (!optional)
		cmd_extra_param(param->cs, &pay_type_param);

	/* Adding setting features */
	cbdata.param = param;
	cbdata.optional = optional;

	get_settings(bconf_vtree(&vtree, bconf_get(trans_bconf, "category_settings")), "params", trans_key_lookup, add_settings_validator_cb, &cbdata);
	vtree_free(&vtree);
}

static void
vf_newad_params_cb(struct cmd_param *param, struct sql_worker *worker, const char *arg) {
	if (worker->next_row(worker)) {
		int optional = 0;
		const char *auth_type = worker->get_value_byname(worker, "o_auth_type");
		if (strcmp(auth_type, "admin") == 0)
			optional = 1;
		new_extra_params(param, optional);
	}
}

static int
vf_newad_params(struct cmd_param *param, const char *arg) {
	if (cmd_haskey(param->cs, "external_ad_id") || strcmp(param->cs->command->name, "review") == 0) {
		new_extra_params(param, 1);
	} else if (cmd_haskey(param->cs, "token")) {
		validate_on_db(param, arg,
			"token", cmd_getval(param->cs, "token"),
			"pgsql.master", "sql/call_get_token_auth.sql",
			NULL, NULL, vf_newad_params_cb
		);
	} else {
		new_extra_params(param, 0);
	}
	return 0;
}

static int
vf_new_category(struct cmd_param *param, const char *arg) {
	if(bconf_get_int(bconf_vget(trans_bconf, "cat", param->value, NULL), "leaf")){
		return 0;
	}
	param->error = "ERROR_CATEGORY_INVALID";
	param->cs->status = "TRANS_ERROR";
	return 1;
}

/*
 * User blocked
 */

static struct c_param email_param = {"email", 	0, "v_email_required_checkblocked"};
static struct c_param import_email_param = {"email", 0, "v_email_basic"};

static int
vf_check_email(struct cmd_param *param, const char *arg) {
	if (cmd_haskey(param->cs, "external_ad_id"))
		cmd_extra_param(param->cs, &import_email_param);
	else
		cmd_extra_param(param->cs, &email_param);
	return 0;
}

/*
 * Company
 */
static void
vf_company_ad_subscription_cb(struct cmd_param *param, struct sql_worker *worker, const char *arg) {

	int store = 0;
	int voucher = -1;

	if (worker->rows(worker) > 0) {
		transaction_printf(param->cs->ts, "stores:");
		while (worker->next_row(worker)) {
			if (cmd_haskey(param->cs, "store") && strcmp(cmd_getval(param->cs, "store"), worker->get_value(worker, 0)) == 0) {
				store = 1;
				if (!worker->is_null(worker, 2))
					voucher = atoi(worker->get_value(worker, 2));
			} else if (worker->rows(worker) == 1 && !worker->is_null(worker, 2)) {
				voucher = atoi(worker->get_value(worker, 2));
			}
			transaction_printf(param->cs->ts, "%s:%s\t",
					   worker->get_value(worker, 0), worker->get_value(worker, 1));
		}
		transaction_printf(param->cs->ts, "\n");
		if (voucher != -1)
			transaction_printf(param->cs->ts, "voucher:%d\n", voucher);

		if (worker->rows(worker) > 1 && (!cmd_haskey(param->cs, "ad_type") || (strncmp(cmd_getval(param->cs, "ad_type"), "edit", 4) != 0 && strncmp(cmd_getval(param->cs, "ad_type"), "renew", 5) != 0) || cmd_haskey(param->cs, "token"))) {
			if (cmd_haskey(param->cs, "store")) {
				if (!store) {
					param->cs->status = "TRANS_ERROR";
					param = cmd_getparam(param->cs, "store");
					if (atoi(cmd_getval(param->cs, "store")) > 0) {
						param->error = "ERROR_STORE_INVALID";
					} else {
						param->error = "ERROR_STORE_MISSING";
					}
				}
			} else {
				param->cs->status = "TRANS_ERROR";
				transaction_printf(param->cs->ts, "store:%s\n", "ERROR_STORE_MISSING");
			}
		}
	}
}

static int
vf_check_store(struct cmd_param *param, const char *arg) {
	validate_on_db(param, arg,
		"email", cmd_getval(param->cs, "email"),
		"pgsql.master", "sql/check_store.sql",
		NULL, NULL, vf_company_ad_subscription_cb
	);
	return 0;
}

static struct c_param store_param  = {"email", 0, "v_store"};
static struct c_param phone_param  = {"phone", P_REQUIRED, "v_phone_basic"};
static struct c_param sell_param  = {"type", 0, "v_type_sell"};

static int
vf_company_ad_subscription(struct cmd_param *param, const char *arg) {
        int is_company_ad = atoi(param->value);

	if (is_company_ad && !cmd_haskey(param->cs, "external_ad_id")) {
                cmd_extra_param(param->cs, &sell_param);
                cmd_extra_param(param->cs, &store_param);
	}
	if (!is_company_ad || !cmd_haskey(param->cs, "link_type") || !bconf_vget(host_bconf, "*.common.import_partner", cmd_getval(param->cs, "link_type"), "no_phone", NULL))
		cmd_extra_param(param->cs, &phone_param);

	return 0;
}

/*
 * Ad subject
 */

/*
 * CHUS 289 ET 3: Allow write the regdate (year for cars) in the subject
 * If you dont need this function, remove it !
 * I'm so lazy to validate if this is needed in other place
 */
static int
vf_subject_regdate(struct cmd_param *param, const char *arg) {
	struct trans_key_lookup_data cbdata;
	struct bpapi_vtree_chain vtree;

	/* This logic is handled in the import script, so allow these ads */
	if(cmd_getval(param->cs, "link_type"))
		return 0;

	cbdata.param = param;
	cbdata.has_param = "regdate";
	cbdata.result = 0;

	get_settings(bconf_vtree(&vtree, bconf_get(trans_bconf, "category_settings")), "params", trans_key_lookup, has_param_validator_cb, &cbdata);
	vtree_free(&vtree);

	return 0;
}

/*
 * Ad body
 */
static int
vf_body_or_item_required(struct cmd_param *param, const char *arg) {
	char *value_buf = NULL;

	if (get_feature_value(param, "body_items", &value_buf)) {
		/* Body should not be sent */
		if (param->value) {
			free(value_buf);
			param->error = "ERROR_NO_SUCH_PARAMETER";
			param->cs->status = "TRANS_ERROR";
			return 1;
		}

		/* Check for required item */
		if (!cmd_haskey(param->cs, value_buf)) {
			char *value_buf_end;

			transaction_printf(param->cs->ts, "%s:", value_buf);
			value_buf_end = value_buf + strlen(value_buf) - 1;
			while (isdigit(*value_buf_end))
				*value_buf_end-- = '\0';
			strmodify(value_buf, toupper);
			transaction_printf(param->cs->ts, "ERROR_%s_MISSING\n", value_buf);
			free(value_buf);
			param->cs->status = "TRANS_ERROR";
			return 1;
		}

		free(value_buf);
	} else if (!param->value) {
		transaction_printf(param->cs->ts, "body:ERROR_BODY_MISSING\n");
		param->cs->status = "TRANS_ERROR";
		return 1;
	}

	return 0;
}


/*
 *  Infopage and its title - Check that both (or none) page and title exists
 */

static struct c_param infopage_title_param  = {"infopage_title", P_REQUIRED, "v_none"};
static struct c_param infopage_param  = {"infopage", P_REQUIRED, "v_none"};

static int
vf_check_infopage(struct cmd_param *param, const char *arg) {
	if (!has_feature(param,"no_infopage_title") && param->value && param->value[0]) {
		cmd_extra_param(param->cs, &infopage_title_param);
	}

	return 0;
}

static int
vf_check_infopage_title(struct cmd_param *param, const char *arg) {
	cmd_extra_param(param->cs, &infopage_param);

	return 0;
}

struct validator v_type_sell[] = {
	VALIDATOR_FUNC(vf_type_sell_subscription),
        {0}
};
ADD_VALIDATOR(v_type_sell);

struct validator v_type[] = {
	VALIDATOR_REGEX("^[ksuhb]$", REG_EXTENDED|REG_NOSUB, "ERROR_TYPE_INVALID"),
	VALIDATOR_FUNC(vf_type_invalid),
	{0}
};
ADD_VALIDATOR(v_type);

struct validator v_uid[] = {
	VALIDATOR_INT(1, -1, "ERROR_UID_INVALID"),
{0}
};

ADD_VALIDATOR(v_uid);

/*
 * Region / City
 */
struct validator v_city[] = {
        VALIDATOR_NREGEX("^0$", REG_EXTENDED|REG_NOSUB, "ERROR_CITY_MISSING"),
	{0}
};
ADD_VALIDATOR(v_city);


/*
 * Extra features
 */

static int
vf_check_weekclash(struct cmd_param *param, const char *arg) {
	if (cmd_haskey(param->cs, "available_weeks_peakseason")) {
		const char *peak = cmd_getval(param->cs, "available_weeks_peakseason");
		char *pos;
		char *curr = param->value;

		while ((pos = strchr(curr, ','))) {
			if (strstrptrs(peak, curr, pos, ",")) {
				param->error = "ERROR_WEEK_CLASH";
				param->cs->status = "TRANS_ERROR";
				return 1;
			}
			curr = pos + 1;
		}

		if (strstrptrs(peak, curr, curr + strlen(curr), ",")) {
			param->error = "ERROR_WEEK_CLASH";
			param->cs->status = "TRANS_ERROR";
			return 1;
		}
	}

	return 0;
}

struct validator v_available_weeks_offseason[] = {
	VALIDATOR_FUNC(vf_check_weekclash),
	{0}
};
ADD_VALIDATOR(v_available_weeks_offseason);

/*
 * Transbank answer validator
 */
struct validator v_tbk_answer[] = {
	VALIDATOR_INT(-9, 9, "ERROR_TRANSBANK_ANSWER_INVALID"),
{0}
};

ADD_VALIDATOR(v_tbk_answer);

/*
 * Validators for plates
 */
static struct c_param plates_for_cars[] = {
	{"plates", P_OPTIONAL, "v_plates_for_cars"},
	{"plates", P_OPTIONAL, "v_plates_for_trucks"},
	{"plates", P_OPTIONAL, "v_plates_for_motorcycles"},
	{NULL}
};

static struct c_param plates_for_cars_required[] = {
	{"plates", P_REQUIRED, "v_plates_for_cars"},
	{"plates", P_REQUIRED, "v_plates_for_trucks"},
	{"plates", P_REQUIRED, "v_plates_for_motorcycles"},
	{NULL}
};

struct validator v_plates_for_cars[] = {
	VALIDATOR_REGEX("^[A-Za-z]{2}([A-Za-z]{2}|[0-9]{2})[0-9]{2}$", REG_EXTENDED|REG_NOSUB|REG_ICASE, "ERROR_PLATES_CARS_INVALID"),
	{0}
};
ADD_VALIDATOR(v_plates_for_cars);

struct validator v_plates_for_trucks[] = {
	VALIDATOR_REGEX("^[A-Za-z]{2}([A-Za-z]{2}|[0-9]{2})[0-9]{2}$", REG_EXTENDED|REG_NOSUB|REG_ICASE, "ERROR_PLATES_TRUCKS_INVALID"),
	{0}
};
ADD_VALIDATOR(v_plates_for_trucks);

struct validator v_plates_for_motorcycles[] = {
	VALIDATOR_REGEX("^[A-Za-z]{2}([A-Za-z]{1}0?|[0-9]{1})[0-9]{2}$", REG_EXTENDED|REG_NOSUB|REG_ICASE, "ERROR_PLATES_MOTO_INVALID"),
	{0}
};
ADD_VALIDATOR(v_plates_for_motorcycles);

static int
vf_newad_plates_for_category(struct cmd_param *param, const char *arg) {
	const char *category = cmd_getval(param->cs, "category");
	const char *plates_validator = bconf_vget_string(trans_bconf, "plates_validator", category, NULL);
	struct c_param *plates_validator_type = plates_for_cars;

	int import_plates_required = bconf_vget_int(trans_bconf, "validator_settings.plates.1.required_import.value", NULL);

	if (cmd_haskey(param->cs, "external_ad_id") && import_plates_required) {
		plates_validator_type = plates_for_cars_required;
	}

	if (plates_validator) {
		struct c_param *pfc;
		for (pfc = plates_validator_type; pfc->name; pfc++) {
			if (!strcmp(pfc->validators, plates_validator)) {
				cmd_extra_param(param->cs, pfc);
				return 0;
			}
		}
	}

	return 0;
}

static int
vf_built_year_check(struct cmd_param *param, const char *arg) {
	const char* year = param->value;
	if (year) {
		int built_year = atoi(year);
		time_t t = time(NULL);
		struct tm tm = *localtime(&t);
		int current_year = tm.tm_year + 1900;

		if (built_year > current_year) {
			param->error = "ERROR_BUILT_YEAR_INVALID";
			param->cs->status = "TRANS_ERROR";
		}
	}
	return 0;
}

struct validator v_built_year_check[] = {
	VALIDATOR_FUNC(vf_built_year_check),
	{0}
};
ADD_VALIDATOR(v_built_year_check);

/*
 * Category
 */

static void
check_category_allowed_cb(struct cmd_param *param, struct sql_worker *worker, const char *arg) {

	const char *import_types = NULL;
	const char *category = cmd_getval(param->cs, "category");
	const char *allowed_categories;
	const char *parent = NULL;
	const char *curr;
	char *pos;
	int found = 0;

	if (bconf_get_int(bconf_vget(trans_bconf, "cat", category, NULL), "level") == 2)
		parent = bconf_value(bconf_vget(trans_bconf, "cat", category, "parent", NULL));

	/* Get import types for store from db */
	if (!worker->next_row(worker) || (import_types = worker->get_value(worker, 0)) == NULL) {
		/* Missing import_type for this store (but link_type was set so we know this is a imported ad). */
		param->cs->status = "TRANS_ERROR";
		param->error = "ERROR_MISSING_IMPORT_TYPE";
		return;
	}

	curr = import_types;

	/* for each import_type */
	while ((pos = strchr(curr, ','))) {
		*pos = '\0';

		/* Get list of allowed categories for import_types from bconf */
		allowed_categories = bconf_value(bconf_vget(trans_bconf, "common", "stores", "import_type", curr, "categories", NULL));

		if (allowed_categories && strstrptrs(allowed_categories, category, category + strlen(category), ",")) {
			found = 1;
		}

		if (parent &&
		    allowed_categories &&
		    strstrptrs(allowed_categories, parent, parent + strlen(parent), ",")) {
			found = 1;
		}

		curr = pos + 1;
	}
	allowed_categories = bconf_value(bconf_vget(trans_bconf, "common", "stores", "import_type", curr, "categories", NULL));

	if (allowed_categories && strstrptrs(allowed_categories, category, category + strlen(category), ",")) {
		found = 1;
	}

	if (parent &&
	    allowed_categories &&
	    strstrptrs(allowed_categories, parent, parent + strlen(parent), ",")) {
		found = 1;
	}

	if (!found) {
		/* Category not allowed for this store */
		param->cs->status = "TRANS_ERROR";
		param->error = "ERROR_NOT_ALLOWED";
	}
}

static int
vf_category_import_type(struct cmd_param *param, const char *arg) {
	/* If it is an imported ad, link_type is set. Check if this store is allowed in selected category */
	if (cmd_haskey(param->cs, "link_type") && cmd_haskey(param->cs, "store")) { 
		validate_on_db(param, arg,
			"store_id", cmd_getval(param->cs, "store"),
			"pgsql.master", "sql/call_get_token_auth.sql",
			NULL, NULL, check_category_allowed_cb
		);
	}
	return 0;
}

/*
 * CATEGORY VALIDATION
 */
struct validator v_category_no_params[] = {
	VALIDATOR_NREGEX("^0$", REG_EXTENDED|REG_NOSUB, "ERROR_CATEGORY_MISSING"),
	VALIDATOR_INT(1, -1, "ERROR_CATEGORY_INVALID"),
	{0}
};
ADD_VALIDATOR(v_category_no_params);

static struct validator v_none[] = {
        {0}
};
ADD_VALIDATOR(v_none);

static struct c_param new_category  = {"category", P_REQUIRED, "v_new_category"};

static int
vf_category_check(struct cmd_param *param, const char *arg) {
	cmd_extra_param(param->cs, &new_category);

	return 0;
}

static struct c_param mandatory_price = {"price", P_REQUIRED, "v_mandatory_check"};

static int
vf_mandatory_price(struct cmd_param *param, const char *arg) {
	const char *category = param->value;
	if (bconf_vget_int(trans_bconf, "cat", category, "price", "required", NULL) == 1)
		cmd_extra_param(param->cs, &mandatory_price);
	return 0;
}

static struct c_param price_length_per_cat = {"price", 0, "v_price_length_per_cat"};

static int
vf_price_length_per_category(struct cmd_param *param, const char *arg) {
	const char *category = param->value;
	struct bconf_node *min_length = bconf_vget(trans_bconf, "cat", category, "price", "min_length", NULL);
	if (min_length)
		cmd_extra_param(param->cs, &price_length_per_cat);
	return 0;
}

struct validator v_category_check[] = {
	VALIDATOR_FUNC(vf_category_check),
	VALIDATOR_FUNC(vf_mandatory_price),
	VALIDATOR_FUNC(vf_price_length_per_category),
	{0}
};
ADD_VALIDATOR(v_category_check);

struct validator v_newad_params[] = {
	VALIDATOR_FUNC(vf_newad_params),
	VALIDATOR_FUNC(vf_newad_plates_for_category),
	{0}
};
ADD_VALIDATOR(v_newad_params);

struct validator v_new_category[] = {
	VALIDATOR_REGEX("^[[:digit:]]{4}$", REG_EXTENDED|REG_NOSUB, "ERROR_CATEGORY_INVALID"),
	VALIDATOR_FUNC(vf_new_category),
	VALIDATOR_FUNC(vf_category_import_type),
	{0}
};
ADD_VALIDATOR(v_new_category);

/*
 * COUNTRY VALIDATION
 */
struct validator v_country_no_params[] = {
	VALIDATOR_NREGEX("^0$", REG_EXTENDED|REG_NOSUB, "ERROR_CATEGORY_MISSING"),
	VALIDATOR_LEN(2, -1, "ERROR_COUNTRY_SHORT"),
	VALIDATOR_LEN(-1, 3, "ERROR_COUNTRY_TOO_LONG"),
	{0}
};
ADD_VALIDATOR(v_country_no_params);

/*
 * Pay validation
 */
struct validator v_pay_type[] = {
	VALIDATOR_REQUIRED_EXECUTION("ERROR_PAY_TYPE_MISSING"),
	VALIDATOR_REGEX("^(verify|card|phone|voucher|paycard_save|voucher_save|campaign|free)$", REG_EXTENDED|REG_NOSUB, "ERROR_PAY_TYPE_INVALID"),
	{0}
};
ADD_VALIDATOR(v_pay_type);

struct validator v_pay_type_not_required[] = {
	VALIDATOR_REGEX("^(verify|card|phone|voucher|free)$", REG_EXTENDED|REG_NOSUB, "ERROR_PAY_TYPE_INVALID"),
	{0}
};
ADD_VALIDATOR(v_pay_type_not_required);

/*
 * Password
 */

struct validator v_passwd[] = {
	VALIDATOR_REQUIRED_EXECUTION("ERROR_PASSWORD_MISSING"),
	VALIDATOR_REGEX("^\\$[0-9]+\\$.{16}[0-9A-Fa-f]{40}$", REG_EXTENDED, "ERROR_PASSWORD_INVALID"),
	{0}
};
ADD_VALIDATOR(v_passwd);

struct validator v_passwd_optional[] = {
	VALIDATOR_REGEX("^\\$[0-9]+\\$.{16}[0-9A-Fa-f]{40}$", REG_EXTENDED, "ERROR_PASSWORD_INVALID"),
	{0}
};
ADD_VALIDATOR(v_passwd_optional);

struct validator v_passwd_not_required[] = {
	VALIDATOR_LEN(5, -1, "ERROR_PASSWORD_TOO_SHORT"),
	{0}
};
ADD_VALIDATOR(v_passwd_not_required);

struct validator v_passwd_sha1_not_required[] = {
	VALIDATOR_NREGEX("^$", REG_EXTENDED|REG_NOSUB, "ERROR_PASSWORD_MISSING"),
        VALIDATOR_REGEX(".{40}", REG_EXTENDED|REG_NOSUB|REG_ICASE, "ERROR_PASSWORD_INVALID"),
	{0}
};
ADD_VALIDATOR(v_passwd_sha1_not_required);

struct validator v_passwd_sha1[] = {
	VALIDATOR_REQUIRED_EXECUTION("ERROR_PASSWORD_MISSING"),
        VALIDATOR_REGEX(".{40}", REG_EXTENDED|REG_NOSUB|REG_ICASE, "ERROR_PASSWORD_INVALID"),
	{0}
};
ADD_VALIDATOR(v_passwd_sha1);

/*
 * Nickname
 */
struct validator v_nickname[] = {
	VALIDATOR_LEN(2, -1, "ERROR_NICKNAME_TOO_SHORT"),
	VALIDATOR_LEN(-1, 50, "ERROR_NICKNAME_TOO_LONG"),
	VALIDATOR_NREGEX("[%^/\\()<>=#\"+]", REG_EXTENDED, "ERROR_NICKNAME_INVALID"),
	VALIDATOR_REGEX("[[:alpha:]]+", REG_EXTENDED, "ERROR_NICKNAME_INVALID"),
	{0}
};
ADD_VALIDATOR(v_nickname);


/*
 * Name
 */
struct validator v_name[] = {
	VALIDATOR_LEN(2, -1, "ERROR_NAME_TOO_FEW_CHARS"),
	VALIDATOR_LEN(-1, 50, "ERROR_NAME_TOO_LONG"),
	VALIDATOR_NREGEX("[%^/\\()<>=#\"+]", REG_EXTENDED, "ERROR_NAME_INVALID_CHARS"),
	VALIDATOR_REGEX("[[:alpha:]]+", REG_EXTENDED, "ERROR_NAME_WITHOUT_LETTERS"),
	VALIDATOR_REGEX("(*UCP)^[[:print:][:alpha:]]+$", REG_EXTENDED, "ERROR_NAME_INVALID"),
	{0}
};
ADD_VALIDATOR(v_name);


/*
 * Business name
 */
struct validator v_business_name[] = {
	VALIDATOR_LEN(2, -1, "ERROR_BUSINESS_NAME_TOO_FEW_CHARS"),
	VALIDATOR_LEN(-1, 50, "ERROR_BUSINESS_NAME_TOO_LONG"),
	VALIDATOR_NREGEX("[%^/\\()<>=#\"+]", REG_EXTENDED, "ERROR_BUSINESS_NAME_INVALID"),
	VALIDATOR_REGEX("[[:alpha:]]+", REG_EXTENDED, "ERROR_BUSINESS_NAME_INVALID"),
	VALIDATOR_REGEX("(*UCP)^[[:print:][:alpha:]]+$", REG_EXTENDED, "ERROR_BUSINESS_NAME_INVALID"),
	{0}
};
ADD_VALIDATOR(v_business_name);


static int
vf_store_bad_words_db(struct cmd_param *param, const char *arg) {
	struct bpapi ba;
	struct sql_worker *worker = NULL;
	const char *errstr = NULL;
	const char *store_block_list = bconf_value(bconf_vget(trans_bconf, "stores", "block_list", "bad_words", "name", NULL));
	int has_error = 0;
	char *first_error = NULL;

	BPAPI_INIT(&ba, NULL, pgsql);
	bpapi_insert(&ba, "list_name",store_block_list);
	bpapi_insert(&ba, "value", param->value);
	worker = sql_blocked_template_query("pgsql.slave", 0, "sql/check_store_bad_words.sql", &ba, param->cs->ts->log_string, &errstr);
	bpapi_free(&ba);

	if (worker) {
		while (sql_blocked_next_result(worker, &errstr) == 1) {
			if (worker->next_row(worker)){
				first_error = (char *) worker->get_value(worker,0);
				has_error = 1;
				break;
			}
		}

		if (has_error == 1) {
			char *upper_column = xstrdup(param->key);
			transaction_printf(param->cs->ts, "%s:ERROR_STORE_%s_BLOCKED:%s\n",   param->key, strmodify(upper_column, toupper), (const char*)first_error);
			param->cs->status = "TRANS_ERROR";
			free(upper_column);
		}
	}

	if (errstr) {
		param->cs->error = "TRANS_DATABASE_ERROR";
		param->cs->message = xstrdup(errstr);
	}

	if (worker)
		sql_worker_put(worker);

	return has_error;
}

/*
 * Store Name
 */
struct validator v_store_name[] = {
	VALIDATOR_LEN(2, -1, "ERROR_STORE_NAME_TOO_SHORT"),
	VALIDATOR_LEN(-1, 100, "ERROR_NAME_TOO_LONG"),
	VALIDATOR_NREGEX("[%^/\\()<>=#\"+]", REG_EXTENDED, "ERROR_STORE_NAME_INVALID"),
	VALIDATOR_REGEX("[[:alpha:]]+", REG_EXTENDED, "ERROR_STORE_NAME_INVALID"),
	VALIDATOR_REGEX("(*UCP)^[[:print:][:alpha:]]+$", REG_EXTENDED, "ERROR_STORE_NAME_INVALID"),
	VALIDATOR_FUNC(vf_store_bad_words_db),
	{0}
};
ADD_VALIDATOR(v_store_name);

/*
* CompanyName
*/
struct validator v_companyname[] = {
	VALIDATOR_LEN(2, -1, "ERROR_COMPANY_TOO_SHORT"),
	VALIDATOR_LEN(-1, 50, "ERROR_COMPANY_TOO_LONG"),
	VALIDATOR_NREGEX("[%^/\\()<>=#\"+]", REG_EXTENDED, "ERROR_COMPANY_INVALID"),
	VALIDATOR_REGEX("[[:alpha:]]+", REG_EXTENDED, "ERROR_COMPANY_INVALID"),
	{0}
};
ADD_VALIDATOR(v_companyname);

/*
 * Import id
 */
struct validator v_import_id[] = {
	VALIDATOR_LEN(-1, 50, "ERROR_IMPORT_ID_TOO_LONG"),
        VALIDATOR_REGEX("^[[:alnum:]_-]+$", REG_EXTENDED, "ERROR_IMPORT_ID_INVALID"),
	{0}
};
ADD_VALIDATOR(v_import_id);

struct validator v_address[] = {
	VALIDATOR_LEN(2, -1, "ERROR_ADDRESS_TOO_SHORT"),
	VALIDATOR_LEN(-1, 50, "ERROR_ADDRESS_LONG"),
	VALIDATOR_NREGEX("[%^\\()<>=#\"+]", REG_EXTENDED, "ERROR_ADDRESS_INVALID"),
	{0}
};
ADD_VALIDATOR(v_address);

struct validator v_address_optional[] = {
	VALIDATOR_LEN(-1, 60, "ERROR_ADDRESS_LONG"),
	VALIDATOR_NREGEX("[%^\\()<>=#\"+]", REG_EXTENDED, "ERROR_ADDRESS_INVALID"),
	{0}
};
ADD_VALIDATOR(v_address_optional);

struct validator v_email_required_checkblocked[] = {
	VALIDATOR_LEN(5, -1, "ERROR_EMAIL_TOO_SHORT"),
	VALIDATOR_LEN(-1, 60, "ERROR_EMAIL_TOO_LONG"),
	VALIDATOR_NREGEX("[" ASCALPHA "]{5,}$", REG_EXTENDED|REG_NOSUB|REG_ICASE, "ERROR_EMAIL_NO_DOMAIN"),
	VALIDATOR_REGEX("^[[:alnum:]_+-]+(\\.[[:alnum:]_+-]*)*@([[:alnum:]_+-]+\\.)+[[:alpha:]]{2,4}$",
			REG_EXTENDED|REG_NOSUB|REG_ICASE, "ERROR_EMAIL_INVALID"),
	VALIDATOR_REGEX("^[" ASCALNUM "_+-]+(\\.[" ASCALNUM "_+-]*)*@([" ASCALNUM "_+-]+\\.)+[" ASCALPHA "]{2,4}$",
			REG_EXTENDED|REG_NOSUB|REG_ICASE, "ERROR_EMAIL_INVALID_CHARS"),
        VALIDATOR_NREGEX("^www", REG_EXTENDED|REG_NOSUB|REG_ICASE, "WARN_EMAIL_WWW"),
	{0}
};
ADD_VALIDATOR(v_email_required_checkblocked);

struct validator v_email[] = {
	VALIDATOR_FUNC(vf_check_email),
	{0}
};
ADD_VALIDATOR(v_email);

/*
  FT 405.
  we (Yapo team) need a diferent validator for sendtip.
 */
struct validator v_emailsendtip[] = {
	VALIDATOR_REQUIRED("ERROR_EMAIL_MISSING_SENDTIP"),
	VALIDATOR_FUNC(vf_check_email),
	{0}
};
ADD_VALIDATOR(v_emailsendtip);

struct validator v_email_not_required[] = {
	VALIDATOR_LEN(5, -1, "ERROR_EMAIL_TOO_SHORT"),
	VALIDATOR_LEN(-1, 60, "ERROR_EMAIL_TOO_LONG"),
	VALIDATOR_NREGEX("[" ASCALPHA "]{5,}$", REG_EXTENDED|REG_NOSUB|REG_ICASE, "ERROR_EMAIL_NO_DOMAIN"),
	VALIDATOR_REGEX("^[[:alnum:]_+-]+(\\.[[:alnum:]_+-]*)*@([[:alnum:]_+-]+\\.)+[[:alpha:]]{2,4}$",
			REG_EXTENDED|REG_NOSUB|REG_ICASE, "ERROR_EMAIL_INVALID"),
	VALIDATOR_REGEX("^[" ASCALNUM "_+-]+(\\.[" ASCALNUM "_+-]*)*@([" ASCALNUM "_+-]+\\.)+[" ASCALPHA "]{2,4}$",
			REG_EXTENDED|REG_NOSUB|REG_ICASE, "ERROR_EMAIL_INVALID_CHARS"),
        VALIDATOR_NREGEX("^www", REG_EXTENDED|REG_NOSUB|REG_ICASE, "WARN_EMAIL_WWW"),
	{0}
};
ADD_VALIDATOR(v_email_not_required);

struct validator v_email_basic[] = {
	VALIDATOR_LEN(5, -1, "ERROR_EMAIL_TOO_SHORT"),
	VALIDATOR_LEN(-1, 60, "ERROR_EMAIL_TOO_LONG"),
	VALIDATOR_REGEX("^[[:alnum:]_+-]+(\\.[[:alnum:]_+-]*)*@([[:alnum:]_+-]+\\.)+[[:alpha:]]{2,4}$",
			REG_EXTENDED|REG_NOSUB|REG_ICASE, "ERROR_INVALID_EMAIL"),
	VALIDATOR_REGEX("^[" ASCALNUM "_+-]+(\\.[" ASCALNUM "_+-]*)*@([" ASCALNUM "_+-]+\\.)+[" ASCALPHA "]{2,4}$",
			REG_EXTENDED|REG_NOSUB|REG_ICASE, "ERROR_EMAIL_INVALID_CHARS"),
	VALIDATOR_NREGEX("[" ASCALPHA "]{5,}$", REG_EXTENDED|REG_NOSUB|REG_ICASE, "ERROR_EMAIL_NO_DOMAIN"),
	{0}
};
ADD_VALIDATOR(v_email_basic);

struct validator v_phone_basic[] = {
	VALIDATOR_LEN(-1, 11, "ERROR_PHONE_TOO_LONG"),
	VALIDATOR_REGEX("^[-+[:blank:][:digit:]()/]*$", REG_EXTENDED, "ERROR_PHONE_INVALID"),
	VALIDATOR_LEN(6, -1, "ERROR_PHONE_TOO_SHORT"),
	{0}
};
ADD_VALIDATOR(v_phone_basic);

struct validator v_phone_strict[] = {
	VALIDATOR_LEN(-1, 9, "ERROR_PHONE_TOO_LONG"),
	VALIDATOR_REGEX("^[[:digit:]]+$", REG_EXTENDED, "ERROR_PHONE_INVALID"),
	VALIDATOR_LEN(9, -1, "ERROR_STRICT_PHONE_TOO_SHORT"),
	{0}
};
ADD_VALIDATOR(v_phone_strict);

struct validator v_phone_hidden[] = {
	{0}
};
ADD_VALIDATOR(v_phone_hidden);

struct validator v_region[] = {
	VALIDATOR_NREGEX("^0$", REG_EXTENDED|REG_NOSUB, "ERROR_REGION_MISSING"),
	VALIDATOR_FUNC(vf_region_bconf),
/*	VALIDATOR_FUNC(vf_region_cities),	*/
	{0}
};
ADD_VALIDATOR(v_region);

struct validator v_commune[] = {
	VALIDATOR_NREGEX("^(0|)$", REG_EXTENDED|REG_NOSUB, "ERROR_COMMUNES_MISSING"),
	VALIDATOR_FUNC(vf_commune_bconf),
	{0}
};
ADD_VALIDATOR(v_commune);

struct validator v_commune_optional[] = {
	VALIDATOR_FUNC(vf_commune_bconf),
	{0}
};
ADD_VALIDATOR(v_commune_optional);


struct validator v_company_ad[] = {
	VALIDATOR_FUNC(vf_company_ad_subscription),
	{0}
};
ADD_VALIDATOR(v_company_ad);

struct validator v_subject[] = {
	VALIDATOR_LEN(2, -1, "ERROR_SUBJECT_TOO_SHORT"),
	VALIDATOR_LEN(-1, 50, "ERROR_SUBJECT_TOO_LONG"),
	VALIDATOR_FUNC(vf_subject_regdate),
	{0}
};
ADD_VALIDATOR(v_subject);

struct validator v_body[] = {
	VALIDATOR_FUNC_ALWAYS(vf_body_or_item_required),
	VALIDATOR_LEN(2, -1, "ERROR_BODY_TOO_SHORT"),
	VALIDATOR_LEN(-1, 2000, "ERROR_BODY_TOO_LONG"),
	{0}
};
ADD_VALIDATOR(v_body);

struct validator v_body_optional[] = {
	VALIDATOR_LEN(2, -1, "ERROR_BODY_TOO_SHORT"),
	VALIDATOR_LEN(-1, 2000, "ERROR_BODY_TOO_LONG"),
	{0}
};
ADD_VALIDATOR(v_body_optional);

struct validator v_adreply_body[] = {
	VALIDATOR_LEN(3, -1, "ERROR_ADREPLY_BODY_TOO_SHORT"),
	VALIDATOR_LEN(-1, 3000, "ERROR_ADREPLY_BODY_TOO_LONG"),
	{0}
};
ADD_VALIDATOR(v_adreply_body);

struct validator v_rabbitmq_body[] = {
	VALIDATOR_FUNC_ALWAYS(vf_body_or_item_required),
	VALIDATOR_LEN(2, -1, "ERROR_BODY_TOO_SHORT"),
	VALIDATOR_LEN(-1, 4000, "ERROR_BODY_TOO_LONG"),
};

ADD_VALIDATOR(v_rabbitmq_body);

struct validator v_infopage[] = {
	VALIDATOR_SUB("v_url"),
	VALIDATOR_FUNC_ALWAYS(vf_check_infopage),
	{0}
};
ADD_VALIDATOR(v_infopage);

struct validator v_url[] = {
	VALIDATOR_LEN(-1, 255, "ERROR_URL_TOO_LONG"),
	VALIDATOR_REGEX("^(https?://)?((([_" ASCALNUM "-]+\\.)+([" ASCALPHA "]{2,4}))|([0-9]{1,3}(\\.[0-9]{1,3}){3}))(/[^<>]*)*$",
			REG_EXTENDED|REG_NOSUB|REG_ICASE,
			"ERROR_URL_INVALID"),
	{0}
};
ADD_VALIDATOR(v_url);

struct validator v_store_external_url[] = {
	VALIDATOR_LEN(-1, 200, "ERROR_URL_TOO_LONG"),
	VALIDATOR_NREGEX("^(-|.*\\.-|.*-\\.)", REG_EXTENDED, "ERROR_URL_INVALID"), /* Detect www.-aaaa.cl or www.aaa-.cl that is forbidden */
	VALIDATOR_NREGEX("^(..--|.*\\...--)", REG_EXTENDED, "ERROR_URL_INVALID"), /* Detect www.aa--aaaa.cl that is forbidden */
	VALIDATOR_REGEX("^(https?://)?([a-z0-9\\-_]+\\.)?"		/* Represents the hostname like www */
			"[a-z0-9\\-�������]{1,63}"	/* Represents domain name like �and� */
			"\\.[a-zA-Z\\.]{2,7}"		/* Represents top level domain like .com, .net, .travel */
			"(/[a-zA-Z0-9\\-�������_/%&=\\?:;\\.~]*)?$", /* Represents the dir name like /foo/var */
			REG_EXTENDED|REG_NOSUB|REG_ICASE, "ERROR_URL_INVALID"),
	{0}
};
ADD_VALIDATOR(v_store_external_url);


struct validator v_store_url[] = {
	VALIDATOR_LEN(-1, 200, "ERROR_URL_TOO_LONG"),
	VALIDATOR_REGEX("^\\w+(-\\w+)*$",
			REG_EXTENDED|REG_NOSUB|REG_ICASE,
			"ERROR_URL_INVALID"),
	{0}
};
ADD_VALIDATOR(v_store_url);

struct validator v_infopage_title[] = {
	VALIDATOR_LEN(-1, 50, "ERROR_INFOPAGE_TITLE_TOO_LONG"),
        VALIDATOR_FUNC(vf_check_infopage_title),
	{0}
};
ADD_VALIDATOR(v_infopage_title);

static int
vf_price_uf(struct cmd_param *param, const char *arg) {
	char* ptr;
	const char* bconf_str = bconf_vget_string(trans_bconf, "max_price", "uf", NULL);
	if (!bconf_str || *bconf_str == '\0'){
		param->error = "ERROR_BCONF_NOT_SET";
		param->cs->status = "TRANS_ERROR";
		return 0;
	}
	
	// strtod uses LC_NUMERIC, so we need to change the param value to
	// "en_US.ISO-8859-1" compliant. In simpler terms, change the comma for dot
	char *value_with_point = xstrdup(param->value);
	char *commapos = strchr(value_with_point, ',');
	if (commapos)
		*commapos = '.';

	double max_uf = strtod(bconf_str, NULL);
	double price = strtod(value_with_point, &ptr);
	if (*ptr != '\0') {
		param->error = "ERROR_PRICE_INVALID";
		param->cs->status = "TRANS_ERROR";
	} else if (price > max_uf) {
		param->error = "ERROR_UF_PRICE_TOO_HIGH";
		param->cs->status = "TRANS_ERROR";
	}
	free(value_with_point);
	return 0;
}

/*
 * Check format of a price in UFs
 * We are limiting the price field's size to 10 digits max.
*/
struct validator v_price_uf[] = {
	VALIDATOR_FUNC(vf_price_uf),
	VALIDATOR_REGEX("^([[:digit:]]+([.,]([[:digit:]]{1,2}))?)$", REG_EXTENDED|REG_NOSUB, "ERROR_PRICE_UF_TOO_MANY_DECIMALS"),
	{0}
};
ADD_VALIDATOR(v_price_uf);

static void
vf_minimum_price_cb(const char *setting, const char *key, const char *value, void *cb) {
	struct trans_key_lookup_data *data = cb;

	if (!strcmp(key, "price"))
		data->result = atol(value);
	else if (!strcmp(key, "error"))
		data->has_param = bconf_key(bconf_vget(host_bconf, "*.language", value, NULL)); /* Avoid memory allocation */
}

static int
vf_minimum_price(struct cmd_param *param, const char *arg) {
	struct trans_key_lookup_data data;
	struct bpapi_vtree_chain vtree;

	data.param = param;
	data.result = 0;
	data.has_param = NULL;

	get_settings(bconf_vtree(&vtree, bconf_get(trans_bconf, "category_settings")), "min_price", trans_key_lookup, vf_minimum_price_cb, &data);
	vtree_free(&vtree);

	if (data.result && data.has_param && atol(param->value) < data.result) {
		param->error = data.has_param;
		param->cs->status = "TRANS_ERROR";
	} else if (atol(param->value) < 0){
		param->error = "ERROR_PRICE_NEGATIVE";
		param->cs->status = "TRANS_ERROR";
	}

	return 0;
}

static int
vf_price(struct cmd_param *param, const char *arg) {
	char *ptr;
	const char* bconf_str = bconf_vget_string(trans_bconf, "max_price", "peso", NULL);
	if (!bconf_str || *bconf_str == '\0'){
		param->error = "ERROR_BCONF_NOT_SET";
		param->cs->status = "TRANS_ERROR";
		return 0;
	}
	long long max_peso = strtoll(bconf_str, NULL, 10);
	long long price = strtoll(param->value, &ptr, 10);
	if (*ptr != '\0') {
		param->error = "ERROR_PRICE_INVALID";
		param->cs->status = "TRANS_ERROR";
	} else if (price > max_peso) {
		param->error = "ERROR_PRICE_TOO_HIGH";
		param->cs->status = "TRANS_ERROR";
	}
	return 0;
}

struct validator v_price[] = {
	VALIDATOR_FUNC(vf_price),
	VALIDATOR_FUNC(vf_minimum_price),
	{0}
};
ADD_VALIDATOR(v_price);

static int
vf_validate_product_id(struct cmd_param *param, const char *arg) {
	char *value_cpy = xstrdup(param->value);
	char *elem_name;
	char *delim = NULL;
	char *tmp = value_cpy;
	int flag_credits = 0;

	if (strlen(param->value)==0){
		param->error = "ERROR_PRODUCT_ID_MISSING";
		param->cs->status = "TRANS_ERROR";
		free(tmp);
		return 1;
	}

	while ((elem_name = strtok_r(value_cpy, "|", &delim))) {
		struct bconf_node *payment_node = bconf_vget(trans_bconf, "payment", "product", elem_name, NULL);
		const char *group = bconf_value(bconf_get(payment_node, "group"));
		const char *prod = bconf_value(bconf_get(payment_node, "codename"));
		value_cpy = NULL;
		if (group && strcmp(group, "CREDITS") == 0) {
			if (flag_credits) {
				param->error = "ERROR_DUPLICATED_PRODUCT_CREDITS";
				param->cs->status = "TRANS_ERROR";
				free(tmp);
				return 1;
			}
			flag_credits = 1;
		}
		if(prod){
			continue;
		}
		param->error = "ERROR_INVALID_PRODUCT_IDS";
		param->cs->status = "TRANS_ERROR";
		free(tmp);
		return 1;
	}
	free(tmp);
	return 0;
}

struct validator v_multiple_product_ids[] = {
	VALIDATOR_FUNC(vf_validate_product_id),
	{0}
};
ADD_VALIDATOR(v_multiple_product_ids);

struct validator v_product_id[] = {
	VALIDATOR_FUNC(vf_validate_product_id),
	{0}
};
ADD_VALIDATOR(v_product_id);

static int
vf_validate_product_price(struct cmd_param *param, const char *arg) {

	if (!cmd_haskey(param->cs, "product_id")) {
		transaction_logprintf(param->cs->ts, D_WARNING, "Missing product_id.");
		return 0;
	}
	char *product_ids = xstrdup(cmd_getval(param->cs, "product_id"));
	char *tmp_ids = product_ids;
	char *product_prices = xstrdup(param->value);
	char *tmp_prices = product_prices;
	char *elem_id;
	char *elem_price;
	char *id_ptr = NULL;
	char *price_ptr = NULL;
	int has_error = 0;

	while ((elem_id = strtok_r(product_ids, "|", &id_ptr))) {
		transaction_logprintf(param->cs->ts, D_DEBUG, "elem_id = %s", elem_id);
		product_ids = NULL;

		struct bconf_node *payment_node = bconf_vget(trans_bconf, "payment", "product", elem_id, NULL);
		const char *group = bconf_value(bconf_get(payment_node, "group"));

		elem_price = strtok_r(product_prices, "|", &price_ptr);
		product_prices = NULL;
		if (!elem_price) {
			param->error = "ERROR_PRICE_MISSMATCH";
			has_error = 1;
			break;
		}
		if (strcmp(group, "CREDITS") != 0) {
			continue;
		}

		int min = bconf_get_int(payment_node, "min_quantity");
		int max = bconf_get_int(payment_node, "max_quantity");
		int value = atoi(elem_price);
		transaction_logprintf(param->cs->ts, D_DEBUG, "elem_id = %s, min = %d, max = %d, price = %d", elem_id, min, max, value);

		if (min && value < min) {
			param->error = "ERROR_PRICE_TOO_LOW";
			has_error = 1;
			break;
		}
		if (max && max < value) {
			param->error = "ERROR_PRICE_TOO_HIGH";
			has_error = 1;
			break;
		}
	}

	if(has_error) {
		param->cs->status = "TRANS_ERROR";
	}
	free(tmp_prices);
	free(tmp_ids);
	return has_error;
}

struct validator v_multiple_product_prices[] = {
	VALIDATOR_REGEX("^[[:digit:]]+([|][[:digit:]]+)*$", REG_EXTENDED|REG_NOSUB, "ERROR_INVALID_PRODUCT_PRICES"),
	VALIDATOR_FUNC(vf_validate_product_price),
	{0}
};
ADD_VALIDATOR(v_multiple_product_prices);

struct validator v_product_price[] = {
	VALIDATOR_REGEX("^[[:digit:]]+([.][[:digit:]]+)*$", REG_EXTENDED|REG_NOSUB, "ERROR_INVALID_PRODUCT_PRICES"),
	{0}
};

ADD_VALIDATOR(v_product_price);

static struct c_param required_account_id = {"account_id", P_REQUIRED, "v_account_id"};

static int
vf_validate_product_names(struct cmd_param *param, const char *arg) {
	struct cmd *cs = param->cs;
	char *value_cpy = xstrdup(param->value);
	char *elem_name;
	char *delim = NULL;
	char *tmp = value_cpy;
	int i;
	struct bconf_node *prod_root = bconf_get(trans_bconf, "payment.product");

	if (strlen(param->value)==0){
		param->error = "ERROR_PRODUCT_NAME_MISSING";
		cs->status = "TRANS_ERROR";
		free(tmp);
		return 1;
	}

	while ((elem_name = strtok_r(value_cpy, "|", &delim))) {
		if(elem_name){	
			int found = 0;
			for (i = 0; i < bconf_count(prod_root); i++) {
				const char *value = bconf_get_string(bconf_byindex(prod_root, i), "codename");
				const char *group = bconf_get_string(bconf_byindex(prod_root, i), "group");
				if(value && strcmp(value,elem_name) == 0){
					found = 1;
					if(group){
						if (bconf_get_int(bconf_vget(trans_bconf, "payment.product_group", group ,NULL), "require_account_id") == 1){
							cmd_extra_param(param->cs, &required_account_id);
						}
					}
					break;
				}
			}
		
			value_cpy = NULL;
			if(found == 1){
				continue;
			}
			param->error = "ERROR_INVALID_PRODUCT_NAMES";
			cs->status = "TRANS_ERROR";
			free(tmp);
			return 1;
		}
	}
	free(tmp);
	return 0;
}


struct validator v_multiple_product_names[] = {
	VALIDATOR_FUNC(vf_validate_product_names),
	{0}
};
ADD_VALIDATOR(v_multiple_product_names);

static int
vf_validate_credits_w_credits(struct cmd_param *param, const char *arg) {
	// If the payment method is not credits is OK
	if (strcmp("credits", param->value) != 0) {
		return 0;
	}

	if (!cmd_haskey(param->cs, "product_id") || strlen(cmd_getval(param->cs, "product_id")) == 0) {
		return 0;
	}
	// If the payment method is credits we must validate
	char *elem_name;
	char *value_cpy = xstrdup(cmd_getval(param->cs, "product_id"));
	char *tmp = value_cpy;
	char *delim = NULL;
	while ((elem_name = strtok_r(value_cpy, "|", &delim))) {
		if (elem_name) {
			const char *group = bconf_get_string(bconf_vget(trans_bconf, "payment.product", elem_name, NULL), "group");
			if (group && strcmp("CREDITS", group) == 0) {
				param->error = "ERROR_PAYING_CREDITS_WITH_CREDITS";
				param->cs->status = "TRANS_ERROR";
				free(tmp);
				return 1;
			}

			value_cpy = NULL;
		}
	}
	free(tmp);
	return 0;
}

static int
vf_validate_product_params(struct cmd_param *param, const char *arg) {

	if (!cmd_haskey(param->cs, "product_id")
			|| !cmd_haskey(param->cs, "product_price")
			|| !cmd_haskey(param->cs, "product_name")
		) {
		transaction_logprintf(param->cs->ts, D_WARNING, "Missing params.");
		return 0;
	}

	const char *prod_id = cmd_getval(param->cs, "product_id");
	const char *prod_price = cmd_getval(param->cs, "product_price");
	const char *prod_name = cmd_getval(param->cs, "product_name");

	if (*prod_id && *prod_price && *prod_name && *param->value) {
		int count_params = 1, count_id = 1, count_price = 1, count_name = 1, i = 0;

		for (i = 0; param->value[i]; i++) {
			count_params += (param->value[i] == '|');
		}
		for (i = 0; prod_id[i]; i++) {
			count_id += (prod_id[i] == '|');
		}
		for (i = 0; prod_price[i]; i++) {
			count_price += (prod_price[i] == '|');
		}
		for (i = 0; prod_name[i]; i++) {
			count_name += (prod_name[i] == '|');
		}

		if (count_params == count_id && count_params == count_price && count_params == count_name) {
			return 0;
		}
	}
	transaction_printf(param->cs->ts, "error:ERROR_PRODUCT_LENGTHS_INVALID\n");
	param->cs->status = "TRANS_ERROR";
	return 1;
}

struct validator v_multiple_product_params[] = {
	VALIDATOR_REGEX("^[[:print:][:alpha:]]+([|][[:print:][:alpha:]]+)*$", REG_EXTENDED|REG_NOSUB, "ERROR_INVALID_PRODUCT_PARAMS"),
	VALIDATOR_FUNC(vf_validate_product_params),
	{0}
};
ADD_VALIDATOR(v_multiple_product_params);
/*
 * telesales seller_id
 */
struct validator v_seller_id[] = {
	VALIDATOR_INT(1, -1, "ERROR_SELLER_ID_INVALID"),
	{0}
};
ADD_VALIDATOR(v_seller_id);

struct validator v_payment_methods[] = {
	VALIDATOR_REGEX("^(webpay_plus|webpay|servipag|appstore|oneclick|khipu|credits)$", REG_EXTENDED, "ERROR_INVALID_PAYMENT_METHOD"),
	VALIDATOR_FUNC(vf_validate_credits_w_credits),
	{0}
};
ADD_VALIDATOR(v_payment_methods);

struct validator v_payment_platform[] = {
	VALIDATOR_REGEX("^(desktop|msite|android|ios|unknown)$", REG_EXTENDED, "ERROR_INVALID_PAYMENT_PLATFORM"),
	{0}
};
ADD_VALIDATOR(v_payment_platform);

struct validator v_multiple_list_ids[] = {
	VALIDATOR_REGEX("^[[:digit:]]+([|][[:digit:]]+)*$", REG_EXTENDED|REG_NOSUB, "ERROR_INVALID_LIST_IDS"),
	{0}
};
ADD_VALIDATOR(v_multiple_list_ids);

struct validator v_id_list[] = {
	VALIDATOR_REGEX("^[[:digit:]]+(,[[:digit:]]+)*$", REG_EXTENDED|REG_NOSUB, "ERROR_INVALID_ID_LIST"),
	{0}
};
ADD_VALIDATOR(v_id_list);

struct validator v_multiple_ad_names[] = {
	VALIDATOR_REGEX("(*UCP)^[[:print:][:alpha:]]+$", REG_EXTENDED, "ERROR_MULTIPLE_NAMES_INVALID"),
};
ADD_VALIDATOR(v_multiple_ad_names);

static void
check_file_on_dav_cb(struct davdb *cs) {
	struct cmd_param *param = cs->data;

	if (cs->result != 200) {
		param->error = "ERROR_FILE_DOES_NOT_EXIST";
		param->message = xstrdup(param->value);
		if (!strcmp(param->cs->status, "TRANS_OK"))
			param->cs->status = "TRANS_ERROR";
	}
	param->cs->pending_validators--;
	param->cs->validator_cb(param->cs);

	free(cs);
}

static void
check_file_on_dav(struct cmd_param *param) {
	struct davdb *dav = zmalloc(sizeof (*dav));

	dav->cs = param->cs;
	dav->command = "GET";
	dav->filename = param->value;
	dav->data = param;
	dav->cb = check_file_on_dav_cb;

	if (strstr (param->cs->command->name, "bid"))
		dav->application = "bid";

	davdb_execute(dav);
	param->cs->pending_validators++;
}

static int
vf_imagefile(struct cmd_param *param, const char *arg) {
	const char *dav_check = bconf_value(bconf_vget(trans_bconf, "common", "media", "dav_check", "enabled", NULL));

	if (dav_check && atoi(dav_check) == 1)
		check_file_on_dav(param);

	return 0;
}

struct validator v_imagefile[] = {
	VALIDATOR_FUNC(vf_imagefile),
	{0}
};
ADD_VALIDATOR(v_imagefile);


struct validator v_store_image_type[] = {
	VALIDATOR_REGEX("^(main_image|logo_image)$", REG_EXTENDED|REG_NOSUB, "ERROR_IMAGE_TYPE_INVALID"),
	{0}
};
ADD_VALIDATOR(v_store_image_type);

struct validator v_store[] = {
	VALIDATOR_FUNC(vf_check_store),
	{0}
};
ADD_VALIDATOR(v_store);

struct validator v_car[] = {
        VALIDATOR_FUNC(vf_cat_car),
        {0}
};
ADD_VALIDATOR(v_car);

/*
 * Ad id
 */
static struct validator v_id[] = {
	VALIDATOR_LEN(1, 15, "ERROR_AD_ID_INVALID"),
	VALIDATOR_REGEX("^[[:digit:]]+$", REG_EXTENDED, "ERROR_AD_ID_INVALID"),
	{0}
};
ADD_VALIDATOR(v_id);

/*
 * Ad id
 */
static struct validator v_tbk_commerce[] = {
	VALIDATOR_LEN(0, 15, "ERROR_TBK_LEN_ID_COMMERCE_INVALID"),
	VALIDATOR_REGEX("^[[:digit:]]*$", REG_EXTENDED, "ERROR_TBK_ID_COMMERCE_INVALID"),
	{0}
};
ADD_VALIDATOR(v_tbk_commerce);

 /*
 * Account id
 */
static struct validator v_account_id[] = {
	VALIDATOR_LEN(1, 15, "ERROR_ACCOUNT_ID_INVALID"),
	VALIDATOR_REGEX("^[[:digit:]]+$", REG_EXTENDED, "ERROR_ACCOUNT_ID_INVALID"),
	{0}
};
ADD_VALIDATOR(v_account_id);


/*
 * Store
 */
struct validator v_store_states[] = {
	VALIDATOR_REGEX("^(pending|active|admin_deactivated|user_deactivated|expired)$", REG_EXTENDED, "ERROR_INVALID_STORE_STATUS"),
	{0}
};
ADD_VALIDATOR(v_store_states);

static struct validator v_store_id[] = {
	VALIDATOR_LEN(1, 15, "ERROR_STORE_ID_INVALID"),
	VALIDATOR_REGEX("^[[:digit:]]+$", REG_EXTENDED, "ERROR_STORE_ID_INVALID"),
	{0}
};
ADD_VALIDATOR(v_store_id);

/*
 * Link type
 */
static void
vf_partner_ad_limit_cb(struct cmd_param *param, struct sql_worker *worker, const char *arg) {
	if (worker->next_row(worker)) {
		int partner_ads = worker->get_value(worker, 0) ? atoi(worker->get_value(worker, 0)) : 0;
		struct fd_pool_conn *conn = redis_sock_conn(redis_mobile_api_pool.pool, "master");
		if (!conn)  {
			param->error = "ERROR_PARTNER_REDIS_CONNECTION";
			param->cs->status = "TRANS_ERROR";
			return ;
		}

		char *key = NULL;
		xasprintf(&key, "mcp1xapp_id_%s", param->value);

		char *reply = redis_sock_hget(conn, key, "ads_limit");
		free(key);

		int ad_limit = reply ? atoi(reply) : -1;
		free(reply);

		transaction_logprintf(param->cs->ts, D_DEBUG, "Partner %s, inserted ads: %d, ad_limits: %d", param->value, partner_ads, ad_limit);

		if ((partner_ads >= ad_limit) && (ad_limit > -1)) {
			transaction_logprintf(param->cs->ts, D_INFO, "Partner %s reached inserted ads limit", param->value);
			param->error = "ERROR_PARTNER_ADS_LIMIT_REACHED";
			param->cs->status = "TRANS_ERROR";
		}

		fd_pool_free_conn(conn);
	}
}

static int
vf_partner_ad_limit(struct cmd_param *param, const char *arg) {
	if (!cmd_haskey(param->cs, "email")) {
		param->error = "ERROR_EMAIL_MISSING";
		param->cs->status = "TRANS_ERROR";
		return 1;
	}
	if (cmd_haskey(param->cs, "ad_type") && strcmp(cmd_getval(param->cs, "ad_type"), "new"))
		return 0;

	validate_on_db(param, arg,
		"email", cmd_getval(param->cs, "email"),
		"pgsql.master", "sql/get_ads_number_by_partner_email.sql",
		NULL, NULL, vf_partner_ad_limit_cb
	);
	return 0;
}

static struct validator v_link_type_limited[] = {
	VALIDATOR_REGEX("^[[:alnum:]_]+$", REG_EXTENDED, "ERROR_LINK_TYPE_NON_ALNUM"),
	VALIDATOR_OTHER("email", P_REQUIRED, "v_email"),
	VALIDATOR_FUNC(vf_partner_ad_limit),
	{0}
};
ADD_VALIDATOR(v_link_type_limited);

static struct validator v_link_type[] = {
	VALIDATOR_REGEX("^[[:alnum:]_]+$", REG_EXTENDED, "ERROR_LINK_TYPE_NON_ALNUM"),
};
ADD_VALIDATOR(v_link_type);

/*
 * Purchase params
 */

static struct validator v_one_click_id[] = {
	VALIDATOR_REGEX("^[0-9]{17}$", REG_EXTENDED, "ERROR_ONECLICK_ID_INVALID"),
	{0}
};
ADD_VALIDATOR(v_one_click_id);

static struct c_param fail_on_yapo_type = {"value", P_REQUIRED, "v_array:1"};
static struct c_param one_click_type = {"value", P_REQUIRED, "v_one_click_id"};

static int
vf_purchase_param_values(struct cmd_param *param, const char *arg) {
	if (strcmp(param->value, "fail_on_yapo") == 0)
		cmd_extra_param(param->cs, &fail_on_yapo_type);
	else if (strcmp(param->value, "oneclick_id") == 0)
		cmd_extra_param(param->cs, &one_click_type);
	else {
 		transaction_logprintf(param->cs->ts, D_ERROR, "ERROR_PURCHASE_PARAM_NAME_INVALID %s\n", param->value);
		param->error = "ERROR_PURCHASE_PARAM_NAME_INVALID";
		param->cs->status = "TRANS_ERROR";
		return 1;
	}
	return 0;
}

static struct validator v_purchase_param_type[] = {
	VALIDATOR_FUNC(vf_purchase_param_values),
	{0}
};
ADD_VALIDATOR(v_purchase_param_type);

/*
 * External ad id
 */
static struct c_param link_type = {"link_type", P_REQUIRED, "v_link_type"};
static struct c_param link_type_limited = {"link_type", P_REQUIRED, "v_link_type_limited"};

static int
vf_external_ad_id(struct cmd_param *param, const char *arg) {
	if (arg && !strcmp(arg, "limited"))
		cmd_extra_param(param->cs, &link_type_limited);
	else
		cmd_extra_param(param->cs, &link_type);
	return 0;
}

static struct validator v_external_ad_id[] = {
	VALIDATOR_LEN(1, 50, "ERROR_EXTERNAL_ID_INVALID"),
	VALIDATOR_REGEX("^[A-Za-z0-9_{}-]+$", REG_EXTENDED, "ERROR_EXTERNAL_ID_INVALID"),
	VALIDATOR_FUNC(vf_external_ad_id),
	{0}
};
ADD_VALIDATOR(v_external_ad_id);

/* User id */
struct validator v_user_id[] = {
	VALIDATOR_INT(1, -1, "ERROR_USER_ID_INVALID"),
	{0}
};

ADD_VALIDATOR(v_user_id);

/*
 * Ad codes
 */
struct validator v_pay_code[] = {
	VALIDATOR_INT(1, -1, "ERROR_PAY_CODE_INVALID"),
	{0}
};
ADD_VALIDATOR(v_pay_code);

struct validator v_orderid[] = {
	VALIDATOR_REGEX("^[[:digit:]]*a[[:digit:]]*$", REG_EXTENDED, "ERROR_ORDERID_INVALID"),
	{0}
};
ADD_VALIDATOR(v_orderid);


struct validator v_prev_action_id[] = {
	VALIDATOR_INT(1, -1, "ERROR_INVALID_PREV_ACTION_ID"),
	{0}
};
ADD_VALIDATOR(v_prev_action_id);

static int
vf_valid_token(struct cmd_param *param, const char *arg) {

	struct command_bpapi cba;
	struct sql_worker *worker = NULL;
	const char *errstr = NULL;
	struct cmd_param *remote_addr;

	command_bpapi_init(&cba, param->cs, CMD_BPAPI_HOST, NULL);

	bpapi_insert(&cba.ba, "token", param->value);
	/* XXX Workaround! Kill it when VALIDATOR_OTHER works for boolean operators (P_AND) */
	if ((remote_addr = cmd_getparam(param->cs, "remote_addr")) != NULL)
		bpapi_insert(&cba.ba, "remote_addr", remote_addr->value);
	bpapi_insert(&cba.ba, "valid_minutes", bconf_get_string_default(trans_bconf, "auth.validtime", "60"));
	bpapi_insert(&cba.ba, "info", param->cs->command->name);

	if (arg)
		bpapi_insert(&cba.ba, "priv_required", arg);

	worker = sql_blocked_template_query("pgsql.master", 0, "sql/call_validate_token.sql", &cba.ba, param->cs->ts->log_string, &errstr);
	command_bpapi_free(&cba);

	if (worker && sql_blocked_next_result(worker, &errstr) == 1) {
		if (worker->next_row(worker)) {
			const char *errorstring = worker->get_value_byname(worker, "o_error");
			if (errorstring && errorstring[0]) {
				transaction_logprintf(param->cs->ts, D_WARNING, "Error from validate_token() - %s", errorstring);
				param->cs->status = "TRANS_ERROR";
				/* Return remote_addr of the new token */
				transaction_printf(param->cs->ts, "token:%s:%s\n", errorstring, worker->get_value_byname(worker, "o_remote_addr"));
				sql_worker_put(worker);
				return 1;
			}

			transaction_printf(param->cs->ts, "token:%s\n", worker->get_value_byname(worker, "o_new_token"));
			sql_worker_put(worker);
			return 0;
		}

		transaction_logprintf(param->cs->ts, D_ERROR, "No result from validate_token()");
		param->cs->status = "TRANS_DATABASE_ERROR";
	}
	
	if (errstr) {
		if (strstr(errstr, "ERROR_TOKEN_INVALID")) {
			param->error = "ERROR_TOKEN_INVALID";
			param->cs->status = "TRANS_ERROR";
		} else if (strstr(errstr, "ERROR_TOKEN_REPLACED")) {
			/* Token given is not valid, token owner has already a new token */
			transaction_logprintf(param->cs->ts, D_WARNING, "Token is old and already replaced");
			/* Return remote_addr of the new token */
			transaction_printf(param->cs->ts, "token:ERROR_TOKEN_REPLACED:%s\n",
                                           worker->get_value_byname(worker, "o_remote_addr"));
			param->cs->status = "TRANS_ERROR";
		} else if (strstr(errstr, "ERROR_TOKEN_OLD")) {
			/* Token given is old */
			transaction_logprintf(param->cs->ts, D_WARNING, "Token is old, admin needs to login again");
			param->error = "ERROR_TOKEN_OLD";
			param->cs->status = "TRANS_ERROR";
		} else if (strstr(errstr, "ERROR_INSUFFICIENT_PRIVS")) {
			param->error = "ERROR_INSUFFICIENT_PRIVS";
			param->cs->status = "TRANS_ERROR";
		} else {
			param->cs->status = "TRANS_DATABASE_ERROR";
			param->cs->message = xstrdup(errstr);
		}
	}
	
	if (worker)
		sql_worker_put(worker);

	return 1;
}

static int
vf_token_and_verify_code(struct cmd_param *param, const char *arg) {
	if (cmd_haskey(param->cs, "verify_token")) {
		transaction_logprintf(param->cs->ts, D_WARNING, "Using token and verify_code on the same transaction [%s]",
				param->cs->command->name);
	}
	return 0;
}

struct validator v_token[] = {
	VALIDATOR_REGEX("^X[0-9a-z]{30,}$", REG_EXTENDED, "ERROR_TOKEN_BAD_FORMAT"),
	VALIDATOR_OTHER("remote_addr", P_REQUIRED, "v_ip"),
	VALIDATOR_FUNC(vf_token_and_verify_code),
	VALIDATOR_FUNC(vf_valid_token),
	{0}
};
ADD_VALIDATOR(v_token);

struct validator v_ng_token[] = {
	VALIDATOR_REGEX("^[0-9a-f]{40}$", REG_EXTENDED, "ERROR_NG_TOKEN_BAD_FORMAT"),
	{0}
};
ADD_VALIDATOR(v_ng_token);

static void
vf_validate_store_token_cb(struct cmd_param *param, struct sql_worker *worker, const char *arg) {
	if (worker->next_row(worker)) {
		const char *error = worker->get_value_byname(worker, "o_error");
		if (error && *error) {
			transaction_logprintf(param->cs->ts, D_WARNING, "Error from validate_token() - %s", error);
			param->cs->status = "TRANS_ERROR";
			/* Return remote_addr of the new token */
			const char *remote_addr = worker->get_value_byname(worker, "o_remote_addr");
			transaction_printf(param->cs->ts, "token:%s:%s\n", error, remote_addr);
		} else {
			const char *new_token = worker->get_value_byname(worker, "o_new_token");
			transaction_printf(param->cs->ts, "token:%s\n", new_token);
		}
	} else {
		transaction_logprintf(param->cs->ts, D_ERROR, "No result from validate_token()");
		param->cs->status = "TRANS_DATABASE_ERROR";
	}
}

static void
vf_validate_store_token_error(struct cmd_param *param, struct sql_worker *worker, const char *errstr) {
	if (strstr(errstr, "ERROR_TOKEN_INVALID")) {
		param->error = "ERROR_TOKEN_INVALID";
		param->cs->status = "TRANS_ERROR";
	} else if (strstr(errstr, "ERROR_TOKEN_REPLACED")) {
		const char *remote_addr = worker->get_value_byname(worker, "o_remote_addr");
		/* Token given is not valid, token owner has already a new token */
		transaction_logprintf(param->cs->ts, D_WARNING, "Token is old and already replaced");
		/* Return remote_addr of the new token */
		transaction_printf(param->cs->ts, "token:ERROR_TOKEN_REPLACED:%s\n", remote_addr);
		param->cs->status = "TRANS_ERROR";
	} else if (strstr(errstr, "ERROR_TOKEN_OLD")) {
		/* Token given is old */
		transaction_logprintf(param->cs->ts, D_WARNING, "Token is old, admin needs to login again");
		param->error = "ERROR_TOKEN_OLD";
		param->cs->status = "TRANS_ERROR";
	} else if (strstr(errstr, "ERROR_INSUFFICIENT_PRIVS")) {
		param->error = "ERROR_INSUFFICIENT_PRIVS";
		param->cs->status = "TRANS_ERROR";
	} else {
		free(param->cs->message);
		param->cs->status = "TRANS_DATABASE_ERROR";
		param->cs->message = xstrdup(errstr);
	}
}

static void
vf_validate_store_token_bpapi(struct cmd_param *param, const char *arg, struct bpapi *ba) {

	const char *req_store_id = cmd_getval(param->cs, arg);
	if ((param = cmd_getparam(param->cs, "token")))
		bpapi_insert(ba, "token", param->value);
	if ((param = cmd_getparam(param->cs, "remote_addr")))
		bpapi_insert(ba, "remote_addr", param->value);
	if (req_store_id)
		bpapi_insert(ba, "store_id_required", req_store_id);

	bpapi_insert(ba, "valid_minutes",  bconf_value(bconf_get(trans_bconf, "auth.validtime")));
	bpapi_insert(ba, "info", param->cs->command->name);
}

static int
vf_valid_store_token(struct cmd_param *param, const char *arg) {

	if (arg && !cmd_haskey(param->cs, arg)) {
		param->error = "ERROR_TOKEN_INVALID";
		return 1;
	}

	validate_on_db(param, arg,
		NULL, NULL,
		"pgsql.master", "sql/call_validate_token.sql",
		vf_validate_store_token_bpapi, vf_validate_store_token_error, vf_validate_store_token_cb
	);

	return 0;
}

struct validator v_store_token[] = {
        VALIDATOR_FUNC(vf_valid_store_token),
        {0}
};
ADD_VALIDATOR(v_store_token);

static int
vf_timestamp_format(struct cmd_param *param, const char *arg) {
	struct tm tm;
	if (cmd_haskey(param->cs, "rel")) {
		param->error = "ERROR_MULTIPLE_TIMES";
		param->cs->status = "TRANS_ERROR";
	} else if (strptime(param->value, "%F %T", &tm) == NULL) {
		param->error = "ERROR_TIME_FORMAT";
		param->cs->status = "TRANS_ERROR";
	}
	return 0;
}
static struct validator v_timestamp[] = {
	VALIDATOR_FUNC(vf_timestamp_format),
	{0}
};

ADD_VALIDATOR(v_timestamp);

static int
vf_date_long_format(struct cmd_param *param, const char *arg) {
	struct tm tm;
	if (strptime(param->value, "%F", &tm) == NULL) {
		param->error = "ERROR_DATE_FORMAT";
		param->cs->status = "TRANS_ERROR";
	}
	return 0;
}
static struct validator v_date_long[] = {
	VALIDATOR_FUNC(vf_date_long_format),
	{0}
};
ADD_VALIDATOR(v_date_long);


static struct validator v_date_time[] = {
	VALIDATOR_REGEX("^[[:digit:]]{4}-[[:digit:]]{2}-[[:digit:]]{2} [[:digit:]]{2}:[[:digit:]]{2}:[[:digit:]]{2}$", REG_EXTENDED, "ERROR_DATE_INVALID"),
	VALIDATOR_FUNC(vf_date_long_format),
	{0}
};
ADD_VALIDATOR(v_date_time);

static struct validator v_date[] = {
	VALIDATOR_REGEX("^[[:digit:]]{4}-[[:digit:]]{2}-[[:digit:]]{2}$", REG_EXTENDED, "ERROR_DATE_INVALID"),
	{0}
};
ADD_VALIDATOR(v_date);

static int
vf_pack_expiration_date(struct cmd_param *param, const char *arg) {

	int result = 0;
	int pack_max_days = bconf_get_int(trans_bconf, "packs.maximum_pack_expiration_date");

	/* YYYY-MM-DD */
	struct tm tm = {0};
	strptime(param->value, "%Y-%m-%d", &tm);

	/* dates in seconds */
	time_t now = time(NULL);
	time_t desired = mktime(&tm);

	/* dates in days */
	now /= 60 * 60 * 24;
	desired /= 60 * 60 * 24;

	time_t diff = desired - now;

	if (diff <= 0) {
		transaction_printf(param->cs->ts, "%s:ERROR_PACK_EXPIRATION_DATE_NEGATIVE\n",   param->key);
		param->cs->status = "TRANS_ERROR";
		result = 1;
	} else if (diff > pack_max_days) {
		transaction_printf(param->cs->ts, "%s:ERROR_PACK_EXPIRATION_DATE_TO_BIG\n",   param->key);
		param->cs->status = "TRANS_ERROR";
		result = 1;
	}

	return result;
}

static struct validator v_pack_expiration_date[] = {
	VALIDATOR_REGEX("^[[:digit:]]{4}-[[:digit:]]{2}-[[:digit:]]{2}$", REG_EXTENDED, "ERROR_DATE_INVALID"),
	VALIDATOR_FUNC(vf_pack_expiration_date),
	{0}
};
ADD_VALIDATOR(v_pack_expiration_date);

static int
vf_validate_false_date(struct cmd_param *param, const char *arg) {
	char *saveptr = NULL;
	char *date = xstrdup(param->value);
	char *year_s = strtok_r(date, "-", &saveptr);
	char *month_s = strtok_r(NULL, "-", &saveptr);
	char *day_s = strtok_r(NULL, "-", &saveptr);

	if (day_s && month_s && year_s) {
		int year = atoi(year_s);
		int month = atoi(month_s);
		int day = atoi(day_s);

		if (year) {
			time_t t = time(NULL);
			struct tm tm = *localtime(&t);
			int current_year = tm.tm_year + 1900;

			if (year < 1905 || year > current_year) {
				param->error = "ERROR_DATE_FORMAT";
				param->cs->status = "TRANS_ERROR";
				free(date);
				return 0;
			}
		}

		if (month < 1 || month > 12 || day < 1 || day > 31) {
			param->error = "ERROR_DATE_FORMAT";
			param->cs->status = "TRANS_ERROR";
			free(date);
			return 0;
		}

		/*Validate 28, 29, 30 and 31 day*/
		if (day == 28 || day == 29 || day == 30 || day == 31) {
			int days = 31;
			if (month == 2) {
				days = 28;
				if (year % 400 == 0 || (year % 4 == 0 && year % 100 != 0)) {
					days = 29;
				}
			} else if (month == 4 || month == 6 || month == 9 || month == 11) {
				days = 30;
			}

			if (day > days) {
				param->error = "ERROR_DATE_FORMAT";
				param->cs->status = "TRANS_ERROR";
				free(date);
				return 0;
			}
		}
	}

	free(date);
	return 1;
}

static struct validator v_date_optional[] = {
	VALIDATOR_REGEX("^([[:digit:]]{4}-((0[1|3|5|7|8]|1[02])-(0[1-9]|[12][0-9]|3[01])|((0[4|6|9]|11)-(0[1-9]|[12][0-9]|30))|((02)-(0[1-9]|[12][0-9]))))?$", REG_EXTENDED, "ERROR_DATE_INVALID"),
	VALIDATOR_FUNC(vf_validate_false_date),
	{0}
};
ADD_VALIDATOR(v_date_optional);

static struct validator v_time[] = {
	VALIDATOR_REGEX("^[[:digit:]]{2}:[[:digit:]]{2}$", REG_EXTENDED, "ERROR_TIME_INVALID"),
	{0}
};
ADD_VALIDATOR(v_time);

static struct validator v_stat[] = {
	VALIDATOR_REGEX("^STORE_VIEW$", REG_EXTENDED, "ERROR_STAT_INVALID"),
	{0}
};
ADD_VALIDATOR(v_stat);

static struct validator v_date_content[] = {
			/*|Year after 2K  |  31 days Month      -  31 days                |  30 days month   30 days            | February 28 or 29 days*/
	VALIDATOR_REGEX("^2[0-9]{3}-((0[1|3|5|7|8]|1[02])-(0[1-9]|[12][0-9]|3[01])|((0[4|6|9]|11)-(0[1-9]|[12][0-9]|30))|((02)-(0[1-9]|[12][0-9])))$", REG_EXTENDED, "ERROR_DATE_INVALID"),
	{0}
};

ADD_VALIDATOR(v_date_content);

static struct validator v_year_month[] = {
	VALIDATOR_REGEX("^[[:digit:]]{4}-[[:digit:]]{2}$", REG_EXTENDED, "ERROR_YEAR_MONTH_INVALID"),
	{0}
};
ADD_VALIDATOR(v_year_month);

static struct validator v_year[] = {
	VALIDATOR_REGEX("^[[:digit:]]{4}$", REG_EXTENDED, "ERROR_YEAR_INVALID"),
	{0}
};
ADD_VALIDATOR(v_year);

static struct validator v_month[] = {
	VALIDATOR_REGEX("^(1|2|3|4|5|6|7|8|9|10|11|12)$", REG_EXTENDED, "ERROR_MONTH_INVALID"),
	{0}
};
ADD_VALIDATOR(v_month);

static void
validate_filter_cb(struct filter_data *data, int status, int rule_id, const char *rule_name,
		   const char *action, const char *target, const char *matched_column, const char *matched_value,
		   int combi_match, const char *errstr) {
        struct cmd_param *param = (struct cmd_param*)data->data;

        if (status == FILTER_ERROR) {
                param->cs->status = "TRANS_DATABASE_ERROR";
		if (param->cs->message)
			free(param->cs->message);
		param->cs->message = xstrdup(errstr);
		param->cs->pending_validators--;
		param->cs->validator_cb(param->cs);
		free(data);
	} else if (status == FILTER_MATCH) {
		struct bconf_node *options = bconf_get(trans_bconf, "common.filter_rule.option");
		struct bconf_node *option;
		int i;

		/* Check if match should be hidden. */
		for (i = 0; (option = bconf_byindex(options, i)); i++) {
			const char *name = bconf_get_string(option, "name");
			if (name && !strcmp(name, matched_column)) {
				if (bconf_get_int(option, "hide_match")) {
					if (strstr(rule_name, bconf_get_string(trans_bconf, "common.unpaid_list.name")) == NULL) {
						param->cs->error = "ERROR_USER_BLOCKED";
					} else {
						param->cs->error = "ERROR_USER_BLOCKED_BY_DEBT";
					}
					param->cs->status = "TRANS_ERROR";
					transaction_logprintf(param->cs->ts, D_WARNING, "HIDDEN MATCH: BLOCKED (%s) %s", matched_column, matched_value);
					return;
				}
				break;
			}
		}

		if (!combi_match) {
			char *upper_column = xstrdup(matched_column);

			if (param->message)
				free(param->message);

			param->message = xstrdup(matched_value);

			transaction_logprintf(param->cs->ts, D_WARNING,
					      "BLOCKED (%s) %s", matched_column, matched_value);
			transaction_printf(param->cs->ts, "%s:ERROR_%s_BLOCKED:%s\n",
					   matched_column, strmodify(upper_column, toupper), matched_value);
			free(upper_column);
		} else {
			param->cs->error = "ERROR_BLOCKED_COMBO";
		}
                param->cs->status = "TRANS_ERROR";
        } else if (status == FILTER_DONE) {
		free(data);
		transaction_logprintf(param->cs->ts, D_DEBUG, "ending name badword check");
		param->cs->pending_validators--;
		param->cs->validator_cb(param->cs);
	}

	return;
}

static int
vf_filter(struct cmd_param *param, const char *arg) {
	struct filter_data *fdata = filter_init(NULL);
	struct cmd *cs = param->cs;
	char *arg_cpy = xstrdup(arg);
	char *elem_name;
	char *delim = NULL;
	char *tmp = arg_cpy;

	while ((elem_name = strtok_r(arg_cpy, ",", &delim))) {
		if (cmd_haskey(cs, elem_name))
			filter_add_element(fdata, elem_name, cmd_getval(cs, elem_name));
		arg_cpy = NULL;
	}

	free(tmp);

	if (!SLIST_EMPTY(&fdata->filter_params)) {
		fdata->cb = validate_filter_cb;
		fdata->data = param;
		param->cs->pending_validators++;
		filter_data(fdata, cs->command->name);
	} else {
		free(fdata);
	}

	return 0;
}


struct validator v_filter[] = {
	VALIDATOR_FUNC(vf_filter),
	{0}
};

ADD_VALIDATOR(v_filter);

static int
vf_category_text_warning(struct cmd_param *param, const char *arg) {
	struct cmd *cs = param->cs;
	char *elem_name;
	char *delim = NULL;
	char *tmp;
	char *arg_cpy;

	transaction_logprintf(cs->ts, LOG_DEBUG, "category_text_warning: %s", param->value);

	if (param->error) /* Don't overwrite existing errors */
		return 0;

	tmp = arg_cpy = xstrdup(arg);

	while (!param->error && (elem_name = strtok_r(arg_cpy, ",", &delim))) {
		const char *val = cmd_getval(cs, elem_name);
		const char *regex = bconf_value(bconf_vget(host_bconf, "newad.category_text_warning.cat", param->value, elem_name, "regex", NULL));
		const char *parent = bconf_value(bconf_vget(host_bconf, "*.cat", param->value, "parent", NULL));
		const char *regex_parent = bconf_value(bconf_vget(host_bconf, "newad.category_text_warning.parent", parent, elem_name, "regex", NULL));

		int flags = PCRE_CASELESS | PCRE_NO_AUTO_CAPTURE | PCRE_DOTALL | PCRE_DOLLAR_ENDONLY;
		if (val && regex) {
			pcre *cregex;
			const char *err;
			int erro;

			transaction_logprintf(cs->ts, LOG_DEBUG, "category_text_warning: matching %s vs %s", val, regex);

			if (!(cregex = pcre_compile(regex, flags, &err, &erro, NULL))) {
				transaction_logprintf(cs->ts, D_ERROR, "(CRIT) category_text_warning: failed to compile regex %s: %s", regex, err);
			} else {
				int match = pcre_exec(cregex, NULL, val, strlen(val), 0, 0, NULL, 0);

				if (match >= 0)
					param->error = bconf_value(bconf_vget(host_bconf, "newad.category_text_warning.cat", param->value, elem_name, "warning", NULL));
				pcre_free(cregex);
			}
		}

		if (val && regex_parent && parent) {
			pcre *cregex;
			const char *err;
			int erro;

			if (!(cregex = pcre_compile(regex_parent, flags, &err, &erro, NULL))) {

				transaction_logprintf(cs->ts, D_ERROR, "(CRIT) category_text_warning: failed to compile regex %s: %s", regex_parent, err);
			} else {
				int match = pcre_exec(cregex, NULL, val, strlen(val), 0, 0, NULL, 0);
				if (match >= 0)
					param->error = bconf_value(bconf_vget(host_bconf, "newad.category_text_warning.parent", parent, elem_name, "warning", NULL));
				pcre_free(cregex);
			}
		}

		arg_cpy = NULL;
	}

	free(tmp);

	return 0;
}

struct validator v_category_text_warning[] = {
	VALIDATOR_FUNC(vf_category_text_warning),
	{0}
};

ADD_VALIDATOR(v_category_text_warning);

static struct validator v_search_id[] = {
        VALIDATOR_REGEX("^[[:digit:]]{1,7}$", REG_EXTENDED, "ERROR_ID_INVALID"),
        {0}
};
ADD_VALIDATOR(v_search_id);

static struct validator v_search_reg[] = {
        VALIDATOR_INT(0, 23, "ERROR_REG"),
        {0}
};
ADD_VALIDATOR(v_search_reg);

static struct validator v_search_near[] = {
        VALIDATOR_INT(1, 23, "ERROR_NEAR"),
        {0}
};
ADD_VALIDATOR(v_search_near);

static struct validator v_search_city[] = {
        VALIDATOR_INT(1, 23, "ERROR_CITY"),
        {0}
};
ADD_VALIDATOR(v_search_city);

static void
vf_url_path_uniq_cb(struct cmd_param *param, struct sql_worker *worker, const char *arg) {
	worker->next_row(worker);
	int n = atoi(worker->get_value(worker, 0));
	if (n > 0) {
		param->cs->status = "TRANS_ERROR";
		param->error = "ERROR_URL_PATH_NOT_UNIQ";
	}
}

static void
vf_url_path_uniq_bpapi(struct cmd_param *param, const char *arg, struct bpapi *ba) {
	if (cmd_haskey(param->cs, "store_id"))
		bpapi_insert(ba, "store_id", cmd_getval(param->cs, "store_id"));
	bpapi_insert(ba, "url_path", param->value);
}

static int
vf_url_path_uniq(struct cmd_param *param, const char *arg) {
	validate_on_db(param, arg,
		NULL, NULL,
		"pgsql.master", "sql/url_path_uniq.sql",
		vf_url_path_uniq_bpapi, NULL, vf_url_path_uniq_cb
	);
	return 0;
}

static int
vf_url_path_reserved(struct cmd_param *param, const char *arg) {
	if (bconf_vget(host_bconf, "controlpanel", "modules", "stores", "url_path", cmd_getval(param->cs, "url_path"), "reserved", NULL)) {	
		param->cs->status = "TRANS_ERROR";
		param->error = "ERROR_URL_PATH_RESERVED";
	}

	return 0;
}

struct validator v_url_path[] = {
	VALIDATOR_REGEX("^[" ASCALNUM "-]*$", REG_EXTENDED, "ERROR_URL_PATH_INVALID"),
	VALIDATOR_LEN(5, -1, "ERROR_URL_PATH_SHORT"),
	VALIDATOR_LEN(-1, 40, "ERROR_URL_PATH_LONG"),
	VALIDATOR_FUNC(vf_url_path_reserved),
	VALIDATOR_FUNC(vf_url_path_uniq),
	{0}
};
ADD_VALIDATOR(v_url_path);

struct validator v_cat_list[] = {
	VALIDATOR_REGEX("^([[:digit:]]{4,4})(,[[:digit:]]{4,4})*$", REG_EXTENDED, "ERROR_INVALID_CATEGORY_LIST"),
	{0}
};
ADD_VALIDATOR(v_cat_list);

struct validator v_list_id[] = {
	VALIDATOR_LEN(-1, 10, "ERROR_LIST_ID_LONG"),
	VALIDATOR_INT(0, -1, "ERROR_LIST_ID_INVALID"),
	{0}
};
ADD_VALIDATOR(v_list_id);

static struct c_param search_types[] = {
	{"list_id", 0, "v_list_id"},
	{"ad_id", 0, "v_id"},
	{"store_id", 0, "v_id"},
	{"email", 0, "v_email_basic"},
	{"email_part", 0, "v_string_isprint"},
	{"user_id", P_REQUIRED, "v_user_id"},
	{"pay_code", P_REQUIRED, "v_pay_code"},
	{"orderid", P_REQUIRED, "v_orderid"},
	{"uid", P_REQUIRED, "v_uid"},
	{"subject", P_REQUIRED, "v_query"},
	{"name", P_REQUIRED, "v_query"},
	{"phone", P_REQUIRED, "v_phone_basic"},
	{"query", P_REQUIRED, "v_query"},
	{"hidden", P_REQUIRED, "v_query"},
	{"ip", 0, "v_ip"},
	{"category", 0, "v_integer"},
	{"invoice", 0, "v_integer"},
	{NULL}
};

static int
v_check_search_type(struct cmd_param *param, const char *arg) {
	struct c_param *st;
	const char *value = param->value;

	for (st = search_types; st->name; st++) {
		if (!strcmp(st->name, value)) {
			cmd_extra_param(param->cs, st);
			return 0;
		}
	}
	param->error = "ERROR_SEARCH_TYPE_INVALID";
	param->cs->status = "TRANS_ERROR";
	return 0;
}

struct validator v_search_type[] = {
	VALIDATOR_FUNC(v_check_search_type),
	{0}
};
ADD_VALIDATOR(v_search_type);

struct c_param v_del_reason = {"deletion_reason", P_REQUIRED, "v_deletion_reason"};
struct c_param v_del_reason_opt = {"deletion_reason", 0, "v_deletion_reason"};
struct c_param v_user_text = {"user_text", 0, "v_string_isprint"};

static int
vf_reason_user_deleted(struct cmd_param *param, const char *arg) {
	if (!strcmp(param->value, "user_deleted")) {
		cmd_extra_param(param->cs, &v_del_reason);
		cmd_extra_param(param->cs, &v_user_text);
	}
	return 0;
}

static int
vf_reason_auto_deleted(struct cmd_param *param, const char *arg) {
	if (!strcmp(param->value, "auto_deleted")) {
		cmd_extra_param(param->cs, &v_del_reason_opt);
		cmd_extra_param(param->cs, &v_user_text);
	}
	return 0;
}

struct validator v_reason[] = {
	VALIDATOR_REGEX("^[[:print:]]+$", REG_EXTENDED, "ERROR_REASON_INVALID"),
	VALIDATOR_LEN(-1, 64, "ERROR_REASON_INVALID"),
	VALIDATOR_FUNC(vf_reason_user_deleted),
	VALIDATOR_FUNC(vf_reason_auto_deleted),
	{0}
};
ADD_VALIDATOR(v_reason);

struct validator v_testimonial[] = {
	VALIDATOR_LEN(2, -1, "ERROR_TESTIMONIAL_TOO_SHORT"),
	VALIDATOR_LEN(-1, 1500, "ERROR_TESTIMONIAL_TOO_LONG"),
	VALIDATOR_REGEX("^([[:print:]]|[[:space:]])+$", REG_EXTENDED, "ERROR_TESTIMONIAL_INVALID"),
	{0}
};
ADD_VALIDATOR(v_testimonial);

struct validator v_site_area[] = {
	VALIDATOR_REGEX("^[[:print:]]+$", REG_EXTENDED, "ERROR_SITE_AREA_INVALID"),
	VALIDATOR_LEN(2, -1, "ERROR_SITE_AREA_TOO_SHORT"),
	{0}
};
ADD_VALIDATOR(v_site_area);

struct c_param v_del_timer = {"deletion_timer", P_REQUIRED, "v_deletion_timer"};

static int
vf_reason_deletion_timer(struct cmd_param *param, const char *arg) {
	if (!strcmp(param->value, "1")) {
		cmd_extra_param(param->cs, &v_del_timer);
	}
	return 0;
}

struct validator v_deletion_reason[] = {
	VALIDATOR_INT(1, 6, "ERROR_DELETION_REASON_INVALID"),
	VALIDATOR_BCONF_LIST_KEY("deletion_reason", "ERROR_INVALID_DELETION_REASON"),
	VALIDATOR_FUNC(vf_reason_deletion_timer),
	{0}
};
ADD_VALIDATOR(v_deletion_reason);

struct validator v_deletion_timer[] = {
	VALIDATOR_INT(1, 5, "ERROR_DELETION_TIMER_INVALID"),
	VALIDATOR_BCONF_LIST_KEY("deletion_reason_timer", "ERROR_INVALID_DELETION_TIMER"),
	{0}
};
ADD_VALIDATOR(v_deletion_timer);

struct validator v_remote_browser[] = {
	VALIDATOR_LEN(-1, 1024, "ERROR_REMOTE_BROWSER_INVALID"),
	{0}
};

ADD_VALIDATOR(v_remote_browser);

struct validator v_info_text[] = {
	VALIDATOR_LEN(20, -1, "ERROR_INFO_TEXT_TOO_SHORT"),
	VALIDATOR_LEN(-1, 500, "ERROR_INFO_TEXT_TOO_LONG"),
	VALIDATOR_FUNC(vf_store_bad_words_db),
	{0}
};

ADD_VALIDATOR(v_info_text);

static int
vf_search_value(struct cmd_param *param, const char *arg) {
	if (cmd_haskey(param->cs, "search_key")) {
		const char *search_key = cmd_getval(param->cs, "search_key");
		if (strcmp(search_key, "list_id") == 0 || strcmp(search_key, "ad_id") == 0 || strcmp(search_key, "store_id") == 0) {
			if (!atoi(param->value)) {
				param->error = "ERROR_INTEGER_INVALID";
				param->cs->status = "TRANS_ERROR";
				return 1;
			}
		}
	}

	return 0;
}


struct validator v_search_value[] = {
	VALIDATOR_FUNC(vf_search_value),
        {0}
};
ADD_VALIDATOR(v_search_value);

struct validator v_pay_phone[] = {
	VALIDATOR_LEN(1, 50, "ERROR_PHONE_INVALID"),
	VALIDATOR_REGEX("^0(0|[1-9].*)$", REG_EXTENDED, "ERROR_PHONE_INVALID"),
	{0}
};

ADD_VALIDATOR(v_pay_phone);

struct validator v_amount[] = {
	VALIDATOR_INT(10, 100000000, "ERROR_AMOUNT_INVALID"),
	{0}
};

ADD_VALIDATOR(v_amount);

struct validator v_reference[] = {
	VALIDATOR_REGEX("^[0-9a-zA-Z_-]{0,40}$", REG_EXTENDED|REG_NOSUB, "ERROR_REFERENCE_INVALID"),
	{0}
};
ADD_VALIDATOR(v_reference);

static int
v_pay_time_format(struct cmd_param *param, const char *arg) {
	struct tm tm;
	if (strptime(param->value, "%F %T", &tm) == NULL) {
		param->error = "ERROR_TIME_FORMAT";
		param->cs->status = "TRANS_ERROR";
		return 1;
	} else {
		return 0;
	}
}

struct validator v_pay_time[] = {
	VALIDATOR_FUNC(v_pay_time_format),
	{0}
};

ADD_VALIDATOR(v_pay_time);

static struct c_param ref_type = {"ref_type", P_REQUIRED, "v_ref_type"};
static struct c_param reference = {"reference", P_REQUIRED, "v_id"};
static struct c_param amount  = {"amount", P_REQUIRED, "v_amount"};
static struct c_param status = {"status", P_REQUIRED, "v_clear_status"};
static struct c_param phone  = {"phone", P_REQUIRED, "v_phone_basic"};
static struct c_param pay_time  = {"pay_time", P_REQUIRED, "v_pay_time"};

static int
pay_code(struct cmd_param *param, const char *arg) {
	if (!cmd_haskey(param->cs, "admin_clear")) {
		cmd_extra_param(param->cs, &phone);
		cmd_extra_param(param->cs, &amount);
		cmd_extra_param(param->cs, &pay_time);
	}
	return 0;
}


static int
vf_order_id(struct cmd_param *param, const char *arg) {
	if (!cmd_haskey(param->cs, "admin_clear")) {
		if (!cmd_haskey(param->cs, "paid_with_voucher")) {
			cmd_extra_param(param->cs, &ref_type);
			cmd_extra_param(param->cs, &reference);
		}
		cmd_extra_param(param->cs, &amount);
		cmd_extra_param(param->cs, &status);
	}

	return 0;
}

struct validator v_order_id[] = {
	VALIDATOR_REGEX("^[0-9a]{3,32}$", REG_EXTENDED, "ERROR_ORDER_ID_INVALID"),
	VALIDATOR_OTHER("admin_clear", P_OPTIONAL, "v_admin_clear"),
	VALIDATOR_OTHER("paid_with_voucher", P_OPTIONAL, "v_bool"),
	VALIDATOR_FUNC(vf_order_id),
	{0}
};

ADD_VALIDATOR(v_order_id);

struct validator v_order_id_simple[] = {
	VALIDATOR_REGEX("^[0-9a]{3,32}$", REG_EXTENDED, "ERROR_ORDER_ID_INVALID"),
	{0}
};

ADD_VALIDATOR(v_order_id_simple);

struct validator v_clear_pay_code[] = {
	VALIDATOR_INT(10000, 99999, "ERROR_PAY_CODE_INVALID"),
	VALIDATOR_OTHER("admin_clear", P_OPTIONAL, "v_admin_clear"),
	VALIDATOR_FUNC(pay_code),
	{0}
};

ADD_VALIDATOR(v_clear_pay_code);

static int
vf_hide_address(struct cmd_param *param, const char *arg) {
	const char  *store_id = 0;

	int hide_address = 0;
	if (!param->value) {
		param->error = "ERROR_HIDE_ADDRESS_MISSING";
		param->cs->status = "TRANS_ERROR";
		return 1;
	}

	hide_address = atoi(param->value);
	store_id = cmd_getval(param->cs, "store");


	if (hide_address == 0)
		return 0;
	if (!store_id || atoi(store_id) == 0) {
		param->error = "ERROR_STORE_MISSING";
		param->cs->status = "TRANS_ERROR";
		return 1;
	}
	return 0;
}
struct validator v_hide_address[] = {
	VALIDATOR_FUNC(vf_hide_address),
	{0}
};

ADD_VALIDATOR(v_hide_address);

static int
vf_area_check(struct cmd_param *param, const char *arg) {
	const char *region;
	const char *city;
	char *municipality;
	const char *subarea = NULL;
	char *pos;
	struct bconf_node *area_node;

	if (!param->value) {
		param->error = "ERROR_AREA_MISSING";
		param->cs->status = "TRANS_ERROR";
		return 1;
	}
	region = cmd_getval(param->cs, "region");
	if (!region) {
		param->error = "ERROR_REGION_MISSING";
		param->cs->status = "TRANS_ERROR";
		return 1;
	}

	/* split param->value to municipiality:subarea */
	municipality = xstrdup(param->value);
	pos = strchr(municipality, ':');
	if (pos) {
		*pos = '\0';
		subarea = ++pos;
	}

	area_node = bconf_vget(host_bconf, "*", "common", "region", region, NULL);

	city = cmd_getval(param->cs, "city");
	if (city)
		area_node = bconf_vget(area_node, "city", city, NULL);


	area_node = bconf_vget(area_node, "municipality", municipality, NULL);

	if (subarea) {
		area_node = bconf_vget(area_node, "subarea", subarea, NULL);
	} else {
		/* Missing subarea, check that there is not subarea in bconf */
		if (area_node && bconf_get(area_node, "subarea")) {
			param->error = "ERROR_AREA_MISSING_SUBAREA";
			param->cs->status = "TRANS_ERROR";
			free(municipality);
			return 1;
		}
	}

	if (!area_node) {
		param->error = "ERROR_AREA_REQUIRED";
		param->cs->status = "TRANS_ERROR";
		free(municipality);
		return 1;
	}

	free(municipality);

	return 0;
}

struct validator v_area[] = {
        VALIDATOR_FUNC(vf_area_check),
	{0}
};
ADD_VALIDATOR(v_area);

struct validator v_queue_queue[] = {
	VALIDATOR_LEN(1, 30, "ERROR_QUEUE_NAME_TOO_LONG"),
	{0}
};
ADD_VALIDATOR(v_queue_queue);

struct validator v_query[] = {
	VALIDATOR_LEN(2, -1, "ERROR_QUERY_TOO_SHORT"),
	{0}
};
ADD_VALIDATOR(v_query);

struct validator v_bid_price[] = {
	VALIDATOR_NREGEX("^$", REG_EXTENDED, "ERROR_BID_MISSING"),
	VALIDATOR_INT(0, -1, "ERROR_BID_PRICE_INVALID"),
	{0}
};
ADD_VALIDATOR(v_bid_price);

struct validator v_bid_companyname[] = {
	VALIDATOR_NREGEX("[%^/\\()<>=#\"+]", REG_EXTENDED, "ERROR_COMPANY_INVALID"),
	VALIDATOR_REGEX("[[:alpha:]]+", REG_EXTENDED, "ERROR_COMPANY_INVALID"),
	{0}
};
ADD_VALIDATOR(v_bid_companyname);


struct validator v_event_name[] = {
	VALIDATOR_LEN(1, 20, "ERROR_EVENT_NAME_TOO_LONG"),
	{0}
};
ADD_VALIDATOR(v_event_name);


struct validator v_info_queue[] = {
	VALIDATOR_LEN(1, 30, "ERROR_INFO_TOO_LONG"),
	{0}
};
ADD_VALIDATOR(v_info_queue);

static int
vf_bconf_value(struct cmd_param *param, const char *arg) {
	struct bconf_node *node = bconf_get(host_bconf, arg);
	int num = bconf_count(node);
	int i;

	for (i = 0; i < num; i++) {
		const char *val = bconf_value(bconf_byindex(node, i));

		if (val && strcmp(val, param->value) == 0)
			return 0;
	}

	param->error = "ERROR_VALUE_NOT_FOUND";
	param->cs->status = "TRANS_ERROR";
	return 0;
}

struct validator v_bconf_value[] = {
	VALIDATOR_FUNC(vf_bconf_value),
	{0}
};
ADD_VALIDATOR(v_bconf_value);

/*
 * UUID validator, ex uuid: ea719d13-ddcd-466e-86fc-b9ad8747ff08
 */
struct validator v_uuid[] = {
	VALIDATOR_REGEX("^[0-9a-f]{8}(-[0-9a-f]{4}){3}-[0-9a-f]{12}$", REG_EXTENDED, "ERROR_UUID_INVALID"),
	{0}
};
ADD_VALIDATOR(v_uuid);

/*
 * SMS session id validator, format phonenumber:datetime (YYYYMMDDhhmmss)
 */
struct validator v_sms_session_id[] = {
	VALIDATOR_REGEX("^[0-9]{5,15}:[0-9]{1,14}$", REG_EXTENDED, "ERROR_SMS_SESSION_ID_INVALID_FORMAT"),
	{0}
};
ADD_VALIDATOR(v_sms_session_id);

struct validator v_sms_appnr[] = {
	VALIDATOR_REGEX("^[0-9]{5}$", REG_EXTENDED, "ERROR_SMS_APPNR_INVALID"),
	{0}
};
ADD_VALIDATOR(v_sms_appnr);

struct validator v_sms_status_code[] = {
	VALIDATOR_REGEX("^[1-9]$", REG_EXTENDED, "ERROR_SMS_STATUS_CODE_INVALID_FORMAT"),
	{0}
};
ADD_VALIDATOR(v_sms_status_code);

struct validator v_sms_phone[] = {
	VALIDATOR_LEN(5, 15, "ERROR_SMS_PHONE_INVALID_LENGTH"),
	VALIDATOR_REGEX("^[0-9]+$", REG_EXTENDED, "ERROR_SMS_PHONE_INVALID_CHARS"),
	{0}
};
ADD_VALIDATOR(v_sms_phone);

struct validator v_sms_text[] = {
	VALIDATOR_LEN(0, 1000, "ERROR_SMS_TEXT_INVALID_LENGTH"),
	{0}
};
ADD_VALIDATOR(v_sms_text);

struct validator v_sms_smsc[] = {
	VALIDATOR_BCONF_LIST("unwire_sms.smsc", "ERROR_INVALID_SMSC"),
	{0}
};
ADD_VALIDATOR(v_sms_smsc);

struct validator v_sms_price[] = {
	VALIDATOR_BCONF_LIST("unwire_sms.tariff", "ERROR_INVALID_SMS_PRICE"),
	{0}
};
ADD_VALIDATOR(v_sms_price);

struct validator v_sms_type[] = {
	VALIDATOR_BCONF_LIST_DOT_NAME("unwire_sms.sms_type", "ERROR_INVALID_SMS_TYPE"),
	{0}
};
ADD_VALIDATOR(v_sms_type);

struct validator v_campaign_id[] = {
	VALIDATOR_LEN(1, 3, "ERROR_CAMPAIGN_ID_INVALID"),
	VALIDATOR_REGEX("^[[:digit:]]+$", REG_EXTENDED, "ERROR_CAMPAIGN_ID_INVALID"),
	{0}
};
ADD_VALIDATOR(v_campaign_id);

struct validator v_source[] = {
	VALIDATOR_BCONF_LIST("ai.source", "ERROR_INVALID_SOURCE"),
	{0}
};
ADD_VALIDATOR(v_source);

struct validator v_sms_user_status[] = {
	VALIDATOR_REGEX("^(active|paused|adminpaused)$", REG_EXTENDED, "ERROR_INVALID_SMS_USER_STATUS"),
	{0}
};
ADD_VALIDATOR(v_sms_user_status);

struct validator v_watch_query_sms_status[] = {
	VALIDATOR_REGEX("^(active|paused|auto-paused)$", REG_EXTENDED, "ERROR_INVALID_WATCH_QUERY_SMS_STATUS"),
	{0}
};
ADD_VALIDATOR(v_watch_query_sms_status);

struct validator v_printable[] = {
        VALIDATOR_REGEX("[[:print:]]", REG_EXTENDED, "ERROR_STRING_INVALID"),
        {0}
};
ADD_VALIDATOR(v_printable);

struct validator v_stat_type[] = {
        VALIDATOR_REGEX("(view|mail)", REG_EXTENDED, "ERROR_INVALID_STAT_TYPE"),
        {0}
};
ADD_VALIDATOR(v_stat_type);

struct validator v_string_isprint[] = {
	VALIDATOR_REGEX("(*UCP)^[[:print:][:alpha:]]+$", REG_EXTENDED, "ERROR_STRING_INVALID"),
	{0}
};
ADD_VALIDATOR(v_string_isprint);

struct validator v_string_isprint_limited[] = {
	VALIDATOR_REGEX("(*UCP)^[[:print:][:alpha:]]+$", REG_EXTENDED, "ERROR_STRING_INVALID"),
	VALIDATOR_LEN(-1, 60, "ERROR_PARAMETER_TOO_LONG"),
	{0}
};
ADD_VALIDATOR(v_string_isprint_limited);

struct validator v_string_isprint_blob[] = {
	VALIDATOR_REGEX("(*UCP)^[[:print:][:alpha:]\n]+$", REG_EXTENDED, "ERROR_STRING_INVALID"),
	{0}
};
ADD_VALIDATOR(v_string_isprint_blob);

struct validator v_string_optional[] = {
	VALIDATOR_REGEX("(*UCP)^[[:print:][:alpha:]]*$", REG_EXTENDED, "ERROR_STRING_INVALID"),
	{0}
};
ADD_VALIDATOR(v_string_optional);

struct validator v_external_receipt[] = {
	VALIDATOR_REGEX("(*UCP)^[[:print:][:alpha:]]+$", REG_EXTENDED, "ERROR_EXTERNAL_RECEIPT_INVALID"),
	{0}
};
ADD_VALIDATOR(v_external_receipt);

struct validator v_gallery[] = {
	VALIDATOR_REGEX("^(request)$", REG_EXTENDED, "ERROR_INVALID_GALLERY_STRING"),
	{0}
};
ADD_VALIDATOR(v_gallery);

struct validator v_zipcode[] = {
   VALIDATOR_LEN(8, 8, "ERROR_ZIPCODE_INVALID"),
   {0}
};
ADD_VALIDATOR(v_zipcode);

struct validator v_estate_type[] = {
	VALIDATOR_BCONF_LIST_KEY("common.estate_type", "ERROR_ESTATE_TYPE_INVALID"),
	{0}
};
ADD_VALIDATOR(v_estate_type);

/* Dummy blob validators that just checks that any value exists */
static int
vf_blob(struct cmd_param *param, const char *arg) {
	if (param->value_size)
		return 0;

	param->error = "ERROR_EMPTY_BLOB";
	param->cs->status = "TRANS_ERROR";
	return 0;
}

struct validator v_blob[] = {
	VALIDATOR_FUNC(vf_blob),
	{0}
};
ADD_VALIDATOR(v_blob);

struct validator v_lang[] = {
	VALIDATOR_BCONF_LIST_KEY("common.lang", "ERROR_LANG_INVALID"),
	{0}
};
ADD_VALIDATOR(v_lang);


struct validator v_unique_order_id[] = {
	VALIDATOR_REGEX("[0-9]{0,16}a[0-9]{0,16}", REG_NOSUB|REG_EXTENDED, "ERROR_ORDERID_INVALID"),
	{0}
};
ADD_VALIDATOR(v_unique_order_id);

/* From trans_clear */
struct validator v_verify_code[] = {
	VALIDATOR_LEN(6, 10, "ERROR_VERIFY_CODE_INVALID"),
	VALIDATOR_REGEX("^[a-z0-9]{6,10}$", REG_EXTENDED|REG_ICASE|REG_NOSUB, "ERROR_VERIFY_CODE_INVALID"),
	{0}
};

ADD_VALIDATOR(v_verify_code);

/* Used in yapo.cl because of multicurrency */
static void
vf_currency_cb(const char *setting, const char *key, const char *value, void *cbdata) {
	struct trans_key_lookup_data *data = cbdata;

	if (!strcmp(key, "uf"))
		data->result = atoi(value);
	else
		data->result = 0;
}

static int
vf_currency(struct cmd_param *param, const char *arg) {
	if (!cmd_haskey(param->cs, "category"))
		return 1;

	const char *curr = param->value;
	if (!strcmp(curr, "uf")) {
		struct trans_key_lookup_data cbdata;
		struct bpapi_vtree_chain vtree;

		cbdata.param = param;
		cbdata.has_param = curr;
		cbdata.result = 0;

		get_settings(bconf_vtree(&vtree, bconf_get(trans_bconf, "category_settings")), "currency", trans_key_lookup, vf_currency_cb, &cbdata);
		vtree_free(&vtree);

		if(!cbdata.result) {
			param->error = "ERROR_INVALID_CURRENCY_CATEGORY";
			param->cs->status = "TRANS_ERROR";
		}
	}
	return 0;
}

struct validator v_currency[] = {
	VALIDATOR_REGEX("^(uf|peso)$", REG_EXTENDED|REG_ICASE|REG_NOSUB, "ERROR_INVALID_CURRENCY"),
	VALIDATOR_FUNC(vf_currency),
	{0}
};
ADD_VALIDATOR(v_currency);

static struct c_param price_uf = {"price", 0, "v_price_uf"};
static struct c_param price_peso = {"price", 0, "v_price"};

static int
vf_currency_check(struct cmd_param *param, const char *arg) {
	if (cmd_haskey(param->cs, "currency") && strcmp(cmd_getval(param->cs, "currency"), "uf")==0 ) {
		cmd_extra_param(param->cs, &price_uf);
	} else {
		cmd_extra_param(param->cs, &price_peso);
	}
	return 0;
}

struct validator v_currency_check[] = {
	VALIDATOR_FUNC(vf_currency_check),
	{0}
};
ADD_VALIDATOR(v_currency_check);

struct validator v_first_image[] = {
	VALIDATOR_REGEX("^[0-1]?[0-9]$", REG_EXTENDED|REG_ICASE|REG_NOSUB, "ERROR_VERIFY_FIRST_IMAGE_INVALID"),
	{0}
};

ADD_VALIDATOR(v_first_image);

struct validator v_account_hashed_password[] = {
	VALIDATOR_REGEX("^\\$[0-9]+\\$.{16}[0-9A-Fa-f]{40}$", REG_EXTENDED, "ERROR_PASSWORD_INVALID"),
        {0}
};
ADD_VALIDATOR(v_account_hashed_password);

//sha-1
struct validator v_account_hash[] = {
        VALIDATOR_REGEX("[0-9a-fA-F]{32}", REG_EXTENDED, "ERROR_SHA-1_HASH_INVALID"),
        {0}
};
ADD_VALIDATOR(v_account_hash);

int
format_price(struct cmd *cs, struct bpapi *ba) {

	if (cmd_haskey(cs, "price") && cmd_haskey(cs, "currency") && strcmp(cmd_getval(cs, "currency"), "uf")==0) {

		/* since we accept both separators, modify it to make it compatible with atof */
		char *price_uf = xstrdup(cmd_getval(cs, "price"));
		char* sep = strstr(price_uf, ",");
		if (sep) {
			*sep = '.' ;
		}

		/* atof will take care of the decimal part, so we can store hundreths of UFs */
		/* XXX this relies on setlocale not being called anywhere in the code. */
		xasprintf(&price_uf, "%d", (int)( atof(price_uf) * 100.0 ) );
		cmd_setval(cs, "price", xstrdup(price_uf));
		if (ba && bpapi_has_key(ba, "price")) {
			bpapi_remove(ba, "price");
			bpapi_insert(ba, "price", price_uf);
		}
		free( (char *)price_uf );
	}
	return 0;
}

/* Validating payment and accounts */
static struct c_param rut 	= {"rut", 	P_REQUIRED,	"v_pay_rut"};
static struct c_param name 	= {"name",	P_REQUIRED,	"v_pay_name"};
static struct c_param lob 	= {"lob",	P_REQUIRED,	"v_pay_lob"};
static struct c_param address 	= {"address",	P_REQUIRED,	"v_pay_address"};
static struct c_param region 	= {"region",	P_REQUIRED,	"v_region"};
static struct c_param communes 	= {"communes",	P_REQUIRED,	"v_commune"};
static struct c_param contact 	= {"contact",		 0,	"v_pay_contact"};

struct validator v_pay_name[] = {
	VALIDATOR_REGEX("(*UCP)^[[:print:][:alpha:]]*$", REG_EXTENDED, "ERROR_NAME_INVALID"),
	VALIDATOR_LEN(2, -1, "ERROR_NAME_TOO_FEW_CHARS"),
	VALIDATOR_LEN(-1, 50, "ERROR_NAME_TOO_LONG"),
	{0}
};
ADD_VALIDATOR(v_pay_name);
struct validator v_pay_lob[] = {
	VALIDATOR_REGEX("(*UCP)^[[:print:][:alpha:]]*$", REG_EXTENDED, "ERROR_LOB_INVALID"),
	VALIDATOR_LEN(2, -1, "ERROR_LOB_TOO_SHORT"),
	VALIDATOR_LEN(-1, 40, "ERROR_LOB_TOO_LONG"),
	{0}
};
ADD_VALIDATOR(v_pay_lob);

struct validator v_pay_lob_optional[] = {
	VALIDATOR_REGEX("(*UCP)^[[:print:][:alpha:]]*$", REG_EXTENDED, "ERROR_LOB_INVALID"),
	VALIDATOR_LEN(-1, 40, "ERROR_LOB_TOO_LONG"),
	{0}
};
ADD_VALIDATOR(v_pay_lob_optional);

struct validator v_pay_address[] = {
	VALIDATOR_REGEX("(*UCP)^[[:print:][:alpha:]]*$", REG_EXTENDED, "ERROR_ADDRESS_INVALID"),
	VALIDATOR_LEN(2, -1, "ERROR_ADDRESS_TOO_SHORT"),
	VALIDATOR_LEN(-1, 60, "ERROR_ADDRESS_TOO_LONG"),
	{0}
};
ADD_VALIDATOR(v_pay_address);

struct validator v_pay_address_optional[] = {
	VALIDATOR_REGEX("(*UCP)^[[:print:][:alpha:]]*$", REG_EXTENDED, "ERROR_ADDRESS_INVALID"),
	VALIDATOR_LEN(-1, 60, "ERROR_ADDRESS_TOO_LONG"),
	{0}
};
ADD_VALIDATOR(v_pay_address_optional);

struct validator v_pay_contact[] = {
	VALIDATOR_REGEX("(*UCP)^[[:print:][:alpha:]]*$", REG_EXTENDED, "ERROR_CONTACT_INVALID"),
	VALIDATOR_LEN(-1, 80, "ERROR_CONTACT_TOO_LONG"),
	{0}
};
ADD_VALIDATOR(v_pay_contact);

/* Chilean rut validator */
static int
vf_pay_rut(struct cmd_param * param, const char *arg){
	char* strtok_state = NULL;
	char* rut_wo_digit;
	char* digit_input;
	char calc_digit;

	if (strlen(param->value)==0)
		return 0;

	if(strchr(param->value,'-') == NULL){
		param->error = "ERROR_RUT_INVALID";
		param->cs->status = "TRANS_ERROR";
		return 1;
	}

	char *rut = xstrdup(param->value);

	int rut_base, digit=1, counter=0;
	/* Split rut full  */
	rut_wo_digit = strtok_r(rut, "-", &strtok_state);
	digit_input = strtok_r(NULL, "-", &strtok_state);
	if(!digit_input){
		param->error = "ERROR_RUT_INVALID";
		param->cs->status = "TRANS_ERROR";
		free(rut);
		return 1;
	}

	/* validation code below just works with uppercase K */
	if (*digit_input == 'k')
		*digit_input = 'K';

	/* Convert to int  */
	rut_base = atoi(rut_wo_digit);
	/* Calculate the digit from rut_base */
	for( ; rut_base ; rut_base/=10 ){
		digit=(digit + rut_base%10 * (9-counter%6))%11;
		counter++;
	}
	/* Get de digit  */
	calc_digit = digit?digit + 47:75;
	/* Compare the digit input with the result calculated */
	if(calc_digit != (int)*digit_input && rut_base < 100000){
		param->error = "ERROR_RUT_INVALID";
		param->cs->status = "TRANS_ERROR";
		free(rut);
		return 1;
	}

	free(rut);
	return 0;
}

struct validator v_pay_rut_optional[] = {
	VALIDATOR_LEN(-1, 10, "ERROR_RUT_TOO_LONG"),
	VALIDATOR_REGEX("(^[0-9]{4,10}-[0-9kK]{1}$)?", REG_EXTENDED, "ERROR_RUT_FORMAT_INVALID"),
	VALIDATOR_FUNC(vf_pay_rut),
	{0}
};
ADD_VALIDATOR(v_pay_rut_optional);

static int
vf_is_company_extra(struct cmd_param * param, const char *arg){
	if(param->value && strcmp(param->value, "1") == 0){
		cmd_extra_param(param->cs, &rut);
		if(cmd_haskey(param->cs, "source")) {
			const char *source = cmd_getval(param->cs,"source");
			if(source && bconf_get_int(bconf_vget(trans_bconf, "accounts.validator_addr", param->cs->command->name, NULL), source)) {
				cmd_extra_param(param->cs, &address);
			}
		} else {
			transaction_logprintf(param->cs->ts, D_DEBUG, "source not found");
		}
	}
	return 0;
}


struct validator v_accounts_is_company[] = {
	VALIDATOR_LEN(-1, 1, "ERROR_IS_COMPANY_TOO_LONG"),
	VALIDATOR_LEN(1, -1, "ERROR_IS_COMPANY_TOO_SHORT"),
	VALIDATOR_REGEX("^(0|1)$", REG_EXTENDED, "ERROR_IS_COMPANY_FORMAT_INVALID"),
	VALIDATOR_FUNC(vf_is_company_extra),
	{0}
};
ADD_VALIDATOR(v_accounts_is_company);

struct validator v_is_company[] = {
	VALIDATOR_LEN(-1, 1, "ERROR_IS_COMPANY_TOO_LONG"),
	VALIDATOR_LEN(1, -1, "ERROR_IS_COMPANY_TOO_SHORT"),
	VALIDATOR_REGEX("^(0|1)$", REG_EXTENDED, "ERROR_IS_COMPANY_FORMAT_INVALID"),
	{0}
};
ADD_VALIDATOR(v_is_company);

struct validator v_accounts_accept_conditions[] = {
	VALIDATOR_REGEX("^(1|on)$", REG_EXTENDED, "ERROR_ACCEPT_CONDITIONS"),
	{0}
};
ADD_VALIDATOR(v_accounts_accept_conditions);
struct validator v_pay_rut[] = {
        VALIDATOR_LEN(5, -1, "ERROR_RUT_TOO_SHORT"),
	VALIDATOR_LEN(-1, 10, "ERROR_RUT_TOO_LONG"),
	VALIDATOR_REGEX("^[^0][0-9]{4,10}-[0-9kK]{1}$", REG_EXTENDED, "ERROR_RUT_FORMAT_INVALID"),
	VALIDATOR_FUNC(vf_pay_rut),
	{0}
};
ADD_VALIDATOR(v_pay_rut);

static int
vf_check_doc_type(struct cmd_param *param, const char *arg) {
	if (cmd_getparam(param->cs, "doc_type") && strcmp(param->value, "invoice") == 0){
		cmd_extra_param(param->cs, &rut);
		cmd_extra_param(param->cs, &name);
		cmd_extra_param(param->cs, &lob);
		cmd_extra_param(param->cs, &address);
		cmd_extra_param(param->cs, &region);
		cmd_extra_param(param->cs, &communes);
		cmd_extra_param(param->cs, &contact);
	}
	return 0;
}

struct validator v_doc_type[] = {
	VALIDATOR_REGEX("^(bill|invoice)$", REG_EXTENDED|REG_NOSUB, "ERROR_DOC_TYPE_INVALID"),
	VALIDATOR_FUNC(vf_check_doc_type),
	{0}
};
ADD_VALIDATOR(v_doc_type);

struct validator v_accounts_passwd[] = {
        VALIDATOR_LEN(5, -1, "ERROR_PASSWORD_TOO_SHORT"),
	{0}
};
ADD_VALIDATOR(v_accounts_passwd);

static int
vf_compare_passwds(struct cmd_param *param, const char *arg) {
	if(cmd_haskey(param->cs,"password") && param->value){
		if ( strcmp(cmd_getval(param->cs,"password"),param->value) != 0 ) {
			param->error = "ERROR_PASSWORD_MISMATCH";
			param->cs->status = "TRANS_ERROR";
			return 1;
		}
	}else{
		param->error = "ERROR_PASSWORD_MISSING";
		param->cs->status = "TRANS_ERROR";
		return 1;
	}
	return 0;

}
struct validator v_accounts_passwd_verify[] = {
        VALIDATOR_LEN(5, -1, "ERROR_PASSWORD_VERIFY_TOO_SHORT"),
	VALIDATOR_REGEX("^\\$[0-9]+\\$.{16}[0-9A-Fa-f]{40}$", REG_EXTENDED, "ERROR_VERIFY_PASSWORD_INVALID"),
	VALIDATOR_FUNC(vf_compare_passwds),
	{0}
};
ADD_VALIDATOR(v_accounts_passwd_verify);


static int
vbconf_validate(struct cmd_param *param, const char *bconf_path, const char *bconf_name, int data_type){
	struct bconf_node *prod_root = bconf_get(trans_bconf, bconf_path);
	int i;
	int value_to_compare;

	switch (data_type) {
	case VBCONF_DATA_TYPE_INTEGER:
		value_to_compare = atoi(param->value);
		for (i = 0; i < bconf_count(prod_root); i++) {
			int value = bconf_get_int(bconf_byindex(prod_root, i), bconf_name);
			if(value && value == value_to_compare){
				return 0;
			}
		}
		break;
	case VBCONF_DATA_TYPE_STRING:
		for (i = 0; i < bconf_count(prod_root); i++) {
			const char *value = bconf_get_string(bconf_byindex(prod_root, i), bconf_name);
			if(value && strcmp(value,param->value) == 0){
				return 0;
			}
		}
		break;
	}
	return 1;
}


struct validator v_pack_period[] = {
	VALIDATOR_BCONF_LIST_KEY("packs.expire_time","ERROR_PACK_PERIOD_INVALID"),
	{0}
};
ADD_VALIDATOR(v_pack_period);

struct validator v_pack_slots[] = {
	VALIDATOR_INT(1, 500, "ERROR_SLOTS_INVALID"),
	{0}
};
ADD_VALIDATOR(v_pack_slots);

struct validator v_pack_service_order[] = {
	VALIDATOR_REGEX("^[[:digit:]]{1,20}$", REG_EXTENDED, "ERROR_SERVICE_ORDER_INVALID"),
	{0}
};
ADD_VALIDATOR(v_pack_service_order);

static int
vf_pack_type_enabled(struct cmd_param *param, const char *arg) {
	struct bconf_node *pack_root = bconf_get(trans_bconf, "packs.type");
	int pack_check = bconf_get_int(bconf_vget(trans_bconf, "packs",  NULL),"enabled");
	int i;

	if(pack_check != 1){
		param->error = "ERROR_PACKS_DISABLED";
		param->cs->status = "TRANS_ERROR";
		return 1;
		
	}
	for (i = 0; i < bconf_count(pack_root); i++) {
		const char *name = bconf_get_string(bconf_byindex(pack_root, i), "name");
		if (name && strcmp(name,param->value) == 0){
			if( bconf_get_int(bconf_byindex(pack_root, i), "enabled") != 1){
				char *name_cpy = xstrdup(name);
				transaction_printf(param->cs->ts, "%s:ERROR_PACK_%s_DISABLED\n", param->key, strmodify(name_cpy,toupper));
				param->cs->status = "TRANS_ERROR";
				free(name_cpy);
				return 1;
			}
		}
	}
	return 0;
}

struct validator v_pack_type[] = {
	VALIDATOR_BCONF_LIST_DOT_NAME("packs.type", "ERROR_INVALID_PACK_TYPE"),
	VALIDATOR_FUNC(vf_pack_type_enabled),
	{0}
};
ADD_VALIDATOR(v_pack_type);

struct validator v_stats_pack_type[] = {
	VALIDATOR_REGEX("^(inmosell|inmorent|cars)$", REG_EXTENDED|REG_NOSUB, "ERROR_TYPE_INVALID"),
	{0}
};
ADD_VALIDATOR(v_stats_pack_type);

static int
validate_pro_type_item(struct cmd_param *param, const char *arg, char *value) {
	char *sep = strchr(value, '-');
	if (!sep) {
		param->error = "ERROR_INVALID_PRO_TYPE_FORMAT";
		return 1;
	}
	*sep = '\0';
	const char *protype = value;
	const char *protypeid = sep+1;
	const char *exists = bconf_vget_string(trans_bconf, protype, "type", protypeid, "name", NULL);
	*sep = '-';
	if (!exists) {
		param->error = "ERROR_INVALID_PRO_TYPES";
		return 1;
	}
	return 0;
}

static int
vf_validate_pro_type(struct cmd_param *param, const char *arg) {
	int res = validate_list(param, arg, ",", "ERROR_PRO_TYPE_MISSING", validate_pro_type_item);
	return res;
}

struct validator v_multiple_pro_type[] = {
	VALIDATOR_FUNC(vf_validate_pro_type),
	{0}
};
ADD_VALIDATOR(v_multiple_pro_type);

struct validator v_multiple_pro_action[] = {
	VALIDATOR_REGEX("^[01](,[01])*$", REG_EXTENDED, "ERROR_PRO_ACTIONS_INVALID"),
	{0}
};

ADD_VALIDATOR(v_multiple_pro_action);

static struct validator v_multiple_account_id[] = {
	VALIDATOR_REGEX("^[[:digit:]]+(,[[:digit:]]+)*$", REG_EXTENDED, "ERROR_MULTIPLE_ACCOUNT_ID_INVALID"),
	{0}
};
ADD_VALIDATOR(v_multiple_account_id);

static int
vf_pack_product_id(struct cmd_param *param, const char *arg){
	const char *group = bconf_get_string(bconf_vget(trans_bconf, "payment.product", param->value, NULL), "group");
	transaction_logprintf(param->cs->ts, D_DEBUG, "Product group: %s",group);
	// Validate product group
	if(!group ||(group && strcmp(group,"PACK") != 0)){
		param->error = "ERROR_PACK_PRODUCT_ID_INVALID";
		param->cs->status = "TRANS_ERROR";
		return 1;
	}
	// Get the other params
	const char *type = cmd_getval(param->cs, "type");
	const char *period = cmd_getval(param->cs, "period");
	const char *slots = cmd_getval(param->cs, "slots");
	
	// Init variables
	char *type_cpy = xstrdup((char[2]){(char)param->value[0], '\0'});
	char *period_cpy = xstrdup((char[2]){(char)param->value[1], '\0'});
	const char *last3 = param->value+2;

	// If has type from the other param
	if(type){
		//search in conf
		transaction_logprintf(param->cs->ts, D_DEBUG, "Type cmd: %s", type);
		const char *name = bconf_get_string(bconf_vget(trans_bconf, "packs.type", type_cpy, NULL), "name");
		transaction_logprintf(param->cs->ts, D_DEBUG, "Name bconf: %s", name);
		if (!name || strcmp(name, type) != 0) {
			param->error = "ERROR_PACK_PRODUCT_ID_AND_TYPE_MISSMATCH";
			param->cs->status = "TRANS_ERROR";
			return 1;
		}
	}

	// if is a custom pack then does not validate expiration_date and slots against product_id
	if (param->value) {
		if (strcmp(param->value, "10000") == 0 || strcmp(param->value, "20000") == 0){
			return 0;
		}
	}

	// Validate the period if this come
	if(period){
		if(atoi(period) != atoi(period_cpy)){
			param->error = "ERROR_PACK_PRODUCT_ID_AND_PERIOD_MISSMATCH";
			param->cs->status = "TRANS_ERROR";
			return 1;
		}
	}

	// Validate the slots if this come
	if(slots){
		if(atoi(last3) != atoi(slots)){
			param->error = "ERROR_PACK_PRODUCT_ID_AND_SLOTS_MISSMATCH";
			param->cs->status = "TRANS_ERROR";
			return 1;
		}
	}
	// If the type, period and slots match with the product_id
	return 0;
}

struct validator v_pack_product_id[] = {
	VALIDATOR_BCONF_LIST_KEY("payment.product","ERROR_PACK_PRODUCT_ID_INVALID"),
	VALIDATOR_FUNC(vf_pack_product_id),
	{0}
};
ADD_VALIDATOR(v_pack_product_id);


static int
vf_bconf_product_expire_time(struct cmd_param *param, const char *arg) {
	int result = vbconf_validate(param, "payment.product", "expire_time", VBCONF_DATA_TYPE_STRING);
	if(result == 1){
		param->error = "ERROR_EXPIRE_TIME_INVALID";
		param->cs->status = "TRANS_ERROR";
		return 1;
	}
	return 0;
}

struct validator v_product_expire_time[] = {
	VALIDATOR_FUNC(vf_bconf_product_expire_time),
	{0}
};
ADD_VALIDATOR(v_product_expire_time);


static int
vf_store_address(struct cmd_param *param, const char *arg) {
	if(param->value && strlen(param->value) > 0)
		cmd_extra_param(param->cs, &address);
	return 0;
}


struct validator v_store_address_number[] = {
	VALIDATOR_REGEX("^\\d*$", REG_EXTENDED, "ERROR_ADDRESS_NUMBER_INVALID"),
	VALIDATOR_LEN(-1, 10, "ERROR_ADDRESS_NUMBER_TOO_LONG"),
	VALIDATOR_FUNC(vf_store_address),
	{0}
};
ADD_VALIDATOR(v_store_address_number);


static int
vf_product_existence_db(struct cmd_param *param, const char *arg, const char *product_code, const char *product_code2,
			const char *product_name, int must_exist) {
	struct bpapi ba;
	struct sql_worker *worker = NULL;
	const char *errstr = NULL;
	int ad_found = 0;
	int has_param = 0;
	int ret_val = 1;

	BPAPI_INIT(&ba, NULL, pgsql);
	bpapi_insert(&ba, "ad_id", param->value);
	bpapi_insert(&ba, "product", bconf_get_string(host_bconf, product_code));

	if (product_code2)
		bpapi_insert(&ba, "product2", bconf_get_string(host_bconf, product_code2));

	const char *counter = cmd_getval(param->cs, "counter");
	if (counter)
		bpapi_insert(&ba, "counter", counter);

	worker = sql_blocked_template_query("pgsql.slave", 0, "sql/check_product_already_exist.sql", &ba, param->cs->ts->log_string, &errstr);
	bpapi_free(&ba);

	if (worker) {
		int ad_active = 1;
		while (sql_blocked_next_result(worker, &errstr) == 1) {
			if (worker->next_row(worker)){
				ad_found = 1;
				const char *adparam = worker->get_value_byname(worker, "name");
				has_param = has_param || (adparam != NULL && strcmp(adparam, ""));
				ad_active = atoi(worker->get_value_byname(worker, "is_active"));
				break;
			}
		}

		if (!ad_found) {
			transaction_printf(param->cs->ts, "%s:ERROR_AD_NOT_FOUND\n", param->key);
		} else if (must_exist && !has_param) {
			transaction_printf(param->cs->ts, "%s:ERROR_AD_MISSING_%s\n", param->key, product_name);
		} else if (!must_exist && has_param) {
			transaction_printf(param->cs->ts, "%s:ERROR_%s_ALREADY_EXISTS_FOR_AD\n", param->key, product_name);
		} else if (!ad_active) {
			transaction_printf(param->cs->ts, "%s:ERROR_AD_NOT_ACTIVE\n", param->key);
		} else {
			ret_val = 0;
		}

		if (ret_val)
			param->cs->status = "TRANS_ERROR";
	}

	if (errstr) {
		param->cs->error = "TRANS_DATABASE_ERROR";
		param->cs->message = xstrdup(errstr);
	}

	if (worker)
		sql_worker_put(worker);

	return ret_val;
}

static int
vf_label_already_exist_db(struct cmd_param *param, const char *arg) {
	return vf_product_existence_db(param, arg, "*.payment.product.9.codename", NULL, "LABEL", PRODUCT_MUST_NOT_EXIST);
}

static int
vf_label_doesnt_exist_db(struct cmd_param *param, const char *arg) {
	return vf_product_existence_db(param, arg, "*.payment.product.9.codename", NULL, "LABEL", PRODUCT_MUST_EXIST);
}

static int
vf_if_pack_already_exist_db(struct cmd_param *param, const char *arg) {
	return vf_product_existence_db(param, arg, "*.payment.product.60.codename", NULL, "INSERTING_FEE", PRODUCT_MUST_NOT_EXIST);
}

static int
vf_if_pack_is_ad_disabled(struct cmd_param *param, const char *arg) {
	if (!strcmp(param->cs->status, "TRANS_ERROR"))
		return 0;
	struct bpapi ba;
	struct sql_worker *worker = NULL;
	const char *errstr = NULL;
	BPAPI_INIT(&ba, NULL, pgsql);
	bpapi_insert(&ba, "ad_id", param->value);
	worker = sql_blocked_template_query("pgsql.master", 0, "sql/ad_status.sql", &ba, param->cs->ts->log_string, &errstr);
	bpapi_free(&ba);
	int has_err = 0;
	if (worker && sql_blocked_next_result(worker, &errstr) == 1) {
		if (worker->next_row(worker)) {
			const char *status = worker->get_value_byname(worker, "status");
			has_err = !(status && (!strcmp(status, "disabled") || !strcmp(status, "inactive")));
		}
	}
	if (errstr) {
		param->cs->error = "TRANS_DATABASE_ERROR";
		param->cs->message = xstrdup(errstr);
	} else if (has_err) {
		param->error = "ERROR_AD_ID_IS_NOT_DISABLED";
		param->cs->status = "TRANS_ERROR";
	}
	if (worker)
		sql_worker_put(worker);

	return has_err;
}
struct validator v_if_pack_ad_id[] = {
	VALIDATOR_INT(1, -1, "ERROR_AD_ID_INVALID"),
	VALIDATOR_LEN(1, 15, "ERROR_AD_ID_INVALID"),
	VALIDATOR_REGEX("^[[:digit:]]+$", REG_EXTENDED, "ERROR_AD_ID_INVALID"),
	VALIDATOR_FUNC(vf_if_pack_already_exist_db),
	VALIDATOR_FUNC(vf_if_pack_is_ad_disabled),
	{0}
};
ADD_VALIDATOR(v_if_pack_ad_id);

static int
vf_if_jobs_already_exist_db(struct cmd_param *param, const char *arg) {
	struct bpapi ba;
	struct sql_worker *worker = NULL;
	const char *errstr = NULL;
	BPAPI_INIT(&ba, NULL, pgsql);
	bpapi_insert(&ba, "ad_id", param->value);
	worker = sql_blocked_template_query("pgsql.master", 0, "sql/get_clear_payment_info.sql", &ba, param->cs->ts->log_string, &errstr);
	bpapi_free(&ba);
	int has_err = 1;
	if (worker && sql_blocked_next_result(worker, &errstr) == 1) {
		if (worker->next_row(worker))
			has_err = 0;
	}
	if (errstr) {
		param->cs->error = "TRANS_DATABASE_ERROR";
		param->cs->message = xstrdup(errstr);
	} else if (has_err){
		param->error = "ERROR_AD_ID_INVALID_FOR_IF";
		param->cs->status = "TRANS_ERROR";
	}
	if (worker)
		sql_worker_put(worker);

	return has_err;
}

struct validator v_if_jobs_ad_id[] = {
	VALIDATOR_INT(1, -1, "ERROR_AD_ID_INVALID"),
	VALIDATOR_LEN(1, 15, "ERROR_AD_ID_INVALID"),
	VALIDATOR_REGEX("^[[:digit:]]+$", REG_EXTENDED, "ERROR_AD_ID_INVALID"),
	VALIDATOR_FUNC(vf_if_jobs_already_exist_db),
	{0}
};
ADD_VALIDATOR(v_if_jobs_ad_id);

static int
vf_weekly_already_exist_db(struct cmd_param *param, const char *arg) {
	const char *daily_bump = cmd_getval(param->cs, "daily_bump");
	const char *is_upselling = cmd_getval(param->cs, "is_upselling");

	if (daily_bump) {
		if (is_upselling) {
			return vf_product_existence_db(param, arg, "*.payment.product.103.codename", "*.payment.product.8.codename",
											"DAILY", PRODUCT_MUST_NOT_EXIST);
		} else {
			return vf_product_existence_db(param, arg, "*.payment.product.8.codename", "*.payment.product.103.codename",
											"DAILY", PRODUCT_MUST_NOT_EXIST);
		}
	} else {
		if (is_upselling) {
			return vf_product_existence_db(param, arg, "*.payment.product.101.codename", "*.payment.product.2.codename",
											"WEEKLY", PRODUCT_MUST_NOT_EXIST);
		} else {
			return vf_product_existence_db(param, arg, "*.payment.product.2.codename", "*.payment.product.101.codename",
											"WEEKLY", PRODUCT_MUST_NOT_EXIST);
		}
	}
}

struct validator v_weekly_ad_id[] = {
	VALIDATOR_INT(1, -1, "ERROR_AD_ID_INVALID"),
	VALIDATOR_LEN(1, 15, "ERROR_AD_ID_INVALID"),
	VALIDATOR_REGEX("^[[:digit:]]+$", REG_EXTENDED, "ERROR_AD_ID_INVALID"),
	VALIDATOR_FUNC(vf_weekly_already_exist_db),
	{0}
};

ADD_VALIDATOR(v_weekly_ad_id);

struct validator v_gallery_ad_id[] = {
	VALIDATOR_INT(1, -1, "ERROR_AD_ID_INVALID"),
	VALIDATOR_LEN(1, 15, "ERROR_AD_ID_INVALID"),
	VALIDATOR_REGEX("^[[:digit:]]+$", REG_EXTENDED, "ERROR_AD_ID_INVALID"),
	{0}
};

ADD_VALIDATOR(v_gallery_ad_id);

struct validator v_label_ad_id[] = {
	VALIDATOR_INT(1, -1, "ERROR_AD_ID_INVALID"),
	VALIDATOR_LEN(1, 15, "ERROR_AD_ID_INVALID"),
	VALIDATOR_REGEX("^[[:digit:]]+$", REG_EXTENDED, "ERROR_AD_ID_INVALID"),
	VALIDATOR_FUNC(vf_label_already_exist_db),
	{0}
};

ADD_VALIDATOR(v_label_ad_id);

struct validator v_no_label_ad_id[] = {
	VALIDATOR_INT(1, -1, "ERROR_AD_ID_INVALID"),
	VALIDATOR_LEN(1, 15, "ERROR_AD_ID_INVALID"),
	VALIDATOR_REGEX("^[[:digit:]]+$", REG_EXTENDED, "ERROR_AD_ID_INVALID"),
	VALIDATOR_FUNC(vf_label_doesnt_exist_db),
	{0}
};

ADD_VALIDATOR(v_no_label_ad_id);

struct validator v_whole_word[] = {
	VALIDATOR_REGEX("^(-1|[012])$", REG_EXTENDED, "ERROR_WHOLE_WORD_INVALID"),
	{0}
};

ADD_VALIDATOR(v_whole_word);

struct validator v_label_type[] = {
	VALIDATOR_BCONF_LIST("product_config.label.options", "ERROR_INVALID_LABEL_TYPE"),
	{0}
};
ADD_VALIDATOR(v_label_type);

static char *vf_get_email_from_session_hash(const char *hash){
	char *rkey = NULL;
	const char *prefix = bconf_value(bconf_vget(trans_bconf, "accounts", "session_redis_prefix", NULL));
	xasprintf(&rkey, "%s_session_%s", prefix, hash);
	struct fd_pool_conn *conn = redis_sock_conn(redis_session_pool.pool, "master");
	char *email = redis_sock_hget(conn, rkey, "email");
	fd_pool_free_conn(conn);
	return email;
}

static int
vf_check_hash_in_redis_session (struct cmd_param *param, const char *arg) {
	char *email = vf_get_email_from_session_hash(param->value);
	if (!email) {
		param->error = "ERROR_ACCOUNT_IS_NOT_LOGGED_IN";
		param->cs->status = "TRANS_ERROR";
		return 1;
	}
	return 0;
}

struct validator v_active_session_hash[] = {
	VALIDATOR_FUNC(vf_check_hash_in_redis_session),
	{0}
};
ADD_VALIDATOR(v_active_session_hash);

static int
vf_check_if_ad_is_external (struct cmd_param *param, const char *arg) {
	const char *is_from_external_partner = NULL;
	struct bpapi ba;
	struct sql_worker *worker;
	const char *errstr = NULL;

	BPAPI_INIT(&ba, NULL, pgsql);
	bpapi_insert(&ba, "list_id", param->value);
	worker = sql_blocked_template_query("pgsql.slave", 0, "sql/call_check_if_list_id_is_from_external_partner.sql", &ba, param->cs->ts->log_string, &errstr);
	bpapi_free(&ba);
	if (worker) {
		while (sql_blocked_next_result(worker, &errstr) == 1) {
			if (worker->next_row(worker)){
				is_from_external_partner = worker->get_value(worker,0);
				if (is_from_external_partner[0] == 't') {
					sql_worker_put(worker);
					return 0;
				}
			}
		}
	}

	if (errstr) {
		param->cs->error = "TRANS_DATABASE_ERROR";
		param->cs->message = xstrdup(errstr);
	}

	if (worker)
		sql_worker_put(worker);

	return 0;
}

static int
vf_check_if_list_id_belongs_to_active_session (struct cmd_param *param, const char *arg) {
	const char *is_list_id_from_session = NULL;
	struct bpapi ba;
	struct sql_worker *worker;
	const char *errstr = NULL;

	char *email = vf_get_email_from_session_hash(cmd_getval(param->cs, "session_hash"));
	if (!email) {
		param->error = "ERROR_ACCOUNT_IS_NOT_LOGGED_IN";
		param->cs->status = "TRANS_ERROR";
		return 1;
	}

	// check if the list_id is related with the email in the db
	BPAPI_INIT(&ba, NULL, pgsql);
	bpapi_insert(&ba, "email", email);
	bpapi_insert(&ba, "list_id", param->value);
	worker = sql_blocked_template_query("pgsql.slave", 0, "sql/call_check_list_id_from_email.sql", &ba, param->cs->ts->log_string, &errstr);
	bpapi_free(&ba);

	if (worker) {
		while (sql_blocked_next_result(worker, &errstr) == 1) {
			if (worker->next_row(worker)){
				is_list_id_from_session = worker->get_value(worker,0);
				if (is_list_id_from_session[0] != 't'){
					param->error = "ERROR_SESSION_AND_LIST_ID_DOES_NOT_MATCH";
					param->cs->status = "TRANS_ERROR";
					sql_worker_put(worker);
					return 1;
				}
				break;
			}
		}
	}

	if (errstr) {
		param->cs->error = "TRANS_DATABASE_ERROR";
		param->cs->message = xstrdup(errstr);
	}

	if (worker)
		sql_worker_put(worker);

	return 0;
}

struct validator v_list_id_of_active_session[] = {
	VALIDATOR_FUNC(vf_check_if_ad_is_external),
	VALIDATOR_FUNC(vf_check_if_list_id_belongs_to_active_session),
	{0}
};
ADD_VALIDATOR(v_list_id_of_active_session);

struct validator v_social_partner[] = {
	VALIDATOR_REGEX("^(facebook_id|google_id|apple_id)$", REG_EXTENDED|REG_NOSUB, "ERROR_SOCIAL_PARTNER_INVALID"),
	{0}
};
ADD_VALIDATOR(v_social_partner);

struct validator v_gender_account[] = {
	VALIDATOR_REGEX("^(male|female)$", REG_EXTENDED, "ERROR_INVALID_GENDER"),
	{0}
};
ADD_VALIDATOR(v_gender_account);

struct validator v_source_account[] = {
	VALIDATOR_BCONF_LIST("accounts.source", "ERROR_INVALID_ACCOUNT_SOURCE"),
	{0}
};
ADD_VALIDATOR(v_source_account);

struct validator v_frequency[] = {
	VALIDATOR_BCONF_LIST_KEY("autobump.frequency", "ERROR_FREQUENCY_INVALID"),
	{0}
};
ADD_VALIDATOR(v_frequency);

struct validator v_autobump_max_days[] = {
	VALIDATOR_INT(1, 93, "ERROR_NUM_DAYS_INVALID"),
	{0}
};
ADD_VALIDATOR(v_autobump_max_days);

static time_t
str_to_time(const char *date) {
	struct tm tm = {0};
	strptime(date, "%Y-%m-%d", &tm);
	return mktime(&tm);
}

static int
vf_date_greater_than(struct cmd_param *param, const char *arg) {
	const char* start_date = cmd_getval(param->cs, arg) ? cmd_getval(param->cs, arg) : NULL;
	if (start_date == NULL) {
		param->error = "ERROR_UNAVAILABLE_START_DATE";
		param->cs->status = "TRANS_ERROR";
		return 1;
	}

	time_t start = str_to_time(start_date);
	time_t end = str_to_time(param->value);
	if (end - start < 0) {
		param->error = "ERROR_END_DATE_EARLIER";
		param->cs->status = "TRANS_ERROR";
		return 1;
	}

	return 0;
}

struct validator v_date_greater_than[] = {
	VALIDATOR_REGEX("^[[:digit:]]{4}-[[:digit:]]{2}-[[:digit:]]{2}$", REG_EXTENDED, "ERROR_DATE_INVALID"),
	VALIDATOR_FUNC(vf_date_greater_than),
	{0}
};
ADD_VALIDATOR(v_date_greater_than);

static struct c_param multi_category = {"category", P_MULTI, "v_new_category"};

/*
* Looks for repeated categories on trans parameters, by searching param->cs
* structure in a for loop, to make sure a single category is included only
* once in a ppage.
*/
static int
vf_category_multi(struct cmd_param *param, const char *arg) {
	struct cmd_param *ptr;
	int count = 0;
	for (ptr = cmd_getparam(param->cs, "category"); ptr; ptr = cmd_nextparam(param->cs, ptr)) {
		if (!strcmp(ptr->value, param->value))
			count++;
		if (count > 1) {
			param->error = "ERROR_REPEATED_CATEGORY";
			param->cs->status = "TRANS_ERROR";
			return 1;
		}
	}
	cmd_extra_param(param->cs, &multi_category);
	return 0;
}

struct validator v_category_multi[] = {
	VALIDATOR_FUNC(vf_category_multi),
	{0}
};
ADD_VALIDATOR(v_category_multi);

struct validator v_mandatory_check[] = {
	VALIDATOR_REGEX("^[[:digit:]].*$", REG_EXTENDED, "ERROR_PRICE_MISSING"),
	VALIDATOR_NREGEX("^0+([.,]0*)?$", REG_EXTENDED, "ERROR_PRICE_INVALID"),
	VALIDATOR_FUNC(vf_currency_check),
	{0}
};
ADD_VALIDATOR(v_mandatory_check);

static int
vf_price_length(struct cmd_param *param, const char *arg) {
	const char *price = param->value;
	const char *category = cmd_haskey(param->cs, "category") ? cmd_getval(param->cs, "category") : NULL;
	const char *currency = cmd_haskey(param->cs, "currency") ? cmd_getval(param->cs, "currency") : "peso";
	if (!category || param->error)
		return 0;

	unsigned int  min_length = bconf_vget_int(trans_bconf, "cat", category, "price", "min_length", currency, NULL);
	if (min_length && (strlen(price) < min_length)) {
		param->error = "ERROR_PRICE_LENGTH";
		param->cs->status = "TRANS_ERROR";
	}
	return 0;
}

struct validator v_price_length_per_cat[] = {
	VALIDATOR_FUNC(vf_price_length),
	{0}
};
ADD_VALIDATOR(v_price_length_per_cat);

struct validator v_multiple_emails[] = {
	VALIDATOR_LEN(5, -1, "ERROR_EMAIL_TOO_SHORT"),
	VALIDATOR_REGEX("^(([[:alnum:]_+-]+(\\.[[:alnum:]_+-]*)*@([[:alnum:]_+-]+\\.)+[[:alpha:]]{2,4})[,]{0,1})+$",
			REG_EXTENDED|REG_NOSUB|REG_ICASE, "ERROR_INVALID_MULTIPLE_EMAIL"),
	{0}
};
ADD_VALIDATOR(v_multiple_emails);

struct validator v_archive_type[] = {
        VALIDATOR_BCONF_LIST("archive_type", "ERROR_INVALID_ARCHIVE_TYPE"),
        {0}
};
ADD_VALIDATOR(v_archive_type);
