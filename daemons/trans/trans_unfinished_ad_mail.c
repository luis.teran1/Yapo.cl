#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdarg.h>
#include <unistd.h>
#include <sys/socket.h>

#include "command.h"
#include "trans.h"
#include "util.h"
#include "ev_popen.h"
#include <ctemplates.h>
#include "trans_mail.h"
#include "validators.h"
#include "bconf.h"
#include "strl.h"

extern struct bconf_node *bconf_root;

static struct c_param parameters[] = {
	{"unf_id",	P_REQUIRED,	"v_none"},
	{"adname",	P_REQUIRED,	"v_none"},
	{"from",	0,	"v_none"},
	{"to",		P_REQUIRED,	"v_none"},
	{"ad_subject",	0,	"v_none"},
	{"session",	P_REQUIRED, "v_none"},
	{"img",	0,	"v_none"},
	{NULL, 0}
};

static void
mail_done(struct mail_data *md, void *arg, int status) {
	struct cmd *cs = arg;

	if (status != 0) {
                cs->status = "TRANS_MAIL_ERROR";
                cmd_done(cs);	
		return;
	}

	cmd_done(cs);
}

static void
mail(struct cmd *cs) {
	struct mail_data *md;
	md = zmalloc(sizeof (*md));
	md->command = cs;
	md->callback = mail_done;
	md->log_string = cs->ts->log_string;

	mail_insert_param(md, "adname", cmd_getval(cs, "adname"));
	mail_insert_param(md, "from", cmd_getval(cs, "from"));
	mail_insert_param(md, "to", cmd_getval(cs, "to"));
	mail_insert_param(md, "ad_subject", cmd_getval(cs, "ad_subject"));
	mail_insert_param(md, "session", cmd_getval(cs, "session"));
	mail_insert_param(md, "unf_id", cmd_getval(cs, "unf_id"));
	mail_insert_param(md, "img", cmd_getval(cs, "img"));

	mail_send(md, "mail/unfinished_ad.mime",cmd_getval(cs, "lang"));
}

ADD_COMMAND(unfinished_ad_mail, NULL, mail, parameters, NULL);
