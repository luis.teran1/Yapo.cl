#include "geoip_backend.h"
#include "trans.h"

struct geoip_data {
	GeoIP *obj;
	char *dbfile;
	int required;
} geoip;

static void
open_geoip_db(void *obj) {
	if (geoip.dbfile) {
		geoip.obj = GeoIP_open(geoip.dbfile, 0);
	} else 
		geoip.obj = GeoIP_open_type(GEOIP_COUNTRY_EDITION, 0 /* CACHE OPTIONS ARE NOT THREAD SAFE */);

	if (!geoip.obj) {
		if (geoip.required)
			xerrx(1, "ERROR: GeoIP object could not be created, but is required. Aborting.");
		else
			xwarnx("Warning: GeoIP object could not be created.");
	} else {
		DPRINTF(D_DEBUG, ("[geoip_backend] GeoIP object acquired."));
	}
}

GeoIP*
get_geoip(void) {
	DPRINTF(D_DEBUG, ("[geoip_backend] GeoIP object returned"));
	return geoip.obj;
}

/* Interface */

static void
init_geo_info(char *check_only) {
	if (check_only && !strstrptrs(check_only, "geoip", NULL, ","))
		return;

	open_geoip_db(NULL);

	/* backend_initfunc_register("geoip_obj", get_geoip); */
	reload_register(open_geoip_db, NULL);
}

static void
deinit_geo_info(void) {
	if (geoip.obj) {
		GeoIP_delete(geoip.obj);
		geoip.obj = NULL;
	}
	free(geoip.dbfile);
	geoip.dbfile = NULL;
}

static int
config_geo_info(struct bconf_node *conf) {

	deinit_geo_info();

	geoip.required = bconf_get_int_default(conf, "geoip.db.required", 0);
	xwarnx("[geoip_backend] GeoIP object availability is %s", geoip.required ? "mandatory" : "optional");

	const char* dbpath = bconf_get_string(conf, "geoip.db.directory"); /* /usr/local/share/GeoIP */

	if (dbpath) {
		xasprintf(&geoip.dbfile, "%s/GeoIP.dat", dbpath);
		DPRINTF(D_DEBUG, ("[geoip_backend] Configured GeoIP database directory = %s", geoip.dbfile));
	} else {
		DPRINTF(D_DEBUG, ("[geoip_backend] Will use default GeoIP database location"));
	}

	return 0;
}

ADD_BACKEND(geoip, config_geo_info, init_geo_info, NULL, deinit_geo_info, NULL);

