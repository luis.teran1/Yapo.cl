#ifndef RABBITMQ_CONF_H
#define RABBITMQ_CONF_H

#define MAX_VHOSTS			10
#define MAX_EXCHANGES		20
#define MAX_QUEUES			20
#define MAX_BINDINGS		50
#define MAX_VHOST_MQ_CONNS	20

#include "trans.h"

struct rabbitmq_exchange {
	const char *name;
	const char *type;
	int passive;
	int durable;
};

struct rabbitmq_queue {
	const char *name;
	int passive;
	int durable;
	int exclusive;
	int auto_delete;
};

struct rabbitmq_binding {
	const char *exchange;
	const char *queue;
	const char *key;
};

struct rabbitmq_vhost {
	const char *host;
	int port;
	int connections;
	const char *name;
	const char *path;
	const char *user;
	const char *pass;

	int n_exchanges;
	int n_queues;
	int n_bindings;

	struct rabbitmq_exchange *exchange[MAX_EXCHANGES];
	struct rabbitmq_queue *queue[MAX_QUEUES];
	struct rabbitmq_binding *binding[MAX_BINDINGS];
};

struct rabbitmq_conf {
	int n_vhosts;
	struct rabbitmq_vhost *vhost[MAX_VHOSTS];
};

struct rabbitmq_conf *rabbitmq_conf_load(struct bconf_node *conf);
void rabbitmq_conf_free(struct rabbitmq_conf *conf);

# endif
