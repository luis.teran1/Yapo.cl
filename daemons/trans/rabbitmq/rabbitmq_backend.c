#include <amqp.h>
#include <amqp_tcp_socket.h>

#include "trans.h"
#include "rabbitmq_backend.h"
#include "rabbitmq_conf.h"
#include "rabbitmq_conn_pool.h"

/* Available globals */

struct mqueue_vhost_conn {
	struct rabbitmq_vhost *vhost;
	mqueue_conn_pool conn_pool;
};

int mqueue_enabled;
int n_mqueue_connections;
struct rabbitmq_conf *rmq_conf;
struct mqueue_vhost_conn *mqueue_connections[MAX_VHOSTS];

static amqp_connection_state_t rabbitmq_connect(struct rabbitmq_vhost *vhost, int channel);
static int rabbitmq_setup_conn(struct rabbitmq_vhost *vhost, amqp_connection_state_t conn, int channel);

static void
rmq_set(rmq_conn *rmq, int vhost_id, amqp_connection_state_t conn, int channel)
{
	rmq->vhost_id = vhost_id;
	rmq->conn = conn;
	rmq->channel = channel;
}

static amqp_connection_state_t
try_to_replenish(struct mqueue_vhost_conn *vhost_conn)
{
	int channel = 1;
	amqp_connection_state_t conn = NULL;
	mqueue_conn_pool pool = vhost_conn->conn_pool;
	struct rabbitmq_vhost *vhost = vhost_conn->vhost;

	int can = mqueue_conn_pool_can_replenish(pool);

	if (can) {
		conn = rabbitmq_connect(vhost, channel);
		if (!conn) {
			DPRINTF(D_ERROR, ("[rabbitmq_backend] Connection to vhost [%s] failed. RabbitMQ may be UNAVAILABLE", vhost->name));
			conn = NULL;
		}
		if (conn) {
			int status = rabbitmq_setup_conn(vhost, conn, channel);
			if (status < 0) {
				amqp_channel_close(conn, channel, AMQP_REPLY_SUCCESS);
				amqp_connection_close(conn, AMQP_REPLY_SUCCESS);
				amqp_destroy_connection(conn);
				conn = NULL;
			}
		}
		if (conn)
			mqueue_conn_pool_replenish_success(pool);
		else 
			mqueue_conn_pool_replenish_failed(pool);
	}

	return conn;
}	

rmq_conn *
get_rmq_conn(const char* virtual_host, rmq_conn *rmq)
{
	int i, channel = 1;
	for ( i = 0 ; i < n_mqueue_connections ; ++i ) {
		struct mqueue_vhost_conn *vhost_conn = mqueue_connections[i];
		if (!strcmp(virtual_host, vhost_conn->vhost->name) ) {
			amqp_connection_state_t conn = mqueue_conn_pool_get(vhost_conn->conn_pool);
			if (!conn) {
				conn = try_to_replenish(vhost_conn);
				if (!conn)
					return NULL;
			}
			rmq_set(rmq, i, conn, channel);
			return rmq;
		}
	}
	return NULL;
}

void
drop_rmq_conn(rmq_conn *rmq)
{
	struct mqueue_vhost_conn *vhost_conn = mqueue_connections[rmq->vhost_id];
	mqueue_conn_pool_dropped_conn(vhost_conn->conn_pool);
}

void
return_rmq_conn(rmq_conn *rmq)
{
	struct mqueue_vhost_conn *vhost_conn = mqueue_connections[rmq->vhost_id];
	mqueue_conn_pool_return(vhost_conn->conn_pool, rmq->conn);
}


static amqp_connection_state_t
rabbitmq_connect(struct rabbitmq_vhost *vhost, int channel)
{
	amqp_connection_state_t conn;
	conn = amqp_new_connection();

	DPRINTF(D_DEBUG, ("[rabbitmq_backend] connect: vhost [%s]", vhost->name));

	if (!conn) {
		DPRINTF(D_ERROR, ("[rabbitmq_backend] connect: error creating connection"));
		return NULL;
	}

	amqp_socket_t *socket = NULL;
	socket = amqp_tcp_socket_new(conn);
	if (!socket) {
		DPRINTF(D_ERROR, ("[rabbitmq_backend] connect: error creating socket"));
		amqp_connection_close(conn, AMQP_INTERNAL_ERROR);
		amqp_destroy_connection(conn);
		return NULL;
	}

	int status = amqp_socket_open(socket, vhost->host, vhost->port);
	if (status) {
		DPRINTF(D_ERROR, ("[rabbitmq_backend] connect: error opening socket (%s:%d)", vhost->host, vhost->port));
		amqp_connection_close(conn, AMQP_INTERNAL_ERROR);
		amqp_destroy_connection(conn);
		return NULL;
	}

	amqp_rpc_reply_t reply;
	reply = amqp_login(conn, vhost->path, AMQP_DEFAULT_MAX_CHANNELS, AMQP_DEFAULT_FRAME_SIZE, AMQP_DEFAULT_HEARTBEAT, AMQP_SASL_METHOD_PLAIN, vhost->user, vhost->pass);
	if (reply.reply_type != AMQP_RESPONSE_NORMAL) {
		DPRINTF(D_ERROR, ("[rabbitmq_backend] connect: error logging in (%s@%s[%s])", vhost->user, vhost->name, vhost->path));
		amqp_connection_close(conn, AMQP_INTERNAL_ERROR);
		amqp_destroy_connection(conn);
		return NULL;
	}

	amqp_channel_open(conn, 1);
	reply = amqp_get_rpc_reply(conn);
	if (reply.reply_type != AMQP_RESPONSE_NORMAL) {
		DPRINTF(D_ERROR, ("[rabbitmq_backend] connect: error opening channel %d", channel));
		amqp_connection_close(conn, AMQP_INTERNAL_ERROR);
		amqp_destroy_connection(conn);
		return NULL;
	}

	return conn;
}

static int
rabbitmq_setup_conn(struct rabbitmq_vhost *vhost, amqp_connection_state_t conn, int channel)
{
    int i;
    amqp_rpc_reply_t reply;

	DPRINTF(D_INFO, ("[rabbitmq_backend] setup: vhost [%s]", vhost->name));

	for ( i = 0 ; i < vhost->n_exchanges ; ++i ) {
		struct rabbitmq_exchange *x = vhost->exchange[i];
		amqp_exchange_declare(conn, channel, amqp_cstring_bytes(x->name), amqp_cstring_bytes(x->type), x->passive, x->durable, amqp_empty_table);
		reply = amqp_get_rpc_reply(conn);
		if (reply.reply_type != AMQP_RESPONSE_NORMAL) {
			DPRINTF(D_ERROR, ("[rabbitmq_backend] setup: declaring exchange [%s] failed", x->name));
			return -1;
		}
	}

	for ( i = 0 ; i < vhost->n_queues ; ++i ) {
		struct rabbitmq_queue *q = vhost->queue[i];
		amqp_queue_declare(conn, channel, amqp_cstring_bytes(q->name), q->passive, q->durable, q->exclusive, q->auto_delete, amqp_empty_table);
		reply = amqp_get_rpc_reply(conn);
		if (reply.reply_type != AMQP_RESPONSE_NORMAL) {
			DPRINTF(D_ERROR, ("[rabbitmq_backend] setup: declaring queue [%s] failed", q->name));
			return -1;
		}
	}

	for ( i = 0 ; i < vhost->n_bindings ; ++i ) {
		struct rabbitmq_binding *b = vhost->binding[i];
		amqp_queue_bind(conn, channel, amqp_cstring_bytes(b->queue), amqp_cstring_bytes(b->exchange), amqp_cstring_bytes(b->key), amqp_empty_table);
		reply = amqp_get_rpc_reply(conn);
		if (reply.reply_type != AMQP_RESPONSE_NORMAL) {
			DPRINTF(D_ERROR, ("[rabbitmq_backend] setup: declaring binding [%s -[%s]-> %s] failed", b->exchange, b->key, b->queue));
			return -1;
		}
	}

    return 0;
}

static struct mqueue_vhost_conn *
mqueue_vhost_conn(struct rabbitmq_vhost *vhost, mqueue_conn_pool cp) {
	struct mqueue_vhost_conn *vhost_conn = zmalloc(sizeof(struct mqueue_vhost_conn));
	vhost_conn->vhost = vhost;
	vhost_conn->conn_pool = cp;
	return vhost_conn;
}

static int
rabbitmq_setup_virtual_hosts(struct bconf_node *conf) {
	int i, j, channel = 1;

	rmq_conf = rabbitmq_conf_load(conf);
	if (!rmq_conf) {
		DPRINTF(D_ERROR, ("[rabbitmq_backend] (FATAL) No config loaded! RabbitMQ UNAVAILABLE"));
		return -1;
	}

	for ( i = 0 ; i < rmq_conf->n_vhosts ; i++ ) {
		struct rabbitmq_vhost *vhost = rmq_conf->vhost[i];
		DPRINTF(D_INFO, ("[rabbitmq_backend] Setting up virtual host '%s'", vhost->name));

		mqueue_conn_pool cp = mqueue_conn_pool_new();
		if (!cp) {
			DPRINTF(D_ERROR, ("[rabbitmq_backend] (FATAL) Couldn't initialize connection pool! RabbitMQ UNAVAILABLE"));
			return -1;
		}

		mqueue_conn_pool_set_required_conns(cp, vhost->connections);
		for ( j = 0 ; j < vhost->connections ; ++j ) {
			amqp_connection_state_t conn = rabbitmq_connect(vhost, channel);
			if (!conn) {
				DPRINTF(D_ERROR, ("[rabbitmq_backend] (FATAL) Connection %d failed. vhost %s! RabbitMQ UNAVAILABLE", i, vhost->name));
				return -1;
			}
			int status = rabbitmq_setup_conn(vhost, conn, channel);
			if (status < 0) {
				amqp_channel_close(conn, channel, AMQP_REPLY_SUCCESS);
				amqp_connection_close(conn, AMQP_REPLY_SUCCESS);
				amqp_destroy_connection(conn);
				return -1;
			}
			mqueue_conn_pool_add(cp, conn);
		}

		mqueue_connections[i] = mqueue_vhost_conn(vhost, cp);
	}
	n_mqueue_connections = i;
	return 0;
}

static int
rabbitmq_close(amqp_connection_state_t conn, int channel)
{
	amqp_rpc_reply_t reply;
	reply = amqp_channel_close(conn, channel, AMQP_REPLY_SUCCESS);
	if (reply.reply_type != AMQP_RESPONSE_NORMAL) {
		DPRINTF(D_ERROR, ("[rabbitmq_backend] close: error closing channel %d", channel));
		return -1;
	}

	reply = amqp_connection_close(conn, AMQP_REPLY_SUCCESS);
	if (reply.reply_type != AMQP_RESPONSE_NORMAL) {
		DPRINTF(D_ERROR, ("[rabbitmq_backend] close: error closing connection"));
		return -1;
	}

	int status = amqp_destroy_connection(conn);
	if (status < 0) {
		DPRINTF(D_ERROR, ("[rabbitmq_backend] close: destroying connection"));
		return -1;
	}

	return 0;
}

static void
rabbitmq_close_all(void)
{
	int i, channel = 1;
	amqp_connection_state_t conn;

	for ( i = 0 ; i < n_mqueue_connections ; ++i ) {
		struct mqueue_vhost_conn *vhost = mqueue_connections[i];
		while ( (conn = mqueue_conn_pool_del(vhost->conn_pool)) )
			rabbitmq_close(conn, channel);
		mqueue_conn_pool_destroy(vhost->conn_pool);
		free(vhost);
	}
}

/* Backend interface */

static void
init_rabbitmq_backend(char *check_only) {
	if (check_only && !strstrptrs(check_only, "rabbitmq_backend", NULL, ","))
		return;

	if (!mqueue_enabled)
		return;

	DPRINTF(D_DEBUG, ("[rabbitmq_backend] Initializing"));
}

static void
init_done_rabbitmq_backend(void) {
	if (!mqueue_enabled)
		return;

	DPRINTF(D_DEBUG, ("[rabbitmq_backend] Post-init"));
}

static void
deinit_rabbitmq_backend(void) {
	DPRINTF(D_DEBUG, ("[rabbitmq_backend] Deinitializing"));
	rabbitmq_close_all();
	rabbitmq_conf_free(rmq_conf);
}

static int
config_rabbitmq_backend(struct bconf_node *conf) {
	DPRINTF(D_DEBUG, ("[rabbitmq_backend] Starting configuration"));
	deinit_rabbitmq_backend();

	mqueue_enabled = bconf_get_int_default(conf, "rabbitmq.enabled", 0);
	if (mqueue_enabled > 0) {
		return rabbitmq_setup_virtual_hosts(conf);
	} else {
		DPRINTF(D_INFO, ("[rabbitmq_backend] Message queue disabled, ignoring all configuration"));
		return 0;
	}
	/* if (configuration failed)
		return -1; */
}

static void
transinfo_rabbitmq_backend(struct transaction *ts) {
	if (mqueue_enabled > 0) {
		transaction_printf(ts, "rabbitmq_backend information: enabled\n");
	} else {
		transaction_printf(ts, "rabbitmq_backend information: disabled\n");
	}
}

ADD_BACKEND(rabbitmq_backend, config_rabbitmq_backend, init_rabbitmq_backend, init_done_rabbitmq_backend, deinit_rabbitmq_backend, transinfo_rabbitmq_backend);
