#ifndef RABBITMQ_H
#define RABBITMQ_H

#include <bconf.h>
#include "factory.h"

int rabbitmq_send(struct cmd *cs, struct bpapi *ba);

#endif	/* RABBITMQ_H */
