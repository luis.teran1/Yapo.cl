#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

#include "memalloc_functions.h"
#include "util.h"

#include "rabbitmq_conf.h"
#include "rabbitmq_conn_pool.h"

/*
 * This beautiful connection pool works by adding some connections to be tracked
 * and giving them to requesters. Once a requester gets a connection, it is
 * responsible for giving it back. The only exception is when a network error is
 * detected, case in which the requester MUST inform that the connections has
 * been dropped.
 *
 * Connections are stored in an array governed by 3 variables:
 *  - required_conns: Is the connection soft limit. It can be set to any value
 *    up to MAX_VHOST_MQ_CONNS.
 *  - conns: The actual number of connections added to the pool. This number
 *    won't change when connections are requested, only will decrease when
 *    connections are dropped, and increase when a replacement connection is
 *    added back. This value will never exceed required_conns.
 *  - next_to_take: Is the first connection available for requesters.
 *
 * The array is partitioned by these variables into 3 sets:
 *  - in use:    range[0, next_to_take)
 *  - available: range[next_to_take, conns)
 *  - lost:      range[conns, required_conns)
 *
 * Whenever the caller detects a broken connection and notifies the pool about
 * it. The pool will respond by cleaning itself up and removing all connections
 * so new calls can populate it back with new connections.
 */

struct mqueue_conn_pool {
	pthread_mutex_t mutex;
	pthread_cond_t not_empty;
	int required_conns;
	int connecting;
	int conns;
	int next_to_take;
	amqp_connection_state_t conn[MAX_VHOST_MQ_CONNS];
}; 

struct mqueue_conn_pool *
mqueue_conn_pool_new()
{
	struct mqueue_conn_pool *cp = zmalloc(sizeof(struct mqueue_conn_pool));
	pthread_mutex_init(&cp->mutex, NULL);
	pthread_cond_init(&cp->not_empty, NULL);
	cp->required_conns = 0;
	cp->connecting = 0;
	cp->conns = 0;
	cp->next_to_take = 0;
	return cp;
}

void
mqueue_conn_pool_destroy(struct mqueue_conn_pool *cp)
{
	pthread_cond_destroy(&cp->not_empty);
	pthread_mutex_destroy(&cp->mutex);
	free(cp);
}

static int
mqueue_conn_pool_is_empty(struct mqueue_conn_pool *cp)
{
	return cp->next_to_take == cp->conns;
}

int
mqueue_conn_pool_available(mqueue_conn_pool cp)
{
	return cp->conns - cp->next_to_take;
}

int
mqueue_conn_pool_in_use(mqueue_conn_pool cp)
{
	return cp->next_to_take;
}

int
mqueue_conn_pool_lost(mqueue_conn_pool cp)
{
	return cp->required_conns - cp->conns;
}

int
mqueue_conn_pool_conns(mqueue_conn_pool cp)
{
	return cp->conns;
}

int
mqueue_conn_pool_required_conns(mqueue_conn_pool cp)
{
	return cp->required_conns;
}

int
mqueue_conn_pool_connecting(mqueue_conn_pool cp)
{
	return cp->connecting;
}

void 
mqueue_conn_pool_set_required_conns(mqueue_conn_pool cp, int required)
{
	if (required > MAX_VHOST_MQ_CONNS) {
		DPRINTF(D_DEBUG, ("mqueue_conn_pool_set_required_conns: setting required conns too high, truncating. (max = %d)", MAX_VHOST_MQ_CONNS));
		required = MAX_VHOST_MQ_CONNS;
	}
	cp->required_conns = required;
}

int
mqueue_conn_pool_add(mqueue_conn_pool cp, amqp_connection_state_t conn)
{
	if (cp->conns >= cp->required_conns) {
		DPRINTF(D_DEBUG, ("mqueue_conn_pool_add: too many connections, discarding. (max required = %d, current = %d)", cp->required_conns, cp->conns));
		return -1;
	}

	cp->conn[cp->conns++] = conn;
	DPRINTF(D_DEBUG, ("mqueue_conn_pool_add: added connection, total = %d", cp->conns));

	return 0;
}

amqp_connection_state_t
mqueue_conn_pool_del(mqueue_conn_pool cp)
{
	if (cp->conns == 0) {
		return NULL;
	}

	cp->conns--;
	return cp->conn[cp->conns];
}

int
mqueue_conn_pool_can_replenish(mqueue_conn_pool cp)
{
	int can = 0;
	mqueue_conn_pool_print(cp, "can_replenish-in");
	pthread_mutex_lock(&cp->mutex);

		if (cp->conns + cp->connecting < cp->required_conns) {
			cp->connecting++;
			can = 1;
		}

	pthread_mutex_unlock(&cp->mutex);
	mqueue_conn_pool_print(cp, "can_replenish-out");
	return can;
}

void mqueue_conn_pool_replenish_success(mqueue_conn_pool cp)
{
	int i;
	mqueue_conn_pool_print(cp, "replenish_success-in");
	pthread_mutex_lock(&cp->mutex);

		/*
		 * We have one new good connection, so we shift the entire pool to the right
		 * to make room for when we finally return it
		 */ 
		for ( i = 0 ; i < cp->conns ; ++i ) {
			cp->conn[cp->conns-i] = cp->conn[cp->conns-i-1];
		}

		cp->connecting--;
		cp->conns++;
		cp->next_to_take++;

	pthread_mutex_unlock(&cp->mutex);
	mqueue_conn_pool_print(cp, "replenish_success-out");
}

static void
destroy_conn(amqp_connection_state_t conn) {
	int channel = 1;
	amqp_channel_close(conn, channel, AMQP_REPLY_SUCCESS);
	amqp_connection_close(conn, AMQP_REPLY_SUCCESS);
	amqp_destroy_connection(conn);
}

static void
remove_all_connections(mqueue_conn_pool cp, int *to_clean, amqp_connection_state_t conn[MAX_VHOST_MQ_CONNS])
{
	int i;
	*to_clean = cp->conns;
	for ( i = 0 ; i < cp->conns ; ++i )
		conn[i] = cp->conn[i];

	cp->next_to_take = 0;
	cp->conns = 0;
}

void mqueue_conn_pool_replenish_failed(mqueue_conn_pool cp)
{
	int i, to_clean;
	amqp_connection_state_t conn[MAX_VHOST_MQ_CONNS];
	mqueue_conn_pool_print(cp, "replenish_failed-in");
	pthread_mutex_lock(&cp->mutex);

		/*
		 * Uh-oh we have a problem here! We can't assure that the rest of our conns
		 * are ok so we get rid of them right here and right now! Well, after we
		 * release the mutex is better.
		 */

		cp->connecting--;
		remove_all_connections(cp, &to_clean, conn);

	pthread_mutex_unlock(&cp->mutex);
	mqueue_conn_pool_print(cp, "replenish_failed-out");

	for ( i = 0 ; i < to_clean ; ++i ) 
		destroy_conn(conn[i]);
}

void
mqueue_conn_pool_dropped_conn(mqueue_conn_pool cp)
{
	if (cp->conns == 0) {
		DPRINTF(D_CRIT, ("mqueue_conn_pool_dropped_conn: there are no connections. What are you dropping, MF? (My friend)"));
		return;
	}

	int i, to_clean;
	amqp_connection_state_t conn[MAX_VHOST_MQ_CONNS];
	mqueue_conn_pool_print(cp, "dropped_conn-in");
	pthread_mutex_lock(&cp->mutex);

		/*
		 * We have a failed connection. Our pool is not to be trusted anymore!
		 * Die, die, die!
		 */

		remove_all_connections(cp, &to_clean, conn);

		/*
		 * There is a case in which we will NOT notice that connections are broken.
		 * This happens when RabbitMQ service is restarted without any transaction
		 * running while the process is down. In this case we may have dull conns
		 * in our pool but we will eventually notice and fix it when a thread gets
		 * a dull connection via mqueue_conn_pool_get and tries to use it.
		 */

	pthread_mutex_unlock(&cp->mutex);
	DPRINTF(D_WARNING, ("mqueue_conn_pool_dropped_conn: Connection dropped, flushing the pool. Check RabbitMQ availability."));
	mqueue_conn_pool_print(cp, "dropped_conn-out");

	for ( i = 0 ; i < to_clean ; ++i ) 
		destroy_conn(conn[i]);
}

amqp_connection_state_t
mqueue_conn_pool_get(struct mqueue_conn_pool *cp)
{
	int given = -1;
	amqp_connection_state_t conn = NULL;
	mqueue_conn_pool_print(cp, "get-in");
	pthread_mutex_lock(&cp->mutex);
	
		/* quickly refill the pool */
		if (mqueue_conn_pool_lost(cp)) {
			pthread_mutex_unlock(&cp->mutex);
			mqueue_conn_pool_print(cp, "get-hack");
			return NULL;
		}

		/* todo: add timeout */
		while (mqueue_conn_pool_conns(cp) && mqueue_conn_pool_is_empty(cp))
			pthread_cond_wait(&cp->not_empty, &cp->mutex);

		if (mqueue_conn_pool_conns(cp)) {
			conn = cp->conn[cp->next_to_take++];
			given = cp->next_to_take-1;
		}

	pthread_mutex_unlock(&cp->mutex);
	mqueue_conn_pool_print(cp, "get-out");
	if (given >= cp->conns / 2) {
		if (given >= cp->conns-1)
			DPRINTF(D_CRIT, ("mqueue_conn_pool_get: pool pressure up to capacity. %d connections out of %d", given+1, cp->conns));
		else
			DPRINTF(D_WARNING, ("mqueue_conn_pool_get: pool pressure over 50%%. %d connections out of %d", given+1, cp->conns));
	}
	return conn;
}

void
mqueue_conn_pool_return(struct mqueue_conn_pool *cp, amqp_connection_state_t conn)
{
	mqueue_conn_pool_print(cp, "return-in");
	pthread_mutex_lock(&cp->mutex);

		cp->conn[cp->next_to_take-1] = conn;
		cp->next_to_take--;
		pthread_cond_signal(&cp->not_empty);

	pthread_mutex_unlock(&cp->mutex);
	mqueue_conn_pool_print(cp, "return-out");
}

void
mqueue_conn_pool_print(mqueue_conn_pool cp, const char *tag)
{
	DPRINTF(D_DEBUG, ("mqueue_conn_pool[%s]: {ntt: %d, conns: %d, connecting: %d, required: %d | avail: %d, in_use: %d, lost: %d}", 
		tag, cp->next_to_take, cp->conns, cp->connecting, cp->required_conns,
		mqueue_conn_pool_available(cp), mqueue_conn_pool_in_use(cp), mqueue_conn_pool_lost(cp)
	));
}
