#ifndef RABBITMQ_BACKEND_H
#define RABBITMQ_BACKEND_H

#include <amqp.h>

typedef struct {
	int vhost_id;
	int channel;
	amqp_connection_state_t conn;
} rmq_conn;

rmq_conn * get_rmq_conn(const char* virtual_host, rmq_conn *rmq);
void drop_rmq_conn(rmq_conn *rmq);
void return_rmq_conn(rmq_conn *rmq);

#endif

