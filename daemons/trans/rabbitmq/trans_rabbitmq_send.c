#include <amqp.h>
#include <amqp_tcp_socket.h>
#include <bconf.h>

#include "rabbitmq_send.h"
#include "factory.h"

extern struct bconf_node *bconf_root;

static struct c_param parameters[] = {
	{"exchange", P_REQUIRED, "v_subject"},
	{"routing_key", P_REQUIRED, "v_subject"},
	{"message", P_REQUIRED, "v_rabbitmq_body"},
	{"virtual_host", P_REQUIRED, "v_subject"},
	{NULL, 0}
};

static struct factory_data data = {
	"rabbitmq",
	0
};

static const struct factory_action actions[] = {
        FA_FUNC(rabbitmq_send),
        FA_DONE()
};

/* Please, rewrite me as a Backend:
 * https://scmcoord.com/wiki/Trans_Cookbook#Interacting_with_other_services
 * https://scmcoord.com/wiki/TransBackends
 *
 * Oh, you did! Thank you epic programmers!
 *
 */
FACTORY_TRANS(rabbitmq_send, parameters, data, actions);
