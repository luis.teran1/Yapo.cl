#ifndef RABBITMQ_CHANNEL_POOL
#define RABBITMQ_CHANNEL_POOL

#include <amqp.h>

typedef struct mqueue_conn_pool * mqueue_conn_pool;

mqueue_conn_pool mqueue_conn_pool_new();
void mqueue_conn_pool_destroy(mqueue_conn_pool cp);

int mqueue_conn_pool_available(mqueue_conn_pool cp);
int mqueue_conn_pool_in_use(mqueue_conn_pool cp);
int mqueue_conn_pool_lost(mqueue_conn_pool cp);
int mqueue_conn_pool_conns(mqueue_conn_pool cp);
int mqueue_conn_pool_required_conns(mqueue_conn_pool cp);
int mqueue_conn_pool_connecting(mqueue_conn_pool cp);

void mqueue_conn_pool_set_required_conns(mqueue_conn_pool cp, int required);

int mqueue_conn_pool_add(mqueue_conn_pool cp, amqp_connection_state_t conn);
amqp_connection_state_t mqueue_conn_pool_del(mqueue_conn_pool cp);

int mqueue_conn_pool_can_replenish(mqueue_conn_pool cp);
void mqueue_conn_pool_replenish_success(mqueue_conn_pool cp);
void mqueue_conn_pool_replenish_failed(mqueue_conn_pool cp);
void mqueue_conn_pool_dropped_conn(mqueue_conn_pool cp);

amqp_connection_state_t mqueue_conn_pool_get(mqueue_conn_pool cp);
void mqueue_conn_pool_return(mqueue_conn_pool cp, amqp_connection_state_t conn);

void mqueue_conn_pool_print(mqueue_conn_pool cp, const char *tag);

#endif
