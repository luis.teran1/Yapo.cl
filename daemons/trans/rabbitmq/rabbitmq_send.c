#include <amqp.h>
#include <amqp_tcp_socket.h>

#include <pthread.h>

#include "rabbitmq_send.h"
#include "rabbitmq_backend.h"
#include "memalloc_functions.h"

#define MAX_VHOSTS		10
#define MAX_EXCHANGES	20
#define MAX_QUEUES		20
#define MAX_BINDINGS	50

#define GENERAL_ERROR -1
#define NETWORK_ERROR -2

static int
translate_error_code(int status)
{
	switch (status) {
		case AMQP_STATUS_BAD_AMQP_DATA:
		case AMQP_STATUS_UNKNOWN_CLASS:
		case AMQP_STATUS_UNKNOWN_METHOD:
		case AMQP_STATUS_CONNECTION_CLOSED:
		case AMQP_STATUS_SOCKET_ERROR:
		case AMQP_STATUS_TIMEOUT:
		case AMQP_STATUS_TIMER_FAILURE:
		case AMQP_STATUS_HEARTBEAT_TIMEOUT:
		case AMQP_STATUS_UNEXPECTED_STATE:
			return NETWORK_ERROR;
		default:
			break;
	}
	return GENERAL_ERROR;
}

static int
rabbitmq_publish(struct cmd *cs, amqp_connection_state_t conn, int channel, const char *exchange, const char *routing_key, const char *message)
{
	amqp_bytes_t message_bytes;
	message_bytes.len = strlen(message);
	message_bytes.bytes = (void *) message;

	amqp_basic_properties_t props;
	props._flags = AMQP_BASIC_DELIVERY_MODE_FLAG;
	props.delivery_mode = AMQP_DELIVERY_PERSISTENT;

	amqp_boolean_t mandatory = 0;
	amqp_boolean_t immediate = 0;

	DPRINTF(D_DEBUG, ("rabbitmq_publish: sending message through channel %d", channel));

	int status = amqp_basic_publish(conn, channel, amqp_cstring_bytes(exchange), amqp_cstring_bytes(routing_key), mandatory, immediate, &props, message_bytes);

	if (status < 0) {
		int error = translate_error_code(status);
		transaction_logprintf(cs->ts, D_ERROR, "rabbitmq_publish: error sending message (%d): %s", status, amqp_error_string2(status));
		return error;
	}

	return 0;
}

int
rabbitmq_send(struct cmd *cs, struct bpapi *ba)
{
	const char *virtual_host = cmd_getval(cs, "virtual_host");
	const char *exchange = cmd_getval(cs, "exchange");
	const char *routing_key = cmd_getval(cs, "routing_key");
	const char *message = cmd_getval(cs, "message");

	if (!virtual_host) {
		cs->status = "TRANS_ERROR_RABBITMQ_BAD_VHOST";
		return -1;
	}

	int status = -1;
	int general_error_count = 0;
	int general_error_threshold = 3;
	rmq_conn conn, *rmq;
	
	while (status < 0 && (rmq = get_rmq_conn(virtual_host, &conn))) {

		status = rabbitmq_publish(cs, rmq->conn, rmq->channel, exchange, routing_key, message);
		if (status < 0) {
			if (status == NETWORK_ERROR ) {
				transaction_logprintf(cs->ts, D_ERROR, "rabbitmq_publish failed miserably. Closing connection.");
				drop_rmq_conn(rmq);
			} else if (general_error_count < general_error_threshold) {
				transaction_logprintf(cs->ts, D_ERROR, "rabbitmq_publish failed elegantly. Retrying.");
				return_rmq_conn(rmq);
				general_error_count++;
				rmq = NULL;
			} else {
				transaction_logprintf(cs->ts, D_ERROR, "rabbitmq_publish failed elegantly %d times in a row. Discarding.", general_error_threshold);
				break;
			}
		}
	
	}

	if (!rmq) {
		cs->status = "TRANS_ERROR_RABBITMQ_BAD_VHOST";
		return -1;
	}

	return_rmq_conn(rmq);
	return 0;
}
