#include "rabbitmq_conf.h"

static void
rabbitmq_vhost_free(struct rabbitmq_vhost *vhost)
{
	if (vhost) {
		int i;
		for ( i = 0 ; i < vhost->n_exchanges ; ++i )
			free(vhost->exchange[i]);
		for ( i = 0 ; i < vhost->n_queues ; ++i )
			free(vhost->queue[i]);
		for ( i = 0 ; i < vhost->n_bindings ; ++i )
			free(vhost->binding[i]);
		free(vhost);
	}
}

static void
rabbitmq_vhost_add(struct rabbitmq_conf *rmq_conf, struct rabbitmq_vhost *vhost)
{
	if (rmq_conf->n_vhosts < MAX_VHOSTS) {
		rmq_conf->vhost[rmq_conf->n_vhosts++] = vhost;
	} else {
		DPRINTF(D_DEBUG, ("[rabbitmq_backend] vhost_add: too many vhosts (%d)", MAX_VHOSTS));
		rabbitmq_vhost_free(vhost);
	}
}

static void
rabbitmq_vhost_add_exchange(struct rabbitmq_vhost *vhost, struct rabbitmq_exchange *exchange)
{
	if (vhost->n_exchanges < MAX_EXCHANGES) {
		vhost->exchange[vhost->n_exchanges++] = exchange;
	} else {
		DPRINTF(D_DEBUG, ("[rabbitmq_backend] vhost_add_exchange: too many exchanges (%d)", MAX_EXCHANGES));
		free(exchange);
	}
}

static void
rabbitmq_vhost_add_queue(struct rabbitmq_vhost *vhost, struct rabbitmq_queue *queue)
{
	if (vhost->n_queues < MAX_QUEUES) {
		vhost->queue[vhost->n_queues++] = queue;
	} else {
		DPRINTF(D_DEBUG, ("[rabbitmq_backend] vhost_add_queue: too many queue (%d)", MAX_QUEUES));
		free(queue);
	}
}

static void
rabbitmq_vhost_add_binding(struct rabbitmq_vhost *vhost, struct rabbitmq_binding *binding)
{
	if (vhost->n_bindings < MAX_BINDINGS) {
		vhost->binding[vhost->n_bindings++] = binding;
	} else {
		DPRINTF(D_DEBUG, ("[rabbitmq_backend] vhost_add_binding: too many bindings (%d)", MAX_BINDINGS));
		free(binding);
	}
}

static struct rabbitmq_vhost *
rabbitmq_vhost_new(const char *host, int port, int connections, const char *name, const char *path, const char *user, const char *pass)
{
	struct rabbitmq_vhost *vhost = zmalloc(sizeof(struct rabbitmq_vhost));
	vhost->host = host;
	vhost->port = port;
	vhost->connections = connections;
	vhost->name = name;
	vhost->path = path;
	vhost->user = user;
	vhost->pass = pass;
	DPRINTF(D_DEBUG, ("[rabbitmq_backend] vhost_new <%s,%d,%d,%s,%s,%s,%s>", host, port, connections, name, path, user, "****"));
	return vhost;
}

static struct rabbitmq_exchange *
rabbitmq_exchange_new(const char *name, const char *type, int passive, int durable)
{
	struct rabbitmq_exchange *exchange = zmalloc(sizeof(struct rabbitmq_exchange));
	exchange->name = name;
	exchange->type = type;
	exchange->passive = passive;
	exchange->durable = durable;
	DPRINTF(D_DEBUG, ("[rabbitmq_backend] exchange_new <%s,%s,%d,%d>", name, type, passive, durable));
	return exchange;
}

static struct rabbitmq_queue *
rabbitmq_queue_new(const char *name, int passive, int durable, int exclusive, int auto_delete)
{
	struct rabbitmq_queue *queue = zmalloc(sizeof(struct rabbitmq_queue));
	queue->name = name;
	queue->passive = passive;
	queue->durable = durable;
	queue->exclusive = exclusive;
	queue->auto_delete = auto_delete;
	DPRINTF(D_DEBUG, ("[rabbitmq_backend] queue_new <%s,%d,%d,%d,%d>", name, passive, durable, exclusive, auto_delete));
	return queue;
}

static struct rabbitmq_binding *
rabbitmq_binding_new(const char *exchange, const char *queue, const char *key)
{
	struct rabbitmq_binding *binding = zmalloc(sizeof(struct rabbitmq_binding));
	binding->exchange = exchange;
	binding->queue = queue;
	binding->key = key;
	DPRINTF(D_DEBUG, ("[rabbitmq_backend] binding_new <%s,%s,%s>", exchange, queue, key));
	return binding;
}

static void
rabbitmq_conf_load_exchanges(struct rabbitmq_vhost *vhost, struct bconf_node *xs)
{
	int i, n = bconf_count(xs);
	for ( i = 0 ; i < n ; ++i ) {
		struct bconf_node *x = bconf_byindex(xs, i);

		const char *name = bconf_get_string(x, "name");
		const char *type = bconf_get_string(x, "type");
		int passive = bconf_get_int(x, "passive");
		int durable = bconf_get_int(x, "durable");

		struct rabbitmq_exchange *exchange = rabbitmq_exchange_new(name, type, passive, durable);
		rabbitmq_vhost_add_exchange(vhost, exchange);
	}
}

static void
rabbitmq_conf_load_queues(struct rabbitmq_vhost *vhost, struct bconf_node *qs)
{
	int i, n = bconf_count(qs);
	for ( i = 0 ; i < n ; ++i ) {
		struct bconf_node *q = bconf_byindex(qs, i);

		const char *name = bconf_get_string(q, "name");
		int passive = bconf_get_int(q, "passive");
		int durable = bconf_get_int(q, "durable");
		int exclusive = bconf_get_int(q, "exclusive");
		int auto_delete = bconf_get_int(q, "auto_delete");

		struct rabbitmq_queue *queue = rabbitmq_queue_new(name, passive, durable, exclusive, auto_delete);
		rabbitmq_vhost_add_queue(vhost, queue);
	}
}

static void
rabbitmq_conf_load_bindings(struct rabbitmq_vhost *vhost, struct bconf_node *bs)
{
	int i, n = bconf_count(bs);
	for ( i = 0 ; i < n ; ++i ) {
		struct bconf_node *b = bconf_byindex(bs, i);

		const char *exchange = bconf_get_string(b, "exchange");
		const char *queue = bconf_get_string(b, "queue");
		const char *key = bconf_get_string(b, "key");

		struct rabbitmq_binding *binding = rabbitmq_binding_new(exchange, queue, key);
		rabbitmq_vhost_add_binding(vhost, binding);
	}
}

static struct rabbitmq_vhost *
rabbitmq_conf_load_vhost(struct bconf_node *vhost_node)
{
	const char *name = bconf_get_string(vhost_node, "name");
	const char *path = bconf_get_string(vhost_node, "path");
	const char *host = bconf_get_string(vhost_node, "host");
	int port = bconf_get_int(vhost_node, "port");
	int connections = bconf_get_int(vhost_node, "connections");

	if (!name || !path || !host || !port || !connections) {
		DPRINTF(D_DEBUG, ("[rabbitmq_backend] conf_load_vhost: incomplete vhost (%s)", bconf_key(vhost_node)));
		return NULL;
	}

	if ( connections > MAX_VHOST_MQ_CONNS ) {
		DPRINTF(D_DEBUG, ("[rabbitmq_backend] conf_load_vhost: too many connections for vhost[%s] truncating to %d", bconf_key(vhost_node), MAX_VHOST_MQ_CONNS));
		connections = MAX_VHOST_MQ_CONNS;
	}

	const char *user = bconf_get_string(vhost_node, "user");
	const char *pass = bconf_get_string(vhost_node, "password");

	struct rabbitmq_vhost *vhost = rabbitmq_vhost_new(host, port, connections, name, path,user, pass);

	struct bconf_node *exchanges = bconf_get(vhost_node, "exchange");
	struct bconf_node *queues = bconf_get(vhost_node, "queue");
	struct bconf_node *bindings = bconf_get(vhost_node, "binding");

	rabbitmq_conf_load_exchanges(vhost, exchanges);
	rabbitmq_conf_load_queues(vhost, queues);
	rabbitmq_conf_load_bindings(vhost, bindings);

	return vhost;
}

struct rabbitmq_conf *
rabbitmq_conf_load(struct bconf_node *conf)
{
	struct bconf_node *vhosts = bconf_get(conf, "rabbitmq.virtual_host");
	if (!vhosts) {
		DPRINTF(D_DEBUG, ("[rabbitmq_backend] conf_load: no virtualhost found"));
		return NULL;
	}

	struct rabbitmq_conf *rmq_conf = zmalloc(sizeof(struct rabbitmq_conf));

	int i, n = bconf_count(vhosts);
	for ( i = 0 ; i < n ; ++i ) {
		struct bconf_node *vhost_node = bconf_byindex(vhosts, i); 
		struct rabbitmq_vhost *vhost = rabbitmq_conf_load_vhost(vhost_node);
		rabbitmq_vhost_add(rmq_conf, vhost);
	}

	return rmq_conf;
}

void
rabbitmq_conf_free(struct rabbitmq_conf *conf) 
{
	if (conf) {
		int i;
		for ( i = 0 ; i < conf->n_vhosts ; ++i ) {
			rabbitmq_vhost_free(conf->vhost[i]);
		}
		free(conf);
	}
}
