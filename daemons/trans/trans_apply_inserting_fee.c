#include "factory.h"
#include "bpapi.h"
#include <bconf.h>

extern struct bconf_node *bconf_root;

static struct c_param parameters[] = {
	{"ad_id",          P_REQUIRED, "v_if_jobs_ad_id"},
	{"remote_addr",    P_REQUIRED, "v_ip"},
	{"remote_browser", P_OPTIONAL, "v_remote_browser"},
	{NULL, 0}
};

static struct factory_data data = {
	"apply_products"
};

static const struct factory_action actions[] = {
	FA_SQL("", "pgsql.master", "sql/get_clear_payment_info.sql", 0),
	FA_TRANS("trans/clear_ad_by_paycode.txt", "fat_", FATRANS_BACAPTURE),
	FA_DONE()
};

FACTORY_TRANS(apply_inserting_fee, parameters, data, actions);
