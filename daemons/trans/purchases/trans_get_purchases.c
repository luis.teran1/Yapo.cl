#include "factory.h" 

static struct c_param parameters[] = {
	{"pgids",	P_REQUIRED,	"v_id_list"},
	{NULL, 0}
};

static struct factory_data data = {
        "get_purchases",
        FACTORY_NO_RES_CNTR
};

static const struct factory_action actions[] = {
        FA_SQL(NULL, "pgsql.slave", "sql/get_purchases.sql", FATRANS_OUT),
        {0}
};

FACTORY_TRANS(get_purchases, parameters, data, actions);
