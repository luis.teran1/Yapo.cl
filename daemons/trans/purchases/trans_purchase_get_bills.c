#include "factory.h" 

static struct c_param parameters[] = {
	{"payment_status",	P_REQUIRED,	"v_array:unpaid,unverified,paid,verified,cleared"},
	{"purchase_status",	P_REQUIRED,	"v_array:paid,reversed,pending,refused,sent,confirmed,refunded,voided,failed"},
	{NULL, 0}
};

FACTORY_TRANSACTION(purchase_get_bills, &config.pg_slave, "sql/purchase_get_bills.sql", NULL, parameters, FACTORY_EMPTY_RESULT_OK);
