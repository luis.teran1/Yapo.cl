#include "factory.h"
#include "utils/send_backend_event.h"

static struct c_param parameters[] = {
	{"purchase_id",		P_REQUIRED,	"v_integer"},
	{"status",			P_REQUIRED,	"v_array:pending,paid,refused,sent,confirmed,refunded,voided,failed"},
	{"doc_num",			P_OPTIONAL,	"v_integer"},
	{"external_doc_id", P_OPTIONAL,	"v_integer"},
    {NULL, 0}
};

static struct factory_data data = {
	"purchase_update",
	FACTORY_EMPTY_RESULT_OK
};

static int notify_event(struct cmd *cs, struct bpapi *ba) {
	// Check if it should send a backend event
	int be_enabled = bconf_get_int(trans_bconf, "purchase_update.backend_event.send_enabled");
	if (be_enabled) {
		transaction_logprintf(cs->ts, D_INFO, "SEND PURCHASE UPDATE BACKEND EVENT ACTIVE");
		bpapi_insert(ba, "type", "purchase_update");
		bpapi_insert(ba, "param_purchase_id", cmd_getval(cs, "purchase_id"));
		bpapi_insert(ba, "param_status", cmd_getval(cs, "status"));
		if (cmd_haskey(cs, "doc_num")) {
			bpapi_insert(ba, "param_doc_num", cmd_getval(cs, "doc_num"));
		}
		if (cmd_haskey(cs, "external_doc_id")) {
			bpapi_insert(ba, "param_external_doc_id", cmd_getval(cs, "external_doc_id"));
		}
		publish_backend_event(cs, ba);
	}
	return 0;
}
static const struct factory_action actions[] = {
	FA_SQL("", "pgsql.master", "sql/call_purchase_update.sql", FASQL_OUT),
	FA_FUNC(notify_event),
	FA_DONE()
};
FACTORY_TRANS(purchase_update, parameters, data, actions);
