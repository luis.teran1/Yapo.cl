#include "factory.h"

static struct c_param parameters[] = {
	{"payment_group_id",		P_REQUIRED,	"v_integer"},
    {NULL, 0}
};

FACTORY_TRANSACTION(purchase_get, &config.pg_master, "sql/purchase_get.sql", NULL, parameters, FACTORY_EMPTY_RESULT_OK);
