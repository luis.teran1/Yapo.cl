#include "factory.h"

static struct c_param parameters[] = {
	{"email",			P_REQUIRED,	"v_email"},
	{"account_id",		P_REQUIRED,	"v_account_id"},
	{"is_company",  	P_REQUIRED, "v_is_company"},

	{"receipt_date",	P_OPTIONAL,	"v_date_time"},
	{"external_receipt",P_OPTIONAL,	"v_external_receipt"},
	{"status",			P_REQUIRED, "v_array:pending,confirmed,refused,refunded,failed"},

	{"product_id",		P_REQUIRED,	"v_product_id"},
	{"product_price",	P_REQUIRED,	"v_product_price"},
	{"product_name",	P_REQUIRED,	"v_multiple_product_names"},

	{"ad_id",			P_OPTIONAL,	"v_ad_id"},

	{"payment_method",	P_REQUIRED,	"v_payment_methods"},
	{"payment_platform",P_REQUIRED,	"v_payment_platform"},
	{"currency",		P_OPTIONAL,	"v_string_isprint"},

	{"remote_addr",		P_REQUIRED,	"v_ip"},
	{"remote_browser", 	P_OPTIONAL, "v_remote_browser"},
	{"action_id",		P_OPTIONAL,	"v_id"},
	{NULL, 0}
};

FACTORY_TRANSACTION(insert_purchase_in_app, &config.pg_master, "sql/call_insert_purchase_in_app.sql", NULL, parameters, FACTORY_NO_RES_CNTR | FACTORY_RES_SHORT_FORM);
