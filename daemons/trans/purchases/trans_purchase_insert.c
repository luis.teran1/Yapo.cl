#include "factory.h"
#include "utils/send_backend_event.h"
#include <ctype.h>

struct prod_data_t {
	int id;
	int prod;
	const char *name;
};

struct prod_data_list_t {
	int n;
	int last;
	struct prod_data_t *pds;
};

static void
prod_data_list_init(struct prod_data_list_t *pdl, int length) {
	pdl->n = length;
	pdl->last = 0;
	pdl->pds = xcalloc(length, sizeof(struct prod_data_t));
}

static void
prod_data_list_clear(struct prod_data_list_t *pdl) {
	free(pdl->pds);
}

static int
vf_validate_duplicated_prod_for_same_id(struct cmd_param *param, const char *arg) {

	if (!cmd_haskey(param->cs, "product_id") || !cmd_haskey(param->cs, "product_name")) {
		transaction_logprintf(param->cs->ts, D_WARNING, "Missing params.");
		return 0;
	}


	if (!*param->value){
		param->error = "ERROR_AD_IDS_MISSING";
		param->cs->status = "TRANS_ERROR";
		return 1;
	}

	char *product_ids = xstrdup(cmd_getval(param->cs, "product_id"));
	char *freeptr_ids = product_ids;
	char *elem_id;
	char *id_ptr = NULL;
	int has_error = 0;

	char *product_names = xstrdup(cmd_getval(param->cs, "product_name"));
	char *freeptr_names = product_names;
	char *elem_name;
	char *name_ptr = NULL;

	char *product_ads = xstrdup(param->value);
	char *freeptr_ads = product_ads;
	char *elem_ad;
	char *ad_ptr = NULL;

	int elements_number = 1;
	for (int i = 0; param->value[i]; i++) {
		elements_number += (param->value[i] == '|');
	}

	struct prod_data_list_t pdl;
	prod_data_list_init(&pdl, elements_number);

	do {
		elem_id = strtok_r(product_ids, "|", &id_ptr);
		elem_name = strtok_r(product_names, "|", &name_ptr);
		elem_ad = strtok_r(product_ads, "|", &ad_ptr);
		product_ids = NULL;
		product_names = NULL;
		product_ads = NULL;
		if (!elem_name || !elem_id || !elem_ad) {
			break;
		}
		const char *bconf_name = bconf_vget_string(trans_bconf, "payment_search_dup", elem_name, "key", NULL);
		if (!bconf_name || (elem_ad && strcmp(elem_ad, "0") == 0)) {
			continue;
		}
		int ad_id = atoi(elem_ad);
		for (int i = 0; i < pdl.n && i < pdl.last  ; ++i) {
			if (pdl.pds[i].name && strcmp(pdl.pds[i].name, bconf_name) == 0 && pdl.pds[i].id == ad_id) {
				char *name_to_upper = xstrdup(bconf_name);
				transaction_printf(param->cs->ts, "error:ERROR_ID_FOR_%s_REPEATED\n", strmodify(name_to_upper, toupper));
				param->cs->status = "TRANS_ERROR";
				has_error = 1;
				free(name_to_upper);
				goto cleanup;
			}
		}

		pdl.pds[pdl.last].id = ad_id;
		pdl.pds[pdl.last].name = bconf_name;
		pdl.pds[pdl.last].prod = atoi(elem_id);
		pdl.last++;
	} while (elem_id && elem_name && elem_ad);

cleanup:
	prod_data_list_clear(&pdl);
	free(freeptr_ids);
	free(freeptr_names);
	free(freeptr_ads);

	return has_error;
}

struct validator v_multiple_list_ids_search_dup[] = {
	VALIDATOR_REGEX("^[[:digit:]]+([|][[:digit:]]+)*$", REG_EXTENDED|REG_NOSUB, "ERROR_INVALID_LIST_IDS"),
	VALIDATOR_FUNC(vf_validate_duplicated_prod_for_same_id),
	{0}
};
ADD_VALIDATOR(v_multiple_list_ids_search_dup);

static struct c_param parameters[] = {
	{"doc_type",		P_REQUIRED,	"v_doc_type"},
	{"receipt_date",	0,			"v_date"},
	{"email",			P_REQUIRED,	"v_email"},
	{"store_id",		0,			"v_integer"},
	{"account_id",		0,			"v_account_id"},

	{"product_id",		P_REQUIRED,	"v_multiple_product_ids"},
	{"product_price",	P_REQUIRED,	"v_multiple_product_prices"},
	{"product_name",	P_REQUIRED,	"v_multiple_product_names"},
	{"product_params",	P_REQUIRED,	"v_multiple_product_params"},
	{"list_id",			0,			"v_multiple_list_ids_search_dup"},

	{"seller_id", 		0,			"v_seller_id"},

	{"payment_method",	P_REQUIRED,	"v_payment_methods"},
	{"payment_platform",P_REQUIRED,	"v_payment_platform"},

	{"tax",			 	0,			"v_integer"},
	{"remote_addr",		P_REQUIRED,	"v_ip"},
	{"token",			P_OPTIONAL, "v_token"},
	{"remote_browser", 	P_OPTIONAL, "v_remote_browser"},
	{NULL, 0}
};

static struct factory_data data = {
	"purchase_insert",
	FACTORY_NO_RES_CNTR | FACTORY_RES_SHORT_FORM
};

static int notify_event(struct cmd *cs, struct bpapi *ba) {
	// Check if it should send a backend event
	int be_enabled = bconf_get_int(trans_bconf, "purchase_insert.backend_event.send_enabled");
	if (be_enabled) {
		transaction_logprintf(cs->ts, D_INFO, "SEND PURCHASE INSERT BACKEND EVENT ACTIVE");
		bpapi_insert(ba, "type", "purchase_insert");
		bpapi_insert(ba, "param_doc_type", cmd_getval(cs, "doc_type"));
		bpapi_insert(ba, "param_email", cmd_getval(cs, "email"));
		bpapi_insert(ba, "param_product_id", cmd_getval(cs, "product_id"));
		bpapi_insert(ba, "param_product_price", cmd_getval(cs, "product_price"));
		bpapi_insert(ba, "param_product_name", cmd_getval(cs, "product_name"));
		bpapi_insert(ba, "param_product_params", cmd_getval(cs, "product_params"));
		bpapi_insert(ba, "param_payment_method", cmd_getval(cs, "payment_method"));
		bpapi_insert(ba, "param_payment_platform", cmd_getval(cs, "payment_platform"));
		bpapi_insert(ba, "param_remote_addr", cmd_getval(cs, "remote_addr"));
		if (cmd_haskey(cs, "receipt_date")) {
			bpapi_insert(ba, "param_receipt_date", cmd_getval(cs, "receipt_date"));
		}
		if (cmd_haskey(cs, "account_id")) {
			bpapi_insert(ba, "param_account_id", cmd_getval(cs, "account_id"));
		}
		if (cmd_haskey(cs, "list_id")) {
			bpapi_insert(ba, "param_list_id", cmd_getval(cs, "list_id"));
		}
		if (cmd_haskey(cs, "seller_id")) {
			bpapi_insert(ba, "param_seller_id", cmd_getval(cs, "seller_id"));
		}
		if (cmd_haskey(cs, "token")) {
			bpapi_insert(ba, "param_token", cmd_getval(cs, "token"));
		}
		const char *payment_group_id = bpapi_get_element(ba, "o_payment_group_id", 0);
		const char *purchase_id = bpapi_get_element(ba, "o_purchase_id", 0);
		const char * pay_code = bpapi_get_element(ba, "o_pay_code", 0);
		const char * purchase_status = bpapi_get_element(ba, "o_purchase_status", 0);
		bpapi_insert(ba, "param_payment_group_id", payment_group_id);
		bpapi_insert(ba, "param_pay_code", pay_code);
		bpapi_insert(ba, "param_purchase_id", purchase_id);
		bpapi_insert(ba, "param_purchase_status", purchase_status);
		publish_backend_event(cs, ba);
	}
	return 0;
}

static int return_output(struct cmd *cs, struct bpapi *ba) {
	const char *payment_group_id = bpapi_get_element(ba, "o_payment_group_id", 0);
	const char *pay_code = bpapi_get_element(ba, "o_pay_code", 0);
	transaction_printf(cs->ts, "o_payment_group_id:%s\no_pay_code:%s\n", payment_group_id, pay_code);
	return 0;
}

static const struct factory_action actions[] = {
	FA_SQL("", "pgsql.master", "sql/call_insert_purchase.sql", 0),
	FA_FUNC(notify_event),
	FA_FUNC(return_output),
	FA_DONE()
};

FACTORY_TRANS(purchase_insert, parameters, data, actions);
