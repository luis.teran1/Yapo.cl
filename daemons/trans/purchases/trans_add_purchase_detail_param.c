#include "factory.h"

static struct c_param parameters[] = {
	{"payment_group_id", P_REQUIRED, "v_id"},
	{"name", P_REQUIRED, "v_array:error"},
	{"value", P_REQUIRED, "v_string_isprint"},
	{NULL, 0}
};

FACTORY_TRANSACTION(
	add_purchase_detail_param,
	&config.pg_master,
	"sql/call_add_purchase_detail_param.sql",
	NULL,
	parameters,
	FACTORY_RES_SHORT_FORM
);
