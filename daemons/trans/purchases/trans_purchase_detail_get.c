#include "factory.h" 

static struct c_param parameters[] = {
	{"doc_num",	P_REQUIRED,	"v_doc_num"},
	{"doc_type",	P_REQUIRED,	"v_array:bill,invoice"},
	{"email",	0,		"v_email"},
	{NULL, 0}
};

struct validator v_doc_num[] = {
	VALIDATOR_REGEX("^[0-9]{1,10}$", REG_EXTENDED, "ERROR_DOC_NUM_INVALID"),
	{0}
};
ADD_VALIDATOR(v_doc_num);

FACTORY_TRANSACTION(purchase_detail_get, &config.pg_slave, "sql/purchase_detail_get.sql", NULL, parameters, FACTORY_NO_RES_CNTR | FACTORY_RES_SHORT_FORM);
