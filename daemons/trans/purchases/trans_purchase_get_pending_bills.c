#include "factory.h" 

static struct c_param parameters[] = {
	{NULL, 0}
};

FACTORY_TRANSACTION(purchase_get_pending_bills, &config.pg_slave, "sql/purchase_get_pending_bills.sql", NULL, parameters, FACTORY_NO_RES_CNTR | FACTORY_RES_SHORT_FORM);
