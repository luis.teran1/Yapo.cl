#ifndef _MAIL_LOG_H
#define _MAIL_LOG_H

#define MAIL_SPAM_SEND 0
#define MAIL_SPAM_RECV 1
#define MAIL_SPAM_AD   2

#define MAIL_SPAM_NUM  3

struct mail_log {
	int state;
	const char *type;
	const char *email;
	const char *remote_addr;
	int list_id;
	int spam_counts[MAIL_SPAM_NUM];
	int spam_minutes;
	int check_only;
	void (*cb)(struct mail_log *, int, const char *);
	void *data;
};

void log_mail(struct mail_log *log);

#endif /*_MAIL_LOG_H*/
