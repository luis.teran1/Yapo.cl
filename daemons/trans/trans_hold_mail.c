#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <time.h>

#include "trans.h"
#include "util.h"
#include <ctemplates.h>
#include "validators.h"
#include <bconf.h>
#include <event.h>

#define HOLD_MAIL_DONE  0
#define HOLD_MAIL_ERROR 1
#define HOLD_MAIL_INIT  2
#define HOLD_MAIL_DELETE 3
#define HOLD_MAIL_DELETE_GET_RESULT 4
#define HOLD_MAIL_SET_PARAM 5
#define HOLD_MAIL_FLUSH 6
#define HOLD_MAIL_LIST 7
#define HOLD_MAIL_LIST_GET_RESULT 8

struct hold_mail_state {
	int state;
        char *sql;
	struct cmd *cs;
	struct sql_worker *sw;
        struct event timeout_event;
};

static struct c_param parameters[] = {
	{"delete",	0,	"v_string_isprint"},
	{"name",        0,	"v_string_isprint"},
	{"value",	0,	"v_string_isprint"},
	{"flush",	0,	"v_string_isprint"},
	{NULL, 0}
};

static const char *hold_mail_fsm(void *, struct sql_worker *, const char *);


/*****************************************************************************
 * hold_mail_done
 * Exit function.
 **********/
static void
hold_mail_done(void *v) {
	struct hold_mail_state *state = v;
	struct cmd *cs = state->cs;
	free(state);
	cmd_done(cs);
}


static void
hold_mail_flush_cb(struct internal_cmd *cmd, const char *line, void *data) {
	struct hold_mail_state *state = data;
	
	if (!line) {
		/* Flush command done, return to fsm. */
		hold_mail_fsm(state, NULL, NULL);
	} else if (!strncmp (line, "flushed:", 8))
		transaction_printf(state->cs->ts, "%s\n", line);
}



/*****************************************************************************
 * hold_mail_fsm
 * State machine
 **********/
static const char *
hold_mail_fsm(void *v, struct sql_worker *worker, const char *errstr) {
	struct hold_mail_state *state = v;
	struct cmd *cs = state->cs;
	struct bpapi ba;

	transaction_logprintf(cs->ts, D_DEBUG, "hold_mail_fsm(%p, %p, %p), state=%d", 
                              v, worker, errstr, state->state);

        /*
         * Clean up pending data
         */
	if (state->sql) {
		free(state->sql);
		state->sql = NULL;
	}

	/* 
	 * General error handling for errors.
	 */
	if (errstr || (worker && worker->sql_errno(worker))) {
                transaction_logprintf(cs->ts, D_ERROR, "Statement failed (%d, %u, %s)", 
                                      state->state,
                                      worker ? worker->sql_errno(worker) : 0,
                                      errstr ? errstr : worker->sql_errstr(worker));
                cs->status = "TRANS_DATABASE_ERROR";
                cs->message = xstrdup(errstr ? errstr : worker->sql_errstr(worker));
		state->state = HOLD_MAIL_ERROR;
		return NULL;
        }

	while (1) {
		switch (state->state) {
		case HOLD_MAIL_INIT:
			/* next state */
			state->state = HOLD_MAIL_DELETE;
                        break;


		case HOLD_MAIL_DELETE:
                        if (cmd_haskey(cs, "delete")) {

                                state->state = HOLD_MAIL_DELETE_GET_RESULT;
                                /* Register SQL query with our hold_mail_fsm() as callback function */

				BPAPI_INIT(&ba, NULL, pgsql);
				bpapi_insert(&ba, "delete", cmd_getval(cs, "delete"));

				sql_worker_template_query(&config.pg_master, "sql/hold_mail_delete.sql", &ba,
						hold_mail_fsm, state, cs->ts->log_string);
				bpapi_output_free(&ba);
				bpapi_simplevars_free(&ba);
				bpapi_vtree_free(&ba);
                                return NULL;
                        } else {
                                state->state = HOLD_MAIL_SET_PARAM;
                        }
                        break;
                        

                case HOLD_MAIL_DELETE_GET_RESULT:
                        /* check result */
                        state->state = HOLD_MAIL_SET_PARAM;
                        break;

                case HOLD_MAIL_SET_PARAM:
			state->state = HOLD_MAIL_FLUSH;
                        if (cmd_haskey(cs, "name") && cmd_haskey(cs, "value")) {

				BPAPI_INIT(&ba, NULL, pgsql);
				bpapi_insert(&ba, "name", cmd_getval(cs, "name"));
				bpapi_insert(&ba, "value", cmd_getval(cs, "value"));
				
                                /* Register SQL query with our hold_mail_fsm() as callback function */
				sql_worker_template_query(&config.pg_master, "sql/hold_mail_insert.sql", &ba,
						hold_mail_fsm, state, cs->ts->log_string);

				bpapi_output_free(&ba);
				bpapi_simplevars_free(&ba);
				bpapi_vtree_free(&ba);
                                return NULL;
                        }
                        break;
                                   
                case HOLD_MAIL_FLUSH:
                        state->state = HOLD_MAIL_LIST;
			if (cmd_haskey(cs, "flush")) {
				/* Flush hold mail queue */
				struct internal_cmd *cmd = zmalloc(sizeof(struct internal_cmd));
				
				cmd->cmd_string = xstrdup("cmd:flushqueue\n"
					  "commit:1\n"
					  "queue:hold_mail\n"
					  "end\n");
				transaction_logprintf(cs->ts, D_DEBUG, "Flushing mail queue");
				cmd->cb = hold_mail_flush_cb;
				cmd->data = state;
				transaction_internal(cmd);
				return NULL;
			}
                        break;

                case HOLD_MAIL_LIST:
                        /* list result */
                        state->state = HOLD_MAIL_LIST_GET_RESULT;

			sql_worker_template_query(&config.pg_master, "sql/hold_mail_list.sql", NULL,
                                         hold_mail_fsm, state, cs->ts->log_string);
                        return NULL;
                        

                case HOLD_MAIL_LIST_GET_RESULT:
                        if (worker->rows(worker)) {
                                while (worker->next_row(worker)) {
                                        transaction_printf(cs->ts, "%s.%s:%s\n",
                                                           worker->get_value(worker, 0), 
                                                           "name",
                                                           worker->get_value(worker, 1));
                                        transaction_printf(cs->ts, "%s.%s:%s\n",
                                                           worker->get_value(worker, 0),
                                                           "value", 
                                                           worker->get_value(worker, 2));
                                }
                        }
                        state->state = HOLD_MAIL_DONE;
                        break;

                case HOLD_MAIL_DONE:
                        hold_mail_done(state);
                        return NULL;
                case HOLD_MAIL_ERROR:
                        hold_mail_done(state);
                        return NULL;

		default:
			/* MAJOR STATE ERROR */
			cs->status = "TRANS_ERROR";
			transaction_logprintf(cs->ts, D_ERROR, "Unknown state (%d)", 
					      state->state);
			hold_mail_done(state);
			return NULL;

		}
	}
}

/*****************************************************************************
 * hold_mail
 * Main function that initialise a state-structure and calls the state machine.
 **********/
static void
hold_mail(struct cmd *cs) {
	transaction_logprintf(cs->ts, D_DEBUG, "Start Hold_Mail transaction");
	struct hold_mail_state *state;
	state = zmalloc(sizeof(*state));
	state->cs = cs;
	state->state = HOLD_MAIL_INIT;

	hold_mail_fsm(state, NULL, NULL);
}

ADD_COMMAND(hold_mail,     /* Name of command */
		NULL,
            hold_mail,     /* Main function name */
            parameters,    /* Parameters struct */
	    "Holds mail if content match regular expressions"); 
