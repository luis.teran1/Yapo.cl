#ifndef TRANS_SHA1_WRAPPER_H
#define TRANS_SHA1_WRAPPER_H

#include "util.h"
#include "sha1.h"
#include "openssl/sha.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char * sha1(const char *);
char * sha256(const char *);

#endif

