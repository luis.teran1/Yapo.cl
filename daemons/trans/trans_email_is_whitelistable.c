#include "factory.h"

static struct c_param parameters[] = {
	{"email", P_REQUIRED, "v_email_basic"},
	{"min_ads", 0		, "v_bool"},
	{NULL, 0}
};

FACTORY_TRANSACTION(email_is_whitelistable, &config.pg_master,
		"sql/call_is_whitelistable.sql", NULL,
		parameters, FACTORY_RES_SHORT_FORM);
