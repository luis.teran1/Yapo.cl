#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <time.h>

#include "trans.h"
#include "util.h"
#include <ctemplates.h>
#include "validators.h"
#include <bconf.h>
#include <event.h>
#include "network.h"
#include <unistd.h>
#include <errno.h>

#define SETCONF_DONE          0
#define SETCONF_ERROR         1
#define SETCONF_INIT          2
#define SETCONF_GETDELETE     3
#define SETCONF_DELETEFILES   4
#define SETCONF_SET           5
#define SETCONF_HANDLE        6
#define SETCONF_HANDLE_COMMIT 7

extern struct bconf_node *bconf_root;

struct setconf_state {
	int state;
	struct cmd *cs;
	struct bpapi ba;
	int trhosts;
};

struct validator v_setconf[] = {
	VALIDATOR_REGEX("^.+=.+$", REG_EXTENDED, "ERROR_SET_INVALID"),
	{0}
};
ADD_VALIDATOR(v_setconf);

struct validator v_delconf[] = {
	VALIDATOR_REGEX("^[^=]+$", REG_EXTENDED, "ERROR_DEL_INVALID"),
	{0}
};
ADD_VALIDATOR(v_delconf);

static struct c_param parameters[] = {
	{"token;batch", P_XOR, "v_token:config;v_bool"},
	{"remote_addr", P_REQUIRED, "v_ip"},
	{"remote_browser", 0, "v_remote_browser"},
	{"set", P_MULTI, "v_setconf"},
	{"set_filedata", P_MULTI, "v_blob"},
	{"del", P_MULTI, "v_delconf"},
	{NULL, 0}
};

static const char *setconf_fsm(void *, struct sql_worker *, const char *);


/*****************************************************************************
 * setconf_done
 * Exit function.
 **********/
static void
setconf_done(void *v) {
	struct setconf_state *state = v;
	struct cmd *cs = state->cs;

	bpapi_output_free(&state->ba);
	bpapi_simplevars_free(&state->ba);
	bpapi_vtree_free(&state->ba);

	free(state);
	cmd_done(cs);
}

static void setconf_imgcmd_cb(struct davdb *ds) {
	const char *error = NULL;

	if (ds->result / 100 != 2) {
		/* XXX If result is 404, assume a failed delete, which we ignore. */
		if (ds->result == 404)
			transaction_logprintf(ds->cs->ts, D_WARNING, "Attempt to delete missing file");
		else
			error = "ERROR_COMMUNICATION";
	}
	setconf_fsm(ds->data, NULL, error);
	free(ds);
}

/*****************************************************************************
 * bind_insert
 * Initialise template API before a call to sql_worker_template_query().
 **********/
static int
bind_insert(struct setconf_state *state) {
	struct cmd_param *cp;
	struct cmd_param *fp;
	const char *ep;
	struct bpapi *ba = &state->ba;
	struct davdb *ds = NULL;
	int pending = 0;
	const char *host;
	const char *filename;
	char *hostname;

	fp = cmd_getparam(state->cs, "set_filedata");
	for (cp = cmd_getparam(state->cs, "set"); cp; cp = cmd_nextparam(state->cs, cp)) {
		ep = strchr(cp->value, '=');
		if (!ep)
			continue;
		if (strncmp(ep + 1, "file://", 7) == 0) {
			host = ep + 8;
			filename = strchr(host, '/');

			if (fp && filename) {
				if (!ds) {
					ds = zmalloc(sizeof(*ds));
					ds->cb = setconf_imgcmd_cb;
					ds->command = "PUT";
					ds->cs = state->cs;
					ds->data = state;
				}
				xasprintf(&hostname, "%.*s", (int)(filename++ - host), host);
				ds->directory = bconf_value(bconf_vget(bconf_root, "*", "setconf", hostname, "dir", NULL));
				free(hostname);
				if (!ds->directory) {
					state->cs->status = "TRANS_ERROR";
					state->cs->error = "ERROR_FILE_INVALID_HOST";
					return pending;
				}
				ds->image = fp->value;
				ds->image_size = fp->value_size;
				ds->filename = filename;
				davdb_execute(ds);
				pending++;
				fp = cmd_nextparam(state->cs, fp);
			} else {
				/* Error */
				state->cs->status = "TRANS_ERROR";
				state->cs->error = "ERROR_FILE_DATA_MISSING";
				return pending;
			}
		}
		bpapi_insert_maxsize(ba, "setkey", cp->value, ep - cp->value);
		bpapi_insert(ba, "setvalue", ep + 1);
	}
	for (cp = cmd_getparam(state->cs, "del"); cp; cp = cmd_nextparam(state->cs, cp)) {
		bpapi_insert(ba, "delkey", cp->value);
	}

	if (cmd_haskey(state->cs, "token"))
		bpapi_insert(ba, "token", cmd_getval(state->cs, "token"));
	return pending;
}

/*****************************************************************************
 * setconf_fsm
 * State machine
 **********/
static const char *
setconf_fsm(void *v, struct sql_worker *worker, const char *errstr) {
	struct setconf_state *state = v;
	struct cmd *cs = state->cs;
	int pending_dav = 0;

	transaction_logprintf(cs->ts, D_DEBUG, "setconf_fsm(%p, %p, %p), state=%d", 
                              v, worker, errstr, state->state);

	/* 
	 * General error handling for db errors.
	 */
	if (errstr) {
                transaction_logprintf(cs->ts, D_ERROR, "Statement failed (%d, %s)", 
                                      state->state,
                                      errstr);
                cs->status = "TRANS_DATABASE_ERROR";
                cs->message = xstrdup(errstr);
		state->state = SETCONF_ERROR;
                if (worker)
			return "ROLLBACK";
        }

	while (1) {
		switch (state->state) {
		case SETCONF_INIT:
			/* next state */
			state->state = SETCONF_GETDELETE;

			/* Initialise the templateparse structure */
			BPAPI_INIT(&state->ba, NULL, pgsql);
			pending_dav = bind_insert(state);
			if (cs->error)
				state->state = SETCONF_ERROR;
			if (pending_dav)
				return NULL;
			continue;

		case SETCONF_GETDELETE:
			if (!cmd_haskey(cs, "del") && !cmd_haskey(cs, "set")) {
				state->state = SETCONF_SET;
				continue;
			}
			/* next state */
			state->state = SETCONF_DELETEFILES;

			/* Register SQL query with our setconf_fsm() as callback function */
			sql_worker_template_query(&config.pg_master,
						  "sql/setconf_getdelete.sql", &state->ba, 
						  setconf_fsm, state, cs->ts->log_string);

			/* Return to libevent */
			return NULL;

		case SETCONF_DELETEFILES:
		{
			struct davdb *ds = NULL;

			while (worker->next_row(worker)) {
				const char *dir;

				if (worker->is_null(worker, 0) ||
				    worker->is_null(worker, 1))
					continue;

				dir = bconf_value(bconf_vget(bconf_root, "*", "setconf", worker->get_value(worker, 0), "dir", NULL));
				if (!dir)
					continue;
				if (!ds) {
					ds = zmalloc(sizeof(*ds));
					ds->cb = setconf_imgcmd_cb;
					ds->command = "DELETE";
					ds->cs = state->cs;
					ds->data = state;
				}
				ds->directory = dir;
				ds->filename = worker->get_value(worker, 1);
				davdb_execute(ds);
			}

			state->state = SETCONF_SET;
			if (ds)
				return NULL;
			continue;
		}

		case SETCONF_SET:
			/* next state */
			state->state = SETCONF_HANDLE;

			/* Register SQL query with our setconf_fsm() as callback function */
			sql_worker_template_query_flags(&config.pg_master, SQLW_FINAL_CALLBACK,
							"sql/setconf.sql", &state->ba, 
							setconf_fsm, state, cs->ts->log_string);

			/* Return to libevent */
			return NULL;

		case SETCONF_HANDLE:
			if (worker->sw_state == RESULT_DONE) {
				state->state = SETCONF_HANDLE_COMMIT;
				return "COMMIT";
			}

                        return NULL;

		case SETCONF_HANDLE_COMMIT:
			state->state = SETCONF_DONE;
			return NULL;

		case SETCONF_DONE:
			setconf_done(state);
			return NULL;

		case SETCONF_ERROR:
			transaction_logprintf(cs->ts, D_ERROR, "Error state");
			setconf_done(state);
			return NULL;

		default:
			/* MAJOR STATE ERROR */
			cs->status = "TRANS_ERROR";
			transaction_logprintf(cs->ts, D_ERROR, "Unknown state (%d)", 
					      state->state);
			return NULL;

		}
	}
}

/*****************************************************************************
 * setconf
 * Main function that initialise a state-structure and calls the state machine.
 **********/
static void
setconf(struct cmd *cs) {
	transaction_logprintf(cs->ts, D_DEBUG, "Start Setconf transaction");
	struct setconf_state *state;
	state = zmalloc(sizeof(*state));
	state->cs = cs;
	state->state = SETCONF_INIT;

	setconf_fsm(state, NULL, NULL);
}

ADD_COMMAND(setconf,     /* Name of command */
		NULL,
            setconf,     /* Main function name */
            parameters,  /* Parameters struct */
            "Setconf command");/* Command description */
