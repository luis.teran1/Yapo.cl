#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <time.h>

#include "trans.h"
#include "util.h"
#include "validators.h"
#include "bpapi.h"
#include "ctemplates.h"

#define DELETEADMIN_DONE 0
#define DELETEADMIN_ERROR 1
#define DELETEADMIN_INIT 2
#define DELETEADMIN_CHECK_RESULT 4 

struct deleteadmin_state {
	int state;
	struct cmd *cs;
	char *sql;
};

static void deleteadmin_done(void *v) {
	struct deleteadmin_state *state = v;
	struct cmd *cs = state->cs;

	free(state);
	cmd_done(cs);
}

static const char *
deleteadmin_fsm(void *v, struct sql_worker *worker, const char *errstr) {
	struct deleteadmin_state *state = v;
	struct cmd *cs = state->cs;
	struct bpapi ba;

	transaction_logprintf(cs->ts, D_DEBUG, "deleteadmin fsm");

	if (state->sql)
		free(state->sql);

	if (state->state == DELETEADMIN_ERROR) {
		deleteadmin_done(state);
		return NULL;
	}

	/* 
	 * General error handling for db errors.
	 */
	if (errstr) {
                transaction_logprintf(cs->ts, D_ERROR, "Statement failed (%d, %s)", 
                                      state->state,
                                      errstr);
                cs->status = "TRANS_DATABASE_ERROR";
                cs->message = xstrdup(errstr);
		state->state = DELETEADMIN_ERROR;
		deleteadmin_done(state);
		return NULL;
        }

	while (state->state != DELETEADMIN_DONE) {
		switch(state->state) {
		case DELETEADMIN_INIT:
			transaction_logprintf(cs->ts, D_DEBUG, "Deleteadmin check user");                       

			BPAPI_INIT(&ba, NULL, pgsql);
			bpapi_insert(&ba, "admin_id", cmd_getval(cs, "admin_id"));

			sql_worker_template_query(&config.pg_master, "sql/deleteadmin.sql", &ba,
					deleteadmin_fsm, state, cs->ts->log_string);

			state->state = DELETEADMIN_CHECK_RESULT;
			bpapi_output_free(&ba);
			bpapi_simplevars_free(&ba);
			bpapi_vtree_free(&ba);

			return NULL;

		case DELETEADMIN_CHECK_RESULT:
			state->state = DELETEADMIN_DONE;
			continue;
		}
	}
                
        deleteadmin_done(state);
        return NULL;		
}

static void
deleteadmin(struct cmd *cs) {
	struct deleteadmin_state *state;

	transaction_logprintf(cs->ts, D_DEBUG, "Deleteadmin command started");
	state = zmalloc(sizeof(*state));

	state->cs = cs;
	state->state = DELETEADMIN_INIT;

	deleteadmin_fsm(state, NULL, NULL);
}

static struct c_param deleteadmin_parameters[] = {
	{"token",		P_REQUIRED,	"v_token:admin"},
	{"admin_id", 		P_REQUIRED,	"v_integer"},
	{"remote_addr", 	0,  	"v_ip"},
	{"remote_browser", 	0,  	"v_remote_browser"},
	{NULL, 0, NULL}
};


ADD_COMMAND(deleteadmin, NULL, deleteadmin, deleteadmin_parameters, NULL);

