#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <time.h>

#include "trans.h"
#include "util.h"
#include <ctemplates.h>
#include "validators.h"
#include <bconf.h>
#include <event.h>

#define UNDO_DELETEAD_DONE  0
#define UNDO_DELETEAD_ERROR 1
#define UNDO_DELETEAD_INIT  2
#define UNDO_DELETEAD_CHECK 3
#define UNDO_DELETEAD_SENDMAIL 4

struct undo_deletead_state {
	int state;
	char *ad_id;
        char *sql;
	struct cmd *cs;
        struct event timeout_event;
};

static struct c_param parameters[] = {
	{"ad_id",  P_REQUIRED, "v_id"},
	{"token",		P_REQUIRED, 	    "v_token:adminad.edit_ad"},
	{"remote_addr", 	0,  			"v_ip"},
	{"remote_browser", 	0,  			"v_remote_browser"},
	{NULL, 0}
};

static const char *undo_deletead_fsm(void *, struct sql_worker *, const char *);


/*****************************************************************************
 * send_mail 
 * Send apologize mail. 
 **********/
//static int
//send_mail(struct undo_deletead_state *state) {
//	struct internal_cmd *cmd = zmalloc(sizeof (*cmd));
//	char *mailcmd;
//
//	transaction_logprintf(state->cs->ts, D_DEBUG, "Creating email to send");
//
//	/* Create the mail command */
//        xasprintf(&mailcmd,
//		  "cmd:admail\n"
//		  "commit:1\n"
//		  "ad_id:%s\n"
//		  "mail_type:undo_deleted\n",
//		  cmd_getval(state->cs, "ad_id")
//		);
//	cmd->cmd_string = mailcmd;
//
//	transaction_logprintf(state->cs->ts, D_DEBUG, "Send apologize mail now");
//
//        return transaction_internal(cmd);
//}


/*****************************************************************************
 * undo_deletead_done
 * Exit function.
 **********/
static void
undo_deletead_done(void *v) {
	struct undo_deletead_state *state = v;
	struct cmd *cs = state->cs;
	free(state);
	cmd_done(cs);
}

/*****************************************************************************
 * bind_insert
 * Initialise template API before a call to sql_worker_template_query().
 **********/
static void
bind_insert(struct bpapi *ba, struct undo_deletead_state *state) {
	bpapi_insert(ba, "ad_id", cmd_getval(state->cs, "ad_id"));
	bpapi_insert(ba, "token", cmd_getval(state->cs, "token"));
}

/*****************************************************************************
 * undo_deletead_fsm
 * State machine
 **********/
static const char *
undo_deletead_fsm(void *v, struct sql_worker *worker, const char *errstr) {
	struct undo_deletead_state *state = v;
	struct cmd *cs = state->cs;
        struct bpapi ba;

        /*
         * Clean up pending data
         */
	if (state->sql) {
		free(state->sql);
		state->sql = NULL;
	}

	/* 
	 * General error handling for db errors.
	 */
	if (errstr) {
                transaction_logprintf(cs->ts, D_ERROR, "undo_deletead_fsm statement failed (%d, %s)", 
                                      state->state,
                                      errstr);
                cs->status = "TRANS_DATABASE_ERROR";
                cs->message = xstrdup(errstr);
		state->state = UNDO_DELETEAD_ERROR;
        }

	while (1) {
		transaction_logprintf(cs->ts, D_DEBUG, "undo_deletead_fsm(%p, %p, %p), state=%d", 
				      v, worker, errstr, state->state);

		switch (state->state) {
		case UNDO_DELETEAD_INIT:
			/* next state */
			state->state = UNDO_DELETEAD_CHECK;

			/* Initialise the templateparse structure */
			BPAPI_INIT(&ba, NULL, pgsql);
			bind_insert(&ba, state);

			/* Register SQL query with our undo_deletead_fsm() as callback function */
			/* Make sure to switch to slave if read-only query. */
			sql_worker_template_query(&config.pg_master, 
						  "sql/undo_deletead.sql", &ba, 
						  undo_deletead_fsm, state, cs->ts->log_string);

			/* Clean-up */
			bpapi_output_free(&ba);
			bpapi_simplevars_free(&ba);
			bpapi_vtree_free(&ba);

			/* Return to libevent */
			return NULL;

                case UNDO_DELETEAD_CHECK:
		{
			int status;

			/* Check for fatal errors */
			if (!worker->next_row(worker)) {
				transaction_printf(cs->ts, "error: no rows\n");
				state->state = UNDO_DELETEAD_ERROR;
				continue;
			}

			/* Check status */	
			status = atoi(worker->get_value_byname(worker, "o_status"));
			transaction_logprintf(cs->ts, D_INFO, "status:%d", status);
			if (status == 1) {
				transaction_printf(cs->ts, "ad_id:ERROR_AD_NOT_FOUND\n");
				state->state = UNDO_DELETEAD_ERROR;
				continue;
			} else if (status == 2) {
				transaction_printf(cs->ts, "ad_id:ERROR_AD_NOT_DELETED\n");
				state->state = UNDO_DELETEAD_ERROR;
				continue;
			}

			/* next state */
			state->state = UNDO_DELETEAD_SENDMAIL;

			continue;
		}

                case UNDO_DELETEAD_SENDMAIL:

			/* next state */
                        state->state = UNDO_DELETEAD_DONE;

			/* Send the mail */
		//	send_mail(state);

			continue;
                        
		case UNDO_DELETEAD_DONE:
			undo_deletead_done(state);
			return NULL;

		case UNDO_DELETEAD_ERROR:
			transaction_logprintf(cs->ts, D_ERROR, "Error state");
			cs->status = "TRANS_ERROR";
			undo_deletead_done(state);
			return NULL;

		default:
			/* MAJOR STATE ERROR */
			cs->status = "TRANS_ERROR";
			transaction_logprintf(cs->ts, D_ERROR, "Unknown state (%d)", 
					      state->state);
			return NULL;

		}
	}
}

/*****************************************************************************
 * undo_deletead
 * Main function that initialise a state-structure and calls the state machine.
 **********/
static void
undo_deletead(struct cmd *cs) {
	transaction_logprintf(cs->ts, D_DEBUG, "Start undo_deletead transaction");
	struct undo_deletead_state *state;
	state = zmalloc(sizeof(*state));
	state->cs = cs;
	state->state = UNDO_DELETEAD_INIT;

	undo_deletead_fsm(state, NULL, NULL);
}

ADD_COMMAND(undo_deletead,     /* Name of command */
            NULL,/* Called during the initalisation phase */
            undo_deletead,     /* Main function name */
            parameters,  /* Parameters struct */
            "Undo an ad deletion and send apologize mail to user");/* Command description */
