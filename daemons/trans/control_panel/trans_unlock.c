#include <stdio.h>
#include <time.h>

#include "command.h"
#include "trans.h"
#include "util.h"
#include "bconf.h"
#include "validators.h"
#include "trans_mail.h"
#include "db.h"
#include <ctemplates.h>

extern struct bconf_node *bconf_root;

#define UNLOCK_DONE	0
#define UNLOCK_ERROR	1
#define UNLOCK_INIT	2
#define UNLOCK_BEGIN	3
#define UNLOCK_COMMIT	4

struct unlock_state {
	int state;
	struct cmd *cs;
	struct sql_worker *sw;
};


static struct c_param parameters[] = {
	{"token",		P_REQUIRED,	  "v_token:adqueue"},
	{"ad_id", 		0,  	  "v_ad_id"},
	{"action_id", 		0,  	  "v_action_id"},
	{"remote_addr", 	0,  	  "v_ip"},
	{"remote_browser", 	0,  	  "v_remote_browser"},
	{"new_queue", 	        0,  	  "v_string_isprint"},
	{NULL}
};

static const char * unlock_fsm(void *, struct sql_worker *, const char *);

static void unlock_done(struct unlock_state *state) {
	struct cmd *cs = state->cs;

	free(state);
	cmd_done(cs);
}

static const char *
unlock_fsm(void *v, struct sql_worker *worker, const char *errstr) {
	struct unlock_state *state = v;
	struct cmd *cs = state->cs;
	struct bpapi ba;

	/*
	 * General error handling for db errors.
	 */
	if (errstr) {
		if (strstr(errstr, "ERROR_INVALID_STATE")) {
			cs->status = "TRANS_ERROR";
			cs->error = "ERROR_INVALID_STATE";
		} else if (strstr(errstr, "ERROR_NO_SUCH_ACTION")) {
			cs->status = "TRANS_ERROR";
			cs->error = "ERROR_NO_SUCH_ACTION";
		} else {
			cs->status = "TRANS_DATABASE_ERROR";
			cs->message = xstrdup(errstr);
		}
		state->state = UNLOCK_ERROR;
	}

	while (state->state != UNLOCK_DONE) {
		switch (state->state) {
			case UNLOCK_INIT:
				BPAPI_INIT(&ba, NULL, pgsql);

				bpapi_insert(&ba, "ad_id", cmd_getval(cs, "ad_id"));
				bpapi_insert(&ba, "action_id", cmd_getval(cs, "action_id"));
				bpapi_insert(&ba, "token", cmd_getval(cs, "token"));
				bpapi_insert(&ba, "remote_addr", cmd_getval(cs, "remote_addr"));
				if (cmd_haskey(cs, "new_queue")) {
					bpapi_insert(&ba, "new_queue", cmd_getval(cs, "new_queue"));
				}
				state->state = UNLOCK_DONE;
				sql_worker_template_query(&config.pg_master,
						"sql/call_unlock_ad.sql", &ba,
						unlock_fsm, state, cs->ts->log_string);
				bpapi_output_free(&ba);
				bpapi_simplevars_free(&ba);
				bpapi_vtree_free(&ba);
				return NULL;

			case UNLOCK_ERROR:
				unlock_done(state);
				return NULL;

		}
	}

	unlock_done(state);
	return NULL;
}

static void
unlock(struct cmd *cs) {
	struct unlock_state *state = zmalloc(sizeof (*state));
	state->cs = cs;
	state->state = UNLOCK_INIT;
	unlock_fsm(state, NULL, NULL);
}

ADD_COMMAND(unlock, NULL, unlock, parameters, NULL);
