#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <time.h>

#include "trans.h"
#include "util.h"
#include <ctemplates.h>
#include "validators.h"
#include <bconf.h>
#include <event.h>

#define SPOOL_REDIR_DONE  0
#define SPOOL_REDIR_ERROR 1
#define SPOOL_REDIR_INIT  2
#define SPOOL_REDIR_BEGIN 3
#define SPOOL_REDIR_DELETE 4
#define SPOOL_REDIR_INSERT 5

struct spool_redir_state {
	int state;
	struct cmd *cs;
};

static struct c_param parameters[] = {
	{"code", P_REQUIRED, "v_redir_code"},
	{"log_date", P_REQUIRED, "v_date_long"},
	{"first_redir", P_REQUIRED, "v_integer"},
	{"repeated_redir", P_REQUIRED, "v_integer"},
	{"inserted_ad", P_REQUIRED, "v_integer"},
	{"paid_ad", P_REQUIRED, "v_integer"},
	{"verified_ad", P_REQUIRED, "v_integer"},
	{"approved_ad", P_REQUIRED, "v_integer"},
	{"ad_reply", P_REQUIRED, "v_integer"},
	{NULL, 0}
};

struct validator v_redir_code[] = {
	VALIDATOR_REGEX("^["BASE64"-]+$", REG_EXTENDED, "ERROR_CODE_INVALID"),
	//VALIDATOR_REGEX("^[A-Za-z0-9_-]+$", REG_EXTENDED, "ERROR_CODE_INVALID"),
	{0}
};
ADD_VALIDATOR(v_redir_code);

static const char *spool_redir_fsm(void *, struct sql_worker *, const char *);


/*****************************************************************************
 * spool_redir_done
 * Exit function.
 **********/
static void
spool_redir_done(void *v) {
	struct spool_redir_state *state = v;
	struct cmd *cs = state->cs;
	free(state);
	cmd_done(cs);
}

/*****************************************************************************
 * bind_insert
 * Initialise template API before a call to sql_worker_template_query().
 **********/
static void
bind_insert(struct bpapi *ba, struct spool_redir_state *state) {
	struct c_param *param;

	for (param = parameters; param->name; param++) {
		if (cmd_haskey(state->cs, param->name))
			bpapi_insert(ba, param->name, cmd_getval(state->cs, param->name));
	}
}

/*****************************************************************************
 * spool_redir_fsm
 * State machine
 **********/
static const char *
spool_redir_fsm(void *v, struct sql_worker *worker, const char *errstr) {
	struct spool_redir_state *state = v;
	struct cmd *cs = state->cs;
        struct bpapi ba;

	/*
	 * General error handling for db errors.
	 */
	if (errstr) {
                transaction_logprintf(cs->ts, D_ERROR, "spool_redir_fsm statement failed (%d, %s)",
                                      state->state,
                                      errstr);
                cs->status = "TRANS_DATABASE_ERROR";
                cs->message = xstrdup(errstr);
		state->state = SPOOL_REDIR_ERROR;
                if (worker) {
			/* Turn of final callback, if set. */
			sql_worker_set_wflags(worker, 0);
			return "ROLLBACK";
		}
        }

	while (1) {
		transaction_logprintf(cs->ts, D_DEBUG, "spool_redir_fsm(%p, %p, %p), state=%d",
				      v, worker, errstr, state->state);

		switch (state->state) {
		case SPOOL_REDIR_INIT:
			/* next state */
			state->state = SPOOL_REDIR_BEGIN;

			/* Initialise the templateparse structure */
			BPAPI_INIT(&ba, NULL, pgsql);
			bind_insert(&ba, state);

			/* Register SQL query with our spool_redir_fsm() as callback function */
			/* Make sure to switch to slave if read-only query. */
			sql_worker_template_query(&config.pg_master,
						  "sql/spool_redir.sql", &ba,
						  spool_redir_fsm, state, cs->ts->log_string);

			/* Clean-up */
			bpapi_output_free(&ba);
			bpapi_simplevars_free(&ba);
			bpapi_vtree_free(&ba);

			/* Return to libevent */
			return NULL;

		case SPOOL_REDIR_BEGIN:
                        state->state = SPOOL_REDIR_DELETE;
                        return NULL;
                case SPOOL_REDIR_DELETE:
			if (worker->affected_rows(worker) > 0)
				transaction_printf(cs->ts, "deleted:1\n");
                        state->state = SPOOL_REDIR_INSERT;
                        return NULL;
                case SPOOL_REDIR_INSERT:
			transaction_printf(cs->ts, "inserted:%d\n", worker->affected_rows(worker));
                        state->state = SPOOL_REDIR_DONE;
                        return "COMMIT";

		case SPOOL_REDIR_DONE:
			spool_redir_done(state);
			return NULL;

		case SPOOL_REDIR_ERROR:
			transaction_logprintf(cs->ts, D_ERROR, "Error state");
			spool_redir_done(state);
			return NULL;

		default:
			/* MAJOR STATE ERROR */
			cs->status = "TRANS_ERROR";
			transaction_logprintf(cs->ts, D_ERROR, "Unknown state (%d)",
					      state->state);
			return NULL;

		}
	}
}

/*****************************************************************************
 * spool_redir
 * Main function that initialise a state-structure and calls the state machine.
 **********/
static void
spool_redir(struct cmd *cs) {
	transaction_logprintf(cs->ts, D_DEBUG, "Start Spool redir transaction");
	struct spool_redir_state *state;
	state = zmalloc(sizeof(*state));
	state->cs = cs;
	state->state = SPOOL_REDIR_INIT;

	spool_redir_fsm(state, NULL, NULL);
}

ADD_COMMAND(spool_redir,     /* Name of command */
		NULL,
            spool_redir,     /* Main function name */
            parameters,  /* Parameters struct */
            "Spool redir command");/* Command description */
