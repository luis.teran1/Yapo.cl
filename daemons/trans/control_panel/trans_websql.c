#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <time.h>

#include "trans.h"
#include "util.h"
#include "strl.h"
#include "trans_mail.h"
#include <ctemplates.h>
#include "validators.h"
#include <bconf.h>
#include <event.h>
#include "bpapi.h"

enum websql_state_val {
	WEBSQL_DONE,
	WEBSQL_ERROR,
	WEBSQL_INIT,
	WEBSQL_SET_STATEMENT_TIMEOUT,
	WEBSQL_QUERY,
	WEBSQL_TIMEOUT,
	WEBSQL_RESULT,
	WEBSQL_MAIL_RESULT,
	WEBSQL_MAIL_ERROR,
	WEBSQL_UPDATE_STATS
};

static const char *state_lut[] = {
	"WEBSQL_DONE",
	"WEBSQL_ERROR",
	"WEBSQL_INIT",
	"WEBSQL_SET_STATEMENT_TIMEOUT",
	"WEBSQL_QUERY",
	"WEBSQL_TIMEOUT",
	"WEBSQL_RESULT",
	"WEBSQL_MAIL_RESULT",
	"WEBSQL_MAIL_ERROR",
	"WEBSQL_UPDATE_STATS",
	"INVALID"
};

extern struct bconf_node *bconf_root;

struct websql_state {
	enum websql_state_val state;
	struct cmd *cs;
	struct mail_data *md;
	struct event timeout_event;
	char *recipient;
	char *tmpfile;
};

/*
  Returns true iff the values of the tm struct makes it through mktime without error or the date changing, indicating
  the date was valid.
*/
static int is_valid_tm(struct tm* tm) {
	struct tm tm_new = *tm;
	time_t t = mktime(&tm_new); /* Will update tm if the date is 'off' and/or return -1 on error */

        if (t == -1) {
            return 0;
        }
        if ((tm_new.tm_isdst == tm->tm_isdst) && ((tm->tm_year != tm_new.tm_year) || (tm->tm_mon != tm_new.tm_mon) || (tm->tm_mday != tm_new.tm_mday))) {
            return 0;
        }
        return 1;
}
/*
 * Validators
 */
static int
vf_datetime(struct cmd_param *param, const char* arg) {
	struct tm tm = { 0 };

	if ( (strptime(param->value, "%F %T", &tm) == NULL) || (!is_valid_tm(&tm))) {
		param->error = "ERROR_TIME_FORMAT";
		param->cs->status = "TRANS_ERROR";
	}
	return 0;
}

static int
vf_date(struct cmd_param *param, const char* arg) {
	struct tm tm = { 0 };

	if ( (strptime(param->value, "%F", &tm) == NULL) || (!is_valid_tm(&tm))) {
		param->error = "ERROR_DATE_FORMAT";
		param->cs->status = "TRANS_ERROR";
	}
	return 0;
}

static int
vf_range(struct cmd_param *param, const char* arg) {
	struct tm tm_from = { 0 }, tm_to = { 1 };
	const char * s_from = cmd_getval(param->cs, "timestamp_from");
	const char * s_to = cmd_getval(param->cs, "timestamp_to");
	
	if ( !param->value ) {
		param->error = "ERROR_RANGE_FORMAT";
		param->cs->status = "TRANS_ERROR";
		return -1;
	}

	time_t tt_max = atoi(param->value);

	if ( tt_max < 0 ) {
		param->error = "ERROR_RANGE_FORMAT";
		param->cs->status = "TRANS_ERROR";
		return -2;
	}

	strptime(s_from, "%F %T", &tm_from);
	strptime(s_to, "%F %T", &tm_to);

	time_t tt_from, tt_to;
	tt_from = mktime(&tm_from);
	tt_to = mktime(&tm_to);

	time_t tt_diff = tt_to - tt_from;

	if ( tt_diff > tt_max ) {
		param->error = "ERROR_RANGE_TOO_LARGE";
		param->cs->status = "TRANS_ERROR";
		return -3;
	}
	return 0;
}

static int
vf_websql_query(struct cmd_param *param, const char *arg) {
	const char *query = param->value;
	int i;
	struct bconf_node *query_param;

	/* For dynamic validator creation */
	struct d_param *dparam;
	struct bconf_node *params;
	struct validator *val;

	/* Check if query exists */
	if (bconf_vget(bconf_root, "*.controlpanel.modules.Websql.queries", query, NULL) == NULL) {
		param->error = "ERROR_QUERY_INVALID";
		param->cs->status = "TRANS_ERROR";
		return 1;
	}

	/* Get params for query */
	params = bconf_vget(bconf_root, "*.controlpanel.modules.Websql.queries", query, "params", NULL);
	if (params) {
		for (i = 0; (query_param = bconf_byindex(params, i)); i++ ) {
			const char *regex = bconf_get_string(query_param, "regex");
			const char *validate_datetime = bconf_get_string(query_param, "validate_datetime");
			const char *validate_date = bconf_get_string(query_param, "validate_date");
			const char *validate_range = bconf_get_string(query_param, "validate_timestamp_range");
			const char *param_type = bconf_get_string(query_param, "type");
			dparam = zmalloc(sizeof(*param));
			dparam->name = xstrdup(bconf_key(query_param));
			int val_idx = 0;

			/* Param validators */
			val = zmalloc(sizeof(*val) * 2);
			/* Yes, you can only have one of these at a time */
			if (validate_datetime) {
				val[val_idx] = (struct validator) VALIDATOR_FUNC(vf_datetime);
			} else if (validate_date) {
				val[val_idx] = (struct validator) VALIDATOR_FUNC(vf_date);
			} else if (validate_range) {
				val[val_idx] = (struct validator) VALIDATOR_FUNC(vf_range);
			} else if (regex) {
				val[val_idx] = (struct validator) VALIDATOR_REGEX(regex, REG_EXTENDED|REG_NOSUB, "ERROR_INVALID");
			}
			dparam->validators = val;
			dparam->flags = 0;
			if (!param_type || strcmp(param_type, "checkbox") != 0)
				dparam->flags |= P_REQUIRED;

			cmd_dyn_param(param->cs, dparam);
		}
	}

	return 0;
}


struct validator v_websql_query[] = {
	VALIDATOR_INT(0, -1, "ERROR_QUERY_INVALID"),
	VALIDATOR_FUNC(vf_websql_query),
	{0}
};
ADD_VALIDATOR(v_websql_query);

/*
 * Command parameters
 */

static struct c_param parameters[] = {
	{"query", 	   P_REQUIRED, 		"v_websql_query"},
	{"remote_addr",    P_REQUIRED,  "v_ip"},
	{"remote_browser", 0,  "v_remote_browser"},
	{"recipient", 	   0, 	 	"v_email_not_required"},
	{"token;internal", P_OR, 	"v_token:Websql;v_array:1"},
	{"lang",	   0,		"v_lang"},
	{NULL, 0}
};

static const char *websql_fsm(void *, struct sql_worker *, const char *);


/*****************************************************************************
 * websql_done
 * Exit function.
 **********/
static void
websql_done(void *v) {
	struct websql_state *state = v;
	struct cmd *cs = state->cs;
	evtimer_del(&state->timeout_event);
	if (state->recipient)
		free(state->recipient);
	if (state->tmpfile) {
		unlink(state->tmpfile);
		free(state->tmpfile);
	}
	free(state);
	cmd_done(cs);
}

static void
websqlmail_done_cb(struct mail_data *md, void *v, int status) {
	struct websql_state *state = v;
	struct cmd *cs = state->cs;

        if (status != 0) {
                cs->status = "TRANS_MAIL_ERROR";
        }

	websql_fsm(v, NULL, NULL);
}


/*****************************************************************************
 * bind_insert
 * Initialise template API before a call to sql_worker_template_query().
 **********/
static const char *
bind_insert(struct bpapi *ba, struct websql_state *state) {
	struct c_param *param;
	char *token;
	int i;
	const char *template;

	/* Params */
	for (param = parameters; param->name; param++) {
		if (cmd_haskey(state->cs, param->name))
			bpapi_insert(ba, param->name, cmd_getval(state->cs, param->name));
	}

	/* Dynamic params */
	for (i = 0; i < state->cs->ndyn_params; i++) {
		struct d_param *dparam = state->cs->dyn_params[i];
		if (cmd_haskey(state->cs, dparam->name)) {
			bpapi_insert(ba, "params_id", dparam->name);
			bpapi_insert(ba, "params_value", cmd_getval(state->cs, dparam->name));
			bpapi_insert(ba, dparam->name, cmd_getval(state->cs, dparam->name));
		}
	}

	template = bconf_value(bconf_vget(bconf_root, "*.controlpanel.modules.Websql.queries", cmd_getval(state->cs, "query"), "template", NULL));
	if (!template) {
		const char *query_id = bconf_value(bconf_vget(bconf_root, "*.controlpanel.modules.Websql.queries", cmd_getval(state->cs, "query"), "query", NULL));
		if (query_id && *query_id) {
			char *query = xstrdup(query_id);
			/* Query, split with % */
			while ( (token = strsep(&query, "%")) != NULL ) {
				bpapi_insert(ba, "query", token);
			}

			free(query);
		} else {
			transaction_logprintf(state->cs->ts, D_ERROR, "(CRIT) Neither template nor bconf query found for websql %s", cmd_getval(state->cs, "query"));
			return NULL;
		}

		template = "sql/websql.sql";
	}

	/* Modulus */
	bpapi_insert(ba, "mod", "0");
	bpapi_insert(ba, "mod", "1");

	return template;
}

static void
websql_timeout_cb(int i, short s, void *v) {
        struct websql_state *state = v;

	state->state = WEBSQL_TIMEOUT;
        websql_fsm(state, NULL, NULL);
}

static const char *
websql_fsm(void *v, struct sql_worker *worker, const char *errstr) {
	struct websql_state *state = v;
	struct cmd *cs = state->cs;
        struct bpapi ba;

	if (errstr) { // General error handling for db errors.
		if (cs->ts)
			transaction_logprintf(cs->ts, D_ERROR, "websql_fsm statement failed (%d, %s)",
					      state->state,
					      errstr);
                cs->status = "TRANS_DATABASE_ERROR";
                cs->message = xstrdup(errstr);
		if (state->state == WEBSQL_MAIL_RESULT)
			state->state = WEBSQL_MAIL_ERROR;
		else
			state->state = WEBSQL_ERROR;
        }

	while (1) {
		/* If it has been detached (cs->ts == NULL) - Zane */
		if (cs->ts)
			transaction_logprintf(cs->ts, D_DEBUG, "websql_fsm(%p, %p, %p), state = [%s]",
				v, worker, errstr, state_lut[state->state]);

		switch (state->state) {
		case WEBSQL_INIT:
			state->state = WEBSQL_SET_STATEMENT_TIMEOUT;
			if (!cmd_haskey(cs, "recipient") && !cmd_haskey(cs, "internal")) {
				BPAPI_INIT(&ba, NULL, pgsql);
				bpapi_insert(&ba, "token", cmd_getval(cs, "token"));
				if (cmd_haskey(cs, "lang"))
					bpapi_insert(&ba, "b__lang", cmd_getval(cs, "lang"));
				sql_worker_template_query(&config.pg_slave, "sql/get_email_by_token.sql", &ba,
						websql_fsm, state, cs->ts->log_string);
				return NULL;
			}
			continue;

		case WEBSQL_SET_STATEMENT_TIMEOUT:
			{
				int sto = bconf_get_int(bconf_root, "*.controlpanel.modules.Websql.statement_timeout");

				if (worker) {
					struct timeval tv;

					if (worker->next_row(worker) && !worker->is_null(worker, 0)) {
						state->recipient = xstrdup(worker->get_value(worker, 0));

						tv.tv_sec = bconf_get_int(bconf_root, "*.controlpanel.modules.Websql.timeout");
						tv.tv_usec = 0;

						if (tv.tv_sec) {
							evtimer_set(&state->timeout_event, websql_timeout_cb, state);
							evtimer_add(&state->timeout_event, &tv);
						}
					}
				}

				state->state = WEBSQL_QUERY;
				if (sto) {
					BPAPI_INIT(&ba, NULL, pgsql);

					bpapi_set_int(&ba, "timeout", sto);
					if (cmd_haskey(cs, "lang"))
						bpapi_insert(&ba, "b__lang", cmd_getval(cs, "lang"));
					sql_worker_template_query_flags(&config.pg_slave, SQLW_DONT_REUSE,
							"sql/statement_timeout.sql", &ba,
							websql_fsm, state, cs->ts->log_string);
					return NULL;
				}
			}
			continue;

		case WEBSQL_QUERY:
		{
			const char *template;

			/* next state */
			if (!cmd_haskey(cs, "recipient")) {
				state->state = WEBSQL_RESULT;
			} else {
				state->state = WEBSQL_MAIL_RESULT;
			}

			/* Initialise the templateparse structure */
			/* XXX app ("index") should come from bconf */
			BPAPI_INIT_APP(&ba, host_bconf, "index", buf, pgsql);
			template = bind_insert(&ba, state);
			if (!template) {
				state->state = WEBSQL_ERROR;
				continue;
			}
			if (cmd_haskey(cs, "lang"))
				bpapi_insert(&ba, "b__lang", cmd_getval(cs, "lang"));

			if (worker) {
				sql_worker_template_newquery(worker, template, &ba);
			} else {
				sql_worker_template_query(&config.pg_slave,
							  template, &ba,
							  websql_fsm, state, cs->ts->log_string);
			}

			/* Clean-up */
			bpapi_output_free(&ba);
			bpapi_simplevars_free(&ba);
			bpapi_vtree_free(&ba);

			/* Return to libevent */
			return NULL;
		}

		case WEBSQL_TIMEOUT:
			evtimer_del(&state->timeout_event);
			memset(&state->timeout_event, 0, sizeof(state->timeout_event));
			transaction_logprintf(cs->ts, D_INFO, "Websql transaction timed out");
			transaction_printf(cs->ts, "timeout:%s\n", state->recipient);
			cmd_detach(cs);
			state->state = WEBSQL_MAIL_RESULT;
			return NULL;

		case WEBSQL_RESULT: {
			int i, j;
                        state->state = WEBSQL_UPDATE_STATS;

			i = 0;
                        while (worker->next_row(worker)) {
				for (j = 0; j < worker->fields(worker); j++) {
					transaction_printf(cs->ts, "result.%d.%s:%s\n",
							   i,
							   worker->field_name(worker, j),
							   worker->get_value(worker, j));
				}
				i++;
                        }

			continue;
		}


		case WEBSQL_MAIL_RESULT: {
			int j;
			struct mail_data *md;
			struct buf_string *buf;
			int fd;
			char *file;
			int k;

			evtimer_del(&state->timeout_event);
			memset(&state->timeout_event, 0, sizeof(state->timeout_event));
			state->state = WEBSQL_UPDATE_STATS;

			md = zmalloc(sizeof(*md));
			md->command = state;
			md->callback = websqlmail_done_cb;
			if (cs->ts)
				md->log_string = cs->ts->log_string;
			else
				md->log_string = "WEBSQL";
			md->bconf_app = "controlpanel";

			BPAPI_INIT_APP(&ba, host_bconf, "controlpanel", buf, pgsql);
			buf = BPAPI_OUTPUT_DATA(&ba);
			k = 0;
			while (worker->next_row(worker)) {
				char valbuf[20];
				snprintf(valbuf, sizeof(valbuf), "value%d", k++);

				int cols = worker->fields(worker);
				for (j = 0; j < cols; j++) {
					bpapi_insert(&ba, valbuf, worker->get_value(worker, j));
					mail_insert_param(md, "row", j == 0 ? "1" : "0");
					mail_insert_param(md, "key", worker->field_name(worker, j));
					mail_insert_param(md, "value", worker->get_value(worker, j));
				}
			}
			file = xstrdup(bconf_get_string(bconf_root, "*.controlpanel.modules.Websql.tmpfile"));
			fd = mkstemp(file);
			if (fd >= 0) {
				char *newfile;

				bpapi_insert(&ba, "attach", "1");
				bpapi_insert(&ba, "query", cmd_getval(cs, "query"));
				bpapi_set_int(&ba, "rows", worker->rows(worker));
				call_template_lang(&ba, "mail/websql.xml", cmd_getval(cs, "lang"), NULL);
				if (buf->buf) {
					if (write(fd, buf->buf, buf->pos) == -1) {
						if (state->cs->ts) { //cmd_detach sets ts = NULL - Zane
							transaction_logprintf(state->cs->ts, D_ERROR, "(CRIT) write() websql attachment");
						} else {
							DPRINTF(D_ERROR, ("(CRIT) write() websql attachment"));
						}
					}
				}
				close(fd);
				xasprintf(&newfile, "%s.xls", file);
				rename(file, newfile);
				free(file);
				state->tmpfile = file = newfile;
			} else
				free(file);
			bpapi_output_free(&ba);
			bpapi_simplevars_free(&ba);
			bpapi_vtree_free(&ba);

			if (cmd_haskey(cs, "recipient"))
				mail_insert_param(md, "recipient", cmd_getval(cs, "recipient"));
			else
				mail_insert_param(md, "recipient", state->recipient);
			if (fd >= 0)
				mail_insert_param(md, "attachment", file);
			mail_insert_param(md, "query", cmd_getval(cs, "query"));
			mail_send(md, "mail/websql.txt", cmd_getval(cs, "lang"));
			return NULL;
		}

		case WEBSQL_MAIL_ERROR:
			{
				struct mail_data *md;
				state->state = WEBSQL_ERROR;

				md = zmalloc(sizeof(*md));
				md->command = state;
				md->callback = websqlmail_done_cb;
				if (cs->ts)
					md->log_string = cs->ts->log_string;
				else
					md->log_string = "WEBSQL";
				md->bconf_app = "controlpanel";

				if (cmd_haskey(cs, "recipient"))
					mail_insert_param(md, "recipient", cmd_getval(cs, "recipient"));
				else
					mail_insert_param(md, "recipient", state->recipient);
				mail_insert_param(md, "query", cmd_getval(cs, "query"));
				mail_insert_param(md, "errstr", errstr);
				mail_send(md, "mail/websql_error.txt", cmd_getval(cs, "lang"));
			}
			return NULL;

		case WEBSQL_UPDATE_STATS:
			state->state = WEBSQL_DONE;
			const char* enabled = bconf_get_string(bconf_root, "*.*.common.Websql.statistics_tracking_enabled");

			if (enabled && *enabled) {
				time_t now = time(NULL);
				char nowstr[sizeof("2008-12-12 12:34:56")];
				struct tm tm;

				localtime_r(&now, &tm);
				strftime(nowstr, sizeof(nowstr), "%F %T", &tm);
				transaction_internal_printf(NULL, NULL, "cmd:update_stats\nstat_name:websql\nstat_name_suffix:_query_%s\nstat_value:1\nstat_time:%s\nis_delta:1\ncommit:1\nend\n", cmd_getval(state->cs, "query"), nowstr);
			}
			continue;

		case WEBSQL_DONE:
			websql_done(state);
			return NULL;

		case WEBSQL_ERROR:
			if (cs->ts)
				transaction_logprintf(cs->ts, D_ERROR, "Error state");
			websql_done(state);
			return NULL;

		default:
			/* MAJOR STATE ERROR */
			cs->status = "TRANS_ERROR";
			if (cs->ts)
				transaction_logprintf(cs->ts, D_ERROR, "Unknown state (%d)", 
						      state->state);
			return NULL;

		}
	}
}

/*****************************************************************************
 * websql
 * Main function that initialise a state-structure and calls the state machine.
 **********/
static void
websql(struct cmd *cs) {
	transaction_logprintf(cs->ts, D_DEBUG, "Start Websql transaction");

	if (cmd_haskey(cs, "recipient") && !cmd_haskey(cs, "internal")) {
		/* Run as an internal transaction instead */
		struct internal_cmd *intcmd = zmalloc(sizeof(*intcmd));
		struct elem *el;
		struct elem *cp;

		TAILQ_INIT(&intcmd->elems);

		TAILQ_FOREACH(el, &cs->ts->ts_elems, el_list) {
			if (strcmp(el->el_key, "end") == 0 || strcmp(el->el_key, "token") == 0)
				continue;

			cp = zmalloc(sizeof(*cp) + strlen(el->el_key) + 1 + el->el_size + 1);
			cp->el_key = (char *)(cp + 1);
			cp->el_size = el->el_size;
			strlcpy(cp->el_key, el->el_key, strlen(el->el_key) + 1);
			if (el->el_value != NULL) {
				cp->el_value = cp->el_key + strlen(el->el_key) + 1;
				memcpy(cp->el_value, el->el_value, el->el_size);
				cp->el_value[el->el_size] = '\0';
			}

			TAILQ_INSERT_TAIL(&intcmd->elems, cp, el_list);
		}

		cp = zmalloc(sizeof(*cp));
		cp->el_key = xstrdup("internal");
		cp->el_value = xstrdup("1");
		cp->el_size = 1;

		TAILQ_INSERT_TAIL(&intcmd->elems, cp, el_list);
		transaction_internal(intcmd);
		cmd_done(cs);
	} else {
		struct websql_state *state;
		state = zmalloc(sizeof(*state));
		state->cs = cs;
		state->state = WEBSQL_INIT;

		websql_fsm(state, NULL, NULL);
	}
}

ADD_COMMAND(websql,     /* Name of command */
		NULL,
            websql,     /* Main function name */
            parameters,  /* Parameters struct */
            "Websql command");/* Command description */
