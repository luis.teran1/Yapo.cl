#include "factory.h"

static struct c_param parameters[] = {
    {"admin_ids",      P_REQUIRED, "v_id_list"},
    {"token",          P_REQUIRED, "v_token"},
    {"remote_addr",    P_OPTIONAL, "v_ip"},
    {"remote_browser", P_OPTIONAL, "v_remote_browser"},
    {NULL, 0}
};

static struct factory_data data = {
    "controlpanel",
    FACTORY_NO_RES_CNTR
};

static const struct factory_action actions[] = {
    FA_SQL(NULL, "pgsql.slave", "sql/get_adminnames.sql", FATRANS_OUT),
    {0}
};

FACTORY_TRANS(get_adminnames, parameters, data, actions);
