#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <time.h>

#include "trans.h"
#include "util.h"
#include "db.h"
#include <ctemplates.h>
#include "validators.h"
#include <bconf.h>
#include <event.h>
#include "similar_text.h"

#define ADQUEUES_DONE  0
#define ADQUEUES_ERROR 1
#define ADQUEUES_INIT  2
#define ADQUEUES_GET_ADS 4
#define ADQUEUES_BEGIN_TRANSACTION 5
#define ADQUEUES_CHECK_TOTAL 6
#define ADQUEUES_LOAD_ADS 7
#define ADQUEUES_NEXT_AD 8
#define ADQUEUES_LOAD_DATA 9
#define ADQUEUES_SHOWAD 10

static const char*
adqueue_state_lut[] = {
	"ADQUEUES_DONE",
	"ADQUEUES_ERROR",
	"ADQUEUES_INIT",
	"",
	"ADQUEUES_GET_ADS",
	"ADQUEUES_BEGIN_TRANSACTION",
	"ADQUEUES_CHECK_TOTAL",
	"ADQUEUES_LOAD_ADS",
	"ADQUEUES_NEXT_AD",
	"ADQUEUES_LOAD_DATA",
	"ADQUEUES_SHOWAD"
};

extern struct bconf_node *bconf_root;
static const char *adqueues_fsm(void *, struct sql_worker *, const char *);

struct adqueues_state {
	int state;
	struct cmd *cs;
	struct sql_worker *sw;
	struct event timeout_event;
	int ads_total;
	int ad_cnt;
	struct action *action;
	struct bpapi ba;
	int lock;
	int use_archive;
	TAILQ_HEAD(,adqueue_entry) adqueue;
	struct timer_instance *ti;
};


/* Local validators */
static const char *
vf_lock_auto_cb(void *v, struct sql_worker *worker, const char *errstr) {
	struct cmd_param *param = (struct cmd_param *)v;
	int autoreview = 0;

	/*
	 * General error handling for db errors.
	 */
	if (errstr) {
		param->cs->status = "TRANS_DATABASE_ERROR";
		param->cs->message = xstrdup(errstr);
		param->cs->pending_validators--;
		param->cs->validator_cb(param->cs);
		return NULL;
	}

	/* Check if autoreview is enabled using both bconf and dyn_bconf */
	if (worker->next_row(worker)) {
		autoreview = atoi(worker->get_value(worker, 0));
	} else {
		autoreview = bconf_get_int(bconf_root, "*.controlpanel.modules.adqueue.settings.autoreview");
	}

	if (autoreview) {
		param->error = "ERROR_LOCK_QUEUE_IS_AUTO";
		param->cs->status = "TRANS_ERROR";
	}

	transaction_logprintf(param->cs->ts, D_DEBUG, "decreasing pending_validators (lock)");
	param->cs->pending_validators--;
	param->cs->validator_cb(param->cs);

	return NULL;
}

static int
vf_lock_auto(struct cmd_param *param, const char *arg) {

	if (atoi(param->value) == 1) {
		const char *queue = cmd_getval(param->cs, "filter_name");

		if (queue && bconf_vget(bconf_root, "*.controlpanel.modules.adqueue.auto", queue, NULL) != NULL) {
			struct bpapi ba;

			BPAPI_INIT(&ba, NULL, pgsql);
			bpapi_insert(&ba, "key", "*.controlpanel.modules.adqueue.settings.autoreview");
			/* Use master for up to date data */
			sql_worker_template_query(&config.pg_master,
						"sql/bconf.sql", &ba,
						vf_lock_auto_cb, param, param->cs->ts->log_string);

			bpapi_output_free(&ba);
			bpapi_simplevars_free(&ba);
			bpapi_vtree_free(&ba);

			transaction_logprintf(param->cs->ts, D_DEBUG, "increasing pending_validators (lock)");
			param->cs->pending_validators++;
			return 0;
		}
	}

	return 0;
}

struct validator v_lock[] = {
	VALIDATOR_INT(0, 1, "ERROR_LOCK_INVALID"),
	VALIDATOR_FUNC(vf_lock_auto),
	{0}
};
ADD_VALIDATOR(v_lock);

/* Command parameters */
static struct c_param parameters[] = {
	{"limit", 0, "v_integer"},
	{"offset", 0, "v_integer"},
	{"get_associated_ads", 0, "v_bool"},
	{"remote_addr", 0, "v_ip"},
	{"remote_browser", 0, "v_remote_browser"},
	{"token", P_REQUIRED, "v_token:adqueue"},
	{"filter_name", 0, "v_string_isprint"},
	{"lock", 0, "v_lock"},
	{"email", 0, "v_email_basic"},
	{"ad_id", 0, "v_integer"},
	{"action_id", 0, "v_integer"},
	{NULL, 0}
};


/*****************************************************************************
 * adqueues_done
 * Exit function.
 **********/
static void
adqueues_done(void *v) {
	struct adqueues_state *state = v;
	struct cmd *cs = state->cs;
	bpapi_output_free(&state->ba);
	bpapi_simplevars_free(&state->ba);
	bpapi_vtree_free(&state->ba);
	if (state->action)
		parse_loadaction_cleanup(state->action);

	while (!TAILQ_EMPTY(&state->adqueue)) {
		struct adqueue_entry *ae = TAILQ_FIRST(&state->adqueue);
		TAILQ_REMOVE(&state->adqueue, ae, queue);
		free(ae->schema);
		free(ae);
	}
	free(state);
	cmd_done(cs);
}

static void
bind_insert_adqueues(struct bpapi *ba, struct adqueues_state *state) {
	char *tmp;
	int limit;
	int in_limit;

	if (cmd_haskey(state->cs, "lock"))
		bpapi_insert(ba, "lock", cmd_getval(state->cs, "lock"));
	if (cmd_haskey(state->cs, "remote_addr"))
		bpapi_insert(ba, "remote_addr", cmd_getval(state->cs, "remote_addr"));
	if (cmd_haskey(state->cs, "token"))
		bpapi_insert(ba, "token", cmd_getval(state->cs, "token"));
	if (cmd_haskey(state->cs, "email"))
		bpapi_insert(ba, "email", cmd_getval(state->cs, "email"));
	if (cmd_haskey(state->cs, "ad_id"))
		bpapi_insert(ba, "ad_id", cmd_getval(state->cs, "ad_id"));
	if (cmd_haskey(state->cs, "action_id"))
		bpapi_insert(ba, "action_id", cmd_getval(state->cs, "action_id"));

	if (cmd_haskey(state->cs, "offset"))
		bpapi_insert(&state->ba, "offset", cmd_getval(state->cs, "offset"));
	limit = atoi(bconf_value(bconf_get(bconf_root, "*.trans.adqueues.max_limit")));
	if (cmd_haskey(state->cs, "limit")) {
		in_limit = atoi(cmd_getval(state->cs, "limit"));
		if (in_limit < limit && in_limit > 0)
			limit = in_limit;
	}
	xasprintf(&tmp, "%d", limit);
	bpapi_insert(ba, "limit", tmp);
	free(tmp);
}

static void
bind_insert_loadaction(struct bpapi *ba, struct adqueues_state *state) {
	if (cmd_haskey(state->cs, "lock"))
		bpapi_insert(ba, "lock", cmd_getval(state->cs, "lock"));
	if (cmd_haskey(state->cs, "remote_addr"))
		bpapi_insert(ba, "remote_addr", cmd_getval(state->cs, "remote_addr"));
	if (cmd_haskey(state->cs, "token"))
		bpapi_insert(ba, "token", cmd_getval(state->cs, "token"));
}

/*****************************************************************************
 * adqueues_fsm
 * State machine
 **********/
static const char *
adqueues_fsm(void *v, struct sql_worker *worker, const char *errstr) {
	struct adqueues_state *state = v;
	struct cmd *cs = state->cs;
	struct bpapi ba;

	/*
	 * General error handling for db errors.
	 */
	if (errstr) {
		transaction_logprintf(cs->ts, D_ERROR, "Statement failed (%d, %s)",
		state->state,
		errstr);
		cs->status = "TRANS_DATABASE_ERROR";
		cs->message = xstrdup(errstr);
		state->state = ADQUEUES_ERROR;
	}

	while (1) {
#define TIMER_STATE(st) if (state->ti != NULL) timer_end(state->ti, NULL); state->ti = timer_start(state->cs->ti, "ADQUEUES_"#st)
		if (state->ti) {
			timer_end(state->ti, NULL);
			state->ti = NULL;
		}

		transaction_logprintf(cs->ts, D_DEBUG, "adqueues_fsm(%p, %p, %p), state=%s",
			v, worker, errstr, adqueue_state_lut[state->state]);

		switch (state->state) {
		case ADQUEUES_INIT:
			TIMER_STATE(INIT);
			/* next state */
			state->state = ADQUEUES_GET_ADS;

			/* Initialise the templateparse structure */
			BPAPI_INIT_APP(&state->ba, host_bconf, "controlpanel", buf, pgsql);
			state->ad_cnt = 0;

			struct bconf_node *node;
			struct bconf_node *list_by_index_node;
			int i = 0;

			list_by_index_node = bconf_vget(bconf_root, "*", "controlpanel", "modules", "adqueue.filter", NULL);

			/* Use email in filters */
			if (cmd_haskey(cs, "email")) {
				bpapi_insert(&state->ba, "email", cmd_getval(cs, "email"));
			}

			if (cmd_haskey(state->cs, "filter_name")) {
				struct bconf_node *filter_index = bconf_vget(bconf_root, "*", "controlpanel", "modules", "adqueue", "filter_name", cmd_getval(state->cs, "filter_name"), NULL);
				timer_add_attribute(state->cs->ti, cmd_getval(state->cs, "filter_name"));

				if (filter_index) {
					int index = bconf_get_int(filter_index, "index");

					node = bconf_byindex(list_by_index_node, index);
					const char *name = bconf_get_string(node, "name");

					if (name) {
						bpapi_insert(&state->ba, "filter_name", name);
						const char *condition = bconf_get_string(node, "condition");

						if (condition)
							bpapi_insert(&state->ba, "condition", condition);

						if (bconf_get_int(node, "use_archive"))
							state->use_archive = 1;

						++state->ad_cnt;
					}
				}
			} else {
				while ((node = bconf_byindex(list_by_index_node, i++))) {
					const char *name = bconf_get_string(node, "name");

					if (!name || bconf_get_int(node, "skip_count"))
						continue;

					bpapi_insert(&state->ba, "filter_name", name);

					const char *condition = bconf_get_string(node, "condition");
					if (condition)
						bpapi_insert(&state->ba, "condition", condition);

					++state->ad_cnt;
				}
			}

			if (state->ad_cnt < 1) {
				transaction_logprintf(cs->ts, D_INFO, "No matching filter.");
				state->state = ADQUEUES_DONE;
				cs->error = "ERROR_QUEUE_FILTER_INVALID";
				cs->status = "TRANS_ERROR";
				continue;
			}

			if (cmd_haskey(cs, "ad_id") && cmd_haskey(cs, "action_id")) {
				state->state = ADQUEUES_GET_ADS;
				state->ad_cnt = 0;
				continue;
			}

			if (state->use_archive)
				sql_worker_template_query(&config.pg_slave, "sql/get_adqueues_archive_count.sql", &state->ba, adqueues_fsm, state, cs->ts->log_string);
			else
				sql_worker_template_query(&config.pg_slave, "sql/get_adqueues_count.sql", &state->ba, adqueues_fsm, state, cs->ts->log_string);

			/* Return to libevent */
			return NULL;

		case ADQUEUES_GET_ADS:
			TIMER_STATE(GET_ADS);
			if (state->ad_cnt) {
				worker->next_row(worker);
				transaction_printf(cs->ts, "total.%s:%s\n",
					worker->get_value(worker, 0),
					worker->get_value(worker, 1)
				);
				transaction_printf(cs->ts, "locked.%s:%s\n",
					worker->get_value(worker, 0),
					worker->get_value(worker, 2)
				);

				struct bconf_node *node =  bconf_get(bconf_root, "*.controlpanel.modules.adqueue.unreviewed_time");

				int i, n = bconf_count(node);
				for ( i = 0 ; i < n ; ++i ) {
					struct bconf_node *unreviewed_time = bconf_byindex(node, i); 
					int index = bconf_get_int(unreviewed_time, "index");
					const char *var_name = bconf_get_string(unreviewed_time, "var");

					transaction_printf(cs->ts, "%s.%s:%s\n",
									var_name,
									worker->get_value(worker, 0),
									worker->get_value(worker, index));
				}

				if (--state->ad_cnt)
					return NULL;
			}

			if (!cmd_haskey(cs, "filter_name")) {
				state->state = ADQUEUES_DONE;
				continue;
			}

			bind_insert_adqueues(&state->ba, state);

			state->state = ADQUEUES_LOAD_ADS;
			if (state->lock) {
				/* Register SQL query with our adqueues_fsm() as callback function */
				sql_worker_template_query_flags(&config.pg_master, SQLW_FINAL_CALLBACK,
								"sql/call_lock_ads_to_review.sql", &state->ba,
								adqueues_fsm, state, cs->ts->log_string);
			} else {
				/* Archive is only available on slave, but otherwise use master
				 * for synchronisation reasons */
				if (state->use_archive)
					sql_worker_template_query_flags(&config.pg_slave, SQLW_FINAL_CALLBACK,
							"sql/get_adqueues_archive.sql", &state->ba,
							adqueues_fsm, state, cs->ts->log_string);
				else
					sql_worker_template_query_flags(&config.pg_master, SQLW_FINAL_CALLBACK,
							"sql/get_adqueues.sql", &state->ba,
							adqueues_fsm, state, cs->ts->log_string);
			}

			return NULL;

		case ADQUEUES_LOAD_ADS:
			TIMER_STATE(LOAD_ADS);
			/* next state */
			state->state = ADQUEUES_CHECK_TOTAL;

			if (!worker->rows(worker)) {
				continue;
			}

			while (worker->next_row(worker)) {
				struct adqueue_entry *ae;
				const char *schema;

				transaction_logprintf(cs->ts, D_INFO, "Got %s in %s",
					worker->get_value(worker, 0),
					worker->field_name(worker, 0)
				);

				ae = xmalloc(sizeof (*ae));
				ae->ad_id = atoi(worker->get_value(worker, 0));
				ae->action_id = atoi(worker->get_value(worker, 1));
				schema = worker->get_value_byname(worker, "schema");
				if (schema && schema[0] && strcmp(schema, "public")) {
					int cl = strlen(schema);

					ae->schema = xmalloc(cl + 2);
					memcpy(ae->schema, schema, cl);
					ae->schema[cl] = '.';
					ae->schema[cl + 1] = '\0';
				} else
					ae->schema = NULL;
				TAILQ_INSERT_TAIL(&state->adqueue, ae, queue);
				state->ads_total++;
			}

			transaction_printf(cs->ts, "offset:%s\n", cmd_haskey(cs, "offset")? cmd_getval(cs, "offset") : "0");
			transaction_printf(cs->ts, "count:%d\n", state->ads_total);

			continue;

		case ADQUEUES_CHECK_TOTAL:
			TIMER_STATE(CHECK_TOTAL);
			if (state->ads_total == 0) {
				transaction_logprintf(cs->ts, D_INFO, "No ads in specified filter.");
				transaction_printf(cs->ts, "%s:%s\n", "filter_name", "ERROR_QUEUE_EMPTY");
				cs->status = "TRANS_ERROR";
				state->state = ADQUEUES_ERROR;
				return NULL;
			}

			state->state = ADQUEUES_NEXT_AD;
			return NULL;

		case ADQUEUES_NEXT_AD:
			TIMER_STATE(NEXT_AD);
		{
			struct adqueue_entry *ae;
			if (TAILQ_EMPTY(&state->adqueue)) {
				state->state = ADQUEUES_DONE;
				continue;
			}

			BPAPI_INIT(&ba, NULL, pgsql);

			ae = TAILQ_FIRST(&state->adqueue);
			bpapi_set_int(&ba, "ad_id", ae->ad_id);
			bpapi_set_int(&ba, "action_id", ae->action_id);
			if (ae->schema) {
				bpapi_insert(&ba, "schema", ae->schema);
				free(ae->schema);
			}
			TAILQ_REMOVE(&state->adqueue, ae, queue);
			free(ae);

			if (cmd_haskey(cs, "get_associated_ads"))
				bpapi_insert(&ba, "get_associated_ads", cmd_getval(cs, "get_associated_ads"));

			bind_insert_loadaction(&ba, state);

			sql_worker_template_newquery(worker, "sql/loadaction.sql", &ba);

			bpapi_output_free(&ba);
			bpapi_simplevars_free(&ba);
			bpapi_vtree_free(&ba);

			/* Return to libevent */
			state->state = ADQUEUES_LOAD_DATA;
			return NULL;
		}

		case ADQUEUES_LOAD_DATA:
			TIMER_STATE(LOAD_DATA);
			if (worker->sw_state == RESULT_DONE) {
				state->state = ADQUEUES_SHOWAD;
				continue;
			}
			/* Parse the result for the current SELECT */
			state->action = parse_loadaction(worker, cs->ts, state->action);
			return NULL;

		case ADQUEUES_SHOWAD:
			TIMER_STATE(SHOWAD);
		{
			int imageno = 0;
			struct action_param *action_param;
			const char *adsubject = NULL;
			const char *adbody = NULL;
			char *pbuf = NULL;
			int pbuflen = 0;
			int pbufpos = 0;
			int printacc = 0;
			int mindupperc = bconf_get_int(bconf_root, "*.controlpanel.modules.adqueue.settings.duplicate_min_percent");

			transaction_logoff(cs->ts);

			/* Note! The loop does not necessarily loop over all list elements since they may be removed in the loop. */
			TAILQ_FOREACH(action_param, &state->action->h, entry) {
				if (!strcmp(action_param->prefix, "ad")) {
					if (!strcmp(action_param->key, "subject")) {
						adsubject = action_param->value;
					} else if (!strcmp(action_param->key, "body")) {
						adbody = action_param->value;
					} else if (!strcmp(action_param->key, "passwd")
							|| !strcmp(action_param->key, "salted_passwd"))
						continue;
				} else if (!strcmp(action_param->prefix, "ad_change_params")) {
					if (!strncmp(action_param->key, "param_", 6)) {
						const char *key = action_param->key + 6;
						bufcat(&pbuf, &pbuflen, &pbufpos, "%s%s:%s\n", pbufpos ? "," : "", key, action_param->value);
					}
				} else if (strcmp(action_param->prefix, "params") == 0) {
					char *key;
					struct action_param *ap;

					xasprintf(&key, "param_%s", action_param->key);

					ap = parse_loadaction_pop_param(state->action,
									"ad_change_params", key);
					if (!ap || ap->value) {
						bufcat(&pbuf, &pbuflen, &pbufpos, "%s%s:%s\n", pbufpos ? "," : "", key, ap ? ap->value : action_param->value);
					}
					if (ap) {
						/* Since we popped it, we have to print it. */
						if (ap->value && (strchr(action_param->value, '\n') != NULL ||
							strchr(action_param->value, '\r') != NULL)) {
							transaction_printf(cs->ts, "blob:%ld:ad.%u.%s.%s\n%s\n",
									(unsigned long)(ap->value ? strlen(ap->value) : 0),
									state->ad_cnt,
									ap->prefix,
									ap->key,
									ap->value ? ap->value : "");
						} else {
							transaction_printf(cs->ts, "ad.%u.%s.%s:%s\n",
									state->ad_cnt,
									ap->prefix,
									ap->key,
									ap->value ? ap->value : "");
						}
						free(ap);
					}
					free(key);
				} else if (!strncmp(action_param->prefix, "associated.", sizeof("associated.") - 1)) {
					if (!strcmp(action_param->key, "ad_id")) {
						/* When starting a new associated ad, check if similarity percentage is high enough. */
						const char *accsubj = parse_loadaction_get_value(state->action, action_param->prefix, "subject");
						const char *accbody = parse_loadaction_get_value(state->action, action_param->prefix, "body");
						const char *list_id = parse_loadaction_get_value(state->action, action_param->prefix, "list_id");
						int subjperc;
						int bodyperc;
						int totperc;

						subjperc = similar_text(adsubject ? adsubject : "", accsubj ? accsubj : "", 0);
						bodyperc = similar_text(adbody ? adbody : "", accbody ? accbody : "", 0);
						/* If subject is the same -> duplicate warning */
						if (subjperc == 100) {
							printacc = 1;
							transaction_printf(cs->ts, "ad.%u.%s.percent:100\n", state->ad_cnt, action_param->prefix);
							transaction_printf(cs->ts, "ad.%u.%s.same_subject:1\n", state->ad_cnt, action_param->prefix);
							transaction_printf(cs->ts, "ad.%u.%s.subject_score:%d\n", state->ad_cnt, action_param->prefix, subjperc);
							transaction_printf(cs->ts, "ad.%u.%s.body_score:%d\n", state->ad_cnt, action_param->prefix, bodyperc);
						} else {
							/* bodyperc = similar_text(adbody ? adbody : "", accbody ? accbody : ""); */
							totperc = (subjperc + bodyperc) / 2;
							if (totperc >= mindupperc) {
								printacc = 1;
								transaction_printf(cs->ts, "ad.%u.%s.percent:%d\n", state->ad_cnt, action_param->prefix, totperc);
								transaction_printf(cs->ts, "ad.%u.%s.same_subject:0\n", state->ad_cnt, action_param->prefix);
								transaction_printf(cs->ts, "ad.%u.%s.subject_score:%d\n", state->ad_cnt, action_param->prefix, subjperc);
								transaction_printf(cs->ts, "ad.%u.%s.body_score:%d\n", state->ad_cnt, action_param->prefix, bodyperc);
							} else {
								printacc = 0;
								transaction_logprintf(cs->ts, D_DEBUG, "ad.%u.%s.percent:%d (subj: %s)", state->ad_cnt, action_param->prefix,
										totperc, accsubj ? accsubj : "");
							}
						}

						/* If the ad is published then get the ad_id and list_id and add them to the output */
						if (strcmp(list_id, "0"))
							transaction_printf(cs->ts, "ad.%u.user_ads.list_id.%s:%s\n", state->ad_cnt, action_param->value, list_id);

					}
					if (!printacc)
						continue;
				}

				if (action_param->value && *action_param->value &&
					(strchr(action_param->value, '\n') || strchr(action_param->value, '\r'))
				) {
					transaction_printf(cs->ts, "blob:%ld:ad.%u.%s.%s\n%s\n",
						(unsigned long)strlen(action_param->value),
						state->ad_cnt,
						action_param->prefix,
						action_param->key,
						action_param->value
					);
				} else if (strcmp(action_param->prefix, "images") == 0) {
					transaction_printf(cs->ts, "ad.%u.%s.%d.%s:%s\n",
						state->ad_cnt,
						action_param->prefix,
						imageno,
						action_param->key,
						action_param->value ? action_param->value : ""
					);
					imageno++;
				} else {
					transaction_printf(cs->ts, "ad.%u.%s.%s:%s\n",
						state->ad_cnt,
						action_param->prefix,
						action_param->key,
						action_param->value ? action_param->value : ""
					);
				}
			}

			free(pbuf);
			transaction_logon(cs->ts);

			state->ad_cnt++;

			parse_loadaction_cleanup(state->action);
			state->action = NULL;

			if (state->ads_total > state->ad_cnt)
				state->state = ADQUEUES_NEXT_AD;
			else
				state->state = ADQUEUES_DONE;
			continue;
		}

		case ADQUEUES_DONE:
			adqueues_done(state);
			return NULL;

		case ADQUEUES_ERROR:
			transaction_logprintf(cs->ts, D_ERROR, "Error state");
			adqueues_done(state);
			return NULL;

		default:
			/* MAJOR STATE ERROR */
			transaction_logprintf(cs->ts, D_ERROR, "Unknown state (%d)", state->state);
			state->state = ADQUEUES_ERROR;
			cs->status = "TRANS_ERROR";
			continue;
		}
	}
}

/*****************************************************************************
 * adqueues
 * Main function that initialise a state-structure and calls the state machine.
 **********/
static void
adqueues(struct cmd *cs) {
	transaction_logprintf(cs->ts, D_DEBUG, "Start adqueues transaction");
	struct adqueues_state *state;
	state = zmalloc(sizeof(*state));
	state->cs = cs;
	state->state = ADQUEUES_INIT;
	TAILQ_INIT(&state->adqueue);

	if (cmd_haskey(state->cs, "lock") &&
		strcmp(cmd_getval(cs, "lock"), "1") == 0) {
		state->lock = 1;
	}

	if (state->lock)
		timer_add_attribute(state->cs->ti, "locked");

	adqueues_fsm(state, NULL, NULL);
}

ADD_COMMAND(adqueues,       /* Name of command */
		NULL,
		adqueues,           /* Main function name */
		parameters,         /* Parameters struct */
		"Adqueues command");/* Command description */
