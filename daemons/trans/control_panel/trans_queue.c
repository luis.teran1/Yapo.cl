#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <time.h>

#include "trans.h"
#include "util.h"
#include <ctemplates.h>
#include "validators.h"
#include <bconf.h>
#include <event.h>

#define QUEUE_DONE  0
#define QUEUE_ERROR 1
#define QUEUE_INIT  2
#define QUEUE_CHECK 3

struct queue_state {
	int state;
        char *sql;
	struct cmd *cs;
        struct event timeout_event;
};

struct validator v_trans_queue_timeout[] = {
	VALIDATOR_INT(1, -1, "ERROR_TIMEOUT_INVALID"),
	{0}
};
ADD_VALIDATOR(v_trans_queue_timeout);

static struct c_param parameters[] = {
	{"timeout", 0, "v_trans_queue_timeout"}, /* measured in seconds */
	{"command", 0, "v_command"},
	{"queue", P_REQUIRED, "v_queue_queue"},
	{"sub_queue", 0, "v_queue_queue"},
	{"info", 0, "v_info_queue"},
	{NULL, 0}
};

static const char *queue_fsm(void *, struct sql_worker *, const char *);

extern char *trans_user;


/*****************************************************************************
 * queue_done
 * Exit function.
 **********/
static void
queue_done(void *v) {
	struct queue_state *state = v;
	struct cmd *cs = state->cs;
	free(state);
	cmd_done(cs);
}

/*****************************************************************************
 * bind_insert
 * Initialise template API before a call to sql_worker_template_query().
 **********/
static void
bind_insert(struct bpapi *ba, struct queue_state *state) {
	bpapi_insert(ba, "added_by", trans_user);

	if (cmd_haskey(state->cs, "timeout"))
		bpapi_insert(ba, "timeout", cmd_getval(state->cs, "timeout"));

	bpapi_insert(ba, "command", cmd_getval(state->cs, "command"));
	bpapi_insert(ba, "queue", cmd_getval(state->cs, "queue"));

	if (cmd_haskey(state->cs, "sub_queue"))
		bpapi_insert(ba, "sub_queue", cmd_getval(state->cs, "sub_queue"));
	if (cmd_haskey(state->cs, "info"))
		bpapi_insert(ba, "info", cmd_getval(state->cs, "info"));
}

/*****************************************************************************
 * queue_fsm
 * State machine
 **********/
static const char *
queue_fsm(void *v, struct sql_worker *worker, const char *errstr) {
	struct queue_state *state = v;
	struct cmd *cs = state->cs;
        struct bpapi ba;

	transaction_logprintf(cs->ts, D_DEBUG, "queue_fsm(%p, %p, %p), state=%d", 
                              v, worker, errstr, state->state);

        /*
         * Clean up pending data
         */
	if (state->sql) {
		free(state->sql);
		state->sql = NULL;
	}

	/* 
	 * General error handling for db errors.
	 */
	if (errstr) {
                transaction_logprintf(cs->ts, D_ERROR, "Statement failed (%d, %s)", 
                                      state->state,
                                      errstr);
                cs->status = "TRANS_DATABASE_ERROR";
                cs->message = xstrdup(errstr);
		state->state = QUEUE_ERROR;
        }

	while (1) {
		switch (state->state) {
		case QUEUE_INIT:
			/* next state */
			state->state = QUEUE_DONE;

			/* Initialise the templateparse structure */
			BPAPI_INIT(&ba, NULL, pgsql);

			bind_insert(&ba, state);

			/* Register SQL query with our queue_fsm() as callback function */
			sql_worker_template_query("pgsql.queue",
						  "sql/insert_queue.sql", &ba, 
						  queue_fsm, state, cs->ts->log_string);

			/* Clean-up */
			bpapi_output_free(&ba);
			bpapi_simplevars_free(&ba);
			bpapi_vtree_free(&ba);

			/* Return to libevent */
			return NULL;

		case QUEUE_DONE:
			queue_done(state);
			return NULL;

		case QUEUE_ERROR:
			transaction_logprintf(cs->ts, D_ERROR, "Error state");
			queue_done(state);
			return NULL;

		default:
			/* MAJOR STATE ERROR */
			cs->status = "TRANS_ERROR";
			transaction_logprintf(cs->ts, D_ERROR, "Unknown state (%d)", 
					      state->state);
			queue_done(state);
			return NULL;

		}
	}
}

/*****************************************************************************
 * queue
 * Main function that initialise a state-structure and calls the state machine.
 **********/
static void
queue(struct cmd *cs) {
	transaction_logprintf(cs->ts, D_DEBUG, "Start Queue transaction");
	struct queue_state *state;
	state = zmalloc(sizeof(*state));
	state->cs = cs;
	state->state = QUEUE_INIT;

	queue_fsm(state, NULL, NULL);
}

ADD_COMMAND(queue,     /* Name of command */
		NULL,
            queue,     /* Main function name */
            parameters,  /* Parameters struct */
            "Queue command");/* Command description */
