#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <time.h>

#include "trans.h"
#include "util.h"
#include <ctemplates.h>
#include "validators.h"
#include <bconf.h>

#define DEAUTHENTICATE_DONE  0
#define DEAUTHENTICATE_ERROR 1
#define DEAUTHENTICATE_INIT  2
#define DEAUTHENTICATE_UPDATE 3

struct deauthenticate_state {
	int state;
        char *sql;
	struct cmd *cs;
	struct sql_worker *sw;
};
static const char *deauthenticate_fsm(void *, struct sql_worker *, const char *);

extern struct bconf_node *bconf_root;

static struct c_param deauthenticate_parameters[] = {
	{"token", P_REQUIRED, "v_string_isprint"},
	{"admin_id", P_REQUIRED, "v_integer"},
	{"remote_addr", 0, "v_ip"},
	{"remote_browser", 0, "v_remote_browser"},
	{NULL, 0, NULL}
};

static void
deauthenticate_done(struct deauthenticate_state *state)
{
	struct cmd *cs = state->cs;

	free(state);
	cmd_done(cs);
}

/*****************************************************************************
 * deauthenticate_fsm
 * State machine
 **********/
static const char *
deauthenticate_fsm(void *v, struct sql_worker *worker, const char *errstr) {
	struct deauthenticate_state *state = v;
	struct cmd *cs = state->cs;
	struct bpapi ba;

        /*
         * Clean up pending data
         */
	if (state->sql) {
		free(state->sql);
		state->sql = NULL;
	}

	/*
	 * General error handling for db errors.
	 */
	if (errstr) {
                transaction_logprintf(cs->ts, D_ERROR, "Statement failed (%d, %s)",
                                      state->state,
                                      errstr);
                cs->status = "TRANS_DATABASE_ERROR";
                cs->message = xstrdup(errstr);
		state->state = DEAUTHENTICATE_ERROR;
		return "ROLLBACK";
        }

        switch (state->state) {
        case DEAUTHENTICATE_INIT:
                /* next state */
                state->state = DEAUTHENTICATE_DONE;

		BPAPI_INIT(&ba, NULL, pgsql);
		bpapi_insert(&ba, "token", cmd_getval(cs, "token"));
		bpapi_insert(&ba, "admin_id", cmd_getval(cs, "admin_id"));

                sql_worker_template_query(&config.pg_master, "sql/deauthenticate_token.sql", &ba,
					  deauthenticate_fsm, state, state->cs->ts->log_string);

		bpapi_output_free(&ba);
		bpapi_simplevars_free(&ba);
		bpapi_vtree_free(&ba);

                        /* Return to libevent */
                return NULL;

        case DEAUTHENTICATE_DONE:
                /* Token is now not valid, no need to do anything */
		deauthenticate_done(state);
                return NULL;
        default:
		deauthenticate_done(state);
                return NULL;
        }
}

/*****************************************************************************
 * deauthenticate
 * Main function that initialise a state-structure and calls the state machine.
 **********/
static void
deauthenticate(struct cmd *cs) {
	transaction_logprintf(cs->ts, D_DEBUG, "Start Deauthenticate transaction");
	struct deauthenticate_state *state;
	state = zmalloc(sizeof(*state));
	state->cs = cs;
	state->state = DEAUTHENTICATE_INIT;

	deauthenticate_fsm(state, NULL, NULL);
}

ADD_COMMAND(deauthenticate, NULL, deauthenticate, deauthenticate_parameters, NULL);
