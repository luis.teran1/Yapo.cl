#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <time.h>

#include "trans.h"
#include <ctemplates.h>
#include "util.h"
#include "validators.h"
#include "utils/filterd.h"

#define BLOCK_LIST_DONE 0
#define BLOCK_LIST_ERROR 1
#define BLOCK_LIST_LIST_LISTS 2
#define BLOCK_LIST_LIST_LISTS_RESULT 3
#define BLOCK_LIST_ADD_LIST 4
#define BLOCK_LIST_ADD_LIST_INSERT 5
#define BLOCK_LIST_ADD_LIST_RESULT 6
#define BLOCK_LIST_EDIT_LIST 7
#define BLOCK_LIST_EDIT_LIST_UPDATE 8
#define BLOCK_LIST_EDIT_LIST_RESULT 9
#define BLOCK_LIST_DEL_LIST 10
#define BLOCK_LIST_DEL_LIST_ITEM_DELETE 11
#define BLOCK_LIST_DEL_LIST_DELETE 12
#define BLOCK_LIST_DEL_LIST_RESULT 13

struct block_list_state {
	int state;
	struct cmd *cs;
};

struct validator v_block_list_action[] = {
	VALIDATOR_LEN(-1, 32, "ERROR_BLOCK_LIST_ACTION_TOO_LONG"),
	{0}
};

ADD_VALIDATOR(v_block_list_action);


static struct c_param block_list_parameters[] = {
	{"token",          P_REQUIRED,    "v_token:filter"},
	{"remote_addr",    P_REQUIRED,    "v_ip"},
	{"remote_browser", 0,             "v_remote_browser"},
	{"list_name",      0,             "v_string_isprint"},
	{"list_id",        0,             "v_integer"},
	{"list_type",      0,             "v_array:ip,email,category,general,country,refusal_reason"},
	{"action",         P_REQUIRED,    "v_block_list_action"},
	{NULL, 0, NULL}
};

extern struct bconf_node *host_bconf;

static void block_list_done(void *v) {
	struct block_list_state *state = v;
	struct cmd *cs = state->cs;

	const char *action = cmd_getval(cs, "action");
	int async = bconf_get_int(host_bconf, "controlpanel.filterd.reload.async");
	if (!strcmp(action, "add") || !strcmp(action, "del") || !strcmp(action, "edit"))
		filterd_reload(cs, async);

	transaction_logprintf(cs->ts, D_DEBUG, "block_list done");
	free(state);
	cmd_done(cs);
}

static void
bind_insert(struct bpapi *ba, struct block_list_state *state) {
	struct c_param *param;

	for (param = block_list_parameters; param->name; param++) {
		if (cmd_haskey(state->cs, param->name))
			bpapi_insert(ba, param->name, cmd_getval(state->cs, param->name));
	}
}


static const char *
block_list_fsm(void *v, struct sql_worker *worker, const char *errstr) {
	struct block_list_state *state = v;
	struct cmd *cs = state->cs;
	struct bpapi ba;
	int count = 0;
        int i;

	transaction_logprintf(cs->ts, D_DEBUG, "block_list fsm");

	if (state->state == BLOCK_LIST_ERROR) {
		block_list_done(state);
		return NULL;
	}

	/* 
	 * General error handling for db errors.
	 */

	if (errstr) {
                cs->status = "TRANS_DATABASE_ERROR";
                cs->message = xstrdup(errstr);
		state->state = BLOCK_LIST_ERROR;
		block_list_done(state);
		return NULL;
	}

	while (state->state != BLOCK_LIST_DONE) {
		switch(state->state) {

		/* List lists */
		case BLOCK_LIST_LIST_LISTS:
			BPAPI_INIT(&ba, NULL, pgsql);
			bind_insert(&ba, state);
			/* Use master for up to date data. */
			sql_worker_template_query(&config.pg_master, "sql/filters/list_block_lists.sql", &ba,
						  block_list_fsm, state, cs->ts->log_string);
			bpapi_output_free(&ba);
			bpapi_simplevars_free(&ba);
			bpapi_vtree_free(&ba);
			state->state = BLOCK_LIST_LIST_LISTS_RESULT;
			return NULL;

		case BLOCK_LIST_LIST_LISTS_RESULT:
			while (worker->next_row(worker)) {
				int nfields = worker->fields(worker);
				for (i = 0; i < nfields; i++) {
					transaction_printf(cs->ts, "block_list.%d.%s:%s\n",
							   count, 
							   worker->field_name(worker, i),
							   worker->get_value(worker, i) ? worker->get_value(worker, i) : "");
				}

				count++;
			}

			state->state = BLOCK_LIST_DONE;
			continue;

		/* Add list */
		case BLOCK_LIST_ADD_LIST:
			BPAPI_INIT(&ba, NULL, pgsql);
			bind_insert(&ba, state);
			sql_worker_template_query(&config.pg_master, "sql/filters/add_block_list.sql", &ba,
						  block_list_fsm, state, cs->ts->log_string);
			bpapi_output_free(&ba);
			bpapi_simplevars_free(&ba);
			bpapi_vtree_free(&ba);
			state->state = BLOCK_LIST_ADD_LIST_INSERT;
			return NULL;

		case BLOCK_LIST_ADD_LIST_INSERT:
			state->state = BLOCK_LIST_ADD_LIST_RESULT;
			return NULL;

		case BLOCK_LIST_ADD_LIST_RESULT:
			transaction_logprintf(cs->ts, D_DEBUG, "Added block_list %s", cmd_getval(cs, "list_name"));
			state->state = BLOCK_LIST_DONE;
			continue;

		/* Edit list */
		case BLOCK_LIST_EDIT_LIST:
			BPAPI_INIT(&ba, NULL, pgsql);
			bind_insert(&ba, state);
			sql_worker_template_query(&config.pg_master, "sql/filters/edit_block_list.sql", &ba,
						  block_list_fsm, state, cs->ts->log_string);
			bpapi_output_free(&ba);
			bpapi_simplevars_free(&ba);
			bpapi_vtree_free(&ba);
			state->state = BLOCK_LIST_EDIT_LIST_UPDATE;
			return NULL;
			
		case BLOCK_LIST_EDIT_LIST_UPDATE:
			state->state = BLOCK_LIST_EDIT_LIST_RESULT;
			return NULL;

		case BLOCK_LIST_EDIT_LIST_RESULT:
			transaction_logprintf(cs->ts, D_DEBUG, "Updated block_list (%s) name to '%s'", cmd_getval(cs, "list_id"), cmd_getval(cs, "list_name"));
			state->state = BLOCK_LIST_DONE;
			continue;


		/* Delete list */
		case BLOCK_LIST_DEL_LIST:
			BPAPI_INIT(&ba, NULL, pgsql);
			bind_insert(&ba, state);
			sql_worker_template_query(&config.pg_master, "sql/filters/del_block_list.sql", &ba,
						  block_list_fsm, state, cs->ts->log_string);
			bpapi_output_free(&ba);
			bpapi_simplevars_free(&ba);
			bpapi_vtree_free(&ba);
			state->state = BLOCK_LIST_DEL_LIST_ITEM_DELETE;
			return NULL;

		case BLOCK_LIST_DEL_LIST_ITEM_DELETE:
			state->state = BLOCK_LIST_DEL_LIST_DELETE;
			return NULL;
		
		case BLOCK_LIST_DEL_LIST_DELETE:
			state->state = BLOCK_LIST_DEL_LIST_RESULT;
			return NULL;

		case BLOCK_LIST_DEL_LIST_RESULT:
			transaction_logprintf(cs->ts, D_DEBUG, "Deleted block_list (%s)", cmd_getval(cs, "list_id"));
			state->state = BLOCK_LIST_DONE;
			continue;


		}
	}

	block_list_done(state);
	return NULL;
}

static void
block_list(struct cmd *cs) {
	struct block_list_state *state;
	const char *action = cmd_getval(cs, "action");

	transaction_logprintf(cs->ts, D_DEBUG, "block_list command started");
	state = zmalloc(sizeof(*state));

	state->cs = cs;

	if (strcmp(action, "list") == 0)
		state->state = BLOCK_LIST_LIST_LISTS;
	else if (strcmp(action, "add") == 0)
		state->state = BLOCK_LIST_ADD_LIST;
	else if (strcmp(action, "del") == 0)
		state->state = BLOCK_LIST_DEL_LIST;
	else if (strcmp(action, "edit") == 0)
		state->state = BLOCK_LIST_EDIT_LIST;
	else
		state->state = BLOCK_LIST_DONE;

	block_list_fsm(state, NULL, NULL);
}

ADD_COMMAND(block_list, NULL, block_list, block_list_parameters, NULL);
