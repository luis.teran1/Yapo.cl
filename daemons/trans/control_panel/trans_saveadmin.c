#include "factory.h"

static struct c_param passwd_param  = {"passwd", 0, "v_passwd_sha1"};
static struct c_param passwd_not_required_param  = {"passwd", 0, "v_passwd_sha1_not_required"};

static int
v_valid_passwd(struct cmd_param *param, const char *arg) {
	if (cmd_haskey(param->cs, "admin_id") &&
			strlen(cmd_getval(param->cs, "admin_id"))) {
		cmd_extra_param(param->cs, &passwd_not_required_param);
	} else {
		cmd_extra_param(param->cs, &passwd_param);
	}

	return 0;
}

struct 
validator v_username[] = {
	VALIDATOR_LEN(2, -1, "ERROR_NAME_TOO_SHORT"),
	VALIDATOR_LEN(-1, 50, "ERROR_NAME_TOO_LONG"),
	VALIDATOR_NREGEX("[%^/\\()=#+]", REG_EXTENDED, "ERROR_NAME_INVALID"),
	VALIDATOR_REGEX("[[:alpha:]]+", REG_EXTENDED, "ERROR_NAME_INVALID"),
	VALIDATOR_FUNC(v_valid_passwd),
	{0}
};

ADD_VALIDATOR(v_username);

struct
validator v_privs[] = {
	VALIDATOR_REGEX("^((adqueue|admin|filter|adminad|adminedit|clearad|credit|config|notice_abuse|search|Stores|popular_ads|on_call|Websql|bids|sms|scarface|ais|landing_page|Adminuf|api|telesales|Packs|Refund|OpMsg|yapesos|Banners|Services|PPages|Partners|Accounts|Yapofact|Subscriptions|PaymentDelivery|Smartbump|Carousel|Rewards|LoanManager|ClaimProduct|HomeLinks)(\\.[a-zA-Z_]+(=[a-zA-Z_0-9]+)?)?,?)*$", REG_EXTENDED, "ERROR_PRIVS_INVALID"),
	{0}
};

ADD_VALIDATOR(v_privs);

static struct c_param parameters[] = {
	{"token",		P_REQUIRED,	"v_token:admin"},
	{"admin_id", 		0,	"v_integer"},
	{"username",		P_REQUIRED,	"v_username"},
	{"fullname",		0,	"v_name"},
	{"jabbername",		0,	"v_name"},
	{"email", 		0,	"v_email_not_required"},
	{"mobile",		0,	"v_phone_basic"},
	{"privs", 		0,	"v_privs"},
	{"remote_addr", 	P_REQUIRED,  	"v_ip"},
	{"remote_browser", 	0,  	"v_remote_browser"},
	{NULL, 0, NULL}
};

FACTORY_TRANSACTION(saveadmin, &config.pg_master, "sql/call_alter_admin.sql", NULL, parameters, FACTORY_NO_RES_CNTR);
