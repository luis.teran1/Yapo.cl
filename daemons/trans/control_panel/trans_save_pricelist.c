#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <time.h>

#include "trans.h"
#include "util.h"
#include <ctemplates.h>
#include "validators.h"
#include <bconf.h>
#include <event.h>

#define SAVE_PRICELIST_DONE  0
#define SAVE_PRICELIST_ERROR 1
#define SAVE_PRICELIST_INIT  2
#define SAVE_PRICELIST_UNLOAD 3

struct save_pricelist_state {
	int state;
	struct cmd *cs;
};

struct validator v_pricelist[] = {
	VALIDATOR_LEN(2, -1, "ERROR_BODY_TOO_SHORT"),
	VALIDATOR_LEN(-1, 20, "ERROR_BODY_TOO_LONG"),
	{0}
};
ADD_VALIDATOR(v_pricelist);

static struct c_param parameters[] = {
	{"list",  P_REQUIRED          , "v_pricelist"},
	{"item",  P_REQUIRED | P_MULTI, "v_pricelist"},
	{"price", P_REQUIRED | P_MULTI, "v_integer"},
	{NULL, 0}
};

static const char *save_pricelist_fsm(void *, struct sql_worker *, const char *);


/*****************************************************************************
 * save_pricelist_done
 * Exit function.
 **********/
static void
save_pricelist_done(void *v) {
	struct save_pricelist_state *state = v;
	struct cmd *cs = state->cs;
	free(state);
	cmd_done(cs);
}

/*****************************************************************************
 * bind_insert
 * Initialise template API before a call to sql_worker_template_query().
 **********/
static void
bind_insert(struct bpapi *ba, struct save_pricelist_state *state) {
	struct cmd_param *item;
	struct cmd_param *price;

	bpapi_insert(ba, "list", cmd_getval(state->cs, "list"));
	for (item = cmd_getparam(state->cs, "item"), price = cmd_getparam(state->cs, "price");
			item && price; item = cmd_nextparam(state->cs, item), price = cmd_nextparam(state->cs, price)) {
		bpapi_insert(ba, "item", item->value);	
		bpapi_insert(ba, "price", price->value);	
	}
	if (item || price) {
		/* Error, mismatching number of params. */
	}
}

/*****************************************************************************
 * save_pricelist_fsm
 * State machine
 **********/
static const char *
save_pricelist_fsm(void *v, struct sql_worker *worker, const char *errstr) {
	struct save_pricelist_state *state = v;
	struct cmd *cs = state->cs;
        struct bpapi ba;

	/* 
	 * General error handling for db errors.
	 */
	if (errstr) {
                transaction_logprintf(cs->ts, D_ERROR, "save_pricelist_fsm statement failed (%d, %s)", 
                                      state->state,
                                      errstr);
                cs->status = "TRANS_DATABASE_ERROR";
                cs->message = xstrdup(errstr);
		state->state = SAVE_PRICELIST_ERROR;
                if (worker) {
			/* Turn of final callback, if set. */
			sql_worker_set_wflags(worker, 0);
			return "ROLLBACK";
		}
        }

	while (1) {
		transaction_logprintf(cs->ts, D_DEBUG, "save_pricelist_fsm(%p, %p, %p), state=%d", 
				      v, worker, errstr, state->state);

		switch (state->state) {
		case SAVE_PRICELIST_INIT:
			/* next state */
			state->state = SAVE_PRICELIST_UNLOAD;

			/* Initialise the templateparse structure */
			BPAPI_INIT(&ba, NULL, pgsql);
			bind_insert(&ba, state);

			/* Register SQL query with our save_pricelist_fsm() as callback function */
			/* Make sure to switch to slave if read-only query. */
			sql_worker_template_query_flags(&config.pg_master, SQLW_FINAL_CALLBACK,
						  "sql/save_pricelist.sql", &ba, 
						  save_pricelist_fsm, state, cs->ts->log_string);

			/* Clean-up */
			bpapi_output_free(&ba);
			bpapi_simplevars_free(&ba);
			bpapi_vtree_free(&ba);

			/* Return to libevent */
			return NULL;

		case SAVE_PRICELIST_UNLOAD:
			if (worker->sw_state == RESULT_DONE) {
				state->state = SAVE_PRICELIST_DONE;
				continue;
			}


			while (worker->next_row(worker)) {
				int cols = worker->fields(worker);
				int i;

				for (i = 0; i < cols; i++) {
					
					if (strcmp(worker->get_value(worker, i), (cmd_getparam(state->cs, "price")->value)))
						transaction_logprintf(cs->ts, D_INFO, "save_pricelist: Changing price_list in item %s, from:%s, to:%s\n", cmd_getparam(state->cs, "item")->value, worker->get_value(worker, i), cmd_getparam(state->cs, "price")->value);
				}
			}


			
			return NULL;

		case SAVE_PRICELIST_DONE:
			save_pricelist_done(state);
			return NULL;

		case SAVE_PRICELIST_ERROR:
			transaction_logprintf(cs->ts, D_ERROR, "Error state");
			save_pricelist_done(state);
			return NULL;

		default:
			/* MAJOR STATE ERROR */
			cs->status = "TRANS_ERROR";
			transaction_logprintf(cs->ts, D_ERROR, "Unknown state (%d)", 
					      state->state);
			save_pricelist_done(state);
			return NULL;

		}
	}
}

/*****************************************************************************
 * save_pricelist
 * Main function that initialise a state-structure and calls the state machine.
 **********/
static void
save_pricelist(struct cmd *cs) {
	transaction_logprintf(cs->ts, D_DEBUG, "Start save_pricelist transaction");
	struct save_pricelist_state *state;
	state = zmalloc(sizeof(*state));
	state->cs = cs;
	state->state = SAVE_PRICELIST_INIT;

	save_pricelist_fsm(state, NULL, NULL);
}

ADD_COMMAND(save_pricelist,     /* Name of command */
		NULL,
            save_pricelist,     /* Main function name */
            parameters,  /* Parameters struct */
	    "Save pricelist command");/* Command description */
