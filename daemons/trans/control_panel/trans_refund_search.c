#include "factory.h"

struct validator v_refund_filter[] = {
	VALIDATOR_BCONF_LIST_KEY("refund_cp.filter", "ERROR_INVALID_REFUND_FILTER"),
	{0}
};
ADD_VALIDATOR(v_refund_filter);

static struct c_param parameters[] = {
	{"date_start", P_REQUIRED, "v_date_time"},
	{"date_end", P_REQUIRED, "v_date_time"},
	{"filter", P_REQUIRED, "v_refund_filter"},
	{"token", P_REQUIRED, "v_token:Refund.searchRefund"},
	{"emailto", P_OPTIONAL, "v_email_basic"},
	{"remote_addr", P_OPTIONAL, "v_ip"},
	{"remote_browser", P_OPTIONAL, "v_remote_browser"},
	{NULL, 0}
};

static struct factory_data data = {
	"refund",
	FACTORY_EMPTY_RESULT_OK
};

static const struct factory_action present_email_true = FA_MAIL("mail/refund_search_cp.mime");

static const struct factory_action *
exist_email(struct cmd *cs, struct bpapi *ba) {
	if (cmd_haskey(cs, "emailto") && cmd_getval(cs,"emailto"))
		return &present_email_true;
	return NULL;
}

static const struct factory_action actions[]= {
	FA_SQL("", "pgsql.master", "sql/refund_search.sql", FA_OUT),
	FA_COND(exist_email),
	FA_DONE()
};

FACTORY_TRANS(refund_search, parameters, data, actions);
