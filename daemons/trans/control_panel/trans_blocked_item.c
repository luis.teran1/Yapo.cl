#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <time.h>

#include "trans.h"
#include <ctemplates.h>
#include "util.h"
#include "validators.h"
#include "utils/filterd.h"

enum blocked_item_e {
	BLOCKED_ITEM_DONE,
	BLOCKED_ITEM_ERROR,
	BLOCKED_ITEM_LIST_ITEMS,
	BLOCKED_ITEM_LIST_ITEMS_LIST_TYPE,
	BLOCKED_ITEM_LIST_ITEMS_RESULT,
	BLOCKED_ITEM_ADD_ITEM,
	BLOCKED_ITEM_ADD_ITEM_INSERT,
	BLOCKED_ITEM_ADD_ITEM_RESULT,
	BLOCKED_ITEM_EDIT_ITEM,
	BLOCKED_ITEM_EDIT_ITEM_UPDATE,
	BLOCKED_ITEM_EDIT_ITEM_RESULT,
	BLOCKED_ITEM_DEL_ITEM,
	BLOCKED_ITEM_DEL_ITEM_DELETE,
	BLOCKED_ITEM_DEL_ITEM_RESULT,
	BLOCKED_ITEM_GET_BYVALUE,
	BLOCKED_ITEM_GET_BYVALUE_RESULT,
	BLOCKED_ITEM_SET_CATEGORIES,
	BLOCKED_ITEM_SET_CATEGORIES_CHECK,
	BLOCKED_ITEM_SET_CATEGORIES_DELETE,
	BLOCKED_ITEM_SET_CATEGORIES_INSERT,
	BLOCKED_ITEM_SET_CATEGORIES_RESULT,
	BLOCKED_ITEM_SET_COUNTRIES,
	BLOCKED_ITEM_SET_COUNTRIES_CHECK,
	BLOCKED_ITEM_SET_COUNTRIES_DELETE,
	BLOCKED_ITEM_SET_COUNTRIES_INSERT,
	BLOCKED_ITEM_SET_COUNTRIES_RESULT,
	BLOCKED_ITEM_SET_REFUSALS,
	BLOCKED_ITEM_SET_REFUSALS_CHECK,
	BLOCKED_ITEM_SET_REFUSALS_DELETE,
	BLOCKED_ITEM_SET_REFUSALS_INSERT,
	BLOCKED_ITEM_SET_REFUSALS_RESULT,
};
struct blocked_item_state {
	enum blocked_item_e state;
	struct cmd *cs;
};

struct validator v_value[] = {
	VALIDATOR_LEN(-1, 200, "ERROR_VALUE_TOO_LONG"),
	VALIDATOR_LEN(1, -1, "ERROR_VALUE_EMPTY"),
	{0}
};

ADD_VALIDATOR(v_value);


static struct c_param blocked_item_parameters[] = {
	{"token",          P_REQUIRED,    "v_token:filter"},
	{"remote_addr",    P_REQUIRED,    "v_ip"},
	{"remote_browser", 0,             "v_remote_browser"},
	{"list_id",        0,             "v_integer"},
	{"action",         P_REQUIRED,    "v_blocked_action"},
	{"search_string",  0,             "v_string_isprint"},
	{NULL, 0, NULL}
};

static struct c_param blocked_item_parameters_get[] = {
	{"value",    P_REQUIRED,    "v_value"},
	{NULL, 0, NULL}
};

static struct c_param blocked_item_parameters_add[] = {
	{"value",      P_REQUIRED,    "v_value"},
	{"item_id",    0,             "v_integer"},
	{"notice",     P_EMPTY,       "v_string_isprint"},
	{NULL, 0, NULL}
};

static struct c_param blocked_item_parameters_edit[] = {
	{"value",      P_REQUIRED,    "v_value"},
	{"item_id",    P_REQUIRED,    "v_integer"},
	{"notice",     P_EMPTY,       "v_string_isprint"},
	{NULL, 0, NULL}
};

static struct c_param blocked_item_parameters_del[] = {
	{"item_id",    P_REQUIRED,    "v_integer"},
	{NULL, 0, NULL}
};

static struct c_param blocked_item_parameters_list[] = {
	{"item_id",    0,    "v_integer"},
	{NULL, 0, NULL}
};

static struct c_param blocked_item_parameters_set_cats[] = {
	{"category",    P_MULTI,    "v_category_no_params"},
	{NULL, 0, NULL}
};

static struct c_param blocked_item_parameters_set_countries[] = {
	{"country",    P_MULTI,    "v_country_no_params"},
	{NULL, 0, NULL}
};

static struct c_param blocked_item_parameters_set_refusals[] = {
	{"refusal_reason",    P_MULTI,    "v_string_isprint"},
	{NULL, 0, NULL}
};

static int
v_action_func(struct cmd_param *param, const char *arg) {
	struct c_param *extra_params = NULL;

	if (strcmp(param->value, "add") == 0)
		extra_params = blocked_item_parameters_add;
	else if (strcmp(param->value, "edit") == 0)
		extra_params = blocked_item_parameters_edit;
	else if (strcmp(param->value, "del") == 0)
		extra_params = blocked_item_parameters_del;
	else if (strcmp(param->value, "list") == 0)
		extra_params = blocked_item_parameters_list;
	else if (strcmp(param->value, "get") == 0)
		extra_params = blocked_item_parameters_get;
	else if (strcmp(param->value, "set_categories") == 0)
		extra_params = blocked_item_parameters_set_cats;
	else if (strcmp(param->value, "set_countries") == 0)
		extra_params = blocked_item_parameters_set_countries;
	else if (strcmp(param->value, "set_refusals") == 0)
		extra_params = blocked_item_parameters_set_refusals;
	
	if (extra_params) {
		while (extra_params->name)
			cmd_extra_param(param->cs, extra_params++);
	}
	return 0;
}

struct validator v_blocked_action[] = {
	VALIDATOR_REGEX("^(add|list|edit|del|get|set_categories|set_countries|set_refusals)$", REG_EXTENDED|REG_NOSUB, "ERROR_ACTION_INVALID"),
	VALIDATOR_FUNC(v_action_func),
	{0}
};

ADD_VALIDATOR(v_blocked_action);

extern struct bconf_node *host_bconf;

static void blocked_item_done(void *v) {
	struct blocked_item_state *state = v;
	struct cmd *cs = state->cs;

	const char *action = cmd_getval(cs, "action");
	int async = bconf_get_int(host_bconf, "controlpanel.filterd.reload.async");
	if (strcmp(action, "get") && strcmp(action, "list"))
		filterd_reload(cs, async);

	transaction_logprintf(cs->ts, D_DEBUG, "blocked_item done");
	cmd_done(cs);
	free(state);
}

static void
bind_insert(struct bpapi *ba, struct blocked_item_state *state) {
	struct c_param *param;
	struct cmd_param *cmdp;

	for (param = blocked_item_parameters; param->name; param++) {
		if (cmd_haskey(state->cs, param->name))
			bpapi_insert(ba, param->name, cmd_getval(state->cs, param->name));
	}

	if (cmd_haskey(state->cs, "value"))
		bpapi_insert(ba, "value", cmd_getval(state->cs, "value"));
	if (cmd_haskey(state->cs, "item_id"))
		bpapi_insert(ba, "item_id", cmd_getval(state->cs, "item_id"));
	if (cmd_haskey(state->cs, "notice"))
		bpapi_insert(ba, "notice", cmd_getval(state->cs, "notice"));
	for (cmdp = cmd_getparam(state->cs, "category"); cmdp; cmdp = cmd_nextparam (state->cs, cmdp)) {
		bpapi_insert(ba, "category", cmdp->value);
	}
	for (cmdp = cmd_getparam (state->cs, "country"); cmdp; cmdp = cmd_nextparam (state->cs, cmdp)) {
		bpapi_insert(ba, "country", cmdp->value);
	}
	for (cmdp = cmd_getparam (state->cs, "refusal_reason"); cmdp; cmdp = cmd_nextparam (state->cs, cmdp)) {
		bpapi_insert(ba, "refusal_reason", cmdp->value);
	}
}


static const char *
blocked_item_fsm(void *v, struct sql_worker *worker, const char *errstr) {
	struct blocked_item_state *state = v;
	struct cmd *cs = state->cs;
	struct bpapi ba;
	int count = 0;
        int i;

	transaction_logprintf(cs->ts, D_DEBUG, "blocked_item fsm");

	if (state->state == BLOCKED_ITEM_ERROR) {
		blocked_item_done(state);
		return NULL;
	}

	/* 
	 * General error handling for db errors.
	 */

	if (errstr) {
		if (strstr(errstr, "ERROR_NOTICE_REQUIRED")) {
			cs->status = "TRANS_ERROR";
			cs->error = "ERROR_NOTICE_REQUIRED";
		} else {
			cs->status = "TRANS_DATABASE_ERROR";
			cs->message = xstrdup(errstr);
		}
		state->state = BLOCKED_ITEM_ERROR;
		blocked_item_done(state);
		return NULL;
	}

	while (state->state != BLOCKED_ITEM_DONE) {
		switch(state->state) {

		/* List items */
		case BLOCKED_ITEM_LIST_ITEMS:
			BPAPI_INIT(&ba, NULL, pgsql);
			bind_insert(&ba, state);
			/* Use master for up to date data. */
			sql_worker_template_query(&config.pg_master, "sql/filters/list_blocked_items.sql", &ba,
						  blocked_item_fsm, state, cs->ts->log_string);
			bpapi_output_free(&ba);
			bpapi_simplevars_free(&ba);
			bpapi_vtree_free(&ba);
			state->state = BLOCKED_ITEM_LIST_ITEMS_LIST_TYPE;
			return NULL;

		case BLOCKED_ITEM_LIST_ITEMS_LIST_TYPE:
			if (worker->next_row (worker)) {
				transaction_printf(cs->ts, "list_type:%s\n", worker->get_value(worker, 0));
			} else {
				cs->status = "TRANS_ERROR";
				transaction_printf(cs->ts, "list_id:ERROR_LIST_NOT_FOUND\n");
			}
			state->state = BLOCKED_ITEM_LIST_ITEMS_RESULT;
			return NULL;

		case BLOCKED_ITEM_LIST_ITEMS_RESULT:
			transaction_logoff(cs->ts);
			while (worker->next_row(worker)) {
				int nfields = worker->fields(worker);
				for (i = 0; i < nfields; i++) {
					if (!worker->is_null(worker, i)) {
						transaction_printf(cs->ts, "blocked_item.%d.%s:%s\n",
								   count, 
								   worker->field_name(worker, i),
								   worker->get_value(worker, i) ? worker->get_value(worker, i) : "");
					}
				}

				count++;
			}
			transaction_logon(cs->ts);

			state->state = BLOCKED_ITEM_DONE;
			continue;

		/* Add item */
		case BLOCKED_ITEM_ADD_ITEM:
			BPAPI_INIT(&ba, NULL, pgsql);
			bind_insert(&ba, state);
			sql_worker_template_query(&config.pg_master, "sql/filters/add_blocked_item.sql", &ba,
						  blocked_item_fsm, state, cs->ts->log_string);
			bpapi_output_free(&ba);
			bpapi_simplevars_free(&ba);
			bpapi_vtree_free(&ba);
			state->state = BLOCKED_ITEM_ADD_ITEM_INSERT;
			return NULL;

		case BLOCKED_ITEM_ADD_ITEM_INSERT:
			if (worker->next_row(worker) && !strcmp(worker->get_value(worker, 0), "t"))
				transaction_printf(cs->ts, "updated:1\n");
			state->state = BLOCKED_ITEM_ADD_ITEM_RESULT;
			return NULL;

		case BLOCKED_ITEM_ADD_ITEM_RESULT:
			transaction_logprintf(cs->ts, D_DEBUG, "Added blocked_item %s", cmd_getval(cs, "item_name"));
			state->state = BLOCKED_ITEM_DONE;
			continue;

		/* Edit item */
		case BLOCKED_ITEM_EDIT_ITEM:
			BPAPI_INIT(&ba, NULL, pgsql);
			bind_insert(&ba, state);
			sql_worker_template_query(&config.pg_master, "sql/filters/edit_blocked_item.sql", &ba,
						  blocked_item_fsm, state, cs->ts->log_string);
			bpapi_output_free(&ba);
			bpapi_simplevars_free(&ba);
			bpapi_vtree_free(&ba);
			state->state = BLOCKED_ITEM_EDIT_ITEM_UPDATE;
			return NULL;
			
		case BLOCKED_ITEM_EDIT_ITEM_UPDATE:
			state->state = BLOCKED_ITEM_EDIT_ITEM_RESULT;
			return NULL;

		case BLOCKED_ITEM_EDIT_ITEM_RESULT:
			transaction_logprintf(cs->ts, D_DEBUG, "Updated blocked_item (%s) name to '%s'", cmd_getval(cs, "item_id"), cmd_getval(cs, "item_name"));
			state->state = BLOCKED_ITEM_DONE;
			continue;


		/* Delete item */
		case BLOCKED_ITEM_DEL_ITEM:
			BPAPI_INIT(&ba, NULL, pgsql);
			bind_insert(&ba, state);
			sql_worker_template_query(&config.pg_master, "sql/filters/del_blocked_item.sql", &ba,
						  blocked_item_fsm, state, cs->ts->log_string);
			bpapi_output_free(&ba);
			bpapi_simplevars_free(&ba);
			bpapi_vtree_free(&ba);
			state->state = BLOCKED_ITEM_DEL_ITEM_DELETE;
			return NULL;

		case BLOCKED_ITEM_DEL_ITEM_DELETE:
			state->state = BLOCKED_ITEM_DEL_ITEM_RESULT;
			return NULL;

		case BLOCKED_ITEM_DEL_ITEM_RESULT:
			transaction_logprintf(cs->ts, D_DEBUG, "Deleted blocked_item (%s)", cmd_getval(cs, "item_id"));
			state->state = BLOCKED_ITEM_DONE;
			continue;


		/* Get item by value */
		case BLOCKED_ITEM_GET_BYVALUE:
			BPAPI_INIT(&ba, NULL, pgsql);
			bind_insert(&ba, state);
			sql_worker_template_query(
				&config.pg_master,
				"sql/filters/get_blocked_item.sql",
				&ba,
				blocked_item_fsm,
				state,
				cs->ts->log_string
			);
			bpapi_output_free(&ba);
			bpapi_simplevars_free(&ba);
			bpapi_vtree_free(&ba);
			state->state = BLOCKED_ITEM_GET_BYVALUE_RESULT;
			return NULL;

		case BLOCKED_ITEM_GET_BYVALUE_RESULT:
			if (worker->next_row(worker)) {
				int i;
				int nfields = worker->fields(worker);
				for (i = 0; i < nfields; i++) {
					transaction_printf(
						cs->ts,
						"blocked_item.%s:%s\n",
						worker->field_name(worker, i),
						worker->get_value(worker, i) ? worker->get_value(worker, i) : ""
					);
				}
			}
			state->state = BLOCKED_ITEM_DONE;
			continue;

		case BLOCKED_ITEM_SET_CATEGORIES:
			BPAPI_INIT(&ba, NULL, pgsql);
			bind_insert(&ba, state);
			sql_worker_template_query(
				&config.pg_master,
				"sql/filters/items_set_categories_check.sql",
				&ba,
				blocked_item_fsm,
				state,
				cs->ts->log_string
			);
			bpapi_output_free(&ba);
			bpapi_simplevars_free(&ba);
			bpapi_vtree_free(&ba);
			state->state = BLOCKED_ITEM_SET_CATEGORIES_CHECK;
			return NULL;

		case BLOCKED_ITEM_SET_CATEGORIES_CHECK:
			if (!worker->next_row (worker)) {
				cs->status = "TRANS_ERROR";
				transaction_printf(cs->ts, "list_id:ERROR_LIST_NOT_FOUND\n");
				state->state = BLOCKED_ITEM_DONE;
				continue;
			}
			if (strcmp (worker->get_value(worker, 0), "category") != 0) {
				cs->status = "TRANS_ERROR";
				transaction_printf(cs->ts, "list_id:ERROR_LIST_NOT_CATEGORIES\n");
				state->state = BLOCKED_ITEM_DONE;
				continue;
			}
			BPAPI_INIT(&ba, NULL, pgsql);
			bind_insert(&ba, state);
			sql_worker_template_newquery (worker, "sql/filters/items_set_categories.sql", &ba);
			bpapi_output_free(&ba);
			bpapi_simplevars_free(&ba);
			bpapi_vtree_free(&ba);
			state->state = BLOCKED_ITEM_SET_CATEGORIES_DELETE;
			return NULL;

		case BLOCKED_ITEM_SET_CATEGORIES_DELETE:
			state->state = BLOCKED_ITEM_SET_CATEGORIES_INSERT;
			return NULL;

		case BLOCKED_ITEM_SET_CATEGORIES_INSERT:
			state->state = BLOCKED_ITEM_SET_CATEGORIES_RESULT;
			return NULL;

		case BLOCKED_ITEM_SET_CATEGORIES_RESULT:
			transaction_logprintf(cs->ts, D_DEBUG, "Updated list_id (%s) categories", cmd_getval(cs, "list_id"));
			state->state = BLOCKED_ITEM_DONE;
			continue;

		case BLOCKED_ITEM_SET_COUNTRIES:

			transaction_logprintf (cs->ts, D_DEBUG, "XXXXX BLOCKED_ITEM_SET_COUNTRIES 1 XXXXX\n");

			BPAPI_INIT(&ba, NULL, pgsql);
			bind_insert(&ba, state);
			sql_worker_template_query(
				&config.pg_master,
				"sql/filters/items_set_categories_check.sql",
				&ba,
				blocked_item_fsm,
				state,
				cs->ts->log_string
			);
			transaction_logprintf (cs->ts, D_DEBUG, "XXXXX BLOCKED_ITEM_SET_COUNTRIES 2  XXXXX\n");
			bpapi_output_free(&ba);
			bpapi_simplevars_free(&ba);
			bpapi_vtree_free(&ba);
			state->state = BLOCKED_ITEM_SET_COUNTRIES_CHECK;
			return NULL;

		case BLOCKED_ITEM_SET_COUNTRIES_CHECK:

			transaction_logprintf (cs->ts,  D_DEBUG,"XXXXX BLOCKED_ITEM_SET_COUNTRIES_CHECK 1 XXXXX\n");

			if (!worker->next_row (worker)) {
				cs->status = "TRANS_ERROR";
				transaction_printf (cs->ts, "list_id:ERROR_LIST_NOT_FOUND\n");
				state->state = BLOCKED_ITEM_DONE;
				continue;
			}

			transaction_logprintf (cs->ts, D_DEBUG, "XXXXX BLOCKED_ITEM_SET_COUNTRIES_CHECK 2 XXXXX\n");

			if (strcmp (worker->get_value(worker, 0), "country") != 0) {
				cs->status = "TRANS_ERROR";
				transaction_printf (cs->ts, "list_id:ERROR_LIST_NOT_COUNTRY\n");
				state->state = BLOCKED_ITEM_DONE;
				continue;
			}

			transaction_logprintf (cs->ts, D_DEBUG, "XXXXX BLOCKED_ITEM_SET_COUNTRIES_CHECK 3 XXXXX\n");

			BPAPI_INIT(&ba, NULL, pgsql);
			bind_insert(&ba, state);
			sql_worker_template_newquery (worker, "sql/filters/items_set_countries.sql", &ba);
			bpapi_output_free(&ba);
			bpapi_simplevars_free(&ba);
			bpapi_vtree_free(&ba);
			state->state = BLOCKED_ITEM_SET_COUNTRIES_DELETE;
			return NULL;

		case BLOCKED_ITEM_SET_COUNTRIES_DELETE:

			transaction_logprintf (cs->ts, D_DEBUG, "XXXXX BLOCKED_ITEM_SET_COUNTRIES_DELETE 1 XXXXX\n");

			state->state = BLOCKED_ITEM_SET_COUNTRIES_INSERT;
			return NULL;

		case BLOCKED_ITEM_SET_COUNTRIES_INSERT:
			state->state = BLOCKED_ITEM_SET_COUNTRIES_RESULT;
			return NULL;

		case BLOCKED_ITEM_SET_COUNTRIES_RESULT:

			transaction_logprintf (cs->ts, D_DEBUG, "XXXXX BLOCKED_ITEM_SET_COUNTRIES_DELETE 1 XXXXX\n");

			transaction_logprintf(cs->ts, D_DEBUG, "Updated list_id (%s) countries", cmd_getval(cs, "list_id"));
			state->state = BLOCKED_ITEM_DONE;
			continue;

		case BLOCKED_ITEM_SET_REFUSALS:

			transaction_logprintf (cs->ts, D_DEBUG, "XXXXX BLOCKED_ITEM_SET_REFUSALS 1 XXXXX\n");

			BPAPI_INIT(&ba, NULL, pgsql);
			bind_insert(&ba, state);
			sql_worker_template_query(
				&config.pg_master,
				"sql/filters/items_set_categories_check.sql",
				&ba,
				blocked_item_fsm,
				state,
				cs->ts->log_string
			);
			transaction_logprintf (cs->ts, D_DEBUG, "XXXXX BLOCKED_ITEM_SET_REFUSALS 2  XXXXX\n");
			bpapi_output_free(&ba);
			bpapi_simplevars_free(&ba);
			bpapi_vtree_free(&ba);
			state->state = BLOCKED_ITEM_SET_REFUSALS_CHECK;
			return NULL;

		case BLOCKED_ITEM_SET_REFUSALS_CHECK:

			transaction_logprintf (cs->ts,  D_DEBUG,"XXXXX BLOCKED_ITEM_SET_REFUSALS_CHECK 1 XXXXX\n");

			if (!worker->next_row (worker)) {
				cs->status = "TRANS_ERROR";
				transaction_printf (cs->ts, "list_id:ERROR_LIST_NOT_FOUND\n");
				state->state = BLOCKED_ITEM_DONE;
				continue;
			}

			transaction_logprintf (cs->ts, D_DEBUG, "XXXXX BLOCKED_ITEM_SET_REFUSALS_CHECK 2 XXXXX\n");

			if (strcmp (worker->get_value(worker, 0), "refusal_reason") != 0) {
				cs->status = "TRANS_ERROR";
				transaction_printf (cs->ts, "list_id:ERROR_LIST_NOT_REFUSALS\n");
				state->state = BLOCKED_ITEM_DONE;
				continue;
			}

			transaction_logprintf (cs->ts, D_DEBUG, "XXXXX BLOCKED_ITEM_SET_REFUSALS_CHECK 3 XXXXX\n");

			BPAPI_INIT(&ba, NULL, pgsql);
			bind_insert(&ba, state);
			sql_worker_template_newquery (worker, "sql/filters/items_set_refusals.sql", &ba);
			bpapi_output_free(&ba);
			bpapi_simplevars_free(&ba);
			bpapi_vtree_free(&ba);
			state->state = BLOCKED_ITEM_SET_REFUSALS_DELETE;
			return NULL;

		case BLOCKED_ITEM_SET_REFUSALS_DELETE:

			transaction_logprintf (cs->ts, D_DEBUG, "XXXXX BLOCKED_ITEM_SET_REFUSALS_DELETE 1 XXXXX\n");

			state->state = BLOCKED_ITEM_SET_REFUSALS_INSERT;
			return NULL;

		case BLOCKED_ITEM_SET_REFUSALS_INSERT:
			state->state = BLOCKED_ITEM_SET_REFUSALS_RESULT;
			return NULL;

		case BLOCKED_ITEM_SET_REFUSALS_RESULT:

			transaction_logprintf (cs->ts, D_DEBUG, "XXXXX BLOCKED_ITEM_SET_REFUSALS_DELETE 1 XXXXX\n");

			transaction_logprintf(cs->ts, D_DEBUG, "Updated list_id (%s) refusals", cmd_getval(cs, "list_id"));
			state->state = BLOCKED_ITEM_DONE;
			continue;

		case BLOCKED_ITEM_ERROR:
		case BLOCKED_ITEM_DONE:
			blocked_item_done(state);
			return NULL;
		}
	}

	blocked_item_done(state);
	return NULL;
}

static void
blocked_item(struct cmd *cs) {
	struct blocked_item_state *state;
	const char *action = cmd_getval(cs, "action");

	transaction_logprintf(cs->ts, D_DEBUG, "blocked_item command started %s", action);
	state = zmalloc(sizeof(*state));

	state->cs = cs;

	if (strcmp(action, "list") == 0) {
		transaction_logprintf(cs->ts, D_DEBUG, "blocked_item list command started");
		state->state = BLOCKED_ITEM_LIST_ITEMS;
	} else if (strcmp(action, "add") == 0)
		state->state = BLOCKED_ITEM_ADD_ITEM;
	else if (strcmp(action, "del") == 0)
		state->state = BLOCKED_ITEM_DEL_ITEM;
	else if (strcmp(action, "edit") == 0)
		state->state = BLOCKED_ITEM_EDIT_ITEM;
	else if (strcmp(action, "get") == 0)
		state->state = BLOCKED_ITEM_GET_BYVALUE;
	else if (strcmp(action, "set_categories") == 0)
		state->state = BLOCKED_ITEM_SET_CATEGORIES;
	else if (strcmp(action, "set_countries") == 0)
		state->state = BLOCKED_ITEM_SET_COUNTRIES;
	else if (strcmp(action, "set_refusals") == 0)
		state->state = BLOCKED_ITEM_SET_REFUSALS;
	else
		state->state = BLOCKED_ITEM_DONE;

	blocked_item_fsm(state, NULL, NULL);
}

ADD_COMMAND(blocked_item, NULL, blocked_item, blocked_item_parameters, NULL);
