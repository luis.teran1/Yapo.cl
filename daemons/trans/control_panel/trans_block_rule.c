#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <time.h>

#include "trans.h"
#include <ctemplates.h>
#include "util.h"
#include "validators.h"
#include "utils/filterd.h"

#define BLOCK_RULE_DONE 0
#define BLOCK_RULE_ERROR 1
#define BLOCK_RULE_LIST_RULES 2
#define BLOCK_RULE_LIST_RULES_RESULT 3
#define BLOCK_RULE_LIST_RULE_CONDITIONS 4
#define BLOCK_RULE_ADD_RULE 5
#define BLOCK_RULE_ADD_RULE_BEGIN 6
#define BLOCK_RULE_ADD_RULE_LOG 7
#define BLOCK_RULE_ADD_RULE_RESULT 8
#define BLOCK_RULE_ADD_CONDITIONS 9
#define BLOCK_RULE_INSERT_CONDITION 10
#define BLOCK_RULE_DEL_RULE 11
#define BLOCK_RULE_DEL_RULE_BEGIN 12
#define BLOCK_RULE_DEL_RULE_CONDITION_DELETE 13
#define BLOCK_RULE_DEL_RULE_DELETE 14
#define BLOCK_RULE_DEL_RULE_RESULT 15

struct block_rule_state {
	int state;
	struct cmd *cs;
	struct bpapi ba;
};

struct validator v_rule_name[] = {
	VALIDATOR_REGEX("[[:alpha:]]", REG_EXTENDED, "ERROR_RULE_NAME_INVALID"),
	{0}
};
ADD_VALIDATOR(v_rule_name);

struct validator v_application[] = {
	VALIDATOR_REGEX("[[:alpha:]]", REG_EXTENDED, "ERROR_APPLICATION_INVALID"),
	{0}
};
ADD_VALIDATOR(v_application);

struct validator v_field[] = {
	VALIDATOR_REGEX("^([[:alpha:]_]+,?)+$", REG_EXTENDED, "ERROR_FIELD_INVALID"),
	{0}
};
ADD_VALIDATOR(v_field);

static struct c_param add_rule_val[] = {
	{"fields",		P_REQUIRED | P_MULTI,     "v_field"},
	{"rule_action",	 	P_REQUIRED,          "v_rule_action"},
	{"list_id",	 	P_REQUIRED | P_MULTI,  	  "v_integer"}
};
static int
vf_action(struct cmd_param *param, const char *arg) {
	if (strcmp(param->value, "add") == 0)
		cmd_extra_param(param->cs, add_rule_val);

	return 0;
}

struct validator v_action_block_rule[] = {
	VALIDATOR_REGEX("^(del|list|add)$", REG_EXTENDED, "ERROR_ACTION_INVALID"),
	VALIDATOR_FUNC(vf_action),
	{0}
};
ADD_VALIDATOR(v_action_block_rule);

static struct c_param rule_target_val =
	{"rule_target",	 	P_REQUIRED,  	  "v_bconf_value:controlpanel.modules.adqueue.checkout_queues"};

static int
vf_rule_action(struct cmd_param *param, const char *arg) {
	if (strcmp(param->value, "move_to_queue") == 0)
		cmd_extra_param(param->cs, &rule_target_val);
	return 0;
}

struct validator v_rule_action[] = {
	VALIDATOR_REGEX("^(delete|stop|spamfilter|move_to_queue)$", REG_EXTENDED, "ERROR_RULE_ACTION_INVALID"),
	VALIDATOR_FUNC(vf_rule_action),
	{0}
};
ADD_VALIDATOR(v_rule_action);

static struct c_param block_rule_parameters[] = {
	{"token",		P_REQUIRED,	"v_token:filter"},
	{"remote_addr", 	P_REQUIRED,  	"v_ip"},
	{"remote_browser", 	0,  	"v_remote_browser"},
	{"rule_name",	 	0,  	  	"v_rule_name"},
	{"rule_id",	 	0,  	  	"v_integer"},
	{"action",	 	P_REQUIRED,  	"v_action_block_rule"},
	{"rule_action",	 	0,  	  "v_rule_action"},
	{"combine_rule", 	0,  	  "v_array:and,or"},
	{"application",	 	0,  	  "v_application"},
	{"fields",		P_MULTI,  "v_field"},
	{"list_id", 		P_MULTI,  "v_integer"},
	{"whole_word", 		P_MULTI,  "v_whole_word"},
	{NULL, 0, NULL}
};

extern struct bconf_node *host_bconf;

static void block_rule_done(void *v) {
	struct block_rule_state *state = v;
	struct cmd *cs = state->cs;

	const char *action = cmd_getval(cs, "action");
	int async = bconf_get_int(host_bconf, "controlpanel.filterd.reload.async");
	if (!strcmp(action, "add") || !strcmp(action, "del"))
		filterd_reload(cs, async);

	transaction_logprintf(cs->ts, D_DEBUG, "block_rule done");
	free(state);
	cmd_done(cs);
}

static void
bind_insert(struct bpapi *ba, struct block_rule_state *state) {
	struct c_param *param;
	struct cmd_param *cp;

	for (param = block_rule_parameters; param->name; param++) {
		if (param->type & P_MULTI) {
			for (cp = cmd_getparam(state->cs, param->name); cp; cp = cmd_nextparam(state->cs, cp)) {
				bpapi_insert(ba, param->name, cp->value);
			}
		} else if (cmd_haskey(state->cs, param->name)) {
			bpapi_insert(ba, param->name, cmd_getval(state->cs, param->name));
		}
	}
	if (cmd_haskey(state->cs, "rule_target"))
		bpapi_insert(ba, "rule_target", cmd_getval(state->cs, "rule_target"));
}


static const char *
block_rule_fsm(void *v, struct sql_worker *worker, const char *errstr) {
	struct block_rule_state *state = v;
	struct cmd *cs = state->cs;
	int count = 0;
        int i;

	transaction_logprintf(cs->ts, D_DEBUG, "block_rule fsm %d", state->state);

	if (state->state == BLOCK_RULE_ERROR) {
		block_rule_done(state);
		return NULL;
	}

	/* 
	 * General error handling for db errors.
	 */

	if (errstr) {
                cs->status = "TRANS_DATABASE_ERROR";
                cs->message = xstrdup(errstr);
		state->state = BLOCK_RULE_ERROR;
		if (worker) {
			sql_worker_set_wflags(worker, 0);
			return "ROLLBACK";
		}
		block_rule_done(state);
		return NULL;
	}

	while (state->state != BLOCK_RULE_DONE) {
		switch(state->state) {

		/* List lists */
		case BLOCK_RULE_LIST_RULES:
			BPAPI_INIT(&state->ba, NULL, pgsql);
			bind_insert(&state->ba, state);
			/* Use master for up to date data. */
			sql_worker_template_query(&config.pg_master, "sql/filters/list_block_rules.sql", &state->ba,
						  block_rule_fsm, state, cs->ts->log_string);
			bpapi_output_free(&state->ba);
			bpapi_simplevars_free(&state->ba);
			bpapi_vtree_free(&state->ba);
			state->state = BLOCK_RULE_LIST_RULES_RESULT;
			return NULL;

		case BLOCK_RULE_LIST_RULES_RESULT:
			while (worker->next_row(worker)) {
				int nfields = worker->fields(worker);
				transaction_logoff(cs->ts);
				for (i = 0; i < nfields; i++) {
					transaction_printf(cs->ts, "block_rule.%d.%s:%s\n",
							   count,
							   worker->field_name(worker, i),
							   worker->get_value(worker, i) ? worker->get_value(worker, i) : "");
				}
				transaction_logon(cs->ts);

				count++;
			}

			if (cmd_haskey(cs, "rule_id")) {

				BPAPI_INIT(&state->ba, NULL, pgsql);
				bind_insert(&state->ba, state);
				sql_worker_template_query(&config.pg_master, "sql/filters/list_block_rule_conditions.sql", &state->ba,
							  block_rule_fsm, state, cs->ts->log_string);
				bpapi_output_free(&state->ba);
				bpapi_simplevars_free(&state->ba);
				bpapi_vtree_free(&state->ba);

				state->state = BLOCK_RULE_LIST_RULE_CONDITIONS;
				return NULL;
			} else {
				state->state = BLOCK_RULE_DONE;
				continue;
			}


		case BLOCK_RULE_LIST_RULE_CONDITIONS:
			while (worker->next_row(worker)) {
				int nfields = worker->fields(worker);
				transaction_logoff(cs->ts);
				for (i = 0; i < nfields; i++) {
					transaction_printf(cs->ts, "block_rule_condition.%d.%s:%s\n",
							   count,
							   worker->field_name(worker, i),
							   worker->get_value(worker, i) ? worker->get_value(worker, i) : "");
				}
				transaction_logon(cs->ts);

				count++;
			}
			state->state = BLOCK_RULE_DONE;
			continue;

		/* Add/edit list */
		case BLOCK_RULE_ADD_RULE:
			BPAPI_INIT(&state->ba, NULL, pgsql);
			bind_insert(&state->ba, state);
			sql_worker_template_query(&config.pg_master, "sql/filters/call_add_block_rule.sql", &state->ba,
						  block_rule_fsm, state, cs->ts->log_string);
			state->state = BLOCK_RULE_ADD_RULE_BEGIN;
			return NULL;

		case BLOCK_RULE_ADD_RULE_BEGIN:
			state->state = BLOCK_RULE_ADD_RULE_LOG;
			return NULL;

		case BLOCK_RULE_ADD_RULE_LOG:
			if (worker->next_row(worker)) {
				bpapi_insert(&state->ba, "rule_id", worker->get_value(worker, 0));
				transaction_printf(cs->ts, "rule_id:%s\n", worker->get_value(worker, 0));
			}
			state->state = BLOCK_RULE_ADD_RULE_RESULT;
			return NULL;

		case BLOCK_RULE_ADD_RULE_RESULT:
			transaction_logprintf(cs->ts, D_DEBUG, "Added block_rule %s", cmd_getval(cs, "rule_name"));
			state->state = BLOCK_RULE_ADD_CONDITIONS;
			continue;

		case BLOCK_RULE_ADD_CONDITIONS:
			sql_worker_set_wflags(worker, SQLW_FINAL_CALLBACK);
			sql_worker_template_newquery(worker, "sql/filters/add_block_condition.sql", &state->ba);
			bpapi_output_free(&state->ba);
			bpapi_simplevars_free(&state->ba);
			bpapi_vtree_free(&state->ba);
			state->state = BLOCK_RULE_INSERT_CONDITION;
			return NULL;

		case BLOCK_RULE_INSERT_CONDITION:
			if (worker->sw_state == RESULT_DONE) {
				sql_worker_set_wflags(worker, 0);
				state->state = BLOCK_RULE_DONE;
				return "COMMIT";
			}
			return NULL;

		/* Delete list */
		case BLOCK_RULE_DEL_RULE:
			BPAPI_INIT(&state->ba, NULL, pgsql);
			bind_insert(&state->ba, state);
			sql_worker_template_query(&config.pg_master, "sql/filters/del_block_rule.sql", &state->ba,
						  block_rule_fsm, state, cs->ts->log_string);
			bpapi_output_free(&state->ba);
			bpapi_simplevars_free(&state->ba);
			bpapi_vtree_free(&state->ba);
			state->state = BLOCK_RULE_DEL_RULE_BEGIN;
			return NULL;

		case BLOCK_RULE_DEL_RULE_BEGIN:
			state->state = BLOCK_RULE_DEL_RULE_CONDITION_DELETE;
			return NULL;

		case BLOCK_RULE_DEL_RULE_CONDITION_DELETE:
			state->state = BLOCK_RULE_DEL_RULE_DELETE;
			return NULL;

		case BLOCK_RULE_DEL_RULE_DELETE:
			state->state = BLOCK_RULE_DEL_RULE_RESULT;
			return NULL;

		case BLOCK_RULE_DEL_RULE_RESULT:
			transaction_logprintf(cs->ts, D_DEBUG, "Deleted block_rule (%s)", cmd_getval(cs, "rule_id"));
			state->state = BLOCK_RULE_DONE;
			return "COMMIT";


		}
	}

	block_rule_done(state);
	return NULL;
}

static void
block_rule(struct cmd *cs) {
	struct block_rule_state *state;
	const char *action = cmd_getval(cs, "action");

	transaction_logprintf(cs->ts, D_DEBUG, "block_rule command started");
	state = zmalloc(sizeof(*state));

	state->cs = cs;

	if (strcmp(action, "list") == 0)
		state->state = BLOCK_RULE_LIST_RULES;
	else if (strcmp(action, "add") == 0)
		state->state = BLOCK_RULE_ADD_RULE;
	else if (strcmp(action, "del") == 0)
		state->state = BLOCK_RULE_DEL_RULE;
	else
		state->state = BLOCK_RULE_DONE;

	block_rule_fsm(state, NULL, NULL);
}

ADD_COMMAND(block_rule, NULL, block_rule, block_rule_parameters, NULL);
