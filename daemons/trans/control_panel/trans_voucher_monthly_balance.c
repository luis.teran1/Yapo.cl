#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <time.h>

#include "trans.h"
#include "util.h"
#include "ev_popen.h"
#include <ctemplates.h>
#include "validators.h"
#include <bconf.h>
#include <event.h>
#include "trans_mail.h"
#include <sys/signal.h>
enum vmb_state_e {
	VOUCHER_MONTHLY_BALANCE_DONE,
	VOUCHER_MONTHLY_BALANCE_ERROR,
	VOUCHER_MONTHLY_BALANCE_INIT,
	VOUCHER_MONTHLY_BALANCE_GET_STORES,
	VOUCHER_MONTHLY_BALANCE_NEXT_STORE,
	VOUCHER_MONTHLY_BALANCE_GET_DATA,
	VOUCHER_MONTHLY_BALANCE_MAIL
};
struct voucher_monthly_balance_state {
	enum vmb_state_e state;
	struct cmd *cs;
	int *stores;
	int num_stores;
	int curr_store;
	struct bufferevent *php_event;
	struct mail_data *md;
};

static struct c_param parameters[] = {
	{"store_id", 		0,		"v_integer"},
	{"date_to", 		0,		"v_date"},
	{NULL}
};

extern struct bconf_node *bconf_root;

static const char *voucher_monthly_balance_fsm(void *, struct sql_worker *, const char *);


/*****************************************************************************
 * voucher_monthly_balance_done
 * Exit function.
 **********/
static void
voucher_monthly_balance_done(void *v) {
	struct voucher_monthly_balance_state *state = v;
	struct cmd *cs = state->cs;
	if (state->stores)
		free(state->stores);
	if (state->md) {
		const char *fn = mail_get_param(state->md, "attachment");
		if (fn)
			unlink(fn);
		mail_free(state->md);
	}
	free(state);
	cmd_done(cs);
}

/*****************************************************************************
 * bind_insert
 * Initialise template API before a call to sql_worker_template_query().
 **********/
static void
bind_insert(struct bpapi *ba, struct voucher_monthly_balance_state *state) {
	struct c_param *param;

	for (param = parameters; param->name; param++) {
		if (cmd_haskey(state->cs, param->name))
			bpapi_insert(ba, param->name, cmd_getval(state->cs, param->name));
	}
}

static void
vmb_read_cb(void *v, struct bufferevent *be) {
	struct voucher_monthly_balance_state *state = v;
	char *line;
	const char *errstr;

	line = evbuffer_readline(be->input);
	if (!line)
		return; /* Wait for more data. */

	if (strcmp(line, "OK") == 0)
		errstr = NULL;
	else
		errstr = line;
	bufferevent_disable(be, EV_READ);
	voucher_monthly_balance_fsm(state, NULL, errstr);
	free(line);
}

static void
vmb_write_cb(void *v, struct bufferevent *be) {
	bufferevent_disable(be, EV_WRITE);
	bufferevent_enable(be, EV_READ);
}

static void
vmb_close_cb(void *v, int status) {
	struct voucher_monthly_balance_state *state = v;

	state->php_event = NULL;
	if (state->state != VOUCHER_MONTHLY_BALANCE_DONE && state->state != VOUCHER_MONTHLY_BALANCE_ERROR) {
		state->cs->error = "ERROR_PHP_EXITED";
		state->cs->status = "TRANS_EXEC_ERROR";
		return;
	}

	voucher_monthly_balance_fsm(state, NULL, NULL);
}

static void
vmd_mail_done_cb(struct mail_data *md, void *v, int status) {
	struct voucher_monthly_balance_state *state = v;
	struct cmd *cs = state->cs;
	const char *fn = mail_get_param(md, "attachment");

	if (fn)
		unlink(fn);
        if (status != 0) {
                cs->status = "TRANS_MAIL_ERROR";
        } else {
		transaction_logprintf(cs->ts, D_INFO, "Mail sent for store_id:%d", state->stores[state->curr_store - 1]);
	}

	voucher_monthly_balance_fsm(v, NULL, NULL);
}

/*****************************************************************************
 * voucher_monthly_balance_fsm
 * State machine
 **********/
static const char *
voucher_monthly_balance_fsm(void *v, struct sql_worker *worker, const char *errstr) {
	struct voucher_monthly_balance_state *state = v;
	struct cmd *cs = state->cs;
        struct bpapi ba;

	/* 
	 * General error handling for db errors.
	 */
	if (errstr) {
                transaction_logprintf(cs->ts, D_ERROR, "voucher_monthly_balance_fsm statement failed (%d, %s)", 
                                      state->state,
                                      errstr);
                cs->status = "TRANS_DATABASE_ERROR";
                cs->message = xstrdup(errstr);
		state->state = VOUCHER_MONTHLY_BALANCE_ERROR;
	}

	while (1) {
		transaction_logprintf(cs->ts, D_DEBUG, "voucher_monthly_balance_fsm(%p, %p, %p), state=%d", 
				      v, worker, errstr, state->state);

		switch (state->state) {
		case VOUCHER_MONTHLY_BALANCE_INIT:
			{
				/* next state */
				state->state = VOUCHER_MONTHLY_BALANCE_GET_STORES;

				int voucher_enabled = bconf_get_int(bconf_root, "*.*.common.voucher.enabled");
				if (!voucher_enabled) {
					state->state = VOUCHER_MONTHLY_BALANCE_DONE;
					continue;
				}

				/* Initialise the templateparse structure */
				BPAPI_INIT(&ba, NULL, pgsql);
				bind_insert(&ba, state);

				/* Register SQL query with our voucher_monthly_balance_fsm() as callback function */
				sql_worker_template_query(&config.pg_slave,
							  "sql/voucher_monthly_balance_get_stores.sql", &ba, 
							  voucher_monthly_balance_fsm, state, cs->ts->log_string);

				/* Clean-up */
				bpapi_output_free(&ba);
				bpapi_simplevars_free(&ba);
				bpapi_vtree_free(&ba);

				/* Return to libevent */
				return NULL;
			}
		case VOUCHER_MONTHLY_BALANCE_GET_STORES:
			state->stores = xmalloc(sizeof (*state->stores) * worker->rows(worker));
			state->num_stores = 0;
			while (worker->next_row(worker)) {
				int store_id = atoi(worker->get_value(worker, 0));
				state->stores[state->num_stores++] = store_id;
				transaction_logprintf(cs->ts, D_INFO, "Adding store to mailing list:%d", store_id);
			}
                        state->state = VOUCHER_MONTHLY_BALANCE_NEXT_STORE;
			continue;

                case VOUCHER_MONTHLY_BALANCE_NEXT_STORE:
			if (state->curr_store >= state->num_stores) {
				transaction_logprintf(cs->ts, D_INFO, "All mails sent");
				state->state = VOUCHER_MONTHLY_BALANCE_DONE;
				continue;
			}
			if (!state->php_event) {
				int res;
				char *cmd;
				const char *php_argv[] = {"php", "-q", "-c", NULL, NULL, NULL};

				ALLOCA_PRINTF(res, cmd, "%s/conf/php.ini", config.basedir);
				php_argv[3] = cmd;
				ALLOCA_PRINTF(res, cmd, "%s/libexec/voucher_monthly_report.php", config.basedir);
				php_argv[4] = cmd;
				transaction_logprintf(cs->ts, D_DEBUG, "Launching PHP helper: /usr/bin/php -q -c %s %s", php_argv[3], php_argv[4]);
				state->php_event = ev_popen("/usr/bin/php", php_argv, vmb_read_cb, vmb_write_cb, NULL, vmb_close_cb, state);
				bufferevent_disable(state->php_event, EV_WRITE);
				bufferevent_disable(state->php_event, EV_READ);
			}
			BPAPI_INIT_APP(&ba, host_bconf, "voucher_monthly_balance", buf, pgsql);
			bind_insert(&ba, state);

			bpapi_set_int(&ba, "store_id", state->stores[state->curr_store++]);

			sql_worker_template_query_flags(&config.pg_slave, SQLW_FINAL_CALLBACK,
					"sql/voucher_monthly_balance_get_data.sql", &ba,
					voucher_monthly_balance_fsm, state, cs->ts->log_string);
			state->state = VOUCHER_MONTHLY_BALANCE_GET_DATA;

			bpapi_output_free(&ba);
			bpapi_simplevars_free(&ba);
			bpapi_vtree_free(&ba);

                        return NULL;

		case VOUCHER_MONTHLY_BALANCE_GET_DATA:
		{
			int cols;
			int col;
			int set_md = 0;

			if (worker->sw_state == RESULT_DONE) {
				if (!state->php_event) {
					/* PHP error, exit. */
					state->state = VOUCHER_MONTHLY_BALANCE_ERROR;
					continue;
				}
				evbuffer_add_printf(state->php_event->output, ".\n");
				bufferevent_enable(state->php_event, EV_WRITE);
				state->state = VOUCHER_MONTHLY_BALANCE_MAIL;
				return NULL;
			}
			if (!state->php_event) {
				/* PHP error, finish select first. */
				return NULL;
			}
			if (!state->md) {
				state->md = zmalloc(sizeof(*state->md));
				state->md->command = state;
				state->md->callback = vmd_mail_done_cb;
				state->md->log_string = cs->ts->log_string;
				set_md = 1;
			}
			cols = worker->fields(worker);
			while (worker->next_row(worker)) {
				evbuffer_add_printf(state->php_event->output, "%s", worker->get_value(worker, 0));
				for (col = 1; col < cols; col++) {
					const char *key = worker->field_name(worker, col);
					const char *val = worker->get_value(worker, col);

					evbuffer_add_printf(state->php_event->output, "\t%s", val);
					if (strcmp(key, "filename") == 0)
						mail_insert_param(state->md, "attachment", val);
					if (set_md)
						mail_insert_param(state->md, key, val);
				}
				evbuffer_add_printf(state->php_event->output, "\n");
			}
			return NULL;
		}

		case VOUCHER_MONTHLY_BALANCE_MAIL:
			state->state = VOUCHER_MONTHLY_BALANCE_NEXT_STORE;
			if (state->md) {
				mail_send(state->md, "mail/voucher_monthly_balance.txt", NULL); /* XXX language */
				state->md = NULL;
				return NULL;
			}
			continue;

		case VOUCHER_MONTHLY_BALANCE_ERROR:
			transaction_logprintf(cs->ts, D_ERROR, "Error state");
			/* Fall through */
		case VOUCHER_MONTHLY_BALANCE_DONE:
			if (state->php_event) {
				pid_t pid = ev_popen_get_pid(state->php_event);

				if (pid) {
					transaction_logprintf(cs->ts, D_DEBUG, "Killing PHP helper pid %d", (int)pid);
					kill(pid, SIGINT);
					return NULL;
				} else
					transaction_logprintf(cs->ts, D_ERROR, "PHP helper pid not found");
			}
			voucher_monthly_balance_done(state);
			return NULL;

		default:
			/* MAJOR STATE ERROR */
			cs->status = "TRANS_ERROR";
			transaction_logprintf(cs->ts, D_ERROR, "Unknown state (%d)", 
					      state->state);
			return NULL;

		}
	}
}

/*****************************************************************************
 * voucher_monthly_balance
 * Main function that initialise a state-structure and calls the state machine.
 **********/
static void
voucher_monthly_balance(struct cmd *cs) {
	transaction_logprintf(cs->ts, D_DEBUG, "Start Voucher monthly balance transaction");
	struct voucher_monthly_balance_state *state;
	state = zmalloc(sizeof(*state));
	state->cs = cs;
	state->state = VOUCHER_MONTHLY_BALANCE_INIT;

	voucher_monthly_balance_fsm(state, NULL, NULL);
}

ADD_COMMAND(voucher_monthly_balance,     /* Name of command */
		NULL,
            voucher_monthly_balance,     /* Main function name */
            parameters,  /* Parameters struct */
	    "Voucher monthly balance command");/* Command description */
