#include "trans.h"
#include "validators.h"

static struct c_param validate_token_parameters[] = {
	{"remote_addr", 0, "v_ip"},
	{"remote_browser", 0, "v_remote_browser"},
	{"token", P_REQUIRED, "v_token"},
	{NULL, 0, NULL}
};

ADD_COMMAND(validate_token, NULL, NULL, validate_token_parameters, NULL);

