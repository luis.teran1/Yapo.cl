#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <time.h>

#include "trans.h"
#include "util.h"
#include <ctemplates.h>
#include "trans_mail.h"
#include "validators.h"
#include <bconf.h>
#include "trans_authenticate.h"

extern struct bconf_node *bconf_root;

#define AUTH_DONE 0
#define AUTH_ERROR 1
#define AUTH_CHECK_USER 2
#define AUTH_BEGIN_RESULT 3
#define AUTH_RESULT 4

static void auth_done(void *v) {
	struct auth_state *state = v;
	struct cmd *cs = state->cs;

	free(state);
	cmd_done(cs);
}

static const char *
authenticate_fsm(void *v, struct sql_worker *worker, const char *errstr) {
	struct auth_state *state = v;
	struct cmd *cs = state->cs;
	struct bpapi ba;

	transaction_logprintf(cs->ts, D_DEBUG, "auth fsm");

	/*
	 * General error handling for db errors.
	 */
	if (errstr) {
		if (strstr (errstr, "ERROR_AUTH_LOGIN")) {
			transaction_logprintf(cs->ts, D_WARNING, "Bad user/password");
			transaction_printf(cs->ts, "passwd:ERROR_AUTH_LOGIN\n");
			state->state = AUTH_ERROR;
			cs->status = "TRANS_ERROR";
		} else {
			cs->status = "TRANS_DATABASE_ERROR";
			cs->message = xstrdup(errstr);
			state->state = AUTH_ERROR;
		}
	}


	while (state->state != AUTH_DONE) {
		switch (state->state) {
		case AUTH_CHECK_USER:
			transaction_logprintf(cs->ts, D_DEBUG, "Auth check user");

			BPAPI_INIT(&ba, NULL, pgsql);
			if (cmd_haskey(cs, "username"))
				bpapi_insert(&ba, "username", cmd_getval(cs, "username"));
			if (cmd_haskey(cs, "passwd"))
				bpapi_insert(&ba, "passwd", cmd_getval(cs, "passwd"));
			if (cmd_haskey(cs, "remote_addr"))
				bpapi_insert(&ba, "remote_addr", cmd_getval(cs, "remote_addr"));
			if (cmd_haskey(cs, "auth_type"))
				bpapi_insert(&ba, "auth_type", cmd_getval(cs, "auth_type"));
			if (cmd_haskey(cs, "auth_resource"))
				bpapi_insert(&ba, "auth_resource", cmd_getval(cs, "auth_resource"));

			bpapi_insert(&ba, "valid_minutes",  bconf_value(bconf_get(bconf_root, "*.*.auth.validtime")));
			bpapi_insert(&ba, "info", cs->command->name);

			sql_worker_template_query(&config.pg_master, "sql/call_authenticate_admin.sql", &ba,
						  authenticate_fsm, state, cs->ts->log_string);
			bpapi_output_free(&ba);
			bpapi_simplevars_free(&ba);
			bpapi_vtree_free(&ba);
			state->state = AUTH_RESULT;
			return NULL;

		case AUTH_RESULT:
			if (worker->next_row(worker)) {
				transaction_logprintf(cs->ts, D_DEBUG, "Auth command done");
				transaction_printf(cs->ts, "token:%s\n", worker->get_value_byname(worker, "o_token"));
				transaction_printf(cs->ts, "admin_id:%s\n",
						   worker->get_value_byname(worker, "o_auth_id"));
				transaction_printf(cs->ts, "admin_privs:%s\n",
						   worker->get_value_byname(worker, "o_admin_privs"));
				state->state = AUTH_DONE;
				continue;
			}
			transaction_logprintf(cs->ts, D_ERROR, "No result from authenticate_admin");
			cs->status = "TRANS_DATABASE_ERROR";
			state->state = AUTH_ERROR;
			continue;

		case AUTH_ERROR:
			auth_done(state);
			return NULL;

		default:
			transaction_logprintf(cs->ts, D_ERROR, "Unknown auth state %d", state->state);
			cs->status = "TRANS_DATABASE_ERROR";
			state->state = AUTH_ERROR;
			continue;
		}
	}

	auth_done(state);
	return NULL;
}

static void
authenticate(struct cmd *cs) {
	transaction_logprintf(cs->ts, D_DEBUG, "Auth command started");
	struct auth_state *state;
	state = zmalloc(sizeof(*state));

	state->cs = cs;
	state->state = AUTH_CHECK_USER;

	authenticate_fsm(state, NULL, NULL);
}

struct validator v_auth_username[] = {
	VALIDATOR_LEN(2, -1, "ERROR_USERNAME_TOO_SHORT"),
	VALIDATOR_LEN(-1, 60, "ERROR_USERNAME_TOO_LONG"),
        VALIDATOR_NREGEX("[%^/\\()=#+]", REG_EXTENDED, "ERROR_USERNAME_INVALID"),
	{0}
};
ADD_VALIDATOR(v_auth_username);

struct validator v_auth_passwd[] = {
	VALIDATOR_LEN(5, -1, "ERROR_PASSWORD_TOO_SHORT"),
	VALIDATOR_LEN(-1, 56, "ERROR_PASSWORD_TOO_LONG"),
	{0}
};
ADD_VALIDATOR(v_auth_passwd);


static struct c_param auth_parameters[] = {
	{"username", P_REQUIRED, "v_auth_username"},
	{"passwd", P_REQUIRED, "v_auth_passwd"},
	{"remote_addr", 0, "v_ip"},
	{"remote_browser", 0, "v_remote_browser"},
	{"auth_type", 0, "v_array:admin,store"},
	{"auth_resource", 0, "v_string_isprint"},
	{NULL, 0, NULL}
};

ADD_COMMAND(authenticate, NULL, authenticate, auth_parameters, NULL);

