#include "factory.h"

static struct c_param parameters[] = {
	{"order_id", P_REQUIRED, "v_integer"},
	{"remote_addr", P_REQUIRED, "v_ip"},
	{"remote_browser", 0, "v_remote_browser"},
	{NULL, 0}
};


FACTORY_TRANSACTION(get_unique_order_id, &config.pg_master, "sql/get_unique_order_id.sql", NULL, parameters, FACTORY_NO_RES_CNTR|FACTORY_RES_SHORT_FORM);
