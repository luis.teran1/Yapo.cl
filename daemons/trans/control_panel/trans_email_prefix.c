#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <time.h>

#include "trans.h"
#include "util.h"
#include <ctemplates.h>
#include "validators.h"
#include <bconf.h>
#include <event.h>

#define EMAIL_PREFIX_DONE       0
#define EMAIL_PREFIX_ERROR      1
#define EMAIL_PREFIX_INIT       2
#define EMAIL_PREFIX_GET_COUNT  3
#define EMAIL_PREFIX_GET_EMAILS 4

struct email_prefix_state {
	int state;
	struct cmd *cs;
};

static struct c_param parameters[] = {
	{"email", P_REQUIRED, "v_string_isprint"},
	{NULL, 0}
};

static const char *email_prefix_fsm(void *, struct sql_worker *, const char *);

extern struct bconf_node *bconf_root;


/*****************************************************************************
 * email_prefix_done
 * Exit function.
 **********/
static void
email_prefix_done(void *v) {
	struct email_prefix_state *state = v;
	struct cmd *cs = state->cs;
	free(state);
	cmd_done(cs);
}

/*****************************************************************************
 * bind_insert
 * Initialise template API before a call to sql_worker_template_query().
 **********/
static void
bind_insert(struct bpapi *ba, struct email_prefix_state *state) {
	bpapi_insert(ba, "email", cmd_getval(state->cs, "email"));
}

/*****************************************************************************
 * email_prefix_fsm
 * State machine
 **********/
static const char *
email_prefix_fsm(void *v, struct sql_worker *worker, const char *errstr) {
	struct email_prefix_state *state = v;
	struct cmd *cs = state->cs;
        struct bpapi ba;

	/* 
	 * General error handling for db errors.
	 */
	if (errstr) {
                transaction_logprintf(cs->ts, D_ERROR, "email_prefix_fsm statement failed (%d, %s)", 
                                      state->state,
                                      errstr);
                cs->status = "TRANS_DATABASE_ERROR";
                cs->message = xstrdup(errstr);
		state->state = EMAIL_PREFIX_ERROR;
                if (worker)
			return "ROLLBACK";
        }

	while (1) {
		transaction_logprintf(cs->ts, D_DEBUG, "email_prefix_fsm(%p, %p, %p), state=%d", 
				      v, worker, errstr, state->state);

		switch (state->state) {
		case EMAIL_PREFIX_INIT:
			/* next state */
			state->state = EMAIL_PREFIX_GET_COUNT;

			/* Initialise the templateparse structure */
			BPAPI_INIT(&ba, NULL, pgsql);
			bind_insert(&ba, state);

			/* Register SQL query with our email_prefix_fsm() as callback function */
			sql_worker_template_query(&config.pg_slave, 
						  "sql/email_prefix_count.sql", &ba, 
						  email_prefix_fsm, state, cs->ts->log_string);

			/* Clean-up */
			bpapi_output_free(&ba);
			bpapi_simplevars_free(&ba);
			bpapi_vtree_free(&ba);

			/* Return to libevent */
			return NULL;

		case EMAIL_PREFIX_GET_COUNT:
		{
			int count;

			worker->next_row(worker);
			count = atoi(worker->get_value(worker, 0));

			transaction_printf(cs->ts, "count:%d\n", count);
			if (count && count <= bconf_get_int(bconf_root, "*.trans.email_prefix.max_limit")) {
				/* A valid count, return emails */
				BPAPI_INIT(&ba, NULL, pgsql);
				bind_insert(&ba, state);

				sql_worker_template_newquery(worker, "sql/email_prefix_get.sql", &ba);
				bpapi_output_free(&ba);
				bpapi_simplevars_free(&ba);
				bpapi_vtree_free(&ba);

				state->state = EMAIL_PREFIX_GET_EMAILS;
				return NULL;
			}
			state->state = EMAIL_PREFIX_DONE;
			continue;
		}

                case EMAIL_PREFIX_GET_EMAILS:
			while (worker->next_row(worker))
				transaction_printf(cs->ts, "email:%s\n", worker->get_value(worker, 0));
			state->state = EMAIL_PREFIX_DONE;
                        continue;

		case EMAIL_PREFIX_DONE:
			email_prefix_done(state);
			return NULL;

		case EMAIL_PREFIX_ERROR:
			transaction_logprintf(cs->ts, D_ERROR, "Error state");
			email_prefix_done(state);
			return NULL;

		default:
			/* MAJOR STATE ERROR */
			cs->status = "TRANS_ERROR";
			transaction_logprintf(cs->ts, D_ERROR, "Unknown state (%d)", 
					      state->state);
			email_prefix_done(state);
			return NULL;

		}
	}
}

/*****************************************************************************
 * email_prefix
 * Main function that initialise a state-structure and calls the state machine.
 **********/
static void
email_prefix(struct cmd *cs) {
	transaction_logprintf(cs->ts, D_DEBUG, "Start Email_prefix transaction");
	struct email_prefix_state *state;
	state = zmalloc(sizeof(*state));
	state->cs = cs;
	state->state = EMAIL_PREFIX_INIT;

	email_prefix_fsm(state, NULL, NULL);
}

ADD_COMMAND(email_prefix,     /* Name of command */
		NULL,
            email_prefix,     /* Main function name */
            parameters,  /* Parameters struct */
            "Email_prefix command");/* Command description */
