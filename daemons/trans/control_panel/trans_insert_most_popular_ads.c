#include "factory.h"

static struct c_param parameters[] = {
	{"quantity",   P_REQUIRED | P_MULTI, "v_integer"},
	{"list_id",   P_REQUIRED | P_MULTI, "v_integer"     },
	{"type",   P_REQUIRED | P_MULTI, "v_string_isprint"     },
	{"stat_time",   P_REQUIRED, "v_string_isprint"     },
	{NULL, 0}
};

FACTORY_TRANSACTION(insert_most_popular_ads, &config.pg_master, "sql/call_update_most_popular_ads.sql", NULL, parameters, FACTORY_NO_RES_CNTR | FACTORY_EMPTY_RESULT_OK | FACTORY_RES_SHORT_FORM );

