#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <time.h>

#include "trans.h"
#include "command.h"
#include "util.h"
#include <ctemplates.h>
#include "validators.h"
#include <bconf.h>
#include <event.h>

#define FLUSHQUEUE_DONE  0
#define FLUSHQUEUE_ERROR 1
#define FLUSHQUEUE_INIT  2
#define FLUSHQUEUE_LOCKED 3
#define FLUSHQUEUE_EXECUTE 4
#define FLUSHQUEUE_WAIT 5

struct flushqueue_state {
	int state;
        int flush_cnt;
        int flush_done;
	int nstarted;
	int nexecuting;
	int limit;
	TAILQ_HEAD(,flush_cmd) queued_cmds;
	TAILQ_HEAD(,flush_cmd) running_cmds;
	struct cmd *cs;
        struct event timeout_event;
};

struct flush_cmd {
	struct internal_cmd *cmd;
	char *trans_queue_id;
	char *result;
	char *status;
	int len;
	int pos;
        struct flushqueue_state *state;
	TAILQ_ENTRY(flush_cmd) list;
};

static struct c_param parameters[] = {
	{"queue", P_REQUIRED, "v_queue_queue"},
	{"per_info", 0, "v_bool"},
	{"maxtime", 0, "v_integer"},
	{NULL, 0}
};

static const char *flushqueue_fsm(void *, struct sql_worker *, const char *);
extern char *trans_user;
extern struct bconf_node *bconf_root;


/*****************************************************************************
 * flushqueue_done
 * Exit function.
 **********/
static void
flushqueue_done(void *v) {
	struct flushqueue_state *state = v;
	struct cmd *cs = state->cs;

	transaction_logprintf(cs->ts, D_DEBUG, "Flushqueue transaction done");

	while (!TAILQ_EMPTY(&state->queued_cmds)) {
		struct flush_cmd *cmd = TAILQ_FIRST(&state->queued_cmds);
		
		TAILQ_REMOVE(&state->queued_cmds, cmd, list);
		free(cmd);
	}
	while (!TAILQ_EMPTY(&state->running_cmds)) {
		struct flush_cmd *cmd = TAILQ_FIRST(&state->running_cmds);
		
		TAILQ_REMOVE(&state->running_cmds, cmd, list);
		free(cmd);
	}
	free(state);
	cmd_done(cs);
}

/*****************************************************************************
 * bind_insert
 * Initialise template API before a call to sql_worker_template_query().
 **********/
static void
bind_insert(struct bpapi *ba, struct flushqueue_state *state) {

	bpapi_insert(ba, "queue", cmd_getval(state->cs, "queue"));

        if (cmd_haskey(state->cs, "maxtime"))
                bpapi_insert(ba, "maxtime", cmd_getval(state->cs, "maxtime"));

        if (state->limit) {
                bpapi_set_int(ba, "limit", state->limit);
        }
}

static const char *
flushqueue_update_cb(void *v, struct sql_worker *worker, const char *errst) {
	struct flushqueue_state *state = v;

	transaction_logprintf (state->cs->ts, D_DEBUG, "flushqueue_update_cb state = %p", state);
        state->flush_done++;
        if (--state->nexecuting == 0) {
		/* Make sure the queue is empty when we return to fsm. */
		while (!TAILQ_EMPTY(&state->running_cmds)) {
			struct flush_cmd *cmd = TAILQ_FIRST(&state->running_cmds);
			
			TAILQ_REMOVE(&state->running_cmds, cmd, list);
			free(cmd);
		}
                flushqueue_fsm(state, NULL, NULL);
        }
	return NULL;
}

static void
flushqueue_run_cb(struct internal_cmd *v, const char *line, void *data) {
        struct bpapi ba;
	struct flush_cmd *cmd = (struct flush_cmd*)v->data;

	DPRINTF(D_DEBUG, ("flushqueue_run_cb(%p, '%s')", cmd, line));

	if (line) {
		if (!strncmp(line, "status:", 7))
			cmd->status = xstrdup(line + 7);
		bufcat(&cmd->result, &cmd->len, &cmd->pos, "%s\n", line);
	} else {
		BPAPI_INIT(&ba, NULL, pgsql);
		bpapi_insert(&ba, "result", cmd->result);
		bpapi_insert(&ba, "status", cmd->status);
		bpapi_insert(&ba, "trans_queue_id", cmd->trans_queue_id);
		bpapi_insert(&ba, "executed_by", trans_user);

		sql_worker_template_query("pgsql.queue",
					  "sql/update_queue_result.sql", &ba, 
					  flushqueue_update_cb, (void *)cmd->state, cmd->state->cs->ts->log_string);
		bpapi_output_free(&ba);
		bpapi_simplevars_free(&ba);
		bpapi_vtree_free(&ba);
		if (cmd->result)
			free(cmd->result);
		if (cmd->status)
			free(cmd->status);
		if (cmd->trans_queue_id)
			free(cmd->trans_queue_id);
	}
		
		
}

/*****************************************************************************
 * flushqueue_fsm
 * State machine
 **********/
static const char *
flushqueue_fsm(void *v, struct sql_worker *worker, const char *errstr) {
	struct flushqueue_state *state = v;
	struct cmd *cs = state->cs;
        struct bpapi ba;

	transaction_logprintf(cs->ts, D_DEBUG, "flushqueue_fsm(%p, %p, %p), state=%d", 
                              v, worker, errstr, state->state);

	/* 
	 * General error handling for db errors.
	 */
	if (errstr) {
                transaction_logprintf(cs->ts, D_ERROR, "Statement failed (%d, %s)", 
                                      state->state,
                                      errstr);
                cs->status = "TRANS_DATABASE_ERROR";
                cs->message = xstrdup(errstr);
		state->state = FLUSHQUEUE_ERROR;
        }

	while (1) {
		switch (state->state) {
		case FLUSHQUEUE_INIT:
			/* next state */
			state->state = FLUSHQUEUE_LOCKED;

			/* Initialise the templateparse structure */
			BPAPI_INIT(&ba, NULL, pgsql);
			bind_insert(&ba, state);

			/* Register SQL query with our flushqueue_fsm() as callback function */
			sql_worker_template_query("pgsql.queue",
						  cmd_haskey(cs, "per_info") ? "sql/call_lock_queue_per_info.sql" :
						  "sql/call_lock_queue_command.sql", &ba, 
						  flushqueue_fsm, state, cs->ts->log_string);

			/* Clean-up */
			bpapi_output_free(&ba);
			bpapi_simplevars_free(&ba);
			bpapi_vtree_free(&ba);

			/* Return to libevent */
			return NULL;

		case FLUSHQUEUE_LOCKED:
		{
			struct flush_cmd *cmd;

                        state->nstarted = 0;
			while (worker->next_row(worker)) {
				cmd = zmalloc (sizeof (*cmd));
				cmd->trans_queue_id = xstrdup(worker->get_value(worker, 0));
				cmd->cmd = zmalloc(sizeof (*cmd->cmd));
				cmd->cmd->cmd_string = xstrdup(worker->get_value(worker, 1));
				cmd->cmd->cb = flushqueue_run_cb;
				cmd->cmd->data = cmd;
                                cmd->state = state;
				TAILQ_INSERT_TAIL(&state->queued_cmds, cmd, list);
                                state->flush_cnt++;
                                state->nstarted++;
			}
                        transaction_logprintf(cs->ts, D_DEBUG, 
                                              "Will be starting %d internal transactions", state->nstarted);
                        if (state->nstarted == 0) {
                                state->state = FLUSHQUEUE_DONE;
                                transaction_printf(cs->ts, "flushed:%d\n", state->flush_cnt);
                        } else {
                                state->state = FLUSHQUEUE_EXECUTE;
                        }
			continue;
		}
			
		case FLUSHQUEUE_EXECUTE:
		{
			struct flush_cmd *cmd;
			int i;

                        state->state = FLUSHQUEUE_WAIT;
			for (i = 0 ; i < state->limit && !TAILQ_EMPTY (&state->queued_cmds) ; i++) {
				state->nexecuting++;
				cmd = TAILQ_FIRST (&state->queued_cmds);
				TAILQ_REMOVE (&state->queued_cmds, cmd, list);
				TAILQ_INSERT_TAIL (&state->running_cmds, cmd, list);
                                transaction_logprintf(cs->ts, D_DEBUG, "Executing command (%p, %s)",
                                                      cmd, cmd->cmd->cmd_string);
                                transaction_internal(cmd->cmd);
                        }
			return NULL;
		}

		case FLUSHQUEUE_WAIT:
                        /* Callback from flushqueue_run_cb */
			if (!TAILQ_EMPTY (&state->queued_cmds)) {
				state->state = FLUSHQUEUE_EXECUTE;
				continue;
			}
			if (cmd_haskey(cs, "per_info")) {
                                transaction_printf(cs->ts, "flushed:%d\n", state->flush_cnt);
				state->state = FLUSHQUEUE_DONE;
			} else
                        	state->state = FLUSHQUEUE_INIT;
			continue;

		case FLUSHQUEUE_DONE:
			flushqueue_done(state);
			return NULL;

		case FLUSHQUEUE_ERROR:
			transaction_logprintf(cs->ts, D_ERROR, "Error state");
			flushqueue_done(state);
			return NULL;

		default:
			/* MAJOR STATE ERROR */
			cs->status = "TRANS_ERROR";
			transaction_logprintf(cs->ts, D_ERROR, "Unknown state (%d)", 
					      state->state);
			return NULL;

		}
	}
}

/*****************************************************************************
 * flushqueue
 * Main function that initialise a state-structure and calls the state machine.
 **********/
static void
flushqueue(struct cmd *cs) {
        const char *limitc = bconf_value(bconf_vget(bconf_root, "*.*.trans.flushqueue", cmd_getval(cs, "queue"), "max_threads", NULL));
        int limit = 10;
	
	if (!limitc)
		limitc = bconf_value(bconf_get(bconf_root, "*.*.trans.flushqueue.max_threads"));
        if (limitc)
                limit = atoi(limitc);

	transaction_logprintf(cs->ts, D_DEBUG, "Start Flushqueue transaction");
	struct flushqueue_state *state;
	state = zmalloc(sizeof(*state));
	state->cs = cs;
	state->state = FLUSHQUEUE_INIT;
	state->limit = limit;
	TAILQ_INIT(&state->queued_cmds);
	TAILQ_INIT(&state->running_cmds);

	flushqueue_fsm(state, NULL, NULL);
}

ADD_COMMAND(flushqueue,     /* Name of command */
		NULL,
            flushqueue,     /* Main function name */
            parameters,  /* Parameters struct */
            "Flushqueue command");/* Command description */
