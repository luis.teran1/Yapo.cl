#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <time.h>

#include "trans.h"
#include "util.h"
#include <ctemplates.h>
#include "validators.h"
#include <bconf.h>
#include <event.h>
#include "trans_mail.h"

#define MAIL_VOUCHER_FILL_DONE  0
#define MAIL_VOUCHER_FILL_ERROR 1
#define MAIL_VOUCHER_FILL_INIT  2

struct mail_voucher_fill_state {
	int state;
	struct cmd *cs;
};

static struct c_param parameters[] = {
	{"store_name", P_REQUIRED, "v_name"},
	{"email", P_REQUIRED, "v_email"},
	{"amount", P_REQUIRED, "v_integer"},
	{"balance", P_REQUIRED, "v_integer"},
	{"date", P_REQUIRED, "v_timestamp"},
	{"order_id", P_REQUIRED, "v_order_id_simple"},
	{NULL, 0}
};

static const char *mail_voucher_fill_fsm(void *, struct sql_worker *, const char *);


/*****************************************************************************
 * mail_voucher_fill_done
 * Exit function.
 **********/
static void
mail_voucher_fill_done(void *v) {
	struct mail_voucher_fill_state *state = v;
	struct cmd *cs = state->cs;
	free(state);
	cmd_done(cs);
}

static void
voucher_filled_mail_cb(struct mail_data *md, void *v, int status) {
	struct mail_voucher_fill_state *state = v;
	struct cmd *cs = state->cs;

        if (status != 0) {
		transaction_logprintf(cs->ts, D_ERROR, "VOUCHER_FILL-SENDMAIL ERROR: Unable to send mail");
		mail_voucher_fill_fsm(state, NULL, "TRANS_MAIL_ERROR");
                return;
        }

        transaction_logprintf(cs->ts, D_INFO, "VOUCHER_FILL-SENDMAIL: 200 Mail sent OK");
	mail_voucher_fill_fsm(state, NULL, NULL);
}


/*****************************************************************************
 * mail_voucher_fill_fsm
 * State machine
 **********/
static const char *
mail_voucher_fill_fsm(void *v, struct sql_worker *worker, const char *errstr) {
	struct mail_voucher_fill_state *state = v;
	struct cmd *cs = state->cs;

	/*
	 * General error handling for db errors.
	 */
	if (errstr) {
                transaction_logprintf(cs->ts, D_ERROR, "mail_voucher_fill_fsm statement failed (%d, %s)",
                                      state->state,
                                      errstr);
                cs->status = "TRANS_DATABASE_ERROR";
                cs->message = xstrdup(errstr);
		state->state = MAIL_VOUCHER_FILL_ERROR;
		if (worker) {
			/* Turn of final callback, if set. */
			sql_worker_set_wflags(worker, 0);
			return "ROLLBACK";
		}
        }

	while (1) {
		transaction_logprintf(cs->ts, D_DEBUG, "mail_voucher_fill_fsm(%p, %p, %p), state=%d",
				      v, worker, errstr, state->state);

		switch (state->state) {
		case MAIL_VOUCHER_FILL_INIT:
			{
				float vat;
				char *s_vat;
				time_t t;
				char date[128];
				char *ref;
				char *apos = NULL;
				struct tm tm = {0};
				struct mail_data *md;

				/* next state */
				state->state = MAIL_VOUCHER_FILL_DONE;

				/* Send email */
				md = zmalloc(sizeof (*md));
				md->command = state;
				md->callback = voucher_filled_mail_cb;
				md->log_string = cs->ts->log_string;

				/* Add email paramaters */
				mail_insert_param(md, "to", cmd_getval(cs, "email"));
				mail_insert_param(md, "store_name", cmd_getval(cs, "store_name"));
				mail_insert_param(md, "balance", cmd_getval(cs, "balance"));
				mail_insert_param(md, "amount", cmd_getval(cs, "amount"));

				ref = xstrdup(cmd_getval(cs, "order_id"));
				apos = strchr(ref, 'a');
				if (apos)
					*apos = '\0';
				mail_insert_param(md, "ref", ref);
				free(ref);


				vat = atoi(cmd_getval(cs, "amount")) * .2;
				xasprintf(&s_vat, "%.2f", vat);
				mail_insert_param(md, "vat", s_vat);
				free(s_vat);

				time(&t);
				strftime(date, sizeof(date), "%e %B %Y %H:%M:%S", localtime_r(&t, &tm));
				mail_insert_param(md, "date", date);

				mail_send(md, "mail/voucher_filled.txt", NULL); /* XXX language */

				/* Return to libevent */
				return NULL;
			}
		case MAIL_VOUCHER_FILL_DONE:
			mail_voucher_fill_done(state);
			return NULL;

		case MAIL_VOUCHER_FILL_ERROR:
			transaction_logprintf(cs->ts, D_ERROR, "Error state");
			mail_voucher_fill_done(state);
			return NULL;

		default:
			/* MAJOR STATE ERROR */
			cs->status = "TRANS_ERROR";
			transaction_logprintf(cs->ts, D_ERROR, "Unknown state (%d)", 
					      state->state);
			return NULL;

		}
	}
}

/*****************************************************************************
 * mail_voucher_fill
 * Main function that initialise a state-structure and calls the state machine.
 **********/
static void
mail_voucher_fill(struct cmd *cs) {
	transaction_logprintf(cs->ts, D_DEBUG, "Start mail_voucher_fill transaction");
	struct mail_voucher_fill_state *state;
	state = zmalloc(sizeof(*state));
	state->cs = cs;
	state->state = MAIL_VOUCHER_FILL_INIT;

	mail_voucher_fill_fsm(state, NULL, NULL);
}

ADD_COMMAND(mail_voucher_fill,     /* Name of command */
		NULL,
            mail_voucher_fill,     /* Main function name */
            parameters,  /* Parameters struct */
            "mail_voucher_fill command");/* Command description */
