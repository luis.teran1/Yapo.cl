#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <time.h>

#include "trans.h"
#include <ctemplates.h>
#include "util.h"
#include "validators.h"

#define LISTADMINS_DONE 0
#define LISTADMINS_ERROR 1
#define LISTADMINS_INIT 2
#define LISTADMINS_RECIEVE 3 

struct listadmins_state {
	int state;
	struct cmd *cs;
};


static void listadmins_done(void *v) {
	struct listadmins_state *state = v;
	struct cmd *cs = state->cs;

	transaction_logprintf(cs->ts, D_DEBUG, "listadmins done");
	free(state);
	cmd_done(cs);
}


static const char *
listadmins_fsm(void *v, struct sql_worker *worker, const char *errstr) {
	struct listadmins_state *state = v;
	struct cmd *cs = state->cs;
	struct bpapi ba;
	int count = 0;
        int i;

	transaction_logprintf(cs->ts, D_DEBUG, "listadmins fsm");

	if (state->state == LISTADMINS_ERROR) {
		listadmins_done(state);
		return NULL;
	}

	/* 
	 * General error handling for db errors.
	 */

	if (errstr) {
                cs->status = "TRANS_DATABASE_ERROR";
                cs->message = xstrdup(errstr);
		state->state = LISTADMINS_ERROR;
		listadmins_done(state);
		return NULL;
        }
	
	while (state->state != LISTADMINS_DONE) {
		switch(state->state) {
		case LISTADMINS_INIT:
			transaction_logprintf(cs->ts, D_DEBUG, "Listadmins check user");

			BPAPI_INIT(&ba, NULL, pgsql);
			if (cmd_getval(cs, "admin_id")) {
				bpapi_insert(&ba, "admin_id", cmd_getval(cs, "admin_id"));
			}
			/* Use master for up to date data. */
			sql_worker_template_query(&config.pg_master, "sql/listadmins.sql", &ba,
						  listadmins_fsm, state, cs->ts->log_string);
			bpapi_output_free(&ba);
			bpapi_simplevars_free(&ba);
			bpapi_vtree_free(&ba);
			state->state = LISTADMINS_RECIEVE;
			return NULL;

		case LISTADMINS_RECIEVE:
			while (worker->next_row(worker)) {
				int nfields = worker->fields(worker);
				for (i = 0; i < nfields; i++) {
					transaction_printf(cs->ts, "admin.%d.%s:%s\n",
							   count, 
							   worker->field_name(worker, i),
							   worker->get_value(worker, i) ? worker->get_value(worker, i) : "");
				}

				count++;
			}

			state->state = LISTADMINS_DONE;
			continue;
		}
	}

	listadmins_done(state);
	return NULL;
}

static void
listadmins(struct cmd *cs) {
	struct listadmins_state *state;

	transaction_logprintf(cs->ts, D_DEBUG, "Listadmins command started");
	state = zmalloc(sizeof(*state));

	state->cs = cs;
	state->state = LISTADMINS_INIT;

	listadmins_fsm(state, NULL, NULL);
}

static struct c_param listadmins_parameters[] = {
	{"token",		P_REQUIRED,	"v_token:admin"},
	{"admin_id",		0, 		"v_integer"},
	{"remote_addr", 	P_REQUIRED,  	"v_ip"},
	{"remote_browser", 	0,  	  	"v_remote_browser"},
	{NULL, 0, NULL}
};

ADD_COMMAND(listadmins, NULL, listadmins, listadmins_parameters, NULL);

