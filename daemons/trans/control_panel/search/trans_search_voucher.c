#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <time.h>

#include "trans.h"
#include "util.h"
#include <ctemplates.h>
#include "validators.h"
#include <bconf.h>
#include <event.h>

#define SEARCH_VOUCHER_DONE  0
#define SEARCH_VOUCHER_ERROR 1
#define SEARCH_VOUCHER_INIT  2
#define SEARCH_VOUCHER_COUNT 3
#define SEARCH_VOUCHER_RESULT 4

struct search_voucher_state {
	int state;
	int offset;
	int limit;

	struct cmd *cs;
};

static struct c_param parameters[] = {
	{"remote_addr",         P_REQUIRED, "v_ip"},
	{"remote_browser",      0, "v_remote_browser"},
	{"store_id",		P_REQUIRED, "v_id"},
	{"offset",	0,	"v_integer"},
	{"limit",	0,	"v_integer"},

        {"token",               P_REQUIRED,       "v_store_token:store_id"},
	{NULL, 0}
};

static const char *search_voucher_fsm(void *, struct sql_worker *, const char *);
extern struct bconf_node *bconf_root;

/*****************************************************************************
 * search_voucher_log_done
 * Exit function.
 **********/
static void
search_voucher_done(void *v) {
	struct search_voucher_state *state = v;
	struct cmd *cs = state->cs;
	free(state);
	cmd_done(cs);
}

/*****************************************************************************
 * bind_insert
 * Initialise template API before a call to sql_worker_template_query().
 **********/
static void
bind_insert(struct bpapi *ba, struct search_voucher_state *state) {
	struct c_param *param;

	for (param = parameters; param->name; param++) {
		if (cmd_haskey(state->cs, param->name))
			bpapi_insert(ba, param->name, cmd_getval(state->cs, param->name));
	}
}

/*****************************************************************************
 * search_pay_log_fsm
 * State machine
 **********/
static const char *
search_voucher_fsm(void *v, struct sql_worker *worker, const char *errstr) {
	struct search_voucher_state *state = v;
	struct cmd *cs = state->cs;
        struct bpapi ba;

	/* 
	 * General error handling for db errors.
	 */
	if (errstr) {
                transaction_logprintf(cs->ts, D_ERROR, "search_voucher_fsm statement failed (%d, %s)", 
                                      state->state,
                                      errstr);
		cs->status = "TRANS_DATABASE_ERROR";
		cs->message = xstrdup(errstr);
		
		state->state = SEARCH_VOUCHER_ERROR;
        }

	while (1) {
		transaction_logprintf(cs->ts, D_DEBUG, "search_voucher_fsm(%p, %p, %p), state=%d", 
				      v, worker, errstr, state->state);

		switch (state->state) {
		case SEARCH_VOUCHER_INIT:
		{
			/* next state */
			state->state = SEARCH_VOUCHER_COUNT;

			/* Initialise the templateparse structure */
			BPAPI_INIT_APP(&ba, host_bconf, "controlpanel", buf, pgsql);
			bind_insert(&ba, state);

			/* Register SQL query with our search_voucher_fsm() as callback function */
			/* Make sure to switch to slave if read-only query. */
			sql_worker_template_query(&config.pg_slave, 
							  "sql/search_voucher.sql", &ba, 
							  search_voucher_fsm, state, cs->ts->log_string);
			/* Clean-up */
			bpapi_output_free(&ba);
			bpapi_simplevars_free(&ba);
			bpapi_vtree_free(&ba);

			/* Return to libevent */
			return NULL;
		}

		case SEARCH_VOUCHER_COUNT:
			if (worker->next_row(worker)) {
				transaction_printf(cs->ts, "total:%s\n", worker->get_value(worker, 0));
			}
			state->state = SEARCH_VOUCHER_RESULT;
			return NULL;

                case SEARCH_VOUCHER_RESULT:
		{
			int j;
			int i = 0;

			transaction_logoff(cs->ts);
			while (worker->next_row(worker)) {
				for (j = 0; j < worker->fields(worker); j++) {
					const char *val = worker->get_value(worker, j);

					if (val[0] == '{') {
						const char *n;

						for (val++; val && *val; val = n) {
							n = strchr(val, ',');
							if (!n)
								n = strchr(val, '}');
							if (!n)
								transaction_printf(cs->ts, "result.%d.%s:%s\n",
										   i,
										   worker->field_name(worker, j),
										   val);
							else if (n != val)
								transaction_printf(cs->ts, "result.%d.%s:%.*s\n",
										i,
										worker->field_name(worker, j),
										(int)(n++ - val),
										val);
							else
								n++;
						}
					} else if (val[0])
						transaction_printf(cs->ts, "result.%d.%s:%s\n",
								   i,
								   worker->field_name(worker, j),
								   val);
				}
				i++;
			}
			transaction_logon(cs->ts);

			state->state = SEARCH_VOUCHER_DONE;
		}

		case SEARCH_VOUCHER_DONE:
			search_voucher_done(state);
			return NULL;

		case SEARCH_VOUCHER_ERROR:
			transaction_logprintf(cs->ts, D_ERROR, "Error state");
			search_voucher_done(state);
			return NULL;

		default:
			/* MAJOR STATE ERROR */
			cs->status = "TRANS_ERROR";
			transaction_logprintf(cs->ts, D_ERROR, "Unknown state (%d)", 
					      state->state);
			return NULL;

		}
	}
}

/*****************************************************************************
 * search_pay_log
 * Main function that initialise a state-structure and calls the state machine.
 **********/
static void
search_voucher(struct cmd *cs) {
	transaction_logprintf(cs->ts, D_DEBUG, "Start Search_voucher transaction");
	struct search_voucher_state *state;
	state = zmalloc(sizeof(*state));
	state->cs = cs;
	state->state = SEARCH_VOUCHER_INIT;

	search_voucher_fsm(state, NULL, NULL);
}

ADD_COMMAND(search_voucher,     /* Name of command */
		NULL,
            search_voucher,     /* Main function name */
            parameters,  /* Parameters struct */
            "Search_voucher command");/* Command description */

