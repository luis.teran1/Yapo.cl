#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <time.h>

#include "trans.h"
#include "util.h"
#include <ctemplates.h>
#include "validators.h"
#include <bconf.h>
#include <event.h>

#define SEARCH_MAIL_QUEUE_DONE  0
#define SEARCH_MAIL_QUEUE_ERROR 1
#define SEARCH_MAIL_QUEUE_INIT  2
#define SEARCH_MAIL_QUEUE_RESULT 3

struct search_mail_queue_state {
	int state;
	struct cmd *cs;
};

static struct c_param parameters[] = {
	{"token",               P_REQUIRED, "v_token:search.paylog"},
	{"remote_addr",         P_REQUIRED, "v_ip"},
	{"remote_browser",      0, "v_remote_browser"},
	{"state",		0, "v_array:sent,blocked,reg,manual,abuse"},
	{"search_key",		P_REQUIRED,	"v_array:remote_addr,sender_name,receipient_name,sender_email,receipient_email,list_id,sender_phone"},
	{"search_value",	0,	"v_search_value"},
	{"timestamp_from",	P_REQUIRED,	"v_timestamp"},
	{"timestamp_to", 	P_REQUIRED,	"v_timestamp"},
	{NULL, 0}
};

static const char *search_mail_queue_fsm(void *, struct sql_worker *, const char *);

extern struct bconf_node *bconf_root;

/*****************************************************************************
 * search_mail_queue_done
 * Exit function.
 **********/
static void
search_mail_queue_done(void *v) {
	struct search_mail_queue_state *state = v;
	struct cmd *cs = state->cs;
	free(state);
	cmd_done(cs);
}

/*****************************************************************************
 * bind_insert
 * Initialise template API before a call to sql_worker_template_query().
 **********/
static void
bind_insert(struct bpapi *ba, struct search_mail_queue_state *state) {
	struct c_param *param;

	for (param = parameters; param->name; param++) {
		if (cmd_haskey(state->cs, param->name))
			bpapi_insert(ba, param->name, cmd_getval(state->cs, param->name));
	}
}

/*****************************************************************************
 * search_mail_queue_fsm
 * State machine
 **********/
static const char *
search_mail_queue_fsm(void *v, struct sql_worker *worker, const char *errstr) {
	struct search_mail_queue_state *state = v;
	struct cmd *cs = state->cs;
        struct bpapi ba;

	/* 
	 * General error handling for db errors.
	 */
	if (errstr) {
                transaction_logprintf(cs->ts, D_ERROR, "search_mail_queue_fsm statement failed (%d, %s)", 
                                      state->state,
                                      errstr);
                cs->status = "TRANS_DATABASE_ERROR";
                cs->message = xstrdup(errstr);
		state->state = SEARCH_MAIL_QUEUE_ERROR;
        }

	while (1) {
		transaction_logprintf(cs->ts, D_DEBUG, "search_mail_queue_fsm(%p, %p, %p), state=%d", 
				      v, worker, errstr, state->state);

		switch (state->state) {
		case SEARCH_MAIL_QUEUE_INIT:
			/* next state */
			state->state = SEARCH_MAIL_QUEUE_RESULT;

			/* Initialise the templateparse structure */
			BPAPI_INIT_APP(&ba, host_bconf, "controlpanel", buf, pgsql);
			bind_insert(&ba, state);

			/* Register SQL query with our search_mail_queue_fsm() as callback function */
			/* Make sure to switch to slave if read-only query. */
			sql_worker_template_query(&config.pg_slave, 
						  "sql/search_mail_queue.sql", &ba, 
						  search_mail_queue_fsm, state, cs->ts->log_string);

			/* Clean-up */
			bpapi_output_free(&ba);
			bpapi_simplevars_free(&ba);
			bpapi_vtree_free(&ba);

			/* Return to libevent */
			return NULL;

                case SEARCH_MAIL_QUEUE_RESULT:
			{
				int i, j;
				state->state = SEARCH_MAIL_QUEUE_DONE;
				transaction_logoff(cs->ts);

				i = 0;
				while (worker->next_row(worker)) {
					for (j = 0; worker->get_value(worker, j); j++) {

						if (strlen(worker->get_value(worker, j))) {
							const char *val = worker->get_value(worker, j);
							if (strchr(val, '\n')) {
								transaction_printf(cs->ts, "blob:%ld:result.%d.%s\n%s\n",
										   (unsigned long)strlen(val),
										   i,
										   worker->field_name(worker, j),
										   val);

							} else {
								transaction_printf(cs->ts, "result.%d.%s:%s\n",
										   i,
										   worker->field_name(worker, j),
										   val);
							}
						}
					}
					i++;
				}

				transaction_logon(cs->ts);
				continue;
			}

		case SEARCH_MAIL_QUEUE_DONE:
			search_mail_queue_done(state);
			return NULL;

		case SEARCH_MAIL_QUEUE_ERROR:
			transaction_logprintf(cs->ts, D_ERROR, "Error state");
			search_mail_queue_done(state);
			return NULL;

		default:
			/* MAJOR STATE ERROR */
			cs->status = "TRANS_ERROR";
			transaction_logprintf(cs->ts, D_ERROR, "Unknown state (%d)", 
					      state->state);
			return NULL;

		}
	}
}

/*****************************************************************************
 * search_mail_queue
 * Main function that initialise a state-structure and calls the state machine.
 **********/
static void
search_mail_queue(struct cmd *cs) {
	transaction_logprintf(cs->ts, D_DEBUG, "Start Search_mail_queue transaction");
	struct search_mail_queue_state *state;
	state = zmalloc(sizeof(*state));
	state->cs = cs;
	state->state = SEARCH_MAIL_QUEUE_INIT;

	search_mail_queue_fsm(state, NULL, NULL);
}

ADD_COMMAND(search_mail_queue,     /* Name of command */
		NULL,
            search_mail_queue,     /* Main function name */
            parameters,  /* Parameters struct */
            "Search_mail_queue command");/* Command description */
