#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <time.h>

#include "trans.h"
#include "util.h"
#include <ctemplates.h>
#include "validators.h"
#include <bconf.h>
#include <event.h>

#define SEARCH_COMP_DONE         0
#define SEARCH_COMP_ERROR        1
#define SEARCH_COMP_INIT         2
#define SEARCH_COMP_COUNT        3
#define SEARCH_COMP_UPGRADED     4
#define SEARCH_COMP_NOTUPGRADED  5
#define SEARCH_COMP_SOON_EXPIRED 6
#define SEARCH_COMP_BEGIN        7
#define SEARCH_COMP_FETCH        8
#define SEARCH_COMP_RESULT       9
#define SEARCH_COMP_START_PACKAGE 10
#define SEARCH_COMP_START_PACKAGE_RESULT 11

struct search_comp_state {
	int state;
	struct cmd *cs;
	int fetchno;
};

static struct c_param parameters[] = {
	{"id",		0,	"v_search_id"}, 
	{"cat", 	0,	"v_category_no_params"},
	{"reg", 	0,	"v_search_reg"},
	{"city",	0,	"v_search_city"},
	{"near",	0,	"v_search_near"}, 
	{"offset",	0,	"v_integer"},
	{"lim", 	0,	"v_integer"},
	{"query",	0,	"v_query"},
	{"admin_search",0,	"v_bool"},
	{"extra_data",	0,	"v_bool"},
	{"no_data",	0,	"v_bool"},
	{"count_status",0,      "v_bool"},
	{"store_status",0,      "v_store_status"},
	{"count_ads",   0,      "v_bool"},
	{NULL, 0}
};

extern struct bconf_node *bconf_root;

struct validator v_store_status[] = {
	VALIDATOR_REGEX("^(pending_review|active(_(not)?upgraded)?|inactive|expired|soon_expired|free_ads_soon_expire_count)$", REG_EXTENDED, "ERROR_STATUS_INVALID"),
	{0}
};
ADD_VALIDATOR(v_store_status);


static const char *search_comp_fsm(void *, struct sql_worker *, const char *);


/*****************************************************************************
 * search_comp_done
 * Exit function.
 **********/
static void
search_comp_done(void *v) {
	struct search_comp_state *state = v;
	struct cmd *cs = state->cs;
	free(state);
	cmd_done(cs);
}

/*****************************************************************************
 * bind_insert
 * Initialise template API before a call to sql_worker_template_query().
 **********/
static void
bind_insert(struct bpapi *ba, struct search_comp_state *state) {
	struct c_param *param;

	for (param = parameters; param->name; param++) {
		if (cmd_haskey(state->cs, param->name))
			bpapi_insert(ba, param->name, cmd_getval(state->cs, param->name));
	}
}

/*****************************************************************************
 * search_comp_fsm
 * State machine
 **********/
static const char *
search_comp_fsm(void *v, struct sql_worker *worker, const char *errstr) {
	struct search_comp_state *state = v;
	struct cmd *cs = state->cs;
        struct bpapi ba;

	/* 
	 * General error handling for db errors.
	 */
	if (errstr) {
                transaction_logprintf(cs->ts, D_ERROR, "search_comp_fsm statement failed (%d, %s)", 
                                      state->state,
                                      errstr);
                cs->status = "TRANS_DATABASE_ERROR";
                cs->message = xstrdup(errstr);
		state->state = SEARCH_COMP_ERROR;
        }

	while (1) {
		transaction_logprintf(cs->ts, D_DEBUG, "search_comp_fsm(%p, %p, %p), state=%d", 
				      v, worker, errstr, state->state);

		switch (state->state) {
		case SEARCH_COMP_INIT:
			/* next state */
			state->state = SEARCH_COMP_COUNT;

			/* Initialise the templateparse structure */
			BPAPI_INIT(&ba, NULL, pgsql);
			bind_insert(&ba, state);

			/* Register SQL query with our search_comp_fsm() as callback function */
			sql_worker_template_query(&config.pg_master, 
						  "sql/search_comp.sql", &ba, 
						  search_comp_fsm, state, cs->ts->log_string);

			/* Clean-up */
			bpapi_output_free(&ba);
			bpapi_simplevars_free(&ba);
			bpapi_vtree_free(&ba);

			/* Return to libevent */
			return NULL;

		case SEARCH_COMP_COUNT:
		case SEARCH_COMP_SOON_EXPIRED:
		case SEARCH_COMP_UPGRADED:
		case SEARCH_COMP_NOTUPGRADED:
			/* Count result */
                        transaction_logprintf(cs->ts, D_INFO, "Got a result: %d rows",
                                              worker->rows(worker));
			if (cmd_haskey(cs, "count_status")) {
				while (worker->next_row(worker))
					transaction_printf(cs->ts, "%s:%s\n", worker->get_value(worker, 0),
							worker->get_value(worker, 1));

				if (state->state != SEARCH_COMP_SOON_EXPIRED) {
					BPAPI_INIT(&ba, NULL, pgsql);
					bind_insert(&ba, state);
					if (state->state == SEARCH_COMP_NOTUPGRADED) {
						bpapi_insert(&ba, "store_status", "soon_expired");
						state->state = SEARCH_COMP_SOON_EXPIRED;
					} else if (state->state == SEARCH_COMP_UPGRADED) {
						bpapi_insert(&ba, "store_status", "active_notupgraded");
						state->state = SEARCH_COMP_NOTUPGRADED;
					} else {
						bpapi_insert(&ba, "store_status", "active_upgraded");
						state->state = SEARCH_COMP_UPGRADED;
					}

					/* Register SQL query with our search_comp_fsm() as callback function */
					sql_worker_template_query(&config.pg_master, 
							"sql/search_comp.sql", &ba, 
							search_comp_fsm, state, cs->ts->log_string);

					/* Clean-up */
					bpapi_output_free(&ba);
					bpapi_simplevars_free(&ba);
					bpapi_vtree_free(&ba);
					return NULL;
				}
			} else {
				worker->next_row(worker);
				transaction_printf(cs->ts, "rows:%s\n", worker->get_value(worker, 0));
			}

			if (cmd_haskey(cs, "no_data"))
			{
				state->state = SEARCH_COMP_START_PACKAGE;
				continue;
			}

			/* next state */
			state->state = SEARCH_COMP_BEGIN;

			/* Initialise the templateparse structure */
			BPAPI_INIT(&ba, NULL, pgsql);
			bind_insert(&ba, state);
			bpapi_insert(&ba, "get_data", "1");

			/* Register SQL query with our search_comp_fsm() as callback function */
			sql_worker_template_query(&config.pg_master, 
						  "sql/search_comp.sql", &ba, 
						  search_comp_fsm, state, cs->ts->log_string);

			/* Clean-up */
			bpapi_output_free(&ba);
			bpapi_simplevars_free(&ba);
			bpapi_vtree_free(&ba);

			/* Return to libevent */
			return NULL;

		case SEARCH_COMP_BEGIN:
			state->state = SEARCH_COMP_FETCH;
			return NULL;

		case SEARCH_COMP_FETCH:
			state->state = SEARCH_COMP_RESULT;
			return "FETCH 50 FROM store_cursor";

                case SEARCH_COMP_RESULT:
		{
			int row = 0;
			int col;
			int cols;

			transaction_logoff(cs->ts);

			cols = worker->fields(worker);
			if (!state->fetchno++) {
				transaction_printf(cs->ts, "hdrs:");
				for (col = 0 ;  col < cols ; col++) {
					transaction_printf(cs->ts, "%s%c",  
							worker->field_name(worker, col), (col + 1 == cols ? '\n' :'\t'));
				}
			}
			while (worker->next_row(worker)) {
				transaction_printf(cs->ts, "row:");
				for (col = 0; col < cols; col++) {
					transaction_printf(cs->ts, "%s%c",  
							   worker->get_value(worker, col), (col + 1 == cols ? '\n' :'\t'));
				}
				row++;
			}
			transaction_logon(cs->ts);
			if (row) {
				state->state = SEARCH_COMP_FETCH;
				continue;
			}

			if (cmd_haskey(cs, "store_status") && strcmp(cmd_getval(cs, "store_status"), "free_ads_soon_expire_count") == 0)
				state->state = SEARCH_COMP_START_PACKAGE;
			else
				state->state = SEARCH_COMP_DONE;

			return "COMMIT";
		}

		case SEARCH_COMP_START_PACKAGE:
			state->state = SEARCH_COMP_START_PACKAGE_RESULT;

			/* Initialise the templateparse structure */
			BPAPI_INIT(&ba, NULL, pgsql);
			bind_insert(&ba, state);

			/* Register SQL query with our search_comp_fsm() as callback function */
			sql_worker_template_query(&config.pg_master, 
						  "sql/start_package_days_left.sql", &ba, 
						  search_comp_fsm, state, cs->ts->log_string);

			/* Clean-up */
			bpapi_output_free(&ba);
			bpapi_simplevars_free(&ba);
			bpapi_vtree_free(&ba);

			/* Return to libevent */
			return NULL;

		case SEARCH_COMP_START_PACKAGE_RESULT:

				if (worker) {
					transaction_printf(cs->ts, "free_ads_soon_expire_count:%d\n", worker->rows(worker));
					if (!cmd_haskey(cs, "no_data") || atoi(cmd_getval(cs, "no_data")) != 0) {
						int i = 0;
						while (worker->next_row(worker)) {
							transaction_printf(cs->ts, "free_ads_company.%d.store_id:%s\n", i, worker->get_value_byname(worker, "store_id"));
							transaction_printf(cs->ts, "free_ads_company.%d.name:%s\n", i, worker->get_value_byname(worker, "name"));
							transaction_printf(cs->ts, "free_ads_company.%d.free_ads:%s\n", i, worker->get_value_byname(worker, "free_ads"));
							transaction_printf(cs->ts, "free_ads_company.%d.deadline:%s\n", i, worker->get_value_byname(worker, "deadline"));
							transaction_printf(cs->ts, "free_ads_company.%d.days_left:%s\n", i, worker->get_value_byname(worker, "days_left"));
							++i;
						}
					}	
					state->state = SEARCH_COMP_DONE;
					continue;
				}
				
				return NULL;

		case SEARCH_COMP_DONE:
			search_comp_done(state);
			return NULL;

		case SEARCH_COMP_ERROR:
			transaction_logprintf(cs->ts, D_ERROR, "Error state");
			search_comp_done(state);
			return NULL;

		default:
			/* MAJOR STATE ERROR */
			cs->status = "TRANS_ERROR";
			transaction_logprintf(cs->ts, D_ERROR, "Unknown state (%d)", 
					      state->state);
			return NULL;

		}
	}
}

/*****************************************************************************
 * search_comp
 * Main function that initialise a state-structure and calls the state machine.
 **********/
static void
search_comp(struct cmd *cs) {
	transaction_logprintf(cs->ts, D_DEBUG, "Start Search comp transaction");
	struct search_comp_state *state;
	state = zmalloc(sizeof(*state));
	state->cs = cs;
	state->state = SEARCH_COMP_INIT;

	search_comp_fsm(state, NULL, NULL);
}

ADD_COMMAND(search_comp,     /* Name of command */
		NULL,
            search_comp,     /* Main function name */
            parameters,  /* Parameters struct */
            "Search comp command");/* Command description */
