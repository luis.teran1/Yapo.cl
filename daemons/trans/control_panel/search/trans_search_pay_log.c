#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <time.h>

#include "trans.h"
#include "util.h"
#include <ctemplates.h>
#include "validators.h"
#include <bconf.h>
#include <event.h>

#define SEARCH_PAY_LOG_DONE  0
#define SEARCH_PAY_LOG_ERROR 1
#define SEARCH_PAY_LOG_INIT  2
#define SEARCH_PAY_LOG_RESULT 3

struct search_pay_log_state {
	int state;
	int i;
	int queries;
	struct cmd *cs;
	int pending_error;
};

static struct c_param parameters[] = {
	{"token",               P_REQUIRED, "v_token:search.paylog"},
	{"remote_addr",         P_REQUIRED, "v_ip"},
	{"remote_browser",      0, "v_remote_browser"},
	{"search_key",		P_REQUIRED,	"v_array:email,list_id,ad_id,store_id,phone,code,status,remote_addr"},
	{"search_value",	P_REQUIRED,	"v_search_value"},
	{"timestamp_from",	P_REQUIRED,	"v_timestamp"},
	{"timestamp_to",	P_REQUIRED,	"v_timestamp"},
	{NULL, 0}
};

static const char *search_pay_log_fsm(void *, struct sql_worker *, const char *);
extern struct bconf_node *bconf_root;

/*****************************************************************************
 * search_pay_log_done
 * Exit function.
 **********/
static void
search_pay_log_done(void *v) {
	struct search_pay_log_state *state = v;
	struct cmd *cs = state->cs;
	free(state);
	cmd_done(cs);
}

/*****************************************************************************
 * bind_insert
 * Initialise template API before a call to sql_worker_template_query().
 **********/
static void
bind_insert(struct bpapi *ba, struct search_pay_log_state *state) {
	struct c_param *param;

	for (param = parameters; param->name; param++) {
		if (cmd_haskey(state->cs, param->name))
			bpapi_insert(ba, param->name, cmd_getval(state->cs, param->name));
	}
}

/*****************************************************************************
 * search_pay_log_fsm
 * State machine
 **********/
static const char *
search_pay_log_fsm(void *v, struct sql_worker *worker, const char *errstr) {
	struct search_pay_log_state *state = v;
	struct cmd *cs = state->cs;
        struct bpapi ba;

	/* 
	 * General error handling for db errors.
	 */
	if (errstr) {
                transaction_logprintf(cs->ts, D_ERROR, "search_pay_log_fsm statement failed (%d, %s)", 
                                      state->state,
                                      errstr);
		if (!state->pending_error) {
			state->pending_error = 1;
			cs->status = "TRANS_DATABASE_ERROR";
			cs->message = xstrdup(errstr);
		}

		if (state->queries-- > 1) {
			return NULL;
		} else {
			state->state = SEARCH_PAY_LOG_ERROR;
		}
        }

	while (1) {
		transaction_logprintf(cs->ts, D_DEBUG, "search_pay_log_fsm(%p, %p, %p), state=%d", 
				      v, worker, errstr, state->state);

		switch (state->state) {
		case SEARCH_PAY_LOG_INIT:
		{
			int cnt;

			/* next state */
			state->state = SEARCH_PAY_LOG_RESULT;

			/* Initialise the templateparse structure */
			BPAPI_INIT(&ba, NULL, pgsql);
			bind_insert(&ba, state);

			/* Register SQL query with our search_pay_log_fsm() as callback function */
			/* Make sure to switch to slave if read-only query. */

			state->queries = bconf_count(bconf_get(bconf_root, "*.controlpanel.modules.adqueue.archive.schemas"));
			for (cnt = 0 ; cnt < state->queries ; cnt++) {
				const char *s = bconf_value(bconf_byindex(bconf_get(bconf_root, "*.controlpanel.modules.adqueue.archive.schemas"), cnt));
				bpapi_insert(&ba, "schema", s);
				sql_worker_template_query(&config.pg_slave, 
							  "sql/search_pay_log.sql", &ba, 
							  search_pay_log_fsm, state, cs->ts->log_string);
			}

			/* Clean-up */
			bpapi_output_free(&ba);
			bpapi_simplevars_free(&ba);
			bpapi_vtree_free(&ba);

			/* Return to libevent */
			return NULL;
		}
                case SEARCH_PAY_LOG_RESULT:
			{
				int j;

				if (!state->pending_error) {
					transaction_logoff(cs->ts);
					while (worker->next_row(worker)) {
						for (j = 0; j < worker->fields (worker); j++) {
							if (strlen(worker->get_value(worker, j)))
								transaction_printf(cs->ts, "result.%d.%s:%s\n",
										   state->i,
										   worker->field_name(worker, j),
										   worker->get_value(worker, j));
						}
						state->i++;
					}
					transaction_logon(cs->ts);
				}

				if (state->queries-- > 1) {
					return NULL;
				} else {
					if (!state->pending_error) {
						state->state = SEARCH_PAY_LOG_DONE;
					} else {
						state->state = SEARCH_PAY_LOG_ERROR;
					}
					continue;
				}
			}

		case SEARCH_PAY_LOG_DONE:
			search_pay_log_done(state);
			return NULL;

		case SEARCH_PAY_LOG_ERROR:
			transaction_logprintf(cs->ts, D_ERROR, "Error state");
			search_pay_log_done(state);
			return NULL;

		default:
			/* MAJOR STATE ERROR */
			cs->status = "TRANS_ERROR";
			transaction_logprintf(cs->ts, D_ERROR, "Unknown state (%d)", 
					      state->state);
			return NULL;

		}
	}
}

/*****************************************************************************
 * search_pay_log
 * Main function that initialise a state-structure and calls the state machine.
 **********/
static void
search_pay_log(struct cmd *cs) {
	transaction_logprintf(cs->ts, D_DEBUG, "Start Search_pay_log transaction");
	struct search_pay_log_state *state;
	state = zmalloc(sizeof(*state));
	state->cs = cs;
	state->state = SEARCH_PAY_LOG_INIT;

	search_pay_log_fsm(state, NULL, NULL);
}

ADD_COMMAND(search_pay_log,     /* Name of command */
		NULL,
            search_pay_log,     /* Main function name */
            parameters,  /* Parameters struct */
            "Search_pay_log command");/* Command description */
