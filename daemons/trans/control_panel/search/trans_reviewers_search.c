#include "factory.h"

static struct c_param parameters[] = {
        {"date",P_REQUIRED,"v_date_content"},
        {"reviewer",P_REQUIRED,"v_integer"},
        {"ad_state",P_OPTIONAL,"v_string_isprint"},
        {"refusal_reason",P_OPTIONAL,"v_integer"},
        {NULL, 0, 0}
};

FACTORY_TRANSACTION(reviewers_search, &config.pg_master, "sql/search_by_reviewers.sql", NULL, parameters, FACTORY_EMPTY_RESULT_OK);
