#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <time.h>

#include "trans.h"
#include "util.h"
#include <ctemplates.h>
#include "validators.h"
#include <bconf.h>
#include <event.h>

#define SEARCH_UID_DONE  0
#define SEARCH_UID_ERROR 1
#define SEARCH_UID_INIT  2
#define SEARCH_UID_RESULT 3

struct search_uid_state {
	int state;
	struct cmd *cs;
};

static struct c_param parameters[] = {
	{"uid",			P_REQUIRED, "v_integer"},
	{"token",		P_REQUIRED, "v_token:search.uid_emails"},
	{"remote_addr",		P_REQUIRED, "v_ip"},
	{"remote_browser",	0, "v_remote_browser"},
	{NULL, 0}
};

static const char *search_uid_fsm(void *, struct sql_worker *, const char *);


/*****************************************************************************
 * search_uid_done
 * Exit function.
 **********/
static void
search_uid_done(void *v) {
	struct search_uid_state *state = v;
	struct cmd *cs = state->cs;
	free(state);
	cmd_done(cs);
}

/*****************************************************************************
 * bind_insert
 * Initialise template API before a call to sql_worker_template_query().
 **********/
static void
bind_insert(struct bpapi *ba, struct search_uid_state *state) {
	struct c_param *param;

	for (param = parameters; param->name; param++) {
		if (cmd_haskey(state->cs, param->name))
			bpapi_insert(ba, param->name, cmd_getval(state->cs, param->name));
	}
}

/*****************************************************************************
 * search_uid_fsm
 * State machine
 **********/
static const char *
search_uid_fsm(void *v, struct sql_worker *worker, const char *errstr) {
	struct search_uid_state *state = v;
	struct cmd *cs = state->cs;
        struct bpapi ba;

	/*
	 * General error handling for db errors.
	 */
	if (errstr) {
                transaction_logprintf(cs->ts, D_ERROR, "search_uid_fsm statement failed (%d, %s)",
                                      state->state,
                                      errstr);
                cs->status = "TRANS_DATABASE_ERROR";
                cs->message = xstrdup(errstr);
		state->state = SEARCH_UID_ERROR;
        }

	while (1) {
		transaction_logprintf(cs->ts, D_DEBUG, "search_uid_fsm(%p, %p, %p), state=%d",
				      v, worker, errstr, state->state);

		switch (state->state) {
		case SEARCH_UID_INIT:
			/* next state */
			state->state = SEARCH_UID_RESULT;

			/* Initialise the templateparse structure */
			BPAPI_INIT(&ba, NULL, pgsql);
			bind_insert(&ba, state);

			/* Register SQL query with our search_uid_fsm() as callback function */
			sql_worker_template_query(&config.pg_slave,
						  "sql/get_emails_by_uid.sql", &ba,
						  search_uid_fsm, state, cs->ts->log_string);

			/* Clean-up */
			bpapi_output_free(&ba);
			bpapi_simplevars_free(&ba);
			bpapi_vtree_free(&ba);

			/* Return to libevent */
			return NULL;

		case SEARCH_UID_RESULT:
			{
				int count = 0;
				transaction_logprintf(cs->ts, D_INFO, "Got a result: %d rows",
						      worker->rows(worker));

				while (worker->next_row(worker)) {
					transaction_printf(cs->ts, "email.%d:%s\n", count++, worker->get_value(worker, 0));
				}

				search_uid_done(state);
				return NULL;
			}
		case SEARCH_UID_ERROR:
			transaction_logprintf(cs->ts, D_ERROR, "Error state");
			search_uid_done(state);
			return NULL;

		default:
			/* MAJOR STATE ERROR */
			cs->status = "TRANS_ERROR";
			transaction_logprintf(cs->ts, D_ERROR, "Unknown state (%d)",
					      state->state);
			search_uid_done(state);
			return NULL;

		}
	}
}

/*****************************************************************************
 * search_uid
 * Main function that initialise a state-structure and calls the state machine.
 **********/
static void
search_uid(struct cmd *cs) {
	transaction_logprintf(cs->ts, D_DEBUG, "Start search_uid transaction");
	struct search_uid_state *state;
	state = zmalloc(sizeof(*state));
	state->cs = cs;
	state->state = SEARCH_UID_INIT;

	search_uid_fsm(state, NULL, NULL);
}

ADD_COMMAND(search_uid,     /* Name of command */
		NULL,
            search_uid,     /* Main function name */
            parameters,  /* Parameters struct */
            "Returns all emails for a uid");/* Command description */
