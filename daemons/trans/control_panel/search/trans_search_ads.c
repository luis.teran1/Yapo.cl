#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <time.h>

#include "trans.h"
#include "util.h"
#include <ctemplates.h>
#include "validators.h"
#include <bconf.h>
#include <event.h>
#include "db.h"

#define SEARCH_ADS_INIT          1
#define SEARCH_ADS_START         2
#define SEARCH_ADS_GET_FILTER    3
#define SEARCH_ADS_COUNT         4
#define SEARCH_ADS_GET_COUNT     5
#define SEARCH_ADS_GET_AD_IDS    6
#define SEARCH_ADS_GET_ADS       7
#define SEARCH_ADS_RESET_NEXT_AD 8
#define SEARCH_ADS_NEXT_AD       9
#define SEARCH_ADS_LOAD_DATA     10
#define SEARCH_ADS_SHOWAD        11
#define SEARCH_ADS_RESET_DONE    12
#define SEARCH_ADS_DONE          13

struct queue_filter {
	const char *name;
	const char *tables;
	const char *condition;
	const char *select;
	const char *timecol;
	int use_archive;
	TAILQ_ENTRY(queue_filter) filter_list;
};

struct search_ads_state {
	int state;
	unsigned int curr_ad;
	unsigned int ads;
	struct cmd *cs;
        struct event timeout_event;
	struct action *action;
	char *search_tables;
	char *search_condition;
	struct queue_filter *current_filter;
	struct queue_filter *fetch_filter;
	unsigned int offset;
	unsigned int limit;
	unsigned int total_count;
	TAILQ_HEAD(,adqueue_entry) adqueue;
	TAILQ_HEAD(,queue_filter) filters;
};

extern struct bconf_node *bconf_root;

/* XXX Shouldn't use v_newad_params nor v_category_check */
static struct c_param parameters[] = {
	{"email_part",      0,          "v_string_isprint"},
	{"token",           P_REQUIRED, "v_token:search.search_ads"},
	{"remote_addr",     0,          "v_ip"},
	{"remote_browser",  0,          "v_remote_browser"},
	{"search_type",     P_REQUIRED, "v_search_type"},
	{"filter_name",     0,          "v_string_isprint"},
	{"category",        0,          "v_integer"},
	{"region",          0,          "v_integer"},
	{"time_from",       0,          "v_timestamp"},
	{"time_to",         0,          "v_timestamp"},
	{"limit",           0,          "v_integer"},
	{"offset",          0,          "v_integer"},
	{"count_all",       0,          "v_bool"},
	{"store_id",        0,          "v_integer"},
	{"noarchive",       0,          "v_bool"},
	{"archive_group",   0,          "v_string_isprint"},
	{"gallery_only",    0,          "v_bool"},
	{"_params",         0,          "v_newad_params"},
	{NULL,              0}
};


/*****************************************************************************
 * search_ads_done
 * Exit function.
 **********/
static void
search_ads_done(void *v) {
	struct search_ads_state *state = v;
	struct cmd *cs = state->cs;

	while (!TAILQ_EMPTY(&state->adqueue)) {
		struct adqueue_entry *ae = TAILQ_FIRST(&state->adqueue);
		TAILQ_REMOVE(&state->adqueue, ae, queue);
		free(ae);
	}

	while (!TAILQ_EMPTY(&state->filters)) {
		struct queue_filter *filter = TAILQ_FIRST(&state->filters);
		TAILQ_REMOVE(&state->filters, filter, filter_list);
		free(filter);
	}
	if (state->search_tables)
		free(state->search_tables);
	if (state->search_condition)
		free(state->search_condition);

	free(state);
	cmd_done(cs);
}

/*****************************************************************************
 * bind_search_ads
 * Initialise template API before a call to sql_worker_template_query().
 **********/
static void
bind_search_ads(struct bpapi *ba, struct search_ads_state *state, struct queue_filter *filter) {
	const char *fields[] = {"time_from", "time_to", "archive_group", NULL};
	int i = 0;
	const char* cat;
	const char* reg;
	const char* level;
	char cat_str[256];

	while (fields[i]) {
		if (cmd_haskey(state->cs, fields[i]))
			bpapi_insert(ba, fields[i], cmd_getval(state->cs, fields[i]));
		i++;
	}

	/* Default to archive_group 'all' */
	if (!cmd_haskey(state->cs, "archive_group"))
		bpapi_insert(ba, "archive_group", "all");

	/* Search queue */
	bpapi_insert(ba, "filter_name", filter->name);

	/* Check for main categories */
	level = bconf_vget_string(host_bconf, "*.cat", cmd_getval(state->cs, "category"), "level", NULL);

	/* Select sub categories */
	 if (level && atoi(level) == 0) {
		int c;
		struct bconf_node* p;
		p = bconf_get(bconf_root, "*.*.cat");
		strcpy(cat_str, "");

		for (c = 0; c < bconf_count(p); c++) {
			if (bconf_value(bconf_get(bconf_byindex(p, c), "parent"))) {
				if (strcmp(bconf_value(bconf_get(bconf_byindex(p, c), "parent")), cmd_getval(state->cs, "category")) == 0) {
					if (strlen(cat_str))
						strcat(cat_str, ",");
					strcat(cat_str, bconf_key(bconf_byindex(p, c)));
				}
			}
		}
		if (strlen(cat_str))
			bpapi_insert(ba, "category", cat_str);
	 } else if ((cat=cmd_getval(state->cs, "category")) != NULL)
		 bpapi_insert(ba, "category", cat);


	/* Search filtering on region */
	if ((reg=cmd_getval(state->cs, "region")) != NULL)
		bpapi_insert(ba, "region", reg);

	if (filter->tables)
		bpapi_insert(ba, "tables", filter->tables);
	bpapi_insert(ba, "select", filter->select);
	bpapi_insert(ba, "condition", filter->condition);
	bpapi_insert(ba, "timecol", filter->timecol);

	/* Search filter */
	if (state->search_tables)
		bpapi_insert(ba, "search_tables", state->search_tables);
	bpapi_insert(ba, "search_condition", state->search_condition);

	bpapi_insert(ba, "query", cmd_getval(state->cs, cmd_getval(state->cs, "search_type")));

	if (cmd_haskey(state->cs, "gallery_only"))
		bpapi_insert(ba, "gallery_only", "1");
}

/*****************************************************************************
 * search_ads_fsm
 * State machine
 **********/
static const char *
search_ads_fsm(void *v, struct sql_worker *worker, const char *errstr) {
	struct search_ads_state *state = v;
	struct cmd *cs = state->cs;
        struct bpapi ba;
	struct action_param *action_param;
	const char *value;

	/* 
	 * General error handling for DB-errors.
	 */
	if (errstr) {
		if (strstr(errstr, "statement timeout") != NULL) {
			cs->status = "TRANS_ERROR";
			cs->error = "ERROR_STATEMENT_TIMEOUT";
			/* Reset timeout */
			state->state = SEARCH_ADS_RESET_DONE;
		} else {
			transaction_logprintf(cs->ts, D_ERROR, "Statement failed (%d,  %s)", 
					      state->state,
					      errstr);
			cs->status = "TRANS_DATABASE_ERROR";
			cs->message = xstrdup(errstr);
			search_ads_done(state);
			return NULL;
		}
        }

	while (1) {
		transaction_logprintf(cs->ts, D_DEBUG, "search_ads fsm state %d", state->state);
		switch (state->state) {

		case SEARCH_ADS_INIT:
			state->state = SEARCH_ADS_START;
			BPAPI_INIT(&ba, NULL, pgsql);
			bpapi_insert(&ba, "timeout", "45000");
			sql_worker_template_query_flags(&config.pg_slave, SQLW_DONT_REUSE,
					"sql/statement_timeout.sql", &ba,
					search_ads_fsm, state, cs->ts->log_string);
			bpapi_output_free(&ba);
			bpapi_simplevars_free(&ba);
			bpapi_vtree_free(&ba);
			return NULL;

		case SEARCH_ADS_START:
		{
			struct bconf_node *st = bconf_vget(bconf_root, "*", "controlpanel", "modules", "search", "filter", cmd_getval(cs, "search_type"), NULL);
			if (!st) {
				transaction_logprintf(cs->ts, D_INFO, "No matching search filter.");
				state->state = SEARCH_ADS_RESET_DONE;  
				cs->error = "ERROR_SEARCH_TYPE_INVALID";
				cs->status = "TRANS_ERROR";
				continue;
			}
			if (!cmd_haskey(cs, cmd_getval(cs, "search_type"))) {
				transaction_logprintf(cs->ts, D_INFO, "No search value given");
				state->state = SEARCH_ADS_RESET_DONE;
				cs->error = "ERROR_SEARCH_VALUE_MISSING";
				cs->status = "TRANS_ERROR";
				continue;
			}

			value = bconf_get_string(st, "tables");
			if (value && value[0])
				state->search_tables = xstrdup(value);
			value = bconf_get_string(st, "condition");
			state->search_condition = xstrdup(value);

			/* Fetch one or all filters */
			state->state = SEARCH_ADS_GET_FILTER;
			continue;
		}

                case SEARCH_ADS_GET_FILTER:
		{
			int max_limit;
			int i = 0;
			struct bconf_node *bconf_filter;

			/* All search queues */
			struct bconf_node *st = bconf_vget(bconf_root, "*", "controlpanel", "modules", "search", "queues", NULL);

			/* Get all queues */
			for (i = 0; (bconf_filter = bconf_byindex(st, i)) != NULL; i++) {
				const char *filter_name = cmd_getval(cs, "filter_name");
				if ((!filter_name || !filter_name[0]) || cmd_haskey(cs, "count_all") || 
				    strcmp(bconf_get_string(bconf_filter, "id"), filter_name) == 0) {
					const char *value;
					struct queue_filter *filter;

					filter = xmalloc(sizeof(*filter));
					filter->name = bconf_get_string(bconf_filter, "id");
					filter->condition = bconf_get_string(bconf_filter, "condition");
					filter->timecol = bconf_get_string(bconf_filter, "timecolumn");
					value = bconf_get_string(bconf_filter, "tables");
					if (value && value[0])
						filter->tables = value;
					else
						filter->tables = NULL;
					filter->select = bconf_get_string(bconf_filter, "select");
					if (cmd_haskey(cs, "noarchive") && atoi(cmd_getval(cs, "noarchive")))
						filter->use_archive = 0;
					else {
						value = bconf_get_string(bconf_filter, "use_archive");
						filter->use_archive = (value && atoi(value));
					}
					transaction_logprintf(cs->ts, D_DEBUG, "FILTER %s USE ARCHIVE: %d\n", filter->name, filter->use_archive);

					TAILQ_INSERT_TAIL(&state->filters, filter, filter_list);
				}
			}

			state->current_filter = TAILQ_FIRST(&state->filters);
			state->fetch_filter = TAILQ_END(&state->filters);
			state->state = SEARCH_ADS_COUNT;
			if (cmd_haskey(cs, "offset"))
				state->offset = atoi(cmd_getval(cs, "offset"));
			if (cmd_haskey(cs, "limit"))
				state->limit = atoi(cmd_getval(cs, "limit"));
			if ((max_limit = bconf_vget_int(host_bconf, "controlpanel.modules", "search", cmd_getval(cs, "search_type"), "max_limit", NULL)) == 0)
				max_limit = bconf_get_int(host_bconf, "controlpanel.modules.search.max_limit");
			if (!state->limit)
				state->limit = max_limit;
			continue;
		}

		case SEARCH_ADS_COUNT:
			/* Count ads in all filters, and determine where to start fetching ad_id and action_id */
			if (state->current_filter == TAILQ_END(&state->filters)) {
				transaction_printf(cs->ts, "total:%u\n", state->total_count);
				state->state = SEARCH_ADS_GET_ADS;
				continue;
			}

			BPAPI_INIT_APP(&ba, host_bconf, "controlpanel", buf, pgsql);
			bind_search_ads(&ba, state, state->current_filter);
			/* XXX */
			if (state->current_filter->use_archive)
				sql_worker_template_newquery(worker, "sql/get_search_ads_archive_count.sql", &ba);
			else
				sql_worker_template_newquery(worker, "sql/get_search_ads_count.sql", &ba);
			bpapi_output_free(&ba);
			bpapi_simplevars_free(&ba);
			bpapi_vtree_free(&ba);

			state->state = SEARCH_ADS_GET_COUNT;
			return NULL;

		case SEARCH_ADS_GET_COUNT:
			if (worker->next_row(worker)) {
				unsigned int count = atoi(worker->get_value(worker, 0));

				if (count && state->fetch_filter == TAILQ_END(&state->filters)) {
					/* If total count has reached offset, store filter for fetching. */
					if (state->offset <= count)
						state->fetch_filter = state->current_filter;
					else
						state->offset -= count;
				}

				transaction_printf(cs->ts, "subtotal.%s:%u\n", state->current_filter->name, count);
				state->total_count += count;
			}
			state->current_filter = TAILQ_NEXT(state->current_filter, filter_list);
			state->state = SEARCH_ADS_COUNT;
			continue;

		case SEARCH_ADS_GET_ADS:
		{
			const char *filter_name = cmd_getval(cs, "filter_name");
			/* Fetch ad_id and action_id */
			if (state->ads >= state->limit || state->fetch_filter == TAILQ_END(state->filters)) {
				state->ads = state->limit;
				/* No need to keep the worker anymore, reset statement timeout */
				state->state = SEARCH_ADS_RESET_NEXT_AD;
				continue;
			}
			if (cmd_haskey(cs, "count_all") && cmd_haskey(cs, "filter_name") && 
			    strcmp(state->fetch_filter->name, filter_name) != 0) {
				state->fetch_filter = TAILQ_NEXT(state->fetch_filter, filter_list);
				continue;
			}

			BPAPI_INIT_APP(&ba, host_bconf, "controlpanel", buf, pgsql);
			bind_search_ads(&ba, state, state->fetch_filter);
			if (state->offset)
				bpapi_set_int(&ba, "offset", state->offset);
			if (state->limit)
				bpapi_set_int(&ba, "limit", state->limit);

			if (state->fetch_filter->use_archive)
				sql_worker_template_newquery(worker, "sql/get_search_ads_archive.sql", &ba);
			else
				sql_worker_template_newquery(worker, "sql/get_search_ads.sql", &ba);
			bpapi_output_free(&ba);
			bpapi_simplevars_free(&ba);
			bpapi_vtree_free(&ba);

			state->state = SEARCH_ADS_GET_AD_IDS;
			return NULL;
		}
		case SEARCH_ADS_GET_AD_IDS:
			/* Process all results */
                        while (worker->next_row(worker)) {
				struct adqueue_entry *ae = xmalloc(sizeof (*ae));

                                ae->ad_id = atoi(worker->get_value_byname(worker, "ad_id"));
                                ae->action_id = atoi(worker->get_value_byname(worker, "action_id"));
				value = worker->get_value_byname(worker, "schema");
				if (value) {
					int len = strlen(value);

					ae->schema = xmalloc(len + 2);
					memcpy(ae->schema, value, len);
					ae->schema[len] = '.';
					ae->schema[len + 1] = '\0';
				} else
					ae->schema = NULL;
				ae->filter = state->fetch_filter->name;
				ae->dbc = "pgsql.slave";
				state->ads++;
				TAILQ_INSERT_TAIL(&state->adqueue, ae, queue);
                        }

			state->offset = 0;
			state->fetch_filter = TAILQ_NEXT(state->fetch_filter, filter_list);
			state->state = SEARCH_ADS_GET_ADS;
			continue;

		case SEARCH_ADS_RESET_NEXT_AD:
			state->state = SEARCH_ADS_NEXT_AD;

			BPAPI_INIT(&ba, NULL, pgsql);
			bpapi_insert(&ba, "timeout", "0");
			sql_worker_template_newquery(worker, "sql/statement_timeout.sql", &ba);
			bpapi_output_free(&ba);
			bpapi_simplevars_free(&ba);
			bpapi_vtree_free(&ba);
			return NULL;

		case SEARCH_ADS_NEXT_AD:
		{
			struct adqueue_entry *ae;

			if (TAILQ_EMPTY(&state->adqueue)) {
				state->state = SEARCH_ADS_DONE;
				continue;
			}

			ae = TAILQ_FIRST(&state->adqueue);
			
			BPAPI_INIT(&ba, NULL, pgsql);

			bpapi_set_int(&ba, "ad_id", ae->ad_id);
			bpapi_set_int(&ba, "action_id", ae->action_id);
			if (ae->schema) {
				bpapi_insert(&ba, "schema", ae->schema);
				free(ae->schema);
			}
			transaction_printf(cs->ts, "ad.%u.filter:%s\n", state->curr_ad, ae->filter);
			TAILQ_REMOVE(&state->adqueue, ae, queue);

			bpapi_insert(&ba, "action_times", "1");
			sql_worker_template_query_flags(ae->dbc, SQLW_FINAL_CALLBACK, "sql/loadaction.sql", &ba, 
					search_ads_fsm, state, cs->ts->log_string);
			free(ae);
			
			state->state = SEARCH_ADS_LOAD_DATA;
			bpapi_output_free(&ba);
			bpapi_simplevars_free(&ba);
			bpapi_vtree_free(&ba);
			return NULL;
		}

		case SEARCH_ADS_LOAD_DATA:
			if (worker->sw_state == RESULT_DONE) {
				state->state = SEARCH_ADS_SHOWAD;
				continue;
			}
			/* Parse the result for the current SELECT */
			state->action = parse_loadaction(worker, cs->ts, state->action);
			return NULL;

		case SEARCH_ADS_SHOWAD:
			/* Dump current ad */
			transaction_logoff(cs->ts);
			TAILQ_FOREACH(action_param, &state->action->h, entry) {
				if (action_param->value && 
				    (strchr(action_param->value, '\n') != NULL || 
				     strchr(action_param->value, '\r') != NULL)) {
					transaction_printf(cs->ts, "blob:%ld:ad.%u.%s.%s\n%s\n", 
							   (unsigned long)strlen(action_param->value), state->curr_ad, 
							   action_param->prefix, action_param->key, action_param->value);
				} else {
					transaction_printf(cs->ts, "ad.%u.%s.%s:%s\n", state->curr_ad, action_param->prefix, 
							   action_param->key, (action_param->value ? action_param->value : ""));
				}
			}
			transaction_logon(cs->ts);
			parse_loadaction_cleanup(state->action);
			state->action = NULL;

			if (++state->curr_ad < state->ads)
				state->state = SEARCH_ADS_NEXT_AD;
			else
				state->state = SEARCH_ADS_DONE;
			continue;

		case SEARCH_ADS_RESET_DONE:
			state->state = SEARCH_ADS_DONE;

			BPAPI_INIT(&ba, NULL, pgsql);
			bpapi_insert(&ba, "timeout", "0");
			sql_worker_template_newquery(worker, "sql/statement_timeout.sql", &ba);
			bpapi_output_free(&ba);
			bpapi_simplevars_free(&ba);
			bpapi_vtree_free(&ba);
			return NULL;

		case SEARCH_ADS_DONE:
			search_ads_done(state);
			return NULL;

		default:
			/* MAJOR STATE ERROR */
			cs->status = "TRANS_ERROR";
			transaction_logprintf(cs->ts, D_ERROR, "Unknown state (%d)", 
					      state->state);
			search_ads_done(state);
			return NULL;

		}
	}
}

/*****************************************************************************
 * search_ads
 * Main function that initialise a state-structure and calls the state machine.
 **********/
static void
search_ads(struct cmd *cs) {
	transaction_logprintf(cs->ts, D_DEBUG, "Start Search_Ads transaction");
	struct search_ads_state *state;
	state = zmalloc(sizeof(*state));
	state->cs = cs;
	state->state = SEARCH_ADS_INIT;
	TAILQ_INIT(&state->adqueue);
	TAILQ_INIT(&state->filters);

	search_ads_fsm(state, NULL, NULL);
}

ADD_COMMAND(search_ads,     /* Name of command */
		NULL,
	    search_ads,     /* Main function name */
	    parameters,     /* Parameters struct */
	    "Search ads given a ad_id, email, user_id or paycode"); 
