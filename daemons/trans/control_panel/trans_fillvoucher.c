#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <time.h>

#include "trans.h"
#include "util.h"
#include <ctemplates.h>
#include "validators.h"
#include <bconf.h>
#include <event.h>

#define FILLVOUCHER_DONE  0
#define FILLVOUCHER_ERROR 1
#define FILLVOUCHER_INIT  2
#define FILLVOUCHER_CHECK  3

struct fillvoucher_state {
	int state;
	struct cmd *cs;
};

extern struct bconf_node *bconf_root;

static struct c_param parameters[] = {
	{"token", P_REQUIRED, "v_store_token:store_id"},
	{"remote_addr", P_REQUIRED, "v_ip"},
	{"remote_browser", 0, "v_remote_browser"},
	{"store_id", P_REQUIRED, "v_id"},
	{"amount", P_REQUIRED, "v_fillvoucher_amount"},
	{NULL, 0}
};

static int
vf_fillvoucher_amount(struct cmd_param *param, const char *arg) {
	const char *prefix = bconf_value(bconf_vget(bconf_root, "*.fillvoucher.order_id.prefix", param->value, NULL));

	if (!prefix || !prefix[0]) {
		param->error = "ERROR_INVALID_AMOUNT";
		param->cs->status = "TRANS_ERROR";
		return 1;
	}
	return 0;
}

struct validator v_fillvoucher_amount[] = {
	VALIDATOR_FUNC(vf_fillvoucher_amount),
	{0}
};
ADD_VALIDATOR(v_fillvoucher_amount);

static const char *fillvoucher_fsm(void *, struct sql_worker *, const char *);


/*****************************************************************************
 * fillvoucher_done
 * Exit function.
 **********/
static void
fillvoucher_done(void *v) {
	struct fillvoucher_state *state = v;
	struct cmd *cs = state->cs;
	free(state);
	cmd_done(cs);
}

/*****************************************************************************
 * bind_insert
 * Initialise template API before a call to sql_worker_template_query().
 **********/
static void
bind_insert(struct bpapi *ba, struct fillvoucher_state *state) {
	struct c_param *param;

	for (param = parameters; param->name; param++) {
		if (cmd_haskey(state->cs, param->name))
			bpapi_insert(ba, param->name, cmd_getval(state->cs, param->name));
	}
}

/*****************************************************************************
 * fillvoucher_fsm
 * State machine
 **********/
static const char *
fillvoucher_fsm(void *v, struct sql_worker *worker, const char *errstr) {
	struct fillvoucher_state *state = v;
	struct cmd *cs = state->cs;
        struct bpapi ba;

	/* 
	 * General error handling for db errors.
	 */
	if (errstr) {
                transaction_logprintf(cs->ts, D_ERROR, "fillvoucher_fsm statement failed (%d, %s)", 
                                      state->state,
                                      errstr);
                cs->status = "TRANS_DATABASE_ERROR";
                cs->message = xstrdup(errstr);
		state->state = FILLVOUCHER_ERROR;
        }

	while (1) {
		transaction_logprintf(cs->ts, D_DEBUG, "fillvoucher_fsm(%p, %p, %p), state=%d", 
				      v, worker, errstr, state->state);

		switch (state->state) {
		case FILLVOUCHER_INIT:
			/* next state */
			state->state = FILLVOUCHER_CHECK;

			/* Initialise the templateparse structure */
			BPAPI_INIT(&ba, NULL, pgsql);
			bind_insert(&ba, state);

			/* Register SQL query with our fillvoucher_fsm() as callback function */
			/* Make sure to switch to slave if read-only query. */
			sql_worker_template_query(&config.pg_master, 
						  "sql/fillvoucher.sql", &ba, 
						  fillvoucher_fsm, state, cs->ts->log_string);

			/* Clean-up */
			bpapi_output_free(&ba);
			bpapi_simplevars_free(&ba);
			bpapi_vtree_free(&ba);

			/* Return to libevent */
			return NULL;

		case FILLVOUCHER_CHECK:
			if (worker->next_row(worker)) {
				const char *payment_group_id = worker->get_value(worker, 0);
				const char *voucher_id = worker->get_value(worker, 1);
				const char *voucher_action_id = worker->get_value(worker, 2);
				const char *prefix = bconf_value(bconf_vget(bconf_root, "*.fillvoucher.order_id.prefix", cmd_getval(cs, "amount"), NULL));

				if (!prefix) {
					cs->error = "ERROR_NO_PREFIX_FOR_AMOUNT";
					cs->status = "TRANS_ERROR";
					state->state = FILLVOUCHER_ERROR;
					continue;
				}

				transaction_printf(cs->ts, "order_id:%s%s\n", prefix, payment_group_id);
				transaction_printf(cs->ts, "voucher_id:%s\n", voucher_id);
				transaction_printf(cs->ts, "voucher_action_id:%s\n", voucher_action_id);
				transaction_printf(cs->ts, "payment_group_id:%s\n", payment_group_id);
			}
			state->state = FILLVOUCHER_DONE;
			continue;

		case FILLVOUCHER_DONE:
			fillvoucher_done(state);
			return NULL;

		case FILLVOUCHER_ERROR:
			transaction_logprintf(cs->ts, D_ERROR, "Error state");
			fillvoucher_done(state);
			return NULL;

		default:
			/* MAJOR STATE ERROR */
			cs->status = "TRANS_ERROR";
			transaction_logprintf(cs->ts, D_ERROR, "Unknown state (%d)", 
					      state->state);
			return NULL;

		}
	}
}

/*****************************************************************************
 * fillvoucher
 * Main function that initialise a state-structure and calls the state machine.
 **********/
static void
fillvoucher(struct cmd *cs) {
	transaction_logprintf(cs->ts, D_DEBUG, "Start fillvoucher transaction");
	struct fillvoucher_state *state;
	state = zmalloc(sizeof(*state));
	state->cs = cs;
	state->state = FILLVOUCHER_INIT;

	fillvoucher_fsm(state, NULL, NULL);
}

ADD_COMMAND(fillvoucher,     /* Name of command */
		NULL,
            fillvoucher,     /* Main function name */
            parameters,  /* Parameters struct */
            "fillvoucher command");/* Command description */
