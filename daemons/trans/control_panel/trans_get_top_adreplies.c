#include "factory.h"

static struct c_param parameters[] = {
	{"stat_time",   P_REQUIRED, "v_string_isprint"},
	{NULL, 0}
};

FACTORY_TRANSACTION(get_top_adreplies, &config.pg_slave, "sql/get_top_adreplies.sql", NULL, parameters, FACTORY_NO_RES_CNTR);
