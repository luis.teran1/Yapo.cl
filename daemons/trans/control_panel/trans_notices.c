#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <time.h>

#include "trans.h"
#include "util.h"
#include <ctemplates.h>
#include "validators.h"
#include <bconf.h>
#include <event.h>

#define NOTICES_DONE          0
#define NOTICES_ERROR         1
#define NOTICES_INIT          2
#define NOTICES_LOAD          3

struct notices_state {
	int state;
	struct cmd *cs;
};

static struct c_param parameters[] = {
	{"token",          P_REQUIRED, "v_token:adqueue"},
	{"remote_addr",    P_REQUIRED, "v_ip"},
	{"remote_browser", 0, "v_remote_browser"},
	{"email",      	   0, "v_email_basic"},
	{NULL, 0}
};

static const char *notices_fsm(void *, struct sql_worker *, const char *);


/*****************************************************************************
 * notices_done
 * Exit function.
 **********/
static void
notices_done(void *v) {
	struct notices_state *state = v;
	struct cmd *cs = state->cs;
	free(state);
	cmd_done(cs);
}

/*****************************************************************************
 * bind_insert
 * Initialise template API before a call to sql_worker_template_query().
 **********/
static int
bind_insert(struct bpapi *ba, struct notices_state *state) {
	bpapi_insert(ba, "email", cmd_getval(state->cs, "email"));
	return 0;
}

/*****************************************************************************
 * notices_fsm
 * State machine
 **********/
static const char *
notices_fsm(void *v, struct sql_worker *worker, const char *errstr) {
	struct notices_state *state = v;
	struct cmd *cs = state->cs;
        struct bpapi ba;

	/* 
	 * General error handling for db errors.
	 */
	if (errstr) {
                transaction_logprintf(cs->ts, D_ERROR, "notices_fsm statement failed (%d, %s)", 
                                      state->state,
                                      errstr);
		cs->status = "TRANS_DATABASE_ERROR";
		cs->message = xstrdup(errstr);
		state->state = NOTICES_ERROR;
        }

	while (1) {
		transaction_logprintf(cs->ts, D_DEBUG, "notices_fsm(%p, %p, %p), state=%d", 
				      v, worker, errstr, state->state);
		
		switch (state->state) {
		case NOTICES_INIT:
			/* next state */
			state->state = NOTICES_LOAD;

			/* Initialise the templateparse structure */
			BPAPI_INIT(&ba, NULL, pgsql);
			if (bind_insert(&ba, state)) {
				bpapi_output_free(&ba);
				bpapi_simplevars_free(&ba);
				bpapi_vtree_free(&ba);
				state->state = NOTICES_ERROR;
				continue;
			}

			sql_worker_template_query(&config.pg_slave, 
						  "sql/notices.sql", &ba, 
						  notices_fsm, state, cs->ts->log_string);

			/* Clean-up */
			bpapi_output_free(&ba);
			bpapi_simplevars_free(&ba);
			bpapi_vtree_free(&ba);

			/* Return to libevent */
			return NULL;

		case NOTICES_LOAD:
		{
			int fields = worker->fields(worker);
			int fc;
			int nc = 0;
			const char *value;


			while (worker->next_row(worker)) {
				for (fc = 0 ; fc  < fields ; fc++) {
					value = worker->get_value(worker, fc);

					if (strchr(value, '\n')) {
						transaction_printf(cs->ts, "blob:%ld:%s\n%s\n", (unsigned long)strlen(value), 
								   worker->field_name(worker, fc), value);
					} else {
						transaction_printf(cs->ts, "notice.%u.%s:%s\n", 
								   nc, worker->field_name(worker, fc), 
								   value);
					}
				}
				nc++;
			}

			state->state = NOTICES_DONE;
			return NULL;
		}

		case NOTICES_DONE:
			notices_done(state);
			return NULL;

		case NOTICES_ERROR:
			transaction_logprintf(cs->ts, D_ERROR, "Error state");
			notices_done(state);
			return NULL;

		default:
			/* MAJOR STATE ERROR */
			cs->status = "TRANS_ERROR";
			transaction_logprintf(cs->ts, D_ERROR, "Unknown state (%d)", 
					      state->state);
			return NULL;

		}
	}
}

/*****************************************************************************
 * notices
 * Main function that initialise a state-structure and calls the state machine.
 **********/
static void
notices(struct cmd *cs) {
	transaction_logprintf(cs->ts, D_DEBUG, "Start notices transaction");
	struct notices_state *state;
	state = zmalloc(sizeof(*state));
	state->cs = cs;
	state->state = NOTICES_INIT;

	notices_fsm(state, NULL, NULL);
}

ADD_COMMAND(notices,        /* Name of command */
	    NULL,	    /* Called during the initalisation phase */
	    notices,        /* Main function name */
	    parameters,     /* Parameters struct */
	    "Load notices");/* Command description */
