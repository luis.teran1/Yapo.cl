#include "factory.h"

static struct c_param parameters[] = {
	{"date", P_REQUIRED, "v_date"},
	{NULL, 0}
};

FACTORY_TRANSACTION(get_top_ads, &config.pg_master, "sql/get_top_ads.sql", NULL, parameters, (FACTORY_NO_RES_CNTR | FACTORY_EMPTY_RESULT_OK));
