#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <time.h>

#include "trans.h"
#include <ctemplates.h>
#include "util.h"
#include "validators.h"
#include "trans_mail.h"
#include "log_event.h"
#include <bconf.h>

#define SPAMFILTER_DONE 0
#define SPAMFILTER_ERROR 1
#define SPAMFILTER_COUNT 2
#define SPAMFILTER_COUNT_RESULT 3
#define SPAMFILTER_LIST 4
#define SPAMFILTER_LIST_RESULT 5
#define SPAMFILTER_GET 6
#define SPAMFILTER_GET_RESULT 7
#define SPAMFILTER_DELETE 8
#define SPAMFILTER_DELETE_RESULT 9
#define SPAMFILTER_SEND 10
#define SPAMFILTER_SEND_RESULT 11

struct spamfilter_state {
	int state;
	struct cmd *cs;
	int num_of_mails;
	int num_of_ips;
	const char *action;
	int send_mail_count; 
	struct mail_data *md;
	int deleted_rows;
};

static struct validator v_spamfilter_action[] = {
	VALIDATOR_REGEX("^(send|delete|list|count|get)$", REG_EXTENDED, "ERROR_ACTION_INVALID"),
	{0}
};
ADD_VALIDATOR(v_spamfilter_action);

static struct c_param spamfilter_parameters[] = {
	{"remote_addr", 	P_REQUIRED,  	"v_ip"},
	{"remote_browser", 	0,  	"v_remote_browser"},
	{"sender_ip",		0,  	"v_ip"},
	{"action",	 	P_REQUIRED,  	"v_spamfilter_action"},
	{"mail_queue_id",	0,	"v_string_isprint"},
	{"email",		0,	"v_email_basic"},
	{"token",		P_REQUIRED,	"v_token:filter"},
	{NULL}
};

extern struct bconf_node *bconf_root;

static const char * spamfilter_fsm(void *v, struct sql_worker *worker, const char *errstr);

static void spamfilter_done(void *v) {
	struct spamfilter_state *state = v;
	struct cmd *cs = state->cs;
	transaction_logprintf(cs->ts, D_DEBUG, "spamfilter done");
	free(state);
	cmd_done(cs);
}

static void
bind_insert(struct bpapi *ba, struct spamfilter_state *state) {
	struct c_param *param;
	time_t now = time(NULL);
	struct tm tm;

	for (param = spamfilter_parameters; param->name; param++) {
		if (cmd_haskey(state->cs, param->name))
			bpapi_insert(ba, param->name, cmd_getval(state->cs, param->name));
	}

	localtime_r(&now, &tm);
	if (tm.tm_mon == 0) {
		/* Search in last year throughout january. */
		bpapi_set_printf(ba, "schemas", "blocket_%d", tm.tm_year + 1900 - 1);
	}
	bpapi_set_printf(ba, "schemas", "blocket_%d", tm.tm_year + 1900);
}


static void
spamfilter_send_cb(struct mail_data *md, void *arg, int status) {
	struct spamfilter_state *state = arg;

	state->send_mail_count--;

	if (status != 0)
		state->cs->status = "TRANS_MAIL_ERROR";

	syslog_ident("mail_queue", "SENT: [%s] (%s), %s, %s, %s, %s, %s",
		     mail_get_param(md, "list_id"),
		     mail_get_param(md, "to"), mail_get_param(md, "name"),
		     mail_get_param(md, "from"), mail_get_param(md, "remote_addr"),
		     mail_get_param(md, "subject"), mail_get_param(md, "body"));

	

	/* Get back to fsm only when the last mails has been sent */
	if (state->send_mail_count == 0)
		spamfilter_fsm(state, NULL, !strcmp(state->cs->status, "TRANS_MAIL_ERROR") ? state->cs->status : NULL);
}

static const char *
spamfilter_fsm(void *v, struct sql_worker *worker, const char *errstr) {
	struct spamfilter_state *state = v;
	struct cmd *cs = state->cs;
	struct bpapi ba;
	int count = 0;
        int i;

	transaction_logprintf(cs->ts, D_DEBUG, "spamfilter fsm %d", state->state);

        /* 
         * General error handling for errors.
         */
	if (errstr) {
		transaction_logprintf(cs->ts, D_ERROR, "spamfilter_fsm statement failed (%d, %s)",
				      state->state, errstr);
		if (strstr(errstr, "TRANS_MAIL_ERROR"))
			cs->status = "TRANS_MAIL_ERROR";
		else {
			cs->status = "TRANS_DATABASE_ERROR";
			cs->message = xstrdup(errstr);
		}
		state->state = SPAMFILTER_ERROR;
		if (worker) {
			/* Turn of final callback, if set. */
			sql_worker_set_wflags(worker, 0);
			return "ROLLBACK";
		}
	}


	while (state->state != SPAMFILTER_DONE) {
		switch(state->state) {
		/* Count mails and ips caught in spamfilter */
		case SPAMFILTER_COUNT:
			BPAPI_INIT(&ba, NULL, pgsql);
			bind_insert(&ba, state);
			/* Use master for up to date data. */
			sql_worker_template_query(&config.pg_master, "sql/filters/spamfilter_count.sql", &ba,
						  spamfilter_fsm, state, cs->ts->log_string);
			bpapi_output_free(&ba);
			bpapi_simplevars_free(&ba);
			bpapi_vtree_free(&ba);
			state->state = SPAMFILTER_COUNT_RESULT;
			return NULL;

		case SPAMFILTER_COUNT_RESULT:
			if (worker->next_row(worker)) {
				state->num_of_mails = atoi(worker->get_value_byname(worker, "num_of_mails"));
				state->num_of_ips = atoi(worker->get_value_byname(worker, "num_of_ips"));
			}

			transaction_printf(cs->ts, "spamfilter.num_of_mails:%d\n", state->num_of_mails);
			transaction_printf(cs->ts, "spamfilter.num_of_ips:%d\n", state->num_of_ips);

			if (strcmp(state->action, "list") == 0)
				state->state = SPAMFILTER_LIST;
			else
				state->state = SPAMFILTER_DONE;
			continue;

		/* List the overview of spammail */
		case SPAMFILTER_LIST:
			if (state->num_of_mails) {
				BPAPI_INIT(&ba, NULL, pgsql);
				bind_insert(&ba, state);
				/* Use master for up to date data. */
				sql_worker_template_query(&config.pg_master, "sql/filters/spamfilter_list.sql", &ba,
							  spamfilter_fsm, state, cs->ts->log_string);
				bpapi_output_free(&ba);
				bpapi_simplevars_free(&ba);
				bpapi_vtree_free(&ba);
				state->state = SPAMFILTER_LIST_RESULT;
				return NULL;
			}

			state->state = SPAMFILTER_DONE;
			continue;

		case SPAMFILTER_LIST_RESULT:
			transaction_logoff(cs->ts);
			while (worker->next_row(worker)) {
				int nfields = worker->fields(worker);
				for (i = 0; i < nfields; i++) {
					if (!worker->is_null(worker, i)) {
						const char *val = worker->get_value(worker, i);

						if (strchr(val, '\n'))
							transaction_printf(cs->ts, "blob:%ld:spamfilter.%d.%s\n%s\n",
									(unsigned long)strlen(val),
									count,
									worker->field_name(worker, i),
									val);
						else
							transaction_printf(cs->ts, "spamfilter.%d.%s:%s\n",
									count, 
									worker->field_name(worker, i),
									val);
					}
				}

				count++;
			}
			transaction_logon(cs->ts);

			state->state = SPAMFILTER_DONE;
			continue;

		/* List spammail from sender_ip */
		case SPAMFILTER_GET:
			BPAPI_INIT(&ba, NULL, pgsql);
			bind_insert(&ba, state);
			/* Use master for up to date data. */
			sql_worker_template_query(&config.pg_master, "sql/filters/spamfilter_get.sql", &ba,
						  spamfilter_fsm, state, cs->ts->log_string);
			bpapi_output_free(&ba);
			bpapi_simplevars_free(&ba);
			bpapi_vtree_free(&ba);
			state->state = SPAMFILTER_GET_RESULT;
			return NULL;

		case SPAMFILTER_GET_RESULT:
		{
			int limit = bconf_get_int(bconf_root, "*.controlpanel.modules.filter.spamfilter.max_mails");

			transaction_logoff(cs->ts);
			while (worker->next_row(worker)) {
				if (!limit || count < limit) {
					int nfields = worker->fields(worker);
					for (i = 0; i < nfields; i++) {
						const char *val = worker->get_value(worker, i);

						if (strchr(val, '\n'))
							transaction_printf(cs->ts, "blob:%ld:mail.%d.%s\n%s\n",
									(unsigned long)strlen(val),
									count,
									worker->field_name(worker, i),
									val);
						else
							transaction_printf(cs->ts, "mail.%d.%s:%s\n",
									   count, 
									   worker->field_name(worker, i),
									   worker->get_value(worker, i) ? worker->get_value(worker, i) : "");
					}
				}

				count++;
			}
			transaction_logon(cs->ts);
			transaction_printf(cs->ts, "count:%d\n", count);

			state->state = SPAMFILTER_DONE;
			continue;
		}

		/* Delete mail */
		case SPAMFILTER_DELETE:
			BPAPI_INIT(&ba, NULL, pgsql);
			bind_insert(&ba, state);
			sql_worker_template_query_flags(&config.pg_master, SQLW_FINAL_CALLBACK,
					"sql/filters/spamfilter_delete.sql", &ba,
					spamfilter_fsm, state, cs->ts->log_string);
			bpapi_output_free(&ba);
			bpapi_simplevars_free(&ba);
			bpapi_vtree_free(&ba);
			state->state = SPAMFILTER_DELETE_RESULT;
			return NULL;

		case SPAMFILTER_DELETE_RESULT:
			if (worker->sw_state != RESULT_DONE) {
				state->deleted_rows += worker->affected_rows(worker);
				return NULL;
			}
			transaction_printf(cs->ts, "%s:%d\n", cmd_getval(cs, "action"), state->deleted_rows);
			state->state = SPAMFILTER_DONE;
			continue;

		/* Send mail */
		case SPAMFILTER_SEND:
			BPAPI_INIT(&ba, NULL, pgsql);
			bind_insert(&ba, state);
			sql_worker_template_query(&config.pg_master, "sql/filters/spamfilter_get.sql", &ba,
						  spamfilter_fsm, state, cs->ts->log_string);
			bpapi_output_free(&ba);
			bpapi_simplevars_free(&ba);
			bpapi_vtree_free(&ba);
			state->state = SPAMFILTER_SEND_RESULT;
			return NULL;
			
		case SPAMFILTER_SEND_RESULT:
			{
				int cols;
				int i;
				/* CHUS228: mark if its needed to go to DELETE state, so the mail_queue state is changed to 'sent' */
				int abuse_exist = 0;

				state->state = SPAMFILTER_DELETE;
				while (worker->next_row(worker)) {
					int send_mail = 1;
					struct mail_data *md = zmalloc(sizeof(*md));

					cols = worker->fields(worker);
					for (i = 0; i < cols; i++) {
						if (!worker->is_null(worker, i)) {
							/* CHUS228: skip sending mail if its 'abuse' */
							if (strcmp(worker->field_name(worker, i), "state") == 0 && strcmp(worker->get_value(worker, i), "abuse") == 0) {
								send_mail = 0;
								abuse_exist = 1;
								break;
							}
							mail_insert_param(md, worker->field_name(worker, i),
									  worker->get_value(worker, i));
						}
					}

					if (send_mail == 1) {
						md->command = state;
						md->callback = spamfilter_send_cb;
						md->log_string = cs->ts->log_string;

						/* workaround to not go through qp_wrapper again */
						mail_insert_param(md, "wrapper_template", "none");

						mail_send(md, mail_get_param(md, "template_name"), NULL); /* XXX language */

						struct bpapi_vtree_chain vtree;
						bconf_vtree(&vtree, bconf_get(bconf_root, "*.*.common.statpoints"));
						stat_log(&vtree, "MAIL", mail_get_param(md, "list_id"));
						vtree_free(&vtree);

						state->send_mail_count++;
					} else {
						mail_free(md);
					}
				}

				/* CHUS228: if there were 'abuse' mails, they must change state */
				if (state->send_mail_count == 0) {
					if (abuse_exist == 0) {
						state->state = SPAMFILTER_DONE;
					}
					continue;
				} else {
					transaction_logprintf(cs->ts, D_INFO, "Will send %d mails", state->send_mail_count);
					return NULL;
				}
			}
		case SPAMFILTER_ERROR:
			state->state = SPAMFILTER_DONE;
			continue;
		}
	}

	spamfilter_done(state);
	return NULL;
}

static void
spamfilter(struct cmd *cs) {
	struct spamfilter_state *state;

	transaction_logprintf(cs->ts, D_DEBUG, "spamfilter command started");
	state = zmalloc(sizeof(*state));

	state->cs = cs;
	state->action = cmd_getval(cs, "action");

	if (strcmp(state->action, "list") == 0 ||  strcmp(state->action, "count") == 0)
		state->state = SPAMFILTER_COUNT;
	else if (strcmp(state->action, "get") == 0)
		state->state = SPAMFILTER_GET;
	else if (strcmp(state->action, "delete") == 0)
		state->state = SPAMFILTER_DELETE;
	else if (strcmp(state->action, "send") == 0)
		state->state = SPAMFILTER_SEND;
	else
		state->state = SPAMFILTER_DONE;

	spamfilter_fsm(state, NULL, NULL);
}

ADD_COMMAND(spamfilter, NULL, spamfilter, spamfilter_parameters, NULL);
