#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <time.h>

#include "trans.h"
#include "util.h"
#include <ctemplates.h>
#include "validators.h"
#include <bconf.h>
#include <event.h>

#define SETNOTICE_DONE          0
#define SETNOTICE_ERROR         1
#define SETNOTICE_INIT          2
#define SETNOTICE_CHECK         3
#define SETNOTICE_GET_ID        4

struct setnotice_state {
	int state;
	struct cmd *cs;
};

struct validator v_notice_id[] = {
	VALIDATOR_INT(1, -1, "ERROR_INVALID_NOTICE_ID"),
	{0}
};
ADD_VALIDATOR(v_notice_id);

static struct c_param parameters[] = {
	{"token",          P_REQUIRED, "v_token:adqueue"},
	{"remote_addr",    P_REQUIRED, "v_ip"},
	{"remote_browser", 0, "v_remote_browser"},
	{"notice_id",      0, "v_notice_id"},
	{"abuse",          0, "v_bool"},
	{"body",           0, "v_string_isprint_blob"},
	{"delete",         0, "v_bool"},
	{"uid",            0, "v_uid"},
	{"user_id",        0, "v_user_id"},
	{"ad_id",          0, "v_ad_id"},
	{"email",          0, "v_string_isprint"},
	{"type",           0, "v_string_isprint"},
	{NULL, 0}
};

static const char *setnotice_fsm(void *, struct sql_worker *, const char *);


/*****************************************************************************
 * setnotice_done
 * Exit function.
 **********/
static void
setnotice_done(void *v) {
	struct setnotice_state *state = v;
	struct cmd *cs = state->cs;
	free(state);
	cmd_done(cs);
}

/*****************************************************************************
 * bind_insert
 * Initialise template API before a call to sql_worker_template_query().
 **********/
static int
bind_insert(struct bpapi *ba, struct setnotice_state *state) {
	bpapi_insert(ba, "token", cmd_getval(state->cs, "token"));

	if (!cmd_haskey(state->cs, "delete") || !atoi(cmd_getval(state->cs, "delete"))) {
		if (cmd_haskey(state->cs, "abuse")) 
			bpapi_insert(ba, "abuse", cmd_getval(state->cs, "abuse"));

		if (cmd_haskey(state->cs, "body")) 
			bpapi_insert(ba, "body", cmd_getval(state->cs, "body"));
	}

	if (cmd_haskey(state->cs, "ad_id"))
		bpapi_insert(ba, "ad_id", cmd_getval(state->cs, "ad_id"));

	if (cmd_haskey(state->cs, "user_id"))
		bpapi_insert(ba, "user_id", cmd_getval(state->cs, "user_id"));

	if (cmd_haskey(state->cs, "uid"))
		bpapi_insert(ba, "uid", cmd_getval(state->cs, "uid"));

	if (cmd_haskey(state->cs, "email"))
		bpapi_insert(ba, "email", cmd_getval(state->cs, "email"));

	if (cmd_haskey(state->cs, "type"))
		bpapi_insert(ba, "type", cmd_getval(state->cs, "type"));

	return 0;
}

/*****************************************************************************
 * setnotice_fsm
 * State machine
 **********/
static const char *
setnotice_fsm(void *v, struct sql_worker *worker, const char *errstr) {
	struct setnotice_state *state = v;
	struct cmd *cs = state->cs;
        struct bpapi ba;

	/* 
	 * General error handling for db errors.
	 */
	if (errstr) {
                transaction_logprintf(cs->ts, D_ERROR, "setnotice_fsm statement failed (%d, %s)", 
                                      state->state,
                                      errstr);
		if (strstr(errstr, "ERROR_NOTICE_EXISTS")) {
			cs->status = "TRANS_ERROR";
			cs->error = "ERROR_NOTICE_EXISTS";
		} else if (strstr(errstr, "ERROR_AD_NOTICE_EXISTS")) {
			cs->status = "TRANS_ERROR";
			cs->error = "ERROR_AD_NOTICE_EXISTS";
		} else if (strstr(errstr, "ERROR_INVALID_NOTICE_ID")) {
			cs->status = "TRANS_ERROR";
			cs->error = "ERROR_INVALID_NOTICE_ID";
		} else {
			cs->status = "TRANS_DATABASE_ERROR";
			cs->message = xstrdup(errstr);
		}
		state->state = SETNOTICE_ERROR;
        }

	while (1) {
		transaction_logprintf(cs->ts, D_DEBUG, "setnotice_fsm(%p, %p, %p), state=%d", 
				      v, worker, errstr, state->state);
		
		switch (state->state) {
		case SETNOTICE_INIT:
			/* Initialise the templateparse structure */
			BPAPI_INIT(&ba, NULL, pgsql);
			if (bind_insert(&ba, state)) {
				bpapi_output_free(&ba);
				bpapi_simplevars_free(&ba);
				bpapi_vtree_free(&ba);
				state->state = SETNOTICE_ERROR;
				continue;
			}

			/* Register SQL query with our setnotice_fsm() as callback function */
			if (cmd_haskey(cs, "delete") && atoi(cmd_getval(state->cs, "delete")))
				sql_worker_template_query(&config.pg_master, 
							  "sql/delete_notice.sql", &ba, 
							  setnotice_fsm, state, cs->ts->log_string);
			else
				sql_worker_template_query(&config.pg_master, 
							  "sql/setnotice.sql", &ba, 
							  setnotice_fsm, state, cs->ts->log_string);

			/* Clean-up */
			bpapi_output_free(&ba);
			bpapi_simplevars_free(&ba);
			bpapi_vtree_free(&ba);

			/* Next state */
			if (cmd_haskey(cs, "delete") && atoi(cmd_getval(state->cs, "delete")))
				state->state = SETNOTICE_CHECK;
			else
				state->state = SETNOTICE_GET_ID;

			/* Return to libevent */
			return NULL;

		case SETNOTICE_CHECK:
			if (worker->affected_rows(worker) < 1) {
				state->state = SETNOTICE_ERROR;
				cs->status = "TRANS_ERROR";
				cs->error = "ERROR_INVALID_NOTICE_ID";
				continue;
			}
                        state->state = SETNOTICE_DONE;
			continue;
			
                case SETNOTICE_GET_ID:
			worker->next_row(worker);
			transaction_printf(cs->ts, "notice_id:%s\n", worker->get_value(worker, 0));
			state->state = SETNOTICE_DONE;
			continue;
			
		case SETNOTICE_DONE:
			setnotice_done(state);
			return NULL;

		case SETNOTICE_ERROR:
			transaction_logprintf(cs->ts, D_ERROR, "Error state");
			setnotice_done(state);
			return NULL;

		default:
			/* MAJOR STATE ERROR */
			cs->status = "TRANS_ERROR";
			transaction_logprintf(cs->ts, D_ERROR, "Unknown state (%d)", 
					      state->state);
			return NULL;

		}
	}
}

/*****************************************************************************
 * setnotice
 * Main function that initialise a state-structure and calls the state machine.
 **********/
static void
setnotice(struct cmd *cs) {
	transaction_logprintf(cs->ts, D_DEBUG, "Start Setnotice transaction");
	struct setnotice_state *state;
	state = zmalloc(sizeof(*state));
	state->cs = cs;
	state->state = SETNOTICE_INIT;

	setnotice_fsm(state, NULL, NULL);
}

ADD_COMMAND(setnotice,     /* Name of command */
		NULL,
            setnotice,     /* Main function name */
            parameters,  /* Parameters struct */
            "Setnotice command");/* Command description */
