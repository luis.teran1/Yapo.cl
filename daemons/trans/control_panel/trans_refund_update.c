#include "factory.h"

static struct c_param parameters[] = {
	{"refund_id", P_REQUIRED, "v_integer"},
	{"refund_status", P_REQUIRED,"v_array:pending,refunded,failed"},
	{"token", P_REQUIRED, "v_token:Refund.searchRefund"},
	{"remote_addr", P_OPTIONAL, "v_ip"},
	{"remote_browser", P_OPTIONAL, "v_remote_browser"},
	{NULL, 0}
};

static struct factory_data data = {
	"refund",
	FACTORY_RES_SHORT_FORM | FACTORY_EMPTY_RESULT_OK
};

static const struct factory_action actions[]= {
	FA_SQL("", "pgsql.master", "sql/call_refund_update.sql", FA_OUT),
	{0}
};

FACTORY_TRANS(refund_update, parameters, data, actions);
