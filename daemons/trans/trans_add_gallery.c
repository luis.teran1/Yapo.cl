#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <time.h>

#include "trans.h"
#include "util.h"
#include <ctemplates.h>
#include "validators.h"
#include <bconf.h>
#include <event.h>

typedef enum {
	ADD_GALLERY_DONE,
	ADD_GALLERY_ERROR,
	ADD_GALLERY_INIT,
	ADD_GALLERY_RESULT,
	ADD_GALLERY_SEND_MAIL
} state_t;

static const char *state_lut[] = {
	"ADD_GALLERY_DONE",
	"ADD_GALLERY_ERROR",
	"ADD_GALLERY_INIT",
	"ADD_GALLERY_RESULT",
	"ADD_GALLERY_SEND_MAIL"
};

struct add_gallery_state {
	state_t state;
        char *sql;
	char *errstr;
	struct cmd *cs;
        struct event timeout_event;
};

static struct c_param parameters[] = {
	{"remote_addr",         P_REQUIRED, "v_ip"},
	{"ad_id",               P_REQUIRED, "v_gallery_ad_id"},
	{"expire_time", 	P_REQUIRED, "v_product_expire_time"},
	// TODO: implement validators
	{"gallery_type",        0, "v_ip"},
	{"action_id",    0, "v_string_isprint"},
	{"email",         		0, "v_ip"},
	{"remote_browser", 	0, 		"v_remote_browser"},
	{"token;batch", 	P_XOR, 		"v_token:adminad.bump;v_bool"},
	{NULL, 0}
};

static const char *add_gallery_fsm(void *, struct sql_worker *, const char *);

extern struct bconf_node *bconf_root;

/*****************************************************************************
 * add_gallery_done
 * Exit function.
 **********/
static void
add_gallery_done(void *v) {
	struct add_gallery_state *state = v;
	struct cmd *cs = state->cs;
	free(state);
	cmd_done(cs);
}

/*****************************************************************************
 * bind_insert
 * Initialise template API before a call to sql_worker_template_query().
 **********/
static void
bind_insert(struct bpapi *ba, struct add_gallery_state *state) {
	struct c_param *param;

	for (param = parameters; param->name; param++) {
		if (cmd_haskey(state->cs, param->name))
			bpapi_insert(ba, param->name, cmd_getval(state->cs, param->name));
	}
	if (cmd_haskey(state->cs, "token"))
		bpapi_insert(ba, "token", cmd_getval(state->cs, "token"));
}

static void
add_gallery_admail_cb (struct internal_cmd *intcmd, const char *line, void *data) {
	struct add_gallery_state *state = data;

	if (!line)
		add_gallery_fsm(state, NULL, state->errstr);
	else if (!state->errstr) {
		const char *cp = strchr(line, ':');

		if (cp && IS_ERROR (cp + 1))
			state->errstr = xstrdup(cp + 1);
	}
}


static const char *
add_gallery_fsm(void *v, struct sql_worker *worker, const char *errstr) {
	struct add_gallery_state *state = v;
	struct cmd *cs = state->cs;
        struct bpapi ba;

	if (state->sql) {
		free(state->sql);
		state->sql = NULL;
	}

	if (errstr) {
		transaction_logprintf(cs->ts, D_ERROR, "add_gallery_fsm statement failed (%s(%d), %s)",
			  state_lut[state->state],
							  state->state,
							  errstr);
		if (errstr && strstr(errstr, "ERROR_AD_NOT_FOUND")) {
			cs->status = "TRANS_ERROR";
			cs->error = "ERROR_AD_NOT_FOUND";
		}
		else {
			cs->status = "TRANS_DATABASE_ERROR";
			cs->message = xstrdup(errstr);
		}
		state->state = ADD_GALLERY_ERROR;
    }

	while (1) {
		transaction_logprintf(cs->ts, D_DEBUG, "add_gallery_fsm(%p, %p, %p), state=%s(%d)",
				      v, worker, errstr, state_lut[state->state], state->state);

		switch (state->state) {
		case ADD_GALLERY_INIT:
			state->state = ADD_GALLERY_RESULT;

			BPAPI_INIT(&ba, NULL, pgsql);
			bind_insert(&ba, state);

			sql_worker_template_query(&config.pg_master,
						  "sql/call_add_gallery.sql", &ba,
						  add_gallery_fsm, state, cs->ts->log_string);

			bpapi_output_free(&ba);
			bpapi_simplevars_free(&ba);
			bpapi_vtree_free(&ba);
			return NULL;

		case ADD_GALLERY_RESULT:
			if (worker->next_row(worker)) {
				state->state = ADD_GALLERY_DONE;
			}
			else {
				state->state = ADD_GALLERY_ERROR;
			}
			continue;

		// The email sent by this state must be parametrized, since it's all wrong: sends info about 'bump' and
		// in the header has info about 'gallery 7', even if you are adding a gallery 1 or 30.
		case ADD_GALLERY_SEND_MAIL:
			state->state = ADD_GALLERY_DONE;
			transaction_internal_printf(add_gallery_admail_cb, state,
				"cmd:admail\n"
				"ad_id:%s\n"
				"mail_type:gallery_ok\n"
				"commit:1\n"
				"end\n",
				cmd_getval(cs, "ad_id")
			);
			return NULL;

		case ADD_GALLERY_DONE:
			add_gallery_done(state);
			return NULL;

		case ADD_GALLERY_ERROR:
			transaction_logprintf(cs->ts, D_ERROR, "Error state");
			add_gallery_done(state);
			return NULL;

		default:
			/* MAJOR STATE ERROR */
			cs->status = "TRANS_ERROR";
			transaction_logprintf(cs->ts, D_ERROR, "Unknown state (%d)",
					      state->state);

			add_gallery_done(state); /* If this state were run there would be a memory leak */
			return NULL;

		}
	}
}

/*****************************************************************************
 * add_gallery
 * Main function that initialise a state-structure and calls the state machine.
 **********/
static void
add_gallery(struct cmd *cs) {
	transaction_logprintf(cs->ts, D_DEBUG, "Start add_gallery transaction");
	struct add_gallery_state *state;
	state = zmalloc(sizeof(*state));
	state->cs = cs;
	state->state = ADD_GALLERY_INIT;

	add_gallery_fsm(state, NULL, NULL);
}

ADD_COMMAND(add_gallery,     /* Name of command */
		NULL,
            add_gallery,     /* Main function name */
            parameters,  /* Parameters struct */
            "add_gallery command");/* Command description */
