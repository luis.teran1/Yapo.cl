#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "db.h"
#include "util.h"
#include "trans.h"

static struct action_param *
create_param(const char *prefix, const char *key, const char *value) {
	struct action_param *p;
	size_t sz = sizeof (*p) + strlen(key) + 1 + strlen(prefix) + 1;
	
	if (value)
		sz += strlen(value) + 1;
	
	p = xmalloc(sz);

	p->key = (char*)(p + 1);
	p->prefix = p->key + strlen(key) + 1;
	if (value)
		p->value = p->prefix + strlen(prefix) + 1;
	else
		p->value = NULL;

	strcpy(p->prefix, prefix);
	strcpy(p->key, key);
	if (value)
		strcpy(p->value, value);

	return p;
}

/*
   Parse loadaction expects to be called with a worker returning a result like:
   SELECT
   	"prefix" AS prefix,
	col1,
	col2,
	col3, ...
   FROM
   	table;

   The result will be added to a TAILQ (pointed to by act), which will be created
   if act is NULL.
   The result will be stored in one of two possible ways, as determined by the prefix:

   i) aka 'get-field'
   The result is stored as key, value pairs where the key is:
   prefix.colx and the value is the column value.

   ii)
   If the prefix contains the substring "param", or there is a "type" column with value "param", the key will be :
   prefix.value of col1 and the value will be the value of col2

   If the prefix ends with a period ("prefix.") a row-sequence will be.
   I.e.: 
   prefix.0.colx
   prefix.1.colx
   prefix.2.colx

 */

struct action*
parse_loadaction(struct sql_worker *worker, struct transaction *ts, struct action *act) {
	int get_field = 0;
	int fc = 0;
	struct action *action;
	int rownum = 0;

	if (act) {
		action = act;
	} else {
		action = xmalloc(sizeof (*action));
		TAILQ_INIT(&action->h);
	}

	transaction_logprintf(ts, LOG_DEBUG, "parse_loadaction");

	get_field = -1;
	while (worker->rows(worker) && worker->next_row(worker)) {
		char *prefix_rownum = NULL;
		const char *prefix = worker->get_value_byname(worker, "prefix");
		int fields = worker->fields(worker);

		/* If requested, add a rownum to the prefix */
		if (prefix[strlen(prefix) - 1] == '.') {
			xasprintf(&prefix_rownum, "%s%u", prefix, rownum);
			prefix = prefix_rownum;
		}

		if (get_field == -1) {
			const char *type = worker->get_value_byname(worker, "type");

			/* Determine which insert-mode (i or ii as above) should be used */
			if (strstr(prefix, "param") || (type && !strcmp(type, "param"))) {
				get_field = 0;
			} else {
				get_field = 1;
			}
		}

		if (get_field) {
			for (fc = 1 ; fc < fields ; fc++) {
				if (!worker->is_null(worker, fc)) {
					const char *fn = worker->field_name(worker, fc);
					struct action_param *ap;

					ap = create_param(prefix, 
							  fn,
							  worker->is_null(worker, fc) ? NULL :
							  worker->get_value(worker, fc));

					TAILQ_INSERT_TAIL(&action->h, ap, entry);
				}
			}
		} else {
			struct action_param *ap = create_param(prefix, 
							       worker->get_value(worker, 1),
							       worker->is_null(worker, 2) ? NULL :
							       worker->get_value(worker, 2));
			TAILQ_INSERT_TAIL(&action->h, ap, entry);
		}

		if (prefix_rownum)
			free(prefix_rownum);

		rownum++;
	}

	return action;
}

void
parse_loadaction_cleanup(struct action *action) {
	struct action_param *ap;
	while ((ap = TAILQ_FIRST(&action->h)) != NULL) {
		TAILQ_REMOVE(&action->h, ap, entry);

		free(ap);
	}

	free(action);
}

const char *
parse_loadaction_get_value(struct action *action, const char *prefix, const char *key) {
	struct action_param *ap;
        TAILQ_FOREACH(ap, &action->h, entry) {
                if (strcmp(ap->key, key) == 0 &&
                    strcmp(ap->prefix, prefix) == 0) {
                        return(ap->value);
                }
        }
        return NULL;
}

struct action_param *
parse_loadaction_pop_param(struct action *action, const char *prefix, const char *key) {
	struct action_param *ap;
        TAILQ_FOREACH(ap, &action->h, entry) {
                if (strcmp(ap->key, key) == 0 &&
                    strcmp(ap->prefix, prefix) == 0) {
			TAILQ_REMOVE(&action->h, ap, entry);
			return ap;
                }
        }
        return NULL;
}
