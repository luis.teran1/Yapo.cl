#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <time.h>

#include "trans.h"
#include "util.h"
#include <ctemplates.h>
#include "validators.h"
#include <bconf.h>
#include <event.h>
#include "trans_mail.h"

#define ABUSE_REPORT_DONE  		0
#define ABUSE_REPORT_ERROR		1
#define ABUSE_REPORT_INIT		2
#define ABUSE_REPORT_DATA_USER		3
#define ABUSE_REPORT_DATA_AD_VERSIONS	4
#define ABUSE_REPORT_DATA_ADS		5
#define ABUSE_REPORT_GET_AD_ID		6
#define ABUSE_REPORT_LOOP_AD_IDS	7

extern struct bconf_node* bconf_root;

struct ad_id_entry {
	char *schema;
	char *ad_id;
	TAILQ_ENTRY(ad_id_entry) ad_id_list;
};

struct abuse_report_state {
	int state;
	struct cmd *cs;
	char *uid;
	char *user_id;
	TAILQ_HEAD(,ad_id_entry) ad_ids;
};

static struct c_param parameters[] = {
	{"token", P_REQUIRED, "v_token:search.abuse_report"},
	{"remote_addr", 0, "v_ip"},
	{"remote_browser", 0, "v_remote_browser"},
	{"search_type", P_REQUIRED, "v_search_type"},
	{"time_from", 0, "v_timestamp"},
	{"time_to", 0, "v_timestamp"},
	{NULL, 0}
};

static const char *abuse_report_fsm(void *, struct sql_worker *, const char *);


/*****************************************************************************
 * abuse_report_done
 * Exit function.
 **********/
static void
abuse_report_done(void *v) {
	struct abuse_report_state *state = v;
	struct cmd *cs = state->cs;

	if (state->uid)
		free(state->uid);
	if (state->user_id)
		free(state->user_id);
	free(state);
	cmd_done(cs);
}

/*****************************************************************************
 * bind_insert
 * Initialise template API before a call to sql_worker_template_query().
 **********/
static void
bind_insert(struct bpapi *ba, struct abuse_report_state *state) {
	struct c_param *param;

	for (param = parameters; param->name; param++) {
		if (cmd_haskey(state->cs, param->name)) {
			bpapi_insert(ba, param->name, cmd_getval(state->cs, param->name));
		}
	}

	bpapi_insert(ba, "query", cmd_getval(state->cs, cmd_getval(state->cs, "search_type")));
}

/*****************************************************************************
 * abuse_report_fsm
 * State machine
 **********/
static const char *
abuse_report_fsm(void *v, struct sql_worker *worker, const char *errstr) {
	struct abuse_report_state *state = v;
	struct cmd *cs = state->cs;
        struct bpapi ba;

	/*
	 * General error handling for db errors.
	 */
	if (errstr) {
                transaction_logprintf(cs->ts, D_ERROR, "abuse_report_fsm statement failed (%d, %s)",
                                      state->state,
                                      errstr);
                cs->status = "TRANS_DATABASE_ERROR";
                cs->message = xstrdup(errstr);
		state->state = ABUSE_REPORT_ERROR;
        }

	while (1) {
		transaction_logprintf(cs->ts, D_DEBUG, "abuse_report_fsm(%p, %p, %p), state=%d",
				      v, worker, errstr, state->state);

		switch (state->state) {
		case ABUSE_REPORT_INIT:
			/* next state */
			state->state = ABUSE_REPORT_GET_AD_ID;
			TAILQ_INIT(&state->ad_ids);

			/* Initialise the templateparse structure */
			BPAPI_INIT_APP(&ba, host_bconf, "controlpanel", buf, pgsql);
			bind_insert(&ba, state);

			/* Register SQL query with our abuse_report_fsm() as callback function */
			/* Make sure to switch to slave if read-only query. */
			sql_worker_template_query(&config.pg_slave,
					"sql/get_ad_ids_by_email_or_uid.sql", &ba,
					abuse_report_fsm, state, cs->ts->log_string);

			/* Clean-up */
			bpapi_output_free(&ba);
			bpapi_simplevars_free(&ba);
			bpapi_vtree_free(&ba);

			/* Return to libevent */
			return NULL;

		case ABUSE_REPORT_GET_AD_ID:
			while (worker->next_row(worker)) {
				struct ad_id_entry *ad_id;

				ad_id = xmalloc(sizeof(*ad_id));
				ad_id->schema = xstrdup(worker->get_value(worker, 0));
				ad_id->ad_id = xstrdup(worker->get_value(worker, 1));
				TAILQ_INSERT_TAIL(&state->ad_ids, ad_id, ad_id_list);
			}
			state->state = ABUSE_REPORT_LOOP_AD_IDS;

			continue;

		case ABUSE_REPORT_LOOP_AD_IDS:
		{
			struct ad_id_entry *ad_id;

			if (TAILQ_EMPTY(&state->ad_ids)) {
				state->state = ABUSE_REPORT_DONE;
				continue;
			}

			ad_id = TAILQ_FIRST(&state->ad_ids);
			TAILQ_REMOVE(&state->ad_ids, ad_id, ad_id_list);

			BPAPI_INIT(&ba, NULL, pgsql);

			bpapi_insert(&ba, "ad_id", ad_id->ad_id);
			bpapi_insert(&ba, "schema", ad_id->schema);

			if (cmd_haskey(cs, "time_to"))
				bpapi_insert(&ba, "time_to", cmd_getval(cs, "time_to"));
			if (cmd_haskey(cs, "time_from"))
				bpapi_insert(&ba, "time_from", cmd_getval(cs, "time_from"));

			state->state = ABUSE_REPORT_DATA_USER;
			sql_worker_template_query_flags(&config.pg_slave, SQLW_FINAL_CALLBACK,
					"sql/create_abuse_report.sql", &ba,
					abuse_report_fsm, state, cs->ts->log_string);

			/* Clean-up */
			bpapi_output_free(&ba);
			bpapi_simplevars_free(&ba);
			bpapi_vtree_free(&ba);
			free(ad_id->ad_id);
			free(ad_id->schema);
			free(ad_id);

			return NULL;
		}

		case ABUSE_REPORT_DATA_USER:
		{
			int new_user = 0;

			if (worker->next_row(worker)) {
				if (!state->uid || strcmp(worker->get_value(worker, 0), state->uid) != 0) {
					new_user = 1;
					if (state->uid)
						free(state->uid);
					state->uid = xstrdup(worker->get_value(worker, 0));
				}
				if (new_user || !state->user_id || strcmp(worker->get_value(worker, 1), state->user_id) != 0) {
					new_user = 1;
					if (state->user_id)
						free(state->user_id);
					state->user_id = xstrdup(worker->get_value(worker, 1));
				}
				if (new_user) {
					transaction_printf(cs->ts, "abuse_report.%s.%s.user.email:%s\n", state->uid, state->user_id, worker->get_value(worker, 2));
					transaction_printf(cs->ts, "abuse_report.%s.%s.user.num_emails:%s\n", state->uid, state->user_id, worker->get_value(worker, 3));
				}
			}

			state->state = ABUSE_REPORT_DATA_AD_VERSIONS;
			return NULL;
		}

		case ABUSE_REPORT_DATA_AD_VERSIONS:
		{
			int fields = worker->fields(worker);
			int fc;
			int row = 1;

			transaction_logoff(cs->ts);

			while (worker->next_row(worker)) {
				for (fc = 0 ; fc < fields ; fc++) {
					if (!worker->is_null(worker, fc)) {
						const char *val = worker->get_value(worker, fc);

						if (strchr(val, '\n')) {
							transaction_printf(cs->ts, "blob:%ld:abuse_report.%s.%s.ads.%s.%d.ads.%s\n%s\n",
									   (unsigned long)strlen(val),
									   state->uid,
									   state->user_id,
									   worker->get_value(worker, 0),
									   row,
									   worker->field_name(worker, fc),
									   val);
						} else {
							transaction_printf(cs->ts, "abuse_report.%s.%s.ads.%s.%d.ads.%s:%s\n",
									   state->uid,
									   state->user_id,
									   worker->get_value(worker, 0),
									   row,
									   worker->field_name(worker, fc),
									   val);
						}
					}
				}
				row++;
			}

			transaction_logon(cs->ts);

			state->state = ABUSE_REPORT_DATA_ADS;
			return NULL;
		}

		case ABUSE_REPORT_DATA_ADS:
		{
			const char *prefix = NULL;
			int fields = worker->fields(worker);
			int fc;

			transaction_logoff(cs->ts);

			while (worker->next_row(worker)) {
				for (fc = 0 ; fc < fields ; fc++) {
					if (!worker->is_null(worker, fc)) {
						const char *val = worker->get_value(worker, fc);

						if (strcmp("prefix", worker->field_name(worker, fc)) == 0) {
							prefix = val;
							continue;
						}

						if (val[0] == '{') {
							const char *cp;
							/* Array */
							for (val++; val; val = cp) {
								int vlen;

								cp = strchr(val, ',');
								if (cp)
									vlen = cp++ - val;
								else
									vlen = strlen(val) - 1;
								if (vlen)
									transaction_printf(cs->ts, "abuse_report.%s.%s.ads.%s.%s.%s.%.*s\n",
											   state->uid,
											   state->user_id,
											   worker->get_value_byname(worker, "ad_id"),
											   worker->get_value_byname(worker, "action_id"),
											   prefix,
											   vlen,
											   val);
							}
						} else if (strchr(val, '\n')) {
							transaction_printf(cs->ts, "blob:%ld:abuse_report.%s.%s.ads.%s.%s.%s.%s\n%s\n",
									   (unsigned long)strlen(val),
									   state->uid,
									   state->user_id,
									   worker->get_value_byname(worker, "ad_id"),
									   worker->get_value_byname(worker, "action_id"),
									   prefix,
									   worker->field_name(worker, fc),
									   val);

						} else {
							transaction_printf(cs->ts, "abuse_report.%s.%s.ads.%s.%s.%s.%s:%s\n",
									   state->uid,
									   state->user_id,
									   worker->get_value_byname(worker, "ad_id"),
									   worker->get_value_byname(worker, "action_id"),
									   prefix,
									   worker->field_name(worker, fc),
									   val);
						}
					}
				}
			}

			transaction_logon(cs->ts);

			state->state = ABUSE_REPORT_LOOP_AD_IDS;
			return NULL;
		}

		case ABUSE_REPORT_DONE:
			abuse_report_done(state);
			return NULL;

		case ABUSE_REPORT_ERROR:
			transaction_logprintf(cs->ts, D_ERROR, "Error state");
			abuse_report_done(state);
			return NULL;

		default:
			/* MAJOR STATE ERROR */
			cs->status = "TRANS_ERROR";
			transaction_logprintf(cs->ts, D_ERROR, "Unknown state (%d)",
					      state->state);
			abuse_report_done(state);
			return NULL;

		}
	}
}


/*****************************************************************************
 * Main function that initialise a state-structure and calls the state machine.
 **********/
static void
abuse_report(struct cmd *cs) {
	struct abuse_report_state *state;
	state = zmalloc(sizeof(*state));
	state->cs = cs;
	state->state = ABUSE_REPORT_INIT;

	abuse_report_fsm(state, NULL, NULL);
}

ADD_COMMAND(abuse_report,     /* Name of command */
            NULL,/* Called during the initalisation phase */
            abuse_report,     /* Main function name */
            parameters,  /* Parameters struct */
            "List on call actions");/* Command description */

