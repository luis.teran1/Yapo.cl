#include "factory.h"
#include "bpapi.h"
#include <bconf.h>

extern struct bconf_node *bconf_root;

static struct c_param parameters[] = {
	{"from",		0,		"v_string_isprint"},
	{"to",			0,		"v_string_isprint"},
	{"doc_type",		P_REQUIRED,	"v_string_isprint"},
	{"purchase_id",		P_REQUIRED,	"v_integer"},
	{"product_id",		P_REQUIRED,	"v_string_isprint"},
	{"quantity",		P_REQUIRED,	"v_string_isprint"},
	{"total_price",		P_REQUIRED,	"v_string_isprint"},
	{"bill_date",		P_REQUIRED,	"v_string_isprint"},
	{"doc_num",		0,		"v_integer"},
	{"external_doc_id",	0,		"v_integer"},
	{"bill_name",		0,		"v_string_isprint"},
	{"auth_code",		0,		"v_string_isprint"},
	{NULL, 0}
};


static struct factory_data data = {
	"mail"
};

static const struct factory_action actions[] = {
        FA_MAIL("mail/bill_send_mail.mime"),
        {0}
};

FACTORY_TRANS(payment_receipt_mail, parameters, data, actions);
