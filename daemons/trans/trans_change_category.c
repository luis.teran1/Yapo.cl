#include "factory.h"

static struct c_param parameters[] = {
	{"ad_id",			P_MULTI | P_REQUIRED,	"v_ad_id"},
	{"category",		P_REQUIRED,				"v_category_check"},
	{"new_gender",		P_OPTIONAL,				"v_integer"},
	{"from_category",	P_REQUIRED,				"v_category_check"},
	{"remote_addr", 	0,						"v_ip"},
	{NULL, 0}
};

static struct factory_data data = {
        "change_category",
        FACTORY_NO_RES_CNTR | FACTORY_RES_SHORT_FORM
};

static const struct factory_action actions[]= {
	FA_SQL("", "pgsql.master", "sql/call_move_ads_to_category.sql", 0),
	{0}
};

FACTORY_TRANS(change_category, parameters, data, actions);
