#ifndef DB_H
#define DB_H

#include <queue.h>
#include "sql_worker.h"

struct adqueue_entry {
	TAILQ_ENTRY(adqueue_entry) queue;
	int ad_id;
	int action_id;
	char *schema;
	const char *filter;
	const char *dbc;
};

struct action_param {
	TAILQ_ENTRY(action_param) entry;
	char *key;
	char *value;
	char *prefix;
};

struct action {
	TAILQ_HEAD(,action_param) h;
};

struct transaction;
struct action *parse_loadaction(struct sql_worker *worker, struct transaction *ts, struct action*);
void parse_loadaction_cleanup(struct action*);
const char *parse_loadaction_get_value(struct action *action, const char *prefix, const char *key);
struct action_param *parse_loadaction_pop_param(struct action *action, const char *prefix, const char *key);

#endif
