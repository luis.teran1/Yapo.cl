#include "factory.h"
#include "bpapi.h"
#include <bconf.h>

extern struct bconf_node *bconf_root;

static struct c_param parameters[] = {
    {"user_email", P_REQUIRED, "v_email_basic"},
    {"ad_subject", P_REQUIRED, "v_subject"},
    {"url_report;noinfo", P_XOR, "v_store_external_url;v_integer"},
    {"delivered",  P_REQUIRED, "v_timestamp"},
    {NULL, 0}
};

static struct factory_data data = {
    "mail"
};

static const struct factory_action actions[] = {
    FA_MAIL("mail/send_autofact_report.mime"),
    {0}
};

FACTORY_TRANS(send_autofact_report, parameters, data, actions);
