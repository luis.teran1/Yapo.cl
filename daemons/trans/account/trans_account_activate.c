#include "factory.h"
#include "bpapi.h"
#include <bconf.h>
#include "utils/refresh_session.h"

/* This trans should be killed soon.
 * Integration tests in API should be changed in order to use
 * daemons/trans/account/trans_activation_account.c
 */

extern struct bconf_node *bconf_root;

static struct c_param parameters[] = {
	{"account_id", 		P_REQUIRED,		"v_user_id"},
	{NULL, 0}
};


static struct factory_data data = {
	"accounts",
	FACTORY_EMPTY_RESULT_OK
};

static const struct factory_action actions[] = {
	FA_SQL("", "pgsql.master", "sql/call_manage_account.sql", FASQL_OUT),
	FA_FUNC(refresh_account),
	{0}
};

FACTORY_TRANS(account_activate, parameters, data, actions);
