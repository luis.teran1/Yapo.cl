#include <stdio.h>
#include <string.h>
#include <ctype.h>

#include "factory.h"
#include "trans.h"
#include "util.h"
#include "utils/refresh_session.h"


static struct c_param parameters[] = {
	{"email", 0, "v_email_basic"},
	{"region", 0, "v_region"},
	{"commune", 0, "v_commune"},
	{NULL, 0}
};

static const struct factory_action refresh_session = FA_FUNC(refresh_account);

static const struct factory_action *
refresh_redis_if_needed(struct cmd *cs, struct bpapi *ba) {
	int updated = !strcmp(bpapi_get_element(ba, "set_commune_if_missing", 0), "t");
	if (updated)
		return &refresh_session;
	return NULL;
}

static struct factory_data data = {
	"accounts"
};

static const struct factory_action actions[] = {
	FA_SQL(NULL, "pgsql.master", "sql/call_set_commune_if_missing.sql", 0),
	FA_COND(refresh_redis_if_needed),
	FA_DONE()
};

FACTORY_TRANS(set_commune_if_missing, parameters, data, actions);
