#include <string.h>
#include <unistd.h>

#include "bconf.h"
#include "factory.h"
#include "fd_pool.h"
#include "sredisapi.h"
#include "utils/create_session_hash.h"
#include "utils/redis_trans_context.h"
#include "utils/send_backend_event.h"

#define REFRESH_OK             (0)
#define REFRESH_NEED_RETRY     (1)
#define REFRESH_CONN_ERROR     (2)
#define REFRESH_DATABASE_ERROR (3)

static const char *status_lut[] = {
	"OK",
	"NEED_RETRY",
	"CONN_ERROR",
	"REFRESH_DATABASE_ERROR",
};

extern struct bconf_node *bconf_root;

static int
fetch_account_data(struct cmd *cs, struct bpapi *ba, int *argc, char ***argv) {
	struct sql_worker *worker = NULL;
	const char *errstr = NULL;

	*argc = 0;
	*argv = NULL;
	worker = sql_blocked_template_query("pgsql.master", 0, "sql/call_get_account.sql", ba, cs->ts->log_string, &errstr);

	// Read data from the database and store it on (argc, argv)
	if (worker && sql_blocked_next_result(worker, &errstr) == 1) {
		int cols = worker->fields(worker);
		char **_argv = malloc(2 * cols * sizeof(char *));
		if (worker->next_row(worker)) {
			for (int col = 0; col < cols; col++) {
				const char *field = worker->field_name(worker, col);
				const char *value = worker->get_value(worker, col);
				int is_array = worker->is_array(worker, col);

				if (value && *value && !is_array) {
					_argv[(*argc)++] = xstrdup(field);
					_argv[(*argc)++] = xstrdup(value);
				}
			}
		}
		*argv = _argv;
	}

	int ret = 0;
	if (errstr) {
		if (factory_translate_trans_error(cs, errstr)) {
			// ACCOUNT_NOT_FOUND is not an error
		} else {
			cs->status = "TRANS_DATABASE_ERROR";
			cs->message = xstrdup(errstr);
			ret = -1;
		}
	}

	if (worker)
		sql_worker_put(worker);

	return ret;
}

static int notify_event(struct cmd *cs, struct bpapi *ba, int nargs, const char *args[nargs]) {
	// Check if it should send a backend event
	int be_enabled = bconf_get_int(trans_bconf, "refresh_account_cache.backend_event.send_enabled");
	if (be_enabled) {
		transaction_logprintf(cs->ts, D_INFO, "SEND REFRESH_ACCOUNT_CACHE BACKEND EVENT ACTIVE");
		bpapi_insert(ba, "type", "refresh_account_cache");
		char *key;
		char *omit_key;
		for (int i = 0; i < nargs; i = i + 2) {
			xasprintf(&omit_key, "refresh_account_cache.backend_event.omit_params.%s", args[i]);
			if (bconf_get_int_default(trans_bconf, omit_key, 0) != 1){
				xasprintf(&key, "param_%s", args[i]);
				bpapi_insert(ba, (const char *)key, args[i+1]);
			}
		}
		publish_backend_event(cs, ba);
		free(omit_key);
		free(key);
	}
	return 0;
}

static int
atomic_refresh(struct cmd *cs, struct bpapi *ba, struct fd_pool_conn *conn, const char *key) {
	// Watch profile key.
	// This prevents race conditions https://redis.io/topics/transactions#cas
	int status = redis_sock_watch(conn, key);
	if (status < 0)
		return REFRESH_CONN_ERROR;

	// Retrieve account data from database
	int argc = 0;
	char **argv = NULL;
	status = fetch_account_data(cs, ba, &argc, &argv);
	if (status < 0) {
		status = REFRESH_DATABASE_ERROR;
		goto cleanup;
	}
	// Account not found
	if (argc == 0)
		goto cleanup;

	// Atomically set the new data on redis
	redis_sock_multi(conn);
	redis_multi_sock_del(conn, key);
	redis_sock_hmset_array(conn, key, argc, (const char **) argv);
	status = redis_sock_exec(conn);
	if (status < 0)
		status = REFRESH_NEED_RETRY;

cleanup:
	transaction_logprintf(cs->ts, D_INFO, "atomic_refresh: %s (%d)", status_lut[status], status);

	if (status == REFRESH_OK && argc > 0) {
		// Send the backend event id enabled
		notify_event(cs, ba, argc, (const char **) argv);
	}

	for (int i = 0; i < argc; i++)
		free(argv[i]);

	free(argv);
	return status;
}

static int
refresh_later(struct cmd *cs, struct bpapi *ba, const char *email) {
	// Triggers when major communications problems arise (backoff times expressed in seconds)
	int backoffmin = bconf_get_int_default(bconf_root, "*.accounts.refresh_account_cache.queue.backoff.min", 60);
	int backoffmax = bconf_get_int_default(bconf_root, "*.accounts.refresh_account_cache.queue.backoff.max", 300);
	const char *queue = bconf_get_string_default(bconf_root, "*.accounts.refresh_account_cache.queue.name", "refresh_account");

	time_t backoff = rand() % backoffmax;
	if (backoff < backoffmin)
		backoff = backoffmin;

	// Command to queue
	char *cmd = NULL;
	size_t length = xasprintf(&cmd,
		"cmd:refresh_account_cache\n"
		"email:%s\n"
		"commit:1\n",
		cmd_getval(cs, "email")
	);

	// Queue for later
	transaction_internal_printf(TRANSACTION_INTERNAL_SYNC_CB, NULL,
		"cmd:queue\n"
		"commit:1\n"
		"queue:%s\n"
		"timeout:%ld\n"
		"blob:%zu:command\n"
		"%s\n",
		queue,
		backoff,
		length,
		cmd
	);

	free(cmd);
	return 0;
}

static int
refresh_account_cache(struct cmd *cs, struct bpapi *ba) {
	// Fetch configuration (backoff times expressed in milliseconds)
	int backoff;
	int retries = bconf_get_int_default(bconf_root, "*.accounts.refresh_account_cache.retries", 10);
	int backoffmin = bconf_get_int_default(bconf_root, "*.accounts.refresh_account_cache.backoff.min", 10);
	int backoffmax = bconf_get_int_default(bconf_root, "*.accounts.refresh_account_cache.backoff.max", 100);

	// Assemble redis key from mail
	const char *email = cmd_getval(cs, "email");
	char *key = create_profile_cache_hash(email);
	struct fd_pool_conn *conn = redis_sock_conn(redis_session_pool.pool, "master");

	// Try to refresh until success or exhaustion
	int r = REFRESH_NEED_RETRY;
	while (r != REFRESH_OK && retries > 0) {
		r = atomic_refresh(cs, ba, conn, key);
		switch (r) {
			case REFRESH_CONN_ERROR:
			case REFRESH_DATABASE_ERROR:
				// Retry with a slight backoff
				backoff = rand() % backoffmax;
				if (backoff < backoffmin)
					backoff = backoffmin;

				usleep(backoff * 1000);
				retries--;
				break;
		}
	}

	// Reschedule later via trans queues
	if (r != REFRESH_OK)
		refresh_later(cs, ba, email);

	// Cleanup
	fd_pool_free_conn(conn);
	free(key);
	return 0;
}

static struct c_param parameters[] = {
	{"email", P_REQUIRED, "v_email_basic"},
	{NULL, 0}
};

static struct factory_data data = {
	"accounts",
	FACTORY_NO_RES_CNTR | FACTORY_RES_SHORT_FORM
};

static const struct factory_action actions[]= {
	FA_FUNC(refresh_account_cache),
	{0}
};

FACTORY_TRANS(refresh_account_cache, parameters, data, actions);
