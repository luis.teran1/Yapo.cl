#include <string.h>

#include "bconf.h"
#include "factory.h"
#include "fd_pool.h"
#include "sha_wrapper.h"
#include "sredisapi.h"
#include "utils/create_email_hash.h"
#include "utils/create_session_hash.h"
#include "utils/refresh_session.h"
#include "utils/redis_trans_context.h"

#define PREFIX "acc_"

extern struct bconf_node *bconf_root;

static struct c_param parameters[] = {
	{"email;user_id;account_id", 	P_XOR, 	"v_email_basic;v_user_id;v_account_id"},
	{NULL, 0}
};

static struct factory_data data = {
	"accounts",
	FACTORY_NO_RES_CNTR | FACTORY_RES_SHORT_FORM
};

static void
hgetall_cb(const void *key, size_t klen, const void *value, size_t vlen, void *cbarg) {
	struct cmd *cs = cbarg;
	transaction_printf(cs->ts, "%*s:%*s\n", (int) klen, (char *) key, (int) vlen, (char *) value);
}

static const char *
get_email_from_redis(struct cmd *cs) {
	const char *account_id = cmd_haskey(cs, "account_id")
		? cmd_getval(cs, "account_id")
		: NULL;
	const char *user_id = cmd_haskey(cs, "user_id")
		? cmd_getval(cs, "user_id")
		: NULL;

	char *rkey = account_id
		? create_account_email_hash(account_id)
		: user_id
		? create_user_email_hash(user_id)
		: NULL;

	struct fd_pool_conn *conn = redis_sock_conn(redis_email_pool.pool, "master");
	const char *email = redis_sock_get(conn, rkey);
	fd_pool_free_conn(conn);
	free(rkey);
	return email;
}

static const struct factory_action from_sql = FA_SQL(PREFIX, "pgsql.master", "sql/call_get_account.sql", 0);
static const struct factory_action refresh_account_session = FA_FUNC(refresh_account);
static const struct factory_action get_account_done = FA_DONE();

static const struct factory_action *
read_from_somewhere(struct cmd *cs, struct bpapi *ba) {
	// Redis key is calculated with the email. if we might not have the email, get it from redis_email
	const char *email = cmd_haskey(cs, "email")
		? cmd_getval(cs, "email")
		: get_email_from_redis(cs);
	// If the email is not on redis_email, skip and load from database
	if (!email)
		return NULL;

	// Assemble redis key and save it for later
	char *rkey = create_profile_cache_hash(email);
	bpapi_insert(ba, "rkey", rkey);

	// Get ALL the things \o this avoids race conditions
	struct fd_pool_conn *conn = redis_sock_conn(redis_session_pool.pool, "master");
	int count = redis_sock_hgetall(conn, rkey, hgetall_cb, cs);
	fd_pool_free_conn(conn);
	free(rkey);

	// Redis had it? Then we have nothing else to do
	if (count > 0)
		return &get_account_done;

	// not in cache, try to populate it with refresh_account_cache
	bpapi_insert(ba, "email", email);
	return &refresh_account_session;
}

static const struct factory_action *
fetch_after_refresh(struct cmd *cs, struct bpapi *ba) {
	const char *rkey = bpapi_get_element(ba, "rkey", 0);
	// If there is no redis key, we don't have email in redis_email, so fetch from DB
	if (!rkey)
		return &from_sql;

	// Try to get the profile from redis (in case it was refreshed)
	struct fd_pool_conn *conn = redis_sock_conn(redis_session_pool.pool, "master");
	int count = redis_sock_hgetall(conn, rkey, hgetall_cb, cs);
	fd_pool_free_conn(conn);
	// If the profile is still not in redis, get it from DB
	if (count <= 0)
		return &from_sql;

	return &get_account_done;
}

static const struct factory_action *
parse_from_db(struct cmd *cs, struct bpapi *ba) {
	// Retrieve key list
	struct bpapi_loop_var loop = {0};
	bpapi_fetch_glob_loop(ba, PREFIX"*", &loop);

	int i = 0;
	const char *key, *value;
	const char **argv = xmalloc(2 * loop.len * sizeof(const char *));

	for (i = 0; i < loop.len; ++i) {
		key = loop.l.list[i] + sizeof(PREFIX) - 1;
		value = bpapi_has_key(ba, loop.l.list[i]) ? bpapi_get_element(ba, loop.l.list[i], 0) : "";
		if (*value)
			transaction_printf(cs->ts, "%s:%s\n", key, value);
	}

	if (loop.cleanup)
		loop.cleanup(&loop);

	free(argv);
	return NULL;
}

static const struct factory_action actions[]= {
	//FT739 When you edit your personal information
	//the profile's page if you load the page again
	//show the previous version of info
	//REPLICATION PROBLEM: moving this transaction to master
	//with the accounts API this petitions will be reduced.
	FA_COND(read_from_somewhere),
	FA_COND(fetch_after_refresh),
	FA_COND(parse_from_db),
	{0}
};

FACTORY_TRANS(get_account, parameters, data, actions);
