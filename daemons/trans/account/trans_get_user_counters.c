#include "factory.h"

static struct c_param parameters[] = {
	{"email", 	P_REQUIRED, 	"v_email_basic"},
	{"asearch_ids",		0, 	"v_string_isprint"},
	{NULL, 0}
};

static struct factory_data data = {
	"accounts",
	FACTORY_NO_RES_CNTR | FACTORY_RES_SHORT_FORM | FACTORY_EMPTY_RESULT_OK
};

static const struct factory_action actions[]= {
	FA_SQL("", "pgsql.master", "sql/call_get_user_counters.sql", FASQL_OUT),
	{0}
};

FACTORY_TRANS(get_user_counters, parameters, data, actions);


