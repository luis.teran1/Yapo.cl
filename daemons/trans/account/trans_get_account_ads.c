#include <bconf.h>
#include <bconfig.h>
#include <bsapi.h>
#include <ctemplates.h>
#include <ctype.h>
#include <event.h>
#include <hiredis.h>
#include <stdio.h>
#include <string.h>
#include <time.h>

#include "trans.h"
#include "util.h"
#include "validators.h"
#include "settings_utils.h"
#include "sredisapi.h"
#include "utils/redis_trans_context.h"

// Enum to allow us to iterate through results easily
typedef enum {
	PROP_AD_ID = 0,
	PROP_LIST_ID,
	PROP_LIST_TIME,
	PROP_SUBJECT,
	PROP_BODY,
	PROP_AD_PARAMS,
	PROP_COMPANY_AD,
	PROP_AD_TYPE,
	PROP_CATEGORY,
	PROP_PRICE,
	PROP_AD_STATUS,
	PROP_AD_MEDIA_ID,
	PROP_IMAGE_COUNT,
	PROP_DAILY_BUMP,
	PROP_WEEKLY_BUMP,
	PROP_LABEL,
	PROP_GALLERY_DATE,
	PROP_AD_PACK_STATUS,
	PROP_VIEWS,
	PROP_MAILS,
	PROP_COUNT
} ad_prop_t;

// Header for the different result columns
const char *ad_prop_name[] = {
	"ad_id",
	"list_id",
	"list_time",
	"subject",
	"body",
	"ad_params",
	"company_ad",
	"ad_type",
	"category",
	"price",
	"ad_status",
	"ad_media_id",
	"image_count",
	"daily_bump",
	"weekly_bump",
	"label",
	"gallery_date",
	"ad_pack_status",
	"views",
	"mails",
	NULL,
};

// ad_data_t will hold a result row from sql/call_get_account_ads.sql query
typedef struct {
	int on_asearch;
	int region;
	char *prop[PROP_COUNT];
} ad_data_t;

typedef struct {
	int n;
	ad_data_t *current_ad;
	ad_data_t *ads;
} ad_data_list_t;

typedef enum {
	CB_STATE_AD_ID,
	CB_STATE_VIEWS,
	CB_STATE_MAILS
} cb_state_t;

typedef struct {
	int i;
	ad_data_t *ad;
	ad_data_list_t *adl;
} stat_cb_data_t;

static ad_data_list_t* ad_data_list_new(int n) {
	ad_data_list_t *adl = xcalloc(1, sizeof(ad_data_list_t));
	adl->n = n;
	adl->ads = xcalloc(n, sizeof(ad_data_t));
	return adl;
}

static void ad_data_list_delete(ad_data_list_t *adl) {
	int i, j;
	for (i = 0; i < adl->n; ++i)
		for (j = 0; j < PROP_COUNT; ++j)
			free(adl->ads[i].prop[j]);
	free(adl->ads);
	free(adl);
}

static ad_data_list_t* ad_data_list_load(struct cmd *cs) {
	struct bpapi ba;
	struct sql_worker *worker = NULL;
	const char *errstr = NULL;

	BPAPI_INIT(&ba, NULL, pgsql);

	if (cmd_getval(cs, "user_id") != NULL) {
		bpapi_insert(&ba, "user_id", cmd_getval(cs, "user_id"));
	}
	else {
		bpapi_insert(&ba, "account_id", cmd_getval(cs, "account_id"));
	}

	if(cmd_haskey(cs,"query")){
		bpapi_insert(&ba, "query", cmd_getval(cs, "query"));
	}

	if(cmd_haskey(cs,"pack_sort")){
		bpapi_insert(&ba, "pack_sort", cmd_getval(cs, "pack_sort"));
	}

	if(cmd_haskey(cs,"all_ad_params")){
		bpapi_insert(&ba, "all_ad_params", cmd_getval(cs, "all_ad_params"));
	}

	worker = sql_blocked_template_query("pgsql.slave", 0, "sql/call_get_account_ads.sql", &ba, cs->ts->log_string, &errstr);
	bpapi_free(&ba);

	int i = 0;
	ad_data_list_t *adl = NULL;

	if (worker && sql_blocked_next_result(worker, &errstr) == 1) {
		int n_ads = worker->rows(worker);
		if (worker->rows(worker) > 0 && worker->next_row(worker)) {
			adl = ad_data_list_new(n_ads);
			/* Remaining premium products:
 			   - The value is taken from db if the ad is disabled
 			   - The value is taken from asearch if it is enabled and indexed */
			do {
				for (int col = 0; col < worker->fields(worker); col++) {
					const char *value = worker->get_value(worker, col);
					if ((col == PROP_DAILY_BUMP || col == PROP_WEEKLY_BUMP ) && value && strcmp(value,"") != 0) {
						if (!adl->ads[i].on_asearch) {
							if (!strcmp(adl->ads[i].prop[PROP_AD_STATUS], "disabled") || !strcmp(adl->ads[i].prop[PROP_AD_STATUS], "review-edit-disabled")) {
								xasprintf(&adl->ads[i].prop[col], "%d", atoi(value));
							}
							else {
								xasprintf(&adl->ads[i].prop[col], "%d", atoi(value) + 1);
							}
						}
					}
					else {
						adl->ads[i].prop[col] = xstrdup(value);
					}
				}
				/* When lua script is not in redis or redis down
				   we don't want to display (null) on views and mails */
				adl->ads[i].prop[PROP_VIEWS] = xstrdup("0");
				adl->ads[i].prop[PROP_MAILS] = xstrdup("0");
				i++;
			}
			while (worker->next_row(worker));
		}
	}

	if (errstr) {
        if (strstr(errstr, "ERROR_ACCOUNT_NOT_FOUND")) {
            cs->status = "TRANS_ERROR";
            cs->error = "ACCOUNT_NOT_FOUND";
		}
		else if (strstr(errstr, "ERROR_ACCOUNT_PENDING_CONFIRMATION")) {
            cs->status = "TRANS_ERROR";
            cs->error = "ACCOUNT_PENDING_CONFIRMATION";
		}
		else {
			cs->message = xstrdup(errstr);
			cs->status = "TRANS_ERROR";
			cs->error = "UNHANDLED_ERROR";
		}
		if (worker) {
			sql_worker_put(worker);
		}
		return NULL;
	}

	if (worker) {
		sql_worker_put(worker);
	}

	return adl;
}

// search ad_id in a list of ads
static ad_data_t* ad_data_list_search_by_ad_id(ad_data_list_t *adl, const char *ad_id) {
	int i;
	for(i = 0; i < adl->n; ++i) {
		ad_data_t *ad = &adl->ads[i];
		if (!strcmp(ad->prop[PROP_AD_ID], ad_id)) {
			return ad;
		}
	}
	return NULL;
}

// search list_id in a list of ads
static ad_data_t* ad_data_list_search_by_list_id(ad_data_list_t *adl, const char *list_id) {
	int i;
	for(i = 0; i < adl->n; ++i) {
		ad_data_t *ad = &adl->ads[i];
		if (!strcmp(ad->prop[PROP_LIST_ID], list_id)) {
			return ad;
		}
	}
	return NULL;
}

extern struct bconf_node *bconf_root;
void get_db_ads(struct cmd *cs);
void get_asearch_ads(struct cmd *cs, ad_data_list_t *ads);

static struct c_param parameters[] = {
	{"user_id;account_id", 	P_XOR, 			"v_integer;v_integer"},
	{"page_number", 		P_REQUIRED, 	"v_integer"},
	{"ads_quantity", 		P_OPTIONAL, 	"v_integer"},
	{"query", 				P_OPTIONAL, 	"v_string_isprint"},
	{"pack_sort",			P_OPTIONAL, 	"v_integer"},
	{"all_ad_params",			P_OPTIONAL, 	"v_integer"},
	{NULL, 0}
};

static void
search_cb(int type, int row, const char *col, const char **val, void *cb_data) {

	ad_data_list_t *adl = cb_data;

	switch (type) {
	case BS_VALUE:

		if (!strcmp(col, "ad_id") && val && val[0]) {
			ad_data_t *ad = ad_data_list_search_by_ad_id(adl, val[0]);
			if (ad) {
				ad->on_asearch = 1;
				adl->current_ad = ad;
			}
		}

		if (!strcmp(col, "region") && val && val[0]) {
			if (adl->current_ad) {
				adl->current_ad->region = atoi(val[0]);
			}
		}

		if (!strcmp(col, "daily_bump") && val && val[0] && strcmp(val[0],"") != 0) {
			if (adl->current_ad) {
				free(adl->current_ad->prop[PROP_DAILY_BUMP]);
				adl->current_ad->prop[PROP_DAILY_BUMP] = xstrdup(val[0]);
			}
		}

		if (!strcmp(col, "weekly_bump") && val && val[0] && strcmp(val[0],"") != 0) {
			if (adl->current_ad) {
				free(adl->current_ad->prop[PROP_WEEKLY_BUMP]);
				adl->current_ad->prop[PROP_WEEKLY_BUMP] = xstrdup(val[0]);
			}
		}
		break;
	}
}

// Updates ad status for dashboard based on DB and asearch data
static void
update_ad_status(struct cmd *cs, ad_data_t *ad) {

	char *on_asearch;
	xasprintf(&on_asearch, "%d", ad->on_asearch);

	transaction_logprintf(cs->ts, D_DEBUG, "get_account_ads: Looking ad_settings for [%s][%s]\n", ad->prop[PROP_AD_STATUS], on_asearch);

	// It's ok, cmd_setval will take care of freeing the original pointers
	cmd_setval(cs, "on_asearch", on_asearch);
	cmd_setval(cs, "status", ad->prop[PROP_AD_STATUS]);
	
	ad->prop[PROP_AD_STATUS] = get_setting_keyval_string(cs, "ad_settings", "status", "status");
}

static void
get_ad_stats_cb(const void *value, size_t vlen, void *cbarg) {
	stat_cb_data_t *data = cbarg;
	// The script outputs data in groups of 3: ad_id, #views, #mails
	switch (data->i % 3) {
	case CB_STATE_AD_ID:
		data->ad = ad_data_list_search_by_list_id(data->adl, value);
		break;
	case CB_STATE_VIEWS:
		if (data->ad) {
			free(data->ad->prop[PROP_VIEWS]);
			data->ad->prop[PROP_VIEWS] = xstrdup(value);
		}
		break;
	case CB_STATE_MAILS:
		if (data->ad) {
			free(data->ad->prop[PROP_MAILS]);
			data->ad->prop[PROP_MAILS] = xstrdup(value);
		}
		break;
	}
	data->i++;
}

/* get each ad stat from redis (views, replies)
   and put them into ad structure */
static void get_ad_stats(struct cmd *cs, ad_data_list_t *adl) {

	int i, count = 0;
	for (i = 0; i < adl->n; ++i )
		count += (adl->ads[i].on_asearch && adl->ads[i].prop[PROP_LIST_ID]);

	transaction_logprintf(cs->ts, D_DEBUG, "BEFORE REDIS ad count = %d", count);
	// we don't have active ads
	if (count == 0) {
		transaction_logprintf(cs->ts, D_DEBUG, "Skip getting stats from redis, no active ads found");
		return;
	}

	const char **argv = xmalloc(count * sizeof(const char *));

	int argc = 0;

	for (i = 0; i < adl->n; ++i) {
		if (adl->ads[i].on_asearch && adl->ads[i].prop[PROP_LIST_ID]) {
			transaction_logprintf(cs->ts, D_DEBUG, "BEFORE REDIS argv[%d] = ads[%d] = %s", argc, i, adl->ads[i].prop[PROP_LIST_ID]);
			argv[argc++] = adl->ads[i].prop[PROP_LIST_ID];
		}
	}

	stat_cb_data_t data;
	data.i = CB_STATE_AD_ID;
	data.ad = NULL;
	data.adl = adl;

	struct fd_pool_conn *conn = redis_sock_conn(redis_stat_pool.pool, "master");
	const char *lua_script_sha1 = bconf_get_string(bconf_root, "*.*.accounts.lua_scripts.get_ad_stats.hash");
	redis_sock_evalsha(conn, lua_script_sha1, argc, argc, argv, get_ad_stats_cb, &data);

	fd_pool_free_conn(conn);
	free(argv);
}

// Get ads from db and call asearch
void get_db_ads(struct cmd *cs) {

	ad_data_list_t *adl = ad_data_list_load(cs);
	int ads_per_page = bconf_get_int(bconf_root, "*.*.accounts.dashboard.ads_per_page");
	if (cmd_haskey(cs,"ads_quantity")) {
		ads_per_page = atoi(cmd_getval(cs, "ads_quantity"));
	}
	int offset = ads_per_page * atoi(cmd_getval(cs, "page_number"));
	int page_ads = 0;
	if (adl) {
		get_asearch_ads(cs, adl);
		get_ad_stats(cs, adl);

		// Iterates all ads, but only print the ads from offset to offset + ads per page
		int i, j, n, p;
		for(i = 0, n = 0, p = 0; i < adl->n ; ++i){
			update_ad_status(cs, &adl->ads[i]);
			if (strcmp(adl->ads[i].prop[PROP_AD_STATUS], "dashboard-hidden")) {
				if(n >= offset && n < offset + ads_per_page){
					++page_ads;
					for (j = 0; j < PROP_COUNT; ++j) {
						if(cmd_getval(cs, "all_ad_params") == NULL) {
							if (j != PROP_AD_TYPE && j != PROP_AD_PARAMS && j != PROP_BODY && j != PROP_COMPANY_AD) {
								transaction_printf(cs->ts, "%s:%s\n", ad_prop_name[j], adl->ads[i].prop[j]);
							}
						}
						else {
							transaction_printf(cs->ts, "%s:%s\n", ad_prop_name[j], adl->ads[i].prop[j]);
						}
					}
					transaction_printf(cs->ts, "%s:%d\n", "region", adl->ads[i].region);
					transaction_printf(cs->ts, "%s:%d\n", "on_asearch", adl->ads[i].on_asearch);
				}

				if (strcmp(adl->ads[i].prop[PROP_AD_PACK_STATUS], "")) {
					p++;
				}
				++n;
			}
		}
		transaction_printf(cs->ts, "page_ads:%d\n", page_ads);
		transaction_printf(cs->ts, "n_ads:%d\n", n);
		if(p > 0){
			transaction_printf(cs->ts, "n_pack_ads:%d\n", p);
		}
		ad_data_list_delete(adl);
	}
	else {
		if (!cs->error) {
			transaction_printf(cs->ts, "n_ads:0\n");
		}
	}

	cmd_done(cs);
}

void get_asearch_ads(struct cmd *cs, ad_data_list_t *ads) {
	struct bpapi_vtree_chain vtree = {0};
	struct bconf_node *node;
	struct bsearch_api ba;
	struct bsconf bs = {0};
	char *search_str = NULL;


	if (cmd_getval(cs, "user_id") != NULL) {
		xasprintf(&search_str, "0 lim:999 user_id:%s", cmd_getval(cs, "user_id"));
	}
	else {
		xasprintf(&search_str, "0 lim:999 account:%s", cmd_getval(cs, "account_id"));
	}

	node = bconf_get(bconf_root, "*.*.common.asearch");
	bconf_vtree(&vtree, node);
	bsconf_init_vtree(&bs, &vtree, 1);
	bsearch_init_bsconf(&ba, &bs, NULL);

	bsearch_search(&ba, search_str, search_cb, ads);

	bsearch_cleanup(&ba);
	vtree_free(&vtree);
	free(search_str);
}

ADD_COMMAND(get_account_ads,			/* Name of command */
			NULL,						/* I don't know add it if you know */
            get_db_ads,					/* Main function name */
            parameters,					/* Parameters struct */
            "get account ads command");	/* Command description */
