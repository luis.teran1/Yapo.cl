#include "factory.h"

static struct c_param parameters[] = {
	{"type",	P_REQUIRED,	"v_integer"},
	{NULL, 0}
};


static struct factory_data data = {
	"get_pending_account",
	FACTORY_NO_RES_CNTR | FACTORY_RES_SHORT_FORM | FACTORY_EMPTY_RESULT_OK 
};

static const struct factory_action actions[] = {
	FA_SQL("", "pgsql.slave", "sql/get_pending_account.sql", FASQL_OUT),
	{0}
};

FACTORY_TRANS(get_pending_account, parameters, data, actions);
