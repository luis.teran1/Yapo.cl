#include "factory.h"
#include "bpapi.h"
#include <bconf.h>
#include "sredisapi.h"
#include "fd_pool.h"
#include "sha_wrapper.h"
#include "utils/create_session_hash.h"
#include "utils/redis_trans_context.h"

extern struct bconf_node *bconf_root;

static struct c_param parameters[] = {
	{"email",	   P_REQUIRED, "v_string_isprint"},
	{"mail_type",  P_REQUIRED, "v_array:account_confirmation,reset_account_password,activate_account,login_max_attempts"},
	{"nickname",   0,          "v_string_isprint"},
	{"op",         0,          "v_integer"},
	{NULL, 0}
};

/*
 * Function: generate_token
 * ------------------------
 *  gets or generates a hash to identify the user who wants to activate account or recover his/her password.
 *  This hash ends up in the email to be sent to the user.
 *
 */
static int generate_token(struct cmd *cs, struct bpapi *ba){
	char *redis_key, *token;
	const char *prefix = bconf_get_string(bconf_root, "*.*.accounts.session_redis_prefix");

	if (!prefix) {
		transaction_logprintf(cs->ts, D_ERROR, "generate_token: we need a prefix! (*.*.accounts.session_redis_prefix)");
		return -1;
	}

	const char *email = cmd_getval(cs, "email");
	const char *mail_type = cmd_getval(cs, "mail_type");

	if (!strcmp(mail_type, "reset_account_password") || !strcmp(mail_type, "login_max_attempts"))
		xasprintf(&redis_key, "%s_forgot_password_%s", prefix, email);
	else
		xasprintf(&redis_key, "%s_validate_%s", prefix, email);

	struct fd_pool_conn *conn = redis_sock_conn(redis_session_pool.pool, "master");
	token = redis_sock_get(conn, redis_key);
	if (!token) {
		token = create_session_hash(email);
		int exp = bconf_get_int(bconf_root, "*.*.accounts.activation_reminder_email.ttl.seconds");
		if (exp) {
			int r = redis_sock_set(conn, redis_key, token, exp);
			if (0 != r) {
				transaction_logprintf(cs->ts, D_ERROR, "generate_token: Couldn't store token. I'm sad."); 
				fd_pool_free_conn(conn);
				free(redis_key);
				free(token);
				return -1;
			}
		}
	}
	fd_pool_free_conn(conn);
	bpapi_insert(ba, "hashval", token);
	free(redis_key);
	free(token);
	return 0;
}

/*
 * Function: set_subject
 * Sets the subject of the email to be sent to the user depending on the mail type
 *
 */
static int set_subject(struct cmd *cs, struct bpapi *ba){
	const char *mail_type = cmd_getval(cs, "mail_type");
	if(!strcmp(mail_type, "account_confirmation")){
		bpapi_insert(ba, "subject", bconf_get_string(bconf_root, "*.*.language.ACCOUNT_CREATION_EMAIL_SUBJECT.es"));
	} else if(!strcmp(mail_type, "reset_account_password") || !strcmp(mail_type, "login_max_attempts")){
		bpapi_insert(ba, "subject", bconf_get_string(bconf_root, "*.*.language.FORGOT_PASSWORD_EMAIL_SUBJECT.es"));
	} else if(!strcmp(mail_type, "activate_account")){
		bpapi_insert(ba, "subject", bconf_get_string(bconf_root, "*.*.language.ACTIVATE_ACCOUNT_SUBJECT.es"));
	} else {
		transaction_logprintf(cs->ts, D_ERROR, "Unknown mail_type in accountmail, (how did I get here?! jumped over the validator)");
		return -1;
	}
	return 0;
}

static const struct factory_action get_account_name = FA_TRANS("trans/call_get_account.txt", "fat_", FATRANS_BACAPTURE);

/*
 * Factory action: has_nickname
 * Conditional function (https://scmcoord.com/wiki/Trans_factory) which checks if we have the nickname parameter.
 * If we don't, gets the nickname from the account of the given email.
 *
 */
static const struct factory_action *
has_nickname(struct cmd *cs, struct bpapi *ba) {
	if (!cmd_haskey(cs, "nickname")) {
		return &get_account_name;
	}
	return NULL;
}

static struct factory_data data = {
	"accounts"
};

static const struct factory_action actions[] = {
		FA_FUNC(generate_token),
		FA_FUNC(set_subject),
		FA_COND(has_nickname),
		FA_MAIL("mail/accounts_mail_template.mime"),
		FA_DONE()
};

FACTORY_TRANS(accountmail, parameters, data, actions);
