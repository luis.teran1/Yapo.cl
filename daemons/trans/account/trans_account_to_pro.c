#include "factory.h"
#include "utils/refresh_session.h"

static struct c_param parameters[] = {
	{"ad_id", P_REQUIRED, "v_id"},
	{"pro_type", P_REQUIRED, "v_string_isprint"},
	{NULL, 0}
};

static struct factory_data data = {
        "account_to_pro",
        FACTORY_NO_RES_CNTR | FACTORY_RES_SHORT_FORM
};

static const struct factory_action refresh_account_session = FA_FUNC(refresh_account);

static const struct factory_action *
refresh_redis_if_needed(struct cmd *cs, struct bpapi *ba) {
	int is_pro = !strcmp(bpapi_get_element(ba, "is_pro", 0), "t");
	if (is_pro)
		return &refresh_account_session;
	return NULL;
}

static int account_to_pro_mail(struct cmd *cs, struct bpapi *ba) {

 	const char *email = bpapi_get_element(ba, "email", 0);
 	const char *name = bpapi_get_element(ba, "name", 0);
 	const char *just_became_pro = bpapi_get_element(ba, "just_became_pro", 0);

	if (just_became_pro != NULL && strcmp(just_became_pro, "t") == 0) {
		transaction_internal_printf(TRANSACTION_INTERNAL_SYNC_CB, NULL,
			"cmd:account_to_pro_mail\n"
			"email:%s\n"
			"name:%s\n"
			"commit:1\n",
			email,
			name
		);
	}

 	return 0;
 }

static const struct factory_action actions[]= {
	FA_SQL("", "pgsql.master", "sql/get_ad_info_account_to_pro.sql", 0),
	FA_SQL("", "pgsql.master", "sql/call_account_to_pro.sql", FASQL_OUT),
	FA_COND(refresh_redis_if_needed),
	FA_FUNC(account_to_pro_mail),
	{0}
};

FACTORY_TRANS(account_to_pro, parameters, data, actions);
