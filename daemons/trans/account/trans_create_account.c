/**
 * This trans command is used to create a new yapo account.
 * The modifications we did was to add source and gender
 * - Source** is used to identify from where the user created the account: desktop, msite, android, ios ***
 * - Gender can be female or male
 *
 * ** Source is now optional, but soon it should be mandatory
 * *** sources are configured in conf/common/bconf/bconf.txt.accounts
 */
#include "factory.h"
#include "fd_pool.h"
#include "sredisapi.h"
#include "utils/refresh_session.h"
#include "utils/create_email_hash.h"
#include "utils/redis_trans_context.h"
#include "utils/send_backend_event.h"

static struct c_param parameters[] = {
	{"name",				P_REQUIRED,	"v_name"},
	{"rut",					0,			"v_pay_rut_optional"},
	{"email", 				P_REQUIRED,	"v_email_basic"},
	{"phone;mobile",		P_OR,		"v_phone_strict;v_bool"},
	{"is_company", 			P_REQUIRED,	"v_accounts_is_company"},
	{"region;mobile", 		P_OR,		"v_region;v_bool"},
	{"password",			P_REQUIRED,	"v_account_hashed_password"},
	{"commune",				0,			"v_commune_optional"},
	{"address",				0,			"v_pay_address_optional"},
	{"contact",				0,			"v_pay_contact"},
	{"lob",					0,			"v_pay_lob_optional"},
	{"gender",				0,			"v_gender_account"},
	{"accept_conditions",	P_REQUIRED,	"v_accounts_accept_conditions"},
	{"source",				P_REQUIRED,	"v_source_account"},
	{"uid",					0,			"v_integer"},
	{"autoactivate",		0,			"v_integer"},
	{"business_name",		0,			"v_business_name"},
	{NULL, 0}
};

static int save_email_redis(struct cmd *cs, struct bpapi *ba) {
	const char *email = cmd_getval(cs, "email");
	const char *account_id = bpapi_get_element(ba, "o_account_id", 0);
	const char *user_id = bpapi_get_element(ba, "o_user_id_", 0);

	char *free_key_account_id = NULL;
	const char *rkey_account_id = NULL;
	char *free_key_user_id = NULL;
	const char *rkey_user_id = NULL;

	free_key_account_id = create_account_email_hash(account_id);
	rkey_account_id = (const char *) free_key_account_id;
	free_key_user_id = create_user_email_hash(user_id);
	rkey_user_id = (const char *) free_key_user_id;

	struct fd_pool_conn *conn = redis_sock_conn(redis_email_pool.pool, "master");
	redis_sock_set(conn, rkey_account_id, email, -1);
	redis_sock_set(conn, rkey_user_id, email, -1);
	fd_pool_free_conn(conn);

	free(free_key_account_id);
	free(free_key_user_id);
	return 0;
}

static int notify_event(struct cmd *cs, struct bpapi *ba) {
	
	// Check if it should send a backend event
	int be_enabled = bconf_get_int(trans_bconf, "create_account.backend_event.send_enabled");
	if (be_enabled) {
		transaction_logprintf(cs->ts, D_INFO, "SEND ACCOUNT BACKEND EVENT ACTIVE");
		bpapi_insert(ba, "type", "create_account");
		bpapi_insert(ba, "param_email", cmd_getval(cs, "email"));
		bpapi_insert(ba, "param_account_id", bpapi_get_element(ba, "o_account_id", 0));
		bpapi_insert(ba, "param_user_id", bpapi_get_element(ba, "o_user_id_", 0));

		publish_backend_event(cs, ba);
	}
	return 0;
}


static const struct factory_action activate_account_trans = FA_TRANS("trans/call_activate_account.txt", "aa_", FATRANS_OUT);

static const struct factory_action *
maybe_activate(struct cmd *cs, struct bpapi *ba) {
	if (cmd_haskey(cs, "autoactivate") && !strcmp(cmd_getval(cs, "autoactivate"), "1"))
		return &activate_account_trans;
	return NULL;
}

static struct factory_data data = {
	"accounts"
};

static const struct factory_action actions[] = {
	FA_SQL("", "pgsql.master", "sql/call_create_account.sql", FASQL_OUT),
	FA_FUNC(save_email_redis),
	FA_FUNC(refresh_account),
	FA_COND(maybe_activate),
	FA_FUNC(notify_event),
	{0}
};

FACTORY_TRANS(create_account, parameters, data, actions);
