/**
 * This trans manage normal authentication by email
 * After an user is authenticated, a session token is created and saved on redis
 * Social login is in the account/trans_account_login.c trans
 */

#include "factory.h"
#include "sredisapi.h"
#include "fd_pool.h"
#include "utils/create_session_hash.h"
#include "utils/redis_trans_context.h"


/*************** internal functions ***************/
static void account_login_basic(struct cmd * cs, struct bpapi *ba);

static struct c_param parameters[] = {
	{"email",				P_REQUIRED,	"v_email_basic"},
	{"hash_password",		P_OPTIONAL,	"v_passwd_sha1_not_required"},
	{"otp_hash_password",	P_OPTIONAL,	"v_passwd_sha1_not_required"},
	{"is_persistent",		0,			"v_bool"},
	{0}
};
static struct factory_data data = {
	"account_login",
	FACTORY_RES_CNTR
};

static int account_login_main(struct cmd *cs, struct bpapi *ba) {
	if (cmd_haskey(cs, "email") && cmd_haskey(cs, "hash_password")) {
		account_login_basic(cs, ba);
	} else if (cmd_haskey(cs, "email") && cmd_haskey(cs, "otp_hash_password")) {
		transaction_logprintf(cs->ts, D_DEBUG, "account_login_otp -> not implemented");
	} else {
		cs->status = "TRANS_ERROR";
		transaction_printf(cs->ts, "login:INVALID_PARAMS_COMBINATION\n");
		return -1;
	}
	return 0;
}

static char *
log_user_in(struct cmd *cs) {
	const char *email = cmd_getval(cs, "email");
	char *token = create_session_hash(email);

	//token never expires
	struct fd_pool_conn *conn = redis_sock_conn(redis_session_pool.pool, "master");
	int create = redis_sock_hset(conn, token, "email", email);
	fd_pool_free_conn(conn);

	if (create != 1) {
		transaction_logprintf(cs->ts, D_ERROR, "create_session_on_redis failed!");
		free(token);
		return NULL;
	}

	return token;
}

static void account_login_basic(struct cmd *cs, struct bpapi *ba) {
	const char *db_password = bpapi_get_element(ba, "password", 0);
	const char *db_account_id = bpapi_get_element(ba, "account_id", 0);
	const char *db_account_status = bpapi_get_element(ba, "account_status", 0);
	if (strcmp(db_account_status, "inactive") == 0 || strcmp(db_account_status, "pending_confirmation") == 0) {
		transaction_printf(cs->ts, "email:ERROR_NOT_ACTIVE_ACC\n");
		cs->status = "TRANS_ERROR";
		return;
	}
	const char *password = cmd_getval(cs, "hash_password");
	if (strcmp(password, db_password) != 0) {
		transaction_printf(cs->ts, "password:ERROR_PASSWORD_MISMATCH\n");
		cs->status = "TRANS_ERROR";
		return;
	}
	char *token = log_user_in(cs);
	if (token) {
		transaction_logprintf(cs->ts, D_DEBUG, "account_id:%s", db_account_id);
		transaction_printf(cs->ts, "account_id:%s\n", db_account_id);
		transaction_printf(cs->ts, "token:%s\n", token);
		free(token);
	} else {
		transaction_logprintf(cs->ts, D_DEBUG, "LOGIN_FAILED\n");
		cs->status = "LOGIN_FAILED";
	}
}

static const struct factory_action actions[] = {
	FA_SQL(NULL, "pgsql.master", "sql/check_account_passwd.sql", 0),
	FA_FUNC(account_login_main),
	FA_DONE()
};

FACTORY_TRANS(account_login, parameters, data, actions);
