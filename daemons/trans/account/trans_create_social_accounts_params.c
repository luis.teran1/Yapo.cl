/**
 * This trans command is used to save user information provided by a social account.
 * When you use facebook to login in yapo, we ask for some data like the user's gender, birthday or name.
 * This params are saved in a specific table: social_accounts_params, all of them, but birthday and gender
 * that are saved in the accounts table, because any user, even without facebook, can set this info.
 * The params that we save are configured in a bconf file with the key "common.social_accounts_params"
 * this conf includes if the param is required or not and what validator to use.
 */
#include "factory.h"

extern struct bconf_node *bconf_root;
struct c_param *social_params = NULL;

static struct c_param parameters[] = {
	{"email;social_account_id",	P_XOR,	"v_email_basic;v_user_id"},
	{"_params",					0,		"v_social_accounts_params"},
	{NULL, 0}
};

static int
vf_social_accounts_params(struct cmd_param *param, const char *arg) {
	struct bconf_node *social_account_params = bconf_get(bconf_root, "*.*.common.social_accounts_params");
	int count = bconf_count(social_account_params);
	int i, cnt_params = 0, required = 0;
	struct c_param *st = NULL;
	const char *validator = NULL;
	social_params = zmalloc((NDYNPARAMS) * sizeof(struct c_param));
	const char *name = NULL;
	struct bconf_node *adparam = NULL;

	for (i = 0; i < count; i++) {
		adparam = bconf_byindex(social_account_params, i);
		name = bconf_get_string(adparam, "name");
		required = bconf_get_int(adparam, "required");
		validator = bconf_get_string(adparam, "validator");
		struct c_param new_params = {name, required, validator};
		social_params[cnt_params++] = new_params;
	}

	for (st = social_params; st->name; st++) {
		cmd_extra_param(param->cs, st);
	}
	return 0;
}

struct validator v_social_accounts_params[] = {
	VALIDATOR_FUNC(vf_social_accounts_params),
	{0}
};

ADD_VALIDATOR(v_social_accounts_params);
static struct factory_data data = {
	"social_accounts",
	FACTORY_RES_SHORT_FORM
};

static int
bind_social_accounts_params(struct cmd *cs, struct bpapi *ba) {
	struct bconf_node *social_account_params = bconf_get(bconf_root, "*.*.common.social_accounts_params");
	int count = bconf_count(social_account_params), i;

	for (i = 0; i < count; i++) {
		struct bconf_node *adparam = bconf_byindex(social_account_params, i);
		const char *name = bconf_get_string(adparam, "name");
		const char *value = cmd_getval(cs, name);

		if (value) {
			// and param is neither gender nor birthdate
			if (strcmp(name, "gender") && strcmp(name, "birthday")) {
				bpapi_insert(ba, "param_name", name);
				bpapi_insert(ba, "param_value", value);
			}
		} else {
			int required = bconf_get_int(adparam, "required");
			if (required == 1) {
				cs->status = "TRANS_ERROR";
				transaction_printf(cs->ts, "login:INVALID_PARAMS_COMBINATION\n");
				return -1;
			}
		}
	}

	return 0;
}

static int
done_social_accounts_params(struct cmd *cs, struct bpapi *ba) {
	free(social_params);
	return 0;
}

static const struct factory_action actions[] = {
	FA_FUNC(bind_social_accounts_params),
	FA_SQL(NULL, "pgsql.master", "sql/call_create_social_accounts_params.sql", FASQL_OUT),
	FA_FUNC(done_social_accounts_params),
	FA_DONE()
};

FACTORY_TRANS(create_social_accounts_params, parameters, data, actions);
