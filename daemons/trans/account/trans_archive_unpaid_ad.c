#include "factory.h"

static struct c_param parameters[] = {
	{"account_id", P_REQUIRED, "v_account_id"},
	{"ad_id",      P_REQUIRED, "v_id"        },
	{NULL, 0}
};

static struct factory_data data = {
	"controlpanel",
	FACTORY_NO_RES_CNTR | FACTORY_RES_SHORT_FORM
};

static const struct factory_action actions[] = {
	FA_SQL(NULL, "pgsql.master", "sql/archive_unpaid_ad.sql", 0),
	FA_OUT("trans/flush_account_ads_cache"),
	{0}
};

FACTORY_TRANS(archive_unpaid_ad, parameters, data, actions);
