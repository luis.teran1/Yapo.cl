#include "factory.h"
#include "util.h"
#include "validators.h"
#include "trans.h"
#include <stdio.h>
#include <string.h>
#include "command.h"
#include "sredisapi.h"
#include "fd_pool.h"
#include "sha_wrapper.h"
#include "utils/create_session_hash.h"
#include "utils/redis_trans_context.h"

static struct c_param parameters[] = {
	{"token",	P_REQUIRED,	"v_ng_token"},
	{"uuid", 	P_REQUIRED,	"v_string_isprint"},
	{NULL, 0}
};

static const struct factory_action from_sql = FA_SQL("redis_", "pgsql.slave", "sql/get_msg_center_uuid.sql", 0);

static int get_email_from_token(struct cmd *cs, struct bpapi *ba) {
	const char *token = cmd_getval(cs, "token");
	struct fd_pool_conn *conn = redis_sock_conn(redis_session_pool.pool, "master");
	char *email = redis_sock_hget(conn, token, "email");
	fd_pool_free_conn(conn);
	if (!email) {
		cs->error = "TOKEN_NOT_FOUND";
		cs->status = "TRANS_ERROR";
		return 1;
	}
	bpapi_insert(ba, "email", email);
	free(email);
	return 0;
}

static const struct factory_action *
get_uuid_from_email(struct cmd *cs, struct bpapi *ba) {
	const char *email = bpapi_get_element(ba, "email", 0);
	char *session_hash = create_profile_cache_hash(email);
	struct fd_pool_conn *conn = redis_sock_conn(redis_session_pool.pool, "master");
	char *uuid = redis_sock_hget(conn, session_hash, "uuid");
	fd_pool_free_conn(conn);
	free(session_hash);

	if (!uuid) {
		transaction_printf(cs->ts, "from:UUID_FROM_DB\n");
		return &from_sql;
	}
	transaction_logprintf(cs->ts, D_DEBUG, "from:UUID_FROM_REDIS");
	bpapi_insert(ba, "redis_uuid", uuid);
	free(uuid);
	return NULL;
}

static int check_user_token(struct cmd *cs, struct bpapi *ba) {

	if (!bpapi_has_key(ba, "uuid") || !bpapi_has_key(ba, "redis_uuid")) {
		cs->error = "ERROR_GETTING_UUID";
		cs->status = "TRANS_ERROR";
		return 1;
	}

	const char *redis_uuid = bpapi_get_element(ba, "redis_uuid", 0);
	const char *param_uuid = cmd_getval(cs, "uuid");

	if (strcmp(redis_uuid, param_uuid) != 0) {
		//if user uuid from ngapi does not match with user id in redis
		struct fd_pool_conn *conn = redis_sock_conn(redis_session_pool.pool, "master");
		const char *token = cmd_getval(cs, "token");
		redis_sock_del(conn, token);
		fd_pool_free_conn(conn);
		cs->error = "UUIDS_NOT_MATCH";
		cs->status = "TRANS_ERROR";
		return 1;
	}
	return 0;
}

static struct factory_data data = {
	"accounts"
};

static const struct factory_action actions[] = {
	FA_FUNC(get_email_from_token),
	FA_COND(get_uuid_from_email),
	FA_FUNC(check_user_token),
	FA_DONE()
};

FACTORY_TRANS(check_user_uuid_by_token, parameters, data, actions);
