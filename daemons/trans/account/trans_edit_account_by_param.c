/**
 * This trans command is used to update specific account params such as name, phone, gender, etc.
 * If you created an account with facebook login, and then you go to ad insert desktop
 * you will have a form asking for your missing data, phone for sure.
 * Also if you have all the data, but you want to change it, you can use the edit button and change your name or phone.
 * The main goal of this command is to handle these atomic changes in different tables.
 */
#include "factory.h"
#include "utils/refresh_session.h"

static struct c_param parameters[] = {
	{"email",			P_REQUIRED,	"v_email_basic"},
	{"name",			P_OPTIONAL,	"v_name"},
	{"phone",			P_OPTIONAL,	"v_phone_basic"},
	{"region",			P_OPTIONAL,	"v_region"},
	{"commune",			P_OPTIONAL,	"v_commune_optional"},
	{"address",			P_OPTIONAL,	"v_pay_address_optional"},
	{"contact",			P_OPTIONAL,	"v_pay_contact"},
	{"lob",				P_OPTIONAL,	"v_pay_lob_optional"},
	{"password",		P_OPTIONAL,	"v_accounts_passwd"},
	{"phone_hidden",	P_OPTIONAL,	"v_bool"},
	{"gender",			P_OPTIONAL,	"v_gender_account"},
	{"birthdate",		P_OPTIONAL,	"v_date_optional"},
	{"token",			P_OPTIONAL,	"v_token"},
	{NULL, 0}
};


static struct factory_data data = {
	"accounts",
	FACTORY_RES_SHORT_FORM
};

static int
bind_create_array_data(struct cmd *cs, struct bpapi *ba) {
    struct cmd_param *cp;

    TAILQ_FOREACH(cp, &cs->params, tq) {
		const char *name = cp->key;
		const char *value = cmd_getval(cs, name);

		if (!strcmp(name, "gender") || !strcmp(name, "birthdate")) {
			bpapi_insert(ba, "param_social_account_name", name);
			bpapi_insert(ba, "param_social_account_value", value);
		} else {
			bpapi_insert(ba, "param_account_name", name);
			bpapi_insert(ba, "param_account_value", value);
		}
	}
	return 0;
}

static const struct factory_action actions[] = {
	FA_FUNC(bind_create_array_data),
	FA_SQL("", "pgsql.master", "sql/call_edit_account_by_param.sql", FASQL_OUT),
	FA_FUNC(refresh_account),
	{0}
};

FACTORY_TRANS(edit_account_by_param, parameters, data, actions);
