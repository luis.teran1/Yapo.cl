#include "factory.h"
#include "validators.h"


static struct c_param parameters[] = {
	{"email", 		P_REQUIRED,		"v_email_basic"},
	{NULL, 0}
};

static struct factory_data data = {
	"accounts",
	FACTORY_NO_RES_CNTR
};

static int print_salt(struct cmd *cs, struct bpapi *ba) {
	const char *salt = bpapi_get_element(ba, "salt", 0);
	if (salt) {
		transaction_printf(cs->ts, "account_salt:%s\n", salt);
		return 0;
	} else {
		cs->status = "ERROR_ACCOUNT_NOT_FOUND";
		return -1;
	}
}

static const struct factory_action actions[] = {
	FA_SQL(NULL, "pgsql.master", "sql/get_account_salt.sql", 0),
	FA_FUNC(print_salt),
	FA_DONE()
};

FACTORY_TRANS(get_account_salt, parameters, data, actions);
