/**
 * This trans command gets the account data related to a social account
 * so we can use that later in the nextgen API to login a user
 */
#include "factory.h"


/*************** internal functions ***************/
static int account_social_login_join_accounts(struct cmd *cs, struct bpapi *ba);

static struct c_param parameters[] = {
	{"facebook_id;google_id;apple_id", P_XOR,      "v_integer;v_integer;v_integer"},
	{"hash_password",                  P_OPTIONAL, "v_passwd_sha1_not_required"},
	{"otp_hash_password",              P_OPTIONAL, "v_passwd_sha1_not_required"},
	{"is_persistent",                  0,          "v_bool"},
	{0}
};
static struct factory_data data = {
	"account_login",
	FACTORY_RES_SHORT_FORM | FACTORY_EMPTY_RESULT_OK
};

static int
bind_account_social_login(struct cmd *cs, struct bpapi *ba) {
	if (cmd_haskey(cs, "facebook_id")) {
		bpapi_insert(ba, "social_partner", "facebook_id");
		bpapi_insert(ba, "social_id", cmd_getval(cs, "facebook_id"));
	} else if (cmd_haskey(cs, "google_id")) {
		bpapi_insert(ba, "social_partner", "google_id");
		bpapi_insert(ba, "social_id", cmd_getval(cs, "google_id"));
	} else if (cmd_haskey(cs, "apple_id")) {
		bpapi_insert(ba, "social_partner", "apple_id");
		bpapi_insert(ba, "social_id", cmd_getval(cs, "apple_id"));
	}
	return 0;
}


static int
account_social_login_join_accounts(struct cmd *cs, struct bpapi *ba) {
	const char *db_account_id = bpapi_get_element(ba, "o_account_id", 0);
	const char *email = bpapi_get_element(ba, "o_email", 0);
	const char *db_account_status = bpapi_get_element(ba, "o_account_status", 0);

	// if the user has a social account created
	if (email) {
		transaction_printf(cs->ts, "account_id:%s\n", db_account_id);
		transaction_printf(cs->ts, "email:%s\n", email);
		transaction_printf(cs->ts, "account_status:%s\n", db_account_status);
	} else {
		transaction_logprintf(cs->ts, D_DEBUG, "LOGIN_FAILED\n");
		cs->status = "LOGIN_FAILED";
	}
	return 0;
}

static const struct factory_action actions[] = {
	FA_FUNC(bind_account_social_login),
	FA_SQL(NULL, "pgsql.master", "sql/get_socialaccount_info.sql", 0),
	FA_FUNC(account_social_login_join_accounts),
	FA_DONE()
};

FACTORY_TRANS(account_social_login, parameters, data, actions);
