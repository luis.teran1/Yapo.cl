/**
 * This trans command is used to edit an account.
 * The modifications we did was to add birthday and gender, that can be female or male
 */
#include "factory.h"
#include "utils/refresh_session.h"

static struct c_param parameters[] = {
	{"name",		P_REQUIRED,	"v_name"},
	{"rut",			0,			"v_pay_rut_optional"},
	{"email",		P_REQUIRED,	"v_email_basic"},
	{"phone",		P_REQUIRED,	"v_phone_strict"},
	{"source",		P_OPTIONAL,	"v_source_account"},
	{"is_company",	P_REQUIRED,	"v_accounts_is_company"},
	{"region",		P_REQUIRED,	"v_region"},
	{"commune",		0,			"v_commune_optional"},
	{"address",		0,			"v_pay_address_optional"},
	{"contact",		0,			"v_pay_contact"},
	{"lob",			0,			"v_pay_lob_optional"},
	{"password",	0,			"v_accounts_passwd"},
	{"phone_hidden",P_OPTIONAL,	"v_bool"},
	{"gender",		P_OPTIONAL,	"v_gender_account"},
	{"birthdate",	P_OPTIONAL,	"v_date_optional"},
	{"token",		P_OPTIONAL,	"v_token"},
	{"remote_addr",	P_OPTIONAL,	"v_ip"},
	{"remote_browser", P_OPTIONAL, "v_remote_browser"},
	{"business_name", P_OPTIONAL, "v_business_name"},
	{NULL, 0}
};


static struct factory_data data = {
	"accounts"
};

static const struct factory_action actions[] = {
	FA_SQL("", "pgsql.master", "sql/call_edit_account.sql", FASQL_OUT),
	FA_FUNC(refresh_account),
	{0}
};

FACTORY_TRANS(edit_account, parameters, data, actions);
