#include "factory.h"
#include "utils/refresh_session.h"

static struct c_param parameters[] = {
	{"accounts",  P_REQUIRED, "v_multiple_account_id"},
	{"pro_types",  P_REQUIRED, "v_multiple_pro_type"},
	{"pro_actions",  P_REQUIRED, "v_multiple_pro_action"},
	{"token",   P_REQUIRED, "v_token"},
	{"remote_addr", P_REQUIRED, "v_ip"},
	{"remote_browser",  P_REQUIRED, "v_remote_browser"},
	{NULL, 0}
};

static struct factory_data data = {
    "accounts",
    FACTORY_NO_RES_CNTR | FACTORY_RES_SHORT_FORM
};

static const struct factory_action actions[]= {
	FA_SQL("", "pgsql.master", "sql/call_accounts_change_pro_status.sql", 0),
	FA_FUNC(refresh_account),
	{0}
};

FACTORY_TRANS(accounts_change_pro_status, parameters, data, actions);
