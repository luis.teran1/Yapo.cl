/**
 * This trans command is used by the Nextgen API to generate a token,
 * that is later used to login the user
 */
#include "factory.h"
#include "sredisapi.h"
#include "fd_pool.h"
#include "utils/create_session_hash.h"
#include "utils/redis_trans_context.h"

extern struct bconf_node *bconf_root;

static struct c_param parameters[] = {
	{"account_id",	P_REQUIRED,	"v_user_id"},
	{NULL, 0}
};

static int
create_token(struct cmd *cs, struct bpapi *ba) {
	const char *email = bpapi_get_element(ba, "email", 0);
	char *token = create_session_hash(email);

	//token never expires
	struct fd_pool_conn *conn = redis_sock_conn(redis_session_pool.pool, "master");
	int create = redis_sock_hset(conn, token, "email", email);
	fd_pool_free_conn(conn);

	if (create != 1) {
		transaction_logprintf(cs->ts, D_ERROR, "create_session_on_redis failed!");
		free(token);
		return -1;
	}

	transaction_printf(cs->ts, "token:%s\n", token);
	free(token);
	return 0;
}

static struct factory_data data = {
	"accounts",
};

static const struct factory_action actions[] = {
	FA_SQL(NULL, "pgsql.master", "sql/call_get_account.sql", 0),
	FA_FUNC(create_token),
	{0}
};


FACTORY_TRANS(create_token, parameters, data, actions);
