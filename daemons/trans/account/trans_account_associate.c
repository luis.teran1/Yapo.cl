/**
 * The aim of this piece of "code" is to join a site account with a new social account.
 * A normal account is linked to a social account and only one (are monogamous)
 * Despite we are, in an explicit way, supporting google integration, we are liars.
 * If you want to add support for google, you must modify templates/sql/call_account_associate.sql.tmpl
 */
#include "factory.h"

static struct c_param parameters[] = {
	{"account_id",                     P_REQUIRED, "v_user_id"},
	{"facebook_id;google_id;apple_id", P_XOR, "v_integer;v_integer;v_integer"},
	{NULL, 0}
};
static struct factory_data data = {
	"accounts",
	FACTORY_RES_SHORT_FORM
};

static const struct factory_action actions[] = {
	FA_SQL("NULL", "pgsql.master", "sql/call_account_associate.sql", FASQL_OUT),
	{0}
};

FACTORY_TRANS(account_associate, parameters, data, actions);
