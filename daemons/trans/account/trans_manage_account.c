#include "factory.h"
#include "bpapi.h"
#include <bconf.h>
#include "utils/refresh_session.h"

static struct c_param parameters[] = {
	{"email;account_id", P_XOR, "v_email_basic;v_user_id"},
	{"status", P_OPTIONAL, "v_array:inactive,pending_confirmation,active"},
	{"token", P_OPTIONAL, "v_token"},
	{"remote_addr", P_OPTIONAL, "v_ip"},
	{"remote_browser",  P_OPTIONAL, "v_remote_browser"},
	{NULL, 0}
};


static struct factory_data data = {
	"accounts",
	FACTORY_RES_SHORT_FORM
};

static const struct factory_action actions[] = {
	FA_SQL("", "pgsql.master", "sql/call_manage_account.sql", FASQL_OUT),
	FA_FUNC(refresh_account),
	{0}
};

FACTORY_TRANS(manage_account, parameters, data, actions);
