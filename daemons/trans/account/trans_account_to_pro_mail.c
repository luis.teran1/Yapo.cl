#include "factory.h"

static struct c_param parameters[] = {
	{"email",	P_REQUIRED,	"v_email"},
	{"name",P_REQUIRED,	"v_string_isprint"},
    {NULL, 0}
};

static struct factory_data data = {
	"account_to_pro_mail"
};

static const struct factory_action actions[] = {
        FA_MAIL("mail/account_to_pro.mime"),
        {0}
};

FACTORY_TRANS(account_to_pro_mail, parameters, data, actions);
