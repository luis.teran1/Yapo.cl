#include "factory.h"
#include "bpapi.h"
#include <bconf.h>
#include "utils/refresh_session.h"

extern struct bconf_node *bconf_root;

static struct c_param parameters[] = {
	{"email", 		P_REQUIRED,		"v_email_basic"},
	{"salted_password",	P_REQUIRED,		"v_string_isprint"},
	{NULL, 0}
};

static struct factory_data data = {
	"accounts"
};

static const struct factory_action actions[] = {
	FA_SQL("", "pgsql.master", "sql/call_change_account_password.sql", FASQL_OUT),
	FA_FUNC(refresh_account),
	{0}
};

FACTORY_TRANS(reset_account_password, parameters, data, actions);
