/**
 * If you want to disaccociate a social account, we delete the social account and
 * social account params from our register (yes, we delete it)
 * You just need the account_id (from the site's account)
 */

#include "factory.h"

static struct c_param parameters[] = {
	{"account_id",	P_REQUIRED,	"v_user_id"},
	{"social_partner", P_OPTIONAL, "v_social_partner"},
	{NULL, 0}
};
static struct factory_data data = {
	"accounts",
	FACTORY_RES_SHORT_FORM | FACTORY_EMPTY_RESULT_OK
};

static const struct factory_action actions[] = {
	FA_SQL("NULL", "pgsql.master", "sql/call_account_disassociate.sql", FASQL_OUT),
	{0}
};

FACTORY_TRANS(account_disassociate, parameters, data, actions);
