#include "factory.h"
#include "util.h"
#include "validators.h"
#include "trans.h"
#include <stdio.h>
#include <string.h>
#include "command.h"
#include "sredisapi.h"
#include "fd_pool.h"
#include "sha_wrapper.h"
#include "utils/create_session_hash.h"
#include "utils/redis_trans_context.h"

static struct c_param parameters[] = {
	{"token", 		P_REQUIRED,		"v_string_isprint"},
	{NULL, 0}
};

static int get_email_from_token(struct cmd *cs, struct bpapi *ba) {
	const char *token = cmd_getval(cs, "token");
	struct fd_pool_conn *conn = redis_sock_conn(redis_session_pool.pool, "master");
	char *email = redis_sock_hget(conn, token, "email");
	fd_pool_free_conn(conn);
	if (!email) {
		transaction_printf(cs->ts, "status:TOKEN_INVALID\n");
		free(email);
		return 1;
	}
	bpapi_insert(ba, "email", email);
	free(email);
	return 0;
}

static struct factory_data data = {
	"accounts"
};

static const struct factory_action actions[] = {
	FA_FUNC(get_email_from_token),
	FA_TRANS("trans/call_get_account.txt", "", FATRANS_OUT),
	FA_DONE()
};

FACTORY_TRANS(account_get_logged_user, parameters, data, actions);
