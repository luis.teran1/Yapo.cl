#include "factory.h"

static struct c_param parameters[] = {
	{NULL, 0}
};

FACTORY_TRANSACTION(flush_stuck_mails, &config.pg_master, "sql/call_flush_stuck_mails.sql", NULL, parameters, FACTORY_NO_RES_CNTR);
