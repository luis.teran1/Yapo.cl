#include "factory.h"
#include "util.h"
#include "validators.h"
#include "trans.h"
#include <stdio.h>
#include <string.h>
#include "command.h"
#include "sredisapi.h"
#include "fd_pool.h"
#include "sha_wrapper.h"

static struct c_param parameters[] = {
	{"ad_id",     		P_REQUIRED, 	"v_ad_id"},
	{"mail_type",		0,		"v_array:bump_ok,mail_first_ad_bump"},
	{"action_id",		P_OPTIONAL, "v_id"},
	{NULL, 0}
};

static int apply_bump_if_active(struct cmd *cs, struct bpapi *ba){
	const char *ad_id= bpapi_get_element(ba, "ad_id", 0);
	const char *status = bpapi_get_element(ba, "status", 0);
	const char *mail_type = cmd_haskey(cs, "mail_type") ? cmd_getval(cs, "mail_type") : "mail_first_ad_bump";
	if (status && strcmp(status, "active") == 0) {
		transaction_internal_printf(TRANSACTION_INTERNAL_SYNC_CB, NULL,
			"cmd:bump_ad\n"
			"batch:1\n"
			"batch_bump:1\n"
			"do_not_send_mail:0\n"
			"mail_type:%s\n"
			"ad_id:%s\n"
			"commit:1\n"
			"end\n",
			mail_type,
			ad_id
		);
	}
	return 0;
}

static struct factory_data data = {
        "first_experience",
        FACTORY_EMPTY_RESULT_OK
};

static const struct factory_action actions[] = {
	FA_SQL(NULL, "pgsql.slave", "sql/check_ad_status.sql", 0),
	FA_FUNC(apply_bump_if_active),
	FA_DONE()
};

FACTORY_TRANS(first_experience_bump, parameters, data, actions);
