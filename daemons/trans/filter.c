
#include <stdlib.h>

#include "trans.h"
#include "unicode.h"

#include <bpapi.h>
#include <ctemplates.h>

#include "filter.h"
#include "sql_worker.h"
#include "utils/filterd.h"
#include "yajl/yajl_tree.h"

static const char *
filter_data_update_count_cb(void *v, struct sql_worker *worker, const char *errstr) {
	struct filter_data *data = v;

	if (errstr) {
		data->cb(data, FILTER_ERROR, 0, NULL, NULL, NULL, NULL, NULL, 0, errstr);
		return NULL;
	}

	while (worker->next_row(worker)) {
		// Nothing to do here really, do not really care about the return
	}

	data->cb(data, FILTER_DONE, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL);
	return NULL;
}

static void
update_filter_counts(struct filter_data *data, int match_rule_arr_count, int *match_rule_arr) {
	char rule_id_str[32];
	struct bpapi bap;
	BPAPI_INIT(&bap, NULL, pgsql);

	for (int i = 0; i < match_rule_arr_count; i++) {
		snprintf(rule_id_str, 32, "%d", match_rule_arr[i]);
		bpapi_insert(&bap, "rule_id", rule_id_str);
	}

	sql_worker_template_query(&config.pg_master, "sql/filters/call_update_filter_counts.sql", &bap,
			filter_data_update_count_cb, data, "filter");
	bpapi_output_free(&bap);
	bpapi_simplevars_free(&bap);
	bpapi_vtree_free(&bap);
}

static void
check_duplicated(int *match_rule_arr_count, int rule_id, int *match_rule_arr) {
	int dup = 0;
	for (int j = 0; j < *match_rule_arr_count; j++) {
		if (match_rule_arr[j] == rule_id) {
			dup = 1;
			break;
		}
	}
	if (!dup) {
		match_rule_arr[*match_rule_arr_count] = rule_id;
		*match_rule_arr_count = *match_rule_arr_count + 1;
	}
}

static const char *
filter_data_cb(void *v, struct sql_worker *worker, const char *errstr) {
	struct filter_data *data = v;
	int rule_id;
	const char *rule_name;
	const char *action;
	const char *target;
	const char *matched_column;
	const char *matched_value;
	int combi_match;
	int *match_rule_arr = NULL;
	int match_rule_arr_count = 0;
	int match_rule_arr_size = 0;

	if (errstr) {
		data->cb(data, FILTER_ERROR, 0, NULL, NULL, NULL, NULL, NULL, 0, errstr);
		return NULL;
	}

	match_rule_arr_size = worker->rows(worker);
	if (match_rule_arr_size) {
		match_rule_arr = alloca(sizeof(int) * match_rule_arr_size);
	}

	while (worker->next_row(worker)) {
		rule_id = atoi(worker->get_value_byname(worker, "o_rule_id"));
		rule_name = worker->get_value_byname(worker, "o_rule_name");
		action = worker->get_value_byname(worker, "o_action");
		target = worker->get_value_byname(worker, "o_target");
		matched_value = worker->get_value_byname(worker, "o_matched_value");
		matched_column = worker->get_value_byname(worker, "o_matched_column");
		combi_match = worker->get_value_byname(worker, "o_combi_match")[0] == 't';
		data->cb(data, FILTER_MATCH, rule_id, rule_name, action, target, matched_column, matched_value, combi_match, NULL);
		// Check to see if we have this rule id as matched already, if not add to set
		check_duplicated(&match_rule_arr_count, rule_id, match_rule_arr);
	}

	if (match_rule_arr_count) {
		update_filter_counts(data, match_rule_arr_count, match_rule_arr);
	} else {
		data->cb(data, FILTER_DONE, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL);
	}

	return NULL;
}

static void
filterd_parse_response(struct filter_data *data, const char *resp) {
	char errbuf[1024];
	int match_rule_arr_count = 0;
	int *match_rule_arr = NULL;
	char *matched_value_latin1 = NULL;
	yajl_val node = yajl_tree_parse((const char *) resp, errbuf, sizeof(errbuf));
	if (node == NULL) {
		DPRINTF(D_ERROR, ("filter_data: error: %s",errbuf));
	} else {
		int rule_id = 0;
		const char *rule_id_s;
		const char *rule_name;
		const char *action;
		const char *target;
		const char *matched_column;
		const char *matched_value;
		int combi_match;
		unsigned int node_len = YAJL_GET_ARRAY(node)->len;
		yajl_val *values = YAJL_GET_ARRAY(node)->values;

		if (node_len)
			match_rule_arr = alloca(sizeof(int) * node_len);

		for (unsigned int i = 0; i < node_len; i++) {
			yajl_val root = values[i];
			rule_id_s = YAJL_GET_STRING(yajl_tree_get(root, (const char *[]){"o_rule_id", NULL}, yajl_t_any));
			rule_id = rule_id_s == NULL? 0 : atoi(rule_id_s);
			rule_name = YAJL_GET_STRING(yajl_tree_get(root, (const char *[]){"o_rule_name", NULL}, yajl_t_any));
			action = YAJL_GET_STRING(yajl_tree_get(root, (const char *[]){"o_action", NULL}, yajl_t_any));
			target = YAJL_GET_STRING(yajl_tree_get(root, (const char *[]){"o_target", NULL}, yajl_t_any));
			matched_column = YAJL_GET_STRING(yajl_tree_get(root, (const char *[]){"o_matched_column", NULL}, yajl_t_any));
			matched_value = YAJL_GET_STRING(yajl_tree_get(root, (const char *[]){"o_matched_value", NULL}, yajl_t_any));
			combi_match = YAJL_GET_STRING(yajl_tree_get(root, (const char *[]){"o_combi_match", NULL}, yajl_t_any))[0] == 't';
			matched_value_latin1 = utf8_to_latin1(matched_value);
			data->cb(data, FILTER_MATCH, rule_id, rule_name, action, target, matched_column, matched_value_latin1, combi_match, NULL);
			// Check to see if we have this rule id as matched already, if not add to set
			check_duplicated(&match_rule_arr_count, rule_id, match_rule_arr);
		}
	}
	if (match_rule_arr_count) {
		update_filter_counts(data, match_rule_arr_count, match_rule_arr);
	} else {
		data->cb(data, FILTER_DONE, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL);
	}
	free(matched_value_latin1);
	yajl_tree_free(node);
}

void
filter_data(struct filter_data *data, const char *appl) {
	struct bpapi ba;
	struct filter_elem *el;
	char *resp_utf8 = NULL;
	int tlen = 0;
	BPAPI_INIT(&ba, NULL, pgsql);

	// Set data in correct format & use filterd
	struct buf_string bs = { 0 };
	bscat(&bs, "{\"application\":\"%s\"",appl);
	bpapi_insert(&ba, "application", appl);
	while ((el = SLIST_FIRST(&data->filter_params))) {
		bscat(&bs, ",\"%s\":\"%s\"",el->key, el->value);
		bpapi_insert(&ba, "field_name", el->key);
		bpapi_insert(&ba, "field_value", el->value);
		SLIST_REMOVE_HEAD(&data->filter_params, slist);
		free(el);
	}
	bscat(&bs, "}");
	bpapi_insert(&ba, "json", bs.buf);
	// Uses filterd by default
	if(!filterd_filter(NULL, &ba, "json", "json_result")) {
		const char *resp = bpapi_get_element(&ba, "json_result", 0);
		DPRINTF(D_DEBUG, ("filter_data: filterd answered with %s", resp));
		resp_utf8 = latin1_to_utf8(resp, -1, &tlen);
		DPRINTF(D_DEBUG, ("filter_data: sending in utf-8 %s", resp_utf8));
		filterd_parse_response(data, resp_utf8);
	} else {
		// Old filter
		DPRINTF(D_DEBUG, ("filter_data: using fallback filter"));
		sql_worker_template_query(&config.pg_slave, "sql/filters/call_filter_data.sql", &ba,
					  filter_data_cb, data, "filter");
	}
	bpapi_output_free(&ba);
	bpapi_simplevars_free(&ba);
	bpapi_vtree_free(&ba);
	free(resp_utf8);
	free(bs.buf);
}

struct filter_data*
filter_init(struct filter_data *fdata) {
	if (!fdata)
		fdata = zmalloc(sizeof(*fdata));

	SLIST_INIT(&fdata->filter_params);

	return fdata;
}

void
filter_add_element(struct filter_data *data, const char *key, const char *value) {
	struct filter_elem *el = zmalloc(sizeof(*el) + strlen(key) + 1 + strlen(value) + 1);

	el->key = strcpy((char*)(el + 1), key);
	el->value = strcpy((char*)(el + 1) + strlen(key) + 1, value);

	SLIST_INSERT_HEAD(&data->filter_params, el, slist);
}
