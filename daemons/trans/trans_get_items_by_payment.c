#include "factory.h" 

static struct c_param parameters[] = {
	{"payment_group_id",	P_REQUIRED,	"v_integer"},
	{NULL, 0}
};

static struct factory_data data = {
        "get_items_by_payment",
        FACTORY_NO_RES_CNTR
};

static const struct factory_action actions[] = {
        FA_SQL(NULL, "pgsql.master", "sql/get_items_by_payment.sql", FATRANS_OUT),
        {0}
};

// FACTORY_TRANSACTION(get_items_by_payment, &config.pg_slave, "sql/get_items_by_payment.sql", NULL, parameters, NULL);
FACTORY_TRANS(get_items_by_payment, parameters, data, actions);
