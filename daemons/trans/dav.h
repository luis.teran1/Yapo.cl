#ifndef ELOJ_DAV_H
#define ELOJ_DAV_H

#include "event.h"

struct davdb {
	int pending;
	struct cmd *cs;
	const char *command;
	const char *filename;
	char *destfilename;
	char **new_filename;
	char *errstr;
	const char *date;
	const char *directory;
	const char *application;

	unsigned long long image_id;
	int image_size;
	const char *image;
	int thumb_size;
	const char *thumb;

	struct event timeout_event;  /* Used in regress mode */

	int result;
	void *data;
	void (*cb)(struct davdb*);
};

struct sql_worker;

const char *imagecmd(void *v, struct sql_worker *worker, const char *errstr);
void davdb_execute(struct davdb *ds);


#endif

