# RabbitMQ configuration file, flavor: test

# RabbitMQ - MSG-CENTER
rabbitmq.virtual_host.%USER%.host=localhost
rabbitmq.virtual_host.%USER%.port=%REGRESS_RABBITMQ_PORT%
rabbitmq.virtual_host.%USER%.connections=10
rabbitmq.virtual_host.%USER%.user=baduser
rabbitmq.virtual_host.%USER%.password=badpasswd
rabbitmq.virtual_host.%USER%.name=%USER%
rabbitmq.virtual_host.%USER%.path=/
rabbitmq.virtual_host.%USER%.exchange.0.name=msg_center
rabbitmq.virtual_host.%USER%.exchange.0.type=topic
rabbitmq.virtual_host.%USER%.exchange.0.passive=0
rabbitmq.virtual_host.%USER%.exchange.0.durable=1
rabbitmq.virtual_host.%USER%.queue.0.name=msg_center
rabbitmq.virtual_host.%USER%.queue.0.passive=0
rabbitmq.virtual_host.%USER%.queue.0.durable=1
rabbitmq.virtual_host.%USER%.queue.0.exclusive=0
rabbitmq.virtual_host.%USER%.queue.0.auto_delete=0
rabbitmq.virtual_host.%USER%.binding.0.exchange=msg_center
rabbitmq.virtual_host.%USER%.binding.0.queue=msg_center
rabbitmq.virtual_host.%USER%.binding.0.key=msg_center

