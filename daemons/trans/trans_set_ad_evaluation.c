#include "factory.h"
#include "filter.h"

static struct c_param parameters[] = {
	{"ad_id",       P_REQUIRED, "v_ad_id"},
	{"action_id",   P_REQUIRED, "v_action_id"},
	{"evaluation",  P_REQUIRED, "v_array:approved,refused,no_decision"},
	{"reason",      P_OPTIONAL, "v_string_isprint"},
	{"remote_addr", P_OPTIONAL, "v_ip"},
	{NULL, 0}
};

static struct factory_data data = {
        "controlpanel",
        FACTORY_NO_RES_CNTR | FACTORY_RES_SHORT_FORM
};


static void
ad_evaluation_filter_cb(struct filter_data *data,
                        int status,
                        int rule_id,
                        const char *rule_name,
                        const char *action,
                        const char *target,
                        const char *matched_column,
                        const char *matched_value,
                        int combi_match,
                        const char *errstr) {
	struct bpapi *ba = data->data;
	switch (status) {
		case FILTER_ERROR:
		case FILTER_DONE:
			free(data);
			break;
		case FILTER_MATCH:
			bpapi_insert(ba, "target_queue", target);
			break;
	}
}

static int
filter_ad_evaluation(struct cmd *cs, struct bpapi *ba) {
	struct filter_data *fdata = filter_init(NULL);
	filter_add_element(fdata, "category", bpapi_get_element(ba, "category", 0));
	filter_add_element(fdata, "external_evaluation", cmd_getval(cs, "evaluation"));
	filter_add_element(fdata, "ad_param", bpapi_get_element(ba, "ad_param", 0));
	filter_add_element(fdata, "action_param", bpapi_get_element(ba, "action_param", 0));
	filter_add_element(fdata, "external_evaluation_reason", cmd_haskey(cs, "reason") ? cmd_getval(cs, "reason") : "");
	fdata->cb = ad_evaluation_filter_cb;
	fdata->data = ba;
	filter_data(fdata, "ad_evaluation");
	return 0;
}

static const struct factory_action queue_prio  = FA_SQL("", "pgsql.master", "sql/call_queue_prio.sql", 0);

static const struct factory_action *
prioritize_queues(struct cmd *cs, struct bpapi *ba) {
	const char *t_queue = bpapi_get_element(ba, "target_queue", 0);
	if (t_queue) {
		return &queue_prio;
	}
	return NULL;
}

static const struct factory_action actions[]= {
	FA_SQL("", "pgsql.master", "sql/get_ad_category.sql", FASQL_OUT),
	FA_SQL("", "pgsql.master", "sql/get_ad_params.sql", FASQL_OUT),
	FA_SQL("", "pgsql.master", "sql/get_action_params.sql", FASQL_OUT),
	FA_FUNC(filter_ad_evaluation),
	FA_COND(prioritize_queues),
	FA_SQL("", "pgsql.master", "sql/call_set_ad_evaluation.sql", 0),
	FA_DONE()
};

FACTORY_TRANS(set_ad_evaluation, parameters, data, actions);
