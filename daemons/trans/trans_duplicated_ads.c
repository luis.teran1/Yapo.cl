#include "factory.h"
#include "bpapi.h"
#include <bconf.h>
#include "similar_text.h"

extern struct bconf_node *bconf_root;

static int find_duplicates(struct cmd *, struct bpapi *);

static struct c_param parameters[] = {
	{"uid;email",   P_OR,       "v_uid;v_email_basic"},
	{"ad_subject",  P_REQUIRED, "v_subject"},
	{"ad_body",     P_REQUIRED, "v_body"},
	{"ad_category", P_REQUIRED, "v_new_category"},
	{"ad_price",    P_OPTIONAL, "v_currency_check"},
	{"ad_currency", P_OPTIONAL, "v_currency"},
	{"ad_status",   P_REQUIRED, "v_string_all"},
	{"ad_id",       P_OPTIONAL, "v_ad_id"},
	{"type",        P_OPTIONAL, "v_string_isprint"},
	{"region",      P_OPTIONAL, "v_region"},
	{NULL, 0}
};

static struct factory_data data = {
	"factory_test",
	FACTORY_RES_CNTR | FACTORY_EMPTY_RESULT_OK
};

static const struct factory_action actions[] = {
	FA_SQL("", "pgsql.slave", "sql/call_get_ads_by_uid.sql", 0),
	FA_OUT("trans/normalize_prices.txt"),
	FA_FUNC(find_duplicates),
	{0}
};

static int is_duplicate(const char *adsubject, const char *adbody, const int adcategory, const int adprice, const char *suspect_subject, const char *suspect_body, const int suspect_category, const int suspect_price, struct cmd *cs) {

	struct bconf_node *node = bconf_vget(bconf_root, "*", "*", "delete2bump", cmd_getval(cs, "ad_status"), "duplicate", "percent", NULL);
	int mindupperc = bconf_get_int(node, "min");
	double duppercsubj = bconf_get_int(node, "subject");
	double duppercbody = bconf_get_int(node, "body");
	double dupperccat = bconf_get_int(node, "category");
	double duppercprice = bconf_get_int(node, "price");

	int subjperc = similar_text(adsubject, suspect_subject, 0);
	int bodyperc = similar_text(adbody, suspect_body, 0);
	int catperc = (adcategory == suspect_category) * 100;
	int priceperc = 0;
	if (adprice != 0 && suspect_price != 0)
		priceperc = (1 - (float)abs(adprice - suspect_price)/(adprice + suspect_price))*100;

	int totperc = subjperc * (duppercsubj / 100) + bodyperc * (duppercbody / 100) + catperc * (dupperccat / 100) + priceperc * (duppercprice / 100);

	transaction_logprintf(cs->ts, D_INFO, "SIMILAR: duppercsubj  : %f - duppercbody  : %f - dupperccat : %f - duppercprice : %f", duppercsubj , duppercbody, dupperccat, duppercprice);
	transaction_logprintf(cs->ts, D_INFO, "SIMILAR: adsubject : %s - adbody : %s - adcategory : %d - adprice: : %d", adsubject, adbody, adcategory, adprice);
	transaction_logprintf(cs->ts, D_INFO, "SIMILAR: suspect_subject : %s - suspect_body : %s - suspect_category : %d - suspect_price : %d", suspect_subject, suspect_body, suspect_category, suspect_price);
	transaction_logprintf(cs->ts, D_INFO, "SIMILAR: mindupperc : %d - subjperj %d - boduperc: %d - catperc: %d - priceperc: %d - totperc : %d", mindupperc, subjperc, bodyperc, catperc, priceperc, totperc);

	if (totperc  == 100) {
		transaction_logprintf(cs->ts, D_INFO, "SIMILAR: ads are equal");
		return 1;
	} else {
		if (totperc >= mindupperc) {
			transaction_logprintf(cs->ts, D_INFO, "SIMILAR: total percentage is higher than the minimum duplicate percentage");
			return 1;
		}else{
			transaction_logprintf(cs->ts, D_INFO, "SIMILAR: total percentage is lower than the minimum duplicate percentage");
			return 0;
		}
	}
}

static int find_duplicates(struct cmd *cs, struct bpapi *ba) {
	transaction_logprintf(cs->ts, D_INFO, "SIMILAR: Entering ");
	int length = bpapi_length(ba, "o_ad_id");
	transaction_logprintf(cs->ts, D_INFO, "SIMILAR: The count of similar ads is: %d", length);
	const char *subject = cmd_getval(cs, "ad_subject");
	const char *body = cmd_getval(cs, "ad_body");
	const int category = atoi(cmd_getval(cs, "ad_category"));
	const int da_price = atoi(bpapi_get_element(ba, "da_price", 0));
	int i, has_duplicates = 0;

	for (i = 0; i < length && !has_duplicates; i++) {
		const char *asubject = bpapi_get_element(ba, "o_subject", i);
		const char *abody = bpapi_get_element(ba, "o_body", i);
		const char *aad_id = bpapi_get_element(ba, "o_ad_id", i);
		const int acategory = atoi(bpapi_get_element(ba, "o_category", i));
		int norm_price = 0;
		if(bpapi_get_element(ba, "norm_price",i) && strcmp(bpapi_get_element(ba, "norm_price",i),""))
				norm_price = atoi(bpapi_get_element(ba, "norm_price", i));
		int aprice = 0;
		if(bpapi_get_element(ba, "o_price",i) && strcmp(bpapi_get_element(ba, "o_price",i),""))
				aprice = atoi(bpapi_get_element(ba, "o_price", i));
		const char *acurrency = bpapi_get_element(ba, "o_currency", i);
		const char* aimage = bpapi_get_element(ba, "o_image", i);
		const char* aemail = bpapi_get_element(ba, "o_email", i);
		const char* aaccount_id = bpapi_get_element(ba, "o_account_id", i);
		const char* arequire_password = bpapi_get_element(ba, "o_require_password", i);

		has_duplicates = is_duplicate(subject, body, category, da_price, asubject, abody, acategory, norm_price, cs);
		if (has_duplicates) {
			transaction_printf(cs->ts, "ad_id:%d\n", atoi(aad_id));
			transaction_printf(cs->ts, "subject:%s\n", asubject);
			transaction_printf(cs->ts, "body:%s\n", abody);
			transaction_printf(cs->ts, "category:%d\n", acategory);
			transaction_printf(cs->ts, "price:%d\n", aprice);
			transaction_printf(cs->ts, "currency:%s\n", acurrency);
			transaction_printf(cs->ts, "image:%s\n", aimage);
			transaction_printf(cs->ts, "email:%s\n", aemail);
			transaction_printf(cs->ts, "account_id:%s\n", aaccount_id);
			transaction_printf(cs->ts, "require_password:%s\n", arequire_password);

			const char *astatus = bpapi_get_element(ba, "o_status", i);
			transaction_printf(cs->ts, "ad_status:%s\n", astatus);
			const char *alist_id = bpapi_get_element(ba, "o_list_id", i);
			transaction_printf(cs->ts, "list_id:%d\n", atoi(alist_id));
			const char *alist_time = bpapi_get_element(ba, "o_list_time", i);
			transaction_printf(cs->ts, "list_time:%s\n", alist_time);
			const char *apack_status = bpapi_get_element(ba, "o_pack_status", i);
			transaction_printf(cs->ts, "pack_status:%s\n", apack_status);
		}
	}

	transaction_printf(cs->ts, "has_duplicates:%d\n", has_duplicates);
	return 0;
}

FACTORY_TRANS(duplicated_ads, parameters, data, actions);
