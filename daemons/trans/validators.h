#ifndef VALIDATORS_H
#define VALIDATORS_H

#include "command.h"
#include "linker_set.h"
#include "dav.h"

#define ASCALPHA "abcdefghijklmnopqrstuvwxyz"
#define ASCALPHA_URL "abcdefghijklmn�opqrstuvwxyz"
#define ASCALNUM ASCALPHA "0123456789"
#define ASCALNUM_URL ASCALPHA_URL "0123456789"
#define BASE64 "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/="


struct trans_key_lookup_data {
	struct cmd_param *param;
	const char *has_param;
	int optional;
	int result;
};
const char*
trans_key_lookup(const char *setting, const char *key, void *cbdata);

/*
 * Synchronous database validation
 */
void
validate_on_db(struct cmd_param *param, const char *arg,
				const char *key, const char *value,
				const char *config, const char *sqltmpl,
				void (*populate_bpapi)(struct cmd_param *param, const char *arg, struct bpapi *ba),
				void (*on_error)(struct cmd_param *param, struct sql_worker *worker, const char *errstr),
				void (*check_for_errors)(struct cmd_param *param, struct sql_worker *worker, const char *arg));
/*
 * Type check
 */
int v_type_invalid(struct cmd_param *param, const char *arg);

int v_region_cities(struct cmd_param *param, const char *arg);

/*
 * Mileage
 */
int v_cat_mileage(struct cmd_param *param, const char *arg);

/*
 * Regdate
 */
int v_cat_regdate(struct cmd_param *param, const char *arg);

/*
 * Extra ad options
 */
int v_category_extra_param(struct cmd_param *param, const char *arg);

/*
 * Category
 */
int v_category_level(struct cmd_param *param, const char *arg);

/*
 * User blocked
 */
int v_email_blocked(struct cmd_param *param, const char *arg);
int v_phone_blocked(struct cmd_param *param, const char *arg);

/*
 * Company
 */
int v_check_store(struct cmd_param *param, const char *arg);
int v_company_ad_subscription(struct cmd_param *param, const char *arg);

/*
 * Ad subject
 */
int v_subject_badword(struct cmd_param *param, const char *arg);
int v_subject_regdate(struct cmd_param *param, const char *arg);

/*
 * Ad body
 */
int v_body_original(struct cmd_param *param, const char *arg);

int v_check_infopage(struct cmd_param *param, const char *arg);
int v_check_infopage_title(struct cmd_param *param, const char *arg);

/*
 * Image handling
 */
void imagecmd_cb(struct davdb *ds);
int v_put_imagefile(struct cmd_param *p, const char *arg);

/*
 * Sell and subscription validation
 */
int v_cat_car(struct cmd_param *param, const char *arg);

int v_type_sell_subscription(struct cmd_param *param, const char *arg);

/*
 * Modifiy a price value according to its currency 
 */
int format_price(struct cmd *cs, struct bpapi *);

#endif
