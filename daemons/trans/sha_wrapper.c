#include "sha_wrapper.h" 

char *
sha1(const char *plain_text) {

	unsigned char * hash = zmalloc(21 * sizeof(char));
	memset(hash, 0, 21 * sizeof(char));
	SHA1_CTX sha1_context;
	SHA1Init(&sha1_context);
	SHA1Update( &sha1_context, (unsigned char*)plain_text, strlen( plain_text ) );
	SHA1Final( hash, &sha1_context );

	char * hexhash = zmalloc(41 * sizeof(char));
	memset(hexhash, 0, 41 * sizeof(char));
	int i;
	for(i = 0; i < 20; i++){
		char tmp[3];
		sprintf(tmp, "%02x", hash[i]);
		strcat(hexhash, tmp);
	}
	free(hash);

	return (char*)hexhash;
}

char *sha256(const char *string) {
	char *outputBuffer = NULL;
	int buflen = 0;
	int bufpos = 0;
	unsigned char hash[SHA256_DIGEST_LENGTH];
	SHA256_CTX sha256;
	SHA256_Init(&sha256);
	SHA256_Update(&sha256, string, strlen(string));
	SHA256_Final(hash, &sha256);
	for (int i = 0; i < SHA256_DIGEST_LENGTH; i++) {
	 	bufcat(&outputBuffer, &buflen, &bufpos, "%02x", hash[i]);
	}
	return outputBuffer;
}
