#include "factory.h"
#include "bpapi.h"

extern struct bconf_node *bconf_root;


static struct c_param parameters[] = {
	{NULL, 0}
};


static int execute_autobump(struct cmd *cs, struct bpapi *ba) {
	int len = bpapi_length(ba, "autobump_id");
	int i;
	for (i = 0; i < len; i++) {
		const char *status = NULL;
		char *list_time = (char *)bpapi_get_element(ba, "current_datetime", i);
		int update_list_time = 0;
		if (!strcmp(bpapi_get_element(ba, "autobump_skipped", i), "1")) {
			status = "skipped";
		} else if (!strcmp(bpapi_get_element(ba, "autobump_cancel", i), "1")) {
			status = "cancel";
		} else {
			char *res_bump_ad = transaction_internal_output_printf(
				"cmd:bump_ad\n"
				"ad_id:%s\n"
				"batch:1\n"
				"transition_type:autobump\n"
				"update_ad_status:%s\n"
				"commit:1\n"
				"end\n",
				bpapi_get_element(ba, "ad_id", i),
				bpapi_get_element(ba, "update_ad_status", i)
			);
			// Search status and list_time in trans response
			char *saveptr;
			char *token = strtok_r(res_bump_ad, "\n", &saveptr);
			while (token) {
				// Save status
				if (strstr(token, "status:")) {
					if (strstr(token + 7, "TRANS_OK")) {
						status = "success";
					} else {
						status = "error";
					}
				} else if (strstr(token, "list_time:")) {
					list_time = xstrdup(token + 10);
					update_list_time = 1;
				}
				token = strtok_r(NULL, "\n", &saveptr);
			}
			free(res_bump_ad);
		}
		bpapi_insert(ba, "list_time", list_time ?: "");
		bpapi_insert(ba, "status", status);
		transaction_logprintf(cs->ts, D_INFO, "Flush autobump autobump_id:%s ad_id:%s status:%s list_time:%s",
			bpapi_get_element(ba, "autobump_id", i),
			bpapi_get_element(ba, "ad_id", i),
			status,
			list_time ?: ""
		);
		if (update_list_time) {
			free(list_time);
		}
	}
	return 0;
}

static struct factory_data data = {
	"autobump",
	FACTORY_NO_RES_CNTR | FACTORY_EMPTY_RESULT_OK
};

static const struct factory_action actions[]= {
	FA_SQL("", "pgsql.master", "sql/load_pending_autobump.sql", 0),
	FA_FUNC(execute_autobump),
	FA_SQL("", "pgsql.master", "sql/call_update_autobump.sql", FASQL_OUT),
	{0}
};

FACTORY_TRANS(flush_autobump, parameters, data, actions);
