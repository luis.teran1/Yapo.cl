#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <time.h>

#include "trans.h"
#include "trans_at.h"
#include "trans_mail.h"
#include "util.h"
#include <ctemplates.h>
#include "validators.h"
#include <bconf.h>
#include <event.h>
#include "db.h"
#include "utils/send_backend_event.h"

extern struct bconf_node *bconf_root;

typedef enum {

	ADMAIL_DONE,
	ADMAIL_ERROR,
	ADMAIL_INIT ,
	ADMAIL_LOAD_DATA,
	ADMAIL_POPULATE,
	ADMAIL_GET_HOLD_MAIL_PARAMS,
	ADMAIL_RECORD_PAYMAIL,
	ADMAIL_RECORD_PAYMAIL_LOAD_DATA,
	ADMAIL_RECORD_HALF_TIME,
	ADMAIL_RECORD_GALLERY_EXPIRED,
	ADMAIL_SEND_MAIL
} admail_state_t;

struct hold_mail_param {
        TAILQ_ENTRY(hold_mail_param) tq;
        char *key;
        char *value;
};


struct admail_state {
	admail_state_t state;
	int mail_sent;
	int pending_error;
        struct mail_data *md;
	TAILQ_HEAD(,hold_mail_param) bp;
        const char *template;
	struct cmd *cs;
	struct sql_worker *sw;
	struct action *action;
	const char *lang;
    char *spidered_list_id;
};

static struct c_param refuse_params[] = {
	{"reason", P_REQUIRED, "v_reason"},
	{"refusal_text",	0,	"v_none"},
	{NULL}
};
static struct c_param accept_with_changes_params[] = {
	{"reason", P_REQUIRED, "v_reason"},
	{"accept_text",	0,	"v_none"},
	{NULL}
};
static struct c_param scarface_params[] = {
	{"reporter_email",	P_REQUIRED, 	"v_none"},
	{"report_id",		0, 	"v_none"},
	{"resolution",		0, 	"v_none"},
	{"list_id",		P_REQUIRED,	"v_none"},
	{"uid",			0, 	"v_none"},
	{"admin_message",	0, 	"v_none"},
	{"report_lang", 	P_REQUIRED, 	"v_none"},
	{"abuse_type",		P_REQUIRED, 	"v_none"},
	{NULL}
};

static struct c_param scarface_reject_custom_params[] = {
	{"reporter_email",	P_REQUIRED, 	"v_none"},
	{"report_id",		0, 	"v_none"},
	{"resolution",		0, 	"v_none"},
	{"list_id",		P_REQUIRED,	"v_none"},
	{"uid",			0, 	"v_none"},
	{"admin_message",	0, 	"v_none"},
	{"report_lang", 	P_REQUIRED, 	"v_none"},
	{"abuse_type",		P_REQUIRED, 	"v_none"},
	{"notify_type",		P_REQUIRED,	"v_integer"},
	{NULL}
};

static struct c_param scarface_warn_params[] = {
	{"ad_id",		P_REQUIRED,	"v_none"},
	{"list_id",		P_REQUIRED,	"v_none"},
	{"email",		P_REQUIRED,	"v_none"},
	{NULL}
};

static struct c_param scarface_notify_owner[] = {
	{"list_id",		P_REQUIRED,	"v_none"},
	{"subject",		P_REQUIRED,	"v_none"},
	{"email",		P_REQUIRED,	"v_none"},
	{"notify_type",		P_REQUIRED,	"v_integer"},
	{NULL}
};

static struct c_param bump_ok_params[] = {
	{"doc_type",		P_REQUIRED,	"v_string_isprint"},
	{"remaining_bumps",	0,	"v_integer"},
	{"transition_type",	0,	"v_array:bump,autobump"},
	{NULL}
};

static struct c_param weekly_bump_ok_params[] = {
	{"remaining_bumps",	0,	"v_integer"},
	{"doc_type",		0,	"v_string_isprint"},
	{"transition_type",	0,	"v_array:bump,autobump"},
	{NULL}
};

static struct c_param mail_first_ad_bump_params[] = {
	{"remaining_bumps",	0,	"v_integer"},
	{"doc_type",		0,	"v_string_isprint"},
	{"transition_type",	0,	"v_array:bump,autobump"},
	{NULL}
};

static int
v_mail_type_func(struct cmd_param *param, const char *arg) {
	struct c_param *extra_params = NULL;

	if (strcmp(param->value, "refuse") == 0 ||
		strcmp(param->value, "edit_refuse") == 0 ||
		strcmp(param->value, "edit_refused_refuse") == 0 ){
		extra_params = refuse_params;
	}

	if (strcmp(param->value, "accept_with_changes") == 0)
		extra_params = accept_with_changes_params;

	if ( strcmp(param->value, "scarface_report") == 0 || strcmp(param->value, "scarface_askinfo") == 0 )
		extra_params = scarface_params;

	if ( strcmp(param->value, "scarface_warn") == 0 )
		extra_params = scarface_warn_params;

	if ( strcmp(param->value, "scarface_owner") == 0 )
		extra_params = scarface_notify_owner;

	if ( strcmp(param->value, "scarface_reject_custom") == 0 )
		extra_params = scarface_reject_custom_params;

	if ( strcmp(param->value, "bump_ok") == 0 )
		extra_params = bump_ok_params;

	if ( strcmp(param->value, "weekly_bump_ok" ) == 0 )
		extra_params = weekly_bump_ok_params;

	if ( strcmp(param->value, "mail_first_ad_bump" ) == 0 )
		extra_params = mail_first_ad_bump_params;

	if (extra_params) {
		while (extra_params->name)
			cmd_extra_param(param->cs, extra_params++);
	}
	return 0;
}


struct validator v_mail_type[] = {
	VALIDATOR_REGEX("^pay(info|_reminder)|accept|accept_with_changes|refuse|deleted|deleted_monthly|undo_deleted|card_receipt|voucher_used|half_time|gallery_expired|free|scarface_(report|askinfo|warn|owner|reject_custom)|category_changed|rejected_wrong_category|bump_ok|weekly_bump_ok|mail_first_ad_bump|mail_advertisement_bump|gallery_ok|deactivated$", REG_EXTENDED|REG_NOSUB, "ERROR_MAIL_TYPE"),
	VALIDATOR_FUNC(v_mail_type_func),
	{0}
};

ADD_VALIDATOR(v_mail_type);


static struct c_param parameters[] = {
	{"ad_id",	0,	"v_ad_id"},
	{"mail_type",	P_REQUIRED,	"v_mail_type"},
	{"action_id",	0,	"v_string_isprint"},
	{"name",	0,	"v_string_isprint"},
	{"from",	0,	"v_string_isprint"},
	{"to",		0,	"v_string_isprint"},
	{"body",	0,	"v_string_isprint"},
	{"remote_addr", 	0,  	"v_ip"},
	{"remote_browser", 	0,  	"v_remote_browser"},
	{"pay_type",    0,      "v_pay_type_not_required"},
	{"voucher",	0,      "v_integer"},
	{NULL, 0}
};

static const char *admail_fsm(void *v, struct sql_worker *worker, const char *errstr);


/*****************************************************************************
 * admail_done
 * Exit function.
 **********/
static void
admail_done(void *v) {
	struct admail_state *state = v;
	struct cmd *cs = state->cs;
	struct hold_mail_param *bp;
	transaction_logprintf(cs->ts, D_DEBUG, "admail_done");

        /* Traverse all parameters */
	while ((bp = TAILQ_FIRST(&state->bp)) != NULL) {
		TAILQ_REMOVE(&state->bp, bp, tq);

		if (bp->key)
			free(bp->key);
		if (bp->value)
			free(bp->value);
		free(bp);
	}

	if (state->md)
		mail_free(state->md);
	if (state->action)
		parse_loadaction_cleanup(state->action);

	free(state);
	cmd_done(cs);
}

/*****************************************************************************/
static void
admail_insert_hold_mail_param(struct admail_state *state, const char *key, const char *value) {
	struct hold_mail_param *param;

	param = xmalloc(sizeof(*param));
	param->key = xstrdup(key);
	param->value = xstrdup(value ? value : "");

	TAILQ_INSERT_TAIL(&state->bp, param, tq);
}

#include "mail_types_action_times.h"

/*****************************************************************************
 * bind_insert
 * Initialise template API before a call to sql_worker_template_query().
 **********/
static void
bind_insert(struct bpapi *ba, struct admail_state *state) {
        int i = 0;
	const char *mail_type;

        while (parameters[i].name) {
                const char *name = parameters[i].name;
                if (cmd_haskey(state->cs, name)) {
                        const char *value = cmd_getval(state->cs, name);
                        bpapi_insert(ba, name, value);
                }
                i++;
        }


	if (cmd_haskey(state->cs, "reason")) {
		bpapi_insert(ba, "reason", cmd_getval(state->cs, "reason"));
	}

	mail_type = cmd_getval(state->cs, "mail_type");
	/* Select regtime, paytime and publishtime */
	if (lookup_mail_types_action_times(mail_type, strlen(mail_type)) != MTAT_NONE) {
		bpapi_insert(ba, "action_times", "1");
	}
}

/*****************************************************************************
 * rebuild_transaction
 **********/
static char *
rebuild_transaction(struct admail_state *state) {
        int i = 0;
        char *buffer = NULL;
        int buflen = 0;
        int bufpos = 0;
        transaction_logprintf(state->cs->ts, D_DEBUG, "Rebuild transaction");
        while (parameters[i].name) {
                const char *name = parameters[i].name;
                if (cmd_haskey(state->cs, name)) {
                        const char *value = cmd_getval(state->cs, name);
                        bufcat(&buffer, &buflen, &bufpos, "%s:%s\n", name, value);
                }
                i++;
        }
	if (cmd_haskey(state->cs, "reason"))
		bufcat(&buffer, &buflen, &bufpos, "reason:%s\n", cmd_getval(state->cs, "reason"));
        bufcat(&buffer, &buflen, &bufpos, "cmd:admail\n");
        bufcat(&buffer, &buflen, &bufpos, "commit:1\n");
        return buffer;
}

static int
send_notification(struct admail_state *state) {
	// Start queues sent
	struct bpapi ba;
	transaction_logprintf(state->cs->ts, D_DEBUG, "Init notification");

	BPAPI_INIT(&ba, NULL, pgsql);

	bpapi_insert(&ba, "type", "notification");
	bpapi_insert(&ba, "param_ad_id", cmd_getval(state->cs, "ad_id"));

	if (cmd_haskey(state->cs, "action_id")) {
		bpapi_insert(&ba, "param_action_id",cmd_getval(state->cs, "action_id"));
	}
	if (cmd_haskey(state->cs, "mail_type")) {
		bpapi_insert(&ba, "param_action_type",cmd_getval(state->cs, "mail_type"));
	}
	if (cmd_haskey(state->cs, "reason")) {
		bpapi_insert(&ba, "param_reason", cmd_getval(state->cs, "reason"));
	}

	struct bconf_node *paramroot = bconf_get(trans_bconf, "admail.backend_event_params");
	struct bconf_node *cfgparam;
	int i;
	for (i = 0; (cfgparam = bconf_byindex(paramroot, i)); i++) {
		const char *name = bconf_get_string(cfgparam, "name");
		const char *param = bconf_get_string(cfgparam, "param");
		const char *value = mail_get_param(state->md, name);

		if (value) {
			bpapi_insert(&ba, param, value);
		}

		transaction_logprintf(state->cs->ts, D_DEBUG, "PARAM:%s-%s:%s", name, param, value);
	}

	transaction_logprintf(state->cs->ts, D_DEBUG, "SEND ADS' notification");
	int response = publish_backend_event(state->cs, &ba);

	bpapi_output_free(&ba);
	bpapi_simplevars_free(&ba);
	bpapi_vtree_free(&ba);

	transaction_logprintf(state->cs->ts, D_DEBUG, "End notification");
	return response;
}

/*****************************************************************************
 * admail_mail_done
 * Called when mail is sent
 **********/
static void
admail_mail_done(struct mail_data *md, void *arg, int status) {
	struct admail_state *state = arg;
	struct cmd *cs = state->cs;

	if (status != 0) {
                cs->status = "TRANS_MAIL_ERROR";
	}

	admail_fsm(state, NULL, NULL);
}

static void
convert_regtime(struct admail_state *state) {
	char date[128];
	struct tm tm;
	const char* regtime = parse_loadaction_get_value(state->action, "actions", "regtime");
	if (regtime) {
		strptime(regtime, "%Y-%m-%d %H:%M:%S", &tm);
		strftime(date, sizeof(date), "%H%M%S", &tm);
		mail_insert_param(state->md, "reg_time", date);
	} else {
		transaction_logprintf(state->cs->ts, D_ERROR,
					  "(CRIT) Couldn't get regtime, mail type missing in mail_types_action_times?");
	}
}

static void
admail_send_mail(struct admail_state *state) {
	struct hold_mail_param *bp;
	const char *mail_type;
	int notification = 0;

        if (!cmd_haskey(state->cs, "mail_type")) {
                transaction_logprintf(state->cs->ts, D_ERROR, "mail_type was empty.");
                admail_mail_done(NULL, state, 1);
                return;
        }

	/* Log ad information */
	if (cmd_haskey(state->cs, "ad_id"))
		transaction_logprintf(state->cs->ts, D_INFO, "ad_id:%s", cmd_getval(state->cs, "ad_id"));
	if (cmd_haskey(state->cs, "action_id"))
		transaction_logprintf(state->cs->ts, D_INFO, "action_id:%s", cmd_getval(state->cs, "action_id"));

	mail_type = cmd_getval(state->cs, "mail_type");
	mail_insert_param(state->md, "mail_type", mail_type);
	mail_insert_param(state->md, "wrapper_template", "none");
	transaction_logprintf(state->cs->ts, D_DEBUG, "mail_type:%s", mail_type);

	if (strcmp(mail_type, "free") == 0) {	
		state->template = "mail/free.txt";	
	} else if ((strcmp(mail_type, "refuse") == 0) || (strcmp(mail_type, "edit_refuse") == 0) || (strcmp(mail_type, "edit_refused_refuse") == 0)) {
		notification = bconf_get_int(trans_bconf, "review_refuse.backend_event.send_enabled");
		if (strcmp(mail_type, "refuse") == 0) {
			state->template = "mail/refused.mime";
		} else if (strcmp(mail_type, "edit_refuse") == 0 ){
			state->template = "mail/edit_refused.mime";
		} else if (strcmp(mail_type, "edit_refused_refuse") == 0 ){
			state->template = "mail/edit_refused_refuse.mime";
		}

		mail_insert_param(state->md, "ad_id", cmd_getval(state->cs, "ad_id"));
		mail_insert_param(state->md, "action_id", cmd_getval(state->cs, "action_id"));
		mail_insert_param(state->md, "reason", cmd_getval(state->cs, "reason"));
		if (cmd_haskey(state->cs, "refusal_text")){
			mail_insert_param(state->md, "refusal_text", cmd_getval(state->cs, "refusal_text"));
		}
	} else if (strcmp(mail_type, "payinfo") == 0 ||
		strcmp(mail_type, "pay_reminder") == 0) {

		if (strcmp(mail_type, "payinfo") == 0) {
			const char *payment_transition;
			const char *payment_status;
			payment_transition = parse_loadaction_get_value(state->action,
								    "actions", "transition");
			/* set paymail action */
			payment_status = parse_loadaction_get_value(state->action,
								    "actions", "state");

			if (payment_transition && strcmp(payment_transition, "paymailsent") == 0) {
				transaction_logprintf(state->cs->ts, D_INFO,
						      "Ad %s has alreaady been sent an email. No mail sent.",
						      cmd_getval(state->cs, "ad_id"));
				admail_mail_done(NULL, state, 0);
				return;
			} else	if (payment_status && strcmp(payment_status, "unpaid") != 0) {
				transaction_logprintf(state->cs->ts, D_INFO,
						      "Ad %s is already paid. No paymail sent.",
						      cmd_getval(state->cs, "ad_id"));
				admail_mail_done(NULL, state, 0);
				return;
			}

			if (cmd_haskey(state->cs, "pay_type"))
				mail_insert_param(state->md, "pay_type", cmd_getval(state->cs, "pay_type"));

			state->template = "mail/payinfo.txt";
		} else {
			/* This is a pay_reminder.
			   Check if ad has been paid already */
			const char *payment_status;
			payment_status = parse_loadaction_get_value(state->action,
								    "actions", "state");

			if (payment_status &&
			    strcmp(payment_status, "unpaid") != 0) {
				transaction_logprintf(state->cs->ts, D_INFO,
						      "Ad %s is already paid. No reminder mail sent.",
						      cmd_getval(state->cs, "ad_id"));
				admail_mail_done(NULL, state, 0);
				return;
			}
			if (!parse_loadaction_get_value(state->action, "ad", "ad_id")) {
				transaction_logprintf(state->cs->ts, D_INFO,
						      "Ad %s is deleted and archived. No reminder mail sent.",
						      cmd_getval(state->cs, "ad_id"));
			}
		        mail_insert_param(state->md, "ad_id", cmd_getval(state->cs, "ad_id"));
			state->template = "mail/pay_reminder.mime";
		}

	} else if (strcmp(mail_type, "accept") == 0) {
		notification = bconf_get_int(trans_bconf, "review_accept.backend_event.send_enabled");
		state->template = "mail/accepted.mime";
	} else if (strcmp(mail_type, "edit_accept") == 0 ){
		notification = bconf_get_int(trans_bconf, "review_accept.backend_event.send_enabled");
		state->template = "mail/edit_accepted.mime";
	} else if (strcmp(mail_type, "accept_with_changes") == 0) {
		notification = bconf_get_int(trans_bconf, "review_accept.backend_event.send_enabled");
		state->template = "mail/accepted_with_changes.mime";
	} else if (strncmp(mail_type, "accept_spidered", 8) == 0) {
		state->template = "mail/accept_spidered.txt";
		convert_regtime(state);
	} else if (strcmp(mail_type, "deleted") == 0) {
		char date[128];
		time_t t;
		struct tm tm = {0};

		state->template = "mail/deleted_ad_mail.mime";
		time(&t);
		strftime(date, sizeof(date), "%e %B %Y %H:%M:%S", localtime_r(&t, &tm));
		mail_insert_param(state->md, "deleted_date", date);

		convert_regtime(state);
	} else if (strcmp(mail_type, "category_deleted") == 0) {
		char date[128];
		time_t t;
		struct tm tm = {0};

		state->template = "mail/category_deleted.txt";
		time(&t);
		strftime(date, sizeof(date), "%e %B %Y %H:%M:%S", localtime_r(&t, &tm));
		mail_insert_param(state->md, "deleted_date", date);

		convert_regtime(state);
	} else if (strcmp(mail_type, "deleted_monthly") == 0) {
		char date[128];
		time_t t;
		struct tm tm = {0};

		state->template = "mail/deleted_ad_monthly.mime";

		convert_regtime(state);

		time(&t);
		strftime(date, sizeof(date), "%e %B %Y %H:%M:%S", localtime_r(&t, &tm));
		mail_insert_param(state->md, "deleted_date", date);

	} else if (strcmp(mail_type, "undo_deleted") == 0) {
		state->template = "mail/undo_deletead.txt";
	} else if (strcmp(mail_type, "card_receipt") == 0) {
		state->template = "mail/card_receipt.txt";
	} else if (strcmp(mail_type, "voucher_used") == 0) {
		state->template = "mail/voucher_used.txt";
		if (cmd_haskey(state->cs, "voucher"))
			mail_insert_param(state->md, "voucher", cmd_getval(state->cs, "voucher"));
	} else if ((strcmp(mail_type, "half_time") == 0)) {
		const char *ad_status = parse_loadaction_get_value(state->action, "ad", "status");

		if (ad_status == NULL || (strcmp(ad_status, "active") != 0 && strcmp(ad_status, "deactivated") != 0 && strcmp(ad_status, "disabled") != 0)) {
			transaction_logprintf(state->cs->ts, D_INFO,
					"Ad %s is not active",
					cmd_getval(state->cs, "ad_id"));
			admail_mail_done(NULL, state, 0);
			return;
		}
		mail_insert_param(state->md, "ad_id", cmd_getval(state->cs, "ad_id"));
		mail_insert_param(state->md, "wrapper_template", "none");

		convert_regtime(state);

		/* Did you remember to add your new mail type to mail_types_action_times.gperf.enum if needed? */
		state->template = "mail/half_time.mime";
	} else if (strcmp(mail_type, "scarface_report") == 0  || strcmp(mail_type, "scarface_askinfo") == 0 ) {
		mail_insert_param(state->md, "reporter_email", cmd_getval(state->cs, "reporter_email"));
		mail_insert_param(state->md, "abuse_type", cmd_getval(state->cs, "abuse_type"));
		state->lang = cmd_getval(state->cs, "report_lang");

		if (strcmp(mail_type, "scarface_report") == 0) {
			mail_insert_param(state->md, "list_id", cmd_getval(state->cs, "list_id"));
			mail_insert_param(state->md, "resolution", cmd_getval(state->cs, "resolution"));
			if (cmd_getval(state->cs, "resolution")) {
				if ( strcmp(cmd_getval(state->cs, "resolution"), "accept") == 0 || strcmp(cmd_getval(state->cs, "resolution"), "accept_and_notify_owner") == 0) {
					state->template = "mail/scarface_accepted.mime";
				} else if (strcmp(cmd_getval(state->cs, "resolution"), "accept_and_delete") == 0 || strcmp(cmd_getval(state->cs, "resolution"), "accept_delete_and_warn") == 0) {
					state->template = "mail/scarface_accepted_and_deleted.txt";
				} else if (strcmp(cmd_getval(state->cs, "resolution"), "reject") == 0) {
					mail_insert_param(state->md, "uid", cmd_getval(state->cs, "uid"));
					mail_insert_param(state->md, "report_id", cmd_getval(state->cs, "report_id"));
					state->template = "mail/scarface_rejected.mime";
				}
			}

		} else {
			mail_insert_param(state->md, "uid", cmd_getval(state->cs, "uid"));
			mail_insert_param(state->md, "report_id", cmd_getval(state->cs, "report_id"));
			mail_insert_param(state->md, "list_id", cmd_getval(state->cs, "list_id"));
			mail_insert_param(state->md, "admin_message", cmd_getval(state->cs, "admin_message"));
			state->template = "mail/scarface_askinfo.txt";
		}
	} else if (strcmp(mail_type, "scarface_reject_custom") == 0) {
		mail_insert_param(state->md, "list_id", cmd_getval(state->cs, "list_id"));
		mail_insert_param(state->md, "uid", cmd_getval(state->cs, "uid"));
		mail_insert_param(state->md, "report_id", cmd_getval(state->cs, "report_id"));
		mail_insert_param(state->md, "reporter_email", cmd_getval(state->cs, "reporter_email"));
		mail_insert_param(state->md, "abuse_type", cmd_getval(state->cs, "abuse_type"));
		mail_insert_param(state->md, "uid", cmd_getval(state->cs, "uid"));
		state->lang = cmd_getval(state->cs, "report_lang");
		mail_insert_param(state->md, "notify_type", cmd_getval(state->cs, "notify_type"));
		state->template = "mail/scarface_rejected_notify_reporter.txt";
	} else if (strcmp(mail_type, "scarface_warn") == 0) {
		mail_insert_param(state->md, "list_id", cmd_getval(state->cs, "list_id"));
		mail_insert_param(state->md, "replier_mail", cmd_getval(state->cs, "email"));
		state->template = "mail/scarface_warning.mime";
	} else if (strcmp(mail_type, "scarface_owner") == 0) {
		mail_insert_param(state->md, "list_id", cmd_getval(state->cs, "list_id"));
		mail_insert_param(state->md, "subject", cmd_getval(state->cs, "subject"));
		mail_insert_param(state->md, "email", cmd_getval(state->cs, "email"));
		mail_insert_param(state->md, "notify_type", cmd_getval(state->cs, "notify_type"));
		state->template = "mail/scarface_notify_owner.mime";
	} else if (strcmp(mail_type, "category_changed") == 0) {
		state->template = "mail/category_changed.txt";
	} else if (strcmp(mail_type, "rejected_wrong_category") == 0) {
		mail_insert_param(state->md, "ad_id", cmd_getval(state->cs, "ad_id"));
		mail_insert_param(state->md, "action_id", cmd_getval(state->cs, "action_id"));
		state->template = "mail/rejected_wrong_category.txt";
	} else if (strcmp(mail_type, "bump_ok") == 0) {
		notification = bconf_get_int(trans_bconf, "bump_mail.backend_event.send_enabled");
		mail_insert_param(state->md, "to", cmd_getval(state->cs, "to"));
		mail_insert_param(state->md, "doc_type", cmd_getval(state->cs, "doc_type"));
		if (cmd_haskey(state->cs, "transition_type") && strcmp(cmd_getval(state->cs, "transition_type"), "autobump") == 0) {
			mail_insert_param(state->md, "product_ids", "10");
		} else {
			mail_insert_param(state->md, "product_ids", "1");
		}
		state->template = "mail/bump_ok.mime";
	} else if (strcmp(mail_type, "gallery_ok") == 0) {
		mail_insert_param(state->md, "to", cmd_getval(state->cs, "to"));
		mail_insert_param(state->md, "doc_type", cmd_getval(state->cs, "doc_type"));
		mail_insert_param(state->md, "product_ids", "3");
		state->template = "mail/bump_ok.mime";
	} else if (strcmp(mail_type, "weekly_bump_ok") == 0) {
		notification = bconf_get_int(trans_bconf, "bump_mail.backend_event.send_enabled");
		mail_insert_param(state->md, "to", cmd_getval(state->cs, "to"));
		mail_insert_param(state->md, "remaining_bumps", cmd_getval(state->cs, "remaining_bumps"));
		mail_insert_param(state->md, "doc_type", cmd_getval(state->cs, "doc_type"));
		state->template = "mail/weekly_bump_ok.mime";
	} else if (strcmp(mail_type, "mail_first_ad_bump") == 0) {
		notification = bconf_get_int(trans_bconf, "bump_mail.backend_event.send_enabled");
		mail_insert_param(state->md, "to", cmd_getval(state->cs, "to"));
		mail_insert_param(state->md, "subject", cmd_getval(state->cs, "subject"));
		state->template = "mail/mail_first_ad_bump.mime";
	} else if (strcmp(mail_type, "mail_advertisement_bump") == 0) {
		state->template = "mail/mail_advertisement_bump.mime";
	} else if (strcmp(mail_type, "deactivated") == 0) {
		state->template = "mail/deactivated_ad.mime";
	} else {
		transaction_logprintf(state->cs->ts, D_ERROR,
				"Wrong mail_type: '%s'",
				mail_type);
		admail_mail_done(NULL, state, 1);
		return;
	}
	/* Send mail */
	transaction_logprintf(state->cs->ts, D_DEBUG,
			      "Sending email included with template name %s", state->template);

	// Check if it should send a notification backend event
	if (notification){
		transaction_logprintf(state->cs->ts, D_DEBUG, "Notification Enabled");
		if (send_notification(state)) {
			transaction_logprintf(state->cs->ts, D_ERROR, "Failed to send notification");
		}
	}
        /*
         * Check if mail shall be held.
         */
	TAILQ_FOREACH(bp, &state->bp, tq) {
		char *key = bp->key;
		char *regex = bp->value;
		const char *mail_value = mail_get_param(state->md, key);
		if (mail_value) {
			pcre *re;
			const char *err;
			int erro;

			if (!(re = pcre_compile(regex, REG_NOSUB | REG_EXTENDED | REG_ICASE, &err, &erro, NULL))) {
				transaction_logprintf(state->cs->ts, D_ERROR, 
						"Wrong hold mail regex: '%s' (%s)",
						regex, err);
			} else {
				if (pcre_exec(re, NULL, mail_value, strlen(mail_value), 0, 0, NULL, 0) == 0) {
					struct internal_cmd *intcmd = zmalloc(sizeof(struct internal_cmd));
					char *cmd = rebuild_transaction(state);

					transaction_logprintf(state->cs->ts, D_INFO,
							"Mail held because rule '%s' and regex '%s' matched value '%s'",
							key, regex, mail_value);
					xasprintf(&intcmd->cmd_string,
							"cmd:queue\n"
							"commit:1\n"
							"queue:hold_mail\n"
							"blob:%zu:command\n"
							"%s\n",
							strlen (cmd), cmd);
					transaction_internal(intcmd);
					pcre_free(re);
					free(cmd);
					admail_mail_done(NULL, state, 0);
					return;
				}
				pcre_free(&re);
			}
		}
	}

	state->md->callback = admail_mail_done;
	state->md->command = state;
	if (!state->lang)
		state->lang = bconf_get_string(bconf_root, "*.*.common.default.lang");
	mail_send(state->md, state->template, state->lang);
	state->mail_sent = 1;
	state->md = NULL; /* mail responsibility to free */
}


/*****************************************************************************
 * admail_fsm
 * State machine
 **********/
static const char *
admail_fsm(void *v, struct sql_worker *worker, const char *errstr) {
	struct admail_state *state = v;
	struct cmd *cs = state->cs;
        struct bpapi ba;
	struct action_param *action_param;

	transaction_logprintf(cs->ts, D_DEBUG, "Admail fsm %u", state->state);

	/* 
	 * General error handling for db errors.
	 */
	if (errstr || (worker && worker->sql_errno(worker))) {
                transaction_logprintf(cs->ts, D_ERROR, "Statement failed (%d, %u, %s)", 
                                      state->state,
                                      worker ? worker->sql_errno(worker) : 0,
                                      errstr ? errstr : worker->sql_errstr(worker));
                cs->status = "TRANS_DATABASE_ERROR";
                cs->message = xstrdup(errstr ? errstr : worker->sql_errstr(worker));
		state->state = ADMAIL_ERROR;
        }

	while (state->state != ADMAIL_DONE) {
		switch ((admail_state_t) state->state) {
		case ADMAIL_INIT:
			/* next state */
			state->state = ADMAIL_LOAD_DATA;

			/* Initialise the templateparse structure */
			BPAPI_INIT(&ba, NULL, pgsql);
			bind_insert(&ba, state);
			
			/* Check if is a spidered ad and pass param to loadaction */
			if (cmd_haskey(cs, "mail_type") && !strcmp(cmd_getval(cs, "mail_type"), "accept_spidered"))
				bpapi_insert(&ba, "spidered", "accept_spidered");

			/* Use master for up to date data */
			sql_worker_template_query_flags(&config.pg_master, SQLW_FINAL_CALLBACK, 
							"sql/loadaction.sql", &ba, 
							admail_fsm, state, cs->ts->log_string);
			/* Clean-up */
			bpapi_output_free(&ba);
			bpapi_simplevars_free(&ba);
			bpapi_vtree_free(&ba);

			/* Return to libevent and wait for the query to return */
			return NULL;

		case ADMAIL_LOAD_DATA:
			transaction_logprintf(cs->ts, D_DEBUG, "parse_loadaction");

			if (worker->sw_state == RESULT_DONE) {
				state->state = ADMAIL_POPULATE;
				continue;
			}
			/* Parse the result for the current SELECT */
			state->action = parse_loadaction(worker,
							 cs->ts, state->action);
			return NULL;

		case ADMAIL_POPULATE:
			if (TAILQ_EMPTY(&state->action->h)) {
				state->state = ADMAIL_ERROR;
				cs->message = xstrdup("No such ad");
				continue;
			}
			state->md = zmalloc(sizeof(*state->md));
			TAILQ_FOREACH(action_param, &state->action->h, entry) {

                transaction_logprintf(cs->ts, D_DEBUG, "insert param: %s", action_param->key);
                //check if the parameter is the country of the ad, because it should not be sent on the email
                if(strcmp(action_param->key, "country") == 0)
                    continue;//pass to the next param
				if (!strcmp(action_param->prefix, "ad_change_params")) {
					if (!action_param->value)
						continue;
					if (!strncmp(action_param->key, "param_", 6)) {
						const char *key = action_param->key + 6;

						if (strcmp(key, "id_orig") != 0 && /* XXX */
						    strcmp(key, "store") != 0) {
							if (!isdigit(key[strlen(key) - 1])) {
								mail_insert_param(state->md, 
										  "param_name",
										  key);
								mail_insert_param(state->md, 
										  "param_value",
										  action_param->value);

							} else {
								/* Original key */
								mail_insert_param(state->md, key, action_param->value);
							}
						}
					} else {
						char *key;

						xasprintf(&key, "ad_%s", action_param->key);
						mail_insert_param(state->md, 
								  key,
								  action_param->value);

						free(key);
					}
				} else if (strcmp(action_param->prefix, "params") == 0) {
					/* 
					 * For params, add:
					 * param_name = 
					 * param_value = 
					 */
                                        if (strcmp(action_param->key, "id_orig") != 0 && /* XXX */
                                            strcmp(action_param->key, "store") != 0) {
						char *key;
						struct action_param *ap;

						xasprintf(&key, "param_%s", action_param->key);

						ap = parse_loadaction_pop_param(state->action,
										"ad_change_params", key);
						if (!ap || ap->value) {
							if (!isdigit(key[strlen(key) - 1])) {
								mail_insert_param(state->md,
										  "param_name",
										  action_param->key);
								mail_insert_param(state->md,
										  "param_value",
										  ap? ap->value : action_param->value);

							}
							mail_insert_param(state->md, key, ap ? ap->value : action_param->value);
						}
						if (ap)
							free(ap);
						free(key);
                                        }
					if (strcmp(action_param->key, "lang") == 0)
						state->lang = action_param->value;
				} else if (strcmp(action_param->prefix, "fetch_extra_check") == 0) {
					/* We should insert the data that we need to mail params */
					mail_insert_param(state->md, action_param->key, action_param->value);
				} else if (strcmp(action_param->prefix, "images.0") == 0) {
					transaction_logprintf(cs->ts, D_DEBUG, "Entering image.0 prefix");
					if(strcmp(action_param->key, "name") == 0) {
						mail_insert_param(state->md, "image_name", action_param->value);
					}
				} else {
					char *key;
					struct action_param *ap;

					if (strcmp(action_param->key, "lang") == 0)
						state->lang = action_param->value;

					xasprintf(&key, "%s_%s", action_param->prefix, action_param->key);

					ap = parse_loadaction_pop_param(state->action, "ad_change_params",
									action_param->key);
					if (!ap || ap->value) {
						mail_insert_param(state->md, 
								  key,
								  ap? ap->value : action_param->value);
					}
					if (ap)
						free(ap);
					free(key);
				}
			}
			state->state = ADMAIL_GET_HOLD_MAIL_PARAMS;

			sql_worker_set_wflags(worker, 0);
			return "SELECT name, value FROM hold_mail_params;";

		case ADMAIL_GET_HOLD_MAIL_PARAMS:
			/* Process all results */
			TAILQ_INIT(&state->bp);
			if (worker->rows(worker)) {
				while (worker->next_row(worker)) {
					const char *name = worker->get_value(worker, 0);
					const char *regex = worker->get_value(worker, 1);
					transaction_logprintf(cs->ts, D_DEBUG, "Adding block rules %s:%s", 
							      name, regex);
					admail_insert_hold_mail_param(state, name, regex);
				}
			}

			state->state = ADMAIL_SEND_MAIL;
			continue;

		case ADMAIL_SEND_MAIL:

                state->state = ADMAIL_RECORD_PAYMAIL;
                admail_send_mail(state);

			/* Return will go back to timeout callback function, and then to libevent */
			return NULL;

		case ADMAIL_RECORD_PAYMAIL:
			if (strcmp(cmd_getval(state->cs, "mail_type"), "payinfo") == 0 && 
			    state->mail_sent == 1 &&
			    cmd_haskey(state->cs, "action_id")) {
				/* Ad action state */
				/* next state */
				state->state = ADMAIL_RECORD_PAYMAIL_LOAD_DATA;

				BPAPI_INIT(&ba, NULL, pgsql);
				bind_insert(&ba, state);

				/* Use master for up to date data */
				sql_worker_template_query_flags(&config.pg_master, SQLW_FINAL_CALLBACK, 
								"sql/insert_state.sql", &ba, 
								admail_fsm, state, cs->ts->log_string);

				/* Clean-up */
				bpapi_output_free(&ba);
				bpapi_simplevars_free(&ba);
				bpapi_vtree_free(&ba);

				/* Return to libevent and wait for the query to return */
				return NULL;
			}

			state->state = ADMAIL_RECORD_HALF_TIME;
			continue;

		case ADMAIL_RECORD_HALF_TIME:
			if (strcmp(cmd_getval(state->cs, "mail_type"), "half_time") == 0 && 
			    state->mail_sent == 1) {
				state->state = ADMAIL_DONE;

				BPAPI_INIT(&ba, NULL, pgsql);
				bind_insert(&ba, state);

				bpapi_insert(&ba, "ad_id", cmd_getval(state->cs, "ad_id"));
				bpapi_insert(&ba, "action_type", "half_time_mail");
				bpapi_insert(&ba, "state", "reg");
				bpapi_insert(&ba, "transition", "initial");

				/* Use master for up to date data */
				sql_worker_template_query(&config.pg_master, 
								"sql/insert_ad_action_state.sql", &ba, 
								admail_fsm, state, cs->ts->log_string);

				/* Clean-up */
				bpapi_output_free(&ba);
				bpapi_simplevars_free(&ba);
				bpapi_vtree_free(&ba);

				/* Return to libevent and wait for the query to return */
				return NULL;
			}

			state->state = ADMAIL_RECORD_GALLERY_EXPIRED;
			continue;

		case ADMAIL_RECORD_GALLERY_EXPIRED:
			state->state = ADMAIL_DONE;

			if (strcmp(cmd_getval(state->cs, "mail_type"), "gallery_expired") == 0) {
				BPAPI_INIT(&ba, NULL, pgsql);

				bpapi_insert(&ba, "ad_id", cmd_getval(state->cs, "ad_id"));
				bpapi_insert(&ba, "action_type", "gal_exp_mail");
				bpapi_insert(&ba, "state", "reg");
				bpapi_insert(&ba, "transition", "initial");

				sql_worker_template_query(&config.pg_master,
							  "sql/insert_ad_action_state.sql", &ba,
							  admail_fsm, state, cs->ts->log_string);

				return NULL;
			}
			continue;
		case ADMAIL_RECORD_PAYMAIL_LOAD_DATA:
			if (worker->sw_state == RESULT_DONE) {
				state->state = ADMAIL_DONE;
				sql_worker_set_wflags(worker, 0);
				continue;
			}

			return NULL;

		case ADMAIL_DONE:
			admail_done(state);
			return NULL;

		case ADMAIL_ERROR:
			/* MAJOR STATE ERROR */
			cs->status = "TRANS_MAIL_ERROR";
			transaction_logprintf(cs->ts, D_ERROR, "ADMAIL_ERROR state (%d)", 
					      state->state);
			admail_done(state);
			return NULL;

		default:
			/* UNKNOWN STATE ERROR */
			cs->status = "TRANS_ERROR";
			transaction_logprintf(cs->ts, D_ERROR, "Unknown state (%d)", 
					      state->state);
			admail_done(state);
			return NULL;
		}
	}

	admail_done(state);
	return NULL;
}

/*****************************************************************************
 * admail
 * Main function that initialise a state-structure and calls the state machine.
 **********/
static void
admail(struct cmd *cs) {
	transaction_logprintf(cs->ts, D_DEBUG, "Start Admail transaction");
	struct admail_state *state;
	state = zmalloc(sizeof(*state));
	state->cs = cs;
	state->state = ADMAIL_INIT;

	admail_fsm(state, NULL, NULL);
}

ADD_COMMAND(admail,     /* Name of command */
		NULL,
            admail,     /* Main function name */
            parameters, /* Parameters struct */
	    "Send different mails given an ad_id"); 
