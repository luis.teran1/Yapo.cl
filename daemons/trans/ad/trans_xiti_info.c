#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <time.h>

#include "trans.h"
#include "util.h"
#include <ctemplates.h>
#include "validators.h"
#include <bconf.h>
#include <event.h>

#define XITI_INFO_DONE  0
#define XITI_INFO_ERROR 1
#define XITI_INFO_INIT  2
#define XITI_INFO_RESULT 3

struct xiti_info_state {
	int state;
	struct cmd *cs;
};

static struct c_param parameters[] = {
	{"verify_code",	P_REQUIRED,	"v_verify_code"},
	{NULL, 0}
};

static const char *xiti_info_fsm(void *, struct sql_worker *, const char *);

extern struct bconf_node *bconf_root;

/*****************************************************************************
 * xiti_info_done
 * Exit function.
 **********/
static void
xiti_info_done(void *v) {
	struct xiti_info_state *state = v;
	struct cmd *cs = state->cs;
	free(state);
	cmd_done(cs);
}

/*****************************************************************************
 * bind_insert
 * Initialise template API before a call to sql_worker_template_query().
 **********/
static void
bind_insert(struct bpapi *ba, struct xiti_info_state *state) {

	struct cmd *cs = state->cs;
	bpapi_insert(ba, "verify_code", 
			cmd_getval(cs, "verify_code"));
}

/*****************************************************************************
 * xiti_info_fsm
 * State machine
 **********/
static const char *
xiti_info_fsm(void *v, struct sql_worker *worker, const char *errstr) {
	struct xiti_info_state *state = v;
	struct cmd *cs = state->cs;
        struct bpapi ba;

	/* 
	 * General error handling for db errors.
	 */
	if (errstr) {
		if( strstr(errstr, "ERROR_VERIFY_CODE_NOT_FOUND") ){
			cs->status = "TRANS_ERROR";
			transaction_printf(cs->ts, "verify_code: ERROR_VERIFY_CODE_NOT_FOUND");
		} else {
			cs->status = "TRANS_DATABASE_ERROR";
		}

                transaction_logprintf(cs->ts, D_ERROR, "xiti_info_fsm statement failed (%d, %s)", 
                                      state->state,
                                      errstr);
                cs->message = xstrdup(errstr);
		state->state = XITI_INFO_ERROR;
        }

	while (1) {
		transaction_logprintf(cs->ts, D_DEBUG, "xiti_info_fsm(%p, %p, %p), state=%d", 
				      v, worker, errstr, state->state);

		switch (state->state) {
		case XITI_INFO_INIT:
			/* next state */
			state->state = XITI_INFO_RESULT;

			/* Initialise the templateparse structure */
			BPAPI_INIT(&ba, NULL, pgsql);
			bind_insert(&ba, state);

			/* Register SQL query with our xiti_info_fsm() as callback function */
			/* Make sure to switch to slave if read-only query. */		
			sql_worker_template_query(&config.pg_slave, 
					"sql/call_xiti_info.sql", &ba, 
					xiti_info_fsm, state, cs->ts->log_string);

			/* Clean-up */
			bpapi_output_free(&ba);
			bpapi_simplevars_free(&ba);
			bpapi_vtree_free(&ba);

			/* Return to libevent */
			return NULL;

                case XITI_INFO_RESULT:
		{
			int col;
			int ncols = worker->fields(worker);
			int ad = 0;

                        state->state = XITI_INFO_DONE;
                        
                        while (worker->next_row(worker)) {
				for (col = 0; col < ncols; col++) {
					if (!worker->is_null(worker, col))
						transaction_printf(cs->ts, "%s:%s\n", 
								worker->field_name(worker, col),
								worker->get_value(worker, col));
				}
				ad++;
                        }
			continue;
		}

		case XITI_INFO_DONE:
			xiti_info_done(state);
			return NULL;

		case XITI_INFO_ERROR:
			transaction_logprintf(cs->ts, D_ERROR, "Error state");
			xiti_info_done(state);
			return NULL;

		default:
			/* MAJOR STATE ERROR */
			cs->status = "TRANS_ERROR";
			transaction_logprintf(cs->ts, D_ERROR, "Unknown state (%d)", 
					      state->state);
			return NULL;

		}
	}
}

/*****************************************************************************
 * xiti_info
 * Main function that initialise a state-structure and calls the state machine.
 **********/
static void
xiti_info(struct cmd *cs) {
	transaction_logprintf(cs->ts, D_DEBUG, "Start XITI info transaction");
	struct xiti_info_state *state;
	state = zmalloc(sizeof(*state));
	state->cs = cs;
	state->state = XITI_INFO_INIT;

	xiti_info_fsm(state, NULL, NULL);
}

ADD_COMMAND(xiti_info,     /* Name of command */
		NULL,
            xiti_info,     /* Main function name */
            parameters,  /* Parameters struct */
            "Ad info command");/* Command description */
