#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <time.h>

#include "trans.h"
#include "util.h"
#include <ctemplates.h>
#include "validators.h"
#include <bconf.h>
#include <event.h>

extern struct bconf_node *bconf_root;

enum monthly_delete_state_val {
	MONTHLY_DELETE_DONE,
	MONTHLY_DELETE_INIT,
	MONTHLY_DELETE_DELETE_AD
};

static const char *state_lut[] = {
	"MONTHLY_DELETE_DONE",
	"MONTHLY_DELETE_INIT",
	"MONTHLY_DELETE_DELETE_AD"
};

static struct c_param parameters[] = {
	{"remote_addr",        P_REQUIRED, "v_ip"},
	{"remote_browser",     P_OPTIONAL, "v_remote_browser"},
	{"special_categories", P_OPTIONAL, "v_bool"},
	{NULL, 0}
};

/*****************************************************************************
 * delete_ad
 * Delete a single ad by using the deletead transaction
 *
 **********/
static int
delete_ad(const char *list_id, const char *remote_addr) {
	char *res = transaction_internal_output_printf(
		"cmd:deletead\n"
		"commit:1\n"
		"id:%s\n"
		"remote_addr:%s\n"
		"monthly:1\n"
		"reason:expired_deleted\n"
		"end\n",
		list_id,
		remote_addr
	);
	int got_ok = res && strstr(res, "status:TRANS_OK");
	free(res);
	return got_ok;
}

/*****************************************************************************
 * monthly_delete
 **********/
static void
monthly_delete(struct cmd *cs) {
	enum monthly_delete_state_val state;
	int total_ads = 0;
	int deleted = 0;
	int deleted_failed = 0;
	struct sql_worker *worker;
	const char *errstr = NULL;
	struct command_bpapi cba;

	/* Make sure we have the two limits in bconf (*.monthly_delete.limit, *.monthly_delete.delete_interval) */

	if (!bconf_vget(bconf_root, "*", "monthly_delete", "limit", NULL) ||
	    !bconf_vget(bconf_root, "*", "monthly_delete", "delete_interval", NULL)) {
		cs->status = "TRANS_ERROR";
		cs->message = xstrdup("bconf values missing, check for monthly_delete.limit and monthly_delete.delete_interval");
		cmd_done(cs);
		return;
	}

	state = MONTHLY_DELETE_INIT;
	command_bpapi_init(&cba, cs, CMD_BPAPI_HOST, "monthly_delete");
	const char *remote_addr = cmd_getval(cs, "remote_addr");
	const char *sqltmpl = "sql/monthly_delete.sql";
	if (cmd_haskey(cs, "special_categories")) {
		sqltmpl = "sql/monthly_delete_by_category.sql";
	}

	while (state != MONTHLY_DELETE_DONE) {
		transaction_logprintf(cs->ts, D_DEBUG, "monthly deletead fsm state %s", state_lut[state]);

		switch (state) {
		case MONTHLY_DELETE_INIT:


			/* Make sure to switch to slave if read-only query. */
			worker = sql_blocked_template_query(
				"pgsql.slave",
				0,
				sqltmpl,
				&cba.ba,
				cs->ts->log_string,
				&errstr
			);

			/* Check if ad exists */
			if (worker) {
				while (sql_blocked_next_result(worker, &errstr) == 1) {
					transaction_logprintf(cs->ts, D_DEBUG, "MONTHLY_DELETE_AD_INIT result! rows:%d", worker->rows(worker));
					if (worker->rows(worker) > 0) {
						sql_worker_bpa_insert(worker, &cba.ba, "");
					}
				}
			}

			if (errstr)
				goto cleanup;

			sql_worker_put(worker);
			worker = NULL;
			state = MONTHLY_DELETE_DELETE_AD;
			continue;

		case MONTHLY_DELETE_DELETE_AD:
			state = MONTHLY_DELETE_DONE;
			total_ads = bpapi_length(&cba.ba, "list_id");
			for (int i = 0; i < total_ads; i++) {
				const char *list_id = bpapi_get_element(&cba.ba, "list_id", i);
				int ok = delete_ad(list_id, remote_addr);
				if (ok)
					deleted++;
				else
					deleted_failed++;
			}
			continue;

		case MONTHLY_DELETE_DONE:
			break;
		}
	}

cleanup:
	if (errstr) {
		transaction_logprintf(cs->ts, D_ERROR,
			"monthly_delete statement failed (%d, %s)",
			state, errstr
		);
		cs->status = "TRANS_DATABASE_ERROR";
		cs->message = xstrdup(errstr);
	}

	if (worker) {
		while (sql_blocked_next_result(worker, &errstr) != 0)
			transaction_logprintf(cs->ts, D_ERROR, "DATABASE ERROR: %s", errstr);
		sql_worker_put(worker);
	}

	if (deleted_failed > 0) {
		cs->status = "TRANS_ERROR";
	}

	transaction_printf(cs->ts, "deleted:%d\n", deleted);
	transaction_printf(cs->ts, "deleted_failed:%d\n", deleted_failed);

	if (total_ads) {
		const char *from = bpapi_get_element(&cba.ba, "list_time", 0);
		const char *to = bpapi_get_element(&cba.ba, "list_time", total_ads - 1);
		transaction_printf(cs->ts, "deleted_from:%s\n", from);
		transaction_printf(cs->ts, "deleted_to:%s\n", to);
	}

	command_bpapi_free(&cba);
	cmd_done(cs);

}

ADD_COMMAND(monthly_delete, /* Name of command */
            NULL,           /* Called during the initalisation phase */
            monthly_delete, /* Main function name */
            parameters,     /* Parameters struct */
            "Monthly delete command - delete all ads older than *.monthly_delete.interval days"); /* Command description */
