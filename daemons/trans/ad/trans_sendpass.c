#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdarg.h>
#include <unistd.h>
#include <sys/socket.h>
#include <ctype.h>
#include <time.h>
#include <syslog.h>
#include <locale.h>

#include "logging.h"
#include "log_event.h"

#include "command.h"
#include "trans.h"
#include "util.h"
#include "bconf.h"
#include "trans_mail.h"
#include "validators.h"
#include "mail_log.h"

extern struct bconf_node *bconf_root;

static struct c_param parameters[] = {
	{"id",  P_REQUIRED, "v_id"},
	{"email",  P_REQUIRED, "v_email"},
	{"ip",  P_REQUIRED, "v_ip"},
	{"recovery_type", 0, "v_recovery_type"},
	{NULL, 0}
};

struct validator v_recovery_type[] = {
	VALIDATOR_REGEX("^(edit|delete)$", REG_EXTENDED|REG_NOSUB, "ERROR_RECOVERY_TYPE_INVALID"),
	{0}
};
ADD_VALIDATOR(v_recovery_type);
static void
sendmail_done_cb(struct mail_data *md, void *v, int status) {
	struct cmd *cs = (struct cmd *)v;

	if (status != 0) {
		log_printf(LOG_CRIT, "SENDPASS-SENDMAIL ERROR: Unable to send mail");
		cs->status = "TRANS_MAIL_ERROR";
		cmd_done(cs);
		return;
	}

	log_event("TYPE : 11");
	transaction_logprintf(cs->ts, D_INFO, "SENDPASS-SENDMAIL: 200 Mail sent OK");
	cmd_done(cs);
}

static const char *
sendpass_cb(void *v, struct sql_worker *worker, const char *errstr) {
	struct cmd *cs = (struct cmd*)v;
	struct mail_data *md;
	time_t t;
	struct tm tm;
	char date[256];

	/* Check if there was an error in last sql */
	if (errstr != NULL) {
		cs->status = "TRANS_DATABASE_ERROR";
		if (cs->message)
			free(cs->message);
		cs->message = xstrdup(errstr);
		cmd_done(cs);
		return NULL;
	}

	/* Check if ad exists */
	if (!worker->next_row(worker)) {
		transaction_printf(cs->ts, "%s:%s\n", "id", "ERROR_AD_NOT_FOUND");
		cs->status = "TRANS_ERROR";
		cmd_done(cs);
		return NULL;
	}

	/* Email check */
	if (cmd_getval(cs, "email") == NULL || strcasecmp(cmd_getval(cs, "email"), worker->get_value_byname(worker, "email")) != 0) {
		if (cmd_getval(cs, "email") != NULL) {
			transaction_printf(cs->ts, "%s:%s\n", "email", "ERROR_EMAIL_WRONG");
		} else {
			transaction_printf(cs->ts, "%s:%s\n", "email", "ERROR_EMAIL_MISSING");
		}
		cs->status = "TRANS_ERROR";
		cmd_done(cs);
		return NULL;
	}

	/* SEND password mail */
	md = zmalloc(sizeof (*md));
	md->command = cs;
	md->callback = sendmail_done_cb;
	md->log_string = cs->ts->log_string;
	/* Add email paramaters */
	mail_insert_param(md, "salted_passwd", worker->get_value_byname(worker, "salted_passwd"));
	mail_insert_param(md, "ad_id", worker->get_value_byname(worker, "ad_id"));
	mail_insert_param(md, "subject", worker->get_value_byname(worker, "subject"));
	mail_insert_param(md, "passwd", worker->get_value_byname(worker, "passwd"));
	mail_insert_param(md, "email", worker->get_value_byname(worker, "email"));
	mail_insert_param(md, "type", worker->get_value_byname(worker, "type"));
	mail_insert_param(md, "id", cmd_getval(cs, "id"));
	mail_insert_param(md, "price", worker->get_value_byname(worker, "price"));
	mail_insert_param(md, "list_time", worker->get_value_byname(worker, "list_time"));
	mail_insert_param(md, "media", worker->get_value_byname(worker,  "ad_media_id"));
	mail_insert_param(md, "currency", worker->get_value_byname(worker,  "currency"));
	mail_insert_param(md, "recovery_type", cmd_getval(cs, "recovery_type"));
	time(&t);
	localtime_r(&t, &tm);
	strftime(date, sizeof(date), "%e %B %Y %H:%M:%S", &tm);
	mail_insert_param(md, "date", date);
	/* Send mail */
	transaction_logprintf(cs->ts, D_DEBUG, "Sending email included with template name");
	mail_send(md, "mail/sendpass_mail.mime", cmd_getval(cs, "lang"));

	return NULL;
}

static void
sendpass_log_cb(struct mail_log *log, int id, const char *errstr) {
	struct cmd *cs = log->data;
	const char *list_id = cmd_getval(cs, "id");
	struct bpapi ba;

	free(log);

	/* Check if there was an error in last sql */
	if (errstr != NULL) {
		if (!strcmp(errstr, "ERROR_SPAM")) {
			cs->status = "TRANS_ERROR";
			cs->error = "ERROR_SPAM";
		} else {
			cs->status = "TRANS_DATABASE_ERROR";
			if (cs->message)
				free(cs->message);
			cs->message = xstrdup(errstr);
		}
		cmd_done(cs);
		return;
	}


	BPAPI_INIT(&ba, NULL, pgsql);
	bpapi_insert(&ba, "list_id", list_id);

	/* Get ad information */
	sql_worker_template_query(&config.pg_master, "sql/get_sendpass_data.sql", &ba,
				  sendpass_cb, cs, cs->ts->log_string);

	bpapi_output_free(&ba);
	bpapi_simplevars_free(&ba);
	bpapi_vtree_free(&ba);
}

static void
sendpass(struct cmd *cs) {
	struct mail_log *log = zmalloc(sizeof(*log));
	
	log->type = "password";
	log->remote_addr = cmd_getval(cs, "ip");
	log->email = cmd_getval(cs, "email");
	log->list_id = atoi(cmd_getval(cs, "id"));
	log->spam_minutes = bconf_get_int(bconf_root, "*.sendpass.spam.minutes");
	log->spam_counts[MAIL_SPAM_SEND] = bconf_get_int(bconf_root, "*.sendpass.spam.send_count");
	log->spam_counts[MAIL_SPAM_RECV] = bconf_get_int(bconf_root, "*.sendpass.spam.recv_count");
	log->spam_counts[MAIL_SPAM_AD] = bconf_get_int(bconf_root, "*.sendpass.spam.ad_count");
	log->cb = sendpass_log_cb;
	log->data = cs;

	log_mail(log);
}

ADD_COMMAND(sendpass, NULL, sendpass, parameters, NULL);
