#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdarg.h>
#include <unistd.h>
#include <sys/socket.h>
#include <ctype.h>
#include <time.h>
#include <syslog.h>
#include <locale.h>
#include "logging.h"
#include "command.h"
#include "trans.h"
#include "util.h"
#include "bconf.h"
#include "mail_log.h"
#include "log_event.h"
#include "trans_at.h"
#include <ctemplates.h>
#include "utils/send_backend_event.h"

#define ADREPLY_DONE   0
#define ADREPLY_ERROR  1
#define ADREPLY_INIT   2
#define ADREPLY_INSERT_MAIL_QUEUE   3
#define ADREPLY_BEGIN  4
#define ADREPLY_CHECK  5
#define ADREPLY_LOG_REDIR 6
#define ADREPLY_SEND_CC 7

extern struct bconf_node *bconf_root;

struct adreply_state {
	int state;
	struct cmd *cs;
	int mail_queue_id;
	char *body;
};

static struct c_param parameters[] = {
	{"id",  	P_REQUIRED,	 	"v_id"}, /* list_id */
	{"email",  	P_REQUIRED,	 	"v_email_basic"},
	{"adreply_body", 	P_REQUIRED, 			"v_adreply_body"},
	{"name", 	P_REQUIRED,		"v_name"},
	{"phone", 	P_EMPTY,		"v_phone_basic"},
	{"remote_addr", 0, 			"v_ip"},
	{"cc", 		P_EMPTY,		"v_bool", 		"If true, send a copy of the mail to the supplied email"},
	{"remote_browser", 0,                   "v_remote_browser"},
    {"redir_code",  0,        		"v_string_isprint"},
   	{"storereply",       0,        		"v_bool"},
	{NULL, 0}
};

static void
adreply_done(void *v) {
	struct adreply_state *state = v;
	struct cmd *cs = state->cs;

	if (state->body)
		free(state->body);
	free(state);
	cmd_done(cs);
}

static const char *
adreply_fsm(void *v, struct sql_worker *worker, const char *errstr) {
	struct adreply_state *state = v;
	struct cmd *cs = state->cs;
        struct bpapi ba;

	/* Check if there was an error in last sql. */
	if (errstr != NULL) {
                transaction_logprintf(cs->ts, D_ERROR, "adreply_fsm statement failed (%d, %s)", 
                                      state->state, errstr);
		if (strstr(errstr, "TRANS_MAIL_ERROR")) {
			cs->status = "TRANS_MAIL_ERROR";
		} else if (strstr(errstr, "ERROR_NOT_FOUND")) {
			cs->status = "TRANS_ERROR";
			cs->error = "ERROR_NOT_FOUND";
		} else {
			cs->status = "TRANS_DATABASE_ERROR";
                	cs->message = xstrdup(errstr);
		}
		state->state = ADREPLY_ERROR;
		if (worker)
			return "ROLLBACK";
	}

	while (1) {
		transaction_logprintf(cs->ts, D_DEBUG, "adreply_fsm state = %d", state->state);

		switch (state->state) {
		case ADREPLY_INIT:
			state->state = ADREPLY_INSERT_MAIL_QUEUE;

			if (cmd_haskey(cs, "storereply")) {
				continue;
			}

			BPAPI_INIT(&ba, NULL, pgsql);

			bpapi_insert(&ba, "list_id", cmd_getval(cs, "id"));

			sql_worker_template_query(&config.pg_slave,
						  "sql/ad_lang.sql", &ba,
						  adreply_fsm, state, cs->ts->log_string);

			/* Clean-up */
			bpapi_output_free(&ba);
			bpapi_simplevars_free(&ba);
			bpapi_vtree_free(&ba);

			/* Return to libevent */
			return NULL;

		case ADREPLY_INSERT_MAIL_QUEUE:
		{
			const char *lang;
			const char *phone = cmd_getval(cs, "phone");

			if (worker) {
				if (!worker->next_row(worker)) {
					state->state = ADREPLY_ERROR;
					continue;
				}
				lang = worker->get_value_byname(worker, "lang");
			} else {
				lang = bconf_get_string(bconf_root, "*.*.common.default.lang");
			}

			if (!lang) {
				state->state = ADREPLY_ERROR;
				continue;
			}

			if (phone && *phone && bconf_vget(bconf_root, "*" , "adreply", "phone_prefix", lang, NULL)) {
				xasprintf(&state->body, "%s\n\n%s %s\n",
					  cmd_getval(cs, "adreply_body"),
					  bconf_value(bconf_vget(bconf_root, "*" , "adreply", "phone_prefix", lang, NULL)),
					  phone);
			}

			BPAPI_INIT(&ba, NULL, pgsql);
			bpapi_insert(&ba, "id", cmd_getval(cs, "id"));
			bpapi_insert(&ba, "email", cmd_getval(cs, "email"));
			bpapi_insert(&ba, "body", state->body ? state->body : cmd_getval(cs, "adreply_body"));
			if (phone && *phone)
				bpapi_insert(&ba, "phone", phone);
			bpapi_insert(&ba, "name", cmd_getval(cs, "name"));
			bpapi_insert(&ba, "remote_addr", cmd_getval(cs, "remote_addr"));
			if (bconf_vget(bconf_root, "*" ,"adreply" ,"subject_prefix", lang, NULL))
				bpapi_insert(&ba, "subj_prefix", bconf_value(bconf_vget(bconf_root, "*" ,"adreply" ,"subject_prefix", lang, NULL)));
			if (cmd_haskey(cs, "storereply")) {
				bpapi_insert(&ba, "template_name", "mail/storereply.txt");
				bpapi_insert(&ba, "mail_type", "storereply");
			}
			else {
				bpapi_insert(&ba, "template_name", "mail/adreply.mime");
				bpapi_insert(&ba, "mail_type", "adreply");
			}

			sql_worker_template_query(&config.pg_master, "sql/call_insert_mail_queue.sql", &ba,
						  adreply_fsm, state, cs->ts->log_string);

			bpapi_output_free(&ba);
			bpapi_simplevars_free(&ba);
			bpapi_vtree_free(&ba);

			state->state = ADREPLY_CHECK;
			return NULL;
		}

		case ADREPLY_CHECK:
		{
			char *cmd;
			worker->next_row(worker);
			state->mail_queue_id = atoi(worker->get_value(worker, 0));
			xasprintf(&cmd, "cmd:flushmail\napplication:adreply\nmail_id:%d\ncommit:1\n", state->mail_queue_id);
			at_register_cmd(cmd, bconf_get_int(bconf_root, "*.adreply.spam.minutes") * 60, "adreply", cmd_getval(cs, "id"), 0);
			free(cmd);
			/* Send a copy */
			if (cmd_haskey(cs, "cc") && atoi(cmd_getval(cs, "cc"))) {
				xasprintf(&cmd, "cmd:flushmail\napplication:adreply_cc\nmail_id:%d\ncopy:1\nemail:%s\nnofilter:1\ncommit:1\n", state->mail_queue_id, cmd_getval(cs, "email"));
				at_register_cmd(cmd, 10, "adreply", cmd_getval(cs, "id"), 0);
				free(cmd);
			}
			state->state = ADREPLY_LOG_REDIR;
			continue;
		}

		case ADREPLY_LOG_REDIR:
			syslog_ident("redir", "%s ad_reply %s", 
				cmd_getval(cs, "redir_code") ?: bconf_get_string(host_bconf, "trans.redir_unknown_code"),
				cmd_getval(state->cs, "remote_addr"));
			transaction_logprintf(state->cs->ts, D_INFO, "REDIR: %s ad_reply %s",
					      cmd_getval(cs, "redir_code") ?: bconf_get_string(host_bconf, "trans.redir_unknown_code"),
					      cmd_getval(state->cs, "remote_addr"));

			state->state = ADREPLY_DONE;

		case ADREPLY_DONE:
		{
			/* send backend event to queue*/
			int be_enabled = bconf_get_int(trans_bconf, "adreply.backend_event.send_enabled");
			if (be_enabled) {
				struct bpapi be;
				const char *phone = cmd_getval(cs, "phone");
				BPAPI_INIT(&be, NULL, pgsql);
				transaction_logprintf(cs->ts, D_INFO, "SEND ADREPLY BACKEND EVENT ACTIVE");
				bpapi_insert(&be, "type", cmd_haskey(cs, "storereply") ? "storereply":"adreply");
				bpapi_insert(&be, "param_list_id", cmd_getval(cs, "id"));
				const char *prefix = "param_";
				for (int col = 0; col < worker->fields(worker); col++){
					char *field = (char*) worker->field_name(worker, col);
					char *prefix_field = malloc(strlen(prefix)+strlen(field)+1);
					strcpy(prefix_field, prefix);
					strcat(prefix_field, field);
					bpapi_insert(&be, prefix_field, worker->get_value(worker,col));
					free(prefix_field);
				}	
				bpapi_insert(&be, "param_sender_email", cmd_getval(cs, "email"));
				bpapi_insert(&be, "param_sender_name", cmd_getval(cs, "name"));
				if (phone && *phone)
					bpapi_insert(&be, "param_sender_phone", phone);
				bpapi_insert(&be, "param_body", state->body ? state->body : cmd_getval(cs, "adreply_body"));
				bpapi_insert(&be, "param_has_cc", cmd_haskey(cs, "cc")? cmd_getval(cs, "cc") : "0");
			 	bpapi_insert(&be, "param_remote_addr", cmd_getval(cs, "remote_addr"));
				publish_backend_event(cs, &be);
				bpapi_output_free(&be);
				bpapi_simplevars_free(&be);
				bpapi_vtree_free(&be);
			}
			adreply_done(state);
			return NULL;
		}
		case ADREPLY_ERROR:
			transaction_logprintf(cs->ts, D_INFO, "Error state");
			adreply_done(state);
			return NULL;

		default:
			state->state = ADREPLY_ERROR;
			transaction_logprintf(cs->ts, D_ERROR, "Unknown state (%d)", state->state);
			continue;
		}
	}
}

static void
adreply(struct cmd *cs) {
	struct adreply_state *state = zmalloc(sizeof (*state));

	state->state = ADREPLY_INIT;
	state->cs = cs;

	adreply_fsm(state, NULL, NULL);
}

ADD_COMMAND(adreply, NULL, adreply, parameters, NULL);

