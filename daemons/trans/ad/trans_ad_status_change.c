#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <time.h>

#include "trans.h"
#include "util.h"
#include <ctemplates.h>
#include "validators.h"
#include <bconf.h>
#include <event.h>

#define AD_STATUS_CHANGE_DONE  0
#define AD_STATUS_CHANGE_ERROR 1
#define AD_STATUS_CHANGE_INIT  2
#define AD_STATUS_CHANGE_RESULT 3

struct ad_status_change_state {
	int state;
	struct cmd *cs;
};

static struct c_param parameters[] = {
	{"token", P_REQUIRED, "v_token:adminad.edit_ad"},
	{"remote_addr", P_REQUIRED, "v_ip"},
	{"remote_browser", 0, "v_remote_browser"},
	{"ad_id", P_REQUIRED, "v_id"},
	{"newstatus", P_REQUIRED, "v_array:hidden,active,passive,disabled"},
	{NULL, 0}
};

static const char *ad_status_change_fsm(void *, struct sql_worker *, const char *);


/*****************************************************************************
 * ad_status_change_done
 * Exit function.
 **********/
static void
ad_status_change_done(void *v) {
	struct ad_status_change_state *state = v;
	struct cmd *cs = state->cs;
	free(state);
	cmd_done(cs);
}

/*****************************************************************************
 * bind_insert
 * Initialise template API before a call to sql_worker_template_query().
 **********/
static void
bind_insert(struct bpapi *ba, struct ad_status_change_state *state) {
	struct c_param *param;

	for (param = parameters; param->name; param++) {
		if (cmd_haskey(state->cs, param->name))
			bpapi_insert(ba, param->name, cmd_getval(state->cs, param->name));
	}
}

/*****************************************************************************
 * ad_status_change_fsm
 * State machine
 **********/
static const char *
ad_status_change_fsm(void *v, struct sql_worker *worker, const char *errstr) {
	struct ad_status_change_state *state = v;
	struct cmd *cs = state->cs;
        struct bpapi ba;

	/*
	 * General error handling for db errors.
	 */
	if (errstr) {
                transaction_logprintf(cs->ts, D_ERROR, "ad_status_change_fsm statement failed (%d, %s)",
                                      state->state,
                                      errstr);
		if (strstr(errstr, "ERROR_INVALID_STATUS")) {
			cs->status = "TRANS_ERROR";
			cs->error = "ERROR_INVALID_STATUS";
		} else if (strstr(errstr, "ERROR_AD_NOT_FOUND")) {
			cs->status = "TRANS_ERROR";
			cs->error = "ERROR_AD_NOT_FOUND";
		} else {
			cs->status = "TRANS_DATABASE_ERROR";
			cs->message = xstrdup(errstr);
		}
		state->state = AD_STATUS_CHANGE_ERROR;
        }

	while (1) {
		transaction_logprintf(cs->ts, D_DEBUG, "ad_status_change_fsm(%p, %p, %p), state=%d",
				      v, worker, errstr, state->state);

		switch (state->state) {
		case AD_STATUS_CHANGE_INIT:
			/* next state */
			state->state = AD_STATUS_CHANGE_RESULT;

			/* Initialise the templateparse structure */
			BPAPI_INIT(&ba, NULL, pgsql);
			bind_insert(&ba, state);

			/* Register SQL query with our ad_status_change_fsm() as callback function */
			/* Make sure to switch to slave if read-only query. */
			sql_worker_template_query(&config.pg_master,
						  "sql/ad_status_change.sql", &ba,
						  ad_status_change_fsm, state, cs->ts->log_string);

			/* Clean-up */
			bpapi_output_free(&ba);
			bpapi_simplevars_free(&ba);
			bpapi_vtree_free(&ba);

			/* Return to libevent */
			return NULL;

                case AD_STATUS_CHANGE_RESULT:
		{
			int cols = worker->fields(worker);
			int col;

                        state->state = AD_STATUS_CHANGE_DONE;

                        while (worker->next_row(worker)) {
				for (col = 0; col < cols; col++) {
					transaction_printf(cs->ts, "%s:%s\n", worker->field_name(worker, col) + 2,
							worker->get_value(worker, col));
				}
                        }


			continue;
		}

		case AD_STATUS_CHANGE_DONE:
			ad_status_change_done(state);
			return NULL;

		case AD_STATUS_CHANGE_ERROR:
			transaction_logprintf(cs->ts, D_ERROR, "Error state");
			ad_status_change_done(state);
			return NULL;

		default:
			/* MAJOR STATE ERROR */
			cs->status = "TRANS_ERROR";
			transaction_logprintf(cs->ts, D_ERROR, "Unknown state (%d)",
					      state->state);
			return NULL;

		}
	}
}

/*****************************************************************************
 * ad_status_change
 * Main function that initialise a state-structure and calls the state machine.
 **********/
static void
ad_status_change(struct cmd *cs) {
	transaction_logprintf(cs->ts, D_DEBUG, "Start Ad status change transaction");
	struct ad_status_change_state *state;
	state = zmalloc(sizeof(*state));
	state->cs = cs;
	state->state = AD_STATUS_CHANGE_INIT;

	ad_status_change_fsm(state, NULL, NULL);
}

ADD_COMMAND(ad_status_change,     /* Name of command */
		NULL,
            ad_status_change,     /* Main function name */
            parameters,  /* Parameters struct */
            "Ad status change command");/* Command description */
