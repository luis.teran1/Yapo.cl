#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <time.h>

#include "trans.h"
#include "factory.h"
#include "util.h"
#include "trans_at.h"
#include <ctemplates.h>
#include "trans_mail.h"
#include "validators.h"
#include "settings_utils.h"
#include <bconf.h>
#include "bconfd_client.h"
#include "log_event.h"
#include "trans_newad.h"
#include "pay_type.h"
#include "geoip_backend.h"
#include "utils/bad_words.h"
#include "utils/bad_word_tree.h"
#include "utils/price_alert.h"
#include "utils/send_newad_event.h"
#include "utils/preprocessing_queue.h"

extern struct bconf_node *bconf_root;

enum newad_state_val {
	NEWAD_DONE,
	NEWAD_BEGIN_INSERT,
	NEWAD_ADMINEDIT,
	NEWAD_MAIL,
	NEWAD_IS_DUPLICATED,
	NEWAD_WHITELIST,
	NEWAD_BUMP_IMPORT_AD,
	NEWAD_SET_FORCE_REVIEW_STATUS,
	NEWAD_UDPATE_ACCOUNT_PROFILE
};

static const char *state_lut[] = {
	"NEWAD_DONE",
	"NEWAD_BEGIN_INSERT",
	"NEWAD_ADMINEDIT",
	"NEWAD_MAIL",
	"NEWAD_IS_DUPLICATED",
	"NEWAD_WHITELIST",
	"NEWAD_BUMP_IMPORT_AD",
	"NEWAD_SET_FORCE_REVIEW_STATUS",
	"NEWAD_UDPATE_ACCOUNT_PROFILE"
};

static char *
format_phone(char *res, const char *phone) {

	int i = 0, j = 0;

	if (phone == NULL)
		return NULL;

	while (phone[i]) {
		if (!isspace(phone[i]) && phone[i] != '-') {
			res[j++] = phone[i];
		}
		i++;
	}

	res[j] = '\0';

	return res;
}

static void
set_category_price(const char *setting, const char *key, const char *value, void *cbdata) {
	struct cmd *cs = cbdata;
	transaction_logprintf(cs->ts, D_INFO, "(INFO) set_category_price: key %s, value %s", key, value);

	if (!strcmp(key, "price") && value)
		cmd_setval(cs, "category_price", xstrdup(value));
}

static const char *
is_pro_for_category(struct cmd *cs) {
	const char *category = cmd_getval(cs, "category");
	const char *email = cmd_getval(cs, "email");
	char *tr_out = transaction_internal_output_printf(
		"cmd:get_account\n"
		"email:%s\n"
		"commit:1\n"
		"end\n",
		email
	);

	char *is_pro_for = strstr(tr_out, "is_pro_for:");
	if (!is_pro_for) {
		free(tr_out);
		return "0";
	}

	char *endl = strchr(is_pro_for, '\n');
	if (endl)
		*endl = '\0';

	const char *result = "0";
	char *saveptr;
	char *cat = strtok_r(is_pro_for + strlen("is_pro_for:"), ",", &saveptr);
	while (cat) {
		if (!strcmp(cat, category)) {
			result = "1";
			break;
		}
		cat = strtok_r(NULL, ",", &saveptr);
	}

	free(tr_out);
	return result;
}

static const char*
price_key_lookup(const char *setting, const char *key, void *cbdata) {
	struct cmd *cs = cbdata;

	if (strcmp(key, "action") == 0) {
		/* edit/addimages/refused */
		const char *val = cmd_getval(cs, "ad_type");

		if (val == NULL)
			return "";

		if (strcmp(val, "refused") == 0 && cmd_haskey(cs, "pre_refuse_action_type"))
			return cmd_getval(cs, "pre_refuse_action_type");

		return val;
	} else if (strcmp(key, "parent") == 0) {
		return bconf_value(bconf_vget(bconf_root, "*.*.cat", cmd_getval(cs, "category"), "parent", NULL));
	} else if (strcmp(key, "category") == 0) {
		return cmd_getval(cs, "category");
	} else if (strcmp(key, "pro_for_category") == 0) {
		// TODO: Fix this blatant lie (probably getting the account)
		return is_pro_for_category(cs);
	}

	return NULL;
}

static int
get_cat_price(struct cmd *cs) {
	if (!cmd_haskey(cs, "category_price")) {
		struct bpapi_vtree_chain vtree;
		get_settings(bconf_vtree(&vtree, bconf_get(bconf_root, "*.*.category_settings")), "price", price_key_lookup, set_category_price, cs);
		vtree_free(&vtree);
	}
	return atoi(cmd_getval(cs, "category_price") ?: "-1");
}

/* get_gallery_price() shouldn't be used to detect if 'gallery allowed' */
static int
get_gallery_price(struct cmd *cs) {
	int gallery_price = 0;
	/* We're getting a string because we want to be able to configure a gallery to cost zero */
	char *gallery_price_str = get_setting_keyval_string(cs, "*.*.gallery_settings", "gallery_price", "price");
	if (gallery_price_str) {
		gallery_price = atoi(gallery_price_str);
		free(gallery_price_str);
	} else {
		/* No specific gallery price defined, possibly fall back to category price plus premium from bconf */
		const char *cpp = bconf_get_string(bconf_root, "*.*.common.gallery.category_price_plus");
		if (cpp)
			gallery_price = get_cat_price(cs) + atoi(cpp);
	}
	return gallery_price;
}

static int
get_extra_images_price(struct cmd *cs) {
	if (cmd_haskey(cs, "addimages") ||
	    (cmd_haskey(cs, "image_set") && strcmp(cmd_getval(cs, "image_set"), "1") == 0)) {
		/* get price from settings */
		return get_setting_keyval_value(cs, NULL, "extra_images", "price");
	}

	return 0;
}

/* Whitelist start */

typedef enum {
	WHITELIST_RULE_NONE = 0,
	WHITELIST_RULE_MIN_ADS,
	WHITELIST_RULE_REJECTED_ADS,
	WHITELIST_RULE_NOTED_ADS,
	WHITELIST_RULE_BAD_WORDS,
	WHITELIST_RULE_PRICE_ALERT,
	WHITELIST_RULE_NOT_CHILEAN_IP,
	WHITELIST_RULE_MIN_PRICE
} whitelist_rule_t;

static const char *whitelist_rule[] = {
	"None",
	"Less than minimum ads",
	"Rejected ads",
	"Ads with notes",
	"Bad words",
	"Duplicate warning",
	"Price is not green",
	"Not chilean IP",
	"Price less than minimum"
};

static int
is_whitelistable(struct cmd *cs, int on_whitelist, whitelist_rule_t *broken_rule) {
	char *res = transaction_internal_output_printf(
		"cmd:email_is_whitelistable\n"
		"email:%s\n"
		"%s"
		"commit:1\n"
		"end\n",
		cmd_getval(cs, "email"),
		on_whitelist ? "min_ads:0\n" : ""
	);

	int whitelistable = res && strstr(res, "is_whitelistable:t");
	char *reason = strstr(res, "reason:");
	if (reason) {
		reason += 7;
		char *end = strchr(reason, '\n');
		if (end)
			*end = '\0';
		if (strncmp(reason, "LESS_THAN_", 10) == 0) {
			*broken_rule = WHITELIST_RULE_MIN_ADS;
		} else if (strcmp(reason, "ADS_WITH_NOTES") == 0) {
			*broken_rule = WHITELIST_RULE_NOTED_ADS;
		} else if (strcmp(reason, "REJECTED_ADS") == 0) {
			*broken_rule = WHITELIST_RULE_REJECTED_ADS;
		}
	}
	free(res);
	return whitelistable;
}

static int
has_bad_words(struct cmd *cs) {
	char *trans_bad_words = transaction_internal_output_printf("cmd:bad_words\ncommit:1\nend\n"); 
	vector_t *bad_word_trees = parse_bad_words(cs, trans_bad_words);

	int has_sub = text_has_bad_words(cs, bad_word_trees, cmd_getval(cs, "subject"));
	int has_bod = (cmd_haskey(cs, "body") && text_has_bad_words(cs, bad_word_trees, cmd_getval(cs, "body")));
	int has = has_bod || has_sub;

	vector_delete(bad_word_trees, bad_word_tree_delete);
	free(trans_bad_words);
	return has;
}

static int
has_price_alert(struct cmd *cs) {

	int category = atoi(cmd_getval(cs, "category"));
	if (category != 2020)
		return 0;

	int price = cmd_haskey(cs, "price") ? atoi(cmd_getval(cs, "price")) : 0;
	int brand = cmd_haskey(cs, "brand") ? atoi(cmd_getval(cs, "brand")) : 0;
	int model = cmd_haskey(cs, "model") ? atoi(cmd_getval(cs, "model")) : 0;
	int version = cmd_haskey(cs, "version") ? atoi(cmd_getval(cs, "version")) : 0;
	int regdate = cmd_haskey(cs, "regdate") ? atoi(cmd_getval(cs, "regdate")) : 0;
	int fuel = cmd_haskey(cs, "fuel") ? atoi(cmd_getval(cs, "fuel")) : 0;
	int gearbox = cmd_haskey(cs, "gearbox") ? atoi(cmd_getval(cs, "gearbox")) : 0;

	int appraisal = get_appraisal(cs, category, brand, model, version, regdate, fuel, gearbox);
	int alert = price_alert(cs, appraisal, price, category);
	transaction_logprintf(cs->ts, LOG_DEBUG, "Price alert is: %d\n", alert);

	return alert != PRICE_ALERT_OK;
}

static int
is_on_whitelist(struct cmd *cs, struct bpapi *ba) {
	int on_whitelist = 0;
	const char *errstr = NULL;
	bpapi_insert(ba, "list_name", "whitelist");
	struct sql_worker *worker = sql_blocked_template_query("pgsql.master", 0, "sql/call_is_in_list.sql", ba, cs->ts->log_string, &errstr);

	if (worker && sql_blocked_next_result(worker, &errstr) == 1) {
		if (worker->next_row(worker)) {
			const char *in_whitelist = worker->get_value_byname(worker, "is_in_list");
			if (in_whitelist)
				on_whitelist = strcmp(in_whitelist, "t") == 0;

			transaction_logprintf(cs->ts, D_DEBUG, "in_whitelist: %s, on_whitelist: %d\n", in_whitelist, on_whitelist);
		}
	}

	/* Should any error happen, we can just assume the user is not on whitelist */
	if (worker)
		sql_worker_put(worker);

	return on_whitelist;
}

static whitelist_rule_t
get_whitelist_broken_rule(struct cmd *cs, struct bpapi *ba, int on_whitelist) {

	whitelist_rule_t broken_rule = WHITELIST_RULE_NONE;
	is_whitelistable(cs, on_whitelist, &broken_rule);

	if (broken_rule)					return broken_rule;
	if (has_bad_words(cs))				return WHITELIST_RULE_BAD_WORDS;
	if (has_price_alert(cs))			return WHITELIST_RULE_PRICE_ALERT;

	const char *country = cmd_getval(cs, "country");
	const char *expected_country = bconf_get_string(bconf_root, "*.*.ai.whitelist.expected_country");
	if (strcmp(country, expected_country))
		return WHITELIST_RULE_NOT_CHILEAN_IP;

	int price = cmd_haskey(cs, "price") ? atoi(cmd_getval(cs, "price")) : 0;
	int min_price = bconf_get_int(bconf_root, "*.*.ai.whitelist.min_accepted_price");
	if (price < min_price)
		return WHITELIST_RULE_MIN_PRICE;

	return WHITELIST_RULE_NONE;
}

static void 
set_force_review(struct cmd *cs, struct bpapi *ba) {
	const char *ad_id = bpapi_get_element(ba, "ad_id", 0);
	const char *action_id = bpapi_get_element(ba, "action_id", 0);
	transaction_internal_printf(TRANSACTION_INTERNAL_SYNC_CB, NULL,
		"cmd:set_force_review\n"
		"ad_id:%s\n"
		"action_id:%s\n"
		"commit:1\n"
		"end\n",
		ad_id,
		action_id
	);
}

static void
force_queue(struct cmd *cs, struct bpapi *ba, const char* queue) {
	const char *ad_id = bpapi_get_element(ba, "ad_id", 0);
	const char *action_id = bpapi_get_element(ba, "action_id", 0);
	transaction_internal_printf(TRANSACTION_INTERNAL_SYNC_CB, NULL,
		"cmd:set_force_queue\n"
		"ad_id:%s\n"
		"action_id:%s\n"
		"queue:%s\n"
		"commit:1\n"
		"end\n",
		ad_id,
		action_id,
		queue
	);
}

static void
whitelist_add(struct cmd *cs) {
	transaction_internal_printf(TRANSACTION_INTERNAL_SYNC_CB, NULL,
		"cmd:add_to_whitelist\n"
		"email:%s\n"
		"notice:%s\n"
		"commit:1\n"
		"end\n",
		cmd_getval(cs, "email"),
		"Email automatically put into Whitelist in Ad Insert."
	);
}

static void
whitelist_remove(struct cmd *cs, whitelist_rule_t broken_rule) {
	transaction_internal_printf(TRANSACTION_INTERNAL_SYNC_CB, NULL,
		"cmd:remove_from_whitelist\n"
		"email:%s\n"
		"notice:%s%s\n"
		"commit:1\n"
		"end\n",
		cmd_getval(cs, "email"),
		"Automatically removed from Whitelist for AI. REASON: ",
		whitelist_rule[broken_rule]
	);
}

static void
apply_whitelist_rules(struct cmd *cs, struct bpapi *ba) {

	int force_review = 0;
	int on_whitelist = is_on_whitelist(cs, ba);
	whitelist_rule_t broken_rule = get_whitelist_broken_rule(cs, ba, on_whitelist);

	if (!broken_rule) {
		if (on_whitelist) {
			transaction_logprintf(cs->ts, D_INFO, "WHITELIST RESULT: keep [%s] on whitelist", cmd_getval(cs, "email"));
		} else {
			transaction_logprintf(cs->ts, D_INFO, "WHITELIST RESULT: adding [%s] to whitelist", cmd_getval(cs, "email"));
			whitelist_add(cs);
		}
	} else {
		if (on_whitelist) {
			switch (broken_rule) {
			case WHITELIST_RULE_REJECTED_ADS:
				transaction_logprintf(cs->ts, D_INFO, "WHITELIST RESULT: removing [%s] from whitelist, broken rule: %s",
					cmd_getval(cs, "email"), whitelist_rule[broken_rule]);
				whitelist_remove(cs, broken_rule);
				break;
			default:
				transaction_logprintf(cs->ts, D_INFO, "WHITELIST RESULT: keep [%s] on whitelist, forced review, broken rule: %s",
					cmd_getval(cs, "email"), whitelist_rule[broken_rule]);
				force_review = 1;
			}
		} else {
			transaction_logprintf(cs->ts, D_INFO, "WHITELIST RESULT: not adding [%s] to whitelist, broken rule: %s",
				cmd_getval(cs, "email"), whitelist_rule[broken_rule]);
		}
	}

	if (force_review)
		set_force_review(cs, ba);
}

/* Whitelist end */

static void
bump_import_ad(struct cmd *cs, struct bpapi *ba) {
	const char *bump_ad = bpapi_get_element(ba, "o_bump_ad", 0);
	if (bump_ad && !strcmp(bump_ad, "t")) {
		const char *errstr = NULL;
		struct sql_worker *worker = sql_blocked_template_query(
			"pgsql.queue", 0, "sql/insert_bump_import_ad.sql", ba, cs->ts->log_string, &errstr
		);

		if (worker && sql_blocked_next_result(worker, &errstr) == 1) {
			if (worker->next_row(worker)) {
				transaction_logprintf(cs->ts, D_INFO, "scheduled bump for imported ad");
			}
		}

		/* Should any error happen, we can just assume the user is not on whitelist */
		if (worker)
			sql_worker_put(worker);
	}
}
/*
 *	Search for duplicated ads
 *	Returns:
 *	0 - Duplicated ad wasn't found
 *	1 - Duplicated found
 *	2 - Duplicated PRO found (pack_status:disabled)
 */
static int
is_duplicating(struct cmd *cs, const char *ad_id, const char *ad_status) {
	const char *body = cmd_haskey(cs, "body") ? cmd_getval(cs, "body") : "";
	const char *price = cmd_haskey(cs, "price") ? cmd_getval(cs, "price") : "0";
	const char *currency = cmd_haskey(cs, "currency") ? cmd_getval(cs, "currency") : "peso";
	const char *type = cmd_haskey(cs, "type") ? cmd_getval(cs, "type") : "";
	const char *region = cmd_haskey(cs, "region") ? cmd_getval(cs, "region") : "";

	char uid[32] = {0};
	if (cmd_haskey(cs, "uid"))
		snprintf(uid, 32, "uid:%s\n", cmd_getval(cs, "uid"));

	char *res = transaction_internal_output_printf(
		"cmd:duplicated_ads\n"
		"%s"
		"email:%s\n"
		"ad_category:%s\n"
		"ad_price:%s\n"
		"ad_currency:%s\n"
		"ad_subject:%s\n"
		"blob:%lu:ad_body\n"
		"%s\n"
		"ad_status:%s\n"
		"ad_id:%s\n"
		"type:%s\n"
		"region:%s\n"
		"commit:1\n"
		"end\n",
		uid,
		cmd_getval(cs, "email"),
		cmd_getval(cs, "category"),
		price,
		currency,
		cmd_getval(cs, "subject"),
		strlen(body),
		body,
		ad_status,
		ad_id,
		type,
		region
	);

	int duplicated = res && strstr(res, "has_duplicates:1");
	if (duplicated && strstr(res, "pack_status:disabled")) {
		duplicated = 2;
	}

	free(res);
	return duplicated;
}

static int
check_pack_category(struct cmd *cs, struct bpapi *ba, const char *type) {
	int ret = 0;
	int cat = atoi(cmd_getval(cs, "category"));
	struct bconf_node *node = bconf_vget(bconf_root, "*.*.packs.type", type, "category", NULL);
	int count = bconf_count(node);
	for (int i = 0; i < count; i++) {
		struct bconf_node *bn = bconf_byindex(node, i);
		int cat_bconf = atoi(bconf_key(bn));
		if (cat == cat_bconf) {
			ret = 1;
			break;
		}
	}
	return ret;
}

static int
has_active_pack(struct cmd *cs, struct bpapi *ba) {
	char *res = transaction_internal_output_printf(
		"cmd:get_packs_by_account\n"
		"email:%s\n"
		"commit:1\n"
		"end\n",
		cmd_getval(cs, "email")
	);

	int pack_status = 0;
	int cat = atoi(cmd_getval(cs, "category")) / 1000;
	if (cat == 1) {
		pack_status = res && (strstr(res, "product_id:2") != NULL) && check_pack_category(cs, ba, "2");
		if (pack_status) transaction_logprintf(cs->ts, D_DEBUG, "(newad: has_active_pack) pack inmo user");
	} else if (cat == 2) {
		pack_status = res && (strstr(res, "product_id:1") != NULL) && check_pack_category(cs, ba, "1");
		if (pack_status) transaction_logprintf(cs->ts, D_DEBUG, "(newad: has_active_pack) pack auto user");
	}

	free(res);
	return pack_status;
}

static int
skip_duplicated(struct cmd *cs, struct bpapi *ba) {
	const char *category = cmd_haskey(cs, "category")
		? cmd_getval(cs, "category")
		: "";
	int skip = bconf_vget_int_default(bconf_root, "*.*.cat", category, "ad_queue.skip_duplicated.value", NULL, 0);
	if (skip)
		transaction_logprintf(cs->ts, D_DEBUG, "(newad: skip_duplicated(%s) = 1)", category);
	return skip;
}

static int
check_duplicates(struct cmd *cs, struct bpapi *ba) {
	const char *ad_id = bpapi_get_element(ba, "o_ad_id", 0);
	int is_pack = has_active_pack(cs, ba);
	int skip_duplicated_by_cat = skip_duplicated(cs, ba);
	int active_duplicated_enabled = is_pack? 0 : bconf_get_int(bconf_root, "*.*.delete2bump.active.duplicate.enable");
	int inactive_duplicated_enable = is_pack? 0 : bconf_get_int(bconf_root, "*.*.delete2bump.inactive.duplicate.enable");
	int disabled_duplicated_enabled = bconf_get_int(bconf_root, "*.*.delete2bump.disabled.duplicate.enable");
	int deactivated_duplicated_enabled = bconf_get_int(bconf_root, "*.*.delete2bump.deactivated.duplicate.enable");
	int duplicating = 0;

	if (skip_duplicated_by_cat) {
		transaction_logprintf(cs->ts, D_INFO,
			"(newad: skip duplicate by category configuration)  skip ad_id = %s - Skipping active/inactive duplicate check",
			ad_id);
		return 0;
	}
	if (is_pack) {
		transaction_logprintf(cs->ts, D_INFO, "(newad: NEWAD_IS_DUPLICATED) pack user, email = [%s] - Skipping active/inactive duplicate check",
			cmd_getval(cs, "email"));
	}

	if (disabled_duplicated_enabled) {
		duplicating = is_duplicating(cs, ad_id, "disabled");
		if (duplicating)
			transaction_logprintf(cs->ts, D_INFO, "(newad: NEWAD_IS_DUPLICATED) ad_id = %s check_duplicates: Duplicated by disabled/delete2bump", ad_id);
	}
	if (deactivated_duplicated_enabled && !duplicating) {
		duplicating = is_duplicating(cs, ad_id, "deactivated");
		if (duplicating)
			transaction_logprintf(cs->ts, D_INFO, "(newad: NEWAD_IS_DUPLICATED) ad_id = %s check_duplicates: Duplicated by deactivated/delete2bump", ad_id);
	}
	if (inactive_duplicated_enable && !duplicating) {
		duplicating = is_duplicating(cs, ad_id, "inactive");
		if (duplicating)
			transaction_logprintf(cs->ts, D_INFO, "(newad: NEWAD_IS_DUPLICATED) ad_id = %s check_duplicates: Duplicated by inactive", ad_id);
	}
	if (duplicating) {
		const char *queue_duplicated = bconf_get_string(bconf_root, "*.controlpanel.modules.adqueue.filter.25.name");
		const char *queue_pro = bconf_get_string(bconf_root, "*.controlpanel.modules.adqueue.filter.26.name");
		force_queue(cs, ba, duplicating==2?queue_pro:queue_duplicated);
		return 1;
	}
	if (active_duplicated_enabled) {
		duplicating = is_duplicating(cs, ad_id, "active");
	}
	if (duplicating) {
		const char *queue_active_duplicated = bconf_get_string(bconf_root, "*.controlpanel.modules.adqueue.filter.29.name");
		transaction_logprintf(cs->ts, D_INFO, "(newad: NEWAD_IS_DUPLICATED) ad_id = %s check_duplicates: Duplicated by Active", ad_id);
		force_queue(cs, ba, queue_active_duplicated);
		return 1;
	}
	return 0;
}

static void
update_account_profile(struct cmd *cs, struct bpapi *ba) {
	if (cmd_haskey(cs, "communes")) {
		transaction_internal_printf(TRANSACTION_INTERNAL_SYNC_CB, NULL,
			"cmd:set_commune_if_missing\n"
			"email:%s\n"
			"region:%s\n"
			"commune:%s\n"
			"commit:1\n"
			"end\n",
			cmd_getval(cs, "email"),
			cmd_getval(cs, "region"),
			cmd_getval(cs, "communes")
		);
	}
}

static struct c_param parameters[] = {
	{"region",                 P_REQUIRED, "v_region"},
	{"category",               P_REQUIRED, "v_category_check"},
	{"communes",               P_REQUIRED, "v_commune"},
	{"name",                   P_REQUIRED, "v_name"},
	{"multi_email",            0,          "v_string_isprint"},
	{"phone_hidden",           0,          "v_phone_hidden"},
	{"phone_type",             0,          "v_phone_hidden"},
	{"phone",                  P_REQUIRED, "v_phone_strict"},
	{"company_ad",             P_REQUIRED, "v_company_ad"},
	{"subject",                P_REQUIRED, "v_subject"},
	{"body",                   0,          "v_body"},
	{"infopage",               0,          "v_infopage"},
	{"infopage_title",         0,          "v_infopage_title"},
	{"currency",               0,          "v_currency"},
	{"price",                  0,          "v_currency_check"},
	{"old_price",              0,          "v_price"},
	{"redir_code",             0,          "v_redir_code"},
	{"remote_addr",            0,          "v_ip"},
	{"remote_browser",         0,          "v_remote_browser"},
	{"image",                  P_MULTI,    "v_imagefile"},
	{"image_set",              0,          "v_bool"},
	{"addimages",              0,          "v_bool"},
	{"thumbnail_digest",       P_MULTI,    "v_string_isprint"},
	{"digest_present",         P_MULTI,    "v_bool"},
	{"store",                  0,          "v_id"},
	{"ad_type",                0,          "v_string_isprint"},
	{"uid",                    0,          "v_uid"},
	{"pre_refuse_action_type", 0,          "v_string_isprint"},
	{"hide_address",           0,          "v_hide_address"},
	{"gallery",                0,          "v_gallery"},
	{"addgallery",             0,          "v_bool"},
	{"lang",                   P_REQUIRED, "v_lang"},
	{"type",                   P_REQUIRED, "v_type"},
	{"estate_type",            0,          "v_estate_type"},

	/* For administrators */
	{"token",                  0,          "v_token:adminad.edit_ad"},
	{"do_not_send_mail",       0,          "v_bool"},
	{"list_id",                0,          "v_id"},
	{"ad_id",                  0,          "v_id"},

	/* For ad import */
	{"external_ad_id",         0,          "v_external_ad_id:limited"},
	{"digest",                 0,          "v_string_isprint"},
	{"list_time",              0,          "v_string_isprint"},
	{"link_type",              0,          "v_link_type_limited"},

	{"category",               0,          "v_category_text_warning:subject,body"},
	{"category",               0,          "v_category_change_block"},
	{"email",                  P_REQUIRED, "v_newad_email"},
	{"_post",                  0,          "v_filter:subject,body,email,remote_addr,name,infopage_title,phone,category,uid"},

	/* action_params */
	{"prev_action_id",         0,          "v_prev_action_id"},
	{"source",                 0,          "v_source"},
	{"cleared_by",             0,          "v_cleared_by"},        /* unused */
	{"campaign_id",            0,          "v_newad_campaign_id"}, /* unused */

	/* Not to be stored in the DB */
	{"state",                  0,          "v_integer"},

	{"_params",                0,          "v_newad_params"},
	{"_passwd",                0,          "v_newad_passwd_extra"},

	{NULL}
};

/* Newad/Review passwd validation */

struct c_param v_extra_email = {"email", P_REQUIRED, "v_email"};

static void
vf_check_account_passwd_cb(struct cmd_param *param, struct sql_worker *worker, const char *arg) {
	if (worker->next_row(worker)) {
		/* The output has this format:
		 0 : password (The value -1 means active account was not found)
		 1 : account_id (This have the same behaviour as password)
		 2 : account_status (Could be null if account was not found)
		 3 : is_pro_for (The value -1 means is not pro)
		*/
		const char *passwd_acc = worker->get_value(worker, 0);
		const char *is_pro_for = worker->get_value(worker, 3);

		int passwd_multi_allowed = 0;
		if (cmd_haskey(param->cs, "category") && !strcmp(is_pro_for, "-1")) {
			// User is not pro
			passwd_multi_allowed = bconf_vget_int(
					trans_bconf, "cat",
					cmd_getval(param->cs, "category"),
					"passwd_multi", "allowed", NULL);
		}

		if (strcmp(passwd_acc, "-1")) {
			// Has account
			if (strcmp(passwd_acc, param->value) && !passwd_multi_allowed) {
				// password doesn't match
				param->error = "ERROR_ACCOUNT_LOGIN_FAILED";
				param->cs->status = "TRANS_ERROR";
			}
		} else if (arg && !strcmp(arg, "force")) {
			// category requires account
			param->error = "ERROR_ACCOUNT_REQUIRED";
			param->cs->status = "TRANS_ERROR";
		}
	} else {
		// This looks like an impossible case
		transaction_printf(param->cs->ts, "account_password:0\n");
	}
}

static int
vf_check_account_passwd(struct cmd_param *param, const char *arg) {

	validate_on_db(param, arg,
		"email", cmd_getval(param->cs, "email"),
		"pgsql.master", "sql/check_account_passwd.sql",
		NULL, NULL, vf_check_account_passwd_cb
	);

	cmd_extra_param(param->cs, &v_extra_email);
	return 0;
}

struct validator v_passwd_account[] = {
	VALIDATOR_REQUIRED_EXECUTION("ERROR_PASSWORD_MISSING"),
	VALIDATOR_REGEX("^\\$[0-9]+\\$.{16}[0-9A-Fa-f]{40}$", REG_EXTENDED, "ERROR_PASSWORD_INVALID"),
	VALIDATOR_OTHER("email", P_REQUIRED, "v_email"),
	VALIDATOR_FUNC(vf_check_account_passwd),
	{0}
};
ADD_VALIDATOR(v_passwd_account);

static void
vf_newad_check_account_cb(struct cmd_param *param, struct sql_worker *worker, const char *arg) {
	if (worker->next_row(worker)) {
		transaction_printf(param->cs->ts, "account:%s\n", worker->get_value(worker, 0));
	} else {
		transaction_printf(param->cs->ts, "account:0\n");
	}
}

static int
vf_newad_check_account(struct cmd_param *param, const char *arg) {

	validate_on_db(param, arg,
		"email", param->value,
		"pgsql.master", "sql/newad_check_account.sql",
		NULL, NULL, vf_newad_check_account_cb
	);

	cmd_extra_param(param->cs, &v_extra_email);
	return 0;
}

static struct c_param passwd_param = {"passwd", 0, "v_passwd_account"};
static struct c_param passwd_optional_param = {"passwd", 0, "v_passwd_optional"};
static struct c_param passwd_mandatory_param = {"passwd", 0, "v_passwd_account:force"};

static int
vf_newad_passwd_extra(struct cmd_param *param, const char *arg) {
	struct cmd *cs = param->cs;

	if (cmd_haskey(cs, "addgallery"))
		return 0;
	if (cmd_haskey(cs, "addimages"))
		return 0;
	if (cmd_haskey(cs, "store"))
		return 0;

	int mandatory_account = cmd_haskey(cs, "category")
		? bconf_vget_int(trans_bconf, "cat", cmd_getval(cs, "category"), "passwd", "required", NULL)
		: 0;

	if (cmd_haskey(cs, "ad_type") && strcmp(cmd_getval(cs, "ad_type"), "new") != 0) {
		/* If ad is not new, password is optional */
		cmd_extra_param(param->cs, &passwd_optional_param);
	} else if (cmd_haskey(cs, "link_type")) {
		/* If imported, passwd is optional */
		cmd_extra_param(param->cs, &passwd_optional_param);
	} else if (mandatory_account) {
		cmd_extra_param(param->cs, &passwd_mandatory_param);
	} else {
		cmd_extra_param(param->cs, &passwd_param);
	}

	return 0;
}

struct validator v_newad_email[] = {
	VALIDATOR_FUNC(vf_newad_check_account),
	VALIDATOR_FUNC(vf_newad_passwd_extra),
	{0}
};
ADD_VALIDATOR(v_newad_email);

static void
vf_category_change_block_populate_bpapi(struct cmd_param *param, const char *arg, struct bpapi *ba) {
	if (cmd_haskey(param->cs, "ad_id"))
		bpapi_insert(ba, "ad_id",   cmd_getval(param->cs, "ad_id"));
	if (cmd_haskey(param->cs, "list_id"))
		bpapi_insert(ba, "list_id", cmd_getval(param->cs, "list_id"));
}

static void
vf_category_change_block_cb(struct cmd_param *param, struct sql_worker *worker, const char *arg) {
	if (worker->next_row(worker)) {
		const char *old_category = worker->get_value(worker, 0);
		const char *new_category = cmd_getval(param->cs, "category");
		transaction_logprintf(param->cs->ts, D_DEBUG, "change block: old_category %s, new_category %s", old_category, new_category);
		if (strcmp(old_category, new_category)) {
			// Category changed
			int blocked_from = bconf_vget_int(trans_bconf, "edit_category_block.from", old_category, NULL);
			int blocked_to = bconf_vget_int(trans_bconf, "edit_category_block.to", new_category, NULL);
			transaction_logprintf(param->cs->ts, D_DEBUG, "change block: blocked_from %d, blocked_to %d", blocked_from, blocked_to);
			if (blocked_from) {
				param->error = "ERROR_CANT_CHANGE_CAT_FROM";
				param->cs->status = "TRANS_ERROR";
			} else if (blocked_to) {
				param->error = "ERROR_CANT_CHANGE_CAT_TO";
				param->cs->status = "TRANS_ERROR";
			}
		}
	}
}

static int
vf_category_change_block(struct cmd_param *param, const char *arg) {
	int is_regular_edit = !cmd_haskey(param->cs, "token")
		&& cmd_haskey(param->cs, "ad_type") && !strcmp(cmd_getval(param->cs, "ad_type"), "edit")
		&& (cmd_haskey(param->cs, "ad_id") || cmd_haskey(param->cs, "list_id"));

	if (is_regular_edit) {
		validate_on_db(param, arg,
			NULL, NULL,
			"pgsql.master", "sql/check_category_change_block.sql",
			vf_category_change_block_populate_bpapi, NULL, vf_category_change_block_cb
		);
	}
	return 0;
}

struct validator v_category_change_block[] = {
	VALIDATOR_FUNC(vf_category_change_block),
	{0}
};
ADD_VALIDATOR(v_category_change_block);

struct validator v_newad_passwd_extra[] = {
        VALIDATOR_FUNC(vf_newad_passwd_extra),
        {0}
};
ADD_VALIDATOR(v_newad_passwd_extra);


static void
vf_cleared_by_cb(struct cmd_param *param, struct sql_worker *worker, const char *arg) {
	if (worker->next_row(worker)) {
		if (!worker->is_true(worker, 0)) {
			param->error = "ERROR_CLEARED_BY_CHECK_FAILED";
			param->cs->status = "TRANS_ERROR";
		}
	}
}

static int
vf_cleared_by(struct cmd_param *param, const char *arg) {
	char *campaign;
	struct tm tm = {0};
	time_t t = 0;
	const char *endtime;
	struct cmd_param *pay_type_param;

	campaign = get_setting_keyval_string(param->cs, NULL, "free_ad_campaign", "valid_for");
	if (!campaign) {
		/* XXX parameter to get error should perhaps be dynamic. */
		struct cmd_param *cat_param = cmd_getparam (param->cs, "category");
		if (cat_param && !cat_param->error)
			cat_param->error = "ERROR_CLEARED_BY_NOT_ALLOWED";
		param->cs->status = "TRANS_ERROR";
		return 0;
	}

	/* Skip the rest of the checks for refused ads since they already passed it once. */
	if (cmd_haskey(param->cs, "ad_type") && strcmp(cmd_getval(param->cs, "ad_type"), "refused") == 0) {
		free(campaign);
		return 0;
	}

	if (bconf_vget_int (bconf_root, "*.*.free_ad_campaign", campaign, "enabled", NULL)) {
		endtime = bconf_vget_string (bconf_root, "*.*.free_ad_campaign", campaign, "end", NULL);

		if (endtime) {
			strptime(endtime, "%Y-%m-%d %H:%M:%S", &tm);
			t = mktime(&tm);
		}
	}

	free (campaign);
	if (t < time(NULL)) {
		param->error = "ERROR_CLEARED_BY_EXPIRED";
		param->cs->status = "TRANS_ERROR";
		return 0;
	}

	pay_type_param = cmd_getparam (param->cs, "pay_type");
	if (pay_type_param && lookup_pay_type (pay_type_param->value, pay_type_param->value_size) != PT_CAMPAIGN) {
		param->error = "ERROR_CLEARED_BY_NOT_ALLOWED";
		param->cs->status = "TRANS_ERROR";
		return 0;
	}

	validate_on_db(param, arg,
		"cleared_by", param->value,
		"pgsql.slave", "sql/check_cleared_by.sql",
		NULL, NULL, vf_cleared_by_cb
	);

	return 0;
}

struct validator v_cleared_by[] = {
	VALIDATOR_INT(1, -1, "ERROR_CLEARED_BY_INVALID"),
	VALIDATOR_FUNC(vf_cleared_by),
	{0}
};
ADD_VALIDATOR(v_cleared_by);

struct validator v_newad_campaign_id[] = {
	VALIDATOR_REGEX("start_package", REG_EXTENDED, "ERROR_CAMPAIGN_ID_INVALID"),
	{0}
};
ADD_VALIDATOR(v_newad_campaign_id);

static int
send_mail(struct cmd *cs, struct bpapi *ba) {

	const char *ad_id = bpapi_get_element(ba, "o_ad_id", 0);
	const char *action_id = bpapi_get_element(ba, "o_action_id", 0);
	const char *_prepaid = bpapi_get_element(ba, "o_prepaid", 0);
	int prepaid = _prepaid && atoi(_prepaid);

	/* If pay_type is card queue up a phone pay mail incase the user times out */
	if (!cmd_haskey(cs, "pay_type") || lookup_pay_type (cmd_getval(cs, "pay_type"), -1) != PT_CARD) {
		char *optional = NULL;

		if (cmd_haskey(cs, "pay_type")) {
			xasprintf(&optional, "pay_type:%s\n", cmd_getval(cs, "pay_type"));
		}

		/* Mail */
		transaction_logprintf(cs->ts, D_DEBUG, "Creating email to send");

		struct internal_cmd *cmd = zmalloc(sizeof (*cmd));
		xasprintf(&cmd->cmd_string,
			"cmd:admail\n"
			"log_string:%s\n"
			"commit:1\n"
			"ad_id:%s\n"
			"action_id:%s\n"
			"mail_type:%s\n"
			"remote_addr:%s\n"
			"%s",
			cs->ts->log_string,
			ad_id,
			action_id,
			lookup_pay_type(cmd_getval(cs, "pay_type"), -1) == PT_VOUCHER
				? "voucher_used"
				: prepaid
					? "free"
					: lookup_pay_type(cmd_getval(cs, "pay_type"), -1) == PT_FREE
						? "free"
						: "payinfo",
			cmd_haskey(cs, "remote_addr") ? cmd_getval(cs, "remote_addr") : "127.0.0.1",
			optional ? optional : ""
		);
		/* Send the mail directly */
		transaction_logprintf(cs->ts, D_DEBUG, "Sending mail..");
		transaction_internal(cmd);
		free(optional);
	}

	if (!prepaid && lookup_pay_type(cmd_getval(cs, "pay_type"), -1) != PT_FREE) {
		int timeout = bconf_get_int_default(bconf_root, "*.*.common.mail.remind_unpaid.seconds", 43200);

		char *cmd_string;
		xasprintf(&cmd_string,
			"cmd:admail\n"
			"commit:1\n"
			"ad_id:%s\n"
			"action_id:%s\n"
			"mail_type:%s\n",
			ad_id,
			action_id,
			"pay_reminder"
		);

		transaction_logprintf(cs->ts, D_DEBUG,
			"Registered remind_unpaid %d seconds from now",
			timeout
		);

		char *info = NULL;
		xasprintf (&info, "%s-%s", ad_id, action_id);
		at_register_cmd(cmd_string, timeout, "payinfo", info, 0);
		free(cmd_string);
		free(info);
	}

	return 0;
}

void
bind_newad_params(struct bpapi *ba, struct cmd *cs) {

	int i;
	int count = bconf_count(bconf_get(bconf_root, "*.*.common.ad_params"));

	for (i = 0; i < count; i++) {
		struct bconf_node *adparam = bconf_byindex(bconf_get(bconf_root, "*.*.common.ad_params"), i);
		const char *name = bconf_get_string(adparam, "name");
		struct bconf_node *list = bconf_get(adparam, "list");

		if (list) {
			int namelen = strlen(name);
			char namebuf[namelen + 3];
			int from = bconf_get_int(list, "from");
			int to = bconf_get_int(list, "to");
			int j;

			for (j = from; j <= to; j++) {
				snprintf(namebuf, namelen + 3, "%s%d", name, j);

				if (cmd_haskey(cs, namebuf)) {
					const char *value = cmd_getval(cs, namebuf);
					bpapi_insert(ba, "param_name", namebuf);
					bpapi_insert(ba, "param_value", value);
				}
			}
		} else if (name && cmd_haskey(cs, name)) {
			const char *value = cmd_getval(cs, name);
			bpapi_insert(ba, "param_name", name);
			bpapi_insert(ba, "param_value", value);
		}
	}

	count = bconf_count(bconf_get(bconf_root, "*.*.common.action_params"));

	for (i = 0; i < count; i++) {
		const char *name = bconf_value(bconf_get(bconf_byindex(bconf_get(bconf_root, "*.*.common.action_params"), i), "name"));
		if (name && cmd_haskey(cs, name)) {
			const char *value = cmd_getval(cs, name);
			bpapi_insert(ba, "action_param_name", name);
			bpapi_insert(ba, "action_param_value", value);
		}
	}

}

static int
bind_insert(struct cmd *cs, struct bpapi *ba) {
	const char *type;
	int i = 0;
	int max_extra_images = 0;
	char price[12];
	int cat_price;

	if (cmd_haskey(cs, "category")) {
		max_extra_images =  get_setting_keyval_value(cs, NULL, "extra_images", "max");
		transaction_logprintf(cs->ts, D_DEBUG, "Allowing up to %d extra images.\n", max_extra_images);
	}

	bpapi_insert(ba, "email", cmd_getval(cs, "email"));
	bpapi_insert(ba, "ads_name", cmd_getval(cs, "name"));
	bpapi_insert(ba, "ads_phone", cmd_getval(cs, "phone"));
	bpapi_insert(ba, "ads_region", cmd_getval(cs, "region"));
	bpapi_insert(ba, "ads_city", "0");	/* unused */

	if (cmd_haskey(cs, "uid"))
		bpapi_insert(ba, "uid", cmd_getval(cs, "uid"));

	if (cmd_haskey(cs, "token"))
		bpapi_insert(ba, "token", cmd_getval(cs, "token"));

	type = cmd_getval(cs, "type");
	switch (type[0]) {
	case 'k':
		bpapi_insert(ba, "ads_type", "buy");
		break;
	case 's':
		bpapi_insert(ba, "ads_type", "sell");
		break;
	case 'u':
		bpapi_insert(ba, "ads_type", "let");
		break;
	case 'h':
		bpapi_insert(ba, "ads_type", "rent");
		break;
	case 'b':
		bpapi_insert(ba, "ads_type", "swap");
		break;
	}

	if (cmd_haskey(cs, "prev_action_id"))
		bpapi_insert(ba, "previous_action_id", cmd_getval(cs, "prev_action_id"));

	if (cmd_haskey(cs, "list_id"))
		bpapi_insert(ba, "list_id", cmd_getval(cs, "list_id"));

	if (cmd_haskey(cs, "ad_id"))
		bpapi_insert(ba, "ad_id", cmd_getval(cs, "ad_id"));

	bpapi_insert(ba, "ads_category", cmd_getval(cs, "category"));
	if (cmd_haskey(cs, "passwd"))
		bpapi_insert(ba, "ads_passwd", cmd_getval(cs, "passwd"));

	if (cmd_haskey(cs, "phone_hidden") && atoi(cmd_getval(cs, "phone_hidden")))
		bpapi_insert(ba, "phone_hidden", "1");

	if (cmd_haskey(cs, "phone_type") && atoi(cmd_getval(cs, "phone_type")))
		bpapi_insert(ba, "phone_type", "1");

	if (cmd_haskey(cs, "company_ad") && atoi(cmd_getval(cs, "company_ad")))
		bpapi_insert(ba, "company_ad", "1");

	bpapi_insert(ba, "ads_subject", cmd_getval(cs, "subject"));
	if (cmd_haskey(cs, "body"))
		bpapi_insert(ba, "ads_body", cmd_getval(cs, "body"));

	if (cmd_haskey(cs, "price"))
		bpapi_insert(ba, "ads_price", cmd_getval(cs, "price"));

	if (cmd_haskey(cs, "old_price"))
		bpapi_insert(ba, "old_price", cmd_getval(cs, "old_price"));

	if (cmd_haskey(cs, "infopage"))
		bpapi_insert(ba, "ads_infopage", cmd_getval(cs, "infopage"));

	if (cmd_haskey(cs, "infopage_title"))
		bpapi_insert(ba, "ads_infopage_title", cmd_getval(cs, "infopage_title"));

	if (cmd_haskey(cs, "image")) {
		struct cmd_param *ip = cmd_getparam(cs, "image");
		struct cmd_param *tp = cmd_getparam(cs, "thumbnail_digest");
		struct cmd_param *dp = cmd_getparam(cs, "digest_present");

		/* Add images */
		for (i = 0; i <= max_extra_images && ip; i++, ip = cmd_nextparam(cs, ip)) {
			bpapi_insert(ba, "ad_images", ip->value);
			bpapi_insert(ba, "ads_thumb_digest",tp ?  tp->value: "0");
			bpapi_insert(ba, "ads_image_had_digest", dp ? dp->value: "0");
			if (tp) {
				tp = cmd_nextparam(cs, tp);
			}
			if (dp) {
				dp = cmd_nextparam(cs, dp);
			}
		}
	}

	bind_newad_params(ba, cs);

	if (cmd_haskey(cs, "remote_addr"))
		bpapi_insert(ba, "remote_addr", cmd_getval(cs, "remote_addr"));

	if (cmd_haskey(cs, "pay_type"))
		bpapi_insert(ba, "pay_type", cmd_getval(cs, "pay_type"));

	cat_price = get_cat_price(cs);
	if (cat_price == -1 && !cmd_haskey(cs, "addimages") && !(cmd_haskey(cs, "ad_type") && lookup_pay_type(cmd_getval(cs, "pay_type"), -1) != PT_FREE)) {
		cs->error = "ERROR_NO_PRICE_FOR_CATEGORY";
		transaction_logprintf(cs->ts, D_ERROR, "(CRIT) No price for category %s!", cmd_getval(cs, "category"));
		return 1;
	}

	if (cat_price < 0)
		cat_price = 0;

	snprintf(price, sizeof(price), "%u", cat_price);
	bpapi_insert(ba, "cat_price", price);
	snprintf(price, sizeof(price), "%u", get_extra_images_price(cs));
	bpapi_insert(ba, "extra_images_price", price);

	if (cmd_haskey(cs, "gallery")) {
		snprintf(price, sizeof(price), "%u", get_gallery_price(cs));
		bpapi_insert(ba, "gallery_price", price);
	}

	if (cmd_haskey(cs, "redir_code")) {
		bpapi_insert(ba, "redir_code", cmd_getval(cs, "redir_code"));
	} else {
		bpapi_insert(ba, "redir_code", bconf_get_string(host_bconf, "trans.redir_unknown_code"));
	}

	if (cmd_haskey(cs, "external_ad_id")  && cmd_haskey(cs, "link_type")) {
		bpapi_insert(ba, "link_type", cmd_getval(cs, "link_type"));
		bpapi_insert(ba, "external_ad_id", cmd_getval(cs, "external_ad_id"));
		if (cmd_haskey(cs, "digest"))
			bpapi_insert(ba, "digest", cmd_getval(cs, "digest"));
		if (cmd_haskey(cs, "list_time"))
			bpapi_insert(ba, "list_time", cmd_getval(cs, "list_time"));
		bpapi_insert(ba, "action_type", "import");
		// check if its an import from a category that requires review
		const char *category = cmd_getval(cs, "category");
		char *import_review_bconf;
		xasprintf(&import_review_bconf, "newad.import.import_review_cat.%s", category);
		if (bconf_get_int_default(trans_bconf, import_review_bconf, 0) != 0) {
			bpapi_insert(
				ba,
				"review_queue",
				bconf_get_string(
					host_bconf,
					"controlpanel.modules.adqueue.default_queue"
				)
			);
		}

	} else if (cmd_haskey(cs, "addimages")) {
		bpapi_insert(ba, "action_type", "extra_images");
	} else if (cmd_haskey(cs, "ad_type")) {
		const char *adtype = cmd_getval(cs, "ad_type");

		if (!strncmp(adtype, "edit", 4))
			bpapi_insert(ba, "action_type", "edit");
		else if (!strncmp(adtype, "renew", 5))
			bpapi_insert(ba, "action_type", "renew");
		else if (strcmp(adtype, "deleted") == 0)
			bpapi_insert(ba, "action_type", "prolong");
		else if (strcmp(adtype, "gallery") == 0)
			bpapi_insert(ba, "action_type", "gallery");	
		else if (strcmp(adtype, "refused") == 0)
			bpapi_insert(ba, "action_type", "editrefused");
		else
			bpapi_insert(ba, "action_type", "new");
	}

	if (cmd_haskey(cs, "store"))
		bpapi_insert(ba, "store", cmd_getval(cs, "store"));

	if (cmd_haskey(cs, "lang"))
		bpapi_insert(ba, "lang", cmd_getval(cs, "lang"));

	bpapi_insert(ba, "param_name", "country");
	bpapi_insert(ba, "param_value", cmd_getval(cs, "country"));

	return 0;
}

static void
bind_country_code(struct cmd *cs) {
	char *country = NULL;
	GeoIP *geoip_obj = get_geoip();

	if (cmd_haskey(cs, "remote_addr") && (geoip_obj != NULL)) {
		const char *_country = GeoIP_country_code_by_addr(geoip_obj, cmd_getval(cs, "remote_addr"));
		country = xstrdup(_country ?: "UNK");
		transaction_logprintf(cs->ts, D_DEBUG, "country for ip %s is %s", cmd_getval(cs, "remote_addr"), country);
	} else {
		country = xstrdup("UNK");
	}

	cmd_setval(cs, "country", country);
}

static void
newad(struct cmd *cs) {

	const char *errstr = NULL;
	struct command_bpapi cba;
	struct timer_instance *ti = NULL;
	struct sql_worker *worker = NULL;
	enum newad_state_val state = NEWAD_BEGIN_INSERT;
	command_bpapi_init(&cba, cs, CMD_BPAPI_HOST, "controlpanel");

	transaction_logprintf(cs->ts, D_DEBUG, "Newad command started");

	/* Modify price, in case it comes in UFs, before sending it to the DB */
	format_price(cs, NULL);
	/* TODO: See if we can safely ignore this step */
	bind_country_code(cs);

	if (bind_insert(cs, &cba.ba)) {
		transaction_logprintf(cs->ts, D_DEBUG, "NEWAD_ERROR");
		goto cleanup;
	}

	while (state != NEWAD_DONE) {
#define TIMER_STATE(st)  if (ti != NULL) timer_end(ti, NULL); ti = timer_start(cs->ti, "NEWAD_"#st)
		if (ti) {
			timer_end(ti, NULL);
			ti = NULL;
		}
		transaction_logprintf(cs->ts, D_DEBUG, "newad fsm state %s", state_lut[state]);

		switch (state) {
			case NEWAD_BEGIN_INSERT:
				TIMER_STATE(NEWAD_BEGIN_INSERT);

				int got_result = 0;
				transaction_logprintf(cs->ts, D_INFO, "NEWAD_BEGIN_INSERT query start");
				worker = sql_blocked_template_query("pgsql.master", 0, "sql/call_insert_ad.sql", &cba.ba, cs->ts->log_string, &errstr);
				transaction_logprintf(cs->ts, D_INFO, "NEWAD_BEGIN_INSERT query end");

				if (worker) {
					while (sql_blocked_next_result(worker, &errstr) == 1) {
						transaction_logprintf(cs->ts, D_INFO, "NEWAD_BEGIN_INSERT result! rows:%d", worker->rows(worker));
						if (worker->rows(worker) > 0) {
							got_result = 1;
							sql_worker_bpa_insert(worker, &cba.ba, "");
						}
					}
					transaction_logprintf(cs->ts, D_INFO, "NEWAD_BEGIN_INSERT results read");
				}

				if (errstr) {
					if (strstr(errstr, "ERROR_PAY_TYPE_INVALID")) {
						cs->status = "TRANS_ERROR";
						transaction_printf(cs->ts, "pay_type:ERROR_PAY_TYPE_INVALID\n");
					} else if (strstr(errstr, "ERROR_STORE_NOT_LOGGED_IN")) {
						cs->status = "TRANS_ERROR";
						transaction_printf(cs->ts, "passwd:ERROR_STORE_PASSWD_MISSING\n");
					} else if (strstr(errstr, "ERROR_PARTNER_ADS_LIMIT_REACHED")) {
						cs->status = "TRANS_ERROR";
						transaction_printf(cs->ts, "link_type:ERROR_PARTNER_ADS_LIMIT_REACHED\n");
					} else if (strstr(errstr, "ERROR_TOO_OLD_TO_EDIT")) {
						cs->status = "TRANS_ERROR";
						transaction_printf(cs->ts, "category:ERROR_TOO_OLD_TO_EDIT\n");
					} else if (strstr(errstr, "ERROR_ORIGINAL_AD_DEACTIVATED")) {
						cs->status = "TRANS_ERROR";
						transaction_printf(cs->ts, "error:ERROR_ORIGINAL_AD_DEACTIVATED\n");
					} else {
						cs->message = xstrdup(errstr);
						cs->status = "TRANS_DATABASE_ERROR";
					}
					goto cleanup;
				}

				if (!got_result) {
					transaction_logprintf(cs->ts, D_DEBUG, "NEWAD_ERROR");
					cs->message = xstrdup("ERROR_NO_RESULT");
					cs->status = "TRANS_ERROR";
					goto cleanup;
				}

				transaction_logprintf(cs->ts, D_INFO, "NEWAD_BEGIN_INSERT checking partner ads");
				if (cmd_haskey(cs, "external_ad_id")) {
					/* Ad import */
					const char *list_id = bpapi_get_element(&cba.ba, "o_list_id", 0);
					const char *action_id = bpapi_get_element(&cba.ba, "o_action_id", 0);
					transaction_printf(cs->ts, "ad_id:%s\n", bpapi_get_element(&cba.ba, "o_ad_id", 0));
					transaction_printf(cs->ts, "new_ad:%d\n", !strcmp(bpapi_get_element(&cba.ba, "o_is_new_ad", 0), "t"));

					if (list_id)
						transaction_printf(cs->ts, "list_id:%s\n", list_id);

					if (action_id && *action_id)
						transaction_printf(cs->ts, "action_id:%s\n", action_id);

					state = NEWAD_BUMP_IMPORT_AD;
					transaction_logprintf(cs->ts, D_INFO, "NEWAD_BEGIN_INSERT commit partner ad");
					sql_worker_newquery(worker, QUERY_COMMIT, &errstr);

					if (errstr) {
						cs->message = xstrdup(errstr);
						cs->status = "TRANS_DATABASE_ERROR";
						goto cleanup;
					}

					transaction_logprintf(cs->ts, D_INFO, "NEWAD_BEGIN_INSERT worker put partner ad");
					while (sql_blocked_next_result(worker, &errstr) == 1);
					sql_worker_put(worker);
					worker = NULL;
					continue;
				}

				const char *prepaid = bpapi_get_element(&cba.ba, "o_prepaid", 0);
				/* If user had enough in his account the ad was prepaid and should be verified */
				if (prepaid && atoi(prepaid)) {
					transaction_printf(cs->ts, "verify:%s\n", bpapi_get_element(&cba.ba, "o_account", 0));
					transaction_logprintf(cs->ts, D_INFO, "This UID has enough in account to verify the ad");
				}
				transaction_printf(cs->ts, "uid:%d\n", atoi(bpapi_get_element(&cba.ba, "o_uid", 0)));

				const char *ad_id = bpapi_get_element(&cba.ba, "o_ad_id", 0);
				const char *action_id = bpapi_get_element(&cba.ba, "o_action_id", 0);
				const char *action_type = bpapi_get_element(&cba.ba, "o_action_type", 0);
				const char *paycode = bpapi_get_element(&cba.ba, "o_paycode", 0);

				bpapi_insert(&cba.ba, "ad_id", ad_id);
				bpapi_insert(&cba.ba, "action_id", action_id);

				transaction_printf(cs->ts, "ad_id:%s\n", ad_id);
				transaction_printf(cs->ts, "action_id:%s\n", action_id);
				transaction_printf(cs->ts, "paycode:%s\n", paycode);

				transaction_logprintf(cs->ts, D_DEBUG, "%s ad with ad id %s, action %s, paycode %s",
					action_type, ad_id, action_id, paycode
				);

				/* Adminedited ads should clear ad and update ad immediately */

				if (!strcmp(action_type, "adminedit")) {
					state = NEWAD_ADMINEDIT;
					continue;
				}

				transaction_logprintf(cs->ts, D_INFO, "NEWAD_BEGIN_INSERT checking for prices");
				if (!prepaid || !atoi(prepaid)) {
					/* Print order id. */
					const char *payment_group_id = bpapi_get_element(&cba.ba, "o_payment_group_id", 0);
					int cat_price = get_cat_price(cs);
					if (cat_price < 0)
						cat_price = 0;
					int gallery_price = 0;
					if (cmd_haskey(cs, "gallery"))
						gallery_price = get_gallery_price(cs);
					int extra_images_price = get_extra_images_price(cs);
					transaction_printf(cs->ts,
						"order_id:%02d%s\n",
						(cat_price + extra_images_price + gallery_price) / 5,
						payment_group_id
					);
				}

				state = NEWAD_MAIL;

				transaction_logprintf(cs->ts, D_INFO, "NEWAD_BEGIN_INSERT commit regular ad");
				sql_worker_newquery(worker, QUERY_COMMIT, &errstr);

				if (errstr) {
					cs->message = xstrdup(errstr);
					cs->status = "TRANS_DATABASE_ERROR";
					goto cleanup;
				}

				transaction_logprintf(cs->ts, D_INFO, "NEWAD_BEGIN_INSERT worker put regular ad");
				while (sql_blocked_next_result(worker, &errstr) == 1);
				sql_worker_put(worker);
				worker = NULL;
				continue;

			case NEWAD_ADMINEDIT:
				TIMER_STATE(ADMINEDIT);

				state = NEWAD_DONE;
				transaction_logprintf(cs->ts, D_INFO, "NEWAD_ADMINEDIT query start");
				sql_blocked_template_newquery(worker, "sql/call_adminedit.sql", &cba.ba, &errstr);
				transaction_logprintf(cs->ts, D_INFO, "NEWAD_ADMINEDIT query end");

				/* Throw away the result */
				while (sql_blocked_next_result(worker, &errstr) == 1);
				transaction_logprintf(cs->ts, D_INFO, "NEWAD_ADMINEDIT results read");

				if (errstr) {
					cs->message = xstrdup(errstr);
					cs->status = "TRANS_DATABASE_ERROR";
					goto cleanup;
				}

				transaction_logprintf(cs->ts, D_INFO, "NEWAD_ADMINEDIT commit");
				sql_worker_newquery(worker, QUERY_COMMIT, &errstr);

				if (errstr) {
					cs->message = xstrdup(errstr);
					cs->status = "TRANS_DATABASE_ERROR";
					goto cleanup;
				}

				transaction_logprintf(cs->ts, D_INFO, "NEWAD_ADMINEDIT worker put");
				while (sql_blocked_next_result(worker, &errstr) == 1);
				sql_worker_put(worker);
				worker = NULL;
				continue;

			case NEWAD_MAIL:
				TIMER_STATE(MAIL);
				{
					const char *_prepaid = bpapi_get_element(&cba.ba, "o_prepaid", 0);
					const char *ad_type = cmd_getval(cs, "ad_type");
					int prepaid = _prepaid && atoi(_prepaid);
					enum pay_type pt;


					if (cmd_haskey(cs, "pay_type"))
						pt = lookup_pay_type (cmd_getval(cs, "pay_type"), -1);
					else
						pt = PT_NONE;

					if (pt != PT_VOUCHER && pt != PT_CAMPAIGN) {
						int generate_adcodes_enabled = bconf_get_int(bconf_root, "*.generate_adcodes.newad.enabled");
						if (generate_adcodes_enabled) {
							if (prepaid) {
								transaction_internal_printf(TRANSACTION_INTERNAL_SYNC_CB, NULL,
									"cmd:create_adcodes\nverifylimit:%s\nverify_gen:1\ncommit:1\nend\n",
									bconf_get_string(bconf_root, "*.generate_adcodes.verifylimit")
								);
							} else {
								transaction_internal_printf(TRANSACTION_INTERNAL_SYNC_CB, NULL,
									"cmd:create_adcodes\npaylimit:%s\npay_gen:1\ncommit:1\nend\n",
									bconf_get_string(bconf_root, "*.generate_adcodes.paylimit")
								);
							}
						}
					}

					/* Send reset to statspoints for prolonged ads */
					if (ad_type && !strcmp(ad_type, "prolong")) {
						struct bpapi_vtree_chain vtree;

						bconf_vtree(&vtree, bconf_get(bconf_root, "*.*.common.statpoints"));

						stat_log(&vtree, "VIEW reset:1", cmd_getval(cs, "list_id"));
						stat_log(&vtree, "MAIL reset:1", cmd_getval(cs, "list_id"));
						vtree_free(&vtree);

					}

					/* If it's not send mail */
					if (cmd_getval(cs, "do_not_send_mail") != NULL) {
						transaction_logprintf(cs->ts, D_INFO, "Suppressed sending of mail.");
					} else {
						if (pt != PT_CAMPAIGN && send_mail(cs, &cba.ba) < 0) {
							cs->status = "TRANS_MAIL_ERROR";
							state = NEWAD_DONE;
							continue;
						} else {
							const char *ad_id = bpapi_get_element(&cba.ba, "o_ad_id", 0);
							const char *paycode = bpapi_get_element(&cba.ba, "o_paycode", 0);
							if (prepaid) {
								syslog_ident("pay_log", "SAVE %s (%s, %s) (ad_id: %s)", paycode, cmd_getval(cs, "remote_addr"), cmd_getval(cs, "email"), ad_id);
							} else if (!cmd_haskey(cs, "link_type")) {
								char *phone;
								int amount = get_cat_price(cs) + get_extra_images_price(cs);

								phone = xmalloc(strlen(cmd_getval(cs, "phone")) + 1);
								format_phone(phone, cmd_getval(cs, "phone"));
								syslog_ident("pay_log", "SAVE %s (%02d%s, %s, %s, %s, %u kr) (ad_id: %s)",
									paycode,
									amount / 5,
									bpapi_get_element(&cba.ba, "o_payment_group_id", 0),
									phone,
									cmd_getval(cs, "email"),
									cmd_getval(cs, "remote_addr"),
									amount,
									ad_id
								);
								free(phone);
							}
						}
					}

					state = NEWAD_IS_DUPLICATED;
					continue;
				}

			case NEWAD_IS_DUPLICATED:
				TIMER_STATE(IS_DUPLICATED);

				if (check_duplicates(cs, &cba.ba))
					state = NEWAD_UDPATE_ACCOUNT_PROFILE;
				else
					state = NEWAD_WHITELIST;
				continue;

			case NEWAD_WHITELIST:
				TIMER_STATE(WHITELIST);

				state = NEWAD_UDPATE_ACCOUNT_PROFILE;
				apply_whitelist_rules(cs, &cba.ba);
				continue;

			case NEWAD_BUMP_IMPORT_AD:
				TIMER_STATE(BUMP_IMPORT_AD);
				state = NEWAD_UDPATE_ACCOUNT_PROFILE;
				bump_import_ad(cs, &cba.ba);
				continue;

			case NEWAD_UDPATE_ACCOUNT_PROFILE:
				TIMER_STATE(UPDATE_ACCOUNT_PROFILE);
				state = NEWAD_DONE;
				update_account_profile(cs, &cba.ba);
				continue;

			default:
				transaction_logprintf(cs->ts, D_ERROR, "(CRIT)Bad state (%d) in newad fsm!", state);
				cs->status = "TRANS_ERROR";
				state = NEWAD_DONE;
				break;
		}
	}

	if (state == NEWAD_DONE) {
		transaction_logprintf(cs->ts, D_DEBUG, "ENTERED NEWAD_DONE");
		if (!cmd_haskey(cs, "ad_type") || !strcmp(cmd_getval(cs, "ad_type"), "new")) {
			syslog_ident("redir", "%s inserted_ad %s",
				cmd_getval(cs, "redir_code") ?: bconf_get_string(host_bconf, "trans.redir_unknown_code"),
				cmd_getval(cs, "remote_addr"));
		}
		// Check if it should send a newad event
		const char *newad_event_conf = "*.trans.newad.newad_event.send_enabled";
		char *ae_enabled = transaction_internal_output_printf("cmd:get_dyn_bconf\nkey:%s\ncommit:1\n", newad_event_conf);
		if (ae_enabled && strstr(ae_enabled, "value:1")) {
			transaction_logprintf(cs->ts, D_INFO, "SEND AD EVALUATION ACTIVE");
			const char *action_type = bpapi_get_element(&cba.ba, "o_action_type", 0);
			if (action_type && (!strcmp(action_type, "edit") || !strcmp(action_type, "adminedit"))) {
				publish_edit_event(cs, &cba.ba);
			}
			else {
				publish_newad_event(cs, &cba.ba);
			}

		}
		free(ae_enabled);
		// Check if the ad is an import. If so, add the enqueue command
		if (cmd_haskey(cs, "external_ad_id")  && cmd_haskey(cs, "link_type")){
			if (bpapi_get_element(&cba.ba, "review_queue", 0)) {
				transaction_logprintf(cs->ts, D_INFO, "newad: import review queue present");
				move_preprocessing_queue(cs, &cba.ba);
			}
		}
	}

cleanup:

	transaction_logprintf(cs->ts, D_DEBUG, "Newad done");

	if (ti)
		timer_end(ti, NULL);

	if (worker) {
		transaction_logprintf(cs->ts, D_ERROR, "newad: Rolling back");

		while (sql_blocked_next_result(worker, &errstr) != 0)
			transaction_logprintf(cs->ts, D_ERROR, "DATABASE ERROR: %s", errstr);

		sql_worker_newquery(worker, QUERY_ROLLBACK, &errstr);
		sql_worker_put(worker);
	}

	command_bpapi_free(&cba);
	cmd_done(cs);
}

ADD_COMMAND(newad, NULL, newad, parameters, NULL);
