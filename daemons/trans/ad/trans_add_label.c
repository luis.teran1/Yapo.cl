#include "factory.h"
#include "bpapi.h"
#include <bconf.h>

static struct c_param parameters[] = {
	{"ad_id",P_REQUIRED, "v_label_ad_id"},
	{"label_type;action_id", P_XOR, "v_label_type;v_id"},
	{"remote_addr",	P_REQUIRED, "v_ip"},
	{"remote_browser",P_OPTIONAL, "v_remote_browser"},
	{"token;batch", P_XOR, "v_token:adminad.bump;v_bool"},
	{NULL, 0}
};

static struct factory_data data = {
        "add_label",
        FACTORY_RES_SHORT_FORM
};

static const struct factory_action actions[]= {
	FA_SQL("", "pgsql.master", "sql/call_add_label.sql", FA_OUT),
	{0}
};

FACTORY_TRANS(add_label, parameters, data, actions);
