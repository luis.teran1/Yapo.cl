#include "factory.h"
#include "bpapi.h"
#include <bconf.h>

static struct c_param parameters[] = {
	{"ad_id",			P_REQUIRED, "v_no_label_ad_id"},
	{"remote_addr",		P_REQUIRED, "v_ip"},
	{"remote_browser",	P_OPTIONAL, "v_remote_browser"},
	{"token;batch", P_XOR, "v_token:adminad.bump;v_bool"},
	{NULL, 0}
};

static struct factory_data data = {
        "remove_label",
        FACTORY_RES_SHORT_FORM
};

static const struct factory_action actions[]= {
	FA_SQL("", "pgsql.master", "sql/call_remove_label.sql", FA_OUT),
	{0}
};

FACTORY_TRANS(remove_label, parameters, data, actions);
