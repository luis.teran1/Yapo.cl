#include "factory.h" 

static struct c_param parameters[] = {
	{"ad_id", P_REQUIRED, "v_id"},
	{"salted_passwd", 0, "v_passwd_sha1_not_required"},
	{"passwd", 0, "v_passwd_not_required"},
	{NULL, 0}
};

FACTORY_TRANSACTION(check_pass, &config.pg_master, "sql/call_check_hard_pass.sql", NULL, parameters, FACTORY_NO_RES_CNTR);
