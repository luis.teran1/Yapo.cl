#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <time.h>

#include "trans.h"
#include "trans_at.h"
#include "util.h"
#include <ctemplates.h>
#include "validators.h"
#include <bconf.h>
#include <event.h>
#include "log_event.h"

#define WEEKLYBUMP_DONE    0
#define WEEKLYBUMP_ERROR   1
#define WEEKLYBUMP_INIT    2
#define WEEKLYBUMP_RESULT  3
#define WEEKLYBUMP_EXECUTE 4

extern struct bconf_node* bconf_root;

struct bump_ad_state {
	int state;
	char *errstr;
	struct cmd *cs;
};

static struct c_param parameters[] = {
	{"daily_bump",		0,	"v_bool"},
	{"is_upselling",	P_OPTIONAL,	"v_bool"},
	{"counter",     	P_REQUIRED, 	"v_integer"},
	{"ad_id",     		P_REQUIRED, 	"v_weekly_ad_id"},
	{"token;batch", 	P_XOR, 		"v_token:adminad.bump;v_bool"},
	{"remote_addr", 	0, 		"v_ip"},
	{"batch_bump", 		0, 		"v_bool"},
	{"remote_browser", 	0, 		"v_remote_browser"},
	{"action_id", 		0, 		"v_id"},
	{"do_not_send_mail",	0,		"v_bool"},
	{NULL, 0}
};

static const char *weekly_bump_ad_fsm(void *, struct sql_worker *, const char *);

/*****************************************************************************
 * weekly_bump_ad_done
 * Exit function.
 **********/
static void
weekly_bump_ad_done(void *v) {
	struct bump_ad_state *state = v;
	struct cmd *cs = state->cs;

	free(state);
	cmd_done(cs);
}

/*****************************************************************************
 * bind_insert
 * Initialise template API before a call to sql_worker_template_query().
 **********/
static void
bind_insert(struct bpapi *ba, struct bump_ad_state *state) {
	struct c_param *param;
	char buffer[3];
	int counter = cmd_haskey(state->cs, "counter") ? atoi(cmd_getval(state->cs, "counter")) - 1 : 0;
	snprintf(buffer, sizeof(buffer), "%d", counter);

	for (param = parameters; param->name; param++) {
		if (cmd_haskey(state->cs, param->name) && strcmp(param->name, "counter")!=0) {
			bpapi_insert(ba, param->name, cmd_getval(state->cs, param->name));
		}
	}

	if (cmd_haskey(state->cs, "token"))
		bpapi_insert(ba, "token", cmd_getval(state->cs, "token"));

	bpapi_insert(ba, "counter", buffer);
}

/*****************************************************************************
 * weekly_bump_ad_fsm
 * State machine
 **********/
static const char *
weekly_bump_ad_fsm(void *v, struct sql_worker *worker, const char *errstr) {
	struct bump_ad_state *state = v;
	struct cmd *cs = state->cs;
	struct bpapi ba;
	int counter = cmd_haskey(cs, "counter") ? atoi(cmd_getval(cs, "counter")) - 1 : 0;
	int dnsm = cmd_haskey(cs, "do_not_send_mail") ? atoi(cmd_getval(cs, "do_not_send_mail")) : 0;
	int action_id = cmd_haskey(cs, "action_id") ? atoi(cmd_getval(cs, "action_id")) : 0;

	/*
	 * General error handling for db errors.
	 */
	if (errstr) {
		transaction_logprintf(cs->ts, D_ERROR, "weekly_bump_ad_fsm statement failed (%d, %s)",
									state->state,
									errstr);
		cs->status = "TRANS_ERROR";
		cs->message = xstrdup(errstr);

		syslog_ident("product_err", "bump: failed bump_ad (ad_id, remote_addr, state, status, error) (%s, %s, %d, %s, %s)",
							cmd_getval(cs, "ad_id"),
							(cmd_haskey(cs, "remote_addr")) ? cmd_getval(cs, "remote_addr") : "",
							state->state,
							cs->status,
							strtrchr(errstr, "\n", ' '));

		state->state = WEEKLYBUMP_ERROR;
	}

	while (1) {
		transaction_logprintf(cs->ts, D_DEBUG, "weekly_bump_ad_fsm(%p, %p, %p), state=%d",
								v, worker, errstr, state->state);

		switch (state->state) {
		case WEEKLYBUMP_INIT:
			state->state = WEEKLYBUMP_RESULT;

			BPAPI_INIT(&ba, NULL, pgsql);
			bind_insert(&ba, state);

			sql_worker_template_query(&config.pg_master,
						"sql/call_queued_bump_counter.sql", &ba,
						weekly_bump_ad_fsm, state, cs->ts->log_string);

			bpapi_output_free(&ba);
			bpapi_simplevars_free(&ba);
			bpapi_vtree_free(&ba);
			return NULL;

		case WEEKLYBUMP_RESULT:
			if (worker->next_row(worker) && !strcmp(worker->get_value(worker, 0), "0") ) {
				if(action_id == 0){
					action_id = atoi(worker->get_value_byname(worker,"action_id"));
				}
				state->state = WEEKLYBUMP_EXECUTE;
			}
			else {
				state->state = WEEKLYBUMP_ERROR;
			}
			continue;

		case WEEKLYBUMP_EXECUTE:
			{
				int next_execution = 0;
				if (cmd_haskey(cs, "daily_bump")) {
					next_execution = bconf_get_int(bconf_root, "*.*.payment.product_specific.daily_bump.interval_seconds");
					transaction_logprintf(cs->ts, D_DEBUG, "daily_bump_ad counter=%d ad_id=%s", counter, cmd_getval(cs, "ad_id"));
				} else {
					next_execution = bconf_get_int(bconf_root, "*.*.payment.product_specific.weekly_bump.interval_seconds");
					transaction_logprintf(cs->ts, D_DEBUG, "weekly_bump_ad counter=%d ad_id=%s", counter, cmd_getval(cs, "ad_id"));
				}

				/* Execute the bump asynchronously*/
				transaction_internal_printf(
					NULL,
					state,
					"cmd:bump_ad\n"
					"commit:1\n"
					"batch:1\n"
					"batch_bump:1\n"
					"do_not_send_mail:%d\n"
					"mail_type:weekly_bump_ok\n"
					"ad_id:%s\n"
					"remaining_bumps:%d\n",
					dnsm,
					cmd_getval(cs, "ad_id"),
					counter
				);

				if (counter > 0) {
					char *weeklycmd;
					// Build next execution command
					if ( action_id != 0){
						xasprintf(&weeklycmd,
							"cmd:weekly_bump_ad\n"
							"ad_id:%s\n"
							"action_id:%d\n"
							"counter:%d\n"
							"%s"
							"%s"
							"batch:1\n"
							"commit:1\n",
							cmd_getval(cs, "ad_id"),
							action_id,
							counter,
							(cmd_haskey(cs, "daily_bump"))?"daily_bump:1\n":"",
							(cmd_haskey(cs, "is_upselling"))?"is_upselling:1\n":""
						);
					}
					else{
						xasprintf(&weeklycmd,
							"cmd:weekly_bump_ad\n"
							"ad_id:%s\n"
							"counter:%d\n"
							"%s"
							"%s"
							"batch:1\n"
							"commit:1\n",
							cmd_getval(cs, "ad_id"),
							counter,
							(cmd_haskey(cs, "daily_bump"))?"daily_bump:1\n":"",
							(cmd_haskey(cs, "is_upselling"))?"is_upselling:1\n":""
						);
					}

					// Add the next command to the corresponding queue
					transaction_internal_printf(NULL, state,
							"cmd:queue\n"
							"commit:1\n"
							"queue:%s\n"
							"timeout:%d\n"
							"blob:%zu:command\n"
							"%s\n",
							(cmd_haskey(cs, "is_upselling"))?"upselling":"weekly_bump",
							next_execution,
							strlen(weeklycmd),
							weeklycmd);
				}

				state->state = WEEKLYBUMP_DONE;
				continue;
			}

		case WEEKLYBUMP_DONE:
			weekly_bump_ad_done(state);
			return NULL;

		case WEEKLYBUMP_ERROR:
			transaction_logprintf(cs->ts, D_ERROR, "Error state");
			if (worker)
				sql_worker_set_wflags(worker, 0);
			weekly_bump_ad_done(state);
			return NULL;

		default:
			/* MAJOR STATE ERROR */
			cs->status = "TRANS_ERROR";
			transaction_logprintf(cs->ts, D_ERROR, "Unknown state (%d)",
					      state->state);
			return NULL;
		}
	}
}

/*****************************************************************************
 * weekly_bump_ad
 * Main function that initialise a state-structure and calls the state machine.
 **********/
static void
weekly_bump_ad(struct cmd *cs) {
	struct bump_ad_state *state;
	state = zmalloc(sizeof(*state));
	state->cs = cs;
	state->state = WEEKLYBUMP_INIT;
	weekly_bump_ad_fsm(state, NULL, NULL);
}

ADD_COMMAND(weekly_bump_ad,		/* Name of command */
		NULL,
		weekly_bump_ad,		/* Main function name */
		parameters,		/* Parameters struct */
		"weekly bump an ad");	/* Command description */

