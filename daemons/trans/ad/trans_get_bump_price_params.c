#include "factory.h"

static struct c_param parameters[] = {
	{"ad_id",  P_REQUIRED, "v_id"},
	{NULL, 0}
};

FACTORY_TRANSACTION(get_bump_price_params, &config.pg_master, "sql/get_bump_price_params.sql", NULL, parameters, FACTORY_NO_RES_CNTR);
