#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <time.h>

#include "trans.h"
#include "util.h"
#include <ctemplates.h>
#include "validators.h"
#include <bconf.h>
#include <event.h>

#define CLEAR_WITH_VOUCHER_DONE  0
#define CLEAR_WITH_VOUCHER_ERROR 1
#define CLEAR_WITH_VOUCHER_INIT  2
#define CLEAR_WITH_VOUCHER_UNLOAD 3
#define CLEAR_WITH_VOUCHER_RESULT 4

struct clear_with_voucher_state {
	int state;
	struct cmd *cs;
};

static struct c_param parameters[] = {
	{"ad_id", 		P_REQUIRED, "v_id"},
	{"action_id", 		P_REQUIRED, "v_integer"},
	{"remote_addr", 	P_REQUIRED, "v_ip"},
	{"remote_browser", 	0, "v_remote_browser"},
	{"store_id", 		P_REQUIRED, "v_integer"},
	{"token", 		P_REQUIRED, "v_token"},
	{NULL, 0}
};

static const char *clear_with_voucher_fsm(void *, struct sql_worker *, const char *);

extern struct bconf_node *bconf_root;

/*****************************************************************************
 * clear_with_voucher_done
 * Exit function.
 **********/
static void
clear_with_voucher_done(void *v) {
	struct clear_with_voucher_state *state = v;
	struct cmd *cs = state->cs;
	free(state);
	cmd_done(cs);
}

/*****************************************************************************
 * bind_insert
 * Initialise template API before a call to sql_worker_template_query().
 **********/
static void
bind_insert(struct bpapi *ba, struct clear_with_voucher_state *state) {
	struct c_param *param;

	for (param = parameters; param->name; param++) {
		if (cmd_haskey(state->cs, param->name))
			bpapi_insert(ba, param->name, cmd_getval(state->cs, param->name));
	}
}

/*****************************************************************************
 * clear_with_voucher_fsm
 * State machine
 **********/
static const char *
clear_with_voucher_fsm(void *v, struct sql_worker *worker, const char *errstr) {
	struct clear_with_voucher_state *state = v;
	struct cmd *cs = state->cs;
        struct bpapi ba;

	/*
	 * General error handling for db errors.
	 */
	if (errstr) {
                transaction_logprintf(cs->ts, D_ERROR, "clear_with_voucher_fsm statement failed (%d, %s)",
                                      state->state,
                                      errstr);
                cs->status = "TRANS_DATABASE_ERROR";
                cs->message = xstrdup(errstr);
		state->state = CLEAR_WITH_VOUCHER_ERROR;
        }

	while (1) {
		transaction_logprintf(cs->ts, D_DEBUG, "clear_with_voucher_fsm(%p, %p, %p), state=%d",
				      v, worker, errstr, state->state);

		switch (state->state) {
		case CLEAR_WITH_VOUCHER_INIT:
			/* next state */
			state->state = CLEAR_WITH_VOUCHER_UNLOAD;

			/* Initialise the templateparse structure */
			BPAPI_INIT_APP(&ba, host_bconf, "controlpanel", buf, pgsql);
			bind_insert(&ba, state);
			sql_worker_template_query(&config.pg_master,
						  "sql/load_category.sql", &ba,
						  clear_with_voucher_fsm, state, cs->ts->log_string);
			bpapi_output_free(&ba);
			bpapi_simplevars_free(&ba);
			bpapi_vtree_free(&ba);
			return NULL;

		case CLEAR_WITH_VOUCHER_UNLOAD:
			state->state = CLEAR_WITH_VOUCHER_RESULT;

			/* Initialise the templateparse structure */
			BPAPI_INIT_APP(&ba, host_bconf, "controlpanel", buf, pgsql);
			bind_insert(&ba, state);
			sql_worker_bpa_insert(worker, &ba, NULL);

			/* Register SQL query with our clear_with_voucher_fsm() as callback function */
			/* Make sure to switch to slave if read-only query. */
			sql_worker_template_newquery(worker, "sql/call_clear_voucher.sql", &ba);

			/* Clean-up */
			bpapi_output_free(&ba);
			bpapi_simplevars_free(&ba);
			bpapi_vtree_free(&ba);

			/* Return to libevent */
			return NULL;

                case CLEAR_WITH_VOUCHER_RESULT:
                        state->state = CLEAR_WITH_VOUCHER_DONE;

                        if (worker->next_row(worker))
				transaction_printf(cs->ts, "voucher_result:%s\n", worker->get_value_byname(worker, "balance"));
                        else
				state->state = CLEAR_WITH_VOUCHER_ERROR;

			continue;

		case CLEAR_WITH_VOUCHER_DONE:
			clear_with_voucher_done(state);
			return NULL;

		case CLEAR_WITH_VOUCHER_ERROR:
			transaction_logprintf(cs->ts, D_ERROR, "Error state");
			clear_with_voucher_done(state);
			return NULL;

		default:
			/* MAJOR STATE ERROR */
			cs->status = "TRANS_ERROR";
			transaction_logprintf(cs->ts, D_ERROR, "Unknown state (%d)",
					      state->state);
			return NULL;

		}
	}
}

/* clear_with_voucher
 * Main function that initialise a state-structure and calls the state machine.
 **********/
static void
clear_with_voucher(struct cmd *cs) {
	transaction_logprintf(cs->ts, D_DEBUG, "Start Clear_With_Voucher transaction");
	struct clear_with_voucher_state *state;
	state = zmalloc(sizeof(*state));
	state->cs = cs;
	state->state = CLEAR_WITH_VOUCHER_INIT;

	clear_with_voucher_fsm(state, NULL, NULL);
}

ADD_COMMAND(clear_with_voucher,     /* Name of command */
		NULL,
            clear_with_voucher,     /* Main function name */
            parameters,  /* Parameters struct */
            "Clear_With_Voucher command");/* Command description */
