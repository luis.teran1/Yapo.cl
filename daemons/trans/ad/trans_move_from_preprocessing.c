#include <errno.h>

#include "log_event.h"
#include "bconfd_client.h"
#include "factory.h"
#include "trans_at.h"
#include "pgsql_api.h"
#include "settings.h"
#include "utils/prioritize_queues.h"

static struct c_param parameters[] = {
	{"ad_id",		P_REQUIRED,		"v_ad_id"},
	{"action_id",	P_REQUIRED,		"v_action_id"},
	{"queue",		P_REQUIRED,	"v_queue_queue"},
	{"remote_addr",		P_OPTIONAL, "v_ip"},
	{"remote_browser",	P_OPTIONAL, "v_remote_browser"},
	{NULL}
};

static const struct factory_action preprocessing_import  = FA_SQL("", "pgsql.master", "sql/call_preprocessing_import.sql", 0);

static const struct factory_action *
move_to_queue_import(struct cmd *cs, struct bpapi *ba) {
	const char *queue = cmd_getval(cs, "queue");
	const char *t_queue = bpapi_get_element(ba, "target_queue", 0);
	if (!strcmp(queue, t_queue)) {
		return &preprocessing_import;
	}
	return NULL;
}

static int
auto_review(struct cmd *cs, struct bpapi *ba) {
	const char *queue = bpapi_get_element(ba, "target_queue", 0);
	const char *ad_id = cmd_getval(cs, "ad_id");
	const char *action_id = cmd_getval(cs, "action_id");

	struct bconf_node *auto_queue = bconf_vget(host_bconf, "controlpanel", "modules", "adqueue", "auto", queue, NULL);

	transaction_logprintf(cs->ts, D_DEBUG, "auto review: ad_id:%s, action_id:%s, queue:%s, auto_queue:%p", ad_id, action_id, queue, auto_queue);

	if (queue && auto_queue) {
		struct bconfd_call bc = {0};
		bc.proto = PROTO_TRANS;
		bconfd_add_request_param(&bc, "type", "conf");

		/* Get the local bconf value */
		int autoreview = bconf_get_int(host_bconf, "controlpanel.modules.adqueue.settings.autoreview");

		int r = bconfd_load(&bc, bconf_get(host_bconf, "*.service_bconfd"));
		if (r != 0) {
			transaction_logprintf(cs->ts, D_WARNING, "auto review: problems with bconfd: %s", bc.errstr);
		} else if (bconf_get(bc.bconf, "*.controlpanel.modules.adqueue.settings.autoreview")) {
			/* If we can reach bconfd, use its result instead */
			autoreview = bconf_get_int(bc.bconf, "*.controlpanel.modules.adqueue.settings.autoreview");
		}

		bconf_free(&bc.bconf);
		transaction_logprintf(cs->ts, D_DEBUG, "auto review: autoreview:%d", autoreview);

		if (autoreview) {
			char *cmd;
			xasprintf(&cmd, "cmd:review\n"
				"commit:1\n"
				"ad_id:%s\n"
				"action_id:%s\n"
				"action:%s\n"
				"filter_name:%s\n"
				"remote_addr:%s\n"
				"%s\n",
				ad_id,
				action_id,
				bconf_get_string(auto_queue, "action"),
				queue,
				cmd_getval(cs, "remote_addr") ?: "",
				bconf_get_string(auto_queue, "extra") ?: "");

			int to = bconf_get_int(auto_queue, "timeout");
			if (to) {
				at_register_cmd(cmd, to, "review", ad_id, 0);
				free(cmd);
			} else {
				struct internal_cmd *t = zmalloc(sizeof (*t));
				t->cmd_string = cmd;
				transaction_internal(t);
			}
			transaction_logprintf(cs->ts, D_INFO, "Ad sent for autoreviewing (ad_id:%s action_id:%s queue:%s)", ad_id, action_id, queue);
		}
	}
	return 0;
}

static int check_ad_evaluation_queue (struct cmd *cs, struct bpapi *ba) {
	prioritize_queues(cs, ba);
	return 0;
}

static struct factory_data data = {
	"controlpanel",
	FACTORY_NO_RES_CNTR | FACTORY_RES_SHORT_FORM
};

static const struct factory_action actions[] = {
	FA_SQL("", "pgsql.master", "sql/get_ad_evaluation_queue.sql", FASQL_OUT),
 	FA_FUNC(check_ad_evaluation_queue),
	FA_SQL("", "pgsql.master", "sql/call_move_to_queue.sql", FASQL_OUT),
	FA_COND(move_to_queue_import),
	FA_FUNC(auto_review),
	FA_DONE()
};

FACTORY_TRANS(move_from_preprocessing, parameters, data, actions);
