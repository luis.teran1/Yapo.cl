#include "factory.h"
#include "bpapi.h"
#include <bconf.h>

static struct c_param parameters[] = {
	{"ad_id", P_REQUIRED, "v_id"},
	{"frequency", P_REQUIRED, "v_frequency"},
	{"num_days", P_REQUIRED, "v_autobump_max_days"},
	{"use_night", P_OPTIONAL, "v_bool"},
	{"purchase_detail_id;token;batch", P_XOR, "v_integer;v_token:adminad.bump;v_bool"},
	{"remote_browser", P_OPTIONAL, "v_remote_browser"},
	{"remote_addr", P_OPTIONAL, "v_ip"},
	{NULL, 0}
};

static struct factory_data data = {
	"autobump",
	FACTORY_NO_RES_CNTR | FACTORY_RES_SHORT_FORM | FACTORY_EMPTY_RESULT_OK
};

static const struct factory_action actions[]= {
	FA_SQL("", "pgsql.master", "sql/call_create_autobump.sql", FASQL_OUT),
	{0}
};

FACTORY_TRANS(create_autobump, parameters, data, actions);
