#include "factory.h"
#include "bpapi.h"
#include <bconf.h>
#include <time.h>
#include "settings_utils.h"

extern struct bconf_node *bconf_root;


static struct c_param parameters[] = {
	{"ad_id;list_id", P_XOR, "v_id;v_id"},
	// Frequency is the interval of bumps defined in hours
	{"frequency", P_REQUIRED, "v_frequency"},
	{"num_days", P_REQUIRED, "v_autobump_max_days"},
	{"use_night", P_OPTIONAL, "v_bool"},
	{"remote_browser",      P_OPTIONAL, "v_remote_browser"},
	{NULL, 0}
};


static int price_autobump_info(struct cmd *cs, struct bpapi *ba) {
	const char *category = bpapi_get_element(ba, "category", 0);

	if(!category) {
		cs->error = "ERROR_AD_NOT_FOUND";
		return 1;
	}

	// This values are to use get_setting_keyval
	cmd_setval(cs, "product", xstrdup("1"));
	cmd_setval(cs, "category", xstrdup(category));

	// Get price per bump
	int price = get_setting_keyval_value(cs, "payment_settings", "product", "price");

	// Generate output
	transaction_printf(cs->ts, "ad_category:%s\n", category);
	transaction_printf(cs->ts, "unit_price:%d\n", price);

	int freq = cmd_getint_default(cs, "frequency", 1);
	int days = cmd_getint_default(cs, "num_days", 1);
	int use_night = cmd_getint_default(cs, "use_night", 0);

	// Truncate time at minutes
	time_t now = time(NULL) / 60 * 60;

	char buff[20];
	strftime(buff, 20, "%Y-%m-%d %H:%M:%S", localtime(&now));
	transaction_printf(cs->ts, "date_start:%s\n", buff);

	// Define date end as now + interval of days
	time_t date_end = now + (days * 24 * 60 * 60);
	strftime(buff, 20, "%Y-%m-%d %H:%M:%S", localtime(&date_end));
	transaction_printf(cs->ts, "date_end:%s\n", buff);

	// Iterate by time
	int loop_counter = 0;

	// Get night definition (in minutes)
	int night_start_at = bconf_get_int(trans_bconf, "autobump.night_start_at");
	int night_end_at= bconf_get_int(trans_bconf, "autobump.night_end_at");

	// Convert the night start & end to hours
	if (night_start_at > 0)
		night_start_at = night_start_at / 60;
	if (night_end_at > 0)
		night_end_at = night_end_at / 60;

	transaction_logprintf(cs->ts, D_DEBUG, "Night from:%d to:%d is_used:%d\n", night_start_at, night_end_at, use_night);
	while (now < date_end) {
		//Get the hour to avoid count at night if they are skipped
		strftime(buff, 20, "%H", localtime(&now));
		int hour = atoi(buff);
		if (use_night == 0 && hour >= night_start_at &&  hour < night_end_at) {
			transaction_logprintf(cs->ts, D_DEBUG, "Do not use night %d", hour);
		} else {
			strftime(buff, 20, "%Y-%m-%d %H:%M:%S", localtime(&now));
			transaction_printf(cs->ts, "execution_%d:%s\n", loop_counter, buff);
			loop_counter++;
		}
		// Always increase the time
		now = now + (freq * 60 * 60);
	}
	// Calculate the final price
	int total_price = loop_counter * price;
	transaction_printf(cs->ts, "total_bumps:%d\n", loop_counter);
	transaction_printf(cs->ts, "total_price:%d\n", total_price);
	return 0;
}


static struct factory_data data = {
	"autobump",
	FACTORY_NO_RES_CNTR | FACTORY_RES_SHORT_FORM | FACTORY_EMPTY_RESULT_OK
};

static const struct factory_action actions[]= {
	FA_SQL("", "pgsql.slave", "sql/get_ad_info.sql", 0),
	FA_FUNC(price_autobump_info),
	{0}
};

FACTORY_TRANS(price_autobump, parameters, data, actions);
