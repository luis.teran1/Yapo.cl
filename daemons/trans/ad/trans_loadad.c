#include <stdio.h>

#include "command.h"
#include "trans.h"
#include "util.h"
#include "bconf.h"
#include "validators.h"
#include "db.h"
#include <ctemplates.h>
#include "trans_loadad.h"

#include "loadad_action_type.h"

extern struct bconf_node *bconf_root;

#define LOADAD_DONE 0
#define LOADAD_ERROR 1
#define LOADAD_INIT 2
#define LOADAD_CHECK_REFUSED 3
#define LOADAD_CHECK 4
#define LOADAD_TOKEN_INFO 5
#define LOADAD_LOAD 6
#define LOADAD_LOAD_DATA 7
#define LOADAD_LOAD_UPSELLING 8
#define LOADAD_LOAD_UPSELLING_DATA 9
#define LOADAD_SHOWAD 10
#define LOADAD_LIST_STORES 11

struct loadad_state {
	int state;
	struct cmd *cs;
	struct action *action;
	char *auth_type;
	char *auth_resource;
	char *dbc;
};

static const int action_types_with_passwd[MAX_LAT_WORD + 1] =
{
	[LAT_EDIT] = 1,
	[LAT_RENEW] = 1,
	[LAT_ADDIMAGES] = 1,
	[LAT_COPY] = 1,
	[LAT_DELETE] = 1
};

static const int action_types_active_check[MAX_LAT_WORD + 1] =
{
	[LAT_EDIT] = 1,
	[LAT_RENEW] = 1,
	[LAT_API] = 1,
	[LAT_ADDIMAGES] = 1,
	[LAT_DELETE] = 1
};

static const char *loadad_fsm(void *v, struct sql_worker *worker, const char *errstr);

static int check_passwd(struct cmd *cs, const char *adpasswd) {
	const char *action = cmd_getval(cs, "action");
	const char *passwd = cmd_getval(cs, "passwd");
	const char *token = cmd_getval(cs, "token");
	const char *id = cmd_getval(cs, "id");

	/* Skip passwd check if we are on load page, have authenticated with token, or are using the links from html mail. */
	if (!action || token || strchr(id, '.'))
		return 1;

	if (action_types_with_passwd[lookup_loadad_action_type(action, strlen(action))]) {
		if (!adpasswd || !passwd || strcmp(passwd, adpasswd) != 0) {
			transaction_logprintf(cs->ts, D_INFO, "Failed password check (%s) (%s)", passwd, adpasswd);
			cs->status = "TRANS_ERROR";
			return 0;
		}
		return 1;
	}

	return 1;
}

static int check_edit_active_ad(struct cmd *cs, const char *ad_status, const char *auth_type) {
	const char *action = cmd_getval(cs, "action");

	if (!action)
		return 1;

	if (!strcmp(action, "api"))
		return 1;

	if (action_types_active_check[lookup_loadad_action_type(action, strlen(action))] &&
			(strcmp(ad_status, "active") != 0)
			&& (strcmp(ad_status, "disabled") != 0)
			&& (strcmp(ad_status, "hidden") != 0 || !auth_type || strcmp(auth_type, "admin") != 0)) {
		transaction_logprintf(cs->ts, D_DEBUG, "Failed active ad check. %s (%s)", ad_status, action);
		cs->status = "TRANS_ERROR";
		return 0;
	}

	return 1;
}

static struct c_param action_id_param =
	{"action_id",   	P_REQUIRED,  	  "v_action_id"};

static int v_check_passwd(struct cmd_param *param, const char *arg) {
	const char *action = param->value;
	const char *passwd = cmd_getval(param->cs, "passwd");
	const char *token = cmd_getval(param->cs, "token");
	const char *id = cmd_getval(param->cs, "id");

	if (!action || !id || token || strchr(id, '.'))
		return 1;

	if (strcmp(action, "store_deleted") == 0 && !token) {
		transaction_printf(param->cs->ts, "token:ERROR_TOKEN_MISSING\n");
		param->cs->status = "TRANS_ERROR";
		return 0;
	}

	if (action_types_with_passwd[lookup_loadad_action_type(action, strlen(action))]) {
		if (!passwd || !passwd[0]) {
			if (strcmp(param->cs->status, "TRANS_ERROR") != 0) {
				param->cs->status = "TRANS_ERROR";
				transaction_printf(param->cs->ts, "%s:%s\n",
						   "passwd", "ERROR_PASSWORD_MISSING");
			}
			return 0;
		}
		return 1;
	}

	if (!strcmp(action, "refused") || !strcmp(action, "deactivated"))
		cmd_extra_param(param->cs, &action_id_param);

	return 1;
}

static struct validator v_action[] = {
	VALIDATOR_REGEX("^(edit|renew|refused|deleted|store_deleted|addimages|copy|delete|gallery|addgallery|api|deactivated|spidered.*)$", REG_EXTENDED, "ERROR_AD_ACTION_CMD_INVALID"),
	VALIDATOR_FUNC(v_check_passwd),
	{0}
};
ADD_VALIDATOR(v_action);

static struct validator v_loadad_id[] = {
	VALIDATOR_REGEX("^[[:digit:]]+(\\.[[:digit:]]+)?$", REG_EXTENDED, "ERROR_AD_ID_INVALID"),
	{0}
};
ADD_VALIDATOR(v_loadad_id);

static struct c_param parameters[] = {
	{"token",		0,	 "v_token:adminad.edit_ad"},
	{"passwd",		0, 	  "v_passwd_not_required"},
	{"id", 			P_REQUIRED,	  "v_loadad_id"},
	{"action",  		0, 	  "v_action"},
	{"remote_addr", 	0,  	  "v_ip"},
	{"remote_browser", 	0,  	  "v_remote_browser"},
	{NULL}
};

static void loadad_done(struct loadad_state *state) {
	struct cmd *cs = state->cs;

	free(state->auth_type);
	free(state->auth_resource);
	free(state->dbc);
	free(state);
	cmd_done(cs);
}

void
bind_loadad_id (struct bpapi *ba, const char *id) {
	size_t sz = strlen(id);
	const char *dp;

	if (sz >= 8 && (dp = strchr(id, '.')) && dp - id >= 7) {
		bpapi_insert(ba, "reg_state_id", dp + 1);
		sz = dp - id;

		if (sz >= 7) {
			char list_id[sz - 5];
			strncpy(list_id, id, sz - 6);
			list_id[sz - 6] = '\0';
			bpapi_insert(ba, "list_id", list_id);
			bpapi_insert_maxsize(ba, "added_check", id + sz - 6, 6);
		}
	} else {
		bpapi_insert(ba, "list_id", id);
	}
};

static const char *
loadad_fsm(void *v, struct sql_worker *worker, const char *errstr) {
	struct loadad_state *state = v;
	struct cmd *cs = state->cs;
	struct bpapi ba;
	struct action_param *action_param;
	const char *id;
	const char *action;
	const char *grace_days;

	if (state->state == LOADAD_ERROR)
		goto error;

	/*
	 * General error handling for db errors.
	 */
	if (errstr) {
		if (strstr (errstr, "out of range") || strstr (errstr, "ERROR_NO_SUCH_AD")) {
			cs->status = "TRANS_ERROR";
			cs->error = "ERROR_NO_SUCH_AD";
		} else if (strstr (errstr, "ERROR_AD_TOO_OLD")) {
			cs->status = "TRANS_ERROR";
			cs->error = "ERROR_AD_TOO_OLD";
		} else {
			cs->status = "TRANS_DATABASE_ERROR";
			cs->message = xstrdup(errstr);
		}
		state->state = LOADAD_ERROR;
		goto error;
	}

	while (state->state != LOADAD_DONE) {
		transaction_logprintf(cs->ts, D_DEBUG, "loadad fsm state %d", state->state);
		switch (state->state) {
			case LOADAD_INIT:
			{
				const char *dbc;

				if (cmd_haskey(cs, "token"))
					dbc = &config.pg_master;
				else
					dbc = &config.pg_slave;

				state->dbc = xstrdup(dbc);
				BPAPI_INIT(&ba, NULL, pgsql);

				action = cmd_getval(cs, "action");
				if (action) {
					bpapi_insert(&ba, "action", action);
					grace_days = bconf_value(bconf_vget(bconf_root, "*.common.grace_time", action, NULL));
					if (grace_days)
						bpapi_insert(&ba, "grace_days", grace_days);
				}

				/* Go to loadad_check_refused if it is a refused ad action */
				if (action && strcmp(action, "refused") == 0 && cmd_haskey(cs, "action_id")) {
					cmd_setval(cs, "ad_id", xstrdup(cmd_getval(cs, "id")));

					state->state = LOADAD_CHECK_REFUSED;
					bpapi_insert(&ba, "ad_id", cmd_getval(cs, "id"));
					bpapi_insert(&ba, "action_id", cmd_getval(cs, "action_id"));
					sql_worker_template_query_flags(state->dbc, SQLW_FINAL_CALLBACK,
								  "sql/get_current_state.sql", &ba,
								  loadad_fsm, state, cs->ts->log_string);

					bpapi_output_free(&ba);
					bpapi_simplevars_free(&ba);
					bpapi_vtree_free(&ba);
					return NULL;
				}

				if (action && strcmp(action, "api") == 0) {
					//cmd_setval(cs, "token_acc_id", xstrdup(cmd_getval(cs, "account_id")));

					state->state = LOADAD_CHECK;
					bpapi_insert(&ba, "ad_id", cmd_getval(cs, "id"));
					sql_worker_template_query_flags(state->dbc, SQLW_FINAL_CALLBACK,
								  "sql/call_get_current_action_id.sql", &ba,
								  loadad_fsm, state, cs->ts->log_string);

					bpapi_free(&ba);
					return NULL;
				}

				id = cmd_getval(cs, "id");
				bind_loadad_id(&ba, id);

				state->state = LOADAD_CHECK;

				sql_worker_template_query_flags(state->dbc, SQLW_FINAL_CALLBACK,
								"sql/get_adid.sql", &ba,
								loadad_fsm, state, cs->ts->log_string);
				bpapi_output_free(&ba);
				bpapi_simplevars_free(&ba);
				bpapi_vtree_free(&ba);
				return NULL;
			}

			case LOADAD_CHECK_REFUSED:
				if (worker->next_row(worker)) {
					transaction_logprintf(cs->ts, D_DEBUG, "Check_refused state: %s", worker->get_value_byname(worker, "state"));
					if (strcmp(worker->get_value_byname(worker, "state"), "refused") == 0 && strcmp(worker->get_value_byname(worker, "status"), "deleted") != 0 && strcmp(worker->get_value_byname(worker, "status"), "deactivated") != 0) {
						state->state = LOADAD_LOAD;
					} else {
						if (strcmp(worker->get_value_byname(worker, "status"), "deleted") == 0)
							cs->error = "ERROR_DELETED";
						else if (strcmp(worker->get_value_byname(worker, "status"), "deactivated") == 0)
							cs->error = "ERROR_DEACTIVATED";
						else
							cs->error = "ERROR_NOT_REFUSED";
						cs->status = "TRANS_ERROR";
						state->state = LOADAD_ERROR;
					}
				} else {
					cs->error = "ERROR_NO_SUCH_AD";
					cs->status = "TRANS_ERROR";
					state->state = LOADAD_ERROR;
					transaction_logprintf(cs->ts, D_DEBUG, "No action found with that ad_id/action_id");
				}

				return NULL;

			case LOADAD_CHECK:
				if (worker->next_row(worker)) {
					cmd_setval(cs, "ad_id", xstrdup(worker->get_value_byname(worker, "ad_id")));
					if (!cmd_haskey(cs, "action_id"))
					    cmd_setval(cs, "action_id", xstrdup(worker->get_value_byname(worker, "action_id")));
					if (cmd_haskey(cs, "token")) {
						BPAPI_INIT(&ba, NULL, pgsql);
						bpapi_insert(&ba, "token", cmd_getval(cs, "token"));
						sql_worker_template_newquery(worker, "sql/call_get_token_auth.sql", &ba);
						state->state = LOADAD_TOKEN_INFO;
						bpapi_output_free(&ba);
						bpapi_simplevars_free(&ba);
						bpapi_vtree_free(&ba);
						return NULL;
					}
					state->state = LOADAD_LOAD;
					transaction_logprintf(cs->ts, D_DEBUG, "Going to LOADAD_LOAD (%d)", state->state);
				} else {
					cs->error = "ERROR_NO_SUCH_AD";
					cs->status = "TRANS_ERROR";
					state->state = LOADAD_ERROR;
					transaction_logprintf(cs->ts, D_DEBUG, "No ads found with that list_id");
				}
				return NULL;

			case LOADAD_TOKEN_INFO:
				if (worker->next_row(worker)) {
					state->auth_type = xstrdup(worker->get_value_byname(worker, "o_auth_type"));
					state->auth_resource = xstrdup(worker->get_value_byname(worker, "o_auth_resource"));
					state->state = LOADAD_LOAD;
				}
				return NULL;

			case LOADAD_LOAD:
				if (cmd_haskey(cs, "ad_id") && cmd_haskey(cs, "action_id")) {
					BPAPI_INIT(&ba, NULL, pgsql);
					bpapi_insert(&ba, "ad_id", cmd_getval(cs, "ad_id"));
					bpapi_insert(&ba, "action_id", cmd_getval(cs, "action_id"));
				} else {
					cs->error = "ERROR_AD_ID_MISSING";
					cs->status = "TRANS_ERROR";
					state->state = LOADAD_ERROR;

					goto error;
				}

				state->state = LOADAD_LOAD_DATA;

				if (cmd_haskey(cs, "token"))
					bpapi_insert(&ba, "get_ad_history", "1");

				sql_worker_template_newquery(worker, "sql/loadaction.sql", &ba);
				bpapi_output_free(&ba);
				bpapi_simplevars_free(&ba);
				bpapi_vtree_free(&ba);
				return NULL;

			case LOADAD_LOAD_DATA:
				if (worker->sw_state == RESULT_DONE) {
					state->state = LOADAD_LOAD_UPSELLING;
					continue;
				}
				/* Parse the result for the current SELECT */
				state->action = parse_loadaction(worker, cs->ts, state->action);
				return NULL;

			case LOADAD_LOAD_UPSELLING:
				BPAPI_INIT(&ba, NULL, pgsql);
				bpapi_insert(&ba, "ad_id", cmd_getval(cs, "ad_id"));
				state->state = LOADAD_LOAD_UPSELLING_DATA;

				sql_worker_template_query_flags("pgsql.queue", SQLW_FINAL_CALLBACK,
								"sql/get_pending_upselling.sql", &ba,
								loadad_fsm, state, cs->ts->log_string);

				bpapi_output_free(&ba);
				bpapi_simplevars_free(&ba);
				bpapi_vtree_free(&ba);
				return NULL;

			case LOADAD_LOAD_UPSELLING_DATA:
				if (worker->sw_state == RESULT_DONE) {
					state->state = LOADAD_SHOWAD;
					continue;
				}
				/* Parse the result for the current SELECT */
				state->action = parse_loadaction(worker, cs->ts, state->action);
				return NULL;

			case LOADAD_SHOWAD:
			{
				int has_store = 0;
				int did_check_passwd = 0;

				transaction_logprintf(cs->ts, D_DEBUG, "ENTER LOADAD_SHOWAD");

				TAILQ_FOREACH(action_param, &state->action->h, entry) {
					if (strcmp(action_param->prefix, "ad") == 0) {
						if (strcmp(action_param->key, "passwd") == 0) {
							/* Password check */
							if (!check_passwd(cs, action_param->value)) {
								transaction_printf(cs->ts, "passwd:ERROR_PASSWORD_WRONG\n");
								transaction_logprintf(cs->ts, D_DEBUG, "Password mismatch");
								state->state = LOADAD_ERROR;
								cs->status = "TRANS_ERROR";
								goto error;
							}
							did_check_passwd = 1;
						} else if (strcmp(action_param->key, "salted_passwd") == 0) {
							/* Salted password check */
							if (!check_passwd(cs, action_param->value)) {
								transaction_printf(cs->ts, "passwd:ERROR_PASSWORD_WRONG\n");
								transaction_logprintf(cs->ts, D_DEBUG, "Password mismatch");
								state->state = LOADAD_ERROR;
								cs->status = "TRANS_ERROR";
								goto error;
							}
							did_check_passwd = 1;
						} else if (strcmp(action_param->key, "status") == 0) {
							/* Edit/renew on non-active check */
							if (!check_edit_active_ad(cs, action_param->value, state->auth_type)) {
								transaction_printf(cs->ts, "ad_id:ERROR_AD_NOT_ACTIVE\n");
								transaction_logprintf(cs->ts, D_DEBUG, "User tried edit/renew on non-active ad");
								state->state = LOADAD_ERROR;
								cs->status = "TRANS_ERROR";
								goto error;
							}
						} else if (strcmp(action_param->key, "store_id") == 0) {
							/* If we authorized with a store token, check that store id is correct. */
							has_store = 1;

							if (state->auth_type && strcmp(state->auth_type, "admin") != 0) {
								if (strcmp(state->auth_type, "store") != 0
								    || !state->auth_resource
								    || strcmp(state->auth_resource, action_param->value) != 0) {
									transaction_logprintf(cs->ts, D_INFO, "User had insufficient privs for store ad");
									cs->error = "ERROR_INSUFFICIENT_PRIVS";
									cs->status = "TRANS_ERROR";
									state->state = LOADAD_ERROR;
									goto error;
								}
							}
						}
					}
				}

				if (!did_check_passwd && !check_passwd(cs, NULL)) {
					transaction_printf(cs->ts, "passwd:ERROR_PASSWORD_WRONG\n");
					transaction_logprintf(cs->ts, D_DEBUG, "Ad has no passwd!?");
					state->state = LOADAD_ERROR;
					cs->status = "TRANS_ERROR";
					goto error;
				}

				if (!has_store && state->auth_type
				    && strcmp(state->auth_type, "store") == 0) {
					transaction_logprintf(cs->ts, D_INFO, "User had insufficient privs for non store ad");
					cs->error = "ERROR_INSUFFICIENT_PRIVS";
					cs->status = "TRANS_ERROR";
					state->state = LOADAD_ERROR;
					goto error;
				}

				if (state->state != LOADAD_ERROR) {
					const char *store_email = NULL;

					transaction_logoff(cs->ts);
					TAILQ_FOREACH(action_param, &state->action->h, entry) {

						if (!action_param->value && !strstr(action_param->prefix, "change"))
							continue;

						if (!strcmp(action_param->prefix, "ad") &&
						    !strcmp(action_param->key, "store_id") &&
						    cmd_haskey(cs, "token")) {
							store_email = parse_loadaction_get_value(state->action, "users", "email");
						} else if (!strcmp(action_param->prefix, "ad") && !strcmp(action_param->key, "passwd"))
							continue;

						if (!action_param->value)
							transaction_printf(cs->ts, "%s.%s:\n",
									action_param->prefix,
									action_param->key);
						else if (strchr(action_param->value, '\n') != NULL || strchr(action_param->value, '\r') != NULL) {
							transaction_printf(cs->ts, "blob:%ld:%s.%s\n%s\n",
									   (unsigned long)strlen(action_param->value),
									   action_param->prefix,
									   action_param->key,
									   action_param->value);
						} else {
							transaction_printf(cs->ts,
									   "%s.%s:%s\n",
									   action_param->prefix,
									   action_param->key,
									   action_param->value);
						}

					}
					transaction_logon(cs->ts);

					if (store_email) {
						BPAPI_INIT(&ba, NULL, pgsql);
						bpapi_insert(&ba, "store_email", store_email);
						sql_worker_template_query_flags(state->dbc, 0,
							"sql/load_stores.sql", &ba,
							loadad_fsm, state, cs->ts->log_string);
						bpapi_output_free(&ba);
						bpapi_simplevars_free(&ba);
						bpapi_vtree_free(&ba);
						state->state = LOADAD_LIST_STORES;
						parse_loadaction_cleanup(state->action);
						state->action = NULL;
						return NULL;
					} else
						state->state = LOADAD_DONE;

					parse_loadaction_cleanup(state->action);
					state->action = NULL;
				}
				break;
			}

			case LOADAD_LIST_STORES:
				transaction_printf(cs->ts, "stores:");
				while (worker->next_row(worker)) {
					transaction_printf(cs->ts, "%s:%s\t",
							worker->get_value(worker, 0), worker->get_value(worker, 1));
				}
				transaction_printf(cs->ts, "\n");
				state->state = LOADAD_DONE;
				/* Let final callback go to copy images. */
				return NULL;

			case LOADAD_ERROR:
				goto error;

			default:
				transaction_logprintf(cs->ts, D_ERROR, "Unknown state in loadad_fsm: %d",
						      state->state);
				goto error;
		}
	}
error:
	if (worker != NULL)
		sql_worker_set_wflags(worker, 0);
	if (state->action)
		parse_loadaction_cleanup(state->action);

	loadad_done(state);
	return NULL;
}

static void
loadad(struct cmd *cs) {
	struct loadad_state *state = zmalloc(sizeof (*state));
	state->cs = cs;

	state->state = LOADAD_INIT;

	loadad_fsm(state, NULL, NULL);
}

ADD_COMMAND(loadad, NULL, loadad, parameters, NULL);
