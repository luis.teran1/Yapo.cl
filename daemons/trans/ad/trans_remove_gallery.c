#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <time.h>

#include "trans.h"
#include "util.h"
#include <ctemplates.h>
#include "validators.h"
#include <bconf.h>
#include <event.h>

typedef enum {
	REMOVE_GALLERY_DONE,
	REMOVE_GALLERY_ERROR,
	REMOVE_GALLERY_INIT,
	REMOVE_GALLERY_RESULT,
	REMOVE_GALLERY_INTERNAL_RESULT,
	REMOVE_GALLERY_SEND_MAIL
} state_t;

static const char *state_lut[] = {
	"REMOVE_GALLERY_DONE",
	"REMOVE_GALLERY_ERROR",
	"REMOVE_GALLERY_INIT",
	"REMOVE_GALLERY_RESULT",
	"REMOVE_GALLERY_INTERNAL_RESULT",
	"REMOVE_GALLERY_SEND_MAIL"
};

struct remove_gallery_state {
	state_t state;
        char *sql;
	char *errstr;
	struct cmd *cs;
        struct event timeout_event;
};

static struct c_param parameters[] = {
	{"remote_addr",         P_REQUIRED, "v_ip"},
	{"ad_id",               P_REQUIRED, "v_id"},
	{NULL, 0}
};

static const char *remove_gallery_fsm(void *, struct sql_worker *, const char *);

extern struct bconf_node *bconf_root;

/*****************************************************************************
 * remove_gallery_done
 * Exit function.
 **********/
static void
remove_gallery_done(void *v) {
	struct remove_gallery_state *state = v;
	struct cmd *cs = state->cs;
	free(state);
	cmd_done(cs);
}

/*****************************************************************************
 * bind_insert
 * Initialise template API before a call to sql_worker_template_query().
 **********/
static void
bind_insert(struct bpapi *ba, struct remove_gallery_state *state) {
	struct c_param *param;

	for (param = parameters; param->name; param++) {
		if (cmd_haskey(state->cs, param->name))
			bpapi_insert(ba, param->name, cmd_getval(state->cs, param->name));
	}
}

static void
remove_gallery_admail_cb (struct internal_cmd *intcmd, const char *line, void *data) {
	struct remove_gallery_state *state = data;

	if (!line)
		remove_gallery_fsm(state, NULL, state->errstr);
	else if (!state->errstr) {
		const char *cp = strchr(line, ':');

		if (cp && IS_ERROR (cp + 1))
			state->errstr = xstrdup(cp + 1);
	}
}


static const char *
remove_gallery_fsm(void *v, struct sql_worker *worker, const char *errstr) {
	struct remove_gallery_state *state = v;
	struct cmd *cs = state->cs;
        struct bpapi ba;

	if (state->sql) {
		free(state->sql);
		state->sql = NULL;
	}

	if (errstr) {
                transaction_logprintf(cs->ts, D_ERROR, "remove_gallery_fsm statement failed (%s(%d), %s)",
				      state_lut[state->state],
                                      state->state,
                                      errstr);
		if (errstr && strstr(errstr, "ERROR_AD_NOT_FOUND")) {
			cs->status = "TRANS_ERROR";
			cs->error = "ERROR_AD_NOT_FOUND";
		} else if (errstr && strstr(errstr, "ERROR_AD_MISSING_GALLERY")) {
			cs->status = "TRANS_ERROR";
			cs->error = "ERROR_AD_MISSING_GALLERY";
		} else {
			cs->status = "TRANS_DATABASE_ERROR";
			cs->message = xstrdup(errstr);
		}
		state->state = REMOVE_GALLERY_ERROR;
        }

	while (1) {
		transaction_logprintf(cs->ts, D_DEBUG, "remove_gallery_fsm(%p, %p, %p), state=%s(%d)",
				      v, worker, errstr, state_lut[state->state], state->state);

		switch (state->state) {
		case REMOVE_GALLERY_INIT:
			state->state = REMOVE_GALLERY_RESULT;

			BPAPI_INIT(&ba, NULL, pgsql);
			bind_insert(&ba, state);

			sql_worker_template_query(&config.pg_master,
						  "sql/call_remove_gallery.sql", &ba,
						  remove_gallery_fsm, state, cs->ts->log_string);

			bpapi_output_free(&ba);
			bpapi_simplevars_free(&ba);
			bpapi_vtree_free(&ba);
			return NULL;

                case REMOVE_GALLERY_RESULT:
			if (worker->next_row(worker)) {
                        	state->state = REMOVE_GALLERY_SEND_MAIL;
			} else {
				state->state = REMOVE_GALLERY_ERROR;
			}
			continue;

                case REMOVE_GALLERY_SEND_MAIL:
                        state->state = REMOVE_GALLERY_INTERNAL_RESULT;

			transaction_internal_printf(remove_gallery_admail_cb, state,
				"cmd:admail\n"
				"ad_id:%s\n"
				"mail_type:gallery_expired\n"
				"commit:1\n"
				"end\n",
				cmd_getval(cs, "ad_id")
			);
                        return NULL;

		case REMOVE_GALLERY_INTERNAL_RESULT:
			state->state = REMOVE_GALLERY_DONE;
			if (state->errstr) {
				cs->status = "TRANS_ERROR";
				cs->error = "ERROR_REMOVE_GALLERY_SEND_MAIL_FAILED";
				state->state = REMOVE_GALLERY_ERROR;
			}
			continue;

		case REMOVE_GALLERY_DONE:
			remove_gallery_done(state);
			return NULL;

		case REMOVE_GALLERY_ERROR:
			transaction_logprintf(cs->ts, D_ERROR, "Error state");
			remove_gallery_done(state);
			return NULL;

		default:
			/* MAJOR STATE ERROR */
			cs->status = "TRANS_ERROR";
			transaction_logprintf(cs->ts, D_ERROR, "Unknown state (%d)",
					      state->state);

			remove_gallery_done(state); /* If this state were run there would be a memory leak */
			return NULL;

		}
	}
}

/*****************************************************************************
 * remove_gallery
 * Main function that initialise a state-structure and calls the state machine.
 **********/
static void
remove_gallery(struct cmd *cs) {
	transaction_logprintf(cs->ts, D_DEBUG, "Start remove_gallery transaction");
	struct remove_gallery_state *state;
	state = zmalloc(sizeof(*state));
	state->cs = cs;
	state->state = REMOVE_GALLERY_INIT;

	remove_gallery_fsm(state, NULL, NULL);
}

ADD_COMMAND(remove_gallery,     /* Name of command */
		NULL,
            remove_gallery,     /* Main function name */
            parameters,  /* Parameters struct */
            "remove_gallery command");/* Command description */
