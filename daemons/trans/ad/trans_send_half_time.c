#include "factory.h"

static struct c_param parameters[] = {
	{"special_categories", P_OPTIONAL, "v_bool"},
	{NULL, 0}
};

static struct factory_data data = {
	"monthly_delete",
	FACTORY_RES_SHORT_FORM
};

static const struct factory_action half_time_no_categories = FA_SQL("", "pgsql.slave", "sql/half_time_exclude_categories.sql", 0);
static const struct factory_action half_time_with_categories = FA_SQL("", "pgsql.slave", "sql/half_time_by_categories.sql", 0);

static const struct factory_action *
is_special_categories(struct cmd *cs, struct bpapi *ba) {
	if (cmd_haskey(cs, "special_categories"))
		return &half_time_with_categories;
	return &half_time_no_categories;
}


static const struct factory_action actions[]= {
	FA_COND(is_special_categories),
	FA_OUT("trans/call_send_half_time.txt"),
	FA_DONE(),
	{0}
};

FACTORY_TRANS(send_half_time, parameters, data, actions);
