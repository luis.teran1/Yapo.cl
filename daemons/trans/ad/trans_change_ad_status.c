#include "factory.h"
#include "trans_deletead.h"
#include "utils/redis_delete_ad.h"
#include "utils/refresh_session.h"

static struct c_param parameters[] = {
	{"list_id;ad_id", P_XOR, "v_list_id;v_id"},
	{"new_status", P_REQUIRED, "v_array:active,deactivated"},
	{"remote_addr", P_REQUIRED, "v_ip"},
	{"salted_passwd;token;batch", P_XOR, "v_passwd_sha1_not_required;v_token:adminad.edit_ad;v_bool"},
	{"remote_browser", P_OPTIONAL, "v_remote_browser"},
	{"source", P_OPTIONAL, "v_source"},
	{"reason",                  0,     "v_reason"},
	{"testimonial",             0,     "v_testimonial"},
	{"site_area",               0,     "v_site_area"},
	{"do_not_send_mail",        0,     "v_bool"},
	{"monthly",                 0,     "v_bool"},
	{"api_token",               0,     "v_apitoken"},
	{"reg_time",                0,     "v_string_isprint"},
	{"account_id",              0,     "v_account_id"},
	{NULL, 0}
};

static struct factory_data data = {
	"change_ad_status",
	FACTORY_RES_SHORT_FORM
};

static const struct factory_action refresh_session = FA_FUNC(refresh_account);

static const struct factory_action *
refresh_account_if_needed(struct cmd *cs, struct bpapi *ba) {
	int is_pro = bpapi_has_key(ba, "is_pro_for")
	           ? *bpapi_get_element(ba, "is_pro_for", 0)
	           : 0;
	if (is_pro)
		return &refresh_session;
	return NULL;
}

static int
words_ranking_info(struct cmd *cs, struct bpapi *ba) {
	if (!strcmp(cmd_getval(cs, "new_status"), "deactivated")) {
		bpapi_insert(ba, "raap_action", "deactivated");
		save_deletead_to_redis(cs, ba);
		remove_all_ad_products(cs, ba);
	}
 	return 0;
}

static const struct factory_action send_mail_deactivated = FA_TRANS("trans/call_deactivated_mail.txt", "mail_", FATRANS_OUT);

static const struct factory_action *
is_deactivating_mail(struct cmd *cs, struct bpapi *ba) {
    if (!strcmp(cmd_getval(cs, "new_status"), "deactivated") && (cmd_haskey(cs, "salted_passwd") || cmd_haskey(cs, "api_token"))) {
        return &send_mail_deactivated;
    }
    return NULL;
}

static const struct factory_action done = FA_DONE();
static const struct factory_action queue_qd = FA_TRANS("trans/call_queued_qd.txt", "qd_", FATRANS_OUT);

static const struct factory_action *
is_deactivating_delete(struct cmd *cs, struct bpapi *ba) {
    if (!strcmp(cmd_getval(cs, "new_status"), "deactivated")) {
        return &queue_qd;
    }
    return NULL;
}

static const struct factory_action *
try_archive_unpaid_ad(struct cmd *cs, struct bpapi *ba) {
	const char *new_status = cmd_getval(cs, "new_status");
	const char *first_action_status = bpapi_get_element(ba, "first_action_status", 0);
	if (first_action_status && !strcmp(first_action_status, "unpaid") && !strcmp(new_status, "deactivated")) {
		int error = archive_unpaid_ad(cs, ba);
		if (!error)
			return &done;
	}
	return NULL;
}

static const struct factory_action actions[]= {
	/* Check for unpaid ads */
	FA_SQL("", "pgsql.slave", "sql/get_first_action_status.sql", 0),
	/* Archives unpaid ads and terminates if succeeds */
	FA_COND(try_archive_unpaid_ad),
	/* Change status for regular ads */
	FA_SQL("", "pgsql.master", "sql/call_change_ad_status.sql", FASQL_OUT),
	FA_COND(refresh_account_if_needed),
	FA_FUNC(words_ranking_info),
	/* Send mail */
	FA_COND(is_deactivating_mail),
	/* Queue delete */
	FA_COND(is_deactivating_delete),
	{0}
};

FACTORY_TRANS(change_ad_status, parameters, data, actions);
