#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <time.h>

#include "trans.h"
#include <ctemplates.h>
#include "validators.h"
#include <bconf.h>
#include <event.h>

extern struct bconf_node *bconf_root;

#define COPY_EXPORT_DONE 0
#define COPY_EXPORT_ERROR 1
#define COPY_EXPORT_START 2
#define COPY_EXPORT_RESULT 3

struct copy_export_state {
	int state;
	struct cmd *cs;
	struct bpapi ba;
	time_t timestamp;
};

static void copy_export_done(void *v) {
	struct copy_export_state *state = v;
	struct cmd *cs = state->cs;

	bpapi_output_free(&state->ba);
	bpapi_simplevars_free(&state->ba);
	bpapi_vtree_free(&state->ba);

	free(state);
	cmd_done(cs);
}

static const char *
copy_export_fsm(void *v, struct sql_worker *worker, const char *errstr) {
	struct copy_export_state *state = v;
	struct cmd *cs = state->cs;

	transaction_logprintf(cs->ts, D_DEBUG, "copy_export fsm");

	if (state->state == COPY_EXPORT_ERROR) {
		copy_export_done(state);
		return NULL;
	}

	/*
	 * General error handling for db errors.
	 */
	if (errstr) {
		transaction_logprintf(cs->ts, D_DEBUG, "copy_export error: %s", errstr);
		cs->status = "TRANS_DATABASE_ERROR";
		cs->message = xstrdup(errstr);
		state->state = COPY_EXPORT_ERROR;
	}

	while (state->state != COPY_EXPORT_DONE && state->state != COPY_EXPORT_ERROR) {
		switch (state->state) {
		case COPY_EXPORT_START:
			transaction_logprintf(cs->ts, D_DEBUG, "copy_export send query");

			BPAPI_INIT(&state->ba, NULL, pgsql);
			bpapi_insert(&state->ba, "time_from", cmd_getval(cs, "time_from"));
			bpapi_insert(&state->ba, "time_to", cmd_getval(cs, "time_to"));
			bpapi_insert(&state->ba, "schema", cmd_getval(cs, "schema"));
			bpapi_insert(&state->ba, "mod", cmd_getval(cs, "mod"));

			sql_worker_template_query(&config.pg_master, "sql/copy_export.sql", &state->ba,
						  copy_export_fsm, state, cs->ts->log_string);

			state->state = COPY_EXPORT_RESULT;
			return NULL;

		case COPY_EXPORT_RESULT:
			if (worker->next_row(worker)) {
				/* report ad archivation result */
			}

			state->state = COPY_EXPORT_DONE;
			break;

		default:
			transaction_logprintf(cs->ts, D_ERROR, "Unknown copy_export state %d", state->state);
			cs->status = "TRANS_DATABASE_ERROR";
			state->state = COPY_EXPORT_ERROR;
		}
	}

	copy_export_done(state);
	return NULL;
}

static void
copy_export(struct cmd *cs) {
	transaction_logprintf(cs->ts, D_DEBUG, "copy_export command started");
	struct copy_export_state *state;
	state = zmalloc(sizeof(*state));

	state->cs = cs;
	state->state = COPY_EXPORT_START;

	copy_export_fsm(state, NULL, NULL);
}

struct validator v_schema[] = {
	VALIDATOR_LEN(-1, 32, "ERROR_SCHEMA_TOO_LONG"),
	{0}
};

ADD_VALIDATOR(v_schema);


static struct c_param copy_export_parameters[] = {
	{"time_from", 0, "v_timestamp"},
	{"time_to", 0, "v_timestamp"},
	{"schema", P_REQUIRED, "v_schema"},
	{"mod", P_REQUIRED, "v_integer"},
	{NULL, 0, NULL}
};

ADD_COMMAND(copy_export, NULL, copy_export, copy_export_parameters, NULL);

