#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <time.h>

#include "trans.h"
#include "util.h"
#include <ctemplates.h>
#include "validators.h"
#include <bconf.h>
#include <event.h>

#define BUMP_IMPORTED_DONE   0
#define BUMP_IMPORTED_ERROR  1
#define BUMP_IMPORTED_INIT   2
#define BUMP_IMPORTED_PROCESS 3
#define BUMP_IMPORTED_DO 4
#define BUMP_IMPORTED_RESULT 5

struct bump_todo {
	TAILQ_ENTRY(bump_todo) link;
	int ad_id;
	char *external_ad_id;
	char *new_list_time;
	char *link_type;
	int do_delete;
};

struct bump_imported_state {
	int state;
	struct cmd *cs;
	int n_failed;
	int rows;
	TAILQ_HEAD(,bump_todo) todo;
};

static struct c_param parameters[] = {
	{"remote_addr", P_REQUIRED, "v_ip"},
	{"remote_browser", 0, "v_remote_browser"},
	{NULL, 0}
};

extern struct bconf_node *bconf_root;

static const char *bump_imported_fsm(void *, struct sql_worker *, const char *);

static void
bump_bumptrans_cb(struct internal_cmd *intcmd, const char *line, void *data) {
	struct bump_imported_state *state = data;

	if (!line) {
		bump_imported_fsm(state, NULL, NULL);
		return;
	}

 	if (strstr(line, "TRANS_ERROR") || strstr(line, "TRANS_DATABASE_ERROR")) {
		++state->n_failed;
	}

}

static void
bump_bumptrans(struct bump_todo *bt, struct bump_imported_state *state) {
	struct internal_cmd *cmd = zmalloc(sizeof (*cmd));

	cmd->cb = bump_bumptrans_cb;
	cmd->data = state;

	if (bt->do_delete) {
		xasprintf(&cmd->cmd_string,
			  "cmd:deletead\n"
			  "do_not_send_mail:1\n"
			  "reason:import_deleted\n"
			  "external_ad_id:%s\n"
			  "link_type:%s\n"
			  "remote_addr:127.0.0.1\n"
			  "commit:1\n"
			  "end\n",
			  bt->external_ad_id,
			  bt->link_type
			  );
	} else
		xasprintf(&cmd->cmd_string,
			  "cmd:bump_ad\n"
			  "batch:1\n"
			  "batch_bump:1\n"
			  "ad_id:%d\n"
			  "%s%s%s"
			  "remote_addr:%s\n"
			  "commit:1\n"
			  "end\n",
			  bt->ad_id,
			  (bt->new_list_time ? "new_list_time:" : ""),
			  (bt->new_list_time ? bt->new_list_time : ""),
			  (bt->new_list_time ? "\n" : ""),
			  cmd_getval(state->cs, "remote_addr")
			  );

	transaction_internal(cmd);
}

/*****************************************************************************
 * bump_imported_done
 * Exit function.
 **********/
static void
bump_imported_done(void *v) {
	struct bump_imported_state *state = v;
	struct cmd *cs = state->cs;
	free(state);
	cmd_done(cs);
}

/*****************************************************************************
 * bind_insert
 * Initialise template API before a call to sql_worker_template_query().
 **********/
static void
bind_insert(struct bpapi *ba, struct bump_imported_state *state) {
	struct c_param *param;

	for (param = parameters; param->name; param++) {
		if (cmd_haskey(state->cs, param->name))
			bpapi_insert(ba, param->name, cmd_getval(state->cs, param->name));
	}
}

/*****************************************************************************
 * bump_imported_fsm
 * State machine
 **********/
static const char *
bump_imported_fsm(void *v, struct sql_worker *worker, const char *errstr) {
	struct bump_imported_state *state = v;
	struct cmd *cs = state->cs;
        struct bpapi ba;

	/* 
	 * General error handling for db errors.
	 */
	if (errstr) {
                transaction_logprintf(cs->ts, D_ERROR, "bump_imported_fsm statement failed (%d, %s)", 
                                      state->state,
                                      errstr);
                cs->status = "TRANS_DATABASE_ERROR";
                cs->message = xstrdup(errstr);
		state->state = BUMP_IMPORTED_ERROR;
        }

	while (1) {
		transaction_logprintf(cs->ts, D_DEBUG, "bump_imported_fsm(%p, %p, %p), state=%d", 
				      v, worker, errstr, state->state);

		switch (state->state) {
		case BUMP_IMPORTED_INIT:
			/* next state */
			state->state = BUMP_IMPORTED_PROCESS;

			/* Initialise the templateparse structure */
			BPAPI_INIT(&ba, NULL, pgsql);
			bind_insert(&ba, state);

			/* Register SQL query with our bump_imported_fsm() as callback function */
			/* Make sure to switch to slave if read-only query. */
			sql_worker_template_query(&config.pg_master, 
						  "sql/bump_imported.sql", &ba, 
						  bump_imported_fsm, state, cs->ts->log_string);

			/* Clean-up */
			bpapi_output_free(&ba);
			bpapi_simplevars_free(&ba);
			bpapi_vtree_free(&ba);

			/* Return to libevent */
			return NULL;

		case BUMP_IMPORTED_PROCESS:
		{

			while (worker->next_row(worker)) {
				struct bump_todo *bt = zmalloc(sizeof(*bt));
				bt->ad_id = atoi(worker->get_value(worker, 0));
				bt->external_ad_id = xstrdup(worker->get_value_byname(worker, "external_ad_id"));
				bt->link_type = xstrdup(worker->get_value_byname(worker, "link_type"));
				bt->new_list_time = worker->is_null_byname(worker, "new_list_time") ? NULL : xstrdup(worker->get_value_byname(worker, "new_list_time"));
				bt->do_delete = worker->is_true_byname(worker, "do_delete");
				TAILQ_INSERT_TAIL(&state->todo, bt, link);
				++state->rows;
			}


			if (state->rows > 0) {
				state->state = BUMP_IMPORTED_DO;
			} else {
				state->state = BUMP_IMPORTED_RESULT;
			}
			continue;
		}

		case BUMP_IMPORTED_DO:
		{
			struct bump_todo *bt;
			bt = TAILQ_FIRST(&state->todo);
			if (bt == NULL) {
				state->state = BUMP_IMPORTED_RESULT;
				continue;
			}
			TAILQ_REMOVE(&state->todo, bt, link);
			bump_bumptrans(bt, state);
			if (bt->new_list_time)
				free(bt->new_list_time);
			if (bt->external_ad_id)
				free(bt->external_ad_id);
			if (bt->link_type)
				free(bt->link_type);
			free(bt);
			return NULL;
		}


                case BUMP_IMPORTED_RESULT:
		{
			transaction_printf(cs->ts, "total:%d\n", state->rows);
			if (state->n_failed != 0) {
				cs->status = "TRANS_ERROR";
				state->state = BUMP_IMPORTED_ERROR;
				transaction_printf(cs->ts, "failed:%d\n", state->n_failed);
			} else {
				state->state = BUMP_IMPORTED_DONE;
			}
			continue;
		}

		case BUMP_IMPORTED_DONE:
			bump_imported_done(state);
			return NULL;

		case BUMP_IMPORTED_ERROR:
			transaction_logprintf(cs->ts, D_ERROR, "Error state");
			bump_imported_done(state);
			return NULL;

		default:
			/* MAJOR STATE ERROR */
			cs->status = "TRANS_ERROR";
			transaction_logprintf(cs->ts, D_ERROR, "Unknown state (%d)", 
					      state->state);
			bump_imported_done(state);
			return NULL;

		}
	}
}

/*****************************************************************************
 * bump_imported
 * Main function that initialise a state-structure and calls the state machine.
 **********/
static void
bump_imported(struct cmd *cs) {
	transaction_logprintf(cs->ts, D_DEBUG, "Start Bump imported transaction");
	struct bump_imported_state *state;
	state = zmalloc(sizeof(*state));
	state->cs = cs;
	state->state = BUMP_IMPORTED_INIT;
	TAILQ_INIT(&state->todo);
	bump_imported_fsm(state, NULL, NULL);
}

ADD_COMMAND(bump_imported,     /* Name of command */
		NULL,
            bump_imported,     /* Main function name */
            parameters,  /* Parameters struct */
            "Bump imported command");/* Command description */
