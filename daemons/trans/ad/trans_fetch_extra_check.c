#include "factory.h"

static struct c_param parameters[] = {
	{"list_id", 0, "v_id"},
	{"ad_id", 0, "v_id"},
	{NULL, 0}
};

FACTORY_TRANSACTION(fetch_extra_check, &config.pg_slave, "sql/fetch_extra_check.sql", NULL, parameters, FACTORY_NO_RES_CNTR | FACTORY_RES_SHORT_FORM );

