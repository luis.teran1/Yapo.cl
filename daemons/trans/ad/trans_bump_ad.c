#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <time.h>

#include "trans.h"
#include "trans_at.h"
#include "util.h"
#include <ctemplates.h>
#include "validators.h"
#include <bconf.h>
#include <event.h>
#include "log_event.h"

#include "utils/send_backend_event.h"

#define BUMP_AD_DONE  0
#define BUMP_AD_ERROR 1
#define BUMP_AD_UPDATEDB  2
#define BUMP_AD_UPDATEDB_DONE  3
#define BUMP_SEND_MAIL 4

extern struct bconf_node* bconf_root;

struct bump_ad_state {
	int state;
	char *errstr;
	struct cmd *cs;
};

static struct c_param parameters[] = {
	{"ad_id",           P_REQUIRED,	"v_ad_id"},
	{"token;batch",     P_XOR,		"v_token:adminad.bump;v_bool"},
	{"remote_addr",     0,		"v_ip"},
	{"batch_bump",      0,		"v_bool"},
	{"remote_browser",  0,		"v_remote_browser"},
	{"action_id",       0,		"v_id"},
	{"new_list_time",   0,		"v_timestamp"},
	{"do_not_send_mail",0,		"v_bool"},
	{"mail_type",       0,		"v_array:bump_ok,weekly_bump_ok,mail_first_ad_bump"},
	{"remaining_bumps", 0,		"v_integer"},
	{"doc_type",        0,		"v_array:bill,invoice,voucher"},
	{"transition_type", 0,		"v_array:bump,autobump,smartbump"},
	{"update_ad_status",0,		"v_bool"},
	{NULL, 0}
};

static const char *bump_ad_fsm(void *, struct sql_worker *, const char *);

/*****************************************************************************
 * bump_ad_done
 * Exit function.
 **********/
static void
bump_ad_done(void *v) {
	struct bump_ad_state *state = v;
	struct cmd *cs = state->cs;

	free(state);
	cmd_done(cs);
}


static void
bump_ad_mail_cb(struct internal_cmd *intcmd, const char *line, void *data) {
	struct bump_ad_state *state = data;

	if (!line) {
		bump_ad_fsm(state, NULL, NULL);
		return;
	}

	if (!strncmp(line, "status:", 7)) {
		if (strstr(line, "TRANS_OK"))  {
			state->state = BUMP_AD_DONE;
		} else {
			/* We do not want to give a hard error for the email so we do not set cs->status to error */
			transaction_logprintf(state->cs->ts, D_ERROR, "bump_ad_mail_cb (ad_id %s)",
														cmd_getval(state->cs, "ad_id"));
			syslog_ident("product_err", "%s: failed bump_ad (ad_id, remote_addr, state, status, error) (%s, %s, %d, EMAIL_ERROR, %s)",
								cmd_haskey(state->cs, "transition_type") ? cmd_getval(state->cs, "transition_type") : "bump",
								cmd_getval(state->cs, "ad_id"),
								cmd_haskey(state->cs, "remote_addr") ? cmd_getval(state->cs, "remote_addr") : "", 
								state->state,
								line);
			state->state = BUMP_AD_ERROR;
		}
	}
}

/*****************************************************************************
 * bind_insert
 * Initialise template API before a call to sql_worker_template_query().
 **********/
static void
bind_insert(struct bpapi *ba, struct bump_ad_state *state) {
	struct c_param *param;

	for (param = parameters; param->name; param++) {
		if (cmd_haskey(state->cs, param->name)) {
			bpapi_insert(ba, param->name, cmd_getval(state->cs, param->name));
		}
	}

	if (cmd_haskey(state->cs, "token"))
		bpapi_insert(ba, "token", cmd_getval(state->cs, "token"));

	/* CHUS 494 */
	if (cmd_haskey(state->cs, "batch"))
		bpapi_insert(ba, "batch", cmd_getval(state->cs, "batch"));
}

/*****************************************************************************
 * bump_ad_fsm
 * State machine
 **********/
static const char *
bump_ad_fsm(void *v, struct sql_worker *worker, const char *errstr) {
	struct bump_ad_state *state = v;
	struct cmd *cs = state->cs;
	struct bpapi ba;
	const char *mail_type = cmd_haskey(cs, "mail_type") ? cmd_getval(cs, "mail_type") : "bump_ok";
	int remaining_bumps = cmd_haskey(cs, "remaining_bumps") ? atoi(cmd_getval(cs, "remaining_bumps")) : 0;

	/*
	 * General error handling for db errors.
	 */
	if (errstr) {
		transaction_logprintf(cs->ts, D_ERROR, "bump_ad_fsm statement failed (%d, %s)", state->state, errstr);
		sql_worker_set_wflags(worker, 0);
		if (strstr(errstr, "ERROR_AD_NOT_ACTIVE")) {
			cs->status = "TRANS_ERROR";
			cs->error = "ERROR_AD_NOT_ACTIVE";
			if (cmd_haskey(cs, "action_id")) {
				/* Try again in exactly one day to make sure we are trying during indexing hours. */
				at_register_printf(86400, "bump", cmd_getval(cs, "ad_id"), 0,
						"cmd:bump_ad\n"
						"ad_id:%s\n"
						"action_id:%s\n"
						"commit:1\n"
						"batch:1\n",
						cmd_getval(cs, "ad_id"),
						cmd_getval(cs, "action_id")
						);
			}
		} else {
			if (strstr(errstr, "ERROR_AD_NOT_FOUND")) {
				cs->status = "TRANS_ERROR";
				cs->error = "ERROR_AD_NOT_FOUND";
			} else {
				cs->status = "TRANS_DATABASE_ERROR";
				cs->message = xstrdup(errstr);
			}
		}

		syslog_ident("product_err", "%s: failed bump_ad (ad_id, remote_addr, state, status, error) (%s, %s, %d, %s, %s)",
			(cmd_haskey(state->cs, "transition_type")) ? cmd_getval(state->cs, "transition_type") : "bump",
			cmd_getval(cs, "ad_id"),
			(cmd_haskey(cs, "remote_addr")) ? cmd_getval(cs, "remote_addr") : "",
			state->state,
			cs->status,
			strtrchr(errstr, "\n", ' '));
		state->state = BUMP_AD_ERROR;
	}

	while (1) {
		transaction_logprintf(cs->ts, D_DEBUG, "bump_ad_fsm(%p, %p, %p), state=%d",
				      v, worker, errstr, state->state);

		switch (state->state) {
		case BUMP_AD_UPDATEDB:
			{
				state->state = BUMP_AD_UPDATEDB_DONE;

				BPAPI_INIT(&ba, NULL, pgsql);
				bind_insert(&ba, state);

				sql_worker_template_query_flags(&config.pg_master, SQLW_FINAL_CALLBACK,
								"sql/call_bump_ad.sql", &ba,
								bump_ad_fsm, state, cs->ts->log_string);

				bpapi_output_free(&ba);
				bpapi_simplevars_free(&ba);
				bpapi_vtree_free(&ba);
 				return NULL;
			}
		case BUMP_AD_UPDATEDB_DONE:
			if (worker->sw_state == RESULT_DONE) {
				if (!cmd_haskey(state->cs, "do_not_send_mail") || strcmp(cmd_getval(cs, "do_not_send_mail"), "1") != 0)
					state->state = BUMP_SEND_MAIL;
				else
					state->state = BUMP_AD_DONE;
				continue;
			}

			worker->next_row(worker);
			transaction_printf(cs->ts, "action_id:%s\n", worker->get_value_byname(worker, "o_action_id"));
			transaction_printf(cs->ts, "list_time:%s\n", worker->get_value_byname(worker, "o_list_time"));

			// Check if it should send a backend event
			int be_enabled = bconf_get_int(trans_bconf, "bump.backend_event.send_enabled");
			if (be_enabled) {
				struct bpapi be;
				BPAPI_INIT(&be, NULL, pgsql);
				transaction_logprintf(cs->ts, D_INFO, "SEND ACCOUNT BACKEND EVENT ACTIVE");
				bpapi_insert(&be, "type", "bump");
				bpapi_insert(&be, "param_ad_id", cmd_getval(cs, "ad_id"));
				bpapi_insert(&be, "param_action_id", worker->get_value_byname(worker, "o_action_id"));
				bpapi_insert(&be, "param_list_time", worker->get_value_byname(worker, "o_list_time"));

				publish_backend_event(cs, &be);
				bpapi_output_free(&be);
				bpapi_simplevars_free(&be);
				bpapi_vtree_free(&be);
			}

			return NULL;

		case BUMP_SEND_MAIL:
			{
				char *mailcmd;
				state->state = BUMP_AD_DONE;

				/* FT583 */
				const char *doc_type = "bill";
				if(cmd_getval(cs, "doc_type") != NULL) {
					doc_type = cmd_getval(cs, "doc_type");
				}

				xasprintf(&mailcmd,
					"cmd:admail\n"
					"commit:1\n"
					"ad_id:%s\n"
					"mail_type:%s\n"
					"remaining_bumps:%d\n"
					"doc_type:%s\n"
					"transition_type:%s\n",
					cmd_getval(cs, "ad_id"),
					mail_type,
					remaining_bumps,
					doc_type,
					(cmd_haskey(state->cs, "transition_type")) ? cmd_getval(state->cs, "transition_type") : "bump"
					);

				transaction_internal_printf(bump_ad_mail_cb, state,
					"cmd:queue\n"
					"commit:1\n"
					"queue:bump_mail\n"
					"blob:%zu:command\n"
					"%s\n",
					strlen(mailcmd),
					mailcmd);

				return NULL;
			}
		case BUMP_AD_DONE:
			bump_ad_done(state);

			return NULL;
		case BUMP_AD_ERROR:
			transaction_logprintf(cs->ts, D_ERROR, "Error state");
			if (worker)
				sql_worker_set_wflags(worker, 0);
			bump_ad_done(state);
			return NULL;

		default:
			/* MAJOR STATE ERROR */
			cs->status = "TRANS_ERROR";
			transaction_logprintf(cs->ts, D_ERROR, "Unknown state (%d)",
					      state->state);
			return NULL;

		}
	}
}

/*****************************************************************************
 * bump_ad
 * Main function that initialise a state-structure and calls the state machine.
 **********/
static void
bump_ad(struct cmd *cs) {
	struct bump_ad_state *state;
	state = zmalloc(sizeof(*state));
	state->cs = cs;
	state->state = BUMP_AD_UPDATEDB;

	bump_ad_fsm(state, NULL, NULL);
}

ADD_COMMAND(bump_ad,     /* Name of command */
		NULL,
            bump_ad,     /* Main function name */
            parameters,  /* Parameters struct */
            "Bump an ad");/* Command description */

