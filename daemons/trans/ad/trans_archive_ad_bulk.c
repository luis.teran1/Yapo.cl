#include "trans.h"
#include "command.h"
#include <ctemplates.h>
#include <bconf.h>
#include <time.h>
#include "trans_mail.h"

extern struct bconf_node *bconf_root;

static const char *
archive_ad_bulk_fsm(void *v, struct sql_worker *worker, const char *errstr);

typedef enum {
	ARCHIVE_AD_BULK_INIT,
	ARCHIVE_BY_PURCHASE,
	ARCHIVE_COUNT_BY_PURCHASE,
	ARCHIVE_AD_BULK_SETUP,
	ARCHIVE_AD_BULK_SETUP_START,
	ARCHIVE_AD_BULK_SETUP_DONE,
	ARCHIVE_AD_BULK_DELETE_SETUP,
	ARCHIVE_AD_BULK_SELECT,
	ARCHIVE_AD_BULK_UNLOAD,
	ARCHIVE_AD_BULK_TRY,
	ARCHIVE_AD_BULK_ARCHIVE_FAILED,
	ARCHIVE_AD_BULK_ARCHIVE_OK,
	ARCHIVE_AD_BULK_POSTPONED,
	ARCHIVE_AD_BULK_DONE,
	ARCHIVE_AD_BULK_ERROR,
} archive_state_t;


static const char*
archive_state_lut[] = {
	"ARCHIVE_AD_BULK_INIT",
	"ARCHIVE_BY_PURCHASE",
	"ARCHIVE_COUNT_BY_PURCHASE",
	"ARCHIVE_AD_BULK_SETUP",
	"ARCHIVE_AD_BULK_SETUP_START",
	"ARCHIVE_AD_BULK_SETUP_DONE",
	"ARCHIVE_AD_BULK_DELETE_SETUP",
	"ARCHIVE_AD_BULK_SELECT",
	"ARCHIVE_AD_BULK_UNLOAD",
	"ARCHIVE_AD_BULK_TRY",
	"ARCHIVE_AD_BULK_ARCHIVE_FAILED",
	"ARCHIVE_AD_BULK_ARCHIVE_OK",
	"ARCHIVE_AD_BULK_POSTPONED",
	"ARCHIVE_AD_BULK_DONE",
	"ARCHIVE_AD_BULK_ERROR",
};

struct archive_ad_bulk_state {
	archive_state_t state;
	struct cmd *cs;
	int *ad_id_list;
	int setup_ready;
	int num_ad_ids;
	int archive_offset;
	int archive_num;
	int bulk_delay;
	int bulk_limit;
	int total_limit;
	int total;
	const char *archive_type;

	char *errstr;
};

static void archive_ad_bulk_done(void *v) {
	struct archive_ad_bulk_state *state = v;
	struct cmd *cs = state->cs;

	free(state->ad_id_list);
	if (state->errstr)
		free(state->errstr);

	free(state);
	cmd_done(cs);
}

static void
archive_ad_bulk_mail_done_cb(struct mail_data *md, void *v, int status) {
	struct archive_ad_bulk_state *state = v;
	struct cmd *cs = state->cs;

	if (status != 0) {
		cs->status = "TRANS_MAIL_ERROR";
	}

	archive_ad_bulk_fsm(v, NULL, NULL);
}

static const char *
archive_ad_bulk_fsm(void *v, struct sql_worker *worker, const char *errstr) {
	struct archive_ad_bulk_state *state = v;
	struct cmd *cs = state->cs;
	struct bpapi ba;

	transaction_logprintf(cs->ts, D_INFO, "archive_ad_bulk_fsm state=%s",
				archive_state_lut[state->state]);

	if (errstr) {
		transaction_logprintf(cs->ts, D_INFO, "archive_ad_bulk error: %s",
					errstr);
		if (state->state == ARCHIVE_AD_BULK_ARCHIVE_OK) {
			/* Archiving error, go into error searching mode. */
			state->state = ARCHIVE_AD_BULK_ARCHIVE_FAILED;
		} else {
			cs->status = "TRANS_DATABASE_ERROR";
			cs->message = xstrdup(errstr);
			state->state = ARCHIVE_AD_BULK_ERROR;
		}
	}

	while (1) {
		switch ((archive_state_t)state->state) {
		case ARCHIVE_AD_BULK_INIT:
		{
			if (!cmd_haskey(cs, "single_run")) {
				time_t now = time(NULL);
				struct tm lt = {0};
				char hmbuf[10];
				int i;
				int num;
				struct bconf_node *allowed_root;

				strftime(hmbuf, sizeof(hmbuf), "%H:%M", localtime_r(&now, &lt));
				allowed_root = bconf_get(bconf_root, "*.common.archive_bulk.allowed");
				num = bconf_count(allowed_root);
				for (i = 0 ; i < num ; i++) {
					struct bconf_node *allowed = bconf_byindex(allowed_root, i);
					const char *from = bconf_get_string(allowed, "from");
					const char *to = bconf_get_string(allowed, "to");

					if (from && to && strcmp(from, hmbuf) <= 0 && strcmp(to, hmbuf) >= 0)
						break;
				}
				if (i == num) {
					transaction_logprintf(cs->ts, LOG_INFO, "archive_ad_bulk running outside allowed time.");
					state->state = ARCHIVE_AD_BULK_DONE;
					continue;
				}
			}
			if (strcmp(state->archive_type, "by_purchase") == 0) {
				state->state = ARCHIVE_BY_PURCHASE;
				continue;
			}

			state->state = ARCHIVE_AD_BULK_SETUP;
			continue;
		}

		case ARCHIVE_BY_PURCHASE:
			transaction_logprintf(cs->ts, D_DEBUG, "archive_ad_bulk trying with %d ads", state->total_limit);

			BPAPI_INIT_APP(&ba, host_bconf, "common", buf, pgsql);
			bpapi_set_int(&ba, "total_limit", state->total_limit);

			sql_worker_template_query(&config.pg_master, "sql/archive_purchase_process.sql",
				&ba, archive_ad_bulk_fsm, state, cs->ts->log_string);
			bpapi_output_free(&ba);
			bpapi_simplevars_free(&ba);
			bpapi_vtree_free(&ba);

			state->state = ARCHIVE_COUNT_BY_PURCHASE;
			return NULL;

		case ARCHIVE_COUNT_BY_PURCHASE:
			transaction_logprintf(cs->ts, D_DEBUG, "Finishing archive by purchase processs");
			while (worker->next_row(worker)) {
				state->total += atoi(worker->get_value(worker, 0));
			}
			state->state = ARCHIVE_AD_BULK_DONE;
			continue;

		case ARCHIVE_AD_BULK_SETUP:
			if (state->setup_ready) {
				state->state = ARCHIVE_AD_BULK_SELECT;
				continue;
			}

			transaction_logprintf(cs->ts, D_INFO, "archive_ad_bulk setting up");

			BPAPI_INIT_APP(&ba, host_bconf, "common", buf, pgsql);
			bpapi_set_int(&ba, "total_limit", state->total_limit);

			sql_worker_template_query(&config.pg_master, "sql/archive_ad_bulk_setup.sql",
						&ba, archive_ad_bulk_fsm, state, cs->ts->log_string);
			bpapi_output_free(&ba);
			bpapi_simplevars_free(&ba);
			bpapi_vtree_free(&ba);

			state->state = ARCHIVE_AD_BULK_SETUP_START;
			return NULL;

		case ARCHIVE_AD_BULK_SETUP_START:
			/* this (useless?) state is required because the template has 2 queries and trans does one query per one state */
			state->state = ARCHIVE_AD_BULK_DELETE_SETUP;
			return NULL;

		case ARCHIVE_AD_BULK_DELETE_SETUP:
			/* this (useless?) state is required because the template has 3 queries and trans does one query per one state */
			state->state = ARCHIVE_AD_BULK_SETUP_DONE;
			return NULL;

		case ARCHIVE_AD_BULK_SETUP_DONE:

			/* Test setup went ok */

			if (1 /* shit ok */) {
				transaction_logprintf(cs->ts, D_INFO, "archive_ad_bulk setup ready");
				state->setup_ready = 1;
				state->state = ARCHIVE_AD_BULK_SELECT;
			} else {
				transaction_logprintf(cs->ts, D_INFO, "archive_ad_bulk setup failed");
				state->state = ARCHIVE_AD_BULK_DONE;
			}
			continue;

		case ARCHIVE_AD_BULK_SELECT: {

			int pending = state->total_limit - state->total;
			int bulk_limit = pending < state->bulk_limit ? pending : state->bulk_limit;

			BPAPI_INIT_APP(&ba, host_bconf, "common", buf, pgsql);
			bpapi_set_int(&ba, "bulk_limit", bulk_limit);

			sql_worker_template_query(&config.pg_master, "sql/archive_ad_bulk_select.sql",
						&ba, archive_ad_bulk_fsm, state, cs->ts->log_string);
			bpapi_output_free(&ba);
			bpapi_simplevars_free(&ba);
			bpapi_vtree_free(&ba);

 			state->state = ARCHIVE_AD_BULK_UNLOAD;
			return NULL;
		}
		case ARCHIVE_AD_BULK_UNLOAD:
			transaction_logprintf(cs->ts, D_DEBUG, "archive_ad_bulk archiving %d rows", 
						worker->rows(worker));
			if (worker->rows(worker) == 0) {
				state->state = ARCHIVE_AD_BULK_DONE;
				continue;
			}

			state->num_ad_ids = 0;
			while (worker->next_row(worker)) {
				state->ad_id_list[state->num_ad_ids++] = atoi(worker->get_value(worker, 0));
			}
			state->archive_offset = 0;
			state->archive_num = state->num_ad_ids;

			state->state = ARCHIVE_AD_BULK_TRY;
			continue;

		case ARCHIVE_AD_BULK_TRY:
		{
			int i;
			transaction_logprintf(cs->ts, D_DEBUG, "archive_ad_bulk trying with %d ads", 
						state->archive_num);

			BPAPI_INIT_APP(&ba, host_bconf, "common", buf, pgsql);
			for (i = 0 ; i < state->archive_num ; i++)
				bpapi_set_int(&ba, "ad_id", state->ad_id_list[state->archive_offset + i]);
			sql_worker_template_query(&config.pg_master, "sql/call_archive_ad.sql",
						&ba, archive_ad_bulk_fsm, state, cs->ts->log_string);
			bpapi_output_free(&ba);
			bpapi_simplevars_free(&ba);
			bpapi_vtree_free(&ba);

			state->state = ARCHIVE_AD_BULK_ARCHIVE_OK;
			return NULL;
		}

		case ARCHIVE_AD_BULK_ARCHIVE_FAILED:
			if (state->archive_num == 1) {
				/* Problematic ad found. */
				transaction_logprintf (cs->ts, D_ERROR, "archive_ad_bulk failed to archive ad %d: %s", state->ad_id_list[state->archive_offset],
						errstr);
				state->state = ARCHIVE_AD_BULK_POSTPONED;
				state->errstr = xstrdup(errstr);

				BPAPI_INIT_APP(&ba, host_bconf, "common", buf, pgsql);
				bpapi_set_int(&ba, "ad_id", state->ad_id_list[state->archive_offset]);
				sql_worker_template_query(&config.pg_master, "sql/call_postpone_archive_ad.sql",
							  &ba, archive_ad_bulk_fsm, state, cs->ts->log_string);
				bpapi_output_free(&ba);
				bpapi_simplevars_free(&ba);
				bpapi_vtree_free(&ba);
				return NULL;
			}

			state->archive_num = (state->archive_num + 1) / 2;
			state->state = ARCHIVE_AD_BULK_TRY;
			continue;

		case ARCHIVE_AD_BULK_ARCHIVE_OK:
			state->archive_offset += state->archive_num;

			if (state->archive_offset >= state->num_ad_ids) {
				int lim = state->total_limit;
				state->total += state->num_ad_ids;

				transaction_logprintf (cs->ts, D_DEBUG, "archive_ad_bulk batch success (%s)",
						cmd_haskey(cs, "single_run") || (lim && state->total >= lim) ? "done" : "init");

				/* This batch is done. */
				if (cmd_haskey(cs, "single_run") || (lim && state->total >= lim)) {
					state->state = ARCHIVE_AD_BULK_DONE;
				} else {
					state->state = ARCHIVE_AD_BULK_INIT;
					/* Maybe we want a little rest before proceeding with the next one */
					if (state->bulk_delay)
						sleep(state->bulk_delay);
				}
				continue;
			}

			transaction_logprintf (cs->ts, D_INFO, "archive_ad_bulk batch success (try %d)",
					(state->archive_num + 1) / 2);

			/* We still half the number of ads to quicker find the problematic one. */
			state->archive_num = (state->archive_num + 1) / 2;
			state->state = ARCHIVE_AD_BULK_TRY;
			continue;

		case ARCHIVE_AD_BULK_POSTPONED:
		{
			struct mail_data *md;

			transaction_logprintf (cs->ts, D_INFO, "archive_ad_bulk postponing %d (%s)",
					state->ad_id_list[state->archive_offset],
					(state->archive_num == 0) ?  "ok" : "try");

			md = zmalloc(sizeof(*md));
			md->command = state;
			md->callback = archive_ad_bulk_mail_done_cb;
			if (cs->ts)
				md->log_string = cs->ts->log_string;

			mail_insert_param(md, "to", bconf_get_string(bconf_root, "*.common.archive_bulk.mail.to"));
			mail_insert_int(md, "ad_id", state->ad_id_list[state->archive_offset]);
			mail_insert_param(md, "errstr", state->errstr);
			mail_send(md, "mail/archive_ad_failed.txt",NULL); /*XXX language */

			state->errstr = NULL;
			free(state->errstr);

			/* Reset to try all the remaining ads. */
			state->archive_offset++;
			state->archive_num = state->num_ad_ids - state->archive_offset;
			if (state->archive_num == 0)
				state->state = ARCHIVE_AD_BULK_ARCHIVE_OK;
			else
				state->state = ARCHIVE_AD_BULK_TRY;
			return NULL;
		}

		case ARCHIVE_AD_BULK_DONE:
			transaction_printf(cs->ts, "total:%d\n", state->total);
			archive_ad_bulk_done(state);
			return NULL;
		case ARCHIVE_AD_BULK_ERROR:
			archive_ad_bulk_done(state);
			return NULL;

		}
	}
}

static void
archive_ad_bulk(struct cmd *cs) {

	struct archive_ad_bulk_state *state;
	state = zmalloc(sizeof (*state));

	state->cs = cs;
	state->state = ARCHIVE_AD_BULK_INIT;
	state->bulk_limit = cmd_haskey(cs, "bulk_limit") ? atoi(cmd_getval(cs, "bulk_limit"))
					  :  bconf_get_int(bconf_root, "*.common.archive_bulk.limit");
	state->total_limit = cmd_haskey(cs, "total_limit") ? atoi(cmd_getval(cs, "total_limit"))
					  :  bconf_get_int(bconf_root, "*.common.archive_bulk.total_limit");
	state->ad_id_list = xmalloc(state->bulk_limit * sizeof (*state->ad_id_list));

	if (cmd_haskey(cs, "reuse_cache"))
		state->setup_ready = atoi(cmd_getval(cs, "reuse_cache"));

	if (cmd_haskey(cs, "bulk_delay"))
		state->bulk_delay = atoi(cmd_getval(cs, "bulk_delay"));

	state->archive_type = cmd_haskey(cs, "archive_type")
			? cmd_getval(cs, "archive_type")
			: "by_ad";

	transaction_logprintf(cs->ts, D_INFO,
		"Archived by: %s",
		state->archive_type
	);

	transaction_logprintf(cs->ts, D_INFO,
		"archive_ad_bulk command started (bulk_limit: %d, total_limit: %d)",
		state->bulk_limit, state->total_limit
	);

	archive_ad_bulk_fsm(state, NULL, NULL);
}

static struct c_param parameters[] = {
	{ "single_run", 0, "v_bool" },
	{ "bulk_delay", 0, "v_integer" },
	{ "bulk_limit", 0, "v_integer" },
	{ "total_limit", 0, "v_integer" },
	{ "reuse_cache", 0, "v_bool" },
	{ "archive_type", 0, "v_archive_type" },
	{ NULL, 0}
};

ADD_COMMAND(archive_ad_bulk, NULL, archive_ad_bulk, parameters, NULL);
