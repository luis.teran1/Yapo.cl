#include "factory.h"

static struct c_param parameters[] = {
	{"list_id;ad_id",    P_XOR,  "v_id;v_id"},
	{NULL, 0}
};

FACTORY_TRANSACTION(ad_status, &config.pg_slave, "sql/ad_status.sql", NULL, parameters, FACTORY_NO_RES_CNTR);

