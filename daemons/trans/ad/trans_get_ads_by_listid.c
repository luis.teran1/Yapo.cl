#include "factory.h"

static struct c_param parameters[] = {
	{"list_id",    P_REQUIRED,  "v_multiple_list_ids"},
	{"ad_status",  P_OPTIONAL,  "v_multiple_product_params"},
	{"with_user",  P_OPTIONAL,  "v_accounts_accept_conditions"},
	{NULL, 0}
};

FACTORY_TRANSACTION(get_ads_by_listid, &config.pg_slave, "sql/get_ads_by_listid.sql", NULL, parameters, FACTORY_RES_SHORT_FORM);
