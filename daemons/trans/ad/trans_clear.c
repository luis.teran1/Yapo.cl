#include <errno.h>
#include <rand.h>

#include "log_event.h"
#include "bconfd_client.h"
#include "factory.h"
#include "trans_at.h"
#include "pgsql_api.h"
#include "settings.h"
#include "utils/filterd.h"
#include "utils/refresh_session.h"
#include "utils/preprocessing_queue.h"

struct validator v_ad_id[] = {
	VALIDATOR_INT(1, -1, "ERROR_AD_ID_INVALID"),
	{0}
};

ADD_VALIDATOR(v_ad_id);

struct validator v_ad_id_clear[] = {
	VALIDATOR_SUB("v_ad_id"),
	VALIDATOR_OTHER("action_id", P_REQUIRED, "v_action_id"),
	{0}
};

ADD_VALIDATOR(v_ad_id_clear);

struct validator v_clear_status[] = {
	VALIDATOR_REGEX("^(OK|ERR_[A-Z_]+)$", REG_EXTENDED, "ERROR_CLEAR_STATUS_INVALID"),
	{0}
};
ADD_VALIDATOR(v_clear_status);

struct validator v_ref_type[] = {
	VALIDATOR_REGEX("^(payex|webpay)$", REG_EXTENDED, "ERROR_REF_TYPE_INVALID"),
	{0}
};
ADD_VALIDATOR(v_ref_type);

static int
vf_admin_clear(struct cmd_param *param, const char *arg) {
	if (cmd_haskey(param->cs, "verify_code")) {
		param->error = "ERROR_VERIFY_CODE_PRESENT";
		param->cs->error = "TRANS_ERROR";
		return 1;
	}
	return 0;
}

struct validator v_admin_clear[] = {
	VALIDATOR_INT(0, 1, "ERROR_ADMIN_CLEAR_INVALID"),
	VALIDATOR_OTHER("token", P_REQUIRED, "v_token:adminad.clear_ad"),
	VALIDATOR_FUNC(vf_admin_clear),
	{0}
};

ADD_VALIDATOR(v_admin_clear);

static struct c_param parameters[] = {
	{"ad_id;pay_code;verify_code;order_id",	P_XOR,		"v_ad_id_clear;v_clear_pay_code;v_verify_code;v_order_id"},
	{"admin_clear",		P_OPTIONAL,	"v_admin_clear"},
	{"remote_addr",		P_OPTIONAL, "v_ip"},
	{"remote_browser",	P_OPTIONAL, "v_remote_browser"},
	{"token",			P_OPTIONAL,	"v_token:adminad.clear_ad"},
	{NULL}
};

extern struct bconf_node *host_bconf;

static int
bind_clear(struct cmd *cs, struct bpapi *ba) {
	
	if (debug_level == D_DEBUG)
		bpapi_insert(ba, "debug", "1");

	if (cmd_haskey(cs, "reference") && cmd_haskey(cs, "ref_type")) {
		/* XXX P_MULTI and loop. */
		bpapi_insert(ba, "ref_type", cmd_getval(cs, "ref_type"));
		bpapi_insert(ba, "reference", cmd_getval(cs, "reference"));
	}

	if (cmd_haskey(cs, "status")) {
		bpapi_insert(ba, "tr_status", "paid");
		bpapi_insert(ba, "transition", "pay");
	} else if (cmd_getval(cs, "admin_clear") && strcmp(cmd_getval(cs, "admin_clear"), "1") == 0) {
		bpapi_insert(ba, "transition", "adminclear");
		bpapi_insert(ba, "tr_status", "cleared");
		bpapi_insert(ba, "admin_clear", "1");
		if (cmd_getval(cs, "token"))	/* XXX workaround to not use bpapi_remove */
			bpapi_insert(ba, "admin_token", cmd_getval(cs, "token"));
	} else {
		if (cmd_getval(cs, "pay_code")) {
			bpapi_insert(ba, "tr_status", "paid");
			bpapi_insert(ba, "transition", "pay");
			if (strcmp(cmd_getval(cs, "phone"), "00") != 0) {
				bpapi_insert(ba, "ref_type", "payphone");
				bpapi_insert(ba, "reference", cmd_getval(cs, "phone"));
			}
			bpapi_insert(ba, "pay_time", cmd_getval(cs, "pay_time"));
		} else if (cmd_getval(cs, "verify_code")) {
			bpapi_insert(ba, "tr_status", "verified");
			bpapi_insert(ba, "transition", "verify");
		} else {
			bpapi_insert(ba, "transition", "cleared_by");
			bpapi_insert(ba, "tr_status", "cleared");
			bpapi_insert(ba, "amount", "0");
			bpapi_insert(ba, "status", "OK");
		}
	}

	return 0;
}

static int
at_clear(struct cmd *cs, struct bpapi *ba) {
	char *info = NULL;
	xasprintf(&info, "%s-%s", 
		bpapi_get_element(ba, "ac_ad_id", 0),
		bpapi_get_element(ba, "action_id", 0) ?: bpapi_get_element(ba, "ac_action_id", 0));

	at_clear_commands("payinfo", info, cs->ts->log_string);
	free(info);
	return 0;
}

static const struct factory_action refresh_session = FA_FUNC(refresh_account);

static const struct factory_action *
refresh_account_if_needed(struct cmd *cs, struct bpapi *ba) {

	int updated = !strcmp(bpapi_get_element(ba, "ac_pack_result", -1), "0");
	if (updated) {
		return &refresh_session;
	}
	return NULL;
}

static const struct factory_action *
delayed_move_to_queue(struct cmd *cs, struct bpapi *ba) {
	const char *action_type = bpapi_get_element(ba, "ac_action_type", 0);
	if (action_type
			&& (strcmp(action_type, "new") == 0
				|| strcmp(action_type, "renew") == 0
				|| strcmp(action_type, "edit") == 0
				|| strcmp(action_type, "editrefused") == 0)) {
		move_preprocessing_queue(cs, ba);
	}
	return NULL;
}

static const char *error_map[] = {
	"NO_ERROR",
	"ERROR_PAY_CODE_INVALID",
	"ERROR_ORDER_ID_INVALID",
	"ERROR_VERIFY_CODE_INVALID",
	"ERROR_NOT_ENOUGH",
	"ERROR_BAD_PAY_STATUS",
	"ERROR_UNKNOWN_TARGET"
};

static int
error_func(struct cmd *cs, struct bpapi *ba) {
	const char *error = bpapi_get_element(ba, "error", 0);
	int err = atoi(error);

	transaction_logprintf(cs->ts, D_INFO, "clear_error_func: error [%s] [%d] [%s]", error, err, error_map[err]);

	cs->status = "TRANS_ERROR";
	cs->error = error_map[err];
	return 1;
}

static const struct factory_action error_out = FA_FUNC(error_func);
static const struct factory_action clear_done = FA_DONE();

static const struct factory_action *
do_action_result(struct cmd *cs, struct bpapi *ba) {
	const char *next_target = bpapi_get_element(ba, "next_target", 0);

	if (next_target) {
		transaction_logprintf(cs->ts, D_INFO, "do_action_result: next_target [%s]", next_target);
		if (!strcmp(next_target, "error"))
			return &error_out;

		if (!strcmp(next_target, "done"))
			return &clear_done;
	}
	
	return NULL;
}

static int
slave_has_data(struct cmd *cs, struct bpapi *ba) {
	struct sql_worker *worker = NULL;
	const char *errstr = NULL;
	int has = 0;

	worker = sql_blocked_template_query("pgsql.slave", 0, "sql/clear_slave_has_data.sql", ba, cs->ts->log_string, &errstr);
	if (worker && sql_blocked_next_result(worker, &errstr) == 1) {
		has = worker->rows(worker) > 0 && worker->next_row(worker);
	}

	if (errstr) {
		cs->message = xstrdup(errstr);
		cs->status = "TRANS_DATABASE_ERROR";
	}

	if (worker)
		sql_worker_put(worker);

	return has;
}

static void
sleep_beautifully(int seconds) {
	struct timespec rem = {0}, req = { seconds, 0 };
	int r = nanosleep(&req, &rem);
	while (r == -1 && errno == EINTR) {
		req = rem;
		r = nanosleep(&req, &rem);
	}
}

static const struct factory_action get_queue_from_master = FA_SQL("ac_", "pgsql.master", "sql/call_get_queue_for_action.sql", 0);
static const struct factory_action get_queue_from_slave  = FA_SQL("ac_", "pgsql.slave", "sql/call_get_queue_for_action.sql", 0);

static const struct factory_action *
get_queue_for_action(struct cmd *cs, struct bpapi *ba) {
	const char *need_filter_data = bpapi_get_element(ba, "ac_need_filter_data", 0);
	if (need_filter_data && !strcasecmp(need_filter_data, "t")) {

		/* Random prob of not using the json_result */
		int threshold = bconf_get_int(host_bconf, "controlpanel.clear.filterd.percentage");
		int rn = random() % 100;
		if (rn < (100-threshold) && bpapi_has_key(ba, "json_result")) {
			transaction_logprintf(cs->ts, D_INFO, "clear: dropping filterd result");
			bpapi_remove(ba, "json_result");
		}

		/* If we have the answer from filterd, avoid the waiting at all */
		const char *json_result = bpapi_get_element(ba, "json_result", 0);
		if (json_result && *json_result) {
			transaction_logprintf(cs->ts, D_INFO, "clear: using filterd result on master");
			return &get_queue_from_master;
		}

		const char *ad_id = bpapi_get_element(ba, "ac_ad_id", 0);
		const char *action_id = bpapi_get_element(ba, "ac_action_id", 0);
		if (ad_id && *ad_id && action_id && *action_id) {
			/* Wait for the slave to be ready to handle the query */
			int sleep_time = bconf_get_int(host_bconf, "controlpanel.clear.sleep_time");
			int retries = bconf_get_int(host_bconf, "controlpanel.clear.retries");
			int has = slave_has_data(cs, ba);
			while (!has && retries > 0) {
				sleep_beautifully(sleep_time);
				has = slave_has_data(cs, ba);
				retries--;
			}
			/* We can't wait anymore, go for master if the slave was not ready */
			transaction_logprintf(cs->ts, D_INFO, "clear: running filter_data on %s (retries left %d)", has ? "slave" : "master", retries);
			return has ? &get_queue_from_slave : &get_queue_from_master;
		}
	}

	return NULL;
}

static int
call_filterd(struct cmd *cs, struct bpapi *ba) {
	filterd_filter(cs, ba, "ac_json", "json_result");
	return 0;
}

static const struct factory_action
call_get_filter_data_json = FA_SQL("ac_", "pgsql.master", "sql/call_get_filter_data_json.sql", 0);

static const struct factory_action *
get_filter_data_json(struct cmd *cs, struct bpapi *ba) {
	const char *ad_id = bpapi_get_element(ba, "ac_ad_id", 0);
	const char *action_id = bpapi_get_element(ba, "ac_action_id", 0);
	const char *user_id = bpapi_get_element(ba, "ac_user_id", 0);
	if (!ad_id || !action_id || !user_id)
		return NULL;
	return &call_get_filter_data_json;
}

static int
pgsql_array_to_bpapi(struct cmd *cs, struct bpapi *ba, const char *input_var, const char *output_var) {

	int count = 0;
	const char *invar = bpapi_get_element(ba, input_var, 0);

	if (invar && *invar) {

		char **elem;
		char *arr = xstrdup(invar);
		count = pgsql_split_array(arr, strlen(arr), &elem);

		if (count < 0) {
			transaction_logprintf(cs->ts, D_ERROR, "clear failed parsing array: %s", invar);
		} else {
			for (int i = 0; i < count; ++i)
				bpapi_insert(ba, output_var, elem[i]);
			free(elem);
		}

		free(arr);
	}

	return count;
}

static int
format_references(struct cmd *cs, struct bpapi *ba) {

	int ref_type = pgsql_array_to_bpapi(cs, ba, "ac_ref_type", "arr_ref_type");
	int reference = pgsql_array_to_bpapi(cs, ba, "ac_reference", "arr_reference");

	if (ref_type < 0 || reference < 0) {
		cs->status = "TRANS_ERROR";
		cs->error = "ERROR_REFERENCE_PARSING";
		return 1;
	}

	if (ref_type != reference) {
		cs->status = "TRANS_ERROR";
		cs->error = "ERROR_REFERENCE_PARSING";
		xasprintf(&cs->message, "ref_type != reference count (%d != %d)", ref_type, reference);
		return 1;
	}

	return 0;
}

static const struct factory_action
call_clear_store_action = FA_SQL("ac_", "pgsql.master", "sql/call_clear_store_action.sql", 0);

static const struct factory_action *
clear_store_action(struct cmd *cs, struct bpapi *ba) {
	const char *target = bpapi_get_element(ba, "clear_get_target", 0);
	if (target && !strcmp(target, "store_action"))
		return &call_clear_store_action;
	return NULL;
}

static const struct factory_action
do_clear_store_output = FA_OUT("trans/clear_store_output.txt");

static const struct factory_action *
clear_store_output(struct cmd *cs, struct bpapi *ba) {
	const char *target = bpapi_get_element(ba, "clear_get_target", 0);
	if (target && !strcmp(target, "store_action"))
		return &do_clear_store_output;
	return NULL;
}

static struct factory_data data = {
	"controlpanel",
	FACTORY_NO_RES_CNTR
};

static const struct factory_action actions[] = {
	FA_FUNC(bind_clear),
	FA_SQL(NULL, "pgsql.master", "sql/clear_get_target.sql", 0),
	// Store action
	FA_COND(clear_store_action),
	FA_COND(clear_store_output),
	FA_COND(do_action_result),
	// Ad action
	FA_SQL("ac_", "pgsql.master", "sql/call_clear_minimum_info.sql", 0),
	FA_COND(refresh_account_if_needed),
	FA_COND(get_filter_data_json),
	FA_FUNC(call_filterd),
	FA_COND(get_queue_for_action),
	FA_SQL("ac_", "pgsql.master", "sql/call_clear_ad_action.sql", 0),
	FA_FUNC(format_references),
	FA_OUT("trans/clear_output.txt"),
	FA_COND(do_action_result),
	FA_FUNC(at_clear),
	FA_COND(delayed_move_to_queue),
	FA_DONE()
};

FACTORY_TRANS(clear, parameters, data, actions);
