#include "factory.h"
#include "bpapi.h"
#include <bconf.h>

extern struct bconf_node *bconf_root;

static int
vf_queue_name(struct cmd_param *param, const char *arg) {
	struct bconf_node *queues = bconf_get(bconf_root, "*.controlpanel.modules.adqueue.filter");
	int n = bconf_count(queues);
	for (int i = 0 ; i < n ; ++i) {
		struct bconf_node *queue = bconf_byindex(queues, i);
		const char *name = bconf_get_string(queue, "name");
		if (!strcmp(name, param->value))
			return 0;
	}
	param->error = "ERROR_QUEUE_NAME_INVALID";
	param->cs->status = "TRANS_ERROR";
	return 0;
}

struct validator v_queue_name[] = {
	VALIDATOR_FUNC(vf_queue_name),
	{0}
};
ADD_VALIDATOR(v_queue_name);

static struct c_param parameters[] = {
	{"ad_id",		P_REQUIRED, "v_id"},
	{"action_id",	P_REQUIRED, "v_id"},
	{"queue",		P_REQUIRED, "v_queue_name"},
	{NULL, 0}
};

static struct factory_data data = {
	"controlpanel",
	FACTORY_EMPTY_RESULT_OK
};

static const struct factory_action actions[]= {
	FA_SQL("", "pgsql.master", "sql/call_set_force_queue.sql", 0),
	{0}
};

FACTORY_TRANS(set_force_queue, parameters, data, actions);
