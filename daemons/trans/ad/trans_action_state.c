#include "factory.h" 

static struct c_param parameters[] = {
	{"ad_id", P_REQUIRED, "v_id"},
	{"action_id", P_REQUIRED, "v_id"},
	{NULL, 0}
};

FACTORY_TRANSACTION(action_state, &config.pg_master, "sql/action_state.sql", NULL, parameters, FACTORY_NO_RES_CNTR);
