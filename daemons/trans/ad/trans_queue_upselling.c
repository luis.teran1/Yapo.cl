#include "factory.h"
#include "bpapi.h"
#include <bconf.h>

extern struct bconf_node *bconf_root;

static struct c_param parameters[] = {
	{"ad_id",  		P_REQUIRED,	"v_weekly_ad_id"},
	{"counter",    	P_REQUIRED,	"v_integer"},
	{"action_id", 	P_OPTIONAL, "v_id"},
	{"daily_bump", 	P_OPTIONAL, "v_bool"},
	{"is_from_nga",	P_OPTIONAL, "v_bool"},
	{"with_delay",	P_OPTIONAL, "v_bool"},
	{NULL, 0}
};

static struct factory_data data = {
	"accounts"
};

static int upselling_queue_insert(struct cmd *cs, struct bpapi *ba) {
	char *weeklycmd;
	int counter = atoi(cmd_getval(cs, "counter"));

	// If upselling_bump is requested we must queue fist_experience_bump transaction
	if (cmd_haskey(cs, "daily_bump") && strcmp(cmd_getval(cs, "daily_bump"),"0") == 0){
		xasprintf(&weeklycmd,
			"cmd:first_experience_bump\n"
			"ad_id:%s\n"
			"action_id:%s\n"
			"mail_type:bump_ok\n"
			"commit:1\n",
			cmd_getval(cs, "ad_id"),
			bpapi_get_element(ba, "action_id", 0)
		);
	} else {
		if (cmd_haskey(cs, "action_id")){
			xasprintf(&weeklycmd,
				"cmd:weekly_bump_ad\n"
				"ad_id:%s\n"
				"action_id:%s\n"
				"counter:%d\n"
				"%s"
				"%s"
				"batch:1\n"
				"commit:1\n",
				cmd_getval(cs, "ad_id"),
				cmd_getval(cs, "action_id"),
				counter,
				(cmd_haskey(cs, "daily_bump"))?"daily_bump:1\n":"",
				"is_upselling:1\n"
			);
		} else {
			xasprintf(&weeklycmd,
				"cmd:weekly_bump_ad\n"
				"ad_id:%s\n"
				"counter:%d\n"
				"%s"
				"%s"
				"batch:1\n"
				"commit:1\n",
				cmd_getval(cs, "ad_id"),
				counter,
				(cmd_haskey(cs, "daily_bump"))?"daily_bump:1\n":"",
				"is_upselling:1\n"
			);
		}
	}

	char *bconf_key;

	if (cmd_haskey(cs, "daily_bump") && strcmp(cmd_getval(cs, "daily_bump"),"0") == 0) {
		xasprintf(&bconf_key, "*.*.payment.product_specific.upselling_bump.first_execution_delay");
	} else {
		xasprintf(&bconf_key,"*.*.payment.product_specific.%s.first_execution_delay",cmd_haskey(cs, "daily_bump") ? "upselling_daily_bump" : "upselling_weekly_bump");
	}
	int first_execution = 0;
	first_execution = bconf_get_int(bconf_root, bconf_key);

	if (cmd_haskey(cs, "with_delay") && strcmp(cmd_getval(cs, "with_delay"),"0") == 0) {
		first_execution = 1;
	}

	transaction_internal_printf(NULL, NULL,
		"cmd:queue\n"
		"commit:1\n"
		"queue:%s\n"
		"timeout:%d\n"
		"blob:%zu:command\n"
		"%s\n",
		"upselling",
		first_execution,
		strlen(weeklycmd),
		weeklycmd);
	return 0;
}

static const struct factory_action actions[] = {
	FA_SQL("", "pgsql.master", "sql/call_insert_upselling_param.sql", FASQL_OUT),
	FA_FUNC(upselling_queue_insert),
	{0}
};

FACTORY_TRANS(queue_upselling, parameters, data, actions);
