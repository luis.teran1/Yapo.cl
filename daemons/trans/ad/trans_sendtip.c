#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdarg.h>
#include <unistd.h>
#include <sys/socket.h>
#include <ctype.h>
#include <time.h>
#include <syslog.h>
#include <locale.h>

#include "logging.h"
#include "log_event.h"

#include "command.h"
#include "trans.h"
#include "util.h"
#include "bconf.h"
#include "trans_mail.h"
#include "validators.h"
#include "mail_log.h"
#include "filter.h"

#define SENDTIP_DONE		0
#define SENDTIP_ERROR		1
#define SENDTIP_INIT		2
#define SENDTIP_MAILLOG		3
#define SENDTIP_FILTER		4
#define SENDTIP_AD_SUBJECT	5

static const char *
sendtip_fsm(void *v, struct sql_worker *worker, const char *errstr);

extern struct bconf_node *bconf_root;

struct validator v_msg[] = {
	VALIDATOR_LEN(0, 300, "ERROR_MSG_TOO_LONG"),
	{0}
};
ADD_VALIDATOR(v_msg);

static struct c_param parameters[] = {
	{"list_id;bid_ad_id",	P_XOR,		"v_id;v_id"},
	{"email",		0,	"v_emailsendtip"},
	{"msg",			0,		"v_msg"},
	{"name",		P_REQUIRED,	"v_name"},
	{"remote_addr",		P_REQUIRED,	"v_ip"},
	{"remote_browser",	0, 	"v_remote_browser"},
	{"lang",		0,	"v_lang"},
	{NULL, 0}
};

struct sendtip_state {
	int state;
	struct cmd *cs;
};

static void
sendtip_done(struct sendtip_state *state) {
	struct cmd *cs = state->cs;
	free(state);
	cmd_done(cs);
}

static void
sendtip_done_cb(struct mail_data *md, void *v, int status) {
	struct sendtip_state *state = v;

        if (status != 0) {
		sendtip_fsm(state, NULL, "TRANS_MAIL_ERROR");
                return;
        }

        transaction_logprintf(state->cs->ts, D_INFO, "SENDTIP-SENDMAIL: 200 Mail sent OK");
	sendtip_fsm(state, NULL, NULL);
}

static void
bind_insert(struct bpapi *ba, struct mail_data *md, struct sendtip_state *state) {
	struct cmd *cs = state->cs;
	struct cmd_param *cp;

	TAILQ_FOREACH(cp, &cs->params, tq) {
		if (ba)
			bpapi_insert(ba, cp->key, cp->value);
		if (md)
			mail_insert_param(md, cp->key, cp->value);
	}
}


static void
sendtip_filter_cb(struct filter_data *data, 
		  int status, 
		  int rule_id, 
		  const char *rule_name, 
		  const char *action, 
		  const char *target, 
		  const char *matched_column, 
		  const char *matched_value, 
		  int combi_match, 
		  const char *errstr) {

	struct sendtip_state *state = data->data;

	if (status == FILTER_ERROR) {
		free(data);
		sendtip_fsm(state, NULL, errstr);
	} else if (status == FILTER_DONE) {
		free(data);
		sendtip_fsm(state, NULL, NULL);
	} else if (status == FILTER_MATCH) {
		transaction_logprintf(state->cs->ts, D_INFO, 
				      "sendtip: matched filter %s (id %d), '%s', action: %s", 
				      rule_name, rule_id, matched_value, action);

		state->cs->error = "ERROR_TIP_BLOCKED";
		state->cs->status = "TRANS_ERROR";
		state->state = SENDTIP_ERROR;
	}
}

static void
sendtip_log_cb(struct mail_log *log, int id, const char *errstr) {
	struct sendtip_state *state = log->data;
	free(log);
	sendtip_fsm(state, NULL, errstr);
}

static const char *
sendtip_fsm(void *v, struct sql_worker *worker, const char *errstr) {
	struct sendtip_state *state = v;
	struct cmd *cs = state->cs;

	/*
	 * General error handling for db errors.
	 */
	if (errstr) {
                transaction_logprintf(cs->ts, D_ERROR, "sendtip_fsm statement failed (%d, %s)",
                                      state->state,
                                     errstr);

		if (!strcmp(errstr, "ERROR_SPAM")) {
			cs->status = "TRANS_ERROR";
			cs->error = "ERROR_SPAM";
		} else if (!strcmp(errstr, "TRANS_MAIL_ERROR")) {
			cs->status = errstr; 
		} else {
			cs->status = "TRANS_DATABASE_ERROR";
			if (cs->message)
				free(cs->message);
			cs->message = xstrdup(errstr);
		}

		state->state = SENDTIP_ERROR;
	}

	while (1) {
		transaction_logprintf(cs->ts, D_DEBUG, "sendtip_fsm(%p, %p, %p), state=%d",
				      v, worker, errstr, state->state);

		switch (state->state) {
		case SENDTIP_INIT:
		{
			/* next state */
			state->state = SENDTIP_MAILLOG;
			struct mail_log *log = zmalloc(sizeof(*log));

			log->type = "tip";
			log->remote_addr = cmd_getval(cs, "remote_addr");
			log->email = cmd_getval(cs, "email");
			log->list_id = atoi(cmd_getval(cs,"list_id"));
			log->spam_minutes = bconf_get_int(bconf_root, "*.sendtip.spam.minutes");
			log->spam_counts[MAIL_SPAM_SEND] = bconf_get_int(bconf_root, "*.sendtip.spam.send_count");
			log->spam_counts[MAIL_SPAM_RECV] = bconf_get_int(bconf_root, "*.sendtip.spam.recv_count");
			log->spam_counts[MAIL_SPAM_AD] = bconf_get_int(bconf_root, "*.sendtip.spam.ad_count");
			log->cb = sendtip_log_cb;
			log->data = state;

			log_mail(log);

			return NULL;
		}

		case SENDTIP_MAILLOG:
		{
			struct filter_data *fdata = filter_init(NULL);
			const char* msg = cmd_getval(cs, "msg");

			state->state = SENDTIP_FILTER;
			filter_add_element(fdata, "name", cmd_getval(cs, "name"));
			filter_add_element(fdata, "email", cmd_getval(cs, "email"));
			if (msg != NULL)
				filter_add_element(fdata, "body", msg);
			filter_add_element(fdata, "remote_addr", cmd_getval(cs, "remote_addr"));
			fdata->cb = sendtip_filter_cb;
			fdata->data = state;

			filter_data(fdata, "sendtip");
			return NULL;
		}

		case SENDTIP_FILTER:
		{
			struct bpapi ba;
			BPAPI_INIT(&ba, NULL, pgsql);
			bind_insert(&ba, NULL, state);
			state->state = SENDTIP_AD_SUBJECT;
			if (cmd_haskey(state->cs, "bid_ad_id")) {
				sql_worker_template_query(&config.pg_slave, "sql/get_bid_ad_subject.sql", &ba,
						  sendtip_fsm, state, cs->ts->log_string);
			} else {
				sql_worker_template_query(&config.pg_slave, "sql/get_sendtip_data.sql", &ba,
						  sendtip_fsm, state, cs->ts->log_string);
			}

			bpapi_output_free(&ba);
			bpapi_simplevars_free(&ba);
			bpapi_vtree_free(&ba);
			return NULL;
		}

		case SENDTIP_AD_SUBJECT:
		{
			/* Mail structure */
			struct mail_data *md;
			/* Date time handling */
			struct tm tm;
			time_t t;
			/* Locale setting */
			char *datestr;

			/* Check if ad exists */
			if (!worker->next_row(worker)) {
				transaction_printf(cs->ts, "%s:%s\n", cmd_haskey(state->cs, "bid_ad_id") ? "bid_ad_id" : "list_id", "ERROR_AD_NOT_FOUND");
				state->state = SENDTIP_ERROR;
				continue;
			}

			state->state = SENDTIP_DONE;

			/* SEND tip mail */
			md = zmalloc(sizeof (*md));
			md->command = state;
			md->callback = sendtip_done_cb;
			md->log_string = cs->ts->log_string;

			/* Add email paramaters */
			bind_insert(NULL, md, state);
			mail_insert_param(md, "subject", worker->get_value_byname(worker, "subject"));
			mail_insert_param(md, "type", worker->get_value_byname(worker, "type"));
			mail_insert_param(md, "category", worker->get_value_byname(worker, "category"));
			mail_insert_param(md, "region", worker->get_value_byname(worker, "region"));
			if(strcmp(worker->get_value_byname(worker, "ad_media_id"), "0")) {
				mail_insert_param(md, "ad_media_id", worker->get_value_byname(worker, "ad_media_id"));
				mail_insert_param(md, "seq_no", worker->get_value_byname(worker, "seq_no"));
			}
			
			if (worker->get_value(worker, 1))
				mail_insert_param(md, "url_path", worker->get_value(worker, 1));

			time(&t);
			localtime_r(&t, &tm);
			xasprintf(&datestr, "%04d-%02d-%02d %02d:%02d:%02d", tm.tm_year + 1900, tm.tm_mon + 1,
				  tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec);
			mail_insert_param(md, "sent_at", datestr);
			free(datestr);

			/* Send mail */
			transaction_logprintf(cs->ts, D_DEBUG, "Sending email included with template name");
			mail_send(md, "mail/sendtip.mime", cmd_getval(cs, "lang"));

			return NULL;

		}

		case SENDTIP_DONE:
			sendtip_done(state);
			return NULL;

		case SENDTIP_ERROR:
			transaction_logprintf(cs->ts, D_ERROR, "Error state");
			sendtip_done(state);
			return NULL;

		default:
			/* MAJOR STATE ERROR */
			cs->status = "TRANS_ERROR";
			transaction_logprintf(cs->ts, D_ERROR, "Unknown state (%d)",
					      state->state);
			return NULL;

		}
	}
}

/*****************************************************************************
 * pgexample
 * Main function that initialise a state-structure and calls the state machine.
 **********/
static void
sendtip(struct cmd *cs) {
	transaction_logprintf(cs->ts, D_DEBUG, "Start Sendtip transaction");
	struct sendtip_state *state;
	state = zmalloc(sizeof(*state));
	state->cs = cs;
	state->state = SENDTIP_INIT;

	sendtip_fsm(state, NULL, NULL);
}

ADD_COMMAND(sendtip, NULL, sendtip, parameters, NULL);
