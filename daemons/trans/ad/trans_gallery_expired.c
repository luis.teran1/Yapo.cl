#include <stdio.h>
#include <time.h>

#include "command.h"
#include "trans.h"
#include "util.h"
#include "bconf.h"
#include "validators.h"
#include "db.h"
#include <ctemplates.h>
#include <queue.h>

extern struct bconf_node *bconf_root;

#define GALLERY_EXPIRED_DONE	0
#define GALLERY_EXPIRED_ERROR	1
#define GALLERY_EXPIRED_INIT	2
#define GALLERY_EXPIRED_LOAD_ADS	3
#define GALLERY_EXPIRED_RUN	4

struct gallery_expired_queue {
	SLIST_ENTRY(gallery_expired_queue) link;
	int ad_id;
};

struct gallery_expired_state {
	int state;
	struct cmd *cs;
	SLIST_HEAD(, gallery_expired_queue) queue;
	int running;
};

static struct c_param parameters[] = {
	{NULL}
};

static const char * gallery_expired_fsm(void *, struct sql_worker *, const char *);

static void gallery_expired_done(struct gallery_expired_state *state) {
	struct cmd *cs = state->cs;

	free(state);
	cmd_done(cs);
}

static void
gallery_expired_run_cb(struct internal_cmd *cmd, const char *line, void *data) {
	struct gallery_expired_state *state = data;

	if (!line && !--state->running)
		gallery_expired_fsm(state, NULL, NULL);
}

static const char *
gallery_expired_fsm(void *v, struct sql_worker *worker, const char *errstr) {
	struct gallery_expired_state *state = v;
	struct cmd *cs = state->cs;
	struct bpapi ba;

	/*
	 * General error handling for db errors.
	 */
	if (errstr) {
		if (strstr(errstr, "ERROR_INVALID_STATE")) {
			cs->status = "TRANS_ERROR";
			cs->error = "ERROR_INVALID_STATE";
		} else if (strstr(errstr, "ERROR_NO_SUCH_ACTION")) {
			cs->status = "TRANS_ERROR";
			cs->error = "ERROR_NO_SUCH_ACTION";
		} else {
			cs->status = "TRANS_DATABASE_ERROR";
			cs->message = xstrdup(errstr);
		}
		state->state = GALLERY_EXPIRED_ERROR;
	}

	while (state->state != GALLERY_EXPIRED_DONE) {
		switch (state->state) {
			case GALLERY_EXPIRED_INIT:
				BPAPI_INIT(&ba, NULL, pgsql);

				state->state = GALLERY_EXPIRED_LOAD_ADS;
				sql_worker_template_query(&config.pg_master,
						"sql/gallery_select_expired.sql", &ba,
						gallery_expired_fsm, state, cs->ts->log_string);
				bpapi_output_free(&ba);
				bpapi_simplevars_free(&ba);
				bpapi_vtree_free(&ba);
				return NULL;

			case GALLERY_EXPIRED_LOAD_ADS:
				state->state = GALLERY_EXPIRED_RUN;

				while (worker->next_row(worker)) {
					transaction_printf(cs->ts, "gallery_expired:%s\n", worker->get_value(worker, 0));
					if (worker->get_value(worker, 0)) {
						struct gallery_expired_queue *queue = xmalloc(sizeof(*queue));
						queue->ad_id = atoi(worker->get_value(worker, 0));
						SLIST_INSERT_HEAD(&state->queue, queue, link);
					} else {
						continue;
					}

				}
				continue;

			case GALLERY_EXPIRED_RUN: {
				int max_run = bconf_get_int(bconf_root, "*.gallery_expired.max_run");

				if (!max_run)
					max_run = 10;

				while (!SLIST_EMPTY(&state->queue) && state->running < max_run) {
					struct gallery_expired_queue *queue = SLIST_FIRST(&state->queue);

					SLIST_REMOVE_HEAD(&state->queue, link);

					state->running++;
					transaction_internal_printf(gallery_expired_run_cb, state,
							"cmd:remove_gallery\n"
							"commit:1\n"
							"ad_id:%d\n"
							"remote_addr:127.0.0.1\n",
							queue->ad_id
							);
					free(queue);
				}

				if (state->running)
					return NULL;

				state->state = GALLERY_EXPIRED_DONE;
				continue;
			}

			case GALLERY_EXPIRED_DONE:
				gallery_expired_done(state);
				return NULL;


			case GALLERY_EXPIRED_ERROR:
				gallery_expired_done(state);
				return NULL;
		}
	}

	gallery_expired_done(state);
	return NULL;
}

static void
gallery_expired(struct cmd *cs) {
	struct gallery_expired_state *state = zmalloc(sizeof (*state));
	state->cs = cs;
	state->state = GALLERY_EXPIRED_INIT;
	gallery_expired_fsm(state, NULL, NULL);
}

ADD_COMMAND(gallery_expired, NULL, gallery_expired, parameters, NULL);
