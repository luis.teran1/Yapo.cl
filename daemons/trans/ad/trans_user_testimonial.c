#include "factory.h" 

static struct c_param parameters[] = {
	{"ad_id", P_REQUIRED, "v_id"},
	{"testimonial",		0,			"v_testimonial"},
	{NULL, 0}
};

FACTORY_TRANSACTION(user_testimonial, &config.pg_master, "sql/call_insert_user_testimonial.sql", NULL, parameters, FACTORY_NO_RES_CNTR);
