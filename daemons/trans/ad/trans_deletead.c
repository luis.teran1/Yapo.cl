#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdarg.h>
#include <unistd.h>
#include <sys/socket.h>
#include <ctype.h>
#include <time.h>
#include <syslog.h>
#include <locale.h>

#include "logging.h"
#include "log_event.h"

#include "trans_at.h"
#include "command.h"
#include "trans.h"
#include "util.h"
#include "bconf.h"
#include "trans_mail.h"
#include "validators.h"
#include "bsapi.h"
#include "trans_loadad.h"
#include "trans_deletead.h"
#include "utils/redis_delete_ad.h"

enum delete_ad_state_val {
	DELETE_AD_DONE,
	DELETE_AD_INIT,
	DELETE_AD_CHECK_PASSWORD,
	DELETE_AD_CHECK_API_TOKEN,
	DELETE_AD_ARCHIVE_UNPAID,
	DELETE_AD_PGSQL_DELETE,
	DELETE_AD_CLEAR_STATS,
	DELETE_AD_SAVE_TO_REDIS,
	DELETE_AD_SENDMAIL,
	DELETE_AD_CLEAR_HALF_TIME,
	DELETE_AD_CLEAR_TWOWEEK
};

static const char *state_lut[] = {
	"DELETE_AD_DONE",
	"DELETE_AD_INIT",
	"DELETE_AD_CHECK_PASSWORD",
	"DELETE_AD_CHECK_API_TOKEN",
	"DELETE_AD_ARCHIVE_UNPAID",
	"DELETE_AD_PGSQL_DELETE",
	"DELETE_AD_CLEAR_STATS",
	"DELETE_AD_SAVE_TO_REDIS",
	"DELETE_AD_SENDMAIL",
	"DELETE_AD_CLEAR_HALF_TIME",
	"DELETE_AD_CLEAR_TWOWEEK"
};

static int
vf_active_api_token(struct cmd_param *param, const char *arg) {

	if (param->value) {
		struct fd_pool_conn *conn = redis_sock_conn(redis_session_pool.pool, "master");
		int exists = redis_sock_hexists(conn, param->value, "email");
		fd_pool_free_conn(conn);
		if (!exists) {
			param->error = "ERROR_APITOKEN_INVALID";
			param->cs->status = "TRANS_ERROR";
			return 1;
		} 
	} else {
		param->error = "ERROR_APITOKEN_MISSING";
		param->cs->status = "TRANS_ERROR";
		return 1;
	}
	return 0;
}

struct validator v_apitoken[] = {
	VALIDATOR_FUNC(vf_active_api_token),
	{0}
};

ADD_VALIDATOR(v_apitoken);

extern struct bconf_node *bconf_root;

static struct c_param parameters[] = {
	{"id;external_ad_id;ad_id", P_XOR, "v_loadad_id;v_external_ad_id;v_id"},
	{"passwd",                  0,     "v_passwd_not_required"},
	{"remote_addr",             0,     "v_ip"},
	{"remote_browser",          0,     "v_remote_browser"},
	{"token",                   0,     "v_token:adminad.edit_ad"},
	{"reason",                  0,     "v_reason"},
	{"testimonial",             0,     "v_testimonial"},
	{"site_area",               0,     "v_site_area"},
	{"do_not_send_mail",        0,     "v_bool"},
	{"monthly",                 0,     "v_bool"},
	{"api_token",               0,     "v_apitoken"},
	{"reg_time",                0,     "v_string_isprint"},
	{"source",                  0,     "v_source"},
	{"account_id",              0,     "v_account_id"},
	{NULL, 0}
};

static int
send_mail(struct cmd *cs, struct bpapi *ba) {
	struct internal_cmd *cmd = zmalloc(sizeof (*cmd));
	char *mailcmd;
	const char *mail_type = NULL;

	/* Mail */
	transaction_logprintf(cs->ts, D_DEBUG, "Creating email to send");

	if (cmd_getval(cs, "monthly")) {
		mail_type = "deleted_monthly";
	} else {
		mail_type = "deleted";
	}

	xasprintf(&mailcmd,
		"cmd:admail\n"
		"commit:1\n"
		"ad_id:%s\n"
		"mail_type:%s\n%s%s",
		bpapi_get_element(ba, "ad_id", 0),
		mail_type,
		(cmd_haskey(cs, "reg_time") ? "reg_time:" : ""),
		(cmd_haskey(cs, "reg_time") ? cmd_getval(cs, "reg_time") : "")
	);

	if (cmd_haskey(cs, "monthly")) {
		transaction_logprintf(cs->ts, D_DEBUG, "queueing mail with at");
		xasprintf(&cmd->cmd_string,
			"cmd:queue\n"
			"commit:1\n"
			"queue:monthly_deleted\n"
			"blob:%zu:command\n"
			"%s\n",
			strlen(mailcmd), mailcmd);
		free(mailcmd);
	} else {
		transaction_logprintf(cs->ts, D_DEBUG, "Send mail now");
		cmd->cmd_string = mailcmd;
	}

	return transaction_internal(cmd);
}

int
archive_unpaid_ad(struct cmd *cs, struct bpapi *ba) {
	const char *first_action_status = bpapi_get_element(ba, "first_action_status", 0);
	if (first_action_status && !strcmp(first_action_status, "unpaid")) {
		const char *ad_id = bpapi_get_element(ba, "ad_id", 0);
		const char *account_id = bpapi_get_element(ba, "account_id", 0);
		char *res = transaction_internal_output_printf(
			"cmd:archive_unpaid_ad\n"
			"ad_id:%s\n"
			"account_id:%s\n"
			"commit:1\n"
			"end\n",
			ad_id,
			account_id
		);
		int error = strstr(res, "status:TRANS_OK") == NULL;
		free(res);
		return error;
	}
	return 0;
}

static void
deletead(struct cmd *cs) {
	transaction_logprintf(cs->ts, D_DEBUG, "Start delete ad transaction");
	enum delete_ad_state_val state = DELETE_AD_INIT;
	const char *passwd = NULL;
	const char *errstr = NULL;
	struct timer_instance *ti = NULL;
	struct sql_worker *worker = NULL;
	struct command_bpapi cba;
	command_bpapi_init(&cba, cs, CMD_BPAPI_HOST, "controlpanel");

#define TIMER_STATE(st)  if (ti != NULL) timer_end(ti, NULL); ti = timer_start(cs->ti, state_lut[st])

	while (state != DELETE_AD_DONE) {
		TIMER_STATE(state);
		transaction_logprintf(cs->ts, D_DEBUG, "deletead fsm state %s", state_lut[state]);

		switch (state) {
		case DELETE_AD_INIT:
		{
			const char *params[] = {
				"passwd", "remote_addr", "remote_browser", "token", "reason",
				"testimonial", "site_area", "do_not_send_mail", "monthly",
				"api_token", "reg_time", "source", "deletion_reason", "ad_id",
				"user_text", "deletion_timer", "external_ad_id", "link_type",
				"account_id", NULL
			};

			for (int i = 0; params[i]; ++i)
				if (cmd_haskey(cs, params[i]))
					bpapi_insert(&cba.ba, params[i], cmd_getval(cs, params[i]));

			/* Get ad information */
			const char *sqltmpl = "sql/deletead_get_info_external_ad_id.sql";
			if (!cmd_haskey(cs, "external_ad_id")) {
				if (cmd_haskey(cs, "id"))
					bind_loadad_id (&cba.ba, cmd_getval(cs, "id"));
				sqltmpl = "sql/deletead_get_info.sql";
			}

			int got_result = 0;
			worker = sql_blocked_template_query(
				"pgsql.slave", 0, sqltmpl, &cba.ba, cs->ts->log_string, &errstr
			);

			/* Check if ad exists */
			if (worker) {
				while (sql_blocked_next_result(worker, &errstr) == 1) {
					transaction_logprintf(cs->ts, D_DEBUG, "DELETE_AD_INIT result! rows:%d", worker->rows(worker));
					if (worker->rows(worker) > 0) {
						got_result = 1;
						sql_worker_bpa_insert(worker, &cba.ba, "");
					}
				}
			}

			if (errstr)
				goto cleanup;

			if (!got_result) {
				transaction_printf(cs->ts, "%s:%s\n", "error", "ERROR_AD_ALREADY_DELETED");
				cs->status = "TRANS_ERROR";
				goto cleanup;
			}

			state = DELETE_AD_CHECK_PASSWORD;
			sql_worker_put(worker);
			worker = NULL;
		}	continue;
		
		case DELETE_AD_CHECK_PASSWORD:

			/* If imported ad or monthly delete, skip password check */
			if (!cmd_haskey(cs, "external_ad_id") && !cmd_haskey(cs, "link_type") && !cmd_haskey(cs, "monthly") && !cmd_haskey(cs, "api_token")) {
				/* Password check (if not admin) */
				passwd = cmd_getval(cs, "passwd");

				if (cmd_getval(cs, "token") == NULL
						&& (!cmd_haskey(cs, "id") || !strchr(cmd_getval(cs, "id"), '.'))
						&& (passwd != NULL
							|| bpapi_get_element(&cba.ba, "external_ad_id", 0)
							|| !cmd_haskey(cs, "reason")
							|| strcmp(cmd_getval(cs, "reason"), "import_deleted") != 0)) {
					const char *ba_password = bpapi_get_element(&cba.ba, "password", 0);
					if (passwd == NULL || (ba_password && strcmp(passwd, ba_password) != 0)) {
						const char *passwd_error = passwd ? "ERROR_PASSWORD_WRONG" : "ERROR_PASSWORD_MISSING";
						transaction_printf(cs->ts, "passwd:%s\n", passwd_error);
						cs->status = "TRANS_ERROR";
						goto cleanup;
					}
				}
			}
			state = DELETE_AD_CHECK_API_TOKEN;
			continue;

		case DELETE_AD_CHECK_API_TOKEN:
			// Checking api_token is loaded and active in redis, and belongs to the same user the ad to delete belongs
			if (cmd_haskey(cs, "api_token")) {
				struct fd_pool_conn *conn = redis_sock_conn(redis_session_pool.pool, "master");
				char *redis_email = redis_sock_hget(conn, cmd_getval(cs, "api_token"), "email");
				int email_matches = !strcmp(redis_email, bpapi_get_element(&cba.ba, "email", 0));

				fd_pool_free_conn(conn);
				free(redis_email);

				if (!email_matches) {
					transaction_printf(cs->ts, "%s:%s\n", "api_token", "ERROR_API_TOKEN_MISMATCH");
					cs->status = "TRANS_ERROR";
					goto cleanup;
				}
			}

			state = DELETE_AD_ARCHIVE_UNPAID;
			continue;

		case DELETE_AD_ARCHIVE_UNPAID:
		{
			int error = archive_unpaid_ad(cs, &cba.ba);
			if (error) {
				transaction_printf(cs->ts, "%s:%s\n", "error", "ERROR_AD_ALREADY_DELETED");
				cs->status = "TRANS_ERROR";
				goto cleanup;
			}
			state = bpapi_get_element(&cba.ba, "first_action_status", 0)
			      ? DELETE_AD_CLEAR_HALF_TIME
			      : DELETE_AD_PGSQL_DELETE;
		} continue;

		case DELETE_AD_PGSQL_DELETE:
		{
			int got_result = 0;
			worker = sql_blocked_template_query(
				"pgsql.master", 0, "sql/call_delete_ad.sql",
				&cba.ba, cs->ts->log_string, &errstr
			);

			if (worker) {
				while (sql_blocked_next_result(worker, &errstr) == 1) {
					transaction_logprintf(cs->ts, D_DEBUG, "DELETE_AD_PGSQL_DELETE result! rows:%d", worker->rows(worker));
					if (worker->rows(worker) > 0) {
						got_result = 1;
						sql_worker_bpa_insert(worker, &cba.ba, "");
					}
				}
			}

			if (errstr)
				goto cleanup;

			sql_worker_put(worker);
			worker = NULL;

			if (got_result) {
				int status = atoi(bpapi_get_element(&cba.ba, "o_status", 0));
				switch (status) {
					case 1:
						/* NOT FOUND */
					case 2:
						/* Already deleted */
						transaction_printf(cs->ts, "%s:%s\n", "error", "ERROR_AD_ALREADY_DELETED");
						cs->status = "TRANS_ERROR";
						goto cleanup;
					case 3:
						/* Password err, already checked above. XXX Should be done here? */
					case 0:
					default:
						/* OK */
						break;
				}
			}

			const char *action_id = bpapi_get_element(&cba.ba, "o_action_id", 0);
			if (!action_id || *action_id == '\0') {
				transaction_printf(cs->ts, "%s:%s\n", "error", "ERROR_NO_ACTION_ID");
				cs->status = "TRANS_ERROR";
				goto cleanup;
			}

			state = DELETE_AD_CLEAR_STATS;
		} continue;

		case DELETE_AD_CLEAR_STATS:
		{
			struct bconf_node *stat_node = bconf_get(bconf_root, "*.*.common.statpoints");
			if (!stat_node) {
				transaction_logprintf(cs->ts, D_ERROR, "No statpoints conf.");
				state = DELETE_AD_SAVE_TO_REDIS;
				continue;
			}

			struct bpapi_vtree_chain vtree;
			memset(&vtree, 0, sizeof(struct bpapi_vtree_chain));
			bconf_vtree(&vtree, stat_node);

			const char *id = cmd_getval(cs, "id");
			stat_log(&vtree, "VIEW delete:1", id);
			stat_log(&vtree, "MAIL delete:1", id);
			stat_log(&vtree, "GALLERYVIEW delete:1", id);
			vtree_free(&vtree);

			state = DELETE_AD_SAVE_TO_REDIS;
		} continue;

		case DELETE_AD_SAVE_TO_REDIS:

			bpapi_insert(&cba.ba, "raap_action", "deleted");
			save_deletead_to_redis(cs, &cba.ba);
			remove_all_ad_products(cs, &cba.ba);

			state = DELETE_AD_SENDMAIL;
			continue;

		case DELETE_AD_SENDMAIL:
		{
			/* Send email to expired ad unless they come from spiders */
			const char *external_ad_id = bpapi_get_element(&cba.ba, "external_ad_id", 0);
			if (cmd_haskey(cs, "monthly")) {
				if (!external_ad_id || !*external_ad_id)
					send_mail(cs, &cba.ba);
			} else if (cmd_getval(cs, "token") == NULL && !cmd_haskey(cs, "do_not_send_mail")) {
				send_mail(cs, &cba.ba);
			}

			transaction_logprintf(cs->ts, D_INFO, "DELETE-SENDMAIL: 200 Mail sent OK");
			xasprintf(&cs->message, "ok, ad %s deleted", bpapi_get_element(&cba.ba, "list_id", 0));

			state = DELETE_AD_CLEAR_HALF_TIME;
		} continue;

		case DELETE_AD_CLEAR_HALF_TIME:

			at_clear_commands("half_time_mail", bpapi_get_element(&cba.ba, "ad_id", 0), cs->ts->log_string);
			state = DELETE_AD_CLEAR_TWOWEEK;
			continue;

		case DELETE_AD_CLEAR_TWOWEEK:

			at_clear_commands("twoweek_mail", bpapi_get_element(&cba.ba, "ad_id", 0), cs->ts->log_string);
			state = DELETE_AD_DONE;
			continue;

		default:
			/* MAJOR STATE ERROR */
			cs->status = "TRANS_ERROR";
			transaction_logprintf(cs->ts, D_ERROR, "Unknown state (%d)", state);
			state = DELETE_AD_DONE;
		}
	}

cleanup:

	transaction_logprintf(cs->ts, D_DEBUG, "Deletead done");

	if (ti)
		timer_end(ti, NULL);

	if (errstr) {
		transaction_logprintf(cs->ts, D_ERROR, "Statement failed (%d, %s)", state, errstr);
		cs->status = "TRANS_DATABASE_ERROR";
		cs->message = xstrdup(errstr);
	}

	if (worker) {
		while (sql_blocked_next_result(worker, &errstr) != 0)
			transaction_logprintf(cs->ts, D_ERROR, "DATABASE ERROR: %s", errstr);
		sql_worker_put(worker);
	}

	command_bpapi_free(&cba);
	cmd_done(cs);
}

ADD_COMMAND(deletead, NULL, deletead, parameters, NULL);
