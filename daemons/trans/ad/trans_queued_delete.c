#include "factory.h"
#include "utils/create_session_hash.h"
#include "utils/redis_delete_ad.h"

static const struct factory_action delete_ad = FA_TRANS("trans/call_deletead.txt", "del_", FATRANS_OUT);

static const struct factory_action *
is_last_action(struct cmd *cs, struct bpapi *ba) {
    if ( atoi(cmd_getval(cs, "action_id")) == bpapi_get_int(ba, "max_action_id") ) {
        return &delete_ad;
    } else if ( atoi(cmd_getval(cs, "action_id")) > bpapi_get_int(ba, "max_action_id") ) {
		transaction_logprintf(cs->ts, D_ERROR, "SENT AID IS BIGGER THAN CURRENT MAX %d vs %d", atoi(cmd_getval(cs, "action_id")), bpapi_get_int(ba, "max_action_id"));
	}
    return NULL;
}

static struct c_param parameters[] = {
	{"action_id",				P_XOR, "v_integer"},
	{"id;external_ad_id;ad_id", P_XOR, "v_loadad_id;v_external_ad_id;v_id"},
	{"passwd",                  0,     "v_passwd_not_required"},
	{"remote_addr",             0,     "v_ip"},
	{"remote_browser",          0,     "v_remote_browser"},
	{"token",                   0,     "v_token:adminad.edit_ad"},
	{"reason",                  0,     "v_reason"},
	{"testimonial",             0,     "v_testimonial"},
	{"site_area",               0,     "v_site_area"},
	{"do_not_send_mail",        0,     "v_bool"},
	{"monthly",                 0,     "v_bool"},
	{"reg_time",                0,     "v_string_isprint"},
	{"source",                  0,     "v_source"},
	{NULL, 0}
};

static struct factory_data data = {
	"queued_delete",
	FACTORY_RES_SHORT_FORM
};

static const struct factory_action actions[]= {
	FA_SQL("", "pgsql.master", "sql/last_action.sql", FASQL_OUT),
	FA_COND(is_last_action),
	{0}
};

FACTORY_TRANS(queued_delete, parameters, data, actions);
