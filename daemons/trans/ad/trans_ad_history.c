#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <time.h>

#include "trans.h"
#include <ctemplates.h>
#include "validators.h"
#include <bconf.h>
#include <event.h>

extern struct bconf_node *bconf_root;

#define AD_HISTORY_DONE 0
#define AD_HISTORY_ERROR 1
#define AD_HISTORY_START 2
#define AD_HISTORY_RESULT 3

struct ad_history_state {
	int state;
	struct cmd *cs;
};

static void ad_history_done(void *v) {
	struct ad_history_state *state = v;
	struct cmd *cs = state->cs;

	free(state);
	cmd_done(cs);
}

static const char *
ad_history_fsm(void *v, struct sql_worker *worker, const char *errstr) {
	struct ad_history_state *state = v;
	struct cmd *cs = state->cs;
	struct bpapi ba;
	unsigned int cnt = 0;

	transaction_logprintf(cs->ts, D_DEBUG, "ad_history fsm");

	if (state->state == AD_HISTORY_ERROR) {
		ad_history_done(state);
		return NULL;
	}

	/*
	 * General error handling for db errors.
	 */
	if (errstr) {
		cs->status = "TRANS_DATABASE_ERROR";
		cs->message = xstrdup(errstr);
		state->state = AD_HISTORY_ERROR;
	}

	while (state->state != AD_HISTORY_DONE && state->state != AD_HISTORY_ERROR) {
		switch (state->state) {
		case AD_HISTORY_START:
			transaction_logprintf(cs->ts, D_DEBUG, "ad_history send query");

			BPAPI_INIT_APP(&ba, host_bconf, "controlpanel", buf, pgsql);
			if (cmd_haskey(cs, "ad_id"))
				bpapi_insert(&ba, "ad_id", cmd_getval(cs, "ad_id"));

			sql_worker_template_query(&config.pg_master, "sql/call_ad_history.sql", &ba,
						  ad_history_fsm, state, cs->ts->log_string);
			bpapi_output_free(&ba);
			bpapi_simplevars_free(&ba);
			bpapi_vtree_free(&ba);
			state->state = AD_HISTORY_RESULT;
			return NULL;

		case AD_HISTORY_RESULT:
			transaction_logprintf(cs->ts, D_DEBUG, "ad_history got result from query");

			cnt = 0;
			while (worker->next_row(worker)) {
				transaction_printf(cs->ts, "ad_history.%u.action_type:%s\n", cnt, worker->get_value(worker, 0));
				transaction_printf(cs->ts, "ad_history.%u.state:%s\n", cnt, worker->get_value(worker, 1));
				transaction_printf(cs->ts, "ad_history.%u.transition:%s\n", cnt, worker->get_value(worker, 2));
				transaction_printf(cs->ts, "ad_history.%u.timestamp:%s\n", cnt, worker->get_value(worker, 3));
				transaction_printf(cs->ts, "ad_history.%u.admin:%s\n", cnt, worker->get_value(worker, 4));
				cnt++;
			}
			state->state = AD_HISTORY_DONE;
			break;

		default:
			transaction_logprintf(cs->ts, D_ERROR, "Unknown ad_history state %d", state->state);
			cs->status = "TRANS_DATABASE_ERROR";
			state->state = AD_HISTORY_ERROR;
		}
	}

	ad_history_done(state);
	return NULL;
}

static void
ad_history(struct cmd *cs) {
	transaction_logprintf(cs->ts, D_DEBUG, "ad_history command started");
	struct ad_history_state *state;
	state = zmalloc(sizeof(*state));

	state->cs = cs;
	state->state = AD_HISTORY_START;

	ad_history_fsm(state, NULL, NULL);
}

static struct c_param ad_history_parameters[] = {
	{"ad_id", P_REQUIRED, "v_id"},
	{NULL, 0, NULL}
};

ADD_COMMAND(ad_history, NULL, ad_history, ad_history_parameters, NULL);

