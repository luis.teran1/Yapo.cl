#include <stdio.h>
#include <time.h>

#include "command.h"
#include "trans.h"
#include "util.h"
#include "bconf.h"
#include "validators.h"
#include "trans_mail.h"
#include "db.h"
#include "hash.h"
#include <ctemplates.h>
#include "trans_newad.h"
#include "log_event.h"
#include "trans_at.h"
#include "utils/first_experience_bump_timer.h"
#include "utils/send_feedback_event.h"
#include "utils/send_backend_event.h"
#include "utils/send_newad_event.h"

extern struct bconf_node *bconf_root;

typedef enum {
	REVIEW_DONE,
	REVIEW_ERROR,
	REVIEW_INIT,
	REVIEW_GET_CATEGORY,
	REVIEW_GET_ADPARAMS,
	REVIEW_CHECK,
	REVIEW_REMOVE_UPSELLING_PRODUCTS,
	REVIEW_MAIL,
	REVIEW_UNLOAD,
	REVIEW_REVIEW,
	REVIEW_DAV_GET_MEDIA,
	REVIEW_DAV_GET_MEDIA_LOAD,
	REVIEW_DAV_MEDIA_CHECK,
	REVIEW_DAV_MEDIA_CHECK_DONE,
	REVIEW_GET_CLEARED_BY,
	REVIEW_GET_CLEARED_BY_DATA,
	REVIEW_CHECK_PRO
} state_t;

static const char *state_lut[] = {
	"REVIEW_DONE",
	"REVIEW_ERROR",
	"REVIEW_INIT",
	"REVIEW_GET_CATEGORY",
	"REVIEW_GET_ADPARAMS",
	"REVIEW_CHECK",
	"REVIEW_REMOVE_UPSELLING_PRODUCTS",
	"REVIEW_MAIL",
	"REVIEW_UNLOAD",
	"REVIEW_REVIEW",
	"REVIEW_DAV_GET_MEDIA",
	"REVIEW_DAV_GET_MEDIA_LOAD",
	"REVIEW_DAV_MEDIA_CHECK",
	"REVIEW_DAV_MEDIA_CHECK_DONE",
	"REVIEW_GET_CLEARED_BY",
	"REVIEW_GET_CLEARED_BY_DATA",
	"REVIEW_CHECK_PRO"	
};

static const char * review_fsm(void *, struct sql_worker *, const char *);

struct media_list_entry {
	const char *name;
	TAILQ_ENTRY (media_list_entry) list;
};

struct review_state {
	state_t state;
	struct cmd *cs;
	char *sql;
	struct bpapi ba;
	int state_cnt;
	struct timer_instance *ti;
	struct hash_table *params;
	TAILQ_HEAD(,media_list_entry) media;
	int media_checks_pending;
	int media_check_errors;
	char *action_id;
	char *action_type;
	int category;
	char *type;
	int company_ad;
	char *upselling_action_id;
};

struct validator v_refusal_text[] = {
	VALIDATOR_LEN(1, 2048, "ERROR_REFUSAL_TEXT_SIZE"),
	{0}
};
ADD_VALIDATOR(v_refusal_text);


static struct c_param refuse_params[] = {
	{"reason", 0, "v_reason"},
	{"refusal_text",	0,	"v_refusal_text"},
	{NULL}
};

/* XXX Shouldn't use v_newad_params nor v_category_check */
static struct c_param accept_params[] = {
	{"category", 		0,	"v_category_check"},
	{"company_ad", 		0,	"v_bool"},
	{"remove_infopage", 	0,	"v_bool"},
	{"currency", 		0,  	  "v_currency"},
	{"price", 		0,  	  "v_currency_check"},
	{"subject", 		0,	"v_subject"},
	{"body", 		0,	"v_body_optional"},
	{"remote_addr", 	P_REQUIRED,	"v_ip"},
	{"remote_browser", 	0,	"v_remote_browser"},
	{"remove_image0",	0,	"v_bool"},
	{"remove_image1",	0,	"v_bool"},
	{"remove_image2",	0,	"v_bool"},
	{"remove_image3",	0,	"v_bool"},
	{"remove_image4",	0,	"v_bool"},
	{"remove_image5",	0,	"v_bool"},
	{"remove_image6",	0,	"v_bool"},
	{"remove_image7",	0,	"v_bool"},
	{"remove_image8",	0,	"v_bool"},
	{"remove_image9",	0,	"v_bool"},
	{"remove_image10",	0,	"v_bool"},
	{"remove_image11",	0,	"v_bool"},
	{"remove_image12",	0,	"v_bool"},
	{"remove_image13",	0,	"v_bool"},
	{"remove_image14",	0,	"v_bool"},
	{"remove_image15",	0,	"v_bool"},
	{"remove_image16",	0,	"v_bool"},
	{"remove_image17",	0,	"v_bool"},
	{"remove_image18",	0,	"v_bool"},
	{"remove_image19",	0,	"v_bool"},
	{"reason",		0,	"v_reason"},
	{"set_first",		0,	"v_first_image"},
	{"type", 		0,	"v_type"},
	{"_params",		0,	"v_newad_params"},
	{NULL}
};

static void
review_media_davcheck_cb(struct davdb *dav) {
	struct review_state *state = dav->data;

	if (dav->result != 200) {
		++state->media_check_errors;
	}

	if (--state->media_checks_pending == 0) {
		review_fsm(state, NULL, NULL);	
	}

	free(dav);
}

static int
v_action_func(struct cmd_param *param, const char *arg) {
	struct c_param *extra_params = NULL;

	if (strcmp(param->value, "accept") == 0 || strcmp(param->value, "accept_with_changes") == 0)
		extra_params = accept_params;
	else if (strcmp(param->value, "refuse") == 0)
		extra_params = refuse_params;

	if (extra_params) {
		while (extra_params->name)
			cmd_extra_param(param->cs, extra_params++);
	}
	return 0;
}


struct validator v_action_type[] = {
	VALIDATOR_REGEX("^accept$|^accept_with_changes|^refuse$", REG_EXTENDED|REG_NOSUB, "ERROR_ACTION"),
	VALIDATOR_FUNC(v_action_func),
	{0}
};

ADD_VALIDATOR(v_action_type);

static struct c_param req_token = {"token", P_REQUIRED,	"v_token:adqueue"};
static struct c_param opt_token = {"token", 0,		"v_token:adqueue"};

static int
vf_review_filter_name(struct cmd_param *param, const char *arg) {
	if (!bconf_vget(bconf_root, "*", "controlpanel", "modules", "adqueue", "auto", param->value, NULL))
		cmd_extra_param(param->cs, &req_token);
	else
		cmd_extra_param(param->cs, &opt_token);
	return 0;
}

struct validator v_review_filter_name[] = {
	VALIDATOR_FUNC(vf_review_filter_name),
	{0}
};
ADD_VALIDATOR(v_review_filter_name);


static struct c_param parameters[] = {
	{"ad_id",            0,          "v_ad_id"},
	{"action_id",        P_REQUIRED, "v_action_id"},
	{"action",           P_REQUIRED, "v_action_type"},
	{"do_not_send_mail", 0,          "v_bool"},
	{"remote_addr",      P_REQUIRED, "v_ip"},
	{"remote_browser",   0,          "v_remote_browser"},
	{"filter_name",	     P_REQUIRED, "v_review_filter_name"},
	{NULL}
};

static void review_done_init_ba(struct cmd *cs, struct bpapi *ba) {
	const char *reason = NULL;

	BPAPI_INIT(ba, NULL, pgsql);
	bpapi_insert(ba, "ad_id", cmd_getval(cs, "ad_id"));
	bpapi_insert(ba, "action_id", cmd_getval(cs, "action_id"));
	bpapi_insert(ba, "action_type", cmd_getval(cs, "action"));
	if (cmd_haskey(cs, "reason")) {
		reason = cmd_getval(cs, "reason");
		bpapi_insert(ba, "reason", reason);
	}
}

static void review_done(struct review_state *state) {
	struct cmd *cs = state->cs;

	// Start queues sent
	struct bpapi ba;
	review_done_init_ba(cs, &ba);

	// Check if action is accept_with_changes. If so, send to implio for update
	const char *action_type = bpapi_get_element(&ba, "action_type", 0);
	// Check if it should send a feedback event
	const char *feedback_event_conf = "*.trans.review.feedback_event.send_enabled";
	char *ae_enabled = transaction_internal_output_printf("cmd:get_dyn_bconf\nkey:%s\ncommit:1\n", feedback_event_conf);
	if (action_type && !strcmp(action_type, "accept_with_changes")) {
		// Check if it should send a newad event
		const char *newad_with_changes_event_conf = "*.trans.newad.review_with_changes_event.send_enabled";
		char *awce_enabled = transaction_internal_output_printf("cmd:get_dyn_bconf\nkey:%s\ncommit:1\n", newad_with_changes_event_conf);
		if (awce_enabled && strstr(awce_enabled, "value:1")) {
			transaction_logprintf(cs->ts, D_INFO, "SEND AD EVALUATION ACTIVE - ACCEPT WITH CHANGES");
			review_done_init_ba(cs, &ba);
			publish_review_with_changes_event(cs, &ba);
		}
	} else if (ae_enabled && strstr(ae_enabled, "value:1")) {
		transaction_logprintf(cs->ts, D_INFO, "SEND ADS' FEEDBACK ACTIVE");
		review_done_init_ba(cs, &ba);
		publish_feedback_event(cs, &ba);

	}

	// Check if it should send a backend event
	int be_enabled = bconf_get_int(trans_bconf, "review.backend_event.send_enabled");
	if (be_enabled) {
		bpapi_insert(&ba, "type", "reviewad");
		bpapi_insert(&ba, "param_ad_id", cmd_getval(cs, "ad_id"));
		bpapi_insert(&ba, "param_action_id",cmd_getval(cs, "action_id"));
		bpapi_insert(&ba, "param_action_type", cmd_getval(cs, "action"));
		if (cmd_haskey(cs, "reason")) {
			bpapi_insert(&ba, "param_reason", cmd_getval(cs, "reason"));
		}
		transaction_logprintf(cs->ts, D_INFO, "SEND ADS' BACKEND ACTIVE");
		publish_backend_event(cs, &ba);
	}

	bpapi_output_free(&ba);
	bpapi_simplevars_free(&ba);
	bpapi_vtree_free(&ba);
	free(ae_enabled);
	// End Queues sent

	bpapi_output_free(&state->ba);
	bpapi_simplevars_free(&state->ba);
	bpapi_vtree_free(&state->ba);

	if (state->params)
		hash_table_free(state->params);

	free(state->action_id);
	free(state->action_type);
	free(state->upselling_action_id);
	free(state->type);
	free(state);
	cmd_done(cs);
}

static int
send_mail(struct cmd *cs, struct review_state *state) {
	struct internal_cmd *cmd = zmalloc(sizeof (*cmd));
	const char *reason = NULL;
	char *refusal_text = NULL;
	char *mailcmd;

	if (cmd_haskey(cs, "reason"))
		reason = cmd_getval(cs, "reason");

	if (cmd_haskey(cs, "refusal_text"))
		xasprintf(&refusal_text, 
				"blob:%zu:refusal_text\n"
				"%s\n",
				strlen(cmd_getval(cs, "refusal_text")),
				cmd_getval(cs, "refusal_text"));

	char *mail_type = NULL;
	const char *filter_name = cmd_getval(cs, "filter_name");
	const char *action = cmd_getval(cs, "action");
	const char *action_type = state->action_type;

	if (filter_name && strcmp(filter_name, "autorefuse") == 0) {
		/* autorefuse shouldn't send a mail */
		return 0;
	} else if (filter_name && strcmp(filter_name, "double_pro_refuse") == 0) {
		/* double_pro_refuse shouldn't send a mail */
		return 0;
	} else if (filter_name && strncmp(filter_name, "spidered", 8) == 0) {
		mail_type = xstrdup("accept_spidered");  	
	} else if (filter_name && strcmp(filter_name, "category_change") == 0 ) {
		mail_type = xstrdup("category_changed");
	} else if (filter_name && strcmp(filter_name, "rejected_wrong_category") == 0 ) {
		mail_type = xstrdup("rejected_wrong_category");
	} else {
		/* Chooses the mail_type based on the action (accept / refuse) and action_type that comes from ad_actions table - rsouza & rcaldeira */
		if (strcmp(action, "accept") == 0) {
			if ( (strcmp(action_type, "editrefused") == 0) || (strcmp(action_type, "edit") == 0) ) {
				mail_type = xstrdup("edit_accept");
			} else {
				mail_type = xstrdup("accept");
			}
		} else if (strcmp(action, "accept_with_changes") == 0) {
			mail_type = xstrdup("accept_with_changes");
		} else {
			if (strcmp(action_type, "editrefused") ==  0) {	
				mail_type = xstrdup("edit_refused_refuse");
			} else if (strcmp(action_type, "edit") == 0) {
				mail_type = xstrdup("edit_refuse");
			} else {
				mail_type = xstrdup("refuse");
			}
		}
	}

	/* Mail */
	transaction_logprintf(cs->ts, D_DEBUG, "Sending mailtype(%s) from action in queue(%s)", mail_type, filter_name );
	xasprintf(&mailcmd,
			"cmd:admail\n"
			"commit:1\n"
			"ad_id:%s\n"
			"action_id:%s\n"
			"mail_type:%s\n"
			"%s%s%s"
			"%s",
			cmd_getval(cs, "ad_id"),
			state->action_id ?: cmd_getval(cs, "action_id"),
			mail_type,
			reason ? "reason:" : "",
			reason ? reason : "",
			reason ? "\n" : "",
			refusal_text ? refusal_text : "");

	free(mail_type);

	if (refusal_text)
		free(refusal_text);

	 if(strcmp(cmd_getval(cs, "action"), "accept_with_changes") == 0) {
		transaction_logprintf(cs->ts, D_DEBUG, "Holding accept with changes mail");
		xasprintf(&cmd->cmd_string,
				"cmd:queue\n"
				"commit:1\n"
				"queue:accepted_mail\n"
				"blob:%zu:command\n"
				"%s\n",
				strlen(mailcmd), mailcmd);
		free(mailcmd);
	} else if (strcmp(cmd_getval(cs, "action"), "accept") == 0) {
		transaction_logprintf(cs->ts, D_DEBUG, "Holding accept mail");
		xasprintf(&cmd->cmd_string,
				"cmd:queue\n"
				"commit:1\n"
				"queue:accepted_mail\n"
				"blob:%zu:command\n"
				"%s\n",
				strlen(mailcmd), mailcmd);
		free(mailcmd);
	} else {
		transaction_logprintf(cs->ts, D_DEBUG, "Send mail now");
		cmd->cmd_string = mailcmd;
	}

	return transaction_internal(cmd);
}

static void
bind_review(struct bpapi *ba, struct review_state *state) {
	struct cmd *cs = state->cs;
	struct c_param *param;
	const char *action;
	const char *type;
	int i;
	char imagename[24];

	for (param = parameters; param->name; param++) {
		if (cmd_haskey (cs, param->name))
			bpapi_insert(ba, param->name, cmd_getval(cs, param->name));
	}
	if (cmd_haskey(cs, "token"))
		bpapi_insert(ba, "token", cmd_getval(cs, "token"));

	action = cmd_getval(cs, "action");
	if (!strcmp(action, "accept") || !strcmp(action, "accept_with_changes")) {
		if (cmd_haskey(cs, "type")) {
			type = cmd_getval(cs, "type");
			switch (type[0]) {
			case 'k':
				bpapi_insert(ba, "ads_type", "buy");
				break;
			case 's':
				bpapi_insert(ba, "ads_type", "sell");
				break;
			case 'u':
				bpapi_insert(ba, "ads_type", "let");
				break;
			case 'h':
				bpapi_insert(ba, "ads_type", "rent");
				break;
			case 'b':
				bpapi_insert(ba, "ads_type", "swap");
				break;
			}
		} else if (state->type) {
			type = state->type;
			bpapi_insert(ba, "ads_type", state->type);
		} else
			type = NULL;
		if (cmd_haskey (cs, "remove_infopage"))
			bpapi_insert(ba, "remove_infopage", cmd_getval(cs, "remove_infopage"));
		if (cmd_haskey (cs, "infopage_title"))
			bpapi_insert(ba, "infopage_title", cmd_getval(cs, "infopage_title"));

		if (cmd_haskey(cs, "set_first")) {
                        const char *set_first = cmd_getval(cs, "set_first");
                        if (*set_first)
                                bpapi_insert(ba, "set_first", set_first);
		}

		for (i = 0; i <= 19; i++) {
			snprintf(imagename, sizeof(imagename), "remove_image%d", i);
			if (!cmd_haskey(cs, imagename))
				bpapi_insert(ba, "remove_images", "f");
			else
				bpapi_insert(ba, "remove_images", (strcmp(cmd_getval(cs, imagename), "1") == 0) ? "t" : "f");
		}

		if (cmd_haskey(cs, "price")) {
                        const char *price = cmd_getval(cs, "price");
                        if (*price)
                                bpapi_insert(ba, "price", price);
                }
		if (cmd_haskey(cs, "subject")) {
			const char *subject = cmd_getval(cs, "subject");
			if (*subject)
				bpapi_insert(ba, "subject", subject);
		}
		if (cmd_haskey(cs, "body")) {
			const char *body = cmd_getval(cs, "body");
			if (*body)
				bpapi_insert(ba, "body", body);
		}

		if (cmd_haskey(cs, "reason")) {
			const char *reason = cmd_getval(cs, "reason");
			if (*reason)
				bpapi_insert(ba, "reason", reason);
		}

		if (cmd_haskey (cs, "category")) {
			const char *features;

			bpapi_insert(ba, "category", cmd_getval(cs, "category"));
			if (type && (features = bconf_value(bconf_vget(bconf_root, "*", "*", "categories",
							cmd_getval(cs, "category"), type, "features", NULL)))) {
				const char *nextfeat;

				for (; features; features = nextfeat) {
					nextfeat = strchr(features, ',');
					if (nextfeat)
						bpapi_insert_maxsize(ba, "valid_params", features, nextfeat++ - features);
					else
						bpapi_insert(ba, "valid_params", features);
				}
			}
		}
		if (cmd_haskey (cs, "company_ad"))
			bpapi_insert(ba, "company_ad", cmd_getval(cs, "company_ad"));
		bpapi_insert(ba, "filter_name", cmd_getval(cs, "filter_name"));
		bind_newad_params(ba, cs);
	} else if (!strcmp(action, "refuse")) {
		if (cmd_haskey(cs, "reason"))
			bpapi_insert(ba, "reason", cmd_getval(cs, "reason"));
		if (cmd_haskey(cs, "refusal_text"))
			bpapi_insert(ba, "refusal_text", cmd_getval(cs, "refusal_text"));
	}
}

static const char *
review_fsm(void *v, struct sql_worker *worker, const char *errstr) {
	struct review_state *state = v;
	struct cmd *cs = state->cs;
	struct bpapi ba;

	transaction_logprintf(cs->ts, D_DEBUG, "review_fsm state = %s (%d)", state_lut[state->state], state->state);

	if (state->sql) {
		free(state->sql);
		state->sql = NULL;
	}

	if (state->state == REVIEW_ERROR) {
		review_done(state);
		return NULL;
	}
	/*
	 * General error handling
	 */
	if (errstr) {
		if (strstr(errstr, "ERROR_INVALID_STATE")) {
			cs->status = "TRANS_ERROR";
			cs->error = "ERROR_INVALID_STATE";
		} else if (strstr(errstr, "ERROR_NO_SUCH_ACTION")) {
			cs->status = "TRANS_ERROR";
			cs->error = "ERROR_NO_SUCH_ACTION";
		} else {
			cs->status = "TRANS_DATABASE_ERROR";
			cs->message = xstrdup(errstr);
		}
		state->state = REVIEW_ERROR;
	}

	while (state->state != REVIEW_DONE) {
#define TIMER_STATE(st) if (state->ti != NULL) timer_end(state->ti, NULL); state->ti = timer_start(state->cs->ti, "REVIEW_"#st)
		if (state->ti) {
			timer_end(state->ti, NULL);
			state->ti = NULL;
		}

		switch ((state_t)state->state) {
		case REVIEW_INIT:
//First experiencie logic
			if ((strcmp(cmd_getval(cs, "action"), "accept") == 0 || strcmp(cmd_getval(cs, "action"), "accept_with_changes") == 0) && strcmp(cmd_getval(cs, "filter_name"), "first_da") == 0) {
				const char *category_ad = cmd_getval(cs, "category");
				struct bconf_node *bconf_request = bconf_vget(bconf_root, "*.*", "first_experience", "category", NULL);
				transaction_logprintf(cs->ts, D_DEBUG, "First ad from category:%s", category_ad);
				if(bconf_get_int(bconf_request, category_ad)) {
					char *first_bump_cmd;
					time_t timer_for_bump = get_timer_for_bump();
					xasprintf(&first_bump_cmd,
						"cmd:first_experience_bump\n"
						"commit:1\n"
						"ad_id:%s\n",
						cmd_getval(cs, "ad_id"));
					if (debug_level == D_DEBUG){
						char *readable_time = get_timer_for_bump_human_readable(timer_for_bump);
						transaction_logprintf(cs->ts, D_DEBUG, "Aplying first bump for ad_id=%s, category=%s in %s",
							cmd_getval(cs, "ad_id"), category_ad, readable_time);
						free(readable_time);
					}
					transaction_internal_printf(NULL,
						state,
						"cmd:queue\n"
						"commit:1\n"
						"queue:weekly_bump\n"
						"timeout:%ld\n"
						"blob:%zu:command\n"
						"%s\n",
						timer_for_bump,
						strlen(first_bump_cmd),
						first_bump_cmd);
					free(first_bump_cmd);
				}
			}
			if (strcmp(cmd_getval(cs, "action"), "accept") == 0 || strcmp(cmd_getval(cs, "action"), "accept_with_changes") == 0) {
				if (!cmd_haskey(cs, "category")) {
					state->state = REVIEW_GET_CATEGORY;
					BPAPI_INIT(&ba, NULL, pgsql);
					bpapi_insert(&ba, "ad_id", cmd_getval(cs, "ad_id"));
					bpapi_insert(&ba, "action_id", cmd_getval(cs, "action_id"));
					sql_worker_template_query_flags(&config.pg_master, SQLW_FINAL_CALLBACK,
									"sql/load_category.sql", &ba,
									review_fsm, state, cs->ts->log_string);

					bpapi_output_free(&ba);
					bpapi_simplevars_free(&ba);
					bpapi_vtree_free(&ba);
					return NULL;
				} else {
					state->state = REVIEW_GET_ADPARAMS;
					continue;
				}
			} else if (strcmp(cmd_getval(cs, "action"), "refuse") == 0) {
				state->state = REVIEW_GET_ADPARAMS;	
				continue;
			} 

			state->state = REVIEW_REVIEW;
			continue;

		case REVIEW_GET_CATEGORY:
			if (worker->sw_state == RESULT_DONE) {
				state->state = REVIEW_GET_ADPARAMS;
				sql_worker_set_wflags(worker, 0);
				continue;
			}

			if (worker->next_row(worker)) {	
				state->category = atoi(worker->get_value(worker, 0));
				state->type = xstrdup(worker->get_value(worker, 1));
			}
			return NULL;

		case REVIEW_GET_ADPARAMS:
			state->state = REVIEW_UNLOAD;

			/* Modify price, in case it comes in UFs, before sending it to the DB */
			format_price(cs, NULL);

			BPAPI_INIT(&ba, NULL, pgsql);
			bpapi_insert(&ba, "ad_id", cmd_getval(cs, "ad_id"));
			bpapi_insert(&ba, "action_id", cmd_getval(cs, "action_id"));

			sql_worker_template_query_flags(&config.pg_master, SQLW_FINAL_CALLBACK,
							"sql/load_adparams.sql", &ba,
							review_fsm, state, cs->ts->log_string);

			bpapi_output_free(&ba);
			bpapi_simplevars_free(&ba);
			bpapi_vtree_free(&ba);
			return NULL;

		case REVIEW_UNLOAD:
			if (worker->sw_state == RESULT_DONE) {
				if (bconf_get_int(bconf_root, "*.*.common.media.dav_check.enabled")) {
					state->state = REVIEW_DAV_GET_MEDIA;
				} else {
					state->state = REVIEW_REVIEW;
				}
				sql_worker_set_wflags(worker, 0);
				continue;
			}

			if (!state->params) {
				state->params = hash_table_create(16, free);
				hash_table_free_keys(state->params, 1);
			}

			while (worker->next_row(worker)) {
				int cols = worker->fields(worker);
				if (cols == 2) {
					hash_table_insert(state->params, xstrdup(worker->get_value(worker, 0)), -1, xstrdup(worker->get_value(worker, 1)));
					transaction_logprintf(cs->ts, D_DEBUG, "TRANS_REVIEW: param %s = %s ",
							      worker->get_value(worker, 0),
							      worker->get_value(worker, 1)); 
				}
			}

			return NULL;

		case REVIEW_DAV_GET_MEDIA:
			if (strcmp(cmd_getval(cs, "action"), "accept") == 0 || strcmp(cmd_getval(cs, "action"), "accept_with_changes") == 0) {
				state->state = REVIEW_DAV_GET_MEDIA_LOAD;

				BPAPI_INIT(&ba, NULL, pgsql);
				bpapi_insert(&ba, "ad_id", cmd_getval(cs, "ad_id"));
				bpapi_insert(&ba, "action_id", cmd_getval(cs, "action_id"));

				sql_worker_template_query(&config.pg_master, "sql/load_media.sql", &ba, review_fsm, state, cs->ts->log_string);

				TAILQ_INIT(&state->media);
				bpapi_output_free(&ba);
				bpapi_simplevars_free(&ba);
				bpapi_vtree_free(&ba);
				return NULL;
			} else {
				state->state = REVIEW_REVIEW;
			}
			continue;
		case REVIEW_DAV_GET_MEDIA_LOAD:
		{
			state->state = REVIEW_DAV_MEDIA_CHECK;
			while (worker->next_row(worker)) {
				struct media_list_entry *entry = zmalloc(sizeof(struct media_list_entry));
				entry->name = xstrdup(worker->get_value(worker, 0));

				TAILQ_INSERT_TAIL(&state->media, entry, list);
				++state->media_checks_pending;
			}
			continue;
		}

		case REVIEW_DAV_MEDIA_CHECK:
		{
			state->state = REVIEW_DAV_MEDIA_CHECK_DONE;
			struct media_list_entry *entry;

			if (TAILQ_EMPTY(&state->media)) {
				state->state = REVIEW_REVIEW;
				continue;
			}

			while ((entry = TAILQ_FIRST(&state->media)) != NULL) {
				TAILQ_REMOVE(&state->media, entry, list);

				struct davdb *dav = zmalloc(sizeof (*dav));

				dav->cs = state->cs;
				dav->command = "GET";
				dav->data = state;
				dav->cb = review_media_davcheck_cb;
				dav->filename = entry->name;
				davdb_execute(dav);

				free(entry);
			}

			return NULL;
		}

		case REVIEW_DAV_MEDIA_CHECK_DONE:
		{
			if (state->media_check_errors) {
				const char* reason = bconf_get_string(bconf_root, "*.*.common.media.dav_check.auto_refuse_reason");

				cmd_setval(cs, "action", xstrdup("refuse"));
				if (reason)
					cmd_setval(cs, "reason", xstrdup(reason));
				cs->status = "TRANS_ERROR";
				cs->error = "ERROR_AUTO_REFUSE_IMAGE";
				transaction_logprintf(cs->ts, D_INFO, "%d DAV media errors detected, ad auto-refused", state->media_check_errors);
			}

			state->state = REVIEW_REVIEW;
			continue;
		}

		case REVIEW_REVIEW:
			TIMER_STATE(INIT);
			BPAPI_INIT(&ba, NULL, pgsql);

			bind_review(&ba, state);
			if (state->category && !cmd_haskey(cs, "category")) {
				bpapi_set_int(&ba, "category", state->category);
			}
			state->state = REVIEW_CHECK;
			sql_worker_template_query(&config.pg_master,
						  "sql/review.sql", &ba,
						  review_fsm, state, cs->ts->log_string);

			bpapi_output_free(&ba);
			bpapi_simplevars_free(&ba);
			bpapi_vtree_free(&ba);
			return NULL;

		case REVIEW_CHECK:
			TIMER_STATE(CHECK);
			state->state = REVIEW_REMOVE_UPSELLING_PRODUCTS;
			
			/* Redir info */
			if (worker->next_row(worker)) {
				const char *redir_code = worker->get_value_byname(worker, "redir_code");
				const char *remote_addr = worker->get_value_byname(worker, "remote_addr");
				const char *action_id = worker->get_value_byname(worker, "action_id");
				const char *action_type = worker->get_value_byname(worker, "action_type");
				const char *upselling_action_id = worker->get_value_byname(worker, "upselling_action_id");
				const char *remove_upselling_products = worker->get_value_byname(worker, "remove_upselling_products");

				transaction_logprintf(state->cs->ts, D_INFO, "QUEUE upselling_action_id: %s", upselling_action_id);
				transaction_logprintf(state->cs->ts, D_INFO, "QUEUE remove_upselling_products: %s", remove_upselling_products);
				
				state->company_ad = worker->is_true_byname(worker, "company_ad");

				if (remove_upselling_products && !strcmp(remove_upselling_products, "t"))
					state->upselling_action_id = xstrdup(upselling_action_id);

				if (action_type)
			 		state->action_type = xstrdup(action_type);

				syslog_ident("redir", "%s approved_ad %s",
					     redir_code ?: bconf_get_string(host_bconf, "trans.redir_unknown_code"),
					     remote_addr);
				transaction_logprintf(state->cs->ts, D_INFO, "REDIR: %s approved_ad %s",
					     redir_code ?: bconf_get_string(host_bconf, "trans.redir_unknown_code"),
					     remote_addr);
				if (action_id && action_id[0])
					state->action_id = xstrdup(action_id);
			}
			continue;

		case REVIEW_REMOVE_UPSELLING_PRODUCTS:
			TIMER_STATE(REMOVE_UPSELLING_PRODUCTS);
			state->state = REVIEW_MAIL;

			if (state->upselling_action_id) {
				BPAPI_INIT(&ba, NULL, pgsql);
				bpapi_insert(&ba, "ad_id", cmd_getval(cs, "ad_id"));
				bpapi_insert(&ba, "upselling_action_id", state->upselling_action_id);

				sql_worker_template_query("pgsql.queue",
					"sql/cancel_upselling_on_refuse.sql", &ba,
					review_fsm, state, cs->ts->log_string
				);

				bpapi_output_free(&ba);
				bpapi_simplevars_free(&ba);
				bpapi_vtree_free(&ba);
				return NULL;
			}
			continue;

		case REVIEW_MAIL:
		{
			char *link_type;
			TIMER_STATE(MAIL);

			/* Skip refuse mail if ad was spidered. */
			if (strcmp(cmd_getval(cs, "action"), "refuse") == 0) {
				if (state->params &&
				    (link_type = hash_table_search(state->params, "link_type", -1, NULL)) &&
				    strncmp(link_type, "spidered", 8) == 0) {
					state->state = REVIEW_DONE;
					continue;
				}
			}

			if (state->params &&
				(link_type = hash_table_search(state->params, "link_type", -1, NULL)) &&
				!strcmp(link_type, "spidered_balcao")) {
				state->state = REVIEW_DONE;
				continue;
			}

			/* Skip accept email if ad is import*/
			if (strcmp(cmd_getval(cs, "action"), "accept") == 0 || strcmp(cmd_getval(cs, "action"), "accept_with_changes") == 0) {
				if (state->params && hash_table_search(state->params, "link_type", -1, NULL)) {
					state->state = REVIEW_DONE;
					continue;
				}
			}

			if (cmd_haskey(cs, "do_not_send_mail") && atoi(cmd_getval(cs, "do_not_send_mail")) == 1) {
				state->state = REVIEW_DONE;
				continue;
			}

			if (send_mail(cs, state)) {
				transaction_logprintf(cs->ts, D_ERROR, "Failed to send email");
				state->state = REVIEW_ERROR;
				continue;
			}


        	if (strcmp(cmd_getval(cs, "action"), "accept") == 0 || strcmp(cmd_getval(cs, "action"), "accept_with_changes") == 0)
				state->state = REVIEW_GET_CLEARED_BY;
			else
				state->state = REVIEW_DONE;
			continue;
		}

		case REVIEW_GET_CLEARED_BY:
		{
			TIMER_STATE(GET_CLEARED_BY);

			state->state = REVIEW_GET_CLEARED_BY_DATA;

			BPAPI_INIT(&ba, NULL, pgsql);

			bpapi_insert(&ba, "cleared_by", cmd_getval(cs, "ad_id"));

			sql_worker_template_query(&config.pg_master,
					"sql/get_ads_cleared_by.sql", &ba,
					review_fsm, state, cs->ts->log_string);

			bpapi_output_free(&ba);
			bpapi_simplevars_free(&ba);
			bpapi_vtree_free(&ba);

			return NULL;
		}

		case REVIEW_GET_CLEARED_BY_DATA:
		{
			const char *ad_id;
			const char *action_id;
			// accounts pro is enabled and the review is for a NEW ad (Not editions)
			if (bconf_get_int(bconf_root, "*.*.accounts.pro.enabled") && strcmp(cmd_getval(cs, "action_id"), "1") == 0) {
				state->state = REVIEW_CHECK_PRO;
			}
			else {
				state->state = REVIEW_DONE;
			}

			if (!worker || !worker->next_row(worker))
				continue;

			ad_id = worker->get_value(worker, 0);
			action_id = worker->get_value(worker, 1);

			transaction_internal_printf(TRANSACTION_INTERNAL_SYNC_CB, NULL,
				"cmd:clear\n"
				"ad_id:%s\n"
				"action_id:%s\n"
				"commit:1\n",
				ad_id,
				action_id
			);

			continue;
		}
		case REVIEW_CHECK_PRO:
		{
			state->state = REVIEW_DONE;
			transaction_internal_printf(TRANSACTION_INTERNAL_SYNC_CB, NULL,
				"cmd:account_to_pro\n"
				"ad_id:%s\n"
				"pro_type:standard\n"
				"commit:1\n",
				cmd_getval(cs, "ad_id")
			);
			continue;
		}	
		case REVIEW_ERROR:
			TIMER_STATE(ERROR);
			transaction_logprintf(cs->ts, D_ERROR, "Error in review fsm");
			review_done(state);
			return NULL;

		default:
			transaction_logprintf(cs->ts, D_ERROR, "Major state error in review fsm (%d)", state->state);
			review_done(state);
			return NULL;
		}
	}

	if (state->ti) {
		timer_end(state->ti, NULL);
		state->ti = NULL;
	}

	review_done(state);
	return NULL;
}

static void
review(struct cmd *cs) {
	struct review_state *state = zmalloc(sizeof (*state));
	state->cs = cs;
	state->state = REVIEW_INIT;
	review_fsm(state, NULL, NULL);
}

ADD_COMMAND(review, NULL, review, parameters, NULL);
