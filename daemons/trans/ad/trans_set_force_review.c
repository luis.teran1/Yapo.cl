#include "factory.h"
#include "bpapi.h"
#include <bconf.h>

static struct c_param parameters[] = {
	{"ad_id",		P_REQUIRED, "v_id"},
	{"action_id",	P_REQUIRED, "v_id"},
	{NULL, 0}
};

static struct factory_data data = {
        "controlpanel",
        FACTORY_EMPTY_RESULT_OK
};

static const struct factory_action actions[]= {
	FA_SQL("", "pgsql.master", "sql/call_set_force_review_status.sql", 0),
	{0}
};

FACTORY_TRANS(set_force_review, parameters, data, actions);
