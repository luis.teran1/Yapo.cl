#include "factory.h"

static struct c_param parameters[] = {
	{"ad_id",    P_REQUIRED,  "v_id"},
	{NULL, 0}
};

FACTORY_TRANSACTION(get_ad, &config.pg_slave, "sql/get_ad_info_minimal.sql", NULL, parameters, FACTORY_RES_SHORT_FORM);
