#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <time.h>

#include "trans.h"
#include "util.h"
#include <ctemplates.h>
#include "validators.h"
#include <bconf.h>
#include <event.h>

#define LIST_DELETED_DONE  0
#define LIST_DELETED_ERROR 1
#define LIST_DELETED_INIT  2
#define LIST_DELETED_RESULT 3

struct list_deleted_state {
	int state;
	struct cmd *cs;
};

static struct c_param parameters[] = {
	{"time_from", 0, "v_timestamp"},
	{"time_to", 0, "v_timestamp"},
	{"list_id", 0, "v_id"},
	{NULL, 0}
};

static const char *list_deleted_fsm(void *, struct sql_worker *, const char *);


/*****************************************************************************
 * list_deleted_done
 * Exit function.
 **********/
static void
list_deleted_done(void *v) {
	struct list_deleted_state *state = v;
	struct cmd *cs = state->cs;
	free(state);
	cmd_done(cs);
}

/*****************************************************************************
 * bind_insert
 * Initialise template API before a call to sql_worker_template_query().
 **********/
static void
bind_insert(struct bpapi *ba, struct list_deleted_state *state) {
	if (cmd_haskey(state->cs, "time_from"))
		bpapi_insert(ba, "start", cmd_getval(state->cs, "time_from"));
	bpapi_insert(ba, "end", cmd_getval(state->cs, "time_to"));
	if (cmd_haskey(state->cs, "list_id"))
		bpapi_insert(ba, "list_id", cmd_getval(state->cs, "list_id"));
}

/*****************************************************************************
 * list_deleted_fsm
 * State machine
 **********/
static const char *
list_deleted_fsm(void *v, struct sql_worker *worker, const char *errstr) {
	struct list_deleted_state *state = v;
	struct cmd *cs = state->cs;
        struct bpapi ba;

	/* 
	 * General error handling for db errors.
	 */
	if (errstr) {
                transaction_logprintf(cs->ts, D_ERROR, "list_deleted_fsm statement failed (%d, %s)", 
                                      state->state,
                                      errstr);
                cs->status = "TRANS_DATABASE_ERROR";
                cs->message = xstrdup(errstr);
		state->state = LIST_DELETED_ERROR;
        }

	while (1) {
		transaction_logprintf(cs->ts, D_DEBUG, "list_deleted_fsm(%p, %p, %p), state=%d", 
				      v, worker, errstr, state->state);

		switch (state->state) {
		case LIST_DELETED_INIT:
			/* next state */
			state->state = LIST_DELETED_RESULT;

			/* Initialise the templateparse structure */
			BPAPI_INIT(&ba, NULL, pgsql);
			bind_insert(&ba, state);

			/* Register SQL query with our list_deleted_fsm() as callback function */
			/* Make sure to switch to slave if read-only query. */
			sql_worker_template_query(&config.pg_master,
						  "sql/list_deleted.sql", &ba,
						  list_deleted_fsm, state, cs->ts->log_string);

			/* Clean-up */
			bpapi_output_free(&ba);
			bpapi_simplevars_free(&ba);
			bpapi_vtree_free(&ba);

			/* Return to libevent */
			return NULL;

		case LIST_DELETED_RESULT:
                        transaction_logprintf(cs->ts, D_INFO, "Got a result: %d rows",
                                              worker->rows(worker));

			transaction_logoff(cs->ts);

			while (worker->next_row(worker)) {
				transaction_printf(cs->ts, "deleted:%s\n", worker->get_value(worker, 0));
			}
			transaction_logon(cs->ts);

			list_deleted_done(state);
			return NULL;

		case LIST_DELETED_ERROR:
			transaction_logprintf(cs->ts, D_ERROR, "Error state");
			list_deleted_done(state);
			return NULL;

		default:
			/* MAJOR STATE ERROR */
			cs->status = "TRANS_ERROR";
			transaction_logprintf(cs->ts, D_ERROR, "Unknown state (%d)",
					      state->state);
			return NULL;

		}
	}
}

/*****************************************************************************
 * list_deleted
 * Main function that initialise a state-structure and calls the state machine.
 **********/
static void
list_deleted(struct cmd *cs) {
	transaction_logprintf(cs->ts, D_DEBUG, "Start list_deleted transaction");
	struct list_deleted_state *state;
	state = zmalloc(sizeof(*state));
	state->cs = cs;
	state->state = LIST_DELETED_INIT;

	list_deleted_fsm(state, NULL, NULL);
}

ADD_COMMAND(list_deleted,     /* Name of command */
		NULL,
            list_deleted,     /* Main function name */
            parameters,  /* Parameters struct */
            "List deleted command, used for getting a list of deleted ads the last 48 hours");/* Command description */
