#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <time.h>

#include "trans.h"
#include "util.h"
#include <ctemplates.h>
#include "validators.h"
#include <bconf.h>
#include <event.h>

#define AD_STATES_DONE  0
#define AD_STATES_ERROR 1
#define AD_STATES_INIT  2
#define AD_STATES_DATA  3

extern struct bconf_node* bconf_root;

struct ad_states_state {
	int state;
	struct cmd *cs;
};

static struct c_param parameters[] = {
	{"ad_id",       P_REQUIRED, "v_id"},
	{"token",          P_REQUIRED, "v_token:adqueue"},
	{"remote_addr",    P_REQUIRED, "v_ip"},
	{"remote_browser", 0, "v_remote_browser"},
	{NULL, 0}
};

static const char *ad_states_fsm(void *, struct sql_worker *, const char *);

/*****************************************************************************
 * ad_states_done
 * Exit function.
 **********/
static void
ad_states_done(void *v) {
	struct ad_states_state *state = v;
	struct cmd *cs = state->cs;

	free(state);
	cmd_done(cs);
}

/*****************************************************************************
 * bind_insert
 * Initialise template API before a call to sql_worker_template_query().
 **********/
static void
bind_insert(struct bpapi *ba, struct ad_states_state *state) {
	struct c_param *param;

	for (param = parameters; param->name; param++) {
		if (cmd_haskey(state->cs, param->name)) {
			bpapi_insert(ba, param->name, cmd_getval(state->cs, param->name));
		}
	}
}

/*****************************************************************************
 * ad_states_fsm
 * State machine
 **********/
static const char *
ad_states_fsm(void *v, struct sql_worker *worker, const char *errstr) {
	struct ad_states_state *state = v;
	struct cmd *cs = state->cs;
        struct bpapi ba;

	/*
	 * General error handling for db errors.
	 */
	if (errstr) {
                transaction_logprintf(cs->ts, D_ERROR, "ad_states_fsm statement failed (%d, %s)",
                                      state->state,
                                      errstr);
                cs->status = "TRANS_DATABASE_ERROR";
                cs->message = xstrdup(errstr);
		state->state = AD_STATES_ERROR;
        }

	while (1) {
		transaction_logprintf(cs->ts, D_DEBUG, "ad_states_fsm(%p, %p, %p), state=%d",
				      v, worker, errstr, state->state);

		switch (state->state) {
		case AD_STATES_INIT:
			/* next state */
			state->state = AD_STATES_DATA;

			/* Initialise the templateparse structure */
			BPAPI_INIT(&ba, NULL, pgsql);
			bind_insert(&ba, state);

			/* Register SQL query with our ad_states_fsm() as callback function */
			/* Make sure to switch to slave if read-only query. */
			sql_worker_template_query_flags(&config.pg_slave, SQLW_FINAL_CALLBACK,
					"sql/ad_states.sql", &ba,
					ad_states_fsm, state, cs->ts->log_string);

			/* Clean-up */
			bpapi_output_free(&ba);
			bpapi_simplevars_free(&ba);
			bpapi_vtree_free(&ba);

			/* Return to libevent */
			return NULL;

		case AD_STATES_DATA:
			if (worker->sw_state == RESULT_DONE) {
				state->state = AD_STATES_DONE;
				continue;
			}

			while (worker->next_row(worker)) {
				int cols = worker->fields(worker);
				int i;

				for (i = 0; i < cols; i++) {
					transaction_printf(cs->ts, "%s:%s\n", worker->field_name(worker, i), worker->get_value(worker, i));
				}
			}

                        return NULL;

		case AD_STATES_DONE:
			ad_states_done(state);
			return NULL;

		case AD_STATES_ERROR:
			transaction_logprintf(cs->ts, D_ERROR, "Error state");
			ad_states_done(state);
			return NULL;

		default:
			/* MAJOR STATE ERROR */
			cs->status = "TRANS_ERROR";
			transaction_logprintf(cs->ts, D_ERROR, "Unknown state (%d)",
					      state->state);
			ad_states_done(state);
			return NULL;

		}
	}
}

/*****************************************************************************
 * ad_states
 * Main function that initialise a state-structure and calls the state machine.
 **********/
static void
ad_states(struct cmd *cs) {
	struct ad_states_state *state;
	state = zmalloc(sizeof(*state));
	state->cs = cs;
	state->state = AD_STATES_INIT;

	ad_states_fsm(state, NULL, NULL);
}

ADD_COMMAND(ad_states,     /* Name of command */
		NULL,
            ad_states,     /* Main function name */
            parameters,  /* Parameters struct */
            "Return all states for an ad");/* Command description */

