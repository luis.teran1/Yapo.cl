#include "factory.h"
#include "bpapi.h"
#include <bconf.h>

static struct c_param parameters[] = {
	{"email;uid", 	P_XOR, 	"v_email_basic;v_uid"},
	{NULL, 0}
};

static struct factory_data data = {
	"accounts",
	FACTORY_NO_RES_CNTR | FACTORY_EMPTY_RESULT_OK | FACTORY_RES_SHORT_FORM
};

static const struct factory_action actions[] = {
	FA_SQL("", "pgsql.slave", "sql/get_accounts_params.sql", FA_OUT),
	FA_DONE()
};

FACTORY_TRANS(get_accounts_params, parameters, data, actions);

