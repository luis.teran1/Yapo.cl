#include "factory.h"

static struct c_param parameters[] = {
	{"partner_name",        P_REQUIRED, "v_string_all"},
	{"partner_pretty_name", P_OPTIONAL, "v_string_all"},
	{"email",               P_REQUIRED, "v_multiple_emails"},
	{"init_date",           P_REQUIRED, "v_date_time"},
	{"end_date",            P_REQUIRED, "v_date_time"},
	{NULL, 0}
};

static const struct factory_action actions[]= {
	FA_SQL(NULL, "pgsql.slave", "sql/get_active_partner_ads.sql", 0),
	FA_MAIL("mail/partner_ads_report.mime"),
	{0}
};

static struct factory_data data = {
	"partner",
	FACTORY_NO_RES_CNTR | FACTORY_RES_SHORT_FORM | FACTORY_EMPTY_RESULT_OK | FACTORY_SEND_MAIL_ALWAYS
};

FACTORY_TRANS(send_partner_ads_report, parameters, data, actions);
