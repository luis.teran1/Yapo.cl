#include "factory.h"

static struct c_param parameters[] = {
	{"email", 	P_REQUIRED, 	"v_email_basic"},
	{NULL, 0}
};

static struct factory_data data = {
        "get_ads_count",
        FACTORY_NO_RES_CNTR | FACTORY_RES_SHORT_FORM
};

static const struct factory_action actions[]= {
	FA_SQL("", "pgsql.slave", "sql/call_get_ads_count.sql", FASQL_OUT),
	{0}
};

FACTORY_TRANS(get_ads_count, parameters, data, actions);
