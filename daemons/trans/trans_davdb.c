#include "trans.h"
#include "command.h"
#include "util.h"
#include "logging.h"
#include "http_req.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>

#include <bconf.h>
#include "validators.h"
#include "bpapi.h"
#include "ctemplates.h"

#include "dav.h"

extern int flag_nodav;

static void
ds_done(struct davdb *ds) {
	/* Called by the davdb commands, report back filename. */
	if (ds->destfilename)
		transaction_printf(ds->cs->ts, "file:%s\n", ds->destfilename);
	else
		transaction_printf(ds->cs->ts, "file:%s\n", ds->filename);

	if (cmd_getval(ds->cs, "image") != ds->image) {
		free((char *) ds->image);
		ds->image = NULL;
	}

	cmd_done(ds->cs);
	if (ds->new_filename)
		free(*ds->new_filename);
	free(ds);
	ds=NULL;
	return;
}

static void
req_done_cb(struct request *httpreq) {
	struct davdb *ds = httpreq->data;

	if (ds->cs)
		transaction_logprintf(ds->cs->ts, D_INFO, "req_done (%s %s)", httpreq->server, httpreq->port);

	/* If communication-problem, check next host */
	if (httpreq->result > 1000 && ++httpreq->current_server < httpreq->servers) {
		log_printf(LOG_INFO, "Connecting to alternate dav host (%u)", httpreq->current_server);
		httpreq->server = bconf_value(bconf_get(bconf_byindex(bconf_get(host_bconf, "*.common.dav.host"), httpreq->current_server), "name"));
		httpreq->port = bconf_value(bconf_get(bconf_byindex(bconf_get(host_bconf, "*.common.dav.host"), httpreq->current_server), "port"));
		httpreq->timeout = 10;
		httpreq->cb_done = req_done_cb;

		httpreq->header_sent = 0;
		httpreq->result = 0;
		httpreq->event_base = ds->cs->base;
		http_req(httpreq);
		return;
	}

	free(httpreq->header);

	--ds->pending;

	if (ds->cb) {
		/*
		 * Since we're doing multiple requests in one, overwrite 200-results since errors are more interesting,
		 * but ignore 1000-results since they mean we retried.
		 */
		if (ds->result == 0 || ds->result/100 == 2 || ds->result/10 >= 10)
			ds->result = httpreq->result;
	       	if (ds->pending == 0) {
			if (ds->cs)
				transaction_logprintf(ds->cs->ts, D_DEBUG, "0 pending in davdb, calling callback (%d)", httpreq->result);
			ds->cb(ds);
		}
		free(httpreq);
		return;
	}


	if (httpreq->result/100 != 2) { /* HTTP 200 status codes indicate success */
		ds->cs->status = "TRANS_ERROR";
		ds->cs->error = "ERROR_COMMUNICATION";
		if (!ds->cs->message)
			xasprintf(&ds->cs->message, "%s:%s %s", httpreq->server, httpreq->port, (http_req_result(httpreq->result)));
	}

	/* ds_done */
	if (ds->pending == 0)
		ds_done(ds);
	free(httpreq);
}

static void
async_timeout_cb(int i, short s, void *v) {
        struct davdb *ds = v;

        evtimer_del(&ds->timeout_event);
	ds->errstr = NULL;
	ds->result = 200;
	ds->cb(ds); /* Always OK */
}

static void
davdb_async_timeout(struct davdb *ds) {
        struct timeval tv;

	if (!ds->cb) {
		ds_done(ds);
		return;
	}

        tv.tv_sec = 1;
        tv.tv_usec = 0;
	if (!evtimer_initialized(&ds->timeout_event)) {
        	evtimer_set(&ds->timeout_event, async_timeout_cb, ds);
		event_base_set(ds->cs->base, &ds->timeout_event);
        	evtimer_add(&ds->timeout_event, &tv);
	}
}

static int
davdb_send(struct davdb *ds, char* filename, char *destfilename, const char *buffer, size_t size) {
       char *httpheader = zmalloc(1024);
       int len = 1024;
       int pos = 0;
       struct request *davreq = zmalloc(sizeof(*davreq));
       int send_status = 0;

       davreq->data = ds;
       bufcat(&httpheader, &len, &pos, "%s %s HTTP/1.0\r\n", ds->command, filename);

       if (destfilename) {
               bufcat(&httpheader, &len, &pos, "Destination: http://localhost/%s\r\n", destfilename);
               bufcat(&httpheader, &len, &pos, "Overwrite: F\r\n");
       }

       if (buffer && size)
               bufcat(&httpheader, &len, &pos, "Content-Length: %ld\r\n", (unsigned long)size);

       bufcat(&httpheader, &len, &pos, "\r\n");


       davreq->header = httpheader;
       davreq->hsize = strlen(httpheader);
       davreq->requestdata = buffer;
       davreq->reqsize = size;

       davreq->servers = bconf_count(bconf_get(host_bconf, "*.common.dav.host"));
       davreq->cb_done = req_done_cb;
       davreq->event_base = ds->cs->base;

       if (davreq->servers) {
               davreq->current_server = 0;

               struct bconf_node *davnode = bconf_get(host_bconf, "*.common.dav.host");
               davreq->server = bconf_value(bconf_get(bconf_byindex(davnode, 0), "name"));
               davreq->port = bconf_value(bconf_get(bconf_byindex(davnode, 0), "port"));
               davreq->timeout = 10;
       }


       ds->pending++;
       http_req(davreq);

       if(ds->cs->error != NULL) {
               send_status = -1;
       }

       return send_status;
}

void
davdb_execute(struct davdb *ds) {
	char *image_path = NULL;
	char *thumb_path = NULL;

	char *dest_image_path = NULL;
	char *dest_thumb_path = NULL;

	const char *bconf_imagedir = "*.common.imagedir";
	const char *bconf_thumbnaildir = "*.common.thumbnaildir";

	if (ds->application) {
		if (strcmp(ds->application, "bid") == 0) {
			bconf_imagedir = "*.common.bid_imagedir";
			bconf_thumbnaildir = "*.common.bid_thumbnaildir";
		} else if (strcmp(ds->application, "stores") == 0) {
			bconf_imagedir = "*.common.stores_imagedir";
		} else if (strcmp(ds->application, "ppages") == 0) {
			bconf_imagedir = "*.common.ppages_imagedir";
		}
	}

	if (ds->directory) {
		xasprintf(&image_path, "/%s/%s", ds->directory, ds->filename);
	} else {
		xasprintf(&image_path, "/%s/%.2s/%s", bconf_value(bconf_get(host_bconf, bconf_imagedir)),
			  ds->filename, ds->filename);
		if (ds->thumb) {
			xasprintf(&thumb_path, "/%s/%.2s/%s",
				bconf_value(bconf_get(host_bconf, bconf_thumbnaildir)),ds->filename, ds->filename);
		}
	}

	if (ds->destfilename) {
		if (ds->directory) {
			xasprintf(&dest_image_path, "/%s/%s", ds->directory, ds->destfilename);
		} else {
			xasprintf(&dest_image_path, "/%s/%.2s/%s", bconf_value(bconf_get(host_bconf, bconf_imagedir)), ds->destfilename, ds->destfilename);

			if (ds->thumb) {
				xasprintf(&dest_thumb_path, "/%s/%.2s/%s",
					bconf_value(bconf_get(host_bconf, bconf_thumbnaildir)),
					ds->destfilename, ds->destfilename);
			}
		}
	}

	if (!flag_nodav) {
		if (!davdb_send(ds, image_path, dest_image_path, ds->image, ds->image_size) == 0 && thumb_path) {
			davdb_send(ds, thumb_path, dest_thumb_path, ds->thumb, ds->thumb_size);
		}
	} else {
		if (ds->cs) {
			transaction_printf(ds->cs->ts, "nodav:%s %s %s\n", ds->command, image_path,
					   dest_image_path ? dest_image_path : "(null)");
		}
	}

	if (image_path)
		free(image_path);
	if (thumb_path)
		free(thumb_path);
	if (dest_image_path)
		free(dest_image_path);
	if (dest_thumb_path)
		free(dest_thumb_path);

	if (flag_nodav) {
		davdb_async_timeout(ds);
	}
}

static int
v_date_format(struct cmd_param *param, const char *arg) {
	struct tm tm;
	if (strptime(param->value, "%Y%m%d", &tm) == NULL) {
		param->error = "ERROR_BAD_DATE";
		return 1;
	}
	return 0;
}

static struct validator v_date_davdb[] = {
	VALIDATOR_FUNC(v_date_format),
	{0}
};
ADD_VALIDATOR(v_date_davdb);

static struct validator v_image[] = {
	{0}
};
ADD_VALIDATOR(v_image);

static struct validator v_thumbnail[] = {
	{0}
};
ADD_VALIDATOR(v_thumbnail);

static struct validator v_image_filename[] = {
	VALIDATOR_REGEX("^[0-9]{10}\\.jpg$", REG_EXTENDED, "ERROR_BAD_FILENAME"),
	{0}
};
ADD_VALIDATOR(v_image_filename);

static struct validator v_filename[] = {
	VALIDATOR_LEN(-1, 100, "ERROR_FILENAME_TOO_LONG"),
	{0}
};
ADD_VALIDATOR(v_filename);


static struct validator v_directory[] = {
	VALIDATOR_LEN(-1, 50, "ERROR_DIRECTORY_TOO_LONG"),
	VALIDATOR_REGEX("^[0-9a-zA-Z/]*$", REG_EXTENDED, "ERROR_BAD_DIRECTORY"),
	{0}
};
ADD_VALIDATOR(v_directory);


static struct c_param put_param[] = {
	{"image",	P_REQUIRED,	"v_image"},
	{"thumbnail",	P_OPTIONAL,	"v_thumbnail"},
	{"date",	0,	"v_date_davdb"},
	{"bid",	        0,      "v_bool"},
	{"width",	0,      "v_integer"},
	{"height",	0,      "v_integer"},
	{"length",	0,      "v_integer"},
	{NULL, 0}
};

static struct c_param put_store_param[] = {
	{"image",	P_REQUIRED,	"v_image"},
	{"store_id",	P_OPTIONAL,	"v_store_id"},
	{"image_type", P_REQUIRED, "v_store_image_type"},
	{NULL, 0}
};

static struct c_param put_ppage_image[] = {
	{"image",	P_REQUIRED,	"v_image"},
	{NULL, 0}
};

static struct c_param put_single_param[] = {
	{"image",	P_REQUIRED,	"v_image"},
	{"date",	0,	"v_date_davdb"},
	{"directory",	0,	"v_directory"},
	{"filename",	P_REQUIRED,	"v_filename"},
	{"width",	0,      "v_integer"},
	{"height",	0,      "v_integer"},
	{"length",	0,      "v_integer"},
	{NULL, 0}
};


static struct c_param del_param[] = {
	{"filename",	0,	"v_filename"},
	{"image_id",	0,	"v_integer"},
	{"directory",	0,	"v_directory"},
	{NULL, 0}
};

static struct c_param cpmv_param[] = {
	{"filename",	0,	"v_filename"},
	{"date",	0,	"v_date_davdb"},
	{"width",	0,      "v_integer"},
	{"height",	0,      "v_integer"},
	{"length",	0,      "v_integer"},
	{NULL, 0}
};

static const char*
imgcmd_do(void *v, struct sql_worker *worker, const char *errstr) {
	struct davdb *ds = (struct davdb*)v;
	struct bpapi ba;
	char *sql_imageid;

	if (errstr != NULL) {
		if (!ds->cb) {
			ds->cs->status = "TRANS_ERROR";
			ds->cs->message = xstrdup(errstr);
			cmd_done(ds->cs);
			free(ds);
		} else {
			ds->errstr = xstrdup(errstr);
			ds->cb(ds);
		}
		return NULL;
	}

	if (worker == NULL) {
		BPAPI_INIT(&ba, NULL, pgsql);

		bpapi_insert(&ba, "media", "image");
		xasprintf(ds->new_filename, "%010llu.jpg", ds->image_id);

		xasprintf(&sql_imageid, "%010llu", ds->image_id);
		bpapi_insert(&ba, "image_id", sql_imageid);
		free(sql_imageid);
		sql_imageid = NULL;

		if (cmd_haskey(ds->cs, "image_type"))
			bpapi_insert(&ba, "image_type", cmd_getval(ds->cs, "image_type"));
		if (cmd_haskey(ds->cs, "store_id"))
			bpapi_insert(&ba, "store_id", cmd_getval(ds->cs, "store_id"));

		if (cmd_haskey(ds->cs, "width"))
			bpapi_insert(&ba, "width", cmd_getval(ds->cs, "width"));
		if (cmd_haskey(ds->cs, "height"))
			bpapi_insert(&ba, "height", cmd_getval(ds->cs, "height"));
		if (cmd_haskey(ds->cs, "length"))
			bpapi_insert(&ba, "length", cmd_getval(ds->cs, "length"));

		if (cmd_haskey(ds->cs, "bid") && strcmp(cmd_getval(ds->cs, "bid"), "1") == 0)
			sql_worker_template_query(&config.pg_master, "sql/insert_dav_bid_media.sql", &ba,
						  imgcmd_do, ds, (ds->cs ? ds->cs->ts->log_string : "DAV"));
		/** Only stores have image_type */
		else if (cmd_haskey(ds->cs, "image_type")) {
			sql_worker_template_query(&config.pg_master, "sql/insert_store_media.sql", &ba,
						  imgcmd_do, ds, (ds->cs ? ds->cs->ts->log_string : "DAV"));
		}
		else
			sql_worker_template_query(&config.pg_master, "sql/insert_dav_image.sql", &ba,
						  imgcmd_do, ds, (ds->cs ? ds->cs->ts->log_string : "DAV"));

		bpapi_output_free(&ba);
		bpapi_simplevars_free(&ba);
		bpapi_vtree_free(&ba);
	} else {
		transaction_logprintf(ds->cs->ts, D_DEBUG, "filename: %s", *ds->new_filename);
		davdb_execute(ds);
		if (ds->cs->error != NULL) {
			cmd_done(ds->cs);
			if(ds)
				free(ds);
			return NULL;
		}
	}
	return NULL;
}

const char *
imagecmd(void *v, struct sql_worker *worker, const char *errstr) {
	struct davdb *ds = (struct davdb*)v;
	struct cmd *cs = ds->cs;
    struct bpapi ba;
    time_t t;
    struct tm tm = {0};
    int image_dir;
    char *sql_image_dir;
	const char *template = "sql/call_get_next_unused_media_id.sql";

	if (cmd_haskey(cs, "image_type")) {
		template =  "sql/imagecmd_next_store_image_id.sql";
	}

	if (errstr != NULL) {
		if (!ds->cb) {
			cs->status = "TRANS_ERROR";
			cs->message = xstrdup(errstr);
			cmd_done(cs);
			free(ds);
		} else {
			ds->errstr = xstrdup(errstr);
			ds->cb(ds);
		}
		return NULL;
	}

	if (worker == NULL) {
		BPAPI_INIT(&ba, NULL, pgsql);

        if (ds->date) {
            strptime(ds->date, "%Y%m%d", &tm); /* date passed validation so should be ok */
            if ((t = mktime(&tm)) == (time_t) -1)
                DPRINTF(D_INFO,("error mktime"));
        } else {
            time(&t);
        }
        image_dir = (t / 86400) % 100;

        xasprintf(&sql_image_dir, "%d", image_dir);
        bpapi_insert(&ba, "image_dir", sql_image_dir);
        free(sql_image_dir);

        /* Make a callback to ourself with updated next_image_id */
        sql_worker_template_query(&config.pg_master, template, &ba,
                 imagecmd, ds, (ds->cs ? ds->cs->ts->log_string : "DAV"));

        bpapi_output_free(&ba);
        bpapi_simplevars_free(&ba);
        bpapi_vtree_free(&ba);

		return NULL;
	} else {
		/* Unique image id from db */
		if (worker->next_row(worker)) {
			const char *id = worker->get_value(worker, 0);
			if (id) {
				ds->image_id = atoll(id);
			}
		}
	}

	imgcmd_do(ds, NULL, NULL);

	return NULL;
}

static void
imgput_single(struct cmd *cs) {
	struct davdb *ds = zmalloc(sizeof(*ds));

	ds->cs = cs;
	ds->command = "PUT";
	ds->new_filename = (char**)&(ds->filename);
	ds->image = cmd_getval(ds->cs, "image");
	ds->image_size = cmd_getsize(ds->cs, "image");
	ds->date = cmd_getval(cs, "date");
	ds->directory = cmd_getval(cs, "directory");
	ds->filename = xstrdup(cmd_getval(cs, "filename"));

	if (cmd_haskey(cs, "bid") && !strcmp(cmd_getval(cs, "bid"), "1"))
		ds->application = "bid";

	davdb_execute(ds);
	if (ds->cs->error != NULL) {
		cmd_done(ds->cs);
		free(ds);
		return;
	}
}

static void
imgput_store(struct cmd *cs) {
	struct davdb *ds = zmalloc(sizeof(*ds));

	ds->cs = cs;
	ds->command = "PUT";
	ds->new_filename = (char**)&(ds->filename);
	ds->image = cmd_getval(ds->cs, "image");
	ds->image_size = cmd_getsize(ds->cs, "image");
	ds->date = NULL;
	ds->application = "stores";
	imagecmd(ds, NULL, NULL);
}

static void
imgput_ppage(struct cmd *cs) {
	struct davdb *ds = zmalloc(sizeof(*ds));

	ds->cs = cs;
	ds->command = "PUT";
	ds->new_filename = (char**)&(ds->filename);
	ds->image = cmd_getval(ds->cs, "image");
	ds->image_size = cmd_getsize(ds->cs, "image");
	ds->date = NULL;
	ds->application = "ppages";
	imagecmd(ds, NULL, NULL);
}

static void
imgput(struct cmd *cs) {
	struct davdb *ds = zmalloc(sizeof(*ds));

	ds->cs = cs;
	ds->command = "PUT";
	ds->new_filename = (char**)&(ds->filename);
	ds->image = cmd_getval(ds->cs, "image");
	ds->image_size = cmd_getsize(ds->cs, "image");

	if ( cmd_haskey(cs, "thumbnail") ) {
		ds->thumb = cmd_getval(ds->cs, "thumbnail");
		ds->thumb_size = cmd_getsize(ds->cs, "thumbnail");
	}

	ds->date = cmd_getval(cs, "date");

	if (cmd_haskey(cs, "bid") && !strcmp(cmd_getval(cs, "bid"), "1"))
		ds->application = "bid";

	imagecmd(ds, NULL, NULL);
}

static const char *
db_image_delete(void *v, struct sql_worker *worker, const char *errstr) {
	struct davdb *ds = v;

	if (errstr != NULL) {
		ds->cs->status = "TRANS_ERROR";
		ds->cs->message = xstrdup(errstr);
		cmd_done(ds->cs);
		free(ds);
		return NULL;
	}

	if (!cmd_haskey(ds->cs, "image_id")) {
		davdb_execute(ds);
		if (ds->cs->error != NULL) {
			cmd_done(ds->cs);
			free(ds);
			return NULL;
		}
	} else {
		if (worker == NULL) {
			struct bpapi ba;

			BPAPI_INIT(&ba, NULL, pgsql);
			bpapi_insert(&ba, "image_id", cmd_getval(ds->cs, "image_id"));
			if(ds->application && strcmp(ds->application, "stores") == 0) {
				sql_worker_template_query(&config.pg_master, "sql/delete_dav_store_image.sql", &ba, db_image_delete, ds, (ds->cs ? ds->cs->ts->log_string : "DAV"));
			}
			else {
				sql_worker_template_query(&config.pg_master, "sql/delete_dav_image.sql", &ba, db_image_delete, ds, (ds->cs ? ds->cs->ts->log_string : "DAV"));
			}
			bpapi_output_free(&ba);
			bpapi_simplevars_free(&ba);
			bpapi_vtree_free(&ba);
		} else if (worker->affected_rows(worker)) {
				davdb_execute(ds);
		} else {
			ds->cs->status = "TRANS_ERROR";
			ds->cs->error = "ERROR_IMAGE_DELETE_DATABASE";
			cmd_done(ds->cs);
			free(ds);
			return NULL;
		}
	}

	return NULL;
}

static void
imgdel(struct cmd *cs) {
	struct davdb *ds = zmalloc(sizeof(*ds));
	ds->cs = cs;

	ds->command = "DELETE";
	ds->filename = cmd_getval(cs, "filename");
	if (cmd_haskey(cs, "directory")) {
		ds->directory = cmd_getval(cs, "directory");
	}

	if (cmd_haskey(cs, "bid") && !strcmp(cmd_getval(cs, "bid"), "1"))
		ds->application = "bid";

	db_image_delete(ds, NULL, NULL);
}

static void
imgdel_store(struct cmd *cs) {
	struct davdb *ds = zmalloc(sizeof(*ds));
	ds->cs = cs;

	ds->command = "DELETE";
	ds->filename = cmd_getval(cs, "filename");
	if (cmd_haskey(cs, "directory")) {
		ds->directory = cmd_getval(cs, "directory");
	}

	ds->application = "stores";

	db_image_delete(ds, NULL, NULL);
}

static void
imgmove(struct cmd *cs) {
	struct davdb *ds = zmalloc(sizeof(*ds));
	ds->cs = cs;

	ds->command = "MOVE";
	ds->filename = cmd_getval(cs, "filename");
	ds->new_filename = &ds->destfilename;
	ds->date = cmd_getval(cs, "date");

	if (cmd_haskey(cs, "bid") && !strcmp(cmd_getval(cs, "bid"), "1"))
		ds->application = "bid";

	imagecmd(ds, NULL, NULL);
}

static void
imgcopy(struct cmd *cs) {
	struct davdb *ds = zmalloc(sizeof(*ds));
	ds->cs = cs;

	ds->command = "COPY";
	ds->filename = cmd_getval(cs, "filename");
	ds->new_filename = &ds->destfilename;
	ds->date = cmd_getval(cs, "date");

	if (cmd_haskey(cs, "bid") && !strcmp(cmd_getval(cs, "bid"), "1"))
		ds->application = "bid";

	imagecmd(ds, NULL, NULL);
}
ADD_COMMAND(imgput_single, NULL, imgput_single, put_single_param, NULL);
ADD_COMMAND(imgput, NULL, imgput, put_param, NULL);
ADD_COMMAND(imgdel, NULL, imgdel, del_param, NULL);
ADD_COMMAND(imgdel_store, NULL, imgdel_store, del_param, NULL);
ADD_COMMAND(imgmove, NULL, imgmove, cpmv_param, NULL);
ADD_COMMAND(imgcopy, NULL, imgcopy, cpmv_param, NULL);
ADD_COMMAND(imgput_store, NULL, imgput_store, put_store_param, NULL);
ADD_COMMAND(imgput_ppage, NULL, imgput_ppage, put_ppage_image, NULL);
