#include "factory.h"
#include "utils/create_session_hash.h"

extern struct bconf_node *bconf_root;

static struct c_param parameters[] = {
	{"date",	P_REQUIRED,	"v_date"		},
	{"pack_type",	P_REQUIRED,	"v_stats_pack_type"	},
	{NULL, 0}
};

static struct factory_data data = {
    NULL,
    FACTORY_NO_RES_CNTR | FACTORY_EMPTY_RESULT_OK
};


static const struct factory_action actions[] = {
	FA_SQL(NULL, "pgsql.slave", "sql/get_pack_users_by_category.sql", 0),
	FA_OUT("trans/call_build_pack_stats_by_user.txt"),
	FA_DONE()
};

FACTORY_TRANS(get_pack_daily_stats, parameters, data, actions);
