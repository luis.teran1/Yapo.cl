#include "factory.h"
#include "bpapi.h"
#include <bconf.h>

extern struct bconf_node *bconf_root;

static struct c_param parameters[] = {
    {"user_email",    P_REQUIRED, "v_email_basic"},
    {"send_to",       P_REQUIRED, "v_email_basic"},
    {"ad_subject",    P_REQUIRED, "v_subject"},
    {"payment_date",  P_REQUIRED, "v_timestamp"},
    {"product",       P_REQUIRED, "v_integer"},
    {"price;us_price",P_XOR,      "v_integer;v_string_isprint"},
    {"slow_process",  P_OPTIONAL, "v_bool"},
    {NULL, 0}
};

static struct factory_data data = {
    "mail"
};

static const struct factory_action actions[] = {
    FA_MAIL("mail/send_autofact_confirm.mime"),
    {0}
};

FACTORY_TRANS(send_autofact_confirm, parameters, data, actions);
