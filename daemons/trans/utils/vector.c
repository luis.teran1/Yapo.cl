#include <stdlib.h>
#include <string.h>

#include "vector.h"
#include "memalloc_functions.h"

typedef void (*freefn)(void *e);

struct vector_t {
	int capacity;
	int size;
	void **data;
};

vector_t* vector_new(int n) {
	vector_t *v = xmalloc(sizeof(vector_t));
	v->data = xcalloc(n, sizeof(void *));
	v->capacity = n;
	v->size = 0;
	return v;
}

void vector_delete(vector_t *v, freefn ff) {
	if (v) {
		int i;
		if (ff)
			for (i = 0; i < v->size; ++i)
				ff(v->data[i]);
		free(v->data);
		free(v);
	}
}

static int vector_grow(vector_t *v) {
	int newcap = v->capacity * 2 + 2;
	void **newdata = xrealloc(v->data, sizeof(void *) * newcap);
	memset(newdata + v->capacity, 0, sizeof(void *) * (newcap - v->capacity));
	v->data = newdata;
	v->capacity = newcap;
	return newcap;
}

int vector_append(vector_t *v, void *e) {
	if (v->size == v->capacity)
		if (vector_grow(v) < 0)
			return -1;
	int p = v->size++;
	v->data[p] = e;
	return p;
}

int vector_size(vector_t *v) {
	return v->size;
}

void** vector_data(vector_t *v) {
	return v->data;
}

void vector_foreach(vector_t *v, iterfn f, void *data) {
	int i;
	for (i = 0 ; i < v->size ; ++i)
		f(i, v->data[i], data);
}

int vector_all(vector_t *v, iterfn f, void *data) {
	int i;
	for (i = 0 ; i < v->size ; ++i)
		if (!f(i, v->data[i], data))
			return 0;
	return 1;
}

int vector_any(vector_t *v, iterfn f, void *data) {
	int i;
	for (i = 0 ; i < v->size ; ++i)
		if (f(i, v->data[i], data))
			return 1;
	return 0;
}
