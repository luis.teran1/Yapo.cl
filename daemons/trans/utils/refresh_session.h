#ifndef YAPO_REFRESH_SESSION_H
#define YAPO_REFRESH_SESSION_H

#include "bpapi.h"
#include "command.h"

// call the 'refresh_account_cache' trans with the specified email
void call_refresh_account(const char * email);

// Gets the email(s) from the 'email' key in the command, the 'email' key from the bapi, 
// or the 'o_email' from the bapi and call the 'refresh_account_cache' trans internally.
// If there are more than one, comma separated, email, cmd:cmd:refresh_account_cache 
// will be called once for each email. If there is no email, logs a warning message.
int refresh_account(struct cmd *cs, struct bpapi *ba);

#endif
