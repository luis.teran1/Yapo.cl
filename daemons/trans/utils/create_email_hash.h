#ifndef YAPO_CREATE_E_HASH_H
#define YAPO_CREATE_E_HASH_H

#include "bpapi.h"
#include "command.h"

// maps account_id to email redis key
char *create_account_email_hash(const char *account_id);


// maps user_id to email redis key
char *create_user_email_hash(const char *user_id);

#endif
