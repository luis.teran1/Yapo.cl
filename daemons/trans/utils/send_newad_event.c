#include "factory.h"
#include "logging.h"
#include "log_event.h"
#include "bconf.h"
#include "utils/vector.h"
#include "vtree.h"
#include "json_vtree.h"
#include "fd_pool.h"
#include "sredisapi.h"
#include "utils/create_session_hash.h"

#include "send_newad_event.h"

extern struct bconf_node *bconf_root;

static void
hgetall_cb(const void *key, size_t klen, const void *value, size_t vlen, void *barg) {
	struct bpapi *ba = barg;
	if (!bpapi_has_key(ba, (char *) key)){
		bpapi_insert(ba, (char *) key, (char *) value);
	}
}

static void
get_account_params(struct cmd *cs, struct bpapi *ba) {
	const char *email = cmd_getval(cs, "email");
	char *rkey = create_profile_cache_hash(email);

	struct fd_pool_conn *conn = redis_sock_conn(redis_session_pool.pool, "master");
	redis_sock_hgetall(conn, rkey, hgetall_cb, ba);
	fd_pool_free_conn(conn);
	free(rkey);
}

static void
get_ad_extra_info(struct cmd *cs, struct bpapi *ba) {
	struct sql_worker *worker = NULL;
	const char *errstr = NULL;
	worker = sql_blocked_template_query("pgsql.master", 0, "sql/call_get_newad_event_extra_info.sql", ba, cs->ts->log_string, &errstr);
	if (worker && sql_blocked_next_result(worker, &errstr) == 1) {
		if (worker->rows(worker) > 0) {
			sql_worker_bpa_insert(worker, ba, "");
		}
	}
	if (worker)
		sql_worker_put(worker);
}

static void
get_ad_edit_extra_info(struct cmd *cs, struct bpapi *ba) {
	struct sql_worker *worker = NULL;
	const char *errstr = NULL;
	worker = sql_blocked_template_query("pgsql.master", 0, "sql/get_edit_event_extra_info.sql", ba, cs->ts->log_string, &errstr);
	if (worker && sql_blocked_next_result(worker, &errstr) == 1) {
		if (worker->rows(worker) > 0) {
			sql_worker_bpa_insert(worker, ba, "");
		}
	}
	if (worker)
		sql_worker_put(worker);
}

static void
get_ad_with_changes_extra_info(struct cmd *cs, struct bpapi *ba) {
	struct sql_worker *worker = NULL;
	const char *errstr = NULL;
	worker = sql_blocked_template_query("pgsql.master", 0, "sql/get_newad_with_changes_event_extra_info.sql", ba, cs->ts->log_string, &errstr);
	if (worker && sql_blocked_next_result(worker, &errstr) == 1) {
		if (worker->rows(worker) > 0) {
			sql_worker_bpa_insert(worker, ba, "");
		}
	}
	if (worker)
		sql_worker_put(worker);

	worker = sql_blocked_template_query("pgsql.master", 0, "sql/get_newad_with_changes_event_ad_params.sql", ba, cs->ts->log_string, &errstr);
	if (worker && sql_blocked_next_result(worker, &errstr) == 1) {
		if (worker->rows(worker) > 0) {
			sql_worker_bpa_insert(worker, ba, "");
		}
	}
	if (worker)
		sql_worker_put(worker);

	worker = sql_blocked_template_query("pgsql.master", 0, "sql/get_newad_with_changes_event_ad_images.sql", ba, cs->ts->log_string, &errstr);
	if (worker && sql_blocked_next_result(worker, &errstr) == 1) {
		if (worker->rows(worker) > 0) {
			sql_worker_bpa_insert(worker, ba, "");
		}
	}
	if (worker)
		sql_worker_put(worker);
}

static void
model_cb(const void *key, size_t klen, const void *value, size_t vlen, void *cbarg) {
	struct bpapi_vtree_chain *vtree = cbarg;
	json_vtree(vtree, "x", (char *) value, vlen, 1);
}

static void
parse_cardata(struct cmd *cs, struct bpapi *ba, int brand, int model, int version) {
	transaction_logprintf(cs->ts, D_DEBUG, "NEWAD_EVENT: Inserting cardata values with values: %d %d %d", brand, model, version);
	char *brand_field = NULL;
	char *model_key = NULL;
	char *model_field = NULL;
	char *version_key = NULL;
	char *version_field = NULL;
	struct bpapi_vtree_chain vtree = {0};

	xasprintf(&brand_field, "%d", brand);
	xasprintf(&model_key, "rcd1xapp_id_cardata_models_%d", brand);
	xasprintf(&model_field, "%d", model);
	xasprintf(&version_key, "rcd1xapp_id_cardata_versions_%d_%d", brand, model);
	xasprintf(&version_field, "%d", version);

	transaction_logprintf(cs->ts, D_DEBUG, "NEW_AD_EVENT: getting from cardata");
	struct fd_pool_conn *conn = redis_sock_conn(redis_cardata_pool.pool, "master");
	char *brand_value = redis_sock_hget(conn, "rcd1xapp_id_cardata_brands", brand_field);
	char *model_value = redis_sock_hget(conn, model_key, model_field);
	int count = redis_sock_hgetall(conn, version_key, model_cb, &vtree);
	fd_pool_free_conn(conn);

	if (model_value)
		bpapi_insert(ba, "model_name", model_value);
	if (brand_value)
		bpapi_insert(ba, "brand_name", brand_value);
	if (count > 0) {
		const char *version_value = vtree_get(&vtree, "x", "name", NULL);
		if (version_value)
			bpapi_insert(ba, "version_name", version_value);
	}

	free(model_value);
	free(brand_value);
	vtree_free(&vtree);
	free(brand_field);
	free(model_key);
	free(model_field);
	free(version_key);
	free(version_field);
}

static int
send_newad_message(struct cmd *cs, char *message){
       const char *exchange = bconf_get_string(bconf_root, "*.*.common.newad_event.rabbitmq.exchange");
       const char *routing_key = bconf_get_string(bconf_root, "*.*.common.newad_event.rabbitmq.routing_key");
       const char *virtual_host = bconf_get_string(bconf_root, "*.*.common.newad_event.rabbitmq.vhost");

       transaction_logprintf(
               cs->ts,
               D_DEBUG,
               "NEW_AD_EVENT: Loading RabbitMQ seetings exchange=%s, routing key=%s, vhost=%s",
               exchange,
               routing_key,
               virtual_host);

       char *res = transaction_internal_output_printf(
               "cmd:rabbitmq_send\n"
               "exchange:%s\n"
               "routing_key:%s\n"
               "virtual_host:%s\n"
               "message:%s\n"
               "commit:1\n",
               exchange,
               routing_key,
               virtual_host,
               message
       );

       if (!res || strstr(res, "TRANS_ERROR")) {
               transaction_logprintf(cs->ts, D_ERROR, "NEW_AD_EVENT: TRANS ERROR sending the new ad event.");
               cs->status = "TRANS_ERROR";
       } else {
               transaction_logprintf(cs->ts, D_INFO, "NEW_AD_EVENT: Message sent to RabbitMQ.");
       }
       free(res);
       return 0;
}

int
publish_newad_event(struct cmd *cs, struct bpapi *ba) {
	transaction_logprintf(cs->ts, D_DEBUG, "Start new ad event transaction");
	transaction_logprintf(cs->ts, D_DEBUG, "NEW_AD_EVENT: Inserting new event params");

	// if there are car params, parse them
	int brand = cmd_haskey(cs, "brand") ? atoi(cmd_getval(cs, "brand")) : 0;
	int model = cmd_haskey(cs, "model") ? atoi(cmd_getval(cs, "model")) : 0;
	int version = cmd_haskey(cs, "version") ? atoi(cmd_getval(cs, "version")) : 0;

	if (brand) {
		parse_cardata(cs, ba, brand, model, version);
	}
	get_account_params(cs, ba);
	get_ad_extra_info(cs, ba);
	struct buf_string *bpapiba = BPAPI_OUTPUT_DATA(ba);
	call_template(ba, "trans/send_newad_event.json");
	transaction_logprintf(cs->ts, D_DEBUG, "finished Calling json template:");
	transaction_logprintf(cs->ts, D_DEBUG, "%s", bpapiba->buf);
	return send_newad_message(cs, bpapiba->buf);
}

int
publish_review_with_changes_event(struct cmd *cs, struct bpapi *ba) {
	transaction_logprintf(cs->ts, D_DEBUG, "Start new ad event transaction");
	transaction_logprintf(cs->ts, D_DEBUG, "NEW_AD_EVENT: Inserting new event params");

	// if there are car params, parse them
	int brand = cmd_haskey(cs, "brand") ? atoi(cmd_getval(cs, "brand")) : 0;
	int model = cmd_haskey(cs, "model") ? atoi(cmd_getval(cs, "model")) : 0;
	int version = cmd_haskey(cs, "version") ? atoi(cmd_getval(cs, "version")) : 0;

	if (brand) {
		parse_cardata(cs, ba, brand, model, version);
	}
	transaction_logprintf(cs->ts, D_DEBUG, "NEW_AD_EVENT: get extra info");
	get_ad_with_changes_extra_info(cs, ba);
	transaction_logprintf(cs->ts, D_DEBUG, "NEW_AD_EVENT: get account params");
	const char *email = bpapi_get_element(ba, "ad_email", 0);
	cmd_setval(cs, "email", xstrdup(email));
	get_account_params(cs, ba);
	transaction_logprintf(cs->ts, D_DEBUG, "NEW_AD_EVENT: init template");
	struct buf_string *bpapiba = BPAPI_OUTPUT_DATA(ba);
	call_template(ba, "trans/send_newad_with_changes_event.json");
	transaction_logprintf(cs->ts, D_DEBUG, "finished Calling json template:");
	transaction_logprintf(cs->ts, D_DEBUG, "%s", bpapiba->buf);
	return send_newad_message(cs, bpapiba->buf);
}

int
publish_edit_event(struct cmd *cs, struct bpapi *ba) {
	transaction_logprintf(cs->ts, D_DEBUG, "Start new ad edit event transaction");
	transaction_logprintf(cs->ts, D_DEBUG, "NEW_AD_EDIT_EVENT: Inserting new event params");

	// if there are car params, parse them
	int brand = cmd_haskey(cs, "brand") ? atoi(cmd_getval(cs, "brand")) : 0;
	int model = cmd_haskey(cs, "model") ? atoi(cmd_getval(cs, "model")) : 0;
	int version = cmd_haskey(cs, "version") ? atoi(cmd_getval(cs, "version")) : 0;

	if (brand) {
		parse_cardata(cs, ba, brand, model, version);
	}
	get_account_params(cs, ba);
	get_ad_edit_extra_info(cs, ba);
	struct buf_string *bpapiba = BPAPI_OUTPUT_DATA(ba);
	call_template(ba, "trans/send_edit_event.json");
	transaction_logprintf(cs->ts, D_DEBUG, "finished Calling json template:");
	transaction_logprintf(cs->ts, D_DEBUG, "%s", bpapiba->buf);
	return send_newad_message(cs, bpapiba->buf);
}
