#ifndef REDIS_DELETE_AD
#define REDIS_DELETE_AD
#include "sredisapi.h"
#include "fd_pool.h"
#include "utils/str_norm.h"
#include "utils/redis_trans_context.h"

#include "bpapi.h"
#include "bsapi.h"
#include "command.h"
#include "bconf.h"

int save_deletead_to_redis(struct cmd *cs, struct bpapi *ba);
int remove_all_ad_products(struct cmd *cs, struct bpapi *ba);

#endif
