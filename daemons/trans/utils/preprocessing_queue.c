#include "factory.h"
#include "logging.h"
#include "log_event.h"
#include "bconf.h"

#include "preprocessing_queue.h"

extern struct bconf_node *bconf_root;


static void
move_queue(struct cmd *cs, const char *ad_id, const char *action_id, const char *remote_addr, const char *pack_result, const char *queue){
	int delay = bconf_get_int_default(bconf_root, "*.trans.clear.move_from_preprocessing.delay", 300);
	int pack_to_preprocessing = bconf_get_int_default(bconf_root, "*.trans.clear.move_from_preprocessing.pack", 0);
	int updated = strcmp(pack_result, "-1");
	char *cmd = NULL;
	size_t length = xasprintf(&cmd,
		"cmd:move_from_preprocessing\n"
		"ad_id:%s\n"
		"action_id:%s\n"
		"queue:%s\n"
		"remote_addr:%s\n"
		"commit:1\n",
		ad_id,
		action_id,
		queue,
		remote_addr
	);
	// check if pack ad or delay.
	if (delay == 0 || (updated && pack_to_preprocessing == 0)) {
		transaction_logprintf(cs->ts, D_DEBUG, "preprocessing_queue: delay = 0, or pack ad with move from preprocessing off; move ad inmediately");
		transaction_internal_printf(TRANSACTION_INTERNAL_SYNC_CB, NULL, "%s", cmd);
		free(cmd);
	} else {
		transaction_logprintf(cs->ts, D_DEBUG, "preprocessing_queue: delay != 0, and ad not pack, or move from preprocessing for pack ads is on; move ad to preprocessing");
		const char *trans_queue = bconf_get_string_default(bconf_root, "*.trans.clear.move_from_preprocessing.queue.name", "enqueue");
		// Queue for later
		transaction_internal_printf(TRANSACTION_INTERNAL_SYNC_CB, NULL,
			"cmd:queue\n"
			"commit:1\n"
			"queue:%s\n"
			"timeout:%d\n"
			"blob:%zu:command\n"
			"%s\n",
			trans_queue,
			delay,
			length,
			cmd
		);
		free(cmd);
	}
}

int
move_preprocessing_queue(struct cmd *cs, struct bpapi *ba) {
	const char *queue = bpapi_get_element(ba, "ac_queue", 0) ?: "normal";
	const char *ad_id = bpapi_get_element(ba, "ac_ad_id", 0) ?: bpapi_get_element(ba, "o_ad_id", 0);
	const char *action_id = bpapi_get_element(ba, "action_id", 0) ?: bpapi_get_element(ba, "ac_action_id", 0) ?: bpapi_get_element(ba, "o_action_id", 0);
	const char *remote_addr = bpapi_get_element(ba, "remote_addr", 0) ?: bpapi_get_element(ba, "o_remote_addr", 0);
	if (!remote_addr || !strcmp(remote_addr, "")) {
		remote_addr = "127.0.0.1";
	}
	const char *pack_result = bpapi_get_element(ba, "ac_pack_result", 0) ?: "-1";
	move_queue(cs, ad_id, action_id, remote_addr, pack_result, queue);
	return 0;
}
