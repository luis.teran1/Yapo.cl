#ifndef FILTERD_H
#define FILTERD_H

#include "command.h"

/* Reload filterd service using sync or async algorithm */
int filterd_reload(struct cmd *cs, int async);
/* Reload filterd service using the algorithm from $(*.controlpanel.filterd.reload.async) */
int filterd_reload_fa(struct cmd *cs, struct bpapi *unused);
/* Do a filter request. Input from inkey, output to outkey */
int filterd_filter(struct cmd *cs, struct bpapi *ba, const char *inkey, const char *outkey);

#endif
