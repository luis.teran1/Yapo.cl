#include <stdio.h>
#include <bconf.h>

#include "fd_pool.h"
#include "vtree.h"
#include "json_vtree.h"
#include "price_alert.h"
#include "sredisapi.h"
#include "utils/redis_trans_context.h"
#include "utils/vector.h"

extern struct bconf_node *bconf_root;

typedef struct car_t car_t;
typedef void (*car_action_fn)(struct bpapi_vtree_chain *vtree, car_t *car);

typedef struct {
	int found, appraisal;
} price_avg_t;

struct car_t {
	car_action_fn action;
	int category;
	int brand, model, version;
	int year, fuel, gearbox;
	int appraisal;
	price_avg_t price_avg[3];
	struct cmd *cs;
	struct fd_pool_conn *conn;
};

static void _first_to_match(struct bpapi_vtree_chain *vtree, car_t *car) {
	struct cmd *cs = car->cs;
	if (car->appraisal != -1)
		return;

	int year = vtree_getint(vtree, "x", "year", NULL);
	int fuel = vtree_getint(vtree, "x", "fuel", NULL);
	int gearbox = vtree_getint(vtree, "x", "gearbox", NULL);

	if (year == car->year && fuel == car->fuel && gearbox == car->gearbox) {
		car->appraisal = vtree_getint(vtree, "x", "appraisal", NULL);
		transaction_logprintf(cs->ts, LOG_DEBUG, "cardata match! Appraisal: %d", car->appraisal);
	}
}

static void appraisal_cb(const void *key, size_t klen, const void *value, size_t vlen, void *cbarg) {
	car_t *car = cbarg;
	struct cmd *cs = car->cs;

	transaction_logprintf(cs->ts, LOG_DEBUG, "%*s:%*s\n", (int) klen, (char *) key, (int) vlen, (char *) value);

	struct bpapi_vtree_chain vtree = {0};
	int errors = json_vtree(&vtree, "x", (char *) value, vlen, 1);

	if (!errors) {
		car->action(&vtree, car);
	} else {
		transaction_logprintf(cs->ts, LOG_ERR, "Cardata Parse Error: %*s:%*s\n", (int) klen, (char *) key, (int) vlen, (char *) value);
	}

	vtree_free(&vtree);
}

static void _sum_nearby_years(struct bpapi_vtree_chain *vtree, car_t *car) {
	struct cmd *cs = car->cs;

	int i;
	int map[3] = { 0, 1, -1 };
	int year = vtree_getint(vtree, "x", "year", NULL);
	for (i = 0; i < 3; ++i) {
		if (year == car->year + map[i]) {
			car->price_avg[i].found++;
			car->price_avg[i].appraisal += vtree_getint(vtree, "x", "appraisal", NULL);
			transaction_logprintf(cs->ts, LOG_DEBUG, "cardata adding appraisal %d: %d (total: %d)", year, vtree_getint(vtree, "x", "appraisal", NULL), car->price_avg[i].appraisal);
		}
	}
}

static int _sum_versions(int i, void *e, void *data) {
	intptr_t version = (intptr_t) e;
	car_t *car = data;

	char key[256];
	snprintf(key, 256, "rcd1xapp_id_cardata_attr_%d_%d_%ld", car->brand, car->model, version);
	redis_sock_hgetall(car->conn, key, appraisal_cb, car);
	return 0;
}

static void model_cb(const void *key, size_t klen, const void *value, size_t vlen, void *cbarg) {
	vector_t *v = cbarg;
	intptr_t n = atoi((char *) key);
	vector_append(v, (void *) n);
}

int get_appraisal(struct cmd *cs, int category, int brand, int model, int version, int year, int fuel, int gearbox) {
	if (category != 2020)
		return 0;

	int appraisal = 0;
	
	if (brand && model && year) {
		struct fd_pool_conn *conn = redis_sock_conn(redis_cardata_pool.pool, "master");
		/* The 'nice' case */
		if (version) {
			/* CHUS 447: use redis to get the cardata */
			char *key = NULL;
			xasprintf(&key, "rcd1xapp_id_cardata_attr_%d_%d_%d", brand, model, version);
			transaction_logprintf(cs->ts, LOG_DEBUG, "Trying to hit on redis key: %s\n", key);

			car_t car = { _first_to_match, category, brand, model, version, year, fuel, gearbox, -1, {{0}}, cs, NULL };
			redis_sock_hgetall(conn, key, appraisal_cb, &car);
			free(key);

			if (car.appraisal != -1)
				appraisal = car.appraisal;

		} else {
			/* CHUS: 295-B : if version == 'other' calculate the alert based on average of all version prices for brand, model, year */
			char *model_key = NULL;
			xasprintf(&model_key, "rcd1xapp_id_cardata_versions_%d_%d", brand, model);
			transaction_logprintf(cs->ts, LOG_DEBUG, "Trying to hit on redis key: %s\n", model_key);

			vector_t *v = vector_new(16);
			redis_sock_hgetall(conn, model_key, model_cb, v);
			free(model_key);

			car_t car = { _sum_nearby_years, category, brand, model, version, year, fuel, gearbox, 0, {{0}}, cs, conn };

			vector_foreach(v, _sum_versions, &car);
			vector_delete(v, NULL);

			int i;
			for (i = 0; i < 3; ++i) {
				if (car.price_avg[i].found) {
					appraisal = car.price_avg[i].appraisal / car.price_avg[i].found;
					break;
				}
			}
		}
		fd_pool_free_conn(conn);
	}

	return appraisal;
}

price_alert_t price_alert(struct cmd *cs, int appraisal, int price, int category) {

	price_alert_t alert = PRICE_ALERT_NOTFOUND;
	if (appraisal < 1) return alert;

	char cat[32];
	snprintf(cat, 32, "%d", category);
	int ref_min = bconf_vget_int (bconf_root, "*.*.price_alert", cat, "refuse.min", NULL);
	int ref_max = bconf_vget_int (bconf_root, "*.*.price_alert", cat, "refuse.max", NULL);
	int war_min = bconf_vget_int (bconf_root, "*.*.price_alert", cat, "warning.min", NULL);
	int war_max = bconf_vget_int (bconf_root, "*.*.price_alert", cat, "warning.max", NULL);
	int ok_min = bconf_vget_int (bconf_root, "*.*.price_alert", cat, "ok.min", NULL);
	int ok_max = bconf_vget_int (bconf_root, "*.*.price_alert", cat, "ok.max", NULL);

	double percent = (price - appraisal) * 100.0 / appraisal;

	transaction_logprintf(cs->ts, LOG_DEBUG, "price: %d, appraisal: %d, percent: %lf\n", price, appraisal, percent);
	transaction_logprintf(cs->ts, LOG_DEBUG, "refuse: %d <= %lf < %d\n", ref_min, percent, ref_max);
	transaction_logprintf(cs->ts, LOG_DEBUG, "warning: %d <= %lf < %d\n", war_min, percent, war_max);
	transaction_logprintf(cs->ts, LOG_DEBUG, "ok: %d <= %lf < %d\n", ok_min, percent, ok_max);

	if (percent >= ref_min && percent < ref_max)
		alert = PRICE_ALERT_REFUSE;

	else if (percent >= war_min && percent < war_max)
		alert = PRICE_ALERT_WARNING;

	else if (percent >= ok_min && percent < ok_max)
		alert = PRICE_ALERT_OK;

	return alert;
}
