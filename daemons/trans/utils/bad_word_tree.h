#ifndef BAD_WORD_TREE_H
#define BAD_WORD_TREE_H

#include "vector.h"
#include "trans.h"
#include "command.h"

typedef struct bad_word_tree_t {
    char* word;
	int bad_word_end;
	vector_t* expanded;
}bad_word_tree_t;

bad_word_tree_t* bad_word_tree_new(char* word, int is_end);

void bad_word_insert(struct cmd *cs, vector_t* bwts, vector_t* words, int depth);

void bad_word_tree_append(struct cmd *cs, vector_t* bwts, vector_t* words, int depth);

void bad_word_tree_delete(void* bwt);

#endif
