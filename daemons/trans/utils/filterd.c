#include "filterd.h"

#include <http.h>
#include <trans.h>

#define BUFSIZE 64

extern struct bconf_node *host_bconf;

static long
do_http_request(struct cmd *cs, struct bconf_node *host, const char *endpoint, struct buf_string *response, const char *poststr, const char *headers[]) {
	long r = -1;
	char buf[BUFSIZE];
	int n = bconf_count(host);

	for (int i = 0; i < n && r == -1; ++i) {
		struct bconf_node *h = bconf_byindex(host, i);
		const char *host = bconf_get_string(h, "name");
		const char *port = bconf_get_string(h, "port");
		snprintf(buf, BUFSIZE, "http://%s:%s/%s", host, port, endpoint);
		if (cs != NULL)
			transaction_logprintf(cs->ts, D_DEBUG, "filterd: poking to %s", buf);
		r = poststr ?
			http_post(buf, response, poststr, headers):
			http_get(buf, response, headers);
	}

	return r;
}

int
filterd_reload(struct cmd *cs, int async) {
	const char *algorithm, *endpoint;
	struct buf_string response = {0};

	if (async) {
		endpoint = "async_reload";
		algorithm = "async";
	} else {
		endpoint = "reload";
		algorithm = "sync";
	}
	if (cs != NULL)
		transaction_logprintf(cs->ts, D_DEBUG, "filterd: %s reloading filterd", algorithm);
	else
		DPRINTF(D_DEBUG, ("filterd: %s reloading filterd", algorithm));

	struct bconf_node *host = bconf_get(host_bconf, "*.filterd.control");
	long r = do_http_request(cs, host, endpoint, &response, NULL, NULL);

	if (r == -1) {
		if (cs != NULL)
			transaction_logprintf(cs->ts, D_WARNING, "filterd: could not connect to filterd, reload not issued");
		else
			DPRINTF(D_WARNING, ("filterd: could not connect to filterd, reload not issued"));
	} else {
		if (cs != NULL)
			transaction_logprintf(cs->ts, D_INFO, "filterd: returned from %s reload with status:%ld", algorithm, r);
		else
			DPRINTF(D_INFO, ("filterd: returned from %s reload with status:%ld",algorithm, r));
	}
	free(response.buf);
	return r == 200 || r == 202 ? 0 : -1;
}

int
filterd_reload_fa(struct cmd *cs, struct bpapi *unused) {
	int async = bconf_get_int(host_bconf, "controlpanel.filterd.reload.async");
	return filterd_reload(cs, async);
}

int
filterd_filter(struct cmd *cs, struct bpapi *ba, const char *inkey, const char *outkey) {
	const char *poststr = bpapi_get_element(ba, inkey, 0);

	if (poststr) {
		char clength[64];
		struct buf_string response = {0};
		snprintf(clength, 64, "Content-Length: %ld", strlen(poststr));
		const char *headers[] = {
			"Content-Type: application/json",
			clength,
			NULL,
		};

		if (cs != NULL)
			transaction_logprintf(cs->ts, D_DEBUG, "filterd: calling filterd with: %s", poststr);
		else
			DPRINTF(D_DEBUG, ("filterd: calling filterd with: [%s]", poststr));
		struct bconf_node *host = bconf_get(host_bconf, "*.filterd.host");
		long r = do_http_request(cs, host, "filter", &response, poststr, headers);

		if (cs != NULL)
			transaction_logprintf(cs->ts, D_INFO, "filterd: returned from filter with status:%ld", r);
		else
			DPRINTF(D_INFO, ("filterd_filter returned from filter with status: %ld", r));

		if (r == 200) {
			if(cs != NULL)
				transaction_logprintf(cs->ts, D_DEBUG, "filterd: response: %.*s", response.pos, response.buf);
			else
				DPRINTF(D_DEBUG, ("filterd: response: %.*s", response.pos, response.buf));
			bpapi_insert(ba, outkey, response.buf);
			if(response.buf != NULL) {
				free(response.buf);
				return 0;
			}
		}

		free(response.buf);
	}
	return 1;
}
