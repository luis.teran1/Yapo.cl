#include <stdint.h>
#include <time.h>
#include "bconf.h"
#include "fd_pool.h"
#include "memalloc_functions.h"
#include "sha_wrapper.h"
#include "sredisapi.h"
#include "utils/redis_trans_context.h"

#include "refresh_session.h"

extern struct bconf_node *bconf_root;

void call_refresh_account(const char * email){
	transaction_internal_printf(
		TRANSACTION_INTERNAL_SYNC_CB, 
		NULL,
		"cmd:refresh_account_cache\n"
		"email:%s\n"
		"commit:1\n",
		email
	);
}

int 
refresh_account(struct cmd *cs, struct bpapi *ba) {
	const char *email = cmd_haskey(cs, "email") ? cmd_getval(cs, "email")
		: bpapi_has_key(ba, "email") ? bpapi_get_element(ba, "email", 0)
		: bpapi_has_key(ba, "o_email") ? bpapi_get_element(ba, "o_email", 0)
		: bpapi_has_key(ba, "ac_email") ? bpapi_get_element(ba, "ac_email", 0)
		: NULL;
	
	if (email && *email) {
		if (strchr(email, ',')) {
			char *ptr = NULL;
			char *email_copy = xstrdup(email);
			char *eml = strtok_r(email_copy, ",", &ptr);

			while (eml) {
				call_refresh_account(eml);
				eml = strtok_r(NULL, ",", &ptr);
			}
			free(email_copy);
		} else {
			call_refresh_account(email);
		}
	} else {
		transaction_logprintf(cs->ts, D_WARNING, "email empty calling refresh_account_cache");	
	}
	return 0;
}
