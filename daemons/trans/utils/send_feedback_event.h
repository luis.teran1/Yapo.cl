#ifndef SEND_FEEDBACK_EVENT
#define SEND_FEEDBACK_EVENT
#include "sredisapi.h"
#include "fd_pool.h"
#include "utils/str_norm.h"
#include "utils/redis_trans_context.h"

#include "vtree.h"
#include "json_vtree.h"
#include "bpapi.h"
#include "bsapi.h"
#include "command.h"
#include "bconf.h"

// These functions send the data of reviewd ads to be used in other applications
// and services. Link to the related documentation that motivated the creation of this
// functions: <INSERT DOCUMENTATION LINK>

// Parse the ad information in the cmd and bpapi, generates a json
// with most of the ad info and sends it to the "newad event" exchange in RabbitMQ.
int publish_feedback_event(struct cmd *cs, struct bpapi *ba);

#endif
