#include <stdio.h>
#include <string.h>

#include <pcre.h>
#include "trans.h"
#include "regex_split.h"

vector_t* regex_split(struct cmd *cs, char *text, const char *pat) {
	pcre *re;
	int errcode;
	const char *errstr;
	re = pcre_compile(pat, 0, &errstr, &errcode, NULL);
	if (!re) {
		transaction_logprintf(cs->ts, D_DEBUG, "Error [%s (%d)] compiling regex: %s\n", errstr, errcode, pat);
		return NULL;
	}

	vector_t *v = vector_new(128);

	int off = 0, len = strlen(text);
	int out[3] = {0};
	int r = pcre_exec(re, NULL, text, len, off, 0, out, 3);

	while (r > 0) {
		text[out[0]] = '\0';
		transaction_logprintf(cs->ts, D_DEBUG, "split got: %s\n", text + off);
		vector_append(v, text + off);
		off = out[1];
		r = pcre_exec(re, NULL, text, len, off, 0, out, 3);
	}

	transaction_logprintf(cs->ts, D_DEBUG, "split got: %s\n", text + off);
	vector_append(v, text + off);
	pcre_free(re);
	return v;
}
