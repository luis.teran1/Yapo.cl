#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "trans.h"
#include "bad_words.h"
#include "regex_split.h"
#include "bad_word_tree.h"

vector_t* parse_bad_words(struct cmd *cs, char *trans_bad_words) {
	char *line, *lineptr;
	const char *pattern = "[[:space:]*[:punct:]*]+";
	vector_t *bwts = vector_new(64);
	line = strtok_r(trans_bad_words, "\n", &lineptr);
	while (line) {
		if (!strncmp(line, "value:", 6)) {
			char *w = line + 6;
			vector_t *words = regex_split(cs, w, pattern);
			bad_word_tree_append(cs, bwts, words, 0);
			vector_delete(words, NULL);
		}
		line = strtok_r(NULL, "\n", &lineptr);
	}
	return bwts;
}

int bad_word_expanded(struct cmd *cs, vector_t* v, int index, vector_t* bwts) {

	if (index >= vector_size(v)){
		return 0;
	}

	int i=0;
	int has=0;
	int options = vector_size(bwts);
	char* curr = vector_data(v)[index];

	for(i=0; i<options && !has; i++){
		if (strcasecmp(((bad_word_tree_t*)vector_data(bwts)[i])->word, curr)==0){
			has = ((bad_word_tree_t*)vector_data(bwts)[i])->bad_word_end || bad_word_expanded(cs, v, index+1, ((bad_word_tree_t*)vector_data(bwts)[i])->expanded);
			if (has) {
				return has;
			}
		}
	}
	return has;
}

int text_has_bad_words(struct cmd *cs, vector_t *bad_word_trees, const char *text) {
	int has = 0;
	int i = 0;
	char *str = xstrdup(text);
	const char *pattern = "[[:space:]*[:punct:]*]+";
	vector_t *v = regex_split(cs, str, pattern);

	if (v) {
		for(i=0; i<vector_size(v) && !has;i++) {
			transaction_logprintf(cs->ts, D_DEBUG, "Checking for bad words from: %s", ((char*)vector_data(v)[i]));
			has = bad_word_expanded(cs, v, i, bad_word_trees);
			if (has) {
				transaction_logprintf(cs->ts, D_DEBUG, "Bad word found from: %s", ((char*)vector_data(v)[i]));
				break;
			}
		}
	}

	vector_delete(v, NULL);
	free(str);
	return has;
}
