#include "redis_trans_context.h"

struct trans_redis redis_stat_pool;
struct trans_redis redis_email_pool;
struct trans_redis redis_session_pool;
struct trans_redis redis_multiproduct_pool;
struct trans_redis redis_wordsranking;
struct trans_redis redis_cardata_pool;
struct trans_redis redis_banners_pool;
struct trans_redis redis_ppages_pool;
struct trans_redis redis_mobile_api_pool;
struct trans_redis redis_dashboard_pool;

static void
init_done_redis_trans_contexts(void) {
	trans_redis_init(&redis_stat_pool, "common.redisstat");
	trans_redis_init(&redis_email_pool, "common.redis_email");
	trans_redis_init(&redis_session_pool, "common.redis_session");
	trans_redis_init(&redis_multiproduct_pool, "common.redis_multiproduct");
	trans_redis_init(&redis_banners_pool, "common.redis_banners");
	trans_redis_init(&redis_ppages_pool, "common.redis_ppages");
	trans_redis_init(&redis_wordsranking, "common.redis_wordsranking");
	trans_redis_init(&redis_cardata_pool, "common.redis_cardata");
	trans_redis_init(&redis_mobile_api_pool, "common.redis_mobile_api");
	trans_redis_init(&redis_dashboard_pool, "common.redis_dashboard");
}

ADD_BACKEND(redis_trans_contexts, NULL, NULL, init_done_redis_trans_contexts, NULL, NULL);
