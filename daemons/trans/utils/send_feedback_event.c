#include "factory.h"
#include "logging.h"
#include "log_event.h"
#include "bconf.h"
#include "utils/vector.h"
#include "vtree.h"
#include "json_vtree.h"
#include "fd_pool.h"
#include "sredisapi.h"
#include "utils/create_session_hash.h"

#include "send_feedback_event.h"

extern struct bconf_node *bconf_root;

static int
send_feedback_message(struct cmd *cs, char *message){
       const char *exchange = bconf_get_string(bconf_root, "*.*.common.feedback_event.rabbitmq.exchange");
       const char *routing_key = bconf_get_string(bconf_root, "*.*.common.feedback_event.rabbitmq.routing_key");
       const char *virtual_host = bconf_get_string(bconf_root, "*.*.common.feedback_event.rabbitmq.vhost");

       transaction_logprintf(
               cs->ts,
               D_DEBUG,
               "FEEDBACK_EVENT: Loading RabbitMQ seetings exchange=%s, routing key=%s, vhost=%s",
               exchange,
               routing_key,
               virtual_host);

       char *res = transaction_internal_output_printf(
               "cmd:rabbitmq_send\n"
               "exchange:%s\n"
               "routing_key:%s\n"
               "virtual_host:%s\n"
               "message:%s\n"
               "commit:1\n",
               exchange,
               routing_key,
               virtual_host,
               message
       );

       if (!res || strstr(res, "TRANS_ERROR")) {
               transaction_logprintf(cs->ts, D_ERROR, "FEEDBACK_EVENT: TRANS ERROR sending feedback event.");
               cs->status = "TRANS_ERROR";
       } else {
               transaction_logprintf(cs->ts, D_INFO, "FEEDBACK_EVENT: Message sent to RabbitMQ.");
       }
       free(res);
       return 0;
}

int
publish_feedback_event(struct cmd *cs, struct bpapi *ba) {
	transaction_logprintf(cs->ts, D_DEBUG, "Start new ads' feedback event transaction");
	transaction_logprintf(cs->ts, D_DEBUG, "FEEDBACK_EVENT: Inserting new ads' feedback event");

	struct buf_string *bpapiba = BPAPI_OUTPUT_DATA(ba);
	call_template(ba, "trans/send_feedback_event.json");
	transaction_logprintf(cs->ts, D_DEBUG, "finished Calling json template:");
	transaction_logprintf(cs->ts, D_DEBUG, "%s", bpapiba->buf);
	return send_feedback_message(cs, bpapiba->buf);
}
