#include <bconf.h>
#include "trans.h"

#include "redis_connect.h"

extern struct bconf_node *bconf_root;

/* connects to redis and switch to corresponding db 
 * base_redis_conf should contain the bconf holding the redis description:
 *
 * Example: "*.*.common.redisstat"i
 */
redisContext * rs_connect(struct cmd *cs, const char *base_redis_conf) {
	struct bconf_node *conf = bconf_get(bconf_root, base_redis_conf);
	const char *host = bconf_get_string(conf, "host.rs1.name");
	int port = bconf_get_int(conf, "host.rs1.port");

	transaction_logprintf(cs->ts, D_DEBUG, "Connecting to redis");
	redisContext *redis = redisConnect(host, port);

	if(!redis || redis->err) {
		transaction_logprintf(cs->ts, D_ERROR, "Error connecting to redis stats");
		cs->error = "TRANS_ERROR";
		cs->message = xstrdup("ERROR_CONNECTING_TO_REDIS");

		if(redis) {
			redisFree(redis);
		}

		return NULL;
	}

	int bconf_db = bconf_get_int_default(conf, "db", -1);
	
	if (bconf_db >= 0) {
		transaction_logprintf(cs->ts, D_DEBUG, "Switching to db [%d] on redis [%s]", bconf_db, base_redis_conf);
		redisReply *db = redisCommand(redis, "SELECT %d", bconf_db);

		if(!db || db->type == 6) {
			transaction_logprintf(cs->ts, D_ERROR, "Error connecting redis [%s:%d]", base_redis_conf, bconf_db);
			cs->error = "TRANS_ERROR";
			cs->message = xstrdup("ERROR_SWITCHING_REDIS_DB");
			redisFree(redis);

			if(db) {
				freeReplyObject(db);
			}

			return NULL;
		}

		freeReplyObject(db);
	}

	return(redis);
}
