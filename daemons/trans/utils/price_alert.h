#ifndef PRICE_ALERT_H
#define PRICE_ALERT_H

#include "command.h"

typedef enum {
	PRICE_ALERT_OK = 0,
	PRICE_ALERT_WARNING,
	PRICE_ALERT_REFUSE,
	PRICE_ALERT_NOTFOUND
} price_alert_t;

int get_appraisal(struct cmd *cs, int category, int brand, int model, int version, int year, int fuel, int gearbox);
price_alert_t price_alert(struct cmd *cs, int appraisal, int price, int category);

#endif

