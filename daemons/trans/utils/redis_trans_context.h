#ifndef REDIS_TRANS_CONTEXT_H
#define REDIS_TRANS_CONTEXT_H

#include "hash.h"
#include "util.h"
#include "bconf.h"
#include "trans.h"
#include "hiredis.h"

/*************** structs *****************/
struct _redis_trans_context {
	struct cmd *cs;
	redisContext *redis;
	struct hash_table *reply_data;
};

typedef struct _redis_trans_context redis_trans_context;

extern struct trans_redis redis_stat_pool;
extern struct trans_redis redis_email_pool;
extern struct trans_redis redis_session_pool;
extern struct trans_redis redis_multiproduct_pool;
extern struct trans_redis redis_wordsranking;
extern struct trans_redis redis_cardata_pool;
extern struct trans_redis redis_banners_pool;
extern struct trans_redis redis_ppages_pool;
extern struct trans_redis redis_mobile_api_pool;
extern struct trans_redis redis_dashboard_pool;

/************** functions ****************/
redis_trans_context *init_context();
void free_context(redis_trans_context *);

#endif

