#ifndef SEND_BACKEND_EVENT
#define SEND_BACKEND_EVENT
#include "sredisapi.h"
#include "fd_pool.h"
#include "utils/str_norm.h"
#include "utils/redis_trans_context.h"

#include "vtree.h"
#include "json_vtree.h"
#include "bpapi.h"
#include "bsapi.h"
#include "command.h"
#include "bconf.h"

// Parse the information in the cmd and bpapi, generates a json
// with most of the info and sends it to the "backend_event" exchange in RabbitMQ.
int publish_backend_event(struct cmd *cs, struct bpapi *ba);

#endif
