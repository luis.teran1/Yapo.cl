#ifndef TRANS_TIMER_BUMP_H
#define TRANS_TIMER_BUMP_H

time_t get_timer_for_bump(void);
char *get_timer_for_bump_human_readable(time_t);

#endif
