
#include "factory.h"
#include "logging.h"
#include "log_event.h"
#include "bconf.h"

#include "prioritize_queues.h"

extern struct bconf_node *bconf_root;

static char*
select_queue(struct cmd *cs, struct bpapi *ba, const char *ad_evaluation_queue, const char *original_queue){
	char *result = (char *)original_queue;
	// if the queues are the same, just use it
	if (!strcmp(original_queue, ad_evaluation_queue)) {
		transaction_logprintf(cs->ts, D_DEBUG, "prioritize queues: both queues are the same.");
		return result;
	}
	transaction_logprintf(cs->ts, D_DEBUG, "prioritize queues: queue:%s evaluation_queue:%s", original_queue, ad_evaluation_queue);
	struct bconf_node *or_queues_queues = bconf_get(bconf_root, "*.*.ad_evaluation_preprocessing.override_queues");
	const char *or_queues_list = bconf_get_string(or_queues_queues, (char *)ad_evaluation_queue);
	if (!or_queues_list) {
		transaction_logprintf(cs->ts, D_DEBUG, "prioritize queues: evaluation queue:%s doesnt override other queues, skipping.", ad_evaluation_queue);
		return result;
	}
	char *queues = xstrdup(or_queues_list);
	transaction_logprintf(cs->ts, D_DEBUG, "content of override queues:%s", queues);
	if (queues) {
		//check if original queue in the list of override queues
		char *ptr;
		char *or_queue = strtok_r(queues, ",", &ptr);
		while (or_queue) {
			if (!strcmp(or_queue, original_queue)) {
				transaction_logprintf(cs->ts, D_DEBUG, "prioritize queues: queue in override list, using ad_evaluation");
				result = (char *)ad_evaluation_queue;
				return result;
			}
			or_queue = strtok_r(NULL, ",", &ptr);
		}
	}
	transaction_logprintf(cs->ts, D_DEBUG, "prioritize queues: queue doesnt match list, using original");
	return result;
}

void
prioritize_queues(struct cmd *cs, struct bpapi *ba) {
	const char *queue = cmd_getval(cs, "queue");
	const char *target_queue = queue;
	const char *ad_evaluation_queue = bpapi_get_element(ba, "ad_evaluation_queue", 0);
	
	transaction_logprintf(cs->ts, D_DEBUG, "prioritize queues: queue:%s evaluation_queue:%s", queue, ad_evaluation_queue);
	
	if (!ad_evaluation_queue){
		bpapi_insert(ba, "target_queue", target_queue);
		return;
	}
	target_queue=select_queue(cs, ba, ad_evaluation_queue, queue);
	bpapi_insert(ba, "target_queue", target_queue);
	return;
}
