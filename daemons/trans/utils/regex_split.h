#ifndef REGEX_SPLIT_H
#define REGEX_SPLIT_H

#include "vector.h"
#include "command.h"

/*
 * Split a string with the regex defined in pattern.
 * Returns a vector filled with the non-matching parts of the string.
 * The original string is modified, so you need to keep it available
 * until you are done with the result vector. After that, the user is
 * responsible for calling vector_delete and freeing the original str
 */
vector_t* regex_split(struct cmd *cs, char *text, const char *pattern);

#endif

