#ifndef PRIORITIZE_QUEUE
#define PRIORITIZE_QUEUE
#include "utils/str_norm.h"

#include "bpapi.h"
#include "bsapi.h"
#include "command.h"
#include "bconf.h"

// This function is used to prioritize between the queue to move an ad (coming from move_from_preprocessing),
// and a queue setted by set_ad_evaluation (if any)

// Parse the queue information in the cmd and bpapi, load the required priority list, and sets on the bpapi
// the queue with more priority on the variable 'target_queue'
void prioritize_queues(struct cmd *cs, struct bpapi *ba);

#endif
