#ifndef VECTOR_H
#define VECTOR_H

typedef struct vector_t vector_t;

/*
 * Function to be called upon every element before freeing the vector
 */
typedef void (*freefn)(void *e);

/*
 * Function to be passed to vector iterators.
 * i is the index of the element
 * e is the element being processed
 * data is the void ptr passed to the iterator function
 */
typedef int (*iterfn)(int i, void *e, void *data);

/*
 * Creates a new vector, initialized to desired capacity
 */
vector_t* vector_new(int capacity);

/*
 * Destroys a vector, if f is given, it will be applied to
 * all elements before freeing the vector
 */
void vector_delete(vector_t *v, freefn f);

/*
 * Appends element e at the end of the vector. Vector is resized if necessary.
 */
int vector_append(vector_t *v, void *e);

/*
 * Returns the number of elements present on the vector.
 */
int vector_size(vector_t *v);

/*
 * Returns vector data pointer.
 * Handle with care, please try not to break it. I know where you live...
 */
void **vector_data(vector_t*);

/*
 * These functions behave as iterators through given vector:
 *
 * vector_foreach: Applies operation to all members, result is discarded
 * vector_all: returns whether or not predicate is true for all elements
 * vector_any: returns whether or not predicate is true for any element
 */
void vector_foreach(vector_t *v, iterfn f, void *data);
int vector_all(vector_t *v, iterfn f, void *data);
int vector_any(vector_t *v, iterfn f, void *data);

#endif
