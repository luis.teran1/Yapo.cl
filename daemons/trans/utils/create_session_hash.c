#include <stdint.h>
#include <time.h>
#include "bconf.h"
#include "fd_pool.h"
#include "memalloc_functions.h"
#include "sha_wrapper.h"
#include "sredisapi.h"
#include "utils/redis_trans_context.h"

#include "create_session_hash.h"

extern struct bconf_node *bconf_root;

char *
create_session_hash(const char *email) {
	time_t now;
	char *token;
	char *token_hash;

	time(&now);
	int rand_num = rand() % 100000;
	xasprintf(&token, "%s-%ju-%d", email, (uintmax_t) now, rand_num);
	token_hash = sha1(token);
	free(token);

	return token_hash;
}

char *
create_profile_cache_hash(const char *email) {
	char *rkey = NULL;
	char *hashed_email = sha1(email);
	const char *prefix = bconf_get_string(bconf_root, "*.*.accounts.session_redis_prefix");
	xasprintf(&rkey, "%s%s", prefix, hashed_email);
	free(hashed_email);
	return rkey;
}

int
flush_redis_profile_key(struct cmd *cs, struct bpapi *ba) {
	const char *email = cmd_haskey(cs, "email") ? cmd_getval(cs, "email") 
					  : bpapi_has_key(ba, "email") ? bpapi_get_element(ba, "email", 0)
					  : bpapi_has_key(ba, "o_email") ? bpapi_get_element(ba, "o_email", 0)
					  : NULL;

    if (email && *email) {

        struct fd_pool_conn *conn = redis_sock_conn(redis_session_pool.pool, "master");

        if (strchr(email, ',')) {
            char *ptr = NULL;
            char *email_copy = xstrdup(email);
            char *eml = strtok_r(email_copy, ",", &ptr);

            while (eml) {
                // Flush ALL the things \o
                char *rkey = create_profile_cache_hash(eml);
                redis_sock_del(conn, rkey);
                free(rkey);
                eml = strtok_r(NULL, ",", &ptr);
            }
            free(email_copy);
        } else {
            char *rkey = create_profile_cache_hash(email);
            redis_sock_del(conn, rkey);
            free(rkey);
        }

        fd_pool_free_conn(conn);
	}
	return 0;
}
