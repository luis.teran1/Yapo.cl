#ifndef PREPROCESSING_QUEUE
#define PREPROCESSING_QUEUE
#include "utils/str_norm.h"

#include "bpapi.h"
#include "bsapi.h"
#include "command.h"
#include "bconf.h"

// This function is used to move an ad from the "preprocessing" queue to the given queue
// Parse the ad information in the cmd and bpapi, and generates a trans command
// to move the ad to the required queue and enqueue it on trans_queue (or execute it
// inmediately if the user is pro for the ad or the delay is 0)
int move_preprocessing_queue(struct cmd *cs, struct bpapi *ba);

#endif
