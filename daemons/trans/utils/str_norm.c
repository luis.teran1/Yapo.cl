#include "str_norm.h"
#include <ctype.h>

static int ready = 0;
static unsigned char map[256];

static void
le_initialization() {

	unsigned const char *from = (unsigned const char *) "��������������������������������������������������������������穥���";
	unsigned const char *to	  = (unsigned const char *) "AAAAAAACEEEEIIIIDNOOOOOOUUUUYsaaaaaaaceeeeiiiinoooooouuuuyyNnCccYRou";

	unsigned const char *c = from;
	unsigned const char *t = to;

	while (*c) {
		int n = *c++;
		map[n] = *t++;
	}

	ready = 1;
}


char *
str_norm(char *str) {
	if (!ready)
		le_initialization();

	unsigned char *s = (unsigned char *) str;
	while (s && *s) {
		if (map[(int) *s])
			*s = map[(int) *s];
		*s = tolower(*s);
		s++;
	}
	return str;
}
