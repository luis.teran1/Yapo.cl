#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "utils/first_experience_bump_timer.h"
#include "bconf.h"
#include "util.h"

extern struct bconf_node *bconf_root;

time_t
get_timer_for_bump() {
    time_t current_time, bump_time;
    struct tm *localTime;
    current_time = time(NULL);

    // Bconfigurable
    int lower_bound = bconf_get_int(bconf_root, "*.*.first_experience.min_bump_hour");
    int upper_bound = bconf_get_int(bconf_root, "*.*.first_experience.max_bump_hour");
    int days_until_go = bconf_get_int(bconf_root, "*.*.first_experience.days_until_bump");
    localTime = localtime(&current_time);

    if (days_until_go >= 0){
        int diferencial_bound = upper_bound - lower_bound + 1;
        int randomic_hours = rand() % diferencial_bound + 1;
        int randomic_mins = rand() % 60;

        int hours_to_midnight = 23 - localTime->tm_hour;
        localTime->tm_hour += hours_to_midnight + ((days_until_go - 1) * 24) + lower_bound + randomic_hours;
        localTime->tm_min = randomic_mins;
        mktime(localTime);
    } else {
        localTime->tm_min += 1;
    }

    bump_time = mktime(localTime);
    return ((bump_time > current_time) ? bump_time - current_time : bump_time - current_time + 86400);
}

char *
get_timer_for_bump_human_readable(time_t timer_for_bump){
    char *readable_time;
    time_t current_time = time(NULL);
    timer_for_bump = current_time + timer_for_bump;
    struct tm *localTime = localtime(&timer_for_bump);
    xasprintf(&readable_time, "%d/%02d/%02d at %02d:%02d:%02d - %i", localTime->tm_year+1900, localTime->tm_mon + 1, localTime->tm_mday,
            localTime->tm_hour, localTime->tm_min, localTime->tm_sec, (unsigned int) timer_for_bump);
    return readable_time;
}
