#ifndef YAPO_REDIS_CONNECT_H
#define YAPO_REDIS_CONNECT_H

#include "command.h"
#include <hiredis.h>

redisContext * rs_connect(struct cmd *cs, const char *base_redis_conf);

#endif
