#ifndef SEND_NEWAD_EVENT
#define SEND_NEWAD_EVENT
#include "sredisapi.h"
#include "fd_pool.h"
#include "utils/str_norm.h"
#include "utils/redis_trans_context.h"

#include "vtree.h"
#include "json_vtree.h"
#include "bpapi.h"
#include "bsapi.h"
#include "command.h"
#include "bconf.h"

// These functions send the data of new ads to be used in other applications
// and services. Link to the related documentation that motivated the creation of this
// functions: https://confluence.schibsted.io/pages/viewpage.action?pageId=103813406

// Parse the ad information in the cmd and bpapi, generates a json
// with most of the ad info and sends it to the "newad event" exchange in RabbitMQ.
int publish_newad_event(struct cmd *cs, struct bpapi *ba);
int publish_review_with_changes_event(struct cmd *cs, struct bpapi *ba);
int publish_edit_event(struct cmd *cs, struct bpapi *ba);

#endif
