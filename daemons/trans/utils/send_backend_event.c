#include "factory.h"
#include "time.h"
#include "stdio.h"
#include "logging.h"
#include "log_event.h"
#include "bconf.h"
#include "utils/vector.h"
#include "vtree.h"
#include "json_vtree.h"
#include "fd_pool.h"
#include "sredisapi.h"
#include "utils/create_session_hash.h"

#include "send_backend_event.h"

extern struct bconf_node *bconf_root;

static int
send_backend_message(struct cmd *cs, char *message){
       const char *exchange = bconf_get_string(bconf_root, "*.*.common.backend_event.rabbitmq.exchange");
       const char *routing_key = bconf_get_string(bconf_root, "*.*.common.backend_event.rabbitmq.routing_key");
       const char *virtual_host = bconf_get_string(bconf_root, "*.*.common.backend_event.rabbitmq.vhost");

       transaction_logprintf(
               cs->ts,
               D_DEBUG,
               "BACKEND_EVENT: Loading RabbitMQ seetings exchange=%s, routing key=%s, vhost=%s",
               exchange,
               routing_key,
               virtual_host);

       char *res = transaction_internal_output_printf(
               "cmd:rabbitmq_send\n"
               "exchange:%s\n"
               "routing_key:%s\n"
               "virtual_host:%s\n"
               "message:%s\n"
               "commit:1\n",
               exchange,
               routing_key,
               virtual_host,
               message
       );

       if (!res || strstr(res, "TRANS_ERROR")) {
               transaction_logprintf(cs->ts, D_ERROR, "BACKEND_EVENT: TRANS ERROR sending backend event.");
               cs->status = "TRANS_ERROR";
       } else {
               transaction_logprintf(cs->ts, D_INFO, "BACKEND_EVENT: Message sent to RabbitMQ.");
       }
       free(res);
       return 0;
}

int
publish_backend_event(struct cmd *cs, struct bpapi *ba) {
	const time_t utc_t = time(NULL);
	char buff_event_ts_utc[30];
	sprintf(buff_event_ts_utc, "%lu", (unsigned long)utc_t);
	bpapi_insert(ba, "event_ts_utc", buff_event_ts_utc);
	struct buf_string *bpapiba = BPAPI_OUTPUT_DATA(ba);
	call_template(ba, "trans/send_backend_event.json");
	transaction_logprintf(cs->ts, D_DEBUG, "finished Calling json template:");
	transaction_logprintf(cs->ts, D_DEBUG, "%s", bpapiba->buf);
	return send_backend_message(cs, bpapiba->buf);
}
