#include "sredisapi.h"
#include "fd_pool.h"
#include "utils/str_norm.h"
#include "utils/redis_trans_context.h"
#include "utils/redis_delete_ad.h"
#include "bconf.h"

extern struct bconf_node *bconf_root;

int
save_deletead_to_redis(struct cmd *cs, struct bpapi *ba) {
	char *key = NULL;
	xasprintf(&key, "ads:%s:deleted", bpapi_get_element(ba, "list_id", 0));

	const char *reason = cmd_haskey(cs, "deletion_reason") ? cmd_getval(cs, "deletion_reason") : "3";
	const char *what = !strcmp(reason, "1") ? "SOLD" : "DELETED";

	struct fd_pool_conn *conn = redis_sock_conn(redis_wordsranking.pool, "master");
	if (conn) {
		redis_sock_hset(conn, key, "reason", what);

		char buff[256];
		const char *vars[] = {"subject", "region", "category", "communes", NULL};
		for (int i = 0; vars[i] != NULL; ++i) {
			const char *value = bpapi_get_element(ba, vars[i], 0);
			snprintf(buff, 256, "%s", value);
			redis_sock_hset(conn, key, vars[i], str_norm(buff));
		}

		int expiration_minutes = bconf_get_int(bconf_root, "*.*.ad_not_found.key.expiration_minutes");
		redis_sock_expire(conn, key, 60 * expiration_minutes);
		fd_pool_free_conn(conn);
	} else {
		transaction_logprintf(cs->ts, D_ERROR, "Couldn't fetch redis to store ad not found info");
	}
	free(key);
	return 0;
}

/**
 * Remove the ads products from the user shopping cart
 */
int
remove_all_ad_products(struct cmd *cs, struct bpapi *ba) {
	const char *prefix = bconf_get_string(bconf_root, "*.*.common.account_cart_info");
	struct bconf_node *payment_prod = bconf_get(bconf_root, "*.*.payment.product");
	int nprods = bconf_count(payment_prod);

	const char *action = bpapi_get_element(ba, "raap_action", 0);
	struct fd_pool_conn *conn = redis_sock_conn(redis_multiproduct_pool.pool, "master");

	if (conn) {
		for (int i = 0; i < nprods; i++) {
			struct bconf_node *product_def = bconf_byindex(payment_prod, i);
			const char *current_key = bconf_key(bconf_byindex(payment_prod, i));
			const char *is_for_ads = bconf_get_string(product_def, "is_for_ads");
			const char *codename = bconf_get_string(product_def, "codename");
			const char *get_ad_by = bconf_get_string(product_def, "get_ad_by");

			//transaction_logprintf(state->cs->ts, D_DEBUG, "Clean cart k:%s,i:%s,cn:%s,g:%s",current_key, is_for_ads, codename, group);
			if (action && strcmp(action,"deactivated") == 0 && codename && strcmp(codename, "bump") == 0) {
				continue;
			}

			if (is_for_ads && atoi(is_for_ads) == 1) {
				char *cart_key = NULL;
				const char *email = bpapi_get_element(ba, "email", 0);
				xasprintf(&cart_key, "%s:%s:%s", prefix, email, current_key);
				
				// XXX This must be rewritten in terms of yi's bconf
				if (!strcmp(get_ad_by, "ad_id")) {
					//Upselling products
					const char *ad_id = bpapi_get_element(ba, "ad_id", 0);
					redis_sock_hdel(conn, cart_key, ad_id);
					transaction_logprintf(cs->ts, D_DEBUG, "Clean cart:%s - ad_id:%s", cart_key, ad_id);
				} else {
					// Normal products
					const char *list_id = bpapi_get_element(ba, "list_id", 0);
					redis_sock_hdel(conn, cart_key, list_id);
					transaction_logprintf(cs->ts, D_DEBUG, "Clean cart:%s - list_id:%s", cart_key, list_id);
				}
				free(cart_key);
			}
		}
		transaction_logprintf(cs->ts, D_DEBUG, "Clean cart DONE");
		fd_pool_free_conn(conn);
	} else {
		transaction_logprintf(cs->ts, D_ERROR, "Conection to redis accounts fail");
	}
	return 0;
}
