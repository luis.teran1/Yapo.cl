#ifndef YAPO_CREATE_HASH_H
#define YAPO_CREATE_HASH_H

#include "bpapi.h"
#include "command.h"

// maps email to session redis key
char *create_session_hash(const char *email);

// maps email to user profile redis key
char *create_profile_cache_hash(const char *email);

// if 'email' key is found on cs or ba, it flushes the user profile on redis
int flush_redis_profile_key(struct cmd *cs, struct bpapi *ba);

#endif
