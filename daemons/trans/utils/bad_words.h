#ifndef BAD_WORDS_H
#define BAD_WORDS_H

#include "vector.h"
#include "command.h"

vector_t* parse_bad_words(struct cmd *cs, char *trans_bad_words);

int bad_word_expanded(struct cmd *cs, vector_t* v, int index, vector_t* bwts);

int text_has_bad_words(struct cmd *cs, vector_t *bad_word_trees, const char *text);

#endif

