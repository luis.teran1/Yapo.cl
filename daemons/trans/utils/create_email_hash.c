#include <stdint.h>
#include <time.h>
#include "bconf.h"
#include "fd_pool.h"
#include "memalloc_functions.h"
#include "sha_wrapper.h"
#include "sredisapi.h"
#include "utils/redis_trans_context.h"

#include "create_email_hash.h"

extern struct bconf_node *bconf_root;

char *
create_account_email_hash(const char *account_id) {
	char *rkey = NULL;
	char *hashed_account_id = sha1(account_id);
	const char *prefix = bconf_get_string(bconf_root, "*.*.accounts.email_redis_account_prefix");
	xasprintf(&rkey, "%s%s", prefix, hashed_account_id);
	free(hashed_account_id);
	return rkey;
}

char *
create_user_email_hash(const char *user_id) {
	char *rkey = NULL;
	char *hashed_user_id = sha1(user_id);
	const char *prefix = bconf_get_string(bconf_root, "*.*.accounts.email_redis_user_prefix");
	xasprintf(&rkey, "%s%s", prefix, hashed_user_id);
	free(hashed_user_id);
	return rkey;
}
