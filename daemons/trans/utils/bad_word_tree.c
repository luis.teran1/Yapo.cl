#include <stdlib.h>
#include <string.h>

#include "bad_word_tree.h"

// constructor
bad_word_tree_t* bad_word_tree_new(char* word, int is_end){
	bad_word_tree_t* ret = (bad_word_tree_t*) xmalloc(sizeof(bad_word_tree_t));
	ret->word = xstrdup(word);
	ret->bad_word_end = is_end;
	ret->expanded = vector_new(1);
	return ret;
}

/* Insert a new "multiple word" bad word in the tree */
void bad_word_insert(struct cmd *cs, vector_t* bwts, vector_t* words, int depth){
	bad_word_tree_t* new_tree = bad_word_tree_new(vector_data(words)[depth], (vector_size(words) == (depth+1)));
	transaction_logprintf(cs->ts, D_DEBUG, "BAD_WORD_TREE [%s] Added at level %d", (char*)vector_data(words)[depth], depth);
	vector_append(bwts, new_tree);
	if (vector_size(words) > (depth+1))
		bad_word_insert(cs, new_tree->expanded, words, depth+1);
}

/* insert a "multiple word" bad word in the tree
 * This may do nothing if the whole word already exists*/
void bad_word_tree_append(struct cmd *cs, vector_t* bwts, vector_t* words, int depth){
	int i=0;
	int options = vector_size(bwts);
	for(i=0; i<options; i++){
        if (strcasecmp(((bad_word_tree_t*)vector_data(bwts)[i])->word, vector_data(words)[depth])==0){
	        transaction_logprintf(cs->ts, D_DEBUG, "BAD_WORD_TREE [%s] Exists at level %d", ((bad_word_tree_t*)vector_data(bwts)[i])->word, depth);
			if (depth+1 != vector_size(words))
				bad_word_tree_append(cs, ((bad_word_tree_t*)vector_data(bwts)[i])->expanded, words, depth+1);
			return;
        }
    }
	bad_word_insert(cs, bwts, words, depth);
}

/* frees the bad_word_tree_t allocated space */
void bad_word_tree_delete(void* bwt){
	if (bwt){
		bad_word_tree_t* tree = (bad_word_tree_t*)bwt;
		free(tree->word);
		vector_delete(tree->expanded, bad_word_tree_delete);
		free(tree);
	}
}
