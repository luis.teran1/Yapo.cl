#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "trans.h"
#include "util.h"
#include "command.h"

/* Testing required var */
static struct validator v_test_required[] = {
	{0}
};
ADD_VALIDATOR(v_test_required);

/* Testing neg-regexp */
#define ERROR_KAKA_NOTMISSING	"43"
static struct validator v_test_nregex_kaka[] = {
	VALIDATOR_NREGEX("kaka", 0, ERROR_KAKA_NOTMISSING),
	{0}
};
ADD_VALIDATOR(v_test_nregex_kaka);

/* Testing regexp */
#define ERROR_KAKA_NOTKAKA	"44"
static struct validator v_test_regex_kaka[] = {
	VALIDATOR_REGEX("kaka", 0, ERROR_KAKA_NOTKAKA),
	{0}
};
ADD_VALIDATOR(v_test_regex_kaka);

/* Testing required execute */
#define ERROR_REQ_EXE_MISSING	"45"
static struct validator v_test_req_exe[] = {
	VALIDATOR_REQUIRED_EXECUTION(ERROR_REQ_EXE_MISSING),
	{0}
};
ADD_VALIDATOR(v_test_req_exe);

/* Testing required validation */
#define ERROR_REQ_VAL_MISSING	"46"
static struct validator v_test_req_val[] = {
	VALIDATOR_REQUIRED_VALIDATION(ERROR_REQ_VAL_MISSING),
	{0}
};
ADD_VALIDATOR(v_test_req_val);


/* Testing validator function */
#define ERROR_FUNC "49"
static int 
test_func_bad_word(struct cmd_param *param, const char *arg) {
	if (strcmp(param->value, "bad") == 0) {
		param->error = ERROR_FUNC;
                param->cs->status = "TRANS_ERROR";
	}

        param->cs->validator_cb(param->cs);
	return 0;
}

static struct validator v_test_func_badstring[] = {
	VALIDATOR_FUNC(test_func_bad_word),
	{0}
};
ADD_VALIDATOR(v_test_func_badstring);

/* Length validator */
#define ERROR_NAME_GOOD_LEN "50"
static struct validator v_test_len_string[] = {
	VALIDATOR_LEN(2, 5, ERROR_NAME_GOOD_LEN),
	{0}
};
ADD_VALIDATOR(v_test_len_string);

/* Length validator */
#define ERROR_NUMBER_NEG "51"
static struct validator v_test_int_test[] = {
	VALIDATOR_INT(0, 100, ERROR_NUMBER_NEG),
	{0}
};
ADD_VALIDATOR(v_test_int_test);

/* Testing sub validators*/
static struct validator v_test_sub_len_regex_kaka[] = {
	VALIDATOR_SUB("v_test_len_string"),
	VALIDATOR_SUB("v_test_regex_kaka"),
	{0}
};
ADD_VALIDATOR(v_test_sub_len_regex_kaka);

/* Dynamic XOR operator. */
static int
vf_test_dynxor(struct cmd_param *param, const char *arg) {
	struct d_param *dparam = zmalloc(sizeof (*dparam));

	dparam->name = xstrdup("dynxor1;dynxor2");
	dparam->flags = P_XOR;

	cmd_dyn_param(param->cs, dparam);

	return 0;
}

static struct validator v_test_dynxor[] = {
	VALIDATOR_FUNC(vf_test_dynxor),
	{0}
};
ADD_VALIDATOR(v_test_dynxor);


static struct c_param parameters[] = {
	{"required",	P_REQUIRED,	"v_test_required"},
	{"required_exe",	0,	"v_test_req_exe"},
	{"required_val",	0,	"v_test_req_val"},
	{"nregex_kaka",	0,	"v_test_nregex_kaka"},
	{"regex_kaka",	0,	"v_test_regex_kaka"},
	{"func_badstring",	0,	"v_test_func_badstring"},
	{"len_string",	0,	"v_test_len_string"},
	{"int_test",	0,	"v_test_int_test"},
	{"sub_len_regex_kaka",	0,	"v_test_sub_len_regex_kaka"},
	{"remote_addr", P_MULTI,      "v_ip"},
	{"print",       P_MULTI,      "v_string_all"},
	{"printblob",   0      ,      "v_string_all_blob"},
	{"dynxor",      0      , "v_test_dynxor"},
	{NULL}
};

static void
doit(struct cmd *cs)
{
	cmd_done(cs);
}

ADD_COMMAND(test, NULL, doit, parameters, NULL);
