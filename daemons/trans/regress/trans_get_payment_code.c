#include "factory.h"

static struct c_param parameters[] = {
	{"ad_id",    P_REQUIRED,  "v_id"},
	{NULL, 0}
};

FACTORY_TRANSACTION(get_payment_code, &config.pg_slave, "sql/get_payment_code.sql", NULL, parameters, FACTORY_NO_RES_CNTR);
