#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <time.h>

#include "trans.h"
#include "util.h"
#include <ctemplates.h>
#include "validators.h"
#include <bconf.h>
#include <event.h>

typedef enum {
	PGEXAMPLE_DONE,
	PGEXAMPLE_ERROR,
	PGEXAMPLE_INIT,
	PGEXAMPLE_STATE1,
	PGEXAMPLE_STATE2,
	PGEXAMPLE_STATE3
} state_t;

static const char *state_lut[] = {
	"PGEXAMPLE_DONE",
	"PGEXAMPLE_ERROR",
	"PGEXAMPLE_INIT",
	"PGEXAMPLE_STATE1",
	"PGEXAMPLE_STATE2",
	"PGEXAMPLE_STATE3"
};

struct pgexample_state {
	state_t state;
        char *sql;
	struct cmd *cs;
        struct event timeout_event;
};

static struct c_param parameters[] = {
	{NULL, 0}
};

static const char *pgexample_fsm(void *, struct sql_worker *, const char *);

extern struct bconf_node *bconf_root;

/*****************************************************************************
 * pgexample_done
 * Exit function.
 **********/
static void
pgexample_done(void *v) {
	struct pgexample_state *state = v;
	struct cmd *cs = state->cs;
	free(state);
	cmd_done(cs);
}

/*****************************************************************************
 * bind_insert
 * Initialise template API before a call to sql_worker_template_query().
 **********/
static void
bind_insert(struct bpapi *ba, struct pgexample_state *state) {
	struct c_param *param;

	for (param = parameters; param->name; param++) {
		if (cmd_haskey(state->cs, param->name))
			bpapi_insert(ba, param->name, cmd_getval(state->cs, param->name));
	}
}

/*****************************************************************************
 * pgexample async call (pgexample_async_timeout and async_timeout_cb)
 * Perform a async call and return into the state machine.
 **********/
static void
async_timeout_cb(int i, short s, void *v) {
        struct pgexample_state *state = v;

        evtimer_del(&state->timeout_event);
        pgexample_fsm(state, NULL, NULL);
}

static void
pgexample_async_timeout(struct pgexample_state *state) {
        struct timeval tv;

	if (bconf_get_int (bconf_root, "*.pgexample.hang_forever"))
		return; /* Returning here without a new cb makes the command stuck forever */
        tv.tv_sec = 1;
        tv.tv_usec = 0;
        evtimer_set(&state->timeout_event, async_timeout_cb, state);
        evtimer_add(&state->timeout_event, &tv);
}

/*****************************************************************************
 * pgexample_fsm
 * State machine
 **********/
static const char *
pgexample_fsm(void *v, struct sql_worker *worker, const char *errstr) {
	struct pgexample_state *state = v;
	struct cmd *cs = state->cs;
        struct bpapi ba;

        /*
         * Clean up pending data
         */
	if (state->sql) {
		free(state->sql);
		state->sql = NULL;
	}

	/*
	 * General error handling for db errors.
	 */
	if (errstr) {
                transaction_logprintf(cs->ts, D_ERROR, "pgexample_fsm statement failed (%s(%d), %s)",
				      state_lut[state->state],
                                      state->state,
                                      errstr);
                cs->status = "TRANS_DATABASE_ERROR";
                cs->message = xstrdup(errstr);
		state->state = PGEXAMPLE_ERROR;
        }

	while (1) {
		transaction_logprintf(cs->ts, D_DEBUG, "pgexample_fsm(%p, %p, %p), state=%s(%d)",
				      v, worker, errstr, state_lut[state->state], state->state);

		switch (state->state) {
		case PGEXAMPLE_INIT:
			/* next state */
			state->state = PGEXAMPLE_STATE1;

			/* Initialise the templateparse structure */
			BPAPI_INIT(&ba, NULL, pgsql);
			bind_insert(&ba, state);

			/* Register SQL query with our pgexample_fsm() as callback function */
			/* Make sure to switch to slave if read-only query. */
			sql_worker_template_query(&config.pg_master,
						  "sql/pgexample.sql", &ba,
						  pgexample_fsm, state, cs->ts->log_string);

			/* Clean-up */
			bpapi_output_free(&ba);
			bpapi_simplevars_free(&ba);
			bpapi_vtree_free(&ba);

			/* Return to libevent */
			return NULL;

                case PGEXAMPLE_STATE1:
                        state->state = PGEXAMPLE_STATE2;
                        transaction_logprintf(cs->ts, D_INFO, "Got a result: %d rows",
                                              worker->rows(worker));
                        return NULL;

                case PGEXAMPLE_STATE2:
                        state->state = PGEXAMPLE_STATE3;
                        return NULL;

                case PGEXAMPLE_STATE3:
                        state->state = PGEXAMPLE_DONE;

                        while (worker->next_row(worker)) {
                                transaction_printf(cs->ts, "debug:reading %s\n",
                                                   worker->get_value(worker, 0));
                                transaction_logprintf(cs->ts, D_INFO, "Got %s in %s",
                                                      worker->get_value(worker, 0),
                                                      worker->field_name(worker, 0));
                        }

			pgexample_async_timeout(state);

			return NULL;

		case PGEXAMPLE_DONE:
			pgexample_done(state);
			return NULL;

		case PGEXAMPLE_ERROR:
			transaction_logprintf(cs->ts, D_ERROR, "Error state");
			pgexample_done(state);
			return NULL;

		default:
			/* MAJOR STATE ERROR */
			cs->status = "TRANS_ERROR";
			transaction_logprintf(cs->ts, D_ERROR, "Unknown state (%d)",
					      state->state);

			pgexample_done(state); /* If this state were run there would be a memory leak */
			return NULL;

		}
	}
}

/*****************************************************************************
 * pgexample
 * Main function that initialise a state-structure and calls the state machine.
 **********/
static void
pgexample(struct cmd *cs) {
	transaction_logprintf(cs->ts, D_DEBUG, "Start pgexample transaction");
	struct pgexample_state *state;
	state = zmalloc(sizeof(*state));
	state->cs = cs;
	state->state = PGEXAMPLE_INIT;

	pgexample_fsm(state, NULL, NULL);
}

ADD_COMMAND(pgexample,     /* Name of command */
		NULL,
            pgexample,     /* Main function name */
            parameters,  /* Parameters struct */
            "pgexample command");/* Command description */
