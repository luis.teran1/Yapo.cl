#include "factory.h"

static struct c_param parameters[] = {
       {"db_name",    P_REQUIRED,  "v_name"},
       {NULL, 0}
};

static struct factory_data data = {
       "db_exists",
       FACTORY_NO_RES_CNTR
};

static const struct factory_action actions[] = {
       FA_SQL(NULL, "pgsql.slave", "sql/db_exists.sql", FASQL_OUT),
       {0}
};

FACTORY_TRANS(db_exists, parameters, data, actions);
