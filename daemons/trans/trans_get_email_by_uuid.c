#include "factory.h"

static struct c_param parameters[] = {
	{"uuid",	P_REQUIRED,	"v_uuid"},
	{NULL, 0}
};

static const struct factory_action actions[]= {
	FA_SQL("", "pgsql.slave", "sql/get_email_by_uuid.sql", FASQL_OUT),
	{0}
};

static struct factory_data data = {
	"get_email_by_uuid",
	FACTORY_NO_RES_CNTR | FACTORY_RES_SHORT_FORM
};

FACTORY_TRANS(get_email_by_uuid, parameters, data, actions);
