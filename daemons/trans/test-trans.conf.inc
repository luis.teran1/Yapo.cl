# Configuration file for trans

bconfd =  %DESTDIR%/conf/bconf.conf
blocket_id = %USER%

db.pgsql.master.engine = PGSQL
db.pgsql.master.name = blocketdb
db.pgsql.master.hostname = localhost
db.pgsql.master.user = %USER%
db.pgsql.master.password = 
db.pgsql.master.socket = %REGRESS_PGSQL_HOST%
db.pgsql.master.options = -c search_path=bpv,public

db.pgsql.slave.engine = PGSQL
db.pgsql.slave.name = blocketdb
db.pgsql.slave.hostname = localhost
db.pgsql.slave.user = %USER%
db.pgsql.slave.password = 
db.pgsql.slave.socket = %REGRESS_PGSQL_HOST%
db.pgsql.slave.options = -c search_path=bpv,public

db.pgsql.queue.engine = PGSQL
db.pgsql.queue.name = blocketdb
db.pgsql.queue.hostname = localhost
db.pgsql.queue.user = %USER%
db.pgsql.queue.password = 
db.pgsql.queue.socket = %REGRESS_PGSQL_HOST%
db.pgsql.queue.options = -c search_path=bpv,public

port = %REGRESS_TRANS_PORT%
failover_port = %REGRESS_TRANS_FPORT%
control_port = %REGRESS_TRANS_CPORT%
control_failover_port = %REGRESS_TRANS_CFPORT%
log_level=info
log_tag=trans

geoip.db.required=1
geoip.db.directory=%TOPDIR%/share/geo_ip

basedir = %BDIR%/regress/regress/trans

rabbitmq.enabled=1
