#include "factory.h"
#include "bpapi.h"
#include <bconf.h>
#include "fd_pool.h"
#include "sredisapi.h"
#include "utils/redis_trans_context.h"

static struct c_param parameters[] = {
	{"list_id",         P_REQUIRED, "v_list_id"},
	{"recipient_email", P_REQUIRED, "v_email"},
	{"account_email",   P_REQUIRED, "v_email"},
	{"token",           P_OPTIONAL, "v_token:adminad.bump"},
	{NULL, 0}
};

static struct factory_data data = {
	NULL,
	FACTORY_NO_RES_CNTR | FACTORY_RES_SHORT_FORM | FACTORY_EMPTY_RESULT_OK
};

static const struct factory_action query_payment = FA_SQL(NULL, "pgsql.slave", "sql/has_user_paid_for_an_ad.sql", 0);
static const struct factory_action done = FA_DONE();

static const struct factory_action *
query_for_payment(struct cmd *cs, struct bpapi *ba) {
	if (cmd_haskey(cs, "token")) {
		bpapi_insert(ba, "user_has_paid", "t");
		return NULL;
	}

	return &query_payment;
};

static const struct factory_action *
check_user_paid(struct cmd *cs, struct bpapi *ba) {
	const char *user_has_paid = bpapi_get_element(ba, "user_has_paid", 0);
	if (user_has_paid && !strcmp(user_has_paid, "t"))
		return NULL;

	cs->status = "TRANS_ERROR";
	cs->error = "JOBS_AR_REPORT_ERROR_USER_HAS_NOT_PAID_FOR_AN_AD";
	return &done;
}

static const struct factory_action actions[] = {
	FA_COND(query_for_payment),
	FA_COND(check_user_paid),
	FA_SQL(NULL, "pgsql.slave", "sql/call_get_jobs_ad_adreply_report.sql", 0),
	FA_MAIL("mail/jobs_ad_adreply_report.mime"),
	FA_DONE()
};

FACTORY_TRANS(send_jobs_ad_adreply_report, parameters, data, actions);
