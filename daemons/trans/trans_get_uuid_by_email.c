#include "factory.h"

static struct c_param parameters[] = {
	{"email",	P_REQUIRED,	"v_email"},
	{NULL, 0}
};

static const struct factory_action actions[]= {
	FA_SQL("", "pgsql.master", "sql/get_uuid_by_email.sql", FASQL_OUT),
	{0}
};

static struct factory_data data = {
	"get_uuid_by_email",
	FACTORY_NO_RES_CNTR | FACTORY_RES_SHORT_FORM
};

FACTORY_TRANS(get_uuid_by_email, parameters, data, actions);
