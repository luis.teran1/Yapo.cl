
#include <stdlib.h>

#include "trans.h"

#include <bpapi.h>
#include <ctemplates.h>

#include "sql_worker.h"
#include "mail_log.h"

#define MAIL_LOG_GET_ID 1

static const char *
mail_log_cb(void *v, struct sql_worker *worker, const char *errstr) {
	struct mail_log *log = v;
	int count;
	struct bpapi ba;

	/* Check if there was an error in last sql */
	if (errstr != NULL) {
		log->cb(log, -1, errstr);
		return NULL;
	}

	if (log->state == MAIL_LOG_GET_ID) {
		int id;

		worker->next_row(worker);
		id = atoi(worker->get_value(worker, 0));
		log->cb(log, id, NULL);
	} else if (worker->next_row(worker)) {
		/* Assume spam select */
		int ncol = worker->fields(worker);
		int col;

		for (col = 0; col < ncol; col++) {
			if (worker->is_null(worker, col))
				continue;

			count = atoi(worker->get_value(worker, col));
			/* Check if the user has sent more than 10 tips in 10 minutes */
			if (count >= log->spam_counts[col]) {
				log->cb(log, -1, "ERROR_SPAM");
				return NULL;
			}
		}

		if (log->check_only) {
			log->cb(log, -1, NULL);
			return NULL;
		}

		BPAPI_INIT(&ba, NULL, pgsql);
		bpapi_insert(&ba, "type", log->type);
		if (log->email)
			bpapi_insert(&ba, "email", log->email);
		if (log->remote_addr)
			bpapi_insert(&ba, "remote_addr", log->remote_addr);
		if (log->list_id)
			bpapi_set_int(&ba, "list_id", log->list_id);

		sql_worker_template_newquery(worker, "sql/mail_log_insert.sql", &ba);
		bpapi_output_free(&ba);
		bpapi_simplevars_free(&ba);
		bpapi_vtree_free(&ba);
	} else {
		/* Assume insert */
		log->state = MAIL_LOG_GET_ID;
	}

	return NULL;
}

static const char *const get_counts[] = {"get_send", "get_recv", "get_ad"};

void
log_mail(struct mail_log *log) {
	struct bpapi ba;
	int i;
	const char *tmpl = (log->check_only ? "sql/mail_log.sql" : "sql/mail_log_insert.sql");
	
	BPAPI_INIT(&ba, NULL, pgsql);
	bpapi_insert(&ba, "type", log->type);
	if (log->remote_addr)
		bpapi_insert(&ba, "remote_addr", log->remote_addr);
	if (log->email)
		bpapi_insert(&ba, "email", log->email);
	if (log->list_id)
		bpapi_set_int(&ba, "list_id", log->list_id);

	bpapi_set_int(&ba, "minutes", log->spam_minutes);

	for (i = 0; i < MAIL_SPAM_NUM; i++) {
		if (log->spam_counts[i]) {
			bpapi_insert(&ba, get_counts[i], "1");
			/* Make the check instead of insert. */
			tmpl = "sql/mail_log.sql";
		}
	}

	sql_worker_template_query(&config.pg_master, tmpl, &ba,
			 	  mail_log_cb, log, "mail_log"); 

	bpapi_output_free(&ba);
	bpapi_simplevars_free(&ba);
	bpapi_vtree_free(&ba);
}
