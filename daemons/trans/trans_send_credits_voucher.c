#include "factory.h"
#include "bpapi.h"
#include <bconf.h>

extern struct bconf_node *bconf_root;

static struct c_param parameters[] = {
    {"from",            0,         "v_email_basic"},
    {"to",              0,          "v_email_basic"},
    {"product_ids",     P_REQUIRED, "v_multiple_product_ids"},
    {"ad_names",        P_REQUIRED, "v_multiple_ad_names"},
    {"credits_used",    P_REQUIRED, "v_integer"},
    {"credits_balance", P_REQUIRED, "v_integer"},
    {"oid",             P_REQUIRED, "v_integer"},
    {"hash",            P_REQUIRED, "v_account_hash"},
    {NULL, 0}
};

static struct factory_data data = {
    "mail"
};

static const struct factory_action actions[] = {
    FA_MAIL("mail/send_credits_voucher.mime"),
    {0}
};

FACTORY_TRANS(send_credits_voucher, parameters, data, actions);
