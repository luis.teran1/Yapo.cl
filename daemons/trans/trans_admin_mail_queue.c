#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <time.h>

#include "trans.h"
#include "util.h"
#include <ctemplates.h>
#include "validators.h"
#include <bconf.h>
#include <event.h>

#define ADMIN_MAIL_QUEUE_DONE		0
#define ADMIN_MAIL_QUEUE_ERROR		1
#define ADMIN_MAIL_QUEUE_INIT		2
#define ADMIN_MAIL_QUEUE_UPDATE		3
#define ADMIN_MAIL_QUEUE_UPDATE_DONE	4
#define ADMIN_MAIL_QUEUE_GET		5
#define ADMIN_MAIL_QUEUE_RESULT		6

struct admin_mail_queue_state {
	int state;
	const char *action;
	struct cmd *cs;
};

static struct c_param parameters[] = {
	{"mail_queue_id",	P_REQUIRED,	"v_integer"},
	{"action",		P_REQUIRED,	"v_array:get,update"},
	{"state",		0,		"v_array:sent,abuse,reg,deleted,manual"},
	{NULL, 0}
};

static const char *admin_mail_queue_fsm(void *, struct sql_worker *, const char *);

extern struct bconf_node *bconf_root;

/*****************************************************************************
 * admin_mail_queue_done
 * Exit function.
 **********/
static void
admin_mail_queue_done(void *v) {
	struct admin_mail_queue_state *state = v;
	struct cmd *cs = state->cs;
	free(state);
	cmd_done(cs);
}

/*****************************************************************************
 * bind_insert
 * Initialise template API before a call to sql_worker_template_query().
 **********/
static void
bind_insert(struct bpapi *ba, struct admin_mail_queue_state *state) {
	struct c_param *param;

	for (param = parameters; param->name; param++) {
		if (cmd_haskey(state->cs, param->name))
			bpapi_insert(ba, param->name, cmd_getval(state->cs, param->name));
	}
}

/*****************************************************************************
 * admin_mail_queue_fsm
 * State machine
 **********/
static const char *
admin_mail_queue_fsm(void *v, struct sql_worker *worker, const char *errstr) {
	struct admin_mail_queue_state *state = v;
	struct cmd *cs = state->cs;
        struct bpapi ba;

	/* 
	 * General error handling for db errors.
	 */
	if (errstr) {
                transaction_logprintf(cs->ts, D_ERROR, "admin_mail_queue_fsm statement failed (%d, %s)", 
                                      state->state,
                                      errstr);
                cs->status = "TRANS_DATABASE_ERROR";
                cs->message = xstrdup(errstr);
		state->state = ADMIN_MAIL_QUEUE_ERROR;
        }

	while (1) {
		transaction_logprintf(cs->ts, D_DEBUG, "admin_mail_queue_fsm(%p, %p, %p), state=%d", 
				      v, worker, errstr, state->state);

		switch (state->state) {
		case ADMIN_MAIL_QUEUE_INIT:
			if (strcmp(state->action, "get") == 0)
				state->state = ADMIN_MAIL_QUEUE_GET;
			else if (strcmp(state->action, "update") == 0)
				state->state = ADMIN_MAIL_QUEUE_UPDATE;
			else
				state->state = ADMIN_MAIL_QUEUE_DONE;
			continue;

		case ADMIN_MAIL_QUEUE_GET:
			/* next state */
			state->state = ADMIN_MAIL_QUEUE_RESULT;

			/* Initialise the templateparse structure */
			BPAPI_INIT_APP(&ba, host_bconf, "controlpanel", buf, pgsql);
			bind_insert(&ba, state);

			/* Register SQL query with our admin_mail_queue_fsm() as callback function */
			/* Make sure to switch to slave if read-only query. */
			sql_worker_template_query(&config.pg_slave, 
						"sql/get_mail_queue.sql", &ba, 
						admin_mail_queue_fsm, state, cs->ts->log_string);

			/* Clean-up */
			bpapi_output_free(&ba);
			bpapi_simplevars_free(&ba);
			bpapi_vtree_free(&ba);

			/* Return to libevent */
			return NULL;

                case ADMIN_MAIL_QUEUE_RESULT:
		{
			int i, j;
			state->state = ADMIN_MAIL_QUEUE_DONE;
			transaction_logoff(cs->ts);

			i = 0;
			while (worker->next_row(worker)) {
				for (j = 0; worker->get_value(worker, j); j++) {

					if (strlen(worker->get_value(worker, j))) {
						const char *val = worker->get_value(worker, j);
						if (strchr(val, '\n')) {
							transaction_printf(cs->ts, "blob:%ld:result.%d.%s\n%s\n",
									   (unsigned long)strlen(val),
									   i,
									   worker->field_name(worker, j),
									   val);

						} else {
							transaction_printf(cs->ts, "result.%d.%s:%s\n",
									   i,
									   worker->field_name(worker, j),
									   val);
						}
					}
				}
				i++;
			}

			transaction_logon(cs->ts);
			continue;
		}

		case ADMIN_MAIL_QUEUE_UPDATE:
			state->state = ADMIN_MAIL_QUEUE_UPDATE_DONE;
			BPAPI_INIT_APP(&ba, host_bconf, "controlpanel", buf, pgsql);
			bind_insert(&ba, state);
			sql_worker_template_query_flags(&config.pg_master, SQLW_FINAL_CALLBACK,
				"sql/update_mail_queue.sql", &ba,
				admin_mail_queue_fsm, state, cs->ts->log_string);
			bpapi_output_free(&ba);
			bpapi_simplevars_free(&ba);
			bpapi_vtree_free(&ba);
			return NULL;

		case ADMIN_MAIL_QUEUE_UPDATE_DONE:
			if (worker->sw_state == RESULT_DONE) {
				state->state = ADMIN_MAIL_QUEUE_DONE;
				continue;
			}
			worker->next_row(worker);
			return NULL;

		case ADMIN_MAIL_QUEUE_DONE:
			admin_mail_queue_done(state);
			return NULL;

		case ADMIN_MAIL_QUEUE_ERROR:
			transaction_logprintf(cs->ts, D_ERROR, "Error state");
			admin_mail_queue_done(state);
			return NULL;

		default:
			/* MAJOR STATE ERROR */
			cs->status = "TRANS_ERROR";
			transaction_logprintf(cs->ts, D_ERROR, "Unknown state (%d)", 
					      state->state);
			return NULL;

		}
	}
}

/*****************************************************************************
 * admin_mail_queue
 * Main function that initialise a state-structure and calls the state machine.
 **********/
static void
admin_mail_queue(struct cmd *cs) {
	transaction_logprintf(cs->ts, D_DEBUG, "Start Admin_mail_queue transaction");
	struct admin_mail_queue_state *state;
	state = zmalloc(sizeof(*state));
	state->cs = cs;
	state->action = cmd_getval(cs, "action");
	state->state = ADMIN_MAIL_QUEUE_INIT;

	admin_mail_queue_fsm(state, NULL, NULL);
}

ADD_COMMAND(admin_mail_queue,     /* Name of command */
		NULL,
            admin_mail_queue,     /* Main function name */
            parameters,  /* Parameters struct */
            "Admin_mail_queue command");/* Command description */
