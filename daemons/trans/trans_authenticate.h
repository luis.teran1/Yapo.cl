#ifndef TRANS_AUTHENTICATE_H
#define TRANS_AUTHENTICATE_H

struct auth_state {
	int state;
	struct cmd *cs;
	struct cmd_param *param;
	const char *req_privs;
	const char *req_store_id;
};

char *create_token(int uid);

#endif
