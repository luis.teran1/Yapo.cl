#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <time.h>

#include "trans.h"
#include "util.h"
#include <ctemplates.h>
#include "validators.h"
#include <bconf.h>
#include <event.h>

#define DELETE_IMAGES_DONE   0
#define DELETE_IMAGES_ERROR  1
#define DELETE_IMAGES_INIT   2
#define DELETE_IMAGES_DELETE 3

extern struct bconf_node *bconf_root;
struct strlist {
	char *str;
	struct strlist *next;
};

struct delete_images_state {
	int state;
	int count;
	struct cmd *cs;
	struct strlist *del_list;
	struct strlist *del_list_tail;
};

static struct c_param parameters[] = {
	{NULL, 0}
};

static const char *delete_images_fsm(void *, struct sql_worker *, const char *);


/****************************************************************************
 * imgdel_done_cb
 * Serialize dav functions by waiting for this one before continuing
 *********/

static void
imgdel_done_cb(struct internal_cmd *cmd, const char *line, void *data) {
	if (!line) {
		if (data)
			delete_images_fsm(data,NULL,NULL);
		else
			DPRINTF(D_ERROR, ("(CRIT) imgdel_done_cb no line and no data"));
	}
	return;	
}

/*****************************************************************************
 * delete_images_done
 * Exit function.
 **********/

static void
delete_images_done(void *v) {
	struct delete_images_state *state = v;
	struct cmd *cs = state->cs;
	free(state);
	cmd_done(cs);
}

/*****************************************************************************
 * delete_images_fsm
 * State machine
 **********/
static const char *
delete_images_fsm(void *v, struct sql_worker *worker, const char *errstr) {
	struct delete_images_state *state = v;
	struct cmd *cs = state->cs;
        struct bpapi ba;

	/*
	 * General error handling for db errors.
	 */
	if (errstr) {
                transaction_logprintf(cs->ts, D_ERROR, "delete_images_fsm statement failed (%d, %s)",
                                      state->state,
                                      errstr);
                cs->status = "TRANS_DATABASE_ERROR";
                cs->message = xstrdup(errstr);
		state->state = DELETE_IMAGES_ERROR;
	}

	while (1) {
		transaction_logprintf(cs->ts, D_DEBUG, "delete_images_fsm(%p, %p, %p), state=%d",
				      v, worker, errstr, state->state);

		switch (state->state) {
		case DELETE_IMAGES_INIT:
		{
			const char *delete_delay =  bconf_get_string(bconf_root, "*.*.common.image.delete_delay");

			if (!delete_delay) {
				cs->status = "TRANS_BCONF_KEY_MISSING";
				cs->message = xstrdup("*.*.common.image.delete_delay");
				state->state = DELETE_IMAGES_ERROR;
				continue;
			}
			state->del_list_tail = state->del_list = NULL;
			state->count=0;

			state->state = DELETE_IMAGES_DELETE;

			BPAPI_INIT(&ba, NULL, pgsql);
			bpapi_insert(&ba, "delete_delay", delete_delay);

			sql_worker_template_query(&config.pg_slave,
						  "sql/delete_images.sql", &ba,
						  delete_images_fsm, state, cs->ts->log_string);

			bpapi_output_free(&ba);
			bpapi_simplevars_free(&ba);
			bpapi_vtree_free(&ba);

			return NULL;
		}
		case DELETE_IMAGES_DELETE:
		{

                        while (worker && worker->next_row(worker)) {
				const char *image_id = worker->get_value(worker, 0);
				if (state->del_list) {
					struct strlist *elem = zmalloc(sizeof(*elem));
					elem->str=xstrdup(image_id);
					elem->next=NULL;
					state->del_list_tail->next = elem;
					state->del_list_tail = elem;
				} else {
					state->del_list = zmalloc(sizeof(*state->del_list));
					state->del_list->str=xstrdup(image_id);
					state->del_list->next=NULL;
					state->del_list_tail=state->del_list;
				}
			}
			if (state->del_list) {
				struct internal_cmd *cmd = zmalloc(sizeof (*cmd));
				struct strlist *delem = state->del_list;
				const char *image_id = state->del_list->str;
				/* XXX when image_id is no longer a bigint that needs to be zeropadded, remove this */
				char image_name[11];
				image_name[10]=0;
				memset(image_name, (char) '0',10);
				memcpy(&image_name[10-strlen(image_id)],image_id,strlen(image_id));
				
				xasprintf(&cmd->cmd_string,
					  "cmd:imgdel\n"
					  "log_string:%s\n"
					  "commit:1\n"
					  "image_id:%s\n"
					  "filename:%s.jpg\n",
					  state->cs->ts->log_string,
					  image_id,
					  image_name);
			
				if (state->del_list->next) {
					state->del_list = state->del_list->next;
					free(delem->str);
					free(delem);
				} else {
					free(state->del_list->str);
					free(state->del_list);
					state->del_list=NULL;
				}	
				cmd->cb = imgdel_done_cb;
				cmd->data = state;
				transaction_internal(cmd);
				state->count++;
				return NULL;
                        } else {
                        	state->state = DELETE_IMAGES_DONE;
			}

			transaction_printf(cs->ts, "deleted:%d\n", state->count);

			continue;
		}
		case DELETE_IMAGES_DONE:
			delete_images_done(state);
			return NULL;

		case DELETE_IMAGES_ERROR:
			transaction_logprintf(cs->ts, D_ERROR, "Error state");
			delete_images_done(state);
			return NULL;

		default:
			/* MAJOR STATE ERROR */
			cs->status = "TRANS_ERROR";
			transaction_logprintf(cs->ts, D_ERROR, "Unknown state (%d)",
					      state->state);
			return NULL;

		}
	}
}

/*****************************************************************************
 * delete_images
 * Main function that initialise a state-structure and calls the state machine.
 **********/
static void
delete_images(struct cmd *cs) {
	transaction_logprintf(cs->ts, D_DEBUG, "Start Delete_images transaction");
	struct delete_images_state *state;
	state = zmalloc(sizeof(*state));
	state->cs = cs;
	state->state = DELETE_IMAGES_INIT;

	delete_images_fsm(state, NULL, NULL);
}

ADD_COMMAND(delete_images,     /* Name of command */
	    NULL,
            delete_images,     /* Main function name */
            parameters,  /* Parameters struct */
            "Delete_images command");/* Command description */
