#include "factory.h"

static struct c_param parameters[] = {
	{"date", P_REQUIRED | P_MULTI, "v_string_isprint"},
	{"page_type", 0|P_MULTI, "v_string_isprint"},
	{"user_region", 0|P_MULTI, "v_integer"},
	{"user_category", 0|P_MULTI, "v_integer"},
	{"page_region", 0|P_MULTI, "v_integer"},
	{"page_category", 0|P_MULTI, "v_integer"},
	{"extra_data", 0|P_MULTI, "v_integer"},
	{"pageviews", 0|P_MULTI, "v_integer"},
	{NULL, 0}
};

FACTORY_TRANSACTION(insert_pageviews_per_reg_cat, &config.pg_master, "sql/insert_pageviews_per_reg_cat.sql", NULL, parameters, FACTORY_NO_RES_CNTR | FACTORY_EMPTY_RESULT_OK | FACTORY_RES_SHORT_FORM );
