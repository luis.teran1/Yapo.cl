#include "factory.h"

static struct c_param parameters[] = {
	{NULL, 0}
};

FACTORY_TRANSACTION(sold_items_on_interval, &config.pg_slave,
	"sql/sold_items_on_interval.sql", NULL, parameters,
	FACTORY_NO_RES_CNTR | FACTORY_EMPTY_RESULT_OK | FACTORY_RES_SHORT_FORM );

