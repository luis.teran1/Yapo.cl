#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <time.h>

#include "trans.h"
#include "util.h"
#include "validators.h"
#include <ctemplates.h>
#include <bconf.h>

typedef enum {
	UPDATE_STATS_DONE,
	UPDATE_STATS_INIT,
	UPDATE_STATS_UPDATE,
	UPDATE_STATS_CHECK_RESULT,
	UPDATE_STATS_ERROR
	
} update_state_t;

const char* update_stats_state_lut[] = {
	"UPDATE_STATS_DONE",
	"UPDATE_STATS_INIT",
	"UPDATE_STATS_UPDATE",
	"UPDATE_STATS_CHECK_RESULT",
	"UPDATE_STATS_ERROR"
};
extern struct bconf_node *bconf_root;


struct update_stats_state {
	update_state_t state;
	struct cmd *cs;
};


static int
vf_stat_name(struct cmd_param *param, const char *arg) {
	const char *stat_type = bconf_value(bconf_vget(bconf_root, "*.*.stats", param->value, "type", NULL));

	if (!stat_type) {
		param->cs->status = "TRANS_ERROR";
		param->error = "ERROR_UNKNOWN_STAT_TYPE";
		return 1;
	}

	return 0;
}

struct validator v_stat_name[] = {
        VALIDATOR_REGEX("^[[:alnum:]_]+$", REG_EXTENDED, "ERROR_STAT_NAME_INVALID"),
	VALIDATOR_FUNC(vf_stat_name),
	{0}
};
ADD_VALIDATOR(v_stat_name);

struct validator v_stat_name_suffix[] = {
        VALIDATOR_REGEX("^[[:alnum:]_]+$", REG_EXTENDED, "ERROR_STAT_NAME_SUFFIX_INVALID"),
	{0}
};
ADD_VALIDATOR(v_stat_name_suffix);

struct validator v_stat_value[] = {
        VALIDATOR_REGEX("^[[:alnum:].,+(): -]+$", REG_EXTENDED, "ERROR_STAT_VALUE_INVALID"),
	{0}
};
ADD_VALIDATOR(v_stat_value);

static struct c_param update_stats_parameters[] = {
	{"stat_name", 		P_REQUIRED,	"v_stat_name"},
	{"stat_name_suffix",	0,	"v_stat_name_suffix"},
	{"stat_value", 		0,	"v_stat_value"},
	{"stat_time", 		0,	"v_timestamp"},
	{"last_stat_time", 	0,	"v_timestamp"},
	{"is_delta", 		0,	"v_bool"},
	{"lang",		0,	"v_lang"},
	{NULL, 0, NULL}
};



static void update_stats_done(void *v) {
	struct update_stats_state *state = v;
	struct cmd *cs = state->cs;

	free(state);
	cmd_done(cs);
}

static void
bind_insert(struct bpapi *ba, struct update_stats_state *state) {
	struct c_param *param;

	for (param = update_stats_parameters; param->name; param++) {
		if (cmd_haskey(state->cs, param->name) && !bpapi_has_key(ba, param->name))
			bpapi_insert(ba, param->name, cmd_getval(state->cs, param->name));
	}
}

static const char *
update_stats_fsm(void *v, struct sql_worker *worker, const char *errstr) {
	struct update_stats_state *state = v;
	struct cmd *cs = state->cs;
        struct bpapi ba;

	transaction_logprintf(cs->ts, D_DEBUG, "update_stats fsm");

	if (state->state == UPDATE_STATS_ERROR) {
		update_stats_done(state);
		return NULL;
	}

	/*
	 * General error handling for db errors.
	 */
	if (errstr) {
                transaction_logprintf(cs->ts, D_ERROR, "Statement failed (%s, %s)", update_stats_state_lut[state->state], errstr);
		if (!strcmp(errstr, "ERROR_COMMUNICATION")) {
			cs->status = "TRANS_ERROR";
			cs->error = "ERROR_COMMUNICATION";
		} else {
			cs->status = "TRANS_DATABASE_ERROR";
			cs->message = xstrdup(errstr);
		}
		state->state = UPDATE_STATS_ERROR;
        }

	while (state->state != UPDATE_STATS_DONE) {
		transaction_logprintf(cs->ts, D_DEBUG, "update_stats fsm: %s", update_stats_state_lut[state->state]);
		switch(state->state) {
		case UPDATE_STATS_INIT: {
			/* If data template in bconf use it to get data */
			int res;
			char *data_template;
			const char *bconf_template = bconf_value(bconf_vget(bconf_root,
									    "*.*.stats", cmd_getval(cs, "stat_name"), "data_template", NULL));
			state->state = UPDATE_STATS_UPDATE;

			if (!bconf_template)
				continue;
			transaction_logprintf(cs->ts, D_DEBUG, "template sql/stats/%s", bconf_template);
			ALLOCA_PRINTF(res, data_template, "sql/stats/%s", bconf_template);

 			BPAPI_INIT(&ba, NULL, pgsql);
			bind_insert(&ba, state);
			sql_worker_template_query(&config.pg_slave, data_template, &ba,
						  update_stats_fsm, state, cs->ts->log_string);

			bpapi_output_free(&ba);
			bpapi_simplevars_free(&ba);
			bpapi_vtree_free(&ba);

			return NULL;
		}
		case UPDATE_STATS_UPDATE: {
			int i;
			int res;
			const char *stat_type = 
				bconf_value(bconf_vget(bconf_root, "*.*.stats", cmd_getval(cs, "stat_name"), "type", NULL));
			char *update_template;

			if (!stat_type)
				stat_type = "hourly"; /* Assume invword counter. */

			state->state = UPDATE_STATS_CHECK_RESULT;
 			BPAPI_INIT(&ba, NULL, pgsql);

			if (worker) {
				if (!worker->rows(worker)) {
					transaction_printf(cs->ts, "no_data:1\n");
					state->state = UPDATE_STATS_DONE;
					continue;
				}

				/* we have data from data_template */
				while (worker->next_row(worker)) {
					for (i=0; i<worker->fields(worker); i++) {
						transaction_logprintf(cs->ts, D_INFO, "XXX Data from data_template %s, %s",
								      worker->field_name(worker, i), worker->get_value(worker, i));
						if (worker->get_value(worker, i)[0])
							bpapi_insert(&ba, worker->field_name(worker, i), worker->get_value(worker, i));
						else
							bpapi_insert(&ba, worker->field_name(worker, i), "NULL");
					}
				}
			}

			bind_insert(&ba, state);
			ALLOCA_PRINTF(res, update_template, "sql/call_update_%s_stats.sql", stat_type);

			sql_worker_template_query_flags(&config.pg_master, SQLW_FINAL_CALLBACK, update_template, &ba,
						  update_stats_fsm, state, cs->ts->log_string);

			bpapi_output_free(&ba);
			bpapi_simplevars_free(&ba);
			bpapi_vtree_free(&ba);

			return NULL;

		}
		/* check result */
		case UPDATE_STATS_CHECK_RESULT:
			if (worker->sw_state == RESULT_DONE) {
				state->state = UPDATE_STATS_DONE;
				continue;
			}
			while (worker->next_row(worker)) {
				int cols = worker->fields(worker);
				int col;

				for (col = 0; col < cols; col++) {
					transaction_printf(cs->ts, "%s:%s\n",
							worker->field_name(worker, col),
							worker->get_value(worker, col));
				}
			}
			return NULL;

		case UPDATE_STATS_ERROR:
			cs->status = "TRANS_ERROR";
			transaction_logprintf(cs->ts, D_ERROR, "UPDATE_STATS_ERROR state (%d)",
					      state->state);
			update_stats_done(state);
			return NULL;

		default:
			/* UNKNOWN STATE ERROR */
			cs->status = "TRANS_ERROR";
			transaction_logprintf(cs->ts, D_ERROR, "Unknown state (%d)",
					      state->state);
			update_stats_done(state);
			return NULL;

		}
	}

        update_stats_done(state);
        return NULL;
}

static void
update_stats(struct cmd *cs) {
	struct update_stats_state *state;

	transaction_logprintf(cs->ts, D_DEBUG, "update_stats command started");

	state = zmalloc(sizeof(*state));

	state->cs = cs;
	state->state = UPDATE_STATS_INIT;

	update_stats_fsm(state, NULL, NULL);
}


ADD_COMMAND(update_stats, NULL, update_stats, update_stats_parameters, NULL);

