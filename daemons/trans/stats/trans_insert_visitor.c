#include "factory.h"

static struct c_param parameters[] = {
	{"vid",   P_MULTI, "v_integer"  },
	{"creation", P_MULTI, "v_string_isprint"},
	{NULL, 0}
};

FACTORY_TRANSACTION(insert_visitor, &config.pg_master, "sql/insert_visitor.sql", NULL, parameters, FACTORY_NO_RES_CNTR | FACTORY_EMPTY_RESULT_OK | FACTORY_RES_SHORT_FORM );
