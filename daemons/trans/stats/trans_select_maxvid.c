#include "factory.h"

static struct c_param parameters[] = {
	{"date", 0, "v_date"},
	{NULL, 0}
};

FACTORY_TRANSACTION(select_maxvid, &config.pg_slave, "sql/select_maxvid.sql", NULL, parameters, FACTORY_NO_RES_CNTR | FACTORY_EMPTY_RESULT_OK | FACTORY_RES_SHORT_FORM );

