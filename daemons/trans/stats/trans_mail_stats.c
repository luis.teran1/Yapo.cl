#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <time.h>

#include "trans.h"
#include "util.h"
#include <ctemplates.h>
#include "validators.h"
#include <bconf.h>
#include <event.h>
#include <unistd.h>
#include <fcntl.h>
#include "trans_mail.h"

#define MAIL_STATS_INIT  0
#define MAIL_STATS_GET_DATA 1
#define MAIL_STATS_MAIL 2
#define MAIL_STATS_ERROR 3
#define MAIL_STATS_DONE  4

struct mail_stats_state {
	int state;
	char *tmpfile;
	struct cmd *cs;
	struct mail_data *md;
	struct bpapi ba;
};

static struct c_param parameters[] = {
	{"day;month", P_XOR, "v_date;v_year_month"},
	{"to", 0, "v_email_basic"},
	{"lang", 0, "v_lang"},
	{NULL}
};

extern struct bconf_node *bconf_root;

static const char *mail_stats_fsm(void *, struct sql_worker *, const char *);


/*****************************************************************************
 * mail_stats_done
 * Exit function.
 **********/
static void
mail_stats_done(void *v) {
	struct mail_stats_state *state = v;
	struct cmd *cs = state->cs;

	if (state->tmpfile) {
	unlink(state->tmpfile);
		free(state->tmpfile);
	}

	bpapi_output_free(&state->ba);
	bpapi_simplevars_free(&state->ba);
	bpapi_vtree_free(&state->ba);

	free(state);
	cmd_done(cs);
}


/*****************************************************************************
 * mail_stats_mail_cb
 * Callback die.
 **********/
static void
mail_stats_mail_cb(struct mail_data *md, void *arg, int status) {
	struct mail_stats_state *state = arg;
	const char *errstr = NULL;

	if (status != 0)
		errstr = "TRANS_MAIL_ERROR";

	mail_stats_fsm(state, NULL, errstr);
}

/*****************************************************************************
 * bind_insert
 * Initialise template API before a call to sql_worker_template_query().
 **********/
static void
bind_insert(struct bpapi* ba, struct mail_stats_state *state) {
	struct c_param *param;

	for (param = parameters; param->name; param++) {
		if (cmd_haskey(state->cs, param->name)) {
			bpapi_insert(&state->ba, param->name, cmd_getval(state->cs, param->name));
		}
	}

	if (cmd_haskey(state->cs, "day"))
		bpapi_insert(ba, "day", cmd_getval(state->cs, "day"));
	if (cmd_haskey(state->cs, "month"))
		bpapi_insert(ba, "month", cmd_getval(state->cs, "month"));

}

/*****************************************************************************
 * mail_stats_fsm
 * State machine
 **********/
static const char *
mail_stats_fsm(void *v, struct sql_worker *worker, const char *errstr) {
	struct mail_stats_state *state = v;
	struct cmd *cs = state->cs;

	/* 
	 * General error handling for db errors.
	 */
	if (errstr) {
                transaction_logprintf(cs->ts, D_ERROR, "mail_stats_fsm statement failed (%d, %s)", 
                                      state->state,
                                      errstr);
                cs->status = "TRANS_DATABASE_ERROR";
                cs->message = xstrdup(errstr);
		state->state = MAIL_STATS_ERROR;
	}

	while (1) {
		transaction_logprintf(cs->ts, D_DEBUG, "mail_stats_fsm(%p, %p, %p), state=%d", 
				      v, worker, errstr, state->state);

		switch (state->state) {
		case MAIL_STATS_INIT:
			{
				state->state = MAIL_STATS_GET_DATA;

				/* Initialise the templateparse structure */
				BPAPI_INIT(&state->ba, NULL, pgsql);
				bind_insert(&state->ba, state);

				/* Register SQL query with our mail_stats_fsm() as callback function */
				sql_worker_template_query_flags(&config.pg_master, SQLW_FINAL_CALLBACK,
							  "sql/mail_stats_fetch_data.sql", &state->ba, 
							  mail_stats_fsm, state, cs->ts->log_string);

				/* Return to libevent */
				return NULL;
			}

		case MAIL_STATS_GET_DATA:
		{
			int cols;
			int col;

			if (worker->sw_state == RESULT_DONE) {
                        	state->state = MAIL_STATS_MAIL;
				continue;
			}

			if (!state->md) {
				state->md = zmalloc(sizeof(*state->md));
				state->md->command = state;	
				state->md->log_string = cs->ts->log_string;
			}

			cols = worker->fields(worker);

			while (worker->next_row(worker)) {
				for (col = 0; col < cols; col++) {
					mail_insert_param(state->md, 
							  worker->field_name(worker, col),
						   	  worker->get_value(worker, col));

					bpapi_insert(&state->ba, worker->field_name(worker, col), worker->get_value(worker, col));
				}
			}

			return NULL;
		}

		case MAIL_STATS_MAIL:
			state->state = MAIL_STATS_DONE;
				
			if (cmd_getval(cs, "month")) {

				/* Write temp file */	
				struct buf_string *buf;
				int fd;

				xasprintf(&state->tmpfile, "%s/%s.xls", bconf_get_string(bconf_root, "*.mail_stats.tmpdir"), cmd_getval(cs, "month"));
				buf = BPAPI_OUTPUT_DATA(&state->ba);
				call_template(&state->ba, "mail/stats_monthly_excel.xls");

				if (!buf->buf) {
					transaction_logprintf(cs->ts, D_ERROR, "(CRIT) Template mail/stats_monthly_excel.xls is missing!");
					state->state = MAIL_STATS_ERROR;
					continue;
				}

				/* Filename for attachment */
				fd = open(state->tmpfile, O_WRONLY | O_CREAT | O_EXCL, 0600);

				if (fd < 0) 	{
					transaction_logprintf(cs->ts, D_ERROR, "(CRIT) Failed to open temp file %s", state->tmpfile);
					/* Free directly to avoid unlink. */
					free(state->tmpfile);
					state->tmpfile = NULL;

					state->state = MAIL_STATS_ERROR;
					continue;
				}
				if (write(fd, buf->buf, buf->pos) < buf->pos) {
					transaction_logprintf(cs->ts, D_ERROR, "(CRIT) Failed to write to file: %m");
					close(fd);
					state->state = MAIL_STATS_ERROR;
					continue;
				}
				close(fd);

				/* Insert temporary filename-path as attachment-parameter */
				mail_insert_param(state->md, "attachment", state->tmpfile);
			     	mail_insert_param(state->md, "month", cmd_getval(cs, "month"));
			} else {
				/* If day is set */
			     	mail_insert_param(state->md, "day", cmd_getval(cs, "day"));
				/* If we use nail to send this email the X-deepblue headers won't be sent, so we need sendmail */
 				mail_insert_param(state->md, "wrapper_template", "none");
			}

			if (state->md) {
				if (cmd_haskey(cs, "to"))
					mail_insert_param(state->md, "to", cmd_getval(cs, "to"));

				if (cmd_haskey(cs, "lang"))
					mail_insert_param(state->md, "lang", cmd_getval(cs, "lang"));

				state->md->command = state;
				state->md->callback = mail_stats_mail_cb;
				state->md->log_string = cs->ts->log_string;

				mail_send(state->md, "mail/stats.txt", cmd_getval(cs,"lang"));
				state->md = NULL;
				return NULL;
			} 
			continue;

		case MAIL_STATS_ERROR:
			transaction_logprintf(cs->ts, D_ERROR, "Error state");
			/* Fall through */

		case MAIL_STATS_DONE:
			mail_stats_done(state);
			return NULL;

		default:
			/* MAJOR STATE ERROR */
			cs->status = "TRANS_ERROR";
			transaction_logprintf(cs->ts, D_ERROR, "Unknown state (%d)", 
					      state->state);
			return NULL;

		}
	}
}

/*****************************************************************************
 * mail_stats
 * Main function that initialise a state-structure and calls the state machine.
 **********/
static void
mail_stats(struct cmd *cs) {
	transaction_logprintf(cs->ts, D_DEBUG, "Start Mail stats transaction");
	struct mail_stats_state *state;
	state = zmalloc(sizeof(*state));
	state->cs = cs;
	state->state = MAIL_STATS_INIT;
	
	mail_stats_fsm(state, NULL, NULL);
}

ADD_COMMAND(mail_stats,     /* Name of command */
		NULL,
            mail_stats,     /* Main function name */
            parameters,  /* Parameters struct */
	    "Mail stats command");/* Command description */
