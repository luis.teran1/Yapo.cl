#include "factory.h"
#include <fd_pool.h>
#include <stdlib.h>
#include "platform/lib/template/redis/redis_filters.h"
#include "sredisapi.h"
#include "fd_pool.h"
#include "utils/redis_trans_context.h"
#include <unistd.h>

extern struct bconf_node *bconf_root;

struct args {
	struct cmd *cs;
	struct bpapi *ba;
	int chunk;
	int chunk_size;
	long long total;
};

static struct c_param parameters[] = {
    {"date", P_REQUIRED, "v_date"},
    {"stat_type", P_REQUIRED, "v_stat_type"},
	{NULL, 0}
};

static struct factory_data data = {
	NULL,
	FACTORY_RES_SHORT_FORM | FACTORY_EMPTY_RESULT_OK
};

static void
store_on_db(struct cmd *cs, struct bpapi *ba) {
	const char *errstr = NULL;
	struct sql_worker *worker = NULL;

	worker = sql_blocked_template_query("pgsql.master", 0, "sql/call_build_daily_stats.sql", ba, cs->ts->log_string, &errstr);

	if (worker) {
		/* Drain the results */
		while (sql_blocked_next_result(worker, &errstr) == 1);
	}

	if (errstr && *errstr) {
		if (!cs->message)
			cs->message = xstrdup(errstr);
		cs->status = "TRANS_DATABASE_ERROR";
	}

	if (worker)
		sql_worker_put(worker);
}

static void
store_results(struct args *params) {
	if (params->chunk > 0) {
		store_on_db(params->cs, params->ba);
		bpapi_remove(params->ba, "redis_zrange_value");
		bpapi_remove(params->ba, "redis_zrange_score");
		params->total += params->chunk;
		params->chunk = 0;
	}
}

static void
store_results_cb(const void *value, size_t vlen, const void *score, size_t slen, void *cbarg) {
	struct args *params = (struct args *) cbarg;
	bpapi_insert(params->ba, "redis_zrange_value", (char *) value);
	bpapi_insert(params->ba, "redis_zrange_score", (char *) score);

	params->chunk++;
	if (params->chunk >= params->chunk_size) {
		store_results(params);
	}
}

static int
save_stats(struct cmd *cs, struct bpapi *ba) {
	int chunk_size = bconf_get_int(bconf_root, "*.common.daily_stats.chunk_size");
	const char *stat_key = !strcmp(cmd_getval(cs, "stat_type"), "view") ? "VIEW:total" : "MAIL:total";
	struct args params = {cs, ba, 0, chunk_size, 0};

	struct fd_pool_conn *conn = redis_sock_conn(redis_stat_pool.pool, "master");
	int r = redis_sock_zrange(conn, stat_key, 0, -1, store_results_cb, &params);
	fd_pool_free_conn(conn);

	/* flush the last and maybe incomplete set of queries */
	store_results(&params);
	transaction_printf(cs->ts, "total:%lld\n", params.total);
	return r < 0 ? r : 0;
}

static const struct factory_action actions[] = {
	FA_FUNC(save_stats),
	FA_DONE()
};

FACTORY_TRANS(build_daily_stats, parameters, data, actions);
