#include "factory.h"

static struct c_param parameters[] = {
        {NULL, 0, 0}
};

FACTORY_TRANSACTION(last_stat_time, &config.pg_slave, "sql/get_last_stat_time.sql", NULL,parameters , FACTORY_EMPTY_RESULT_OK);
