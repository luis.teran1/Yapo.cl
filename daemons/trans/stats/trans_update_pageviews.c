#include "factory.h"

static struct c_param parameters[] = {
	{"code",   P_REQUIRED|P_MULTI, "v_string_isprint"  },
	{"minute", P_REQUIRED|P_MULTI, "v_string_isprint"  },
	{"pageviews",   P_REQUIRED|P_MULTI, "v_integer"  },
	{NULL, 0}
};

FACTORY_TRANSACTION(update_pageviews, &config.pg_master, "sql/update_pageviews.sql", NULL, parameters, FACTORY_NO_RES_CNTR | FACTORY_EMPTY_RESULT_OK | FACTORY_RES_SHORT_FORM );

