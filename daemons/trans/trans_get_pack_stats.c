#include "factory.h"
#include "bpapi.h"
#include <bconf.h>

static struct c_param parameters[] = {
	{"user_id",	P_OPTIONAL,	"v_user_id"		},
	{"pack_type",	P_REQUIRED,	"v_stats_pack_type"	},
	{"date",	P_REQUIRED,	"v_date"		},
	{NULL, 0}
};

static struct factory_data data = {
	NULL,
	FACTORY_NO_RES_CNTR | FACTORY_EMPTY_RESULT_OK | FACTORY_RES_SHORT_FORM
};

static const struct factory_action actions[] = {
	FA_SQL("", "pgsql.slave", "sql/call_get_pack_stats.sql", FA_OUT),
	FA_DONE()
};

FACTORY_TRANS(get_pack_stats, parameters, data, actions);
