#include "factory.h"
#include "utils/filterd.h"

static struct c_param parameters[] = {
	{"email",	P_REQUIRED,	"v_email_basic"},
	{"notice",	P_REQUIRED,	"v_string_isprint"},
	{NULL, 0}
};

static struct factory_data data = {
	"controlpanel",
	FACTORY_EMPTY_RESULT_OK
};

static const struct factory_action filterd_reload_action = FA_FUNC(filterd_reload_fa);

static const struct factory_action *
reload_filterd_if_changed(struct cmd *cs, struct bpapi *ba) {
	const char *updated = bpapi_get_element(ba, "updated", 0);
	transaction_printf(cs->ts, "add_to_whitelist.0.add_to_whitelist:%s\n", updated);
	if (!strcmp(updated, "t"))
		return &filterd_reload_action;
	return NULL;
}

static const struct factory_action actions[] = {
	FA_SQL(NULL, "pgsql.master", "sql/call_add_to_whitelist.sql", 0),
	FA_COND(reload_filterd_if_changed),
	FA_DONE()
};

FACTORY_TRANS(add_to_whitelist, parameters, data, actions);
