#include "factory.h"
#include "bpapi.h"
#include <bconf.h>
#include "fd_pool.h"
#include "validators.h"
#include "sredisapi.h"
#include "utils/redis_trans_context.h"
#include "promo_page_utils.h"

struct validator v_banner_id[] = {
	VALIDATOR_REGEX("^([[:alnum:]]+:)*[a-z_]+:[[:alnum:]]+:[[:alnum:]]+:[[:digit:]]{1,10}$", REG_EXTENDED|REG_NOSUB, "ERROR_BANNER_ID_INVALID"),
	{0}
};
ADD_VALIDATOR(v_banner_id);

static struct c_param parameters[] = {
	{"banner_id", P_REQUIRED, "v_banner_id"},
	{NULL, 0}
};

static struct factory_data data = {
	NULL,
	FACTORY_NO_RES_CNTR
};

static const struct factory_action actions[] = {
	FA_OUT("trans/delete_pp_banner_on_redis"),
	FA_DONE()
};

FACTORY_TRANS(delete_pp_banner, parameters, data, actions);
