#ifndef PPAGES_H
#define PPAGES_H

/**
 * Checks for error variable on bpapi ba
 * sets TRANS_ERROR if found, and prints
 * the error message
 */
int
check_error(struct cmd *cs, struct bpapi *ba);

#endif
