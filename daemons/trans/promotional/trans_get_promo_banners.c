#include "factory.h"
#include "bpapi.h"
#include <bconf.h>
#include "fd_pool.h"
#include "promo_page_utils.h"

static struct c_param parameters[] = {
	{"key", P_OPTIONAL, "v_string_isprint"},
	{NULL, 0}
};

static struct factory_data data = {
	NULL,
	FACTORY_NO_RES_CNTR
};

static const struct factory_action actions[] = {
	FA_OUT("trans/get_promo_banners_from_redis"),
	FA_FUNC(check_error),
	FA_DONE()
};

FACTORY_TRANS(get_promo_banners, parameters, data, actions);
