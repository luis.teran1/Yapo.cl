#include "factory.h"
#include "bpapi.h"
#include <bconf.h>
#include "fd_pool.h"
#include "promo_page_utils.h"

static struct c_param parameters[] = {
	{"pp_id", P_OPTIONAL, "v_integer"},
	{"status", P_OPTIONAL, "v_array:active,all"},
	{NULL, 0}
};

static struct factory_data data = {
	NULL,
	FACTORY_NO_RES_CNTR
};

static const struct factory_action actions[] = {
	FA_OUT("trans/get_ppage_from_redis"),
	FA_FUNC(check_error),
	FA_DONE()
};

FACTORY_TRANS(get_promotional_pages, parameters, data, actions);
