#include "factory.h"
#include "bpapi.h"
#include <bconf.h>
#include "fd_pool.h"
#include "validators.h"
#include "sredisapi.h"
#include "utils/redis_trans_context.h"
#include "promo_page_utils.h"

static struct c_param parameters[] = {
	{"queue;pp_id", P_IMPLY, "v_bool;v_integer"},
	{NULL, 0}
};

static struct factory_data data = {
	NULL,
	FACTORY_NO_RES_CNTR
};

static const struct factory_action
	enqueue_publication = FA_OUT("trans/queue_ppage_publication"),
	publish_ppage_on_redis = FA_OUT("trans/publish_ppage_on_redis"),
	publish_ppage_banners = FA_OUT("trans/publish_banners");

static const struct factory_action *
dispatch_action(struct cmd *cs, struct bpapi *ba) {
	return (cmd_haskey(cs, "queue") && !strcmp(cmd_getval(cs, "queue"), "1"))
		? &enqueue_publication
		: &publish_ppage_on_redis;
}

static const struct factory_action *
publish_banners(struct cmd *cs, struct bpapi *ba) {
	return (cmd_haskey(cs, "queue") && !strcmp(cmd_getval(cs, "queue"), "1"))
		? NULL
		: &publish_ppage_banners;
}

static const struct factory_action actions[] = {
	FA_COND(dispatch_action),
	FA_FUNC(check_error),
	FA_COND(publish_banners),
	FA_DONE()
};

FACTORY_TRANS(publish_promotional_page, parameters, data, actions);
