#include "factory.h"
#include "bpapi.h"
#include <bconf.h>
#include "fd_pool.h"
#include "validators.h"
#include "sredisapi.h"
#include "utils/redis_trans_context.h"
#include "promo_page_utils.h"

static struct c_param parameters[] = {
	{"pp_id", P_REQUIRED, "v_integer"},
	{"url", P_REQUIRED, "v_string_isprint"},
	{"platform", P_REQUIRED, "v_string_isprint"},
	{"position", P_REQUIRED, "v_string_isprint"},
	{"start_date", P_REQUIRED, "v_date"},
	{"end_date", P_OPTIONAL, "v_date_greater_than:start_date"},
	{"regions", P_MULTI|P_OPTIONAL, "v_integer"},
	{"categories", P_MULTI|P_OPTIONAL, "v_category_multi"},
	{"image", P_REQUIRED, "v_string_isprint"},
	{"key", P_OPTIONAL, "v_string_isprint"},
	{NULL, 0}
};

static struct factory_data data = {
	NULL,
	FACTORY_NO_RES_CNTR
};

static const struct factory_action actions[] = {
	FA_OUT("trans/set_pp_banner_on_redis"),
	FA_FUNC(check_error),
	FA_OUT("trans/queue_banner_publication"),
	FA_DONE()
};

FACTORY_TRANS(set_pp_banner, parameters, data, actions);
