#include "factory.h"
#include "bpapi.h"
#include <bconf.h>
#include "fd_pool.h"
#include "validators.h"
#include "sredisapi.h"
#include "utils/redis_trans_context.h"
#include "promo_page_utils.h"

int
check_error(struct cmd *cs, struct bpapi *ba) {
	const char *error = bpapi_get_element(ba, "error", 0);
	if (error) {
		cs->message = xstrdup(error);
		cs->status = "TRANS_ERROR";
		return 1;
	}
	return 0;
}

static void
vf_pp_unique_cb_redis(const void *value, size_t vlen, void *cbarg) {
	struct cmd_param *param = cbarg;

	if (cmd_haskey(param->cs, "pp_id")) {
		char *key = NULL;
		xasprintf(&key, "pp_id:%s", cmd_getval(param->cs, "pp_id"));
		bool same_pp_id = strstr((const char*) value, key) != NULL;
		free(key);
		if (same_pp_id)
			return ;
	}
	struct fd_pool_conn *conn = redis_sock_conn(redis_ppages_pool.pool, "slave");
	if (!conn) {
		param->error = "ERROR_REDIS_CONNECTION";
		param->cs->status = "TRANS_ERROR";
		return ;
	}

	transaction_logprintf(param->cs->ts, D_DEBUG,
			"vf_pp_unique_cb_redis: looking for %s == %s in redis key: %s", 
			param->key, param->value, (const char*)value);

	char *val = redis_sock_hget(conn, (const char*) value, param->key);
	if (val && !strcmp(param->value, val)) {
		param->cs->status = "TRANS_ERROR";
		if(!strcmp(param->key, "url"))
			param->error = "ERROR_URL_ALREADY_EXISTS";
		else
			param->error = "ERROR_TITLE_ALREADY_EXISTS";
	}

	fd_pool_free_conn(conn);
	free(val);
}

static int
vf_pp_unique(struct cmd_param *param, const char *arg) {
	struct fd_pool_conn *conn = redis_sock_conn(redis_ppages_pool.pool, "slave");
	if (!conn) {
		param->error = "ERROR_REDIS_CONNECTION";
		param->cs->status = "TRANS_ERROR";
		return 1;
	}

	const char *key_arr[] = {"pp_id:*", "draft:pp_id:*", NULL};
	const char **key_arr_ptr = &key_arr[0];
	while (*key_arr_ptr) {
		int err = redis_sock_keys(conn, *key_arr_ptr, vf_pp_unique_cb_redis, param);
		if (param->error)
			break;
		if (err < 0) {
			param->error = "ERROR_REDIS_KEYS";
			param->cs->status = "TRANS_ERROR";
		}
		key_arr_ptr++;
	}
	fd_pool_free_conn(conn);
	return 0;
}

struct validator v_pp_title[] = {
	VALIDATOR_REGEX("(*UCP)^[[:print:][:alpha:]]+$", REG_EXTENDED, "ERROR_TITLE_INVALID"),
	VALIDATOR_LEN(-1, 100, "ERROR_TITLE_TOO_LONG"),
	VALIDATOR_FUNC(vf_pp_unique),
	{0}
};
ADD_VALIDATOR(v_pp_title);

struct validator v_ppage_url[] = {
	VALIDATOR_REGEX("(*UCP)^[[:print:][:alpha:]]+$", REG_EXTENDED, "ERROR_URL_INVALID"),
	VALIDATOR_LEN(-1, 100, "ERROR_URL_TOO_LONG"),
	VALIDATOR_FUNC(vf_pp_unique),
	{0}
};
ADD_VALIDATOR(v_ppage_url);

struct validator v_ppage_feature[] = {
	VALIDATOR_BCONF_LIST_KEY("controlpanel.ppages.features", "ERROR_INVALID_PP_FEATURE"),
	{0}
};
ADD_VALIDATOR(v_ppage_feature);

static struct c_param parameters[] = {
	{"pp_id", P_OPTIONAL, "v_integer"},
	{"title", P_REQUIRED, "v_pp_title"},
	{"url", P_REQUIRED, "v_ppage_url"},
	{"image", P_REQUIRED, "v_string_isprint"},
	{"start_date", P_REQUIRED, "v_date"},
	{"end_date", P_OPTIONAL, "v_date_greater_than:start_date"},
	{"search_query", P_MULTI, "v_string_isprint"},
	{"category", P_MULTI|P_REQUIRED, "v_category_multi"},
	{"feature", P_OPTIONAL, "v_ppage_feature"},
	{NULL, 0}
};

static struct factory_data data = {
	NULL,
	FACTORY_NO_RES_CNTR
};

static const struct factory_action actions[] = {
	FA_OUT("trans/set_ppage_on_redis"),
	FA_FUNC(check_error),
	FA_DONE()
};

FACTORY_TRANS(set_promotional_page, parameters, data, actions);

