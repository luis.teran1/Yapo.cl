#include "factory.h"
#include "bpapi.h"
#include <bconf.h>
#include "fd_pool.h"
#include "promo_page_utils.h"

static struct c_param parameters[] = {
	{"pp_id", P_REQUIRED, "v_integer"},
	{"type", P_OPTIONAL, "v_array:draft,all"}, /* default: all */
	{NULL, 0}
};

static struct factory_data data = {
	NULL,
	FACTORY_NO_RES_CNTR | FACTORY_RES_SHORT_FORM
};

static const struct factory_action actions[] = {
	FA_OUT("trans/delete_ppage_from_redis"),
	FA_FUNC(check_error),
	FA_OUT("trans/delete_ppage_banners_from_redis"),
	FA_DONE()
};

FACTORY_TRANS(delete_promotional_page, parameters, data, actions);

