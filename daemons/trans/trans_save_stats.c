#include "factory.h"

static struct c_param parameters[] = {
	{"stat_type", P_REQUIRED, "v_array:store_view"},
	{"ext_id", P_REQUIRED, "v_integer"},
	{"reg_date", P_REQUIRED, "v_date"},
	{"reg_hour", P_REQUIRED, "v_time"},
	{"visits", P_REQUIRED, "v_integer"},
	{NULL, 0}
};

static struct factory_data data = {
        "save_stats",
        FACTORY_RES_SHORT_FORM
};

static const struct factory_action actions[]= {
	FA_SQL("", "pgsql.master", "sql/call_save_stats.sql", FA_OUT),
	{0}
};

FACTORY_TRANS(save_stats, parameters, data, actions);
