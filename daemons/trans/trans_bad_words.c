#include "factory.h" 

static struct c_param parameters[] = {
	//this transaction only reads from bconf the lists to be selected, if you want to pass the lists from where to extract new bad words, please feel free to extend this transaction
	//if you decide to extend this, consider to never stop reading from conf unless you provide the values
	{NULL, 0}
};

FACTORY_TRANSACTION(bad_words, &config.pg_slave, "sql/filters/list_bad_words.sql", NULL, parameters, FACTORY_NO_RES_CNTR | FACTORY_RES_SHORT_FORM | FACTORY_RES_NO_LOG);
