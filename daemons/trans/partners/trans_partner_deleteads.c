#include "factory.h"

static struct c_param parameters[] = {
	{"partner", P_REQUIRED, "v_string_isprint"},
	{"check", P_REQUIRED, "v_bool"},
	{NULL, 0}
};

static struct factory_data data = {
	NULL,
	FACTORY_RES_SHORT_FORM
};

static int delete_partner_ads(struct cmd *cs, struct bpapi *ba) {
	int length = bpapi_length(ba, "ad_id");
	int i;
	for (i = 0; i < length; i++) {
		const char *external_ad_id = bpapi_get_element(ba, "external_ad_id", i);
		transaction_internal_printf(TRANSACTION_INTERNAL_SYNC_CB, NULL,
			"cmd:deletead\n"
			"do_not_send_mail:1\n"
			"external_ad_id:%s\n"
			"link_type:%s\n"
			"commit:1\n",
			external_ad_id,
			cmd_getval(cs, "partner"));
	}
	return 0;
}

static const struct factory_action present_email_true = FA_FUNC(delete_partner_ads);

static const struct factory_action *
check_mode(struct cmd *cs, struct bpapi *ba) {
	if (cmd_haskey(cs, "check") && !strcmp(cmd_getval(cs, "check"), "0"))
		return &present_email_true;
	return NULL;
}

static const struct factory_action actions[]= {
	FA_SQL("", "pgsql.slave", "sql/get_partner_ads.sql", FA_OUT),
	FA_COND(check_mode),
	{0}
};

FACTORY_TRANS(partner_deleteads, parameters, data, actions);
