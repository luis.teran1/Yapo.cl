#include "factory.h"
#include "trans.h"
#include "bpapi.h"
#include <stdio.h>
#include <string.h>
#include "utils/create_session_hash.h"

static struct c_param parameters[] = {
	{"name", 	P_REQUIRED, 	"v_name"},
	{"email", 	P_REQUIRED, 	"v_email_basic"},
	{"phone", 	P_REQUIRED, 	"v_phone_strict"},
	{"rut",		P_OPTIONAL,	"v_pay_rut_optional"},
	{"is_company",	P_OPTIONAL,	"v_accounts_is_company"},
	{"region", 	P_OPTIONAL,	"v_region"},
	{"password",	P_REQUIRED,	"v_account_hashed_password"},
	{"commune",	P_OPTIONAL,	"v_commune_optional"},
	{"gender",	P_OPTIONAL,	"v_gender_account"},
	{NULL, 0}
};

static int
attempt_create_account(struct cmd *cs, struct bpapi *ba) {

	const char *name = cmd_getval(cs, "name");
	const char *email = cmd_getval(cs, "email");
	const char *phone = cmd_getval(cs, "phone");
	const char *password = cmd_getval(cs, "password");
	
	const char *default_gender = bconf_get_string(trans_bconf, "gender.default");
	const char *default_region = bconf_get_string(trans_bconf, "region.default");
	const char *default_commune = bconf_get_string(trans_bconf, "commune.default");
	const char *default_rut = bconf_get_string(trans_bconf, "valid_rut.default");
	const char *default_company = bconf_get_string(trans_bconf, "is_company.default");

	char *res = transaction_internal_output_printf(
		"cmd:create_account\n"
		"name:%s\n"
		"email:%s\n"
		"phone:%s\n"
		"rut:%s\n"
		"is_company:%s\n"
		"region:%s\n"
		"password:%s\n"
		"commune:%s\n"
		"gender:%s\n"
		"accept_conditions:on\n"
		"source:web\n"
		"autoactivate:1\n"
		"commit:1\n"
		"end\n",
		name,
		email,
		phone,
		cmd_haskey(cs, "rut")  ? cmd_getval(cs, "rut")  : default_rut,
		cmd_haskey(cs, "is_company")  ? cmd_getval(cs, "is_company")  : default_company,
		cmd_haskey(cs, "region")  ? cmd_getval(cs, "region")  : default_region,
		password,
		cmd_haskey(cs, "commune")  ? cmd_getval(cs, "commune")  : default_commune,
		cmd_haskey(cs, "gender")  ? cmd_getval(cs, "gender")  : default_gender
	);

	char *error = strstr(res, "ERROR");
	if (error) {
		char *exists = strstr(res, "error:ACCOUNT_ALREADY_EXISTS");
		if (exists) {
			cs->error="ACCOUNT_ALREADY_EXISTS";
		} else {
			cs->error=error;
		}
		cs->status="TRANS_ERROR";
		transaction_logprintf(cs->ts, D_ERROR, "Result from failed trans:%s ", res);
		free(res);
		return 1;
	}
	
	free(res);
	return 0;
}

static const struct factory_action actions[]= {
	FA_FUNC(attempt_create_account),
	FA_DONE()
};

static struct factory_data data = {
	"accounts",
	FACTORY_NO_RES_CNTR | FACTORY_RES_SHORT_FORM
};

FACTORY_TRANS(activate_account, parameters, data, actions);
