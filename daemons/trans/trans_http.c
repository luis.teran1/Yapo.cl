#include "trans.h"
#include "command.h"
#include "util.h"
#include "logging.h"
#include "http_req.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>

#include <bconf.h>
#include "validators.h"
#include "bpapi.h"
#include "query_string.h"
#include "tls.h"
#include "url.h"

#define HTTP_DONE  0
#define HTTP_ERROR 1
#define HTTP_INIT  2
#define HTTP_SEND_REQUEST 3

extern struct bconf_node *bconf_root;

struct tls_context http_tls_ctx;

enum request_type {
	HTTP_GET,
	HTTP_POST
};

struct http_state {
	int state;
	struct cmd *cs;

	enum request_type reqt;
	char* hostname;
	char* port;
	int timeout;
	struct evbuffer *databuf;
	char *qs;
};

static struct validator v_hostname[] = {
	VALIDATOR_LEN(-1, 64, "ERROR_HOSTNAME_TOO_LONG"),
	VALIDATOR_REGEX("^[a-zA-Z0-9-]+(\\.[a-zA-Z0-9-]+)*(:[0-9]{1,5})?$", REG_EXTENDED, "ERROR_HOSTNAME_INVALID"), /* http should not be supplied */
	{0}
};
ADD_VALIDATOR(v_hostname);

static struct validator v_requestmethod[] = {
	VALIDATOR_REGEX("^POST|GET$", REG_EXTENDED, "ERROR_METHOD_INVALID"),
	{0}
};
ADD_VALIDATOR(v_requestmethod);

static struct validator v_path[] = {
	VALIDATOR_LEN(-1, 128, "ERROR_PATH_TOO_LONG"),
	VALIDATOR_REGEX("^\\/[[:alnum:]\\._+-]*(\\/[[:alnum:]\\._+-]*)*$", REG_EXTENDED, "ERROR_PATH_INVALID"), 
	{0}
};
ADD_VALIDATOR(v_path);

static struct validator v_querystring[] = {
	VALIDATOR_LEN(-1, 1024, "ERROR_QUERYSTRING_TOO_LONG"),
	VALIDATOR_REGEX("^[[:alnum:]_+&@=. %<>\\/\?\\:-]*$", REG_EXTENDED, "ERROR_QUERYSTRING_INVALID"), 
	{0}
};
ADD_VALIDATOR(v_querystring);

static struct validator v_http_header[] = {
	VALIDATOR_LEN(-1, 128, "ERROR_HTTP_HEADER_TOO_LONG"),
	VALIDATOR_REGEX("^[a-zA-Z0-9-]+: [a-zA-Z0-9/+. -]+$", REG_EXTENDED, "ERROR_HTTP_HEADER_INVALID"),
	{0}
};
ADD_VALIDATOR(v_http_header);

static struct validator v_timeout[] = {
	VALIDATOR_INT(-1, 200, "ERROR_INVALID_TIMEOUT"),
	{0}
};
ADD_VALIDATOR(v_timeout);

struct validator v_http_arg[] = {
	VALIDATOR_REGEX(":", 0, "ERROR_COLON_MISSING"),
	{0}
};
ADD_VALIDATOR(v_http_arg);

static struct c_param parameters[] = {
	{"host",		P_REQUIRED,	"v_hostname"},			/* Host to send request to */
	{"method",		P_REQUIRED,	"v_requestmethod"},		/* Request method (POST/GET) */
	{"path",		P_REQUIRED,	"v_path"},
	{"querystring",		0,		"v_querystring"},
	{"header",		P_MULTI,	"v_http_header"},		/* HTTP headers */
	{"timeout",		0,		"v_timeout"},			/* HTTP request timeout in seconds */
	{"arg",			P_MULTI,	"v_http_arg"},
	{"tls",			0,		"v_bool"},
	{NULL, 0}
};

static const char *http_fsm(void *, struct sql_worker *, const char *);


/*****************************************************************************
 * http_done
 * Exit function.
 **********/
static void
http_done(void *v) {
	struct http_state *state = v;
	struct cmd *cs = state->cs;
	free(state->hostname);
	free(state->port);

	if (state->databuf)
		evbuffer_free(state->databuf);
	if (state->qs)
		free(state->qs);

	free(state);
	cmd_done(cs);
}


static void
httpreq_done_cb(struct request *httpreq) {
	struct http_state *state = httpreq->data;
	const char *errstr = NULL;

	if (state->cs)
		transaction_logprintf(state->cs->ts, D_INFO, "httpreq_done (%s %s)", httpreq->server, httpreq->port);

	free(httpreq->header);

	if (httpreq->result/100 != 2) { /* HTTP 200 status codes indicate success */
		state->cs->status = "ERROR_COMMUNICATION";
		errstr = http_req_result(httpreq->result);
		if (!state->cs->message)
			xasprintf(&state->cs->message, "%s:%s %s", httpreq->server, httpreq->port, (http_req_result(httpreq->result)));
	}

	transaction_printf(state->cs->ts, "status_code:%d\n", httpreq->result);

	free(httpreq);
	http_fsm(state, NULL, errstr);
}

static char *
fetch_qs(struct cmd *cs, size_t *qssz, int *need_free) {
	struct cmd_param *var;
	struct buf_string qs = {0};

	if (cmd_haskey(cs, "querystring")) {
		*need_free = 0;
		*qssz = cmd_getsize(cs, "querystring");
		return (char*)cmd_getval(cs, "querystring");
	}

	for (var = cmd_getparam(cs, "arg"); var; var = cmd_nextparam(cs, var)) {
		const char *cp = memchr(var->value, ':', var->value_size);

		if (!cp)
			continue; /* Shouldn't happen */

		if (qs.pos)
			bufwrite(&qs.buf, &qs.len, &qs.pos, "&", 1);
		url_encode(&qs, var->value, cp - var->value);
		bufwrite(&qs.buf, &qs.len, &qs.pos, "=", 1);
		url_encode(&qs, cp + 1, var->value_size - (cp + 1 - var->value));
	}
	*qssz = qs.pos;
	*need_free = 1;
	return qs.buf;
}

/*****************************************************************************
 * http_fsm
 * State machine
 **********/
static const char *
http_fsm(void *v, struct sql_worker *worker, const char *errstr) {
	struct http_state *state = v;
	struct cmd *cs = state->cs;

	/*
	 * General error handling
	 */
	if (errstr) {
                transaction_logprintf(cs->ts, D_ERROR, "http_fsm statement failed (%d, %s)",
                                      state->state,
                                      errstr);
                cs->status = "TRANS_ERROR";
		if (!cs->message)
			cs->message = xstrdup(errstr);
		state->state = HTTP_ERROR;
        }

	while (1) {
		transaction_logprintf(cs->ts, D_DEBUG, "http_fsm(%p, %p, %p), state=%d",
				      v, worker, errstr, state->state);

		switch (state->state) {
		case HTTP_INIT:
		{
			state->state = HTTP_SEND_REQUEST;

			if (cmd_getval(state->cs, "timeout")) {
				state->timeout = atoi(cmd_getval(state->cs, "timeout"));
				transaction_logprintf(cs->ts, D_DEBUG, "Timeout set to %d", state->timeout);
			}

			state->hostname = xstrdup(cmd_getval(state->cs, "host"));
			char* port_part = strrchr(state->hostname, ':');
			if (port_part) {
				if (state->port)
					free(state->port);
				state->port = xstrdup(port_part + 1);
				*port_part = '\0';
			}

			transaction_logprintf(cs->ts, D_DEBUG, "Host and port is '%s' and '%s'", state->hostname, state->port);

			continue;
		}

		case HTTP_SEND_REQUEST:
		{

			/* Setup HTTP header */
			static const int MAX_HTTP_HEADER_SIZE = 2048;
			char *httpheader = zmalloc(MAX_HTTP_HEADER_SIZE);
			int len = MAX_HTTP_HEADER_SIZE;
			int pos = 0;
			int user_supplied_content_type = 0;
			char *qs;
			int qs_need_free;
			size_t qssz;

			qs = fetch_qs(cs, &qssz, &qs_need_free);

                        state->state = HTTP_DONE;

			if (strcmp(cmd_getval(state->cs, "method"), "POST") == 0) {
				bufcat(&httpheader, &len, &pos, "POST %s HTTP/1.1\r\n", cmd_getval(state->cs, "path"));
				state->reqt = HTTP_POST;
			} else if(strcmp(cmd_getval(state->cs, "method"), "GET") == 0) {
				bufcat(&httpheader, &len, &pos, "GET %s", cmd_getval(state->cs, "path"));
				if (qs)
					bufcat(&httpheader, &len, &pos, "?%s", qs);
				bufcat(&httpheader, &len, &pos, " HTTP/1.1\r\n");
				state->reqt = HTTP_GET;
			} else {
				/* This can really only happen if the validators aren't correctly specified */
				transaction_logprintf(cs->ts, D_ERROR, "(CRIT): Unknown request method.");
				cs->status = "TRANS_ERROR";
				cs->message = xstrdup("Unknown request method.");
				state->state = HTTP_ERROR;
				continue;
			}

			/* Adding Host to headers */
			bufcat(&httpheader, &len, &pos, "Host: %s%s%s\r\n", state->hostname, state->port ? ":" : "", state->port ? state->port : "");

			/* Iterate over headers, adding them to the buffer */
			struct cmd_param *header;
			for (header = cmd_getparam(state->cs, "header") ; header ; header = cmd_nextparam(state->cs, header)) {
				bufcat(&httpheader, &len, &pos, "%s\r\n", header->value);
				if (strncmp("Content-Type: ", header->value, 14) == 0) {
					user_supplied_content_type = 1;
				}
			}


			/* Setup http request structure */
			struct request *httpreq = zmalloc(sizeof(*httpreq));

			httpreq->server = state->hostname;
			httpreq->port = state->port;
			httpreq->timeout = state->timeout;

			/* requestdata and content-length are only valid for POST */
			if (state->reqt == HTTP_POST && qs) {
				/* If no Content-Type header was given by the user, we add a sane default */
				if (!user_supplied_content_type)
					bufcat(&httpheader, &len, &pos, "Content-Type: application/x-www-form-urlencoded\r\n");
					
				httpreq->requestdata = qs;
				httpreq->reqsize = qssz;
				bufcat(&httpheader, &len, &pos, "Content-Length: %zu\r\n", httpreq->reqsize);
			}

			if (qs_need_free)
				state->qs = qs;

			bufcat(&httpheader, &len, &pos, "\r\n");

			httpreq->header = httpheader;
			httpreq->hsize = strlen(httpheader);
			httpreq->data = state;
			httpreq->cb_done = httpreq_done_cb;

			state->databuf = evbuffer_new();
			httpreq->databuf = state->databuf;

			if (cmd_haskey(cs, "tls")) {
				if (!http_tls_ctx.ssl_certs_path) {
					const char *cert_dir = bconf_get_string (bconf_root, "*.http.cert_dir");

					if (!cert_dir)
						cert_dir = tls_get_cert_dir ();
					http_tls_ctx.ssl_certs_path = cert_dir;
				}
				httpreq->tls_ctx = &http_tls_ctx;
				transaction_logprintf(cs->ts, D_DEBUG, "Using TLS, cert dir = %s", http_tls_ctx.ssl_certs_path);
			}

			http_req(httpreq);

			return NULL;
		}

		case HTTP_DONE:
		{
			if (state->databuf) {
				char *line;
				while ((line = evbuffer_readline(state->databuf)) && line[0]) {
					transaction_printf(state->cs->ts, "header:%s\n", line);
					free(line);
				}
				if (line)
					free(line);

				if (EVBUFFER_LENGTH(state->databuf)) {
					transaction_printf(state->cs->ts, "blob:%zu:body\n%.*s\n",
							   EVBUFFER_LENGTH(state->databuf),
							   (int)EVBUFFER_LENGTH(state->databuf),
							   EVBUFFER_DATA(state->databuf));
				}
			}
			http_done(state);
			return NULL;
		}
		case HTTP_ERROR:
			transaction_logprintf(cs->ts, D_ERROR, "Error state");
			http_done(state);
			return NULL;

		default:
			/* MAJOR STATE ERROR */
			cs->status = "TRANS_ERROR";
			transaction_logprintf(cs->ts, D_ERROR, "Unknown state (%d)",
					      state->state);
			return NULL;

		}
	}
}

/*****************************************************************************
 * http
 * Main function that initialise a state-structure and calls the state machine.
 **********/
static void
http(struct cmd *cs) {
	transaction_logprintf(cs->ts, D_DEBUG, "Start HTTP transaction");
	struct http_state *state;
	state = zmalloc(sizeof(*state));
	state->cs = cs;
	state->state = HTTP_INIT;
	state->timeout = 60;
	state->port = xstrdup("80");

	http_fsm(state, NULL, NULL);
}

ADD_COMMAND(http, NULL, http, parameters, "HTTP transaction");

