#include "factory.h"
#include "bpapi.h"
#include <bconf.h>

extern struct bconf_node *bconf_root;

static struct c_param parameters[] = {
	{"name",	P_REQUIRED,	"v_string_isprint"},
	{"to",		P_REQUIRED,	"v_email_basic"},
	{"product_id",	P_REQUIRED,	"v_pack_product_id"},
	{NULL, 0}
};


static struct factory_data data = {
	"packs_expires"
};

static const struct factory_action actions[] = {
        FA_MAIL("mail/pack_reminder.mime"),
        {0}
};

FACTORY_TRANS(send_pack_reminder_mail, parameters, data, actions);
