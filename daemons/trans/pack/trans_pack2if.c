#include "factory.h"

extern struct bconf_node *bconf_root;

static struct c_param parameters[] = {
	{"ad_id",          P_REQUIRED, "v_if_pack_ad_id"},
	{"action_id",      P_OPTIONAL, "v_ad_id"},
	{"token",          P_OPTIONAL, "v_token"},
	{"remote_addr",    P_OPTIONAL, "v_ip"},
	{"remote_browser", P_OPTIONAL, "v_remote_browser"},
	{"service_order",  P_OPTIONAL, "v_pack_service_order"},
	{"service_amount", P_OPTIONAL, "v_price"},
	{"retries",        P_OPTIONAL, "v_integer"},
	{NULL, 0}
};

static struct factory_data data = {
	"pack",
	FACTORY_RES_SHORT_FORM
};

static const struct factory_action execute_pack2if = FA_SQL("", "pgsql.master", "sql/call_pack2if.sql", FASQL_OUT);

static const struct factory_action *
check_status(struct cmd *cs, struct bpapi *ba) {
	const char *status = bpapi_get_element(ba, "status", 0);

	// Is not inactive then run and return
	if (status && strcmp(status, "inactive")) {
		return &execute_pack2if;
	}

	// Is inactive queue next execution
	int retries = 0;
	if (cmd_haskey(cs, "retries")) {
		retries = atoi(cmd_getval(cs, "retries"));
	}

	int max_retries = bconf_get_int_default(bconf_root, "*.*.pack2if.retries", 60);
	if (retries >= max_retries) {
		transaction_logprintf(cs->ts, D_ERROR,"PACK2IF MAX RETRIES REACHED FOR ad_id %s", cmd_getval(cs, "ad_id"));
		return NULL;
	}

	transaction_logprintf(cs->ts, D_DEBUG, "The ad is pending_review, i'm going to queue this execution");
	char *action_id = NULL;
	if (cmd_haskey(cs, "action_id")) {
		xasprintf(&action_id, "action_id:%s\n", cmd_getval(cs, "action_id"));
	}
	char *token = NULL;
	if (cmd_haskey(cs, "token")) {
		xasprintf(&token, "token:%s\n", cmd_getval(cs, "token"));
	}
	char *remote_addr = NULL;
	if (cmd_haskey(cs, "remote_addr")) {
		xasprintf(&remote_addr, "remote_addr:%s\n", cmd_getval(cs, "remote_addr"));
	}

	char *remote_browser = NULL;
	if (cmd_haskey(cs, "remote_browser")) {
		xasprintf(&remote_browser, "remote_browser:%s\n", cmd_getval(cs, "remote_browser"));
	}

	char *service_order = NULL;
	if (cmd_haskey(cs, "service_order")) {
		xasprintf(&service_order, "service_order:%s\n", cmd_getval(cs, "service_order"));
	}

	char *service_amount = NULL;
	if (cmd_haskey(cs, "service_amount")) {
		xasprintf(&service_amount, "service_amount:%s\n", cmd_getval(cs, "service_amount"));
	}
	transaction_logprintf(cs->ts, D_DEBUG, "params checked");

	char *pack2if_cmd = NULL;
	retries = retries + 1;
	xasprintf(&pack2if_cmd,
			"cmd:pack2if\n"
			"ad_id:%s\n"
			"%s"
			"%s"
			"%s"
			"%s"
			"%s"
			"%s"
			"retries:%d\n"
			"commit:1\n",
			cmd_getval(cs, "ad_id"),
			(action_id)?action_id:"",
			(token)?token:"",
			(remote_addr)?remote_addr:"",
			(remote_browser)?remote_browser:"",
			(service_order)?service_order:"",
			(service_amount)?service_amount:"",
			retries
	);
	transaction_logprintf(cs->ts, D_DEBUG,"command build");

	free(action_id);
	free(token);
	free(remote_addr);
	free(remote_browser);
	free(service_order);
	free(service_amount);

	transaction_logprintf(cs->ts, D_DEBUG, "memory released");
	int next_execution = bconf_get_int_default(bconf_root, "*.*.pack2if.interval", 60);
	const char *send_to_queue = bconf_get_string_default(bconf_root, "*.*.pack2if.queue", "queue:AT");
	// Add the next command to the corresponding queue
	transaction_logprintf(cs->ts, D_DEBUG,"next_execution in %d", next_execution);
	transaction_internal_printf(NULL, NULL,
			"cmd:queue\n"
			"commit:1\n"
			"%s\n"
			"timeout:%d\n"
			"blob:%zu:command\n"
			"%s\n",
			send_to_queue,
			next_execution,
			strlen(pack2if_cmd),
			pack2if_cmd);
	transaction_logprintf(cs->ts, D_DEBUG,"command sent: \n%s\n", pack2if_cmd);
	free(pack2if_cmd);
	return NULL;
}

static const struct factory_action actions[]= {
	FA_SQL("", "pgsql.master", "sql/ad_status.sql", 0),
	FA_COND(check_status),
	{0}
};

FACTORY_TRANS(pack2if, parameters, data, actions);
