#include "factory.h"
#include "bpapi.h"
#include <bconf.h>

extern struct bconf_node *bconf_root;

static struct c_param parameters[] = {
	{NULL, 0}
};

static int
call_send_disabled_reminder_mail(struct cmd *cs, struct bpapi *api) {

	int len = bpapi_length(api, "ad_id");
	int i;

	transaction_printf(cs->ts, "rows:%d\n", len);

	for (i = 0; i < len; i++){
		transaction_internal_printf(TRANSACTION_INTERNAL_SYNC_CB, NULL,
				"cmd:admail\n"
				"ad_id:%s\n"
				"action_id:%s\n"
				"mail_type:accept\n"
				"commit:1\n",
				bpapi_get_element(api, "ad_id", i),
				bpapi_get_element(api, "action_id", i)
		);
	}

	return 0;
}

static struct factory_data data = {
	"disabled_ads",
	FACTORY_NO_RES_CNTR | FACTORY_EMPTY_RESULT_OK | FACTORY_RES_SHORT_FORM
};

static const struct factory_action actions[] = {
       FA_SQL(NULL, "pgsql.master", "sql/get_disabled_reminder_expiration.sql", 0),
       FA_FUNC(call_send_disabled_reminder_mail),
       FA_DONE()
};

FACTORY_TRANS(disabled_reminder, parameters, data, actions);
