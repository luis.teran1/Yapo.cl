#include "factory.h"

static struct c_param parameters[] = {
	{"account_id;email", P_XOR, "v_account_id;v_email"},
	{"list_all", P_OPTIONAL, "v_bool"},
	{NULL, 0}
};

static struct factory_data data = {
        "get_packs_by_account",
         FACTORY_EMPTY_RESULT_OK | FACTORY_RES_SHORT_FORM
};

static const struct factory_action actions[]= {
	FA_SQL("", "pgsql.slave", "sql/get_packs_by_account.sql", FASQL_OUT),
	{0}
};

FACTORY_TRANS(get_packs_by_account, parameters, data, actions);
