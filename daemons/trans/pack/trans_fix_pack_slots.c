#include "factory.h"
#include "utils/refresh_session.h"

static struct c_param parameters[] = {
	{"email", P_REQUIRED, "v_email"},
	{"token",P_REQUIRED, "v_token"},
	{"remote_addr", P_OPTIONAL, "v_ip"},
	{"remote_browser", P_OPTIONAL, "v_remote_browser"},
	{NULL, 0}
};

static struct factory_data data = {
	"pack",
	FACTORY_RES_SHORT_FORM
};

static const struct factory_action actions[]= {
	FA_SQL("", "pgsql.master", "sql/call_fix_pack_slots.sql", FASQL_OUT),
	FA_FUNC(refresh_account),
	{0}
};

FACTORY_TRANS(fix_pack_slots, parameters, data, actions);
