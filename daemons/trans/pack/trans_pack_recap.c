#include "factory.h"

static struct c_param parameters[] = {
	{"account_id;email", P_XOR, "v_account_id;v_email"},
	{NULL, 0}
};

static struct factory_data data = {
	"pack_recap",
    FACTORY_RES_SHORT_FORM
};

static const struct factory_action actions[]= {
	FA_SQL("", "pgsql.master", "sql/pack_recap.sql", FASQL_OUT),
	{0}
};

FACTORY_TRANS(pack_recap, parameters, data, actions);
