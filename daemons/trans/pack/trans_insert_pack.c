#include "factory.h"
#include "utils/refresh_session.h"

static struct c_param parameters[] = {
	{"account_id", P_REQUIRED, "v_account_id"},
	{"type", P_REQUIRED, "v_pack_type"},
	{"slots", P_REQUIRED, "v_pack_slots"},
	{"period;expiration_date",P_XOR, "v_pack_period;v_pack_expiration_date"},
	{"product_id", P_REQUIRED, "v_pack_product_id"},
	{"payment_group_id", P_OPTIONAL, "v_integer"},
	{"token",P_OPTIONAL, "v_token"},
	{"service_order", P_OPTIONAL, "v_pack_service_order"},
	{"service_amount", P_OPTIONAL, "v_price"},

	{"remote_addr", P_OPTIONAL, "v_ip"},
	{"remote_browser", P_OPTIONAL, "v_remote_browser"},
	{NULL, 0}
};

static struct factory_data data = {
	"insert_pack",
	FACTORY_RES_SHORT_FORM
};

static const struct factory_action actions[]= {
	FA_SQL("", "pgsql.master", "sql/call_insert_pack.sql", FASQL_OUT),
	FA_FUNC(refresh_account),
	FA_TRANS("trans/flush_pack_delete.txt", "fat_", FATRANS_OUT),
	{0}
};

FACTORY_TRANS(insert_pack, parameters, data, actions);
