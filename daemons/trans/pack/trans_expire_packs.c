#include <bconf.h>
#include <bconfig.h>
#include <bsapi.h>
#include <ctemplates.h>
#include <ctype.h>
#include <event.h>
#include <hiredis.h>
#include <stdio.h>
#include <string.h>
#include <time.h>

#include "trans.h"
#include "util.h"
#include "validators.h"
#include "utils/redis_connect.h"
#include "utils/refresh_session.h"

static struct c_param parameters[] = {
	{"pack_id", P_OPTIONAL, "v_integer"},
	{NULL, 0}
};

void call_expire_packs(struct cmd *cs);
// Enum to allow us to iterate through results easily
typedef enum {
	PROP_ACCOUNT_ID = 0,
	PROP_EMAIL,
	PROP_SLOTS_EXPIRED,
	PROP_AVAILABLE_SLOTS,
	PROP_DISABLED_ADS
} expired_pack_prop_t;

// Header for the different result columns
const char *expired_pack_prop_name[] = {
	"account_id",
	"email",
	"slots_expired",
	"available_slots",
	"disabled_ads"
};

// Get ads from db and call asearch
void call_expire_packs(struct cmd *cs) {
	struct bpapi ba;
	struct sql_worker *worker = NULL;
	const char *errstr = NULL;

	BPAPI_INIT(&ba, NULL, pgsql);
	if (cmd_getval(cs, "pack_id") != NULL) {
		bpapi_insert(&ba, "pack_id", cmd_getval(cs, "pack_id"));
	}
	worker = sql_blocked_template_query("pgsql.master", 0, "sql/call_expire_packs.sql", &ba, cs->ts->log_string, &errstr);
	bpapi_free(&ba);

	if (worker && sql_blocked_next_result(worker, &errstr) == 1) {
		if (worker->rows(worker) > 0 && worker->next_row(worker)) {
			do {
				for (int col = 0; col < worker->fields(worker); col++) {
					const char *value = worker->get_value(worker, col);
					transaction_printf(cs->ts, "%s:%s\n", expired_pack_prop_name[col], value);
					if (col == PROP_EMAIL) {
						BPAPI_INIT(&ba, NULL, pgsql);
						bpapi_insert(&ba, "email", value);
						refresh_account(cs, &ba);
						bpapi_free(&ba);
					}
				}
			}
			while (worker->next_row(worker));
		}
	}

	if (errstr) {
			cs->message = xstrdup(errstr);
			cs->status = "TRANS_ERROR";
			cs->error = "UNHANDLED_ERROR";
	}

	if (worker) {
		sql_worker_put(worker);
	}

	cmd_done(cs);
}

ADD_COMMAND(expire_packs,			/* Name of command */
			NULL,						/* I don't know add it if you know */
            call_expire_packs,					/* Main function name */
            parameters,					/* Parameters struct */
            "get account ads command");	/* Command description */
