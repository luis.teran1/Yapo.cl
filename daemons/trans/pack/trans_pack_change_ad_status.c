#include "factory.h"
#include "utils/refresh_session.h"

static struct c_param parameters[] = {
	{"account_id;email;token", P_XOR, "v_account_id;v_email;v_token"},
	{"list_id;ad_id", P_XOR, "v_list_id;v_id"},
	{"new_status", P_REQUIRED, "v_array:disabled,active"},
	{"remote_addr",    P_REQUIRED, "v_ip"},
	{"remote_browser", P_OPTIONAL, "v_remote_browser"},
	{NULL, 0}
};

static struct factory_data data = {
        "pack_change_ad_status",
        FACTORY_RES_SHORT_FORM
};

static const struct factory_action actions[]= {
	FA_SQL("", "pgsql.master", "sql/call_pack_change_ad_status.sql", FASQL_OUT),
	FA_FUNC(refresh_account),
	{0}
};

FACTORY_TRANS(pack_change_ad_status, parameters, data, actions);

