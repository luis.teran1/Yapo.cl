#include "factory.h"
#include "bpapi.h"
#include <bconf.h>
#include "fd_pool.h"
#include "sredisapi.h"
#include "utils/redis_trans_context.h"

static struct c_param parameters[] = {
	{"user_id", P_REQUIRED, "v_integer"},
	{NULL, 0}
};

static struct factory_data data = {
	"controlpanel",
	FACTORY_NO_RES_CNTR | FACTORY_RES_SHORT_FORM
};

static const struct factory_action actions[] = {
	FA_SQL(NULL, "pgsql.slave", "sql/pro_adreply_report.sql", 0),
	FA_OUT("trans/pro_ad_reply_report.txt"),
	FA_DONE()
};

FACTORY_TRANS(pro_adreply_report, parameters, data, actions);
