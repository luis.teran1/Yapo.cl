#include "factory.h"
#include "bpapi.h"
#include <bconf.h>

extern struct bconf_node *bconf_root;

static struct c_param parameters[] = {
	{"from",		0,			"v_string_isprint"},
	{"to",			P_REQUIRED,	"v_email_basic"},
	{"name",		P_REQUIRED,	"v_name"},
	{"product_ids",	P_REQUIRED,	"v_string_isprint"},
	{"expires",		P_REQUIRED,	"v_string_isprint"},
	{NULL, 0}
};


static struct factory_data data = {
	"mail"
};

static const struct factory_action actions[] = {
        FA_MAIL("mail/pack_purchased.mime"),
        {0}
};

FACTORY_TRANS(pack_purchased_mail, parameters, data, actions);
