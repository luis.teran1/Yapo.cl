#include "factory.h"
#include "bpapi.h"
#include <bconf.h>

static struct c_param parameters[] = {
	{NULL, 0}
};

static struct factory_data data = {
	"inmo_pro_users",
	FACTORY_NO_RES_CNTR | FACTORY_RES_SHORT_FORM
};

static const struct factory_action actions[] = {
	FA_SQL(NULL, "pgsql.slave", "sql/call_get_pro_users_packs_inmo.sql", 0),
	FA_MAIL("mail/pro_users.mime"),
	FA_DONE()
};

FACTORY_TRANS(inmo_pro_users, parameters, data, actions);

