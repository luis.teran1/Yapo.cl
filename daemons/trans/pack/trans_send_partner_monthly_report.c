#include "factory.h"
#include "bpapi.h"
#include <bconf.h>
#include "fd_pool.h"
#include "sredisapi.h"
#include "utils/redis_trans_context.h"

static struct c_param parameters[] = {
	{"email", P_REQUIRED, "v_email"},
	{"month", P_REQUIRED, "v_month"},
	{"year", P_REQUIRED, "v_year"},
	{"partner", P_REQUIRED, "v_string_isprint"},
	{NULL, 0}
};

static struct factory_data data = {
	NULL,
	FACTORY_NO_RES_CNTR | FACTORY_RES_SHORT_FORM
};

static const struct factory_action monthly_actions[] = {
	FA_SQL(NULL, "pgsql.slave", "sql/partner_monthly_report.sql", 0),
	FA_MAIL("mail/partner_monthly_report.mime"),
	FA_DONE()
};

FACTORY_TRANS(send_partner_monthly_report, parameters, data, monthly_actions);

static const struct factory_action active_actions[] = {
	FA_SQL(NULL, "pgsql.slave", "sql/partner_active_ads_report.sql", 0),
	FA_MAIL("mail/partner_active_report.mime"),
	FA_DONE()
};

FACTORY_TRANS(send_partner_active_report, parameters, data, active_actions);
