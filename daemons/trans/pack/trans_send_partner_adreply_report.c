#include "factory.h"
#include "bpapi.h"
#include <bconf.h>
#include "fd_pool.h"
#include "sredisapi.h"
#include "utils/redis_trans_context.h"

static struct c_param parameters[] = {
	{"email", P_REQUIRED, "v_email"},
	{"year", P_REQUIRED, "v_year"},
	{"month", P_REQUIRED, "v_month"},
	{"partner", P_REQUIRED, "v_string_isprint"},
	{NULL, 0}
};

static struct factory_data data = {
	"controlpanel",
	FACTORY_NO_RES_CNTR | FACTORY_RES_SHORT_FORM
};

static const struct factory_action actions[] = {
	FA_SQL(NULL, "pgsql.slave", "sql/send_partner_adreply_report.sql", 0),
	FA_MAIL("mail/partner_adreply_report.mime"),
	FA_DONE()
};

FACTORY_TRANS(send_partner_adreply_report, parameters, data, actions);
