#include "factory.h"
#include "bpapi.h"
#include <bconf.h>
#include "fd_pool.h"
#include "sredisapi.h"
#include "utils/redis_trans_context.h"

static struct c_param parameters[] = {
	{"session_hash", P_REQUIRED, "v_active_session_hash"},
	{"list_id;date", P_XOR, "v_list_id_of_active_session;v_date"},
	{"email", P_REQUIRED, "v_email"},
	{NULL, 0}
};

extern struct bconf_node *bconf_root;
static const struct factory_action send_email_report_action = FA_MAIL("mail/pack_adreply_report.mime");

static const struct factory_action *
check_results(struct cmd *cs, struct bpapi *ba) {
	const char *email = bpapi_get_element(ba, "sender_email", 0);
	if (email == NULL) {
		cs->status = "TRANS_ERROR";
		cs->error = "ADREPLY_REPORT_EMPTY_RESULT";
		return NULL;
	}
	return &send_email_report_action;
}

static int
get_email_from_session_hash(struct cmd *cs, struct bpapi *ba) {
	char *rkey = NULL;
	const char *hash = cmd_getval(cs, "session_hash");
	const char *prefix = bconf_value(bconf_vget(trans_bconf, "accounts", "session_redis_prefix", NULL));
	xasprintf(&rkey, "%s_session_%s", prefix, hash);
	struct fd_pool_conn *conn = redis_sock_conn(redis_session_pool.pool, "master");
	char *email = redis_sock_hget(conn, rkey, "email");
	fd_pool_free_conn(conn);
	free(rkey);
	if (!email) {
		cs->error = "EMAIL_NOT_FOUND";
		cs->status = "TRANS_ERROR";
		return 1;
	}
	bpapi_insert(ba, "session_email", email);
	free(email);
	return 0;
}

static struct factory_data data = {
	NULL,
	FACTORY_NO_RES_CNTR | FACTORY_EMPTY_RESULT_OK | FACTORY_RES_SHORT_FORM
};

static const struct factory_action actions[] = {
	FA_FUNC(get_email_from_session_hash),
	FA_SQL(NULL, "pgsql.slave", "sql/call_send_pack_adreply_report.sql", 0),
	FA_COND(check_results),
	FA_DONE()
};

FACTORY_TRANS(send_pack_adreply_report, parameters, data, actions);
