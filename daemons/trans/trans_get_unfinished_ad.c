#include "factory.h"

static struct c_param params[] = {
	{"unf_ad_id", P_REQUIRED, "v_integer"},
	{NULL,0} 
} ;

FACTORY_TRANSACTION(get_unfinished_ad, &config.pg_slave, "sql/get_unfinished_ad.sql", NULL, params, FACTORY_RES_SHORT_FORM);

