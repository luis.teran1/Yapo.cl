#!/bin/bash

BDIR=/opt/blocket
sms_send=0
sms_copy=0
regress=0
distribute_full=0
CTL_PREFIX=''
asearchhost=all
smssearchhost=all
smsmergehost=smssearch

if [ "${SHELLOPTS/xtrace//}" != "$SHELLOPTS" ]; then
	CTL_PREFIX="sh -x"
fi

for arg in $*; do
	if [ ${arg/=//} != $arg ]; then
		eval $arg
	else
		eval $arg=1
	fi
done

# Helper vars
if [ "$regress" = 1 ]; then
	smsmergehost=localhost
	smssearchhost=localhost
	asearchhost=localhost
fi
PERL5LIB="$BDIR/lib/perl5"
export PERL5LIB



# Update
$CTL_PREFIX $BDIR/bin/index_ctl update asearch

# SMS delta
if [ "$sms_send" = 1 -o "$sms_copy" = 1 ]; then
	$CTL_PREFIX $BDIR/bin/index_ctl distribute_delta asearch $smsmergehost receive_delta_nomerge
fi

# Distribute and merge
if [ "$distribute_full" = 1 ]; then
	$CTL_PREFIX $BDIR/bin/index_ctl distribute_full asearch $asearchhost
else
	$CTL_PREFIX $BDIR/bin/index_ctl distribute_delta asearch $asearchhost
fi

# Reload and send out accepted mails
if [ "$regress" = 1 ]; then
	kill -HUP $(< $BDIR/../.asearchpid)
else
	$CTL_PREFIX $BDIR/bin/index_ctl reload asearch $asearchhost
	sleep 50
	$BDIR/bin/trans_command.pl -configfile $BDIR/conf/bconf.conf "cmd:flushqueue\nqueue:accepted_mail\nmaxtime:`date -d '10 minutes ago' +\%s`\ncommit:1\nend\n"
	$BDIR/bin/trans_command.pl -configfile $BDIR/conf/bconf.conf "cmd:flushqueue\nqueue:bump_mail\ncommit:1\nend\n"
fi

# Send SMS
if [ "$sms_send" = 1 ]; then
	state_id=`$CTL_PREFIX $BDIR/bin/index_ctl merge_deltas asearch smssearch $smsmergehost | grep from_state_id | uniq`
	$CTL_PREFIX $BDIR/bin/index_ctl start_search smssearch $smssearchhost
	$BDIR/bin/trans_command.pl -configfile $BDIR/conf/bconf.conf "cmd:adwatch_run_sms_queries\ncommit:1\n$state_id\nend\n"
	$CTL_PREFIX $BDIR/bin/index_ctl stop_search smssearch $smssearchhost
fi

