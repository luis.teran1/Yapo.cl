# dispatcher.py
#
# MessageDispatcher abstract base class.
# Core functionality and main loop implementation.
# subclasses must implement: get_input, send_output
#

import sys
import syslog
import traceback
from abc import ABCMeta, abstractmethod


class NoLogFileException(Exception):
    pass


class DispatcherInput(object):
    __metaclass__ = ABCMeta

    @abstractmethod
    def get_input(self):
        raise NotImplementedError('MessageClass.get_input not implemented')

    def ack(self, message, ok):
        pass

    def keep_open(self):
        return None

    def log_stats(self):
        pass


class DispatcherOutput(object):
    __metaclass__ = ABCMeta

    @abstractmethod
    def send_output(self, msg):
        raise NotImplementedError('MessageClass.send_output not implemented')

    def keep_open(self):
        return None

    def deferred_ack(self):
        return False

    def log_stats(self):
        pass


class MessageDispatcher(object):

    def __init__(self, input, output):
        self.input = input
        self.output = output
        self.print_stats = False

    def request_stats(self):
        self.print_stats = True

    def stats(self):
        syslog.syslog(syslog.LOG_INFO, "dispatcher: Le stats")
        self.input.log_stats()
        self.output.log_stats()
        self.print_stats = False

    def work(self):
        syslog.syslog(syslog.LOG_INFO, "dispatcher: starting work loop")
        deferred_ack = self.output.deferred_ack()
        for msg in self.input.get_input():
            if self.print_stats:
                self.stats()
            if deferred_ack:
                self.output.send_output(msg, self.input.ack)
            else:
                ok = self.output.send_output(msg)
                self.input.ack(msg, ok)
        syslog.syslog(syslog.LOG_INFO, "dispatcher: EOF reached, terminating")
