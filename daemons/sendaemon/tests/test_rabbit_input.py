import pika
import unittest

# tested module
import rabbit_input


class RabbitInput(unittest.TestCase):

    @classmethod
    def rabbitmq_conn(cls):
        auth = pika.credentials.PlainCredentials(username='guest', password='guest')
        params = pika.connection.ConnectionParameters(host='localhost', port=20272, virtual_host='/', credentials=auth)
        conn = pika.BlockingConnection(params)
        channel = conn.channel()
        return conn, channel

    def setUp(self):
        self.conn, self.channel = self.__class__.rabbitmq_conn()
        self.channel.exchange_declare(exchange='test', exchange_type='topic', passive=False, durable=True)
        self.channel.queue_declare(queue='test', passive=False, durable=False, exclusive=False, auto_delete=False)
        self.channel.queue_bind(queue='test', exchange='test', routing_key='test')

    def tearDown(self):
        self.channel.queue_delete(queue='test')
        self.channel.close()
        self.conn.close()

    def test_receive_one(self):
        body = 'Le test'
        self.channel.basic_publish('test', 'test', body)
        input = rabbit_input.RabbitInput(port=20272)
        iter = input.get_input()
        data = iter.next()
        self.assertEquals(data['body'], body)

    def test_receive_many(self):
        N = 100;
        body = 'Le test {0}'
        for k in xrange(N):
            self.channel.basic_publish('test', 'test', body.format(k))
        input = rabbit_input.RabbitInput(port=20272)
        iter = input.get_input()
        for k in xrange(N):
            data = iter.next()
            self.assertEquals(data['body'], body.format(k))

    def test_nack(self):
        body = 'Le test'
        other = 'Le other'
        self.channel.basic_publish('test', 'test', body)
        input = rabbit_input.RabbitInput(port=20272)
        iter = input.get_input()

        data = iter.next()
        self.assertEquals(data['body'], body)
        self.assertFalse(data['method'].redelivered)
        input.ack(data, False)
        self.channel.basic_publish('test', 'test', other)

        data = iter.next()
        self.assertEquals(data['body'], body)
        self.assertTrue(data['method'].redelivered)

    def test_ack(self):
        body = 'Le test'
        other = 'Le other'
        self.channel.basic_publish('test', 'test', body)
        input = rabbit_input.RabbitInput(port=20272)
        iter = input.get_input()

        data = iter.next()
        self.assertEquals(data['body'], body)
        self.assertFalse(data['method'].redelivered)
        input.ack(data, True)
        self.channel.basic_publish('test', 'test', other)

        data = iter.next()
        self.assertEquals(data['body'], other)
        self.assertFalse(data['method'].redelivered)
