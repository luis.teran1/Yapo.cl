# Echo server program
import socket
import sys
import threading

HOST = None
PORT = 20205

def serve(conn, addr):
    print 'Connected by', addr
    data = conn.recv(4096)
    print 'got', len(data), 'bytes!'
    n = conn.send(data)
    print 'sent', n, 'bytes!'
    conn.close()

def threaded_serve(conn, addr):
    thr = threading.Thread(target=serve, args=(conn, addr))
    thr.daemon = True
    thr.start()

def accept_loop(host, port):
    s = None
    for res in socket.getaddrinfo(host, port, socket.AF_UNSPEC,
                                socket.SOCK_STREAM, 0, socket.AI_PASSIVE):
        af, socktype, proto, canonname, sa = res
        try:
            s = socket.socket(af, socktype, proto)
            s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        except socket.error as msg:
            s = None
            continue
        try:
            s.bind(sa)
            s.listen(1)
        except socket.error as msg:
            s.close()
            s = None
            continue
        break
    if s is None:
        print 'could not open socket'
        sys.exit(1)

    print "Listening on ", host, port
    while True:
        conn, addr = s.accept()
        threaded_serve(conn, addr)

accept_loop(HOST, PORT)
