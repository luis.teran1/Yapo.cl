import threading
import unittest

# tested module
import trans_output


class SyncTransOutput(unittest.TestCase):

    def test_send_sync_trans_ok(self):
        data = {'body': 'status:TRANS_OK'}
        out = trans_output.TransOutput('localhost', 20205, 1)
        ok = out.send_output(data)
        self.assertTrue(ok)

    def test_send_sync_trans_error(self):
        data = {'body': 'status:TRANS_ERROR'}
        out = trans_output.TransOutput('localhost', 20205, 1)
        ok = out.send_output(data)
        self.assertTrue(ok)

    def test_send_sync_no_status(self):
        data = {'body': 'no_status_for_you:loser'}
        out = trans_output.TransOutput('localhost', 20205, 1)
        ok = out.send_output(data)
        self.assertFalse(ok)


class AsyncTransOutput(unittest.TestCase):

    def setUp(self):
        self.sem = threading.Semaphore(0)

    def ack_fn(self, msg, ok, expected):
        try:
            self.assertEquals(ok, expected)
        finally:
            self.sem.release()

    def ack_ok_fn(self, msg, ok):
        self.ack_fn(msg, ok, True)

    def ack_nok_fn(self, msg, ok):
        self.ack_fn(msg, ok, False)

    def test_send_async_trans_ok(self):
        data = {'body': 'status:TRANS_OK'}
        out = trans_output.TransOutput('localhost', 20205, 1)
        out.deferred_ack()
        out.send_output(data, self.ack_ok_fn)
        self.sem.acquire()

    def test_send_async_trans_error(self):
        data = {'body': 'status:TRANS_ERROR'}
        out = trans_output.TransOutput('localhost', 20205, 1)
        out.deferred_ack()
        out.send_output(data, self.ack_ok_fn)
        self.sem.acquire()

    def test_send_async_trans_no_status(self):
        data = {'body': 'no_status_for_you:loser'}
        out = trans_output.TransOutput('localhost', 20205, 1)
        out.deferred_ack()
        out.send_output(data, self.ack_nok_fn)
        self.sem.acquire()

    def test_many_requests(self):
        N = 5000
        data = {'body': 'status:TRANS_OK'}
        out = trans_output.TransOutput('localhost', 20205, 10, check_interval=0.1)
        out.deferred_ack()
        for i in xrange(N):
            out.send_output(data, self.ack_ok_fn)

        for i in xrange(N):
            self.sem.acquire()
