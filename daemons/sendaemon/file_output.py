# file_output.py
#
# Console module with stdin input and stdout output.
# Input should only consist of json-one-liners.
# Dispatcher will terminate when EOF is reached
#

import sys
import syslog
from dispatcher import DispatcherOutput


class FileOutput(DispatcherOutput):

    def __init__(self, filename):
        syslog.syslog(syslog.LOG_INFO, "Starting output: file > {0}".format(filename))
        self.file = open(filename, 'a')

    def __del__(self):
        self.file.close()

    def send_output(self, json_msg):
        print >> self.file, "{0}".format(json_msg)
        self.file.flush()
        return True

    def keep_open(self):
        return self.file
