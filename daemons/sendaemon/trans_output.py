# trans_output.py
#
# Input will be send AS IS to configured trans server
# Whenever we get a response with status, send_output will return True
# The nature of this module is asynchronous. One thread = one connection.
#

import socket
import sys
import syslog
import threading
import time
from stats import Stats
from dispatcher import DispatcherOutput


class TransOutput(DispatcherOutput):

    def __init__(self, host, port, maxthreads, check_interval=1):
        syslog.syslog(syslog.LOG_INFO, "Starting output: trans({0}, {1})".format(host, port))
        self.addr = (host, port)
        self.maxthreads = int(maxthreads)
        self.deferred = False
        self.check_interval = int(check_interval)
        self.stats = Stats()

    def log_stats(self):
        for line in self.stats.stats():
            syslog.syslog(syslog.LOG_INFO, "TransOutput: {0}".format(line))
        syslog.syslog(syslog.LOG_INFO, "TransOutput: Active threads: {0}".format(len(self.threads)))

    def deferred_ack(self):
        if self.deferred:
            return True

        self.setup_ack_thread()
        self.deferred = True
        return True

    def setup_ack_thread(self):
        syslog.syslog(syslog.LOG_INFO, "TransOutput: setting up result thread")
        self.threads = []
        self.ack_lock = threading.Lock()
        self.thread_sem = threading.BoundedSemaphore(self.maxthreads)
        self.ack_thread = threading.Thread(target=self.ack_loop)
        self.ack_thread.setDaemon(True)
        self.ack_thread.start()

    def ack_loop(self):
        syslog.syslog(syslog.LOG_INFO, "TransOutput: Ack thread is alive")
        loops = 0
        while True:
            loops += 1
            time.sleep(self.check_interval)
            self.check_results()
            if loops >= 60:
                loops = 0
                self.log_stats()

    def check_results(self):
        with self.ack_lock:
            if not self.threads:
                return

            remaining = []
            for thr in self.threads:
                if thr.is_alive():
                    remaining.append(thr)
                else:
                    thr.join()
                    self.thread_sem.release()
            self.threads = remaining
        self.stats.register('THREADS', float(len(remaining)))

    def send_output(self, json_msg, ack_fn=None):
        if not self.deferred or not ack_fn:
            return bool(_send_trans(self.addr, json_msg['body']))

        thr = threading.Thread(
            target=self.async_send_output,
            args=(json_msg, ack_fn)
        )

        # Honor maxthreads
        self.thread_sem.acquire()
        thr.start()

        with self.ack_lock:
            self.threads.append(thr)

    def async_send_output(self, json_msg, ack_fn):
        start = time.time()
        status = _send_trans(self.addr, json_msg['body'])

        self.stats.register(status, time.time() - start)
        ok = bool(status)

        start = time.time()
        ack_fn(json_msg, ok)
        self.stats.register('ACK', time.time() - start)


def _send_trans(addr, body):
    sock = socket.create_connection(addr)
    try:
        sock.sendall(body)
        out = ''
        while True:
            r = sock.recv(4096)
            if not r:
                break
            out += r
    finally:
        sock.close()

    status = ''
    for line in out.splitlines():
        if line.startswith('status:'):
            status = line.split(':')[1]
            break

    if status != 'TRANS_OK':
        syslog.syslog(syslog.LOG_ERR, "TransOutput: ERROR [{0}] ==> [{1}]".format(repr(body), repr(out)))
    else:
        syslog.syslog(syslog.LOG_INFO, "TransOutput: INFO [{0}] ==> [{1}]".format(repr(body), repr(out)))

    return status
