"""
debug.py

Debug routines and features:
+ Dump exceptions to syslog: enable_threaded_exception_dump()
+ Dump all threads backtraces to syslog: dump_all_threads(level)
"""

import sys
import syslog
import threading
import traceback as tb


def list_to_syslog(level, list):
    for multiline in list:
        for line in multiline.splitlines():
            syslog.syslog(level, "!! {0}".format(line))


def dump_exception(type, value, traceback):
    syslog.syslog(syslog.LOG_ERR, "{0}: Unhandled exception ocurred".format(sys.argv[0]))
    list_to_syslog(syslog.LOG_ERR, tb.format_exception(type, value, traceback))
    syslog.syslog(syslog.LOG_ERR, "{0}: Exception logging done. Terminating thread".format(sys.argv[0]))


def dump_thread(level, thread_id, stack):
    syslog.syslog(level, "ThreadID: {0}".format(thread_id))
    list_to_syslog(level, tb.format_list(tb.extract_stack(stack)))


def dump_all_threads(level):
    syslog.syslog(syslog.LOG_ERR, "{0}: Dumping all threads".format(sys.argv[0]))
    for thread_id, stack in sys._current_frames().items():
        dump_thread(level, thread_id, stack)


def enable_threaded_excepthook():
    """
    Workaround for sys.excepthook thread bug
    From http://spyced.blogspot.com/2007/06/workaround-for-sysexcepthook-bug.html
    (https://sourceforge.net/tracker/?func=detail&atid=105470&aid=1230540&group_id=5470).

    Call once from __main__ before creating any threads.
    If using psyco, call psyco.cannotcompile(threading.Thread.run)
    since this replaces a new-style class method.

    Taken from: http://bugs.python.org/issue1230540
    """
    init_old = threading.Thread.__init__

    def init(self, *args, **kwargs):
        init_old(self, *args, **kwargs)
        run_old = self.run

        def run_with_except_hook(*args, **kw):
            try:
                run_old(*args, **kw)
            except (KeyboardInterrupt, SystemExit):
                raise
            except:
                sys.excepthook(*sys.exc_info())
        self.run = run_with_except_hook
    threading.Thread.__init__ = init


def enable_threaded_exception_dump():
    sys.excepthook = dump_exception
    enable_threaded_excepthook()
