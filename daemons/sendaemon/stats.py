# stats.py
#
# Stat accumulation for arbitrary events
#

import sys


class Stat(object):

    def __init__(self, name):
        self.name = name
        self.data = {}
        self.min = sys.maxint
        self.avg = 0
        self.max = 0
        self.total = 0
        self.count = 0

    def register(self, duration):
        if duration < self.min:
            self.min = duration
        if duration > self.max:
            self.max = duration
        self.count += 1
        self.total += duration

    def stat_str(self):
        return '{0} min = {1}, max = {2}, avg = {3}, count = {4}'.format(
            self.name, self.min, self.max, self.total / self.count, self.count
        )


class Stats(object):

    def __init__(self):
        self.events = {}

    def register(self, event, duration):
        if event not in self.events:
            self.events[event] = Stat(event)
        self.events[event].register(duration)

    def stats(self):
        return [e.stat_str() for _, e in self.events.items()]
