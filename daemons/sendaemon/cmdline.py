# cmdline.py
#
# Command line parsing and argument handling code
#

import getopt
import logging
import os
import sys
import textwrap
from pwd import getpwnam
from conf import Conf


class Args(object):

    def __init__(self):
        try:
            opts, args = getopt.getopt(sys.argv[1:], "hi:u:f:", ["help", "instance=", "user=", "conf="])
        except getopt.GetoptError as err:
            print >> sys.stderr, str(err)
            Args.usage(2)

        self.conf = None
        self.instance = None
        self.uid = os.getuid()

        for o, v in opts:
            if o in ("-h", "--help"):
                Args.usage()
            elif o in ("-i", "--instance"):
                self.instance = v
            elif o in ("-u", "--user"):
                try:
                    uid = getpwnam(v).pw_uid
                except KeyError:
                    print >> sys.stderr, "Invalid user '{0}'".format(v)
                    sys.exit(2)
                self.uid = uid
            elif o in ("-f", "--conf"):
                self.conf = Conf(v)
            else:
                print >> sys.stderr, "Invalid option: {0}".format(o)
                Args.usage(2)

        if self.conf is None:
            print >> sys.stderr, "{0}: Please tell me which conf file shall I load".format(sys.argv[0])
            Args.usage(2)

        if self.instance is None:
            print >> sys.stderr, "{0}: Please tell me which bconf instance shall I load".format(sys.argv[0])
            Args.usage(2)

    @staticmethod
    def usage(exitcode=0):
        print >> sys.stderr, textwrap.dedent(
            """
            Usage: sendaemon.py -i INSTANCE -f CONF_FILE [-u USER]
              or:  sendaemon.py --instance INSTANCE --conf CONF_FILE [--user USER]
            Start sendaemon process, according to the bconf instance INSTANCE
            An optional user can be passed to run with, instead of the current one

            Mandatory arguments to long options are mandatory for short options too.
                -h, --help                      this incredible helpful message
                -i, --instance=INSTANCE         loads sendaemon.INSTANCE conf to set itself up
                -f, --conf=CONF_FILE            loads given conf file to setup instance
                -u, --user=USER                 tries to run process as user USER instead of the calling user

            The specified conf instance is loaded and used to set up the daemon.
               sendaemon.INSTANCE
            Daemon configuration is specified in the conf object:
               sendaemon.INSTANCE.conf
            Input module is speficied in the input object:
               sendaemon.INSTANCE.input
            Output module is speficied in the output object:
               sendaemon.INSTANCE.output

            Available input modules are:
                dummy                           Generates defined messages on periodic intervals
                rabbit                          Reads input from a RabbitMQ queue

            Available output modules are:
                file                            Output is appended to a file
            """)
        sys.exit(exitcode)
