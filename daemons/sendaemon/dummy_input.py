# dummy_input.py
#
# A dummy dispatcher class with no input and stdout output
#

import syslog
import time
from dispatcher import DispatcherInput


class DummyInput(DispatcherInput):
    c = 0

    def __init__(self, interval=1, message='I read {0}'):
        syslog.syslog(syslog.LOG_INFO, "Starting input: dummy (msg every {0} seconds)".format(interval))
        self.interval = interval
        self.message = message

    def get_input(self):
        while True:
            time.sleep(self.interval)
            self.c += 1
            yield self.message.format(self.c)
