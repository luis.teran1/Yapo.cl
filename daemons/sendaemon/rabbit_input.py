# console_dispatcher.py
#
# Console module with stdin input and stdout output.
# Input should only consist of json-one-liners.
# Dispatcher will terminate when EOF is reached
#

import json
import pika
import sys
import syslog
import threading
from pika.exceptions import ConnectionClosed
from dispatcher import MessageDispatcher
from dispatcher import DispatcherInput, DispatcherOutput


class RabbitInput(DispatcherInput):

    def __init__(self, username='guest', password='guest', host='localhost', port=5672, virtual_host='/', queue_name='test'):
        syslog.syslog(syslog.LOG_INFO, "Starting input: rabbitmq://{0}@{1}:{2}/ vhost [{3}] queue [{4}]".format(
            username, host, port, virtual_host, queue_name)
        )
        conn_credentials = pika.credentials.PlainCredentials(
            username=username,
            password=password
        )
        conn_params = pika.connection.ConnectionParameters(
            host=host,
            port=port,
            virtual_host=virtual_host,
            credentials=conn_credentials
        )
        self.rb_conn = pika.BlockingConnection(conn_params)
        self.rb_channel = self.rb_conn.channel()
        self.queue_name = queue_name
        self.ack_lock = threading.Lock()

    def declare_exchange(self, exchange, exchange_type, passive=False, durable=False):
        syslog.syslog(syslog.LOG_INFO, "rabbitmq: declaring exchange [{0},{1},{2},{3}]".format(
            exchange, exchange_type, passive, durable)
        )
        self.rb_channel.exchange_declare(
            exchange=exchange,
            exchange_type=exchange_type,
            passive=passive,
            durable=durable
        )

    def declare_queue(self, queue, passive=False, durable=False, exclusive=False, auto_delete=False):
        syslog.syslog(syslog.LOG_INFO, "rabbitmq: declaring queue [{0},{1},{2},{3},{4}]".format(
            queue, passive, durable, exclusive, auto_delete)
        )
        self.rb_channel.queue_declare(
            queue=queue,
            passive=passive,
            durable=durable,
            exclusive=exclusive,
            auto_delete=auto_delete
        )

    def declare_binding(self, queue, exchange, routing_key):
        syslog.syslog(syslog.LOG_INFO, "rabbitmq: declaring binding [{0},{1},{2}]".format(
            queue, exchange, routing_key)
        )
        self.rb_channel.queue_bind(
            queue=queue,
            exchange=exchange,
            routing_key=routing_key
        )

    def get_input(self):
        try:
            for method, properties, body in self.rb_channel.consume(self.queue_name):
                msg = {
                    'method': method,
                    'properties': properties,
                    'body': body
                }
                syslog.syslog(syslog.LOG_INFO, "rabbitmq: got [{0}]".format(msg))
                yield msg
        except ConnectionClosed as cc:
            syslog.syslog(syslog.LOG_ERR, "rabbitmq: connection dropped {0}".format(cc))

    def ack(self, msg, ok):
        ack_fn = self.rb_channel.basic_ack if ok else self.rb_channel.basic_nack
        with self.ack_lock:
            ack_fn(delivery_tag=msg['method'].delivery_tag)

    def keep_open(self):
        return self.rb_conn.socket
