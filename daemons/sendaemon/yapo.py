# yapo.py
#
# Code to interact with the yapo platform
#

import os
from libs import check_output


def genport(n):
    uid = os.getuid()
    port = (uid - (uid / 100) * 100) * 200 + 20000 + n
    return port

REGRESS_TRANS_PORT = genport(5)


class BConf(object):

    def __init__(self, trans_host, trans_port):
        self.bconf = BConf.load_bconf(trans_host, trans_port)

    def get(self, key):
        return self.bconf.get(key)

    @staticmethod
    def trans_host(flavor):
        return BConf.trans[flavor]

    @staticmethod
    def load_bconf(host, port):
        bc = trans('bconf', host=host, port=port)
        bconf = {}
        for thing in bc['conf']:
            kv = thing.split('=', 2)
            bconf[kv[0]] = kv[1]
        return bconf


def trans(cmd, commit=1, host='localhost', port=0, **kwargs):
    """Makes a request to trans and returns the results as a dict.
    Care is to be taken as keys and values are latin-1 in nature but
    returned as utf-8 by this function.
    """
    # set port
    # call trans process
    kwargs.update({'cmd': cmd, 'commit': commit})
    q = '\n'.join(map(lambda v: v[0] + ':' + str(v[1]), kwargs.items())) + '\nend\n'
    out = check_output('printf "{query}" | nc {host} {port}'.format(
        query=q, host=host, port=port), shell=True
    )
    # gather output
    result = {}
    for line in out.decode('latin-1').encode('utf-8').splitlines():
        s = str(line).split(':', 1)
        if len(s) >= 2:
            # if already existed, make it a list
            if s[0] in result:
                if isinstance(result[s[0]], str):
                    result[s[0]] = [result[s[0]]]
                result[s[0]].append(s[1])
            else:
                result[s[0]] = s[1]
    return result
