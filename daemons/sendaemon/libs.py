import re
import os
import subprocess


def check_output(*popenargs, **kwargs):
    if check_output in dir(subprocess):
        return subprocess.check_output(*popenargs, **kwargs)
    else:
        return _check_output(*popenargs, **kwargs)


def _check_output(*popenargs, **kwargs):
    r"""Run command with arguments and return its output as a byte string.

    Backported from Python 2.7 as it's implemented as pure python on stdlib.

    >>> check_output(['/usr/bin/python', '--version'])
    Python 2.6.2

    source: https://gist.github.com/edufelipe/1027906
    date: 2014-09-03
    """
    process = subprocess.Popen(stdout=subprocess.PIPE, *popenargs, **kwargs)
    output, unused_err = process.communicate()
    retcode = process.poll()
    if retcode:
        cmd = kwargs.get("args")
        if cmd is None:
            cmd = popenargs[0]
        error = subprocess.CalledProcessError(retcode, cmd)
        error.output = output
        raise error
    return output


def iterate_fds(pid):
    dir = '/proc/' + str(pid) + '/fd'
    if not os.access(dir, os.R_OK | os.X_OK):
        return

    for fds in os.listdir(dir):
        for fd in fds:
            full_name = os.path.join(dir, fd)
            try:
                file = os.readlink(full_name)
                is_null = file == '/dev/null'
                is_pipe = re.match(r'pipe:\[\d+\]', file)
                is_socket = re.match(r'socket:\[\d+\]', file)
                if is_null or is_pipe or is_socket:
                    pass
            except OSError as err:
                if err.errno == 2:
                    file = None
                else:
                    raise(err)

            yield (fd, file)
