/* eslint-env node */
/* eslint global-require:0 */
'use strict';

var exec = require('child_process').exec;
var config = require('./www/gulp-config');
var gulp = require('gulp');
var merge2 = require('merge2');
var path = require('path');
var buffer = require('vinyl-buffer');
var $ = require('gulp-load-plugins')({
  pattern: ['gulp-*', 'gulp.*']
});

var paths = {
  regress: config.regressDir,
  regressFinal: config.regressDestDir,
  deploy: config.deployDir,
  msite: 'msite/',
  img: 'www/img/',
  sass: 'www/sass/',
  js: 'www/js/',
  templates: 'templates/',
  ssl: {
    img: 'www-ssl/img/',
    js: 'www-ssl/js/'
  },
  components: 'src/frontend/components',
  frontendApp: 'src/frontend/',
  yapoLegacyFrontendDir: 'node_modules/@yapo-legacy-fe/sass/dist/'
};

// @TODO: read all directories of sprite-src and remove inline config
gulp.task('sprites', function spritesTask() {
  var sourcesSSL = path.join(paths.yapoLegacyFrontendDir, paths.ssl.img, '**/*.png');
  var sslDest = path.join(paths.regressFinal, paths.ssl.img);
  return gulp.src(sourcesSSL)
    .pipe(gulp.dest(sslDest));
});

gulp.task('sprites:dist', function spriteDist() {
  console.warn("WARNING: sprites:dist is not being used anymore");

  var nodePath = path.join(paths.yapoLegacyFrontendDir, paths.ssl.img, '**/*.png');
  var regressFinalPath = path.join(paths.regressFinal, paths.ssl.img);
  var copyToRegressFinal = gulp.src(nodePath)
      .pipe(gulp.dest(regressFinalPath));
  
  var sslDest = path.join(paths.deploy, paths.ssl.img);
  var sourcesSSL = path.join(paths.yapoLegacyFrontendDir, paths.ssl.img, '**/*.png');

  var copyToOptBlocket = gulp.src(sourcesSSL)
    .pipe(gulp.dest(sslDest));

  return merge2(copyToRegressFinal, copyToOptBlocket);
    
});

gulp.task('setProdNodeEnv', function setProdNodeEnv(done) {
  setNodeEnv('production');
  return done();
});

gulp.task('build', gulp.series(
  'setProdNodeEnv',
  'sprites:dist'
, function onBuild(done) {
  setNodeEnv('development');
  return done();
}));

function setNodeEnv(env) {
  process.env.NODE_ENV = env || 'production';
}

gulp.task('default', gulp.series('build'));
