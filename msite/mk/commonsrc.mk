# Define USE_MSITE_TEMPLATES
# if you want to use mobile site's
# templates / filters
ifdef USE_MSITE_TEMPLATES
USE_TRANS_FILTERS=yes
USE_BLOCKET_FUNCTIONS=yes
USE_REDIS_FILTERS=yes
USE_VTREE_FILTERS=yes
USE_SEARCH_FILTERS=yes
USE_JSON_FILTERS=yes
USE_LOOP_FILTERS=yes

USE_TEMPLATES += $(MSITEDIR)/templates/mobile

#SRCS += api_template_functions.c
#SRCS += api_template_filters.c
#SRCS += vtree_prefix_filter.c
#VPATH := $(MOBWEBDIR)/lib/template:${VPATH}
endif
