TOPDIR?=../../../

include ${TOPDIR}/mk/defvars.mk

# Depends on the mobile API
remsite reinstall-mobile-site:
	make -C ${MSITEDIR}/templates/mobile install DESTDIR=${REGRESS_FINALDIR} INSTALLROOT=${TOPDIR}/regress_final BUILD_STAGE=REGRESS
	make -C ${MSITEDIR}/www install DESTDIR=${REGRESS_FINALDIR} INSTALLROOT=${TOPDIR}/regress_final BUILD_STAGE=REGRESS
	make -C ${MSITEDIR}/conf install DESTDIR=${REGRESS_FINALDIR} INSTALLROOT=${TOPDIR}/regress_final BUILD_STAGE=REGRESS
	make -C ${TOPDIR} reml remt ti
	make  add-api-key

remsite-www:
	make -C ${MSITEDIR}/www install DESTDIR=${REGRESS_FINALDIR} INSTALLROOT=${TOPDIR}/regress_final BUILD_STAGE=REGRESS

remsite-bconf:
	make -C ${MSITEDIR}/conf install DESTDIR=${REGRESS_FINALDIR} INSTALLROOT=${TOPDIR}/regress_final BUILD_STAGE=REGRESS
	make -C ${TOPDIR} ti
