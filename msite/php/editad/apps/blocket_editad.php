<?php

require_once('autoload_lib.php');
require_once('blocket_application.php');
require_once('blocket_mobile_application.php');
require_once('ApiGatewayClass.php');
require_once('AccountUtil.php');
require_once('bAd.php');
require_once('util.php');

openlog("edit_mobile", LOG_ODELAY, LOG_LOCAL0);

/* Session for logged in users */
$account_session;

class blocket_editadapp extends blocket_mobile_application {
	public $list_id;
	public $ad_type;
	public $ht;
	public $ad;
	public $validated = false;
	public $publish_similar = false;
	public $pre_refuse_action_type;
	public $action_id = NULL;
	public $action = NULL;
	public $tmpl_data;
	public $salted_passwd;
	public $salt;
	public $is_mobile_app = false;
	public $old_category;

	function blocket_editadapp() {
		global $session;
		global $account_session;
		global $BCONF;
		$account_session = new AccountSession();
		$this->account_util = new AccountUtil();
		$this->mobile_appl = 'editad';
		$this->add_static_vars(array('list_id', 'ad'));
		$this->init('load', bconf_get($BCONF, "*.common.session.instances.ai"));
	}

	function get_state($statename) {
		switch($statename) {
		case 'load':
			return array(
				'function' => 'editad_load',
				'params'   => array('id' => 'f_newad_id', 'p' => 'f_string', 'ht' => 'f_integer', 'cmd' => 'f_string', 'is_mobile_app' => 'f_is_mobile_app'),
				'method'   => 'get',
			);
		case 'validacion':
			return array(
				'function' => 'editad_validacion',
				'method' => 'get'
			);
		case 'validate':
			return array(
				'function' => 'editad_validate',
				'method' => 'both',
				'params'   => array('id' => 'f_newad_id', 'p' => 'f_string'),
			);
		case 'formulario':
			return array(
				'function' => 'editad_formulario',
				'method' => 'get'
			);
		case 'blocked':
			return array(
				'function' => 'editad_blocked',
				'method' => 'get'
			);
		case 'similar':
			return array(
				'function' => 'editad_similar',
				'method' => 'get'
			);
		case 'submit':
			return array(
				'function' => 'editad_submit',
				'method' => 'post',
				'params' => array()
			);
		case 'error':
			return array('function' => 'editad_error',
				'method' => 'get'
			);
		}
	}

	function editad_load($id, $p, $ht, $cmd, $is_mobile_app = false) {
		global $session;
		$this->ad = new bAd();
		if (empty($id))
			$this->list_id = @$_SESSION['static_data']['blocket_editad']['list_id'];
		elseif (!empty($id['id']))
			$this->list_id = $id['id'];

		$this->action_id = @$id['action_id'];
		$this->action = @$id['action'];
		if ((!empty($this->action) && $this->action == 'fromcmd') || !empty($cmd))
			$this->action = $cmd;

		$this->ht = !empty($ht) ? $ht : bconf_get($BCONF, '*.ai.source.default_mobile');
		$this->ad_type = !empty($this->action) ? $this->action : 'edit';
		$this->tmpl_data = $this->get_tmpl_data($this->list_id, $this->action_id, $this->action);

		if($is_mobile_app == true){
			$this->is_mobile_app = true;
			$this->tmpl_data['display_for_app'] = "true";
		}

		$ad_status = $this->tmpl_data['ad_status'];

		if (in_array($ad_status, array('deleted', 'refused')) && $this->action != "refused") {
			//validate that ad was deleted
			return 'error';
		}

		if (empty($this->list_id)) {
			// error handling
			syslog(LOG_INFO, log_string()."Error loading page editar-aviso: invalid value 0 for list_id ");
			return 'error';
		}

		if (!empty($p)) {
			$this->password = $p;
			return 'validate';
		}

		if (@$id['action'] == 'fromcmd' || $this->action == "refused") {
			return 'validate';
		}

		return 'validacion';
	}

	function editad_validacion() {
		global $account_session;
		$this->validated = false;
		$id = (int)$this->list_id;
		if (empty($id)) {
			// error handling
			syslog(LOG_INFO, log_string()."Error loading page password form for editar-aviso: invalid value 0 for list_id ");
			return "error";
		}

		syslog(LOG_DEBUG, log_string()." loading with id: {$this->list_id} actionid: {$this->action_id} action: {$this->action}");
		$tmpl_data = $this->tmpl_data;
		if (in_array($tmpl_data['ad_status'], array('deleted', 'refused'))){
			//validate that ad was deleted
			return 'error';
		}

		$tmpl_data['mobile_appl'] = "adpassword";
		if(!$account_session->is_logged() || ( $account_session->is_logged() && !$this->logged_account_owns_ad() )){
			// if bconf enabled
			$forced_login = bconf_get($BCONF, "*.forced_login.modal.editad.{$tmpl_data['editad_category']}.disabled");

			if ($forced_login === '0' && !$account_session->is_logged()) {
				header("Location: ". bconf_get($BCONF, "*.common.base_url.mobile") . "/login?list_id={$id}&cat={$tmpl_data['editad_category']}");
			} else {
				$this->display_mobile_layout('mobile/smartphone/editad_pwd_verify.html', $tmpl_data);
			}
		}
		else{
			return 'validate';
		}
	}

	function editad_validate($id,$passwd=''){
		global $account_session;
		if ((int)$id == 0) {
			// error handling
			syslog(LOG_INFO, log_string()."Error loading page editar-aviso: invalid value 0 for list_id ");
			return "error";
		}

		if (empty($passwd) && !isset($this->list_id) && !isset($this->action) && !$account_session->is_logged() ) {
			// error handling
			syslog(LOG_INFO, log_string()."Error loading page editar-aviso: invalid value 0 for list_id or passwd is empty");
			return "validacion";
		}

		if(!$account_session->is_logged()){
			if($account_session->login($this->ad->email, $passwd)){
				syslog(LOG_INFO, log_string()."XXX: User " . $this->ad->email . " logged in ok");
			}
		}
		else if(!$this->logged_account_owns_ad()){
			$account_new_session = new AccountSession();
			if($account_new_session->login($this->ad->email, $passwd)){
				syslog(LOG_INFO, log_string()."XXX: User " . $this->ad->email . " changing session");
				$account_new_session->expire();
				$account_session->expire();
				$account_session->login($this->ad->email, $passwd);
			} else {
				$account_session->expire();
			}
		}

		$valid_ad_password = false;
		/* if this->password does not exist, then the validation use the password from 'password form' */
		if (empty($this->password)) {
			$this->password = hash_password($passwd, $this->salt);
			$valid_ad_password = ($this->password == $this->salted_passwd);
		} else {
			$valid_ad_password = check_password_hash($this->password, $this->salted_passwd);
		}

		$valid_ad_access = false;
		if (preg_match('/^[0-9]{7,}\.[0-9]+$/', $this->list_id)
			|| $this->action == 'refused'
			|| $valid_ad_password) {
				$valid_ad_access = true;
		}

		$valid_user_access_to_ad = false;
		if (!$account_session->is_logged() && $valid_ad_access)
			$valid_user_access_to_ad = true;
		else if ($account_session->is_logged() && ($this->logged_account_owns_ad() || $valid_ad_access))
			$valid_user_access_to_ad = true;
		else
			$valid_user_access_to_ad = false;

		if (!$valid_user_access_to_ad) {
			$this->password = '';	// avoid using 'p' from url, since its wrong, and asks for password
			return 'validacion';
		}

		$this->validated = true;

		if (is_array($id)) {
			$this->list_id = $id['id'];
			$this->action_id = $id['action_id'];
			$this->action = $id['action'];
		}

		return 'formulario';
	}

	function editad_formulario(){
		global $account_session;
		if(!$this->validated) return 'validacion';

		syslog(LOG_DEBUG, log_string()." loading form with id: {$this->list_id} actionid: {$this->action_id} action: {$this->action}");
		$tmpl_data = $this->tmpl_data;

		//XITI stuff
		$tmpl_data['xtor'] = @$_GET['xtor'];
		$tmpl_data['ca'] = @$_GET['ca'];
		$tmpl_data['mv'] = 1;
		$tmpl_data['hide_footer'] = 1;
		if (!empty($this->ad->list_id)){
			$tmpl_data['clean_list_id'] = $this->ad->list_id;
		}

		if($account_session->is_logged()){
			$is_pro_for = $account_session->get_param('is_pro_for');
			if( !empty($is_pro_for) ){
				$tmpl_data['is_pro_for'] = $is_pro_for;
			}
		}

		if (!$this->publish_similar) {
			$edit_block = bconf_get($BCONF,"*.edit_allowed_days.cat.".$tmpl_data['editad_category']);
			if ($edit_block != null && array_key_exists('can_edit_ad', $tmpl_data) && $tmpl_data['can_edit_ad'] === 'f') {
				return 'blocked';
			}
		}

		$this->display_mobile_layout('mobile/smartphone/editad_form.html', $tmpl_data);
	}

	function editad_blocked() {
		if(!$this->validated) return 'validacion';

		bLogger::logInfo(__METHOD__, "SIMILAR: entering editad_blocked (list_id:$this->list_id, action_id:$this->action_id)");
		$tmpl_data = $this->tmpl_data;

		if (!array_key_exists('category', $tmpl_data) && array_key_exists('editad_category', $tmpl_data)) {
			$tmpl_data['category'] = $tmpl_data['editad_category'];
		}

		$this->headstyles = bconf_get($BCONF,"*.common.editad.blocked.headstyles");
		$this->display_mobile_layout('ai/edition_expired.html', $tmpl_data);
	}

	function editad_similar() {
		bLogger::logInfo(__METHOD__, "SIMILAR: entering editad_similar (list_id:$this->list_id, action_id:$this->action_id)");
		$this->publish_similar = true;
		$this->tmpl_data['subject'] = '';
		$this->tmpl_data['newad_name'] = $this->tmpl_data['name'];
		$this->tmpl_data['newad_email'] = $this->tmpl_data['email'];
		$this->tmpl_data['images'] = array();
		$this->ad_type = 'new';
		$this->tmpl_data['ad_type'] = 'new';
		return 'formulario';
	}

	/* Error state */
	function editad_error() {
		$this->headstyles = bconf_get($BCONF,"*.common.newad.error_headstyles");
		$error_tmpl_data = array('content' => 'mobile/list/view_missing.html');
		if ($this->is_mobile_app) {
			$error_tmpl_data['display_for_app'] = "true";
			$error_tmpl_data['hide_footer'] = 1;
		}
		$this->display_mobile_layout('mobile/list/view_missing.html', $error_tmpl_data);
	}

	function get_unique_errors($trans) {
		// error inserting ad
		$errors = $trans->get_errors();
		$messages = $trans->get_messages();

		$unique_errors = array();
		foreach ($errors as $key => &$value) {
			if ($key == "job_category")
				$key = "jc";
			else if ($key == "service_type")
				$key = "sct";
			else if ($key == "footwear_type")
				$key = "fwtype";
			if(is_array($value)){
				foreach ($value as &$subvalue){
					$subvalue = lang($subvalue);
					if(!in_array($subvalue,$unique_errors)){
						$unique_errors[$key] = $subvalue;
					}
				}
			}else{
				$value = lang($value);

				if (!empty($messages[$key]))
					$value .= ' "' . $messages[$key] . '"';
				if(!in_array($value,$unique_errors)){
					/* Blocked user error */
					if ($key == 'error')
						$unique_errors['phone'] = $value;
					else
						$unique_errors[$key] = $value;
				}
			}
		}
		return $unique_errors;
	}

	function clear($newad_reply, &$trans) {
		$trans->reset();
		$trans->add_client_info();
		$trans->add_data('verify_code', $newad_reply['paycode']);
		return $trans->send_command('clear', true);
	}

	function newad(&$trans) {
		global $BCONF;
		$trans_data = $this->ad->get_data_array();
		unset($trans_data['category_group']);
		unset($trans_data['can_edit_ad']);
		$trans->reset();
		$trans->populate($trans_data);

		/* If editing a refused action, send previous action_id. */
		if ($this->action == 'refused') {
			$trans->add_data('prev_action_id', $this->action_id);
			/* This is the action type before any editrefused. */
			if ($this->pre_refuse_action_type)
				$trans->add_data('pre_refuse_action_type', $this->pre_refuse_action_type);
		}

		$trans->add_data('source', ht_to_source($this->ht));
		$trans->add_data('ad_type', $this->ad_type);
		$trans->add_data('pay_type', 'free');
		$trans->add_data('do_not_send_mail', '1');
		return  $trans->send_command('newad', true, true);
	}

	function phone_standarization($code, $phone, $type) {
		if($type == 'm') {
			return '9'.$phone;
		} else {
			return $code.$phone;
		}
	}

	function editad_submit() {
		global $account_session;
		$trans = new bTransaction();
		/*The $_POST array has the encoding as UTF-8 should be ISO-8859-1*/
		$data = array();
		$area_code;

		if(!($this->ad instanceof bAd)){
			syslog(LOG_WARNING, log_string()." Edit Ad: session error, attribute ad is not instance of the bAd");
			echo(json_encode(array(
				'error'=>'session_error',
			)));
			return 'FINISH';
		}

		foreach($_POST as $key => $value){
			if (is_array($value)) {
				$data[$key] = $value;
			} else {
				if ($key == 'area_code') {
					$area_code = $value;
					continue;
				}
				$data[$key] = iconv("UTF-8", "ISO-8859-1", $value);
			}
		}
		$data['phone'] = $this->phone_standarization($area_code, $data['phone'], $data['phone_type']);
		$is_pro_for = @$account_session->get_param("is_pro_for");
		if($account_session->is_logged() && !empty($is_pro_for)){
			$io = array('category_group' => $_POST['category'] );
			get_settings(bconf_get($BCONF,"*.category_settings"), 'is_pro_for',
					create_function('$s,$k,$d', 'return isset($d[$k]) ? $d[$k] : NULL;'),
					create_function('$s,$k,$v,$d', '$d["value"] = $k;'),
					$io);

			$val = intval(@$io['value']);
			if($val == 0){	
				$data["company_ad"] = "1";
			}
		}
		$this->ad->populate($data);
		$this->ad->populate_jobs_and_services($data);
		if (!empty($_POST['image'])) {
			$this->ad->image = $_POST['image'];
		}

		$reply = $this->newad($trans);
		if ($trans->has_error()) {
			syslog(LOG_DEBUG, log_string()."Ad {$this->list_id} could not be edited");
			$unique_errors = $this->get_unique_errors($trans);
			$json_output = yapo_json_encode(array('ok'=>false,	'errors'=>$unique_errors));
			echo($json_output);
			exit;
		}

		$data_exists = isset($data['phone'], $data['email']);
		$data_to_update = ($data_exists) ? array('phone' => $data['phone'], 'region' => $data['region'], 'commune' => $data['communes']) : array();

		if($data_exists && !$this->account_util->edit_account_data_if_empty($data['email'], $data_to_update, $account_session)){
			syslog(LOG_ERR, log_string()."ERROR: Account data could not be updated for this account");
		}

		$reply_clear = $this->clear($reply, $trans);
		$pack_result = $reply_clear['pack_result'];
		if ($pack_result == 5) {
			$pack_result = -1;
		}

		if(!isset($this->action))
			$this->action = 'edit';
		if (in_array($this->action, array('refused','editrefuse','edit','renew'))){
			if(!empty($data['want_bump']) && $data['want_bump'] == 1){
				$this->want_bump();
			}
		}

		$result = array( 
			'ok'=>true,
			'action'=>$this->action,
			'pack_result'=>$pack_result,
			'old_category'=>$this->old_category,
			'has_weekly_bump' => $this->tmpl_data['has_weekly_bump'],
			'has_daily_bump' => $this->tmpl_data['has_daily_bump'],
			'has_gallery' => $this->tmpl_data['has_gallery'],
			'has_label' => $this->tmpl_data['has_label'],
			'da_id'=>$this->ad->ad_id
		);

		if(isset($data['upselling_product']) && bconf_get($BCONF, "*.payment.product.".$data['upselling_product'].".group") == "COMBO_UPSELLING") {
			$upselling_product = $data['upselling_product'];
			$upselling_product_label = isset($data['upselling_product_label']) ? $data['upselling_product_label']: "";
			$result['upselling_product'] = $upselling_product;
			$result['upselling_product_label'] = $upselling_product_label; 
			$result['payment_url'] = $this->get_pay_upselling_url($this->ad->ad_id, $upselling_product, $upselling_product_label);
			bLogger::logDebug(__METHOD__, "Mobile Ad Edit OK -> upselling_product { $upselling_product} and label {$upselling_product_label}");
		}

		echo(json_encode($result));

		return 'FINISH';
	}

	function get_pay_upselling_url($ad_id, $upselling_product, $upselling_product_label) {
		$paymentUrl = bconf_get($BCONF, "*.common.base_url.payment") . '/pagos?id=' . $ad_id . '&prod=' . $upselling_product . '&ftype=2&from=edit';
		if (isset($upselling_product_label) && $upselling_product_label != "") {
			$paymentUrl = $paymentUrl . '&lt=' . $upselling_product_label;
		}
		return $paymentUrl;
	}

	function want_bump() {
		$list_id = 0;
		if(!empty($this->ad->list_id)){
			$list_id = $this->ad->list_id;
		}else{
			return;
		}

		$prefix =  bconf_get($BCONF, "*.redis_session.redis.prepend")."x";
		$redis_key_subject	= "rksubject";
		$redis_key_price	= "rkprice";
		$redis_key_image0	= "rkimage0";
		$redis_key_len_image	= "rklenimage";
		$expire = bconf_get($BCONF,"*.payment.redis_expiration.standard");
		$roptions = array(
			'servers' =>  bconf_get($BCONF, "*.redis.session.host"),
			'debug' => false,
			'id_separator' => bconf_get($BCONF, "*.redis.session.id_separator"),
			'timeout' => bconf_get($BCONF, "*.redis.session.connect_timeout")
		);
		$redis = new RedisSessionClient($roptions);
		$redis->set($prefix.$list_id."_".$redis_key_subject   , $this->ad->subject);
		$redis->expire($prefix.$list_id."_".$redis_key_subject,$expire);
		$redis->set($prefix.$list_id."_".$redis_key_price     , $this->ad->price);
		$redis->expire($prefix.$list_id."_".$redis_key_price,$expire);
		$redis->set($prefix.$list_id."_".$redis_key_image0    , @$this->images[0]);
		$redis->expire($prefix.$list_id."_".$redis_key_image0,$expire);
		$redis->set($prefix.$list_id."_".$redis_key_len_image , count($this->images));
		$redis->expire($prefix.$list_id."_".$redis_key_len_image,$expire);
		$redis->disconnect();
		return;
	}

	function loadad($transaction, $id, $action_id=NULL, $action=NULL) {
		$transaction->reset();
		$transaction->add_data('id', $id);
		if (!empty($action_id) && !empty($action)) {
			$transaction->add_data('action_id', $action_id);
			$transaction->add_data('action', $action);
		}
		$transaction->add_data('commit', 1);
		$reply = $transaction->send_command('loadad');
		return $reply;
	}

	function get_phone($phone) {
		if (substr($phone, 0, 1) == '2' || substr($phone, 0, 1) == '9') {
			return substr($phone, 1, 8);
		} else {
			return substr($phone, 2, 8);
		}
	}

	function get_code($phone) {
		if (substr($phone, 0, 1) == '2' || substr($phone, 0, 1) == '9') {
			return substr($phone, 0, 1);
		} else {
			return substr($phone, 0, 2);
		}
	}
	function get_phone_type($phone) {
		if (substr($phone, 0, 1) == '9') {
			return "m";
		} else {
			return "f";
		}
	}

	function get_tmpl_data($id, $action_id=NULL, $action=NULL) {
		$transaction = new bTransaction();
		$reply = $this->loadad($transaction, $id, $action_id, $action);
		$ad = $reply['ad'];
		$user = $reply['users'];
		$this->salted_passwd = $reply['ad']['salted_passwd'];
		$this->salt = $reply['ad']['salt'];

		if (!$transaction->has_error()) {
			$this->ad->populate($reply, true);
			if (!empty($ad['list_id']))
				$this->ad->list_id = $ad['list_id'];
			if (!empty($ad['ad_id']))
				$this->ad->ad_id = $ad['ad_id'];
			$this->ad->uid = $user['uid'];
		}

		if ($action == 'refused') {
			syslog(LOG_INFO, log_string().' Populating with refusal info');
			$this->populate_refuse($reply);
		}

		$data = array (
			'status'		  => $reply['status'],
			'ad_status'		  => $ad['status'],
			'editad_region'   => $this->ad->region,
			'editad_category' => $this->ad->category,
			'subject'         => $this->ad->subject,
			'body'            => $this->ad->body,
			'price'           => $this->ad->price,
			'company_ad'      => $this->ad->company_ad,
			'can_edit_ad'     => $this->ad->can_edit_ad,
			'name'            => $this->ad->name,
			'email'           => $this->ad->email,
			'phone'           => $this->get_phone($this->ad->phone),
			'area_code'       => $this->get_code($this->ad->phone),
			'phone_type'      => $this->get_phone_type($this->ad->phone),
			'has_weekly_bump' => has_product($reply, "weekly_bump"),
			'has_daily_bump'  => has_product($reply, "daily_bump"),
			'has_gallery'	  => has_product($reply, "gallery"),
			'has_label'       => has_product($reply, "label"),
			'phone_hidden'    => $this->ad->phone_hidden
		);

		$this->old_category = $this->ad->category;

		if (empty($this->images)) {
			$images = array();
			if(isset($reply['images']) && is_array($reply['images'])) {
				foreach($reply['images'] as $image) {
					array_push($images, $image['name']);
				}
			}

			$this->images = $images;
		} else {
			syslog(LOG_DEBUG, log_string()." edit images ".print_r($this->images, true));
			$images = $this->images;
		}
		$data['images'] = $images;

		$params = array();
		$param_keys = $this->get_category_param_names($reply);
		if (!empty($param_keys)) {
			foreach ($param_keys as $index => $name) {
				if (isset($this->ad->$name)) {
					$params[$name] = $this->ad->$name;
				}
			}
		}

		syslog(LOG_DEBUG, log_string().' edit params '.print_r($params, true));

		if (isset($params) && is_array($params)) {
			foreach($params as $name => $value) {
				if($name != 'category') // So the category can be changed in the edit mobile.
					$data['param_'.$name] = $value;
			}
		}

		if (!empty($this->refused_reason)) {
			$data['refused_reason'] = $this->refused_reason;
		}

		return $data;
	}

	function f_string($val) {
		return strip_tags($val);
	}

	function f_is_mobile_app($var){
		return $var == "true";
	}

	/* Filter for the id. Return an array to be sent to loadad. */
	function f_newad_id($val) {

		if (preg_match('/^RH([a-zA-Z0-9=\+\/]+)$/', $val, $match)) {
			global $BCONF;
			$key = bconf_get($BCONF, '*.common.aes_cbc_keys.ad_refuse_email');
			$data = aes_cbc_decode($key, $match[1]);

			$parts = explode('_', $data);
			$id = $parts[0];
			$action_id = $parts[1];

			return array('action' => 'refused', 'id' => $id, 'action_id' => $action_id);
		}

		if (preg_match('/^[0-9]{7,}\.[0-9]+$/', $val))
			return array('action' => 'fromcmd', 'id' => $val);

		if (preg_match('/^[0-9]+$/', $val))
			return array('id' => $val);

		if (!empty($val))
			return false;

		return NULL;
	}

	function get_category_param_names($reply) {
		global $BCONF;
		$type = bconf_get($BCONF, "*.common.type.{$reply['ad']['type']}.short_name");
		if (isset($reply['ad_change_params']['type'])){
			$type = bconf_get($BCONF, "*.common.type.{$reply['ad_change_params']['type']}.short_name");
		}
		$params_postedition = array('category' => $reply['ad']['category'],
			'parent' => bconf_get($BCONF, "*.cat.{$reply['ad']['category']}.parent"),
			'type' => $type,
			'company_ad' => isset($reply['ad_change_params']['company_ad'])?$reply['ad_change_params']['company_ad']:$reply['ad']['company_ad']);
		if (isset($reply['params']['estate_type'])){
			if (isset($reply['ad_change_params']['estate_type']))
				$params_postedition['estate_type'] = $reply['ad_change_params']['estate_type'];
			else
				$params_postedition['estate_type'] = $reply['params']['estate_type'];
		}
		get_settings(bconf_get($BCONF,"*.category_settings"), "params",
			create_function('$s,$k,$d', 'return isset($d[$k]) ? $d[$k] : NULL;'),
			create_function('$s,$k,$v,$d', '$d[$k] = $v;'),
			$params_postedition);
		return array_keys($params_postedition);
	}

	function populate_refuse($reply) {
		if (isset($reply['actions']) && isset($reply['actions']['has_parent']) && $reply['actions']['has_parent'] == 't') {
			syslog(LOG_INFO, log_string().' Refusal already edited');
			$this->refusal_edited = 1;
			return 'error';
		}

		if (isset($reply['ad_change_params'])) {
			/* user should see the refused changes. apply ad_change_params to ad */

			/* CATEGORY CHANGED. New category adparams must be created in $this->ad object
			and old category adparams must be unset.*/
			if (isset($reply['ad_change_params']['category'])) {
				// get old category params 
				get_settings(bconf_get($BCONF,"*.category_settings"), "params", array($this, "key_lookup"), array($this, "set_values"), $params_preedition);
				// get new category params 
				$params_postedition = array('category' => $reply['ad_change_params']['category'],
					'parent' => bconf_get($BCONF, "*.cat.{$reply['ad_change_params']['category']}.parent"),
					'type' => isset($reply['ad_change_params']['type'])?$reply['ad_change_params']['type']:$reply['ad']['type'],
					'company_ad' => isset($reply['ad_change_params']['company_ad'])?$reply['ad_change_params']['company_ad']:$reply['ad']['company_ad']);
				get_settings(bconf_get($BCONF,"*.category_settings"), "params", 
					create_function('$s,$k,$d', 'return isset($d[$k]) ? $d[$k] : NULL;'),
					create_function('$s,$k,$v,$d', '$d[$k] = $v;'),
					$params_postedition);

				$this->ad->category_group = $reply['ad_change_params']['category'];
				// Create new adparams in bAd object
				$this->ad->edit_params = $params_postedition;
				foreach (array_keys($params_postedition) as $key => $value) {
					if (!array_key_exists($value, get_object_vars($this->ad)) ) {
						$this->ad->$value = '';
					}
				}

				// Unset from bAd object not needed old category adparams 
				foreach (explode(',', $params_preedition) as $key => $value) {
					if (array_key_exists($value, get_object_vars($this->ad)) 
						&& !in_array($value, array_keys($params_postedition))) {
							unset($this->ad->$value);
						}
				}
			} 

			foreach ($reply['ad_change_params'] as $key => $value) {
				if (!strncmp($key, "param_", 6)) {
					$key = substr($key, 6);
				}

				if (array_key_exists($key, get_object_vars($this->ad))) {
					$this->ad->$key = $value;
				} elseif (preg_match('/\d+$/', $key)) {
					// Apply changes to numbered (array-based) ad_params 
					$match_data = array();
					preg_match('/([^\d]*)(\d+)$/', $key, $match_data);
					if (count($match_data) == 3) {
						$arr_idx = intval($match_data[2])-1; 
						$arr_key_ref = &$this->ad->$match_data[1];
						$arr_key_ref[$arr_idx] = $value;
					} else {
						syslog(LOG_INFO, log_string()."Unexpected number of elements after split of numbered adparam '".$key."'");
					}
				} 
			}

			$this->ad->cleanup();
		}

		if (isset($reply['ad_image_changes'])) {
			ksort($reply['ad_image_changes']);
			$this->images = array();
			$this->thumbnail_digest = array();
			$this->digest_present = array();
			$digest_found = 0;
			foreach ($reply['ad_image_changes'] as $image_id => $image) {
				if (empty($image['name']))
					continue;
				$this->images[] = $image['name'];
				$this->digest_present[] = ($image['is_new'])? 0: 1;
				if (isset($reply['digest'])) {
					foreach ( $reply['digest'] as $ind => $values) {
						if(@$values['db_name'] == $image['name'])
							if (isset($values['digest'])) {
								$this->thumbnail_digest[] = $values['digest'];
								$digest_found = 1;
							}
					}
				}
				if (!$digest_found)
					$this->thumbnail_digest[] = "";
				else
					$digest_found = 0;
			}
		}

		$this->action_id = $reply['actions']['action_id'];
		$this->refused_reason = @$reply['refused']['reason'];
		$this->refused_refusal_text = @$reply['refused']['refusal_text'];
		if (isset($reply['pre_refuse_action_type']['action_type'])) {
			$this->pre_refuse_action_type = $reply['pre_refuse_action_type']['action_type'];

			//if ($this->pre_refuse_action_type == 'extra_images' && $reply['ad']['status'] == 'active') {
			//	/* Redirect to addimages app. */
			//	$this->list_id = $reply['ad']['list_id'];
			//	return 'add_images';
			//}

			//if ($this->pre_refuse_action_type == 'video' && $reply['ad']['status'] == 'active') {
			//	/* Redirect to video app. */
			//	$this->list_id = $reply['ad']['list_id'];
			//	return 'add_video_form';
			//}
		}

		$this->action = 'refused';
		if (!empty($reply['action_params']['cleared_by']) || (!empty($reply['payment']) && $reply['payment']['pay_type'] == 'campaign' && !empty($reply['action_params']['campaign_id']))) {
			$this->campaign = $reply['action_params']['campaign_id'];
			$this->campaign_ad_id = $reply['action_params']['cleared_by'];
			$this->pay_type = 'campaign';
		}
	}

	/**
	 * Returns true if the current logged account is the
	 * owner of the current ad
	 */
	function logged_account_owns_ad() {
		global $account_session;
		return ($account_session->is_logged() && $this->ad->email === $account_session->get_param('email'));
	}

	function set_values($setting, $key, $value, $data = null) {
		if ($data)
			$data .= ',' . $key;
		else
			$data = $key;

		if ($value)
			$data .= ":" . $value;
	}

	function key_lookup($setting, $key, $data = null) {
		global $BCONF;

		if ($key == 'type' && !is_object($this->ad))
			return 's';

		if (is_object($this->ad)) {
			if (!empty($this->ad->sub_category))
				$cat = $this->ad->sub_category;
			else if (!empty($this->ad->category_group))
				$cat = $this->ad->category_group;
			else
				$cat = $this->ad->category;

			if ($key == 'category')
				return $cat;
			else if ($key == 'parent')
				return bconf_get($BCONF, "*.cat.{$cat}.parent");
		} elseif ($key == 'category')
			return '0';

		if ($key == 'action' && $this->action == 'refused' && $this->pre_refuse_action_type == 'edit') {
			return 'edit';
		} else if (is_object($this->ad) && isset($this->ad->$key)) {
			if (is_array($this->ad->$key)) {
				return NULL;
			}
			return $this->ad->$key;
		} else {
			return @$this->$key;
		}
	}

}
	
/**
 * Check if reply has the product indicated
 */
function has_product($reply, $product){

	if (isset($reply['params'][$product]) || isset($reply['params']['upselling_'.$product])){
		return 1;
	}

	if(isset($reply['upselling_pending']) && $product != 'gallery' && $product != 'label'){
		$command = $reply['upselling_pending']['command'];
		if(!is_array($command))
			$command = array($command);

		foreach ($command as $cmd) {
			if($product == 'weekly_bump' && strpos($cmd,'daily_bump:1') === false){
				return 1;
			}
			if($product == 'daily_bump' && strpos($cmd,'daily_bump:1') !== false){
				return 1;
			}
		}

	}
	return 0;
}

$editadapp = new blocket_editadapp();
$editadapp->run_fsm();
