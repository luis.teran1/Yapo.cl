<?php
require_once('JSON.php');
require_once('init.php');
require_once('ApiGatewayClass.php');
if ($_SERVER['REQUEST_METHOD'] == "POST") {
	$request = $_POST;
} else {
	$request = $_GET;
}
$api_gateway = new ApiGateway();
$action = $request['action'];
$params = $request;
unset($params['action']);
$response = $api_gateway->call_action($action, $params);
if (!empty($response['es'])) {
	$response = $response['es'][0];
}
print yapo_json_encode($response);
