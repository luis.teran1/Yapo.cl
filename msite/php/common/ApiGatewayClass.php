<?php
require_once('JSON.php');
require_once('init.php');
require_once('bLogger.php');

class ApiGateway {

	private $passwd;
	private $list_id;
	private $action;
	private $action_id;
	private $hash;

	function __construct() {
		$this->passwd  = '';
		$this->list_id = '';
		$this->action  = '';
		$this->action_id  = '';
		$this->hash = '';
	}

	public function call_action($action, $params=array())
	{
		if (!empty($this->hash)) {
			$params = array_merge($params, array('hash' => $this->hash));
		}
		$response = $this->_connect_to_api($action, $params);
		syslog(LOG_DEBUG, print_r($response, true));
		if (!empty($response['authorize'])) {
			$this->hash = $this->_get_hash($response['authorize']->challenge);
			$params = array_merge($params, array('hash' => $this->hash));
			$response = $this->_connect_to_api($action, $params);
			// Still no auth
			if (!empty($response['authorize'])) {
				bLogger::logError(__METHOD__, "Tried to get new API hash but new one was still invalid: {$this->hash}");
				$response = array('status' => "ERROR");
			}
		}
		return $response;
	}

	/*
	public function process_request($request) {
		$response = $this->_validate_request($request);

		if($response['status'] =='ERROR'){
			return $response;
		}

		$this->list_id = $request['list_id'];
		$this->action  = $request['action'];
		If(isset($request['passwd'])) $this->passwd  = $request['passwd'];
		if(isset($request['action_id'])) $this->action_id  = $request['action_id'];

		$response = $this->_get_query();
		if($response['status'] == 'ERROR'){
			return $response;
		}
		$response = $this->_connect_to_api($this->action, $response['data']);
		$api_response = (array)$response['es'][0];
		$status = $api_response['status'];
		if($status != "OK"){
			$error = array('status' => 'ERROR', 'data' => $api_response);
			return $error;
		}
		$final_response = array('status' => 'SUCCESS', 'data' => $api_response);
		return $final_response;
	}
	 */

	/*
	private function _validate_request($request) {

		$msg_error_pwd = bconf_get($BCONF,"*.language.INSERT_MUST_PASSWORD.es");
		if(empty($request['list_id'])){
			$error = array('status' => 'ERROR',"data"=>array("status"=>"ID_MISSING","message"=>"list_id is not defined"));
			return $error;
		}
		if(empty($request['action'])){
			$error = array('status' => 'ERROR',"data"=>array("status"=>"ACTION_MISSING","message"=>"action is not defined"));
			return $error;
		}
		if(empty($request['passwd']) && empty($request['action_id']) && !preg_match('/^[0-9]{7,}\.[0-9]+$/', $request['list_id'])){
			$error = array('status' => 'ERROR',"data"=>array("status"=>"PASSWORD_MISSING","message"=>$msg_error_pwd));
			return $error;
		}
		return array('status' => 'SUCCESS');
	}
	 */

	/*
	private function _get_query() {
		// Get challenge
		$response = $this->_connect_to_api($this->action);
		$array_authorize = (array)$response['authorize'];
		$status = $array_authorize['status'];
		if($status != "NOT A VALID API-KEY"){
			$error = array('status' => 'ERROR', 'data' => $response);
			return $error;
		}
		// Get hash
		$hash = $this->_get_hash($array_authorize['challenge']);

		// Put all params together
		$params = array('hash'=>$hash,'ad_id'=>$this->list_id);
		if(!empty($this->passwd)) {
			$params['passwd'] = $this->passwd;
		}
		if(!empty($this->action_id)) {
			$params['action_id'] = $this->action_id;
		}
		$query  = $this->_format_params($params);
		return array('status' => 'SUCCESS', 'data' => $query);
	}
	 */

	/**
	 * Creates a HTTP GET style string with a trailing ampersand at the begginging
	 */
	public static function urlize_array($arr) {
		$result = "";
		$first = true;
		foreach ($arr as $k => $v) {
			$result .= '&';
			$result .= sprintf('%s=%s', $k, urlencode($v));
		}
		return $result;
	}

	private function _connect_to_api($args_app, $args_params=null){
		$response = '';
		$url = '/apitemplates/'.$args_app.'.json?app_id=blocket_mobile';
		if($args_params){
			if (is_array($args_params)) {
				$args_params = self::urlize_array($args_params);
			}
			$url .= $args_params;
		}
		$file = @fopen($this->_get_host().$url, "r");
		if($file){
			while(!feof($file)){
				$response .= @fgets($file, 4096);
			}
			fclose ($file);
		}
		return (array) yapo_json_decode(utf8_encode($response));
	}

	/*
	private function _format_params($param){
		$array = array();
		foreach($param as $k=>$v){
			array_push($array,"$k=$v");
		}
		$var_return = implode("&",$array);
		return $var_return;
	}
	 */

	private function _get_hash($challenge){
		$api_key = "746e1a72b4172e50d9c6b4f981312a1803283a74";
		return sha1($challenge.$api_key);
	}

	private function _get_host(){
		return bconf_get($BCONF, "*.common.internal_url.api_gateway");
	}
}
?>
