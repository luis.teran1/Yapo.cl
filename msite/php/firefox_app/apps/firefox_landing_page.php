<?php

require_once('bResponse.php');

class FirefoxLandingPage
{
	private $cookie_name;
	private $cookie_domain;
	private $main_url;

	function run()
	{
		$this->setAppCookie();
		$this->goToMainPage();
	}

	function setAppCookie()
	{
		setcookie($this->cookie_name, "1", 0, "/", $this->cookie_domain);
	}

	function goToMainPage()
	{
		$response = new bResponse();
		$response->redirect($this->main_url);
	}

	function __construct()
	{
		$this->cookie_name =  bconf_get($BCONF, "*.firefox_app.is_firefox_app");
		$this->cookie_domain =  bconf_get($BCONF, '*.common.session.cookiedomain');
		$this->main_url =  bconf_get($BCONF, "*.common.base_url.mobile");
	}
}

$lander = new FirefoxLandingPage();
$lander->run();

?>
