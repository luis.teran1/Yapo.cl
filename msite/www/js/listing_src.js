const countFilterSearch  = new countFilter({countUrl: '/count-filter'});

function searchbox_check_changes() {
  var selected_searchbox_filtered = 0;
  var values=['Filtros','Filtros*'];
  var elements = selected_searchbox.find('select');

  for (var i = 0; i < elements.length; ++i) {
    if (elements[i].value!='') {
      selected_searchbox_filtered = 1;
    }
  }

  if ($('#type_person').is(':checked') != $('#type_company').is(':checked')) {
    selected_searchbox_filtered=1;
  }
  $('#filters').val(values[selected_searchbox_filtered]);
}

var selected_searchbox = null;

function searchbox_change() {
  var t = $('input[name=st]:checked');
  var type = (t.length==0) ? 's' : t.val();

  if (t.length == 0) {
    t = $('input[name=default_st]');
    if (t.length != 0) {
      type=t.val();
    }
  }

  if (t.length != 0) {
    $('.stype').hide();
  }

  selected_searchbox=$('#searchbox_'+type);

  if (selected_searchbox.find('form').length == 0) {
    selected_searchbox=$('#searchbox_a');
  }

  searchbox_check_changes();
  selected_searchbox.show();
  selected_searchbox.find('select').change(searchbox_check_changes);

  var inputs_selected = selected_searchbox.find('select');

  for (var i = 0 ; i < inputs_selected.length ; i++ ) {
    var input_name = inputs_selected[i].name;
    var input_val =  inputs_selected[i].value;
    // Set Search Count Params
    countFilterSearch.addFilterParam(input_name, input_val);

    if (input_val == '') {
      if (undefined != filter_values[input_name]) {
        delete filter_values[input_name];
        document.cookie = 'filter_values=' + escape(JSON.stringify(filter_values)) + '; path=/';
        $('[name='+input_name+']').each(function(){
          $(this).val('');
        });
      }
    }
  }

  var category = $.trim($('#selectbox_categories').val());

  if (category == 2020) {
    var br = selected_searchbox.find('select[name=br]');

    if (br.attr('changed') == null) {
      br.change(function() {
        if ($(this).val() == '') {
          $('select[name=mo]').each(function() {
            $(this).html('<option value="">Modelo</option>');
            $(this).attr('disabled','disabled');
          });
        } else {
          getBrandModels(br.val());
        }
      });
      br.attr('changed', true);
    }
  }
}

function advertiser() {
  var is_person=$(this).attr('id')=='type_person';

  if(!($('#type_person').is(':checked') || $('#type_company').is(':checked'))) {
    $('#type_person').attr('checked', !is_person);
    $('#type_company').attr('checked', is_person);
  }
  searchbox_check_changes();
}

async function category_change(needSubmit =  true) {
  var tdef;
  var radio = $('input[name=st]:checked');
  var category = $.trim($('#selectbox_categories').val());

  // check if st is defined in the query params
  // if defined its value takes priority
  var regex = new RegExp('[\\?&]st=([^&#]*)');
  var results = regex.exec(location.search);
  if (results) {
    tdef = decodeURIComponent(results[1].replace(/\+/g, ' '));
  } else {
    tdef = searchDefaultTypes[category];
    if (!tdef) {
      tdef = searchDefaultTypes['default'];
    }
  }

  var f = 'a';
  var w = $.trim($('#w').val());
  var ret = $('select[name=ret]:visible').val();
  var region = $.trim($('#region').attr('data-region'));

  if (!$('#type_person').is(':checked') || !$('#type_company').is(':checked')) {
    f = ($('#type_person').is(':checked')) ? 'p' : 'c';
  }

  var objData = {
    cg: category,
    creg: region,
    ctype: tdef,
    w: w,
    f: f
  };

  var allRet;

  if (typeof (ret) === 'undefined') {
    allRet = $('select[name=ret]');

    for (var i = 0; i < allRet.length; i++) {
      if (allRet[i].value != '') {
        ret = allRet[i].value;
      }
    }
  }

  if (typeof (ret) !== 'undefined' && ret != '') {
    objData.ret = ret;
  }

  var cmn = $('#cmn_select').val();

  if (cmn) {
    objData.cmn = cmn;
  }
  
  // Clear obj filterSearchCount
  countFilterSearch.clearFiltersParams();
  // Load Default Data Search Count
  countFilterSearch.defaultParamsSearchCount(objData);

  await $.ajax({
    type: 'GET',
    url: '/templates/mobile/list/searchbox.html',
    data: (objData),
    dataType: 'text',
    success: function (data) {
      var e;
      $('#searchbox').hide().html(data).show();
      $('input[name=st]:radio').change(searchbox_change);
      $('select[name=ret]').change(subcategory_change);
      var model = filter_values.mo;
      var brand = filter_values.br;
      if (brand != null) if (brand['value'] == '') brand = null;

      $.each(filter_values, function (key,value) {
        var val = value['value'];
        var html = value['html'];
        var input_found = false;
        if (key !== 'ret') {
          $('[name=' + key + ']').each(function () {
            if ($(this).find('option[value=' + val + ']').html() == html) {
              $(this).val(val);
              input_found = true;
            }
          });
        }

        if (!input_found) {
          delete filter_values[key];
          document.cookie = 'filter_values=' + escape(JSON.stringify(filter_values)) + '; path=/';
        }
      });

      setMinMax($('#searchbox_a').find('form').get(0));

      if (brand != null) {
        if ($('select[name=mo]') != null) {
          getBrandModels(brand['value'], function () {
            if (model != null) {
              $('select[name=mo]').each(function () {
                $(this).val(model['value']);
                countFilterSearch.addFilterParam('mo', model['value']);
              });
            }
          });
        }
      } else {
        $('select[name=mo]').each(function () {
          $(this).attr('disabled', 'disabled');
        });
      }

      e = $('input[name=st][value="' + tdef + '"]:radio');

      if (e.length === 0) {
        searchbox_change();
      } else {
        e.click();
        e.change();
      }

      onFwgendChange(null);
      setFilterCarsDataFromUrl();
      setFilterInmoDataFromUrl();
    }
  }).then(function(){
    if ($("#searchbox_container").is(":hidden") && $("#selectbox_categories").data('val') != undefined && needSubmit) {
      formSearchSubmit();
    }
  });
}

function subcategory_change() {
  var tdef;
  var radio = $('input[name=st]:checked');
  var category = $.trim($('#selectbox_categories').val());

  // check if st is defined in the query params
  // if defined its value takes priority
  var regex = new RegExp('[\\?&]st=([^&#]*)');
  var results = regex.exec(location.search);
  if (results) {
    tdef = decodeURIComponent(results[1].replace(/\+/g, ' '));
  } else {
    tdef = CATEGORY_SETTINGS['default_type'][category];
    if (!tdef) {
      tdef = CATEGORY_SETTINGS['default_type']['default'];
    }
  }

  var f = 'a';
  var w = $.trim($('#w').val());
  var ret = $('select[name=ret]:visible').val();
  var region = $.trim($('#region').attr('data-region'));

  if (!$('#type_person').is(':checked') || !$('#type_company').is(':checked')) {
    f = ($('#type_person').is(':checked')) ? 'p' : 'c';
  }

  var objData = {
    cg: category,
    creg: region,
    ctype: tdef,
    w: w,
    f: f
  };

  var allRet;

  if (typeof (ret) === 'undefined') {
    allRet = $('select[name=ret]');

    for (var i = 0; i < allRet.length; i++) {
      if (allRet[i].value != '') {
        ret = allRet[i].value;
      }
    }
  }

  if (typeof (ret) !== 'undefined' && ret != '') {
    objData.ret = ret;
  }

  var cmn = $('#cmn_select').val();

  if (cmn) {
    objData.cmn = cmn;
  }

  // Clear obj filterSearchCount
  countFilterSearch.clearFiltersParams();
  // Load Default Data Search Count
  countFilterSearch.defaultParamsSearchCount(objData);

  $.ajax({
    type: 'GET',
    url: '/templates/mobile/list/searchbox.html',
    data: (objData),
    dataType: 'text',
    success: function (data) {
      var e;
      $('#searchbox').hide().html(data).show();
      $('input[name=st]:radio').change(searchbox_change);
      $('select[name=ret]').change(subcategory_change);
      var model = filter_values.mo;
      var brand = filter_values.br;
      if (brand != null) if (brand['value'] == '') brand = null;

      $.each(filter_values, function (key,value) {
        var val = value['value'];
        var html = value['html'];
        var input_found = false;
        if (key !== 'ret') {
          $('[name=' + key + ']').each(function () {
            if ($(this).find('option[value=' + val + ']').html() == html) {
              $(this).val(val);
              input_found = true;
            }
          });
        }

        if (!input_found) {
          delete filter_values[key];
          document.cookie = 'filter_values=' + escape(JSON.stringify(filter_values)) + '; path=/';
        }
      });

      setMinMax($('#searchbox_a').find('form').get(0));

      if (brand != null) {
        if ($('select[name=mo]') != null) {
          getBrandModels(brand['value'], function () {
            if (model != null) {
              $('select[name=mo]').each(function () {
                $(this).val(model['value']);
                countFilterSearch.addFilterParam('mo', model['value']);
              });
            }
          });
        }
      } else {
        $('select[name=mo]').each(function () {
          $(this).attr('disabled', 'disabled');
        });
      }

      e = $('input[name=st][value="' + tdef + '"]:radio');

      if (e.length === 0) {
        searchbox_change();
      } else {
        e.click();
        e.change();
      }

      onFwgendChange(null);
      setFilterCarsDataFromUrl();
    }
  });
}

function getBrandModels(brand, callback) {
  $.ajax({
    type: 'GET',
    url: '/get_brand_models?cg=2020&brand=' + brand + '',
    success: function (data) {
      $('select[name=mo]').each(function() {
        $(this).html(data);
        $(this).removeAttr('disabled');
      });

      if (callback) {
        callback();
      }
    }
  });
}

var filter_values = {};

$(document).ready(function () {
  $('.toggle_filters').click(function() {
    $('.searchbox').toggle();
    $('.listing').toggle();
    $('.pagination').toggle();
    onFwgendChange(null);
  });

  countFilterSearch.init();

  //Gallery carrusel
  $('.gallery-list').bxSlider({
    slideWidth: 132,
    minSlides: 2,
    maxSlides: 5,
    slideMargin: 2,
    pager: false,
    controls: false,
    onSliderLoad: function() {
      $('.gallery-preloader').fadeOut(1200);
    }
  });

  $('#search, #search_filter').click(function() {
    formSearchSubmit(true);
  });

  $('#keyword').keypress(function(event) {
    if (event.which == 13) {
      $('#search').click();
    }
  });

  function select_has_changed() {
    $(this).data('val', $(this).val());
  }

  $('#selectbox_categories').focusin(select_has_changed);
  $('#selectbox_categories').change(category_change);
  $('select[name=ret]').change(subcategory_change);

  $('#searchbox_container').on('change', '.searchbox__select', function(){
    var value = $(this).val();
    var html = (value == '') ? '' : $(this).find('option[value='+value+']').html();
    var name = $(this).attr('name');

    if (value != '') {
      $('[name='+name+']').each(function() {
        if ($(this).find('option[value=' + value + ']').html() == html) {
          $(this).val(value);
        }
      });

    }

    if (value == '') {
      delete filter_values[name];
    } else {
      filter_values[name] = {
        'value' : value,
        'html' : html
      };
    }

    document.cookie = 'filter_values=' + escape(JSON.stringify(filter_values)) + '; path=/';
    
    countFilterSearch.addFilterParam($(this).attr('name'), value)
    setMinMax($('#searchbox_a').find('form').get(0));
  });


  $('input[name=st]:radio').change(searchbox_change);
  searchbox_change();

  $('#type_person').on('click', advertiser);
  $('#type_company').on('click', advertiser);

  $('#searchbox').ajaxError(function(event, request, settings) {
    $('#searchbox').html('Error Calling: ' + settings.url + '<br />HTTP Code: ' + request.status);
  });

  if ($('#selectbox_categories').val() != 0) {
    var cookie_name = 'filter_values';
    var start = document.cookie.indexOf(cookie_name + '=');
    var end;

    if (start != -1) {
      start = start + cookie_name.length + 1;
      end = document.cookie.indexOf(';', start);

      if (end == -1) {
        end = document.cookie.length;
      }
      filter_values = JSON.parse(unescape(document.cookie.substring(start, end)));
    }
    $('#selectbox_categories').change();
  } else {
    filter_values = {};
    document.cookie = 'filter_values=' + escape(JSON.stringify(filter_values)) + '; path=/';
  }

  $('img.lazy').lazyload();
});

$(document).on('change', '[name=fwgend]', onFwgendChange);

function onFwgendChange(ev){
  $('[name=fwsize]').prop( "disabled", false );
  sizes = $('[name=fwsize] option');
  sizes_len = sizes.length;
  var i;
  if ($('[name=fwgend]').val()) {
    for (i=0; i<sizes_len; i++){
      if (ev) {
        sizes[i].selected=false;
      }
	  if (sizes[i].getAttribute('value') != "") {
        if ($('[name=fwgend] option:selected')[0].getAttribute('data-sizeseq').split(",").indexOf(sizes[i].getAttribute('value')) != -1) {
          sizes[i].setAttribute('style', 'display:block');
        } else {
          sizes[i].setAttribute('style', 'display:none');
        }
      }
    }
  } else {
    for (i=0; i<sizes_len; i++){
	  sizes[i].selected=false;
	}
    $('[name=fwsize]').prop( "disabled", true);
  }
}

/*
   SELECT ELEMENTS FROM THE FORM
   The select controls used for range search are named 'string_xs' and 'string_xe', where 'string_x' corresponds to the name of the range and 's' for start and 'e' for end values.
   We consider rootName as 'string_x' and leafName as 's' or 'e'. The only range that not apply this convention is the price, that uses 'ps_x' for start values and 'pe_x' for end values. In this case we consider 'p_x' as the rootName and 's' or 'e' as leafName; The sizelist have a different pattern too, it uses 'ss_x' for start values and 'se_x'for end values. In this case we consider 's_x' as the rootName and 's' or 'e' as leafName;
   We loop through all the elements in the form and we get the rootName and baseName, and we store it in an array in a form of [rootName][leafName].
   After check if both 's' and 'e' values exists
   and the 's'  does not have the minimum index selected
   and 'e' does not have the maximum index selected
   and the label of 's' is greater than the label of 'e',
   we apply the function changeSelectedValues.
 */
function setMinMax(form) {
  var ctrlArray = [];

  for (var i = 0; i < form.elements.length; i++) {
    if (form.elements[i].type === 'select-one' &&
        form.elements[i].parentNode.style.display !== 'none' &&
        form.elements[i].selectedIndex > 0) {
      var elementNameLength = form.elements[i].name.length;
      var rootName;
      var leafName;

      if (form.elements[i].id.match(/[p|s]\w_\d/)) {
        rootName = form.elements[i].id.replace(/(p|s)._/, '$1_');
        leafName = form.elements[i].id.substring(1, 2);
      } else {
        rootName = form.elements[i].name.substring(0, elementNameLength - 1);
        leafName = form.elements[i].name.substring(elementNameLength - 1);
      }

      if (!ctrlArray[rootName]) {
        ctrlArray[rootName] = [];
      }

      ctrlArray[rootName][leafName] = form.elements[i];
      var minElement = ctrlArray[rootName].s;
      var maxElement = ctrlArray[rootName].e;
      if (minElement && maxElement) {
        var minSelectedIndex = minElement.selectedIndex;
        var maxSelectedIndex = maxElement.selectedIndex;
        var maxElementLength = maxElement.length;
        var minValue = Number(minElement.value);
        var maxValue = Number(maxElement.value);
        if ((minSelectedIndex > 0) && (maxSelectedIndex < maxElementLength)
            && (minValue > maxValue)) {
          changeSelectValues(minElement, maxElement);
        }
      }
    }
  }
}

/*
 * To change the select values we do the following:
 * For the max value, we apply the same index as selected in the minimum.
 */
function changeSelectValues (selectmin, selectmax) {
  var maxValue = selectmax.value;
  var minValue = selectmin.value;
  selectmax.value = minValue;
  selectmin.value = maxValue;
  /*
   * call method change to save value into a cookie
   */
  $(selectmax).change();
  $(selectmin).change();
}

function setFilterCarsDataFromUrl() {
  var queryString = window.location.search;
  var params = new URLSearchParams(queryString);
  var brand = '';
  var model = '';

  for (var entry of params.entries()) {
    if (entry[0] == 'mo') {
      model = entry[1];
    } else if (entry[0] == 'br' || entry[0] == 'ctp' || entry[0] == 'fu') {
      $('select[name='+ entry[0] +']').each(function () {
        $(this).val(entry[1]);
      });
    }

    if (entry[0] == 'br') {
      brand = entry[1];
    }
  }

  if (brand != '') {
    getBrandModels(brand, function () {
      if (model != '') {
        $('select[name=mo]').each(function () {
          $(this).val(model);
          countFilterSearch.addFilterParam('mo', model);
        });
      }
    });
  }
  searchbox_check_changes();
}

function setFilterInmoDataFromUrl() {
  var queryString = window.location.search;
  var params = new URLSearchParams(queryString);
  var inmoParams = ['ps','pe', 'ros', 'roe', 'ss', 'se', 'brs', 'bre', 'cos', 'coe', 'gs'];

  for (var entry of params.entries()) {
    if (inmoParams.includes(entry[0])) {
      $('select[name='+ entry[0] +']').each(function () {
        $(this).val(entry[1]);
      });
    }
  }

  searchbox_check_changes();
}

/**
 * NOTIFICATIONS
 */
loadDataFinish = false;

window.addEventListener('message', function validateLogginInStatus (evt){
  if(evt.origin === window.location.origin && evt.data ==='legacy_hub::dataupdated') {
    loadDataFinish = true;
  }
})

if (window.fetch) {
  try {
    var idInterval = setInterval(function() {
      if(loadDataFinish) {
        initializeNotifications("MSITE", function(){
          clearInterval(idInterval);
        })
      }
    }, 200);
  } catch (e) {
    console.error(e);
  }
}

function formSearchSubmit(clickFromSearchButton) {
  if (clickFromSearchButton) {
    addCategoryForKeyword().then(function() {
      setDataFormAndSubmit();
    });
  } else {
    setDataFormAndSubmit();
  }
}

function setDataFormAndSubmit(resetKeyword = false) {
  const type_p = $('#type_person').is(':checked');
  const type_c = $('#type_company').is(':checked');
  const cmn = $('#cmn_select').val();
  const keyword = (resetKeyword) ? '' : $('#keyword').val();
  let q = selected_searchbox.find('input[name=q]');
  let f = selected_searchbox.find('form');
  let person = selected_searchbox.find('input[name=f]');
  
  q.val(keyword);

  if (cmn != '' && (typeof cmn) != 'undefined' ) {
    f.append('<input type="hidden" name="cmn" value="' + cmn + '" />');
  }
  if ((type_c && type_p) || (!type_c && !type_p)) {
    person.val('a');
  } else {
    if (type_p) {
      person.val('p');
    } else {
      person.val('c');
    }
  }
  
  setMinMax(f.get(0));
  f.submit();
}

//Category for keyword data casa only poc
async function addCategoryForKeyword() {
  const keyword = document.getElementById('keyword');
  const categoryKeyword = {
    'casa': {
      category: '1000',
      name: 'inmuebles'
    }
  };
  let selectCategory = document.getElementById('selectbox_categories');
  let keywordFind = false;
  let cat = categoryKeyword[keyword.value.toLowerCase()]
  
  if (typeof cat !== 'undefined' && selectCategory.value === "0") {
    selectCategory.value = cat.category;
    await category_change(false);
    keywordFind = true;
  }
}
