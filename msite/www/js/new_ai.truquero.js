var Truquero = (function() {
  /* Predicts fields/values pairs based on known fields */

  var predictedValues = {};

  function existsAndAreSet(keys, obj) {
    var r = true;
    for (var i = 0; i < keys.length; i++) {
      if (!(keys[i] in obj) || obj[keys[i]] === '') {
        r = false;
        break;
      }
    }
    return r;
  }

  function predict(obj) {
    // route to specific prediction function

    if (existsAndAreSet(['brand', 'model', 'version'], obj)) {
      predictCars(obj.brand, obj.model, obj.version);
    }
  }

  function predictCars(brandId, modelId, versionId) {
    /* For cars, if a specific brand-model-version was selected,
     * we can predict some ad params, based on the data we have,
     * most of the code was stolen from desktop.
     * What we predict is: [regdate, fuel, gearbox, cartype] */

    // cleaning before, to be sure to start from a good point
    var valuesToBePredicted = ['regdate', 'fuel', 'gearbox', 'cartype'];
    for (var i = 0; i < valuesToBePredicted.length; i++) {
      delete predictedValues[valuesToBePredicted[i]];
    }

    return $.ajax({
      async: false,
      timeout: 30000,
      data: {
        brand: brandId,
        model: modelId,
        version: versionId
      },
      dataType: 'json',
      url: '/params_get.json'
    }).done(function (data) {

      var attributes = data['es'][0]['attributes'];
      var regdates = [], fuels = [], gearboxes = [], cartypes = [];
      for (var i = 0; i < attributes.length; i++) {
        regdates.push(attributes[i].year);
        fuels.push(attributes[i].fuel);
        gearboxes.push(attributes[i].gearbox);
        cartypes.push(attributes[i].cartype);
      }

      var attrsOptions = {regdate: $.unique(regdates),
                          fuel: $.unique(fuels),
                          gearbox: $.unique(gearboxes),
                          cartype: $.unique(cartypes)
                         };

      for (var attr in attrsOptions) {
        if (attrsOptions[attr].length == 1) {
          predictedValues[attr] = attrsOptions[attr][0];
        }
      }

    });
  }

  return {
    predict: predict,
    predictedValues: predictedValues
  };
})();
