(function ($) {
  'use strict';

  /**
   *
   * @name horizontalPageSlide
   * @description
   *
   * Plugin for load a PageSlide with horizontal animate
   *
   * @param {DOMelement} element template that will be load inside pageSlide
   * @param {object} options for setting plugin
   *
   * @returns {void}
   *
   */
  $.horizontalPageSlide = function (element, options) {
    var defaults = {
      idPageOut: 'site-wrapper',
      classContent: 'pageSlideContent',
      classCustom: '',
      noImagesSelectors: [],
      title: '',
      type: 'none',
      optionSel: 'pageSlide-option',
      extraOptionSel: 'pageSlide-extraOption',
      select: '',
      onClose: function () {}
    };

    var plugin = this;
    var $element = $(element);
    var opsToChoice = {
      optionSelected: 0
    };

    plugin.settings = {};
    plugin.init = function () {
      plugin.settings = $.extend({}, defaults, options);

      if (plugin.settings.type === 'selectDashboard') {
        opsToChoice = {
          optionSelected: 0,
          labelSelected: 0,
          labelName: ''
        }
      }

      createPage();
    };

  /**
   *
   * @name horizontalPageSlide#open
   * @description
   *
   * Show the pageSlide when is already load the element
   *
   * @returns {void}
   *
   */
    $.horizontalPageSlide.open = function (options) {
      plugin.settings.select = options.select;

      handleDisabled();

      animatePages();
    };

  /**
   *
   * @name createPage
   * @description
   *
   * Initiatilize the creation of pageSlide
   *
   * @returns {void}
   *
   */
    var createPage = function () {
      buildTemplate(animatePages);
    };

  /**
   *
   * @name buildTemplate
   * @description
   *
   * Build the template for PageSlide and load content
   *
   * @param {function} animate callback for final animation
   *
   * @returns {void}
   *
   */
    var buildTemplate = function (animate) {
      var templateMain = $('<div />', { class: 'pageSlideIn' });
      var header = $('<div />', { class: 'pageSlideIn-header' });
      var button = $('<button />', { class: 'pageSlideIn-header-back' });
      var title = $('<h3 />', { class: 'pageSlideIn-header-title', html: plugin.settings.title });
      var content = $('<div />', { class: '' + plugin.settings.classContent + ' ' + plugin.settings.classCustom + '',  html: $element.html() });

      header.append([button, title]);
      templateMain.append(header);
      templateMain.append(content);
      $('body').append(templateMain);
      
      if (plugin.settings.type == 'selectDashboard') {
        addPrices(templateMain);
        handleDisabled();  
      }

      addClases();
      addEvents();
      animate();
    };

  /**
   *
   * @name addClases
   * @description
   *
   * Add clases to element who slideOut
   *
   * @returns {void}
   *
   */
    var addClases = function () {
      $('#' + plugin.settings.idPageOut + '').addClass('pageSlideOut');
    };

  /**
   *
   * @name addEvents
   * @description
   *
   * Load events like back button and label options click
   *
   * @return {void}
   *
   */
    var addEvents = function () {
      $('.pageSlideIn-header-back').click(function () {
        closeSlide();
      });

      $('body').on('click', '.' + plugin.settings.optionSel + '', function () {
        if ($(this).data('extra_option') === undefined && !$(this).hasClass('disabled')) {
          opsToChoice.optionSelected = $(this).data('option');
          closeSlide();
        }
      });
      
      $('body').on('click', '.' + plugin.settings.extraOptionSel + '', function (e) {
        var divOption = $(this).parents('.pageSlideSelectDashboard-option');
       
        if (!divOption.hasClass('disabled')) {
          opsToChoice.optionSelected = divOption.data('option');
          opsToChoice.labelSelected = $(this).data('value');
          opsToChoice.labelName = $(this).html();

          closeSlide();
        }

        e.preventDefault();
      });
    };
  
  /**
   *
   * @name addPrices
   * @description
   *
   * Add prices to options from select
   *
   * @returns {void}
   *
   */
    var addPrices = function () {
      var options = plugin.settings.select.find('option');
      
      options.each(function(index, option) {
        if (option.value != 0) {
          $('.pageSlideIn .' + plugin.settings.classContent + '-price_' + option.value + '').html($(option).data('price')); 
        }
      });
    };

  /**
   *
   * @name animatePages
   * @description
   *
   * Animate PageSlideIn and element SlideOut
   *
   * @returns {void}
   *
   */
    var animatePages = function () {
      var timeShow = 0;
      var timeSlide = 100;

      if ($('.pageSlideIn').hasClass('showSlide')) {
        timeShow = 350;
        timeSlide = 0;
      }
      setTimeout(function () {
        $('.pageSlideIn').toggleClass('showSlide');
      }, timeShow);

      setTimeout(function () {
        $('.pageSlideOut').toggleClass('slideOut');
        $('.pageSlideIn').toggleClass('slideIn');
        $('body').toggleClass('pageSlideScrollHidden');
      }, timeSlide);
    };

  /**
   *
   * @name closeSlide
   * @description
   *
   * Handle close pageSlide, callback and resetOpts
   *
   * @returns {void}
   *
   */
    var closeSlide = function () {
      animatePages();
      plugin.settings.onClose.call(opsToChoice, plugin.settings.select);
      resetOps();
    };
  
  /**
   *
   * @name handleDisabled
   * @description
   *
   * Handle disabled options
   *
   * @returns {void}
   *
   */
    var handleDisabled = function () {
      if (plugin.settings.select.data('images') == "0") {
        disabledOptions('images');
      }

      if (plugin.settings.select.data('premium-available') == "0" && plugin.settings.select.data('images') != "0") {
        disabledOptions('premium');
      }
    };

  /**
   *
   * @name disabledOptions
   * @description
   *
   * Disabled options when there arent iamges
   *
   * @returns {void}
   *
   */
    var disabledOptions = function (op) {
      var time = 0;
      
      if ($('.pageSlideIn .' + plugin.settings.classContent + '-noImages').hasClass('disabled') || 
          $('.pageSlideIn div[data-option="at_combo3"]').hasClass('disabled')) {
        time = 350;
      }

      setTimeout(function() {
        if (op == 'images') {
          $('.pageSlideIn .' + plugin.settings.classContent + '-noImages').toggleClass('__show');
        
          plugin.settings.noImagesSelectors.forEach(function (el, index) {
            $('.pageSlideIn div[data-option="' + el + '"]').toggleClass('disabled');
          });
        } else if (op == 'premium') {
            $('.pageSlideIn div[data-option="at_combo3"]').toggleClass('disabled');
        }
      }, time);
    };

  /**

  /**
   *
   * @name resetOps
   * @description
   *
   * Clean options
   *
   * @returns {void}
   *
   */
    var resetOps = function () {
      opsToChoice.optionSelected = 0;
      opsToChoice.labelSelected = 0;
      opsToChoice.labelName = '';
      
      handleDisabled(); 
    };

    plugin.init();
  };

  /**
   *
   * @name fn.horizontalPageSlide
   * @description
   *
   * Handle contruction of plugin and assing options
   *
   * @param {object} options for plugin
   *
   * @returns {void}
   *
   */
  $.fn.horizontalPageSlide = function (options) {
    return this.each(function () {
      if (undefined === $(this).data('horizontalPageSlide')) {
        var plugin = new $.horizontalPageSlide(this, options);

        $(this).data('horizontalPageSlide', plugin);
      } else {
        $.horizontalPageSlide.open(options);
      }
    });
  };
}
)(jQuery);
