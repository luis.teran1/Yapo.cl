/* globals dictionary */

var Config = (function() {

  function getNode(nodes, index) {
    // search a node in the tree of nodes
    // If you get rid of this function, I'll buy you a beer
    var output;
    $.each(nodes, function onEachNode(i, node) {
      if (output) return false;

      if (i === index) {
        output = node;
        return false;
      }

      if (node.childs) {
        var buffer = getNode(node.childs, index);
        if (buffer) output = buffer;
      }
    });

    return output;
  }

  return {

    getChildParamsNodes: function(config) {
      // node is a config object for a select / list like rooms
      // the values, are the selectable values like 1 room, 2 rooms, 3 rooms, etc
      // here we are mapping the backend response to the config that uses the horizontalSelect
      return $.map(config.values, function (val) {
        return {
          value: val.id,
          text: val.label
        };
      });
    },

    getNode: getNode,

    parseAdParamsResponse: function (data) {
      // parses the data response of the site for a specific category
      var parsedAdParams = {};
      $.map(data['es'][0].categories[0].types, function(element) {
        parsedAdParams[element.id] = {name: dictionary[element.label]};
        if (element.newad) {
          parsedAdParams[element.id]['params'] = Config.getParamsForAdParam(element.newad);
        }
      });
      return parsedAdParams;
    },

    getParamsForAdParam: function (nodes) {
      // maps from backend / server json data to the data that is used in frontend code
      return $.map(nodes, function(element) {
        var values = $.map(element.values, function(value) {
          return {id: value.id, label: value.value};
        });
        return {
          id: element.name,
          optional: element.optional,
          type: element.type,
          extended_type: element.extended_type,
          maxSelection: element.maxSelection,
          childs: {
            label: element.translate,
            values: values
          },
          validations: element.validations,
          translate: element.translate
        };
      });
    },

    getAdParamsItems: function(params) {
      // maps ad types s,k,h,u
      return $.map(params, function(node, index) {
        return {
          'value': index,
          'text': node.name
        };
      });
    },

    getDynamicItems: function (nodes, index) {
      // returns a list of childs for a specific node in the tree
      var childNode = Config.getNode(nodes, index);

      if (!childNode.childs) return [];
      return $.map(childNode.childs, function onEachChild(node, value) {
        return {
          description: node.description,
          icon: node.icon,
          text: node.name,
          value: value
        };
      });
    }
  };
})();
