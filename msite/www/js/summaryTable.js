/*
 * summaryTable drag handler
 *
 * */

var Yapo = Yapo || {};

Yapo.summaryTable = (function yapoComponent() {
  'use strict';
  var MODIFIER_CLASS = ' __swipe';
  var DRAG_ELEMENT_CLASS = 'summaryTable-drag';
  var CLOSE_ELEMENT_CLASS = 'summaryTable-closeIcon';
  var ARROW_ELEMENT_CLASS = 'summaryTable-arrowIcon';

  var ARROW_MODIFIER = ' __show';
  var CLOSE_MODIFIER = ' __rotate';

  function setListeners(callback) {
    function clickDelete(e) {
      deleteRow(e);
      callback(e);
    }

    var i;
    var elements = document.getElementsByClassName(DRAG_ELEMENT_CLASS);

    if (elements.length === 0) {
      return false;
    }

    for (i = 0; i < elements.length; i++) {
      // @TODO: Remove jQuery event
      $(elements[i]).on('swipe', swipeHandler);

      elements[i].addEventListener('click', clickHandler);

      elements[i].querySelector('.__delete').addEventListener('click', clickDelete);
    }
  }

  function swipeHandler(data) {
    var element = data.currentTarget;
    // @TODO: If jQuery is removed, this should be fixed too.
    var start = data.swipestart.coords[0];
    var stop = data.swipestop.coords[0];

    var close = element.querySelectorAll('.' + CLOSE_ELEMENT_CLASS)[0];
    var arrow = element.querySelectorAll('.' + ARROW_ELEMENT_CLASS)[0];

    if (start < stop) {
      // Right - Remove swipe class
      swipeRight(element, close, arrow);
    } else {
      // Left - Add swipe class
      swipeLeft(element, close, arrow);
    }
  }

  function clickHandler(data) {
    var target = data.target;
    var element = data.currentTarget;

    if (target.className.indexOf('Icon') === -1) {
      return false;
    }

    var findShowClass = data.currentTarget.className.indexOf(MODIFIER_CLASS) > -1 ? true : false;

    var close = element.querySelectorAll('.' + CLOSE_ELEMENT_CLASS)[0];
    var arrow = element.querySelectorAll('.' + ARROW_ELEMENT_CLASS)[0];

    if (findShowClass) {
      swipeRight(element, close, arrow);
    } else {
      swipeLeft(element, close, arrow);
    }
  }

  function swipeLeft(element, close, arrow) {
    element.className = DRAG_ELEMENT_CLASS + MODIFIER_CLASS;
    hideCloseIcon(close, arrow);
  }

  function swipeRight(element, close, arrow) {
    element.className = DRAG_ELEMENT_CLASS;
    showCloseIcon(close, arrow);
  }

  function showCloseIcon(close, arrow) {
    close.className = close.className.replace(CLOSE_MODIFIER, '');
    arrow.className = arrow.className.replace(ARROW_MODIFIER, '');
  }

  function hideCloseIcon(close, arrow) {
    close.className = close.className + CLOSE_MODIFIER;
    arrow.className = arrow.className + ARROW_MODIFIER;
  }

  function deleteRow(evnt) {
    var current = evnt.target;
    var notFound = true;

    while (notFound) {
      if (current.className === 'summaryTable-row') {
        current.className += ' __delete';
        notFound = false;
      } else {
        current = current.parentElement;
      }
    }
  }

  function summaryTable(callback) {
    setListeners(callback);
  }

  return summaryTable;
})();

