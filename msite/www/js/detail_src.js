var page = document.getElementById('page'),
	ua = navigator.userAgent,
	iphone = ~ua.indexOf('iPhone') || ~ua.indexOf('iPod'),
	ipad = ~ua.indexOf('iPad'),
	ios = iphone || ipad,
	// Detect if this is running as a fullscreen app from the homescreen
	fullscreen = window.navigator.standalone,
	android = ~ua.indexOf('Android'),
	modalMap;

$(document).ready( function(){
  $('#cotiza_raw_link').on('is-empty', function(e){
    $(this).parents('.services').remove();
  }).on('click', 'a', function(e){
    $div = $(this).parents('#cotiza_raw_link');
    try {
      xt_med('C', $div.data('xiti_xtn2'), $div.data('xtclick_name'), $div.data('xtclick_type'));
    }catch(e){}
  });

	buildPreviewMapImage();

	//Init map wrap height
	getWindowHeight();

	$('#map_container').click( function(e){
		e.preventDefault();
		showMapModal();
		try{xt_med('C', '', $(this).data('xtclick_name'), $(this).data('xtclick_type'));}catch(e){}
	});

	$('#close_modal_map').click( function(e){
		e.preventDefault();
		hideMapModal();
	});

	window.onorientationchange = function(){
		setTimeout(function(){
			adjustMapWrapSize();
			if(window.mapInstance){
				window.mapInstance.invalidateSize();
			}
		}, 300);
	};

	//Setup styles to handle orientation changes
	setPreviewMapClass();

	//send tealium tags
	$('#ad_reply_top').click(function(){
		$('#tealium_ad_reply_show_top').click();
	});
	$('#ad_reply_bottom').click(function(){
		$('#tealium_ad_reply_show_bottom').click();
	});

});

function buildPreviewMapImage(){
	w = $(window).innerWidth();
	if ($('#map_image_url').length > 0){
		var mapSrc = $('#map_image_url').val();
		$('#map_container').css("background-image", "url('" + mapSrc + "')");
	}
}

function showMapModal(){
	h = getWindowHeight();
	//Setup top position of modal so it opens from static map
	var top = ($('#map_container').offset().top - $(window).scrollTop() - 50);
	$('#modal_map').css('top', top);

	//reveal the modal
	$('#modal_map').show();
	setTimeout( function(){
		tryBuildMap();
		$('#modal_map').addClass('reveal-map');
		$('#modal_map').css('height', h);
	}, 50);
	//lock scroll for document
	$("body").css("overflow", "hidden");

	// Adjust map wrapper size in case orientation changed
	adjustMapWrapSize();
	if(window.mapInstance){
		window.mapInstance.invalidateSize();
	}
}

function hideMapModal(){
	$('#modal_map').hide();
	setTimeout( function(){
		$('#modal_map').removeClass('reveal-map');
		$('#modal_map').css('height', 210);
	}, 50);
	//unlock scroll for document
	$("body").css("overflow", "auto");
}

function changeImage(num) {
	var total = $(".bullets .bullet_num:visible").length;
	var i = 0;
	var current = 0;

	for (i = 0; i < total; i++) {
		if ($($(".bullets .bullet_num")[i]).hasClass("current")) {

			current = i;

			if ((current + num) < 0 ) num = (total -1);

			$($(".slides .item")[current]).css("display", "none");

			$($(".bullets .bullet_num")[current]).removeClass("current");
			$($(".bullets .bullet_num")[((current+num)%total)]).addClass("current");

			$($(".slides .item")[((current+num)%total)]).css("display", "inline");

			return false;
		}
	}
	return false;
}

function next() { return changeImage(1); }
function prev() { return changeImage(-1); }

function showReplyTop(){
	if($(".reply").css("display").toLowerCase() == 'none') {
		try{
			var click = $('#xt_reply_top').attr('click');
			var page = $('#xt_reply_top').attr('page');
			xt_med('C','',click,'N');
			xt_med('F','',page);
		} catch(err){}
	}
	return showReply();
}

function showReplyBottom(){
	if($(".reply").css("display").toLowerCase() == 'none') {
		try{
			var click = $('#xt_reply_bottom').attr('click');
			var page = $('#xt_reply_bottom').attr('page');
			xt_med('C','',click,'N');
			xt_med('F','',page);
		} catch(err){}
	}
	return showReply();
}

function showReply() {

	var beforeResponse = $("#form_response_ok").is(":hidden");
	$("#form_adreply").show();
	$("#form_response_ok").hide();
	if(beforeResponse){
		$(".reply").toggle();
		if(!$(".reply").is(":hidden")){
			$('html,body').animate({scrollTop: $(".reply").offset().top - 10});
		}
	}else{
		$("#adreply_body").val("");
	}
	document.location = "#TOP";
        return false;
}

var elements=["name","email","adreply_body", "phone"];

$(window).load(function(){
	var s="";
	$(".slides .item").each(function(index) {
		s = s+"<span class=\"bullet_num";

		if (index == 0) {
			s = s+" current";
		}
		s = s+"\">"+(index+1)+"</span>";
	});
	$(".bullets").html(s+"<span style=\"clear: left\"></span>");
	$(".slides .item").hide();
	$(".slides .item").first().show();

	$("#send").click(function() {
		for(var i=0; i<elements.length; ++i) {
			var e=$("#ar_err_"+elements[i]);
			e.html("");
			e.hide();
		}
		var adReplyBody = $("#adreply_body").val().replace(/\r\n|\r|\n/g, " ");
		$.ajax({
			type: "GET",
			url: window.location.protocol+"//"+window.location.host+"/adreply.json",
			data: ({id: $("#list_id").val(), name: $("#yourName").val(), email: $("#yourEmail").val(), adreply_body: adReplyBody, cc: $("#cc").is(":checked")?1:0, phone: $("#yourPhone").val() }),
			dataType: "json",
			success: function (data) {
				if(data.status=="OK") {

					$("#form_adreply").hide();
					$("#form_response_ok").show();
					$('html,body').animate({scrollTop: $("#form_response_ok").offset().top - 10});

					/* add stats */
					if (document.getElementById("stats_1x1")) {
						document.getElementById("stats_1x1").src = document.getElementById("stats_1x1").src.replace('_vi', '_arajax');
					}

					$("#tealium_mail_submit").click();

					/* xiti metrics for ad_reply */
					var xtcustom = $('#send_adreply').val();
					try{
						return xt_click(this, 'F', '', xtcustom);
					}catch(e){}

				} else {
					var madeScroll = false;
					for(i=0; i<elements.length; ++i) {
						var n=elements[i];
						var e=$("#ar_err_"+n);
						if("err_"+n in data) {
							e.html(data["err_"+n]);
							e.show();
							madeScroll = true;
						}
					}
					if(madeScroll){
						$('html,body').animate({scrollTop: $(".reply").offset().top - 10});
					}
					var xtcustom = document.getElementById('sendTipErrXiti').value;
					var xtcustom_var = get_page_custom_variables();
					xt_med('F','','foo&stc=' + xtcustom + '&' + xtcustom_var);
					$("#tealium_mail_submit_error").click();
				}
			}
		})
	});
});

function tryBuildMap(){
	if(!$('#modal_map_wrap').html().trim()){
		$('#modal_map_wrap').yapomap({
			debug: true,
			appl: 'mvi',
			appId: $('#modal_map_wrap').data('app_id'),
			getPreciseOptionValue: function(){ return (this.$precise.data('precise') == '1'); },
			preciseInput: $('#modal_map_wrap'),
			appCode: $('#modal_map_wrap').data('app_code'),
			center: $('#modal_map_wrap').data('location'),
			baseUrl: $('#base_url').val(),
			marker: $('#modal_map_wrap').data('location'),
			zoom: $('#modal_map_wrap').data('zoom'),
			draggable: false
		});
	}
}

function adjustMapWrapSize(){
	w = $(window).innerWidth();
	h = getWindowHeight();
	hf = ($('#modal_map_footer').height()) + 22;
	hh = ($('.modal-map-header').height()) + 32;
	$('#modal_map').css('height', h);
	$('#modal_map_wrap').css('width', w);
	$('#modal_map_wrap').css('height', (h - (hf + hh)));
}

function getWindowHeight(){
	var h = $(window).innerHeight();
	var ua = window.navigator.userAgent;
	if (iphone && !fullscreen){
		h = getIOSWindowHeight();
	}
	return h;
}

function isLandscape(){
	return window.innerWidth > window.innerHeight;
}

function setPreviewMapClass(){
	if(isLandscape()){
		$('#map_container').addClass('landscape');
	}else{
		$('#map_container').addClass('portrait');
	}
}

var getIOSWindowHeight = function() {
    var zoomLevel = document.documentElement.clientWidth / window.innerWidth;
    return window.innerHeight * zoomLevel;
};
