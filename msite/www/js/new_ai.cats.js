/* globals adParams:true, categories, images, validator, Config, returnToForm,
 Draft, Truquero, category_xt */

 function AdParamSelect(config, items) {
  this.name = config.id;
  this.createElement(config, items);
}


AdParamSelect.prototype.createElement = function(config, items) {
  // Creates the horizontal select, trigger, the native select based on the config
  this.selectId = config.id;
  var isMultiple = config.type === 'multidropdownlist';
  var onSelect = this.onSelect;

  var defaultsLevelOptions = {
    multiple: isMultiple,
    onSelect: $.proxy(onSelect, this),
    maxSelection: config.maxSelection
  };

  var levelOptions = $.extend({}, defaultsLevelOptions, config.levelOptions);

  this.wrap = $.horizontalSelect.getNewLevel(config.parentSelectId,
    this.selectId, config.childs.label, items, levelOptions);

  // if only 1 option is present, we do not display the ad param, and we select directly the only value
  // We use 2 here because the label is prepended to the items array, so 1 option + the label = 2
  if (this.shouldShow()) {
    this.show();
  }

  var elementName = this.selectId;

  if (isMultiple) {
    elementName += '[]';
  }

  validator.settings.rules[elementName] = config.validations;
};


AdParamSelect.prototype.load = function() {
  // handle dependencies or load anything you may need to use this select
  var fn = 'load' + this.name.charAt(0).toUpperCase() + this.name.slice(1);
  if (typeof this[fn] === 'function') {
    this[fn]();
  }
};


AdParamSelect.prototype.onSelect = function adParamOnSelect(event, select) {
  // what to do when an ad param is selected in the horizontal select
  var selectId = select.attr('id');
  validator.element('#' + selectId);

  Draft.nodeOnChange(AdParams);

  // XXX, we should config this
  if (selectId === 'estate_type') {
    AdParams.reset(3);
  } else if (selectId === 'footwear_gender') {
    AdParams.reset(4);
  } else {
    var index;

    $.each(AdParams.params, function onEachParam(i, param) {
      if (param.name === selectId) {
        index = i;
        return false;
      }
    });

    if (index) {
      AdParams.currentIndex = index;
    }
  }

  if (select.is('[multiple]')) {
    var $nextActionButton = $('#hsp-' + selectId + ' .close');

    if (select.val()) {
      $nextActionButton.addClass('btn-ready').text('Listo');
    } else {
      $nextActionButton.removeClass('btn-ready').html('<i class="icon-yapo icon-close"/>');
    }

    if ($("#eqp-skip-button").length) {
      setEquipmentState();
    }

    return;
  }

  Categories.catInfo.trigger('next');
};


AdParamSelect.prototype.loadModel = function() {
  // loads the models of a specific car brand

  if ($('#brand').val() !== '')  {
    $.ajax({
      async: false,
      data: {
        brand: $('#brand').val()
      },
      dataType: 'json',
      url: '/params_get.json'
    }).done(function onGetModels(data) {

      var $modelsSelector = $('#model');

      $modelsSelector.empty();
      $modelsSelector.data('oldValue', '');

      $.each(data.es[0].newad[0].values, function onEachModel(index, model) {
        var option = $('<option>', {
          value: model.id,
          text: model.value
        });
        $modelsSelector.append(option);
      });

      $.horizontalSelect.updatePage('model');
    });
  }


  this.show();
};


AdParamSelect.prototype.loadVersion = function() {
  // loads the versions of a specific car brand / model
  $.ajax({
    async: false,
    data: {
      brand: $('#brand').val(),
      model: $('#model').val()
    },
    dataType: 'json',
    url: '/params_get.json'
  }).done(function onGetVersions(data) {

    var $versionSelector = $('#version');

    $versionSelector.empty();
    $versionSelector.data('oldValue', '');

    $.each(data.es[0].newad[0].values, function onEachVersion(index, version) {
      var option = $('<option>', {
        value: version.id,
        text: version.value
      });

      $versionSelector.append(option);
    });

    $.horizontalSelect.updatePage('version');
  });


  this.show();
};


AdParamSelect.prototype.step = function() {
  $.horizontalSelect.changePage(this.name);
};


AdParamSelect.prototype.setValue = function(value) {
  if (!value) {
    return;
  }

  if (typeof value  === 'string') {
    $('#hsp-' + this.name + ' li a[value="' + value + '"]').click();
  } else {
    for (var i = 0; i < value.length; i++) {
      $('#hsp-' + this.name + ' li a[value="' + value[i] + '"]').click();
    }
  }
};


AdParamSelect.prototype.getValue = function() {
  return this.wrap.find('select').val();
};


AdParamSelect.prototype.show = function() {
  this.wrap.find('.hs-trigger').show();
};


AdParamSelect.prototype.shouldShow = function() {
  var length = this.name === 'model' || this.name === 'version' ? 1 : 2;
  return this.wrap.find('select option').length > length;
};

/**
 *
 * @name AdParamInput
 * @constructor
 * @description
 *
 * Create a new AdParamInput.
 *
 * @param {Object} config - Configuration to create a new input
 *
 */
function AdParamInput(config) {
  this.name = config.id;
  this.createElement(config);
}


/**
 *
 * @name AdParamInput#createElement
 * @public
 * @description
 *
 * Creates the input and trigger based on the config.
 *
 * @param {Object} config - Options to create an input element.
 *
 */
AdParamInput.prototype.createElement = function(config) {
  this.input = $('<input/>', {
    name: config.id,
    type: config.extended_type,
    placeholder: config.childs.label,
    'data-parent': config.parentSelectId
  });

  if (config.extended_type === 'number') {
    this.input.attr('pattern', '[0-9]*');
  }

  $('#cat_info').append(this.input);

  this.input.on('change', function onClick() {
    Draft.nodeOnChange(AdParams);
  });

  validator.settings.rules[config.id] = config.validations;
};


/**
 *
 * @name AdParamInput#load
 * @public
 * @description
 *
 * Empty function =(.
 *
 */
AdParamInput.prototype.load = function() {
  // Empty function to works with AdParams and doesn't add `if` =(
};


/**
 *
 * @name AdParamInput#getValue
 * @public
 * @description
 *
 * Return the current value of `this.input`.
 *
 * @return {String}
 *
 */
AdParamInput.prototype.getValue = function() {
  return this.input.val();
};


/**
 *
 * @name AdParamInput#setValue
 * @public
 * @description
 *
 * Update value of `this.input` and trigger a blur event.
 *
 * @param {String} value - New value for `this.input`
 *
 */
AdParamInput.prototype.setValue = function(value) {
  this.input.val(value).trigger('blur');
};




/**
 *
 * @name AdParamCheckbox
 * @constructor
 * @description
 *
 * Create a new AdParamCheckbox.
 *
 * @param {Object} config - Configuration to create a new checkbox.
 *
 */
function AdParamCheckbox(config) {
  this.name = config.id;
  this.createElement(config);
}


/**
 *
 * @name AdParamCheckbox#createElement
 * @public
 * @description
 *
 * Creates the checkbox and trigger based on the config.
 *
 * @param {Object} config - Options to create a new checkbox.
 *
 */
AdParamCheckbox.prototype.createElement = function(config) {
  this.checkbox = $('<a/>', {
    id: this.name,
    text: config.translate,
    class: 'checkbox-param multiselect ui-btn ui-btn-icon-right ui-icon-carat-r'
  });

  var check =  $('<div/>', {
    class: 'check'
  });
  var icon = $('<i/>', {
    class: 'icon-yapo icon-check'
  });

  check.append(icon);
  this.checkbox.append(check);

  this.checkbox.on('click', function onClick() {
    $(this).toggleClass('selected');
    Draft.nodeOnChange(AdParams);
  });
};


/**
 *
 * @name AdParamCheckbox#load
 * @public
 * @description
 *
 * Empty function =(.
 *
 */
AdParamCheckbox.prototype.load = function() {
  // Empty function to works with AdParams and doesn't add `if` =(
};


/**
 *
 * @name AdParamCheckbox#getValue
 * @public
 * @description
 *
 * Return the current value of `this.checkbox`.
 *
 * @return {Number}
 *
 */
AdParamCheckbox.prototype.getValue = function() {
  return this.checkbox.hasClass('selected') ? 1 : 0;
};


/**
 *
 * @name AdParamCheckbox#setValue
 * @public
 * @description
 *
 * Toogle `selected` cssClass of `this.checkbox`
 *
 * @param {Boolean} value - Flag to change the value of `this.checkbox`.
 *
 */
AdParamCheckbox.prototype.setValue = function(value) {
  var isSelected = this.checkbox.hasClass('selected');

  if (value && !isSelected || !value && isSelected) {
    this.checkbox.toggleClass('selected');
  }
};

function ModifyLegend(legendId, text) {
  var legend = document.getElementById(legendId).getElementsByTagName('legend')[0];
  legend.innerHTML = text;
}

function ModifyHspTitle(id, text) {
  var hsp = document.getElementById(id);
  var title = hsp.getElementsByTagName('h1')[0];
  title.innerHTML = text;
}

function UpdateLocationLegend(category) {
  if (category === '1000') {
    ModifyLegend('loc_info', '2. Ubicaci�n de tu inmueble *')
    ModifyHspTitle('hsp-region', 'Regi�n de tu inmueble');
    ModifyHspTitle('hsp-communes', 'Comuna de tu inmueble');
  } else {
    ModifyLegend('loc_info', '2. Ubicaci�n de tu aviso *')
    ModifyHspTitle('hsp-region', 'Regi�n');
    ModifyHspTitle('hsp-communes', 'Comuna');
  }
}

function UpdateGeolocationVisibility(category) {
  var locInfoElement = document.getElementById('loc_info');
  if (category === '1000') {
    locInfoElement.querySelector('.new-location-box').style.display = 'none';
    document.querySelector('adinsert-map').style.display = 'block';
  } else {
    locInfoElement.querySelector('.new-location-box').style.display = 'block';
    document.querySelector('adinsert-map').style.display = 'none';
    var adinsertMapShadow = document.querySelector('adinsert-map-shadow');

    if(adinsertMapShadow){
      adinsertMapShadow.shadowRoot
        .querySelector('.adinsert-map__clean-button-container')
        .querySelector('.adinsert-map__button').click();
    }
  }
}

var Categories = (function() {

  function onMainCategorySelect(event, select) {
    var category = select.val();
    // creates the select page, and what is shown to the user after selecting values
    var parentSelectId = select.attr('id');
    var config = {
      id: 'category',
      parentSelectId: parentSelectId,
      type: 'dropdownlist',
      childs: {
        label: 'Subcategor&iacute;as' // @TODO: change this crap
      },
      validations: {
        required: 'required'
      },
      levelOptions: {
        clean: 1, // if category changes, it cleans everything downstream
        onSelect: onCategorySelect
      }
    };
    var items = Config.getDynamicItems(categories, category);  // this function is only called from here
    var element;

    // this code creates the new level
    AdParams.reset(0);

    element = new AdParamSelect(config, items);
    AdParams.push(element);
    select.data('childId', element.selectId);

    categoryTriggerLoadAccountRegion(category);
    Categories.catInfo.append(element.wrap);
    element.wrap.find('select').attr('f', category_xt);
    Categories.catInfo.trigger('next');
    Draft.nodeOnChange(AdParams);
    UpdateLocationLegend(category);
    UpdateGeolocationVisibility(category);
  }

  function onCategorySelect(event, select) {
    var category = select.val();
    /* when the category is selected: the ad params should be loaded and displayed */
    AdParams.reset(1);
    AdParams.load(category);
    Draft.nodeOnChange(AdParams);
    Yapo.toggleJobEditionAdvice(category, category_list[category].name, category_features, category_settings);
    Yapo.toggleJobInsertionPriceAdvice(category, category_features, category_settings, true);
  }


  function init() {
    this.catInfo = $('#cat_info');
    $('#parent_category').horizontalSelect({
      clean: 1, // if parent_category changes, it cleans everything downstream
      onSelect: onMainCategorySelect
    });
    UpdateGeolocationVisibility('');
  }

  return {
    init: init
  };
})();




var AdParams = (function() {

  var currentIndex = -1; // so first next step starts with 0 index
  var params = [];
  var knownValues = {};
  var id = 'AdParams';


  function create(parentSelect, childs) {
    /* create the elements in the form for each ad param */
    
    var parentSelectId = parentSelect.attr('id');
    var textControlsWrapper;

    // SELECTS: loading all the selects first with their triggers, select element, and the horizontal select page
    $(childs).filter(function filterSelect(index, node) {
      var type = node.type;
      return type === 'dropdownlist' || type === 'multidropdownlist';
    }).each(function onEachSelect(index, node) {

      node.parentSelectId = parentSelectId;
      // this function is only called from here
      var items = Config.getChildParamsNodes(node.childs);

      // XXX, we should config this
      if (node.id === 'estate_type' || node.id === 'footwear_gender') {
        node.levelOptions = { clean: 1 };
      }

      var adParam = new AdParamSelect(node, items);

      AdParams.push(adParam);
      parentSelect.parent().parent().append(adParam.wrap);
      $('#' + parentSelectId).data('childId', adParam.selectId);
      parentSelectId = adParam.selectId;
    });


    // INPUTS: loading all the inputs then with their "triggers" and the input element
    $(childs).filter(function filterTextField(index, element) {
      return element.type === 'textfield';
    }).each(function onEachTextField(index, node) {

      if (!textControlsWrapper) {
        textControlsWrapper = $('<div/>', {
          class: 'new-ai-box'
        });
      }

      node.parentSelectId = parentSelectId;
      var adParam = new AdParamInput(node);
      AdParams.push(adParam);
      textControlsWrapper.append(adParam.input);
    });

    $(childs).filter(function filterTextField(index, element) {
      return element.type === 'extra_info';
    }).each(function onEachTextField(index, node) {

      if (!textControlsWrapper) {
        textControlsWrapper = $('<div/>', {
          class: 'new-ai-box'
        });
      }

      var msgbox = '<div class="aiMessage">';
          msgbox  +='<i class="icon-yapo icon-info-circled aiMessage-infoIcon"></i>';
          msgbox  +='<span>'+node.translate+'</span>';
          msgbox  +='</div>';

      textControlsWrapper.append(msgbox);
    });

    $(childs).filter(function filterCheckbox(index, element) {
      return element.type === 'checkbox';
    }).each(function onEachCheckbox(index, node) {

      var adParam = new AdParamCheckbox(node);
      AdParams.push(adParam);
      textControlsWrapper.append(adParam.checkbox);
    });

    if (textControlsWrapper) {
      parentSelect.parent().parent().append(textControlsWrapper);
      textControlsWrapper.find('input').mdTextField();
    }
  }


  function init() {
    $('#cat_info').on('next', $.proxy(next, this));
  }


  function reset(index) {
    this.params = this.params.slice(0, index);
    this.currentIndex = index - 1;
  }


  function getValue() {
    /* Gets the value of the ad params and category currently loaded */
    var output = {
      'parent_category': $('#parent_category').val()
    };

    this.params.forEach(function onEachParam(param) {
      output[param.name] = param.getValue();
    });

    return JSON.stringify(output);
  }


  function setValue(value) {
    if (!value) {
      return;
    }

    var paramList = {
      'services': 'srv',
      'equipment': 'eqp',
      'job_category': 'jc',
      'service_type': 'sct',
      'footwear_type': 'fwtype'
    };
    var triggerValue;

    /* Sets the values for the entire module's ad params */
    if (typeof value === 'string') {
      this.knownValues = JSON.parse(value);
    } else {
      this.knownValues = value;
    }

    for (var k in paramList) {
      if (k in this.knownValues) {
        this.knownValues[paramList[k]] = this.knownValues[k].split(',');
        delete this.knownValues[k];
      }
    }

    triggerValue = this.knownValues.parent_category; // eslint-disable-line camelcase
    $('#hsp-parent_category li a[value="' + triggerValue + '"]').trigger('click');
  }


  function next(event, automatic) {
    var previousElement = this.params[this.currentIndex];
    var value;
    var element;

    if (previousElement) {
      this.afterSelect(previousElement.name);
    }

    if (this.params.length > this.currentIndex + 1) {
      this.currentIndex += 1;
    } else {
      returnToForm();
      return;
    }

    // XXX: we are just predicting brand/model/version for now, so this is hardcoded :D, blame Javier
    if ($('#category').val() === '2020' && previousElement.name === 'version') {
      Truquero.predict({
        brand: $('#brand').val(),
        model: $('#model').val(),
        version: $('#version').val()
      });
      $.extend(this.knownValues, Truquero.predictedValues, this.knownValues);
    }

    // get next param
    element = this.params[this.currentIndex];
    // checks & loads stuff it needs to do his work, used with dependant variables
    element.load();

    // handle known values, so these are set before anything else
    // used to load data automagically
    if (element.name in this.knownValues) {
      value = this.knownValues[element.name];
      delete this.knownValues[element.name];

      if (element instanceof AdParamCheckbox) {
        // AdParamCheckbox just allow integer values
        value = value.toString().length ? parseInt(value, 10) : 0;
      }

      element.setValue(value);

      // for type k "busco", or other, if the value is no longer present, we select the only option
      var val = element.getValue();
      if (JSON.stringify(val) !== JSON.stringify(value)) {
        selectTheOnlyValue(element);
      }

      Categories.catInfo.trigger('next', true);

      return;
    }

    if (shouldMoveToRegionSelect(previousElement, element, automatic)) {
      moveToRegionSelect();
      return;
    }

    if (typeof element.step === 'function') {
      if (element.shouldShow()) {
        element.step();
      } else {
        // select the only value and continue
        selectTheOnlyValue(element);
      }
    } else {
      returnToForm();
    }
  }

  function shouldMoveToRegionSelect(previousElement, element, automatic) {
    return (previousElement instanceof AdParamSelect && !(element instanceof AdParamSelect) && !automatic);
  }

  function moveToRegionSelect() {
    var regionSelect = document.getElementById('hst-region');
    var regionSelectLink = regionSelect.getElementsByTagName('a')[0];
    regionSelectLink.click();
  }

  function selectTheOnlyValue(element) {
    // select the only value and continue
    var value = element.wrap.find('select option:nth-child(2)').attr('value');
    // XXX: for model and version, select have only one element
    if (!value) {
      value = element.wrap.find('select option:nth-child(1)').attr('value');
    }
    element.setValue(value);
  }


  function createAdType() {
    // loads and display the type of the ad, (s,k,u,h), with the select
    var parentSelect = $('#category');
    var items = Config.getAdParamsItems(adParams);
    var node = {
      id: 'type',
      type: 'dropdownlist',
      parentSelectId: parentSelect.attr('id'),
      childs: {
        label: 'Tipo'
      }, // I know, this is bullshit
      validations: {
        required: 'required'
      }
    };
    var select = new AdParamSelect(node, items);

    this.push(select);
    Categories.catInfo.append(select.wrap);
    Categories.catInfo.trigger('next');
  }


  function load(category) {
    // loads the ad params for a specific category
    $.ajax({
      async: false,
      data: {
        'child_category': category
      },
      dataType: 'json',
      url: '/params_get.json'
    }).done(function onLoadAdParams(data) {
      images.setMaxImages(parseInt(data.es[0].categories[0].total_images, 10));
      adParams = Config.parseAdParamsResponse(data);
      AdParams.createAdType();
    });
  }


  function push(element) {
    // to add a new ad param to the inner list
    this.params.push(element);
  }


  function afterSelect(name) {
    // checks if with the current selected values, we can / should load more fields (ad params) in this fieldset
    var upperName = name.split('_').map(function onEachNamePart(word) {
      return word.charAt(0).toUpperCase() + word.slice(1);
    }).join('');
    var functionName = 'afterSelect' + upperName;

    if (typeof this[functionName] === 'function') {
      this[functionName]();
    }
  }


  function labelKeyLookup(key) {
    return $('#'+key).val();
  }


  function getLabelFor(param) {
    var setting = split_setting(get_settings(param, labelKeyLookup, label_settings));
    return setting.label || '';
  }


  function afterSelectCategory() {
    $('label[for="price"]').text(getLabelFor('price_mai'));
    $('label[for="uf_price"]').text(getLabelFor('price_mai'));
  }


  function afterSelectType() {
    // this function handles when the ad type (s,k,h,u) is selected,
    // so new ad params should be loaded
    var $type = $('#type');
    var childNode = Config.getNode(adParams, $type.val());
    var childs = childNode.params;
    if (childs) {
      // this function is only called from here
      AdParams.create($type, childs);
    }
    $('label[for="price"]').text(getLabelFor('price_mai'));
    $('label[for="uf_price"]').text(getLabelFor('price_mai'));
  }


  function afterSelectEstateType() {
    // loads the ad params for a specific category (real estate)
    $.ajax({
      async: false,
      data: {
        'child_category': $('#category').val(),
        'estate_type': $('#estate_type').val()
      },
      dataType: 'json',
      url: '/params_get.json'
    }).done(function onLoadEstateTypeParams(data) {
      createChilds(data, '#estate_type', 1);
    });
    setOptionalInputs();
  }

  function afterSelectFootwearGender() {
    // loads the ad params for a specific category (real estate)
    $.ajax({
      async: false,
      data: {
        'child_category': $('#category').val(),
        'footwear_gender': $('#footwear_gender').val()
      },
      dataType: 'json',
      url: '/params_get.json'
    }).done(function onLoadFootwearParams(data) {
      createChilds(data, '#footwear_gender', 2);
    });
  }

  function createChilds(data, id, slice) {
    adParams = Config.parseAdParamsResponse(data);
    var childNode = Config.getNode(adParams, $('#type').val());
    var childs = childNode.params;
    if (childs) {
      AdParams.create($(id), childs.slice(slice, childs.length));
    }
  }

  function setOptionalInputs() {
    var optionalParameters = [
      'util_size',
      'size',
      'built_year',
      'garage_spaces',
      'condominio',
      'irrig_hect',
    ];

    var textFieldWrappers = document.getElementsByClassName('mdTextField-wrapper');
    var textFieldWrappersLength = textFieldWrappers.length;

    for(var i = 0; i < textFieldWrappersLength; i++){
      var textFieldWrapper = textFieldWrappers[i];
      var inputElement = textFieldWrapper.getElementsByTagName('input')[0];
      var labelElement = textFieldWrapper.getElementsByTagName('label')[0];

      if(inputElement){
        var inputElementName = inputElement.name;

        if(optionalParameters.includes(inputElementName)){

          if(labelElement){
            labelElement.innerHTML = labelElement.innerHTML + ' (Opcional)';
          }
    
          var selector = '[name=' + inputElementName + ']';
          $(document).on('focus', selector, { labelElement: labelElement }, focusUtil);
          $(document).on('focusout', selector, { labelElement: labelElement }, focusUtil);
        }
      }
    }
  }

  function focusUtil(event){
    var labelElement = event.data.labelElement;
  
    if(event.type == 'focusin'){
      event.target.placeholder = '(Opcional)';
      labelElement.innerHTML = labelElement.innerHTML.replace(' (Opcional)', '');
    }
    if(event.type == 'focusout'){
      event.target.placeholder = '';

      if(!labelElement.innerHTML.includes(' (Opcional)')){
        labelElement.innerHTML = labelElement.innerHTML + ' (Opcional)';
      }
    }
  }

  function initDraft() {
    Draft.draftMe(AdParams);
  }


  function clear() {
    /* Clears the cat_info fieldset, called from draft clear */
    AdParams.reset(0);
    $('#parent_category').val('').trigger('change');
    $('label[for="price"]').text(getLabelFor('price_mai'));

    if (validator) {
      validator.resetForm();
    }

    $('.wrap-error').removeClass('wrap-error');

    UpdateLocationLegend('');
    UpdateGeolocationVisibility('');
  }

  return {
    init: init,
    createAdType: createAdType, // is not mandatory to be here
    create: create, // is not mandatory to be here
    load: load,
    params: params, // is not mandatory to be here
    currentIndex: currentIndex, // is not mandatory to be here
    reset: reset,
    push: push,
    afterSelect: afterSelect,
    afterSelectCategory: afterSelectCategory,
    afterSelectType: afterSelectType,
    afterSelectEstateType: afterSelectEstateType,
    afterSelectFootwearGender: afterSelectFootwearGender,
    id: id,
    getValue: getValue,
    setValue: setValue,
    initDraft: initDraft,
    knownValues: knownValues,
    clear: clear
  };
})();
