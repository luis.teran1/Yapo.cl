$(document).ready(function() {
        displayIfAndroid(navigator.userAgent);
});

function displayIfAndroid(userAgent){
        var android_cookie = getCookie("android_banner_close");

        if ($('.g-button').size() > 0 && isAndroidVersionOrNewer(userAgent, '2.2') && android_cookie == null) {

                $('.g-button').css('display', 'inline-block');
                $('.logo .link').addClass('android');

                $("#android_app").click(function(event){
                        event.preventDefault();
                        xt_click(this,'C','',android_banner.xtclick,'N');
                        window.open(android_banner.link);
                        return false;
                });

                $("#android_banner_close").click(function(event){
                        event.preventDefault();
                        document.cookie="android_banner_close=true;max-age=7776000"
                        $('.g-button').css('display', 'none');
                        return false;
                });
        }
}
