/* global fbConnect, loginBconf, xt_click */
/* eslint no-useless-escape:0, wrap-iife:["error", "inside"] */

(function() {
  'use strict';

  var baseUrl;
  var shortReferer;

  $(document).ready(onReady);
  $(document).on('submit', '.loginBox form', login);
  $(document).on('submit', '#facebook-email-form', changeEmail);
  $(document).on('submit', '#facebook-password-form', joinAccount);
  $(document).on('click', '.facebookBox-backBtn', back);
  $(document).on('click', '#fbConnectBtn', fbConnectBtnClick);
  $(document).on('click', '#ads_without_account', adsWithoutAccountClick);
  $(document).on('click', '#customGoogleBtn', googleConnectBtnClick);

  function onReady() {
    baseUrl = loginBconf.baseUrl;
    shortReferer = $('.loginBox input[name=short_referer]').val();
    
    googleConnect.config({
      baseUrl: baseUrl,
      onSuccess: onGoogleSuccess,
      onError: hideLoading,
    });
    
    var createAccountEl = document.querySelector('.loginBox');
    
    if (createAccountEl) {
      googleConnect.attachSigninButton(document.querySelector('#customGoogleBtn'));
    }
    
    fbConnect.config({
      baseUrl: baseUrl,
      source: 'msite',
      selector: '.loginBox #fbConnectBtn',
      showLoading: showLoading,
      onConnect: function onConnect() {
        hideLoading();

        var redirect = getCookie('ref_go_back');
        if (redirect) {
          deleteCookie('ref_go_back');
          location.href = decodeURIComponent(redirect);
        } else if (shortReferer === 'publica-un-aviso') {
          location.href = baseUrl + '/publica-un-aviso';
        } else {
          location.href = baseUrl + '/dashboard';
        }
      },
      onRequireEmail: function onRequireEmail(user) {
        $('#facebook-box .facebookBox-header-title').text('Hola ' + user.name);
        $('#facebook-box .facebookBox-header-userImage').attr('src', user.picture);
        $('#facebook-email-form').show();

        if (user.email) {
          $('#facebook-email').val(user.email).trigger('input');
        } else {
          $('.facebookBox-header-description:first').text(
            loginBconf.messages.LOGIN_REQUIRE_EMAIL);
        }

        hideLoading();

        $.mobile.changePage('#facebookMobilePage', { transition: 'slide' });
      },
      onRequirePassword: function onRequirePassword(user) {
        $('.facebookBox-header-title').text('Hola ' + user.name);
        $('.facebookBox-header-userImage').attr('src', user.picture);
        $('.facebookBox-header-description span').text(user.email);

        hideLoading();

        $('#facebook-box').addClass('__requirePassword');
        $('#facebook-box > div').toggleClass('is-hidden');
      },
      onError: function onError(response) {
        var error;
        var errors;
        var $element = $('#facebookLoginError');

        hideLoading();
        if (response.cancel) { return; }

        errors = {
          EMAIL_NOT_VERIFIED: loginBconf.messages.ERROR_EMAIL_NOT_VERIFIED,
          LOGIN_FAILED: loginBconf.messages.ERROR_LOGIN_FAILED,
          ERROR_ACCOUNT_CANNOT_REPLACE_FACEBOOK_ID:
            loginBconf.messages.ERROR_ACCOUNT_CANNOT_REPLACE_FACEBOOK_ID,
          ERROR_ACCOUNT_FACEBOOK_ID_IN_USE: loginBconf.messages.ERROR_ACCOUNT_FACEBOOK_ID_IN_USE,
          VALIDATION_FAILED: loginBconf.messages.ERROR_VALIDATION_FAILED,
          CONECTION_ERROR: loginBconf.messages.ERROR_CONECTION_ERROR,
          SOCIAL_ACCOUNT_EXISTS: loginBconf.messages.ERROR_SOCIAL_ACCOUNT_EXISTS,
          BAD_BROWSER_FOR_FACEBOOK: loginBconf.messages.ERROR_BAD_BROWSER_FOR_FACEBOOK
        };

        error = errors[response.code] || 'Mensaje de error gen�rico';

        if (response.code === 'EMAIL_NOT_VERIFIED') {
          $('.facebookBox-header-description:first').text(error);
          $('#facebook-email-form').hide();
          return;
        }

        if (!$('.loginBox').is(':visible')) {
          $element = $('.facebookBox .loginFeedback');
        }

        $element
          .text(error)
          .addClass('__error')
          .show();
      }
    });

    $('input[type=email], input[type=password]').mdTextField();

    $('#facebookMobilePage').appendTo($.mobile.pageContainer);
    $('#my_ads_page').appendTo($.mobile.pageContainer);

    Yapo.aiCategoryLogin(categoryReferer, forcedLoginData)
  }

  function login() {
    var $email = $('.loginBox input[name=accbar_email]');
    var $password = $('.loginBox input[name=accbar_password]');
    var $shortReferer = $('.loginBox input[name=short_referer]');
    var $form = $(this);

    var emailError;
    var passwordError;

    $('.loginBox .loginFeedback:last').hide();

    emailError = validateEmail($email);
    passwordError = validatePassword($password);

    if (emailError || passwordError) { return false; }

    showLoading(true);

    $.ajax({
      url: $form.attr('action'),
      method: 'POST',
      data: {
        email: $email.val(),
        password: $password.val(),
        short_referer: $shortReferer.val()
      },
      xhrFields: {
        withCredentials: true
      },
      success: function loginSuccess(response) {
        if (response.redirectTo) {
          location.href = response.redirectTo;
          return;
        }

        if (response.status === 'ok') {
          location.href = baseUrl + '/dashboard';
          return;
        }

        hideLoading();
        showLoginError(response.errorText || '�Oh! algo fall�. Intenta nuevamente.');
      },
      error: function loginError() {
        hideLoading();
        showLoginError(loginBconf.messages.ACC_ERROR_LOGIN_INVALID);
      }
    });

    return false;
  }

  function changeEmail() {
    var $email = $('#facebook-email');
    var error = validateEmail($email);

    if (error) { return false; }

    showLoading(false, 'Facebook');
    fbConnect.changeEmail($email.val());
    return false;
  }

  function joinAccount() {
    var $password = $('#facebook-password');
    var error = validatePassword($password);

    if (error) { return false; }

    showLoading(false, 'Facebook');
    fbConnect.joinAccount($password.val());

    xt_click(this, 'C', '', 'log_in\:\:log_in\:\:log_in\:\:Facebook_confirm_password', 'A');

    return false;
  }

  function validateEmail($email) {
    var $emailWrapper = $email.parent();
    var $error = $emailWrapper.next();
    var email = $email.val();
    var error;

    $error.hide();
    $emailWrapper.removeClass('wrap-error');

    if (email) {
      if (!isEmail(email)) {
        error = loginBconf.messages.ERROR_EMAIL_INVALID;
      }
    } else {
      error = loginBconf.messages.ERROR_EMAIL_MISSING;
    }

    if (error) {
      $emailWrapper.addClass('wrap-error');
      $error.text(error).show();
    }

    return error;
  }

  function validatePassword($password) {
    var $passwordWrapper = $password.parent();
    var $error = $passwordWrapper.next();
    var password = $password.val();
    var error;

    $error.hide();
    $passwordWrapper.removeClass('wrap-error');

    if (password) {
      if (password.length > $password.attr('maxlength')) {
        error = loginBconf.messages.ERROR_PASSWORD_TOO_LONG;
      }
    } else {
      error = loginBconf.messages.ERROR_PASSWORD_MISSING;
    }

    if (error) {
      $passwordWrapper.addClass('wrap-error');
      $error.text(error).show();
    }

    return error;
  }

  function back(evt) {
    evt.preventDefault();

    $('#facebook-box').removeClass('__requirePassword');
    $('#facebook-box > div').toggleClass('is-hidden');
  }

  function showLoginError(error) {
    var $loginFeedback = $('.loginBox .loginFeedback:last');

    $('.mdTextField-wrapper').addClass('wrap-error');
    $loginFeedback.text(error);
    $loginFeedback.show();
  }

  function isEmail(email) {
    return /^[a-zA-Z0-9._-]+@[a-zA-Z0-9-]+\.[a-zA-Z.]{2,5}$/i.test(email);
  }

  function fbConnectBtnClick() {
    xt_click(this, 'C', '', 'log_in\:\:log_in\:\:log_in\:\:Facebook_login', 'A');
  }

  function googleConnectBtnClick () {
    showLoading(false, 'Google');
    
    if (typeof window.utag != 'undefined') {
      window.utag.link({ event_name: 'user_login_with_google_account' });
    }
  }

  function adsWithoutAccountClick() {
    $.mobile.changePage('#my_ads_page', { transition: 'slide' });
  }
})();
