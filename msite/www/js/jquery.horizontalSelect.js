/* global category_list */

(function($) {
  'use strict';

  var SELECTED_CLASS = 'selected';
  var DISABLED_CLASS = 'disabled';
  var DIRTY_CLASS = 'hs-wrapper-dirty';
  var TRANSITION = 'slide';

  $.horizontalSelect = {};


  /**
   *
   * @jsdoc method
   * @name horizontalSelect#updatePage
   * @description
   *
   * Description here.
   *
   * @param {String} id Select's id
   * @param {Boolean} hasChild Flag to add reverse direction
   *
   */
  $.horizontalSelect.updatePage = function(id, hasChild) {
    var ids = _getIds(id);
    var page = $('#' + ids.pageId);
    var trigger = $('#' + ids.triggerId);
    var select = $('#' + id);

    var items = _getItems.call(select);
    var html = createItems(items, id, hasChild).html();
    var ul = page.find('ul');

    ul.html(html);
    ul.listview().listview('refresh');

    trigger.find('.hs-trigger-link').text(select.attr('placeholder'));
  };


  /**
   *
   * @jsdoc method
   * @name horizontalSelect#changePage
   * @description
   *
   * Description here.
   *
   * @param {string} selectId Id of page's parent select
   * @param {Object} options Forward options to $.mobile.changePage's
   *
   */
  $.horizontalSelect.changePage = function(selectId, options) {
    var ids = _getIds(selectId);
    options = options || {};
    $.extend(options, {transition: TRANSITION});
    return $.mobile.changePage('#' + ids.pageId, options);
  };


  /**
   *
   * @jsdoc method
   * @name horizontalSelect#getNewLevel
   * @description
   *
   * Description here.
   *
   * @param {String} parentId Parent's id
   * @param {String} id Select's id
   * @param {String} title Select's placeholder and page's title
   * @param {Array} items Select's options and page's items
   * @param {Object=} options horizontalSelect configuration
   * @return {Object} A jQuery object
   *
   */
  $.horizontalSelect.getNewLevel = function(parentId, id, title, items, options) {
    var name = !options.multiple ? id : id + '[]';
    var select = $('<select/>', {
      id: id,
      name: name,
      placeholder: title,
      'data-parent': parentId,
      multiple: options.multiple
    });
    var firstItem = items[0];
    select.hide();

    if (firstItem && firstItem.value !== '') {
      items.unshift({
        value: '',
        text: title
      });
    }

    $.each(items, function(i, item) {
      var cat = category_list[parseInt(item.value)];
      var option = $('<option value="' + item.value + '">');

      if (cat && cat.priceRequired && cat.priceRequired === 1) {
        option = $('<option value="' + item.value + '" data-reqprice="true">');
      }

      if (item.isSelected) option.attr('selected', true);
      if (parentId == 'parent_category') option.attr({'data-description': item.description, 'data-icon': item.icon});
      option.text(item.text);
      select.append(option);
    });

    select.horizontalSelect(options);
    return select.parent();
  };


  /**
   *
   * @jsdoc method
   * @name horizontalSelect
   * @description
   *
   * Description here.
   *
   * @param {Object} config Configuration object
   * @return {object} Return `this`
   *
   */
  $.fn.horizontalSelect = function(config) {
    var selects = $(this);
    config = config || {};
    selects.each(function(i, element) {
      var select = $(element);
      var id = select.attr('id');
      var opts = {};
      var page;
      var trigger;

      $.extend(opts, config, {
        title: select.attr('placeholder') || select.attr('title'),
        selected: select.find(':selected').text(),
        items: _getItems.call(this)
      });
      opts.selected = opts.selected || opts.title;

      page = createPage(id, opts.title, opts.items, opts.multiLevel, opts.multiple);
      trigger = createTrigger(id, opts.selected, opts.title);

      if (!select.is(':visible')) trigger.hide();

      select.wrap('<div class="hs-wrapper"/>');
      select.hide();
      select.after(trigger);

      page.appendTo($.mobile.pageContainer);
      page.on('click', 'li > a', function onClickItem(event) {
        updateTrigger.call(this, event, select, opts);
      });

      select.on('change', function(event, extraData) {
        if (extraData && extraData.dontUpdateTrigger) return;
        var val = select.val() || '';
        if (select.is('[multiple]')) {
          // to start from scratch and avoid deleting the values
          select.val([]);
        }
        if (typeof(val) == 'string') {
          val = [val];
        }
        val.forEach(function(v) {
          var item = page.find('a[value="' + v + '"]');
          updateTrigger.call(item[0], event, select, opts, true);
        });
      });
    });

    $('body').trigger('updatelayout');

    return this;
  };


  /**
   *
   * @jsdoc function
   * @name horizontalSelect#updateTrigger
   * @description
   *
   * Update selected's value, trigger's text, and check config to proceed.
   *
   * @param {Object} event DOM's event
   * @param {Object} select jQuery object of select
   * @param {Object} config Configuration
   * @param {Boolean} preventTransition Boolean to prevent changePage transition
   * @return {Object} A jQuery object
   *
   */
  function updateTrigger(event, select, config, preventTransition) {
    var item = $(this);
    var items = item.closest('ul').find('a');
    var ids = _getIds(select.attr('id'));
    var trigger = $('#' + ids.triggerId);
    var triggerLink = trigger.find('.hs-trigger-link');
    var text = item.length ? (category_list[item.attr('value')]) ? category_list[item.attr('value')].name : item.text() : config.title;
    var wrapper = trigger.parent();
    var oldValue = select.data('oldValue');
    var newValue = item.attr('value');
    var maxSelection = config.maxSelection;

    if (!select.is('[multiple]')) {

      select.removeData('oldValue');
      select.data('oldValue', newValue);
      select.val(newValue);

      if (oldValue !== newValue) {
        triggerLink.text(text);
        items.removeClass(SELECTED_CLASS);
        item.addClass(SELECTED_CLASS);
        select.trigger('change', { dontUpdateTrigger: true });
        showHideCommunesSelect(trigger, newValue); 
      }
    } else {
      _handleSelectMultiple(select, item, items, trigger, maxSelection);
    }

    if (newValue) {
      wrapper.addClass(DIRTY_CLASS);
    } else {
      wrapper.removeClass(DIRTY_CLASS);
    }

    if (config.clean) clean(ids.id);
    if (preventTransition) return;
    if (config.nextLevel) return $.horizontalSelect.changePage(config.nextLevel);
    if (config.onSelect) return config.onSelect.call(this, event, select);
    return $.mobile.changePage('#', {transition: TRANSITION, reverse: true});
  }


  /**
   *
   * @jsdoc function
   * @name horizontalSelect#createPage
   * @description
   *
   * Create a page.
   *
   * @param {String} id Select's id
   * @param {String} title Page's title
   * @param {Array} items An array of items with this properties
   *                      * {Boolean} isSelected
   *                      * {String|Number} text
   *                      * {String|Number} value
   * @param {Boolean=} multiLevel Flag to set data-direction
   * @param {Boolean=} multiselect Flag to indicate that select accept multiple values
   * @return {Object} A jQuery object
   *
   */
  function createPage(id, title, items, multiLevel, multiselect) {
    var ids = _getIds(id);
    var page = $('<div/>', {
      id: ids.pageId,
      'data-role': 'page',
      class: 'hs-page',
      'data-select-id': ids.id
    });
    var href = $('body [data-role=page]:first-child').attr('data-url') || '/';
    var template = '<div data-role="header">'
      + '  <a data-transition="slide" data-rel="back"><i class="icon-yapo icon-arrow-left-bold"></i></a>'
      + '  <h1>' + title + '</h1>'
      + '  <a href="' + href + '" data-transition="slide"'
      + '    data-direction="reverse" class="close"><i class="icon-yapo icon-close"></i></a>'
      + '</div>'
      + '<div date-role="main" class="ui-content">'
      + '  <ul data-role="listview"></ul>'
      + '</div>';

    page.html(template.replace('{title}', title));
   
    if ((id == 'category' || id == 'parent_category') && items[1].description != '') {
     page.addClass('hs-page-description-data');
    }

    items = createItems(items, id, multiLevel, multiselect);
    page.find('ul').html(items.html());

    return page;
  }


  function createItems(items, parent_id, hasChild, multiselect) {
    var ul = $('<ul/>');

    $.each(items, function(i,item) {
      if (item.value === '') return true;
      var iconPre = $('<i/>', { class: 'icon-yapo icon-left-select'});
      var description = $('<p/>', { text: item.description, class: 'hs-page-text-description' });
      var name = $('<p/>', { class:"hs-page-text-name", text: item.text});
      var wrapperText = $('<div/>', { class: "hs-page-text-wrapper"});
      var li = $('<li/>', { class: 'hs-page-item-wrapper'});
      var a = $('<a/>', {
        id: parent_id + '-' + item.value,
        value: item.value,
        'data-transition': TRANSITION,
        'data-direction': !hasChild ? 'reverse' : false,
        class: item.isSelected ? SELECTED_CLASS : false
      });

      var check =  $('<div/>', { class: 'check' });
      var icon = $('<i/>', { class: 'icon-yapo icon-check' });
      check.append(icon);

      if ((parent_id == 'parent_category' || parent_id == 'category') && item.description) {
	wrapperText.append(name);
	wrapperText.append(description);
	a.append(wrapperText);
      } else {
	a.text(item.text);
      }
      
      if (item.icon) {
        iconPre.css('background-image', 'url(' + item.icon + ')');
        a.prepend(iconPre);
      }

      if (multiselect) {
        a.addClass('multiselect');
        a.append(check);
      }

      li.append(a);
      ul.append(li);
    });

    return ul;
  }


  /**
   *
   * @jsdoc function
   * @name horizontalSelect#clean
   * @description
   *
   * Delete a page by parent's id.
   *
   * @param {String} id Select's id
   *
   */
  function clean(id) {
    var childSelect = $('[data-parent=' + id + ']');
    var ids = _getIds(childSelect.attr('id'));
    if (childSelect.length) clean(ids.id);
    $('#' + ids.pageId + ', #' + ids.triggerId ).remove();
    if (childSelect.hasClass('mdTextField-input')) {
      return childSelect.closest('.new-ai-box').remove();
    }
    childSelect.parent().remove();
  }


  /**
   *
   * @jsdoc function
   * @name horizontalSelect#createTrigger
   * @description
   *
   * Create a link element to trigger the horizontalSelect's page.
   *
   * @param {String} id Select's id
   * @param {String} text Text to show in trigger link
   * @param {String} labelText Label to show after select an option
   * @return {Object} A jQuery object
   *
   */
  function createTrigger(id, text, labelText) {
    var ids = _getIds(id);
    var triggerWrap = $('#' + ids.triggerId);
    var label;
    var trigger;

    if (triggerWrap.length) return triggerWrap;

    triggerWrap = $('<div/>', { id: ids.triggerId, class: 'hs-trigger' });
    trigger = $('<a/>', {
      class: 'ui-btn ui-btn-icon-right ui-icon-carat-r hs-trigger-link',
      href: '#' + ids.pageId,
      'data-title' : text,
      'data-transition' : TRANSITION
    });
    trigger.html(text);
    
    if (ids.pageId == 'hsp-communes') {
      trigger.addClass('is-hide');
    }

    label = $('<label/>', { class: 'is-hide hs-trigger-label '});
    label.html(labelText || text);
    triggerWrap.append(label).append(trigger);

    return triggerWrap;
  }

  /**
   *
   * @jsdoc function
   * @name horizontalSelect#_handleSelectMultiple
   * @private true
   * @description
   *
   * Handles the select event when the select element is multiple
   *
   * @param {Object} select A jQuery object of the select
   * @param {Object} item A jQuery object of the selected item
   * @param {Array} items An array with the items
   * @param {Object} trigger A jQuery object of the trigger
   * @param {Number} maxSelection - Indicates the max quantity of selected sub categories
   *
   */
  function _handleSelectMultiple(select, item, items, trigger, maxSelection) {
    var newValue = item.attr('value');
    var values = select.val() || [];
    var indexOfNewValue = values.indexOf(newValue);
    var max = maxSelection || items.length;

    // if the value is new, added
    if (indexOfNewValue < 0) {
      values.push(newValue);
    } else {
      // if it was previously added, remove it please
      values.splice(indexOfNewValue, 1);
    }

    if (!item.hasClass(SELECTED_CLASS) && values.length > max) {
      return;
    }

    if (!item.hasClass(SELECTED_CLASS) && values.length >= max) {
      _disableOptions(items.toArray());
    } else {
      _enableOptions(items.toArray());
    }

    item.toggleClass(SELECTED_CLASS);
    item.removeClass(DISABLED_CLASS);
    select.val(values);

    // Updates trigger's text & UI
    var selectedOptions = [];
    items.filter('.' + SELECTED_CLASS).each(function() {
      selectedOptions.push('<p class="multi-op-item">' + $(this).text() + '</p>');
    });
    trigger.find('.hs-trigger-link').html(selectedOptions.join(''));
  }

  /**
   * @name horizontalSelect#_disableOptions
   * @description
   *
   * Add `disable` class to the enabled items
   *
   * @param {Array} items - DOM elements list
   */
  function _disableOptions(items) {
    items.forEach(function(item) {
      if (!item.classList.contains(SELECTED_CLASS)) {
        item.classList.add(DISABLED_CLASS);
      }
    });
  }

  /**
   * @name horizontalSelect#_enableOptions
   * @description
   *
   * Remove `disable` class to the disabled items
   *
   * @param {Array} items - DOM elements list
   */
  function _enableOptions(items) {
    items.forEach(function(item) {
      if (item.classList.contains(DISABLED_CLASS)) {
        item.classList.remove(DISABLED_CLASS);
      }
    });
  }

  /**
   *
   * @jsdoc function
   * @name horizontalSelect#_getIds
   * @private true
   * @description
   *
   * Create the `triggerId` and `pageId` by `Id`
   *
   * @param {String} id Id base
   * @return {Object} An object with 3 properties: id, triggerId and pageId
   *
   */
  function _getIds(id) {
    return {
      id: id,
      triggerId: 'hst-' + id,
      pageId: 'hsp-' + id
    };
  }


  /**
   *
   * @jsdoc function
   * @name horizontalSelect#_getItems
   * @private true
   * @description
   *
   * Description here.
   *
   * @return {Array} An array of item object with this properties:
   *                      * {Boolean} isSelected
   *                      * {String|Number} text
   *                      * {String|Number} value
   *
   */
  function _getItems() {
    return $('option', this).map(function() {
      var option = $(this);

      return {
        description: option.data('description'),
        icon: option.data('icon'),
        isSelected: option.is(':selected'),
        text: option.text(),
        value: option.val()
      };
    });
  }


  /**
   *
   * @jsdoc function
   * @name showHideCommunesSelect
   * @private true
   * @description
   * show select trigger when you choide region select
   *
   * @param trigger element
   * @param value new value selected
   *
   * @return void
   *
   */
  function showHideCommunesSelect(trigger, value) {
    var triggerLink = trigger.find('hs-trigger-link');
   
    if (trigger.is('#hst-region') && typeof value == 'undefined') {
      $('#hst-communes .hs-trigger-link').addClass('is-hide');
    } else if (trigger.is('#hst-region')) {
      $('#hst-communes .hs-trigger-link').removeClass('is-hide');
    } else if (trigger.is('#hst-communes')) {
      triggerLink.removeClass('is-hide');
    }
  }
})(jQuery);
