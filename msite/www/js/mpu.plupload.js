// http://www.plupload.com/
(function (window) {

  window.UploaderPU = UploaderPU;

  function UploaderPU() {
    this.uploader = new plupload.Uploader({
      browse_button: 'upload',
      url: '/mpu/upload/',
      file_data_name: 'image',
      max_files: 6,
      multipart: true,
      multipart_params: {
        json: '1',
      },
      multi_selection: true,
      resize: {
        width: 640,
        height: 640,
      },
      filters: {
        mime_types: [
          { title: 'Image files', extensions: 'jpeg,jpg,gif,png' },
        ],
        max_file_size: '50mb',
      },
      init: {
        FilesAdded: FilesAdded,
        UploadProgress: UploadProgress,
        FileUploaded: FileUploaded,
        FilesRemoved: FilesRemoved,
        Error: Error,
      },
    });
    this.uploader.init();
  }

  UploaderPU.prototype = {
    disableBrowse: function (disable) {
      this.uploader.disableBrowse(disable);
    },
    refresh: function () {
      this.uploader.refresh();
    },
    removeFile: function (fileId) {
      this.uploader.removeFile(fileId);
    },
    setMaxImages: function (max) {
      this.uploader.settings.max_files = max;
    },
    splice: function (max) {
      this.uploader.splice(max);
    },
  };

  function FilesAdded(pluploader, files) {
    // This method handles when new images are added to the upload queue
    // and based on the category, the images are accepted or are rejected
    // We could have used plupload File Filters to pre filter which images
    // are added to the queue, but, that method receives only one file, and
    // has no way to compute how many more files are being added together
    // so when multiple files are uploaded, this method (prefilter) is useless
    // That's why I kept this behavior, mainly
    var uploading = files.length;
    var filteredFiles = files;
    // If me pase
    var imagesCount = images.count();
    if (imagesCount + uploading > pluploader.settings.max_files) {
      var howManyMoreCanIAdd = pluploader.settings.max_files - imagesCount;
      filteredFiles = files.slice(0, howManyMoreCanIAdd);
      files.slice(howManyMoreCanIAdd, files.length).forEach(function (file) {
        pluploader.removeFile(file);
      });
    }
    filteredFiles.forEach(function (file) {
      images.appendImageLoaderFor(file.id);
    });

    images.render();
    images.scrollToEnd();
    this.start();
  }

  function UploadProgress(pluploader, file) {
    images.uploadProgress(file.id, file.percent);
  }

  function FileUploaded(pluploader, file, response) {
    response = JSON.parse(response.response);
    if (response.ok && response.image) {
      images.push(file.id, response.image);
      imageUploadedTag(file.size < file.origSize);
    } else {
      // need an error response? here's the place
      images.error(response.error_message);
    }

    images.render();
    pluploader.refresh();
  }

  function FilesRemoved(pluploader, files) {
    plupload.each(files, function (file) {
      images.removeImageLoaderFor(file.id);
    });
  }

  function Error(pluploader, error) {
    // Image is not uploaded, not even added to the queue
    // error.code == -600 && error.message == 'File size error.'
    // error.code == -702 && error.message == 'Resolution exceeds the allowed limit of 16777216 pixels.'
    var errorReason = null;
    if (error.code == -600 || error.code == -702) {
      // I18N
      errorReason = messages['ERROR_IMAGE_SIZE_BIG_OR_SMALL'];
    }
    images.error(errorReason);
  }


})(window);