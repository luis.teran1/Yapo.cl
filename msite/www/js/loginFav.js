$(function onReady () {
  var pathName = window.location.pathname;
  if (pathName === '/login' && window.localStorage) {
    var ls = window.localStorage;
    var optionFav = ls.getItem('loginFavOptions') || '{}';
    if (optionFav !== '{}') {
      var optionsObject = JSON.parse(optionFav);
      if (optionsObject.title) {
        $('.loginBox-header .loginBox-header-title').html(optionsObject.title);
      }
      if (optionsObject.subtitle) {
        $('.loginBox-header .loginBox-header-subtitle').html(optionsObject.subtitle);
      }
      ls.removeItem('loginFavOptions');
    }
  }
});
