// http://fineuploader.com/
function UploaderFU() {
  this.uploader = new qq.FineUploaderBasic({
    maxConnections: 1,
    button: document.getElementById('upload'),
    request: {
      inputName: 'image',
      endpoint: '/mpu/upload',
      forceMultipart: true,
      params: {
        json: '1'
      }
    },
    validation: {
      sizeLimit: 52428800, // 50mb
      allowedExtensions: ['jpeg', 'jpg', 'gif', 'png']
      // a 16MP image is usually 4920 x 3264, see http://design215.com/toolbox/megapixels.php
      // CHECK IF ENABLED: test what happens with images with weird orientation
      // disabled because is only supported in modern browsers
      // image: {maxHeight: 4920, maxWidth: 3264},
    },
    messages: {
      // I18N
      typeError: messages['AI_ERROR_IMAGE_FORMATS']
    },
    multiple: true,
    callbacks: {
      onSubmit: function(id, file) {
        if (images.count() >= images.max) {
          return false;
        }
        images.appendImageLoaderFor(id);
        images.render();
        images.scrollToEnd();
        return true;
      },

      onComplete: function(id, file, response) {
        if (response.ok && response.image) {
          images.push(id, response.image);
          imageUploadedTag();
        } else {
          images.removeImageLoaderFor(id);
        }
        images.render();
      },

      onProgress: function(id, name, uploadedBytes, totalBytes) {
        var percent = Math.floor((parseFloat(uploadedBytes)/parseFloat(totalBytes))*100);
        images.uploadProgress(id, percent);
      },

      onError: function(id, name, errorReason, xhrOrXdr) {
        if (xhrOrXdr) {
          errorReason = JSON.parse(xhrOrXdr.response).error_message;
        }

        images.error(errorReason);
      }
    }
  });
}

UploaderFU.prototype = {
  disableBrowse: function(){},
  refresh: function(){},
  removeFile: function(fileId) {
    this.uploader.cancel(fileId);
  },
  setMaxImages: function(max){},
  splice: function(max){}
};
