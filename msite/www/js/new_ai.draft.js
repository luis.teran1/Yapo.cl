/* globals isLogged, appl, storageSetItem, storageRemoveItem, storageGetItem, showDraft, xt_click, validator, updateCommunes */

var Draft = (function() {
  var _draftedNodes = {};
  var _available = null;
  var _draftStoreUri = 'yapoDraft.' + location.host + '/mai/form/';

  function available(value) {
    if (typeof(value) == 'undefined') {
      if (_available == null) {
        _available = (appl && appl == 'newad');
      }
    } else {
      _available = value;
    }
    return _available;
  }
  function setDraftFlag() {
    storageSetItem(getDraftStorageURI('.hasDraft'), 1);
    storageSetItem(getDraftStorageURI('.showDraft'), 1);
  }

  function removeDraftFlag() {
    storageRemoveItem(getDraftStorageURI('.hasDraft'));
    storageRemoveItem(getDraftStorageURI('.showDraft'));
  }

  function getDraftCodeName(node) {
    if (node.id) {
      return node.id;
    }

    return null;
  }

  function getValueFromStorage(node) {
    return storageGetItem(getDraftStorageURI(getDraftCodeName(node)));
  }

  function setValueToStorage(node) {
    if (node.getValue) {
      storageSetItem(getDraftStorageURI(getDraftCodeName(node)), node.getValue());
    }

    setDraftFlag();
  }

  function removeValueFromStorage(node) {
    storageRemoveItem(getDraftStorageURI(getDraftCodeName(node)));
  }

  function getDraftStorageURI(sufix) {
    return _draftStoreUri + sufix;
  }

  function reset(soft) {
    var index;

    /* soft means that we'll not remove the elements in the html,
     * only the stored value in session storage */
    soft = soft || false;
    for (index in _draftedNodes) {
      if (_draftedNodes[index].node.clear && !soft) {
        _draftedNodes[index].node.clear();
      }

      if (index != 'userInfo') {
        removeValueFromStorage(_draftedNodes[index].node);
      }
    }
    removeDraftFlag();
    showDraft();
    return xt_click(this, 'C', '10', 'Ad_insertion::Ad_insertion::Ad_insertion::Clean_form', 'A');
  }

  function draftMe(node) {
    // TODO: here I can check if all methods required exists and works...
    if (available()) {
      _draftedNodes[node.id] = { node: node };

      if (node.setValue) {
        node.setValue(getValueFromStorage(node));
      }
    }
  }

  function nodeOnChange(node) {
    if (available()) {
      setValueToStorage(node);
      showDraft();
    }
  }

  return {
    // each node should implement the methods clear, setValue, getValue, id
    draftMe: draftMe,
    nodeOnChange: nodeOnChange,
    reset: reset,
    available: available,
    getValue: getValueFromStorage,
  };

})();

var AdInfo = (function() {
  var selector = '#subject, #body, #price, #currency_type, #uf_price';
  var id = 'adInfo';

  function clear() {
    $(selector)
      .val('')
      .trigger('change')
      .trigger('blur');
  }

  function getValue() {
    var json = [];
    $(selector).each(function() {
      json.push({
        id: $(this).attr('id'),
        value: $(this).val()
      });
    });
    return JSON.stringify(json);
  }

  function setValue(value) {
    var nodes;
    var node;
    var index;

    if (value == null) return false;
    nodes = JSON.parse(value);

    for (index in nodes) {
      node = nodes[index];

      if (node.id === 'currency_type') {
        $('#' + node.id).val(node.value).trigger('change');
        $('#currency_' + node.value)
          .prop('checked', false)
          .trigger('click');
      } else {
        $('#' + node.id)
          .val(node.value)
          .trigger('change')
          .trigger('blur');
      }
    }
  }

  function initDraft() {
    Draft.draftMe(AdInfo);
    $(selector).on('change', function() {
      if ($(this).is(':visible')) {
        Draft.nodeOnChange(AdInfo);
      }
    });
  }

  return {
    clear: clear,
    getValue: getValue,
    setValue: setValue,
    selector: selector,
    initDraft: initDraft,
    id: id
  };
})();


var UserInfo = (function() {
  var id = 'userInfo';
  var userTypeSelector = '#user_type';
  var inputs = ['#name', '#email', '#rut', '#phone', '#area_code'];
  var switchSelector = ['#switch', '#phoneInputTypeHome', '#is_company_c'];
  var passwdSelector = '#passwd, #passwd_ver';

  function clear() {
    $(passwdSelector)
      .val('')
      .trigger('change')
      .trigger('blur');
  }

  function getValue() {
    var json = [];
    var i;
    // get the value for the user_type control
    json.push({ id: 'user_type', value: $(userTypeSelector).val() });
    // get the values for the inputs
    for (i in inputs) {
      json.push({
        id: inputs[i],
        value: $(inputs[i]).val()
      });
    }
    // get the values for the hide_phone switch
    for (i in switchSelector) {
      json.push({
        id: switchSelector[i],
        value: $(switchSelector[i]).is(':checked')
      });
    }
    return JSON.stringify(json);
  }

  function setValue(value) {
    var nodes;
    var index;
    var currentNodeId;
    var profileSelector;
    var radioSelectors = ['#switch', '#phoneInputTypeHome', '#is_company_c'];

    //dont fill the account info when is logged
    if ((typeof isLogged !== 'undefined' && isLogged == '1') || value == null) {
      return false;
    }

    nodes = JSON.parse(value);
    for (index in nodes) {
      currentNodeId = nodes[index].id;

      if (radioSelectors.indexOf(currentNodeId) !== -1) {
        if ($(currentNodeId).is(':checked') != nodes[index].value) {
          $(currentNodeId).click();
        }
      } else if (currentNodeId === 'user_type') {
        profileSelector = nodes[index].value === '0' ? '#is_company_p': '#is_company_c';
        $(profileSelector).trigger('click');
      } else {
        $(currentNodeId)
          .val(nodes[index].value)
          .trigger('blur');
      }
    }
  }

  function initDraft() {
    var selectors = switchSelector.concat(inputs, userTypeSelector);

    Draft.draftMe(UserInfo);

    selectors.forEach(function onEach(elementId) {
      $(elementId).on('change', function() {
        Draft.nodeOnChange(UserInfo);
      });
    });
  }

  return {
    clear: clear,
    getValue: getValue,
    setValue: setValue,
    initDraft: initDraft,
    id: id
  };
})();

var GeoPosition = (function() {
  var id = 'geoposition';
  var $geoposition = '#geoposition';
  var $region = '#region';
  var $commune = '#communes';

  function clear() {
    var triggerCommunes = $('#hst-communes .hs-trigger-link');
    var triggerRegion = $('#hst-region .hs-trigger-link');
    $('#geoposition').val('').trigger('change');
    $('#region').val('').trigger('change');
    $('#communes').val('').trigger('change');
    $('#geolocate_me').removeClass('-error -error-1 -success -loading');
    validator.resetForm();
    $('.wrap-error').removeClass('wrap-error');
    $('#hsp-communes .ui-listview').empty();
    triggerCommunes.html(triggerCommunes.data('title'));
    triggerRegion.html($('#region')[0].options[0].text);
  }

  function getValue() {
    var json = {
      geoposition: $geoposition.val(),
      region: $region.val(),
      communes: $commune.val()
    };

    return JSON.stringify(json);
  }

  function setValue(value) {
    var values = null;
    var region;

    if (value == null) {
      return false;
    }

    if (typeof(value) === 'string') {
      values = JSON.parse(value);
    } else {
      values = value;
    }

    if (values['geoposition'] && values['geoposition'].length) {
      $geoposition.val(values['geoposition']);
      $('#geolocate_me').addClass('-success');
    }

    region = values['region'];
    if (region != '0') {
      $region.val(region).trigger('change');
      updateCommunes(region);
      $commune.val(values['communes']).trigger('change');
    }
  }

  function init() {
    $geoposition = $($geoposition);
    $region = $($region);
    $commune = $($commune);
  }

  function initDraft() {
    init();
    Draft.draftMe(GeoPosition);
    $geoposition.on('change', function() { Draft.nodeOnChange(GeoPosition); });
    $commune.on('change', function() { Draft.nodeOnChange(GeoPosition); });
  }

  return {
    clear: clear,
    getValue: getValue,
    setValue: setValue,
    init: init,
    initDraft: initDraft,
    id: id
  };
})();
