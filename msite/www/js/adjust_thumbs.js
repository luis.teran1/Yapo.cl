//Adjust thumbnails on mobile site

function adjustThumbs(dpi, thumbPath, thumbPath2x){
	//Set thumbnail according to dpi
	$(".thumbnail:not(.adjusted)").each(function(){
		$(this).addClass("adjusted");
		$(this).hide();
		var src = $(this).data("src");
		if(dpi > 1){
			$(this).attr("src", thumbPath2x + src);
		}else{
			$(this).attr("src", thumbPath + src);
		}
		$(this).load( function(){
			var w = $(this).width();
			var h = $(this).height();
			if(dpi > 1){
				w = w/2 - 1;
				h = h/2 - 3;
				$(this).width(w);
				$(this).height(h);
			}
			if(h > 0){
				$(this).css("position", "relative");
				$(this).css("top", "50%");
				$(this).css("margin-top", "-" + h/2 + "px");
			}
			$(this).show();
		});
	})
}
