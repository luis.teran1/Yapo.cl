/* global AdParams, forcedLoginData, loginURL, isLogged, Draft, listId */
var Yapo = Yapo || {};

/**
 * @name Yapo.aiForcedLogin
 * @description
 *
 * This pseudo component force login for the configured categories.
 *
 * Related bconfs:
 *
 * *.*.forced_login.modal.newad
 * *.*.forced_login.modal.editad
 *
 */
Yapo.aiForcedLogin = (function() {

  /**
   * @name checkCategoryOnEnter
   * @description
   *
   * Cleans Ad insert form when page is loaded and the selected category is configured with
   * forced login.
   */
  function checkCategoryOnEnter() {
    var params = JSON.parse(Draft.getValue(AdParams)) || {};
    var category = params.category;

    $(document).on('pagechange', checkSelectedCategory);

    if (!shouldRedirect(forcedLoginData, category)) {
      return;
    }

    sessionStorage.clear();
    Draft.reset();
  }

  /**
   * @name checkSelectedCategory
   * @description
   *
   * Redirects to login page when horizontal select is closed.
   *
   */
  function checkSelectedCategory() {
    var category = $('#category').val();
    var $siteWrapper = $('.wrapper.new-ai');
    var $form = $('#newad_form');
    var isNavClosed = $siteWrapper.is(':visible');
    var queryString;
    var queryParams;

    if (shouldRedirect(forcedLoginData, category) && !$form.data('redirect')) {
      $form.attr('data-should-redirect', true);
    }

    if (!isNavClosed || !shouldRedirect(forcedLoginData, category)) {
      return;
    }

    queryParams = {
      list_id: listId || '',
      cat: category
    };

    queryString = Object.keys(queryParams)
      .filter(function(key) {
        return queryParams[key].length;
      }).map(function(key) {
        return key + '=' + queryParams[key];
      }).join('&');

    location.href = loginURL + '?' + queryString;
  }

  /**
   * @name shouldRedirect
   * @description
   *
   * Evaluates the conditions for redirect to the login page.
   *
   * @param {Object} loginData - Forced login configuration for the selected category.
   * @param {String} category - Selected category
   *
   * @returns {Boolean}
   */
  function shouldRedirect(loginData, category) {
    var hasConfig = loginData && loginData.hasOwnProperty(category);
    var isDisabled = hasConfig && parseInt(loginData[category].disabled, 10) === 1;

    return hasConfig && !isDisabled && isLogged === '0';
  }

  return checkCategoryOnEnter;
})();
