$(document).ready(function(){
  var dpi = window.devicePixelRatio;
  var thumbPath = $("#thumbnail-path").val();
  var thumbPath2x = $("#thumbnail-path-2x").val();

  adjustThumbs(dpi, thumbPath, thumbPath2x);

  // Move upselling_modal_label page as child of pageContainer
  $('#upselling_modal_label').remove().appendTo($.mobile.pageContainer).attr('data-role', 'page');

  // Header in modal page
  $('.pm-header a').click(function(e) {
    e.preventDefault();
    window.history.back();
  });
});
