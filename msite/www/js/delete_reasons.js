$(document).ready(function(){

	$("#time-slider").radiosToSlider({
		animation:false // look a little less sexy to improve perfomance on mobile website
	});

	HideFirstOptionFields();

	$("#time-slider").click( function(){
		ShowFirstOptionFields();
	});

	$(".ui-accordion-content").hide();

	$(".ui-accordion-header").click( function(){

		$(".ui-accordion-header-icon").removeClass("ui-icon-triangle-1-s");
		$(".ui-accordion-header-icon").addClass("ui-icon-triangle-1-e");

		$(".ui-accordion-content").not($(this).next()).hide();
		
		if($(this).next().is(":visible")){
			$(this).next().hide();
			$(this).find("span").removeClass("ui-icon-triangle-1-s");
			$(this).find("span").addClass("ui-icon-triangle-1-e");
		}else{
			$(this).next().show();
			$(this).find("span").removeClass("ui-icon-triangle-1-e");
			$(this).find("span").addClass("ui-icon-triangle-1-s");
		}

		var topPos = $(this).offset().top;
		$("body").scrollTop(topPos);

	});


	$(".badge-help").click(function(){
		$.pgwModal({
    		target: '#pay-info'
    	});
    	setHelpPopupEvents();
	});

	$("a[name=continue]").click( function(e){
		e.preventDefault();
		var reason = $(this).attr("data-reason");
		submitDeleteForm(reason);
	});

	$(".form-delete").submit( function(e){
		e.preventDefault();
		var reason = $(this).attr("data-reason");
		submitDeleteForm(reason);
	});

	$("#ok-help").click( function(){
		$.pgwModal('close');
	});

});

function submitDeleteForm(reason){
	var form = $('#form_' + reason);
	var formarray = form.serializeArray();
	var formdata = {};
	for (var i in formarray) {
		var v = formarray[i];
		var name = v['name'];
		var value = v['value'];
		formdata[name] = value;
	}
	$("p#error" + reason).hide();

	var execDelete = false;
	if(formdata.pass != undefined && formdata.pass.length > 0 ) execDelete = true;
	else if(formdata.passHash != undefined && formdata.passHash.length > 0) execDelete = true;
	else if(formdata.pass == undefined && formdata.passHash == undefined ) execDelete = true;

	if(execDelete){
		$.ajax({
			url: "/eliminar-aviso/delete_response",
			data : formdata,
			type: "POST",
			success :function(res){ 
				var result = $.parseJSON(res);
				//parse the result to see if ok
				if(result.status == "TRANS_OK"){
					var xtcustom = document.getElementById('mobDelConfirm' + reason).value;
					//send tealium tag
					addAttrReasonToTealiumInputAndSend('#tealium_delete_ad_submit', reason);
					xt_med('F','','foo&stc=' + xtcustom + '&' + xtcustom_var);
					$('input').blur();
					$('textarea').blur();

					var modalMsj = LANG.DEACTIVE_AD;
					var btnOptions = [{'text' : LANG.GO_TO_SEARCH_FROM_DELETE, 'url':'/chile'}];
					if(result.logged == '1' || result.pro == '1'){
						btnOptions.push({'text' : LANG.GO_MY_ACCOUNT, 'url':$('#acc_page_user_name').attr('href')});
						if(result.pro == '1') {
							modalMsj = LANG.DELETED_AD;
						}
					}

					Pop(modalMsj, btnOptions, function() {
						// the user just closed the popup
						document.location.href = '/chile';
					});
					
				}else{
					$("p#error" + reason).html(result.error);
					$("p#error" + reason).show();
					var xtcustom = document.getElementById('mobDelErrXiti').value;
					var xtcustom_var = get_page_custom_variables();
					xt_med('F','','foo&stc=' + xtcustom + '&' + xtcustom_var);
					//send tealium tag
					addAttrReasonToTealiumInputAndSend('#tealium_delete_ad_error', reason);
				}
			}
		});
		//hack to add history state
		document.location.href = document.location.href + "#deleted";
	}else{
		$("p#error" + reason).html(LANG.ERROR_PASSWORD_MISSING);
		$("p#error" + reason).show();
		//send tealium tag
		addAttrReasonToTealiumInputAndSend('#tealium_delete_ad_error', reason);
	}
}

function addAttrReasonToTealiumInputAndSend(selectorInput, reason){
	try{
		var reasonText = $("#ui-accordion-accordion-header-"+ reason).text().trim();
		$(selectorInput).attr('data-reason', reasonText);
		$(selectorInput).click();
	}catch(e){}
}

function showMessageFormSubmitted(){
	$(".pm-content #help-form").hide();
	$(".pm-content #help-ok-message").show();
	$(".pm-content #ok-help").click( function(){
		$.pgwModal('close');
	});

	$("input").blur();
	$("textarea").blur();
}

function HideFirstOptionFields(){
	$("label[for=pass1]").hide();
	$("#pass1").hide();
	$($("a.help")[0]).hide();
	$("a.btn[data-reason=1]").hide();
}

function ShowFirstOptionFields(){
	$("label[for=pass1]").show();
	$("#pass1").show();
	$(("a.help")[0]).show();
	$("a.btn[data-reason=1]").show();
}


function setHelpPopupEvents(){

	$(".pm-content .bottom-info a").click( function(){
		showCustomerSupportPopup($(this));
	});

	$(".popup-acc-body").hide();

	$(".popup-acc-head").click( function(){
		
		$(".popup-acc-body").not($(this).next()).hide();
		$(".popup-acc-head i").removeClass("caret-s");
		$(".popup-acc-head i").addClass("caret-e");
		
		if($(this).next().is(":visible")){
			$(this).next().hide();
			$(this).find("i").removeClass("caret-s");
			$(this).find("i").addClass("caret-e");
		}else{
			$(this).next().show();
			$(this).find("i").removeClass("caret-e");
			$(this).find("i").addClass("caret-s");
		}
	});
}

function setCustomerSupportPopupEvents(){
	$(".pm-content .help-form").submit( function(e){
		e.preventDefault();

		$("div.error").hide();
		//validate fields
		var errors = false;
		if($.trim($(".pm-content input[name=name]").val()).length == 0){
			$(".pm-content #error-name").show();
			errors = true;
		}
		if($.trim($(".pm-content input[name=email]").val()).length == 0){
			$(".pm-content  #error-email").show();
			errors = true;
		}
		if($.trim($(".pm-content textarea").val()).length == 0){
			$(".pm-content #error-body").show();
			errors = true;
		}
		

		if(!errors){
			$.ajax({
				url: $(this).attr('action'),
				data: 	{
					is_ajax: 1,
					name: $(".pm-content input[name=name]").val(),
					email: $(".pm-content input[name=email]").val(),
					support_subject: $(".pm-content input[name=support_subject]").val(),
					support_body: $(".pm-content textarea").val()
				},
				type: $(this).attr('method'),
				dataType: 'json',
				success: function(data, status, jqXHR){
					
					if(data.status == "OK"){
						showMessageFormSubmitted();
					}else{
						if (data.email != null) {
							$(".pm-content #error-email").html(data.email)
							$(".pm-content #error-email").show();
						}
						if (data.name != null){
							$(".pm-content #error-name").html(data.name)
							$(".pm-content #error-name").show();
						}
						if (data.support_body != null){
							$(".pm-content #error-body").html(data.support_body)
							$(".pm-content #error-body").show();
						}
					}
				},
				error: function (data, status) {
					showMessageFormSubmitted();
				}
			})
		}
	});
}

function showCustomerSupportPopup($clicked_tag){
	$.pgwModal({
		target: '#pay-help'
	});
	setCustomerSupportPopupEvents();
	$("#help-ok-message").hide();
	$("div.error").hide();

	if($clicked_tag.attr("data-xiti-page-xtn2") && clicked_tag.attr("data-xiti-page-tag")) {
    	xt_med('F', clicked_tag.attr("data-xiti-page-xtn2"), clicked_tag.attr("data-xiti-page-tag"));
    }
}
