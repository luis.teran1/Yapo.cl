// Global recommendedAPIConf

var recommendedAPI = (function () {
  function getAPI(adViewHost, api, id, queryLimit, queryParams, pro, carouselType) {
    if (pro) {
      return 'https://' + adViewHost + api + '/' + carouselType + '/' + id + queryLimit + queryParams;
    } else {
      return 'https://' + adViewHost + api + '/' + id + queryLimit + queryParams;
    }
  }

  function buildQueryParams(params) {
    return '&params=' + params;
  }

  function buildQueryLimit(limit) {
    return '?limit=' + limit;
  }

  function getHost() {
    return window.location.host;
  }

  function getAds(id, limit, params) {
    var api = getAPI(getHost(), recommendedAPIConf.path, id, buildQueryLimit(limit), buildQueryParams(params))
    return fetchAds(api);
  }

  function getProAds(id, limit, params, carouselType) {
    var api = getAPI(getHost(), recommendedAPIConf.pro, id, buildQueryLimit(limit), buildQueryParams(params), true, carouselType)
    return fetchAds(api);
  }

  function fetchAds(path) {
    return fetch(path)
      .then((res) => res.json())
      .then(function(data) {
        return data.ads;
      })
      .catch(function () {
        return [];
      })
  };
  return {
    getAds,
    getProAds
  }
})();