/* globals paymentSettings, upsellingProducts, upsellingProductsLabels, upsellingProductsCategories */

var Yapo = Yapo || {};

/**
 * Mobile Insert Upselling
 *
 * Sections (Find by S:Number)
 * 1 - UTILS
 * 2 - TAG/LABEL RELATED
 * 3 - BOXES
 * 4 - CATEGORY RELATED
 * 5 - CONTAINER
 **/

Yapo.aiUpselling = (function() {
  // Shitty globals
  var SELECTED_CLASS = '__selected';
  var DEACTIVATED_CLASS = '__deactivated';
  var BOXES = document.getElementsByClassName('aiUpselling-comboBox');

  /* S:0 External Dep */
  /**
   * This function depends on xt_click to work. Not sure what to do about that.
   **/
  function xitiClick(box) {
    if (box.className.indexOf('selected') > -1) {
      return false;
    }

    var cat;
    var tag;
    var subCat;
    var xitiTag = box.attributes.xititag;

    if (typeof xt_click === 'function' && xitiTag) { // eslint-disable-line
      cat = document.getElementById('parent_category').value;
      subCat = document.getElementById('category').value;

      tag = 'Ad_insertion::' + cat + '::' + subCat + '::' + xitiTag.value;

      xt_click(this, 'C', '', tag, 'A'); // eslint-disable-line
    }
  }


  /* S:1 UTILS */
  // Checks if an element doesn't have a class before adding it.
  function addClass(element, className) {
    if (element.className.indexOf(className) === -1) {
      element.className += ' ' + className; // eslint-disable-line
    }
  }

  // Checks if an element has a class before removing it.
  function removeClass(element, className) {
    var currentClass;
    if (!element) {
      return false;
    }

    currentClass = element.className;

    if (element.className.indexOf(className) > -1) {
      element.className = currentClass.replace(className, ''); // eslint-disable-line
    }

    return true;
  }

  /* S:2 TAG/LABEL RELATED */
  function generateLabels(box) {
    var key;
    var title;
    var labelsInfo;
    var currentLabel;
    var labelContainer = box.querySelector('.aiUpselling-labelContainer');

    if (upsellingProductsLabels) {
      labelsInfo = upsellingProductsLabels;
    } else {
      //console.error('If you are reading this, something went really wrong.');
      return false;
    }

    labelContainer.innerHTML = '';

    title = document.createElement('h2');
    title.className = 'aiUpselling-labelTitle';
    title.innerHTML = 'Elige tu Etiqueta:';

    labelContainer.appendChild(title);

    for (key in labelsInfo) { // eslint-disable-line
      currentLabel = createLabel(key, labelsInfo[key]);
      labelContainer.appendChild(currentLabel);
    }

    return true;
  }

  function createLabel(id, text) {
    var div = document.createElement('div');

    div.setAttribute('labelId', id);
    div.innerHTML = text;
    div.className = 'aiUpselling-label';

    return div;
  }

  function labelListener() {
    var cont = document.getElementsByClassName('aiUpselling')[0];

    cont.addEventListener('click', validateLabel, false);
  }

  function validateLabel(e) {
    var attr;

    if (e.target && e.target.className.indexOf('aiUpselling-label') > -1) {
      attr = e.target.attributes.labelId;

      if (attr) {
        chooseLabel(e.target, attr.value);
      } else {
        return false;
      }
    }

    return true;
  }

  function chooseLabel(elem, labelId) {
    clearLabels();

    addClass(elem, SELECTED_CLASS);

    updateLabelInfo(labelId);
  }

  function clearLabels() {
    var labels = document.getElementsByClassName('aiUpselling-label');

    [].forEach.call(labels, function(elem) {
      removeClass(elem, ' ' + SELECTED_CLASS);
    });
  }

  function updateLabelInfo(id) {
    document.getElementById('upselling_product_label').value = id;
  }

  /* S:3 BOX RELATED */
  // Add classes to chosen box and update upselling_product id.
  function chooseBox(box) {
    var title;
    var radio;
    var productId;
    var labelContainer;
    var containerNotHidden;

    var labelId = false;

    var selectedLabel = box.querySelector('.aiUpselling-label.__selected');

    if (!box || isBoxEnabled(box)) {
      return false;
    }

    xitiClick(box);

    radio = box.querySelector('.aiUpselling-radio');
    title = box.querySelector('.aiUpselling-title');
    labelContainer = box.querySelector('.aiUpselling-labelContainer');

    clearBoxes();

    if (!selectedLabel) {
      clearLabels();
    }


    addClass(box, SELECTED_CLASS);

    if (radio) {
      addClass(radio, SELECTED_CLASS);
    }

    if (title) {
      addClass(title, SELECTED_CLASS);
    }

    if (box.attributes.productId) {
      productId = box.attributes.productId.value;
    }

    if (box.className.indexOf('hasLabels') > -1) {
      removeClass(box.querySelector('.aiUpselling-labelContainer'), ' __hide');
    }

    labelContainer = box.querySelector('.aiUpselling-labelContainer');
    containerNotHidden = labelContainer ? labelContainer.className.indexOf('hide') : false;

    if (!selectedLabel && labelContainer && containerNotHidden === -1) {
      addClass(box.querySelector('.aiUpselling-label'), ' ' + SELECTED_CLASS);
      labelId = '1';
    }

    setInputInformation(productId, labelId);

    return true;
  }

  function setBasicBox() {
    var basic = document.getElementsByClassName('__basic')[0];
    chooseBox(basic);
  }

  // Set product id and label id.
  function setInputInformation(productId, labelId) {
    var upsellingProduct = document.getElementById('upselling_product');
    var upsellingProductLabel = document.getElementById('upselling_product_label');

    if (!productId || productId == 'noproduct') {
      upsellingProduct.removeAttribute('value');
    } else {
      upsellingProduct.value = productId;
    }

    if (labelId) {
      upsellingProductLabel.value = labelId;
    } else {
      upsellingProductLabel.removeAttribute('value');
    }
  }

  // Check if current box is enabled.
  function isBoxEnabled(box) {
    return box.className.indexOf(DEACTIVATED_CLASS) > -1;
  }

  function enableBoxes() {
    var deactivated;

    [].forEach.call(BOXES, function(box) {
      removeClass(box, DEACTIVATED_CLASS);
    });

    deactivated = document.querySelectorAll('.__deactivated');
    if (deactivated.length === 0) {
      hideImagesWarning();
    }
  }

  function disableBoxes() {
    var deactivated;
    var boxesWithGallery = document.getElementsByClassName('hasGallery');

    [].forEach.call(boxesWithGallery, function(box) {
      if (box.className.indexOf('selected') > -1) {
        setBasicBox();
      }

      addClass(box, DEACTIVATED_CLASS);
    });

    deactivated = document.querySelectorAll('.__deactivated');
    if (deactivated.length > 0) {
      showImagesWarning();
    }
  }

  function hideImagesWarning() {
    var infoBox = document.getElementsByClassName('aiUpselling-infoBox')[0];
    addClass(infoBox, '__hide');
  }

  function showImagesWarning() {
    var infoBox = document.getElementsByClassName('aiUpselling-infoBox')[0];
    removeClass(infoBox, '__hide');
  }

  // Clean all boxes.
  function clearBoxes() {
    var labelContainer;

    [].forEach.call(BOXES, function(box) {
      removeClass(box, ' ' + SELECTED_CLASS);
      removeClass(box.querySelector('.aiUpselling-radio'), ' ' + SELECTED_CLASS);
      removeClass(box.querySelector('.aiUpselling-title'), ' ' + SELECTED_CLASS);

      labelContainer = box.querySelector('.aiUpselling-labelContainer');
      if (labelContainer) {
        addClass(labelContainer, '__hide');
      }
    });
  }

  // Update upselling information.
  function updateUpselling(category, isProFor) {
    var currentContent;

    clearBoxes();
    currentContent = Yapo.aiUpsellingProducts.getUpsellingCombos(
        paymentSettings,
        upsellingProducts,
        upsellingProductsCategories,
        category,
        isProFor
    );
    updateInfo(currentContent);
  }

  // Update boxes with chosen upselling information.
  function updateInfo(info) {
    var current;
    var types = ['basic', 'standard', 'advanced', 'premium'];

    [].forEach.call(types, function(elem) {
      current = document.getElementsByClassName('__' + elem)[0];
      updateBox(current, info[elem]);
    });
  }

  /**
   * Update box contents.
   *
   * Updates based on class (which are the ones contained in the sections variable).
   *
   **/
  function updateBox(elem, info) {
    var i;
    var key;
    var text;
    var what;
    var target;
    var current;

    var sections = [
      'price',
      {
        content: 'body_short'
      },
      'title',
      'subtitle'
    ];

    if (!info) {
      return;
    }

    elem.setAttribute('productId', info.product_id); // eslint-disable-line

    info.subtitle = info.subtitle.replace('|', ' '); // eslint-disable-line

    for (i = 0; i < sections.length; i++) {
      if (typeof sections[i] === 'object') {
        key = Object.keys(sections[i])[0];
        current = key;
        text = sections[i][key];
      } else {
        current = text = sections[i];
      }

      what = '.aiUpselling-' + current;
      target = elem.querySelector(what);
      if (target) {
        target.innerHTML = info[text]; // eslint-disable-line
      }
    }

    if (info.hasGallery) {
      addClass(elem, 'hasGallery');
    } else if (elem.className.indexOf('hasGallery') > -1) {
      removeClass(elem, 'hasGallery');
      removeClass(elem, '__deactivated');
    }

    if (info.hasLabels) {
      generateLabels(elem);
      addClass(elem, 'hasLabels');
    } else if (elem.className.indexOf('hasLabels') > -1) {
      removeClass(elem, 'hasLabels');
    }

    if (info.xitiTagName) {
      elem.setAttribute('xitiTag', info.xitiTagName);
    }
  }

  // Add listener to each box.
  function boxListener() {
    [].forEach.call(BOXES, function(box) {
      box.addEventListener('click', function() {
        chooseBox(this);
      }, false);
    });
  }

  /* S:4 CATEGORY RELATED */
  /**
   * Set a listener on the subcategory select, which initializes upselling with the
   * current category or hides upselling.
   **/
  function categoryListener() {
    // @TODO: Remove jQuery
    $('#cat_info').on('change', function(e) {
      if (e.target.id === 'parent_category' && !e.target.value) {
        hideUpselling();
      } else if (e.target.id === 'category') {
        setTimeout(function() {
          initUpselling(e.target.value);
        }, 500); // @TODO: Fix this shitty timeout (set because of Async images)
      }
    });
  }

  function imageUploaderListener() {
    var imagenlist = document.querySelector('[type=file]');

    if (imagenlist) {
      imagenlist.addEventListener('change', function(e) {
        enableBoxes();
      });
    }
  }

  function imageDeleteListener() {
    document.getElementById('da_images').addEventListener('click', function(e) {
      if (e.target.className.indexOf('icon-close-circled') > -1) {
        if (countImages() === 0) {
          disableBoxes();
        }
      }
    });
  }

  // Returns number of images loaded
  function countImages() {
    var images = document.getElementsByClassName('image');
    return images.length;
  }

  function detectImages() {
    if (countImages() === 0) {
      disableBoxes();
    } else {
      enableBoxes();
    }
  }

  // Call everything needed to initialize upselling.
  function initUpselling(id) {
    var isProFor = $('.account-info').data('isprofor') || '';
    updateUpselling(id, isProFor);

    detectImages();

    imageUploaderListener();
    imageDeleteListener();

    labelListener();

    setBasicBox();
    showUpselling();
  }

  /* S:5 CONTAINER */
  function hideUpselling() {
    // @TODO: Remove jQuery.
    $('#upselling').hide(500);
    updateNumber(4);
    setBasicBox();
  }

  function showUpselling() {
    // @TODO: Remove jQuery.
    $('#upselling').show(500);
    updateNumber(5);
  }

  // Update category number.
  function updateNumber(number) {
    document.getElementById('yourInfoNumber').innerHTML = number;
  }

  // Check if theres available data to show combos.
  function isThereAnyData(callback) {
    if (upsellingProducts) {
      callback();
    } else {
      return false;
    }

    return true;
  }

  return function init() {
    boxListener();
    isThereAnyData(categoryListener);
  };
})();

