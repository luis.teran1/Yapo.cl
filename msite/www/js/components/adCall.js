var adCall = (function() {
  function adCallRecommendedTagging(eventName, adId, originButton) {
    var utagData = utag_data !== undefined ? Object.assign({}, utag_data) : {};
    utagData.event_name = eventName;
    utagData.data = {
      ad_id: adId,
      origin_page: 'Ad View',
      root: 'Post Call',
      with_button: originButton ? 'yes' : 'no',
    }
    if (typeof window.utag !== 'undefined') {
      utag.link(utagData);
    } else {
      console.log(utagData);
    }
  }
  function adCallLeadTagging(eventName, interaction, ad) {
    var types = {
      s: 'sell',
      k: 'buy',
      u: 'rent'
    };
    var utagData = utag_data !== undefined ? Object.assign({}, utag_data) : {};
    utagData.event_name = eventName;
    utagData.data = {
      interaction: interaction,
      origin_page: 'Ad View',
      root: 'Post Call',
      ad_id: ad.id,
      category: ad.categoryDescription,
      region: ad.regionDescription,
      ad_title: ad.title,
      ad_type: types[ad.type] || 'sell',
      publisherType: ad.publisherType
    }
    if (typeof window.utag !== 'undefined') {
      utag.link(utagData);
    } else {
      console.log(utagData);
    }
  }

  function createAdCallSimilarCardItem (ad) {
    var cardItem = document.createElement('card-item');
    cardItem.classList.add('modal-recommended__card-item')
    cardItem.setAttribute('text-button', 'Enviar mensaje');
    cardItem.setAttribute('title', ad.title);
    cardItem.setAttribute('url', ad.url);
    if (ad.price && ad.price > 0) {
      cardItem.setAttribute('price', formatStrPrice(ad.price, ad.currency));
    }
    cardItem.setAttribute('adid', ad.id);
    cardItem.setAttribute('theme', 'accent');
    if (ad.images && ad.images.full != undefined) {
      cardItem.setAttribute('image', ad.images.full);
    } else {
      cardItem.setAttribute('image', recommendedAdcallConf.defaultImage)
    }

    if (ad.phonelink && ad.phonelink != '') {
      cardItem.setAttribute('text-button', 'Llamar');
      cardItem.setAttribute('icon', 'far fa-phone fa-flip-horizontal');
      cardItem.addEventListener('click-button', function cardItemEvent(event) {
        var phoneLink = ad.phonelink;
        adCallLeadTagging('similar_ads_interaction_button', 'Call', ad);
        setTimeout(function () {
          location.assign(phoneLink);
        }, 800);
      })
    } else {
      cardItem.setAttribute('text-button', 'Ver aviso');
      cardItem.addEventListener('click-button', function cardItemEvent(event) {
        var adid = event.detail.adid;
        var url = event.detail.url;
        adCallLeadTagging('similar_ads_interaction_button', 'AdView', ad);
        setTimeout(function () {
          location.assign(url);
        }, 800);
      })
    }
    return cardItem;
  }
 
  function showRecommendedAdCall(data) {
    var id = data;
    recommendedAPI.getProAds(id, recommendedAdcallConf.limit || 6, 'phonelink,publisherType,type,category,region', 'default').then(function processData(ads) {
      if (ads && ads.length > 0) {
        var modal = yapoModal.initialize({
          id: 'post-adreply-recommended',
          conf: {
            attrs: {
              title: recommendedAdcallConf.title || '',
              position: '95',
              overlayClose: 'true',
            },
          },
        });
        var button = createSimilarButton();
        button.addEventListener('click', function showModal() {
          adCallRecommendedTagging('similar_ads_bubble_button');
          yapoModal.showModal(modal)
        });
        document.querySelector('body').appendChild(button);

        var content = document.createElement('div');
        content.classList.add('modal-recommended')
        ads.forEach((ad) => {
          var cardItem = createAdCallSimilarCardItem(ad);
          cardItem.addEventListener('click-ad', function redirectCard(event) {
            var url = event.detail.url;
            var adid = event.detail.adid;
            adCallRecommendedTagging('similar_ads_go_to_recommended_ad', adid);
            setTimeout(function () {
              location.assign(url);
            }, 800);
          });
          content.appendChild(cardItem);
        });
        modal.addEventListener('closed', function closeModal(event) {
          var origin = event.detail.origin;
          adCallRecommendedTagging('similar_ads_close', undefined, origin === 'close-button')
        });
        modal.appendChild(content);
        adCallRecommendedTagging('similar_ads_shown');
      }
    });
  }

  return { showRecommendedAdCall }
})();