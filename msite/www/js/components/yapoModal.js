var yapoModal = (function () {
  var EVENT_OPEN = 'open';
  var EVENT_CLOSE = 'close';
  function createModal(id) {
    if (document.querySelector('#' + id)) {
      document.querySelector('#' + id).remove();
    }
    var modal = document.createElement('yapo-modal');
    modal.setAttribute('id', id);
    return modal;
  }

  function setModalConf(modal, conf) {
    var attrs = conf.attrs || {};
    modal.setAttribute('title', attrs.title || '');
    modal.setAttribute('theme', attrs.theme || 'accent');
    modal.setAttribute('position', attrs.position || '100');
    modal.setAttribute('type', attrs.type || 'rounded-top');
    modal.setAttribute('initial-state', attrs.initalState || 'shown');
    modal.setAttribute('overlay-close', attrs.overlayClose || 'false');
    return modal
  }

  function showModal(modal) {
    modal.dispatchEvent(new CustomEvent(EVENT_OPEN))
    return modal;
  }

  function closeModal(modal) {
    modal.dispatchEvent(new CustomEvent(EVENT_CLOSE))
    return modal;
  }

  function append(modal) {
    document.querySelector('body').appendChild(modal);
    return modal
  }

  function initialize(data) {
    var id = data.id;
    var modal = createModal(id);
    setModalConf(modal, data.conf || {});
    append(modal);
    return modal
  }
  return {
    initialize,
    closeModal,
    showModal,
  }
})();
