var quotaFinance = (function (yapoModal) {

  function graph (urlYapoQL, adId) {
    const baseUrl = window.location.origin + urlYapoQL;
    const client = new YapoQL.default({ url: baseUrl });
    //consulta graphQL
    const queryFunc = ` query getSimulationByAdId($adId: String) {
      getSimulationByAdId (adId: $adId) {
        formattedPrice
        formattedDownPaymentValue
        formattedLoanAmount
        installments
        formattedTax
        formattedMonthlyFee
        formattedOperationalCosts
        formattedCae
      }
    }`;
    const loanData = client.query({query: queryFunc, variables: {adId}});
    return loanData;
  }

  function fillData(data) {
    var modal = data.modal;
    var dataFinance = data.data || {};
    var quotaFinance = document.createElement('quota-finance');
    quotaFinance.setAttribute('title', quotaFinanceData.title);
    quotaFinance.setAttribute('value', dataFinance.formattedPrice);
    quotaFinance.setAttribute('initial-footer', dataFinance.formattedDownPaymentValue);
    quotaFinance.setAttribute('request-amount', dataFinance.formattedLoanAmount);
    quotaFinance.setAttribute('number-quotas', dataFinance.installments);
    quotaFinance.setAttribute('interest', dataFinance.formattedTax);
    quotaFinance.setAttribute('quota-value', dataFinance.formattedMonthlyFee);
    quotaFinance.setAttribute('operational-expenses', dataFinance.formattedOperationalCosts);
    quotaFinance.setAttribute('cae', dataFinance.formattedCae);
    quotaFinance.setAttribute('note', quotaFinanceData.note);
    modal.appendChild(quotaFinance);
    return quotaFinance;
  }

  function initialize(data) {
    var ad = data.ad || {};
    const device = ad.device;
    //datos de la consulta graphQL - opcion
    graph(ad.urlYapoQL, ad.adId).then( function(responseGraphQL) {

      var dataGraphQL = responseGraphQL.response.getSimulationByAdId;

      if (dataGraphQL !== null) {
        document.querySelector('#da-detail').setAttribute('product-cuota', dataGraphQL.formattedMonthlyFee + ' al mes');
        var type = 'large';

        if (device === 'desktop') {
          document.getElementById('btn-da-detail').style.display = 'block';
          type = 'small';
        }

        var modal = yapoModal.initialize({
          id: 'financeModal',
          conf: {
            attrs: {
              title: ad.title,
              theme: 'accent',
              type: type,
              initalState: 'hidden',
              overlayClose: 'false'
            },
          },
        });

        //tagging cuando is displayed btn quiero financiamiento
        tagging('ad_financing_button_display');

        dataGraphQL.title = ad.title;
        var quota = fillData({modal, data: dataGraphQL});
        listener(modal);
        quotaClick({quota, modal, data: dataGraphQL, device});
      } else {
        if (device === 'desktop') {
          document.getElementById('btn-da-detail').style.display = 'none';
        }
      }
    });
  }

  function adreplyMessage(data) {
    return quotaFinanceData.helloMessage + '\n\n' +
    quotaFinanceData.descpMessage + '\n\n' +
    quotaFinanceData.nameCarMessage + data.title + '\n' +
    quotaFinanceData.priceMessage + data.formattedPrice  + '\n' +
    quotaFinanceData.downPaymentValueMessage + data.formattedDownPaymentValue  + '\n' +
    quotaFinanceData.loanAmountMessage + data.formattedLoanAmount  + '\n' +
    quotaFinanceData.installmentsMessage + data.installments  + '\n' +
    quotaFinanceData.taxMessage + data.formattedTax  + '\n' +
    quotaFinanceData.monthlyFeeMessage + data.formattedMonthlyFee  + '\n' +
    quotaFinanceData.operationalCostsMessage + data.formattedOperationalCosts  + '\n' +
    quotaFinanceData.caeMessage + data.formattedCae + '\n';
  }

  function quotaClick(data) {
    var quota = data.quota;
    var modal = data.modal;
    var info = data.data;
    var device = data.device;

    quota.addEventListener('simulationClick', function openAdreply() {
      modal.dispatchEvent(new CustomEvent('close'));

      if(device === 'mobile') {
        document.querySelector('.addetail-stickybar__link-chat').click();
      }

      //tag cuando se hace click a enviar mensaje al vendedor
      tagging('ad_financing_send_message');

      document.querySelector('#adreply_body').value = adreplyMessage(info);
      if(device === 'mobile') {
        document.querySelector('[for=adreply_body]').classList.add('mdTextField-input-dirty');
      }else if (device === 'desktop') {
        document.getElementById('adreply_body').focus();
        document.getElementById('your_name').focus();
      }

      function tagSend() {
        document.removeEventListener('adreply::ok', tagSend);
        //tag cuando se hace click a enviar
        tagging('ad_financing_adreply_submitted');
      }
      document.addEventListener('adreply::ok', tagSend);
    });
  }

  function listener(modal) {
    //desplegar simulador de cuotas - financiamiento auto
    document.addEventListener('btn-finance-clicked', function shownModal() {
      //tag cuando se hace click a btn financiamiento
      tagging('ad_financing_go_to_financing_info');
      modal.dispatchEvent(new CustomEvent('open'));
    });
  }

  function tagging(eventName) {
    var extraData = { event_name: eventName};
    var utagData = (typeof utag_data !== 'undefined') ? Object.assign(utag_data, extraData) : extraData;

    if (typeof window.utag !== 'undefined') {
      utag.link(utagData);
    }else{
      console.log(utagData);
    }
  }

  return {
    initialize,
  }
}
)(yapoModal);
