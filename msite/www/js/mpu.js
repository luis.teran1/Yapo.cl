/* globals messages, Draft, clean, images, UploaderPU, UploaderFU, imageUploadedTag, xt_med  */

var DEFAULT_MAX_IMAGES = 6;

function Images() {
  this.uploadButton = $('#upload');
  this.counter = $('#upload .counter');
  this.container = $('#load_images');
  this.scrollContainer = $('#da_images');
  this.errors = $('#images_errors');
  var thisImages = this;
  this.currentWidth = $('body').width();
  this.scrollContainer.css('width', this.currentWidth + 'px');

  // Handle image deletion and cleanup
  this.container.on('click', '.remove', function(e) {
    var image = $(e.currentTarget).parents('div.image, div.loading');
    thisImages.remove(image);
  });

  this.setupScroll();
  this.setupOrientationChange();
}

Images.prototype = {
  id: 'images',
  max: DEFAULT_MAX_IMAGES,

  defaultWidth: 110,

  // for requests that need uploader to be set first
  requests: [],

  setUploader: function(u) {
    this.uploader = u;
    // when someone calls images.setMaxImages, and we dont have the uploader yet
    // we queue the request, and execute it here
    this.requests.forEach(function(fn) {
      fn();
    });
  },

  getElementsWidth: function ($element) {
    // Used to compute the width of the container (used for horizontal scrolling)

    var width = 0;

    if ($element.size() > 0) {
      ['margin-right', 'margin-left'].forEach(function(property) {
        var propertyValue = $element.css(property);
        width += parseInt(propertyValue.substring(0, propertyValue.length - 2));
      });
      return width + $element.width();
    }

    return width;
  },

  // Renders the images
  render: function() {
    var uploader = this.uploader;

    // count if images are more than
    var count = this.count();

    // initial big button checking
    if (count == 0) {
      this.uploadButton.addClass('initial');
    } else {
      this.uploadButton.removeClass('initial');
    }

    this.defaultWidth = this.getElementsWidth($('.image, .loading'));

    if (count >= this.max) {

      // disable the upload and hide the button
      uploader.disableBrowse(true);
      this.uploadButton.hide();
      // if we have more images, lets hide them
      this.container.find('div.image, div.loading').slice(this.max).hide();
      this.container.width((this.max * this.defaultWidth) + 'px');
      // I18N
      this.error(messages['AI_WARNINGS.MAX_IMAGES_OVERFLOW']);

    } else {

      // enable the upload and show the button
      uploader.disableBrowse(false);
      this.uploadButton.show();

      // update the counter
      var remaining = count + '/' + this.max;
      this.counter.html(remaining);

      // setting the max for horizontal scroll
      if (count != 0) {
        this.container.width(((count + 1) * this.defaultWidth) + 'px');
      } else {
        this.container.width('100%');
      }

      // If error is max image reached, we remove it now
      if (this.errors.text() == messages['AI_WARNINGS.MAX_IMAGES_OVERFLOW']) {
        this.error('');
      }

    }
    uploader.refresh();
  },

  count: function() {

    // count the loaded and loading images
    return this.container.find('div.image, div.loading').length;
  },

  isLoading: function() {
    return $('#da_images .loading-background').length > 0;
  },

  remove: function(fileNode) {

    // If we have hidden images, clean those first
    if (this.count() > this.max) {
      this.uploader.splice(this.max);
      $('.image').slice(this.max).remove();
    }

    // Remove from the internal array and the DOM
    var fileId = $(fileNode).attr('id');
    if (fileId)
      this.uploader.removeFile(fileId);

    $(fileNode).remove();

    this.render();
    Draft.nodeOnChange(images);
  },

  uploadProgress: function(fileId, percent) {
    var loading = this.container.find('#' + fileId + ' .loading-background');
    loading.css('width', percent + '%');
  },

  appendImageLoaderFor: function(fileId) {
    /* A new image is added to be uploaded, lets display a loader */
    var html = '<div id="' + fileId + '" class="loading"><span class="remove"><i class="icon-yapo icon-close-circled"></i><div class="bg-white"></div></span><i class="icon-yapo icon-camera"></i><img src="/img/m_loader_bg.gif" /><div class="loading-background"></div></div>';
    // clear the errors
    this.error('');
    this.uploadButton.before(html);
  },

  removeImageLoaderFor: function(fileId) {
    this.container.find('#' + fileId).remove();
  },

  push: function(fileId, img) {
    var container = this.container;
    container.find('#' + fileId).html('<span class="remove"><i class="icon-yapo icon-close-circled"></i><div class="bg-white"></div></span><i class="icon-yapo icon-camera"></i><img src="/img/m_loader_bg.gif" /></div>');
    var url = this.container.data('image_host') + '/thumbsai/' + img.substring(0, 2) + '/' + img;
    var image = document.createElement('img');
    image.setAttribute('src', url);
    image.className = 'is-hidden';
    image.onload = function() {
      var html = '<div class="image" style="background-image:url(' + url + ');" data-image="' + img + '">';
      html += '<span class="remove"><i class="icon-yapo icon-close-circled"></i><div class="bg-white"></div></span>';
      html += '</div>';
      container.find('#' + fileId).replaceWith(html);
      Draft.nodeOnChange(images);
      images.sendEvent();
    };
    // Actually downloading the img
    $(image);
  },
  
  sendEvent: function () {
    var addImages = new CustomEvent('addImageAdInsert', { detail: { count: images.count() }});
    this.container.get(0).dispatchEvent(addImages);
  },

  scrollToEnd: function() {
    this.scrollContainer.scrollLeft((this.count() + 1) * this.defaultWidth);
  },

  setMaxImages: function (maxImages) {
    var uploader = this.uploader;

    if (uploader == undefined) {
      this.requests.push(function() {
        images.setMaxImages(maxImages);
      });
      return;
    }

    // the more images message logic
    var $moreImages = $('#more_images_yeah');
    if (maxImages > this.max) {
      $moreImages.show();
      setTimeout(function(){$moreImages.hide();}, 5000);
      $moreImages.one('click', '.icon-close', function() {
        $moreImages.hide();
      });
    } else {
      // just in case the user has not closed the message
      $moreImages.hide();
    }

    this.max = maxImages;
    uploader.setMaxImages(maxImages);
    var count = this.count();
    if (count <= maxImages) {
      this.container.find('div.image, div.loading').show();
    } else {
      this.container.find('div.image, div.loading').slice(maxImages).hide();
    }

    this.render();
    this.scrollToEnd();
  },

  clean: function() {
    // for some reasons you want to remove all your images
    this.container.find('div.image, div.loading').remove();
    this.setMaxImages(DEFAULT_MAX_IMAGES);
    this.render();
  },

  error: function (message) {
    if (message != '') {
      this.errors.html(message).show();
      this.container.find('div.loading img').hide();
    } else {
      this.errors.empty().hide();
    }
  },


  clear: function(){ this.clean(); },

  setupScroll: function() {
    /* The function setupScroll setups the horizontal scroll for older versions of native android browser */

    // This code is similar to the code that we have in isAndroidBrowser function,
    // but I couldnt think in a better way to have this shared code, than creating a
    // browser detection lib, but I am not going to do that now.
    var thisImages = this;
    var webKitVersion = 0;
    var result = /AppleWebKit\/([\d.]+)/.exec(navigator.userAgent);
    if (result) {
      webKitVersion = parseFloat(result[1]);
    }

    // This should find all Android browsers lower than build 533.1
    if (webKitVersion <= 533.1) {
      var scrollStartPos, scrollInPx = 0;

      this.scrollContainer.get(0).addEventListener('touchstart', function(event) {
        scrollStartPos = event.touches[0].pageX;
      });

      this.scrollContainer.get(0).addEventListener('touchmove', function(event) {
        event.preventDefault();
        scrollInPx = scrollStartPos - event.touches[0].pageX;
        var currentScroll = thisImages.scrollContainer.scrollLeft();
        if (scrollInPx > 0) {
          thisImages.scrollContainer.scrollLeft(currentScroll + thisImages.defaultWidth);
        } else {
          thisImages.scrollContainer.scrollLeft(currentScroll - thisImages.defaultWidth);
        }
      });
    }
  },

  setupOrientationChange: function() {

    var thisImages = this;
    function checkOrientationChange() {
      var width = $('body').width();
      if (thisImages.currentWidth != width) {
        thisImages.currentWidth = width;
        thisImages.scrollContainer.css('width', width + 'px');
        thisImages.render();
        return;
      }
      setTimeout(checkOrientationChange, 50);
    }

    window.onorientationchange = checkOrientationChange;
  },

  getValue: function(){
    var imagesURL = $('#da_images div.image').map(function(){
      return $(this).data('image');
    }) || [];
    return $.makeArray(imagesURL);
  },

  setValue: function(value){
    if (value) {
      value = value.split(',');
      value.forEach(function(image) {
        var fileId = image.split('.')[0];
        images.appendImageLoaderFor(fileId);
        images.push(fileId, image);
      });
    }
  },

  initDraft: function(){
    Draft.draftMe(images);
  }
};

function UploaderFactory() { // eslint-disable-line no-unused-vars
  /* Decides which image upload lib to use */

  this.create = function(cb) {
    $.ajaxSetup({ cache: true });
    var version = images.container.data('version');
    var compression = images.container.data('compression');

    if (compression && canCompressImages() && !isAndroidBrowser() && !isWindowsPhone()) {
      $.getScript('/js-'+version+'/plupload-new.js', function() {
        var uploader = new UploaderPU();
        cb(uploader);
      });
    } else {
      $.getScript('/js-'+version+'/fine-uploader-new.js', function() {
        var uploader = new UploaderFU();
        cb(uploader);
      });
    }
  };

  function canCompressImages() {
    return resizeImage() && sendMultipart();
  }

  function resizeImage() {
    return accessBinary() && createCanvas();
  }

  function sendMultipart() {
    return !!(window.XMLHttpRequest && new XMLHttpRequest().upload && window.FormData);
  }

  function accessBinary() {
    return !!(window.FileReader || (window.File && window.File.getAsDataURL));
  }

  function createCanvas() {
    var el = document.createElement('canvas');
    return !!(el.getContext && el.getContext('2d'));
  }

  function isAndroidBrowser() {
    var isWindows = isWindowsPhone();
    var isAndroid = navigator.userAgent.indexOf('Android') >= 0;
    var webkitVer = parseInt((/WebKit\/([0-9]+)/.exec(navigator.appVersion) || 0)[1],10) || void 0; // also match AppleWebKit
    var isNativeAndroid = isAndroid && !isWindows && webkitVer <= 534 && navigator.vendor.indexOf('Google') == 0;
    return isNativeAndroid;
  }

  function isWindowsPhone() {
    return navigator.userAgent.indexOf('Windows Phone') >= 0;
  }
}

function imageUploadedTag(compression) { // eslint-disable-line no-unused-vars
  try {
    xt_med('C', '10', 'Add_an_image::Add_an_image::Add_an_image::Add_image', 'A');
    if (!compression)
      xt_med('C', '10', 'Add_an_image::Add_an_image::Add_an_image::No_compression', 'A');
  } catch(e) {} // eslint-disable-line no-empty
}
