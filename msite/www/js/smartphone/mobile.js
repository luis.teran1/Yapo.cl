/* exported get_page_custom_variables, showRegions*/

// mobile.js: functions common to ALL mobile pages
// add to this file only what's STRICTLY NECESSARY for ALL PAGES

// scroll to the beginning of the page to hide the URL bar on mobile browsers
setTimeout(function() {
  if (!document.location.hash) {
    window.scrollTo(0, 1);
  }
}, 100);

/**
 *
 * @name get_page_custom_variables
 * @description
 *
 * [TODO: write this].
 *
 * @param {String} errorClass - CSS classname of error elements.
 * @return {String}
 *
 */
function get_page_custom_variables(errorClass) { // eslint-disable-line camelcase
  var selector = errorClass || 'error';
  var elems = document.getElementsByClassName(selector);

  var data = [].map.call(elems, function onEachElement(element) {
    var f = element.getAttribute('f');
    var text = element.innerHTML;

    if (!f) {
      return false;
    }

    if (element.offsetHeight <= 0 && element.offsetWidth <= 0) {
      // If the error's element isn't visible
      return false;
    }

    return f + '=' + encodeURIComponent(text);

  }).filter(function filterData(value) {
    return value;
  });

  return data.join('&');
}

function showRegions() {
  $('.regions_list').toggleClass('show-region');
  window.scrollTo(0, 0);
  $('#ct:not(.ad-detail_pg)').toggle();
  $('.wrapper.new-ai').toggle();
  return false;
}

