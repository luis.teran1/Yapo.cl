
function setupFineUploader(selector) {

	var uploader = new qq.FineUploaderBasic({
		debug: true,
		maxConnections: 1,
		button: document.getElementById('upload'),
		request: {
			inputName: 'image',
			endpoint: '/mpu/upload',
			forceMultipart: true,
			params: {
				json: "1"
			}
		},
		multiple: true,
		callbacks: {
			onSubmit: function(id, file) {
				var success = true;

				if ($('#load_images div').length >= uploader.settings.max_files) {
					$('#ad_images_error_message', window.parent.document).show();
					success = false;
				} else {
					$('#upload').before('<div class="loading"><img src="/img/loader-light.gif"></div>');
				}

				uploader.mpuRefresh();
				return success;
			},
			onComplete: function(id, file, response) {
				$('#load_images').find('.loading:first').remove();

				if (response.ok && response.image) {
					var img = response.image;
					var n = createImageNode(img);
					$('#upload').before(n);
					imageUploadedTag();
				} else {
					// need an error response? here's the place
				}
				uploader.mpuRefresh();
			}
		}
	});

	uploader.settings = {
		max_files: 6
	};

	uploader.mpuChangeMaxImages = function (num_images) {
		var cur_images = $('#load_images div').length;
		if (cur_images <= num_images) {
			uploader.settings.max_files = num_images;
			$('.layer.image.loaded').show()
		} else if (cur_images > num_images) {
			uploader.settings.max_files = num_images;
			$('.layer.image.loaded').slice(num_images).hide()
			$('#ad_images_error_message', window.parent.document).show();
		}
		this.mpuRefresh();
	};

	uploader.mpuRefresh = function () {
		if ($('#load_images div').length >= uploader.settings.max_files) {
			$('#upload').hide();
		} else {
			$('#upload').show();
			$('#ad_images_error_message', window.parent.document).hide();
		}
		updateDraftForImages();
	};

	uploader.mpuDelete = function (fileNode) {
		// If we have hidden images, clean those first
		if ($('#load_images div').length > uploader.settings.max_files) {
			//uploader.splice(uploader.settings.max_files);
			$('.layer.image.loaded').slice(uploader.settings.max_files).remove()
		}
		// Remove from the internal array and the DOM
		$(fileNode).remove();
		uploader.mpuRefresh();
	};

	selector.data('mpu', uploader);
	return uploader;
}

