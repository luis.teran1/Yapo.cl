var submitting = false;

$(document).ready( function(){

	$('#ad_images').show()
	mpuSetup();

	withUploader(load_category_image_containers);

	/*
	 * Removed hack (adjustImagePlaceholders) that haunted the minds of the js gods for many moons
	 * and replaced it with the all mighty and beautiful plupload library (interfaced in mpu.js)
	 */

	//Apply icheck
	$(".icheck.blue").iCheck({
		checkboxClass: 'icheckbox_minimal-blue',
		increaseArea: '20%' // optional
	});

	$(".icheck.green").iCheck({
		checkboxClass: 'icheckbox_minimal-green',
		increaseArea: '20%' // optional
	});

	// load category data when the category changes
	$('#category').change(categoryOnChange);
	$('#estate_type').change(categoryOnChange);

	// Handle image deletion and cleanup
	$('#newad_form').on('click', '.remove', function() {
		var t = this;
		withUploader(function(uploader) {
			Pop(LANG.DELETE_IMAGE_CONFIRM, [
				{text: LANG.YES,
				click: function(){
					uploader.mpuDelete(t.parentNode);
				}},
				{text: LANG.NO}
			]);
		});
	});

	$('#newad_form').on('click', 'input[name=type]',toggle_type_params);

	//Limit number values inputs
	numberLimit($('#price'),10);
	numberLimit($('#phone'),11);
	numberLimit($('#address_number'),5);

	$('select, input, textarea').change( function(){
		if($(this).val()){
			clearError($(this));
		}
	});

	// prevent submit of form when you click on go button
	// (detected in androide Gingerbread)
	$('#newad_form').submit(function(){return false;});
	$('#newad_submit').click(function(e) {
		if (submitting) {
			// prevent double submits
			return false;
		}
		if ($('.loading').length > 0 ) {
			// at least one image is still being submitted
			Pop(LANG.WAIT_FOR_IMAGES);
			submitting = false;
			return false;
		}
		if (!$('#terms').is(':checked')) {
			// user must agree with terms
			Pop(LANG.ERROR_MUST_AGREE_TERMS);
			$('#terms').focus();
			$('html, body').animate({ scrollTop: 0 }, "fast");
			sendTealiumTagByAjax(true);
			return false;
		}

		if ($('#passwd').length && $('#passwd_ver').length){
			// password verification
			if ($('#passwd').val() != $('#passwd_ver').val()) {
				Pop(LANG.ERROR_PASSWORD_MISMATCH);
				sendTealiumTagByAjax(true);
				return false;
			}
			if ($('#passwd').val().length < 5) {
				Pop(LANG.ERROR_PASSWORD_TOO_SHORT);
				sendTealiumTagByAjax(true);
				return false;
			}
			if ($('#passwd').val().length > 15) {
				Pop(LANG.ERROR_PASSWORD_TOO_LONG);
				sendTealiumTagByAjax(true);
				return false;
			}
		}
		// prevent double-submitting
		submitting = true;
		$(this).attr('disabled', true);

		// add image inputs
		var images = $('.layer.image.loaded').map(function() { return $(this).data('image'); });
		$('form#newad_form input[name="image[]"]').remove();
		$.map(images, function(img) {
			  $('<input type="hidden" name="image[]"/>').val(img).appendTo('form#newad_form');
		});

		// we do magic
		$('div.alert:visible').hide();
		var $form = $('#newad_form');
		var data = {};
		$.ajax($form.attr('action'), {
			data: $form.serialize(),
			dataType: 'json',
			type: 'post',
			success: function(data) {

				if (data && data.ok) {
					/* add stats */
					if (document.getElementById("stats_1x1")) {
						document.getElementById("stats_1x1").src = document.getElementById("stats_1x1").src.replace('_mai', '_vf');
					}
					var xtcustom = document.getElementById('MaiConfXiti').value;
					var category = $('#category').val();
					xtcustom = xtcustom.replace('CATEGORY_JS_REPLACE', category);
					xt_med('F','','foo&stc=' + xtcustom);

					if($('#want_bump').prop('checked')){
						document.location.href = $("#newad_form").data("payment-redirect");
						return;
					}

					$("[name|='pop-modal-body']").attr('id','success_ad_insert');
					/* Still alive for native aps  */
					if($("#is_mobile_app").attr("value") == "true"){
						Pop(LANG.THANK_YOU_WANT_NEW_AD_MINUTES, [
							{'text' : LANG.GO_TO_SEARCH, 'url':'/chile'},
							{'text' : LANG.PLACE_NEW_AD, 'url':'/mai?is_mobile_app=true'}
							], function() {
								// the user just closed the popup
								document.location.href = '/mai?is_mobile_app=true';
							});

						$("[name|='pop-modal-body']").attr('id','success_ad_insert');
					}else{
						if(!data.account_mail){
							$('#create_account').attr('checked',false);
						}
						if(data.action) {
							$('#newad_form').append('<input type="hidden" name="old_category" value="'+data.old_category+'">');
							$('#newad_form').append('<input type="hidden" name="da_id" value="'+data.da_id+'">');
							$('#newad_form').append('<input type="hidden" name="action" value="'+data.action+'">');
							$('#newad_form').attr('action','/mai/success-edit');
						} else {
							$('#newad_form').append('<input type="hidden" name="da_id" value="'+data.da_id+'">');
							$('#newad_form').attr('action','/mai/success');
						}
						$('#newad_form').append('<input type="hidden" name="pack_result" value="'+data.pack_result+'">');
						$('#newad_form').append('<input type="hidden" name="has_weekly_bump" value="'+(data.has_weekly_bump||0)+'">');
						$('#newad_form').append('<input type="hidden" name="has_daily_bump" value="'+(data.has_daily_bump||0)+'">');
						$('#newad_form').append('<input type="hidden" name="has_gallery" value="'+(data.has_gallery||0)+'">');
						$('#newad_form').append('<input type="hidden" name="has_label" value="'+(data.has_label||0)+'">');
						document.getElementById('newad_form').submit();
					}
					cleanDraft();

					return;
				}
				if(data.error && data.error == "session_error"){
					$('#newad_form').attr('action','/mai');
					document.getElementById('newad_form').submit();
					return;
				}

				$("div.malert").each(function() {
					$(this).hide();
				});


				// handling the UI if the ad was detected as a duplicated
				if ("has_duplicates" in data.errors && data.errors.has_duplicates == "1") {
					return duplicated(data);
				}

				// some error ocurred
				var focusedFirst = false;
				var lowerPosition = 9999999999;
				for (var key in data.errors) {
					$('div.malert[for=' + key + '] span.msg').html(data.errors[key]);
					$('div.malert[for=' + key + ']').show();

					if ($('#' +key + ':enabled').position() != undefined) {
						if ($('#' + key + ':enabled').position().top < lowerPosition) {

							var $elm = $('#' + key + ':enabled');
							lowerPosition = $('#' + key +':enabled').position().top;
						}
					} else if ($('div#ad_information > div:has(label.checkbox[for=' + key + '_1])').position() != undefined) {
						if ($('div#ad_information > div:has(label.checkbox[for=' + key + '_1])').position().top < lowerPosition) {

							lowerPosition = $('div#ad_information > div:has(label.checkbox[for=' + key + '_1])').position().top;
							var $scroll = true;
						}
					}
				}

				// Create an array of every visible error message.
				// If 5 or less errors, show them in same popup. Sorry Paulo :)
				var alerts = $("span.msg:visible").map(function(){
					if (this.innerHTML.length) {
						return this.innerHTML;
					}
				});

				var alertArray = alerts.toArray();
				if (alertArray.length > 5) {
					var alertMessage = LANG.MULTIPLE_ERRORS;
				} else {
					var alertMessage = alertArray.join("<br />");
				}

				var xtcustom = document.getElementById('MaiErrXiti').value;
				var xtcustom_var = get_page_custom_variables('error_msg');
				xt_med('F','','foo&stc=' + xtcustom + '&' + xtcustom_var);
				sendTealiumTagByAjax(true);

				Pop(alertMessage, null, function() {
						if ($scroll != undefined)
							$('html, body').animate({ scrollTop: lowerPosition }, "fast");
						else if ($elm != undefined) {
							$elm.focus();
					}
				});

				$('#newad_submit').removeAttr('disabled');
				submitting = false;
			}
		});
	});

	if ($('#category').val() != "") {
		$('#category').trigger('change');
	}

	$('#newad_form').on('change','#brand:not([disabled])', brandOnChange).on('focus', '#brand:not([disabled])',function(){ focus_car_params() });
	$('#newad_form').on('change', '#model:not([disabled])', modelOnChange);
	$('#newad_form').on('change', '#version:not([disabled])', versionOnChange);
	$('#newad_form').on('change', '#regdate:not([disabled])',function(){
		var category = $('#category').val();
		sync_select_value('#regdate');
		if(category && category =="2020" && $('body').hasClass('app-newad')){
			on_change_car_modify_title_mobile();
		}
	});
        $('#newad_form').on("change",'select[name=gearbox]', function(){sync_select_value('#gearbox');});
        $('#newad_form').on("change",'select[name=fuel]', function(){sync_select_value('#fuel');});
        $('#newad_form').on("change",'select[name=cartype]', function(){sync_select_value('#cartype');});
        $('#newad_form').on("change",'input[name=mileage]', function(){sync_input_value('#mileage');});
        $('#newad_form').on("change",'select[name=cubiccms]', function(){sync_select_value('#cubiccms');});

	$('#newad_form').on("change", '#c_ad', check_account_rut);
	$('#newad_form').on("change",'#p_ad', check_account_rut);
	$('#newad_form').on("change",'#create_account', check_account_rut);


	$('#newad_form').on('ifUnchecked', ".icheck", function(){
		$(this).parent().removeClass("hover");
	});

	initDraft('#category, #subject, #price, #body');
	initDraft('.da-location-box #region');
	initDraft('.da-seller-information-box #pdefault input');
	initDraft('#create_account');
	loadImagesFromDraft();
});


function categoryOnChange() {
	var $this = $('#category');
	var category = $this.val();
	var region   = $("#region").val();
	var estate_type  = $("#estate_type").val();
	if (category) {
		load_category_params(category, region,estate_type);
		set_pro_for_category(category);
	} else {
		$('#ad_information').hide();
	}
};

function brandOnChange(e, $this) {
	if (e)
		$this = $(e.target);
	var brand = $this.val();
	var category = $('#category').val();
	var brandSelector = '#brand[accesskey=' + $this.attr('accesskey') + ']';
	if(typeof(params) != "undefined" && brand && brand >= 0){
		if (params['param_brand'] != brand){
			params['param_brand'] = brand;
			// avoid to change value before the control get loaded
			if ($('#newad_form').data('last_loaded_model_from_brand'))
				params['param_model'] = '';
			if ($('#newad_form').data('last_loaded_version_data'))
				params['param_version'] = '';
		}
	}
	if(category && category > 0 && brand && brand >= 0){
		// sync selectors
		$('#brand[disabled]').get(0).selectedIndex = $this.get(0).selectedIndex;
		/* if we had changed the brand */
		var modelSelector = '#model[accesskey=' + $this.attr('accesskey').replace('brand', 'model') + ']';
		var versionSelector = '#version[accesskey=' + $this.attr('accesskey').replace('brand', 'version') + ']';
		/* clear ALL the things \o */
		$(modelSelector).find('option[value!=""]').remove();
		$(versionSelector).find('option[value!=""]').remove();

		load_models(category, brand);
	}
	if($('body').hasClass('app-newad')) {
		on_change_car_modify_title_mobile();
	}
}

function modelOnChange(e, $this){
	if (e)
		$this = $(e.target);
	if (! $this)
		$this = $('#model:not([disabled])');
	var brandSelector = '#brand[accesskey=' + $this.attr('accesskey').replace('model', 'brand') + ']';
	var model = $this.val();
	var brand = $(brandSelector).val();
	if(typeof(params) != "undefined" && model && model >= 0){
		if (params['param_model'] != model){
			params['param_model'] = model;
			if ($('#newad_form').data('last_loaded_version_data'))
				params['param_version'] = '';
		}
	}
	var category = $('#category').val();
	if(category && category > 0 && brand && brand >= 0 && model && model >= 0){
		load_versions(category, brand, model);
	}
	if($('body').hasClass('app-newad')) {
		on_change_car_modify_title_mobile();
	}
}

function versionOnChange(e){
	var version = $(this).val();
	if(typeof(params) != "undefined" && version && version >= 0){
		if (params['param_version'] != version)
			params['param_version'] = version;
	}
}

function load_models(category, brand) {
	var last_loaded_model_from_brand = $('#newad_form').data('last_loaded_model_from_brand');
	if (last_loaded_model_from_brand != brand){
		$('#newad_form').data('last_loaded_model_from_brand', brand);
		$('#model').removeClass('disabled-look');
		$.ajax({
			url: '/get_brand_models',
			data: {'cg': category, 'brand': brand},
			success: function(data) {
				$('#newad_form').data('last_loaded_model_from_brand_data', data);
				set_models_data(category, brand, data);
			}
		});
	} else {
		set_models_data(category, brand, $('#newad_form').data('last_loaded_model_from_brand_data'));
	}
};

function set_models_data(category, brand, data){
	$('#model:not(.disabled-look)').html(data);
	if (typeof(params) != "undefined" && typeof(params['param_model']) != "undefined" && params['param_brand'] == brand) {
		$('#model:not([disabled])').val(params['param_model']).change();
	}
	if (initDraft){
		initDraft('[name=model]', true);
	}
	if(brand == 0){
		$("#model:not(.disabled-look) option[value='0']").prop("selected",true);
		$("#model:not(.disabled-look)").addClass("disabled-look");
		load_versions(category,brand,0);
	}
}

function load_versions(category, brand, model) {
	var last_loaded_version_from_brand = $('#newad_form').data('last_loaded_version_from_brand');
	var last_loaded_version_from_model = $('#newad_form').data('last_loaded_version_from_model');
	if (last_loaded_version_from_model != model || last_loaded_version_from_brand != brand){
		$('#newad_form').data('last_loaded_version_from_brand', brand);
		$('#newad_form').data('last_loaded_version_from_model', model);
		$('#version').removeClass('disabled-look');
		$.ajax({
			url: '/get_brand_models',
			data: {'cg': category, 'brand': brand, 'model': model},
			success: function(data) {
				$('#newad_form').data('last_loaded_version_data', data);
				set_versions_data(category, brand, model, data);
			}
		});
	} else {
		set_versions_data(category, brand, model, $('#newad_form').data('last_loaded_version_data'));
	}
};

function set_versions_data(category, brand, model, data){
	$('#version:not(.disabled-look)').html(data);
	if (typeof(params) != "undefined" && typeof(params['param_version']) != "undefined"
		&& params['param_brand'] == brand
		&& params['param_model'] == model) {
			$('#version:not([disabled])').val(params['param_version']).change();
	}
	if (initDraft)
		initDraft('[name=version]', true);
	if(model == 0){
		$("#version:not(.disabled-look) option[value='0']").prop("selected",true);
		$("#version:not(.disabled-look)").addClass("disabled-look");
	}
}

function toggle_params_div(e, args){
	var params_div_array = document.getElementsByName("params_div");
	for (var i = 0; i < params_div_array.length; i++) {
		if (params_div_array[i].id == "params_" +  this.getAttribute("id")) {
			params_div_array[i].style.display = "block";
		}
		else {
			params_div_array[i].style.display = "none";
		}
	}
}

function get_settings(setting_name, keylookup_func, settings_root, extra) {
	if (!settings_root)
		settings_root = settings;

	var res;

	for (var i in settings_root[setting_name]) {
		var setting = settings_root[setting_name][i];
		var val;

		val = null;
		if (settings_root[setting_name][i]['keys']) {
			for (var j in settings_root[setting_name][i]['keys']) {
				var key = settings_root[setting_name][i]['keys'][j];
				var key_val = keylookup_func(key, extra);

				if (setting[key_val]) {
					setting = setting[key_val];
				} else if (setting['*']) {
					setting = setting['*'];
				} else {
					break;
				}
			}

			if (setting['value'])
				val = setting['value'];
		} else if (settings_root[setting_name][i]['default']) {
			val = settings_root[setting_name][i]['default'];
		}
		if (val) {
			if (res)
				res += ',' + val;
			else
				res = val;
			if (!settings_root[setting_name][i]['continue'])
				break;
		}
	}

	return res;
}

function split_setting(val) {
	if (!val)
		return {};

	var arr = val.split(',');
	var res = {};

	for (i = 0; i < arr.length; i++) {
		var kv = arr[i].split(':', 2);

		if (kv && kv.length > 1)
			res[kv[0]] = kv[1];
		else {
			res[arr[i]] = 1;
		}
	}

	return res;
}

function showRegions(){
	$(".regions_list").toggleClass('show-region');
  window.scrollTo(0, 0);
	$(".row-fluid").toggle();
	$("#newad_form").toggle();
    //Show the draft wrap only when the flag is 1, hide it when the region list is open.
    if($('.regions_list.show-region').size() == 0){
        if(isDraftMessageVisible()){
            $("#draft_wrap").show();
        }
    } else {
        $("#draft_wrap").hide();
    }
	return false;
}

function car_get_selects_mobile() {
	select_brand = $('#brand:not([disabled])');
	select_model = $('#model:not([disabled])');
	select_version = $('#version:not([disabled])');
	select_year = $('#regdate:not([disabled])');
	category_group = $('#category');
}

// shows or hides elements according to the type
function toggle_type_params() {
	// hide the unused param divs, disable their elements
	$('div.control-group[id*="params_"]').each(function() {
		$(this).hide();
		$('[name]', this).attr('disabled', true);
	});

	// show the current type's parameters, enabled them
	$('#params_' + getType()).each(function() {
		$(this).show();
		$('[name]', this).removeAttr('disabled');
		$('[name]', this).change();
		var size = $('#size:enabled');
		var mileage = $('#mileage:enabled');
		var internal_memory = $('#internal_memory:enabled');
		var garagespaces = $('#garage_spaces:enabled');
		var condominio = $('#condominio:enabled');
		numberLimit(size,7);
		numberLimit(mileage,9);
		numberLimit(internal_memory,6);
		numberLimit(garagespaces,5);
		numberLimit(condominio,10);
	});
};

function load_category_image_containers(uploader) {
	// let the library know of the existence of the original ad's images
	update_category_image_container(uploader);
}

function update_category_image_container(uploader, selector){
	// set max according to new category
	var selected_category = $('#category :selected').val();
	var num_images = $('#default_image_num').data('default_image_num') + 1;
	var num_images_cat = $('#category_image_num').data('category_image_num');
	if (num_images_cat)
		num_images = num_images_cat;
	uploader.mpuChangeMaxImages(num_images);
};

// newad initialization
function getType() {
	var current_type =  $('input[name=type]:checked').val();
	if(current_type == undefined) {
		var current_type =  $('input[name=type]').val();
	}
	return current_type;
};

function prefill_ad_params() {
	if (typeof(params) == 'undefined') {
		return false;
	}
	var types = ['s','u','k','h'];

	for (var k in params) {
		for (i = 0; i < types.length; i++) {
			var type = types[i];
			var paramname = k.substring(6);
			var field = (paramname) ? $('#params_'+type+' #'+paramname) : $('');
			if (field.size() > 0) {
				//after jquery upgrade, we MUST verify if value exists before select it
				if(!(field.is('select') && field.find('option[value="' + params[k] + '"]').size() == 0)){
						field.val(params[k]);
				}
			}
		}

	}

	if (typeof(params.param_job_category) != 'undefined') {
		job_cats = params.param_job_category.split('|');
		for(var k in job_cats) {
			$('#jc_'+job_cats[k]).attr('checked', 'checked');
		}
	} else if (typeof(params.param_service_type) != 'undefined') {
		service_types = params.param_service_type.split('|');
		for(var k in service_types) {
			$('#sct_'+service_types[k]).attr('checked', 'checked');
		}
	}
}


// load the params for the specified category
function load_category_params(c, r, t){
	var type = getType();
	var elementForType = function(type) {
		return $('input[name=type][value=' + type + ']:visible');
	};

	$('#ad_information').load('/get_category_params', {cat:c, region:r, estate_type:t}, function() {
		var typeSelected = false;

		// do stuff with the loaded parameters
		if (type) {
			var $type = elementForType(type);
			if ($type.length) {
				// the previously-selected type is still valid
				$type.attr('checked', true);
				typeSelected = true;
			}
		}

		// select a default type if none is selected
		if (!typeSelected && typeof(params) != 'undefined') {
			var param_type = params['param_type'];
			if (elementForType(param_type).length) {
				elementForType(param_type).attr('checked', true);
			} else {
				// no type "s". that's uncommon
				$('input[name=type]:visible:first').attr('checked', true);
			}
		}

		prefill_ad_params();
		toggle_type_params();

		withUploader(update_category_image_container);
		initDraft('#ad_information select:not(#model, #version), #ad_information input, #ad_information textarea');
		$('#estate_type').change(categoryOnChange);
	});
	$('#ad_information').show();
	toggle_price(c);

	// set data-cat attribute to detect that the form has already reloaded
	$('#ad_information').attr('data-cat', c);
};

function set_pro_for_category(category) {
	var is_pro = get_settings('is_pro_for', function(){return category;}, CATEGORY_SETTINGS);
	if ($("#pdefault").data("is_pro_for")  != "") {
		if(is_pro == "1"){
			$("#pdefault").show();
		}
		else{
			$("#pdefault").hide();
		}
	}
}

function toggle_price(c) {
	// shows or hides price param depending on category
	var previous_price = $('#price').val();

	var key = function () { return c; };

	var toggle = split_setting(get_settings('noprice', key, CATEGORY_SETTINGS));
	if (toggle && toggle.enabled == 1) {
		$('#price').hide();
		previous_price = $('#price').val();
		$('#price').val(null);
	} else {
		$('#price').show();
		$('#price').val(previous_price);
	}
};

function check_account_rut(){
	if($('#c_ad').is(":checked") &&  $('#create_account').is(":checked") ){
		$('#rut_container').show();
	}else{
		$('#rut_container').hide();
	}
}

function clearError($element){
	$element.next('.malert').hide();
}

function numberLimit($input,limit) {
	$input.waitUntilExists(function() {
		$(this).on('keyup keypress blur change', function(e) {
			if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
			   return false;
			}else{
				if( $(this).val().length >= parseInt(limit) && (e.which != 8 && e.which != 0)){
					return false;
				}
			}
		});
	});
}

function sync_select_value(selector){
	var $select_active = $(selector + ':not([disabled])');
	var $select_inactive = $(selector + '[disabled]');
	$select_inactive.val($select_active.val());
}

function sync_input_value(selector){
	var $input_active = $(selector + ':not([disabled])');
	var $input_inactive = $(selector + '[disabled]');
	$input_inactive.val($input_active.val());
}
