(function($) {

	function PluploadMPU(selector, options) {
		this.selector = selector;
		this.options = $.extend(options, {
			init: {
				FilesAdded: this.__proto__.plupFilesAdded,
				UploadProgress: this.__proto__.plupUploadProgress,
				FileUploaded: this.__proto__.plupFileUploaded,
				FilesRemoved: this.__proto__.plupFilesRemoved,
				Error: this.__proto__.plupError,
			}
		});
		this.uploader = new plupload.Uploader(options);
		this.uploader.mpu = this;
		this.uploader.init();
	};

	PluploadMPU.prototype = {

		_mpuIndexOf: function (file) {
			var uploader = this.uploader;
			for (var i = 0; i < uploader.files.length ; i++) {
				var f = uploader.files[i];
				if (file.id === f.id ) {
					return i;
				}
			}
			return -1;
		},

		mpuImageCount: function() {
			return $('#load_images div').length - 1;
		},

		mpuRefresh: function () {
			var uploader = this.uploader;
			if (this.mpuImageCount() >= uploader.settings.max_files) {
				uploader.disableBrowse(true);
				$('#upload').hide();
			} else {
				uploader.disableBrowse(false);
				$('#upload').show();
				$('#ad_images_error_message', window.parent.document).hide();
			}
			uploader.refresh();
			updateDraftForImages();
		},

		mpuDelete: function (fileNode) {
			var uploader = this.uploader;
			// If we have hidden images, clean those first
			if (this.mpuImageCount() > uploader.settings.max_files) {
				uploader.splice(uploader.settings.max_files);
				$('.layer.image.loaded').slice(uploader.settings.max_files).remove()
			}
			// Remove from the internal array and the DOM
			if ($(fileNode).data('file_id'))
				uploader.removeFile($(fileNode).data('file_id'));
			$(fileNode).remove();
			this.mpuRefresh();
		},

		mpuChangeMaxImages:	function (num_images) {
			var uploader = this.uploader;
			var cur_images = this.mpuImageCount();
			if (cur_images <= num_images) {
				uploader.settings.max_files = num_images;
				$('.layer.image.loaded').show()
			} else if (cur_images > num_images) {
				uploader.settings.max_files = num_images;
				$('.layer.image.loaded').slice(num_images).hide()
				$('#ad_images_error_message', window.parent.document).show();
			}
			this.mpuRefresh();
		},

		plupFilesAdded: function(up, files) {
			var uploader = up.mpu;
			var uploading = files.length
			plupload.each(files.reverse(), function(file) {
				if (uploader.mpuImageCount() + uploading > up.settings.max_files) {
					up.removeFile(file);
					$('#ad_images_error_message', window.parent.document).show();
				} else {
					$('#upload').before('<div class="loading"><img src="/img/loader-light.gif"></div>');
				}
				uploading--;
			});
			uploader.mpuRefresh();
			this.start();
		},

		plupUploadProgress: function(up, file) {

		},

		plupFileUploaded: function(up, file, response){
			var uploader = up.mpu;
			var r = $(response.response);
			var error = r.data('error');
			if (error) {
				up.removeFile(file);
				if (error == 1) {
					// PHP upload error, maybe wrong upload type
				}
			} else {
				r.data('file_id', file.id);
				$('#upload').before(r);
				imageUploadedTag(file.size < file.origSize);
			}
			var k = uploader._mpuIndexOf(file);
			if (k >= 0 && uploader.mpuImageCount() > up.settings.max_files) { 
				var extra = Math.max(up.settings.max_files, k+1);
				up.splice(extra);
				$('.layer.image.loaded').slice(extra).remove()
			}
			$('#load_images').find('.loading:first').remove();
			uploader.mpuChangeMaxImages(up.settings.max_files);
			up.refresh();
		},

		plupFilesRemoved: function(up, files) {
			plupload.each(files, function(file) {
				$('#load_images').find('.loading:first').remove();
			});
		},

		plupError: function(up, err) {
			// Ka-boom!
		}

	};

	$.fn.mpu = function(options) {
		return this.each(function(){
			if ($(this).data('mpu') == undefined) {
				options = $.extend({}, $.fn.mpu.defaults, options);
				var mpu = new PluploadMPU($(this), options);
				$(this).data('mpu', mpu);
			}
		});
	};

	$.fn.mpu.defaults = {
		browse_button: 'upload',
		url: '/mpu/upload/',
		file_data_name: 'image',
		max_files: 6,
		multipart: true,
		multi_selection: true,
		resize: {
			width: 640,
			height: 640
		},
		filters: {
			//prevent_duplicates: true,
			mime_types : [
				{ title : "Image files", extensions : "jpeg,jpg,gif,png" }
			],
			max_file_size: "50mb"
		}
	};
})(jQuery);

