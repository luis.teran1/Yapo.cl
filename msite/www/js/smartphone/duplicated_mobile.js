function activateAdDone() {
	$("#duplicated_da").addClass("is-hidden");
	$("#notice_publish_f").addClass("is-hidden");
	$("#activated_da").removeClass("is-hidden");
}

function activateAdFailed() {
	$("#duplicated_da").addClass("is-hidden");
	$("#notice_publish_f").addClass("is-hidden");
	$("#activated_da").removeClass("is-hidden");
	$("#activated_da .message.successfully").addClass("is-hidden");
	$("#activated_da .message.failed").removeClass("is-hidden");
}


function changeFailedMsg(text) {
  $("#activated_da .message.failed").html("<h3>" + text + "</h3>");
}

function changeFailedTitle(text) {
  $("#activated_da .activatedAd-subtitle").html(text);
  $("#activated_da .activatedAd-title").html("Activando aviso");
}
