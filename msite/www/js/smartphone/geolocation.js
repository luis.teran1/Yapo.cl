// support for geo-location
window.Geo = {
	supported: (navigator.geolocation && navigator.geolocation.getCurrentPosition) ? true : false,
	get: function(cb, error_cb) {
		// determines current position
		navigator.geolocation.getCurrentPosition(function(pos) {
			// got a valid position from the browser
			$.getJSON("/get_region_url.json?longitude=" + pos.coords.longitude + '&latitude=' + pos.coords.latitude, function(data) {
				if (cb) {
					// call the callback function with the server's response and the browser's position
					cb(data, pos);
				}
			});
		}, function(error) {
			// an error happened; maybe the user denied permission?
			if (error_cb) {
				error_cb(error);
			}
		});
	}
};

