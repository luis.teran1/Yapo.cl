var draftFirstStageLoadedFlag = false;
var phoenixSettings = getPhoenixSettings();
var hasDraftFlag = storageGetItem('yapoDraft.' + location.host + '/mai/form/.hasDraft');
var isEditForm = (location.href.indexOf('editar') > 0);
var isStorageDisabled = (! getStorage()) ? true : false ;
var waitingForAdparamsDraft = false;

function draftElementSaved(e){
	var id = e.target.getAttribute('id');
	var s_path_input = 'yapoDraft.' + location.host + '/mai/form/.INPUT.';
	var s_path_select = 'yapoDraft.' + location.host + '/mai/form/.SELECT.';
	switch (id){
		case 'create_account': // end of first stage draft loading
			if (!draftFirstStageLoadedFlag){
				initDraft('#name, #rut, #email, #phone, #phone_hidden, #create_account');
				draftFirstStageLoadedFlag = !draftFirstStageLoadedFlag;
			}
			break;
		case 'category':
			if (draftFirstStageLoadedFlag){
				sessionStorage.removeItem(s_path_input + 'type_s.type.');
				sessionStorage.removeItem(s_path_input + 'type_u.type.');
				sessionStorage.removeItem(s_path_input + 'type_k.type.');
				sessionStorage.removeItem(s_path_input + 'type_h.type.');
			}
			break;
		case 'type_s':
			storageSetItem(s_path_input + 'type_u.type.', $('#type_u').is(':checked'));
			storageSetItem(s_path_input + 'type_k.type.', $('#type_k').is(':checked'));
			storageSetItem(s_path_input + 'type_h.type.', $('#type_h').is(':checked'));
			break;
		case 'type_u':
			storageSetItem(s_path_input + 'type_s.type.', $('#type_s').is(':checked'));
			storageSetItem(s_path_input + 'type_k.type.', $('#type_k').is(':checked'));
			storageSetItem(s_path_input + 'type_h.type.', $('#type_h').is(':checked'));
			break;
		case 'type_k':
			storageSetItem(s_path_input + 'type_s.type.', $('#type_s').is(':checked'));
			storageSetItem(s_path_input + 'type_u.type.', $('#type_u').is(':checked'));
			storageSetItem(s_path_input + 'type_h.type.', $('#type_h').is(':checked'));
			break;
		case 'type_h':
			storageSetItem(s_path_input + 'type_s.type.', $('#type_s').is(':checked'));
			storageSetItem(s_path_input + 'type_u.type.', $('#type_u').is(':checked'));
			storageSetItem(s_path_input + 'type_k.type.', $('#type_k').is(':checked'));
			break;
		case 'approx':
			storageSetItem(s_path_input + 'exact.geoposition_is_precise.', $('#exact').is(':checked'));
			$('#exact').data('old_value', $('#exact').is(':checked'));
			break;
		case 'exact':
			storageSetItem(s_path_input + 'approx.geoposition_is_precise.', $('#approx').is(':checked'));
			$('#approx').data('old_value', $('#approx').is(':checked'));
			break;
		case 'p_ad':
			storageSetItem(s_path_input + 'c_ad.company_ad.', $('#c_ad').is(':checked'));
			$('#p_ad').data('old_value', $('#p_ad').is(':checked'));
			break;
		case 'c_ad':
			storageSetItem(s_path_input + 'p_ad.company_ad.', $('#p_ad').is(':checked'));
			$('#p_ad').data('old_value', $('#p_ad').is(':checked'));
			break;
	}
}

function draftElementLoaded(e){
	var id = e.target.getAttribute('id');
	var selector = '#' + id;
	var accessKey = e.target.getAttribute('accesskey');
	if (accessKey) {
		var type = accessKey.substring(0,1);
		selector = selector + '[accesskey=' + accessKey + ']';
	}
	var $this = $(selector);
	var value = $this.val();
	if (! $this.data('phoenix_loaded')){
		$this.data('phoenix_loaded', true);
		switch (id){
			case 'category':
				if ($('#category').val() == '2020')
					waitingForAdparamsDraft = true;
				categoryOnChange();
				break;
			case 'version':
				waitingForAdparamsDraft = false;
				break;
			case 'type_s':
			case 'type_u':
			case 'type_k':
			case 'type_h':
				if($(selector).is(':checked') ){
					toggle_type_params();
				}
				break
			case 'brand':
				if ($('#type_'+type).is(':checked'))
					brandOnChange(null, $this);
				break;
			case 'model':
				if ($('#type_'+type).is(':checked'))
					modelOnChange(null, $this);
				break;
			case 'p_ad':
			case 'c_ad':
				check_account_rut();
				break;
			case 'communes':
				enableDisableAddLocation();
				initDraft('#add_location');
				break;
			case 'add_location':
				initDraft('#address');
				$('#' + id).trigger('change');
				break;
			case 'address':
				initDraft('#address_number');
				$('#' + id).trigger('change');
				break;
			case 'address_number':
				initDraft('#approx, #exact, #geoposition');
				$('#' + id).trigger('change');
				break
			case 'region':
				$('#' + id).trigger('change');
				break
		}
	}
}

function getPhoenixSettings(){
	return { saveInterval: -1, webStorage: getStorage(), clearOnSubmit: '#newad_form'};
}

function initDraft(selector, force){
	if (isEditForm || isStorageDisabled) return false;
	showDraftMessage();
	$(selector).each(function(index){
		var node = $(selector)[index];
		var $node = $(node);
		if (force)
			$node.removeAttr('phoenix_loaded');
		if (! $node.data('plugin_phoenix')){
			$node.bind('phnx.loaded', draftElementLoaded);
			$node.bind('phnx.saved', draftElementSaved);
			$node.phoenix(phoenixSettings);
			if ($node.attr('id') != 'communes')
				$node.on('change', draftNodeOnChange);
		} else {
			var nodeSelector = $node.attr('id');
			var accessKey = $node.attr('accesskey');
			if (accessKey)
				nodeSelector = nodeSelector + '[accesskey=' + accessKey +  ']';
			loadDraft(nodeSelector);
		}
	});
}

function draftNodeOnChange(e){
	var $this = $(e.target);
	var old_value = $this.data('old_value');
	var $node = $this.data('plugin_phoenix');
	var value = getPhoenixValue($node);
	if (( ! old_value) && value == ''){
		$this.data('old_value', '');
	}
	else {
		if (old_value != value){
			$this.data('old_value', value);
			setHasDraftFlag(true);
			draftNodeValueChange($this);
		}
	}
}

function draftNodeValueChange($this){
	var id = $this.attr('id')
	var value = $this.val();
	switch (id){
		case 'p_ad':
		case 'c_ad':
			check_account_rut();
			break;
	}
}

function getPhoenixValue(node){
	return node.$element.is(":checkbox, :radio") ? node.element.checked : node.element.tagName === "SELECT" ? (selectedValues = $.map(node.$element.find("option:selected"), function(el) { return el.value; }), JSON.stringify(selectedValues)) : node.element.value;
}

function setHasDraftFlag(value){
	if (value){
		setShowDraftFlag(true);
		$('#draft_wrap').show();
	} else
		$('#draft_wrap').hide();
	
	hasDraftFlag = (value) ? 1 : 0;
	storageSetItem('yapoDraft.' + location.host + '/mai/form/.hasDraft', hasDraftFlag);
}

function setShowDraftFlag(value){
	storageSetItem('yapoDraft.' + location.host + '/mai/form/.showDraft', (value) ? 1 : 0);
}

function showDraftMessage(){
	if (hasDraftFlag == 1)
		$('#draft_wrap').show();
}

function loadDraft(selector){
	$(selector).phoenix('load');
}

function saveDraft(selector){
	$(selector).phoenix('save');
}

function cleanDraft(){
	// clean all the elements
	$everything = 
		$('#newad_form input, #newad_form select, #newad_form textarea')
		.not('#show_location')
		.not('.yellow input');
	$radios = $('#newad_form input')
		.not('#show_location')
		.not('.yellow input');

	$everything.phoenix('remove');
	$everything.val('');
	$radios.prop('checked', false);
	$everything.removeData('old_value');

	// fucking special cases
	selectRegion(-1);
	$('#category').val("").trigger('change');
	$('#add_location').trigger('change');
	$("#communes").trigger('change');

	// images
	if (uploader === undefined){
		$('.layer.image.loaded').remove();
		$('#upload').show();
		$('#ad_images_error_message', window.parent.document).hide();
	} else
		$('.layer.image.loaded').each(function(){ uploader.mpuDelete(this) });
	cleanDraftForImages();

	// we are ready, draft's gone
	setHasDraftFlag(false);
	setShowDraftFlag(false);
}

/* Draft for image upload */
function updateDraftForImages(){
	if (isEditForm || isStorageDisabled) return false;
	var imagesURL = new Array();
	var $nodes = $('#load_images div.image.loaded');
	if ($nodes.size() > 0){
		$nodes.each(function(index){
			imagesURL.push($(this).data('image'));
		});
		setHasDraftFlag(true);
		storageSetItem('yapoDraft.' + location.host + '/mai/form/.uploadedImages', imagesURL);
	}
}

function loadImagesFromDraft(){
	if (isEditForm || isStorageDisabled) return false;
	var imagesURL = storageGetItem('yapoDraft.' + location.host + '/mai/form/.uploadedImages');
	if (imagesURL){
		imagesURL = imagesURL.split(',');
		var parentNode = ($('#load_images'));
		for (var i = 0; i < imagesURL.length; i++){
			$('#upload').before(createImageNode(imagesURL[i]));
		}
	}
}

function cleanDraftForImages(){
	if (isEditForm || isStorageDisabled) return false;
	sessionStorage.removeItem('yapoDraft.' + location.host + '/mai/form/.uploadedImages');
}
