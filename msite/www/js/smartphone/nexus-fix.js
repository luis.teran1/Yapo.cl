$(document).ready(function() {
	setSpritesChrome();
});

function setSpritesChrome(){
	var ua = navigator.userAgent;
	if(ua.indexOf("Galaxy Nexus") != -1 && ua.indexOf("Chrome") != -1){
		$(".bt_back-listing, .bt_prev-next, .bt_reply, .bt_callphone, validation_msg, .error, .logo .link, .item, .bt_inserir-anuncio, .bt_localizacao_state, .bt_meus-anuncios").each(function(){
			if($(this).css("background-image").indexOf("sprite_mobile2.png") != -1){
				$(this).addClass("sprite-bg");
			}
		});
	}
}
