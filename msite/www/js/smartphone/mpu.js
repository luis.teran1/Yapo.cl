
// When the scripts are loaded $('#upload').data('mpu') will contain the uploader
function mpuSetup() {

	$.ajaxSetup({ cache: true })
	var version = $('#load_images').data('version');
	var compression = $('#load_images').data('compression');

	if (compression && canCompressImages() && !isAndroidBrowser() && !isWindowsPhone()) {
		$.getScript('/js-'+version+'/plupload.js', function () {
			$('#upload').mpu();
		});
	} else {
		$.getScript('/js-'+version+'/fine-uploader.js', function () {
			setupFineUploader($('#upload'));
		});
	}
}

// Execute given code after the uploader is ready
function withUploader(func, delay) {
	uploader = $('#upload').data('mpu');
	if (uploader)
		func(uploader);
	else
		setTimeout(withUploader, delay ? delay : 500, func);
}

function canCompressImages() {
	
	function access_binary() { 
		return !!(window.FileReader || (window.File && window.File.getAsDataURL));
	};

	function create_canvas() {
		var el = document.createElement('canvas');
		return !!(el.getContext && el.getContext('2d'));
	}

	function resize_image() {
		return access_binary() && create_canvas();
	};

	function send_multipart() {
		return !!(window.XMLHttpRequest && new XMLHttpRequest().upload && window.FormData);
	}

	return resize_image() && send_multipart();
}

function isAndroidBrowser() {

	var isWindows = navigator.userAgent.indexOf('Windows Phone') >= 0;
	var isAndroid = navigator.userAgent.indexOf('Android') >= 0;
	var webkitVer = parseInt((/WebKit\/([0-9]+)/.exec(navigator.appVersion) || 0)[1],10) || void 0; // also match AppleWebKit
	var isNativeAndroid = isAndroid && !isWindows && webkitVer <= 534 && navigator.vendor.indexOf('Google') == 0;

	return isNativeAndroid;
}

function isWindowsPhone() {
	var isWindows = navigator.userAgent.indexOf('Windows Phone') >= 0;
	return isWindows;
}

function imageUploadedTag(compression) {
	try {
		xt_med('C', '10', 'Add_an_image::Add_an_image::Add_an_image::Add_image', 'A');
		if (!compression)
			xt_med('C', '10', 'Add_an_image::Add_an_image::Add_an_image::No_compression', 'A');
	} catch(e) {}
};

function createImageNode(img) {
	// based on ./msite/templates/mobile/smartphone/mpu.html.tmpl
	var url = $('#load_images').data('image_host')+'/thumbsli/'+img.substring(0,2)+'/'+img; 
	var r = document.createElement('div');
		r.className = 'layer image loaded';
		r.style.backgroundImage = "url("+url+")";
		$(r).data('image', img);

		var i = document.createElement('input');
			i.className = 'remove';
			i.value = '';
			i.type = 'button';

		r.appendChild(i);
	return r;
};
