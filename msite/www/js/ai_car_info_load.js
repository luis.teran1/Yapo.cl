/* Global: Draft, AdParams, AdInfo */

let handleObservers = [];

function initCarValues() {
  const carValues = getCarValues();
  
  if (carValues) {
    docReady(loadParams(handleCarValues(carValues)));
  }
}

function loadInputParams(values) {
  const priceEl = document.getElementById('price');
  priceEl.value = values.price;
  priceEl.focus();
}

function loadParams(values) {
  const catSelect = document.getElementById('parent_category');
  const adParams = {
    parent_category: "2000",
    category: "2020",
    type: "s",
    brand: values.brand.toString(),
    model: values.model.toString(),
    version:values.version.toString(),
    regdate: values.year.toString(),
  };
  const adInfo = [
    {
      "id": "currency_type",
      "value": "peso"
    },
    {
      "id": "price",
      "value": values.price.toString()
    }
  ];

  Draft.reset(true);
  AdParams.setValue(adParams);
  AdInfo.setValue(JSON.stringify(adInfo));
  removeLocalCarValues('car_info', handleObservers);
}
