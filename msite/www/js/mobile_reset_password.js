$(document).ready( function(){
	
	$("input[type=password]").keyup( function(){
		checkPasswords();
	});

	$("#go-to-publish").click( function(e){
		e.preventDefault();
		var link = $(this).attr("data-link");
		location.href = link;
	})

});

function checkPasswords(){
	
	$(".icon-form-ok").hide();
	
	if($("#password").val().length >= 5){
		$("#password").next().show();
	}else{
		return;
	}

	if($("#password_verification").val().length >= 5 && $("#password").val() == $("#password_verification").val()){
		$("#password_verification").next().show();
	}
}
