/* globals Yapo:true, communes, UploaderFactory, Images, images, params
 messages, get_page_custom_variables, xt_med, app_id, app_code,
 duplicated, Draft, GeoPosition, Categories, AdParams, AdInfo,
 UserInfo, regionRegExp, ext_code_regexp_alnum */

/* eslint wrap-iife:["error", "inside"], quote-props:0, no-useless-escape:0 */

var validator;
var submitting = false;

Yapo = window.Yapo || {};

(function() {
  'use strict';

  var $region;
  var $commune;
  var $geoButton;
  var $geoPosition;
  var $form;
  var $rut;
  var $companyAd;
  var phoneInput;
  var $yourInfoLoginButton;
  var hsLastFocus = false;
  var userClickedSubmit = false;
  var useGeoPosition = false;
  var areaCodeValidation = {
    areaCode: 'areaCode',
    required: true
  };
  var $lastFocus;

  location.hash = '';

  $(document).ready(init);
  $(document).on('click', '#geolocate_me', requestUserLocation);
  $(document).on('change', '#want_bump', toggleCheck);

  $(document).on('pagebeforeshow', function(e) {
    var page = $(e.target);

    if (page.hasClass('hs-page') && !hsLastFocus) {
      hsLastFocus = page;
    }
  });

  function silentScroll(page) {
    var id;
    var element;
    var position;

    if (hsLastFocus && page.attr('data-url') === location.pathname) {
      id = hsLastFocus.attr('id').replace('hsp-', '');
      element = $('#' + id).parent();
      position = element.offset().top;
      $.mobile.silentScroll(position);
      hsLastFocus = false;
    }
  }

  $(document).on('pageshow', function(e) {
    var page = $(e.target);

    silentScroll(page);
    
    if (page.is("#hsp-region") || page.is('#hsp-communes')) {
      window.scrollTo({ top: 0, behavior: 'smooth' });
    }
  
    if (page.is("#hsp-eqp")) {
      onEquipmentPageShow();
    }
  /*
   * This thing is sort of a semaphore for the tests.
   * If you don't know what that is, google it :)
   * Please keep it at the end of this function.
   *
   * Thank you! <3
   */
    $('#newad_form').attr('data-where', e.target.id);
  });

  $(document).on('keydown', '[type=number]', preventNotDigits);
  $(document).on('keydown', '#uf_price', preventNotDigitsAndPoints);
  $(document).on('focus', '#uf_price', formatUFwithoutDots);
  $(document).on('paste', '[type=number]', getFalse);
  $(document).on('blur', '[type=number]', removeLeftZero);

  function init() {
    var mobileRadio = document.getElementById('phoneInputTypeMobile');
    var uploaderFactory;
    var $userType;

    setupViewTags();
    setupInteractionTags();
    setupMapWidget();

    if (typeof Yapo !== 'undefined') {
      Yapo.aiUpselling();
      Yapo.aiForcedLogin();
    }

    $('#is_company_c,#is_company_p').on('change', function(evt) {
      changeProfile($(evt.currentTarget));
    });

    $(document).on('change', '#category', changeCategory);
    $(document).on('change', '.currency-type', changeCurrencyValue);

    function changeCategory() {
      setCurrency();
      tagNewAiSubCategories($(this));
    }

    $region = $('#region');
    $commune = $('#communes');
    $geoButton = $('#geolocate_me');
    $geoPosition = $('#geoposition');
    $form = $('#newad_form');
    $rut = $('#rut');
    $companyAd = $('#user_type');
    $yourInfoLoginButton = $('#your_info-login-button');

    $yourInfoLoginButton.click(clickLoginButtonAdinsert);

    // horizontalSelect config
    $region.horizontalSelect({
      onSelect: function onSelectRegion(event, select) {
        updateCommunes(select.val());
        Draft.nodeOnChange(GeoPosition);
        $.horizontalSelect.changePage('communes');
      }
    });

    $commune.horizontalSelect();
    $region.on('change', onRegionChange);
    $commune.on('change', onCommuneChange);
    
    // phoneInput component
    phoneInput = new Yapo.PhoneInput(onPhoneTypeChange, true);

    // mdTextField config
    $('.new-ai input').mdTextField();
    $('.new-ai textarea').mdTextField({ counter: true });

    $('.icheck-radio').iCheck({
      radioClass: 'iradio_minimal-blue',
      increaseArea: '20%' // optional
    });

    // Uploader config
    window.images = new Images();
    uploaderFactory = new UploaderFactory();
    uploaderFactory.create(function onCreateUploader(uploader) {
      images.setUploader(uploader);
      images.render();
    });
 
    tagNewAiCallFunctions();
    preventKeysInput(/[%^\\\/()<>=#\"+]/g, 'name');

    $('[data-url]:first-child').on('pageshow', function() {
      images.render();
      $('body').removeClass('ui-mobile-viewport-transitioning');
    });

    // used to call xiti on failed validation only when the user hits the submit button
    $('#newad_form button[type="submit"]').on('click', function() {
      userClickedSubmit = true;
    });

    $.validator.addMethod('areaCode', function validateAreaCode(value) {
      return phoneInput.isValidAreaCode(parseInt(value, 10));
    });

    // Validation config
    $.validator.addMethod('email', function validateEmail(value) {
      return /^[a-zA-Z0-9._-]+@[a-zA-Z0-9-]+\.[a-zA-Z.]{2,5}$/i.test(value);
    });

    $.validator.addMethod('rut', function validateRut(value) {
      return $.Rut.validar(value);
    });

    /**
     *
     * @name validatePriceMaxLength
     * @description
     *
     * Add Method to validator plugin calling priceMaxLength for run on blur event,
     * this validates if the input number has the max length set in data-max attribute.
     *
     * @param {String} value - value of input text delivered for plugin jQuery validator
     * @param {Object} element - current target of event deliverd for plugin jQuery validator
     *
     **/
    $.validator.addMethod('priceMaxLength', function validatePriceMaxLength(value, element) {
      var maxLength = element.getAttribute('data-max');

      if (value[0] === '0') {
        element.value = element.value.replace('0', '');
      }

      if (value.length >= maxLength) {
        element.value = element.value.slice(0, maxLength);
      }

      return true;
    }, messages.ERROR_PRICE_TOO_HIGH);

    /**
     *
     * @name validatePriceMinLength
     * @description
     *
     * Add Method to validator plugin calling priceMinLength for run on blur event,
     * this validates if the input number has the min length of data-min var only for Inmo Categories
     *
     * @param {String} value - value of input text delivered for plugin jQuery validator
     * @param {Object} element - current target of event deliverd for plugin jQuery validator
     *
     **/
    $.validator.addMethod('priceMinLength', function validatePriceMinLength(value, element) {
      var minLength = element.getAttribute('data-min');
      var cat = $('#category').children(":selected");

      return !(cat.data('reqprice') && value.length < minLength);

    }, messages.ERROR_PRICE_LENGTH);

    $.validator.addMethod('ufPrice', function validateUFPrice(value, element) {
      if(formatUfPrice(element)) {
        var tmp = value.replace('.', '');
        var val = tmp.replace(',', '.');

        if (value !== '') {
          /*
           * Validates price uf not big than ufMax, and has 2 decimals at limit
           * ufMax as global bconf validation value
           *
           */
          if (Math.ceil(val) > ufMax) {
            return false;
          }
        }
      }
      return true;
    }, messages.ERROR_PRICE_TOO_HIGH_MAX);

    $.validator.addMethod('ufInvalid', function validateUFPrice(value) {
        var tmp = value.replace('.', '');
        tmp = tmp.replace('0', '');

        if (tmp !== '') {
          return /(?:^\d{1,6}(,\d*)?$)/.test(tmp);
        }

        return true;
    }, messages.ERROR_PRICE_UF_INVALID);

    $.validator.addMethod('ufMaxLength', function validateUFPRiceLength(value) {
      // ufLen as global bconf validation value
      return value.replace(/,.*/g, '').length <= ufLen;
    }, messages.ERROR_UF_PRICE_TOO_HIGH);

    $.validator.addMethod('ufMaxDecimals', function validateUFPriceDecimals(value) {
      var hasComma = value.match(/,/g);
      var commaIndex = value.indexOf(',');

      return (hasComma && value.substring(commaIndex + 1).length <= 2) || !hasComma;
    }, messages.ERROR_PRICE_UF_TOO_MANY_DECIMALS);

    // Validation plates
    $.validator.addMethod('verify_msite_2060', function validatePlates(value) {
      if (value != '') {
        return regexp_2060.test(value)?'plates':false;
      } else {
        return true;
      }
    }, messages.ERROR_PLATES_MOTO_INVALID);

    $.validator.addMethod('verify_msite_2040', function validatePlates(value) {
      if (value != '') {
        return regexp_2040.test(value)?'plates':false;
      } else {
        return true;
      }
    }, messages.ERROR_PLATES_TRUCKS_INVALID);

    $.validator.addMethod('verify_msite_2020', function validatePlates(value) {
      if (value != '') {
        return regexp_2020.test(value)?'plates':false;
      } else {
        return true;
      }
    }, messages.ERROR_PLATES_CARS_INVALID);

    /**
     *
     * @name validateExt_Code
     * @description
     *
     * Add Method calling extCode to validator plugin, for run on blur event,
     * this validates if the input text have numbers and letters only.
     *
     * @param {String} value - value of input text delivered for plugin jQuery validator
     * @param {Object} element - current target of event deliverd for plugin jQuery validator
     *
     **/
    $.validator.addMethod('extCode', function validateExt_Code(value) {
      if (value != '') {
        return ext_code_regexp_alnum.test(value);
      } else {
        return true;
      }
    }, messages.ERROR_EXT_CODE_INVALID);

    /**
     *
     * @name validateBuiltYear
     * @description
     *
     * Add Method calling builtYear to validator plugin, for run on blur event,
     * this validates if the input number is between 1000 and 1900.
     *
     * @param {String} value - value of input text delivered for plugin jQuery validator
     * @param {Object} element - current target of event deliverd for plugin jQuery validator
     *
     **/
    $.validator.addMethod('builtYear', function validateBuiltYear(value) {
      var val = parseInt(value, 10);

      if (val < 1000 || val > new Date().getFullYear()) {
        return false;
      }

      return true;
    }, messages.ERROR_BUILT_YEAR_INVALID);

    /* This commands overrides default jqueryValidation messages (which are in english), and
     * afaik Yapo is spanish only
     */
    $.extend($.validator.messages, {
      required: 'Este campo es obligatorio.'
    });

    validator = $form.validate({
      onkeyup: false,
      onblur: false,
      errorElement: 'span',
      ignore: ':not([name])',
      errorPlacement: placeError,
      invalidHandler: function() {
        var element = validator.findLastActive();
        var topPosition;

        if (!element) {
          element = validator.errorList[0] && validator.errorList[0].element;
          if (!element) {
            return;
          }
        }

        element = $(element);

        if (element[0].tagName === 'SELECT') {
          element = element.next().find('.hs-trigger-link');
        }

        topPosition = element.offset().top - 20;

        element.focus();
        $('html, body').animate({
          scrollTop: topPosition
        }, 500);
      },
      showErrors: function showErrors() {
        var phoneError = $('#phone-error');
        var xtCustomVar = [];
        var xtCustom;
        var element;
        var error;
        var i;

        for (i = 0; this.errorList[i]; i++) {
          error = this.errorList[i];

          this.showLabel(error.element, error.message);
        }

        for (i = 0; this.successList[i]; i++) {
          element = $(this.successList[i]);
          this.showLabel(element[0]);
          if (element.attr('f') !== '') {
            xtCustomVar.push(element.attr('f') + '=');
          }
        }

        xtCustom = document.getElementById('MaiErrXiti').value + '&';
        xtCustom = '&' + xtCustomVar.join('&');

        if (xtCustomVar.length) {
          xtCustom += '&';
        }

        xtCustom += get_page_custom_variables('error-ai');

        if (phoneError.is(':visible')) {
          xtCustom += '&' + phoneError.attr('f') + '=' +
            encodeURIComponent(phoneError.text());
        }

        if (xtCustom.match(/\=[^\&]+\&?/g)) {
          if (userClickedSubmit) {
            try {
              xt_med('F', '', 'foo&stc=' + xtCustom);
            } catch(e) {} // eslint-disable-line
            userClickedSubmit = false;
          }
        }

        if (this.errorList.length) {
          this.toShow = this.toShow.add(this.containers);
        }

        this.toHide = this.toHide.not(this.toShow);
        this.hideErrors();
        this.addWrapper(this.toShow).show();
      },
      success: function(error, input) {
        var $input = $(input);
        var parent = $input.parent();
        var errorId = error.attr('id');
        var phoneErrorsIds = ['area_code-error', 'phone-error'];
        var phoneError = document
          .getElementsByClassName('phoneInput-errorMessage')[0];
        var areaCode = document.getElementsByClassName('phoneInput-areaCode')[0];
        var areaIsValid;

        parent.removeClass('wrap-error');
        parent.addClass('wrap-valid');

        areaIsValid = !areaCode.parentNode.classList.contains('wrap-error');

        if (phoneErrorsIds[0] === errorId) {
          validator.element('.phoneInput-number');
        } else if (phoneErrorsIds[1] === errorId && areaIsValid) {
          phoneError.innerHTML = '';
          $input.removeClass('error');
          phoneError.classList.add('is-hidden');
          return;
        }

        error.remove();
      },
      rules: {
        email: {
          required: true,
          email: 'email'
        },
        'passwd_ver': {
          equalTo: '#passwd'
        },
        phone: {
          digits: true
        },
        price: {
          digits: true
        }
      },
      messages: {
        subject: {
          required: messages.ERROR_SUBJECT_MISSING,
          maxlength: messages.ERROR_SUBJECT_TOO_LONG,
          minlength: messages.ERROR_SUBJECT_TOO_SHORT
        },
        body: {
          required: messages.ERROR_BODY_MISSING,
          maxlength: messages.ERROR_BODY_TOO_LONG,
          minlength: messages.ERROR_BODY_TOO_SHORT
        },
        price: {
          required: messages.ERROR_PRICE_MISSING,
          maxlength: messages.ERROR_PRICE_TOO_HIGH,
          digits: messages.ERROR_PRICE_INVALID,
          number: messages.ERROR_PRICE_INVALID
        },
        'parent_category': {
          required: messages.ERROR_CATEGORY_MISSING
        },
        category: {
          required: messages.ERROR_SUBCATEGORY_MISSING
        },
        type: {
          required: messages.ERROR_TYPE_MISSING
        },
        region: {
          required: messages.ERROR_REGION_MISSING
        },
        communes: {
          required: messages.ERROR_COMMUNES_MISSING
        },
        name: {
          required: messages.ERROR_NAME_MISSING,
          minlength: messages.ERROR_NAME_TOO_SHORT
        },
        email: {
          required: messages.ERROR_EMAIL_MISSING,
          email: messages.ERROR_EMAIL_INVALID
        },
        passwd: {
          required: messages.ERROR_PASSWORD_MISSING,
          maxlength: messages.ERROR_PASSWORD_TOO_LONG,
          minlength: messages.ERROR_ACCOUNT_PASSWORD_TOO_SHORT
        },
        'passwd_ver': {
          equalTo: messages.ERROR_PASSWORD_MISMATCH,
          required: messages.ERROR_PASSWORD_MISSING
        },
        rut: {
          required: messages.ERROR_RUT_MISSING,
          rut: messages.ERROR_RUT_INVALID
        },
        'area_code': {
          maxlength: messages.ERROR_AREA_CODE_TOO_LONG,
          minlength: messages.ERROR_AREA_CODE_TOO_SHORT,
          required: messages.ERROR_AREA_CODE_MISSING,
          areaCode: messages.ERROR_AREA_CODE_INVALID
        },
        phone: {
          required: messages.ERROR_PHONE_MISSING,
          minlength: messages.ERROR_PHONE_TOO_SHORT,
          maxlength: messages.ERROR_PHONE_TOO_LONG,
          digits: messages.ERROR_PHONE_INVALID,
          number: messages.ERROR_PHONE_INVALID
        },
        rooms: {
          required: messages.ERROR_ROOMS_MISSING
        },
        'garage_spaces': {
          required: messages.ERROR_GARAGE_SPACES_INVALID,
          min: messages.ERROR_GARAGE_SPACES_INVALID,
          max: messages.ERROR_GARAGE_SPACES_TOO_LONG,
          number: messages.ERROR_INTEGER_ONLY
        },
        'capacity': {
          required: messages.ERROR_CAPACITY_INVALID,
          min: messages.ERROR_CAPACITY_INVALID,
          max: messages.ERROR_CAPACITY_TOO_LONG,
          number: messages.ERROR_INTEGER_ONLY
        },
        size: {
          required: messages.ERROR_WAREHOUSE_SIZE_INVALID,
          min: messages.ERROR_WAREHOUSE_SIZE_INVALID,
          max: messages.ERROR_SIZE_TOO_LONG,
          number: messages.ERROR_INTEGER_ONLY
        },
        util_size: {
          required: messages.ERROR_SIZE_INVALID,
          min: messages.ERROR_WAREHOUSE_SIZE_INVALID,
          max: messages.ERROR_SIZE_TOO_LONG,
          number: messages.ERROR_INTEGER_ONLY
        },
        irrig_hect: {
          required: messages.ERROR_IRRIG_HECT_INVALID,
          min: messages.ERROR_IRRIG_HECT_MIN,
          max: messages.ERROR_IRRIG_HECT_TOO_LONG,
          number: messages.ERROR_INTEGER_ONLY
        },
        built_year: {
          number:  messages.ERROR_INTEGER_ONLY
        },
        condominio: {
          required: messages.ERROR_CONDOMINIO_INVALID,
          min: messages.ERROR_CONDOMINIO_INVALID,
          max: messages.ERROR_CONDOMINIO_TOO_LONG,
          number: messages.ERROR_INTEGER_ONLY
        },
        brand: {
          required: messages.ERROR_BRAND_MISSING
        },
        model: {
          required: messages.ERROR_MODEL_MISSING
        },
        version: {
          required: messages.ERROR_VERSION_MISSING
        },
        regdate: {
          required: messages.ERROR_REGDATE_MISSING
        },
        gearbox: {
          required: messages.ERROR_GEARBOX_MISSING
        },
        fuel: {
          required: messages.ERROR_FUEL_MISSING
        },
        cartype: {
          required: messages.ERROR_CARTYPE_MISSING
        },
        mileage: {
          required: messages.ERROR_MILEAGE_MISSING,
          max: messages.ERROR_MILEAGE_TOO_LONG,
          number: messages.ERROR_INTEGER_ONLY
        },
        cubiccms: {
          required: messages.ERROR_CUBICCMS_MISSING
        },
        condition: {
          required: messages.ERROR_CONDITION_MISSING
        },
        'internal_memory': {
          max: messages.ERROR_INTERNAL_MEMORY_TOO_LONG,
          number: messages.ERROR_INTEGER_ONLY
        },
        'jc[]': {
          required: messages.ERROR_JOB_CATEGORY_MISSING
        },
        'sct[]': {
          required: messages.ERROR_SERVICE_TYPE_MISSING
        },
        'footwear_type[]': {
          required: messages.ERROR_FOOTWEAR_TYPE_MISSING
        }
      },
      submitHandler: function(form) {
        postNewAd();
      }
    });

    Categories.init();
    AdParams.init();
    GeoPosition.init();

    // ad edit load ad params
    if (typeof params !== 'undefined') {
      AdParams.setValue($.extend({}, categoryParams, params));
      GeoPosition.setValue(params);
    }

    if (!mobileRadio.checked) {
      validator.settings.rules.area_code = areaCodeValidation; // eslint-disable-line camelcase
    }

    $form.find('select').on('change', function onChangeSelect() {
      validator.element(this);
    });

    onPositionAvailable($geoPosition.val());
    $('#notification_message .icon-close').on('click', function onClickClose() {
      $('#notification_message').hide();
    });

    initDraft();
    initCarValues();
    cleanPhoneInputStyle();
    setCurrency();

    if ($companyAd.val() === '1') {
      $userType = $('#user_type');
      changeProfile($userType);
    }
  }

  function initDraft() {
    AdInfo.initDraft();
    UserInfo.initDraft();
    images.initDraft();
    GeoPosition.initDraft();
    AdParams.initDraft();
  }

  function cleanGeoButtonClasses() {
    $('#geolocate_me').removeClass('-error -error-1 -success -loading');
  }

  function mapClear() {
    cleanGeoButtonClasses();
    $('#geoposition').val('').trigger('change');
  }

  function requestUserLocation() {
    if ($geoPosition.val()) {
      mapClear();
      return;
    }

    cleanGeoButtonClasses();
    $geoButton.addClass('-loading');

    if (navigator.geolocation) {
      navigator.geolocation
        .getCurrentPosition(onPositionSuccess, onPositionError, {
          enableHighAccuracy: false,
          timeout: 5000,
          maximumAge: 0
        });
    }
  }

  function onPositionSuccess(position) {
    var coords = position.coords;
    var coordsString = coords.latitude + ',' + coords.longitude;

    $geoPosition.val(coordsString).trigger('change');
    getReverseGeocode(coords.latitude, coords.longitude);
    onPositionAvailable(position);

    try {
      xt_med('C', '10', 'Ad_insertion::form::form::map_add', 'A');
    } catch(e) {} // eslint-disable-line
  }


  function onPositionError(err) {
    cleanGeoButtonClasses();
    $geoButton.addClass(err.code === 1 ? '-error-1' : '-error');
    // in case the user has blocked xiti
    try {
      xt_med('C', '10', 'Ad_insertion::form::form::map_add_error', 'A');
    } catch(e) {} // eslint-disable-line
  }


  function onPositionAvailable(position) {
    if (!position) {
      return false;
    }

    cleanGeoButtonClasses();
    $geoButton.addClass('-success');
    Draft.nodeOnChange(GeoPosition);
    return true;
  }


  function placeError(error, input) {
    var parent = input.parent();
    var next = input.next();
    var errorId = error.attr('id');
    var oldError = $('#' + errorId);
    var phoneErrorsIds = ['area_code-error', 'phone-error'];
    var phoneError = document
      .getElementsByClassName('phoneInput-errorMessage')[0];
    var areaCode = document.getElementsByClassName('phoneInput-areaCode')[0];
    var areaIsValid;
    error.attr('f', input.attr('f'));
    error.addClass('error-ai');
    parent.addClass('wrap-error');
    parent.removeClass('wrap-valid');

    if (oldError.length && phoneErrorsIds.indexOf(errorId) === -1) {
      oldError.html(error.html());
      return;
    }

    if (next.hasClass('hs-trigger')) {
      next.append(error);
      return;
    }

    areaIsValid = !areaCode.parentNode.classList.contains('wrap-error');

    if (phoneErrorsIds.indexOf(errorId) !== -1) {
      if (phoneErrorsIds[0] === errorId || !phoneError.innerHTML.length) {
        phoneError.innerHTML = error.html();
      }

      if (phoneErrorsIds[1] === errorId &&
          phoneError.innerHTML !== error.html() && areaIsValid) {
        phoneError.innerHTML = error.html();
      }

      phoneError.classList.remove('is-hidden');
      return;
    }

    input.after(error);
  }


  function getReverseGeocode(lat, lng) {
    // @TODO: request only necessaries fields. Check API docs
    $.ajax({
      url: 'https://reverse.geocoder.api.here.com/6.2/reversegeocode.json',
      dataType: 'json',
      data: {
        prox: lat + ',' + lng,
        gen: 9,
        mode: 'retrieveAddresses',
        app_id: app_id, // eslint-disable-line camelcase
        app_code: app_code // eslint-disable-line camelcase
      },
      success: function onReverseGeocodeSuccess(data) {
        fillUbicationParams(
          data.Response.View[0].Result[0].Location.Address.State,
          data.Response.View[0].Result[0].Location.Address.City
        );
      }
    });
  }

  /* Getting the reveser value for commune and region */
  function fillUbicationParams(region, commune) {
    $.ajax({
      url: '/reverse_geo_get.json',
      dataType: 'json',
      data: {
        region: region.replace('.', '_').replace(regionRegExp, ''),
        commune: commune
      },
      success: function onSuccessReverseGeocode(response) {
        $region.val(response.region_id).trigger('change');
        updateCommunes(response.region_id);
        $commune.val(response.commune_id).trigger('change');
        Draft.nodeOnChange(GeoPosition);
      }
    });
  }

  function postNewAd() {
    var data = [];
    var localFields = ['parent_category', 'passwd_ver'];
    var $button;
    var rut;

    if (submitting) {
      return false;
    }

    submitting = true;

    if (images.isLoading()) {
      showNotificationMessage(messages.WAIT_FOR_IMAGES_TO_UPLOAD, 5000);
      submitting = false;
      return false;
    }

    $button = $form.find('[type=submit]');
    $button.addClass('-loading');

    // remove any previously created inputs
    $('#newad_form input[name="image[]"]').remove();
    // add image inputs
    $('.image:visible').each(function onEachImage() {
      var image = $(this).data('image');
      var input = $('<input>', {
        type: 'hidden',
        name: 'image[]'
      });
      input.val(image);
      input.appendTo(images.scrollContainer.parent());
    });

    // please don't send rut if company_ad = 0
    if ($companyAd.val() === '0') {
      $rut.removeAttr('name');
    } else {
      rut = $.Rut.formatear($rut.val(), true);
      $rut.val(rut.replace(/\./g, ''));
    }

    $.each($form.serializeArray(), function filterData(i, field) {
      if (localFields.indexOf(field.name) !== -1) {
        return;
      }
      // format Price whitout points
      if (field.name === 'price') {
        field.value = field.value.replace('.', '');
      }

      data.push(field);
    });

    if ($('#new_realestate').size() === '1') {
      data.push({
        name: 'new_realestate',
        value: $('#new_realestate').hasClass('selected') ? 1 : 0
      });
    }

    if (useGeoPosition) {
      data.push({ name: 'address',                value: getAddress(), });
      data.push({ name: 'address_number',         value: getAddressNumber(), });
      data.push({ name: 'geoposition_is_precise', value: getIsPrecise(), });
    }

    $.ajax($form.attr('action'), {
      type: 'post',
      data: data,
      success: function onSuccessPost(dataObject) {
        // @TODO: make only one requests. Refactor backend.
        var response = JSON.parse(dataObject);
        var fields;
        var xtcustom;
        var xtcustomVar;

        if (response.error && response.error === 'session_error') {
          document.location.href = '/publica-un-aviso';
          return;
        }

        $button.removeClass('-loading');
        if (!response || !response.ok) {
          // handling the UI if the ad was detected as a duplicated
          if (response.errors.has_duplicates &&
              response.errors.has_duplicates === '1') {
            duplicated(response);
            return;
          }

          fields = $form.find('[name]').map(function onEachInput(i, element) {
            return element.name;
          });

          $.each(response.errors, function onEachError(name, error) {
            var input = $('[name=' + name + ']');
            var span;
            var currency = document.getElementById('currency_type').value;
            var idError = name + '-error';

            if (name === 'user_info') {
              $('#yourInfoError').html(error).removeClass('is-hidden');
              return;
            }

            if (error === 'ERROR_OTHER_PARAM') {
              validator.element(input);
              return;
            }

            if (name === 'price' && currency === 'uf') {
              idError = 'uf_price-error';
              input = $('#uf_price');
            } else if (name === 'price' && currency === 'peso') {
              input = $('#price');
            }

            span = $('<span/>', {
              id: idError,
              class: 'error',
              text: error
            });

            placeError(span, input);
          });

          xtcustom = document.getElementById('MaiErrXiti').value;
          xtcustomVar = get_page_custom_variables('error-ai');

          xt_med('F', '', 'foo&stc=' + xtcustom + '&' + xtcustomVar);
          submitting = false;

          $.each(fields, function onEachField(i, name) {
            var element;

            if (!response.errors[name]) {
              return true;
            }

            element = $('[name=' + name + ']');

            if (element.parent().hasClass('hs-wrapper')) {
              element = element.parent().find('.hs-trigger-link');
            }
            
            element.focus();
            return false;
          });

          return;
        }

        if ($('#want_bump').is(':checked')) {
          document.location.href = $('#newad_form').data('payment-redirect');
          return;
        }
        if (response.hasOwnProperty('payment_url')) {
          Draft.reset(true); // soft = true
          document.location.href = response.payment_url;
          return;
        }
        if (response.hasOwnProperty('upselling_payment_url')) {
          Draft.reset(true); // soft = true
          document.location.href = response.upselling_payment_url;
          return;
        }

        if (response.action) {
          $form.append(createHidden('old_category', response.old_category));
          $form.append(createHidden('da_id', response.da_id));
          $form.append(createHidden('action', response.action));
          $form.attr('action', '/mai/success-edit');
        } else {
          $form.append(createHidden('da_id', response.da_id));
          $form.attr('action', '/mai/success');
        }

        $form.append(createHidden('pack_result', response.pack_result));
        $form.append(createHidden('pack_url_suffix', response.pack_url_suffix));
        $form.append(createHidden('has_weekly_bump', response.has_weekly_bump || 0));
        $form.append(createHidden('has_daily_bump', response.has_daily_bump || 0));
        $form.append(createHidden('has_gallery', response.has_gallery || 0));
        $form.append(createHidden('has_label', response.has_label || 0));

        Draft.reset(true); // soft = true
        document.getElementById('newad_form').submit();
      }
    });

    return false;
  }

  function getAddress() {
    return document.querySelector('adinsert-map-shadow').shadowRoot.querySelector('.adinsert-map__input-address').querySelector('input').value;
  }

  function getAddressNumber() {
    return document.querySelector('adinsert-map-shadow').shadowRoot.querySelector('.adinsert-map__input-number').querySelector('input').value;
  }

  function getIsPrecise() {
    var value = document.querySelector('adinsert-map-shadow').shadowRoot.querySelector('adinsert-radio-button[checked=true]').value;
    if (value == "1") {
      return "0";
    } 
    if (value == "2") {
      return "1";
    }
  }

  function setupMapWidget() {
    $('#geoposition').val('');
    document.addEventListener('yapomap::onchange', (event) => {
      $geoPosition.val(event.detail.value[0] + ',' + event.detail.value[1]);
      Draft.nodeOnChange(GeoPosition);
    });

    var adinsertMapShadow = document.querySelector('adinsert-map-shadow');
    if(adinsertMapShadow){
      adinsertMapShadow.shadowRoot
        .querySelector('.adinsert-map__clean-button-container')
        .querySelector('.adinsert-map__button')
        .addEventListener('click', function(e){
          useGeoPosition = false;
          $('#geoposition').val('');
        });
  
      adinsertMapShadow.
        shadowRoot.querySelector('.adinsert-map__button-container')
        .querySelector('.adinsert-map__button')
        .addEventListener('click', function(e){
          useGeoPosition = true;
        });
    }
  }
  
  function createHidden(name, value) {
    return '<input type="hidden" name="' + name + '" value="' + value + '">';
  }

  function changeProfile(input) {
    var HIDE_CLASS = '-hide';
    var value = input.val();
    var parentRut = $('#rut').parent().parent();
    var parentBusinessName = $('#business_name').parent().parent().parent();

    $('#user_type').val(value).trigger('change');

    if (value === '1') {
      parentBusinessName.removeClass(HIDE_CLASS);
    } else {
      parentBusinessName.addClass(HIDE_CLASS);
    }

    if (!$rut.length) {
      return;
    }

    if (value === '1') {
      toggleRutValidation(true);
      parentRut.removeClass(HIDE_CLASS);
    } else {
      parentRut.addClass(HIDE_CLASS);
      $rut.parent().removeClass('wrap-error');
    }

    $('.mdTextField-label[for=name]').text(input.data('username-label'));
  }

  /**
   *
   * @name getFalse
   * @description
   *
   * Always return `false`. It's to be used as callback.
   *
   * @return {Boolean} Always `false`
   *
   */
  function getFalse() {
    return false;
  }

  /**
   *
   * @name removeLeftZero
   * @description
   *
   * For numeric inputs remove left zero of value
   * 0001 to 1
   *
   */
  function removeLeftZero() {
    var $this = $(this);
    var value = $this.val();

    if (value !== '') {
      $this.val(parseInt(value, 10));
    }
  }

  /**
   *
   * @name toggleCheck.
   * @description
   *
   * Add/Remove `checked` CSS's className to '.check' element closest to.
   * event.target
   *
   * @param {Object} event DOM's event.
   *
   */
  function toggleCheck(event) {
    var input = $(event.target);
    var check = input.closest('.check');
    var fn = input.is(':checked') ? 'addClass' : 'removeClass';
    check[fn]('checked');
  }

  function onCommuneChange(evt) {
    var communeCode = evt.target.value;
    if (communeCode && phoneInput) {
      phoneInput.setAreaCodeByCommune(communeCode);
      $('#area_code')
        .trigger('change')
        .closest('.mdTextField-wrapper')
        .addClass('wrap-valid');
    }

    if (communeCode) {
      var communeName = getHereApiName('commune', communeCode);
      document.querySelector('adinsert-map').setAttribute('commune', communeName);
      document.querySelector('adinsert-map').setAttribute('commune-code', communeCode);
    }
  }

  function onRegionChange(evt) {
    var regionCode = evt.target.value;
    if (regionCode) {
      var regionName = getHereApiName('region', regionCode);
      document.querySelector('adinsert-map').setAttribute('region', regionName);
      document.querySelector('adinsert-map').setAttribute('region-code', regionCode);
    }
  }

  function getHereApiName(type, index) {
    return document.querySelector(`.here-api-names__${type}[number='${index}']`).getAttribute('name');
  }

  function onPhoneTypeChange(evt, isMobileNumber) {
    var wrappers = document.querySelectorAll(
      '.phoneInput-inputs .mdTextField-wrapper');
    var rules = {};
    var classes = [];

    [].forEach.call(wrappers, function onEachWrapper(wrapper) {
      classes = classes.concat(Array.from(wrapper.classList));
    });

    if (!isMobileNumber) {
      rules = areaCodeValidation;
    }

    validator.settings.rules.area_code = rules; // eslint-disable-line camelcase

    if (classes.indexOf('wrap-valid') === -1 &&
        classes.indexOf('wrap-error') === -1) {
      return;
    }

    validator.element('.phoneInput-areaCode');
    validator.element('.phoneInput-number');
  }

  /**
   *
   * @name cleanPhoneInputStyle
   * @description
   *
   * Remove style attr and is-hidden class who are added by mdTextField's plugin.
   *
   */
  function cleanPhoneInputStyle() {
    // @TODO this code is duplicate in m_accounts_src.js
    var areaCode = document.getElementsByClassName('phoneInput-areaCode')[0];

    areaCode.classList.remove('is-hidden');
  }

  function setupInteractionTags() {
    setupFocusInteractionTag('#size', 'Total Area Text Field');
    setupFocusInteractionTag('#util_size', 'Useful Area Text Field');
    setupFocusInteractionTag('#garage_spaces', 'Parking Text Field');
    setupFocusInteractionTag('#built_year', 'Constuction Year Text Field');
    setupFocusInteractionTag('#condominio', 'Common Expenses Text Field');
    setupFocusInteractionTag('#price', 'Price Text Field');
    setupFocusInteractionTag('#uf_price', 'Price Text Field');
    setupFocusInteractionTag('#name', 'Seller Name Text Field');
    setupFocusInteractionTag('#business_name', 'Social Reason Text Field');
    setupFocusInteractionTag('#email', 'Seller_Email Text Field');
    setupClickInteractionTag('#hsp-eqp .close', 'Ready Basic Information Button');
    setupClickInteractionTag('#eqp-skip-button', 'Ready Basic Information Button');
    setupClickInteractionTag('#eqp-add-button', 'Ready Basic Information Button');
  }

  function setupClickInteractionTag(elementSelector, elementType) {
    $(document).on('click', elementSelector, function(){
      sendAdInsertTealiumTagInteraction(elementType);
    });
  }

  function setupFocusInteractionTag(elementSelector, elementType) {
    $(document).on('focusin', elementSelector, function(event){
      //Prevent triggers from random focuses when clicking outside the DOM
      if ($lastFocus && event.target == $lastFocus){
        return;
      }
      $lastFocus = event.target
      sendAdInsertTealiumTagInteraction(elementType);
    });
  }

  function setupViewTags() {
    $(document).on('pageshow', function(e) {
      var page = $(e.target);
      
      if (page.is("#hsp-estate_type")) {
        sendAdInsertTealiumTagDisplay('Property Type Section');
      }
      if (page.is("#hsp-rooms")) {
        sendAdInsertTealiumTagDisplay('Number of Rooms Section');
      }
      if (page.is("#hsp-bathrooms")) {
        sendAdInsertTealiumTagDisplay('Number of Bathrooms Section');
      }
      if (page.is("#hsp-eqp")) {
        sendAdInsertTealiumTagDisplay('Available Equipment Section');
      }
    });
  }

  function sendAdInsertTealiumTagDisplay(elementType) {
    if (typeof window.utag !== 'undefined') {
      var utagData = { 
        event_name: 'ad_insert_user_journey_display', 
        data: { element_type: elementType } 
      }
      window.utag.view(utagData);
    }
  }

  function sendAdInsertTealiumTagInteraction(elementType) {
    if (typeof window.utag !== 'undefined') {
      var utagData = { 
        event_name: 'ad_insert_user_journey_interaction', 
        data: { element_type: elementType } 
      }
      window.utag.link(utagData);
    }
  }
})();


function updateCommunes(region) {
  var options = $.map(communes[region], function(name, value) {
    return '<option value="' + value + '">' + name + '</option>';
  });
  var $communes = $('#communes');

  $communes.html(options.join('\n'));
  $communes.val(0).trigger('change');
  $.horizontalSelect.updatePage('communes');
  $('#hst-communes').show();
}

$(document).on('click', '.check-account-cont', function() {
  var check = $('.check', $(this));
  var checked = check.hasClass('checked');
  var input = $('#create_account');

  if (checked) {
    check.removeClass('checked');
    input.prop('checked', false);
  } else {
    check.addClass('checked');
    input.prop('checked', true);
  }
});

function toggleRutValidation(validate) {
  var rule = {};

  if (validate) {
    rule = {
      required: true,
      rut: true
    };
  }

  validator.settings.rules.rut = rule;
}

function setPriceValidation(validate) {
  var currentRules = validator.settings.rules.price;
  var newRulesCurrency = {
    "price" : {
      digits: true,
      required: priceRequired(),
      ufPrice: false,
      ufInvalid: false,
      ufMaxLength: false,
      ufMaxDecimals: false,
      priceMaxLength: true,
      priceMinLength: true
    },
    "uf" : {
      digits: false,
      required: priceRequired(),
      ufPrice: true,
      ufInvalid: true,
      ufMaxLength: true,
      ufMaxDecimals: true,
      priceMaxLength: false,
      priceMinLength: false
    }
  };

  var newRules = validate ? newRulesCurrency.price : newRulesCurrency.uf;

  validator.settings.rules.price = $.extend({}, currentRules, newRules);
  $(validate ? '#price' : '#uf_price').trigger('blur');
}

function togglePriceVisibility(isUF) {
  var wrapperClass = '.mdTextField-wrapper';
  var $uf = $('#uf_price');
  var $price = $('#price');

  if (isUF) {
    $uf
      .removeAttr('disabled')
      .closest(wrapperClass)
      .show();

    $price
      .attr('disabled', 'disabled')
      .closest(wrapperClass)
      .hide();
  } else {
    $uf
      .attr('disabled', 'disabled')
      .closest(wrapperClass)
      .hide();

    $price
      .removeAttr('disabled', 'disabled')
      .closest(wrapperClass)
      .show();
  }
}

function showNotificationMessage(message, timeout) {
  var $message = $('#notification_message');
  var $messageText = $('#notification_message span');

  $messageText.text(message);
  $message.show();

  if (!timeout) {
    return;
  }

  setTimeout(function() {
    $message.hide();
  }, timeout);
}

function setCurrency() {
  var $category = $('#category');
  var $currency = $('.currencyTypes');
  var excludedCategories = [1220, 1240];

  if ($category.length && excludedCategories.indexOf(parseInt($category.val(), 10)) !== -1) {
    $currency.find('.currency-type:checked')
      .prop('checked', false)
      .trigger('click');
    $currency.show();
  } else {
    $('#uf_price').closest('.mdTextField-wrapper').hide();
    $currency.hide();
    // Force currency to `Peso`
    $currency.find('#currency_peso')
      .prop('checked', false)
      .trigger('click');
  }
}

function changeCurrencyValue(evt) {
  var $currency = $(evt.currentTarget);
  var isUF = $currency.hasClass('__uf');

  $('#currency_type')
    .val($currency.val())
    .trigger('change');

  setPriceValidation(!isUF);
  togglePriceVisibility(isUF);
}

function onEquipmentPageShow() {
  var $eqpButtonsContainer = $('#eqp-options');

  if (!$eqpButtonsContainer.length) {
    onEquipmentFirstShow();
  }

  setEquipmentState();
}

function onEquipmentFirstShow() {
  var $eqpElement = $('#hsp-eqp');
  var $uiContentElement = $('#hsp-eqp .ui-content');
  var $nextActionButton = $('#hsp-eqp .close');
  var $uiTitle = $('#hsp-eqp .ui-title');
  var $eqpButtonsContainer = $('<div class="eqp-options" id="eqp-options"/>');
  var $eqpSkipButton = $('<input id="eqp-skip-button" type="button" value="Saltar"></div>');
  var $eqpAddButton = $('<input id="eqp-add-button" type="button" value="Agregar"></div>');

  $uiContentElement.css("margin-bottom", "80px")
  $nextActionButton.css("display", "none");
  $uiTitle.css('margin', '0');
  $eqpSkipButton.addClass("yapo-btn yapo-btn--primary yapo-btn--fullwidth-rounded eqp-options__option-skip");
  $eqpAddButton.addClass("yapo-btn yapo-btn--primary yapo-btn--fullwidth-rounded");

  $eqpButtonsContainer.appendTo($eqpElement);
  $eqpSkipButton.appendTo($eqpButtonsContainer);
  $eqpAddButton.appendTo($eqpButtonsContainer);

  $eqpSkipButton.on('click', function () {
    Categories.catInfo.trigger('next');
  });

  $eqpAddButton.on('click', function () {
    Categories.catInfo.trigger('next');
  });
}

function setEquipmentState() {
  var currentParams = JSON.parse(AdParams.getValue());
  var currentEquipment = null;

  if (currentParams["eqp"]) {
    currentEquipment = currentParams["eqp"]
  }

  if (currentEquipment) {
    onEquipmentNotEmpty();
  } else {
    onEquipmentEmpty();
  }
}

function onEquipmentEmpty() {
  var $eqpAddButton = $('#eqp-add-button');
  $eqpAddButton.attr("disabled", true);
  $eqpAddButton.addClass('eqp-options__option-primary--disabled');
}

function onEquipmentNotEmpty() {
  var $eqpAddButton = $('#eqp-add-button');
  $eqpAddButton.attr("disabled", false);
  $eqpAddButton.removeClass('eqp-options__option-primary--disabled');
}
