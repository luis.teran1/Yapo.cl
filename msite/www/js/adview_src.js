/*globals estate_type, xt_med,
  get_page_custom_variables, messages, xt_click
  getCookie*/

/* exported getScrollXY*/

var submittingAdReply = false;

$(document).ready(function() {
  const adData = GetAdData();
  if (adData) {
    SetupAdviewDataLayer(ParseDataForGTM(adData), GetUserId(), document.referrer);
  }

  if (window.dataLayer) {
    dataLayer.push({
      event: 'adview_event',
      event_name: 'Ad detail viewed',
      event_type: 'View',
      object_type: 'ClassifiedAd',
      target_type: 'ClassifiedAd',
    });
  }

  //Loan manager 
  var adType = document.querySelector('#dataAd').getAttribute('data-adtype');
  var category = document.querySelector('#dataAd').getAttribute('data-category');
  var price = document.querySelector('#dataAd').getAttribute('data-price');
  if (adType === '1' && urlYapoQL && urlYapoQL != '' && category === categoryFinance && price != ''){
    var dataAd = document.querySelector('#dataAd');
    quotaFinance.initialize({ ad: { title: dataAd.getAttribute('data-title'), adId: dataAd.getAttribute('data-id'), urlYapoQL:  urlYapoQL, device: 'mobile'}});
  }
 
  /* Add hectares to estate_type 5 (Terrenos) besides m�*/
  (function setHectares() {
    /*
     * estate_type comes defined from the template
     */
    if (estate_type !== 5) {
      return false;
    }

    /* @TODO: This function is being used in www/js/hectares.js,
     * try placing it in utils.js (which, at this moment, doesn't exist)
     * */
    function truncate(number, places) {
      var shift = Math.pow(10, places);
      return ((number * shift) | 0) / shift;
    }

    var child;
    var value;
    var meters;
    var current;
    var hectares;

    var params = document.getElementsByClassName('ad-param');

    // Loop through every param
    for (var i = 0; i < params.length; i++) {
      current = params[i];
      child = current.children;

      meters = parseInt(child[1].innerText.replace('m�', ''));

      // If current label is 'Superficie' and it's content is higher than 9999
      if (child[0].innerText === 'Superficie' && meters > 9999) {
        value = (meters/10000);
        hectares = String(truncate(value, 1));

        if (hectares.indexOf('.') > -1 || hectares.indexOf(',') > -1) {
          if (hectares[hectares.length - 1] == 0) {
            hectares = hectares.substr(0, hectares.length - 2);
          }
        }

        child[1].innerText = child[1].innerText + ' (' + hectares + ' ha)';
      }
    }
  })();

  $('#ar_close').click(function(){
     $('#form_adreply').hide();
   });

  $('#cotiza_raw_link').on('is-empty', function( ){
    $(this).parents('.services').remove();
  }).on('click', 'a', function() {
    var $div = $(this).parents('#cotiza_raw_link');

    try {
      xt_med('C', $div.data('xiti_xtn2'), $div.data('xtclick_name'), $div.data('xtclick_type'));
    } catch(e) {} // eslint-disable-line
  });

  checkAndRemoveIcon();

  //Description animate
  function checkAndRemoveIcon() {
    var box = document.querySelector('.description-cont');
    var text = document.querySelector('.textBox');
    var textHeight = text.offsetHeight;
    var boxHeight = box.offsetHeight;
    var descriptionDegr;
    var descriptionWrapper;
    var icon;

    if (textHeight < boxHeight) {
      descriptionDegr = box.querySelector('.description-degr');
      descriptionWrapper = document.querySelector('.descriptionWrapper');
      icon = descriptionWrapper.querySelector('.icon-yapo');

      if (icon) {
        descriptionWrapper.removeChild(icon);
      }

      if (descriptionDegr) {
        descriptionDegr.remove();
      }
    }
  }

  $('.descriptionWrapper .icon-yapo').click(function(){
    var box = $('.description-cont');
    var text = $('.textBox');
    var degrade = $('.description-degr');
    var textHeight = text.outerHeight() + 20;

    if ($(this).hasClass('icon-arrow-down')){
      box.css('max-height', textHeight);
    } else {
    box.removeAttr('style');
    }
    $(this).toggleClass('icon-arrow-down');
    $(this).toggleClass('icon-arrow-up');
    degrade.toggleClass('__noDegrade');
  });

  // Move pages transitions and stickyBar of daview page at the pageContainer
  createPageTransitions();
  moveStickyBarOfAdviewPage();

  //Close category type
  $('.value').click(function(){
    if ($(this).hasClass('open')){
      $('.value').removeClass('open');
      $('.value').addClass('close');
      $('.value .icon-yapo').removeClass('icon-arrow-up');
      $('.value .icon-yapo').addClass('icon-arrow-down');
      $('.job-service-type .description-degr').show();
    } else {
      $('.value').removeClass('close');
      $('.value').addClass('open');
      $('.value .icon-yapo').removeClass('icon-arrow-down');
      $('.value .icon-yapo').addClass('icon-arrow-up');
      $('.job-service-type .description-degr').hide();
    }
  });

  //Admin panel animation
  $('.options-panel').click(function(){
    var $iconArrow = $('.options-panel .icon-yapo');
    if  ($iconArrow.hasClass('icon-yapo icon-arrow-down')){
      $iconArrow.removeClass('icon-arrow-down');
      $iconArrow.addClass('icon-arrow-up');
    } else {
      $iconArrow.removeClass('icon-arrow-up');
      $iconArrow.addClass('icon-arrow-down');
    }

    $('.admin-panel').slideToggle('fast', function(){
      $('html, body').animate({
        scrollTop: $('.admin-panel').offset().top
      });
    });
  });

  $('.cotiza-link').click(function() {
    $('addetail-stickybar').hide();
  });

  //Get account data
  var accountData = {
    accSession: '',
  };
  
  document.addEventListener('account-service::islogged', function getUser() {
    accountData = JSON.parse(localStorage.getItem('accountData'));
  });

  function NotifyInteractionByMail(contactMethod) {
    var isLogged = accountData.accSession !== '';

    if (!isLogged) {
      return;
    }

    var url = '/notify';
    var ad = document.querySelector('#dataAd');
    var adId = ad.getAttribute('data-id');

    var body = {
      'list_id': adId,
      'contact_method': contactMethod, 
    };
    
    fetch(url, {
      method: 'POST',
      credentials: 'include',
      body: JSON.stringify(body),
    });
  }

  // Big Map
  $('#map_page').on('pageshow', function() {
    showMapModal();
    var stickyBar = document.querySelector('addetail-stickybar');
    if (stickyBar) {
      stickyBar.setAttribute('hide-navigation', '');
      stickyBar.style.display = 'block';
    }
    restartAdReplyForm();
    $('.overlay').click();
    xt_med('C', '', $('#map_container').data('xtclick_name'), $('#map_container').data('xtclick_type'));
  });
  window.onorientationchange = function() {
    setTimeout(function() {
      adjustMapWrapSize();
      if (window.mapInstance) {
        window.mapInstance.invalidateSize();
      }
    }, 300);
  };

  // Daview page
  $('#site-wrapper').parent().on('pageshow', function() {
    var $stickyBar = $('addetail-stickybar');
    var $adreplyResponseOk = $('#form_response_ok');

    $stickyBar.removeAttr('hide-navigation');
    $stickyBar.show();

    if ($adreplyResponseOk.is(':visible') && !$adreplyResponseOk.data('response_ok')) {
      $('html,body').animate({scrollTop: $adreplyResponseOk.offset().top - 10}, function() {
        $adreplyResponseOk.data('response_ok', true);
      });
    }
  });

  // Subir page
  $('#subir_page').on('pageshow', function() {
    $('addetail-stickybar').hide();
  });

  // Header in modal page
  $('.modal-page-header a').click(function(e) {
    e.preventDefault();
    window.history.back();
  });


  $('.new-ai-box input').mdTextField();
  $('.new-ai-box textarea').mdTextField({
    counter:true
  });

  // Ad reply
  $('#form_adreply').on('pageshow', function() {
    const placeholderEnabled = placeholderMsite.enabled || 0;
    const placeholderCategories = placeholderMsite.categories || [];
    const placeholderText = placeholderMsite.text || '';
    const placeholderIsAvailable = placeholderMsite.isAvailable || '';
    const adData = document.querySelector('#dataAd')
    const currentCat = adData.getAttribute('data-category');
    const adTitle = adData.getAttribute('data-title');
    const sellerName = document.querySelector('seller-info').getAttribute('username');
    const replyBody = document.querySelector('#adreply_body');
    const labelReplyBody = document.querySelector('[for=adreply_body]');
    const spanCounter = document.querySelector('span.mdTextField-counter');
    const maxChar = replyBody.getAttribute('maxlength');

    if (placeholderEnabled && placeholderCategories.includes(currentCat)) {
      try {
        replyBody.value = `${placeholderText} ${sellerName}, ${decodeEntities(adTitle)}, ${placeholderIsAvailable}.`;
        labelReplyBody.classList.add('mdTextField-input-dirty');
        spanCounter.innerText = maxChar - replyBody.value.length;
        sendTealiumTagAddUtagData({event_name: 'ad_reply_placeholder_shown'}, false);
      } catch(err) {} // eslint-disable-line
    }

    try {
      xt_med('F', '', $('#xt_reply_bottom').attr('page'));
    } catch(err){} // eslint-disable-line

    $('#tealium_ad_reply_show_bottom').trigger('click');
  });

  $('.adreply_cc').keydown(function(e){
    var keyCode = e.keyCode || e.which;
    if(keyCode === 32){
      checkInputIcheckCc();
    }
  });

  $(document).on('click', '.adreply_cc', function() {
    checkInputIcheckCc();
  });

  var userClickedSubmit = false;

  // used to call xiti on failed validation only when the user hits the submit button
  $('#reply_data button[type="submit"]').on('click', function() {
    userClickedSubmit = true;
  });

  $('#reply_data').validate({
    onkeyup: false,
    errorElement: 'span',
    ignore: ':not([name])',
    errorPlacement: placeError,
    showErrors: function() {
      var plugin = this;

      var xtcustom = $('#sendTipErrXiti').data('page');
      var xtcustom_var = get_page_custom_variables();

      // send xiti tag
      if (userClickedSubmit && this.errorList.length) {
        try {
          xt_med('F','','foo&stc=' + xtcustom + '&' + xtcustom_var);
        } catch(e) {} // eslint-disable-line
        userClickedSubmit = false;
      }
      // Show errors
      $.each(this.errorList, function(index, error) {
        plugin.showLabel(error.element, error.message);
      });

      // Show success elements
      $.each(this.successList, function(index, element) {
        plugin.showLabel(element);
      });

      if (this.errorList.length){
        this.toShow = this.toShow.add(this.containers);
      }
      this.toHide = this.toHide.not(this.toShow);
      this.hideErrors();
      this.addWrapper(this.toShow).show();
    },
    success: function(error, input) {
      input = $(input);
      var parent = input.parent();
      parent.removeClass('wrap-error');
      parent.addClass('wrap-valid');
      error.remove();
    },
    submitHandler: function(form) {
      sendAdReply(form);
    },
    rules: {
      email: {
        required: true,
        email: 'email'
      },
      phone: {
        digits: true
      }
    },
    messages: {
      name: {
        required: messages.ERROR_NAME_MISSING,
        maxlength: messages.ERROR_NAME_TOO_LONG,
        minlength: messages.ERROR_NAME_TOO_SHORT
      },
      email: {
        required: messages.ERROR_EMAIL_MISSING,
        email: messages.ERROR_EMAIL_INVALID
      },
      phone: {
        required: messages.ERROR_PHONE_MISSING,
        minlength: messages.ERROR_PHONE_TOO_SHORT,
        maxlength: messages.ERROR_PHONE_TOO_LONG,
        digits: messages.ERROR_PHONE_INVALID,
        number: messages.ERROR_PHONE_INVALID
      },
      adreply_body: {
        required: messages.ERROR_ADREPLY_BODY_MISSING,
        maxlength: messages.ERROR_ADREPLY_BODY_TOO_LONG,
        minlength: messages.ERROR_ADREPLY_BODY_TOO_SHORT
      }
    }
  });

  // Share Functions
  $('#share_facebook').click(function(event) {
    event.preventDefault();
    url = $(this).attr('data-facebook');
    window.open('https://www.facebook.com/sharer.php?t=&u=' + url, 'facebook_popup', 'width=640, height=480');
  });

  $('#share_twitter').click(function(event) {
    event.preventDefault();
    url = $(this).attr('data-tweet');
    window.open('https://twitter.com/share?text=' + url, 'twitter_popup', 'width=640, height=480');
  });

  //Load suggested questions
  const faqs = getFaqsYapoQL(urlGraphQL, parseInt(categorySuggestedQuestions, 10));
 
  faqs.then(function (data) {
    if (data.response) {
      const questions = loadSuggestedQuestions(data.response.getFAQsByCategory);

      if (questions.length > 0) {
        renderSuggestedQuestionWidget('suggested_questions', questions);
      }
    }
  });

  // addetail-stickybar functionality
  (function () {
    var bar = document.getElementsByTagName('addetail-stickybar');
    if (bar.length !== 1 || !(stickybarConfig)) {
      return false;
    }
    bar = bar[0];

    // show whatsapp
    $.get('/enabled?list_id=' + stickybarConfig.adId).always(function (result) {
      if (typeof result === 'object' && result !== null) {
        var props = Object.keys(result);
        if (props.indexOf('enabled') > -1) {
          bar.setAttribute('has-whatsapp', '');
          if (result.enabled === true) {
            bar.setAttribute('whatsapp-enabled', '');
          }
        }
      }
    });

    bar.addEventListener('clickCustom:prev', function () {
      sendTealiumTagAddUtagData(stickybarConfig.tealium.prev, false);
    });

    bar.addEventListener('clickCustom:phone', function () {
      sendTealiumTagAddUtagData(stickybarConfig.tealium.phone, false);
      NotifyInteractionByMail('PHONE_NUMBER');
    });

    bar.addEventListener('clickCustom:phoneCall', function () {
      sendTealiumTagAddUtagData(stickybarConfig.tealium.phoneCall, false);
      NotifyInteractionByMail('PHONE_CALL');
      stickybarConfig.event.phoneCall();
    });

    bar.addEventListener('clickCustom:phoneModalOverlay', function () {
      var dataAd = document.querySelector('#dataAd');
      if (dataAd) {
        var adViewId = dataAd.getAttribute('data-id');
        var adViewType = dataAd.getAttribute('data-adtype');
        var adViewCategory = dataAd.getAttribute('data-category');

        var callCategories = recommendedAdcallConf.categories || [];
        var callTypes = recommendedAdcallConf.adTypes || [];
        var callEnabled = recommendedAdcallConf.enabled || 0;
        if (callEnabled
          && (callCategories.includes(adViewCategory) | callCategories.includes('*'))
          && (callTypes.includes(adViewType) | callTypes.includes('*'))) {
          adCall.showRecommendedAdCall(adViewId)
        }
      }
    });

    bar.addEventListener('clickCustom:whatsapp', function (event) {
      var promA = new Promise(function (resolve) {
        sendTealiumTagAddUtagData({ event_name: 'ad_detail_whatsapp_contact' }, false, function () {
          setTimeout(resolve, 0);
        });
      });

      if (!event.detail.disabled) {
        var promises = [promA];
        var resultRedirect = false;
        promises.push(new Promise(function (resolve) {
          var errorMessage = 'No se pudo contactar al vendedor. Intente m�s tarde';
          event.detail.pause();
          $.get(stickybarConfig.whatsappMessageUrl, function (data) {
            if (data && typeof data.redirect === 'string' && data.redirect.length > 0) {
              resultRedirect = data.redirect;
            } else {
              alert(errorMessage);
            }
            resolve();
          }).fail(function () {
            alert(errorMessage);
            resolve();
          });
        }));

        Promise.all(promises).then(function () {
          if (resultRedirect) {
            event.detail.continue(resultRedirect);
          }
        });

        NotifyInteractionByMail('WHATSAPP');
      }
    });

    bar.addEventListener('clickCustom:chat', function () {
      sendTealiumTagAddUtagData(stickybarConfig.tealium.chat, false);
      $.mobile.changePage('#form_adreply', { transition: 'slide' });
      $('addetail-stickybar').hide();
      restartAdReplyForm();
      try {
        xt_med('C', '', $('#xt_reply_bottom').attr('click'), 'N');
      } catch(err) {} // eslint-disable-line
    });

    bar.addEventListener('clickCustom:next', function () {
      sendTealiumTagAddUtagData(stickybarConfig.tealium.next, false);
    });
  })();
});

function getScrollXY() {
  var scrOfX = 0, scrOfY = 0;
  if( typeof( window.pageYOffset ) == 'number' ) {
    //Netscape compliant
    scrOfY = window.pageYOffset;
    scrOfX = window.pageXOffset;
  } else if( document.body && ( document.body.scrollLeft || document.body.scrollTop ) ) {
    //DOM compliant
    scrOfY = document.body.scrollTop;
    scrOfX = document.body.scrollLeft;
  }
  return [ scrOfX, scrOfY ];
}

function moveStickyBarOfAdviewPage(){
  var $stickyBar = $('addetail-stickybar').remove();
  $stickyBar.appendTo($.mobile.pageContainer);
}

// Page transitions
function createPageTransitions(){
  var pagesTranstions = $('#subir_page, #form_adreply, #map_page, .cotiza-page, .cotiza-banner-page');
  $.each(pagesTranstions, function(index, value){
    var $page = $(value).remove();
    $page.appendTo($.mobile.pageContainer);
    $page.attr('data-role', 'page');
  });
}

// Init Maps functions
function showMapModal(){
  var h = getHeightMap();

  $('#map_page').css('position', 'absolute');

  setTimeout( function(){
    tryBuildMap();
    $('#map_page').css('height', h);
  }, 50);

  // Adjust map wrapper size in case orientation changed
  adjustMapWrapSize();
  if(window.mapInstance){
    window.mapInstance.invalidateSize();
  }
}

function tryBuildMap(){
  if(!$('#modal_map_wrap').html().trim()){
    $('#modal_map_wrap').yapomap({
      debug: true,
      appl: 'mvi',
      appId: $('#modal_map_wrap').data('app_id'),
      getPreciseOptionValue: function(){ return (this.$precise.data('precise') == '1'); },
      preciseInput: $('#modal_map_wrap'),
      appCode: $('#modal_map_wrap').data('app_code'),
      center: $('#modal_map_wrap').data('location'),
      baseUrl: $('#base_url').val(),
      marker: $('#modal_map_wrap').data('location'),
      zoom: $('#modal_map_wrap').data('zoom'),
      draggable: false
    });
  }
}

function adjustMapWrapSize() {
  var w = $(window).innerWidth();
  var h = getHeightMap();
  var hSticky = $('addetail-stickybar .addetail-stickybar').height();
  var hf = $('#map_page_footer').height();
  var hh = $('#map_page_header').height();

  $('#map_page').css('height', h);
  $('#modal_map_wrap').css('width', w);
  $('#modal_map_wrap').css('height', (h - (hf + hh + hSticky)));
}

function getHeightMap(){
  var hSticky = $('addetail-stickybar .addetail-stickybar').height();
  var h = getWindowHeight() - hSticky;
  return h;
}

function getWindowHeight(){
  var ua = navigator.userAgent;
  var iphone = ~ua.indexOf('iPhone') || ~ua.indexOf('iPod');

  // Detect if this is running as a fullscreen app from the homescreen
  var fullscreen = window.navigator.standalone;
  var h = $(window).innerHeight();

  if (iphone && !fullscreen){
    h = getIOSWindowHeight();
  }
  return h;
}

var getIOSWindowHeight = function() {
  var zoomLevel = document.documentElement.clientWidth / window.innerWidth;
  return window.innerHeight * zoomLevel;
};

// Init ad reply functions
function placeError(error, input) {
  var parent = input.parent();
  var next = input.next();
  var oldError = $('#' + error.attr('id'));

  error.attr('f', input.attr('f'));
  error.addClass('error-ad-reply');
  parent.addClass('wrap-error');
  parent.removeClass('wrap-valid');

  if (oldError.length) {
    return oldError.html(error.html());
  }

  if (next.hasClass('hs-trigger')) {
    return next.append(error);
  }

  input.after(error);
}

function checkInputIcheckCc() {
  var $check = $('.adreply_cc.check');
  var $inputCheckCc = $('#cc');

  if (!$check.hasClass('checked')) {
    $check.addClass('checked');
    $inputCheckCc[0].checked = true;
  } else {
    $check.removeClass('checked');
    $inputCheckCc[0].checked = false;
  }
}

function restartAdReplyForm() {
  var $adreplyResponseOk = $('#form_response_ok');
  var $adreplyBody = $('#adreply_body');

  // if ad reply form was already sent
  $adreplyResponseOk.hide();
  $adreplyResponseOk.removeData('response_ok');
  submittingAdReply = false;
  // Clean only adreplay body
  $adreplyBody.val('').
  trigger('change').trigger('blur');
}

function createSimilarButton () {
  var id = 'similarAdButton';
  if(document.querySelector('#' + id)) {
    document.querySelector('#' + id).remove();
  }
  var button = document.createElement('button');
  button.setAttribute('id', id);
  var icon = document.createElement('i');
  icon.classList.add('fal');
  icon.classList.add('fa-clone');
  button.appendChild(icon)
  button.append(' Avisos similares');
  button.classList.add('yapo-btn');
  button.classList.add('yapo-btn--accent');
  button.classList.add('yapo-btn--medium-circle');
  button.classList.add('recommended-btn');
  return button;
}

function decodeEntities(encodedString) {
  var textArea = document.createElement('textarea');
  textArea.innerHTML = encodedString;
  return textArea.value;
}

function createAdReplySimilarCardItem (ad) {
  var cardItem = document.createElement('card-item');
  cardItem.classList.add('modal-recommended__card-item')
  cardItem.setAttribute('text-button', 'Enviar mensaje');
  cardItem.setAttribute('title', decodeEntities(ad.title));
  cardItem.setAttribute('url', ad.url);
  if (ad.price && ad.price > 0 ) {
    cardItem.setAttribute('price', formatStrPrice(ad.price, ad.currency));
  }
  cardItem.setAttribute('adid', ad.id);
  cardItem.setAttribute('theme', 'accent');
  if (ad.images && ad.images.full != undefined) {
    cardItem.setAttribute('image', ad.images.full);
  } else {
    cardItem.setAttribute('image', recommendedAdcallConf.defaultImage)
  }
  return cardItem;
}

function adReplyRecommendedTagging(eventName, recommendedAd, originButton, interaction) {
  var ad = recommendedAd || {};
  var types = {
    s: 'sell',
    k: 'buy',
    u: 'rent',
  }
  var extraData = {
    event_name: eventName,
    data: {
      root: 'Post Reply',
      interaction,
      ad_id: ad.id,
      origin_page: 'Ad View',
      with_button: originButton ? 'yes' : 'no',
      category: ad.categoryDescription,
      ad_type: types[ad.type] || 'sell',
      ad_title: ad.title,
      publisherType: ad.publisherType,
      region: ad.regionDescription,
    },
  };
  var cloneUtagData = typeof utag_data !== 'undefined' ? Object.assign(JSON.parse(JSON.stringify(utag_data))) : {};
  var utagData = Object.assign(cloneUtagData, extraData);
  if (typeof window.utag !== 'undefined') {
    utag.link(utagData);
  } else {
    console.log(utagData);
  }
}

function showRecommendedAdReply(data, category) {
  var id = data.data.id;
  var carouselType = (category == "1220" || category == "1240") ? "post_adreply_inmo" : "default";

  /* show recomended*/
  recommendedAPI.getProAds(id, recommendedAdreplyConf.limit || 6, ',publisherType,type,category,region', carouselType).then(function processData(ads) {
    if (ads && ads.length > 0) {
      var modal = yapoModal.initialize({
        id: 'post-adreply-recommended',
        conf: {
          attrs: {
            title: recommendedAdreplyConf.title || '',
            position: '95',
            overlayClose: 'true',
          },
        },
      });
      var button = createSimilarButton();
      button.addEventListener('click', function showModal() {
        adReplyRecommendedTagging('similar_ads_bubble_button');
        yapoModal.showModal(modal)
      });
      document.querySelector('body').appendChild(button);

      var content = document.createElement('div');
      content.classList.add('modal-recommended')
      ads.forEach((ad) => {
        var cardItem = createAdReplySimilarCardItem(ad);
        cardItem.addEventListener('click-button', function cardItemEvent(event) {
          data.data.id = event.detail.adid;
          sendAdReplyForm(data, function () {
            cardItem.removeAttribute('text-button');
            cardItem.setAttribute('text-label', '\xA1Mensaje enviado!');
            adReplyRecommendedTagging('similar_ads_interaction_button', ad, false ,'AdReply');
          })
        })
        cardItem.addEventListener('click-ad', function redirectCard(event) {
          var url = event.detail.url;

          adReplyRecommendedTagging('similar_ads_go_to_recommended_ad', ad);
          setTimeout(function () {
            location.assign(url);
          }, 800);
        });
        content.appendChild(cardItem);
      });
      modal.addEventListener('closed', function closeModal(event) {
        var origin = event.detail.origin;
        adReplyRecommendedTagging('similar_ads_close', {}, origin === 'close-button')

      })
      modal.appendChild(content);
      adReplyRecommendedTagging('similar_ads_shown');
    }
  });
}

function sendAdReplyForm(data, callback) {
  $.ajax(data.action, {
    type: data.method,
    data: data.data,
    dataType: 'json',
    success: callback,
  });
};

function sendAdReply(form) {
  if (submittingAdReply) {
    return false;
  }
  submittingAdReply = true;
  var elements = ['name','email','adreply_body', 'phone'];
  var $formAdreply = $(form);
  var adReplyBody = $('#adreply_body').val().replace(/\r\n|\r|\n/g, ' ');
  var data = {id: $('#list_id').val(),
    name: $('#yourName').val(),
    email: $('#yourEmail').val(),
    adreply_body: adReplyBody,
    cc: $('#cc').is(':checked') ? 1: 0,
    phone: $('#yourPhone').val()};
  var action = $formAdreply.attr('action');
  var method = $formAdreply.attr('method');
  var formData = {
    data,
    action,
    method,
  };
  sendAdReplyForm(formData,
    function successAjax(data) {
      if (data.status == 'OK') {
        $('#form_response_ok').show();
        $('#form_adreply .modal-page-header a').trigger('click');

        /* add stats */
        var $stats1x1 = $('#stats_1x1');
        if ($stats1x1.length) {
          $stats1x1.attr('src', $stats1x1.attr('src').replace('_vi', '_arajax'));
        }

        document.dispatchEvent(new CustomEvent ('adreply::ok'));
        $('#tealium_mail_submit').trigger('click');

        var adType = document.querySelector('#dataAd').getAttribute('data-adtype');
        var category = document.querySelector('#dataAd').getAttribute('data-category');
        var hasQuota = false;
        if (document.querySelector('da-detail')) {
          var productQuota = document.querySelector('da-detail').getAttribute('product-cuota');
          hasQuota = productQuota && productQuota !== null && productQuota !== '';
        }
        var cats = recommendedAdreplyConf.categories || [];
        var adTypes = recommendedAdreplyConf.adTypes || [];
        var enabled = recommendedAdreplyConf.enabled || 0;
        if (enabled
          && (cats.includes(category) | cats.includes('*'))
          && (adTypes.includes(adType) | adTypes.includes('*'))
          && !hasQuota) {
          showRecommendedAdReply(formData, category);
        }

        /* xiti metrics for ad_reply */
        var xtcustom = $('#send_adreply').val();
        try{
          return xt_click(this, 'F', '', xtcustom);
        } catch(e) {} // eslint-disable-line

      } else {
        $.each(elements, function(index, value){
          var error = 'err_' + value;
          var $input = $('#reply_data [name=' + value + ']');
  
          if (error in data) {
            var $span = $('<span/>', {
              id: $input.attr('id') + '-error',
              class: 'error',
              text: data[error]
            });

            placeError($span, $input);
          }
        });
        submittingAdReply = false;

        try{
          xtcustom = $('#sendTipErrXiti').data('page');
          var xtcustom_var = get_page_custom_variables();
          xt_med('F','','foo&stc=' + xtcustom + '&' + xtcustom_var);
        } catch(e) {} // eslint-disable-line

        document.dispatchEvent(new CustomEvent ('adreply::error'));
        $('#tealium_mail_submit_error').trigger('click');
      }
  });
}
