/**
 * Functions used for methods in new_ai.jquery.js
 *
 */

/**
 *
 * @name preventNotDigits.
 * @description
 *
 * Prevent write `-` `,` `.`, `+` `e` `E` and accent marks.
 *
 * @param {Object} event DOM's event.
 * @return {Boolean} True if event.which isn't a invalid keycode.
 *
 */
function preventNotDigits(event) {
  return [69, 187, 188, 189, 190, 229].indexOf(event.which) === -1;
}

/**
 *
 * @name preventNotDigitsAndPoints
 * @description
 *
 * Prevent write `-` `.`, `+` `e` `E` and accent marks.
 *
 * @param {Object} event DOM's event.
 * @return {Boolean} True if event.which isn't a invalid keycode.
 *
 */
function preventNotDigitsAndPoints(event) {
  if (event.which >= 65 && event.which <= 90)
     return false;
  return [69, 91, 189, 190, 229].indexOf(event.which) === -1;
}

/**
 *
 * @name formatUfPrice
 * @description
 *
 * Set price with decimals
 *
 * @param {Object} event DOM's blur event
 *
 */
function formatUfPrice(element) {
  var el = element;
  var price = el.value;
  var splittedPrice = price.replace(/\./g, '').split(',');
  var convertedPrice = splittedPrice[0];
  convertedPrice = convertedPrice.length
    ? parseInt(convertedPrice, 10).toLocaleString('es-CL')
    : '0';

  convertedPrice += splittedPrice[1] ? ',' + splittedPrice[1] : ''; // has decimmals
  convertedPrice === '0' ? convertedPrice = '' : convertedPrice;
  el.value = price.length ? convertedPrice : '';

  return true;
};


/**
 *
 * @name formatUFwithoutDots
 * @description
 *
 * Set price without dots
 *
 * @param {Object} event DOM's focus event
 *
 */
function formatUFwithoutDots(event) {
  var el = event.currentTarget;
  var price = el.value;
  var formatPrice = price.replace(/\./g, '');

  el.value = formatPrice;

  return true;
};

/**
 *
 * @name priceRequired
 * @description
 *
 * priceRequired validate not Empty price in Inmo Categories (1220 and 1240)
 *
 *
 **/
function priceRequired() {
  var cat = $('#category').children(":selected");

  if (cat.data('reqprice')) {
    return true;
  }

  return false;
};

/**
 *
 * @name tagImageUpload
 * @description
 *
 * Create tag when user upload a image, only the first
 *
 *
 **/
function tagImageUpload() {
  var imageWrapper = document.querySelector('#load_images');
  var addOneImage = false;
  
  if (imageWrapper) { 
    imageWrapper.addEventListener('addImageAdInsert', function(event) {
      if (event.detail.count == '1' && !addOneImage) {
        if (typeof window.utag != 'undefined') {
          window.utag.link({ event_name: 'ad_insert_image' });
        }

        addOneImage = true;
      }
    });
  }
}

/**
 *
 * @name tagInputNewAi
 * @description
 *
 * Create tag when user insert text on input whit class tagInputNewAi
 *
 *
 **/
function tagInputNewAi() {
  var inputsNewAi = Array.prototype.slice.call(document.querySelectorAll('.tagInputNewAi'));
 
  if (inputsNewAi) {
    inputsNewAi.map(element => element.addEventListener("keyup", function(event) {
      var listNamesTag = { subject: 'title', body: 'description', price: 'price'
                          , name: 'name', business_name: 'business_name', phone: 'phone_contact'
                          ,email: 'email', passwd: 'password', passwd_ver: 'verify_password'
                          ,rut: 'rut' };
      var inputValue = event.target.value;
      var inputName = event.target.getAttribute('name');
      var lengthValue = 2;

      if (typeof window.utag != 'undefined' && inputValue.length == lengthValue) {
        window.utag.link({ event_name: 'ad_insert_seller_' + listNamesTag[inputName] });
      };
    })); 
  }
}


/**
 *
 * @name tagInputNewAiClick
 * @description
 *
 * Create tag when user click on radio or check
 *
 *
 **/
function tagNewAiSeller() {
  $('#tagInputNewAiClick input:radio').click(function() {
    sendUtagLink('ad_insert_seller_contact_type');
  });

  $('#tagInputNewAiClick input:checkbox').click(function() {
    sendUtagLink('ad_insert_seller_hidden_phone_contact');
  });

  $('#create_new_account').click(function() {    
    setTimeout(() => {
      if ($(this).find('.check').hasClass('checked')) {
        sendUtagLink('ad_insert_seller_create_account');
      } else {
        sendUtagLink('ad_insert_seller_create_account_uncheck');
      }
    }, 0.1);
  });

  $('#tagInputNewAiClickUsers input:radio').click(function() {
    sendUtagLink('ad_insert_seller_type');
  });
}

/**
 *
 * @name tagNewAiGeoPosition
 * @description
 *
 * Create tag for Geo Position Fields
 *
 *
 **/
function tagNewAiGeoPosition() {
  $('#geolocate_me').click(function () {
    sendUtagLink('ad_insert_seller_location_approximate');
  });

  $('#hst-region').click(function () {
    sendUtagLink('ad_insert_seller_location_region');
  });

  $('#hst-communes').click(function () {
    var regionSelected = $('#region').val();
    var eventName = (regionSelected != '') ? 'ad_insert_seller_location_commune' : 'ad_insert_seller_location_before_region';
    
    sendUtagLink(eventName);
  });

  $('#geoposition').change(function () {
    ($(this).val() != '') ? sendUtagLink('ad_insert_seller_location_approximate_select') : true;
  });

  $('#region').change(function () {
    var geoPos = $('#geoposition').val();

    (geoPos == '' && $(this).val()) ? sendUtagLink('ad_insert_seller_location_region_select') : true;
  });
  
  $('#communes').change(function () {
    var geoPos = $('#geoposition').val();

    (geoPos == '' && $(this).val()) ? sendUtagLink('ad_insert_seller_location_commune_select') : true;
  });
}

/**
 *
 * @name tagNewAiCategories
 * @description
 *
 * Create tag for Categories select
 *
 *
 **/
function tagNewAiCategories() {
  $(document).on('click', '#hst-parent_category', function h() {
    sendUtagLink('ad_insert_category_load');
  });

  $(document).on('click', '#hst-category', function h() {
    sendUtagLink('ad_insert_subcategory_load');
  });

  $('#parent_category').change(function () {
    var cat = $(this).val();

    (cat && cat != '') ? sendUtagLinkCat('ad_insert_category_select', cat) : true;
  });
}

/**
 *
 * @name tagNewAiSubCategories
 * @description
 *
 * Create tag for SubCategories select, is called from new_ai.jquery init fucntion
 *
 *
 **/
function tagNewAiSubCategories(el) {
  var cat = el.val();

  (cat && cat != '') ? sendUtagLinkCat('ad_insert_subcategory_select', cat) : true;
}

/**
 *
 * @name sendUtagLink
 * @description
 *
 * send tag use utag link function
 * 
 * @param {String} name of tag
 **/

function sendUtagLink(name) {
  if (typeof window.utag != 'undefined'  ) {
    window.utag.link({ event_name: name });
  }
}

/**
 *
 * @name sendUtagLinkCat
 * @description
 *
 * send tag use utag link function
 * 
 * @param {String} name of tag
 * 
 * @param {String} category id
 **/

function sendUtagLinkCat(name, category) {
  if (typeof window.utag != 'undefined'  ) {
    window.utag.link({ event_name: name, data: { category } });
  }
}

/**
 *
 * @name tagNewAiCallFunctions
 * @description
 *
 * call tag fucntions 
 * 
 **/

function tagNewAiCallFunctions() {
  tagImageUpload();
  tagInputNewAi();
  tagNewAiSeller();
  tagNewAiGeoPosition();
  tagNewAiCategories();
}

/**
 *
 * @name clickLoginButtonAdinsert
 * @description
 *
 * redirect login
 * 
 **/

function clickLoginButtonAdinsert() {
  sendUtagLink('ad_insert_seller_login_my_account');

  setTimeout( function goLogin () {
    window.location.assign('/login');
  }, 800);
}

/**
 *
 * @name categoryTriggerLoadAccountRegion
 * @description
 *
 * when user select a category trigger autofill if user region exist
 * 
 * @param {number} cat categorie selected
 * 
 **/

function categoryTriggerLoadAccountRegion(cat) {
  var regionSel = document.getElementById('region');
  var communeSel = document.getElementById('communes');
  var accountRegion = regionSel.dataset.selected;
  var catsNotAllowed = regionSel.dataset.cats_not_allowed.split(',');

  if (accountRegion == '' || catsNotAllowed.includes(cat) || regionSel.value != '') {
    return false;
  }

  loadCommuneForRegionSelected(regionSel, communeSel);
}

/**
 *
 * @name loadCommuneForRegionSelected
 * @description
 *
 * load Commune when region is fill from account region
 * 
 * @param {object} regionEl element of region
 * 
 * @param {object} communeEl element of region
 **/

function loadCommuneForRegionSelected(regionEl, communeEl) {
  var regionSelected = (regionEl) ? regionEl.dataset.selected : '';
  var communeSelected = (communeEl) ? communeEl.dataset.selected : '';

  if (regionSelected != '') {
    regionEl.value = regionSelected;
    regionEl.dispatchEvent(new Event('change'));
    updateCommunes(regionSelected);

    if (communeSelected != '') {
      communeEl.value = communeSelected;
      communeEl.dispatchEvent(new Event('change'));
    } else {
      var labelCommunes = document.querySelector('#hst-communes .hs-trigger-link');

      if (labelCommunes) labelCommunes.innerHTML = 'Seleciona una comuna';
    }
  }
}

/**
 *
 * @name preventKeysInput
 * @description
 *
 * prevent keys press for RegExp on innput text
 *
 * @param {string} rExp regular expresion
 *
 * @param {string} id of element to prevent keys
 **/

function preventKeysInput(regEx, id) {
  const nameInput = document.getElementById(id);

  if (nameInput) {
    nameInput.addEventListener('keyup', (e) => {
        nameInput.value = nameInput.value.replace(regEx, '');
    });
  }
}

