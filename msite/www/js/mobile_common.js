/* globals xt_click, appl, globalStorage, Draft */
$(document).ready(function() {
  if ($('.btn-user')[0]) {
    $('.btn-user').dotdotdot({
      watch: 'window',
      wrap: 'letter',
      fallbackToLetter: true
    });
  }
  showDraft();

  if ($.mobile) {
    $.mobile.hideUrlBar = false;
  }

  // cleaning the draft if we are in mobile ai
  $('#draft_wrap').click(function() {
    if (typeof(Draft) !== 'undefined') {
      hideCurrencyNodes();
      Draft.reset();
    }
  });

  // this code hides the draft message, but does not clean the draft
  $('#discard_draft').click(function() {
    setDraftMessageVisibility(false);
    $('#draft_wrap').hide();
  });

  if (typeof dataSplashScreen !== "undefined" && dataSplashScreen.enabled == 1) {
    new SplashScreen ({
      data : dataSplashScreen,
      numberOfImages: dataSplashScreen.numberofimages,
      multipleText: dataSplashScreen.multipletext,
      numberOfText: dataSplashScreen.numberoftext
    });
  }
});

function hideCurrencyNodes() {
  $('.currencyTypes').hide();
  $('#currency_peso').trigger('click');
}

function showDraft() {
  if (isDraftMessageVisible()) {
    $('#draft_wrap').show();
  } else {
    $('#draft_wrap').hide();
  }
}

function showRegions() { // eslint-disable-line
  $('.regions_list').toggleClass('show-region');
  window.scrollTo(0, 0);
  $('#ct:not(.ad-detail_pg)').toggle();
  return false;
}

function isAndroidVersionOrNewer(userAgent, version) { // eslint-disable-line
  var ua = userAgent.toLowerCase();
  var UAHeaderToLook = 'android';
  var versionOfDevice;
  var versionParsed;
  var uaHeaderIndex = ua.indexOf(UAHeaderToLook);

  try {
    if (uaHeaderIndex !== -1) {
      versionOfDevice = ua.slice(
        uaHeaderIndex + UAHeaderToLook.length + 1,
        ua.indexOf(';', uaHeaderIndex)
      );
      versionParsed = parseFloat(versionOfDevice);
      return isNaN(versionParsed) || versionParsed >= parseFloat(version);
    }
  } catch (err) {} // eslint-disable-line
  return false;
}

$(document).ready(function() {
  var nua = navigator.userAgent;
  var isAndroid = nua.indexOf('Mozilla/5.0') > -1 &&
    nua.indexOf('Android ') > -1 && nua.indexOf('AppleWebKit');
  var isWindowsPhone = (
    nua.indexOf('Chrome') > -1 || navigator.userAgent.indexOf('Windows Phone') > -1
  );

  if (isAndroid && !isWindowsPhone) {
    $('body').addClass('android-browser');
  }
});

function try_xtc(name, type, source) { // eslint-disable-line
  try {
    xt_click(source, 'C', '', name, type);
  } catch(e) {} // eslint-disable-line
  return true;
}

function getStorage() {
  var key = 'storage_test';

  if (window.yapoDeviceStorage) {
    return window.yapoDeviceStorage;
  } else if (typeof window.yapoDeviceStorage === 'undefined') {
    // check if the session storage allow us to write and read
    try {
      sessionStorage.setItem(key, key);
      sessionStorage.removeItem(key);
      window.yapoDeviceStorage = 'sessionStorage';
      return window.yapoDeviceStorage;
    } catch (e) { } // eslint-disable-line
    // check if the local storage allow us to write and read
    try {
      localStorage.setItem(key, key);
      localStorage.removeItem(key);
      window.yapoDeviceStorage = 'localStorage';
      return window.yapoDeviceStorage;
    } catch (e) { } // eslint-disable-line
    // check if the global storage allow us to write and read
    try {
      globalStorage.setItem(key, key);
      globalStorage.removeItem(key);
      window.yapoDeviceStorage = 'globalStorage';
      return window.yapoDeviceStorage;
    } catch (e) { } // eslint-disable-line
    window.yapoDeviceStorage = null;
  }
  return window.yapoDeviceStorage;
}

function storageSetItem(key, value) {
  var storage = getStorage();

  if (storage) {
    window[storage].setItem(key, value);
  }
}

function storageGetItem(key) {
  var storage = getStorage();

  if (storage) {
    return window[storage].getItem(key);
  }

  return null;
}

function storageRemoveItem(key) { // eslint-disable-line
  var storage = getStorage();

  if (storage) {
    return window[storage].removeItem(key);
  }

  return null;
}

function isDraftMessageVisible() {
  if (typeof(appl) !== 'undefined' && appl === 'editad') return false;
  return (storageGetItem('yapoDraft.' + location.host + '/mai/form/.showDraft') == '1');
}

function setDraftMessageVisibility(value) {
  var flag = value ? 1 : 0;
  storageSetItem('yapoDraft.' + location.host + '/mai/form/.showDraft', flag);
}

function normalizeWordAccent(str) {
  var from = normalizeStringFrom;
  var to = normalizeStringTo;
  var mapping = {};
  var ret = [];

  for (var i = 0; i < from.length; i++) {
    mapping[from.charAt(i)] = to.charAt(i);
  }

  for (i = 0; i < str.length; i++) {
    var c = str.charAt(i);

    if (mapping.hasOwnProperty(str.charAt(i))) {
      ret.push(mapping[c]);
    } else {
      ret.push(c);
    }
  }

  return ret.join('');
}
