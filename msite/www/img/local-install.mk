TOPDIR?=../..

INSTALLDIR=/www-ssl/img

INSTALL_TARGETS=ajax-loader.gif
INSTALL_TARGETS+=loader-light.gif
INSTALL_TARGETS+=ajax-loader.png
INSTALL_TARGETS+=arrow_darkblue_left.png
INSTALL_TARGETS+=arrow_darkblue_right.png
INSTALL_TARGETS+=bt_inserir-anuncio.png
INSTALL_TARGETS+=bt_localizacao_ddd.png
INSTALL_TARGETS+=bt_localizacao_state.png
INSTALL_TARGETS+=bt_meus-anuncios.png
INSTALL_TARGETS+=icons-18-black.png
INSTALL_TARGETS+=icons-18-white.png
INSTALL_TARGETS+=icons-36-black.png
INSTALL_TARGETS+=icons-36-white.png
INSTALL_TARGETS+=logo_small.png
INSTALL_TARGETS+=logo.png
INSTALL_TARGETS+=morephotos.png
INSTALL_TARGETS+=selectbox_arrow.jpg
INSTALL_TARGETS+=sprite_mobile.png
INSTALL_TARGETS+=sprite_mobile2.png
INSTALL_TARGETS+=morephotos.png
INSTALL_TARGETS+=thumb_40x30.jpg
INSTALL_TARGETS+=thumb_60x45.jpg
INSTALL_TARGETS+=thumb_80x60.jpg
INSTALL_TARGETS+=btn-left.png
INSTALL_TARGETS+=btn-right.png

#bump sprites
INSTALL_TARGETS+=sprite-bump-mobile.png
INSTALL_TARGETS+=sprite-bump-mobile@2x.png

#account mobile
INSTALL_TARGETS+=mobile_accounts.png
INSTALL_TARGETS+=mobile_accounts2.png

include ${TOPDIR}/mk/all.mk
include ${TOPDIR}/mk/install.mk
