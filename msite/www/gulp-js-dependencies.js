/* eslint-env node */

/*
 * Source files needed to build each js file on msite
 */
var jsDependencies = [

  {
    final: 'delete.js',
    sources: [
      'www/bower_components/pgwmodal/pgwmodal.js',
      'www/js/dialog-wrap.js',
      'www/bower_components/radios-to-slider/js/jquery.radios-to-slider.js',
      'msite/www/js/delete_reasons.js'
    ]
  },
  {
    final: 'detail.js',
    sources: [
      'msite/www/js/detail_src.js',
      'msite/www/js/smartphone/mobile.js',
      'www/3rdparty/nokia-maps/leaflet.js',
      'www/3rdparty/nokia-maps/Edit.SimpleShape.js',
      'www/3rdparty/nokia-maps/Edit.Circle.js',
      'www/js/yapo-map.js'
    ]
  },
  {
    final: 'icheck.js',
    sources: ['www/js/jquery.icheck.js']
  },
  {
    final: 'listing.js',
    sources: [
      'www/js/formatPrice.js',
      'www/3rdparty/jquery.lazyload/jquery.lazyload.js',
      'www/3rdparty/bx-slider.js/dist/jquery.bxslider.js',
      'www/js/notifications.js',
      'msite/www/js/listing_src.js',
      'www/js/google-gpt-config.js',
      'www/bower_components/pgwmodal/pgwmodal.js',
      'www/js/search-filter-events.js'
    ]
  },
  {
    final: 'mai_success.js',
    sources: [
      'www/bower_components/jquery-mobile-bower/js/jquery.mobile-1.4.5.js',
      'msite/www/js/adjust_thumbs.js',
      'msite/www/js/mai_success.js'
    ]
  },
  {
    final: 'mobile.js',
    sources: [
      'www/bower_components/jquery/dist/jquery.js',
      'www/bower_components/jquery.dotdotdot/src/js/jquery.dotdotdot.js',
      'www/js/components/splashScreen.js',
      'msite/www/js/mobile_common.js',
      'www/js/pixel.js',
      'msite/www/js/home.js',
      'msite/www/js/smartphone/nexus-fix.js',
      'www/js/formatPrice.js',
      'msite/www/js/jquery.horizontalPageSlide.js',
      'msite/www/js/loginFav.js',
      'node_modules/@Yapo/yapo-tag-manager/lib/yapo-tag-manager.js'
    ]
  },

  // image loading and compression
  {
    final: 'plupload.js',
    sources: [
      // plupload is not available in bower
      'www/3rdparty/plupload-2.1.8/js/moxie.js',
      'www/3rdparty/plupload-2.1.8/js/plupload.dev.js',
      'msite/www/js/smartphone/mpu.plupload.js'
    ]
  },
  {
    final: 'fine-uploader.js',
    sources: [
      'www/bower_components/fine-uploader/client/js/util.js',
      'www/bower_components/fine-uploader/client/js/uploader.basic.api.js',
      'www/bower_components/fine-uploader/client/js/button.js',
      'www/bower_components/fine-uploader/client/js/features.js',
      'www/bower_components/fine-uploader/client/js/upload-data.js',
      'www/bower_components/fine-uploader/client/js/upload-handler/upload.handler.js',
      'www/bower_components/fine-uploader/client/js/upload-handler/upload.handler.controller.js',
      'www/bower_components/fine-uploader/client/js/upload-handler/form.upload.handler.js',
      'www/bower_components/fine-uploader/client/js/upload-handler/xhr.upload.handler.js',
      'www/bower_components/fine-uploader/client/js/uploader.basic.js',
      'www/bower_components/fine-uploader/client/js/traditional/traditional.xhr.upload.handler.js',
      'www/bower_components/fine-uploader/client/js/traditional/traditional.form.upload.handler.js',
      'www/bower_components/fine-uploader/client/js/traditional/all-chunks-done.ajax.requester.js',
      'www/bower_components/fine-uploader/client/js/ajax.requester.js',
      'www/bower_components/fine-uploader/client/js/blob-proxy.js',
      'www/bower_components/fine-uploader/client/js/promise.js',
      'www/bower_components/fine-uploader/client/js/jquery-plugin.js',
      'www/bower_components/fine-uploader/client/js/window.receive.message.js',
      'msite/www/js/smartphone/mpu.fine-uploader.js'
    ]
  },
  {
    final: 'plupload-new.js',
    sources: [
      // plupload is not available in bower
      'www/3rdparty/plupload-2.1.8/js/plupload.full.min.js',
      'msite/www/js/mpu.plupload.js'
    ]
  },
  {
    final: 'fine-uploader-new.js',
    sources: [
      'www/bower_components/fine-uploader/client/js/util.js',
      'www/bower_components/fine-uploader/client/js/uploader.basic.api.js',
      'www/bower_components/fine-uploader/client/js/button.js',
      'www/bower_components/fine-uploader/client/js/features.js',
      'www/bower_components/fine-uploader/client/js/upload-data.js',
      'www/bower_components/fine-uploader/client/js/upload-handler/upload.handler.js',
      'www/bower_components/fine-uploader/client/js/upload-handler/upload.handler.controller.js',
      'www/bower_components/fine-uploader/client/js/upload-handler/form.upload.handler.js',
      'www/bower_components/fine-uploader/client/js/upload-handler/xhr.upload.handler.js',
      'www/bower_components/fine-uploader/client/js/uploader.basic.js',
      'www/bower_components/fine-uploader/client/js/traditional/traditional.xhr.upload.handler.js',
      'www/bower_components/fine-uploader/client/js/traditional/traditional.form.upload.handler.js',
      'www/bower_components/fine-uploader/client/js/traditional/all-chunks-done.ajax.requester.js',
      'www/bower_components/fine-uploader/client/js/ajax.requester.js',
      'www/bower_components/fine-uploader/client/js/blob-proxy.js',
      'www/bower_components/fine-uploader/client/js/promise.js',
      'www/bower_components/fine-uploader/client/js/jquery-plugin.js',
      'www/bower_components/fine-uploader/client/js/window.receive.message.js',
      'msite/www/js/mpu.fine-uploader.js'
    ]
  },
  // Smartphone
  // FIXME: unify with 'normal' js
  {
    final: 'geolocation.js',
    sources: ['msite/www/js/smartphone/geolocation.js']
  },
  {
    final: 'mai.js',
    sources: [
      'www/js/car_subject.js',
      'www/js/format_number.js',
      'msite/www/js/smartphone/nexus-fix.js',
      'www/js/jquery.icheck.js',
      'www/js/dialog-wrap.js',
      'www/bower_components/pgwmodal/pgwmodal.js',
      'www/bower_components/jquery-cookie/jquery.cookie.js',
      'www/bower_components/phoenix/jquery.phoenix.js',
      'msite/www/js/smartphone/draft_handler.js',
      'msite/www/js/smartphone/mai.js',
      'msite/www/js/smartphone/mpu.js',
      'www/js/format_number.js',
      'www/js/ai_map_setup.js',
      'www/js/jquery-ui-1.9.1.js',
      'www/3rdparty/nokia-maps/leaflet.js',
      'www/3rdparty/nokia-maps/Edit.SimpleShape.js',
      'www/3rdparty/nokia-maps/Edit.Circle.js',
      'www/js/yapo-map.js',
      'www/3rdparty/waituntilexists/waitUntilExists.js',
      'www/bower_components/doT/doT.js',
      'www/js/duplicated.js',
      'msite/www/js/smartphone/duplicated_mobile.js',
      'www/js/moment.js'
    ]
  },
  {
    final: 'smartphone_mobile.js',
    sources: [
      'msite/www/js/smartphone/mobile.js',
      'www/js/pixel.js'
    ]
  },
  {
    final: 'mpu.js',
    sources: ['msite/www/js/smartphone/mpu.js']
  },
  {
    final: 'pop_navigation_message.js',
    sources: ['msite/www/js/smartphone/pop_navigation_message.js']
  },

  // Thirdparty (should be replaced with bower_components)
  // FIXME: replace with bower components and remove direct requests for this files
  {
    final: 'bootstrap.min.js',
    sources: ['msite/www/js/thirdparty/bootstrap.min.js']
  },
  {
    final: 'jquery.cookie.js',
    sources: ['msite/www/js/thirdparty/jquery.cookie.js']
  },
  {
    final: 'jquery.easing.min.js',
    sources: ['msite/www/js/thirdparty/jquery.easing.min.js']
  },
  {
    final: 'jquery.mobilesite.min.js',
    sources: [
      'www/bower_components/jquery/dist/jquery.js',
      'www/bower_components/jquery.dotdotdot/src/js/jquery.dotdotdot.js',
      'msite/www/js/mobile_common.js'
    ]
  },
  {
    final: 'new_ai.js',
    sources: [
      'www/js/common.js',
      'www/js/aiUpsellingProducts.js',
      'msite/www/js/components/aiUpselling.js',
      'www/js/settings.js',
      'www/bower_components/jquery-mobile-bower/js/jquery.mobile-1.4.5.js',
      'www/bower_components/jquery.validation/dist/jquery.validate.js',
      'www/bower_components/phoenix/jquery.phoenix.js',
      'msite/www/js/jquery.horizontalSelect.js',
      'www/3rdparty/jquery.mdTextField/jquery.mdTextField.js',
      'www/js/jquery-rut.js',
      'msite/www/js/new_ai.functions.js',
      'msite/www/js/new_ai.jquery.js',
      'msite/www/js/mpu.plupload.js',
      'msite/www/js/mpu.js',
      'msite/www/js/new_ai.categories.js',
      'msite/www/js/new_ai.cats.js',
      'msite/www/js/new_ai.config.js',
      'msite/www/js/new_ai.draft.js',
      'msite/www/js/new_ai.truquero.js',
      'www/bower_components/doT/doT.js',
      'www/js/duplicated.js',
      'msite/www/js/smartphone/duplicated_mobile.js',
      'www/js/moment.js',
      'www/js/jquery.icheck.js',
      'www/js/components/phoneInput.js',
      'www/js/jobAdvices.js',
      'msite/www/js/new_ai.forced_login.js',
      'www/js/ai_car_info_common.js',
      'msite/www/js/ai_car_info_load.js'
    ]
  },
  {
    final: 'daview.js',
    sources: [
      'www/js/ad_view_common.js',
      'www/js/gtm_controller.js',
      'msite/www/js/smartphone/mobile.js',
      'www/bower_components/jquery-mobile-bower/js/jquery.mobile-1.4.5.js',
      'www/bower_components/jquery.validation/dist/jquery.validate.js',
      'msite/www/js/thirdparty/owl.carousel.js',
      'msite/www/js/thirdparty/photoswipe.js',
      'msite/www/js/thirdparty/photoswipe_default.js',
      'www/js/suggested_questions.js',
      'www/3rdparty/nokia-maps/leaflet.js',
      'www/3rdparty/nokia-maps/Edit.SimpleShape.js',
      'www/3rdparty/nokia-maps/Edit.Circle.js',
      'www/js/yapo-map.js',
      'www/3rdparty/jquery.mdTextField/jquery.mdTextField.js',
      'msite/www/js/components/yapoModal.js',
      'msite/www/js/components/recommendedAPI.js',
      'msite/www/js/components/adCall.js',
      'msite/www/js/adview_src.js',
      'www/js/modalListener.js',
      'www/js/google-gpt-config.js',
      'www/js/cotiza-banners-google.js',
      'msite/www/js/components/quotaFinance.js'
    ]
  },
  {
    final: 'wc-info-box.js',
    sources: [
      'node_modules/@altiro-widgets/information-box/dist/info-box.bundled.js'
    ]
  }
];

module.exports = jsDependencies;

