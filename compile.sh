#!/bin/sh

if [ -f /etc/redhat-release ] && [ -z "$NO_SCL" ] && [ -f /usr/bin/scl ] && ! scl_enabled devtoolset-2; then
	exec scl enable devtoolset-2 "$0 $*"
fi

BUILDVERSION=`./platform/scripts/build/buildversion.sh`
OBJDIR=ninja_build
BUILDBUILD=`/bin/pwd`/platform/scripts/build/build-build.pl

if [ -z "$FLAVOR" ] ; then
	FLAVOR=regress
fi

BUILDBUILD="$BUILDBUILD --with-flavor=$FLAVOR"
# hack 
rm -f $OBJDIR/build.ninja

if [ ! -f $OBJDIR/build.ninja ] ; then
	$BUILDBUILD
fi

if [ "$BUILDVERSION" != "`cat $OBJDIR/version 2>/dev/null`" ] ; then
	echo $BUILDVERSION > $OBJDIR/version
fi

#BEGIN_ZANE
STATIC_FILES_BUILD_VERSION=`./platform/scripts/build/buildversion.sh`
echo $STATIC_FILES_BUILD_VERSION > $OBJDIR/short_version;
export STATIC_FILES_BUILD_VERSION=${STATIC_FILES_BUILD_VERSION}
#END_ZANE

ninja -f $OBJDIR/build.ninja $*

if [ $? -eq 1 ]; then
	ninja -n -f $OBJDIR/build.ninja $* >/dev/null 2>&1
	if [ $? = 1 ] ; then
		echo "Probable mismatch between ninja files and build env. Forcing rebuild of ninja files and retrying."
		$BUILDBUILD
		ninja -f $OBJDIR/build.ninja $*
	else
		false
	fi
fi

if [ $? != "0" ] ; then
	exit 1
fi

if [ -n "$RUNTESTS" -a -e $OBJDIR/regress/bin/regress-runner ] ; then
	mkdir -p $OBJDIR/regress/tests
	$OBJDIR/regress/bin/regress-runner --outdir $OBJDIR/regress/tests
fi

