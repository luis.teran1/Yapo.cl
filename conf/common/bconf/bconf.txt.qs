# All search variables in query string
#*.*.qs.vars.awe.regex=^[0-9]{4}:[0-9]{1,2}$
#*.*.qs.vars.aws.regex=^[0-9]{4}:[0-9]{1,2}$
*.*.qs.vars.be.regex=^[0-9]{1,2}$
*.*.qs.vars.blf.regex=^[0-9]{1,2}$
*.*.qs.vars.blm.regex=^[0-9]{1,2}$
*.*.qs.vars.bs.regex=^[0-9]{1,2}$
*.*.qs.vars.c.regex=^[0-9]{4}$
*.*.qs.vars.ca.regex=^[0-9]{1,2}_([0-9]{1,2}_)?[a-z]$
*.*.qs.vars.cg.regex=^[0-9]{4}$
*.*.qs.vars.ci.regex=^[0-9]{1,2}$
*.*.qs.vars.ck.regex=^[0-9]{1,3}$
*.*.qs.vars.co.regex=^[0-9]{0,3}$
*.*.qs.vars.cs.regex=^[0-9]$
*.*.qs.vars.csz.regex=^[0-9]+$
*.*.qs.vars.f.regex=^[cpa]$
*.*.qs.vars.f.ignore_in_watch_query_regex=^[a]$
*.*.qs.vars.fk.regex=^1$
*.*.qs.vars.fu.regex=^[0-9]$
*.*.qs.vars.gb.regex=^[0-9]$
*.*.qs.vars.id.regex=^[0-9]{1,10}$
*.*.qs.vars.last.regex=^1$
*.*.qs.vars.first.regex=^1$
*.*.qs.vars.ls.regex=^1$
*.*.qs.vars.m.regex=^[0-9]{1,3}$
*.*.qs.vars.mas.regex=^[0-9]{1,2}$
*.*.qs.vars.me.regex=^[0-9]{1,6}$
*.*.qs.vars.mre.regex=^[0-9]{1,2}$
*.*.qs.vars.ms.regex=^[0-9]{1,6}$
*.*.qs.vars.next.regex=^1$
*.*.qs.vars.next_error.regex=^1$
*.*.qs.vars.prev.regex=^1$
*.*.qs.vars.prev_error.regex=^1$
*.*.qs.vars.o.regex=^[0-9]*$
*.*.qs.vars.pe.regex=^[0-9]{1,2}$
*.*.qs.vars.ps.regex=^[0-9]{1,2}$
*.*.qs.vars.q.match.q_is_id.regex=^id:[0-9]+(,[0-9]+)*$
*.*.qs.vars.q.regex=^[\x20-\xff]{1,500}$
*.*.qs.vars.roe.regex=^[0-9]{1,2}$
*.*.qs.vars.ros.regex=^[0-9]{1,2}$
*.*.qs.vars.rs.regex=^[0-9]{4}$
*.*.qs.vars.re.regex=^[0-9]{4}$
*.*.qs.vars.s.regex=^1$
*.*.qs.vars.sa.regex=^[0-9]{1,2}$
*.*.qs.vars.se.regex=^[0-9]{1,2}$
*.*.qs.vars.sp.regex=^([012]|price_asc|price_desc|date_asc|date_desc)$
*.*.qs.vars.ss.regex=^[0-9]{1,2}$
*.*.qs.vars.st.regex=^[askubh]$
*.*.qs.vars.th.regex=^[01]$
*.*.qs.vars.th.ignore_in_watch_query_regex=^[01]$
*.*.qs.vars.md.regex=^(th|li|mp)$
*.*.qs.vars.md.ignore_in_watch_query_regex=^(th|li|mp)$
*.*.qs.vars.mv.regex=^[01]$
*.*.qs.vars.v.regex=^1$
*.*.qs.vars.w.regex=^([0-3]|1[0-9]{2})$
*.*.qs.vars.wid.regex=^[0-9]+$
*.*.qs.vars.wq0.regex=.
*.*.qs.vars.wq1.regex=.
*.*.qs.vars.wq2.regex=.
*.*.qs.vars.wq3.regex=.
*.*.qs.vars.wq4.regex=.
*.*.qs.vars.wq5.regex=.
*.*.qs.vars.wq6.regex=.
#*.*.qs.vars.wre.regex=^[0-9]{1,2}$
*.*.qs.vars.xc.regex=^[0-9]{4}$
*.*.qs.vars.ql.regex=^(es)$
*.*.qs.vars.ql.multiple=1
*.*.qs.vars.ctp.regex=^[0-9]$
*.*.qs.vars.ccs.regex=^[0-9]{0,5}$
*.*.qs.vars.cce.regex=^[0-9]{0,5}$
*.*.qs.vars.gend.regex=^[0-9]$
*.*.qs.vars.cond.regex=^[0-9]$
*.*.qs.vars.jc.regex=^[0-9]{1,2}(,[0-9]{1,2})*$
*.*.qs.vars.jc.multiple=1
*.*.qs.vars.sct.regex=^[0-9]{1,2}(,[0-9]{1,2})*$
*.*.qs.vars.sct.multiple=1
*.*.qs.vars.zn.regex=^[0-9]{1,3}$
*.*.qs.vars.cmn.regex=^[0-9]{1,3}(,[0-9]{1,3})*$
*.*.qs.vars.cmn.multiple=1
*.*.qs.vars.vns.regex=^[0-9]{1}$

*.*.qs.vars.br.regex=^[0-9]{1,4}$
*.*.qs.vars.mo.regex=^[0-9]{1,4}$
*.*.qs.vars.gs.regex=^[0-9]$

*.*.qs.vars.ret.regex=^[0-9]{1,4}$
*.*.qs.vars.bre.regex=^[0-9]{1,2}$
*.*.qs.vars.brs.regex=^[0-9]{1,2}$
*.*.qs.vars.coe.regex=^[0-9]{1,2}$
*.*.qs.vars.cos.regex=^[0-9]{1,2}$

*.*.qs.vars.fwgend.regex=^[0-9]$
*.*.qs.vars.fwtype.regex=^[0-9](,[0-9])*$
*.*.qs.vars.fwtype.multiple=1
*.*.qs.vars.fwsize.regex=^[0-9]{1,4}$
*.*.qs.vars.price_range.regex=^([0-9]{0,10}-[0-9]{0,10})$
*.*.qs.vars.rooms_range.regex=^([1-3]{0,1}-[1-4]{0,1})$
*.*.qs.vars.regdate_range.regex=^((\d{4})?-(\d{4})?)$
*.*.qs.vars.mileage_range.regex=^(\d{0,10}-\d{0,10})$
*.*.qs.vars.size_range.regex=^(\d{0,6}-\d{0,6})$
*.*.qs.vars.condominio_range.regex=^(\d{0,6}-\d{0,6})$
*.*.qs.vars.bathroom_range.regex=^(\d{0,1}-\d{0,1})$
*.*.qs.vars.ccm_range.regex=^(\d{0,4}-\d{0,4})$

#
# Offset parsing of query string
#
*.*.qs.settings.offset.1.keys.1=?q_is_id
*.*.qs.settings.offset.1.true.value=value:0
*.*.qs.settings.offset.2.keys.1=?o
*.*.qs.settings.offset.2.true.value=qs:o
*.*.qs.settings.offset.3.default=value:0

#
# Limit
#

*.*.qs.settings.limit.1.keys.1=md
*.*.qs.settings.limit.1.li.value=value:100

*.*.qs.settings.limit.2.default=value:50


#
# Count level queries, should maybe be merged with filter, but we depend on it being
# parsed before both filter and params, to get count_filter and count_filter_key.
#
*.*.qs.settings.count_level.1.keys.1=appl
*.*.qs.settings.count_level.1.aw.value=none
*.*.qs.settings.count_level.1.sms.value=none

# Single store serch
*.*.qs.settings.count_level.2.keys.1=?id
*.*.qs.settings.count_level.2.true.value=none

# id: in query.
*.*.qs.settings.count_level.3.keys.1=?q_is_id
*.*.qs.settings.count_level.3.true.value=none

##
# Search engine filters for the search tabs.
#

# Price sorting

*.*.qs.settings.filter.1.keys.1=appl
*.*.qs.settings.filter.1.keys.2=sp
*.*.qs.settings.filter.1.keys.3=type
*.*.qs.settings.filter.1.aw.*.*.value=none
*.*.qs.settings.filter.1.sms.*.*.value=none
*.*.qs.settings.filter.1.keys.4==cg==7040
*.*.qs.settings.filter.1.aw.*.*.*.value=none
*.*.qs.settings.filter.1.sms.*.*.*.value=none
*.*.qs.settings.filter.1.*.1.s.false.value=value: subsort\:+
*.*.qs.settings.filter.1.*.1.h.false.value=value: subsort\:+
*.*.qs.settings.filter.1.*.1.u.false.value=value: subsort\:+
*.*.qs.settings.filter.1.*.1.k.false.value=value: subsort\:+
*.*.qs.settings.filter.1.*.1.a.false.value=value: subsort\:+

*.*.qs.settings.filter.1.*.price_desc.*.false.value=value: subsort\:-
*.*.qs.settings.filter.1.*.price_asc.*.false.value=value: subsort\:+
*.*.qs.settings.filter.1.*.date_desc.*.false.value=value: sort\:-
*.*.qs.settings.filter.1.*.date_asc.*.false.value=value: sort\:+
*.*.qs.settings.filter.1.continue=1

# Single store search.
*.*.qs.settings.filter.2.keys.1=?id
*.*.qs.settings.filter.2.true.1.value=none
*.*.qs.settings.filter.2.continue=0

# id: in q arg.
*.*.qs.settings.filter.3.keys.1=?q_is_id
*.*.qs.settings.filter.3.true.value=value: count(c_tab)\:company_ad\:1
*.*.qs.settings.filter.3.continue=0

# Choosen tab
*.*.qs.settings.filter.4.keys.1=appl
*.*.qs.settings.filter.4.keys.2=w
*.*.qs.settings.filter.4.keys.3=?count_filter
*.*.qs.settings.filter.4.keys.4=?id
*.*.qs.settings.filter.4.aw.*.*.*.value=none
*.*.qs.settings.filter.4.*.1.false.false.value=value: count_all(region_tab)
#*.*.qs.settings.filter.4.*.2.false.false.value=value: filter\:near\:,qs:region,value: count(near_tab)\:near\:,qs:region,value: count(region_tab)\:region\:,qs:region,value: count(country_tab)\:region\:-
*.*.qs.settings.filter.4.*.3.false.false.value=value: count(region_tab)\:region\:,qs:region,value: count_all(country_tab)
# I have no idea if this case is possible - Zane
*.*.qs.settings.filter.4.*.*.false.false.value=value: filter\:region\:,qs:region,value: count(region_tab)\:region\:,qs:region,value: count(country_tab)\:region\:-
*.*.qs.settings.filter.4.continue=1

# Other categories, no choosen tab.
#*.*.qs.settings.filter.5.keys.1=appl
#*.*.qs.settings.filter.5.keys.2=?count_filter
#*.*.qs.settings.filter.5.aw.*.value=none
#*.*.qs.settings.filter.5.sms.*.value=none
#*.*.qs.settings.filter.5.*.false.value=value: count(c_tab)\:company_ad\:1
#*.*.qs.settings.filter.5.*.true.value=value: count(c_tab)\:company_ad\:1;,implode:count_filter\:;
#*.*.qs.settings.filter.5.continue=0

*.*.qs.settings.filter.5.keys.1=appl
*.*.qs.settings.filter.5.keys.2=f
*.*.qs.settings.filter.5.keys.3=?count_filter
*.*.qs.settings.filter.5.*.p.false.value=value: filter\:company_ad\:0
*.*.qs.settings.filter.5.*.c.false.value=value: filter\:company_ad\:1
*.*.qs.settings.filter.5.continue=1

# company / private count
*.*.qs.settings.filter.7.keys.1=appl
*.*.qs.settings.filter.7.keys.2=w
*.*.qs.settings.filter.7.aw.*.value=none
*.*.qs.settings.filter.7.*.1.value=value: count(c_company)\:company_ad\:1
#*.*.qs.settings.filter.7.*.2.value=value: count(c_company)\:company_ad\:1\;near\:,qs:region
*.*.qs.settings.filter.7.*.3.value=value: count(c_company)\:company_ad\:1
*.*.qs.settings.filter.7.continue=1

# Unfiltered counter
*.*.qs.settings.unfiltered.1.keys.1=?count_filter

# Unfiltered counter
*.*.qs.settings.unfiltered.1.keys.1=?count_filter
*.*.qs.settings.unfiltered.1.true.value=value: count(unfiltered)\:,implode:count_filter\:;
*.*.qs.settings.unfiltered.1.false.value=value: count_all(unfiltered)

#
# Parameters
#

# Single store search short cicuits the params and searches for the store id.
*.*.qs.settings.params.1.keys.1=?id
*.*.qs.settings.params.1.keys.2=sp
*.*.qs.settings.params.1.true.1.value=value: suborder\:1-
*.*.qs.settings.params.1.continue=1

*.*.qs.settings.params.2.keys.1=?id
*.*.qs.settings.params.2.true.value=value: store\:,qs:id

# Make sure params is empty for company list.
*.*.qs.settings.params.3.keys.1=fk
*.*.qs.settings.params.3.1.value=none

# q_is_id short cicuits the params and searches for the id in q.
*.*.qs.settings.params.4.keys.1=?q_is_id
*.*.qs.settings.params.4.true.value=qs:q

# Regdate
*.*.qs.settings.params.5.keys.1=?rs
*.*.qs.settings.params.5.keys.2=?re
*.*.qs.settings.params.5.keys.3=cg
*.*.qs.settings.params.5.true.true.2020.value=value: regdate\:,qs:rs,value:-,qs:re
*.*.qs.settings.params.5.true.false.2020.value=value: regdate\:,qs:rs,value:-
*.*.qs.settings.params.5.false.true.2020.value=value: regdate\:-,qs:re
*.*.qs.settings.params.5.true.true.2040.value=value: regdate\:,qs:rs,value:-,qs:re
*.*.qs.settings.params.5.true.false.2040.value=value: regdate\:,qs:rs,value:-
*.*.qs.settings.params.5.false.true.2040.value=value: regdate\:-,qs:re
*.*.qs.settings.params.5.true.true.2060.value=value: regdate\:,qs:rs,value:-,qs:re
*.*.qs.settings.params.5.true.false.2060.value=value: regdate\:,qs:rs,value:-
*.*.qs.settings.params.5.false.true.2060.value=value: regdate\:-,qs:re
*.*.qs.settings.params.5.continue=1

# Mileage
*.*.qs.settings.params.6.keys.1=?ms
*.*.qs.settings.params.6.keys.2=?me
*.*.qs.settings.params.6.keys.3=cg
*.*.qs.settings.params.6.true.true.2020.value=value: mileage\:,qs:ms,value:-,qs:me
*.*.qs.settings.params.6.true.false.2020.value=value: mileage\:,qs:ms,value:-
*.*.qs.settings.params.6.false.true.2020.value=value: mileage\:-,qs:me

*.*.qs.settings.params.6.true.true.2040.value=value: mileage\:,qs:ms,value:-,qs:me
*.*.qs.settings.params.6.true.false.2040.value=value: mileage\:,qs:ms,value:-
*.*.qs.settings.params.6.false.true.2040.value=value: mileage\:,value:-,qs:me

*.*.qs.settings.params.6.true.true.2060.value=value: mileage\:,qs:ms,value:-,qs:me
*.*.qs.settings.params.6.true.false.2060.value=value: mileage\:,qs:ms,value:-
*.*.qs.settings.params.6.false.true.2060.value=value: mileage\:-,qs:me
*.*.qs.settings.params.6.continue=1

# Monthly rent
*.*.qs.settings.params.7.keys.1=?mre
*.*.qs.settings.params.7.keys.2=cg
*.*.qs.settings.params.7.keys.3=type
*.*.qs.settings.params.7.true.*.u.value=value: suborder\:1-,map:monthly_rent\:mre
*.*.qs.settings.params.7.true.*.h.value=value: suborder\:1-,map:monthly_rent\:mre
*.*.qs.settings.params.7.continue=1

# Beds
*.*.qs.settings.params.11.keys.1=?bs
*.*.qs.settings.params.11.keys.2=?be
*.*.qs.settings.params.11.keys.3=cg
*.*.qs.settings.params.11.keys.4=type
*.*.qs.settings.params.11.true.true.1080.*.value=value: beds\:,qs:bs,value:-,qs:be
*.*.qs.settings.params.11.true.false.1080.*.value=value: beds\:,qs:bs,value:-
*.*.qs.settings.params.11.false.true.1080.*.value=value: beds\:,value:-,qs:be
*.*.qs.settings.params.11.continue=1

# Rooms
*.*.qs.settings.params.13.keys.1=?ros
*.*.qs.settings.params.13.keys.2=?roe
*.*.qs.settings.params.13.keys.3=cg
*.*.qs.settings.params.13.true.true.1220.value=value: rooms\:,map:ros\:ros,value:-,map:roe\:roe
*.*.qs.settings.params.13.true.true.1240.value=value: rooms\:,map:ros\:ros,value:-,map:roe\:roe
*.*.qs.settings.params.13.true.true.1260.value=value: rooms\:,map:ros\:ros,value:-,map:roe\:roe

*.*.qs.settings.params.13.true.false.1220.value=value: rooms\:,map:ros\:ros,value:-
*.*.qs.settings.params.13.true.false.1240.value=value: rooms\:,map:ros\:ros,value:-
*.*.qs.settings.params.13.true.false.1260.value=value: rooms\:,map:ros\:ros,value:-

*.*.qs.settings.params.13.false.true.1220.value=value: rooms\:-,map:roe\:roe
*.*.qs.settings.params.13.false.true.1240.value=value: rooms\:-,map:roe\:roe
*.*.qs.settings.params.13.false.true.1260.value=value: rooms\:-,map:roe\:roe

*.*.qs.settings.params.13.continue=1

# Size
*.*.qs.settings.params.14.keys.1=?ss
*.*.qs.settings.params.14.keys.2=?se
*.*.qs.settings.params.14.keys.3=cg
*.*.qs.settings.params.14.keys.4=ret

*.*.qs.settings.params.14.true.true.1220.5.value=value: size\:,map:size_2\:ss,value:-,map:size_2\:se
*.*.qs.settings.params.14.true.true.1220.6.value=value: size\:,map:size_1\:ss,value:-,map:size_1\:se
*.*.qs.settings.params.14.true.true.1220.*.value=value: size\:,map:size_0\:ss,value:-,map:size_0\:se
*.*.qs.settings.params.14.true.false.1220.5.value=value: size\:,map:size_2\:ss,value:-
*.*.qs.settings.params.14.true.false.1220.6.value=value: size\:,map:size_1\:ss,value:-
*.*.qs.settings.params.14.true.false.1220.*.value=value: size\:,map:size_0\:ss,value:-
*.*.qs.settings.params.14.false.true.1220.5.value=value: size\:-,map:size_2\:se
*.*.qs.settings.params.14.false.true.1220.6.value=value: size\:-,map:size_1\:se
*.*.qs.settings.params.14.false.true.1220.*.value=value: size\:-,map:size_0\:se

*.*.qs.settings.params.14.true.true.1240.5.value=value: size\:,map:size_2\:ss,value:-,map:size_2\:se
*.*.qs.settings.params.14.true.true.1240.6.value=value: size\:,map:size_1\:ss,value:-,map:size_1\:se
*.*.qs.settings.params.14.true.true.1240.*.value=value: size\:,map:size_0\:ss,value:-,map:size_0\:se
*.*.qs.settings.params.14.true.false.1240.5.value=value: size\:,map:size_2\:ss,value:-
*.*.qs.settings.params.14.true.false.1240.6.value=value: size\:,map:size_1\:ss,value:-
*.*.qs.settings.params.14.true.false.1240.*.value=value: size\:,map:size_0\:ss,value:-
*.*.qs.settings.params.14.false.true.1240.5.value=value: size\:-,map:size_2\:se
*.*.qs.settings.params.14.false.true.1240.6.value=value: size\:-,map:size_1\:se
*.*.qs.settings.params.14.false.true.1240.*.value=value: size\:-,map:size_0\:se
# On category 1260 the user can't filter by size

*.*.qs.settings.params.14.continue=1


# Bathrooms
*.*.qs.settings.params.15.keys.1=?brs
*.*.qs.settings.params.15.keys.2=?bre
*.*.qs.settings.params.15.keys.3=cg
*.*.qs.settings.params.15.true.true.1220.value=value: bathrooms\:,map:brs\:brs,value:-,map:bre\:bre
*.*.qs.settings.params.15.true.true.1240.value=value: bathrooms\:,map:brs\:brs,value:-,map:bre\:bre
*.*.qs.settings.params.15.true.true.1260.value=value: bathrooms\:,map:brs\:brs,value:-,map:bre\:bre

*.*.qs.settings.params.15.true.false.1220.value=value: bathrooms\:,map:brs\:brs,value:-
*.*.qs.settings.params.15.true.false.1240.value=value: bathrooms\:,map:brs\:brs,value:-
*.*.qs.settings.params.15.true.false.1260.value=value: bathrooms\:,map:brs\:brs,value:-

*.*.qs.settings.params.15.false.true.1220.value=value: bathrooms\:-,map:bre\:bre
*.*.qs.settings.params.15.false.true.1240.value=value: bathrooms\:-,map:bre\:bre
*.*.qs.settings.params.15.false.true.1260.value=value: bathrooms\:-,map:bre\:bre

*.*.qs.settings.params.15.continue=1

# Municipality (skipped if cl filter is on this level)
#*.*.qs.settings.params.15.keys.1=?m
#*.*.qs.settings.params.15.keys.2=any:count_filter_key==munic
#*.*.qs.settings.params.15.true.false.value=value: munic\:,qs:m
#*.*.qs.settings.params.15.continue=1

# Subarea (skipped if cl filter is on this level)
#*.*.qs.settings.params.16.keys.1=?m
#*.*.qs.settings.params.16.keys.2=?sa
#*.*.qs.settings.params.16.keys.3=cg
#*.*.qs.settings.params.16.keys.4=any:count_filter_key==subarea
#*.*.qs.settings.params.16.true.true.3000.false.value=value: subarea\:,qs:sa
#*.*.qs.settings.params.16.true.true.3020.false.value=value: subarea\:,qs:sa
#*.*.qs.settings.params.16.true.true.3040.false.value=value: subarea\:,qs:sa
#*.*.qs.settings.params.16.true.true.3060.false.value=value: subarea\:,qs:sa
#*.*.qs.settings.params.16.true.true.3100.false.value=value: subarea\:,qs:sa
#*.*.qs.settings.params.16.true.true.7060.false.value=value: subarea\:,qs:sa
#*.*.qs.settings.params.16.continue=1

# Country
#*.*.qs.settings.params.17.keys.1=?co
#*.*.qs.settings.params.17.keys.2==co==
#*.*.qs.settings.params.17.keys.3=cg
#*.*.qs.settings.params.17.true.false.3080.value=value: country\:,qs:co
#*.*.qs.settings.params.17.continue=1

# Gearbox
*.*.qs.settings.params.18.keys.1=?gb
*.*.qs.settings.params.18.keys.2=cg
*.*.qs.settings.params.18.true.2020.value=value: gearbox\:,qs:gb
*.*.qs.settings.params.18.true.2040.value=value: gearbox\:,qs:gb
*.*.qs.settings.params.18.continue=1

# Fuel
*.*.qs.settings.params.19.keys.1=?fu
*.*.qs.settings.params.19.keys.2=cg
*.*.qs.settings.params.19.keys.3=type
*.*.qs.settings.params.19.true.2020.*.value=value: fuel\:,qs:fu
*.*.qs.settings.params.19.true.2040.*.value=value: fuel\:,qs:fu
*.*.qs.settings.params.19.continue=1

# Region and type (sipped if cl filter is on region level or if w is 3)
# First handle if subtype is set, some categories should search only in sell and rent when subtype all is choosen.
*.*.qs.settings.params.23.keys.1=any:count_filter_key==region
*.*.qs.settings.params.23.keys.2=?st
*.*.qs.settings.params.23.keys.3=w
*.*.qs.settings.params.23.keys.4=cg
*.*.qs.settings.params.23.keys.5=st
# Geographic categories
#*.*.qs.settings.params.23.false.true.*.*.s.value=value: region_type\:,qs:region,value:_s\,,qs:region,value:_k
#*.*.qs.settings.params.23.false.true.*.*.u.value=value: region_type\:,qs:region,value:_u\,,qs:region,value:_h
#*.*.qs.settings.params.23.false.true.*.*.a.value=value: region\:,qs:region
#*.*.qs.settings.params.23.false.true.2.*.s.value=value: near_type\:,qs:region,value:_s\,,qs:region,value:_k
#*.*.qs.settings.params.23.false.true.2.*.u.value=value: near_type\:,qs:region,value:_u\,,qs:region,value:_h
#*.*.qs.settings.params.23.false.true.2.*.a.value=value: near\:,qs:region

# Vehicles with rent subtype (all -> sell and rent)
#*.*.qs.settings.params.23.false.true.*.1060.false.value=value: region_type\:,qs:region,value:_,qs:st
#*.*.qs.settings.params.23.false.true.*.1060.true.value=value: region_type\:,qs:region,value:_s\,,qs:region,value:_u
#*.*.qs.settings.params.23.false.true.*.1100.false.value=value: region_type\:,qs:region,value:_,qs:st
#*.*.qs.settings.params.23.false.true.*.1100.true.value=value: region_type\:,qs:region,value:_s\,,qs:region,value:_u
#*.*.qs.settings.params.23.false.true.2.1060.false.value=value: near_type\:,qs:region,value:_,qs:st
#*.*.qs.settings.params.23.false.true.2.1060.true.value=value: near_type\:,qs:region,value:_s\,,qs:region,value:_u
#*.*.qs.settings.params.23.false.true.2.1100.false.value=value: near_type\:,qs:region,value:_,qs:st
#*.*.qs.settings.params.23.false.true.2.1100.true.value=value: near_type\:,qs:region,value:_s\,,qs:region,value:_u
*.*.qs.settings.params.23.false.true.3.*.*.value=none
*.*.qs.settings.params.23.continue=1

# No subtype set
*.*.qs.settings.params.24.keys.1=any:count_filter_key==region
*.*.qs.settings.params.24.keys.2=w
*.*.qs.settings.params.24.keys.3=?cg
*.*.qs.settings.params.24.keys.4=type
*.*.qs.settings.params.24.keys.5=?id
*.*.qs.settings.params.24.false.1.*.*.true.value=value: region\:,qs:region
#*.*.qs.settings.params.24.false.2.*.*.true.value=value: near\:,qs:region
*.*.qs.settings.params.24.false.3.*.*.*.value=none
*.*.qs.settings.params.24.continue=1

# no category (implies ignoring subtype)
#*.*.qs.settings.params.25.keys.1=any:count_filter_key==region
#*.*.qs.settings.params.25.keys.2=w
#*.*.qs.settings.params.25.keys.3=?cg
#*.*.qs.settings.params.25.keys.4=type
#*.*.qs.settings.params.25.false.*.false.*.value=value: type\:s\,k\,h\,u\,b
#*.*.qs.settings.params.25.false.2.false.*.value=value: near\:,qs:region
#*.*.qs.settings.params.25.false.3.*.*.value=none
#*.*.qs.settings.params.25.continue=1

# City
# XXX The ci param should really only apply to the correct regions.
#*.*.qs.settings.params.26.keys.1=w
#*.*.qs.settings.params.26.keys.2==cg==3080
#*.*.qs.settings.params.26.keys.3=?ci
#*.*.qs.settings.params.26.keys.4=?city
#*.*.qs.settings.params.26.*.false.true.*.value=value: city\:,qs:ci
#*.*.qs.settings.params.26.0.false.false.true.value=value: city\:,qs:city
#*.*.qs.settings.params.26.continue=1

# Category and type
*.*.qs.settings.params.27.keys.1=cg
*.*.qs.settings.params.27.keys.2=st
*.*.qs.settings.params.27.1000.s.value=value: category_type\:,qs:cg,value:_s
*.*.qs.settings.params.27.1000.u.value=value: category_type\:,qs:cg,value:_u
*.*.qs.settings.params.27.1000.a.value=value: category_type\:,qs:cg,value:_s\,,qs:cg,value:_u
*.*.qs.settings.params.27.1220.*.value=value: category_type\:,qs:cg,value:_s
*.*.qs.settings.params.27.1240.*.value=value: category_type\:,qs:cg,value:_u
*.*.qs.settings.params.27.1260.*.value=value: category_type\:,qs:cg,value:_u

# Cars
*.*.qs.settings.params.27.2000.s.value=value: category_type\:,qs:cg,value:_s
*.*.qs.settings.params.27.2000.k.value=value: category_type\:,qs:cg,value:_k
*.*.qs.settings.params.27.2000.a.value=value: category_type\:,qs:cg,value:_s\,,qs:cg,value:_k
*.*.qs.settings.params.27.2020.s.value=value: category_type\:,qs:cg,value:_s
*.*.qs.settings.params.27.2020.k.value=value: category_type\:,qs:cg,value:_k
*.*.qs.settings.params.27.2020.a.value=value: category_type\:,qs:cg,value:_s\,,qs:cg,value:_k
*.*.qs.settings.params.27.2040.s.value=value: category_type\:,qs:cg,value:_s
*.*.qs.settings.params.27.2040.k.value=value: category_type\:,qs:cg,value:_k
*.*.qs.settings.params.27.2040.a.value=value: category_type\:,qs:cg,value:_s\,,qs:cg,value:_k
*.*.qs.settings.params.27.2060.s.value=value: category_type\:,qs:cg,value:_s
*.*.qs.settings.params.27.2060.k.value=value: category_type\:,qs:cg,value:_k
*.*.qs.settings.params.27.2060.a.value=value: category_type\:,qs:cg,value:_s\,,qs:cg,value:_k
*.*.qs.settings.params.27.2080.s.value=value: category_type\:,qs:cg,value:_s
*.*.qs.settings.params.27.2080.k.value=value: category_type\:,qs:cg,value:_k
*.*.qs.settings.params.27.2080.a.value=value: category_type\:,qs:cg,value:_s\,,qs:cg,value:_k
*.*.qs.settings.params.27.2100.s.value=value: category_type\:,qs:cg,value:_s
*.*.qs.settings.params.27.2100.k.value=value: category_type\:,qs:cg,value:_k
*.*.qs.settings.params.27.2100.a.value=value: category_type\:,qs:cg,value:_s\,,qs:cg,value:_k
*.*.qs.settings.params.27.2120.s.value=value: category_type\:,qs:cg,value:_s
*.*.qs.settings.params.27.2120.k.value=value: category_type\:,qs:cg,value:_k
*.*.qs.settings.params.27.2120.a.value=value: category_type\:,qs:cg,value:_s\,,qs:cg,value:_k

*.*.qs.settings.params.27.*.*.value=value: category_type\:,qs:cg,value:_s\,,qs:cg,value:_k
*.*.qs.settings.params.27.continue=1

# No subtype
*.*.qs.settings.params.28.keys.1=?st
*.*.qs.settings.params.28.keys.2=?cg
*.*.qs.settings.params.28.keys.3=type
*.*.qs.settings.params.28.false.true.s.value=value: category\:,qs:cg
*.*.qs.settings.params.28.false.true.k.value=value: category\:,qs:cg
*.*.qs.settings.params.28.continue=1

# Ad type is used if neither category nor region given.
#*.*.qs.settings.params.29.keys.1=?c
#*.*.qs.settings.params.29.keys.2=cg
#*.*.qs.settings.params.29.keys.3=w
#*.*.qs.settings.params.29.keys.4=type
#*.*.qs.settings.params.29.false.*.1.a.value=value: category_type\:,qs:cg,value:_s\,,qs:cg,value:_u\,,qs:cg,value:_k\,,qs:cg,value:_h,value: region_type\:,qs:region,value:_s\,,qs:region,value:_u\,,qs:region,value:_k\,,qs:region,value:_h
#*.*.qs.settings.params.29.false.*.1.s.value=value: category_type\:,qs:cg,value:_s,value: region_type\:,qs:region,value:_s
#*.*.qs.settings.params.29.false.*.1.u.value=value: category_type\:,qs:cg,value:_u,value: region_type\:,qs:region,value:_u
#*.*.qs.settings.params.29.false.*.1.b.value=value: category_type\:,qs:cg,value:_b,value: region_type\:,qs:region,value:_b
#*.*.qs.settings.params.29.false.*.1.k.value=value: category_type\:,qs:cg,value:_k,value: region_type\:,qs:region,value:_k
#*.*.qs.settings.params.29.false.*.1.h.value=value: category_type\:,qs:cg,value:_h,value: region_type\:,qs:region,value:_h
#*.*.qs.settings.params.29.false.*.2.a.value=value: category_type\:,qs:cg,value:_s\,,qs:cg,value:_u\,,qs:cg,value:_k\,,qs:cg,value:_h,value: near_type\:,qs:region,value:_s\,,qs:region,value:_u\,,qs:region,value:_k\,,qs:region,value:_h
#*.*.qs.settings.params.29.false.*.2.s.value=value: category_type\:,qs:cg,value:_s,value: near_type\:,qs:region,value:_s
#*.*.qs.settings.params.29.false.*.2.u.value=value: category_type\:,qs:cg,value:_u,value: near_type\:,qs:region,value:_u
#*.*.qs.settings.params.29.false.*.2.b.value=value: category_type\:,qs:cg,value:_b,value: near_type\:,qs:region,value:_b
#*.*.qs.settings.params.29.false.*.2.k.value=value: category_type\:,qs:cg,value:_k,value: near_type\:,qs:region,value:_k
#*.*.qs.settings.params.29.false.*.2.h.value=value: category_type\:,qs:cg,value:_h,value: near_type\:,qs:region,value:_h
#*.*.qs.settings.params.29.false.*.3.a.value=value: category_type\:,qs:cg,value:_s\,,qs:cg,value:_u\,,qs:cg,value:_k\,,qs:cg,value:_h
#*.*.qs.settings.params.29.false.*.3.s.value=value: category_type\:,qs:cg,value:_s
#*.*.qs.settings.params.29.false.*.3.u.value=value: category_type\:,qs:cg,value:_u
#*.*.qs.settings.params.29.false.*.3.b.value=value: category_type\:,qs:cg,value:_b
#*.*.qs.settings.params.29.false.*.3.k.value=value: category_type\:,qs:cg,value:_k
#*.*.qs.settings.params.29.false.*.3.h.value=value: category_type\:,qs:cg,value:_h

*.*.qs.settings.params.29.keys.1=?cg
*.*.qs.settings.params.29.keys.2=w
*.*.qs.settings.params.29.keys.3=type
*.*.qs.settings.params.29.false.3.a.value=value: type\:s\,u\,k\,h
*.*.qs.settings.params.29.false.3.s.value=value: type\:-
*.*.qs.settings.params.29.false.3.k.value=value: type\:-
*.*.qs.settings.params.29.continue=1

# Price
*.*.qs.settings.params.30.keys.1=?ps
*.*.qs.settings.params.30.keys.2=?pe
*.*.qs.settings.params.30.keys.3=cg
*.*.qs.settings.params.30.keys.4=type

*.*.qs.settings.params.30.true.true.1000.a.value=value: price\:,map:pricelist_0\:ps,value:-,map:pricelist_0\:pe
*.*.qs.settings.params.30.true.true.1000.s.value=value: price\:,map:pricelist_10\:ps,value:-,map:pricelist_10\:pe
*.*.qs.settings.params.30.true.true.1000.u.value=value: price\:,map:pricelist_2\:ps,value:-,map:pricelist_2\:pe
*.*.qs.settings.params.30.true.false.1000.a.value=value: price\:,map:pricelist_0\:ps,value:-
*.*.qs.settings.params.30.true.false.1000.s.value=value: price\:,map:pricelist_10\:ps,value:-
*.*.qs.settings.params.30.true.false.1000.u.value=value: price\:,map:pricelist_2\:ps,value:-
*.*.qs.settings.params.30.false.true.1000.a.value=value: price\:1-,map:pricelist_0\:pe
*.*.qs.settings.params.30.false.true.1000.s.value=value: price\:1-,map:pricelist_10\:pe
*.*.qs.settings.params.30.false.true.1000.u.value=value: price\:1-,map:pricelist_2\:pe

*.*.qs.settings.params.30.true.true.1220.*.value=value: price\:,map:pricelist_10\:ps,value:-,map:pricelist_10\:pe
*.*.qs.settings.params.30.true.false.1220.*.value=value: price\:,map:pricelist_10\:ps,value:-
*.*.qs.settings.params.30.false.true.1220.*.value=value: price\:1-,map:pricelist_10\:pe

*.*.qs.settings.params.30.true.true.1240.*.value=value: price\:,map:pricelist_2\:ps,value:-,map:pricelist_2\:pe
*.*.qs.settings.params.30.true.false.1240.*.value=value: price\:,map:pricelist_2\:ps,value:-
*.*.qs.settings.params.30.false.true.1240.*.value=value: price\:1-,map:pricelist_2\:pe

*.*.qs.settings.params.30.true.true.1260.*.value=value: price\:,map:pricelist_9\:ps,value:-,map:pricelist_9\:pe
*.*.qs.settings.params.30.true.false.1260.*.value=value: price\:,map:pricelist_9\:ps,value:-
*.*.qs.settings.params.30.false.true.1260.*.value=value: price\:1-,map:pricelist_9\:pe

*.*.qs.settings.params.30.continue=1

# Rest of categories with price
# XXX: They all use pricelist:2
*.*.qs.settings.params.31.keys.1=?ps
*.*.qs.settings.params.31.keys.2=?pe
*.*.qs.settings.params.31.keys.3=cg

# Vehicles
*.*.qs.settings.params.31.true.true.2000.value=value: suborder\:,map:pricelist_1\:ps,value:-,map:pricelist_1\:pe
*.*.qs.settings.params.31.true.false.2000.value=value: suborder\:,map:pricelist_1\:ps,value:-
*.*.qs.settings.params.31.false.true.2000.value=value: suborder\:1-,map:pricelist_1\:pe
*.*.qs.settings.params.31.true.true.2020.value=value: suborder\:,map:pricelist_8\:ps,value:-,map:pricelist_8\:pe
*.*.qs.settings.params.31.true.false.2020.value=value: suborder\:,map:pricelist_8\:ps,value:-
*.*.qs.settings.params.31.false.true.2020.value=value: suborder\:1-,map:pricelist_8\:pe
*.*.qs.settings.params.31.true.true.2040.value=value: suborder\:,map:pricelist_8\:ps,value:-,map:pricelist_8\:pe
*.*.qs.settings.params.31.true.false.2040.value=value: suborder\:,map:pricelist_8\:ps,value:-
*.*.qs.settings.params.31.false.true.2040.value=value: suborder\:1-,map:pricelist_8\:pe
*.*.qs.settings.params.31.true.true.2060.value=value: suborder\:,map:pricelist_8\:ps,value:-,map:pricelist_8\:pe
*.*.qs.settings.params.31.true.false.2060.value=value: suborder\:,map:pricelist_8\:ps,value:-
*.*.qs.settings.params.31.false.true.2060.value=value: suborder\:1-,map:pricelist_8\:pe
*.*.qs.settings.params.31.true.true.2080.value=value: suborder\:,map:pricelist_1\:ps,value:-,map:pricelist_1\:pe
*.*.qs.settings.params.31.true.false.2080.value=value: suborder\:,map:pricelist_1\:ps,value:-
*.*.qs.settings.params.31.false.true.2080.value=value: suborder\:1-,map:pricelist_1\:pe
*.*.qs.settings.params.31.true.true.2100.value=value: suborder\:,map:pricelist_1\:ps,value:-,map:pricelist_1\:pe
*.*.qs.settings.params.31.true.false.2100.value=value: suborder\:,map:pricelist_1\:ps,value:-
*.*.qs.settings.params.31.false.true.2100.value=value: suborder\:1-,map:pricelist_1\:pe
*.*.qs.settings.params.31.true.true.2120.value=value: suborder\:,map:pricelist_1\:ps,value:-,map:pricelist_1\:pe
*.*.qs.settings.params.31.true.false.2120.value=value: suborder\:,map:pricelist_1\:ps,value:-
*.*.qs.settings.params.31.false.true.2120.value=value: suborder\:1-,map:pricelist_1\:pe

# Computers and electronics
*.*.qs.settings.params.31.true.true.3000.value=value: suborder\:,map:pricelist_5\:ps,value:-,map:pricelist_5\:pe
*.*.qs.settings.params.31.true.false.3000.value=value: suborder\:,map:pricelist_5\:ps,value:-
*.*.qs.settings.params.31.false.true.3000.value=value: suborder\:1-,map:pricelist_5\:pe
*.*.qs.settings.params.31.true.true.3020.value=value: suborder\:,map:pricelist_5\:ps,value:-,map:pricelist_5\:pe
*.*.qs.settings.params.31.true.false.3020.value=value: suborder\:,map:pricelist_5\:ps,value:-
*.*.qs.settings.params.31.false.true.3020.value=value: suborder\:1-,map:pricelist_5\:pe
*.*.qs.settings.params.31.true.true.3040.value=value: suborder\:,map:pricelist_5\:ps,value:-,map:pricelist_5\:pe
*.*.qs.settings.params.31.true.false.3040.value=value: suborder\:,map:pricelist_5\:ps,value:-
*.*.qs.settings.params.31.false.true.3040.value=value: suborder\:1-,map:pricelist_5\:pe
*.*.qs.settings.params.31.true.true.3060.value=value: suborder\:,map:pricelist_5\:ps,value:-,map:pricelist_5\:pe
*.*.qs.settings.params.31.true.false.3060.value=value: suborder\:,map:pricelist_5\:ps,value:-
*.*.qs.settings.params.31.false.true.3060.value=value: suborder\:1-,map:pricelist_5\:pe
*.*.qs.settings.params.31.true.true.3080.value=value: suborder\:,map:pricelist_5\:ps,value:-,map:pricelist_5\:pe
*.*.qs.settings.params.31.true.false.3080.value=value: suborder\:,map:pricelist_5\:ps,value:-
*.*.qs.settings.params.31.false.true.3080.value=value: suborder\:1-,map:pricelist_5\:pe

# Home stuff
*.*.qs.settings.params.31.true.true.5000.value=value: suborder\:,map:pricelist_3\:ps,value:-,map:pricelist_3\:pe
*.*.qs.settings.params.31.true.false.5000.value=value: suborder\:,map:pricelist_3\:ps,value:-
*.*.qs.settings.params.31.false.true.5000.value=value: suborder\:1-,map:pricelist_3\:pe
*.*.qs.settings.params.31.true.true.5020.value=value: suborder\:,map:pricelist_3\:ps,value:-,map:pricelist_3\:pe
*.*.qs.settings.params.31.true.false.5020.value=value: suborder\:,map:pricelist_3\:ps,value:-
*.*.qs.settings.params.31.false.true.5020.value=value: suborder\:1-,map:pricelist_3\:pe
*.*.qs.settings.params.31.true.true.5040.value=value: suborder\:,map:pricelist_3\:ps,value:-,map:pricelist_3\:pe
*.*.qs.settings.params.31.true.false.5040.value=value: suborder\:,map:pricelist_3\:ps,value:-
*.*.qs.settings.params.31.false.true.5040.value=value: suborder\:1-,map:pricelist_3\:pe
*.*.qs.settings.params.31.true.true.5060.value=value: suborder\:,map:pricelist_3\:ps,value:-,map:pricelist_3\:pe
*.*.qs.settings.params.31.true.false.5060.value=value: suborder\:,map:pricelist_3\:ps,value:-
*.*.qs.settings.params.31.false.true.5060.value=value: suborder\:1-,map:pricelist_3\:pe
#*.*.qs.settings.params.31.true.true.5080.value=value: suborder\:,map:pricelist_3\:ps,value:-,map:pricelist_3\:pe
#*.*.qs.settings.params.31.true.false.5080.value=value: suborder\:,map:pricelist_3\:ps,value:-
#*.*.qs.settings.params.31.false.true.5080.value=value: suborder\:1-,map:pricelist_3\:pe
*.*.qs.settings.params.31.true.true.4020.value=value: suborder\:,map:pricelist_3\:ps,value:-,map:pricelist_3\:pe
*.*.qs.settings.params.31.true.false.4020.value=value: suborder\:,map:pricelist_3\:ps,value:-
*.*.qs.settings.params.31.false.true.4020.value=value: suborder\:1-,map:pricelist_3\:pe
*.*.qs.settings.params.31.true.true.5120.value=value: suborder\:,map:pricelist_3\:ps,value:-,map:pricelist_3\:pe
*.*.qs.settings.params.31.true.false.5120.value=value: suborder\:,map:pricelist_3\:ps,value:-
*.*.qs.settings.params.31.false.true.5120.value=value: suborder\:1-,map:pricelist_3\:pe
*.*.qs.settings.params.31.true.true.4040.value=value: suborder\:,map:pricelist_3\:ps,value:-,map:pricelist_3\:pe
*.*.qs.settings.params.31.true.false.4040.value=value: suborder\:,map:pricelist_3\:ps,value:-
*.*.qs.settings.params.31.false.true.4040.value=value: suborder\:1-,map:pricelist_3\:pe
*.*.qs.settings.params.31.true.true.5160.value=value: suborder\:,map:pricelist_3\:ps,value:-,map:pricelist_3\:pe
*.*.qs.settings.params.31.true.false.5160.value=value: suborder\:,map:pricelist_3\:ps,value:-
*.*.qs.settings.params.31.false.true.5160.value=value: suborder\:1-,map:pricelist_3\:pe
*.*.qs.settings.params.31.true.true.4080.value=value: suborder\:,map:pricelist_3\:ps,value:-,map:pricelist_3\:pe
*.*.qs.settings.params.31.true.false.4080.value=value: suborder\:,map:pricelist_3\:ps,value:-
*.*.qs.settings.params.31.false.true.4080.value=value: suborder\:1-,map:pricelist_3\:pe

# Free time
*.*.qs.settings.params.31.true.true.6000.value=value: suborder\:,map:pricelist_4\:ps,value:-,map:pricelist_4\:pe
*.*.qs.settings.params.31.true.false.6000.value=value: suborder\:,map:pricelist_4\:ps,value:-
*.*.qs.settings.params.31.false.true.6000.value=value: suborder\:1-,map:pricelist_4\:pe
*.*.qs.settings.params.31.true.true.6020.value=value: suborder\:,map:pricelist_4\:ps,value:-,map:pricelist_4\:pe
*.*.qs.settings.params.31.true.false.6020.value=value: suborder\:,map:pricelist_4\:ps,value:-
*.*.qs.settings.params.31.false.true.6020.value=value: suborder\:1-,map:pricelist_4\:pe
#*.*.qs.settings.params.31.true.true.6040.value=value: suborder\:,map:pricelist_4\:ps,value:-,map:pricelist_4\:pe
#*.*.qs.settings.params.31.true.false.6040.value=value: suborder\:,map:pricelist_4\:ps,value:-
#*.*.qs.settings.params.31.false.true.6040.value=value: suborder\:1-,map:pricelist_4\:pe
*.*.qs.settings.params.31.true.true.6060.value=value: suborder\:,map:pricelist_4\:ps,value:-,map:pricelist_4\:pe
*.*.qs.settings.params.31.true.false.6060.value=value: suborder\:,map:pricelist_4\:ps,value:-
*.*.qs.settings.params.31.false.true.6060.value=value: suborder\:1-,map:pricelist_4\:pe
*.*.qs.settings.params.31.true.true.6080.value=value: suborder\:,map:pricelist_4\:ps,value:-,map:pricelist_4\:pe
*.*.qs.settings.params.31.true.false.6080.value=value: suborder\:,map:pricelist_4\:ps,value:-
*.*.qs.settings.params.31.false.true.6080.value=value: suborder\:1-,map:pricelist_4\:pe
*.*.qs.settings.params.31.true.true.6100.value=value: suborder\:,map:pricelist_4\:ps,value:-,map:pricelist_4\:pe
*.*.qs.settings.params.31.true.false.6100.value=value: suborder\:,map:pricelist_4\:ps,value:-
*.*.qs.settings.params.31.false.true.6100.value=value: suborder\:1-,map:pricelist_4\:pe
*.*.qs.settings.params.31.true.true.6120.value=value: suborder\:,map:pricelist_4\:ps,value:-,map:pricelist_4\:pe
*.*.qs.settings.params.31.true.false.6120.value=value: suborder\:,map:pricelist_4\:ps,value:-
*.*.qs.settings.params.31.false.true.6120.value=value: suborder\:1-,map:pricelist_4\:pe
*.*.qs.settings.params.31.true.true.6140.value=value: suborder\:,map:pricelist_4\:ps,value:-,map:pricelist_4\:pe
*.*.qs.settings.params.31.true.false.6140.value=value: suborder\:,map:pricelist_4\:ps,value:-
*.*.qs.settings.params.31.false.true.6140.value=value: suborder\:1-,map:pricelist_4\:pe
*.*.qs.settings.params.31.true.true.6160.value=value: suborder\:,map:pricelist_4\:ps,value:-,map:pricelist_4\:pe
*.*.qs.settings.params.31.true.false.6160.value=value: suborder\:,map:pricelist_4\:ps,value:-
*.*.qs.settings.params.31.false.true.6160.value=value: suborder\:1-,map:pricelist_4\:pe
*.*.qs.settings.params.31.true.true.6180.value=value: suborder\:,map:pricelist_4\:ps,value:-,map:pricelist_4\:pe
*.*.qs.settings.params.31.true.false.6180.value=value: suborder\:,map:pricelist_4\:ps,value:-
*.*.qs.settings.params.31.false.true.6180.value=value: suborder\:1-,map:pricelist_4\:pe
*.*.qs.settings.params.31.true.true.4060.value=value: suborder\:,map:pricelist_4\:ps,value:-,map:pricelist_4\:pe
*.*.qs.settings.params.31.true.false.4060.value=value: suborder\:,map:pricelist_4\:ps,value:-
*.*.qs.settings.params.31.false.true.4060.value=value: suborder\:1-,map:pricelist_4\:pe

# Job offers
*.*.qs.settings.params.31.true.true.7080.value=value: suborder\:,map:pricelist_6\:ps,value:-,map:pricelist_6\:pe
*.*.qs.settings.params.31.true.false.7080.value=value: suborder\:,map:pricelist_6\:ps,value:-
*.*.qs.settings.params.31.false.true.7080.value=value: suborder\:1-,map:pricelist_6\:pe

#other
*.*.qs.settings.params.31.true.true.8020.value=value: suborder\:,map:pricelist_7\:ps,value:-,map:pricelist_7\:pe
*.*.qs.settings.params.31.true.false.8020.value=value: suborder\:,map:pricelist_7\:ps,value:-
*.*.qs.settings.params.31.false.true.8020.value=value: suborder\:1-,map:pricelist_7\:pe

#mom stuff
*.*.qs.settings.params.31.true.true.9020.value=value: suborder\:,map:pricelist_3\:ps,value:-,map:pricelist_3\:pe
*.*.qs.settings.params.31.true.false.9020.value=value: suborder\:,map:pricelist_3\:ps,value:-
*.*.qs.settings.params.31.false.true.9020.value=value: suborder\:1-,map:pricelist_3\:pe

*.*.qs.settings.params.31.true.true.9040.value=value: suborder\:,map:pricelist_3\:ps,value:-,map:pricelist_3\:pe
*.*.qs.settings.params.31.true.false.9040.value=value: suborder\:,map:pricelist_3\:ps,value:-
*.*.qs.settings.params.31.false.true.9040.value=value: suborder\:1-,map:pricelist_3\:pe

*.*.qs.settings.params.31.true.true.9060.value=value: suborder\:,map:pricelist_3\:ps,value:-,map:pricelist_3\:pe
*.*.qs.settings.params.31.true.false.9060.value=value: suborder\:,map:pricelist_3\:ps,value:-
*.*.qs.settings.params.31.false.true.9060.value=value: suborder\:1-,map:pricelist_3\:pe

*.*.qs.settings.params.31.continue=1

# Generic suborder incase that price is missing
*.*.qs.settings.params.32.keys.1=sp
*.*.qs.settings.params.32.keys.2=?ps
*.*.qs.settings.params.32.keys.3=?pe
*.*.qs.settings.params.32.keys.4==cg==7040
*.*.qs.settings.params.32.1.false.false.false.value=value: suborder\:1-
*.*.qs.settings.params.32.2.false.false.false.value=value: suborder\:1-
*.*.qs.settings.params.32.price_desc.false.false.false.value=value: suborder\:1-
*.*.qs.settings.params.32.price_asc.false.false.false.value=value: suborder\:1-
*.*.qs.settings.params.32.continue=1

# Exclude categories.
#*.*.qs.settings.params.32.keys.1=xc
#*.*.qs.settings.params.32.keys.2=cg
#*.*.qs.settings.params.32.keys.3=type
#*.*.qs.settings.params.32.keys.4=?c
#*.*.qs.settings.params.32.1143.1140.s.false.value=value: NOT category\:1143
#*.*.qs.settings.params.32.continue=1

# Communes
*.*.qs.settings.params.33.keys.1=?cmn
*.*.qs.settings.params.33.true.value=value: communes\:,qs:cmn
*.*.qs.settings.params.33.continue=1


*.*.qs.settings.params.35.keys.1=?cos
*.*.qs.settings.params.35.keys.2=?coe
*.*.qs.settings.params.35.keys.3=cg
*.*.qs.settings.params.35.keys.4=ret

*.*.qs.settings.params.35.true.true.1220.1.value=value: condominio\:,map:condominio_list\:cos,value:-,map:condominio_list\:coe
*.*.qs.settings.params.35.true.false.1220.1.value=value: condominio\:,map:condominio_list\:cos,value:-
*.*.qs.settings.params.35.false.true.1220.1.value=value: condominio\:1-,map:condominio_list\:coe
*.*.qs.settings.params.35.true.true.1220.2.value=value: condominio\:,map:condominio_list\:cos,value:-,map:condominio_list\:coe
*.*.qs.settings.params.35.true.false.1220.2.value=value: condominio\:,map:condominio_list\:cos,value:-
*.*.qs.settings.params.35.false.true.1220.2.value=value: condominio\:1-,map:condominio_list\:coe
*.*.qs.settings.params.35.true.true.1220.6.value=value: condominio\:,map:condominio_list\:cos,value:-,map:condominio_list\:coe
*.*.qs.settings.params.35.true.false.1220.6.value=value: condominio\:,map:condominio_list\:cos,value:-
*.*.qs.settings.params.35.false.true.1220.6.value=value: condominio\:1-,map:condominio_list\:coe

*.*.qs.settings.params.35.true.true.1240.1.value=value: condominio\:,map:condominio_list\:cos,value:-,map:condominio_list\:coe
*.*.qs.settings.params.35.true.false.1240.1.value=value: condominio\:,map:condominio_list\:cos,value:-
*.*.qs.settings.params.35.false.true.1240.1.value=value: condominio\:1-,map:condominio_list\:coe
*.*.qs.settings.params.35.true.true.1240.2.value=value: condominio\:,map:condominio_list\:cos,value:-,map:condominio_list\:coe
*.*.qs.settings.params.35.true.false.1240.2.value=value: condominio\:,map:condominio_list\:cos,value:-
*.*.qs.settings.params.35.false.true.1240.2.value=value: condominio\:1-,map:condominio_list\:coe
*.*.qs.settings.params.35.true.true.1240.6.value=value: condominio\:,map:condominio_list\:cos,value:-,map:condominio_list\:coe
*.*.qs.settings.params.35.true.false.1240.6.value=value: condominio\:,map:condominio_list\:cos,value:-
*.*.qs.settings.params.35.false.true.1240.6.value=value: condominio\:1-,map:condominio_list\:coe
*.*.qs.settings.params.35.true.true.1240.7.value=value: condominio\:,map:condominio_list\:cos,value:-,map:condominio_list\:coe
*.*.qs.settings.params.35.true.false.1240.7.value=value: condominio\:,map:condominio_list\:cos,value:-
*.*.qs.settings.params.35.false.true.1240.7.value=value: condominio\:1-,map:condominio_list\:coe

*.*.qs.settings.params.35.continue=1

# language list
*.*.qs.settings.params.37.keys.1=?ql
*.*.qs.settings.params.37.true.value=value: lang\:,multi,qs:ql

# Cartype
*.*.qs.settings.params.38.keys.1=?ctp
*.*.qs.settings.params.38.keys.2=cg
*.*.qs.settings.params.38.true.2020.value=value: cartype\:,qs:ctp
*.*.qs.settings.params.38.continue=1

# CC
*.*.qs.settings.params.39.keys.1=?ccs
*.*.qs.settings.params.39.keys.2=?cce
*.*.qs.settings.params.39.keys.3=cg
*.*.qs.settings.params.39.true.true.2060.value=value: cubiccms_value\:,qs:ccs,value:-,qs:cce
*.*.qs.settings.params.39.false.true.2060.value=value: cubiccms_value\:-,qs:cce
*.*.qs.settings.params.39.true.false.2060.value=value: cubiccms_value\:,qs:ccs,value:-
*.*.qs.settings.params.39.continue=1

# GENDER
*.*.qs.settings.params.40.keys.1=?gend
*.*.qs.settings.params.40.keys.2=cg
*.*.qs.settings.params.40.true.4020.value=value: gender\:,qs:gend
*.*.qs.settings.params.40.true.4040.value=value: gender\:,qs:gend
*.*.qs.settings.params.40.true.4060.value=value: gender\:,qs:gend
*.*.qs.settings.params.40.continue=1

# CONDITION
*.*.qs.settings.params.41.keys.1=?cond
*.*.qs.settings.params.41.keys.2=cg
#*.*.qs.settings.params.41.true.5080.value=value: condition\:,qs:cond
*.*.qs.settings.params.41.true.4020.value=value: condition\:,qs:cond
*.*.qs.settings.params.41.true.4080.value=value: condition\:,qs:cond
*.*.qs.settings.params.41.true.5120.value=value: condition\:,qs:cond
*.*.qs.settings.params.41.true.4040.value=value: condition\:,qs:cond
*.*.qs.settings.params.41.true.9020.value=value: condition\:,qs:cond
*.*.qs.settings.params.41.true.9040.value=value: condition\:,qs:cond
*.*.qs.settings.params.41.true.9060.value=value: condition\:,qs:cond
#*.*.qs.settings.params.41.true.5160.value=value: condition\:,qs:cond
*.*.qs.settings.params.41.continue=1

*.*.qs.settings.params.42.keys.1=?jc
*.*.qs.settings.params.42.keys.2=cg
*.*.qs.settings.params.42.true.7020.value=value: job_category\:,qs:jc
*.*.qs.settings.params.42.true.7040.value=value: job_category\:,qs:jc
*.*.qs.settings.params.42.continue=1

*.*.qs.settings.params.43.keys.1=?sct
*.*.qs.settings.params.43.keys.2=cg
*.*.qs.settings.params.43.true.7060.value=value: service_type\:,qs:sct
*.*.qs.settings.params.43.continue=1

*.*.qs.settings.params.44.keys.1=?zn
*.*.qs.settings.params.44.true.value=value: zone\:,qs:zn
*.*.qs.settings.params.44.continue=1


*.*.qs.settings.params.45.keys.1=?br
*.*.qs.settings.params.45.keys.2=cg
*.*.qs.settings.params.45.true.2020.value=value: brand\:,qs:br
*.*.qs.settings.params.45.continue=1

*.*.qs.settings.params.46.keys.1=?mo
*.*.qs.settings.params.46.keys.2=cg
*.*.qs.settings.params.46.true.2020.value=value: model\:,qs:mo
*.*.qs.settings.params.46.continue=1

# Estacionamiento(Garage Spaces)
*.*.qs.settings.params.47.keys.1=?gs
*.*.qs.settings.params.47.keys.2==gs==5
*.*.qs.settings.params.47.keys.3=cg
*.*.qs.settings.params.47.true.false.1220.value=value: garage_spaces\:,qs:gs
*.*.qs.settings.params.47.true.false.1240.value=value: garage_spaces\:,qs:gs
*.*.qs.settings.params.47.true.false.1260.value=value: garage_spaces\:,qs:gs
*.*.qs.settings.params.47.true.true.1220.value=value: garage_spaces\:5-
*.*.qs.settings.params.47.true.true.1240.value=value: garage_spaces\:5-
*.*.qs.settings.params.47.true.true.1260.value=value: garage_spaces\:5-
*.*.qs.settings.params.47.continue=1

*.*.qs.settings.params.48.keys.1=?ret
*.*.qs.settings.params.48.keys.2=cg
*.*.qs.settings.params.48.true.1220.value=value: estate_type\:,qs:ret
*.*.qs.settings.params.48.true.1240.value=value: estate_type\:,qs:ret
*.*.qs.settings.params.48.true.1260.value=value: estate_type\:,qs:ret
*.*.qs.settings.params.48.continue=1

# The following allows you to search for men and boys when looking for 'male',
# and women and girls when looking for 'female'. Must change in the future.

*.*.qs.settings.params.49.keys.1=?fwgend
*.*.qs.settings.params.49.keys.2=cg
*.*.qs.settings.params.49.true.4080.value=value: footwear_gender\:,qs:fwgend
*.*.qs.settings.params.49.continue=1

*.*.qs.settings.params.50.keys.1=?fwsize
*.*.qs.settings.params.50.keys.2=cg
*.*.qs.settings.params.50.true.4080.value=value: footwear_size\:,qs:fwsize
*.*.qs.settings.params.50.continue=1

*.*.qs.settings.params.51.keys.1=?fwtype
*.*.qs.settings.params.51.keys.2=cg
*.*.qs.settings.params.51.true.4080.value=value: footwear_type\:,qs:fwtype
*.*.qs.settings.params.51.continue=1

# Price range
*.*.qs.settings.params.52.keys.1=?price_range
*.*.qs.settings.params.52.true.value=value: price\:,qs:price_range
*.*.qs.settings.params.52.continue=1

# Cubic CM range
*.*.qs.settings.params.53.keys.1=?ccm_range
*.*.qs.settings.params.53.true.value=value: cubiccms_value\:,qs:ccm_range
*.*.qs.settings.params.53.continue=1

# Rooms range
*.*.qs.settings.params.54.keys.1=?rooms_range
*.*.qs.settings.params.54.true.value=value: rooms\:,qs:rooms_range
*.*.qs.settings.params.54.continue=1

# Regdate range
*.*.qs.settings.params.55.keys.1=?regdate_range
*.*.qs.settings.params.55.true.value=value: regdate\:,qs:regdate_range
*.*.qs.settings.params.55.continue=1

# Mileage range
*.*.qs.settings.params.56.keys.1=?mileage_range
*.*.qs.settings.params.56.true.value=value: mileage\:,qs:mileage_range
*.*.qs.settings.params.56.continue=1

# Size range
*.*.qs.settings.params.57.keys.1=?size_range
*.*.qs.settings.params.57.true.value=value: size\:,qs:size_range
*.*.qs.settings.params.57.continue=1

# Condominio range
*.*.qs.settings.params.58.keys.1=?condominio_range
*.*.qs.settings.params.58.true.value=value: condominio\:,qs:condominio_range
*.*.qs.settings.params.58.continue=1

# Bathroom range
*.*.qs.settings.params.59.keys.1=?bathroom_range
*.*.qs.settings.params.59.true.value=value: bathrooms\:,qs:bathroom_range
*.*.qs.settings.params.59.continue=1

# Region
*.*.qs.settings.params.geo_for_adwatch.keys.1=appl
*.*.qs.settings.params.geo_for_adwatch.keys.2=w
*.*.qs.settings.params.geo_for_adwatch.aw.1.value=value: region\:,qs:region
*.*.qs.settings.params.geo_for_adwatch.aw.2.value=value: near\:,qs:region
*.*.qs.settings.params.geo_for_adwatch.continue=1

#
# Query part
#
*.*.qs.settings.query.1.keys.1=?id
*.*.qs.settings.query.1.keys.2=?fk
*.*.qs.settings.query.1.keys.3=?q_is_id
*.*.qs.settings.query.1.keys.4=?q
*.*.qs.settings.query.1.false.false.false.true.value=qs:q

#
# Description
#
*.*.qs.settings.desc.1.keys.1=?q
*.*.qs.settings.desc.1.true.value=qs:q
*.*.qs.settings.desc.1.continue=1

# Category and type
*.*.qs.settings.desc.2.default=sep,tmpl:common/qs_cat_type_desc.txt
*.*.qs.settings.desc.2.continue=1

# Region, area and country
*.*.qs.settings.desc.3.default=sep,tmpl:common/qs_region_desc.txt
*.*.qs.settings.desc.3.continue=1

# Mileage
*.*.qs.settings.desc.5.keys.1=?ms
*.*.qs.settings.desc.5.keys.2=?me
*.*.qs.settings.desc.5.keys.3=cg
*.*.qs.settings.desc.5.keys.4=type
*.*.qs.settings.desc.5.true.true.2020.s.value=sep,value:mileage ,map:mileage_desc_start\:ms,value: - ,map:mileage_desc_end\:me
*.*.qs.settings.desc.5.true.false.2020.s.value=sep,value:kilometres from ,map:mileage_desc_start\:ms
*.*.qs.settings.desc.5.false.true.2020.s.value=sep,value:kilometres to ,map:mileage_desc_end\:me
*.*.qs.settings.desc.5.continue=1

# Monthly rent
#*.*.qs.settings.desc.6.keys.1=?mre
#*.*.qs.settings.desc.6.keys.2=cg
#*.*.qs.settings.desc.6.keys.3=type
#*.*.qs.settings.desc.6.true.3020.u.value=sep,value:monthly rent to ,map:monthly_rent_desc\:mre
#*.*.qs.settings.desc.6.true.3100.u.value=sep,value:monthly rent to ,map:monthly_rent_desc\:mre
#*.*.qs.settings.desc.6.continue=1

# Weekly rent
#*.*.qs.settings.desc.7.keys.1=?wre
#*.*.qs.settings.desc.7.keys.2=cg
#*.*.qs.settings.desc.7.keys.3=type
#*.*.qs.settings.desc.7.true.3040.u.value=sep,value:weekly rent to ,map:weekly_rent_desc\:wre
#*.*.qs.settings.desc.7.true.3080.u.value=sep,value:weekly rent to ,map:weekly_rent_desc\:wre
#*.*.qs.settings.desc.7.continue=1

# Max rent
#*.*.qs.settings.desc.8.keys.1=?mas
#*.*.qs.settings.desc.8.keys.3=cg
#*.*.qs.settings.desc.8.keys.4=type
#*.*.qs.settings.desc.8.true.3020.h.value=sep,value:max hyra fr o m \:,map:max_rent_desc\:mas
#*.*.qs.settings.desc.8.true.3040.h.value=sep,value:max hyra fr o m \:,map:max_rent_desc\:mas
#*.*.qs.settings.desc.8.continue=1

# Available weeks
#*.*.qs.settings.desc.9.keys.1=?aws
#*.*.qs.settings.desc.9.keys.2=?awe
#*.*.qs.settings.desc.9.keys.3=cg
#*.*.qs.settings.desc.9.keys.4=type
#*.*.qs.settings.desc.9.true.true.3040.u.value=sep,value:ledig mellan v,sub:aws\:5,value: och v,sub:awe\:5
#*.*.qs.settings.desc.9.true.true.3080.u.value=sep,value:ledig mellan v,sub:aws\:5,value: och v,sub:awe\:5
#*.*.qs.settings.desc.9.true.false.3040.u.value=sep,value:ledig tidigast v,sub:aws\:5
#*.*.qs.settings.desc.9.true.false.3080.u.value=sep,value:ledig tidigast v,sub:aws\:5
#*.*.qs.settings.desc.9.false.true.3040.u.value=sep,value:ledig senast v,sub:aws\:5
#*.*.qs.settings.desc.9.false.true.3080.u.value=sep,value:ledig senast v,sub:aws\:5
#*.*.qs.settings.desc.9.continue=1

# Beds
*.*.qs.settings.desc.10.keys.1=?bs
*.*.qs.settings.desc.10.keys.3=cg
*.*.qs.settings.desc.10.keys.4=type
*.*.qs.settings.desc.10.true.1080.*.value=sep,value:beds from ,qs:bs
*.*.qs.settings.desc.10.continue=1

*.*.qs.settings.desc.11.keys.1=?be
*.*.qs.settings.desc.11.keys.2=cg
*.*.qs.settings.desc.11.keys.3=type
*.*.qs.settings.desc.11.true.1080.*.value=sep,value:beds to ,qs:be
*.*.qs.settings.desc.11.continue=1

# Rooms
*.*.qs.settings.desc.12.keys.1=?ros
*.*.qs.settings.desc.12.keys.2=?roe
*.*.qs.settings.desc.12.keys.3=cg
*.*.qs.settings.desc.12.keys.4=type
*.*.qs.settings.desc.12.true.true.1020.s.value=sep,value:antal rum ,qs:ros,value: - ,qs:roe
*.*.qs.settings.desc.12.true.true.1020.u.value=sep,value:antal rum ,qs:ros,value: - ,qs:roe
*.*.qs.settings.desc.12.true.false.1020.s.value=sep,value:rum fr o m ,qs:ros
*.*.qs.settings.desc.12.true.false.1020.u.value=sep,value:rum fr o m ,qs:ros
*.*.qs.settings.desc.12.false.true.1020.s.value=sep,value:rum t o m ,qs:roe
*.*.qs.settings.desc.12.false.true.1020.u.value=sep,value:rum t o m ,qs:roe
*.*.qs.settings.desc.12.true.true.1040.s.value=sep,value:antal rum ,qs:ros,value: - ,qs:roe
*.*.qs.settings.desc.12.true.true.1040.u.value=sep,value:antal rum ,qs:ros,value: - ,qs:roe
*.*.qs.settings.desc.12.true.false.1040.s.value=sep,value:rum fr o m ,qs:ros
*.*.qs.settings.desc.12.true.false.1040.u.value=sep,value:rum fr o m ,qs:ros
*.*.qs.settings.desc.12.false.true.1040.s.value=sep,value:rum t o m ,qs:roe
*.*.qs.settings.desc.12.false.true.1040.u.value=sep,value:rum t o m ,qs:roe
*.*.qs.settings.desc.12.continue=1

# Size
*.*.qs.settings.desc.13.keys.1=?ss
*.*.qs.settings.desc.13.keys.2=?se
*.*.qs.settings.desc.13.keys.3=cg
*.*.qs.settings.desc.13.keys.4=type
*.*.qs.settings.desc.13.true.true.1020.s.value=sep,value:boyta ,map:size_desc\:ss,value: - ,map:size_desc\:se
*.*.qs.settings.desc.13.true.true.1020.u.value=sep,value:boyta ,map:size_desc\:ss,value: - ,map:size_desc\:se
*.*.qs.settings.desc.13.true.false.1020.s.value=sep,value:boyta fr o m ,map:size_desc\:ss
*.*.qs.settings.desc.13.true.false.1020.u.value=sep,value:boyta fr o m ,map:size_desc\:ss
*.*.qs.settings.desc.13.false.true.1020.s.value=sep,value:boyta t o m ,map:size_desc\:se
*.*.qs.settings.desc.13.false.true.1020.u.value=sep,value:boyta t o m ,map:size_desc\:se
*.*.qs.settings.desc.13.true.true.1040.s.value=sep,value:boyta ,map:size_desc\:ss,value: - ,map:size_desc\:se
*.*.qs.settings.desc.13.true.true.1040.u.value=sep,value:boyta ,map:size_desc\:ss,value: - ,map:size_desc\:se
*.*.qs.settings.desc.13.true.false.1040.s.value=sep,value:boyta fr o m ,map:size_desc\:ss
*.*.qs.settings.desc.13.true.false.1040.u.value=sep,value:boyta fr o m ,map:size_desc\:ss
*.*.qs.settings.desc.13.false.true.1040.s.value=sep,value:boyta t o m ,map:size_desc\:se
*.*.qs.settings.desc.13.false.true.1040.u.value=sep,value:boyta t o m ,map:size_desc\:se
*.*.qs.settings.desc.13.true.true.1080.s.value=sep,value:boyta ,map:size_desc\:ss,value: - ,map:size_desc\:se
*.*.qs.settings.desc.13.true.true.1080.u.value=sep,value:boyta ,map:size_desc\:ss,value: - ,map:size_desc\:se
*.*.qs.settings.desc.13.true.false.1080.s.value=sep,value:boyta fr o m ,map:size_desc\:ss
*.*.qs.settings.desc.13.true.false.1080.u.value=sep,value:boyta fr o m ,map:size_desc\:ss
*.*.qs.settings.desc.13.false.true.1080.s.value=sep,value:boyta t o m ,map:size_desc\:se
*.*.qs.settings.desc.13.false.true.1080.u.value=sep,value:boyta t o m ,map:size_desc\:se

*.*.qs.settings.desc.13.continue=1

# Gearbox
*.*.qs.settings.desc.17.keys.1=?gb
*.*.qs.settings.desc.17.keys.2=cg
*.*.qs.settings.desc.17.keys.3=type
*.*.qs.settings.desc.17.true.2020.s.value=sep,map:gearbox\:gb,value: gearbox
*.*.qs.settings.desc.17.continue=1

# Fuel
*.*.qs.settings.desc.18.keys.1=?fu
*.*.qs.settings.desc.18.keys.2=cg
*.*.qs.settings.desc.18.keys.3=type
*.*.qs.settings.desc.18.true.2020.s.value=sep,map:fuel\:fu,value: fuel
*.*.qs.settings.desc.18.true.2040.s.value=sep,map:fuel\:fu,value: fuel
*.*.qs.settings.desc.18.continue=1

# Exclude category
#*.*.qs.settings.desc.22.keys.1=xc
#*.*.qs.settings.desc.22.keys.2=cg
#*.*.qs.settings.desc.22.keys.3=type
#*.*.qs.settings.desc.22.keys.4=?c
#*.*.qs.settings.desc.22.keys.5=c
#*.*.qs.settings.desc.22.1143.1140.s.false.*.value=sep,value:ej fyrhjuling/ATV
#*.*.qs.settings.desc.22.1143.1140.s.true.0.value=sep,value:ej fyrhjuling/ATV

# Cartype
*.*.qs.settings.desc.23.keys.1=?ctp
*.*.qs.settings.desc.23.keys.2=cg
*.*.qs.settings.desc.23.true.2020.value=sep,map:cartype\:ctp,value: cartype
*.*.qs.settings.desc.23.continue=1

# CC
*.*.qs.settings.desc.23.keys.1=?cc
*.*.qs.settings.desc.23.keys.2=cg
*.*.qs.settings.desc.23.true.2060.value=sep,map:cubiccms_desc\:cc,value: cubiccms
*.*.qs.settings.desc.23.continue=1

# Clothes condition
*.*.qs.settings.desc.24.keys.1=?cond
*.*.qs.settings.desc.24.keys.2=cg
#*.*.qs.settings.desc.24.true.5080.value=sep,map:condition\:cond,value: condition
*.*.qs.settings.desc.24.true.4020.value=sep,map:condition\:cond,value: condition
*.*.qs.settings.desc.24.true.4080.value=sep,map:condition\:cond,value: condition
*.*.qs.settings.desc.24.true.5120.value=sep,map:condition\:cond,value: condition
*.*.qs.settings.desc.24.true.4040.value=sep,map:condition\:cond,value: condition
*.*.qs.settings.desc.24.true.9020.value=sep,map:condition\:cond,value: condition
*.*.qs.settings.desc.24.true.9040.value=sep,map:condition\:cond,value: condition
*.*.qs.settings.desc.24.true.9060.value=sep,map:condition\:cond,value: condition
#*.*.qs.settings.desc.24.true.5160.value=sep,map:condition\:cond,value: condition
*.*.qs.settings.desc.24.continue=1

# Clothes gender
*.*.qs.settings.desc.25.keys.1=?gend
*.*.qs.settings.desc.25.keys.2=cg
#*.*.qs.settings.desc.25.true.5080.value=sep,map:gender\:gend,value: gender
*.*.qs.settings.desc.25.true.4020.value=sep,map:gender\:gend,value: gender
*.*.qs.settings.desc.25.true.5120.value=sep,map:gender\:gend,value: gender
*.*.qs.settings.desc.25.true.4040.value=sep,map:gender\:gend,value: gender
#*.*.qs.settings.desc.25.true.5160.value=sep,map:gender\:gend,value: gender
*.*.qs.settings.desc.25.continue=1

# Brand
*.*.qs.settings.desc.26.keys.1=?br
*.*.qs.settings.desc.26.keys.2=cg
*.*.qs.settings.desc.26.true.2020.value=sep,map:brand\:br,value: brand
*.*.qs.settings.desc.26.continue=1

# Model
*.*.qs.settings.desc.27.keys.1=?mo
*.*.qs.settings.desc.27.keys.2=cg
*.*.qs.settings.desc.27.true.2020.value=sep,map:model\:mo,value: model
*.*.qs.settings.desc.27.continue=1

#########
#Geographic level attribute
*.*.qs.settings.params.100.keys.1=appl
*.*.qs.settings.params.100.keys.2=w
*.*.qs.settings.params.100.keys.3=?id
*.*.qs.settings.params.100.*.1.false.value=value: region\:,qs:region
########## END_SETTINGS ##############


#
# Conversion maps used for params
#
*.*.qs.map.cartype.1=Passeio
*.*.qs.map.cartype.2=Conversivel
*.*.qs.map.cartype.3=Pick-up
*.*.qs.map.cartype.4=Antigo
*.*.qs.map.cartype.5=SUV

*.*.qs.map.ros.1=1
*.*.qs.map.ros.2=2
*.*.qs.map.ros.3=3
*.*.qs.map.ros.4=4
*.*.qs.map.ros.5=

*.*.qs.map.roe.1=1
*.*.qs.map.roe.2=2
*.*.qs.map.roe.3=3
*.*.qs.map.roe.4=4
*.*.qs.map.roe.5=

*.*.qs.map.brs.1=1
*.*.qs.map.brs.2=2
*.*.qs.map.brs.3=3
*.*.qs.map.brs.4=4
*.*.qs.map.brs.5=

*.*.qs.map.bre.1=1
*.*.qs.map.bre.2=2
*.*.qs.map.bre.3=3
*.*.qs.map.bre.4=4
*.*.qs.map.bre.5=


*.*.qs.map.cubiccms_desc.1=50 cc
*.*.qs.map.cubiccms_desc.2=125 cc
*.*.qs.map.cubiccms_desc.3=250 cc
*.*.qs.map.cubiccms_desc.4=500 cc
*.*.qs.map.cubiccms_desc.5=750 cc
*.*.qs.map.cubiccms_desc.6=1.000 cc
*.*.qs.map.cubiccms_desc.7=M�s de 1.000 cc
*.*.qs.map.cubiccms_desc.8=100 cc
*.*.qs.map.cubiccms_desc.9=150 cc
*.*.qs.map.cubiccms_desc.10=180 cc
*.*.qs.map.cubiccms_desc.11=200 cc
*.*.qs.map.cubiccms_desc.12=300 cc
*.*.qs.map.cubiccms_desc.13=350 cc
*.*.qs.map.cubiccms_desc.14=400 cc
*.*.qs.map.cubiccms_desc.15=450 cc
*.*.qs.map.cubiccms_desc.16=550 cc
*.*.qs.map.cubiccms_desc.17=600 cc
*.*.qs.map.cubiccms_desc.18=650 cc
*.*.qs.map.cubiccms_desc.19=700 cc
*.*.qs.map.cubiccms_desc.20=800 cc
*.*.qs.map.cubiccms_desc.21=850 cc
*.*.qs.map.cubiccms_desc.22=900 cc
*.*.qs.map.cubiccms_desc.23=950 cc

*.*.qs.map.cubiccms.1=50
*.*.qs.map.cubiccms.2=125
*.*.qs.map.cubiccms.3=250
*.*.qs.map.cubiccms.4=500
*.*.qs.map.cubiccms.5=750
*.*.qs.map.cubiccms.6=1000
*.*.qs.map.cubiccms.7=10000
*.*.qs.map.cubiccms.8=100
*.*.qs.map.cubiccms.9=150
*.*.qs.map.cubiccms.10=180
*.*.qs.map.cubiccms.11=200
*.*.qs.map.cubiccms.12=300
*.*.qs.map.cubiccms.13=350
*.*.qs.map.cubiccms.14=400
*.*.qs.map.cubiccms.15=450
*.*.qs.map.cubiccms.16=550
*.*.qs.map.cubiccms.17=600
*.*.qs.map.cubiccms.18=650
*.*.qs.map.cubiccms.19=700
*.*.qs.map.cubiccms.20=800
*.*.qs.map.cubiccms.21=850
*.*.qs.map.cubiccms.22=900
*.*.qs.map.cubiccms.23=950

*.*.qs.map.condition.1=Nuevo
*.*.qs.map.condition.2=Usado

*.*.qs.map.gender.1=Feminina
*.*.qs.map.gender.2=Masculina
*.*.qs.map.gender.3=Unisex

*.*.qs.map.monthly_rent.1=500
*.*.qs.map.monthly_rent.2=1000
*.*.qs.map.monthly_rent.3=1500
*.*.qs.map.monthly_rent.4=2000
*.*.qs.map.monthly_rent.5=2500
*.*.qs.map.monthly_rent.6=3000
*.*.qs.map.monthly_rent.7=3500
*.*.qs.map.monthly_rent.8=4000
*.*.qs.map.monthly_rent.9=4500
*.*.qs.map.monthly_rent.10=5000
*.*.qs.map.monthly_rent.11=5500
*.*.qs.map.monthly_rent.12=6000
*.*.qs.map.monthly_rent.13=6500
*.*.qs.map.monthly_rent.14=7000
*.*.qs.map.monthly_rent.15=

#*.*.qs.map.weekly_rent.1=500
#*.*.qs.map.weekly_rent.2=1000
#*.*.qs.map.weekly_rent.3=1500
#*.*.qs.map.weekly_rent.4=2000
#*.*.qs.map.weekly_rent.5=2500
#*.*.qs.map.weekly_rent.6=3000
#*.*.qs.map.weekly_rent.7=3500
#*.*.qs.map.weekly_rent.8=4000
#*.*.qs.map.weekly_rent.9=4500
#*.*.qs.map.weekly_rent.10=5000
#*.*.qs.map.weekly_rent.11=5500
#*.*.qs.map.weekly_rent.12=6000
#*.*.qs.map.weekly_rent.13=6500
#*.*.qs.map.weekly_rent.14=7000
#*.*.qs.map.weekly_rent.15=

*.*.qs.map.max_rent.1=500
*.*.qs.map.max_rent.2=1000
*.*.qs.map.max_rent.3=1500
*.*.qs.map.max_rent.4=2000
*.*.qs.map.max_rent.5=2500
*.*.qs.map.max_rent.6=3000
*.*.qs.map.max_rent.7=3500
*.*.qs.map.max_rent.8=4000
*.*.qs.map.max_rent.9=4500
*.*.qs.map.max_rent.10=5000
*.*.qs.map.max_rent.11=5500
*.*.qs.map.max_rent.12=6000
*.*.qs.map.max_rent.13=6500
*.*.qs.map.max_rent.14=7000
*.*.qs.map.max_rent.15=8000
*.*.qs.map.max_rent.16=9000
*.*.qs.map.max_rent.17=10000
*.*.qs.map.max_rent.18=12000
*.*.qs.map.max_rent.19=14000
*.*.qs.map.max_rent.20=14001

*.*.qs.map.size.0=0
*.*.qs.map.size.1=25
*.*.qs.map.size.2=35
*.*.qs.map.size.3=50
*.*.qs.map.size.4=60
*.*.qs.map.size.5=70
*.*.qs.map.size.6=80
*.*.qs.map.size.7=90
*.*.qs.map.size.8=100
*.*.qs.map.size.9=125
*.*.qs.map.size.10=150
*.*.qs.map.size.11=175
*.*.qs.map.size.12=200
*.*.qs.map.size.13=
*.*.qs.map.size.14=300
*.*.qs.map.size.15=400
*.*.qs.map.size.16=500
*.*.qs.map.size.17=800
*.*.qs.map.size.18=1000
*.*.qs.map.size.19=1500
*.*.qs.map.size.20=2000
*.*.qs.map.size.21=2500
*.*.qs.map.size.22=3000
*.*.qs.map.size.23=

*.*.qs.map.size_0.0=0
*.*.qs.map.size_0.1=30
*.*.qs.map.size_0.2=60
*.*.qs.map.size_0.3=90
*.*.qs.map.size_0.4=120
*.*.qs.map.size_0.5=150
*.*.qs.map.size_0.6=180
*.*.qs.map.size_0.7=200
*.*.qs.map.size_0.8=250
*.*.qs.map.size_0.9=300
*.*.qs.map.size_0.10=400
*.*.qs.map.size_0.11=500

*.*.qs.map.size_1.0=0
*.*.qs.map.size_1.1=100
*.*.qs.map.size_1.2=150
*.*.qs.map.size_1.3=200
*.*.qs.map.size_1.4=250
*.*.qs.map.size_1.5=300
*.*.qs.map.size_1.6=400
*.*.qs.map.size_1.7=500
*.*.qs.map.size_1.8=750
*.*.qs.map.size_1.9=1000
*.*.qs.map.size_1.10=1500
*.*.qs.map.size_1.11=2000
*.*.qs.map.size_1.12=9999999

#Terrenos 
*.*.qs.map.size_2.0=0
*.*.qs.map.size_2.1=250
*.*.qs.map.size_2.2=500
*.*.qs.map.size_2.3=1000
*.*.qs.map.size_2.4=2500
*.*.qs.map.size_2.5=5000
*.*.qs.map.size_2.6=10000
*.*.qs.map.size_2.7=100000
*.*.qs.map.size_2.8=500000
*.*.qs.map.size_2.9=99999999

# Used for real estates - buy/sell
*.*.common.pricelist_0.0=0
*.*.common.pricelist_0.1=500000
*.*.common.pricelist_0.2=750000
*.*.common.pricelist_0.3=1000000
*.*.common.pricelist_0.4=2000000
*.*.common.pricelist_0.5=4000000
*.*.common.pricelist_0.6=6000000
*.*.common.pricelist_0.7=8000000
*.*.common.pricelist_0.8=10000000
*.*.common.pricelist_0.9=15000000
*.*.common.pricelist_0.10=20000000
*.*.common.pricelist_0.11=25000000
*.*.common.pricelist_0.12=30000000
*.*.common.pricelist_0.13=40000000
*.*.common.pricelist_0.14=50000000
*.*.common.pricelist_0.15=60000000
*.*.common.pricelist_0.16=70000000
*.*.common.pricelist_0.17=80000000
*.*.common.pricelist_0.18=90000000
*.*.common.pricelist_0.19=100000000
*.*.common.pricelist_0.20=150000000
*.*.common.pricelist_0.21=200000000
*.*.common.pricelist_0.22=300000000
*.*.common.pricelist_0.23=400000000
*.*.common.pricelist_0.24=500000000
*.*.common.pricelist_0.25=600000000
*.*.common.pricelist_0.26=700000000
*.*.common.pricelist_0.27=800000000
*.*.common.pricelist_0.28=900000000
*.*.common.pricelist_0.29=1000000000
*.*.common.pricelist_0.30=2000000000

# Used for vehicles and rest categories
*.*.common.pricelist_1.1=100000
*.*.common.pricelist_1.2=150000
*.*.common.pricelist_1.3=200000
*.*.common.pricelist_1.4=250000
*.*.common.pricelist_1.5=300000
*.*.common.pricelist_1.6=400000
*.*.common.pricelist_1.7=500000
*.*.common.pricelist_1.8=750000
*.*.common.pricelist_1.9=1000000
*.*.common.pricelist_1.10=1500000
*.*.common.pricelist_1.11=2000000
*.*.common.pricelist_1.12=2500000
*.*.common.pricelist_1.13=3000000
*.*.common.pricelist_1.14=4000000
*.*.common.pricelist_1.15=5000000
*.*.common.pricelist_1.16=7500000
*.*.common.pricelist_1.17=10000000
*.*.common.pricelist_1.18=15000000
*.*.common.pricelist_1.19=20000000
*.*.common.pricelist_1.20=25000000
*.*.common.pricelist_1.21=30000000
*.*.common.pricelist_1.22=40000000
*.*.common.pricelist_1.23=50000000
*.*.common.pricelist_1.24=75000000
*.*.common.pricelist_1.25=99999999

# Pricelist 2 used for real estate rent/let
*.*.common.pricelist_2.0=0
*.*.common.pricelist_2.1=100000
*.*.common.pricelist_2.2=200000
*.*.common.pricelist_2.3=300000
*.*.common.pricelist_2.4=400000
*.*.common.pricelist_2.5=500000
*.*.common.pricelist_2.6=600000
*.*.common.pricelist_2.7=700000
*.*.common.pricelist_2.8=800000
*.*.common.pricelist_2.9=900000
*.*.common.pricelist_2.10=1000000
*.*.common.pricelist_2.11=1200000
*.*.common.pricelist_2.12=1400000
*.*.common.pricelist_2.13=1600000
*.*.common.pricelist_2.14=1800000
*.*.common.pricelist_2.15=2000000
*.*.common.pricelist_2.16=3000000
*.*.common.pricelist_2.17=4000000
*.*.common.pricelist_2.18=2000000000

# Pricelist Things for home
*.*.common.pricelist_3.1=10000
*.*.common.pricelist_3.2=30000
*.*.common.pricelist_3.3=50000
*.*.common.pricelist_3.4=75000
*.*.common.pricelist_3.5=100000
*.*.common.pricelist_3.6=150000
*.*.common.pricelist_3.7=200000
*.*.common.pricelist_3.8=250000
*.*.common.pricelist_3.9=300000
*.*.common.pricelist_3.10=400000
*.*.common.pricelist_3.11=500000
*.*.common.pricelist_3.12=750000
*.*.common.pricelist_3.13=1000000
*.*.common.pricelist_3.14=1500000
*.*.common.pricelist_3.15=2000000
*.*.common.pricelist_3.16=2500000
*.*.common.pricelist_3.17=3000000
*.*.common.pricelist_3.18=4000000
*.*.common.pricelist_3.19=5000000
*.*.common.pricelist_3.20=99999999

# Pricelist for Free time
*.*.common.pricelist_4.1=10000
*.*.common.pricelist_4.2=30000
*.*.common.pricelist_4.3=50000
*.*.common.pricelist_4.4=75000
*.*.common.pricelist_4.5=100000
*.*.common.pricelist_4.6=150000
*.*.common.pricelist_4.7=200000
*.*.common.pricelist_4.8=250000
*.*.common.pricelist_4.9=300000
*.*.common.pricelist_4.10=400000
*.*.common.pricelist_4.11=500000
*.*.common.pricelist_4.12=750000
*.*.common.pricelist_4.13=1000000
*.*.common.pricelist_4.14=1500000
*.*.common.pricelist_4.15=2000000
*.*.common.pricelist_4.16=2500000
*.*.common.pricelist_4.17=3000000
*.*.common.pricelist_4.18=4000000
*.*.common.pricelist_4.19=5000000
*.*.common.pricelist_4.20=7500000
*.*.common.pricelist_4.21=10000000
*.*.common.pricelist_4.22=99999999

# Pricelist for Computers and electronics
*.*.common.pricelist_5.1=10000
*.*.common.pricelist_5.2=30000
*.*.common.pricelist_5.3=50000
*.*.common.pricelist_5.4=75000
*.*.common.pricelist_5.5=100000
*.*.common.pricelist_5.6=150000
*.*.common.pricelist_5.7=200000
*.*.common.pricelist_5.8=250000
*.*.common.pricelist_5.9=300000
*.*.common.pricelist_5.10=400000
*.*.common.pricelist_5.11=500000
*.*.common.pricelist_5.12=750000
*.*.common.pricelist_5.13=1000000
*.*.common.pricelist_5.14=1500000
*.*.common.pricelist_5.15=2000000
*.*.common.pricelist_5.16=2500000
*.*.common.pricelist_5.17=3000000
*.*.common.pricelist_5.18=4000000
*.*.common.pricelist_5.19=5000000
*.*.common.pricelist_5.20=99999999

# Pricelist for Jobs and bussines
# For Industrial devices
*.*.common.pricelist_6.1=10000
*.*.common.pricelist_6.2=30000
*.*.common.pricelist_6.3=50000
*.*.common.pricelist_6.4=75000
*.*.common.pricelist_6.5=100000
*.*.common.pricelist_6.6=150000
*.*.common.pricelist_6.7=200000
*.*.common.pricelist_6.8=250000
*.*.common.pricelist_6.9=300000
*.*.common.pricelist_6.10=400000
*.*.common.pricelist_6.11=500000
*.*.common.pricelist_6.12=750000
*.*.common.pricelist_6.13=1000000
*.*.common.pricelist_6.14=1500000
*.*.common.pricelist_6.15=2000000
*.*.common.pricelist_6.16=2500000
*.*.common.pricelist_6.17=3000000
*.*.common.pricelist_6.18=4000000
*.*.common.pricelist_6.19=5000000
*.*.common.pricelist_6.20=7500000
*.*.common.pricelist_6.21=10000000
*.*.common.pricelist_6.22=15000000
*.*.common.pricelist_6.23=20000000
*.*.common.pricelist_6.24=25000000
*.*.common.pricelist_6.25=30000000
*.*.common.pricelist_6.26=40000000
*.*.common.pricelist_6.27=50000000
*.*.common.pricelist_6.28=75000000
*.*.common.pricelist_6.29=100000000
*.*.common.pricelist_6.30=150000000
*.*.common.pricelist_6.31=200000000
*.*.common.pricelist_6.32=250000000
*.*.common.pricelist_6.33=300000000
*.*.common.pricelist_6.34=400000000
*.*.common.pricelist_6.35=999999999

# Pricelist for other products
*.*.common.pricelist_7.1=10000
*.*.common.pricelist_7.2=30000
*.*.common.pricelist_7.3=50000
*.*.common.pricelist_7.4=75000
*.*.common.pricelist_7.5=100000
*.*.common.pricelist_7.6=150000
*.*.common.pricelist_7.7=200000
*.*.common.pricelist_7.8=250000
*.*.common.pricelist_7.9=300000
*.*.common.pricelist_7.10=400000
*.*.common.pricelist_7.11=500000
*.*.common.pricelist_7.12=750000
*.*.common.pricelist_7.13=1000000
*.*.common.pricelist_7.14=1500000
*.*.common.pricelist_7.15=2000000
*.*.common.pricelist_7.16=2500000
*.*.common.pricelist_7.17=3000000
*.*.common.pricelist_7.18=4000000
*.*.common.pricelist_7.19=5000000
*.*.common.pricelist_7.20=7500000
*.*.common.pricelist_7.21=10000000
*.*.common.pricelist_7.22=15000000
*.*.common.pricelist_7.23=20000000
*.*.common.pricelist_7.24=25000000
*.*.common.pricelist_7.25=30000000
*.*.common.pricelist_7.26=40000000
*.*.common.pricelist_7.27=99999999

# Pricelist for cars and others
*.*.common.pricelist_8.1=0
*.*.common.pricelist_8.2=250000
*.*.common.pricelist_8.3=500000
*.*.common.pricelist_8.4=1000000
*.*.common.pricelist_8.5=1500000
*.*.common.pricelist_8.6=2000000
*.*.common.pricelist_8.7=2500000
*.*.common.pricelist_8.8=3000000
*.*.common.pricelist_8.9=3500000
*.*.common.pricelist_8.10=4000000
*.*.common.pricelist_8.11=4500000
*.*.common.pricelist_8.12=5000000
*.*.common.pricelist_8.13=5500000
*.*.common.pricelist_8.14=6000000
*.*.common.pricelist_8.15=6500000
*.*.common.pricelist_8.16=7000000
*.*.common.pricelist_8.17=7500000
*.*.common.pricelist_8.18=8000000
*.*.common.pricelist_8.19=8500000
*.*.common.pricelist_8.20=9000000
*.*.common.pricelist_8.21=9500000
*.*.common.pricelist_8.22=10000000
*.*.common.pricelist_8.23=10500000
*.*.common.pricelist_8.24=11000000
*.*.common.pricelist_8.25=11500000
*.*.common.pricelist_8.26=12000000
*.*.common.pricelist_8.27=12500000
*.*.common.pricelist_8.28=13000000
*.*.common.pricelist_8.29=13500000
*.*.common.pricelist_8.30=14000000
*.*.common.pricelist_8.31=14500000
*.*.common.pricelist_8.32=15000000
*.*.common.pricelist_8.33=15500000
*.*.common.pricelist_8.34=16000000
*.*.common.pricelist_8.35=16500000
*.*.common.pricelist_8.36=17000000
*.*.common.pricelist_8.37=17500000
*.*.common.pricelist_8.38=18000000
*.*.common.pricelist_8.39=18500000
*.*.common.pricelist_8.40=19000000
*.*.common.pricelist_8.41=19500000
*.*.common.pricelist_8.42=20000000
*.*.common.pricelist_8.43=40000000
*.*.common.pricelist_8.44=60000000
*.*.common.pricelist_8.45=999999999

# Pricelist for Arriendos de Temporada
*.*.common.pricelist_9.1=0
*.*.common.pricelist_9.2=10000
*.*.common.pricelist_9.3=20000
*.*.common.pricelist_9.4=30000
*.*.common.pricelist_9.5=40000
*.*.common.pricelist_9.6=50000
*.*.common.pricelist_9.7=60000
*.*.common.pricelist_9.8=70000
*.*.common.pricelist_9.9=80000
*.*.common.pricelist_9.10=90000
*.*.common.pricelist_9.11=100000
*.*.common.pricelist_9.12=150000
*.*.common.pricelist_9.13=2000000000

# Pricelist for Comprar
*.*.common.pricelist_10.0=0
*.*.common.pricelist_10.1=10000000
*.*.common.pricelist_10.2=20000000
*.*.common.pricelist_10.3=30000000
*.*.common.pricelist_10.4=40000000
*.*.common.pricelist_10.5=50000000
*.*.common.pricelist_10.6=60000000
*.*.common.pricelist_10.7=70000000
*.*.common.pricelist_10.8=80000000
*.*.common.pricelist_10.9=90000000
*.*.common.pricelist_10.10=100000000
*.*.common.pricelist_10.11=120000000
*.*.common.pricelist_10.12=140000000
*.*.common.pricelist_10.13=160000000
*.*.common.pricelist_10.14=180000000
*.*.common.pricelist_10.15=200000000
*.*.common.pricelist_10.16=220000000
*.*.common.pricelist_10.17=240000000
*.*.common.pricelist_10.18=260000000
*.*.common.pricelist_10.19=280000000
*.*.common.pricelist_10.20=300000000
*.*.common.pricelist_10.21=400000000
*.*.common.pricelist_10.22=500000000
*.*.common.pricelist_10.23=600000000
*.*.common.pricelist_10.24=700000000
*.*.common.pricelist_10.25=800000000
*.*.common.pricelist_10.26=900000000

# Pricelist Ppages for Comprar
*.*.common.ppages_pricelist_1220.0=0
*.*.common.ppages_pricelist_1220.1=59000000
*.*.common.ppages_pricelist_1220.2=147000000
*.*.common.ppages_pricelist_1220.3=206000000
*.*.common.ppages_pricelist_1220.4=265000000
*.*.common.ppages_pricelist_1220.5=353000000
*.*.common.ppages_pricelist_1220.6=441000000
*.*.common.ppages_pricelist_1220.7=529000000
*.*.common.ppages_pricelist_1220.8=620000000
*.*.common.ppages_pricelist_1220.9=735000000
*.*.common.ppages_pricelist_1220.10=2147483646

*.*.common.condominio_list.0=0
*.*.common.condominio_list.1=25000
*.*.common.condominio_list.2=50000
*.*.common.condominio_list.3=75000
*.*.common.condominio_list.4=100000
*.*.common.condominio_list.5=150000
*.*.common.condominio_list.6=200000
*.*.common.condominio_list.7=99999999

*.*.qs.map.mileage_desc.1=0
*.*.qs.map.mileage_desc.2=5.000
*.*.qs.map.mileage_desc.3=10.000
*.*.qs.map.mileage_desc.4=20.000
*.*.qs.map.mileage_desc.5=30.000
*.*.qs.map.mileage_desc.6=40.000
*.*.qs.map.mileage_desc.7=60.000
*.*.qs.map.mileage_desc.8=80.000
*.*.qs.map.mileage_desc.9=100.000
*.*.qs.map.mileage_desc.10=120.000
*.*.qs.map.mileage_desc.11=140.000
*.*.qs.map.mileage_desc.12=160.000
*.*.qs.map.mileage_desc.13=180.000
*.*.qs.map.mileage_desc.14=200.000
*.*.qs.map.mileage_desc.15=250.000
*.*.qs.map.mileage_desc.16=300.000
*.*.qs.map.mileage_desc.17=400.000
*.*.qs.map.mileage_desc.18=500.000
*.*.qs.map.mileage_desc.19=M�s de 500.000

*.*.qs.map.mileage.1=0
*.*.qs.map.mileage.2=5000
*.*.qs.map.mileage.3=10000
*.*.qs.map.mileage.4=20000
*.*.qs.map.mileage.5=30000
*.*.qs.map.mileage.6=40000
*.*.qs.map.mileage.7=60000
*.*.qs.map.mileage.8=80000
*.*.qs.map.mileage.9=100000
*.*.qs.map.mileage.10=120000
*.*.qs.map.mileage.11=140000
*.*.qs.map.mileage.12=160000
*.*.qs.map.mileage.13=180000
*.*.qs.map.mileage.14=200000
*.*.qs.map.mileage.15=250000
*.*.qs.map.mileage.16=300000
*.*.qs.map.mileage.17=400000
*.*.qs.map.mileage.18=500000
*.*.qs.map.mileage.19=999999

*.*.qs.map.monthly_rent_desc.1=500
*.*.qs.map.monthly_rent_desc.2=1 000
*.*.qs.map.monthly_rent_desc.3=1 500
*.*.qs.map.monthly_rent_desc.4=2 000
*.*.qs.map.monthly_rent_desc.5=2 500
*.*.qs.map.monthly_rent_desc.6=3 000
*.*.qs.map.monthly_rent_desc.7=3 500
*.*.qs.map.monthly_rent_desc.8=4 000
*.*.qs.map.monthly_rent_desc.9=4 500
*.*.qs.map.monthly_rent_desc.10=5 000
*.*.qs.map.monthly_rent_desc.11=5 500
*.*.qs.map.monthly_rent_desc.12=6 000
*.*.qs.map.monthly_rent_desc.13=6 500
*.*.qs.map.monthly_rent_desc.14=7 000
*.*.qs.map.monthly_rent_desc.15=more than 7 000

#*.*.qs.map.weekly_rent_desc.1=500
#*.*.qs.map.weekly_rent_desc.2=1 000
#*.*.qs.map.weekly_rent_desc.3=1 500
#*.*.qs.map.weekly_rent_desc.4=2 000
#*.*.qs.map.weekly_rent_desc.5=2 500
#*.*.qs.map.weekly_rent_desc.6=3 000
#*.*.qs.map.weekly_rent_desc.7=3 500
#*.*.qs.map.weekly_rent_desc.8=4 000
#*.*.qs.map.weekly_rent_desc.9=4 500
#*.*.qs.map.weekly_rent_desc.10=5 000
#*.*.qs.map.weekly_rent_desc.11=5 500
#*.*.qs.map.weekly_rent_desc.13=6 500
#*.*.qs.map.weekly_rent_desc.14=7 000
#*.*.qs.map.weekly_rent_desc.15=more than 7 000

# Remove 500 steps if used for selectbox
*.*.qs.map.max_rent_desc.1=500
*.*.qs.map.max_rent_desc.2=1 000
*.*.qs.map.max_rent_desc.3=1 500
*.*.qs.map.max_rent_desc.4=2 000
*.*.qs.map.max_rent_desc.5=2 500
*.*.qs.map.max_rent_desc.6=3 000
*.*.qs.map.max_rent_desc.7=3 500
*.*.qs.map.max_rent_desc.8=4 000
*.*.qs.map.max_rent_desc.9=4 500
*.*.qs.map.max_rent_desc.10=5 000
*.*.qs.map.max_rent_desc.11=5 500
*.*.qs.map.max_rent_desc.12=6 000
*.*.qs.map.max_rent_desc.13=6 500
*.*.qs.map.max_rent_desc.14=7 000
*.*.qs.map.max_rent_desc.15=8 000
*.*.qs.map.max_rent_desc.16=9 000
*.*.qs.map.max_rent_desc.17=10 000
*.*.qs.map.max_rent_desc.18=12 000
*.*.qs.map.max_rent_desc.19=14 000
*.*.qs.map.max_rent_desc.20=Over 14 000

*.*.qs.map.size_desc.0=0
*.*.qs.map.size_desc.1=25
*.*.qs.map.size_desc.2=35
*.*.qs.map.size_desc.3=50
*.*.qs.map.size_desc.4=60
*.*.qs.map.size_desc.5=70
*.*.qs.map.size_desc.6=80
*.*.qs.map.size_desc.7=90
*.*.qs.map.size_desc.8=100
*.*.qs.map.size_desc.9=125
*.*.qs.map.size_desc.10=150

### Limit of ads per page
*.*.common.ads_per_page.th=50
*.*.common.ads_per_page.mp=20
*.*.common.ads_per_page.li=100
*.*.common.ads_per_page.default=100

# Josesearch config

*.*.ppqs.vars.sort.regex=^[012]?$
*.*.ppqs.vars.promo.regex=^[0-9]{0,10}$
*.*.ppqs.vars.reg.regex=^[0-9]{1,2}$
*.*.ppqs.vars.cmn.regex=^[0-9]{1,3}(,[0-9]{1,3})*$
*.*.ppqs.vars.cmn.multiple=1
*.*.ppqs.vars.roe.regex=^[0-9]{1,2}$
*.*.ppqs.vars.ros.regex=^[0-9]{1,2}$
*.*.ppqs.vars.bre.regex=^[0-9]{1,2}$
*.*.ppqs.vars.brs.regex=^[0-9]{1,2}$
*.*.ppqs.vars.ret.regex=^[0-9]{1,2}$
*.*.ppqs.vars.ss.regex=^[0-9]{1,2}$
*.*.ppqs.vars.se.regex=^[0-9]{1,2}$
*.*.ppqs.vars.cat.regex=^[0-9]{4}$
*.*.ppqs.vars.query.regex=^[\x20-\xff]{1,500}$
*.*.ppqs.vars.ps.regex=^[0-9]{1,2}$
*.*.ppqs.vars.pe.regex=^[0-9]{1,2}$

*.*.ppqs.settings.filter.1.keys.1=sort
*.*.ppqs.settings.filter.1.1.value=value: subsort\:+
*.*.ppqs.settings.filter.1.2.value=value: subsort\:-
*.*.ppqs.settings.filter.1.*.value=none
*.*.ppqs.settings.filter.1.continue=1

*.*.ppqs.settings.params.1.keys.1=?promo
*.*.ppqs.settings.params.1.true.value=value: pp_id\:,qs:promo
*.*.ppqs.settings.params.1.continue=1

*.*.ppqs.settings.params.2.keys.1=?cat
*.*.ppqs.settings.params.2.true.value=value: category\:,qs:cat
*.*.ppqs.settings.params.2.continue=1

*.*.ppqs.settings.params.3.keys.1=?reg
*.*.ppqs.settings.params.3.true.value=value: region\:,qs:reg
*.*.ppqs.settings.params.3.continue=1

*.*.ppqs.settings.params.4.keys.1=?cmn
*.*.ppqs.settings.params.4.true.value=value: communes\:,qs:cmn
*.*.ppqs.settings.params.4.continue=1

# Rooms
*.*.ppqs.settings.params.5.keys.1=?ros
*.*.ppqs.settings.params.5.keys.2=?roe
*.*.ppqs.settings.params.5.true.true.value=value: rooms\:,map:ros\:ros,value:-,map:roe\:roe
*.*.ppqs.settings.params.5.true.false.value=value: rooms\:,map:ros\:ros,value:-
*.*.ppqs.settings.params.5.false.true.value=value: rooms\:-,map:roe\:roe
*.*.ppqs.settings.params.5.continue=1

# Bathrooms
*.*.ppqs.settings.params.6.keys.1=?brs
*.*.ppqs.settings.params.6.keys.2=?bre
*.*.ppqs.settings.params.6.true.true.value=value: bathrooms\:,map:brs\:brs,value:-,map:bre\:bre
*.*.ppqs.settings.params.6.true.false.value=value: bathrooms\:,map:brs\:brs,value:-
*.*.ppqs.settings.params.6.false.true.value=value: bathrooms\:-,map:bre\:bre
*.*.ppqs.settings.params.6.continue=1

# Estate type
*.*.ppqs.settings.params.7.keys.1=?ret
*.*.ppqs.settings.params.7.true.value=value: estate_type\:,qs:ret
*.*.ppqs.settings.params.7.continue=1

# Size
*.*.ppqs.settings.params.8.keys.1=?ss
*.*.ppqs.settings.params.8.keys.2=?se
*.*.ppqs.settings.params.8.keys.3=ret
*.*.ppqs.settings.params.8.true.true.5.value=value: size\:,map:size_2\:ss,value:-,map:size_2\:se
*.*.ppqs.settings.params.8.true.true.6.value=value: size\:,map:size_1\:ss,value:-,map:size_1\:se
*.*.ppqs.settings.params.8.true.true.*.value=value: size\:,map:size_0\:ss,value:-,map:size_0\:se
*.*.ppqs.settings.params.8.true.false.5.value=value: size\:,map:size_2\:ss,value:-
*.*.ppqs.settings.params.8.true.false.6.value=value: size\:,map:size_1\:ss,value:-
*.*.ppqs.settings.params.8.true.false.*.value=value: size\:,map:size_0\:ss,value:-
*.*.ppqs.settings.params.8.false.true.5.value=value: size\:-,map:size_2\:se
*.*.ppqs.settings.params.8.false.true.6.value=value: size\:-,map:size_1\:se
*.*.ppqs.settings.params.8.false.true.*.value=value: size\:-,map:size_0\:se
*.*.ppqs.settings.params.8.continue=1

# Price
*.*.ppqs.settings.params.9.keys.1=?ps
*.*.ppqs.settings.params.9.keys.2=?pe
*.*.ppqs.settings.params.9.keys.3=cat

*.*.ppqs.settings.params.9.true.true.1000.value=value: price\:,map:pricelist_0\:ps,value:-,map:pricelist_0\:pe
*.*.ppqs.settings.params.9.true.false.1000.value=value: price\:,map:pricelist_0\:ps,value:-
*.*.ppqs.settings.params.9.false.true.1000.value=value: price\:1-,map:pricelist_0\:pe

*.*.ppqs.settings.params.9.true.true.1220.value=value: price\:,map:ppages_pricelist_1220\:ps,value:-,map:ppages_pricelist_1220\:pe
*.*.ppqs.settings.params.9.true.false.1220.value=value: price\:,map:ppages_pricelist_1220\:ps,value:-
*.*.ppqs.settings.params.9.false.true.1220.value=value: price\:1-,map:ppages_pricelist_1220\:pe

*.*.ppqs.settings.params.9.true.true.1240.value=value: price\:,map:pricelist_2\:ps,value:-,map:pricelist_2\:pe
*.*.ppqs.settings.params.9.true.false.1240.value=value: price\:,map:pricelist_2\:ps,value:-
*.*.ppqs.settings.params.9.false.true.1240.value=value: price\:1-,map:pricelist_2\:pe

*.*.ppqs.settings.params.9.true.true.1260.value=value: price\:,map:pricelist_9\:ps,value:-,map:pricelist_9\:pe
*.*.ppqs.settings.params.9.true.false.1260.value=value: price\:,map:pricelist_9\:ps,value:-
*.*.ppqs.settings.params.9.false.true.1260.value=value: price\:1-,map:pricelist_9\:pe

*.*.ppqs.settings.params.9.continue=1

*.*.ppqs.settings.query.1.keys.1=?query
*.*.ppqs.settings.query.1.true.value=qs:query

