# BCONF
La carpeta bconf contiene configuraciones del sitio. Los cambios aqui son de alto impacto, por lo que se debe 
editar/eliminar con mucha precaución.

Documentación: https://confluence.schibsted.io/display/SBP/Bconf

Estos archivos son del tipo properties/ini donde las variables se establecen por linea.

## Agregar un nuevo CONF
Agregar una nueva configuración es posible realizando los siguientes pasos

1. Crear un nuevo archivo del tipo `bconf.txt.{descripcion_nombre}` por ejemplo bconf.txt.example_config
2. Agregar contenido, este debe comenzar por `*.*.{tipo}.{subtipo|opcional}.{nombre}.{idioma}` por ejemplo: 
    ``` properties
        *.*.language.aria.TEXTO_EJEMPLO.es=Este es un texto de ejemplo
    ```
3. Agregar el nuevo archivo creado a bconf.txt
    ``` 
    ### Aria Label
    include bconf.txt.aria_label
    
    ### Example
    include bconf.txt.example_conf
    ```
4. Agregar el nuevo archivo creado a Builddesc
    ```
    INSTALL(conf
        conf[
            bconf.txt
            ...
            bconf.txt.aria_label
            bconf.txt.example_conf
            ...
            ]
        )
    ```
5. Agregar nuevo archivo a /rpm/blocket.spec
    ```
    ...
    %config %{blocket}/conf/bconf.txt.language_merken
    %config %{blocket}/conf/bconf.txt.example_conf
    %config %{blocket}/conf/bconf.txt.listing
    ...
    ```
    
Con estos pasos deberia ser suficiente y las variables estarán disponibles para su uso en template.