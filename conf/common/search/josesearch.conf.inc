blocket_id=josesearch
timeout=15
host.1.port=5656

db_name=/var/opt/bsearch/index/josedb
cache_memory.query_cache_mb=512
cache_memory.subquery_cache_mb=50
cache_memory.attrsort_cache_mb=50

locale=es_CL
port.search=6081
port.command=6083
port.keepalive=6085

hundict.es=/opt/blocket/share/search/es_CL

default.lang=es
charmap=/opt/blocket/conf/charmap.es
dont_warm_cache=1

indexer=indexer
index.template=index/joseindex.txt
index.logtag=joseindex
index.loglevel=info
index.shard_size=300000
index.db_delay=0
