blocket_id=asearch
timeout=15
host.1.port=5656
# CHUS640 modification
# host.1.name=10.45.1.20
# host.1.port=5656
# host.2.name=10.45.1.42
# host.2.port=5656
db_name=/var/opt/bsearch/index/adb
threads=200
mmap_section_margin=25
# cache_size=1024
cache_memory.query_cache_mb=4000
cache_memory.subquery_cache_mb=500
cache_memory.attrsort_cache_mb=500
locale=es_CL

port.search=9080
port.command=9082
port.keepalive=9084

hundict.es=/opt/blocket/share/search/es_CL

default.lang=es
charmap=/opt/blocket/conf/charmap.es
dont_warm_cache=1

indexer=indexer
index.template=index/aindex.txt
index.logtag=aindex
#index.loglevel=debug
index.shard_size=300000
index.db_delay=1

index.db.dbname = blocketdb
index.db.user = bsearch
index.db.password = iujuyhgyt3s
index.db.options = -c search_path=%BPVSCHEMA%,public 

syn_regexes.1.regex=\b([13567][1-6][0-9])[ ]*([ISDCXL]{1,2})?\b
syn_regexes.1.with=BMW
syn_regexes.1.match1=1
syn_regexes.1.match2=2
