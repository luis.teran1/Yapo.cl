# Redis configuration file example
#
# NOTE: Comments have been mostly stripped.
# See redis source directory for full configuration file.
#
daemonize yes
# bind 127.0.0.1
timeout 0

pidfile /var/redis/redissession/redis_session.pid
port 6350
loglevel notice
#logfile stdout
# For default save/load DB in/from the working directory
# Note that you must specify a directory not a file name.
dir /var/redis/redissession/db
# The filename where to dump the DB
dbfilename redissessiondump.rdb

databases 8
#   save <seconds> <changes>
save 900 1
save 300 10
save 60 10000

rdbcompression no

appendonly no
appendfsync always

# log
syslog-enabled yes

# Specify the syslog identity.
syslog-ident redis_session

# Specify the syslog facility. Must be USER or between LOCAL0-LOCAL7.
syslog-facility local0
