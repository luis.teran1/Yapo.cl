# Redis configuration file example
#
# NOTE: Comments have been mostly stripped.
# See redis source directory for full configuration file.
#
daemonize yes
# bind 127.0.0.1
timeout 0

pidfile /var/redis/redisstat/redisstat.pid
port 6380
loglevel notice
# logfile stdout
# For default save/load DB in/from the working directory
# Note that you must specify a directory not a file name.
dir /var/redis/redisstat/db
# The filename where to dump the DB
dbfilename redisstatdump.rdb

databases 8
#   save <seconds> <changes>
save 900 1
save 300 10
save 60 10000

rdbcompression no


appendonly no
appendfsync always

#This is for logging in admin server
syslog-enabled yes
syslog-ident redis_stat
syslog-facility local0
