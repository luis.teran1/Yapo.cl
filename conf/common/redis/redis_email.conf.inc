daemonize yes
# bind 127.0.0.1
timeout 0

loglevel notice

# The filename where to dump the DB
dbfilename redisemailddump.rdb

databases 1
#   save <seconds> <changes>
save 900 1
save 300 10
save 60 10000

rdbcompression no

# slaveof <masterip> <masterport>
# masterauth <master-password>
# requirepass foobared
# maxclients 128
appendonly no
appendfsync always

#This is for logging in admin server
syslog-enabled yes
syslog-ident redis_email
syslog-facility local0
