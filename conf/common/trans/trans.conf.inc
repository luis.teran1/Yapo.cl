bconfd =  %DESTDIR%/conf/bconf.conf

db.pgsql.master.engine = PGSQL
db.pgsql.master.name = blocketdb
db.pgsql.master.user = trans
db.pgsql.master.password = transPass
db.pgsql.master.options = -c search_path=%BPVSCHEMA%,public
#db.pgsql.master.max_workers = 100

db.pgsql.slave.engine = PGSQL
db.pgsql.slave.name = blocketdb
db.pgsql.slave.user = trans
db.pgsql.slave.password = transPass
db.pgsql.slave.options = -c search_path=%BPVSCHEMA%,public
#db.pgsql.slave.max_workers = 100

db.pgsql.queue.engine = PGSQL
db.pgsql.queue.name = blocketdb
db.pgsql.queue.user = trans
db.pgsql.queue.password = transPass
db.pgsql.queue.options = -c search_path=%BPVSCHEMA%,public
#db.pgsql.queue.max_workers = 100

port = 5656
failover_port = 5657
control_port = 5660
control_failover_port = 5661

basedir=%DESTDIR%
max_connections=300
max_command_threads=100

geoip.db.required=1
geoip.db.directory=/usr/share/GeoIP

blocket_id=trans

log_level=info
log_tag=trans

rabbitmq.enabled=1
