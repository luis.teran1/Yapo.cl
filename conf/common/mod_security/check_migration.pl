#!/usr/bin/perl -w
#
# Process mod_security 1.x configuration files and suggest 2.x syntax
#

my %location_vardef;

#opendir(HERE,".");
#my @confs=grep { /mod_sec.*\.conf/ } readdir(HERE);
#closedir(HERE);
my @confs=@ARGV;


if (scalar @confs < 1) {
	die "No configuration files found"
}

foreach my $conf (@confs) {
	print STDERR "=== $conf ===\n";
	check_conffile($conf) or exit 1;
}

sub check_conffile {
	my $name = shift;
	open (CONF,$name) or die "cannot open $name";
	my @all = <CONF>;
	chomp @all;
	my $row=0;
	my $location = '';
	my $location_printed = '';
	foreach my $filter_or_rule (@all) {
		$row++;
		if ($filter_or_rule =~ /^[\t\ ]*$/
		or $filter_or_rule =~ /^[\t\ ]*#/) {
			next;
		}
		if ($filter_or_rule =~ /^[\t\ ]*SecRule[\t\ ]+ARGS_NAMES/
		or $filter_or_rule =~ /SecRuleInheritance/) {
			next;
		}
		if ($filter_or_rule =~ /^[\ \t]*<Location/) {
			$location=$filter_or_rule;
			next;
		}
		if ($filter_or_rule =~ /^[\ \t]*<\/?IfModule/) {
			next;
		}

		$ruleq = filter2rule($filter_or_rule,$location);
		if(!defined($ruleq)) {
			$ruleq=$filter_or_rule;
			$filter=rule2filter($filter_or_rule,$location);
		} else {
			$filter = $filter_or_rule;
		}
		if ($filter =~ /^[\ \t]*<\/Location/) {
			xcheck_vars();
			$location='';
		}
		$numfilters = grepcount($filter, $location, @all);	
		$numrules = grepcount($ruleq, $location, @all);	
		if ($numrules < $numfilters) {
			if (length($location) > 0 && $location_printed ne $location) {
				print "$location\n";
				$location_printed=$location;
			}
			print STDERR "# Add ".($numfilters-$numrules)." ($numfilters - $numrules) instance(s) of \n";
			print $ruleq."\n";
			my @matches = grep { /^$filter$/ } @all;
		}
		if ($numrules > $numfilters) {
			print STDERR "Row $row has >>$ruleq<< without corresponding SecFilter\n";	
		}
	}
}

sub filter2rule {
	my ($filter,$location) = @_;
	my $rule = $filter;
	my $vardef;
	$rule =~ /SecRule/ and return undef;
	if($rule =~ s/SecFilterSelective([\ \t]+)ARG_([^\ \t]+)/SecRule$1ARGS:$2/) {
		my $arg=$2;
		if (defined($location_varlist{$arg})  and $location eq $location_varlist{$arg}) {
			delete $location_varlist{$arg};
		}
	}
	$vardef = $rule;
	$vardef =~ s/.*ARGS_NAMES[\ \t]+// and %location_varlist = mkvarlist($vardef,$location);
	$rule =~ s/SecFilterSelective/SecRule/;
	return $rule;
}

sub rule2filter {
	my ($rule,$location) = @_;
	my $filter = $rule;
	$filter =~ /SecFilterSelective/ and return undef;
	$filter =~ s/SecRule([\ \t]+)ARGS:([^\ \t]+)/SecFilterSelective$1ARG_$2/;
	$filter =~ s/SecRule/SecFilterSelective/;
	return $filter;
}

sub grepcount {
	my $needle = shift;
	my $location = shift;
	my @all = @_;
	my $count=0;
	my $loc='';
	$needle =~ s/^[\ \t]*//;

	foreach my $str (@all) {
		$str =~ s/^[\ \t]*//;
		if ($str =~ /^<Location/) {
			$loc=$str;
			next;
		}
		if ($str =~ /SecRule[\ \t]+ARGS:\/?\^?([^\$\/]+)\$?\/?[\ \t]/ ) {
			my $arg=$1;
			if (defined($location_varlist{$arg})  and $loc eq $location_varlist{$arg}) {
				delete $location_varlist{$arg};
			}
		}
		if ($needle eq $str && $location eq $loc) {
			$count++;
		}
	}
	return $count;
}

sub mkvarlist {
	my ($vars,$location)=@_;
	my %vars;
	$vars =~ s/^[\"!\(^]+//;
	$vars =~ s/[\"\)\?\$]+$//;
	foreach $var (split(/\|/,$vars)) {
		$vars{$var}=$location;
	}
	return %vars;
}

sub xcheck_vars {
	my $var;
	foreach $var (keys %location_varlist) {
		print STDERR "WARNING: can't find spec for ARGS:$var location $location_varlist{$var}\n";
		delete $location_varlist{$var};
	}
}
