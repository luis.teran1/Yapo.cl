# RabbitMQ configuration file, flavor: regress

# Virtualhost
rabbitmq.virtual_host.%USER%.host=%REGRESS_HOSTNAME%
rabbitmq.virtual_host.%USER%.port=%REGRESS_RABBITMQ_PORT%
rabbitmq.virtual_host.%USER%.connections=10
rabbitmq.virtual_host.%USER%.user=guest
rabbitmq.virtual_host.%USER%.password=guest
rabbitmq.virtual_host.%USER%.name=%USER%
rabbitmq.virtual_host.%USER%.path=/

# RabbitMQ - Clear
rabbitmq.virtual_host.%USER%.exchange.0.name=clear
rabbitmq.virtual_host.%USER%.exchange.0.type=topic
rabbitmq.virtual_host.%USER%.exchange.0.passive=0
rabbitmq.virtual_host.%USER%.exchange.0.durable=1
rabbitmq.virtual_host.%USER%.queue.0.name=clear
rabbitmq.virtual_host.%USER%.queue.0.passive=0
rabbitmq.virtual_host.%USER%.queue.0.durable=1
rabbitmq.virtual_host.%USER%.queue.0.exclusive=0
rabbitmq.virtual_host.%USER%.queue.0.auto_delete=0
rabbitmq.virtual_host.%USER%.binding.0.exchange=clear
rabbitmq.virtual_host.%USER%.binding.0.queue=clear
rabbitmq.virtual_host.%USER%.binding.0.key=clear

# RabbitMQ - MSG-CENTER
rabbitmq.virtual_host.%USER%.exchange.1.name=msg_center
rabbitmq.virtual_host.%USER%.exchange.1.type=topic
rabbitmq.virtual_host.%USER%.exchange.1.passive=0
rabbitmq.virtual_host.%USER%.exchange.1.durable=1
rabbitmq.virtual_host.%USER%.queue.1.name=msg_center
rabbitmq.virtual_host.%USER%.queue.1.passive=0
rabbitmq.virtual_host.%USER%.queue.1.durable=1
rabbitmq.virtual_host.%USER%.queue.1.exclusive=0
rabbitmq.virtual_host.%USER%.queue.1.auto_delete=0
rabbitmq.virtual_host.%USER%.binding.1.exchange=msg_center
rabbitmq.virtual_host.%USER%.binding.1.queue=msg_center
rabbitmq.virtual_host.%USER%.binding.1.key=msg_center

# RabbitMQ - AD-EVALUATION
rabbitmq.virtual_host.%USER%.exchange.2.name=newad_event
rabbitmq.virtual_host.%USER%.exchange.2.type=topic
rabbitmq.virtual_host.%USER%.exchange.2.passive=0
rabbitmq.virtual_host.%USER%.exchange.2.durable=1
rabbitmq.virtual_host.%USER%.queue.2.name=newad_event
rabbitmq.virtual_host.%USER%.queue.2.passive=0
rabbitmq.virtual_host.%USER%.queue.2.durable=1
rabbitmq.virtual_host.%USER%.queue.2.exclusive=0
rabbitmq.virtual_host.%USER%.queue.2.auto_delete=0
rabbitmq.virtual_host.%USER%.binding.2.exchange=newad_event
rabbitmq.virtual_host.%USER%.binding.2.queue=newad_event
rabbitmq.virtual_host.%USER%.binding.2.key=newad_event

# RabbitMQ - AD-EVALUATION-FEEDBACK
rabbitmq.virtual_host.%USER%.exchange.3.name=feedback_event
rabbitmq.virtual_host.%USER%.exchange.3.type=topic
rabbitmq.virtual_host.%USER%.exchange.3.passive=0
rabbitmq.virtual_host.%USER%.exchange.3.durable=1
rabbitmq.virtual_host.%USER%.queue.3.name=feedback_event
rabbitmq.virtual_host.%USER%.queue.3.passive=0
rabbitmq.virtual_host.%USER%.queue.3.durable=1
rabbitmq.virtual_host.%USER%.queue.3.exclusive=0
rabbitmq.virtual_host.%USER%.queue.3.auto_delete=0
rabbitmq.virtual_host.%USER%.binding.3.exchange=feedback_event
rabbitmq.virtual_host.%USER%.binding.3.queue=feedback_event
rabbitmq.virtual_host.%USER%.binding.3.key=feedback_event

# RabbitMQ - BACKEND EVENTS TRANS MESSAGES
rabbitmq.virtual_host.%USER%.exchange.4.name=backend_event
rabbitmq.virtual_host.%USER%.exchange.4.type=topic
rabbitmq.virtual_host.%USER%.exchange.4.passive=0
rabbitmq.virtual_host.%USER%.exchange.4.durable=1
rabbitmq.virtual_host.%USER%.queue.4.name=backend_event
rabbitmq.virtual_host.%USER%.queue.4.passive=0
rabbitmq.virtual_host.%USER%.queue.4.durable=1
rabbitmq.virtual_host.%USER%.queue.4.exclusive=0
rabbitmq.virtual_host.%USER%.queue.4.auto_delete=0
rabbitmq.virtual_host.%USER%.binding.4.exchange=backend_event
rabbitmq.virtual_host.%USER%.binding.4.queue=backend_event
rabbitmq.virtual_host.%USER%.binding.4.key=backend_event
