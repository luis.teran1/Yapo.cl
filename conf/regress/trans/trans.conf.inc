bconfd =  %DESTDIR%/conf/bconf.conf
db.pgoptions = -c search_path=%BPVSCHEMA%,public

db.pgsql.master.engine = PGSQL
db.pgsql.master.name = blocketdb
db.pgsql.master.hostname = %REGRESS_HOSTNAME%
db.pgsql.master.user = %USER%
db.pgsql.master.password = 
db.pgsql.master.socket = %REGRESS_PGSQL_HOST%
db.pgsql.master.options = -c search_path=%BPVSCHEMA%,public

db.pgsql.slave.engine = PGSQL
db.pgsql.slave.name = blocketdb
db.pgsql.slave.hostname = %REGRESS_HOSTNAME%
db.pgsql.slave.user = %USER%
db.pgsql.slave.password = 
db.pgsql.slave.socket = %REGRESS_PGSQL_HOST%
db.pgsql.slave.options = -c search_path=%BPVSCHEMA%,public

db.pgsql.queue.engine = PGSQL
db.pgsql.queue.name = blocketdb
db.pgsql.queue.hostname = localhost
db.pgsql.queue.user = %USER%
db.pgsql.queue.password = 
db.pgsql.queue.socket = %REGRESS_PGSQL_HOST%
db.pgsql.queue.options = -c search_path=%BPVSCHEMA%,public

port = %REGRESS_TRANS_PORT%
failover_port = %REGRESS_TRANS_FPORT%
control_port = %REGRESS_TRANS_CPORT%
control_failover_port = %REGRESS_TRANS_CFPORT%

basedir=%DESTDIR%
max_connections=300

geoip.db.required=1
geoip.db.directory=/usr/share/GeoIP

blocket_id=trans

log_level=info
log_tag=trans

rabbitmq.enabled=1
