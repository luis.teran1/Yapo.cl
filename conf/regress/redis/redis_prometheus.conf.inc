pidfile %TOPDIR%/.redis_prometheus.pid
port %REGRESS_REDISPROMETHEUS_PORT%

logfile %DESTDIR%/logs/redisprometheus.log
# For default save/load DB in/from the working directory
# Note that you must specify a directory not a file name.
dir %DESTDIR%/redis_prometheus

# 512mb limit
maxmemory 536870912
