pidfile %TOPDIR%/.redis_email.pid
port %REGRESS_REDISEMAIL_PORT%

logfile %DESTDIR%/logs/redisemail.log
# For default save/load DB in/from the working directory
# Note that you must specify a directory not a file name.
dir %DESTDIR%/redis_email

# 512mb limit
maxmemory 536870912
