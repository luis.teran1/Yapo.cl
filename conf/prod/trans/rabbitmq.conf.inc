# RabbitMQ configuration file, flavor: prod

# RabbitMQ - MSG-CENTER
rabbitmq.virtual_host.msg_center.host=10.45.1.118
rabbitmq.virtual_host.msg_center.port=5672
rabbitmq.virtual_host.msg_center.connections=10
rabbitmq.virtual_host.msg_center.user=yapo
rabbitmq.virtual_host.msg_center.password=aXC2lrNbhO
rabbitmq.virtual_host.msg_center.name=msg_center
rabbitmq.virtual_host.msg_center.path=msg_center
rabbitmq.virtual_host.msg_center.exchange.0.name=msg_center
rabbitmq.virtual_host.msg_center.exchange.0.type=topic
rabbitmq.virtual_host.msg_center.exchange.0.passive=0
rabbitmq.virtual_host.msg_center.exchange.0.durable=1
rabbitmq.virtual_host.msg_center.queue.0.name=msg_center
rabbitmq.virtual_host.msg_center.queue.0.passive=0
rabbitmq.virtual_host.msg_center.queue.0.durable=1
rabbitmq.virtual_host.msg_center.queue.0.exclusive=0
rabbitmq.virtual_host.msg_center.queue.0.auto_delete=0
rabbitmq.virtual_host.msg_center.binding.0.exchange=msg_center
rabbitmq.virtual_host.msg_center.binding.0.queue=msg_center
rabbitmq.virtual_host.msg_center.binding.0.key=msg_center

# RabbitMQ - AD-EVALUATION
rabbitmq.virtual_host.newad_event.host=10.45.1.118
rabbitmq.virtual_host.newad_event.port=5672
rabbitmq.virtual_host.newad_event.connections=4
rabbitmq.virtual_host.newad_event.user=yapo
rabbitmq.virtual_host.newad_event.password=aXC2lrNbhO
rabbitmq.virtual_host.newad_event.name=newad_event
rabbitmq.virtual_host.newad_event.path=newad_event
rabbitmq.virtual_host.newad_event.exchange.0.name=newad_event
rabbitmq.virtual_host.newad_event.exchange.0.type=topic
rabbitmq.virtual_host.newad_event.exchange.0.passive=0
rabbitmq.virtual_host.newad_event.exchange.0.durable=1
rabbitmq.virtual_host.newad_event.queue.0.name=newad_event
rabbitmq.virtual_host.newad_event.queue.0.passive=0
rabbitmq.virtual_host.newad_event.queue.0.durable=1
rabbitmq.virtual_host.newad_event.queue.0.exclusive=0
rabbitmq.virtual_host.newad_event.queue.0.auto_delete=0
rabbitmq.virtual_host.newad_event.binding.0.exchange=newad_event
rabbitmq.virtual_host.newad_event.binding.0.queue=newad_event
rabbitmq.virtual_host.newad_event.binding.0.key=newad_event

# RabbitMQ - AD-EVALUATION-FEEDBACK
rabbitmq.virtual_host.feedback_event.host=10.45.1.118
rabbitmq.virtual_host.feedback_event.port=5672
rabbitmq.virtual_host.feedback_event.connections=4
rabbitmq.virtual_host.feedback_event.user=yapo
rabbitmq.virtual_host.feedback_event.password=aXC2lrNbhO
rabbitmq.virtual_host.feedback_event.name=feedback_event
rabbitmq.virtual_host.feedback_event.path=feedback_event
rabbitmq.virtual_host.feedback_event.exchange.0.name=feedback_event
rabbitmq.virtual_host.feedback_event.exchange.0.type=topic
rabbitmq.virtual_host.feedback_event.exchange.0.passive=0
rabbitmq.virtual_host.feedback_event.exchange.0.durable=1
rabbitmq.virtual_host.feedback_event.queue.0.name=feedback_event
rabbitmq.virtual_host.feedback_event.queue.0.passive=0
rabbitmq.virtual_host.feedback_event.queue.0.durable=1
rabbitmq.virtual_host.feedback_event.queue.0.exclusive=0
rabbitmq.virtual_host.feedback_event.queue.0.auto_delete=0
rabbitmq.virtual_host.feedback_event.binding.0.exchange=feedback_event
rabbitmq.virtual_host.feedback_event.binding.0.queue=feedback_event
rabbitmq.virtual_host.feedback_event.binding.0.key=feedback_event

# RabbitMQ - BACKEND EVENTS TRANS MESSAGES 
rabbitmq.virtual_host.backend_event.host=10.45.1.118
rabbitmq.virtual_host.backend_event.port=5672
rabbitmq.virtual_host.backend_event.connections=4
rabbitmq.virtual_host.backend_event.user=yapo
rabbitmq.virtual_host.backend_event.password=aXC2lrNbhO
rabbitmq.virtual_host.backend_event.name=backend_event
rabbitmq.virtual_host.backend_event.path=backend_event
rabbitmq.virtual_host.backend_event.exchange.0.name=backend_event
rabbitmq.virtual_host.backend_event.exchange.0.type=topic
rabbitmq.virtual_host.backend_event.exchange.0.passive=0
rabbitmq.virtual_host.backend_event.exchange.0.durable=1
rabbitmq.virtual_host.backend_event.queue.0.name=backend_event
rabbitmq.virtual_host.backend_event.queue.0.passive=0
rabbitmq.virtual_host.backend_event.queue.0.durable=1
rabbitmq.virtual_host.backend_event.queue.0.exclusive=0
rabbitmq.virtual_host.backend_event.queue.0.auto_delete=0
rabbitmq.virtual_host.backend_event.binding.0.exchange=backend_event
rabbitmq.virtual_host.backend_event.binding.0.queue=backend_event
rabbitmq.virtual_host.backend_event.binding.0.key=backend_event
