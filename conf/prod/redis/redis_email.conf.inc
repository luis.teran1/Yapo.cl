pidfile /var/redis/redisemails/redis_email.pid
port 6359

# For default save/load DB in/from the working directory
# Note that you must specify a directory not a file name.
dir /var/redis/redisemail/db

# 4GB limit
maxmemory 4294967296
