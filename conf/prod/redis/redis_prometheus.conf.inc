pidfile /var/redis/redisprometheus/redis_prometheus.pid
port 6379

# For default save/load DB in/from the working directory
# Note that you must specify a directory not a file name.
dir /var/redis/redisprometheus/db

# 1GB limit
maxmemory 1073741824
