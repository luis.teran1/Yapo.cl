import json
import re
import sys


def file_lines(file):
    with open(file, 'r') as f:
        return [line[:-1] if line[-1] == '\n' else line for line in f]


def split(lines):
    return (lines[:5], json.loads(' '.join(lines[5:])))


def matches(o, e):
    if type(o) != type(e):
        return False
    if type(o) is dict:
        if sorted(o.keys()) != sorted(e.keys()):
            return False
        for k in o.keys():
            if not matches(o[k], e[k]):
                return False
        return True
    if type(o) is str:
        replaces = [('{{.*}}', '.*'), ('[', '\['), (']', '\]'), ('(', '\('), (')', '\)'), ('+', '\+')] 
        for rep in replaces:
            e = e.replace(*rep)
        return re.match('^' + e + '$', o)
    return o == e


def error(str):
    print(str, file=sys.stderr)
    sys.exit(1)


def ad_matches(o, e):
    o_id = o['list_id']
    e_id = e['list_id']
    o_keys = sorted(o.keys())
    e_keys = sorted(e.keys())
    if o_keys != e_keys:
        error ('Mismatching keys on list_id [{}].\nFound {}\nExpected {}\nExtra: {}\nMissing: {}'.format(
            o_id, o_keys, e_keys, list(set(o_keys) - set(e_keys)), list(set(e_keys) - set(o_keys))))

    for k in o_keys:
        if not matches(o[k], e[k]):
            error ('Mismatching value on list_id [{}].\nFound [{}: {}]\nExpected [{}: {}]'.format(
                o_id, k, o[k], k, e[k]))


def json_matches(o, e):
    o_keys = sorted(o.keys())
    e_keys = sorted(e.keys())
    if o_keys != e_keys:
        error ('Mismatching keys at top level.\nFound {}\nExpected {}'.format(o_keys, e_keys))

    for k in o_keys:
        if k == 'ads':
            continue
        if o[k] != e[k]:
            error ('Mismatching values at top level.\nFound [{}: {}]\nExpected [{}: {}]'.format(k, o[k], k, e[k]))

    if len(o['ads']) != len(e['ads']):
        error ('Mismatching number of returned ads. Found [{}]. Expected [{}].'.format(len(o['ads']), len(e['ads'])))

    for o, e in zip(o['ads'], e['ads']):
        ad_matches(o, e)


def compare(output, expected):
    o_head, o_data = split(output)
    e_head, e_data = split(expected)

    for o, e in zip(o_head, e_head):
        if not matches(o, e):
            error ('Header mismatch. [{}] does not match expected format [{}]'.format(o, e))

    json_matches(o_data, e_data)


output = file_lines(sys.argv[1])
expected = file_lines(sys.argv[2])
compare(output, expected)
