<?php
require_once('init.php');

$unconfigured_params = array();
$newad_order = bconf_get($BCONF, "*.api.ad_params.newad.order");
$list_order = bconf_get($BCONF, "*.api.ad_params.list.order");

$categories = bconf_get($BCONF, "*.cat_order");
foreach ($categories as $category) {
	innerIteration($category);
}

if (count($unconfigured_params) > 0) {
	print "MOBILE API MISCONFIGURED!!

A new ad param is defined but not for mobile apps. 
In order to complete the configuration of this new ad param you must modify bconf.txt.api.ad_params accordingly!
But remember that this is NOT not enough to make the API work!! You MUST run the application tests and complete the configuration for this ad_param.

If any help is needed contact mobilesiteteam@schibstediberica.es

The Mobile Site Team.\n";
} else {
	print "OK";
}
exit();

function innerIteration($category) {
	global $BCONF;
	global $unconfigured_params;
	global $newad_order, $list_order;

	$callback_data = array('category'=>$category, 'value'=>array());
	get_settings(bconf_get($BCONF, "*.category_settings"), 'types', "key_lookup", "set_values", $callback_data);
	if ( ! isset($callback_data['value']) )
		return;

	$adType = $callback_data['value'];
	foreach ($adType as $type) {
		$callback_data = array('category'=>$category, 'type'=>$type, 'value'=>array());
		get_settings(bconf_get($BCONF, "*.category_settings"), 'params', "key_lookup", "set_values", $callback_data);
		if ( ! isset($callback_data['value']) )
			continue;

		foreach ($callback_data['value'] as $ad_param) {

			/* Do not iterate again over the same one.... */
			if (in_array($ad_param, $unconfigured_params))
				continue;

			/* Step 1, check if the parameter has a "use" entry in API */
			$use = bconf_get($BCONF, "*.api.ad_params.$ad_param.use");
			if (!isset($use)) {
				print "USE ENTRY MISSING:\n\t$ad_param\n\n"; 
				array_push($unconfigured_params, $ad_param);
				continue;
			} else if ($use == 0) {
				/* configured and not used, that's OK */
				continue;
			}

			/* Step 2: If it is in use, check if it has a config entry for 'newad' or 'list0' */
			$newad_config = bconf_get($BCONF, "*.api.ad_params.$ad_param.newad");
			$list_config = bconf_get($BCONF, "*.api.ad_params.$ad_param.list");
			if ($newad_config == NULL && $list_config == NULL) {
				print "CONFIGURATION ENTRIES MISSING:\n\t$ad_param\n\n"; 
				array_push($unconfigured_params, $ad_param);
				continue;
			}

			/* Step 3: If it's there and has configuration, it needs an 'order' entry to show up... */
			$is_ordered = (is_array($newad_order) && in_array($ad_param, $newad_order)) ||
					(is_array($list_order) && in_array($ad_param, $list_order));
			if (!$is_ordered) {
				print "ORDER ENTRY MISSING:\n\t$ad_param\n\n"; 
				array_push($unconfigured_params, $ad_param);
				continue;
			}
		}
	}
}

function set_values($setting, $value, $que, $callback_data){
	$callback_data["value"][] = $value;
}

function key_lookup($setting, $key, $callback_data){
	return @$callback_data[$key];
}

