#!/usr/bin/perl -w

## Script to test if JSON well-formedness and correctness and validity.

use strict;

use LWP::UserAgent;
use JSON;
use DBI;
use Data::Dumper;

use Switch;

my $debug = 0;

print "> Init application: " if ($debug);

sub get_json {
	# JSON Parsing

	print "\n> ...Retriving and parsing data from JSON files... " if ($debug);
	my $url;
	my $result;
	my $json;

	print "\033[32;1mOK\033[0m\n" if ($debug);
	print "Setting UserAgent: " if ($debug);
	
	my $ua = LWP::UserAgent->new;
	$ua->agent("Mozilla/5.0(iPad; U; CPU iPhone OS 3_2 like Mac OS X; en-us) AppleWebKit/531.21.10 (KHTML, like Gecko) Version/4.0.4 Mobile/7B314 Safari/531.21.10");

	# Get JSON from API
	print "\033[32;1mOK\033[0m\n" if ($debug);

	# Create a request
	$url = shift;
	print "  Connecting with URL [$url]: " if ($debug);
	my $req = HTTP::Request->new(GET => $url);
	$req->referer('');

	# Pass request to the user agent and get a response back
	my $res = $ua->request($req);

	# Log the outcome of the response
	my $http_status = $res->code;
	if ($http_status ne "200") {
		print "\033[31;1mError loading URL [$url] (status $http_status)...\033[0m\n" if ($debug);
		my $error = $res->status_line;
		print "$error\n";
		exit 1;
	} 

	print "\033[32;1mOK\033[0m\n" if ($debug);
	return $res->content;
}

sub validate_json {
	my $data;
	my $result;
	my $json;

	$data = shift;

	# JSON parse
	print "Parsing JSON object\n" if ($debug);
	$json = JSON->new->allow_nonref;
	my $json_decoded = $json->decode($data);
	return $json_decoded;
}





# JSON library exits unexpectedly if JSON is bad formed
eval {
	#my $url = shift ;
	#my $data = get_json($url);

	my $file_contents;
	my $file = shift;

	open FILE, "<$file";
	$file_contents = do { local $/; <FILE> };

	my $data = validate_json($file_contents);

	if(!$data) {
		exit 1;
	}
};

if ($@) {
	exit 2;
}

print "\033[31;1mAll done!!!!\033[0m\n" if ($debug);
exit 0;

