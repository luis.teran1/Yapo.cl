#Fixing makefile includes
ifndef __ext-modules_mobile-api_samples_modules_paths_mk
__ext-modules_mobile-api_samples_modules_paths_mk:=1

MOBILEDIR?=$(realpath ${TOPDIR}/mobile)

MOBILE_INC=${MOBILEDIR}/mk/commonsrc.mk
INCLUDE_MK:=$(shell [ ! -z "${MOBILEDIR}" ] && find ${MOBILEDIR}/ -name commonsrc.mk)

ifeq (${INCLUDE_MK}, ${MOBILE_INC})
include ${MOBILE_INC}
endif


endif #__ext-modules_mobile-api_samples_modules_paths_mk
