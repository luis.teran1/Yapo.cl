#!/bin/bash

# This script should be called just after checking out the site sources
# it'll download all the required external libs and set up
# the needed links

# This is just a bare simple PoC, it'll need to be implemented
# properly sometimes in the future

DFLT_GIT_REPO="/opt/git/"
EXT_MODULES_DIR="ext-modules"
GLOBAL_TOPDIR=`pwd`

MODULES_LIST_FILES=conf/modules/*-manifest

mkdir -p "${EXT_MODULES_DIR}"

for file in ${MODULES_LIST_FILES}; do
	# Reset variables
	GIT_REPO=${DFLT_GIT_REPO}
	GIT_MODULE=""
	GIT_BRANCH=""
	CHECKOUT_DIR=""

	echo "*** Handling module from ${file}"

	. ${GLOBAL_TOPDIR}/${file}

	pushd ${EXT_MODULES_DIR}
	GIT_COMMAND="git clone"

	# Print log messages, pleeease
	[ -z "${GIT_MODULE}" ] && exit 1

	MODULE_NAME=`echo ${GIT_MODULE} | sed 's/\.git//'`

	[ ! -z "${GIT_BRANCH}" ] && GIT_COMMAND="${GIT_COMMAND} -b ${GIT_BRANCH}"
	[ -z "${CHECKOUT_DIR}" ] && CHECKOUT_DIR="${MODULE_NAME}"
	
	GIT_COMMAND="${GIT_COMMAND} ${GIT_REPO}/${GIT_MODULE} ${CHECKOUT_DIR}"

	echo " = Executing ${GIT_COMMAND}"

	${GIT_COMMAND}

	popd

	if [ ! -z "${LINK_FROM}" ]; then
		LN_DST_DIR="${EXT_MODULES_DIR}/${CHECKOUT_DIR}"

		[ ! -z "${LINK_TO}" ] && LN_DST_DIR="${LN_DST_DIR}/${LINK_TO}"

		echo " = Setting up link from ${LINK_FROM} to ${LN_DST_DIR}"

		ln -s "${LN_DST_DIR}" "${LINK_FROM}"
	fi
done

