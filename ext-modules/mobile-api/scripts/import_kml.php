#!/usr/bin/php
<?php
// imports polygons from kml files to the database
// example connection string 'dbname=blocketdb host=/dev/shm/regress-paulo/pgsql0/data'




main($argc, $argv);

function quit($m) {
	echo "$m\n";
	die(1);
}


function write($region_id, $name, $coords) {
	global $db;	
	// parse coordinates
	$points = array();
	foreach (explode(' ', preg_replace('/[ \r\n\t]+/', ' ', $coords)) as $line) {
		$parts = explode(',', trim($line));
		if (count($parts) == 3 /* Lon,Lat,Altitude */ || count($parts) == 2 /* Lon,Lat */) {
			$points[] = $parts[0];
			$points[] = $parts[1];
		}
		else {
			// bad line, ignore
		}
	}

	$sql = "insert into region_polygons (region, name, poli) values ($region_id, '$name', '".implode(',', $points)."');";

	if (pg_query($db, $sql) === false) {
		quit("error inserting the polygon: $sql");
	}
}

function registerNs($xml){

	$xml->registerXPathNamespace("kml", "http://www.w3.org/2005/Atom");
	$xml->registerXPathNamespace("kml", "http://earth.google.com/kml/2.2");
}

function ProcessXmlFile($map_filename){

	$xml = new XMLReader();
	$kml = 'kml:';
	$placemark_id = 0;
	$polygon_id = 0;
	$coordinate_id = 0;
	$xml = simplexml_load_file($map_filename) or die("Failed to open input file.");

	registerNs($xml);

	$result = $xml->xpath('//kml:Document/kml:Placemark');

	foreach($xml->xpath('//kml:Placemark') as $placemark) {

		$placemark_id++;
		echo "Processing placemark ". $placemark_id . "\n";

		registerNs($placemark);

		unset ($region_id);

		foreach ($placemark->xpath('descendant::kml:ExtendedData') as $extendedData) {
		
			registerNs($extendedData);
					
			$region = $extendedData->xpath('descendant::kml:Data[@name=\'REGION_ID\']/kml:value');
		
			if ($region) {
				$region_id = (string) $region[0];
				echo "-->Placemark associated to region $region_id\n";
				if (is_numeric($region_id));
					break;
			}
			else {
				echo "-->Placemark without a region ID.\n";
			}
			
		}
		
		if (isset($region_id)) {
			if (is_numeric($region_id)) {
				foreach ($placemark->xpath('descendant::kml:Polygon') as  $polygon) {

					registerNs($polygon);
					$polygon_id++;
					echo "---->Processing polygon $polygon_id\n";
					
					
					foreach ($polygon->xpath('descendant::kml:coordinates') as $coordinates) {
						$coordinate_id++;
						echo "------>Processing coordinates $coordinate_id\n";
						write($region_id, "Poli_$polygon_id"."_Coord_$coordinate_id", $coordinates);
					}

				}
			}
			else {
				echo "---->Region Id must be a number\n";
			}
			
		}
	}
}

function dbConnect($connection_string){
	global $db;
	if (($db = pg_connect($connection_string)) === false) {
		quit('could not connect to '.$connection_string);
	}
	echo "1: " . var_dump($db);
}


function main($argc, $argv) {
	// parse arguments
	if ($argc != 3) {
		global $argv;
		quit("usage: {$argv[0]} <kml-file> <connection-string>" );
	}
	
	dbConnect($argv[2]);
	ProcessXmlFile($argv[1]);
}

