create table region_polygons (
	id serial primary key,
	region int,
	name varchar(30),
	poli polygon not null
);
