<?php

require_once 'blocket_application.php';

class blocket_mobile_application extends blocket_application {

	function display_mobile_layout($template = null, $simple_data = null, $extended_data = null, $bconf_appl = null, $wrapper = 'general_smartphone') {
		global $BCONF;

		if (!is_array($simple_data)) {
			$simple_data = array();
		}

		$simple_data += array(
			'l' => $this->caller->layout,
			'ca' => $this->caller->to_string(),
			'ca_region' => $this->caller->region,
			'ca_city' => $this->caller->city,
			'content' => $template,
			'mobile_appl' => empty($this->mobile_appl) ? $this->application_name : $this->mobile_appl
		);
		if(isset($_COOKIE["sq"]) ){
			$sq = $_COOKIE["sq"];
			parse_str($sq, $string_query);	
			@$this->caller->parse_caller($string_query["ca"]);
			$simple_data['ca_region'] = $this->caller->region;
		}

		$tmpl = 'mobile/smartphone/' . $wrapper . '.html';
		$this->display($tmpl, $simple_data, $extended_data, $bconf_appl);
	}

	function api_response($api_content, $appl = null, $simple_data = null, $extended_data = null, $bconf_appl = null) {
		global $BCONF;

		if (!is_array($simple_data)) {
			$simple_data = array();
		}

		$simple_data += array(
			'l' => $this->caller->layout,
			'ca' => $this->caller->to_string(),
			'ca_region' => $this->caller->region,
			'ca_city' => $this->caller->city,
			'content' => "apitemplates/$api_content.json",
			'mobile_appl' => empty($this->mobile_appl) ? $this->application_name : $this->mobile_appl
		);

		$tmpl = 'mobile/smartphone/api_response.html';
		$this->display($tmpl, $simple_data, $extended_data, $bconf_appl, true);
	}
}
