<?php

require_once('autoload_lib.php');
require_once 'bImage.php';
require_once 'util.php';
require_once 'blocket_mobile_application.php';
require_once 'AccountSession.php';
require_once('AccountUtil.php');
require_once('bLogger.php');
require_once('httpful.php');
require_once 'JSON.php';

openlog('MAI', LOG_ODELAY, LOG_LOCAL0);

use Yapo\Ads\MultiInsertChecker;
use Yapo\Products;
use Yapo\PaymentDeliveryQueryService;
use Yapo\DuplicatedAdMobile;

class blocket_mai extends blocket_mobile_application {

	var $ad_id;
	var $body;
	var $category;
	var $communes;
	var $email;
	var $geoposition;
	var $image;
	var $is_company;
	var $job_category;
	var $name;
	var $business_name;
	var $password;
	var $phone;
	var $area_code;
	var $phone_type;
	var $price;
	var $currency;
	var $region;
	var $rut;
	var $service_type;
	var $footwear_type;
	var $subject;
	var $service;
	var $equipment;

	var $accept_conditions;
	var $account_successfully_created = false;
	var $application_name;
	var $file;
	var $is_mobile_app = false;
	var $last_ad;
	var $mobile_appl;
	var $pack_result;
	var $upselling_product = "";
	var $upselling_product_label = "";

	var $account;

	var $ad_price = 0;

	function blocket_mai() {
		global $BCONF;
		global $session;
		$this->add_static_vars(array('last_ad','is_mobile_app'));
		$this->application_name = 'mai';
		$this->mobile_appl = 'newad';
		$this->file = '';
		$this->init('load', 0);

		$this->attrs = array(
			'body'			=> array('attr' => 'body',			'type' => 'string'),
			'category'		=> array('attr' => 'category',		'type' => 'int'),
			'communes'		=> array('attr' => 'communes',		'type' => 'int'),
			'company_ad'	=> array('attr' => 'is_company',	'type' => 'int'),
			'da_id'			=> array('attr' => 'ad_id',			'type' => 'int'),
			'email'			=> array('attr' => 'email',			'type' => 'email'),
			'geoposition'	=> array('attr' => 'geoposition',	'type'=> 'string'),
			'image'			=> array('attr' => 'image',			'type' => 'array'),
			'jc'			=> array('attr' => 'job_category',	'type' => 'array'),
			'name'			=> array('attr' => 'name',			'type' => 'string'),
			'passwd'		=> array('attr' => 'password',		'type' => 'raw'),
			'phone'			=> array('attr' => 'phone',			'type' => 'int'),
			'area_code'		=> array('attr' => 'area_code',			'type' => 'int'),
			'phone_type'		=> array('attr' => 'phone_type',			'type' => 'string'),
			'price'			=> array('attr' => 'price',			'type' => 'string'),
			'currency'			=> array('attr' => 'currency',			'type' => 'string'),
			'region'		=> array('attr' => 'region',		'type' => 'int'),
			'rut'			=> array('attr' => 'rut',			'type' => 'string'),
			'business_name'	=> array('attr' => 'business_name',	'type' => 'string'),
			'sct'			=> array('attr' => 'service_type',	'type' => 'array'),
			'subject'		=> array('attr' => 'subject',		'type' => 'string'),
			'srv'			=> array('attr' => 'service',		'type' => 'array'),
			'eqp'			=> array('attr' => 'equipment',		'type' => 'array'),
			'type'			=> array('attr' => 'type',			'type' => 'string'),
			'source'		=> array('attr' => 'source',		'type' => 'string'),
			'fwtype'		=> array('attr' => 'footwear_type',	'type' => 'array'),
		);
	}

	function get_state($name) {
		switch($name) {
			case 'load':
				return array(
					'function' => 'mai_load',
					'method' => 'get',
					'params' => array("is_mobile_app" => "f_is_mobile_app")
				);

			case 'form':
				return array(
					'function' => 'mai_form',
					'method' => 'get',
					'params' => array("is_mobile_app" => "f_is_mobile_app")
				);

			case 'submit':
				return array(
					'function' => 'mai_submit',
					'method' => 'post',
					'params' => array()
				);

			case 'delete':
				return array(
					'function' => 'mai_delete',
					'method' => 'get',
					'params' => array()
				);

			case 'delete_response':
				return array(
					'function' => 'mai_delete_response',
					'method' => 'post',
					'params' => array('list_id', 'deletion_reason', 'passwd', 'passwdHash' => 'f_passwd', 'testimonial' => 'f_string')
				);

			case 'done':
				return array(
					'function' => 'mai_done',
					'method' => 'get',
					'params' => array()
				);

			case 'success':
				return array(
					'function' => 'mai_success',
					'method' => 'get',
					'params' => array()
				);
			case 'success-edit':
				return array(
					'function' => 'mai_success_edit',
					'method' => 'get',
					'params' => array()
				);

		}
	}

	/*////////////////////// actions ////////////////////*/
	function mai_load($is_mobile_app) {
		if($is_mobile_app == true)
			$this->is_mobile_app = true;
		return 'form';
	}

	function mai_form($is_mobile_app) {
		global $BCONF;
		$this->last_ad = @$_SESSION['static_data']['blocket_mai']['last_ad'];
		$tmpl_data = array();
		if($is_mobile_app == true){
			$this->is_mobile_app = true;
			$tmpl_data['display_for_app'] = "true";
		}

		$account_session = new AccountSession();
		$tmpl_data['show_upselling_in_form'] = (int) Upselling::showUpselling(
			$BCONF,
			$account_session->is_logged(),             /* userLoggedIn */
			$account_session->get_param('is_pro_for'), /* userProCategories */
			false,                                     /* adHasProducts */
			0,                                         /* userHasSlots */
			false                                      /* packUpsellingEnabled */
		);

		if ($account_session->is_logged()) {
			$tmpl_data['account_region'] = $account_session->get_param('region');
			$is_pro_for = $account_session->get_param('is_pro_for');
			if(!empty($is_pro_for) ){
				$tmpl_data['is_pro_for'] = $is_pro_for;
			}
		}

		$this->display_mobile_layout('mobile/smartphone/newad_form.html', $tmpl_data);
		unset($this->post);
		unset($this->errors);
		return 'FINISH';
	}

	function check_account_is_pro($account_session, &$transaction, &$is_inserted){
		//Verify if the user is pro and set pro as default for this category
		$is_pro_for = $account_session->get_param("is_pro_for");
		if ($account_session->is_logged() && !empty($is_pro_for) && !empty($_POST['category'])) {
			$this->category = sanitizeVar($_POST['category'], 'int');
			$io = array('category_group' => $this->category );
			get_settings(bconf_get($BCONF,"*.category_settings"), 'is_pro_for',
				create_function('$s,$k,$d', 'return isset($d[$k]) ? $d[$k] : NULL;'),
				create_function('$s,$k,$v,$d', '$d["value"] = $k;'),
				$io);
			$val = intval(@$io['value']);
			if($val == 0){
				$transaction->add_data("company_ad","1");
				$is_inserted["company_ad"] = true;
			}
		}
	}

	private function sanitize_and_populate(Array $params) {
		foreach($params as $k => $v) {
			if (isset($this->attrs[$k])) {
				$attr = $this->attrs[$k]['attr'];
				$type = $this->attrs[$k]['type'];

				$this->$attr = sanitizeVar($v, $type);
			}
		}
	}


	function mai_populate_newad_transaction(){
		$transaction = new bTransaction();
		$is_inserted = array();

		$this->sanitize_and_populate($_POST);
		$this->phone = $this->phone_standarization($this->area_code, $this->phone, $this->phone_type);

		// hash the password
		if (!empty($this->password)) {

			if (!empty($this->email)) {
				$account_session = new AccountSession();

				$transaction->add_data('email', $this->email);
				$is_inserted['email'] = true;

				if($account_session->authenticate($this->email, $this->password)){
					$this->password = $account_session->get_param('password');
					$this->check_account_is_pro($account_session, $transaction, $is_inserted);
				}
				else {
					$this->password = hash_password($this->password);
				}
				$this->account = $account_session;
			}
			else {
				$this->password = hash_password($this->password);
			}

			$transaction->add_data('passwd', $this->password);
			$is_inserted['passwd'] = true;
		}
		else{
			if (!empty($this->email)) {
				$account_session = new AccountSession();
				if($account_session->is_logged()){
					$this->password = $account_session->get_param('password');
					$this->check_account_is_pro($account_session, $transaction, $is_inserted);
					$transaction->add_data('passwd', $this->password);
				}
				$this->account = $account_session;
			}
		}

		/* Add job categories*/
		if(!empty($this->job_category)) {
			$this->job_category = implode('|',array_values($this->job_category));
			$transaction->add_data('job_category', $this->job_category);
			$is_inserted['jc'] = true;
		}

		/* Add service type */
		if(!empty($this->service_type)){
			$this->service_type = implode('|',array_values($this->service_type));
			$transaction->add_data('service_type', $this->service_type);
			$is_inserted['sct'] = true;
		}

		/* Add services */
		if(!empty($this->service)){
			$this->service = implode('|',array_values($this->service));
			$transaction->add_data('services', $this->service);
			$is_inserted['srv'] = true;
		}

		/* Add equipment */
		if(!empty($this->equipment)){
			$this->equipment = implode('|',array_values($this->equipment));
			$transaction->add_data('equipment', $this->equipment);
			$is_inserted['eqp'] = true;
		}

		/* Add footwear type */
		if(!empty($this->footwear_type)){
			$this->footwear_type = implode('|',array_values($this->footwear_type));
			$transaction->add_data('footwear_type', $this->footwear_type);
			$is_inserted['fwtype'] = true;
		}

		/* Skip creation of account for AI transaction */
		if(isset($_POST['ai_create_account']) && $_POST['ai_create_account'] === '0'){
			$is_inserted['ai_create_account'] = true;
		}

		/* Skip creation of account for AI transaction */
		if(isset($this->rut)){
			$is_inserted['rut'] = true;
		}

		if (isset($this->business_name)) {
			$is_inserted['business_name'] = true;
		}

		$this->ad_price = $this->get_ad_price();
		// add data to the transaction
		if (bconf_get($BCONF, "*.common.pay_type.free.enabled") == 1) {
			if ($this->ad_price > 0)
				$transaction->add_data('pay_type', 'card');
			else {
				$transaction->add_data('pay_type', 'free');
				if (bconf_get($BCONF, '*.ai.no_verify_email') == 1) {
					$transaction->add_data('do_not_send_mail', '1');
				}
			}
		}
		$transaction->add_data('lang', 'es');

		foreach($_POST as $key => $value) {
			if (isset($is_inserted[$key]) && $is_inserted[$key] === true) {
				continue;
			}
			if ($key == 'area_code') {
				continue;
			}

			if (is_array($value)) {
				$transaction->add_data($key, $value);
			}
			else if (strlen($value)) {
				switch($key) {
					case 'subject':
						$value = sanitizeVar($value, 'string');
						$value = iconv("UTF-8", "ISO-8859-1", $value);
						$this->clean_subject($value);
						$transaction->add_data($key,$value);
						break;
					case 'skip_duplicated':
					case 'upselling_product':
					case 'upselling_product_label':
					case 'show_upselling_in_form':
						break;
					default:
						if (isset($this->attrs[$key])) {
							$attr = $this->attrs[$key]['attr'];
							$transaction->add_data($key, iconv("UTF-8", "ISO-8859-1", $this->$attr));
						}
						else {
							$value = sanitizeVar($value, 'string');
							$transaction->add_data($key, iconv("UTF-8", "ISO-8859-1", $value));
						}
				}
			}
		}

		if (isset($_REQUEST['ht'])) {
			$source = ht_to_source((int)$_REQUEST['ht']);
		}
		else {
			$ht = (int)bconf_get($BCONF, '*.ai.source.default_mobile');
			$source = ht_to_source($ht);
		}

		$transaction->add_data('source', $source);

		/* UID */
		if (!empty($_COOKIE['uid'])) {
			// only use the cookie UID when it has a valid value
			$cookie_uid = (int)$_COOKIE['uid'];
			if ($cookie_uid > 0) {
				$transaction->add_data('uid', $cookie_uid);
			} else {
				unset($_COOKIE['uid']);
			}
		}

		return $transaction;
	}

	function mai_populate_account_transaction(){
		if (isset($_POST['ai_create_account']) && $_POST['ai_create_account'] == "0"){
			$this->accept_conditions = 1;

			$account_trans = new bTransaction();
			$account_trans->add_data('name',  $this->name);
			$account_trans->add_data('email', $this->email);
			$account_trans->add_data('phone', $this->phone);
			$account_trans->add_data('is_company', $this->is_company);
			$account_trans->add_data('region', $this->region);
			$account_trans->add_data('password',  $this->password);
			$account_trans->add_data('accept_conditions', $this->accept_conditions);
			$account_trans->add_data('source', $this->source);
			if (!empty($this->business_name)) {
				$account_trans->add_data('business_name', $this->business_name);
			}

			if(isset($this->rut) && !empty($this->rut)) {
				$account_trans->add_data('rut', $this->rut);
			}

			return $account_trans;
		}
		return false;
	}

	function phone_standarization($code, $phone, $type) {
		if($type == 'm') {
			return '9'.$phone;
		} else {
			return $code.$phone;
		}
	}

	function f_rut ($val) {
		return strtoupper(str_replace('.','',strip_tags($val)));
	}

	function f_is_mobile_app($var){
		if($var=="true")
			return true;
		return false;
	}


	function mai_submit() {
		global $BCONF;
		$account_session = new AccountSession();

		$duplicated = new DuplicatedAdMobile($account_session);
		$category = isset($_POST['category']) ? $_POST['category'] : null;
		$skip_duplicated = isset($_POST['skip_duplicated']) ? $_POST['skip_duplicated'] : null;
		$check_duplicated = $duplicated->shouldCheck($category, $skip_duplicated);

		$this->account_successfully_created = false;
		$transaction = $this->mai_populate_newad_transaction();

		if (isset($_POST['upselling_product'])) {
			$this->upselling_product = sanitizeVar($_POST['upselling_product'], 'int');
		}
		if (isset($_POST['upselling_product_label'])) {
			$this->upselling_product_label = sanitizeVar($_POST['upselling_product_label'], 'int');
		}

		// Validate the transaction, check the result
		$reply = $transaction->send_command('newad', false /* ONLY VALIDATE */, true/*put user data in*/);
		$errors = $transaction->get_errors();

		$account_trans = $this->mai_populate_account_transaction();
		if($account_trans !== false){
			$account_reply = $account_trans->validate_command('create_account');
			if ($account_trans->has_error(true)) {
				syslog(LOG_INFO, log_string()."error validating create_account");
				$account_errors= $account_trans->get_errors();
				foreach($account_errors as $k => $v) {
					if(isset($errors_transaction[$k])){
						unset($account_errors[$k]);
					}
				}
				$errors = $errors + $account_errors;
			}
		}

		// Verify login retry
		if (isset($errors['passwd']) && $errors['passwd'] == 'ERROR_ACCOUNT_LOGIN_FAILED') {
			$account_session->incr_retry_login($this->email);
		}
		if ($account_session->is_blocked($this->email)) {
			$errors['passwd'] = 'ACC_ERROR_ACCOUNT_BLOCKED';
		}

		// Here we check if the ad is a duplicated
		if (empty($errors)) {
			// unless we force the ad to be "published anyway", we check always
			if ($check_duplicated) {
				$array_data = array();
				$array_data['email'] = $this->email;
				$array_data['subject'] = $this->subject;
				$array_data['body'] = $this->body;
				$array_data['category'] = $this->category;
				$array_data['price'] = $this->price;
				$array_data['currency'] = $this->currency;
				$array_data['type'] = $this->type;
				$array_data['region'] = $this->region;

				$result = $duplicated->check($array_data);
				if ($result == 'FINISH') {
					return $result;
				}
			}

			// Only for new ads check if inserting at the same time
			$multiInsertion = new MultiInsertChecker($BCONF);
			$multiInsertionParams = array("email" => $this->email, "phone" => $this->phone);

			$extraSession = new AccountSession();
			$accountInfo = $extraSession->obtainAccountInfo($this->email);
			$userIsPro = isset($accountInfo['is_pro_for']) ? $accountInfo['is_pro_for'] : '';

			// Call to verification
			$multiInsertionAllowed = $multiInsertion->allowInsertAttempt(
				$this->category,
				$multiInsertionParams,
				is_pro_for($this->category, $userIsPro)
			);
			// Not allowed
			if (!$multiInsertionAllowed) {
				syslog(LOG_INFO, log_string() . "ERROR MULTI INSERT:".var_export($multiInsertionParams, true));
				$errors['user_info'] = lang("ERROR_MANY_INSERTS");
			}
		}

		if(empty($errors)){
			$reply = $transaction->send_command('newad', true /* With COMMIT!! */, true/*put user data in*/);
			$errors = $transaction->get_errors();
		}

		if(empty($errors)){
			$this->set_uid_cookie($reply['uid']);
			# call to the create account trans
			$account_util = new AccountUtil();
			if (isset($_POST['ai_create_account']) && $_POST['ai_create_account'] == "0"){
				# check if the email has an account
				if (!$account_session->authenticateSpecial($this->email)){
					$account_reply = $account_trans->send_command('create_account');
					if (!$account_trans->has_error(true) || (isset($account_trans->_reply['error']) && $account_trans->_reply['error'] == 'ACCOUNT_ALREADY_EXISTS')){
						$account_mail = $account_util->account_mail($this->email, $this->name);
						$this->account_successfully_created = true;
					}
				}
			}

			$data_to_update = array('phone' => $this->phone, 'region' => $this->region, 'commune' => $this->communes);
			if(!$account_util->edit_account_data_if_empty($this->email, $data_to_update, $account_session)){
				syslog(LOG_ERR, log_string()."ERROR: Account data could not be updated for this account");
			}
		}
		else{
			// error inserting ad
			$messages = $transaction->get_messages();

			/* FIXME: to newad only with password fix TRANS_ERROR_NO_SUCH_PARAMETER on password */
			if ($errors && isset($errors['passwd']) && $errors['passwd'] == "TRANS_ERROR_NO_SUCH_PARAMETER" ){
			        unset($errors['passwd']);
			}

			$unique_errors = array();
			foreach ($errors as $key => &$value) {
				if ($key == "job_category")
					$key = "jc";
				else if ($key == "service_type")
					$key = "sct";
				else if ($key == "footwear_type")
					$key = "fwtype";
				if(is_array($value)){
					foreach ($value as &$subvalue){
						$subvalue = lang($subvalue);
						if(!in_array($subvalue,$unique_errors)){
							$unique_errors[$key] = $subvalue;
						}
					}
				}else{
					if ($value == "ERROR_ACCOUNT_LOGIN_FAILED") {
						$value = lang("ERROR_ACCOUNT_LOGIN_FAILED_SHORT");
					} else {
						$value = lang($value);
					}
					if (!empty($messages[$key]))
						$value .= ' "' . $messages[$key] . '"';
					if(!in_array($value,$unique_errors)){
						/* Blocked user error */
						if ($key == 'error')
							$unique_errors['phone'] = $value;
						else
							$unique_errors[$key] = $value;
					}
				}
			}

			echo(yapo_json_encode(array(
				'ok'=>false,
				'errors'=>$unique_errors
			)));
			return 'FINISH';
		}

		$this->ad_id = $reply['ad_id'];
		if ((int)$this->ad_price > 0) {
			$this->mai_done();
			return 'FINISH';
		}
		syslog(LOG_DEBUG, log_string().' calling clear with paycode: ' . $reply['paycode'] );
		$this->mai_clear($reply['paycode']);
	}

	function mai_clear($verify_code = false) {
		if (!$verify_code)
			return 'ERROR';

		$vf_trans = new bTransaction();
		$vf_trans->add_client_info();
		$vf_trans->add_data('verify_code', $verify_code);
		$vf_reply = $vf_trans->send_command('clear', 1);

		if ($vf_trans->has_error()) {
			return 'ERROR';
		}

		$this->pack_result = $vf_reply['pack_result'];
		$this->mai_done();
	}

	function mai_delete() {
		$tmpl_data = array();
		$this->display_mobile_layout('mobile/smartphone/deletion_form.html', $tmpl_data);
		return 'FINISH';
	}

	function mai_delete_response($list_id, $deletion_reason, $passwd, $passwdHash, $testimonial) {
		$data = array('ad_id' => $list_id,
					'deletion_reason' => $deletion_reason,
					'passwd' => $passwd);

		if(!empty($testimonial)){
			$data['testimonial'] = $testimonial;
		}

		if(!empty($passwdHash)){
			$data['passwdHash'] = $passwdHash;
		}

		$this->api_response('deletead', 'mai', $data);
		return 'FINISH';
	}

	function mai_done() {
		$output = array('ok'=>true);
		if($this->account_successfully_created){
			$output['account_mail'] = true;
		}
		if (isset($this->pack_result)){
			if(is_array($this->pack_result))
				$output['pack_result'] = $this->pack_result[0];
			else
				$output['pack_result'] = $this->pack_result;
		}

		if ($this->ad_price > 0) {
			$output['payment_url'] = $this->get_inserting_fee_url();
		}

		if(bconf_get($BCONF, "*.payment.product.$this->upselling_product.group") == "COMBO_UPSELLING") {
			bLogger::logDebug(__METHOD__, "Mobile Ad Insert OK -> upselling_product { $this->upselling_product}");
			$output['show_upselling_in_form'] = 1;
			$output['upselling_product'] = $this->upselling_product;
			$output['upselling_product_label'] = isset($this->upselling_product_label)?$this->upselling_product_label:'';
			$output['upselling_payment_url'] = $this->get_pay_upselling_url();
		}
		if (isset($this->ad_id))
			$output['da_id'] = $this->ad_id;
		echo(yapo_json_encode($output));
	}

	function get_pay_upselling_url() {
		$action = (isset($this->action) && $this->action == "edit" ? "edit" : "newad");
		$paymentUrl = bconf_get($BCONF, "*.common.base_url.payment") . '/pagos?id=' . $this->ad_id . '&prod=' . $this->upselling_product . '&ftype=2&from=' . $action;
		if (isset($this->upselling_product_label) && $this->upselling_product_label != "") {
			$paymentUrl = $paymentUrl . '&lt=' . $this->upselling_product_label;
		}
		return $paymentUrl;
	}

	function get_inserting_fee_url() {
		$paymentUrl = bconf_get($BCONF, "*.common.base_url.payment") . '/pagos?id=' . $this->ad_id . '&prod=' . Products::INSERTING_FEE . '&from=newad';
		return $paymentUrl;
	}

	function mai_success() {
		return $this->populate_success();
	}
	function mai_success_edit() {
		return $this->populate_success(true);
	}

	function populate_success($is_edition = false){

		/* we are not using display template we are showing an ok message through js */
		global $BCONF;
		$tmpl_data = array();

		if (empty($_POST)) {
			header("Location: /publica-un-aviso" );
		} else {
			$this->get_ad_data($tmpl_data);
			$this->subject = sanitizeVar($_POST['subject'], 'string');
			$this->category = sanitizeVar($_POST['category'], 'int');
			$this->ad_id = sanitizeVar($_POST['da_id'], 'int');
			$this->clean_subject($this->subject);

			$tmpl_data['post_subject'] = $this->subject;
			$tmpl_data['category'] = $this->category;
			$tmpl_data['da_id'] = $this->ad_id;

			if(isset($_POST['price']) && $_POST['price'] != "" ) {
				$tmpl_data['post_price'] = sanitizeVar($_POST['price'], 'int');
			}

			if(isset($_POST['image'])){
				$tmpl_data['post_image'] = $_POST['image'][0];
				$tmpl_data['post_number_images'] = count($_POST['image']);
			} else {
				$tmpl_data['post_number_images'] = 0;
			}

			if (isset($_POST['ai_create_account']) && $_POST['ai_create_account'] == "0"){
				$tmpl_data['post_create_account'] = 1;
				// this variable should be dynamically obtained from query, but for now just set it to 1
				// it seems that in this seccion of code process newads from users with and without account
				if ($this->validatePaymentDeliveryAd()) {
					$tmpl_data['pd_newad_info'] = 1;
				}
			}

			if($is_edition){
				$tmpl_data['is_edition'] = 1;
				$tmpl_data['old_category'] = sanitizeVar($_POST['old_category'], 'int');
				$tmpl_data['action'] = sanitizeVar($_POST['action'], 'string');
				$tmpl_data['has_weekly_bump'] = sanitizeVar($_POST['has_weekly_bump'], 'string');
				$tmpl_data['has_daily_bump'] = sanitizeVar($_POST['has_daily_bump'], 'string');
				$tmpl_data['has_gallery'] = sanitizeVar($_POST['has_gallery'], 'string');
				$tmpl_data['has_label'] = sanitizeVar($_POST['has_label'], 'int');
			} else {
				$tmpl_data['old_category'] = $tmpl_data['category'];
				$tmpl_data['has_weekly_bump'] = 0;
				$tmpl_data['has_daily_bump'] = 0;
				$tmpl_data['has_gallery'] = 0;
				$tmpl_data['has_label'] = 0;
			}

			if (!empty($_POST['passwd'])) {
				if (!empty($_POST['email'])) {
					$account_session = new AccountSession();
					$this->email = sanitizeVar($_POST['email'], 'email');
					$this->password = sanitizeVar($_POST['passwd'], 'string');

					if($account_session->login($this->email, $this->password)){
						$short_user_name = $account_session->get_param('name');
						$tmpl_data['account_name'] = $short_user_name;
						$tmpl_data['account_is_pro_for'] = $account_session->get_param('is_pro_for');
						$tmpl_data['acc_session_email'] = $this->email;

						$short_user_name = explode(" ", $short_user_name);
						$short_user_name = $short_user_name[0];
						if(strlen($short_user_name) > 10){
							$short_user_name = substr($short_user_name,0,7)."&hellip;";
						}
						$tmpl_data['short_user_name'] = $short_user_name;
					}
				}
			}

			if (!empty($_POST['email'])) {
				$this->email = sanitizeVar($_POST['email'], 'email');
				$tmpl_data['email'] = $this->email;
			}

			$tmpl_data['hide_footer'] = 1;
			$this->mobile_appl = 'newad_success';

			if (isset($_POST['pack_result'])) {
				$tmpl_data['pack_result'] = sanitizeVar($_POST['pack_result'], 'string');
				$tmpl_data['pack_url_suffix'] = pack_url_suffix($_POST['category']);
				if ($_POST['pack_result'] == '-3'){
					$tmpl_data['noupselling'] = isset($_POST['skip_duplicated'])?sanitizeVar($_POST['skip_duplicated'], 'string') : '';
					$this->display_mobile_layout('mobile/smartphone/dual_pro.html', $tmpl_data);
					return 'FINISH';
				}
			}

			$tmpl_data['noupselling'] = isset($_POST['skip_duplicated'])?sanitizeVar($_POST['skip_duplicated'], 'string') : '';
			$account_session = new AccountSession();
			// this variable should be dynamically obtained from query, but for now just set it to 1
			// it seems that in this seccion of code process newads from users with and without account
			if ($account_session->is_logged() && $this->validatePaymentDeliveryAd()) {
				$tmpl_data['pd_newad_info'] = 1;
			}

			$this->display_mobile_layout('mobile/smartphone/newad_done.html', $tmpl_data);
		}
		return 'FINISH';
	}

	function validatePaymentDeliveryAd() {
		global $BCONF;
		if (!bconf_get($BCONF, "*.pd_newad_info.enabled")) {
			return false;
		}
		$PaymentDeliveryQueryService = new PaymentDeliveryQueryService();
		$body = array(
			"ad_id" => strval($this->ad_id),
			"category" => strval($this->category),
			"price" => sanitizeVar($_POST['price'], 'string'),
			"communes" => sanitizeVar($_POST['communes'], 'string'),
			"region" => sanitizeVar($_POST['region'], 'string'),
			"subject" => sanitizeVar($_POST['subject'], 'string'),
			"body" => sanitizeVar($_POST['body'], 'string'),
			"account" => "1"
		);
		bLogger::logInfo(__METHOD__, "El broma que se le envio al broma fue: ".print_r($body, true));
		return !$PaymentDeliveryQueryService->execute("ad_status", $body) == null;
	}

	function f_list_id($list_id){
                if (preg_match('/^[0-9]+$/', $list_id))
                        return $list_id;

                if (preg_match('/^[0-9]{8,}\.[0-9]+$/', $list_id))
                        return $list_id;

                if (!empty($list_id))
                        return false;

                return NULL;
	}

	function f_deletion_reason($deletion_reason){
		global $BCONF;
		if (bconf_get($BCONF, "*.deletion_reason.$deletion_reason"))
			return $deletion_reason;
		return null;
	}

	function f_passwd($passwd){
		return (string)$passwd;
	}

	function f_string($v) {
		return strip_tags($v);
	}


	/*FT839 This function cleans the title of various characters like the website.*/
	function clean_subject(&$subject){
		$subject = preg_replace("/\t/", " ", $subject);
		$subject = preg_replace("/ +/", " ", $subject);
		// $80 is euro in FF latin1
		$subject = preg_replace("/[!\$�\x80]/", '', $subject);
		$subject = preg_replace('/\*+/', '*', $subject);
		$subject = preg_replace('/^[*#<>~-]+/', '', $subject);
		$subject = preg_replace('/\(+/', '(', $subject);
		$subject = preg_replace('/\)+/', ')', $subject);
		$subject = preg_replace('/~+/', '~', $subject);
		$subject = preg_replace("/\\\\+/", "\\", $subject);
		if ($subject == strtoupper($subject)){
			$subject = strtolower($subject);
		}
		preg_match('/^(\.? ?)(.*)/', $subject, $match);
		$subject = $match[1] . ucfirst($match[2]);
	}

	function get_ad_data(&$tmpl_data){
		foreach($_POST as $k=>$v){
			if(is_string($k)){
				$tmpl_data['post_'.$k] = $v;
			}
		}
	}

	function set_uid_cookie($uid) {
		if (isset($uid) && !empty($uid)) {
			setcookie('uid', $uid, time() + 10 * 365 * 24 * 60 * 60, '/', bconf_get($BCONF, '*.common.session.cookiedomain')); // Expire in ten years!
			unset($uid);
		}
	}

	function get_ad_price() {
		get_settings(bconf_get($BCONF,"*.category_settings"), "price", array($this, "key_lookup"),
				create_function('$s,$k,$v,$d', '$d = $v;'), $price);
		return $price;
	}

	function key_lookup($setting, $key, $data = null) {
		global $BCONF;

		if ($key == 'pro_for_category') {
			$is_pro_for = $this->account->get_param('is_pro_for');
			$pro_for_cat = AccountUtil::is_pro_for_cat($is_pro_for, $this->category);
			return $pro_for_cat;
		}
		return @$this->$key;
	}
}


$mai = new blocket_mai();
$mai->run_fsm();

