<?php

openlog('MPU', LOG_ODELAY, LOG_LOCAL0);
require_once('autoload_lib.php');
require_once 'bImage.php';
require_once 'blocket_mobile_application.php';
require_once('bLogger.php');
require_once('JSON.php');

class blocket_mpu extends blocket_mobile_application {

	function blocket_mpu() {
		global $BCONF;
		global $session;
		$this->application_name = 'mpu';
		$this->file = '';
		$this->init('load', 0);
	}

	function get_state($name) {
		switch($name) {
			case 'load':
				return array(
					'function' => 'mpu_load',
					'method' => 'get',
					'params' => array('image')
				);

			case 'show':
				return array(
					'function' => 'mpu_show',
					'method' => 'get',
					'params' => array('image')
				);

			case 'form':
				return array(
					'function' => 'mpu_form',
					'method' => 'get',
					'params' => array('image', 'errors')
				);

			case 'upload':
				return array(
					'function' => 'mpu_upload',
					'method' => 'post',
					'params' => array('image', 'json')
				);
		}
	}

	/*////////////////////// filters ////////////////////*/ 
	function f_image($v) {
		return preg_match('/^[0-9]{3,20}\.jpe?g/', (string)$v) ? (string)$v : '';
	}

	function f_errors($v) {
		return array_map('intval', array_filter(explode(',', (string)$v)));
	}

	function f_json($v) {
		return preg_match('/^[01]$/', (string)$v) ? (string)$v : 0;
	}

	/*////////////////////// helper functions ////////////////////*/ 
	function logm($kind, $msg) {
		if (count($args = func_get_args()) > 1) {
			$msg = call_user_func_array('sprintf', $args);
		}
		$debug = var_export($_SERVER['CONTENT_TYPE'], true);
		syslog($kind, sprintf('%s mpu[%s] >> %s [%s]', log_string(), $this->current_state, $msg, $debug));
	}

	// transform image, generate thumbnail
	function transform() {
		global $BCONF;

		if ((!isset($_FILES['image']) || (isset($_FILES['image']['error']) && ($_FILES['image']['error'] != UPLOAD_ERR_OK)) || (!isset($_FILES['image']['name']) || (!strlen($_FILES['image']['name']))))) {
			$this->logm(LOG_ERR, 'upload error [%s/%s]', print_r($_FILES, true), UPLOAD_ERR_OK);
			return array(array(1), null, null);
		}

		$this->logm(LOG_DEBUG, 'image [%s] uploaded with type [%s] and size [%s]',
			$_FILES['image']['name'],
			$_FILES['image']['type'],
			$_FILES['image']['size']
		);

		// set the PHP memory limit to image max resolution * 4 (rgba) * 2 (to have enough memory to resize and etc)
		ini_set('memory_limit', (bconf_get($BCONF, '*.common.image.maxres') * 4 * 2).'M');
		ini_set('max_execution_time', bconf_get($BCONF, '*.common.image.max_execution_time'));

		// convert the uploaded image
		$image = new bImage($_FILES['image']['tmp_name']);

		switch($image->get_error()){
			case 'ERROR_IMAGE_TOO_BIG':
			return array(array(6), null, null);
			case 'ERROR_IMAGE_TOO_SMALL':
			return array(array(7), null, null);
			case 'ERROR_IMAGE_LOAD':
			return array(array(3), null, null);
		}

		//Explicitly silence errors, it's only a log after all
		$size = @$image->get_image_size();
		$size = sprintf("w:%s, h:%s", @$size[1], @$size[2]);
		$exif = @print_r(@$image->_exif, true);
		//Log original image size and exif data
		bLogger::logInfo(__METHOD__, "uploaded image from mobile size: $size, exif $exif");

		$buffer_thumbnail = $image->create_thumbnail();
		$image->resize();
		$buffer_image = $image->create_image();

		// check if image upload generated error messages
		if ($image->has_error()) {
			$error = $image->get_error();
			if (is_array($error)) {
				foreach ($error as $e) {
					$this->logm(LOG_ERR, 'resize error: [%s]', $e);
				}
			} else {
				$this->logm(LOG_ERR, 'resize error: [%s]', $error);
			}
			$res = array(array(2), null, null);
		} else {
			$this->logm(LOG_DEBUG, 'image [%s] conversion, status=OK', $_FILES['image']['name']);
			$res = array(array(), $buffer_image, $buffer_thumbnail);
		}
		
		// release image memory
		$image->destruct();

		// reset the memory limit to its original state
		ini_restore("memory_limit");
		ini_restore("max_execution_time");
		return $res;
	}

	function imgput() {
		$this->logm(LOG_DEBUG, 'imgput: will transform');
		list($errors, $image, $thumbnail) = $this->transform();
		if (count($errors)) {
			return $errors;
		}

		$this->logm(LOG_DEBUG, 'imgput: will trans imgput');
		$transaction = new bTransaction();
		$transaction->add_data('log_string', log_string());
		$transaction->add_file('image', $image);
		//$transaction->add_file('thumbnail', $thumbnail);
		$reply = $transaction->send_command('imgput');

		if ($reply['status'] != 'TRANS_OK' || !isset($reply['file'])) {
			foreach ($transaction->get_errors() as $e) {
				$this->logm(LOG_DEBUG, 'imgput error [%s]', $e);
			}
			return array(3);
		}

		$this->logm(LOG_DEBUG, 'retrieved image file [%s]', $reply['file']);
		return $reply['file'];
	}

	// upload a new version of the image and thumbnail
	function update_image($filename, $buffer_image, $buffer_thumbnail) {
		global $BCONF;
		$path_image = sprintf('/%s/%s', bconf_get($BCONF, "*.common.imagedir"), substr($filename, 0, 2));
		$path_thumbnail = sprintf('/%s/%s', bconf_get($BCONF, "*.common.thumbnaildir"), substr($filename, 0, 2));

		// upload new image
		$trans = new bTransaction();
		$trans->add_data('log_string', log_string());
		$trans->add_file('image', $buffer_image);
		$trans->add_data('filename', $filename);
		$trans->add_data('directory', $path_image);
		$reply = $trans->send_command('imgput_single');
		if ($reply['status'] != 'TRANS_OK' || !isset($reply['file'])) {
			$this->logm(LOG_ERR, 'failed to imgput [%s] image: %s', $filename, implode('; ', $trans->get_errors()));
			return false;
		}
		
		// upload new thumb
		$trans = new bTransaction();
		$trans->add_data('log_string', log_string());
		$trans->add_file('image', $buffer_thumbnail);
		$trans->add_data('filename', $filename);
		$trans->add_data('directory', $path_thumbnail);
		$reply = $trans->send_command('imgput_single');
		if ($reply['status'] != 'TRANS_OK' || !isset($reply['file'])) {
			$this->logm(LOG_ERR, 'failed to imgput [%s] thumbnail: %s', $filename, implode('; ', $trans->get_errors()));
			return false;
		}

		return true;
	}

	/*////////////////////// actions ////////////////////*/ 
	function mpu_load($image) {
		$this->logm(LOG_DEBUG, 'loaded image [%s]', $image);
		$this->redirect('form', array(
			'image' => $image,
			'errors' => ''
		));
	}

	function mpu_form($image, $errors) {
		global $BCONF;
		
		if ($image) {
			$image_path = substr($image, 0, 2).'/'.$image;
		}
		else {
			$image_path = '';
		}

		$tmpl_data = array(
			'error' => $errors,
			'image' => $image,
			'image_path' => $image_path
		);

		$this->display_mobile_layout('mobile/smartphone/mpu.html', $tmpl_data, null, null, 'stripped');

	}

	function mpu_upload($image, $json) {

		$result = $this->imgput();
		if (!$json && isset($_GET['json']))
			$json = $_GET['json'];
		if ($json == "1") {
			$image = is_array($result) ? null : $result ;
			$errors  = is_array($result) ? implode(',', $result) : null ;
			if ($image != null) {
				echo(json_encode(array('ok'=>true, 'image' => $image, 'success' => true)));
			} else {
				// I18N
				$messages = array(6 => bconf_get($BCONF, '*.language.ERROR_IMAGE_SIZE_BIG_OR_SMALL.es'),
								  7 => bconf_get($BCONF, '*.language.ERROR_IMAGE_SIZE_BIG_OR_SMALL.es'),
								  3 => bconf_get($BCONF, '*.language.ERROR_IMAGE_SERVER_FAILED.es'));
				echo(yapo_json_encode(array('ok'=>false, 'errors' => $errors, 'success' => false, 'error_message' => $messages[$errors])));
			}
			die();
		}
		$this->redirect('form', array(
			'image' => is_array($result) ? '' : $result,
			'errors' => is_array($result) ? implode(',', $result) : ''
		));
	}

	function mpu_rotate($image) {
		global $BCONF;
		// set the PHP memory limit to image max resolution * 4 (rgba) * 2 (to have enough memory to resize and etc)
		$this->logm(LOG_DEBUG, 'gonna rotate [%s]', $image);
		ini_set('memory_limit', (bconf_get($BCONF, '*.common.image.maxres') * 4 * 2).'M');
		ini_set('max_execution_time', bconf_get($BCONF, '*.common.image.max_execution_time'));

		// convert the uploaded image
		$bImage = new bImage();
		if ($bImage->load_from_server(intval($image)) === false) {
			$bImage->destruct();
			$this->redirect('form', array(
				'image' => $bImage,
				'errors' => '4'
			));
		}

		$bImage->rotate_image(90);
		$buffer_thumbnail = $bImage->create_thumbnail('', false);
		$bImage->resize();
		$buffer_image = $bImage->create_image();

		// check if image upload generated error messages
		$errors = array();
		if ($bImage->has_error()) {
			$this->logm(LOG_ERR, 'resize [%s] error', $image);
			$errors[] = 4;
		
		} elseif ($this->update_image($image, $buffer_image, $buffer_thumbnail) === false) {
			// error on trans imgput
			$errors[] = 5;

		} else {
			// all ok
			$this->logm(LOG_INFO, 'resize [%s] ok', $image);
		}
		
		// release image memory, reset the memory
		$bImage->destruct();
		ini_restore("memory_limit");
		ini_restore("max_execution_time");

		// back to the form
		$this->redirect('form', array(
			'image' => $image,
			'errors' => implode(',', $errors)
		));
	}

	function error_image() {
		header("Cache-Control: no-cache");
		header("Pragma: no-cache");
		header("Content-Type: image/png");
		die(base64_decode('iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABHNCSVQICAgIfAhkiAAAAAlwSFlz
AAALEgAACxIB0t1+/AAAABx0RVh0U29mdHdhcmUAQWRvYmUgRmlyZXdvcmtzIENTM5jWRgMAAAAV
dEVYdENyZWF0aW9uIFRpbWUAMi8xNy8wOCCcqlgAAAQRdEVYdFhNTDpjb20uYWRvYmUueG1wADw/
eHBhY2tldCBiZWdpbj0iICAgIiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+Cjx4Onht
cG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDQu
MS1jMDM0IDQ2LjI3Mjk3NiwgU2F0IEphbiAyNyAyMDA3IDIyOjExOjQxICAgICAgICAiPgogICA8
cmRmOlJERiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRh
eC1ucyMiPgogICAgICA8cmRmOkRlc2NyaXB0aW9uIHJkZjphYm91dD0iIgogICAgICAgICAgICB4
bWxuczp4YXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iPgogICAgICAgICA8eGFwOkNy
ZWF0b3JUb29sPkFkb2JlIEZpcmV3b3JrcyBDUzM8L3hhcDpDcmVhdG9yVG9vbD4KICAgICAgICAg
PHhhcDpDcmVhdGVEYXRlPjIwMDgtMDItMTdUMDI6MzY6NDVaPC94YXA6Q3JlYXRlRGF0ZT4KICAg
ICAgICAgPHhhcDpNb2RpZnlEYXRlPjIwMDgtMDMtMjRUMTk6MDA6NDJaPC94YXA6TW9kaWZ5RGF0
ZT4KICAgICAgPC9yZGY6RGVzY3JpcHRpb24+CiAgICAgIDxyZGY6RGVzY3JpcHRpb24gcmRmOmFi
b3V0PSIiCiAgICAgICAgICAgIHhtbG5zOmRjPSJodHRwOi8vcHVybC5vcmcvZGMvZWxlbWVudHMv
MS4xLyI+CiAgICAgICAgIDxkYzpmb3JtYXQ+aW1hZ2UvcG5nPC9kYzpmb3JtYXQ+CiAgICAgIDwv
cmRmOkRlc2NyaXB0aW9uPgogICA8L3JkZjpSREY+CjwveDp4bXBtZXRhPgogICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDUdUmQA
AACUSURBVDiNxZMxDoMwDEX/R8xcpifgJA0ng5N07NaNkzCF6bNQZJxCREHCkod85z/ZjkJJOBPF
KfflAJIAGUBqI9uEIGnJCIQIKJOt9dAucSBXG63m2kDC6YtQ2kJ03VUZPQGMyYD7+vUA32rvZj/c
wWNe4mcH5AEBQPc9v38bO3tYPSNJvICnv2TNtdRsAv6J+z/TBMuCUtS9gEARAAAAAElFTkSuQmCC'));
	}

	function mpu_show($image) {
		global $BCONF, $ai_instance, $index;

		if (!$image) {
			// no image by this ad
			$this->logm(LOG_DEBUG, 'showing [%s]: no such image', $image);
			$this->error_image();
		}

		$filename = substr($image, 0, 2).'/'.$image;
		$this->logm(LOG_DEBUG, 'will read [%s] -> [%s] (dir:%s)', $image, $filename, bconf_get($BCONF, "*.common.thumbnaildir"));
		if (!($data = http_readfile($filename, bconf_get($BCONF, "*.common.thumbnaildir"), true))) {
			// could not read file
			$this->logm(LOG_ERR, 'showing [%s]: could not read', $image);
			$this->error_image();
		}
		
		header("Cache-Control: no-cache");
		header("Pragma: no-cache");
		header("Content-Type: image/jpeg");
		die($data);
	}

	/*
	 *  This function embed thumbnail digest in image EXIF header
	 */
	function digest_embed($uploaded_image) {
		$digest = $uploaded_image->_thumb_digest;

		// Get jpeg header
		$jpeg_header_data = get_jpeg_header_data($uploaded_image);

		if ($jpeg_header_data == FALSE) {
			$uploaded_image->_error = 'ERROR_READING_JPEG_HEADER'; 
			syslog(LOG_ERR, log_string()."Couldn't read Image jpeg header:this picture hasnt md5 coef embed");
			return;
		}

		// New EXIF header construction
		// 15 is mandatory, 15 is leboncoin digest-md5 tag index
		$Exif_array_header = array('Byte_Align'=> 'MM',
			'Tags Name'=>'TIFF', 
			array(15 => array('Tag Name'=>'digest',
				'Tag Number' => 15,
				'Data Type'=>'2', 
				'Data'=> array($digest),
				'Type' => 'String')
			)
		);

		// EXIF header Insertion  in old JPEG header. Old exif header will be overwrited
		$new_jpeg_header_data = put_EXIF_JPEG( $Exif_array_header, $jpeg_header_data );
		if ($new_jpeg_header_data == FALSE) {
			$uploaded_image->_error = 'ERROR_MODIFYING_JPEG_HEADER'; 
			syslog(LOG_ERR, log_string()."Couldn't write exif header into jpeg header:this picture hasnt md5 coef embed");
			return;
		}

		// Get new file same data, header modified with our digest tag
		if (put_jpeg_header_data($uploaded_image, $uploaded_image->_image_buffer, $uploaded_image->_image_buffer, $new_jpeg_header_data) === FALSE) { 
			$uploaded_image->_error = 'ERROR_INSERTING_JPEG_HEADER'; 
			syslog(LOG_ERR, log_string()."Couldn't write exif header into jpeg header:this picture hasnt md5 coef embed");
			return;
		}
	}
}

$mpu = new blocket_mpu();
$mpu->run_fsm();

