<?php
require_once("bAd.php");
require_once("init.php");
require_once("bTransaction.php");
require_once("bImage.php");

define("BAD_CLEANUP", 1);
define("BAD_UNSET", 2);

class blocket_api_newad {

	public function __construct($list_id = 0, $account_id = NULL, $action = NULL) {
		$this->load_ad_images = array();
		$this->ad = new bAd();
		$this->account_id = $account_id;
		$this->already_paid_extra_images = 0;
		if ($list_id != 0) {
			$this->load_ad($list_id, $account_id, $action);
		}
		$this->set_upselling_data($list_id, $action);
	}

	public function validate_ad($ad_data) {
		return $this->handle_request($ad_data, 'validate_ad');
	}

	public function insert_ad($ad_data) {
		// force user passwd
		if (array_key_exists('passwd', $ad_data)) {
			$ad_data['passwd'] =  $this->hash_passwd($ad_data['passwd']);
		}

		$ret = $this->handle_request($ad_data, 'insert_ad');
		return $ret;
	}

	public function check_not_editable($ad_data) {
		global $BCONF;
		$not_editable = bconf_get($BCONF, "*.api.ad_params.not_editable");
		$non_editable_fields = array();
		foreach ($ad_data as $key => $val) {
			if (array_key_exists($key, $not_editable) && $not_editable[$key] == 1) {
				$non_editable_fields[] = $key;
			}
		}
		return $non_editable_fields;
	}

	public function update_ad($ad_data) {
		$ret = $this->handle_request($ad_data, 'update_ad');
		if ($ret['status'] == 'TRANS_ERROR' && @$ret['pay_type'] == 'ERROR_PAY_TYPE_INVALID' && $ad_data['pay_type'] == 'verify') {
			$ad_data['pay_type'] = 'phone';
			$ret = $this->handle_request($ad_data, 'update_ad');
		}
		return $ret;
	}

	public function upload_image_b64($b64_data, $image_name = 'b64_image', $image_source = 'api', $options) {
		$tmpdir = ini_get('upload_tmp_dir');
		if (!$tmpdir)
			$tmpdir = '/tmp';
		$filename = tempnam($tmpdir, "image_load_server");
		if (!$filename)
			return 'ERROR_IMAGE_TYPE';

		if (empty($b64_data)) {
			return 'ERROR_IMAGE_TYPE';
		}

		file_put_contents($filename, base64_decode($b64_data));
		$ret = $this->upload_image($filename, $image_name, $image_source, $options);
		unlink($filename);

		return $ret;
	}

	public function upload_image_binary($image_data, $image_source = 'api', $options = array()) {
		if (@$image_data['error'] == UPLOAD_ERR_OK && strlen(@$image_data['name'])) {
			syslog(LOG_INFO, log_string()."Image ({$image_data['name']}) uploaded via api with type ({$image_data['type']}) and size ({$image_data['size']})");
			return $this->upload_image($image_data['tmp_name'], $image_data['name'], $image_source, $options);
		} else if (strlen(@$image_data['name']) > 0) {
			syslog(LOG_WARNING, log_string()."Image upload went wrong, should never happen");
			return 'ERROR_IMAGE_LOAD';
		} else {
			return 'ERROR_UNKNOWN_ERROR';
		}
	}

	protected function upload_image($image_path, $image_name, $image_source = 'api', $options = array()) {
		/* Convert the uploaded image */
		$b_image = new bImage($image_path);
		if (isset($options['quality']))
			$b_image->set_image_quality($options['quality']);
		if (isset($options['use_original']))
			$b_image->set_use_original_image($options['use_original']);
		$thumbnail_buffer = $b_image->create_thumbnail();
		$b_image->resize();
		$new_size = $b_image->get_image_size();
		$image_buffer = $b_image->create_image();

		/* Check if image upload generated error messages */
		if ($b_image->has_error()) {
			$image = $b_image->get_error();
		} else {
			syslog(LOG_INFO, log_string()."Image ({$image_name}) conversion, status=OK");
			$image = array($image_buffer, $thumbnail_buffer, $new_size);
		}

		/* Release image memory */
		$b_image->destruct();

		if (empty($image)) {
			return 'ERROR_IMAGE_TYPE';
		}

		if (is_array($image)) {
			$transaction = new bTransaction();
			$transaction->add_data('log_string', log_string());
			$transaction->add_file('image', $image[0]);
			$transaction->add_file('thumbnail', $image[1]);
			$transaction->add_data('width', (int)$image[2][1]);
			$transaction->add_data('height', (int)$image[2][2]);
#			$transaction->add_data('comment', "source=$image_source");
			$reply = $transaction->send_command('imgput');
			if ($reply['status'] == 'TRANS_OK' && isset($reply['file'])) {
				return array('image_id' => $reply['file']);
			} else {
				return $reply['status'];
			}
		} else {
			return 'ERROR_IMAGE_TYPE';
		}
	}

	protected function handle_request($ad_data, $command) {
		global $BCONF;
		$data = array();

		if (isset($ad_data['type']) && preg_match('/'.$ad_data['type'].'/' , $ad_data['ad_type_limit']) && isset($ad_data['category']) && preg_match('/'.$ad_data['category'].'/', $ad_data['cat_limit'])) {

			$this->ad->populate($ad_data);

			$aux = $this->ad->get_data_array(BAD_CLEANUP | BAD_UNSET);

			$transaction = new bTransaction();
			$transaction->add_data('log_string', log_string());

			if (isset($ad_data['import'])) {
				$transaction->add_data('do_not_send_mail', 1);
				$transaction->add_data('link_type', $ad_data['link_type']);
				$transaction->add_data('external_ad_id', $ad_data['external_ad_id']);
			}
			$transaction_data = $this->ad->get_data_array(BAD_CLEANUP | BAD_UNSET);

			$images = array();
			$num_images_in_post = 0;
			foreach ($ad_data as $key => $val) {
				if (substr($key, 0, 8) == "image_id") {
					$num_images_in_post++;
					$imageIndex = substr($key, 8);
					$images[$imageIndex] = $val;
				}
			}
			ksort($images, SORT_NUMERIC);
			$transaction->add_data('image', $images);
			$transaction->populate($transaction_data);

			if ($command == 'insert_ad' || $command == 'update_ad') {
				$reply = $transaction->send_command('newad', true, false);
			} else {
				$reply = $transaction->validate_command('newad');
			}

			if ($transaction->has_error()) {
				$err_array = $transaction->get_errors();
				$err_msg = $transaction->get_messages();
				$reply['status'] = "TRANS_ERROR";
				$retData = $this->generate_reply($reply, $ad_data, $err_array, $err_msg);
			} else {
				$retData = $this->generate_reply($reply, $ad_data);
			}
		} else {
			$retData = array("status" => "INVALID_CATEGORY_OR_AD_TYPE");
		}
		return $retData;

	}

	protected function generate_reply($reply, $ad_data, $err_array = NULL, $err_msg = NULL) {
		$retData = array();
		$retData['status'] = $reply['status'];
		if(array_key_exists('list_id', $reply)) {
			$retData['list_id'] = $reply['list_id'];
		}

		if (isset($err_array)) {
			foreach($err_array as $err => $val) {
				if (is_array($val)) {
					$i = 0;
					foreach($val as $err2 => $val2) {
						$err2 = str_replace(';', '_', $err2);
						$val2 = str_replace(';', '_', $val2);
						if (!empty($err_msg[$err][$i])) {
							$val2 = $val2 . ":" . $err_msg[$err][$i];
						}
						$retData[$err][$i] = $val2;
						$i++;
					}
				} else {
					$err = str_replace(';', '_', $err);
					$val = str_replace(';', '_', $val);
					if (!empty($err_msg[$err])) {
						$val = $val . ":" . $err_msg[$err];
					}
					$retData[$err] = $val;
				}
			}
		}

		return $retData;
	}

	protected function cat_lookup($setting, $key, $data = null) {
		if (!empty($this->ad->sub_category)) {
			$cat = $this->ad->sub_category;
		} else if (!empty($this->ad->category_group)) {
			$cat = $this->ad->category_group;
		} else {
			$cat = $this->ad->category;
		}

		if ($key == 'category') {
			return $cat;
		} else if ($key == 'parent') {
			return bconf_get($BCONF, "*.cat.{$cat}.parent");
		}
	}

	protected function get_gallery_price() {
		global $BCONF;

		get_settings(bconf_get($BCONF, "*.gallery_settings"), "gallery_price", array($this, "cat_lookup"),
				create_function('$s,$k,$v,$d', '$d[$k] = $v;'), $price);

		if (isset($price['price']))
			return $price['price'];

		return 0;
	}

	protected function init_card_payment($reply) {
		$price = intval(substr($reply['order_id'], 0, 2)) * 5;
		$remote_browser = (isset($_SERVER['HTTP_USER_AGENT']) && !empty($_SERVER['HTTP_USER_AGENT'])) ? $_SERVER['HTTP_USER_AGENT'] : "Mobile API";
		/* Run payex_initialize transaction */
		$transaction = new bTransaction();
		$transaction->add_data('ad_id', $reply['ad_id']);
		$transaction->add_data('action_id', $reply['action_id']);
		$transaction->add_data('price', $price);
		$transaction->add_data('description', 'Blocketannons ' . $price . ' kr');
		$transaction->add_data('email', $this->ad->email);
		$transaction->add_data('order_id', $reply['order_id']);
		$transaction->add_data('instance_id', 0);
		$transaction->add_data('remote_browser', $remote_browser);
		$transaction->add_data('remote_addr', $_SERVER['REMOTE_ADDR']);
		$transaction->add_data('payment_group_id', $reply['payment_group_id']);
		$transaction->add_data('returnurl', bconf_get($BCONF, '*.common.base_url_httpsproxy.mobile') . bconf_get($BCONF, '*.payex.api_return_path'));
		$transaction->add_data('cancelurl', bconf_get($BCONF, '*.common.base_url_httpsproxy.mobile') . bconf_get($BCONF, '*.payex.api_cancel_path'));
		$trans_reply = $transaction->send_command('payex_initialize');
		return $trans_reply['orderref'];
	}

	protected function hash_passwd($passwd, $salt="", $loops=0) {
		global $BCONF;

		if ($salt == "") {
			for ($i = 0; $i < 16; $i++)
				$salt .= chr(mt_rand(32, 126));
		}

		if ($loops==0) {
			$loops = bconf_get($BCONF, "ai.passwd.stretch_loops");
		}

		for ($i = 0; $i < $loops; $i++)
			$passwd = sha1($salt . $passwd);

		return "\$$loops\$" . $salt . $passwd;
	}

	private function check_store_passwd($store_id, $store_username, $store_passwd) {
		$transaction = new bTransaction();
		$transaction->add_data('store_id', $store_id);
		$reply = $transaction->send_command('store_login_info');

		if ($transaction->has_error()) {
			return $transaction->get_errors();
		}

		$salt = $reply['salt'];
		$store_passwd = $salt . sha1($salt . $store_passwd);

		$transaction->reset();
		$transaction->add_client_info();
		$transaction->auth_type = 'store';
		$transaction->add_data('username', $store_username);
		$transaction->add_data('passwd', $store_passwd);
		$transaction->add_data('auth_type', 'store');
		$transaction->add_data('auth_resource', $store_id);

		$reply = $transaction->send_command('authenticate', true, false, false);

		return $reply;
	}

	public function get_account($email) {	// tillgodo=account, extremely poor name but I want to be consistent with previous names
		$check_account = new bTransaction();
		$check_account->add_data('email', $email);
		$reply = $check_account->send_command('newad_check_account');
		if ($reply['status'] == 'TRANS_OK' ) {
			return $reply['newad_check_account']['0']['account'];
		} else {
			return 0;
		}
	}

	public function load_ad($list_id, $account_id = NULL, $action = NULL) {
		global $BCONF;
		$load_ad = new bTransaction();
		$load_ad->add_data('id', $list_id);
		if (!empty($account_id) && $account_id > 0) {
			$load_ad->add_data('account_id', $account_id);
			$load_ad->add_data('action', $action);
		}

		$reply = $load_ad->send_command('loadad');
		if ($reply['status'] == "TRANS_OK") {
			$this->ad->populate($reply, true);
			$this->passwd = @$reply['ad']['salted_passwd'];
			if (!empty($reply['ad']['salt'])) {
				if (preg_match('/^\$([0-9]+)\$(.*)/', $reply['ad']['salt'], $regs)) {
					$this->loops = $regs[1];
					$this->salt = $regs[2];
				}
			}
			if (!empty($reply['images'])) {
				foreach ($reply['images'] as $key => $img) {
					$this->load_ad_images[] = $img['name'];
				}
				if(count($this->load_ad_images) > 1) {
					$this->already_paid_extra_images = 1;
				}
			}
			if ($reply['ad']['account_id'] > 0)
				$this->ad->account_id = $reply['ad']['account_id'];
			$reply = get_object_vars($this->ad);
		}
		return $reply;
	}

	public function ad_passwd_check($passwd = NULL) {
		/* Check if account exists on received email */
		$ad_password = $this->hash_passwd(@$passwd, @$this->salt, @$this->loops);
		$reply = array();
		if (!strcmp(@$this->passwd, $ad_password) || $this->account_id == @$this->ad->account_id) {
			$reply = array('status' => 'TRANS_OK');
		} else {
			$reply = array(
					'status' => 'TRANS_ERROR',
					'passwd' => 'ERROR_PASSWORD_WRONG'
				);
		}

		return $reply;
	}

	private function set_upselling_data($list_id, $action) {
		syslog(LOG_INFO, log_string() . "--> ");
	}

}

?>
