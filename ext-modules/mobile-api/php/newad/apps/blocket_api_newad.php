<?php
openlog("API_NEWAD", LOG_ODELAY, LOG_LOCAL0);

include_once('autoload_lib.php');
include_once('blocket_api_newad_class.php');

global $BCONF;
global $lang;

/* Determine return format */
$script_url = $_SERVER["SCRIPT_URL"];
$format = (array_key_exists('format', $_REQUEST) && $_REQUEST['format'] == 'json') ? 'json' : (preg_match("/\.(json|xml)$/i", $script_url, $matches) ? strtolower($matches[1]) : "xml");

openlog("API_NEWAD", LOG_ODELAY, LOG_LOCAL0);
$auth_output = call_autorize_template($format);

if (bconf_get($BCONF, "*.multilang.enabled") && isset($_REQUEST['lang'])) {
	$lang = $_REQUEST['lang'];
}

if (isset($_REQUEST['app_id'])) {
	$redis_key = bconf_get($BCONF, "*.common.redis_mobile_api.master").'xapp_id_'.$_REQUEST['app_id'];
}

if (!empty($auth_output)) {
	render_response($auth_output, $format);
} else {
	if (isset($_REQUEST['ad_id'])) {
		switch ($_REQUEST['action']) {
			case 'renew_ad':
				$trans_action = 'renew';
				break;
			case 'edit_ad':
				$trans_action = 'edit';
				break;
			default:
				$trans_action = NULL;
		}
		$api_newad = new blocket_api_newad($_REQUEST['ad_id'], @$account_id, @$trans_action);
	} else {
		$api_newad = new blocket_api_newad();
	}

	$limit = array();
	$limit = get_limitations($redis_key);

	// TODO, review this shit
	/* Set the PHP memory limit to image max resolution * 4 (rgba) * 2 (to have enough memory to resize and etc) */
	ini_set("memory_limit", (bconf_get($BCONF, "*.common.image.maxres") * 4 * 2)."M");
	ini_set("max_execution_time", bconf_get($BCONF, "*.common.image.max_execution_time"));

	$cmd = false;

	if (isset($limit['logging']) && $limit['logging'] == 1 && $_REQUEST['action'] == "insert_ad") {
		$reqString = "";
		foreach ($_REQUEST as $key => $val) {
			if (!preg_match("/^image([0-9]+)$/", $key)) {
				$reqString = $reqString . "&" . $key . "=" . $val;
			}
		}
		syslog(LOG_INFO, log_string()."User $redis_key made request: $reqString");
	}

	switch ($_REQUEST['action']) {
		// TODO: remove
		case 'renew_ad':
			$api_newad->ad_type = "renew";

		case 'edit_ad':
			if (!isset($api_newad->ad_type)) {
				/* uggly hack to let renew fall through to this state */
				$api_newad->ad_type = "edit";
			}
			$pass_check = $api_newad->ad_passwd_check(@$_REQUEST['passwd']);
			if ($pass_check['status'] != "TRANS_OK") {
				$ret = $pass_check;
				break;
			}
			$not_editable = $api_newad->check_not_editable($_REQUEST);
			if (!empty($not_editable)) {
				$ret = array();
				foreach ($not_editable as $field) {
					$ret[$field] = "NOT_EDITABLE";
				}
				$ret["status"] = "TRANS_ERROR";
				break;
			}

			$ad_data = array_merge(get_object_vars($api_newad->ad), $_REQUEST, $limit);

			$ret = $api_newad->update_ad($ad_data);
			break;

		case 'insert_ad':
			if (isset($_REQUEST['import']) && !isset($_REQUEST['external_ad_id'])) {
				if (!isset($_REQUEST['external_ad_id'])) {
					$ret = array(
						'status' => 'ERROR_IMPORT',
						'messsage' => 'MISSING_EXTERNAL_AD_ID'
					);
				}
				break;
			} else {
				$cmd = 'insert';
			}

		case 'validate_ad':
			$cmd = $cmd ? $cmd : 'validate';
			$ad_data = array_merge($_REQUEST, $limit);

			if (isset($_REQUEST['import'])) {
				$ad_data['link_type'] = $_REQUEST['app_id'];
			}
			// force company_ad
			$ad_data['company_ad'] = 1;

			// force user passwd
			$passwd = substr(md5(@$ad_data['email'] . $ad_data['app_id']), 0, 10);
			$ad_data['passwd'] = $passwd;

			if ($cmd == 'insert') {
				$ret = $api_newad->insert_ad($ad_data);
			} else {
				$ret = $api_newad->validate_ad($ad_data);
			}
			break;

		case 'upload_image':
			if (array_key_exists('image', $_FILES) && is_array($_FILES['image'])) {
				$ret = $api_newad->upload_image_binary($_FILES['image']);
			} else {
				$ret = $api_newad->upload_image_b64($_REQUEST['image'], 'b64_image', 'api', array());
			}
			if (is_array($ret)) {
				$ret['status'] = 'OK';
			} else {
				$ret = array(
					'status' => 'IMAGE_ERROR',
					'message' => $ret
				);
			}

			break;
		default:
			$ret = array(
				'status' => 'ERROR',
				'messsage' => 'INVALID ACTION'
			);
			break;
	}

	/* Reset the memory limit to its original state */
	ini_restore("memory_limit");
	ini_restore("max_execution_time");

	render_response($ret, $format);
}

function get_limitations($key) {
    $response = new bResponse();
    $response->add_data('key', $key);

    $val = array();

    $lines = explode("\n", trim($response->call_template('apitemplates/get_limitations.txt', true)));
    foreach ($lines as $l) {
        $kv = explode('=', $l, 2);
        $val[$kv[0]] = $kv[1];
    }

    return $val;
}

function render_response($data, $format) {
	global $lang;
	$trans_response = new bResponse($lang);
	switch ($format) {
		case "json":
			$mime_type = "application/json";
			break;

		case "xml":
		default:
			$mime_type = "text/xml";
			break;
	}

	/*
	 * Headers to disable caching
	 */
	header("Content-type: " . $mime_type);
	header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");


	if (is_array($data)) {
		$trans_response->add_extended_array("data", $data);
	} else {
		$trans_response->add_data("data", $data);
	}
	$trans_response->call_template("apitemplates/newad_resp.{$format}");
}

function call_autorize_template($format) {
	$response = new bResponse();
	$response->add_data('application_name', 'api_newad');		//MJA: REFACTOR, appication_name vs application???

	if (isset($_REQUEST['app_id'])) {
		$response->add_data('app_id', $_REQUEST['app_id']);
	}

	if (isset($_REQUEST['hash'])) {
		$response->add_data('hash', $_REQUEST['hash']);
	}

	if (isset($_REQUEST['import'])) {
		$response->add_data('application', 'import');
	} else {
		$response->add_data('application', 'newad');
	}

	return trim($response->call_template('apitemplates/authorize.' . $format, true));
}

