<?php

require_once('autoload_lib.php');
require_once 'util.php';
require_once 'blocket_mobile_application.php';
require_once('bTransaction.php');
require_once('ApiGatewayClass.php');
require_once('JSON.php');

openlog('MDELETE', LOG_ODELAY, LOG_LOCAL0);

class blocket_delete extends blocket_mobile_application {
	var $account_session;
	function blocket_delete() {
		global $BCONF;
		global $session;
		$this->account_session = new AccountSession();
		$this->application_name = 'mdelete';
		$this->mobile_appl = 'delete';
		$this->file = '';
		$this->init('delete_response', 0);
	}

	function get_state($name) {
		switch($name) {
			case 'delete_response':
				return array(
					'function' => 'delete_response',
					'method' => 'post',
					'params' => array(
						'list_id',
						'deletion_reason' => 'f_deletion_reason',
						'passwd',
						'password_hash' => 'f_string',
						'passwdHash' => 'f_passwd',
						'testimonial' => 'f_string',
						'sold_time' => 'f_integer',
						'user_text' => 'f_string'
					)
				);
		}
	}

	function delete_response($list_id, $deletion_reason, $passwd, $passwdHash, $password_hash, $testimonial, $sold_time, $user_text) {
		if (!empty($passwdHash)) {
			$passwd = $passwdHash;
		} else if (!empty($password_hash)) {
			$passwd = $password_hash;
		}

		$trans = new bTransaction();
		$trans->add_data('id', $list_id);
		$result = $trans->send_command('loadad');

		if ($trans->has_error(true) || $result['status'] != 'TRANS_OK' ) {
			bLogger::logError(__METHOD__, "loadad failed for list_id: ${list_id}");
			bLogger::logError(__METHOD__, var_export($trans->get_errors(), true));
			echo yapo_json_encode(array(
				'result' => array('status'=> 'TRANS_ERROR'),
				'error' => bconf_get($BCONF, '*.language.ERROR_AD_DELETE_FAILED.es')
			));
			return 'FINISH';
		}
		$email = $result['users']['email'];
		$ad_id = $result['ad']['ad_id'];
		$cat_id = $result['ad']['category'];
		$has_if = 0;
		if(isset($result['params']['inserting_fee'])){
			$has_if = $result['params']['inserting_fee'];
		}

		if(empty($passwd)){
			if($this->account_session->is_logged()){
				if($this->account_session->get_param('email') == $email){
					$passwdHash = sha1($this->account_session->get_param('password'));
					$passwd = $passwdHash;
				}
			}
		}
		else{
			if(!$this->account_session->is_logged()){
				$this->account_session->login($email,$passwd);
			}
			else{
				if($this->account_session->get_param('email') != $email){
					$account_new_session = new AccountSession();
            		if($account_new_session->login($email, $passwd)){
                		syslog(LOG_INFO, log_string()."User " . $email . " changing session");
                		$account_new_session->expire();
                		$this->account_session->expire();
                		$this->account_session->login($email, $passwd);
					}
				}
			}
		}

		$is_pro = 0;
		$is_logged = 0;
		if($this->account_session->is_logged()){
			$is_logged = 1;
			$is_pro_for = $this->account_session->get_param('is_pro_for');
			$pack_status = $this->account_session->get_param('pack_status');
			if(allow_delete_ad($is_pro_for, $pack_status, $cat_id, $has_if) && $email == $this->account_session->get_param('email')){
				$is_pro = 1;
			}
		}

		$data = array('ad_id' => $list_id,
					'deletion_reason' => $deletion_reason,
					'is_pro' => $is_pro,
					'passwd' => $passwd);

		if(!empty($testimonial)){
			$data['testimonial'] = $testimonial;
		}


		  if(!empty($passwdHash)){
		  	$data['passwdHash'] = $passwdHash;
		  }


		if(!empty($sold_time)){
			$data['sold_time'] = $sold_time;
		}

		if(!empty($user_text)){
			$data['user_text'] = preg_replace('/[\x00-\x1F\x80-\xFF]/', ' ', trim($user_text));
		}

		// $this->api_\response('deletead', 'mai', $data);
		$api = new ApiGateway();
		$api_response = $api->call_action('deletead', $data);
		bLogger::logInfo(__METHOD__, "API_RESPONSe:".print_r($api_response, true));
		if (!empty($api_response['status']) && $api_response['status'] == "TRANS_OK") {
			$api_response['logged'] = $is_logged;
			$api_response['pro'] = $is_pro;

			$main_cat_id = bconf_get($BCONF, "*.cat.{$cat_id}.parent");
			$main_cat_name = utf8_encode(bconf_get($BCONF, "*.cat.{$main_cat_id}.name"));

		}

		echo yapo_json_encode($api_response);
		return 'FINISH';
	}

	function f_list_id($list_id) {
                if (preg_match('/^[0-9]+$/', $list_id))
                        return $list_id;

                if (preg_match('/^[0-9]{8,}\.[0-9]+$/', $list_id))
                        return $list_id;

                if (!empty($list_id))
                        return false;

                return NULL;
	}

	function f_deletion_reason($deletion_reason) {
		global $BCONF;

                if (bconf_get($BCONF, "*.deletion_reason.$deletion_reason"))
                        return $deletion_reason;
                return null;
	}

	function f_passwd($passwd) {
		return (string)$passwd;
	}

	 function f_string($v) {
		 return strip_tags($v);
	 }
}

$delete = new blocket_delete();
$delete->run_fsm();
