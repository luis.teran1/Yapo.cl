<?php
require_once('blocket_mobile_application.php');

/* 
   Mobile adwatch:

   madwatch is a simple version of the regular adwatch application, in this lighter version no cookies are managed, instead of that it works
   just using ids, witch should be managed by the client. (The response of this madwatch app is a json). 

   When watch_ad or save methods are called without an unique_id (or with a invalid one) a new id is create and returned in 
   the json.

   Save this id is a task that should be done by the caller. Saving it as a cookie if it is a site, or file/database/somewhere 
   if it is a nonweb application.

   Once the client received the cookie it should be sent to the nexts commands sent to this application.
*/
require_once('bTransaction.php');

openlog("MOBILE_ADWATCH", LOG_ODELAY, LOG_LOCAL0);

class blocket_madwatch extends blocket_mobile_application {
	var $watch_unique_id;
        var $watch_ads;
        var $watch_ads_cachetimeout;
	var $watch_query_id;

	 function blocket_madwatch() { 
		global $BCONF;

		$this->application_name = 'madwatch';
		$this->init('start', 1, true);
	}

	function get_state($statename) {
		switch($statename) {
			case 'start':
				return array('function' => 'aw_start',
						'method' => 'get');
			case 'save':
				return array('function' => 'aw_save',
						'params' => array ('query_id' => 'f_integer', 'unique_id' => 'f_string'),
						'method' => 'both');

			case 'watch_ad':
				return array('function' => 'aw_watch_ad',
						'params' => array('aid' => 'f_integer', 'unique_id' => 'f_string'),
						'method' => 'both');
			case 'delete_ad': 
				return array('function' => 'aw_delete_ad',
						'params' => array('aid' => 'f_integer', 'unique_id' => 'f_string'), 
						'method' => 'both'); 

			case 'delete':
				return array('function' => 'aw_delete',
						'params' => array ('query_id' => 'f_integer', 'unique_id' => 'f_string'),
						'method' => 'both');
		}
	}

        function f_string($v) {                                                                                                                                                                                    
                return strip_tags($v);
        }

	function aw_start() {
	}

	function aw_delete_ad($list_id, $unique_id) {
		global $BCONF;
		$this->watch_unique_id = $unique_id;

		$transaction = new bTransaction();

		if ($this->watch_unique_id) 
			$transaction->add_data('watch_unique_id', $this->watch_unique_id);

		$transaction->add_data('list_id', $list_id);

		$reply = $transaction->send_command('delete_watch_ad', true, true);

		if ($transaction->has_error()) {
			$this->set_errors($transaction->get_errors());

			die (json_encode (array ('status' => reset($this->errors), 'unique_id' => $this->watch_unique_id)));
		}

		die (json_encode (array ('status' => 'OK', 'unique_id' => $this->watch_unique_id)));
	}

	function aw_delete($query_id, $unique_id) {
		global $BCONF;
		$this->watch_unique_id = $unique_id;

		$transaction = new bTransaction(); 
		$transaction->add_data('watch_unique_id', $this->watch_unique_id); 
		$transaction->add_data('watch_query_id', $query_id); 

		$reply = $transaction->send_command('delete_watch_query', true, true);  

		if($transaction->has_error()) {
			$this->set_errors($transaction->get_errors());
			die (json_encode (array ('status' => reset($this->errors), 'unique_id' => $this->watch_unique_id)));
		}

		die (json_encode (array ('status' => 'OK', 'unique_id' => $this->watch_unique_id)));
	}

        function aw_watch_ad($list_id, $unique_id) {
		global $BCONF;
		$this->watch_unique_id = $unique_id;
                if ($this->watch_ad($list_id)) {
			die (json_encode (array ('status' => 'OK', 'unique_id' => $this->watch_unique_id)));
		}
		die (json_encode (array ('status' => reset($this->errors), 'unique_id' => $this->watch_unique_id)));
        }

        function aw_save($query_id, $unique_id) {
		global $BCONF;
		$this->watch_unique_id = $unique_id;
                if ($this->watch_query($_SERVER['QUERY_STRING'], $query_id)) {
			die (json_encode (array ('status' => 'OK', 'unique_id' => $this->watch_unique_id, 'watch_query_id' => $this->watch_query_id)));
                }

		die (json_encode (array ('status' => reset($this->errors), 'unique_id' => $this->watch_unique_id)));
        }

        function filter_query_string($qs) {
                global $BCONF;

                $vars = bconf_get($BCONF, '*.qs.vars');
                $res = array();
                parse_str($qs, $query);
                foreach ($query as $key => $val) {
                        $ignore_in_watch_query_regex = @$vars[$key]['ignore_in_watch_query_regex'];
                        if ($ignore_in_watch_query_regex && preg_match("/$ignore_in_watch_query_regex/", $val))
                                continue;

                        $regex = @$vars[$key]['regex'];
                        if ($regex && preg_match("/$regex/", $val))
                                $res[$key] = $val;
                }
                return http_build_query($res);
        }

        function watch_ad($list_id) {

                $transaction = new bTransaction();
                if ($this->watch_unique_id)
                        $transaction->add_data('watch_unique_id', $this->watch_unique_id);
                $transaction->add_data('list_id', $list_id);
                $reply = $transaction->send_command('watch_ad', true, true);
                if ($transaction->has_error()) {
                        $this->set_errors($transaction->get_errors());
                        return false;
                }

                $unique_id = $reply['watch_ad'][0]['watch_unique_id'];
                if ($unique_id && $unique_id != $this->watch_unique_id) {
                        $this->watch_unique_id = $reply['watch_ad'][0]['watch_unique_id'];
                }

                return true;
        }

	function watch_query($query_string, $query_id = 0) {

                $qs = $this->filter_query_string($query_string);

                $transaction = new bTransaction();

                if ($this->watch_unique_id)
                        $transaction->add_data('watch_unique_id', $this->watch_unique_id);
                if ($query_id)
                        $transaction->add_data('watch_query_id', $query_id);
                $transaction->add_data('query_string', $qs);

                $reply = $transaction->send_command('watch_query', true, true);

                if ($transaction->has_error()) {
                        $this->set_errors($transaction->get_errors());
                        return false;
                }

                $unique_id = $reply['watch_query'][0]['watch_unique_id'];

		$watch_query_id = $reply['watch_query'][0]['watch_query_id'];

		if ($watch_query_id)
			$this->watch_query_id = $watch_query_id;
		else
			$this->watch_query_id = "";

                if ($unique_id && $unique_id != $this->watch_unique_id) {
                        $this->invalidate_watch_ads_cache(); /* Invalidate cache */
                        /* Save unique id */
                        $this->watch_unique_id = $reply['watch_query'][0]['watch_unique_id'];
                }
               
		 return true;
        }

        function invalidate_watch_ads_cache() {
                $this->watch_ads = null;
                $this->watch_ads_cachetimeout = null;
        }
}

$madwatch = new blocket_madwatch();
$madwatch->run_fsm();
?>
