#!/bin/bash

# Initialize our own variables:
redis_host=ch36
redis_port=6396
expire=300
region=1,2,3,4,5,6,7,8,9,10,11,12,13,14,15
ad_type=h,k,s,u
appl=import,importdeletead,newad
cat=1000,1220,1240,1260,2000,2020,2060,2040,2100,2080,2120,5000,5020,5040,5060,5160,4000,4020,4040,4060,9000,9020,9040,9060,6000,6140,6020,6060,6180,6080,6100,6120,6160,3000,3040,3060,3020,3080,7000,7020,7040,7060,7080,8000,8020
requests=100000
log=1

function show_help {
	echo "key_gen.sh: Generate redis key for api partner"
	echo
	echo -e "\t-a appl     | Application endpoints to allow access to"
	echo -e "\t-c cat      | Categories in which partner can publish ads"
	echo -e "\t-e expire   | How long until we renew the token (in seconds)"
	echo -e "\t-p partner  | Partner name. Mandatory"
	echo -e "\t-r region   | Regions in which partner can publish ads [1-15]"
	echo -e "\t-r ad_type  | Types of ads the partner can publish [h,k,s,u]"
	echo
	echo "All parameters (except partner) take comma separated lists"
	echo
}

while getopts "ha:c:e:p:r:t:" opt; do
    case "$opt" in
    h)
        show_help
        exit 0
        ;;
	a)  appl=$OPTARG
		;;
	c)  cat=$OPTARG
		;;
	e)  expire=$OPTARG
		;;
	p)  partner=$OPTARG
		;;
    r)  region=$OPTARG
        ;;
    t)  ad_type=$OPTARG
        ;;
    esac
done

shift $((OPTIND-1))

[ "$1" = "--" ] && shift

if [ "$partner" = "" ]; then
	show_help
	echo "Error: Please specify a partner (-p)"
	exit 1
fi

key=$(dd if=/dev/urandom bs=32 count=8 status=none| sha1sum | awk '{print $1}')

echo -n "redis-cli -host $redis_host -port $redis_port HMSET "
echo -n "mcp1xapp_id_$partner "
echo -n "api_key $key "
echo -n "expire_time $expire "
echo -n "region_limit $region "
echo -n "ad_type_limit $ad_type "
echo -n "request_limit $requests "
echo -n "appl_limit $appl "
echo -n "cat_limit $cat "
echo -n "logging $log"
echo
