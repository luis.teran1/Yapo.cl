#include <bpapi.h>


/*
 * urldecode filter
 */
struct _urldec{
	int pos;
	char buf[1024];
};

static void flush_urldecode(struct bpapi_output_chain *ochain);

static int get_hexval(const unsigned char c)
{
	if(c >= '0' && c <= '9') {
		return c - '0';
	} else if(c >='a' && c <= 'f') {
		return c - 'a' + 10;
	} else if(c >='A' && c <= 'F') {
		return c - 'A' + 10;
	}
	// XXX Maybe CRIT log here later.. 
	return 0;
}

static int
urldecode_outstring_raw(struct bpapi_output_chain *ochain, const char *str, size_t len) {
	const unsigned char *from;
	const unsigned char *end;
	unsigned char c;
	unsigned char c2,c3;
	int res;

	struct _urldec *ue = ochain->data;

	from = (unsigned char*) str;
	end = (unsigned char*) str + len;

	while (from < end) {
		if (ue->pos >= 1010) {
			res += ue->pos;
			flush_urldecode(ochain);
		}

		c = *from++;

		if (c == '+') {
			ue->buf[ue->pos++] = ' ';
		} else if (c == '%' && *from && *(from+1)) {
			c2 = *from++;
			c3 = *from++;
			ue->buf[ue->pos++] = (get_hexval(c2) << 4) | get_hexval(c3);
		} else {
			ue->buf[ue->pos++] = c;
		}
	}

	return 0;
}

static int
init_urldecode(const struct bpfilter *filter, struct bpapi *api, int argc, const struct tpvar **argv) {
	return 0;
}

static void
fini_urldecode(struct bpapi_output_chain *ochain) {
	flush_urldecode(ochain);
}

static void
flush_urldecode(struct bpapi_output_chain *ochain) {
	struct _urldec *ue = ochain->data;
	if (ue->pos) {
		bpo_outstring_raw(ochain->next, ue->buf, ue->pos);
		ue->pos = 0;
	}
}

static const struct bpapi_output urldecode_output = {
	self_outstring_fmt,
	urldecode_outstring_raw,
	fini_urldecode,
	flush_urldecode
};

ADD_OUTPUT_FILTER(urldecode, sizeof(struct _urldec));
