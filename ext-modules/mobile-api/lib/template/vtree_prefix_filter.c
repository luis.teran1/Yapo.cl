#include <bpapi.h>
#include <string.h>
#include <ctype.h>
#include <time.h>
#include <stdio.h>
#include <ctemplates.h>
#include <stdlib.h>
#include "util.h"
#include "settings.h"
#include "patchvars.h"


/*
 * This filter will be integrated in V2 sometimes soon.
 * In the meantime, include it here, in the usual overridable way
 */

struct vtree_prefix_data {
    struct shadow_vtree svt;
    struct bpapi_vtree_chain subtree;
    char * pe;  
};

static void
vtree_prefix_free(struct shadow_vtree *svt) {
    struct vtree_prefix_data *sd = (struct vtree_prefix_data *)svt;
    free(sd->pe);
}

static inline int 
_init_vtree_prefix(const struct bpfilter *filter, struct bpapi *api, int optional, int argc, const struct tpvar **argv) {
    struct vtree_prefix_data *sd = BPAPI_VTREE_DATA(api); 
    const char * prefix = tpvar_str(argv[0], &sd->pe);

    int i;
    const char *keys[argc];
    for (i = 1 ; i < argc; i++) {
        TPVAR_STRTMP(keys[i - 1], argv[i]);
    }   
    keys[i - 1] = NULL;


    if(!vtree_getnode_cachev(api->vchain.next, NULL, &sd->subtree, NULL, argc - 1, keys)) {
        free(sd->pe);
        return optional;
    }   

    prefix_vtree_init(&sd->svt.vtree, prefix, &sd->subtree);    
    sd->svt.free_cb = vtree_prefix_free;

    return 0;
}

static int 
init_vtree_prefix(const struct bpfilter *filter, struct bpapi *api, int argc, const struct tpvar **argv) {
    return _init_vtree_prefix(filter, api, 0, argc, argv);
}
static int 
init_vtree_prefix_required(const struct bpfilter *filter, struct bpapi *api, int argc, const struct tpvar **argv) {
    return _init_vtree_prefix(filter, api, 1, argc, argv);
}


#define vtree_prefix_vtree shadow_vtree
#define vtree_prefix_vtree_required shadow_vtree

#ifdef ADD_FILTER_WEAK
ADD_FILTER_WEAK(vtree_prefix, NULL, 0, NULL, 0, &shadow_vtree, sizeof(struct vtree_prefix_data));
ADD_FILTER_WEAK(vtree_prefix_required, NULL, 0, NULL, 0, &shadow_vtree, sizeof(struct vtree_prefix_data));
#else
ADD_FILTER(vtree_prefix, NULL, 0, NULL, 0, &shadow_vtree, sizeof(struct vtree_prefix_data));
ADD_FILTER(vtree_prefix_required, NULL, 0, NULL, 0, &shadow_vtree, sizeof(struct vtree_prefix_data));
#endif

