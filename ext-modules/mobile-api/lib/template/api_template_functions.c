#include <sys/types.h>
#include <time.h>
#include <ctype.h>
#include <stdio.h>
#include <stdint.h>
#include <ctemplates.h>
#include <unistd.h>
#include <sha1.h>
#include <bpapi.h>

static void
generate_ad_hash(char output[], const char* salt, const char * input, int stretch_loops) {
	SHA1_CTX ctx;
	unsigned char digest[SHA1_DIGEST_LENGTH];
	int n,i;

	SHA1Init(&ctx);
	SHA1Update(&ctx, (const u_int8_t *)salt, strlen(salt));
	SHA1Update(&ctx, (const u_int8_t *)input, strlen(input));
	SHA1Final(digest, &ctx);

	for (n = 0; n < stretch_loops ; ++n) {

		for (i = 0; i < SHA1_DIGEST_LENGTH; i++) {
			sprintf(&output[i * 2], "%02x", (unsigned int)digest[i]);
		}

		SHA1Init(&ctx);
		SHA1Update(&ctx, (const u_int8_t *)salt, strlen(salt));
		SHA1Update(&ctx, (unsigned char*)output, SHA1_DIGEST_STRING_LENGTH-1);
		SHA1Final(digest, &ctx);

	}

	for (n = 0; n < SHA1_DIGEST_LENGTH; n++)
		sprintf(&output[n * 2], "%02x", (unsigned int)digest[n]);
}

static const struct tpvar *
generate_salted_ad_passwd(struct bpapi *api, struct tpvar *dst, enum bpcacheable *cc, int argc, const struct tpvar **argv) {
	char result[SHA1_DIGEST_STRING_LENGTH] = "EMPTY PASSWORD";
	char *hash = NULL;
	int stretch_loops = 0;

	if(argc != 2)
		return TPV_SET(dst, TPV_NULL);

	const char *input;
	const char *salt;
	TPVAR_STRTMP(input, argv[0]);
	TPVAR_STRTMP(salt, argv[1]);
	const char *s;

	if (*input != '\0' && *salt != '\0') {
		if (sscanf(salt, "$%d$", &stretch_loops) == 1) {
			s = strchr(salt+1, '$') + 1;
			generate_ad_hash(result, s, input, stretch_loops-1);
		}
	}
	xasprintf(&hash, "%s%s", salt, result);
	return TPV_SET(dst, TPV_DYNSTR(hash));
}

ADD_TEMPLATE_FUNCTION_WEAK(generate_salted_ad_passwd);

/*
 * Check if value exists in list
 * Usage:
 * &in_list("needle", "list_name");
 * returns the first index of hit, NULL if not found
 */

static const struct tpvar *
in_list(struct bpapi *api, struct tpvar *dst, enum bpcacheable *cc, int argc, const struct tpvar **argv) {
	int i;
	int list_len;
	char *ret;
	const char *needle;
	const char *list;

	if (argc != 2)
		return TPV_SET(dst, TPV_NULL);

	TPVAR_STRTMP(needle, argv[0]);
	TPVAR_STRTMP(list,  argv[1]);

	*cc = BPCACHE_CANT;

	list_len = bpapi_length(api, list);

	for (i = 0; i < list_len; i++) {
		const char *list_val = bpapi_get_element(api, list, i);
		if ( list_val && strcmp(needle, list_val) == 0 ) {
			xasprintf(&ret, "%d", i);
			return TPV_SET(dst, TPV_DYNSTR(ret));
		}
	}
	return TPV_SET(dst, TPV_NULL);
}

ADD_TEMPLATE_FUNCTION_WEAK(in_list);

/*
 * Hooks for local functions
 */
static const struct tpvar *
get_price_lowered(struct bpapi *api, struct tpvar *dst, enum bpcacheable *cc, int argc, const struct tpvar **argv) {
	/* Noop */
	return TPV_SET(dst, TPV_NULL);
}

ADD_TEMPLATE_FUNCTION_WEAK(get_price_lowered);

static const struct tpvar *
image_url(struct bpapi *api, struct tpvar *dst, enum bpcacheable *cc, int argc, const struct tpvar **argv) {

	/* Noop */
	return TPV_SET(dst, TPV_NULL);
}

ADD_TEMPLATE_FUNCTION_WEAK(image_url);

