#Fixing makefile includes
ifndef __ext-modules_mobile-api_mk_commonsrc_mk
__ext-modules_mobile-api_mk_commonsrc_mk:=1

# Define USE_MOBILE_TEMPLATE_API
# if you want to link into your module
# (probably mod_template) the mobile API
# templates / filters
ifdef USE_MOBILE_TEMPLATE_API

USE_TRANS_FILTERS=yes
USE_BLOCKET_FUNCTIONS=yes
USE_REDIS_FILTERS=yes
USE_VTREE_FILTERS=yes
USE_SEARCH_FILTERS=yes
USE_JSON_FILTERS=yes
USE_LOOP_FILTERS=yes

USE_TEMPLATES += ${MOBILEDIR}/templates/apitemplates
USE_TEMPLATES += ${MOBILEDIR}/templates/apitests
#USE_TEMPLATES += ${MOBILEDIR}/templates/apisql

SRCS += vtree_prefix_filter.c
SRCS += api_template_functions.c
VPATH := ${MOBILEDIR}/lib/template:${VPATH}
endif


ifdef USE_URL_DECODE
SRCS += api_template_filters.c

# Commented out because it is the same as below
#VPATH := $(MOBILEDIR)/lib/template:${VPATH}
endif

ifdef USE_MOBILE_TRANS
include ${MOBILEDIR}/daemons/trans/local.mk
endif

endif #__ext-modules_mobile-api_mk_commonsrc_mk
