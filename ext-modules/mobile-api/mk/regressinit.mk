#Fixing makefile includes
ifndef __ext-modules_mobile-api_mk_regressinit_mk
__ext-modules_mobile-api_mk_regressinit_mk:=1

TOPDIR?=../../..

include ${TOPDIR}/mk/defvars.mk
include ${TOPDIR}/mobile/mk/defvars.mk

# As reported by matthias@kapaza.be, why use this in production?? XXX: Why at all ??
#IM_REGRESS:=$(shell fgrep "*.*.common.regress" ${REGRESS_FINALDIR}/conf/bconf.txt.site | cut -d"=" -f2)
IM_REGRESS:=$(shell grep "common.basedir" ${REGRESS_FINALDIR}/conf/bconf.txt.site  2> /dev/null | cut -d"=" -f2 | grep regress_final  | wc -l)
ifeq (${IM_REGRESS},1)
REQUEST_LIMIT=request_limit 100000
CATEGORY_LIMIT=cat_limit $(shell grep "cat.*name=" ${NINJA_REGRESS_DIR}/conf/bconf.txt.categories | cut -d "." -f4 | uniq | tr "\n" "," | sed -e "s/,$$//g")
REGION_LIMIT=region_limit $(shell grep "region.*name.*=" ${NINJA_REGRESS_DIR}/conf/bconf.txt.regions | cut -d. -f5 | uniq | tr "\n" "," | sed -e 's/,$$//g')
endif

# Add API configuration to the defined
add-api-key:
	printf "HMSET mcp1xapp_id_fuenzalida api_key 746e1a72b4172e50d9c6b4f981312a1803283a66 ads_limit -1 expire_time 300 ${REGION_LIMIT} ad_type_limit b,h,k,s,u ${REQUEST_LIMIT} appl_limit import,importdeletead,linkshelf,list,get_save_searches,get_saved_ads,view_phone,view,areas,categories,category_settings,category_params,sendmail,region_getcommunes,settings,related_ads,newad,sendtip,deletead,findstores,account_login,account_create,account_modify,account_change_password,account_lost_password,account_get_unpublished_ads,account_get_published_ads,account_adwatches,account_get_adwatch_ads,account_info,account_adwatch_create,account_adwatch_delete,account_save_ad,account_delete_save_ad,account_get_saved_ads,account_adwatch_email_start,account_adwatch_email_stop,list_latest,payment_status,payment_choice,account_create_jobb,getregion,getinsertingfee,account_jobb_list,sendpass,cars_data,cars_getmodels,cars_getversions,cars_getattributes,api_conf,delete_reasons,faq_list,support,rules,forgot_pass,my_ads,ad_password,ad_refused ${CATEGORY_LIMIT}" | ${REGRESS_REDIS_CLIENT} -p ${REGRESS_REDIS_PORT}
	printf "HMSET mcp1xapp_id_blocket_mobile api_key 746e1a72b4172e50d9c6b4f981312a1803283a74 expire_time 300 ${REGION_LIMIT} ad_type_limit b,h,k,s,u ${REQUEST_LIMIT} appl_limit import,importdeletead,linkshelf,list,get_save_searches,get_saved_ads,view_phone,view,areas,categories,category_settings,category_params,sendmail,region_getcommunes,settings,related_ads,newad,sendtip,deletead,findstores,account_login,account_create,account_modify,account_change_password,account_lost_password,account_get_unpublished_ads,account_get_published_ads,account_adwatches,account_get_adwatch_ads,account_info,account_adwatch_create,account_adwatch_delete,account_save_ad,account_delete_save_ad,account_get_saved_ads,account_adwatch_email_start,account_adwatch_email_stop,list_latest,payment_status,payment_choice,account_create_jobb,getregion,getinsertingfee,account_jobb_list,sendpass,cars_data,cars_getmodels,cars_getversions,cars_getattributes,api_conf,delete_reasons,faq_list,support,rules,forgot_pass,my_ads,ad_password,ad_refused ${CATEGORY_LIMIT}" | ${REGRESS_REDIS_CLIENT} -p ${REGRESS_REDIS_PORT}
	printf "HMSET mcp1xapp_id_android_app api_key 2751bfb91c28e4702c0bd4f9b7378a0f81701699 expire_time 300 ${REGION_LIMIT} ad_type_limit b,h,k,s,u ${REQUEST_LIMIT} appl_limit import,importdeletead,linkshelf,list,get_save_searches,get_saved_ads,view_phone,view,areas,categories,category_settings,category_params,sendmail,region_getcommunes,settings,related_ads,newad,sendtip,deletead,findstores,account_login,account_create,account_modify,account_change_password,account_lost_password,account_get_unpublished_ads,account_get_published_ads,account_adwatches,account_get_adwatch_ads,account_info,account_adwatch_create,account_adwatch_delete,account_save_ad,account_delete_save_ad,account_get_saved_ads,account_adwatch_email_start,account_adwatch_email_stop,list_latest,payment_status,payment_choice,account_create_jobb,getregion,getinsertingfee,account_jobb_list,sendpass,cars_data,cars_getmodels,cars_getversions,cars_getattributes,api_conf,delete_reasons,faq_list,support,rules,forgot_pass,my_ads,ad_password,ad_refused ${CATEGORY_LIMIT}" | ${REGRESS_REDIS_CLIENT} -p ${REGRESS_REDIS_PORT}
	printf "HMSET mcp1xapp_id_iphone_app api_key 57bf1caa03f887e8aa3d4a6e39ad183800a77679 expire_time 300 ${REGION_LIMIT} ad_type_limit b,h,k,s,u ${REQUEST_LIMIT} appl_limit import,importdeletead,linkshelf,list,get_save_searches,get_saved_ads,view_phone,view,areas,categories,category_settings,category_params,sendmail,region_getcommunes,settings,related_ads,newad,sendtip,deletead,findstores,account_login,account_create,account_modify,account_change_password,account_lost_password,account_get_unpublished_ads,account_get_published_ads,account_adwatches,account_get_adwatch_ads,account_info,account_adwatch_create,account_adwatch_delete,account_save_ad,account_delete_save_ad,account_get_saved_ads,account_adwatch_email_start,account_adwatch_email_stop,list_latest,payment_status,payment_choice,account_create_jobb,getregion,getinsertingfee,account_jobb_list,sendpass,cars_data,cars_getmodels,cars_getversions,cars_getattributes,api_conf,delete_reasons,faq_list,support,rules,forgot_pass,my_ads,ad_password,ad_refused ${CATEGORY_LIMIT}" | ${REGRESS_REDIS_CLIENT} -p ${REGRESS_REDIS_PORT}

#	printf "HMSET mcp1xapp_id_blocket_mobile api_key 746e1a72b4172e50d9c6b4f981312a1803283a74 region_limit 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15 ad_type_limit b,h,k,s,u request_limit 100000 appl_limit import,importdeletead,linkshelf,list,view,areas,categories,category_settings,category_params,sendmail,settings,related_ads,newad,sendtip,deletead,findstores,account_login,account_create,account_modify,account_change_password,account_lost_password,account_get_unpublished_ads,account_get_published_ads,account_adwatches,account_get_adwatch_ads,account_info,account_adwatch_create,account_adwatch_delete,account_save_ad,account_delete_save_ad,account_get_saved_ads,account_adwatch_email_start,account_adwatch_email_stop,list_latest,payment_status,payment_choice,account_create_jobb,account_jobb_list cat_limit 1000,1020,1040,1060,1080,1100,1120,2000,2020,2040,2060,2080,2100,2120,3000,3020,3040,3060,3080,5000,5020,5040,5060,5100,5120,5140,6000,6020,6060,6080,6100,6120,6140,6160,6180,6200,7000,7020,7040,7060,7080,8000,8020"

reinstall-mobile rem:
	make -C ${MOBILEDIR}/templates/apitemplates install DESTDIR=${REGRESS_FINALDIR} INSTALLROOT=${TOPDIR}/regress_final BUILD_STAGE=REGRESS
	make -C ${MOBILEDIR}/templates/apisql install DESTDIR=${REGRESS_FINALDIR} INSTALLROOT=${TOPDIR}/regress_final BUILD_STAGE=REGRESS
	make -C ${MOBILEDIR}/conf install DESTDIR=${REGRESS_FINALDIR} INSTALLROOT=${TOPDIR}/regress_final BUILD_STAGE=REGRESS
	make -C ${MOBILEDIR}/php install DESTDIR=${REGRESS_FINALDIR} INSTALLROOT=${TOPDIR}/regress_final BUILD_STAGE=REGRESS
	make -C ${TOPDIR} remt reml ti
	make -C ${MOBILEDIR} redis_mobile_api-build-stop redis_mobile_api-build-start

#You have to add this objetive to your main project to stop and clean mobile services.
mobile-stop:
	make redis_mobile_api-build-stop

# Just for the Mobile APP
redis_mobile_api-build-start:
	@if [ -f ${REGRESS_REDIS_CLIENT} ]; then \
		if (cd ${TOPDIR}; util/lock redis_mobile_api_build${GENPORTOFF}); then \
			echo -e "${PURPLE}Starting redis_mobile_api${NO_COLOR}" && \
			mkdir -p ${REGRESS_FINALDIR}/redis_mobile_api >/dev/null 2>&1 && \
			exec ${REGRESS_REDIS_SERVER} ${TOPDIR}/regress_final/conf/redis_mobile_api.conf; \
			echo -n "${GREEN}Waiting for redis_mobile_api to start${NO_COLOR}"; \
			while ${REGRESS_REDIS_CLIENT} -p ${REGRESS_REDISSTAT_PORT} get nonexistantvar > /dev/null 2>&1 && ((try++ < 50)); do echo -n "."; sleep 0.5; done ; echo; \
			if ((try == 51)); then \
				echo "${RED}redis_mobile_api unresponsive${NO_COLOR}"; \
				exit 1; \
			fi; \
		else \
			echo -e "${GREEN}Already started!${NO_COLOR}" && \
			true; \
		fi; \
	else \
		if (cd ${TOPDIR}; util/lock redis_mobile_api_build${GENPORTOFF}); then \
			if (test -n ${DESTDIR} -a ${DESTDIR} != " "); then \
				perl -p -i.bak -w -e s=${DESTDIR}=${TOPDIR}/build=g ${TOPDIR}/build/conf/redis_mobile_api.conf; \
			fi; \
			echo -e "${PURPLE}Starting redis_mobile_api${NO_COLOR}" && \
			mkdir -p ${TOPDIR}/build/redis_mobile_api >/dev/null 2>&1 && \
			exec ${REGRESS_REDIS_SERVER} ${TOPDIR}/build/conf/redis_mobile_api.conf; \
			echo -n "${GREEN}Waiting for redis_mobile_api to start${NO_COLOR}"; \
			while ${TOPDIR}/build/daemons/redis/redis-cli -p ${REGRESS_REDIS_PORT} get nonexistantvar > /dev/null 2>&1 && ((try++ < 50)); do echo -n "."; sleep 0.5; done ; echo; \
			if ((try == 51)); then \
				echo "${RED}redis_mobile_api unresponsive${NO_COLOR}"; \
				exit 1; \
			fi; \
		else \
			echo -e "${GREEN}Already started!${NO_COLOR}" && \
			true; \
		fi; \
	fi
	make add-api-key

redis_mobile_api-build-stop:
	@if [ -f ${REGRESS_REDIS_CLIENT} ]; then \
		if (cd ${TOPDIR}; util/unlock redis_mobile_api_build${GENPORTOFF}); then \
			echo -e "${PURPLE}Stopping redis_mobile_api${NO_COLOR}" && \
			kill -9 `cat ${REGRESS_FINALDIR}/.redis_mobile_api.pid` ; \
			while ! ${REGRESS_REDIS_CLIENT} -p  ${REGRESS_REDIS_PORT}  get nonexistantvar > /dev/null 2>&1  &&  ((try++)); do echo -n "."; sleep 0.2; done; echo; \
			if ((try == 51)); then \
				echo -e "${RED}redis_mobile_api failed to stop${NO_COLOR}"; \
				exit 1; \
			else \
				rm -f ${REGRESS_FINALDIR}/.redis_mobile_api.pid; \
			fi; \
		else \
			echo -e "${PURPLE}No lockfile present${NO_COLOR}"; \
		fi; \
	else \
		if (cd ${TOPDIR}; util/unlock redis_mobile_api_build${GENPORTOFF}); then \
			echo -e "${PURPLE}Stopping redis_mobile_api${NO_COLOR}" && \
			kill -9 `cat ${TOPDIR}/build/.redis_mobile_api.pid` ; \
			while ! ${TOPDIR}/build/daemons/redis/redis-cli -p  ${REGRESS_REDIS_PORT}  get nonexistantvar > /dev/null 2>&1  &&  ((try++)); do echo -n "."; sleep 0.2; done; echo; \
			if ((try == 51)); then \
				echo -e "${RED}redis_mobile_api failed to stop${NO_COLOR}"; \
				exit 1; \
			else \
				rm -f ${TOPDIR}/build/.redis_mobile_api.pid; \
			fi; \
			if (test -f "${TOPDIR}/build/conf/redis_mobile_api.conf.bak"); then \
				mv ${TOPDIR}/build/conf/redis_mobile_api.conf.bak ${TOPDIR}/build/conf/redis_mobile_api.conf; \
			fi; \
		else \
			echo -e "${PURPLE}No lockfile present${NO_COLOR}"; \
		fi; \
	fi

redis_mobile_api-dropall:
	@if [ -f ${REGRESS_REDIS_CLIENT} ]; then \
		printf "dropall\n" | ${REGRESS_REDIS_CLIENT} -p ${REGRESS_REDIS_PORT}; \
	else \
		printf "dropall\n" | ${TOPDIR}/build/daemons/redis/redis-cli -p ${REGRESS_REDIS_PORT}; \
	fi

pgsql-mobile-start:
	@echo -e "${PURPLE}Starting mobile postgresql configuration${NO_COLOR}"
	/usr/bin/psql -h ${REGRESS_PGSQL_HOST} blocketdb -a -f ${TOPDIR}/mobile/scripts/db/create_region_polygons.sql
	${TOPDIR}/mobile/scripts/import_kml.php ${KML_MAP} host='${REGRESS_PGSQL_HOST} dbname=blocketdb'

endif #__ext-modules_mobile-api_mk_regressinit_mk
