import sys
import testapi
import re
import string
import xml.dom.minidom
import json

cats=[[1000,"Inmuebles"], [1020,"Departamentos y piezas"], [1040,"Casas"], [1060,"Oficinas"], [1080,"Comercial e industrial"], [1100,"Terrenos"], [1120,"Estacionamientos, bodegas y otros"], [2000,"Vehiculos"], [2020,"Autos, camionetas y 4x4"], [2040,"Buses, camiones y furgones"], [2060,"Motos"], [2080,"Barcos, lanchas y aviones"], [2100,"Accesorios y piezas para vehiculos"], [2120,"Otros vehiculos"], [3000,"Computadores & electronica"], [3020,"Consolas y videojuegos"], [3040,"Computadores y accesorios"], [3060,"Celulares y telefonos"], [3080,"Audio, TV, video y fotografia"], [5000,"Tu hogar y ti"], [5020,"Muebles y articulos del hogar"], [5040,"Electrodomesticos"], [5060,"Jardin y herramientas"], [5100,"Moda y vestuario"], [5120,"Embarazadas, bebes y ninos"], [5140,"Bolsos, bisuteria y accesorios"], [6000,"Tiempo libre"], [6020,"Deportes y gimnasia"], [6060,"Bicicletas y ciclismo"], [6080,"Instrumentos musicales"], [6100,"Musica y peliculas (DVDs, etc.)"], [6120,"Libros y revistas"], [6140,"Animales y sus accesorios"], [6160,"Arte, antiguedades y colecciones"], [6180,"Hobbies y outdoor"], [6200,"Salud y belleza"], [7000,"Servicios, negocios y empleo"], [7020,"Ofertas de empleo"], [7040,"Busco empleo"], [7060,"Servicios"], [7080,"Negocios, maquinaria y construccion"], [8000,"Otros"], [8020,"Otros productos"]]


if len(sys.argv) < 2:
	print "usage %s <host:port>" % sys.argv[0]
	sys.exit()

args = sys.argv[1].split(":")
 
testapi.API_HOST = args[0]
testapi.API_PORT = args[1]
testapi.API_PARTNER = "blocket_mobile"
testapi.API_KEY = "746e1a72b4172e50d9c6b4f981312a1803283a74"
 
fs = {"xml":  [testapi.perform_xml, "{{.*}}\n{{.*}}\n\n{{.*}}\n"], 
      "json": [testapi.perform_json, "{{.*}}\n{{.*}}\n\n{{.*}}\n\n"]}


def gen_test(prefix, v, params, t):
	print "REGRESS_TARGETS += %s" % prefix

	fi=open(prefix + ".in", "wt")
	fi.write("list \"%s\" %s" % (params, t))
	fi.close()

	fo=open(prefix + ".out", "wt")
	clean = re.sub("%s:[0-9]+/" % (testapi.API_HOST), "{{.*}}/", v[0]("list", "POST", params))

        if k == "json":
                clean = json.dumps(json.loads(clean, encoding="ISO-8859-1"), sort_keys=False, indent=4)
        elif k == "xml":
                clean = string.replace(clean, "&", "&amp;")
                clean = xml.dom.minidom.parseString(clean).toprettyxml()

	fo.write( v[1]+clean+"\n" )
	fo.close()


print "# list test by categories"
for cat in cats:
	#print "# category %d (%s)" % (cat[0], cat[1])	
	for k,v in fs.items():
		prefix = "tapi-list-%d-%s" % (cat[0], k)
		gen_test(prefix, v, "&cg=%d" % cat[0], k)

print "# list test by pagination"
for i in (1,2,3,4):
	for k,v in fs.items():
		prefix = "tapi-list-pagination-%d-%s" % (i, k)
		gen_test(prefix, v, "&o=%d" % i, k)

print "# list test by simplequery"
for q in ("accord", "mercedes", "britax"):
        for k,v in fs.items():
                prefix = "tapi-list-simplequery-%s-%s" % (q, k)
                gen_test(prefix, v, "&q=%s" % q, k)

print "# list test by pricerange"
for r in ([1,3], [1,2], [2,3]):
        for k,v in fs.items():
                prefix = "tapi-list-pricerange-%d-%d-%s" % (r[0], r[1], k)
                gen_test(prefix, v, "&cg=2020&ps=%d&pe=%d" % (r[0], r[1]), k)

