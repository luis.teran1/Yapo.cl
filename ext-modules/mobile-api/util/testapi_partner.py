#!/usr/bin/python2.6
import httplib
import hashlib
import json
import sys
import mimetools
import mimetypes
import urllib2
reload(sys)

sys.setdefaultencoding("latin-1")

API_HOST = None
API_PORT = None
API_PARTNER = "blocket_mobile"
API_KEY = "746e1a72b4172e50d9c6b4f981312a1803283a74"

def out(a):
    pass

def out2(a):
    sys.stdout.write("%s\n" % a)

def perform_json(template, method, params):
    out("Executing: "+template)

    server = "/api/%s.json" % (template)
    conn = httplib.HTTPSConnection(API_HOST, API_PORT)

    dd = {"GET": ("%s?app_id=%s" % (server, API_PARTNER), None, {}),
              "POST": ("%s" % (server), "app_id=%s" % (API_PARTNER), {"Content-Type": "application/x-www-form-urlencoded"}) }

    conn.request(method, dd[method][0], dd[method][1], dd[method][2])
    r1 = conn.getresponse()

    payload = r1.read()
    #print "payload: %s\n" % payload
    try:
        challenge = json.loads(payload)['authorize']['challenge']
        out("challenge: %s\n" % (challenge))
    except:
        out("[-] Error getting the challenge (authentication).")

    h=hashlib.new('sha1')
    h.update("%s%s" % (challenge, API_KEY))
    response = h.hexdigest()
    out("[+] response calculated: %s\n" % (response))

    if template == 'newad' and 'upload_image' in params:

        # blame Luis :-)
        import os
        pwd = os.path.dirname(os.path.abspath(__file__))
        image = pwd + '/../../../www/img/TryOutBg.jpg'
        with open(image) as f:
            data2 = post_multipart(API_HOST+":"+API_PORT,server,[("app_id",API_PARTNER),("hash",response),("action","upload_image")], [('image', image, f.read())])
        return data2

    else:
        url_params = ""
        if params != None :
            url_params = url_params + params

        dd = {"GET": ("%s?app_id=%s&hash=%s%s" % (server, API_PARTNER, response, url_params), None, {}),
              "POST": ("%s" % (server), "app_id=%s&hash=%s%s" % (API_PARTNER, response, url_params), {"Content-Type": "application/x-www-form-urlencoded"}) }

        conn.request(method, dd[method][0], dd[method][1], dd[method][2])

        r2 = conn.getresponse()
        if r2.status != 200:
            out("[-] Status is not 200 (%d)" % r2.status)

        data2 = r2.read()
        conn.close()
        return data2

def post_multipart(host, selector, fields, files):
    """
    Post fields and files to an http host as multipart/form-data.
    fields is a sequence of (name, value) elements for regular form fields.
    files is a sequence of (name, filename, value) elements for data to be uploaded as files
    Return the server's response page.
    """
    content_type, body = encode_multipart_formdata(fields, files)
    headers = {'Content-Type': content_type,
               'Content-Length': str(len(body))}
    r = urllib2.Request("https://%s%s" % (host, selector), body, headers)
    return urllib2.urlopen(r).read()

def encode_multipart_formdata(fields, files):
    """
    fields is a sequence of (name, value) elements for regular form fields.
    files is a sequence of (name, filename, value) elements for data to be uploaded as files
    Return (content_type, body) ready for httplib.HTTP instance
    """
    BOUNDARY = mimetools.choose_boundary()
    CRLF = '\r\n'
    L = []
    for (key, value) in fields:
        L.append('--' + BOUNDARY)
        L.append('Content-Disposition: form-data; name="%s"' % key)
        L.append('')
        L.append(value)
    for (key, filename, value) in files:
        L.append('--' + BOUNDARY)
        L.append('Content-Disposition: form-data; name="%s"; filename="%s"' % (key, filename))
        L.append('Content-Type: %s' % get_content_type(filename))
        L.append('')
        L.append(value)
    L.append('--' + BOUNDARY + '--')
    L.append('')
    body = CRLF.join(L)
    content_type = 'multipart/form-data; boundary=%s' % BOUNDARY
    return content_type, body

def get_content_type(filename):
    return mimetypes.guess_type(filename)[0] or 'application/octet-stream'

if  __name__ == "__main__":
    out = out2
    if len(sys.argv) < 4:
        print "usage %s <host:port> <app> <params> [json|json_raw] [POST|GET]" % sys.argv[0]
        sys.exit()

    args = sys.argv[1].split(":")
    args_app = sys.argv[2]
    args_params = sys.argv[3]

    args_type = "json"
    args_method = "GET"

    if len(sys.argv) > 4:
        args_type = sys.argv[4]
        if len(sys.argv) > 5:
            args_method = sys.argv[5]

    if len(args) != 2 or args_type not in ["json", "json_raw"] or args_method not in ["GET", "POST"]:
        print "usage %s <host:port> <app> <params> [json|json_raw] [POST|GET]" % sys.argv[0]
        sys.exit()

    API_HOST = args[0] # "tetsuo.schibsted.cl"
    API_PORT = args[1] # 23004

    fs = {"json": perform_json, "json_raw": perform_json}

    datos = fs[args_type](args_app, args_method, args_params)

    if args_type == "json":
        print json.dumps(json.loads(datos, encoding="ISO-8859-1"), sort_keys=False, indent=4)
    else:
        print datos
