#!/usr/bin/php -q
<?php

function exists($haystack,$needle) {
	$pos = strpos($haystack,$needle);
	
	if($pos === false) {
		return false;
	} else {
		return true;
	}
}

if ($argc != 3) {
	echo "Call this script with input and output files.\n";
	exit(0);
}


$key_get = "vars";
$key_post = "post_vars";

$content = '';
$lines = file($argv[1]);
foreach($lines as $line_num => $line)
{
	if (exists($line, "%METHOD%") && exists($line, "%STYLE%")){
		$json_get = str_replace("%METHOD%", $key_get, $line);
		$json_get = str_replace("%STYLE%", "json", $json_get);
		$json_post = str_replace("%METHOD%", $key_post, $line);
		$json_post = str_replace("%STYLE%", "json", $json_post);
		$xml_get = str_replace("%METHOD%", $key_get, $line);
		$xml_get = str_replace("%STYLE%", "xml", $xml_get);
		$xml_post = str_replace("%METHOD%", $key_post, $line);
		$xml_post = str_replace("%STYLE%", "xml", $xml_post);
		$content .= $json_get . $json_post . $xml_get . $xml_post;
	} else if (exists($line, "%METHOD%")) {
		$get = str_replace("%METHOD%", $key_get, $line);
		$post = str_replace("%METHOD%", $key_post, $line);
		$content .= $get . $post;
	} else if (exists($line, "%STYLE%")) {
		$json = str_replace("%STYLE%", "json", $line);
		$xml = str_replace("%STYLE%", "xml", $line);
		$content .= $json . $xml;
	} else {
		$content .= $line;
	}
}
file_put_contents($argv[2], $content);

?>
