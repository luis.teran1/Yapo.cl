#!/usr/bin/python2.6
import httplib
import hashlib
import json
import xml.dom.minidom
import sys
import string
reload(sys)

#sys.setdefaultencoding("ISO-8859-1")
sys.setdefaultencoding("latin-1")


API_HOST = None
API_PORT = None
API_PARTNER = "blocket_mobile"
API_KEY = "746e1a72b4172e50d9c6b4f981312a1803283a74"


def out(a):
	pass

def out2(a):
	sys.stdout.write("%s\n" % a)

def perform_json(template, method, params):
	out("Executing: "+template)

	server = "/apitemplates/%s.json" % (template)
	conn = httplib.HTTPSConnection(API_HOST, API_PORT)

	dd = {"GET": ("%s?app_id=%s" % (server, API_PARTNER), None, {}), 
              "POST": ("%s" % (server), "app_id=%s" % (API_PARTNER), {"Content-Type": "application/x-www-form-urlencoded"}) }

	conn.request(method, dd[method][0], dd[method][1], dd[method][2])
	
	r1 = conn.getresponse()
	
	payload = r1.read()
	#print "payload: %s\n" % payload
	try:
		challenge = json.loads(payload)['authorize']['challenge']
		out("challenge: %s\n" % (challenge))
	except:
		out("[-] Error getting the challenge (authentication).")

	h=hashlib.new('sha1')
	h.update("%s%s" % (challenge, API_KEY))
	response = h.hexdigest()
	out("[+] response calculated: %s\n" % (response))

	url_params = ""
	if params != None :
		url_params = url_params + params

	dd = {"GET": ("%s?app_id=%s&hash=%s%s" % (server, API_PARTNER, response, url_params), None, {}), 
              "POST": ("%s" % (server), "app_id=%s&hash=%s%s" % (API_PARTNER, response, url_params), {"Content-Type": "application/x-www-form-urlencoded"}) }

	conn.request(method, dd[method][0], dd[method][1], dd[method][2])

	r2 = conn.getresponse()
	if r2.status != 200:
		out("[-] Status is not 200 (%d)" % r2.status)

	data2 = r2.read()
	conn.close()
	return data2


def perform_xml(template, method, params):
	out("Executing: "+template)

	server = "/apitemplates/%s.xml" % (template)

	conn = httplib.HTTPConnection(API_HOST, API_PORT)

	dd = {"GET": ("%s?app_id=%s" % (server, API_PARTNER), None, {}), 
              "POST": ("%s" % (server), "app_id=%s" % (API_PARTNER), {"Content-Type": "application/x-www-form-urlencoded"}) }

	conn.request(method, dd[method][0], dd[method][1], dd[method][2])

	r1 = conn.getresponse()

        payload = r1.read()
        #print "payload: %s\n" % payload
        try:
		oxml = xml.dom.minidom.parseString(payload)
		challenge = oxml.getElementsByTagName('authorize')[0].getElementsByTagName('challenge')[0].childNodes[0].data
                out("challenge: %s\n" % (challenge))
        except:
                out("[-] Error getting the challenge (authentication).")

	h=hashlib.new('sha1')
	h.update("%s%s" % (challenge, API_KEY))
	response = h.hexdigest()
	out("[+] response calculated: %s" % (response))

	url_params = ""
	if params != None :
		url_params = url_params + params

	dd = {"GET": ("%s?app_id=%s&hash=%s%s" % (server, API_PARTNER, response, url_params), None, {}), 
              "POST": ("%s" % (server), "app_id=%s&hash=%s%s" % (API_PARTNER, response, url_params), {"Content-Type": "application/x-www-form-urlencoded"}) }

	conn.request(method, dd[method][0], dd[method][1], dd[method][2])

	r2 = conn.getresponse()
	#print r2.getheaders()
	#print r2.status, r2.reason
	if r2.status != 200:
		out("[-] Status is not 200 (%d)" % r2.status)

	data2 = r2.read()
	conn.close()
	return data2


if  __name__ == "__main__":
	out = out2
	if len(sys.argv) < 4:
		print "usage %s <host:port> <app> <params> [xml|json|xml_raw|json_raw] [POST|GET]" % sys.argv[0]
		sys.exit()

	args = sys.argv[1].split(":")
	args_app = sys.argv[2]
	args_params = sys.argv[3]

	args_type = "json"
	args_method = "GET"

	if len(sys.argv) > 4:
		args_type = sys.argv[4]
		if len(sys.argv) > 5:
			args_method = sys.argv[5]

	if len(args) != 2 or args_type not in ["json", "xml", "json_raw", "xml_raw"] or args_method not in ["GET", "POST"]:
		print "usage %s <host:port> <app> <params> [xml|json|xml_raw|json_raw] [POST|GET]" % sys.argv[0]
		sys.exit()



	API_HOST = args[0] # "tetsuo.schibsted.cl"
	API_PORT = args[1] # 23004


	fs = {"xml": perform_xml, "xml_raw": perform_xml, "json": perform_json, "json_raw": perform_json}

	datos = fs[args_type](args_app, args_method, args_params)

	if args_type == "json":
		print json.dumps(json.loads(datos, encoding="ISO-8859-1"), sort_keys=False, indent=4)
	elif args_type == "xml":
		datos=string.replace(datos, "&", "&amp;") 
		print xml.dom.minidom.parseString(datos).toprettyxml()
	else:
		print datos
