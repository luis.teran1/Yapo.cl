#!/bin/bash

# This script generate a bconf base file for api ad_params.
# 
# Version 0.1 2012-10.25
# Boris Cruchet C.
# Dany Jaque H.
#
# TODO:
# - get the type for the param

FILE=$1
BCONF_PATH=`dirname $FILE`
count=0
count_order=0;
for line in `cat $FILE | egrep "^\*\.\*.common.ad_params.*.name"`; do

	# *.*.common.ad_params.20.aind=1
	param=`echo $line | cut -d= -f 2`
	id=`echo $line | cut -d. -f 5`

	# order
	echo "*.*.api.ad_params.newad.order.$count_order=$param"
	echo "*.*.api.ad_params.list.order.$count_order=$param"
	echo ""

	count_order=`echo "$count_order+1" | bc -q`

done

for line in `cat $FILE | egrep "^\*\.\*.common.ad_params.*.name"`; do

	# *.*.common.ad_params.20.aind=1
	param=`echo $line | cut -d= -f 2`
	id=`echo $line | cut -d. -f 5`
	translate=`egrep "label_settings.$param.*.default" $BCONF_PATH/bconf.txt.labels | grep label_settings  | cut -d: -f 2`

	# new ad
	echo "*.*.api.ad_params.$param.newad.$count.name=$param"
	echo "*.*.api.ad_params.$param.newad.$count.ad_param=$param"
	echo "*.*.api.ad_params.$param.newad.$count.type=<fill with the correct type here>"
	echo "*.*.api.ad_params.$param.newad.$count.translate=$tranlate"

	# list ad
	echo "*.*.api.ad_params.$param.list.$count.name=$param"
	echo "*.*.api.ad_params.$param.list.$count.ad_param=$param"
	echo "*.*.api.ad_params.$param.list.$count.type=<fill with the correct type here>"
	echo "*.*.api.ad_params.$param.list.$count.translate=$translate"

	echo ""
done
