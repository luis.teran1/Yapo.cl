#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <time.h>

#include "trans.h"
#include "trans_at.h"
#include "util.h"
#include <ctemplates.h>
#include "validators.h"
#include <bconf.h>
#include <event.h>

#define GET_REGION_DONE  0
#define GET_REGION_ERROR 1
#define GET_REGION_REQUESTDATA  2
#define GET_REGION_REQUESTDATA_DONE  3

extern struct bconf_node* bconf_root;


struct get_region_state {
	int state;

	struct cmd *cs;
};


static struct c_param parameters[] = {
	{"longitude",		P_REQUIRED,	"v_coordinate"},
	{"latitude",		P_REQUIRED,	"v_coordinate"},
	{NULL, 0}
};

static const char *get_region_fsm(void *, struct sql_worker *, const char *);

/*****************************************************************************
 * get_region_done
 * Exit function.
 **********/
static void
get_region_done(void *v) {
	struct get_region_state *state = v;
	struct cmd *cs = state->cs;

	free(state);
	cmd_done(cs);
}

/*****************************************************************************
 * bind_insert
 * Initialise template API before a call to sql_worker_template_query().
 **********/
static void
bind_insert(struct bpapi *ba, struct get_region_state *state) {
	struct c_param *param;

	for (param = parameters; param->name; param++) {
		if (cmd_haskey(state->cs, param->name)) {
			bpapi_insert(ba, param->name, cmd_getval(state->cs, param->name));
		}
	}

	if (cmd_haskey(state->cs, "token"))
		bpapi_insert(ba, "token", cmd_getval(state->cs, "token"));
}

/*****************************************************************************
 * get_region_fsm
 * State machine
 **********/
static const char *
get_region_fsm(void *v, struct sql_worker *worker, const char *errstr) {
	struct get_region_state *state = v;
	struct cmd *cs = state->cs;
        struct bpapi ba;

	/*
	 * General error handling for db errors.
	 */
	if (errstr) {
                transaction_logprintf(cs->ts, D_ERROR, "get_region_fsm statement failed (%d, %s)", 
                                      state->state,
                                      errstr);
                cs->status = "TRANS_DATABASE_ERROR";
                cs->message = xstrdup(errstr);
		state->state = GET_REGION_ERROR;
	}

	while (1) {
		transaction_logprintf(cs->ts, D_DEBUG, "get_region_fsm(%p, %p, %p), state=%d",
				      v, worker, errstr, state->state);

		switch (state->state) {
		case GET_REGION_REQUESTDATA:
			{
				state->state = GET_REGION_REQUESTDATA_DONE;

				BPAPI_INIT(&ba, NULL, pgsql);
				bind_insert(&ba, state);

				sql_worker_template_query(&config.pg_slave,
								//"apisql/get_region_from_coordinates.sql", &ba,
								"sql/get_region_from_coordinates.sql", &ba,
								get_region_fsm, state, cs->ts->log_string);

				bpapi_output_free(&ba);
				bpapi_simplevars_free(&ba);
				bpapi_vtree_free(&ba);
 				return NULL;
			}
		case GET_REGION_REQUESTDATA_DONE:
			if (worker->sw_state == RESULT_DONE) {
				state->state = GET_REGION_DONE;
				continue;
			}

			while (worker->next_row(worker)) {
				transaction_printf(cs->ts, "name:%s\n", worker->get_value_byname(worker, "name"));
				transaction_printf(cs->ts, "region:%s\n", worker->get_value_byname(worker, "region"));
			}

			get_region_done(state);
			return NULL;

		case GET_REGION_DONE:
			get_region_done(state);
		case GET_REGION_ERROR:
			transaction_logprintf(cs->ts, D_ERROR, "Error state");
			sql_worker_set_wflags(worker, 0);
			get_region_done(state);
			return NULL;

		default:
			/* MAJOR STATE ERROR */
			cs->status = "TRANS_ERROR";
			transaction_logprintf(cs->ts, D_ERROR, "Unknown state (%d)",
					      state->state);
			return NULL;

		}
	}
}

/*****************************************************************************
 * getregion
 * Main function that initialise a state-structure and calls the state machine.
 **********/
static void
getregion(struct cmd *cs) {
	struct get_region_state *state;
	state = zmalloc(sizeof(*state));
	state->cs = cs;
	state->state = GET_REGION_REQUESTDATA;

	get_region_fsm(state, NULL, NULL);
}

ADD_COMMAND(getregion,     /* Name of command */
		NULL,
            getregion,     /* Main function name */
            parameters,  /* Parameters struct */
            "Get the region from the gps coordinates");/* Command description */

