How to configure a new key
--------------------------

Generate a API key like:

    $ echo "some random text" | md5sum
    > 7770d0bd5e0ff8b9a9368410ab56480c  -

Decide what regions the key will have access to insert ads, in this case, every region:

	> 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15

Determine what type of ad the key will be able to insert (sell, rent, looking for rent, buy):

	> s or h,k,s,u

Request limit is not being used, so dont worry

Settle which applications the key can use. The whole list of apps can be seen in the code, but the apps that we use for partners that import cars are:

	> import,importdeletead,newad,cars_data

Establish which categories the key will have access to, in this case:

	> 2020

And, in the majority of the cases, we want logging for our partners, so their requests are kept in a log file:

	> logging 1

In conclusion, a key, and how to set it, should be like this:

	> printf "HMSET mcp1xapp_id_amotor_cl api_key 7770d0bd5e0ff8b9a9368410ab56480c region_limit 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15 ad_type_limit s request_limit 100000 appl_limit import,importdeletead,newad,cars_data cat_limit 2020 logging 1" | redis-cli -p port -h host
