Mobile API for sites based on Blocket-Platform
==============================================

Requirements
------------

The Mobile API makes an extensive use of features introduced in Platform V2, which makes
it a hard requirement.

Installation Instructions
-------------------------

This file describes the steps needed to get the mobile-API to work
into your regress environment.

### Preliminary steps

1. Create a fresh branch of your stable tree. You know how to do that
   This branch will need to be based on platform V2, and it should
   contain all the deff*_overridable template builtin commands.

2. Get a fresh copy of the _stable_ Mobile API module.
    git clone gitolite@coordination.blocket.com:mobile-api.git
   or ask somebody who have access to that repository to clone it for you

3. Since the Git Repository is hosted outside of your source tree, and since you may not be
   running Git as your SCM, the suggested way of integrating in a "consistent" way is to
   create a script and call this script during your environment "setup" procedure.
   Remember that the mainstream Git repository is Read-Only for everyone outside the Coordination
   Team. Since it's Git, you can set up a local repository where you'll have RW access.
   Beware that it'll be your sole reposibility to get any local modification approved upstream.
   One possible setup would be to create a directory at the top level of your source tree, which
   will hold any "external module"
    cd ${SRCDIR}
    mkdir ext-modules
   Then make the script run a Git clone of the Mobile API repository under the ext-modules directory.
   You'll end up having something like this
    ${TOPDIR}
       |
	   -> conf
	   |
	   -> ...
	   |
	   -> ext-modules
             |
 			-> mobile-api
             |
       -> ...
   Hint: You may have a look at the sample setup script which comes in the "samples" directory of the API module

4. Create a symbolic link in your toplevel directory, pointing to the mobile's API directory. If you've been using
   the layout proposed in the previous step, you should issue something like this
    cd ${SRCDIR}
    ln -s ext-modules/mobile-api mobile
   You can use the name you prefer for the symbolic link, you'll just have to set it correctly in the file we're about
   to create in the next step.

5. Create a mk file that will be included where needed.
   You can automate the creation of this file in the "setup" script described above.
   Create it as _mk/modules_paths.mk_, and fill it with the following content :
    # MOBILEDIR should point to the symbolic link you created in your TOPDIR,
    # realpath will take care of resolving it to the real absolute path
    MOBILEDIR?=$(realpath ${TOPDIR}/mobile)

    include $(MOBILEDIR)/mk/commonsrc.mk

6. Include this new file in your mk/all.mk. Simply add this line to your mk/all.mk file
    include ${TOPDIR}/mk/modules_paths.mk

7. Edit your _mk/commonsrc.mk_ file, add in the following lines:
    # Include modules_paths
    ifdef USE_MOBILE_TEMPLATE_API
    include ${TOPDIR}/mk/modules_paths.mk
    endif

8. Now, you usually need to link the mobile API in the following modules: mod_templates, mod_list, php_templates
   The easiest way of doing it, is to define the variable USE_MOBILE_TEMPLATE_API in the relevant Makefiles.
   So, add the following line
    # This is needed for the Mobile API
    USE_MOBILE_TEMPLATE_API=yes                                                                                                                                                                       
    # End API requirements           
   to the files
    ${SRCDIR}/modules/mod_templates/Makefile
    ${SRCDIR}/modules/mod_list/local.mk
    ${SRCDIR}/modules/php_templates/local.mk
    ${SRCDIR}/modules/php_templates/local-sql.mk

9. After all these steps you should be able to build (as in compile) your site, which will include the Mobile API templates.
   **Run a build, check that everything is fine and report issues**

### Basic Customizations

1. Toplevel Makefile
  You'll have to add the mobile subdir to the list of directories that make will go through when when recursively
  running targets - such as regress-install - for instance.
  If you run a build and you end up missing bconf.txt.api in regress_final/conf, this usually means that you've
  skipped this step. Just add the following line to your top-level Makefile
    SUBDIR+=mobile
  As usual, replace "mobile" with whatever name you chose for the top-level symlink

2. Redis regress configuration
   Make sure you have the variable REGRESS_REDIS_PORT in your defvars.mk
   That's usually the case for most sites, since you're already running a redis
   server in your regress environment.
   If that's not the case you'll have to
   - Define the REGRESS_REDIR_PORT variable in defvars.mk
   - Add the necessary makefile targets for starting / stopping redis in your
     regress (that usually implies playing with the REGRESS_DEPEND variable
     and somesuch)
   - restart your regress and make sure that you can connect to the running redis
     server
   The API provides a bconf file, which will be installed as bconf.txt.api.redis (it's included in the main
   API's bconf file, you don't need to do that explicitely).
   This file is usable as is in your regress environment. For your production environment, dont forget to add
   the entries overriding IP/PORT values for the redis node. Depending on the way you do that it will be either
   in bconf.txt.local or in bconf.txt.site.
   The API defines a redis node, called "redm". If you want it to use an already existing redis node in your
   environment, you should override the following key in your local bconf:
    *.*.common.redis.api_master_node=redm
   replacing "redm" with the redis node of your choice (most of you will set it to mc1 most likely).
   You should also initialize redis data as part of the rd target, when setting up your regress environment.
   To do that, you should create a makefile target, usually under regressinit.mk, that looks as follows:
    .PHONY: add-api-key
    REGRESS_REDIS_CLIENT=regress_final/bin/redis-cli
    add-api-key:
        printf "HMSET redmxapp_id_blocket_mobile api_key 746e1a72b4172e50d9c6b4f981312a1803283a74 region_limit 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23 ad_type_limit b,h,k,s,u appl_limit import,importdeletead,linkshelf,list,view,areas,categories,category_settings,category_params,sendmail,settings,related_ads,newad,sendtip,deletead,findstores,account_login,account_create,account_modify,account_change_password,account_lost_password,account_get_unpublished_ads,account_get_published_ads,account_adwatches,account_get_adwatch_ads,account_info,account_adwatch_create,account_adwatch_delete,account_save_ad,account_delete_save_ad,account_get_saved_ads,account_adwatch_email_start,account_adwatch_email_stop,list_latest,payment_status,payment_choice,account_create_jobb,account_jobb_list cat_limit 1000,1020,1040,1041,1042,1043,1044,1045,1046,1047,1060,1061,1062,1063,1064,1065,1066,1067,1080,1081,1082,1083,1084,1085,1100,1101,1102,1103,1120,1121,1122,1123,1140,1141,1142,1143,1147,1144,1145,1146,1148,1160,1161,1162,1163,1164,1220,1223,1221,1224,1240,1226,1222,1225,1180,1200,2000,2020,2021,2022,2023,2024,2028,2025,2027,2040,2041,2042,2043,2046,2045,2044,2047,2048,2049,2050,2060,2061,2062,2063,2064,2065,2066,2026,3000,3020,3040,3060,3080,3100,4000,4080,4060,4061,4062,4063,4064,4020,4040,4041,4042,4043,4044,4045,5000,5020,5021,5022,5023,5024,5025,5026,5040,5041,5042,5043,5044,5045,5046,5047,5060,6000,6020,6021,6022,6023,6025,6026,6024,6031,6032,6033,6040,6060,6061,6062,6063,6064,6065,6066,6067,6080,6081,6082,6089,6083,6084,6085,6086,6090,6087,6088,6100,6101,6102,6103,6104,6105,6106,6107,6120,6121,6122,6123,6124,6125,6126,6140,6141,6142,6143,6160,6161,6162,6163,6164,6165,6166,6167,6180,6181,6182,6183,6184,6185,6186,6187,7000,7020,7021,7022,7023,7024,7025,7040,7041,7042,7043,7044,7045,7046,7047,7060,7061,7062,7063,7064,7066,7080,7081,7082,7083,7084,7085,7086,7087,7088,7089,7090,7091,7092,7093,7100,7104,7108,7112,7116,7120,7124,7128,7132,7136,7140,7144,7148,7152,7156,7160,7164,7168,7172,7176,7180,8000,8020,8040,9000,9010,9020,9030,9040,9050,9060,9070,9080,9090,9100,9110,9120,9130" | ${REGRESS_REDIS_CLIENT} -p ${REGRESS_REDIS_PORT}
   Adapt it to match your categories, api_key, and so forth. You can add request_limit 100000 if needed.
   You'll then need to add this new target (add-api-key) to your regress-depend chain (usually by adding this target in the REGRESS_DEPEND variable, in regress/final/Makefile)

3. Include bconf
  The Mobile API comes with its very own bconf "data". It's splitted into multiple files for mantaining convenience, but
  all the sub-files are included in a main wrapper. Only this file will need to be included in your site, and it'll take
  care of bringing in all the relevant bits
  To do so, just add the following line(s) to your bconf.txt file (before bconf.txt.local unless you want to go nuts)
    ### API
    include bconf.txt.api
    # Include the site specific file AFTER the main
    # one, to allow key overrides
    # include bconf.txt.api.site
   We'll see about the bconf.txt.api.site later, just ignore it for the moment.

4. Include mod_secutiry conf
   There is a mod_security file provided with the API. you should include it from your httpd.conf file.
   If you're going to require extra configuration for mod_security, you should create a file on your
   local tree, say mod_sec_api_site.conf, and then include it as well in httpd.conf, as follows:
    # API
   Include "%DESTDIR%/conf/m?d_sec_api.conf"
   Include "%DESTDIR%/conf/m?d_sec_api_site.conf"


5. Add some basic show-template tests
   At this stage, you should really really really add in some show_template tests, to verify that every
   bit of the API is working correctly on your site.
   you'll need to test at least the following templates :
    areas.json (The region listing with sub areas)
    list.json (the "listing" method)
    view.json ("adview" method)
    sendmail.json ("adreply" method)
    sendtip.json ("sendtip" method)

   The API itseld doesn't come with regress tests, because those require a data model and a data set to be
   really meaningful, that's why adding tests for the API is left as a site-task... you don't want to
   test that you have all the "swedish" regions in your areas.json output, don't you?

   It's important that you write those tests as soon as possible, because they'll tell you
   - wether the API is actually giving real output or not
   - wether your REDIS is correctly set up or not
   - wether the standard API behavior suits your site behavior or not. In the latter case the list
     of missing / uncorrect behavior will provide the building blocks for the next step


6. Create your own bconf file
   If you need to use custom templates and define different bconf key for the API (think for instance
   about the _keys_ given to different partners), you'll have to create your local bconf.txt.api file.
   To do that, create a new bconf file in your source tree and call it bconf.txt.api.site
   Uncomment the line we saw ealier, mentioning the bconf.txt.api.site file
    include bconf.txt.api.site
   And then make sure you install this new file in your regress, adding it to the appropriate Makefile

7. Create your onw filters

8. Create your own templates
   Eg. Myaccount (blocket)

9. Add tests
   It goes without saying. Add tests for any filter / template you implement that overrides the standard API
   behavior. You'll also want to add tests on a higher layer, once you'll start using the API from clients
   applications (mobile site, mobile device applications, partners?)

10. Add files to blocke.spec
   You'll need to add the following files to your blocket.spec file
    # Bconf
    bconf.txt.api
    bconf.txt.api.templates
    bconf.txt.api.custom_templates
    bconf.txt.api.site
    
    # Mod Security
    mod_sec_api.conf
    api.conf
    mod_sec_api_site.conf

11. Various
  If may want to have "make rb" take care of the conf under the API directory (usually ext-modules/mobile-api/conf), you should
  edit your toplevel Makefile, and add the following line to the "rb" target:
    make -C ${MOBILEDIR}/conf install
  Before trans reload / apache reload and so on

/* End */
vim: expandtab tabstop=4 shiftwidth=4 softtabstop=4
