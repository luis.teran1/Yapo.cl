#!/bin/sh

if [ -n "$DOCKER_IMAGE$USE_DOCKER" ] && [ -z "$NO_DOCKER" ] && type -p docker; then
	echo "Calling docker.sh"
	exec bash docker.sh "$@"
fi

if [ -f /etc/redhat-release ] && [ -z "$NO_SCL" ] && [ -d /opt/rh/devtoolset-2 ] && ! scl_enabled devtoolset-2; then
	exec scl enable devtoolset-2 "$0 $*"
fi

BUILDVERSION=`./scripts/build/buildversion.sh`
if [ $? != 0 ]; then
	echo "Couldn't determine build version."
	exit 1
fi
BUILDPATH=${BUILDPATH:-build}
export BUILDPATH
BUILDBUILD="./scripts/build/build-build.pl --without-flavor=gcov --quiet"
if [ -n "$FLAVOR" ] ; then
	BUILDBUILD="./scripts/build/build-build.pl --with-flavor=$FLAVOR"
	# hack
	rm -f $BUILDPATH/build.ninja
else
	FLAVOR=regress
fi

if [ ! -f $BUILDPATH/build.ninja ] ; then
	$BUILDBUILD || exit 1
fi

if [ "$BUILDVERSION" != "`cat $BUILDPATH/version 2>/dev/null`" ] ; then
	echo $BUILDVERSION > $BUILDPATH/version
fi

ninja -f $BUILDPATH/build.ninja $*
s=$?

if [ $s -eq 1 ]; then
	ninja -n -f $BUILDPATH/build.ninja $* >/dev/null 2>&1
	s=$?
	if [ $s = 1 ] ; then
		echo "Probable mismatch between ninja files and build env. Forcing rebuild of ninja files and retrying."
		$BUILDBUILD || exit 1
		ninja -f $BUILDPATH/build.ninja $*
		s=$?
	else
		s=1
	fi
fi

if [ $s != "0" ] ; then
	exit 1
fi

if [ -n "$RUNTESTS" ] ; then
	REGRESS_RUNNER=$BUILDPATH/$FLAVOR/bin/regress-runner
	if [ -e $REGRESS_RUNNER ] ; then
		mkdir -p $BUILDPATH/$FLAVOR/tests
		$REGRESS_RUNNER --outdir $BUILDPATH/$FLAVOR/tests --make-args "FLAVOR=$FLAVOR" $REGRESS_RUNNER_FLAGS || exit $?
	else
		echo "No regress-runner available, tried $REGRESS_RUNNER."
		exit 2
	fi
fi

if [ -n "$RUNGCOVR" ] ; then
	GCOVR=`which gcovr`
	if [ -e $GCOVR ] ; then
		echo "Running $GCOVR..."
		$GCOVR -v --root . --xml --output=$BUILDPATH/$FLAVOR/coverage.xml --exclude='^(/usr/|.*gperf|.*\.(sql|tmpl|txt|html|mime|xml|js|fun)\.c)' --object-directory=$BUILDPATH/$FLAVOR || exit $?
	else
		echo "No gcovr available, tried $GCOVR."
		exit 3
	fi
fi

