/*
	Functions for working against a serialized bursttrie.

	You're expected to either read the file into memory or mmap it,
	and provide its location as the 'map' argument. 

*/
#include "bursttrie_common.h"

#ifdef __cplusplus
extern "C" {
#endif

/*
	Callback type used by brt_ser_prefix()

	The first two arguments together (this is an optimization) make up the full key, but the
	second can be NULL. 'data' is a pointer to any user-data associated with the key.

	If the return-value is a larger than zero, 'limit' will be raised to this value if lower.
*/
typedef brt_count(*brt_ser_prefix_cb)(char*, char*, brt_count count, void* data, void* cbdata);

/*
	Callback type used by brt_ser_levenshtein()

	'data' is a pointer to any user-data associated with the key.

	NOT YET IMPLEMENTED: If the return-value is a larger than zero, 'limit' will be raised to this value if lower.
*/
typedef brt_count(*brt_ser_search_cb)(const char*, brt_count count, int dist, void* data, void* cbdata);

/*
	Look up a key. Example:
		brt_count c = brt_ser_find(map, header->root_offset, key, NULL));

	Returns 0 if key doesn't exist.
*/
brt_count brt_ser_find(unsigned char* map, int levelofs, const char* key, void** data);

/*
	Prefix search. Example:
		int ret = brt_ser_prefix(map, header->root_offset, prefix, 2, print_prefix_cb, NULL);

	The print_prefix_cb will fire for every key that has 'prefix' as a prefix, and occurs more than the limit (2).
*/
int brt_ser_prefix(unsigned char* map, int levelofs, const char* key, brt_count limit, brt_ser_prefix_cb onhit, void* cbdata);

/*
	Levenshtein search. Example:
		brt_ser_levenshtein(map, "volvo", 2, 0, USE_RESTRICTED_EDIT_DISTANCE, print_candidate, NULL);

*/
void brt_ser_levenshtein(unsigned char* map, const char* word, int max_k, int cutoff, enum ser_search_flags flags, brt_ser_search_cb cb, void* cbdata);

#ifdef __cplusplus
}
#endif
