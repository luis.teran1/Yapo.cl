#ifndef BURSTTRIE_HEADER_COMMON
#define BURSTTRIE_HEADER_COMMON

#include <stdint.h>

#define BRT_SERIALIZED_ID0 { 'B','R','S','T','R','I','E','0' }
#define BRT_MAX_KEY_LENGTH 256

/* These... are not good, except to verify things no breaking when changed. */
#define BST_FULL_SYMLO 0
#define BST_FULL_SYMHI 255

enum bnt_type {
	BNT_BST,
	BNT_FULL,
};

enum ser_flags {
	BRT_SER_USER_DATA = 1,
	BRT_SER_UTF8 = 2,
};

enum ser_search_flags {
	LEVSOPT_OPPORTUNISTIC_CUTOFF = 1,
	USE_RESTRICTED_EDIT_DISTANCE = 2
};

typedef uint32_t brt_count;

struct ser_level_header {
	uint16_t	type;
	uint16_t	nsyms;
} __attribute__((packed));

struct ser_level_entry {
	uint32_t	offset;
	uint32_t	term_count;
	uint32_t	subtree_max_count;
} __attribute__((packed));

struct ser_bst_cntlen {
	uint32_t	count;
	uint16_t	offset;
} __attribute__((packed));

struct ser_user_entry {
	uint32_t	data;
} __attribute__((packed));

/* WARNING: Changes to these ser_* structures can modify the serialized representation */
struct ser_header {
	char		id[8];
	uint32_t	tree_size;
	uint32_t	num_keys;
	uint32_t	max_key_len;
	uint32_t	max_level;
	uint32_t	flags;
	struct ser_level_entry	root;
	struct ser_user_entry	root_data;
} __attribute__((packed));

#endif

