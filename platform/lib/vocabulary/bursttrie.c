/*
	Bursttrie

	NOTES
		Replacing cntstr+cbst with wordcnt+avl caused a sizable loss in speed and increase in memory use.

	TODO
		bitindex nodes were never implemented.
	 	brt_serialization_visitor() should probably be split into two separate functions (it's too long)
		should have a 'conf struct' that must be passed to some functions. Make "BST_BURST_LIMIT" dynamic.
		should be possible to serialize to memory
		brt_free should pass a queue into level-free functions where they populate it with brt-ptrs to be free'd (non-recursively)
		brt_insert_file() shouldn't be here
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <stdint.h>

#include "avl.h"

#include "bursttrie.h"
#include "macros.h"
#include "util.h"

static const int BST_BURST_LIMIT = 32;

enum brt_search_type {
	BRT_WORD,
	BRT_PREFIX
};

struct brt_val {
	brt_count	count;
	void*		data;
};

static inline size_t padto8(size_t a) {
	return (a+7)-((a+7) & 7);
}

static int padfile8(FILE* f, size_t written) {
	size_t a = padto8(written) - written;
	if (a) {
		static char padding[8] = { 0,0,0,0,0,0,0,0 };
		fwrite(padding, a, 1, f);
	}
	return a;
}

struct wordcnt {
	struct avl_node tree;
	char* str;
	struct brt_val	val;
};

static struct wordcnt* new_wordcnt(const char* s) {
	if (!s)
		return NULL;

	struct wordcnt* wc = malloc(sizeof(struct wordcnt));
	if (wc) {
		wc->val.count = 0;
		wc->val.data = NULL;
		wc->str = __builtin_strdup(s);
	}
	return wc;
}


static inline int
wordcnt_compare(const struct wordcnt *a, const struct wordcnt *b) {
	return strcmp(a->str, b->str);
}

static int
wordcnt_avlcmp(const struct avl_node *an, const struct avl_node *bn) {
	const struct wordcnt *a = avl_data(an, const struct wordcnt, tree);
	const struct wordcnt *b = avl_data(bn, const struct wordcnt, tree);

	return wordcnt_compare(a,b);
}

const char* bnt_type_name[] = {
	"BNT_BST",
	"BNT_FULL",
};

struct brt_header {
	enum bnt_type type;
	struct brt_val val;
};

struct brt {
	struct brt_header header;
};

struct brt_bst {
	struct brt_header header;
	struct avl_node* bst;
	int count;				// num entries in bst
};


struct brt_full {
	struct brt_header header;
	struct brt* nodes[BST_FULL_SYMHI-BST_FULL_SYMLO+1]; // Don't need 0-31
};

static void brt_bst_debug(struct brt* bt);
static struct brt* brt_bst_create();
static void brt_bst_free(struct brt** bt, void(*userfree)(void*));

void* brt_serialization_visitor(void* data, void* cbdata, void* subtree_results, int num_subtree_results, struct brt_postwalk_stats* stats);

inline static void brt_inc_terminal_count(struct brt* bt, brt_count value) {
	bt->header.val.count += value;
}

inline static struct brt_val* brt_get_terminal_val(struct brt* bt) {
	return &(bt->header.val);
}

inline static void brt_set_terminal_val(struct brt* bt, struct brt_val* val)  {
	bt->header.val = *val;
}


//
// FULL NODE FUNCTIONS
//

static void brt_full_debug(struct brt* bt) {
	struct brt_full* l_bt = (struct brt_full*)bt;
	printf("Debug %s node at %p (term_count is %u, data at %p):\n", bnt_type_name[l_bt->header.type], (void*)l_bt, l_bt->header.val.count, (void*)l_bt->header.val.data);
	for (int i=0 ; i < (int)(sizeof(l_bt->nodes)/sizeof(l_bt->nodes[0])) ; ++i) {
		if (l_bt->nodes[i] != NULL) {
			printf("%d ['%c']: at %p -> %s at %p\n", i, (char)BST_FULL_SYMLO+i, (void*)&l_bt->nodes[i], bnt_type_name[ l_bt->nodes[i]->header.type ], (void*)l_bt->nodes[i]);
		}
	}
	for (int i=0 ; i < (int)(sizeof(l_bt->nodes)/sizeof(l_bt->nodes[0])) ; ++i) {
		if (l_bt->nodes[i] != NULL) {
		   	if (l_bt->nodes[i]->header.type == BNT_BST) {
				printf("Contents of BST member %d structure:\n", i);
				brt_bst_debug(l_bt->nodes[i]);
			} else {
				printf("\n-------------------------------------------------\n");
				printf("Contents of FULL member %d structure:\n", i);
				brt_full_debug(l_bt->nodes[i]);
				printf("-------------------------------------------------\n\n");
			}
		}
	}
}

static struct brt* brt_full_create(void) {
	struct brt_full* n = calloc(1, sizeof(struct brt_full));
	n->header.type = BNT_FULL;
	return (struct brt*)n;
}

static void brt_full_free(struct brt** bt, void(*userfree)(void*)) {
	if (!bt || !*bt)
		return;

	struct brt_full* l_bt = (struct brt_full*)*bt;

	for (int i=0 ; i < (int)(sizeof(l_bt->nodes)/sizeof(l_bt->nodes[0])) ; ++i) {
		if (l_bt->nodes[i] != NULL) {
			assert((l_bt->nodes[i]->header.type == BNT_BST) || (l_bt->nodes[i]->header.type == BNT_FULL));
			if (l_bt->nodes[i]->header.type == BNT_BST) {
				brt_bst_free(&(l_bt->nodes[i]), userfree);
			} else if (l_bt->nodes[i]->header.type == BNT_FULL) {
				brt_full_free(&(l_bt->nodes[i]), userfree);
			}
		}
	}
	if (userfree && l_bt->header.val.data)
		userfree(l_bt->header.val.data);
	free(l_bt);
}

static inline struct brt** brt_full_get_arc_location(struct brt_full* bt, unsigned char symbol) {
	int sym = symbol-BST_FULL_SYMLO;
	// TODO: turn into assert
	if (sym > BST_FULL_SYMLO + BST_FULL_SYMHI + 1 || sym < 0) {
		fprintf(stderr, "get arc: Cannot follow symbol '%c/%d' (arc %d), it's out of range of [%d..%d]\n", symbol, (int)symbol, sym, BST_FULL_SYMLO, BST_FULL_SYMHI);
		abort();
	}
	return &(bt->nodes[sym]);
}

/*
	Given a node (level) and a key, return a pointer to the pointer to the next level,
	and populate 'entry' if this is a terminal state.
*/
static struct brt** brt_full_transition(struct brt** bt, const char** key, struct brt_val** entry, int* done) {

	struct brt_full* l_bt = (struct brt_full*)*bt;
	struct brt** next_level = brt_full_get_arc_location(l_bt, *(*key));

	if (next_level && *next_level) {
		// If we're at the last symbol of our input, check if we're at a terminating state.
	 	++(*key);
		if (*(*key) == '\0') {
			*done = 1;
			if (entry)
				*entry = brt_get_terminal_val(*next_level);
		}
	}
	return next_level;
}

/*
	To insert something into a full-node means to create the outgoing pointer
*/
static struct brt** brt_full_insert(struct brt** bt, const char** key) {
	assert(bt && *bt);

	struct brt_full* l_bt = (struct brt_full*)*bt;
	struct brt** next_level = brt_full_get_arc_location(l_bt, *(*key));

	if (!next_level)
		return NULL;

	if (!*next_level)
		*next_level = brt_bst_create();

	++(*key); // consume symbol.

	return next_level;
}

//
// BST NODE FUNCTIONS
//
static void brt_bst_debug(struct brt* bt) {
	struct brt_bst* l_bt = (struct brt_bst*)bt;
	printf("Debug %s node at %p (count is %d, term_count is %u, user-data at %p):\n", bnt_type_name[l_bt->header.type], (void*)l_bt, l_bt->count, l_bt->header.val.count, l_bt->header.val.data);

	struct avl_node* n;
	struct avl_it it;

	avl_it_init(&it, l_bt->bst, NULL, NULL, wordcnt_avlcmp);
	while ((n = avl_it_next(&it)) != NULL) {
		struct wordcnt *e = avl_data(n, struct wordcnt, tree);
		printf("%s:%u (data at %p)\n", e->str, e->val.count, (void*)e->val.data);
	}
}

static struct brt* brt_bst_create(void) {
	struct brt_bst* bst = xmalloc(sizeof(struct brt_bst));
	bst->header.type = BNT_BST;
	bst->header.val.count = 0;
	bst->header.val.data = NULL;
	bst->count = 0;
	bst->bst = NULL;
	return (struct brt*)bst;
}

static void internal_avl_free(struct avl_node *n, void(*userfree)(void*)) {
	if (!n)
		return;
	// Our trees are small, so these recursive calls should be ok.
	internal_avl_free(n->link[0], userfree);
	internal_avl_free(n->link[1], userfree);
	struct wordcnt* e = avl_data(n, struct wordcnt, tree);
	if (e) {
		free(e->str);
		if (userfree && e->val.data)
			userfree(e->val.data);
		free(e);
	}
}

static void brt_bst_free(struct brt** bt, void(*userfree)(void*)) {
	if (!bt || !*bt)
		return;

	struct brt_bst* l_bt = (struct brt_bst*)*bt;
	internal_avl_free(l_bt->bst, userfree);
	if (userfree && l_bt->header.val.data)
		userfree(l_bt->header.val.data);
	free(l_bt);
	*bt = NULL;
}

static void* brt_bst_lookup_entry(struct brt_bst* level, const char* s) {
	struct wordcnt* e = NULL;
	struct wordcnt fi;
	fi.str = (char*)s;

	struct avl_node* n = avl_lookup(&fi.tree, &(level->bst), wordcnt_avlcmp);
	if (n)
		e = avl_data(n, struct wordcnt, tree);
	return e;
}

static void* brt_bst_insert_new_entry(struct brt_bst* level, const char* s) {
	struct wordcnt* e = new_wordcnt(s);
	if (e) {
		avl_insert(&e->tree, &(level->bst), wordcnt_avlcmp);
		++level->count;
	}
	return e;
}

static void* brt_bst_insert_entry(struct brt_bst* level, const char* s) {
	struct wordcnt* e = brt_bst_lookup_entry(level, s);

	if (!e)
		e = brt_bst_insert_new_entry(level, s);

	return e;
}

static int brt_bst_burst(void* data, void* cbdata) {
	struct brt_full* bt = data;
	struct wordcnt* wc = cbdata;

	const char* keyleft = wc->str;

	struct brt** next_level = brt_full_get_arc_location(bt, *keyleft);
	if (!next_level)
		return -1;

	++keyleft; // consume symbol.

	if (!*next_level)
		*next_level = brt_bst_create();

	if (!*keyleft) {
		brt_set_terminal_val((struct brt*)*next_level, &(wc->val));
		return 0;
	}

	struct wordcnt* new_e = brt_bst_insert_new_entry((struct brt_bst*)*next_level, keyleft);
	new_e->val = wc->val;

	return 0;
}

static struct brt** brt_bst_insert(struct brt** bt, const char** key, struct brt_val** entry) {
	assert(*bt);

	struct brt_bst** l_bt = (struct brt_bst**)bt;

	if ((*l_bt)->count >= BST_BURST_LIMIT) {
		// Create a new full level
		struct brt* bf = brt_full_create();

		// For each member of the bst, insert them into the new level.
		struct avl_node* n;
		struct avl_it it;
		avl_it_init(&it, (*l_bt)->bst, NULL, NULL, wordcnt_avlcmp);
		while ((n = avl_it_next(&it)) != NULL) {
			struct wordcnt* e = avl_data(n, struct wordcnt, tree);
			if (brt_bst_burst(bf, e) != 0) {
				fprintf(stderr, "Error bursting, aborting. This is bad, no error handling for this");
				abort();
			}
		}

		// Copy our terminal value
		brt_set_terminal_val(bf, &(((struct brt_bst*)*l_bt)->header.val));

		// Free the current level, but don't free userdata.
		brt_bst_free((struct brt**)l_bt, NULL);

		// Link in the new level in its place
		*bt = bf;

		// TODO: We could probably optimize here by directly inserting into the right node without re-searching.
		return bt; // Force retry of word which resulted in burst.
	}

	struct wordcnt* wc = brt_bst_insert_entry(*l_bt, *key);
	// We're not increasing the count here, that's handled at a higher level.
	*entry = &(wc->val);
	*key = NULL; // We've consumed the whole key
	return bt;
}

static struct brt** brt_bst_search(struct brt** bt, const char** key, struct brt_val** entry, int* done) {

	struct brt_bst* l_bt = (struct brt_bst*)(*bt);
	// hack: generalization is a todo (need struct with config (comparator, etc) passed in
	struct wordcnt* wc;
	if ((wc = brt_bst_lookup_entry(l_bt, *key)) != NULL) {
		if (entry)
			*entry = &(wc->val);
		*key = NULL; // We've consumed the whole key
	}
	*done = 1;
	return bt;
}

//
// BURST TREE FUNCTIONS
//

static struct brt** brt_search(struct brt** bt, const char** key, struct brt_val** entry, enum brt_search_type stype) {

	int done = 0;
	while (!done && bt && *bt) {
		switch ((*bt)->header.type) {
			case BNT_BST:
				/* If landed in a BST node during prefix search, return it and let the caller handle further search */
				if (stype == BRT_PREFIX)
					return bt;
				bt = brt_bst_search(bt, key, entry, &done);
				break;

			case BNT_FULL:
				bt = brt_full_transition(bt, key, entry, &done);
				break;
		}
	}

	return bt;
}

brt_count brt_insert_count(struct brt** bt, const char* key, brt_count count, void** data) {
	struct brt_val* pentry = NULL;

	if (!bt)
		return 0;

	if (!*bt) {
		*bt = brt_full_create(); // Decides the type of the first level.
	}

	while (key && *key) {
		assert(*bt);

		assert(((*bt)->header.type == BNT_FULL) || ((*bt)->header.type == BNT_BST));

		if ((*bt)->header.type == BNT_FULL)
			bt = brt_full_insert(bt, &key);
		else
			bt = brt_bst_insert(bt, &key, &pentry);
	}

	assert(bt && *bt);

	// If not set, it means we ran out of key while on a FULL node
	if (!pentry) {
		brt_inc_terminal_count(*bt, count);
		pentry = brt_get_terminal_val(*bt);
	} else {
		pentry->count += count;
	}

	if (data)
		*data = &(pentry->data);

	return pentry->count;
}


brt_count brt_insert(struct brt** bt, const char* key, void** data) {
	return brt_insert_count(bt, key, 1, data);
}

brt_count brt_find(struct brt* bt, const char* key, void** data) {

	if (!bt)
		return 0;

	struct brt_val* val = NULL;

	struct brt* src = bt;
	const char* pkey = key;
	brt_search(&src, &pkey, &val, BRT_WORD);

	if (val) {
		if (data)
			*data = &(val->data);
		return val->count;
	}

	return 0;
}

brt_count brt_set_count(struct brt* bt, const char* key, brt_count count) {

	if (!bt)
		return 0;

	struct brt_val* val = NULL;

	struct brt* src = bt;
	const char* pkey = key;
	brt_search(&src, &pkey, &val, BRT_WORD);
	brt_count old_count = 0;

	if (val) {
		old_count = val->count;
		val->count = count;
	}

	return old_count;
}

void brt_free(struct brt** bt, void(*userfree)(void*)) {
	if (!bt || !*bt)
		return;

	assert(((*bt)->header.type == BNT_FULL) || ((*bt)->header.type == BNT_BST));

	if ((*bt)->header.type == BNT_FULL)
		brt_full_free(bt, userfree);
	else if ((*bt)->header.type == BNT_BST)
		brt_bst_free(bt, userfree);
}

void brt_debug(struct brt* bt) {
	if (!bt)
		return;

	assert((bt->header.type == BNT_FULL) || (bt->header.type == BNT_BST));

	if (bt->header.type == BNT_BST)
			brt_bst_debug(bt);
	else
			brt_full_debug(bt);
}

/* Shouldn't be here */
int brt_insert_file(struct brt** bt, const char* filename) {

	FILE* f = fopen(filename, "r");
	if (!f)
		return -1;

	char buf[BRT_MAX_KEY_LENGTH];
	char* p = NULL;
	int line = 0;
	int inserted = 0;

	while (!feof(f)) {
		UNUSED_RESULT(fgets(&buf[0], sizeof(buf), f));
		++line;
		if ((p = strrchr(buf, '\n')) != NULL)
			*p = '\0';
		if ((p = strrchr(buf, '\f')) != NULL)
			*p = '\0';
		if ((p = strrchr(buf, '\r')) != NULL)
			*p = '\0';
		if (*buf) {
			brt_insert(bt, &buf[0], NULL);
			++inserted;
		}
		buf[0] = '\0';
	}

	fclose(f);
	return inserted;
}

struct search {
	char buffer[BRT_MAX_KEY_LENGTH]; // Workbuffer
	char* start_pos; // pointer into buffer AFTER the current output prefix.
	const char* prefix; // search-prefix for BST-nodes
	unsigned int	min_count; // Only output entries with this many or more occurences.
	brt_prefix_cb   onhit;
	void*			cbdata;
};

/*
	todo: optimize this to return if we've matched anything 
	previously, but this invocation the prefix doesn't match.
	Since we walk INORDER this means no further matches are possible.
*/
static void brt_prefix_internal(struct brt* bt, struct search* s) {

	// If we have no rest-prefix and the current node has a termination count, then buffer is a key in the trie, so output it.
	if (!(*s->prefix)) {
		struct brt_val* val = brt_get_terminal_val(bt);
		if (val->count >= s->min_count)
			s->onhit(s->buffer, NULL, &val->count, val->data, s->cbdata);
	}

	// if FULL; for each node, call self with out-arc character added to buffer
	if (bt->header.type == BNT_FULL) {
		struct brt_full* btf = (struct brt_full*)bt;
		for (int i=0; i < (int)(sizeof(btf->nodes)/sizeof(btf->nodes[0])) ; ++i) {
			if (btf->nodes[i] != NULL) {
				*(s->start_pos++) = BST_FULL_SYMLO+i;
				brt_prefix_internal(btf->nodes[i], s);
				*(s->start_pos--) = '\0';
			}
		}
	} else if (bt->header.type == BNT_BST) {
		struct avl_node* n;
		struct avl_it it;
		avl_it_init(&it, ((struct brt_bst*)bt)->bst, NULL, NULL, wordcnt_avlcmp);
		while ((n = avl_it_next(&it)) != NULL) {
			struct wordcnt *e = avl_data(n, struct wordcnt, tree);
			if (e->val.count >= s->min_count)
				if ( (s->prefix == NULL || *(s->prefix) == '\0' ) || (strncmp(e->str, s->prefix, strlen(s->prefix)) == 0))
					s->onhit(s->buffer, e->str, &e->val.count, e->val.data, s->cbdata);
		}
	}
}

void brt_prefix(struct brt* bt, const char* prefix, int limit, brt_prefix_cb onhit, void* cbdata) {
	if (!bt)
		return;

	struct brt** scont = &bt;
	const char* pkey = prefix;
	if (*prefix)
		scont = brt_search(&bt, &pkey, NULL, BRT_PREFIX);

	if (scont && *scont) {
		struct search s = { {0}, NULL, NULL, limit, onhit, cbdata };
		assert(pkey && prefix);
		int plen = (int)(pkey-prefix);
		if (plen < 0 || plen > (int)(sizeof(s.buffer)-2))
			return;
		strncpy(&(s.buffer[0]), prefix, plen);
		s.prefix = pkey;
		s.start_pos = s.buffer + plen;
		brt_prefix_internal(*scont, &s);
	}
}

// TODO: rename and move.
struct ser_level {
	struct ser_level_entry entry;
	void* user_data;
};

static void* brt_postwalk_internal(struct brt* bt, brt_postwalk_visitor visit, void* cbdata, struct brt_postwalk_stats* stats, int level) {

	if (bt->header.type == BNT_BST) {
		if (stats)
			stats->curr_level = level;
		return visit(bt, cbdata, NULL, 0, stats);
	} else {
		struct brt_full* l_bt = (struct brt_full*)bt;
		struct ser_level** lvl = xmalloc(sizeof(struct ser_level*)*(BST_FULL_SYMHI-BST_FULL_SYMLO+1));

		for (int i=0 ; i < (int)(sizeof(l_bt->nodes)/sizeof(l_bt->nodes[0])) ; ++i) {
			if (l_bt->nodes[i] != NULL) {
				lvl[i] = brt_postwalk_internal(l_bt->nodes[i], visit, cbdata, stats, level + 1);
			} else {
				lvl[i] = NULL;
			}
		}
		if (stats)
			stats->curr_level = level;
		return visit(bt, cbdata, lvl, (BST_FULL_SYMHI-BST_FULL_SYMLO+1), stats);
	}

}

void* brt_postwalk(struct brt* bt, brt_postwalk_visitor visit, void* cbdata, struct brt_postwalk_stats* stats) {
	if (!bt || !visit)
		return NULL;

	return brt_postwalk_internal(bt, visit, cbdata, stats, 1);
}

struct brt_serialization_cbdata {
	FILE*   f;
	size_t	prev_offset;
	brt_count	limit;
	brt_userdata_visitor userdata_visitor;
	void* userdata_cbdata;
};


void* brt_serialization_visitor(void* data, void* cbdata, void* subtree_results, int num_subtree_results, struct brt_postwalk_stats* stats) {
	struct brt_serialization_cbdata* fl = (struct brt_serialization_cbdata*)cbdata;
	struct ser_level* retval = xmalloc(sizeof(struct ser_level));
	struct ser_level** results = (struct ser_level**)subtree_results;
	struct ser_level_header levhead;
	struct brt_val* val;
	size_t written = 0;
	int i = 0;
	int j = 0;

	memset(retval, 0, sizeof(struct ser_level_entry));

	if (((struct brt*)data)->header.type == BNT_BST) {
		/* Serialize a BST node:
			Write a count of the number of string (n)
			write n count+offset records
			Write n stringz
		*/
		struct brt_bst* l_bt = (struct brt_bst*)data;

		val = brt_get_terminal_val(data);
		retval->entry.term_count = val->count;
		retval->user_data = val->data;

		if (l_bt->count == 0) { // Empty BST, just a term_count
			goto leavefunc;
		}

		struct avl_node* n;
		struct avl_it it;

		struct wordcnt** entries = xmalloc(l_bt->count * sizeof(struct wordcnt*));
		struct ser_bst_cntlen* cle = xcalloc(l_bt->count + 1, sizeof(struct ser_bst_cntlen)); // Add one for sentinel
		struct ser_user_entry* userentry = NULL;

		if (fl->userdata_visitor)
			userentry = calloc(l_bt->count, sizeof(struct ser_user_entry));

		// Initialize ser_bst_cntlen's, and filter out entries below threshold.
		avl_it_init(&it, l_bt->bst, NULL, NULL, wordcnt_avlcmp);
		int stringz_size = 0;
		while ((n = avl_it_next(&it)) != NULL) {
			struct wordcnt* e = avl_data(n, struct wordcnt, tree);
			if (e->val.count >= fl->limit) {
				if (e->val.count > retval->entry.subtree_max_count)
					retval->entry.subtree_max_count = e->val.count;

				if (userentry) {
					userentry[i].data = fl->userdata_visitor(e->val.data, fl->userdata_cbdata);
				}

				cle[i].count = e->val.count;
				cle[i].offset = stringz_size;

				stringz_size += strlen(e->str) + 1;

				entries[i++] = e;
			}
		}
		levhead.nsyms = i;
		levhead.type = BNT_BST;

		if (levhead.nsyms < 1) { // No entries rose above the requested limit, so this subtree will be empty.
			free(cle);
			free(userentry);
			free(entries);
			goto leavefunc;
		}

		if (stats)
			stats->num_keys += levhead.nsyms;

		char* stringz = malloc(stringz_size);

		cle[levhead.nsyms].offset = stringz_size; // sentinel
		j = 0;
		for (i=0 ; i < levhead.nsyms ; ++i) {
			int str_len = cle[i+1].offset-cle[i].offset;

			// printf("putting '%s' at offset %d (len=%d, count=%d)\n", entries[i]->str, j, str_len, entries[i]->val.count);
			assert(j + str_len <= stringz_size);
			memcpy(stringz+j, entries[i]->str, str_len);
			if (stats && stats->curr_level+str_len-2 > stats->max_key_len) {
				// Reason for -2 is that we must disregard the current level, and the terminating zero in the string.
				stats->max_key_len = stats->curr_level+str_len-2;
			}
			j += str_len;
		}

		// Write header
		fwrite(&levhead, sizeof(struct ser_level_header), 1, fl->f);
		written = sizeof(struct ser_level_header);

		// Write userdata
		if (userentry) {
			fwrite(userentry, sizeof(struct ser_user_entry), levhead.nsyms, fl->f);
			written += sizeof(struct ser_user_entry)*levhead.nsyms;
		}

		// Write counts+lengths
		fwrite(cle, sizeof(struct ser_bst_cntlen), levhead.nsyms, fl->f);
		written += sizeof(struct ser_bst_cntlen)*levhead.nsyms;

		// Write string-data
		fwrite(stringz, stringz_size, 1, fl->f);
		written += sizeof(char)*stringz_size;

		free(cle);
		free(stringz);
		free(userentry);
		free(entries);

		written += padfile8(fl->f, written);

		size_t prev_offset = fl->prev_offset;
		fl->prev_offset += written;
		retval->entry.offset = prev_offset;

	} else {
		/* Serialize a FULL node: */
		val = brt_get_terminal_val(data);
		retval->entry.term_count = val->count;
		retval->user_data = val->data;
		// if has user-data, serialize data and setup retval with offset+len

		unsigned char syms[256];
		// Collect used symbols and pack results array to be contiguous.
		// We mustn't forget to free any nodes we step over or overwrite.
		for (i = 0 ; i < num_subtree_results ; ++i) {
			if (results[i]) {
				if ((results[i]->entry.subtree_max_count >= fl->limit) || (results[i]->entry.term_count >= fl->limit)) {
					syms[j] = i;
					if (j < i)
						results[j] = results[i];
					if (stats && results[i]->entry.term_count) {
						++stats->num_keys;
						if (stats->curr_level > stats->max_key_len)
							stats->max_key_len = stats->curr_level;
					}
					++j;
				} else {
					free(results[i]);
					results[i] = NULL;
				}
			}
		}
		if (j == 0) { // No subtrees to write.
			goto leavefunc;
		}

		levhead.type = BNT_FULL;
		levhead.nsyms = j;
		fwrite(&levhead, sizeof(struct ser_level_header), 1, fl->f);
		written = sizeof(struct ser_level_header);

		fwrite(&syms, sizeof(char), levhead.nsyms, fl->f);
		written += sizeof(char)*levhead.nsyms;

		int level_entry_size = sizeof(struct ser_level_entry)*j;
		for (i = 0 ; i < levhead.nsyms ; ++i) {
			fwrite(&(results[i]->entry), sizeof(struct ser_level_entry), 1, fl->f);
			if (results[i]->entry.subtree_max_count > retval->entry.subtree_max_count)
				retval->entry.subtree_max_count = results[i]->entry.subtree_max_count;
			if (results[i]->entry.term_count > retval->entry.subtree_max_count)
				retval->entry.subtree_max_count = results[i]->entry.term_count;
		}
		written += level_entry_size;

		if (fl->userdata_visitor) {
			// Write out the ser_user_entry table
			for (i = 0 ; i < levhead.nsyms ; ++i) {
				struct ser_user_entry ue = { 0 };
				if (results[i]->user_data) {
					ue.data = fl->userdata_visitor(results[i]->user_data, fl->userdata_cbdata);
				}
				fwrite(&ue, sizeof(struct ser_user_entry), 1, fl->f);
			}
			written += sizeof(struct ser_user_entry)*levhead.nsyms;
		}

		for (i = 0 ; i < levhead.nsyms ; ++i) {
			free(results[i]);
		}

		written += padfile8(fl->f, written);

		size_t prev_offset = fl->prev_offset;
		fl->prev_offset += written;
		retval->entry.offset = prev_offset;
	}
	if (stats && stats->curr_level > stats->max_level)
			stats->max_level = stats->curr_level;

leavefunc:
	free(subtree_results);
	if (retval->entry.term_count < fl->limit)
		retval->entry.term_count = 0;
	return retval;
}

int brt_serialize_to(struct brt* bt, brt_count limit, brt_userdata_visitor userdata_visitor, void* userdata_cbdata, struct brt_postwalk_stats* stats, FILE* f, enum ser_flags flags) {

	struct brt_postwalk_stats l_stats = { 0, 0, 0, 0 };
	struct brt_serialization_cbdata ft;
	ft.prev_offset = 0;
	ft.f = f;
	ft.limit = limit;
	ft.userdata_visitor = userdata_visitor;
	ft.userdata_cbdata = userdata_cbdata;

	struct ser_header header = {
		.id = BRT_SERIALIZED_ID0,
		.tree_size = -1,
		.num_keys = 0,
		.max_key_len = 0,
		.max_level = 0,
		.flags = flags
	};
	header.flags |= userdata_visitor ? BRT_SER_USER_DATA : 0;

	// TODO: use f{get,set}pos instead
	long start_foffs = ftell(f);

	fwrite(&header, sizeof(header), 1, f);
	// ft.prev_offset = padfile8(f, sizeof(header));
	struct ser_level* retval = brt_postwalk(bt, brt_serialization_visitor, &ft, &l_stats);

	if (retval) {
		header.root = retval->entry;
		// Manually count the empty-string
		if (retval->entry.term_count)
			++l_stats.num_keys;
		if (ft.userdata_visitor)
			header.root_data.data = ft.userdata_visitor(retval->user_data, ft.userdata_cbdata);
		free(retval);
	}

	header.num_keys = l_stats.num_keys;
	header.max_key_len = l_stats.max_key_len;
	header.max_level = l_stats.max_level;
	header.tree_size = ft.prev_offset;
	if (stats) {
		l_stats.curr_level = 0;
		*stats = l_stats;
	}

	long end_foffs = ftell(f);

	int fres = fseek(f, start_foffs, SEEK_SET);
	if (fres < 0) {
		return -1;
	}
	fwrite(&header, sizeof(header), 1, f);
	fseek(f, end_foffs, SEEK_SET);

	return sizeof(header) + header.tree_size;
}

