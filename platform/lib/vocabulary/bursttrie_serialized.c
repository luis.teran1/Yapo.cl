#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <assert.h>
#include <limits.h>

#include "bursttrie_serialized.h"
#include "levs.h"

enum {
	SER_FIND_FOUND = 1,
	SER_FIND_EXHAUSTED = -1,
	SER_FIND_STOP_NO_ARC = -2,
	SER_FIND_EMPTY_KEY = -5,
	SER_FIND_STOP_AT_BST = -10,
};


static unsigned int
brt_ser_find_internal(unsigned char* map, int levelofs, const char* key, int* retval, int* last_level_ofs, const char** keyleft, void** data) {
	int i;

	struct ser_header* header = (struct ser_header*)map;
	map += sizeof(struct ser_header);

	while (*key) {
		struct ser_level_header* lh = (struct ser_level_header*)(map + levelofs);
		unsigned char* base = map + levelofs + sizeof(struct ser_level_header);

		assert(lh->type == BNT_FULL || lh->type == BNT_BST);
		assert(lh->nsyms < 256);

		if (lh->type == BNT_FULL) {
			struct ser_level_entry* idx = (struct ser_level_entry*)(base + lh->nsyms);
			for (i=0 ; i < lh->nsyms ; ++i) {
				if (base[i] == (unsigned char)*key) {
					++key;
					if (keyleft)
						*keyleft = key;

					// We've exhausted our key.
					if (*key == '\0') {
						if (last_level_ofs && (idx[i].subtree_max_count != 0))
							*last_level_ofs = idx[i].offset;
						if (idx[i].term_count) {
							if (header->flags & BRT_SER_USER_DATA && data) {
								struct ser_user_entry* user_entries = (struct ser_user_entry*)(base + lh->nsyms + sizeof(struct ser_level_entry)*lh->nsyms);
								*data = &user_entries[i].data;
							}
							*retval = SER_FIND_FOUND;
							return idx[i].term_count;
						} else {
							*retval = SER_FIND_EXHAUSTED; // exhausted key, but it's not terminated here.
							return 0;
						}
					}
					if (idx[i].subtree_max_count == 0) {
						if (last_level_ofs)
							*last_level_ofs = levelofs;
						*retval = SER_FIND_STOP_NO_ARC; // path ends here, no-where to go.
						return 0;
					}
					levelofs = idx[i].offset;
					break;
				}
			}
			if (i == lh->nsyms) {
				*retval = SER_FIND_STOP_NO_ARC;
				return 0; // exit: key leading char does not exist on level
			}
		} else if (lh->type == BNT_BST) {
			if (last_level_ofs) { // It's a signal we don't really care about the contents of the container, only where it is.
				*last_level_ofs = levelofs;
				*retval = SER_FIND_STOP_AT_BST;
				return 0;
			}

			struct ser_user_entry* user_entries = NULL;
			if (header->flags & BRT_SER_USER_DATA)
				user_entries = (struct ser_user_entry*)base;

			struct ser_bst_cntlen* idx = (struct ser_bst_cntlen*)(base + (user_entries ? sizeof(struct ser_user_entry)*lh->nsyms : 0));
			char*  stringz = (char*)(idx) + sizeof(struct ser_bst_cntlen)*lh->nsyms;

			// Binary search for the rest of the key.
			int hi = lh->nsyms-1;
			int mid = -1;
			int low = 0;
			while (low <= hi) {
				mid = low + (hi-low) / 2;
				int comp = strcmp(key, stringz+idx[mid].offset);
				if (comp > 0)
					low = mid + 1;
				else if (comp < 0)
					hi = mid - 1;
				else {
						if (user_entries && data)
							*data = &user_entries[mid].data;
						*retval = SER_FIND_FOUND;
						return idx[mid].count;
				}
			};

			/*
				Linear scan (alternative):
				for (i=0 ; i < lh->nsyms ; ++i) {
					if (strcmp(key, (char*)(base+sizeof(struct ser_bst_cntlen)*lh->nsyms+idx[i].offset)) == 0) {
						[... handle user data ...]
						return idx[i].count;
					}
				}
			*/
			*retval = SER_FIND_STOP_NO_ARC;
			return 0; // key tail not found
		}
	}

	*retval = SER_FIND_EMPTY_KEY;
	return 0; // exit: got empty key as input
}

brt_count brt_ser_find(unsigned char* map, int levelofs, const char* key, void** data) {
	int retval = 0;

	brt_count cnt = brt_ser_find_internal(map, levelofs, key, &retval, NULL, NULL, data);
	return retval == SER_FIND_FOUND ? cnt : 0;
}

struct search {
	unsigned char*		map;
	char				buffer[BRT_MAX_KEY_LENGTH]; // Workbuffer
	char*				start_pos; // pointer into buffer AFTER the current output prefix.
	const char* 		prefix; // search-prefix for BST-nodes
	unsigned int		limit; // Only output entries with this many or more occurences.
	brt_ser_prefix_cb	onhit;
	void*				cbdata;
};

static void brt_ser_prefix_internal(int levelofs, struct search* s) {

	struct ser_header* header = (struct ser_header*)s->map;
	struct ser_level_header* lh = (struct ser_level_header*)(s->map + sizeof(struct ser_header) + levelofs);
	unsigned char* base = s->map + sizeof(struct ser_header) + levelofs + sizeof(struct ser_level_header);
	struct ser_user_entry* user_entries = NULL;

	brt_count newlim;

	assert(lh->type == 0 || lh->type == 1);
	assert(lh->nsyms < 256);

	if (lh->type == BNT_FULL) {
	 	struct ser_level_entry* idx = (struct ser_level_entry*)(base + lh->nsyms);
		if (header->flags & BRT_SER_USER_DATA)
			user_entries = (struct ser_user_entry*)(struct ser_user_entry*)(base + lh->nsyms + sizeof(struct ser_level_entry)*lh->nsyms);

		for (int i=0 ; i < lh->nsyms ; ++i) {
			*(s->start_pos++) = base[i];
			if (idx[i].term_count >= s->limit) {
				newlim = s->onhit(s->buffer, NULL, idx[i].term_count, user_entries ? &user_entries[i] : NULL, s->cbdata);
				if (newlim > s->limit)
					s->limit = newlim;
			}
			if (idx[i].subtree_max_count >= s->limit)
				brt_ser_prefix_internal(idx[i].offset, s);
			*(s->start_pos--) = '\0';
		}

	} else if (lh->type == BNT_BST) {
		if (header->flags & BRT_SER_USER_DATA)
			user_entries = (struct ser_user_entry*)base;

		struct ser_bst_cntlen* idx = (struct ser_bst_cntlen*)(base + (user_entries ? sizeof(struct ser_user_entry)*lh->nsyms : 0));
		// TODO: If nsyms is large then we'd definitely want a binary search to find the first item.
		for (int i=0 ; i < lh->nsyms ; ++i) {
			char* bstkey = ((char*)idx + sizeof(struct ser_bst_cntlen)*lh->nsyms + idx[i].offset);
			if (idx[i].count >= s->limit) {
			  	if (s->prefix == NULL || *(s->prefix) == '\0' || strncmp(s->prefix, bstkey, strlen(s->prefix)) == 0) {
					newlim = s->onhit(s->buffer, bstkey, idx[i].count, user_entries ? &user_entries[i] : NULL, s->cbdata);
					if (newlim > s->limit)
						s->limit = newlim;
				}
			}
		}
	}
}

int brt_ser_prefix(unsigned char* map, int levelofs, const char* key, brt_count limit, brt_ser_prefix_cb onhit, void* cbdata) {
	const char* keyleft = key;
	void* userdata = NULL;
	int slevel = -1;
	int retval = 0;

	brt_count cnt = brt_ser_find_internal(map, levelofs, key, &retval, &slevel, &keyleft, &userdata);

	if (retval == SER_FIND_STOP_NO_ARC)
		return 0;

	if (retval == SER_FIND_EMPTY_KEY)
		slevel = levelofs;

	struct search s = { map, { 0 }, s.buffer, keyleft, limit, onhit, cbdata };

	if (retval == SER_FIND_FOUND) {
		brt_count newlim = onhit((char*)key, NULL, cnt, userdata, cbdata);
		if (newlim > s.limit)
			s.limit = newlim;
	}

	// Should we proceed and process the children of the found key?
	if ((slevel != -1 && (retval == SER_FIND_FOUND || retval == SER_FIND_EXHAUSTED)) || (retval == SER_FIND_STOP_AT_BST) || (retval == SER_FIND_EMPTY_KEY)) {

		strncpy(s.buffer, key, keyleft - key);
		s.start_pos += keyleft - key;
		s.prefix = keyleft;
		if (s.limit < 1)
			s.limit = 1;

		brt_ser_prefix_internal(slevel, &s);
	}

	return 0;
}

// Below is levenshtein search implementation

struct search_record {
	unsigned char* map;
	unsigned char* level;
	int max_k;
	unsigned int level_cutoff;
	int min_dist_cutoff;

	enum ser_search_flags flags;
	int brt_flags;
	struct lev_s* lev;

	struct lev_decode_state* stack;
	int stack_ptr;
	int stack_size;

	const char* word;

	brt_ser_search_cb cb;
	void* cbdata;

#ifdef BRTSTATS
	int nodes_visited;
	int words_visited;
#endif
};

static void search_level(struct search_record* sr);

static void search_level_full(struct search_record* sr) {

	struct ser_level_header* lh = (struct ser_level_header*)(sr->level);
	unsigned char* base = sr->level + sizeof(struct ser_level_header);
	struct ser_level_entry* idx = (struct ser_level_entry*)(base + lh->nsyms);

	for (int i=0 ; i < lh->nsyms ; ++i) {
		int cuted, dist;

		sr->stack[sr->stack_ptr++] = lev_decode_state_get(sr->lev);

		int syminc = lev_concat(sr->lev, base[i]);
		if (syminc == 0) {
			dist = lev_update(sr->lev);
#ifdef BRTSTATS
			if (idx[i].term_count)
				sr->words_visited += 1;
#endif

			// We check for term_count GREATER than cutoff, meaning LEVSOPT_PRELOAD_CUTOFF will never include the 'preload' candidate if any.
			if (dist <= sr->max_k && idx[i].term_count > sr->level_cutoff) {
				if (sr->brt_flags & BRT_SER_USER_DATA) {
					struct ser_user_entry* user_entries = (struct ser_user_entry*)(base + lh->nsyms + sizeof(struct ser_level_entry)*lh->nsyms);
					sr->cb(lev_get_candidate(sr->lev), idx[i].term_count, dist, &user_entries[i].data, sr->cbdata);
				} else {
					sr->cb(lev_get_candidate(sr->lev), idx[i].term_count, dist, NULL, sr->cbdata);
				}
				if (dist == sr->min_dist_cutoff && (sr->flags & LEVSOPT_OPPORTUNISTIC_CUTOFF)) {
					sr->level_cutoff = idx[i].term_count;
					sr->min_dist_cutoff = 1;
				}
			}
		}

		if (idx[i].subtree_max_count >= sr->level_cutoff && (syminc || ((cuted = lev_cuted(sr->lev, sr->max_k)) <= sr->max_k))) {
			sr->level = sr->map + idx[i].offset;
			search_level(sr);
		}

		lev_decode_state_set(sr->lev, &sr->stack[--sr->stack_ptr]);
	}

}

static void search_level_bst(struct search_record* sr) {

	struct ser_level_header* lh = (struct ser_level_header*)(sr->level);
	unsigned char* base = sr->level + sizeof(struct ser_level_header);
	struct ser_bst_cntlen* idx = (struct ser_bst_cntlen*)(base + (sr->brt_flags & BRT_SER_USER_DATA ? sizeof(struct ser_user_entry)*lh->nsyms : 0));

	struct lev_decode_state original_state = lev_decode_state_get(sr->lev);
	int dist;

	for (int i=0 ; i < lh->nsyms ; ++i) {
		// TODO OPT: If BST nodes were (also) sorted on count, we could break the first time count fell under cutoff.
		if (idx[i].count > sr->level_cutoff) {
			char* bstkey = ((char*)idx + sizeof(struct ser_bst_cntlen)*lh->nsyms + idx[i].offset);
			int bstkeylen = strlen(bstkey); /* TODO: calc len from index? */

			// TODO OPT: 'continue' if len makes the candidate longer than sr->word+max_k
			lev_concat_n(sr->lev, bstkey, bstkeylen);

			if ((dist = lev_update(sr->lev)) <= sr->max_k) {
				if (sr->brt_flags & BRT_SER_USER_DATA) {
					struct ser_user_entry* user_entries = (struct ser_user_entry*)base;
					sr->cb(lev_get_candidate(sr->lev), idx[i].count, dist, &user_entries[i], sr->cbdata);
				} else {
					sr->cb(lev_get_candidate(sr->lev), idx[i].count, dist, NULL, sr->cbdata);
				}

				if (dist == sr->min_dist_cutoff && (sr->flags & LEVSOPT_OPPORTUNISTIC_CUTOFF)) {
					sr->level_cutoff = idx[i].count;
				}
			}
			lev_decode_state_set(sr->lev, &original_state);
		}
	}

#ifdef BRTSTATS
	sr->words_visited += lh->nsyms;
#endif
}

static void search_level(struct search_record* sr) {

	struct ser_level_header* lh = (struct ser_level_header*)(sr->level);

	assert(lh->type == BNT_FULL || lh->type == BNT_BST);
	assert(lh->nsyms < 256);

#ifdef BRTSTATS
	sr->nodes_visited += 1;
#endif

	if (lh->type == BNT_FULL) {
		search_level_full(sr);
	} else {
		search_level_bst(sr);
	}

}

void brt_ser_levenshtein(unsigned char* map, const char* word, int max_k, int cutoff, enum ser_search_flags flags, brt_ser_search_cb cb, void* cbdata) {

	struct ser_header* header = (struct ser_header*)map;
	struct search_record sr;

	enum lev_flags internal_flags = 0;
	if (header->flags & BRT_SER_UTF8)
		internal_flags |= USE_UTF8;

	/* USER */
	sr.max_k = max_k;
	sr.flags = flags;
	sr.word = word;

	sr.map = map + sizeof(struct ser_header);
	sr.level = sr.map + header->root.offset;
	sr.brt_flags = header->flags;
	sr.lev = lev_setup(NULL, header->max_key_len+1, sr.word, internal_flags);
	sr.stack_ptr = 0;
	sr.stack_size = header->max_level;
	sr.stack = calloc(sr.stack_size, sizeof(struct lev_decode_state));
	sr.level_cutoff = cutoff;
	sr.min_dist_cutoff = 0;
#ifdef BRTSTATS
	sr.nodes_visited = 0;
	sr.words_visited = 0;
#endif
	sr.cb = cb;
	sr.cbdata = cbdata;

	search_level(&sr);

#ifdef BRTSTATS
	printf("STATS: words_visited=%d, nodes_visited=%d\n", sr.words_visited, sr.nodes_visited);
#endif

	free(sr.stack);
	lev_free(sr.lev);
}

