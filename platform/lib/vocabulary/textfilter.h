#ifndef TEXTFILTER_H
#define TEXTFILTER_H

struct textfilter_filter_conf {
	int	min_len;
	int	max_len;
};

int textfilter_latin(const char* restrict s, size_t len, char* restrict target, void* userdata);
int textfilter_utf8(const char* restrict s, size_t len, char* restrict target, void* userdata);

#endif

