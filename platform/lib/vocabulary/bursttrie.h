#include <stdint.h>
#include "bursttrie_common.h"

struct brt;
struct brt_val;

struct brt_postwalk_stats {
	int	max_level;
	int	num_keys;
	int	max_key_len;
	int	curr_level;
};

/*
	User-data serialization: In the callback, write your data (using info
	in cbdata presumably) to an external file, and return whatever value
	you use to find it again (index, offset)
*/
typedef uint32_t(*brt_userdata_visitor)(void* data, void* cbdata);

/*
	Prototype for brt_postwalk visitor
*/
typedef void*(*brt_postwalk_visitor)(void* data, void* cbdata, void* subtree_results, int num_subtree_results, struct brt_postwalk_stats* stats);

/*
	Callback used by the prefix search.
	The first to arguments together form the key, but the second can be NULL.
*/
typedef int(*brt_prefix_cb)(char*, char*, brt_count* count, void* data, void* cbdata);

brt_count brt_insert(struct brt** bt, const char* key, void** data);
brt_count brt_insert_count(struct brt** bt, const char* key, brt_count count, void** data);
brt_count brt_find(struct brt* bt, const char* key, void** data);

/*
	Set the count for an existing key, and return its old count.
*/
brt_count brt_set_count(struct brt* bt, const char* key, brt_count count);

void brt_prefix(struct brt* bt, const char* prefix, int limit, brt_prefix_cb onhit, void* cbdata);

int brt_insert_file(struct brt** bt, const char* filename);

void* brt_postwalk(struct brt* bt, brt_postwalk_visitor visit, void* cbdata, struct brt_postwalk_stats* stats);

int brt_serialize_to(struct brt* bt, brt_count limit, brt_userdata_visitor userdata_visitor, void* userdata_cbdata, struct brt_postwalk_stats* stats, FILE* f, enum ser_flags flags);

void brt_debug(struct brt* bt);

void brt_free(struct brt** bt, void(*userfree)(void*));

