/*
	The module implements vocabulary accumulation, used for search suggestions.

	In this context, a vocabulary is one or more separate 'databases' of words /w counts.
*/
#ifndef PLATFORM_VOCABULARY_H
#define PLATFORM_VOCABULARY_H

struct hash_table;
struct bconf_node;
struct voc_entry;
struct bpapi;

struct vocabulary {
	struct hash_table* dbs;
	struct voc_entry*  current_entry;
};


/*
	Initialize a vocabulary, using configuration from the given bconf node.
*/
int vocabulary_init(struct vocabulary *v, struct bpapi* api, const char* confpath);

/*
	Return a pointer to internal configuration.
*/
void* vocabulary_get_conf(struct vocabulary *v);

/*
	Write out the vocabulary as separate files as 'basename.<dbname>.*'
*/
int vocabulary_store(struct vocabulary *v, const char* statsname, struct bconf_node** statsnode);

/*
	Switch the currently selected database.
*/
int vocabulary_switch_db(struct vocabulary *v, const char *db);

/*
	Insert key or (re)set count. Works on currently selected database.

	Resetting a count is incompatible with using a sumtree, except in the special
	case of setting the count below the threshold so that the node is pruned on
	serialization.
*/
int vocabulary_insert(struct vocabulary *v, const char *key, int count, int category);
int vocabulary_set_count(struct vocabulary *v, const char *key, int count);

/*
	Free all resources used by the vocabulary, but not itself.
*/
int vocabulary_deinit(struct vocabulary *v);


#endif

