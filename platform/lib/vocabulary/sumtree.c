#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include "sumtree_internal.h"
#include "sumtree.h"
#include "macros.h"
#include "util.h"

/*
	The sumtree is a data-structure that can be used to track counts over
	some parent-child integer hierarchy. The instanced sumtree itself is a
	logical representation, while one or more separate arrays are used to
	store the actual counts.

	In addition, the hierarchy can be 'folded', such that levels beyond some
	depth only count into their parental non-folded levels, saving space
	in the physical representation (a simple array) of the counts by
	effectively 'quantizing' the data.

	categories	max level	physical size
	239			3 (all)		960 bytes	(239+1 * sizeof(int))
	239			2			280 bytes
	239			1			40 bytes
	239			0			4 bytes

	Index zero of the array is reserved to always tracks to total sum.

	TODO: 
		  map back from phy to logical/cat
		  should fix some identifiers with bad names.
*/

struct sumtree* sumtree_create(int size) {
	struct sumtree* st = calloc(1, sizeof(struct sumtree));
	if (st) {
		st->map_size = size;
		st->map_used = 1;
		st->map = calloc(st->map_size, sizeof(struct st_entry));
		st->map[0].parent = -1;
	}
	return st;
}

void sumtree_free(struct sumtree* st) {
	if (st && ((st->flags & ST_STATIC) != ST_STATIC)) {
		free(st->map);
		free(st->rev_map);
		free(st);
	}
}

static int sumtree_resize_map(struct sumtree* st, int size) {
	struct st_entry* tmp = realloc(st->map, sizeof(struct st_entry)*size);
	if (tmp) {
		st->map = tmp;
		st->map_size = size;
		return 1;
	}
	return 0;
}

static inline struct st_entry* sumtree_find(struct sumtree* st, int cat) {
	struct st_entry key = { .cat = cat };
	return bsearch(&key, st->map, st->map_used, sizeof(struct st_entry), comp_entry_cat);
}

int sumtree_size(struct sumtree* st) {
	return st->live_size;
}

int sumtree_map_size(struct sumtree* st) {
	return st->map_used;
}

void sumtree_insert(struct sumtree* st, int cat, int parent) {
	if (st->flags & ST_FINALIZED)
		return;
	if (st->map_used >= st->map_size)
		sumtree_resize_map(st, st->map_size + (st->map_size / 2));
	st->map[st->map_used].cat = cat;
	st->map[st->map_used].parent = parent;
	st->map[st->map_used].mapped = 0;
	++st->map_used;
}

int sumtree_add(struct sumtree* st, uint32_t* sums, int cat, int score) {
	struct st_entry* e = sumtree_find(st, cat);
	if (e) {
		int idx = e - st->map;
		while (idx >= 0) {
			if (st->map[idx].mapped > 0 || idx == 0)
				sums[st->map[idx].mapped] += score;
			idx = st->map[idx].parent;
		}
		return 1;
	}
	st->oob += score;
	return 0;
}

int sumtree_get(struct sumtree* st, uint32_t* sums, int cat) {
	struct st_entry* e = sumtree_find(st, cat);
	if (e) {
		int idx = e - st->map;
		while (idx >= 0) {
			if (st->map[idx].mapped)
				return sums[st->map[idx].mapped];
			idx = st->map[idx].parent;
		}
	}
	return -1;
}

int sumtree_cat(struct sumtree* st, int idx) {
	assert(idx >= 0 && idx < st->live_size);
	return st->rev_map[idx];
}

static void sumtree_calc_levels(struct sumtree* st) {

	for (int i=0 ; i < st->map_used ; ++i) {
		int idx = i;
		int level = 0;
		while (idx >= 0) {
			if (level && st->map[idx].level) {
				level = st->map[idx].level + 2;
				break;
			}
			idx = st->map[idx].parent;
			++level;
		}
		st->map[i].level = level - 1;
	}
}

static void sumtree_calc_revmap(struct sumtree* st) {

	st->rev_map = xmalloc(sizeof(int)*st->live_size);
	st->rev_map[0] = 0;
	for (int i=0 ; i < st->map_used ; ++i) {
		if (st->map[i].mapped > 0) {
			// printf("Reverse mapping %d to category %d\n", st->map[i].mapped, st->map[i].cat);
			st->rev_map[st->map[i].mapped] = st->map[i].cat;
		}
	}
}

static int sumtree_quantize(struct sumtree* st, int max_level) {

	/* Setup parents and mapped such that we'll be able to store to the correct locations. */
	int base_idx = 0;
	for (int i=1 ; i < st->map_used ; ++i) {
		st->map[i].mapped = 0;
		if (st->map[i].level > max_level) {
			int j = i;
			while (st->map[j].level > max_level && st->map[j].parent >= 0) {
				j = st->map[j].parent;
			}
			st->map[i].parent = j;
		} else {
			st->map[i].mapped = ++base_idx;
		}
	}
	st->live_size = 1 + base_idx;
	return st->live_size;
}

int sumtree_finalize(struct sumtree* st, int max_level) {
	if (st->flags & ST_FINALIZED)
		return st->live_size;
	qsort(st->map, st->map_used, sizeof(struct st_entry), comp_entry_cat);

	int i = 1;
	int track_parent = -2;
	while (i < st->map_used) {
		if (st->map[i].parent != track_parent && st->map[i].mapped == 0 && st->map[i].parent > 0) {
			track_parent = st->map[i].parent;
			struct st_entry* e = sumtree_find(st, track_parent); 
			assert(e != NULL);
			if (e != NULL) {
				int idx = e - st->map;
				for (int j = i ; j < st->map_used ; ++j) {
					if (st->map[j].parent == track_parent) {
						st->map[j].parent = idx;
						st->map[j].mapped = 1;
					}
				}
			}
		}
		++i;
	}

	sumtree_calc_levels(st);
	sumtree_quantize(st, max_level); /* Must do this unconditinally to set up .mapped */
	sumtree_calc_revmap(st);
	st->flags |= ST_FINALIZED;
	return st->live_size;
}

struct sumtree* sumtree_deserialize(FILE* f) {
	char tmp[] = ST_SERIALIZED_ID0;

	struct sumtree_header header;
	UNUSED_RESULT(fread((void*)&header, sizeof(header), 1, f));

	if (strncmp(header.id, tmp, sizeof(header.id)) != 0)
		return NULL;

	if (header.ptrsize != sizeof(struct st_entry*)) {
		/* Serialized data layout would not be compatible with this machine */
		return NULL;
	}

	struct sumtree* st = calloc(1, sizeof(struct sumtree));
	UNUSED_RESULT(fread(st, sizeof(struct sumtree), 1, f));
	if (st->map_size == st->map_used && st->map_size > 0 && st->map_size < 4096) {
		st->map = xmalloc(sizeof(struct st_entry)*st->map_used);
		UNUSED_RESULT(fread(st->map, sizeof(struct st_entry), st->map_used, f));
		st->flags &= ~ST_STATIC;
		sumtree_calc_revmap(st);
	}
	return st;
}

int sumtree_serialize(struct sumtree* st, FILE* f) {

	if ((st->flags & ST_FINALIZED) != ST_FINALIZED)
		return -1;

	struct sumtree_header header = { .id = ST_SERIALIZED_ID0 };
	header.size = sizeof(header);
	header.sumtreesize = sizeof(struct sumtree) + sizeof(struct st_entry) * st->map_used; 
	header.ptrsize = sizeof(struct st_entry*);
	header.dummy = 0;

	fwrite(&header, sizeof(header), 1, f);

	/* Make a copy of local header so we can make some changes before writing it */
	struct sumtree headstatic = *st;
	headstatic.map = (void*) sizeof(struct sumtree);
	headstatic.rev_map = NULL;
	headstatic.map_size = headstatic.map_used;
	headstatic.flags |= ST_STATIC;

	fwrite(&headstatic, sizeof(struct sumtree), 1, f);
	fwrite(st->map, sizeof(struct st_entry), st->map_used, f);

	return header.size + header.sumtreesize;
}

