#include <string.h>
#include "textfilter.h"

static int tfdefault_filter_latin(const char* st, const char* en, void* userdata) {
		struct textfilter_filter_conf* cfg = userdata;

		if (!st || !en || en <= st)
			return 1;

		if (cfg && ((en-st > cfg->max_len) || (en-st < cfg->min_len)))
			return 2;

		// May no begin with
		if (strchr("0123456789&-+%'`", *st))
			return 3;

		const char* p = st;
		int bad_count = 0;
		while (p < en) {
			if ((unsigned char)*p < 0x20)
				return 4;

			switch (*p) {
				// May not contain
				case '.':
				case ',':
				case ':':
				case ';':
				case '$':
				case '%':
				case '/':
				case '@':
				case '{':
				case '}':
				case '~':
				case '=':
					return 5;
				// Reject any string with multiple of these characters
				case '&':
				case '-':
				case '\'':
				case '`':
					if (++bad_count > 1)
						return 6;
			}

			++p;
		}

		// Disallow three-character repeated at the beginnig ('www', etc)
		if (en-st > 2) {
			if (*st == *(st+1) && *st == *(st+2))
				return 7;
		}

		return 0;
}

static int tfdefault_filter_utf8(const char* st, const char* en, void* userdata) {
	struct textfilter_filter_conf* cfg = userdata;

	if (!st || !en || en <= st)
	       return 1;

	const char* p = st;
	if (cfg) {
		int len = 0;
		while (p < en) {
			if ((*p++ & 0xc0) != 0x80)
				++len;
		}
		if (len < cfg->min_len || len > cfg->max_len)
			return 2;

		p = st;
	}

	// May no start with
	if (strchr("0123456789&-+%'`", *st))
		return 3;

	while (p < en) {
		if ((unsigned char)*p < 0x20)
			return 4;

		switch (*p) {
			// May not contain
			case '.':
			case ',':
			case ':':
			case ';':
			case '$':
			case '%':
			case '/':
			case '@':
			case '{':
			case '}':
			case '~':
			case '=':
				return 5;
		}

		++p;
	}

	return 0;
}

typedef int(*textfilter_filter)(const char*, const char*, void*);

struct textfilter_conf {
	const char* delims;
	const char* edible_delimiter_tbl;
	textfilter_filter filter;
};

struct textfilter_conf textfilter_configuration[] = {
	// latin-1 filter configuration
	{
		" \t\n()[]\"?!~*+\xB4^_></\\#|\xa0", /* B4 = ´, A0 = nbsp */
		"%&.,-:/'`=",
		tfdefault_filter_latin
	},
	// UTF8 filter configuration
	{
		" \t\n()[]\"?!~*+^_></\\#|",
		"%&.,-:/'`=",
		tfdefault_filter_utf8
	},
};

static int textfilter_internal(const char* restrict s, size_t len, char* restrict target, struct textfilter_conf* conf, void* restrict userdata) {

	if (!s)
		return 0;

	int filter_cause;
	const char* st = s;
	const char* en = NULL;
	const char* pend = s + len;
	const char* tend = target + len;
	const char* target_base = target;

	while (st < pend && target < tend) {
		en = strpbrk(st+1, conf->delims);
		if (!en)
			en = pend;

		// FF through starting delimiters
		while (st < en && *st && strchr(conf->delims, *st))
			   ++st;

		// Eat at the beginning...
		while (st < en && *st && strchr(conf->edible_delimiter_tbl, *st))
			++st;

		// Eat at the end...
		while (st < en && en+1 > s && strrchr(conf->edible_delimiter_tbl, *(en-1)))
			--en;

		if (! (filter_cause = conf->filter(st, en, userdata))) {
			memcpy(target, st, (int)(en-st));
			target += (int)(en-st);
			*target++ = ' ';
		}

		// FF through trailing delimiters
		while (en < pend && *en && strchr(conf->delims, *en))
		   ++en;

		st = en;
	}
	/* Ensure we stay within the buffer. If we wrote we know the last character is a space, so replace it */
	if (target > target_base)
		--target;
	*target = '\0';
	return target - target_base;
}

int textfilter_latin(const char* restrict s, size_t len, char* restrict target, void* restrict userdata) {
	return textfilter_internal(s, len, target, &textfilter_configuration[0], userdata);
}

int textfilter_utf8(const char* restrict s, size_t len, char* restrict target, void* restrict userdata) {
	return textfilter_internal(s, len, target, &textfilter_configuration[1], userdata);
}

