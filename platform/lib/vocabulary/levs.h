
enum lev_flags {
	USE_UTF8 = 1,
};

struct lev_s;

struct lev_decode_state {
	int dec_ci;
	int raw_i;
	int raw_decode_base;
	int symbol_incomplete;
};

struct lev_s* lev_setup(struct lev_s* levs, int max_depth, const char* word, enum lev_flags flags);
int lev_concat(struct lev_s* levs, unsigned char sym);
void lev_concat_n(struct lev_s* levs, const char* syms, int nsyms);
int lev_update(struct lev_s* levs);
int lev_cuted(struct lev_s* levs, int max_k);

struct lev_decode_state lev_decode_state_get(struct lev_s* levs);
void lev_decode_state_set(struct lev_s* levs, struct lev_decode_state* state);
const char* lev_get_candidate(struct lev_s* levs);

void lev_free(struct lev_s* levs);

void lev_debug(struct lev_s* levs);


