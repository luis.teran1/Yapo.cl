#ifndef PLATFORM_SUMTREE_INTERNAL_H
#define PLATFORM_SUMTREE_INTERNAL_H

#include <stdint.h>

enum sumtree_flags {
	ST_FINALIZED = 1,
	ST_STATIC = 2,
};

struct st_entry {
	int32_t cat;
	int32_t parent;
	int32_t mapped;
	int32_t level;
}  __attribute__((packed));

struct sumtree {
	struct st_entry* map;
	int32_t* rev_map;
	int32_t map_size;
	int32_t map_used;
	int32_t live_size;
	int32_t oob;
	uint32_t flags; /* enum sumtree_flags */
} __attribute__((packed));

static inline int comp_entry_cat(const void *e1, const void *e2) {
	struct st_entry* ei1 = (struct st_entry*) e1;
	struct st_entry* ei2 = (struct st_entry*) e2;
	return ei1->cat - ei2->cat;
}

#endif

