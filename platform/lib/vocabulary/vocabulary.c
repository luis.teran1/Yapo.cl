#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <sys/types.h>
#include <unicode/unorm.h>
#include "util.h"
#include "logging.h"
#include "bconf.h"
#include "vtree.h"
#include "hash.h"
#include "ctemplates.h"

#include "vocabulary.h"
#include "bursttrie.h"
#include "sumtree.h"

struct sumtree_serdata {
	FILE*	f;
	struct sumtree* sumtree;
	uint32_t	offset;
	uint32_t	counter;
};

static uint32_t ser_sumtree_ext(void* data, void* cbdata) {
	struct sumtree_serdata* serdata = cbdata;

	if (!data)
		return 0;

	int written = sumtree_size(serdata->sumtree)*sizeof(uint32_t);
	fwrite(data, written, 1, serdata->f);
	serdata->offset += written;

	return ++serdata->counter;
}

struct voc_entry {
	struct brt* trie;
	struct sumtree* sumtree;
	size_t insertions;

	struct bpapi_vtree_chain conf;
};

static void free_voc_entry(void* p) {
	struct voc_entry *ve = p;

	if (ve->trie)
		brt_free(&ve->trie, ve->sumtree ? free : NULL);
	if (ve->sumtree) {
		sumtree_free(ve->sumtree);
	}
	vtree_free(&ve->conf);
	free(ve);
}

void* vocabulary_get_conf(struct vocabulary *v) {
	if (!v || !v->current_entry)
		return NULL;
	return &(v->current_entry->conf);
};

static struct sumtree* setup_sumtree(struct bpapi *api, const char* template, int maxlevel) {

	/* 256 = best guess at number of categories, will realloc if needed */
	struct sumtree* sumtree = sumtree_create(256);

	struct bpapi ba;
	struct buf_string* buf = { 0 };

	BPAPI_COPY(&ba, api, 0, 1, 1);
	buf = buf_output_init(&ba.ochain);

	if (!call_template(&ba, template)) {
		log_printf(LOG_CRIT, "Tried to call nonexisting template: %s", template);
		sumtree_free(sumtree);
		return NULL;
	} else if (buf->buf && *buf->buf) {
		char* line;
		char* nl;

		/* Parse lines of "<category> <parent>" */
		for (line = buf->buf ; line && *line ; line = nl) {

			nl = strchr(line, '\n');
			if (nl)
				*nl++ = '\0';

			while (isspace(*line))
				++line;

			char* parent = strchr(line, ' ');
			if (*line && parent) {
				*parent++ = '\0';
				while (isspace(*parent))
					++parent;
				int cat = atoi(line);
				int par = atoi(parent);
				sumtree_insert(sumtree, cat, par);
				// log_printf(LOG_DEBUG, "Sumtree: Inserted category %d with parent %d", cat, par);
			}
		}

	}
	bpapi_output_free(&ba); // XXX: Absolutely do not free anything more here.

	if (maxlevel < 1)
		maxlevel = 42;

	int n = sumtree_finalize(sumtree, maxlevel);
	log_printf(LOG_DEBUG, "Sumtree finalized, %d entries. Will require %zu bytes/word for userdata.", n, sumtree_size(sumtree)*sizeof(uint32_t));

	return sumtree;
}


/*
	Initialize an already existing vocabulary structure.
*/
int vocabulary_init(struct vocabulary *v, struct bpapi *api, const char* confpath) {

	v->dbs = hash_table_create(5, free_voc_entry);
	hash_table_free_keys(v->dbs, 1); /* Free xstrdup'ed db keys */

	struct bpapi_vtree_chain node = { 0 };
	struct bpapi_vtree_chain* root = vtree_getnode(&api->vchain, &node, confpath, NULL);
	struct bpapi_loop_var dbs;

	vtree_fetch_keys(root, &dbs, NULL);

	int num_configured = 0;
	for (int i=0 ; i < dbs.len ; ++i) {
		const char* key = dbs.l.list[i];

		int configured = 0;
		if (vtree_getint(root, key, "enabled", NULL)) {
			struct voc_entry* ve = zmalloc(sizeof(struct voc_entry));

			// Store configuration leaf branch for later.
			vtree_getnode(&api->vchain, &ve->conf, confpath, key, NULL);

			// Create sumtree object if requested.
			const char* sumtree_template;
			if ((sumtree_template = vtree_get(&ve->conf, "sumtree_template", NULL)) != NULL) {
				ve->sumtree = setup_sumtree(api, sumtree_template, vtree_getint(&ve->conf, "sumtree_max_level", NULL));
			}

			hash_table_insert(v->dbs, xstrdup(key), -1, ve);

			++num_configured;
			log_printf(LOG_DEBUG, "Vocabulary accumulation db '%s' activated, with%s sumtree", key, ve->sumtree == NULL ? "out" : "");
			configured = 1;
		}

		if (!configured)
			log_printf(LOG_DEBUG, "Vocabulary accumulation db '%s' disabled in configuration.", key);
	}

	if (dbs.cleanup)
		dbs.cleanup(&dbs);

	vtree_free(&node);

	return num_configured;
}

int vocabulary_store(struct vocabulary *v, const char* statsname, struct bconf_node** statsnode) {
	struct voc_entry* ve;
	void *state = NULL;
	void *key;
	char buf[256];
	char itoabuf[16];
	int  num_written = 0;

	while ((ve = hash_table_next(v->dbs, &state, &key, NULL))) {

		const char* filename = vtree_get(&ve->conf, "db_name", NULL);
		if (!filename)
			continue;
		if (!ve->trie) {
			log_printf(LOG_INFO, "No words in vocabulary db '%s'", (const char*)key);
			continue;
		}

		int limit = vtree_getint(&ve->conf, "min_occurences", NULL);
		enum ser_flags flags = 0;

		if (vtree_getint(&ve->conf, "utf8", NULL)) {
			flags |= BRT_SER_UTF8;
		}

		log_printf(LOG_INFO, "Writing vocabulary db '%s' to %s (limit %d)", (const char*)key, filename, limit);

		snprintf(buf, sizeof(buf), "%s.brt", filename);

		FILE* f = fopen(buf, "wb");
		if (f) {
			struct brt_postwalk_stats stats;
			size_t brt_size = 0;

			if (ve->sumtree) {
				snprintf(buf, sizeof(buf), "%s.sumtree", filename);
				FILE* fext = fopen(buf, "wb");
				if (fext) {
					sumtree_serialize(ve->sumtree, fext);
					struct sumtree_serdata serdata = { .f = fext, .sumtree = ve->sumtree, .offset = 0, .counter = 0 };
					brt_size = brt_serialize_to(ve->trie, limit, ser_sumtree_ext, &serdata, &stats, f, flags);
					++num_written;
					fclose(fext);
					/* TODO: insert serdata.counter into statsnode */
				} else {
					log_printf(LOG_ERR, "(ERROR) Failed to open sumtree output file '%s' for writing", buf);
				}
			} else {
				brt_size = brt_serialize_to(ve->trie, limit, NULL, NULL, &stats, f, flags);
				++num_written;
			}

			if (statsname && statsnode) {
				/* TODO: make a function to remove this duplication */

				snprintf(buf, sizeof(buf), "%s.%s.size", statsname, (const char*)key);
				snprintf(itoabuf, sizeof(itoabuf), "%zu", brt_size);
				bconf_add_data(statsnode, buf, itoabuf);

				snprintf(buf, sizeof(buf), "%s.%s.keys", statsname, (const char*)key);
				snprintf(itoabuf, sizeof(itoabuf), "%d", stats.num_keys);
				bconf_add_data(statsnode, buf, itoabuf);

				snprintf(buf, sizeof(buf), "%s.%s.max_key_len", statsname, (const char*)key);
				snprintf(itoabuf, sizeof(itoabuf), "%d", stats.max_key_len);
				bconf_add_data(statsnode, buf, itoabuf);

				snprintf(buf, sizeof(buf), "%s.%s.levels", statsname, (const char*)key);
				snprintf(itoabuf, sizeof(itoabuf), "%d", stats.max_level);
				bconf_add_data(statsnode, buf, itoabuf);
			}

			fclose(f);
		} else {
			log_printf(LOG_ERR, "(ERROR) Couldn't write db '%s' to %s", (const char*)key, filename);
			/* continue to attempt the next db if any */
		}
	}
	return num_written;
}

int vocabulary_deinit(struct vocabulary *v) {

	hash_table_free(v->dbs);

	return 1;
}

int vocabulary_switch_db(struct vocabulary *v, const char *db) {
	if (v && v->dbs) {
		struct voc_entry* ve = hash_table_search(v->dbs, db, -1, NULL);
		if (ve) {
			v->current_entry = ve;
			return 1;
		}
	}
	return 0;
}

int vocabulary_insert(struct vocabulary *v, const char *key, int count, int category) {
	if (!v || !v->current_entry)
		return 0;

	struct sumtree* st = v->current_entry->sumtree;
	uint32_t** nodedata = NULL;

	int new_count = brt_insert_count(&v->current_entry->trie, key, count, (void*)&nodedata);

	if (st) {
		if (!*nodedata) {
			*nodedata = calloc(sumtree_size(st), sizeof(uint32_t));
		}
		// log_printf(LOG_DEBUG, "Adding %d to count of '%s', category %d in sumtree at %p", count, key, category, *nodedata);
		sumtree_add(st, *nodedata, category, count);
	}

	if (new_count == count)
		++v->current_entry->insertions;

	return new_count;
}

int vocabulary_set_count(struct vocabulary *v, const char *key, int count) {
	if (!v || !v->current_entry)
		return 0;

	return brt_set_count(v->current_entry->trie, key, count);
}

