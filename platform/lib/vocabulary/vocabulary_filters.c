#include <bpapi.h>
#include <string.h>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include "util.h"
#include "patchvars.h"
#include "unicode.h"
#include "bconf.h"
#include "logging.h"
#include "vtree.h"

#include "vocabulary.h"
#include "textfilter.h"

static struct vocabulary *
get_vocabulary_object(struct bpapi_vtree_chain* vtree) {
	struct vocabulary* vocab = NULL;
	if (vtree_haskey(vtree, "_vocabulary", "obj", NULL)) {
		vocab = (void*)vtree_get(vtree, "_vocabulary", "obj", NULL);
	}

	return vocab;
}


static void
vocabulary_session_free(struct shadow_vtree *sv) {
	struct vocabulary *vocab = sv->cbdata;
	struct bconf_node *node = sv->vtree.data;

	if (vocab) {
		vocabulary_deinit(vocab);
		free(vocab);
	}

	bconf_free(&node);
}

static int
init_vocabulary_session(const struct bpfilter *filter, struct bpapi *api, int argc, const struct tpvar **argv) {
	struct shadow_vtree *subtree = BPAPI_VTREE_DATA(api);
	struct bconf_node *bnode = NULL;
	struct bpapi_vtree_chain node = {0};
	const char *confpath;

	if (argc != 1)
		goto out;

	TPVAR_STRTMP(confpath, argv[0]);

	if (vtree_haskey(api->vchain.next, "_vocabulary", "obj", NULL))
		goto out; /* No point in recreating. */

	if (!vtree_getnode(api->vchain.next, &node, confpath, NULL))
		goto out;

	struct vocabulary* vocab = xmalloc(sizeof(struct vocabulary));

	vocabulary_init(vocab, api, confpath);
	bconf_add_bindata(&bnode, "_vocabulary.obj", vocab);

	bconf_vtree(&subtree->vtree, bnode);

	subtree->free_cb = vocabulary_session_free;
	subtree->cbdata = vocab;

out:
	return 0;
}

#define vocabulary_session_vtree shadow_vtree

ADD_VTREE_FILTER(vocabulary_session, sizeof(struct shadow_vtree));

struct vocabulary_data {
	struct buf_string buf;
	struct bpapi_vtree_chain *vtree; /* not needed */
	struct vocabulary* vocab;

	char* db;
	char* delim;
	char* count_delim;
	int category;
};

static int
init_vocabulary(const struct bpfilter *filter, struct bpapi *api, int argc, const struct tpvar **argv) {
	struct vocabulary_data *data = BPAPI_OUTPUT_DATA(api);

	data->vtree = &api->vchain;
	data->vocab = get_vocabulary_object(&api->vchain);
	if (!data->vocab) {
		log_printf(LOG_ERR, "(ERROR) %s: Couldn't get object from session.", __FUNCTION__);
		return 1;
	}

	switch (argc) {
		case 4:
			data->category = tpvar_int(argv[3]);
		case 3:
			data->count_delim = tpvar_strdup(argv[2]);
		case 2:
			data->delim = tpvar_strdup(argv[1]);
		case 1:
			data->db = tpvar_strdup(argv[0]);
			break;
	}

	return 0;
}

static void
fini_vocabulary(struct bpapi_output_chain *ochain) {
	struct vocabulary_data *data = ochain->data;

	if (!data->vocab) {
		free(data->buf.buf);
		return;
	}

	if (!data->buf.buf)
		return;

	if (vocabulary_switch_db(data->vocab, data->db ?: "en")) {
		/* Loop over buffer, split on delimiters and pass on to vocabulary */
		char* tok = data->buf.buf;
		char* state = NULL;
		char* countptr = NULL;
		int cnt;
		const char *delim = data->delim && *data->delim ? data->delim : " \n\t";
		const char *count_delim = data->count_delim && *data->count_delim ? data->count_delim : NULL;

		while ((tok = strtok_r(tok, delim, &state)) != NULL) {
			/* Handle case where a count is specified */
			if (count_delim && (countptr = strchr(tok, *(count_delim))) != NULL) {
				*countptr++ = '\0';
				if (countptr[0]) {
					cnt = atoi(countptr);
					if (cnt) {
						cnt = vocabulary_insert(data->vocab, tok, cnt, data->category);
					} else {
						cnt = vocabulary_set_count(data->vocab, tok, cnt);
					}
				}
			} else if (*tok) {
				cnt = vocabulary_insert(data->vocab, tok, 1, data->category);
			}
			tok = NULL;
		}
	}

	free(data->count_delim);
	free(data->delim);
	free(data->db);
	free(data->buf.buf);
}

const struct bpapi_output vocabulary_output = {
	bufstring_outstring_fmt,
	bufstring_outstring_raw,
	fini_vocabulary
};

ADD_OUTPUT_FILTER(vocabulary, sizeof(struct vocabulary_data));

struct vocabulary_store_data {
	struct bconf_node *node;
};

static void
fini_vocabulary_store(struct patch_data *patch) {
	struct vocabulary_store_data *data = patch->user_data;
	bconf_free(&data->node);
	free(data);
}

static int
init_vocabulary_store(const struct bpfilter *filter, struct bpapi *api, int argc, const struct tpvar **argv) {
	struct patch_data *patch = BPAPI_SIMPLEVARS_DATA(api);
	struct shadow_vtree *subtree = BPAPI_VTREE_DATA(api);
	struct vocabulary_store_data *data = zmalloc(sizeof(struct vocabulary_store_data));

	const char *nodename = "vocstats";

	switch (argc) {
		case 1:
			TPVAR_STRTMP(nodename, argv[0]);
	}
	struct vocabulary* vocab = get_vocabulary_object(&api->vchain);
	if (vocab) {
		patchvars_init(patch, "vocabulary_store", "" /*prefix*/, 0 /*prefix len*/, fini_vocabulary_store, data);

		vocabulary_store(vocab, nodename, &data->node);
	}

	bconf_vtree(&subtree->vtree, data->node);

	return 0;
}

ADD_FILTER(vocabulary_store, NULL, 0, &patch_simplevars, sizeof(struct patch_data), &shadow_vtree, sizeof (struct shadow_vtree));

struct vocabulary_filter_data {
	struct buf_string buf;
	struct textfilter_filter_conf tfc;
	int utf8;
};

static void
fini_vocabulary_filter(struct bpapi_output_chain *ochain) {
	struct vocabulary_filter_data *data = ochain->data;

	if (data && data->buf.pos > 0 && data->buf.buf[0]) {
		/* The textfilters should never expand the buffer, but we need room for a zero-terminator */
		char* buffer = xmalloc(data->buf.pos + 1);
		int outlen = 0;

		if (data->utf8)
			outlen = textfilter_utf8(data->buf.buf, data->buf.pos, buffer, &data->tfc);
		else
			outlen = textfilter_latin(data->buf.buf, data->buf.pos, buffer, &data->tfc);

		if (outlen)
			bpo_outstring_raw(ochain->next, buffer, outlen);

		free(buffer);
		free(data->buf.buf);
	}
}

static int
init_vocabulary_filter(const struct bpfilter *filter, struct bpapi *api, int argc, const struct tpvar **argv) {
	struct vocabulary_filter_data *data = BPAPI_OUTPUT_DATA(api);

	/* Some sensible defaults */
	data->utf8 = 0;
	data->tfc.min_len = 3;
	data->tfc.max_len = 25;
	const char* db = NULL;

	switch (argc) {
		case 2:
			data->utf8 = tpvar_int(argv[1]);
		case 1:
			TPVAR_STRTMP(db, argv[0]);
	}

	/* If we've been supplied a db, we can try to switch to it and get its config */
	if (db) {
		struct vocabulary* vocab = get_vocabulary_object(&api->vchain);
		if (vocab && vocabulary_switch_db(vocab, db)) {
			struct bpapi_vtree_chain* vc = vocabulary_get_conf(vocab);
			if (vc) {
				if (vtree_getint(vc, "utf8", NULL))
					data->utf8 = 1;
				int minl = vtree_getint(vc, "min_word_len", NULL);
				int maxl = vtree_getint(vc, "max_word_len", NULL);
				if (minl > 0)
					data->tfc.min_len = minl;
				if (maxl > 0)
					data->tfc.max_len = maxl;
			}
		}
	}

	return 0;
}

struct bpapi_output vocabulary_filter_output = {
	bufstring_outstring_fmt,
	bufstring_outstring_raw,
	fini_vocabulary_filter,
	NULL
};

ADD_OUTPUT_FILTER(vocabulary_filter, sizeof(struct vocabulary_filter_data));

