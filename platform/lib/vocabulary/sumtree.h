#ifndef PLATFORM_SUMTREE_H
#define PLATFORM_SUMTREE_H

#ifdef __cplusplus
extern "C" {
#endif

#define ST_SERIALIZED_ID0 { 'S','U','M','T','R','E','E','0' }

struct sumtree_header {
	char	id[8];
	uint32_t size;		/* Size of this header */
	uint32_t sumtreesize;	/* Size of static sumtree */
	uint32_t ptrsize;
	uint32_t dummy;
} __attribute__((packed));

struct sumtree* sumtree_create(int size);
int sumtree_finalize(struct sumtree* st, int max_level);
void sumtree_free(struct sumtree* st);

void sumtree_insert(struct sumtree* st, int cat, int parent);

int sumtree_add(struct sumtree* st, uint32_t* sums, int cat, int score);
int sumtree_get(struct sumtree* st, uint32_t* sums, int cat);

int sumtree_cat(struct sumtree* st, int idx);

int sumtree_size(struct sumtree* st);
int sumtree_map_size(struct sumtree* st);

int sumtree_serialize(struct sumtree* st, FILE* f);
struct sumtree* sumtree_deserialize(FILE* f);

#ifdef __cplusplus
}
#endif

#endif

