#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "unicode.h"
#include "util.h"

#include "bursttrie_serialized.h"
#include "levs.h"

#define LEVD(levs, y, x) ((levs)->d[((levs)->wlen + 1) * (y) + (x)])

/*
TODO:
	Fold lev_cuted into lev_update if it makes sense.

Matrix Format:
	i n p u t w o r d
 c  0 1 2 3 4 5 6 7 8
 a  1
 n  2
 d  3
 i  4
 d  5
 .  6
 .  .

*/

static inline int max2(int a, int b) {
	if (a < b) a = b;
	return a;
}

static inline int min2(int a, int b) {
	if (a > b) a = b;
	return a;
}

static inline int min3(int a, int b, int c) {
	if (a > b) a = b;
	if (a > c) a = c;
	return a;
}

static int* alloc_init_table(int w, int h) {

	int *d = zmalloc(h * w * sizeof(*d));

	for (int i=0 ; i < h ; ++i) {
		d[i * w + 0] = i; /* setup vertical at column 0 */
	}
	for (int j=0 ; j < w ; ++j)
		d[0 * w + j] = j; /* setup horizontal at row 0 */

	return d;
}

static void print_table(int* tbl, int w, int h) {
	for (int i=0 ; i < h ; ++i) {
		for (int j=0 ; j < w ; ++j) {
			printf("\t%d", tbl[i * w + j]);
		}
		printf("\n");
	}

}

struct lev_s {
	int wlen;		// Matrix width (columns) - 1
	int max_rows;

	enum lev_flags flags;

	struct lev_decode_state state;

	int *d;		// Matrix[max_rows][wlen]

	int* dec_word;
	int* dec_candidate;
	char* raw_candidate;

};

struct lev_s* lev_setup(struct lev_s* levs, int max_depth, const char* word, enum lev_flags flags) {
	if (!levs) {
		levs = calloc(1, sizeof(struct lev_s));
	}
	if (!levs)
		return NULL;

	levs->flags = flags;

	if (levs->flags & USE_UTF8) {
		levs->wlen = strlen_utf8(word);
	} else {
		levs->wlen = strlen(word);
	}

	levs->max_rows = max_depth; /* Maximum length of candidate strings in octets. Usually trie's header->max_key_len+1 */

	levs->d = alloc_init_table(levs->wlen+1, levs->max_rows);

	// TODO: Should use same allocation
	levs->dec_word = xmalloc(levs->wlen * sizeof(int));
	levs->dec_candidate = xmalloc(levs->max_rows * sizeof(int));
	levs->raw_candidate = xmalloc(levs->max_rows * 4); // Room for UTF8. Can use *1 for non-UTF8 mode. This string used for output.
	levs->raw_candidate[0] = '\0';

	int i = 0;
	if (levs->flags & USE_UTF8) {
		const char* p = word;
		int ch;
		while ((ch = utf8_char(&p)) != 0) {
			levs->dec_word[i++] = ch;
		}
	} else {
		for (i=0 ; i < levs->wlen ; ++i)
			levs->dec_word[i] = word[i];
	}

	return levs;
}

// Add symbols to the candidate string, but don't recalculate matrix nor decode.
int lev_concat(struct lev_s* levs, unsigned char sym) {
	static const int skiptbl[16] = { 1,1,1,1,1,1,1,1,1,1,1,1,1,1,2,3 };

	levs->raw_candidate[levs->state.raw_i++] = sym;
	levs->raw_candidate[levs->state.raw_i] = '\0';

	// Update symbol_incomplete with the number of outstanding octets to make the current character complete.
	if (levs->flags & USE_UTF8) {
		if (levs->state.symbol_incomplete == 0) {
			if (sym >= 0x80)
				levs->state.symbol_incomplete = skiptbl[sym >> 4];
		} else {
			--levs->state.symbol_incomplete;
		}
	}
	return levs->state.symbol_incomplete;
}

void lev_concat_n(struct lev_s* levs, const char* syms, int nsyms) {
	memcpy(levs->raw_candidate+levs->state.raw_i, syms, nsyms);
	levs->state.raw_i += nsyms;
	levs->raw_candidate[levs->state.raw_i] = '\0';
	levs->state.symbol_incomplete = 0; // We assume that if we're adding n symbols, we know what we're doing.
}

// Recalculate matrix based on current state.
int lev_update(struct lev_s* levs) {
	// decode from raw_candidate[raw_decode_base..raw_i] to dec_candidate[dec_ci++];
	int i;
	int current_row = levs->state.dec_ci;
	if ((levs->flags & USE_UTF8) == 0) {
		for (i=levs->state.raw_decode_base ; levs->raw_candidate[i] != 0 ; ++i) {
			levs->dec_candidate[levs->state.dec_ci++] = levs->raw_candidate[i];
		}
		levs->state.raw_decode_base = i;
	} else {
		const char* p = &levs->raw_candidate[levs->state.raw_decode_base];
		const char* prevp = p;
		int ch;

		while( (ch = utf8_char(&p)) != 0) {
			levs->dec_candidate[levs->state.dec_ci++] = ch;
		}
		levs->state.raw_decode_base += (p - prevp) - 1;
	}

	int lev = 666;
	for (i=current_row+1 ; i < levs->state.dec_ci+1 ; ++i) {
		for (int j=1 ; j < levs->wlen+1 ; ++j) {
			int cost = levs->dec_candidate[i-1] == levs->dec_word[j-1] ? 0 : 1;
			int cdel = LEVD(levs, i - 1, j) + 1;
			int cins = LEVD(levs, i, j - 1) + 1;
			int csub = LEVD(levs, i - 1, j - 1) + cost;
			LEVD(levs, i, j) = min3(cdel, cins, csub);
			// TODO OPT: duplicate loop for this case instead
			if ((levs->flags & USE_RESTRICTED_EDIT_DISTANCE) && i > 1 && j > 1 && (levs->dec_candidate[i-1] == levs->dec_word[j-2]) && (levs->dec_candidate[i-2] == levs->dec_word[j-1])) {
				LEVD(levs, i, j) = min2(LEVD(levs, i, j), LEVD(levs, i - 2, j - 2) + cost);
			}
		}
		lev = LEVD(levs, i, levs->wlen);
	}

	return lev;
}

// This calculates the cut edit-distance on the current matrix.
//
// TODO OPT: Closely related to lev_update, could do it in there instead.
int lev_cuted(struct lev_s* levs, int max_k) {
	// In effect, we scan an interval on the last row of the matrix.
	int clen = levs->state.dec_ci;

	int l = max2(1, clen - max_k);
	int u = min2(levs->wlen, clen + max_k);
	int min_ed = 666;
	for (int i = l ; i <= u && min_ed ; ++i) {
		if (LEVD(levs, clen, i) < min_ed)
			min_ed = LEVD(levs, clen, i);
	}
	return min_ed;
}

inline struct lev_decode_state lev_decode_state_get(struct lev_s* levs) {
	struct lev_decode_state s = levs->state;
	return s;
}

inline void lev_decode_state_set(struct lev_s* levs, struct lev_decode_state* state) {
	levs->state = *state;
	levs->raw_candidate[levs->state.raw_i] = '\0';
}

inline const char* lev_get_candidate(struct lev_s* levs) {
	return levs->raw_candidate;
}

void lev_free(struct lev_s* levs) {
	free(levs->d);
	free(levs->dec_word);
	free(levs->dec_candidate);
	free(levs->raw_candidate);
	free(levs);
}

void lev_debug(struct lev_s* levs) {
	printf("dec_word(%d):\t\t", levs->wlen);
	for (int i=0 ; i < levs->wlen ; ++i)
		printf("%d ", levs->dec_word[i]);
	printf("\n");

	printf("dec_candidate(%d):\t", levs->state.dec_ci);
	for (int i=0 ; i < levs->state.dec_ci ; ++i)
		printf("%d ", levs->dec_candidate[i]);
	printf("\n");

	printf("raw_candidate(%d):\t\"%s\"\n", levs->state.raw_i, levs->raw_candidate);
	printf("\traw_decode_base=%d\n", levs->state.raw_decode_base);

	print_table(levs->d, levs->wlen+1, levs->state.dec_ci+1);
}

