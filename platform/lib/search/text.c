#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <sys/types.h>
#include <regex.h>
#include <unicode/unorm.h>
#include "charmap.h"
#include "synize.h"
#include "util.h"
#include "hunstem.h"
#include "index.h"
#include "bconf.h"
#include "logging.h"
#include "text.h"
#include "langmap.h"

/* These are characters that we don't allow as prefix, as runs of more than one of, nor any at all around a word, within the index */
static const char* edible_delimiter_tbl = ".-:/";

static inline int
is_edible_delimiter(char c) {
	char* delim = strchr(edible_delimiter_tbl, c);
	return delim == NULL ? 0 : *delim;
}

static inline int
push_char(char *word, char c, int *offset) {
	char cm = c;

	if (!cm || cm == '*')
		cm = ' ';

	if (cm == ' ') {
		if (!*offset)
			return 0; 		/* Skip leading space */
		else {
			while (*offset && (word[*offset - 1] == '.' || word[*offset - 1] == ':')) {
				(*offset)--;
			}
			word[*offset] = '\0';
			return 1;
		}
	}

	int delim = is_edible_delimiter(cm);

	if (*offset == 0 && delim)
		return 0;

	if (*offset && delim && word[*offset - 1] == delim)
		return 0;

	word[(*offset)++] = cm;

	return 0;
}

static void
add_word_with_stems(struct add_text_config *atc, void *v, const char *word, int do_synize) {
	char *w = NULL;
	char lmbuf[64];

	if (atc->hun_obj) {
		char **stems = NULL;
		int nstems = 0;
		if (hun_spell_ok(atc->hun_obj, word) && 
		    ((nstems = hun_stem(atc->hun_obj, &stems, word)) > 0)) {
			/* XXX look at more results than the first? */
			size_t wl = strlen(stems[0]) * 2 + 1;

			w = alloca(wl);
			casefold(w, wl, stems[0], atc->cfdata, 0);
			hun_free_list(atc->hun_obj, &stems, nstems);
			if (strcmp(w, word) != 0) {
				(*atc->word_cb)(v, ATW_STEM, w, -1);
			}
		}
	}

	(*atc->word_cb)(v, ATW_WORD, word, -1);
	if (atc->langmap && (w = langmap_lookup(atc->langmap, lmbuf, sizeof(lmbuf), atc->lang, w ?: word))) {
		(*atc->word_cb)(v, ATW_LANGMAP, w, -1);
	}

	if (do_synize && atc->synizer)
		(*atc->word_cb)(v, ATW_SYN, word, -1);		/* XXX - WTF? */
}

struct syn_regex {
	regex_t preg;

	regex_t pwithreg;
	int has_with;

	int match1;
	int match2;
};

struct syn_regexes {
	struct syn_regex *syn_regexes;
	int num_syn_regexes;
};

struct syn_regexes *
init_syn_regexes(struct bconf_node *node) {
	struct syn_regexes *sr;
	int num = bconf_count(node);
	int i;

	sr = xmalloc(sizeof(*sr) + num * sizeof (*sr->syn_regexes));
	sr->syn_regexes = (struct syn_regex *)(sr + 1);
	sr->num_syn_regexes = 0;

	for (i = 0; i < num; i++) {
		struct bconf_node *rnode = bconf_byindex(node, i);
		const char *regex = bconf_get_string(rnode, "regex");
		const char *with = bconf_get_string(rnode, "with");

		log_printf(LOG_DEBUG, "Adding syn_regex %s", regex);

		if (regcomp(&sr->syn_regexes[sr->num_syn_regexes].preg, regex, REG_EXTENDED | REG_ICASE) != 0) {
			log_printf(LOG_CRIT, "Failed to compile syn regex %d: %s", i, regex);
			continue;
		}
		sr->syn_regexes[sr->num_syn_regexes].has_with = (with != NULL);
		if (with && regcomp(&sr->syn_regexes[sr->num_syn_regexes].pwithreg, with, REG_EXTENDED | REG_ICASE) != 0) {
			log_printf(LOG_CRIT, "Failed to compile syn with regex %d: %s", i, with);
			regfree(&sr->syn_regexes[sr->num_syn_regexes].preg);
			continue;
		}

		sr->syn_regexes[sr->num_syn_regexes].match1 = bconf_get_int(rnode, "match1");
		sr->syn_regexes[sr->num_syn_regexes].match2 = bconf_get_int(rnode, "match2");
		sr->num_syn_regexes++;
	}

	return sr;
}

void
free_syn_regexes(struct syn_regexes *sr) {
	int i;

	for (i = 0; i < sr->num_syn_regexes; i++) {
		if (sr->syn_regexes[i].has_with)
			regfree(&sr->syn_regexes[i].pwithreg);
		regfree(&sr->syn_regexes[i].preg);
	}
	free(sr);
}

#define MAX_WORDLEN 100

char *
syn_regex_update(struct syn_regexes *sr, const char *text, void *cfdata, void (*match_cb)(const char *word, int match, void *cbarg), void *cbarg) {
	char *res = NULL;
	char word[MAX_WORDLEN + 2];
	char folded[MAX_WORDLEN * 2 + 2];
	int i;

	if (sr == NULL)
		return NULL;

	for (i = 0; i < sr->num_syn_regexes; i++) {
		regmatch_t matches[10];

		if (sr->syn_regexes[i].has_with && regexec(&sr->syn_regexes[i].pwithreg, text, 0, NULL, 0) != 0)
			continue;
		if (regexec(&sr->syn_regexes[i].preg, text, 10, matches, 0) == 0) {
			regmatch_t orig = matches[0];
			regmatch_t match1 = matches[sr->syn_regexes[i].match1 ?: 1];
			regmatch_t match2 = matches[sr->syn_regexes[i].match2 ?: 2];

			/* Ignore empty matches */
			if (match1.rm_so == -1 || match2.rm_so == -1 || text[match2.rm_so] == ' ' || text[match2.rm_so] == '\0')
				continue;

			log_printf(LOG_DEBUG, "Syn regexing %.*s => %.*s%.*s (%.*s, %.*s)", (int)(orig.rm_eo - orig.rm_so), text + orig.rm_so, (int)(match1.rm_eo - match1.rm_so), text + match1.rm_so, (int)(match2.rm_eo - match2.rm_so), text + match2.rm_so, (int)(match1.rm_eo - match1.rm_so), text + match1.rm_so, (int)(match2.rm_eo - match2.rm_so), text + match2.rm_so);

			if (match_cb && match1.rm_eo - match1.rm_so <= MAX_WORDLEN) {
				memcpy(word, text + match1.rm_so, match1.rm_eo - match1.rm_so);
				word[match1.rm_eo - match1.rm_so] = '\0';
				casefold(folded, sizeof (folded), word, cfdata, 0);

				match_cb(folded, 1, cbarg);
			}
			if (match_cb && match2.rm_eo - match2.rm_so <= MAX_WORDLEN) {
				memcpy(word, text + match2.rm_so, match2.rm_eo - match2.rm_so);
				word[match2.rm_eo - match2.rm_so] = '\0';
				casefold(folded, sizeof (folded), word, cfdata, 0);

				match_cb(folded, 2, cbarg);
			}

			if (match1.rm_eo == match2.rm_so && orig.rm_eo - orig.rm_so == match2.rm_eo - match1.rm_so) {
				/* Orig and matches are the same. */
			} else {
				if (!res)
					text = res = xstrdup(text);
				/*
				if (match1.rm_so != orig.rm_so) {
					memmove(res + orig.rm_so, res + match1.rm_so, match1.rm_eo - match1.rm_so);
					match1.rm_eo = orig.rm_so + match1.rm_eo - match1.rm_so;
				}
				*/
				memmove(res + match1.rm_eo, res + match2.rm_so, match2.rm_eo - match2.rm_so);
				match1.rm_eo = match1.rm_eo + match2.rm_eo - match2.rm_so;
				memmove(res + match1.rm_eo, res + match2.rm_eo, strlen(res + match2.rm_eo) + 1);
			}
		}
	}
	return res;
}

struct sruaw {
	void *v;
	struct add_text_config *atc;
};

static void
syn_regex_update_add_word(const char *word, int match, void *cbarg) {
	struct sruaw *s = cbarg;
	log_printf(LOG_DEBUG, "Adding match%d: %s", match, word);
	(*s->atc->word_cb)(s->v, ATW_SYN, word, -1);
}

static int
normalize_word(struct add_text_config *atc, char **wordp, int *word_len, size_t buf_size) {
	char *word = *wordp;
	int offset = *word_len;

	/* Clean up tail of word by removing unwanted crud from it, specifically '-' */
	while (offset > 0 && is_edible_delimiter(word[offset-1])) {
		word[--offset] = '\0';
	}

	/* If first char is quote, then remove it and trailing. */
	if (word[0] == '"') {
		while (offset > 0 && word[offset-1] == '"') {
			word[--offset] = '\0';
		}
		++word;
		--offset;
	} else {
		/* Allow trailing quote only before digit (for 'inch') */
		while (offset > 1 && word[offset-1] == '"' && !isdigit(word[offset-2])) {
			word[--offset] = '\0';
		}
	}

	if (offset < atc->min_wordlen || offset > atc->max_wordlen) {
		return -1;
	}

	*word_len = offset;
	*wordp = word;

	return 0;
}

struct at_secondary_cb {
	const char *(*cb)(void *, int, const char *, int);
	void *v;
};

static const char *
add_secondary_word(void *v, int at, const char *w, int wl) {
	struct at_secondary_cb *s = v;
	return (*s->cb)(s->v, at|ATW_SECONDARY, w, wl);
}

void
split_text(struct add_text_config *atc, void *v, const char *text) {
	char *tmp;
	int offset = 0;
	char word_buf[atc->max_wordlen + 2];
	int res;
	int done = 0;
	TCLIST *wordlist = NULL;

	if (!text)
		return;

	if (atc->synizer)
		wordlist = tclistnew();

	tmp = syn_regex_update(atc->syn_regexes, text, atc->cfdata, syn_regex_update_add_word, &((struct sruaw){ v, atc }));
	if (tmp) {
		size_t tmpsz = strlen(tmp) * 2 + 1;
		char *tmp2;

		tmp2 = xmalloc(tmpsz);
		casefold(tmp2, tmpsz, tmp, atc->cfdata, 0);
		free(tmp);
		tmp = tmp2;
	} else {
		size_t tmpsz = strlen(text) * 2 + 1;

		tmp = xmalloc(tmpsz);
		casefold(tmp, tmpsz, text, atc->cfdata, 0);
	}
	text = tmp;

	while (!done) {
		if (!*tmp)
			done = 1;
		res = push_char(word_buf, *tmp, &offset);
		if (res == 0 && offset == atc->max_wordlen + 1) {
			/* Word too long, skip until next space */
			while (*tmp && *tmp != ' ')
				tmp++;
			offset = 0;
			continue;
		}

		if (res == 1) {
			char *word = word_buf;

			if (normalize_word(atc, &word, &offset, sizeof(word_buf)) == 0) {
				/* Add synonyms */
				if (atc->synizer)
					tclistpush2(wordlist, word);

				add_word_with_stems(atc, v, word, 0);

				if (strpbrk(word, edible_delimiter_tbl)) {
					char *tmp_word = xstrdup(word);
					char *tmp = tmp_word;
					while (*tmp) {
						if (is_edible_delimiter(*tmp))
							*tmp = ' ';
						tmp++;
					}
					{
						/* We wrap the callback with our own callback that sets the ATW_SECONDARY flag. */
						struct add_text_config natc = *atc;
						struct at_secondary_cb nv = { atc->word_cb, v };
						natc.word_cb = add_secondary_word;
						split_text(&natc, &nv, tmp_word);
					}
					free(tmp_word);
				}

				(*atc->word_cb)(v, ATW_NEXT, NULL, -1);
			}
			offset = 0;
		}

		tmp++;
	}

	if (atc->synizer) {
		synize(atc, v, wordlist, atc->synizer);

		tclistdel(wordlist);
	}

	free((char*)text);
}

struct doc_add_word_data {
	struct add_text_config *atc;
	struct document *doc;
	struct docpos *dpos;
};

const char *
doc_atc_add_word(void *v, int at, const char *w, int wl) {
	struct doc_add_word_data *da = v;

	switch (at & ~(ATW_SECONDARY)) {
	case ATW_WORD:
	case ATW_STEM:
		if (!(at & ATW_SECONDARY)) {
			doc_add_word(da->doc, w, wl, da->dpos);
			break;
		}
		/* Yes, this is a fallthrough. XXX - But why? We could index the parts of the words at the same position. */
	case ATW_SYN:
	case ATW_LANGMAP:
		doc_add_word(da->doc, w, wl, NULL);
		break;
	case ATW_SYNIZE:
		add_word_with_stems(da->atc, v, w, 1);
		break;
	case ATW_NEXT:
		/* Secondary words don't cause the pos to advance. */
		if (da->dpos && !(at & ATW_SECONDARY))
			da->dpos->pos++;
		break;
	case ATW_Q_ATTR:
		return tcmapget2(da->doc->attrs_printed, w);
	case ATW_Q_WORD_EXISTS:
		return tcmapget2(da->doc->words, w);
	}
	return NULL;
}

/* Normalize and add text. */
void
add_text(struct add_text_config *atc, struct document *doc, const char *text, struct docpos *dpos) {
	struct add_text_config natc = *atc;
	natc.word_cb = doc_atc_add_word;
	split_text(&natc, &((struct doc_add_word_data){ &natc, doc, dpos}), text);
}

