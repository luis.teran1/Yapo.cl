#include "index.h"
#include "util.h"
#include "logging.h"
#include <pthread.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sha1.h>
#include <assert.h>
#include <limits.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <inttypes.h>

#include "avl.h"
#include "strl.h"
#include "bconf.h"
#include "json_vtree.h"
#include "vtree.h"
#include "index_merge.h"
#include "xxhash.h"

/*
 * body-index
 * inverse index for words -> documents
 * inverse index for subs -> words (for substring matching)
 * attribute indicies
 */

int index_regress_fail_fsync;

static void
db_add_attribute(struct index_db *db, const char *att) {
	int nattrs = bconf_count(bconf_vget(db->meta, "attr", "order", NULL));
	char nbuf[16];

	snprintf(nbuf, sizeof(nbuf), "%d", nattrs);

	bconf_add_datav(&db->meta, 3, (const char *[]){ "attr", "name", att }, nbuf, BCONF_DUP);
	bconf_add_datav(&db->meta, 3, (const char *[]){ "attr", "order", nbuf }, att, BCONF_DUP);
}

static void
load_doc_header(struct index_db *db) {
	struct bconf_node *attrs, *bn;
	int nsize = 0;
	int offs;
	int i;

	attrs = bconf_vget(db->meta, "attr", "order", NULL);

	for (i = 0; (bn = bconf_byindex(attrs, i)) != NULL; i++) {
		nsize += strlen(bconf_value(bn)) + 1;
	}
	if (nsize == 0) {
		return;
	}
	db->header = xmalloc(nsize);

	for (offs = 0, i = 0 ; (bn = bconf_byindex(attrs, i)) != NULL; i++) {
		const char *attr  = bconf_value(bn);
		int al = strlen(attr);
	
		memcpy(&db->header[offs], attr, al);
		db->header[offs + al] = '\t';
		offs += al + 1;
	}
	db->header[offs - 1] = '\0';
}

const char *
index_get_doc_header(struct index_db *db) {
	return db->header;
}

static void
cache_config_header(struct index_db *db) {
	struct buf_string bs = {0};
	struct bconf_node *n, *cn;
	int i;

	/*
	 * XXX - All this is highly search engine specific and belongs in the search engine.
	 */

	bscat(&bs, "conf:footprint=%s\n", index_get_conf_footprint(db));

	cn = bconf_vget(db->meta, "conf", NULL);
	for (i = 0; (n = bconf_byindex(cn, i)) != NULL; i++) {
		bscat(&bs, "conf:%s=%s\n", bconf_key(n), bconf_value(n));
	}

	db->conf_header = bs.buf;
	db->conf_size = bs.pos;
}

const char *
index_get_config(struct index_db *db, size_t *sz) {
	if (!db->conf_size)
		cache_config_header(db);
	*sz = db->conf_size;
	return db->conf_header;
}

void
index_debug_set_conf_rev(struct index_db *db, const char *debug_conf_rev) {
	free(db->conf_footprint);
	db->conf_footprint = strdup(debug_conf_rev);

	free(db->conf_header);
	db->conf_header = NULL;
	db->conf_size = 0;
}

const char *
index_get_to_id(struct index_db *db) {
	return bconf_get_string(db->meta, "state.to_id");
}

void
index_set_to_id(struct index_db *db, const char *id) {
	if (id)
		bconf_add_datav(&db->meta, 2, (const char *[]){"state", "to_id"}, id, BCONF_DUP);
}

const char *
index_get_from_id(struct index_db *db) {
	return bconf_get_string(db->meta, "state.from_id");
}

void
index_set_from_id(struct index_db *db, const char *id) {
	if (id)
		bconf_add_datav(&db->meta, 2, (const char *[]){"state", "from_id"}, id, BCONF_DUP);
}

const char *
index_get_timestamp(struct index_db *db) {
	return bconf_get_string(db->meta, "state.timestamp");
}

static void
index_set_timestamps(struct index_db *db, const char *ts) {
	bconf_add_datav(&db->meta, 2, (const char *[]){"state", "timestamp"}, ts, BCONF_DUP);
}

void
index_set_timestamp(struct index_db *db, time_t ts) {
	char timestamp[32];

	snprintf(timestamp, sizeof (timestamp), "%ld", (long)ts);
	index_set_timestamps(db, timestamp);
}

static int
store_attrs_printed(struct index_db *db, struct document *doc) {
	const char *attr;
	const char *val;
	int bsize = 0;
	struct bconf_node *att = bconf_vget(db->meta, "attr", "order", NULL);
	char *body;
	int ndattr;
	int boffs;
	int res;
	int al;
	int vl;
	int i;

	/*
	 * Calculate the size of the body, including the \t between elements.
	 */
	ndattr = 0;
	tcmapiterinit(doc->attrs_printed);
	while ((attr = tcmapiternext(doc->attrs_printed, &al)) != NULL) {
		tcmapiterval(attr, &vl);
		bsize += vl + 1;
		ndattr++;
	}

	bsize += bconf_count(att);

	body = alloca(bsize);
	boffs = 0;
	for (i = 0; i < bconf_count(att); i++) {
		attr = bconf_value(bconf_byindex(att, i));
		al = strlen(attr);

		if ((val = tcmapget(doc->attrs_printed, attr, al, &vl)) != NULL) {
			memcpy(&body[boffs], val, vl);
			boffs += vl;
		}
		body[boffs] = '\t';
		boffs++;

		/*
		 * Notice that this corrupts the doc and the only reasonable
		 * thing to do after store_doc() is close_doc().
		 */
		tcmapout(doc->attrs_printed, attr, al);
	}

	/* Check for new attributes */
	if (tcmaprnum(doc->attrs_printed)) {
		tcmapiterinit(doc->attrs_printed);
		while ((attr = tcmapiternext(doc->attrs_printed, &al)) != NULL) {
			const char *v = tcmapiterval(attr, &vl);

			memcpy(&body[boffs], v, vl);
			body[boffs + vl] = '\t';
			boffs += vl + 1;

			db_add_attribute(db, attr);
		}
	}

	if (boffs > 0)
		body[boffs - 1] = '\0';

	res = index_blob_store_doc(db, &doc->id, body, boffs, &doc->docoff);

	return res;
}

struct _invword {
	int32_t docoff;
	int npos;
	struct docpos *posarr;
};

struct word_indexed {
	struct avl_node avl;
	char *word;
	struct _invword *docs;
	int ndocs;
	int nalloc;
};

static int
wi_cmp(const struct avl_node *aa, const struct avl_node *ab) {
	struct word_indexed *a = avl_data(aa, struct word_indexed, avl);
	struct word_indexed *b = avl_data(ab, struct word_indexed, avl);
	return strcmp(a->word, b->word);
}

static void
wi_free(struct avl_node *aa) {
	struct word_indexed *w; 
	if (aa == NULL)
		return;
	wi_free(aa->link[0]);
	wi_free(aa->link[1]);
	w = avl_data(aa, struct word_indexed, avl);
	free(w->word);
	free(w->docs);
	free(w);
}

static int
store_words(struct index_db *db, struct document *doc) {
	const struct docpos *posarr;
	const char *word;
	int poslen;
	int wl;

	/*log_printf(LOG_DEBUG, "store_words %p", doc->words);*/
	if (!doc->words)
		return 0;

	tcmapiterinit(doc->words);
	while ((word = tcmapiternext(doc->words, &wl)) != NULL) {
		struct avl_node *aword;
		struct word_indexed *w;
		struct word_indexed s;
		struct _invword *iw;

		/*log_printf(LOG_DEBUG, "store_words (%s)", word);*/

		if (index_is_stopword(db, word))
			continue;

		s.word = (char *)word;
		if ((aword = avl_lookup(&s.avl, &db->invword_docs, wi_cmp)) != NULL) {
			w = avl_data(aword, struct word_indexed, avl);
		} else {
			w = xmalloc(sizeof(*w));
			w->word = xstrdup(word);
			w->nalloc = 4;
			w->docs = zmalloc(sizeof(*w->docs) * w->nalloc);
			w->ndocs = 0;
			avl_insert(&w->avl, &db->invword_docs, wi_cmp);
			db->ninvword_docs++;
		}
		if (w->ndocs == w->nalloc) {
			w->nalloc *= 2;
			w->docs = xrealloc(w->docs, sizeof(*w->docs) * w->nalloc);
		}
		iw = &w->docs[w->ndocs++];

		posarr = (const struct docpos *)tcmapiterval(word, &poslen);

		iw->docoff = doc->docoff;
		iw->npos = poslen / sizeof(struct docpos);
		iw->posarr = xmalloc(poslen);
		memcpy(iw->posarr, posarr, poslen);
	}

	return 0;
}

struct attr_indexed {
	struct avl_node avl;
	char *attr;
	int32_t *docs;
	int ndocs;
	int nalloc;
};

static int
ai_cmp(const struct avl_node *aa, const struct avl_node *ab) {
	struct attr_indexed *a = avl_data(aa, struct attr_indexed, avl);
	struct attr_indexed *b = avl_data(ab, struct attr_indexed, avl);
	return strcmp(a->attr, b->attr);
}

static void
ai_free(struct avl_node *aa) {
	struct attr_indexed *a;
	if (aa == NULL)
		return;
	ai_free(aa->link[0]);
	ai_free(aa->link[1]);
	a = avl_data(aa, struct attr_indexed, avl);
	free(a->attr);
	free(a->docs);
	free(a);
}

static int
store_attrs_indexed(struct index_db *db, struct document *doc) {
	const char *word;
	int wl;
	int num;
	int i;

	if (!doc->attrs_indexed)
		return 0;

	num = tclistnum(doc->attrs_indexed);
	for (i = 0 ; i < num ; i++) {
		struct avl_node *aattr;
		struct attr_indexed *attr;
		struct attr_indexed s;

		word = tclistval(doc->attrs_indexed, i, &wl);

		/* Doubt this check is actually needed. */
		if (index_is_stopword(db, word))
			continue;

		s.attr = (char *)word;

		if ((aattr = avl_lookup(&s.avl, &db->invattr_docs, ai_cmp)) != NULL) {
			attr = avl_data(aattr, struct attr_indexed, avl);
		} else {
			attr = xmalloc(sizeof(*attr));
			attr->attr = xstrdup(word);
			attr->nalloc = 4;
			attr->docs = zmalloc(sizeof(*attr->docs) * attr->nalloc);
			attr->ndocs = 0;
			avl_insert(&attr->avl, &db->invattr_docs, ai_cmp);
			db->ninvattr_docs++;
		}

		if (attr->ndocs == attr->nalloc) {
			attr->nalloc *= 2;
			attr->docs = xrealloc(attr->docs, sizeof(*attr->docs) * attr->nalloc);
		}
		if (attr->ndocs > 0 && attr->docs[attr->ndocs - 1] == doc->docoff) {
			log_printf(LOG_WARNING, "duplicate indexed attribute: [%s] in document id: %d", word, doc->id.id);
		} else {
			attr->docs[attr->ndocs++] = doc->docoff;
		}
	}

	return 0;
}

struct blob_writer {
	char *map;
	size_t mapsz;
	size_t mapoff;

	int fd;
	off_t offset;
};

static int
b_open(struct blob_writer *bw, int fd, off_t offset) {
	/* Lazy error handling in another place in the code */
	if (offset < 0)
		return -1;

	bw->mapsz = 256*1024*1024;
	bw->mapoff = 0;
	bw->fd = fd;
	bw->offset = offset;

	if (ftruncate(fd, bw->offset + bw->mapsz))
		return -1;
	if ((bw->map = mmap(NULL, bw->mapsz, PROT_READ|PROT_WRITE, MAP_FILE|MAP_SHARED, fd, offset)) == MAP_FAILED)
		return -1;
	return 0;
}

static off_t
b_close(struct blob_writer *bw) {
	if (bw->map && bw->map != MAP_FAILED) {
		if (munmap(bw->map, bw->mapsz))
			return -1;
	}
	if (bw->fd > 0) {
		if (ftruncate(bw->fd, bw->offset + bw->mapoff))
			return -1;
	}
	return bw->offset + bw->mapoff;
}

static off_t
b_write(struct blob_writer *bw, const void *d, size_t sz) {
	size_t resid = sz;
	const char *cd = d;
	off_t ret = bw->offset + bw->mapoff;

	while (resid > 0) {
		size_t tsz = resid;
		if (tsz > bw->mapsz - bw->mapoff)
			tsz = bw->mapsz - bw->mapoff;
		memmove(bw->map + bw->mapoff, cd, tsz);
		cd += tsz;
		resid -= tsz;
		bw->mapoff += tsz;
		if (resid > 0) {
			if (b_open(bw, bw->fd, b_close(bw)) == -1)
				return -1;
		}
	}

	return ret;
}

static off_t
b_align2(struct blob_writer *bw, size_t align) {
	char zeros[align];
	off_t al = ((bw->mapoff + (align - 1)) & ~(align - 1)) - bw->mapoff;

	memset(zeros, 0, align);
	return b_write(bw, zeros, al);
}

struct docpos_collector {
	uint32_t offset;
	struct avl_node *tree;
};

struct docpos_el {
	struct avl_node t;
	uint32_t posoffs;
	struct docpos pos[0];
};

static int
dc_cmp(const struct avl_node *an, const struct avl_node *bn) {
	struct docpos_el *a = avl_data(an, struct docpos_el, t);
	struct docpos_el *b = avl_data(bn, struct docpos_el, t);
	if (a->pos[0].pos != b->pos[0].pos)
		return a->pos[0].pos - b->pos[0].pos;
	return memcmp(&a->pos[1], &b->pos[1], a->pos[0].pos * sizeof(a->pos[1]));
}

static struct docpos_collector *
dc_init(void) {
	return zmalloc(sizeof(struct docpos_collector));
}

static int
dc_clean(const struct docpos *src, struct docpos *dst) {
	int total_boost = 0;
	int i, cpi;

	/*
	 * For bm25, we calculate sum of all boosts and put it in the first (fake) pos.
	 */

	for (cpi = 1, i = 1; i <= src[0].pos; i++) {
		total_boost += src[i].rel_boost;
		if (src[i].pos == (uint16_t)-1)
			continue;
		dst[cpi++] = src[i];
	}
	dst[0].pos = cpi - 1;
	dst[0].flags = 0;
	if (total_boost > UINT16_MAX)
		total_boost = UINT16_MAX;
	dst[0].rel_boost = total_boost;
	return cpi - 1;
}

static void
dc_collect(struct docpos_collector *dc, struct docindex *docs, int ndocs, const struct docpos *d, int nd) {
	int i;

	for (i = 0; i < ndocs; i++) {
		if (docs[i].ptr != INDEX_DOCINDEX_NO_POS) {
			const struct docpos *dp = &d[docs[i].ptr];
			const struct docpos_el *s = (const struct docpos_el *)(const void*)(((char *)dp) - offsetof(struct docpos_el, pos));	/* I'm going to burn in hell. */
			struct docpos cleanpos[dp[0].pos + 1];
			struct avl_node *eln;
			struct docpos_el *el;

			if (dc_clean(dp, cleanpos) == 0) {
				docs[i].ptr = INDEX_DOCINDEX_NO_POS;
				continue;
			}

			dp = cleanpos;

			if ((eln = avl_lookup(&s->t, &dc->tree, dc_cmp)) == NULL) {
				el = zmalloc(sizeof(*el) + (dp[0].pos + 1) * sizeof(el->pos[0]));
				el->posoffs = dc->offset;
				if (dc->offset + dp[0].pos + 1 < dc->offset)
					xerrx(1, "docpos overflow, you need to bump the size of posptrs to 64 bits and change the relevant functions to deal with it");
				dc->offset += dp[0].pos + 1;
				memcpy(&el->pos, dp, (dp[0].pos + 1) * sizeof(*dp));
				avl_insert(&el->t, &dc->tree, dc_cmp);
			} else {
			       	el = avl_data(eln, struct docpos_el, t);
			}
			docs[i].ptr = el->posoffs;
		}
	}
}

static void
dc_foldrec(struct avl_node *eln, struct docpos *d) {
	struct docpos_el *el;
	struct avl_node *l, *r;

	if (eln == NULL)
		return;
	el = avl_data(eln, struct docpos_el, t);
	memcpy(&d[el->posoffs], &el->pos[0], (el->pos[0].pos + 1) * sizeof(*d));
	l = eln->link[0];
	r = eln->link[1];
	free(el);
	dc_foldrec(l, d);
	dc_foldrec(r, d);
}

static struct docpos *
dc_fold(struct docpos_collector *dc, unsigned int *npos) {
	/* XXX - this can be done nicer, by a direct mmap of the blob */
	struct docpos *ret = xmalloc(dc->offset * sizeof(*ret));

	*npos = dc->offset;
	dc_foldrec(dc->tree, ret);
	return ret;
}

int
index_blob_store_doc(struct index_db *db, const struct doc *docid, const char *body, size_t bsz, int32_t *out_off) {
	struct ib_document *doc;

	if (db->b_docs_off == db->b_docs_sz) {
		if (db->b_docs_sz == 0) {
			db->b_docs_sz = 4096;
			db->b_docs = xmalloc(sizeof(*db->b_docs) * db->b_docs_sz);
		} else {
			db->b_docs_sz *= 2;
			db->b_docs = xrealloc(db->b_docs, sizeof(*db->b_docs) * db->b_docs_sz);
		}
	}

	*out_off = db->b_docs_off;
	doc = &db->b_docs[db->b_docs_off++];

	if (docid->suborder < 0)
		db->b_nsubdocs++;

	memcpy(&doc->id, docid, sizeof(*db->b_docs));
	doc->doclen = bsz;
	if ((doc->blob_offs = b_write(db->bw, body, bsz)) == (u_int64_t)-1)
		return -1;
	return 0;
}

static int
ptrs_cmp(const void *a, const void *b) {
	struct ib_document * const *da = a, * const *db = b;
	struct ib_doc *ida = &(**da).id;
	struct ib_doc *idb = &(**db).id;

	return index_doc_cmp(ida, idb);
}

int
index_blob_sync_docs(struct index_db *db, int flags) {

	if (b_align2(db->bw, getpagesize()) == -1)
		return -1;

	if ((db->b_hdr.documents_off = b_write(db->bw, NULL, 0)) == (uint64_t)-1)
		return -1;

	if ((flags & INDEX_CLOSE_SKIP_SORT) == 0) {
		unsigned int i;
		struct timer_instance *ti = timer_start(db->ti, "docs_sort");

		/*
		 * In addition to sorting, we have to build a map from the old index to the new.
		 * The map is used to convert the offset of invwords and invattrs.
		 * For this to work, we sort pointers to the data, then copy directly into
		 * file mmap.
		 */
		struct ib_document **ptrs = xmalloc(db->b_docs_off * sizeof(*ptrs));
		for (i = 0; i < db->b_docs_off ; i++)
			ptrs[i] = db->b_docs + i;
		qsort(ptrs, db->b_docs_off, sizeof(*ptrs), ptrs_cmp);

		db->docoff_map = xmalloc(db->b_docs_off * sizeof(*db->docoff_map));

		for (i = 0; i < db->b_docs_off ; i++) {
			int old_off = ptrs[i] - db->b_docs;

			db->docoff_map[old_off] = i;
			if (b_write(db->bw, ptrs[i], sizeof(**ptrs)) == (off_t)-1)
				return -1;
		}

		free(ptrs);

		timer_end(ti, NULL);
	} else if (b_write(db->bw, db->b_docs, db->b_docs_off * sizeof(*db->b_docs)) == (off_t)-1)
		return -1;

	db->b_hdr.ndocuments = db->b_docs_off;
	db->b_hdr.nsubdocs = db->b_nsubdocs;
	free(db->b_docs);
	db->b_docs_sz = 0;
	db->b_docs_off = 0;
	db->b_docs = NULL;
	db->b_nsubdocs = 0;

	return 0;
}

int
index_blob_store_attr(struct index_db *db, const char *attr, int attrsz, const int32_t *docarr, int ndocs) {
	struct ib_invattr *at;

	if (db->b_attrs_off == db->b_attrs_sz) {
		if (db->b_attrs_sz == 0) {
			db->b_attrs_sz = 4096;
			db->b_attrs = xmalloc(sizeof(*db->b_attrs) * db->b_attrs_sz);
		} else {
			db->b_attrs_sz *= 2;
			db->b_attrs = xrealloc(db->b_attrs, sizeof(*db->b_attrs) * db->b_attrs_sz);
		}
	}

	at = &db->b_attrs[db->b_attrs_off++];
	if ((at->attr_offs = b_write(db->bw, attr, attrsz)) == (uint64_t)-1)
		return -1;
	if (b_align2(db->bw, sizeof(int32_t)) == -1)
		return -1;
	at->docslen = ndocs * sizeof(*docarr);
	if ((at->docs_offs = b_write(db->bw, docarr, ndocs * sizeof(*docarr))) == (uint64_t)-1)
		return -1;

	return 0;
}

int
index_blob_sync_attrs(struct index_db *db) {
	if (b_align2(db->bw, getpagesize()) == -1)
		return -1;
	if ((db->b_hdr.invattrs_off = b_write(db->bw, db->b_attrs, db->b_attrs_off * sizeof(*db->b_attrs))) == (uint64_t)-1)
		return -1;
	db->b_hdr.ninvattrs = db->b_attrs_off;
	free(db->b_attrs);
	db->b_attrs_sz = 0;
	db->b_attrs_off = 0;
	db->b_attrs = NULL;

	return 0;
}

int
index_blob_store_word(struct index_db *db, const char *word, int wordsz, struct docindex *docarr, int ndocs, const struct docpos *docpos, int ndocpos) {
	struct ib_invword *w;

	if (db->b_words_off == db->b_words_sz) {
		if (db->b_words_sz == 0) {
			db->b_words_sz = 4096;
			db->b_words = xmalloc(sizeof(*db->b_words) * db->b_words_sz);
		} else {
			db->b_words_sz *= 2;
			db->b_words = xrealloc(db->b_words, sizeof(*db->b_words) * db->b_words_sz);
		}
	}

	w = &db->b_words[db->b_words_off++];
	if ((w->word_offs = b_write(db->bw, word, wordsz)) == (uint64_t)-1)
		return -1;
	if (docpos) {
		/* Notice - this modifies the docarr. */
		dc_collect(db->b_dc, docarr, ndocs, docpos, ndocpos);
	}
	if (b_align2(db->bw, sizeof(int32_t)) == -1)
		return -1;
	w->docslen = ndocs * sizeof(*docarr);
	if ((w->docs_offs = b_write(db->bw, docarr, ndocs * sizeof(*docarr))) == (uint64_t)-1)
		return -1;

	db->b_hdr.total_word_len += wordsz;

	return 0;
}

int
index_blob_sync_words(struct index_db *db) {
	uint64_t posoffs = 0;
	unsigned int i;

	/* We might get called twice, second time we won't have a dc anymore. */
	if (db->b_dc && db->b_dc->offset) {
		unsigned int npos;
		struct docpos *pos = dc_fold(db->b_dc, &npos);

		if (b_align2(db->bw, getpagesize()) == -1)
			return -1;
		posoffs = b_write(db->bw, pos, npos * sizeof(*pos));
		free(pos);
		if (posoffs == (uint64_t)-1)
			return -1;
	}
	free(db->b_dc);
	db->b_dc = NULL;

	for (i = 0; i < db->b_words_off; i++) {
		db->b_words[i].docpos_offs = posoffs;
	}

	if (db->b_words) {
		if (b_align2(db->bw, getpagesize()) == -1)
			return -1;
		if ((db->b_hdr.invwords_off = b_write(db->bw, db->b_words, db->b_words_off * sizeof(*db->b_words))) == (uint64_t)-1)
			return -1;
		db->b_hdr.ninvwords = db->b_words_off;
		free(db->b_words);
	}
	db->b_words_sz = 0;
	db->b_words_off = 0;
	db->b_words = NULL;

	return 0;
}

int
index_blob_sync_hdr(struct index_db *db) {
	db->b_hdr.magic = IB_MAGIC;
	db->b_hdr.version = IB_VERSION;
	if (pwrite(db->blob_fd, &db->b_hdr, sizeof(db->b_hdr), 0) != sizeof(db->b_hdr))
		return -1;
	return 0;
}

static int32_t
map_docoff(struct index_db *db, int32_t off) {
	if (!db->docoff_map)
		return off;
	return (db->docoff_map[off]);
}

static int
int32_cmp(const void *a, const void *b) {
	return *(int32_t*)a - *(int32_t*)b;
}

static int
flush_db_blob(struct index_db *db) {
	struct timer_instance *all_ti;
	struct timer_instance *ti;
	struct avl_node *an;
	struct avl_it ait;
	const char *word;
	int wl;
	int i;

	all_ti = timer_start(db->ti, "flush_db_blob");

	log_printf(LOG_DEBUG, "Flushing %u words", db->ninvword_docs);
	avl_it_init(&ait, db->invword_docs, NULL, NULL, wi_cmp);
	while ((an = avl_it_next(&ait)) != NULL) {
		struct word_indexed *wi = avl_data(an, struct word_indexed, avl);
		const struct _invword *iw = wi->docs;
		struct docindex *ndocuments;
		struct docpos *ndocpos;
		int npos = 0;			/* number of new positions. */
		int ndocs = wi->ndocs;		/* number of new docs. */
		int posc;

		word = wi->word;
		wl = strlen(wi->word);

		for (i = 0; i < ndocs; i++)
			npos += iw[i].npos;

		ti = timer_start(all_ti, "mallocs");
		ndocuments = xmalloc(sizeof(*ndocuments) * ndocs);
		ndocpos = xmalloc(sizeof(*ndocpos) * (npos + ndocs));

		posc = 0;
		timer_handover(ti, "rebuild_docs");

		for (i = 0; i < ndocs; i++) {
			int di = ndocs - i - 1;

			ndocuments[i].docoff = map_docoff(db, iw[di].docoff);
			if (iw[di].npos) {
				ndocuments[i].ptr = posc;

				ndocpos[posc++].pos = iw[di].npos;
				memcpy(&ndocpos[posc], iw[di].posarr, iw[di].npos * sizeof(*ndocpos));
				posc += iw[di].npos;
			} else {
				ndocuments[i].ptr = INDEX_DOCINDEX_NO_POS;
			}

			free(iw[di].posarr);
		}

		timer_handover(ti, "qsort");
		qsort(ndocuments, ndocs, sizeof(*ndocuments), int32_cmp);

		timer_handover(ti, "store");
		if (index_blob_store_word(db, word, wl + 1, ndocuments, ndocs, ndocpos, posc))
			return -1;

		timer_handover(ti, "free");
		free(ndocuments);
		free(ndocpos);
		timer_end(ti, NULL);
	}
	if (index_blob_sync_words(db))
		return -1;

	log_printf(LOG_DEBUG, "Flushing %d indexed attributes", db->ninvattr_docs);
	avl_it_init(&ait, db->invattr_docs, NULL, NULL, ai_cmp);
	while ((an = avl_it_next(&ait)) != NULL) {
		struct attr_indexed *ai = avl_data(an, struct attr_indexed, avl);

		ti = timer_start(all_ti, "attr_qsort");
		for (i = 0 ; i < ai->ndocs ; i++)
			ai->docs[i] = map_docoff(db, ai->docs[i]);
		qsort(ai->docs, ai->ndocs, sizeof(*ai->docs), int32_cmp);
		timer_handover(ti, "attr_store");
		if (index_blob_store_attr(db, ai->attr, strlen(ai->attr) + 1, ai->docs, ai->ndocs))
			return -1;
		timer_end(ti, NULL);
	}
	if (db->b_attrs && index_blob_sync_attrs(db))
		return -1;

	timer_end(all_ti, NULL);

	wi_free(db->invword_docs);
	db->invword_docs = NULL;
	db->ninvword_docs = 0;
	ai_free(db->invattr_docs);
	db->invattr_docs = NULL;
	db->ninvattr_docs = 0;
	return 0;
}

struct meta_writer {
	struct blob_writer *bw;
	uint64_t first_off;
	uint64_t written;
};

static int
p_meta(void *v, int n, int d, const char *fmt, ...) {
	struct meta_writer *mw = v;
	char *buf = NULL;
	va_list ap;
	int res;

	va_start(ap, fmt);
	res = vasprintf(&buf, fmt, ap);
	va_end(ap);

	if (res > 0) {
		uint64_t meta_offs;
		if ((meta_offs = b_write(mw->bw, buf, res)) == (uint64_t)-1) {
			res = -1;
			goto out;
		}
		if (mw->first_off == 0)
			mw->first_off = meta_offs;
		mw->written += res;
	}
out:
	free(buf);

	return res;
}

static void
db_store_meta(struct index_db *db) {
	struct bpapi_vtree_chain vt;
	struct meta_writer mw = {0};

	mw.bw = db->bw;

	bconf_vtree(&vt, db->meta);
	vtree_json(&vt, 0, 0, p_meta, &mw);
	vtree_free(&vt);

	db->b_hdr.meta_sz = mw.written;
	db->b_hdr.meta_off = mw.first_off;
}

static int
db_load_meta(struct index_db *db, const char *base) {
	struct ib_hdr *hdr = db->db_blob;

	if (hdr && hdr->meta_sz) {
		if (json_bconf(&db->meta, NULL, ib_getmeta(hdr), hdr->meta_sz, 0)) {
			log_printf(LOG_DEBUG, "Loading meta failed (%s).", bconf_get_string(db->meta, "error"));
			return -1;
		}
	} else if (base != NULL) {
		/*
		 * Migration code. Reproduce new meta from old meta db.
		 */
		char mpath[PATH_MAX];
		TCHDB *ometa = tchdbnew();
		char *dump;
		int l;

		snprintf(mpath, sizeof(mpath), "%s/meta.tch", base);
		if (!tchdbopen(ometa, mpath, HDBOREADER)) {
			tchdbdel(ometa);
			return 0;
		}
		/* Attributes */
	       	if ((dump = tchdbget(ometa, "attributes", sizeof("attributes") - 1, &l))) {
			TCLIST *att = tclistload(dump, l);
			int i;

			for (i = 0; i < tclistnum(att); i++) {
				db_add_attribute(db, tclistval(att, i, &l));
			}

			free(dump);
			tclistdel(att);
		}
		/* Stopwords */
		if ((dump = tchdbget(ometa, "stopwords", sizeof("stopwords") - 1, &l))) {
			TCMAP *s = tcmapload(dump, l);
			TCLIST *st = tcmapkeys(s);
			int i;

			for (i = 0; i < tclistnum(st); i++) {
				add_stopword(db, tclistval(st, i, &l));
			}

			free(dump);
			tclistdel(st);
			tcmapdel(s);
		}
		/* Opers */
		if ((dump = tchdbget(ometa, "opers", sizeof("opers") - 1, &l))) {
			TCMAP *o = tcmapload(dump, l);
			TCLIST *ok = tcmapkeys(o);
			int i;

			for (i = 0; i < tclistnum(ok); i++) {
				const struct {
					char lang[4];
					char word[12];
				} *op;
				const uint8_t *oper;
				op = tclistval(ok, i, &l);
				if (l != sizeof(*op)) {
					log_printf(LOG_CRIT, "wrong size of migrated oper %d", l);
					continue;
				}
				oper = tcmapget(o, op, sizeof(*op), &l);
				index_add_oper(db, op->lang, op->word, *oper);
			}

			free(dump);
			tclistdel(ok);
			tcmapdel(o);
		}
		/* Conf */
		if ((dump = tchdbget(ometa, "conf", sizeof("conf") - 1, &l))) {
			TCMAP *c = tcmapload(dump, l);
			TCLIST *ck = tcmapkeys(c);
			int i;

			for (i = 0; i < tclistnum(ck); i++) {
				const char *k = tclistval(ck, i, &l);
				index_add_conf(db, k, tcmapget2(c, k));
			}
		}

		/* others */
		const char *d;
		if ((d = tchdbget(ometa, "timestamp", sizeof("timestamp") - 1, &l))) {
			index_set_timestamps(db, d);
		}
		if ((d = tchdbget(ometa, "from_id", sizeof("from_id") - 1, &l))) {
			index_set_from_id(db, d);
		}
		if ((d = tchdbget(ometa, "to_id", sizeof("to_id") - 1, &l))) {
			index_set_to_id(db, d);
		}
		
		tchdbclose(ometa);
		tchdbdel(ometa);
	}
	return 0;
}

int
store_doc(struct index_db *db, struct document *doc, int allow_cache) {
	/*log_printf(LOG_DEBUG, "store_doc %d/%d", doc->id.id, doc->id.order);*/
	/* Negative id is not supported. */
	if (doc->id.id < 0 || doc->id.order < 0)
		return 1;

	/* Insert printed attributes */
	if (store_attrs_printed(db, doc)) {
		close_doc(doc);
		return 1;
	}

	/* Insert words */
	store_words(db, doc);

	/* Insert searchable attributes */
	store_attrs_indexed(db, doc);

	return close_doc(doc);
}

struct document *
open_doc(struct index_db *db) {
	struct document *doc = xmalloc(sizeof(*doc));
	int nattrs = bconf_count(bconf_vget(db->meta, "attr", "order", NULL));
	doc->words = tcmapnew2(500);
	doc->attrs_printed = tcmapnew2(nattrs);
	doc->attrs_indexed = tclistnew2(nattrs); /* Use same size as printed, should be approximately right. */
	return doc;
}

int
delete_doc(struct index_db *db, u_int32_t id) {
	char buf[16];

	snprintf(buf, sizeof(buf), "%u", id);
	bconf_add_datav(&db->meta, 2, (const char *[]){ "deleted_docs", buf }, "", BCONF_REF);

	return 0;
}

int
close_doc(struct document *doc) {
	if (doc->attrs_printed)
		tcmapdel(doc->attrs_printed);

	if (doc->attrs_indexed)
		tclistdel(doc->attrs_indexed);

	if (doc->words)
		tcmapdel(doc->words);

	free(doc);

	return 0;
}

void
doc_add_word(struct document *doc, const char *word, int wl, const struct docpos *pos) {
	if (wl == -1)
		wl = strlen(word);

	if (pos)
		tcmapputcat(doc->words, word, wl, pos, sizeof(*pos));
	else
		tcmapputcat(doc->words, word, wl, "", 0);
}

int
doc_add_attr(struct document *doc, const char *name, const char *value, int index) {
	if(!value) {
		return 0;
	}

	if (index & AATTR) {
		if (strpbrk(value, "\t\n")) {
			char *val = xstrdup(value);
			char *tp = val;

			while ((tp = strpbrk(tp, "\t\n")))
				*tp++ = ' ';
			tcmapput2(doc->attrs_printed, name, val);
			free(val);
		} else {
			tcmapput2(doc->attrs_printed, name, value);
		}
	}

	if (index & AIND) {
		char *attr_word;
		int res;

		/*
		 * XXX - instead of strdup:ing this string at least three times, we
		 * could just look the attr up in invattr_docs here and save the
		 * pointer.
		 */
		ALLOCA_PRINTF(res, attr_word, "%s:%s", name, value);
		tclistpush(doc->attrs_indexed, attr_word, res);
	}

	return 0;
}

int
doc_add_attr_len(struct document *doc, const char *name, size_t nlen, const char *value, size_t vlen, int index) {
	char *n = strndupa(name, nlen);
	char *v = strndupa(value, vlen);

	return doc_add_attr(doc, n, v, index);
}

int
doc_has_attribute(struct document *doc, const char *name) {
	return tcmapget2(doc->attrs_printed, name) != NULL;
}

int
doc_num_attributes(struct document *doc) {
	return tcmaprnum(doc->attrs_printed);
}

/*
 * Prime a new delta-index using an old index. 
 *
 *  - copy the attributes (since the new index may only add attributes on top
 *    of the old ones, not rearrange them)
 *  - copy the "to" state id in the old index into the "from" state id of
 *    the new index.
 */
struct index_db *
index_prime_new(const char *base, int shard, int update, int mode) {
	char ndb_name[PATH_MAX];
	char odb_base[PATH_MAX];
	const char *id, *timestamp;
	struct index_db *db = NULL;
	struct index_db *odb;

	mode |= INDEX_WRITER;

	assert(mode == INDEX_WRITER);		/* XXX - not yet implemented */

	if (update && shard) {
		log_printf(LOG_CRIT, "Can't update and shard");
		exit(1);
	}

	if (update) {
		strlcpy(odb_base, base, sizeof(odb_base));
	} else {
		if (shard == -1) {
			int i;

			for (i = 0 ; ; i++) {
				snprintf(ndb_name, sizeof(ndb_name), "%s%d/db.blob", base, i);
				if (access(ndb_name, F_OK) == -1)
					break;
			}
			snprintf(ndb_name, sizeof(ndb_name), "%s%d", base, i);
			if (i == 0) {
				if ((db = open_index(ndb_name, mode)) == NULL) {
					log_printf(LOG_CRIT, "Error reopening new db");
					exit(1);
				}
				return db;
			}
			shard = i;
		} else {
			snprintf(ndb_name, sizeof(ndb_name), "%s%d", base, shard);
		}
		snprintf(odb_base, sizeof(odb_base), "%s%d", base, shard - 1);
	}

	if ((odb = open_index(odb_base, INDEX_READER)) == NULL) {
		log_printf(LOG_CRIT, "Failed to open old meta %s", odb_base);
		/* exit(10) tells index_ctl to launch a reindex if updating. */
		exit(10);
	}

	id = index_get_to_id(odb);

	if (update) {
		/* Create new delta index db with the db name 'ts' */
		snprintf(ndb_name, sizeof(ndb_name), "%s/delta_%s", base, id);
		if ((db = open_index(ndb_name, INDEX_WRITER)) == NULL) {
			log_printf(LOG_CRIT, "Can't create new delta index");
			exit(1);
		}
		if (id)
			index_set_from_id(db, id);
	} else {
		if ((db = open_index(ndb_name, INDEX_WRITER)) == NULL) {
			log_printf(LOG_CRIT, "Error reopening new db");
			exit(1);
		}
		if (id)
			index_set_to_id(db, id);
	}

	bconf_merge_prefix(&db->meta, "stopwords", bconf_get(odb->meta, "stopwords"));
	bconf_merge_prefix(&db->meta, "opers", bconf_get(odb->meta, "opers"));
	bconf_merge_prefix(&db->meta, "attr", bconf_get(odb->meta, "attr"));
	bconf_merge_prefix(&db->meta, "conf", bconf_get(odb->meta, "conf"));
	if ((timestamp = index_get_timestamp(odb)) != NULL) {
		bconf_add_data(&db->meta, "state.timestamp", timestamp);
	}

	close_index(odb, 0);

	/*
	 * Recache attributes, header and stopwords now that they've been reloaded.
	 */
	free(db->header);
	db->header = NULL;
	load_doc_header(db);

	return db;
}

int
index_prime_update(struct index_db *ndb, struct index_db *odb) {
	const char *timestamp;

	bconf_merge_prefix(&ndb->meta, "stopwords", bconf_get(odb->meta, "stopwords"));
	bconf_merge_prefix(&ndb->meta, "opers", bconf_get(odb->meta, "opers"));
	bconf_merge_prefix(&ndb->meta, "attr", bconf_get(odb->meta, "attr"));
	bconf_merge_prefix(&ndb->meta, "conf", bconf_get(odb->meta, "conf"));
	if ((timestamp = index_get_timestamp(odb)) != NULL) {
		bconf_add_data(&ndb->meta, "state.timestamp", timestamp);
	}

	return 0;
}

int
index_merge_open_verify_delta_match(struct index_db *base, struct index_db *delta) {
	const char *base_to_id = index_get_to_id(base);
	const char *delta_from_id = index_get_from_id(delta);
	struct bconf_node *a1, *a2;
	int i;

#if 0
	/* Not yet, but we might want to support this in the future. */
	if (base_to_id == NULL) {
		if (delta_from_id != NULL) {
			log_printf(LOG_CRIT, "index_merge_open_verify_delta_match: delta from without base to");
			return -1;
		}
	} else {
		if (delta_from_id == NULL) {
			log_printf(LOG_CRIT, "index_merge_open_verify_delta_match: delta from missing");
			return -1;			
		}
		if (strcmp(base_to_id, delta_from_id)) {
			log_printf(LOG_CRIT, "index_merge_open_verify_delta: State id missmatch between from_db and delta update, %s != %s", id_1, from_id);
			return -1;
		}
	}
#else
	if (base_to_id == NULL) {
		log_printf(LOG_CRIT, "index_merge_open_verify_delta: base index missing to_id");
		return -1;
	}
	if (delta_from_id == NULL) {
		log_printf(LOG_CRIT, "index_merge_open_verify_delta: delta index missing from_id");
		return -1;
	}
	if (strcmp(base_to_id, delta_from_id)) {
		log_printf(LOG_CRIT, "index_merge_open_verify_delta:State id missmatch between from_db and delta update, %s != %s", base_to_id, delta_from_id);
		return -1;
	}
#endif

	/* Check attributes */
	a1 = bconf_vget(base->meta, "attr", "order", NULL);
	a2 = bconf_vget(delta->meta, "attr", "order", NULL);
	if (bconf_count(a2) < bconf_count(a1)) {
		log_printf(LOG_CRIT, "index_merge_open_verify_delta: delta index has fewer attributes than base");
		return -1;
	}
	for (i = 0; i < bconf_count(a1); i++) {
		const char *aa1 = bconf_value(bconf_byindex(a1, i));
		const char *aa2 = bconf_value(bconf_byindex(a2, i));
		if (strcmp(aa1, aa2)) {
			log_printf(LOG_CRIT, "index_merge_open_verify_delta: Attribute mismatch: (%s) != (%s)", aa1, aa2);
			return -1;
		}
	}
	return 0;
}

int
open_indexes(const char *base, int tuning, int update, struct index_db **iret, int *ni) {
	struct bconf_node *a1, *a2;
	const char *id_1 = NULL;
	int indexes;
	int j;
	int i;

	for (indexes = 0; indexes < *ni; indexes++) {
		char db_name[PATH_MAX];

		if (update)
			snprintf(db_name, sizeof(db_name), "%s", base);
		else
			snprintf(db_name, sizeof(db_name), "%s%d", base, indexes);

		/*
		 * Since we don't know how many shards there are, we check if the blob file exists
		 * here.
		 */
		if (indexes > 0) {
			char blob_name[PATH_MAX];

			if (update)
				break;

			log_printf(LOG_INFO, "checking for %s", db_name);
			snprintf(blob_name, sizeof(blob_name), "%s/db.blob", db_name);
			if (access(blob_name, F_OK) == -1) {
				snprintf(blob_name, sizeof(blob_name), "%s/db.blob", db_name);
				if (access(blob_name, F_OK) == -1) {
					log_printf(LOG_INFO, "blob %s doesn't exist", db_name);
					break;
				}
			}
		}

		log_printf(LOG_DEBUG, "Opening index \"%s\"", db_name);
		if ((iret[indexes] = open_index(db_name, INDEX_READER)) == NULL) {
			/* Failover to base */
			if (indexes != 0 || (iret[indexes] = open_index(base, INDEX_READER)) == NULL) {
				log_printf(LOG_CRIT, "Failed to open index %s", db_name);
				goto fail;
			}
			log_printf(LOG_DEBUG, "Failover to base \"%s\"", base);
		}

		/* State_id checking */
		if (indexes == 0) {
			id_1 = index_get_to_id(iret[0]);
		} else {
			const char *id_2 = index_get_to_id(iret[indexes]);

			if (((!id_1 || !id_2) && id_1 != id_2) || strcmp(id_1, id_2)) {
				log_printf(LOG_CRIT, "id mismatch %s != %s", id_1, id_2);
				goto fail;
			}
		}

		/* We can't have from_id here. */
		if (index_get_from_id(iret[indexes])) {
			log_printf(LOG_CRIT, "from_id unexpected");
			goto fail;
		}

		/* Check attributes */
		a1 = bconf_vget(iret[0]->meta, "attr", "order", NULL);
		a2 = bconf_vget(iret[indexes]->meta, "attr", "order", NULL);
		if (bconf_count(a2) < bconf_count(a1)) {
			log_printf(LOG_CRIT, "Second index has fewer attributes than first");
			goto fail;
		}
		for (i = 0; i < bconf_count(a1); i++) {
			const char *aa1 = bconf_value(bconf_byindex(a1, i));
			const char *aa2 = bconf_value(bconf_byindex(a2, i));
			if (strcmp(aa1, aa2)) {
				log_printf(LOG_CRIT, "Attribute mismatch: (%s) != (%s)", aa1, aa2);
				goto fail;
			}
		}

		/* Check for no deleted docs */
		if (!update && bconf_get(iret[indexes]->meta, "deleted_docs")) {
			log_printf(LOG_CRIT, "Found deleted docs");
			goto fail;
		}
	}


	if (indexes == *ni) {
		log_printf(LOG_CRIT, "Found more shards than maximum supported value of %d shards. Increase shard_size in config    to reduce the number of shards.", *ni);
		goto fail;
	}

	/* Update */
	if (update) {
		char delta[PATH_MAX];
		if (indexes != 1) {
			log_printf(LOG_CRIT, "Update index requires only one index shard, got %d shards", indexes);
			goto fail;
		}

		/* Open delta index */
		snprintf(delta, sizeof(delta), "%s/delta_%s", base, id_1);
		log_printf(LOG_INFO, "Opening delta index %s", delta);
		if ((iret[1] = open_index(delta, INDEX_READER)) == NULL) {
			log_printf(LOG_CRIT, "Failed to open delta index %s", delta);
			goto fail;
		}
		indexes = 2;

		if (index_merge_open_verify_delta_match(iret[0], iret[1])) {
			goto fail;
		}
	}

	*ni = indexes;

	return 0;
fail:
	for (j = 0; j < indexes; j++) {
		if (iret[j])
			close_index(iret[j], 0);
	}
	*ni = 0;

	return -1;
}

/*
 * This function converts the version 1 docpos array into a version 2 docpos array.
 * This makes a few assumptions about how docpos are stored. Since there was never
 * any other code that wrote blob indexes, those assumptions are safe, but they
 * are tested anyway to be safe.
 */
static int
open_index_convert_docpos_ver1(struct index_db *db) {
	const struct ib_hdr *hdr = db->db_blob;
	const struct ib_invword *iw = ib_getinvwords(hdr);
	const struct ib_docpos_v1 *dp = NULL;
	uint64_t posoffs = 0;
	uint32_t totpos = 0, totpos2;
	unsigned int i;

	for (i = 0; i < hdr->ninvwords; i++) {
		const struct ib2_docindex *di = ib2_getworddocs(hdr, &iw[i]);
		unsigned int j;

		dp = (const struct ib_docpos_v1 *)((char *)hdr + iw[i].docpos_offs);

		if (iw[i].docpos_offs != (uint64_t)-1) {
			if (posoffs == 0) {
				posoffs = iw[i].docpos_offs;
			} else if (posoffs != iw[i].docpos_offs) {
				/* All indexes have been created pointing to the same docpos. */
				log_printf(LOG_CRIT, "open_index_convert_docpos_ver1: docpos_offs mismatch");
				return -1;
			}
		}
		for (j = 0; j < iw[i].docslen / sizeof(*di); j++) {
			uint32_t npos;
			int ndocpos;
			uint32_t pptr = di[j].posptr;

			if (pptr == (uint32_t)-1)
				continue;
			/* totpos is calculated as the highest numbered pos pointed to by a word. */
			ndocpos = dp[pptr].pos;
			npos = pptr + ndocpos + 1;
			if (totpos < npos)
				totpos = npos;
		}
	}
	totpos2 = (hdr->invwords_off - posoffs) / sizeof(*dp);
	
	uint32_t ps = getpagesize();
	uint32_t tp = ((totpos * sizeof(*dp)  + (ps - 1)) & ~(ps - 1)) / sizeof(*dp);

	if (tp != totpos2) {
		log_printf(LOG_CRIT, "open_index_convert_docpos_ver1: totpos mismatch %d != %d", tp, totpos2);
		return -1;
	}
	dp = (struct ib_docpos_v1 *)((char *)hdr + posoffs);
	db->db_docpos_rewrite = xmalloc(totpos * sizeof(*db->db_docpos_rewrite));
	for (i = 0; i < totpos; i++) {
		db->db_docpos_rewrite[i].flags = dp[i].flags;
		db->db_docpos_rewrite[i].pos = dp[i].pos;
		db->db_docpos_rewrite[i].rel_boost = 0;
	}
	return 0;
}

static int
docs_cmp(const void *a, const void *b) {
	const struct ib_document *ia = a;
	const struct ib_document *ib = b;
	return index_doc_cmp(&ia->id, &ib->id);
}

struct fake_docoff {
	struct ib_document doc; /* Must be first due to casting */
	struct avl_node tree_id;
};

static int
fake_docoff_cmp_id(const struct avl_node *an, const struct avl_node *bn) {
	struct fake_docoff *a = avl_data(an, struct fake_docoff, tree_id);
	struct fake_docoff *b = avl_data(bn, struct fake_docoff, tree_id);

	return index_doc_cmp(&a->doc.id, &b->doc.id);
}

static void
create_fake_docoff(struct index_db *db, struct avl_node **root, int *cnt, const struct ib_doc *doc) {
	struct fake_docoff d = { .doc = { .id = *doc } };
	struct avl_node *n;

	n = avl_lookup(&d.tree_id, root, fake_docoff_cmp_id);
	if (n)
		return;

	/* We need to be able to lookup both by doc and by docoff, thus need two trees. */
	struct fake_docoff *nd = zmalloc(sizeof (*nd));
	nd->doc = d.doc;
	avl_insert(&nd->tree_id, root, fake_docoff_cmp_id);
	(*cnt)++;
}

/*
 * This recurses through the fake docoff tree, populates our docs array and frees all the elements in the tree at the same time.
 */
static void
fake_docoff_iter(struct avl_node *n, struct ib_document *docs, uint64_t *docs_off) {
	if (!n)
		return;
	struct fake_docoff *fd = avl_data(n, struct fake_docoff, tree_id);
	docs[*docs_off] = fd->doc;
	(*docs_off)++;
	fake_docoff_iter(n->link[0], docs, docs_off);
	fake_docoff_iter(n->link[1], docs, docs_off);
	free(fd);
}

static void
open_index_sort_docs_ver2(struct index_db *db) {
	const struct ib_hdr *hdr = db->db_blob;
	const struct ib_document *documents = ib_getdocuments(hdr);
	struct avl_node *fake_docoff_tree_id = NULL;
	int nfakedocs = 0;
	uint64_t i;

	/*
	 * Find all subads in the docarrs for attrs and words.
	 */

	/* Exhaustive search invattrs for subads. */
	const struct ib_invattr *attrs = ib_getinvattrs(hdr);
	for (i = 0 ; i < hdr->ninvattrs ; i++) {
		const struct ib_doc *docs = ib2_getattrdocs(hdr, &attrs[i]);
		uint64_t ndocs = attrs[i].docslen / sizeof(*docs);

		for (uint64_t j = 0 ; j < ndocs ; j++) {
			if (docs[j].suborder < 0)
				create_fake_docoff(db, &fake_docoff_tree_id, &nfakedocs, &docs[j]);
		}
	}

	/* Exhaustive search invwords for subads. */
	const struct ib_invword *words = ib_getinvwords(hdr);
	for (i = 0 ; i < hdr->ninvwords ; i++) {
		const struct ib2_docindex *docs = ib2_getworddocs(hdr, &words[i]);
		uint64_t ndocs = words[i].docslen / sizeof(*docs);

		for (uint64_t j = 0 ; j < ndocs ; j++) {
			if (docs[j].doc.suborder < 0)
				create_fake_docoff(db, &fake_docoff_tree_id, &nfakedocs, &docs[j].doc);
		}
	}

	/* Sort real plus fake docs */

	struct ib_document *sorted_docs;

	sorted_docs = xmalloc((hdr->ndocuments + nfakedocs) * sizeof (*db->sorted_docs));

	for (i = 0; i < hdr->ndocuments; i++)
		sorted_docs[i] = documents[i];

	fake_docoff_iter(fake_docoff_tree_id, sorted_docs, &i);

	qsort(sorted_docs, i, sizeof(*db->sorted_docs), docs_cmp);

	db->sorted_docs = sorted_docs;
	db->nsorted_docs = i;
	db->free_sorted_docs = 1;
}

/*
 * Round up to page size.
 */
static unsigned long long
round_page(unsigned long long v) {
	static unsigned long long ps;

	if (ps == 0) {	/* This is thread safe if assignment to ULL is atomic. */
		ps = getpagesize();
	}
	return (v + ps - 1) & ~(ps - 1);
}

static int
open_index_reader_finalize(struct index_db *db, const char *base) {
	struct ib_hdr *hdr = db->db_blob;

	if (hdr->magic != IB_MAGIC) {
		log_printf(LOG_CRIT, "index blob: bad magic");
		return -1;
	} else if (hdr->version < IB_VERSION0 || hdr->version > IB_VERSION3) {
		log_printf(LOG_CRIT, "index blob: unknown blob version");
		return -1;
	}
	/*
	 * Deal with incompatible format for docpos of indexes below IB_VERSION2.
	 */
	if (hdr->version < IB_VERSION2) {
		if (open_index_convert_docpos_ver1(db)) {
			log_printf(LOG_CRIT, "index blob: ver1 docpos conversion failed");
			return -1;
		}
	}

	if (hdr->version < IB_VERSION3) {
		open_index_sort_docs_ver2(db);
	} else {
		/*
		 * Saving this here makes the rest of the code
		 * more readable since we have to deal with
		 * this in the V2 compat layer.
		 */
		db->sorted_docs = ib_getdocuments(hdr);
		db->nsorted_docs = hdr->ndocuments;
		db->free_sorted_docs = 0;
	}

	db_load_meta(db, base);
	load_doc_header(db);

	log_printf(LOG_DEBUG, "blob version 0x%" PRIx64 " loaded", hdr->version);

	return 0;
}

/*
 * Open a blob index from pre-allocated memory.
 *
 * do_munmap tells us that the memory we get is mmap:ed and given away to us to munmap on close_index.
 */
struct index_db *
open_mem_index_reader(void *blob, size_t blob_sz, int do_munmap) {
	struct index_db *db;

	if ((db = calloc(1, sizeof *db)) == NULL) {
		log_printf(LOG_CRIT, "open_mem_index_reader: malloc: %m");
		return NULL;
	}
	db->malloced = 1;
	db->blob_fd = -1;
	db->db_blob = blob;
	db->db_blob_size = blob_sz;
	db->db_blob_munmap = do_munmap;
	db->ti = timer_start(NULL, "db");
	if (open_index_reader_finalize(db, NULL) == -1) {
		free(db);
		db = NULL;
	}
	return db;
}

static int
index_blob_map_reader(struct index_db *db, int fd) {
	struct stat sb;

	if (fstat(fd, &sb) == -1) {
		log_printf(LOG_INFO, "Failed to stat index: %m");
		return -1;
	}
	db->db_blob_size = round_page(sb.st_size);
	db->db_blob_munmap = 1;

	if ((db->db_blob = mmap(NULL, db->db_blob_size, PROT_READ, MAP_SHARED|MAP_FILE, fd, 0)) == MAP_FAILED) {
		log_printf(LOG_CRIT, "Failed to mmap db blob: %m");
		return -1;
	}
	return 0;
}

struct index_db *
open_mem_index_writer(void) {
	char shm_name[NAME_MAX];
	struct index_db *db;

	db = zmalloc(sizeof(*db));

	db->malloced = 1;

	/*
	 * Attempt to have the shm_name unique by using our pid.
	 *
	 * This will fail horribly when opening indexes in multiple
	 * threads. Figure out a better way then.
	 */
	snprintf(shm_name, sizeof(shm_name), "/indexer%d", (int)getpid());

	if ((db->blob_fd = shm_open(shm_name, O_RDWR|O_CREAT|O_EXCL|O_TRUNC, 0600)) == -1) {
		log_printf(LOG_CRIT, "open_mem_index_writer: shm_open: %m");
		goto fail;
	}
	if (shm_unlink(shm_name) == -1) {
		log_printf(LOG_CRIT, "open_mem_index_writer: shm_unlink: %m");
		close(db->blob_fd);
		goto fail;
	}

	db->db_blob = MAP_FAILED;
	db->ti = timer_start(NULL, "db");
	db->bw = zmalloc(sizeof(*db->bw));
	if (b_open(db->bw, db->blob_fd, getpagesize())) {
		log_printf(LOG_CRIT, "open_mem_index_writer: b_open failed: %m");
		close(db->blob_fd);
		goto fail;
	}
	db->b_dc = dc_init();

	return db;
fail:
	free(db);
	return NULL;
}

struct index_db *
open_index(const char *base, int mode) {
	struct timer_instance *open_ti;
	char db_name[PATH_MAX];
	struct index_db *db;

	db = zmalloc(sizeof(*db));

	db->blob_fd = -1;
	db->db_blob = MAP_FAILED;

	db->ti = timer_start(NULL, "db");
	open_ti = timer_start(db->ti, "open");

	/* Make sure one and only one of READER or WRITER is set. */
	if (!!(mode & INDEX_READER) == !!(mode & INDEX_WRITER)) {
		log_printf(LOG_CRIT, "open_index(%s) invalid mode 0x%x", base, mode);
		goto failed;
	}

	if (mode & INDEX_FILE) {
		snprintf(db_name, sizeof(db_name), "%s", base);
	} else {
		snprintf(db_name, sizeof(db_name), "%s/db.blob", base);
	}
	if (mode & INDEX_WRITER) {
		if (access(db_name, F_OK) != 0) {
			if (!(mode & INDEX_FILE)) {
				mkdir(base, 0755);
			}
		} else {
			log_printf(LOG_CRIT, "Tried to open existing index \"%s\" for writing", base);
			goto failed;
		}
	}

	if ((mode & INDEX_READER)) {
		int fd;

		if ((fd = open(db_name, O_RDONLY)) == -1) {
			log_printf(LOG_CRIT, "Failed to open index %s for reading.", db_name);
			goto failed;
		}

		if (index_blob_map_reader(db, fd) == -1) {
			close(fd);
			/* It has already logged why. */
			goto failed;
		}

		/* Since we're not writing we don't need the fd. */
		close(fd);

		if (open_index_reader_finalize(db, base) == -1)
			goto failed;
	} else if (mode & INDEX_WRITER) {
		if ((db->blob_fd = open(db_name, O_RDWR|O_CREAT|O_EXCL, 0644)) == -1) {
			log_printf(LOG_INFO, "failed to create blob: %m");
			goto failed;
		}
		db->bw = zmalloc(sizeof(*db->bw));
		if (b_open(db->bw, db->blob_fd, getpagesize())) {
			log_printf(LOG_INFO, "init blob writer failed: %m");
			goto failed;
		}
		db->b_dc = dc_init();
	} else {
		log_printf(LOG_CRIT, "Unknown/unsupported open mode 0x%x", mode);
		goto failed;
	}

	timer_end(open_ti, NULL);

	return db;

failed:
	if (db->db_blob != MAP_FAILED) {
		munmap(db->db_blob, db->db_blob_size);
		db->db_blob = NULL;
	}
	if (db->bw) {
		b_close(db->bw);
		free(db->bw);
	}
	if (db->blob_fd != -1) {
		close(db->blob_fd);
	}
	free(db);

	return NULL;
}

int
index_is_stopword(struct index_db *db, const char *word) {
	return bconf_vget(db->meta, "stopwords", word, NULL) != NULL;
}

void
add_stopword(struct index_db *db, const char *word) {
	bconf_add_datav(&db->meta, 2, (const char *[]){"stopwords", word}, "", BCONF_REF);
}

int
index_get_oper(struct index_db *db, const char *lang, const char *word) {
	struct bconf_node *iop = bconf_vget(db->meta, "opers", lang, word, NULL);

	return iop ? atoi(bconf_value(iop)) : -1;
}

void
index_add_oper(struct index_db *db, const char *lang, const char *word, uint8_t op) {
	char num[16];

	snprintf(num, sizeof(num), "%u", (unsigned int)op);

	bconf_add_datav(&db->meta, 3, (const char *[]){"opers", lang, word}, num, BCONF_DUP);
}

void
index_add_conf(struct index_db *db, const char *key, const char *value) {
	/*
	 * Notice that key can have dots in it. Unless you're looking for a specific key without dots only use
	 * bconf_byindex to access these keys.
	 */
	bconf_add_datav(&db->meta, 2, (const char *[]){ "conf", key}, value, BCONF_DUP);
}

const char *
index_get_conf_footprint(struct index_db *db) {
	if (!db->conf_footprint) {
		struct bconf_node *cn, *n;
		SHA1_CTX conf_fp;
		int i;

		cn = bconf_vget(db->meta, "conf", NULL);
		if (!cn)
			return NULL;

		SHA1Init(&conf_fp);
		for (i = 0; (n = bconf_byindex(cn, i)) != NULL; i++) {
			const char *k = bconf_key(n);
			const char *v = bconf_value(n);
			SHA1Update(&conf_fp, (const u_int8_t *)k, strlen(k));
			SHA1Update(&conf_fp, (const u_int8_t *)v, strlen(v));
		}
		db->conf_footprint = SHA1End(&conf_fp, NULL);
	}
	return db->conf_footprint;
}

static int
sync_index(struct index_db *db, struct timer_instance *all_ti, int close_flags) {
	struct timer_instance *ti;
	int res = 0;

	if (db->b_docs) {
		if (index_blob_sync_docs(db, close_flags) == -1) {
			log_printf(LOG_CRIT, "Error syncing docs");
			return -1;
		}
	}

	ti = timer_start(all_ti, "close");
	if (db->bw) {
		timer_handover(ti, "flush_blob");
		if ((res = flush_db_blob(db))) {
			log_printf(LOG_CRIT, "Error flushing db: %m");
			return -1;
		}
		timer_handover(ti, "store_meta");
		db_store_meta(db);
		timer_handover(ti, "sync_hdr");
		if (index_blob_sync_hdr(db)) {
			log_printf(LOG_CRIT, "Error syncing blob header");
			return -1;
		}
		b_close(db->bw);
		free(db->bw);
		db->bw = NULL;
	}

	timer_handover(ti, "free_words");
	wi_free(db->invword_docs);
	db->invword_docs = NULL;
	db->ninvword_docs = 0;

	timer_handover(ti, "free_attrs");
	ai_free(db->invattr_docs);
	db->invattr_docs = NULL;
	db->ninvattr_docs = 0;

	if (db->blob_fd != -1) {
		if (fsync(db->blob_fd)) {
			log_printf(LOG_CRIT, "Error flushing blob to disk: %m");
			return -1;
		}
		if (index_regress_fail_fsync) {
			log_printf(LOG_CRIT, "Error flushing blob to disk: requested");
			return -1;
		}
	}

	return 0;
}

int
close_index(struct index_db *db, int close_flags) {
	struct timer_instance *all_ti = timer_start(db->ti, "close_index");
	struct timer_instance *ti;

	ti = timer_start(all_ti, "sync");

	if (sync_index(db, ti, close_flags)) {
		return -1;
	}

	if (db->db_blob && db->db_blob != MAP_FAILED) {
		if (db->db_blob_munmap) {
			munmap(db->db_blob, db->db_blob_size);
		}
		db->db_blob = NULL;
	}

	if (db->blob_fd != -1) {
		close(db->blob_fd);
	}
	free(db->db_docpos_rewrite);

	timer_handover(ti, "free_meta");
	bconf_free(&db->meta);

	timer_handover(ti, "free_stuff");
	free(db->header);
	free(db->conf_header);
	free(db->conf_footprint);
	free(db->docoff_map);
	free(db->checksum);

	timer_end(ti, NULL);
	timer_end(all_ti, NULL);
	timer_end(db->ti, NULL);
	db->ti = NULL;
	if (db->free_sorted_docs)
		free((void *)db->sorted_docs);

	free(db);

	return 0;
}

int
index_writer_downgrade_to_reader(struct index_db *db) {
	struct timer_instance *all_ti = timer_start(db->ti, "downgrade_reader");
	struct timer_instance *ti = timer_start(all_ti, "sync");

	if (sync_index(db, ti, 0)) {
		/* Already logged. */
		return -1;
	}
	bconf_free(&db->meta);

	/* This needs to be closed. */
	assert(db->bw == NULL);
	assert(db->db_blob == NULL || db->db_blob == MAP_FAILED);
	assert(db->invword_docs == NULL);
	assert(db->invattr_docs == NULL);
	assert(db->b_docs == NULL);
	assert(db->b_attrs == NULL);
	assert(db->b_words == NULL);
	assert(db->b_dc == NULL);
	assert(db->meta == NULL);

	/* This needs to remain open. */
	assert(db->blob_fd != -1);

	timer_handover(ti, "map_reader");

	if (index_blob_map_reader(db, db->blob_fd) == -1) {
		/* It has already logged why. */
		return -1;
	}
	/* Reader doesn't need to keep the blob fd open. */
	close(db->blob_fd);
	db->blob_fd = -1;

	timer_end(ti, NULL);
	timer_end(all_ti, NULL);

	return open_index_reader_finalize(db, NULL);
}

uint64_t
index_get_num_docs(struct index_db *db) {
	struct ib_hdr *hdr = db->db_blob;
	return hdr->ndocuments;
}

uint64_t
index_get_num_subdocs(struct index_db *db) {
	struct ib_hdr *hdr = db->db_blob;
	return hdr->nsubdocs;
}

const struct doc *
index_get_doc(struct index_db *db, int32_t docoff) {
	unsigned int idx = docoff;

	assert(idx < db->nsorted_docs);
	return (const struct doc *)(void*)&db->sorted_docs[idx].id;
}

const char *
index_get_checksum_string(struct index_db *db) {
	char *digest_string;

	if (db->checksum)
		return db->checksum;

	/* We only support calculating checksums on readers. */
	if (db->db_blob == NULL || db->db_blob == MAP_FAILED)
		return NULL;

	xasprintf(&digest_string, "%llx", XXH64(db->db_blob, db->db_blob_size, 4711));

	/* Check if we lost the race to generate the checksum to someone else. */
	if (!__sync_bool_compare_and_swap(&db->checksum, NULL, digest_string)) {
		free(digest_string);
	}

	return db->checksum;
}

struct index_docs_iter {
	struct index_db *db;
	uint64_t ndocs;
	uint64_t i;
};

struct index_docs_iter *
index_docs_iter_open(struct index_db *db, int persistent, unsigned int *ndocs) {
	struct index_docs_iter *it = zmalloc(sizeof(*it));

	it->db = db;
	it->ndocs = db->nsorted_docs;

	if (it->ndocs == 0) {
		free(it);
		*ndocs = 0;
		return NULL;
	}

	*ndocs = it->ndocs;

	return it;
}

void
index_docs_iter_close(struct index_docs_iter *it) {
	free(it);
}

void
index_docs_iter_get(struct index_docs_iter *it, const struct doc **key, int *ksz, const char **value, int *vsz, int32_t *docoff) {
	const struct ib_hdr *hdr = it->db->db_blob;
	const struct ib_document *document;

	document = &it->db->sorted_docs[it->i];
	*docoff = it->i;

	*key = (void *)&document->id;
	*ksz = sizeof(struct doc);
	*value = ib_getdocval(hdr, document);
	*vsz = document->doclen;
}

struct index_docs_iter *
index_docs_iter_next(struct index_docs_iter *it) {
	if (++it->i == it->ndocs) {
		index_docs_iter_close(it);
		return NULL;
	}
	return it;
}

void
index_docs_iter_foreach(struct index_docs_iter *it, void (*cb)(int n, const struct doc *key, int32_t docoff, const char *value, void *), void *v) {
	const struct ib_hdr *hdr = it->db->db_blob;
	const struct ib_document *documents;
	uint64_t ndocs;
	uint64_t i;

	documents = it->db->sorted_docs;
	ndocs = it->db->nsorted_docs;

	log_printf(LOG_DEBUG, "index_docs_iter_foreach: blob %llu", (unsigned long long)ndocs);

	for (i = 0; i < ndocs; i++) {
		const struct ib_document *ibd = &documents[i];
		const char *body = ibd->blob_offs ? ib_getdocval(hdr, ibd) : NULL;

		(*cb)(i, (const struct doc *)(void*)&ibd->id, i, body, v);
	}		
}

struct index_attrs_iter {
	struct index_db *db;
	uint64_t nattrs;
	uint64_t i;
	void *bw_store;
};

struct index_attrs_iter *
index_attrs_iter_open(struct index_db *db, int persistent, unsigned int *nattrs) {
	struct index_attrs_iter *it = zmalloc(sizeof(*it));
	struct ib_hdr *hdr = db->db_blob;

	it->db = db;
	it->nattrs = hdr->ninvattrs;

	if (it->nattrs == 0) {
		free(it);
		*nattrs = 0;
		return NULL;
	}

	*nattrs = it->nattrs;

	return it;
}

/* bw_store is a slist pointer stored before the docarr, thus +/- sizeof(void*) */
#define BWS_ALLOC(sz) ((void*)((char*)xmalloc(sizeof(void*) + sz) + sizeof(void*)))
#define BWS_VALUE(ptr) ((char*)(ptr) - sizeof(void*))
static void
bw_store_free(void *bw_store) {
	while (bw_store) {
		void *tmp = *(void**)bw_store;
		free(bw_store);
		bw_store = tmp;
	}
}

static void
bw_store_add(void **bw_store, void *val) {
	val = BWS_VALUE(val);
	*(void**)val = *bw_store;
	*bw_store = val;
}

static void
bw_store_replace(void **bw_store, void *val) {
	bw_store_free(*bw_store);
	val = BWS_VALUE(val);
	*(void**)val = NULL;
	*bw_store = val;
}

void
index_attrs_iter_close(struct index_attrs_iter *it) {
	bw_store_free(it->bw_store);
	free(it);
}

static void
get_attr_docs_ver2(struct index_db *db, const struct ib_invattr *attr, const int32_t **docarr, int *dsz) {
	const struct ib_hdr *hdr = db->db_blob;
	const struct ib_doc *da;
	int dasz;
	int32_t *fake_docarr;

	da = ib2_getattrdocs(hdr, attr);
	dasz = attr->docslen;
	int ndocs = dasz / sizeof(*da);
	*docarr = fake_docarr = BWS_ALLOC(ndocs * sizeof(**docarr));
	for (int i = 0 ; i < ndocs ; i++) {
		const struct ib_document id = { .id = da[i] };
		const struct ib_document *document = bsearch(&id, db->sorted_docs, db->nsorted_docs, sizeof(*db->sorted_docs), docs_cmp);
		fake_docarr[i] = document - db->sorted_docs;
		assert(i == 0 || fake_docarr[i] >= fake_docarr[i - 1]);
	}

	*dsz = ndocs * sizeof(**docarr);
}


static void
get_attr_docs(struct index_db *db, const struct ib_invattr *attr, const int32_t **docarr, int *dsz) {
	const struct ib_hdr *hdr = db->db_blob;
	*docarr = ib_getattrdocs(hdr, attr);
	*dsz = attr->docslen;
}

void
index_attrs_iter_get(struct index_attrs_iter *it, const char **key, int *ksz, const int32_t **docarr, int *dsz) {
	const struct ib_hdr *hdr = it->db->db_blob;
	const struct ib_invattr *attr = &((ib_getinvattrs(hdr))[it->i]);

	if (key) {
		*key = ib_getattrkey(hdr, attr);
		*ksz = strlen(*key) + 1;
	}
	if (docarr) {
		if (hdr->version >= IB_VERSION3) {
			get_attr_docs(it->db, attr, docarr, dsz);
		} else {
			get_attr_docs_ver2(it->db, attr, docarr, dsz);
			bw_store_replace(&it->bw_store, (void*)*docarr);
		}
	}
}

struct index_attrs_iter *
index_attrs_iter_next(struct index_attrs_iter *it) {
	if (++it->i == it->nattrs) {
		index_attrs_iter_close(it);
		return NULL;
	}

	return it;
}

void
index_attrs_iter_foreach(struct index_attrs_iter *it, void (*cb)(int n, const char *key, const int32_t *, int ndocs, void *), void *v) {
	const struct ib_hdr *hdr = it->db->db_blob;
	const struct ib_invattr *attrs = ib_getinvattrs(hdr);
	uint64_t i;

	log_printf(LOG_DEBUG, "index_attrs_iter_foreach: blob %llu", (unsigned long long)hdr->ninvattrs);
	for (i = 0; i < hdr->ninvattrs; i++) {
		const int32_t *docarr;
		int dsz;

		if (hdr->version >= IB_VERSION3) {
			get_attr_docs(it->db, &attrs[i], &docarr, &dsz);
		} else {
			get_attr_docs_ver2(it->db, &attrs[i], &docarr, &dsz);
			bw_store_add(&it->bw_store, (void*)docarr);
		}
		(*cb)(i, ib_getattrkey(hdr, &attrs[i]), docarr, dsz / sizeof(int32_t), v);
	}
}

struct index_words_iter {
	struct index_db *db;
	uint64_t nwords;
	uint64_t i;
	void *bw_store;
};

struct index_words_iter *
index_words_iter_open(struct index_db *db, int persistent, unsigned int *nwords, size_t *tot_word_len) {
	struct index_words_iter *it = zmalloc(sizeof(*it));
	struct ib_hdr *hdr = db->db_blob;

	it->db = db;
	it->nwords = hdr->ninvwords;

	if (it->nwords < 1) {
		if (tot_word_len)
			*tot_word_len = 0;
		*nwords = 0;
		free(it);
		return NULL;
	}

	if (tot_word_len)
		*tot_word_len = hdr->total_word_len + it->nwords;

	*nwords = it->nwords;

	return it;
}

void
index_words_iter_close(struct index_words_iter *it) {
	free(it->bw_store);
	free(it);
}

static void
get_word_docs_ver2(struct index_db *db, const struct ib_invword *word, const struct docindex **docarr, int *dsz) {
	const struct ib_hdr *hdr = db->db_blob;
	const struct ib2_docindex *da;
	struct docindex *fake_docarr;
	int dasz;

	da = ib2_getworddocs(hdr, word);
	dasz = word->docslen;
	int ndocs = dasz / sizeof(*da);
	*docarr = fake_docarr = BWS_ALLOC(ndocs * sizeof(**docarr));
	for (int i = 0 ; i < ndocs ; i++) {
		const struct ib_document id = { .id = da[i].doc };
		const struct ib_document *document = bsearch(&id, db->sorted_docs, db->nsorted_docs, sizeof(*db->sorted_docs), docs_cmp);
		fake_docarr[i].docoff = document - db->sorted_docs;
		fake_docarr[i].ptr = da[i].posptr;
		assert(i == 0 || fake_docarr[i].docoff >= fake_docarr[i - 1].docoff);
	}

	*dsz = ndocs * sizeof(**docarr);
}

static void
get_word_docs(struct index_db *db, const struct ib_invword *word, const struct docindex **docarr, int *dsz) {
	const struct ib_hdr *hdr = db->db_blob;

	*docarr = ib_getworddocs(hdr, word);
	*dsz = word->docslen;
}

void
index_words_iter_get(struct index_words_iter *it, const char **key, int *ksz, const struct docindex **docarr, int *dsz, const struct docpos **docpos, int *dpsz) {
	const struct ib_hdr *hdr = it->db->db_blob;
	const struct ib_invword *word = &((ib_getinvwords(hdr))[it->i]);
	const struct docindex *da;
	int dasz;

	if (key) {
		*key = ib_getwordkey(hdr, word);
		*ksz = strlen(*key) + 1;
	}
	if (hdr->version >= IB_VERSION3) {
		get_word_docs(it->db, word, &da, &dasz);
	} else {
		get_word_docs_ver2(it->db, word, &da, &dasz);
		bw_store_replace(&it->bw_store, (void*)da);
	}
	if (docarr) {
		*docarr = da;
		*dsz = dasz;
	}
	if (docpos) {
		unsigned int i;

		*docpos = db_getwordpos(it->db, word);
		*dpsz = 0;
		for (i = 0 ; i < dasz / sizeof(*da) ; i++) {
			if (da[i].ptr != INDEX_DOCINDEX_NO_POS)
				*dpsz += (*docpos)[da[i].ptr].pos + 1;
		}
		*dpsz *= sizeof(**docpos);
	}
}

struct index_words_iter *
index_words_iter_next(struct index_words_iter *it) {
	if (++it->i == it->nwords) {
		index_words_iter_close(it);
		return NULL;
	}
	return it;
}

void
index_words_iter_foreach(struct index_words_iter *it, void (*cb)(int, const char *, const struct docindex *, int, const struct docpos *, void *), void *v) {
	unsigned int i;
	const struct ib_hdr *hdr = it->db->db_blob;
	const struct ib_invword *words = ib_getinvwords(hdr);

	log_printf(LOG_DEBUG, "index_words_iter_foreach: blob %llu", (unsigned long long)hdr->ninvwords);

	for (i = 0; i < hdr->ninvwords; i++) {
		const struct docindex *da;
		int dasz;
		const char *word = ib_getwordkey(hdr, &words[i]);
		if (hdr->version >= IB_VERSION3) {
			get_word_docs(it->db, &words[i], &da, &dasz);
		} else {
			get_word_docs_ver2(it->db, &words[i], &da, &dasz);
			bw_store_add(&it->bw_store, (void*)da);
		}
		(*cb)(i, word, da, dasz / sizeof(struct ib_docindex), db_getwordpos(it->db, &words[i]), v);
	}
}

