#ifndef INDEX_H
#define INDEX_H

#include <sys/types.h>
#include <tchdb.h>

#include <pthread.h>

#include "queue.h"
#include "timer.h"
#include "index_blob.h"
#include "sha1.h"
#include "avl.h"

struct doc {
	int id;
	int order;
	union {
		int suborder;
		float suborder_f;
	};
};

struct document {
	struct doc id;
	TCMAP *words;
	TCLIST *attrs_indexed;
	TCMAP *attrs_printed;
	int32_t docoff;
};

#define INDEX_DOCINDEX_NO_POS 0xFFFFFFFF

struct docindex {
	int32_t docoff;
	uint32_t ptr;
};

#define NULL_DOCOFF -1
#define VALID_DOCOFF(d) ((d) >= 0)

#define INDEX_DOCPOS_PRIVATE_MASK 0xFF00

struct docpos {
	u_int16_t flags;
	u_int16_t pos;
	u_int16_t rel_boost;
} __attribute__((packed));

struct blob_writer;
struct docpos_collector;
struct bconf_node;
struct index_db {
	int malloced;
	/* Permanent storage */
	void *db_blob;
	size_t db_blob_size;
	int db_blob_munmap;

	/* Converted versions of earlier versions of blob structures. */
	struct docpos *db_docpos_rewrite;

	/* Cache/temporary storage before flush. */
	struct avl_node *invword_docs;
	int ninvword_docs;
	struct avl_node *invattr_docs;
	int ninvattr_docs;

	/* blob writer */
	int blob_fd;
	struct ib_hdr b_hdr;
	struct blob_writer *bw;
	struct ib_document *b_docs;
	uint64_t b_docs_sz;
	uint64_t b_docs_off;
	struct ib_invattr *b_attrs;
	uint64_t b_attrs_sz;
	uint64_t b_attrs_off;
	struct docpos_collector *b_dc;
	struct ib_invword *b_words;
	uint64_t b_words_sz;
	uint64_t b_words_off;
	uint64_t b_nsubdocs;

	struct timer_instance *ti;

	int32_t *docoff_map;

	/*
	 * Notice that this is not a copy of the global bconf
	 * just an internal storage of key-values for meta data.
	 */
	struct bconf_node *meta;

	/* cached conf footprint. */
	char *conf_footprint;

	/* cached conf header */
	char *conf_header;
	int conf_size;

	/* cached attr header */
	char *header;

	/* cached checksum */
	char *checksum;

	/*
	 * cached document array.
	 * Also used as a Version 2 compatibility layer.
	 */
	const struct ib_document *sorted_docs;
	uint64_t nsorted_docs;
	int free_sorted_docs;
};

enum {
	INDEX_READER = 0x01,
	INDEX_WRITER = 0x02,
	INDEX_FILE = 0x04
};
enum {TUNE_INDEX, TUNE_SEARCH};

enum iops {IOP_AND, IOP_OR, IOP_NOT};

#define AATTR 1
#define AIND 2

#define INDEX_CLOSE_SKIP_SORT 0x1 /* If you know the document array is already sorted. */

struct index_db* open_index(const char *base, int mode);
int open_indexes(const char *base, int tuning, int update, struct index_db **iret, int *ni);
int close_index(struct index_db *, int close_flags);

struct index_db *open_mem_index_reader(void *, size_t, int do_munmap);
struct index_db *open_mem_index_writer(void);
int index_writer_downgrade_to_reader(struct index_db *db);

int index_prime_update(struct index_db *ndb, struct index_db *odb);
struct index_db *index_prime_new(const char *, int shard, int update, int mode);

struct document *open_doc(struct index_db *);
int store_doc(struct index_db *, struct document *, int);
int close_doc(struct document *doc);
int delete_doc(struct index_db *, u_int32_t);
int edit_doc(struct index_db *, struct document*);

void doc_add_word(struct document *, const char *word, int wl, const struct docpos *pos);

int doc_add_attr(struct document *doc, const char *name, const char *value, int);
int doc_add_attr_len(struct document *doc, const char *name, size_t nlen, const char *value, size_t vlen, int);
int doc_has_attribute(struct document *doc, const char *name);
int doc_num_attributes(struct document *doc);

int index_is_stopword(struct index_db *db, const char *word);
void add_stopword(struct index_db *db, const char *word);
int index_get_oper(struct index_db *db, const char *lang, const char *word);
void index_add_oper(struct index_db *db, const char *lang, const char *word, uint8_t op);

void index_add_conf(struct index_db *db, const char *key, const char *value);
const char* index_get_conf_footprint(struct index_db *db);
const char *index_get_config(struct index_db *, size_t *);
void index_debug_set_conf_rev(struct index_db *, const char *);

const char *index_get_doc_header(struct index_db *);
const char *index_get_from_id(struct index_db *);
const char *index_get_to_id(struct index_db *);
void index_set_from_id(struct index_db *, const char *);
void index_set_to_id(struct index_db *, const char *);

const char *index_get_timestamp(struct index_db *db);
void index_set_timestamp(struct index_db *db, time_t ts);

const char *index_get_checksum_string(struct index_db *db);


int key_doc_cmp_op(const void *a, const void *b, void *op);


static inline int index_docoff_is_subdoc(struct index_db *, int32_t docoff) FUNCTION_PURE ARTIFICIAL;
static inline int
index_docoff_is_subdoc(struct index_db *db, int32_t docoff) {
	return db->sorted_docs[docoff].id.suborder < 0;
}

static inline int index_doc_cmp(const void *a, const void *b) FUNCTION_PURE ARTIFICIAL;
static inline int
index_doc_cmp(const void *a, const void *b) {
	const struct doc *da = (const struct doc*)a;
	const struct doc *db = (const struct doc*)b;

	if (da->order != db->order)
		return db->order - da->order;
	return db->id - da->id;
}

static inline int index_docoff_cmp(const void *a, const void *b) FUNCTION_PURE ARTIFICIAL;
static inline int
index_docoff_cmp(const void *a, const void *b) {
	int32_t *da = (int32_t*)a;
	int32_t *db = (int32_t*)b;

	return *da - *db;
}

uint64_t index_get_num_docs(struct index_db *);
uint64_t index_get_num_subdocs(struct index_db *db);
const struct doc *index_get_doc(struct index_db *, int32_t docoff) FUNCTION_PURE; /* Note: NOT idx */

struct index_docs_iter;
struct index_docs_iter *index_docs_iter_open(struct index_db *, int persistent, unsigned int *);
void index_docs_iter_close(struct index_docs_iter *);
void index_docs_iter_foreach(struct index_docs_iter *, void (*)(int n, const struct doc *key, int32_t docoff, const char *value, void *), void *);
void index_docs_iter_get(struct index_docs_iter *, const struct doc **key, int *ksz, const char **value, int *vsz, int32_t *docoff);
struct index_docs_iter *index_docs_iter_next(struct index_docs_iter *);

struct index_attrs_iter;
struct index_attrs_iter *index_attrs_iter_open(struct index_db *, int persistent, unsigned int *);
void index_attrs_iter_close(struct index_attrs_iter *);
void index_attrs_iter_foreach(struct index_attrs_iter *, void (*)(int n, const char *key, const int32_t *docarr, int ndocs, void *), void *);
void index_attrs_iter_get(struct index_attrs_iter *, const char **key, int *ksz, const int32_t **docarr, int *dsz);
struct index_attrs_iter *index_attrs_iter_next(struct index_attrs_iter *);

struct index_words_iter;
struct index_words_iter *index_words_iter_open(struct index_db *, int persistent, unsigned int *, size_t *);
void index_words_iter_close(struct index_words_iter *);
void index_words_iter_foreach(struct index_words_iter *, void (*)(int n, const char *key, const struct docindex *, int ndocs, const struct docpos *, void *), void *);
void index_words_iter_get(struct index_words_iter *, const char **key, int *ksz, const struct docindex **docarr, int *dsz, const struct docpos **, int *dpsz);
struct index_words_iter *index_words_iter_next(struct index_words_iter *);

int index_blob_store_doc(struct index_db *, const struct doc *, const char *, size_t, int32_t *out_docoff);
int index_blob_sync_docs(struct index_db *, int);

int index_blob_store_attr(struct index_db *, const char *, int, const int32_t *, int);
int index_blob_sync_attrs(struct index_db *);

int index_blob_store_word(struct index_db *, const char *, int, struct docindex *, int, const struct docpos *, int);
int index_blob_sync_words(struct index_db *);
int index_blob_sync_hdr(struct index_db *);

/*
 * Compatiblity converter.
 * We have two different versions of the docpos struct, so open_index
 * will create a converted version of it. This is the way to access
 * the converted version.
 *
 * The way indexes have been created all of them point to the same docpos struct.
 */
static inline const struct docpos *
db_getwordpos(struct index_db *db, const struct ib_invword *word) {
	if (db->db_docpos_rewrite) {
		return db->db_docpos_rewrite;
	}
	return ib_getwordpos((struct ib_hdr *)db->db_blob, word);
}

#endif
