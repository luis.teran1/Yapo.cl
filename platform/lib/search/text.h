#ifndef TEXT_H
#define TEXT_H

#ifdef __cplusplus
extern "C" {
#endif

#define POSFLAG_SUBJECT 0x0100
#define POSFLAG_BODY 0x0200

struct document;
struct synize;
struct bconf_node;
struct langmap;
struct syn_regexes;
struct docpos;

/*
 * add_text divides up a string into various tokens.
 * The ATW flags/constants lets us know what those tokens are.
 *
 *  ATW_WORD - The main searchable word. Typically this will be what
 *             comes between two whitespace with some extra weird
 *             characters stripped. This is what we want to be
 *             searchable in phrases.
 *
 *  ATW_STEM - The stem of ATW_MAIN. If hunspell knows how to stem the
 *             main word, this will be it. This will also be
 *             searchable in phrases.
 *
 *  ATW_LANGMAP - If langmap matches a word, this will be it.
 *
 *  ATW_SYN - The synonym for ATW_MAIN from syn_regexes.
 *
 *  ATW_SYNIZE - The synonym for ATW_MAIN from synize.
 *
 *  ATW_NEXT - Called without a word to indicate that we're done with
 *             a word.
 *
 *  ATW_SECONDARY - If ATW_MAIN could be further split into separate
 *                  tokens (like words with dashes in them) this flag
 *                  be set on any further matches (recursively).
 */
#define ATW_WORD	0x001
#define ATW_STEM	0x002
#define ATW_LANGMAP	0x003
#define ATW_SYN		0x004
#define ATW_SYNIZE	0x005
#define ATW_NEXT	0x006
/*
 * This is flag, please make
 * sure that it is above the token types above.
 */
#define ATW_SECONDARY	0x100

/*
 * Two query callbacks.
 *
 * For now we also use the same callback to let the text
 * tokenizer query the backend for various things. This is
 * only used by synize and might be broken out to a separate
 * callback at some point in the future if necessary.
 */
#define ATW_Q_ATTR		0x10001
#define ATW_Q_WORD_EXISTS	0x10002

struct add_text_config {
	int min_wordlen;
	int max_wordlen;
	void *hun_obj;
	struct synize *synizer;
	struct langmap *langmap;
	void *cfdata;
	struct syn_regexes *syn_regexes;
	const char *lang;
	const char *(*word_cb)(void *, int, const char *, int);
};

struct syn_regexes *init_syn_regexes(struct bconf_node *node);
void free_syn_regexes(struct syn_regexes *);
char *syn_regex_update(struct syn_regexes *, const char *text, void *cfdata, void (*match_cb)(const char *word, int match, void *cbarg), void *cbarg);

void split_text(struct add_text_config *atc, void *v, const char *text);
void add_text(struct add_text_config *atc, struct document *doc, const char *text, struct docpos *dpos);
const char *doc_atc_add_word(void *, int, const char *, int);

#ifdef __cplusplus
}
#endif

#endif
