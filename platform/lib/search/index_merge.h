#include <inttypes.h>

struct index_db;
struct index_merge_state {
	struct index_db *to;
	struct index_db *from_db[256];
	int nfrom;
	int32_t *from_docoff_map[256];
	uint32_t *deleted;
	int delsz;
	int update;
	int create;
};

struct timer_instance;
int index_perform_merge(struct index_merge_state *, struct timer_instance *);
int index_merge_open_verify_delta_match(struct index_db *base, struct index_db *delta);
