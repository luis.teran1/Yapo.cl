#ifndef HUNSTEM_H
#define HUNSTEM_H

#ifdef __cplusplus
extern "C" {
#endif

void* hun_setup(const char *aff, const char *dict);
int hun_stem(void *H, char ***stem_list, const char *word);
int hun_suggest(void *H, char ***sugg_list, const char *word);
int hun_spell_ok(void *H, const char *word);
void hun_free_list(void *H, char ***slst, int n);
void hun_clear(void *H);

#ifdef __cplusplus
}
#endif

#endif
