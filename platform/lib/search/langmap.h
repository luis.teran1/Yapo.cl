#ifndef _LANGMAP_H_
#define _LANGMAP_H_

#ifdef __cplusplus
extern "C" {
#endif

struct langmap;

#define LANGMAP_WORD_PREFIX "_l"

struct langmap *langmap_open(const char *fname);
void langmap_close(struct langmap *);
char *langmap_lookup(struct langmap *, char *buf, size_t bufsz, const char *lang, const char *word);
char *langmap_key(char *buf, size_t bufsz, const char *lang, const char *word);

#ifdef __cplusplus
}
#endif

#endif
