#include <sys/types.h>

#include <string.h>
#include <assert.h>

#include "strl.h"
#include "index.h"
#include "util.h"
#include "timer.h"
#include "logging.h"
#include "bconf.h"
#include "index_merge.h"

static int
del_sortcomp(const void *av, const void *bv) {
	const int32_t *a = av, *b = bv;
	return *a - *b;
}

static int
del_lookup(struct index_merge_state *ind, const struct doc *doc) {
	int i, ns, no;
	int32_t res;
	u_int32_t id = doc->id;
       
	if (doc->suborder < 0)
		id = - doc->suborder;

	if (ind->deleted == NULL)
		return 0;

	/*
	 * Binary search in the del array.
	 */
	no = 0;
	ns = ind->delsz;

	while (ns > 0) {
		int even = ~ns & 1;

		ns /= 2;
		i = no + ns;

		res = ind->deleted[i] - id;
		if (res == 0)
			return 1;
		if (res > 0)
			continue;
		no += ns + 1;
		ns -= even;
	}

	return 0;
}

static int32_t
map_offset(struct index_merge_state *ind, int fromidx, int32_t off) {
	return ind->from_docoff_map[fromidx][off];
}

static int
merge_meta(struct index_merge_state *ind) {
	struct bconf_node *from_meta = ind->from_db[ind->nfrom - 1]->meta;

	bconf_merge_prefix(&ind->to->meta, "stopwords", bconf_get(from_meta, "stopwords"));
	bconf_merge_prefix(&ind->to->meta, "opers", bconf_get(from_meta, "opers"));
	bconf_merge_prefix(&ind->to->meta, "counters", bconf_get(from_meta, "counters"));
	bconf_merge_prefix(&ind->to->meta, "attr", bconf_get(from_meta, "attr"));
	bconf_merge_prefix(&ind->to->meta, "conf", bconf_get(from_meta, "conf"));

	index_set_to_id(ind->to, index_get_to_id(ind->from_db[ind->nfrom - 1]));
	if (bconf_get(from_meta, "state.timestamp"))
		bconf_add_data(&ind->to->meta, "state.timestamp", bconf_get_string(from_meta, "state.timestamp"));

	/*
	 * If merging a delta index, get all the deleted doc ids to check for ads that should be ignored during merge
	 */
	if (ind->update) {
		struct bconf_node *dn, *n;
		struct index_docs_iter *it;
		unsigned int ndocs = 0;

		dn = bconf_get(from_meta, "deleted_docs");
		it = index_docs_iter_open(ind->from_db[ind->nfrom - 1], 0, &ndocs);

		if (dn != NULL || it != NULL) {
			int i;

			ind->deleted = xmalloc(sizeof(*ind->deleted) * ((dn ? bconf_count(dn) : 0) + ndocs));
			for (i = 0; dn != NULL && (n = bconf_byindex(dn, i)); i++) {
				unsigned long d;

				d = strtoul(bconf_key(n), NULL, 10);
				if (d == 0 || d >= UINT32_MAX) {
					log_printf(LOG_CRIT, "Invalid deleted doc %s", bconf_key(n));
					return -1;
				}
				ind->deleted[i] = d;
			}
			for (; it != NULL; it = index_docs_iter_next(it), i++) {
				const char *value;
				const struct doc *key;
				int ksz, vsz;
				int32_t docoff;
				index_docs_iter_get(it, &key, &ksz, &value, &vsz, &docoff);
				ind->deleted[i] = key->id;
			}
			ind->delsz = i;
			qsort(ind->deleted, ind->delsz, sizeof(uint32_t), del_sortcomp);
			log_printf(LOG_DEBUG, "Will have to delete %u documents", ind->delsz);
		}
	}

	return 0;
}

static int
merge_doc(struct index_merge_state *ind) {
	struct index_docs_iter *docs_curs[256];
	int curleft;
	int i;

	/*
	 * Initialize cursors for all the indexes
	 */
	curleft = 0;
	for (i = 0; i < ind->nfrom; i++) {
		unsigned int ndocs;

		docs_curs[i] = index_docs_iter_open(ind->from_db[i], 0, &ndocs);
		if (docs_curs[i] && ndocs > 0)
			curleft++;
		ind->from_docoff_map[i] = zmalloc(ndocs * sizeof(**ind->from_docoff_map));
	}

	while (curleft) {
		const struct doc *curdoc = NULL;
		const char *curval = NULL;
		const struct doc *key;
		int32_t curoff = -1;
		int curvsz = 0;
		int from_update = 0;

		/*
		 * Find the lowest doc, and collect the cursors from all the indexes pointing at the same doc
		 */
		int ncurs = 0;
		int curs[256];
		int cur_offs[256];
		for (i = 0; i < ind->nfrom; i++) {
			const char *value;
			int ksz, vsz;
			int32_t off;
			int cmp;

			if (docs_curs[i] == NULL)
				continue;
			index_docs_iter_get(docs_curs[i], &key, &ksz, &value, &vsz, &off);
			if (curdoc == NULL || (cmp = index_doc_cmp(curdoc, key)) > 0) {
				ncurs = 0;
				cmp = 0;
			}
			/* We always want the value from the last of the indexes because it's the latest delta. */
			if (cmp == 0) {
				curdoc = key;
				curval = value;
				curvsz = vsz;
				curoff = off;
				cur_offs[ncurs] = off;
				curs[ncurs++] = i;
				if (i == ind->nfrom - 1)
					from_update = 1;
			}
		}

		if (ncurs > 1 && !ind->update) {
			log_printf(LOG_CRIT, "index_merge_doc: Duplicate document but not updating");
			goto fail;
		}

		key = curdoc;
		if (!key || !curval) {
			log_printf(LOG_CRIT, "index_merge_doc: failed to get the current cursor key or value");
			goto fail;
		}

		if (from_update || !del_lookup(ind, key)) {
			/* Save the document to the output index */
			int32_t newoff;
			if (index_blob_store_doc(ind->to, key, curval, curvsz, &newoff)) {
				log_printf(LOG_CRIT, "index_merge_doc: blob_store_doc");
				goto fail;
			}

			/* As mentioned above, the valid document is from the last cursor. */
			ind->from_docoff_map[curs[ncurs - 1]][curoff] = newoff;
		} else {
			assert(ncurs == 1);
			ind->from_docoff_map[curs[ncurs - 1]][curoff] = -1;
		}

		for (i = 0 ; i < ncurs ; i++) {
			if (i < ncurs - 1) {
				ind->from_docoff_map[curs[i]][cur_offs[i]] = -1;
			}
			if ((docs_curs[curs[i]] = index_docs_iter_next(docs_curs[curs[i]])) == NULL)
				curleft--;
		}
	}

	if (ind->to->bw) {
		if (index_blob_sync_docs(ind->to, INDEX_CLOSE_SKIP_SORT)) {
			log_printf(LOG_CRIT, "index_merge_doc: blob_sync_docs");
			goto fail;
		}
	}

	return 0;
fail:
	for (i = 0; i < ind->nfrom; i++) {
		if (docs_curs[i] == NULL)
			continue;
		index_docs_iter_close(docs_curs[i]);
	}
	return -1;
}

static int
merge_invwords(struct index_merge_state *ind) {
	struct index_words_iter *iw_curs[256];
	int curleft, i;

	/*
	 * Initialize cursors for all the indexes
	 */
	curleft = 0;
	for (i = 0; i < ind->nfrom; i++) {
		unsigned int nwords;

		if ((iw_curs[i] = index_words_iter_open(ind->from_db[i], 0, &nwords, NULL)))
			curleft++;
	}

	/*
	 * Continue as long there are cursors left.
	 */
	while (curleft) {
		const struct docindex *documents[256];
		struct docindex *alldocuments;
		const char *curkey = NULL;
		int totndocs, totndocpos;
		const struct docpos *docpos[256];
		int curkeylen = 0;
		int ndocpos[256];
		struct docpos *alldocpos;
		int ndocs[256];
		int curs[256];
		int dind[256];
		int ncurs;
		int posc;
		int di;
		int i;

		/*
		 * Find the lowest key, and collect the cursors from all the indexes pointing at the same key
		 */
		ncurs = 0;
		for (i = 0; i < ind->nfrom; i++) {
			const char *key;
			int cmp;
			int kl;

			if (iw_curs[i] == NULL)
				continue;
			index_words_iter_get(iw_curs[i], &key, &kl, NULL, NULL, NULL, NULL);
			if (curkey == NULL || (cmp = strcmp(curkey, key)) > 0) {
				curkey = key;
				curkeylen = kl;
				ncurs = 0;
				cmp = 0;
			}
			if (cmp == 0) {
				curs[ncurs++] = i;
			}
		}

		totndocs = totndocpos = 0;
		for (i = 0; i < ncurs; i++) {
			index_words_iter_get(iw_curs[curs[i]], NULL, NULL, &documents[i], &ndocs[i], &docpos[i], &ndocpos[i]);
			ndocs[i] /= sizeof(documents[i][0]);
			totndocs += ndocs[i];
			ndocpos[i] /= sizeof(docpos[i][0]);
			totndocpos += ndocpos[i];
			dind[i] = 0;
		}

		alldocuments = xmalloc(sizeof(*alldocuments) * totndocs + sizeof(*alldocpos) * totndocpos);
		if (totndocpos)
			alldocpos = (struct docpos *)(alldocuments + totndocs);
		else
			alldocpos = NULL;

		/*
		 * Loop through all the documents and copy in order, lowest first
		 */
		di = posc = 0;
		for (i = 0; i < totndocs; i++) {
			const struct docpos *pos;
			int lowest = -1;
			int32_t lowestoff = -1;
			int j;
			
			/*
			 * Find the lowest document
			 */
			for (j = 0; j < ncurs; j++) {
				int32_t joff = -1;

				/* Skip deleted documents. */
				while (dind[j] < ndocs[j] && ((joff = map_offset(ind, curs[j], documents[j][dind[j]].docoff)) == -1)) {
					dind[j]++;
				}
				if (joff == -1) {
					continue;
				}
				if (lowest == -1 || joff < lowestoff) {
					lowest = j;
					lowestoff = joff;
				}
			}
			if (lowest == -1)
				continue;

			/* Double check that the sort order isn't violated. */
			if (di > 0 && alldocuments[di - 1].docoff > lowestoff) {
				log_printf(LOG_CRIT, "invwords merge: sort order violated (%.*s): %d > %d", curkeylen, curkey, alldocuments[di - 1].docoff, lowestoff);
				free(alldocuments);
				goto fail;
			}

			alldocuments[di].docoff = lowestoff;
			alldocuments[di].ptr = documents[lowest][dind[lowest]].ptr;

			if (documents[lowest][dind[lowest]].ptr != INDEX_DOCINDEX_NO_POS) {
				pos = &docpos[lowest][documents[lowest][dind[lowest]].ptr];
				if (pos->pos || pos->rel_boost) {
					alldocuments[di].ptr = posc;
					memcpy(&alldocpos[posc], pos, (pos->pos + 1) * sizeof(*pos));
					posc += pos->pos + 1;
				} else
					alldocuments[di].ptr = INDEX_DOCINDEX_NO_POS;
			} else {
				alldocuments[di].ptr = INDEX_DOCINDEX_NO_POS;
			}

			di++;
			dind[lowest]++;
		}

		/*
		 * di will remain 0 if a word was removed by a merge.
		 */
		if (di > 0) {
			if (index_blob_store_word(ind->to, curkey, curkeylen, alldocuments, di, alldocpos, posc)) {
				log_printf(LOG_CRIT, "index_merge_invwords: blob_store_word: %m");
				free(alldocuments);
				goto fail;
			}
		}

		free(alldocuments);

		/*
		 * For each of the cursor pointing at the lowest key
		 * Advance to the next invword_document.
		 */
		for (i = 0 ; i < ncurs ; i++) {
			if ((iw_curs[curs[i]] = index_words_iter_next(iw_curs[curs[i]])) == NULL)
				curleft--;
		}
	}
	
	if (index_blob_sync_words(ind->to)) {
		log_printf(LOG_CRIT, "index_merge_invwords: blob_sync_words: %m");
		return -1;
	}
	return 0;
fail:
	for (i = 0; i < ind->nfrom; i++) {
		index_words_iter_close(iw_curs[i]);
	}
	return -1;
}

static int
merge_invattrs(struct index_merge_state *ind) {
	struct index_attrs_iter *ia_curs[256];
	int curleft, i;

	/*
	 * Initialize cursors for all the indexes
	 */
	curleft = 0;
	for (i = 0; i < ind->nfrom; i++) {
		unsigned int nattrs;
		if ((ia_curs[i] = index_attrs_iter_open(ind->from_db[i], 0, &nattrs)))
			curleft++;
	}

	/*
	 * Continue as long there is cursors
	 */
	while (curleft) {
		const int32_t *documents[256];
		int32_t *alldocuments;
		const char *curkey = NULL;
		int totndocs;
		int curkeylen = 0;
		int ndocs[256];
		int curs[256];
		int dind[256];
		int ncurs;
		int di;
		int i;

		/*
		 * Find the lowest key, and collect the cursors from all the indexes pointing at the same key
		 */
		ncurs = 0;
		for (i = 0; i < ind->nfrom; i++) {
			const char *key;
			int cmp;
			int kl;

			if (ia_curs[i] == NULL)
				continue;
			index_attrs_iter_get(ia_curs[i], &key, &kl, NULL, NULL);
			if (curkey == NULL || (cmp = strcmp(curkey, key)) > 0) {
			       curkey = key;
			       curkeylen = kl;
			       ncurs = 0;
			       cmp = 0;
			}
			if (cmp == 0) {
				curs[ncurs++] = i;
			}
		}

		totndocs = 0;
		for (i = 0; i < ncurs; i++) {
			index_attrs_iter_get(ia_curs[curs[i]], NULL, NULL, &documents[i], &ndocs[i]);
			ndocs[i] /= sizeof(documents[i][0]);
			totndocs += ndocs[i];

			dind[i] = 0;
		}

		alldocuments = xmalloc(sizeof(*alldocuments) * totndocs);

		/*
		 * Loop through all the documents and copy in order, lowest first
		 */
		di = 0;
		for (i = 0; i < totndocs; i++) {
			int lowest = -1;
			int j;
			int32_t lowestoff = -1;
			
			/*
			 * Find the lowest document
			 */
			for (j = 0; j < ncurs; j++) {
				int32_t joff = -1;

				/* Skip deleted documents. */
				while (dind[j] < ndocs[j] && (joff = map_offset(ind, curs[j], documents[j][dind[j]])) == -1) {
					dind[j]++;
				}
				if (joff == -1) {
					continue;
				}
				if (lowest == -1 || joff < lowestoff) {
					lowest = j;
					lowestoff = joff;
				}
			}
			if (lowest == -1)
				continue;
			if (di > 0 && alldocuments[di - 1] == lowestoff) {
				log_printf(LOG_WARNING, "Duplicate attribute %.*s in document %d", curkeylen, curkey, lowestoff);
			} else if (di > 0 && alldocuments[di - 1] > lowestoff) {
				log_printf(LOG_CRIT, "invattrs merge: sort order violated (%.*s): %d > %d\n", curkeylen, curkey, alldocuments[di - 1], lowestoff);
				free(alldocuments);
				goto fail;
			} else {
				alldocuments[di++] = lowestoff;
			}
			dind[lowest]++;
		}

		/*
		 * Don't save if there are no new documents
		 */
		if (di > 0) {
			if (index_blob_store_attr(ind->to, curkey, curkeylen, alldocuments, di)) {
				log_printf(LOG_CRIT, "index_merge_invattrs: blob_store_attr: %m");
				free(alldocuments);
				goto fail;
			}
		}

		free(alldocuments);

		/*
		 * For each of the cursor pointing at the lowest key
		 * Advance to the next invattr_document.
		 */
		for (i = 0 ; i < ncurs ; i++) {
			if ((ia_curs[curs[i]] = index_attrs_iter_next(ia_curs[curs[i]])) == NULL)
				curleft--;
		}
	}
	if (index_blob_sync_attrs(ind->to)) {
		log_printf(LOG_CRIT, "index_merge_invattrs: blob_sync_attrs: %m");
		return -1;
	}
	return 0;
fail:
	for (i = 0; i < ind->nfrom; i++) {
		index_attrs_iter_close(ia_curs[i]);
	}
	return -1;
}

int
index_perform_merge(struct index_merge_state *ind, struct timer_instance *ti) {
	int ret = 0;
	int i;

	/* Just to be safe if we bail before merge_doc and the caller hasn't zeroed the state. */
	for (i = 0; i < ind->nfrom; i++) {
		ind->from_docoff_map[i] = NULL;
	}

	log_printf(LOG_DEBUG, "Merging meta");

	timer_handover(ti, "meta");

	if ((ret = merge_meta(ind)))
		goto out;

	log_printf(LOG_DEBUG, "Merging documents");
	timer_handover(ti, "documents");
	if ((ret = merge_doc(ind)))
		goto out;

	log_printf(LOG_DEBUG, "Merging invwords");
	timer_handover(ti, "invwords");
	if ((ret = merge_invwords(ind)))
		goto out;

	log_printf(LOG_DEBUG, "Merging invattrs");
	timer_handover(ti, "invattrs");
	if ((ret = merge_invattrs(ind)))
		goto out;

	if (ind->to->bw) {
		if (index_blob_sync_hdr(ind->to)) {
			log_printf(LOG_CRIT, "index_perform_merge: blob_sync_hdr: %m");
			ret = -1;
			goto out;
		}
	}

out:
	for (i = 0; i < ind->nfrom; i++) {
		free(ind->from_docoff_map[i]);
	}
	free(ind->deleted);
	return ret;
}
