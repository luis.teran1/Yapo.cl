#include <stdio.h>
#include <string.h>
#include <tchdb.h>

#include "langmap.h"

char *
langmap_key(char *buf, size_t bufsz, const char *lang, const char *word)
{
	if (snprintf(buf, bufsz, "_w%s:%s", lang, word) >= (int)bufsz)
		return NULL;
	return buf;
}

struct langmap {
	TCHDB *db;
};

struct langmap *
langmap_open(const char *fname) {
	struct langmap *lm;

	if (!(lm = calloc(sizeof(*lm), 1)))
		return NULL;

	if ((lm->db = tchdbnew()) == NULL) {
		free(lm);
		return NULL;
	}

	if (!tchdbopen(lm->db, fname, HDBOREADER)) {
		tchdbdel(lm->db);
		free(lm);
		return NULL;
	}

	return lm;
}

void
langmap_close(struct langmap *lm) {
	if (lm->db) {
		tchdbclose(lm->db);
		tchdbdel(lm->db);
	}
	free(lm);
}

char *
langmap_lookup(struct langmap *lm, char *buf, size_t bufsz, const char *lang, const char *word) {
	char keybuf[64], *key;
	int sz;

	if ((key = langmap_key(keybuf, sizeof(keybuf), lang, word)) == NULL)
		return NULL;
	if (buf) {
		if ((sz = tchdbget3(lm->db, key, strlen(key), buf, bufsz)) == -1)
			return NULL;
		buf[sz] = '\0';
		return buf;
	} else {
		return tchdbget2(lm->db, key);
	}
}
