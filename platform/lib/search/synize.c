#include <string.h>
#include <stdio.h>
#include "index.h"
#include "bconf.h"
#include "util.h"
#include "logging.h"

#include <sys/types.h>
#include <regex.h>

#include "synize.h"
#include "text.h"

struct synonym {
	regex_t re;
	int regex;
	char *regex_str;
	TCMAP *words;
	TCLIST *add_words;
	char *with;
	char *without;
	TCLIST *cat_required; /* Only used for global_regex_syns.
				 XXX Generalize this to doc attribute required and get rid of all mentions of category. */
	TCLIST *regex_add_matches_list;
	regex_t re_next_word;
	char *regex_next_word_str;
	int add_concat_with_next_word; /* XXX Currently only for regex_next_word synonyms but could be generalized. */
};

struct synize {
	TCMAP *catsyns;
	TCMAP *globals;
	TCLIST *syns;
	TCLIST *global_regex_syns;
};

#define MAX_RE_MATCHES 4

static void
tcmap_iter_cb(TCMAP *words, struct add_text_config *atc, void *v) {
	const char *word;
	int sp;

	if (!words)
		return;

	tcmapiterinit(words);
	while ((word = tcmapiternext(words, &sp))) {
		(*atc->word_cb)(v, ATW_SYNIZE, word, -1);
	}
}

static void
tclist_iter_cb(TCLIST *words, struct add_text_config *atc, void *v) {
	int cnt;

	if (!words)
		return;

	for (cnt = 0; cnt < tclistnum(words); cnt++) {
		int sp;
		const char *word = tclistval(words, cnt, &sp);

		(*atc->word_cb)(v, ATW_SYNIZE, word, -1);
	}
}


static void
apply_synonym(struct add_text_config *atc, void *v, const struct synonym *s, TCLIST *words) {
	const char *word;
	int i;

	if (s->without && (*atc->word_cb)(v, ATW_Q_WORD_EXISTS, s->without, -1) != NULL)
		return;
	if (s->with && (*atc->word_cb)(v, ATW_Q_WORD_EXISTS, s->with, -1) == NULL)
		return;

	if (s->regex) {
		int regex_matched = 0;

		for (i = 0; i < tclistnum(words); i++) {
			int wl;

			word = tclistval(words, i, &wl);

			if (s->regex_add_matches_list) {
				regmatch_t re_matches[MAX_RE_MATCHES];

				if (regexec(&s->re, word, MAX_RE_MATCHES, re_matches, 0) == 0) {
					int cnt;

					if (s->regex_next_word_str) {
						const char *next_word = tclistval(words, i + 1, &wl);

						if (next_word && regexec(&s->re_next_word, next_word, 0, NULL, 0) != 0)
							continue;
						if (s->add_concat_with_next_word) {
							char *concat_word;
							xasprintf(&concat_word, "%s%s", word, next_word);
							(*atc->word_cb)(v, ATW_SYNIZE, concat_word, -1);
							free(concat_word);
						}
					}

					regex_matched = 1;

					for (cnt = 0; cnt < tclistnum(s->regex_add_matches_list); cnt++) {
						int sp;
						const char *match_no_str = tclistval(s->regex_add_matches_list, cnt, &sp);

						if (match_no_str) {
							int match_no = atoi(match_no_str);
							if (match_no > 0 && match_no <= MAX_RE_MATCHES && re_matches[match_no].rm_so != -1) {
								char *match = xstrndup(word + re_matches[match_no].rm_so, re_matches[match_no].rm_eo - re_matches[match_no].rm_so);
								(*atc->word_cb)(v, ATW_SYNIZE, match, -1);
								free(match);
							}
						}
					}
				}
			} else if (regexec(&s->re, word, 0, NULL, 0) == 0) {
				if (s->regex_next_word_str) {
					const char *next_word = tclistval(words, i + 1, &wl);

					if (next_word && regexec(&s->re_next_word, next_word, 0, NULL, 0) != 0)
						continue;
					if (s->add_concat_with_next_word) {
						char *concat_word;
						xasprintf(&concat_word, "%s%s", word, next_word);
						(*atc->word_cb)(v, ATW_SYNIZE, concat_word, -1);
						free(concat_word);
					}
				}

				regex_matched = 1;
			}
		}

		if (regex_matched) {
			tcmap_iter_cb(s->words, atc, v);
			tclist_iter_cb(s->add_words, atc, v);
		}
	} else {
		for (i = 0; i < tclistnum(words); i++) {
			int wl;

			word = tclistval(words, i, &wl);

			if (s->words && tcmapget2(s->words, word)) {
				tcmap_iter_cb(s->words, atc, v);
				break;
			}
		}
	}
}

void
synize(struct add_text_config *atc, void *v, TCLIST *words, struct synize *syn) {
	const char *cat;
	const char *word;
	const char *next_word UNUSED;
	int cnt;
	TCMAP *synmap = NULL;
	const void *tmp;
	const struct synonym *s;
	int wl;
	int nwl;
	int c;

	if ((cat = (*atc->word_cb)(v, ATW_Q_ATTR, "category", -1))) {
		c = atoi(cat);

		if (syn->catsyns) {
			int tl;

			/* XXX The return value here should be a list of all syns for the category */
			tmp = tcmapget(syn->catsyns, (void*)&c, sizeof(c), &tl);
			if (tmp) {
				synmap = *(TCMAP**)tmp;
			}
		}
	}

	if (!syn->globals && !synmap)
		return;

	for (cnt = 0 ; cnt < tclistnum(words) ; cnt++) {
		int tl;

		word = tclistval(words, cnt, &wl);
		next_word = tclistval(words, cnt + 1, &nwl);

		if (synmap) {
			if ((tmp = tcmapget(synmap, word, wl, &tl))) {
				s = *(const struct synonym**)tmp;
				apply_synonym(atc, v, s, words);
			}
		}

		if (syn->globals) {
			if ((tmp = tcmapget(syn->globals, word, wl, &tl))) {
				s = *(const struct synonym**)tmp;
				apply_synonym(atc, v, s, words);
			}
		}

		if (syn->global_regex_syns) {
			int i;
			for (i = 0 ; i < tclistnum(syn->global_regex_syns) ; i++) {
				int tl;
				const void *tmp = tclistval(syn->global_regex_syns, i, &tl);
				s = *(struct synonym**)tmp;

				if (s->cat_required && (!cat || tclistbsearch(s->cat_required, cat, strlen(cat)) == -1)) {
					continue;
				}

				apply_synonym(atc, v, s, words);
			}
		}

	}
}

static void
reg_syn(TCMAP *map, struct synonym *s) {
	if (s->words) {
		const char *word;
		int wl;

		tcmapiterinit(s->words);
		while ((word = tcmapiternext(s->words, &wl))) {
			tcmapput(map, word, wl, (char*)&s, sizeof(s));
		}

		if (s->with)
			tcmapput(map, s->with, strlen(s->with), (char*)&s, sizeof(s));
	}
}

static int
add_synonym(struct synize *syn, struct bconf_node *synonym) {
	const char *cats = bconf_value(bconf_get(synonym, "categories"));
	const char *words = bconf_value(bconf_get(synonym, "words"));
	const char *add_words = bconf_value(bconf_get(synonym, "add_words"));
	const char *with = bconf_value(bconf_get(synonym, "with"));
	const char *without = bconf_value(bconf_get(synonym, "without"));
	const char *regex = bconf_value(bconf_get(synonym, "regex"));
	const char *regex_add_matches = bconf_value(bconf_get(synonym, "regex_add_matches"));
	const char *regex_next_word = bconf_value(bconf_get(synonym, "regex_next_word"));
	const char *add_concat_with_next_word = bconf_value(bconf_get(synonym, "add_concat_with_next_word"));

	struct synonym *s = zmalloc(sizeof(*s));
	TCLIST *wl = NULL;
	TCLIST *cl = NULL;
	tclistpush(syn->syns, (char*)&s, sizeof(s));

	if (words) {
		wl = tcstrsplit(words, ",");
	}

	if (add_words) {
		s->add_words = tcstrsplit(add_words, ",");
	}

	if (cats) {
		cl = tcstrsplit(cats, ",");
	}

	if (regex_add_matches) {
		s->regex_add_matches_list = tcstrsplit(regex_add_matches, ",");
	}

	if (with)
		s->with = xstrdup(with);

	if (without)
		s->without = xstrdup(without);

	if (regex) {
		s->regex = 1;
		s->regex_str = xstrdup(regex);
		if (regcomp(&s->re, regex, REG_EXTENDED|REG_ICASE) != 0) {
			fprintf(stderr, "Failed to compile: %s\n", regex);
			return 1;
		}

		if (regex_next_word) {
			s->regex_next_word_str = xstrdup(regex_next_word);
			if (regcomp(&s->re_next_word, regex_next_word, REG_EXTENDED|REG_ICASE) != 0) {
				fprintf(stderr, "Failed to compile: %s\n", regex_next_word);
				return 1;
			}

			if (add_concat_with_next_word)
				s->add_concat_with_next_word = 1;
		}

	}

	if (words) {
		int cnt;
		s->words = tcmapnew();
		for (cnt = 0 ; cnt < tclistnum(wl) ; cnt++) {
			int wwl;
			const char *word = tclistval(wl, cnt, &wwl);

			tcmapput(s->words, word, wwl, "", 0);
		}
		tclistdel(wl);
	} else if (regex) {
		/* No words, but regex. These needs to be run for all words. */
		if (!syn->global_regex_syns)
			syn->global_regex_syns = tclistnew();

		tclistpush(syn->global_regex_syns, (char*)&s, sizeof(s));
		if (cl) {
			s->cat_required = tclistdup(cl);
		}
	}

	if (!cats) {
		if (!syn->globals)
			syn->globals = tcmapnew();

		reg_syn(syn->globals, s);
	} else {
		int cnt;

		/* Create or use old map for all category syns */
		if (!syn->catsyns)
			syn->catsyns = tcmapnew();

		/* Loop through all categories */
		for (cnt = 0 ; cnt < tclistnum(cl) ; cnt++) {
			int ccl;
			int cat = atoi(tclistval(cl, cnt, &ccl));
			int tl;
			const void *tmp = tcmapget(syn->catsyns, (void*)&cat, sizeof(cat), &tl);
			TCMAP *map;

			if (!tmp) {
				map = tcmapnew();
				/* Put map into catsyns for this category */
				tcmapput(syn->catsyns, (void*)&cat, sizeof(cat), (char *)&map, sizeof(map));
			} else {
				map = *(TCMAP**)tmp;
			}

			/* Register synonym into map for all words */
			reg_syn(map, s);
		}
		tclistdel(cl);
	}

	return 0;
}

/* syn should point to bconf-struct containin synonyms.* */
struct synize*
syn_setup(struct bconf_node *root) {
	int cnt;
	struct synize *s = zmalloc(sizeof(*s));
	struct bconf_node *snode = bconf_get(root, "synonyms");
	int synonyms = bconf_count(snode);
	s->syns = tclistnew();

	log_printf(LOG_INFO, "Adding %d synonyms", synonyms);
	for (cnt = 0 ; cnt < synonyms ; cnt++) {
		if (add_synonym(s, bconf_byindex(snode, cnt)) != 0) {
			fprintf(stderr, "Warning: failed adding synonym %u", cnt);
		}
	}

	return s;
}

static void
clear_synonym(struct synonym *syn) {
	if (syn->regex)
		regfree(&syn->re);
	if (syn->regex_next_word_str)
		regfree(&syn->re_next_word);
	if (syn->words)
		tcmapdel(syn->words);
	if (syn->cat_required)
		tclistdel(syn->cat_required);
	if (syn->add_words)
		tclistdel(syn->add_words);
	if (syn->regex_add_matches_list)
		tclistdel(syn->regex_add_matches_list);

	free(syn->with);
	free(syn->without);
	free(syn->regex_str);
	free(syn->regex_next_word_str);
}

void
syn_clear(struct synize *syn) {
	struct synonym *s;
	const void *tmp;
	const char *key;
	int cnt;

	for (cnt = 0 ; cnt < tclistnum(syn->syns) ; cnt++) {
		int tl;
		const void *tmp = tclistval(syn->syns, cnt, &tl);
		s = *(struct synonym**)tmp;
		clear_synonym(s);
		free(s);
	}

	if (syn->globals) {
		tcmapdel(syn->globals);
	}

	if (syn->catsyns) {
		TCMAP *catmap;
		int kl;
		tcmapiterinit(syn->catsyns);
		while ((key = tcmapiternext(syn->catsyns, &kl))) {
			int tl;
			tmp = tcmapget(syn->catsyns, key, kl, &tl);
			catmap = *(TCMAP**)tmp;
			tcmapdel(catmap);
		}
		tcmapdel(syn->catsyns);
	}

	tclistdel(syn->syns);
	if (syn->global_regex_syns)
		tclistdel(syn->global_regex_syns);

	free(syn);
}
