#include "hunspell.hxx"
#include <pthread.h>
#include "hunstem.h"

static pthread_mutex_t hunspell_mutex;

#ifdef __cplusplus
extern "C" {
#endif

void* hun_setup(const char *aff, const char *dict) {
	pthread_mutex_init(&hunspell_mutex, NULL);
	return new Hunspell(aff, dict);
}


int hun_spell_ok(void *H, const char *word) {
	pthread_mutex_lock(&hunspell_mutex);
	Hunspell *pMS = (Hunspell*)H;
	int rval = pMS->spell(word);
	pthread_mutex_unlock(&hunspell_mutex);
	return rval;
}

int hun_stem(void *H, char ***stem_list, const char *word) {
	pthread_mutex_lock(&hunspell_mutex);
	Hunspell *pMS = (Hunspell*)H;
	int rval =  pMS->stem(stem_list, word);
	pthread_mutex_unlock(&hunspell_mutex);
	return rval;
}

void hun_free_list(void *H, char ***slst, int n) {
	Hunspell *pMS = (Hunspell*)H;
	pMS->free_list(slst, n);
}

int hun_suggest(void *H, char ***slist, const char *word) {
	Hunspell *pMS = (Hunspell*)H;
	return pMS->suggest(slist, word);
}

void hun_clear(void *H) {
	Hunspell *pMS = (Hunspell*)H;
	delete pMS;
	pthread_mutex_destroy(&hunspell_mutex);
}

}

