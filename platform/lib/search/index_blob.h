#ifndef SEARCH_INDEX_BLOB_H
#define SEARCH_INDEX_BLOB_H

struct ib_hdr {
	uint64_t magic;
	uint64_t version;

	uint64_t ndocuments;
	uint64_t documents_off;

	uint64_t ninvattrs;
	uint64_t invattrs_off;

	uint64_t ninvwords;
	uint64_t invwords_off;
	uint64_t total_word_len;

	uint64_t meta_sz;
	uint64_t meta_off;

	uint64_t nsubdocs;
} __attribute__((__packed__));

#define IB_MAGIC        0xa81513a55e1a00ae
#define IB_VERSION0     0x0000000000000000      /* Initial blob version. */
#define IB_VERSION1     0x0000000000000010      /* Meta data in blob. */
#define IB_VERSION2     0x0000000000000020      /* Docpos change. */
#define IB_VERSION3     0x0000000000000030      /* docoff instead of struct doc */
#define IB_VERSION      IB_VERSION3

struct ib_doc {
	uint32_t id;
	uint32_t order;
	int32_t suborder;
} __attribute__((__packed__));

struct ib_document {
	struct ib_doc id;
	uint32_t doclen;
	uint64_t blob_offs;
} __attribute__((__packed__));

struct ib2_docindex {
	struct ib_doc doc;
	uint32_t posptr;
} __attribute__((__packed__));

struct ib_docindex {
	int32_t docoff;
	uint32_t posptr;
} __attribute__((__packed__));

struct ib_docpos_v1 {
	uint16_t flags;
	uint16_t pos;
} __attribute__((__packed__));

struct ib_docpos {
	uint16_t flags;
	uint16_t pos;
	uint16_t rel_boost;
} __attribute__((__packed__));

struct ib_invword {
	uint64_t docslen;
	uint64_t word_offs;
	uint64_t docs_offs;
	uint64_t docpos_offs;
} __attribute__((__packed__));

struct ib_invattr {
	uint64_t docslen;
	uint64_t attr_offs;
	uint64_t docs_offs;
} __attribute__((__packed__));

static inline const struct ib_document *
ib_getdocuments(const struct ib_hdr *hdr) {
	return (const struct ib_document *)(((char *)hdr) + hdr->documents_off);
}

static inline const struct ib_invattr *
ib_getinvattrs(const struct ib_hdr *hdr) {
	return (const struct ib_invattr *)(((char *)hdr) + hdr->invattrs_off);
}

static inline const struct ib_invword *
ib_getinvwords(const struct ib_hdr *hdr) {
	return (const struct ib_invword *)(((char *)hdr) + hdr->invwords_off);
}

static inline const char *
ib_getmeta(const struct ib_hdr *hdr) {
	return ((const char *)hdr) + hdr->meta_off;
}

static inline const char *
ib_getdocval(const struct ib_hdr *hdr, const struct ib_document *doc) {
	return (const char *)(((char *)hdr) + doc->blob_offs);
}

static inline const char *
ib_getattrkey(const struct ib_hdr *hdr, const struct ib_invattr *attr) {
	return (const char *)(((char *)hdr) + attr->attr_offs);
}

static inline const struct ib_doc *
ib2_getattrdocs(const struct ib_hdr *hdr, const struct ib_invattr *attr) {
	return (const struct ib_doc *)(((char *)hdr) + attr->docs_offs);
}

static inline const int32_t *
ib_getattrdocs(const struct ib_hdr *hdr, const struct ib_invattr *attr) {
	return (const int32_t *)(void *)(((char *)hdr) + attr->docs_offs);
}

static inline const char *
ib_getwordkey(const struct ib_hdr *hdr, const struct ib_invword *word) {
	return (const char *)(((char *)hdr) + word->word_offs);
}

static inline const struct ib2_docindex *
ib2_getworddocs(const struct ib_hdr *hdr, const struct ib_invword *word) {
	return (const struct ib2_docindex *)(((char *)hdr) + word->docs_offs);
}

static inline const struct docindex *
ib_getworddocs(const struct ib_hdr *hdr, const struct ib_invword *word) {
	return (const struct docindex *)(((char *)hdr) + word->docs_offs);
}

static inline const struct docpos *
ib_getwordpos(const struct ib_hdr *hdr, const struct ib_invword *word) {
	return (const struct docpos *)(((char *)hdr) + word->docpos_offs);
}

#endif /*SEARCH_INDEX_BLOB_H*/
