#include <ctype.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <unicode/ustring.h>
#include <unicode/ucnv.h>
#include <unicode/unorm.h>
#include <unicode/uchar.h>
#include <sys/stat.h>
#include <sys/mman.h>

#include "charmap.h"
#include "util.h"

struct charmap_utf8
{
	uint32_t magic;
	uint32_t ndst;
	uint32_t nsec;
	struct {
		UChar32 start;
		UChar32 end;
		uint32_t offset;
	} secs[];
} __attribute__((packed));

struct charmap_thread {
	struct charmap *charmap;
	void *cfdata;
};

struct charmap {
	char char_map[256];
	struct charmap_utf8 *charmap_utf8;
	size_t charmap_utf8_sz;
	uint32_t ndst;
};

static void
init_charmap_utf8(struct charmap *cm, int fd) {
	struct stat st;

	if (fstat(fd, &st))
		xerr(1, "init_charmap_utf8: fstat");

	cm->charmap_utf8_sz = st.st_size;
	cm->charmap_utf8 = mmap(NULL, st.st_size, PROT_READ, MAP_PRIVATE, fd, 0);
	if (cm->charmap_utf8 == MAP_FAILED)
		xerr(1, "init_charmap_utf8: mmap");
	cm->ndst = cm->charmap_utf8->ndst;
}

static UChar32
charmap_utf8_lookup(struct charmap *cm, UChar32 ch) {
	int o = 0;
	int n = cm->charmap_utf8->nsec;
	int i;

	while (n) {
		UChar32 *map;
		int even = ~n & 1;

		n /= 2;
		i = o + n;

		if (ch < cm->charmap_utf8->secs[i].start)
			continue;
		if (ch > cm->charmap_utf8->secs[i].end) {
			o += n + 1;
			n -= even;
			continue;
		}

		map = (UChar32*)(void*)((char*)cm->charmap_utf8 + cm->charmap_utf8->secs[i].offset);
		return map[ch - cm->charmap_utf8->secs[i].start];
	}
	return ' ';
}

struct charmap *
init_charmap(const char *file) {
	struct charmap *cm;
	int fd;

	cm = zmalloc(sizeof(*cm));

	fd = open(file, O_RDONLY);
	if (fd < 0)
		xerr(1, "init_charmap: failed to open \"%s\"", file);

	if (read(fd, cm->char_map, 4) != 4)
		xerrx(1, "init_charmap: failed to read 4 bytes from \"%s\"", file);
	if (memcmp(cm->char_map, "UTF8", 4) == 0) {
		init_charmap_utf8(cm, fd);
	} else {
		if (read(fd, cm->char_map + 4, sizeof(cm->char_map) - 4) != sizeof(cm->char_map) - 4)
			xerrx(1, "init_charmap: failed to read charmap from \"%s\"", file);
		if (read(fd, &cm->ndst, sizeof(cm->ndst)) != sizeof(cm->ndst))
			xerrx(1, "init_charmap: failed to read ndst from \"%s\"", file);
	}
	close(fd);

	return cm;
}

void *
init_charmap_thread(struct charmap *cm) {
	struct charmap_thread *ct;

	ct = zmalloc(sizeof(*ct));
	ct->charmap = cm;

	if (cm->charmap_utf8) {
		UErrorCode err = 0;
		UConverter *cnv;

		cnv = ucnv_open("UTF-8", &err);
		if (U_FAILURE(err))
			xerrx(1, "Failed to initialize utf8_cnv: %s", u_errorName(err));
		ct->cfdata = cnv;

	}

	return ct;
}

/*
 * Horrible, horrible hack.
 *
 * The UTF8 converter is indepentend of the charmap and expensive to open.
 * So for every request where the charmap can change, we kinda steal the old
 * converter and make it point to the new charmap. All to avoid rewriting
 * the whole API.
 */
void *
reinit_charmap_thread(struct charmap *cm, void *thread) {
	struct charmap_thread *ct = thread;
	ct->charmap = cm;
	return ct;
}

void
free_charmap_thread(void *thread) {
	struct charmap_thread *ct = thread;

	if (ct->cfdata) {
		ucnv_close(ct->cfdata);
	}
	free(ct);

}

void
free_charmap(struct charmap *cm) {
	if (cm->charmap_utf8) {
		munmap(cm->charmap_utf8, cm->charmap_utf8_sz);
	}
	free(cm);
}

int charmap_is_utf8(struct charmap *cm) {
	return cm->charmap_utf8 ? 1 : 0;
}

char *
casefold(char *dst, size_t dstsz, const char *string, void *thread, int flags) {
	struct charmap_thread *ct = thread;
	struct charmap *cm = ct->charmap;

	if (cm->charmap_utf8) {
		int slen = strlen(string);
		UChar bufa[dstsz];
		UChar bufb[dstsz];
		int l;
		UErrorCode err = 0;
		UConverter *cnv = ct->cfdata;
		int i;

		l = ucnv_toUChars(cnv, bufa, dstsz, string, slen, &err);
		if (U_FAILURE(err))
			xerrx(1, "casefold(%s): error from ucnv_toUChars: %s", string, u_errorName(err));

		/* Do case folding first. */
		l = u_strFoldCase(bufb, dstsz, bufa, l, U_FOLD_CASE_DEFAULT, &err);
		if (U_FAILURE(err))
			xerrx(1, "casefold(%s): error from u_strFoldCase: %s", string, u_errorName(err));

		/* Now convert to NFKC. */
		l = unorm_normalize(bufb, l, UNORM_NFKC, 0, bufa, dstsz, &err);
		if (U_FAILURE(err))
			xerrx(1, "casefold(%s): error from unorm_normalize: %s", string, u_errorName(err));

		/* Lookup in charmap. */
		for (i = 0 ; i < l ; i++) {
			UChar32 ch = bufa[i];
			int washigh = 0;

			if ((ch & 0xFC00) == 0xD800 && i < l - 1 && (bufa[i + 1] & 0xFC00) == 0xDC00) {
				ch = 0x10000 + ((ch & 0x3FF) << 10 | (bufa[i + 1] & 0x3FF));
				washigh = 1;
			}
			ch = charmap_utf8_lookup(cm, ch);

			if (ch != ' ' && (flags & CHARMAP_FILTER_ALNUM)) {
				if (!u_isalnum(ch))
					ch = ' ';
			}

			if (ch >= 0x10000) {
				bufa[i] = 0xD800 + ((ch - 0x10000) >> 10 & 0x3FF);
				bufa[++i] = 0xDC00 + ((ch - 0x10000) & 0x3FF);
			} else {
				bufa[i] = ch;
				if (washigh) {
					memmove(bufa + i + 1, bufa + i + 2, l - i - 2);
					l--;
				}
			}
		}

		/* Finally put the result in dst. */
		l = ucnv_fromUChars(cnv, dst, dstsz - 1, bufa, l, &err);
		if (U_FAILURE(err))
			xerrx(1, "casefold(%s): error from ucnv_fromUChars: %s", string, u_errorName(err));
		dst[dstsz - 1] = '\0';
	} else {
		unsigned char *tmp = (unsigned char*)string;
		char *end = dst + dstsz - 1;
		while (tmp && *tmp && dst < end) {
			*dst = cm->char_map[(int)*tmp++];
			if (flags & CHARMAP_FILTER_ALNUM) {
				if (!isalnum(*dst))
					*dst = ' ';
			}
			if (flags & CHARMAP_TOLOWER)
				*dst = tolower(*dst);
			dst++;
		}
		*dst = '\0';
	}
	return dst;
}

uint32_t
charmap_ndst(struct charmap *cm) {
	return cm->ndst;
}

#ifdef TEST
int
main(int argc, char **argv) {
	int cnt;
	init_charmap(argv[1]);

	for (cnt = 0 ; cnt < 255 ; cnt++) {
		printf("%d %c -> %c \n", cnt, (unsigned char)cnt, char_map[cnt]);
	}
}
#endif
