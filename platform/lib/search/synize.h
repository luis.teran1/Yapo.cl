#ifndef SYNIZE_H
#define SYNIZE_H

#include "tcutil.h"

struct document;
struct synize;
struct bconf_node;
struct add_text_config;
void synize(struct add_text_config *, void *, TCLIST *, struct synize *);
struct synize *syn_setup(struct bconf_node *root);
void syn_clear(struct synize *syn);

#endif
