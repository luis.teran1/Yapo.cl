#ifndef CHARMAP_H
#define CHARMAP_H

#include <sys/types.h>
#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

#define CHARMAP_FILTER_ALNUM (1 << 0)
#define CHARMAP_TOLOWER      (1 << 1)

char* casefold(char *dst, size_t dstsz, const char *string, void *thread, int flags);

struct charmap;
struct charmap *init_charmap(const char *file);
void *init_charmap_thread(struct charmap *);
void *reinit_charmap_thread(struct charmap *, void *);
void free_charmap(struct charmap *);
void free_charmap_thread(void *);

uint32_t charmap_ndst(struct charmap *);
int charmap_is_utf8(struct charmap *);

#ifdef __cplusplus
}
#endif

#endif
