#include <sys/socket.h>
#include <netinet/in.h>
#include <signal.h>
#include <pthread.h>
#include <unistd.h>
#include <string.h>
#include <netdb.h>
#include <stdlib.h>
#include <stdio.h>
#include <limits.h>
#include <errno.h>
#include <assert.h>
#include <stdbool.h>

#include <logging.h>
#include <http_parser.h>

#include <ctemplates.h>
#include <bconf.h>
#include <parse_query_string.h>
#include <controller.h>
#include <sock_util.h>
#include <strl.h>
#include <stat_counters.h>
#include <stat_messages.h>

struct ctrl_handler_int {
	struct ctrl_handler hand;
	uint64_t *cnt;
};

/* This should be struct controller or something, this doesn't describe one thread (as the code was in the beginning), but all the threads. */
struct ctrl_thread {
	struct ctrl_handler_int *handlers;
	struct bconf_node *bconf;
	struct bconf_node *ctrl_conf;
	int nhandlers;
	int l;
	int quit;
	pthread_t *threads;
	struct ct_thread {
		struct ctrl_thread *thr;
		struct stat_message *thr_state;
		struct stat_message *handler_name;
		uint64_t *handler_data_total;
		uint64_t *handler_data_current;
		int id;
	} *ct_thr;
	int nthreads;
	int mythread;

	const char *stat_counters_prefix;
};

struct ctrl_req {
	struct ct_thread *ctt;
	struct http_parser hp;
	int s;

	enum {
		HS_NONE,
		HS_VALUE,
		HS_FIELD
	} header_state;		/* the recommended state machine for catching headers. */
	struct buf_string current_header_name;
	struct buf_string current_header_value;

	const struct ctrl_handler_int *handler;
	void *handler_data;

	struct bconf_node *cr_bconf;
	struct bpapi ba;
	struct bconf_node *custom_headers;
	size_t content_length;

	int close_conn;
	int status;
	int in_handler;
	const char *response_content_type;
	void *raw_response_data;
	size_t raw_response_data_sz;
};

#define CTT_STATE(ctt, ...) do { if (ctt->thr_state) { stat_message_printf(ctt->thr_state, __VA_ARGS__); } } while (0)

void
render_template_cb(struct ctrl_req *cr, struct bpapi *ba, void *data) {
	const char *tmpl_name = data;

	if (!call_template(ba, tmpl_name))
		log_printf(LOG_CRIT, "render_template_cb: non-existent template %s", tmpl_name);
}

static int
error_consume_post(struct ctrl_req *cr, struct bpapi *ba, size_t totlen, const char *data, size_t len, void *v) {
	return 0;
}

static struct ctrl_handler_int error_handler = {
	.hand.url = "",
	.hand.consume_post = error_consume_post,
	.hand.finish = render_template_cb,
	.hand.cb_data = (char *)"error_tmpl"
};

void
ctrl_output_json(struct ctrl_req *cr, struct bpapi *ba, const char *root) {
	bpapi_insert(ba, "vtree_root", root);
	ctrl_set_content_type(cr, "application/json");
	render_template_cb(cr, ba, (char *)"output_json");
}

void
ctrl_set_custom_headers(struct ctrl_req *cr, const char *key, const char *value) {
	if ((strncmp(key, "X-", 2) != 0) || bconf_get(cr->custom_headers, key))
		xerrx(1,"Only unique 'X-' headers allowed, \"%s\"", key);

	bconf_add_data(&cr->custom_headers, key, value);
}

void
ctrl_close(struct ctrl_req *cr) {
	cr->close_conn = 1;
}

void
ctrl_status(struct ctrl_req *cr, int status) {
	cr->status = status;
}

void
ctrl_error(struct ctrl_req *cr, int error, const char *fmt, ...) {
	char buf[32];
	va_list ap;
	char *msg = NULL;

	snprintf(buf, sizeof(buf), "%d", error);
	bpapi_insert(&cr->ba, "status", buf);

	va_start(ap, fmt);
	int res = vasprintf(&msg, fmt, ap);
	va_end(ap);

	if (res == -1)
		msg = (char*)"<error message lost>";

	bpapi_insert(&cr->ba, "error_message", msg);

	/* Generally this is only accessed by internal services so things shouldn't cause errors, so log everything as CRIT. */
	log_printf(LOG_CRIT, "controller: ctrl_error called: %d (%s) (handler: %s)", error, msg, cr->handler ? cr->handler->hand.url : "<none>");

	cr->handler = &error_handler;
	cr->status = error;
	ctrl_close(cr);		/* We don't know if it's a fatal protocol error or just some misunderstanding, always close. */

	if (cr->in_handler == 1) {
		cr->in_handler = 2;
		(*error_handler.hand.finish)(cr, &cr->ba, error_handler.hand.cb_data);
	}

	if (res != -1)
		free(msg);
}

void
ctrl_thread_quit(struct ctrl_thread *ct) {
	int i;

	log_printf(LOG_DEBUG, "ctrl_thread_quit");

	ct->quit = 1;
	for (i = 0; i < ct->nthreads; i++) {
		pthread_cancel(ct->threads[i]);
	}
	close(ct->l);	
}

void
ctrl_quit(struct ctrl_req *cr) {
	ctrl_thread_quit(cr->ctt->thr);
}

void
ctrl_set_handler_data(struct ctrl_req *cr, void *v) {
	cr->handler_data = v;
}

void
ctrl_set_content_type(struct ctrl_req *cr, const char *ct) {
	cr->response_content_type = ct;
}

void
ctrl_set_raw_response_data(struct ctrl_req *cr, void *d, size_t sz) {
	cr->raw_response_data = d;
	cr->raw_response_data_sz = sz;
}

size_t
ctrl_get_content_length(struct ctrl_req *cr) {
	return cr->content_length;
}

struct bconf_node **
ctrl_get_bconfp(struct ctrl_req *cr) {
	return &cr->cr_bconf;
}

const struct ctrl_handler *
ctrl_get_handler(struct ctrl_req *cr) {
	return &cr->handler->hand;
}

int
ctrl_get_peer(struct ctrl_req *cr, char *hbuf, size_t hbuflen, char *pbuf, size_t pbuflen, int gni_flags) {
	struct sockaddr_storage ss;
	socklen_t sslen = sizeof(ss);

	if (getpeername(cr->s, (struct sockaddr*)&ss, &sslen))
		return EAI_SYSTEM;
	return getnameinfo((struct sockaddr*)&ss, sslen, hbuf, hbuflen, pbuf, pbuflen, gni_flags);
}

static int
on_message_begin(struct http_parser *hp) {
	struct ctrl_req *cr = hp->data;

	cr->status = 200;	/* Assume success */

	return 0;
}

static int
on_message_complete(struct http_parser *hp) {
	struct ctrl_req *cr = hp->data;

	if (cr->ctt->thr->nthreads == 1)
		ctrl_close(cr);	/* Don't keep alive if we only have one thread. */

	if (cr->handler) {
		struct buf_string hdrs = { 0 };
		char *data;
		size_t data_sz;
		ssize_t r;
		char *hdr_data;
		size_t hdr_sz;

		CTT_STATE(cr->ctt, "handler_finish");

		cr->in_handler = 1;		/* For error bailouts. */
		(*cr->handler->hand.finish)(cr, &cr->ba, cr->handler_data ?: cr->handler->hand.cb_data);

		if (cr->raw_response_data != NULL) {
			struct buf_string *buf = BPAPI_OUTPUT_DATA(&cr->ba);
			data = cr->raw_response_data;
			data_sz = cr->raw_response_data_sz;
			if (buf->pos != 0)
				log_printf(LOG_CRIT, "controller: [%s]: raw data with buf output, using raw data", cr->handler->hand.url);
		} else {
			struct buf_string *buf = BPAPI_OUTPUT_DATA(&cr->ba);
			data = buf->buf;
			data_sz = buf->pos;
		}

		bscat(&hdrs, "HTTP/1.1 %d %s\r\n", cr->status, cr->status >= 200 && cr->status < 300 ? "Success" : "Error");
		if (cr->close_conn)
			bscat(&hdrs, "Connection: close\r\n");
		bscat(&hdrs, "Content-Length: %llu\r\n", (unsigned long long)data_sz);
		if (cr->response_content_type)
			bscat(&hdrs, "Content-Type: %s\r\n", cr->response_content_type);

		for (int i = 0; i < bconf_count(cr->custom_headers); i++)	
			bscat(&hdrs, "%s: %s\r\n", bconf_key(bconf_byindex(cr->custom_headers, i)), bconf_value(bconf_byindex(cr->custom_headers, i)));
		bscat(&hdrs, "\r\n");

		CTT_STATE(cr->ctt, "sending_result (%llu + %z bytes)", (unsigned long long)hdrs.pos, data_sz);

		hdr_data = hdrs.buf;
		hdr_sz = hdrs.pos;
		do {
			r = write(cr->s, hdr_data, hdr_sz);
			if (r < 1) {
				log_printf(LOG_CRIT, "controller: Failed to write response header: %m");
			} else {
				hdr_data += r;
				hdr_sz -= r;
			}
		} while (r > 0 && hdr_sz > 0);
		free(hdrs.buf);
		if (data_sz && hdr_sz == 0) {
			do {
				r = write(cr->s, data, data_sz);
				if (r < 1) {
					log_printf(LOG_CRIT, "controller: Failed to write response data: %m");
				} else {
					data += r;
					data_sz -= r;
				}
			} while (r > 0 && data_sz > 0);
		}

		if (cr->handler->hand.cleanup)
			(*cr->handler->hand.cleanup)(cr, cr->handler_data ?: cr->handler->hand.cb_data);

		CTT_STATE(cr->ctt, "result sent");
	}

	if (cr->ctt->handler_name) {
		stat_message_printf(cr->ctt->handler_name, "<none>");
		STATCNT_SET(cr->ctt->handler_data_total, 0);
		STATCNT_SET(cr->ctt->handler_data_current, 0);
	}

	bpapi_free(&cr->ba);
	bconf_free(&cr->custom_headers);
	bconf_free(&cr->cr_bconf);
	cr->handler = NULL;
	cr->handler_data = NULL;
	cr->content_length = 0;
	cr->status = 0;
	cr->in_handler = 0;
	cr->response_content_type = NULL;

	return 0;
}

static void
parse_qs_cb(struct parse_cb_data *d, char *key, int klen, char *val, int vlen) {
	struct bpapi *ba = d->cb_data;
	char k[klen + 1];
	char v[vlen + 1];

	strlcpy(k, key, klen + 1);
	strlcpy(v, val, vlen + 1);
	bpapi_insert(ba, k, v);
}

struct path_param {
	const char *key;
	size_t key_len;
	const char *value;
	size_t value_len;
};

static bool
match_handler(const char *handler_url, size_t handler_url_len, const char *request_url, size_t request_url_len, struct path_param **p, int *num_params) {
	bool match = false;
	const char *hup = handler_url;
	const char *hend = handler_url + handler_url_len;
	const char *rup = request_url;
	const char *rend = request_url + request_url_len;
	int paramidx = 0;
	int num_vars = 0, num_var_ends = 0; 
	struct path_param *params = NULL;

	for (const char *p = hup; p < hend; p++) {
		int diff;
		if (*p == '<')
			num_vars++;
		if (*p == '>')
			num_var_ends++;
		
		diff = num_vars - num_var_ends;
		if (diff != 0 && diff != 1) {
			log_printf(LOG_CRIT, "Malformed handler url found: %.*s", (int)handler_url_len, handler_url);
			goto out;
		}
	}

	params = xcalloc(num_vars, sizeof(*params));
	for (; hup != hend && rup != rend; hup++, rup++) {
		if (*hup == *rup) {
			continue;
		} else if (*hup == '<') {
			params[paramidx].key = ++hup;
			while (*hup++ != '>') {
				params[paramidx].key_len++;
			}

			params[paramidx].value = rup;
			while (*rup != '/' && rup != rend) {
				rup++;
				params[paramidx].value_len++;
			}
			paramidx++;

			if (rup == rend || hup == hend)
				break;
		} else if (*hup != *rup) {
			break;
		}
	}

	if (hup != hend || rup != rend) {
		log_printf(LOG_DEBUG, "Rest after failed match %.*s %.*s", (int)(hend - hup), hup, (int)(rend - rup), rup);
		goto out;
	}

	log_printf(LOG_DEBUG, "Found matching handler for with url: %.*s", (int)handler_url_len, handler_url);
	match = true;
	
out:
	if (!match) {
		free(params);
		params = NULL;
		num_vars = 0;
	}
	*p = params;
	*num_params = num_vars;
	return match;
}

static int
on_url(struct http_parser *hp, const char *at, size_t length) {
	struct ctrl_req *cr = hp->data;
	struct http_parser_url hpu;
	struct path_param *params = NULL;
	int num_path_params = 0;
	int i;
	int res;

	if ((res = http_parser_parse_url(at, length, 0, &hpu)) != 0) {
		log_printf(LOG_CRIT, "handle_command: url parse failed %d", res);
		return res;
	}

	/*
	 * We only care about path and query.
	 */
	if (!(hpu.field_set & (1 << UF_PATH))) {
		ctrl_error(cr, 400, "on_url: no path");
		return 0;
	}
	for (i = 0; i < cr->ctt->thr->nhandlers; i++) {
		const struct ctrl_handler_int *hi = &cr->ctt->thr->handlers[i];

		if (match_handler(hi->hand.url, strlen(hi->hand.url), at + hpu.field_data[UF_PATH].off, hpu.field_data[UF_PATH].len, &params, &num_path_params)) {
			cr->handler = hi;
			if (hi->cnt)
				STATCNT_INC(hi->cnt);
			break;
		}
	}

	if (cr->handler == NULL) {
		BPAPI_INIT_BCONF(&cr->ba, cr->ctt->thr->bconf, buf, hash);
		ctrl_error(cr, 404, "unknown url (%.*s)", hpu.field_data[UF_PATH].len, at + hpu.field_data[UF_PATH].off);
		return 0;
	}

	if (cr->ctt->handler_name) {
		stat_message_printf(cr->ctt->handler_name, "%s", &cr->handler->hand.url[1]);
	}

	/* XXX - we should set up a shadow instead. */
	bconf_merge(&cr->cr_bconf, cr->ctt->thr->bconf);
	BPAPI_INIT_BCONF(&cr->ba, cr->cr_bconf, buf, hash);
	cr->custom_headers = NULL;
	
	for (int i = 0; i < num_path_params; i++) {
		char k[params[i].key_len + 1];
		char v[params[i].value_len + 1];

		strlcpy(k, params[i].key, params[i].key_len + 1);
		strlcpy(v, params[i].value, params[i].value_len + 1);
		bpapi_insert(&cr->ba, k, v);
	}

	if (cr->handler->hand.start) {
		(*cr->handler->hand.start)(cr, cr->handler->hand.cb_data);
	}

	if (hpu.field_set & (1 << UF_QUERY)) {
		char *qs = strndup(at + hpu.field_data[UF_QUERY].off, hpu.field_data[UF_QUERY].len); /* GRR. parse_query_string should be smarter. */

		/* Parse query string will just silently ignore arguments that it doesn't like. I'd like to see some error handling too. */
		parse_query_string(qs, parse_qs_cb, &cr->ba, NULL, 0, RUTF8_REQUIRE, NULL);
		free(qs);
	}

	free(params);
	return 0;
}

/* For now we limit the body size to 100GB */
#define MAX_BODY_SIZE (100LL*1024*1024*1024)

static void
finalize_header(struct ctrl_req *cr) {
	if (cr->current_header_name.pos != 0) {
		const char *nameb = cr->current_header_name.buf;
		int namep = cr->current_header_name.pos;
		const char *valueb = cr->current_header_value.buf;
		int valuep = cr->current_header_value.pos;

		/* Be safe. Even though http_parser probably deals with this already. */
		if (valueb == NULL || valuep == 0) {
			ctrl_error(cr, 400, "bad header value");
			goto out;
		}

		/*
		 * There are only a few headers we care about at this moment. All other headers are dropped.
		 */
		if (!strncmp(nameb, "Content-Length", namep)) {
			char *endptr = NULL;
			long long v;
			errno = 0;
			v = strtoll(valueb, &endptr, 10);
			if (errno == ERANGE || v < 0 || v > MAX_BODY_SIZE || endptr != valueb + valuep) {
				ctrl_error(cr, 400, "bad content-length");
				goto out;
			}
			cr->content_length = v;
			if (cr->ctt->handler_data_total)
				STATCNT_SET(cr->ctt->handler_data_total, v);
		} if (!strncmp(nameb, "Connection", namep)) {
			if (!strncmp(valueb, "close", valuep))
				ctrl_close(cr);
		}
	}
out:
	free(cr->current_header_name.buf);
	cr->current_header_name.buf = NULL;
	cr->current_header_name.len = cr->current_header_name.pos = 0;
	free(cr->current_header_value.buf);
	cr->current_header_value.buf = NULL;
	cr->current_header_value.len = cr->current_header_value.pos = 0;
}

static int
on_header_field(struct http_parser *hp, const char *at, size_t length) {
	struct ctrl_req *cr = hp->data;

	switch (cr->header_state) {
	case HS_VALUE:
		finalize_header(cr);
		break;
	case HS_NONE:
	case HS_FIELD:
		break;
	}
	cr->header_state = HS_FIELD;
	bswrite(&cr->current_header_name, at, length);	
	return 0;
}

static int
on_header_value(struct http_parser *hp, const char* at, size_t length) {
	struct ctrl_req *cr = hp->data;

	switch (cr->header_state) {
	case HS_VALUE:
	case HS_FIELD:
		break;
	default:
	case HS_NONE:
		log_printf(LOG_CRIT, "on_header_value: wrong state %d", cr->header_state);
		return 1;
	}
	cr->header_state = HS_VALUE;
	bswrite(&cr->current_header_value, at, length);
	return 0;
}

static int
on_headers_complete(struct http_parser *hp) {
	struct ctrl_req *cr = hp->data;

	finalize_header(cr);
	return 0;
}

static int
on_body(struct http_parser *hp, const char* at, size_t length) {
	struct ctrl_req *cr = hp->data;
	int ret;

	/* In the future we want to decode things here with json, for now, we just error out. */
	if (cr->handler == NULL || cr->handler->hand.consume_post == NULL) {
		ctrl_error(cr, 400, "POST data with no data handler.");
		return 0;
	}

	ret = (*cr->handler->hand.consume_post)(cr, &cr->ba, cr->content_length, at, length, cr->handler_data ?: cr->handler->hand.cb_data);
	if (!ret && cr->ctt->handler_data_current) {
		STATCNT_ADD(cr->ctt->handler_data_current, length);
	}
	return ret;
}

static struct http_parser_settings hp_settings = {
	.on_message_begin = on_message_begin,
	.on_url = on_url,
	.on_header_field = on_header_field,
	.on_header_value = on_header_value,
	.on_headers_complete = on_headers_complete,
	.on_body = on_body,
	.on_message_complete = on_message_complete,
	
};

static void
handle_command(struct ctrl_req *cr) {
	char buf[65536];
	size_t nparsed;
	ssize_t len;

	http_parser_init(&cr->hp, HTTP_REQUEST);
	cr->hp.data = cr;

	while (1) {
		if ((len = read(cr->s, buf, sizeof(buf))) == -1) {
			log_printf(LOG_CRIT, "handle_command: read %m");
			close(cr->s);
			return;
		}
		nparsed = http_parser_execute(&cr->hp, &hp_settings, buf, len);
		if (nparsed != (size_t)len) {
			log_printf(LOG_CRIT, "handle_command: request parse error: %s (%s)",
			    http_errno_description(HTTP_PARSER_ERRNO(&cr->hp)),
			    http_errno_name(HTTP_PARSER_ERRNO(&cr->hp)));
			close(cr->s);
			return;
		}
		if (len > 0 && !cr->close_conn) {
			/* We could see the difference between keep-alive and more data if we set a flag in on_message_complete */
			CTT_STATE(cr->ctt, "read request data / keep-alive");
		} else {
			CTT_STATE(cr->ctt, "closed");
			break;
		}
	}
	close(cr->s);
}

STAT_COUNTER_DECLARE(num_accept, "control_thread", "accept");

static void *
command_thread(void *v) {
	struct sockaddr_storage sas;
	socklen_t slen = sizeof(sas);
	struct ct_thread *ctt = v;
	struct ctrl_thread *thr = ctt->thr;

	while (!thr->quit) {
		int oldcancel;
		struct ctrl_req cr = {
			.ctt = ctt,
		};

		CTT_STATE(ctt, "idle");

		if ((cr.s = accept(thr->l, (struct sockaddr *)&sas, &slen)) == -1) {
			log_printf(LOG_CRIT, "accept: %m");
			pthread_exit(NULL);
		}

		STATCNT_INC(&num_accept);

		CTT_STATE(ctt, "accepted");

		pthread_setcancelstate(PTHREAD_CANCEL_DISABLE, &oldcancel);	/* No cancellation while handling the command. */
		handle_command(&cr);
		pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, &oldcancel);

	}

	if (thr->stat_counters_prefix) {
		/* XXX - assymetric freeing of the state and it is not cancellation safe. We need to handle the joins inside this API. */
		stat_message_dynamic_free(ctt->thr_state);
		stat_message_dynamic_free(ctt->handler_name);
		stat_counter_dynamic_free(ctt->handler_data_total);
		stat_counter_dynamic_free(ctt->handler_data_current);
	}

	pthread_exit(NULL);
}

int
ctrl_thread_get_listen_socket(struct ctrl_thread *ct) {
	return ct->l;
}

struct ctrl_thread *
ctrl_thread_setup(struct bconf_node *ctrl_conf, struct bconf_node *bconf_root, pthread_t **retthr, int *nthreads, const struct ctrl_handler *handlers, int nhandlers, int listen_socket) {
	const char *host = bconf_get_string(ctrl_conf, "host");
	const char *port = bconf_get_string(ctrl_conf, "port");
	struct ctrl_thread *thr = calloc(1, sizeof *thr);
	int r;
	int i;

	if (!bconf_get_int(ctrl_conf, "bind_host"))
		host = NULL;

	thr->bconf = bconf_root;
	thr->ctrl_conf = ctrl_conf;
	thr->stat_counters_prefix = bconf_get_string(thr->ctrl_conf, "stat_counters_prefix");
	thr->nhandlers = nhandlers;
	thr->handlers = calloc(nhandlers, sizeof(*thr->handlers));
	for (i = 0; i < nhandlers; i++) {
		thr->handlers[i].hand = handlers[i];
		if (thr->stat_counters_prefix) {
			/* We skip the first character of the url because it's always a '/'. */
			thr->handlers[i].cnt = stat_counter_dynamic_alloc(3, thr->stat_counters_prefix, &handlers[i].url[1], "calls");
		}
	}

	if (listen_socket == -1) {
		if ((thr->l = create_socket(host, port)) == -1) {
			free(thr);
			return NULL;
		}
	} else {
		/* There already is a listen socket. */
		thr->l = listen_socket;
	}

	thr->nthreads = *nthreads = bconf_get_int(ctrl_conf, "nthreads") ?: 1;
	thr->threads = *retthr = calloc(*nthreads, sizeof(*thr->threads));
	thr->ct_thr = calloc(*nthreads, sizeof(*thr->ct_thr));

	for (i = 0; i < thr->nthreads; i++) {
		thr->ct_thr[i].thr = thr;
		thr->ct_thr[i].id = i;
		if (thr->stat_counters_prefix) {
			char buf[16];
			snprintf(buf, sizeof(buf), "%d", i);			
			thr->ct_thr[i].thr_state = stat_message_dynamic_alloc(4, thr->stat_counters_prefix, "thread", buf, "thread_state");
			thr->ct_thr[i].handler_name = stat_message_dynamic_alloc(4, thr->stat_counters_prefix, "thread", buf, "current_handler");
			thr->ct_thr[i].handler_data_total = stat_counter_dynamic_alloc(5, thr->stat_counters_prefix, "thread", buf, "post_data", "total");
			thr->ct_thr[i].handler_data_current = stat_counter_dynamic_alloc(5, thr->stat_counters_prefix, "thread", buf, "post_data", "current");
		}
		
		if ((r = pthread_create(&thr->threads[i], NULL, command_thread, &thr->ct_thr[i])) != 0) {
			log_printf(LOG_CRIT, "Failed to create command thread: %d %s", r, strerror(r));
			close(thr->l);
			free(thr->ct_thr);
			free(thr);
			return NULL;
		}
	}
	return thr;
}
