#include <sys/types.h>

#include "macros.h"

struct bconf_node;
struct bpapi;
struct bpapi_vtree_chain;
struct ctrl_req;
struct ctrl_handler {
	const char *url;
	/*
	 * We always parse GET arguments and pass them on.
	 * POST data we either parse as JSON (not yet) or pass to the handler raw if there's a callback for it.
	 */
	void (*start)(struct ctrl_req *, void *);				/* Any necessary setup before things are fully initialized. */
	int (*consume_post)(struct ctrl_req *, struct bpapi *, size_t, const char *, size_t, void *);		/* Handler of POST data. */
	void (*finish)(struct ctrl_req *, struct bpapi *, void *);		/* The main message handler, called after the full request has been read. */
	void (*cleanup)(struct ctrl_req *, void *);				/* In case we need to do any cleanup after everything has been completed. */
	void *cb_data;
};

struct ctrl_thread;

struct ctrl_thread *ctrl_thread_setup(struct bconf_node *, struct bconf_node *, pthread_t **retthr, int *nthreads, const struct ctrl_handler *, int nhandlers, int listen_socket);
/*
 * Kill all command threads.
 */
void ctrl_thread_quit(struct ctrl_thread *ct);
int ctrl_thread_get_listen_socket(struct ctrl_thread *ct);

/*
 * Tell the backend to not keep-alive the connection after this command.
 */
void ctrl_close(struct ctrl_req *);
/*
 * Signal an error for this command.
 */
void ctrl_error(struct ctrl_req *, int, const char *fmt, ...) __attribute__((format (printf, 3, 4)));
/*
 * Signal a status other than 200 that is not an error.
 */
void ctrl_status(struct ctrl_req *, int);
/*
 * Set a callback data pointer for this particular command.  This can
 * be used in raw_post_cb to allocate a buffer or some other context
 * that's needed to handle this particular request. The general
 * cb_data must be NULL when this function is used and it can only be
 * called once per request. Subsequent calls to raw_post_cb and
 * message_cb will be using that pointer for cb_data. The caller is
 * responsible for freeing the data in message_cb (message_cb is only
 * called once and last in the request).
 */
void ctrl_set_handler_data(struct ctrl_req *, void *);

/*
 * Sets the content-type of the successful response.
 */
void ctrl_set_content_type(struct ctrl_req *cr, const char *ct);

/*
 * Makes this request return a raw data blob as the response. It's an
 * internal error to use this and write to the output buf.
 */
void ctrl_set_raw_response_data(struct ctrl_req *, void *, size_t);

/*
 * Returns the Content-Length set by the request or 0 if none has been received.
 */
size_t ctrl_get_content_length(struct ctrl_req *);

/*
 * Get a pointer to the current handler. Note: this is NOT a pointer into the
 * array given to ctrl_thread_setup, but rather a copy.
 */
const struct ctrl_handler *ctrl_get_handler(struct ctrl_req *cr);

/*
 * Calls getpeername then getnameinfo on the socket.
 * Return result from getnameinfo.
 */
int ctrl_get_peer(struct ctrl_req *cr, char *hbuf, size_t hbuflen, char *pbuf, size_t pbuflen, int gni_flags);

/*
 * Indicate that we need to shut down the command thread cleanly.
 */
void ctrl_quit(struct ctrl_req *);
/*
 * Get the root for the per-request bconf.
 */
struct bconf_node;
struct bconf_node **ctrl_get_bconfp(struct ctrl_req *);
/*
 * Called from a callback to output json as our response. "root" is the root of the vtree that we want to output.
 */
void ctrl_output_json(struct ctrl_req *cr, struct bpapi *ba, const char *root);

/*
 * Sets custom headers.
 * Only unique headers starting with 'X-' are accepted.
 * The sanity of the keys and values is not checked.
 */
void ctrl_set_custom_headers(struct ctrl_req *cr, const char *key, const char *value) NONNULL_ALL;

void render_template_cb(struct ctrl_req *, struct bpapi *, void *);

/* Default handler for timers, stat_counters and stat_messages */
extern const struct ctrl_handler ctrl_stats_handler;
extern const struct ctrl_handler ctrl_stats_view_handler;
