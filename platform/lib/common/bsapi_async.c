#include "bsapi.h"

#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <netdb.h>
#include <ctype.h>
#include <util.h>

#include <sys/socket.h>
#include <sys/un.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include "event.h"
#include "fd_pool.h"


#include <logging.h>

extern int h_errno;

#define BUFFER_SIZE 65536

struct bsapi_async {
	struct event connect_ev;
	struct event_base *base;
	struct bsearch_api *ba;
	char *search_string;
	void (*parse_cb)(int, int, const char *, const char *, void *);
	void *parse_cb_data;
};


static void
bsa_write_cb(struct bufferevent *bev, void *data) {
	struct bsearch_api *ba = data;

	shutdown(ba->sockfd, SHUT_WR);
	bufferevent_disable(bev, EV_WRITE);
	bufferevent_enable(bev, EV_READ);
}

static void
bsa_read_cb(struct bufferevent *bev, void *data) {
	/* XXX write this so we can handle keepalive data */
	/* Noop, we handle data in error cb. */
}

static void
bsa_error_cb(struct bufferevent *bev, short what, void *data) {
	struct bsearch_api *ba = data;

	size_t len = EVBUFFER_LENGTH(bev->input);
	if (ba->buffer)
		free(ba->buffer);
	ba->buffer = xmalloc(len + 1);
	memcpy(ba->buffer, EVBUFFER_DATA(bev->input), len);
	ba->buffer[len] = '\0';
	bufferevent_free(bev);
	close(ba->sockfd);

	bsearch_parse_list(ba);
}

static void
bsa_conn_fail(struct bsapi_async *baa) {
	(*baa->ba->parse_cb)(BS_NOAD, 0, NULL, NULL, baa->ba->cb_data);
	free(baa->search_string);
	free(baa);
}

static void
bsa_conn(int fd, short ev, void *v) {
	struct bsapi_async *baa = v;
	struct bsearch_api *ba = baa->ba;
	struct timeval tv;
	int timeout;
	int res;
	int soerr = 0;
	socklen_t n = sizeof(soerr);
	struct bufferevent *bev;
        char *search_string = baa->search_string;

	if (fd != -1 && getsockopt(fd, SOL_SOCKET, SO_ERROR, &soerr, &n) != 0) {
		close(fd);
		fd = -1;
	}

	if ((fd == -1) || (ev & EV_TIMEOUT) || soerr) {
		if (fd != -1) {
			if (ev & EV_TIMEOUT)
				ba->berrno = BERR_CONNECT_TIMEOUT;
			else
				ba->berrno = soerr + 1024;
			ba->sc_status = SBCS_FAIL;
			close(fd);
		}
		while ((res = bsearch_connection_create(ba, 1)) == -1)
			;
		if (res == -2) {
			bsa_conn_fail(baa);
			return;
		}

		timeout = ba->bs->bs_timeout;
		tv.tv_sec = timeout / 1000;
		tv.tv_usec = (timeout % 1000) * 1000;

		event_set(&baa->connect_ev, ba->sockfd, EV_READ|EV_WRITE, bsa_conn, baa);
		event_base_set(baa->base, &baa->connect_ev);
		event_add(&baa->connect_ev, &tv);
		return;
	}

	bev = bufferevent_new(ba->sockfd, bsa_read_cb, bsa_write_cb, bsa_error_cb, ba);
	bufferevent_base_set(baa->base, bev);
	bufferevent_disable(bev, EV_READ);
	/* Need to cast since no const in function declaration. */
	bufferevent_write(bev, search_string, strlen(search_string));
	timeout = ba->bs->bs_timeout;
	/* timeout is in seconds in bufferevet */
	if (timeout > 1000)
		timeout = timeout / 1000;	
	bufferevent_settimeout(bev, timeout, timeout);

	free(search_string);
	free(baa);
}

int
bsearch_async_search(struct bsearch_api *ba, struct event_base *base, const char *search_string, void (*parse_cb)(int, int, const char*, const char**, void*), void *cb_data) {
	struct bsapi_async *baa;

	baa = xmalloc(sizeof(struct bsapi_async));
	baa->ba = ba;
	baa->search_string = xstrdup(search_string);
	baa->base = base;
	ba->parse_cb = parse_cb;
	ba->cb_data = cb_data;

	fd_pool_set_async(ba->conn, 1);

	bsa_conn(-1, 0, baa);

	return 0;
}

