#ifndef COMMON_NETWORK_H
#define COMMON_NETWORK_H

#define NET_TIMEOUT_CONNECT -2
#define NET_ERROR_CONNECT   -3

#include <stdlib.h>
#include "macros.h"

struct event_base;
struct iovec;

/*
 * Connect to a host and port.
 * Might return -1 on error, with errno set.
 * Otherwise it will use libevent to call the callback with the fd
 * or an error code.
 *
 * Note that timeout is per connect call. There might be multiple
 * connect calls depending on how host and port resolves.
 *
 * socktype should be either SOCK_STREAM, SOCK_DGRAM or 0 if you do not care.
 * (it is given directly to getaddrinfo).
 *
 * tos is a IPTOS_* value from <netinet/ip.h> or 0 for default.
 *
 * Possible future enhancements:
 *  - Return a token that can be used to cancel connect.
 *  - Use bconf nodes for multiple hosts and ports.
 */
int
net_connect_host_port (struct event_base *base, int socktype, int tos, int timeout, const char *host, const char *port,
		void (*cb)(int fd_res, void *cbdata), void *cbdata);


/*
	Helper to calculate the total size of an array of iovecs
*/
ssize_t
get_iovlen_sum(const struct iovec *iov, int iovcnt) FUNCTION_PURE;

/*
	Wrapper around writev to handle partial writes.

	The iovecs can be modified internally, but will be restored before the function
	returns, allowing a higher-level retry on another fd without re-loading the iovecs.

	If the total iovlen is unknown, pass -1 for iovlen_sum.

	On success, 0 is returned.
	On error, a negative value is returned.
*/
int
writev_retry(int fd, struct iovec *iov, int iovcnt, ssize_t iovlen_sum);

#endif /*COMMON_NETWORK_H*/
