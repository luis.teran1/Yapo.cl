#include "fd_pool.h"
#include "sbalance.h"
#include "queue.h"
#include "vtree.h"
#include "util.h"
#include "strl.h"
#include "logging.h"
#include "timer.h"
#include "network.h"

#include <fcntl.h>
#include <sys/poll.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <stdio.h>
#include <unistd.h>
#include <pthread.h>
#include <sys/time.h>
#include <sys/resource.h>

struct fd_pool_entry {
	SLIST_ENTRY(fd_pool_entry) link;
	int fd;
};

struct fd_pool_node {
	char *key;
	struct bpapi_vtree_chain vtree;
	SLIST_HEAD(, fd_pool_entry) entries;
	int socktype;
	struct sockaddr *sockaddr;
	socklen_t addrlen;
	pthread_mutex_t lock;
	int keepalive;
	char peer[256];
};

struct fd_pool {
	struct bpapi_vtree_chain vtree;
	struct sbalance sb;
	SLIST_HEAD(, fd_pool_entry) free_entries;
	int timeout;
	pthread_mutex_t lock;
};

struct fd_pool_conn {
	struct fd_pool *pool;
	struct fd_pool_node *node;
	struct fd_pool_entry *entry;
	struct sbalance_connection sc;
	int keepalive;
	int async;
	uint32_t sc_hash;
	struct timer_instance *ti;
	char *node_filter;
};

#define LOCK(x) pthread_mutex_lock((x))
#define UNLOCK(x) pthread_mutex_unlock((x))

static const struct addrinfo default_hints = {
	.ai_flags = AI_ADDRCONFIG,
	.ai_socktype = SOCK_STREAM
};

static void
fd_pool_add_addrinfo(struct fd_pool *pool, const char *key, struct bpapi_vtree_chain *vtree, struct addrinfo *res, int keepalive) {
	struct addrinfo *curr;

	for (curr = res; curr != NULL; curr = curr->ai_next) {
		struct fd_pool_node *node = xmalloc(sizeof (*node) + curr->ai_addrlen);
		char h[NI_MAXHOST];
		char s[NI_MAXSERV];

		node->key = key ? xstrdup(key) : NULL;
		if (vtree)
			node->vtree = *vtree;
		else
			node->vtree.fun = NULL;

		SLIST_INIT(&node->entries);

		node->keepalive = keepalive;

		node->socktype = curr->ai_socktype;
		node->sockaddr = (struct sockaddr*)(node + 1);
		memcpy(node->sockaddr, curr->ai_addr, curr->ai_addrlen);
		node->addrlen = curr->ai_addrlen;

		if (getnameinfo(node->sockaddr, node->addrlen, h, sizeof(h), s, sizeof(s), NI_NUMERICHOST|NI_NUMERICSERV) == 0)
			snprintf(node->peer, sizeof(node->peer), "%s %s", h, s);
		else
			strlcpy(node->peer, "X", sizeof(node->peer));

		pthread_mutex_init(&node->lock, NULL);

		sbalance_add_serv(&pool->sb, vtree_getint(vtree, "cost", NULL) ?: 1, node);
	}
}

static void
fd_pool_free_node(void *v) {
	struct fd_pool_node *node = v;

	while (!SLIST_EMPTY(&node->entries)) {
		struct fd_pool_entry *entry = SLIST_FIRST(&node->entries);

		SLIST_REMOVE_HEAD(&node->entries, link);
		close(entry->fd);
		free(entry);
	}
	free(node->key);
	free(node);
}

struct fd_pool *
fd_pool_create(struct bpapi_vtree_chain *vtree, int use_keepalive, const struct addrinfo *hints) {
	if (!vtree || !vtree->fun)
		return NULL;

	const char *strat = vtree_get(vtree, "strat", NULL);
	enum sbalance_strat st = ST_SEQ;
	struct vtree_keyvals loop;
	int cnt;
	int start = vtree_getint(vtree, "start", NULL);
	int retries = vtree_getint(vtree, "retries", NULL);

	if (!hints)
		hints = &default_hints;

	if (start)
		start--; /* 1-indexed in bconf. */

	if (strat && *strat) {
		if (strcmp(strat, "hash") == 0)
			st = ST_HASH;
		else if (strcmp(strat, "random") == 0)
			st = ST_RANDOM;
	} else if (vtree_getint(vtree, "client_hash", NULL)) {
		st = ST_HASH;
	} else if (vtree_getint(vtree, "random_pick", NULL)) {
		st = ST_RANDOM;
	}

	if (st != ST_SEQ && retries <= 0)
		retries = 1;

	struct fd_pool *pool = zmalloc(sizeof (*pool));

	pool->vtree = *vtree;

	sbalance_init(&pool->sb, retries, vtree_getint(vtree, "failcost", NULL) ?: FD_POOL_DEFAULT_FAIL,
			vtree_getint(vtree, "tempfailcost", NULL) ?: FD_POOL_DEFAULT_TEMPFAIL, st);

	SLIST_INIT(&pool->free_entries);

	pthread_mutex_init(&pool->lock, NULL);

	pool->timeout = vtree_getint(vtree, "connect_timeout", NULL);
	if (!pool->timeout) {
		pool->timeout = vtree_getint(vtree, "timeout", NULL);
	}
	if (pool->timeout && pool->timeout < 1000) {
		log_printf(LOG_INFO, "fd_pool: Ignoring timeout %d ms < 1000", pool->timeout);
		pool->timeout = 0;
	}
	if (!pool->timeout)
		pool->timeout = FD_POOL_DEFAULT_TIMEOUT;

	vtree_fetch_keys_and_values(vtree, &loop, "host", VTREE_LOOP, NULL);

	for (cnt = 0 ; cnt < loop.len ; cnt++) {
		int i = (cnt + start) % loop.len;

		if (loop.list[i].type != vkvNode)
			continue;

		const char *host = vtree_get(&loop.list[i].v.node, "name", NULL);
		const char *port = use_keepalive ? vtree_get(&loop.list[i].v.node,  "keepalive_port", NULL) : NULL;
		int keepalive = (port != NULL);

		if (!keepalive)
			port = vtree_get(&loop.list[i].v.node, "port", NULL);

		if (host && port) {
			struct addrinfo *res;
			int err;

			if ((err = getaddrinfo(host, port, hints, &res))) {
				/* Failed, just get the next one */
				continue;
			}
			
			fd_pool_add_addrinfo(pool, loop.list[i].key, &loop.list[i].v.node, res, keepalive);
			freeaddrinfo(res);
		}
	}

	if (loop.cleanup)
		loop.cleanup(&loop);

	if (!pool->sb.sb_nserv) {
		sbalance_free(&pool->sb, fd_pool_free_node);
		free(pool);
		return NULL;
	}

	return pool;
}

struct fd_pool *
fd_pool_create_single(const char *host, const char *port, int retries, int timeout, const struct addrinfo *hints) {
	struct addrinfo *res;
	int err;
	if (!hints)
		hints = &default_hints;

	if ((err = getaddrinfo(host, port, hints, &res))) {
		return NULL;
	}

	struct fd_pool *pool = zmalloc(sizeof (*pool));

	sbalance_init(&pool->sb, retries, 0, 0, ST_SEQ);

	SLIST_INIT(&pool->free_entries);

	pool->timeout = timeout;

	fd_pool_add_addrinfo(pool, NULL, NULL, res, 0);
	freeaddrinfo(res);

	if (!pool->sb.sb_nserv) {
		sbalance_free(&pool->sb, fd_pool_free_node);
		free(pool);
		return NULL;
	}

	return pool;
}

void
fd_pool_free(struct fd_pool *pool) {
	if (!pool)
		return;

	sbalance_free(&pool->sb, fd_pool_free_node);

	while (!SLIST_EMPTY(&pool->free_entries)) {
		struct fd_pool_entry *entry = SLIST_FIRST(&pool->free_entries);

		SLIST_REMOVE_HEAD(&pool->free_entries, link);
		free(entry);
	}
	free(pool);
}

void
fd_pool_clear_node(struct fd_pool *pool) {
	memset(&pool->vtree, 0, sizeof(pool->vtree));
}

struct bpapi_vtree_chain *
fd_pool_node(struct fd_pool *pool) {
	if (pool->vtree.fun)
		return &pool->vtree;
	return NULL;
}

int
fd_pool_timeout(struct fd_pool *pool) {
	return pool->timeout;
}

struct fd_pool_conn *
fd_pool_new_conn(struct fd_pool *pool, const char *remote_addr, int async) {
	struct fd_pool_conn *conn = zmalloc(sizeof(*conn));

	conn->pool = pool;
	conn->node = NULL;
	conn->async = async;
	conn->sc_hash = remote_addr ? sbalance_hash_string(remote_addr) : 0;

	return conn;
}

void
fd_pool_free_conn(struct fd_pool_conn *conn) {
	if (!conn)
		return;

	sbalance_conn_done(&conn->sc);

	if (conn->entry) {
		LOCK(&conn->pool->lock);
		SLIST_INSERT_HEAD(&conn->pool->free_entries, conn->entry, link);
		UNLOCK(&conn->pool->lock);
	}
	free(conn->node_filter);
	free(conn);
}

int
fd_pool_get(struct fd_pool_conn *conn, enum sbalance_conn_status status, const char **peer, int *is_ka) {
	int fd = -1;
	int flags = 0;
	socklen_t sl;
	int error;

	if (status == SBCS_START)
		sbalance_conn_new(&conn->sc, &conn->pool->sb, conn->sc_hash);

	while (fd == -1) {
		if (conn->ti)
			timer_handover(conn->ti, "connect");

		/*
		 * conn->entry will be NULL only if the last try was a new connection made to the node.
		 * Thus we retry the same node until a new connection fails.
		 */
		if (!conn->node || !conn->entry) {
			do {
				conn->node = sbalance_conn_next(&conn->sc, status);
				if (!conn->node)
					return -1;
				status = SBCS_START;
			} while (conn->node_filter && strcmp(conn->node->key, conn->node_filter) != 0);
		}
		if (conn->ti)
			timer_add_attribute(conn->ti, conn->node->peer);

		if (peer)
			*peer = conn->node->peer;
		if (is_ka)
			*is_ka = conn->node->keepalive;

		if (conn->entry) {
			LOCK(&conn->pool->lock);
			SLIST_INSERT_HEAD(&conn->pool->free_entries, conn->entry, link);
			UNLOCK(&conn->pool->lock);
		}

		if (!SLIST_EMPTY(&conn->node->entries)) {
			LOCK(&conn->node->lock);
			while ((conn->entry = SLIST_FIRST(&conn->node->entries))) {
				struct pollfd pfd;
				int n;

				SLIST_REMOVE_HEAD(&conn->node->entries, link);
				UNLOCK(&conn->node->lock);

				memset(&pfd, 0, sizeof(pfd));

				pfd.fd = conn->entry->fd;
				pfd.events = POLLHUP | POLLRDHUP;

				n = poll(&pfd, 1, 0);

				if (n == 0) {
					log_printf(LOG_DEBUG, "fd_pool: using existing fd to %s", conn->node->peer);
					return conn->entry->fd;
				}

				if (n > 0)
					log_printf(LOG_DEBUG, "fd_pool: NOT using existing fd to %s: EOF", conn->node->peer);
				else
					log_printf(LOG_DEBUG, "fd_pool: NOT using existing fd to %s: %m", conn->node->peer);
				close(conn->entry->fd);

				LOCK(&conn->pool->lock);
				SLIST_INSERT_HEAD(&conn->pool->free_entries, conn->entry, link);
				UNLOCK(&conn->pool->lock);

				LOCK(&conn->node->lock);
			}
			UNLOCK(&conn->node->lock);
		}

		conn->entry = NULL;
		status = SBCS_FAIL;

		if ((fd = socket(conn->node->sockaddr->sa_family, conn->node->socktype, 0)) == -1) {
			log_printf(LOG_INFO, "fd_pool: socket(%s): %m", conn->node->peer);
			continue;
		}

		/*
		 * Set the socket to be non-blocking so that we can time out in the connect step.
		 */
		flags = fcntl(fd, F_GETFL, 0);
		fcntl(fd, F_SETFL, flags | O_NONBLOCK);
		flags = fcntl(fd, F_GETFD, 0);
		fcntl(fd, F_SETFD, flags | FD_CLOEXEC);

		if (connect(fd, conn->node->sockaddr, conn->node->addrlen) < 0) {
			/*
			 * Connection in progress. poll on it so that we
			 * can time out (except if async).
			 */
			if (errno == EINPROGRESS) {
				struct pollfd pfd;
				int n;
				
				if (conn->async)
					break;

				memset(&pfd, 0, sizeof(pfd));

				pfd.fd = fd;
				pfd.events = POLLIN | POLLOUT | POLLHUP | POLLRDHUP;

				do {
					n = poll(&pfd, 1, conn->pool->timeout);
					/* XXX we restart the timeout on EINTR. */
				} while (n == -1 && errno == EINTR);

				if (n == 1 && (pfd.revents & (POLLIN|POLLOUT)) != 0) {
					/* Almost ok, just check SO_ERROR as well. */
					error = 0;
					sl = sizeof(error);
					if (getsockopt(fd, SOL_SOCKET, SO_ERROR, &error, &sl) == 0 && error == 0)
						break;

					if (error != 0)
						errno = error;
					log_printf(LOG_INFO, "fd_pool: getsockopt(%s): %m", conn->node->peer);
				} else {
					switch (n) {
					case 1:
						errno = ECONNREFUSED; /* XXX Not sure this is true. */
						break;
					case 0:
						errno = ETIMEDOUT;
						break;
					}
					log_printf(LOG_INFO, "fd_pool: poll(%s): %m", conn->node->peer);
				}
			} else {
				log_printf(LOG_INFO, "fd_pool: connect(%s): %m", conn->node->peer);
			}
			close(fd);
			fd = -1;
			if (conn->ti)
				timer_add_attribute(conn->ti, "conn_fail");
		}

	}
	if (fd != -1)
		log_printf(LOG_DEBUG, "fd_pool: Connected to %s", conn->node->peer);

	if (!conn->async) {
		/*
		 * Restore old flags, the socket shouldn't be non-blocking anymore.
		 */
		fcntl(fd, F_SETFL, flags);
	}

	return fd;
}

void
fd_pool_put(struct fd_pool_conn *conn, int fd) {
	struct fd_pool_entry *entry = conn->entry;
	static struct rlimit fdlimit;

	/* Assume it doesn't change after first time we're called. */
	if (!fdlimit.rlim_cur)
		getrlimit(RLIMIT_NOFILE, &fdlimit);

	if ((fdlimit.rlim_cur != RLIM_INFINITY && (unsigned)fd >= (fdlimit.rlim_cur * 9 / 10))) {
		/* Safety net while waiting for a better solution. */
		log_printf(LOG_DEBUG, "Not keeping fd %d due to rlimit", fd);
		close(fd);
		return;
	}

	if (entry) {
		conn->entry = NULL;
	} else {
		if (!SLIST_EMPTY(&conn->pool->free_entries)) {
			LOCK(&conn->pool->lock);
			entry = SLIST_FIRST(&conn->pool->free_entries);
			if (entry)
				SLIST_REMOVE_HEAD(&conn->pool->free_entries, link);
			UNLOCK(&conn->pool->lock);
		}
		if (!entry)
			entry = xmalloc(sizeof (*entry));
	}
	entry->fd = fd;
	LOCK(&conn->node->lock);
	SLIST_INSERT_HEAD(&conn->node->entries, entry, link);
	UNLOCK(&conn->node->lock);
}

struct bpapi_vtree_chain *
fd_pool_current_node(struct fd_pool_conn *conn) {
	return &conn->node->vtree;
}

struct fd_pool *
fd_pool_conn_pool(struct fd_pool_conn *conn) {
	return conn->pool;
}

void
fd_pool_set_async(struct fd_pool_conn *conn, int async) {
	conn->async = async;
}

void
fd_pool_set_timer(struct fd_pool_conn *conn, struct timer_instance *ti) {
	conn->ti = ti;
}

void
fd_pool_set_node_key(struct fd_pool_conn *conn, const char *key) {
	conn->node_filter = xstrdup(key);
}

int
fd_pool_conn_writev(struct fd_pool_conn *conn, int *fd, struct iovec *iov, int iovcnt, ssize_t iovlen_sum) {

	ssize_t n_total = iovlen_sum;

	if (n_total < 0)
		n_total = get_iovlen_sum(iov, iovcnt);

	int sockfd = *fd;
	int res = 0;

	do {
		if (sockfd < 0 || res < 0) {
			enum sbalance_conn_status status = SBCS_START;
			if (sockfd >= 0) {
				close(sockfd);
				status = SBCS_FAIL;
			}

			sockfd = fd_pool_get(conn, status, NULL, NULL);
		}

		/* No socket available to write to -- this is fatal */
		if (sockfd < 0) {
			*fd = -1;
			return -1;
		}

		res = writev_retry(sockfd, iov, iovcnt, n_total);

	} while (res < 0);

	*fd = sockfd;

	return 0;
}

