#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <libgen.h>

#include "util.h"
#include "bconfig.h"
#include "bconf.h"
#include "bconfd_client.h"

static int
config_init_file(const char *filename, struct bconf_node **rootp) {
	FILE *fp;
	char buf[1024];
	char *ptr;
	char *key;
	char *value;
	int i;

	if ((fp = fopen(filename, "r")) == NULL) {
		return -1;
	}

	/* Parse the file  */
	while (fgets(buf, sizeof(buf), fp)) {
		/* Check for newline or comment start characters */
		if ((ptr = strpbrk(buf, "\n")))
			*ptr = '\0';

		/* Require whitespace after 'include' to treat as directive */
		if ((strncmp(buf, "include", 7) == 0) && ((i = strspn(buf + 7, " \t")) > 0)) {
			char *n = buf + sizeof("include") - 1 + i;
			if (n[0] == '/') {
				config_init_file(n, rootp);
			} else {
				char *dir = xstrdup(filename);
				int res UNUSED;

				xasprintf(&n, "%s/%s", dirname(dir), n);
				res = config_init_file(n, rootp);

				free(dir);
				free(n);
			}
			continue;
		}

		/* Check for separator and split into key and value */
		if ( (ptr = strchr(buf, '=')) == NULL)
			continue;

		*ptr = '\0';

		key = buf;
		value = ptr + 1;

		while (isspace(*key))
			key++;
		/* We're actually a comment, ignore */
		if (*key == '#')
			continue;
		while (ptr > key && isspace(*(ptr - 1)))
			ptr--;
		*ptr = '\0';
		while (isspace(*value))
			value++;
		ptr = value + strlen(value);
		while (ptr > value && isspace(*(ptr - 1)))
			ptr--;
		*ptr = '\0';

		bconf_add_data(rootp, key, value);
	}

	fclose(fp);

	return 0;
}

struct bconf_node *
config_init(const char *filename) {
	struct bconf_node *config_root = NULL;

	if (config_init_file(filename, &config_root) != 0) {
		bconf_free(&config_root);
		return NULL;
	}

	return config_root;
}

void
load_bconf(const char *appl, struct bconf_node **root) {
	struct bconfd_call call = { 0 };

	bconfd_add_request_param(&call, "host", bconf_get_string(*root, "blocket_id"));
	bconfd_add_request_param(&call, "appl", appl);
	if (bconfd_load(&call, *root)) {
		bconf_free(&call.bconf);
		xerrx(1, "load_bconf failed: %s", call.errstr);	
	}

	bconf_merge(root, call.bconf);
	bconf_free(&call.bconf);
}

int
load_bconf_file(const char *appl, struct bconf_node **root, const char *filename) {
	struct bconf_node *tmproot, *starstar, *starappl, *hoststar, *hostappl;
	const char *host = NULL;

	if (bconf_get(*root, "blocket_id"))
		host = bconf_get_string(*root, "blocket_id");

	if ((tmproot = config_init(filename)) == NULL)
		return -1;

	starstar = bconf_vget(tmproot, "*", "*", NULL);
	starappl = appl ? bconf_vget(tmproot, "*", appl, NULL) : NULL;
	hoststar = host ? bconf_vget(tmproot, host, "*", NULL) : NULL;
	hostappl = (appl && host) ? bconf_vget(tmproot, host, appl, NULL) : NULL;

	if (starstar)
		bconf_merge(root, starstar);

	if (starappl && starappl != starstar)
		bconf_merge(root, starappl);
	if (hoststar && hoststar != starstar && hoststar != starappl)
		bconf_merge(root, hoststar);
	if (hostappl && hostappl != starstar && hostappl != starappl && hostappl != hoststar)
		bconf_merge(root, hostappl);

	bconf_free(&tmproot);
	return 0;
}
