#include <string.h>
#include <stdlib.h>
#include <sbalance.h>
#include <util.h>
#include <stdio.h>


struct sb_strategy {
	void (*strat_init)(struct sbalance_connection *, uint32_t hash);
	void (*strat_reinit)(struct sbalance_connection *);
	int (*strat_next)(struct sbalance_connection *);
};

void sbalance_seq_init(struct sbalance_connection *, uint32_t);
void sbalance_seq_reinit(struct sbalance_connection *);
int sbalance_seq_next(struct sbalance_connection *);

void sbalance_hash_init(struct sbalance_connection *, uint32_t);
void sbalance_rand_init(struct sbalance_connection *, uint32_t);
void sbalance_rand_reinit(struct sbalance_connection *);
int sbalance_rcycle_next(struct sbalance_connection *);

static struct sb_strategy strat_seq = { sbalance_seq_init, sbalance_seq_reinit, sbalance_seq_next };
static struct sb_strategy strat_rand = { sbalance_rand_init, sbalance_rand_reinit, sbalance_rcycle_next };
static struct sb_strategy strat_hash = { sbalance_hash_init, sbalance_rand_reinit, sbalance_rcycle_next };

void
sbalance_init(struct sbalance *sb, unsigned int retries, unsigned int failcost, unsigned int softfailcost, enum sbalance_strat strat) {
	memset(sb, 0, sizeof(*sb));

	sb->sb_retries = retries;
	sb->sb_failcost = failcost;
	sb->sb_softfailcost = softfailcost;
	switch(strat) {
	case ST_SEQ:
		sb->sb_strat = &strat_seq;
		break;
	case ST_RANDOM:
		sb->sb_strat = &strat_rand;
		break;
	case ST_HASH:
		sb->sb_strat = &strat_hash;
		break;
	}

	sb->sb_rand = rand_open();
}

void
sbalance_free(struct sbalance *sb, void (*free_f)(void *)) {
	unsigned int i;

	rand_close(sb->sb_rand);

	if (sb->sb_nserv == 0)
		return;
	if (free_f != NULL) {
		for (i = 0; i < sb->sb_nserv; i++) {
			(*free_f)(sb->sb_service[i].data);
		}
	}
	free(sb->sb_service);
}

/*
 * Add a service to the pool. Services are sorted in descending cost order.
 */
void
sbalance_add_serv(struct sbalance *sb, int cost, void *data) {
	if (cost == 0)
		cost = 1;

	sb->sb_service = xrealloc(sb->sb_service, sizeof(*sb->sb_service) * (sb->sb_nserv + 1));
	sb->sb_service[sb->sb_nserv].cost = cost;
	sb->sb_service[sb->sb_nserv].tempfailcost = 0;
	sb->sb_service[sb->sb_nserv].data = data;
	sb->sb_nserv++;
}

void
sbalance_conn_new(struct sbalance_connection *sc, struct sbalance *sb, uint32_t hash) {
	if (hash)
		rand_add(sb->sb_rand, &hash, sizeof(hash));
	sc->sc_sb = sb;

	(*sb->sb_strat->strat_init)(sc, hash);
}

void *
sbalance_conn_next(struct sbalance_connection *sc, enum sbalance_conn_status status) {
	struct sbalance *sb = sc->sc_sb;
	int newcost = 0;

	/*
	 * If the last connection failed for some reason, account for it in the temporary
	 * cost. Note, that we don't recalculate anything for this connection, this is
	 * for future use.
	 */
	if (status != SBCS_START) {
		if (status == SBCS_FAIL)
			newcost = sb->sb_failcost;
		else
			newcost = sb->sb_softfailcost;

		sb->sb_service[sc->sc_last].tempfailcost = newcost;
	}


	if (sc->sc_first == 0) {
		if (sc->sc_retries-- == 0)
			return (NULL);
		(*sb->sb_strat->strat_reinit)(sc);
	}

	sc->sc_first--;

	return sc->sc_sb->sb_service[sc->sc_last = (*sb->sb_strat->strat_next)(sc)].data;
}

void
sbalance_conn_done(struct sbalance_connection *sc) {
	struct sbalance *sb = sc->sc_sb;

	/*
	 * If we haven't exhausted retries, it means that we succeeded to
	 * connect to a service. In that case, if the service was under
	 * a temporary failure it means that the failure status can
	 * be cleared and the service can return to the normal cost.
	 */

	if (sb && sc->sc_retries >= 0 && sb->sb_service[sc->sc_last].tempfailcost) {
		sb->sb_service[sc->sc_last].tempfailcost = 0;
	}
}

void
sbalance_seq_init(struct sbalance_connection *sc, uint32_t hash) {
	sbalance_seq_reinit(sc);
	sc->sc_retries = sc->sc_sb->sb_retries;
}

void
sbalance_seq_reinit(struct sbalance_connection *sc) {
	sc->sc_offs = 0;
	sc->sc_first = sc->sc_sb->sb_nserv;
}

int
sbalance_seq_next(struct sbalance_connection *sc) {
	return sc->sc_offs++;
}

void
sbalance_rand_init(struct sbalance_connection *sc, uint32_t hash) {
	sbalance_hash_init(sc, rand_uint32(sc->sc_sb->sb_rand));
}

void
sbalance_rand_reinit(struct sbalance_connection *sc) {
	struct sbalance *sb = sc->sc_sb;

	rcycle_init(&sc->sc_rc, sb->sb_nserv, rand_uint32(sb->sb_rand));
	sc->sc_first = sb->sb_nserv;
	sc->sc_offs = -1;
}

void
sbalance_hash_init(struct sbalance_connection *sc, uint32_t hash) {
	struct sbalance *sb = sc->sc_sb;
	struct drand48_data rd;
	unsigned int pick = 0;
	unsigned int i;
	double tw = 0.0;

	srand48_r(hash, &rd);

	for (i = 0; i < sb->sb_nserv; i++) {
		double w = 1.0/(double)(sb->sb_service[i].tempfailcost ?: sb->sb_service[i].cost);
		double r;

		tw += w;
		drand48_r(&rd, &r);

		if ((w / tw) > r)
			pick = i;
	}

	sc->sc_offs = pick;
	sc->sc_first = 1;
	sc->sc_retries = sb->sb_retries;
}

int
sbalance_rcycle_next(struct sbalance_connection *sc) {
	if (sc->sc_offs != -1)
		return sc->sc_offs;
	else
		return rcycle_generate(&sc->sc_rc);
}

uint32_t
sbalance_hash_string(const char *str) {
	uint32_t hash = 2166136261;

	while (*str)
		hash = (hash * 16777619) ^ *str++;

	return hash;
}

