#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <netdb.h>
#include <ctype.h>
#include "queue.h"
#include "util.h"
#include "bconf.h"
#include "logging.h"

#include "cacheapi.h"
#include "vtree.h"
#include "memcacheapi.h"
#include "sredisapi.h"
#include "fd_pool.h"

int
cache_init_pools(struct cache_pools *pools, struct bpapi_vtree_chain *vtree) {
	struct bpapi_vtree_chain session_node = { 0 };
	const char *backend_node_path;
	int res = 0;

	memset(pools, 0, sizeof(struct cache_pools));

	if (!vtree_getnode(vtree, &session_node, "common", "session", NULL))
		return -1;

	backend_node_path = vtree_get(&session_node, "backend", "memcache", "node", NULL);
	if (backend_node_path && vtree_getnode(vtree, &pools->memcache_node, backend_node_path, NULL)) {
		pools->memcache = fd_pool_create(&pools->memcache_node, 0, NULL);
		res |= 2;
	}

	backend_node_path = vtree_get(&session_node, "backend", "redis", "node", NULL);
	if (backend_node_path && vtree_getnode(vtree, &pools->redis_node, backend_node_path, NULL)) {
		if (vtree_haskey(&pools->redis_node, "id_separator", NULL)) {
			pools->redis = fd_pool_create(&pools->redis_node, 0, NULL);
			res |= 1;
		} else {
			vtree_free(&pools->redis_node);
			memset(&pools->redis_node, 0, sizeof(pools->redis_node));
			pools->redis = NULL;
		}
	}

	pools->write_both = vtree_getint(&session_node, "write_both", NULL);
	vtree_free(&session_node);
	return res;
}

void
cache_free_pools(struct cache_pools *pools) {
	if (pools->redis)
		fd_pool_free(pools->redis);

	if (pools->memcache)
		fd_pool_free(pools->memcache);
}

char *
cache_get(struct cache_pools *pools, const char *session_id, const char *key) {
	char *ret = NULL;

	if (pools->redis) {
		ret = session_redis_get(session_id, key, pools->redis);
	}

	if(ret == NULL && pools->memcache) {
		ret = memcache_get(session_id, key, pools->memcache);
	}

	return ret;
}

int
cache_set(struct cache_pools *pools, const char *session_id, const char *key, const char *value, int expire) {
	int ret = -1;

	if (pools->redis) {
		ret = session_redis_set(session_id, key, value, expire, pools->redis);
	}

	if (pools->memcache && (!pools->redis || pools->write_both == 1)) {
		ret = memcache_set(session_id, key, value, expire, pools->memcache);
	}

	return ret;
}

