#ifndef PHP_H
#include <php.h>
#endif

#define ZEND_GET_MODULE_PROTOTYPE(name) \
	BEGIN_EXTERN_C()\
		ZEND_DLEXPORT zend_module_entry *get_module(void); \
	END_EXTERN_C()

