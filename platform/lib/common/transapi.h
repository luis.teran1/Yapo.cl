#ifndef TRANSAPI_H
#define TRANSAPI_H
#include <sys/types.h>

enum {
	TRANS_COMMIT_OK,
	TRANS_COMMIT_ERROR_LOOKUP   = -1,
	TRANS_COMMIT_ERROR_SOCKET   = -2,
	TRANS_COMMIT_ERROR_GETFL    = -3,
	TRANS_COMMIT_ERROR_SETFL    = -4,
	TRANS_COMMIT_ERROR_CONNECT  = -5,
	TRANS_COMMIT_ERROR_BUSY     = -6,
	TRANS_COMMIT_ERROR_IO       = -7,
	TRANS_COMMIT_ERROR_TIMEOUT  = -8,
	TRANS_COMMIT_ERROR_EOF      = -9,
	TRANS_COMMIT_ERROR_BLOB_NO_NL = -10
};


typedef struct transaction_struct transaction_t;
struct bpapi_vtree_chain;
struct fd_pool;

typedef void (*trans_cb_t)(transaction_t *trans, const char *key, size_t klen, const char *value, size_t vlen, void *cbdata);

transaction_t *trans_init_fd_pool(unsigned int timeout, struct fd_pool *pool);
transaction_t *trans_init_vtree(unsigned int timeout, struct bpapi_vtree_chain *vchain);
transaction_t *trans_init_host(unsigned int timeout, const char *hostname, const char *port);

int trans_add_pair(transaction_t *trans, const char *key, const char *value, ssize_t size);
void trans_add_text(transaction_t *trans, const char *text, size_t len);
int trans_add_printf(transaction_t *trans, const char *fmt, ...) __attribute__ ((format (printf, 2, 3)));

void trans_set_cbdata(transaction_t *trans, void *cbdata);
int trans_add_callback(transaction_t *trans, const char *key, trans_cb_t cb);
int trans_commit(transaction_t *trans); 
int trans_iter_next(transaction_t *trans, char **key, char **value);
void trans_iter_reset(transaction_t *trans);
void trans_iter_reset_to_request(transaction_t *trans);
char *trans_get_key(transaction_t *trans, const char *key);
int trans_get_response_size(transaction_t *trans);
void trans_free(transaction_t *trans);

const char *trans_strerror(int error);

#endif
