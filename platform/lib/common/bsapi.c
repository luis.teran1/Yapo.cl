#include "bsapi.h"

#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <netdb.h>
#include <ctype.h>
#include <util.h>

#include <sys/poll.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netinet/tcp.h>

#include "bconf.h"
#include <timer.h>
#include "logging.h"
#include "strl.h"
#include "atomic.h"

extern int h_errno;

#define BUFFER_SIZE 65536

struct bsconf_conf {
	struct bpapi_vtree_chain vtree;
	TAILQ_ENTRY(bsconf_conf) bc_list;
	struct bconf_node *bc_conf;
	int ref;
};

static void bsearch_parse_conf(struct bsearch_api *ba, const char *key, const char *value);
static struct bsconf_conf *bsearch_finalize_conf(struct bsearch_api *ba, const char *footprint);
static void bsearch_release_conf(struct bsconf_conf *);

int
bsearch_connection_create(struct bsearch_api *ba, int async) {
	int fd;
	const char *peer;

	if (ba->ti)
		fd_pool_set_timer(ba->conn, ba->ti);
	fd = fd_pool_get(ba->conn, ba->sc_status, &peer, &ba->keepalive);
	if (fd == -1) {
		log_printf(LOG_ERR, "Connect failed: No hosts left to try (%m)");
		ba->berrno = errno + 2024;
		return -2;
	}

	return (ba->sockfd = fd);
}

static int
readall(struct bsearch_api *ba) {
	int n = 0;
	int tot = 0;
	int totlines = -1;
	int nlines = 0;

	while ((n = read(ba->sockfd, ba->buffer + tot, ba->buffer_size - tot - 1))) {
		char *ptr = memrchr(ba->buffer, '\n', tot);
		char *np;

		if (n == -1) {
			ba->berrno = errno + 4024;
			return -1;
		}
		
		tot += n;
		ba->buffer[tot] = '\0';

		/* Look for info:lines: to determine when to quit reading. */
		if (ptr)
			ptr++;
		else
			ptr = ba->buffer;
		for (; (np = memchr (ptr, '\n', ba->buffer + tot - ptr)) ; ptr = np + 1) {
			if (totlines == -1) {
				if (!strncmp(ptr, "info:goaway\n", sizeof("info:goaway\n") - 1)) {
					return tot;
				} else if (ba->buffer + tot - ptr > 11 && 
				    memcmp (ptr, "info:lines:", 11) == 0) {
					totlines = atoi(ptr + 11);
				}
			} else if (np - ptr < 5 || memcmp (ptr, "info:", 5) != 0)
				nlines++;
		}
		/* We read totlines + 1 lines to include the headers. */
		if (totlines != -1 && nlines > totlines)
			break;

		if (ba->buffer_size - tot < 1024) {
			/* Increase buffer_size */
			char *tmp = realloc(ba->buffer, ba->buffer_size + 2048);
			if (tmp) {
				ba->buffer = tmp;
				ba->buffer_size += 2048;
			} else {
				ba->berrno = errno + 1024;
				return -1;
			}
		}
	}

	return tot;
}

static int
read_response(struct bsearch_api *ba) {
	struct pollfd pfd;
	int res, n;

	memset(&pfd, 0, sizeof(pfd));

	pfd.fd = ba->sockfd;
	pfd.events = POLLIN | POLLHUP | POLLRDHUP;

	do {
		res = poll(&pfd, 1, ba->bs->bs_timeout);
		/* XXX we restart the timeout on EINTR. */
	} while (res == -1 && errno == EINTR);

	if (res > 0) {
		if (((pfd.revents & POLLIN) == 0) ||
		    ((n = readall(ba)) <= 0)) {
			ba->berrno = BERR_RECONNECT;
			close(ba->sockfd);
			return 1;
		}

		if (ba->keepalive) {
			fd_pool_put(ba->conn, ba->sockfd);
		} else {
			close(ba->sockfd);
		}
		ba->buffer[n] = '\0';
		return 0;
	} else {
		if (res == 0) 
			ba->berrno = BERR_TIMEOUT;
		else
			ba->berrno = errno + 1024;
		close(ba->sockfd);

		return 1;
	}
}

static int
_bsearch_init_bsconf(struct bsearch_api *ba, struct bsconf *bs, const char *remote_addr) {
	ba->bs = bs;
	ba->berrno = BERR_SUCCESS;
	ba->buffer = xmalloc(BUFFER_SIZE);
	ba->buffer_size = BUFFER_SIZE - 1;
	ba->sc_status = SBCS_START;
	if (ba->conn)
		fd_pool_free_conn(ba->conn);
	ba->conn = fd_pool_new_conn(bs->pool, remote_addr, 0);
	if (bs->bs_timer_name) {
		ba->ti_root = timer_start(NULL, bs->bs_timer_name);
		ba->ti = timer_start(ba->ti_root, "setup");
	} else {
		ba->ti_root = NULL;
		ba->ti = NULL;
	}

	return 0;
}

int
bsearch_init_bsconf(struct bsearch_api *ba, struct bsconf *bs, const char *remote_addr) {
	memset(ba, 0, sizeof(*ba));
	return _bsearch_init_bsconf(ba, bs, remote_addr);
}

int
bsconf_init_vtree(struct bsconf *bs, struct bpapi_vtree_chain *node, int use_keepalive) {

	pthread_mutex_init(&bs->lock, NULL);

	if (vtree_haskey(node, "use_timer", NULL))
		bs->bs_timer_name = vtree_get(node, "use_timer", NULL);
	else
		bs->bs_timer_name = NULL;

	bs->use_conf = vtree_getint(node, "use_conf", NULL);
	bs->bs_timeout = vtree_getint(node, "timeout", NULL);
	if (bs->bs_timeout <= 0)
		bs->bs_timeout = BS_DEFAULT_TIMEOUT;

	bs->pool = fd_pool_create(node, use_keepalive, NULL);

	TAILQ_INIT(&bs->bs_confs);
	bs->bs_nconfs = 0;

	if (!bs->pool)
		return 1;

	return 0;
}

int
bsearch_init_vtree(struct bsearch_api *ba, struct bpapi_vtree_chain *node, const char *remote_addr) {
	int ret;

	memset(ba, 0, sizeof(*ba));
	if ((ret = bsconf_init_vtree(&ba->bs_static, node, 0)) != 0)
		return (ret);
	return _bsearch_init_bsconf(ba, &ba->bs_static, remote_addr);
}

void
bsconf_del(struct bsconf *bs) {
	struct bsconf_conf *bc;

	while ((bc = TAILQ_FIRST(&bs->bs_confs)) != NULL) {
		TAILQ_REMOVE(&bs->bs_confs, bc, bc_list);
		bsearch_release_conf(bc);
	}
	if (bs->pool)
		fd_pool_free(bs->pool);
}

void 
bsearch_cleanup(struct bsearch_api *ba) {
	/*
	 * Must be careful here since we can be called without ever having been initialized.
	 */

	if (ba->buffer) {
		free(ba->buffer);
		ba->buffer = NULL;
	}

	if (ba->conn)
		fd_pool_free_conn(ba->conn);
	ba->conn = NULL;

	if (ba->conf_refs[0]) {
		unsigned int i;

		for (i = 0; i < sizeof(ba->conf_refs) / sizeof(ba->conf_refs[0]); i++) {
			if (ba->conf_refs[i]) {
				bsearch_release_conf(ba->conf_refs[i]);
				ba->conf_refs[i] = NULL;
			}
		}
	}
	/*
	 * If this was a one-shot connection (bsearch_init_vtree), free the sbalance struct.
	 */
	if (ba->bs == &ba->bs_static)
		bsconf_del(ba->bs);

	if (ba->ti_root) {
		timer_end(ba->ti_root, NULL);
	}


	ba->berrno = BERR_SUCCESS;
}

static void
parse_line(struct bsearch_api *ba, char *header, char *val, void (*parse_cb)(int, int, const char *, const char **, void *), void *cb_data) {
	char *value = val;
	char *h_end;
	char *v_end;

	/* fprintf(stderr, "parse_line (%s) -> (%s) \n", header, value);  */

	while ((h_end = strchr(header, '\t')), (v_end = strchr(value, '\t'), h_end)) {
		*h_end = '\0';

		if (!v_end) {
			parse_cb(BS_VALUE, ba->row, header, (const char *[]){ value, NULL }, cb_data);
			value = value + strlen(value);
		} else {
			*v_end = '\0';
			parse_cb(BS_VALUE, ba->row, header, (const char *[]){ value, NULL }, cb_data);
			value = v_end + 1;
		}

		/* Restore the header */
		*h_end = '\t';

		header = h_end + 1;
	}

	parse_cb(BS_VALUE, ba->row, header, (const char *[]){ value, NULL }, cb_data);
	ba->row++;
}

/* Find the number of \t seaprated fields on the line */
static int
count_cols(char *header) {
	int res = 1;
	
	while ((header = strchr(header, '\t'))) {
		header++;
		res++;
	}

	return res;
}

/* Separate all \t separated fields with \0 */
static void
sep_cols(char *header) {
	while ((header = strchr(header, '\t'))) {
		*header++ = '\0';
	}
}

int
bsearch_parse_list(struct bsearch_api *ba) {
	void (*parse_cb)(int, int, const char *, const char **, void *) = ba->parse_cb;
	void *cb_data = ba->cb_data;
	char *header;
	char *line;
	char *data;
	char *ptr;

	if ((data = strchr(ba->buffer, '\n'))) {
		line = ba->buffer;

		/* Read conf: lines */
		while (strncmp(line, "conf:", 5) == 0) {
			char *conf = line + 5;
			ptr = strchr(conf, '=');
			if (!ptr) {
				line = strchr(line, '\n');
				line++;
				continue;
			}

			*ptr = '\0';
			ptr++;
			line = strchr(ptr, '\n');
			*line = '\0';

			if (ba->query_flags & BS_FLAG_CONF)
				bsearch_parse_conf(ba, conf, ptr);
			else
				parse_cb(BS_CONF, 0, conf, (const char *[]){ ptr, NULL }, cb_data); 

			line++;
		}

		/* Read info: lines */
		while (strncmp(line, "info:", 5) == 0) {
			char *info = line + 5;
			const char *infov[64];
			char *s, *d;
			int infoc;

			ptr = strchr(info, ':');
			if (!ptr) {
				line = strchr(line, '\n');
				line++;
				continue;
			}

			*ptr = '\0';
			ptr++;

			infoc = 0;
			infov[infoc++] = ptr;

			s = d = ptr;
			while (*s != '\n') {
				if (*s == '\\') {
					s++;
					if (*s != ':' && *s != '\\') {
						return -1;
					}
					*d++ = *s++;
					continue;
				}
				if (*s == ':') {
					s++;
					*d++ = '\0';
					infov[infoc++] = d;
				} else {
					*d++ = *s++;
				}
				if (*s == '\0') {
					return -1;
				}
			}

			*d = '\0';
			line = s;
			*line = '\0';
			infov[infoc] = NULL;

			if ((ba->query_flags & BS_FLAG_CONF) && !strcmp(info, "footprint")) {
				ba->conf = bsearch_finalize_conf(ba, *infov);
			}

			parse_cb(BS_INFO, 0, info, infov, cb_data); 

			line++;
		}

		/* Setup pointer to header and find first data line */
		header = line;
	       
		if ((line = strchr(header, '\n'))) {
			int rows = 0;
			*line++ = '\0';
			
			parse_cb(BS_COLS, count_cols(header), NULL, NULL, cb_data);
		
			/* Find next line */
			while ((data = strchr(line, '\n')) ) {
				*data = '\0';
				
				parse_line(ba, header, line, parse_cb, cb_data);
				rows++;
				line = data + 1;
			}

			sep_cols(header);
			if (rows)
				parse_cb(BS_DONE, rows, NULL, NULL, cb_data);
			else
				parse_cb(BS_NOAD, 0, NULL, NULL, cb_data);
		} else
			parse_cb(BS_NOAD, 0, NULL, NULL, cb_data);
	} else
		parse_cb(BS_NOAD, 0, NULL, NULL, cb_data);
		
	return 0;
}

int 
bsearch_getad(struct bsearch_api *ba, int ad_id, void (*parse_cb)(int, int, const char *, const char **, void *), void *cb_data) {
	char query[64];

	snprintf(query, sizeof(query), "0 id:%u", ad_id);
	return bsearch_search(ba, query, parse_cb, cb_data);
}

#define TRANSACTION_BANNER_OK "220 Welcome.\n"

static int
parse_trans_result(struct bsearch_api *ba, void (*parse_cb)(int, int, const char *, const char **, void *), void *cb_data) {
	char *data;
	char *ptr;
	char *header;
	char *line;

	ptr = ba->buffer;

	if (memcmp(ptr, TRANSACTION_BANNER_OK, sizeof(TRANSACTION_BANNER_OK) - 1) != 0)
		return -1;

	ptr += sizeof(TRANSACTION_BANNER_OK) - 1;

	/* Take care of header: "rows:<total-count>\n" */
	if ((data = strchr(ptr, '\n'))) {
		*data = '\0';

		if (strncmp(ptr, "rows:", 5) != 0)
			return -1;

		ptr = ptr + 5;
		if (*ptr == '\0')
			return -1;

		/* Find total */
		parse_cb(BS_INFO, 0, "total", (const char *[]){ ptr, NULL }, cb_data); 

		if (strcmp(ptr, "0") == 0) {
			parse_cb(BS_NOAD, 0, NULL, NULL, cb_data);
			return 0;
		}

		/* Setup pointer to header and find first data line */
		header = data + 1;
		if (strncmp(header, "hdrs:", 5) != 0)
			return -1;

		header += 5;
	       
		if ((line = strchr(header, '\n'))) {
			int rows = 0;
			*line++ = '\0';
			
			parse_cb(BS_COLS, count_cols(header), NULL, NULL, cb_data);
		
			/* Find next line */
			while ((data = strchr(line, '\n'))) {
				*data = '\0';

				if (strncmp(line, "status:", 7) == 0) {
					line = data + 1;
					continue;
				}
				
				if (strcmp(line, "end") == 0)
					break;
				
				if (strncmp(line, "row:", 4) != 0) {
					return -1;
				}
				line += 4;
				parse_line(ba, header, line, parse_cb, cb_data);
				rows++;
				line = data + 1;
			}

			sep_cols(header);
			if (rows)
				parse_cb(BS_DONE, rows, NULL, NULL, cb_data);
			else
				parse_cb(BS_NOAD, 0, NULL, NULL, cb_data);
		}
	}
		
	return 0;
}

/*
 * Old backwards compat wrapper.
 */
int
bsearch_search(struct bsearch_api *ba, const char *search_string, void (*parse_cb)(int, int, const char *, const char **, void *), void *cb_data) {
	return bsearch_do_search(ba, search_string, 0, parse_cb, cb_data);
}

int
bsearch_do_search(struct bsearch_api *ba, const char *search_string, int flags, void (*parse_cb)(int, int, const char *, const char **, void *), void *cb_data) {
	ssize_t slen = strlen(search_string), wlen;
	char confbuf[1024];
	struct iovec w[3];
	int iovcnt = 0;
	int fd;

	wlen = slen;

	if (ba->ti)
		timer_handover(ba->ti, "search");

	ba->query_flags = flags;

	/*
	 * If the user wants us to handle conf and the backend uses conf,
	 * add conf data to the query.
	 */
	if ((flags & BS_FLAG_CONF) && ba->bs->use_conf) {
		struct bsconf_conf *bc;
		int i = 0;

		pthread_mutex_lock(&ba->bs->lock);
		strlcpy(confbuf, "F:", sizeof(confbuf));
		TAILQ_FOREACH(bc, &ba->bs->bs_confs, bc_list) {
			ba->conf_refs[i] = bc;
			bc->ref++;
			
			if (bc != TAILQ_FIRST(&ba->bs->bs_confs)) {
				strlcat(confbuf, ",", sizeof(confbuf));
			}
			strlcat(confbuf, bconf_get_string(bc->bc_conf, "footprint"), sizeof(confbuf));
			if (++i == sizeof(ba->conf_refs) / sizeof(ba->conf_refs[0]))
				break;
		}
		if (TAILQ_EMPTY(&ba->bs->bs_confs)) {
			strlcat(confbuf, "init ", sizeof(confbuf));
		} else {
			strlcat(confbuf, " ", sizeof(confbuf));
		}
		pthread_mutex_unlock(&ba->bs->lock);
		if (flags & BS_FLAG_CONF_INFO) {
			parse_cb(BS_INFO, 0, "conf_query", (const char *[]) { &confbuf[0], NULL }, cb_data);
		}
		w[0].iov_base = confbuf;
		w[0].iov_len = strlen(confbuf);
		w[1].iov_base = (void *)search_string;
		w[1].iov_len = slen;
		iovcnt = 2;
		wlen += w[0].iov_len;
	} else {
		ba->query_flags &= ~BS_FLAG_CONF;
		w[0].iov_base = (void *)search_string;
		w[0].iov_len = slen;
		iovcnt = 1;
	}

	if (search_string[slen - 1] != '\n') {
		w[iovcnt].iov_base = (void *)"\n";
		w[iovcnt].iov_len = 1;
		iovcnt++;
		wlen += 1;
	} else {
		iovcnt = 1;
	}

	while (1) {
		fd = bsearch_connection_create(ba, 0);
		if (fd < -1)
			break;
		if (ba->ti)
			timer_handover(ba->ti, "send_query");
		/* Send query */
		if (writev(fd, w, iovcnt) != wlen) {
			ba->berrno = 9024 + errno;
			ba->sc_status = SBCS_FAIL;
			log_printf(LOG_INFO, "Short write: %d\n", errno);
			close(fd);
			if (ba->ti)
				timer_add_attribute(ba->ti, "write_fail");
			continue;
		}

		if (ba->ti)
			timer_handover(ba->ti, "read_response");
		/* Read response into buffer, honoring the timeout. */
		if (read_response(ba) != 0) {
			if (ba->berrno != BERR_RECONNECT)
				log_printf(LOG_ERR, "read_response failed: %s\n", bsearch_errstr(ba->berrno));
			ba->sc_status = SBCS_FAIL;
			if (ba->ti)
				timer_add_attribute(ba->ti, "read_fail");
			continue;
		}

		if (!ba->buffer) {
			return 1;
		}

		/* Is search engine busy? */
		if ((memcmp(ba->buffer, SEARCH_BANNER_BUSY, sizeof(SEARCH_BANNER_BUSY) - 1) == 0)) {
			ba->sc_status = SBCS_TEMPFAIL;
			if (ba->ti)
				timer_add_attribute(ba->ti, "goaway");
			continue;
		}

		if ((memcmp(ba->buffer, TRANSACTION_BANNER_OK, sizeof(TRANSACTION_BANNER_OK) - 1) == 0)) {
			if (parse_trans_result(ba, parse_cb, cb_data) != 0) {
				ba->berrno = BERR_PARSE;
				return 1;
			}
		} else {
			ba->parse_cb = parse_cb;
			ba->cb_data = cb_data;
			if (ba->ti)
				timer_handover(ba->ti, "parse");
			if (bsearch_parse_list(ba) != 0) {
				ba->berrno = BERR_PARSE;
				return 1;
			}
		}
		break;
	}
	if (ba->ti) {
		timer_end(ba->ti, NULL);
	}

	return ((fd == -2) ? 1 : 0);
}

/*
 * Error message handling
 */

static const char *
_bsearch_errors[] = {
	"No error",
	"Host name lookup error",
	"Connection failure",
	"Communication timeout",
	"Parse error",
	"No such ad",
	"Configuration error",
	"Connection timeout",
	"Needs reconnect"
};

const char *
bsearch_errstr(int berrno) {
	if (berrno >= 65536) {
		return hstrerror(berrno - 65536);
	} else if (berrno >= 1024) {
		return xstrerror(berrno % 1000 - 24);
	} else {
		return _bsearch_errors[berrno];
	}
}

/*
 * Conf management.
 *
 * The caller has the option to handle conf itself, but since it makes much more sense to have it cached per bsapi,
 * we provide conf management here which will be used when bsearch_do_search is called with the BS_FLAG_CONF flag.
 */

static void
bsearch_parse_conf(struct bsearch_api *ba, const char *key, const char *value) {
	/*
	 * Always allocate and parse the conf. When we're done, we will check in the bsconf if
	 * the conf is already there and drop this one and reuse the old one.
	 *
	 * This is potentially inefficient, but it avoids a locking hell.
	 */
	if (ba->conf == NULL) {
		ba->conf = zmalloc(sizeof(*ba->conf));
		ba->conf->ref = 1;
	}
	bconf_add_data(&ba->conf->bc_conf, key, value);
}

struct bpapi_vtree_chain *
bsearch_get_conf(struct bsearch_api *ba) {
	return ba->conf ? &ba->conf->vtree : NULL;
}

static struct bsconf_conf *
bsearch_finalize_conf(struct bsearch_api *ba, const char *footprint) {
	struct bsconf_conf *bc = NULL;

	pthread_mutex_lock(&ba->bs->lock);
	if (footprint) {
		TAILQ_FOREACH(bc, &ba->bs->bs_confs, bc_list) {
			if (!strcmp(footprint, bconf_get_string(bc->bc_conf, "footprint"))) {
			  	(void)atomic_xadd_int(&bc->ref, 1);
				break;
			}
		}
	}

	/*
	 * We parsed some kind of conf. Let's finalize it or throw away.
	 */
	if (ba->conf && ba->conf->bc_conf) {
		if (!bc) {
			bc = ba->conf;
			bc->ref++;
			bconf_vtree(&bc->vtree, ba->conf->bc_conf);
			TAILQ_INSERT_HEAD(&ba->bs->bs_confs, bc, bc_list);
			if (++ba->bs->bs_nconfs > 3) {
				struct bsconf_conf *bsf = TAILQ_FIRST(&ba->bs->bs_confs), *n;
				while ((n = TAILQ_NEXT(bsf, bc_list))) {
					bsf = n;
				}
				TAILQ_REMOVE(&ba->bs->bs_confs, bsf, bc_list);
				ba->bs->bs_nconfs--;
				bsearch_release_conf(bsf);
			}
		}
	}

	pthread_mutex_unlock(&ba->bs->lock);

	/* Done outside the lock */
	if (bc != ba->conf && ba->conf) {
		bconf_free(&ba->conf->bc_conf);
		free(ba->conf);
		ba->conf = NULL;
	}

	return bc;
}

void
bsearch_put_conf(struct bsearch_api *ba) {
	if (ba->conf)
		bsearch_release_conf(ba->conf);
}

static void
bsearch_release_conf(struct bsconf_conf *bc) {
	if (atomic_xadd_int(&bc->ref, -1) == 1) {
		vtree_free(&bc->vtree);
		bconf_free(&bc->bc_conf);
		free(bc);
	}
}
