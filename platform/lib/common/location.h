#ifndef LOCATION_H
#define LOCATION_H
#include <math.h>

struct location {
	double rt90_x;
	double rt90_y;
	double wgs84_lat;
	double wgs84_lon;
};

void wgs84_to_rt90(struct location *l);
void wgs84_to_utm(struct location *l);
#endif
