#ifndef BSAPI_H
#define BSAPI_H

#include <sys/socket.h>
#include <netinet/in.h>
#include <pthread.h>
#include "vtree.h"
#include "fd_pool.h"
#include "queue.h"

enum bsearch_status {
	BERR_SUCCESS, 
	BERR_LOOKUP, 
	BERR_CONNECTION, 
	BERR_TIMEOUT, 
	BERR_PARSE, 
	BERR_NOAD, 
	BERR_CONFIG, 
	BERR_CONNECT_TIMEOUT,
	BERR_RECONNECT
};
enum bsearch_state {
	BS_INFO,
	BS_NOAD,
	BS_VALUE,
	BS_DONE,
	BS_COLS,
	BS_CONF,
};
#define BS_DEFAULT_TIMEOUT 5000
#define SEARCH_BANNER_BUSY "info:goaway"

struct bsconf_conf;

struct bsconf {
	struct fd_pool *pool;
	int bs_timeout;
	const char *bs_timer_name;
	int use_conf;

	pthread_mutex_t lock;
	TAILQ_HEAD(,bsconf_conf) bs_confs;
	int bs_nconfs;
};

struct bsearch_api {
	struct bsconf bs_static;
	struct bsconf *bs;
	enum sbalance_conn_status sc_status;
	struct fd_pool_conn *conn;
	struct timer_instance *ti_root;
	struct timer_instance *ti;

	int query_flags;

	struct bsconf_conf *conf_refs[3];
	struct bsconf_conf *conf;
	
	char *buffer;
	int buffer_size;

	int berrno;
	int sockfd;
	int keepalive;

	char *current_line;
	char *next_line;
	int first_line;
	int flag;
	int row;

	void (*parse_cb)(int, int, const char *, const char **, void*);
	void *cb_data;
};

int bsconf_init_vtree(struct bsconf *, struct bpapi_vtree_chain *, int use_keepalive);
void bsconf_del(struct bsconf *bs);

int bsearch_init_vtree(struct bsearch_api *, struct bpapi_vtree_chain *node, const char *remote_addr);
int bsearch_init_bsconf(struct bsearch_api *, struct bsconf *, const char *remote_addr);
void bsearch_cleanup(struct bsearch_api*); 

int bsearch_getad(struct bsearch_api *, int, void (*)(int, int, const char *, const char **, void *), void *);

/* Flag to tell bsearch_do_search to deal with the conf for us. */
#define BS_FLAG_CONF		0x01
#define BS_FLAG_CONF_INFO	0x02
int bsearch_do_search(struct bsearch_api *, const char *query, int flags, void (*)(int, int, const char *, const char **, void *), void *);
int bsearch_search(struct bsearch_api *, const char *search_string, void (*)(int, int, const char *, const char **, void *), void*);

struct event_base;
int bsearch_async_search(struct bsearch_api *, struct event_base *, const char *search_string, void (*)(int, int, const char *, const char **, void*), void*);
const char *bsearch_errstr(int berrno);
int bsearch_parse_list(struct bsearch_api *ba);

struct bpapi_vtree_chain *bsearch_get_conf(struct bsearch_api *);
void bsearch_put_conf(struct bsearch_api *);

/* For async api, do not use directly.  */
int bsearch_connection_create(struct bsearch_api *ba, int async);

#endif
