/*
 * This code was originally for moftpd, and therefore has a bit different
 * coding style.
 */

#include "tls.h"
#include "util.h"

#include <openssl/ssl.h>
#include <openssl/rand.h>
#include <openssl/err.h>
#include <pthread.h>

struct CRYPTO_dynlock_value
{
	pthread_rwlock_t rwlock;
};

static pthread_mutex_t *tls_locks;

static void
tls_locking_function(int mode, int n, const char *file, int line) {
	if (mode & CRYPTO_LOCK)
		pthread_mutex_lock(&tls_locks[n]);
	else
		pthread_mutex_unlock(&tls_locks[n]);
}

static unsigned long
tls_thread_id_function(void) {
	return pthread_self();
}

static struct CRYPTO_dynlock_value *
tls_dynlock_create(const char *file, int line) {
	struct CRYPTO_dynlock_value *res = zmalloc(sizeof (*res));

	pthread_rwlock_init(&res->rwlock, NULL);
	return res;
}

static void
tls_dynlock_lock(int mode, struct CRYPTO_dynlock_value *l, const char *file, int line) {
	switch (mode) {
	case CRYPTO_LOCK | CRYPTO_WRITE:
		pthread_rwlock_wrlock(&l->rwlock);
		break;
	case CRYPTO_LOCK | CRYPTO_READ:
		pthread_rwlock_rdlock(&l->rwlock);
		break;
	case CRYPTO_UNLOCK | CRYPTO_WRITE:
	case CRYPTO_UNLOCK | CRYPTO_READ:
		pthread_rwlock_unlock(&l->rwlock);
		break;
	default:
		xerrx(1, "Unknown mode to %s: 0x%x, called at %s:%d", __func__, mode, file, line);
	}
}

static void
tls_dynlock_destroy(struct CRYPTO_dynlock_value *l, const char *file, int line) {
	pthread_rwlock_destroy(&l->rwlock);
	free(l);
}

const char *
tls_get_cert_dir (void)
{
	return X509_get_default_cert_dir ();
}

static int
tls_init (struct tls_context *ctx) {
	static pthread_mutex_t init_mutex = PTHREAD_MUTEX_INITIALIZER;
	static int inited = 0;

	pthread_mutex_lock (&init_mutex);
	if (!inited)
	{
		int nlocks;
		int i;

		SSL_load_error_strings ();
		SSL_library_init ();

		nlocks = CRYPTO_num_locks();
		tls_locks = xmalloc(nlocks * sizeof (*tls_locks));
		for (i = 0 ; i < nlocks ; i++)
			pthread_mutex_init(&tls_locks[i], NULL);
		CRYPTO_set_locking_callback(tls_locking_function);
		CRYPTO_set_id_callback(tls_thread_id_function);
		CRYPTO_set_dynlock_create_callback(tls_dynlock_create);
		CRYPTO_set_dynlock_lock_callback(tls_dynlock_lock);
		CRYPTO_set_dynlock_destroy_callback(tls_dynlock_destroy);

		inited = 1;
	}

	if (!ctx->ssl_ctx)
	{
		ctx->ssl_ctx = SSL_CTX_new (ctx->method ?: SSLv23_method());
		if (!ctx->ssl_ctx) {
			pthread_mutex_unlock (&init_mutex);
			return 1;
		}
		if (ctx->ssl_certs_path)
			SSL_CTX_load_verify_locations (ctx->ssl_ctx, NULL, ctx->ssl_certs_path);
		SSL_CTX_set_verify(ctx->ssl_ctx, SSL_VERIFY_NONE, NULL);
	}

	if (!ctx->context_id_set)
	{
		ctx->context_id_set = 1;
		RAND_pseudo_bytes (ctx->context_id, sizeof (ctx->context_id));
	}

	pthread_mutex_unlock (&init_mutex);
	return 0;
}

struct tls *
tls_open (struct tls_context *ctx, int fd, int options, tlscert_t cert, tlskey_t key)
{
	struct tls *res;
	int sslopts;

	if (fd < 0)
		return NULL;

	if (tls_init(ctx))
		return NULL;

	res = zmalloc (sizeof (*res));
	if (!res) {
		return NULL;
	}
	res->ssl = SSL_new (ctx->ssl_ctx);
	if (!res->ssl) {
		free(res);
		return NULL;
	}

	sslopts = 0;
	SSL_set_options (res->ssl, sslopts);
	if (cert && key) {
		if (!SSL_use_certificate (res->ssl, cert))
		{
			syslog (LOG_ERR, "Failed to load certificate file: %s",
					ERR_reason_error_string (ERR_get_error ()));
			SSL_free (res->ssl);
			free(res);
			return NULL;
		}
		if (!SSL_use_PrivateKey (res->ssl, key))
		{
			syslog (LOG_ERR, "Failed to load private key: %s",
					ERR_reason_error_string (ERR_get_error ()));
			SSL_free (res->ssl);
			free(res);
			return NULL;
		}
	}

	SSL_set_session_id_context (res->ssl, ctx->context_id, sizeof (ctx->context_id));
	if (options & tlsVerifyPeer)
	{
		SSL_set_verify (res->ssl, SSL_VERIFY_PEER | SSL_VERIFY_FAIL_IF_NO_PEER_CERT |
				SSL_VERIFY_CLIENT_ONCE, NULL);

		if (ctx->ssl_certs_path) {
			STACK_OF(X509_NAME) *castack = sk_X509_NAME_new_null ();

			SSL_add_dir_cert_subjects_to_stack (castack, ctx->ssl_certs_path);
			SSL_set_client_CA_list (res->ssl, castack);
		}
	}
	res->bio = BIO_new_socket (fd, BIO_NOCLOSE);
	if (!res->bio)
	{
		SSL_free (res->ssl);
		free(res);
		return NULL;
	}
	BIO_set_nbio (res->bio, 1);
	return res;
}

void
tls_start (struct tls *tls)
{
	SSL_set_bio (tls->ssl, tls->bio, tls->bio);
}

int
tls_stop (struct tls *tls)
{
	int l = SSL_shutdown (tls->ssl);

	if (l == 1)
		return 0;
	if (l)
	{
		if (SSL_get_error (tls->ssl, l) == SSL_ERROR_WANT_READ)
			return 1;
		if (SSL_get_error (tls->ssl, l) == SSL_ERROR_WANT_WRITE)
			return 2;
		return l;
	}
	return -1;
}

void
tls_free (struct tls *tls)
{
	SSL_free (tls->ssl);
}

int
tls_accept (struct tls *tls)
{
	int l = SSL_accept (tls->ssl);

	if (l == 1)
		return 0;
	if (l)
	{
		if (SSL_get_error (tls->ssl, l) == SSL_ERROR_WANT_READ)
			return 1;
		if (SSL_get_error (tls->ssl, l) == SSL_ERROR_WANT_WRITE)
			return 2;
		return l;
	}
	return -1;
}

int
tls_connect (struct tls *tls)
{
	int l = SSL_connect (tls->ssl);

	if (l == 1)
		return 0;
	if (l)
	{
		if (SSL_get_error (tls->ssl, l) == SSL_ERROR_WANT_READ)
			return 1;
		if (SSL_get_error (tls->ssl, l) == SSL_ERROR_WANT_WRITE)
			return 2;
		return l;
	}
	return -1;
}

ssize_t
tls_read (struct tls *tls, void *buf, size_t maxlen)
{
	return SSL_read (tls->ssl, buf, maxlen);
}

ssize_t
tls_write (struct tls *tls, const void *buf, size_t len)
{
	return SSL_write (tls->ssl, buf, len);
}

ssize_t
tls_write_vecs (struct tls *tls, struct iovec *vecs, int num)
{
	int i, l = 0;
	int res = 0;

	for (i = 0; i < num; i++)
	{
		l = tls_write (tls, vecs[i].iov_base, vecs[i].iov_len);
		if (l < 0)
			break;
		res += l;
	}
	if (res)
		return res;
	return l;
}

tlscert_t
tls_read_cert (const char *file)
{
	FILE *fp = fopen (file, "rb");
	tlscert_t res;

	if (!fp)
		return NULL;

	res = PEM_read_X509 (fp, NULL, NULL, NULL);
	fclose (fp);
	return res;
}

tlscert_t
tls_get_peer_cert (const struct tls *tls)
{
	return SSL_get_peer_certificate (tls->ssl);
}

void
tls_free_cert (tlscert_t cert)
{
	X509_free (cert);
}

int
tls_get_cn (tlscert_t cert, char *buf, size_t buflen)
{
	X509_NAME *subject = X509_get_subject_name (cert);

	return X509_NAME_get_text_by_NID (subject, NID_commonName, buf, buflen - 1);
}

int
tls_compare_certs (const tlscert_t c1, const tlscert_t c2)
{
	return X509_cmp (c1, c2);
}

tlskey_t
tls_read_key (const char *file)
{
	FILE *fp = fopen (file, "rb");
	tlskey_t res;

	if (!fp)
		return NULL;

	res = PEM_read_PrivateKey (fp, NULL, NULL, NULL);
	fclose (fp);
	return res;
}

void
tls_free_key (tlskey_t key)
{
	EVP_PKEY_free (key);
}

const char *
tls_error (const struct tls *tls, int res)
{
	int ec = SSL_get_error (tls->ssl, res);
	const char *err;

	if (ec == SSL_ERROR_SYSCALL) {
		err = ERR_reason_error_string (ERR_get_error ());
		if (!err)
			err = xstrerror (errno);
	} else if (ec != SSL_ERROR_SSL) {
		err = ERR_reason_error_string (ec);
	} else
		err = ERR_reason_error_string (ERR_get_error ());

	return err;
}
