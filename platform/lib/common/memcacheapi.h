#ifndef MEMCACHEAPI_H
#define MEMCACHEAPI_H

struct fd_pool;

char *memcache_get(const char *session_id, const char *key, struct fd_pool *pool);

int memcache_set(const char *session_id, const char *key, const char *value, 
        int expire, struct fd_pool *pool);
int memcache_incr(const char *session_id, const char *key, int value, struct fd_pool *pool);
int memcache_decr(const char *session_id, const char *key, int value, struct fd_pool *pool);

#endif
