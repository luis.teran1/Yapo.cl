#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <signal.h>
#include <wait.h>
#include <errno.h>
#include <string.h>
#include <time.h>
#include <pwd.h>
#include "util.h"
#include "logging.h"
#include <sys/prctl.h>
#include <sys/resource.h>

#include "daemon.h"

char *pidfile;
char *switchuid;
size_t coresize;
int quick_start;
int respawn_delay_min = 2;
int respawn_delay_max = 2;
float respawn_delay_backoff_rate = 1.0f;

int hup = 0;
static void bos_sighup(int signum) {
	hup = 1;
}

int usr1 = 0;
static void bos_sigusr1(int signum) {
	usr1 = 1;
}

int usr2 = 0;
static void bos_sigusr2(int signum) {
	usr2 = 1;
}

int term = 0;
static void bos_sigterm(int signum) {
	term = 1;
}

static void NORETURN
doexit(int code) {
	if (pidfile) {
		int fd = open(pidfile, O_RDONLY);
		char pidbuf[13];

		if (fd >= 0) {
			ssize_t len = read(fd, pidbuf, sizeof(pidbuf) - 1);

			close (fd);

			if (len >= 0) {
				pidbuf[len] = 0;
				if (atoi(pidbuf) == getpid())
					unlink(pidfile);
			}
		}

		free(pidfile);
	}

	exit(code);
}

void set_pidfile(const char *path) {
	if (pidfile)
		free(pidfile);
	pidfile = xstrdup(path);
}

void write_pidfile(void) {
	int fd;
	char pidbuf[13];

	if ((fd = open(pidfile, O_WRONLY | O_CREAT | O_TRUNC, 0644)) < 0)
		xerr(1, "open(pidfile %s)", pidfile);
	sprintf(pidbuf, "%d\n", (int)getpid());
	UNUSED_RESULT(write(fd, pidbuf, strlen(pidbuf)));
	close(fd);
}

void set_switchuid(const char *uid) {
	if (switchuid)
		free(switchuid);
	switchuid = uid ? xstrdup(uid) : NULL;
}

void set_coresize(size_t sz) {
	coresize = sz;
}

void
set_quick_start(int flag) {
	quick_start = flag;
}

void
set_respawn_backoff_attrs(int min_s, int max_s, float rate) {
	respawn_delay_backoff_rate = rate;
	respawn_delay_min = min_s;
	respawn_delay_max = max_s;
}

void
do_switchuid(void) {
	if (switchuid) {
		struct passwd *pw = getpwnam(switchuid);

		if (!pw)
			xerr(1, "getpwnam(%s)", switchuid);
		if (setgid(pw->pw_gid))
			xerr(1, "setgid(%s)", switchuid);
		if (setuid(pw->pw_uid))
			xerr(1, "setuid(%s)", switchuid);
		if (prctl(PR_SET_DUMPABLE, 1, 0, 0, 0) == -1)
			xwarnx("prctl failed");
	}
}

static int
nonresponsive_child_allowed_seconds() {
	const char *s = getenv("NONRESPONSIVE_CHILD_ALLOWED_SECONDS");
	if (!s)
		return 5;

	errno = 0;
	char *end = (char *) s;
	long int result = strtol(s, &end, 10);
	if (errno == ERANGE || end == s)
		return 5;
	return result;
}

void
bos_here(void) {
	pid_t child;
	int status;
	int ret;
	int respawn = 0;
	float respawn_delay = respawn_delay_min;

	while (1) {
		time_t restart_time = time(NULL);
		struct sigaction sa;
		int waiterr;

		switch ((child = fork())) {
		case -1:
			/* Fork failed */
			exit(1);
		case 0:
			/* In child */
			if (pidfile) {
				free(pidfile);
				pidfile = NULL;
			}
			signal(SIGHUP, SIG_DFL);
			signal(SIGUSR1, SIG_DFL);
			signal(SIGUSR2, SIG_DFL);
			signal(SIGINT, SIG_DFL);
			signal(SIGTERM, SIG_DFL);
			signal(SIGPIPE, SIG_IGN);
			if (respawn) {
				log_printf(LOG_CRIT, "BOS restarting main in %d seconds. Attempt #%d", (int)respawn_delay, respawn);
				sleep(respawn_delay);
			} else  {
				xwarnx("(INFO) BOS starting");
			}
			return;
		}

		/* Parent - handle signals */

		memset(&sa, 0, sizeof(sa));
		sigemptyset(&sa.sa_mask);
		sa.sa_flags = 0;
		sa.sa_handler = bos_sighup;
		if (sigaction(SIGHUP, &sa, NULL))
			xerr(1, "sigaction");
		sa.sa_handler = bos_sigusr1;
		if (sigaction(SIGUSR1, &sa, NULL))
			xerr(1, "sigaction");
		sa.sa_handler = bos_sigusr2;
		if (sigaction(SIGUSR2, &sa, NULL))
			xerr(1, "sigaction");
		sa.sa_handler = bos_sigterm;
		if (sigaction(SIGTERM, &sa, NULL))
			xerr(1, "sigaction");
		if (sigaction(SIGINT, &sa, NULL))
			xerr(1, "sigaction");

		/*
		 * Forward SIGHUP, SIGUSR1, SIGUSR2 and SIGTERM to the child.
		 */
		do {
			hup = 0;
			usr1 = 0;
			usr2 = 0;
			term = 0;
			ret = waitpid(child, &status, 0);
			waiterr = errno;
			if (ret == -1 && errno == EINTR)
				log_printf(LOG_INFO, "BOS signalled (%s)", hup ? "hup" : usr1 ? "usr1" : usr2 ? "usr2" : term ? "term" : "unknown");
			if (hup) {
				if (kill(child, SIGHUP))
					log_printf(LOG_WARNING, "kill(%d, SIGHUP): %m", child);
			}
			if (usr1) {
				if (kill(child, SIGUSR1))
					log_printf(LOG_WARNING, "kill(%d, SIGUSR1): %m", child);
			}
			if (usr2) {
				if (kill(child, SIGUSR2))
					log_printf(LOG_WARNING, "kill(%d, SIGUSR2): %m", child);
			}
			if (term) {
				if (kill(child, SIGTERM))
					log_printf(LOG_WARNING, "kill(%d, SIGTERM): %m", child);
			}
		} while (ret == -1 && waiterr == EINTR && term == 0 && (hup || usr1 || usr2));

		if (ret == -1 && waiterr == EINTR && term) {
			int stat = 0;
			int res;
			int done = 0;
			int wait_up_to = nonresponsive_child_allowed_seconds();
			res = kill(child, SIGTERM);

			while (!done) {
				stat++;
				switch ((res = waitpid(child, &status, WNOHANG))) {
				case -1:
					doexit(0);
				case 0:
					if (stat <= wait_up_to) {
						sleep(1);
					} else if (stat == wait_up_to + 1) {
						xwarnx("(INFO) Child nonresponsive, sending SIGINT");
						fprintf(stderr, "Child nonresponsive, sending SIGINT\n");
						kill(child, SIGINT);
						sleep(20);
					} else {
						xwarnx("(WARN) Child nonresponsive, sending SIGKILL");
						fprintf(stderr, "Child nonresponsive, sending SIGKILL\n");
						kill(child, SIGKILL);
						sleep(1);
					}
					break;
				default:
					doexit(0);
				}
			}
		} else {
			if (WIFEXITED(status)) {
				/* Child exited normally */
				xwarnx("(INFO) BOS Child %u exit status: %d", ret, WEXITSTATUS(status));
				if (WEXITSTATUS(status) == 0)
					doexit(0);
			} else if (WIFSIGNALED(status)) {
				/* Child terminated by signal */
				xwarnx("(CRIT) BOS Child %u term signal: %s (%d)", ret, strsignal(WTERMSIG(status)), WTERMSIG(status));
			}

#ifdef WCOREDUMP
			if (WCOREDUMP(status)) {
				/* Child dumped core */
				xwarnx("(INFO) BOS Child %u dumped core", ret);
			}
#endif
		}

		time_t time_client_lifetime = time(NULL) - restart_time - respawn_delay; /* Subtract delay we know was spent sleeping */

		/* If we fail at the first attempt, probably operator near. */
		if (respawn == 0 && time_client_lifetime <= 5) {
			log_printf(LOG_CRIT, "Child died within 5 seconds, shutting down BOS");
			exit(1);
		}

		if ((int)respawn_delay != respawn_delay_min && time_client_lifetime >= 60*5) {
			log_printf(LOG_INFO, "Child lived longer than five minutes, resetting restart delay from %d to %d", (int)respawn_delay, respawn_delay_min);
			respawn_delay = respawn_delay_min;
		}

		if (respawn_delay*respawn_delay_backoff_rate >= respawn_delay_max)
			respawn_delay = respawn_delay_max;
		else
			respawn_delay *= respawn_delay_backoff_rate;

		respawn++;

	}
}

int
bos(int (*func)(void)) {
	bos_here();
	doexit(func());
}

void
daemonify_here(bool nobos) {
	int fd;
	int status;

	switch (status = fork()) {
	case -1:
		xerr(1, "fork");
	case 0:
		break;
	default:
		/*
		 * The main process forks out here.
		 * Sleep a while and check that child is still running.
		 */
		if (pidfile)
			free(pidfile);

		if (!quick_start)
			sleep(5);
		if (waitpid(status, &status, WNOHANG) > 0) {
			if (WIFEXITED(status))
				exit(WEXITSTATUS(status));
			exit(1);
		}
		exit(0);
	}

	if (pidfile) {
		write_pidfile();
	}

	// 'change_user' is used to enable/disable this part.... 
	if (switchuid) {
		do_switchuid();
	}

	if (coresize) {
		struct rlimit rlim;

		if (getrlimit(RLIMIT_CORE, &rlim) == -1)
			xerr(1, "getrlimit()");
		rlim.rlim_cur = coresize;
		if (setrlimit(RLIMIT_CORE, &rlim) == -1)
			xerr(1, "setrlimit()");
	}

	if ((fd = open("/dev/null", O_RDWR, 0)) == -1)
		xerr(1, "open(/dev/null)");

	if (setsid() == -1)
		xerr(1, "setsid");

	dup2(fd, 0);
	dup2(fd, 1);
	dup2(fd, 2);

	if (fd > 2)
		close(fd);

	if (!nobos)
		bos_here();
}

void
daemonify(int nobos, int (*func)(void)) {
	daemonify_here(nobos);
	doexit(func());
}
