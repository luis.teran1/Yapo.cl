#ifndef FD_POOL_H
#define FD_POOL_H

#include "sbalance.h"
#include "macros.h"

struct bpapi_vtree_chain;
struct iovec;
struct timer_instance;

/*
 * Takes one of our common bconf trees
 * .host.1.name=x
 * .host.1.port=x
 * .host.2.name=x
 * .host.2.port=x
 * and sticks the dns lookup results in an sbalance.
 * Then creates a fd pool per node and each call to
 * fd_pool_get() returns a fd you can use.
 * It returns -1 when you've run out of places to connect.
 * peer is an out parameter on the format "host port", always numeric.
 * is_ka is set to 1 if the keepalive port was used.
 * Use fd_pool_put() when done, or close it if it can't be reused.
 */

#define FD_POOL_DEFAULT_TIMEOUT 5000
#define FD_POOL_DEFAULT_FAIL     100
#define FD_POOL_DEFAULT_TEMPFAIL   0

struct fd_pool;
struct fd_pool_conn;
struct addrinfo;

struct fd_pool *fd_pool_create(struct bpapi_vtree_chain *vtree, int use_keepalive, const struct addrinfo *hints) ALLOCATOR;
struct fd_pool *fd_pool_create_single(const char *host, const char *port, int retries, int timeout, const struct addrinfo *hints) ALLOCATOR NONNULL(1, 2);
void fd_pool_free(struct fd_pool *pool);

void fd_pool_clear_node(struct fd_pool *pool) NONNULL(1);
struct bpapi_vtree_chain *fd_pool_node(struct fd_pool *pool) NONNULL(1);
int fd_pool_timeout(struct fd_pool *pool) NONNULL(1);

struct fd_pool_conn *fd_pool_new_conn(struct fd_pool *pool, const char *remote_addr, int async) ALLOCATOR NONNULL(1);
void fd_pool_free_conn(struct fd_pool_conn *conn);

int fd_pool_get(struct fd_pool_conn *conn, enum sbalance_conn_status status, const char **peer, int *is_ka) NONNULL(1);
void fd_pool_put(struct fd_pool_conn *conn, int fd) NONNULL(1);

struct bpapi_vtree_chain *fd_pool_current_node(struct fd_pool_conn *conn) NONNULL(1);
struct fd_pool *fd_pool_conn_pool(struct fd_pool_conn *conn) NONNULL(1);

void fd_pool_set_async(struct fd_pool_conn *conn, int async) NONNULL(1);
void fd_pool_set_timer(struct fd_pool_conn *conn, struct timer_instance *ti);
void fd_pool_set_node_key(struct fd_pool_conn *conn, const char *key);

/*
	Retrying writev() with fd_pool connection fallback.

	Will write the complete iovecs to the fd passed in, with fallback to the
	connection pool if a write fails, or the passed fd was -1.

	On success, 0 is returned and fd contains a working fd.
	On error, a negative value is returned and fd is -1.

*/
int fd_pool_conn_writev(struct fd_pool_conn *conn, int *fd, struct iovec *iov, int iovcnt, ssize_t iovlen_sum) NONNULL(1,2,3);

#endif /*FD_POOL_H*/
