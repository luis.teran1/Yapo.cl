#include <stdlib.h>
#include <string.h>

#include "bconfd_client.h"

#include "memalloc_functions.h"
#include "string_functions.h"
#include "buf_string.h"
#include "bconf.h"
#include "json_vtree.h"
#include "transapi.h"
#include "vtree.h"
#include "bconfig.h"
#include "queue.h"

#define CHECKSUM_HEADER_NAME "X-Scm-Bconf-Checksum"

#define CONFIG_INIT_ERROR	"Error calling config_init"
#define BCONFD_INVALID_PROTO	"Invalid protocol"
#define TRANS_INIT_VTREE_FAILED	"Error calling trans_init_vtree"
#define REQUEST_ERROR		"Request error"
#define JSON_BCONF_FAILED	"Error calling json_bconf"
#define JSON_HOST_MISSING	"Missing json_host in configuration"

struct req_param {
	SLIST_ENTRY(req_param) entry;
	char *key;
	char *value;
};

struct is {
	SLIST_HEAD(, req_param) pairs;
};

static size_t
curl_wr(char *d, size_t s, size_t n, void *v) {
	struct buf_string *b = v;

	return bswrite(b, d, s * n);
}

static size_t
curl_checksum_wr(char *d, size_t s, size_t n, void *v) {
	char *checksum = v;
	char *c;
	size_t hl = strlen(CHECKSUM_HEADER_NAME);

	if (d[n] != '\0')
		d[n] = '\0';

	if (!strncasecmp(d, CHECKSUM_HEADER_NAME, hl) && (*(d + hl) == ':') && (hl + 1 + 1 < n)) {
		c = d + hl + 1;
		trim(c);

		if (strlen(c) + 1 == CHECKSUM_HEADER_LENGTH)
			memcpy(checksum, c, CHECKSUM_HEADER_LENGTH);
	}
	return s * n;
}

static void
trans_checksum_wr(transaction_t *trans, const char *key, size_t klen, const char *val, size_t vlen, void *cbdata) {
	char *checksum = cbdata;

	if (vlen + 1 == CHECKSUM_HEADER_LENGTH) {
		memcpy(checksum, val, vlen);
		checksum[vlen] = '\0';
	}
}

static void
add_params_to_trans(void *data, const char *key, const char *value) {
	transaction_t *trans = data;
	trans_add_pair(trans, key, value, -1);
}

static void
add_params_to_json(void *data, const char *key, const char *value) {
	struct buf_string *buf = data;
	bscat(buf, "%s%s=%s", buf->buf ? "&" : "", key, value);
}

static void
flush_params_to_req(struct bconfd_call *call, void (pf)(void *data, const char *key, const char *value), void *data) {
	struct is *state = call->is;

	if (!state)
		return;

	while (!SLIST_EMPTY(&state->pairs)) {
		struct req_param *r = SLIST_FIRST(&state->pairs);
		if (pf)
			pf(data, r->key, r->value);

		SLIST_REMOVE_HEAD(&state->pairs, entry);
		free(r->key);
		free(r->value);
		free(r);
	}
	free(state);
	call->is = NULL;
}

static int
bconfd_load_json(struct bconfd_call *call, struct bconf_node *root) {
	struct buf_string response = { 0 };
	struct buf_string req = { 0 };
	struct bconf_node *json_host = bconf_get(root, "json_host");
	struct bconf_node *n;
	long status = 0;
	char *full_url;
	CURLcode r = 0;
	CURL *c;

	if (!json_host) {
		call->errstr = JSON_HOST_MISSING;
		flush_params_to_req(call, NULL, NULL);
		return 1;
	}

	c = curl_easy_init();
	if (!c) {
		call->errstr = curl_easy_strerror(CURLE_FAILED_INIT);
		flush_params_to_req(call, NULL, NULL);
		return 1;
	}

	curl_easy_setopt(c, CURLOPT_NOSIGNAL, 1L);
	curl_easy_setopt(c, CURLOPT_PROTOCOLS, CURLPROTO_HTTP | CURLPROTO_HTTPS);

#ifdef CURLOPT_CONNECTTIMEOUT_MS
	curl_easy_setopt(c, CURLOPT_CONNECTTIMEOUT_MS, 1000L);  /* 1000ms should be enough to connect to anybody. */
#else
	curl_easy_setopt(c, CURLOPT_CONNECTTIMEOUT, 1L);
#endif

	curl_easy_setopt(c, CURLOPT_WRITEFUNCTION, curl_wr);
	curl_easy_setopt(c, CURLOPT_WRITEDATA, &response);

	curl_easy_setopt(c, CURLOPT_HEADERFUNCTION, curl_checksum_wr);
	curl_easy_setopt(c, CURLOPT_HEADERDATA, &call->checksum);

	flush_params_to_req(call, add_params_to_json, &req);

	for (int i = 0; (n = bconf_byindex(json_host, i)) != NULL; i++) {
		xasprintf(&full_url, "%s:%s/%s%s%s",
			bconf_get_string(n, "name"),
			bconf_get_string(n, "port"),
			call->cmd,
			req.buf ? "?" : "",
			req.buf ?: ""
		);

		curl_easy_setopt(c, CURLOPT_URL, full_url);

		r = curl_easy_perform(c);

		free(full_url);
		full_url = NULL;

		if (!r) {
			curl_easy_getinfo(c, CURLINFO_RESPONSE_CODE, &status);
			break;
		}
	}
	curl_easy_cleanup(c);
	free(req.buf);

	if (r) {
		call->errstr = curl_easy_strerror(r);
	} else if (status != 200) {
		call->errstr = REQUEST_ERROR;
	} else if (response.buf && json_bconf(&call->bconf, NULL, response.buf, response.pos, 0)) {
		call->errstr = JSON_BCONF_FAILED;
		status = 500;
	}
	free(response.buf);

	return (r || status != 200) ? 1 : 0;
}

static int
bconfd_load_trans(struct bconfd_call *call, struct bconf_node *root) {
	transaction_t *trans;
	int res = 0;
	char *key;
	char *value;
	char *ptr;
	int timeout = 1000;
	struct bpapi_vtree_chain vtree;

	timeout = bconf_get_int(root, "timeout") * 1000;
	trans = trans_init_vtree(timeout > 0 ? timeout : 1000, bconf_vtree(&vtree, root));
	if (!trans) {
		/* flush added params if any */
		flush_params_to_req(call, NULL, NULL);
		call->errstr = TRANS_INIT_VTREE_FAILED;
		return -1;
	}

	trans_add_pair(trans, "cmd", call->cmd, -1);
	trans_add_pair(trans, "commit", "1", -1);
	trans_add_callback(trans, CHECKSUM_HEADER_NAME, trans_checksum_wr);
	trans_set_cbdata(trans, &call->checksum);
	flush_params_to_req(call, add_params_to_trans, trans);

	res = trans_commit(trans);
	vtree_free(&vtree);

	if (res) {
		trans_free(trans);
		call->errstr = trans_strerror(res);
		return res;
	}

	char *status = trans_get_key(trans, "status");
	if (!status || strcmp(status, "TRANS_OK") != 0) {
		call->errstr = "TRANS_ERROR";

		trans_free(trans);
		free(status);

		return -1;
	}
	free(status);

	while (trans_iter_next(trans, &key, &value)) {
		ptr = strchr(value, '=');
		if (ptr) {
			*ptr++ = '\0';
			bconf_add_data(&call->bconf, value, ptr);
		}
	}
	trans_free(trans);

	return 0;
}

int
bconfd_add_request_param(struct bconfd_call *call, const char *key, const char *value) {
	if (key == NULL || value == NULL)
		return 0;

	struct is **state = (struct is **)&call->is;
	struct req_param *r = zmalloc(sizeof(*r));

	if (*state == NULL) {
		*state = zmalloc(sizeof(**state));
		SLIST_INIT(&(*state)->pairs);
	}

	r->key = xstrdup(key);
	r->value = xstrdup(value);
	SLIST_INSERT_HEAD(&(*state)->pairs, r, entry);

	return 0;
}

int
bconfd_load(struct bconfd_call *call, struct bconf_node *root) {
	if (!call->cmd)
		call->cmd = "bconf";

	switch (call->proto) {
	case PROTO_TRANS:
		return bconfd_load_trans(call, root);
	case PROTO_JSON:
		return bconfd_load_json(call, root);
	default:
		call->errstr = BCONFD_INVALID_PROTO;
		return 1;
	}
}

int
bconfd_load_with_file(struct bconfd_call *call, const char *config_file) {
	struct bconf_node *root = config_init(config_file);

	if (!root) {
		call->errstr = CONFIG_INIT_ERROR;
		return 1;
	}
	int res = bconfd_load(call, root);

	bconf_free(&root);
	return res;
}

