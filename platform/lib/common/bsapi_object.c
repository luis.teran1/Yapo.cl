
#include "bsapi_object.h"

#include "error_functions.h"
#include "memalloc_functions.h"

/* TODO: Read the data lazily from server */

struct bsapi_object {
	struct bsapi_object_info **infos;
	int num_infos;
	int alloced_infos;
	int infoidx;

	char **infolist;
	const char **valuelists;
	int col;
	int cols;
	int row;
	int arows;
	struct bsearch_api bs;

	struct bsapi_object_keyval kv;
};

static void
search_add_data(int state, int info, const char *key, const char **value, void *data) {
	struct bsapi_object *bo = data;
	int i;

	/* Note: all values are just kept as pointers into the bsapi buffer. */

	switch (state) {
	case BS_INFO:
		if (!bo->infos) {
			bo->alloced_infos = 4;
			bo->infos = xmalloc(bo->alloced_infos * sizeof (*bo->infos));
		} else if (bo->num_infos == bo->alloced_infos) {
			bo->alloced_infos *= 2;
			bo->infos = xrealloc(bo->infos, bo->alloced_infos * sizeof (*bo->infos));
		}
		{
			int nvalues;

			for (nvalues = 0 ; value[nvalues] != NULL ; nvalues++)
				;

			struct bsapi_object_info *info = bo->infos[bo->num_infos++] = xmalloc(sizeof (*info) + sizeof(*info->value) * nvalues);
			info->key = key;
			info->nvals = nvalues;

			for (i = 0 ; i < nvalues ; i++)
				info->value[i] = value[i];
		}
		if (strcmp(key, "lines") == 0) {
			bo->arows = atoi(*value) + 1;
		}
		break;
	case BS_VALUE:
		if (info != bo->row) {
			bo->row = info;
			bo->col = 0;
		}
		if (bo->row == 0)
			bo->valuelists[bo->col * bo->arows] = key;
		bo->valuelists[bo->col * bo->arows + 1 + bo->row] = *value;
		bo->col++;
		break;
	case BS_COLS:
		bo->cols = info;
		if (!bo->arows)
			xerrx(1, "no info:lines row from search!?");
		bo->valuelists = xmalloc(bo->cols * sizeof (*bo->valuelists) * bo->arows);
		break;
	case BS_DONE:
	case BS_NOAD:
		bo->infoidx = -1;
		bo->row = -1;
		bo->col = -1;
		break;
		break;
	default:
		break;
	}


}

struct bsapi_object *
bsapi_object_new(struct bsconf *bsconf, const char *remote_addr) {
	struct bsapi_object *bo = zmalloc(sizeof (*bo));

	if (bsearch_init_bsconf(&bo->bs, bsconf, remote_addr))
		goto fail;

	return bo;

fail:
	free(bo);
	return NULL;
}

struct bsapi_object *
bsapi_object_new_vtree(struct bpapi_vtree_chain *vt, const char *remote_addr) {
	struct bsapi_object *bo = zmalloc(sizeof (*bo));

	if (bsearch_init_vtree(&bo->bs, vt, remote_addr))
		goto fail;

	return bo;

fail:
	free(bo);
	return NULL;
}

void
bsapi_object_free(struct bsapi_object *bo) {
	bsearch_cleanup(&bo->bs);
	free(bo->valuelists);
	while (--bo->num_infos >= 0)
		free(bo->infos[bo->num_infos]);
	free(bo->infos);
	free(bo);
}

int
bsapi_object_search(struct bsapi_object *bo, const char *query) {
	return bsearch_search(&bo->bs, query, search_add_data, bo);
}

const struct bsapi_object_info *
bsapi_object_get_info(struct bsapi_object *bo, const char *key, int idx) {
	int i;

	for (i = 0 ; i < bo->num_infos ; i++) {
		if (strcmp(key, bo->infos[i]->key) == 0 && !idx--)
			return bo->infos[i];
	}
	return NULL;
}

const struct bsapi_object_info *
bsapi_object_info_idx(struct bsapi_object *bo, int idx) {
	if (idx < 0 || idx >= bo->num_infos)
		return NULL;

	return bo->infos[idx];
}

const struct bsapi_object_info *
bsapi_object_next_info(struct bsapi_object *bo) {
	if (++bo->infoidx >= bo->num_infos)
		return NULL;

	return bo->infos[bo->infoidx];
}

const char *
bsapi_object_column_name(struct bsapi_object *bo, int idx) {
	if (idx >= 0 && idx < bo->cols)
		return bo->valuelists[idx * bo->arows];
	return NULL;
}

int
bsapi_object_num_infos(struct bsapi_object *bo) {
	return bo->num_infos;
}

int
bsapi_object_num_cols(struct bsapi_object *bo) {
	return bo->cols;
}

int
bsapi_object_num_rows(struct bsapi_object *bo) {
	return bo->arows - 1;
}

bool
bsapi_object_next_row(struct bsapi_object *bo) {
	if (++bo->row >= bo->arows - 1)
		return false;

	bo->col = -1;
	return true;
}

bool
bsapi_object_set_row(struct bsapi_object *bo, int row) {
	if (row < 0 || row >= bo->arows - 1)
		return false;

	bo->row = row;
	bo->col = -1;
	return true;
}

const struct bsapi_object_keyval *
bsapi_object_next_column(struct bsapi_object *bo) {
	if (++bo->col >= bo->cols)
		return NULL;
	bo->kv.key = bo->valuelists[bo->col * bo->arows];
	bo->kv.value = bo->valuelists[bo->col * bo->arows + 1 + bo->row];
	return &bo->kv;
}

const struct bsapi_object_keyval *
bsapi_object_column(struct bsapi_object *bo, int idx) {
	if (idx < 0 || idx > bo->cols)
		return NULL;
	bo->kv.key = bo->valuelists[idx * bo->arows];
	bo->kv.value = bo->valuelists[idx * bo->arows + 1 + bo->row];
	return &bo->kv;
}


const char *
bsapi_object_column_value(struct bsapi_object *bo, const char *column) {
	int i;

	for (i = 0 ; i < bo->cols ; i++) {
		if (strcmp(bo->valuelists[i * bo->arows], column) == 0)
			return bo->valuelists[i * bo->arows + 1 + bo->row];
	}
	return NULL;
}

const char **
bsapi_object_all_values(struct bsapi_object *bo, int idx) {
	if (idx < 0 || idx >= bo->cols)
		return NULL;
	return &bo->valuelists[idx * bo->arows + 1];
}

const char **
bsapi_object_column_all_values(struct bsapi_object *bo, const char *column) {
	int i;

	for (i = 0 ; i < bo->cols ; i++) {
		if (strcmp(bo->valuelists[i * bo->arows], column) == 0)
			return &bo->valuelists[i * bo->arows + 1];
	}
	return NULL;
}

