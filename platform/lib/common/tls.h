#ifndef COMMON_TLS_H
#define COMMON_TLS_H

#include <openssl/ssl.h>
#include <sys/uio.h>
#include "macros.h"

struct tls
{
	SSL *ssl;
	BIO *bio;
};

/*
 * You are not supposed to free the context, keep it in a global variable.
 */
struct tls_context {
	/* In params */
	const char *ssl_certs_path;
	SSL_METHOD *method; /* defaults to SSLv23_method() */

	/* Private params, 0 initialized */
	SSL_CTX *ssl_ctx;
	int context_id_set;
	unsigned char context_id[32];
};

/* In case we ever want to switch to some other engine. Unlikely. */
typedef X509 *tlscert_t;
typedef EVP_PKEY *tlskey_t;

enum
{
	tlsVerifyPeer = 1 << 0
};

const char *tls_get_cert_dir (void);

/* Initialize TLS for communicating on an fd. The fd is not considered TLS enabled until you call tls_start, however. */
struct tls *tls_open (struct tls_context *ctx, int fd, int options, tlscert_t cert, tlskey_t key) ALLOCATOR;
/* Free the TLS structure. Be sure to call tls_stop first if you plan to continue using the fd. */
void tls_free (struct tls *tls);

/* Indicate that communication will now be done through TLS. */
void tls_start (struct tls *tls);

/*
 * Shutdown TLS on the given fd. You can communicate in clear text after the call is done.
 * Return values:
 *       0: call done.
 *       1: wait for data available for read on fd and call again.
 *       2: wait for fd available for write and call again.
 *   other: error.
 */
int tls_stop (struct tls *tls);

/* Negotiate as a TLS server. Called after tls_start. Same return values as tls_close. */
int tls_accept (struct tls *tls);
/* Negotiate as a TLS client. Called after tls_start. Same return values as tls_close. */
int tls_connect (struct tls *tls);

/* Read and write, same symantics as the syscalls. */
ssize_t tls_read (struct tls *tls, void *buf, size_t maxlen);
ssize_t tls_write (struct tls *tls, const void *buf, size_t len);
ssize_t tls_write_vecs (struct tls *tls, struct iovec *vecs, int num);

/* Certificate handling. Always free the certs returned. */
tlscert_t tls_read_cert (const char *file) ALLOCATOR;
tlscert_t tls_get_peer_cert (const struct tls *tls) ALLOCATOR;
void tls_free_cert (tlscert_t cert);

/* Certificate utility funcs. */
int tls_get_cn (tlscert_t cert, char *buf, size_t buflen);
int tls_compare_certs (const tlscert_t c1, const tlscert_t c2);

/* Key handling. */
tlskey_t tls_read_key (const char *file);
void tls_free_key (tlskey_t key);

const char *tls_error (const struct tls *tls, int res);

#endif /*COMMON_TLS_H*/
