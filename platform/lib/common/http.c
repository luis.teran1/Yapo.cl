#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "http.h"
#include "memalloc_functions.h"

static const char **
merge_headers(const char *a[], const char *b[]) {
	int a_len = 0;
	int b_len = 0;
	const char **headers;

	while (a && a[a_len++]);
	while (b && b[b_len++]);

	headers = xmalloc(sizeof(*headers) * (a_len + b_len + 1));

	for (int i = 0; i < a_len; i++)
		headers[i] = a[i];
	for (int i = 0; i < b_len; i++)
		headers[i + a_len] = b[i];

	headers[a_len + b_len] = NULL;

	return headers;
}

static size_t
null_write_data(void *buffer, size_t size, size_t nmemb, void *cb_data) {
	return size * nmemb;
}

static size_t
default_write_data(void *buffer, size_t size, size_t nmemb, void *cb_data) {
	struct buf_string *bs = (struct buf_string *) cb_data;
	size_t rs = size * nmemb;

	bswrite(bs, buffer, rs);

	return rs;
}

struct http *
http_create() {
	struct http *h;

	h = zmalloc(sizeof(struct http));
	h->write_function = null_write_data;
	h->header_write_function = null_write_data;

	return h;
}

void
http_free(struct http *h) {
	curl_easy_cleanup(h->ch);
	free(h);
}

long
http_perform(struct http *h) {
	long response_code;
	struct curl_slist *header_list = NULL;

	if (h->response_body && (h->write_function == NULL || h->write_function == null_write_data)) 
		h->write_function = default_write_data;

	if (h->response_header && (h->header_write_function == NULL || h->header_write_function == null_write_data)) 
		h->header_write_function = default_write_data;
	
	h->curl_status = CURLE_OK;
	
	if ((h->ch = curl_easy_init()) == NULL) {
		return -1;
	}

	curl_easy_setopt(h->ch, CURLOPT_URL, h->url);
	curl_easy_setopt(h->ch, CURLOPT_CUSTOMREQUEST, h->method);
	
	curl_easy_setopt(h->ch, CURLOPT_PROTOCOLS, CURLPROTO_HTTP | CURLPROTO_HTTPS);
	curl_easy_setopt(h->ch, CURLOPT_FOLLOWLOCATION, 1);
	curl_easy_setopt(h->ch, CURLOPT_REDIR_PROTOCOLS, CURLPROTO_HTTP | CURLPROTO_HTTPS);

	curl_easy_setopt(h->ch, CURLOPT_WRITEFUNCTION, h->write_function);
	curl_easy_setopt(h->ch, CURLOPT_WRITEDATA, h->response_body);

	curl_easy_setopt(h->ch, CURLOPT_HEADERFUNCTION, h->header_write_function);
	curl_easy_setopt(h->ch, CURLOPT_HEADERDATA, h->response_header);

	curl_easy_setopt(h->ch, CURLOPT_ERRORBUFFER, h->error);

	curl_easy_setopt(h->ch, CURLOPT_NOSIGNAL, 1);

	if (h->body) {
		curl_easy_setopt(h->ch, CURLOPT_POSTFIELDS, (const char *) h->body);
		curl_easy_setopt(h->ch, CURLOPT_POSTFIELDSIZE_LARGE, (curl_off_t)h->body_length);
	}

	if (h->headers) {
		while (*(h->headers)) {
			header_list = curl_slist_append(header_list, *h->headers++);
		}
		curl_easy_setopt(h->ch, CURLOPT_HTTPHEADER, header_list);
	}

	if (h->extra_config_function && h->extra_config_function(h->ch, h->extra_config_data) != 0) {
		response_code = -1;
		goto out;
	}

	if ((h->curl_status = curl_easy_perform(h->ch)) != CURLE_OK) {
		response_code = -1;
		goto out;
	}
	
	curl_easy_getinfo(h->ch, CURLINFO_RESPONSE_CODE, &response_code);
		
out:
	curl_slist_free_all(header_list);
	return response_code;
}

long
http_get(const char *url, struct buf_string *response, const char *headers[]) {
	struct http *h = http_create();
	long rc;
	
	h->method = "GET";
	h->url = url;
	h->headers = headers;

	if (response) {
		memset(response, 0, sizeof(*response));
		h->response_body = response;
		h->write_function = default_write_data;
	}

	rc = http_perform(h);

	http_free(h);
	return rc;
}

long
http_post(const char *url, struct buf_string *response, const char *poststr, const char *headers[]) {
	struct http *h = http_create();
	long rc;
	
	h->method = "POST";
	h->url = url;
	h->headers = headers;

	if (response) {
		memset(response, 0, sizeof(*response));
		h->response_body = response;
		h->write_function = default_write_data;
	}

	h->body = poststr;
	h->body_length = strlen(poststr);

	rc = http_perform(h);

	http_free(h);
	return rc;
}

long
http_delete(const char *url, const char *headers[]) {
	struct http *h = http_create();
	long rc;
	
	h->method = "DELETE";
	h->url = url;
	h->headers = headers;

	rc = http_perform(h);

	http_free(h);
	return rc;
}

long
http_put_str(const char *url, const char *data, const char *headers[]) {
	struct http *h = http_create();
	long rc;
	
	h->method = "PUT";
	h->url = url;
	h->headers = headers;

	h->body = data;
	h->body_length = strlen(data);

	rc = http_perform(h);

	http_free(h);
	return rc;
}

long
http_put_bin(const char *url, void *data, size_t len, const char *headers[]) {
	struct http *h = http_create();
	long rc;
	
	h->method = "PUT";
	h->url = url;
	h->headers = headers;

	h->body = data;
	h->body_length = len;

	rc = http_perform(h);

	http_free(h);
	return rc;
}

long
http_move(const char *url, const char *dest, const char *headers[]) {
	char *dest_hdr;
	const char **hdrs;
	long rc;
	struct http *h = http_create();

	xasprintf(&dest_hdr, "Destination: %s", dest);
	hdrs = merge_headers(headers, (const char *[]){dest_hdr, NULL});

	h->method = "MOVE";
	h->url = url;
	h->headers = hdrs;

	rc = http_perform(h);

	http_free(h);
	free(hdrs);
	free(dest_hdr);
	return rc;
}

long
http_copy(const char *url, const char *dest, const char *headers[]) {
	char *dest_hdr;
	const char **hdrs;
	long rc;
	struct http *h = http_create();

	xasprintf(&dest_hdr, "Destination: %s", dest);
	hdrs = merge_headers(headers, (const char *[]){dest_hdr, NULL});

	h->method = "COPY";
	h->url = url;
	h->headers = hdrs;

	rc = http_perform(h);

	http_free(h);
	free(hdrs);
	free(dest_hdr);
	return rc;
}
