#include <unistd.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <netdb.h>
#include <ctype.h>

#include "queue.h"
#include "util.h"
#include "bconf.h"
#include "logging.h"
#include "sredisapi.h"
#include "vtree.h"
#include "fd_pool.h"
#include "strl.h"
#include "hiredis.h"
#if HIREDIS_MAJOR == 0 && HIREDIS_MINOR < 12
#include "hiredisextra.h"
#endif

static int redis_status_reply(redisReply *reply);

redisReply *
redis_conn_send(struct fd_pool_conn *conn, int argc, const char **argv, const size_t *argvlen) {
	int sockfd = -1;
	redisContext *ctx = NULL;
	redisReply *reply;
	const char *dbnum = NULL;
	struct bpapi_vtree_chain *pool_node = fd_pool_node(fd_pool_conn_pool(conn));

	if (pool_node != NULL)
		dbnum = vtree_get(pool_node, "db", NULL);

	while (1) {
		enum sbalance_conn_status status;

		if (sockfd >= 0) {
			close(sockfd);
			status = SBCS_FAIL;
		} else {
			status = SBCS_START;
		}

		sockfd = fd_pool_get(conn, status, NULL, NULL);

		if (sockfd == -1) {
			log_printf(LOG_WARNING, "sredisapi: -1 from fd_pool_get");
			return NULL;
		}

		ctx = redisConnectFd(sockfd);

		if (dbnum) {
			reply = redisCommandArgv(ctx, 2, (const char *[]){ "SELECT", dbnum }, NULL);
			if (redis_status_reply(reply) != 0) {
				sockfd = redisFreeKeepFd(ctx);
				continue;
			}
		}

		reply = redisCommandArgv(ctx, argc, argv, argvlen);

		if (reply)
			break;

		sockfd = redisFreeKeepFd(ctx);
	}

	/*
	 * The reply is currently decoupled from the context, but the docs are unclear if that
	 * is official so will have to be checked after each upgrade.
	 */
	sockfd = redisFreeKeepFd(ctx);
	fd_pool_put(conn, sockfd);

	return reply;
}

static redisReply *
session_redis_send(const char *key, struct fd_pool *pool, int argc, const char **argv, size_t *argvlen) {
	struct bpapi_vtree_chain *node = fd_pool_node(pool);
	const char *sep = vtree_get(node, "id_separator", NULL);
	char *s = NULL;
	const char *rack_name;
	struct fd_pool_conn *conn = fd_pool_new_conn(pool, NULL, 0);
	struct redisReply *retval;

	if(key && sep) {
		s = strstr(key, sep);
	}

	if(s)
		rack_name = strndupa(key, s - key);
	else
		rack_name = vtree_get(node, "master", NULL);

	fd_pool_set_node_key(conn, rack_name);

	retval = redis_conn_send(conn, argc, argv, argvlen);

	fd_pool_free_conn(conn);

	return retval;
}

static char *
redis_string_reply(redisReply *reply) {
	char *val = NULL;

	if (!reply)
		return NULL;

	switch (reply->type) {
	case REDIS_REPLY_ERROR:
	case REDIS_REPLY_ARRAY:
	case REDIS_REPLY_NIL:
		break;
	case REDIS_REPLY_STATUS:
	case REDIS_REPLY_STRING:
		val = xstrdup(reply->str);
		break;
	case REDIS_REPLY_INTEGER:
		xasprintf(&val, "%lld", reply->integer);
		break;
	}

	freeReplyObject(reply);
	return val;
}

static int
redis_status_reply(redisReply *reply) {
	int val;
	/* XXX Just return 0 or -1 for now. Probably should be possible to extract the error/status somehow. */

	if (!reply)
		return -1;

	if (reply->type == REDIS_REPLY_STATUS)
		val = 0;
	else
		val = -1;

	freeReplyObject(reply);
	return val;
}

static long long
redis_int_reply(redisReply *reply) {
	long long val = 0;

	if (!reply)
		return 0; /* XXX not useful */

	switch (reply->type) {
	case REDIS_REPLY_ERROR:
		val = -1;
		break;
	case REDIS_REPLY_ARRAY:
	case REDIS_REPLY_NIL:
		break;
	case REDIS_REPLY_STATUS:
	case REDIS_REPLY_STRING:
		val = atoll(reply->str);
		break;
	case REDIS_REPLY_INTEGER:
		val = reply->integer;
		break;
	}

	freeReplyObject(reply);
	return val;
}

char *
session_redis_get(const char *session_id, const char *key, struct fd_pool *pool) {
	redisReply *reply;
	char *redis_key;
	struct bpapi_vtree_chain *node = fd_pool_node(pool);
	int argc;
	const char *argv[2];

	if(key == NULL)
		return NULL;

	if (session_id == NULL) {
		xasprintf(&redis_key, "%s%s_%s", vtree_get(node, "master", NULL), vtree_get(node, "id_separator", NULL), key);
	} else {
		xasprintf(&redis_key, "%s_%s", session_id, key);
	}
	
	log_printf(LOG_DEBUG, "redis_get: key: %s, session_id: %s", redis_key, session_id);

	argc = 2;
	argv[0] = "GET";
	argv[1] = redis_key;
	reply = session_redis_send(session_id, pool, argc, argv, NULL);

	if (!reply || reply->type == REDIS_REPLY_ERROR)
		log_printf(LOG_INFO, "redis_get: Failed for key: %s", redis_key);
	free(redis_key);

	return redis_string_reply(reply);
}

int
session_redis_set(const char *session_id, const char *key, const char *value, int expire, struct fd_pool *pool) {
	char *redis_key;
	int res = 0;
	struct bpapi_vtree_chain *node = fd_pool_node(pool);
	int argc;
	const char *argv[4];

	if(key == NULL)
		return res;

	if (session_id == NULL) {
		xasprintf(&redis_key, "%s%s_%s", vtree_get(node, "master", NULL), vtree_get(node, "id_separator", NULL), key);
	} else {
		xasprintf(&redis_key, "%s_%s", session_id, key);
	}

	log_printf(LOG_DEBUG, "redis_set: key: %s, value: %s, expire: %d, session_id: %s", redis_key, value, expire, session_id);

	if (value != NULL && *value != '\0') {
		if(expire > -1) {
			argc = 4;
			argv[0] = "SETEX";
			argv[1] = redis_key;
			ALLOCA_PRINTF(res, argv[2], "%d", expire);
			argv[3] = value;
		}
		else {
			argc = 3;
			argv[0] = "SET";
			argv[1] = redis_key;
			argv[2] = value;
		}
	} else {
		argc = 2;
		argv[0] = "DEL";
		argv[1] = redis_key;
	}
	redisReply *reply = session_redis_send(redis_key, pool, argc, argv, NULL);

	if (!reply || reply->type != REDIS_REPLY_STATUS)
		log_printf(LOG_WARNING, "redis_set: Failed for key: %s with value: \"%s\", expire: %d, Error: \"%s\"", redis_key, value, expire, (reply && reply->type == REDIS_REPLY_ERROR) ? reply->str : NULL);

	free(redis_key);

	return redis_status_reply(reply);
}

/**
 * Notice, redis function INCR will reset keys with TTL
 * set a 100
 * expire a 360
 * incr a
 * => 1
 * http://code.google.com/p/redis/wiki/ExpireCommand
 */
int
session_redis_incr(const char *session_id, const char *key, struct fd_pool *pool) {
	char *redis_key;
	int argc;
	const char *argv[2];

	if (session_id == NULL) {
		xasprintf(&redis_key, "%s", key);
	} else {
		xasprintf(&redis_key, "%s_%s", session_id, key);
	}

	argc = 2;
	argv[0] = "INCR";
	argv[1] = redis_key;
	redisReply *reply = session_redis_send(session_id, pool, argc, argv, NULL);

	if (!reply || reply->type != REDIS_REPLY_INTEGER)
		log_printf(LOG_WARNING, "redis_incr: Failed for key: %s", redis_key);

	free(redis_key);

	return redis_int_reply(reply);
}


/**
 * Socket functions 
 */

struct fd_pool_conn *
redis_sock_conn(struct fd_pool *pool, const char *host) {
	struct fd_pool_conn *conn = fd_pool_new_conn(pool, NULL, 0);
	
	if (host) {
		struct bpapi_vtree_chain *vtree = fd_pool_node(pool);

		if (strcmp(host, "master") == 0)
			host = vtree_get(vtree, "master", NULL);
		else if (strcmp(host, "slave") == 0) {
			host = vtree_get(vtree, "slave", NULL);
			if (!host)
				host = vtree_get(vtree, "master", NULL);
		}
		if (host)
			fd_pool_set_node_key(conn, host);
	}

	return conn;
}

int 
redis_sock_zadd(struct fd_pool_conn *conn, const char *key, int score, const char *member) {
	int argc;
	const char *argv[4];
	int res;

	if (key == NULL || member == NULL) {
		return -1;
	}

	argc = 4;
	argv[0] = "ZADD";
	argv[1] = key;
	ALLOCA_PRINTF(res, argv[2], "%d", score);
	argv[3] = member;

	redisReply *reply = redis_conn_send(conn, argc, argv, NULL);

	if (!reply) {
		log_printf(LOG_WARNING, "No connection to redis: ZADD %s %s", key, member);
		return -1;
	}

	if (reply->type != REDIS_REPLY_INTEGER)
		log_printf(LOG_WARNING, "redis_zadd: Failed for key: %s with member: \"%s\", score: %d, Redis response: \"%s\"", key, member, score, reply->type == REDIS_REPLY_ERROR ? reply->str : NULL);

	return redis_int_reply(reply);
}

int
redis_sock_zrem(struct fd_pool_conn *conn, const char *key, const char *member) {
	int argc;
	const char *argv[3];

	if (key == NULL || member == NULL) {
		return -1;
	}

	argc = 3;
	argv[0] = "ZREM";
	argv[1] = key;
	argv[2] = member;

	redisReply *reply = redis_conn_send(conn, argc, argv, NULL);

	if (!reply) {
		log_printf(LOG_WARNING, "No connection to redis: ZREM %s %s", key, member);
		return -1;
	}

	if (reply->type != REDIS_REPLY_INTEGER)
		log_printf(LOG_WARNING, "redis_zrem: Failed for key: %s with member: \"%s\" Resp: \"%s\"", key, member, reply->type == REDIS_REPLY_ERROR ? reply->str : NULL);

	return redis_int_reply(reply);
}

int
redis_sock_zrank(struct fd_pool_conn *conn, const char *key, const char *member) {
	int argc;
	const char *argv[3];

	if (key == NULL || member == NULL) {
		return -1;
	}

	argc = 3;
	argv[0] = "ZRANK";
	argv[1] = key;
	argv[2] = member;

	redisReply *reply = redis_conn_send(conn, argc, argv, NULL);

	if (!reply) {
		log_printf(LOG_WARNING, "No connection to redis: ZRANK %s %s", key, member);
		return -1;
	}
	
	if (reply->type != REDIS_REPLY_STRING && reply->type != REDIS_REPLY_INTEGER && reply->type != REDIS_REPLY_NIL)
		log_printf(LOG_WARNING, "redis_zrank: Failed for key: %s with member: \"%s\" Resp: \"%s\"", key, member, reply->type == REDIS_REPLY_ERROR ? reply->str : NULL);

	if (reply->type == REDIS_REPLY_INTEGER)
		return redis_int_reply(reply);
	else
		return -1;
}

int
redis_sock_zscore(struct fd_pool_conn *conn, const char *key, const char *member) {
	int argc;
	const char *argv[3];

	if (key == NULL || member == NULL) {
		return -1;
	}

	argc = 3;
	argv[0] = "ZSCORE";
	argv[1] = key;
	argv[2] = member;

	redisReply *reply = redis_conn_send(conn, argc, argv, NULL);

	if (!reply) {
		log_printf(LOG_WARNING, "No connection to redis: ZSCORE %s %s", key, member);
		return -1;
	}
	
	if (reply->type != REDIS_REPLY_STRING && reply->type != REDIS_REPLY_INTEGER && reply->type != REDIS_REPLY_NIL)
		log_printf(LOG_WARNING, "redis_zscore: Failed for key: %s with member: \"%s\" Resp: \"%s\"", key, member, reply->type == REDIS_REPLY_ERROR ? reply->str : NULL);

	return redis_int_reply(reply);
}

int
redis_sock_zrange(struct fd_pool_conn *conn, const char *key, int start, int stop, void (*cb)(const void *value, size_t vlen, const void *score, size_t slen, void *cbarg), void *cbarg) {
	int argc;
	const char *argv[5];
	int res;
	unsigned int i;

	argc = 5;
	argv[0] = "ZRANGE";
	argv[1] = key;
	ALLOCA_PRINTF(res, argv[2], "%d", start);
	ALLOCA_PRINTF(res, argv[3], "%d", stop);
	argv[4] = "WITHSCORES";

	redisReply *reply = redis_conn_send(conn, argc, argv, NULL);

	if (!reply) {
		log_printf(LOG_WARNING, "No connection to redis: ZRANGE %s %d %d WITHSCORES", key, start, stop);
		return -1;
	}
	
	if (reply->type != REDIS_REPLY_ARRAY) {
		if (reply->type != REDIS_REPLY_NIL)
			log_printf(LOG_WARNING, "redis_zrange: Failed for key: %s with start: \"%d\" and stop: \"%d\" Resp: \"%s\"", key, start, stop, reply->type == REDIS_REPLY_ERROR ? reply->str : NULL);
		freeReplyObject(reply);
		return -1;
	}

	if (reply->elements == 0) {
		freeReplyObject(reply);
		return 0;
	}

	for (i = 0 ; i < reply->elements - 1 ; i += 2) {
		redisReply *value = reply->element[i];
		redisReply *score = reply->element[i + 1];

		if (value->type != REDIS_REPLY_STRING) {
			log_printf(LOG_WARNING, "redis_sock_zrange: value is not string");
			continue;
		}

		if (score->type != REDIS_REPLY_STRING) {
			log_printf(LOG_WARNING, "redis_sock_zrange: score is not int");
			continue;
		}

		(*cb)(value->str, value->len, score->str, score->len, cbarg);
	}
	
	res = reply->elements / 2;

	freeReplyObject(reply);

	return res;
}

int
redis_sock_lrange(struct fd_pool_conn *conn, const char *key, int start, int lim, void (*cb)(const void *value, size_t vlen, void *cbarg), void *cbarg) {
	int argc;
	const char *argv[4];
	int res;
	unsigned int i;

	argc = 4;
	argv[0] = "LRANGE";
	argv[1] = key;
	ALLOCA_PRINTF(res, argv[2], "%d", start);
	ALLOCA_PRINTF(res, argv[3], "%d", lim);

	redisReply *reply = redis_conn_send(conn, argc, argv, NULL);

	if (!reply) {
		log_printf(LOG_WARNING, "No connection to redis: LRANGE %s %d %d", key, start, lim);
		return -1;
	}
	
	if (reply->type != REDIS_REPLY_ARRAY) {
		if (reply->type != REDIS_REPLY_NIL)
			log_printf(LOG_WARNING, "redis_lrange: Failed for key: %s with start: \"%d\" and lim: \"%d\" Resp: \"%s\"", key, start, lim, reply->type == REDIS_REPLY_ERROR ? reply->str : NULL);
		freeReplyObject(reply);
		return -1;
	}

	if (reply->elements == 0) {
		freeReplyObject(reply);
		return 0;
	}

	for (i = 0 ; i < reply->elements; i++) {
		redisReply *value = reply->element[i];

		if (value->type != REDIS_REPLY_STRING) {
			log_printf(LOG_WARNING, "redis_sock_lrange: value is not string");
			continue;
		}

		(*cb)(value->str, value->len, cbarg);
	}
	
	res = reply->elements;

	freeReplyObject(reply);

	return res;
}

int
redis_sock_zrevrange(struct fd_pool_conn *conn, const char *key, int start, int stop, void (*cb)(const void *value, size_t vlen, const void *score, size_t slen, void *cbarg), void *cbarg) {
	int argc;
	const char *argv[5];
	int res;
	unsigned int i;

	argc = 5;
	argv[0] = "ZREVRANGE";
	argv[1] = key;
	ALLOCA_PRINTF(res, argv[2], "%d", start);
	ALLOCA_PRINTF(res, argv[3], "%d", stop);
	argv[4] = "WITHSCORES";

	redisReply *reply = redis_conn_send(conn, argc, argv, NULL);

	if (!reply) {
		log_printf(LOG_WARNING, "No connection to redis: ZREVRANGE %s %d %d WITHSCORES", key, start, stop);
		return -1;
	}
	
	if (reply->type != REDIS_REPLY_ARRAY) {
		if (reply->type != REDIS_REPLY_NIL)
			log_printf(LOG_WARNING, "redis_zrevrange: Failed for key: %s with start: \"%d\" and stop: \"%d\" Resp: \"%s\"", key, start, stop, reply->type == REDIS_REPLY_ERROR ? reply->str : NULL);
		freeReplyObject(reply);
		return -1;
	}

	if (reply->elements == 0) {
		freeReplyObject(reply);
		return 0;
	}

	for (i = 0 ; i < reply->elements - 1 ; i += 2) {
		redisReply *value = reply->element[i];
		redisReply *score = reply->element[i + 1];

		if (value->type != REDIS_REPLY_STRING) {
			log_printf(LOG_WARNING, "redis_sock_zrevrange: value is not string");
			continue;
		}

		if (score->type != REDIS_REPLY_STRING) {
			log_printf(LOG_WARNING, "redis_sock_zrevrange: score is not string");
			continue;
		}

		(*cb)(value->str, value->len, score->str, score->len, cbarg);
	}
	
	res = reply->elements / 2;

	freeReplyObject(reply);

	return res;
}

int
redis_sock_zrangebyscore(struct fd_pool_conn *conn, const char *key, const char *min, const char *max, int offset, int count, void (*cb)(const void *value, size_t vlen, const void *score, size_t slen, void *cbarg), void *cbarg) {
	int argc;
	const char *argv[8];
	int res;
	unsigned int i;

	argc = 5;
	argv[0] = "ZRANGEBYSCORE";
	argv[1] = key;
	argv[2] = min;
	argv[3] = max;
	if(offset >= 0) {
		argc = 8;
		argv[4] = "LIMIT";
		ALLOCA_PRINTF(res, argv[5], "%d", offset);
		ALLOCA_PRINTF(res, argv[6], "%d", count);
	} 
	argv[argc - 1] = "WITHSCORES";

	redisReply *reply = redis_conn_send(conn, argc, argv, NULL);

	if (!reply) {
		log_printf(LOG_WARNING, "No connection to redis: ZRANGEBYSCORE %s %s %s LIMIT %d %d WITHSCORES", key, min, max, offset, count);
		return -1;
	}
	
	if (reply->type != REDIS_REPLY_ARRAY) {
		log_printf(LOG_WARNING, "redis_zrangebyscore: Failed for key: %s with min: \"%s\", max: \"%s\", offset: \"%d\", count: \"%d\", Resp: \"%s\"", key, min, max, offset, count, reply->type == REDIS_REPLY_ERROR ? reply->str : NULL);
		freeReplyObject(reply);
		return -1;
	}

	if (reply->elements == 0) {
		freeReplyObject(reply);
		return 0;
	}

	for (i = 0 ; i < reply->elements - 1 ; i += 2) {
		redisReply *value = reply->element[i];
		redisReply *score = reply->element[i + 1];

		if (value->type != REDIS_REPLY_STRING) {
			log_printf(LOG_WARNING, "redis_zrangebyscore: value is not string");
			continue;
		}

		if (score->type != REDIS_REPLY_STRING) {
			log_printf(LOG_WARNING, "redis_zrangebyscore: score is not int");
			continue;
		}

		(*cb)(value->str, value->len, score->str, score->len, cbarg);
	}
	
	res = reply->elements / 2;

	freeReplyObject(reply);

	return res;
}

int
redis_sock_del(struct fd_pool_conn *conn, const char *key) {
	int argc;
	const char *argv[2];

	if (key == NULL) {
		return -1;
	}

	argc = 2;
	argv[0] = "DEL";
	argv[1] = key;

	redisReply *reply = redis_conn_send(conn, argc, argv, NULL);

	if (!reply) {
		log_printf(LOG_WARNING, "No connection to redis: DEL %s", key);
		return -1;
	}

	if (reply->type != REDIS_REPLY_INTEGER)
		log_printf(LOG_WARNING, "redis_cdel failed for key:%s, with resp: %s", key, reply->type == REDIS_REPLY_ERROR ? reply->str : NULL);

	return redis_int_reply(reply);
}

int
redis_multi_sock_del(struct fd_pool_conn *conn, const char *key) {
	int argc;
	const char *argv[2];

	if (key == NULL) {
		return -1;
	}

	argc = 2;
	argv[0] = "DEL";
	argv[1] = key;

	redisReply *reply = redis_conn_send(conn, argc, argv, NULL);

	if (!reply) {
		log_printf(LOG_WARNING, "No connection to redis: DEL %s", key);
		return -1;
	}

	if (reply->type != REDIS_REPLY_STATUS)
		log_printf(LOG_WARNING, "redis_multi_del failed for key:%s, with resp: %s", key, reply->type == REDIS_REPLY_ERROR ? reply->str : NULL);

	return redis_status_reply(reply);
}

int
redis_sock_zincrby(struct fd_pool_conn *conn, const char *key, int incrby, const char *member) {
	int argc;
	const char *argv[4];
	int res;

	if (key == NULL || member == NULL) {
		return -1;
	}

	argc = 4;
	argv[0] = "ZINCRBY";
	argv[1] = key;
	ALLOCA_PRINTF(res, argv[2], "%d", incrby);
	argv[3] = member;

	redisReply *reply = redis_conn_send(conn, argc, argv, NULL);

	if (!reply) {
		log_printf(LOG_WARNING, "No connection to redis: ZINCRBY %s %d %s", key, incrby, member);
		return -1;
	}

	if (reply->type != REDIS_REPLY_STRING && reply->type != REDIS_REPLY_INTEGER)
		log_printf(LOG_WARNING, "redis_sock_zincrby: Failed for key: %s with member: \"%s\" Resp: \"%s\"", key, member, reply->type == REDIS_REPLY_ERROR ? reply->str : NULL);

	return redis_int_reply(reply);
}

int
redis_sock_hincrby(struct fd_pool_conn *conn, const char *hash, const char* key, int incrby) {
	int argc;
	const char *argv[4];
	int res;

	if (hash == NULL || key == NULL) {
		return -1;
	}

	argc = 4;
	argv[0] = "HINCRBY";
	argv[1] = hash;
	argv[2] = key;
	ALLOCA_PRINTF(res, argv[3], "%d", incrby);

	redisReply *reply = redis_conn_send(conn, argc, argv, NULL);

	if (!reply) {
		log_printf(LOG_WARNING, "No connection to redis: HINCRBY %s %s %d", hash, key, incrby);
		return -1;
	}

	if (reply->type != REDIS_REPLY_STRING && reply->type != REDIS_REPLY_INTEGER)
		log_printf(LOG_WARNING, "redis_sock_hincrby: Failed for hash: %s, key: %s with increment %d. Resp: \"%s\"", hash, key, incrby, reply->type == REDIS_REPLY_ERROR ? reply->str : NULL);

	return redis_int_reply(reply);
}

int
redis_sock_exists(struct fd_pool_conn *conn, const char *key) {
	int argc;
	const char *argv[2];

	if (key == NULL) {
		return -1;
	}

	argc = 2;
	argv[0] = "EXISTS";
	argv[1] = key;

	redisReply *reply = redis_conn_send(conn, argc, argv, NULL);

	if (!reply) {
		log_printf(LOG_WARNING, "No connection to redis: EXISTS %s", key);
		return -1;
	}

	if (reply->type != REDIS_REPLY_INTEGER)
		log_printf(LOG_WARNING, "redis_sock_exists failed for key:%s, with resp: %s", key, reply->type == REDIS_REPLY_ERROR ? reply->str : NULL);

	return redis_int_reply(reply);
}

int
redis_sock_setnx(struct fd_pool_conn *conn, const char *key, const char *value) {
	int argc;
	const char *argv[3];

	if (key == NULL || value == NULL) {
		return -1;
	}

	argc = 3;
	argv[0] = "SETNX";
	argv[1] = key;
	argv[2] = value;

	redisReply *reply = redis_conn_send(conn, argc, argv, NULL);

	if (!reply) {
		log_printf(LOG_WARNING, "No connection to redis: SETNX %s %s", key, value);
		return -1;
	}

	if (reply->type != REDIS_REPLY_INTEGER)
		log_printf(LOG_WARNING, "redis_sock_setnx failed for: %s %s, with resp: %s", key, value, reply->type == REDIS_REPLY_ERROR ? reply->str : NULL);

	return redis_int_reply(reply);
}

int
redis_sock_set(struct fd_pool_conn *conn, const char *key, const char *value, int expire) {
	int argc;
	const char *argv[4];
	int res;

	if (key == NULL || value == NULL) {
		return -1;
	}

	if(expire > -1) {
		argc = 4;
		argv[0] = "SETEX";
		argv[1] = key;
		ALLOCA_PRINTF(res, argv[2], "%d", expire);
		argv[3] = value;
	} else {
		argc = 3;
		argv[0] = "SET";
		argv[1] = key;
		argv[2] = value;
	}

	redisReply *reply = redis_conn_send(conn, argc, argv, NULL);

	if (!reply) {
		log_printf(LOG_WARNING, "No connection to redis: %s %s %d %s", argv[0], key, expire, value);
		return -1;
	}

	if (reply->type != REDIS_REPLY_STATUS)
		log_printf(LOG_WARNING, "redis_set: Failed for key: %s with value: \"%s\", expire: %d, Error: \"%s\"", key, value, expire, reply->type == REDIS_REPLY_ERROR ? reply->str : NULL);

	return redis_status_reply(reply);
}

char *
redis_sock_get(struct fd_pool_conn *conn, const char *key) {
	int argc;
	const char *argv[2];

	if (key == NULL) {
		return NULL;
	}

	argc = 2;
	argv[0] = "GET";
	argv[1] = key;

	redisReply *reply = redis_conn_send(conn, argc, argv, NULL);

	if (!reply) {
		log_printf(LOG_WARNING, "No connection to redis: GET %s", key);
		return NULL;
	}

	if (!reply || reply->type == REDIS_REPLY_ERROR)
		log_printf(LOG_INFO, "redis_sock_get: Failed for key: %s", key);

	return redis_string_reply(reply);
}

int
redis_sock_hset(struct fd_pool_conn *conn, const char *key, const char *field, const char *value) {
	int argc;
	const char *argv[4];

	if (key == NULL || value == NULL) {
		return -1;
	}

	argc = 4;
	argv[0] = "HSET";
	argv[1] = key;
	argv[2] = field;
	argv[3] = value;

	redisReply *reply = redis_conn_send(conn, argc, argv, NULL);

	if (!reply) {
		log_printf(LOG_WARNING, "No connection to redis: HSET %s %s %s", key, field, value);
		return -1;
	}

	if (reply->type != REDIS_REPLY_INTEGER)
		log_printf(LOG_WARNING, "redis_sock_hset failed for: %s %s %s, with resp: %s", key, field, value, reply->type == REDIS_REPLY_ERROR ? reply->str : NULL);

	return redis_int_reply(reply);
}

int
redis_sock_hmset_array(struct fd_pool_conn *conn, const char *key, int nargs, const char *args[nargs]) {
	const char *argv[2+nargs];

	if (key == NULL)
		return -1;
	/* Enforce an even count of keys and values */
	if (nargs & 1)
		return -3;

	argv[0] = "HMSET";
	argv[1] = key;

	for (int i = 0; i < nargs; ++i)
		argv[i+2] = args[i];

	redisReply *reply = redis_conn_send(conn, 2+nargs, argv, NULL);

	if (!reply) {
		log_printf(LOG_WARNING, "No connection to redis: HMSET %s <%d keys>", key, nargs);
		return -1;
	}

	if (reply->type != REDIS_REPLY_STATUS)
		log_printf(LOG_WARNING, "redis_sock_hmset failed for: %s <%d keys>, with resp: %s", key, nargs, reply->type == REDIS_REPLY_ERROR ? reply->str : NULL);

	return redis_status_reply(reply);
}

int
redis_sock_hmset(struct fd_pool_conn *conn, const char *key, ...) {
	const char *argv[128]; /* room for a maximum of 128/2 keys+values in vararg */
	va_list args;
	int argc = 0;

	va_start(args, key);
	const char* next_arg = va_arg(args, const char*);
	while (next_arg) {
		if (argc >= (int)(sizeof(argv)/sizeof(argv[0]))) {
			va_end(args);
			return -2;
		}
		argv[argc++] = next_arg;
		next_arg = va_arg(args, const char*);
	}
	va_end(args);

	return redis_sock_hmset_array(conn, key, argc, argv);
}

char *
redis_sock_hget(struct fd_pool_conn *conn, const char *key, const char *field) {
	int argc;
	const char *argv[3];

	if (key == NULL) {
		return NULL;
	}

	argc = 3;
	argv[0] = "HGET";
	argv[1] = key;
	argv[2] = field;

	redisReply *reply = redis_conn_send(conn, argc, argv, NULL);

	if (!reply) {
		log_printf(LOG_WARNING, "No connection to redis: HGET %s %s", key, field);
		return NULL;
	}

	if (reply->type == REDIS_REPLY_ERROR)
		log_printf(LOG_INFO, "redis_sock_hget: Failed for key: %s %s", key, field);

	return redis_string_reply(reply);
}

int
redis_sock_incr(struct fd_pool_conn *conn, const char *key) {
	int argc;
	const char *argv[2];

	argc = 2;
	argv[0] = "INCR";
	argv[1] = key;

	redisReply *reply = redis_conn_send(conn, argc, argv, NULL);

	if (!reply) {
		log_printf(LOG_WARNING, "No connection to redis: INCR %s", key);
		return 0;
	}

	if (!reply || reply->type != REDIS_REPLY_INTEGER)
		log_printf(LOG_WARNING, "redis_sock_incr: Failed for key: %s", key);

	return redis_int_reply(reply);
}

int
redis_sock_keys(struct fd_pool_conn *conn, const char *key, void (*cb)(const void *value, size_t vlen, void *cbarg), void *cbarg) {
	int argc = 2;
	const char *argv[2];
	unsigned int i;

	argv[0] = "KEYS";
	argv[1] = key;

	log_printf(LOG_DEBUG, "redis_sock_keys: %s %s", argv[0], argv[1]) ;

	redisReply *reply = redis_conn_send(conn, argc, argv, NULL);

	if (!reply) {
		log_printf(LOG_WARNING, "No connection to redis: KEYS %s", key);
		return -1;
	}

	if (reply->type != REDIS_REPLY_ARRAY) {
		if (reply->type != REDIS_REPLY_NIL)
			log_printf(LOG_WARNING, "redis_sock_keys: Failed for key: %s error: %s", key, reply->type == REDIS_REPLY_ERROR ? reply->str : NULL);
		else
			log_printf(LOG_DEBUG, "redis_sock_keys: Received nil reply for key: %s", key) ;
		freeReplyObject(reply);
		return -1;
	}

	if (reply->elements == 0) {
		log_printf(LOG_DEBUG, "redis_sock_keys:  Received zero length reply for key: %s", key) ;
		freeReplyObject(reply);
		return 0;
	}

	log_printf(LOG_DEBUG, "redis_sock_keys: Received reply with %ld elements", reply->elements) ;

	for (i = 0 ; i < reply->elements ; i++) {
		redisReply *value = reply->element[i];

		if (value->type != REDIS_REPLY_STRING) {
			log_printf(LOG_WARNING, "redis_sock_keys: value is not string");
			continue;
		}

		log_printf(LOG_DEBUG, "redis_sock_keys: Received reply element with value: %s", value->str) ;

		(*cb)(value->str, value->len, cbarg);
	}
	
	int res = reply->elements;

	freeReplyObject(reply);

	return res;
}


int
redis_sock_hgetall(struct fd_pool_conn *conn, const char *key, void (*cb)(const void *key, size_t klen, const void *value, size_t vlen, void *cbarg), void *cbarg) {
	int argc = 2;
	const char *argv[2];
	unsigned int i;

	argv[0] = "HGETALL";
	argv[1] = key;

	redisReply *reply = redis_conn_send(conn, argc, argv, NULL);

	if (!reply) {
		log_printf(LOG_WARNING, "No connection to redis: HGETALL %s", key);
		return -1;
	}

	if (reply->type != REDIS_REPLY_ARRAY) {
		if (reply->type != REDIS_REPLY_NIL)
			log_printf(LOG_WARNING, "redis_sock_hgetall: Failed for key: %s error: %s", key, reply->type == REDIS_REPLY_ERROR ? reply->str : NULL);
		freeReplyObject(reply);
		return -1;
	}

	if (reply->elements == 0) {
		freeReplyObject(reply);
		return 0;
	}

	for (i = 0 ; i < reply->elements - 1 ; i += 2) {
		redisReply *key = reply->element[i];
		redisReply *value = reply->element[i + 1];

		if (key->type != REDIS_REPLY_STRING) {
			log_printf(LOG_WARNING, "redis_sock_hgetall: key is not string");
			continue;
		}

		if (value->type != REDIS_REPLY_STRING) {
			log_printf(LOG_WARNING, "redis_sock_hgetall: value is not string");
			continue;
		}

		(*cb)(key->str, key->len, value->str, value->len, cbarg);
	}
	
	int res = reply->elements / 2;

	freeReplyObject(reply);

	return res;
}

int 
redis_sock_expire(struct fd_pool_conn *conn, const char *key, unsigned int seconds) {
	int argc;
	const char *argv[3];
	int res;

	argc = 3;
	argv[0] = "EXPIRE";
	argv[1] = key;
	ALLOCA_PRINTF(res, argv[2], "%d", seconds);

	redisReply *reply = redis_conn_send(conn, argc, argv, NULL);
	if (!reply) {
		log_printf(LOG_WARNING, "No connection to redis: EXPR %s %d", key, seconds);
		return 0;
	}
	
	if (!reply || reply->type != REDIS_REPLY_INTEGER)
		log_printf(LOG_WARNING, "redis_sock_expire: Failed for key: %s", key);

	return redis_int_reply(reply);

}


int 
redis_sock_ttl(struct fd_pool_conn *conn, const char *key) {
	int argc;
	const char *argv[2];

	argc = 2;
	argv[0] = "TTL";
	argv[1] = key;

	redisReply *reply = redis_conn_send(conn, argc, argv, NULL);

	if (!reply) {
		log_printf(LOG_WARNING, "No connection to redis: TTL %s", key);
		return 0;
	}
	
	if (!reply || reply->type != REDIS_REPLY_INTEGER)
		log_printf(LOG_WARNING, "redis_sock_ttl: Failed for key: %s", key);
	
	return redis_int_reply(reply);
}

int
redis_sock_hdel(struct fd_pool_conn *conn, const char *set, const char *key) {
	int argc;
	const char *argv[3];

	if (key == NULL || set == NULL) {
		return -1;
	}

	argc = 3;
	argv[0] = "HDEL";
	argv[1] = set;
	argv[2] = key;

	redisReply *reply = redis_conn_send(conn, argc, argv, NULL);

	if (!reply) {
		log_printf(LOG_WARNING, "No connection to redis: HDEL %s", key);
		return -1;
	}

	if (reply->type != REDIS_REPLY_INTEGER)
		log_printf(LOG_WARNING, "redis_hdel failed for key:%s, with resp: %s", key, reply->type == REDIS_REPLY_ERROR ? reply->str : NULL);

	return redis_int_reply(reply);
}

int
redis_sock_publish(struct fd_pool_conn *conn, const char *channel, const char *message) {
	int argc;
	const char *argv[3];

	if (channel == NULL || message == NULL) {
		return -1;
	}

	argc = 3;
	argv[0] = "PUBLISH";
	argv[1] = channel;
	argv[2] = message;

	redisReply *reply = redis_conn_send(conn, argc, argv, NULL);

	if (!reply) {
		log_printf(LOG_WARNING, "No connection to redis: PUBLISH %s", channel);
		return -1;
	}

	if (reply->type != REDIS_REPLY_INTEGER)
		log_printf(LOG_WARNING, "redis_sock_publish failed for channel:%s, with resp: %s", channel, reply->type == REDIS_REPLY_ERROR ? reply->str : NULL);

	return redis_int_reply(reply);
}

int
redis_sock_rename(struct fd_pool_conn *conn, const char *key, const char *newkey) {
	int argc;
	const char *argv[3];

	if (key == NULL || newkey == NULL) {
		return -1;
	}

	argc = 3;
	argv[0] = "RENAME";
	argv[1] = key;
	argv[2] = newkey;

	redisReply *reply = redis_conn_send(conn, argc, argv, NULL);

	if (!reply) {
		log_printf(LOG_WARNING, "No connection to redis: RENAME %s %s", key, newkey);
		return -1;
	}

	if (reply->type != REDIS_REPLY_STATUS)
		log_printf(LOG_WARNING, "redis_rename: Failed for key, newkey: (%s, %s). Error: \"%s\"", key, newkey, reply->type == REDIS_REPLY_ERROR ? reply->str : NULL);

	return redis_status_reply(reply);
}

int
redis_sock_sadd(struct fd_pool_conn *conn, const char *key, const char *member) {
	int argc;
	const char *argv[3];

	if (key == NULL || member == NULL) {
		return -1;
	}

	argc = 3;
	argv[0] = "SADD";
	argv[1] = key;
	argv[2] = member;

	redisReply *reply = redis_conn_send(conn, argc, argv, NULL);

	if (!reply) {
		log_printf(LOG_WARNING, "No connection to redis: SADD %s %s", key, member);
		return -1;
	}

	if (reply->type != REDIS_REPLY_INTEGER)
		log_printf(LOG_WARNING, "redis_sadd: Failed for key: %s with member: \"%s\", Redis response: \"%s\"", key, member, reply->type == REDIS_REPLY_ERROR ? reply->str : NULL);

	return redis_int_reply(reply);
}


int
redis_sock_srem(struct fd_pool_conn *conn, const char *key, const char *member) {
	int argc;
	const char *argv[3];

	if (key == NULL || member == NULL) {
		return -1;
	}

	argc = 3;
	argv[0] = "SREM";
	argv[1] = key;
	argv[2] = member;

	redisReply *reply = redis_conn_send(conn, argc, argv, NULL);

	if (!reply) {
		log_printf(LOG_WARNING, "No connection to redis: SREM %s %s", key, member);
		return -1;
	}

	if (reply->type != REDIS_REPLY_INTEGER)
		log_printf(LOG_WARNING, "redis_srem: Failed for key: %s with member: \"%s\" Resp: \"%s\"", key, member, reply->type == REDIS_REPLY_ERROR ? reply->str : NULL);

	return redis_int_reply(reply);
}

int
redis_sock_sismember(struct fd_pool_conn *conn, const char *key, const char *member) {
	int argc;
	const char *argv[3];

	if (key == NULL || member == NULL) {
		return -1;
	}

	argc = 3;
	argv[0] = "SISMEMBER";
	argv[1] = key;
	argv[2] = member;

	redisReply *reply = redis_conn_send(conn, argc, argv, NULL);

	if (!reply) {
		log_printf(LOG_WARNING, "No connection to redis: SISMEMBER %s %s", key, member);
		return -1;
	}

	if (reply->type != REDIS_REPLY_INTEGER)
		log_printf(LOG_WARNING, "redis_sismember: Failed for key: %s with member: \"%s\" Resp: \"%s\"", key, member, reply->type == REDIS_REPLY_ERROR ? reply->str : NULL);

	return redis_int_reply(reply);

}

int
redis_sock_scard(struct fd_pool_conn *conn, const char *set) {
	int argc = 2;
	const char *argv[2];

	if (set == NULL) {
		return -1;
	}

	argv[0] = "SCARD";
	argv[1] = set;

	redisReply *reply = redis_conn_send(conn, argc, argv, NULL);

	if (!reply) {
		log_printf(LOG_WARNING, "No connection to redis: SCARD %s", set);
		return -1;
	}

	if (reply->type != REDIS_REPLY_INTEGER)
		log_printf(LOG_WARNING, "redis_scard: Failed for set: %s Resp: \"%s\"", set, reply->type == REDIS_REPLY_ERROR ? reply->str : NULL);

	return redis_int_reply(reply);
}

int
redis_sock_llen(struct fd_pool_conn *conn, const char *key) {
	int argc;
	const char *argv[2];

	if (key == NULL) {
		return -1;
	}

	argc = 2;
	argv[0] = "LLEN";
	argv[1] = key;

	redisReply *reply = redis_conn_send(conn, argc, argv, NULL);

	if (!reply) {
		log_printf(LOG_WARNING, "No connection to redis: LLEN %s", key);
		return -1;
	}

	if (!reply || reply->type == REDIS_REPLY_ERROR)
		log_printf(LOG_INFO, "redis_sock_llen: Failed for key: %s", key);

	return redis_int_reply(reply);
}

char *
redis_sock_lindex(struct fd_pool_conn *conn, const char *key, int idx) {
	int argc;
	const char *argv[4];
	int res;

	if (key == NULL || idx < 0) {
		return NULL;
	}

	argc = 3;
	argv[0] = "LINDEX";
	argv[1] = key;
	ALLOCA_PRINTF(res, argv[2], "%d", idx);

	redisReply *reply = redis_conn_send(conn, argc, argv, NULL);

	if (!reply) {
		log_printf(LOG_WARNING, "No connection to redis: %s %s %d", argv[0], key, idx);
		return NULL;
	}

	if (reply->type == REDIS_REPLY_ERROR)
		log_printf(LOG_WARNING, "redis_sock_lindex: Failed for key: %s, index %d, Error: \"%s\"", key, idx, reply->str);

	return redis_string_reply(reply);
}

int
redis_sock_hexists(struct fd_pool_conn *conn, const char *key, const char *field) {
	int argc;
	const char *argv[3];

	if (key == NULL) {
		return 0;
	}

	argc = 3;
	argv[0] = "HEXISTS";
	argv[1] = key;
	argv[2] = field;

	redisReply *reply = redis_conn_send(conn, argc, argv, NULL);

	if (!reply) {
		log_printf(LOG_WARNING, "No connection to redis: HEXISTS %s %s", key, field);
		return 0;
	}

	if (reply->type == REDIS_REPLY_ERROR)
		log_printf(LOG_INFO, "redis_sock_hexists: Failed for key: %s %s", key, field);

	return redis_int_reply(reply);
}

int
redis_sock_hlen(struct fd_pool_conn *conn, const char *key) {
	int argc;
	const char *argv[2];

	if (key == NULL) {
		return 0;
	}

	argc = 2;
	argv[0] = "HLEN";
	argv[1] = key;

	redisReply *reply = redis_conn_send(conn, argc, argv, NULL);

	if (!reply) {
		log_printf(LOG_WARNING, "No connection to redis: HLEN %s", key);
		return 0;
	}

	if (reply->type == REDIS_REPLY_ERROR)
		log_printf(LOG_INFO, "redis_sock_hlen: Failed for key: %s", key);

	return redis_int_reply(reply);
}

/* lists */
int
redis_sock_lpush (struct fd_pool_conn *conn, const char *key, const char *value) {
	int argc;
	const char *argv[3];

	if (key == NULL || value == NULL) {
		return -1;
	}

	argc = 3;
	argv[0] = "LPUSH";
	argv[1] = key;
	argv[2] = value;

	redisReply *reply = redis_conn_send(conn, argc, argv, NULL);

	if (!reply) {
		log_printf(LOG_WARNING, "No connection to redis: LPUSH %s %s", key, value);
		return -1;
	}

	if (reply->type != REDIS_REPLY_INTEGER)
		log_printf(LOG_WARNING, "redis_sock_lpush failed for: %s %s, with resp: %s", key, value, reply->type == REDIS_REPLY_ERROR ? reply->str : NULL);

	return redis_int_reply(reply);
}

int
redis_sock_rpush (struct fd_pool_conn *conn, const char *key, const char *value) {
	int argc;
	const char *argv[3];

	if (key == NULL || value == NULL) {
		return -1;
	}

	argc = 3;
	argv[0] = "RPUSH";
	argv[1] = key;
	argv[2] = value;

	redisReply *reply = redis_conn_send(conn, argc, argv, NULL);

	if (!reply) {
		log_printf(LOG_WARNING, "No connection to redis: RPUSH %s %s", key, value);
		return -1;
	}

	if (reply->type != REDIS_REPLY_INTEGER)
		log_printf(LOG_WARNING, "redis_sock_rpush failed for: %s %s, with resp: %s", key, value, reply->type == REDIS_REPLY_ERROR ? reply->str : NULL);

	return redis_int_reply(reply);
}

char *
redis_sock_rpop (struct fd_pool_conn *conn, const char *key) {
	int argc;
	const char *argv[2];

	if (key == NULL) {
		return NULL;
	}

	argc = 2;
	argv[0] = "RPOP";
	argv[1] = key;

	redisReply *reply = redis_conn_send(conn, argc, argv, NULL);

	if (!reply) {
		log_printf(LOG_WARNING, "No connection to redis: RPOP %s", key);
		return NULL;
	}

	if (reply->type == REDIS_REPLY_ERROR)
		log_printf(LOG_WARNING, "redis_sock_rpop: Failed for key: %s", key);

	return redis_string_reply(reply);
}

int
redis_sock_sinter(struct fd_pool_conn *conn, const char *set_a, const char *set_b, void (*cb)(const void *value, size_t vlen, void *cbarg), void *cbarg) {
	int argc = 3;
	const char *argv[3];
	unsigned int i;

	argv[0] = "SINTER";
	argv[1] = set_a;
	argv[2] = set_b;

	log_printf(LOG_DEBUG, "redis_sock_sinter: %s %s %s", argv[0], argv[1], argv[2]) ;

	redisReply *reply = redis_conn_send(conn, argc, argv, NULL);

	if (!reply) {
		log_printf(LOG_WARNING, "No connection to redis: SINTER %s %s", set_a, set_b);
		return -1;
	}

	if (reply->type != REDIS_REPLY_ARRAY) {
		if (reply->type != REDIS_REPLY_NIL)
			log_printf(LOG_WARNING, "redis_sock_sinter: Failed for sets: %s %s error: %s", set_a, set_b, reply->type == REDIS_REPLY_ERROR ? reply->str : NULL);
		else
			log_printf(LOG_DEBUG, "redis_sock_sinter: Received nil reply for sets: %s %s", set_a, set_b) ;
		freeReplyObject(reply);
		return -1;
	}

	if (reply->elements == 0) {
		log_printf(LOG_DEBUG, "redis_sock_sinter:  Received zero length reply for sets: %s %s", set_a, set_b);
		freeReplyObject(reply);
		return 0;
	}

	log_printf(LOG_DEBUG, "redis_sock_sinter: Received reply with %ld elements", reply->elements) ;

	for (i = 0 ; i < reply->elements ; i++) {
		redisReply *value = reply->element[i];

		if (value->type != REDIS_REPLY_STRING) {
			log_printf(LOG_WARNING, "redis_sock_sinter: value is not string");
			continue;
		}

		log_printf(LOG_DEBUG, "redis_sock_sinter: Received reply element with value: %s", value->str) ;

		(*cb)(value->str, value->len, cbarg);
	}

	int res = reply->elements;

	freeReplyObject(reply);

	return res;
}

int
redis_sock_smembers(struct fd_pool_conn *conn, const char *set, void (*cb)(const void *value, size_t vlen, void *cbarg), void *cbarg) {
	int argc = 2;
	const char *argv[2];
	unsigned int i;

	argv[0] = "SMEMBERS";
	argv[1] = set;

	log_printf(LOG_DEBUG, "redis_sock_smembers: %s %s", argv[0], argv[1]);

	redisReply *reply = redis_conn_send(conn, argc, argv, NULL);

	if (!reply) {
		log_printf(LOG_WARNING, "No connection to redis: SMEMBERS %s", set);
		return -1;
	}

	if (reply->type != REDIS_REPLY_ARRAY) {
		if (reply->type != REDIS_REPLY_NIL)
			log_printf(LOG_WARNING, "redis_sock_smembers: Failed for set: %s error: %s", set, reply->type == REDIS_REPLY_ERROR ? reply->str : NULL);
		else
			log_printf(LOG_DEBUG, "redis_sock_smembers: Received nil reply for set: %s", set);
		freeReplyObject(reply);
		return -1;
	}

	if (reply->elements == 0) {
		log_printf(LOG_DEBUG, "redis_sock_smembers:  Received zero length reply for set: %s", set);
		freeReplyObject(reply);
		return 0;
	}

	log_printf(LOG_DEBUG, "redis_sock_smembers: Received reply with %ld elements", reply->elements) ;

	for (i = 0 ; i < reply->elements ; i++) {
		redisReply *value = reply->element[i];

		if (value->type != REDIS_REPLY_STRING) {
			log_printf(LOG_WARNING, "redis_sock_smembers: value is not string");
			continue;
		}

		log_printf(LOG_DEBUG, "redis_sock_smembers: Received reply element with value: %s", value->str) ;

		(*cb)(value->str, value->len, cbarg);
	}

	int res = reply->elements;

	freeReplyObject(reply);

	return res;
}

int
redis_sock_evalsha(struct fd_pool_conn *conn, const char *sha, int nkeys, int nargs, const char *args[nargs], void (*cb)(const void *value, size_t vlen, void *cbarg), void *cbarg) {
	int argc = nargs+3;
	unsigned int i;
	const char *argv[argc];

	log_printf(LOG_INFO, "redis_sock_evalsha: Calling with sha %s, %d keys (total %d arguments)", sha, nkeys, nargs);

	if (!sha || !(*sha)) {
		log_printf(LOG_WARNING, "redis_sock_evalsha: Calling with empty sha");
		return -1;
	}

	if (strlen(sha) != 40) {
		log_printf(LOG_WARNING, "redis_sock_evalsha: Invalid hash: %s", sha);
		return -1;
	}

	char keys[32];
	snprintf(keys, 32, "%d", nkeys);

	argv[0] = "EVALSHA";
	argv[1] = sha;
	argv[2] = keys;

	int k;
	for (k = 0; k < nargs; ++k)
		argv[3+k] = args[k];

	redisReply *reply = redis_conn_send(conn, argc, argv, NULL);

	if (!reply) {
	       log_printf(LOG_WARNING, "No connection to redis: EVALSHA %s", sha);
	       return -1;
	}

	if (reply->type != REDIS_REPLY_ARRAY) {
		if (reply->type != REDIS_REPLY_NIL)
			log_printf(LOG_WARNING, "redis_sock_evalsha: Failed for hash: %s error: %s", sha, reply->type == REDIS_REPLY_ERROR ? reply->str : NULL);
		else
			log_printf(LOG_WARNING, "redis_sock_evalsha: Received nil reply for hash: %s", sha);
		freeReplyObject(reply);
		return -1;
	}

	if (reply->elements == 0) {
		log_printf(LOG_DEBUG, "redis_sock_evalsha:  Received zero length reply for hash: %s", sha);
		freeReplyObject(reply);
		return 0;
	}

	log_printf(LOG_DEBUG, "redis_sock_evalsha: Received reply with %ld elements", reply->elements) ;

	for (i = 0 ; i < reply->elements ; i++) {
		redisReply *value = reply->element[i];

		if (value->type != REDIS_REPLY_STRING) {
			log_printf(LOG_WARNING, "redis_sock_evalsha: value is not string");
			continue;
		}

		log_printf(LOG_DEBUG, "redis_sock_evalsha: Received reply element with value: %s", value->str) ;

		(*cb)(value->str, value->len, cbarg);
	}
	
	int res = reply->elements;

	freeReplyObject(reply);

	return res;
}

int
redis_sock_watch(struct fd_pool_conn *conn, const char *key) {
	int argc;
	const char *argv[2];

	if (key == NULL) {
		return -1;
	}

	argc = 2;
	argv[0] = "WATCH";
	argv[1] = key;

	redisReply *reply = redis_conn_send(conn, argc, argv, NULL);

	if (!reply) {
		log_printf(LOG_WARNING, "No connection to redis: WATCH %s", key);
		return -1;
	}

	if (reply->type == REDIS_REPLY_ERROR)
		log_printf(LOG_INFO, "redis_sock_watch: Failed for key: %s", key);

	return redis_status_reply(reply);
}

int
redis_sock_multi(struct fd_pool_conn *conn) {
	int argc;
	const char *argv[1];

	argc = 1;
	argv[0] = "MULTI";

	redisReply *reply = redis_conn_send(conn, argc, argv, NULL);

	if (!reply) {
		log_printf(LOG_WARNING, "No connection to redis: MULTI");
		return -1;
	}

	if (reply->type == REDIS_REPLY_ERROR)
		log_printf(LOG_INFO, "redis_sock_multi: Failed");

	return redis_status_reply(reply);
}

int
redis_sock_exec(struct fd_pool_conn *conn) {
	int argc;
	const char *argv[1];

	argc = 1;
	argv[0] = "EXEC";

	redisReply *reply = redis_conn_send(conn, argc, argv, NULL);

	if (!reply) {
		log_printf(LOG_WARNING, "No connection to redis: EXEC");
		return -1;
	}

	if (reply->type != REDIS_REPLY_ARRAY) {
		log_printf(LOG_INFO, "redis_sock_exec: Failed");
		return redis_status_reply(reply);
	}

	freeReplyObject(reply);
	return 0;
}
