#include <unistd.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <netdb.h>
#include <ctype.h>
#include <limits.h>
#include "queue.h"
#include "util.h"
#include "vtree.h"
#include "memcacheapi.h"
#include "fd_pool.h"
#include "fdgets.h"
#include "logging.h"

static struct fd_pool_conn *
memcache_query(const char *cmd, const char *session_id, const char *keyval, struct fd_pool *pool, int *fd) {
	struct bpapi_vtree_chain *node = fd_pool_node(pool);
	char *s = NULL;
	const char *memc_name;
	int sockfd = -1;
	struct fd_pool_conn *conn = fd_pool_new_conn(pool, NULL, 0);
	ssize_t n_written = 0;
	ssize_t n_remaining = 0;
	ssize_t one_read_remaining = 0;
	struct iovec *piov = NULL;

	if (session_id) {
		const char *sep = vtree_get(node, "id_separator", NULL);
		s = strstr(session_id, sep);
	}

	if (s) {
		memc_name = strndupa(session_id, s - session_id);
	} else {
		memc_name = vtree_get(node, "server", NULL);
	}

	if (!memc_name) {
		log_printf(LOG_ERR, "(CRIT) memcache_query failed to setup server name for cmd %s(%s) -- no server defined?", cmd, session_id); 
		return NULL;
	}

	fd_pool_set_node_key(conn, memc_name);

	do {
		enum sbalance_conn_status status;

		if (sockfd >= 0) {
			close(sockfd);
			status = SBCS_FAIL;
		} else {
			status = SBCS_START;
		}

		sockfd = fd_pool_get(conn, status, NULL, NULL);

		if (sockfd == -1) {
			log_printf(LOG_ERR, "(ERR) memcache_query fd_pool_get() failed for session_id/name '%s'/'%s', connection impossible.", session_id, memc_name);
			fd_pool_free_conn(conn);
			return NULL;
		}

		struct iovec iov[5];
		int i = 0;
		iov[i].iov_base = (char*)cmd;
		iov[i].iov_len = strlen(cmd);
		n_remaining += iov[i].iov_len;
		++i;
		if (session_id) {
			iov[i].iov_base = (char*)session_id;
			iov[i].iov_len = strlen(session_id);
			n_remaining += iov[i].iov_len;
			++i;
			iov[i].iov_base = (char*)"_";
			iov[i].iov_len = 1;
			n_remaining += iov[i].iov_len;
			++i;
		}
		iov[i].iov_base = (char*)keyval;
		iov[i].iov_len = strlen(keyval);
		n_remaining += iov[i].iov_len;
		++i;
		iov[i].iov_base = (char*)"\r\n";
		iov[i].iov_len = 2;
		n_remaining += iov[i].iov_len;
		++i;

		piov = iov;
		/* Consume the whole vector */
		while (n_written >= 0 && n_remaining > 0) {
			n_written = writev(sockfd, piov, i);
			n_remaining -= n_written;
			one_read_remaining = n_written;

			/* Handle partial writes, calculate the offset for the subsequent write */
			while (one_read_remaining >= (ssize_t)piov->iov_len) {
				one_read_remaining -= piov->iov_len;
				++piov;
				--i;
			}

			if (n_remaining > 0) {
				piov->iov_len -= one_read_remaining;
				piov->iov_base = (char *)piov->iov_base + one_read_remaining;
			}
		}
	} while (n_written <= 0);

	*fd = sockfd;
	return conn;
}

char *memcache_get(const char *session_id, const char *key, struct fd_pool *pool) {
	char header[1024];
	int state[2] = {0, 0};
	int sockfd;
	struct fd_pool_conn *conn = memcache_query("get ", session_id, key, pool, &sockfd);

	if (!conn)
		return NULL;

	if (fdgets(header, sizeof(header), state, sockfd)) {
		int size;
		int r;
		r = sscanf(header, "VALUE %*s %*u %u", &size);

		if (r == 1) {
			char *res = zmalloc(size + 2); /* Room for data plus \r\n */
			size_t remaining_sz = size + 2;
			int one_read = 0;

			while ((one_read >= 0) && (remaining_sz > 0)) {
				one_read = fdgets_read(res + size + 2 - remaining_sz, remaining_sz, header, state, sockfd);
				remaining_sz -= one_read;
			}

			if (one_read < 0) {
				log_printf(LOG_ERR, "%s Error reading data for key '%s'", __func__, key);
			}
			res[size] = 0;

			fdgets(header, sizeof(header), state, sockfd);
			if (strncmp(header, "END", 3) != 0) {
				xwarnx("%s Data remain for key: %s in memcache", __func__, key);
			}

			fd_pool_put(conn, sockfd);
			fd_pool_free_conn(conn);
			return res;
		}
		fd_pool_put(conn, sockfd);
	} else {
		close(sockfd);
	}

	fd_pool_free_conn(conn);

	return NULL;
}

int
memcache_set(const char *session_id, const char *key, const char *value, int expire, struct fd_pool *pool) {
	const char *arg;
	int vl = value ? strlen(value) : 0;
	char buf[256 + vl];
	char header[256];
	int res = 0;
	int state[2] = {0, 0};
	int sockfd;
	const char *cmd;

	if (value != NULL && strlen(value) > 0) {
		/* Note, memcached is case SENSITIVE wrt to cmd */
		cmd = "set ";
		snprintf (buf, 256 + vl, "%s 0 %d %lu \r\n%s", key, expire, (unsigned long)strlen(value), value);
		arg = buf;
	} else {
		cmd = "delete ";
		arg = key;
	}
	struct fd_pool_conn *conn = memcache_query(cmd, session_id, arg, pool, &sockfd);

	if (!conn)
		return -1;

	if (fdgets(header, sizeof(header), state, sockfd)) {
		if (strncmp(header, "STORED", 6) == 0 || strncmp(header, "DELETED", 7) == 0) {
			res = 1;
		}
		fd_pool_put(conn, sockfd);
	} else {
		close(sockfd);
	}

	fd_pool_free_conn(conn);

	return res;
}

int
memcache_incr(const char *session_id, const char *key, int value, struct fd_pool *pool) {
	char buf[256];
	char header[256];
	int res = 0;
	int state[2] = {0, 0};
	int sockfd;
	const char *cmd;

	/* Note, memcached is case SENSITIVE wrt to cmd */
	cmd = "incr ";
	snprintf(buf, 256, "%s %d\r\n", key, value);
	struct fd_pool_conn *conn = memcache_query(cmd, session_id, buf, pool, &sockfd);

	if (!conn)
		return -1;

	if (fdgets(header, sizeof(header), state, sockfd)) {
		if (strncmp(header, "NOT_FOUND", 9) == 0) {
			res = -1;
		} else {
			unsigned long l;

			if (sscanf(header, "%lu", &l) == 1) {
				if (l > INT_MAX)
					l = INT_MAX;
				res = l;
			} else {
				res = -1;
			}
		}
		fd_pool_put(conn, sockfd);
	} else {
		close(sockfd);
	}

	fd_pool_free_conn(conn);

	return res;
}

int
memcache_decr(const char *session_id, const char *key, int value, struct fd_pool *pool) {
	char buf[256];
	char header[256];
	int res = 0;
	int state[2] = {0, 0};
	int sockfd;
	const char *cmd;

	/* Note, memcached is case SENSITIVE wrt to cmd */
	cmd = "decr ";
	snprintf(buf, 256, "%s %d\r\n", key, value);
	struct fd_pool_conn *conn = memcache_query(cmd, session_id, buf, pool, &sockfd);

	if (!conn)
		return -1;

	if (fdgets(header, sizeof(header), state, sockfd)) {
		if (strncmp(header, "NOT_FOUND", 9) == 0) {
			res = -1;
		} else {
			unsigned long l;

			if (sscanf(header, "%lu", &l) == 1) {
				if (l > INT_MAX)
					l = INT_MAX;
				res = l;
			} else {
				res = -1;
			}
		}
		fd_pool_put(conn, sockfd);
	} else {
		close(sockfd);
	}

	fd_pool_free_conn(conn);

	return res;
}
