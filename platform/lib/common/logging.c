
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <dlfcn.h>

#include <execinfo.h>

#define SYSLOG_NAMES
#include <syslog.h>

#include <pthread.h>


#ifdef DEBUG_STDLIB
#include "debug_stdlib.h"
#endif

#include "logging.h"
#include "util.h"

static struct {
	int level;
} log = { -1 };
static pthread_key_t log_tsd;
static pthread_once_t log_key_once = PTHREAD_ONCE_INIT;

inline static const char*
level_name(int level) {
	switch(level) {
		case LOG_EMERG: return "EMERG";
		case LOG_ALERT: return "ALERT";
		case LOG_CRIT: return "CRIT";
		case LOG_ERR: return "ERR";
		case LOG_WARNING: return "WARNING";
		case LOG_NOTICE: return "NOTICE";
		case LOG_INFO: return "INFO";
		case LOG_DEBUG: return "DEBUG";
		default: return "";
	}
}

int
get_priority_from_level(const char* level, int default_priority) {
	if (level) {
		CODE *pri_code = prioritynames;
		while (pri_code->c_name) {
			if (strcmp(pri_code->c_name, level) == 0)
				return pri_code->c_val;
			++pri_code;
		}
	}
	return default_priority;
}

int
vlog_printf(int level, const char *fmt, va_list ap) {
	int res = 0;
	char *ptr;
	char *log_string = NULL;
	char *logfmt;

	if (level > log.level)
		return 0;

	log_string = pthread_getspecific(log_tsd);
	void (*hook_vsyslog)(int priority, const char *format, va_list ap) = dlsym(RTLD_DEFAULT, "sysloghook_vsyslog");

	if (log_string)
		ALLOCA_PRINTF(res, logfmt, "(%s : %s): %s", level_name(level), log_string, fmt);
	else
		ALLOCA_PRINTF(res, logfmt, "(%s): %s", level_name(level), fmt);

	if ((ptr = strchr(logfmt, '\r'))) {
		*ptr = '\0';
	}

	if (hook_vsyslog)
		hook_vsyslog(level, logfmt, ap);
	else
		vsyslog(level, logfmt, ap);

	return 1;
}

int
log_printf(int level, const char *fmt, ...) {
	va_list ap;

	va_start(ap, fmt);
	int res = vlog_printf(level, fmt, ap);
	va_end(ap);

	return res;
}

void
log_backtrace(int level, int skip) {
	void *btbuf[50];
	int btsiz = 50;
	int bti;
	char **bts;

	if (skip < -1)
		return;

	btsiz = backtrace(btbuf, btsiz);
	if (btsiz > 0) {
		bts = backtrace_symbols(btbuf, btsiz);
		for (bti = skip + 1 ; bti < btsiz ; bti++)
			log_printf(level, " bt: %s", bts[bti]);
		free(bts);
	}
}


static
void
key_free(void *ptr) {
	free(ptr);
}

static
void
log_thread_once(void) {
	pthread_key_create(&log_tsd, key_free);
}

void
log_register_thread(const char *fmt, ...) {
	char *old_log_string;
	char *log_string;
	va_list ap;

	old_log_string = pthread_getspecific(log_tsd);
	if (old_log_string)
		free(old_log_string);

	va_start(ap, fmt);
	if (vasprintf(&log_string, fmt, ap) == -1)
		;
	va_end(ap);

	pthread_setspecific(log_tsd, log_string);
}


int
log_level(void) {
	return log.level;
}

static int
log_setup_options(const char *appname, const char *level, int options) {

	pthread_once(&log_key_once, log_thread_once);

	void (*hook_openlog)(const char *ident, int option, int facility) = dlsym(RTLD_DEFAULT, "sysloghook_openlog");

	if (hook_openlog)
		hook_openlog(appname, options, LOG_LOCAL0);
	else
		openlog(appname, options, LOG_LOCAL0);

	log.level = get_priority_from_level(level, LOG_INFO);

	return 1;
}

int
log_setup(const char *appname, const char *level) {
	return log_setup_options(appname, level, 0);
}

int
log_setup_perror(const char *appname, const char *level) {
	return log_setup_options(appname, level, LOG_PERROR);
}

int
log_shutdown(void) {

	void (*hook_closelog)(void) = dlsym(RTLD_DEFAULT, "sysloghook_closelog");

	if (hook_closelog)
		hook_closelog();
	else
		closelog();
	return 1;
}

