#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <netdb.h>
#include <ctype.h>
#include "queue.h"
#include "util.h"
#include "transapi.h"
#include "vtree.h"
#include "logging.h"
#include <sys/poll.h>
#include "hash.h"
#include <sys/types.h>
#include <sys/time.h>
#include "bconf.h"
#include "fd_pool.h"

struct listpair_struct {
	TAILQ_ENTRY(listpair_struct) list;
	char *key;
	char *value;
	size_t size;
};
typedef struct listpair_struct listpair_t;

TAILQ_HEAD(listhead, listpair_struct);

#define TRANSACTION_BANNER_OK "220 Welcome."

struct transaction_struct {
	struct listhead request;
	struct buf_string text;
	struct listhead response;
	int sockfd;
	int timeout;
	char *buffer;
	int buffer_size;
	listpair_t *current_elem;
	struct hash_table *direct_cb_hash;
	char *line;
	void *cbdata;

	struct fd_pool *pool;
	struct fd_pool_conn *conn;
	int free_pool;
};

extern int h_errno;

/* Socket functions */
static int
connection_established(int sockfd, int timeout, int sock_flags, int checkwrite) {
	struct pollfd pfd;
	socklen_t sl;
	int error = 0;
	int n;

	memset(&pfd, 0, sizeof(pfd));

	pfd.fd = sockfd;
	pfd.events = POLLIN;
	if (checkwrite)
		pfd.events |= POLLOUT;

	do {
		n = poll(&pfd, 1, timeout);
		/* XXX we restart the timeout on EINTR. */
	} while (n == -1 && errno == EINTR);

	if (n > 0) {
		if ((pfd.revents & POLLIN) || (checkwrite && (pfd.revents & POLLOUT))) {
			sl = sizeof(error);

			if (getsockopt(sockfd, SOL_SOCKET, SO_ERROR, &error, &sl) < 0 || error) {
				if (error)
					errno = error;
				return -1;
			} else {
				fcntl(sockfd, F_SETFL, sock_flags);
				return 0;
			}
		}
	}

	return -1;
}

/*
	Read output into buffer

	Complicated by the fact that we want to fire callbacks as soon as possible,
	primarily for 'token'. For that to work we need to track blobs so that we
	don't use blob-data by mistake.
*/
static int
readall(transaction_t *trans) {
	int n = 0;
	int tot = 0;
	char *np;
	char *cp;
	int left_of_blob = 0;

	int sock_flags = fcntl(trans->sockfd, F_GETFL, 0);

	while (connection_established(trans->sockfd, trans->timeout, sock_flags, 0) == 0) {
		n = read(trans->sockfd, trans->buffer + tot, trans->buffer_size - tot);
		if (n == 0)
			break;
		if (n == -1)
			return -1;

		tot += n;

		if (trans->buffer_size - tot < 1024) {
			/* Increase buffer_size */
			size_t lineoff = trans->line - trans->buffer;
			trans->buffer = xrealloc(trans->buffer, trans->buffer_size * 2);
			trans->buffer_size *= 2;
			trans->line = trans->buffer + lineoff;
		}

		if (left_of_blob) {
			if (left_of_blob > n) {
				left_of_blob -= n;
				continue;
			} else {
				trans->line = trans->buffer + tot - n + left_of_blob;
				left_of_blob = 0;
			}
		}

		/* Check for keys with direct callback. */
		if (trans->direct_cb_hash) {
			while ((np = memchr(trans->line, '\n', trans->buffer + tot - trans->line))) {
				cp = memchr(trans->line, ':', np - trans->line);

				if (cp) {
					if (strncmp(trans->line, "blob:", 5) == 0 && sscanf(trans->line + 5, "%u:", &left_of_blob) == 1) {
						int left_in_buffer = trans->buffer + tot - np - 1;

						if (left_of_blob < left_in_buffer) {
							trans->line = np + left_of_blob + 1;
							left_of_blob = 0;
							/* Continue parsing lines */
							continue;
						} else {
							left_of_blob -= left_in_buffer;
							/* Read next block */
							break;
						}
					}

					trans_cb_t cb = hash_table_search(trans->direct_cb_hash, trans->line, cp - trans->line, NULL);
					if (cb) {
						cb(trans, trans->line, cp - trans->line, cp + 1, np - cp - 1, trans->cbdata);
					}
				}

				trans->line = np + 1;
			}
		}
	}

	return tot;
}

transaction_t *
trans_init_fd_pool(unsigned int timeout, struct fd_pool *pool) {
	transaction_t *trans;

	trans = xmalloc(sizeof(transaction_t));

	TAILQ_INIT(&trans->request);
	memset(&trans->text, 0, sizeof(trans->text));
	TAILQ_INIT(&trans->response);

	trans->buffer_size = 1024;
	trans->line = trans->buffer = xmalloc(trans->buffer_size);

	trans->timeout = timeout ?: (unsigned)fd_pool_timeout(pool);
	trans->direct_cb_hash = NULL;

	trans->pool = pool;
	trans->free_pool = 0;
	trans->conn = fd_pool_new_conn(pool, NULL, 0);

	return trans;
}

transaction_t *
trans_init_vtree(unsigned int timeout, struct bpapi_vtree_chain *vchain) {
	struct fd_pool *pool = fd_pool_create(vchain, 0, NULL);
	transaction_t *trans;

	if (!pool)
		return NULL;

	if (!timeout)
		timeout = vtree_getint(vchain, "timeout", NULL);

	trans = trans_init_fd_pool(timeout, pool);
	if (!trans)
		return NULL;

	trans->free_pool = 1;
	return trans;
}

transaction_t *
trans_init_host(unsigned int timeout, const char *hostname, const char *port) {
	struct fd_pool *pool = fd_pool_create_single(hostname, port, 0, timeout, NULL);
	transaction_t *trans;

	if (!pool)
		return NULL;

	trans = trans_init_fd_pool(timeout, pool);
	if (!trans)
		return NULL;

	trans->free_pool = 1;
	return trans;
}

static void
free_list(struct listhead *head, int freevals) {
	listpair_t *lp;

	while ((lp = TAILQ_FIRST(head)) != NULL) {
		TAILQ_REMOVE(head, lp, list);
		if (freevals) {
			free(lp->key);
			free(lp->value);
		}
		free(lp);
	}
}

void
trans_free(transaction_t *trans) {
	free_list(&trans->request, 1);
	free_list(&trans->response, 0);
	free(trans->buffer);
	if (trans->direct_cb_hash)
		hash_table_free(trans->direct_cb_hash);
	fd_pool_free_conn(trans->conn);
	if (trans->free_pool)
		fd_pool_free(trans->pool);
	free(trans->text.buf);
	free(trans);
}

static int
add_pair_to_response(transaction_t *trans, char *key, char *value, size_t size) {
	listpair_t *lp;

	lp = xmalloc(sizeof(listpair_t));

	lp->key = key;
	lp->value = value;
	lp->size = size;

	TAILQ_INSERT_TAIL(&trans->response, lp, list);

	return 1;
}

int
trans_add_pair(transaction_t *trans, const char *key, const char *value, ssize_t size) {
	listpair_t *lp;

	if (key == NULL || value == NULL)
		return 0;

	if (size <= 0)
		size = strlen(value);

	lp = xmalloc(sizeof(listpair_t));

	lp->key = xstrdup(key);
	lp->value = xmalloc(size+1);
	memcpy(lp->value, value, size);
	lp->value[size] = '\0';
	lp->size = size;

	TAILQ_INSERT_TAIL(&trans->request, lp, list);

	return 1;
}

void
trans_add_text(transaction_t *trans, const char *text, size_t len) {
	bswrite(&trans->text, text, len);
}

int
trans_add_printf(transaction_t *trans, const char *fmt, ...) {
	va_list ap;

	va_start(ap, fmt);
	int res = vbscat(&trans->text, fmt, ap);
	va_end(ap);
	return res;
}

void
trans_set_cbdata(transaction_t *trans, void *cbdata) {
	trans->cbdata = cbdata;
}

int
trans_add_callback(transaction_t *trans, const char *key, trans_cb_t cb) {
	if (!trans->direct_cb_hash) {
		trans->direct_cb_hash = hash_table_create(1, NULL);
		hash_table_free_keys(trans->direct_cb_hash, 1);
	}
	hash_table_insert(trans->direct_cb_hash, xstrdup(key), -1, cb);
	return 1;
}

static int
get_response(transaction_t *trans) {
	struct pollfd pfd;
	int n = 0;
	int res;

	memset(&pfd, 0, sizeof(pfd));

	pfd.fd = trans->sockfd;
	pfd.events = POLLIN;

	do {
		res = poll(&pfd, 1, trans->timeout);
		/* XXX we restart the timeout on EINTR. */
	} while (res == -1 && errno == EINTR);

	if (res > 0) {
		if ((n = readall(trans)) <= 0) {
			return TRANS_COMMIT_ERROR_IO;
		}

		trans->buffer[n] = '\0';
	} else {
		if (res == 0)
			return TRANS_COMMIT_ERROR_TIMEOUT;
		else
			return TRANS_COMMIT_ERROR_IO;
	}

	return n;
}

static int
send_request(transaction_t *trans) {
	listpair_t *lp;
	int len;
	int needed;

	TAILQ_FOREACH(lp, &trans->request, list) {
		needed = strlen(lp->key) + lp->size + 3;
		if (trans->buffer_size < needed) {
			size_t lineoff = trans->line - trans->buffer;

			trans->buffer = xrealloc(trans->buffer, trans->buffer_size + needed + 1024);
			trans->buffer_size += (needed + 1024);
			trans->line = trans->buffer + lineoff;
		}

		if (strncmp(lp->key, "blob:", 5) == 0) {
			/*  Concat key and value with new line */
			len = snprintf(trans->buffer, trans->buffer_size, "%s\n", lp->key);
			memcpy(&trans->buffer[len], lp->value, lp->size);
			len += lp->size;
			trans->buffer[len++] = '\n';
		} else {
			len = snprintf(trans->buffer, trans->buffer_size, "%s:%s\n", lp->key, lp->value);
		}

		if (write(trans->sockfd, trans->buffer, len) != len) {
			return TRANS_COMMIT_ERROR_IO;
		}
	}

	if (trans->text.pos) {
		if (write(trans->sockfd, trans->text.buf, trans->text.pos) != trans->text.pos)
			return TRANS_COMMIT_ERROR_IO;
	}
	if (write(trans->sockfd, "end\n", 4) == -1)
		;

	return TRANS_COMMIT_OK;
}

static int
parse_response(transaction_t *trans, int len) {
	char *current_line = trans->buffer;
	char *end = trans->buffer + len;
	int size;

	while (current_line < end) {
		char *lend = current_line;
		char *ptr = NULL;

		while (lend < end && *lend != '\r' && *lend != '\n') {
			if (*lend == ':' && !ptr)
				ptr = lend;
			lend++;
		}
		if (lend >= end)
			break;

		*lend++ = '\0';

		if (strcmp(current_line, "end") == 0) {
			trans->current_elem = TAILQ_FIRST(&trans->response);
			return TRANS_COMMIT_OK;
		}

		/* current_line is "key:value" */

		if (ptr) {
			*ptr++ = '\0';
			size = lend - ptr - 1;

			/* Check if is blob */
			if (strcmp("blob", current_line) == 0) {
				size = atoi(ptr);

				if ((ptr = strchr(ptr, ':')) == NULL)
					continue;

				ptr++;

				/* name */
				current_line = ptr;

				/* value */
				ptr = lend;

				/* lend jump over blob */
				lend += size;

				if (lend >= end)
					break;

				if (*lend != '\r' && *lend != '\n')
					return TRANS_COMMIT_ERROR_BLOB_NO_NL;

				*lend++ = '\0';
			}

			add_pair_to_response(trans, current_line, ptr, size);
		}

		while (lend < end && (*lend == '\n' || *lend == '\r'))
			lend++;

		current_line = lend;
	}

	return TRANS_COMMIT_ERROR_EOF;
}

int
trans_commit(transaction_t *trans) {
	int res;
	char buf[256];
	int ret = 0;
	enum sbalance_conn_status status = SBCS_START;

	do {
		struct pollfd pfd;
		struct timeval ts, te;
		int td;
		int timeout;
		int l;

		gettimeofday(&ts, NULL);
		trans->sockfd = fd_pool_get(trans->conn, status, NULL, NULL);
		if (trans->sockfd == -1)
			return TRANS_COMMIT_ERROR_CONNECT; /* XXX switch(errno) here. */
		gettimeofday(&te, NULL);
		td = (te.tv_sec - ts.tv_sec)*1000 + (te.tv_usec - ts.tv_usec)/1000;
		timeout = trans->timeout - td;
		timeout = timeout > 0 ? timeout : 0;

		pfd.fd = trans->sockfd;
		pfd.events = POLLIN;

		do {
			res = poll(&pfd, 1, timeout);
			/* XXX we restart the timeout on EINTR. */
		} while (res == -1 && errno == EINTR);

		if (res != 1 || (l = read(trans->sockfd, buf, sizeof(TRANSACTION_BANNER_OK) - 1)) <= 0) {
			log_printf(LOG_ERR, "Connect failed: No banner from trans");
			status = SBCS_FAIL;
			close(trans->sockfd);
			trans->sockfd = -1;
		} else if (l != sizeof(TRANSACTION_BANNER_OK) -1 || memcmp(buf, TRANSACTION_BANNER_OK, sizeof(TRANSACTION_BANNER_OK) - 1)) {
			close(trans->sockfd);
			trans->sockfd = -1;
			log_printf(LOG_ERR, "Connect failed: Trans busy");
			ret = TRANS_COMMIT_ERROR_BUSY;
			status = SBCS_TEMPFAIL;
		}
	} while (trans->sockfd == -1);

	if (trans->sockfd < 0) {
		return ret;
	}

	if ((res = send_request(trans)) == TRANS_COMMIT_OK && (res = get_response(trans)) >= 0) {
		res = parse_response(trans, res);
	}

	close(trans->sockfd);

	return res;
}

char*
trans_get_key(transaction_t *trans, const char *key) {
	listpair_t *lp;

	TAILQ_FOREACH(lp, &trans->response, list) {
		if (strcmp(lp->key, key) == 0)
			return xstrdup(lp->value);
	}

	return NULL;
}

int
trans_get_response_size(transaction_t *trans) {
	int n = 0;
	listpair_t *lp;

	TAILQ_FOREACH(lp, &trans->response, list) {
		n++;
	}

	return n;
}

int
trans_iter_next(transaction_t *trans, char **key, char **value) {
	if (trans->current_elem == NULL)
		return 0;

	*key = trans->current_elem->key;
	*value = trans->current_elem->value;

	trans->current_elem = TAILQ_NEXT(trans->current_elem, list);

	return 1;
}


void
trans_iter_reset(transaction_t *trans) {
	trans->current_elem = TAILQ_FIRST(&trans->response);
}

void
trans_iter_reset_to_request(transaction_t *trans) {
	trans->current_elem = TAILQ_FIRST(&trans->request);
}

const char *
trans_strerror(int error) {
	switch (error) {
	case TRANS_COMMIT_OK:
		return "no error";
	case TRANS_COMMIT_ERROR_LOOKUP:
		return "getaddrinfo() failed";
	case TRANS_COMMIT_ERROR_SOCKET:
		return "socket() failed";
	case TRANS_COMMIT_ERROR_GETFL:
		return "fcntl() F_GETFL failed";
	case TRANS_COMMIT_ERROR_SETFL:
		return "fcntl() F_SETFL failed";
	case TRANS_COMMIT_ERROR_CONNECT:
		return "failed to connect";
	case TRANS_COMMIT_ERROR_BUSY:
		return "transaction handler busy";
	case TRANS_COMMIT_ERROR_IO:
		return "I/O error";
	case TRANS_COMMIT_ERROR_TIMEOUT:
		return "transaction timeout";
	case TRANS_COMMIT_ERROR_EOF:
		return "unexpected EOF";
	case TRANS_COMMIT_ERROR_BLOB_NO_NL:
		return "blob not followed by newline";
	default:
		return "unknown error";
	}
}

