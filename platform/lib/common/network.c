
#include "network.h"

#include <netdb.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <stdlib.h>

#include "event.h"
#include "util.h"

struct net_connect
{
	int tos;
	int timeout;
	void (*cb)(int fd_res, void *cbdata);
	void *cbdata;

	struct addrinfo *ai_res;
	struct addrinfo *ai_curr;

	struct event_base *base;
	struct event ev;
};

/*
 * Lookup host and port.
 * Sets ai_res and return 0 if successful, else sets status and return -1.
 */
static int
net_lookup(struct net_connect *net, int socktype, const char *host, const char *port) {
	struct addrinfo hints = {0};
	struct addrinfo *res;

	hints.ai_socktype = socktype;

	if (getaddrinfo(host, port, &hints, &res) || !res) {
		return -1;
	}

	net->ai_res = res;
	net->ai_curr = NULL;
	return 0;
}

static int
net_connect_next(struct net_connect *net, int tos) {
	struct addrinfo *curr;
	int fd;

	if (net->ai_curr)
		curr = net->ai_curr = net->ai_curr->ai_next;
	else
		curr = net->ai_res;

	for (; curr; curr = curr->ai_next) {
		fd = socket(curr->ai_family, curr->ai_socktype, curr->ai_protocol);

		if (fd < 0) {
			continue;
		}

		/* Set nonblocking. */
		if (fcntl(fd, F_SETFL, O_NONBLOCK) < 0) {
			close(fd);
			continue;
		}
		if (fcntl(fd, F_SETFD, FD_CLOEXEC) < 0) {
			close(fd);
			continue;
		}

		/* Start connect. */
		if (connect(fd, curr->ai_addr, curr->ai_addrlen) < 0 && errno != EINPROGRESS) {
			close(fd);
			continue;
		}

		if (tos)
			setsockopt(fd, IPPROTO_IP, IP_TOS, &tos, sizeof(tos));

		net->ai_curr = curr;
		return fd;
	}

	return -1;
}

static void
net_connect_done(int fd, short what, void *cbdata) {
	struct net_connect *net = cbdata;

	if (what & EV_TIMEOUT) {
		close(fd);
		fd = NET_TIMEOUT_CONNECT;
	} else {
		int err;
		socklen_t errlen = sizeof(err);

		getsockopt(fd, SOL_SOCKET, SO_ERROR, &err, &errlen);
		if (err) {
			close(fd);
			fd = NET_ERROR_CONNECT;
		}
	}
	if (fd < 0) {
		int nfd = net_connect_next(net, net->tos);

		if (nfd >= 0) {
			struct timeval tv;

			event_set(&net->ev, nfd, EV_WRITE, net_connect_done, net);
			if (net->base)
				event_base_set(net->base, &net->ev);
			tv.tv_sec = net->timeout;
			tv.tv_usec = 0;
			event_add(&net->ev, &tv);
			return;
		}
	}
	net->cb(fd, net->cbdata);
	if (net->ai_res)
		freeaddrinfo(net->ai_res);
	free(net);
}

int
net_connect_host_port(struct event_base *base, int socktype, int tos, int timeout, const char *host, const char *port,
		void (*cb)(int fd_res, void *cbdata), void *cbdata) {
	struct net_connect *res = zmalloc(sizeof (*res));
	struct timeval tv;
	int fd;

	res->base = base;
	res->tos = tos;
	res->timeout = timeout;
	res->cb = cb;
	res->cbdata = cbdata;

	if (net_lookup(res, socktype, host, port)) {
		free(res);
		return -1;
	}
	fd = net_connect_next(res, tos);
	if (fd < 0) {
		if (res->ai_res)
			freeaddrinfo(res->ai_res);
		free(res);
		return -1;
	}

	event_set(&res->ev, fd, EV_WRITE, net_connect_done, res);
	if (res->base)
		event_base_set(res->base, &res->ev);
	tv.tv_sec = res->timeout;
	tv.tv_usec = 0;
	event_add(&res->ev, &tv);
	return 0;
}

ssize_t
get_iovlen_sum(const struct iovec *iov, int iovcnt) {
	ssize_t total = 0;

	for (int i=0 ; i < iovcnt ; ++i) {
		total += iov[i].iov_len;
	}

	return total;
}

int writev_retry(int fd, struct iovec *iov, int iovcnt, ssize_t iovlen_sum) {
	ssize_t n_total = iovlen_sum;

	if (n_total < 0)
		n_total = get_iovlen_sum(iov, iovcnt);

	struct iovec *piov = iov;
	struct iovec old_iov = { 0 };
	int old_iov_idx = -1;
	int num_iov_left = iovcnt;
	int hwm_idx = 0;	/* high-water marks */
	ssize_t hwm_total = 0;
	ssize_t n_remaining = n_total;

	/* Write to the fd while there's data left to write, or we detect that we make no progress or get an error */
	while (n_remaining > 0) {
		ssize_t res = writev(fd, piov, num_iov_left);

		/* Restore the iov modified last iteration, if any */
		if (res && old_iov_idx > -1) {
			iov[old_iov_idx] = old_iov;
			old_iov_idx = -1;
		}

		if (res < 0) {
			/* Something went wrong writing to the fd */
			return -1;
		} 

		n_remaining -= res;

		/* Partial non-empty write: advance piov and adjust num_iov_left for next attempt */
		if (res && n_remaining > 0) {
			ssize_t n_left = n_total - n_remaining - hwm_total;
			for (int i=hwm_idx ; i < iovcnt ; ++i) {
				if ((ssize_t)iov[i].iov_len > n_left) {
					/* Backup iov */
					old_iov = iov[i];
					old_iov_idx = i;

					/* Update iov */
					iov[i].iov_base = (char*)iov[i].iov_base + n_left;
					iov[i].iov_len -= n_left;

					num_iov_left = iovcnt - i;
					piov = &iov[i];

					hwm_idx = i;
					break;
				}
				n_left -= iov[i].iov_len;
				hwm_total += iov[i].iov_len;
			}
		}
	}

	return 0;
}

