#ifndef BCONFD_CLIENT_H
#define BCONFD_CLIENT_H

#include <curl/curl.h>
#include "macros.h"
#include "queue.h"

#define STORAGE_DIGEST_STRING_LENGTH (16 + 1)
#define CHECKSUM_HEADER_LENGTH STORAGE_DIGEST_STRING_LENGTH

enum proto {
	PROTO_TRANS,
	PROTO_JSON,
	MAX_PROTO
};

struct bconf_node;
struct bconfd_call {
	struct bconf_node *bconf;
	enum proto proto;
	const char *cmd;
	const char *errstr;
	char checksum[CHECKSUM_HEADER_LENGTH];
	void *is;
};

int bconfd_load(struct bconfd_call *call, struct bconf_node *root) NONNULL_ALL;
int bconfd_load_with_file(struct bconfd_call *call, const char *config_file) NONNULL_ALL;
int bconfd_add_request_param(struct bconfd_call *call, const char *key, const char *value) NONNULL(1);

#endif

