
struct stringpool *stringpool_new(void);
void stringpool_free(struct stringpool *pool);

const char *stringpool_get(struct stringpool *pool, const char *str);

