#ifndef COMMON_UNICODE_H
#define COMMON_UNICODE_H
 
#include <unicode/ucnv.h>

#ifdef __cplusplus
extern "C" {
#endif

/*
 * Decode a UTF-8 string using the converters.
 * src and dst can be the same pointer.
 */
char *utf8_decode(char *dst, int *dstlen, const char *str, int slen, UConverter *utf8_cnv, UConverter *dst_cnv);

/*
 * Calculate the length (number of chars) of a UTF-8 string
 */
size_t strlen_utf8(const char *str);

/*
 * Simple encoder and decoder. Returned string is allocated and needs to be free()-ed.
 *
 * Please notice that this isn't strictly ISO-8859-1, but rather Windows-1252, see the comment
 * above the function implementations for the reason why.
 */
char *latin1_to_utf8(const char *src, int slen, int *dstlen);
char *utf8_to_latin1(const char *src);

/*
 * Encode string from src to dst. Returning the number of converted
 * latin-1 chars and if outlen is non NULL the length of the UTF-8 string.
 */
size_t latin1_to_utf8_buf(size_t *outlen, const char *src, size_t slen,
    char *dst, size_t dlen);

#ifdef __cplusplus
}
#endif

#endif /*COMMON_UNICODE_H*/
