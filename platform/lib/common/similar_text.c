/*
   +----------------------------------------------------------------------+
   | PHP Version 5                                                        |
   +----------------------------------------------------------------------+
   | Copyright (c) 1997-2007 The PHP Group                                |
   +----------------------------------------------------------------------+
   | This source file is subject to version 3.01 of the PHP license,      |
   | that is bundled with this package in the file LICENSE, and is        |
   | available through the world-wide-web at the following url:           |
   | http://www.php.net/license/3_01.txt                                  |
   | If you did not receive a copy of the PHP license and are unable to   |
   | obtain it through the world-wide-web, please send a note to          |
   | license@php.net so we can mail you a copy immediately.               |
   +----------------------------------------------------------------------+
   | Authors: Rasmus Lerdorf <rasmus@php.net>                             |
   |          Stig S�ther Bakken <ssb@php.net>                            |
   |          Zeev Suraski <zeev@zend.com>                                |
   +----------------------------------------------------------------------+
 */

#include "similar_text.h"
#include <math.h>
#include <string.h>
#include <unicode/utf8.h>
#include <unicode/uchar.h>

static
int u8strlen(const char* buf) {
	UChar32 ch = 0;
	int i = 0;
	int buf_size = strlen(buf);
	int slen = 0;
	int chsz = 1;
	if (buf_size) {
		for (; i < buf_size && chsz; ++slen) {
			U8_NEXT_UNSAFE(buf, i, ch);
			chsz = U8_LENGTH(ch);
		}
	}
	return slen;
}

/* {{{ php_similar_str
 */
static void php_similar_str(const char *txt1, int len1, const char *txt2, int len2, int *pos1, int *pos2, int *max, int *max_bytes, int is_utf8) {
	char *end1 = (char *) txt1 + len1;
	char *end2 = (char *) txt2 + len2;
	int l;
	UChar32 cp, cq, lcp, lcq;
	int ip = 0;
	int iq = 0;
	int lp, lq;
	int lsz;
	int cpp, cqp;
	int clp, clq;

	*max = 0;
	*max_bytes = 0;
	while (txt1 + ip < end1) {
		cpp = ip;
		if (is_utf8) {
     			U8_NEXT_UNSAFE(txt1, ip, cp);
		} else {
			cp = txt1[ip++];
		}
		iq = 0;
		while (txt2 + iq < end2) {
			cqp = iq;
			if (is_utf8) {
				U8_NEXT_UNSAFE(txt2, iq, cq);
			} else {
				cq = txt2[iq++];
			}
			lsz = 0;
			lp = ip;
			lq = iq;
			lcp = cp;
			lcq = cq;
			clp = cpp;
			clq = cqp;
			for (l = 0; lcp == lcq && txt1 + clp < end1 && txt2 + clq < end2; ++l) {
				clp = lp;
				clq = lq;
				if (is_utf8) {
					lsz += U8_LENGTH(lcp);
					U8_NEXT_UNSAFE(txt1, lp, lcp);
					U8_NEXT_UNSAFE(txt2, lq, lcq);
				} else {
					lsz++;
					lcp = txt1[lp++];
					lcq = txt2[lq++];
				}
			}
			if (l > *max) {
				*max = l;
				*max_bytes = lsz;
				*pos1 = cpp;
				*pos2 = cqp;
			}
		}
	}
}
/* }}} */

/* {{{ php_similar_char
 */
static int php_similar_char(const char *txt1, int len1, const char *txt2, int len2, int is_utf8)
{
	int sum;
	int pos1=0, pos2=0, max, max_bytes;

	php_similar_str(txt1, len1, txt2, len2, &pos1, &pos2, &max, &max_bytes, is_utf8);
	if ((sum = max)) {
		if (pos1 && pos2) {
			sum += php_similar_char(txt1, pos1,
						txt2, pos2,
						is_utf8);
		}
		if ((pos1 + max_bytes < len1) && (pos2 + max_bytes < len2)) {
			sum += php_similar_char(txt1 + pos1 + max_bytes, len1 - pos1 - max_bytes,
						txt2 + pos2 + max_bytes, len2 - pos2 - max_bytes,
						is_utf8);
		}
	}

	return sum;
}
/* }}} */

/* Calculates the similarity between two strings, expressed in percent. */
int
similar_text(const char *s1, const char *s2, int is_utf8) {
	int sim;
	int s1len;
	int s2len;

	if (!s1 || !s2 || (!*s1 && !*s2))
		return 0;

	s1len = strlen(s1);
	s2len = strlen(s2);
	sim = php_similar_char(s1, s1len, s2, s2len, is_utf8);

	if (is_utf8)
		return rintf(sim * 200.0 / (u8strlen(s1) + u8strlen(s2)));
	else
		return rintf(sim * 200.0 / (s1len + s2len));
}

