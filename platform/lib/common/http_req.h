#ifndef HTTP_REQ_H
#define HTTP_REQ_H

#include "tls.h"

struct event_base;

struct request {
	/* Public part */
	int current_server;
	int servers;
	const char *server;
	const char *port;

	struct tls_context *tls_ctx; /* Optional, https if set. */
	int tls_option_noVerifyPeer;

	char *header;
	int hsize;
	int header_sent;
	const char *requestdata;
	size_t reqsize;
	void (*cb_done)(struct request*);
	int timeout;
	struct evbuffer *databuf;

	const char *dstfile;
	char *buffer;
	int read;

	/* Private part */
	ssize_t resid;
	int result;
	struct event ev_read;
	struct event ev_write;
	int fd;
	int dstfd;
	void *data;
	struct event_base *event_base; /* needs to be set up to point to the correct event base (likely cs->base) by client code */
	struct tls *tls;
	int tcp_nodelay;	/* set to 1 to disable nagle algorithm */
#define RESULT_TIMEOUT		1001
#define RESULT_BADSOCKET	1002
#define RESULT_DISCONNECT	1003
#define RESULT_WRITEERR		1004
#define RESULT_READERR		1005
#define RESULT_SHORTREAD	1006
#define RESULT_PROTOERR		1007
#define RESULT_BADCONNECT	1008
#define RESULT_GETADDRINFO	1009
#define RESULT_TLS_BADCONNECT	1010
#define RESULT_TLS_BAD_CN	1011
#define RESULT_OPENFILE         1012
#define RESULT_WRONGDATA        1013

#define READ_BUFSIZE           65536
};

int http_req(struct request *req);
const char* http_req_result(int status);

#endif
