#ifndef SOCK_UTIL_H
#define SOCK_UTIL_H
#include <sys/types.h>

#include "macros.h"

#ifdef __cplusplus
extern "C" {
#endif

int create_socket(const char *host, const char *port);
int create_socket_unix(const char *);

/* Return malloced string with port number in *port */
int create_socket_any_port(const char *host, char **port) NONNULL(2);

size_t readline(int fd, char *vptr, size_t n);

/* Calls getsockname then getnameinfo. Return getnameinfo result. */
int get_local_port(int s, char *pbuf, size_t pbuflen, int gni_flags);

#ifdef __cplusplus
}
#endif

#endif
