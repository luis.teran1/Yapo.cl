#ifndef _ATOMIC_H
#define _ATOMIC_H

static __inline int
x86_atomic_cas_int(volatile int *ptr, int expect, int set)
{
	int res;
	__asm volatile("lock cmpxchgl %2, %1" : "=a" (res), "=m" (*ptr)
			: "r" (set), "a" (expect), "m" (*ptr) : "memory");
	return (res);
}

static __inline u_long
x86_atomic_cas_ul(volatile u_long *ptr, u_long expect, u_long set)
{
	u_long res;
	__asm volatile("lock cmpxchgl %2, %1" : "=a" (res), "=m" (*ptr)
			: "r" (set), "a" (expect), "m" (*ptr) : "memory");
	return (res);
}

static __inline void *
x86_atomic_cas_ptr(void *ptr, void *expect, void *set)
{
	return (void*)x86_atomic_cas_ul((u_long *)ptr, (u_long)expect, (u_long)set);
}

#define atomic_cas_int x86_atomic_cas_int
#define atomic_cas_ptr x86_atomic_cas_ptr

#endif /*_I386_ATOMIC_H*/
