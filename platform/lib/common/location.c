#include "location.h"

/*
 * Convert WGS84 to RT90
 *
 * Arguments:
 *		phi    ( Lat in decimal )
 *		lambda ( Lon in decimal )
 *
 * Returns:
 *		X
 *		Y
 *
 * Example:	Lat N 56� 13' 7" Lon E 15� 15' 50"
 *		Lat/Lon:	56.2188, 15.2640
 *		    X/Y:	6226924.358894, 1466404.585607
 *		
 */
void wgs84_to_rt90(struct location *l) {
	double phi = l->wgs84_lat;
	double lambda = l->wgs84_lon;

	/* RT 90 */
	double a = 6377397.155;
	double f = 1/299.1528128;

	/* Contants */
	double PI = 3.1415926535898;
	double lambda0 = 15.80628453 * PI / 180; /* 15� 48' 22.624306" */ 

	double k0 = 1.00000561024;
	double FN = -667.711;
	double FE = 1500064.274;

	double e2 = f * (2 - f);
	double n = f / (2 - f);
	double aa = a / (1 + n) * (1 + 1/4 * pow(n, 2.0) + 1/64 * pow(n, 4.0));

	double beta1 = 1/2 * n - 2/ 3 * pow(n, 2.0) + 5/16 * pow(n, 3.0) + 41/180 * pow(n, 4.0);
	double beta2 = 13/48 * pow(n, 2.0) - 3/5 * pow(n, 3.0) + 557/1440 * pow(n, 4.0);
	double beta3 = 61/240 * pow(n, 3.0) - 103/140 * pow(n, 4.0);
	double beta4 = 49561/161280 * pow(n, 4.0);

	double A = e2;
	double B = 1/6 * (5 * pow(e2, 2.0) - pow(e2, 3.0));
	double C = 1/120 * (104 * pow(e2, 3.0) - 45 * pow(e2, 4.0));
	double D = 1/1260 * (1237 * pow(e2, 4.0));

	/* Translate */
	lambda = lambda * PI / 180;
	phi = phi * PI / 180;

	double phix = phi - sin(phi) * cos(phi) * (A +
						   B * pow(sin(phi), 2.0) +
						   C * pow(sin(phi), 4.0) +
						   D * pow(sin(phi), 6.0));
	double dlambda = lambda - lambda0;

	double xip = atan2(tan(phix), cos(dlambda));
	double etap = atanh(cos(phix) * sin(dlambda));

	double x = k0 * aa * (xip + 
			  beta1 * sin(2 * xip) * cosh(2 * etap) +
			  beta2 * sin(4 * xip) * cosh(4 * etap) +
			  beta3 * sin(6 * xip) * cosh(6 * etap) +
			  beta4 * sin(8 * xip) * cosh(8 * etap)) + FN;
	double y = k0 * aa * (etap +
			  beta1 * cos(2 * xip) * sinh(2 * etap) +
			  beta2 * cos(4 * xip) * sinh(4 * etap) +
			  beta3 * cos(6 * xip) * sinh(6 * etap) +
			  beta4 * cos(8 * xip) * sinh(8 * etap)) + FE;

	l->rt90_x = x;
	l->rt90_y = y;
}

/*
 * Convert WGS84 to UTM
 *
 * Arguments:
 *		phi    ( Lat in decimal )
 *		lambda ( Lon in decimal )
 *
 * Returns:
 *		X
 *		Y
 *
 * Example:	Lat N 56� 13' 7" Lon E 15� 15' 50"
 *		Lat/Lon:	45.4773, 9.1815
 *		    X/Y:	514185.71, 5035990.70
 *		
 */

#define a   6378137  //equatorial radius
#define b   6356752.31  //polar radius
#define k0  0.9996   //scale factor

#include <stdio.h> //just for debugging
void wgs84_to_utm(struct location *l) {
	double lat = l->wgs84_lat * M_PI / 180;
	double lon = l->wgs84_lon;
    //The original formula doesn't seem to work in all zones. While it seems fine if I set always the first zone that is 32.
    //int zone = (int)(lon/6)+31;
    int zone = 32;
    int zone_cm = 6*zone-183;
    float delta_lon = lon - zone_cm;
    float p = delta_lon * M_PI / 180;

    //Datum constants, could be replaced with #define?
    double f    = (a - b)/a; //flattening
    double invf = 1/f; //inverse flattening
    double rm   = sqrt(a*b); //mean radius
    //eccentricity
    double e  = sqrt(1-pow(b/a, 2.0));
    double e1sq = e*e/(1-e*e);
    double n  = (a-b)/(a+b);
    //--------------------------------

    double rho = a * (1-e*e)/pow(1-pow(e * sin(lat),2.0),1.5);
    double nu  = a / sqrt(1-pow(e*sin(lat),2));

    //Calculate Meridional Arc Length
    double A0 = a*(1-n+(5*n*n/4)*(1-n)+(81*pow(n,4)/64)*(1-n));
    double B0 = (3*a*n/2)*(1-n-(7*n*n/8)*(1-n)+55*pow(n,4)/64);
    double C0 = (15*a*n*n/16)*(1-n+(3*n*n/4)*(1-n));
    double D0 = (35*a*pow(n,3)/48)*(1-n+11*n*n/16);
    double E0 = (315*a*pow(n,4)/51)*(1-n);
    double S  = A0*lat-B0*sin(2*lat)+C0*sin(4*lat)-D0*sin(6*lat)+E0*sin(8*lat);

    //Coefficients for UTM Coordinates
    double Ki   = S*k0;
    double Kii  = nu*sin(lat)*cos(lat)*k0/2;
    double Kiii = ((nu*sin(lat)*pow(cos(lat),3))/24)*(5-pow(tan(lat),2)+9*e1sq*pow(cos(lat),2)+4*pow(e1sq,2)*pow(cos(lat),4))*k0;
    double Kiv  = nu*cos(lat)*k0;
    double Kv   = pow(cos(lat),3)*(nu/6)*(1-pow(tan(lat),2)+e1sq*pow(cos(lat),2))*k0;
    //double A6   =

    //Final result!
    l->rt90_x = 500000+(Kiv*p+Kv*pow(p,3));
    l->rt90_y = Ki+Kii*p*p+Kiii*pow(p,4);

    //DEBUGGING
    fprintf(stdout, "lat = %f\n", l->wgs84_lat);
    fprintf(stdout, "lon = %f\n", lon);
    fprintf(stdout, "zone  = %i\n", zone );
    fprintf(stdout, "zone cm = %i\n", zone_cm );
    fprintf(stdout, "delta_lon = %f\n", delta_lon );
    fprintf(stdout, "p (rad) = %f\n", p );

    fprintf(stdout, "lat_radiant = %f\n", lat);
    fprintf(stdout, "f = %f\n", f);
    fprintf(stdout, "1/f = %f\n", invf);
    fprintf(stdout, "rm = %f\n", rm);
    fprintf(stdout, "e = %f\n", e);
    fprintf(stdout, "e'2 = %f\n", e1sq);
    fprintf(stdout, "n = %f\n", n);
    fprintf(stdout, "rho = %f\n", rho);
    fprintf(stdout, "nu = %f\n", nu);

    fprintf(stdout, "A0 = %f\n", A0);
    fprintf(stdout, "B0 = %f\n", B0);
    fprintf(stdout, "C0 = %f\n", C0);
    fprintf(stdout, "D0 = %f\n", D0);
    fprintf(stdout, "E0 = %f\n", E0);
    fprintf(stdout, "S  = %f\n", S );

    fprintf(stdout, "Ki   = %f\n", Ki   );
    fprintf(stdout, "Kii  = %f\n", Kii  );
    fprintf(stdout, "Kiii = %f\n", Kiii );
    fprintf(stdout, "Kiv  = %f\n", Kiv  );
    fprintf(stdout, "Kv   = %f\n", Kv   );

    fprintf(stdout, "\nUTM X = %f\n", l->rt90_x  );
    fprintf(stdout, "UTM Y = %f\n", l->rt90_y  );
}
