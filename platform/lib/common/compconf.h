#ifndef COMMON_COMPCONF_H
#define COMMON_COMPCONF_H

#include <stdarg.h>

struct compconf_lookup {
	const char *key;
	int idx;
};

struct compconf_node {
	const char *key;
	const struct compconf_lookup *(*lookup_func)(const char *str, unsigned int len);
	const char *value;
	const struct compconf_node *children;
	unsigned int nchildren;
	const struct compconf_node *star;
};

const struct compconf_node *compconf_get(const struct compconf_node *root, const char *key);
const struct compconf_node *compconf_vget(const struct compconf_node *root, ...);
const struct compconf_node *compconf_vnget(const struct compconf_node *root, int args, va_list ap);
const struct compconf_node *compconf_byname(const struct compconf_node *root, const char *key, const char *index);
const struct compconf_node *compconf_byindex(const struct compconf_node *root, const char *key, int index);
int compconf_count(const struct compconf_node *node);
const char *compconf_value(const struct compconf_node *node);
const char *compconf_key(const struct compconf_node *node);
const char *compconf_get_string(const struct compconf_node *root, const char *key);
int compconf_get_int(const struct compconf_node *root, const char *key);
int compconf_vget_int(const struct compconf_node *root, ...);
int compconf_in_list(const char *value, const char *path, const struct compconf_node *root);
void compconf_json(const struct compconf_node *n, int depth, int (*pf)(void *, int, const char *, ...), void *cbdata);

#endif /*COMMON_COMPCONF_H*/
