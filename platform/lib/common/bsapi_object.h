
#include "bsapi.h"

#include <stdbool.h>

struct bsapi_object;

struct bsapi_object_info
{
	const char *key;
	int nvals;
	const char *value[];
};

struct bsapi_object_keyval
{
	const char *key;
	const char *value;
};

struct bsapi_object *bsapi_object_new(struct bsconf *bsconf, const char *remote_addr);
struct bsapi_object *bsapi_object_new_vtree(struct bpapi_vtree_chain *vt, const char *remote_addr);
void bsapi_object_free(struct bsapi_object *);

//void bsapi_object_use_conf(struct bsapi_object *, bool flag);

int bsapi_object_search(struct bsapi_object *, const char *query);

const struct bsapi_object_info *bsapi_object_get_info(struct bsapi_object *, const char *key, int idx);
const struct bsapi_object_info *bsapi_object_info_idx(struct bsapi_object *, int idx);
const struct bsapi_object_info *bsapi_object_next_info(struct bsapi_object *);

const char *bsapi_object_column_name(struct bsapi_object *, int idx);

int bsapi_object_num_infos(struct bsapi_object *);
int bsapi_object_num_cols(struct bsapi_object *);
int bsapi_object_num_rows(struct bsapi_object *);

bool bsapi_object_next_row(struct bsapi_object *);
bool bsapi_object_set_row(struct bsapi_object *, int row);

/* Note: Always returns the same buffer, don't keep between calls. */
const struct bsapi_object_keyval *bsapi_object_next_column(struct bsapi_object *);
const struct bsapi_object_keyval *bsapi_object_column(struct bsapi_object *, int idx);

const char *bsapi_object_column_value(struct bsapi_object *, const char *column);

/* Note: _Not_ NULL terminated. */
/* XXX: Return type should probably be const char * const * */
const char **bsapi_object_all_values(struct bsapi_object *, int colidx);
const char **bsapi_object_column_all_values(struct bsapi_object *, const char *column);

