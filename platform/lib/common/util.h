#ifndef UTIL_H
#define UTIL_H

#ifdef __cplusplus
extern "C" {
#endif

#include <syslog.h>

#include "macros.h"
#include "string_functions.h"
#include "date_functions.h"
#include "memalloc_functions.h"
#include "error_functions.h"

/* For now. A lot of code expects it in util.h. */
#include "buf_string.h"

enum debug_level {
	D_CRIT = LOG_CRIT,
	D_ERROR = LOG_ERR,
	D_WARNING = LOG_WARNING,
	D_INFO = LOG_INFO,
	D_DEBUG = LOG_DEBUG
};

extern const char *debug_levels[];

extern enum debug_level debug_level;

#define DPRINTF(l, p) do { if ((l) <= debug_level) xwarnx p ; } while (0)

#ifdef __cplusplus
}
#endif

#endif
