
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include "url.h"
#include "util.h"
#include "cached_regex.h"
#include "buf_string.h"

/*
 * Split a URL into components. It's up to the caller to free the return value 
 */
struct url *
split_url(const char *url) {
	int pr_len, host_len, port_len, path_len;
	int re_matches[OV_VSZ(8)];
	struct url *u;
	static struct cached_regex re = { URL_RE, PCRE_CASELESS | PCRE_UTF8 };

	if (!cached_regex_match(&re, url, re_matches, sizeof(re_matches) / sizeof(*re_matches)))
		return NULL;

	pr_len   =  re_matches[OV_END(1)] - re_matches[OV_START(1)];
	host_len =  re_matches[OV_END(2)] - re_matches[OV_START(2)];
	port_len =  re_matches[OV_END(3)] - re_matches[OV_START(3)];
	path_len =  re_matches[OV_END(4)] - re_matches[OV_START(4)];
	
	u = xmalloc(sizeof(*u) + pr_len + host_len + port_len + path_len + 4);
	
	u->protocol = (char *)(u + 1);
	u->host = u->protocol + pr_len + 1;
	u->port = u->host + host_len + 1;
	u->path = u->port + port_len + 1;

	memcpy(u->protocol, url + re_matches[OV_START(1)],   pr_len);
	memcpy(u->host,     url + re_matches[OV_START(2)],   host_len);
	memcpy(u->port,     url + re_matches[OV_START(3)],   port_len);
	memcpy(u->path,     url + re_matches[OV_START(4)],   path_len);

	u->protocol[pr_len] = '\0';
	u->host[host_len]   = '\0';
	u->port[port_len]   = '\0';
	u->path[path_len]   = '\0';
  	
	return u;
}

struct _urlenc_conv {
	int start;
	int end;
	char dest;
};

char *
perform_google_encode(const char *str, ssize_t len) {
	register unsigned char c;
	const unsigned char *from;
	const unsigned char *end;
	int res = 0;
	unsigned int i;
	int present;
	int pos=0;
	char buf[2048];

	if (len < 0)
		len = strlen(str);
	
	/* converts accented characters to <127 characters */
	/* XXX latin1 presumed */
	struct _urlenc_conv conv_table[]={
		{ 192, 198, 'A'},
		{ 199, 199, 'C'},
		{ 200, 203, 'E'},
		{ 204, 207, 'I'},
		{ 208, 208, 'D'},
		{ 209, 209, 'N'},
		{ 210, 214, 'O'},
		{ 217, 220, 'U'},
		{ 224, 230, 'a'},
		{ 231, 231, 'c'},
		{ 232, 235, 'e'},
		{ 236, 239, 'i'},
		{ 242, 246, 'o'},
		{ 249, 252, 'u'},
		{ 0, 0, 0}};
	unsigned char hexchars[] = "0123456789ABCDEF";	
	unsigned char whitelist[]="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";

	from = (unsigned char*) str;
	end = (unsigned char*) str + len;

	while (from < end) {
		if (pos >= 2044) {
			res += pos;
			/*
			 * this only works if the size of the string is bigger then 1020. We talking about URL's that can have 1024 chars. Not useful case
			 * flush_googleencode(api->filter);
			 */
		}

		c = *from++;
		/* remove latin-1 characters */
		if (c>127) {
			i = 0;
			while (0 != conv_table[i].start) {
				if ( c>=conv_table[i].start && c<=conv_table[i].end ) {
					c = conv_table[i].dest; 
					break;
				}
				i+=1;
			}
		}
		
		/* clean string */
		present=1;
		for (i = 0 ; i < sizeof(whitelist) - 1; i += 1) {
			if (c == whitelist[i]) {
				present=0;
				break;
			}
		}

		if (1 == present) {
			c=' ';
		}


		if (c == ' ') {
			buf[pos++] = '+';
		} else if ((c < '0' && c != '-' && c != '.') ||
			   (c < 'A' && c > '9') ||
			   (c > 'Z' && c < 'a' && c != '_') ||
			   (c > 'z')) {
			buf[pos] = '%';
			buf[pos + 1] = hexchars[c >> 4];
			buf[pos + 2] = hexchars[c & 15]; 
			pos += 3;
		} else {
			buf[pos++] = c;
		}
	}

	from=(unsigned char *)buf;
	end=(unsigned char *)buf+pos;
	pos=0;
					
	while (from<end) {
		c=*from++;
		while (c == '+' && *from == '+') c=*from++;
		buf[pos++]=c;
	}
	buf[pos]=0;

	return xstrdup(buf);
}

char *
url_decode(char *str, int max, const char *stopchars, int unsafe, int *is_utf8) {
	char *ptr = str;
	int len = strlen(str);
	char *end = str + (max >= 0 ? max : len);
	int trailing = 0;
	int minuch = 0, uch = 0;

	if (is_utf8)
		*is_utf8 = 1;

	while (*ptr && ptr < end && (!stopchars || !strchr(stopchars, *ptr))) {
		if (*ptr == '%' && isxdigit(*(ptr + 1)) && isxdigit(*(ptr + 2))) {
			int a = *(unsigned char*)(ptr + 1);
			int b = *(unsigned char*)(ptr + 2);

			if (a <= '9')
				a -= '0';
			else if (a <= 'F')
				a -= 'A' - 10;
			else
				a -= 'a' - 10;
			if (b <= '9')
				b -= '0';
			else if (b <= 'F')
				b -= 'A' - 10;
			else
				b -= 'a' - 10;
			*ptr = a * 16 + b;
			if (!unsafe) {
				if (*ptr == '\t')
					*ptr = ' ';
				else if (*(unsigned char*)ptr < ' ') /* Extra safety check. */
					*ptr = '?';
			}
			memmove(ptr + 1, ptr + 3, str + len - (ptr + 3) + 1);
			end -= 2;
			len -= 2;
		} else if (*ptr == '+') {
			*ptr = ' ';
		}
		if (is_utf8 && *is_utf8) {
			if ((*ptr & 0x80) == 0) {
				if (trailing > 0)
					*is_utf8 = 0;
			} else if ((*ptr & 0xC0) == 0x80) {
				uch = uch << 6 | (*ptr & 0x3F);
				if (--trailing < 0)
					*is_utf8 = 0;
				else if (trailing == 0 && uch < minuch)
					*is_utf8 = 0;
			} else if (trailing > 0) {
				*is_utf8 = 0;
			} else if ((*ptr & 0xE0) == 0xC0) {
				trailing = 1;
				minuch = 0x80;
				uch = *ptr & 0x1F;
			} else if ((*ptr & 0xF0) == 0xE0) {
				trailing = 2;
				minuch = 0x800;
				uch = *ptr & 0x1F;
			} else if ((*ptr & 0xF8) == 0xF0) {
				trailing = 3;
				minuch = 0x10000;
				uch = *ptr & 0x1F;
			} else {
				*is_utf8 = 0;
			}
		}
		ptr++;
	}
	if (is_utf8 && trailing > 0)
		*is_utf8 = 0;
	return ptr;
}

void
url_encode(struct buf_string *dst, const char *str, size_t len) {
	const char *end = str + len;

	for (; str < end ; str++) {
		if ((*str & 0xFF) >= 0x7F || *str < 0x20) {
			bufcat(&dst->buf, &dst->len, &dst->pos, "%%%02X", *str & 0xFF);
		} else switch (*str) {
		case ' ':
			bufwrite(&dst->buf, &dst->len, &dst->pos, "+", 1);
			break;

		/* Possibly reserved: */
		case ';':
		case '/':
		case '?':
		case ':':
		case '@':
		case '&':
		case '=':
		case '+':
		case '$':
		case ',':
		/* Excluded: */
		case '<':
		case '>':
		case '#':
		case '%':
		case '"':
		/* Unwise: */
		case '{':
		case '}':
		case '|':
		case '\\':
		case '^':
		case '[':
		case ']':
		case '`':
		/* Adding ' here on my own volition */
		case '\'':
			bufcat(&dst->buf, &dst->len, &dst->pos, "%%%02X", *str & 0xFF);
			break;

		default:
			bufwrite(&dst->buf, &dst->len, &dst->pos, str, 1);
			break;
		}
	}
}
