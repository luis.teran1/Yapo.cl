#include <string.h>
#include <stdlib.h>
#include <limits.h>

#include "compconf.h"

const struct compconf_node *
compconf_get(const struct compconf_node *root, const char *key) {
	const struct compconf_node *n = root; 
	const char *tmp;

	if (key == NULL || root == NULL)
		return NULL;

	do {
		const struct compconf_node *star = n->star;
		const struct compconf_lookup *l;

		if (!n->lookup_func)
			return NULL;

		tmp = key;
		while (*tmp != '.' && *tmp)
			tmp++;

		l = n->lookup_func(key, tmp - key);

		if (l)
			n = &n->children[l->idx];
		else {
			if (star == NULL)
				return NULL;
			n = star;
		}
		key = tmp + 1;
	} while (*tmp);

	return n;
}

const struct compconf_node * 
compconf_vget(const struct compconf_node *root, ...) {
	va_list ap;
	const char *key;
	const struct compconf_node *res;

	va_start(ap, root);

	res = root;

	while ((key = va_arg(ap, const char *)) != NULL && res != NULL) {
		res = compconf_get(res, key);
	}

	va_end(ap);

	return res;
}

const struct compconf_node * 
compconf_vnget(const struct compconf_node *root, int args, va_list ap) {
	const char *key;
	const struct compconf_node *res;

	res = root;

	while (args-- && (key = va_arg(ap, const char *)) && res != NULL) {
		res = compconf_get(res, key);
	}

	return res;
}


const struct compconf_node *
compconf_byname(const struct compconf_node *root, const char *key, const char *index) {
	const struct compconf_node *node;

	if ((node = compconf_get(root, key)))
		return (compconf_get(node, index));

	return NULL;
}

const struct compconf_node*
compconf_byindex(const struct compconf_node *root, const char *key, int index) {
	const struct compconf_node *node;

	if (key)
		node = compconf_get(root, key);
	else
		node = root;
	if (node && node->nchildren > index) {
		return &(node->children[index % node->nchildren]);
	}

	return NULL;
}

int 
compconf_count(const struct compconf_node *node) {
	if (node)
		return node->nchildren;
	else
		return 0;
}

const char* 
compconf_value(const struct compconf_node *node) {
	if (node)
		return node->value;
	else 
		return NULL;
}

const char *
compconf_key(const struct compconf_node *node) {
	if (node)
		return node->key;
	else
		return NULL;
}

const char *
compconf_get_string(const struct compconf_node *root, const char *key) {
	const struct compconf_node *node;
	
	if ((node = compconf_get(root, key)) == NULL)
		return NULL;
	
	return compconf_value(node);
}

int
compconf_get_int(const struct compconf_node *root, const char *key) {
	const struct compconf_node *node;

	if ((node = compconf_get(root, key)) == NULL)
		return 0;
	
	return atoi(compconf_value(node));
}

int
compconf_vget_int(const struct compconf_node *root, ...) {
	const struct compconf_node *node;
	va_list ap;

	va_start(ap, root);

	node = compconf_vnget(root, INT_MAX, ap);
	va_end(ap);
	
	return node ? atoi(compconf_value(node)) : 0;
}

int
compconf_in_list(const char *value, const char *path, const struct compconf_node *root) {
	const struct compconf_node *node;
	int count;
	int i;

	if (!value)
		return -1;

	if (path)
		node = compconf_get(root, path);
	else
		node = root;

	count = compconf_count(node);

	for (i = 0; i < count; i++) {
		const char *v = compconf_value(compconf_byindex(node, NULL, i));
		if (v && !strcmp(v, value)) {
			return i;
		}
	}

	return -1;
}

void
compconf_json(const struct compconf_node *n, int depth, int (*pf)(void *, int, const char *, ...), void *cbdata) {
	int c = compconf_count(n);
	int i;

	if (!depth)
		pf(cbdata, depth, "{\n");
	for (i = 0 ; i < c ; i++) {
		const struct compconf_node *ns = compconf_byindex(n, NULL, i);
		pf(cbdata, depth + 1, "'%s': ", ns->key);

		if (!ns->value) {
			pf(cbdata, 0, "{\n");
			compconf_json(ns, depth + 1, pf, cbdata);
			if (i < c - 1)
				pf(cbdata, 0, ",\n");
			else
				pf(cbdata, 0, "\n");
			break;
		} else {
			const char *p = ns->value;
			const char *ch = strchr(p, '\'');

			pf(cbdata, 0, "'");
			while (ch) {
				pf(cbdata, 0, "%.*s\\'", ch - p, p);
				p = ch + 1;
				ch = strchr(p, '\'');
			}

			pf(cbdata, 0, "%s'%s\n", p, ((i < c - 1) ? "," : ""));
			break;
		}
	}
	pf(cbdata, depth, "}");
}
