
#include <string.h>
#include <unicode/ucnv.h>
#include <unicode/unorm.h>
#include <unicode/ucnv_cb.h>
#include <unicode/utf8.h>
#include <stdlib.h>

#include "unicode.h"
#include "util.h"
#include "logging.h"

static void
utf8_decode_error_cb(const void *ctx, UConverterToUnicodeArgs *args, const char *badptr, int32_t badlen,
		UConverterCallbackReason reason, UErrorCode *err) {
	UChar subst[badlen];
	UConverter *dst_cnv = (UConverter *)ctx;
	int l, i;

	switch (reason) {
	case UCNV_UNASSIGNED:
	case UCNV_ILLEGAL:
	case UCNV_IRREGULAR:
		/* Use the destination charset */
		/* This won't fail it seems, we'll just get a \uFFFD char on bad values */
		*err = U_ZERO_ERROR;
		l = ucnv_toUChars(dst_cnv, subst, badlen, badptr, badlen, err);
		if (!U_FAILURE(*err)) {
			for (i = 0 ; i < l ; i++) {
				if (subst[i] == 0xFFFD)
					subst[i] = '?';
			}
			*err = U_ZERO_ERROR;
			ucnv_cbToUWriteUChars(args, subst, l, 0, err);
		}
		break;
	
	case UCNV_RESET:
	case UCNV_CLOSE:
	case UCNV_CLONE:
		break;
	}
}

char *
utf8_decode(char *dst, int *dstlen, const char *str, int slen, UConverter *utf8_cnv, UConverter *dst_cnv) {
	int srclen = slen >= 0 ? slen : (int)strlen(str);
	UChar *bufa;
	UChar *bufb;
	int l;
	UErrorCode err = 0;
	UConverterToUCallback old_tou_cb;
	const void *old_tou_ctx;

	ucnv_reset(utf8_cnv);
	ucnv_reset(dst_cnv);

	if (srclen < 1024) {
		bufa = alloca((srclen + 1) * sizeof (*bufa));
		bufb = alloca((srclen + 1) * sizeof (*bufb));
	} else {
		bufa = xmalloc((srclen + 1) * sizeof (*bufa));
		bufb = xmalloc((srclen + 1) * sizeof (*bufb));
	}

	ucnv_setToUCallBack(utf8_cnv, utf8_decode_error_cb, dst_cnv, &old_tou_cb, &old_tou_ctx, &err);
	ucnv_setSubstChars(dst_cnv, "?", 1, &err);

	l = ucnv_toUChars(utf8_cnv, bufa, srclen + 1, str, srclen, &err);
	if (U_FAILURE(err))
		xerrx(1, "utf8_decode(%.*s): error from ucnv_toUChars: %s", srclen, str, u_errorName(err));

	l = unorm_normalize(bufa, l, UNORM_NFC, 0, bufb, srclen + 1, &err);
	if (U_FAILURE(err))
		xerrx(1, "utf8_decode(%.*s): error from unorm_normalize: %s", srclen, str, u_errorName(err));

	l = ucnv_fromUChars(dst_cnv, dst, *dstlen, bufb, l, &err);
	if (U_FAILURE(err))
		xerrx(1, "utf8_decode(%.*s): error from ucnv_fromUChars: %s", srclen, str, u_errorName(err));
	if (err == U_STRING_NOT_TERMINATED_WARNING)
		dst[--l] = '\0';

	if (srclen >= 1024) {
		free(bufa);
		free(bufb);
	}

	*dstlen = l;
	return dst;
}


size_t strlen_utf8(const char *str) {
	size_t n = 0, i;
	size_t ln = strlen(str);
	for (i = 0 ; i < ln ; (U8_FWD_1(str, i, ln)) ) {
		n++;
	}
	return n;
}

/*
 * From http://en.wikipedia.org/wiki/Windows-1252
 *
 * Most modern web browsers and e-mail clients treat the MIME charset
 * ISO-8859-1 as Windows-1252 to accommodate such mislabeling. This is
 * now standard behavior in the draft HTML 5 specification, which
 * requires that documents advertised as ISO-8859-1 actually be parsed
 * with the Windows-1252 encoding.
 */
static struct {
	int codepoint;
	unsigned char win1252;
	const char *encoding;
} translit[] = {
	{ 0x20ac, 0x80, "\xe2\x82\xac" }, /* € = EURO SIGN */
	{ 0x0, 0x0, "?" },
	{ 0x201a, 0x82, "\xe2\x80\x9a" }, /* ‚ = SINGLE LOW-9 QUOTATION MARK */
	{ 0x192, 0x83, "\xc6\x92" },      /* ƒ = LATIN SMALL LETTER F WITH HOOK */
	{ 0x201e, 0x84, "\xe2\x80\x9e" }, /* „ = DOUBLE LOW-9 QUOTATION MARK */
	{ 0x2026, 0x85, "\xe2\x80\xa6" }, /* … = HORIZONTAL ELLIPSIS */
	{ 0x2020, 0x86, "\xe2\x80\xa0" }, /* † = DAGGER */
	{ 0x2021, 0x87, "\xe2\x80\xa1" }, /* ‡ = DOUBLE DAGGER */
	{ 0x2c6, 0x88, "\xcb\x86" },      /* ˆ = MODIFIER LETTER CIRCUMFLEX ACCENT */
	{ 0x2030, 0x89, "\xe2\x80\xb0" }, /* ‰ = PER MILLE SIGN */
	{ 0x160, 0x8a, "\xc5\xa0" },      /* Š = LATIN CAPITAL LETTER S WITH CARON */
	{ 0x2039, 0x8b, "\xe2\x80\xb9" }, /* ‹ = SINGLE LEFT-POINTING ANGLE QUOTATION MARK */
	{ 0x152, 0x8c, "\xc5\x92" },      /* Œ = LATIN CAPITAL LIGATURE OE */
	{ 0x0, 0x0, "?" },
	{ 0x17d, 0x8e, "\xc5\xbd" },      /* Ž = LATIN CAPITAL LETTER Z WITH CARON */
	{ 0x0, 0x0, "?" },
	{ 0x0, 0x0, "?" },
	{ 0x2018, 0x91, "\xe2\x80\x98" }, /* ‘ = LEFT SINGLE QUOTATION MARK */
	{ 0x2019, 0x92, "\xe2\x80\x99" }, /* ’ = RIGHT SINGLE QUOTATION MARK */
	{ 0x201c, 0x93, "\xe2\x80\x9c" }, /* “ = LEFT DOUBLE QUOTATION MARK */
	{ 0x201d, 0x94, "\xe2\x80\x9d" }, /* ” = RIGHT DOUBLE QUOTATION MARK */
	{ 0x2022, 0x95, "\xe2\x80\xa2" }, /* • = BULLET */
	{ 0x2013, 0x96, "\xe2\x80\x93" }, /* – = EN DASH */
	{ 0x2014, 0x97, "\xe2\x80\x94" }, /* — = EM DASH */
	{ 0x2dc, 0x98, "\xcb\x9c" },      /* ˜ = SMALL TILDE */
	{ 0x2122, 0x99, "\xe2\x84\xa2" }, /* ™ = TRADE MARK SIGN */
	{ 0x161, 0x9a, "\xc5\xa1" },      /* š = LATIN SMALL LETTER S WITH CARON */
	{ 0x203a, 0x9b, "\xe2\x80\xba" }, /* › = SINGLE RIGHT-POINTING ANGLE QUOTATION MARK */
	{ 0x153, 0x9c, "\xc5\x93" },      /* œ = LATIN SMALL LIGATURE OE */
	{ 0x0, 0x0, "?" },
	{ 0x17e, 0x9e, "\xc5\xbe" },      /* ž = LATIN SMALL LETTER Z WITH CARON */
	{ 0x178, 0x9f, "\xc5\xb8" },      /* Ÿ = LATIN CAPITAL LETTER Y WITH DIAERESIS */
};

char *
latin1_to_utf8(const char *src, int slen, int *dstlen) {
	char *dst;
	const char *s = src;
	const char *end = NULL;
	size_t dlen, utf8len = 0;

	if (slen >= 0)
		end = src + slen;
	else
		slen = 0;

	do {
		if ((*s & 0xC0) == 0xC0 || (*s & 0xA0) == 0xA0)
			utf8len += 2;
		else if (*s & 0x80)
			utf8len += 3;	/* may be too much but that is no issue */
		else
			utf8len++;
		if (end && s >= end)	/* make sure to also count space for the '\0' */
			break;
	} while (*s++);

	dst = xmalloc(utf8len);

	latin1_to_utf8_buf(&dlen, src, slen, dst, utf8len);

	memset(dst + dlen, 0, utf8len - dlen);
	if (dstlen)
		*dstlen = dlen;

	return dst;
}

size_t
latin1_to_utf8_buf(size_t *outlen, const char *src, size_t slen,
    char *dst, size_t dlen)
{
	const char *s = src;
	const char *send = NULL;
	char *d = dst;
	char *dend = dst + dlen;

	if (slen)
		send = s + slen;

	for (; *s; s++) {
		if (send && s >= send)
			break;
		if ((*s & 0xC0) == 0xC0) {
			if (d + 2 >= dend)
				break;
			*d++ = 0xC3;
			*d++ = 0x80 + (*s & 0x3F);
		} else if ((*s & 0xA0) == 0xA0) {
			if (d + 2 >= dend)
				break;
			*d++ = 0xC2;
			*d++ = 0x80 + (*s & 0x3F);
		} else if (*s & 0x80) {
			const char *x = translit[*s & 0x1f].encoding;
			if (d + 3 >= dend)
				break;
			while (*x)
				*d++ = *x++;
		} else {
			if (d + 1 >= dend)
				break;
			*d++ = *s;
		}
	}

	if (outlen)
		*outlen = d - dst;
	return s - src;
}

char *
utf8_to_latin1(const char *src) {
	char *tmp;
	char *buf;

	tmp = buf = xstrdup(src);
	while (*src) {
		int c = utf8_char(&src);
		if (c > 255) {
			unsigned int i;
			for (i = 0; i < sizeof(translit) / sizeof(translit[0]); i++) {
				if (translit[i].codepoint == c) {
					if ((c = translit[i].win1252) == 0)
						c = '?';
					break;
				}
			}
			if (i == sizeof(translit) / sizeof(translit[0]))
				c = '?';
		}
		*tmp++ = c;
	}
	*tmp = '\0';
	return buf;
}
