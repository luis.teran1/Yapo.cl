#ifndef SIMILAR_TEXT_H
#define SIMILAR_TEXT_H

int similar_text(const char *s1, const char *s2, int is_utf8);

#endif /*SIMILAR_TEXT_H*/
