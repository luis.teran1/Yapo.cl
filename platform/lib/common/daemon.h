#ifndef DAEMON_H
#define DAEMON_H

#include "macros.h"

#include <stdbool.h>
#include <sys/types.h>

void set_pidfile(const char *pidfile);
void write_pidfile(void);
void set_switchuid(const char *uid);
void set_coresize(size_t sz);
void set_quick_start(int flag);
void set_respawn_backoff_attrs(int min_s, int max_s, float rate);

void bos_here(void);
int bos(int (*)(void)) NORETURN;

void do_switchuid(void);

void daemonify_here(bool nobos);
void daemonify(int, int (*)(void)) NORETURN;

#endif /*DAEMON_H*/
