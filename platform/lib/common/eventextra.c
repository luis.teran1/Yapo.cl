#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <sys/types.h>
#include <sys/time.h>

#include "event.h"
#include "eventextra.h"

/*
 * Reads a line terminated by a given newline.
 * Calls evbuffer_readline if newline is NULL.
 * The returned buffer needs to be freed by the called.
 */

char *
evbuffer_read_newline(struct evbuffer *buffer, const char *newline, size_t nllen)
{
	u_char *data;
	int len;
	char *line;
	int i;
	int nli = 0;

	if (!newline || !nllen)
		return evbuffer_readline(buffer);

	data = EVBUFFER_DATA(buffer);
	len = EVBUFFER_LENGTH(buffer);

	for (i = 0; i < len; i++) {
		if (data[i] == newline[nli]) {
			if (++nli == (int)nllen)
				break;
		} else if (nli) {
			/* Jump back to recheck characters passed. */
			i -= nli;
			nli = 0;
		}
	}

	if (i == len)
		return (NULL);
	i -= nli - 1;

	if ((line = malloc(i + 1)) == NULL) {
		fprintf(stderr, "%s: out of memory\n", __func__);
		evbuffer_drain(buffer, i);
		return (NULL);
	}

	memcpy(line, data, i);
	line[i] = '\0';

	evbuffer_drain(buffer, i + nli);

	return (line);
}

char *
bufferevent_strwhat(short what, char *buf, size_t buflen) {
	char *p = buf;
	char *e = buf + buflen;

#define COPY(x) \
	if ((what & x) && p + sizeof(" | " #x) <= e) { \
		if (p > buf) \
			p = stpcpy(p, " | "); \
		p = stpcpy(p, #x); \
	}
	COPY(EVBUFFER_READ);
	COPY(EVBUFFER_WRITE);
	COPY(EVBUFFER_EOF);
	COPY(EVBUFFER_ERROR);
	COPY(EVBUFFER_TIMEOUT);
#undef COPY
	if (p == buf && buflen >= sizeof("0"))
		strcpy(p, "0");
	return buf;
}

