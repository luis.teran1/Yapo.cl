#ifndef TPTYPES_H
#define TPTYPES_H

#include "avl.h"
#include "cached_regex.h"

#include <string.h>

struct bpapi;

/* XXX layering violation, templates should not know about ctemplates implementation */
struct hash_entry {
	struct avl_node tree;
	const char **valuelist;
	const char *key;
	int valuelistlen;
	int len;
	int free;
	const char *one_val;
	char *tmpval;
};

struct template_var {
	struct hash_entry *entry;
	int initialized;
};

struct bpapi_loop_var {
	int len;
	union {
		const char **list;
		struct bpapi_vtree_chain *vlist;
	} l;
	void (*cleanup)(struct bpapi_loop_var *var);
};

struct template_opaque_var {
	void *data;
	void (*cleanup)(struct template_opaque_var *var);
};

enum tpvtype {
	TPV_STRING,
	TPV_DYNSTR, /* Note: <= in tpvar_str macro */
	TPV_INT,
	TPV_NODE,
	TPV_CODE,
	TPV_TPVARR,
	TPV_REGEX,
	TPV_OPAQUE
};

#ifndef __cplusplus

/*
 * XXX - add precomputed length of strings.
 */
struct tpvar {
	union {
		const char *str;
		int i;
		struct cached_regex *regex;
		struct template_opaque_var *opaque;
		struct {
			char *(*func)(void *arg, int *outlen);
			void *arg;
		} code;
		struct {
			int nvars;
			const struct tpvar **vars;
		} tpvarr;
		struct bpapi_vtree_chain *node;
	} v;
	enum tpvtype type;
};
#define TPV_NULL ((const struct tpvar){ { .str = "" }, TPV_STRING })
#define TPV_STRING(x) ((const struct tpvar){ { .str = (x) }, TPV_STRING })
#define TPV_INT(x) ((const struct tpvar){ { .i = (x) }, TPV_INT })
#define TPV_REGEX(x) ((const struct tpvar){ { .regex = (x) }, TPV_REGEX })
#define TPV_NODE(x) ((const struct tpvar){ { .node = (x) }, TPV_NODE })
#define TPV_OPAQUE(x) ((const struct tpvar){ { .opaque = (x) }, TPV_OPAQUE })
#define TPV_CODE(x, y) ((const struct tpvar){ { .code = { (x), (y) } }, TPV_CODE })
#define TPV_DYNSTR(x) ((const struct tpvar){ { .str = (x) }, TPV_DYNSTR })
#define TPV_TPVARR(n, ...) ((const struct tpvar){ { .tpvarr = { (n), ((const struct tpvar *[]){ __VA_ARGS__ }) } }, TPV_TPVARR })

#define TPV_SET(p, v) (*(p) = (v), (p))

/* Shortcut here to help constant expressions. */
#define tpvar_str(var, freeptr) (__extension__({ const struct tpvar *_tpvar_v = (var); (_tpvar_v && _tpvar_v->type <= TPV_DYNSTR ? *freeptr = NULL, _tpvar_v->v.str : get_tpvar_str(_tpvar_v, (freeptr))); }))
const char *get_tpvar_str(const struct tpvar *var, char **freeptr);
char *tpvar_strdup(const struct tpvar *var);
char *tpvar_strcpy(char *dst, const struct tpvar *var, size_t dstsz);

#define TPVAR_STRTMP(dst, var) \
	do { \
		char *_ft; \
		const char *_t = tpvar_str((var), &_ft); \
		if (_ft) { \
			(dst) = strdupa(_t); \
			free(_ft); \
		} else { \
			(dst) = _t; \
	       	} \
	} while(0)

int tpvar_int(const struct tpvar *var);
float tpvar_float(const struct tpvar *var);

struct bpapi_vtree_chain *tpvar_node(const struct tpvar *var);
struct template_opaque_var *tpvar_opaque(const struct tpvar *var);
struct cached_regex *tpvar_regex(const struct tpvar *var);

static inline struct tpvar *
tpvar_rebuild(struct tpvar *dst, const char *val, char *freeptr) {
	if (freeptr == val)
		return TPV_SET(dst, TPV_DYNSTR(freeptr));

	free(freeptr);
	return TPV_SET(dst, TPV_STRING(val));
}
#endif

/* Must be ordered by increased cacheability */
enum bpcacheable {
	BPCACHE_CANT,
	BPCACHE_UNKNOWN,
	BPCACHE_CAN,
	BPCACHE_USED
};


#endif /*TPTYPES_H*/
