#ifndef VTREE_H
#define VTREE_H

#include <stdarg.h>

#include "macros.h"

#include "tptypes.h"

#define VTREE_LOOP ((const char *)-1)

struct vtree_keyvals;

struct bpapi_vtree_chain {
	const struct bpapi_vtree {
		int (*getlen)(struct bpapi_vtree_chain *, enum bpcacheable *cc, int, const char **);
		const char* (*get)(struct bpapi_vtree_chain *, enum bpcacheable *cc, int, const char **);
		int (*haskey)(struct bpapi_vtree_chain *, enum bpcacheable *cc, int, const char **);

		void (*fetch_keys)(struct bpapi_vtree_chain *, struct bpapi_loop_var *loop, enum bpcacheable *cc, int, const char **);
		void (*fetch_values)(struct bpapi_vtree_chain *, struct bpapi_loop_var *loop, enum bpcacheable *cc, int, const char **);
		void (*fetch_byval)(struct bpapi_vtree_chain *, struct bpapi_loop_var *loop, enum bpcacheable *cc, const char *value, int, const char **);

		struct bpapi_vtree_chain *(*getnode)(struct bpapi_vtree_chain *, enum bpcacheable *cc, struct bpapi_vtree_chain *dst, int, const char **);
		void (*fetch_nodes)(struct bpapi_vtree_chain *, struct bpapi_loop_var *loop, enum bpcacheable *cc, int, const char **);

		void (*fetch_keys_and_values)(struct bpapi_vtree_chain *, struct vtree_keyvals *loop, enum bpcacheable *cc, int, const char **);

		void (*free)(struct bpapi_vtree_chain *);
#define bpv_free(vchain) ((vchain)->fun && (vchain)->fun->free ? (vchain)->fun->free(vchain) : (void)0)
#define vtree_free(vchain) bpv_free(vchain)
	} *fun;
	void *data;
	struct bpapi_vtree_chain *next;
};

struct vtree_keyvals {
	enum vtree_keyvals_type {
		vktUnknown, /* Backend doesn't support types. key is set, but might be ignored */
		vktDict, /* key is set and should be used */
		vktList, /* key is NULL, index should be used */
	} type;
	int len;
	struct vtree_keyvals_elem {
		const char *key;
		enum vtree_value_type {
			vkvNone,
			vkvValue,
			vkvNode
		} type;
		union {
			const char *value;
			struct bpapi_vtree_chain node;
		} v;
	} *list;
	void (*cleanup)(struct vtree_keyvals *);
};

struct shadow_vtree
{
	struct bpapi_vtree_chain vtree;
	void (*free_cb)(struct shadow_vtree *sv);
	void *cbdata;
};

const char* vtree_get_cache(struct bpapi_vtree_chain *vchain, enum bpcacheable *cc, const char **cache, ...) SENTINEL(0);
#define vtree_get(vchain, ...) vtree_get_cache(vchain, NULL, NULL, __VA_ARGS__)
int vtree_getint_cache(struct bpapi_vtree_chain *vchain, enum bpcacheable *cc, int *cache, ...) SENTINEL(0);
#define vtree_getint(vchain, ...) vtree_getint_cache(vchain, NULL, NULL, __VA_ARGS__)
int vtree_haskey_cache(struct bpapi_vtree_chain *vchain, enum bpcacheable *cc, int *cache, ...) SENTINEL(0);
#define vtree_haskey(vchain, ...) vtree_haskey_cache(vchain, NULL, NULL, __VA_ARGS__)
int vtree_getlen_cache(struct bpapi_vtree_chain *vchain, enum bpcacheable *cc, int *cache, ...) SENTINEL(0);
#define vtree_getlen(vchain, ...) vtree_getlen_cache(vchain, NULL, NULL, __VA_ARGS__)
void vtree_fetch_keys_cache(struct bpapi_vtree_chain *vchain, struct bpapi_loop_var *loop, enum bpcacheable *cc, ...) SENTINEL(0);
#define vtree_fetch_keys(vchain, loop, ...) vtree_fetch_keys_cache(vchain, loop, NULL, __VA_ARGS__)
void vtree_fetch_values_cache(struct bpapi_vtree_chain *vchain, struct bpapi_loop_var *loop, enum bpcacheable *cc, ...) SENTINEL(0);
#define vtree_fetch_values(vchain, loop, ...) vtree_fetch_values_cache(vchain, loop, NULL, __VA_ARGS__)
void vtree_fetch_keys_by_value_cache(struct bpapi_vtree_chain *vchain, struct bpapi_loop_var *loop, enum bpcacheable *cc, const char *value, ...) SENTINEL(0);
#define vtree_fetch_keys_by_value(vchain, loop, value, ...) vtree_fetch_keys_by_value_cache(vchain, loop, NULL, value, __VA_ARGS__)

/* Please not that dst will be freed so it needs to be properly initialized (for example to {0}) */
struct bpapi_vtree_chain *vtree_getnode_cache(struct bpapi_vtree_chain *vchain, enum bpcacheable *cc, struct bpapi_vtree_chain *dst, struct bpapi_vtree_chain **cache, ...) SENTINEL(0);
#define vtree_getnode(vchain, dst, ...) vtree_getnode_cache(vchain, NULL, dst, NULL, __VA_ARGS__)

void vtree_fetch_nodes_cache(struct bpapi_vtree_chain *vchain, struct bpapi_loop_var *loop, enum bpcacheable *cc, ...) SENTINEL(0);
#define vtree_fetch_nodes(vchain, loop, ...) vtree_fetch_nodes_cache(vchain, loop, NULL, __VA_ARGS__)
void vtree_fetch_keys_and_values_cache(struct bpapi_vtree_chain *vchain, struct vtree_keyvals *loop, enum bpcacheable *cc, ...) SENTINEL(0);
#define vtree_fetch_keys_and_values(vchain, loop, ...) vtree_fetch_keys_and_values_cache(vchain, loop, NULL, __VA_ARGS__)

const char* vtree_get_cachev(struct bpapi_vtree_chain *vchain, enum bpcacheable *cc, const char **cache, int argc, const char **argv);
int vtree_getint_cachev(struct bpapi_vtree_chain *vchain, enum bpcacheable *cc, int *cache, int argc, const char **argv);
int vtree_haskey_cachev(struct bpapi_vtree_chain *vchain, enum bpcacheable *cc, int *cache, int argc, const char **argv);
int vtree_getlen_cachev(struct bpapi_vtree_chain *vchain, enum bpcacheable *cc, int *cache, int argc, const char **argv);
void vtree_fetch_keys_cachev(struct bpapi_vtree_chain *vchain, struct bpapi_loop_var *loop, enum bpcacheable *cc, int argc, const char **argv);
void vtree_fetch_values_cachev(struct bpapi_vtree_chain *vchain, struct bpapi_loop_var *loop, enum bpcacheable *cc, int argc, const char **argv);
void vtree_fetch_keys_by_value_cachev(struct bpapi_vtree_chain *vchain, struct bpapi_loop_var *loop, enum bpcacheable *cc, const char *value, int argc, const char **argv);
struct bpapi_vtree_chain *vtree_getnode_cachev(struct bpapi_vtree_chain *vchain, enum bpcacheable *cc, struct bpapi_vtree_chain *dst, struct bpapi_vtree_chain **cache, int argc, const char **argv);
void vtree_fetch_nodes_cachev(struct bpapi_vtree_chain *vchain, struct bpapi_loop_var *loop, enum bpcacheable *cc, int argc, const char **argv);
void vtree_fetch_keys_and_values_cachev(struct bpapi_vtree_chain *vchain, struct vtree_keyvals *loop, enum bpcacheable *cc, int argc, const char **argv);

void shadow_vtree_init(struct bpapi_vtree_chain *, struct shadow_vtree *, struct bpapi_vtree_chain *);
extern const struct bpapi_vtree shadow_vtree;

void prefix_vtree_init(struct bpapi_vtree_chain *, const char *, struct bpapi_vtree_chain *);

#endif /*VTREE_H*/
