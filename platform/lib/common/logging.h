#ifndef LOGGING_H
#define LOGGING_H

#include "macros.h"

#ifdef __cplusplus
extern "C"
{
#endif /* __cplusplus */


#include <syslog.h>
#include <stdarg.h>

int get_priority_from_level(const char* level, int default_priority);

int log_printf(int level, const char* fmt, ...) FORMAT_PRINTF(2, 3) VISIBILITY_HIDDEN;
int vlog_printf(int level, const char *fmt, va_list ap) VISIBILITY_HIDDEN;
void log_backtrace(int level, int skip) VISIBILITY_HIDDEN;

int log_setup(const char *filename, const char *level) VISIBILITY_HIDDEN;
int log_setup_perror(const char *filename, const char *level) VISIBILITY_HIDDEN;
int log_shutdown(void) VISIBILITY_HIDDEN;
int log_level() VISIBILITY_HIDDEN;

void log_register_thread(const char *log_string, ...) FORMAT_PRINTF(1, 2) VISIBILITY_HIDDEN;

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif
