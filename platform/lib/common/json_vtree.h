#ifndef COMMON_JSON_VTREE_H
#define COMMON_JSON_VTREE_H

struct bpapi_vtree_chain;
struct bconf_node;

int json_vtree(struct bpapi_vtree_chain *dst, const char *root_name, const char *json_str, ssize_t jsonlen, int validate_utf8);
int json_bconf(struct bconf_node **dst, const char *root_name, const char *json_str, ssize_t jsonlen, int validate_utf8);
void vtree_json(struct bpapi_vtree_chain *n, int use_arrays, int depth, int (*pf)(void *, int, int, const char *, ...), void *cbdata);

#endif /*COMMON_JSON_VTREE_H*/
