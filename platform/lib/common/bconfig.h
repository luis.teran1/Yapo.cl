#ifndef CONFIG_H
#define CONFIG_H

struct bconf_node;
struct bconf_node* config_init(const char *filename);
int config_free();

void load_bconf(const char *appl, struct bconf_node **root);
int load_bconf_file(const char *appl, struct bconf_node **root, const char *filename);

#endif
