
/* ---------------------------------------------------------------------------
 * These are two functions from OpenBSD, we might want to autoconf for them
 * in the future, but until then, just use prefixed names.
 * ------------------------------------------------------------------------ */

#ifdef __cplusplus
extern "C" {
#endif

size_t mb_strlcpy(char *dst, const char *src, size_t siz);
size_t mb_strlcat(char *dst, const char *src, size_t siz);

#ifdef __cplusplus
}
#endif

#ifndef HAVE_STRLCPY
#define strlcpy(a,b,c) mb_strlcpy(a,b,c)
#endif
#ifndef HAVE_STRLCAT
#define strlcat(a,b,c) mb_strlcat(a,b,c)
#endif
