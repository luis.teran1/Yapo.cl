#ifndef _EVENTEXTRA_H_
#define _EVENTEXTRA_H_

#include "event.h"

char *evbuffer_read_newline(struct evbuffer *buffer, const char *newline, size_t nllen);

char *bufferevent_strwhat(short what, char *buf, size_t buflen);

#endif /*_EVENTEXTRA_H_*/
