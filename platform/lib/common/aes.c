#include <openssl/aes.h>
#include <openssl/hmac.h>
#include <string.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <sys/time.h>
#include <stdio.h>

#include "util.h"
#include "strl.h"
#include "base64.h"

#include "aes.h"

static const char b64[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
static const char ub64[] = ">" /* + */
			   "\0\0\0"
			   "?456789:;<=" /* /, 0 - 9 */
			   "\0\0\0"
			   "\0" /* = */
			   "\0\0\0"
			   "\0\1\2\3\4\5\6\7\10\11\12\13\14\15\16\17\20\21\22\23\24\25\26\27\30\31" /* A - Z */
			   "\0\0\0\0\0\0"
			   "\32\33\34\35\36\37 !\"#$%&'()*+,-./0123"; /* a - z */

static int
decode_b64(const char *str, int slen, unsigned char *bin) {
	int i;
	int r;

	for (i = r = 0; i < slen; i += 4, r += 3) {
		if (str[i] < '+' || str[i] > 'z' || str[i + 1] < '+' || str[i + 1] > 'z'
				|| str[i + 2] < '+' || str[i + 2] > 'z' || str[i + 3] < '+' || str[i + 3] > 'z') {
			errno = EINVAL;
			return -1;
		}
		bin[r] = ub64[str[i] - '+'] << 2 | ub64[str[i + 1] - '+'] >> 4;
		bin[r + 1] = ub64[str[i + 1] - '+'] << 4 | ub64[str[i + 2] - '+'] >> 2;
		bin[r + 2] = ub64[str[i + 2] - '+'] << 6 | ub64[str[i + 3] - '+'];
	}

	if (str[slen - 1] == '=') {
		if (str[slen - 2] == '=')
			return 2;
		else
			return 1;
	}
	return 0;
}

static char *
encode_b64(const unsigned char *enc, int elen) {
	char *res = malloc((elen + 2) / 3 * 4 + 1);
	int i;
	int r;

	if (!res)
		return NULL;

	for (i = r = 0; i < elen; i += 3, r += 4) {
		res[r] = b64[enc[i] >> 2];
		if (i + 1 < elen) {
			res[r + 1] = b64[(enc[i] & 0x3) << 4 | enc[i + 1] >> 4];
			if (i + 2 < elen) {
				res[r + 2] = b64[(enc[i + 1] & 0x0f) << 2 | enc[i + 2] >> 6];
				res[r + 3] = b64[enc[i + 2] & 0x3F];
			} else {
				res[r + 2] = b64[(enc[i + 1] & 0x0f) << 2];
				res[r + 3] = '=';
			}
		} else {
			res[r + 1] = b64[(enc[i] & 0x3) << 4];
			res[r + 2] = '=';
			res[r + 3] = '=';
		}
	}
	res[r] = '\0';
	return res;
}

static void
generate_iv(unsigned char *dst, const char *src) {
	const int ivlen = 128 / 8;

	memset (dst, 0, ivlen);
	/* Fetch a default iv which is unlikely to be reused for this key. */
	int use_default_iv = (src == NULL) || (src[0] == '\0') ; 
	if (use_default_iv) {
		struct timeval tv;

		gettimeofday(&tv, NULL);
		snprintf((char*)dst, ivlen, "%lx%lx", (long)tv.tv_sec, (long)tv.tv_usec);
	} else {
		/* This is supposed to be strncpy */
		strncpy((char*) dst, src, ivlen);
	}
}

/* Use multiple of 3 for pad to always avoid = at end of result. */
char *
aes_encode(const void *inbuf, int inlen, int pad, const char *nonce, const char *key, int klen) {
	unsigned char *enc;
	int elen;
	const int ivlen = 128 / 8;
	const int bklen = 128 / 8;
	unsigned char iv_buf[ivlen];
	unsigned char nonce_buf[bklen];
	int num;
	int i;
	unsigned char *binkey;
	AES_KEY aes;
	unsigned char *padbuf = NULL;

	if (inlen == -1)
		inlen = strlen(inbuf) + 1;
	if (pad < 1)
		pad = 1;

	/* Base64 decode key. */
	if (klen == -1)
		klen = strlen(key);
	if (klen != (bklen + 2) / 3 * 4) {
		errno = EINVAL;
		return NULL;
	}
	binkey = alloca(bklen + 2);
	i = decode_b64(key, klen, binkey);
	if (i < 0)
		return NULL;
	AES_set_encrypt_key(binkey, bklen * 8, &aes);

	/* NIST SP 800-38A: "The first method is to apply the forward cipher function, under the same key
	   that is used for the encryption of the plaintext, to a nonce. The nonce must be a data block
	   that is unique to each execution of the encryption operation." */

	/* Setup nonce in a buffer for encryption. Input is treated as a string, and padded to one block if needed. */
	generate_iv(nonce_buf, nonce);

	/* Encrypt nonce using same key as message to use as IV */
	AES_ecb_encrypt(nonce_buf, iv_buf, &aes, AES_ENCRYPT);

	if (pad <= 1)
		pad = 0;
	else
		pad -= (inlen + ivlen) % pad;
	elen = inlen + pad;
	enc = alloca(ivlen + elen);

	/* Store iv and ciphertext in enc. */
	memcpy(enc, iv_buf, ivlen);
	num = 0;
	AES_cfb128_encrypt(inbuf, enc + ivlen, inlen, &aes, iv_buf, &num, AES_ENCRYPT);
	if (pad > 0) {
		padbuf = alloca(pad);
		memset(padbuf, 0, pad);
		AES_cfb128_encrypt(padbuf, enc + ivlen + inlen, elen - inlen, &aes, iv_buf, &num, AES_ENCRYPT);
	}

	/* Finally base64 encode the result. */
	return encode_b64(enc, ivlen + elen);
}

void *
aes_decode(const char *str, int slen, const char *key, int klen, int *reslen) {
	return aes_decode_buf(str, slen, key, klen, NULL, reslen);
}

void *
aes_decode_buf(const char *str, int slen, const char *key, int klen, void *resbuf, int *reslen) {
	const int ivlen = 128 / 8;
	unsigned char *enc;
	int elen;
	AES_KEY aes;
	int i;
	int r;
	char *res;
	int num;
	const int bklen = 128 / 8;
	unsigned char *binkey;

	if (slen == -1)
		slen = strlen(str);
	if (!slen)
		return strdup("");

	if (slen % 4) {
		errno = EINVAL;
		return NULL;
	}
	
	/* Base64 decode key. */
	if (klen == -1)
		klen = strlen(key);
	if (klen != (bklen + 2) / 3 * 4) {
		errno = EINVAL;
		return NULL;
	}
	binkey = alloca(bklen + 2);
	i = decode_b64(key, klen, binkey);
	if (i < 0)
		return NULL;
	AES_set_encrypt_key(binkey, bklen * 8, &aes);

	/* Base64 decode str. */
	elen = slen / 4 * 3;
	enc = alloca(elen);

	i = decode_b64(str, slen, enc);
	if (i < 0)
		return NULL;
	elen -= i;

	r = elen - ivlen;
	if (r <= 0) {
		errno = EINVAL;
		return NULL;
	}

	if (resbuf == NULL) {
		res = malloc(r + 1);
		if (!res)
			return NULL;
	} else {
		if (*reslen < r + 1)
			return NULL;
		res = resbuf;
	}

	/* Uncipher text into res. */
	num = 0;
	AES_cfb128_encrypt(enc + ivlen, (unsigned char*) res, r, &aes, enc, &num, AES_DECRYPT);
	res[r] = '\0';

	if (reslen)
		*reslen = r;
	return res;
}


char *
aes_cbc_encode(const void *inbuf, int inlen, const char *nonce, const char *key, int klen) {
	unsigned char *enc;
	unsigned char *block_buf;
	int elen;
	const int ivlen = 128 / 8;
	const int bklen = 128 / 8;
	unsigned char iv_buf[ivlen];
	unsigned char nonce_buf[bklen];
	int i;
	unsigned char *binkey;
	AES_KEY aes;
	int pad = 0;

	if (inlen == -1)
		inlen = strlen(inbuf);

	pad = (16 - inlen % 16) % 16;

	/* Base64 decode key. */
	if (klen == -1)
		klen = strlen(key);
	if (klen != (bklen + 2) / 3 * 4) {
		errno = EINVAL;
		return NULL;
	}
	binkey = alloca(bklen + 2);
	i = decode_b64(key, klen, binkey);
	if (i < 0)
		return NULL;
	AES_set_encrypt_key(binkey, bklen * 8, &aes);

	/* Setup nonce in a buffer for encryption. Input is treated as a string, and padded to one block if needed. */
	generate_iv(nonce_buf, nonce);

	/* Encrypt nonce using same key as message to use as IV */
	AES_ecb_encrypt(nonce_buf, iv_buf, &aes, AES_ENCRYPT);

	elen = inlen + pad;
	enc = alloca(ivlen + elen);

	/* Padd the in buf, should be divisble by 16*/
	block_buf = zmalloc(inlen + pad);
	memcpy(block_buf, inbuf, inlen);

	/* Store iv and ciphertext in enc. */
	memcpy(enc, iv_buf, ivlen);
	AES_cbc_encrypt(block_buf, enc + ivlen, inlen + pad, &aes, iv_buf, AES_ENCRYPT);
	free(block_buf);

	/* Finally base64 encode the result. */
	return encode_b64(enc, ivlen + elen);
}

void *
aes_cbc_decode(const char *str, int slen, const char *key, int klen, int *reslen) {
	return aes_cbc_decode_buf(str, slen, key, klen, NULL, reslen);
}

void *
aes_cbc_decode_buf(const char *str, int slen, const char *key, int klen, void *resbuf, int *reslen) {
	const int ivlen = 128 / 8;
	unsigned char *enc;
	int elen;
	AES_KEY aes;
	int i;
	int r;
	char *res;
	const int bklen = 128 / 8;
	unsigned char *binkey;

	if (slen == -1)
		slen = strlen(str);
	if (!slen)
		return strdup("");

	if (slen % 4) {
		errno = EINVAL;
		return NULL;
	}
	
	/* Base64 decode key. */
	if (klen == -1)
		klen = strlen(key);
	if (klen != (bklen + 2) / 3 * 4) {
		errno = EINVAL;
		return NULL;
	}
	binkey = alloca(bklen + 2);
	i = decode_b64(key, klen, binkey);
	if (i < 0)
		return NULL;
	AES_set_decrypt_key(binkey, bklen * 8, &aes);

	/* Base64 decode str. */
	elen = slen / 4 * 3;
	enc = alloca(elen);

	i = decode_b64(str, slen, enc);
	if (i < 0)
		return NULL;
	elen -= i;

	r = elen - ivlen;
	if (r <= 0) {
		errno = EINVAL;
		return NULL;
	}

	if (resbuf == NULL) {
		res = malloc(r + 1);
		if (!res)
			return NULL;
	} else {
		if (*reslen < r + 1)
			return NULL;
		res = resbuf;
	}

	/* Uncipher text into res. */
	AES_cbc_encrypt(enc + ivlen, (unsigned char*) res, r, &aes, enc, AES_DECRYPT);
	res[r] = '\0';

	if (reslen)
		*reslen = r;
	return res;
}

char *
aes_encode_sign(const void *inbuf, int inlen, int pad, const char *nonce, const char *key, int klen, const char *sign_key) {
	unsigned char *enc;
	int elen;
	const int ivlen = 128 / 8;
	const int bklen = 128 / 8;
	unsigned char iv_buf[ivlen];
	unsigned char nonce_buf[bklen];
	int num;
	int i;
	unsigned char *binkey;
	AES_KEY aes;
	unsigned char *padbuf = NULL;
	const EVP_MD *evp_hmac = EVP_sha256();
	size_t hmac_size = EVP_MD_size(evp_hmac);
	int sign_size = hmac_size;

	if (inlen == -1)
		inlen = strlen(inbuf) + 1;
	if (pad < 1)
		pad = 1;

	size_t hmac_klen = 0;
	char *hmac_key = base64_decode_new(sign_key, &hmac_klen);
	if (!hmac_key)
		return NULL;

	/* Base64 decode key. */
	if (klen == -1)
		klen = strlen(key);
	if (klen != (bklen + 2) / 3 * 4) {
		errno = EINVAL;
		return NULL;
	}
	binkey = alloca(bklen + 2);
	i = decode_b64(key, klen, binkey);
	if (i < 0)
		return NULL;
	AES_set_encrypt_key(binkey, bklen * 8, &aes);

	/* NIST SP 800-38A: "The first method is to apply the forward cipher function, under the same key
	   that is used for the encryption of the plaintext, to a nonce. The nonce must be a data block
	   that is unique to each execution of the encryption operation." */

	/* Setup nonce in a buffer for encryption. Input is treated as a string, and padded to one block if needed. */
	generate_iv(nonce_buf, nonce);

	/* Encrypt nonce using same key as message to use as IV */
	AES_ecb_encrypt(nonce_buf, iv_buf, &aes, AES_ENCRYPT);

	if (pad <= 1)
		pad = 0;
	else
		pad -= (inlen + ivlen) % pad;
	elen = inlen + pad;
	enc = alloca(ivlen + elen + sign_size);

	/* Store iv and ciphertext in enc. */
	memcpy(enc, iv_buf, ivlen);
	num = 0;
	AES_cfb128_encrypt(inbuf, enc + ivlen, inlen, &aes, iv_buf, &num, AES_ENCRYPT);
	if (pad > 0) {
		padbuf = alloca(pad);
		memset(padbuf, 0, pad);
		AES_cfb128_encrypt(padbuf, enc + ivlen + inlen, elen - inlen, &aes, iv_buf, &num, AES_ENCRYPT);
	}

	/*
		HMAC() computes the message authentication code of the n bytes at d using the hash function evp_md and the key key
		which is key_len bytes long.

		It places the result in md (which must have space for the output of the hash function, which is no more than
		EVP_MAX_MD_SIZE bytes).  The size of the output is placed in md_len, unless it is NULL.
	*/
	unsigned char hmac_result[hmac_size];
	if (!HMAC(evp_hmac, hmac_key, hmac_klen, enc, ivlen+elen, hmac_result, NULL)) {
		free(hmac_key);
		return NULL;
	}
	free(hmac_key);

	/* Copy signature into place */
	memcpy(enc + ivlen + elen, hmac_result, sign_size);

	/* Finally base64 encode the result. */
	return encode_b64(enc, ivlen + elen + sign_size);
}

void *
aes_decode_signed(const char *str, int slen, const char *key, int klen, int *reslen, const char *sign_key, int *verified) {
	return aes_decode_signed_buf(str, slen, key, klen, NULL, reslen, sign_key, verified);
}

void *
aes_decode_signed_buf(const char *str, int slen, const char *key, int klen, void *resbuf, int *reslen, const char *sign_key, int *verified) {
	const int ivlen = 128 / 8;
	unsigned char *enc;
	int elen;
	AES_KEY aes;
	int i;
	int r;
	char *res;
	int num;
	const int bklen = 128 / 8;
	unsigned char *binkey;
	const EVP_MD *evp_hmac = EVP_sha256();
	size_t hmac_size = EVP_MD_size(evp_hmac);
	int sign_size = hmac_size;

	if (slen == -1)
		slen = strlen(str);
	if (!slen)
		return strdup("");

	if (slen % 4) {
		errno = EINVAL;
		return NULL;
	}

	/* Base64 decode key. */
	if (klen == -1)
		klen = strlen(key);
	if (klen != (bklen + 2) / 3 * 4) {
		errno = EINVAL;
		return NULL;
	}
	binkey = alloca(bklen + 2);
	i = decode_b64(key, klen, binkey);
	if (i < 0)
		return NULL;
	AES_set_encrypt_key(binkey, bklen * 8, &aes);

	/* Base64 decode str. */
	elen = slen / 4 * 3;
	enc = alloca(elen);

	i = decode_b64(str, slen, enc);
	if (i < 0)
		return NULL;
	elen -= i;
	int sign_start = elen - sign_size;

	r = elen - ivlen - sign_size;
	if (r <= 0) {
		errno = EINVAL;
		return NULL;
	}

	/* Decode signature key */
	size_t hmac_klen = 0;
	char *hmac_key = base64_decode_new(sign_key, &hmac_klen);
	if (!hmac_key)
		return NULL;

	/* Calculate signature key */
	unsigned char hmac_result[hmac_size];
	if (!HMAC(evp_hmac, hmac_key, hmac_klen, enc, ivlen+r, hmac_result, NULL)) {
		free(hmac_key);
		return NULL;
	}
	free(hmac_key);

	/* Verify signature */
	int ok = memcmp(hmac_result, enc + sign_start, sign_size) == 0;
	if (verified)
		*verified = ok;
	if (!ok)
		return NULL;

	if (resbuf == NULL) {
		res = malloc(r + 1);
		if (!res)
			return NULL;
	} else {
		if (*reslen < r + 1)
			return NULL;
		res = resbuf;
	}

	/* Uncipher text into res. */
	num = 0;
	AES_cfb128_encrypt(enc + ivlen, (unsigned char*) res, r, &aes, enc, &num, AES_DECRYPT);
	res[r] = '\0';

	if (reslen)
		*reslen = r;
	return res;
}

