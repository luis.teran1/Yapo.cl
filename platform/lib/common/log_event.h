#ifndef EVENT_H
#define EVENT_H

#include "macros.h"

#ifdef __cplusplus
extern "C"
{
#endif /* __cplusplus */
struct bpapi_vtree_chain;
int log_event(const char *event);
int syslog_ident(const char *ident, const char *fmt, ...) FORMAT_PRINTF(2, 3) WEAK;
int stat_log(struct bpapi_vtree_chain *vtree, const char *event, const char *id);

#ifdef __cplusplus
}
#endif /* __cplusplus */
#endif
