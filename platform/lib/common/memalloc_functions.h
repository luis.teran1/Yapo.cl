#ifndef PLATFORM_MEMALLOC_FUNCTIONS_H
#define PLATFORM_MEMALLOC_FUNCTIONS_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stddef.h>
#include <stdarg.h>
#include "macros.h"

/*
 * Wrappers for memory allocation functions.
 * Guaranteed to return non-NULL.
 */
void *xmalloc(size_t size) ALLOCATOR;
void *xcalloc(size_t count, size_t size) ALLOCATOR;
void *zmalloc(size_t size) ALLOCATOR;
void *xrealloc(void *ptr, size_t size) ALLOCATOR;
char *xstrdup(const char *orig) ALLOCATOR;
char *xstrndup(const char *orig, size_t n) ALLOCATOR;
int xasprintf(char **strp, const char *fmt, ...) FORMAT_PRINTF(2, 3);
int xvasprintf(char **strp, const char *fmt, va_list ap);

#ifdef __cplusplus
}
#endif

#endif

