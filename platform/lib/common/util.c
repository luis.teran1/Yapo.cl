#include "util.h"

const char *debug_levels[] = {
	[D_CRIT] = "CRIT",
	[D_ERROR] = "ERROR",
	[D_WARNING] = "WARNING",
	[D_INFO] = "INFO",
	[D_DEBUG] = "DEBUG"
};

