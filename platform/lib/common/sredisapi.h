#ifndef SREDISAPI_H
#define SREDISAPI_H

#include "hiredis.h"
#include "macros.h"

struct fd_pool;

char *session_redis_get(const char *session_id, const char *key, struct fd_pool *pool);
int session_redis_set(const char *session_id, const char *key, const char *value, int expire, struct fd_pool *pool);
int session_redis_incr(const char *session_id, const char *key, struct fd_pool *pool);

struct fd_pool_conn *redis_sock_conn(struct fd_pool *pool, const char *host);
redisReply *redis_conn_send(struct fd_pool_conn *conn, int argc, const char **argv, const size_t *argvlen);

int redis_sock_zadd(struct fd_pool_conn *conn, const char *key, int score, const char *member);
int redis_sock_zrem(struct fd_pool_conn *conn, const char *key, const char *member);
int redis_sock_zscore(struct fd_pool_conn *conn, const char *key, const char *member);
int redis_sock_zrank(struct fd_pool_conn *conn, const char *key, const char *member);
int redis_sock_del(struct fd_pool_conn *conn, const char *key);
int redis_sock_zincrby(struct fd_pool_conn *conn, const char *key, int incrby, const char *member);
int redis_sock_set(struct fd_pool_conn *conn, const char *key, const char *value, int expire);
int redis_sock_setnx(struct fd_pool_conn *conn, const char *key, const char *value);
char *redis_sock_get(struct fd_pool_conn *conn, const char *key);
int redis_sock_hset(struct fd_pool_conn *conn, const char *key, const char *field, const char *value);
int redis_sock_hmset_array(struct fd_pool_conn *conn, const char *key, int nargs, const char *args[nargs]);
int redis_sock_hmset(struct fd_pool_conn *conn, const char *key, ...) SENTINEL(0);
char *redis_sock_hget(struct fd_pool_conn *conn, const char *key, const char *field);
int redis_sock_incr(struct fd_pool_conn *conn, const char *key);
int redis_sock_expire(struct fd_pool_conn *conn, const char *key, unsigned int seconds);
int redis_sock_ttl(struct fd_pool_conn *conn, const char *key);
int redis_sock_hdel(struct fd_pool_conn *conn, const char *set, const char *key);
int redis_sock_hincrby(struct fd_pool_conn *conn, const char *hash, const char* key, int incrby);
int redis_sock_publish(struct fd_pool_conn *conn, const char *channel, const char *message);
int redis_sock_rename(struct fd_pool_conn *conn, const char *key, const char *newkey);

int redis_sock_exists(struct fd_pool_conn *conn, const char *key);
int redis_sock_hexists(struct fd_pool_conn *conn, const char *key, const char *field);
int redis_sock_hlen(struct fd_pool_conn *conn, const char *key);

int redis_sock_sadd(struct fd_pool_conn *conn, const char *key, const char *member);
int redis_sock_srem(struct fd_pool_conn *conn, const char *key, const char *member);
int redis_sock_sismember(struct fd_pool_conn *conn, const char *key, const char *member);
int redis_sock_scard(struct fd_pool_conn *conn, const char *set);

int redis_sock_llen(struct fd_pool_conn *conn, const char *key);
char *redis_sock_lindex(struct fd_pool_conn *conn, const char *key, int idx);

int redis_sock_lpush(struct fd_pool_conn *conn, const char *key, const char *value);
int redis_sock_rpush(struct fd_pool_conn *conn, const char *key, const char *value);
char *redis_sock_rpop(struct fd_pool_conn *conn, const char *key);

/* Multi value response */
int redis_sock_keys(struct fd_pool_conn *conn, const char *key, void (*cb)(const void *value, size_t vlen, void *cbarg), void *cbarg);
int redis_sock_hgetall(struct fd_pool_conn *conn, const char *key, void (*cb)(const void *key, size_t klen, const void *value, size_t vlen, void *cbarg), void *cbarg);
int redis_sock_zrange(struct fd_pool_conn *conn, const char *key, int start, int lim, void (*cb)(const void *value, size_t vlen, const void *score, size_t slen, void *cbarg), void *cbarg);
int redis_sock_zrevrange(struct fd_pool_conn *conn, const char *key, int start, int lim, void (*cb)(const void *value, size_t vlen, const void *score, size_t slen, void *cbarg), void *cbarg);
int redis_sock_zrangebyscore(struct fd_pool_conn *conn, const char *key, const char *min, const char *max, int offset, int count, void (*cb)(const void *value, size_t vlen, const void *score, size_t slen, void *cbarg), void *cbarg);
int redis_sock_lrange(struct fd_pool_conn *conn, const char *key, int start, int lim, void (*cb)(const void *value, size_t vlen, void *cbarg), void *cbarg);
int redis_sock_sinter(struct fd_pool_conn *conn, const char *set_a, const char *set_b, void (*cb)(const void *value, size_t vlen, void *cbarg), void *cbarg);
int redis_sock_smembers(struct fd_pool_conn *conn, const char *set, void (*cb)(const void *value, size_t vlen, void *cbarg), void *cbarg);
int redis_sock_evalsha(struct fd_pool_conn *conn, const char *sha, int nkeys, int nargs, const char *args[nargs], void (*cb)(const void *value, size_t vlen, void *cbarg), void *cbarg);

/* Transaction API */
int redis_sock_watch(struct fd_pool_conn *conn, const char *key);
int redis_sock_multi(struct fd_pool_conn *conn);
int redis_sock_exec(struct fd_pool_conn *conn);

/* Multi mode variations (Use these under multi mode) */
int redis_multi_sock_del(struct fd_pool_conn *conn, const char *key);

#endif
