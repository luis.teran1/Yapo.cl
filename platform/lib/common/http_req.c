#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <netdb.h>
#include <event.h>
#include <string.h>
#include <fcntl.h>
#include <time.h>
#include <errno.h>
#include <limits.h>
#include <netinet/tcp.h>
#include <ctype.h>

#include "http_req.h"
#include "logging.h"
#include "util.h"
#include "network.h"

static int start_request(struct request *);
static void end_request(struct request *, int);
static void finish_connect(int, void *);

static void read_ev(int, short, void *);
static void write_ev(int, short, void *);

int
http_req(struct request *req) {
	req->fd = -1;
	req->dstfd = -1;

	if (req->dstfile) {
		mode_t mode = S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH;
		req->dstfd = open(req->dstfile, O_WRONLY|O_CREAT, mode);
		if (req->dstfd == -1) {
			req->result = RESULT_OPENFILE;
			req->cb_done(req);
			return -1;
		}
		fcntl(req->dstfd, F_SETFD, FD_CLOEXEC);
		req->buffer = zmalloc(READ_BUFSIZE);
		req->read = 0;
	}

	return start_request(req);
}

static void
read_ev(int fd, short ev, void *v)
{
	struct request *req = v;
	char buffer[65536];
	ssize_t sz;
	ssize_t wrote UNUSED;
	int remaining;

	if (req->dstfd >= 0) {
		char *startcontent = NULL;
		char *p = req->buffer + req->read;

		if (req->read < READ_BUFSIZE-1) {
			/* Fill the internal buffer */
			while (req->read < READ_BUFSIZE-1) {
				remaining = READ_BUFSIZE - 1 - req->read;
				sz = read(req->fd, p, remaining);

				if (sz < 0) {
					if (errno == EAGAIN)
						return;
					end_request(req, RESULT_READERR);
					return;
				}
				if (sz == 0) {
					/* Nothing more to read, exit the loop and handle
					 * the buffer */
					break;
				}

				req->read += sz;
				p += sz;
			}
			*(p+1) = '\0';

			/* either there is nothing more to read or the buffer is full
			 * so look for the end of the headers and write the file */
			startcontent = strstr(req->buffer, "\r\n\r\n");

			if (startcontent) {
				startcontent += 4;
				/* Write to the dst fd */
				wrote = write(req->dstfd, startcontent, p - startcontent);
			} else {
				wrote = write(req->dstfd, req->buffer, req->read);
			}
		}

		/* if there is more data to read, read and write everyting
		 * Reset the buffer, but keep
		 * req->read set in order to avoid the loop go back into
		 * the "buffer filing" portion... we have already stripped the
		 * http headers here */
		bzero(req->buffer, READ_BUFSIZE);
		sz = read(req->fd, req->buffer, READ_BUFSIZE - 1);
		if (sz > 0) {
			wrote = write(req->dstfd, req->buffer, sz);
			do {
				sz = read(req->fd, req->buffer, READ_BUFSIZE - 1);
				if (sz > 0) {
					wrote = write(req->dstfd, req->buffer, sz);
				} else if (sz < 0) {
					if (errno == EAGAIN) {
						return;
					}
					end_request(req, RESULT_READERR);
					return;
				} else if (sz == 0){
					break;
				}
			} while (sz > 0);
		} else if (sz < 0) {
			if (errno == EAGAIN) {
				return;
			}
			end_request(req, RESULT_READERR);
			return;
		} else if (sz == 0) {
			close(req->dstfd);
			if (req->buffer)
				free(req->buffer);
			end_request(req, req->result ? req->result : RESULT_DISCONNECT);
		}
	} else {
		do {
			if (req->tls)
				sz = tls_read (req->tls, buffer, sizeof (buffer) - 1);
			else
				sz = read(req->fd, buffer, sizeof(buffer)-1);
			if (sz < 0) {
				if (errno == EAGAIN)
					return;
				end_request(req, RESULT_READERR);
				return;
			}
			if (sz == 0) {
				end_request(req, req->result ? req->result : RESULT_DISCONNECT);
				return;
			}

			if (req->databuf)
				evbuffer_add(req->databuf, buffer, sz);

			if (req->result == 0) {
				char *stat = NULL;
				buffer[sz] = 0;
				if (!(strncmp(buffer, "HTTP/1.", 7) == 0 &&
				      ((stat = strchr(buffer, ' ')) &&
				       sscanf(stat + 1, "%d", &req->result) == 1))) {
					xwarnx("%.*s", sz > 256 ? 256 : (int)sz, buffer);
					end_request(req, RESULT_PROTOERR);
					return;
				}
			}
		} while (sz > 0);
	}
}

static void
write_ev(int fd, short ev, void *v)
{
	struct request *req = v;
	ssize_t sz;

	if (!req->header_sent) {
		if (req->tls)
			sz = tls_write(req->tls, &req->header[req->hsize - req->resid], req->resid);
		else
			sz = write(req->fd, &req->header[req->hsize - req->resid], req->resid);
		if (sz == req->resid && req->requestdata) {
			req->header_sent = 1;
			req->resid = req->reqsize;
			if (req->tls)
				sz = tls_write(req->tls, &req->requestdata[req->reqsize - req->resid], req->resid);
			else
				sz = write(req->fd, &req->requestdata[req->reqsize - req->resid], req->resid);
		}
	} else {
		if (req->tls)
			sz = tls_write(req->tls, &req->requestdata[req->reqsize - req->resid], req->resid);
		else
			sz = write(req->fd, &req->requestdata[req->reqsize - req->resid], req->resid);
	}
	if (sz < 0) {
		end_request(req, RESULT_WRITEERR);
		return;
	}
	if ((req->resid -= sz) == 0) {
		event_del(&req->ev_write);
		event_add(&req->ev_read, NULL);
	}
}

static int
start_request(struct request *req)
{
	req->result = 0;
	req->resid = req->hsize;

	if (net_connect_host_port(req->event_base, SOCK_STREAM, 0, req->timeout, req->server, req->port, finish_connect, req)) {
		end_request(req, RESULT_BADCONNECT);
		return -1;
	}
	return 0;
}

static void finish_tls_connect_ev (int fd, short ev, void *cbarg);

static int
finish_tls_connect(struct request *req)
{
	int err;

	switch ((err = tls_connect (req->tls))) {
	case 0: /* Done */
	{
		tlscert_t cert = tls_get_peer_cert (req->tls);
		if (cert) {
			char cn[256];
			int cnlen = tls_get_cn (cert, cn, sizeof (cn));
			int slen = strlen(req->server);
			int i = 0;
			int j = 0;

			/* XXX we should probably find some third party implementation of this */
			/* * at the end does not match but I think that's just as well. */
			while (i < cnlen && j < slen) {
				if (cn[i] == '*') {
					if (req->server[j] == '.')
						i++;
					else
						j++;
					continue;
				}
				if (tolower(cn[i]) != tolower(req->server[j]))
					break;
				i++;
				j++;
			}
			if (i == cnlen && j == slen) {
				tls_free_cert (cert);

				event_del(&req->ev_write);
				event_set(&req->ev_write, req->fd, EV_WRITE | EV_PERSIST, write_ev, req);
				event_base_set(req->event_base, &req->ev_write);
				event_add(&req->ev_write, NULL);
				event_del(&req->ev_read);
				event_set(&req->ev_read, req->fd, EV_READ|EV_PERSIST, read_ev, req);
				event_base_set(req->event_base, &req->ev_read);
				return 0;
			}
			xwarnx ("common name mismatch: %s (%d) <> %s (%d)", cnlen < 0 ? NULL : cn, cnlen, req->server, slen);
			tls_free_cert (cert);
		} else
			xwarnx ("failed to get peer certificate");
		end_request(req, RESULT_TLS_BAD_CN);
		return 1;
	}

	case 1: /* Need read */
		event_del(&req->ev_read);
		event_set(&req->ev_read, req->fd, EV_READ, finish_tls_connect_ev, req);
		event_base_set(req->event_base, &req->ev_read);
		event_add(&req->ev_read, NULL);
		return 0;
	case 2: /* Need write */
		event_del(&req->ev_write);
		event_set(&req->ev_write, req->fd, EV_WRITE, finish_tls_connect_ev, req);
		event_base_set(req->event_base, &req->ev_write);
		event_add(&req->ev_write, NULL);
		return 0;
	default:
		xwarnx("tls_connect: %s (%d)", tls_error(req->tls, err), err);
		end_request(req, RESULT_TLS_BADCONNECT);
		return -1;
	}
}

static void
finish_tls_connect_ev (int fd, short ev, void *cbarg)
{
	finish_tls_connect (cbarg);
}

static void
finish_connect(int fd, void *v)
{
	struct request *req = v;
	int val;

	switch (fd) {
	case NET_TIMEOUT_CONNECT:
		end_request(req, RESULT_TIMEOUT);
		return;
	case NET_ERROR_CONNECT:
		end_request(req, RESULT_BADCONNECT);
		return;
	}

	req->fd = fd;
	setsockopt( req->fd, IPPROTO_TCP, TCP_NODELAY, (char *)&req->tcp_nodelay, sizeof(req->tcp_nodelay) );

	if ((val = fcntl(req->fd, F_GETFL, 0)) < 0)
		xerr(1, "fcntl");
	val |= O_NONBLOCK;
	if (fcntl(req->fd, F_SETFL, val) < 0)
		xerr(1, "fcntl");
	if ((val = fcntl(req->fd, F_GETFD, 0)) < 0)
		xerr(1, "fcntl");
	val |= FD_CLOEXEC;
	if (fcntl(req->fd, F_SETFD, val) < 0)
		xerr(1, "fcntl");

	event_set(&req->ev_read, req->fd, EV_READ|EV_PERSIST, read_ev, req);
	event_base_set(req->event_base, &req->ev_read);

	if (req->tls_ctx) {
		int tls_options = tlsVerifyPeer;

		if (req->tls_option_noVerifyPeer) {
			tls_options &= ~tlsVerifyPeer;
		}

		req->tls = tls_open (req->tls_ctx, req->fd, tls_options, NULL, NULL);
		if (!req->tls)
			xerrx(1, "tls_open: %s", tls_error(req->tls, 0));
		tls_start(req->tls);
		event_del(&req->ev_write);
		event_set(&req->ev_write, req->fd, EV_WRITE|EV_PERSIST, finish_tls_connect_ev, req);
		event_base_set(req->event_base, &req->ev_write);
		event_add(&req->ev_write, NULL);
		return;
	}
		

	event_del(&req->ev_write);
	event_set(&req->ev_write, req->fd, EV_WRITE|EV_PERSIST, write_ev, req);
	event_base_set(req->event_base, &req->ev_write);
	event_add(&req->ev_write, NULL);
	return;
}

static void
end_request(struct request *req, int result)
{
	event_del(&req->ev_read);
	event_del(&req->ev_write);

	if (req->fd >= 0)
		close(req->fd);

	if (req->dstfd >= 0)
		close(req->dstfd);

	if (req->tls) {
		tls_free(req->tls);
		free(req->tls);
	}

	req->result = result;
	req->cb_done(req);
}

const char *
http_req_result(int status)
{
	static char static_buffer[1024];

	switch(status) {
	case 200:
	case 204:
		return "OK";
	case 201:
		return "Created OK";
	case 301:
		return "Moved Permanently";
	case 302:
		return "Found (Temp. Moved)";
	case 303:
		return "See Other";
	case 304:
		return "Not Modified";
	case 307:
		return "Temporary Redirect";
	case 400:
		return "Bad Request";
	case 403:
		return "Forbidden";
	case 404:
		return "Not Found";
	case 408:
		return "Request Timeout";
	case 500:
		return "Internal Server Error";
	case 501:
		return "Not Implemented";
	case RESULT_TIMEOUT:
		return "Timeout";
	case RESULT_BADSOCKET:
		return "Bad socket";
	case RESULT_DISCONNECT:
		return "Server disconnect";
	case RESULT_WRITEERR:
		return "Write error";
	case RESULT_READERR:
		return "Read error";
	case RESULT_SHORTREAD:
		return "Short read";
	case RESULT_PROTOERR:
		return "Protocol error";
	case RESULT_BADCONNECT:
		return "Connection failed";
	case RESULT_GETADDRINFO:
		return "Getaddrinfo failed";
	case RESULT_TLS_BADCONNECT:
		return "TLS negotiation failed";
	case RESULT_TLS_BAD_CN:
		return "Commonname mismatch";
	default:
		snprintf(static_buffer, sizeof(static_buffer), "unknown %d", status);
		return static_buffer;
	}
}
