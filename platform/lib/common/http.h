#ifndef HTTP_H
#define HTTP_H
#include <curl/curl.h>
#include "buf_string.h"

typedef size_t (*http_response_cb)(void *buffer, size_t size, size_t nmemb, void *cb_data);
typedef int (*http_extra_config_cb)(CURL *ch, void *data);

struct http {
	const char *url;
	const char *method;
	const void *body;
	size_t body_length;
	const char **headers;
	http_response_cb write_function;
	http_response_cb header_write_function;
	http_extra_config_cb extra_config_function;
	void *extra_config_data;	

	char error[CURL_ERROR_SIZE];
	CURL *ch;
	CURLcode curl_status;

	int http_status;
	struct buf_string *response_body;
	struct buf_string *response_header;
};


long http_delete(const char *url, const char *headers[]);
long http_get(const char *url, struct buf_string *response, const char *headers[]);
long http_post(const char *url, struct buf_string *response, const char *poststr, const char *headers[]);
long http_put_str(const char *url, const char *data, const char *headers[]);
long http_put_bin(const char *url, void *data, size_t len, const char *headers[]);
long http_move(const char *url, const char *dest, const char *headers[]);
long http_copy(const char *url, const char *dest, const char *headers[]);


struct http *http_create();
long http_perform(struct http *);
void http_free(struct http *);

#endif
