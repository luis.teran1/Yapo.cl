#ifndef CACHEAPI_H
#define CACHEAPI_H

#include "vtree.h"

struct fd_pool;
struct bpapi_vtree_chain;

struct cache_pools {
	struct bpapi_vtree_chain redis_node;
	struct fd_pool *redis;
	struct bpapi_vtree_chain memcache_node;
	struct fd_pool *memcache;
	int write_both;
};

/*
	Returns a positive integer if any pools were initialized, or a negative
	integer on configuration error.
*/
int cache_init_pools(struct cache_pools *pools, struct bpapi_vtree_chain *vtree);
void cache_free_pools(struct cache_pools *pools);

char *cache_get(struct cache_pools *pools, const char *session_id, const char *key);
int cache_set(struct cache_pools *pools, const char *session_id, const char *key, const char *value, int expire);

#endif
