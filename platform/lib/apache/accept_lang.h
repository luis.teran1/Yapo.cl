#ifndef ACCEPT_LANG_H
#define ACCEPT_LANG_H
#include <httpd.h>
#include <apr_strings.h>
#include "bconf.h"

enum language_conf_type {
	TYPE_NONE,
	TYPE_TEMPLATES_CONF,
	TYPE_LIST_CONF
};

struct language_setup_data {
	enum language_conf_type type;
	void *conf;
	struct bconf_node* tmpl_root;

	/* output */
	const char *lang;
	const char *country;
	void *user_data;
};

char * best_accept_language(request_rec *r, struct bconf_node *node);
char * extract_subdomain(request_rec *r);
char * subdomain_language(request_rec * r, struct bconf_node * node);
int language_setup(request_rec *r, struct bconf_node *bconf_root, struct language_setup_data *lsd) WEAK;

#endif /*ACCEPT_LANG_H*/
