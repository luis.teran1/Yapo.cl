#ifndef COOKIES_H
#define COOKIES_H

#include <httpd.h>
char *get_cookie_with_count(request_rec *r, const char *param, int *count);
char *get_cookie(request_rec *r, const char *param);

void set_cookie_with_expiry_seconds(request_rec *r, const char *param, const char *value, const char *domain, int seconds, int httponly);
void set_cookie_with_expiry_minutes(request_rec *r, const char *param, const char *value, const char *domain, int minutes, int httponly);
void set_cookie_with_expiry(request_rec *r, const char *param, const char *value, const char *domain, int days, int httponly);
void set_cookie(request_rec *r, const char *param, const char *value, const char *domain, int httponly);
void set_secure_cookie_with_expiry_seconds(request_rec *r, const char *param, const char *value, const char *domain, int seconds);
void set_secure_cookie_with_expiry_minutes(request_rec *r, const char *param, const char *value, const char *domain, int minutes);
void set_secure_cookie_with_expiry(request_rec *r, const char *param, const char *value, const char *domain, int days);
void set_secure_cookie(request_rec *r, const char *param, const char *value, const char *domain);
void clear_cookie(request_rec *r, const char *param, const char *domain);

#endif /*COOKIES_H*/
