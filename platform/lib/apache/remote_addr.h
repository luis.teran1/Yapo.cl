#ifndef REMOTE_ADDR_H
#define REMOTE_ADDR_H

#include <httpd.h>

struct bconf_node;

const char *get_remote_addr(request_rec *r, struct bconf_node *root);
const char *get_forwarded_proto(request_rec *r, struct bconf_node *root);

#endif /*REMOTE_ADDR_H*/
