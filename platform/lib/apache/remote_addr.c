
#include "remote_addr.h"
#include "bconf.h"

#include <apr.h>
#include <apr_strings.h>
#include <http_protocol.h>
#include <http_log.h>

static int
verify_proxy(const char *ra, struct bconf_node *proxies) {
	int i;

	for (i = 0 ; i < bconf_count(proxies) ; i++) {
		const char *p = bconf_get_string(bconf_byindex(proxies, i), "name");

		if (p && strcmp(ra, p) == 0)
			return 1;
	}
	return 0;
}

static const char *
get_conn_ip(request_rec *r) {
	const char *ra;
#if AP_MODULE_MAGIC_AT_LEAST(20111130,0)
	ra = r->connection->client_ip;
#else
	ra = r->connection->remote_ip;
#endif

	return ra;
}

const char *
get_remote_addr(request_rec *r, struct bconf_node *root) {
	const char *ra = get_conn_ip(r);
	const char *fw = apr_table_get(r->headers_in, "X-Forwarded-For");
	const char *comma;
	struct bconf_node *proxies = bconf_get(root, "common.webfront.proxy.host");

	if (!ra)
		return NULL;

	if (strncmp(ra, "::ffff:", 7) == 0)
		ra += 7;

 	if (!bconf_get_int(root, "common.webfront.proxy.enabled"))
 		return ra;

 	/* nginx uses X-Real-IP, favor it if that is our reverse proxy */
 	if (bconf_get_int(root, "common.webfront.proxy.use_real_ip")) {
 		const char *real = apr_table_get(r->headers_in, "X-Real-IP");
 		if (real)
 			fw = real;
 	}

	if (!fw)
		return ra;

	if (!verify_proxy(ra, proxies)) {
		ap_log_error(APLOG_MARK, APLOG_DEBUG, OK, NULL, "Remote IP %s is not in list of proxies!", ra);
		return ra;
	}

	if (strncmp(fw, "::ffff:", 7) == 0)
		fw += 7;

	if ((comma = strchr(fw, ',')))
		fw = apr_pstrndup(r->pool, fw, comma - fw);

	/* XXX check fw format? */

	return fw;
}

const char *
get_forwarded_proto(request_rec *r, struct bconf_node *root) {
	if (!bconf_get_int(root, "common.webfront.proxy.proto.nocheck")) {
		const char *ra = get_conn_ip(r);

		if (!ra || (!bconf_get_int(root, "common.webfront.proxy.enabled") && !bconf_get_int(root, "common.webfront.proxy.proto_only")))
			return ap_run_http_scheme(r);

		if (strncmp(ra, "::ffff:", 7) == 0)
			ra += 7;

		if (!verify_proxy(ra, bconf_get(root, "common.webfront.proxy.host")))
			return ap_run_http_scheme(r);
	}

	const char *xfp = apr_table_get(r->headers_in, "X-Forwarded-Proto");
	if (xfp != NULL) {
		if (strncmp(xfp, "https", 5) == 0) {
			return "https";
		} else if (strncmp(xfp, "http", 4) == 0) {
			return "http";
		}
	}

	return ap_run_http_scheme(r);
}
