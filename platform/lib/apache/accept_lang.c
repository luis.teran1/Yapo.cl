#include "accept_lang.h"
#include "cookies.h"
#include <http_core.h>
#include <http_log.h>

char *
best_accept_language(request_rec *r, struct bconf_node *node) {
	const char *language_header;
	char *langs;
	char *lang;
	char *strtok_buf = NULL;
	const char *bestq = NULL;
	char *bestlang = NULL;

	language_header = apr_table_get(r->headers_in, "Accept-Language");

	if (!language_header) {
		return NULL;
	}

	langs = apr_pstrdup(r->pool, language_header);

	lang = strtok_r(langs, ",", &strtok_buf);
	while (lang) {
		char *q;
		char *l = apr_pstrndup(r->pool, lang, 2);

		if (bconf_get(node, l)) {
			if ((q = strchr(lang, ';')) != NULL) {
				if (!bestq || strcmp(q, bestq) > 0) {
					bestq = q;
					bestlang = lang;
				}
			} else {
				bestq = ";q=1.0";
				bestlang = lang;
			}
		}
		lang = strtok_r(NULL, ",", &strtok_buf);
	}

	if (!bestlang)
		return NULL;

	return apr_pstrndup(r->pool, bestlang, 2);
}

char * 
subdomain_language(request_rec * r, struct bconf_node * node) {
	char *bestlang = NULL;

	// Subdomain (Kapaza) - Subdomain dominates even the language header.
	const char *subdomain = extract_subdomain(r);
	ap_log_error(APLOG_MARK, APLOG_WARNING, OK, NULL, "subdomain: %s", subdomain);
	if(subdomain) {
		// Test if we have the subdomain has a lang in bconf.
		const char *subdomain_lang = bconf_get_string(node, subdomain);
		ap_log_error(APLOG_MARK, APLOG_WARNING, OK, NULL, "subdomain_lang: %s", subdomain_lang);
		if(subdomain_lang) {
			bestlang = apr_pstrdup(r->pool, subdomain_lang);
			ap_log_error(APLOG_MARK, APLOG_NOTICE, OK, NULL, "Robot! uri(%s) lang (%s)", r->uri, bestlang);
		}
	}

	if ( !bestlang ) { 
		return NULL ;
	}

	return apr_pstrndup(r->pool, bestlang, 2);
}


char *
extract_subdomain(request_rec *r) {

	const char *hostname = NULL;
	const char *subdomain = NULL;
	int subdomain_len;
	char *pos_d = NULL;

	/*
	 * Robots will most likely faill into the last condition... some may implement RFC rules a bit better so we care also for those
	 */
	if(apr_table_get(r->headers_in, "Host")) {
		hostname = apr_pstrdup(r->pool, apr_table_get(r->headers_in, "Host"));
	}
	else if(r->hostname) {
                hostname = apr_pstrdup(r->pool, r->hostname);
        }
	else if(ap_get_server_name(r)) {
                hostname = apr_pstrdup(r->pool, ap_get_server_name(r));
        }

	if(hostname) {
		pos_d = strchr(hostname, '.');
        	if( pos_d ) {
                	subdomain_len = (int)(strlen(hostname) - strlen(pos_d));
			if(subdomain_len > 0 && subdomain_len <= 25) {
                        	subdomain = apr_pstrndup(r->pool, hostname, subdomain_len);
                	}
		}
	}

	return (char *)subdomain;

}

/* Clients should not make any changes to this code, use a local override instead. */
int
language_setup(request_rec *r, struct bconf_node *bconf_root, struct language_setup_data *lsd) {
	const char *lang = NULL;

	if (bconf_get_int(bconf_root, "multilang.enabled")) {
		if (lsd->type == TYPE_TEMPLATES_CONF && lsd->tmpl_root) {
			/* Allow template configuration to override language detection */
			lang = bconf_get_string(lsd->tmpl_root, "lang");
			/* Allow us to make this language persistent */
			if (bconf_get_int(lsd->tmpl_root, "set_lang_cookie")) 
				set_cookie(r, "lang", lang, bconf_get_string(bconf_root, "common.cookie.domain"), 0);
		}
		if (!lang) {
			lang = get_cookie(r, "lang");
			if (lang) {
				/* if not in the list it is something else */
				if (!bconf_vget(bconf_root, "common", "lang", lang, NULL)) {
					clear_cookie(r, "lang", bconf_get_string(bconf_root, "common.cookie.domain"));
					lang = NULL;
				}
			}
		}

		if (!lang)
			lang = subdomain_language(r, bconf_vget(bconf_root, "common", "domainlang", NULL)) ;

		if (!lang)
			lang = best_accept_language(r, bconf_vget(bconf_root, "common", "lang", NULL));
	}

	lsd->lang = lang;
	lsd->country = bconf_get_string(bconf_root, "common.site_country");

	return 1;
}

