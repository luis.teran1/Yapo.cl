#include <apr_strings.h>
#include <time.h>

#include "macros.h"
#include "cookies.h"
#include "strl.h"

char *
get_cookie_with_count(request_rec *r, const char *param, int *count) {
	const char *incookie;
	char *cookie, *ptr, *key, *saveptr=NULL, *value = NULL;
	int len;

	if ( (incookie = apr_table_get(r->headers_in, "Cookie")) ) {
		/* Take a copy of the cookie before using strtok_r - should not be necessary! */
		cookie = apr_pstrdup(r->pool, incookie);

		/* Setup the key were looking for */
		key = apr_psprintf(r->pool, "%s=", param);
		len = strlen(key);

		while ( (ptr = strtok_r(cookie, ";", &saveptr)) ) {
			/* Skip starting spaces */
			while (*ptr == ' ')
				ptr++;

			if (memcmp(ptr, key, len) == 0) {
				/* Always use the first value */
				if (value == NULL)
					value = apr_pstrdup(r->pool, ptr + len);

				if (count)
					(*count)++;
				else
					break;
			}

			cookie = NULL;
		}
	}

	/*
	if (count)
		ap_log_error(APLOG_MARK, APLOG_NOTICE, OK, NULL, "get_cookie_with_count(%s) = (%s) # %d", param, value, *count);
	else
		ap_log_error(APLOG_MARK, APLOG_NOTICE, OK, NULL, "get_cookie(%s) = (%s)", param, value);
	*/

	return value;
}

struct get_cookie_set_data
{
	request_rec *r;
	const char *param;
	int plen;
	char *res;
};

static int
get_cookie_set_cb(void *d, const char *key, const char *value)
{
	struct get_cookie_set_data *data = d;

	if (data->res)
		return TRUE;

	if (strncmp(value, data->param, data->plen) == 0 && value[data->plen] == '=') {
		const char *start = value + data->plen + 1;
		const char *semi = strchr(start, ';');

		if (semi)
			data->res = apr_pstrndup(data->r->pool, start, semi - start);
		else
			data->res = apr_pstrdup(data->r->pool, start);
		return FALSE;
	}
	return TRUE;
}

char*
get_cookie(request_rec *r, const char *param) {
	struct get_cookie_set_data data = {r, param, strlen(param), NULL};

	if (!apr_table_do(get_cookie_set_cb, &data, r->err_headers_out, "Set-Cookie", NULL))
		return data.res;

	return get_cookie_with_count(r, param, NULL);
}

static const char *
get_domain(const char *hostname) {
	int i = 0;
	int dot = 0;

	if (!hostname)
		return NULL;

	i = strlen(hostname);
	while (--i >= 0) {
		if (hostname[i] == ':' || hostname[i] == ']') {
			/* IPv6 numeric ip. */
			return hostname;
		}
		if(hostname[i] == '.') {
			char *tolcheck = NULL;

			dot++;
			if (dot > 1)
				return hostname + i;

			UNUSED_RESULT(strtol(hostname + i + 1, &tolcheck, 0));
			if (tolcheck && *tolcheck == '\0') {
				/* Dotted ip, abort. */
				return hostname;
			}
		}
	}
	return hostname;
}

static void
raw_set_cookie(request_rec *r, const char *param, const char *value, const char *domain, int nonsession, int seconds, int secure, int httponly) {
	char *cookie = apr_psprintf(r->pool, "%s=%s;", param, value ? value : "");

	if (!domain && r->hostname) {
		domain = get_domain(r->hostname);
	}
	
	if (domain && domain[0])
		cookie = apr_psprintf(r->pool, "%s domain=%s; path=/;", cookie, domain);

	if (secure)
		cookie = apr_psprintf(r->pool, "%s secure;", cookie);

	if (nonsession) {
		char expires[35];


		if (seconds) {
			struct tm tm;
			time_t expire_at;

			expire_at = time(NULL) + abs(seconds);
			gmtime_r(&expire_at, &tm);
			strftime(expires, sizeof(expires), "%a, %d-%b-%Y %T GMT", &tm);
		} else {
			strlcpy(expires, "Thu, 01-Jan-1970 00:00:01 GMT", sizeof(expires));
		}

		cookie = apr_psprintf(r->pool, "%s expires=%s;", cookie, expires);
	}

	if (secure || httponly)
		cookie = apr_psprintf(r->pool, "%s HttpOnly", cookie);
	
	/* ap_log_error(APLOG_MARK, APLOG_NOTICE, OK, NULL, "set_cookie(%s) = (%s) -> (%s)", param, value, cookie); */
	apr_table_add(r->err_headers_out, "Set-Cookie", cookie);
}

void
set_cookie_with_expiry(request_rec *r, const char *param, const char *value, const char *domain, int days, int httponly) {
	raw_set_cookie(r, param, value, domain, 1, days * 24 * 60 * 60, 0, httponly);
}

void
set_cookie_with_expiry_minutes(request_rec *r, const char *param, const char *value, const char *domain, int minutes, int httponly) {
	raw_set_cookie(r, param, value, domain, 1, minutes * 60, 0, httponly);
}
void

set_cookie_with_expiry_seconds(request_rec *r, const char *param, const char *value, const char *domain, int seconds, int httponly) {
	raw_set_cookie(r, param, value, domain, 1, seconds, 0, httponly);
}

void
set_cookie(request_rec *r, const char *param, const char *value, const char *domain, int httponly) {
	raw_set_cookie(r, param, value, domain, 0, 0, 0, httponly);
}

void
set_secure_cookie_with_expiry(request_rec *r, const char *param, const char *value, const char *domain, int days) {
	raw_set_cookie(r, param, value, domain, 1, days * 24 * 60 * 60, 1, 1);
}

void
set_secure_cookie_with_expiry_minutes(request_rec *r, const char *param, const char *value, const char *domain, int minutes) {
	raw_set_cookie(r, param, value, domain, 1, minutes * 60, 1, 1);
}

void
set_secure_cookie_with_expiry_seconds(request_rec *r, const char *param, const char *value, const char *domain, int seconds) {
	raw_set_cookie(r, param, value, domain, 1, seconds, 1, 1);
}

void
set_secure_cookie(request_rec *r, const char *param, const char *value, const char *domain) {
	raw_set_cookie(r, param, value, domain, 0, 0, 1, 1);
}

void
clear_cookie(request_rec *r, const char *param, const char *domain) {
	char *value = get_cookie(r, param);

	if (value && strlen(value) > 0)
		raw_set_cookie(r, param, "", domain, 1, 0, 0, 0);
}

