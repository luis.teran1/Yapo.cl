#include "python_vtree.h"
#include "memalloc_functions.h"
#include "stringpool.h"
#include "vtree.h"

#include <stdbool.h>
#include <string.h>

struct python_vtree_data
{
	PyObject *node;
	struct stringpool *strpool;
};

#define PVNODE(vchain) ((struct python_vtree_data*)vchain->data)->node

static PyObject *
python_vtree_find(PyObject *node, const char *sentinel, int argc, const char **argv) {
	int i;

	for (i = 0; node && i < argc && argv[i] && argv[i] != sentinel; i++) {
		char *dp;
		const char *key = argv[i];

		/* XXX this is duplicated from php_templates.c */
		if ((dp = strchr(key, '.'))) {
			char *dk = strdupa(key);

			dp = dk + (dp - key);
			key = dk;
		}

		while (1) {
			if (!PyDict_Check(node))
				return NULL;

			if (dp)
				*dp++ = '\0';

			node = PyDict_GetItemString(node, key);

			if (!dp)
				break;

			key = dp;
			dp = strchr(key, '.');
		}
	}

	return node;
}

static int
python_vtree_getlen(struct bpapi_vtree_chain *vchain, enum bpcacheable *cc, int argc, const char **argv) {
	PyObject *node = python_vtree_find(PVNODE(vchain), NULL, argc, argv);

	if (!node)
		return 0;

	if (PyDict_Check(node))
		return PyDict_Size(node);

	return 1;
}

static const char *
python_vtree_asstring(struct python_vtree_data *data, PyObject *node) {
	PyObject *str = NULL, *bytes;
	const char *res = NULL;

	if (PyUnicode_Check(node)) {
		bytes = PyUnicode_AsUTF8String(node);
	} else {
		str = PyObject_Str(node);
		bytes = PyUnicode_AsUTF8String(str);
	}

	if (bytes) {
		const char *bs = PyBytes_AsString(bytes);

		if (bs) {
			res = stringpool_get(data->strpool, bs);
		} else {
			res = NULL;
			PyErr_SetString(PyExc_TypeError, "can't convert to string");
		}
		Py_DECREF(bytes);
	}

	if (str) {
		Py_DECREF(str);
	}
	return res;
}

static const char*
python_vtree_get(struct bpapi_vtree_chain *vchain, enum bpcacheable *cc, int argc, const char **argv) {
	PyObject *node = python_vtree_find(PVNODE(vchain), NULL, argc, argv);

        if (!node)
                return NULL;

	return python_vtree_asstring(vchain->data, node);
}

static int
python_vtree_haskey(struct bpapi_vtree_chain *vchain, enum bpcacheable *cc, int argc, const char **argv) {
	PyObject *node = python_vtree_find(PVNODE(vchain), NULL, argc, argv);

	return node != NULL;
}

static void
python_vtree_fetch_cleanup(struct bpapi_loop_var *loop) {
	free(loop->l.list);
}

static PyObject *
python_vtree_fetch_setup(struct bpapi_vtree_chain *vchain, struct bpapi_loop_var *loop, bool vlist, const char *sentinel, int argc, const char **argv) {
	PyObject *node = python_vtree_find(PVNODE(vchain), sentinel, argc, argv);

	if (!node || !PyDict_Check(node))
	{
		loop->len = 0;
		loop->l.list = NULL;
		loop->cleanup = NULL;
		return NULL;
        }

	loop->len = PyDict_Size(node);
	if (vlist)
		loop->l.vlist = xmalloc(loop->len * sizeof (*loop->l.vlist));
	else
		loop->l.list = xmalloc(loop->len * sizeof (*loop->l.list));
	loop->cleanup = python_vtree_fetch_cleanup;
	return node;
}

static void
python_vtree_fetch_keys(struct bpapi_vtree_chain *vchain, struct bpapi_loop_var *loop, enum bpcacheable *cc, int argc, const char **argv) {
	PyObject *node = python_vtree_fetch_setup(vchain, loop, false, NULL, argc, argv);
	Py_ssize_t pos = 0;
	int i = 0;
	PyObject *key;

	if (!node)
		return;

	for (i = 0 ; PyDict_Next(node, &pos, &key, NULL) ; i++) {
		loop->l.list[i] = python_vtree_asstring(vchain->data, key) ?: "";
	}
}

static void
python_vtree_fetch_values(struct bpapi_vtree_chain *vchain, struct bpapi_loop_var *loop, enum bpcacheable *cc, int argc, const char **argv) {
	PyObject *node = python_vtree_fetch_setup(vchain, loop, false, NULL, argc, argv);
	Py_ssize_t pos = 0;
	int i = 0;
	PyObject *value;

	if (!node)
		return;

	for (i = 0 ; PyDict_Next(node, &pos, NULL, &value) ; i++) {
		loop->l.list[i] = python_vtree_asstring(vchain->data, value) ?: "";
	}
}

static void
python_vtree_fetch_keys_by_value(struct bpapi_vtree_chain *vchain, struct bpapi_loop_var *loop, enum bpcacheable *cc, const char *value, int argc, const char **argv) {
	PyObject *node = python_vtree_fetch_setup(vchain, loop, false, VTREE_LOOP, argc, argv);
	Py_ssize_t pos = 0;
	int i = 0;
	PyObject *key, *vnode;
	int argoff;

	if (!node)
		return;

	for (argoff = 0; argv[argoff] != VTREE_LOOP && argv[argoff] != NULL ; argoff++)
		;
	argoff++;

	for (i = 0 ; PyDict_Next(node, &pos, &key, &vnode) ; ) {
		const char *str;
		vnode = python_vtree_find(vnode, NULL, argc - argoff, argv + argoff);
		if (!vnode)
			continue;
		str = python_vtree_asstring(vchain->data, vnode);
                if (!str || strcmp(str, value) != 0)
			continue;

		loop->l.list[i] = python_vtree_asstring(vchain->data, key) ?: "";
		i++;
	}
	loop->len = i;
}

static struct bpapi_vtree_chain *
python_vtree_getnode(struct bpapi_vtree_chain *vchain, enum bpcacheable *cc, struct bpapi_vtree_chain *dst, int argc, const char **argv) {
	PyObject *node = python_vtree_find(PVNODE(vchain), NULL, argc, argv);

	if (!node || !PyDict_Check(node))
		return NULL;

	python_vtree_init(dst, node);
	dst->next = NULL;
	return dst;
}

static void
python_vtree_fetch_nodes(struct bpapi_vtree_chain *vchain, struct bpapi_loop_var *loop, enum bpcacheable *cc, int argc, const char **argv) {
	PyObject *node = python_vtree_fetch_setup(vchain, loop, true, NULL, argc, argv);
	Py_ssize_t pos = 0;
	int i = 0;
	PyObject *value;

	if (!node)
		return;

	for (i = 0 ; PyDict_Next(node, &pos, NULL, &value) ; i++) {
		python_vtree_init(&loop->l.vlist[i], value);
		loop->l.vlist[i].next = NULL;
	}
}

static void
python_vtree_fetch_keyvals_cleanup(struct vtree_keyvals *loop) {
	free(loop->list);
}

static void
python_vtree_fetch_keys_and_values(struct bpapi_vtree_chain *vchain, struct vtree_keyvals *loop, enum bpcacheable *cc, int argc, const char **argv) {
	PyObject *node = python_vtree_find(PVNODE(vchain), VTREE_LOOP, argc, argv);
	Py_ssize_t pos = 0;
	int i = 0;
	PyObject *key, *vnode;
	int argoff;

	loop->type = vktDict;

	if (!node || !PyDict_Check(node))
	{
		loop->len = 0;
		loop->list = NULL;
		loop->cleanup = NULL;
		return;
	}

	loop->len = PyDict_Size(node);
	loop->list = xmalloc(loop->len * sizeof (*loop->list));
	loop->cleanup = python_vtree_fetch_keyvals_cleanup;

	for (argoff = 0; argv[argoff] != VTREE_LOOP && argv[argoff] != NULL ; argoff++)
		;
	argoff++;

	for (i = 0 ; PyDict_Next(node, &pos, &key, &vnode) ; ) {
		if ((loop->list[i].key = python_vtree_asstring(vchain->data, key)) == NULL)
			loop->list[i].key = "";

		vnode = python_vtree_find(vnode, NULL, argc - argoff, argv + argoff);
		if (!vnode) {
			loop->list[i].type = vkvNone;
			continue;
		}

		if (PyDict_Check(vnode)) {
			loop->list[i].type = vkvNode;
			python_vtree_init(&loop->list[i].v.node, vnode);
			loop->list[i].v.node.next = NULL;
		} else {
			loop->list[i].v.value = python_vtree_asstring(vchain->data, vnode);
			if (loop->list[i].v.value)
				loop->list[i].type = vkvValue;
			else
				loop->list[i].type = vkvNone;
		}
		i++;
	}
	loop->len = i;
}

static void
python_vtree_free(struct bpapi_vtree_chain *vtree) {
	struct python_vtree_data *data = vtree->data;

	stringpool_free(data->strpool);
	free(data);
}


static const struct bpapi_vtree python_vtree_functions = {
	python_vtree_getlen,
	python_vtree_get,
	python_vtree_haskey,
	python_vtree_fetch_keys,
	python_vtree_fetch_values,
	python_vtree_fetch_keys_by_value,
	python_vtree_getnode,
	python_vtree_fetch_nodes,
	python_vtree_fetch_keys_and_values,
	python_vtree_free
};

void
python_vtree_init(struct bpapi_vtree_chain *vtree, PyObject *node) {
	struct python_vtree_data *data = zmalloc(sizeof (*data));

	data->node = node;
	data->strpool = stringpool_new();

	vtree->fun = &python_vtree_functions;
	vtree->data = data;
}
