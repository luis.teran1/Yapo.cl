#include <stdio.h>
#include <pthread.h>
#include "bpapi.h"
#include "vtree.h"
#include "bconf.h"
#include "logging.h"

#define FILTER_STATE_ROOT "_filter_state"

void *
filter_state(struct bpapi *api, const char *key, filter_state_cb_t setup_cb, void *setup_cb_arg) {
	if (!api->filter_state)
		return NULL;
	return api->filter_state(api, key, setup_cb, setup_cb_arg);
}

static const char *
fs_type_key(enum fs_type type) {
	switch (type) {
	case FS_GLOBAL:
		return "filter_state_store_global";
		break;
	case FS_THREAD:
		return "filter_state_store_thread";
		break;
	case FS_SESSION:
		return "filter_state_store_session";
		break;
	default:
		return NULL;
	}
}

/*
 * The layout of the merged filter_state trees is as follows:
 *
 * root = FILTER_STATE_ROOT
 *
 * root.{unlocked,locked}._values - contains all the values. 
 * root.{unlocked,locked}._free - contains all the free functions.
 *   locked/unlocked specifies if we need to hold the mutex to touch those values, this includes reading.
 *
 * root.mutex - the mutex to lock the global state. needs to be held when doing anything with "root.locked".
 * root.filter_state_store_{global,thread,session} - storage for the various types of filter state.
 * Notice that no keys will be added under the root for any types, especially GLOBAL after initialization.
 */

void
filter_state_bconf_set(struct bconf_node **bn, const char *key, void *value, void (*freefn)(void *), enum fs_type type) {
	const char *ktype = type == FS_GLOBAL ? "locked" : "unlocked";
	if (freefn) {
		bconf_add_bindatav(bn, 4, (const char *[]){ FILTER_STATE_ROOT, ktype, "free", key }, freefn);
	}
	bconf_add_bindatav(bn, 4, (const char *[]){ FILTER_STATE_ROOT, ktype, "values", key }, value);
}
/*
 * This is the default filter state function used when we have a bconf
 * node backing the filter state.
 *
 * The caller needs to have allocated a bconf_node * and initialized it
 * with filter_state_bconf_init.
 */
void *
filter_state_bconf(struct bpapi *api, const char *key, filter_state_cb_t setup_cb, void *setup_cb_arg) {
	pthread_mutex_t *mtx = NULL;
	struct bconf_node **bn;
	struct filter_state_data res = { 0 };

	/*
	 * First look inside the unlocked values.
	 * We check with haskey first because NULL is a valid value.
	 */
	if (vtree_haskey(&api->vchain, FILTER_STATE_ROOT, "unlocked", "values", key, NULL))
		return (void *)vtree_get(&api->vchain, FILTER_STATE_ROOT, "unlocked", "values", key, NULL);

	mtx = (pthread_mutex_t *)(void *)vtree_get(&api->vchain, FILTER_STATE_ROOT, "mutex", NULL);
	if (!mtx) {
		log_printf(LOG_CRIT, "filter_state: no mutex, possibly uninitilized filter_state");
		return NULL;
	}
	/* lock mutex for checking the locked values. */
	if (pthread_mutex_lock(mtx)) {
		log_printf(LOG_CRIT, "filter_state: pthread_mutex_lock failed");
		return NULL;
	}
	if (vtree_haskey(&api->vchain, FILTER_STATE_ROOT, "locked", "values", key, NULL)) {
		void *ret = (void *)vtree_get(&api->vchain, FILTER_STATE_ROOT, "locked", "values", key, NULL);
		pthread_mutex_unlock(mtx);
		return ret;
	}
	/* Don't hold the mutex while calling the setup callback. */
	pthread_mutex_unlock(mtx);

	if (!setup_cb)
		return NULL;

	setup_cb(api, key, &res, setup_cb_arg);
	if (!res.value)
		return NULL;

	bn = (struct bconf_node **)(void *)vtree_get(&api->vchain, FILTER_STATE_ROOT, fs_type_key(res.type), NULL);
	if (!bn) {
		log_printf(LOG_CRIT, "filter_state: no store for %d, possibly uninitialized filter_state", res.type);
		return NULL;
	}
	if (res.type == FS_GLOBAL) {
		if (pthread_mutex_lock(mtx)) {
			log_printf(LOG_CRIT, "filter_state mutex failed %m");
			return NULL;
		}
		/*
		 * Now that we have the mutex, double check if we lost the race to initialize.
		 *
		 * We can only lose the race for locked values. There is no
		 * concurrent access to the unlocked values (except if the
		 * callback function calls filter_state recursively, let's
		 * ignore that case, leaking is better than the added
		 * complexity of checking for that).
		 */
		if (vtree_haskey(&api->vchain, FILTER_STATE_ROOT, "locked", "values", key, NULL)) {
			void *ret = (void *)vtree_get(&api->vchain, FILTER_STATE_ROOT, "locked", "values", key, NULL);

			/* Don't hold the mutex while freeing. */
			pthread_mutex_unlock(mtx);
			if (res.freefn)
				res.freefn(res.value);

			return ret;
		}
	}
	filter_state_bconf_set(bn, key, res.value, res.freefn, res.type);
	if (res.type == FS_GLOBAL)
		pthread_mutex_unlock(mtx);
	return res.value;
}

void
filter_state_bconf_init(struct bconf_node **filter_tree, enum fs_type type) {
	pthread_mutex_t *mtx;

	bconf_add_bindatav(filter_tree, 2, (const char *[]){ FILTER_STATE_ROOT, fs_type_key(type) }, filter_tree);

	if (type == FS_GLOBAL) {
		mtx = xmalloc(sizeof(*mtx));
		pthread_mutex_init(mtx, NULL);
		bconf_add_bindatav(filter_tree, 2, (const char *[]){ FILTER_STATE_ROOT, "mutex" }, mtx);
	}
}

void
filter_state_bconf_init_all(struct bconf_node **filter_tree) {
	filter_state_bconf_init(filter_tree, FS_GLOBAL);
	filter_state_bconf_init(filter_tree, FS_THREAD);
	filter_state_bconf_init(filter_tree, FS_SESSION);
}

void
filter_state_bconf_free(struct bconf_node **filter_tree) {
	pthread_mutex_t *mtx = (pthread_mutex_t *)bconf_binvalue(bconf_vget(*filter_tree, FILTER_STATE_ROOT, "mutex", NULL));
	struct bconf_node *fn, *n;
	int i;

	if (mtx) {
		pthread_mutex_destroy(mtx);
		free(mtx);
	}
	for (const char **lk = (const char*[]){ "locked", "unlocked", NULL } ; *lk ; lk++) {
		fn = bconf_vget(*filter_tree, FILTER_STATE_ROOT, *lk, "free", NULL);
		for (i = 0; (n = bconf_byindex(fn, i)); i++) {
			void (*ff)(void *) = (void *)bconf_value(n);
			void *v = (void *)bconf_value(bconf_vget(*filter_tree, FILTER_STATE_ROOT, *lk, "values", bconf_key(n), NULL));
			(*ff)(v);
		}
	}
	bconf_free(filter_tree);
}

void
filter_state_bconf_glue_session_start(struct bpapi_vtree_chain *out, struct filter_state_bconf_glue *fsg, struct bconf_node *global_fs, struct bconf_node *thread_fs, struct bpapi_vtree_chain *vtree) {
	memset(fsg, 0, sizeof(*fsg));

	filter_state_bconf_init(&fsg->session_fs, FS_SESSION);

	bconf_vtree(&fsg->global_shadow.vtree, global_fs);
	shadow_vtree_init(&fsg->global_vt, &fsg->global_shadow, vtree);

	if (thread_fs) {
		bconf_vtree(&fsg->thread_shadow.vtree, thread_fs);
		shadow_vtree_init(&fsg->thread_vt, &fsg->thread_shadow, &fsg->global_vt);
	} else {
		filter_state_bconf_init(&fsg->session_fs, FS_THREAD);
	}

	bconf_vtree(&fsg->session_shadow.vtree, fsg->session_fs);
	shadow_vtree_init(out, &fsg->session_shadow, thread_fs ? &fsg->thread_vt : &fsg->global_vt);
}

void
filter_state_bconf_glue_session_end(struct filter_state_bconf_glue *fsg) {
	filter_state_bconf_free(&fsg->session_fs);
}
