struct hash_entry *hash_lookup(struct bpapi_simplevars_chain *, const char *) VISIBILITY_HIDDEN;
void hash_fetch_loop(struct bpapi_simplevars_chain *, const char *, struct bpapi_loop_var *) VISIBILITY_HIDDEN;
void hash_init_var(struct bpapi_simplevars_chain *, const char *, struct template_var *) VISIBILITY_HIDDEN;
int hash_has_key(struct bpapi_simplevars_chain *schain, const char *key) VISIBILITY_HIDDEN;
int hash_length(struct bpapi_simplevars_chain *schain, const char *key) VISIBILITY_HIDDEN;
int keyglob_len(struct bpapi_simplevars_chain *schain, const char *key) VISIBILITY_HIDDEN;
void hash_fetch_glob_loop(struct bpapi_simplevars_chain *schain, const char *key, struct bpapi_loop_var *loop) VISIBILITY_HIDDEN;
void hash_insert(struct bpapi_simplevars_chain *schain, const char *key, int klen, const char *value, int vlen) VISIBILITY_HIDDEN;
void insert_maxsize(struct bpapi_simplevars_chain *schain, const char *key, const char *value, int maxsize) VISIBILITY_HIDDEN;
void hash_delete(struct bpapi_simplevars_chain *schain, const char *key) VISIBILITY_HIDDEN;
void hash_simplevars_free(struct bpapi_simplevars_chain *schain) VISIBILITY_HIDDEN;

struct ctemplates_simplevars_data {
	int nkeys;
	struct avl_node *keys;
	void *conn; /* Actually PGconn */
};
