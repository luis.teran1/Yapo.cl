
#include <ctemplates.h>
#include <bpapi.h>
#include <patchvars.h>
#include <unicode.h>
#include <query_string.h>

#include "filter_state_ucnv.h"

struct qs_sq_var {
	const char *key;
	struct qs_sq_var *next;
};

struct qs_sq_var_data {
	struct qs_sq_var *vars;
	struct bpapi *api;
};

static void
qs_sq_var_cb(void *cb_data, char *key, int klen, int kbufsz, char *value, int vlen, int vbufsz)
{
	struct qs_sq_var_data *data = cb_data;
	struct qs_sq_var *var = data->vars;
	UConverter *utf8_cnv = filter_state(data->api, "ucnv_utf8", fs_ucnv_utf8, NULL);
	UConverter *dst_cnv = filter_state(data->api, "ucnv_dst", fs_ucnv_dst, NULL);

	if (!utf8_cnv || !dst_cnv)
		xerrx(1, "Missing converter for qs_sq");

	utf8_decode(key, &kbufsz, key, klen, utf8_cnv, dst_cnv);
	utf8_decode(value, &vbufsz, value, vlen, utf8_cnv, dst_cnv);

	for (; var ; var = var->next) {
		if (strcmp(key, var->key) == 0 || strcmp(var->key, "*") == 0) {
			bpapi_insert(data->api, key, value);
			return;
		}
	}
}

static int
init_qs_sq(const struct bpfilter *filter, struct bpapi *api, int argc, const struct tpvar **argv) {
	const char *qs;
	const char *appl;
	const char *rootpath;
	struct qs_sq_data data = {};
	struct patch_data *pd;
	struct qs_sq_var_data var_data = {NULL, api};
	int i;

	TPVAR_STRTMP(rootpath, argv[0]);
	TPVAR_STRTMP(appl, argv[1]);
	TPVAR_STRTMP(qs, argv[2]);

	for (i = 3; i < argc; i++) {
		struct qs_sq_var *v = xmalloc(sizeof (*v));
		TPVAR_STRTMP(v->key, argv[i]);
		v->next = var_data.vars;
		var_data.vars = v;
	}

	data.vtree = &api->vchain;
	data.rootpath = rootpath;
	data.lang = vtree_get(&api->vchain, "tmpl", "lang", NULL);
	if (!data.lang && bps_has_key(api->schain.next, "b__lang"))
		data.lang = bps_get_element(api->schain.next, "b__lang", 0);
	data.country = vtree_get(&api->vchain, "tmpl", "country", NULL);
	if (var_data.vars) {
		data.parse_qs_cb = qs_sq_var_cb;
		data.cb_data = &var_data;
	}
	data.req_utf8 = get_req_utf8_vtree(&api->vchain);

	query_string_search_query(appl, qs, &data);

	while (var_data.vars) {
		struct qs_sq_var *v = var_data.vars;
		var_data.vars = v->next;
		free(v);
	}

	pd = BPAPI_SIMPLEVARS_DATA(api);
	patchvars_init(pd, "qs_sq", "sq_", 3, NULL, NULL);
	bps_set_int(&pd->vars, "sq_offset", data.offset);
	bps_set_int(&pd->vars, "sq_limit", data.limit);
	if (data.count_level.buf)
		bps_insert(&pd->vars, "sq_count_level", data.count_level.buf);
	if (data.filter.buf)
		bps_insert(&pd->vars, "sq_filter", data.filter.buf);
	if (data.unfiltered.buf)
		bps_insert(&pd->vars, "sq_unfiltered", data.unfiltered.buf);
	if (data.params.buf)
		bps_insert(&pd->vars, "sq_params", data.params.buf);
	if (data.query.buf)
		bps_insert(&pd->vars, "sq_query", data.query.buf);
	if (data.desc.buf)
		bps_insert(&pd->vars, "sq_desc", data.desc.buf);
	free_qs_sq(&data);

	return 0;
}

ADD_FILTER(qs_sq, NULL, 0, &patch_simplevars, sizeof(struct patch_data), NULL, 0);
