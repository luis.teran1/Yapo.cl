
#include <stdio.h>

#include <ctemplates.h>
#include <bpapi.h>
#include <bsapi.h>
#include <query_string.h>
#include <logging.h>

#include "search/filter_state_bsconf.h"

struct vsearch_data {
	struct shadow_vtree svt;
	struct shadow_vtree svt2;

	struct bpapi_vtree_chain res_prefix;
	struct bpapi_vtree_chain conf_prefix;
	struct bpapi_vtree_chain res_vtree;

	struct bsearch_api bs;
	struct bsconf *bsc;

	struct bconf_node *res_root;
	struct bconf_node *info_root;

	char *fe;
	char *pe;
	int col;
	int row;
	struct bconf_node *row_node;
};

static void
vsearch_add_data(int state, int info, const char *key, const char **value, void *data) {
	struct vsearch_data *sd = data;
	const char *keyv[20];
	int keyc;

	switch (state) {
	case BS_INFO:
		if (value[0] == NULL)
			break;
		keyv[0] = key;
		for (keyc = 1; value[keyc] != NULL; keyc++)
			keyv[keyc] = value[keyc - 1];
		bconf_add_datav(&sd->info_root, keyc, keyv, value[keyc - 1], BCONF_DUP);
		break;
	case BS_VALUE:
		if (*value[0]) {
			if (sd->row != info) {
				char keybuf[24];

				snprintf(keybuf, sizeof(keybuf), "%d", info);
				const char *argv[3] = {"row", keybuf, key };

				bconf_add_datav(&sd->res_root, 3, argv, value[0], BCONF_REF);

				/* Saving row_node must be done after adding any data on that path */
				sd->row_node = bconf_vasget(sd->res_root, NULL, 2, argv);
				sd->row = info;
			} else {
				bconf_add_datav(&sd->row_node, 1, (const char *[]){key}, value[0], BCONF_REF);
			}
		}
		break;
	case BS_COLS:
	case BS_DONE:
	default:
		break;
	}
}

static void
vsearch_vtree_free(struct shadow_vtree *svt) {
	struct vsearch_data *sd = (struct vsearch_data *)svt;

	bsearch_put_conf(&sd->bs);
	bsearch_cleanup(&sd->bs);
	free(sd->fe);
	free(sd->pe);
	vtree_free(&sd->res_vtree);
	bconf_free(&sd->res_root);
}

static int
init_vsearch(const struct bpfilter *filter, struct bpapi *api, int argc, const struct tpvar **argv) { 
	struct vsearch_data *sd = BPAPI_VTREE_DATA(api); 
	struct bpapi_vtree_chain *conf = NULL;
	const char *remote_ip = NULL;
	const char *error = NULL;
	const char *engine;
	const char *prefix;
	const char *query;
	char *buf;
	int res;

	engine = tpvar_str(argv[0], &sd->fe);

	remote_ip = filter_state(api, "remote_addr", NULL, NULL);
	ALLOCA_PRINTF(res, buf, "bsconf_%s", engine);
	sd->bsc = filter_state(api, buf, filter_state_bsconf, (void *)engine);

	prefix = tpvar_str(argv[1], &sd->pe);

	if (sd->bsc) {
		bsearch_init_bsconf(&sd->bs, sd->bsc, remote_ip);
	} else {
		error = "no search config";
		goto out;
	}

	TPVAR_STRTMP(query, argv[2]);
	bconf_add_data(&sd->res_root, "info.query", query);
	sd->info_root = bconf_get(sd->res_root, "info");

	sd->row = -1;

	res = bsearch_do_search(&sd->bs, query, BS_FLAG_CONF|BS_FLAG_CONF_INFO, vsearch_add_data, sd); 
	if (res) {
		error = bsearch_errstr(res);
	}

	conf = bsearch_get_conf(&sd->bs);

out:
	if (error) {
		bconf_add_data(&sd->res_root, "error", error);
	}

	prefix_vtree_init(&sd->svt.vtree, prefix, &sd->res_prefix);
	shadow_vtree_init(&sd->res_prefix, &sd->svt2, &sd->res_vtree);
	if (conf)
		prefix_vtree_init(&sd->svt2.vtree, "conf", conf);
	bconf_vtree(&sd->res_vtree, sd->res_root);
	sd->svt.free_cb = vsearch_vtree_free;

	return 0;
}

#define vsearch_vtree shadow_vtree

ADD_FILTER(vsearch, NULL, 0, NULL, 0, &shadow_vtree, sizeof(struct vsearch_data));
