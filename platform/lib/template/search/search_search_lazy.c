#include <stdio.h>

#include <bpapi.h>
#include <ctemplates.h>
#include <bsapi_object.h>
#include <patchvars.h>

#include "search/filter_state_bsconf.h"

struct search_data {
	struct patch_data pd;
	struct bsapi_object *bo;
	char *fp;
	char *fe;
	char *ft;
};

static void
fini_search(struct patch_data *data) {
	struct search_data *sd = data->user_data;

	bsapi_object_free(sd->bo);
	free(sd->fp);
	free(sd->fe);
	free(sd->ft);
}

static int
_init_search(const char *tag, const struct bpfilter *filter, struct bpapi *api, int argc, const struct tpvar **argv) {
	struct search_data *sd = BPAPI_SIMPLEVARS_DATA(api);
	const char *remote_ip = NULL;
	int res;
	struct bsconf *bsc = NULL;
	const char *prefix;
	const char *engine;
	int prefix_len;
	const char *query;
	char buf[1024];

	engine = tpvar_str(argv[0], &sd->fe);

	remote_ip = filter_state(api, "remote_addr", NULL, NULL);
	snprintf(buf, sizeof(buf), "bsconf_%s", engine);
	bsc = filter_state(api, buf, filter_state_bsconf, (void *)engine);

	if (bsc) {
		sd->bo = bsapi_object_new(bsc, remote_ip);
	} else {
 		struct bpapi_vtree_chain vtree = {0};
		sd->bo = bsapi_object_new_vtree(vtree_getnode(&api->vchain, &vtree, "common", engine, NULL), remote_ip);
		vtree_free(&vtree);
		if (sd->bo == NULL) {
 			return 1;
 		}
	}

	prefix = tpvar_str(argv[1], &sd->fp);
	prefix_len = strlen(prefix);
	TPVAR_STRTMP(query, argv[2]);

	patchvars_init(&sd->pd, tag ? tag : engine, prefix, prefix_len, fini_search, sd);

	res = bsapi_object_search(sd->bo, query);
	if (res) {
		snprintf(buf, sizeof(buf), "%s%serror", prefix, prefix_len > 0 ? "_" : "");
		bps_set_int(&sd->pd.vars, buf, res);
	} else {
		const struct bsapi_object_info *info;
		int i;

		while ((info = bsapi_object_next_info(sd->bo))) {
			if (info->nvals == 1) {
				snprintf(buf, sizeof(buf), "%s%sinfo_%s", sd->pd.prefix, sd->pd.prefix_len > 0 ? "_" : "", info->key);
				bps_insert(&sd->pd.vars, buf, info->value[0]);
			} else {
				for (i = 0; i < info->nvals;  i++) {
					snprintf(buf, sizeof(buf), "%s%sinfo_%s_%d", sd->pd.prefix, sd->pd.prefix_len > 0 ? "_" : "", info->key, i);
					bps_insert(&sd->pd.vars, buf, info->value[i]);
				}
			}
		}

		sd->pd.rows = bsapi_object_num_rows(sd->bo);

		if (sd->pd.rows > 0) {
			int cols = bsapi_object_num_cols(sd->bo);
			for (i = 0 ; i < cols ; i++) {
				const char *key = bsapi_object_column_name(sd->bo, i);
				const char **values = bsapi_object_all_values(sd->bo, i);

				snprintf(buf, sizeof(buf), "%s%s", sd->pd.prefix, key);
				bps_insert_list(&sd->pd.vars, buf, values, sd->pd.rows);
			}
		}
	}
	return 0;
}

static int
init_search(const struct bpfilter *filter, struct bpapi *api, int argc, const struct tpvar **argv) {
	return _init_search(NULL, filter, api, argc, argv);
}

ADD_FILTER(search, NULL, 0, &patch_simplevars, sizeof(struct search_data), NULL, 0);

static int
init_search_lazy(const struct bpfilter *filter, struct bpapi *api, int argc, const struct tpvar **argv) {
	struct search_data *sd = BPAPI_SIMPLEVARS_DATA(api);
	return _init_search(tpvar_str(argv[0], &sd->ft), filter, api, argc - 1, argv + 1);
}

ADD_FILTER(search_lazy, NULL, 0, &patch_simplevars, sizeof(struct search_data), NULL, 0);

#define loop_search loop_patch
#define loop_length_search loop_length_patch

ADD_LOOP_FILTER(search, NULL, 0, &patch_loop_simplevars, sizeof(struct search_data), NULL, 0);
