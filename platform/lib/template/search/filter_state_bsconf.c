#include <stdio.h>
#include <ctemplates.h>
#include <bpapi.h>
#include <bsapi.h>
#include <logging.h>

#include "search/filter_state_bsconf.h"

static void
fs_bsconf_free(void *v) {
	bsconf_del(v);
	free(v);
}

void
filter_state_bsconf(struct bpapi *api, const char *key, struct filter_state_data *fsd, void *v) {
	struct bpapi_vtree_chain engine_node = { 0 };
	const char *engine = v;
	struct bsconf *bsc;
	int ret;

	const char *engine_path = vtree_get(&api->vchain, "search", "engine_path", NULL);
	if (!engine_path)
		engine_path = "common";

	if (!vtree_getnode(&api->vchain, &engine_node, engine_path, engine, NULL)) {
		log_printf(LOG_WARNING, "fs_bsconf: no search engine node (%s.%s)", engine_path, engine);
		return;
	}
	bsc = zmalloc(sizeof(*bsc));
	if ((ret = bsconf_init_vtree(bsc, &engine_node, 1))) {
		free(bsc);
		log_printf(LOG_WARNING, "fs_bsconf: bsconf_init_vtree(%s) = %d", engine, ret);
		return;
	}
	fd_pool_clear_node(bsc->pool);
	vtree_free(&engine_node);
	fsd->value = bsc;
	fsd->freefn = fs_bsconf_free;
	fsd->type = FS_GLOBAL;
}
