#include <libpq-fe.h>

#include <bpapi.h>
#include <ctemplates.h>
#include <patchvars.h>
#include <bconf.h>

static void
pgsql_session_free(struct shadow_vtree *sv) {
	PGconn *conn = sv->cbdata;
	struct bconf_node *node = sv->vtree.data;

	if (conn)
		PQfinish(conn);
	bconf_free(&node);
}

static void
fs_pgsql_session(struct bpapi *ba, const char *key, struct filter_state_data *fsd, void *v) {
	const char *db_node_name = NULL;
	struct bpapi_vtree_chain db_node = {0};

	if (!strcmp(key, "_pgsql")) {
		db_node_name = vtree_get(&ba->vchain, "pgsql_session", "db_name", NULL);
	} else {
		const char *node;
		node = strchr(key, '_');
		if (node) {
			node++;
			db_node_name = vtree_get(&ba->vchain, "pgsql_session", node, "db_name", NULL);
		}
	}
	if (!db_node_name || !vtree_getnode(&ba->vchain, &db_node, db_node_name, NULL))
		return;

	const char *host = vtree_get(&db_node, "host", NULL);
	const char *port = vtree_get(&db_node, "port", NULL);
	const char *password = vtree_get(&db_node, "password", NULL);
	const char *user = vtree_get(&db_node, "user", NULL);
	const char *dbname = vtree_get(&db_node, "dbname", NULL);
	const char *options = vtree_get(&db_node, "options", NULL);

	vtree_free(&db_node);
	char *value = NULL;
	if (asprintf(&value, "host = '%s' %s%s%s user = '%s' %s%s%s dbname = '%s' options = '%s'",
		     host ?: "",
		     port ? "port = '" : "",
		     port ?: "",
		     port ? "'" : "",
		     user ?: getenv("USER"),
		     password ? "password = '" : "",
		     password ?: "",
		     password ? "'" : "",
		     dbname ?: "blocketdb",
		     options ?: "") == -1) {
		return;
	}
	fsd->value = value;
	fsd->freefn = free;
	fsd->type = FS_GLOBAL;	
}

static int
init_pgsql_session(const struct bpfilter *filter, struct bpapi *api, int argc, const struct tpvar **argv) {
	struct shadow_vtree *subtree = BPAPI_VTREE_DATA(api);
	struct bconf_node *node = NULL;
	PGconn *conn = NULL;
	const char *conninfo = NULL;

	if (argc == 0) {
		conninfo = filter_state(api, "_pgsql", fs_pgsql_session, NULL);
	} else {
		const char *node_name;
		char *state_name;
		TPVAR_STRTMP(node_name, argv[0]);
		xasprintf(&state_name, "pgsql_%s", node_name);
		conninfo = filter_state(api, state_name, fs_pgsql_session, NULL);
		free(state_name);
	}

	if (conninfo) {
		conn = PQconnectdb(conninfo);
		if (!conn) {
			bconf_add_data(&node, "_pgsql.error", "PQconnectdb: malloc failed");
		} else if (PQstatus(conn) != CONNECTION_OK) {
			bconf_add_data(&node, "_pgsql.error", PQerrorMessage(conn));
			PQfinish(conn);
			conn = NULL;
		} else {
			bconf_add_bindata(&node, "_pgsql.conn", conn);
		}
	} else {
		bconf_add_data(&node, "_pgsql.error", "No conninfo");
	}

	bconf_vtree(&subtree->vtree, node);
	subtree->free_cb = pgsql_session_free;
	subtree->cbdata = conn;

	return 0;
}

#define pgsql_session_vtree shadow_vtree

ADD_VTREE_FILTER(pgsql_session, sizeof(struct shadow_vtree));

