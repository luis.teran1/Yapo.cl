#include <bpapi.h>
#include <string.h>
#include <ctype.h>
#include <queue.h>
#include <stdio.h>
#include <libpq-fe.h>
#include <time.h>
#include <stdlib.h>

#include "strl.h"
#include "patchvars.h"
#include "bconf.h"
#include "logging.h"

struct pgsql_data {
	struct patch_data pd;
	char *fp;
	int error;
	int rows;
	int row;
	char msg[256];
	const char *query;
	char *fq;
	PGconn *conn;
	PGresult *res;
	const char **value_lists;
};

static void
pgsql_insert_data(struct pgsql_data *pd) {
	char buf[256];

	if (pd->error) {
		snprintf(buf, sizeof(buf), "%s%serror", pd->pd.prefix, pd->pd.prefix_len ? "_" : "");
		bps_insert(&pd->pd.vars, buf, pd->msg);
	}
	if (pd->query) {
		snprintf(buf, sizeof(buf), "%s%squery", pd->pd.prefix, pd->pd.prefix_len ? "_" : "");
		bps_insert(&pd->pd.vars, buf, pd->query);
	}

	snprintf(buf, sizeof(buf), "%s%srows", pd->pd.prefix, pd->pd.prefix_len ? "_" : "");
	bps_set_int(&pd->pd.vars, buf, pd->rows);

	if (pd->res && pd->rows) {
		int x, y;
		int cols = PQnfields(pd->res);
		int rows = PQntuples(pd->res);
		const char **value_lists = xmalloc(2 * cols * rows * sizeof(*value_lists));
		const char **isnull_lists = value_lists + cols * rows;

		for (x = 0 ; x < cols ; x++) {
			const char *col = PQfname(pd->res, x);
			int i = 0;

			for (y = 0 ; y < rows ; y++) {
				value_lists[x * rows + y] = PQgetvalue(pd->res, y, x);
				isnull_lists[x * rows + y] = PQgetisnull(pd->res, y, x) ? "1" : "0";
			}

			snprintf(buf, sizeof(buf), "%s%s", pd->pd.prefix, col);
			while (bps_has_key(&pd->pd.vars, buf))
				snprintf(buf, sizeof(buf), "%s%s%d", pd->pd.prefix, col, ++i);
			bps_insert_list(&pd->pd.vars, buf, value_lists + x * rows, rows);
			if (i)
				snprintf(buf, sizeof(buf), "%sisnull_%s%d", pd->pd.prefix, col, i);
			else
				snprintf(buf, sizeof(buf), "%sisnull_%s", pd->pd.prefix, col);
			bps_insert_list(&pd->pd.vars, buf, isnull_lists + x * rows, rows);
		}
		pd->value_lists = value_lists;
	}
}

static void
fini_pgsql(struct patch_data *data) {
	struct pgsql_data *pd = data->user_data;

	free(pd->fq);
	free(pd->fp);
	if (pd->res)
		PQclear(pd->res);
	free(pd->value_lists);
}

static int
init_pgsql(const struct bpfilter *filter, struct bpapi *api, int argc, const struct tpvar **argv) {
	struct pgsql_data *pd = BPAPI_SIMPLEVARS_DATA(api);
	PGconn *conn;

	pd->pd.prefix = tpvar_str(argv[0], &pd->fp);
	pd->query = tpvar_str(argv[1], &pd->fq);

	patchvars_init(&pd->pd, filter->name, pd->pd.prefix, strlen(pd->pd.prefix), fini_pgsql, pd);

	conn = (PGconn*)vtree_get(&api->vchain, "_pgsql", "conn", NULL);
	if (!conn) {
		const char *error = vtree_get(&api->vchain, "_pgsql", "error", NULL);
		if (error) {
			strlcpy(pd->msg, error, sizeof(pd->msg));
		} else {
			strlcpy(pd->msg, "No connection available", sizeof(pd->msg));
		}
		pd->error = 1;
		pgsql_insert_data(pd);
		return 0;
	}
	pd->conn = conn;

	if (filter->loop) {
		pd->row = -1;
		pd->error = !PQsendQuery(conn, pd->query);
		return 0;
	}

	/* Send the query */
	pd->res = PQexec(conn, pd->query);

	if (!pd->res) {
		/* Fatal error */
		strlcpy(pd->msg, PQerrorMessage(conn), sizeof(pd->msg));
		pd->error = 1;
		pgsql_insert_data(pd);
		return 0;
	} 

	if (PQresultStatus(pd->res) == PGRES_COMMAND_OK) {
		pd->rows = 0;
	} else if (PQresultStatus(pd->res) == PGRES_TUPLES_OK) {
		pd->rows = PQntuples(pd->res);
	} else {
		strlcpy(pd->msg, PQresultErrorMessage(pd->res), sizeof(pd->msg));
		pd->error = 1;
	}
	pd->pd.rows = pd->rows;

	pgsql_insert_data(pd);

	return 0;
}

#define pgsql_simplevars patch_simplevars

ADD_SIMPLEVARS_FILTER(pgsql, sizeof(struct pgsql_data));

static int
loop_pgsql_fetch(struct bpapi *api) {
	struct pgsql_data *pd = BPAPI_SIMPLEVARS_DATA(api);
	PGresult *res = NULL;
	PGconn *conn = pd->conn;

	if (!conn)
		conn = (PGconn*)vtree_get(&api->vchain, "_pgsql", "conn", NULL);

	if (pd->error == 1 && !pd->res) {
		/* Loop one turn if we got an error on PQsendQuery. */
		strlcpy(pd->msg, PQerrorMessage(conn), sizeof(pd->msg));
		free(pd->value_lists);
		pd->value_lists = NULL;
		pgsql_insert_data(pd);
		pd->error = 2; /* Prevent endless loop. */
		return 1;
	}

	/* Use the last result, similar to PQexec. */
	do {
		if (pd->res)
			PQclear(pd->res);
		pd->res = res;
	} while (conn && (res = PQgetResult(conn)));

	if (!pd->res)
		return 0;

	if (PQresultStatus(pd->res) == PGRES_COMMAND_OK) {
		pd->rows = 0; /* PQaffectedRows */
	} else if (PQresultStatus(pd->res) == PGRES_TUPLES_OK) {
		pd->rows = PQntuples(pd->res);
	} else {
		strlcpy(pd->msg, PQresultErrorMessage(pd->res), sizeof(pd->msg));
		pd->error = 1;
	}
	pd->pd.rows = pd->rows;

	if (!pd->rows && !pd->error)
		return 0;

	if (!pd->error) {
		pd->error = !PQsendQuery(conn, pd->query);
		/* XXX set msg here if error. */
	}

	patchvars_clear(&pd->pd);
	free(pd->value_lists);
	pd->value_lists = NULL;
	pgsql_insert_data(pd);

	return 1;
}

static int
loop_length_pgsql_fetch(struct bpapi *api) {
	return -1;
}

#define init_pgsql_fetch init_pgsql

ADD_LOOP_FILTER(pgsql_fetch, NULL, 0, &pgsql_simplevars, sizeof(struct pgsql_data), NULL, 0);

static int
loop_pgsql_fetch_row(struct bpapi *api) {
	struct pgsql_data *pd = BPAPI_SIMPLEVARS_DATA(api);

	if (++pd->row >= pd->rows) {
		pd->row = 0;
		return loop_pgsql_fetch(api);
	}
	return 1;
}

static int
loop_length_pgsql_fetch_row(struct bpapi *api) {
	return -1;
}

static const char *
pgsql_fetch_row_get_element(struct bpapi_simplevars_chain *schain, const char *key, int element) {
	struct pgsql_data *pd = schain->data;
	const char *v;

	v = bps_get_element(&pd->pd.vars, key, pd->row + element);
	if (v)
		return v;

	return bps_get_element(schain->next, key, element);
}

const struct bpapi_simplevars pgsql_fetch_row_simplevars = PATCH_WITH_LOOP(pgsql_fetch_row_get_element);

#define init_pgsql_fetch_row init_pgsql

ADD_LOOP_FILTER(pgsql_fetch_row, NULL, 0, &pgsql_fetch_row_simplevars, sizeof(struct pgsql_data), NULL, 0);

