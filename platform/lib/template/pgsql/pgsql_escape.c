#include <libpq-fe.h>

#include <bpapi.h>
#include <ctemplates.h>
#include <patchvars.h>
#include <queue.h>

struct _esc_filter_data {
	struct buf_string buf;
	PGconn	*conn;
};

static void
fini_pgsql_escape(struct bpapi_output_chain *ochain) {
	struct _esc_filter_data *efd = ochain->data;

	if (!efd->conn || efd->buf.pos < 1) {
		free(efd->buf.buf);
		return;
	}

	char* buffer = xmalloc(efd->buf.pos*2 + 1);

	PQescapeStringConn(efd->conn, buffer, efd->buf.buf, efd->buf.pos, NULL);
	bpo_outstring_raw(ochain->next, buffer, strlen(buffer));

	free(buffer);
	free(efd->buf.buf);
}

static int
init_pgsql_escape(const struct bpfilter *filter, struct bpapi *api, int argc, const struct tpvar **argv) {
	struct _esc_filter_data *efd = BPAPI_OUTPUT_DATA(api);

	efd->conn = (PGconn*)vtree_get(&api->vchain, "_pgsql", "conn", NULL);

	return 0;
}

struct bpapi_output pgsql_escape_output = {
	bufstring_outstring_fmt,
	bufstring_outstring_raw,
	fini_pgsql_escape
};

ADD_OUTPUT_FILTER(pgsql_escape, sizeof(struct _esc_filter_data));

