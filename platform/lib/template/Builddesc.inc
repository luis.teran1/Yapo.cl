	srcs[query_string_setting.gperf.enum query_string_value_key.gperf.enum
		query_string_lookup_key.gperf.enum bpapi.c
		blocket/*.c
		ctemplates.c
		crypto/*.c
		patchvars.c
		datetime/*.c
		encoding/*.c
		html/*.c
		js/*.c
		json/*.c
		math/*.c
		memcache/*.c
		loop/*.c
		redis/*.c
		search/*.c
		string/*.c
		tpcapi.c 
		tpfilters/*.c 
		tpfunctions/*.c
		trans/*.c
		vtree/*.c
		xml/*.c
		query_string.c
		filter_state_ucnv.c
		vtree_literal.c
		vtree_value.c
	]
	includes[bpapi.h ctemplates.h patchvars.h templateparse_internal.h
		tmpl_lru.h query_string.h filter_state_ucnv.h search/filter_state_bsconf.h tpfunctions/filter_state_random.h
		vtree_literal.h
		vtree_value.h
		]
	libs[platform_core pcre z crypto expat platform_filter_state m]
	deps[blocket/build_version.c:$incdir/build_version.h]
