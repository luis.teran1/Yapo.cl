#ifndef CTEMPLATES_H
#define CTEMPLATES_H

#include "bpapi.h"

#include <string.h>

struct buf_string;

void fd_output_init(struct bpapi_output_chain *ochain);
void fd_output_set_fd(struct bpapi_output_chain *ochain, int *);
void stdout_output_init(struct bpapi_output_chain *ochain);
struct buf_string *buf_output_init(struct bpapi_output_chain *ochain);
void null_output_init(struct bpapi_output_chain *);

void hash_simplevars_init(struct bpapi_simplevars_chain *schain);

void pgsql_simplevars_init(struct bpapi_simplevars_chain *schain);
void pgsql_simplevars_setconn(struct bpapi_simplevars_chain *schain, void *conn);

#define bpapi_common_init(api) \
(api)->debug.source_line = __LINE__; \
(api)->debug.source_file = __FILE__; \
(api)->filter_state = default_filter_state; \
(api)->ochain.next = NULL; \
(api)->schain.next = NULL; \
(api)->vchain.next = NULL;

#define BPAPI_INIT_APP(api, host_bconf, app, otype, stype) \
bpapi_common_init(api) \
otype##_output_init(&(api)->ochain); \
stype##_simplevars_init(&(api)->schain); \
bconf_vtree_app(&(api)->vchain, host_bconf, app)

#define BPAPI_INIT_BCONF(api, bconf, otype, stype) \
bpapi_common_init(api) \
otype##_output_init(&(api)->ochain); \
stype##_simplevars_init(&(api)->schain); \
bconf_vtree(&(api)->vchain, bconf)


#endif
