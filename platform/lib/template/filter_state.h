#ifndef _FILTER_STATE_H_
#define _FILTER_STATE_H_
/*
 * Filter state handles the persistent state that filters might need to keep between calls
 * to filters.
 *
 * The way to access filter state is by calling the filter_state function in a filter
 * with a bpapi and a key and a callback+callbackarg. It will attempt to extract the
 * opaque value associated with the key and if it doesn't exist the callback will be called
 * to set up the value.
 *
 * The callback fills in the filter_state_callback_data with the value that needs to be
 * stored, a free() function if necessary and the type of the data. The type can be:
 *  - FS_GLOBAL - persists until process exit.
 *  - FS_THREAD - persists until thread exit.
 *  - FS_SESSION - persists until template rendering ends.
 */

struct filter_state_data {
	void *value;
	void (*freefn)(void *);
	enum fs_type {
		FS_GLOBAL,
		FS_THREAD,
		FS_SESSION
	} type;
};

typedef void (*filter_state_cb_t)(struct bpapi *, const char *, struct filter_state_data *, void *);

extern void *(*default_filter_state)(struct bpapi *, const char *, filter_state_cb_t, void *);

/*
 * Default bconf filter_state implementation.
 */
struct bconf_node;
void *filter_state_bconf(struct bpapi *, const char *, filter_state_cb_t, void *);
void filter_state_bconf_init(struct bconf_node **, enum fs_type);
void filter_state_bconf_init_all(struct bconf_node **);
void filter_state_bconf_free(struct bconf_node **);
void filter_state_bconf_set(struct bconf_node **, const char *, void *, void (*)(void *), enum fs_type);

void *filter_state(struct bpapi *, const char *key, filter_state_cb_t setup_cb, void *setup_cb_arg);


/*
 * filter_state_glue is for simplifying setting up all the plumbing for multiple filter states in one session.
 * Each session will allocate a struct filter_state_glue with the same lifetime as the bpapi it serves.
 * At the beginning of the session we will set up the vtree for a bpapi that will contain all the necessary shadows to make
 * filter_states work correctly and initialize the session filter state. thread_fs is optional, if not provided, the thread
 * filter_state will live in the session filter state.
 */
struct filter_state_bconf_glue {
	struct shadow_vtree global_shadow, thread_shadow, session_shadow;
	struct bpapi_vtree_chain global_vt, thread_vt, session_vt;
	struct bconf_node *session_fs;
};

void filter_state_bconf_glue_session_start(struct bpapi_vtree_chain *out, struct filter_state_bconf_glue *, struct bconf_node *global, struct bconf_node *thread, struct bpapi_vtree_chain *in);
void filter_state_bconf_glue_session_end(struct filter_state_bconf_glue *);
#endif
