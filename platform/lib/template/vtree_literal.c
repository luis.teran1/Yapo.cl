
#include "vtree_literal.h"

#include "memalloc_functions.h"
#include "vtree.h"
#include "vtree_value.h"

#include <stdbool.h>

static int
get_index(const struct vtree_keyvals *data, int argc, const char **argv) {
	if (argc < 1)
		return -1;

	if (data->type == vktList) {
		char *ep;
		int idx = strtol(argv[0], &ep, 10);
		if (*argv[0] == '\0' || *ep != '\0' || idx < 0 || idx >= data->len)
			return -1;
		return idx;
	} else {
		for (int idx = 0 ; idx < data->len ; idx++) {
			if (strcmp(argv[0], data->list[idx].key) == 0)
				return idx;
		}
		return -1;
	}

}

static int
vtree_literal_getlen(struct bpapi_vtree_chain *vchain, enum bpcacheable *cc, int argc, const char **argv) {
	const struct vtree_keyvals *data = vchain->data;

	if (argc == 0)
		return data->len;

	int idx = get_index(data, argc, argv);
	if (idx < 0 || data->list[idx].type != vkvNode)
		return 0;
	return vtree_getlen_cachev(&data->list[idx].v.node, cc, NULL, argc - 1, argv + 1);
}

static const char *
vtree_literal_get(struct bpapi_vtree_chain *vchain, enum  bpcacheable *cc, int argc, const char **argv) {
	const struct vtree_keyvals *data = vchain->data;

	int idx = get_index(data, argc, argv);
	if (idx < 0)
		return NULL;

	switch(data->list[idx].type) {
	case vkvNone:
		return NULL;
	case vkvValue:
		if (argc == 1)
			return data->list[idx].v.value;
		return NULL;
	case vkvNode:
		if (argc == 1)
			return NULL;
		return vtree_get_cachev(&data->list[idx].v.node, cc, NULL, argc - 1, argv + 1);
	}
	return NULL;
}

static int
vtree_literal_haskey(struct bpapi_vtree_chain *vchain, enum  bpcacheable *cc, int argc, const char **argv) {
	const struct vtree_keyvals *data = vchain->data;

	int idx = get_index(data, argc, argv);
	if (idx < 0)
		return 0;

	switch(data->list[idx].type) {
	case vkvNone:
	case vkvValue:
		if (argc == 1)
			return 1;
		return 0;
	case vkvNode:
		if (argc == 1)
			return 1;
		return vtree_haskey_cachev(&data->list[idx].v.node, cc, NULL, argc - 1, argv + 1);
	}
	return 0;
}

static void
vtree_literal_cleanup(struct bpapi_loop_var *loop) {
	free(loop->l.list);
}

static void
vtree_literal_cleanup_all(struct bpapi_loop_var *loop) {
	for (int i = 0 ; i < loop->len ; i++)
		free((void*)loop->l.list[i]);
	free(loop->l.list);
}

static void
vtree_literal_fetch_keys(struct bpapi_vtree_chain *vchain, struct bpapi_loop_var *loop, enum bpcacheable *cc, int argc, const char **argv) {
	const struct vtree_keyvals *data = vchain->data;

	if (argc == 0) {
		loop->len = data->len;
		loop->l.list = xmalloc(loop->len * sizeof(*loop->l.list));
		if (data->type == vktList) {
			for (int i = 0 ; i < loop->len ; i++)
				xasprintf((char**)&loop->l.list[i], "%d", i);
			loop->cleanup = vtree_literal_cleanup_all;
			return;
		}
		for (int i = 0 ; i < data->len ; i++)
			loop->l.list[i] = data->list[i].key;
		loop->cleanup = vtree_literal_cleanup;
		return;
	}

	int idx = get_index(data, argc, argv);
	if (idx >= 0 && data->list[idx].type == vkvNode) {
		vtree_fetch_keys_cachev(&data->list[idx].v.node, loop, cc, argc - 1, argv + 1);
		return;
	}

	loop->len = 0;
	loop->l.vlist = NULL;
	loop->cleanup = NULL;
}

static void
vtree_literal_fetch_values(struct bpapi_vtree_chain *vchain, struct bpapi_loop_var *loop, enum bpcacheable *cc, int argc, const char **argv) {
	const struct vtree_keyvals *data = vchain->data;

	loop->len = 0;
	loop->cleanup = NULL;

	if (argc < 1)
		return;

	if (argv[0] == VTREE_LOOP) {
		loop->len = data->len;
		loop->l.list = xmalloc(sizeof (*loop->l.list) * data->len);
		loop->cleanup = vtree_literal_cleanup;
		for (int i = 0 ; i < data->len ; i++) {
			switch(data->list[i].type) {
			case vkvNone:
				loop->l.list[i] = NULL;
				break;
			case vkvValue:
				if (argc == 1)
					loop->l.list[i] = data->list[i].v.value;
				else
					loop->l.list[i] = NULL;
				break;
			case vkvNode:
				loop->l.list[i] = vtree_get_cachev(&data->list[i].v.node, cc, NULL, argc - 1, argv + 1);
				break;
			}
		}
		return;
	}

	int idx = get_index(data, argc, argv);
	if (idx >= 0 && data->list[idx].type == vkvNode)
		vtree_fetch_values_cachev(&data->list[idx].v.node, loop, cc, argc - 1, argv + 1);
}

static void
vtree_literal_fetch_keys_by_value(struct bpapi_vtree_chain *vchain, struct bpapi_loop_var *loop, enum bpcacheable *cc, const char *value, int argc, const char **argv) {
	const struct vtree_keyvals *data = vchain->data;

	loop->len = 0;
	loop->cleanup = NULL;

	if (argc < 1)
		return;

	if (argv[0] == VTREE_LOOP) {
		loop->l.list = xmalloc(data->len * sizeof(*loop->l.list));
		int n = 0;
		for (int i = 0 ; i < data->len ; i++) {
			bool match = false;
			switch(data->list[i].type) {
			case vkvNone:
				break;
			case vkvValue:
				if (argc == 1 && strcmp(value, data->list[i].v.value) == 0)
					match = true;
				break;
			case vkvNode:
				{
					const char *v = vtree_get_cachev(&data->list[i].v.node, cc, NULL, argc - 1, argv + 1);
					if (v && strcmp(value, v) == 0)
						match = true;
				}
				break;
			}
			if (!match)
				continue;
			if (data->type == vktList) {
				xasprintf((char**)&loop->l.list[n], "%d", i);
			} else {
				loop->l.list[n] = data->list[i].key;
			}
			n++;
		}
		if (data->type == vktList)
			loop->cleanup = vtree_literal_cleanup_all;
		else
			loop->cleanup = vtree_literal_cleanup;
		return;
	}

	int idx = get_index(data, argc, argv);
	if (idx >= 0 && data->list[idx].type == vkvNode) {
		vtree_fetch_keys_by_value_cachev(&data->list[idx].v.node, loop, cc, value, argc - 1, argv + 1);
		return;
	}

	loop->len = 0;
	loop->l.vlist = NULL;
	loop->cleanup = NULL;
}

static struct bpapi_vtree_chain *
vtree_literal_getnode(struct bpapi_vtree_chain *vchain, enum bpcacheable *cc, struct bpapi_vtree_chain *dst, int argc, const char **argv) {
	const struct vtree_keyvals *data = vchain->data;

	if (argc == 0)
		return vchain;

	int idx = get_index(data, argc, argv);
	if (idx < 0)
		return NULL;

	switch(data->list[idx].type) {
	case vkvNone:
		return NULL;
	case vkvValue:
		if (argc == 1) {
			dst->fun = &vtree_value_vtree;
			dst->data = (void*)data->list[idx].v.value;
			dst->next = NULL;
			return dst;
		}
		return NULL;
	case vkvNode:
		return vtree_getnode_cachev(&data->list[idx].v.node, cc, dst, NULL, argc - 1, argv + 1);
	}
	return NULL;
}

static void
vtree_literal_fetch_nodes(struct bpapi_vtree_chain *vchain, struct bpapi_loop_var *loop, enum bpcacheable *cc, int argc, const char **argv) {
	const struct vtree_keyvals *data = vchain->data;

	if (argc == 0) {
		loop->len = data->len;
		loop->l.vlist = xmalloc(loop->len * sizeof(*loop->l.vlist));
		for (int i = 0 ; i < data->len ; i++) {
			switch(data->list[i].type) {
			case vkvNone:
				memset(&loop->l.vlist[i], 0, sizeof(loop->l.vlist[i]));
				break;
			case vkvValue:
				loop->l.vlist[i].fun = &vtree_value_vtree;
				loop->l.vlist[i].data = (void*)data->list[i].v.value;
				loop->l.vlist[i].next = NULL;
				break;
			case vkvNode:
				loop->l.vlist[i] = data->list[i].v.node;
				break;
			}
		}
		loop->cleanup = vtree_literal_cleanup;
		return;
	}

	int idx = get_index(data, argc, argv);
	if (idx >= 0 && data->list[idx].type == vkvNode) {
		vtree_fetch_nodes_cachev(&data->list[idx].v.node, loop, cc, argc - 1, argv + 1);
		return;
	}

	loop->len = 0;
	loop->l.vlist = NULL;
	loop->cleanup = NULL;
}

static void
vtree_literal_cleanup_keys_and_values(struct vtree_keyvals *loop) {
	struct bpapi_vtree_chain *dsts = (struct bpapi_vtree_chain*)(loop->list + loop->len);
	for (int i = 0 ; i < loop->len ; i++)
		vtree_free(&dsts[i]);
	free(loop->list);
}

static void
vtree_literal_fetch_keys_and_values(struct bpapi_vtree_chain *vchain, struct vtree_keyvals *loop, enum bpcacheable *cc, int argc, const char **argv) {
	const struct vtree_keyvals *data = vchain->data;

	if (argc == 0) {
		*loop = *data;
		loop->cleanup = NULL;
		return;
	}

	if (argv[0] == VTREE_LOOP) {
		loop->len = data->len;
		loop->type = data->type;

		/* We need to hold on to any getnode destinations until we're freed. */
		loop->list = xmalloc(sizeof (*loop->list) * data->len + sizeof(struct bpapi_vtree_chain) * data->len);
		struct bpapi_vtree_chain *dsts = (struct bpapi_vtree_chain*)(loop->list + data->len);
		memset(dsts, 0, sizeof(struct bpapi_vtree_chain) * data->len);

		loop->cleanup = vtree_literal_cleanup_keys_and_values;
		for (int i = 0 ; i < data->len ; i++) {
			loop->list[i].key = data->list[i].key;
			switch(data->list[i].type) {
			case vkvNone:
				loop->list[i].type = vkvNone;
				break;
			case vkvValue:
				if (argc == 1) {
					loop->list[i].type = vkvValue;
					loop->list[i].v.value = data->list[i].v.value;
				} else {
					loop->list[i].type = vkvNone;
				}
				break;
			case vkvNode:
				{
					struct bpapi_vtree_chain *node = vtree_getnode_cachev(&data->list[i].v.node, cc, &dsts[i], NULL, argc - 1, argv + 1);
					const char *v;

					if (!node) {
						loop->list[i].type = vkvNone;
					} else if ((v = vtree_get(node, NULL))) {
						loop->list[i].type = vkvValue;
						loop->list[i].v.value = v;
					} else {
						loop->list[i].type = vkvNode;
						loop->list[i].v.node = *node;
					}
				}
				break;
			}
		}
		return;
	}
}

const struct bpapi_vtree vtree_literal_vtree = {
	vtree_literal_getlen,
	vtree_literal_get,
	vtree_literal_haskey,
	vtree_literal_fetch_keys,
	vtree_literal_fetch_values,
	vtree_literal_fetch_keys_by_value,
	vtree_literal_getnode,
	vtree_literal_fetch_nodes,
	vtree_literal_fetch_keys_and_values,
	NULL, //vtree_literal_free
};

