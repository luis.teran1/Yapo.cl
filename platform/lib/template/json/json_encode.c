#include <stdio.h>
#include <bpapi.h>

/* encode a string as the contents of a JSON string */
struct _jsonenc {
	int pos;
	int escape_solus;
	char buf[1024];
};

static void
flush_jsonencode(struct bpapi_output_chain *ochain) {
	struct _jsonenc *je = ochain->data;
	if (je->pos) {
		bpo_outstring_raw(ochain->next, je->buf, je->pos);
		je->pos = 0;
	}
}

static int
jsonencode_outstring_raw(struct bpapi_output_chain *ochain, const char *str, size_t len) {
	const unsigned char *from;
	const unsigned char *end;
	unsigned char c;
	int res = 0;

	struct _jsonenc *je = ochain->data;

	from = (unsigned char*) str;
	end = (unsigned char*) str + len;

	while (from < end) {
		if (je->pos >= (int)(sizeof(je->buf)-8)) {
			res += je->pos;
			flush_jsonencode(ochain);
		}

		c = *from++;

		switch (c) {
		case '\b':
			je->buf[je->pos++] = '\\';
			je->buf[je->pos++] = 'b';
			break;
		case '\f':
			je->buf[je->pos++] = '\\';
			je->buf[je->pos++] = 'f';
			break;
		case '\n':
			je->buf[je->pos++] = '\\';
			je->buf[je->pos++] = 'n';
			break;
		case '\r':
			je->buf[je->pos++] = '\\';
			je->buf[je->pos++] = 'r';
			break;
		case '\t':
			je->buf[je->pos++] = '\\';
			je->buf[je->pos++] = 't';
			break;
		case '\\':
		case '"':
			je->buf[je->pos++] = '\\';
			je->buf[je->pos++] = c;
			break;
		case '/':
			if (je->escape_solus)
				je->buf[je->pos++] = '\\';
			je->buf[je->pos++] = c;
			break;
		default:
			if (c > 0x1f) {
				je->buf[je->pos++] = c;
			} else {
				/* encode other control characters as unicode hex */
				int w = snprintf(je->buf + je->pos, sizeof(je->buf)-je->pos, "\\u%04x", c);
				if (w == 6)
					je->pos += w;
			}
			break;
		}
	}

	return 0;
}

static int
init_jsonencode(const struct bpfilter *filter, struct bpapi *api, int argc, const struct tpvar **argv) {
	struct _jsonenc *je = BPAPI_OUTPUT_DATA(api);

	je->escape_solus = argc > 0 ? tpvar_int(argv[0]) : 0;

	return 0;
}

static void
fini_jsonencode(struct bpapi_output_chain *ochain) {
	flush_jsonencode(ochain);
}

static const struct bpapi_output jsonencode_output = {
	self_outstring_fmt,
	jsonencode_outstring_raw,
	fini_jsonencode,
	flush_jsonencode
};

ADD_OUTPUT_FILTER(jsonencode, sizeof(struct _jsonenc));
