#include "bpapi.h"
#include "json_vtree.h"

#include <string.h>

static int
init_json_vtree(const struct bpfilter *filter, struct bpapi *api, int argc, const struct tpvar **argv) { 
	struct shadow_vtree *subtree = BPAPI_VTREE_DATA(api);
	const char * json_str;
	const char * root_name = NULL;
	char *fs;
	int validate_utf8 = 0;

	json_str = tpvar_str(argv[0], &fs);
	if (!json_str || !*json_str) {
		free(fs);
		return 0;
	}
	if (argc >= 2)
		TPVAR_STRTMP(root_name, argv[1]);
	if (argc >= 3)
		validate_utf8 = tpvar_int(argv[2]);

	if (!root_name || !*root_name)
		root_name = "root";

	json_vtree(&subtree->vtree, root_name, json_str, -1, validate_utf8);

	free(fs);

	return 0;
}

ADD_FILTER(json_vtree, NULL, 0, NULL, 0, &shadow_vtree, sizeof (struct shadow_vtree));
