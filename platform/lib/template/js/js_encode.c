#include <bpapi.h>

/* encode a string as the contents of a javascript string */
struct _jsenc{
	int pos;
	char buf[1024];
};

static void
flush_jsencode(struct bpapi_output_chain *ochain) {
	struct _jsenc *je = ochain->data;
	if (je->pos) {
		bpo_outstring_raw(ochain->next, je->buf, je->pos);
		je->pos = 0;
	}
}

static int
jsencode_outstring_raw(struct bpapi_output_chain *ochain, const char *str, size_t len) {
	const unsigned char *from;
	const unsigned char *end;
	unsigned char c;
	int res = 0;

	struct _jsenc *je = ochain->data;

	from = (unsigned char*) str;
	end = (unsigned char*) str + len;

	while (from < end) {
		if (je->pos >= (int)(sizeof(je->buf)-4)) {
			res += je->pos;
			flush_jsencode(ochain);
		}

		c = *from++;

		switch (c) {
		case '\n':
			je->buf[je->pos++] = '\\';
			je->buf[je->pos++] = 'n';
			break;
		case '\\':
		case '\'':
		case '"':
			je->buf[je->pos++] = '\\';
			je->buf[je->pos++] = c;
			break;
		case '<':
		case '>':
			/* make sure that inline JS is not inserting HTML keywords like </script> */
			je->buf[je->pos++] = '\\';
			je->buf[je->pos++] = 'x';
			je->buf[je->pos++] = '3';
			je->buf[je->pos++] = c == '<' ? 'c' : 'e';
			break;
		default:
			/* silently ignore any unhandled control char under tab */
			if (c < 9)
				break;
			je->buf[je->pos++] = c;
			break;
		}
	}

	return 0;
}

static int
init_jsencode(const struct bpfilter *filter, struct bpapi *api, int argc, const struct tpvar **argv) {
	return 0;
}

static void
fini_jsencode(struct bpapi_output_chain *ochain) {
	flush_jsencode(ochain);
}

static const struct bpapi_output jsencode_output = {
	self_outstring_fmt,
	jsencode_outstring_raw,
	fini_jsencode,
	flush_jsencode
};

ADD_OUTPUT_FILTER(jsencode, sizeof(struct _jsenc));

