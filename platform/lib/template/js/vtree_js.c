#include <stdio.h>

#include "bpapi.h"
#include "json_vtree.h"


/* XXX return the value, do not print it */
static int
pf(void *a, int d, int n, const char *fmt, ...) {
	struct bpapi *api = a;
	int res;
	char *str;
	int len;

	while (d--) {
		bpapi_outstring_raw(api, "\t", 1);
	}


	ALLOCA_VPRINTF(len, str, fmt);
	res = bpapi_outstring_raw(api, str, len);
	if(n) bpapi_outstring_raw(api, "\n", 1);
	return res;
}
static int
sf(void *a, int d, int n, const char *fmt, ...) {
	struct bpapi *api = a;
	int res;
	char *str;
	int len;


	ALLOCA_VPRINTF(len, str, fmt);
	res = bpapi_outstring_raw(api, str, len);
	return res;
}

static const struct tpvar *
vtree_js_common(struct bpapi *api, struct tpvar *dst, enum bpcacheable *cc, int argc, const struct tpvar **argv,
		int use_arrays, int (*pf)(void *, int, int, const char *, ...)) {
	int i = 0;
	struct bpapi_vtree_chain n = {0}, nn = {0}, *root;

	if (argv[0]->type == TPV_NODE) {
		root = tpvar_node(argv[0]);
	} else {
		root = &api->vchain;
		for (i = 0 ; i < argc ; i++) {
			char *fptr;

			if ((root = vtree_getnode(root, &nn, tpvar_str(argv[i], &fptr), NULL))) {
				n = *root;
				root = &n;
				memset(&nn, 0, sizeof(nn));
			}

			free(fptr);
		}
	}

	if (!root) {
		bpapi_outstring_raw(api, "null", 4);
		return TPV_SET(dst, TPV_NULL);
	}

	vtree_json(root, use_arrays, 0, pf, api);
	vtree_free(&n);
	return TPV_SET(dst, TPV_NULL);
}

static const struct tpvar *
vtree_js(struct bpapi *api, struct tpvar *dst, enum bpcacheable *cc, int argc, const struct tpvar **argv) {
	return vtree_js_common(api, dst, cc, argc, argv, 0, sf);
}

ADD_TEMPLATE_FUNCTION(vtree_js);
static const struct tpvar *
vtree_js_pretty(struct bpapi *api, struct tpvar *dst, enum bpcacheable *cc, int argc, const struct tpvar **argv) {
	return vtree_js_common(api, dst, cc, argc, argv, 0, pf);
}
ADD_TEMPLATE_FUNCTION(vtree_js_pretty);

static const struct tpvar *
vtree_js_arrays(struct bpapi *api, struct tpvar *dst, enum bpcacheable *cc, int argc, const struct tpvar **argv) {
	return vtree_js_common(api, dst, cc, argc, argv, 1, sf);
}

ADD_TEMPLATE_FUNCTION(vtree_js_arrays);
