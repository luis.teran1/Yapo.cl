#ifndef REDIS_FILTERS_H
#define REDIS_FILTERS_H

#define REDIS_FILTER_STATE_KEY_PREFIX "redis_fsconf_"

/* Filter state for redis filters and functions. */
struct redis_fsconf {
	struct fd_pool *pool;
	struct bpapi_vtree_chain engine_node;
};

/* Data Structures */
struct redis_filter_data {
	struct buf_string buf;

	const char *key;
	const char *field;
	char *fk, *ff;

	int expire;

	struct redis_fsconf *fsconf;
};

struct redis_range_data {
	struct bconf_node *node;
	int idx;
};

/* Function prototypes */

struct redis_fsconf * get_redis_fsconf(struct bpapi *api, const char *engine);
struct fd_pool * get_redis_pool(struct bpapi *api, const char *engine);

#endif
