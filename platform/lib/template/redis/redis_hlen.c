
#include <bpapi.h>
#include <sredisapi.h>
#include <fd_pool.h>

#include "redis_filters.h"

static const struct tpvar *
redis_hlen(struct bpapi *api, struct tpvar *dst, enum bpcacheable *cc, int argc, const struct tpvar *argv[]) {
	const char *engine;
	const char *key;
	struct fd_pool *pool;
	int res = -1;

	switch (argc) {
	case 2:
		TPVAR_STRTMP(engine, argv[0]);
		TPVAR_STRTMP(key, argv[1]);
		break;
	default:
		return TPV_SET(dst, TPV_NULL);
	}

	pool = get_redis_pool(api, engine);
	if (pool) {
		struct fd_pool_conn *conn = redis_sock_conn(pool, "slave");

		res = redis_sock_hlen(conn, key);

		fd_pool_free_conn(conn);
	}

	*cc = BPCACHE_CANT;
	return TPV_SET(dst, TPV_INT(res));
}

ADD_TEMPLATE_FUNCTION(redis_hlen);

