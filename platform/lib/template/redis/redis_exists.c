
#include <bpapi.h>
#include <sredisapi.h>
#include <fd_pool.h>

#include "redis_filters.h"


static const struct tpvar *
tmpl_redis_exists(struct bpapi *api, struct tpvar *dst, enum bpcacheable *cc, int argc, const struct tpvar *argv[]) {
	const char *key;
	const char *engine;
	int i, res = -1;

	TPVAR_STRTMP(engine, argv[0]);

	switch (argc) {
	case 2:
		TPVAR_STRTMP(key, argv[1]);
		break;
	case 3:
		{
			const char *t1, *t2;

			TPVAR_STRTMP(t1, argv[1]);
			TPVAR_STRTMP(t2, argv[2]);
			ALLOCA_PRINTF(i, key, "%s_%s", t1, t2);
		}
		break;
	default:
		return TPV_SET(dst, TPV_NULL);
	}

	struct fd_pool *pool = get_redis_pool(api, engine);
	if (pool) {
		struct fd_pool_conn *conn = redis_sock_conn(pool, "slave");

		res = redis_sock_exists(conn, key);

		fd_pool_free_conn(conn);
	}

	*cc = BPCACHE_CANT;
	return TPV_SET(dst, TPV_INT(res));
}

ADD_TEMPLATE_FUNCTION_WITH_NAME(tmpl_redis_exists, redis_exists);
