
#include <bpapi.h>
#include <sredisapi.h>
#include <fd_pool.h>
#include <logging.h>

#include "redis_filters.h"


static int
init_redis_set(const struct bpfilter *filter, struct bpapi *api, int argc, const struct tpvar **argv) {
	struct redis_filter_data *data = BPAPI_OUTPUT_DATA(api);
	const char *engine;

	TPVAR_STRTMP(engine, argv[0]);
	data->key = tpvar_str(argv[1], &data->fk);
	if (argc >= 3)
		data->expire = tpvar_int(argv[2]);
	else
		data->expire = -1;

	data->fsconf = get_redis_fsconf(api, engine);
	if (!data->fsconf)
		return 1;

	return 0;
}

static void
fini_redis_set(struct bpapi_output_chain *ochain) {
	struct redis_filter_data *data = ochain->data;
	int err;

	if (!data->fsconf)
		goto out;

	struct fd_pool_conn *conn = redis_sock_conn(data->fsconf->pool, "master");

	err = redis_sock_set(conn, data->key, data->buf.buf ?: "", data->expire);
	if (err < 0)
		log_printf(LOG_CRIT, "redis_sock_set call failed for %s:%s with error: %d", data->key, data->buf.buf ? : "", err);

	fd_pool_free_conn(conn);

out:
	free(data->buf.buf);
	free(data->fk);
}

const struct bpapi_output redis_set_output = {
	bufstring_outstring_fmt,
	bufstring_outstring_raw,
	fini_redis_set
};

ADD_OUTPUT_FILTER(redis_set, sizeof(struct redis_filter_data));
