#include <bpapi.h>
#include <sredisapi.h>
#include <fd_pool.h>

#include "redis_filters.h"


static const struct tpvar *
redis_zrank(struct bpapi *api, struct tpvar *dst, enum bpcacheable *cc, int argc, const struct tpvar *argv[]) {
	const char *key, *set, *engine;
	struct fd_pool *pool;
	int res = -1;

	TPVAR_STRTMP(engine, argv[0]);
	TPVAR_STRTMP(set, argv[1]);
	TPVAR_STRTMP(key, argv[2]);

	pool = get_redis_pool(api, engine);
	if (pool) {
		struct fd_pool_conn *conn = redis_sock_conn(pool, "slave");

		res = redis_sock_zrank(conn, set, key);

		fd_pool_free_conn(conn);
	}

	*cc = BPCACHE_CANT;
	return TPV_SET(dst, TPV_INT(res));
}

ADD_TEMPLATE_FUNCTION(redis_zrank);
