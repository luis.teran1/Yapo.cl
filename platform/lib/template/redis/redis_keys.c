
#include <bpapi.h>
#include <sredisapi.h>
#include <fd_pool.h>
#include <patchvars.h>
#include <bconf.h>

#include "redis_filters.h"


struct redis_keys_data {
	struct bconf_node *node;
};

static void
fini_redis_keys(struct patch_data *patch) {
	struct redis_keys_data *data = patch->user_data;
	bconf_free(&data->node);
	free(data);
}


static void
redis_keys_add(const void *value, size_t vlen, void *cbarg) {
	struct patch_data *patch = cbarg;
	struct redis_keys_data *data = patch->user_data;

	if (patch->prefix_len) {
		bps_insert(&patch->vars, patch->prefix, value);
		bconf_add_data(&data->node, patch->prefix, value);
	} else {
		bps_insert(&patch->vars, "redis_keys", value);
		bconf_add_data(&data->node, "redis_keys", value);
	}
}

static int
init_redis_keys(const struct bpfilter *filter, struct bpapi *api, int argc, const struct tpvar **argv) {
	struct patch_data *patch = BPAPI_SIMPLEVARS_DATA(api);
	const char *engine, *key, *var_name = NULL;
	char *fp = NULL;
	struct fd_pool *pool;
	struct shadow_vtree *subtree = BPAPI_VTREE_DATA(api);

	struct redis_keys_data *data;
	data = zmalloc(sizeof(struct redis_keys_data));


	TPVAR_STRTMP(engine, argv[0]);
	TPVAR_STRTMP(key, argv[1]);
	if (argc == 3) {
		var_name = tpvar_str(argv[2], &fp);
	}

	patchvars_init(patch, "redis_keys", var_name, var_name ? strlen(var_name) : 0, fini_redis_keys, data);

	pool = get_redis_pool(api, engine);
	if (pool) {
		struct fd_pool_conn *conn = redis_sock_conn(pool, "slave");

		redis_sock_keys(conn, key, redis_keys_add, patch);

		bconf_vtree(&subtree->vtree, data->node);

		fd_pool_free_conn(conn);
	}

	free(fp);

	return 0;
}

ADD_FILTER(redis_keys, NULL, 0, &patch_simplevars, sizeof(struct patch_data), &shadow_vtree, sizeof (struct shadow_vtree));
