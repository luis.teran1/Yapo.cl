#include <bpapi.h>
#include <sredisapi.h>
#include <fd_pool.h>
#include <vtree.h>
#include <bconf.h>
#include "logging.h"

#include "redis_filters.h"

static int
init_redis_session(const struct bpfilter *filter, struct bpapi *api, int argc, const struct tpvar **argv) {
	/* Currently a no-op due to filter states */
	return 0;
}

#define redis_session_vtree shadow_vtree

ADD_VTREE_FILTER(redis_session, sizeof(struct shadow_vtree));

