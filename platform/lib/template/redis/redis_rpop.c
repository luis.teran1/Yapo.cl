
#include <bpapi.h>
#include <sredisapi.h>
#include <fd_pool.h>

#include "redis_filters.h"


static const struct tpvar *
redis_rpop(struct bpapi *api, struct tpvar *dst, enum bpcacheable *cc, int argc, const struct tpvar *argv[]) {
	const char *engine;
	const char *key;
	char *tmp = NULL;
	struct fd_pool *pool;

	if (argc != 2)
		return TPV_SET(dst, TPV_NULL);

	TPVAR_STRTMP(engine, argv[0]);
	TPVAR_STRTMP(key, argv[1]);

	pool = get_redis_pool(api, engine);
	if (pool) {
		struct fd_pool_conn *conn = redis_sock_conn(pool, "master");

		tmp = redis_sock_rpop(conn, key);

		fd_pool_free_conn(conn);
	}

	*cc = BPCACHE_CANT;
	return TPV_SET(dst, tmp ? TPV_DYNSTR(tmp) : TPV_NULL);
}

ADD_TEMPLATE_FUNCTION(redis_rpop);
