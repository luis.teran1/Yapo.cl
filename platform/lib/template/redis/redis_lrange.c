#include <bpapi.h>
#include <sredisapi.h>
#include <fd_pool.h>
#include <patchvars.h>
#include <bconf.h>

#include "redis_filters.h"


static void
fini_redis_lrange(struct patch_data *patch) {
	struct redis_range_data *data = patch->user_data;
	bconf_free(&data->node);
	free(data);
}

static void
redis_lrange_add(const void *value, size_t vlen, void *cbarg) {
	struct patch_data *patch = cbarg;
	struct redis_range_data *data = patch->user_data;
	char vtree_key[256];
	char k[256];

	snprintf(k, sizeof(k), "%s_value", patch->prefix);
	bps_insert(&patch->vars, k, value);

	snprintf(vtree_key, sizeof(vtree_key), "%s.%d.value", patch->prefix, data->idx);
	bconf_add_data(&data->node, vtree_key, value);
	data->idx++;
}

static int
init_redis_lrange(const struct bpfilter *filter, struct bpapi *api, int argc, const struct tpvar **argv) {
	struct patch_data *patch = BPAPI_SIMPLEVARS_DATA(api);
	const char *engine, *key, *prefix = NULL;
	char *fp = NULL;
	struct fd_pool *pool;
	struct shadow_vtree *subtree = BPAPI_VTREE_DATA(api);
	int start, stop;

	struct redis_range_data *data;
	data = zmalloc(sizeof(struct redis_range_data));

	TPVAR_STRTMP(engine, argv[0]);
	TPVAR_STRTMP(key, argv[1]);
	start = tpvar_int(argv[2]);
	stop = tpvar_int(argv[3]);
	if (argc == 5) {
		prefix = tpvar_str(argv[4], &fp);
	} else {
		prefix = "redis_lrange";
	}

	patchvars_init(patch, "redis_lrange", prefix, strlen(prefix), fini_redis_lrange, data);

	pool = get_redis_pool(api, engine);
	if (pool) {
		struct fd_pool_conn *conn = redis_sock_conn(pool, "slave");

		redis_sock_lrange(conn, key, start, stop, redis_lrange_add, patch);

		bconf_vtree(&subtree->vtree, data->node);

		fd_pool_free_conn(conn);
	}

	free(fp);

	return 0;
}

ADD_FILTER(redis_lrange, NULL, 0, &patch_simplevars, sizeof(struct patch_data), &shadow_vtree, sizeof (struct shadow_vtree));

