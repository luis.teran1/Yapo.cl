
#include <bpapi.h>
#include <sredisapi.h>
#include <fd_pool.h>
#include <patchvars.h>
#include <bconf.h>

#include "redis_filters.h"


struct redis_hgetall_data {
	struct bconf_node *node;
};

static void
fini_redis_hget_all(struct patch_data *patch) {
	struct redis_hgetall_data *data = patch->user_data;
	bconf_free(&data->node);
	free(data);
}


static void
redis_hgetall_add(const void *key, size_t klen, const void *value, size_t vlen, void *cbarg) {
	struct patch_data *patch = cbarg;
	struct redis_hgetall_data *data = patch->user_data;
	char *vtree_key;

	if (patch->prefix_len) {
		char sep = '_';
		char k[klen + patch->prefix_len + 2];

		memcpy(k, patch->prefix, patch->prefix_len);
		memcpy(k + patch->prefix_len, &sep, 1);
		memcpy(k + 1 + patch->prefix_len, key, klen);
		k[klen + 1 + patch->prefix_len] = '\0';

		bps_insert(&patch->vars, k, value);
		xasprintf(&vtree_key, "%s.%.*s", patch->prefix, (int)klen, (const char*)key);
		bconf_add_data(&data->node, vtree_key, value);
	} else {
		xasprintf(&vtree_key, "redis_hgetall.%.*s", (int)klen, (const char*)key);

		bps_insert(&patch->vars, key, value);
		bconf_add_data(&data->node, vtree_key, value);
	}
	free(vtree_key);
}

static int
init_redis_hgetall(const struct bpfilter *filter, struct bpapi *api, int argc, const struct tpvar **argv) {
	struct patch_data *patch = BPAPI_SIMPLEVARS_DATA(api);
	const char *engine, *key, *prefix = NULL;
	char *fp = NULL;
	struct shadow_vtree *subtree = BPAPI_VTREE_DATA(api);

	struct redis_hgetall_data *data;
	data = zmalloc(sizeof(struct redis_hgetall_data));

	TPVAR_STRTMP(engine, argv[0]);
	TPVAR_STRTMP(key, argv[1]);
	if (argc == 3) {
		prefix = tpvar_str(argv[2], &fp);
	}

	patchvars_init(patch, "redis_hgetall", prefix, prefix ? strlen(prefix) : 0, fini_redis_hget_all, data);

	struct fd_pool *pool = get_redis_pool(api, engine);
	if (!pool) {
		goto out;
	}

	struct fd_pool_conn *conn = redis_sock_conn(pool, "slave");

	redis_sock_hgetall(conn, key, redis_hgetall_add, patch);

	bconf_vtree(&subtree->vtree, data->node);

	fd_pool_free_conn(conn);

out:
	free(fp);

	return 0;
}

ADD_FILTER(redis_hgetall, NULL, 0, &patch_simplevars, sizeof(struct patch_data), &shadow_vtree, sizeof (struct shadow_vtree));
