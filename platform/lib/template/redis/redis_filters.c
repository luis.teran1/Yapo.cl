#include "sredisapi.h"
#include "bpapi.h"
#include "vtree.h"
#include "util.h"
#include "bconf.h"
#include "logging.h"
#include "fd_pool.h"

#include "redis_filters.h"

struct redis_fs_state_selector {
	const char *engine;
	enum fs_type type;
};

static void
redis_fsconf_free(void *rfc) {
	struct redis_fsconf *c = rfc;

	fd_pool_free(c->pool);
	vtree_free(&c->engine_node);
	free(c);
}

static void
redis_filterstate_init(struct bpapi *api, const char *key, struct filter_state_data *fsd, void *v) {
	struct redis_fsconf *fsconf = zmalloc(sizeof(*fsconf));
	struct redis_fs_state_selector *state = v;

	const char *engine_path = vtree_get(&api->vchain, "redis", "engine_path", NULL);
	if (!engine_path)
		engine_path = "common";

	if (!vtree_getnode(&api->vchain, &fsconf->engine_node, engine_path, state->engine, NULL)) {
		log_printf(LOG_CRIT, "redis_filterstate_init: no redis engine node (%s.%s)", engine_path, state->engine);
		redis_fsconf_free(fsconf);
		return;
	}

	fsconf->pool = fd_pool_create(&fsconf->engine_node, 0, NULL);

	fsd->value = fsconf;
	fsd->freefn = redis_fsconf_free;
	fsd->type = state->type;
	return;
}


struct redis_fsconf *
get_redis_fsconf(struct bpapi *api, const char *engine) {
	char buf[128] = { REDIS_FILTER_STATE_KEY_PREFIX };

	/*
	 * Replace all dots in the engine name since the key will be added without
	 * tokenization while lookups are tokenized.
	 */
	const char *tmp = engine;
	char *esc = buf + sizeof(REDIS_FILTER_STATE_KEY_PREFIX)-1;

	while (*tmp && esc < buf + sizeof(buf) - 2) {
		if (*tmp == '.') {
			*esc++ = '_';
			*esc++ = '_';
		} else {
			*esc++ = *tmp;
		}
		++tmp;
	}

	struct redis_fs_state_selector fs_init_state = {
		.engine = engine,
		.type = FS_GLOBAL
	};

	struct redis_fsconf *fsconf = filter_state(api, buf, redis_filterstate_init, (void*)&fs_init_state);

	return fsconf;
}

struct fd_pool *
get_redis_pool(struct bpapi *api, const char *engine) {
	struct redis_fsconf *fsconf = get_redis_fsconf(api, engine);
	if (!fsconf)
		return NULL;
	return fsconf->pool;
}

