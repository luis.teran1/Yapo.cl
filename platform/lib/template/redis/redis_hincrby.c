
#include <bpapi.h>
#include <sredisapi.h>
#include <fd_pool.h>
#include <logging.h>

#include "redis_filters.h"


static int
init_redis_hincrby(const struct bpfilter *filter, struct bpapi *api, int argc, const struct tpvar **argv) {
	struct redis_filter_data *data = BPAPI_OUTPUT_DATA(api);
	const char *engine;

	TPVAR_STRTMP(engine, argv[0]);
	data->key = tpvar_str(argv[1], &data->fk); /* hash */
	data->field = tpvar_str(argv[2], &data->ff); /* key */
	data->fsconf = get_redis_fsconf(api, engine);

	return 0;
}

static void
fini_redis_hincrby(struct bpapi_output_chain *ochain) {
	struct redis_filter_data *data = ochain->data;

	if (!data->fsconf)
		goto out;

	struct fd_pool_conn *conn = redis_sock_conn(data->fsconf->pool, "master");

	int err = redis_sock_hincrby(conn, data->key, data->field, atoi(data->buf.buf));
	if (err < 0)
		log_printf(LOG_CRIT, "redis_sock_hincrby call failed: %d", err);

	fd_pool_free_conn(conn);
out:
	free(data->buf.buf);
	free(data->fk);
	free(data->ff);
}

const struct bpapi_output redis_hincrby_output = {
	bufstring_outstring_fmt,
	bufstring_outstring_raw,
	fini_redis_hincrby
};

ADD_OUTPUT_FILTER(redis_hincrby, sizeof(struct redis_filter_data));
