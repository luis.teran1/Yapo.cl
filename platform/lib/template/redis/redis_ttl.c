
#include <bpapi.h>
#include <sredisapi.h>
#include <fd_pool.h>

#include "redis_filters.h"


static const struct tpvar *
tmpl_redis_ttl(struct bpapi *api, struct tpvar *dst, enum bpcacheable *cc, int argc, const struct tpvar *argv[]) {
	const char *key;
	struct fd_pool *pool;
	const char *engine;
	int ttl = -1;

	TPVAR_STRTMP(engine, argv[0]);
	TPVAR_STRTMP(key, argv[1]);

	pool = get_redis_pool(api, engine);
	if (pool) {
		struct fd_pool_conn *conn = redis_sock_conn(pool, "slave");

		ttl = redis_sock_ttl(conn, key);

		fd_pool_free_conn(conn);
	}

	*cc = BPCACHE_CANT;
	return TPV_SET(dst, TPV_INT(ttl));
}

ADD_TEMPLATE_FUNCTION_WITH_NAME(tmpl_redis_ttl, redis_ttl);
