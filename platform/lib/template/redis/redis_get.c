
#include <bpapi.h>
#include <sredisapi.h>
#include <fd_pool.h>

#include "redis_filters.h"


static const struct tpvar *
tmpl_redis_get(struct bpapi *api, struct tpvar *dst, enum bpcacheable *cc, int argc, const struct tpvar *argv[]) {
	const char *key;
	char *tmp = NULL;
	struct fd_pool *pool;
	const char *engine;

	TPVAR_STRTMP(engine, argv[0]);

	switch (argc) {
	case 2:
		TPVAR_STRTMP(key, argv[1]);
		break;
	case 3:
		{
			int i;
			const char *t1, *t2;

			TPVAR_STRTMP(t1, argv[1]);
			TPVAR_STRTMP(t2, argv[2]);
			ALLOCA_PRINTF(i, tmp, "%s_%s", t1, t2);
			key = tmp;
		}
		break;
	default:
		return TPV_SET(dst, TPV_NULL);
	}

	pool = get_redis_pool(api, engine);
	if (pool) {
		struct fd_pool_conn *conn = redis_sock_conn(pool, "slave");

		tmp = redis_sock_get(conn, key);

		fd_pool_free_conn(conn);
	}

	*cc = BPCACHE_CANT;
	return TPV_SET(dst, tmp ? TPV_DYNSTR(tmp) : TPV_NULL);
}

ADD_TEMPLATE_FUNCTION_WITH_NAME(tmpl_redis_get, redis_get);
