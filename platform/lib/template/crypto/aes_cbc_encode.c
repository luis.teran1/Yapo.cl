#include <bpapi.h>
#include <aes.h>
#include "aes_filters.h"

/*
 * AES CBC ENCODE FILTER
 */

static int 
aes_cbc_encode_raw(struct bpapi_output_chain *ochain, const char *str, size_t len) {
	struct _aes *aes = ochain->data;

	bufwrite(&aes->buf.buf, &aes->buf.len, &aes->buf.pos, str, len);

	return 0;
}

static int
init_aes_cbc_encode(const struct bpfilter *filter, struct bpapi *api, int argc, const struct tpvar **argv) {
	struct _aes *aes = BPAPI_OUTPUT_DATA(api);

	tpvar_strcpy(aes->iv, argv[0], sizeof(aes->iv));
	tpvar_strcpy(aes->key, argv[1], sizeof(aes->key));

	return 0;
}

static void
fini_aes_cbc_encode(struct bpapi_output_chain *ochain) {
	struct _aes *aes = ochain->data;

	if (aes->buf.buf) {
		char *tmp = aes_cbc_encode(aes->buf.buf, aes->buf.pos, aes->iv, aes->key, strlen(aes->key)); 	
		if (tmp) {
			bpo_outstring_raw(ochain->next, tmp, strlen(tmp));
			free(tmp);
		}
		free(aes->buf.buf);
	}
}

static const struct bpapi_output aes_cbc_encode_output = {
	self_outstring_fmt,
	aes_cbc_encode_raw,
	fini_aes_cbc_encode,
	NULL
};

ADD_OUTPUT_FILTER(aes_cbc_encode, sizeof(struct _aes));
