#include <bpapi.h>
#include <stdio.h>
#include "sha1.h"

/*
 * SHA 1 FILTER
 */

struct _sha1_hex {
	SHA1_CTX ctx;
};

static int 
sha1_hex_raw(struct bpapi_output_chain *ochain, const char *str, size_t len) {
	struct _sha1_hex *sha1_hex = ochain->data;

	SHA1Update(&sha1_hex->ctx, (const u_int8_t *)str, len);

	return 0;
}

static int
init_sha1_hex(const struct bpfilter *filter, struct bpapi *api, int argc, const struct tpvar **argv) {
	struct _sha1_hex *sha1_hex = BPAPI_OUTPUT_DATA(api);

	SHA1Init(&sha1_hex->ctx);
	return 0;
}

static void
flush_sha1_hex(struct bpapi_output_chain *ochain) {
	struct _sha1_hex *sha1_hex = ochain->data;
	unsigned char digest[SHA1_DIGEST_LENGTH];
	char sha1_sum[SHA1_DIGEST_LENGTH * 2 + 1];
	int n;

	SHA1Final(digest, &sha1_hex->ctx);

	for (n = 0; n < SHA1_DIGEST_LENGTH; n++)
		sprintf(&sha1_sum[n * 2], "%02x", (unsigned int)digest[n]);

	bpo_outstring_raw(ochain->next, sha1_sum, SHA1_DIGEST_LENGTH * 2);
}

static const struct bpapi_output sha1_hex_output = {
	self_outstring_fmt,
	sha1_hex_raw,
	NULL,
	flush_sha1_hex
};

ADD_OUTPUT_FILTER(sha1_hex, sizeof(struct _sha1_hex));
