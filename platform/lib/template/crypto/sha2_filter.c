#include <bpapi.h>
#include <stdio.h>
#include "sha2.h"

/*
 * SHA-2 FILTERS
 */

struct _sha2_funcs {
	const int DIGEST_LENGTH;

	void (*SHA2_Init)(SHA2_CTX *);
	void (*SHA2_Update)(SHA2_CTX *, const u_int8_t *, size_t);
	void (*SHA2_Final)(u_int8_t *, SHA2_CTX *);
};

struct _sha2_funcs sha256_funtbl = { SHA256_DIGEST_LENGTH, SHA256Init, SHA256Update, SHA256Final };
struct _sha2_funcs sha384_funtbl = { SHA384_DIGEST_LENGTH, SHA384Init, SHA384Update, SHA384Final };
struct _sha2_funcs sha512_funtbl = { SHA512_DIGEST_LENGTH, SHA512Init, SHA512Update, SHA512Final };

struct _sha2_ctx {
	SHA2_CTX sha2_ctx;
	struct _sha2_funcs *fun;
};

static int
init_sha2(const struct bpfilter *filter, struct bpapi *api, int argc, const struct tpvar **argv) {
	struct _sha2_ctx *ctx = BPAPI_OUTPUT_DATA(api);

	if (argc > 1)
		return 1;

	int bits = argc > 0 ? tpvar_int(argv[0]) : 256;
	switch (bits) {
		case 256:
			ctx->fun = &sha256_funtbl;
			break;
		case 384:
			ctx->fun = &sha384_funtbl;
			break;
		case 512:
			ctx->fun = &sha512_funtbl;
			break;
		default:
			return 1;
	};

	ctx->fun->SHA2_Init(&ctx->sha2_ctx);

	return 0;
}

static int
sha2_raw(struct bpapi_output_chain *ochain, const char *str, size_t len) {
	struct _sha2_ctx *ctx = ochain->data;

	ctx->fun->SHA2_Update(&ctx->sha2_ctx, (const u_int8_t *)str, len);

	return 0;
}

static void
flush_sha2(struct bpapi_output_chain *ochain) {
	struct _sha2_ctx *ctx = ochain->data;
	unsigned char digest[SHA512_DIGEST_LENGTH]; /* Large enough for up to 512 bit digests */

	ctx->fun->SHA2_Final(digest, &ctx->sha2_ctx);

	bpo_outstring_raw(ochain->next, (char*)digest, ctx->fun->DIGEST_LENGTH);
}

static const struct bpapi_output sha2_output = {
	self_outstring_fmt,
	sha2_raw,
	NULL,
	flush_sha2
};

ADD_OUTPUT_FILTER(sha2, sizeof(struct _sha2_ctx));
