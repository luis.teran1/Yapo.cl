#include <bpapi.h>
#include <aes.h>
#include "aes_filters.h"

static int 
aes_cbc_decode_raw(struct bpapi_output_chain *ochain, const char *str, size_t len) {
	struct _aes *aes = ochain->data;

	bufwrite(&aes->buf.buf, &aes->buf.len, &aes->buf.pos, str, len);

	return 0;
}

static int
init_aes_cbc_decode(const struct bpfilter *filter, struct bpapi *api, int argc, const struct tpvar **argv) {
	struct _aes *aes = BPAPI_OUTPUT_DATA(api);

	tpvar_strcpy(aes->key, argv[0], sizeof(aes->key));

	return 0;
}

static void
fini_aes_cbc_decode(struct bpapi_output_chain *ochain) {
	struct _aes *aes = ochain->data;
	int reslen;

	if (aes->buf.buf) {
		char *tmp = aes_cbc_decode(aes->buf.buf, aes->buf.pos, aes->key, strlen(aes->key), &reslen); 	
		if (tmp) {
			bpo_outstring_raw(ochain->next, tmp, strlen(tmp));
			free(tmp);
		}
		free(aes->buf.buf);
	}
}

static const struct bpapi_output aes_cbc_decode_output = {
	self_outstring_fmt,
	aes_cbc_decode_raw,
	fini_aes_cbc_decode,
	NULL
};

ADD_OUTPUT_FILTER(aes_cbc_decode, sizeof(struct _aes));
