#ifndef AES_FILTERS_H
#define AES_FILTERS_H
#include "base64.h"
struct _aes {
	char iv[BASE64_NEEDED(128 / 8)];
	char key[BASE64_NEEDED(128 / 8)];
	struct buf_string buf;
};
#endif
