#include "bpapi.h"
#include "linker_set.h"
#include <stdio.h>
#include <unistd.h>
#include <dlfcn.h>
#include "timer.h"
#include <pthread.h>
#include "qsort.h"
#include "atomic.h"
#include "vtree.h"
#include "bconf.h"

static struct templ **sorted_templates = NULL;

void *(*default_filter_state)(struct bpapi *, const char *, filter_state_cb_t, void *) = NULL;

int
bpapi_get_int(struct bpapi *bparse, const char *key) {
	const char *ptr;

	if ( (ptr = bpapi_get_element(bparse, key, 0)) == NULL)
		return 0;

	return atoi(ptr);
}

void
bps_set_int(struct bpapi_simplevars_chain *schain, const char *key, int value) {
	char *buf;
	int len;

	ALLOCA_PRINTF(len, buf, "%d", value);
	bps_insert(schain, key, buf);
}

void
bps_set_char(struct bpapi_simplevars_chain *schain, const char *key, char value) {
	char buf[2];

	buf[0] = value;
	buf[1] = '\0';

	bps_insert(schain, key, buf);
}

void
bps_set_printf(struct bpapi_simplevars_chain *schain, const char *key, const char *fmt, ...) {
	char *buf;
	int len;

	ALLOCA_VPRINTF(len, buf, fmt);
	bps_insert(schain, key, buf);
}

void
bps_set_hex(struct bpapi_simplevars_chain *schain, const char *key, const void *data, size_t len) {
	char hexbuf[len * 2 + 1];
	unsigned int i;
	const char hexchars[] = "0123456789ABCDEF";
	const unsigned char *dp = data;

	for (i = 0 ; i < len ; i++) {
		hexbuf[i * 2] = hexchars[(dp[i] >> 4) & 0xF];
		hexbuf[i * 2 + 1] = hexchars[dp[i] & 0xF];
	}
	hexbuf[i * 2] = '\0';
	bps_insert(schain, key, hexbuf);
}

static int template_timers_enabled;
static pthread_key_t template_timer_key;
static pthread_once_t template_timer_once = PTHREAD_ONCE_INIT;

static void
make_template_timer_key(void) {
	pthread_key_create(&template_timer_key, NULL);
}

void
template_timers_ctl(int enable) {
	pthread_once(&template_timer_once, make_template_timer_key);
	template_timers_enabled = enable;
}

static int
count_slashes(const char *s) {
	int i = 0;
	while (*s) {
		if (*s++ == '/')
			i++;
	}

	return i;
}

/*
 * Just like call_template, but with automagic lang and country lookup.
 */
int
call_template_auto(struct bpapi *api, const char *template_name) {
	return call_template_lang(api, template_name, vtree_get(&api->vchain, "tmpl", "lang", NULL), vtree_get(&api->vchain, "tmpl", "country", NULL));
}

int
call_template_lang(struct bpapi *api, const char* template_name, const char* lang, const char *country) {
	int len;
	char *buf;
	char *tmpl;
	char *tmpl_country;
	char *suffix;
	struct shadow_vtree tmpltree = {};
	struct bconf_node *tmplconf = NULL;
	struct bpapi ba = *api;
	int res = 0;

	if (!lang)
		return call_template(api, template_name);

	buf = strdupa(template_name);
	suffix = strrchr(buf, '.');
	if (!suffix) {
		return call_template(api, template_name);
	}

	*suffix++ = '\0';

	if (!vtree_haskey(&api->vchain, "tmpl", "lang", NULL)) {
		/* XXX I feel this is a bit layer violation, but no better way right now. */
		bconf_add_data(&tmplconf, "tmpl.lang", lang);
		if (country)
			bconf_add_data(&tmplconf, "tmpl.country", country);
		bconf_vtree(&tmpltree.vtree, tmplconf);
		shadow_vtree_init(&ba.vchain, &tmpltree, &api->vchain);
	}

	if (!bpapi_has_key(api, "b__lang"))
		bpapi_insert(api, "b__lang", lang);

	if (country != NULL) {
		ALLOCA_PRINTF(len, tmpl_country, "%s.%s_%s.%s", buf, lang, country, suffix);
		res = call_template(&ba, tmpl_country);
	}

	if (!res) {
		ALLOCA_PRINTF(len, tmpl, "%s.%s.%s", buf, lang, suffix);
		res = call_template(&ba, tmpl);
	}

	if (!res)
		res = call_template(&ba, template_name);

	bconf_free(&tmplconf);
	return res;
}

typedef void (*_template_entry)(struct bpapi*);

int
call_template(struct bpapi *api, const char* template_name) {
	char *dyn_path = getenv("TEMPLATES_PATH");

	/* First check for dynamic template */

	if (!template_name || !*template_name)
		return 0;

	if (dyn_path) {
		char *so_file;
		xasprintf(&so_file, "%s/%s.so", dyn_path, template_name);
		if (access(so_file, R_OK|X_OK) == 0) {
			void *so_handle;
			so_handle = dlopen(so_file, RTLD_LAZY|RTLD_GLOBAL);
			if (so_handle) {
				_template_entry f;
				char *fname;
				const char *tmp = template_name;
				char *ftmp;

				fname = xmalloc(9 + strlen(template_name) + count_slashes(template_name) + 1);
				strcpy(fname, "template_");
				ftmp = fname + 9;

				while (*tmp) {
					if (*tmp == '/') {
						*ftmp = '_';
						*(ftmp + 1) = '_';
						ftmp++;
					} else if (*tmp == '.') {
						*ftmp = '_';
					} else {
						*ftmp = *tmp;
					}

					ftmp++;
					tmp++;
				}
				*ftmp = '\0';

				f = (_template_entry)dlsym(so_handle, fname);

				if (f) {
					struct timer_instance *pti, *ti;
					if (template_timers_enabled) {
						pti = pthread_getspecific(template_timer_key);
						ti = timer_start(pti, template_name);
						pthread_setspecific(template_timer_key, ti);
						(*f)(api);
						pthread_setspecific(template_timer_key, pti);
						timer_end(ti, NULL);
					} else {
						(*f)(api);
					}
					dlclose(so_handle);
					free(fname);
					free(so_file);
					return 1;
				} else
					syslog(LOG_ERR, "dlsym(%s) failed: %s", fname, dlerror());
				dlclose(so_handle);
				free(fname);
			} else
				syslog(LOG_ERR, "dlopen(%s) failed: %s", so_file, dlerror());
		}
		free(so_file);
	}

#ifndef TPAPI_NO_TEMPLATES
	static int numsorted = 0;

	if (!sorted_templates) {
		struct templ **data;
		LINKER_SET_DECLARE(tmpl, struct templ);

		numsorted = LINKER_SET_END(tmpl) - LINKER_SET_START(tmpl);
		data = xmalloc(numsorted * sizeof (struct templ*));
		memcpy(data, LINKER_SET_START(tmpl), numsorted * sizeof (struct templ*));
#define cmp(a, b) (strcmp((*a)->name, (*b)->name) < 0)
		QSORT(struct templ*, data, numsorted, cmp);
#undef cmp
		if (atomic_cas_ptr(&sorted_templates, NULL, data))
			free(data);
	}

	struct templ **start = sorted_templates;
	int num_tmpl = numsorted;
	const struct templ *curr;

	while (num_tmpl > 0) {
		int even = ~num_tmpl & 1;
		int res;

		num_tmpl /= 2;
		curr = *(start + num_tmpl);

		res = strcmp(curr->name, template_name);
		if (res == 0) {
			struct timer_instance *pti, *ti;
			if (template_timers_enabled) {
				pti = pthread_getspecific(template_timer_key);
				ti = timer_start(pti, template_name);
				pthread_setspecific(template_timer_key, ti);
				curr->tmpl(api);
				pthread_setspecific(template_timer_key, pti);
				timer_end(ti, NULL);
			} else {
				curr->tmpl(api);
			}
			return 1;
		}
		if (res > 0)
			continue;
		start += num_tmpl + 1;
		num_tmpl -= even;
	}
#endif

	return 0;
}

void
flush_template_cache(const char *name) {
#ifndef TPAPI_NO_TEMPLATES
	struct templ * const *t;

	LINKER_SET_DECLARE(tmpl, struct templ);

	LINKER_SET_FOREACH(t, tmpl) {
                if (name) {
                        if (!strcmp(name, (*t)->name)) {
                                if ((*t)->clear_cache)
                                        (*t)->clear_cache();
                                break;
                        }
                }
		else if ((*t)->clear_cache)
			(*t)->clear_cache();
	}
#endif
}

void
flush_template_list(void) {
	free(sorted_templates);
	sorted_templates = NULL;
}

void
bpapi_free(struct bpapi *api) {
	bpapi_output_free(api);
	bpapi_simplevars_free(api);
	bpapi_vtree_free(api);
}

