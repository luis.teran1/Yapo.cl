#include <unicode/ucnv.h>
#include "bpapi.h"
#include "filter_state.h"
#include "filter_state_ucnv.h"
#include "logging.h"

static void
fs_ucnv_free(void *v) {
	ucnv_close(v);
}

void
fs_ucnv_utf8(struct bpapi *ba, const char *key, struct filter_state_data *fsd, void *v) {
	UErrorCode err = 0;
	UConverter *ret;

	ret = ucnv_open("UTF-8", &err);
	if (U_FAILURE(err)) {
		log_printf(LOG_CRIT, "Failed to initialize utf8_cnv: %s", u_errorName(err)); 
		return;
	}
	fsd->value = ret;
	fsd->type = FS_THREAD;
	fsd->freefn = fs_ucnv_free;
}

void
fs_ucnv_dst(struct bpapi *ba, const char *key, struct filter_state_data *fsd, void *v) {
	UErrorCode err = 0;
	const char *enc;
	UConverter *ret;

	enc = vtree_get(&ba->vchain, "common", "charset", NULL);
	if (!enc)
		enc = "ISO-8859-1";
	ret = ucnv_open(enc, &err);
	if (U_FAILURE(err)) {
		log_printf(LOG_CRIT, "Failed to initialize ucnv for (%s): %s", enc, u_errorName(err)); 
		return;
	}
	fsd->value = ret;
	fsd->type = FS_THREAD;
	fsd->freefn = fs_ucnv_free;
}
