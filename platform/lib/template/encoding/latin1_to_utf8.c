#include <bpapi.h>
#include <ctemplates.h>
#include <unicode.h>

static int
latin1_to_utf8_outstring_raw(struct bpapi_output_chain *ochain, const char *str,
    size_t len)
{
	char dst[2048];
	size_t off, dlen;

	for (off = 0; off < len; ) {
		off += latin1_to_utf8_buf(&dlen, str + off, len - off,
		    dst, sizeof(dst));
		bpo_outstring_raw(ochain->next, dst, dlen);
	}

	return 0;
}

static int
init_latin1_to_utf8(const struct bpfilter *filter, struct bpapi *api, int argc,
    const struct tpvar *argv[])
{
	return 0;
}

static const struct bpapi_output latin1_to_utf8_output = {
	self_outstring_fmt,
	latin1_to_utf8_outstring_raw,
	NULL,
	NULL
};

ADD_OUTPUT_FILTER(latin1_to_utf8, 0);
