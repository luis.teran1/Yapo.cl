#include <bpapi.h>
#include <ctemplates.h>
#include <unicode.h>

#include <stdio.h>

static const struct tpvar *
tmpl_utf8_to_latin1(struct bpapi *api, struct tpvar *dst, enum bpcacheable *cc, int argc, const struct tpvar **argv) {
	const char *src;
	char *buf;
	char *fa0;

	src = tpvar_str(argv[0], &fa0);
	buf = utf8_to_latin1(src);
	free(fa0);

	return TPV_SET(dst, TPV_DYNSTR(buf));
}

ADD_TEMPLATE_FUNCTION_WITH_NAME(tmpl_utf8_to_latin1, utf8_to_latin1);
