#include "bpapi.h"
#include "util.h"
#include "ctemplates.h"
#include "logging.h"
#include <libpq-fe.h>
#include "hash.h"
#include "ctemplates_internal.h"

static const char *
pgsql_get_element(struct bpapi_simplevars_chain *schain, const char *key, int pos) {
	struct ctemplates_simplevars_data *d = schain->data;
	struct hash_entry *e;
	const char *orig;

	if ((e = hash_lookup(schain, key)) == NULL)
		return NULL;
	orig = e->valuelist[pos % e->len];

	if (!orig || !*orig || !d->conn)
		return orig;

	int len = strlen(orig);
	char *esc_value = xmalloc(len * 2 + 1);
	PQescapeStringConn(d->conn, esc_value, orig, len, NULL);

	if (e->tmpval)
		free(e->tmpval);
	e->tmpval = esc_value;
	return esc_value;
}

static void
pgsql_loop_cleanup(struct bpapi_loop_var *loop) {
	int i;

	for (i = 0 ; i < loop->len ; i++)
		free((char*)loop->l.list[i]);
	free(loop->l.list);
}

static void
pgsql_fetch_loop(struct bpapi_simplevars_chain *schain, const char *key, struct bpapi_loop_var *loop) {
	struct ctemplates_simplevars_data *d = schain->data;
	struct hash_entry *e;

	if (!d->conn)
		return hash_fetch_loop(schain, key, loop);

	if ((e = hash_lookup(schain, key)) != NULL) {
		int i;

		loop->len = e->len;
		loop->l.list = xmalloc(e->len * sizeof (*loop->l.list));
		for (i = 0 ; i < e->len ; i++) {
			int l = strlen(e->valuelist[i]);

			loop->l.list[i] = xmalloc(l * 2 + 1);
			PQescapeStringConn(d->conn, (char*)loop->l.list[i], e->valuelist[i], l, NULL);
		}
		loop->cleanup = pgsql_loop_cleanup;
	} else {
		loop->len = 0;
		loop->l.list = NULL;
		loop->cleanup = NULL;
	}
}

static void
pgsql_init_var(struct bpapi_simplevars_chain *schain, const char *key, struct template_var *var) {
	struct ctemplates_simplevars_data *d = schain->data;

	if (d->conn)
		return;

	hash_init_var(schain, key, var);
}


const struct bpapi_simplevars __pgsql_api_simplevars = {
	hash_has_key,
	hash_length,
	pgsql_get_element,
	pgsql_fetch_loop,
	keyglob_len,
	hash_fetch_glob_loop,
	hash_insert,
	/*pgsql_insert_list*/NULL,
	insert_maxsize,
	hash_delete,
	hash_simplevars_free,
	pgsql_init_var
};

void
pgsql_simplevars_init(struct bpapi_simplevars_chain *schain) {
	struct ctemplates_simplevars_data *d = xmalloc(sizeof(*d));

	d->nkeys = 0;
	d->keys = NULL;
	d->conn = NULL;
	schain->data = d;
	schain->fun = &__pgsql_api_simplevars;
	schain->cache_gen = 1;
}

void
pgsql_simplevars_setconn(struct bpapi_simplevars_chain *schain, void *conn) {
	struct ctemplates_simplevars_data *d = schain->data;

	d->conn = conn;
}

