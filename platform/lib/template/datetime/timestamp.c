#include <time.h>

#include <bpapi.h>
#include <ctemplates.h>


static const struct tpvar *
timestamp(struct bpapi *api, struct tpvar *dst, enum bpcacheable *cc, int argc, const struct tpvar **argv) {

	if (argc != 0)
		return TPV_SET(dst, TPV_NULL);

	*cc = BPCACHE_CANT;
	time_t now = time(NULL);
	return TPV_SET(dst, TPV_INT(now));
}
ADD_TEMPLATE_FUNCTION(timestamp);
