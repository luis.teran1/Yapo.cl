#include <bpapi.h>
#include <ctemplates.h>


static const struct tpvar *
define_month_days(struct bpapi *api, struct tpvar *dst, enum bpcacheable *cc, int argc, const struct tpvar **argv) {
	const char *year_month;
	const char *list_name;
	struct date_rec dr = {0};
	const char *dp;
	int month;

	if (argc != 2)
		return TPV_SET(dst, TPV_NULL);

	TPVAR_STRTMP(year_month, argv[0]);
	TPVAR_STRTMP(list_name, argv[1]);

	dp = strchr (year_month, '-');
	if (!dp)
		return TPV_SET(dst, TPV_NULL);

	dr.year = atoi(year_month);
	month = dr.month = atoi(dp + 1);
	dr.day = 1;

	while (dr.month == month) {
		bpapi_set_printf(api, list_name, "%04d-%02d-%02d", dr.year, dr.month, dr.day);
		date_set_next_day(&dr);
	}
	*cc = BPCACHE_CANT;
	return TPV_SET(dst, TPV_NULL);
}
ADD_TEMPLATE_FUNCTION(define_month_days);
