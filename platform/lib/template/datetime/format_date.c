#include <time.h>

#include <bpapi.h>
#include <ctemplates.h>


static const struct tpvar *
format_date(struct bpapi *api, struct tpvar *dst, enum bpcacheable *cc, int argc, const struct tpvar **argv) {
	char *date;
	time_t t;
	struct tm tm = {0};
	const char *fmt;

	if (argc < 1 || argc > 2)
		return TPV_SET(dst, TPV_NULL);

	date = xmalloc(64);

	TPVAR_STRTMP(fmt, argv[0]);

	if (argc == 1) {
		t = time(NULL);
		strftime(date, 64, fmt, localtime_r(&t, &tm));
		*cc = BPCACHE_CANT;
	} else {
		const char *ts;

		TPVAR_STRTMP(ts, argv[1]);

		strptime(ts, "%Y-%m-%d %H:%M:%S", &tm);
/*
  	Next statement is comented out because was breaking date formatting :(
	Specifically, sitemap was broken as no timezone information was gathered
		tm.tm_isdst = -1;
*/
		strftime(date, 64, fmt, &tm);
	}

	return TPV_SET(dst, TPV_DYNSTR(date));
}

ADD_TEMPLATE_FUNCTION(format_date);
