#include <time.h>

#include <bpapi.h>
#include <ctemplates.h>


static const struct tpvar *
format_ts(struct bpapi *api, struct tpvar *dst, enum bpcacheable *cc, int argc, const struct tpvar **argv) {
	char *date;
	time_t t;
	struct tm tm = {0};
	const char *fmt;

	if (argc != 2)
		return TPV_SET(dst, TPV_NULL);

	date = xmalloc(64);

	t = tpvar_int(argv[1]);
	localtime_r(&t, &tm);

	TPVAR_STRTMP(fmt, argv[0]);
	strftime(date, 64, fmt, &tm);

	return TPV_SET(dst, TPV_DYNSTR(date));
}

ADD_TEMPLATE_FUNCTION(format_ts);
