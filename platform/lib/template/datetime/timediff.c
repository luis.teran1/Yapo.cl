#include <time.h>

#include <bpapi.h>
#include <ctemplates.h>


static const struct tpvar *
timediff(struct bpapi *api, struct tpvar *dst, enum bpcacheable *cc, int argc, const struct tpvar **argv) {
	time_t t, t_now;
	struct tm tm = {0};
	int diff;

	if (argc == 1) {
		t = tpvar_int(argv[0]);
	} else if (argc == 2) {
		const char *ts;
		const char *fmt;

		TPVAR_STRTMP(ts, argv[0]);
		TPVAR_STRTMP(fmt, argv[1]);

		tm.tm_isdst = -1;
		if (strptime(ts, fmt, &tm) == NULL)
			return TPV_SET(dst, TPV_NULL);
		t = mktime(&tm);
	} else {
		return TPV_SET(dst, TPV_NULL);
	}

	t_now = time(0);
	diff = (int)difftime(t_now, t);

	*cc = BPCACHE_CANT;
	return TPV_SET(dst, TPV_INT(diff));
}

ADD_TEMPLATE_FUNCTION(timediff);
