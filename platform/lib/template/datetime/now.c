#include <time.h>

#include <bpapi.h>
#include <ctemplates.h>


static const struct tpvar *
now(struct bpapi *api, struct tpvar *dst, enum bpcacheable *cc, int argc, const struct tpvar **argv) {
	char *date;
	time_t t;
	struct tm tm = {0};

	date = xmalloc(64);
	t = time(0);
	localtime_r(&t, &tm);
	strftime(date, 64, "%Y-%m-%d %H:%M:%S", &tm);

	return TPV_SET(dst, TPV_DYNSTR(date));
}

ADD_TEMPLATE_FUNCTION(now);
