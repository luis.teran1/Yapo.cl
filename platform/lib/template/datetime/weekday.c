#include <stdio.h>
#include <time.h>

#include <bpapi.h>
#include <ctemplates.h>


static const struct tpvar *
weekday(struct bpapi *api, struct tpvar *dst, enum bpcacheable *cc, int argc, const struct tpvar **argv) {
	struct date_rec d;
	const int day_zero = 5; /* 1999-12-31 was a friday. */
	int wday;
	int yoff;
	int moff;
	int doff;
	const char *date;

	if (argc != 1)
		return TPV_SET(dst, TPV_NULL);

	TPVAR_STRTMP(date, argv[0]);

	/* Extract year, month, day. */
	if (sscanf(date, "%d-%d-%d", &d.year, &d.month, &d.day) != 3)
		return TPV_SET(dst, TPV_NULL);

	yoff = d.year - 2000;

	moff = 0;
	switch (d.month) {
	case 12:
	case 11:
	case 10:
	case 9:
		moff += (d.month - 8) * 30 + (d.month - 7) / 2;
		d.month = 8;
		/* Fallthrough */
	case 8:
	case 7:
	case 6:
	case 5:
	case 4:
		moff += (d.month - 3) * 30 + (d.month - 2) / 2;
		/* Fallthrough */
	case 3:
		moff += 28 + (d.year % 4 == 0 && (d.year % 100 != 0 || d.year % 400 == 0) ? 1 : 0);
		/* Fallthrough */
	case 2:
		moff += 31;
		break;
	}

	if (yoff > 0) {
		/* The leap day isn't passed until the year after the leap year. */
		doff = yoff * 365 + (yoff + 3) / 4 - (yoff - 1) / 100 + (yoff - 1) / 400 + moff + d.day;
	} else {
		/* The leap day is passed on the leap year. */
		doff = yoff * 365 + yoff / 4 - yoff / 100 + yoff / 400 + moff + d.day;
	}
	wday = (doff + day_zero) % 7;
	while (wday <= 0)
		wday += 7;

	return TPV_SET(dst, TPV_INT(wday));
}
ADD_TEMPLATE_FUNCTION(weekday);
