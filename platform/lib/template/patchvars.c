
#include "patchvars.h"
#include "ctemplates.h"

int
patch_has_key(struct bpapi_simplevars_chain *schain, const char *key) {
	struct patch_data *d = schain->data;

	if (d->prefix_len == 0) {
		if (bps_has_key(&d->vars, key))
			return 1;
		return bps_has_key(schain->next, key);
	}

	if (strncmp(key, d->prefix, d->prefix_len) != 0)
		return bps_has_key(schain->next, key);

	return bps_has_key(&d->vars, key);
}

int
patch_key_length(struct bpapi_simplevars_chain *schain, const char *key) {
	struct patch_data *d = schain->data;

	if (d->prefix_len == 0) {
		int len = bps_length(&d->vars, key);

		if (len > 0)
			return len;

		return bps_length(schain->next, key);
	}

	if (strncmp(key, d->prefix, d->prefix_len) != 0)
		return bps_length(schain->next, key);

	return bps_length(&d->vars, key);
}

const char *
patch_get_element(struct bpapi_simplevars_chain *schain, const char *key, int pos) {
	struct patch_data *d = schain->data;

	if (d->prefix_len == 0) {
		const char *res = bps_get_element(&d->vars, key, pos);

		if (res)
			return res;
		return bps_get_element(schain->next, key, pos);
	}

	if (strncmp(key, d->prefix, d->prefix_len) != 0)
		return bps_get_element(schain->next, key, pos);

	return bps_get_element(&d->vars, key, pos);
}

void
patch_fetch_loop(struct bpapi_simplevars_chain *schain, const char *key, struct bpapi_loop_var *loop) {
	struct patch_data *d = schain->data;

	if (d->prefix_len == 0) {
		bps_fetch_loop(&d->vars, key, loop);
		if (loop->len)
			return;

		bps_fetch_loop(schain->next, key, loop);
		return;
	}

	if (strncmp(key, d->prefix, d->prefix_len) != 0)
		bps_fetch_loop(schain->next, key, loop);
	else
		bps_fetch_loop(&d->vars, key, loop);
}


int
patch_keyglob_len(struct bpapi_simplevars_chain *schain, const char *key) {
	struct patch_data *d = schain->data;

	if (d->prefix_len == 0) {
		int us = bps_keyglob_len(&d->vars, key);
		int them = bps_keyglob_len(schain->next, key);

		if (us == 0 || them == 0)
			return us + them;
		
		/* Sigh, need to do a union. Easiest way is to simply fetch. */
		struct bpapi_loop_var loop;

		bps_fetch_glob_loop(schain, key, &loop);
		if (loop.cleanup)
			loop.cleanup(&loop);
		return loop.len;
	}

	if (strncmp(key, d->prefix, d->prefix_len) == 0)
		return bps_keyglob_len(&d->vars, key);

	unsigned int l = strlen(key) - 1;

	if (l <= d->prefix_len && strncmp(key, d->prefix, l) == 0) {
		char *pglob = alloca(d->prefix_len + 2);
		memcpy(pglob, d->prefix, d->prefix_len);
		pglob[d->prefix_len] = '*';
		pglob[d->prefix_len + 1] = '\0';

		return bps_keyglob_len(schain->next, key) + bps_keyglob_len(&d->vars, key)
			- bps_keyglob_len(schain->next, pglob);
	}

	return bps_keyglob_len(schain->next, key);
}

static void
patch_free_loop(struct bpapi_loop_var *loop) {
	int i;

	for (i = 0 ; i < loop->len ; i++)
		free((char*)loop->l.list[i]);
	free(loop->l.list);
}

void
patch_fetch_glob_loop(struct bpapi_simplevars_chain *schain, const char *key, struct bpapi_loop_var *loop) {
	struct patch_data *d = schain->data;
	struct bpapi_loop_var old_loop;
	struct bpapi_loop_var new_loop;

	if (d->prefix_len > 0 && strncmp(key, d->prefix, d->prefix_len) == 0) {
		bps_fetch_glob_loop(&d->vars, key, loop);
		return;
	}

	bps_fetch_glob_loop(schain->next, key, &old_loop);
	bps_fetch_glob_loop(&d->vars, key, &new_loop);

	if (!old_loop.len)
		*loop = new_loop;
	else {
		int i, j, k;

		loop->l.list = xmalloc(sizeof (*loop->l.list) * (old_loop.len + new_loop.len));
		loop->cleanup = patch_free_loop;
		i = j = k = 0;
		while (i < old_loop.len || j < new_loop.len) {
			int c;
		       
			if (i < old_loop.len && d->prefix_len && strncmp(old_loop.l.list[i], d->prefix, d->prefix_len) == 0) {
				i++;
				continue;
			}

			if (i >= old_loop.len)
				c = 1;
			else if (j >= new_loop.len)
				c = -1;
			else
				c = strcmp(old_loop.l.list[i], new_loop.l.list[j]);

			if (c <= 0) {
				loop->l.list[k++] = xstrdup(old_loop.l.list[i++]);
				if (c == 0)
					j++;
			} else {
				loop->l.list[k++] = xstrdup(new_loop.l.list[j++]);
			}
		}
		loop->len = k;
		if (new_loop.cleanup)
			new_loop.cleanup(&new_loop);
	}
	if (old_loop.cleanup)
		old_loop.cleanup(&old_loop);
}

void
patch_init_var(struct bpapi_simplevars_chain *schain, const char *key, struct template_var *cache) {
	struct patch_data *d = schain->data;

	if (d->prefix_len == 0) {
		bps_init_var(&d->vars, key, cache);
		if (!cache->entry)
			bps_init_var(schain->next, key, cache);
		return;
	}

	if (strncmp(key, d->prefix, d->prefix_len) != 0) {
		bps_init_var(schain->next, key, cache);
	} else
		bps_init_var(&d->vars, key, cache);
}

void
fini_patch(struct bpapi_simplevars_chain *schain) {
	struct patch_data *d = schain->data;

	if (d->fini_func)
		d->fini_func(d);
	bps_free(&d->vars);
}

void
parent_insert(struct bpapi_simplevars_chain *schain, const char *key, int klen, const char *val, int vlen) {
	schain->cache_gen++;
	return bps_insert_buf(schain->next, key, klen, val, vlen);
}

void
parent_insert_list(struct bpapi_simplevars_chain *schain, const char *a, const char **b, int c) {
	schain->cache_gen++;
	return bps_insert_list(schain->next, a, b, c);
}

void
parent_insert_maxsize(struct bpapi_simplevars_chain *schain, const char *a, const char *b, int maxsize) {
	schain->cache_gen++;
	return bps_insert_maxsize(schain->next, a, b, maxsize);
}

void
parent_remove(struct bpapi_simplevars_chain *schain, const char *a) {
	schain->cache_gen++;
	if (bps_has_remove(schain->next))
		return bps_remove(schain->next, a);
}

const struct bpapi_simplevars patch_simplevars = {
	patch_has_key,
	patch_key_length,
	patch_get_element,
	patch_fetch_loop,
	patch_keyglob_len,
	patch_fetch_glob_loop,
	parent_insert,
	parent_insert_list,
	parent_insert_maxsize,
	parent_remove,
	fini_patch,
	patch_init_var,
};

static void
patch_with_scope_insert(struct bpapi_simplevars_chain *schain, const char *key, int klen, const char *val, int vlen) {
	struct patch_data *d = schain->data;
	schain->cache_gen++;

	return bps_insert_buf(&d->vars, key, klen, val, vlen);
}

static void
patch_with_scope_insert_list(struct bpapi_simplevars_chain *schain, const char *a, const char **b, int c) {
	struct patch_data *d = schain->data;
	schain->cache_gen++;

	return bps_insert_list(&d->vars, a, b, c);
}

static void
patch_with_scope_insert_maxsize(struct bpapi_simplevars_chain *schain, const char *a, const char *b, int maxsize) {
	struct patch_data *d = schain->data;
	schain->cache_gen++;

	return bps_insert_maxsize(&d->vars, a, b, maxsize);
}

static void
patch_with_scope_remove(struct bpapi_simplevars_chain *schain, const char *key) {
	struct patch_data *d = schain->data;

	schain->cache_gen++;
	if (bps_has_key(&d->vars, key))
		bps_remove(&d->vars, key);
}

const struct bpapi_simplevars patch_with_scope_simplevars = {
	patch_has_key,
	patch_key_length,
	patch_get_element,
	patch_fetch_loop,
	patch_keyglob_len,
	patch_fetch_glob_loop,
	patch_with_scope_insert,
	patch_with_scope_insert_list,
	patch_with_scope_insert_maxsize,
	patch_with_scope_remove,
	fini_patch,
	patch_init_var,
};


void
patchvars_init(struct patch_data *data, const char *tag, const char *prefix, int prefix_len, void (*fini_func)(struct patch_data *data), void *user_data)
{
	hash_simplevars_init(&data->vars);

	data->tag = tag;

	data->prefix = prefix;
	data->prefix_len = prefix_len;

	data->fini_func = fini_func;
	data->user_data = user_data;

	data->row = -1;
}

void
patchvars_clear(struct patch_data *data) {
	bps_free(&data->vars);
	hash_simplevars_init(&data->vars);
}

int
loop_patch(struct bpapi *api) {
	struct patch_data *d = BPAPI_SIMPLEVARS_DATA(api);

	if (++d->row < d->rows)
		return 1;
	d->row = -1; /* Reset */
	return 0;
}

int
loop_length_patch(struct bpapi *api) {
	struct patch_data *d = BPAPI_SIMPLEVARS_DATA(api);

	return d->rows;
}

const char *
patch_loop_get_element(struct bpapi_simplevars_chain *schain, const char *key, int pos) {
	struct patch_data *d = schain->data;

	if (d->prefix_len == 0) {
		const char *res = bps_get_element(&d->vars, key, pos + d->row);

		if (res)
			return res;
		return bps_get_element(schain->next, key, pos);
	}

	if (strncmp(key, d->prefix, d->prefix_len) != 0)
		return bps_get_element(schain->next, key, pos);

	return bps_get_element(&d->vars, key, pos + d->row);
}

const struct bpapi_simplevars patch_loop_simplevars = {
	patch_has_key,
	patch_key_length,
	patch_loop_get_element,
	patch_fetch_loop,
	patch_keyglob_len,
	patch_fetch_glob_loop,
	parent_insert,
	parent_insert_list,
	parent_insert_maxsize,
	parent_remove,
	fini_patch,
	patch_init_var,
};
