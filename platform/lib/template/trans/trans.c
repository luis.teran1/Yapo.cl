#include <bpapi.h>
#include <string.h>
#include <ctype.h>
#include <time.h>
#include <ctemplates.h>
#include <stdlib.h>
#include "transapi.h"
#include "util.h"
#include "patchvars.h"

#include "trans_filters.h"

static int
init_trans(const struct bpfilter *filter, struct bpapi *api, int argc, const struct tpvar **argv) {
	transaction_t *t;
	struct bpapi_vtree_chain sub = {0};
	int ret = 0;

	struct patch_data *pd = BPAPI_SIMPLEVARS_DATA(api);
	patchvars_init(pd, "trans", "tr_", 3, fini_trans, NULL);

	t = trans_init_vtree(10000, vtree_getnode(api->vchain.next, &sub, "common", "transaction", NULL));
	if (t) {
		ret = trans_common(filter, api, argc, argv, pd, t);
		trans_free(t);
	} else {
		bps_insert(&pd->vars, "tr_error", "ERROR_TRANS_INIT_FAILED");
	}
	return ret;
}

ADD_FILTER(trans, NULL, 0, &patch_simplevars, sizeof(struct patch_data), &shadow_vtree, sizeof (struct shadow_vtree));

