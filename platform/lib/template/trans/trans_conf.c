#include <bpapi.h>
#include <fd_pool.h>
#include <transapi.h>
#include <patchvars.h>

#include "trans_filters.h"


static int
init_trans_conf(const struct bpfilter *filter, struct bpapi *api, int argc, const struct tpvar **argv) {
	struct patch_data *pd = BPAPI_SIMPLEVARS_DATA(api);
	transaction_t *t = NULL;
	const struct tpvar **tpvars = NULL;
	struct bpapi_vtree_chain sub = {0}, *node;
	char *bconf_node = NULL;
	const char **array = NULL;
	char *p;
	int i,d;
	int ret = 0;

	patchvars_init(pd, "trans", "tr_", 3, fini_trans, NULL);

	if (argc < 4) {
		bps_insert(&pd->vars, "tr_error", "ERROR_TOO_FEW_ARGUMENTS");
		goto out;
	}

	int timeout = tpvar_int(argv[0]);
	/* Any invalid string will give you a 10s timeout */
	if (timeout == 0)
		timeout = 10000;
	/* Timeouts shorter than one second makes little sense, adjust. */
	if (timeout < 1000)
		timeout = 1000;

	if (argv[1]->type == TPV_NODE) {
		node = tpvar_node(argv[1]);
	} else {
		bconf_node = tpvar_strdup(argv[1]);

		/* Turn dot-delimiters into array of separate strings */
		d = count_chars(bconf_node, '.');
		array = xmalloc(sizeof(char *) * (d + 1));
		array[0] = bconf_node;
		for (p=bconf_node,i=1; *p != '\0' && i < d + 1; p++ ) {
			if ( *p == '.' ){
				*p = '\0';
				array[i++] = p+1;
			}
		}
		node = vtree_getnode_cachev(api->vchain.next, NULL, &sub, NULL, d+1, array);
	}

	t = trans_init_vtree(timeout, node);
	if (t) {
		ret = trans_common(filter, api, argc-2, argv+2, pd, t);
		trans_free(t);
	} else {
		bps_insert(&pd->vars, "tr_error", "ERROR_TRANS_INIT_FAILED");
	}
	vtree_free(&sub);

out:
	free(bconf_node);
	free(tpvars);
	free(array);
	return ret;
}

ADD_FILTER(trans_conf, NULL, 0, &patch_simplevars, sizeof(struct patch_data), &shadow_vtree, sizeof (struct shadow_vtree));

