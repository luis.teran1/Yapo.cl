#ifndef TRANS_FILTERS_H
#define TRANS_FILTERS_H


int trans_common(const struct bpfilter *filter, struct bpapi *api, int argc, const struct tpvar **argv, struct patch_data *pd, transaction_t *t);
void fini_trans(struct patch_data *data);

#endif
