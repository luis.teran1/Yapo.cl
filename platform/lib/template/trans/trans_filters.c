#include <stdio.h>
#include <bpapi.h>
#include <fd_pool.h>
#include <transapi.h>
#include <patchvars.h>
#include "bconf.h"

#include "trans_filters.h"

void
fini_trans(struct patch_data *data) {
	struct bconf_node *node = data->user_data;
	bconf_free(&node);
}

int
trans_common(const struct bpfilter *filter, struct bpapi *api, int argc, const struct tpvar **argv, struct patch_data *pd, transaction_t *t) {
	struct shadow_vtree *subtree = BPAPI_VTREE_DATA(api);
	struct bconf_node *root = NULL;
	const char *key, *value;
	int res;
	char *rkey, *rvalue;
	int i;
	const char *multi_pos;


	for (i = 0; i < argc; i += 2) {
		char *multi_key = NULL;

		TPVAR_STRTMP(key, argv[i]);

		if (!key || *key == '\0')
			break;

		if (i + 1 == argc) {
			bps_insert(&pd->vars, "tr_error", "ERROR_KEY_WITHOUT_VALUE");
			goto out;
		}

		if ((multi_pos = strstr(key, ":multi")))
			multi_key = xstrndup(key, multi_pos - key);

		TPVAR_STRTMP(value, argv[i + 1]);

		/* Do not send empty values to trans */
		if (value && strlen(value) > 0) {
			if (strpbrk(value, "\n\r")) {
				char *blob;

				xasprintf(&blob, "%zd:%s\n%s", strlen(value), key, value);
				trans_add_pair(t, "blob", blob, strlen(blob));
				free(blob);
			} else if (multi_key) {
				const char *next;
				do {
					next = strchrnul(value, ',');

					trans_add_pair(t, multi_key, value, next - value);
					value = next + 1;
				} while (*next);
			} else {
				trans_add_pair(t, key, value, strlen(value));
			}
		}

		free(multi_key);
	}

	res = trans_commit(t);

	if (res) {
		void (*error_cb)(transaction_t *t, int res);

		bconf_vtree(&subtree->vtree, NULL);
		bps_set_int(&pd->vars, "tr_error", res);

		if ((error_cb = filter_state(api, "trans_errorcb", NULL, NULL)))
			error_cb(t, res);
	} else {
		char buf[1024];

		trans_iter_reset(t);
		while (trans_iter_next(t, &rkey, &rvalue)) {
			snprintf(buf, sizeof(buf), "tr_%s", rkey);
			bps_insert(&pd->vars, buf, rvalue);
			bconf_add_data(&root, rkey, rvalue);
		}
		bconf_vtree(&subtree->vtree, root);
		pd->user_data = root;
	}

out:
	return 0;
}



