#include <bpapi.h>
#include <ctemplates.h>


static const struct tpvar*
strcount(struct bpapi *api, struct tpvar *dst, enum bpcacheable *cc, int argc, const struct tpvar **argv) {
	const char *pos;
	const char *haystack;
	const char *needle;
	char *fh, *fn;
	int count = 0;
	int needle_len = 0;

	if (argc != 2)
		return TPV_SET(dst, TPV_NULL);

	if ((haystack = tpvar_str(argv[0], &fh)) == NULL)
		return TPV_SET(dst, TPV_NULL);

	needle = tpvar_str(argv[1], &fn);
	needle_len = strlen(needle);

	/* Avoid endless loop if needle is the empty string */
	if (needle_len == 0)
		return TPV_SET(dst, TPV_INT(0));

	pos = haystack;
	while ((pos = strstr(pos, needle)) != NULL) {
		count++;
		pos += needle_len;
	}

	free(fh);
	free(fn);

	(void)TPV_SET(dst, TPV_INT(count));
	return dst;
}

ADD_TEMPLATE_FUNCTION(strcount);
