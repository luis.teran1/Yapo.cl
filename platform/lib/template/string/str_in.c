#include <bpapi.h>
#include <ctemplates.h>


/*
 * str_in(string, value, ...): returns "1" or "0" depending on whether the
 * first string equals any of the others
 */
static const struct tpvar*
str_in(struct bpapi *api, struct tpvar *dst, enum bpcacheable *cc, int argc, const struct tpvar **argv) {
	int i, any = 0;
	const char *needle;
	char *fn;

	needle = tpvar_str(argv[0], &fn);
	
	for (i = 1; !any && i < argc; ++i) {
		char *fa;
		const char *a = tpvar_str(argv[i], &fa);

		if (strcmp(needle, a) == 0)
			any = 1;

		free(fa);
	}

	free(fn);

	return TPV_SET(dst, TPV_INT(any));
}

ADD_TEMPLATE_FUNCTION(str_in);
