#include <bpapi.h>
#include <ctemplates.h>


static const struct tpvar*
tmplstrcmp(struct bpapi *api, struct tpvar *dst, enum bpcacheable *cc, int argc, const struct tpvar **argv) {
	const char *a, *b;
	char *fa, *fb;
	int res;

	if (argc != 2)
		return TPV_SET(dst, TPV_NULL);

	a = tpvar_str(argv[0], &fa);
	b = tpvar_str(argv[1], &fb);

	res = strcmp(a, b);

	free(fb);
	free(fa);

	return TPV_SET(dst, TPV_INT(res));
}

ADD_TEMPLATE_FUNCTION_WITH_NAME(tmplstrcmp, strcmp);
