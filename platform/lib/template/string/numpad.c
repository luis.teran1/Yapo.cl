#include <bpapi.h>
#include <ctemplates.h>


static const struct tpvar *
numpad(struct bpapi *api, struct tpvar *dst, enum bpcacheable *cc, int argc, const struct tpvar **argv) {
	char *res;

	if (argc < 2)
		return TPV_SET(dst, TPV_NULL);

	xasprintf(&res, "%0*d", tpvar_int(argv[1]), tpvar_int(argv[0]));
	return TPV_SET(dst, TPV_DYNSTR(res));
}

ADD_TEMPLATE_FUNCTION(numpad);
