#include <bpapi.h>
#include <ctemplates.h>


static const struct tpvar *
replace(struct bpapi *api, struct tpvar *dst, enum bpcacheable *cc, int argc, const struct tpvar **argv) {
	const char *from;
	const char *to;
	const char *str;
	const char *next;
	size_t fromlen;
	size_t tolen;
	struct buf_string res = {0};

	if (argc < 3)
		return TPV_SET(dst, TPV_NULL);

	TPVAR_STRTMP(from, argv[0]);
	fromlen = strlen(from);
	TPVAR_STRTMP(to, argv[1]);
	tolen = strlen(to);
	TPVAR_STRTMP(str, argv[2]);

	if (fromlen < 1)
		return TPV_SET(dst, TPV_NULL);

	while ((next = strstr(str, from))) {
		bufwrite(&res.buf, &res.len, &res.pos, str, next - str);
		bufwrite(&res.buf, &res.len, &res.pos, to, tolen);
		str = next + fromlen;
	}
	bufwrite(&res.buf, &res.len, &res.pos, str, strlen(str));
	return TPV_SET(dst, TPV_DYNSTR(res.buf));
}

ADD_TEMPLATE_FUNCTION(replace);
