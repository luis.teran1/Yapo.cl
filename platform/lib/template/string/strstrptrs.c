#include <bpapi.h>
#include <ctemplates.h>


static const struct tpvar*
tmplstrstrptrs(struct bpapi *api, struct tpvar *dst, enum bpcacheable *cc, int argc, const struct tpvar **argv) {
	const char *pos;
	const char *haystack;
	const char *needle;
	const char *delim;

	if (argc != 3)
		return TPV_SET(dst, TPV_NULL);

	TPVAR_STRTMP(haystack, argv[0]);
	TPVAR_STRTMP(needle, argv[1]);
	TPVAR_STRTMP(delim, argv[2]);

	if ((pos = strstrptrs(haystack, needle, NULL, delim)))
		(void)TPV_SET(dst, TPV_INT(pos - haystack));
	else
		(void)TPV_SET(dst, TPV_NULL);

	return dst;
}

ADD_TEMPLATE_FUNCTION_WITH_NAME(tmplstrstrptrs, strstrptrs);
