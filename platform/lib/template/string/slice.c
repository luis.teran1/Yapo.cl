#include <bpapi.h>
#include <ctemplates.h>


static const struct tpvar *
slice(struct bpapi *api, struct tpvar *dst, enum bpcacheable *cc, int argc, const struct tpvar **argv) {
	int offset1;
	int offset2;
	int sl;
	char *res;
	const char *str;
	
	if (argc != 3)
		return TPV_SET(dst, TPV_NULL);

	TPVAR_STRTMP(str, argv[0]);
	offset1 = tpvar_int(argv[1]);
	offset2 = tpvar_int(argv[2]);
	sl = strlen(str);
	if ((offset1 < 0) || (offset1 > sl))
		return TPV_SET(dst, TPV_NULL);

	/* Support negative offset2 to remove fixed length from end of string */
	if (offset2 == 0)
		offset2 = sl;
	if (offset2 < 0)
		offset2 = sl+offset2; /* subtraction */
	if (offset2 < 0)
		offset2 = 0;
	if (offset2 > sl)
		offset2 = sl;
	if (offset1 >= offset2)
		return TPV_SET(dst, TPV_NULL);
	
	xasprintf(&res, "%.*s", offset2 - offset1, str + offset1);
	return TPV_SET(dst, TPV_DYNSTR(res));
}

ADD_TEMPLATE_FUNCTION(slice);
