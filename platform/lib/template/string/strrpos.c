#include <bpapi.h>
#include <ctemplates.h>

#include <unicode/utf8.h>
#include <unicode/uchar.h>
#include <unicode.h>

static char *
strrstr(const char *str, const char *pat) {
	size_t len, patlen;
	const char *p;

	len = strlen(str);
	patlen = strlen(pat);

	if (patlen > len)
		return NULL;
	for (p = str + (len - patlen); p > str; --p)
		if (*p == *pat && strncmp(p, pat, patlen) == 0)
			return (char *) p;
	return NULL;
}

static const struct tpvar *
strrpos(struct bpapi *api, struct tpvar *dst, enum bpcacheable *cc, int argc, const struct tpvar **argv) {
	const char *pos;
	const char *haystack;
	const char *needle;
	char *fh, *fn;

	if (argc != 2)
		return TPV_SET(dst, TPV_NULL);

	haystack = tpvar_str(argv[0], &fh);
	needle = tpvar_str(argv[1], &fn);

	if ((pos = strrstr(haystack, needle)))
		(void)TPV_SET(dst, TPV_INT(pos - haystack));
	else
		(void)TPV_SET(dst, TPV_NULL);

	free(fh);
	free(fn);

	return dst;
}

ADD_TEMPLATE_FUNCTION(strrpos);


static const struct tpvar *
strrpos_utf8(struct bpapi *api, struct tpvar *dst, enum bpcacheable *cc, int argc, const struct tpvar **argv) {
	const char *pos;
	const char *haystack;
	const char *needle;
	char *fh, *fn;
	int i, j, len;
	UChar32 ch;

	if (argc != 2)
		return TPV_SET(dst, TPV_NULL);

	haystack = tpvar_str(argv[0], &fh);
	needle = tpvar_str(argv[1], &fn);

	if ((pos = strrstr(haystack, needle))) {
		len = strlen(haystack);
		for (i = 0, j = 0; i+haystack < pos; j++) {
			U8_NEXT(haystack, i, len, ch);
		}
		(void)TPV_SET(dst, TPV_INT(j));
	} else
		(void)TPV_SET(dst, TPV_NULL);

	free(fh);
	free(fn);

	return dst;
}

ADD_TEMPLATE_FUNCTION(strrpos_utf8);
