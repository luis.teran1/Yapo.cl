#include <bpapi.h>
#include <ctemplates.h>


static const struct tpvar *
str_len(struct bpapi *api, struct tpvar *dst, enum bpcacheable *cc, int argc, const struct tpvar **argv) {
        int len = 0;
        int i;

        for (i = 0; i < argc ; i++) {
		char *fa;
		const char *a = tpvar_str(argv[i], &fa);

		len += strlen(a);
		free(fa);
	}
        
	return TPV_SET(dst, TPV_INT(len));
}

ADD_TEMPLATE_FUNCTION(str_len);
