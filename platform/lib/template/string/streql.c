#include <bpapi.h>
#include <ctemplates.h>


static const struct tpvar *
streql(struct bpapi *api, struct tpvar *dst, enum bpcacheable *cc, int argc, const struct tpvar **argv) {
	int i;
	const char *str;

	TPVAR_STRTMP(str, argv[0]);

	for (i = 0 ; i < argc ; i++) {
		const char *a;

		TPVAR_STRTMP(a, argv[i]);

		if (strcmp(str, a) != 0) {
			return TPV_SET(dst, TPV_INT(0));
		}
	}

	return TPV_SET(dst, TPV_INT(1));
}

ADD_TEMPLATE_FUNCTION(streql);
