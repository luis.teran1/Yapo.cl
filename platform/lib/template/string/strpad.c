#include <bpapi.h>
#include <ctemplates.h>


static const struct tpvar *
strpad(struct bpapi *api, struct tpvar *dst, enum bpcacheable *cc, int argc, const struct tpvar **argv) {
	char *res;
	char *fa0;

	if (argc < 2)
		return TPV_SET(dst, TPV_NULL);

	xasprintf(&res, "%-*s", tpvar_int(argv[1]), tpvar_str(argv[0], &fa0));
	free(fa0);

	return TPV_SET(dst, TPV_DYNSTR(res));
}

ADD_TEMPLATE_FUNCTION(strpad);
