#include <bpapi.h>
#include <ctemplates.h>


static const struct tpvar *
concat(struct bpapi *api, struct tpvar *dst, enum bpcacheable *cc, int argc, const struct tpvar **argv) {
        int i;
	struct buf_string buf = {0};

        for (i = 0; i < argc ; i++) {
		char *fa;
		const char *a = tpvar_str(argv[i], &fa);

		bswrite(&buf, a, strlen(a));
		free(fa);
	}
	return TPV_SET(dst, TPV_DYNSTR(buf.buf));
}

ADD_TEMPLATE_FUNCTION(concat);
