#include <bpapi.h>
#include <ctemplates.h>

#include <unicode/utf8.h>
#include <unicode/uchar.h>
#include <unicode.h>

static const struct tpvar *
substring(struct bpapi *api, struct tpvar *dst, enum bpcacheable *cc, int argc, const struct tpvar **argv) {
	int offset;
	int limit;
	const char *string;
	int sl;
	char *res;
	char *fs;
	
	if (argc < 2 || argc > 3)
		return TPV_SET(dst, TPV_NULL);

	if ((string = tpvar_str(argv[0], &fs)) == NULL)
		return TPV_SET(dst, TPV_NULL);
	sl = strlen(string);
	offset = tpvar_int(argv[1]);

	if (argc == 3)
		limit = tpvar_int(argv[2]);
	else {
		if (offset < 0)
			limit = -offset;
		else	
			limit = sl - offset;
	}

	if (offset < 0) {
		offset += sl; /* New offset is string length + offset */
		if (offset > 0)
			string += offset;
	} else if (offset >= sl)
		return TPV_SET(dst, TPV_NULL); /* Skipped past the whole string */
	else
		string += offset;

	if (argc == 2 && !fs)
		return TPV_SET(dst, TPV_STRING(string));

	xasprintf(&res, "%.*s", limit, string);
	free(fs);
	return TPV_SET(dst, TPV_DYNSTR(res));
}

ADD_TEMPLATE_FUNCTION(substring);

static const struct tpvar *
substring_utf8(struct bpapi *api, struct tpvar *dst, enum bpcacheable *cc, int argc, const struct tpvar **argv) {
	int offset;
	int limit;
	const char *string;
	int sl;
	char *fs;
	char *res;
	int i, j, len;
	UChar32 ch;

	if (argc < 2 || argc > 3)
		return TPV_SET(dst, TPV_NULL);

	if ((string = tpvar_str(argv[0], &fs)) == NULL)
		return TPV_SET(dst, TPV_NULL);

	sl = strlen_utf8(string);
	offset = tpvar_int(argv[1]);

	if (argc == 3)
		limit = tpvar_int(argv[2]);
	else {
		if (offset < 0)
			limit = -offset;
		else
			limit = sl - offset;
	}

	if (offset < 0) {
		offset += sl; /* New offset is string length + offset */
	} else if (offset >= sl) {
		return TPV_SET(dst, TPV_NULL); /* Skipped past the whole string */
	}

	len = strlen(string);
	for (i = 0, j = 0; i < len && j < offset; j++) {
		U8_NEXT(string, i, len, ch);
	}

	string += i;

	len = strlen(string);
	for (i = 0, j = 0; i < len && j < limit; j++) {
		U8_NEXT(string, i, len, ch);
	}

	xasprintf(&res, "%.*s", i, string);

	if (fs)
		free(fs);

	return TPV_SET(dst, TPV_DYNSTR(res));
}

ADD_TEMPLATE_FUNCTION(substring_utf8);
