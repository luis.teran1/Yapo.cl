#ifndef TPC_TMPL_LRU_H
#define TPC_TMPL_LRU_H

#include "lru.h"
#include "tptypes.h"

struct template_lru_entry {
	struct lru_entry *entry;
	int new_entry;
};

struct template_opaque_var *template_lru_cache(struct lru **lru, int sz, const struct tpvar *keyvar, struct template_lru_entry *entry);

#endif
