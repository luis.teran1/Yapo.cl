
#include "bpapi.h"
#include "patchvars.h"

#include <stdbool.h>

enum stree_bucket {
	VS_TOP,
	VS_LOOP_TOP,
	VS_OUTSIDE,
	VS_STREE,
	VS_LTREE,
	VS_STREE_MEMBER,
	VS_LTREE_MEMBER,
	VS_LTREE_MEMBER_IDX,
	VS_TOO_DEEP,
};

struct stree_data {
	struct bpapi_simplevars_chain *svars;
	char *s_prefix;
	char *l_prefix;
	enum stree_bucket root;
	char *root_key;
	int root_idx;
};

static int
get_index(struct bpapi_simplevars_chain *svars, const char *var, const char *idx) {
	if (idx == VTREE_LOOP)
		return -1;

	char *end;
	int n = strtol(idx, &end, 10);

	if (idx[0] != '\0' && *end == '\0' && n >= 0 && (var == VTREE_LOOP || n < bps_length(svars, var)))
		return n;
	return -1;
}

static enum stree_bucket
get_top_bucket(const struct stree_data *data, int argc, const char **argv, const char **out_key, int *out_idx) {
	if (argv[0] == VTREE_LOOP)
		return VS_LOOP_TOP;

	bool is_s = data->s_prefix != NULL && strcmp(data->s_prefix, argv[0]) == 0;
	bool is_l = !is_s && data->l_prefix != NULL && strcmp(data->l_prefix, argv[0]) == 0;

	if (!is_s && !is_l)
		return VS_OUTSIDE;

	if (argc >= 2)
		*out_key = argv[1];
	if (argc >= 3)
		*out_idx = get_index(data->svars, argv[1], argv[2]);

	switch (argc) {
	case 1:
		return is_s ? VS_STREE : VS_LTREE;
	case 2:
		return is_s ? VS_STREE_MEMBER : VS_LTREE_MEMBER;
	case 3:
		if (is_s)
			return VS_TOO_DEEP;
		return VS_LTREE_MEMBER_IDX;
	}
	return VS_TOO_DEEP;
}

static enum stree_bucket
get_bucket(const struct stree_data *data, int argc, const char **argv, const char **out_key, int *out_idx) {
	if (argc == 0) {
		*out_key = data->root_key;
		*out_idx = data->root_idx;
		return data->root;
	}

	switch (data->root) {
	case VS_TOP:
		return get_top_bucket(data, argc, argv, out_key, out_idx);
	case VS_STREE:
		*out_key = argv[0];
		if (argc == 1)
			return VS_STREE_MEMBER;
		break;
	case VS_LTREE:
		*out_key = argv[0];
		if (argc == 1)
			return VS_LTREE_MEMBER;
		*out_idx = get_index(data->svars, argv[0], argv[1]);
		if (argc == 2)
			return VS_LTREE_MEMBER_IDX;
		break;
	case VS_LTREE_MEMBER:
		*out_key = data->root_key;
		*out_idx = get_index(data->svars, data->root_key, argv[0]);
		if (argc == 1)
			return VS_LTREE_MEMBER_IDX;
		break;
	case VS_STREE_MEMBER:
	case VS_LTREE_MEMBER_IDX:
		*out_key = data->root_key;
		*out_idx = data->root_idx;
		break;
	default:
		break;
	}
	return VS_TOO_DEEP;
}

static int
stree_getlen(struct bpapi_vtree_chain *vchain, enum bpcacheable *cc, int argc, const char **argv) {
	const struct stree_data *data = vchain->data;

	const char *key;
	int idx;
	switch (get_bucket(data, argc, argv, &key, &idx)) {
	case VS_TOP:
		return (data->s_prefix != NULL) + (data->l_prefix != NULL) + vchain->next->fun->getlen(vchain->next, cc, argc, argv);
	case VS_OUTSIDE:
		return vchain->next->fun->getlen(vchain->next, cc, argc, argv);
	case VS_STREE:
	case VS_LTREE:
		*cc = BPCACHE_CANT;
		return bps_keyglob_len(data->svars, "*");
	case VS_STREE_MEMBER:
		*cc = BPCACHE_CANT;
		return bps_has_key(data->svars, key) ? 1 : 0;
	case VS_LTREE_MEMBER:
		*cc = BPCACHE_CANT;
		return bps_length(data->svars, key);
	case VS_LTREE_MEMBER_IDX:
		*cc = BPCACHE_CANT;
		return idx >= 0;
	case VS_LOOP_TOP:
	case VS_TOO_DEEP:
		*cc = BPCACHE_CANT;
		return 0;
	}
	return 0;
}

static const char *
stree_get(struct bpapi_vtree_chain *vchain, enum  bpcacheable *cc, int argc, const char **argv) {
	const struct stree_data *data = vchain->data;

	const char *key;
	int idx;
	switch (get_bucket(data, argc, argv, &key, &idx)) {
	case VS_TOP:
	case VS_LOOP_TOP:
	case VS_STREE:
	case VS_LTREE:
	case VS_LTREE_MEMBER:
	case VS_TOO_DEEP:
		*cc = BPCACHE_CANT;
		return NULL;
	case VS_OUTSIDE:
		return vchain->next->fun->get(vchain->next, cc, argc, argv);
	case VS_STREE_MEMBER:
		*cc = BPCACHE_CANT;
		return bps_get_element(data->svars, key, 0);
	case VS_LTREE_MEMBER_IDX:
		{
			*cc = BPCACHE_CANT;
			if (idx < 0)
				return NULL;
			return bps_get_element(data->svars, key, idx);
		}
	}
	return NULL;
}

static int
stree_haskey(struct bpapi_vtree_chain *vchain, enum  bpcacheable *cc, int argc, const char **argv) {
	const struct stree_data *data = vchain->data;

	const char *key;
	int idx;
	switch (get_bucket(data, argc, argv, &key, &idx)) {
	case VS_TOP:
	case VS_STREE:
	case VS_LTREE:
		*cc = BPCACHE_CANT;
		return true;
	case VS_OUTSIDE:
		return vchain->next->fun->haskey(vchain->next, cc, argc, argv);
	case VS_STREE_MEMBER:
	case VS_LTREE_MEMBER:
		*cc = BPCACHE_CANT;
		return bps_has_key(data->svars, key);
	case VS_LTREE_MEMBER_IDX:
		*cc = BPCACHE_CANT;
		return idx >= 0;
	case VS_LOOP_TOP:
	case VS_TOO_DEEP:
		*cc = BPCACHE_CANT;
		return false;
	}
	return false;
}

static void
stree_loop_cleanup(struct bpapi_loop_var *loop) {
	free(loop->l.list);
}

static void
stree_loop_cleanup_all(struct bpapi_loop_var *loop) {
	int i;

	for (i = 0 ; i < loop->len ; i++)
		free((char*)loop->l.list[i]);
	free(loop->l.list);
}

static void
stree_loop_cleanup_nodes(struct bpapi_loop_var *loop) {
	int i;

	for (i = 0 ; i < loop->len ; i++)
		vtree_free(&loop->l.vlist[i]);
	free(loop->l.vlist);
}

static void
stree_fetch_keys_top(struct bpapi_vtree_chain *vchain, struct bpapi_loop_var *loop, enum bpcacheable *cc) {
	const struct stree_data *data = vchain->data;
	bool add_s = data->s_prefix != NULL, add_l = data->l_prefix != NULL;
	int i;

	vchain->next->fun->fetch_keys(vchain->next, loop, cc, 0, NULL);

	/* Most likely we have to add s and l, but double check */
	for (i = 0 ; i < loop->len && (add_s || add_l); i++) {
		if (add_s && strcmp(loop->l.list[i], data->s_prefix) == 0)
			add_s = false;
		else if (add_l && strcmp(loop->l.list[i], data->l_prefix) == 0)
			add_l = false;
	}

	if (add_s || add_l) {
		int n = loop->len + (add_s ? 1 : 0) + (add_l ? 1 : 0);
		char **newlist = xmalloc(n * sizeof(*newlist));
		for (i = 0 ; i < loop->len ; i++)
			newlist[i] = xstrdup(loop->l.list[i]);
		if (add_s)
			newlist[i++] = xstrdup(data->s_prefix);
		if (add_l)
			newlist[i++] = xstrdup(data->l_prefix);
		if (loop->cleanup)
			loop->cleanup(loop);
		loop->len = n;
		loop->l.list = (const char**)newlist;
		loop->cleanup = stree_loop_cleanup_all;
		*cc = BPCACHE_CANT;
	}
}

static void
stree_fetch_keys_list(const struct stree_data *data, struct bpapi_loop_var *loop, enum bpcacheable *cc, const char *key) {
	*cc = BPCACHE_CANT;
	loop->len = bps_length(data->svars, key);

	if (loop->len == 0) {
		loop->l.list = NULL;
		loop->cleanup = NULL;
		return;
	}

	loop->l.list = xmalloc(loop->len * sizeof(*loop->l.list));
	for (int i = 0 ; i < loop->len ; i++)
		xasprintf((char**)&loop->l.list[i], "%d", i);
	loop->cleanup = stree_loop_cleanup_all;
}

static void
stree_fetch_keys(struct bpapi_vtree_chain *vchain, struct bpapi_loop_var *loop, enum bpcacheable *cc, int argc, const char **argv) {
	const struct stree_data *data = vchain->data;

	const char *key;
	int idx;
	switch (get_bucket(data, argc, argv, &key, &idx)) {
	case VS_TOP:
		stree_fetch_keys_top(vchain, loop, cc);
		break;
	case VS_OUTSIDE:
		vchain->next->fun->fetch_keys(vchain->next, loop, cc, argc, argv);
		break;
	case VS_STREE:
	case VS_LTREE:
		*cc = BPCACHE_CANT;
		bps_fetch_glob_loop(data->svars, "*", loop);
		break;
	case VS_LOOP_TOP:
	case VS_STREE_MEMBER:
	case VS_LTREE_MEMBER_IDX:
	case VS_TOO_DEEP:
		*cc = BPCACHE_CANT;
		loop->len = 0;
		loop->l.list = NULL;
		loop->cleanup = NULL;
		break;
	case VS_LTREE_MEMBER:
		stree_fetch_keys_list(data, loop, cc, key);
		break;
	}
}

static void
stree_fetch_values_vars(const struct stree_data *data, struct bpapi_loop_var *loop, const char *keys_by_value) {
	bps_fetch_glob_loop(data->svars, "*", loop);

	if (!loop->len)
		return;

	const char **newlist = xmalloc(loop->len * sizeof (*newlist));

	int n = 0;
	for (int i = 0 ; i < loop->len ; i++) {
		const char *v = bps_get_element(data->svars, loop->l.list[i], 0);
		if (!keys_by_value) {
			newlist[n++] = v;
			continue;
		}

		if (strcmp(v, keys_by_value) != 0)
			continue;

		/* Have to dup since I don't know if they should be freed or not. */
		newlist[n++] = xstrdup(loop->l.list[i]);
	}

	if (loop->cleanup)
		loop->cleanup(loop);
	loop->len = n;
	loop->l.list = newlist;
	loop->cleanup = keys_by_value ? stree_loop_cleanup_all : stree_loop_cleanup;
}

static void
stree_fetch_values_loop_top(struct bpapi_vtree_chain *vchain, struct bpapi_loop_var *loop, enum bpcacheable *cc, int argc, const char **argv, const char *keys_by_value) {
	/*
	 * XXX this is kind of broken, our vars should have precedence.
	 * But nobody uses $(@) anyway.
	 */
	struct stree_data *data = vchain->data;

	bool add_s = data->s_prefix != NULL && !vchain->next->fun->haskey(vchain->next, cc, 1, (const char*[]){data->s_prefix});
	bool add_l = data->l_prefix != NULL && !vchain->next->fun->haskey(vchain->next, cc, 1, (const char*[]){data->l_prefix});

	if (keys_by_value)
		vchain->next->fun->fetch_byval(vchain->next, loop, cc, keys_by_value, argc, argv);
	else
		vchain->next->fun->fetch_values(vchain->next, loop, cc, argc, argv);

	if (!add_s && !add_l)
		return;

	*cc = BPCACHE_CANT;

	int sz = loop->len + (add_s ? 1 : 0) + (add_l ? 1 : 0);
	char **newlist = xmalloc(sz * sizeof(*newlist));

	int i;
	for (i = 0 ; i < loop->len ; i++)
		newlist[i] = loop->l.list[i] ? xstrdup(loop->l.list[i]) : NULL;

	if (loop->cleanup)
		loop->cleanup(loop);
	loop->l.list = (const char**)newlist;
	loop->cleanup = stree_loop_cleanup_all;

	if (add_s) {
		const char *v;
		if (argc == 2 && (v = bps_get_element(data->svars, argv[1], 0))) {
			if (keys_by_value) {
				if (strcmp(v, keys_by_value) == 0)
					loop->l.list[i++] = xstrdup(data->s_prefix);
			} else {
				loop->l.list[i++] = xstrdup(v);
			}
		} else if (!keys_by_value) {
			loop->l.list[i++] = NULL;
		}
	}

	if (add_l) {
		int n;

		if (argc == 3 && (n = get_index(data->svars, argv[1], argv[2])) >= 0) {
			const char *v = bps_get_element(data->svars, argv[1], n);
			if (keys_by_value) {
				if (strcmp(v, keys_by_value) == 0)
					loop->l.list[i++] = xstrdup(data->l_prefix);
			} else {
				loop->l.list[i++] = xstrdup(v);
			}
		} else if (!keys_by_value) {
			loop->l.list[i++] = NULL;
		}
	}

	loop->len = i;
}

static void
stree_fetch_values_loop(const struct stree_data *data, struct bpapi_loop_var *loop, int idx, const char *keys_by_value) {
	if (idx < 0)
		return;

	bps_fetch_glob_loop(data->svars, "*", loop);

	if (!loop->len)
		return;

	const char **newlist = xmalloc(loop->len * sizeof (*newlist));

	int n = 0;
	for (int i = 0 ; i < loop->len ; i++) {
		const char *v = idx < bps_length(data->svars, loop->l.list[i]) ?
				bps_get_element(data->svars, loop->l.list[i], idx) : NULL;

		if (!keys_by_value) {
			newlist[n++] = v;
			continue;
		}

		if (!v || strcmp(v, keys_by_value) != 0)
			continue;

		newlist[n++] = xstrdup(loop->l.list[i]);
	}

	if (loop->cleanup)
		loop->cleanup(loop);
	loop->len = n;
	loop->l.list = newlist;
	loop->cleanup = keys_by_value ? stree_loop_cleanup_all : stree_loop_cleanup;
}

static void
stree_fetch_values(struct bpapi_vtree_chain *vchain, struct bpapi_loop_var *loop, enum bpcacheable *cc, int argc, const char **argv) {
	const struct stree_data *data = vchain->data;

	loop->len = 0;
	loop->l.list = NULL;
	loop->cleanup = NULL;

	const char *key;
	int idx;
	switch (get_bucket(data, argc, argv, &key, &idx)) {
	case VS_TOP:
	case VS_STREE:
	case VS_LTREE:
		/* Shouldn't happen, no VTREE_LOOP */
		break;
	case VS_LTREE_MEMBER:
	case VS_TOO_DEEP:
		*cc = BPCACHE_CANT;
		break;
	case VS_LOOP_TOP:
		stree_fetch_values_loop_top(vchain, loop, cc, argc, argv, NULL);
		break;
	case VS_OUTSIDE:
		vchain->next->fun->fetch_values(vchain->next, loop, cc, argc, argv);
		break;
	case VS_STREE_MEMBER:
		*cc = BPCACHE_CANT;
		if (key == VTREE_LOOP)
			stree_fetch_values_vars(data, loop, NULL);
		break;
	case VS_LTREE_MEMBER_IDX:
		*cc = BPCACHE_CANT;
		if (key == VTREE_LOOP)
			stree_fetch_values_loop(data, loop, idx, NULL);
		else if (argc > 0 && argv[argc - 1] == VTREE_LOOP)
			bps_fetch_loop(data->svars, key, loop);
		break;
	}
}

static void
stree_fetch_loop_byval(const struct stree_data *data, const char *key, struct bpapi_loop_var *loop, const char *value) {
	bps_fetch_loop(data->svars, key, loop);

	if (!loop->len)
		return;

	char **newlist = xmalloc(loop->len * sizeof(*newlist));

	int n = 0;
	for (int i = 0; i < loop->len ; i++) {
		if (!loop->l.list[i] || strcmp(loop->l.list[i], value) != 0)
			continue;

		xasprintf(&newlist[n++], "%d", i);
	}

	if (loop->cleanup)
		loop->cleanup(loop);
	loop->len = n;
	loop->l.list = (const char**)newlist;
	loop->cleanup = stree_loop_cleanup_all;
}

static void
stree_fetch_keys_by_value(struct bpapi_vtree_chain *vchain, struct bpapi_loop_var *loop, enum bpcacheable *cc, const char *value, int argc, const char **argv) {
	const struct stree_data *data = vchain->data;

	loop->len = 0;
	loop->l.list = NULL;
	loop->cleanup = NULL;

	const char *key;
	int idx;
	switch (get_bucket(data, argc, argv, &key, &idx)) {
	case VS_TOP:
	case VS_STREE:
	case VS_LTREE:
		/* Shouldn't happen, no VTREE_LOOP */
		break;
	case VS_LTREE_MEMBER:
	case VS_TOO_DEEP:
		break;
	case VS_LOOP_TOP:
		stree_fetch_values_loop_top(vchain, loop, cc, argc, argv, value);
		break;
	case VS_OUTSIDE:
		vchain->next->fun->fetch_byval(vchain->next, loop, cc, value, argc, argv);
		break;
	case VS_STREE_MEMBER:
		*cc = BPCACHE_CANT;
		if (key == VTREE_LOOP)
			stree_fetch_values_vars(data, loop, value);
		break;
	case VS_LTREE_MEMBER_IDX:
		*cc = BPCACHE_CANT;
		if (key == VTREE_LOOP)
			stree_fetch_values_loop(data, loop, idx, value);
		else if (argc > 0 && argv[argc - 1] == VTREE_LOOP)
			stree_fetch_loop_byval(data, key, loop, value);
		break;
	}
}

const struct bpapi_vtree stree_vtree;

static struct bpapi_vtree_chain *
stree_getnode_copy(const struct stree_data *data, struct bpapi_vtree_chain *dst, enum stree_bucket root, const char *key, int idx) {
	struct stree_data *newdata = xmalloc(sizeof (*newdata));

	newdata->svars = data->svars;
	newdata->root = root;
	if (root == VS_TOP) {
		newdata->s_prefix = data->s_prefix;
		newdata->l_prefix = data->l_prefix;
		/* Need to somehow mark that data is heap allocated. Abuse root_key for this. */
		newdata->root_key = (char*)newdata;
	} else {
		newdata->s_prefix = newdata->l_prefix = NULL;
		if (root == VS_LTREE_MEMBER || root == VS_LTREE_MEMBER_IDX)
			newdata->root_key = xstrdup(key);
		else
			newdata->root_key = NULL;
		newdata->root_idx = idx;
	}

	dst->fun = &stree_vtree;
	dst->data = newdata;
	dst->next = NULL;
	return dst;
}

static struct bpapi_vtree_chain *
stree_getnode(struct bpapi_vtree_chain *vchain, enum bpcacheable *cc, struct bpapi_vtree_chain *dst, int argc, const char **argv) {
	const struct stree_data *data = vchain->data;

	const char *key;
	int idx;
	enum stree_bucket b = get_bucket(data, argc, argv, &key, &idx);
	switch (b) {
	case VS_TOP:
	case VS_STREE:
	case VS_LTREE:
	case VS_LTREE_MEMBER:
	case VS_STREE_MEMBER:
	case VS_LTREE_MEMBER_IDX:
		*cc = BPCACHE_CANT;
		return stree_getnode_copy(data, dst, b, key, idx);
	case VS_OUTSIDE:
		return vchain->next->fun->getnode(vchain->next, cc, dst, argc, argv);
	case VS_LOOP_TOP:
	case VS_TOO_DEEP:
		*cc = BPCACHE_CANT;
		return NULL;
	}
	return NULL;
}

static void
stree_fetch_top_nodes(struct bpapi_vtree_chain *vchain, struct bpapi_loop_var *loop, enum bpcacheable *cc, int argc, const char **argv) {
	struct stree_data *data = vchain->data;

	bool add_s = data->s_prefix != NULL && !vchain->next->fun->haskey(vchain->next, cc, 1, (const char*[]){data->s_prefix});
	bool add_l = data->l_prefix != NULL && !vchain->next->fun->haskey(vchain->next, cc, 1, (const char*[]){data->l_prefix});

	vchain->next->fun->fetch_nodes(vchain->next, loop, cc, argc, argv);

	if (!add_s && !add_l)
		return;

	*cc = BPCACHE_CANT;
	int sz = loop->len + (add_s ? 1 : 0) + (add_l ? 1 : 0);
	struct bpapi_vtree_chain *newlist = xmalloc(sz * sizeof(*newlist));

	int i;
	for (i = 0 ; i < loop->len ; i++)
		newlist[i] = loop->l.vlist[i];

	if (loop->cleanup) {
		/* Clear the old nodes since we moved them. */
		memset(loop->l.vlist, 0, loop->len * sizeof(*loop->l.vlist));
		loop->cleanup(loop);
	}

	if (add_s)
		stree_getnode_copy(data, &newlist[i++], VS_STREE, NULL, 0);

	if (add_l)
		stree_getnode_copy(data, &newlist[i++], VS_LTREE, NULL, 0);

	loop->l.vlist = newlist;
	loop->cleanup = stree_loop_cleanup_nodes;
	loop->len = i;
}

static void
stree_fetch_var_nodes(const struct stree_data *data, struct bpapi_loop_var *loop, enum stree_bucket b) {
	bps_fetch_glob_loop(data->svars, "*", loop);
	if (!loop->len)
		return;

	struct bpapi_vtree_chain *newlist = xmalloc(loop->len * sizeof(*newlist));

	b = b == VS_STREE ? VS_STREE_MEMBER : VS_LTREE_MEMBER;

	for (int i = 0 ; i < loop->len ; i++)
		stree_getnode_copy(data, &newlist[i], b, loop->l.list[i], 0);

	if (loop->cleanup)
		loop->cleanup(loop);
	loop->l.vlist = newlist;
	loop->cleanup = stree_loop_cleanup_nodes;
}

static void
stree_fetch_list_nodes(const struct stree_data *data, struct bpapi_loop_var *loop, const char *key) {
	bps_fetch_loop(data->svars, key, loop);
	if (!loop->len)
		return;

	struct bpapi_vtree_chain *newlist = xmalloc(loop->len * sizeof(*newlist));

	for (int i = 0 ; i < loop->len ; i++)
		stree_getnode_copy(data, &newlist[i], VS_LTREE_MEMBER_IDX, key, i);

	if (loop->cleanup)
		loop->cleanup(loop);
	loop->l.vlist = newlist;
	loop->cleanup = stree_loop_cleanup_nodes;
}

static void
stree_fetch_nodes(struct bpapi_vtree_chain *vchain, struct bpapi_loop_var *loop, enum bpcacheable *cc, int argc, const char **argv) {
	const struct stree_data *data = vchain->data;

	loop->len = 0;
	loop->l.list = NULL;
	loop->cleanup = NULL;

	const char *key;
	int idx;
	enum stree_bucket b = get_bucket(data, argc, argv, &key, &idx);
	switch (b) {
	case VS_TOP:
		stree_fetch_top_nodes(vchain, loop, cc, argc, argv);
		break;
	case VS_OUTSIDE:
		vchain->next->fun->fetch_nodes(vchain->next, loop, cc, argc, argv);
		break;
	case VS_STREE:
	case VS_LTREE:
		*cc = BPCACHE_CANT;
		stree_fetch_var_nodes(data, loop, b);
		break;
	case VS_LTREE_MEMBER:
		*cc = BPCACHE_CANT;
		stree_fetch_list_nodes(data, loop, key);
		break;
	case VS_LOOP_TOP:
	case VS_STREE_MEMBER:
	case VS_LTREE_MEMBER_IDX:
	case VS_TOO_DEEP:
		*cc = BPCACHE_CANT;
		break;
	}
}

static void
stree_loop_cleanup_keys_and_values(struct vtree_keyvals *loop) {
	for (int i = 0 ; i < loop->len ; i++) {
		free((char*)loop->list[i].key);
		switch(loop->list[i].type) {
		case vkvNone:
			break;
		case vkvValue:
			free((char*)loop->list[i].v.value);
			break;
		case vkvNode:
			vtree_free(&loop->list[i].v.node);
			break;
		}
	}
	free(loop->list);
}

static void
stree_fetch_keys_and_values_loop_top(struct bpapi_vtree_chain *vchain, struct vtree_keyvals *loop, enum bpcacheable *cc, int argc, const char **argv) {
	struct stree_data *data = vchain->data;

	bool add_s = data->s_prefix != NULL && !vchain->next->fun->haskey(vchain->next, cc, 1, (const char*[]){data->s_prefix});
	bool add_l = data->l_prefix != NULL && !vchain->next->fun->haskey(vchain->next, cc, 1, (const char*[]){data->l_prefix});

	vchain->next->fun->fetch_keys_and_values(vchain->next, loop, cc, argc, argv);

	if (!add_s && !add_l)
		return;

	*cc = BPCACHE_CANT;
	int sz = loop->len + (add_s ? 1 : 0) + (add_l ? 1 : 0);
	struct vtree_keyvals_elem *newlist = xmalloc(sz * sizeof(*newlist));

	int i;
	for (i = 0 ; i < loop->len ; i++)
		newlist[i] = loop->list[i];

	if (loop->cleanup)
		loop->cleanup(loop);
	loop->list = newlist;
	loop->cleanup = stree_loop_cleanup_keys_and_values;

	if (add_s) {
		newlist[i].key = xstrdup(data->s_prefix);
		switch (argc) {
		case 1:
			newlist[i].type = vkvNode;
			stree_getnode_copy(data, &newlist[i].v.node, VS_STREE, NULL, 0);
			break;
		case 2:
			{
				const char *v = bps_get_element(data->svars, argv[1], 0);
				if (v) {
					newlist[i].type = vkvValue;
					newlist[i].v.value = xstrdup(v);
				} else {
					newlist[i].type = vkvNone;
				}
			}
			break;
		default:
			newlist[i].type = vkvNone;
			break;
		}
		i++;
	}

	if (add_l) {
		newlist[i].key = xstrdup(data->s_prefix);
		switch (argc) {
		case 1:
			newlist[i].type = vkvNode;
			stree_getnode_copy(data, &newlist[i].v.node, VS_LTREE, NULL, 0);
			break;
		case 2:
			if (bps_has_key(data->svars, argv[1])) {
				newlist[i].type = vkvNode;
				stree_getnode_copy(data, &newlist[i].v.node, VS_LTREE_MEMBER, argv[1], 0);
			} else {
				newlist[i].type = vkvNone;
			}
			break;
		case 3:
			{
				int idx = get_index(data->svars, argv[1], argv[2]);
				if (idx >= 0) {
					newlist[i].type = vkvValue;
					newlist[i].v.value = xstrdup(bps_get_element(data->svars, argv[1], idx));
				} else {
					newlist[i].type = vkvNone;
				}
			}
			break;
		default:
			newlist[i].type = vkvNone;
			break;
		}
		i++;
	}

	loop->len = i;
}

static void
stree_fetch_keys_and_values_vars_value(const struct stree_data *data, struct vtree_keyvals *loop, int idx) {
	struct bpapi_loop_var gloop;
	bps_fetch_glob_loop(data->svars, "*", &gloop);

	loop->type = vktDict;
	loop->len = gloop.len;
	loop->list = xmalloc(loop->len * sizeof(*loop->list));
	for (int i = 0 ; i < loop->len ; i++) {
		loop->list[i].key = xstrdup(gloop.l.list[i]);

		const char *v = idx >= 0 && idx < bps_length(data->svars, gloop.l.list[i]) ?
				bps_get_element(data->svars, gloop.l.list[i], idx) : NULL;
		if (v) {
			loop->list[i].type = vkvValue;
			loop->list[i].v.value = xstrdup(v);
		} else {
			loop->list[i].type = vkvNone;
		}
	}
	loop->cleanup = stree_loop_cleanup_keys_and_values;

	if (gloop.cleanup)
		gloop.cleanup(&gloop);
}

static void
stree_fetch_keys_and_values_vars_node(const struct stree_data *data, struct vtree_keyvals *loop) {
	struct bpapi_loop_var gloop;
	bps_fetch_glob_loop(data->svars, "*", &gloop);

	loop->type = vktDict;
	loop->len = gloop.len;
	loop->list = xmalloc(loop->len * sizeof(*loop->list));
	for (int i = 0 ; i < loop->len ; i++) {
		loop->list[i].key = xstrdup(gloop.l.list[i]);
		loop->list[i].type = vkvNode;
		stree_getnode_copy(data, &loop->list[i].v.node, VS_LTREE_MEMBER, gloop.l.list[i], 0);
	}
	loop->cleanup = stree_loop_cleanup_keys_and_values;

	if (gloop.cleanup)
		gloop.cleanup(&gloop);
}

static void
stree_fetch_keys_and_values_list(const struct stree_data *data, struct vtree_keyvals *loop, const char *key) {
	struct bpapi_loop_var lloop;
	bps_fetch_loop(data->svars, key, &lloop);

	loop->type = vktList;
	loop->len = lloop.len;
	loop->list = xmalloc(loop->len * sizeof(*loop->list));
	for (int i = 0 ; i < loop->len ; i++) {
		xasprintf((char**)&loop->list[i].key, "%d", i);
		loop->list[i].type = vkvValue;
		loop->list[i].v.value = xstrdup(lloop.l.list[i]);
	}
	loop->cleanup = stree_loop_cleanup_keys_and_values;

	if (lloop.cleanup)
		lloop.cleanup(&lloop);
}

static void
stree_fetch_keys_and_values(struct bpapi_vtree_chain *vchain, struct vtree_keyvals *loop, enum bpcacheable *cc, int argc, const char **argv) {
	const struct stree_data *data = vchain->data;

	loop->len = 0;
	loop->list = NULL;
	loop->cleanup = NULL;

	const char *key;
	int idx;
	enum stree_bucket b = get_bucket(data, argc, argv, &key, &idx);
	switch (b) {
	case VS_TOP:
	case VS_STREE:
	case VS_LTREE:
		/* No VTREE_LOOP */
		break;
	case VS_LOOP_TOP:
		stree_fetch_keys_and_values_loop_top(vchain, loop, cc, argc, argv);
		break;
	case VS_OUTSIDE:
		vchain->next->fun->fetch_keys_and_values(vchain->next, loop, cc, argc, argv);
		break;
	case VS_STREE_MEMBER:
		*cc = BPCACHE_CANT;
		if (key == VTREE_LOOP)
			stree_fetch_keys_and_values_vars_value(data, loop, 0);
		break;
	case VS_LTREE_MEMBER:
		*cc = BPCACHE_CANT;
		if (key == VTREE_LOOP)
			stree_fetch_keys_and_values_vars_node(data, loop);
		break;
	case VS_LTREE_MEMBER_IDX:
		*cc = BPCACHE_CANT;
		if (key == VTREE_LOOP)
			stree_fetch_keys_and_values_vars_value(data, loop, get_index(data->svars, VTREE_LOOP, argv[2]));
		else if (argc > 0 && argv[argc - 1] == VTREE_LOOP)
			stree_fetch_keys_and_values_list(data, loop, key);
		break;
	case VS_TOO_DEEP:
		*cc = BPCACHE_CANT;
		/* We can still maybe fetch the keys. */
		if (key == VTREE_LOOP)
			stree_fetch_keys_and_values_vars_value(data, loop, -1);
		/* TODO: idx == VTREE_LOOP, unlikely to be used. */
		break;
	}
}

static void
stree_free(struct bpapi_vtree_chain *vchain) {
	struct stree_data *data = vchain->data;

	if (data->root == VS_TOP && !data->root_key) {
		free(data->s_prefix);
		free(data->l_prefix);
	} else {
		free(data->root_key);
		free(data);
	}
}

const struct bpapi_vtree stree_vtree = {
	stree_getlen,
	stree_get,
	stree_haskey,
	stree_fetch_keys,
	stree_fetch_values,
	stree_fetch_keys_by_value,
	stree_getnode,
	stree_fetch_nodes,
	stree_fetch_keys_and_values,
	stree_free
};

static int
init_stree(const struct bpfilter *filter, struct bpapi *api, int argc, const struct tpvar **argv) {
	struct stree_data *data = BPAPI_VTREE_DATA(api);

	data->svars = &api->schain;
	data->root = VS_TOP;
	data->root_key = NULL;

	if (argc >= 1) {
		data->s_prefix = tpvar_strdup(argv[0]);
		if (data->s_prefix[0] == '\0') {
			free(data->s_prefix);
			data->s_prefix = NULL;
		}
	} else {
		data->s_prefix = xstrdup("s");
	}
	if (argc >= 2) {
		data->l_prefix = tpvar_strdup(argv[1]);
		if (data->l_prefix[0] == '\0') {
			free(data->l_prefix);
			data->l_prefix = NULL;
		}
	} else {
		data->l_prefix = xstrdup("l");
	}

	return 0;
}

ADD_VTREE_FILTER(stree, sizeof(struct stree_data));
