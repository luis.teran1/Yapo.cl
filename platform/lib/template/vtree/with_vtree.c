
#include <ctemplates.h>
#include <patchvars.h>

static int
init_with_vtree(const struct bpfilter *filter, struct bpapi *api, int argc, const struct tpvar **argv) {
	struct shadow_vtree *subtree = BPAPI_VTREE_DATA(api);
	struct bpapi_vtree_chain *vtree = api->vchain.next;
	int i;

	if (argc == 1 && argv[0]->type == TPV_NODE) {
		vtree = tpvar_node(argv[0]);
		if (vtree)
			subtree->vtree = *vtree;
		else
			BPAPI_VTREE_DATA(api) = NULL;
		return 0;
	}

	for (i = 0; i < argc; i++) {
		char *fa;
		const char *a;

		a = tpvar_str(argv[i], &fa);
		if (!vtree_getnode(vtree, &subtree->vtree, a, NULL)) {
			BPAPI_VTREE_DATA(api) = NULL;
			free(fa);
			break;
		}
		free(fa);
		vtree = &subtree->vtree;
	}

	return 0;
}

#define with_vtree_vtree shadow_vtree

ADD_VTREE_FILTER(with_vtree, sizeof(struct shadow_vtree));
