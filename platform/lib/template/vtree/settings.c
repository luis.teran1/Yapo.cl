#include <stdio.h>
#include <ctemplates.h>
#include <settings.h>
#include <patchvars.h>


struct settings_data {
	struct patch_data pd;
	struct bpapi_simplevars_chain *schain;
	struct bpapi_vtree_chain *vchain;
};

static const char *
key_lookup(const char *setting, const char *key, void *cbdata) {
	struct settings_data *sd = cbdata;
	const char *s;
	char *path = NULL;
	char *curr = NULL;
	const char *ret;
	int levels = 1;
	int i = 0;
	const char **argv;

	/* special hacks to read from the vtree and not just simplevars */
	if ((strlen(key) > 0 && key[0] == '$') || (strlen(key) > 1 && key[1] == '$' && (key[0] == '?' || key[0] == '!')) ) {
		path = xstrdup(key);
		curr = path;
		while ((curr = strchr(curr + 1, '.'))) {
			levels++;
		}
		argv = xmalloc(sizeof(char*)*levels);

		curr = strchr(path, '$') + 1;
		while (curr && i < levels) {
			argv[i++] = curr;
			curr = strchr(curr, '.');
			if(curr)
				*curr++ = '\0';
		}
		if (key[0] == '?' ) {
			ret = vtree_haskey_cachev(sd->vchain, NULL, NULL, levels, argv) ? "true" : "false";
		} else if (key[0] == '!') {
			s = vtree_get_cachev(sd->vchain, NULL, NULL, levels, argv);
			if (s && *s)
				ret = "false";
			else
				ret = "true";
		} else
			ret = vtree_get_cachev(sd->vchain, NULL, NULL, levels, argv);

		free(argv);
		free(path);

		return ret;
	}

	/* Check if defined */
	if (key[0] == '?')
		return bps_has_key(sd->schain, key + 1) ? "true" : "false";
	/* Check if undefined or empty */
	if (key[0] == '!') {
		s = bps_get_element(sd->schain, key + 1, 0);
		if (s && *s)
			return "false";
		return "true";
	}

	return bps_get_element(sd->schain, key, 0);
}

static void
set_value(const char *setting, const char *key, const char *value, void *cbdata) {
	struct settings_data *sd = cbdata;
	char *k;
	int len;

	ALLOCA_PRINTF(len, k, "%s_%s", setting, key);
	bps_insert(&sd->pd.vars, k, value ? value : "");
}

static int
init_settings(const struct bpfilter *filter, struct bpapi *api, int argc, const struct tpvar **argv) {
	struct settings_data *sd = BPAPI_SIMPLEVARS_DATA(api);
	const char *rootpath;
	struct bpapi_vtree_chain vtree = {0};

	patchvars_init(&sd->pd, NULL, "", 0, NULL, NULL);

	TPVAR_STRTMP(rootpath, argv[0]);

	sd->schain = &api->schain;
	sd->vchain = &api->vchain;

	if (vtree_getnode(&api->vchain, &vtree, rootpath, NULL)) {
		int n;
		const char *s;

		for (n = 1; n < argc; n++) {
			TPVAR_STRTMP(s, argv[n]);
			get_settings(&vtree, s, key_lookup, set_value, sd);
		}

		if (argc == 1)
			get_settings(&vtree, NULL, key_lookup, set_value, sd);

		vtree_free(&vtree);
	}

	return 0;
}

ADD_FILTER(settings, NULL, 0, &patch_simplevars, sizeof(struct settings_data), NULL, 0);
