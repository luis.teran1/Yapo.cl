#include "util.h"
#include "bpapi.h"
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <pcre.h>
#include <ctype.h>
#include <unistd.h>
#include "tmpl_lru.h"
#include "atomic.h"
#include "ctemplates.h"
#include "templateparse_internal.h"
#include "patchvars.h"
#include "strl.h"

void
template_output_var(struct bpapi *api, const struct tpvar *var) {
	if (!var)
		return;

	switch (var->type) {
	case TPV_STRING:
	case TPV_DYNSTR:
		template_output(api, var->v.str);
		break;
	case TPV_INT:
		template_output_int(api, var->v.i);
		break;
	case TPV_NODE:
		break;
	case TPV_CODE:
		{
			int l;
			char *str = var->v.code.func(var->v.code.arg, &l);
			if (str) {
				bpapi_outstring_raw(api, str, l);
				free(str);
			}
		}
		break;
	case TPV_TPVARR:
		{
			int i;
			for (i = 0; i < var->v.tpvarr.nvars; i++) {
				template_output_var(api, var->v.tpvarr.vars[i]);
			}
		}
		break;
	default:
		break;
	}
}

int
template_compare_vars(const struct tpvar *l, const struct tpvar *r) {
	/* Only called when we should do a string compare. */
	char *fl, *fr;
	const char *lv = tpvar_str(l, &fl);
	const char *rv = tpvar_str(r, &fr);
	int res;

	res = template_compare_str(lv, rv);
	free(fl);
	free(fr);
	return res;
}

int
template_regcompare(const char *str, const char *re) {
	pcre *reg;
	const char *errptr;
	int erroffset;
	char *regex;
	char *flags;
	int cflags = 0;
	int res;

	if (!str || !re || re[0] != '/')
		return 0;

	/* Skip the leading slash */
	regex = strdup(re + 1);
	flags = strrchr(regex, '/');
	if (!flags) {
		free(regex);
		return 0;
	}

	*flags = '\0';
	flags++;

	if (strchr(flags, 'i')) {
		cflags |= PCRE_CASELESS;
	}

	if ((reg = pcre_compile(regex, cflags, &errptr, &erroffset, NULL)) == NULL) {
		free(regex);
		return 0;
	}

	res = pcre_exec(reg, NULL, str, strlen(str), 0, 0, NULL, 0);
	if (res < -1) {
		pcre_free(reg);
		free(regex);
		return 0;
	}

	res = res == PCRE_ERROR_NOMATCH ? 1 : 0;
	if (strchr(flags, 'v'))
		res = !res;

	pcre_free(reg);
	free(regex);

	return res == 0;
}

int
template_regcompare_vars(const struct tpvar *strvar, const struct tpvar *revar) {
	const char *str, *re;
	char *fs, *fre;
	int res;

	str = tpvar_str(strvar, &fs);
	re = tpvar_str(revar, &fre);

	res = template_regcompare(str, re);

	free(fs);
	free(fre);

	return res;
}

static void
template_lru_clean(void *v) {
	struct template_opaque_var *var = v;

	if (var->cleanup)
		var->cleanup(var);
	free(var);
}

struct template_opaque_var *
template_lru_cache(struct lru **lru, int sz, const struct tpvar *keyvar, struct template_lru_entry *entry) {
	char *fk;
	const char *key = tpvar_str(keyvar, &fk);

	if (!*lru) {
		struct lru *l = lru_init(sz, template_lru_clean, NULL);

		if (atomic_cas_ptr(lru, NULL, l) != NULL)
			lru_free(l);
	}
	entry->entry = cache_lru(*lru, key, &entry->new_entry, NULL, NULL);
	free(fk);
	if (entry->entry) {
		if (entry->new_entry && !entry->entry->storage)
			entry->entry->storage = zmalloc(sizeof (struct template_opaque_var));
		return entry->entry->storage;
	}
	return NULL;
}


int
self_outstring_fmt(struct bpapi_output_chain *ochain, const char *fmt, ...) {
	/* XXX - this is horrible, but there's not much we can do. */
	char small_buf[4096];
	char *tmp = small_buf;
	va_list ap, cap;
	int len;

	va_start(ap, fmt);
	va_copy(cap, ap);
	if ((len = vsnprintf(tmp, sizeof(small_buf), fmt, ap)) >= (int)sizeof(small_buf)) {
		tmp = xmalloc(len + 1);
		len = vsnprintf(tmp, len + 1, fmt, cap);
	}
	va_end(cap);
	va_end(ap);

	if (len > 0)
		bpo_outstring_raw(ochain, tmp, len);

	if (tmp != small_buf)
		free(tmp);

	return len;
}

void
init_filter(const struct bpfilter * __restrict filter, struct bpapi * __restrict api, struct bpapi * __restrict old_api,
	    void * __restrict dbuf, int * __restrict ofilters) {
	char *d = dbuf;

	BPAPI_COPY(api, old_api, !filter->output, !filter->simplevars, !filter->vtree);

	if (filter->output) {
		api->ochain.fun = filter->output;
		api->ochain.data = NULL;
		api->ochain.next = &old_api->ochain;
		*ofilters = 1;
	}

	if (filter->simplevars) {
		api->schain.fun = filter->simplevars;
		api->schain.data = NULL;
		api->schain.next = &old_api->schain;
		api->schain.cache_gen = old_api->schain.cache_gen;
	}

	if (filter->vtree) {
		api->vchain.fun = filter->vtree;
		api->vchain.data = NULL;
		api->vchain.next = &old_api->vchain;
	}

	if (filter->output_sz) {
		BPAPI_OUTPUT_DATA(api) = d;
		d += TMPL_F_ROUND(filter->output_sz);
		memset(BPAPI_OUTPUT_DATA(api), 0, filter->output_sz);
	}
	if (filter->simplevars_sz) {
		BPAPI_SIMPLEVARS_DATA(api) = d;
		d += TMPL_F_ROUND(filter->simplevars_sz);
		memset(BPAPI_SIMPLEVARS_DATA(api), 0, filter->simplevars_sz);
	}
	if (filter->vtree_sz) {
		BPAPI_VTREE_DATA(api) = d;
		d += TMPL_F_ROUND(filter->vtree_sz);
		memset(BPAPI_VTREE_DATA(api), 0, filter->vtree_sz);
	}
}

void
deinit_filter(const struct bpfilter * __restrict filter, struct bpapi * __restrict api, struct bpapi * __restrict old_api) {
	if (filter->output)
		bpapi_output_free(api);
	if (filter->simplevars)
		bpapi_simplevars_free(api);
	if (filter->vtree)
		bpapi_vtree_free(api);
	if (!filter->simplevars)
		old_api->schain.cache_gen = api->schain.cache_gen;
}

void
tmpl_cached_loop_finalize(struct bpapi_loop_var * __restrict gl, struct bpapi_loop_var * __restrict ll, enum bpcacheable cc_l) {
	if (cc_l != BPCACHE_CAN || atomic_cas_int(&gl->len, -1, -2) != -1) {
		if (ll->cleanup) {
			ll->cleanup(ll);
		}
	} else {
		gl->l.list = ll->l.list;
		gl->cleanup = ll->cleanup;
		atomic_cas_int(&gl->len, -2, ll->len);
	}
}

int
bufstring_outstring_raw(struct bpapi_output_chain *ochain, const char *str, size_t len) {
	return bswrite(ochain->data, str, len);
}

int
bufstring_outstring_fmt(struct bpapi_output_chain *ochain, const char *fmt, ...) {
	va_list ap;
	int res;

	va_start(ap, fmt);
	res = vbscat(ochain->data, fmt, ap);
	va_end(ap);

	return res;
}

const char *
get_tpvar_str(const struct tpvar *var, char **freeptr) {
	if (!var) {
		*freeptr = NULL;
		return NULL;
	}

	switch (var->type) {
	case TPV_STRING:
	case TPV_DYNSTR:
		*freeptr = NULL;
		return var->v.str;
	case TPV_INT:
		xasprintf(freeptr, "%d", var->v.i);
		return *freeptr;
	case TPV_NODE:
		*freeptr = NULL;
		return "";
	case TPV_CODE:
		{
			int l;
			*freeptr = var->v.code.func(var->v.code.arg, &l);
		}
		return *freeptr ?: "";
	case TPV_TPVARR:
		{
			struct buf_string bs = { NULL, 0, 0, };
			int i;
			for (i = 0; i < var->v.tpvarr.nvars; i++) {
				const struct tpvar *t = var->v.tpvarr.vars[i];

				switch (t->type) {
				case TPV_STRING:
				case TPV_DYNSTR:
					bswrite(&bs, t->v.str, strlen(t->v.str));
					break;
				case TPV_INT:
					bscat(&bs, "%d", t->v.i);
					break;
				case TPV_NODE:
					/* Nop */
					break;
				case TPV_CODE:
					{
						int l;
						char *str = t->v.code.func(t->v.code.arg, &l);
						if (str) {
							bswrite(&bs, str, l);
							free(str);
						}
					}
					break;
				case TPV_TPVARR:
					{
						char *fv;
						const char *s = get_tpvar_str(t, &fv);
						bswrite(&bs, s, strlen(s));
						free(fv);
					}
					break;
				case TPV_REGEX:
				case TPV_OPAQUE:
					break;
				}
			}
			*freeptr = bs.buf;
			return bs.buf ?: "";
		}
	default:
		/* Error */
		*freeptr = NULL;
		return NULL;
	}
}

/*
 * This function is for bug compatibility with the define filter. 
 * It's used so much that we don't really dare to change it, but it behaves
 * differently if the block calls template_output with an empty string
 * vs not calling template_output at all, so that case has to be detected.
 * static since we don't want others to use it.
 */
static const char *
get_tpvar_str_nullcode(const struct tpvar *var, char **freeptr) {
	if (!var) {
		*freeptr = NULL;
		return NULL;
	}

	switch (var->type) {
	case TPV_CODE:
		{
			int l;
			*freeptr = var->v.code.func(var->v.code.arg, &l);
		}
		return *freeptr; /* Might be NULL. */
	default:
		return get_tpvar_str(var, freeptr);
	}
}


char *
tpvar_strdup(const struct tpvar *var) {
	char *res;

	if (!var)
		return NULL;

	switch (var->type) {
	case TPV_STRING:
	case TPV_DYNSTR:
		return xstrdup(var->v.str);
	case TPV_INT:
		xasprintf(&res, "%d", var->v.i);
		return res;
	case TPV_NODE:
		return xstrdup("");
	case TPV_CODE:
		{
			int l;
			return var->v.code.func(var->v.code.arg, &l) ?: xstrdup("");
		}
	case TPV_TPVARR:
		{
			char *fv;
			get_tpvar_str(var, &fv);
			return fv;
		}
	default:
		return NULL;
	}
}

char *
tpvar_strcpy(char *dst, const struct tpvar *var, size_t dstsz) {
	if (!var) {
		strlcpy(dst, "", dstsz);
		return dst;
	}

	switch (var->type) {
	case TPV_STRING:
	case TPV_DYNSTR:
		strlcpy(dst, var->v.str, dstsz);
		return dst;
	case TPV_INT:
		snprintf(dst, dstsz, "%d", var->v.i);
		return dst;
	case TPV_NODE:
		if (dstsz > 0)
			dst[0] = '\0';
		return dst;
	case TPV_CODE:
		{
			int l;
			char *str = var->v.code.func(var->v.code.arg, &l);

			if (str) {
				strlcpy(dst, str, dstsz);
				free(str);
			} else {
				dst[0] = '\0';
			}
			return dst;
		}
	case TPV_TPVARR:
		{
			char *d = dst;
			int i;

			*d = '\0';
			for (i = 0; i < var->v.tpvarr.nvars; i++) {
				const struct tpvar *t = var->v.tpvarr.vars[i];
				size_t sz = 0;

				switch (t->type) {
				case TPV_STRING:
				case TPV_DYNSTR:
					sz = strlcat(d, t->v.str, dstsz);
					break;
				case TPV_INT:
					sz = snprintf(d, dstsz, "%d", t->v.i);
					break;
				case TPV_NODE:
					/* Nop */
					break;
				case TPV_CODE:
					{
						int l;
						char *str = t->v.code.func(t->v.code.arg, &l);
						if (str) {
							sz = strlcat(d, str, dstsz);
							free(str);
						}
					}
					break;
				case TPV_TPVARR:
					{
						char *fv;
						const char *s = get_tpvar_str(t, &fv);
						sz = strlcat(d, s, dstsz);
						free(fv);
					}
					break;
				case TPV_REGEX:
				case TPV_OPAQUE:
					break;
				}
				if (sz > dstsz)
					break;
				dstsz -= sz;
				d += sz;
			}
			return dst;
		}
	default:
		/* Error */
		return NULL;
	}
}

int
tpvar_int(const struct tpvar *var) {
	if (!var)
		return 0;

	switch (var->type) {
	case TPV_STRING:
	case TPV_DYNSTR:
		return atoi(var->v.str);
	case TPV_INT:
		return var->v.i;
	case TPV_NODE:
		return 0;
	case TPV_CODE:
		{
			int l;
			char *str = var->v.code.func(var->v.code.arg, &l);
			if (str) {
				int res = atoi(str);

				free(str);
				return res;
			}
			return 0;
		}
	case TPV_TPVARR:
		{
			char *fv;
			int res = atoi(get_tpvar_str(var, &fv));
			free(fv);
			return res;
		}
	default:
		/* Error */
		return 0;
	}
}

float
tpvar_float(const struct tpvar *var) {
	if (!var)
		return 0;

	switch (var->type) {
	case TPV_STRING:
	case TPV_DYNSTR:
		return atof(var->v.str);
	case TPV_INT:
		return var->v.i;
	case TPV_NODE:
		return 0;
	case TPV_CODE:
	case TPV_TPVARR:
		{
			char *fv;
			float res = atof(get_tpvar_str(var, &fv));
			free(fv);
			return res;
		}
	default:
		/* Error */
		return 0;
	}
}

struct bpapi_vtree_chain *
tpvar_node(const struct tpvar *var) {
	if (!var || var->type != TPV_NODE)
		return NULL; /* error */
	return var->v.node;
}

struct cached_regex *
tpvar_regex(const struct tpvar *var) {
	if (!var || var->type != TPV_REGEX)
		return NULL; /* error */
	return var->v.regex;
}

struct template_opaque_var *
tpvar_opaque(const struct tpvar *var) {
	if (!var || var->type != TPV_OPAQUE)
		return NULL; /* error */
	return var->v.opaque;
}

const char *
template_getvar_var(struct bpapi *api, const struct tpvar *var) {
	char *fv;
	const char *v;
	const char *res;

	v = tpvar_str(var, &fv);
	res = template_getvar(api, v);
	free(fv);
	return res;
}

int
template_listlen_var(struct bpapi *api, const struct tpvar *var) {
	char *fv;
	const char *v;
	int res;

	v = tpvar_str(var, &fv);
	res = template_listlen(api, v);
	free(fv);
	return res;
}

int
template_ifdef_var(struct bpapi *api, const struct tpvar *var) {
	char *fv;
	const char *v;
	int res;

	v = tpvar_str(var, &fv);
	res = template_ifdef(api, v);
	free(fv);
	return res;
}

const char *
template_getlist_var(struct bpapi *api, const struct tpvar *var, int idx) {
	char *fv;
	const char *v;
	const char *res;

	v = tpvar_str(var, &fv);
	res = template_getlist(api, v, idx);
	free(fv);
	return res;
}

void
template_fetchlist_var(struct bpapi *api, const struct tpvar *var, struct bpapi_loop_var *loop) {
	char *fv;
	const char *v;

	v = tpvar_str(var, &fv);
	template_fetchlist(api, v, loop);
	free(fv);
}

void
template_define_var(struct bpapi *api, const struct tpvar *key, const struct tpvar *val) {

	if (bpapi_has_insert(api)) {
		char *fv, *fk;
		const char *k = tpvar_str(key, &fk);
		const char *v = get_tpvar_str_nullcode(val, &fv);
		if (v) {
			bpapi_insert(api, k, v);
		}
		free(fv);
		free(fk);
	}
}
