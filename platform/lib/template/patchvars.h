#ifndef PATCHVARS_H
#define PATCHVARS_H

#include "bpapi.h"

struct patch_data {
	struct bpapi_simplevars_chain vars;

	const char *tag; /* For loopfilt filter */

	const char *prefix;
	size_t prefix_len;

	void (*fini_func)(struct patch_data *data);
	void *user_data;

	/* For loop filters.
	 * Set row = -1 if you don't use patchvars_init.
	 * You always have to set rows.
	 */
	int rows;
	int row;
};

extern const struct bpapi_simplevars patch_simplevars;
extern const struct bpapi_simplevars patch_with_scope_simplevars;
extern const struct bpapi_simplevars patch_loop_simplevars;

void patchvars_init(struct patch_data *data, const char *tag, const char *prefix, int prefix_len, void (*fini_func)(struct patch_data *data), void *user_data);
void patchvars_clear(struct patch_data *data);

int patch_has_key(struct bpapi_simplevars_chain *schain, const char *key);
int patch_key_length(struct bpapi_simplevars_chain *schain, const char *key);
const char *patch_get_element(struct bpapi_simplevars_chain *schain, const char *key, int pos);
const char *patch_loop_get_element(struct bpapi_simplevars_chain *schain, const char *key, int pos);
void patch_fetch_loop(struct bpapi_simplevars_chain *schain, const char *key, struct bpapi_loop_var *loop);
int patch_keyglob_len(struct bpapi_simplevars_chain *schain, const char *key);
void patch_fetch_glob_loop(struct bpapi_simplevars_chain *schain, const char *key, struct bpapi_loop_var *loop);
void patch_init_var(struct bpapi_simplevars_chain *schain, const char *key, struct template_var *cache);
void fini_patch(struct bpapi_simplevars_chain *schain);

void parent_insert(struct bpapi_simplevars_chain *schain, const char *key, int klen, const char *val, int vlen);
void parent_insert_list(struct bpapi_simplevars_chain *schain, const char *a, const char **b, int c);
void parent_insert_maxsize(struct bpapi_simplevars_chain *schain, const char *a, const char *b, int maxsize);
void parent_remove(struct bpapi_simplevars_chain *schain, const char *a);

int loop_patch(struct bpapi *api);
int loop_length_patch(struct bpapi *api);

#define PATCH_WITH_LOOP(get_element_fun) { \
	patch_has_key, \
	patch_key_length, \
	(get_element_fun), \
	patch_fetch_loop, \
	patch_keyglob_len, \
	patch_fetch_glob_loop, \
	parent_insert, \
	parent_insert_list, \
	parent_insert_maxsize, \
	parent_remove, \
	fini_patch, \
	patch_init_var, \
}

#endif /*PATCHVARS_H*/
