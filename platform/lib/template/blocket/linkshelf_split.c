#include <bpapi.h>
#include <stdio.h>
#include "log_event.h"

#define LINKSHELF_BUFSZ 128
/* &linkshelf_split(num_cols, "sorted_list", "splitted_list" */
/* XXX should be filter */
static const struct tpvar *
linkshelf_split(struct bpapi *api, struct tpvar *dst, enum bpcacheable *cc, int argc, const struct tpvar **argv) {
	int i;
	int j;
	int rows;
	int tail_rows;
	int cols;
	int layout;
	const char *input_list;
	const char *output_prefix;
	char *fil, *fop;
	int transpose = 0;

	struct bpapi_loop_var list;

	if (argc < 3)
		return TPV_SET(dst, TPV_NULL);

	input_list = tpvar_str(argv[1], &fil);
	output_prefix = tpvar_str(argv[2], &fop);

	layout = tpvar_int(argv[0]);
	if (argc == 4)
		transpose = tpvar_int(argv[3]);

	if (layout == 0) {
		free(fil);
		free(fop);
		return TPV_SET(dst, TPV_NULL);
	}

	bpapi_fetch_loop(api, input_list, &list);
	if (!list.len) {
		goto out;
	}

	rows = 1 + (list.len - 1) / layout;
	tail_rows = list.len % rows;
	cols = list.len / rows + (transpose && tail_rows ? 1 : 0);

	for (i = 0; i < (transpose ? cols : rows) ; i++) {
		char list_name[LINKSHELF_BUFSZ];
		int len;

		snprintf(list_name, LINKSHELF_BUFSZ, "%s%03d", output_prefix, i);

		if (transpose)
			len = (i + 1 < cols || tail_rows == 0) ? rows : tail_rows;
		else
			len = cols + (tail_rows > i ? 1 : 0);
		for (j = 0; j < len ; j++) {
			bpapi_insert(api, list_name, list.l.list[(transpose ? (i * rows + j) : (j * rows + i)) % list.len]);
		}
	}
out:
	if (list.cleanup) {
		list.cleanup(&list);
	}
	free(fil);
	free(fop);
	return TPV_SET(dst, TPV_NULL);
}

ADD_TEMPLATE_FUNCTION(linkshelf_split);

