#ifndef TRANSLATE_H
#define TRANSLATE_H
#include <bpapi.h>
#include "macros.h"
const struct tpvar * translate(struct bpapi *api, struct tpvar *dst, enum bpcacheable *cc, int argc, const struct tpvar **argv) VISIBILITY_HIDDEN;
#endif
