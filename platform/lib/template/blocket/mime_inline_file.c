#include <bpapi.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include "base64.h"
#include "string_functions.h"

/* XXX should change name now that it also does attachments. */
/* XXX convert to filter */
static const struct tpvar *
mime_inline_file(struct bpapi *api, struct tpvar *dst, enum bpcacheable *cc, int argc, const struct tpvar **argv) {
	const int MIME_LINE_LIMIT = 76;
	char content[MIME_LINE_LIMIT * 3]; /* Dividable with 3*4 */
	char content_base64[MIME_LINE_LIMIT * 3 / 3 * 4]; /* Dividable with 3*4 ... compiler will optimize this */
	char *content_base64_current;
	char *filename_real = NULL;
	const char *basedir = NULL;
	const char *sitename = NULL;
	const char *filename = NULL;
	char *quotedname = NULL;
	const char *mimetype = NULL;
	const char *cid = NULL;
	ssize_t filesize = 0;
	ssize_t bytes = 0;
	ssize_t bytes_total = 0;
	int file = -1;
	char *ff, *fm, *fc = NULL;

	if (argc < 2 || argc > 3)
		return TPV_SET(dst, TPV_NULL);

	filename = tpvar_str(argv[0], &ff);
	mimetype = tpvar_str(argv[1], &fm);
	if (argc == 3)
		cid = tpvar_str(argv[2], &fc);
	if (cid && !*cid)
		cid = NULL;

	basedir = vtree_get(&api->vchain, "common", "basedir", NULL);
	if (!basedir)
		goto out;

	sitename = vtree_get(&api->vchain, "common", "sitename", NULL);
	if (!sitename)
		goto out;

	if (cid) {
		quotedname = escape_dquotes(filename);
		bpapi_outstring_fmt(api, "Content-Disposition: inline;\n\tfilename=%s\n", quotedname);
		bpapi_outstring_fmt(api, "Content-Transfer-Encoding: base64\n");
		bpapi_outstring_fmt(api, "Content-Type: %s\n", mimetype);
		bpapi_outstring_fmt(api, "Content-Id: <%s@%s>\n", cid, sitename);
	} else {
		const char *ls = strrchr(filename, '/');

		quotedname = escape_dquotes(ls ? ls + 1 : filename);
		bpapi_outstring_fmt(api, "Content-Disposition: attachment;\n\tfilename=\"%s\"\n", quotedname);
		bpapi_outstring_fmt(api, "Content-Transfer-Encoding: base64\n");
		bpapi_outstring_fmt(api, "Content-Type: %s;\n\tname=\"%s\"\n", mimetype, quotedname);
	}
	free(quotedname);
	bpapi_outstring_fmt(api, "\n");

	if (filename[0] == '/')
		xasprintf(&filename_real, "%s", filename);
	else
		xasprintf(&filename_real, "%s/%s/%s", basedir, cid ? "www" : "share", filename);

	file = open(filename_real, O_RDONLY);

	if (file == -1) {
		bpapi_outstring_fmt(api, "Content fetch failed: %s\n", filename_real);
		goto out;
	}

	filesize = lseek(file, 0, SEEK_END);
	lseek(file, 0, SEEK_SET);

	while (filesize > 0) {
		bytes_total = 0;

		do {
			bytes = read(file, content+bytes_total, sizeof(content)-bytes_total);
			if (bytes > 0)
				bytes_total += bytes;
		} while (bytes_total < (ssize_t)sizeof(content) && bytes > 0);

		bytes = (ssize_t)base64_encode(content_base64, content, (int)bytes_total);

		content_base64_current = content_base64;
		while (bytes > 0) {
			bpapi_outstring_fmt(api, "%.*s\n", bytes > MIME_LINE_LIMIT ? MIME_LINE_LIMIT : (int)bytes, content_base64_current);
			content_base64_current += MIME_LINE_LIMIT;
			bytes -= MIME_LINE_LIMIT;
		}

		filesize -= bytes_total;
	}

	close(file);

out:
	free(filename_real);
	free(ff);
	free(fm);
	free(fc);
	return TPV_SET(dst, TPV_NULL);
}

ADD_TEMPLATE_FUNCTION(mime_inline_file);

