#include <bpapi.h>
#include <stdio.h>

/* XXX Do something smarter, a first class operator. */
static const struct tpvar *
in_vtree_list(struct bpapi *api, struct tpvar *dst, enum bpcacheable *cc, int argc, const struct tpvar **argv) {
	int i;
	struct bpapi_loop_var values;
	char *fa0 = NULL;
	const char *value;
	char *fa1;

	if (argc != 2)
		return TPV_SET(dst, TPV_NULL);

	(void)TPV_SET(dst, TPV_INT(0));

	struct bpapi_vtree_chain *node = tpvar_node(argv[0]);
	if (node) {
		vtree_fetch_values(node, &values, VTREE_LOOP, NULL);
	} else {
		/* XXX only one step (though separators might be supported) */
		vtree_fetch_values(&api->vchain, &values, tpvar_str(argv[0], &fa0), VTREE_LOOP, NULL);
	}
	value = tpvar_str(argv[1], &fa1);

	for (i = 0; i < values.len; i++) {
		if (strcmp(values.l.list[i], value) == 0) {
			(void)TPV_SET(dst, TPV_INT(1));
			break;
		}
	}

	if (values.cleanup)
		values.cleanup(&values);

	free(fa0);
	free(fa1);

	return dst;
}

ADD_TEMPLATE_FUNCTION(in_vtree_list);

