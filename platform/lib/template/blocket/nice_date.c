#include <bpapi.h>
#include <time.h>
#include <stdio.h>
#include "translate.h"

static const struct tpvar *
nice_date(struct bpapi *api, struct tpvar *dst, enum bpcacheable *cc, int argc, const struct tpvar **argv) {
	struct date_rec d;
	struct date_rec today;
	time_t now;
	struct tm t;
	int longmon = 0;
	char *res;
	char *fa0;

	if (argc == 2)
		longmon = tpvar_int(argv[1]);
	else if (argc != 1)
		return TPV_SET(dst, TPV_NULL);

	/* Extract year, month, day and timestamp from value. */
	if (sscanf(tpvar_str(argv[0], &fa0), "%d-%d-%d", &d.year, &d.month, &d.day) != 3) {
		free(fa0);
		return TPV_SET(dst, TPV_NULL);
	}
	free(fa0);

	if (!longmon) {
		now = time(NULL);
		localtime_r(&now, &t);
		date_set(&today, &t);
		*cc = BPCACHE_CANT;

		if (date_cmp(&today, &d) == 0) {
			return translate(api, dst, cc, 1, (const struct tpvar *[]){&TPV_STRING("TODAY")});
		}

		date_set_prev_day(&today);
		if (date_cmp(&today, &d) == 0) {
			return translate(api, dst, cc, 1, (const struct tpvar *[]){&TPV_STRING("YESTERDAY")});
		}
	}

	if (d.month >= 1 && d.month <= 12) {
		const char *datefm;
		char mbuf[3];
		struct tpvar m;
		char *fm;

		snprintf(mbuf, sizeof(mbuf), "%d", d.month);
		translate(api, &m, cc, 2, (const struct tpvar*[]){&TPV_STRING(mbuf), &TPV_STRING(longmon ? "long_month" : "short_month")});
		datefm = vtree_get(&api->vchain, "common", "default", "nice_date", vtree_get(&api->vchain, "common", "default", "lang", NULL), NULL); 
		if (!(datefm && *datefm))
			datefm = "%d %s";
		xasprintf(&res, datefm, d.day, tpvar_str(&m, &fm));
		free(fm);
		if (m.type == TPV_DYNSTR)
			free((char*)m.v.str);
		return TPV_SET(dst, TPV_DYNSTR(res));
	}
	return TPV_SET(dst, TPV_NULL);
}
ADD_TEMPLATE_FUNCTION(nice_date);
