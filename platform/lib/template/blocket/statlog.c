#include <bpapi.h>
#include "log_event.h"

static const struct tpvar *
statlog(struct bpapi *api, struct tpvar *dst, enum bpcacheable *cc, int argc, const struct tpvar **argv) {
	struct bpapi_vtree_chain sub = {0};
	char *fa0 = NULL, *fa1 = NULL;

	if (argc > 1)
		stat_log(vtree_getnode(&api->vchain, &sub, "common", "statpoints", NULL), tpvar_str(argv[0], &fa0), tpvar_str(argv[1], &fa1));
	free(fa0);
	free(fa1);
	vtree_free(&sub);
	*cc = BPCACHE_CANT;
	return TPV_SET(dst, TPV_NULL);
}
ADD_TEMPLATE_FUNCTION_WITH_NAME(statlog, stat_log);
