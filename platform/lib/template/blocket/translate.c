#include <string.h>
#include <bpapi.h>

#include "settings.h"
#include "translate.h"

struct translate_data
{
	const char *code;
	const char *code_cat;
	struct bpapi_simplevars_chain *vars;
	char *value;
};

static const char *
translate_key_lookup(const char *settings, const char *key, void *cbdata) {
	struct translate_data *data = cbdata;

	if (strcmp(key, "code") == 0)
		return data->code;
	if (strcmp(key, "code_cat") == 0)
		return data->code_cat;
	return bps_get_element(data->vars, key, 0);
}

static void
translate_set_value(const char *setting, const char *key, const char *value, void *cbdata) {
	struct translate_data *data = cbdata;

	if (strcmp(key, "value") == 0 && value && !data->value)
		data->value = xstrdup(value);
}

static char *
translate_insert_variables(struct bpapi *api, const char *in_value) {
	struct buf_string buf = {0};
	const char *strptr = in_value;

	while (*strptr) {
		const char *start = strstr(strptr, "{{");
		if (!start)
			break;

		const char *end = strstr(start + 2, "}}");
		if (!end)
			break;

		bswrite(&buf, strptr, start - strptr);

		start += 2;

		char bps_key[end - start + 1];
		memcpy(bps_key, start, end - start);
		bps_key[end - start] = '\0';

		trim(bps_key);
		
		const char *bps_key_value = bpapi_get_element(api, bps_key, 0);

		if (bps_key_value)
			bswrite(&buf, bps_key_value, strlen(bps_key_value));

		strptr = end + 2;
	}

	if (!buf.buf)
		return NULL;

	if (*strptr)
		bswrite(&buf, strptr, strlen(strptr));
	return buf.buf;
}

const struct tpvar *
translate(struct bpapi *api, struct tpvar *dst, enum bpcacheable *cc, int argc, const struct tpvar **argv) {
	struct translate_data data;
	struct bpapi_vtree_chain vtree = {};
	char *fa0, *fa1 = NULL;
	char *inserted;

	if (argc < 1 || argc > 2)
		return TPV_SET(dst, TPV_NULL);

	data.code = tpvar_str(argv[0], &fa0);
	data.code_cat = argc >= 2 ? tpvar_str(argv[1], &fa1) : NULL;
	data.vars = &api->schain;
	data.value = NULL;

	get_settings(vtree_getnode(&api->vchain, &vtree, "lang_settings", NULL), "code_message",
			translate_key_lookup, translate_set_value, &data);
	vtree_free(&vtree);

	free(fa1);

	if (data.value) {
		free(fa0);
		inserted = translate_insert_variables(api, data.value);
		if (inserted) {
			free(data.value);
			return TPV_SET(dst, TPV_DYNSTR(inserted));
		}
		return TPV_SET(dst, TPV_DYNSTR(data.value));
	}

	if (argc == 1) {
		const char *value;

		if (vtree_getint(&api->vchain, "multilang", "enabled", NULL)) {
			value = vtree_get(&api->vchain, "language", data.code, bpapi_get_element(api, "b__lang", 0), NULL);
		} else {
			value = vtree_get(&api->vchain, "language", data.code, vtree_get(&api->vchain, "common", "default", "lang", NULL), NULL);
		}

		if (value && *value) {
			free(fa0);
			inserted = translate_insert_variables(api, value);
			if (inserted)
				return TPV_SET(dst, TPV_DYNSTR(inserted));
			return TPV_SET(dst, TPV_STRING(value));
		}
	}
	
	inserted = translate_insert_variables(api, data.code);
	if (inserted) {
		free(fa0);
		return TPV_SET(dst, TPV_DYNSTR(inserted));
	}

	return tpvar_rebuild(dst, data.code, fa0);
}

ADD_TEMPLATE_FUNCTION(translate);
