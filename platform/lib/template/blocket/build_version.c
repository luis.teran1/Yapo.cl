#include <bpapi.h>
#include "build_version.h"

static const struct tpvar *
build_version(struct bpapi *api, struct tpvar *dst, enum bpcacheable *cc, int argc, const struct tpvar **argv) {
	return TPV_SET(dst, TPV_STRING(BUILD_VERSION));
}
ADD_TEMPLATE_FUNCTION(build_version);
