#include <bpapi.h>
#include <stdio.h>
#include <time.h>
#include "settings.h"

struct time_left_data
{
	const char *code;
	char quantity[12];
	struct bpapi_simplevars_chain *vars;
	char *value;
};

static const char *
time_left_key_lookup(const char *settings, const char *key, void *cbdata) {
	struct time_left_data *data = cbdata;

	if (strcmp(key, "code") == 0)
		return data->code;
	if (strcmp(key, "quantity") == 0)
		return data->quantity;
	return bps_get_element(data->vars, key, 0);
}

static void
time_left_set_value(const char *setting, const char *key, const char *value, void *cbdata) {
	struct time_left_data *data = cbdata;

	if (strcmp(key, "value") == 0 && value && !data->value)
		data->value = xstrdup(value);
}

static const struct tpvar *
time_left(struct bpapi *api, struct tpvar *dst, enum bpcacheable *cc, int argc, const struct tpvar **argv) {
	time_t t, t_now;
	struct tm tm = { .tm_isdst = -1 };
	struct tm tm_now = { .tm_isdst = -1 };
	int diff, d, h, m;
	int pos = 0, len = 128;
	char *output;
	struct time_left_data data;
	struct bpapi_vtree_chain vtree = {};
	char *ff;
	
	if (argc == 1) {
		t_now = time(0); 
	} else if (argc == 2) {
		if (strptime(tpvar_str(argv[0], &ff), "%Y-%m-%d %H:%M:%S", &tm_now) == NULL) {
			free(ff);
			return TPV_SET(dst, TPV_NULL);
		}
		free(ff);
		t_now = mktime(&tm_now);
	} else
		return TPV_SET(dst, TPV_NULL);

	if (strptime(tpvar_str(argv[argc == 1 ? 0 : 1], &ff), "%Y-%m-%d %H:%M:%S", &tm) == NULL) {
		free(ff);
		return TPV_SET(dst, TPV_NULL);
	}
	free(ff);

	t = mktime(&tm);

	diff = (int)difftime(t, t_now);

	if (diff <= 0)
	       return TPV_SET(dst, TPV_NULL);

	d = (int)(diff / 86400);
	h = (int)((diff % 86400) / 3600);
	m = (int)(((diff % 86400) % 3600) / 60);

	if (!vtree_getnode(&api->vchain, &vtree, "lang_settings", NULL)) {
		*cc = BPCACHE_CANT;
		return TPV_SET(dst, TPV_NULL);
	}

	output = zmalloc(len);

	if (d != 0) {
		data.code = "DAYS";
		snprintf(data.quantity, sizeof(data.quantity), "%d", d);
		data.vars = &api->schain;
		data.value = NULL;
		get_settings(&vtree, "code_message", time_left_key_lookup, time_left_set_value, &data);
		if (data.value) {
			bufcat(&output, &len, &pos, "%d %s", d, data.value);
			free(data.value);
		}
	}

	if (h != 0) {
		data.code = "HOURS";
		snprintf(data.quantity, sizeof(data.quantity), "%d", h);
		data.vars = &api->schain;
		data.value = NULL;
		get_settings(&vtree, "code_message", time_left_key_lookup, time_left_set_value, &data);
		if (data.value) {
			bufcat(&output, &len, &pos, "%s%d %s", (d > 0 ? ", " : ""), h, data.value);
			free(data.value);
		}
	}

	if (m != 0) {
		data.code = "MINUTES";
		snprintf(data.quantity, sizeof(data.quantity), "%d", h);
		data.vars = &api->schain;
		data.value = NULL;
		get_settings(&vtree, "code_message", time_left_key_lookup, time_left_set_value, &data);
		if (data.value) {
			bufcat(&output, &len, &pos, "%s%d %s", (d > 0 || h > 0 ? ", " : ""), m, data.value);
			free(data.value);
		}
	}

	vtree_free(&vtree);

	*cc = BPCACHE_CANT;
	return TPV_SET(dst, TPV_DYNSTR(output));
}

ADD_TEMPLATE_FUNCTION(time_left);
