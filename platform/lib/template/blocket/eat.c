#include <bpapi.h>

static const struct tpvar *
eat(struct bpapi *api, struct tpvar *dst, enum bpcacheable *cc, int argc, const struct tpvar **argv) {
	return TPV_SET(dst, TPV_NULL);
}
ADD_TEMPLATE_FUNCTION(eat);
