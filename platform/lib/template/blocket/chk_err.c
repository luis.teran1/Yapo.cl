#include <bpapi.h>
#include "ctemplates.h"
#include "settings.h"

struct keyval_pair {
	const char *key;
	char *template;
	const char *error;
	const char *appl;
};

static const char *
err_tmpl_lookup(const char *settings, const char *key, void *cbdata) {
	struct keyval_pair *data = cbdata;

	if (strcmp(key, "field") == 0)
		return data->key;
	if (strcmp(key, "error") == 0)
		return data->error;
	if (strcmp(key, "appl") == 0)
		return data->appl;
	return NULL;
}

static void
err_tmpl_set_value(const char *setting, const char *key, const char *value, void *cbdata) {
	struct keyval_pair *data = cbdata;

	if ((strcmp(key, "template") == 0) && value)
		data->template = xstrdup(value);
}

static const struct tpvar *
chk_err(struct bpapi *api, struct tpvar *dst, enum bpcacheable *cc, int argc, const struct tpvar **argv) {
	struct bpapi ba;
	struct buf_string *buf;
	struct keyval_pair data;
	char *err_str;
	char *err_str_payload;
	char *warn_str;
	const char *template;
	char *ft = NULL, *fn;
	const char *name = tpvar_str(argv[0], &fn);

	xasprintf(&err_str, "err_%s", name);
	xasprintf(&warn_str, "warn_%s", name);
	xasprintf(&err_str_payload, "err_%s_payload", name);
	data.template = NULL;

	switch(argc) {
		case 0:
			free(fn);
			return TPV_SET(dst, TPV_NULL);
		case 2:
			template = tpvar_str(argv[1], &ft);
			break;
		default: {
			struct bpapi_vtree_chain vtree = {};
			data.key = name;
			data.error = bpapi_get_element(api, err_str, 0);
			data.appl = bpapi_get_element(api, "appl", 0);

			get_settings(
				vtree_getnode(&api->vchain, &vtree, "chk_err_settings", NULL),
				"error_message",
				err_tmpl_lookup,
				err_tmpl_set_value,
				&data
			);
			vtree_free(&vtree);

			if (data.template)
				template = data.template;
			else
				template = vtree_get(&api->vchain, "common", "error", "template", NULL);
			break;
		}
	}

	buf = buf_output_init(&ba.ochain);
	BPAPI_COPY(&ba, api, 0, 1, 1);

	if (bpapi_has_key(api, err_str)) {
		bpapi_insert(&ba, "msg", bpapi_get_element(api, err_str, 0));
		bpapi_insert(&ba, "error", "1");
		if (bpapi_has_key(api, err_str_payload))
			bpapi_insert(&ba, "msg_payload", bpapi_get_element(api, err_str_payload, 0));
	} else if (bpapi_has_key(api, warn_str)) {
		bpapi_insert(&ba, "msg", bpapi_get_element(api, warn_str, 0));
		bpapi_insert(&ba, "warning", "1");
	}

	bpapi_insert(&ba, "arg", name);
	call_template_auto(&ba, template);

	if (bpapi_has_key(&ba, err_str_payload)) {
		bpapi_remove(&ba, "msg_payload");
	}

	if (bpapi_has_key(&ba, err_str)) {
		bpapi_remove(&ba, err_str);
		bpapi_remove(&ba, "error");
	}

	if (bpapi_has_key(&ba, warn_str)) {
		bpapi_remove(&ba, warn_str);
		bpapi_remove(&ba, "warning");
	}

	bpapi_remove(&ba, "arg");
	bpapi_remove(&ba, "msg");

	(void)TPV_SET(dst, buf->buf ? TPV_DYNSTR(buf->buf) : TPV_NULL);
	buf->buf = NULL;
	*cc = BPCACHE_CANT;

	bpapi_output_free(&ba);
	free(data.template);
	free(err_str);
	free(err_str_payload);
	free(warn_str);
	free(fn);
	free(ft);
	return dst;
}

ADD_TEMPLATE_FUNCTION(chk_err);
