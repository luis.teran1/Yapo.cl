#include <bpapi.h>
#include "log_event.h"

static const struct tpvar *
log_string(struct bpapi *api, struct tpvar *dst, enum bpcacheable *cc, int argc, const struct tpvar **argv) {
	const char *application;
	const char *log_string;
	char *fa, *fl;

	if (argc < 2)
		return TPV_SET(dst, TPV_NULL);

	application = tpvar_str(argv[0], &fa); 	
	log_string = tpvar_str(argv[1], &fl);

	syslog_ident(application, "%s", log_string); 
	free(fa);
	free(fl);
	*cc = BPCACHE_CANT;
	return TPV_SET(dst, TPV_NULL);
}
ADD_TEMPLATE_FUNCTION(log_string);
