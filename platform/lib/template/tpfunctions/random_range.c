#include <bpapi.h>
#include <rand.h>
#include <tpfunctions/filter_state_random.h>

static const struct tpvar *
random_range(struct bpapi *api, struct tpvar *dst, enum bpcacheable *cc, int argc, const struct tpvar **argv) {

        if (argc < 2)
                return TPV_SET(dst, TPV_NULL);

	int rmin = tpvar_int(argv[0]);
	int rmax = tpvar_int(argv[1]);
	rand_t *r = filter_state(api, "rand_state", fs_rand_get, NULL);
	uint32_t c = rand_uint32_range(r, rmax - rmin);

	*cc = BPCACHE_CANT;
	return TPV_SET(dst, TPV_INT(c + rmin));
}

ADD_TEMPLATE_FUNCTION(random_range);
