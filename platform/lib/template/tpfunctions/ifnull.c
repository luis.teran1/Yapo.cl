#include <bpapi.h>

static const struct tpvar *
ifnull(struct bpapi *api, struct tpvar *dst, enum bpcacheable *cc, int argc, const struct tpvar **argv) {
	int i;

	for (i = 0 ; i < argc ; i++) {
		if (argv[i]) {
			char *fa;
			const char *a = tpvar_str(argv[i], &fa);

			if (a && *a)
				return tpvar_rebuild(dst, a, fa);
			free(fa);
		}
	}

	return TPV_SET(dst, TPV_NULL);
}
ADD_TEMPLATE_FUNCTION(ifnull);
