#include <bpapi.h>
#include <string.h>

static const struct tpvar *
undef(struct bpapi *api, struct tpvar *dst, enum bpcacheable *cc, int argc, const struct tpvar **argv) {
	const char *a;

	if (argc != 1)
		return TPV_SET(dst, TPV_NULL);

	TPVAR_STRTMP(a, argv[0]);
	bpapi_remove(api, a);
	return TPV_SET(dst, TPV_NULL);
}

ADD_TEMPLATE_FUNCTION(undef);
