#include <bpapi.h>
#include <string.h>
#include "query_string.h"

static const struct tpvar *
t_clean_qs(struct bpapi *api, struct tpvar *dst, enum bpcacheable *cc, int argc, const struct tpvar **argv) {
	struct buf_string res = {0};
	const char *qs;

	if (argc < 1)
		return TPV_SET(dst, TPV_NULL);

	TPVAR_STRTMP(qs, argv[0]);
	
	const char *args[argc - 1];
	int i;

	for (i = 1 ; i < argc ; i++)
		TPVAR_STRTMP(args[i - 1], argv[i]);

	clean_qs(&res, qs, argc - 1, args);
	return TPV_SET(dst, res.buf ? TPV_DYNSTR(res.buf) : TPV_NULL);
}
ADD_TEMPLATE_FUNCTION_WITH_NAME(t_clean_qs, clean_qs);
