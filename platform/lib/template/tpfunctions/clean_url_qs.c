#include <bpapi.h>
#include <string.h>
#include "query_string.h"

static const struct tpvar *
clean_url_qs(struct bpapi *api, struct tpvar *dst, enum bpcacheable *cc, int argc, const struct tpvar **argv) {
	struct buf_string res = {0};
	const char *qsstart;
	char *fq;
	const char *qs;

	if (argc < 1)
		return TPV_SET(dst, TPV_NULL);
	
	qs = tpvar_str(argv[0], &fq);

	qsstart = strchr(qs, '?');
	if (!qsstart)
		return tpvar_rebuild(dst, qs, fq);

	qsstart++;
	bufwrite(&res.buf, &res.len, &res.pos, qs, qsstart - qs);

	const char *args[argc - 1];
	int i;

	for (i = 1 ; i < argc ; i++)
		TPVAR_STRTMP(args[i - 1], argv[i]);

	clean_qs(&res, qsstart, argc - 1, args);
	free(fq);
	return TPV_SET(dst, res.buf ? TPV_DYNSTR(res.buf) : TPV_NULL);
}
ADD_TEMPLATE_FUNCTION(clean_url_qs);
