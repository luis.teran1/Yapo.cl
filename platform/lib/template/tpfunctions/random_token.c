#include <bpapi.h>
#include <rand.h>
#include <tpfunctions/filter_state_random.h>

static const struct tpvar *
random_token(struct bpapi *api, struct tpvar *dst, enum bpcacheable *cc, int argc, const struct tpvar **argv) {

	if (argc < 1)
		return TPV_SET(dst, TPV_NULL);

	rand_t *r = filter_state(api, "rand_state", fs_rand_get, NULL);

	return argv[rand_uint32_range(r, argc)];
}

ADD_TEMPLATE_FUNCTION(random_token);
