#include <bpapi.h>
#include <string.h>

static const struct tpvar *
pretty_number(struct bpapi *api, struct tpvar *dst, enum bpcacheable *cc, int argc, const struct tpvar **argv) {
	int min_length = argc >= 2 ? tpvar_int(argv[1]) : 0;
	char thou_sep_char = ' ';
	char *res;

	if (argc >= 3) {
		const char *thou_sep;

		TPVAR_STRTMP(thou_sep, argv[2]);
		thou_sep_char = *thou_sep ?: ' ';
	}

	res = pretty_format_number_thousands(tpvar_int(argv[0]), min_length, thou_sep_char);
	return TPV_SET(dst, TPV_DYNSTR(res));
}

ADD_TEMPLATE_FUNCTION(pretty_number);
