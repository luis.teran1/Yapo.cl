#include <bpapi.h>

/* strips HTML tags; usage: strip_tags(text) */
static const struct tpvar *
strip_tags(struct bpapi *api, struct tpvar *dst, enum bpcacheable *cc, int argc, const struct tpvar **argv) {
	int inside, length = 0;
	const char *s;
	char *r;
	char *res;
	const char *src;
	char *fs;

	src = tpvar_str(argv[0], &fs);

	/*
	 * to avoid a lot of realloc()s, we use two passes
	 * count the non-HTML length
	 */
	for (inside = 0, s = src; *s; ++s) {
		if (inside) {
			if (*s == '>') inside = 0;
			continue;
		}
		if (*s == '<') inside = 1;
		else length++;
	}

	/* copies the string, skipping HTML */
	r = res = xmalloc(length + 1);
	r[0] = 'A';
	for (inside = 0, s = src; *s; ++s) {
		if (inside) {
			if (*s == '>') inside = 0;
			continue;
		}
		if (*s == '<') inside = 1;
		else {
			*r = *s;
			r++;
		}
	}
	res[length] = 0; /* ends the string */

	free(fs);

	return TPV_SET(dst, TPV_DYNSTR(res));
}

ADD_TEMPLATE_FUNCTION(strip_tags);
