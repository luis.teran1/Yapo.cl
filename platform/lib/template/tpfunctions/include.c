#include <bpapi.h>

static const struct tpvar *
include(struct bpapi *api, struct tpvar *dst, enum bpcacheable *cc, int argc, const struct tpvar **argv) {
	int cnt;

	for (cnt = 0 ; cnt < argc ; cnt++) {
		char *fa;
		const char *a = tpvar_str(argv[cnt], &fa);

		if (vtree_haskey(&api->vchain, "tmpl", "lang", NULL)) {
			call_template_auto(api, a);
		} else if (bpapi_has_key(api, "b__lang")) {
			/* XXX - this isn't necessary anymore, but let's not break things by cleanups. */
			call_template_lang(api, a, bpapi_get_element(api, "b__lang", 0), NULL);
		} else {
			call_template(api, a);
		}
		free(fa);
	}
	*cc = BPCACHE_CANT;
	return TPV_SET(dst, TPV_NULL);
}

ADD_TEMPLATE_FUNCTION(include);
