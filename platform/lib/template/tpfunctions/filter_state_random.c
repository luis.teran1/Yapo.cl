#include <bpapi.h>
#include <rand.h>
#include <tpfunctions/filter_state_random.h>

static void
fs_rand_free(void *v) {
	rand_t *r = v;
	rand_close(r);
}

void
fs_rand_get(struct bpapi *ba, const char *key, struct filter_state_data *fsd, void *v) {
	fsd->value = rand_open();
	fsd->type = FS_THREAD;
	fsd->freefn = fs_rand_free;
}
