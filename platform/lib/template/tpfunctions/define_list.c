#include <bpapi.h>
#include <string.h>

static const struct tpvar *
define_list(struct bpapi *api, struct tpvar *dst, enum bpcacheable *cc, int argc, const struct tpvar **argv) {
	int from;
	int to;
	int step;
	const char *var;
	int i;

	TPVAR_STRTMP(var, argv[0]);

	switch (argc) {
	case 2:
		from = 0;
		to = tpvar_int(argv[1]);
		step = 1;
		break;
	case 3:
		from = tpvar_int(argv[1]);
		to = tpvar_int(argv[2]);
		step = 1;
		break;
	case 4:
		from = tpvar_int(argv[1]);
		to = tpvar_int(argv[2]);
		step = tpvar_int(argv[3]);
		if (!step)
			return TPV_SET(dst, TPV_NULL);
		break;
	default:
		return TPV_SET(dst, TPV_NULL);
	}

	for (i = from; i < to; i += step) {
		bpapi_set_int(api, var, i);
	}

	*cc = BPCACHE_CANT;
	return TPV_SET(dst, TPV_NULL);
}
ADD_TEMPLATE_FUNCTION(define_list);
