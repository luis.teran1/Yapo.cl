#include "query_string.h"
#include "vtree.h"
#include "hash.h"
#include "ctemplates.h"
#include "settings.h"
#include "logging.h"
#include "url.h"
#include "unicode.h"

#include <pcre.h>
#include <ctype.h>
#include <string.h>
#include <alloca.h>
#include <stdio.h>

#include "query_string_setting.h"
#include "query_string_value_key.h"
#include "query_string_lookup_key.h"

enum req_utf8 get_req_utf8_vtree(struct bpapi_vtree_chain *root) {
	const char *charset = vtree_get(root, "common", "charset", NULL);

	if (charset && strcmp(charset, "UTF-8") == 0) {
		const char *fallback = vtree_get(root, "common", "charset_fallback", NULL);

		if (fallback && strcmp(fallback, "latin1") == 0)
			return RUTF8_FALLBACK_LATIN1;
		else
			return RUTF8_REQUIRE;
	}
	return RUTF8_NOCHECK;
}

enum req_utf8 get_req_utf8_bconf(struct bconf_node *root) {
	const char *charset = bconf_get_string(root, "common.charset");

	if (charset && strcmp(charset, "UTF-8") == 0) {
		const char *fallback = bconf_get_string(root, "common.charset_fallback");

		if (fallback && strcmp(fallback, "latin1") == 0)
			return RUTF8_FALLBACK_LATIN1;
		else
			return RUTF8_REQUIRE;
	}
	return RUTF8_NOCHECK;
}

char *
clean_qs(struct buf_string *dst, const char *src, int numclean, const char **clean) {
	const char *str;
	const char *key;
	const char *start;
	int firstpos;
	int i;

	if (!src || !dst || numclean < 0)
		return NULL;

	firstpos = dst->pos;
	start = key = str = src;
	while (*str) {
		switch (*str) {
		case '&':
			if (start && start < str)
				bufwrite(&dst->buf, &dst->len, &dst->pos, start, str - start);
			start = str;
			if (strncmp(str + 1, "amp;", 4) == 0)
				str += 4;
			key = str + 1;
			if (firstpos == dst->pos || *key == '&' || *key == '\0')
				start = key;
			break;
		case '=':
			if (start && str > key) {
				for (i = 0; i < numclean; i++) {
					if (clean[i][0] != '\0' && strncmp(key, clean[i], str - key) == 0 && clean[i][str - key] == '\0') {
						/* Match, filter out */
						start = NULL;
						break;
					}
				}
			}
			break;
		}
		str++;
	}
	if (start && start < str)
		bufwrite(&dst->buf, &dst->len, &dst->pos, start, str - start);

	return dst->buf;
}

static void
qs_sq_parse_qs(struct parse_cb_data *pqcb_data, char *key, int klen, char *value, int vlen) {
	struct qs_sq_data *data = pqcb_data->cb_data;

	if (!vtree_getint(data->vtree, data->rootpath, "vars", key, "multiple", NULL))
		bpapi_remove(&data->ba, key);

	if (data->parse_qs_cb) {
		char *k, *v;

		if (klen < 1024)
			k = alloca(klen * 2 + 1);
		else
			k = xmalloc(klen * 2 + 1);
		memcpy(k, key, klen + 1);

		if (vlen < 1024)
			v = alloca(vlen * 2 + 1);
		else
			v = xmalloc(vlen * 2 + 1);
		memcpy(v, value, vlen + 1);

		data->parse_qs_cb(data->cb_data, k, klen, klen * 2 + 1, v, vlen, vlen * 2 + 1);
		bpapi_insert(&data->ba, k, v);

		if (klen >= 1024)
			free(k);
		if (vlen >= 1024)
			free(v);
	} else {
		bpapi_insert(&data->ba, key, value);
	}

	if (qs_sq_parse_qs_local)
		qs_sq_parse_qs_local(pqcb_data, key, value);
}

static const char *
qs_sq_key_lookup (const char *setting, const char *key, void *cbdata) {
	struct qs_sq_data *data = cbdata;
	const char *eq = NULL;
	const char *v;
	const char *col = NULL;
	enum query_string_lookup_key keyval = QSLK_NONE;

	if (key[0] == '?')
		v = bpapi_has_key(&data->ba, key + 1) ? "true" : "false";
	else if (key[0] == '!') {
		/* Check if undefined or empty */
		const char *val = bpapi_get_element(&data->ba, key + 1, 0);
		if (val && *val)
			return "false";
		return "true";
	} else if (key[0] == '=' && (eq = strstr (key, "=="))) {
		char *k = strndupa(key + 1, eq - key - 1);
		int cnt = bpapi_length(&data->ba, k);
		const char *val = bpapi_get_element(&data->ba, k, 0);
		if (cnt > 1)
			v = "false";
		else
			v = val && strcmp(val, eq + 2) == 0 ? "true" : "false";
	} else if ((col = strchr(key, ':')) && (keyval = lookup_query_string_lookup_key(key, col - key)) != QSLK_NONE) {
		col++;
		v = NULL;
		switch (keyval) {
		case QSLK_ANY:
			eq = strstr(col, "==");
			if (eq) {
				char *k = strndupa(col, eq - col);
				int cnt = bpapi_length(&data->ba, k);
				int i;

				v = "false";
				for (i = 0 ; i < cnt ; i++) {
					const char *val = bpapi_get_element(&data->ba, k, i);

					if (val && strcmp(val, eq + 2) == 0) {
						v = "true";
						break;
					}
				}
			}
			break;
		case QSLK_IN_ARRAY:
			eq = strstr(col, "=");
			if (eq) {
				char *k = strndupa(col, eq - col);
				int cnt = bpapi_length(&data->ba, k);
				int i;

				v = "true";
				for (i = 0 ; i < cnt ; i++) {
					const char *val = bpapi_get_element(&data->ba, k, i);
					if (val && strstrptrs(eq + 1, val, NULL, ",") == NULL) { 
						v = "false";
						break;
					}
				}
			}
			break;
		case QSLK_MAPEXIST:
			{
				const char *k = strchr(col, ':');
				if (k) {
					char *map = strndupa(col, k++ - col);
					const char *val = bpapi_get_element(&data->ba, k, 0);
					const char *mval = val ? vtree_get(&data->ba.vchain, data->rootpath, "map", map, val, NULL) : NULL;

					if (mval && *mval)
						v = "true";
					else
						v = "false";
				}
			}
			break;
		case QSLK_CMAPEXIST:
			{
				const char *k = strchr(col, ':');
				if (k) {
					char *map = strndupa(col, k++ - col);
					const char *val = bpapi_get_element(&data->ba, k, 0);
					const char *mval = val ? vtree_get(&data->ba.vchain, "common", map, val, NULL) : NULL;

					if (mval && *mval)
						v = "true";
					else
						v = "false";
				}
			}
			break;

		case QSLK_NONE: /* Satisfy warning. */
			break;
		}
	} else {
		v = bpapi_get_element(&data->ba, key, 0);
		if ((v && !*v))
		    v = NULL;
	}
	/*xwarnx ("qs_sq_key_lookup (%s, %s) => %s", setting, key, v);*/
	return v;
}

static void
qs_sq_set_value(const char *setting, const char *key, const char *value, void *cbdata) {
	struct qs_sq_data *data = cbdata;
	const char *val = NULL;
	int ifnotempty = 0;
	int freeval = 0;
	enum query_string_setting setval = lookup_query_string_setting(setting, strlen(setting));
	enum query_string_value_key keyval = lookup_query_string_value_key(key, strlen(key));

	/* log_printf(LOG_DEBUG, "qs_sq_set_value(setting:%s, key:%s, value:%s)", setting, key, value); */

	switch (keyval) {
	case QSVK_MULTI:
		/* Reset old multi val */
		if (data->multi)
			data->multi_buf.pos = 0;
		data->multi = 1;
		break;
	case QSVK_VALUE:
		val = value;
		break;
	case QSVK_QS: {
		int cnt = bpapi_length(&data->ba, value);
		if (cnt > 1) {
			int i;
			struct buf_string str = {0};
			for (i = 0; i < cnt; i++) {
				const char *vv = bpapi_get_element(&data->ba, value, i);

				if (i){
					bufwrite(&str.buf, &str.len, &str.pos, ",", 1);
					bufwrite(&str.buf, &str.len, &str.pos, data->multi_buf.buf, data->multi_buf.pos);
				}
				if (vv)
					bufwrite(&str.buf, &str.len, &str.pos, vv, strlen(vv));
			}

			if (str.len)
				val = strdupa(str.buf);
			if (str.buf)
				free(str.buf);

			/* disable multi val buffer */
			data->multi = 0;
			data->multi_buf.pos = 0;
		} else
			val = bpapi_get_element(&data->ba, value, 0);
		break;
	}
	case QSVK_IMPLODE: {
		const char *v = strchr(value, ':');

		val = NULL;
		if (v) {
			char *k = strndupa(value, v - value);
			int cnt = bpapi_length(&data->ba, k);
			int i;
			struct buf_string str = {0};

			for (i = 0; i < cnt; i++) {
				const char *vv = bpapi_get_element(&data->ba, k, i);

				if (vv) {
					if (i)
						bufcat(&str.buf, &str.len, &str.pos, "%s%s", v + 1, vv);
					else
						bufwrite(&str.buf, &str.len, &str.pos, vv, strlen(vv));
				}
			}
			if (str.buf) {
				val = strdupa(str.buf);
				free(str.buf);
			}
		}
		break;
	}
	case QSVK_IMPLODEMAP: {
		const char *ks = strchr (value, ':');

		val = NULL;
		if (ks) {
			char *map = strndupa (value, ks - value);
			const char *v = strchr (++ks, ':');
			
			if (v) {
				char *k = strndupa (ks, v - ks);
				int cnt = bpapi_length(&data->ba, k);
				int i;
				struct buf_string str = {0};

				for (i = 0; i < cnt; i++) {
					const char *vv = bpapi_get_element(&data->ba, k, i);
					struct buf_string *buf = BPAPI_OUTPUT_DATA(&data->ba);

					/* XXX name collisions */
					bpapi_insert(&data->ba, "rootpath", data->rootpath);
					bpapi_insert(&data->ba, "map", map);
					bpapi_insert(&data->ba, "value", vv);
					if (!call_template(&data->ba, "qs/qs_sq_translate.txt")) {
						syslog(LOG_ERR, "Nonexistent template qs/qs_sq_translate.txt");
					}
					bpapi_remove(&data->ba, "map");
					bpapi_remove(&data->ba, "value");
					bpapi_remove(&data->ba, "rootpath");

					if (buf->buf && *buf->buf) {
						char *ep = buf->buf + strlen(buf->buf) - 1;

						while (ep >= buf->buf && isspace(*ep))
							*ep-- = '\0';
						val = buf->buf;
						buf->buf = NULL;
						buf->pos = buf->len = 0;
						if (i)
							bufcat(&str.buf, &str.len, &str.pos, "%s%s", v + 1, val);
						else
							bufwrite(&str.buf, &str.len, &str.pos, val, strlen(val));
					}
				}
				if (str.buf) {
					val = str.buf;
					freeval = 1;
				}
			}
		}
		break;
	}
	case QSVK_SEP:
		if (value && *value)
			val = value;
		else
			val = ", ";
		ifnotempty = 1;
		break;
	case QSVK_MAP: {
		const char *v = strchr(value, ':');

		val = NULL;
		if (v) {
			const char *bv = bpapi_get_element(&data->ba, v + 1, 0);

			if (bv) {
				char *map = strndupa(value, v - value);
				struct buf_string *buf = BPAPI_OUTPUT_DATA(&data->ba);

				/* XXX name collisions */
				bpapi_insert(&data->ba, "rootpath", data->rootpath);
				bpapi_insert(&data->ba, "map", map);
				bpapi_insert(&data->ba, "value", bv);
				if (!call_template(&data->ba, "qs/qs_sq_translate.txt")) {
					syslog(LOG_ERR, "Nonexistent template qs/qs_sq_translate.txt");
				}
				bpapi_remove(&data->ba, "map");
				bpapi_remove(&data->ba, "value");
				bpapi_remove(&data->ba, "rootpath");

				if (buf->buf && *buf->buf) {
					char *ep = buf->buf + strlen(buf->buf) - 1;

					while (ep >= buf->buf && isspace(*ep))
						*ep-- = '\0';
					val = buf->buf;
					freeval = 1;
					buf->buf = NULL;
					buf->pos = buf->len = 0;
				}
			}
		}
		break;
	}
	case QSVK_COUNT_FILTER:
	case QSVK_COUNT_FILTER_CONST: {
		const char *v = strchr(value, ':');

		if (v) {
			/* Insert a count_filter in ba */
			const char *vv;
			if (keyval == QSVK_COUNT_FILTER)
				vv = bpapi_get_element(&data->ba, v + 1, 0);
			else
				vv = v + 1;
			if (vv && *vv) {
				int kvbufsz = (v - value + strlen(vv) + 2) * 2;
				int kvlen;
				char *kv = alloca(kvbufsz);
				int res;
				char *valbuf;

				kvlen = snprintf(kv, kvbufsz, "%.*s:%s", (int)(v - value), value, vv);

				bpapi_insert(&data->ba, "count_filter", kv);
				bpapi_insert_maxsize(&data->ba, "count_filter_key", value, v - value);
				ALLOCA_PRINTF(res, valbuf, " filter:%s ", kv);
				val = valbuf;
				if (data->parse_qs_cb) {
					char k[sizeof("count_filter") * 2] = "count_filter";
					data->parse_qs_cb(data->cb_data, k, sizeof("count_filter") - 1, sizeof("count_filter") * 2, kv, kvlen, kvbufsz);
				}
			}
		}
		break;
	}
	case QSVK_TMPL: {
		struct buf_string *buf = BPAPI_OUTPUT_DATA(&data->ba);

		call_template_lang(&data->ba, value, data->lang, data->country);
/* 		warnx("calling template %s => %s", value, buf->buf); */

		if (buf->buf) {
			char *ep = buf->buf + strlen(buf->buf) - 1;

			while (ep >= buf->buf && isspace(*ep))
				*ep-- = '\0';
			val = buf->buf;
			freeval = 1;
			buf->buf = NULL;
			buf->pos = buf->len = 0;
		}
		break;
	}
	case QSVK_NONE:
		val = NULL;
		break;
	}

	if (!val)
		return;

	int vlen = strlen(val);

	if (data->multi) {
		bufwrite(&data->multi_buf.buf, &data->multi_buf.len, &data->multi_buf.pos, val, vlen);
	}

	struct buf_string *bufp = NULL;
	switch (setval) {
	case QSS_OFFSET:
		data->offset = atoi(val);
		break;
	case QSS_LIMIT:
		data->limit = atoi(val);
		break;
	case QSS_COUNT_LEVEL:
		bufp = &data->count_level;
		break;
	case QSS_FILTER:
		bufp = &data->filter;
		break;
	case QSS_UNFILTERED:
		bufp = &data->unfiltered;
		break;
	case QSS_PARAMS:
		bufp = &data->params;
		break;
	case QSS_QUERY:
		bufp = &data->query;
		break;
	case QSS_DESC:
		bufp = &data->desc;
		break;
	case QSS_NONE:
		break;
	}

	if (bufp) {
		if (ifnotempty) {
			if (bufp->buf && bufp->buf[0]) {
				int len = strlen(bufp->buf);

				if (len < vlen || strcmp(&bufp->buf[len - vlen], val) != 0)
					bufwrite(&bufp->buf, &bufp->len, &bufp->pos, val, vlen);
			}
		} else
			bufwrite(&bufp->buf, &bufp->len, &bufp->pos, val, vlen);
	}
	if (freeval)
		free((char*)val);
}

static const char *
cat_key_lookup(const char *setting, const char *key, void *cbdata) {
	struct qs_sq_data *data = cbdata;
	/* Are there other keys that aren't in the bpapi? */
	const char *cat;
	int ret_tf;
	const char *value = NULL;

	if (key[0] == '?') {
		ret_tf = 1;
		key++;
	} else if (key[0] == '!') {
		ret_tf = 2;
		key++;
	} else {
		ret_tf = 0;
	}

	if (bpapi_length(&data->ba, "c"))
		cat = bpapi_get_element(&data->ba, "c", 0);
	else
		cat = bpapi_get_element(&data->ba, "cg", 0);

	if (strcmp(key, "category") == 0) {
		value = cat;
	} else if (strcmp(key, "parent") == 0) {
		if (!cat) {
			value = NULL;
		} else {
			/* If only cg is sent and the category is a main category then thats the parent */
			/* XXX this code is backwards, but leaving it here for now in case someone depends on it anyway */
			if (bpapi_length(&data->ba, "c")) {
				const char *level = vtree_get(data->vtree, "cat", cat, "level", NULL);
				if (level && atoi(level) == 0)
					value = cat;
			}

			if (!value)
				value = vtree_get(data->vtree, "cat", cat, "parent", NULL);
		}
	} else {
		value = bpapi_get_element(&data->ba, key, 0);
	}

	switch (ret_tf) {
	case 1:
		value = value ? "true" : "false";
		break;
	case 2:
		value = value && *value ? "false" : "true";
		break;
	}

	return value;
}

static void list_set_value(const char *setting, const char *key, const char *value, void *cbdata) {
	struct qs_sq_data *data = cbdata;

	if (strcmp("searchextras", setting) == 0) {
		if (strcmp(key, "types") == 0 && value && strcmp(value, "all") == 0) {
			bpapi_insert(&data->ba, "st", "a");
		}
	}
}

static void cat_set_value(const char *setting, const char *key, const char *value, void *cbdata) {
	struct qs_sq_data *data = cbdata;

	if (strcmp("types", setting) == 0) {
		struct bpapi_loop_var types;
		int i;

		vtree_fetch_values(data->vtree, &types, "common", "default", "type", VTREE_LOOP, NULL);

		for (i = 0; i < types.len; i++) {
			if (strcmp(key, types.l.list[i]) == 0) {
				bpapi_insert(&data->ba, "st", key);
			}
		}
		if (types.cleanup)
			types.cleanup(&types);
	} else if (strcmp(setting, "search_default") == 0) {
		if (strcmp(key, "type") == 0)
			bpapi_insert(&data->ba, "st", value);
	}
}

/* Bind variables that don't directly come from the query string but are needed prior to doing qs settings lookup */
static void 
query_string_search_query_variable_bind(const char * appl, struct qs_sq_data * data) {
	const char *ca = bpapi_get_element(&data->ba, "ca", 0);
	if (ca && !*ca)
		ca = NULL;
	const char *w = bpapi_get_element(&data->ba, "w", 0);

	if (w && !*w)
		w = NULL;

	/*warnx ("ca = %s, w = %s", ca, w);*/
	/* Insert w if missing */
	if (!w) {
		/* Compat, if no ca and no w then show whole country, otherwise w defaults to region */
		if (!ca) { 
			bpapi_insert(&data->ba, "w", "3");
		}
		else if (count_chars(ca, '_') == 2) {
			bpapi_insert(&data->ba, "w", "0");
		} else if (count_chars(ca, '_') == 1) {
			char *c = strchr(ca, '_');
			if (c && *c && atoi(c+1)) {
				bpapi_insert(&data->ba, "w", "0");
			} else {
				bpapi_insert(&data->ba, "w", "1");				
			}
		} else {
			bpapi_insert(&data->ba, "w", "1");
		}
	}

	if (!ca) 
		ca = vtree_get(data->vtree, "common", "default", "caller", NULL) ?: "";

	/* Insert region */
	const char *rs = strchr(ca, '_');
	if (rs == NULL)
		rs = ca + strlen(ca);
	char *reg = NULL;

	bpapi_insert_maxsize(&data->ba, "ca_region", ca, rs - ca);

	/* Special w > 100 case means (w - 100) equals region to search in. */
	if (w && atoi(w) > 100) {
		int wi = 1;

		/* Skip leading zeros. */
		while (w[wi] == '0')
			wi++;

		reg = xstrdup (w + wi);
	} else {
 		reg = xstrndup (ca, rs - ca);
		if (!vtree_haskey (data->vtree, "common", "region", reg, NULL)) {
			free (reg);
			reg = xstrdup (vtree_get (data->vtree, "common", "default_region", NULL) ?: "");
		}
	}
	bpapi_insert(&data->ba, "region", reg);

	/* Insert city */
	if (w && atoi(w) > 100) {
		char *city;

		if ((city = strchr(w, ':'))) {
			city++;
			if (vtree_haskey (data->vtree, "common", "region", reg, "cities", city, NULL)) {
				bpapi_insert (&data->ba, "ci", city);
				rs = NULL; /* Disable check below. */
			}				
		}
	}
	if (rs && *rs) {
		char *st = strchr(++rs, '_');
		char *cs;

		if (st && *st) {
			cs = xstrndup(rs, st-rs);
		} else {
			cs = xstrdup(rs);
		}
		if (vtree_haskey(data->vtree, "common", "region", reg, "cities", cs, NULL)) {
			bpapi_insert(&data->ba, "city", cs);
		} else if (vtree_haskey(data->vtree, "common", "region", reg, "subregion", cs, NULL)) {
			bpapi_insert(&data->ba, "subregion", cs);
		}		    
		free(cs);
	}

	free(reg);
	bpapi_insert(&data->ba, "appl", appl);

	/* Insert type */
	if (! bpapi_has_key(&data->ba, "st")) {
		/* Backward compat */
		if (ca[strlen(ca) - 1] == 'k') {
			bpapi_insert(&data->ba, "st", "k");
			bpapi_insert(&data->ba, "type", "k");
		} else if (ca[strlen(ca) - 1] == 's') {
			/* XXX Argh, ugly, but so is this whole function. */
			struct bpapi_vtree_chain vtree = {0};
			get_settings(vtree_getnode(&data->ba.vchain, &vtree, "category_settings", NULL), NULL, cat_key_lookup, cat_set_value, data);
			vtree_free(&vtree);

			if (!bpapi_has_key(&data->ba, "st"))
				bpapi_insert (&data->ba, "st", "s");
			bpapi_insert (&data->ba, "type", "s");
		} else if (ca[strlen(ca) - 1] == 'a') {
			bpapi_insert(&data->ba, "st", "a");
			bpapi_insert(&data->ba, "type", "a");
		} else {
			/* End of backward compat */
			if (bpapi_length(&data->ba, "cg" )) {
				struct bpapi_vtree_chain vtree = {0};
				/* Check for types:all in search search extras */
				get_settings(vtree_getnode(&data->ba.vchain, &vtree, "list_settings", NULL), NULL, cat_key_lookup, list_set_value, data);
				vtree_free(&vtree);
				if (!bpapi_has_key(&data->ba, "st")) {
					/* check the category's types.
					   Filter these types against what our default types usually are. take the interseaction, */
					get_settings(vtree_getnode(&data->ba.vchain, &vtree, "category_settings", NULL), NULL, cat_key_lookup, cat_set_value, data);
					vtree_free(&vtree);
				}
			} else {
				/* Load all the default types */
				struct bpapi_loop_var types;
				int i;

				vtree_fetch_values(data->vtree, &types, "common", "default", "type", VTREE_LOOP, NULL);
				for (i = 0; i < types.len; i++) {
					bpapi_insert(&data->ba, "st", types.l.list[i]);
				}
				if (types.cleanup)
					types.cleanup(&types);
			}
		}
	} else {
		/* backward compat */
		const char *st = bpapi_get_element(&data->ba, "st", 0);

		if (vtree_haskey(&data->ba.vchain, "common", "type", "list", st, NULL))
			bpapi_insert(&data->ba, "type", st);
		else
			bpapi_insert(&data->ba, "type", (vtree_get(&data->ba.vchain, "common", "default", "type", "0", NULL) ?: ""));
	}

	/* XXX Ugly */
	if (data->lang)
		bpapi_insert(&data->ba, "b__lang", data->lang);
}

void
query_string_search_query(const char *appl, const char *query_string, struct qs_sq_data *data) {
	char *qs;
	struct bpapi_vtree_chain vars = {0};

	qs = xstrdup(query_string);

	buf_output_init(&data->ba.ochain);
	hash_simplevars_init(&data->ba.schain);
	data->ba.vchain = *data->vtree;

	if (!data->rootpath)
		data->rootpath = "qs";

	parse_query_string(qs, qs_sq_parse_qs, data, vtree_getnode(data->vtree, &vars, data->rootpath, "vars", NULL), 0, data->req_utf8, NULL);
	free(qs);
	vtree_free(&vars);

	/* Bind variables to the bpapi which aren't directly taken from the query string */ 
	query_string_search_query_variable_bind(appl, data) ;
	if ( query_string_search_query_variable_bind_local ) {
		query_string_search_query_variable_bind_local(appl, data) ;
	}

	struct bpapi_vtree_chain vtree = {0};
	get_settings(vtree_getnode(&data->ba.vchain, &vtree, data->rootpath, "settings", NULL), NULL, qs_sq_key_lookup, qs_sq_set_value, data);
	vtree_free(&vtree);

	bpapi_output_free(&data->ba);
	bpapi_simplevars_free(&data->ba);

	if (data->query.buf) {
		/* Clean join from query string */
		char *join;
		while ( (join = stristrptrs(data->query.buf, "join", NULL, " +-;|,\t\r\n\v")) != NULL) {
			int i;
			for ( i = 0; i < 4 ; i++)
				*join++ = ' ';
		}
	}
}

void
free_qs_sq(struct qs_sq_data *data) {
	if (data->count_level.buf)
		free(data->count_level.buf);
	if (data->filter.buf)
		free(data->filter.buf);
	if (data->unfiltered.buf)
		free(data->unfiltered.buf);
	if (data->params.buf)
		free(data->params.buf);
	if (data->query.buf)
		free(data->query.buf);
	if (data->desc.buf)
		free(data->desc.buf);
	if (data->multi_buf.buf)
		free(data->multi_buf.buf);
}

