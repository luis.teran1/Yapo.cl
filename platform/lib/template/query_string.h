#ifndef QUERY_STRING_H
#define QUERY_STRING_H

#include "util.h"
#include "bpapi.h"
#include "bconf.h" /* needed for tristate enum. */
#include "parse_query_string.h"

struct qs_sq_data {
	/* In fields */
	struct bpapi_vtree_chain *vtree;
	const char *lang;
	const char *country;
	const char *rootpath;
	enum req_utf8 req_utf8;

	/*
	 * You are responsible for UTF-8 decoding key and value destructivly.
	 * The max buffersize is given in kbufsz and vbufsz
	 */
	void (*parse_qs_cb)(void *cb_data, char *key, int klen, int kbufsz, char *value, int vlen, int vbufsz);
	void *cb_data;

	/* Multiple valie buffer */
	int multi;
	struct buf_string multi_buf;


	/* Out fields, should be zero initialized */
	int offset;
	int limit;
	struct buf_string count_level;
	struct buf_string filter;
	struct buf_string unfiltered;
	struct buf_string params;
	struct buf_string query;
	struct buf_string desc;
	
	/* Internal fields */
	struct bpapi ba;
};

/* Point these to the normal bconf root (*.*) */
enum req_utf8 get_req_utf8_vtree(struct bpapi_vtree_chain *root);
enum req_utf8 get_req_utf8_bconf(struct bconf_node *root);

char *clean_qs(struct buf_string *dst, const char *src, int numclean, const char **clean);

void query_string_search_query (const char *appl, const char *query_string, struct qs_sq_data *data);
void free_qs_sq (struct qs_sq_data *data);
void query_string_search_query_variable_bind_local (const char *appl, struct qs_sq_data * data) WEAK ;

void qs_sq_parse_qs_local(struct parse_cb_data *cb_data, char *key, char *value) WEAK;

#endif /*QUERY_STRING_H*/
