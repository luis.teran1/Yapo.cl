#ifndef BPARSE_H
#define BPARSE_H

#include <stdlib.h>
#include <linker_set.h>
#include <util.h>
#include <tree.h>
#include <string.h>

#include "macros.h"

#include "tptypes.h"
#include "vtree.h"
#include "filter_state.h"

struct templf {
	const char *name;
	const struct tpvar *(*template_func)(struct bpapi*, struct tpvar *dst, enum bpcacheable *cc, int argc, const struct tpvar **argv);
};

/*
 * struct bpapi is used to keep track of variables (simplevars and
 * vtrees) used by templates. Function pointers are used to make it
 * possible to have multiple implementations of the interface defined
 * here. There are three main components of this struct:
 *
 * - struct bpapi_outputchain ochain
 * - struct bpapi_simplevars_chain schain
 * - struct bpapi_vtree_chain vtree
 *
 * The accessor macros defined below should be used when struct bpapi
 * is used.
 */
struct bpapi {
	struct bpapi_output_chain {
		const struct bpapi_output {
			int (*outstring_fmt)(struct bpapi_output_chain *chain, const char *fmt, ...) __attribute__((format (printf, 2, 3)));
#define bpo_outstring_fmt(ochain, ...) (ochain)->fun->outstring_fmt(ochain, __VA_ARGS__)
#define bpapi_outstring_fmt(api, ...) bpo_outstring_fmt(&(api)->ochain, __VA_ARGS__)
			int (*outstring_raw)(struct bpapi_output_chain *chain, const char *str, size_t len);
#define bpo_outstring_raw(ochain, str, len) (ochain)->fun->outstring_raw(ochain, str, len)
#define bpapi_outstring_raw(api, str, len) bpo_outstring_raw(&(api)->ochain, str, len)
			void (*free)(struct bpapi_output_chain *chain);
#define bpo_output_free(ochain) ((ochain)->fun && (ochain)->fun->free ? (ochain)->fun->free(ochain) : (void)0)
#define bpapi_output_free(api) bpo_output_free(&(api)->ochain)
			void (*flush)(struct bpapi_output_chain *chain);
#define bpo_flush(ochain) ((ochain)->fun->flush ? (ochain)->fun->flush(ochain) : (void)0)
#define bpapi_flush(api) bpo_flush(&(api)->ochain)
		} *fun;
		void *data;
#define BPAPI_OUTPUT_DATA(api) (api)->ochain.data
		struct bpapi_output_chain *next;
	} ochain;

	/*
	 * struct bpapi_simplevars_chain keeps track of the
	 * simplevars.
	 */
	struct bpapi_simplevars_chain {
		const struct bpapi_simplevars {
			/*
			 * Check if variable 'key' exists. 
			 * Return 0 iff key does not exist.
			 */
			int (*has_key)(struct bpapi_simplevars_chain *, const char*);
#define bps_has_key(schain, key) (schain)->fun->has_key(schain, key)
#define bpapi_has_key(api, key) bps_has_key(&(api)->schain, key)

			/* Get number of elements in the variables named 'key'. */
			int (*length)(struct bpapi_simplevars_chain *, const char*);
#define bps_length(schain, key) (schain)->fun->length(schain, key)
#define bpapi_length(api, key) (api)->schain.fun->length(&(api)->schain, key)

			/* Get element 'pos' in the variable named 'key'. */
			const char* (*get_element)(struct bpapi_simplevars_chain *, const char*, int pos);
#define bps_get_element(schain, key, pos) (schain)->fun->get_element(schain, key, pos)
#define bpapi_get_element(api, key, pos) bps_get_element(&(api)->schain, key, pos)

			/*
			 * Fetch all elements in the variable
			 * 'key'. The elements are stored in 'loop'.
			 */
			void (*fetch_loop)(struct bpapi_simplevars_chain *, const char*, struct bpapi_loop_var *loop);
#define bps_fetch_loop(schain, key, loopvar) (schain)->fun->fetch_loop(schain, key, loopvar)
#define bpapi_fetch_loop(api, key, loopvar) (api)->schain.fun->fetch_loop(&(api)->schain, key, loopvar)

			/*
			 * Count number of variable names that match
			 * the glob 'prefix'.
			 */
			int (*keyglob_len)(struct bpapi_simplevars_chain *, const char*);
#define bps_keyglob_len(schain, prefix) (schain)->fun->keyglob_len(schain, prefix)
#define bpapi_keyglob_len(api, prefix) bps_keyglob_len(&(api)->schain, prefix)

			/*
			 * Fetch variable names matching the glob
			 * 'prefix'. The variable names are stored in
			 * 'loop'.
			 */
			void (*fetch_glob_loop)(struct bpapi_simplevars_chain *, const char*, struct bpapi_loop_var *loop);
#define bps_fetch_glob_loop(schain, prefix, loopvar) (schain)->fun->fetch_glob_loop(schain, prefix, loopvar)
#define bpapi_fetch_glob_loop(api, prefix, loopvar) bps_fetch_glob_loop(&(api)->schain, prefix, loopvar)
			void (*insert)(struct bpapi_simplevars_chain *, const char*, int, const char*, int);
#define bps_insert(schain, key, val) (schain)->fun->insert(schain, key, -1, val, -1)
#define bps_insert_buf(schain, key, klen, val, vlen) (schain)->fun->insert(schain, key, klen, val, vlen)
#define bpapi_insert(api, key, val) bps_insert(&(api)->schain, key, val)
#define bpapi_insert_buf(api, key, klen, val, vlen) bps_insert_buf(&(api)->schain, key, klen, val, vlen)
#define bpapi_insertf(api, key, ...) { \
	char *__s; \
	xasprintf(&__s, __VA_ARGS__); \
	bps_insert(&(api)->schain, key, __s); \
	free(__s); \
}
#define bps_has_insert(schain) ((schain)->fun->insert != NULL)
#define bpapi_has_insert(api) bps_has_insert(&(api)->schain)
			void (*insert_list)(struct bpapi_simplevars_chain *, const char*, const char**, int);
#define bps_insert_list(schain, key, vallist, len) (schain)->fun->insert_list(schain, key, vallist, len)
#define bpapi_insert_list(api, key, vallist, len) bps_insert_list(&(api)->schain, key, vallist, len)
			void (*insert_maxsize)(struct bpapi_simplevars_chain *, const char*, const char*, int maxsize);
#define bps_insert_maxsize(schain, key, val, maxlen) (schain)->fun->insert_maxsize(schain, key, val, maxlen)
#define bpapi_insert_maxsize(api, key, val, maxlen) bps_insert_maxsize(&(api)->schain, key, val, maxlen)
			void (*remove)(struct bpapi_simplevars_chain *, const char*);
#define bps_remove(schain, key) (schain)->fun->remove(schain, key)
#define bpapi_remove(api, key) bps_remove(&(api)->schain, key)
#define bps_has_remove(schain) ((schain)->fun->remove != NULL)
#define bpapi_has_remove(api) bps_has_remove(&(api)->schain)
			void (*free)(struct bpapi_simplevars_chain *);
#define bps_free(schain) ((schain)->fun && (schain)->fun->free ? (schain)->fun->free(schain) : (void)0)
#define bpapi_simplevars_free(api) bps_free(&(api)->schain)
			void (*init_var)(struct bpapi_simplevars_chain *, const char*, struct template_var*);
#define bps_init_var(schain, key, tmplvar) ((schain)->fun->init_var ? (schain)->fun->init_var(schain, key, tmplvar) : (void)0)
#define bpapi_init_var(api, key, tmplvar) bps_init_var(&(api)->schain, key, tmplvar)
		} *fun;
		void *data;
		struct bpapi_simplevars_chain *next;
#define BPAPI_SIMPLEVARS_DATA(api) (api)->schain.data

		/*
		 * Cache generation, checked for validity of negative caching.
		 */
		int cache_gen;
	} schain;

	struct bpapi_vtree_chain vchain;
#define bpapi_vtree_free(api) bpv_free(&(api)->vchain)
#define BPAPI_VTREE_DATA(api) (api)->vchain.data

	void *(*filter_state)(struct bpapi *, const char *, filter_state_cb_t, void *);

	/* Somewhere to stash bpapi initialization information to aid debugging. */
	struct {
		const char *source_file;
		int source_line;
	} debug;
};

#define BPAPI_COPY(n, o, co, cs, cv) do { \
		if (co) (n)->ochain = (o)->ochain; \
		if (cs) (n)->schain = (o)->schain; \
		if (cv) (n)->vchain = (o)->vchain; \
		(n)->filter_state = (o)->filter_state; \
		(n)->debug.source_line = __LINE__; \
		(n)->debug.source_file = __FILE__; \
	} while (0)

typedef enum {NE, EQ, GT, GTE, LT, LTE} oper_t;

int bpapi_get_int(struct bpapi*, const char*);
void bps_set_int(struct bpapi_simplevars_chain *, const char*, int);
#define bpapi_set_int(api, key, val) bps_set_int(&(api)->schain, key, val)
void bps_set_char(struct bpapi_simplevars_chain *, const char*, char);
#define bpapi_set_char(api, key, val) bps_set_char(&(api)->schain, key, val)
void bps_set_printf(struct bpapi_simplevars_chain *, const char *key, const char *fmt, ...) __attribute__((format (printf, 3, 4)));
#define bpapi_set_printf(api, key, ...) bps_set_printf(&(api)->schain, key, __VA_ARGS__)
void bps_set_hex(struct bpapi_simplevars_chain *, const char*, const void *data, size_t len);
#define bpapi_set_hex(api, key, data, len) bps_set_hex(&(api)->schain, key, data, len)

void bpapi_free(struct bpapi*);

struct templ {
	const char *name;
	void (*tmpl)(struct bpapi *);
	void (*clear_cache)(void);
};

struct bpfilter {
	const char *name;
	int (*init)(const struct bpfilter*, struct bpapi*, int, const struct tpvar **);
	int (*loop)(struct bpapi *);
	int (*loop_length)(struct bpapi *);

	const struct bpapi_output *output;
	size_t output_sz;
	const struct bpapi_simplevars *simplevars;
	size_t simplevars_sz;
	const struct bpapi_vtree *vtree;
	size_t vtree_sz;
};

struct bpfunction_result {
	struct bpfunction_result *next;
	char *value;
};

int call_template_lang(struct bpapi*, const char* template_name, const char* lang, const char *country);
int call_template(struct bpapi*, const char* template_name);
int call_template_auto(struct bpapi*, const char* template_name);

void template_timers_ctl(int enable);

typedef void (*template_func)(struct bpapi*, int argc, char **argv);
typedef void (*flush_cache_func)(void);

/* XXX flush_template_cache is not thread safe. */
void flush_template_cache(const char *name); 
void flush_template_list(void);

int self_outstring_fmt(struct bpapi_output_chain *ochain, const char *fmt, ...);

int bufstring_outstring_fmt(struct bpapi_output_chain *ochain, const char *fmt, ...);
int bufstring_outstring_raw(struct bpapi_output_chain *ochain, const char *str, size_t len);

#ifdef BPAPI_GLOBAL_SYMBOLS
#define TEMPLATE_VISIBILITY
#else
#define TEMPLATE_VISIBILITY VISIBILITY_HIDDEN
#endif

#define TEMPLATE_WEAK __attribute__((__weak__))

#define ADD_TEMPLATE(name, func, clear_cache)					\
	static const struct templ __TMPL_TEMPLATE(func) = {#name, func, clear_cache}; \
	LINKER_SET_ADD_DATA(tmpl, __TMPL_TEMPLATE(func))

#define ADD_NULL_TEMPLATE(clear_cache)					\
	static const struct templ tNULL = {"", NULL, clear_cache}; \
	LINKER_SET_ADD_DATA(tmpl, tNULL)

#define ADD_TEMPLATE_FUNCTION_WITH_NAME_ATTR(func, name, attr)			\
	const struct templf __TMPL_FUNCTION(name) TEMPLATE_VISIBILITY attr = {#name, func};

#define ADD_TEMPLATE_FUNCTION_WITH_NAME(func, name)				\
	ADD_TEMPLATE_FUNCTION_WITH_NAME_ATTR(func, name,)

#define ADD_TEMPLATE_FUNCTION(func)						\
	ADD_TEMPLATE_FUNCTION_WITH_NAME(func, func)

#define ADD_TEMPLATE_FUNCTION_WEAK(func)					\
	ADD_TEMPLATE_FUNCTION_WITH_NAME_ATTR(func, func, TEMPLATE_WEAK)

#define ADD_FILTER_WITH_ATTR(filter, o_struct, o_sz, s_struct, s_sz, v_struct, v_sz, attr)	\
	struct bpfilter __TMPL_FILTER(filter) TEMPLATE_VISIBILITY attr = {#filter, 		\
	    init_##filter,							\
	    NULL,								\
	    NULL,								\
	    o_struct,								\
	    o_sz,								\
	    s_struct,								\
	    s_sz,								\
	    v_struct,								\
	    v_sz								\
	};


#define ADD_FILTER(filter, o_struct, o_sz, s_struct, s_sz, v_struct, v_sz)	\
	ADD_FILTER_WITH_ATTR(filter, o_struct, o_sz, s_struct, s_sz, v_struct, v_sz,)

#define ADD_FILTER_WEAK(filter, o_struct, o_sz, s_struct, s_sz, v_struct, v_sz)	\
	ADD_FILTER_WITH_ATTR(filter, o_struct, o_sz, s_struct, s_sz, v_struct, v_sz, TEMPLATE_WEAK)

#define ADD_OUTPUT_FILTER_WITH_ATTR(filter, o_sz, attr)						\
	ADD_FILTER_WITH_ATTR(filter, &filter##_output, o_sz, NULL, 0, NULL, 0, attr)

#define ADD_OUTPUT_FILTER(filter, o_sz)						\
	ADD_OUTPUT_FILTER_WITH_ATTR(filter, o_sz,)

#define ADD_OUTPUT_FILTER_WEAK(filter, o_sz)						\
	ADD_OUTPUT_FILTER_WITH_ATTR(filter, o_sz, TEMPLATE_WEAK)

#define ADD_SIMPLEVARS_FILTER(filter, s_sz)					\
	ADD_FILTER(filter, NULL, 0, &filter##_simplevars, s_sz, NULL, 0)

#define ADD_SIMPLEVARS_FILTER_WEAK(filter, s_sz)					\
	ADD_FILTER_WITH_ATTR(filter, NULL, 0, &filter##_simplevars, s_sz, NULL, 0, TEMPLATE_WEAK)

#define ADD_VTREE_FILTER(filter, v_sz)					\
	ADD_FILTER(filter, NULL, 0, NULL, 0, &filter##_vtree, v_sz)

#define ADD_VTREE_FILTER_WEAK(filter, v_sz)					\
	ADD_FILTER_WITH_ATTR(filter, NULL, 0, NULL, 0, &filter##_vtree, v_sz, TEMPLATE_WEAK)

#define ADD_LOOP_FILTER(filter, o_struct, o_sz, s_struct, s_sz, v_struct, v_sz)	\
	struct bpfilter __TMPL_LOOP_FILTER(filter) TEMPLATE_VISIBILITY = {#filter, 		\
	    init_##filter,							\
	    loop_##filter,							\
	    loop_length_##filter,						\
	    o_struct,								\
	    o_sz,								\
	    s_struct,								\
	    s_sz,								\
	    v_struct,								\
	    v_sz								\
	};

#endif
