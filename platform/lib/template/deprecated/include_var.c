#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <bpapi.h>
#include <ctemplates.h>
#include <ctype.h>
#include "log_event.h"
#include "logging.h"

/*
	Deprecation platform v2.3.1 target:
		include_var
*/

static const struct tpvar *
include_var(struct bpapi *api, struct tpvar *dst, enum bpcacheable *cc, int argc, const struct tpvar **argv) {
	struct bpapi ba;
	int cnt;
	struct buf_string *buf;
	char *res;

	if ((argc % 2) == 0)
		return TPV_SET(dst, TPV_NULL);

	buf = buf_output_init(&ba.ochain);
	hash_simplevars_init(&ba.schain);
	ba.schain.next = NULL;
	ba.vchain = api->vchain;

	for (cnt = 1 ; cnt < argc ; cnt += 2) {
		char *fk, *fv;
		const char *k = tpvar_str(argv[cnt], &fk);
		const char *v = tpvar_str(argv[cnt + 1], &fv);

		bpapi_insert(&ba, k, v);

		free(fk);
		free(fv);
	}

	char *ft;
	const char *template = tpvar_str(argv[0], &ft);

	call_template(&ba, template);

	free(ft);

	res = buf->buf;
	buf->buf = NULL;

	bpapi_output_free(&ba);
	bpapi_simplevars_free(&ba);

	*cc = BPCACHE_CANT;
	return TPV_SET(dst, res ? TPV_DYNSTR(res) : TPV_NULL);
}

ADD_TEMPLATE_FUNCTION(include_var);

