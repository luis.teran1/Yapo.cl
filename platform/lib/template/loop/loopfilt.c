
#include <bpapi.h>
#include <ctemplates.h>
#include <patchvars.h>
#include "logging.h"

static int
init_loopfilt(const struct bpfilter *filter, struct bpapi *api, int argc, const struct tpvar **argv) {
	const char *tag;
	char *ft;
	struct bpapi_simplevars_chain *schain;
	struct patch_data *pd = NULL;
	struct patch_data *pdt = NULL;

	tag = tpvar_str(argv[0], &ft);

	for (schain = &api->schain ; schain ; schain = schain->next) {
		if (schain->fun == &patch_simplevars) {
			pd = schain->data;
			if (pd->tag && strcmp(pd->tag, tag) == 0) {
				pdt = pd;
				break;
			}
		}
	}

	if (!pdt) {
		log_printf(LOG_WARNING, "loopfilt: filter tag not found: %s", tag);
		return 1;
	}

	BPAPI_SIMPLEVARS_DATA(api) = pdt;
	return 0;
}

const struct bpapi_simplevars loopfilt_simplevars = {
	patch_has_key,
	patch_key_length,
	patch_loop_get_element,
	patch_fetch_loop,
	patch_keyglob_len,
	patch_fetch_glob_loop,
	parent_insert,
	parent_insert_list,
	parent_insert_maxsize,
	parent_remove,
	NULL,
	patch_init_var,
};

#define loop_loopfilt loop_patch
#define loop_length_loopfilt loop_length_patch

ADD_LOOP_FILTER(loopfilt, NULL, 0, &loopfilt_simplevars, 0, NULL, 0);
