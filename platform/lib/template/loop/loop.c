
#include <bpapi.h>
#include <ctemplates.h>
#include <patchvars.h>

#include <stdbool.h>

struct loop_data
{
	struct bpapi_simplevars_chain vars;
	int idx;
	int len;
	struct bpapi_loop_var *lists;
	int nlists;
	bool all;
};

static struct bpapi_loop_var *
loop_new_loopvar(struct loop_data *data, int *alloc_lists) {
	if (*alloc_lists == data->nlists) {
		*alloc_lists *= 2;
		data->lists = xrealloc(data->lists, *alloc_lists * sizeof (*data->lists));
	}

	return &data->lists[data->nlists];
}

static void
loop_add_list(struct loop_data *data, const char *key) {
	if (data->lists[data->nlists].len > data->len)
		data->len = data->lists[data->nlists].len;

	if (data->lists[data->nlists].len)
		bps_insert_list(&data->vars, key, data->lists[data->nlists].l.list, data->lists[data->nlists].len);
	else
		bps_insert(&data->vars, key, "");
	data->nlists++;
}

static void
loop_add_simplevar(struct bpapi_simplevars_chain *schain, int *alloc_lists, const char *key) {
	struct loop_data *data = schain->data;

	if (data->all) {
		int len = bps_length(schain->next, key);
		if (len > data->len)
			data->len = len;
		return;
	}

	struct bpapi_loop_var *list = loop_new_loopvar(data, alloc_lists);

	bps_fetch_loop(schain->next, key, list);
	loop_add_list(data, key);
}

static int
init_loop(const struct bpfilter *filter, struct bpapi *api, int argc, const struct tpvar **argv) {
	struct loop_data *data = BPAPI_SIMPLEVARS_DATA(api);
	int i;
	int alloc_lists;

	hash_simplevars_init(&data->vars);
	data->idx = -1;
	data->lists = xmalloc(argc * sizeof (*data->lists));
	alloc_lists = argc;
	data->nlists = 0;
	data->all = false;

	for (i = 0; i < argc; i++) {
		struct bpapi_vtree_chain *node = tpvar_node(argv[i]);

		if (node) {
			if (i >= argc - 2)
				break;
			char *fk, *fv;
			const char *keyname = tpvar_str(argv[i + 1], &fk);
			const char *valuename = tpvar_str(argv[i + 2], &fv);

			if (keyname && *keyname) {
				vtree_fetch_keys(node, loop_new_loopvar(data, &alloc_lists), NULL);
				loop_add_list(data, keyname);
			}
			if (valuename && *valuename) {
				vtree_fetch_values(node, loop_new_loopvar(data, &alloc_lists), VTREE_LOOP, NULL);
				loop_add_list(data, valuename);
			}

			free(fk);
			free(fv);

			i += 2;
			continue;
		}

		char *fk;
		const char *key = tpvar_str(argv[i], &fk);
		int kl = strlen(key);

		if (strcmp(key, "*") == 0) {
			/* Short circuit. */
			data->all = true;
		}

		if (kl && key[kl - 1] == '*') {
			struct bpapi_loop_var loop;
			int i;

			bps_fetch_glob_loop(api->schain.next, key, &loop);

			for (i = 0 ; i < loop.len ; i++) {
				loop_add_simplevar(&api->schain, &alloc_lists, loop.l.list[i]);
			}
			if (loop.cleanup)
				loop.cleanup(&loop);
		} else {
			loop_add_simplevar(&api->schain, &alloc_lists, key);
		}
		free(fk);
	}

	return 0;
}

static int
loop_loop(struct bpapi *api) {
	struct loop_data *data = BPAPI_SIMPLEVARS_DATA(api);

	return ++data->idx < data->len;
}

static int
loop_length_loop(struct bpapi *api) {
	struct loop_data *data = BPAPI_SIMPLEVARS_DATA(api);

	return data->len;
}

static int
parent_has_key(struct bpapi_simplevars_chain *schain, const char *key) {
	return bps_has_key(schain->next, key);
}

static int
parent_key_length(struct bpapi_simplevars_chain *schain, const char *key) {
	return bps_length(schain->next, key);
}

static const char *
loop_get_element(struct bpapi_simplevars_chain *schain, const char *key, int pos) {
	struct loop_data *d = schain->data;

	const char *res = bps_get_element(&d->vars, key, pos + d->idx);
	if (res)
		return res;
	if (d->all)
		pos += d->idx;
	return bps_get_element(schain->next, key, pos);
}

static void
parent_fetch_loop(struct bpapi_simplevars_chain *schain, const char *key, struct bpapi_loop_var *loop) {
	bps_fetch_loop(schain->next, key, loop);
}


static int
parent_keyglob_len(struct bpapi_simplevars_chain *schain, const char *key) {
	return bps_keyglob_len(schain->next, key);
}

static void
parent_fetch_glob_loop(struct bpapi_simplevars_chain *schain, const char *key, struct bpapi_loop_var *loop) {
	bps_fetch_glob_loop(schain->next, key, loop);
}

static void
loop_init_var(struct bpapi_simplevars_chain *schain, const char *key, struct template_var *cache) {
	struct loop_data *d = schain->data;

	if (d->all || bps_has_key(&d->vars, key))
		return;

	bps_init_var(schain->next, key, cache);
}

static void
fini_loop(struct bpapi_simplevars_chain *schain) {
	struct loop_data *d = schain->data;
	int i;

	bps_free(&d->vars);
	for (i = 0 ; i < d->nlists ; i++) {
		if (d->lists[i].cleanup)
			d->lists[i].cleanup(&d->lists[i]);
	}
	free(d->lists);
}

const struct bpapi_simplevars loop_simplevars = {
	parent_has_key,
	parent_key_length,
	loop_get_element,
	parent_fetch_loop,
	parent_keyglob_len,
	parent_fetch_glob_loop,
	parent_insert,
	parent_insert_list,
	parent_insert_maxsize,
	parent_remove,
	fini_loop,
	loop_init_var,
};

ADD_LOOP_FILTER(loop, NULL, 0, &loop_simplevars, sizeof(struct loop_data), NULL, 0);
