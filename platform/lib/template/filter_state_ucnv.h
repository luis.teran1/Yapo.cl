#include "filter_state.h"

/*
 * Setup functions for the ucnv per-thread filter_state
 */

void fs_ucnv_utf8(struct bpapi *, const char *, struct filter_state_data *, void *);
void fs_ucnv_dst(struct bpapi *, const char *, struct filter_state_data *, void *);
