#include <math.h>

#include <bpapi.h>
#include <ctemplates.h>


static const struct tpvar *
divide(struct bpapi *api, struct tpvar *dst, enum bpcacheable *cc, int argc, const struct tpvar **argv) {
	float numer;
	float denom;
	float quote;
	char *res;
	const char *flag;

	if (argc < 2 || argc > 3)
		return TPV_SET(dst, TPV_NULL);

	numer = tpvar_float(argv[0]);
	denom = tpvar_float(argv[1]);

	if (fpclassify(denom) != FP_NORMAL)
		return TPV_SET(dst, TPV_NULL);

	if (argc == 3) {
		TPVAR_STRTMP(flag, argv[2]);
		if ((strcmp(flag, "ceil") == 0)) {
			quote = ceil(numer / denom);
			xasprintf(&res, "%.0f", quote);
		} else {
			quote = numer / denom;
			xasprintf(&res, "%.*f", atoi(flag), quote);
		}
	} else {
		quote = floor(numer / denom);
		xasprintf(&res, "%.0f", quote);
	}
	return TPV_SET(dst, TPV_DYNSTR(res));
}
ADD_TEMPLATE_FUNCTION(divide);
