#include <bpapi.h>
#include <ctemplates.h>


static const struct tpvar *
modulo(struct bpapi *api, struct tpvar *dst, enum bpcacheable *cc, int argc, const struct tpvar **argv) {
	int num;
	int mod;

	if (argc != 2)
		return TPV_SET(dst, TPV_NULL);

	num = tpvar_int(argv[0]);
	mod = tpvar_int(argv[1]);

	if (mod <= 0)
		return TPV_SET(dst, TPV_NULL);

	while (num < 0)
		num += mod;

	return TPV_SET(dst, TPV_INT(num % mod));
}

ADD_TEMPLATE_FUNCTION(modulo);
