#include <bpapi.h>
#include <ctemplates.h>


static const struct tpvar *
fmultiply(struct bpapi *api, struct tpvar *dst, enum bpcacheable *cc, int argc, const struct tpvar **argv) {
	if (argc < 2)
		return TPV_SET(dst, TPV_NULL);

	float prod = tpvar_float(argv[0]);
	int i;

	for ( i = 1; i < argc; i++) {
		prod *= tpvar_float(argv[i]);
	}

	char *sres;
	xasprintf(&sres, "%f", prod);
	return TPV_SET(dst, TPV_DYNSTR(sres));
}
ADD_TEMPLATE_FUNCTION(fmultiply);
