#include <bpapi.h>
#include <ctemplates.h>


static const struct tpvar *
maxint(struct bpapi *api, struct tpvar *dst, enum bpcacheable *cc, int argc, const struct tpvar **argv) {
	int i;
	int m;

	if (argc < 1)
		return TPV_SET(dst, TPV_NULL);

	m = tpvar_int(argv[0]);
	for (i = 1 ; i < argc ; i++) {
		int n = tpvar_int(argv[i]);

		if (n > m)
			m = n;
	}
	return TPV_SET(dst, TPV_INT(m));
}
ADD_TEMPLATE_FUNCTION(maxint);
