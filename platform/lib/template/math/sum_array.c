#include <bpapi.h>
#include <ctemplates.h>


static const struct tpvar *
sum_array(struct bpapi *api, struct tpvar *dst, enum bpcacheable *cc, int argc, const struct tpvar **argv) {
	int i;
	int len;
	int res = 0;
	const char *list;

	if (argc < 1 || argc > 2)
		return TPV_SET(dst, TPV_NULL);

	TPVAR_STRTMP(list, argv[0]);
	len = bpapi_length(api, list);
	for (i = 0; i < len; i++) {
		if (argc == 2)
			res += atoi(bpapi_get_element(api, bpapi_get_element(api, list, i), 0) ?: "0");
		else
			res += atoi(bpapi_get_element(api, list, i));
	}
	*cc = BPCACHE_CANT;
	return TPV_SET(dst, TPV_INT(res));
}
ADD_TEMPLATE_FUNCTION(sum_array);
