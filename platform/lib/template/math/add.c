#include <bpapi.h>
#include <ctemplates.h>

static const struct tpvar *
add(struct bpapi *api, struct tpvar *dst, enum bpcacheable *cc, int argc, const struct tpvar **argv) {
        if (argc < 2)
                return TPV_SET(dst, TPV_NULL);

        int sum = 0;
        int i;

        for ( i = 0; i < argc ; i++) {
		sum += tpvar_int(argv[i]);
        }

	return TPV_SET(dst, TPV_INT(sum));
}

ADD_TEMPLATE_FUNCTION(add);
