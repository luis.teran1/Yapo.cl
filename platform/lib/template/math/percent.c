#include <bpapi.h>
#include <ctemplates.h>


static const struct tpvar *
percent(struct bpapi *api, struct tpvar *dst, enum bpcacheable *cc, int argc, const struct tpvar **argv) {
	float numer;
	float denom;
	float quote;
	char *res;

	if (argc < 2)
		return TPV_SET(dst, TPV_NULL);

	numer = tpvar_float(argv[0]);
	denom = tpvar_float(argv[1]);

	if (!denom)
		return TPV_SET(dst, TPV_NULL);

	quote = 100 * numer / denom;
	xasprintf(&res, "%.*f", argc == 3 ? tpvar_int(argv[2]) : 0, quote);
	return TPV_SET(dst, TPV_DYNSTR(res));
}
ADD_TEMPLATE_FUNCTION(percent);
