#include <bpapi.h>
#include <ctemplates.h>


static const struct tpvar *
fcmp(struct bpapi *api, struct tpvar *dst, enum bpcacheable *cc, int argc, const struct tpvar **argv) {
	if (argc != 2)
		return TPV_SET(dst, TPV_NULL);

	float a = tpvar_float(argv[0]);
	float b = tpvar_float(argv[1]);

	if (a < b)
		return TPV_SET(dst, TPV_INT(-1));
	if (a > b)
		return TPV_SET(dst, TPV_INT(1));
	return TPV_SET(dst, TPV_INT(0));
}
ADD_TEMPLATE_FUNCTION(fcmp);
