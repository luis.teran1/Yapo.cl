#include <bpapi.h>
#include <ctemplates.h>


static const struct tpvar *
multiply(struct bpapi *api, struct tpvar *dst, enum bpcacheable *cc, int argc, const struct tpvar **argv) {
        if (argc < 2)
                return TPV_SET(dst, TPV_NULL);

        int prod = tpvar_int(argv[0]);
        int i;

        for ( i = 1; i < argc ; i++) {
		prod *= tpvar_int(argv[i]);
        }

	return TPV_SET(dst, TPV_INT(prod));
}
ADD_TEMPLATE_FUNCTION(multiply);
