#include <bpapi.h>
#include <string.h>
#include <stdio.h>
#include <sqlite3.h>
#include <stdlib.h>

#include "strl.h"
#include "patchvars.h"
#include "bconf.h"
#include "logging.h"

static int
sqlite_patchvars_cb(void *v, int argc, char **argv, char **col) {
	struct patch_data *pd = v;
	int i;

	for (i = 0; i < argc; i++) {
		bps_insert(&pd->vars, col[i], argv[i] ?: "");
	}
	pd->rows++;
	return 0;
}

static int
init_sqlite_exec(const struct bpfilter *filter, struct bpapi *api, int argc, const struct tpvar **argv) {
	struct patch_data *pd = BPAPI_SIMPLEVARS_DATA(api);
	const char *dbfile;
	const char *prefix;
	const char *query;
	char *zerrmsg = NULL;
	sqlite3 *db;

	TPVAR_STRTMP(dbfile, argv[0]);
	TPVAR_STRTMP(prefix, argv[1]);
	TPVAR_STRTMP(query, argv[2]);


	log_printf(LOG_DEBUG, "Setting up filter with tag: %s", filter->name);
	patchvars_init(pd, filter->name, prefix, strlen(prefix), NULL, NULL);

	if (sqlite3_open(dbfile, &db)) {
		log_printf(LOG_CRIT, "sqlite3_open failed");
		return 1;
	}
	if (sqlite3_exec(db, query, sqlite_patchvars_cb, pd, &zerrmsg) != SQLITE_OK) {
		log_printf(LOG_CRIT, "sqlite3_exec failed: %s", zerrmsg);
		sqlite3_free(zerrmsg);
		sqlite3_close(db);
		return 1;
	}

	sqlite3_close(db);
	return 0;
}

#define sqlite_exec_simplevars patch_simplevars
ADD_SIMPLEVARS_FILTER(sqlite_exec, sizeof(struct patch_data));
