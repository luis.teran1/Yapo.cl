
#include <bpapi.h>
#include <ctemplates.h>
#include <qsort.h>


struct _highlight {
	char* buf;
	int len;
	int pos;
	const char *varname;
	const char *template;
	char *fv, *ft;
	struct bpapi ba_copy;
};

static int
highlight_outstring_raw(struct bpapi_output_chain *ochain, const char *str, size_t len) {
	struct _highlight *hl = ochain->data;

	bufwrite(&hl->buf, &hl->len, &hl->pos, str, len);
	return 0;
}

static int
init_highlight(const struct bpfilter *filter, struct bpapi *api, int argc, const struct tpvar **argv) {
	struct _highlight *hl = BPAPI_OUTPUT_DATA(api);

	hl->varname = tpvar_str(argv[0], &hl->fv);
	hl->template = tpvar_str(argv[1], &hl->ft);

	BPAPI_COPY(&hl->ba_copy, api, 0, 1, 1);

	if (!bpapi_has_key(api, hl->varname)) {
		syslog(LOG_DEBUG, "Highlight filter called with varname '%s' which is not defined.", hl->varname);
	}

	return 0;
}

struct highlight_replacement
{
	int start;
	int end;
	char *buf;
};

static void
fini_highlight(struct bpapi_output_chain *ochain) {
	struct _highlight *hl = ochain->data;

	if (hl->buf) {
		int i = 0;
		const char* word = NULL;
		int words = bps_length(&hl->ba_copy.schain, hl->varname);
		struct highlight_replacement *matches = NULL;
		int nmatches = 0;
		int amatches = 0;

		if (words > 0) {
			for (i = 0 ; i < words ; ++i) {
				word = bps_get_element(&hl->ba_copy.schain, hl->varname, i);

				/* Early out if word can't possibly match. Might be a case of premature optimization. */
				if (*word == '\0' || strcasestr(hl->buf, word) == NULL) {
					continue;
				}

				/* Case-insensitive dupe skipping. Probably not the most efficient solution, but simple. */
				int j;
				int dupe = 0;
				for (j = 0 ; j < i ; ++j) {
					if (strcasecmp(word, bps_get_element(&hl->ba_copy.schain, hl->varname, j)) == 0) {
						dupe = 1;
						break;
					}
				}

				if (dupe)
					continue;

				struct buf_string *buf;
				struct bpapi ba;

				/* To-do: Need access to parent bapi -- setup api pointers similar to varblock? */
				buf = buf_output_init(&ba.ochain);
				BPAPI_COPY(&ba, &hl->ba_copy, 0, 1, 1);

				bpapi_insert(&ba, "current_searchword", word);
				bpapi_set_int(&ba, "current_searchword_index", i);

				call_template(&ba, hl->template);

				/* We have a template result to replace into the buffer. */
				if (buf->buf) {
					char* t;
					pcre *pe;
					const char *err;
					int errval;
					int pematch[3];
					int off = 0;
					int flags = PCRE_CASELESS;
					const char *charset = vtree_get(&hl->ba_copy.vchain, "common", "charset", NULL);

					if (charset && strcmp(charset, "UTF-8") == 0)
						flags |= PCRE_UTF8;

					/* Word boundry only, and we escape regular expression meta-characters for safety */
					xasprintf(&t, "\\b(\\Q%s\\E)\\b", word);
					pe = pcre_compile(t, flags, &err, &errval, NULL);

					if (!pe)
						abort();

					while (off < hl->pos) {
						errval = pcre_exec(pe, NULL, hl->buf, hl->pos, off, 0, pematch, 3);

						if (errval == PCRE_ERROR_NOMATCH)
							break;

						if (!amatches) {
							amatches = 2;
							matches = xmalloc(2 * sizeof(*matches));
						} else if (nmatches >= amatches) {
							amatches *= 2;
							matches = xrealloc(matches, amatches * sizeof(*matches));
						}
						matches[nmatches].start = pematch[0];
						matches[nmatches].end = pematch[1];
						matches[nmatches++].buf = xstrdup(buf->buf); /* XXX wasteful */
						off = pematch[1];
					}

					free(t);
					pcre_free(pe);
				}

				bpapi_remove(&ba, "current_searchword");
				bpapi_remove(&ba, "current_searchword_index");

				bpapi_output_free(&ba);
			}
		}

		int offset = 0;
		if (nmatches > 0) {
			/* XXX Actually we're likely to hit worst case for qsort, a merge would be better. */
#define cmp(a, b) (a->start < b->start)
			QSORT(struct highlight_replacement, matches, nmatches, cmp);
#undef cmp

			for (i = 0 ; i < nmatches ; i++) {
				if (matches[i].start < offset) {
					free(matches[i].buf);
					continue;
				}

				if (matches[i].start > offset)
					bpo_outstring_raw(ochain->next, hl->buf + offset, matches[i].start - offset);

				char *rep = matches[i].buf;
				char *repstart = rep;

				for (; *rep  != '\0' ; rep++) {
					if (*rep == '$' && *(rep + 1) == '1') {
						if (repstart < rep) {
							bpo_outstring_raw(ochain->next, repstart, rep - repstart);
						}
						rep++;
						repstart = rep + 1;

						bpo_outstring_raw(ochain->next, hl->buf + matches[i].start, matches[i].end - matches[i].start);
					}
				}
				if (repstart < rep)
					bpo_outstring_raw(ochain->next, repstart, rep - repstart);

				free(matches[i].buf);

				offset = matches[i].end;
			}
			free(matches);
		}
		if (offset < hl->pos)
			bpo_outstring_raw(ochain->next, hl->buf + offset, hl->pos - offset);

		free(hl->buf);
	}

	free(hl->fv);
	free(hl->ft);
}

static const struct bpapi_output highlight_output = {
	self_outstring_fmt,
	highlight_outstring_raw,
	fini_highlight,
	NULL
};

ADD_OUTPUT_FILTER(highlight, sizeof(struct _highlight));
