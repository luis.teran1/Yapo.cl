
#include <bpapi.h>
#include <ctemplates.h>
#include <query_string.h>
#include <unicode.h>
#include <patchvars.h>
#include <url.h>

#include "filter_state_ucnv.h"

/**
	If we have a referrer, see if it's a know search engine.
	If so, add any search words in the query to the bapi.
*/
struct qs_searchwords {
	const char* param_name;
	struct bpapi_simplevars_chain *vars;
	int max_words;
	UConverter *utf8_cnv;
	UConverter *dst_cnv;
	struct bpapi_vtree_chain *vtree;
};

static void
insert_searchwords_cb(struct parse_cb_data *pqcb_data, char *key, int klen, char *value, int vlen) {
	struct qs_searchwords* qs_data = (struct qs_searchwords*)(pqcb_data->cb_data);
	struct bpapi_simplevars_chain *vars = qs_data->vars;
	const unsigned int minimum_searchword_length = vtree_getint(qs_data->vtree, "common", "referer_searchwords", "min_word_length", NULL) ?: 3;

	if (strcmp(key, qs_data->param_name) == 0) {
		char* value_copy = strtrchr(value, ".+*^$()<>-=&\\\"", ' ');
		char* tmp = value_copy;
		char* elem_name = NULL;
		char* delim = NULL;
		char buf[1024];

		/* It'd be nice to do a parse which recognize strings like '+"a search phrase" -notme"', but this will have to do. */
		while ((elem_name = strtok_r(value_copy, " ", &delim))) {
			if (strlen(elem_name) >= minimum_searchword_length && qs_data->max_words > 0 ) {
				utf8_decode(buf, (int[]){ sizeof(buf) }, elem_name, -1, qs_data->utf8_cnv, qs_data->dst_cnv);
				bps_insert(vars, "referer_searchwords", elem_name);
				--qs_data->max_words;
			}
			value_copy = NULL;
		}

		free(tmp);
	}
}

static int
init_referer_searchwords(const struct bpfilter *filter, struct bpapi *api, int argc, const struct tpvar **argv) {
	int i = 0;
	int max_words = 0;
	int words_left = 0;
	const char* query_part = NULL;
	const char *referer;
	struct bpapi_loop_var nodes;
	struct patch_data *pd = BPAPI_SIMPLEVARS_DATA(api);
	enum req_utf8 req_utf8;

	TPVAR_STRTMP(referer, argv[0]);

	patchvars_init(pd, NULL, NULL, 0, NULL, NULL);

	if (!referer || !referer[0])
		return 0;

	if ( (max_words = vtree_getint(&api->vchain, "common", "referer_searchwords", "max_words", NULL)) < 1)
		return 0;

	req_utf8 = get_req_utf8_vtree(&api->vchain);

	words_left = max_words;

	query_part = strchr(referer, '?');
	if (query_part == NULL) {
		/* Google query part can start with a '#' instead of a '?'. */
		query_part = strchr(referer, '#');
	}

	UConverter *utf8_cnv = filter_state(api, "ucnv_utf8", fs_ucnv_utf8, NULL);
	UConverter *dst_cnv = filter_state(api, "ucnv_dst", fs_ucnv_dst, NULL);

	vtree_fetch_nodes(&api->vchain, &nodes, "common", "referer_searchwords", "searchengines", NULL);
	for (i = 0 ; i < nodes.len ; i++) {
		const char* pattern = vtree_get(&nodes.l.vlist[i], "name_pattern", NULL);
		const char* pattern_loc;

		if (!pattern || !pattern[0])
			continue;

		if ( (pattern_loc = strstr(referer, pattern)) != NULL) {
			const char* query_param = vtree_get(&nodes.l.vlist[i], "query_param", NULL);

			/* Parse query string if present and the name pattern occurs _before_ it. */
			if (query_param && query_part && *(query_part+1) && (pattern_loc < query_part)) {
				struct qs_searchwords qs_data;
				char *qsstr = xstrdup(query_part + 1);
				qs_data.vars = &pd->vars;
				qs_data.param_name = query_param;
				qs_data.max_words = max_words;
				qs_data.utf8_cnv = utf8_cnv;
				qs_data.dst_cnv = dst_cnv;
				qs_data.vtree = &api->vchain;
				parse_query_string(qsstr, insert_searchwords_cb, &qs_data, NULL, 0, req_utf8, NULL);
				free(qsstr);
				words_left = qs_data.max_words;
				break;
			}

			const char *start_pat = vtree_get(&nodes.l.vlist[i], "path", "start", NULL);
			const char *end_pat = vtree_get(&nodes.l.vlist[i], "path", "end", NULL);
			const char *start;
			const char *end;
			int splen = start_pat ? strlen(start_pat) : 0;

			if (start_pat && end_pat
					&& (start = strstr(referer, start_pat))
					&& start > pattern_loc
					&& (end = strstr(start + splen, end_pat))) {
				int len = end - start - splen;
				int pslen = len * 2 + 1;
				char *pathstr = xmalloc(pslen);
				struct qs_searchwords qs_data;

				memcpy(pathstr, start + splen, len);
				pathstr[len] = '\0';
				url_decode(pathstr, len, NULL, 0, NULL);
				qs_data.vars = &pd->vars;
				qs_data.param_name = "q";
				qs_data.max_words = max_words;
				qs_data.utf8_cnv = utf8_cnv;
				qs_data.dst_cnv = dst_cnv;
				qs_data.vtree = &api->vchain;
				struct parse_cb_data pqcb_data = { .cb_data = &qs_data, .options = NULL };
				/* XXX hackish */
				insert_searchwords_cb(&pqcb_data, (char*)"q", 1, pathstr, pslen);
				free(pathstr);
				break;
			}
		}
	}
	if (nodes.cleanup)
		nodes.cleanup(&nodes);

	/* Autodetect search words from q argument regardless of actual refering site */
	if ( query_part && (words_left == max_words) && (vtree_getint(&api->vchain, "common", "referer_searchwords", "query_autodetect", NULL) > 0) && (strstr(query_part, "q=") != NULL) ) {

		int autodetect = 1;
		struct bpapi_loop_var ignore_nodes;

		vtree_fetch_values(&api->vchain, &ignore_nodes, "common", "referer_searchwords", "query_autodetect_ignore", VTREE_LOOP, "hostpart", NULL);

		/* Only autodetect if not on ignore-list */
		for (i = 0 ; i < ignore_nodes.len ; i++) {
			const char* hostpart = ignore_nodes.l.list[i];
			if (hostpart && hostpart[0] && (strstr(referer, hostpart) != NULL)) {
				autodetect = 0;
				break;
			}
		}
		if (ignore_nodes.cleanup)
			ignore_nodes.cleanup(&ignore_nodes);

		if (autodetect) {
			struct qs_searchwords qs_data;
			char *qsstr = xstrdup(query_part + 1);
			qs_data.vars = &pd->vars;
			qs_data.param_name = "q";
			qs_data.max_words = words_left;
			qs_data.utf8_cnv = utf8_cnv;
			qs_data.dst_cnv = dst_cnv;
			qs_data.vtree = &api->vchain;
			parse_query_string(qsstr, insert_searchwords_cb, &qs_data, NULL, 0, req_utf8, NULL);
			free(qsstr);
		}
	}
	return 0;
}

ADD_FILTER(referer_searchwords, NULL, 0, &patch_simplevars, sizeof(struct patch_data), NULL, 0);
