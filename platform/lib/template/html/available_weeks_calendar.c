
#include <stdio.h>
#include <limits.h>
#include <time.h>

#include <bpapi.h>
#include <ctemplates.h>
#include <queue.h>

#include "patchvars.h"

struct awc_entry {
	SLIST_ENTRY(awc_entry) l;
	const char *season;
	unsigned char weeks[3][53];
	char *fs;
};

/* Input data is given in pairs of (name, weeks)
 * name will be the output into @awc_monthday_* variables whenever a week is
 * given in the input weeks list. The weeks is comma separated list of
 * year:week with optional information about indivudual days available appended
 * (I.e. YYYY:W[:MM][,YYYY:W[:MM] ..]). MM data is two hex digits with a
 * bitmask (first 7 bits) of indicating which days are available (for instance
 * 2013:02:01 would indicate monday 2nd week of 2013 is available, 2013:02
 * would indicate all 2nd week of 2013 is available).
 *
 * Input data is used in the order specified, first matching data will
 * be the name in output
 */
static int
init_available_weeks_calendar(const struct bpfilter *filter, struct bpapi *api, int argc, const struct tpvar **argv) {
	struct patch_data *data = BPAPI_SIMPLEVARS_DATA(api);
	int min_year = INT_MAX;
	int min_month = 12;
	SLIST_HEAD(, awc_entry) list = SLIST_HEAD_INITIALIZER(&list);
	struct awc_entry *awc;
	time_t t;
	struct tm tm = {0};
	char weekbuf[10];
	struct date_rec dr;
	int week;
	int min_wday;
	int weekstoshow;
	int i;
	int curr_year;
	int curr_month;
	int dim;

	t = time(NULL);
	localtime_r(&t, &tm);
	curr_year = tm.tm_year + 1900;
	curr_month = tm.tm_mon + 1;

	patchvars_init(data, "available_weeks_calendar", "awc_", sizeof("awc_") - 1, NULL, 0);

	/* Figure out the first month to display, use first month >=
	   current month with information or current month if none */
	for (i = 0; i < argc; i += 2) {
		int year;
		int week;
		int month;
		unsigned int daymask;

		const char *weeks;
		char *weeks_ptr;

		if (i + 1 == argc) {
			/* wrong number of args. */
			break;
		}

		weeks = tpvar_str(argv[i + 1], &weeks_ptr);

		/* Find first week in month >= current month */
		while (weeks) {
			const char *next = strchr(weeks, ',');

			int matched = sscanf(weeks, "%d:%d:%2x", &year, &week, &daymask);
			if (matched == 2) {
				daymask = 0xff;
			} else if (matched < 3) {
				break;
			}

			/* Is this week the earliest available week in
			 * the current month or later? If so we set
			 * this as the start date of display */
			memset(&tm, 0, sizeof(tm));
			tm.tm_year = year - 1900;
			tm.tm_mon = 0;
			tm.tm_mday = 4 + (week - 1) * 7;
			mktime(&tm);
			month = tm.tm_mon + 1;

			if (year > curr_year || (year == curr_year && month >= curr_month)) {
				if (year < min_year || (year == min_year && month < min_month)) {
					min_year = year;
					min_month = month;
				}
				break;
			}

			if (next)
				weeks = next + 1;
			else
				weeks = NULL;
		}
		free(weeks_ptr);

	}

	/* Could not find any information about weeks earlier then this
	 * one? Then use the current month as start of display */
	if (min_year == INT_MAX) {
		min_year = curr_year;
		min_month = curr_month;
	}

	/* Go over all input week arrays, for the first display year
	 * and the following year only, convert to bitmask per day and
	 * build list with information */
	for (i = 0; i < argc; i += 2) {
		int year;
		int week;
		unsigned int daymask;

		const char *weeks;
		char *weeks_ptr;

		const struct tpvar *season;
		const struct tpvar *weeksvar;

		season = argv[i];
		weeksvar = argv[i + 1];

		if (i + 1 == argc) {
			break;
		}

		/* Add season/week details to list for processing later */
		awc = xmalloc(sizeof(*awc));
		awc->season = tpvar_str(season, &awc->fs);
		memset(awc->weeks, 0, sizeof(awc->weeks));
		weeks = tpvar_str(weeksvar, &weeks_ptr);

		/* Find first month >= current month */
		while (weeks) {
			const char *next = strchr(weeks, ',');

			int matched = sscanf(weeks, "%d:%d:%2x", &year, &week, &daymask);
			if (matched == 2) {
				daymask = 0xff;
			} else if (matched < 3) {
				break;
			}

			/* Only add data for this and the surrounding years, we
			 * will render only a single month, so we will
			 * not need more data then this */
			if ((year >= min_year - 1) && (year <= (min_year + 1)) && week <= 53 && week >= 1) {
				year = year - min_year + 1;

				awc->weeks[year][week - 1] = daymask;
			}

			if (next) {
				weeks = next + 1;
			} else {
				weeks = NULL;
			}
		}
		free(weeks_ptr);
		SLIST_INSERT_HEAD(&list, awc, l);
	}

	/* Starting year and month for the calendar */
	bps_set_int(&data->vars, "awc_year", min_year);
	bps_set_int(&data->vars, "awc_month", min_month);

	/* Figure out week number for year/month of first month to display */
	tm.tm_year = min_year - 1900;
	tm.tm_mon = min_month - 1;
	tm.tm_mday = 1;
	tm.tm_isdst = -1;
	mktime(&tm);
	strftime(weekbuf, sizeof(weekbuf), "%V", &tm);
	week = atoi(weekbuf);
	min_wday = tm.tm_wday;

	date_set(&dr, &tm);
	dim = date_days_in_month(&dr);

	/* Figure out start day. */
	if (min_wday == 0)
		date_set_day_offset(&dr, -6);
	else
		date_set_day_offset(&dr, -(min_wday - 1));

	/* If we are on feb 1 on a non leap year, only show 4 weeks, otherwise 5 or 6. */
	if (dr.month == 2 && dr.day == 1 && (dr.year % 4 != 0 || (dr.year % 100 == 0 && dr.year % 400 != 0)))
		weekstoshow = 4;
	else if ((min_wday == 0 && min_month != 2) || (min_wday == 6 && dim == 31))
		weekstoshow = 6;
	else
		weekstoshow = 5;

	for (i = 0; i < weekstoshow; i++) {
		int found = 0;

		/* Contains season for each day of the week. Argument
		 * lists are searched in order provided, first match
		 * will win */
		const char *seasons[7] = { 0 };

		int weekyear = dr.year;

		if (dr.month == 12 && week == 1) {
			weekyear++;
		}

		/* We have data for the surrounding years only, shift array offset by first */
		weekyear = weekyear - min_year + 1;

		/* List contains information for each provided series
		 * of data, season name and bitmask for the days of the
		 * year to use */
		SLIST_FOREACH(awc, &list, l) {
			unsigned char weekmask = awc->weeks[weekyear][week - 1];
			int bm;

			for (bm = 0 ; bm < 7 ; ++bm) {
				if (!seasons[bm] && ((1 << bm) & weekmask)) {
					seasons[bm] = awc->season;
					found++;
				}
			}
			if (found == 7)
				break;
		}

		/* awc_season is set for Compability reasons only, for
		 * full week displays all days will be the same, but
		 * for day displays all days may differ, so do not use
		 * this one for data with per day information */
		bps_insert(&data->vars, "awc_season", (seasons[0] ?: ""));
		bps_insert(&data->vars, "awc_season_mon", (seasons[0] ?: ""));
		bps_insert(&data->vars, "awc_season_tue", (seasons[1] ?: ""));
		bps_insert(&data->vars, "awc_season_wed", (seasons[2] ?: ""));
		bps_insert(&data->vars, "awc_season_thu", (seasons[3] ?: ""));
		bps_insert(&data->vars, "awc_season_fri", (seasons[4] ?: ""));
		bps_insert(&data->vars, "awc_season_sat", (seasons[5] ?: ""));
		bps_insert(&data->vars, "awc_season_sun", (seasons[6] ?: ""));

		bps_set_int(&data->vars, "awc_week", week++);
		/* One record for each day of the week in the output */
		bps_set_int(&data->vars, "awc_monthday_mon", dr.day);
		date_set_next_day(&dr);
		bps_set_int(&data->vars, "awc_monthday_tue", dr.day);
		date_set_next_day(&dr);
		bps_set_int(&data->vars, "awc_monthday_wed", dr.day);
		date_set_next_day(&dr);
		bps_set_int(&data->vars, "awc_monthday_thu", dr.day);
		date_set_next_day(&dr);
		bps_set_int(&data->vars, "awc_monthday_fri", dr.day);
		date_set_next_day(&dr);
		bps_set_int(&data->vars, "awc_monthday_sat", dr.day);
		date_set_next_day(&dr);
		bps_set_int(&data->vars, "awc_monthday_sun", dr.day);
		date_set_next_day(&dr);
		if (dr.day_of_year <= 4 || (dr.month == 12 && dr.day >= 29))
			week = 1;
	}
	while (!SLIST_EMPTY(&list)) {
		awc = SLIST_FIRST(&list);
		SLIST_REMOVE_HEAD(&list, l);

		free(awc->fs);
		free(awc);
	}
	return 0;
}
#define available_weeks_calendar_simplevars patch_simplevars
ADD_SIMPLEVARS_FILTER(available_weeks_calendar, sizeof(struct patch_data));
