
#include <bpapi.h>
#include <ctemplates.h>
#include <url.h>


static int
googleencode_outstring_raw(struct bpapi_output_chain *ochain, const char *str, size_t len) {
	char *ue = perform_google_encode(str, len);

	if (ue) {
		bpo_outstring_raw(ochain->next, ue, strlen(ue)); /* XXX strlen(ue) is probably equal to len */
		free(ue);
	}
	return 0;
}

static int
init_googleencode(const struct bpfilter *filter, struct bpapi *api, int argc, const struct tpvar **argv) {
	return 0;
}

static const struct bpapi_output googleencode_output = {
	self_outstring_fmt,
	googleencode_outstring_raw,
};

ADD_OUTPUT_FILTER(googleencode, 0);
