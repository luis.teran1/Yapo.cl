
#include <bpapi.h>
#include <ctemplates.h>


const char *xiti_allowed_chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789./-_~";
struct xiti_transliterate_char {
	const char *from;
	const char to;
};

struct xiti_transliterate_utf8 {
	const int codepoint;
	const char to;
};

const struct xiti_transliterate_char xiti_transliterate_latin1[] = {
	{ "\xE5\xE4\xE1\xE0\xE2\xE3", 'a' }, /* åäáàâã */
	{ "\xC5\xC4\xC1\xC0\xC2\xC3", 'A' }, /* ÅÄÁÀÂÃ */
	{ "\xF6\xF3\xF2\xF4\xF5", 'o' }, /* öóòôõ */
	{ "\xD6\xD3\xD2\xD4\xD5", 'O' }, /* ÖÓÒÔÕ */
	{ "\xEB\xE9\xE8\xEA", 'e' }, /* ëéèê */
	{ "\xCB\xC9\xC8\xCA", 'E' }, /* ËÉÈÊ */
	{ "\xFA\xFC\xFB", 'u' }, /* úüû */
	{ "\xDA\xDC\xDB", 'U' }, /* ÚÜÛ */
	{ "\xF1", 'n' }, /* ñ */
	{ "\xD1", 'N' }, /* Ñ */
	{ NULL }
};

/* Used by jofogas */
const struct xiti_transliterate_char xiti_transliterate_latin2[] = {
	{ "\xE1", 'a' }, /* á */
	{ "\xE9", 'e' }, /* é */
	{ "\xED", 'i' }, /* í */
	{ "\xF3\xF6\xF5", 'o' }, /* óöő */
	{ "\xFA\xFC\xFB", 'u' }, /* úüű */
	{ "\xC1", 'A' }, /* Á */
	{ "\xC9", 'E' }, /* É */
	{ "\xCD", 'I' }, /* Í */
	{ "\xD3\xD6\xD5", 'O' }, /* ÓÖŐ */
	{ "\xDA\xDC\xDB", 'U' }, /* ÚÜŰ */
	{ NULL }
};

const struct xiti_transliterate_char xiti_transliterate_greek[] = {
	{ "\xE1\xDC", 'a' }, /* αά */
	{ "\xC1\xB6", 'A' }, /* ΑΆ */
	{ "\xE2", 'b' }, /* β */
	{ "\xC2", 'B' }, /* Β */
	{ "\xE3", 'g' }, /* γ */
	{ "\xC3", 'G' }, /* Γ */
	{ "\xE4", 'd' }, /* δ */
	{ "\xC4", 'D' }, /* Δ */
	{ "\xE5\xDD", 'e' }, /* εέ */
	{ "\xC5\xB8", 'E' }, /* ΕΈ */
	{ "\xE6", 'z' }, /* ζ */
	{ "\xC6", 'Z' }, /* Ζ */
	{ "\xE7\xDE", 'h' }, /* ηή */
	{ "\xC7\xB9", 'H' }, /* ΗΉ */
	{ "\xE8", 't' }, /* θ */
	{ "\xC8", 'T' }, /* Θ */
	{ "\xE9\xDF", 'i' }, /* ιί */
	{ "\xC9\xBA", 'I' }, /* ΙΊ */
	{ "\xEA", 'k' }, /* κ */
	{ "\xCA", 'K' }, /* Κ */
	{ "\xEB", 'l' }, /* λ */
	{ "\xCB", 'L' }, /* Λ */
	{ "\xEC", 'm' }, /* μ */
	{ "\xCC", 'M' }, /* Μ */
	{ "\xED", 'n' }, /* ν */
	{ "\xCD", 'N' }, /* Ν */
	{ "\xEE", 'x' }, /* ξ */
	{ "\xCE", 'X' }, /* Ξ */
	{ "\xEF\xFC", 'o' }, /* οό */
	{ "\xCF\xBC", 'O' }, /* ΟΌ */
	{ "\xF0", 'p' }, /* π */
	{ "\xD0", 'P' }, /* Π */
	{ "\xF1", 'r' }, /* ρ */
	{ "\xD1", 'R' }, /* Ρ */
	{ "\xF3\xF2", 's' }, /* σς */
	{ "\xD3", 'S' }, /* Σ */
	{ "\xF4", 't' }, /* τ */
	{ "\xD4", 'T' }, /* Τ */
	{ "\xF5\xFD", 'u' }, /* υύ */
	{ "\xD5\xBE", 'U' }, /* ΥΎ */
	{ "\xF6", 'f' }, /* φ */
	{ "\xD6", 'F' }, /* Φ */
	{ "\xF7", 'x' }, /* χ */
	{ "\xD7", 'X' }, /* Χ */
	{ "\xF8", 'c' }, /* ψ */
	{ "\xD8", 'C' }, /* Ψ */
	{ "\xF9\xFE", 'o' }, /* ωώ */
	{ "\xD9\xBF", 'O' }, /* ΩΏ */
	{ NULL }
};

const struct xiti_transliterate_utf8 xiti_transliterate_utf8_def[] = {
	{0x00C0, 'A'}, /* LATIN CAPITAL LETTER A WITH GRAVE */
	{0x00C1, 'A'}, /* LATIN CAPITAL LETTER A WITH ACUTE */
	{0x00C2, 'A'}, /* LATIN CAPITAL LETTER A WITH CIRCUMFLEX */
	{0x00C3, 'A'}, /* LATIN CAPITAL LETTER A WITH TILDE */
	{0x00C4, 'A'}, /* LATIN CAPITAL LETTER A WITH DIAERESIS */
	{0x00C5, 'A'}, /* LATIN CAPITAL LETTER A WITH RING ABOVE */
	{0x00C6, 'A'}, /* LATIN CAPITAL LETTER AE */
	{0x00C7, 'C'}, /* LATIN CAPITAL LETTER C WITH CEDILLA */
	{0x00C8, 'E'}, /* LATIN CAPITAL LETTER E WITH GRAVE */
	{0x00C9, 'E'}, /* LATIN CAPITAL LETTER E WITH ACUTE */
	{0x00CA, 'E'}, /* LATIN CAPITAL LETTER E WITH CIRCUMFLEX */
	{0x00C1, 'A'}, /* LATIN CAPITAL LETTER A WITH ACUTE */
	{0x00C2, 'A'}, /* LATIN CAPITAL LETTER A WITH CIRCUMFLEX */
	{0x00C3, 'A'}, /* LATIN CAPITAL LETTER A WITH TILDE */
	{0x00C4, 'A'}, /* LATIN CAPITAL LETTER A WITH DIAERESIS */
	{0x00C5, 'A'}, /* LATIN CAPITAL LETTER A WITH RING ABOVE */
	{0x00C6, 'A'}, /* LATIN CAPITAL LETTER AE */
	{0x00C7, 'C'}, /* LATIN CAPITAL LETTER C WITH CEDILLA */
	{0x00C8, 'E'}, /* LATIN CAPITAL LETTER E WITH GRAVE */
	{0x00C9, 'E'}, /* LATIN CAPITAL LETTER E WITH ACUTE */
	{0x00CA, 'E'}, /* LATIN CAPITAL LETTER E WITH CIRCUMFLEX */
	{0x00CB, 'E'}, /* LATIN CAPITAL LETTER E WITH DIAERESIS */
	{0x00CC, 'I'}, /* LATIN CAPITAL LETTER I WITH GRAVE */
	{0x00CD, 'I'}, /* LATIN CAPITAL LETTER I WITH ACUTE */
	{0x00CE, 'I'}, /* LATIN CAPITAL LETTER I WITH CIRCUMFLEX */
	{0x00CF, 'I'}, /* LATIN CAPITAL LETTER I WITH DIAERESIS */
	{0x00D0, 'D'}, /* LATIN CAPITAL LETTER ETH */
	{0x00D1, 'N'}, /* LATIN CAPITAL LETTER N WITH TILDE */
	{0x00D2, 'O'}, /* LATIN CAPITAL LETTER O WITH GRAVE */
	{0x00D3, 'O'}, /* LATIN CAPITAL LETTER O WITH ACUTE */
	{0x00D4, 'O'}, /* LATIN CAPITAL LETTER O WITH CIRCUMFLEX */
	{0x00D5, 'O'}, /* LATIN CAPITAL LETTER O WITH TILDE */
	{0x00D6, 'O'}, /* LATIN CAPITAL LETTER O WITH DIAERESIS */
	{0x00D8, 'O'}, /* LATIN CAPITAL LETTER O WITH STROKE */
	{0x00D9, 'U'}, /* LATIN CAPITAL LETTER U WITH GRAVE */
	{0x00DA, 'U'}, /* LATIN CAPITAL LETTER U WITH ACUTE */
	{0x00DB, 'U'}, /* LATIN CAPITAL LETTER U WITH CIRCUMFLEX */
	{0x00DC, 'U'}, /* LATIN CAPITAL LETTER U WITH DIAERESIS */
	{0x00DD, 'Y'}, /* LATIN CAPITAL LETTER Y WITH ACUTE */
	{0x00DF, 'B'}, /* LATIN SMALL LETTER SHARP S */
	{0x00E0, 'a'}, /* LATIN SMALL LETTER A WITH GRAVE */
	{0x00E1, 'a'}, /* LATIN SMALL LETTER A WITH ACUTE */
	{0x00E2, 'a'}, /* LATIN SMALL LETTER A WITH CIRCUMFLEX */
	{0x00E3, 'a'}, /* LATIN SMALL LETTER A WITH TILDE */
	{0x00E4, 'a'}, /* LATIN SMALL LETTER A WITH DIAERESIS */
	{0x00E5, 'a'}, /* LATIN SMALL LETTER A WITH RING ABOVE */
	{0x00E6, 'a'}, /* LATIN SMALL LETTER AE */
	{0x00E7, 'c'}, /* LATIN SMALL LETTER C WITH CEDILLA */
	{0x00E8, 'e'}, /* LATIN SMALL LETTER E WITH GRAVE */
	{0x00E9, 'e'}, /* LATIN SMALL LETTER E WITH ACUTE */
	{0x00EA, 'e'}, /* LATIN SMALL LETTER E WITH CIRCUMFLEX */
	{0x00EB, 'e'}, /* LATIN SMALL LETTER E WITH DIAERESIS */
	{0x00EC, 'i'}, /* LATIN SMALL LETTER I WITH GRAVE */
	{0x00ED, 'i'}, /* LATIN SMALL LETTER I WITH ACUTE */
	{0x00EE, 'i'}, /* LATIN SMALL LETTER I WITH CIRCUMFLEX */
	{0x00EF, 'i'}, /* LATIN SMALL LETTER I WITH DIAERESIS */
	{0x00F1, 'n'}, /* LATIN SMALL LETTER N WITH TILDE */
	{0x00F2, 'o'}, /* LATIN SMALL LETTER O WITH GRAVE */
	{0x00F3, 'o'}, /* LATIN SMALL LETTER O WITH ACUTE */
	{0x00F4, 'o'}, /* LATIN SMALL LETTER O WITH CIRCUMFLEX */
	{0x00F5, 'o'}, /* LATIN SMALL LETTER O WITH TILDE */
	{0x00F6, 'o'}, /* LATIN SMALL LETTER O WITH DIAERESIS */
	{0x00F8, 'o'}, /* LATIN SMALL LETTER O WITH STROKE */
	{0x00F9, 'u'}, /* LATIN SMALL LETTER U WITH GRAVE */
	{0x00FA, 'u'}, /* LATIN SMALL LETTER U WITH ACUTE */
	{0x00FB, 'u'}, /* LATIN SMALL LETTER U WITH CIRCUMFLEX */
	{0x00FC, 'u'}, /* LATIN SMALL LETTER U WITH DIAERESIS */
	{0x00FD, 'y'}, /* LATIN SMALL LETTER Y WITH ACUTE */
	{0x00FF, 'y'}, /* LATIN SMALL LETTER Y WITH DIAERESIS */
	{0x0100, 'A'}, /* LATIN CAPITAL LETTER A WITH MACRON */
	{0x0101, 'a'}, /* LATIN SMALL LETTER A WITH MACRON */
	{0x0102, 'A'}, /* LATIN CAPITAL LETTER A WITH BREVE */
	{0x0103, 'a'}, /* LATIN SMALL LETTER A WITH BREVE */
	{0x0104, 'A'}, /* LATIN CAPITAL LETTER A WITH OGONEK */
	{0x0105, 'a'}, /* LATIN SMALL LETTER A WITH OGONEK */
	{0x0106, 'C'}, /* LATIN CAPITAL LETTER C WITH ACUTE */
	{0x0107, 'c'}, /* LATIN SMALL LETTER C WITH ACUTE */
	{0x0108, 'C'}, /* LATIN CAPITAL LETTER C WITH CIRCUMFLEX */
	{0x0109, 'c'}, /* LATIN SMALL LETTER C WITH CIRCUMFLEX */
	{0x010A, 'C'}, /* LATIN CAPITAL LETTER C WITH DOT ABOVE */
	{0x010B, 'c'}, /* LATIN SMALL LETTER C WITH DOT ABOVE */
	{0x010C, 'R'}, /* LATIN CAPITAL LETTER C WITH CARON */
	{0x010D, 'x'}, /* LATIN SMALL LETTER C WITH CARON */
	{0x010E, 'D'}, /* LATIN CAPITAL LETTER D WITH CARON */
	{0x010F, 'd'}, /* LATIN SMALL LETTER D WITH CARON */
	{0x0110, 'D'}, /* LATIN CAPITAL LETTER D WITH STROKE */
	{0x0111, 'd'}, /* LATIN SMALL LETTER D WITH STROKE */
	{0x0112, 'E'}, /* LATIN CAPITAL LETTER E WITH MACRON */
	{0x0113, 'e'}, /* LATIN SMALL LETTER E WITH MACRON */
	{0x0114, 'E'}, /* LATIN CAPITAL LETTER E WITH BREVE */
	{0x0115, 'e'}, /* LATIN SMALL LETTER E WITH BREVE */
	{0x0116, 'E'}, /* LATIN CAPITAL LETTER E WITH DOT ABOVE */
	{0x0117, 'e'}, /* LATIN SMALL LETTER E WITH DOT ABOVE */
	{0x0118, 'E'}, /* LATIN CAPITAL LETTER E WITH OGONEK */
	{0x0119, 'e'}, /* LATIN SMALL LETTER E WITH OGONEK */
	{0x011A, 'E'}, /* LATIN CAPITAL LETTER E WITH CARON */
	{0x011B, 'e'}, /* LATIN SMALL LETTER E WITH CARON */
	{0x011C, 'G'}, /* LATIN CAPITAL LETTER G WITH CIRCUMFLEX */
	{0x011D, 'g'}, /* LATIN SMALL LETTER G WITH CIRCUMFLEX */
	{0x011E, 'G'}, /* LATIN CAPITAL LETTER G WITH BREVE */
	{0x011F, 'g'}, /* LATIN SMALL LETTER G WITH BREVE */
	{0x0120, 'G'}, /* LATIN CAPITAL LETTER G WITH DOT ABOVE */
	{0x0121, 'g'}, /* LATIN SMALL LETTER G WITH DOT ABOVE */
	{0x0122, 'G'}, /* LATIN CAPITAL LETTER G WITH CEDILLA */
	{0x0123, 'g'}, /* LATIN SMALL LETTER G WITH CEDILLA */
	{0x0124, 'H'}, /* LATIN CAPITAL LETTER H WITH CIRCUMFLEX */
	{0x0125, 'h'}, /* LATIN SMALL LETTER H WITH CIRCUMFLEX */
	{0x0126, 'H'}, /* LATIN CAPITAL LETTER H WITH STROKE */
	{0x0127, 'h'}, /* LATIN SMALL LETTER H WITH STROKE */
	{0x0128, 'I'}, /* LATIN CAPITAL LETTER I WITH TILDE */
	{0x0129, 'i'}, /* LATIN SMALL LETTER I WITH TILDE */
	{0x012A, 'I'}, /* LATIN CAPITAL LETTER I WITH MACRON */
	{0x012B, 'i'}, /* LATIN SMALL LETTER I WITH MACRON */
	{0x012C, 'I'}, /* LATIN CAPITAL LETTER I WITH BREVE */
	{0x012D, 'i'}, /* LATIN SMALL LETTER I WITH BREVE */
	{0x012E, 'I'}, /* LATIN CAPITAL LETTER I WITH OGONEK */
	{0x012F, 'i'}, /* LATIN SMALL LETTER I WITH OGONEK */
	{0x0130, 'I'}, /* LATIN CAPITAL LETTER I WITH DOT ABOVE */
	{0x0131, 'i'}, /* LATIN SMALL LETTER DOTLESS I */
	{0x0134, 'J'}, /* LATIN CAPITAL LETTER J WITH CIRCUMFLEX */
	{0x0135, 'j'}, /* LATIN SMALL LETTER J WITH CIRCUMFLEX */
	{0x0136, 'K'}, /* LATIN CAPITAL LETTER K WITH CEDILLA */
	{0x0137, 'k'}, /* LATIN SMALL LETTER K WITH CEDILLA */
	{0x0139, 'L'}, /* LATIN CAPITAL LETTER L WITH ACUTE */
	{0x013A, 'l'}, /* LATIN SMALL LETTER L WITH ACUTE */
	{0x013B, 'L'}, /* LATIN CAPITAL LETTER L WITH CEDILLA */
	{0x013C, 'l'}, /* LATIN SMALL LETTER L WITH CEDILLA */
	{0x013D, 'L'}, /* LATIN CAPITAL LETTER L WITH CARON */
	{0x013E, 'l'}, /* LATIN SMALL LETTER L WITH CARON */
	{0x013F, 'L'}, /* LATIN CAPITAL LETTER L WITH MIDDLE DOT */
	{0x0140, 'l'}, /* LATIN SMALL LETTER L WITH MIDDLE DOT */
	{0x0141, 'L'}, /* LATIN CAPITAL LETTER L WITH STROKE */
	{0x0142, 'l'}, /* LATIN SMALL LETTER L WITH STROKE */
	{0x0143, 'N'}, /* LATIN CAPITAL LETTER N WITH ACUTE */
	{0x0144, 'n'}, /* LATIN SMALL LETTER N WITH ACUTE */
	{0x0145, 'N'}, /* LATIN CAPITAL LETTER N WITH CEDILLA */
	{0x0146, 'n'}, /* LATIN SMALL LETTER N WITH CEDILLA */
	{0x0147, 'N'}, /* LATIN CAPITAL LETTER N WITH CARON */
	{0x0148, 'n'}, /* LATIN SMALL LETTER N WITH CARON */
	{0x0149, 'n'}, /* LATIN SMALL LETTER N PRECEDED BY APOSTROPHE */
	{0x014C, 'O'}, /* LATIN CAPITAL LETTER O WITH MACRON */
	{0x014D, 'o'}, /* LATIN SMALL LETTER O WITH MACRON */
	{0x014E, 'O'}, /* LATIN CAPITAL LETTER O WITH BREVE */
	{0x014F, 'o'}, /* LATIN SMALL LETTER O WITH BREVE */
	{0x0150, 'O'}, /* LATIN CAPITAL LETTER O WITH DOUBLE ACUTE */
	{0x0151, 'o'}, /* LATIN SMALL LETTER O WITH DOUBLE ACUTE */
	{0x0152, 'O'}, /* LATIN CAPITAL LIGATURE OE */
	{0x0153, 'o'}, /* LATIN SMALL LIGATURE OE */
	{0x0154, 'R'}, /* LATIN CAPITAL LETTER R WITH ACUTE */
	{0x0155, 'r'}, /* LATIN SMALL LETTER R WITH ACUTE */
	{0x0156, 'R'}, /* LATIN CAPITAL LETTER R WITH CEDILLA */
	{0x0157, 'r'}, /* LATIN SMALL LETTER R WITH CEDILLA */
	{0x0158, 'R'}, /* LATIN CAPITAL LETTER R WITH CARON */
	{0x0159, 'r'}, /* LATIN SMALL LETTER R WITH CARON */
	{0x015A, 'S'}, /* LATIN CAPITAL LETTER S WITH ACUTE */
	{0x015B, 's'}, /* LATIN SMALL LETTER S WITH ACUTE */
	{0x015C, 'S'}, /* LATIN CAPITAL LETTER S WITH CIRCUMFLEX */
	{0x015D, 's'}, /* LATIN SMALL LETTER S WITH CIRCUMFLEX */
	{0x015E, 'S'}, /* LATIN CAPITAL LETTER S WITH CEDILLA */
	{0x015F, 's'}, /* LATIN SMALL LETTER S WITH CEDILLA */
	{0x0160, 'S'}, /* LATIN CAPITAL LETTER S WITH CARON */
	{0x0161, 's'}, /* LATIN SMALL LETTER S WITH CARON */
	{0x0162, 'T'}, /* LATIN CAPITAL LETTER T WITH CEDILLA */
	{0x0163, 't'}, /* LATIN SMALL LETTER T WITH CEDILLA */
	{0x0164, 'T'}, /* LATIN CAPITAL LETTER T WITH CARON */
	{0x0165, 't'}, /* LATIN SMALL LETTER T WITH CARON */
	{0x0166, 'T'}, /* LATIN CAPITAL LETTER T WITH STROKE */
	{0x0167, 't'}, /* LATIN SMALL LETTER T WITH STROKE */
	{0x0168, 'U'}, /* LATIN CAPITAL LETTER U WITH TILDE */
	{0x0169, 'u'}, /* LATIN SMALL LETTER U WITH TILDE */
	{0x016A, 'U'}, /* LATIN CAPITAL LETTER U WITH MACRON */
	{0x016B, 'u'}, /* LATIN SMALL LETTER U WITH MACRON */
	{0x016C, 'U'}, /* LATIN CAPITAL LETTER U WITH BREVE */
	{0x016D, 'u'}, /* LATIN SMALL LETTER U WITH BREVE */
	{0x016E, 'U'}, /* LATIN CAPITAL LETTER U WITH RING ABOVE */
	{0x016F, 'u'}, /* LATIN SMALL LETTER U WITH RING ABOVE */
	{0x0170, 'U'}, /* LATIN CAPITAL LETTER U WITH DOUBLE ACUTE */
	{0x0171, 'u'}, /* LATIN SMALL LETTER U WITH DOUBLE ACUTE */
	{0x0172, 'U'}, /* LATIN CAPITAL LETTER U WITH OGONEK */
	{0x0173, 'u'}, /* LATIN SMALL LETTER U WITH OGONEK */
	{0x0174, 'W'}, /* LATIN CAPITAL LETTER W WITH CIRCUMFLEX */
	{0x0175, 'w'}, /* LATIN SMALL LETTER W WITH CIRCUMFLEX */
	{0x0176, 'Y'}, /* LATIN CAPITAL LETTER Y WITH CIRCUMFLEX */
	{0x0177, 'y'}, /* LATIN SMALL LETTER Y WITH CIRCUMFLEX */
	{0x0178, 'Y'}, /* LATIN CAPITAL LETTER Y WITH DIAERESIS */
	{0x0179, 'Z'}, /* LATIN CAPITAL LETTER Z WITH ACUTE */
	{0x017A, 'z'}, /* LATIN SMALL LETTER Z WITH ACUTE */
	{0x017B, 'Z'}, /* LATIN CAPITAL LETTER Z WITH DOT ABOVE */
	{0x017C, 'z'}, /* LATIN SMALL LETTER Z WITH DOT ABOVE */
	{0x017D, 'Z'}, /* LATIN CAPITAL LETTER Z WITH CARON */
	{0x017E, 'z'}, /* LATIN SMALL LETTER Z WITH CARON */
	{0x0, 0},
};

char xiti_char_tab_latin1[256];
int xiti_char_tab_inited_latin1;

char xiti_char_tab_latin2[256];
int xiti_char_tab_inited_latin2;

char xiti_char_tab_greek[256];
int xiti_char_tab_inited_greek;

char xiti_char_tab_utf8[256];
int xiti_char_tab_inited_utf8;

struct xiti_sanitize_data {
	char *char_tab;
	int use_utf8;
};

static int
xiti_sanitize_outstring_raw(struct bpapi_output_chain *ochain, const char *str, size_t len) {
	const char *end = str + len;
	char *buf;
	unsigned int opos = 0;
	struct xiti_sanitize_data *data = ochain->data;

	if(!len)
		return 0;

	/* Even if input is UTF-8 as we produce us ascii we will never produce more bytes then input */
	buf = alloca(len);

	while (str < end) {
		if (data->use_utf8 && (*str & 0x80)) {
			int cp = 0;
			const struct xiti_transliterate_utf8 *u8tl = xiti_transliterate_utf8_def;

			utf8_char_safe(&str, end);

			/* XXX should be a binary search */
			while(u8tl->codepoint && u8tl->codepoint < cp) {
				u8tl++;
			}
			if(cp && u8tl->codepoint == cp) {
				buf[opos++] = u8tl->to;
			} else {
				buf[opos++] = '_';
			}
		} else {
			buf[opos++] = data->char_tab[*str++ & 0xFF];
		}
	}
	return bpo_outstring_raw(ochain->next, buf, opos);
}

static int
init_xiti_sanitize(const struct bpfilter *filter, struct bpapi *api, int argc, const struct tpvar **argv) {
	const char *encoding;
	char *xiti_char_tab;
	int *xiti_char_tab_inited;
	const struct xiti_transliterate_char *xiti_transliterate = NULL;
	char *fe;
	struct xiti_sanitize_data *data = calloc(1, sizeof(struct xiti_sanitize_data));

	if (argc)
		encoding = tpvar_str(argv[0], &fe);
	else
		xerrx(1, "no encoding given to xiti_sanitize");

	if (strcmp(encoding, "latin1") == 0) {
		xiti_char_tab = xiti_char_tab_latin1;
		xiti_char_tab_inited = &xiti_char_tab_inited_latin1;
		xiti_transliterate = xiti_transliterate_latin1;
	} else if (strcmp(encoding, "latin2") == 0) {
		xiti_char_tab = xiti_char_tab_latin2;
		xiti_char_tab_inited = &xiti_char_tab_inited_latin2;
		xiti_transliterate = xiti_transliterate_latin2;
	} else if (strcmp(encoding, "greek") == 0) {
		xiti_char_tab = xiti_char_tab_greek;
		xiti_char_tab_inited = &xiti_char_tab_inited_greek;
		xiti_transliterate = xiti_transliterate_greek;
	} else if (strcmp(encoding, "utf-8") == 0) {
		xiti_char_tab = xiti_char_tab_utf8;
		xiti_char_tab_inited = &xiti_char_tab_inited_utf8;
		data->use_utf8 = 1;
	} else {
		xerrx(1, "invalid encoding given to xiti_sanitize");
	}
	free(fe);

	if (!*xiti_char_tab_inited) {
		const unsigned char *ac = (const unsigned char *)xiti_allowed_chars;
		unsigned int i;

		for (i = 0; xiti_allowed_chars[i] != '\0'; i++) {
			xiti_char_tab[ac[i]] = ac[i];
		}

		if(xiti_transliterate) {
			while (xiti_transliterate->from) {
				const unsigned char *f = (const unsigned char *)xiti_transliterate->from;
				unsigned int j;

				for (j = 0; j < strlen((const char *)f); j++) {
					xiti_char_tab[f[j]] = xiti_transliterate->to;
				}
				xiti_transliterate++;
			}
		}

		for (i = 1; i < 256; i++) {
			if (xiti_char_tab[i] == '\0')
				xiti_char_tab[i] = '_';
		}

		xiti_char_tab[0] = 0;			/* Paranoia. */

		*xiti_char_tab_inited = 1;		/* Idempotent. No need to worry about atomicity. */
	}
	data->char_tab = xiti_char_tab;

	BPAPI_OUTPUT_DATA(api) = data;

	return 0;
}

static void
fini_xiti_sanitize(struct bpapi_output_chain *ochain) {
	struct xiti_sanitize_data *data = ochain->data;

	if(data) {
		free(data);
	}
}

static const struct bpapi_output xiti_sanitize_output = {
	self_outstring_fmt,
	xiti_sanitize_outstring_raw,
	fini_xiti_sanitize
};

ADD_OUTPUT_FILTER(xiti_sanitize, 0);
