#include "bpapi.h"
#include "util.h"
#include "ctemplates.h"
#include "logging.h"
#include "strl.h"
#include <stdarg.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include "avl.h"
#include "hash.h"
#include "ctemplates_internal.h"


/* buf output start */
static int
buf_outstring_fmt(struct bpapi_output_chain *ochain, const char *fmt, ...) {
	int res;
	struct buf_string *buf = ochain->data;

	va_list ap;

	va_start(ap, fmt);

	res = vbufcat(&buf->buf, &buf->len, &buf->pos, fmt, ap);
	va_end(ap);
	return res;
}

static int
buf_outstring_raw(struct bpapi_output_chain *ochain, const char *str, size_t len) {
	struct buf_string *buf = ochain->data;
	return bufwrite(&buf->buf, &buf->len, &buf->pos, str, len);
}

static void
buf_output_free(struct bpapi_output_chain *ochain) {
	struct buf_string *buf = ochain->data;

	if (buf) {
		if (buf->buf)
			free(buf->buf);
		free(buf);
	}
	ochain->data = NULL;
}

static
const struct bpapi_output buf_api_output = {
	buf_outstring_fmt,
	buf_outstring_raw,
	buf_output_free
};

struct buf_string *
buf_output_init(struct bpapi_output_chain *ochain) {
	struct buf_string *buf = zmalloc(sizeof (*buf));
	
	ochain->fun = &buf_api_output;
	ochain->data = buf;
	return buf;
}
/* buf output end */

static int
null_outstring_fmt(struct bpapi_output_chain *ochain, const char *fmt, ...) {
	return 1;	/* XXX - this is incorrect. */
}

static int
null_outstring_raw(struct bpapi_output_chain *ochain, const char *str, size_t len) {
	return len;
}

static const struct bpapi_output null_output = {
	null_outstring_fmt,
	null_outstring_raw,
	NULL
};

void
null_output_init(struct bpapi_output_chain *ochain) {
	ochain->fun = &null_output;
	ochain->data = NULL;
}

/* fd output start */
static int
fd_outstring_fmt(struct bpapi_output_chain *ochain, const char *fmt, ...) {
	int *fdp = ochain->data;
	va_list ap;
	int res;

	if (!fdp)
		return -1;

	va_start(ap, fmt);
	res = vdprintf(*fdp, fmt, ap);
	va_end(ap);
	return res;
}

static int
fd_outstring_raw(struct bpapi_output_chain *ochain, const char *str, size_t len) {
	int *fdp = ochain->data;
	if (!fdp)
		return -1;
	return write(*fdp, str, len);
}

static const struct bpapi_output fd_api_output = {
	fd_outstring_fmt,
	fd_outstring_raw,
	NULL
};

void
fd_output_init(struct bpapi_output_chain *ochain) {
	ochain->fun = &fd_api_output;
	ochain->data = NULL;
}

void
fd_output_set_fd(struct bpapi_output_chain *ochain, int *fdp) {
	ochain->data = fdp;
}

void
stdout_output_init(struct bpapi_output_chain *ochain) {
	static int fd = 1;
	fd_output_init(ochain);
	fd_output_set_fd(ochain, &fd);
}
/* fd output end */

/* hash (actually avl) simplevars start */
static int
tree_compare(const struct avl_node *an, const struct avl_node *bn) {
	struct hash_entry *a = avl_data(an, struct hash_entry, tree);
	struct hash_entry *b = avl_data(bn, struct hash_entry, tree);

	return strcmp(a->key, b->key);
}

#if 1
/*
 * This is the inline expanded avl_lookup function. It shaves quite a few
 * precious cycles from the most used code path in the whole template code
 * without increasing the cache footprint,  so it's worth the ugly. The
 * version in the else part is what the function should actually be. Notice
 * that the compare function call has the keys and result reversed compared to
 * avl_lookup on a blind guess about how the compiler will act on register
 * passing function call ABIs (it works). This might seem like total overkill
 * but the win is on the order of 7-10% for a macro benchmark.
 */
inline struct hash_entry *
hash_lookup(struct bpapi_simplevars_chain *schain, const char *key) {
	struct ctemplates_simplevars_data *d = schain->data;
	struct avl_node *n = d->keys;

	while (n) {
		struct hash_entry *e = avl_data(n, struct hash_entry, tree);
		int c;
#if 1
		/*
		 * This is a further nanooptimization - inlined strcmp.
		 * Shaves a few more cycles (on the order of 0.5% for a
		 * macro benchmark). Just like for the function,
		 * the else part is the equivalent readable part.
		 */
		unsigned int i;
		for (i = 0; (c = e->key[i] - key[i]) == 0; i++) {
			if (key[i] == 0)
				return e;
		}
		n = n->link[c < 0];
#else
		c = strcmp(e->key, key);
		if (c == 0)
			return e;
		n = n->link[c < 0];
#endif
	}
	return NULL;
}
#else
static struct hash_entry *
hash_lookup(struct bpapi_simplevars_chain *schain, const char *key) {
	struct ctemplates_simplevars_data *d = schain->data;
	struct hash_entry s;
	struct avl_node *n;
	s.key = key;
	n = avl_lookup(&s.tree, &d->keys, tree_compare);
	return n ? avl_data(n, struct hash_entry, tree) : NULL;
}
#endif

/*
	Specialized to support lookups when we have a non-terminated 'string' as the key.
*/
static inline struct hash_entry *
hash_lookup_klen(struct bpapi_simplevars_chain *schain, const char *key, int klen) {
	struct ctemplates_simplevars_data *d = schain->data;
	struct avl_node *n = d->keys;

	while (n) {
		struct hash_entry *e = avl_data(n, struct hash_entry, tree);
		int c = 1;
		int i;

		for (i = 0; i < klen && ((c = e->key[i] - key[i]) == 0); ++i) {
			/* nothing */
		}
		if (i == klen && e->key[i] == 0)
			return e;
		n = n->link[c < 0]; /* NOTE: if key was shorter than e->key and c == 0 (all matched) we pick e.key > key which is correct */
	}
	return NULL;
}

int
hash_has_key(struct bpapi_simplevars_chain *schain, const char *key) {
	return hash_lookup(schain, key) != NULL;
}

int
hash_length(struct bpapi_simplevars_chain *schain, const char *key) {
	struct hash_entry *e = hash_lookup(schain, key);
	return e ? e->len : 0;
}

static int
recurse_glob_loop(struct avl_it *it, const char ***dest, int pos) {
	struct avl_node *n;

	/*
	 * Depth first. We iterate to the end to figure out the number
	 * of matching elements, then allocate the storage for them
	 * and populate the array on our way back through the
	 * recursion.
	 */

	if ((n = avl_it_next(it)) != NULL) {
		struct hash_entry *ent;
		int res = recurse_glob_loop(it, dest, pos + 1);

		ent = avl_data(n, struct hash_entry, tree);
		if (dest)
			(*dest)[pos] = ent->key;
		return res;
	}

	if (dest) {
		if (pos)
			*dest = xmalloc(pos * sizeof(**dest));
		else
			*dest = NULL;
	}

	return pos;
}

static void
hash_fetch_glob_cleanup(struct bpapi_loop_var *loop) {
	free(loop->l.list);
	loop->l.list = NULL;
}

static int
glob_loop(struct bpapi_simplevars_chain *schain, const char *key, const char ***dest) {
	struct ctemplates_simplevars_data *d = schain->data;
	size_t len = strlen(key);
	char *ks = alloca(len);
	char *ke = alloca(len);
	struct hash_entry s, e;
	struct avl_it it;

	/*
	 * The AVL iterator returns elements [s,e)
	 * We set up the keys so that the first element of the glob
	 * is the first one that could possibly match and the last
	 * element to be the first one that couldn't possibly match.
	 */
	if (len == 1) {
		/* Just a '*' */
		avl_it_init(&it, d->keys, NULL, NULL, tree_compare);
	} else {
		/* Strip the trailing '*' */
		memcpy(ks, key, len);
		ks[len - 1] = '\0';

		memcpy(ke, key, len);
		ke[len - 2]++;
		ke[len - 1] = '\0';

		s.key = ks;
		e.key = ke;
		avl_it_init(&it, d->keys, &s.tree, &e.tree, tree_compare);
	}

	return recurse_glob_loop(&it, dest, 0);
}

void
hash_fetch_glob_loop(struct bpapi_simplevars_chain *schain, const char *key, struct bpapi_loop_var *loop) {
	loop->len = glob_loop(schain, key, &loop->l.list);
	if (loop->len)
		loop->cleanup = hash_fetch_glob_cleanup;
	else
		loop->cleanup = NULL;
}

int
keyglob_len(struct bpapi_simplevars_chain *schain, const char *key) {
	return glob_loop(schain, key, NULL);
}

static const char*
hash_get_element(struct bpapi_simplevars_chain *schain, const char *key, int pos) {
	struct hash_entry *e = hash_lookup(schain, key);

	return e ? e->valuelist[pos % e->len] : NULL;
}

void
hash_fetch_loop(struct bpapi_simplevars_chain *schain, const char *key, struct bpapi_loop_var *loop) {
	struct hash_entry *e;

	if ((e = hash_lookup(schain, key)) != NULL) {
		loop->len = e->len;
		loop->l.list = e->valuelist;
	} else {
		loop->len = 0;
		loop->l.list = NULL;
	}
	loop->cleanup = NULL;
}

void
hash_insert(struct bpapi_simplevars_chain *schain, const char *key, int klen, const char *value, int vlen) {
	struct ctemplates_simplevars_data *d = schain->data;
	struct hash_entry *e;

	if (klen < 0)
		klen = strlen(key);

	if ((e = hash_lookup_klen(schain, key, klen)) == NULL) {
		e = xmalloc(sizeof(*e) + klen + 1);
		e->key = (char *)(e + 1);
		memcpy((char *)e->key, key, klen);
		((char*)e->key)[klen] = 0;
		e->len = 0;
		e->valuelistlen = 1;
		e->valuelist = &e->one_val;
		e->tmpval = NULL;
		avl_insert(&e->tree, &d->keys, tree_compare);
		d->nkeys++;
	}

	e->free = 1;
	if (e->len == e->valuelistlen) {
		/* Grow list */
		e->valuelistlen *= 10;
		if (e->valuelist == &e->one_val) {
			e->valuelist = xmalloc(e->valuelistlen * sizeof (char*));
			e->valuelist[0] = e->one_val;
		} else {
			e->valuelist = xrealloc(e->valuelist, e->valuelistlen * sizeof (char*));
		}
	}

	if (vlen < 0)
		e->valuelist[e->len++] = xstrdup(value);
	else {
		char *duped_value = xmalloc(vlen+1);
		memcpy(duped_value, value, vlen);
		duped_value[vlen] = 0;
		e->valuelist[e->len++] = duped_value;
	}

	schain->cache_gen++;
}


void
insert_maxsize(struct bpapi_simplevars_chain *schain, const char *key, const char *value, int maxsize) {
	char new_string[maxsize + 1];

	strlcpy(new_string, value, maxsize + 1);
	bps_insert(schain, key, new_string);
}

static void
hash_insert_list(struct bpapi_simplevars_chain *schain, const char *key, const char **value_list, int len) {
	struct ctemplates_simplevars_data *d = schain->data;
	struct hash_entry *e;

	if ((e = hash_lookup(schain, key)) == NULL) {
		size_t klen = strlen(key) + 1;

		e = xmalloc(sizeof(*e) + klen);
		e->key = (char *)(e + 1);
		memcpy((char *)e->key, key, klen); /* copy terminator */
		avl_insert(&e->tree, &d->keys, tree_compare);
		d->nkeys++;
	} else {
		if (e->free) {
			int cnt;

			for (cnt = 0 ; cnt < e->len ; cnt++)
				free((char*)e->valuelist[cnt]);
			if (e->valuelist != &e->one_val)
				free(e->valuelist);
		}
	}

	e->valuelistlen = len;
	e->len = len;
	e->valuelist = value_list;
	e->free = 0;
	e->tmpval = NULL;
	schain->cache_gen++;
}

void
hash_simplevars_free(struct bpapi_simplevars_chain *schain) {
	struct ctemplates_simplevars_data *d = schain->data;
	struct hash_entry *entry, *head = NULL;
	struct avl_node *n;
	struct avl_it it;

	if (!d)
		return;

	avl_it_init(&it, d->keys, NULL, NULL, tree_compare);

	/*
	 * Removing the elements from the tree would be a pain since it would cause
	 * lots of rebalancing operations on a tree that will be nuked anyway.
	 * So instead of doing this nicely, walk the tree, build a list of elements
	 * to free with the one_val pointers, then sweep.
	 */
	while ((n = avl_it_next(&it)) != NULL) {
		entry = avl_data(n, struct hash_entry, tree);

		if (entry->free) {
			int i;

			for (i = 0; i < entry->len; i++)
				free((char*)entry->valuelist[i]);

			if (entry->valuelist != &entry->one_val)
				free(entry->valuelist);
		}
		if (entry->tmpval)
			free(entry->tmpval);

		entry->one_val = (const char *)head;
		head = entry;
	}

	while ((entry = head) != NULL) {
		head = (struct hash_entry *)(void*)entry->one_val;
		free(entry);
	}

	free(d);
	schain->data = NULL;
}

void
hash_init_var(struct bpapi_simplevars_chain *schain, const char *key, struct template_var *var) {
	var->entry = hash_lookup(schain, key);
	var->initialized = schain->cache_gen;
}

void
hash_delete(struct bpapi_simplevars_chain *schain, const char *key) {
	struct ctemplates_simplevars_data *d = schain->data;
	struct hash_entry *e;

	if ((e = hash_lookup(schain, key)) == NULL)
		return;

	avl_delete(&e->tree, &d->keys, tree_compare);

	if (e->free) {
		int i;

		for (i = 0; i < e->len; i++)
			free((char*)e->valuelist[i]);

		if (e->valuelist != &e->one_val)
			free(e->valuelist);
	}

	free(e->tmpval);
	free(e);	
}

static const struct bpapi_simplevars hash_api_simplevars = {
	hash_has_key,
	hash_length,
	hash_get_element,
	hash_fetch_loop,
	keyglob_len,
	hash_fetch_glob_loop,
	hash_insert,
	hash_insert_list,
	insert_maxsize,
	hash_delete,
	hash_simplevars_free,
	hash_init_var
};

void
hash_simplevars_init(struct bpapi_simplevars_chain *schain) {
	struct ctemplates_simplevars_data *d = xmalloc(sizeof(*d));

	d->nkeys = 0;
	d->keys = NULL;
	schain->data = d;
	schain->fun = &hash_api_simplevars;
	schain->cache_gen = 1;
}
