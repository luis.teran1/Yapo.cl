#ifndef TRUNC_H
#define TRUNC_H
#include "macros.h"

struct _trunc {
	int filt_len;
	int chars_left;
	char terminator[16];
	char break_char[8];
	int entity_aware;
	int break_char_found;
};
int trunc_outstring_raw(struct bpapi_output_chain *ochain, const char *str, size_t len) VISIBILITY_HIDDEN;
#endif
