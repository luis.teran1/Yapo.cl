#include <bpapi.h>

struct join_data
{
	struct buf_string buf;
	const char *separator;
	char *fs;
	int seplen;
	int nflushed;
};

static int 
join_outstring_raw(struct bpapi_output_chain *ochain, const char *str, size_t len) {
	struct join_data *jd = ochain->data;

	bufwrite(&jd->buf.buf, &jd->buf.len, &jd->buf.pos, str, len);

	return 0;
}

static int
init_join(const struct bpfilter *filter, struct bpapi *api, int argc, const struct tpvar **argv) {
	struct join_data *jd = BPAPI_OUTPUT_DATA(api);

	jd->separator = tpvar_str(argv[0], &jd->fs);
	jd->seplen = strlen(jd->separator);

	return 0;
}

static void
flush_join(struct bpapi_output_chain *ochain) {
	struct join_data *jd = ochain->data;

	if (jd->buf.buf && !is_ws(jd->buf.buf)) {
		if (jd->nflushed++) {
			bpo_outstring_raw(ochain->next, jd->separator, jd->seplen);
		}
		bpo_outstring_raw(ochain->next, jd->buf.buf, jd->buf.pos);
		free(jd->buf.buf);
		jd->buf.buf = NULL;
		jd->buf.pos = jd->buf.len = 0;
	}
}

static void
fini_join(struct bpapi_output_chain *ochain) {
	struct join_data *jd = ochain->data;

	if (jd->buf.buf) {
		bpo_outstring_raw(ochain->next, jd->buf.buf, jd->buf.pos);
		free(jd->buf.buf);
	}
	free(jd->fs);
}

const struct bpapi_output join_output = {
	self_outstring_fmt,
	join_outstring_raw,
	fini_join,
	flush_join
};

ADD_OUTPUT_FILTER(join, sizeof(struct join_data));
