#include <bpapi.h>
#include <string.h>
#include "sbo.h"

struct sbo_data {
	char salt;
	struct buf_string buf;
};

static int
sbo_outstring_raw(struct bpapi_output_chain *ochain, const char *str, size_t len) {
	struct sbo_data *data = ochain->data;

	bufwrite(&data->buf.buf, &data->buf.len, &data->buf.pos, str, len);
	return 0;
}

static void
flush_sbo(struct bpapi_output_chain *ochain) {
	struct sbo_data *data = ochain->data;
	int i, n;
	int32_t val = 0;
	char coded[9];

	for (i = n = 0 ; i < data->buf.pos ; i++) {
		if (data->buf.buf[i] >= '0' && data->buf.buf[i] <= '9') {
			val = val << 4 | (data->buf.buf[i] - '0');
			n++;
		} else if (data->buf.buf[i] >= 'A' && data->buf.buf[i] <= 'F') {
			val = val << 4 | (data->buf.buf[i] - 'A' + 10);
			n++;
		} else if (data->buf.buf[i] >= 'a' && data->buf.buf[i] <= 'f') {
			val = val << 4 | (data->buf.buf[i] - 'a' + 10);
			n++;
		}
		if (n >= 8) {
			sbo_encode(coded, val, data->salt);
			bpo_outstring_raw(ochain->next, coded, 8);
			n = 0;
			val = 0;
		}
	}
	if (n > 0) {
		sbo_encode(coded, val, data->salt);
		bpo_outstring_raw(ochain->next, coded, 8);
	}
	free(data->buf.buf);
	data->buf.pos = data->buf.len = 0;
}

static int
init_sbo(const struct bpfilter *filter, struct bpapi *api, int argc, const struct tpvar **argv) {
	struct sbo_data *data = BPAPI_OUTPUT_DATA(api);
	const char *salt;

	TPVAR_STRTMP(salt, argv[0]);

	data->salt = salt[0];

	return 0;
}

const struct bpapi_output sbo_output = {
	self_outstring_fmt,
	sbo_outstring_raw,
	NULL,
	flush_sbo
};

ADD_OUTPUT_FILTER(sbo, sizeof(struct sbo_data));
