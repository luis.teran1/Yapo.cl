#include <bpapi.h>

struct remove_data
{
	const char *chars;
	char *fc;
};

static int 
remove_outstring_raw(struct bpapi_output_chain *ochain, const char *str, size_t len) {
	struct remove_data *data = ochain->data;
	char *str_remove = xmalloc(len + 1);
	char *tmp = str_remove;
	int res;
	const char *filter = data->chars;
	const char *end = str + len;

	while (str < end) {
		if (!strchr(filter, *str)) {
			*tmp = *str;
			tmp++;
		} else
			len--;
		str++;
	}

	res = bpo_outstring_raw(ochain->next, str_remove, len);
	free(str_remove);
	return res;
}

static int
init_remove(const struct bpfilter *filter, struct bpapi *api, int argc, const struct tpvar **argv) {
	struct remove_data *data = BPAPI_OUTPUT_DATA(api);

	if (argc)
		data->chars = tpvar_str(argv[0], &data->fc);

	return 0;
}

static void
fini_remove(struct bpapi_output_chain *ochain) {
	struct remove_data *data = ochain->data;

	free(data->fc);
}

const struct bpapi_output remove_output = {
	self_outstring_fmt,
	remove_outstring_raw,
	fini_remove
};

ADD_OUTPUT_FILTER(remove, sizeof(struct remove_data));
