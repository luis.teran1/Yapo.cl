#include <bpapi.h>
#include <unicode/utf8.h>
#include <unicode/uchar.h>
#include "trunc.h"
#include "strl.h"

/* START  trunc for utf8 */

static int
trunc_utf8_outstring_raw(struct bpapi_output_chain *ochain, const char *str, size_t len) {
	struct _trunc *trunc = ochain->data;
	int i;
	UChar32 ch;
	char buf[5];
	int j, length = (int) len;
	int res = 0;
	if (trunc->chars_left <= 0) {
		trunc->chars_left = -1;
		return 0;
	}
	for (i = 0 ; i < length ; ) {
		/* get out if no chars left */
		if (trunc->chars_left <= 0) {
			trunc->chars_left = -1;
			break;
		}
		U8_NEXT(str, i, length, ch);
		if (ch == U_SENTINEL) {
			break;
		}
		j = 0;
		U8_APPEND_UNSAFE(buf, j, ch);
		/* count down one char */
		trunc->chars_left--;
		res += bpo_outstring_raw(ochain->next, buf, j);
	}
	return res;
}

static int
init_trunc_utf8(const struct bpfilter *filter, struct bpapi *api, int argc, const struct tpvar **argv) {
	struct _trunc *trunc = BPAPI_OUTPUT_DATA(api);

	switch (argc) {
	case 2:
		tpvar_strcpy(trunc->terminator, argv[1], sizeof(trunc->terminator));
	case 1:
		trunc->filt_len = tpvar_int(argv[0]);
	}
	if (argc < 2)
		strlcpy(trunc->terminator, "...", sizeof(trunc->terminator));
	trunc->chars_left = trunc->filt_len;

	return 0;
}

static void
flush_trunc_utf8(struct bpapi_output_chain *ochain) {
	struct _trunc *trunc = ochain->data;

	if (trunc->chars_left == -1) {
		bpo_outstring_raw(ochain->next, trunc->terminator, strlen(trunc->terminator));
	}
	trunc->chars_left = trunc->filt_len;
}

static const struct bpapi_output trunc_utf8_output = {
	self_outstring_fmt,
	trunc_utf8_outstring_raw,
	NULL,
	flush_trunc_utf8
};

ADD_OUTPUT_FILTER(trunc_utf8, sizeof(struct _trunc));
