#include <bpapi.h>
#include <unicode/utf8.h>
#include <unicode/uchar.h>
#include "upper.h"

static int
upper_utf8_outstring_raw(struct bpapi_output_chain *ochain, const char *str, size_t len) {
	int i;
	UChar32 ch;
	char buf[5];
	int j;
	int res = 0;

	for (i = 0 ; i < (int)len ; ) {
		U8_NEXT(str, i, (int)len, ch);
		if (ch == U_SENTINEL)
			break;

		ch = u_toupper(ch);
		j = 0;
		U8_APPEND_UNSAFE(buf, j, ch);
		res += bpo_outstring_raw(ochain->next, buf, j);
	}
	return res;
}

static int
init_upper_utf8(const struct bpfilter *filter, struct bpapi *api, int argc, const struct tpvar **argv) {
	return 0;
}

const struct bpapi_output upper_utf8_output = {
	self_outstring_fmt,
	upper_utf8_outstring_raw,
	NULL,
	NULL
};

ADD_OUTPUT_FILTER(upper_utf8, 0);
