#include <bpapi.h>
#include "base64.h"

static int
init_base64_decode(const struct bpfilter *filter, struct bpapi *api, int argc, const struct tpvar **argv) {
	return 0;
}

static void
fini_base64_decode(struct bpapi_output_chain *ochain) {
	struct buf_string *buf = ochain->data;
	int len = buf->pos;
	size_t sz = BASE64DECODE_NEEDED(len);
	char *b64 = zmalloc(sz);
	ssize_t out_len;

	out_len = base64_decode(b64, buf->buf, len);

	bpo_outstring_raw(ochain->next, b64, out_len);

	free(b64);
	free(buf->buf);
}

const struct bpapi_output base64_decode_output = {
	bufstring_outstring_fmt,
	bufstring_outstring_raw,
	fini_base64_decode
};

ADD_OUTPUT_FILTER(base64_decode, sizeof(struct buf_string));
