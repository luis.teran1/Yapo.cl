#ifndef DEFINE_H
#define DEFINE_H
#include "macros.h"
struct _define {
	struct bpapi_simplevars_chain *schain;
	const char *name;
	char *fn;
	char *buf;
	int len;
	int pos;
	char delim[16]; /* delimeter used in define_split and define_keyval. (default ","/"&") */
	const char *extra; /* extra data used in keyval for "define only this name" */
	char *fe;
	int append; /* 1 = append values to existing key. 0 = redefine. (default 0) */
	int even_if_empty;
};
int define_outstring_raw(struct bpapi_output_chain *ochain, const char *str, size_t len) VISIBILITY_HIDDEN;
#endif
