#include <ctype.h>
#include <bpapi.h>
#include <stdio.h>
#include <unicode/utf8.h>
#include <unicode/uchar.h>
#include "cached_regex.h"
#include "qp.h"

/* Max length of mime qp lines - 2 (to count for terminaing characters) */
#define MIME_QP_LINE_LENGTH (76-2)

int 
qp_outstring_raw(struct bpapi_output_chain *ochain, const char *str, size_t len) {
	struct _qp *qp = ochain->data;
	const char *start = str;
	const char *end = str + len;
	const char *tmp = start;
	char buf[4];
	int res = 0;

	while (tmp <= end) {
		/* Print header */
		if (*qp->header_encoding) {
			if (qp->header_started == 0) {
				while (tmp < end && *tmp != ':')
					tmp++;

				if (tmp < end) {
					static struct cached_regex has_address = { "^(From|To|CC|Reply-To):", PCRE_CASELESS };

					if (cached_regex_match(&has_address, start, NULL, 0)) {
						/* XXX somewhat ugly, maybe abuseable. */
						qp->has_address = 1;
					} else {
						qp->has_address = 0;
					}
					qp->header_started = 1;
					tmp++;
				}
			}
			if (qp->header_started == 1) {
				while (isspace(*tmp))
					tmp++;
				qp->header_started = 2;
			}
			if (qp->header_started == 2) {
				if (tmp < end) {
					int strres;
					char *str;

					qp->header_started = 3;

					ALLOCA_PRINTF(strres, str, "%.*s=?%s?Q?", (int)(tmp - start), start, qp->header_encoding);
					res += bpo_outstring_raw(ochain->next, str, strres);
					qp->line_length += strres;
					start = tmp;
				}
			}
		}

		if (tmp == end) {
			if (tmp != start) {
				res += bpo_outstring_raw(ochain->next, start, tmp - start);
				start = tmp;
			}
			break;
		}

		/* Append terminator */
		if (qp->line_length + 1 >= MIME_QP_LINE_LENGTH) {
			if (tmp != start) {
				res += bpo_outstring_raw(ochain->next, start, tmp - start);
				start = tmp;
			}

			if (*qp->header_encoding && qp->header_started)
				res += bpo_outstring_raw(ochain->next, "?=\n ", 4);
			else
				res += bpo_outstring_raw(ochain->next, "=\n", 2);

			qp->line_length = 0;
			qp->header_started = 2;
			continue;
		}

		/* Handle newlines in input */
		if (*tmp == '\n') {
			qp->line_length = 0;

			tmp++;
			continue;
		}

		/* Encode special characters */
		if ((unsigned char) *tmp >= 0x80 || *tmp == '=') {
			/* If we are doing utf-8 encode whole codepoints not single byte*/
			if (qp->header_started && strcmp(qp->header_encoding, "UTF-8") == 0) {
				char ubuf[4*3 + 1];
				UChar32 ch;
				int j = 0;
				int i = 0;

				if (qp->line_length + 3*4 > MIME_QP_LINE_LENGTH) {
					qp->line_length = MIME_QP_LINE_LENGTH;
					continue;
				}

				if (tmp != start) {
					res += bpo_outstring_raw(ochain->next, start, tmp - start);
				}

				U8_NEXT(tmp, i, end-tmp, ch);
				tmp += i;
				if (ch == U_SENTINEL) {
					continue;
				}
				for (j = 0; j < i; ++j) {
					snprintf(ubuf + 3*j, 4, "=%.2hhX", (unsigned char) *(tmp - i + j));
				}
				res += bpo_outstring_raw(ochain->next, ubuf, strlen(ubuf));
				qp->line_length += strlen(ubuf);
				start = tmp;
			} else {
				if (qp->line_length + 3 > MIME_QP_LINE_LENGTH) {
					qp->line_length = MIME_QP_LINE_LENGTH;
					continue;
				}

				if (tmp != start) {
					res += bpo_outstring_raw(ochain->next, start, tmp - start);
				}

				snprintf(buf, sizeof (buf), "=%.2hhX", *tmp);
				res += bpo_outstring_raw(ochain->next, buf, strlen(buf));
				qp->line_length += strlen(buf);

				start = ++tmp;
			}
			continue;
		}

		/* Encode special chars in header */
		if (*qp->header_encoding) {
			switch (*tmp) {
				case ' ':
					if (tmp != start) {
						res += bpo_outstring_raw(ochain->next, start, tmp - start);
					}
					if (qp->has_address && tmp < end && *(tmp + 1) == '<') {
						/* XXX won't work if different outstring calls. */
						if (qp->line_length + (end - tmp - 1) + 2 > MIME_QP_LINE_LENGTH) {
							res += bpo_outstring_raw(ochain->next, "_?=\n ", 5);
							qp->line_length = 1;
						} else {
							res += bpo_outstring_raw(ochain->next, "?= ", 3);
							qp->line_length += 3;
						}
						qp->header_started = 0;
					} else {
						res += bpo_outstring_raw(ochain->next, "_", 1);
						qp->line_length += 1;
					}
					start = ++tmp;
					continue;

				case '(':
				case ')':
				case '@':
				case ',':
				case ';':
				case ':':
				case '<':
				case '>':
				case '/':
				case '[':
				case ']':
				case '?':
				case '_':
					if (qp->line_length + 3 > MIME_QP_LINE_LENGTH) {
						qp->line_length = MIME_QP_LINE_LENGTH;
						continue;
					}

					if (tmp != start) {
						res += bpo_outstring_raw(ochain->next, start, tmp - start);
					}

					snprintf(buf, sizeof (buf), "=%.2hhX", *tmp);
					res += bpo_outstring_raw(ochain->next, buf, strlen(buf));
					qp->line_length += strlen(buf);
					start = ++tmp;
					continue;
			}
		}

		qp->line_length++;
		tmp++;
	}

	return res;
}

static int
init_qp(const struct bpfilter *filter, struct bpapi *api, int argc, const struct tpvar **argv) {
	struct _qp *qp = BPAPI_OUTPUT_DATA(api);

	if (argc)
		tpvar_strcpy(qp->header_encoding, argv[0], sizeof(qp->header_encoding));

	return 0;
}

static void
fini_qp(struct bpapi_output_chain *ochain) {
	struct _qp *qp = ochain->data;

	/* Print qp terminator if marked as an header */
	if (*qp->header_encoding && qp->header_started)
		bpo_outstring_raw(ochain->next, "?=", 2);
}

static const struct bpapi_output qp_output = {
	self_outstring_fmt,
	qp_outstring_raw,
	fini_qp
};

ADD_OUTPUT_FILTER(qp, sizeof (struct _qp));
