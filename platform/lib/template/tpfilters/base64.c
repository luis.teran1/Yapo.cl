#include <bpapi.h>
#include "base64.h"

enum base64mode {
	BASE64_MIME,
	BASE64_URL,
};

struct base64state {
	struct buf_string buf;
	enum base64mode mode;
	int line_limit;
};

static int
init_base64(const struct bpfilter *filter, struct bpapi *api, int argc, const struct tpvar **argv) {
	struct base64state *state = BPAPI_OUTPUT_DATA(api);

	state->mode = BASE64_MIME;
	state->line_limit = 72;
	switch (argc) {
	case 1:
		state->line_limit = tpvar_int(argv[0]);
	}

	return 0;
}

static int
init_base64url(const struct bpfilter *filter, struct bpapi *api, int argc, const struct tpvar **argv) {
	struct base64state *state = BPAPI_OUTPUT_DATA(api);

	state->mode = BASE64_URL;
	state->line_limit = 0;

	return 0;
}

static void
fini_base64(struct bpapi_output_chain *ochain) {
	struct base64state *state = ochain->data;
	int len = state->buf.pos;
	size_t sz =  BASE64_NEEDED(len);
	char *b64 = zmalloc(sz);
	char *row;
	char *s;
	ssize_t encoded;

	if (state->mode == BASE64_URL)
		encoded = base64url_encode(b64, state->buf.buf, len);
	else
		encoded = base64_encode(b64, state->buf.buf, len);

	s = row = b64;

	if (state->line_limit > 0) {
		while (*s) {
			if ((++s - row) % state->line_limit == 0) {
				bpo_outstring_raw(ochain->next, row, state->line_limit);
				bpo_outstring_raw(ochain->next, "\n", 1);
				row = s;
			}
		}
		bpo_outstring_raw(ochain->next, row, s - row);
	} else if (encoded > 0) {
		bpo_outstring_raw(ochain->next, row, encoded);
	}

	free(b64);
	free(state->buf.buf);
}

const struct bpapi_output base64_output = {
	bufstring_outstring_fmt,
	bufstring_outstring_raw,
	fini_base64
};

const struct bpapi_output base64url_output = {
	bufstring_outstring_fmt,
	bufstring_outstring_raw,
	fini_base64
};

ADD_OUTPUT_FILTER(base64, sizeof(struct base64state));
ADD_OUTPUT_FILTER(base64url, sizeof(struct base64state));
