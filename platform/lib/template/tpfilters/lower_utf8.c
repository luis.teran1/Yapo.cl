#include <bpapi.h>
#include <unicode/utf8.h>
#include <unicode/uchar.h>

static int
lower_utf8_outstring_raw(struct bpapi_output_chain *ochain, const char *str, size_t len) {
	int i;
	UChar32 ch;
	char buf[5];
	int j;
	int res = 0;

	for (i = 0 ; i < (int)len ; ) {
		U8_NEXT(str, i, (int)len, ch);
		if (ch == U_SENTINEL)
			break;

		ch = u_tolower(ch);
		j = 0;
		U8_APPEND_UNSAFE(buf, j, ch);
		res += bpo_outstring_raw(ochain->next, buf, j);
	}
	return res;
}

static int
init_lower_utf8(const struct bpfilter *filter, struct bpapi *api, int argc, const struct tpvar **argv) {
	return 0;
}

const struct bpapi_output lower_utf8_output = {
	self_outstring_fmt,
	lower_utf8_outstring_raw,
	NULL,
	NULL
};

ADD_OUTPUT_FILTER(lower_utf8, 0);
