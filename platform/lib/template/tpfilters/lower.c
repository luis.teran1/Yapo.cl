#include <bpapi.h>
#include <ctype.h>
#include "lower.h"

char
local_lower(char c) {
	switch (c) {
		case '\xC5': /* Å */
			c = '\xE5'; /* å */
			break;
		case '\xC4': /* Ä */
			c = '\xE4'; /* ä */
			break;
		case '\xD3': /* Ó */
			c = '\xF3'; /* ó */
			break;
		case '\xD6': /* Ö */
			c = '\xF6'; /* ö */
			break;
		case '\xDC': /* Ü */
			c = '\xFC'; /* ü */
			break;
		case '\xC9': /* É */
			c = '\xE9'; /* é */
			break;
		case '\xC8': /* È */
			c = '\xE8'; /* è */
			break;
		case '\xC0': /* À */
			c = '\xE0'; /* à */
			break;
		case '\xC1': /* Á */
			c = '\xE1'; /* á */
			break;
		case '\xCA': /* Ê */
			c = '\xEA'; /* ê */
			break;
		case '\xC7': /* Ç */
			c = '\xE7'; /* ç */
			break;
		case '\xD1': /* Ñ */
			c = '\xF1'; /* ñ */
			break;
		case '\xC2': /* Â */
			c = '\xE2'; /* â */
			break;
		case '\xD8': /* Ø */
			c = '\xF8'; /* ø */
			break;
		case '\xC6': /* Æ */
			c = '\xE6'; /* æ */
			break;
		case '\xD4': /* Ô */
			c = '\xF4'; /* ô */
			break;
		case '\xD5': /* Õ */
			c = '\xF5'; /* õ */
			break;
		case '\xDA': /* Ú */
			c = '\xFA'; /* ú */
			break;
		case '\xDB': /* Û */
			c = '\xFB'; /* û */
			break;
		case '\xCB': /* Ë */
			c = '\xEB'; /* ë */
			break;
		case '\xCE': /* Î */
			c = '\xEE'; /* î */
			break;
		case '\xCF': /* Ï */
			c = '\xEF'; /* ï */
			break;
		case '\xCD': /* Í */
			c = '\xED'; /* í */
			break;
		case '\xCC': /* Ì */
			c = '\xEC'; /* ì */
			break;
		case '\xD9': /* Ù */
			c = '\xF9'; /* ù */
			break;
	}
	return c;
}

static int
lower_outstring_raw(struct bpapi_output_chain *ochain, const char *str, size_t len) {
	char *str_lower = xmalloc(len + 1);
	char *tmp = str_lower;
	int res;
	const char *end = str + len;

	while (str < end) {
		*tmp = local_lower(tolower(*str));
		tmp++;
		str++;
	}

	*tmp = '\0';

	res = bpo_outstring_raw(ochain->next, str_lower, len);
	free(str_lower);
	return res;
}

static int
init_lower(const struct bpfilter *filter, struct bpapi *api, int argc, const struct tpvar **argv) {
	return 0;
}

const struct bpapi_output lower_output = {
	self_outstring_fmt,
	lower_outstring_raw,
	NULL,
	NULL
};

ADD_OUTPUT_FILTER(lower, 0);
