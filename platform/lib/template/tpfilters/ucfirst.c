#include <bpapi.h>
#include <ctype.h>
#include "upper.h"
#include "lower.h"

struct _ucfirst {
	int first_only; /* If set, _only_ the first character is uppercased */
	int reset_on_special; /* Applicable on when first_only. If set, will reset "uppercaseification" after amps */
	int has_amp;
};

static int 
ucfirst_outstring_raw(struct bpapi_output_chain *ochain, const char *str, size_t len) {
	struct _ucfirst *ucfirst = ochain->data;
	char *str_ucfirst = xmalloc(len + 1);
	char *tmp = str_ucfirst;
	int res;
	const char *end = str + len;

	while (str < end) {
		if (tmp == str_ucfirst || ((!ucfirst->first_only || ucfirst->has_amp) && tmp > str_ucfirst && *(tmp-1) == ' ')) {
			*tmp = local_upper(toupper(*str));
		} else  {
			*tmp = local_lower(tolower(*str));
		}

		if (ucfirst->reset_on_special) {
			if (*tmp == '&')
				ucfirst->has_amp = 1;
			else if (ucfirst->has_amp && *tmp != ' ')
				ucfirst->has_amp = 0;
		}

		tmp++;
		str++;
	}

	*tmp = '\0';

	res = bpo_outstring_raw(ochain->next, str_ucfirst, len);
	free(str_ucfirst);
	return res;
}

static int
init_ucfirst(const struct bpfilter *filter, struct bpapi *api, int argc, const struct tpvar **argv) {
	struct _ucfirst *ucfirst = BPAPI_OUTPUT_DATA(api);

	switch (argc) {
	case 2:
		ucfirst->reset_on_special = tpvar_int(argv[1]);
	case 1:
		ucfirst->first_only = tpvar_int(argv[0]);
	}
	ucfirst->has_amp = 0;

	return 0;
}

const struct bpapi_output ucfirst_output = {
	self_outstring_fmt,
	ucfirst_outstring_raw
};

ADD_OUTPUT_FILTER(ucfirst, sizeof(struct _ucfirst));
