#ifndef SORT_H
#define SORT_H
#include <locale.h>
#include <xlocale.h>
#include "macros.h"

#define SORT_PURGE_EMPTY	0x1
#define SORT_COLS_BREAK		0x2
#define SORT_CASE_SENSITIVE	0x4
#define GENERATE_ARRAY_NAME	0x8
#define SORT_NUMERIC		0x10
#define SORT_UNIQUE		0x20
#define SORT_DESC		0x40
#define SORT_LOCALE		0x80

#define SORT_FN(a, b) sortfn(a, b, data)
#define SORT_FN_DESC(a, b) sortfn(b, a, data)

struct sort_filter_elem {
	int idx;
	char *str;
	int len;
};

struct sort_filter_data {
	struct sort_filter_elem *elems;
	locale_t sort_locale;
	int num_arg;
	int num_elems;
	int elem_idx;
	int alloced_elems;
	int buf_len;
	int buf_pos;
	int num_rows;
	char **lists;
	int flags;
	char *list_prefix;
	char *list_name;
	struct bpapi_simplevars_chain *schain;
};
int sort_outstring_raw(struct bpapi_output_chain *ochain, const char *str, size_t len) VISIBILITY_HIDDEN;
void flush_sort(struct bpapi_output_chain *ochain) VISIBILITY_HIDDEN;

void filter_state_sort(struct bpapi *api, const char *key, struct filter_state_data *fsd, void *v);

static inline int
sort_elem_cmp(struct sort_filter_elem *a, struct sort_filter_elem *b, struct sort_filter_data *data) {
	if (a->str == NULL || b->str == NULL)
		return a->str < b->str;

	if (data->flags & SORT_CASE_SENSITIVE)
		return strcmp(a->str, b->str) < 0;
	return strcasecmp(a->str, b->str) < 0;
}

static inline int
sort_elem_locale_cmp(struct sort_filter_elem *a, struct sort_filter_elem *b, struct sort_filter_data *data) {
	if (a->str == NULL || b->str == NULL)
		return a->str < b->str;
	if (data->sort_locale)
		return strcoll_l(a->str, b->str, data->sort_locale) < 0;
	else
		return sort_elem_cmp(a, b, data);
}

static inline int
sort_elem_num_cmp(struct sort_filter_elem *a, struct sort_filter_elem *b, struct sort_filter_data *data) {
	if (a->str == NULL || b->str == NULL)
		return a->str < b->str;
	if (a->len != b->len)
		return  a->len < b->len;
	return strcasecmp(a->str, b->str) < 0;
}
#endif
