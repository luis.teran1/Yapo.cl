#include <string.h>
#include <bpapi.h>
#include <qsort.h>
#include "sort.h"
#include <logging.h>

static void
fs_sort_free(void *v) {
	freelocale(v);
}

void
filter_state_sort(struct bpapi *api, const char *key, struct filter_state_data *fsd, void *v) {
	const char *locale = vtree_get(&api->vchain, "common", "locale", NULL);
	locale_t l;

	if (!locale)
		locale = "C";

	l = newlocale(LC_COLLATE_MASK, locale, NULL);
	if (!l) {
		log_printf(LOG_WARNING, "filter_state_sort: newlocale(%s) failed", locale);
		return;
	}
	fsd->value = l;
	fsd->freefn = fs_sort_free;
	fsd->type = FS_THREAD;
}

/*
 * Sort filter (approx number of items, flags, number of lists, list names...)
 */
int 
sort_outstring_raw(struct bpapi_output_chain *ochain, const char *str, size_t len) {
	struct sort_filter_data *data = ochain->data;

	if (!data || !len)
		return 0;
	/*
	 * We're not guaranteed that outstring will be called for every flush, so we need to be careful
	 * about resizing the array until we catch up.
	 */
	while (data->num_elems >= data->alloced_elems) {
		data->alloced_elems *= 2;
		data->elems = xrealloc(data->elems, data->alloced_elems * sizeof (*data->elems));
		memset(&data->elems[data->num_elems], 0, sizeof(*data->elems) * (data->alloced_elems - data->num_elems));
	}

	bufwrite(&data->elems[data->num_elems].str, &data->buf_len, &data->buf_pos, str, len);
	return 0;
}

void
flush_sort(struct bpapi_output_chain *ochain) {
	struct sort_filter_data *data = ochain->data;

	if (!data)
		return;
	if (data->buf_pos || !(data->flags & SORT_PURGE_EMPTY)) {
		data->elems[data->num_elems].idx = data->elem_idx;
		data->elems[data->num_elems++].len = data->buf_pos;
		data->buf_pos = 0;
	}
	data->elem_idx++;
}

static int
init_sort(const struct bpfilter *filter, struct bpapi *api, int argc, const struct tpvar **argv) {
	struct sort_filter_data *data = BPAPI_OUTPUT_DATA(api);
	const char *flags;

	if (argc != 3)
		return 1;

	data->schain = &api->schain;
	data->alloced_elems = tpvar_int(argv[0]);
	if (!data->alloced_elems) {
		data->alloced_elems = 8;	/* XXX - just picking something > 0 */
	}

	TPVAR_STRTMP(flags, argv[1]);
	for (; *flags; flags++) {
		switch (*flags) {
		case 'P':
			data->flags |= SORT_PURGE_EMPTY;
			break;
		case 'n':
			data->flags |= SORT_NUMERIC;
			break;
		case 'u':
			data->flags |= SORT_UNIQUE;
			break;
		case 'r':
			data->flags |= SORT_DESC;
			break;
		case 'l':
			data->flags |= SORT_LOCALE;
			data->sort_locale = filter_state(api, "sort_locale", filter_state_sort, NULL);
			break;
		case 'c':
			data->flags |= SORT_CASE_SENSITIVE;
			break;
		}
	}

	data->list_name = tpvar_strdup(argv[2]);

	data->elems = zmalloc(sizeof (*data->elems) * data->alloced_elems);
	return 0;
}

static void
fini_sort(struct bpapi_output_chain *ochain) {
	int (*sortfn)(struct sort_filter_elem *, struct sort_filter_elem *, struct sort_filter_data *data);
	struct sort_filter_data *data = ochain->data;
	int i;

	if (!data)
		return;
	if (!data->num_elems)
		goto out;

	switch (data->flags & (SORT_NUMERIC|SORT_LOCALE)) {
	case SORT_NUMERIC:
		sortfn = sort_elem_num_cmp;
		break;
	case SORT_LOCALE:
		sortfn = sort_elem_locale_cmp;
		break;
	default:
		sortfn = sort_elem_cmp;
		break;
	}

	if (data->flags & SORT_DESC) {
		QSORT(struct sort_filter_elem, data->elems, data->num_elems, SORT_FN_DESC);
	} else {
		QSORT(struct sort_filter_elem, data->elems, data->num_elems, SORT_FN);
	}

	/* Elements inserted into lists by rows */
	for (i = 0; i < data->num_elems; i++) {
		if (i && (data->flags & SORT_UNIQUE)) {
			/* Skip if both or NULL or if they are identical. */
			if (data->elems[i].str == NULL && data->elems[i - 1].str == NULL)
				continue;
			if (data->elems[i].str && data->elems[i - 1].str
					&& strcasecmp(data->elems[i].str, data->elems[i - 1].str) == 0)
				continue;
		}
		bps_set_int(data->schain, data->list_name, data->elems[i].idx);
	}
	for (i = 0; i < data->num_elems; i++)
		free(data->elems[i].str);

out:
	free(data->list_name);
	free(data->elems);
}

static const struct bpapi_output sort_output = {
	self_outstring_fmt,
	sort_outstring_raw,
	fini_sort,
	flush_sort
};

ADD_OUTPUT_FILTER(sort, sizeof(struct sort_filter_data));
