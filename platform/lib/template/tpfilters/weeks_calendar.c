#include <bpapi.h>
#include <time.h>
#include "patchvars.h"

struct weeks {
	int year;
	int week;
};

static void
setup_week_info(struct weeks *weeks, int *num_weeks, time_t now) {
	struct tm tm;
	struct date_rec thursday;
	int startwnum;
	int wnum;
	int week;

	localtime_r(&now, &tm);

	/* Find thursday of current week. */
	date_set(&thursday, &tm);
	if (tm.tm_wday == 0)
		date_set_day_offset(&thursday, 4 - 7);
	else if (tm.tm_wday != 4)
		date_set_day_offset(&thursday, 4 - tm.tm_wday);

	/* Find current week number. */
	startwnum = wnum = (thursday.day_of_year - 1) / 7 + 1;
	for (week = 0; week < 53; week++, wnum++) {
		if (thursday.day_of_year <= 7)
			wnum = 1;
		if (week > 50 && wnum == startwnum)
			break;

		weeks[week].year = thursday.year;
		weeks[week].week = wnum;

		date_set_day_offset(&thursday, 7);
	}
	*num_weeks = week;
}

static int
init_weeks_calendar(const struct bpfilter *filter, struct bpapi *api, int argc, const struct tpvar **argv) {
	struct patch_data *data = BPAPI_SIMPLEVARS_DATA(api);
	int week;
	struct weeks weeks[53];
	int num_weeks = 0;
	time_t now;

	patchvars_init(data, "weeks_calendar", "weeks_", sizeof("weeks_") - 1, NULL, 0);

	if (argc < 1)
		return 0;

	now = tpvar_int(argv[0]);

	setup_week_info(weeks, &num_weeks, now);

	for (week = 0; week < num_weeks; week++) {
		bps_set_int(&data->vars, "weeks_calendar_year", weeks[week].year);
		bps_set_int(&data->vars, "weeks_calendar_week", weeks[week].week);
		bps_set_printf(&data->vars, "weeks_calendar_value", "%d:%d", weeks[week].year, weeks[week].week);
	}

	return 0;
}
#define weeks_calendar_simplevars patch_simplevars
ADD_SIMPLEVARS_FILTER(weeks_calendar, sizeof(struct patch_data));
