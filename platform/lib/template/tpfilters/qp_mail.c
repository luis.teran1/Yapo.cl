#include <bpapi.h>
#include <string.h>
#include "qp.h"

struct qp_mail {
	struct _qp qp;
	struct buf_string buf;
	const char *multipart;
	char *fm;
};

static int
qp_mail_outstring_raw(struct bpapi_output_chain *ochain, const char *str, size_t len) {
	struct qp_mail *data = ochain->data;
	const char *eline;
	int res = 0;
	const char *l;

	if (!*data->qp.header_encoding)
		return qp_outstring_raw(ochain, str, len);

	bufwrite(&data->buf.buf, &data->buf.len, &data->buf.pos, str, len);
	while ((eline = memchr(data->buf.buf, '\n', data->buf.pos))) {
		if (eline == data->buf.buf) {
			if (data->multipart) {
				bpo_outstring_fmt(ochain->next,
						"MIME-Version: 1.0\n"
						"Content-Type: multipart/mixed;\n"
						"\tboundary=\"%s\"\n"
						"\n"
						"--%s\n", data->multipart, data->multipart);
			}

			/* XXX this part should probably be more configurable. */
			bpo_outstring_fmt(ochain->next,
					"Content-Type: text/plain; charset=%s\n"
					"Content-Transfer-Encoding: quoted-printable\n",
					data->qp.header_encoding);

			data->qp.header_encoding[0] = '\0';

			return res + qp_outstring_raw(ochain, data->buf.buf, data->buf.pos);
		}

		for (l = data->buf.buf ; l < eline ; l++) {
			if ((*l & 0x80) || *l == '=')
				break;
		}
		if (l < eline) {
			res += qp_outstring_raw(ochain, data->buf.buf, eline - data->buf.buf);
			if (data->qp.header_started == 3)
				res += bpo_outstring_raw(ochain->next, "?=\n", 3);
			else
				res += bpo_outstring_raw(ochain->next, "\n", 1);
		} else {
			res += bpo_outstring_raw(ochain->next, data->buf.buf, eline - data->buf.buf + 1);
		}

		data->buf.pos -= eline - data->buf.buf + 1;
		memmove(data->buf.buf, eline + 1, data->buf.pos + 1);
		data->qp.header_started = 0;
		data->qp.line_length = 0;
	}

	return res;
}

static int
init_qp_mail(const struct bpfilter *filter, struct bpapi *api, int argc, const struct tpvar **argv) {
	struct qp_mail *data = BPAPI_OUTPUT_DATA(api);

	tpvar_strcpy(data->qp.header_encoding, argv[0], sizeof(data->qp.header_encoding));
	if (!*data->qp.header_encoding)
		xerrx(1, "No encoding given to qp_mail filter");		/* XXX */
	if (argc > 1) {
		data->multipart = tpvar_str(argv[1], &data->fm);
		if (data->multipart && !*data->multipart)
			data->multipart = NULL;
	}

	return 0;
}

static void
fini_qp_mail(struct bpapi_output_chain *ochain) {
	struct qp_mail *data = ochain->data;

	if (data->buf.buf)
		free(data->buf.buf);
	free(data->fm);
}

static const struct bpapi_output qp_mail_output = {
	self_outstring_fmt,
	qp_mail_outstring_raw,
	fini_qp_mail
};

ADD_OUTPUT_FILTER(qp_mail, sizeof (struct qp_mail));
