#include <bpapi.h>
#include <ctype.h>

/* filter for trimming strings */
static int
trim_outstring_raw(struct bpapi_output_chain *ochain, const char *str, size_t len) {
	struct buf_string *buf = ochain->data;
	const char *cp;
	int res;

	if (buf->buf == (char*)-1) {
		/* ltrim */
		while (len > 0) {
			if (!isspace(*str))
				break;
			str++;
			len--;
		}
		if (!len)
			return 0;
		buf->buf = NULL;
	}

	/* rtrim (maybe) */
	for (cp = str + len - 1 ; cp >= str ; cp--) {
		if (!isspace(*cp))
			break;
	}

	if (cp < str) {
		bswrite(buf, str, len);
		return 0;
	}

	if (buf->pos) {
		bpo_outstring_raw(ochain->next, buf->buf, buf->pos);
		buf->pos = 0;
	}

	res = bpo_outstring_raw(ochain->next, str, cp - str + 1);
	if (cp < str + len - 1)
		bswrite(buf, cp + 1, str + len - cp - 1);

	return res;
}

static void
fini_trim(struct bpapi_output_chain *ochain) {
	struct buf_string *buf = ochain->data;

	if (buf->buf != (char*)-1)
		free(buf->buf);
}

static int
init_trim(const struct bpfilter *filter, struct bpapi *api, int argc, const struct tpvar **argv) {
	struct buf_string *buf = BPAPI_OUTPUT_DATA(api);

	buf->buf = (char*)-1;

	return 0;
}

const struct bpapi_output trim_output = {
	self_outstring_fmt,
	trim_outstring_raw,
	fini_trim,
	NULL
};

ADD_OUTPUT_FILTER(trim, sizeof (struct buf_string));
