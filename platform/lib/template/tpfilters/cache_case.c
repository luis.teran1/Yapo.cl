#include <string.h>
#include <bpapi.h>
#include <time.h>
#include <pthread.h>
#include "atomic.h"

/*
 * cache_case filter
 *
 * <%|cache_case(<tag>, <cache_bucket>, [<timeout>])|
 *
 * cache the contents of the tag in <cache_bucket> (use a construct to have multiple buckets).
 */

struct cache_case_case {
	pthread_rwlock_t lock;
	char *buf;
	int len;
	time_t ts;
};

struct cache_case_buf {
	struct buf_string str;
	struct cache_case_case *dest;
};

static int 
cache_case_outstring_raw(struct bpapi_output_chain *ochain, const char *str, size_t len) {
	struct cache_case_buf *buf = ochain->data;

	if (buf)
		bufwrite(&buf->str.buf, &buf->str.len, &buf->str.pos, str, len);
	return bpo_outstring_raw(ochain->next, str, len);
}

static void
cache_case_cleanup(struct template_opaque_var *var)
{
	struct cache_case_case *cases = var->data;

	free(cases->buf);
	cases->buf = NULL;
	free(cases);
	var->data = NULL;
}


static int
init_cache_case(const struct bpfilter *filter, struct bpapi *api, int argc, const struct tpvar **argv) {
	const char *tag UNUSED;
	struct template_opaque_var *var;
	void **cases_ptr = NULL;
	struct cache_case_buf *buf;
	struct cache_case_case *cases;
	time_t timeout = 0;

	switch (argc) {
	case 3:
		timeout = time(NULL) - tpvar_int(argv[2]);
	case 2:
		TPVAR_STRTMP(tag, argv[0]);
		var = tpvar_opaque(argv[1]);
		break;
	default:
		return 1;
	}

	if (var)
		cases_ptr = &var->data;

	if (!cases_ptr) {
		/*log_printf(LOG_DEBUG, "cache_case(%s): NULL bucket", tag);*/
		BPAPI_OUTPUT_DATA(api) = NULL;
		return 0;
	}

	if (*cases_ptr)
		cases = *cases_ptr;
	else {
		cases = zmalloc(sizeof (struct cache_case_case));
		pthread_rwlock_init(&cases->lock, NULL);
		if (atomic_cas_ptr(cases_ptr, NULL, cases)) {
			pthread_rwlock_destroy(&cases->lock);
			free(cases);
			cases = *cases_ptr;
		}
		var->cleanup = cache_case_cleanup;
	}

	if (timeout) {
		pthread_rwlock_rdlock(&cases->lock);
		if (cases->buf && timeout < cases->ts) {
			int len = cases->len;
			char *data = xmalloc(cases->len);

			memcpy(data, cases->buf, len);
			pthread_rwlock_unlock(&cases->lock);

			bpapi_outstring_raw(api, data, len);
			free(data);
			return 1;
		}
		pthread_rwlock_unlock(&cases->lock);
		if (cases->buf) {
			pthread_rwlock_wrlock(&cases->lock);
			if (cases->buf) {
				free(cases->buf);
				cases->buf = NULL;
			}
			pthread_rwlock_unlock(&cases->lock);
		}
	} else if (cases->buf) {
		bpapi_outstring_raw(api, cases->buf, cases->len);
		BPAPI_OUTPUT_DATA(api) = NULL;
		return 1;
	}

	buf = zmalloc(sizeof(*buf));
	buf->dest = cases;
	BPAPI_OUTPUT_DATA(api) = buf;
	return 0;
}

static void
fini_cache_case(struct bpapi_output_chain *ochain) {
	struct cache_case_buf *buf = ochain->data;

	if (!buf)
		return;

	if (!buf->str.buf)
		bufwrite(&buf->str.buf, &buf->str.len, &buf->str.pos, "", 0);

	if (buf->dest->buf || atomic_cas_ptr(&buf->dest->buf, NULL, buf->str.buf)) {
		free(buf->str.buf);
	} else {
		buf->dest->len = buf->str.pos;
		buf->dest->ts = time(NULL);
	}
	free(buf);
}

static const struct bpapi_output cache_case_output = {
	self_outstring_fmt,
	cache_case_outstring_raw,
	fini_cache_case,
	NULL
};

ADD_OUTPUT_FILTER(cache_case, 0);
