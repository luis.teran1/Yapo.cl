#include <bpapi.h>

struct str_repeat_data
{
	struct buf_string buf;
	int m;
};

static void
fini_str_repeat(struct bpapi_output_chain *ochain) {
	struct str_repeat_data *data = ochain->data;
	int i;

	if (data->buf.buf) {
		for (i = 0 ; i < data->m ; i++)
			bpo_outstring_raw(ochain->next, data->buf.buf, data->buf.pos);
		free(data->buf.buf);
	}
}

static int
init_str_repeat(const struct bpfilter *filter, struct bpapi *api, int argc, const struct tpvar **argv) {
	struct str_repeat_data *data = BPAPI_OUTPUT_DATA(api);

	if (argc >= 1)
		data->m = tpvar_int(argv[0]);
	else
		data->m = 0;

	return 0;
}

struct bpapi_output str_repeat_output =
{
	bufstring_outstring_fmt,
	bufstring_outstring_raw,
	fini_str_repeat,
	NULL
};

ADD_OUTPUT_FILTER(str_repeat, sizeof(struct str_repeat_data));
