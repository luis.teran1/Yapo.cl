#include <bpapi.h>
#include <time.h>

/*
 * RFC-822 date
 *
 * Reformats a date and time string to RFC-822 compliant string
 */

struct rfc822_date_data {
	char *str;
	int pos;
	int len;
};

static int 
rfc822_date_outstring_raw(struct bpapi_output_chain *filter, const char *str, size_t len) {
	struct rfc822_date_data *data = filter->data;

	bufwrite(&data->str, &data->len, &data->pos, str, len);
	return 0;
}

static int
init_rfc822_date(const struct bpfilter *filter, struct bpapi *api, int argc, const struct tpvar **argv) {
	return 0;
}

static void
fini_rfc822_date(struct bpapi_output_chain *filter) {
	struct rfc822_date_data *data = filter->data;

	if (data->str)
		free(data->str);
}

static void
flush_rfc822_date(struct bpapi_output_chain *filter) {
	struct rfc822_date_data *data = filter->data;

	if (data->str) {
		struct tm tm = { .tm_isdst = -1 };
		char buf[256];

		if (!strptime(data->str, "%Y-%m-%d %H:%M:%S", &tm)) {
			bpo_outstring_raw(filter->next, data->str, data->pos);
		} else {
			mktime(&tm);
			size_t l = strftime(buf, sizeof(buf), "%a, %d %b %Y %T %z", &tm);
			bpo_outstring_raw(filter->next, buf, l);
			free(data->str);
			data->str = NULL;
			data->len = data->pos = 0;
		}
	}
}

const struct bpapi_output rfc822_date_output = {
	self_outstring_fmt,
	rfc822_date_outstring_raw,
	fini_rfc822_date,
	flush_rfc822_date
};

ADD_OUTPUT_FILTER(rfc822_date, sizeof(struct rfc822_date_data));
