#include <bpapi.h>
#include "trunc.h"
#include "strl.h"

static int
init_trunc_with_breakchar(const struct bpfilter *filter, struct bpapi *api, int argc, const struct tpvar **argv) {
	struct _trunc *trunc = BPAPI_OUTPUT_DATA(api);

	switch (argc) {
	case 3:
		tpvar_strcpy(trunc->break_char, argv[2], sizeof(trunc->break_char));
	case 2:
		tpvar_strcpy(trunc->terminator, argv[1], sizeof(trunc->terminator));
	case 1:
		trunc->filt_len = tpvar_int(argv[0]);
	}
	if (argc < 2)
		strlcpy(trunc->terminator, "...", sizeof(trunc->terminator));
	trunc->chars_left = trunc->filt_len;

	return 0;
}

static void
flush_trunc_with_breakchar(struct bpapi_output_chain *ochain) {
	struct _trunc *trunc = ochain->data;

	if (trunc->chars_left == -1)
		bpo_outstring_raw(ochain->next, trunc->terminator, strlen(trunc->terminator));
	trunc->chars_left = trunc->filt_len;
}

static const struct bpapi_output trunc_with_breakchar_output = {
	self_outstring_fmt,
	trunc_outstring_raw,
	NULL,
	flush_trunc_with_breakchar
};

ADD_OUTPUT_FILTER(trunc_with_breakchar, sizeof(struct _trunc));
