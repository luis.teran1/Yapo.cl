#include <bpapi.h>
#include <stdlib.h>

/*
 * float_precision
 *
 * Reformats a int/float to a specified number of decimals
 */

struct float_precision_data {
	char *str;
	int pos;
	int len;
	int precision;
};

static int
float_precision_outstring_raw(struct bpapi_output_chain *ochain, const char *str, size_t len) {
	struct float_precision_data *data = ochain->data;

	bufwrite(&data->str, &data->len, &data->pos, str, len);
	return 0;
}

static int
init_float_precision(const struct bpfilter *filter, struct bpapi *api, int argc, const struct tpvar **argv) {
	struct float_precision_data *fpd = BPAPI_OUTPUT_DATA(api);

	fpd->precision = tpvar_int(argv[0]);

	return 0;
}

static void
fini_float_precision(struct bpapi_output_chain *ochain) {
	struct float_precision_data *data = ochain->data;

	if (data->str)
		free(data->str);
}

static void
flush_float_precision(struct bpapi_output_chain *ochain) {
	struct float_precision_data *data = ochain->data;

	if (data->str) {
		double f;
		char *e;

		f = strtod(data->str, &e);
		if (*e != '\0') {
			bpo_outstring_raw(ochain->next, data->str, data->pos);
		} else {
			bpo_outstring_fmt(ochain->next, "%.*f", data->precision, f);
		}
		free(data->str);
		data->str = NULL;
		data->len = data->pos = 0;
	}
}

const struct bpapi_output float_precision_output = {
	self_outstring_fmt,
	float_precision_outstring_raw,
	fini_float_precision,
	flush_float_precision
};

ADD_OUTPUT_FILTER(float_precision, sizeof(struct float_precision_data));
