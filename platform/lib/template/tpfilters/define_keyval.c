#include <bpapi.h>
#include <stdio.h>
#include "define.h"

/**
	Given a name "var" and a string like 'a=alfa&b=beta&e=erik' as the data,
	define the vars "var_a" -> "alfa", "var_b" -> "beta" and "var_e" -> "erik"

	Optional second argument is keyvalue pair delimiter ("&")
	Optional third argument is name name of a single key to define, all others are ignored.

	The key-value delimiter is currently hard-coded as '='.

	\see: init_define_split
*/
static int
init_define_keyval(const struct bpfilter *filter, struct bpapi *api, int argc, const struct tpvar **argv) {
	struct _define *def = BPAPI_OUTPUT_DATA(api);

	switch (argc) {
	case 3:
		def->extra = tpvar_str(argv[2], &def->fe);
	case 2:
		tpvar_strcpy(def->delim, argv[1], sizeof(def->delim));
	case 1:
		def->name = tpvar_str(argv[0], &def->fn);
		break;
	default:
		return 1;
	}
	def->schain = &api->schain;

	return 0;
}

static void
fini_define_keyval(struct bpapi_output_chain *ochain) {
	struct _define *def = ochain->data;
	char define_name[128];
	char* q = NULL;
	char* q_start = NULL;
	char* strtok_state = NULL;
	char* var = NULL;
	char* var_data = NULL;
	char delim[2];

	if (def->buf) {
		if (bps_has_insert(def->schain)) {
			delim[0] = def->delim[0] ?: '&';
			delim[1] = '\0';

			q_start = q = xstrdup(def->buf);
			while ((var = strtok_r(q, delim, &strtok_state))) {
				if (!def->delim[0] && strncmp(var, "amp;", 4) == 0)
					var += 4;
				if ((var_data = strchr(var, def->delim[0] && def->delim[1] ? def->delim[1] : '=')) && var_data + 1 != '\0') {
					/* If def->extra is NULL, define all vars, else only the one */
					if (def->extra == NULL || strncmp(def->extra, var, (int)(var_data-var)) == 0) {
						snprintf(&define_name[0], sizeof(define_name), "%s%s%.*s", def->name, *def->name ? "_" : "", (int)(var_data - var), var);
						bps_insert(def->schain, define_name, var_data + 1);
					}
				}
				q = NULL;
			}
			free(q_start);
		}
		free(def->buf);
	}
	free(def->fn);
	free(def->fe);
}

const struct bpapi_output define_keyval_output = {
	self_outstring_fmt,
	define_outstring_raw,
	fini_define_keyval
};

ADD_OUTPUT_FILTER(define_keyval, sizeof(struct _define));
