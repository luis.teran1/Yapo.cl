#include <bpapi.h>
#include "define.h"

static int
init_define_split(const struct bpfilter *filter, struct bpapi *api, int argc, const struct tpvar **argv) {
	struct _define *def = BPAPI_OUTPUT_DATA(api);

	switch (argc) {
	case 3:
		def->append = tpvar_int(argv[2]);
	case 2:
		tpvar_strcpy(def->delim, argv[1], sizeof(def->delim));
	case 1:
		def->name = tpvar_str(argv[0], &def->fn);
		break;
	default:
		return 1;
	}
	def->schain = &api->schain;

	if (!def->append && bps_has_remove(def->schain))
		bps_remove(def->schain, def->name);

	return 0;
}

static void
fini_define_split(struct bpapi_output_chain *ochain) {
	struct _define *def = ochain->data;

	if (def->buf) {
		char *str = def->buf;

		if (*str) {
			char *p;
			while (bps_has_insert(def->schain) && (p = strsep(&str, *def->delim ? def->delim : ",")) != NULL) {
				bps_insert(def->schain, def->name, p);
			}
		}
		free(def->buf);
	}
	free(def->fn);
}

const struct bpapi_output define_split_output = {
	self_outstring_fmt,
	define_outstring_raw,
	fini_define_split
};

ADD_OUTPUT_FILTER(define_split, sizeof(struct _define));
