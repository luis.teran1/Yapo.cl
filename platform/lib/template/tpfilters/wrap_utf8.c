#include <bpapi.h>
#include "wrap.h"

/**
 * wrap for utf8, need to allocate more memory than wrap
 * since wrap_len is treated as number of characters
 */
static int
init_wrap_utf8(const struct bpfilter *filter, struct bpapi *api, int argc, const struct tpvar **argv) {
	struct _wrap *wrap = BPAPI_OUTPUT_DATA(api);

	wrap->wrap_len = tpvar_int(argv[0]);
	wrap->skip_html = 0;
	wrap->pos = 0;
	wrap->len = 0;
	wrap->spacepos = -1;
	wrap->str = xmalloc(MAX_WRAP_SIZE + 1);
	wrap->str[wrap->wrap_len] = '\0';
	wrap->as_utf8 = 1;
	return 0;
}

const struct bpapi_output wrap_utf8_output = {
	self_outstring_fmt,
	wrap_outstring_raw,
	fini_wrap,
	flush_wrap
};

ADD_FILTER(wrap_utf8, &wrap_utf8_output, sizeof(struct _wrap), NULL, 0, NULL, 0);
