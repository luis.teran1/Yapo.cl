#include <bpapi.h>
#include "tr.h"

static int 
tr_outstring_raw(struct bpapi_output_chain *ochain, const char *str, size_t len) {
	struct tr_data *data = ochain->data;
	char *trstr = replace_chars(str, len, data->chars);
	int res = bpo_outstring_raw(ochain->next, trstr, strlen(trstr));

	free(trstr);
	return res;
}

static int
init_tr(const struct bpfilter *filter, struct bpapi *api, int argc, const struct tpvar **argv) {
	struct tr_data *data = BPAPI_OUTPUT_DATA(api);

	data->chars = tpvar_str(argv[0], &data->fc);
	return 0;
}

void
fini_tr(struct bpapi_output_chain *ochain) {
	struct tr_data *data = ochain->data;

	free(data->fc);
	if (data->free_mapp)
		free(data->mapp);
}

const struct bpapi_output tr_output = {
	self_outstring_fmt,
	tr_outstring_raw,
	fini_tr,
};

ADD_OUTPUT_FILTER(tr, sizeof(struct tr_data));
