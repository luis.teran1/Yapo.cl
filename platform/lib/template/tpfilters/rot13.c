#include <bpapi.h>

/* ROT13 example filter hooking the outstring function */

static int 
rot13_outstring_raw(struct bpapi_output_chain *ochain, const char *str, size_t len) {
	char *str_rot13 = xmalloc(len + 1);
	char *tmp = str_rot13;
	int res;
	const char *end = str + len;

	while (str < end) {
		*tmp = *str;

		if ((*tmp >= 'a' && *tmp <= 'm') ||
		    (*tmp >= 'A' && *tmp <= 'M')) {
			*tmp += 13;
		} else if ((*tmp >= 'n' && *tmp <= 'z') ||
			   (*tmp >= 'N' && *tmp <= 'Z')) {
			*tmp -= 13;
		}
		tmp++;
		str++;
	}

	*tmp = '\0';

	res = bpo_outstring_raw(ochain->next, str_rot13, len);
	free(str_rot13);
	return res;
}

static int
init_rot13(const struct bpfilter *filter, struct bpapi *api, int argc, const struct tpvar **argv) {
	return 0;
}

const struct bpapi_output rot13_output = {
	self_outstring_fmt,
	rot13_outstring_raw,
	NULL
};

ADD_OUTPUT_FILTER(rot13, 0);
