#include <string.h>
#include <ctype.h>
#include <bpapi.h>
#include "strl.h"
#include "trunc.h"


/* trunc_outstring_raw is used for both filters: trunc and trunc_with_breakchar */
int
trunc_outstring_raw(struct bpapi_output_chain *ochain, const char *str, size_t len) {
	struct _trunc *trunc = ochain->data;

	if (trunc->chars_left <= 0) {
		trunc->chars_left = -1;
		return 0;
	}

	if (trunc->entity_aware) {
		int i;
		int entstart = -1;

		for (i = 0 ; i < (int)len ; i++) {
			if (entstart >= 0) {
				trunc->chars_left++;
				if (str[i] == ';') {
					entstart = -1;
				} else if (i - entstart >= 10) {
					trunc->chars_left -= i - entstart;
					entstart = -1;
				}
			} else if (i >= trunc->chars_left) {
				break;
			} else if (str[i] == '&') {
				entstart = i;
			}
		}
	}

	if ((int)len <= trunc->chars_left) {
		trunc->chars_left -= len;
		return bpo_outstring_raw(ochain->next, str, len);
	} else {
		int res;
		const char *b;

		/* Break after last break_char if set and found */
		if (trunc->break_char[0] && (b = memrchr(str, trunc->break_char[0], trunc->chars_left))) {
			if (isspace(trunc->break_char[0])) {
				while (b > str && strchr(trunc->break_char, *(b - 1)))
					b--;
			}
			res = bpo_outstring_raw(ochain->next, str, b - str);
		} else
			res = bpo_outstring_raw(ochain->next, str, trunc->chars_left);
		trunc->chars_left = -1;
		return res;
	}
	return 0;
}

static int
init_trunc(const struct bpfilter *filter, struct bpapi *api, int argc, const struct tpvar **argv) {
	struct _trunc *trunc = BPAPI_OUTPUT_DATA(api);

	switch (argc) {
	case 3:
		trunc->entity_aware = tpvar_int(argv[2]);
	case 2:
		tpvar_strcpy(trunc->terminator, argv[1], sizeof(trunc->terminator));
	case 1:
		trunc->filt_len = tpvar_int(argv[0]);
	}
	if (argc < 2)
		strlcpy(trunc->terminator, "...", sizeof(trunc->terminator));
	trunc->chars_left = trunc->filt_len;

	return 0;
}

static void
flush_trunc(struct bpapi_output_chain *ochain) {
	struct _trunc *trunc = ochain->data;

	if (trunc->chars_left == -1) {
		bpo_outstring_raw(ochain->next, trunc->terminator, strlen(trunc->terminator));
	}
	trunc->chars_left = trunc->filt_len;
}

static const struct bpapi_output trunc_output = {
	self_outstring_fmt,
	trunc_outstring_raw,
	NULL,
	flush_trunc
};

ADD_OUTPUT_FILTER(trunc, sizeof(struct _trunc));
