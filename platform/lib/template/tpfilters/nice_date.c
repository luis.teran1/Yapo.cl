#include <bpapi.h>
#include <time.h>
#include "strl.h"

/*
 * nice_date
 *
 * Reformats a date and time string to just date
 */

struct nice_date_data {
	char *str;
	int pos;
	int len;
	char format[32];
};

static int 
nice_date_outstring_raw(struct bpapi_output_chain *ochain, const char *str, size_t len) {
	struct nice_date_data *data = ochain->data;

	bufwrite(&data->str, &data->len, &data->pos, str, len);
	return 0;
}

static int
init_nice_date(const struct bpfilter *filter, struct bpapi *api, int argc, const struct tpvar **argv) {
	struct nice_date_data *data = BPAPI_OUTPUT_DATA(api);

	if (argc) {
		tpvar_strcpy(data->format, argv[0], sizeof(data->format));
	} else {
		strlcpy(data->format, "%Y-%m-%d", sizeof(data->format));
	}

	return 0;
}

static void
fini_nice_date(struct bpapi_output_chain *ochain) {
	struct nice_date_data *data = ochain->data;

	if (data->str)
		free(data->str);
}

static void
flush_nice_date(struct bpapi_output_chain *ochain) {
	struct nice_date_data *data = ochain->data;

	if (data->str) {
		struct tm tm = {0};
		char buf[256];

		if (!strptime(data->str, "%Y-%m-%d %H:%M:%S", &tm)) {
			bpo_outstring_raw(ochain->next, data->str, data->pos);
		} else {
			size_t l = strftime(buf, sizeof(buf), data->format, &tm);
			bpo_outstring_raw(ochain->next, buf, l);
			free(data->str);
			data->str = NULL;
			data->len = data->pos = 0;
		}
	}
}

const struct bpapi_output nice_date_output = {
	self_outstring_fmt,
	nice_date_outstring_raw,
	fini_nice_date,
	flush_nice_date,
};

ADD_OUTPUT_FILTER(nice_date, sizeof(struct nice_date_data));
