#include <bpapi.h>
#include <string.h>

struct _decoderec {
	int pos;
	char buf[1024];
};

static void
flush_htmlentities_decode(struct bpapi_output_chain *ochain) {
	struct _decoderec *dr = ochain->data;
	if (dr->pos) {
		bpo_outstring_raw(ochain->next, dr->buf, dr->pos);
		dr->pos = 0;
	}
}

static int
htmlentities_decode_outstring_raw(struct bpapi_output_chain *ochain, const char *str, size_t len) {
	const char *from = (char*) str;
	const char *end = (char*) str + len;
	struct _decoderec *dr = ochain->data;
	int buf_size = sizeof(dr->buf);

	while (from < end) {
		if (dr->pos >= (buf_size - 10)) {
			flush_htmlentities_decode(ochain);
		}
		if (*from != '&') {
			dr->buf[dr->pos++] = *from++;
		} else if (strncmp(from, "&lt;", 4) == 0) {
			dr->buf[dr->pos++] = '<';
			from += 4;
		} else if (strncmp(from, "&gt;", 4) == 0) {
			dr->buf[dr->pos++] = '>';
			from += 4;
		} else if (strncmp(from, "&quot;", 6) == 0) {
			dr->buf[dr->pos++] = '"';
			from += 6;
		} else if (strncmp(from, "&amp;", 5) == 0) {
			dr->buf[dr->pos++] = '&';
			from += 5;
		} else if (strncmp(from, "&#34;", 5) == 0) {
			dr->buf[dr->pos++] = '"';
			from += 5;
		} else {
			/* XXX: unrecognized HTML entity, copying... */
			dr->buf[dr->pos++] = *from++;
		}
	}

	return 0;
}

static int
init_htmlentities_decode(const struct bpfilter *filter, struct bpapi *api, int argc, const struct tpvar *argv[]) {
	return 0;
}

const struct bpapi_output htmlentities_decode_output = {
	self_outstring_fmt,
	htmlentities_decode_outstring_raw,
	NULL,
	flush_htmlentities_decode
};

ADD_OUTPUT_FILTER(htmlentities_decode, sizeof(struct _decoderec));
