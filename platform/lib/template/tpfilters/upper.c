#include <bpapi.h>
#include <ctype.h>
#include "upper.h"

/* XXX: This should be rewritten to use charmap or a similar table-approach */
char
local_upper(char c) {
	switch (c) {
		case '\xE5': /* å */
			c = '\xC5'; /* Å */
			break;
		case '\xE4': /* ä */
			c = '\xC4'; /* Ä */
			break;
		case '\xF6': /* ö */
			c = '\xD6'; /* Ö */
			break;
		case '\xFC': /* ü */
			c = '\xDC'; /* Ü */
			break;
		case '\xE9': /* é */
			c = '\xC9'; /* É */
			break;
		case '\xE8': /* è */
			c = '\xC8'; /* È */
			break;
		case '\xE0': /* à */
			c = '\xC0'; /* À */
			break;
		case '\xE1': /* á */
			c = '\xC1'; /* Á */
			break;
		case '\xEA': /* ê */
			c = '\xCA'; /* Ê */
			break;
		case '\xE7': /* ç */
			c = '\xC7'; /* Ç */
			break;
		case '\xF1': /* ñ */
			c = '\xD1'; /* Ñ */
			break;
		case '\xE2': /* â */
			c = '\xC2'; /* Â */
			break;
		case '\xF8': /* ø */
			c = '\xD8'; /* Ø */
			break;
		case '\xE6': /* æ */
			c = '\xC6'; /* Æ */
			break;
		case '\xF4': /* ô */
			c = '\xD4'; /* Ô */
			break;
		case '\xFB': /* û */
			c = '\xDB'; /* Û */
			break;
		case '\xEB': /* ë */
			c = '\xCB'; /* Ë */
			break;
		case '\xEE': /* î */
			c = '\xCE'; /* Î */
			break;
		case '\xEF': /* ï */
			c = '\xCF'; /* Ï */
			break;
		case '\xED': /* í */
			c = '\xCD'; /* Í */
			break;
		case '\xEC': /* ì */
			c = '\xCC'; /* Ì */
			break;
		case '\xF3': /* ó */
			c = '\xD3'; /* Ó */
			break;
		case '\xF5': /* õ */
			c = '\xD5'; /* Õ */
			break;
		case '\xF9': /* ù */
			c = '\xD9'; /* Ù */
			break;
		case '\xFA': /* ú */
			c = '\xDA'; /* Ú */
			break;
	}
	return c;
}

static int 
upper_outstring_raw(struct bpapi_output_chain *ochain, const char *str, size_t len) {
	char *str_upper = xmalloc(len + 1);
	char *tmp = str_upper;
	int res;
	const char *end = str + len;

	while (str < end) {
		*tmp = local_upper(toupper(*str));
		tmp++;
		str++;
	}

	*tmp = '\0';

	res = bpo_outstring_raw(ochain->next, str_upper, len);
	free(str_upper);
	return res;
}

static int
init_upper(const struct bpfilter *filter, struct bpapi *api, int argc, const struct tpvar **argv) {
	return 0;
}

const struct bpapi_output upper_output = {
	self_outstring_fmt,
	upper_outstring_raw,
	NULL,
	NULL
};

ADD_OUTPUT_FILTER(upper, 0);
