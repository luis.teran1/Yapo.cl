#include <bpapi.h>
#include "urlencode.h"

static int
urlencode_utf8_outstring_raw(struct bpapi_output_chain *ochain, const char *str, size_t len) {
	const unsigned char *from;
	const unsigned char *end;
	unsigned char c;
	int res = 0;

	struct _urlenc *ue = ochain->data;

	from = (unsigned char*) str;
	end = (unsigned char*) str + len;

	while (from < end) {
		if (ue->pos >= 1010) {
			res += ue->pos;
			flush_urlencode(ochain);
		}

		c = *from++;

		if (c == ' ') {
			ue->buf[ue->pos++] = '+';
		} else if ((c < '0' && c != '-' && c != '.') ||
				(c < 'A' && c > '9') ||
				(c > 'Z' && c < 'a' && c != '_') ||
				(c > 'z')) {
			ue->buf[ue->pos] = '%';
			ue->buf[ue->pos + 1] = hexchars[c >> 4];
			ue->buf[ue->pos + 2] = hexchars[c & 15]; 
			ue->pos += 3;
		} else {
			ue->buf[ue->pos++] = c;
		}
	}

	return 0;
}

static const struct bpapi_output urlencode_utf8_output = {
	self_outstring_fmt,
	urlencode_utf8_outstring_raw,
	fini_urlencode,
	flush_urlencode
};

#define init_urlencode_utf8 init_urlencode

ADD_OUTPUT_FILTER(urlencode_utf8, sizeof(struct _urlenc));
