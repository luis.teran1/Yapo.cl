#include <bpapi.h>
#include <unicode/utf8.h>
#include <unicode/uchar.h>

struct _ucfirst_utf8 {
        int first_only; /* If set, _only_ the first character is uppercased */
        int reset_on_special; /* Applicable on when first_only. If set, will reset "uppercaseification" after amps */
        int has_amp;
};

static int
ucfirst_utf8_outstring_raw(struct bpapi_output_chain *ochain, const char *str, size_t len) {
        struct _ucfirst_utf8 *ucfirst = ochain->data;
        int i;
        UChar32 ch;
        int lch_space = 0;
        char buf[5];
        int j;
        int res = 0;
        int w_counter = 0;

        for (i = 0 ; i < (int)len ; ) {
                U8_NEXT(str, i, (int)len, ch);
                if (ch == U_SENTINEL)
                        break;

                if (w_counter == 0 || ((!ucfirst->first_only || ucfirst->has_amp) && w_counter > 0 && lch_space == 1)) {
                        ch = u_toupper(ch);
                } else {
                        ch = u_tolower(ch);
                }

                if (ucfirst->reset_on_special) {
                        if (ch == '&')
                                ucfirst->has_amp = 1;
                        else if (ucfirst->has_amp && ch != ' ')
                                ucfirst->has_amp = 0;
                }
                if (ch == ' ') {
                        lch_space = 1;
                } else {
                        lch_space = 0;
                }
                j = 0;
                U8_APPEND_UNSAFE(buf, j, ch);
                w_counter++;
                res += bpo_outstring_raw(ochain->next, buf, j);
        }
        return res;
}

static int
init_ucfirst_utf8(const struct bpfilter *filter, struct bpapi *api, int argc, const struct tpvar **argv) {
        struct _ucfirst_utf8 *ucfirst = BPAPI_OUTPUT_DATA(api);
        switch (argc) {
        case 2:
                ucfirst->reset_on_special = tpvar_int(argv[1]);
        case 1:
                ucfirst->first_only = tpvar_int(argv[0]);
        }
        ucfirst->has_amp = 0;

        return 0;
}

const struct bpapi_output ucfirst_utf8_output = {
        self_outstring_fmt,
        ucfirst_utf8_outstring_raw
};

ADD_OUTPUT_FILTER(ucfirst_utf8, sizeof(struct _ucfirst_utf8));
