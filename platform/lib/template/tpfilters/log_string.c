#include <bpapi.h>
#include "log_event.h"

/*
 * log_string filter
 *
 * Send a string to syslog
 */

struct log_string_data {
	const char *application;
	char *ap;
	char *str;
	int pos;
	int len;
};

static int
log_string_outstring_raw(struct bpapi_output_chain *ochain, const char *str, size_t len) {
	struct log_string_data *data = ochain->data;

	bufwrite(&data->str, &data->len, &data->pos, str, len);
	return 0;
}

static int
init_log_string(const struct bpfilter *filter, struct bpapi *api, int argc, const struct tpvar **argv) {
	struct log_string_data *data = BPAPI_OUTPUT_DATA(api);

	switch (argc) {
	case 1:
		data->application = tpvar_str(argv[0], &data->ap);
	}

	return 0;
}

static void
flush_log_string(struct bpapi_output_chain *ochain) {
	struct log_string_data *data = ochain->data;

	if (data->str && data->application) {
		syslog_ident(data->application, "%s", data->str); 
		data->pos = 0;
		if (data->len > 0)
			data->str[0] = '\0';
	}
}

static void
fini_log_string(struct bpapi_output_chain *ochain) {
	struct log_string_data *data = ochain->data;

	free(data->str);
	free(data->ap);
}

static const struct bpapi_output log_string_output = {
	self_outstring_fmt,
	log_string_outstring_raw,
	fini_log_string,
	flush_log_string
};

ADD_OUTPUT_FILTER(log_string, sizeof(struct log_string_data));
