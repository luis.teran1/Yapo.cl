#include <bpapi.h>

static int 
eat_outstring_fmt(struct bpapi_output_chain *ochain, const char *fmt, ...) {
	return 0;
}

static int 
eat_outstring_raw(struct bpapi_output_chain *ochain, const char *str, size_t len) {
	return 0;
}

static int
init_eat(const struct bpfilter *filter, struct bpapi *api, int argc, const struct tpvar **argv) {
	return 0;
}

const struct bpapi_output eat_output = {
	eat_outstring_fmt,
	eat_outstring_raw,
	NULL
};

ADD_OUTPUT_FILTER(eat, 0);
