#ifndef TR_H
#define TR_H
#include "macros.h"

struct tr_data
{
	const char *chars;
	char *fc;
	int (*mapp)[2];
	int nmap;
	int free_mapp;
};
void fini_tr(struct bpapi_output_chain *ochain) VISIBILITY_HIDDEN;
#endif
