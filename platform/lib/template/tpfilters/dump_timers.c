#include <bpapi.h>
#include <stdio.h>
#include "timer.h"
#include "bconf.h"

struct dump_timer_data {
	struct shadow_vtree vt;
	const char *prefix;
	struct bconf_node *node;
	char *fp;
};

static void
dump_timer_cb(struct timer_class *tc, void *data) {
	struct dump_timer_data *dt = data;
	struct timespec avgts;
	double avg = (double)tc->tc_total.tv_sec + (double)tc->tc_total.tv_nsec / 1000000000.0;
	struct timespec selfts;
	struct timespec selfavgts;
	char sbuf[16];

	if (tc->tc_count == 0)
		return;

	avg /= (double)tc->tc_count;

	avgts.tv_sec = avg;
	avgts.tv_nsec = (avg - (double)avgts.tv_sec) * 1000000000.0;

	selfts.tv_sec = tc->tc_total.tv_sec - tc->tc_children.tv_sec;
	selfts.tv_nsec = tc->tc_total.tv_nsec - tc->tc_children.tv_nsec;
	if (selfts.tv_nsec < 0) {
		selfts.tv_sec--;
		selfts.tv_nsec += 1000000000L;
	}
	avg = (double)selfts.tv_sec + (double)selfts.tv_nsec / 1000000000.0;
	avg /= (double)tc->tc_count;
	selfavgts.tv_sec = avg;
	selfavgts.tv_nsec = (avg - (double)avgts.tv_sec) * 1000000000.0;

#define ADD_T(name, ts) do { \
		snprintf(sbuf, sizeof(sbuf), "%08f", ((double)((ts)->tv_sec * 1000000000.0 + (ts)->tv_nsec))); \
		bconf_add_datav(&dt->node, 4, (const char *[]){ dt->prefix, tc->tc_name, name, "ns" }, sbuf, BCONF_DUP); \
		snprintf(sbuf, sizeof(sbuf), "%08f", ((double)((ts)->tv_sec * 1000000000.0 + (ts)->tv_nsec)/1000.0)); \
		bconf_add_datav(&dt->node, 4, (const char *[]){ dt->prefix, tc->tc_name, name, "us" }, sbuf, BCONF_DUP); \
		snprintf(sbuf, sizeof(sbuf), "%08f", ((double)((ts)->tv_sec * 1000000000.0 + (ts)->tv_nsec)/1000000.0)); \
		bconf_add_datav(&dt->node, 4, (const char *[]){ dt->prefix, tc->tc_name, name, "ms" }, sbuf, BCONF_DUP); \
		snprintf(sbuf, sizeof(sbuf), "%08f", ((double)((ts)->tv_sec * 1000000000.0 + (ts)->tv_nsec)/1000000000.0)); \
		bconf_add_datav(&dt->node, 4, (const char *[]){ dt->prefix, tc->tc_name, name, "s" }, sbuf, BCONF_DUP); \
		snprintf(sbuf, sizeof(sbuf), "%ld", (long int)(ts)->tv_sec); \
		bconf_add_datav(&dt->node, 4, (const char *[]){ dt->prefix, tc->tc_name, name, "tv_s" }, sbuf, BCONF_DUP); \
		snprintf(sbuf, sizeof(sbuf), "%ld", (long int)(ts)->tv_nsec); \
		bconf_add_datav(&dt->node, 4, (const char *[]){ dt->prefix, tc->tc_name, name, "tv_ns" }, sbuf, BCONF_DUP); \
	} while (0)

	snprintf(sbuf, sizeof(sbuf), "%lld", tc->tc_count);
	bconf_add_datav(&dt->node, 3, (const char *[]){ dt->prefix, tc->tc_name, "count" }, sbuf, BCONF_DUP);
	ADD_T("total", &tc->tc_total);
	ADD_T("min", &tc->tc_min);
	ADD_T("max", &tc->tc_max);
	ADD_T("average", &avgts);
	ADD_T("self_total", &selfts);
	ADD_T("self_average", &selfavgts);
}

static void
dump_timers_free(struct shadow_vtree *sv) {
	struct dump_timer_data *dt = (struct dump_timer_data *)sv;
	bconf_free(&dt->node);
	free(dt->fp);
}

static int
init_dump_timers(const struct bpfilter *filter, struct bpapi *api, int argc, const struct tpvar **argv) {
	struct dump_timer_data *dt = BPAPI_VTREE_DATA(api);

	if (argc != 1) {
		return 1;
	}

	dt->prefix = tpvar_str(argv[0], &dt->fp);

	timer_foreach(dump_timer_cb, dt);
	bconf_vtree(&dt->vt.vtree, dt->node);
	dt->vt.free_cb = dump_timers_free;

	return 0;
}

#define dump_timers_vtree shadow_vtree
ADD_VTREE_FILTER(dump_timers, sizeof(struct dump_timer_data));
