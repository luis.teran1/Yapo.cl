#include <stdio.h>
#include <bpapi.h>

struct _entityrec{
	int pos;
	char buf[1024];
};

static void
flush_htmlentities(struct bpapi_output_chain *ochain) {
	struct _entityrec *er = ochain->data;
	if (er->pos) {
		bpo_outstring_raw(ochain->next, er->buf, er->pos);
		er->pos = 0;
	}
}

static int
htmlentities_outstring_raw(struct bpapi_output_chain *ochain, const char *str, size_t len) {
	unsigned char c;
	const unsigned char *from;
	const unsigned char *end;
	struct _entityrec *er = ochain->data;
	int buf_size = sizeof(er->buf);
	int last_is_dash = 0;

	from = (unsigned char*) str;
	end = (unsigned char*) str + len;

	while (from < end) {
		if (er->pos >= (buf_size - 10)) {
			flush_htmlentities(ochain);
		}

		c = *from++;
		switch (c) {
			case '&':
				snprintf(er->buf + er->pos, buf_size - er->pos, "&amp;");
				er->pos += 5;
				break;
			case '<':
				snprintf(er->buf + er->pos, buf_size - er->pos, "&lt;");
				er->pos += 4;
				break;
			case '>':
				snprintf(er->buf + er->pos, buf_size - er->pos, "&gt;");
				er->pos += 4;
				break;
			case '"':
				snprintf(er->buf + er->pos, buf_size - er->pos, "&quot;");
				er->pos += 6;
				break;
			case '\'':
				/* http://www.w3.org/TR/html4/sgml/entities.html -> &apos; is not HTML4 valid */
				snprintf(er->buf + er->pos, buf_size - er->pos, "&#39;");
				er->pos += 5;
				break;
			case '-':
				/* Firefox breaks if -- appears in a comment. therefore we encode the second - if two appears in a row. */
				if (!last_is_dash) {
					er->buf[er->pos++] = c;
					last_is_dash = 1;
					continue;
				}
				snprintf(er->buf + er->pos, buf_size - er->pos, "&#45;");
				er->pos += 5;
				break;
			default:	
				er->buf[er->pos++] = c;
		}
		last_is_dash = 0;
	}

	return 0;
}

static int
init_htmlentities(const struct bpfilter *filter, struct bpapi *api, int argc, const struct tpvar **argv) {
	return 0;
}

const struct bpapi_output htmlentities_output = {
	self_outstring_fmt,
	htmlentities_outstring_raw,
	flush_htmlentities,
	flush_htmlentities
};

ADD_OUTPUT_FILTER(htmlentities, sizeof(struct _entityrec));
