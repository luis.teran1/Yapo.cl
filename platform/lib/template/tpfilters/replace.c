#include <bpapi.h>
#include <string.h>

struct _replace {
	const char *from;
	const char *to;
	char *ff;
	char *ft;
	char *buffer;
	unsigned int length;
	unsigned int capacity;
};

static int
replace_outstring_raw(struct bpapi_output_chain *ochain, const char *str, size_t len) {
	struct _replace *rp = ochain->data;

	if (rp->length + len >= rp->capacity) {
		rp->capacity = rp->length + len + 32; /* +32 to allow shorter strings to be added without a new relloc */
		rp->buffer = xrealloc(rp->buffer, rp->capacity);
	}

	memcpy(rp->buffer + rp->length, str, len);
	rp->length += len;

	return 0;
}

static int
init_replace(const struct bpfilter *filter, struct bpapi *api, int argc, const struct tpvar **argv) {
	struct _replace *rp = BPAPI_OUTPUT_DATA(api);

	rp->from = tpvar_str(argv[0], &rp->ff);
	rp->to = tpvar_str(argv[1], &rp->ft);

	rp->length = 0;
	rp->capacity = 32; /* Allow up to 32 chars at start (31+null) */
	rp->buffer = xmalloc(sizeof(char) * rp->capacity);
	*rp->buffer = '\0';

	return 0;
}

static void
fini_replace(struct bpapi_output_chain *ochain) {
	struct _replace *rp = ochain->data;

	const char *rep;
	const char *tmp = rp->buffer;
	const char *end = rp->buffer + rp->length;
	int from_len = strlen(rp->from);
	int to_len = strlen(rp->to);

	if (from_len) {
		while (tmp < end && (rep = memmem(tmp, rp->length - (tmp - rp->buffer), rp->from, from_len))) {
			bpo_outstring_raw(ochain->next, tmp, rep - tmp);
			bpo_outstring_raw(ochain->next, rp->to, to_len);
			tmp = rep + from_len;
		}
	}

	bpo_outstring_raw(ochain->next, tmp, rp->length - (tmp - rp->buffer));

	if (rp->buffer)
		free(rp->buffer);

	free(rp->ff);
	free(rp->ft);
}

const struct bpapi_output replace_output = {
	self_outstring_fmt,
	replace_outstring_raw,
	fini_replace
};

ADD_OUTPUT_FILTER(replace, sizeof(struct _replace));
