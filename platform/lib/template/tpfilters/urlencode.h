#ifndef URLENCODE_H
#define URLENCODE_H
#include "macros.h"

static const unsigned char hexchars[] = "0123456789ABCDEF";
struct _urlenc{
	int pos;
	char buf[1024];
};
void flush_urlencode(struct bpapi_output_chain *ochain) VISIBILITY_HIDDEN;
int init_urlencode(const struct bpfilter *filter, struct bpapi *api, int argc, const struct tpvar **argv) VISIBILITY_HIDDEN;
void fini_urlencode(struct bpapi_output_chain *ochain) VISIBILITY_HIDDEN;
#endif
