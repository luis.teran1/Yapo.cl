#ifndef WRAP_H
#define WRAP_H
#include "macros.h"

#define MAX_WRAP_SIZE 4095
struct _wrap {
	int wrap_len;
	int len;
	int pos;
	int spacepos;
	int spacelen;
	int skip_html;
	char *str;
	int as_utf8;
};
int wrap_outstring_raw(struct bpapi_output_chain *ochain, const char *str, size_t l) VISIBILITY_HIDDEN;
int init_wrap(const struct bpfilter *filter, struct bpapi *api, int argc, const struct tpvar **argv) VISIBILITY_HIDDEN;
void fini_wrap(struct bpapi_output_chain *ochain) VISIBILITY_HIDDEN;
void flush_wrap(struct bpapi_output_chain *ochain) VISIBILITY_HIDDEN;
#endif
