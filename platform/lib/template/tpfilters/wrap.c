#include <ctype.h>
#include <string.h>
#include <bpapi.h>
#include <unicode/uchar.h>
#include "wrap.h"

int 
wrap_outstring_raw(struct bpapi_output_chain *ochain, const char *str, size_t l) {
	struct _wrap *wrap = ochain->data;
	int len = 0;
	const int wrap_html_called = wrap->skip_html;
	int prev_space_found=0;
	const char *end = str + l;
	int slen = l;

	while (str < end) {
		if (*str == '\n') {
			wrap->str[wrap->pos] = '\0';
			len += bpo_outstring_raw(ochain->next, wrap->str, wrap->pos);
			len += bpo_outstring_raw(ochain->next, "\n", 1);
			wrap->pos = 0;
			wrap->len = 0;
			wrap->spacepos = -1;
			str++;
			continue;
		}
		if (wrap->skip_html && wrap->pos < MAX_WRAP_SIZE) {
			if (*str == '<') {
				wrap->skip_html++;
				if (end - str >= 3 && strncmp("<br", str, 3) == 0) {
					wrap->str[wrap->pos] = '\0';
					len += bpo_outstring_raw(ochain->next, wrap->str, wrap->pos);
					len += bpo_outstring_raw(ochain->next, "<br", 3);
					wrap->pos = 0;
					wrap->len = 0;
					wrap->spacepos = -1;
					str+=3;
					continue;
				}
			}

			if (wrap->skip_html > 1) {
				if (*str == '>')
					wrap->skip_html--;
				wrap->str[wrap->pos++] = *str++;
				continue;
			}

		}

		if (wrap->len >= wrap->wrap_len || wrap->pos >= MAX_WRAP_SIZE) {
			if (isspace(*str) || wrap->spacepos == -1) {
				wrap->str[wrap->pos] = '\0';
				len += bpo_outstring_raw(ochain->next, wrap->str, wrap->pos);
				wrap->pos = 0;
				wrap->len = 0;
				wrap->spacepos = -1;
				if (isspace(*str)) {
					len += bpo_outstring_raw(ochain->next, "\n", 1);
					str++;
					continue;
				}
			} else {
				wrap->str[wrap->spacepos] = '\0';
				len += bpo_outstring_raw(ochain->next, wrap->str, wrap->spacepos);
				wrap->pos -= wrap->spacepos + 1;
				wrap->len -= wrap->spacelen + 1;
				memmove(wrap->str, wrap->str + wrap->spacepos + 1, wrap->pos);
				wrap->spacepos = -1;
				prev_space_found = 1;
			}

			if (wrap_html_called && !prev_space_found){
				len += bpo_outstring_raw(ochain->next, "<wbr />", 7);
			} else {
				len += bpo_outstring_raw(ochain->next, "\n", 1);
				prev_space_found=0;
			}

		}

		if (isspace(*str)) {
			wrap->spacepos = wrap->pos;
			wrap->spacelen = wrap->len;
		}

		if (wrap->as_utf8) {
			int i = 0;
			UChar32 ch;

			U8_NEXT(str, i, slen, ch);
			str += i;
			slen -= i;
			if (ch == U_SENTINEL) {
				continue;
			}

			U8_APPEND_UNSAFE(wrap->str, wrap->pos, ch);
		}
		else {
			wrap->str[wrap->pos++] = *str++;
		}
		wrap->len++;
	}
	return len;
}

int
init_wrap(const struct bpfilter *filter, struct bpapi *api, int argc, const struct tpvar **argv) {
	struct _wrap *wrap = BPAPI_OUTPUT_DATA(api);

	wrap->wrap_len = tpvar_int(argv[0]);
	wrap->skip_html = 0;
	wrap->pos = 0;
	wrap->len = 0;
	wrap->spacepos = -1;
	wrap->str = xmalloc(wrap->wrap_len + 1);
	wrap->as_utf8 = 0;
	return 0;
}

void
fini_wrap(struct bpapi_output_chain *ochain) {
	struct _wrap *wrap = ochain->data;
	free(wrap->str);
}

void
flush_wrap(struct bpapi_output_chain *ochain) {
	struct _wrap *wrap = ochain->data;

	if (wrap->pos) {
		wrap->str[wrap->pos] = '\0';
		bpo_outstring_raw(ochain->next, wrap->str, wrap->pos);
		wrap->pos = 0;
		wrap->len = 0;
		wrap->spacepos = -1;
	}
}

const struct bpapi_output wrap_output = {
	self_outstring_fmt,
	wrap_outstring_raw,
	fini_wrap,
	flush_wrap
};

ADD_OUTPUT_FILTER(wrap, sizeof(struct _wrap));
