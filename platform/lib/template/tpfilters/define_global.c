#include <bpapi.h>
#include "define.h"

static int
init_define_global(const struct bpfilter *filter, struct bpapi *api, int argc, const struct tpvar **argv) {
	struct _define *def = BPAPI_OUTPUT_DATA(api);

	def->name = tpvar_str(argv[0], &def->fn);
	def->schain = &api->schain;

	return 0;
}

static void
fini_define_global(struct bpapi_output_chain *ochain) {
	struct _define *def = ochain->data;

	if (def->buf) {
		struct bpapi_simplevars_chain *schain = def->schain;

		while (schain->next) {
			schain->cache_gen++;
			schain = schain->next;
		}
		if (bps_has_insert(schain)) {
			bps_insert(schain, def->name, def->buf);
		}
		free(def->buf);
	}
	free(def->fn);
}

const struct bpapi_output define_global_output = {
	self_outstring_fmt,
	define_outstring_raw,
	fini_define_global
};

ADD_OUTPUT_FILTER(define_global, sizeof(struct _define));
