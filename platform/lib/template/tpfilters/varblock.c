#include <bpapi.h>
#include "patchvars.h"

static int
init_varblock(const struct bpfilter *filter, struct bpapi *api, int argc, const struct tpvar **argv) {
	struct patch_data *data = BPAPI_SIMPLEVARS_DATA(api);

	patchvars_init(data, NULL, NULL, 0, NULL, 0);
	return 0;
}

#define varblock_simplevars patch_with_scope_simplevars
ADD_SIMPLEVARS_FILTER(varblock, sizeof(struct patch_data));
