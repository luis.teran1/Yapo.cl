#include <bpapi.h>
#include "tr.h"
#include "atomic.h"

static int 
tr_utf8_outstring_raw(struct bpapi_output_chain *ochain, const char *str, size_t len) {
	struct tr_data *data = ochain->data;
	char *trstr = replace_chars_utf8(str, len, data->mapp, data->nmap);
	int res = bpo_outstring_raw(ochain->next, trstr, strlen(trstr));

	free(trstr);
	return res;
}

struct tr_utf8_cache
{
	int (*mapp)[2];
	int nmap;
};

static void
rcu_cleanup(struct template_opaque_var *var)
{
	struct tr_utf8_cache *data = var->data;

	free(data->mapp);
	free(data);
	var->data = NULL;
}

static int
init_tr_utf8(const struct bpfilter *filter, struct bpapi *api, int argc, const struct tpvar **argv) {
	struct tr_data *data = BPAPI_OUTPUT_DATA(api);
	struct template_opaque_var *cache;
	struct tr_utf8_cache *cdata;

	data->chars = tpvar_str(argv[0], &data->fc);
	cache = argc == 2 ? tpvar_opaque(argv[1]) : NULL;

	if (cache && cache->data) {
		cdata = cache->data;
		data->mapp = cdata->mapp;
		data->nmap = cdata->nmap;
	} else {
		data->mapp = replace_chars_utf8_create_map(data->chars, &data->nmap);
		if (cache) {
			cdata = xmalloc(sizeof (*cdata));
			cdata->mapp = data->mapp;
			cdata->nmap = data->nmap;
			if (atomic_cas_ptr(&cache->data, NULL, cdata)) {
				free(cdata);
				data->free_mapp = 1;
			} else {
				cache->cleanup = rcu_cleanup;
			}
		} else {
			data->free_mapp = 1;
		}
	}

	return 0;
}

const struct bpapi_output tr_utf8_output = {
	self_outstring_fmt,
	tr_utf8_outstring_raw,
	fini_tr,
};

ADD_OUTPUT_FILTER(tr_utf8, sizeof(struct tr_data));
