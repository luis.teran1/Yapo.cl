#include <bpapi.h>
#include "define.h"

/* DEFINE filter hooking the get_element, has_key and length functions */

int
define_outstring_raw(struct bpapi_output_chain *ochain, const char *str, size_t len) {
	struct _define *def = ochain->data;

	bufwrite(&def->buf, &def->len, &def->pos, str, len);

	return 0;
}


static int
init_define(const struct bpfilter *filter, struct bpapi *api, int argc, const struct tpvar **argv) {
	struct _define *def = BPAPI_OUTPUT_DATA(api);

	switch (argc) {
	case 2:
		def->even_if_empty = tpvar_int(argv[1]);
	case 1:
		def->name = tpvar_str(argv[0], &def->fn);
		break;
	default:
		return 1;
	}
	def->schain = &api->schain;

	return 0;
}

static void
fini_define(struct bpapi_output_chain *ochain) {
	struct _define *def = ochain->data;

	if (def->buf) {
		if (bps_has_insert(def->schain)) {
			bps_insert(def->schain, def->name, def->buf);
		}
		free(def->buf);
	} else if (def->even_if_empty && bps_has_insert(def->schain)) {
		bps_insert(def->schain, def->name, "");
	}
	free(def->fn);
}

const struct bpapi_output define_output = {
	self_outstring_fmt,
	define_outstring_raw,
	fini_define
};

ADD_OUTPUT_FILTER(define, sizeof(struct _define));
