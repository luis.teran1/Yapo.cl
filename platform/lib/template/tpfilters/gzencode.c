#include <bpapi.h>
#include <zlib.h>

struct _gzencode {
	z_streamp stream;
};

static int
init_gzencode(const struct bpfilter *filter, struct bpapi *api, int argc, const struct tpvar **argv) {
	struct _gzencode *gz = BPAPI_OUTPUT_DATA(api);
	int status;
	Bytef *buf = zmalloc(1024 * 1024);
	z_streamp stream;

	stream = zmalloc(sizeof(*stream));

	stream->data_type = Z_ASCII;
	stream->opaque = (voidpf) Z_NULL;
	stream->avail_out = 1024 * 1024;
	stream->next_out = buf;
	stream->zalloc = Z_NULL;
	stream->zfree = Z_NULL;
	status = deflateInit2(stream, Z_DEFAULT_COMPRESSION, Z_DEFLATED, MAX_WBITS + 16, MAX_MEM_LEVEL, Z_DEFAULT_STRATEGY);
	gz->stream = stream;
	if (status != Z_OK) 
		return 1;

	return 0;
}

static int
gzencode_outstring_raw(struct bpapi_output_chain *ochain, const char *str, size_t len) {
	struct _gzencode *gz = ochain->data;
	int status;

	if (len == 0)
		return 0;

	gz->stream->next_in = (Bytef *)str;
	gz->stream->avail_in = len;

	status = deflate(gz->stream, Z_NO_FLUSH);
	if (status == Z_OK || status == Z_BUF_ERROR) {
		if (gz->stream->avail_out == 0 || status == Z_BUF_ERROR) {
			gz->stream->next_out = xrealloc(gz->stream->next_out - gz->stream->total_out, 1024);
			gz->stream->next_out += gz->stream->total_out;
			gz->stream->avail_out = 1024;
			deflate(gz->stream, Z_NO_FLUSH);
		}
		if (status < 0 && status != Z_BUF_ERROR) {
			char *buf;
			xasprintf(&buf, "Got error from deflate: %s", gz->stream->msg);
			bpo_outstring_raw(ochain->next, buf, strlen(buf));
			free(buf);
			return 1;
		}
	}

	return 0;
}

static void
fini_gzencode(struct bpapi_output_chain *ochain) {
	struct _gzencode *gz = ochain->data;
	int status;
	size_t len = gz->stream->total_out;

	for (;;) {
		if (gz->stream->avail_out) {
			status = deflate(gz->stream, Z_FINISH);

			if (status == Z_STREAM_END)
				break;
			if (status == Z_OK || status == Z_BUF_ERROR) {
				len = len * 2;
				gz->stream->next_out = xrealloc(gz->stream->next_out - gz->stream->total_out, len);
				gz->stream->next_out += gz->stream->total_out;
				gz->stream->avail_out = len;
			} 
			if (status < 0 && status != Z_BUF_ERROR) {
				char *buf;
				xasprintf(&buf, "Got error from deflate: %s", gz->stream->msg);
				bpo_outstring_raw(ochain->next, buf, strlen(buf));
				free(buf);
				return;
			}
		} else {
			char *buf;
			xasprintf(&buf, "Not enough space in avail_out");
			bpo_outstring_raw(ochain->next, buf, strlen(buf));
			free(buf);
			return;
		}
	}
	deflateEnd(gz->stream);
	bpo_outstring_raw(ochain->next, (char *)gz->stream->next_out - gz->stream->total_out, gz->stream->total_out);
	free(gz->stream->next_out - gz->stream->total_out);
	free(gz->stream);
}

static const struct bpapi_output gzencode_output = {
	self_outstring_fmt,
	gzencode_outstring_raw,
	fini_gzencode
};

ADD_OUTPUT_FILTER(gzencode, sizeof (struct _gzencode));
