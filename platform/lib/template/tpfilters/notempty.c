#include <bpapi.h>

struct notempty_data {
	const char *prefix;
	const char *suffix;
	char *fp;
	char *fs;
	int notempty;
};

static int 
notempty_outstring_raw(struct bpapi_output_chain *ochain, const char *str, size_t len) {
	struct notempty_data *data = ochain->data;
	int res = 0;

	if (len && !data->notempty) {
		if (data->prefix)
			res = bpo_outstring_raw(ochain->next, data->prefix, strlen(data->prefix));
		data->notempty = 1;
	}
	return res + bpo_outstring_raw(ochain->next, str, len);
}

static int
init_notempty(const struct bpfilter *filter, struct bpapi *api, int argc, const struct tpvar **argv) {
	struct notempty_data *data = BPAPI_OUTPUT_DATA(api);

	switch (argc) {
	case 2:
		data->suffix = tpvar_str(argv[1], &data->fs);
		/* Fall through */
	case 1:
		data->prefix = tpvar_str(argv[0], &data->fp);
	}

	return 0;
}

static void
fini_notempty(struct bpapi_output_chain *ochain) {
	struct notempty_data *data = ochain->data;

	if (data->notempty && data->suffix)
		bpo_outstring_raw(ochain->next, data->suffix, strlen(data->suffix));

	free(data->fp);
	free(data->fs);
}

const struct bpapi_output notempty_output = {
	self_outstring_fmt,
	notempty_outstring_raw,
	fini_notempty
};

ADD_OUTPUT_FILTER(notempty, sizeof(struct notempty_data));
