#include <bpapi.h>
#include <string.h>
#include <qsort.h>
#include <math.h>
#include "sort.h"

static int
init_sort_and_split(const struct bpfilter *filter, struct bpapi *api, int argc, const struct tpvar **argv) {
	struct sort_filter_data *data = BPAPI_OUTPUT_DATA(api);
	const char *flags;
	int i;

	if (argc < 4)
		return 1;

	data->schain = &api->schain;
	data->alloced_elems = tpvar_int(argv[0]);
	if (!data->alloced_elems) {
		data->alloced_elems = 8;		/* XXX - anything is better than 0 */
	}
	TPVAR_STRTMP(flags, argv[1]);
	for (; *flags; flags++) {
		switch (*flags) {
		case 'P':
			data->flags |= SORT_PURGE_EMPTY;
			break;
		case 'A':
			data->flags |= GENERATE_ARRAY_NAME;
			break;
		case 'N':
			/* No longer used. */
			break;
		case 'C':
			/* C implies A since we can't do va_arg in fini_sort */
			data->flags |= SORT_COLS_BREAK | GENERATE_ARRAY_NAME;
			break;
		case 'n':
			data->flags |= SORT_NUMERIC;
			break;
		case 'r':
			data->flags |= SORT_DESC;
			break;
		case 'l':
			data->flags |= SORT_LOCALE;
			data->sort_locale = filter_state(api, "sort_locale", filter_state_sort, NULL);
			break;
		}
	}

	/* If num args is char or int */
	data->num_arg = tpvar_int(argv[2]);

	/* If sorting arrays should be by cols or by rows */
	if (data->flags & SORT_COLS_BREAK) {
		data->list_prefix = tpvar_strdup(argv[3]);
	} else {
		data->num_rows = data->num_arg;

		data->lists = xmalloc(sizeof(*data->lists) * data->num_rows);

		/* If the arrays names should be generated or given */
		if (data->flags & GENERATE_ARRAY_NAME) {
			const char *arg;

			TPVAR_STRTMP(arg, argv[3]);

			for (i = 0; i < data->num_rows; i++)
				xasprintf(&data->lists[i], "%s%d", arg, i);
		} else {
			for (i = 0; i < data->num_rows; i++)
				data->lists[i] = tpvar_strdup(argv[3 + i]);
		}
	}

	data->elems = zmalloc(sizeof (*data->elems) * data->alloced_elems);
	return 0;
}


static void
fini_sort_and_split(struct bpapi_output_chain *ochain) {
	int (*sortfn)(struct sort_filter_elem *, struct sort_filter_elem *, struct sort_filter_data *data);
	struct sort_filter_data *data = ochain->data;
	int i;
	int j;
	int x;

	if (!data)
		return;
	if (!data->num_elems)
		goto out;

	switch (data->flags & (SORT_NUMERIC|SORT_LOCALE)) {
	case SORT_NUMERIC:
		sortfn = sort_elem_num_cmp;
		break;
	case SORT_LOCALE:
		sortfn = sort_elem_locale_cmp;
		break;
	default:
		sortfn = sort_elem_cmp;
		break;
	}

	if (data->flags & SORT_DESC) {
		QSORT(struct sort_filter_elem, data->elems, data->num_elems, SORT_FN_DESC);
	} else {
		QSORT(struct sort_filter_elem, data->elems, data->num_elems, SORT_FN);
	}

	if (data->flags & SORT_COLS_BREAK) {
		/* Allocate space for the lists */
		data->num_rows = ceil((float)data->num_elems / (float)data->num_arg);
		data->lists = xmalloc(sizeof(*data->lists) * data->num_rows);

		for (i = 0; i < data->num_rows; i++)
			xasprintf(&data->lists[i], "%s%d", data->list_prefix, i);
		free(data->list_prefix);

		j = 0;
		/* Elements inserted into lists by cols */
		for (x = 0; x < data->num_arg; x++) {
			int fill_cells = data->num_rows;
			if (data->num_elems % data->num_arg != 0 && (x + 1) > (data->num_elems % data->num_arg)) 
				fill_cells--;
			for (i = 0; i < fill_cells; i++) {
				if (j < data->num_elems) {
					bps_set_int(data->schain, data->lists[i], data->elems[j].idx);
					free(data->elems[j].str);
					j++;
				}
			}
		}
	} else {
		/* Elements inserted into lists by rows */
		for (i = 0; i < data->num_elems; i++) {
			j = i / ((data->num_elems + data->num_rows - 1) / data->num_rows);
			bps_set_int(data->schain, data->lists[j], data->elems[i].idx);
			free(data->elems[i].str);
		}
	}

out:
	free(data->elems);
	for (j = 0; j < data->num_rows; j++)
		free(data->lists[j]);
	free(data->lists);
}

static const struct bpapi_output sort_and_split_output = {
	self_outstring_fmt,
	sort_outstring_raw,
	fini_sort_and_split,
	flush_sort
};

ADD_OUTPUT_FILTER(sort_and_split, sizeof(struct sort_filter_data));
