#include <bpapi.h>
#include <time.h>

/*
 * nice_time
 *
 * Reformats a time string for easier reading.
 */

struct nice_time_data {
	char *str;
	int pos;
	int len;
};

static int 
nice_time_outstring_raw(struct bpapi_output_chain *ochain, const char *str, size_t len) {
	struct nice_time_data *data = ochain->data;

	bufwrite(&data->str, &data->len, &data->pos, str, len);
	return 0;
}

static int
init_nice_time(const struct bpfilter *filter, struct bpapi *api, int argc, const struct tpvar **argv) {
	return 0;
}

static void
fini_nice_time(struct bpapi_output_chain *ochain) {
	struct nice_time_data *data = ochain->data;

	if (data->str)
		free(data->str);
}

static void
flush_nice_time(struct bpapi_output_chain *ochain) {
	struct nice_time_data *data = ochain->data;

	if (data->str) {
		struct tm tm = {0};
		char buf[256];

		if (!strptime(data->str, "%Y-%m-%d %H:%M", &tm)) {
			bpo_outstring_raw(ochain->next, data->str, data->pos);
		} else {
			/* 2006-05-18 10:22 */
			size_t l = strftime(buf, sizeof(buf), "%Y-%m-%d %R", &tm);
			bpo_outstring_raw(ochain->next, buf, l);
			free(data->str);
			data->str = NULL;
			data->len = data->pos = 0;
		}
	}
}

static const struct bpapi_output nice_time_output = {
	self_outstring_fmt,
	nice_time_outstring_raw,
	fini_nice_time,
	flush_nice_time
};

ADD_OUTPUT_FILTER(nice_time, sizeof(struct nice_time_data));
