#include <bpapi.h>

static int
init_include(const struct bpfilter *filter, struct bpapi *api, int argc, const struct tpvar **argv) {
	int i;

	for (i = 0 ; i < argc ; i++) {
		char *ft;
		const char *tmpl = tpvar_str(argv[i], &ft);
		call_template_auto(api, tmpl);
		free(ft);
	}

	return 0;
}

ADD_FILTER(include, NULL, 0, NULL, 0, NULL, 0);
