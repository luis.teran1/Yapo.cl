#ifndef QP_H
#define QP_H
#include "macros.h"
struct _qp {
	char header_encoding[32];
	int header_started;
	int line_length;
	int has_address;
};
int qp_outstring_raw(struct bpapi_output_chain *ochain, const char *str, size_t len) VISIBILITY_HIDDEN;
#endif
