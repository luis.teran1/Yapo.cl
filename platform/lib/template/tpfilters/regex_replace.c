#include <bpapi.h>
#include "cached_regex.h"

struct _regex_replace {
	struct cached_regex *regex;
	const char *replace;
	char *fr;
	char *buffer;
	unsigned int length;
	unsigned int capacity;
	unsigned int global;
};

static int 
regex_replace_outstring_raw(struct bpapi_output_chain *ochain, const char *str, size_t len) {
	struct _regex_replace *rp = ochain->data;

	if (rp->length + len >= rp->capacity) {
		rp->capacity = rp->length + len + 32; /* +32 to allow shorter strings to be added without a new relloc */
		rp->buffer = xrealloc(rp->buffer, rp->capacity);
	}

	memcpy(rp->buffer + rp->length, str, len);
	rp->length += len;

	return 0;
}

static int
init_regex_replace(const struct bpfilter *filter, struct bpapi *api, int argc, const struct tpvar **argv) {
	struct _regex_replace *rp = BPAPI_OUTPUT_DATA(api);
	const char *replace;

	rp->regex = tpvar_regex(argv[0]);
	replace = argc >= 2 ? tpvar_str(argv[1], &rp->fr) : NULL;
	rp->global = argc >= 3 ? tpvar_int(argv[2]) : 0;

	if (replace) 
		rp->replace = replace;
	else
		rp->replace = "";

	rp->length = 0; /* Not really needed, but VERY cheap and good for clarity */
	rp->capacity = 32; /* Allow up to 32 chars at start (31+null) */
	rp->buffer = xmalloc(sizeof(char) * rp->capacity);
	*rp->buffer = '\0';

	return 0;
}

static void
fini_regex_replace(struct bpapi_output_chain *ochain) {
	struct _regex_replace *rp = ochain->data;

	struct buf_string result = {0};
	int ret = 0;

	ret = cached_regex_replace(&result, rp->regex, rp->replace, rp->buffer, rp->length, rp->global);

	if (ret != -1)
		bpo_outstring_raw(ochain->next, result.buf, result.pos);

	/* bpo_outstring_raw(ochain->next, rp->buffer, rp->length); */

	if (rp->buffer)
		free(rp->buffer);

	free(rp->fr);

	if (result.buf)
		free(result.buf);
}

const struct bpapi_output regex_replace_output = {
	self_outstring_fmt,
	regex_replace_outstring_raw,
	fini_regex_replace
};

ADD_OUTPUT_FILTER(regex_replace, sizeof (struct _regex_replace));
