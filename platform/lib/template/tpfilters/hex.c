#include <bpapi.h>

static const char *hexchars_lower = "0123456789abcdef";

static size_t hex_encode_internal(char *dst, size_t dst_len, const char *src, size_t src_len, const char *alpha) {
	static const int stride = 2;
	size_t len = dst_len >= src_len*stride ? src_len : dst_len/stride;
	size_t i;

	for (i = 0; i < len ; ++i) {
		dst[i*stride+0] = alpha[(unsigned char)src[i] >> 4];
		dst[i*stride+1] = alpha[src[i] & 0x0f];
	}

	return i;
}

static int
hex_outstring_raw(struct bpapi_output_chain *ochain, const char *str, size_t len) {
	int res = 0;
	size_t written = 0;
	char buf[256];

	while (written < len) {
		size_t octets_processed = hex_encode_internal(buf, sizeof(buf), str + written, len - written, hexchars_lower);

		res += bpo_outstring_raw(ochain->next, buf, octets_processed*2);

		written += octets_processed;
	}

	return res;
}

static int
init_hex(const struct bpfilter *filter, struct bpapi *api, int argc, const struct tpvar **argv) {
	return 0;
}

const struct bpapi_output hex_output = {
	self_outstring_fmt,
	hex_outstring_raw,
	NULL,
	NULL
};

ADD_OUTPUT_FILTER(hex, 0);
