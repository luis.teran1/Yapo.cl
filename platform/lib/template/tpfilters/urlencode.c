#include <bpapi.h>
#include "urlencode.h"

static int
urlencode_outstring_raw(struct bpapi_output_chain *ochain, const char *str, size_t len) {
	const unsigned char *from;
	const unsigned char *end;
	unsigned char c;
	int res = 0;

	struct _urlenc *ue = ochain->data;

	from = (unsigned char*) str;
	end = (unsigned char*) str + len;

	while (from < end) {
		if (ue->pos >= 1010) {
			res += ue->pos;
			flush_urlencode(ochain);
		}

		c = *from++;

		if (c == ' ') {
			ue->buf[ue->pos++] = '+';
		} else if (c > 0x7f) {
			/* UTF8 encode */
			unsigned char out;
			ue->buf[ue->pos++] = '%';
			out = 0xC0 | (c >> 6 & 0x03);
			ue->buf[ue->pos++] = hexchars[out >> 4];
			ue->buf[ue->pos++] = hexchars[out & 15]; 
			ue->buf[ue->pos++] = '%';
			out = 0x80 | (c & 0x3F);
			ue->buf[ue->pos++] = hexchars[out >> 4];
			ue->buf[ue->pos++] = hexchars[out & 15]; 
		} else if ((c < '0' && c != '-' && c != '.') ||
				(c < 'A' && c > '9') ||
				(c > 'Z' && c < 'a' && c != '_') ||
				(c > 'z')) {
			ue->buf[ue->pos] = '%';
			ue->buf[ue->pos + 1] = hexchars[c >> 4];
			ue->buf[ue->pos + 2] = hexchars[c & 15]; 
			ue->pos += 3;
		} else {
			ue->buf[ue->pos++] = c;
		}
	}

	return 0;
}

int
init_urlencode(const struct bpfilter *filter, struct bpapi *api, int argc, const struct tpvar **argv) {
	return 0;
}

void
fini_urlencode(struct bpapi_output_chain *ochain) {
	flush_urlencode(ochain);
}

void
flush_urlencode(struct bpapi_output_chain *ochain) {
	struct _urlenc *ue = ochain->data;
	if (ue->pos) {
		bpo_outstring_raw(ochain->next, ue->buf, ue->pos);
		ue->pos = 0;
	}
}

static const struct bpapi_output urlencode_output = {
	self_outstring_fmt,
	urlencode_outstring_raw,
	fini_urlencode,
	flush_urlencode
};

ADD_OUTPUT_FILTER(urlencode, sizeof(struct _urlenc));
