#include <bpapi.h>
#include <unicode/utf8.h>
#include <unicode/uchar.h>
#include "trunc.h"
#include "strl.h"


/* trunc_utf8_with_breakchar */

static int
utf8_chars_count(const char *str) {
	int len = strlen(str), i, counter = 0;
	UChar32 ch;
	for (i = 0; i < len; ) {
		U8_NEXT(str, i, len, ch);
		if (ch == U_SENTINEL) {
			break;
		}
		counter++;
	}
	return counter;
}

static int
trunc_utf8_with_breakchar_outstring_raw(struct bpapi_output_chain *ochain, const char *str, size_t len) {
	struct _trunc *trunc = ochain->data;
	int i;
	UChar32 ch;
	char buf[5];
	int j, length = (int) len, b = 0;
	int res = 0;
	int break_char_length;
	int break_char_count;

	if (trunc->chars_left <= 0) {
		trunc->chars_left = -1;
		return 0;
	}
	/* if no break char push out */
	if (trunc->break_char[0]) {
		break_char_length = strlen(trunc->break_char);
		break_char_count = utf8_chars_count(trunc->break_char);
		for (i = 0 ; i < length ; ) {
			/* next char */
			U8_NEXT(str, i, length, ch);
			trunc->chars_left--;
			b += U8_LENGTH(ch);
			/* error or reach the end of the string */
			if (ch == U_SENTINEL || length - i == 0) {
				if (b > 0 && (!trunc->break_char_found || trunc->chars_left > 0)) {
					res += bpo_outstring_raw(ochain->next, str + (i-b), b);
				}
				break;
			}
			/* reach the trunc length */
			if (trunc->chars_left <= 0) {
				if (!trunc->break_char_found && b > 0) {
					res += bpo_outstring_raw(ochain->next, str + (i-b), b);
				}
				trunc->chars_left = -1;
				break;
			}

			/* Hit, push out and reset b */
			if (memcmp(str + i, trunc->break_char, break_char_length) == 0) {
				/* push in the break char if the trunc length is not reached */
				if (trunc->chars_left > 0) {
					res += bpo_outstring_raw(ochain->next, str + (i - b), b + break_char_length);
					i += break_char_length;
					trunc->chars_left -= break_char_count;
					trunc->break_char_found = 1;
				} else if (trunc->chars_left == 0) {
					res += bpo_outstring_raw(ochain->next, str + (i - b), b);
					/* job done */
					break;
				}
				b = 0;
			}
		}
	} else {
		for (i = 0 ; i < length ; ) {
			/* get out if no chars left */
			if (trunc->chars_left <= 0) {
				trunc->chars_left = -1;
				break;
			}
			U8_NEXT(str, i, length, ch);
			if (ch == U_SENTINEL) {
				break;
			}
			j = 0;
			U8_APPEND_UNSAFE(buf, j, ch);
			/* count down one char */
			trunc->chars_left--;
			res += bpo_outstring_raw(ochain->next, buf, j);
		}
	}
	return res;
}

static int
init_trunc_utf8_with_breakchar(const struct bpfilter *filter, struct bpapi *api, int argc, const struct tpvar **argv) {
	struct _trunc *trunc = BPAPI_OUTPUT_DATA(api);

	switch (argc) {
	case 3:
		tpvar_strcpy(trunc->break_char, argv[2], sizeof(trunc->break_char));
	case 2:
		tpvar_strcpy(trunc->terminator, argv[1], sizeof(trunc->terminator));
	case 1:
		trunc->filt_len = tpvar_int(argv[0]);
	}
	if (argc < 2)
		strlcpy(trunc->terminator, "...", sizeof(trunc->terminator));

	trunc->chars_left = trunc->filt_len;
	trunc->break_char_found = 0;
	return 0;
}

static void
flush_trunc_utf8_with_breakchar(struct bpapi_output_chain *ochain) {
	struct _trunc *trunc = ochain->data;

	if (trunc->chars_left == -1) {
		bpo_outstring_raw(ochain->next, trunc->terminator, strlen(trunc->terminator));
	}
	trunc->chars_left = trunc->filt_len;
}

static const struct bpapi_output trunc_utf8_with_breakchar_output = {
	self_outstring_fmt,
	trunc_utf8_with_breakchar_outstring_raw,
	NULL,
	flush_trunc_utf8_with_breakchar
};

ADD_OUTPUT_FILTER(trunc_utf8_with_breakchar, sizeof(struct _trunc));
