
#include "bpapi.h"
#include "bconf.h"
#include "buf_string.h"
#include "unicode.h"
#include "filter_state_ucnv.h"

#include <expat.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>

#define NS_DELIM '.'

struct xml_vtree_parse_data {
	struct bconf_node *stack[100];
	int level;
	struct buf_string databuf;
	const char *root_tag;
	UConverter *utf8_cnv;
	UConverter *dst_cnv;
};

static void
xml_vtree_add_data(struct bconf_node *tagnode, struct xml_vtree_parse_data *data) {
	if (data->databuf.buf) {
		char *str;
		int strsz = data->databuf.pos * 2 + 1;
		int strl = strsz;

		if (strsz >= 1024)
			str = xmalloc(strsz);
		else
			str = alloca(strsz);

		utf8_decode(str, &strl, data->databuf.buf, data->databuf.pos, data->utf8_cnv, data->dst_cnv);

		char *s = str;
		char *e = s + strl - 1;

		while (s <= e && isspace (*s))
			s++;
		while (e >= s && isspace (*e))
			e--;
		/* XXX normalize in the middle as well. */

		if (s <= e) {
			char kbuf[1024];

			snprintf(kbuf, sizeof(kbuf), "sub.%d.text", bconf_count(bconf_get(tagnode, "sub")));
			*(e + 1) = '\0';
			bconf_add_data(&tagnode, kbuf, s);
		}
		free(data->databuf.buf);
		data->databuf.buf = NULL;
		data->databuf.pos = data->databuf.len = 0;

		if (strsz >= 1024)
			free(str);
	}
}

static void XMLCALL
xml_vtree_start_tag(void *userData, const XML_Char *name, const XML_Char **atts) {
	struct xml_vtree_parse_data *data = userData;
	char *tagns = NULL;
	const char *nsdelim = strrchr(name, NS_DELIM);
	char kbuf[1024];

	if (nsdelim) {
		tagns = strndupa(name, nsdelim - name);
		name = nsdelim + 1;
	}

	if (data->level) {
		xml_vtree_add_data(data->stack[data->level], data);
		snprintf(kbuf, sizeof(kbuf), "sub.%d", bconf_count(bconf_get(data->stack[data->level], "sub")));
	} else {
		strcpy(kbuf, data->root_tag ?: "xml");
	}
	data->stack[data->level + 1] = bconf_add_listnode(&data->stack[data->level], kbuf);
	data->level++;

	/* XXX only utf-8 decodes values for now. Probably not a problem. */
	bconf_add_data(&data->stack[data->level], "tag", name);
	if (tagns)
		bconf_add_data(&data->stack[data->level], "ns", tagns);

	for (; *atts ; atts += 2) {
		char *v;
		int vsz;

		nsdelim = strchr(*atts, NS_DELIM);
		if (nsdelim || !tagns)
			snprintf(kbuf, sizeof(kbuf), "attr.%s", *atts);
		else
			snprintf(kbuf, sizeof(kbuf), "attr.%s.%s", tagns, *atts);

		vsz = strlen(*(atts + 1)) * 2 + 1;
		v = xmalloc(vsz);
		utf8_decode(v, &vsz, *(atts + 1), -1, data->utf8_cnv, data->dst_cnv);
		bconf_add_data(&data->stack[data->level], kbuf, *(atts + 1));
		free(v);
	}
}

static void XMLCALL
xml_vtree_end_tag(void *userData, const XML_Char *name) {
	struct xml_vtree_parse_data *data = userData;
	const char *t;
	char kbuf[1024];

	xml_vtree_add_data(data->stack[data->level], data);

	if (bconf_count(bconf_get(data->stack[data->level], "sub")) == 1
			&& (t = bconf_get_string(data->stack[data->level], "sub.0.text"))) {
		/* If a node only contains text, add it directly. */
		bconf_add_data(&data->stack[data->level], "text", t);
	}

	/* Copy to path lookup. */
	if (data->level) {
		const char *tag = bconf_get_string(data->stack[data->level], "tag");
		const char *ns = bconf_get_string(data->stack[data->level], "ns");
		struct bconf_node *dst;

		if (ns) {
			snprintf(kbuf, sizeof(kbuf), "path.%s.%s", ns, tag);
		} else {
			snprintf(kbuf, sizeof(kbuf), "path.%s", tag);
		}
		dst = bconf_add_listnode(&data->stack[data->level - 1], kbuf);
		bconf_merge(&dst, data->stack[data->level]);
	}

	data->level--;
}

static void XMLCALL
xml_vtree_data(void *userData, const XML_Char *s, int len) {
	struct xml_vtree_parse_data *data = userData;

	bswrite(&data->databuf, s, len);
}

static void
xml_vtree_free(struct shadow_vtree *vtree) {
	struct bconf_node *root = vtree->vtree.data;
	bconf_free(&root);
}

static int
init_xml_vtree(const struct bpfilter *filter, struct bpapi *api, int argc, const struct tpvar **argv) {
	struct shadow_vtree *subtree = BPAPI_VTREE_DATA(api);
	const char *xml;
	char *fxml;
	struct xml_vtree_parse_data data = {};
	XML_Parser parser;
	const char *charset = NULL;
	char *fr = NULL, *fcs = NULL;

	switch (argc) {
	case 3:
		charset = tpvar_str(argv[2], &fcs);
	case 2:
		data.root_tag = tpvar_str(argv[1], &fr);
	case 1:
		xml = tpvar_str(argv[0], &fxml);
		break;
	default:
		return 1;
	}

	data.utf8_cnv = filter_state(api, "ucnv_utf8", fs_ucnv_utf8, NULL);
	data.dst_cnv = filter_state(api, "ucnv_dst", fs_ucnv_dst, NULL);

	if (!charset)
		charset = vtree_get(api->vchain.next, "common", "charset", NULL);
	if (!charset)
		charset = "ISO-8859-1";

	/* Note: expat only supports UTF-8, UTF-16, ISO-8859-1, US-ASCII */
	parser = XML_ParserCreateNS(charset, NS_DELIM);

	XML_SetElementHandler(parser, xml_vtree_start_tag, xml_vtree_end_tag);
	XML_SetCharacterDataHandler(parser, xml_vtree_data);

	XML_SetUserData(parser, &data);

	if (!XML_Parse(parser, xml, strlen(xml), 1)) {
		char kbuf[128];

		snprintf(kbuf, sizeof(kbuf), "%s._error", data.root_tag ?: "xml");
		if (!bconf_get(data.stack[0], kbuf))
			bconf_add_data(&data.stack[0], kbuf, XML_ErrorString(XML_GetErrorCode(parser)));
	}
	bconf_vtree(&subtree->vtree, data.stack[0]);
	subtree->free_cb = xml_vtree_free;

	free(fxml);
	free(fcs);
	free(fr);

	XML_ParserFree(parser);
	return 0;
}

#define xml_vtree_vtree shadow_vtree

ADD_VTREE_FILTER(xml_vtree, sizeof(struct shadow_vtree));
