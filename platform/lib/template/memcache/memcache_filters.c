
#include <ctemplates.h>
#include <vtree.h>
#include <logging.h>
#include <fd_pool.h>

#include "memcache_filters.h"


struct fd_pool *
get_fd_pool(struct bpapi_vtree_chain *vtree, const char *engine, struct bpapi_vtree_chain *node, int *free_pool) {
	struct fd_pool *pool;

	if (vtree_haskey(vtree, "_memcache", "conf", engine, NULL)) {
		pool = (void*)vtree_get(vtree, "_memcache", "conf", engine, NULL);
		*free_pool = 0;
	} else {
		pool = fd_pool_create(vtree_getnode(vtree, node, "common", engine, NULL), 0, NULL);
		if (!pool)
			xerrx(1, "(CRIT) Key-value storage engine %s not correctly configured", engine);
		*free_pool = 1;
	}

	return pool;
}
