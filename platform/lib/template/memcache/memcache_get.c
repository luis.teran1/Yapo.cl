
#include <bpapi.h>
#include <memcacheapi.h>
#include <fd_pool.h>

#include "memcache_filters.h"


static const struct tpvar *
tmpl_memcache_get(struct bpapi *api, struct tpvar *dst, enum bpcacheable *cc, int argc, const struct tpvar *argv[]) {
	const char *key;
	char *tmp;
	struct fd_pool *pool;
	int free_pool;
	struct bpapi_vtree_chain node = {0};
	const char *engine;

	if (argc != 2)
		return TPV_SET(dst, TPV_NULL);

	TPVAR_STRTMP(engine, argv[0]);
	TPVAR_STRTMP(key, argv[1]);

	pool = get_fd_pool(&api->vchain, engine, &node, &free_pool);

	tmp = memcache_get(NULL, key, pool);

	if (free_pool) {
		fd_pool_free(pool);
		vtree_free(&node);
	}

	*cc = BPCACHE_CANT;
	return TPV_SET(dst, tmp ? TPV_DYNSTR(tmp) : TPV_NULL);
}

ADD_TEMPLATE_FUNCTION_WITH_NAME(tmpl_memcache_get, memcache_get);
