#ifndef MEMCACHE_FILTERS_H
#define MEMCACHE_FILTERS_H

/* TODO: Can probably be generalzed/merged with struct redis_set_data */
struct memcache_set_data
{
	const char *engine;
	const char *key;
	int expire;
	char *fe, *fk;
	struct bpapi_vtree_chain *vtree;

	struct buf_string buf;
};


struct fd_pool *get_fd_pool(struct bpapi_vtree_chain *vtree, const char *engine, struct bpapi_vtree_chain *node, int *free_pool);

#endif
