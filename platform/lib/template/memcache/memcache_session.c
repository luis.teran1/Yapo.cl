
#include <stdio.h>

#include <bpapi.h>
#include <memcacheapi.h>
#include <fd_pool.h>
#include <bconf.h>
#include <logging.h>

#include "memcache_filters.h"


static void
memcache_session_free(struct shadow_vtree *sv) {
	struct fd_pool *pool = sv->cbdata;
	struct bconf_node *node = sv->vtree.data;

	if (pool) {
		fd_pool_free(pool);
	}
	bconf_free(&node);
}

static int
init_memcache_session(const struct bpfilter *filter, struct bpapi *api, int argc, const struct tpvar **argv) {
	struct shadow_vtree *subtree = BPAPI_VTREE_DATA(api);
	struct bconf_node *bnode = NULL;
	struct bpapi_vtree_chain node = {0};
	char *fe;
	const char *engine = tpvar_str(argv[0], &fe);
	struct fd_pool *pool;

	if (!engine)
		goto out; /* XXX error */

	if (vtree_haskey(api->vchain.next, "_memcache", "conf", engine, NULL))
		goto out; /* No point in recreating. */

	if (!vtree_getnode(api->vchain.next, &node, "common", engine, NULL))
		goto out;

	pool = fd_pool_create(&node, 0, NULL);

	if (pool) {
		char *key;
		int res;

		ALLOCA_PRINTF(res, key, "_memcache.conf.%s", engine);
		bconf_add_bindata(&bnode, key, pool);
		bconf_vtree(&subtree->vtree, bnode);
		fd_pool_clear_node(pool);
	}
	vtree_free(&node);

	subtree->free_cb = memcache_session_free;
	subtree->cbdata = pool;

out:
	free(fe);
	return 0;
}

#define memcache_session_vtree shadow_vtree

ADD_VTREE_FILTER(memcache_session, sizeof(struct shadow_vtree));
