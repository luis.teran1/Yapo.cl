
#include <bpapi.h>
#include <memcacheapi.h>
#include <fd_pool.h>
#include <logging.h>

#include "memcache_filters.h"


static int
memcache_set_outstring_raw(struct bpapi_output_chain *ochain, const char *str, size_t len) {
	struct memcache_set_data *data = ochain->data;

	bufwrite(&data->buf.buf, &data->buf.len, &data->buf.pos, str, len);

	return 0;
}


static int
init_memcache_set(const struct bpfilter *filter, struct bpapi *api, int argc, const struct tpvar **argv) {
	struct memcache_set_data *data = BPAPI_OUTPUT_DATA(api);

	data->engine = tpvar_str(argv[0], &data->fe);
	data->key = tpvar_str(argv[1], &data->fk);
	if (argc >= 3)
		data->expire = tpvar_int(argv[2]);
	else
		data->expire = -1;
	data->vtree = &api->vchain;

	return 0;
}

static void
fini_memcache_set(struct bpapi_output_chain *ochain) {
	struct memcache_set_data *data = ochain->data;
	struct fd_pool *pool;
	struct bpapi_vtree_chain node = {0};
	int free_pool;

	pool = get_fd_pool(data->vtree, data->engine, &node, &free_pool);

	/* Connection is handled internally */
	int res = memcache_set(NULL, data->key, data->buf.buf ?: "", data->expire, pool);

	if (res < 1)
		log_printf(LOG_ERR, "memcache_set call failed: res=%d", res);

	free(data->buf.buf);

	if (free_pool) {
		fd_pool_free(pool);
		vtree_free(&node);
	}
	free(data->fe);
	free(data->fk);
}

const struct bpapi_output memcache_set_output = {
	self_outstring_fmt,
	memcache_set_outstring_raw,
	fini_memcache_set
};

ADD_OUTPUT_FILTER(memcache_set, sizeof(struct memcache_set_data));
