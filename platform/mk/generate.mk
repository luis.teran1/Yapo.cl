USE_BUILD?=yes

PLATFORMDIR?=${TOPDIR}/platform

include ${PLATFORMDIR}/mk/all.mk

ifneq (${USE_BUILD},yes)
GENERATE_TARGETS+=${IN_FILES:.in=}

ifndef GENERATE_NO_INSTALL
ifndef INSTALL_IN_FILES 
INSTALL_TARGETS+=${GENERATE_TARGETS}
else
INSTALL_TARGETS+=${INSTALL_IN_FILES}
endif
endif

all: ${GENERATE_TARGETS}

CLEANFILES+=${GENERATE_TARGETS}

# for example depend php.ini on php.ini.in
${GENERATE_TARGETS}: ${IN_FILES}

# Running install or in-file will make sure generated all sources for all .in files.
install: ${GENERATE_TARGETS}
in-file: ${GENERATE_TARGETS}

# All generate targets shud always be rebuilt
.PHONY: ${GENERATE_TARGETS}

include ${PLATFORMDIR}/mk/clean.mk
endif
