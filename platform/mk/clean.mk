ifndef CL_I
CL_I:=1
CLEANFILES+=${EXTRACLEAN}

clean:
	@printf "\033[31m"
	rm ${RMFLAGS} -f ${CLEANFILES}	
	@printf "\033[0m"

cleandir: clean
	@printf "\033[31m"
	rm -f .depend*
	rm -f .in${LOCAL} ${IN_FILES:.in=}
	@printf "\033[0m"
ifdef CLEANDIRS
	+@for d in ${CLEANDIRS} ; do echo "Make clean in $$d"; ${MAKE} -C $$d cleandir; done
endif
endif
