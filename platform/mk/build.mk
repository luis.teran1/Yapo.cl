ifndef BUILD_I
BUILD_I:=yes
CL_I:=yes

${TOPDIR}/${BUILDPATH}:
	mkdir -p $@

ifeq ($(USE_BUILD),yes)

RELDIR:=$(patsubst $(abspath ${TOPDIR})/%,%,$(abspath ${CURDIR}))
OBJDIR?=${TOPDIR}/${BUILDPATH}/${RELDIR}
FIRST_MAKEFILE:=$(firstword ${MAKEFILE_LIST})

$(filter-out cleandir,${DEFAULT_TARGETS}): ${OBJDIR}/${FIRST_MAKEFILE}
	+${MAKE} -C ${OBJDIR} -f ${FIRST_MAKEFILE} $@

${OBJDIR}/${FIRST_MAKEFILE}: ${FIRST_MAKEFILE}
	mkdir -p ${OBJDIR}
	sed -e '1i\
USE_BUILD=no\
SRCDIR=$(abspath ${CURDIR})\
PLATFORMDIR=$(abspath ${PLATFORMDIR})' \
	    -e '/^USE_BUILD/d' \
	    -e 's,^TOPDIR.*,TOPDIR?=${TOPDIR},' \
	    -e '/CLEANDIRS/d' \
	    $< > $(dir $@)/.tmp.$(notdir $@).$$$$; \
	echo 'VPATH:=$${SRCDIR}:$${VPATH}' >> $(dir $@)/.tmp.$(notdir $@).$$$$; \
	echo 'CPPFLAGS+=-I. -I$${SRCDIR}' >> $(dir $@)/.tmp.$(notdir $@).$$$$; \
	mv -f $(dir $@)/.tmp.$(notdir $@).$$$$ $@

show-build:
	@echo ${OBJDIR}

cleandir: clean
	rm -f ${EXTRACLEAN}
ifdef CLEANDIRS
	+for d in ${CLEANDIRS} ; do ${MAKE} -C $$d cleandir ; done
endif

else
OBJDIR:=.
endif
endif
