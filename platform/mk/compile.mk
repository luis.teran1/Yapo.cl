ifndef TPC

CCbin = gcc
CXXbin = g++

COPTS?=-O2 -g -fvisibility=hidden -fstack-protector
CWARNFLAGS?=-Wall -Werror -Wwrite-strings -Wpointer-arith -Wcast-align -Wsign-compare -Wformat-security -Wmissing-declarations
CONLYWARN?=-Wold-style-definition
CCACHE?=ccache

ifndef CCC_ANALYZER_ANALYSIS
CC:=${CCACHE} ${CCbin}
CXX:=${CCACHE} ${CXXbin}
else ifdef NO_ANALYSIS
CC:=${CCACHE} ${CCbin}
CXX:=${CCACHE} ${CXXbin}
else
CC_NO_ANALYSIS:=${CCACHE} ${CCbin}
CXX_NO_ANALYSIS:=${CCACHE} ${CXXbin}
endif
FLEX?=flex
YACC=bison
ifneq (${TOPDIR},${PLATFORMDIR})
TPC_BIN_DIR=$(subst ${TOPDIR}, ${TOPDIR}/${BUILDPATH}, ${PLATFORMDIR}/bin/tpc)
else
TPC_BIN_DIR=${PLATFORMDIR}/${BUILDPATH}/bin/tpc
endif

TPC?=${TPC_BIN_DIR}/tpc
TPCSRC?=${PLATFORMDIR}/bin/tpc

PICFLAGS=-fpic -shared
PICDEFINES=-DPIC

CSTD?=-std=gnu99 -D_GNU_SOURCE
CXXSTD=-std=c++0x

CFLAGS=${CSTD} ${COPTS} ${CPPFLAGS} ${CONLYFLAGS} -pipe
CXXFLAGS=${CXXSTD} ${COPTS} ${CPPFLAGS} ${CXXOPTS} -pipe
PICCFLAGS=${CFLAGS} ${PICFLAGS} ${PICDEFINES}

ifeq (${SOVERBOSE},1)
TPCFLAGS=-v
else
TPCFLAGS=
endif

SRCS_LL=${filter %.ll, ${SRCS}}
SRCS_YY=${filter %.yy, ${SRCS}}
SRCS_CC=${filter %.cc, ${SRCS}}
SRCS_CXX=${filter %.cxx, ${SRCS}}
SRCS_L=${filter %.l, ${SRCS}}
SRCS_Y=${filter %.y, ${SRCS}}
SRCS_TMPL=${filter %.tmpl, ${SRCS}}
SRCS_C=${filter %.c, ${SRCS}} ${SRCS_TMPL:.tmpl=.c}
SRCS_GPERF_ENUM=${filter %.gperf.enum, ${SRCS}}

OBJS+=${SRCS_YY:.yy=.o}
OBJS+=${SRCS_LL:.ll=.o}
OBJS+=${SRCS_CC:.cc=.o}
OBJS+=${SRCS_CXX:.cxx=.o}
OBJS+=${SRCS_Y:.y=.o}
OBJS+=${SRCS_L:.l=.o}
OBJS+=${SRCS_C:.c=.o}
#OBJS+=${SRCS_TMPL:.tmpl=.o}

HDRS+=${SRCS_GPERF_ENUM:.gperf.enum=.h} $(foreach gssrc,${GPERF_SWITCH},$(lastword $(subst :, ,${gssrc})).h)

PICOBJS=${OBJS:.o=.soo}

CLEANFILES+=${SRCS_LL:.ll=.cc}
CLEANFILES+=${SRCS_YY:.yy=.cc}
CLEANFILES+=${SRCS_YY:.yy=.hh}
CLEANFILES+=${SRCS_L:.l=.c}
CLEANFILES+=${SRCS_Y:.y=.c}
CLEANFILES+=${SRCS_Y:.y=.h}
CLEANFILES+=${SRCS_TMPL:.tmpl=.c}
CLEANFILES+=${SRCS_GPERF_ENUM:.gperf.enum=.h}

CLEANFILES+=${OBJS} ${PICOBJS}

ifeq "${SRCS_LL}${SRCS_YY}${SRCS_CC}${SRCS_CXX}" ""
LD=${CC}
else
LD=${CXX}
endif

GPERF=gperf
GPERF_ENUM=${PLATFORMDIR}/bin/gperf_enum.pl

ifdef OBJDIRS
PREOBJDEPEND+=objdirs

objdirs:
	for d in ${OBJDIRS}; do mkdir -p $$d; done
endif

.c.o:
	$(or ${CC_$(subst .,_,$(notdir $<))},${CC}) -c $(filter-out ${CCFILTEROPTIONS},${CFLAGS} $(or ${CWARNFLAGS_$(subst .,_,$(notdir $<))},${CWARNFLAGS}) ${CONLYWARN}) ${LIBPIC} -o $@ $<
.cc.o:
	${CXX} -c ${CXXFLAGS} $(or ${CWARNFLAGS_$(subst .,_,$(notdir $<))},${CWARNFLAGS}) -o $@ $<
.cxx.o:
	${CXX} -c ${CXXFLAGS} $(or ${CWARNFLAGS_$(subst .,_,$(notdir $<))},${CWARNFLAGS}) -o $@ $<
.ll.cc:
	${FLEX} -+ -o$@ $<
.l.c:
	${FLEX} -o$@ $<
.yy.cc .yy.hh:
	${YACC} -y -d --debug -o $(notdir $(<:yy=cc)) $<
.y.c .y.h:
	${YACC} -y -d --debug -o $(notdir $(<:y=c)) $<
.c.soo:
	$(or ${CC_$(subst .,_,$(notdir $<))},${CC}) -c $(filter-out ${CCFILTEROPTIONS},${PICCFLAGS} $(or ${CWARNFLAGS_$(subst .,_,$(notdir $<))},${CWARNFLAGS}) ${CONLYWARN}) -o $@ $<

%.c: %.tmpl ${TPC}
	@[ -d "$(dir $@)" -o "$(dir $@)" = "" -o "$(dir $@)" = ./ ] || mkdir -p "$(dir $@)"
	${TPC} ${TPCFLAGS} ${TMPLDIR}${@:.c=.tmpl} $@ $<

%.h: %.gperf
	${GPERF} ${GPERFFLAGS} -L ANSI-C --output-file=$@ $<

%.gperf: %.gperf.enum
	${GPERF_ENUM} $@ $<

${TPC}: $(foreach file,lex.ll lang.yy *.cc *.h,${TPCSRC}/${file}) # XXX dynamic?
	+${MAKE} -C ${TPCSRC} all

define gperf_switch_rule
$(lastword $(subst :, ,${gssrc})).gperf: $(firstword $(subst :, ,${gssrc}))
	${GPERF_ENUM} -s $(lastword $(subst :, ,${gssrc})).gperf ${SRCDIR}/$(firstword $(subst :, ,${gssrc}))
endef

$(foreach gssrc,${GPERF_SWITCH},$(eval ${gperf_switch_rule}))

endif
