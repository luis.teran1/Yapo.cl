USE_BUILD?=yes

PLATFORMDIR?=${TOPDIR}/platform

include ${PLATFORMDIR}/mk/all.mk

ifneq (${USE_BUILD},yes)
INPARSE?=${PLATFORMDIR}/bin/in/in.pl
DESTDIR=${TOPDIR}/${BUILDPATH}
INCLUDEPARSE?=${PLATFORMDIR}/bin/in/include.sh

ifdef IN_FILES

first_makefile := $(word 1,$(MAKEFILE_LIST))
in-file: .in${LOCAL}

%.in: %.include
		sh ${INCLUDEPARSE} $< $@ 

.in${LOCAL}: ${IN_FILES}
	${INPARSE} $^ > .in${LOCAL}
	+${MAKE} -f ${first_makefile} ${IN_FILES:.in=}

-include .in${LOCAL}

$(filter-out ${IN_TARGETS},${IN_FILES:.in=}):
	-rm -f .in${LOCAL}
	+${MAKE} -f ${first_makefile} in-file
endif
endif
