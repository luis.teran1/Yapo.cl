# This is a special makefile fragment for specific tmpltut filters that want to be tested inside other regress environments
# like trans and search. You most likely don't want to use it, ever.

TMPLTUT=${DEST_BIN}/tmpltut

EXTRACLEAN+=.tmpltut.out

tut-%:
	${SYSLOGHOOK} ${CMD_PREFIX} ${TMPLTUT} -c ${DEST}/regress/conf/bconf.conf $(subst tut-,,$@) > .tmpltut.out
	${MATCH} .tmpltut.out $@.out
