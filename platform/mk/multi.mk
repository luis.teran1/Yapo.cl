PLATFORMDIR?=${TOPDIR}/platform

include ${PLATFORMDIR}/mk/all.mk

${DEFAULT_TARGETS}:
	+@for local_mk_file in ${LOCAL_MAKEFILES}; do \
		printf "Making \033[34m$@\033[0m in \033[36m$$local_mk_file\033[0m\n"; \
		${MAKE} --no-print-directory $@ LOCAL=_`basename $$local_mk_file .mk` DESTDIR=${DESTDIR} TOPDIR=${TOPDIR} INSTALLROOT=${INSTALLROOT} STAMPFILE=${STAMPFILE} -f $$local_mk_file || exit 1; \
	done 
	@if [ "$@" = cleandir -a -n "${EXTRACLEAN}${CLEANFILES}" ]; then \
		rm -rf ${EXTRACLEAN} ${CLEANFILES}; \
	fi || exit 1

CL_I=yes
