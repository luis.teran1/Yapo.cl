BUILD_VERSION ?= $(shell cd ${PLATFORMDIR} ; sh ${PLATFORMDIR}/scripts/build/buildversion.sh)
USE_BUILD?=yes

PLATFORMDIR?=${TOPDIR}/platform
include ${PLATFORMDIR}/mk/all.mk

ifneq (${USE_BUILD},yes)
CPPFLAGS+=-I${TEMPLATESRCDIR}

ifndef TMPL_I
SRCDIR?=.
INFILES:=$(subst ${SRCDIR}/,,$(shell find ${SRCDIR} -name '*tmpl.in'))
IMPORTFILES:=$(subst ${SRCDIR}/,,$(shell find ${SRCDIR} -name '*tmpl.import'))
SRCS:=$(filter-out ${INFILES:.in=},$(subst ${SRCDIR}/,,$(shell find ${SRCDIR} -name '*tmpl' -and -not -name '.*')))
SRCS+=$(INFILES:.in=)
SRCS+=$(IMPORTFILES:.import=)

EXTRACLEAN+=$(INFILES:.in=)

LIB_STATIC=yes
LIB=libtemplates
RENAME_OBJS=yes

NO_REGRESS=yes

PREOBJDEPEND=${TPC}
BEFOREDEPEND=${TPC}

TMPLDIR?=$(notdir $(abspath ${CURDIR}))/

endif

USE_ATOMIC=yes

include ${PLATFORMDIR}/mk/lib.mk

.SUFFIXES: .in .import

in-file: $(INFILES:.in=) $(IMPORTFILES:.import=)

%.tmpl:	%.tmpl.in
	sed -e s.#BUILD_VERSION#.${BUILD_VERSION}. -e s.#CSS_VERSION#.${BUILD_VERSION}. -e s.#REVNO#.${REVNO}. < $< > $@

%.tmpl:	%.tmpl.import
	cp $(subst .import,,$(subst templates_estore,templates,$<)) $@

endif
