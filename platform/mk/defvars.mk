ifndef DEFVARS_I
DEFVARS_I:=1

GENPORTOFF?=0

PLATFORMDIR?=$(realpath ${PLATFORMDIR})

-include ${TOPDIR}/${BUILDPATH}/cache${GENPORTOFF}.mk

ifndef BUILD_STAGE
 BUILD_STAGE:=PROD
endif

SOURCE_PATH=${shell readlink -f ${TOPDIR}}

ifndef REGRESS_USER_ID
	ifndef JENKINS_ID
		REGRESS_USER_ID := $(shell if [ -f /.dockerenv ]; then echo 500; else id -u; fi)
	else
		REGRESS_USER_ID := ${JENKINS_ID}
	endif
	DEFINECACHE += REGRESS_USER_ID
endif

ifndef REGRESS_BLOCKET_ID
	ifdef COUNTRY
		REGRESS_BLOCKET_ID := ${COUNTRY}
	else
		REGRESS_BLOCKET_ID := $(shell id -un)
	endif
# DEFINECACHE += REGRESS_BLOCKET_ID
endif

ifndef GENERATE_GENPORT_BASE
	GENERATE_GENPORT_BASE = sh -c "expr ${GENPORTOFF} + \( ${REGRESS_USER_ID} - \( ${REGRESS_USER_ID} / 100 \) \* 100 \) \* 200 + 20000"
endif
ifndef GENPORT_BASE
	GENPORT_BASE := $(shell $(GENERATE_GENPORT_BASE))
	DEFINECACHE += GENPORT_BASE
endif
genport = $(shell expr ${GENPORT_BASE} + $(1))

ifndef REGRESS_FAST_DIR
  REGRESS_FAST_DIR=`find /run/shm /dev/shm /tmp $(abspath ${TOPDIR}) -maxdepth 0 -type d 2>/dev/null | head -n 1`/regress-`id -un`
  DEFINECACHE += REGRESS_FAST_DIR
endif

ifndef REGRESS_HOSTNAME
  REGRESS_HOSTNAME:=$(shell hostname -s | head -1)#cache
  DEFINECACHE += REGRESS_HOSTNAME
endif

ifndef REGRESS_DOMAIN
	REGRESS_DOMAIN:=$(shell hostname | head -1 | sed -e 's/^[^.]*\././')
	DEFINECACHE += REGRESS_DOMAIN
endif

ifndef REGRESS_HOST_ESCAPED
  REGRESS_HOST_ESCAPED:=$(shell hostname | head -1 | sed 's/\./_/g')
  DEFINECACHE += REGRESS_HOST_ESCAPED
endif

ifndef REGRESS_HOSTIP
  REGRESS_HOSTIP:=$(shell (hostname --all-ip-addresses 2>/dev/null || hostname -i) | xargs -n 1 | head -1)#cache
  DEFINECACHE += REGRESS_HOSTIP
endif

ifndef GCC_VERSION
	GCC_VERSION:=`${CC} --version | head -n 1 | sed -r 's/[^ ]+ \([^\)]*\) //' | cut -f 1 -d' '`
	GCC_MAJOR:=$(shell echo ${GCC_VERSION} | cut -d. -f 1)
	GCC_MINOR:=$(shell echo ${GCC_VERSION} | cut -d. -f 2)
	GCC_LT_4_5:=$(shell [ $(GCC_MAJOR) -lt 4 -o \( $(GCC_MAJOR) -eq 4 -a $(GCC_MINOR) -lt 5 \) ] && echo true)

# Set up a list of options that must be filtered if the available gcc version is too old to handle them.
ifeq ($(GCC_LT_4_5),true)
	CCFILTEROPTIONS:=-Wno-unused-result -Wno-unused-but-set-variable
endif
	DEFINECACHE += CCFILTEROPTIONS GCC_VERSION GCC_MAJOR GCC_MINOR
endif

ifndef LSOF
	LSOF:=$(shell which /usr/sbin/lsof lsof 2>/dev/null | head -n 1)
	DEFINECACHE += LSOF
endif

ifndef NETCAT
# If we have nc6 in path, prefer it
NETCAT:=$(call pathsearch,nc6)
ifeq (${NETCAT},)
NETCAT:=nc
NETCAT_LISTEN:=nc -6 -l
else
NETCAT_LISTEN:=${NETCAT} -l --half-close -p
NETCAT:=${NETCAT} --half-close
endif
DEFINECACHE += NETCAT NETCAT_LISTEN
endif

ifndef PGVERSION
 PGVERSION:=$(shell postgres --version | egrep -o [0-9]\.[0-9])
 DEFINECACHE += PGVERSION
endif

ifndef PGVERSION_MAJOR
 PGVERSION_MAJOR:=$(shell echo "$(PGVERSION)" | cut -f1 -d.)
 PGVERSION_MINOR:=$(shell echo "$(PGVERSION)" | cut -f2 -d.)
 DEFINECACHE += PGVERSION_MAJOR
 DEFINECACHE += PGVERSION_MINOR
endif

# XXX remove these.
HTTPD_WORKER_PORT:=80
HTTPD_PORT:=80
HTTPS_PORT:=443
HTTPS_CP_PORT:=4430

ifndef REGRESS_IMAGEPORT
 REGRESS_IMAGEPORT:=$(call genport,1)
 DEFINECACHE += REGRESS_IMAGEPORT
endif

ifndef REGRESS_BIDPORT
 REGRESS_BIDPORT:=$(call genport,35)
 DEFINECACHE += REGRESS_BIDPORT
endif

ifndef REGRESS_STREAMPORT
 REGRESS_STREAMPORT:=$(call genport,33)
 DEFINECACHE += REGRESS_STREAMPORT
endif

ifndef REGRESS_DAVPORT
 REGRESS_DAVPORT:=$(call genport,2)
 DEFINECACHE += REGRESS_DAVPORT
endif

ifndef REGRESS_MOD_IMAGE_PORT
 REGRESS_MOD_IMAGE_PORT:=$(call genport,98)
 DEFINECACHE += REGRESS_MOD_IMAGE_PORT
endif

ifndef REGRESS_MCPORT
 REGRESS_MCPORT:=$(call genport,3)
 DEFINECACHE += REGRESS_MCPORT
endif

ifndef REGRESS_ASEARCH_SEARCH_PORT
 REGRESS_ASEARCH_SEARCH_PORT:=$(call genport,10)
 REGRESS_ASEARCH_CMD_PORT:=$(call genport,12)
 REGRESS_ASEARCH_KEEPALIVE_PORT:=$(call genport,14)
 DEFINECACHE += REGRESS_ASEARCH_SEARCH_PORT REGRESS_ASEARCH_CMD_PORT REGRESS_ASEARCH_KEEPALIVE_PORT
endif

ifndef REGRESS_FAKESEARCH
 REGRESS_FAKESEARCH:=$(call genport,99)
 DEFINECACHE += REGRESS_FAKESEARCH
endif

ifndef REGRESS_CSEARCH_SEARCH_PORT
 REGRESS_CSEARCH_SEARCH_PORT:=$(call genport,20)
 REGRESS_CSEARCH_CMD_PORT:=$(call genport,21)
 REGRESS_CSEARCH_KEEPALIVE_PORT:=$(call genport,38)
 DEFINECACHE += REGRESS_CSEARCH_SEARCH_PORT REGRESS_CSEARCH_CMD_PORT  REGRESS_CSEARCH_KEEPALIVE_PORT
endif

ifndef REGRESS_BIDSEARCH_SEARCH_PORT
 REGRESS_BIDSEARCH_SEARCH_PORT:=$(call genport,26)
 REGRESS_BIDSEARCH_CMD_PORT:=$(call genport,27)
 REGRESS_BIDSEARCH_KEEPALIVE_PORT:=$(call genport,28)
 DEFINECACHE += REGRESS_BIDSEARCH_SEARCH_PORT REGRESS_BIDSEARCH_CMD_PORT REGRESS_BIDSEARCH_KEEPALIVE_PORT
endif
 
ifndef REGRESS_NEWS_SEARCH_PORT
 REGRESS_NEWS_SEARCH_PORT:=$(call genport,23)
 REGRESS_NEWS_CMD_PORT:=$(call genport,24)
 REGRESS_NEWS_KEEPALIVE_PORT:=$(call genport,39)
 DEFINECACHE += REGRESS_NEWS_SEARCH_PORT REGRESS_NEWS_CMD_PORT REGRESS_NEWS_KEEPALIVE_PORT
endif

ifndef REGRESS_CAMP_SEARCH_PORT
 REGRESS_CAMP_SEARCH_PORT:=$(call genport,22)
 REGRESS_CAMP_CMD_PORT:=$(call genport,25)
 REGRESS_CAMP_KEEPALIVE_PORT:=$(call genport,40)
 DEFINECACHE += REGRESS_CAMP_SEARCH_PORT REGRESS_CAMP_CMD_PORT REGRESS_CAMP_KEEPALIVE_PORT
endif

ifndef REGRESS_SMSSEARCH_SEARCH_PORT
 REGRESS_SMSSEARCH_SEARCH_PORT:=$(call genport,36)
 REGRESS_SMSSEARCH_CMD_PORT:=$(call genport,37)
 REGRESS_SMSSEARCH_KEEPALIVE_PORT:=$(call genport,41)
 DEFINECACHE += REGRESS_SMSSEARCH_SEARCH_PORT REGRESS_SMSSEARCH_CMD_PORT REGRESS_SMSSEARCH_KEEPALIVE_PORT
endif

ifndef REGRESS_MSEARCH_SEARCH_PORT
 REGRESS_MSEARCH_SEARCH_PORT:=$(call genport,42)
 REGRESS_MSEARCH_CMD_PORT:=$(call genport,43)
 REGRESS_MSEARCH_KEEPALIVE_PORT:=$(call genport,44)
 DEFINECACHE += REGRESS_MSEARCH_SEARCH_PORT REGRESS_MSEARCH_CMD_PORT REGRESS_MSEARCH_KEEPALIVE_PORT
endif

ifndef REGRESS_HTTPD_PORT
 REGRESS_HTTPD_PORT:=$(call genport,4)
 DEFINECACHE += REGRESS_HTTPD_PORT
endif

ifndef REGRESS_HTTPD_WORKER_PORT
 REGRESS_HTTPD_WORKER_PORT:=$(call genport,8)
 DEFINECACHE += REGRESS_HTTPD_WORKER_PORT
endif 

ifndef REGRESS_HTTPS_CP_PORT
 REGRESS_HTTPS_CP_PORT:=$(call genport,13)
 DEFINECACHE += REGRESS_HTTPS_CP_PORT
endif

ifndef REGRESS_HTTPS_PORT
 REGRESS_HTTPS_PORT:=$(call genport,11)
 DEFINECACHE += REGRESS_HTTPS_PORT
endif

ifndef REGRESS_FTPD_PORT
 REGRESS_FTPD_PORT:=$(call genport,32)
 DEFINECACHE += REGRESS_FTPD_PORT
endif

ifndef REGRESS_TRANS_CFPORT
 REGRESS_TRANS_PORT:=$(call genport,5)
 REGRESS_TRANS_FPORT:=$(call genport,6)
 REGRESS_TRANS_CPORT:=$(call genport,7)
 REGRESS_TRANS_CFPORT:=$(call genport,9)
 DEFINECACHE += REGRESS_TRANS_PORT REGRESS_TRANS_FPORT REGRESS_TRANS_CPORT REGRESS_TRANS_CFPORT
endif

ifndef BUILD_ARCH
 BUILD_ARCH := $(shell uname -m)
 DEFINECACHE += BUILD_ARCH
endif

ifndef XVFB_PATH
 ifeq ($(BUILD_ARCH),x86_64)
  APXS?=$(shell which /usr/sbin/apxs apxs2 2>/dev/null | head -n 1)
  HTTPD?=$(shell which /usr/sbin/httpd /usr/lib/apache2/mpm-prefork/apache2 2>/dev/null)
  HTTPD_WORKER?=$(shell which /usr/sbin/httpd.worker /usr/lib/apache2/mpm-worker/apache2 2>/dev/null)
  HTTPD_MOD_DIR?=$(call pickdir,/usr/lib64/httpd/modules:/usr/lib/apache2/modules)
  PHP_EXT_DIR?=/usr/lib64/php/modules/
  PHP_TOOL?=php
  XVFB_PATH=/usr/bin/Xvfb
 endif

 DEFINECACHE += HTTPD_MOD_DIR APXS HTTPD HTTPD_WORKER PHP_EXT_DIR PHP_TOOL XVFB_PATH
endif

ifndef APACHE_MINOR
 APACHEINCDIR=$(call pickdir,/usr/include/httpd:/usr/include/apache2)
 APACHE_MINOR:= $(shell sed -n 's/^.define AP_SERVER_MINORVERSION[A-Z_]*\ [^0-9]*\([0-9]\)[^0-9]*/\1/p' ${APACHEINCDIR}/ap_release.h)
 DEFINECACHE += APACHE_MINOR APACHEINCDIR
endif

ifndef PERL_INSTALL
 PERL_INSTALL:= /lib/$(shell perl -MConfig -le 'print "$$Config{package}/$$Config{version}"')
 DEFINECACHE += PERL_INSTALL
endif

# spiderpoints daemon.
ifndef REGRESS_SPOINTS_INPORT
 REGRESS_SPOINTS_INPORT:=$(call genport,29)
 REGRESS_SPOINTS_DUMPPORT:=$(call genport,30)
 DEFINECACHE += REGRESS_SPOINTS_INPORT REGRESS_SPOINTS_DUMPPORT
endif

ifndef REGRESS_STATPOINTS_PORT
 REGRESS_STATPOINTS_PORT:=$(call genport,31)
 REGRESS_STATPOINTS_PORT2:=$(call genport,34)
 DEFINECACHE += REGRESS_STATPOINTS_PORT REGRESS_STATPOINTS_PORT2
endif


ifndef REGRESS_PGSQL_HOST
 REGRESS_PGSQL_HOST:=${REGRESS_FAST_DIR}/pgsql${GENPORTOFF}/data
 DEFINECACHE += REGRESS_PGSQL_HOST
endif

ifndef BPVSCHEMA
 BPVSCHEMA:= bpv
 DEFINECACHE += BPVSCHEMA
endif

ifndef REGRESS_REDIS_PORT
 REGRESS_REDIS_PORT:=$(call genport,45)
 DEFINECACHE += REGRESS_REDIS_PORT 
endif

ifneq (${DEFINECACHE},)
${TOPDIR}/${BUILDPATH}/cache${GENPORTOFF}.mk: CACHEFORCE
	mkdir -p ${TOPDIR}/${BUILDPATH}
	rm -f $@
	@( $(foreach CACHEVAR,$(sort $(DEFINECACHE) ${CACHE_WRITTEN}),echo $(CACHEVAR) := $($(CACHEVAR));) ) >> $@
	echo CACHE_WRITTEN := $(sort $(DEFINECACHE) ${CACHE_WRITTEN}) >> $@

CACHEFORCE:
endif

REGRESS_MOBILE_PORT:=$(call genport,60)

endif #DEFVARS_I
