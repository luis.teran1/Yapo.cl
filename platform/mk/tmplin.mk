
USE_BUILD?=yes
USE_ATOMIC?=yes

include ${PLATFORMDIR}/mk/all.mk

ifneq (${USE_BUILD},yes)

include ${PLATFORMDIR}/mk/compile.mk
include ${PLATFORMDIR}/mk/commonsrc.mk

SHOW_TEMPLATE=$(subst ${TOPDIR},${TOPDIR}/${BUILDPATH},$(realpath ${PLATFORMDIR}))/bin/show_template/show_template

ifdef TMPLIN_FILES
CLEANFILES+=${TMPLIN_FILES:.tmpl=} ${TMPLIN_FILES:.tmpl=.so} ${TMPLIN_FILES:.tmpl=.soo} ${TMPLIN_FILES:.tmpl=.c}

# Example of convoluted make syntax, base on example in info make eval function page.
define tmplin_rules
${tmplin}: ${tmplin}.so ${SHOW_TEMPLATE}
	env TEMPLATES_PATH=. ${SHOW_TEMPLATE} ${TMPLIN_ARGS} $$@ > $$@ || (rm $$@ ; false)

${tmplin}.so: ${tmplin}.tsoo
	${LD} ${LDFLAGS} ${PICFLAGS} -o $$@ $$< -Wl,${PLATFORMDIR}/scripts/linker/discard_templates

${tmplin}.tsoo: ${tmplin}.c
	$${CC} -c -DBPAPI_GLOBAL_SYMBOLS $${PICCFLAGS} $$(or $${CWARNFLAGS_$$(subst .,_,$$(notdir $$<))},$${CWARNFLAGS}) $${CONLYWARN} -o $$@ $$<
endef

$(foreach tmplin,${TMPLIN_FILES:.tmpl=},$(eval ${tmplin_rules}))

ifndef TMPLIN_NOAUTO
in-file: ${TMPLIN_FILES:.tmpl=}
endif

CPPFLAGS+=-I${TEMPLATESRCDIR} -I${COMMONSRCDIR}

SHOW_TEMPLATESRC?=${PLATFORMDIR}/bin/show_template

${SHOW_TEMPLATE}: $(foreach file,show_template.c,${SHOW_TEMPLATESRC}/${file}) # XXX dynamic?
	+${MAKE} -C $(dir ${SHOW_TEMPLATE}) $(notdir ${SHOW_TEMPLATE})

endif
endif
