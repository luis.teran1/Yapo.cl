ifndef COMMONSRC_I
COMMONSRC_I:=1

include ${PLATFORMDIR}/mk/all.mk

COMMONSRCDIR=${PLATFORMDIR}/lib/common
SEARCHSRCDIR=${PLATFORMDIR}/lib/search
TEMPLATESRCDIR=${PLATFORMDIR}/lib/template
VOCABULARYSRCDIR=${PLATFORMDIR}/lib/vocabulary
APACHESRCDIR=${PLATFORMDIR}/lib/apache

CPPFLAGS+=-I${COMMONSRCDIR}

SCMCOORD_CONTRIB?=/opt/scmcoord_contrib

CPPFLAGS+=-I${SCMCOORD_CONTRIB}/include
LDLIBS+=-L${SCMCOORD_CONTRIB}/lib

ifdef USE_VOCABULARY_FILTERS
SRCS+=vocabulary_filters.c
USE_VOCABULARY=yes
VPATH:=${VOCABULARYSRCDIR}:${VPATH}
endif

ifdef USE_VOCABULARY
SRCS+=vocabulary.c
USE_BCONF=YES
USE_VTREE=yes
USE_BCONF_VTREE=yes
USE_HASH=yes
USE_BURSTTRIE=yes
USE_TEXTFILTER=yes
USE_SUMTREE=yes
VPATH:=${VOCABULARYSRCDIR}:${VPATH}
CPPFLAGS+=-I${VOCABULARYSRCDIR}
endif

ifdef USE_TEXTFILTER
SRCS+=textfilter.c
VPATH:=${VOCABULARYSRCDIR}:${VPATH}
CPPFLAGS+=-I${VOCABULARYSRCDIR}
endif

ifdef USE_SUMTREE
SRCS+=sumtree.c
VPATH:=${VOCABULARYSRCDIR}:${VPATH}
CPPFLAGS+=-I${VOCABULARYSRCDIR}
endif
 
ifdef USE_BURSTTRIE
SRCS+=bursttrie.c
VPATH:=${VOCABULARYSRCDIR}:${VPATH}
USE_AVL=yes
CPPFLAGS+=-I${VOCABULARYSRCDIR}
endif

ifdef USE_BURSTTRIE_SERIALIZED
SRCS+=bursttrie_serialized.c levs.c
VPATH:=${VOCABULARYSRCDIR}:${VPATH}
USE_AVL=yes
USE_UNICODE=yes
CPPFLAGS+=-I${VOCABULARYSRCDIR}
endif

ifdef USE_LOOP_FILTERS
OBJDIRS+=loop
SRCS+=loop/loop.c loop/loopfilt.c
USE_CTEMPLATES = yes
endif

ifdef USE_VTREE_FILTERS
OBJDIRS+=vtree
SRCS+=vtree/settings.c vtree/with_vtree.c
USE_CTEMPLATES = yes
USE_SETTINGS = yes
endif

ifdef USE_XML_FILTERS
OBJDIRS+=xml
SRCS+=xml/xml_vtree.c
USE_CTEMPLATES = yes
LDLIBS+=-lexpat
endif

ifdef USE_TRANS_FILTERS
OBJDIRS+=trans
SRCS+=trans/trans.c trans/trans_conf.c trans/trans_filters.c
USE_CTEMPLATES = yes
USE_TRANSAPI = yes
USE_PATCHVARS = yes
endif

ifdef USE_SEARCH_FILTERS
OBJDIRS+=search
SRCS+=search/qs_sq.c search/search_search_lazy.c search/vsearch.c
USE_BSAPI = yes
USE_CTEMPLATES = yes
USE_QUERY_STRING = yes
USE_PATCHVARS = yes
USE_UNICODE = yes
endif

ifdef USE_HTML_FILTERS
OBJDIRS+=html
SRCS += html/available_weeks_calendar.c html/googleencode.c html/highlight.c html/referer_searchwords.c html/xiti_sanitize.c
USE_CTEMPLATES = yes
USE_PATCHVARS = yes
USE_QUERY_STRING = yes
USE_URL = yes
USE_UNICODE = yes
USE_STRL = yes
endif

ifdef USE_BLOCKET_FUNCTIONS
OBJDIRS+=blocket
SRCS+= blocket/build_version.c blocket/chk_err.c blocket/eat.c blocket/in_vtree_list.c blocket/linkshelf_split.c
SRCS+=blocket/log_string.c blocket/mime_inline_file.c blocket/nice_date.c blocket/statlog.c blocket/time_left.c blocket/translate.c
USE_BUILDVERSION = yes
USE_CTEMPLATES = yes
USE_LOGGING = yes
USE_LOG_EVENT = yes
endif

ifdef USE_CTEMPLATES
USE_QUERY_STRING := YES
USE_LOG_EVENT = yes
endif

ifdef USE_QUERY_STRING
USE_PCRE = yes
USE_UTIL = yes
USE_CTEMPLATES = yes
USE_SETTINGS = yes
USE_URL = yes
CPPFLAGS+=-I.
VPATH:=${TEMPLATESRCDIR} ${COMMONSRCDIR}:${VPATH}
SRCS+=parse_query_string.c query_string.c query_string_setting.gperf.enum query_string_value_key.gperf.enum query_string_lookup_key.gperf.enum
endif

ifdef USE_REDIS_FILTERS
OBJDIRS+=redis
SRCS+=redis/redis_del.c redis/redis_filters.h redis/redis_hgetall.c redis/redis_hset.c redis/redis_publish.c redis/redis_ttl.c
SRCS+=redis/redis_exists.c redis/redis_get.c redis/redis_hget.c redis/redis_incr.c redis/redis_rpop.c redis/redis_zadd.c redis/redis_exec.c
SRCS+=redis/redis_expire.c redis/redis_hdel.c redis/redis_hincrby.c redis/redis_keys.c redis/redis_session.c redis/redis_zrange_zrangebyscore_zrevrange.c
SRCS+=redis/redis_filters.c redis/redis_hexists.c redis/redis_hlen.c redis/redis_lpush.c redis/redis_set.c redis/redis_zscore.c
SRCS+=redis/redis_zrem.c redis/redis_zrank.c redis/redis_lrange.c redis/redis_sadd.c redis/redis_srem.c redis/redis_sismember.c
SRCS+=redis/redis_smembers.c
USE_HIREDIS = YES
endif

ifdef USE_JSON_FILTERS
OBJDIRS+=json
SRCS+=json/json_encode.c json/json_vtree.c
USE_JSON_VTREE=yes
endif

ifdef USE_MEMCACHE_FILTERS
OBJDIRS+=memcache
SRCS+=memcache/memcache_filters.c memcache/memcache_filters.h memcache/memcache_get.c
SRCS+=memcache/memcache_session.c memcache/memcache_set.c
USE_MCAPI=yes
endif

ifdef USE_PGSQL_FILTER
OBJDIRS+=pgsql
SRCS+=pgsql/pgsql.c pgsql/pgsql_escape.c pgsql/pgsql-pgsql_fetch-pgsql_fetch_row.c pgsql/pgsql_session.c
USE_STRL=yes
USE_POSTGRES=yes
endif

ifdef USE_CTEMPLATES
USE_STRL=yes
USE_AES = yes
USE_SHA1 = yes
USE_RAND = yes
#USE_TRANSAPI=yes
USE_ATOMIC=yes
USE_PCRE = yes
USE_BASE64 = yes
USE_BCONF = yes
USE_HASH = yes
USE_LRU = yes
USE_VTREE = yes
USE_BCONF_VTREE = yes
USE_SBO = yes
USE_CACHED_REGEX = yes
USE_AVL = yes
USE_UNICODE = yes
USE_STRL = yes
USE_JSON_VTREE = yes
USE_FILTER_STATE=yes
USE_FILTER_STATE_UCNV=yes
CPPFLAGS+= -DBUF_TEMPLATES
OBJDIRS+=tpfunctions
SRCS+=tpfunctions/clean_qs.c
SRCS+=tpfunctions/clean_url_qs.c
SRCS+=tpfunctions/define_list.c
SRCS+=tpfunctions/ifnull.c
SRCS+=tpfunctions/include.c
SRCS+=tpfunctions/pretty_number.c
SRCS+=tpfunctions/random_range.c tpfunctions/filter_state_random.c
SRCS+=tpfunctions/random_token.c
SRCS+=tpfunctions/strip_tags.c
SRCS+=tpfunctions/undef.c
SRCS+=ctemplates.c tpcapi.c bpapi.c
OBJDIRS+=tpfilters crypto
SRCS+=crypto/aes_cbc_decode.c
SRCS+=crypto/aes_cbc_encode.c
SRCS+=crypto/aes_decode.c
SRCS+=crypto/aes_encode.c
SRCS+=tpfilters/base64.c
SRCS+=tpfilters/base64_decode.c
SRCS+=tpfilters/cache_case.c
SRCS+=tpfilters/define.c
SRCS+=tpfilters/define_global.c
SRCS+=tpfilters/define_keyval.c
SRCS+=tpfilters/define_split.c
SRCS+=tpfilters/dump_timers.c
SRCS+=tpfilters/eat.c
SRCS+=tpfilters/float_precision.c
SRCS+=tpfilters/gzencode.c
SRCS+=tpfilters/htmlentities.c
SRCS+=tpfilters/htmlentities_decode.c
SRCS+=tpfilters/include.c
SRCS+=tpfilters/join.c
SRCS+=tpfilters/log_string.c
SRCS+=tpfilters/lower.c
SRCS+=tpfilters/lower_utf8.c
SRCS+=tpfilters/nice_date.c
SRCS+=tpfilters/nice_time.c
SRCS+=tpfilters/notempty.c
SRCS+=tpfilters/qp.c
SRCS+=tpfilters/qp_mail.c
SRCS+=tpfilters/regex_replace.c
SRCS+=tpfilters/remove.c
SRCS+=tpfilters/replace.c
SRCS+=tpfilters/rfc822_date.c
SRCS+=tpfilters/rot13.c
SRCS+=tpfilters/sbo.c
SRCS+=crypto/sha1_hex.c
SRCS+=tpfilters/sort_and_split.c
SRCS+=tpfilters/sort.c
SRCS+=tpfilters/str_repeat.c
SRCS+=tpfilters/tr.c
SRCS+=tpfilters/trim.c
SRCS+=tpfilters/trunc.c
SRCS+=tpfilters/trunc_utf8.c
SRCS+=tpfilters/trunc_utf8_with_breakchar.c
SRCS+=tpfilters/trunc_with_breakchar.c
SRCS+=tpfilters/tr_utf8.c
SRCS+=tpfilters/ucfirst.c
SRCS+=tpfilters/ucfirst_utf8.c
SRCS+=tpfilters/upper.c
SRCS+=tpfilters/upper_utf8.c
SRCS+=tpfilters/urlencode.c
SRCS+=tpfilters/urlencode_utf8.c
SRCS+=tpfilters/varblock.c
SRCS+=tpfilters/weeks_calendar.c
SRCS+=tpfilters/wrap.c
SRCS+=tpfilters/wrap_html.c
SRCS+=tpfilters/wrap_html_utf8.c
SRCS+=tpfilters/wrap_utf8.c
SRCS+=patchvars.c
ifdef USE_PGSQL_TEMPLATES
SRCS+=ctemplates_pg.c
endif
OBJDIRS+=datetime encoding js math string
SRCS+=datetime/define_month_days.c datetime/format_date.c datetime/format_ts.c datetime/now.c
SRCS+=datetime/timediff.c datetime/timestamp.c datetime/weekday.c
SRCS+=encoding/latin1_to_utf8.c encoding/utf8_to_latin1.c
SRCS+=js/js_encode.c js/vtree_js.c
SRCS+=math/add.c math/divide.c math/fcmp.c math/fmultiply.c math/maxint.c math/minint.c
SRCS+=math/modulo.c math/multiply.c math/percent.c math/sum_array.c
SRCS+=string/concat.c string/replace.c string/strcaseeql.c string/strcount.c string/str_in.c
SRCS+=string/str_len_utf8.c string/strpos.c string/strstrptrs.c string/numpad.c string/slice.c
SRCS+=string/strcmp.c string/streql.c string/str_len.c string/strpad.c string/strrpos.c string/substring.c

TMPLROOT?=${TOPDIR}/templates

CPPFLAGS+=-I${TEMPLATESRCDIR}
LDLIBS+=-rdynamic -Wl,-whole-archive $(foreach dir,${USE_TEMPLATES},$(subst ${TOPDIR},${TOPDIR}/${BUILDPATH},$(if $(findstring /,${dir}),,${TMPLROOT}/)${dir})/libtemplates.a) -Wl,-no-whole-archive -lm -ldl -lz

VPATH:=${COMMONSRCDIR}:${TEMPLATESRCDIR}:${VPATH}
SRCDIR?=.

ifndef TPC
include ${PLATFORMDIR}/mk/compile.mk
endif

ifeq ($(abspath ${TMPLROOT}),${TMPLROOT})
EXTRADEPEND+=$(foreach dir,${USE_TEMPLATES},$(subst ${TOPDIR},${TOPDIR}/${BUILDPATH},$(if $(findstring /,${dir}),,${TMPLROOT}/)${dir})/libtemplates.a)
define template_rule
$(subst ${TOPDIR},${TOPDIR}/${BUILDPATH},$(if $(findstring /,${tmplpath}),,${TMPLROOT}/)${tmplpath})/libtemplates.a: $(shell find $(if $(findstring /,${tmplpath}),,${TMPLROOT}/)${tmplpath} -type f -print -o -name .svn -prune) ${TPC}
	@echo " ** Compiling templates ROOT: '$(if $(findstring /,${tmplpath}),,${TMPLROOT}/)' TEMPDIR '${tmplpath}':"
	+exec ${MAKE} --no-print-directory -C $(if $(findstring /,${tmplpath}),,${TMPLROOT}/)${tmplpath} all
endef
else
EXTRADEPEND+=$(foreach dir,${USE_TEMPLATES},$(subst ${TOPDIR},${TOPDIR}/${BUILDPATH},$(if $(findstring /,${dir}),,$(abspath ${SRCDIR}/${TMPLROOT})/)${dir})/libtemplates.a)
define template_rule
$(subst ${TOPDIR},${TOPDIR}/${BUILDPATH},$(if $(findstring /,${tmplpath}),,$(abspath ${SRCDIR}/${TMPLROOT})/)${tmplpath})/libtemplates.a: $(shell find ${SRCDIR}/${TMPLROOT}/${tmplpath} -type f -print -o -name .svn -prune) ${TPC}
	+exec ${MAKE} --no-print-directory -C $(if $(findstring /,${tmplpath}),${tmplpath},${SRCDIR}/)${TMPLROOT}/${tmplpath} all
endef
endif


$(foreach tmplpath,${USE_TEMPLATES},$(eval ${template_rule}))

TMPLFORCE:

ifndef USE_TEMPLATES
CPPFLAGS+=-DTPAPI_NO_TEMPLATES
endif
endif

ifdef USE_CACHEAPI
SRCS+=cacheapi.c
VPATH:=${COMMONSRCDIR}:${VPATH}
USE_LOGGING=yes
USE_HIREDIS=yes
USE_MCAPI=yes
endif

ifdef USE_HIREDIS
LDLIBS+=-lhiredis
CPPFLAGS+=-I${PLATFORMDIR}/lib/common
VPATH:=${PLATFORMDIR}/lib/common:${VPATH}
SRCS+=sredisapi.c
USE_FD_POOL=yes
USE_LOGGING=yes
USE_BCONF_VTREE = yes
endif

ifdef USE_MCAPI
SRCS+=memcacheapi.c
VPATH:=${COMMONSRCDIR}:${VPATH}
USE_BCONF_VTREE=yes
USE_FDGETS=yes
endif

ifdef USE_SETTINGS
SRCS+=settings.c
USE_VTREE = yes
VPATH:=${COMMONSRCDIR}:${VPATH}
endif

ifdef USE_CONFIG
SRCS+=config.c bconfd_client.c
LDLIBS+=-lcurl
VPATH:=${COMMONSRCDIR}:${VPATH}
USE_BCONF = yes
USE_VTREE = yes
USE_TRANSAPI = yes
USE_BCONF_VTREE = yes
endif

ifdef USE_SBO
SRCS+=sbo.c
VPATH:=${COMMONSRCDIR}:${VPATH}
endif

ifdef USE_SIMILAR_TEXT
SRCS+=similar_text.c
VPATH:=${COMMONSRCDIR}:${VPATH}
LDLIBS+=-lm
endif

ifdef USE_LEVENSHTEIN
SRCS+=levenshtein.c
VPATH:=${COMMONSRCDIR}:${VPATH}
endif

ifdef USE_TRANSAPI
SRCS+=transapi.c
VPATH:=${COMMONSRCDIR}:${VPATH}
USE_UTIL=yes
USE_HASH=yes
USE_LOGGING=yes
USE_VTREE=yes
USE_FD_POOL=yes
endif

ifdef USE_BSAPI_ASYNC
SRCS+=bsapi_async.c
VPATH:=${COMMONSRCDIR}:${VPATH}
USE_BSAPI=yes
endif

ifdef USE_BSAPI
USE_LOGGING=yes
USE_VTREE=yes
USE_UTIL=yes
USE_FD_POOL=yes
USE_ATOMIC=yes
SRCS+=bsapi.c bsapi_object.c
VPATH:=${COMMONSRCDIR}:${VPATH}
endif

ifdef USE_FD_POOL
USE_SBALANCE=yes
USE_STRL=yes
USE_VTREE=yes
USE_LOGGING=yes
USE_UTIL=yes
USE_NETWORK=yes
SRCS+=fd_pool.c
VPATH:=${COMMONSRCDIR}:${VPATH}
endif

ifdef USE_NETWORK
SRCS+=network.c
VPATH:=${COMMONSRCDIR}:${VPATH}
endif

ifdef USE_SBALANCE
USE_RAND=yes
SRCS+=sbalance.c rcycle.c
VPATH:=${COMMONSRCDIR}:${VPATH}
endif

ifdef USE_DAEMON
SRCS+=daemon.c
VPATH:=${COMMONSRCDIR}:${VPATH}
USE_UTIL = YES
USE_LOGGING = yes
endif

ifdef USE_PS_STATUS
VPATH:=${COMMONSRCDIR}:${VPATH}
SRCS+=ps_status.c
USE_UTIL = YES
endif

ifdef USE_SEARCH_INDEX
USE_STRL=yes
USE_SHA1=yes
SRCS+=index.c
VPATH:=${SEARCHSRCDIR}:${VPATH}
USE_TOKYO=yes
USE_UTIL=yes
USE_LOGGING=yes
USE_AVL=yes
USE_JSON_VTREE=yes
CPPFLAGS+=-I${SEARCHSRCDIR}
endif

ifdef USE_JSON_VTREE
SRCS+=json_vtree.c
VPATH:=${COMMONSRCDIR}:${VPATH}
USE_BCONF_VTREE=yes
USE_YAJL=yes
endif

ifdef USE_YAJL
LDLIBS+=-lyajl
endif

ifdef USE_BCONF_VTREE
SRCS+=bconf_vtree.c
USE_BCONF = yes
USE_VTREE = yes
VPATH:=${COMMONSRCDIR}:${VPATH}
endif

ifdef USE_VTREE
SRCS+=vtree.c
CPPFLAGS+=-I${TEMPLATESRCDIR}
VPATH:=${TEMPLATESRCDIR}:${VPATH}
endif

ifdef USE_SEARCH_TEXT
SRCS+=text.c synize.c
VPATH:=${SEARCHSRCDIR}:${VPATH}
USE_HUNSPELL=yes
USE_CHARMAP=yes
USE_LANGMAP=yes
USE_BCONF=YES
CPPFLAGS+=-I${SEARCHSRCDIR}
endif

ifdef USE_LANGMAP
USE_TOKYO=yes
SRCS+=langmap.c
CPPFLAGS+=-I${SEARCHSRCDIR}
VPATH:=${SEARCHSRCDIR}:${VPATH}
endif

ifdef USE_CHARMAP
SRCS+=charmap.c
USE_UTIL=yes
VPATH:=${SEARCHSRCDIR}:${VPATH}
CPPFLAGS+=-I${SEARCHSRCDIR}
LDLIBS+=-licui18n -licuuc -licudata -lpthread -lm
endif

ifdef USE_LOGGING
SRCS+=logging.c
USE_UTIL = yes
VPATH:=${COMMONSRCDIR}:${VPATH}
LDLIBS+=-lrt -ldl
endif

ifdef USE_LOG_EVENT
SRCS+=log_event.c
USE_BCONF = yes
USE_UTIL = yes
USE_STRL = yes
VPATH:=${COMMONSRCDIR}:${VPATH}
endif

ifdef USE_HTTP_REQ
SRCS+=http_req.c
VPATH:=${COMMONSRCDIR}:${VPATH}
USE_TLS=yes
USE_LIBEVENT=yes
USE_UTIL=yes
LDLIBS+=-lssl -lcrypto
endif

ifdef USE_TLS
SRCS+=tls.c
VPATH:=${COMMONSRCDIR}:${VPATH}
endif

ifdef USE_RAND
SRCS+=rand.c
VPATH:=${COMMONSRCDIR}:${VPATH}
endif

ifdef USE_AES
SRCS+=aes.c
VPATH:=${COMMONSRCDIR}:${VPATH}
LDLIBS+=-lcrypto
USE_UTIL = yes
USE_STRL = yes
endif

ifdef USE_LOCATION
SRCS+=location.c
VPATH:=${COMMONSRCDIR}:${VPATH}
LDLIBS+=-lm
endif

ifdef USE_LRU
SRCS += lru.c 
USE_UTIL = yes
USE_SPINLOCK = yes
endif

ifdef USE_THREADS
CPPFLAGS+=-D_REENTRANT
LDLIBS+=-lpthread
endif

ifdef USE_HUNSPELL
LDLIBS+=`${PLATFORMDIR}/scripts/build/hunspell-libs.sh` -lstdc++
SRCS+=hunstem.cc 
VPATH:=${SEARCHSRCDIR}:${VPATH}
endif

ifdef USE_BCONF
SRCS+=bconf.c
VPATH:=${COMMONSRCDIR}:${VPATH}
USE_UTIL = yes
USE_MEMPOOL = yes
USE_AVL = yes
endif

ifdef USE_AVL
SRCS+=subr_avl.c
VPATH:=${COMMONSRCDIR}:${VPATH}
endif

ifdef USE_UNICODE
SRCS+=unicode.c
USE_UTIL=yes
VPATH:=${COMMONSRCDIR}:${VPATH}
LDLIBS+=-licui18n -licuuc -licudata -lpthread -lm
endif

ifdef USE_URL
SRCS+=url.c
USE_UTIL=yes
USE_CACHED_REGEX=yes
VPATH:=${COMMONSRCDIR}:${VPATH}
endif

ifdef USE_CACHED_REGEX
SRCS+=cached_regex.c
USE_ATOMIC = yes
USE_PCRE = yes
USE_UTIL = yes
VPATH:=${COMMONSRCDIR}:${VPATH}
endif

ifdef USE_PCRE
LDLIBS+=-lpcre
endif

ifdef USE_FDGETS
SRCS+=fdgets.c
VPATH:=${COMMONSRCDIR}:${VPATH}
endif

ifdef USE_UTIL
SRCS+=util.c timer.c buf_string.c string_functions.c date_functions.c memalloc_functions.c error_functions.c
USE_STRL=yes
VPATH:=${COMMONSRCDIR}:${VPATH}
LDLIBS+=-lrt
endif

ifdef USE_COMPCONF
SRCS+=compconf.c
VPATH:=${COMMONSRCDIR}:${VPATH}
endif

ifdef USE_HASH
SRCS+=hash.c
VPATH:=${COMMONSRCDIR}:${VPATH}
endif

ifdef USE_TOKYO
LDLIBS+=-lz -lpthread -lm -ltokyocabinet
endif

ifdef USE_SPINLOCK
USE_ATOMIC = yes
endif

ifdef USE_ATOMIC
CPPFLAGS+=-I${COMMONSRCDIR}/${BUILD_ARCH}
endif

ifdef USE_TCMALLOC
ifdef TCMALLOC_PATH
LDLIBS+=-L${TCMALLOC_PATH} -ltcmalloc -lstdc++
else
$(error USE_TCMALLOC set but no TCMALLOC_PATH)
endif
endif

ifdef USE_SHA1
SRCS+=sha1.c sha1hl.c
VPATH:=${COMMONSRCDIR}:${VPATH}
endif

ifdef USE_BASE64
SRCS+=base64.c
VPATH:=${COMMONSRCDIR}:${VPATH}
endif

ifdef USE_APACHE
SRCS+=cookies.c accept_lang.c remote_addr.c
VPATH:=${APACHESRCDIR}:${VPATH}
CPPFLAGS+=-I${APACHESRCDIR} -I${APACHEINCDIR}
USE_STRL=yes
endif

ifdef USE_LIBXML2
CPPFLAGS+= -I/usr/include/libxml2
LDLIBS+= -lxml2
endif

ifdef USE_MOD_BCONF
CPPFLAGS+=-I${PLATFORMDIR}/modules/mod_bconf
endif

ifdef USE_MEMPOOL
SRCS+=mempool.c
VPATH:=${COMMONSRCDIR}:${VPATH}
endif

ifdef USE_BUILDVERSION
BEFOREDEPEND+=build_version.h
CPPFLAGS+=-I.
build_version.h:
	echo "#define BUILD_VERSION \""`(cd ${PLATFORMDIR} ; sh ${PLATFORMDIR}/scripts/build/buildversion.sh)`"\"" > build_version.h || (rm build_version.h ; exit 1)
endif

ifdef USE_POSTGRES
ifdef POSTGRES_PATH
CPPFLAGS += -I`${POSTGRES_PATH}/bin/pg_config --includedir`
LDLIBS += -L`${POSTGRES_PATH}/bin/pg_config --libdir` -lpq
else
CPPFLAGS += -I`pg_config --includedir`
LDLIBS += -L`pg_config --libdir` -lpq
endif
endif

ifdef USE_STRL
SRCS+=strlcpy.c strlcat.c
VPATH:=${COMMONSRCDIR}:${VPATH}
endif

ifdef USE_FILTER_STATE
SRCS+=filter_state.c
VPATH:=${TEMPLATESRCDIR}:${VPATH}
CPPFLAGS+=-I${TEMPLATESRCDIR}
ifdef USE_FILTER_STATE_UCNV
SRCS+=filter_state_ucnv.c
endif
ifdef USE_SEARCH_FILTERS
SRCS+=search/filter_state_bsconf.c
endif
endif

endif # COMMONSRC_I
