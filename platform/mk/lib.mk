USE_BUILD?=yes

PLATFORMDIR?=${TOPDIR}/platform

include ${PLATFORMDIR}/mk/all.mk
include ${PLATFORMDIR}/mk/commonsrc.mk

ifneq ($(USE_BUILD),yes)
ifeq (${BUILD_ARCH},x86_64)
LIBPIC=-fPIC
endif

ifdef USE_PHP
PHP_CONFIG?=php-config
CPPFLAGS+=$(shell ${PHP_CONFIG} --includes) -DCOMPILE_DL=1
CONLYWARN+=-Wno-old-style-definition
endif

ifdef USE_APACHE
CPPFLAGS+=-I $(shell ${APXS} -q INCLUDEDIR) -I $(shell ${APXS} -q APR_INCLUDEDIR)
endif

ifndef LIB_STATIC
LIB_DYNAMIC=1
LIBNAME=${LIB}.so
LDFLAGS?=-rdynamic
else
LIBNAME=${LIB}.a
endif

INSTALL_TARGETS+=${LIBNAME}

CLEANFILES+=${LIBNAME}

ifdef LIB
all: ${LIBNAME}
endif

include ${PLATFORMDIR}/mk/compile.mk
include ${PLATFORMDIR}/mk/clean.mk

${LIB}.so: ${PREOBJDEPEND} ${HDRS} ${PICOBJS} ${EXTRADEPEND}
	${LD} ${LDFLAGS} ${PICFLAGS} -o ${LIBNAME} ${PICOBJS} ${LDLIBS} ${LDSCRIPTS}

${LIB}.a: ${PREOBJDEPEND} ${HDRS} ${OBJS} ${EXTRADEPEND}
ifdef RENAME_OBJS
	for o in ${OBJS}; do case $$o in ./*/* | [^.]*/*) cp $$o  `echo $$o | sed 's,^\./,,' | sed 's,/,__,g' ` ;; esac; done
	ar cru ${LIB}.a ${subst /,__,${subst ./,,${OBJS}}}
	for o in ${OBJS}; do case $$o in ./*/* | [^.]*/*) rm `echo $$o | sed 's,^\./,,' | sed 's,/,__,g' ` ;; esac; done
else
	ar cru ${LIB}.a ${OBJS}
endif
	ranlib ${LIB}.a

ifdef LIB
include ${PLATFORMDIR}/mk/depend.mk
include ${PLATFORMDIR}/mk/in.mk
include ${PLATFORMDIR}/mk/install.mk
endif
endif
