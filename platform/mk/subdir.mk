PLATFORMDIR?=${TOPDIR}/platform

include ${PLATFORMDIR}/mk/all.mk

ifndef SUBDIR_I
SUBDIR_I:=1
_REGRESS_DEFINED=yes
CL_I=yes

TOPDIR:=$(abspath ${TOPDIR})

${DEFAULT_TARGETS}:
ifdef LOCALFIRST
	+if [ -n "$$(find . -maxdepth 1 -name 'local*.mk')" ]; then \
		for mf in local*.mk; do \
			printf "\nMaking \033[34m$@\033[0m in \033[36m${subst ${TOPDIR},,${CURDIR}}/$$mf\033[0m\n"; \
			${MAKE} $@ DESTDIR=${DESTDIR} TOPDIR=${TOPDIR} INSTALLROOT=${INSTALLROOT} STAMPFILE=${STAMPFILE} -f $$mf || exit 1; \
		done; \
	fi || exit 1 
endif
	+for DIR in ${SUBDIR} ; do \
		printf "\nMaking \033[34m$@\033[0m in \033[36m${subst ${TOPDIR},,${CURDIR}}/$$DIR\033[0m\n"; \
		${MAKE} -C $$DIR $@ DESTDIR=${DESTDIR} TOPDIR=${TOPDIR} INSTALLROOT=${INSTALLROOT} STAMPFILE=${STAMPFILE} || exit 1 ; \
	done
ifndef LOCALFIRST
	+if [ -n "$$(find . -maxdepth 1 -name 'local*.mk')" ]; then \
		for mf in local*.mk; do \
			printf "\nMaking \033[34m$@\033[0m in \033[36m${subst ${TOPDIR},,${CURDIR}}/$$mf\033[0m\n"; \
			${MAKE} $@ DESTDIR=${DESTDIR} TOPDIR=${TOPDIR} INSTALLROOT=${INSTALLROOT} STAMPFILE=${STAMPFILE} -f $$mf || exit 1; \
		done; \
	fi || exit 1 
endif
	if [ "$@" = cleandir -a -n "${EXTRACLEAN}${CLEANFILES}" ]; then \
		rm -rf ${EXTRACLEAN} ${CLEANFILES}; \
	fi || exit 1
ifeq (${DOSTAMP},yes)
	if [ "$@" = install ]; then \
		touch /dev/null ${STAMPFILE}; \
	fi || false
endif

regress-do:
	for DIR in ${SUBDIR} ; do \
		${MAKE} -C $$DIR regress DESTDIR=${DESTDIR} TOPDIR=${TOPDIR} STAMPFILE=${STAMPFILE} || exit 1 ; \
	done
	if [ -f local.mk ]; then exec ${MAKE} regress DESTDIR=${DESTDIR} TOPDIR=${TOPDIR} STAMPFILE=${STAMPFILE} -f local.mk; fi || exit 1 

.PHONY: regress-init regress-initclean regress-do regress

ifdef REGRESS_INIT
regress-init:
	for t in ${REGRESS_INIT} ;  do ${MAKE} $$t || exit 1; done
else
regress-init:
endif

ifdef REGRESS_FINI
regress-fini:
	for t in ${REGRESS_FINI} ; do ${MAKE} $$t ; done
else
regress-fini:
endif

regress:
	${MAKE} regress-init
	if ${MAKE} regress-do ; then ${MAKE} regress-fini ; else ${MAKE} regress-fini ; exit 1 ; fi

endif
