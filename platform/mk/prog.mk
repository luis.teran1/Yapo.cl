USE_BUILD?=yes

PLATFORMDIR?=${TOPDIR}/platform

include ${PLATFORMDIR}/mk/all.mk
include ${PLATFORMDIR}/mk/commonsrc.mk

ifneq ($(USE_BUILD),yes)
ifdef USE_LIBEVENT
LDLIBS+=-levent -lrt
SRCS+=eventextra.c
VPATH+=${COMMONSRCDIR}
endif

LDFLAGS?=-rdynamic
LDOPTS?=-g

CLEANFILES+=${PROG}

INSTALL_TARGETS+=${PROG}

all: ${PROG}

include ${PLATFORMDIR}/mk/compile.mk
include ${PLATFORMDIR}/mk/clean.mk

${PROG}: ${PREOBJDEPEND} ${HDRS} ${OBJS} ${EXTRADEPEND}
	${LD} ${LDFLAGS} ${LDOPTS} -o ${PROG} ${OBJS} ${LDLIBS} ${LDSCRIPTS}


include ${PLATFORMDIR}/mk/depend.mk
include ${PLATFORMDIR}/mk/in.mk
include ${PLATFORMDIR}/mk/install.mk
endif
