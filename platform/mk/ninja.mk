ALL_I:=1

SHELL=bash

ninjabuild:
	(cd ${TOPDIR} && ./compile.sh)
all depend install:: ninjabuild

RDDEP=ninjabuild

.PHONY: ninjabuild

# Determines build root, path to binaries.
FLAVOR?=regress
BUILDPATH?=build

-include ${TOPDIR}/${BUILDPATH}/obj/${FLAVOR}/tools/in.conf

DEST=${TOPDIR}/${BUILDPATH}/${FLAVOR}
DEST_BIN=${DEST}/bin

SYSLOGHOOKSO=${DEST}/modules/sysloghook.so
SYSLOGHOOK=env SYSLOGROOT=${SYSLOGROOT} LD_PRELOAD="${DEST}/modules/sysloghook.so"
REGRESS_LOG?=/tmp/${USER}-regress.log
# OBJDIR is only supposed to be used by regress tests that need to write.
RELDIR:=$(patsubst $(abspath ${TOPDIR})/%,%,$(shell pwd))
BUILDROOT=${TOPDIR}/${BUILDPATH}/obj/${FLAVOR}
OBJDIR=${BUILDROOT}/${RELDIR}

