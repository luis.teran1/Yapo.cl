DEPSRCS=${SRCS_C} ${SRCS_CC} ${SRCS_CXX} ${SRCS_YY:.yy=.cc} ${SRCS_LL:.ll=.cc} \
	${SRCS_Y:.y=.c} ${SRCS_L:.l=.c} ${SRCS_TMPL:.tmpl=.c}

ifeq "${SRCS_LL}${SRCS_YY}${SRCS_CC}${SRCS_CXX}" ""
DEPCC=${CC}
DEPFLAGS=${CFLAGS}
else
DEPCC=${CXX}
DEPFLAGS=${CXXFLAGS}
endif

# Add BEFOREDEPEND directly as a depend, makes sure depend is not broaken when running -jX parallel make.
depend: ${DEPSRCS} ${HDRS} ${BEFOREDEPEND}
	${DEPCC} ${DEPFLAGS} -M $(filter %.c, $^) $(filter %.cc, $^) $(filter %.cxx, $^) $(filter %.h, $^)  > .tmp.depend${LOCAL}.$$$$ && \
	mv -f .tmp.depend${LOCAL}.$$$$ .depend${LOCAL}
ifdef LIB_DYNAMIC
	sed -i s/\.o:/.soo:/ .depend${LOCAL}
endif

-include .depend${LOCAL}
