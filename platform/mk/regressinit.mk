include ${PLATFORMDIR}/mk/all.mk

LOCK=cd ${TOPDIR}; ${PLATFORMDIR}/regress/lock ${GENPORTOFF}
UNLOCK=cd ${TOPDIR}; ${PLATFORMDIR}/regress/unlock ${GENPORTOFF}

bconfd-regress-start:
	@echo -e "\\033[1;35mStarting bconfd\\033[39;0m"
	(${LOCK} bconfd) && (${SYSLOGHOOK} ${CMD_PREFIX} ${DEST_BIN}/bconfd --quick-start --config ${DEST}/regress/bconfd/bconfd.conf --pidfile ${TOPDIR}/.bconfd.pid &)
	while ! printf "cmd:bconf\ncommit:1\nend\n" | ${NETCAT} localhost ${REGRESS_BCONFD_TRANS_PORT} | grep TRANS_OK > /dev/null ; do sleep 0.1 ; done ; true

bconfd-regress-start-%:
	echo -e "\\033[1;35mStarting bconfd\\033[39;0m"
	(${LOCK} bconfd) && (${SYSLOGHOOK} ${CMD_PREFIX} ${DEST_BIN}/bconfd --quick-start --config ${DEST}/regress/bconfd/bconfd.conf --bconf ${DEST}/$(subst --,/,$*) --pidfile ${TOPDIR}/.bconfd.pid)
	while ! printf "cmd:bconf\ncommit:1\nend\n" | ${NETCAT} localhost ${REGRESS_BCONFD_TRANS_PORT} | grep TRANS_OK > /dev/null ; do sleep 0.1 ; done ; true

bconfd-regress-config-start-%:
	echo -e "\\033[1;35mStarting bconfd\\033[39;0m"
	(${LOCK} bconfd) && (${SYSLOGHOOK} ${CMD_PREFIX} ${DEST_BIN}/bconfd --quick-start --config ${DEST}/$(subst --,/,$*) --pidfile ${TOPDIR}/.bconfd.pid)
	while ! printf "cmd:bconf\ncommit:1\nend\n" | ${NETCAT} localhost ${REGRESS_BCONFD_TRANS_PORT} | grep TRANS_OK > /dev/null ; do sleep 0.1 ; done ; true

bconfd-regress-stop:
	@echo -e "\\033[1;35mStopping bconfd\\033[39;0m"
	if (${UNLOCK} bconfd); then \
		kill `cat ${TOPDIR}/.bconfd.pid` ; \
		rm ${TOPDIR}/.bconfd.pid ; \
		while ${LSOF} -nP -u $$(id -u) | fgrep -q 'TCP *:${REGRESS_BCONFD_TRANS_PORT}' ; do sleep 0.1 ; done ; \
	fi ; true

bconfd-trans-regress-reload:
	@test "$$(curl -s --write-out "%{http_code}" http://${REGRESS_HOSTNAME}:${REGRESS_BCONFD_CONTROLLER_PORT}/reload -o /dev/null)" = 200; \
	printf 'cmd:control\naction:reload\ncommit:1\nend\n' | ${NETCAT} localhost ${REGRESS_TRANS_CPORT} | fgrep TRANS_OK

pgsql-platform-build-start:
	@echo -e "\\033[1;35mStarting postgres\\033[39;0m"
	@if [ ! -d ${REGRESS_PGSQL_HOST} -a ! -w ${REGRESS_PGSQL_HOST} ] ; then \
		mkdir -p ${REGRESS_PGSQL_HOST} ; \
	fi
	@if (${LOCK} pgsql_build); then \
		(cd ${PLATFORMDIR}/scripts/db && exec env BPVSCHEMA=${BPVSCHEMA} TOPDIR=${TOPDIR} PLATFORMDIR=${PLATFORMDIR} DEST_BIN=${DEST_BIN} DEST=${DEST} ./start-build-pgsql.sh ${REGRESS_PGSQL_HOST} ${PGVERSION}) || exit 1; \
	else \
		true; \
	fi

pgsql-platform-build-start-%:
	@echo -e "\\033[1;35mStarting postgres\\033[39;0m"
	@if [ ! -d ${REGRESS_PGSQL_HOST} -a ! -w ${REGRESS_PGSQL_HOST} ] ; then \
		mkdir -p ${REGRESS_PGSQL_HOST} ; \
	fi
	@if (${LOCK} pgsql_build); then \
		(cd ${PLATFORMDIR}/scripts/db && exec env BPVSCHEMA=${BPVSCHEMA} TOPDIR=${TOPDIR} PLATFORMDIR=${PLATFORMDIR} DEST_BIN=${DEST_BIN} DEST=${DEST} ./start-build-pgsql.sh ${REGRESS_PGSQL_HOST} ${PGVERSION} $* ) || exit 1; \
	else \
		true; \
	fi

pgsql-platform-build-stop: 
	@echo -e "\\033[1;35mStopping postgres\\033[39;0m"
	@if (${UNLOCK} pgsql_build); then \
		(cd ${PLATFORMDIR}/scripts/db && exec env TOPDIR=${TOPDIR} ./stop-build-pgsql.sh ${REGRESS_PGSQL_HOST}); \
		rm -rf ${REGRESS_PGSQL_HOST}; \
	else \
		true; \
	fi

pgsql-platform-replica-build-start-%:
	@echo -e "\\033[1;35mStarting postgres\\033[39;0m"
	@if [ ! -d ${REGRESS_PGSQL_HOST} -a ! -w ${REGRESS_PGSQL_HOST} ] ; then \
		mkdir -p ${REGRESS_PGSQL_HOST} ; \
	fi
	@if (${LOCK} pgsql_build); then \
		(cd ${PLATFORMDIR}/scripts/db && exec env BPVSCHEMA=${BPVSCHEMA} TOPDIR=${TOPDIR} PLATFORMDIR=${PLATFORMDIR} DEST_BIN=${DEST_BIN} ./start-build-pgsql2.sh ${REGRESS_PGSQL_HOST} ${REGRESS_PGSQL_REPLICA_HOST} $*) || exit 1; \
	else \
		true; \
	fi

pgsql-platform-replica-build-stop: 
	@echo -e "\\033[1;35mStopping postgres\\033[39;0m"
	if (${UNLOCK} pgsql_build); then \
		(cd ${PLATFORMDIR}/scripts/db && env TOPDIR=${TOPDIR} ./stop-build-pgsql.sh ${REGRESS_PGSQL_HOST} && env TOPDIR=${TOPDIR} ./stop-build-pgsql.sh ${REGRESS_PGSQL_REPLICA_HOST} ${TOPDIR}/.postgres_replica.pid ${TOPDIR}/.pgsql_replica.log); \
		rm -rf ${REGRESS_PGSQL_HOST}; \
		rm -rf ${REGRESS_PGSQL_REPLICA_HOST}; \
	else \
		true; \
	fi

rabbitmq-regress-start:
	@echo -e "\\033[1;35mStarting rabbitmq\\033[39;0m"
	@if (${LOCK} rabbitmq_build); then \
		${DEST_BIN}/regress-rabbitmq rabbitmq-server &>/dev/null & \
		while ! ${NETCAT} localhost ${REGRESS_RABBITMQ_PORT} < /dev/null > /dev/null && ((try++ < 50)); do echo -n "."; sleep 1; done ; \
		if ((try == 51)); then \
			echo -e "\nERROR: RabbitMQ non-responsive, stopping."; \
			exit 1; \
		fi; \
		echo; \
	else \
		echo -e "Could not start RabbitMQ server"; \
		exit 1; \
	fi

rabbitmq-regress-stop:
	@echo -e "\\033[1;35mStopping rabbitmq\\033[39;0m"
	@if (${UNLOCK} rabbitmq_build); then \
		${DEST_BIN}/regress-rabbitmq rabbitmqctl stop; \
		while kill -0 $$(< ${REGRESS_DIR}/.rabbitpid) 2> /dev/null; do echo -n . ; sleep 0.5 ; done; \
		echo; \
		pkill -u ${USER} epmd; \
	else \
		true; \
	fi

.PHONY: db-snap-%
db-snap-%:
	printf "cmd:db_snap\naction:snap\nname:$(subst db-snap-,,$@)\ncommit:1\nend\n" | ${NETCAT} localhost ${REGRESS_TRANS_PORT} | fgrep TRANS_OK

.PHONY: db-store-%
db-store-%:
	printf "cmd:db_snap\naction:store\nname:$(subst db-store-,,$@)\ncommit:1\nend\n" | ${NETCAT} localhost ${REGRESS_TRANS_PORT} | fgrep TRANS_OK

.PHONY: db-restore-%
db-restore-%:
	printf "cmd:db_snap\naction:restore\nname:$(subst db-restore-,,$@)\ncommit:1\nend\n" | ${NETCAT} localhost ${REGRESS_TRANS_PORT} | fgrep TRANS_OK

REGRESS_REDIS_DIR=$(abspath ${DEST}/regress/redis)
REDIS?=redis-server
REDIS_CLI?=redis-cli

.PHONY: redis-platform-regress-start
redis-platform-regress-start:
	@echo -e "\\033[1;35mStarting redis\\033[39;0m"
	@if (${LOCK} redis); then \
		rm -r ${REGRESS_REDIS_DIR}/data ; \
		mkdir ${REGRESS_REDIS_DIR}/data ; \
		(setsid ${REDIS} ${REGRESS_REDIS_DIR}/conf/redis.conf); \
		while ! ${NETCAT} localhost ${REGRESS_REDIS_PORT} < /dev/null ; do sleep 0.1 ; done; true; \
	else \
		true; \
	fi

.PHONY: redis-platform-regress-stop
redis-platform-regress-stop:
	@echo -e "\\033[1;35mStopping redis\\033[39;0m"
	@if (${UNLOCK} redis); then \
		kill `cat ${REGRESS_REDIS_DIR}/data/redis.pid`; \
	else \
		true; \
	fi

# Usage: asearch-platform-regress-start-foo--bar--gazonksearch
# This will specify that the configuration directory is foo/bar and
# the config to use is gazonksearch.conf
#
# The only difference between asearch and csearch is the lock.
# It's up to you to generate the correct config files.
.PHONY: asearch-platform-regress-start-%
asearch-platform-regress-start-%:
	(${LOCK} asearch) && ${SYSLOGHOOK} env REGRESSDIR=$(dir $(subst --,/,$*)) REGRESSENGINE=$(notdir $(subst --,/,$*)) REGRESSSEARCH=${DEST_BIN}/search NETCAT="${NETCAT}" CMD_PREFIX="${CMD_PREFIX}" ${INIT_PREFIX} ${PLATFORMDIR}/scripts/init/search start
.PHONY: asearch-platform-regress-stop-ignore-error-%
asearch-platform-regress-stop-ignore-error-%:
	(${UNLOCK} asearch) && env REGRESSDIR=$(dir $(subst --,/,$*)) REGRESSENGINE=$(notdir $(subst --,/,$*)) NETCAT="${NETCAT}" ${INIT_PREFIX} ${PLATFORMDIR}/scripts/init/search stop; true
.PHONY: asearch-platform-regress-stop-%
asearch-platform-regress-stop-%:
	(${UNLOCK} asearch) && env REGRESSDIR=$(dir $(subst --,/,$*)) REGRESSENGINE=$(notdir $(subst --,/,$*)) NETCAT="${NETCAT}" ${INIT_PREFIX} ${PLATFORMDIR}/scripts/init/search stop

.PHONY: csearch-platform-regress-start-%
csearch-platform-regress-start-%:
	(${LOCK} csearch) && ${SYSLOGHOOK} env REGRESSDIR=$(dir $(subst --,/,$*)) REGRESSENGINE=$(notdir $(subst --,/,$*)) REGRESSSEARCH=${DEST_BIN}/search NETCAT="${NETCAT}" CMD_PREFIX="${CMD_PREFIX}" ${INIT_PREFIX} ${PLATFORMDIR}/scripts/init/search start
.PHONY: csearch-platform-regress-stop-%
csearch-platform-regress-stop-%:
	(${UNLOCK} csearch) && env REGRESSDIR=$(dir $(subst --,/,$*)) REGRESSENGINE=$(notdir $(subst --,/,$*)) ${INIT_PREFIX} NETCAT="${NETCAT}" ${PLATFORMDIR}/scripts/init/search stop

# Usage: apache-platform-regress-start-foo--bar--conf--httpd.conf
# this will specify that httpd.conf is in foo/bar/conf/httpd.conf
# Stop needs to be used with ServerRoot, so in most cases it would
# be: apache-platform-regress-stop-foo--bar to match the start above.
.PHONY: apache-platform-regress-start-%
apache-platform-regress-start-%:
	(${LOCK} apache) && (${SYSLOGHOOK} ${CMD_PREFIX} ${HTTPD} -D DSO_MPM_WORKER -D REGRESS -c "PidFile httpd.pid" -f $(subst --,/,$*))
	while ! ${LSOF} -nP -u $$(id -u) | fgrep -q 'TCP *:${REGRESS_HTTPD_WORKER_PORT}' ; do sleep 0.1 ; done ; true
.PHONY: apache-platform-regress-stop-%
apache-platform-regress-stop-%:
	(kill `cat $$(awk -F'"' '/ServerRoot/ { print $$2 }' $(subst --,/,$*))/httpd.pid`) ; (${UNLOCK} apache)
	while ${LSOF} -nP -u $$(id -u) | fgrep -q 'TCP *:${REGRESS_HTTPD_WORKER_PORT}' ; do sleep 0.1 ; done ; true

.PHONY: dav-platform-regress-start-%
dav-platform-regress-start-%:
	@mkdir -p ${DEST}/regress/mod_dav/upload
	(${LOCK} dav) && (${SYSLOGHOOK} ${HTTPD} -D DSO_MPM_PREFORK -D REGRESS -c "PidFile dav.pid" -f $(subst --,/,$*))
.PHONY: apache-platform-regress-stop-%
dav-platform-regress-stop-%:
	(kill `cat $$(awk -F'"' '/ServerRoot/ { print $$2 }' $(subst --,/,$*))/dav.pid`) ; (${UNLOCK} dav)
	while ${LSOF} -nP -u $$(id -u) | fgrep -q 'TCP *:${REGRESS_DAVPORT}' ; do sleep 0.1 ; done ; true
	rm -r ${DEST}/regress/mod_dav
