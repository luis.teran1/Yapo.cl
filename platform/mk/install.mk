INSTALL?=/usr/bin/install

ifneq (${USE_BUILD},yes)
ifdef INSTALLDIR
ifdef STAMPFILE
install: ${GENERATE_TARGETS} $(shell find ${INSTALL_TARGETS} -newer ${STAMPFILE})
	@for a in $(shell find ${INSTALL_TARGETS} -newer ${STAMPFILE}) ; do \
		for dir in ${INSTALLDIR} ; do \
			mkdir -p ${INSTALLROOT}$$dir; \
			${INSTALL} $$a ${INSTALLROOT}$$dir ; \
			printf "install (${STAMPFILE}) \033[1;32m%-40s\033[0m -> \033[1;36m%s\033[0m;\n" $$a ${INSTALLROOT}$$dir; \
		done \
	done
else
install: ${GENERATE_TARGETS} ${INSTALL_TARGETS}
	@for dir in ${INSTALLDIR} ; do \
		mkdir -p ${INSTALLROOT}$$dir; \
		for a in ${INSTALL_TARGETS} ; do \
			if [ -d "$$a" ]; then \
				printf "rsync -a \033[1;32m%-40s\033[0m -> \033[1;36m%s\033[0m\n" $$a ${INSTALLROOT}$$dir; \
				rsync -a --delete --exclude=.svn $$a ${INSTALLROOT}$$dir; \
			elif [ -f "$$a" -o -z "${SRCDIR}" ]; then \
				${INSTALL} $$a ${INSTALLROOT}$$dir ; \
				printf "install \033[1;32m%-40s\033[0m -> \033[1;36m%s\033[0m\n" $$a ${INSTALLROOT}$$dir; \
			else \
				${INSTALL} ${SRCDIR}/$$a ${INSTALLROOT}$$dir ; \
				printf "install \033[1;32m%-40s\033[0m -> \033[1;36m%s\033[0m\n" $$a ${INSTALLROOT}$$dir; \
			fi ; \
		done \
	done
endif
endif

endif
