ifndef ALL_I
ALL_I:=1

all:

# Some macros

# pickdir: Return the first directory that exists.
# arg 1 = colon-separated list of directories to check.
pickdir = $(shell find $(subst :, ,$(1)) -print -prune -type d 2>/dev/null | head -n 1)

# pathsearch: Search for binary in PATH
# arg 1 = binary to find
pathsearch = $(firstword $(wildcard $(addsuffix /$(1),$(subst :, ,$(PATH)))))

# Some default variables.

TOPDIR:=$(abspath ${TOPDIR})
BUILDPATH?=build
SHELL=bash

PLATFORMDIR?=${TOPDIR}/platform

DEFAULT_TARGETS=all clean depend in-file cleandir install regress-print-tests
.PHONY: ${DEFAULT_TARGETS} regress
.SUFFIXES: .o .c .so .soo .tmpl .cc .cxx .l .ll .y .yy
MAKEFLAGS+=-r

REGRESS_LOG?=/tmp/${USER}-regress.log
SYSLOGHOOKSO=$(subst ${TOPDIR},${TOPDIR}/${BUILDPATH},$(realpath ${PLATFORMDIR}))/modules/sysloghook/sysloghook.so
SYSLOGHOOK=env LD_PRELOAD="${SYSLOGHOOKSO}"
ifeq (${BUILD_STAGE},REGRESS)
REGRESS_PHP_HOOKABLE_LOGGING=extension=php_hookable_logging.so
else
REGRESS_PHP_HOOKABLE_LOGGING=
endif

${DEFAULT_TARGETS}:

include ${PLATFORMDIR}/mk/defvars.mk

endif #ALL_I
ifeq ($(USE_BUILD),yes)
include ${PLATFORMDIR}/mk/build.mk
endif
