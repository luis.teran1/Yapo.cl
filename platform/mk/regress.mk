
default: regress

PLATFORMDIR?=${TOPDIR}/platform

include ${PLATFORMDIR}/mk/all.mk
include ${PLATFORMDIR}/mk/clean.mk

TESTDIR?=tests

ifndef NOSUPP
	VALGRIND_SUPPRESSIONS=$(addprefix --suppressions=,$(wildcard *.supp)) $(addprefix --suppressions=,$(foreach dir,${VALGRIND_SUPPRESSION_DIRS},$(wildcard $(dir)/*.supp)))
endif

ifdef VALGRIND
	CMD_PREFIX=valgrind --tool=memcheck --leak-check=full --leak-resolution=high --num-callers=40 --trace-children=yes --track-origins=yes ${VALGRIND_SUPPRESSIONS}
endif

ifdef HELGRIND
	CMD_PREFIX=valgrind --tool=helgrind --read-var-info=yes ${VALGRIND_SUPPRESSIONS}
endif

ifdef MEMCHECK_REPORT
	MEMCHECK_TARGETS:=${REGRESS_TARGETS}
endif

ifdef MEMCHECK_TARGETS
	MEMCHECK_REPORT_CMDLINE:=valgrind --tool=memcheck --leak-check=full --leak-resolution=high --num-callers=40 --trace-children=yes --track-origins=yes --vgdb=no --child-silent-after-fork=yes --xml=yes
	MEMCHECK_REPORT_DIR?=${DESTDIR}/reports/memcheck
endif

MATCH:=${PLATFORMDIR}/bin/match/match.pl
MAILDECODER:=${PLATFORMDIR}/bin/match/maildecoder.sh
FAKEMAIL:=${PLATFORMDIR}/scripts/fakemail/fakemail

ifdef REGRESS_SUITE
REGRESS_SKIPTO?=testsuite-$(REGRESS_SUITE)
endif

_REGRESS_NAME:=$(subst $(TOPDIR)/,,$(CURDIR))
_REGRESS_NAME_ESC:=$(subst /,-,${_REGRESS_NAME})
_REGRESS_OUT= | tee -a ${REGRESS_LOG} 2>&1

REGRESS_FAIL_FILE?=.regress_failed

ifndef _REGRESS_DEFINED
ifdef REGRESS_TARGETS
EXTRACLEAN+=$(REGRESS_FAIL_FILE)

RPT_OUT?=/dev/null

regress-print-tests:
	@ echo -n 'PWD: ' | tee -a ${RPT_OUT} ; \
	pwd | tee -a ${RPT_OUT} ; \
	for rd in ${REGRESS_DEPEND} ; do \
		echo DEPEND: $$rd ; \
	done | tee -a ${RPT_OUT} ; \
	for rt in ${REGRESS_TARGETS} ; do \
		echo TEST: $$rt ; \
	done | tee -a ${RPT_OUT} ; \
	echo CLEANUP: regress-rmextra | tee -a ${RPT_OUT} ; \
	for rd in ${REGRESS_CLEANUP} ; do \
		echo CLEANUP: $$rd ; \
	done | tee -a ${RPT_OUT}

regress-valgrind-report:
	@if [ -n "${MEMCHECK_TARGETS}" ]; then \
		mkdir -p ${MEMCHECK_REPORT_DIR} && \
		if [ -n "${MEMCHECK_RUN_DEPEND}" ]; then \
			make rd ; \
			if [ $$? != 0 ]; then \
				exit 1; \
			fi; \
		fi ; \
		for rt in ${MEMCHECK_TARGETS} ; do \
			if [ "$${rt:0:8}" = "nocheck-" ]; then \
				make $${rt:8} ; \
			else \
				env REGRESS_IS_MEMCHECK=1 CMD_PREFIX="${MEMCHECK_REPORT_CMDLINE} --xml-file=\"${MEMCHECK_REPORT_DIR}/${_REGRESS_NAME_ESC}-$$rt.%p.memcheck\"" make $$rt ; \
			fi ; \
		done ; \
		make rc ; \
	fi

regress-doit rdo:
ifeq (${JUNITXML},1)
	@rm ${TOPDIR}/.junit.cursuite 2>/dev/null || /bin/true
endif
	@skipto="${REGRESS_SKIPTO}" ; \
	skippast="$(subst testsuite,endtestsuite,${REGRESS_SKIPPAST})" ; \
	insuite="" ; \
	rtargets="" ; \
	[ -n "${REGRESS_LOGERR_FILE}" ] && rtprefix="logfail-"; \
	for rt in ${REGRESS_TARGETS} ; do \
		if [ "$$rt" = "$$skipto" ] ; then skipto="" ; fi ; \
		if [ -n "$$skipto" -o -n "$$skippast" ] ; then \
			if [ -z "$(REGRESS_SUITE)" ]; then \
				echo -ne "\\033[1;32mSKIPPED\\033[0;39m " ${_REGRESS_OUT} ; \
				echo ${_REGRESS_NAME}/$$rt ${_REGRESS_OUT} ; \
			fi ; \
		else \
			case "$$rtargets" in \
				$$rtprefix$$rt|$$rtprefix$$rt\ *|*\ $$rtprefix$$rt\ *|*\ $$rtprefix$$rt) \
					${MAKE} $$rtargets; \
					if [ $$? != 0 ]; then \
						exit 1; \
					fi; \
					rtargets=""; \
				;; \
			esac; \
			if [ -n "$$insuite" ] ; then \
				rtargets="$$rtargets $$rtprefix$$rt" ; \
			else \
				rtargets="$$rtargets regress-pre-$$rt $$rtprefix$$rt" ; \
			fi ; \
		fi ; \
		if [ "$${rt:0:9}" = "testsuite" ] ; then \
			insuite="$$rt" ; \
		elif [ "$${rt:0:12}" = "endtestsuite" ] ; then \
			insuite="" ; \
		fi ; \
		if [ "$$rt" = "$$skippast" ] ; then skippast="" ; fi ; \
		if [ "$$rt" = "endtestsuite-$(REGRESS_SUITE)" ]; then exec ${MAKE} $$rtargets; fi; \
	done ; \
	exec ${MAKE} $$rtargets regress-rmextra


trans-depends-%:
	@counter=0 ; \
	QUERY_CMD=$(subst trans-depends-,,$@); \
	echo "LOOKING FOR -$$QUERY_CMD-" ; \
	for rt in ${REGRESS_TARGETS} ; do \
		if [ "$${rt:0:9}" = "testsuite" ] ; then \
			SUITENAME="$$rt" ; \
			SKIP_SUITE=false; \
		else if [ $$SKIP_SUITE == false -a "$${rt:0:2}" = "t-" ] ; then \
			FILENAME="`echo $$rt | sed 's/\..*//g'`" ; \
			FILENAME="tests/$$FILENAME.in" ; \
			if [ -f "$$FILENAME" ] ; then \
				CMD=`grep -m 1 "cmd:" "$$FILENAME" | awk -F: '{ print $$2 }'` ; \
				if [ ! -z "$$CMD" -a "$$CMD" == "$$QUERY_CMD" ] ; then \
					echo "$$SUITENAME" | sed 's/testsuite-//g' ; \
					SKIP_SUITE=true; \
				fi ; \
			fi ; \
		fi ; \
		fi ; \
	done ; \

regress-doit-details rdodetails:
	@counter=0 ; \
	for rt in ${REGRESS_TARGETS} ; do \
		if [ "$${rt:0:9}" = "testsuite" ] ; then \
			(( counter++ )); \
			echo "$$counter - $$rt" ; \
		else if [ "$${rt:0:2}" = "t-" ] ; then \
			FILENAME="`echo $$rt | sed 's/\..*//g'`" ; \
			FILENAME="tests/$$FILENAME.in" ; \
			if [ -f "$$FILENAME" ] ; then \
				CMD=`grep -m 1 "cmd:" "$$FILENAME" | awk -F: '{ print $$2 }'` ; \
				if [ ! -z "$$CMD" ] ; then \
					printf " - - $$CMD\n" ; \
				fi ; \
			fi ; \
		fi ; \
		fi ; \
	done ; \

regress-doit-list rdolist:
	@counter=0 ; \
	for rt in ${REGRESS_TARGETS} ; do \
		if [ "$${rt:0:9}" = "testsuite" ] ; then \
			(( counter++ )); \
			echo "$$counter - $$rt" ; \
		fi ; \
	done ; \

regress-doit-% rdo-%:
	q=$(subst rdo-,,$@); \
	(( q-- )); \
	total=$(shell echo ${REGRESS_TARGETS} | sed 's/ /\n/g' | grep "^testsuite-" | wc -l); \
	(( group_length=total/${TESTS_GROUPS} )); \
	(( start_index=group_length\*q )); \
	(( end_index=start_index\+$$group_length-1 )); \
	@counter=0 ; \
	if [ $$q -gt 0 ] ; then \
		eval ${MAKE} rs-load selenium-start ; \
	fi ; \
	TARGETS='' ; \
	[ -n "${REGRESS_LOGERR_FILE}" ] && rtprefix="logfail-"; \
	for rt in ${REGRESS_TARGETS} ; do \
		if [ "$${rt:0:9}" = "testsuite" ] ; then \
			(( counter++ )); \
		fi ; \
		if [ $$counter -ge $$start_index -a $$counter -lt $$end_index ] ; then \
			TARGETS+="$$rt "; \
		fi ; \
	done ; \
	exec ${MAKE} regress-doit REGRESS_TARGETS="$$TARGETS"


# The whole eval/exec mess below is there to get the exit code
# from the make command while piping stdout/stdout both to
# a log file and to stdout.  PIPESTATUS is supposed to be
# able to solve this more elegantly but I couldn't get it to
# work and found a number of bug reports related to it.
# Inside the `...`, fd4 goes to the pipe
# whose other end is read and passed to eval;
# fd1 is the normal standard output preserved
# the line before with exec 3>&1
logfail-%:
ifeq (${JUNITXML},1)
	@start_time=`date +%s%N`; \
        exec 3>&1; \
        eval `exec 4>&1 >&3 3>&-; ( ${MAKE} --no-print-directory $* 4>&- 2>&1 ; echo "ec1=$$?;" >&4 ) | tee .junit.log`; \
	if [ $$ec1 -ne 0 ]; then \
		echo "$(subst logfail-,,$@)" >> ${REGRESS_LOGERR_FILE}; \
		res=FAILED; \
	else \
		res=OK; \
	fi; \
	end_time=`date +%s%N`; \
	${PLATFORMDIR}/scripts/junit/junit.pl ${TOPDIR}/.junit.cursuite ${TOPDIR}/junitresults $* $$start_time $$end_time $$res .junit.log
	@rm -f .junit.log
else
	@${MAKE} --no-print-directory $(subst logfail-,,$@) || echo "$(subst logfail-,,$@)" >> ${REGRESS_LOGERR_FILE}
endif

regress-rmextra:
	rm ${RMFLAGS} -f ${EXTRACLEAN}

regress-pre-%:
	@echo $* > $(REGRESS_FAIL_FILE)
#	@echo Running test $*

regress-continue rco::
	exec ${MAKE} regress-doit REGRESS_SKIPTO="`cat $(REGRESS_FAIL_FILE)`"

regress-continue-next rnext::
	exec ${MAKE} regress-doit REGRESS_SKIPPAST="`cat $(REGRESS_FAIL_FILE)`"

regress-suite-%:
	exec ${MAKE} regress-doit REGRESS_SUITE=$*

testsuite-%:
	@echo -e "\\033[1;36mStarting suite $*\\033[39;0m ($(shell date +%H:%M:%S))"
	@${MAKE} --no-print-directory countsuite-$*
ifeq (${JUNITXML},1)
	@line=$(shell echo ${REGRESS_TARGETS} | sed 's/ /\n/g' | grep "^testsuite-" | grep -E "^testsuite-$*[ \t]*$$" -n | cut -d ':' -f1); \
        if [ "$$line" ] ; then \
                line=`printf "%03d" $$line`; \
        fi; \
	echo s$${line}-$* > ${TOPDIR}/.junit.cursuite
endif

endtestsuite-%:
	@echo -e "\\033[1;36mEnding suite $(@:endtestsuite-%=%)\\033[39;0m"
ifeq (${JUNITXML},1)
	@rm ${TOPDIR}/.junit.cursuite 2>/dev/null || /bin/true
endif

countsuite-%:
	@line=$(shell echo ${REGRESS_TARGETS} | sed 's/ /\n/g' | grep "^testsuite-" | grep -E "^testsuite-$*[ \t]*$$" -n | cut -d ':' -f1); \
	if [ "$$line" ] ; then \
		total=$(shell echo ${REGRESS_TARGETS} | sed 's/ /\n/g' | grep "^testsuite-" | wc -l); \
		echo -e "\\033[1;36m$* is test $$line of $$total\\033[39;0m" ; \
		case "$$TERM" in xterm*) echo -en "\\033]0;$$line of $$total: $*\\007" ;; esac ; \
	fi

ifdef REGRESS_DEPEND
${SYSLOGHOOKSO}:
	${MAKE} -C ${PLATFORMDIR}/modules/sysloghook

RDDEP?=${SYSLOGHOOKSO}

regress-depend:: $(RDDEP)
	if test "x$(REGRESS_LOG_KEEP)" = "x" -a -f "$(REGRESS_LOG)"; then \
		rm -f "$(REGRESS_LOG)" "$(REGRESS_MAMA_LOG)"; \
	fi
	mkdir -p $$(dirname "${REGRESS_LOG}")
ifneq ($(strip $(REGRESS_DEPEND)),)
	@$(foreach rule,$(REGRESS_DEPEND),printf "\n * Running 'make $(rule)' in '$(abspath $(CURDIR))': \n\n" && make $(rule) && ) true;
endif
else
regress-depend: $(RDDEP)
endif
ifdef REGRESS_CLEANUP
regress-clean::
	for t in ${REGRESS_CLEANUP} ; do printf "\n * Running 'make $$t' in '$(abspath ${CURDIR})': \n\n" && make $$t || true; done
else
regress-clean::
endif

regress r:
	${MAKE} regress-depend
	if ${MAKE} regress-doit ; then ${MAKE} regress-clean ; else ${MAKE} regress-clean ; exit 1 ; fi

else
ifdef NO_REGRESS
regress r:
regress-print-tests:
regress-valgrind-report:
else
regress r:
	@echo -e "\\033[1;31mFAILED\\033[0;39m" ${_REGRESS_NAME}/no-test ${_REGRESS_OUT}
endif
endif
endif

# Aliases
rd:	regress-depend

rc:	regress-clean


rs-%:
	$(MAKE) bconf_restart
	$(MAKE) regress-suite-$*

bconf-overwrite-%:
	@test "$$(curl -s --write-out "%{http_code}" http://${REGRESS_HOSTNAME}:${REGRESS_BCONFD_JSON_PORT}/'bconf_overwrite?key=$(patsubst $(firstword $(subst :, ,$*)):%,$(firstword $(subst :, ,$*))&value=%,$*)' -o /dev/null)" = 200; \
	printf 'cmd:control\naction:reload\ncommit:1\nend\n' | ${NETCAT} localhost ${REGRESS_TRANS_CPORT} | fgrep TRANS_OK

bconf-only-overwrite-%:
	@test "$$(curl -s --write-out "%{http_code}" http://${REGRESS_HOSTNAME}:${REGRESS_BCONFD_JSON_PORT}/'bconf_overwrite?key=$(patsubst $(firstword $(subst :, ,$*)):%,$(firstword $(subst :, ,$*))&value=%,$*)' -o /dev/null)" = 200

bconf-path-overwrite-%:
	@test "$$(curl -s --write-out "%{http_code}" http://${REGRESS_HOSTNAME}:${REGRESS_BCONFD_JSON_PORT}/'bconf_overwrite?key=$(subst --,/,$(subst :,&value=,$*))' -o /dev/null)" = 200; \
	printf 'cmd:control\naction:reload\ncommit:1\nend\n' | ${NETCAT} localhost ${REGRESS_TRANS_CPORT} | fgrep TRANS_OK

bconf-only-path-overwrite-%:
	@test "$$(curl -s --write-out "%{http_code}" http://${REGRESS_HOSTNAME}:${REGRESS_BCONFD_JSON_PORT}/'bconf_overwrite?key=$(subst --,/,$(subst :,&value=,$*))' -o /dev/null)" = 200; \

bconf-flush-overwrite:
	@test "$$(curl -s --write-out "%{http_code}" http://${REGRESS_HOSTNAME}:${REGRESS_BCONFD_CONTROLLER_PORT}/reload -o /dev/null)" = 200;

trans-flush-overwrite:
	@test "$$(curl -s --write-out "%{http_code}" http://${REGRESS_HOSTNAME}:${REGRESS_BCONFD_CONTROLLER_PORT}/reload -o /dev/null)" = 200; \
	printf 'cmd:control\naction:reload\ncommit:1\nend\n' | ${NETCAT} localhost ${REGRESS_TRANS_CPORT} | fgrep TRANS_OK
	

# Trans
DO_TEST=${NETCAT} localhost ${REGRESS_TRANS_PORT}
EXTRACLEAN+=.cmd.in .watch_unique_id .cmd.out .prevtime .ad.code .token .mail.txt .mail.orig.txt .attachment .mail.db

trans-pending-wait:
	@while [ -z "`printf "cmd:transinfo\ncommit:1\nend\n" | ${DO_TEST} | grep \"pending_transactions total (internal/external):[1-9] (0/[1-9])\"`" ]; do \
		echo Waiting for pending transactions...; \
		sleep 0.1; \
        done;

platform-t-%:
	@rm -f .cmd.in; \
	TAR="`echo t-$* | sed s,[.].*,,`"; \
	cat ${TESTDIR}/$$TAR.in >> .cmd.in; \
	sed -i -e 's/%REGRESS_HTTPD_PORT%/${REGRESS_HTTPD_PORT}/' -e 's/%REGRESS_HTTPS_PORT%/${REGRESS_HTTPS_PORT}/' -e 's/%REGRESS_HOSTNAME%/${REGRESS_HOSTNAME}/' -e 's,%TESTDIR%,$(abspath ${CURDIR})/${TESTDIR},' .cmd.in; \
	rm -f .cmd.out; \
	if [ "$(subst .mail,,t-$*)" != "t-$*" -o "$(subst .nomail,,t-$*)" != "t-$*" ]; then \
		rm -f .mail.txt; \
		rm -f .mail.orig.txt; \
		rm -f .attachment; \
	fi; \
	if [ "$(subst .exec,,t-$*)" != "t-$*" ]; then \
		env TOPDIR=$(abspath ${TOPDIR}) PROG=${PROG} REGRESS_LOG=${REGRESS_LOG} REGRESS_BLOCKET_ID=${REGRESS_BLOCKET_ID} PGOPTIONS="-c search_path=${BPVSCHEMA},public" REGRESS_PGSQL_HOST=${REGRESS_PGSQL_HOST} OBJDIR=${OBJDIR} REGRESS_TRANS_PORT=${REGRESS_TRANS_PORT} NETCAT="${NETCAT}" ${TESTDIR}/$$TAR.exec; \
		test $$? != 0 && exit 1; \
	else \
		cat .cmd.in | ${DO_TEST} > .cmd.out; \
		test $$? != 0 && exit 1; \
	fi; \
	${MATCH} .cmd.out ${TESTDIR}/$$TAR.out --ignore-time --ignore-token; \
	if [ $$? != 0 ]; \
	then \
		echo ": edit INPUT  ; $$EDITOR ${TESTDIR}/$$TAR.in"; \
		exit 1; \
	fi; \
	if [ "$(subst .pgsqldiff,,t-$*)" != "t-$*" ]; then \
		cp -f ${TESTDIR}/$$TAR.pgsqlin .sql.in; \
		sed -i -e "s/%AD_CODE%/$$(< .ad.code)/" -e "s/%YEAR%/$$(date +%Y)/g" .sql.in; \
		psql --no-psqlrc -h ${REGRESS_PGSQL_HOST} --no-align --field-separator=, --pset footer blocketdb < .sql.in > .sql.out; \
		${MATCH} .sql.out ${TESTDIR}/$$TAR.pgsqlout ;\
		if [ $$? != 0 ]; \
		then \
			echo ": edit INPUT  ; $$EDITOR ${TESTDIR}/$$TAR.pgsqlin"; \
			exit 1; \
		fi; \
	fi; \
	if [ "$(subst .mail,,t-$*)" != "t-$*" ]; then \
		cnt=180; \
		while [ $$cnt -gt 0 -a \( ! -f .mail.txt -o `/sbin/fuser -s .mail.txt 2>/dev/null; echo $$?` -eq 0 \) ] ; \
			do sleep 0.1 ; cnt=`expr $$cnt - 1` ; \
		done ; \
		if [ "$(subst .mailskip,,t-$*)" = "t-$*" ]; then \
			mv .mail.txt .mail.orig.txt ;\
			${MAILDECODER} < .mail.orig.txt > .mail.txt ;\
			${MATCH} .mail.txt ${TESTDIR}/$$TAR.mail ;\
			if [ $$? != 0 ]; \
			then \
				echo ": edit INPUT  ; $$EDITOR ${TESTDIR}/$$TAR.mail"; \
				exit 1; \
			fi; \
			if [ -f .attachment ]; then \
				${MATCH} .attachment ${TESTDIR}/$$TAR.attach ;\
				if [ $$? != 0 ]; \
				then \
					echo ": edit INPUT  ; $$EDITOR ${TESTDIR}/$$TAR.attach"; \
					exit 1; \
				fi; \
			fi; \
		fi; \
	fi; \
	echo -n "."; exit 0

ifdef CHECK_DB_OLDFORMAT
check-db-%:
	cat ${TESTDIR}/$@.in >.sql.in && env PGOPTIONS='-c search_path=bpv,public' psql -h ${REGRESS_PGSQL_HOST} -t blocketdb < ${TESTDIR}/$@.in >.sql.out
	${MATCH} .sql.out ${TESTDIR}/$@.out
else
check-db-%:
	cat ${TESTDIR}/$@.in >.sql.in && env PGOPTIONS='-c search_path=bpv,public' psql --no-align --field-separator=, --pset footer -h ${REGRESS_PGSQL_HOST} -t blocketdb < ${TESTDIR}/$@.in >.sql.out
	${MATCH} .sql.out ${TESTDIR}/$@.out
endif

update-db-%:
	sed -e "s/%YEAR%/$$(date +%Y)/g" ${TESTDIR}/$@.in >.sql.in && env PGOPTIONS='-c search_path=bpv,public' psql -h ${REGRESS_PGSQL_HOST} -t blocketdb < .sql.in >.sql.out
	${MATCH} .sql.out ${TESTDIR}/$@.out

wait-db-%:
	@i=0; \
	while [ $$i -lt 20 ]; do \
		psql --no-align --field-separator=, --pset footer -h ${REGRESS_PGSQL_HOST} -t blocketdb < ${TESTDIR}/$@.in >.sql.out ; \
		${MATCH} .sql.out ${TESTDIR}/$@.out > /dev/null && break ; \
		sleep 0.5 ; \
		(( i++ )); \
	done; \
	if [ $$i = 20 ]; then \
		echo ${MATCH} .sql.out ${TESTDIR}/$@.out; \
		${MATCH} .sql.out ${TESTDIR}/$@.out; \
		echo "Timed out in $@"; \
		exit 1; \
	fi; \
	true

check-redis-%:
	sed -e 's/%CURRDATE%/${shell date +"%Y%m%d"}/' ${TESTDIR}/$@.in | \
	${REDIS_CLI} -p ${REGRESS_REDIS_PORT} -x > .redis.out
	${MATCH} .redis.out ${TESTDIR}/$@.out

gotest-%:
	@(cd ${TOPDIR} && TMPDIR=${BUILDROOT} ninja -f ${BUILDPATH}/build.ninja ${BUILDPATH}/${FLAVOR}/gotest/$*)
