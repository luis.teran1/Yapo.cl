
# Version 2.9.3 [2015-06-17] #

* [!655] Fix syntax/Check return status in target bconfd-trans-regress-reload (segmx)
* [!656] Correct race in bconfd that would result in empty bconf being returned (segmx)
* [!660] Fix compiling with make.
* [!665] bconfd initscript and pid file handling fixes (segmx)
* [!668] Add filter state/pool NULL checks to redis filters.
* [!672] Only include hiredisextra.h if libhiredis version < 0.12
* [!666 #576] Redis filter state: handle engine names with dots in the keys.

# Version 2.9.2 [2015-04-28] #

* [!647 #567] Fix for buf_string corruption issue on concatenate path (HIGH PRIORITY)
* [!648 #568] Fix search engine {sub}order range parsing when upper range is zero.

# Version 2.9.1 [2015-03-19] #

* [!623] Don't leak search_instance_config in search (harmless, but valgrind whines).
* [!624] CentOS 7 was calling mod_templats templates from mod_list.
* [!626] Properly set vtree type in shadow_vtree and prefix_vtree.
* [!627] Fix multiple problems with the log_string filter when used with loop variables. (blocket)
* [!628] Fixed memory leak in trans command_bpapi_free. (yapo)
* [!629] More efficient growing of buf_string buffers.
* [!630] Remove 'rpms' and 'debs' files from root.
* [#561 !631] Fix postgres error string leak in trans pgsql api. (yapo)
* [#562 !632] Don't use strlen() on input to qp filter. (blocket)
* [!633] Correct configuration for common/http test. (platform internal)
* [!635] UPGRADE block for trans/redis changes from 2.9.0.
* [!637] Properly initialize private fds in http_req.c. (regression in platform 2.2.2)
* [!640] Debug output for configured backends in trans. (tutti)
* [#564 !641] New search query parser got double confused on single sort attributes.
* [!642] Fix entropy in rand_uint16().
* [!643 !645 #565] Fix transapi immediate callbacks (i.e token) by making code blob-aware.

# Version 2.9.0 [2015-02-06] #

* [#169 !500] Let some template filters/functions accept vtree nodes.
* [#361 !537] mod_bsconf removed.
* [#362 !595] Escape control characters (newlines) in message part of trans status.
* [#406 !583] Add platform regress support for generating valgrind memcheck reports (platform internal)
* [#454 !464] Add docker images for platform regress.
* [#482 !502] Add more control over mod_image speed/quality trade-off when resampling.
* [#488 !554] Trans loads bconf from bconfd on start up
* [#489 !493] Script to verify all packages needed in a Dockerfile.
* [#499 !537] Redis filters use filter_state.
* [#503 !527] Add BOS restart delay backoff.
* [#513 !524] Add SHA-2 lib to lib/common, add sha2 and hex template filters.
* [#514 !575] Move crypto filters and functions to own directory
* [#527 !578] bconf_overwrite now belongs to bconfd, available in the regress flavour only
* [#529 !557] A simple interface to add custom headers to controllers
* [#539 !591] Fix for interval search in combination with sorting.
* [#540 !593] Fix handling of string literals in indirect key lookups
* [!478] New method for distributing indexes between the indexer and search engines has been developed.
* [!487] Small cleanup of buf_string code.
* [!488] Add bconf_vget_int_default()
* [!490] Added support for deltas in timers. Needed for logging later.
* [!492] Add writev_retry() and fd_pool_conn_writev() to lib/common
* [!497] Add a list/dict type to vtree_keyvals.
* [!499] Add a filter to access simplevars through vtree.
* [!506] Support building platform on Debian 7 (wheezy) and Debian 8 (jessie)
* [!511] Use su instead of sudo in scripts/init/search
* [!512] In prefix_vtree, make sure to check for the dot when prefix match.
* [!514] Allow factories to perform COMMIT or ROLLBACK transactions in different FA_SQL actions
* [!516] Simple stat counter infrastructure similar to counters, but far simpler. See the test for how to use.
* [!522] mod_image will INFO log platform and GD version on being configured.
* [!526] Add Docker information to README.md
* [!529] Removed obsolete index.counters bconf (platform internal)
* [!532] Add deffun_vtree which puts it's parameters in the vtree.
* [!535] bconf add-data API cleanup/NULL checking changes.
* [!539] When built with -DDEBUG, the timer module will invoke a destructor to free itself.
* [!543] Add UTF-8 versions of strpos, strrpos and substring
* [!546] Add the ! functionality to the bconf key lookups for settings. Extend tests.
* [!557] '#include' no longer supported by bconf nor config_init()
* [!562] Escape newline in jsencode filter.
* [!563] Change vocabulary tests so that we don't have an appl with dashes (platform internal)
* [!564] Add bconf_backend transinfo
* [!567] Fix a bad cast in the redis filters code by collapsing two structures to one (tutti)
* [!576] Added HELGRIND similar to VALGRIND, and suppression file support.
* [!579] Add bpapi_insert_buf(key, klen, value, vlen) to BPAPI.
* [!582] Fix logic error related to arguments in bconf_vtree (tutti)
* [!585] Hook up tpc and sbalance tests in regress (platform internal)
* [!586] Fix for distsort in search engine, and hook up search-distance tests.
* [!587] Add support for case-sensitive sorting (blocket)
* [!587] Make sysloghook log with microseconds (blocket)
* [!587] redis_int_reply returns the underlying long long (blocket)
* [!592] Fix HMSET return value handling in redis_sock_hmset
* [!594] Add watermark padding support to mod_image (seg.mx)
* [!596] Removed the previously deprecated &latin1_to_utf8 template function.
* [!598] Use FA_DONE() instead of {0} in our regress trans factories (platform internal)
* [!601] Validate bconf entries while loading on bconfd

## Bconfd ##

Bconf has been split from trans to a separate service, bconfd. This avoids the
most common use of the badly working trans reload. This new service can also
produce bconf as JSON.

Traditionally, bconf entries can be stored either in files or in the database. In the
first case, usually referred to as 'static' or 'file' and in the latter case usually referred
to as 'dynamic conf' or 'dynconf'. The configuration served by default is the merging of
the static and the dynamic, naturally referred to as 'merged'. Bconfd is of course responsible for
reading entries from the database however it is **not** responsible for writing to the database.
That responsibility still belongs to trans.

Trans is now a client of bconfd as the rest of the services / scripts. Since the
nature of bconf sets the merged configuration as the default one, now trans starts with the
merged configuration in its memory as opposed to the static one. This can potentially create
some confusion to the user since transactions never before had access to bconf nodes existing in
the dynamic configuration. To avoid stale values, dynamic configuration should continue to be
passed as parameters to transactions.

Also the ``bconf`` command has changed, since it only serves the configuration that
trans has loaded during start up or on reload. That means that now it should
**only** be used for administration purposes.

The bconf daemon will start only if valid keys/values are present in the file
configuration or database. On a reload operation, if invalid key/value pairs are
present then the reload will fail and the previously loaded configuration continues
to be served.

A single invalid entry is enough the cancel the load / reload operation when reading
from the static configuration in the files. While loading from the db though, invalid or
conflicting keys will be ignored and all valid entries will be successfully loaded. In
both cases keys that are ignored are logged. Counters for the invalid or conflicting
keys in the dynamic bconf are additionally returned in the `/stats` output. See the
[wiki](https://scmcoord.com/wiki/Bconfd) for the available bconfd commands.

The following entries are considered invalid:

	|:-------------:|:-------------:|
	| Case		| Example	|
	|:-------------:|:-------------:|
	| Missing '='	| foo.bar	|
	| Missing key	| =val		|
	| Missing node	| .foo.bar=val	|
	| Missing node	| foo..bar=val	|
	| Missing node	| foo.bar.=val	|

As well as entries that create a node/list conflict:

	foo.bar=val
	foo.bar.1=val

In the above scenario both entries are valid in their own accord, though
combined they will trigger an error since semantically a node can not be
a leaf and internal at the same time.

Since bconfd is responsible for reading and serving bconf, `bconf_overwrite` now belongs to it.
On the JSON port of bconf, `bconf_overwrite` is only available in the regress flavour.
It follows that `bconf_overwrite` has been removed from trans and with it a nasty bug that
could lead to trans crashing if `bconf_flush` was used in production under certain local
implementations of setconf.

In order to issue `bconf_overwrite` in regress, use:

	curl http://${REGRESS_HOSTNAME}:${REGRESS_BCONFD_JSON_PORT}/bconf_overwrite?key=<key>&value=<value>

On successful completion, the proper http_code is returned along with a body containing "status:success" in JSON
format. In the event of a failure, the appropriate http_code is returned with an empty body.

The overwritten configuration is held internally in a specific storage of bconfd. It is only visible to the user
in the ``merged`` checksum provided from the daemon.

The greatest implication of this move is that bconf_overwrites in regress have to explicitly ask trans to reload
the new configuration. This is already handled by the platform provided ``make`` target.

Two new ``make`` targets have been added to flush the overwritten configuration. One in bconfd only and another in
both bconfd and trans:

	make bconf-flush-overwrite
	make trans-flush-overwrite

The future plans for bconfd is that it will be able to push out changes without reloading services.

For information regarding the control port of bconfd,
please refer to [the corresponding wiki page](https://scmcoord.com/wiki/Bconfd#Control_Port)

Refer to [UPGRADE.md](UPGRADE.md) for a complete set of upgrade instructions.

## Controllers ##

Controllers are a new library for controlling services using HTTP. Search, bconfd,
and the indexer daemon use this for their control ports.

In some cases it is useful to set some custom header in controllers for the requester.
In order to avoid the complexity of general headers that will require special handling
some restrictions have been set in place.

Only unique headers starting with 'X-' are allowed in order to keep a level of sanity
On duplicates or headers not starting with 'X-' the controller will xerrx.
The sanity of the header keys and values is left to the user.

Use:

	ctrl_set_custom_headers(cr, "X-Custom", "some-value");

## Docker ##

The platform tree now contains Dockerfiles for creating images to test
platform. This makes it easier to contribute and test the same way
as we do, and also adds documentation about required packages and other
environment settings.

The docker files are in scripts/docker, more information is also
available in docs/docker.md and in the [wiki](https://scmcoord.com/wiki/Docker).

## Libcommon ##

The BOS has historically slept for two seconds before restarting a child.
This can now be configured with an optional backoff:

	set_respawn_backoff_attrs(int min_s, int max_s, float rate);

Rate should be specified as 1.0f or higher. The defaults are equal
to calling

	set_respawn_backoff_attrs(2, 2, 1.0f);

This is meant for situations where the child is likely to use a lot
of resources on startup.

	set_respawn_backoff_attrs(3, 120, 1.4f);

This would ramp the backoff from 3 to 120 seconds at a rate of 40%,
peaking around the 12:th attempt.

If the client stays alive for more than five minutes the delay resets
back to the minimum.

## mod_image ##

The new type level configuration 'watermark_padding' can be used to add a
regular amount of padding around the watermark.

### Fast resampling ###

By default mod_image resamples images when scaling by calling gdImageCopyResampled(). This produces good
looking results, but is CPU intensive.

The CPU load is mostly determined by the size of the _source image_, since every pixel in it will be sampled
multiple times. One way to reduce the workload, while trying to retain some of the image quality offered by
a good resampler, is to resize the image to an intermediate size with a quick algorithm, and then use the
better resampling algorithm from there.

This approach is now implemented in mod_image and configured using the new 'resample.fast_*' options:

	resample.enabled (default: 1)

Interpolated resampling can now be disabled entirely by setting this to 0.
In this case gdImageCopyResized() is called instead. Using this non-interpolating
resize is about 20 times faster, but of course the resulting image will be very noisy so
it's not likely to be useful except in special circumstances.

*IMPORTANT NOTE*: The features below requires GD 2.1.0 or later!

	resample.fast_min_pixels (default: 307200 (640x480))

For fast resampling, only care about source images with at least this many pixels. It's a
waste of time on small images, and this option allows us to tune what we consider 'small'.

	resample.fast_max_level (default: 0)

The intermediate image resolution is determined through repeated halving. The more times the source
image can be halved (never going below the destination size, of course), the greater the speed-up,
but also the worse the final image quality.

This parameter controls the maximum number of times we halve an image. The default is '0'
which also means the fast resampling is disabled. If you do not wish to restrict the depth
in practice you can set this to something like 10.

	resample.fast_backoff (default: 0)

Once we've calculated the maximum level, back-off this many levels. This option allows us
fine-tune for a minimum level of quality. The default is '0', but 1-3 are reasonable depending
on the circumstances.

Setting this to a number greater or equal to 'fast_max_level' makes no sense, since they'll
cancel out.

These options can be set on the root-level to provide defaults for all configuration nodes.

#### Benchmarks ####

These are micro-benchmarks representing a best-case scenario, using a very high-resolution
source image and with little contention on the CPU.

		Benchmark 'canal.jpg' (5184x3456) to 640x480 ("vi")
	Level		Intermediate size		Rate			Speedup
	0			5184x3456				2.8/s			0
	1			2592x1728				7.6/s			2.7
	2			1296x864				19.5/s			6.9

		Benchmark 'canal.jpg' (5184x3456) to 130x98 ("list thumb")
	Level		Intermediate size		Rate			Speedup
	0			5184x3456				3.13/s			0
	1			2592x1728				10.93/s			3.5
	2			1296x864				43.33/s			13.8
	3			648x432					139.66/s		44.6
	4			324x216					366.13/s		116.9
	5			162x108					684.80/s		218.6

What we can not tell from this table is the reduction in image quality, so when deciding on the
parameters some testing will be necessary.

#### Recommendations ####

Don't use the fast path indiscriminately. Because there is a component of image degradation,
manual testing with visual inspection is called for. Use the fast path if you are having
problems with CPU load, and/or after extensive testing.

For main images, fast_min_pixels > 2M and fast_max_level 1 may be a good starting point.

For thumbnails, fast_max_level=10, fast_backoff=2 may be a good starting point.

Please pass any success stories to us.

## Regress ##

The VALGRIND=1 and HELGRIND=1 environment variables expand into CMD_PREFIX,
a make variable which is used by many common rules in regressinit.mk and elsewhere
to invoke valgrind for debugging.

Both will add any local suppression files by looking for '*.supp'.

Also, VALGRIND_SUPPRESSION_DIRS can be set to a list of additional directories
to scan.

Setting NOSUPP=1 disables suppression file use.

### valgrind memcheck reports ###

(The following feature was built for internal platform use, but should be easy to adapt to site use)

We've added rules and configuration to more easily generate valgrind memcheck reports from regress tests.

Setting MEMCHECK_REPORT=1, or listing targets in MEMCHECK_TARGETS, will cause the the regress.mk
make rule 'regress-valgrind-report' to run said targets (or all REGRESS_TARGETS in the case of
MEMCHECK_REPORT=1) under a valgrind configuration that writes an XML report file to MEMCHECK_REPORT_DIR,
defaulting to ${DESTDIR}/reports/memcheck.

MEMCHECK_RUN_DEPEND can be set if the standard dependencies are required for the test(s) to run.
Note that the dependencies themselves are not run under valgrind.

For Jenkins integration, see https://wiki.jenkins-ci.org/display/JENKINS/Valgrind+Plugin

## Templates ##

Some filters and function can accept a vtree node. This is done by ending on the dot.
You will need to consult each filter or function on how they process the node.
A vtree node is a list of keys and values (an ordered dictionary), thus both keys
and values are usually processed.

Filters that accept nodes:
* loop
* trans_conf
* with_vtree

Functions that accept nodes:
* in_vtree_list
* vtree_js
* vtree_js_arrays
* vtree_js_pretty

In particular, the loop filter will let you access a vtree list as `%value`. See
example in `platform/bin/tmpltut/templates/loop_filters.txt.tmpl`.

deffun_vtree has been added. It works like deffun but the parameters
will be available in the vtree instead of simplevars.
It works especially well with the vtree node arguments with which you
can access a whole subtree. Example in
`platform/bin/tmpltut/template/deffun_vtree.txt.tmpl`

Added a filter to encode a string in hexadecimal.

	<%|hex|ABC123%>

Added SHA-2 family of hashes as a template filter called sha2 capable of generating SHA-256, SHA-384 and SHA-512 digests.

Should normally be paired with an encoding filter such as hex or base64 as seen in the example:

	<%|hex,sha2(256)|Something worth hashing%>

The argument must be one of 256 (default if omitted), 384 or 512 for digests of size 32, 48 and 64 characters.

An `stree` filter has been added, which allows you to access simplevars
though vtree. By default `$(s.foo)` and `$(l.foo.@)`.

Additionally, whenever `$(s)` or `$(l)` are used (or `$(s.foo)` etc.)
the compiler will add an `stree` filter to that block automatically,
unless already added at a higher block. Thus you usually don't have
to do anything except use the keys when needed.

For more examples, see `platform/bin/tmpltut/vtree_filters.txt.tmpl`.

The vtree backend can now tell functions such as vtree_js if a node
should be represented by a dictionary or a list.

E.g. if a PHP array has numeric keys with values 0, 1, ... it will
always be represented as a list by vtree_js.

## Trans ##

Each FA_SQL action was using a new worker on each call. That meant that there was no
way to be able to perform pgsql transactions in different actions since there was no
guarantee that the same worker would be assigned.

In order to maintain the intended simplicity of factory transaction and to allow to
perform ROLLBACKs or COMMITS two new flags have been introduced:
* FASQL_HOLD_WORKER: hold this worker for later use. Only one worker is allowed to be held at any moment.
* FASQL_REUSE_WORKER: use the held worker for a new query. After the result has been parsed the worker is put.

An error "ERROR_FA_SQL_ACTION:error_flags" is returned in case that:
* FASQL_HOLD_WORKER flag is set and a previous FA_SQL action has reserved a worker
* FASQL_REUSE_WORKER flag is set and no worker has been reserved
* both flags are set

It follows reason that when a worker will be reused then a config.db node is not necessary

Practically this means that the following is legal
```
        FA_SQL(NULL, "pgsql.master", "sql/farollback_begin.sql", FASQL_OUT | FASQL_HOLD_WORKER),
        FA_FUNC(somefunc),
        FA_SQL(NULL, "", "sql/rollback_commit.sql", FASQL_REUSE_WORKER),
```

It also means that even if the second FA_SQL actions was declared as
```
	FA_SQL(NULL, "pgsql.something_else", "sql/rollback_commit.sql", FASQL_REUSE_WORKER),
```
The action would have been performed in "pgsql.master" declared in the first action.

Trans is now a client of bconfd for configuration, see
[UPGRADE.md](UPGRADE.md) for how to apply the changes

The obvious side effect is that trans is no longer responsible for polling the
db for configuration changes. Hence the key `type` of the command `bconf` is no
longer used. Ask bconfd for the dynamic configuration.

The global keys `bconf_root`, `host_bconf` and `trans_bconf` are maintained.

To obtain a new configuration a reload has to be issued on the control port

Since bconf is loaded in trans from bconfd, the related information has been added
to transinfo. In the backends info some two new lines have been added:

	bconf.checksum:<merged_checksum>
	bconf.loaded_at:<epoch> (elapsed: <elapsed>s)

## Indexer Daemon ##

A new method exists for distributing indexes between the indexer and
the search engines.

No changes are necessary to keep using the old mechanism, but sites
are encouraged to start testing the indexer daemon because it provides
better performance and greater configurability.

The indexer daemon is documented on the [wiki](https://scmcoord.com/wiki/Indexer_daemon).

# Version 2.8.2 [2014-12-11] #

 * [#516 !540] Transapi callback erroneously invoked for data found in blobs (blocket).
 * [#501 !528] Improve locking in sql_worker and make transinfo safer.
 * [#438 !533] More detailed logging of factory transactions (debug only).
 * [!521] Searches for "idfixed:x,x" (duplicates of same ad) triggered a CRIT log in the search engine and cut the query short.
 * [!523] Ninja's gcov variables are now separate, don't override copts/ldopts.
 * [!536] Minor cleanup in trans_filters.
 * [!544] Allow FA_TRANS NULL prefix (701search).
 * [!555] Generate correct platform_templates lib dependency for tmplin targets.
    
## factory debug logging ##

In debug a log line has been added for each action in the format:

```
   factory_fsm [command_name] action[action_counter] ACTION_NAME(parameters...)
```

* The action_counter is 1 based.
* For FA_FUNC and FA_COND the pointer to the function is printed.

Example output for "fatrans_test":

```
   factory_fsm [fatrans_test], action[1] FA_SQL((null), pgsql.slave, sql/fatrans_test.sql, 0)
   factory_fsm [fatrans_test], action[2] FA_TRANS(trans/fatrans_test.txt)
   factory_fsm [fatrans_test], action[3] FA_MAIL(mail/fatrans_test.txt)
   factory_fsm [fatrans_test], action[4] FA_DONE()
```

## transinfo ##

A new info line has been added to transinfo for sql_worker:
    busy_%u:state %s in transition

This line can appear when the worker is still in the queue of busy_workers but the
request itself has not been updated yet. For example in sql_worker_put when the request
has completed but there are more queued requests to be handled.

In practice this means that in a subsequent call to transinfo that worker will have 
either moved to the free list or will have taken up a new request.
 


# Version 2.8.1 [2014-11-05] #

* [!486] Add a basic test for the 'timer' module (platform internal)
* [!495] Autoput async workers from all config nodes when requesting a new one.
* [!496] Fix some template related memory leaks.
* [!503] Add template output warning to set_cookie et.al.
* [!505] Make regress-depend actually fail on errors.
* [!509] Minor bug fixes in mkcharmap. Normalization problems in casefold.
* [!515] Correct bad merge affecting vtree_filters.c (tutti)
* [!517] Add README.md to platform directory with build+test instructions.

# mod_templates #

Template functions that generate or modify HTTP headers can not be used after we've
already flushed template data to Apache. A warning was logged for decline, set_expire,
redirect and mime_type, we've now simplified the code and added set_{secure_,}cookie,
clear_cookie and statuscode to that list.

# search #

The charmap code for UTF-8 had a problem where if case folding produced
decomposed characters then they were not recomposed and the charmap
thus checked the wrong values.

Not likely to affect latin characters.

# regress/make #

The `regress-depend` rule would not stop on errors; it would *break*, but continue
happily from there on. Which meant that, for instance, if trans can't start for
some reason, apache would try to start anyway (and fail), and the cause would
not be immediately obvious.

We now use a `foreach` to generate a conjunction that will terminate at the point of failure.

If you get the error 'make: *** empty string invalid as file name.  Stop.' it
means you are setting REGRESS_DEPEND="" in that directory. If there are no
deps to start, you should just remove the line.


# Version 2.8.0 [2014-10-01] #

* [#102 !278] http_req and tls are deprecated and soon will be removed from the repo.
* [#367 !452] Allow for duplicate INSTALL targets in the same Builddesc
* [#427 !411] Updates to build on Ubuntu 14.04 (PHP 5.5.9, Apache 2.4.7/20120211.27)
* [#430 !449] Quote dashes in search parse string
* [#446 !450] Fix tpc crash with loop counters on otherwise empty blocks
* [#447 !415] Generalize VALIDATOR_BCONF_LIST_DOT_NAME
* [#455 !451] Require ninja to build most platform components
* [#461 !427] Build a replacement for sendmail.fake
* [#473 !463] Allow flavors to work in INSTALL/PROG etc
* [#478 !474] Kill invword_counters and invattr_counters
* [#459 !427] Mail queue abstraction added to transaction handler.
* [!260] mod_image watermark improvements
* [!327] Fix verification of PL/SQL procs in trans
* [!366] Teach transaction_internal how to handle blobs (tutti)
* [!389] Rewrite search parser
* [!407] Redesign statpoints
* [!408] Logical implication for validators in trans
* [!412] Drop support for gcc < 4.8
* [!416] Added some basic tests for test_transaction_bind_bpapi
* [!418] Add support in webstress for tagging search queries
* [!425] Ninja rule for linting PHP code during installation
* [!426] Add support for a foreground color in mod_textgif
* [!428] Add support for dumping the docs in an attr in index_stats
* [!431] Escape '<' and '>' with \xNN in jsencode filter (tutti)
* [!432] latin1_to_utf8 template filter (tutti)
* [!433] Make chk_err() template configurable via settings 
* [!440] Use a single command to calculate the build version (git rev-list) instead of 2 (git log + wc)
* [!453] Implement a trivial tcpsend analogous to our udpsend. Needed because the nc version in centos 7 is working in a surprising manner
* [!454] Make platform build and pass tests on centos 7
* [!455] incdeps should be order only dependencies
* [!458] Fix a race condition in ev_popen reporting the wrong exit status
* [!462] Add a line directive to installed headers
* [!465] Stabilize platform ev_popen test
* [!466] Include scmcoord_contrib from template_errors regress tests.
* [!467] Less spammy regress-runner.
* [!468] Fix inconsistencies for how text is split in the indexer and search engine by using the same code for both.
* [!469] Untangle the word/variable `builddir` and also be consistent between make and ninja
* [!475] Cleanup daemon.c

## Build ##

### `CC` environment ###

The `CC` environment variable will now be honored if it specifies a compatible
compiler. `CXX` is ignored, instead the C++ compiler is derived from the C one.

Setting `compiler` in your top level Builddesc still has the highest priority.

### ninja ###

Most platform components can now only be built with ninja, see
[UPGRADE.md](UPGRADE.md) for how to proceed if you're still using make.

### Mod_image ###

New watermark positions are available:

* topmiddle
* middleright
* bottommiddle
* middleleft
* randommiddle
  Selects from the above four.
* random_middle_corner
  Selects any random position.

`random` keeps its old behaviour of selecting among the corners only.

In addition, there's a new `watermark_image_small` option, which will
be used for small images. Example bconf:

    *.*.mod_image.threshold.small_marker.width=250
    *.*.mod_image.threshold.small_marker.height=250
    *.*.test_mod_image.type.w640w.watermark_image=%DESTDIR%/img/watermark.png
    *.*.test_mod_image.type.w640w.watermark_image_small=%DESTDIR%/img/watermark_small.png

## mod_textgif ##

Added support for a foreground color.
Example for a light blue text:

    *.*.common.textgif.bg.r=255
    *.*.common.textgif.bg.g=255
    *.*.common.textgif.bg.b=255
    *.*.common.textgif.fg.r=16
    *.*.common.textgif.fg.g=16
    *.*.common.textgif.fg.b=255

## platform/scripts/fakemail ##

'fakemail' is a replacement for the 'sendmail.fake' script, meant to solve the
main issue of that script: How to deal with tests that send multiple emails.

Fakemail solves this by parsing the input into a sqlite database instead of
outputting it raw to a file. This allows you to, as long as there is some
distinguishing feature of each mail, use SQL to select out parts of the
email you want to inspect and match them with ${MATCH} as usual, regardless
of the order in which fakemail was invoked.

Fakemail can also be used to test error-handling via the error-injection
mechanism, though as of writing there exists no way to have mail_send()
add the required arguments.

When you include regress.mk the script will be available in ${FAKEMAIL}.
See platform/regress/fakemail and platform/regress/trans for examples of
how to build tests. Note that you must have an explicit rule to create
the database first if you're firing off multiple mails, or you may
run into a race condition due to the timing between when the database
and the tables are created.

Fakemail depends on perl-DBI and perl-DBD-SQLite.

## statpoints ##

Statpoints has now been completely redesigned in an attempt to prepare for a
future where we might want to push recorded events to more than one
destination. The previous implementation was based on libevent, and the write
loop was intertwined with the read loop making it difficult to extend
statpoints to handle an arbitrary number of event destinations.

The new design has done away with libevent and is now based on pthreads and has
multiple smaller components and it is our intention is that this will simplify
the task of extending statpoints in case we need to record events at multiple
places. Below there is a simple diagram of how the data flows through the new
implementation. Each box represents a separate thread and they all communicate
via simple queues.

                                            +--------------+
                                         +->| Redis Writer |
                                         |  +--------------+
                                         |
     +--------------+        +---------+ |
     | UDP Receiver |------->| Decoder |-+
     +--------------+        +---------+ |
                                         |
                                         |  +----------+
                                         +->| Fifo Ack |
                                            +----------+

Since the new design is meant to be a drop in replacement for the old
statpoints, there should be no changes in configuration needed for the new
implementation.

## templates ##

The template to invoke for reporting errors via chk_err() can now be configured using settings
per application, field and error. The field key is the param name passed down to trans and error
is the actual non-translated error code returned from trans.

    *.*.chk_err_settings.error_message.1.keys.1=appl
    *.*.chk_err_settings.error_message.1.keys.2=field
    *.*.chk_err_settings.error_message.1.keys.3=error
    *.*.chk_err_settings.error_message.1.ai.subject.ERROR_SUBJECT_BLOCKED.value=template:modules/input_error_blocked.html
    *.*.chk_err_settings.error_message.1.ai.body.ERROR_BODY_BLOCKED.value=template:modules/input_error_blocked.html

If no configuration exist the default template will be chosen as per before. If a template is
passed down as the second parameter it will take precedence and be chosen even if a setting would
match.

Once consumed, the error will be removed from the context and thus subsequent calls for the same
error within the same template will yield blank results.

In order to configure on the actual error from trans and to retrieve the payload (if any) the 
translation via lang() in PHP needs to be removed and the payload sent separately as a simplevar
rather than appended to the translated string. The patch below against blocket_application.php
alter the behaviour like this and push translation into the template layer instead.

 ```
 diff --git a/php/common/include/blocket_application.php b/php/common/include/blocket_application.php
 index bedfeea..4f4ca61 100644
 --- a/php/common/include/blocket_application.php
 +++ b/php/common/include/blocket_application.php
 @@ -492,10 +492,10 @@ class blocket_application extends blocket_filter {
  						if (!empty($this->messages[$tmpkey]))
  							$simple_data['msg_' . str_replace('.', '_', $tmpkey)] = $this->messages[$tmpkey];
  					} else {
 -						$simple_data[$err_key] = lang((end($codes) == $codes[$tmppos] ? "" : (end($codes) . "_")) . $codes[$tmppos], $this);
 +						$simple_data[$err_key] = (end($codes) == $codes[$tmppos] ? "" : (end($codes) . "_")) . $codes[$tmppos];
  						$simple_data[$err_key] = str_replace('#caller', $this->caller->to_string(), $simple_data[$err_key]);
  						if (!empty($this->messages[$tmpkey])) {
 -							$simple_data[$err_key] .= ' "' . $this->messages[$tmpkey] . '"';
 +							$simple_data[$err_key . "_payload"] = '"' . $this->messages[$tmpkey] . '"';
  							$simple_data['msg_' . str_replace('.', '_', $tmpkey)] = $this->messages[$tmpkey];
  						}
  					}
 ``` 

## trans ##

### VALIDATOR_BCONF_LIST_DOT_NAME ###

VALIDATOR_BCONF_LIST_DOT_PATH(node, path, err) has been added which is a generalization of VALIDATOR_BCONF_LIST_DOT_NAME.

Example usage:
Assume a configuration similar to:

    *.*.somenode.accepted_value.1.some.path=foo
    *.*.somenode.accepted_value.2.some.path=bar
    *.*.somenode.accepted_value.3.some.path=naming is hard

adding:

    VALIDATOR_BCONF_LIST_DOT_PATH("somenode.accepted_value", "some.path", "SOME_ERROR")

to a parameter will accept the values "foo", "bar" and "naming is hard"

### P_IMPLY flag ###

Usage

Implication (P_IMPLY) is a new validator type which can be used when you have
two optional parameters, but when the first one is present then the second one
must also be present.

Example:

You have two parameters, remote_addr and token. Remote_addr should be optional
since it can always be present. However, when token is present then remote_addr
must also be present.

This can be expressed as following:

	static struct c_param baz[] = {
		{"token;remote_addr",   P_IMPLY,         "v_foo;v_bar"},
		{NULL}
	};

### trans/mail: mail_queue ###

The addition of the ev_popen process limiter in 2.3.0 (ev_popen.limit) made some
transactions racey because they assume that no callbacks can happen until they
return to libevent. This is true, as long as the limit is not hit. When it is,
trans will pump events to try and make room for new popens, causing the race.

One such transaction is trans_spamfilter.c which calls mail_send() in a loop,
counting the number of times it's called, and then use that total to determine
when all mail have been sent by decrementing it towards zero in the callback.

To make it easier to bulk-send mail we've introduced a mail queue abstraction to trans:

	struct mail_queue *mq = mail_queue_new(cs->ts, NULL);

	(.. some loop which creates new mail_data structures ..) {
		struct mail_data *md = zmalloc(sizeof(*md));
		/* setup params ... */
		md->command = state;
		md->callback = some_local_cb;
		md->log_string = cs->ts->log_string;
		/* OLD: mail_send(md, "mail/factory_mail.txt", NULL); */
		mail_queue_add(mq, md, "mail/factory_mail.txt", NULL);
	}

	mail_queue_flush(mq, -1);
	mail_queue_destroy(mq);

So instead of just calling 'mail_send()' you call mail_queue_add() with the mq struct as the
first argument, and the rest are the same as for mail_send.

The 'mail_queue_flush()' call will flush (send) the mail in batches.

While the mail_queue will by default use its own event base (md->base is in fact overwritten),
it WILL obey the (global) trans 'ev_popen.limit'.

See also platform/regress/trans/trans_test_mail_queue.c for an example, or the
implementation itself in platform/bin/trans/mail_queue.* for details.

## Ubuntu 14.04 LTS support ##

The platform now builds and pass its regression suite on Ubuntu 14.04 LTS. This
distribution runs Apache 2.4 and PHP 5.5.9 which our modules now build and run on.

See https://scmcoord.com/wiki/Repo for information about our package repository.

# Version 2.7.3 [2014-08-26] #

* [!447] Apply index_stats id filter to doc array
* [!448] Make sure to use new value for suborder on index update.
* [!456] Only install to /plsql in regress/gcov flavors.

# Version 2.7.2 [2014-08-05] #

* [#357 !406] Cleanup validators for abs and rel in trans_at
* [!438] Change search init script engine name handling (from scmv)
* [!442] Add CMD_PREFIX to regress/common Makefiles (platform internal)
* [!443] Plug memory leak in vtree_json_vtree test (platform internal)
* [!444] Fix a crash in mod_image on failing to load a PNG (tutti)

# init/search #

If $0 of the search init script begins with S or K, remove the first three
characters and use the rest as $ENGINE. Solves a problem when being symlinked
from rcN.d

# Version 2.7.1 [2014-07-14] #

* [!413] Fix compiling with clang.
* [!414] Preserve arguments used for build-build.pl when invoked automatically.
* [!417] Clean up binary search in serialized bursttrie.
* [!421] Add make kill to platform Makefile
* [!423] Allow trans get_connection to find the correct connection from the cache
* [!429] Index offset merge fix
* [!430] Remove state->action incrementing in factory action FACTORY_AT
* [!434] Add a basic test for shadow_vtree
* [!435] Fix segfault in trans when NULL values were assigned to parameters with the P_EMPTY bit set.
* [!437 #458] Fixed 'FACTORY_TRANSLATE ignores the extra "CONTEXT" line if an error is raised from a nested stored procedure.'
* [!439 #460] Execute search-real-bench tests only if the DATA directory exists.

## trans ##

Currently trans accepts parameters to have NULL values. This happens if the 
parameter is passed without colon. If the param has the P_EMPTY flag set, then
a check similar to strlen(param->value) is performed leading to segfault.

Also a log line has been added if a parameter has a NULL value. It can be 
argued that NULL values should not be permitted. This would be true and 
trivally implementable for all cases but the "commit" param, where a bit more
attention is required. Before depracating such behaviour this log is added in 
order to help sites monitor if such calls are made.

## trans config ##

Mentioned by LBC in the forum
 [https://scmcoord.com/forum/index.php/topic,94.0.html]

The connection cached internally was identified by 6 keys :
 (1) hostname
 (2) data backend user
 (3) data backend user password
 (4) database name
 (5) type of backend ( only PGSQL is implemented now )
 (6) internal flags

So a configuration of the type:

 db.pgsql.slave.engine = PGSQL
 db.pgsql.slave.name = blocketdb
 db.pgsql.slave.hostname = 10.0.19.4
 db.pgsql.slave.user = trans
 db.pgsql.slave.password =
 db.pgsql.slave.max_workers = 25
 
 db.pgsql.queue.engine = PGSQL
 db.pgsql.queue.name = blocketdb
 db.pgsql.queue.hostname = 10.0.19.4
 db.pgsql.queue.user = trans
 db.pgsql.queue.password =
 db.pgsql.queue.max_workers = 250

Will always return 25 workers for ''pgsql.queue'' since this node will be identiefied
as ''pgsql.slave''. In the proposed solution, connection is cached on ''config''

Also transinfo returns the number of max_workers set and ''config'' is added on
connection info. If you are parsing these lines in monitoring tools, you might want to
update them. The information returned now looks like:

 connection:<node> <db_engine> (<user>@<host>)

## trans factory ##

Since FACTORY_COND was introduced, actions are handled via state->actions which is responsible for walking through the actions list.

Also register the AT commands using the at_register_storage interface.


## documentation / examples ##

This is intended as a simple example of setting shadow_vtree and filter_state.

The file located at ${PLATFORMDIR}/regress/shadow_vtree/shadow_vtree.c is
presenting a basic scenario of use.

There is a root bconf_node that is set global. It is assumed that the bpapi
should not use the global node directly, so a local node has to be set and the
root has to be merged via shadow_vtree. A simple filter has been written that
reads from the filter_state set on the local conf. A simple template verifies
access to the data structures.



# Version 2.7.0 [2014-06-23] #

## IMPORTANT NOTES ##

The platform project is now a subproject to the 'coord' group in Gitlab.
Note therefore its new URI 'git@scmcoord.com:coord/platform.git'

This is the first release after a Gitlab upgrade which caused all issues and MRs
to be renumbered. This means issue- and MR numbers mentioned for versions =< 2.6.1
no longer accurately maps onto the Gitlab database.

### Availability ###

You can download the code from here:
  git@scmcoord.com:coord/platform.git

Current version tagged as:
  platform/2.7.0

### What's new ###

* [#130 !324] Search index size optimization (index offset)
* [!325] Rewrite of script/build/in.pl variable substitution loop
* [!349] Improved gperf enum.
* [!355] HTML index generated for static analyzer report (platform internal)
* [!360] Upgrade OpenBSD queue.h from v1.27 to v1.38
* [#402 !362] gcov flavor, flavor selection for build-build script and coverage reporting.
* [#405 !363] Improved test coverage of lib/common (+ !370) (platform internal)
* [#405 !364] Removed the FTP library.
* [!374] Change install.mk to print full target path to console, not echo for-loop.
* [!377] Add FA_COND, a type of conditional, to the trans factory (tutti)
* [!379] Removed much magic from mod_templates uri to template matching code.
* [!380] Add a "full" flag to maildecoder to decode =XX characters.
* [!382] Add -fstack-protector to compile flags.
* [!384] Fixed memory leaks in regress common tests (platform internal)
* [!386] A stab at testing id_prefix and id_prefix.before (platform internal)
* [#422 !387] Removed dead BSAPI test (platform internal)
* [!388] Fix trans AT reload crash related to the redis-based queue.
* [!390] build-build: Flavor level cflags
* [#425 !392] Upper boundary is now included in the last slice
* [#426 !393] Renamed trans database configuration entry 'pwd' to 'password'.
* [#424 !394] Skipping sort slice header with zero count
* [#373 !395] Removed superfluous definitions of _GNU_SOURCE
* [#441 !396] Statpoints now generates CRIT messages when it fails to write to Redis
* [!397] Fix for a bug in how FA_OUT (trans factory) handles blobs. Added tests.
* [#398 !398] Cleanup so clang won't complain about unused functions. Stop using -Wno-deprecated-register globally.
* [!399] INFO log when trans commands are run as validate-only (i.e without commit:1) (tutti)
* [!401] Add FA_AT() to factory transaction in order to register at jobs
* [!402] Add the ability to use at_register_(cmd|printf) commands for redis
* [!404] Adding some basic synonyms tests (platform internal)
* [!405] Use call_template_auto() in &chk_err for better lang support
* [#439 !409] Update trans help to handle more validator types

#### Search ####

Optimizations has been made to decrease the index size.
Tests on a sizeable production index show a ~25 % decrease in size,
which propogates directly to search engine memory usage.
The new format is also better prepared for 64 bit list_ids and other
upcoming features.

Indexing might however be slightly slower since documents must now be sorted
before store.

#### Build ####

Flavor selection:

The build-build.pl script now supports the repeatable options '--with-flavor' to
force only those flavors to be built, or '--without-flavor' to exclude some flavors.

Flavor level cflags:

You can set flavor level cflags with `cflags:flavor[]` in `CONFIG`.
The intended use is to add `-DNDEBUG` to your prod flavor.
These flags are used in addition to any set by cflags, it's not possible
to override them in a specific directory.
It should be used sparingly, in general we recommend running the same code
in regress and production.

gperf:

The gperf enum / switch feature has been improved. You you can now put the
strings directly in your source file. You still have to add a specialsrcs
target for it in the Builddesc however. For an example see regress/gperf_enum/.
You can even add more data to the lookup if required.
Note: it's very important to have dependencies setup correctly, since by
default the source line number is used to tie together the switch and lookup.
If line numbers change but the gperf file isn't updated they will get out of
sync.


#### Transaction factory ####

FA_AT:

Add FA_AT() to factory transaction in order to register at jobs:

Of course one can use FA_TRANS() and call cmd:queue with the desired params
in order to solve that problem. In that sence this macro helps to write 
cleaner and simpler factory transactions.

It takes all the arguments that at_register_cmd does and also adds the possibility
to user redis instead of the db via the FAAT_REDIS flag.

A call would look like:
	FA_AT("template", "sub_queue", "info", seconds, critical, FLAG)

where FLAG can be either one of FAAT_DB or FAAT_REDIS

FA_COND:

ADD FA_COND() to factory transaction in order to load one action during execution.

It takes a function as argument. The function should return a pointer to a struct 
factory_action or NULL if no action should be loaded. For documentation refer to:
	https://scmcoord.com/wiki/Trans_factory#Actions

#### Transaction handler ####

Add the ability to use at_register_(cmd|printf) commands for redis:

Backwards compatibility is maintained so no existing calls to at_register_cmd()
or at_register_printf() have to change. These two interfaces continue storing the
at jobs at the database as usual.

Two new interfaces have been added, at_register_redis_cmd() and at_register_redis_printf()
with the same list of arguments as their pre-existing equivalents.

Also two general interfaces have been added at_register_storage_(cmd|printf).

at_register_storage(cmd, seconds, sub_queue, info, critical, storage)
at_register_storage_printf(seconds, sub_queue, info, critical, storage, ...)

storage can be either of AT_DB or AT_REDIS

example usage:
at_register_storage("cmd:somecommand\ncommit:1", 60, "some_queue", "info", 0, AT_DB);


#### Platform internal ####

A new 'gcov' flavor is now available. It must be explicitly requested by passing FLAVOR=gcov to ./compile.sh

As can be gleaned from the name, this flavor builds everything instrumented for coverage analysis with gcov.

$ FLAVOR=gcov RUNTESTS=1 ./compile.sh
$ mkdir -p coverage && gcovr --root . --html --html-details --exclude=build/ --output=coverage/platform.html

This uses the tool gcovr (http://gcovr.com/) to generate a pretty HTML report.

A jenkins build machine can use the build line 'FLAVOR=gcov RUNTESTS=1 RUNGCOVR=1 ./compile.sh' to
generate XML (build/<flavor>/coverage.xml) suitable for processing using Cobertura.



# Version 2.6.1 [2014-04-15] #

### Availability ###

You can download the code from here:
  git@scmcoord.com:platform.git

Current version tagged as:
  platform/2.6.1

### What's new ###

* [#570 !1576] Added test for php_bsearch module.
* [#544 !1274] loop filter on undefined key does not enter the block
* [#532 !1278] info header paging_last is not returned on a paging search if there are no more documents
* [#506 !1293] If the user pipes more than ENGINE_INPUT_BUFSZ chars to the search engine's keepalive port the remainder will be discarded
* [!1722] Add test coverage for the lib/common similar_text implementation.
* [!1716] Add test coverage for the lib/common Levenshtein implementation.
* [!1672] Reconnect legacy template tests
* [!1668] Add a shell script to access in.conf variables.
* [!1666] Fix overflow in search query parser
* [!1649] format_date should now figure out DST by itself
* [!1630] Fix for memory leak in sredisapi:session_redis_send()
* [!1604] Properly drain sql workers.
* [!1545] Fixed deps for mod_image and mod_bsconf
* [!1540] Error on unknown parameters in Builddesc.
* [!1494] Add WEAK to macros.h, expanding to __attribute__((weak))
* [!1466] Search paging with lim:0 should return 0 documents
* [!1388] Compile using devtoolset
* [!1304] A simple blocking transaction was added on the tree which can be used a reference. No need to use state machines all the time 
* [!1296] generate the correct parse_string after calling ParseNodeEmpty::get_parse_string
* [!1294] Interval search, do not return any documents in searches for the previous of the first document. Return parse error for intervals outside the 32bit integer range.

Build
=====

You can now access the in.conf variables from shell script by sourcing
platform/scripts/build/regressvars.sh



# Version 2.6.0 [2014-03-06] #

### Availability ###

You can download the code from here:
  git@scmcoord.com:platform.git

Current version tagged as:
  platform/2.6.0

### What's new ###

* [#342 !963] Require GCC 4.4 or clang 3.2
* [#388 !967] REGRESS_HOSTNAME no longer short. REGRESS_DOMAIN extraction better.
* [#391 !932] Fixes for easier RPM building with ninja.
* [#531 !1035] Remove code related to the unused 'parent' argument from statpoints/stat_log.
* [#339 !1274] Move -Wmissing-declarations from covering C only to C and C++
* [!300] Add quotes around filenames in mime_inline_file(), and recognize '*.jpeg' as image/jpeg.
* [!759] Replace {js,json}_encode template functions with {js,json}encode filters.
* [!776] Find postgres includes and libs using pg_config.
* [!880] Better error handling in search lexer (add token LEXER_ERROR)
* [!1064] Test time related functions in a few different time zones (regress).
* [!1110] Remove possibility of 'useragent' NULL pointer dereference in mod_redir.c
* [!1200] Distance and relevance sorting and sort slicing for search engine.

Build
=====

1. GCC 4.4 or later is required, and C++ code is now compiled using C++11,
although not fully so since we still support GCC 4.4, which isn't fully
compatible.

2. When building with ninja, intermediary files have been separated for the
final installation. This makes the installation directory much more clean,
the goal is that you should be able to package `build/prod` directly as
your rpm root.

Intermediary files now goes into `$buildpath/obj/$flavor`, installed files
are kept at `$buildpath/$flavor`.

If you need files at a certain path but don't want to packages them you
can install them to `builddir/path`, e.g. pgsql modules sql files can be
installed to `builddir/pgsql/modules` and you'll then find them at
`build/obj/$flavor/pgsql/modules`. Another options is to package them.

If you spend a little time to clean up you should be able to modify
your `rpm/blocket.spec` and set `BuildRoot` to `%{cwd}/build/prod`.
Then you can remove the rsync.


Relevance sorting
=================

This is a partial import/reimplementation of the relevance search done by
the now defunct Blocket Shopping. It's included in the release by accident
because the mechanisms to implement relevance search were relevant for
implementing distance search.

The details of the implementation and how to set it up are described
in [docs/relevance-sort.md](docs/relevance-sort.md).

Sort slicing
============

Our sites are usually designed around list time being a valuable attribute
that lots of sites monetize early in their growth. This creates a problem
when letting users sort the search results on something other than list time.
On one hand, we want the users to refine searches on other attributes, on the
other hand the more those sorting features get used, the less valuable list
time is and sites can't sell bumps. Sort slicing is a compromise to eat the
cake and have it too.

The idea is to take a search result sorted on some attribute, find some
boundaries in the sort attributes and then group the search results on those
boundaries into slices of roughly equal size and then sort by list time within
the slices.

For example. We perform some query that sorts on distance (see below) returns
1000 results and we ask for 5 slices. We look at the result number 200 - it's
1km from us, result number 400 - 2km, 600 - 5km, 800 - 10km. The slicing
operator will sort the search result on list time first everything within 1km
(even if it's the first 250 ads that are actually within that distance), then
sort on list time everything that's under 2km, etc. New info headers will be
returned to indicate the breaking points for the slice boundaries.

The syntax is simple: `sort_slice:<number of slices>:<sort order><nulls first/last>`

Example:

 0 lim:100 sort_slice:3:++ sort:++:coord_x:- category:10 coord_x:120-

This will search all ads in category 10 and coord_x >= 120. It will then sort
the result on coord_x in ascending order, then slice the result in three slices.

The output will contain info headers like this:

 info:slice:0:boundary_upper:130
 info:slice:0:count:10
 info:slice:1:boundary_lower:130
 info:slice:1:boundary_upper:140
 info:slice:1:count:11
 info:slice:2:boundary_lower:140
 info:slice:2:count:16

`boundary_upper` and `boundary_lower` specify which boundaries were chosen as
the breakpoints between the various slices. `count` specifies how many documents
ended up a particular slice. In this case slice 0 contains 10 documents with
coord_x in the [0,130) range, slice 1 contains 11 documents with coord_x in
the [130,140) range and slice 2 contains 16 documents in the [140,infinity) range.

Sorting of the slices only has minimal relevance since the same thing can be
accomplished by reversing the sorting of the attribute. It will slightly change
the results because of minor differences in how the boundaries are chosen. It's
recommended that slices are always sorted in ascending order (`++`). Because
of a peculiarity of how normal sorting is done, the second sort order argument
(the second `+` or `-`) has no effect since there will never be any empty sort
values.

Distance sorting
================

It is now possible to sort the search result by distance.

The algorithm is a simple unit agnostic flat-earth model and intended to be
used with the slicing operator to group the results by distance, but still
sort on list time. It can be used for distance sorting too, but the further
away you get the more incorrect the results will be.

Example:

	0 lim:17 sort_slice:5:++ distsort:100,100 category:-

This will slice up the result in 5 circles around point 100,100 (in some
arbitrary coordinate system).

To configure distance sorting the following two keys need to be set in the
search engine config (the .r key is optional):

distance_search.attr.x=<name of attribute with x coordinate>
distance_search.attr.y=<name of attribute with y coordinate>
distance_search.attr.r=<name of attribute with coordinate precision>

This tells the search engine which attributes (they need to be indexed) to
look in for the coordinates of each document. The `r` attribute is the
imprecision of the coordinates. The attributes have to positive integers.

The distance to each document is simply calculated with
`sqrt((attr_x - query_x)^2 + (attr_y - query_y)^2) + attr_r` (like mentioned
above: flat earth model). This means that the value of r must be in compatible
units depending on the coordinate system used to position the user and the
documents. The search engine doesn't make any decisions about the units here,
it's up the the caller and indexer to figure out the units and coordinate
systems.

The imprecision (`r`) of the coordinates in the documents is always added to
the result so as to give higher priority to documents with more accurate
precision. It can be used for example for documents where we only know the
region and the coordinates are set to the centroid of the region. In that
case the `r` attribute should be set to the approximate radius of the
region (assume a frictionless spherical region of uniform density). Every
document within that region with better precision will get closer distance
to a query from within the region. This is by design to encourage users to
provide better positioning data. Bad coordinate precision = bad spot in the
result list.

Index stats words
=================

index_stats now has support to dump out the word -> docs/docpos array for a word
with the -W option.

Internal redesign of the indexer library
========================================

All functions that add text and words to the index in libplatform_search have had
their API changed to be simpler to use. If you use any of those internally (most
sites shouldn't), you'll have to read through the changes to figure out the new API.

Search index format change and size growth
==========================================

The internal format of the index has changed (version bumped to 0x20 from 0x10). This
is handled automatically by a new search engine or index_merge. When running a new
search engine on an old index the search engine will take some additional memory
to convert the format, this can be avoided by running index_merge to copy an old
index to a new one.

The reason for the version bump is that the docpos struct in the index has grown
from 4 to 6 bytes. This shouldn't be a problem since docpos is usually well
compressed and only a small part of the index, but just to make sure that the
index doesn't grow too big for the memory on search engines it is recommended to
run index_merge manually on an index from production to see how much it grows.

Sqlite template filters
=======================

A new template filter has been added - sqlite_exec, available through
platform_sqlite_templates library (only available when you build with ninja).
It's called as:

	<%|sqlite_exec(<path to sqlite file>, <variable prefix>, <query>)|

The first argument is the sqlite file, second argument is the variable prefix
for the variables that will be set to the columns returned from the query, the
query is an SQL query. The variables are also accessible through the
`@loopfilt("sqlite_exec")` loop filter for easier access.

There's no persistency of open sqlite databases or any optimizations. This is
intended to be used for simplifying building regress tests is probably a
terrible idea to use in production.



# Version 2.5.1 [2013-12-05] #

### Availability ###

You can download the code from here:
  git@scmcoord.com:platform.git

Current version tagged as:
  platform/2.5.1
Build
=====

C++ is now compiled using C++11, although not fully so since we still
support gcc 4.4, which isn't fully compatible

### What's new ###

* [#394 !310] Use the proper "X-Forwarded-Proto" header instead of "X-FORWARDED_PROTO" for determining the forwarded proto.
* [#412 !596] Don't pass in 2000 second read timeout into the search engine from the init script which overrides all configuration.
* [#449 !681] Fix a concurrent access bug in global filter states that can be triggered when there are enough filter states allocated.
* [#405,#411 !739] filter_state is now relatively easily available in trans.
* [!307] Add a .gitignore to the platform tree (internal)
* [!365] buildversion.sh: Cache the revision number using top-most revision as key (build performance)
* [!367] Added template functions/filters for redis: ZREM, ZRANK and LRANGE
* [!505] Added support for hooking up a 'next url' function to mod_redir (bomnegocio)
* [!549] Fixed crash in qp_mail filter when validly called without multipart argument.
* [!595] Add an optional param for cmd:at - `reg_sync`. When set to false, it will make the transaction not wait for the at job to register and will not return trans_queue_id.
* [!603] Improved documentation in bpapi header file.
* [!690] Support count_each from search engine in mod_list.
* [!712] Support for running the search filters without a functioning filter_state. This is a bad idea, but some sites do it.

filter_state in trans
=====================

There's now a general handling of filter_state inside of trans. It
wasn't possible to make it fully transparent, but it should be
relatively easy to retrofit into old code.

All factory transactions will have filter_state, also trans_mail and
sql_worker templates that weren't called with an externally allocated
bpapi. Those that need filter_state and were called with an externally
allocated bpapi will need the changes below.

Where the old code allocated a struct bpapi and intialized it with
BPAPI_INIT:

    struct bpapi ba;
    BPAPI_INIT(&ba, NULL, pgsql);
    sql_worker_template_query(..., &ba, ...)
    bpapi_free(&ba); /* or bpapi_output_free(&ba); bpapi_simplevars_free(&ba); bpapi_vtree_free(&ba); */

is equivalent to:

    struct command_bpapi cba;
    command_bpapi_init(&cba, cmd /* or NULL if there's no access to current struct cmd */, CMD_BPAPI_HOST, NULL);
    sql_worker_template_query(..., &cba.ba, ...)
    command_bpapi_free(&cba);

When initialized with BPAPI_INIT_APP:

    BPAPI_INIT_APP(&ba, host_bconf, "controlpanel", buf, pgsql);

it can be replaced with:

    command_bpapi_init(&cba, cmd, CMD_BPAPI_HOST, "controlpanel");

The prototypes are:

    command_bpapi_init(struct command_bpapi *cba, struct cmd *cmd, int flags, const char *appl);
    command_bpapi_free(struct command_bpapi *cba);

The arguments are:

 * cba - the command_bpapi we're working with.
 * cmd - the struct cmd for the current trans command being run. Can
         be NULL if we don't have access to it, but the code is more
         efficient with it.
 * flags - various flags, see below.
 * appl - application for which the bconf is fetched (same as the
   third argument for `BPAPI_INIT_APP`).

The flags argument has these flags:

 * CMD_BPAPI_HOST - use host_bconf as the backend bconf
 * CMD_BPAPI_TRANS - use trans_bconf as the backend bconf
 * CMD_BPAPI_NO_OUTPUT - don't handle output (NOTE: the caller is then
   responsible for calling bpapi_output_free)
 * CMD_BPAPI_NO_SIMPLEVARS - don't handle simplevars (NOTE: the caller
   is then responsible for calling bpapi_simplevars_free)
 * CMD_BPAPI_SIMPLEVARS_HASH - Use hash_simplevars instead of the
   default pgsql_simeplvars.

Notice that the simplevars used in command_bpapi default to pgsql
simplevars with an option to change it to hash_simplevars if
required. Feel free to make a merge request with additional flags for
other types of simplevars if it's needed for your application.


# Version 2.5.0 [2013-10-17] #

### Availability ###

You can download the code from here:
  git@scmcoord.com:platform.git

Current version tagged as:
  platform/2.5.0

### What's new ###

* [#393 !315] Moved v_bconf_keyval so it will be included in prod build.
* [#395 !314] The redis rpop template function now correctly accesses the master node.
* [#46 !320] parse_query_string code extended to support bconf lists of whitelisted values.
* [#392 !321] Added support for generating progressive jpegs in mod_image.
* [#401 !372] Allow comments anywhere in Builddesc files. Improved error handling in build-build.pl.
* [!304] Explicit rule dependecies for ninja builds.
* [!309] Add set_secure_cookie_with_expiry_seconds.
* [!311] Cleaner dependency generation for library includes. Shrinks generated ninja files by 60-70%.
* [!319] Define _GNU_SOURCE globally.
* [!325] HTML5 and de facto usage pretend that latin1 is actually win1252. Roll that into the utf8 conversion functions.
* [!327] Error message gave wrong information in xcalloc.
* [!354] Make index_ctl create all the intermediate directory components when creating directories.
* [!357] Collect targets into variables that can be used as source files in other descriptors.
* [!362] Paranoid NULL checks when printing values in transinfo.
* [!366] Support large lists of objects to the linker in ninja by using rsp files.
* [!370] Local function in mod_list called `replace_list_vars_local` for local replace_args.
* [!403] Print the pid of the timed out forcibly killed child in the search engine.
* [!432] Allow fd_pool to be used with UDP.
* [!433] Python interface for bsapi
* [!487] Default the db_delay in the example indexer.
* [!500] Use-after-free fixed in indexer exit.
* [!531] `http_req_result` now understands more well-known HTTP error codes.

parse_query_string support of enumerated whitelists.
======================================================

The `parse_query_string` used by mod_templates, qs_sq and others has
been extended to support lists of whitelisted values.

In addition to regular expressions to validate vars, the code now also
supports simple lists through the 'allow' node.

Regex:
	common.template.42.vars.lang.regex=^(sv|en|de)$

List:
	common.template.42.vars.lang.allow.0=sv
	common.template.42.vars.lang.allow.1=en
	common.template.42.vars.lang.allow.2=de

The use of both approaches for the same variable is allowed, should
that ever make sense.

Progressive/interlaced images in mod_image.
===========================================

mod_image can now be instructed to generate progressive JPEGs (or
interlaced PNGs) by setting the 'interlace' flag:

	mod_image.type.cfgname.interlace=1

For larger images, progressive JPEGs are usually a bit smaller than
their baseline compressed counter-parts, but require slightly more CPU
to (de)compress. Beware of browser issues with using this format.
While all modern browsers should display the same image eventually,
those that do not fully support progressive updates may show nothing
until the whole image is loaded.

Python
======

This release contains a python interface for bsapi. It requires Python
3 to function, and is slightly awkward to use, writing a wrapper
object is recommended.

The python library is only buildable with ninja, Makefiles are not
provided.

Common Library
==============

A string interning library has been added, called stringpool.

There's now a more object-like interface for bsapi.

Ninja - target collection.
==========================

There is now functionality to collect install targets into a variable
accessible internally by build-build.pl. For example if a bunch of css
or js files are installed from different Builddescs and those need to
be minified, the installed targets can be collected in one global
variable and then that variable can be used as an input for a script.
This is a build-build.pl internal variable that can only be used as
a source for other targets, the variable definition is not output
into any generated files.

See [docs/COMPILING.md](docs/COMPILING.md) under `collect_target_var`
and also the test in platform/regress/{build-system,conf,charmap} for
how to use this in practice.

# Version 2.4.3 [2013-09-03] #

### Availability ###

You can download the code from here:
  gitolite@scmcoord.com:platform.git

Current version tagged as:
  platform/2.4.3

### What's new ###

* [#297 !316] Added a warning message when X-Forwarded-For is set but remote addr is not in list of approved proxies.
* [!317] The t-% rule, deprecated in 2.3.0, has been removed from mk/regress.mk
* [#396 !336] Fix rebuilding of in files for newer versions of ninja.
* [#328 !322] The template_stress test has been reconnected to platform tests.

# Version 2.4.2 [2013-08-08] #

### What's new ###

* [!135] Sorting in templates can now be based on locale.
* [!298] Properly handle conversions of names in value and glob loops in tpc. Fixes a compiler crash.
* [!301] In ninja, build mklangmap as a tool, not as a prog.
* [#286 !302] Make the invars.sh script also internally set each variable it outputs.
* [!303] Various cleanups in how tpc output is generated to make it more readable.
* [!306] An error will be emitted if USE_TCMALLOC is used without TCMALLOC_PATH.
* [!312] mod_textgif intialized a hash table incorrectly which led to sometimes hanging apache on large strings.
* [!313] If &redirect() is called multiple times, the last call will win. Errors will also be emitted.


# Version 2.4.1 [2013-07-05] #

### Availability ###

You can download the code from here:
  gitolite@scmcoord.com:platform.git

Current version tagged as:
  platform/2.4.1

### What's new ###

* [!296 #368] A tool to discover missing ninja dependencies. Fix a bunch of missing dependencies.
* [!297] Workaround a bug in PHP triggered by recent reorganization of one of our functions.


# Version 2.4.0 [2013-07-03] #

### Availability ###

You can download the code from here:
  gitolite@scmcoord.com:platform.git

Current version tagged as:
  platform/2.4.0

### What's new ###

* [#47 !286] Added tests for validator P-flags (regress)
* [#78 !281] init_func is redesigned and is called filter_state now.
* [#109 !271] Redis backed at queue implemented.
* [#159 !260] Make call_template in php_template dumber, don't try to detect 'lang'.
* [#193 !248] A new function, set_xerr_abort, has been introduced. With this function you can set a flag that causes xerr/xerrx to call abort instead of exit.
* [#253 !243] Make platform source UTF-8.
* [#294 !241] Tags for search queries for easier optimization of search queries.
* [#305 !266] aes_encode functions takes nonce and generates IV using cipher forward-function.
* [#323 !254] tokyocabinet library removed from platform directory.
* [#329 !284] Templates now use pcre
* [#332 !259] Extend available_weeks_calendar with day support.
* [#336 !258] Split pgsql filters from main ones.
* [#339 !256] Update codebase to support -Wmissing-declarations
* [#340 !267] Remove the 'ambiguous' REGRESS_HOST from defvars.
* [#343 !261] Set the language standard for c to gnu99, globally.
* [#344 !262] Prepare to remove magic rules from mod_templates uri to template mapping.
* [#346 !276] Fix issue where the loopfilt would loop once on results with only info-lines.
* [#349 !280] Copy return value from setlocale() for safety.
* [#350 !285] Change trans_conf filter to take timeout as first argument.
* [#351 !277] Upper/lower tables extended once again.
* [#352 !283] Don't crash in index_merge when indexes don't have to_id set.
* [#353 !279] Fix issue where P_OPTIONAL was not respected when used in conjunction with VALIDATOR_OTHER (Kapaza)
* [#358 !278] config_init_file() no longer support inline # comments
* [#359 !282] Fix mod_image crash when dimensions were scaled down below 1px.
* [#363 !289] One more attempt at getting the code generation right in tpc in some corner cases when locals are emitted.
* [#365 !293] Support for "endless scroll" in the search engine. The new P: query can support a continuation of a search even in case of a pivot being deleted.
* [#369 !287] When building with ninja, be more careful with .in dependecies so that fewer files will get rebuilt after a commit.
* [#383 !290] Fix some memory leaks in filter_state.
* [!247] Creation of MD5 checksums for procedure validation is now created in trans instead of sql template.
* [!255] Upgraded tree.h to revision 1.10.2.2 from freebsd.org
* [!264] Show the line number in the vimdiff command outputted by match.pl
* [!269] Do the safe thing on errors in php_templates simplevars loops instead of causing a later crash.
* [!270] Use the same compare function when comparing tpvars and normal strings in templates. Might lead to subtle differences between what's larger or smaller than. Equality not affected.
* [!272] cmd:help output is now sorted.
* [!294] Correct the way static libraries are created to avoid ar overwriting template functions with filters and vice versa.
* [!295] The php_bconf module can now be configured with extra bconf keys for an application.

Search Tags
===========

Search queries can now be tagged with the new keyword `_tag:<some tag>` which will
be added to the slow query output and will also be included in the timer output on
search engine reload/stop.

For example, all queries tagged with `_tag:foo` will be accumulated in the timer output as:

    2013-04-23 14:37:14 search: (INFO : Engine 8223 (parent: 8221)): timer query/foo count:5
    2013-04-23 14:37:14 search: (INFO : Engine 8223 (parent: 8221)): timer query/foo total:0.001
    2013-04-23 14:37:14 search: (INFO : Engine 8223 (parent: 8221)): timer query/foo min:0.000
    2013-04-23 14:37:14 search: (INFO : Engine 8223 (parent: 8221)): timer query/foo max:0.000
    2013-04-23 14:37:14 search: (INFO : Engine 8223 (parent: 8221)): timer query/foo average:0.000

Configurable xerr/xerrx behavior
================================

xerr/xerrx can now call abort instead of exit. This is intended to simplify
debugging in certain circumstances. Following is a set of instructions to
enable this feature in trans and mod_template.

Trans:
The default behaviour of xerr/xerrx, which is to call exit, can be overriden
during runtime by using the control command in trans:

    printf "cmd:control\naction:abort_enable\ncommit:1\nend:1\n" | nc <trans host> <control port>

To switch back to regular exit functionality again, call the abort_disable
action with the control command.

The default behaviour can also be overridden during the startup phase of trans.
Add the following to trans.conf to enable the abort functionality:

    xerr_abort=1

mod_template:
A new bconf directive is used to configure mod_template to use the abort
functionality:

    *.*.mod_templates.xerr_abort=1

new tool - bin/getbconfvars
===========================

A new tool is available as bin/getbconfvars. This is meant to replace
sed/grep as a way to extract values from configuration fields for use in
shell scripts.

Best demonstrated with this snippet from the updated trans init script:

    if [ -x "$GETVARSBIN" ]; then
    	eval `$GETVARSBIN --file $TRANS_CONFIG_FILE --key '(control_port|max_connections)'`
    else
    	control_port=$(sed -nre 's/^control_port[[:space:]]*=[[:space:]]*(.*)/\1/p' ${TRANS_CONFIG_FILE})
    	max_connections=$(sed -nre 's/^max_connections[[:space:]]*=[[:space:]]*(.*)/\1/p' ${TRANS_CONFIG_FILE})
    fi

Unlike using sed/grep, this tool handles include files and overrides correctly.

Any platform init scripts updated to use it will be kept backwards
compatible for the time being, but packaging and use of this tool is
encouraged.

trans at jobs optionally in redis
=================================

It is now possible to queue up at jobs in redis instead
of the database.

Before this new feature can be used, you need to add a
few lines of configuration.

    *.trans.at.redis_node=common.redis
    *.trans.at.redis.poll_period=100
    *.trans.at.redis.lock_period=600
    *.trans.at.redis.num_workers=5

The first line configures which redis node to use.
Poll_period defines how often trans will check redis
for jobs to execute.
Lock_period defines for how long a job will be locked
in redis before being released again.
Num_workers defines how many workers executing jobs in
parallel there should be.

To add jobs to the redis queue instead of the regular
queue just add use_redis:1 as a parameter to the
at command in trans. The same thing goes for at_clear so
if you want to remove a few jobs form the redis queue you
just call at_clear with the additional use_redis parameter.


# Version 2.3.3 [2013-06-13] #

### Availability ###

You can download the code from here:
  gitolite@scmcoord.com:platform.git

Current version tagged as:
  platform/2.3.3

### What's new ###

* [#35 !253] Any held sqlworkers will be released when command is done. A CRIT message will be generated if that is the case.
* [#313 !251] Fix available_weeks_calendar filter leaking variables.
* [#345 !265] Do not allow NULL validators in trans extra params.
* [#334 !257] Fix issue with mod_image crop mode on images smaller than the target size.
* [#269 !252] tpc breaks local variable anchoring for elements that need locals inside a varcode block.
* [#347 !263] Fix UTF-8 issue with bconf key-comparison function (shotgun)
* [!268] Better REGRESS_FAST_DIR selection.
* [!249] Fix a minor memory leak in chatter threads.

# Version 2.3.2 [2013-05-13] #

### Availability ###

You can download the code from here:
  gitolite@scmcoord.com:platform.git

Current version tagged as:
  platform/2.3.2

### What's new ###

* [#28 !246] Completed split of template functions and filters into separate files.
* [#300 !239] tpc now escapes trigraphs.
* [#301 !242] Add mutex to trans debug info to fix crash on cmd:transinfo.
* [#311 !245] Added 'C timestamp' search meta query, and error on invalid meta queries.
* [!234, !235] Added search engine benchmark suite (internal)
* [!236] Do not deadlock on ct_pool_lock when having too many command threads.
* [!237] Fixes to build with clang.
* [!238] Various saffron fixes, including buffer overrun in textfilter.
* [!240] Converged in-tree documentation to ./docs

# Version 2.3.1 [2013-04-10] #

### Availability ###

You can download the code from here:
  gitolite@scmcoord.com:platform.git

Current version tagged as:
  platform/2.3.1

### What's new ###

* [!233] Fixes for stopwords and counters in index_merge, plus support for opening indexes with old meta.
* [!232] Don't treat failures in cleanups in the vocabulary tests as regress test errors.
* [!231] Add correct dependency for building platform_version.h test.
* [!230] Don't fail the charmap test when LC_ALL is set to something other than en_US.

# Version 2.3.0 [2013-04-08] #

### Availability ###

You can download the code from here:
  gitolite@scmcoord.com:platform.git

Current version tagged as:
  platform/2.3.0

### What's new ###

* [#6   !161] Fixes for pgsql family of filters.
* [#19  !131] Removed random_seed template function.
* [#21  !168] Cleanup template_stress
* [#23  !212] Rename t-% to platform-t-% and deprecate the old rule.
* [#27  !164] mailapp:sendmail is now wrapper_template:none
* [#28  !142] Template functions/filters split
* [#61  !152] Platform version define
* [#62  !158] Make session backend bconf nodes configurable for cacheapi
* [#108 !138] Trans configuration limit for ev_popen, and transinfo info and stats.
* [#115 !202] Search meta in blob.
* [#116 !179] Remove libhiredis
* [#117 !169] Remove redis 2.0.4 from tree.
* [#118 !173] Remove PCRE 8.10 from tree.
* [#119 !172] Remove yajl 2.0.1 from tree.
* [#120 !174] Remove libhunspell 1.2.11 from tree.
* [#121 !171] Remove libevent 1.4.12 from tree.
* [#122 !178] Remove tcmalloc from tree.
* [#138 !167] Assimilate Webstress
* [#140 !162] Allow extra search threads to spawn while waiting for cache
* [#156 !217] Added !-operator to settings filter/qs_sq. Returns true iff key is undefined or empty.
* [#228 !139] Trans Regress Only Code
* [#252 !157] Make trans_db_snap capable of flushing db connections in external transaction handlers (mama)
* [#256 !153] Remodel search cache size.
* [#265 !194] include_var deprecated.
* [#271 !221] A new way of running regress tests which also generates statistics for jenkins
* [#275 !193] Fix broken __TMPL_BASE_PREFIX expansion.
* [#278 !208] Support for per-directive flavors in Builddesc files.
* [#285 !210] ninja build can now link with full path external library files (/usr/lib64/libresolv.a).
* [#289 !211] Extend build system with user defined rules based on file endings
* [#290 !213] Trans log tag and level configurable through configuration file.
* [#296 !218] Clear trans debug info on ROLLBACK and COMMIT.
* [#298 !223] Bursttrie serialization bug w.r.t key occurence limit
* [!28 ] event_dispatch added on [init|reload]_done step
* [!72 ] Add VALIDATOR_OTHER
* [!147] Allow non-integer template alias keys.
* [!149] log_setup for swig_index
* [!159] Add library version information output to trans startup sequence.
* [!163] Cleaned up trans read_config()
* [!186] Export DEST_BIN and PROG_REGRESSDIR for tests
* [!203] Allow for vtree key lookups in settings using $foo.bar
* [!205] Let qs_sq use ? settings lookups
* [!214] Add the number of bytes sent by send_result to the search slow query log.
* [!220] Redis Cli Allow Override
* [!222] Added Makefiles to build and install bursttools in regress.
* [!224] Ninja Resolve Srcdir Fix
* [!228] Various vocabulary/saffron fixes.

Search
======

Search will be able to spawn extra threads whenever one is blocked waiting
for the cache. This should lower number of info:goaway when something is
spawning the same question many times, but at the cost of temporarily
increasing the number of threads.

Trans
=====

1. Adds a VALIDATOR_OTHER, which can be used to validate another
parameter before this one. This is needed because validator funtions
are no longer allowed to use an unvalidated parameter through cmd_getval
(see UPGRADE.md).


2. You can limit the number of concurrent processess launched through ev_popen()
using the trans.conf option 'ev_popen.limit=100'. The default is 0 (disabled)

  A warning will be emitted to the log when the limit is hit, and congestion
can be tracked through the transinfo output for this backend. This also
outputs the list of pending children and how long they've been running.


3. There's a new transaction that can be used to flush SQL workers in remote
transaction handlers, such as MAMA. This can be used to overcome the
problem where lingering workers from MAMA is blocking db-snap, breaking
your tests. You do not need to change anything if you are not using MAMA
in regress.

  First some configuration to point out the MAMA host. This should
mirror the already existing 'mama_review.external_server' bconf,
which unfortunately is written in a non-standard format.

        *.mama_review.external_server.host.1.name=localhost
        *.mama_review.external_server.host.1.port=%REGRESS_MAMA_PORT%

  Then we give db_snap a list of nodes where it can fine the host+port config:

        *.*.common.trans_db_snap.external.flush_delay=3
        *.*.common.trans_db_snap.external.flush.1.node=*.mama_review.external_server

  If db-snap blocks for more than three seconds, it will attempt to gain
exclusivity over the DB by telling MAMA to finish up any transactions
and then flush all SQL workers.


Ninja
=====

You can now specify rules that trigger on specified file extensions. This is done by
creating rules as usual and then binding them within a perl module. A function named
register which returns a tuple containing the file ending and a function pointer is
required in the perl module.

Example of a module, foo.pm:

	package foo;

	sub register {
		return ('less', \&compile_less);
	}

	sub compile_less {
		my ($srcdir, $src, $ops, $basesrc) = @_;
		main::add_target($ops, $basesrc, "lesscss", [ $src ], "obj", $srcdir, undef, { ruledep => 1});
	}

	1;

In this example, we tell our build system that for every file ending with 'less' we want to execute
the compile_less function. The compile_less function ads a target to our build tree where we indicate
that we want to execute the lesscss-rule for this file. More information about adding targets to the 
tree can be found in build-build.pl.

The next step is to add our perl module to a new clause called extensions into the CONFIG section
of our build. How this is done can be seen below:

Builddesc:

	CONFIG(
		extensions[foo.pm]
		...
	)

Bursttools
==========

If you are using the Make-based build system you can now
have bursttools built and installed in regress by adding

        SUBDIR+=platform/bin/bursttools

to your top-level Makefile.


Settings
========

The settings filter and qs_sq settings now supports the '!' operator.
Whereas '?key' returns true iff key exists, '!key' returns true iff
key is undefined or empty.

Platform regress
================

Run regress tests with the following command
env RUNTESTS=1 ./compile.sh


## Version 2.2.2 [2013-03-04] ##

### Availability ###

You can download the code from here:
  gitolite@scmcoord.com:platform.git

Current version tagged as:
  platform/2.2.2

### What's new ###

* [#262 !198] Inject 'platform_templates' dependency when there are template attributes.
* [#156 !196] Disable interpolation by default.
* [!196] ninja: Use -O3 to build search.
* [#267 !195] Fix trans filter crashes on invalid input.
* [!191] in.pl Fixes from Ninja Workshop
* [!190] Ninja Rebuild Ninjas
* [!185] Deletion of redundant platform_template in trans' libs clause
* [#281 !184] Empty parameter clauses are now allowed in Builddesc
* [#266 !175] Refactor scan-build.
* [#270 !170] http_req.c no longer uses the first result from GAI to do the requests.
* [#273 !166] Fix crash in transaction_internal(TRANSACTION_INTERNAL_SYNC_CB)
* [#20] Clean up platform/regress/conf/bconf.txt.blob

Search engine
------------

Interpolation is now disabled by default in the search engine. Should you wish to enable
interpolation, modify parse_node_invword.cc.

Static analyzer
---------------
You can now run the clang static analyzer on the code.
Provided you have clang 3.1 or later installed, simply run

./compile.sh build/regress/analyse

To generate a report. It's quite slow and therefore not enabled
by default.

Observant people might notice this was mentioned earlier, but it's now much more simple to use.


## Version 2.2.1 [2013-02-06] ##

### Availability ###

You can download the code from here:
  gitolite@coordination.blocket.com:platform.git

Current version tagged as:
  platform/2.2.1

### What's new ###

* [#261 !137] php_templates now clean up locally allocated and cached variables on module shutdown. (fixes a minor memory leak)
* [#255 !127] Fix the lexer in tpc that broke code generation with `\"`.
* [#250 !126] Fix operator precedence in tpc broken since the generated code no longer depends on operator precedence in C.
* [#240 !129] Implement the redis HLEN operation through `&redis_hlen()` template function.
* [#216 !154] `<%#eatspace(...)%>` fixes to make it behave like documented. Documentation of tpc internals.
* [#165 !133] Function deprecated in 2.2.0 have now been removed.
* [#139 !143] BPAPI initialization cleanup.
* [#63 !140] Rate limit the debug dump in trans when max_connections is reached.
* [#41 !150] Words with quotation marks around them are no longer indexed with the quotation marks (only without).
* [#32 !128] Auto-detect local timezone for rfc822_date filter.
* [#24 !145] Split util.c into several smaller files with clear purpose.
* [#22 !146] Add test for newquery on worker after a failed query.
* [#69 !156] Set CLOEXEC on most fds.
* [!73] - Support building with clang and/or scan-build
* [!134] - .gitignore removed
* [!136] - Trans test transactions moved to regress
* [!144] - INCLUDE handling in CONFIG descriptors in Builddesc. Better split between local and non-local config.
* [!148] - Use only include filter in platform templates.
* [!151] - Allow site invars.sh to source platform one.
* [!155] - Added trans_add_printf() to transapi


## Version 2.2.0 [2013-01-17] ##

### Availability ###

You can download the code from here:
  gitolite@coordination.blocket.com:platform.git

Current version tagged as:
  platform/2.2.0

### What's new ###

* [#2 !75] FA_TRANS for template-based internal transactions in trans factory transactions.
* [#12 !78] Clean up template functions and filters. Deprecations.
* [#38 !87] Separate port for trans control and transinfo commands.
* [#40 !81] Make platform runnable on more distros: Build also on Ubuntu LTS 12.04 (debian)
* [#45] Add a ParseNodeOrder.
* [#60 !88] Fix label handling for count_level, count_tree, bucket in the search engine results.
* [#64 !104] Make varcode() blocks smarter. (optimization)
* [#129 !125] Saffron: Search engine suggestions
* [#144 !100] Replace make with ninja.
* [#218 !99] Make transaction_internal inherit the log_string if non given.
* [#226 !105] strncpy usage in filters.
* [#227 !117] Fix transaction_internal_output.
* [#229 !111] Fix deadlock. Queue SQL requests based on event base instead of thread.
* [#230 !110] Autoput old SQL worker when a new one is requested.
* [#237 !116] Fix issue with search engine sort problem on pivot and sort.
* [#247 !120] Ninja make regress fix.
* [MR: !15] Merge init_base and reload_base into one single pointer init_reload_base.
* [MR: !68] Interpolation search for bfind_w() in invword nodes. (optimization)
* [MR: !71] Cleanup when trans exists.
* [MR: !76] Use pthread_equals() when comparing thread ids.
* [MR: !77] Hunstemchk --byclass.
* [MR: !108] Dump timers cleanup.
* [MR: !112] Fix for possible double free of multi_key.
* [MR: !113] CentOS 5 fixes for ninja.
* [MR: !114] Ubuntu fixes for ninja.
* [MR: !115] Regress fixes for Ubuntu.
* [MR: !118] Various ninja fixes.
* [MR: !121] Fix for lex warning.
* [MR: !122] Fix for queued workers.
* [MR: !123] Ninja perl XS compiler.
* [MR: !124] Ninja syslogroot location.

### search ###

As part of project Saffron, the search engine can now generate suggestions for possibly misspelled words.
For documentation, please see the wiki:
	https://scmcoord.com/wiki/Search_engine_suggestions

You can now filter on order (list_time timestamp). Example:
	order:1358159229-

### trans ###

Trans now features a control port. This allows us to sidestep the backlog, which is important for
commands such as 'transinfo'.

Trans factory now supports the FA_TRANS action, which can be used to run internal transactions:

	FA_TRANS("trans/fatrans_test.txt", "fat_", FATRANS_OUT | FATRANS_BACAPTURE)

The first argument is a template which should render to the transaction command input.
The second argument is a prefix (see below)
The third argument are flags:

	FATRANS_OUT
		Pass through the transaction output from the internal transaction, using prefix if set.
	FATRANS_BACAPTURE
		The key:value output from the internal transaction is captured into the BPAPI for use
		in later actions, using prefix if set.

The use of a prefix is recommended, or you can theoretically run into problems with duplicate lines. As a
special case any "status:" line output is omitted if no prefix given.

As per issue #230 this release lets you call `sql_worker_put(worker);` in your fsm when worker is no longer needed,
even for event based workers. In addition, it will be called for you automatically whenever you do a new sql_worker
query (except for newquery). Thus you can't use the worker variable after such a call. Bad code example:

  sql_worker_template_query("pgsql.master", ...);
  if (worker->next_row(worker))

The reason this is done is that sql_worker_template_query might block for a while and we don't want to keep hold of unused resources.
You can disable the auto-put by adding the `SQLR_NOAUTOPUT` flag to your request.

### templates ###

* |dump_timers template filter implemented. Instead of outputing a static output, it creates a prefixed shadow vtree and allows you to output it any way you want.
  The keys created are: `<prefix>.<timer_names>.{s,ms,us,ns,tv_s,tv_ns}`. `s`, `ms`, `us`, `ns` are truncated floating point values for seconds, milliseconds,
  microseconds, nanoseconds. `tv_s` and `tv_ns` are the original, unformatted integer timespec values.
* &dump_timers function is removed. An example reimplementation with the same output is provided as a deffun inside tmpltut/templates/dump_timers.fun.tmpl

### hunstemchk ###

Added --byclass option. In this mode, instead of reporting all the
stems for each word as in the default mode, the reports is transposed
and consists of a stem and all words that share that stem. This can be used
to detect issues where conceptually different words happen to stem alike.

### Compatibility issues ###

* Builds on Ubuntu 12.04 LTS using GCC 4.6
	SHELL is set to 'bash' during build.
	index_ctl no longer gives an explicit path for 'nice', assumes it is PATH.

* If using ninja, for in-files with sections, the lines outside any section will be added to the output.

### Deprecated functions / APIs ###

* The following template functions have been DEPRECATED pending removal:
  
  	area_descr, format_weeks, format_price, format_zipcode, format_cents
  
On merge, they should be COPIED from platform/lib/template/deprecated_functions.c
into the local tree as needed. Under NO CIRCUMSTANCE should the existing platform
implementation be linked in.

### Ninja ###

The default build system for the platform itself has been changed to ninja. This should not affect sites since
everything is compileable with the old make infrastructure. Sites that wish to switch to ninja as their default
build system can read [COMPILING.md](COMPILING.md) for documentation of the new build system.

### Miscellaneous ###

This is a list of changes introduced with the ninja branch but not directly related to the ninja build framework:

* url_encode() has moved from query_string.c to url.c and a new Makefile flag has been introduced for building url.c in your binary: USE_URL

- gperf_enum.pl, mk/compile.mk
  * gperf_enum.pl takes the output file as the argument.

- _GNU_SOURCE
  * Defined in the files where it's necessary instead of command line.

- index_ctl
  * Use env variables (REGRESSBINDIR, REGRESSCONFDIR) to find the binaries and config files.

- mkcharmap
  * Don't print output unless something went wrong. (cluttered ninja output)

- show_template
  * Added an option to properly parse bconf, instead of getting it from trans.

- config.c, bconfig.h
  * Support for properly parsing bconf.
  * Correct handling of "include".

- everywhere.[ch]
  * correct includes, instead of letting overreaching includes include everything.

- log_event.[ch]
  * Instead of using a define HAS_SYSLOG_IDENT, make syslog_ident a weak function.

- blocket_functions.c
  * &build_version() template function.

- bpapi.[ch], tpfunctions.c
  * TEMPLATE_TIMERS are no longer optional, instead there's a template_timers_ctl function
    to enable and disable template timers.

- tmpltut
  * template timers are enabled in here like they were through -DTEMPLATE_TIMERS before.

- ctemplates.c, ctemplates_pg.c
  * PGSQL_TEMPLATES no longer optional.
  * pgsql templates split into own file.

- query_string.[ch], parse_query_string.[ch]
  * Query string handling split between two files. parse_query_string.[ch] is for the
    general problem of parsing a query string. query_string.[ch] is for qs_sq and other
    vtree+template related stuff.

- mk/regressinit.mk, mk/regress-tut.mk
  * adaptations to ninja regress. Local to platform tests.

- mk/regress.mk
  * adaptations to ninja regress.

- mod_redir.c
  * parse_query_string split.

- php_templates.c
  * Compiler warnings.

- regress/*
  * Adaptations to ninja build.

- scripts/db/start-build-pgsql.sh
  * different location of binaries in ninja build.

- scripts/init/search
  * configurable engine and search binary through env variables (REGRESSENGINE, REGRESSSEARCH)



## Version 2.1.3 [2012-12-05] ##

### Availability ###

You can download the code from here:
  gitolite@coordination.blocket.com:platform.git

Current version tagged as:
  platform/2.1.3

### What's new ###

* [#58   !92] Don't use pthread_t as a boolean
* [#66   !86] Fix tpc segfault on conditional block composed by whitespace when eatspace(eol) is on
* [#211  !83] Add a call to log_setup() in php_templates to properly handle calls to log_printf()
* [#212  !84] Make sure to return NULL for nonexisting keys in PHP vtree subnode gets.
* [#213  !85] Improve mod_textinfo logging and put all apache's error_log files under ${TOPDIR}/logs during regress testing
* [#219  !89] Remove incomplete and buggy gettext support from templates.
* [#220  !91] Don't accept shard_size=0 as valid value for shard sizes in index_ctl
* [#223  !94] Avoid using global cache variable for thread local data.
* [#225  !98] Stop using strlen on str in output_raw functions.
* [#231 !103] Correct the tag name for git subtree add
* [ !90] Prevent a deadlock in trans_at
* [ !96] Add U-umlaut chars to xiti_transliterate_latin1
* [ !97] Make sure to only extract a single IP for REGRESS_HOST and REGRESS_HOSTIP
* [!101] Do the non-utf8 detection even for strings that aren't correctly url-encoded.
* [!102] Fix a race in cached_regex.
* [!106] Fix for enctype comparision in mod_templates/request.c

### Compatibility issues ###

Issue #219 removes gettext template support. As far as we know nobody is using this feature.

MR !103 updates the upgrade instructions from 2.1.0 to 2.1.1. Please use the following link for the up to date version:
https://coordination.blocket.com/gitlab/platform/tree/stable/UPGRADE.md

### Deprecated functions / APIs ###

## Version 2.1.2 [2012-11-09] ##

### Availability ###

You can download the code from here:
  gitolite@coordination.blocket.com:platform.git

Current version tagged as:
  platform/2.1.2

### What's new ###

* [MR !44 - Issue #88] Prevent trans from segfaulting when trans_mail is invoked without params
* [MR !45 - Issue #86] Avoid out of boundary array access in php_bsearch
* [MR !61 - Issue #83] Include <locale.h> for setlocale() definition
* [MR !62 - Issue #84] Fix a segfault when calling random_range() and random_token() from trans due to missing init_func
* [MR !59 - Issue #87] Use a more specific prefix for symbols in the generated C code from templates, to avoid collisions with system provided library symbols. Before the prefix uset to be simply 't', which lead to potentially dangerous symbol names, like 'tsearch()'. The new prefix will be '_bt_'.
* [MR !65 - Issue #90] In the search engine, make it possible to search for 'previous_ad' by allowing negative numbers in id ranges.
* [MR !63 - Issue #89] Call 'flush_template_cache()' in 'bconf_overwrite()' to avoid templates use cached values for bconf keys after being overwritten.
* [MR !64 - Issue #77] Fix lang and country lookup for the include filter.
* [MR !74 - Issue #93] Add more RPM dependencies to the 'rpms' file.
* [MR !70 - Issue #97] Use strwait instead of printing status integer (Search Engine).
* [MR !69 - Issue #98] Kill sigpipe handler in daemon.c.
* [MR !82 - Issue #58] Use pthread_equal() instead of using the equal '==' operator to compare pthread IDs
* [MR !43 - Issue #101] Check for stopwords again after stemming a word.
* [MR !58 - Issue #100] Explicitly check for application/x-www-form-urlencoded instead of using it by default
* [MR !46] Force the minimum timeout to be 1000ms for both "connect_timeout" and "timeout" for fd_pool
* Fix a typo in regress/php_templates/Makefile: the test was still referring to ${PLATFORMDIR}/logs, where the correct path is ${TOPDIR}/logs

### Compatibility issues ###

MR !59 introduces a binary incompatibility with any previous version, since we basically changed the name of all the public symbols in the templates libraries code. This means that, after upgrading the sources to this version, you'll need to clean up all your object files. Running a simple
    make cleandir
will do it.

This change might also have impacts for the binaries you build for release:
* If you use any CI tool that does a fresh build every time to produce binaries for release, then you're *safe*, since every object file will be regenerated.
* If you're building your binaries manually, remember to clean up everything before building for this release, either by using `make cleandir` or by starting from a clean checkout / clone from your repository.

### Deprecated functions / APIs ###


## Version 2.1.1 [2012-11-01] ##

Halloween release!
This version includes a single logical "change": we got rid of the extra 'platform/'
directory in the platform module.

### Availability ###

You can download the code from here:
  gitolite@coordination.blocket.com:platform.git

Current version tagged as:
  platform/2.1.1

### What's new ###

* [Issue #82] Move all the files under 'platform/' at toplevel, finally getting rid of the extra 'platform/' directory

### Compatibility issues ###

This version introduces a few compatibility issues, both for site integration and for the platform module
itself, as described in the following sections.

#### Site integration ####

This release will need a final resync for every site. Checke the instructions in the UPGRADE.md file.
Beside that, no other change should be needed in the site code, unless you're using some weird chains
of inclusion between your own makefiles and the makefiles in the platform directory.

#### Platform "standalone" module ####

A new "requirement" is introduced because of the 'platform/' directory removal.
If you're building the platform module itself, you might want to create a "container directory" into
which you'll clone the platform project, which will hold the autogenerated 'build', 'log' and 'pgsql0' directories.

### Deprecated functions / APIs ###

* NA


## Version 2.1.0 [2012-09-28] ##

### Availability ###

You can download the code from here:
  gitolite@coordination.blocket.com:platform.git

Current version tagged as:
  platform/2.1.0

### What's new ###

* New documentation file: CHANGES.md and UPGRADE.md
* Various bugfixes
* Add a couple of redis filters
* check git log for a complete list (for this version)

### Compatibility issues ###

* If your trans server makes use of the at_clear_commands() you'll need to copy the PLSQL function
  under platform/scripts/db/plsql/at_clear_commands.sql under your local tree, eg:

    cp platform/scripts/db/plsql/at_clear_commands.sql scripts/db/plsql/

### Deprecated functions / APIs ###

* NA
