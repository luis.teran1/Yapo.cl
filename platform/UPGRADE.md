# From 2.9.2 to 2.9.3 #

Update your git repository with the latest code from platform:

    git checkout -b upgrade_platform_2.9.3 <your-stable-branch>
    git remote add platform git@scmcoord.com:coord/platform.git
    git fetch platform
    git subtree --prefix=platform pull platform platform/2.9.3

# From 2.9.1 to 2.9.2 #

Update your git repository with the latest code from platform:

    git checkout -b upgrade_platform_2.9.2 <your-stable-branch>
    git remote add platform git@scmcoord.com:coord/platform.git
    git fetch platform
    git subtree --prefix=platform pull platform platform/2.9.2

# From 2.9.0 to 2.9.1 #

Update your git repository with the latest code from platform:

    git checkout -b upgrade_platform_2.9.1 <your-stable-branch>
    git remote add platform git@scmcoord.com:coord/platform.git
    git fetch platform
    git subtree --prefix=platform pull platform platform/2.9.1

## trans / redis ##

The change to the redis functions and filters in 2.9.0 to use filter-state
means that any code that calls into templates where these filters are used
must have had the bpapi initialized in the 'correct way'. You're most
likely to run into this issue with trans.

Here's an example from trans_account_delete.c

		case ACCOUNT_DELETE:
		{
			BPAPI_INIT(&ba, NULL, pgsql);
			account_delete_bind(&ba, state);

			state->state = ACCOUNT_DELETE_DONE;

			sql_worker_template_query("pgsql.master", "sql/call_account_delete.sql",
			    &ba, account_delete_fsm, state, cs->ts->log_string);

			call_template(&ba, "trans/account_block.txt");
			bpapi_free(&ba);
			return NULL;
		}

Where 'trans/account_block.txt' is a template that calls redis filters.

For this to work, the code must be updated to use the command_bpapi_init()
method of initialization outlined in the CHANGES for 2.5.1, or in the
[trans cookbook](https://scmcoord.com/wiki/Trans_Cookbook#Initializing_bpapi)
in the wiki.

At the very least, any code that call call_template*() should be updated.

# From 2.8.2 to 2.9.0 #

Update your git repository with the latest code from platform:

    git checkout -b upgrade_platform_2.9.0 <your-stable-branch>
    git remote add platform git@scmcoord.com:coord/platform.git
    git fetch platform
    git subtree --prefix=platform pull platform platform/2.9.0


## setvars.in ##

In case you don't include the platform one, the following keys have to be
added to your invars.sh

```
	genport "REGRESS_BCONFD_TRANS_PORT" 50
	genport "REGRESS_BCONFD_CONTROLLER_PORT" 51
	genport "REGRESS_BCONFD_JSON_PORT" 52
```

## Bconf ##

In 2.8.0 we told you you could remove any 'counters.invword' configuration. That
was a bit too specific. You can also remove 'counters.sum'.

These new keys have to be added to your bconf.txt.site.in in the REGRESS part:

```
	*.*.service.bconfd.1.host = localhost
	*.*.service.bconfd.1.port = %REGRESS_BCONFD_TRANS_PORT%
	*.*.service.bconfd.1.json_host = localhost
	*.*.service.bconfd.1.json_port = %REGRESS_BCONFD_JSON_PORT%
```

In the PROD part, use the same hosts as your trans servers (or whatever your
sysadmin tells you). Port 5758 for "port" and 5759 for "json_port".

In bconf.conf.in, change %REGRESS_TRANS_PORT% to %REGRESS_BCONFD_TRANS_PORT% and
5656 to 5758.

You should also copy the file platform/docs/bconfd.conf.in.sample to
conf/bconf/bconfd.conf.in and adjust it to your needs.

Add to conf/bconf/Builddesc:
```
diff --git a/conf/bconf/Builddesc b/conf/bconf/Builddesc
index 64146fc..83a1a4f 100644
--- a/conf/bconf/Builddesc
+++ b/conf/bconf/Builddesc
@@ -1,6 +1,7 @@
 INSTALL(conf
        srcs[
                bconf.conf.in
+               bconfd.conf.in
                bconf.txt.adsense.in
                bconf.txt.adview.count.in
                bconf.txt.site.in
@@ -13,6 +14,7 @@ INSTALL(conf
        ]
        conf[
                bconf.conf
+               bconfd.conf
                bconf.txt.adsense
                bconf.txt.adview.count
                bconf.txt.site
```

## Build ##

The platform scripts/build/buildversion.sh script no longer tries to
query bzr. We don't expect this to be an issue.

## Libcommon ##

`bufprealloc` has been renamed to `bsprealloc`, fix if the compiler errors
on the old function name.

### Bconf ###

There's been some small changes to the bconf_add_* family of functions, which should
not require any changes to client code for the most part.

Some NULL checks on the root parameter and associated xerrs() have been removed,
and return value made void since in general client code did not use it.

The function bconf_add_data_canfail() has been added for when a client wants
to detect failures.

### Config (also bconf) ###

In practice the following is unlikely to require any site changes, but
because it may result in lost configuration it's very important to verify.

'#include' support has been removed, use 'include'. The pound-include form
has been generating a warning in the trans backend for a long time. Note
however that support was also removed in `config_init()` which is used to load
bconf-style configuration files.

You'll have to grep through your configuration files (don't forget in-files)
for `^#include` and change any occurrences. Example:

	find . -name "*conf*" | xargs grep "^#include"

## mod_bsconf ##

mod_bsconf, used to provide state to the search filters, has been made obsolete and removed.

You can remove any LoadModule directive from your httpd conf(s):

	LoadModule bsconf_module ...

As well as the associated configuration directive:

	BSConf prefix1 prefix2

The arguments here specify, in a roundabout way, the bconf roots for search and redis engines
respectively. The path specified had to have an 'engine_prefix' node whose value in turn
specified the actual engine path prefix.

This extra indirection was deemed unnecessary and is no longer available.

Indirection through bconf is still available through the following configuration keys,
shown here with the default values:

	*.*.search.engine_path=common
	*.*.redis.engine_path=common

This says that if, for instance, you use the search filter with "asearch" as the engine,
the configuration will be taken from 'common.asearch.*'

### Old bconf ###

You can remove any search.engines and redis.engines bconf, it is no longer used.

	-*.*.search.engines.1=asearch
	-*.*.search.engines.2=csearch
	...

### Code changes ###

All redis template functions and filters now use a global filter state. The implication
of this is that the connection sharing works also when there's no redis_session. In fact
the redis_session filter is now a no-op (though this may change in the future).

IMPORTANT: If you used the undocumented 2-argument version of the redis session filter,
you'll have to change it. The second argument was used to specify the path to the engine
node. This is no longer supported.

The code changes means that any local redis functions and filters will no longer compile.
The changes necessary have been kept relatively minor though.

To get the connection pool, you now just pass an api and the engine:

    - pool = get_redis_pool(&api->vchain, engine, &node, &free_pool);
    + pool = get_redis_pool(api, engine);

The node and the code related to it is no longer required, so the variables
'node' and 'free_pool' can be removed:

    - int free_pool;
    - struct bpapi_vtree_chain node = {0};

    - if (free_pool) {
    -	fd_pool_free(pool);
    -	vtree_free(&node);
    - }

For filters, the redis_filter_data struct no longer contains a vtree member, since it wasn't
required by any of the existing filters. Instead we've added a pointer to the filter state
called 'fsconf', so that you can get it in the init function and use it anywhere else.

    - data->vtree = &api->vchain;
    + data->fsconf = get_redis_fsconf(api, engine);

With the state and its connection pool available, calls to the sredisapi can now
be done as so:

    - pool = get_redis_pool(data->vtree, data->engine, &node, &free_pool);
    - struct fd_pool_conn *conn = redis_sock_conn(pool, "master");
    + struct fd_pool_conn *conn = redis_sock_conn(data->fsconf->pool, "master");

TL;DNR: Look at the functions in platform/lib/template/redis/ if you need guidance.

## Templates ##

The `&latin1_to_utf8` template function, previously deprecated in 2.8.0,
has been removed. Use the filter with the same name instead.

If you have a custom vtree backend with a fetch_keys_and_values function you
need to make user it sets `loop->type` to the correct value, see vtree.h.

Note: we're probably happy to take over any such custom vtree backend.

## trans ##

The bconf_flush command has been removed. In practice that could have only been used by the
local implementation of setconf.

	 grep "\<bconf_flush\>" . -rnI

If the above returns any results in your local tree, these have to removed. See below for
specifics about trans_setconf.c.

### trans.conf ###

The following keys have been removed from `trans.conf`

	bconf 
	bconf_db

The following key has been added to `trans.conf`

	bconfd =  %DESTDIR%/regress/conf/bconf.conf

### cmd:bconf ###

Does not accept type as key since it does not visit db
Please refer to [CHANGES.md](CHANGES.md) for a complete set of changes.

### setconf transaction ###

Since the bconf daemon is polling the database every ``dbpoller.interval_ms``, the
setconf transaction **should not** flush the configuration in trans **or** bconfd.

Thus the relevant part has to be removed from trans_setconf.c

```
diff --git a/daemons/trans/trans_setconf.c b/daemons/trans/trans_setconf.c
index 5a0a00a..e073f1e 100644
--- a/daemons/trans/trans_setconf.c
+++ b/daemons/trans/trans_setconf.c
@@ -21,7 +21,6 @@
 #define SETCONF_SET           5
 #define SETCONF_HANDLE        6
 #define SETCONF_HANDLE_COMMIT 7
-#define SETCONF_FLUSH_TRANS   8

 extern struct bconf_node *bconf_root;

@@ -88,75 +87,6 @@ static void setconf_imgcmd_cb(struct davdb *ds) {
 	free(ds);
 }

-/*
- * Simplistic trans api.
- */
-struct setconf_trans_data {
-	const char *index;
-	int fd;
-	struct setconf_state *state;
-};
-
-static void
-setconf_trans_done (struct setconf_trans_data *data) {
-	if (--data->state->trhosts <= 0)
-		setconf_fsm(data->state, NULL, NULL);
-	free(data);
-}
-
-static void
-setconf_trans_read_cb (struct bufferevent *bev, void *v) {
-	struct setconf_trans_data *data = v;
-	char *line;
-
-	while ((line = evbuffer_readline (bev->input))) {
-		transaction_printf(data->state->cs->ts, "flush.%s.result:%s\n", data->index, line);
-		free(line);
-	}
-}
-
-static void
-setconf_trans_write_cb (struct bufferevent *bev, void *v) {
-	struct setconf_trans_data *data = v;
-
-	shutdown(data->fd, SHUT_WR);
-	bufferevent_disable(bev, EV_WRITE);
-}
-
-static void
-setconf_trans_error_cb (struct bufferevent *bev, short what, void *v) {
-	struct setconf_trans_data *data = v;
-
-	/* Drain data */
-	setconf_trans_read_cb(bev, v);
-
-	bufferevent_free(bev);
-	close(data->fd);
-	setconf_trans_done(data);
-}
-
-static void
-setconf_trans_connect_cb (int fd, void *v) {
-	struct setconf_trans_data *data = v;
-
-	if (fd < 0) {
-		transaction_printf(data->state->cs->ts, "flush.%s.failed:%d\n", data->index, fd);
-		setconf_trans_done(data);
-	} else {
-		/* For simplicity, ignore the busy banner check, read will catch it. */
-		struct bufferevent *bev = bufferevent_new(fd, setconf_trans_read_cb, setconf_trans_write_cb,
-				setconf_trans_error_cb, data);
-		if (!bev)
-			xerr(1, "bufferevent_new");
-
-		bufferevent_base_set(data->state->cs->base, bev);
-
-		data->fd = fd;
-		evbuffer_add_printf(bev->output, "cmd:bconf_flush\ncommit:1\nend\n");
-		bufferevent_enable(bev, EV_WRITE | EV_READ);
-	}
-}
-
 /*****************************************************************************
  * bind_insert
  * Initialise template API before a call to sql_worker_template_query().
@@ -337,36 +267,8 @@ setconf_fsm(void *v, struct sql_worker *worker, const char *errstr) {
                         return NULL;

 		case SETCONF_HANDLE_COMMIT:
-			state->state = SETCONF_FLUSH_TRANS;
-			return NULL;
-
-		case SETCONF_FLUSH_TRANS:
-		{
-			struct bconf_node *trhosts = bconf_get(bconf_root, "*.*.common.transaction.host");
-			int i;
-
-			for (i = 0; i < bconf_count (trhosts); i++) {
-				struct bconf_node *node = bconf_byindex(trhosts, i);
-				const char *host = bconf_get_string(node, "name");
-				const char *port = bconf_get_string(node, "port");
-				struct setconf_trans_data *data = zmalloc (sizeof (*data));
-
-				data->index = bconf_key(node);
-				data->state = state;
-				if (host && port && !net_connect_host_port (cs->base, SOCK_STREAM, 0, 5, host, port,
-						setconf_trans_connect_cb, data)) {
-					state->trhosts++;
-					transaction_logprintf(cs->ts, D_INFO, "Flushing trans at %s:%s", host, port);
-				} else {
-					transaction_printf(data->state->cs->ts, "flush.%s.failed:%d\n", data->index, errno);
-					free(data);
-				}
-			}
 			state->state = SETCONF_DONE;
-			if (state->trhosts)
-				return NULL;
-			continue;
-		}
+			return NULL;

 		case SETCONF_DONE:
 			setconf_done(state);
```

File uploading of configuration through setconf is not officially supported and
should be considered deprecated. Check if you are using it by grepping the logs:

	grep "<< set_filedata:" <TRANS_LOG>


TL;DNR: Go back and read, this is important.

### trans tests ###

Trans tests have to include bconfd on the REGRESS_DEPEND before trans starts:

	REGRESS_DEPEND += bconfd-regress-config-start-conf--bconfd.conf

bconf_overwrite is available in the regress flavor only. If you are using the make targets
provided in platform then some minimal changes have to be applied in your test suites.

The changes in trans testsuites can be summarized by the following two cases.

The first case, and by far the most common, can be seen in bellow:

	 REGRESS_TARGETS += testsuite-mail-error
	-REGRESS_TARGETS += trans-init-reload
	+REGRESS_TARGETS += trans-flush-overwrite
	 REGRESS_TARGETS += bconf-overwrite-\*.\*.common.sendmail_path:mail_fail.sh
	 REGRESS_TARGETS += platform-t-mail-fail
	 REGRESS_TARGETS += platform-t-mail-noparam-fail
	-REGRESS_TARGETS += trans-init-reload
	+REGRESS_TARGETS += trans-flush-overwrite
	 REGRESS_TARGETS += endtestsuite-mail-error

Before, a reload of trans would have sufficed for all the overwritten configuration to be flushed.
Now the provided ``trans-flush-overwrite`` target has to be used, which is responsible for both
flushing the overwritten configuration from bconf and forcing trans to reload it.

The second case applies in very specific tests and probably does not apply in most local testsuites

	 REGRESS_TARGETS += testsuite-at-execute-max
	 REGRESS_TARGETS += db-restore-test
	-REGRESS_TARGETS += trans-init-restart
	+REGRESS_TARGETS += trans-flush-overwrite
	 REGRESS_TARGETS += bconf-overwrite-\*.example.hang_forever:1
	 REGRESS_TARGETS += platform-t-at-register-execute-max
	 REGRESS_TARGETS += platform-t-at-register-execute-max
	 REGRESS_TARGETS += platform-t-at-register-execute-max
	 REGRESS_TARGETS += platform-t-at-register-execute-max
	 REGRESS_TARGETS += check-db-execute-max
	+REGRESS_TARGETS += bconf-flush-overwrite
	 REGRESS_TARGETS += trans-init-restart
	 REGRESS_TARGETS += endtestsuite-at-execute-max

Notice that bconfd's overwritten conf has to be flushed before trans gets restarted. That is because,
in that case trans has been asked specifically to hang. Also the commands that hang trans are in trans_queue. 
If one does not flush the configuration from bconfd before restarting trans, the new trans will load with the
configuration that will make it hang again.

### front end tests ###

The way that bCmd.php and friends handles bconf-overwrite has changed

	--- a/util/selenium-core/bCmd.php.in
	+++ b/util/selenium-core/bCmd.php.in
	@@ -201,8 +201,8 @@ if (isset($_REQUEST['bconf-overwrite'])) {
	        $key = $_REQUEST['bconf-overwrite'];
	        $value = $_REQUEST['value'];
	         $description = "Setting $key key to $value value in bconf";
	-        $trans_command = "cmd:bconf_overwrite\nkey:$key\nvalue:$value\ncommit:1\nend\n";
	-       execute_trans_command($trans_command, $description);
	+       $command = "make -C %TOPDIR% bconf-overwrite-$key:$value"
	+       execute_sh_command($command, $description);
	        /* Cannot restart apache because that would kill selenium */
	        $description="Apache graceful";
	        $command = "make -C %TOPDIR% apache-regress-graceful";

Just execute the provided make target and gracefull restart apache. Of course that implies that the make target is
exposed to %TOPDIR%.

There are a lot of other ways to reload bconf. You might want to add a `bconf-flush-overwrite` next to any of these:

       trans-test-stop, trans-test-start, trans-regress-reload, trans-regress-stop, trans-regress-start,
       trans-reload,

There's also `bconfd-trans-regress-reload` which reloads both bconfd and trans.

### Output ###

The message part of status messages now have any control characters (newlines)
escaped in order to preserve the line-based nature of trans output.

Before:

	status:TRANS_DATABASE_ERROR:ERROR:  syntax error at or near "BAD"
	LINE 1: BAD SQL, BAD BAD;
			^

Now:

	status:TRANS_DATABASE_ERROR:ERROR:  syntax error at or near "BAD"\nLINE 1: BAD SQL, BAD BAD;\n        ^\n

If you need the original formatting you can pass the string through printf, or in the case
of SQL errors like these, look in the database log.

We've found code outside of trans that parses TRANS_DATABASE_ERROR lines. This is not a good idea.
TRANS_DATABASE_ERROR is meant for fatal errors that should be reported to developers/sysadmins.
Thus you should change those to proper TRANS_ERROR by parsing in the error handling of the
specific transaction. Regardless, you will have to grep your code for any such parsing since it
might not work anymore.

## php dynamic configuration ##

Dynamic configuration in php **has** to be obtained from bconfd.
In practice this simply means that the function get_dynamic_bconf should
send the trans command using bconfd's connection info.

```
	diff --git a/php/common/include/bTransaction.php b/php/common/include/bTransaction.php
	index 569ce27..06441f7 100644
	--- a/php/controlpanel/include/dyn_config.php
	+++ b/php/controlpanel/include/dyn_config.php
	@@ -15,13 +15,16 @@ function array_to_bconf($array, $conf = '') {
	        return $result;
	 }
	
	 function get_dynamic_bconf() {
	         global $BCONF;
	+       $bconfd_conninfo = bconf_get($BCONF, "*.service.bconfd");
	+
	        $transaction = new bTransaction();
	        $host = strtolower(bconf_get($BCONF, '*.common.httpd_confname'));
	        $transaction->add_data('type', 'conf');
	        $transaction->add_data('host', $host);
	-       $reply = $transaction->send_command('bconf');
	+       $reply = $transaction->send_command('bconf', true, false, $bconfd_conninfo);
	        if (strpos($reply['status'], 'TRANS_OK') !== FALSE) {
	                $dyn_bconf = array();
	
```

## blocket.spec ##

mod_bsconf should be removed.

The new bconfd has to be included in blocket.spec. Also the dependencies of trans have changed,
it does not require blocket-bconf anymore and it needs ''bconf.conf''.

You have to add the following definition to blocket.spec.

```
	%package bconfd
	Summary: Bconf daemon
	Group: Blocket/Daemons
	Requires: blocket-bconf
	Requires: libcurl
	Requires: nc
	
	%description bconfd
	A daemon that serves Bconf
	
	%post bconfd
	%(cat scripts/postinst/bconfd-postinstall.sh)
	
	%files bconfd
	%defattr(-,root,root)
	%{blocket}/bin/bconfd
	%{blocket}/etc/init.d/bconfd
	%config %{blocket}/conf/bconfd.conf
```

You also have to add the following config to your ''%files trans'' section

	%config %{blocket}/conf/bconf.conf

It is advised that you remove the following dependency from the ''%package trans'' section

	Requires: blocket-bconf

## init and postinstall scripts ##

A bconfd init script is provided in the platform directory under scripts/init/bconfd.
You can use it directly if you decide to and use the proposed bconfd.conf
which can be found in the [wiki](https://scmcoord.com/wiki/Bconfd#Sample_of_a_minimal_configuration)

A sample postinstall script can be found in platform directory under
docs/bconfd-postinstall.sh


# From 2.8.1 to 2.8.2 #

Update your git repository with the latest code from platform:

    git checkout -b upgrade_platform_2.8.2 <your-stable-branch>
    git remote add platform git@scmcoord.com:coord/platform.git
    git fetch platform
    git subtree --prefix=platform pull platform platform/2.8.2

# From 2.8.0 to 2.8.1 #

Update your git repository with the latest code from platform:

    git checkout -b upgrade_platform_2.8.1 <your-stable-branch>
    git remote add platform git@scmcoord.com:coord/platform.git
    git fetch platform
    git subtree --prefix=platform pull platform platform/2.8.1

# From 2.7.3 to 2.8.0 #

Update your git repository with the latest code from platform:

    git checkout -b upgrade_platform_2.8.0 <your-stable-branch>
    git remote add platform git@scmcoord.com:coord/platform.git
    git fetch platform
    git subtree --prefix=platform pull platform platform/2.8.0


## Build ##

### GCC 4.8 and Clang 3.4 ###

The lowest supported compiler versions are now GCC 4.8 and Clang 3.4.

The easiest way to obtain GCC 4.8 on CentOS is to install devtoolset-2
distributed by CERN (maybe CentOS will also distribute a version in the
future).

It can be obtained here:

http://linux.web.cern.ch/linux/devtoolset/

You will also need the CERN repo key, installed with this command:

	sudo wget -O /etc/pki/rpm-gpg/RPM-GPG-KEY-cern https://www.scientificlinux.org/documentation/gpg/RPM-GPG-KEY-cern

If for some reason this fails, an alternative can be:

	sudo wget -O /etc/pki/rpm-gpg/RPM-GPG-KEY-cern http://linuxsoft.cern.ch/cern/slc65/x86_64/RPM-GPG-KEY-cern

If you're using ninja, it's recommended you put this snippet at the top of
your compile.sh file:

	if [ -f /etc/redhat-release ] && [ -z "$NO_SCL" ] && ! scl_enabled devtoolset-2; then
		exec scl enable devtoolset-2 "$0 $*"
	fi

It will activate devtoolset-2 automatically before compiling. You could activate other SCL
packages the same way, e.g. ruby193 or python33. Install `centos-release-SCL` to get
access to those.

There are advantages to enable it in your shell as well though, for example it contains
a more modern version of `git`.

`ccache` is once again explicitly used, and thus required.

### ninja ###

Most platform components can now only be built using ninja. Best way
to handle this is to switch over your whole build system to ninja, but
if you're not ready for that, you can also build only the platform
components with ninja and the rest with make.

Follow the instructions in the wiki, available at
https://scmcoord.com/wiki/Getting_started_using_ninja
to see how to build the search engine using ninja in a tree otherwise
built with make. It should be fairly easy to apply it to the rest of
the components as well.

### builddir ###

Some uses of the name `builddir` have been renamed:

* If you set `builddir` in your top level `Builddesc` you
  have to rename that option to `buildpath`.
* You might also want update `compile.sh` to use the new
  name.
* You will need to recursively grep your makefiles for the
  use of `BUILDDIR` and replace with `OBJDIR`.

## http_req and tls deprecated ##

http_req and tls are deprecated and soon will be removed from platform. We recommend that
libcurl should be used instead.

We have provided two basic wrapper functions for libcurl which simplifies GET
and POST requests. 

It is imperative that libcurl is initialized with
curl_global_init(CURL_GLOBAL_ALL) before using any functions in libcurl or the
wrapper functions provided by us. A backend that does the libcurl
initialization in trans has been included and the wrapper functions should be
safe to call directly anywhere, except in other backends during the init phase,
in trans.

The wrapper functions will follow redirects, but to keep some level of sanity
they will only follow redirects pointing to either http or https.

Examples of how to use the wrapper functions can be found in
platform/regress/common/http/testhttp.c

## Libcommon ##

The Basic OverSeer provided by daemon.c will now exit if the overseen
daemon exits with status 0. Before it respawned the program.

If you have custom daemons relying on the old behaviour you need to
change them to exit with status code != 0.

## mod_redir ##

The client IP logging has changed slightly in mod_redir. Where it used to log c->remote_ip
directly it now calls get_remote_addr() of the "remote_addr" API. This shouldn't make a difference
in practice, but could make reporting more accurate in case the request was handled by
a proxy.

## tcpsend / nc ##

A trivial tcpsend was implemented. It is needed in the platform because the nc vesrion in centos 7
is working in a surprising matter.

Should you choose to use it, you will have to add its path to COMPONENT in the top level Builddesc

          @@ -108,6 +108,7 @@ COMPONENT([
                  platform/bin/perl_xs_compiler
                  platform/bin/search
                  platform/bin/statpoints
          +       platform/bin/tcpsend
                  platform/bin/tpc
                  platform/bin/trans
                  platform/lib/apache

If you are building with `make` check the `ninja` version above and add it to `SUBDIR`.

Also make sure that your local `DO_TEST` parameter is populated in `platform/mk/regress.mk` or
assign `NETCAT` to
	$TOPDIR/$destroot/bin/tcpsend

## templates ##

The &latin1_to_utf8() template function is now deprecated and will be
removed in a future release. You can replace it with the corresponding
<%|latin1_to_utf8|%> filter instead.

## trans ##

### validators ###

The VFLAG_NAMELIST flag in validators has been substituted by VFLAG_DOTPATH.

In the unlikely case that your code is dynamically setting validators 
and using VFLAG_NAMELIST then you will have to apply a diff similar to:

           if (type && strcmp(type, "namelist") == 0)
           -            v_data.v_flags |= VFLAG_NAMELIST;
           +            v_data.v_flags |= VFLAG_DOTPATH;

### transaction_internal ###

The way the transaction_internal callback is called changed, before the callback was
called for each line of output from the transaction now the callback is called once
for each key:value pair. For non-blob output the result is the same but blob values
are now passed all at once and not split up line by line. Callbacks that
parse blobs by reading them in line by line need to modified. Instead of an extra
state machine in the callback to reconstruct the blob it is now passed all in one
starting with the "blob:<size>:key\n" line. See also platform/bin/trans/factory.c
as an example on how to parse a blob. Callbacks of transaction_internal calls that
do not return data as blobs do not need to be modified.

### trans/mail_queue/spamfilter ###

If your transaction handlers are running with 'ev_popen.limit' set, some
transactions that used to be safe may have become racey and can crash. This
is because the limiter must pump events to make sure it doesn't dead-lock, and
thus you can no longer rely on there being no callbacks just because you have
not returned to libevent (i.e 'return NULL' in a transaction FSM)

This is known to be a problem with most if not all implementations of trans_spamfilter.c,
which should be fixed by using the new mail queue abstraction provided with this release.

The broken code looks something like this:

	loop (...) {
		mail_send(md, mail_get_param(md, "template_name"), mail_get_param(md, "lang"));
		state->send_mail_count++;
		state->state = SPAMFILTER_DELETE;
	}

	if (state->send_mail_count == 0) {
		state->state = SPAMFILTER_DONE;
		continue;
	} else {
		transaction_logprintf(cs->ts, D_INFO, "Will send %d mails", state->send_mail_count);
		return NULL;
	}

With the mail callback jumping back into the FSM when the counter reaches zero:

        static void
        spamfilter_send_cb(struct mail_data *md, void *arg, int status) {
        	struct spamfilter_state *state = arg;
        
        	state->send_mail_count--;
        
        	[...]
        	/* Get back to fsm only when the last mails has been sent */
        	if (state->send_mail_count == 0)
        		spamfilter_fsm(state, NULL, !strcmp(state->cs->status, "TRANS_MAIL_ERROR") ? state->cs->status : NULL);
        }

This breaks because the limit on the number of mail processes started may hit at
any point in the loop over mail_send(), at which point the callbacks are called,
which will then decrement 'state->send_mail_count' and hit zero, returning to the
FSM perhaps even before all the mail have been sent.

Instead of keeping this asynchronous code in the transaction, we can now use the
mail_queue abstraction to hide it.

IMPORTANT: Make sure you remove any call back into the FSM from the callback when using the mail_queue instead.

See CHANGES for more information on mail_queue and how to use it to solve this problem.

This issue was first reported by Blocket.

### Fix verification of PL/SQL procs in trans ###

trans was supposed to verify that the PL/SQL procs installed in the
database was the same ones that trans knew about. However, the test
had a bug and as a result the test always passed regardless of any
differences.

The bug has now been fixed. To make sure trans verifies the PL/SQL
procs the following three files may need changes:

* config.ninja: Set plsqlprocdir to the directory where your PL/SQL
procs are stored (e.g., scripts/db/plsql).

* start-build-pgsql.sh and blocket.spec: Make sure that the PL/SQL
procs from $plsqlprocdir and platform/scripts/db/plsql are installed.

## Indexer / Search ##

### Search ###

The search query parser has been rewritten. Although we've done extensive testing
some minor hiccups might occur. You should test carefully.

Known changes:

* Language specific operators are no longer recognized outside of free text.
  Instead you should use "NOT" and "OR" on attributes/joined searches.
  Most likely you already are, and have the English names as local language
  parameters. You can now remove those if you wish.
* The parser is slightly more strict while parsing the first part of the
  query (before *:*/*-*). These things will be rejected:
  
  * NOT without a matching attribute or word,
    e.g. `0 NOT *:*` instead of `0 NOT foo:1 *:*`
  * counters/filters without a complete attribute,
    e.g. `0 count(foo_counter):foo` instead of `0 count(foo_counter):foo:1`

### indexer/search ###

We've removed the 'invword/attr' counter functionality from the
search engine and indexer since it was broken and no one was using it.

You can remove any 'counters.invword' configuration from bconf.

### Magic hardcoded stopwords removed. ###

Since time immemorial the text parsing code in the indexer has had a
list of words that weren't indexed in some, but not all situations
with this comment:

    /* XXX make this better. */"

The list is: "HTTP", "WWW", "COM", "NET", "FR", "SE", "BE". Apparently
someone didn't want certain URLs to be indexed for some reason. If your
site is critically dependent on not indexing those words, please add
them to the standard bconf for stopwords.

### Search query parser made more consistent with the indexer ###

The code that parses the text part of the query in the search engine
(the part after *:* or *-*) has been made more consistent with the
indexer. The same rules are used for splitting text into words and
should generally mean more accurate matches between queries and
indexed text.

There's no action required from sites, this should give the same or
more results than before. This is just a heads up that in case
customers start complaining about saved queries suddenly returning
more results or other strange behaviors, don't hesitate to contact
the coordination team with example queries to test and explain the new
behavior.

An example is that if the ad text contained the string "foo / bar", in
the past the indexer would index the words "foo" and "bar", while if
the same string was indexed in the search engine the search engine
would look for the words "foo", "/" and "bar" and since "/" wasn't
indexed the search query wouldn't return a result.

## mod_templates ##

The jsencode filter will now change '<' and '>' to '\x3c' and '\x3e' to mitigate
XSS attacks. While not causing any problems on the clients it may break some tests.

# From 2.7.2 to 2.7.3 #

Update your git repository with the latest code from platform:

    git checkout -b upgrade_platform_2.7.3 <your-stable-branch>
    git remote add platform git@scmcoord.com:coord/platform.git
    git fetch platform
    git subtree --prefix=platform pull platform platform/2.7.3

# From 2.7.1 to 2.7.2 #

Update your git repository with the latest code from platform:

    git checkout -b upgrade_platform_2.7.2 <your-stable-branch>
    git remote add platform git@scmcoord.com:coord/platform.git
    git fetch platform
    git subtree --prefix=platform pull platform platform/2.7.2

# From 2.7.0 to 2.7.1 #

Update your git repository with the latest code from platform:

    git checkout -b upgrade_platform_2.7.1 <your-stable-branch>
    git remote add platform git@scmcoord.com:coord/platform.git
    git fetch platform
    git subtree --prefix=platform pull platform platform/2.7.1

# From 2.6.1 to 2.7.0 #

Update your git repository with the latest code from platform:

    git checkout -b upgrade_platform_2.7.0 <your-stable-branch>
    git remote add platform git@scmcoord.com:coord/platform.git
    git fetch platform
    git subtree --prefix=platform pull platform platform/2.7.0


## search ##

The format of the index has changed. This is mostly transparent for
small indexes, but you should be careful with large ones.

The first time index_merge is run after the upgrade the index will be
converted to the new format. There's no need for a full index.

Search can still run with old version index, but it has a big penalty
to both memory usage and startup time when doing so. It basically
converts to the new format during startup.

When running on an old version index, results sorted on suborder or an
attribute might also be slightly wrong, the order of documents with the
same sort value is random (specifically the order they were stored in).
Normally documents with the same sort value appears in the default order.

If these are concerns you should make sure to run index_merge to convert
your index manually. It's asing simple as

    /opt/blocket/bin/index_merge index index.new

Running index_merge will also take longer on the old version index.

## trans ##

For consistency the 'pwd' key of the trans database configuration has been renamed to 'password'.

	db.pgsql.master.pwd=xyz -> db.pgsql.master.password=xyz

For reasons of code hygiene, no fallback is provided. You must change your configuration and
restart (not reload) trans after release.

## mod_templates ##

The 'magic' discussed in the upgrade section for release 2.4.1 is no more.

The id_prefix matching has been slightly relaxed with regard to the
extraction of the list-id. We no longer assume that there's necessarily
a ".htm" (or any such four character string) at the end of the URL. We're
also excluding LONG_MAX as a valid list-id.

Action is needed iff you are running your site with

*.*.mod_templates.act_magic=<something>

If disabled, you can simply remove the line since this configuration is no longer
used.

If enabled, you will now have to fix the issues seen that made you enable it in
the first place. Please refer back to the aforementioned section for full details.

## lib/common ##

SIMPLEQ_REMOVE_NEXT is now SIMPLEQ_REMOVE_AFTER

The ftp module has been removed from the platform:

This code is known to be used by 'dav_put'. Unless there are local changes to how dav_put
is used though, it's unlikely that the FTP functionality is actually used. Historically
the FTP support in dav_put was used to upload movies.

For your convenience, a patch is provided in docs/remove-ftp-from-dav_put.diff

You need to update your your dav_put.conf.in; Remove all ftp mentions and in the
regress section, add

	regress=1

If you miss this step you will see dav_put zombies and errors in 'error_log'.

You can then remove the USE_FTP line from any Makefile and clean up REGRESS_FTPD_PORT
in mk/defvars.mk and perhaps remove conf/moftpd.conf.in


In the alternative, if for whatever reason you are using this code elsewhere, you can
chose to copy the library files into your local tree and continue on as normal.

## lib/common cacheapi ##

cache_init_pools() now returns a status int. The value will be positive if any
backend pools were initialized.

memcache_set() will return -1 on connection error instead of 0 like before.

It's unlikely that any client code is impacted by these changes.

## lib/search ##

`close_index` takes an additional flags parameter. You should use 0 for
this parameter.  There's one flag defined but it's only useful to
index_merge which is aware of index format internals.

## build/ninja ##

We recommended to add `cflags:prod[-DNDEBUG]` to `CONFIG`.
You should also recursively grep for `assert` and make sure they're
used properly, because they will now be ignored in your prod builds.

## build/regress-runner ##

regress-runner now have proper options handling. Where before the first argument was the output directory,
you must now give it as the '-o' or '--outdir' option.

(probably no external site using regress-runner)



# From 2.6.0 to 2.6.1 #

Update your git repository with the latest code from platform:

    git checkout -b upgrade_platform_2.6.1 <your-stable-branch>
    git remote add platform git@scmcoord.com:platform.git
    git fetch platform
    git subtree --prefix=platform pull platform platform/2.6.1

=== Paging Search [#532] ===

A paging search will only return the related header, paging_last, if there are more documents to be returned. 
Users of paging search can rely on the presence or absence of this header to know if they have 
consumed the documents returned from a paging search, for example the last page has been reached.

If users of paging were treating the absence of the paging_last header as an error, they would have to update 
their code accordigly.



# From 2.5.1 to 2.6.0 #

Update your git repository with the latest code from platform:

    git checkout -b upgrade_platform_2.6.0 <your-stable-branch>
    git remote add platform git@scmcoord.com:platform.git
    git fetch platform
    git subtree --prefix=platform pull platform platform/2.6.0

Build
=====

1. If you use REGRESS_HOSTNAME as defined by platform you may see it's changed from
a short name to a FQDN (this also depends on local configuration though)

2. The ninja variable `$buildroot` has been split up into `$destroot` and `$builddir`.
You should grep for this and replace them with one or the other.

`$destroot` is used for installed files, while `$builddir` is used for intermediary
files not meant to be part of the final installation.

3. We now require at least GCC 4.4 or clang 3.4 to compile.
If you're still on CentOS 5 you will have to install gcc44 and gcc44-c++
from the Base repo (available since CentOS 5.4), or another newer compiler.
CentOS 6 has gcc 4.4 as its default compiler.

If you're compiling with ninja, the newer compiler will be used automatically
but if you're still using make you will have to edit platform/mk/compile.mk
to set the correct binary.

4. Object symbols are now hidden by default, an EXPORTED macro has been added
to mark a symbol as exported, which mostly affect modules loaded via dlopen.
For apache modules:

	-module AP_MODULE_DECLARE_DATA templates_module = {
	+module AP_MODULE_DECLARE_DATA templates_module EXPORTED = {

You may also have to add 'platform_apache' to libs if building with ninja.

For PHP modules:

	#ifdef COMPILE_DL_TEMPLATES
	+#undef ZEND_DLEXPORT
	+#define ZEND_DLEXPORT EXPORTED
	ZEND_GET_MODULE_PROTOTYPE(template)
	ZEND_GET_MODULE(template)
	#endif

For perl modules, in the .xs file:

	+XS(boot_blocket__bconf) EXPORTED;
	+
	 MODULE = blocket::bconf		PACKAGE = blocket::bconf

For postgres related modules (e.g pguuid.c) either reset visibility to the default
as a work-around:

	+#pragma GCC visibility push(default)
	+
	PG_MODULE_MAGIC;

Or in the laternative, include "macros.h" and redefine PGDLLIMPORT to EXPORTED, and
then mark the individual exported functions as EXPORTED:

	+#include "macros.h"

	+#undef PGDLLIMPORT
	+#define PGDLLIMPORT EXPORTED
	 PG_MODULE_MAGIC;

	-Datum uuid(void);
	+Datum uuid(void) EXPORTED;

Swig modules are unaffected.

5. The compiler flags for C++ now include -Wmissing-declarations which may
trigger compilation errors in site code. See the section 'Compilation warnings'
in the UPGRADE section for 2.4.1 for the rationale.

This change was held back from that release because it was incompatible
with the old GCC shipped with CentOS 5.

Trans/mail
==========

Filenames in MIME attachements will now appear between quotes. This fixes an issue
with filenames that contains spaces. You should expect to see some tests fail due
to this change.

The build changes when using ninja may place 'sendmail.fake' such that you have to
update the local bconf for it to be found. You'll be able to tell if the mail tests
break completely if nothing else.

	-*.*.common.sendmail_path=%BDIR%/tools/libexec/sendmail.fake
	+*.*.common.sendmail_path=%BDIR%/../obj/regress/tools/libexec/sendmail.fake

Templates
=========

The &json_encode() and &js_encode() functions have been replaced with the <%|jsonencode|%>
and <%|jsencode|%> filters. The jsonencode filter behaves differently from the old function
which only did escaping of double-quotes and backslashes. The filter version encodes all
control characters either as their standard escaped form (\n etc) or as unicode-hex \u00xx,
and will escape the forward-slash (solidus) if passed a 1 as the first filter argument.

&js_encode seems to primarily be used in the template-based mobile API. If you are using it
but do not have good test coverage, keeping the old function in the local tree can be an
alternative.

Statpoints [#531]
=================

The API contained references to a 'parent' argument, but the code didn't actually use it
so we have removed all references to it. If you are calling 'stat_log()' from C you must
remove the last argument, expected to be NULL in existing code. Expect a number of such
calls in trans_{newad,deletead,flushmail}.

Any template calls to &stat_log with three arguments will continue to work as before, that
is to say, the parent argument if present will be ignored. Such calls should similarly be
updated to remove the last argument.



# From 2.5.0 to 2.5.1 #

Update your git repository with the latest code from platform:

    git checkout -b upgrade_platform_2.5.1 <your-stable-branch>
    git remote add platform git@scmcoord.com:platform.git
    git fetch platform
    git subtree --prefix=platform pull platform platform/2.5.1


# From 2.4.3 to 2.5.0 #

Update your git repository with the latest code from platform:

    git checkout -b upgrade_platform_2.5.0 <your-stable-branch>
    git remote add platform git@scmcoord.com:platform.git
    git fetch platform
    git subtree --prefix=platform pull platform platform/2.5.0

raw_set_cookie function changed
===============================

The raw_set_cookie function has been changed to only take expiration
time in seconds. All the callers in the platform tree have been
updated, but in case the funcion is used in local code, it needs to be
changed to either calculate the number of seconds in a minute or day
or preferably use one of the wrapper functions.

Swig
====

This release requires swig 2.0.10. It is available in scmcoord-bin.
Notice that any other swig package needs to be uninstalled for
this one to be used.

Python
======

To use the python bsapi interface, python 3.3 is required.

    yum install scmcoord-python3\*

installs it into /opt/scmcoord_contrib

Explicit rule dependencies with ninja
=====================================

When building with ninja with local rules, the way to create rule
dependencies has changed. Instead of `depend_rule_*` variables rule
dependencies are specified in the global CONFIG with the new
`ruledeps` argument. The format is:

    ruledeps[in:$inconf,$intool]

This example makes all targets generated with the rule `in` to depend
on `$inconf` and `$intool`. For more examples, see `Builddesc.config`
in the platform directory. [Also documented here](docs/COMPILING.md).

Build - _GNU_SOURCE
===================

_GNU_SOURCE is now defined globally. That means the define has to be
removed from all source files and headers to avoid compiler errors.
The following command might help, but will probably leave a few
`#ifdef _GNU_SOURCE`:

    grep -l '#define _GNU_SOURCE' * | xargs sed -i '/#define _GNU_SOURCE/d'

Latin1 to UTF8 conversion
=========================

An important quote from wikipedia:

[It is very common to mislabel Windows-1252 text with the charset
label ISO-8859-1. A common result was that all the quotes and
apostrophes (produced by “smart quotes” in word-processing software)
were replaced with question marks or boxes on non-Windows operating
systems, making text difficult to read. Most modern web browsers and
e-mail clients treat the MIME charset ISO-8859-1 as Windows-1252 to
accommodate such mislabeling. This is now standard behavior in the
draft HTML 5 specification, which requires that documents advertised
as ISO-8859-1 actually be parsed with the Windows-1252
encoding.](http://en.wikipedia.org/wiki/Windows-1252).

The `latin1_to_utf8` and `utf8_to_latin1` functions were updated to
follow that behavior. This shouldn't matter for most sites, the
changes will most likely be just to what the user sees in extreme
situations. It's recommended though to double check that things are
still behaving the same. Especially tests that depended on the old
behavior (outputing question marks) will need to be updated. Another
possible improvement is to relax mod_security rules and transaction
validators to accomodate for this.

Ninja - install rule changed
============================

The `install` rule has been split into `install_conf` and
`install_script`. If `install` was used in `specalsrcs` it needs to be
changed the appropriate rule instead. `install_script` makes the
target file executable, while `install_conf` doesn't.


# From 2.4.2 to 2.4.3 #

Update your git repository with the latest code from platform:

    git checkout -b upgrade_platform_2.4.3 <your-stable-branch>
    git remote add platform gitolite@scmcoord.com:platform.git
    git fetch platform
    git subtree --prefix=platform pull platform platform/2.4.3

The t-% Makefile target has been removed
========================================

The t-% rule in platform/mk/regress.mk has been removed as announced
in the 2.3.0. If your trans tests start failing, please review the
upgrade instructions for 2.3.0.

# From 2.4.1 to 2.4.2 #

Update your git repository with the latest code from platform:

    git checkout -b upgrade_platform_2.4.2 <your-stable-branch>
    git remote add platform gitolite@scmcoord.com:platform.git
    git fetch platform
    git subtree --prefix=platform pull platform platform/2.4.2

Changes to redirect trans filter
================================

Previously to this release, multiple calls to redirect would result in multiple 
Location headers being generated. Now the last call to redirect will win. However,
multiple calls to redirect is erroneous and will generate error messages in your logs.

# From 2.3.3 to 2.4.1 #

Update your git repository with the latest code from platform:

    git checkout -b upgrade_platform_2.4.1 <your-stable-branch>
    git remote add platform gitolite@scmcoord.com:platform.git
    git fetch platform
    git subtree --prefix=platform pull platform platform/2.4.1

Header tree.h updated
=====================

The BSD tree.h header has been updated to revision 1.10.2.2 from freebsd.org.
After the upgrade is it necessary to change RB_FIND_GTE to RB_NFIND (same prototype)

Changes to trans_conf filter
============================

The trans_conf filter, which works like the trans filter but accepts
additional parameters for greater control, was changed in the following way.

It used to be that you would optionally pass in a magic 'bconf_node'
string as the first argument, and follow that up with the path of the
bconf node you wanted to be used.

	OLD:
	<%|trans_conf("bconf_node", "common.transaction", "cmd", "help", "commit", "1")

Now the two first arguments are mandatory, and they should be the desired timeout
in milliseconds and the bconf path.

	NEW:
	<%|trans_conf(10000, "common.transaction", "cmd", "help", "commit", "1")

The timeout was hard-coded to 10000ms in the old code.

Template pgsql filters
======================

When using ninja, the pgsql filters have been split out from the rest to avoid
linking modules with libpq. For indexer this is handled automatically but if
you have any custom binaries using those filters (e.g. php_template_pgsql)
you will have to add `libs[platform_pgsql_filters]`.

Tokyo cabinet source removed
============================

The tokyocabinet library, still used by the search engine and mklangmap,
has been removed from the platform directory.

We provide a pre-compiled package 'scmcoord-libtokyocabinet1' containing
a static library and the necessary headers through our repository.
Please see https://scmcoord.com/wiki/Repo for instructions.

Alternatively, you can use packages from your distribution. Please note
that whereas our package provides a static library only, distributions
prefer dynamic libraries, which you must see to it are installed everywhere
you run search engines.

CentOS: Packages 'tokyocabinet-devel' and 'tokyocabinet' for runtime.
Ubuntu: Packages 'libtokyocabinet-dev' and 'libtokyocabinet8' for runtime.


Build changes
=============

REGRESS_HOST is no longer defined by the platform mk/defvars.mk,
and you should consider removing it locally if you have your own
definition. REGRESS_HOST is a hostname on some sites and an IP
on others. Ideally this shouldn't matter, but in practice this
often breaks tests and just generally makes it harder to move
between sites.

Instead, use REGRESS_HOSTNAME if you need a name, or REGRESS_HOSTIP
if you need an IP number.

Wholesale rewriting of the relevant patterns, indentified as
%REGRESS_HOSTNAME%, ${REGRESS_HOSTNAME} and $(REGRESS_HOSTNAME), can be
accomplished with something like this:

	find . -type f | xargs sed -r -i 's/(%|\$[{(])REGRESS_HOST([%})])/\1REGRESS_HOSTNAME\2/g'

Some occurences should no doubt be IPs instead, so some manual
updating may be warranted.

PHP templates language and country handling changed
===================================================

To improve on the code, a layering violation in the php_templates module
has been fixed. As a result, this module will no longer try to "detect"
language and country before calling the template, instead it is up to the
caller to pass in this information using the optional 'options' argument,
if it is not available as tmpl.{lang,country} in extended_data or
bconf_data already.

	call_template($simple_data, $extended_data, $bconf_data, "template.txt", $options);

Where $options is an array = ("lang" => "en", "country" => "gb", ...);

It used to be that the last argument could be a string representing the
language. That is no longer allowed.

You can add a function like the following to your init.php to 'wrap' the
lang selection and generate the options array in a way that should be
suitable for most uses.

	function default_template_options($lang = NULL) {
	       global $BCONF;

	       if (empty($lang) && bconf_get($BCONF, "*.common.multilang.enabled"))
		       $lang = @$_COOKIE['lang'];

	       if (empty($lang) || !bconf_get($BCONF, "*.common.lang.".$lang))
		       $lang = bconf_get($BCONF, "*.common.default.lang");

	       return array('lang' => $lang,
			    'country' => bconf_get($BCONF, "*.common.site_country")
	       );
	}

Call site example:

	call_template($simple_data, $extended_data, $bconf_data, $template, default_template_options($this->b__lang));

Don't confuse the global call_template() with the bResponse method with
the same name. If you keep the signature of the bResponse call, the number
of places you have to update should be relatively limited.

Regular expressions in templates
================================

For consistency we're moving to use the same regular-expression
engine, PCRE ("Perl Compatible Regular Expressions), for all code.

Pattern matching expressions in templates, "=~", involving variables
or bare strings (i.e excluding the regex construct R[]), was not
using PCRE but POSIX.

In the old code, POSIX BRE syntax was expected UNLESS one or
both of the flags 'i' and 'e' were used, in which case the
REG_EXTENDED flag was set and POSIX Extended Regular Expressions
(ERE) were expected.

http://www.regular-expressions.info/posix.html

	"The Basic Regular Expressions or BRE flavor standardizes
	a flavor similar to the one used by the traditional UNIX
	grep command. This is pretty much the oldest regular expression
	flavor still in use today. One thing that sets this flavor
	apart is that most metacharacters require a backslash to give
	the metacharacter its flavor. Most other flavors, including
	POSIX ERE, use a backslash to suppress the meaning of
	metacharacters. Using a backslash to escape a character that
	is never a metacharacter is an error."

You need to inspect any such comparisons that use character classes
and/or metacharacters and make sure the escaping is correct.

	grep -R --color "=~ \W*[^R]" templates

To match a literal, you should escape ^$+?()[{\| outside of
character classes, and ^-]\ inside. The following guide to PCRE/BRE/ERE
meta-escaping may be useful.

	http://stackoverflow.com/a/400316

Relatively few changes to client templates are expected as a result
of this change.

Mod_templates magic uri matching
================================

The mapping from uri to templates in mod_templates::default_get_template_name()
contains several "Magic" rules which don't really make sense but have been
kept around for legacy reasons.

The following applies directly ONLY if you are using the default lookup
function, which most sites are.

## configuration & instructions ##

Three new configuration options have been added, with these default values:

    *.*.mod_templates.act_magic=0
    # Magic rules are logged at level 1, normal rules at 9
    *.*.mod_templates.rule_log.level=1
    *.*.mod_templates.rule_log.all=0

This means that whenever the old behaviour is hit, it is logged to the
apache error_log, but not acted upon.

You should test your site in regress/QA using these flags and see if
anything breaks.

For the first production release you can release with act_magic=1 first for
safety, and keep an eye on the log to make sure nothing is logged. Issues
will have to be solved on a case-by-case basis. Please be mindful at
release, having the logging activated can result in heavy pressure on
the error_log.

The 'rule_log.all' flag can be set to log all requests, not only those
that are OK (result in a template render). This might be useful for general
debugging of template lookup problems, but should never be used in production
of course.

The rules run when act_magic is set are to be considered deprecated, and
will be removed together with the flag in a subsequent platform release.


## integration ##

The extra logging unfortunately resulted in a change to the prototype for
get_template_name, where we have added the second argument, 'rule'. If you
redefine 'mod_template_get_template_name' in local code, you need to update
it.

    extern const char *(*mod_template_get_template_name)(struct request_rec *r, struct rule_rec *rule, const char **tmpl_idx, const char **alias_idx, unsigned long *id, char *id_prefix, size_t id_prefix_sz, const char **path_info);

You can look at the platform version of 'default_get_template_name' for
how to use the RETURN_TEMPLATE_RULE_TAG macro to add support for the new
logging, if you so wish.


## details ##

We will log "MAGIC 1 DEPRECATION" when the rule is hit that states
that if the uri ends with a backslash, send the user to the url formed by
prefixing it by the default template root and suffixing it with 'index.html'.

	Example 1: site.com/help/ -> site.com/docs/help/index.html

We will log "MAGIC 2 DEPRECATION" when the rule is hit that states
that if the requested uri does not end with '.htm', then return the url
formed by prefixing it by the default template root and suffixing it with
'/index.html'

	Example 2: site.com/help -> site.com/docs/help/index.html

We will log "MAGIC 3 DEPRECATION" when the catch-all at the end of
the function is reached, which creates the url by concatenating the
default template root to the uri and adding an 'l', presumably to
generate the following mapping:

	Example 3: site.com/info.htm -> site.com/docs/info.html

Where the 'default template root' is given by *.*.mod_templates.default.root

The fact that we might not act on some of the earlier rules any more will
probably mean that the catch-all is called more often.

The suggested remedy when these rules are hit is to add aliases.

You may find that your / (index) page hits the third magic rule. This is
because the / will be parsed into '/index.htm' on its way through the handlers,
this is then mapped by magic rule 3 to /docs/index.html. Adding the an alias
solves the issue:

	*.*.common.template.14.name=docs/index.html
	*.*.common.template.14.alias.0=/index.htm


## prefix matching changes ##

We have moved the prefix_path rule so that we do prefix path matching
/after/ id prefix matching. This allows these two different rules to
share a set of prefixes and yield the 'correct' match. This should
only be an issue if you have BOTH id_prefix AND path_prefix in you
template configuration.

Specifically this issue would arise when you have listing page uris
and view ad uris which both start the same way, say with a region, and
you want to replace mod_list with a mod_templates implementation.

Reversing the order of the rules works better because an id_prefix
rule will only match iff the uri contains a list-id, so it's more specific
than the prefix path rule.

In addition, this rule no longer requires a slash at the end to
delimit a path segment. It used to be that that

	site.com/somepath/

would try to match 'somepath' against the prefix_path table,
whereas

	site.com/somepath

Would not. We would have to duplicate the whole prefix_path
configuration as aliases to get the wanted effect, which seemed
wasteful. If for some reason you need these to go to different
templates you are probably doing something wrong, but it could
be accomplished using a shim-template that checks for the presence
of the slash.

Redesign of init_func
=======================

`init_func` has been removed and replaced by `filter_state`. The documentation of how to use the new functionality is on the wiki at https://scmcoord.com/wiki/Filter_State

Please read the wiki and platform filters to see how you need to adapt your local filters if they used `init_func`.

Below is the documentation of the likely changes you need to do to fix some fallout.

## pgsql_session ##

In the past there were three different ways to set up the connection info for the `pgsql_session` filter. The indexer set up the fullest conninfo with port numbers and all other information from the `index.db` bconf node. The show_template application had a less functional conninfo string that was built up from the `db` bconf node. The php_templates module called a user provided function called `pgsql_filter_init` that returned the conninfo string which usually was pregenerated from an .in file which may or may not have accessed bconf. All this has been unified into one consistent mechanism.

The rendering of the conninfo string has been taken from the indexer since it had the fullest support and it supports the following vtree nodes for db configuration: `host`, `port`, `user`, `password`, `dbname` and `options`. Those nodes can be from the global vtree or local application config. Another node is needed though and it is HIGHLY RECOMMENDED that it is located in the local configuration for the application and not in global bconf. That node is `pgsql_filter.db_node`. To repeat for SECURITY REASONS the `pgsql_filter.db_node` should be in LOCAL CONFIGURATION and NOT in global bconf. The node should point out where the rest of the db configuration is, so for the indexer to keep the same functionality as before it should be:

    pgsql_session.db_name=index.db

Most sites will have to add `pgsql_filter.db_node=index.db` into their various search.conf, at least asearch.conf. If you have show_template applications that talk to the database they will have to add `pgsql_filter.db_node=db` to their configurations to keep working like before. If you have php_templates applications, you'll need to figure out how their conninfo was rendered and replicate that with bconf. Look for the `pgsql_filter_init` functions and see what they are doing, most likely they include php code generated from an in file, replicate the information in that in file in bconf nodes and provide local application configuration for that php_templates application that points to that bconf node.

If you're using an old php version of show_template, you can add extra bconf with pgsql_session config and db config with the `bconf_extra_bconf` configuration directive for the php_bconf module. Remember that php stores its internal bconf with the application part, so the additional bconf should be in the form:

    *.pgsql_session.db_name=<something>.db


## Local versions of standard filters ##

For those that have local variations of search filters or some other filter that use init_func, please look at the original implementation of that filter and the changes should be obvious.

## building ##

USE_CTEMPLATES implies USE_FILTER_STATE so normally you should not run into any build problems due to missing linkage.

Config file comment changes
===========================

config_init_file(), used to load many .conf files 'as bconf', no
longer treats hash tags ('#') as comments unless they occur first
one the line.

This resolves an inconsistency between this code and how the bconf
backend in trans parses bconf.

Example:

	some.key=http://tpb.gov/test.html#content

The value of 'some.key' used to be 'http://tpb.gov/test.html',
now it will be 'http://tpb.gov/test.html#content'.

Use of such in-line comments are believed to be rare, but you can
check for them with something like:

	find . -name "*.conf*" | xargs grep "=.*#"

Compilation warnings
====================

The compiler flags now include -Wmissing-declarations which
will likely trigger compilation errors in site code.

The purpose of this warning is to catch cases where modules
are not including their own headers, or the header files are
incomplete or out of sync, the sort of things that can lead
to all sorts of problems.

The error will look something like this:

	file.c:NN: warning: no previous declaration for 'somesymbol'

In most of the cases it's just a matter of making the indicated
symbol 'static'. Less common is the case where the relevant
#include is actually missing.

Problem areas when upgrading site, in decreasing order of changes needed:
 * trans: Validator functions, transactions, backends
 * daemons: pixel, spiderpoints
 * modules: mod_list, psql_uuid
 * util: bcli, dav

For PHP modules you need to include "platform_php.h" and then
add ZEND_GET_MODULE_PROTOTYPE(name) before ZEND_GET_MODULE(name)

Any complains about *_RB* indicates you should either change the
RB_GENERATE() to RB_GENERATE_STATIC(), or forward-declare it
with RB_PROTOTYPE()


# From 2.3.2 to 2.3.3 #

Update your git repository with the latest code from platform:

    git checkout -b upgrade_platform_2.3.3 <your-stable-branch>
    git remote add platform gitolite@scmcoord.com:platform.git
    git fetch platform
    git subtree --prefix=platform pull platform platform/2.3.3

This is a bugfix release, requiring no special steps to upgrade.

# From 2.3.1 to 2.3.2 #

Update your git repository with the latest code from platform:

    git checkout -b upgrade_platform_2.3.2 <your-stable-branch>
    git remote add platform gitolite@scmcoord.com:platform.git
    git fetch platform
    git subtree --prefix=platform pull platform platform/2.3.2

This is a bugfix release, requiring no special steps to upgrade.

# From 2.2.2 to 2.3.1 #

Update your git repository with the latest code from platform:

    git checkout -b upgrade_platform_2.3.1 <your-stable-branch>
    git remote add platform gitolite@scmcoord.com:platform.git
    git fetch platform
    git subtree --prefix=platform pull platform platform/2.3.1

You will need to remove the build directory after upgrading to this release.

Bugfix release
==============

This is a quick release that fixes a serious search index bug
introduced in 2.3.0. There's no special release procedure, just follow
the 2.3.0 instructions. Don't update your site to 2.3.0 first and then
2.3.1, just go straight to 2.3.1, following the 2.3.0 instructions.

# From 2.2.2 to 2.3.0 #

Update your git repository with the latest code from platform:

    git checkout -b upgrade_platform_2.3.0 <your-stable-branch>
    git remote add platform gitolite@scmcoord.com:platform.git
    git fetch platform
    git subtree --prefix=platform pull platform platform/2.3.0

You will need to remove the build directory after upgrading to this release.


Third party sources removed
=================================

This release removes sources for many third party packages from the platform tree, you'll need to install them separately.
For some of them we're providing binary packages, mostly due to local modifications or custom compilation flags.

For installation instructions see https://scmcoord.com/wiki/Repo

At least these packages should be installed from there:

* scmcoord-libhiredis1
* scmcoord-pcre8 (CentOS only)
* scmcoord-libhunspell1
* scmcoord-libyajl2-2.0.1-1.x86_64

libevent 1.4.12 removed from tree
=================================

Both CentOS 5 and 6 provide libevent 1.4.13 as the packages 'libevent' and
'libevent-devel'.

Ubuntu 12.04 LTS provides libevent 1.4 through 'libevent-1.4' and
'libevent1-dev' (libevent 2.0 is also available)


Redis 2.0.4 removed
===================

Redis is now available in all the major distributions, so we have removed
the in-tree version. As of writing, CentOS5 and 6 provides version 2.4
through the package 'redis', and Ubuntu 12.04 version 2.2 through the
package 'redis-server'.

The exact version can be determined using yum or apt-cache respectively:

$ yum info redis
$ apt-cache show redis-server (or apt-get changelog redis-server)

Both production servers and development servers must have the redis
package installed, of course.

According to the redis documentation, upgrading from 2.0 to 2.2 or 2.4
should straight-forward as the new versions are supersets of 2.0.

On upgrading from 2.0 to 2.2: "it is very unlikely that you will
experience any problem upgrading your 2.0 instances to 2.2, as
2.2 can work as a drop in replacement for 2.0"
 -- https://raw.github.com/antirez/redis/2.2/00-RELEASENOTES

On upgrading from 2.2 to 2.4: "The only thing you should be aware
is that you can't use .rdb and AOF files generated with 2.4 into
a 2.2 instance. 2.4 slaves can be attached to 2.2 masters, but
not the contrary, and only for the time needed to perform the
version upgrade."
 -- https://raw.github.com/antirez/redis/2.4/00-RELEASENOTES

2.4 should be able to use .rdb and AOF files from 2.0.4, but
you may instead prefer to upgrade by replication. That's up to you.

Should you have redis 2.6 or later packages, please refer to their
documentation for instructions on how to migrate.

Caveats
-------

CentOS does not include /usr/sbin in the path per default for non-root
users, so you may need to address that issue on development machines.

The tree version contained an ipv6 patch. Full ipv6 support is not
coming to redis until 2.8 at the earliest, according to:
	https://github.com/antirez/redis/pull/61

Due to changes to redis-cli, some tests may break. One issue is
that a 'raw output' mode is used for redirected output, meaning you
will get \0a where previously you got (nil)\0a. You can chose to either
accept this, or use a workaround such as running through script:

	script -qc "redis-cli -p 1..."


Trans
=====

1. Important! For security reasons, the following sequence of validators
will no longer work:

        {"token", 0, "v_token"},
        {"remote_addr", 0, "v_ip"},

  because v_token depends on remote_addr, and is no longer allowed to be
used before remote_addr is validated. There's two ways to fix it, either
reverse the order of token and remote_addr, or (more simple) add

        VALIDATOR_OTHER("remote_addr", 0, "v_ip")

  to `v_token`.
  Same applies to any other local validators depending on other parameters.

  Some validator functions, e.g. `validate_token_fsm`, use `cmd_getval` to
  fetch the value of the validated parameter. This no longer works, use
  `param->value` instead.

  `validator_settings` has some conditionals that are difficult to fix
  because the ordering is alphabetic. Where it's using `cmd_haskey` you should
  instead use `cmd_getparam` to check existence. Affects `require_if` and
  `exists_if_has_key`.

  If you have parameters with NULL validators you will have to fix those
  because those parameters are considered unvalidated. NULL validators have
  been unsupported since platform v1 was released.

  `v_newad_params` has to be moved to the end of validation since it access
  other paramters, and `vf_newad_passwd_extra` has to split from email to
  be run at a later stage.
  
  Full diff for `trans_newad.c`:

        diff --git a/daemons/trans/trans_newad.c b/daemons/trans/trans_newad.c
        index 6a4dec3..bc971d1 100644
        --- a/daemons/trans/trans_newad.c
        +++ b/daemons/trans/trans_newad.c
        @@ -210,10 +210,7 @@ static void newad_done(void *v) {
         }
         
         static struct c_param parameters[] = {
        -        {"_pre",        0,        "v_newad_params"},
        -
                {"region",                 P_REQUIRED,            "v_region"},
        -        {"type",                 P_REQUIRED,            "v_type"},
                {"category",                 P_REQUIRED,            "v_category_check"},
                {"name",                P_REQUIRED, "v_name"},
                {"email",                 P_REQUIRED, "v_newad_email"},
        @@ -246,6 +243,7 @@ static struct c_param parameters[] = {
                {"gallery",                0,        "v_gallery"},
                {"addgallery",                0,        "v_bool"},
                {"lang",                 P_REQUIRED,        "v_lang"},
        +        {"type",                 P_REQUIRED,            "v_type"},
         
                /* Video */
                {"video_name",          0,      "v_string_isprint"},
        @@ -279,6 +277,8 @@ static struct c_param parameters[] = {
                /* Not to be stored in the DB */
                {"state",                0,        "v_integer"},
                
        +        {"_params",        0,        "v_newad_params"},
        +        {"_passwd",        0,        "v_newad_passwd_extra"},
                {NULL}
         };
         
        @@ -349,11 +349,16 @@ vf_newad_passwd_extra(struct cmd_param *param, const char *arg) {
         
         struct validator v_newad_email[] = {
                VALIDATOR_FUNC(vf_newad_check_account),
        -        VALIDATOR_FUNC(vf_newad_passwd_extra),
                {0}
         };
         ADD_VALIDATOR(v_newad_email);
         
        +struct validator v_newad_passwd_extra[] = {
        +        VALIDATOR_FUNC(vf_newad_passwd_extra),
        +        {0}
        +};
        +ADD_VALIDATOR(v_newad_passwd_extra);
        +
         void
         vf_minimum_price_cb(const char *setting, const char *key, const char *value, void *cb) {
                struct trans_key_lookup_data *data = cb;


2. trans_mail/mail_send() used to use a parameter called 'mailapp'
to determine whether or not to use the default wrapper template when
sending mail. If mailapp was 'sendmail', the wrapper would NOT be used.

  'mailapp' is now 'wrapper_template', and 'sendmail' is now 'none'.

  If, in your bconf, you have
        common.mail.mailapp=sendmail
  Change it to
        common.mail.wrapper_template=none
  else change it to
        common.mail.wrapper_template=mail/qp_wrapper.mime
  If not specified, it will default to this last version.

  In code, if you insert param "mailapp" with value "sendmail", change it
to "wrapper_template" and "none". Else, if it's "mailapp" and "nail",
remove it to get the default wrapper, or specify a new wrapper explicitly.

  Certainly you'll have to make many changes to your transactions where you
probably have code like:
        mail_insert_param(md, "mailapp", "sendmail");
  or transactions accepting mailapp as a parameter:
        {"mailapp",             P_REQUIRED,     "v_mailapp"},
  and transactions passing mailapp to internal transactions:
        "mailapp:sendmail\n"
  You may also have SQL that return 'mailapp' to transactions.

  Search for 'mailapp' over your whole code base, and make the required changes.


3. The following new configuration options are now available to trans.conf:

        log_level=info
  Set the general logging level, usually 'info' or 'debug'.
  Also available as the --loglevel option.

        log_tag=trans
  Mirrors --logtag option.

        debug=0
  DPRINTF log level INFO (0), or DEBUG (1)
  Mirrors --debug option.
  This option can also have different effects beyond just logging,
  some of which may not take effect if enabled on an already
  running trans through a reload.

  If you use the provided trans init script there should be no change in
default behaviour, but if you start trans manually or use your own script,
you will want to add "log_level=debug" or "--loglevel debug" for regress.

  Note that debug=1 does not imply anything about log_level.

4. cmd:test is only built in platform regress trans. If you still have old
cmd:test tests in daemons/trans/ you should remove them.

tcmalloc removed
================

tcmalloc has been removed from the platform tree and is no longer used
by default.

If you want to use tcmalloc, you need to install it separately. We
provide the old 0.97 version that was in the tree in our repo, or
you can install some other version. Then you have to set TCMALLOC_PATH
to the path of the library in your mk/defvars.mk:

	TCMALLOC_PATH=/opt/scmcoord_contrib/lib

If you were using this option before this upgrade, you need to
add '/lib' to the path (previously implicit).

As before, it will only be used where USE_TCMALLOC is set.


random_seed removed
===================

The template function 'random_seed' has been removed. A random seed should always be available now,
so calls to this function should be removed.


Search
======

1. The cache size in the search engine configuration has changed. It used to be configured as:

    cache_size=1000

The value was in number of entries in the cache which wasn't very useful since one entry can be between 50B and 100 MB. The new cache sizing strategy makes you explicitly set how much memory the cache should eat. To make things more complicated, the old parameter was used for three different caches, now they are all configured separately. The new configuration parameters are:

    cache_memory.query_cache_mb=512
    cache_memory.subquery_cache_mb=50
    cache_memory.attrsort_cache_mb=50

These value tells how much memory the caches should take, the unit is MB.

The three caches are:

 - query_cache - is the main cache for result of search queries.
 - subquery_cache - currently only used for `cache_here` queries. Will be used more in the future for caching partial queries.
 - attrsort_cache - cache for alternative sortings of search results.

You can ignore the configuration for the caches you don't use.

If left unconfigured it will default to 100MB for the query cache and 10% of that for the other two caches. If you don't change your current "cache_size" configuration our best guess is that you can allow your cache grow to 0.5GB. The best strategy to tune this is to carefully see how much memory the search engines eat and scale it so that `cache_memory_mb < 0.4 * (<memory in machine> - 2 * <size of index>).`. Although be careful and start from lower values. The search engine without a cache takes a lot of memory. Ramp it up slowly and don't let the machine swap. 


2. Support for init_dirty_pages has been removed. You should remove it from
your conf files.


3. max_reserve_threads option is added. It defaults to threads / 5.
search will spawn up to this number of extra threads if running out and
there's blocked threads waiting on cache entry.


Modules
=======

1. The signature of mod_template_get_template_name has changed,
you will have to update your local version, if any.
Specifically `int *alias_idx` changed to `const char **alias_idx`,
you're expected to set it to a bconf key.

2. The bconf path cacheapi uses for session configuration is now
configurable. It used to be that memcache would be set up from
"common.session" and redis from "common.redis".

These MUST now be configured as follows:

    *.*.common.session.backend.memcache.node=common.session
    *.*.common.session.backend.redis.node=common.redis

NOTE! You'd have to update client code (e.g php session handlers)
in order to be able to change these defaults to something else
in practice. Even so, they are REQUIRED.


Templates
=========

1. The template function 'include_var' has been deprecated and will be removed
in an upcoming release.

  If you use this function, you have two options. You can either a) copy
it from platform/lib/template/deprecated_functions.c into you local tree,
or b) rewrite your code to use https://scmcoord.com/wiki/Deffilter instead.

2. The pgsql and pgsql_escape filters MUST now be used from within a pgsql_session only.
You must locate any use of these filters and change the templates to conform:

	<%|pgsql_session|
		<%|pgsql("sql_", "SELECT 1 as foo")|%sql_foo%>
		...
	%>

  This change was made to simplify the code, and to fix the pgsql_escape filter which
was completely broken in previous versions.


Makefiles
=========

The t-% rule in platform/mk/regress.mk will be removed in
the next release. You must copy it into your local mk/regress.mk


# From 2.2.1 to 2.2.2 #

Update your git repository with the latest code from platform:

    git checkout -b upgrade_platform_2.2.2 <your-stable-branch>
    git remote add platform gitolite@scmcoord.com:platform.git
    git fetch platform
    git subtree --prefix=platform pull platform platform/2.2.2

Any db.pgsql.*.debug.transinfo.connid configuration can be removed from trans.conf, this is now enabled always.

# From 2.2.0 to 2.2.1 #

Update your git repository with the latest code from platform:

    git checkout -b upgrade_platform_2.2.1 <your-stable-branch>
    git remote add platform gitolite@coordination.blocket.com:platform.git
    git fetch platform
    git subtree --prefix=platform pull platform platform/2.2.1

1. We've split util.{c.h} into several different files;
    lib/common/{string,date,memalloc}_functions.{c.h}
`util.h` still exists and will include all of these, but
we have removed many includes from it which may break
client code which relied on the implicit inclusion of
some files through this header.

`util.h` will be removed in the future, at which point all
code that includes it will have to be updated to include
the correct new file(s) instead.

2. transapi_add_printf
Extended transapi with a printf-style function. Example:

    trans_add_printf(trans, "cmd:somecmd\n"
    	"arg:whatever\n"
    	"suspend:%d\n"
    	"log_string:%s\n"
    	"commit:1\n",
    	suspend, cmd_getval(cs, "log_string")
    );

3. The following functions, deprecated in the 2.2.0 release, have now
been removed. This should not have any impact on the upgrade,
assuming the previous instructions were followed.  Removed:
`area_descr`, `format_weeks`, `format_price`, `format_zipcode`,
`format_cents`, `cat_type_name`.

4. The minimum delay (in seconds) between trans dumping debug-info
into the log, after reaching the connection limit, can be controlled
via the 'debug_dump_delay' trans.conf key. If not set, it will default
to 10.

5. If using ninja, you need to add platform/scripts/build/gcc.ninja to the
rules directive of CONFIG in the top level Builddesc. clang can be used
as an alternative compiler, but we don't recommend that for now.

6. The `test` transaction has been removed from normal trans. If you
have regression tests depending on this transaction you can copy the
file from platform/regress/trans/trans_test.c into your local
trans and add it to the local build. It's recommended to not include
it in production.

# From 2.1.3 to 2.2.0 #

Update your git repository with the latest code from platform:

    git checkout -b upgrade_platform_2.2.0 <your-stable-branch>
    git remote add platform gitolite@coordination.blocket.com:platform.git
    git fetch platform
    git subtree --prefix=platform pull platform platform/2.2.0

1. You will need to add a second pair of ports to trans.conf.
For regress use the variables `%REGRESS_TRANS_CPORT%` and
`%REGRESS_TRANS_CFPORT%`:

    control_port = %REGRESS_TRANS_CPORT%
    control_failover_port = %REGRESS_TRANS_CFPORT%

They have the offsets 7 and 9, respectively.
For production we recommend port 5660 and 5661:

    control_port = 5660
    control_failover_port = 5661

You will have to update any makefile that sed or otherwise
manipulate these values. (i.e daemons/trans/Makefile)

2. For 'url_encode()', add USE_URL=yes and include 'url.h'

3. We have deprecated a slew of template functions:
  	area_descr, format_weeks, format_price, format_zipcode, format_cents
  
On merge, they should be COPIED from platform/lib/template/deprecated_functions.c
into the local tree as needed. Under NO CIRCUMSTANCE should the existing platform
implementation be linked in.

4. In trans, `init_base` and `reload_base` have been merged into a single event_base, called
    init_reload_base
You will have to fix the code that used to reference `init_base` (e.g. MAMA backends initializers) and
replace it with `init_reload_base`.

5. Starting from this version, the Search Engine syntax honors properly tags passed in for:

    count_level(tag), count_tree(tag), bucket(tag)

Most of the sites beside Shopping don't use those operators, with the exception of count_level.
If you used to specify tags for count_level() in your code, be aware that the resulting infos will use the tag instead of 'count_level'
as was previously the case.
Example:
    count_level(zakay):attrind:a-zzzzz
Now returns
    info:zakay:attrind\:<blah>:1
instead of
    info:count_level:attrind\:<blah>:1

# From 2.1.2 to 2.1.3 #

Update your git repository with the latest code from platform:

    git checkout -b upgrade_platform_2.1.3 <your-stable-branch>
    git remote add platform gitolite@coordination.blocket.com:platform.git
    git fetch platform
    git subtree --prefix=platform pull platform platform/2.1.3

This update introduces an optional `templates_log_level` directive in php.ini. It defaults to info.

# From 2.1.1 to 2.1.2 #

Update your git repository with the latest code from platform:

    git checkout -b upgrade_platform_2.1.2 <your-stable-branch>
    git remote add platform gitolite@coordination.blocket.com:platform.git
    git fetch platform
    git subtree --prefix=platform pull platform platform/2.1.2

For some sites (ex. Risk), we had to fix some symbol name conflicts during the V2 upgrade (ex. 'tsearch()'), which means that you might end up having
conflicts when upgrading to this version due to MR !59. Use the version provided with this upgrade, since it's cleaner and it's the one which will be used by anyone else after the upgrade.

If you're having problems resolving those conflicts, do not hesitate to ask us.

Clean up your binaries, by either doing a:

    make cleandir

or by removing the build dir:

    rm -rf build/

Then rebuild everything and make sure that all your test pass and push your branch to your central repository:

    git push -u origin upgrade_platform_2.1.2

Do all the required steps to test and release the new version.


# From 2.1.0 to 2.1.1 #

This new upgrade will require a (hopefully) final sync with the site's tree.

For people using Git (should be everyone after the Workshop), this are the steps that you should follow:

Prepare a branch to work on:

    git checkout <your-base-branch>
    git checkout -b upgrade_platform_2.1.1

Add the platform "remote":

    git remote add platform gitolite@coordination.blocket.com:platform.git
    git fetch platform

Extract all the diffs you have in your local platform tree against Version 2.1.0 (the last version before the directory move):

    git diff platform/2.1.0 upgrade_platform_2.1.1 -- platform/ > local_platform_changes.diff

Then you'll have to edit the diff file, namely reverting a few file "remove" (the 'VERSION' file and the 'CHANGES.md' file are
good examples, but you might discover that you're missing a few regress tests and similar). Make sure that what's left contains
only your local changes.
Then, you can get rid of the old "platform" directory. Just move it for the moment:

    git mv platform/ old-platform
	git commit -m "Rename the old platform directory"

Then, add back in the platform code using git subtree:

    git subtree add --prefix=platform platform platform/2.1.1

Now you're ready to reapply the local changes:

	patch -p1  < local_platform_changes.diff

You might need to fix some conflicts. Once you're satisfied with that (Don't do any extra modification!), commit the result:

    git commit -m "Reapply local changes to platform directory"

And test that everything is working fine with the updated version:

	make build

We've tested this step with one site (Risk) and the process went fine. Do not hesitate to report any problem you might encounter in this step, we'll make
sure to include the relevant fixes in the next "patch" release.

Once everything looks good, you can finally get rid of the 'old-platform' directory:

	git rm old-platform
	git commit -m "Remove the old-platform directory"

That's it, you're ready to apply the future upgrades by issuing the command:

    git subtree --prefix=platform pull platform stable


# From pre-2.1.0 to 2.1.0 #

This has been handled by the Coordination Team, every site is already running this version.
