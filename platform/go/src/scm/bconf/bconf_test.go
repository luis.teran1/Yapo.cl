package bconf

import (
	"os"
	"testing"
)

func TestBconfBasic(t *testing.T) {
	bconf := New()
	err := bconf.InitFromFile(os.Getenv("DEST") + "/regress/conf/bconf.conf")
	if err != nil {
		t.Error(err)
	}
}
