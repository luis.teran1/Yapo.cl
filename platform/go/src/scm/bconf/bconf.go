package bconf

import (
	"fmt"
)

// #include "bconf.h"
// #include "bconfig.h"
// typedef struct bconf_node bcnode;
import "C"


// XXXcgo LDFLAGS: -lplatform_core -lplatform_util -ldl -lrt



type Bconf struct {
	n *C.bcnode
};

// Returns an empty bconf we can work with.
func New() Bconf {
	return Bconf{}
}

// Initialize the base config from a config file.
func (b *Bconf) InitFromFile(filename string) error {
	b.n = C.config_init(C.CString(filename))
	if b.n == nil {
		return fmt.Errorf("config_init failed");
	}
	return nil
}

// Loads a bconf from a proper bconf file.
// (this usually requires a pre-loaded config so that we can get the blocket_id)
func (b *Bconf) LoadBconfFile(appl, filename string) error {
	if e, err := C.load_bconf_file(C.CString(appl), &b.n, C.CString(filename)); e != 0 {
		return err
	}
	return nil
}

// Gets a bconf string from the bconf.
func (b Bconf) GetString(k ...string) (string, bool) {
	n := b.n
	if n == nil {
		return "", false
	}
	// We can't pass vargargs into C, we need to iterate ourselves
	for _, v := range k {
		n = C.bconf_get(n, C.CString(v))
		if n == nil {
			return "", false
		}
	}
	v := C.bconf_value(n)
	if v == nil {
		return "", false
	}
	return C.GoString(v), true
}
