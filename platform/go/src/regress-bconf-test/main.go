package main

import (
	"scm/bconf"
	"fmt"
	"flag"
	"os"
	"strings"
)

var confFile = flag.String("config", "", "Config file")
var bconfFile = flag.String("bconf", "", "Bconf file")
var bconfKey = flag.String("key", "common.ad_params.1.name", "Bconf key to fetch")

func usage() {
	fmt.Fprintf(os.Stderr, "usage: hw --config=<config> --bconf=<bconf> [--key=<key>]\n")
	flag.PrintDefaults()
	os.Exit(1)
}

func main() {
	flag.Parse()

	if *confFile == "" || *bconfFile == "" {
		usage()
	}

	b := bconf.New()
	b.InitFromFile(*confFile)
	b.LoadBconfFile("hw", *bconfFile)
	keys := strings.Split(*bconfKey, ".")
	s, _ := b.GetString(keys...)
	fmt.Printf("foo: %v -> %v\n", keys, s)
}
