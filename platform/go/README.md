# Go code.

This directory follows the standard layout of normal go build
environment.

The components are built with Builddescs using `GOPROG(foo)`
to build the program in the directory.

# CGO and internal build dependencies.

Most go programs can be built without any additional
configuration. But to ensure that our build system dependencies are
set up correctly for cgo packages that link to our libraries, a CGO
package needs to set up a fake LIB to express its library
dependencies. For example, the bconf wrapper has this:

    LIB(go-bconf
        libs[platform_core platform_util dl rt]
    )

This is a fake library that doesn't build or install anything itself,
but it expresses which libraries we depend on. Also, the build system
figures out the libraries to link with just like when building normal
libraries and sets the environment variable CGO_LDFLAGS when building
with this library.

A program that uses this package will then look like this:

    GOPROG(foo
        libs[go-bconf]
    )

This program will not be built until the dependencies for the fake library
are satisfied.

You can optionally add gopkg[foo] to manually set the package name, in case
the build script fails to figure it out from the directory name, but it
shouldn't be needed in most cases.

It's also possible to add go unit tests to our regress system. This is done
by adding a marker in Builddesc:

    GOTEST(foo
    )

You will have to add and libs etc. needed since it compiles much like a
GOPROG. You can then use the `gotest-%` target defined in `regress.mk`
to execute `go test`, e.g.

    REGRESS_TARGETS+=gotest-foo
