#!/bin/bash

PYTHON="$(type -p python3 || type -p python)"

install -m0644 "$@" || exit 1

"$PYTHON" -m py_compile "$2" && "$PYTHON" -O -m py_compile "$2"
s=$?
test $s != 0 && rm -f "$2"
exit $s
