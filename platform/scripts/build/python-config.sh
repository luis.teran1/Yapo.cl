#!/bin/bash

config="$(type -p python3-config || type -p python2-config || type -p python-config || type -p python2.6-config)"
exec "$config" "$@"
