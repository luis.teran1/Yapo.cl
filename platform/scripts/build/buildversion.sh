#!/bin/bash
export LANG="C"
	if [ -f .buildversion.cache ]; then
		read cached_sha1 cached_revno < .buildversion.cache
	fi
	sha1=`git log --format=%H -1 2>/dev/null`
	if [ "$cached_sha1" = "$sha1" ]; then
		revno=$cached_revno
	else
		revno=`git rev-parse --short HEAD 2>/dev/null`
		[ -n "$revno" -a "$revno" != 0 ] && echo $sha1 $revno > .buildversion.cache
	fi
if [ -z "$revno" ]; then
	exit 1
else
	echo $revno
fi
