
TOPDIR=${TOPDIR:-.}
BUILDPATH=${BUILDPATH:-build}
FLAVOR=${FLAVOR:-regress}

DEST=${TOPDIR}/${BUILDPATH}/${FLAVOR}
DEST_BIN=${DEST}/bin
BUILDROOT=${TOPDIR}/${BUILDPATH}/obj/${FLAVOR}

while read _invar; do
	eval "${_invar%%=*}=\"${_invar#*=}\""
done < ${BUILDROOT}/tools/in.conf
unset _invar
