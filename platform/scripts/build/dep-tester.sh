#!/bin/sh

# This is a brute force ninja dependency tester.
# We generate the ninja files, extract all the targets from them and then build each target in a fresh build tree.
# Use this once in a while to make sure that all the dependencies are good enough to at least see if the final targets
# build correctly.

if [ -f /etc/redhat-release ] && [ -z "$NO_SCL" ] && [ -f /usr/bin/scl ] && ! scl_enabled devtoolset-2; then
	exec scl enable devtoolset-2 "$0 $*"
fi

BUILDVERSION=`./scripts/build/buildversion.sh`
test -z "$BUILDPATH" && BUILDPATH=build
BUILDBUILD=./scripts/build/build-build.pl

rm -rf $BUILDPATH

$BUILDBUILD

echo $BUILDVERSION > $BUILDPATH/version

ninja -f $BUILDPATH/build.ninja -n -t targets | grep -v analyse | sed -r 's,:[^:]+$,,' > $BUILDPATH/test-targets

err=0
for a in `cat $BUILDPATH/test-targets` ; do
	echo TESTING $a
	rm -rf $BUILDPATH
	$BUILDBUILD
	echo $BUILDVERSION > $BUILDPATH/version
	ninja -f $BUILDPATH/build.ninja $a
	if [ $? != "0" ] ; then
		echo DEPENDENCY FAIL FOR $a >&2
		err=1
	else
		test -t 1 && printf '\e[3A\e[J'
	fi
done
test -t 1 && printf '\e[3B'
exit $err
