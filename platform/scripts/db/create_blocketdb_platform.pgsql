
CREATE ROLE breaders;
CREATE ROLE bwriters;

-- example table
CREATE TABLE example (
	id integer PRIMARY KEY,
	col integer default 0
);
GRANT select,insert,delete,update ON example to bwriters;
GRANT select on example TO breaders;

CREATE SEQUENCE trans_queue_id_seq;

CREATE TABLE trans_queue (
	trans_queue_id bigint PRIMARY KEY default nextval('trans_queue_id_seq'), 
	added_at timestamp NOT NULL,
	added_by varchar(80) NOT NULL,
	execute_at timestamp DEFAULT NULL,
	executed_at timestamp default NULL,
	executed_by varchar(80) default NULL,
	status varchar(80) default NULL, -- XXX not null?
	locked_at timestamp default NULL,
	locked_by varchar(80) default NULL,
	command text NOT NULL,
	response text,
	queue varchar(30) NOT NULL,
	sub_queue varchar(30) DEFAULT NULL,
	info varchar(60) DEFAULT NULL,
	critical bool NOT NULL DEFAULT 'f'
);

GRANT select,insert,delete,update ON trans_queue TO bwriters;
GRANT select on trans_queue TO breaders;
GRANT select,insert,delete,update ON trans_queue_id_seq TO bwriters;
GRANT select on trans_queue_id_seq TO breaders;

CREATE INDEX index_trans_queue_status ON trans_queue (status);
CREATE INDEX index_trans_queue_command ON trans_queue (command text_pattern_ops);
CREATE INDEX index_trans_queue_executed_at ON trans_queue (executed_at);
CREATE INDEX index_trans_queue_queue_null_executed_at ON trans_queue (queue, (executed_at IS NULL));
CREATE INDEX index_trans_queue_locked_at ON trans_queue USING btree (locked_at);
CREATE INDEX index_trans_queue_at_nullstats_execute_at ON trans_queue (execute_at) WHERE status IS NULL and queue = 'AT';
CREATE INDEX index_trans_queue_at_unrun_sub_queue_info ON trans_queue (sub_queue, info) WHERE queue = 'AT' AND status IS NULL;
CREATE INDEX index_trans_queue_monthly_deleted_isrun ON trans_queue ((executed_at IS NULL)) WHERE queue = 'monthly_deleted';

CREATE TABLE conf (
	key varchar(255) PRIMARY KEY,
	value text default NULL,
	modified_at timestamp NOT NULL default CURRENT_TIMESTAMP
);
GRANT select,insert,delete,update ON conf TO bwriters;
GRANT select ON conf TO breaders;
