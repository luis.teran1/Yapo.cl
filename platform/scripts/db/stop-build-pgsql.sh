#!/bin/bash

test -z "$1" && exit 1
test ${1%data*} = $1 && exit 1
cd $1 || exit 1
export PGDATA=`pwd`
cd -

if [ "$2" ]; then
	PIDFILE=$2
else
	PIDFILE=$TOPDIR/.postgres.pid
fi

if [ "$3" ]; then
	LOGFILE=$3
else
	LOGFILE=$TOPDIR/.pgsql.log
fi

while kill -INT $(<$PIDFILE) 2>/dev/null; do echo "stopping postgres"; sleep 1; done

rm -rf $PGDATA
rm -f $LOGFILE $PIDFILE

exit 0