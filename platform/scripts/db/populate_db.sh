
echo "Adding plsqlprocs"
# plsqlprocs
for proc in ${DEST}/plsql/*.sql
do
	echo "FILE $proc" >> ${TOPDIR}/.psql.stderr
	env PGOPTIONS="-c search_path=$BPVSCHEMA,public" psql $DSNARGS blocketdb < $proc > /dev/null 2>> ${TOPDIR}/.psql.stderr
done

