#!/bin/bash

test -z "$1" && exit 1
test "${1%data*}" = "$1" && exit 1
rm -rf $1/* 
mkdir -m 700 -p $1 >/dev/null
cd $1 || exit 1
PGDATA=`pwd`
cd -
export PGDATA

if [ -n "$2" ]; then
	rm -rf $2/*
	mkdir -m 700 -p $2 > /dev/null
	cd $2 || exit 1
	PGDATA_REPLICA=`pwd`
	cd -
fi

PGVERSION=`postgres --version | egrep -o [0-9]\.[0-9]`
if [ "$3" != "" ]; then
	PGLOCALE=" --locale=$3"
fi

# Try to find the local timezone for a speed boost.
export TZ=`tail -1 /etc/localtime | sed -e 's/[^A-Z].*//'`

echo "Installing modules"

echo "Starting Postgresql ($PGVERSION$PGLOCALE)"
if [ "$PGVERSION" = "8.4" -o $(expr "$PGVERSION" ">=" 9) = 1 ] ;  then
	initdb --pgdata=$PGDATA --auth="ident" $PGLOCALE >/dev/null
else
	initdb --pgdata=$PGDATA --auth="ident sameuser" $PGLOCALE >/dev/null
fi

if [ -n "$2" ]; then
	sed 's,^#\(local.*replication\),\1,g' -i $PGDATA/pg_hba.conf
fi

if test -f postgresql.conf; then
	cat postgresql.conf >> $PGDATA/postgresql.conf
	if [ $(expr "$PGVERSION" ">=" 9.2) = 1 ]; then
		sed -i -e '/custom_variable_classes/d' $PGDATA/postgresql.conf
	fi
	if [ $(expr "$PGVERSION" "<" 9) = 1 ]; then
		sed -i -e '/wal_level\|max_wal_senders\|wal_keep_segments/d' $PGDATA/postgresql.conf
	fi
fi

setsid postgres -h '' -k $PGDATA -D $PGDATA 1> $TOPDIR/.pgsql.log < /dev/null 2>&1 &
echo $! > $TOPDIR/.postgres.pid

echo -n "Creating blocketdb"
DSNARGS="-h $PGDATA"
while !  createdb $DSNARGS blocketdb 2>/dev/null; do
	/bin/sleep 0.2
	echo -n "."
done
echo

if [ -n "$PGDATA_REPLICA" ]; then
	echo "Starting Postgresql replica ($PGVERSION$PGLOCALE)"

	pg_basebackup -h $PGDATA -D $PGDATA_REPLICA

	echo "hot_standby = 'on'" >> $PGDATA_REPLICA/postgresql.conf

	printf "standby_mode = 'on'\nprimary_conninfo = 'host=$PGDATA'\n" > $PGDATA_REPLICA/recovery.conf	

	rm -f $PGDATA_REPLICA/.s.PGSQL.*.lock

	setsid postgres -h '' -k $PGDATA_REPLICA -D $PGDATA_REPLICA 1> $TOPDIR/.pgsql_replica.log < /dev/null 2>&1 &
	echo $! > $TOPDIR/.postgres_replica.pid	
fi

sleep 10


echo "Connect with psql $DSNARGS blocketdb"
echo "or export PGHOST=$PGDATA PGDATABASE=blocketdb"

rm -f ${TOPDIR}/.psql.stderr

if [ "$PGVERSION" = "8.3" -o "$PGVERSION" = "8.4" ]; then
	echo "Creating language plpgsql"
	env PGOPTIONS="-c search_path=$BPVSCHEMA,public" psql $DSNARGS blocketdb -c "CREATE LANGUAGE plpgsql;" > /dev/null 2>> ${TOPDIR}/.psql.stderr || exit 1
fi

echo "Adding module procs"

echo "Creating schemas"
psql $DSNARGS blocketdb < create_blocketdb_platform.pgsql > /dev/null 2>> ${TOPDIR}/.psql.stderr || exit 1

# Create procedure schema
psql $DSNARGS blocketdb < ${DEST_BIN}/create_proschema.pgsql > /dev/null 2>> ${TOPDIR}/.psql.stderr || exit 1

source ${PLATFORMDIR}/scripts/db/populate_db.sh

psql $DSNARGS postgres -c "CREATE DATABASE blocketdb_pristine TEMPLATE blocketdb;" > /dev/null 2>> ${TOPDIR}/.psql.stderr || exit 1

if grep -C2 '^ERROR:' ${TOPDIR}/.psql.stderr ; then
    exit 1
fi
rm -f ${TOPDIR}/.psql.stderr
