CREATE OR REPLACE FUNCTION at_clear_commands(
		i_queue trans_queue.queue%TYPE,
		i_sub_queue trans_queue.sub_queue%TYPE,
		i_info trans_queue.info%TYPE,
		OUT o_trans_queue_id trans_queue.trans_queue_id%TYPE
		) RETURNS SETOF trans_queue.trans_queue_id%TYPE AS $$
BEGIN
	FOR
		o_trans_queue_id
	IN
		UPDATE
			trans_queue
		SET
			status = 'CLEARED',
			executed_at = CURRENT_TIMESTAMP
		WHERE
			queue = i_queue
			AND sub_queue = i_sub_queue
			AND info = i_info
			AND status IS NULL
		RETURNING
			trans_queue_id
	LOOP
		RETURN NEXT;
	END LOOP;
END;
$$ LANGUAGE plpgsql;
