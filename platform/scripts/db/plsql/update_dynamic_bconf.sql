CREATE OR REPLACE FUNCTION update_dynamic_bconf(
		i_key conf.key%TYPE,
		i_value conf.value%TYPE
		) RETURNS VOID AS $$
BEGIN
	LOOP
		UPDATE conf SET value = i_value WHERE key = i_key;
		IF found THEN
			RETURN;
		END IF;

		BEGIN
			INSERT INTO conf (key, value) VALUES (i_key, i_value);
			RETURN;
		EXCEPTION WHEN unique_violation THEN
			-- Try again
		END;
	END LOOP;
END;
$$ LANGUAGE plpgsql;
