#!/bin/sh

SCRIPT_DIR=`dirname ${0}`
TOPDIR=${SCRIPT_DIR}/..

if [ -e ${TOPDIR}/VERSION ]; then
	PREV_VERSION=`cat ${TOPDIR}/VERSION`
	GIT_PREV_TAG="platform/${PREV_VERSION}"

	GIT_EXTRA_PARAM="${GIT_PREV_TAG}.."
fi

git --no-pager log --format="%ai %aN %n%n%x09* %s%d%n" ${GIT_EXTRA_PARAM}
