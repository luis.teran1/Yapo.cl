#!/bin/bash

FILE="$1"
MODE="${2:-100644}"

CONTENT="$(</dev/stdin)"
# Note: This strips a newline, so one is added below (printf + echo instead of echo -n).

sha1sum=$(type -p sha1sum || type -p shasum)
deflate=(perl -MCompress::Zlib -e 'undef $/; print compress(<>)')

BLOB_ID=$(printf "blob %d\0%s\n" $(echo "$CONTENT" | wc -c) "$CONTENT" | $sha1sum | cut -d ' ' -f 1)

if git cat-file -t "$BLOB_ID" >& /dev/null; then
	otype="$(git cat-file -t "$BLOB_ID")"
	ocont="$(git cat-file -p "$BLOB_ID")"
	if [[ "$otype" != "blob" || "$ocont" != "$CONTENT" ]]; then
		echo "ERROR: object $BLOB_ID exists but doesn't match" >&2
		return 1
	fi
else
	mkdir -p "objects/${BLOB_ID:0:2}"
	printf "blob %d\0%s\n" $(echo "$CONTENT" | wc -c) "$CONTENT" | "${deflate[@]}" > "objects/${BLOB_ID:0:2}/${BLOB_ID:2}"
fi
git update-index --add --cacheinfo "$MODE" "$BLOB_ID" "$FILE"
