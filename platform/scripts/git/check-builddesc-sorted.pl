#!/usr/bin/perl

# Much of the parsing is copied from build-build.pl

use strict;

my $data = join("", <>);
my $exit = 0;

while (!($data =~ /\G$/sgc)) {
	# Eat whitespace
	if ($data =~ /\G[ \t\n]+/sgc) {
		print $&;
		next;
	}

	$data =~ /\G([A-Z_]+)\(([^)]*)\)/sg or die "can't parse '$data'";

	if ($1 ne "COMPONENT") {
		print $&;
		next;
	}

	print "$1(";

	my $l = $2;

	while ($l =~ /\G([ \t\n]*)([A-Za-z_]*)(:([A-Za-z0-9_]+))?\[([^]]*)\]([ \t\n]*)/sg) {
		if ($2 . $3 ne '') {
			print $&;
			next;
		}
		print "${1}[";
		my $suff = $6;

		my @comps = split(/\n/, $5);
		my @sortcomps = sort @comps;

		# Use a unique exit code to not conflict with perl errors.
		$exit = 13 if join("\n", @comps) ne join("\n", @sortcomps);

		print join("\n", @sortcomps);

		print "\n]$suff";
	}

	print ")";
}

# For some reason the last newline is stripped.
print "\n";

exit $exit;
