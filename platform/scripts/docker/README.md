Base images for running platform tests via docker.

Complete steps for building the centos6 platform image:
```bash
cd platform-centos6
docker build -t platform-centos6 .
```

To run the platform tests in the newly built image, execute the following
command:
```bash
DOCKER_IMAGE=<imagename> RUNTESTS=1 ./compile.sh
```

