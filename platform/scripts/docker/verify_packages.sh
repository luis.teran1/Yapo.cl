#!/bin/bash

if [ -z "$DOCKER_IMAGE" ]; then
	DOCKER_IMAGE="$1"
	[ -z "$DOCKER_IMAGE" ] && exit 1
fi

DOCKER_TOPDIR=$(dirname "$0")
DOCKER_FILE=${DOCKER_TOPDIR}/$DOCKER_IMAGE/Dockerfile

COMMANDS="RUN|yum|apt-get|install|-y"
REPOS="http|epel-release|software-properties-common|scmcoord-repo"
WHITELIST="vim|valgrind|strace|gdb"

PACKAGES=($(grep -w install "$DOCKER_FILE" | xargs -n 1 | grep -vwE "$COMMANDS|$REPOS|$WHITELIST"))

if test -z "$PACKAGES"; then
	echo "Failed to parse packages from $DOCKER_FILE" >&2
	exit 1
fi

if grep -qw yum "$DOCKER_FILE"; then
	REMOVE_CMD="yum remove -y"
else
	REMOVE_CMD="apt-get purge -y"
fi

DOCKER_CMD="RUNTESTS=1 NO_DOCKER=1 REGRESS_RUNNER_FLAGS=--exit-on-error ./compile.sh"

DOCKER_DONT_RUN=1
source docker.sh

for pkg in "${PACKAGES[@]}"; do
	rm -rf build
	$DOCKER_RUN bash -c "
		$setup
		$REMOVE_CMD $pkg
		$runtests
	"
	s=$?
	if [ $s = 2 ]; then
		# Exit = 2 seems to indicate someone presses ^C
		echo "Interrupted" >&2
		break
	fi
	if [ $s = 0 ]; then
		TO_REMOVE=("${TO_REMOVE[@]}" "$pkg")
	fi
done

if [ -n "$TO_REMOVE" ]; then
	echo "These packages can be removed on $DOCKER_IMAGE: " "${TO_REMOVE[@]}"
	exit 1
fi
exit 0
