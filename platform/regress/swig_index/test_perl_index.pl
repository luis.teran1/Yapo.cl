
use strict;

use search_index qw(:all);

index_log_setup("swig_perl", "debug");

my $db = openindex("test_perl", INDEX_WRITER, 0) or die "openindex";

my $doc = opendoc($db, 123, 1, 0);

doc_add_attr($doc, "foofoo", "barbar", AIND | AATTR);

store_doc($db, $doc, 1);

close_index($db, 0);

for (1 .. 3) {
	my $db = indexprimenew("test_perl_shard", -1, 0);

	my $doc = opendoc($db, 123 + $_, 1, 0);

	doc_add_attr($doc, "foofoo", "$_", AIND | AATTR);

	store_doc($db, $doc, 1);

	close_index($db, 0);
}
