<?php
	openlog("php_templates", LOG_ODELAY, LOG_LOCAL0);

	function template_template_statuscode($code) {
		$status = "HTTP/1.0 $code";
		header($status);
		syslog(LOG_DEBUG, "function ".__FUNCTION__.": ".$status);
	}

	$template_vars["var1"] = "Variabel 1";
	$template_vars["template"] = "shared/".$argv[2].".txt";

	$bconf["common"]["default"]["lang"] = "en";
	$notbconf['notcached'] = 0;

	call_template_foo($template_vars, $notbconf, $bconf, $template_vars["template"]);

	closelog();
?>
