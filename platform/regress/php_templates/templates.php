<?php
	$template_vars["var1"] = "Variabel 1";
	$template_vars["list1"] = array("fnargel", "pargel", "nisse", "pisse", "popp");
	$template_vars["list3"] = array(1, 2, 3, 4, "5");
	$template_vars["list2"] = array("A", "B");
	$template_vars["lists"] = array(1, 2, 3);
	$template_vars["list4"] = array(4, 7, 1, 3, 2);
	$template_vars["list5"] = array(4.0, 7.1, 1.2, 3.3, 2.4);
	$template_vars["list6"] = array("Pensador Mexicano", "Peo", "pema", "Pe��n de los Ba�os");
	$template_vars["pos"] = 2;
	$template_vars["cat"] = 4;
	$template_vars["region"] = 2;
	$template_vars["template"] = "test/template1.txt";
	$template_vars["dbdata"] = "' \\ '";
	$template_vars["category"] = "46";
	$template_vars["var1johan"] = "Variabel 1";

	$bconf["a"]["b"]["c"]["d"] = "ABCD";
	$bconf["a"]["b"]["c"]["ABCD"] = "abcABCD";
	$bconf["a"]["b"]["fnargel"]["d"] = "FNARGEL";
	$bconf["a"]["b"]["nisse"]["d"] = "NISSE";
	$bconf["a"]["b"]["pargel"]["d"] = "PARGEL";
	$bconf["a"]["b"]["pisse"]["d"] = "PISSE";
	$bconf["x"]["y"]["1"] = "XY1";
	$bconf["x"]["y"]["2"] = "XY2";
	$bconf["x"]["y"]["3"] = "XY3";

	$bconf["common"]["categories"]["order"]["0"] = "8";
	$bconf["common"]["categories"]["order"]["1"] = "7";

	$bconf["common"]["category"]["7"]["name"] = "Fnargel";
	$bconf["common"]["category"]["7"]["price"] = "20";
	$bconf["common"]["category"]["8"]["name"] = "Mielse";
	$bconf["common"]["category"]["8"]["price"] = "30";

	$bconf["common"]["region"]["1"]["name"] = "Reg1";
	$bconf["common"]["region"]["1"]["cities"]["1"] = "Arjeplog";
	$bconf["common"]["region"]["1"]["cities"]["2"] = "Arvidsjaur";
	$bconf["common"]["region"]["1"]["cities"]["3"] = "Boden";
	$bconf["common"]["region"]["1"]["cities"]["4"] = "G�llivare";
	$bconf["common"]["region"]["1"]["cities"]["5"] = "Haparanda";
	$bconf["common"]["region"]["1"]["cities"]["6"] = "Jokkmokk";
	$bconf["common"]["region"]["1"]["cities"]["7"] = "Kalix";
	$bconf["common"]["region"]["1"]["cities"]["8"] = "Kiruna";
	$bconf["common"]["region"]["1"]["cities"]["9"] = "Lule�";
	$bconf["common"]["region"]["1"]["cities"]["10"] = "Pajala";
	$bconf["common"]["region"]["1"]["cities"]["11"] = "Pite�";
	$bconf["common"]["region"]["1"]["cities"]["12"] = "�lvsbyn";
	$bconf["common"]["region"]["1"]["cities"]["13"] = "�verkalix";
	$bconf["common"]["region"]["1"]["cities"]["14"] = "�vertorne�";
	$bconf["common"]["region"]["2"]["name"] = "Reg2";
	$bconf["common"]["region"]["3"]["name"] = "Reg3";
	$bconf["common"]["region"]["3"]["cities"]["1"]["name"] = "City1";
	$bconf["common"]["region"]["4"]["name"] = "Reg4";
	$bconf["common"]["region"]["5"]["name"] = "Reg5";


	$bconf["ejtest"]["category_order"]["0"] = "8";
	$bconf["ejtest"]["category_order"]["1"] = "7";
	$bconf["ejtest"]["category_order"]["2"] = "1";
	$bconf["ejtest"]["category_order"]["3"] = "2";
	$bconf["ejtest"]["category_order"]["4"] = "4";

	$bconf["ejtest"]["category"]["1"]["name"] = "Fnargel";
	$bconf["ejtest"]["category"]["1"]["price"] = "20";
	$bconf["ejtest"]["category"]["1"]["level"] = "0";
	$bconf["ejtest"]["category"]["2"]["name"] = "Mielse";
	$bconf["ejtest"]["category"]["2"]["price"] = "30";
	$bconf["ejtest"]["category"]["2"]["level"] = "1";
	$bconf["ejtest"]["category"]["2"]["break"] = "space";
	$bconf["ejtest"]["category"]["4"]["name"] = "Karatepinnar";
	$bconf["ejtest"]["category"]["4"]["price"] = "30";
	$bconf["ejtest"]["category"]["4"]["level"] = "1";
	$bconf["ejtest"]["category"]["7"]["name"] = "Igelkottar";
	$bconf["ejtest"]["category"]["7"]["price"] = "30";
	$bconf["ejtest"]["category"]["7"]["level"] = "0";
	$bconf["ejtest"]["category"]["7"]["break"] = "col";
	$bconf["ejtest"]["category"]["8"]["name"] = "Zakayer";
	$bconf["ejtest"]["category"]["8"]["price"] = "30";
	$bconf["ejtest"]["category"]["8"]["level"] = "1";

	$bconf["configs"]["category"]["46"]["config"]["features"]["keys"]["1"]["name"] = "apartment_type";
	$bconf["configs"]["category"]["46"]["config"]["features"]["keys"]["1"]["optional"] = "";
	$bconf["configs"]["category"]["46"]["config"]["features"]["keys"]["2"]["name"] = "type";
	$bconf["configs"]["category"]["46"]["config"]["features"]["keys"]["2"]["optional"] = "";
	$bconf["configs"]["category"]["46"]["config"]["features"]["rentals"]["u"]["value"] = "apartment_type,safepayment,displace_zipcode,zipcode_hint,rooms,size,zipcode,monthly_rent,address,bjornsbytare";
	$bconf["configs"]["category"]["46"]["config"]["features"]["rentals"]["b"]["value"] = "apartment_type,safepayment,displace_zipcode,zipcode_hint,rooms,size,zipcode,address,monthly_rent,info_swap,bjornsbytare";
	$bconf["configs"]["category"]["46"]["config"]["features"]["rentals"]["h"]["value"] = "apartment_type,safepayment,displace_zipcode,zipcode_hint,rooms,size,max_rent";
	$bconf["configs"]["category"]["46"]["config"]["features"]["tenant_ownership"]["s"]["value"] = "apartment_type,safepayment,pricelist:3,displace_zipcode,zipcode_hint,rooms,size,monthly_rent,zipcode,address";
	$bconf["configs"]["category"]["46"]["config"]["features"]["tenant_ownership"]["u"]["value"] = "apartment_type,safepayment,pricelist:3,displace_zipcode,zipcode_hint,rooms,size,zipcode,monthly_rent,address,bjornsbytare";
	$bconf["configs"]["category"]["46"]["config"]["features"]["tenant_ownership"]["h"]["value"] = "apartment_type,safepayment,pricelist:3,displace_zipcode,zipcode_hint,rooms,size,max_rent";
	$bconf["configs"]["category"]["46"]["config"]["features"]["tenant_ownership"]["b"]["value"] = "apartment_type,safepayment,pricelist:3,displace_zipcode,zipcode_hint,rooms,size,zipcode,address,monthly_rent,info_swap,bjornsbytare";
	$bconf["configs"]["category"]["46"]["config"]["features"]["tenant_ownership"]["k"]["value"] = "apartment_type,safepayment,pricelist:3,displace_zipcode,zipcode_hint";
	$bconf["configs"]["category"]["46"]["config"]["features"]["value"] = "apartment_type";

	$bconf["common"]["transaction"]["host"]["1"]["name"] = "localhost";
	$bconf["common"]["transaction"]["host"]["1"]["port"] = $argv[1];

	$bconf['test']['var1'] = 'var1';
	$bconf['test']['number'] = '1111';

	$notbconf['notcached'] = 0;
	$bconf['cached'] = 0;

	$bconf['test']['comma_seperated_string'] = "knatte,fnatte,tjatte";

	$bconf['settings']['features']['1']['46']['rentals']['b']['value'] = "apartment_type,safepayment,displace_zipcode,zipcode_hint,rooms,size,zipcode,address,monthly_rent,info_swap,bjornsbytare";
	$bconf['settings']['features']['1']['46']['rentals']['h']['value'] = "apartment_type,safepayment,displace_zipcode,zipcode_hint,rooms,size,max_rent";
	$bconf['settings']['features']['1']['46']['rentals']['u']['value'] = "apartment_type,safepayment,displace_zipcode,zipcode_hint,rooms,size,zipcode,monthly_rent,address,bjornsbytare";
	$bconf['settings']['features']['1']['46']['tenant_ownership']['b']['value'] = "apartment_type,safepayment,pricelist:3,displace_zipcode,zipcode_hint,rooms,size,zipcode,address,monthly_rent,info_swap,bjornsbytare";
	$bconf['settings']['features']['1']['46']['tenant_ownership']['h']['value'] = "apartment_type,safepayment,pricelist:3,displace_zipcode,zipcode_hint,rooms,size,max_rent";
	$bconf['settings']['features']['1']['46']['tenant_ownership']['k']['value'] = "apartment_type,safepayment,pricelist:3,displace_zipcode,zipcode_hint";
	$bconf['settings']['features']['1']['46']['tenant_ownership']['s']['value'] = "apartment_type,safepayment,pricelist:3,displace_zipcode,zipcode_hint,rooms,size,monthly_rent,zipcode,address";
	$bconf['settings']['features']['1']['46']['tenant_ownership']['u']['value'] = "apartment_type,safepayment,pricelist:3,displace_zipcode,zipcode_hint,rooms,size,zipcode,monthly_rent,address,bjornsbytare";
	$bconf['settings']['features']['1']['keys']['1'] = "category";
	$bconf['settings']['features']['1']['keys']['2'] = "apartment_type";
	$bconf['settings']['features']['1']['keys']['3'] = "type";


	$bconf['settings']['features']['2']['46']['value'] = "apartment_type";
	$bconf['settings']['features']['2']['keys']['1'] = "category";

	$bconf["common"]["default"]["lang"] = "en";
	$bconf["common"]["locale"] = "es_US";

	$options = array("lang" => $bconf["common"]["default"]["lang"]);

	call_template_foo($template_vars, $notbconf, $bconf, "test/template.txt", $options);

	$template_vars["apartment_type"] = "tenant_ownership";
	$template_vars["type"] = "s";

	$notbconf['notcached'] = 1;
	$bconf['cached'] = 1;
	call_template_foo($template_vars, $notbconf, $bconf, "test/template.txt", $options);
?>
