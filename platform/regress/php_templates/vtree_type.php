<?php
	$vtree["list"][] = "a";
	$vtree["list"][] = "b";
	$vtree["list"][] = "c";

	$vtree["dict"]["d"] = "e";
	$vtree["dict"]["f"] = "g";
	$vtree["dict"]["h"] = "i";

	// Just using "0", "1", "2" won't trigger the expected case since
	// PHP will convert the keys to integers.
	$vtree["unknown"]["00"] = "j";
	$vtree["unknown"]["01"] = "k";
	$vtree["unknown"]["02"] = "l";

	$vtree["dict2"]["1"] = "m";
	$vtree["dict2"]["2"] = "n";
	$vtree["dict2"]["3"] = "o";

	call_template_foo(array(), $vtree, array(), "test/vtree_type.txt")
?>
