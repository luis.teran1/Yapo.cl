<?php
	$template_vars["foo"] = "bar";
	$notbconf['notcached'] = 0;
	$bconf['cached'] = 0;

	$bconf["common"]["redis"]["master"] = "m";
	$bconf["common"]["redis"]["host"]["m"]["name"] = "localhost";
	$bconf["common"]["redis"]["host"]["m"]["port"] = "12345";
	$bconf["common"]["redis"]["db"] = "0";
	$bconf["common"]["redis"]["timeout"] = "1000";

	$bconf["common"]["default"]["lang"] = "en";

	call_template_foo($template_vars, $notbconf, $bconf, "test/redis_filter_fail_log_error.txt", array("lang" => $bconf["common"]["default"]["lang"]));
?>
