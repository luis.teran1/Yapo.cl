#include "ctemplates.h"
#include "bconf.h"
#include <stdlib.h>
#include <stdio.h>
#include <time.h>

struct bconf_node *root = NULL;

const char *(*orig_vget)(struct bpapi_vtree_chain *vchain, enum bpcacheable *cc, int argc, const char **argv);

static const char *
testvtree_get(struct bpapi_vtree_chain *vchain, enum bpcacheable *cc, int argc, const char **argv) {
	if (strcmp (argv[0], "notcached") == 0)
		*cc = BPCACHE_CANT;
	return orig_vget(vchain, cc, argc, argv);
}

static int test_settings_filter(int argc, char* argv[]) {
	struct bpapi api;

	bconf_add_data(&root, "isset_settings.testop.1.keys.1", "?isset");
	bconf_add_data(&root, "isset_settings.testop.1.true.value", "result:set");
	bconf_add_data(&root, "isset_settings.testop.1.false.value", "result:unset");
	bconf_add_data(&root, "isset_settings.testop.1.monkey.value", "result:isset-monkey");

	bconf_add_data(&root, "isset_settings.testop.2.default", "result:isempty-fail");

	bconf_add_data(&root, "isempty_settings.testop.1.keys.1", "!isempty");
	bconf_add_data(&root, "isempty_settings.testop.1.true.value", "result:empty");
	bconf_add_data(&root, "isempty_settings.testop.1.false.value", "result:notempty");
	bconf_add_data(&root, "isempty_settings.testop.1.monkey.value", "result:isempty-monkey");

	bconf_add_data(&root, "isempty_settings.testop.2.default", "result:isempty-fail");

	struct bpapi_vtree vtree;

	BPAPI_INIT_BCONF(&api, root, stdout, hash);

	filter_state_bconf_init_all(&root);
	filter_state_bconf_set(&root, "define", (void *)2, NULL, FS_GLOBAL);
	filter_state_bconf_set(&root, "config_lookup", root, NULL, FS_GLOBAL);
	filter_state_bconf_set(&root, "bconf_js", root, NULL, FS_GLOBAL);

	api.filter_state = filter_state_bconf;
	vtree = *api.vchain.fun;
	orig_vget = vtree.get;
	vtree.get = testvtree_get;
	api.vchain.fun = &vtree;

	call_template(&api, "test/settings_filter.txt");

	bpapi_free(&api);

	return 0;
}

static int test_legacy(int argc, char* argv[]) {
	struct bpapi api;
	int i;

	bconf_add_data(&root, "a.b.c.d", "ABCD");
	bconf_add_data(&root, "a.b.c.ABCD", "abcABCD");
	bconf_add_data(&root, "a.b.fnargel.d", "FNARGEL");
	bconf_add_data(&root, "a.b.nisse.d", "NISSE");
	bconf_add_data(&root, "a.b.pargel.d", "PARGEL");
	bconf_add_data(&root, "a.b.pisse.d", "PISSE");

	bconf_add_data(&root, "x.y.1", "XY1");
	bconf_add_data(&root, "x.y.2", "XY2");
	bconf_add_data(&root, "x.y.3", "XY3");

	bconf_add_data(&root, "common.categories.order.0", "8");
	bconf_add_data(&root, "common.categories.order.1", "7");

	bconf_add_data(&root, "common.category.7.name", "Fnargel");
	bconf_add_data(&root, "common.category.7.price", "20");
	bconf_add_data(&root, "common.category.8.name", "Mielse");
	bconf_add_data(&root, "common.category.8.price", "30");

	bconf_add_data(&root, "common.region.1.name", "Reg1");
	bconf_add_data(&root, "common.region.1.cities.1", "Arjeplog");
	bconf_add_data(&root, "common.region.1.cities.2", "Arvidsjaur");
	bconf_add_data(&root, "common.region.1.cities.3", "Boden");
	bconf_add_data(&root, "common.region.1.cities.4", "G\xE4llivare"); /* Gällivare */
	bconf_add_data(&root, "common.region.1.cities.5", "Haparanda");
	bconf_add_data(&root, "common.region.1.cities.6", "Jokkmokk");
	bconf_add_data(&root, "common.region.1.cities.7", "Kalix");
	bconf_add_data(&root, "common.region.1.cities.8", "Kiruna");
	bconf_add_data(&root, "common.region.1.cities.9", "Lule\xE5"); /* Luleå */
	bconf_add_data(&root, "common.region.1.cities.10", "Pajala");
	bconf_add_data(&root, "common.region.1.cities.11", "Pite\xE5"); /* Piteå */
	bconf_add_data(&root, "common.region.1.cities.12", "\xC4lvsbyn"); /* Älvsbyn */
	bconf_add_data(&root, "common.region.1.cities.13", "\xD6verkalix"); /* Överkalix */
	bconf_add_data(&root, "common.region.1.cities.14", "\xD6vertorne\xE5"); /* Övertorneå */
	bconf_add_data(&root, "common.region.2.name", "Reg2");
	bconf_add_data(&root, "common.region.3.name", "Reg3");
	bconf_add_data(&root, "common.region.3.cities.1.name", "City1");
	bconf_add_data(&root, "common.region.4.name", "Reg4");
	bconf_add_data(&root, "common.region.5.name", "Reg5");


	bconf_add_data(&root, "ejtest.category_order.0", "8");
	bconf_add_data(&root, "ejtest.category_order.1", "7");
	bconf_add_data(&root, "ejtest.category_order.2", "1");
	bconf_add_data(&root, "ejtest.category_order.3", "2");
	bconf_add_data(&root, "ejtest.category_order.4", "4");

	bconf_add_data(&root, "ejtest.category.1.name", "Fnargel");
	bconf_add_data(&root, "ejtest.category.1.price", "20");
	bconf_add_data(&root, "ejtest.category.1.level", "0");
	bconf_add_data(&root, "ejtest.category.2.name", "Mielse");
	bconf_add_data(&root, "ejtest.category.2.price", "30");
	bconf_add_data(&root, "ejtest.category.2.level", "1");
	bconf_add_data(&root, "ejtest.category.2.break", "space");
	bconf_add_data(&root, "ejtest.category.4.name", "Karatepinnar");
	bconf_add_data(&root, "ejtest.category.4.price", "30");
	bconf_add_data(&root, "ejtest.category.4.level", "1");
	bconf_add_data(&root, "ejtest.category.7.name", "Igelkottar");
	bconf_add_data(&root, "ejtest.category.7.price", "30");
	bconf_add_data(&root, "ejtest.category.7.level", "0");
	bconf_add_data(&root, "ejtest.category.7.break", "col");
	bconf_add_data(&root, "ejtest.category.8.name", "Zakayer");
	bconf_add_data(&root, "ejtest.category.8.price", "30");
	bconf_add_data(&root, "ejtest.category.8.level", "1");

	bconf_add_data(&root, "test.var1", "var1");

	bconf_add_data(&root, "settings.features.1.keys.1", "category");
	bconf_add_data(&root, "settings.features.1.keys.2", "apartment_type");
	bconf_add_data(&root, "settings.features.1.keys.3", "type");

	bconf_add_data(&root, "settings.features.1.46.rentals.u.value", "apartment_type,safepayment,displace_zipcode,zipcode_hint,rooms,size,zipcode,monthly_rent,address,bjornsbytare");
	bconf_add_data(&root, "settings.features.1.46.rentals.b.value", "apartment_type,safepayment,displace_zipcode,zipcode_hint,rooms,size,zipcode,address,monthly_rent,info_swap,bjornsbytare");
	bconf_add_data(&root, "settings.features.1.46.rentals.h.value", "apartment_type,safepayment,displace_zipcode,zipcode_hint,rooms,size,max_rent");
	bconf_add_data(&root, "settings.features.1.46.tenant_ownership.s.value", "apartment_type,safepayment,pricelist:3,displace_zipcode,zipcode_hint,rooms,size,monthly_rent,zipcode,address");
	bconf_add_data(&root, "settings.features.1.46.tenant_ownership.u.value", "apartment_type,safepayment,pricelist:3,displace_zipcode,zipcode_hint,rooms,size,zipcode,monthly_rent,address,bjornsbytare");
	bconf_add_data(&root, "settings.features.1.46.tenant_ownership.h.value", "apartment_type,safepayment,pricelist:3,displace_zipcode,zipcode_hint,rooms,size,max_rent");
	bconf_add_data(&root, "settings.features.1.46.tenant_ownership.b.value", "apartment_type,safepayment,pricelist:3,displace_zipcode,zipcode_hint,rooms,size,zipcode,address,monthly_rent,info_swap,bjornsbytare");
	bconf_add_data(&root, "settings.features.1.46.tenant_ownership.k.value", "apartment_type,safepayment,pricelist:3,displace_zipcode,zipcode_hint");

	bconf_add_data(&root, "settings.features.2.keys.1", "category");
	bconf_add_data(&root, "settings.features.2.46.value", "apartment_type");

	bconf_add_data(&root, "test.number", "1111");

	bconf_add_data(&root, "test.comma_seperated_string", "knatte,fnatte,tjatte");

	bconf_add_data(&root, "common.locale", "es_US");

	filter_state_bconf_init_all(&root);
	filter_state_bconf_set(&root, "define", (void *)2, NULL, FS_GLOBAL);
	filter_state_bconf_set(&root, "config_lookup", root, NULL, FS_GLOBAL);
	filter_state_bconf_set(&root, "bconf_js", root, NULL, FS_GLOBAL);

	for (i = 0; i < (argc == 2 ? atoi(argv[1]) : 1); i++) {
		struct bpapi_vtree vtree;

		BPAPI_INIT_BCONF(&api, root, stdout, hash);

		api.filter_state = filter_state_bconf;

		vtree = *api.vchain.fun;
		orig_vget = vtree.get;
		vtree.get = testvtree_get;
		api.vchain.fun = &vtree;

		bpapi_insert(&api, "var1", "Variabel 1");
		bpapi_insert(&api, "var1johan", "Variabel 1");
		bpapi_insert(&api, "list1", "fnargel");
		bpapi_insert(&api, "list1", "pargel");
		bpapi_insert(&api, "list1", "nisse");
		bpapi_insert(&api, "list1", "pisse");
		bpapi_insert(&api, "list1", "popp");
		bpapi_insert(&api, "list3", "1");
		bpapi_insert(&api, "list3", "2");
		bpapi_insert(&api, "list3", "3");
		bpapi_insert(&api, "list3", "4");
		bpapi_insert(&api, "list3", "5");
		bpapi_insert(&api, "list2", "A");
		bpapi_insert(&api, "list2", "B");
		bpapi_insert(&api, "pos", "2");
		bpapi_insert(&api, "region", "2");
		bpapi_insert(&api, "cat", "4");
		bpapi_insert(&api, "template", "test/template1.txt");
		bpapi_insert(&api, "lists", "1");
		bpapi_insert(&api, "lists", "2");
		bpapi_insert(&api, "lists", "3");

		bpapi_insert(&api, "list4", "4");
		bpapi_insert(&api, "list4", "7");
		bpapi_insert(&api, "list4", "1");
		bpapi_insert(&api, "list4", "3");
		bpapi_insert(&api, "list4", "2");

		bpapi_insert(&api, "list5", "4");
		bpapi_insert(&api, "list5", "7.1");
		bpapi_insert(&api, "list5", "1.2");
		bpapi_insert(&api, "list5", "3.3");
		bpapi_insert(&api, "list5", "2.4");

		bpapi_insert(&api, "list6", "Pensador Mexicano");
		bpapi_insert(&api, "list6", "Peo");
		bpapi_insert(&api, "list6", "pema");
		bpapi_insert(&api, "list6", "Pe\xF1\xF3n de los Ba\xF1os");  /* Peñón de los Baños */

		bpapi_insert(&api, "dbdata", "' \\ '");
		bpapi_insert(&api, "category", "46");

		bconf_add_data(&root, "notcached", "0");
		bconf_add_data(&root, "cached", "0");

		bpapi_insert(&api, "b__lang", "en");

		call_template(&api, "test/template.txt");

		bpapi_insert(&api, "apartment_type", "tenant_ownership");
		bpapi_insert(&api, "type", "s");

		bconf_add_data(&root, "notcached", "1");
		bconf_add_data(&root, "cached", "1");

		call_template(&api, "test/template.txt");

		flush_template_cache(NULL);

		bpapi_output_free(&api);
		bpapi_simplevars_free(&api);
		bpapi_vtree_free(&api);
	}
	return 0;
}

struct suite {
	const char *name;
	int (*fun)(int, char **);
} tests[] = {
	{ "expect", test_legacy },
	{ "settings_filter", test_settings_filter },
	{ NULL, NULL }
};


int main(int argc, char **argv) {

	if (argc == 0) {
		printf("usage: %s <test-suite-name> [args...]\n", argv[0]);
		return 1;
	}

	struct suite *s = tests;
	while (s && s->name) {
		if (strcmp(argv[1], s->name) == 0) {
			return s->fun(argc-1, argv+1);
		}
		++s;
	}

	return 0;
}

