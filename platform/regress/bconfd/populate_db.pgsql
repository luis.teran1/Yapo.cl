INSERT INTO conf VALUES ('*.fromdb.jubilee.1', 'street');
INSERT INTO conf VALUES ('*.fromdb.jubilee.2', 'finishing');
-- These keys should not be added due to conflicts with the static storage
INSERT INTO conf VALUES ('*.*.prio.prius', 'should not exist');
INSERT INTO conf VALUES ('*.*.uncommon', 'should not exist');

