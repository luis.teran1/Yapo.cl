
#include <bpapi.h>
#include <ctemplates.h>
#include <patchvars.h>

/*
 * This is for a bloody test only, DO NOT USE.
 */
static int
init_loopmuch(const struct bpfilter *filter, struct bpapi *api, int argc, const struct tpvar **argv) {
	const char *name;
	char *fname = NULL;
	int i;
	int n;

	name = tpvar_str(argv[0], &fname);
	n = tpvar_int(argv[1]);

	for (i = 0; i < n; i++) {
		bpapi_insertf(api, name, "%d", i);
	}

	free(fname);
	return 0;
}

ADD_FILTER(loopmuch, NULL, 0, NULL, 0, NULL, 0);
