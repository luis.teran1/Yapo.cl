
import scm.platform.bsapi as bsapi
import sys

bs = bsapi.bsconf_struct()
if bsapi.bsconf_init_vtree(bs, { 'host' : { '1' : { 'name' : 'localhost', 'port' : int(sys.argv[1]) } } }, True) != 0:
	raise Exception("bsconf_init_vtree failed")

bo = bsapi.bsapi_object_new(bs, b'::1')

if bsapi.bsapi_object_search(bo, b'0 lim:10 ad_type:s,k') != 0:
	raise Exception("bsapi_object_search failed")

for key, values in bo.infos():	
	values = [v.decode("utf-8") for v in values]
	print(key.decode('utf-8') + " : " + ",".join(values))

while bsapi.bsapi_object_next_row(bo):
	for key, value in bo.columns():
		print(key.decode('utf-8') + " : " + value.decode('utf-8'))

