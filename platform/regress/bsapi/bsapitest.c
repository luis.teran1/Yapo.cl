#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <bsapi.h>
#include <bconf.h>
#include <transapi.h>
#include <bconfig.h>
#include <timer.h>
#include "logging.h"
#include "util.h"

static void
null_cb(int a, int b, const char *c, const char **d, void *e) {
}

struct bconf_node *root;

static void
timer_dump(struct timer_class *tc, void *v) {
	struct timespec avgts;
	double avg = (double)tc->tc_total.tv_sec + (double)tc->tc_total.tv_nsec / 1000000000.0;

	if (tc->tc_count == 0)
		return;

	avg /= (double)tc->tc_count;

	avgts.tv_sec = avg;
	avgts.tv_nsec = (avg - (double)avgts.tv_sec) * 1000000000.0;

	printf("Timer: %s.count:%lld\n", tc->tc_name, tc->tc_count);
	printf("Timer: %s.total:%d.%03ld\n", tc->tc_name, (int)tc->tc_total.tv_sec, tc->tc_total.tv_nsec / 1000000);
	if (tc->tc_count > 1) {
		printf("Timer: %s.min:%d.%03ld\n", tc->tc_name, (int)tc->tc_min.tv_sec, tc->tc_min.tv_nsec / 1000000);
		printf("Timer: %s.max:%d.%03ld\n", tc->tc_name, (int)tc->tc_max.tv_sec, tc->tc_max.tv_nsec / 1000000);
		printf("Timer: %s.average:%d.%03ld\n", tc->tc_name, (int)avgts.tv_sec, avgts.tv_nsec / 1000000);
	}
}



int
main(int argc, char **argv) {
	struct bpapi_vtree_chain vtree;
	struct bconf_node *node;
	struct bsearch_api ba;
	struct bsconf bs;
	int rounds = 100;
	int i;

	log_setup("bsapitest", "debug");

	if (argc < 2)
		xerrx(1, "Usage: %s <trans.conf> [rounds]", argv[0]);

	if (argc > 2)
		rounds = atoi(argv[2]);

        if ((root = config_init(argv[1])) == NULL)
                xerrx(1, "failed to read conf file <%s>", argv[1]);

	load_bconf("regress", &root);

	if ((node = bconf_get(root, "common.tsearch")) == NULL)
		xerrx(1, "no common.tsearch node");

	if (bsconf_init_vtree(&bs, bconf_vtree(&vtree, node), 1))
		xerrx(1, "bsconf_init_vtree");
	vtree_free(&vtree);

	for (i = 0; i < rounds; i++) {
		int total;

		if (bsearch_init_bsconf(&ba, &bs, NULL))
			xerrx(1, "bsearch_init_bsconf");
		if (bsearch_search(&ba, "0 type:s", null_cb, &total))
			xerrx(1, "bsearch_search");
		bsearch_cleanup(&ba);
	}

	timer_foreach(timer_dump, NULL);

        return 0;
}
