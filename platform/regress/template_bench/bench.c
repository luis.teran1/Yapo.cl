#include <stdio.h>
#include <stdlib.h>

#include "ctemplates.h"
#include "timer.h"
#include "bconf.h"

#define NVALUES 100000

int
main(int argc, char **argv) {
	struct bconf_node *n = NULL;
	struct bpapi api;
	struct timer_instance *ti;
	int repeat = 1000;
	int i;

	template_timers_ctl(1);

	for (i = 0; i < NVALUES; i++) {
		char key[128];
		snprintf(key, sizeof(key), "lots.keys.%d", i);
		ti = timer_start(NULL, "bconf_insertion");
		bconf_add_data(&n, key, "string");
		timer_end(ti, NULL);
	}

	for (i = 0; i < NVALUES; i++) {
		char key[128];
		snprintf(key, sizeof(key), "lots.keys_with_names.%d.name", i);
		bconf_add_data(&n, key, "string");
	}


	BPAPI_INIT_BCONF(&api, n, null, hash);
	/* Populate a variable with 100k values */
	for (i = 0; i < NVALUES; i++) {
		ti = timer_start(NULL, "simplevar_insertion");
		bpapi_insert(&api, "lots_values", "string");
		timer_end(ti, NULL);
	}

	for (i = 0; i < NVALUES; i++) {
		char key[128];
		snprintf(key, sizeof(key), "lots_keys%d", i);

		ti = timer_start(NULL, "simplevar_insertion_keys");
		bpapi_insert(&api, key, "string");
		timer_end(ti, NULL);		
	}

	for (i = 0; i < repeat; i++) {
		ti = timer_start(NULL, "overhead");
		call_template(&api, "bench/null");
		timer_end(ti, NULL);
	}

	for (i = 0; i < repeat; i++) {
		call_template(&api, "bench/simplevars_loop");
	}
	for (i = 0; i < repeat; i++) {
		call_template(&api, "bench/keyglob_loop");
	}
	for (i = 0; i < repeat; i++) {
		call_template(&api, "bench/filter_loop");
	}
	for (i = 0; i < repeat; i++) {
		call_template(&api, "bench/empty_loop");
	}
	for (i = 0; i < repeat; i++) {
//		call_template(&api, "bench/varblock_define");
	}
	for (i = 0; i < repeat; i++) {
		call_template(&api, "bench/vtree_loop_keys");
		flush_template_cache("bench/vtree_loop_keys");
	}
	for (i = 0; i < repeat; i++) {
		call_template(&api, "bench/vtree_loop_values");
		flush_template_cache("bench/vtree_loop_values");
	}
	for (i = 0; i < repeat; i++) {
		call_template(&api, "bench/vtree_loop_with_name");
		flush_template_cache("bench/vtree_loop_with_name");
	}
	for (i = 0; i < repeat; i++) {
		call_template(&api, "bench/vtree_loop_with_name_keys");
		flush_template_cache("bench/vtree_loop_with_name_keys");
	}

	bpapi_free(&api);

	BPAPI_INIT_BCONF(&api, n, stdout, hash);
	call_template(&api, "funs/dump_timers");

	bpapi_free(&api);

	return 0;
}
