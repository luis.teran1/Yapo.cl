#!/usr/bin/perl -w
#
# Modify a search query log by injecting params and editing keywords.
#
# TODO:
# 	Recognize and ignore ops (AND, OR, ...) in search string.
use strict;
use Getopt::Long;

my $chance = 0;
my $debug = 0;
my $minlen = 3;
my $maxedits = 1;
my $verbose = 0;
my @addparam;

my $tot = 0;
my $toted = 0;

sub help() {
print <<EOT;
usage: $0 [--options] < logfile

  --addparam	String(s) that will be injected as a parameter in the search query after lim:nn
  --chance	A percentage representing the probability of editing a keyword.
  --minlen	Minimum word length req. to be considered for editing.
  --maxedits	Maximum number of words to edit in one query.
  --verbose	Talk a bit more on stderr

example:
  cat 120kqueries | $0 --addparam "suggest:2" --chance 50
EOT
exit(0);
}

#
# Edit a word by picking a random character from it and duplicating it at a random position.
#
sub fuzz_word_dup($) {
	my $word = shift;

	my $idx = int(rand(length($word)));
	my $dupchar = substr($word, $idx, 1);
	my $inpos = int(rand(length($word)));

	substr($word, $inpos, 0) = $dupchar;
	return $word;

}

sub fuzz_keywords($) {
	my ($kws) = @_;

	my @words = split(/[ \t\n\r]+/, $kws);

	my $editsleft = $maxedits;
words:	for my $word (@words) {
		if (length($word) >= $minlen) {
			if ($chance > int(rand(100))) {
				$word = fuzz_word_dup($word);
				++$toted;
			}
			++$tot;
			last words if (--$editsleft < 1);
		}
	}
	return join(' ', @words);
}

GetOptions("chance=s" => \$chance,
	   "minlen=s" => \$minlen,
	   "maxedits=s" => \$maxedits,
	   "addparam=s" => \@addparam,
	   "verbose" => \$verbose,
	   "debug" => \$debug,
	   "help" => sub { help(); }
);

my $added_params = join(' ', @addparam);

while (my $line = <>) {
	#0 lim:0 count_level:munic:- count_level:munic_subarea:-  count(c_tab):company_ad:1 count_all(unfiltered)  region_type:11_s category_type:4042_s advertiser_type:p *:* scooter
	if ($line =~ m/(^[0-9].*) \*:\* (.*)/ ) {
		if (length($2) >= $minlen) {
			my $sq = $1;
			my $kw = fuzz_keywords($2);
			if ($added_params) {
				$sq =~ s/lim:([0-9]+) /lim:$1 $added_params /;
			}
			print $sq." *:* ".$kw."\n";
		}
	}
}

print STDERR "Edited $toted of $tot keywords\n" if $verbose;

exit 0;

