
CREATE TYPE enum_ads_status AS ENUM ('active', 'deleted');

CREATE TABLE ads (
	ad_id bigserial PRIMARY KEY,
	list_id bigint,
	status enum_ads_status NOT NULL,
	subject varchar NOT NULL,
	body text NOT NULL,
	replaced_by bigint REFERENCES ads (ad_id),
	price bigint,
	order_override bigint
);

CREATE INDEX index_ads_replaced_by ON ads (replaced_by);
CREATE UNIQUE INDEX index_ads_list_id ON ads (list_id) WHERE replaced_by IS NULL;
CREATE INDEX index_ads_status ON ads (status) WHERE replaced_by IS NULL;

INSERT INTO ads VALUES (default, 1, 'active', 'foo subject', 'foo body', NULL, 42);
INSERT INTO ads VALUES (default, 2, 'active', 'bar subject', 'bar body', NULL, 4711);
INSERT INTO ads VALUES (default, 3, 'active', 'test subject', 'Selling, 2.5 cars for 990:-', NULL, 42);
INSERT INTO ads VALUES (default, 4, 'active', '"quotedsubject"', 'Some 12" woofers with "42" meanings.', NULL, 42);

INSERT INTO conf VALUES ('fookey.1', 'barval');
INSERT INTO conf VALUES ('fookey.2', 'bazval');
