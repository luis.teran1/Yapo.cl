#include <stdio.h>
#include <err.h>

#include "index.h"
#include "text.h"
#include "generate-index.h"

extern int verbose;

void generate_index(const char *db_name, int ndocs, int nattrs) {
	struct index_db *db;
	char id[16];
	int d;

	if ((db = open_index(db_name, INDEX_WRITER)) == NULL)
		err(1, "open_index");

	for (d = 0; d < ndocs; d++) {
		struct document *doc = open_doc(db);
		int a;

		if (verbose && (d % 100000) == 0 && d)
			fprintf(stderr, "indexed docs: %d\n", d);

		for (a = 0; a < nattrs; a++) {
			char attr[16];

			snprintf(attr, sizeof(attr), "%d", a);
			doc_add_attr(doc, "ai", attr, AIND);
			snprintf(attr, sizeof(attr), "%d", d);
			doc_add_attr(doc, "id", attr, AATTR|AIND);
		}

		doc->id.id = d;
		doc->id.order = d;
		doc->id.suborder = 0;
		if (store_doc(db, doc, 0))
			err(1, "store_doc");
	}
	snprintf(id, sizeof(id), "%d", ndocs - 1);
	index_set_to_id(db, id);
	close_index(db, 0);
}
