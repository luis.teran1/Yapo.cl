#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <unistd.h>
#include <netdb.h>
#include <stdio.h>
#include <fcntl.h>
#include <math.h>
#include <err.h>

#include <bconfig.h>
#include <bsapi.h>
#include <bconf.h>
#include <timer.h>
#include <buf_string.h>
#include "generate-index.h"

/*
 * This program generates a pile of numbers for evaluating various aspects of search queries.
 *
 * The way to use those numbers with gnuplot (after putting the relevant parts of the output in the right files):
 * gnuplot> plot "attrs" using ($3):($6/($3)) with linespoints title "Union, time per attribute, 50 docs."
 * gnuplot> plot "docs" using ($5):($6/($5)) with linespoints title "Union, time per document, 20 attrs."
 * gnuplot> plot "docs-sz" using ($3):($7/($3)) with linespoints title "Index size per document, 20 attrs."
 * gnuplot> plot "attrs-sz" using ($5):($7/($5)) with linespoints title "Index size per attribute, 50 docs."
 *
 */

int verbose;

static int
search_command(struct bconf_node *root, const char *cmd) {
	static struct addrinfo *res;
	char buf[256];
	int fd;

	if (!res) {
		int error;
		struct addrinfo hints = {0};
		const char *server = bconf_get_string(root, "host.1.name");
		const char *port = bconf_get_string(root, "host.1.command_port");

		hints.ai_family = PF_UNSPEC;
		hints.ai_socktype = SOCK_STREAM;

		error = getaddrinfo(server, port, &hints, &res);
		if (error)
			errx(1, "gai: %s", gai_strerror(error));
	}
	if ((fd = socket(res->ai_family, res->ai_socktype, res->ai_protocol)) == -1)
		err(1, "socket");

	if (connect(fd, res->ai_addr, res->ai_addrlen) == -1)
		return -1;

	/* Wait for banner. */
	UNUSED_RESULT(read(fd, buf, sizeof(buf)));

	snprintf(buf, sizeof(buf), "cmd:%s\nend\n", cmd);
	if (write(fd, buf, strlen(buf)) != (ssize_t)strlen(buf))
		err(1, "search command %s: write", cmd);

	/* Wait for any response */
	UNUSED_RESULT(read(fd, buf, sizeof(buf)));
	close(fd);

	return 0;
}

static void
start_search(struct bconf_node *root) {
	const char *bin = bconf_get_string(root, "tsearch.bin");
	const char * const argv[] = {
		"search",
		"--config",
		bconf_get_string(root, "tsearch.conf"),
		"--quick-start",
		"--logfile=stderr",
		NULL
	};
	int i;

	if (fork() == 0) {
		int fd = open("/dev/null", O_RDWR);

		if (fd == -1)
			err(1, "open(/dev/null)");

		dup2(fd, 0);
		dup2(fd, 1);
		dup2(fd, 2);

		if (execv(bin, (char * const *)argv))
			err(1, "exec(search)");
	}
		
	for (i = 0; i < 100; i++) {
		if (search_command(bconf_get(root, "tsearch"), "ready") == 0)
			break;
		if (verbose) {
			if (i == 0)
				fprintf(stderr, "waiting for search to start: ");
			fprintf(stderr, ".");
		}
		usleep(500000);
	}
	if (verbose)
		fprintf(stderr, "\n");
}

static void
null_cb(int a, int b, const char *c, const char **d, void *e) {
}

static void
timer_dump(struct timer_class *tc, void *v) {
	double tot = (double)tc->tc_total.tv_sec + (double)tc->tc_total.tv_nsec / 1000000000.0;

	if (tc->tc_count == 0)
		return;

	tot /= (double)tc->tc_count;

	if (strncmp(tc->tc_name, "union", sizeof("union") - 1) == 0 ||
	    strncmp(tc->tc_name, "index", sizeof("index") - 1) == 0 ||
	    strncmp(tc->tc_name, "id", sizeof("id") - 1) == 0 ||
	    strncmp(tc->tc_name, "intersection", sizeof("intersection") - 1) == 0) {
		static int lattrs, ldocs;
		int attrs, docs;
		char scratch[128];
		sscanf(tc->tc_name, "%[^0-9]%d%[^0-9]%d", scratch, &attrs, scratch, &docs);
		if (lattrs != attrs && ldocs != docs && lattrs != 0 && ldocs != 0)
			printf("\n");
		lattrs = attrs;
		ldocs = docs;
		printf("%s %.8f (%d %d)\n", tc->tc_name, tot, attrs, docs);
	}
}

static void
timer_dump_gnuplot(struct timer_class *tc, void *v) {
	double tot = (double)tc->tc_total.tv_sec + (double)tc->tc_total.tv_nsec / 1000000000.0;
	FILE *f = v;

	if (tc->tc_count == 0)
		return;

	tot /= (double)tc->tc_count;

	if (strncmp(tc->tc_name, "union", sizeof("union") - 1) == 0 ||
	    strncmp(tc->tc_name, "index", sizeof("index") - 1) == 0 ||
	    strncmp(tc->tc_name, "id", sizeof("id") - 1) == 0 ||
	    strncmp(tc->tc_name, "intersection", sizeof("intersection") - 1) == 0) {
		static int lattrs, ldocs;
		int attrs, docs;
		char scratch[128];
		sscanf(tc->tc_name, "%[^0-9]%d%[^0-9]%d", scratch, &attrs, scratch, &docs);
		if (lattrs != attrs && ldocs != docs && lattrs != 0 && ldocs != 0)
			fprintf(f, "\n");
		lattrs = attrs;
		ldocs = docs;
		fprintf(f, "%d %d %f\n", attrs, docs, tot);
	}
}

static void
run_index(struct bconf_node *root, struct bsconf *bs, int nattrs, int ndocs) {
	struct timer_instance *ti;
	char timer_name[32];

	snprintf(timer_name, sizeof(timer_name), "index attr %d doc %d", ndocs, nattrs);
	ti = timer_start(NULL, timer_name);
	generate_index(bconf_get_string(root, "tsearch.db_name_new"), ndocs, nattrs);
	timer_end(ti, NULL);

	/* Laziest possible way to rename the new index. */
	search_command(bconf_get(root, "tsearch"), "reload");	
}

static void
run_size(struct bconf_node *root, struct bsconf *bs, int nattrs, int ndocs) {
	char blob_path[1024];
	struct stat sb;

	generate_index(bconf_get_string(root, "tsearch.db_name_new"), ndocs, nattrs);

	snprintf(blob_path, sizeof(blob_path), "%s/db.blob", bconf_get_string(root, "tsearch.db_name_new"));

	if (stat(blob_path, &sb) == -1)
		err(1, "stat(%s)", blob_path);

	printf("Size: docs: %d attrs: %d sz: %lld\n", ndocs, nattrs, (long long)sb.st_size);

	/* Laziest possible way to rename the new index. */
	search_command(bconf_get(root, "tsearch"), "reload");
}

static void
run_union(struct bconf_node *root, struct bsconf *bs, int nattrs, int ndocs) {
	char timer_name[32];
	int rounds = 10;
	int i;

	snprintf(timer_name, sizeof(timer_name), "union attr %d doc %d", nattrs, ndocs);

	generate_index(bconf_get_string(root, "tsearch.db_name_new"), ndocs, nattrs);

	search_command(bconf_get(root, "tsearch"), "reload");

	for (i = 0; i < rounds; i++) {
		struct timer_instance *ti;
		struct bsearch_api ba;
		int total;

		if (bsearch_init_bsconf(&ba, bs, NULL))
			errx(1, "bsearch_init_bsconf");
		ti = timer_start(NULL, timer_name);
		if (bsearch_search(&ba, "0 lim:0 ai:-", null_cb, &total))
			errx(1, "bsearch_search %s", bsearch_errstr(ba.berrno));
		timer_end(ti, NULL);
		search_command(bconf_get(root, "tsearch"), "flush_cache");
	}

}

static void
run_intersection(struct bconf_node *root, struct bsconf *bs, int maxattrs, int nattrs, int ndocs) {
	struct buf_string q = { 0 };
	char timer_name[32];
	int rounds = 10;
	int i;

	bscat(&q, "0 lim:0 ");
	for (i = 0; i < nattrs; i++) {
		bscat(&q, "ai:%d ", i % nattrs);
	}

	snprintf(timer_name, sizeof(timer_name), "intersection attr %d doc %d", nattrs, ndocs);

	if (verbose)
		fprintf(stderr, "%s -> I", timer_name);

	generate_index(bconf_get_string(root, "tsearch.db_name_new"), ndocs, nattrs);

	if (verbose)
		fprintf(stderr, " R");

	search_command(bconf_get(root, "tsearch"), "reload");

	if (verbose)
		fprintf(stderr, " S\n");

	for (i = 0; i < rounds; i++) {
		struct timer_instance *ti;
		struct bsearch_api ba;
		int total;

		if (bsearch_init_bsconf(&ba, bs, NULL))
			errx(1, "bsearch_init_bsconf");
		ti = timer_start(NULL, timer_name);
		if (bsearch_search(&ba, q.buf, null_cb, &total))
			errx(1, "bsearch_search %s", bsearch_errstr(ba.berrno));
		timer_end(ti, NULL);
		search_command(bconf_get(root, "tsearch"), "flush_cache");
	}
	free(q.buf);
}

static void
run_id(struct bconf_node *root, struct bsconf *bs, int nattrs, int ndocs) {
	char timer_name[32];
	int rounds = 1000;
	char q[16];
	int i;

	snprintf(timer_name, sizeof(timer_name), "id attr %d doc %d", nattrs, ndocs);

	generate_index(bconf_get_string(root, "tsearch.db_name_new"), ndocs, nattrs);

	search_command(bconf_get(root, "tsearch"), "reload");

	snprintf(q, sizeof(q), "id:%d\n", ndocs / 7);

	for (i = 0; i < rounds; i++) {
		struct timer_instance *ti;
		struct bsearch_api ba;
		int total;

		if (bsearch_init_bsconf(&ba, bs, NULL))
			errx(1, "bsearch_init_bsconf");
		ti = timer_start(NULL, timer_name);
		if (bsearch_search(&ba, q, null_cb, &total))
			errx(1, "bsearch_search %s", bsearch_errstr(ba.berrno));
		timer_end(ti, NULL);
		search_command(bconf_get(root, "tsearch"), "flush_cache");
	}

}

/*
 * Predefined tests.
 */
static void
t_index_attr(struct bconf_node *root, struct bsconf *bs) {
	int i;

	for (i = 15; i <= 10000; i += sqrt(2.0 * i)) {
		run_index(root, bs, i, 50);
	}
}

static void
t_index_doc(struct bconf_node *root, struct bsconf *bs) {
	int i;

	for (i = 15; i <= 10000; i += sqrt(2.0 * i)) {
		run_index(root, bs, 20, i);
	}
}

static void
t_size_attr(struct bconf_node *root, struct bsconf *bs) {
	int i;

	for (i = 15; i <= 10000; i += sqrt(2.0 * i)) {
		run_size(root, bs, i, 50);
	}
}

static void
t_size_doc(struct bconf_node *root, struct bsconf *bs) {
	int i;

	for (i = 15; i <= 10000; i += sqrt(2.0 * i)) {
		run_size(root, bs, 20, i);
	}
}

static void
t_union_attr(struct bconf_node *root, struct bsconf *bs) {
	int i;

	for (i = 15; i <= 10000; i += sqrt(2.0 * i)) {
		run_union(root, bs, i, 50);
	}
}

static void
t_union_doc(struct bconf_node *root, struct bsconf *bs) {
	int i;

	for (i = 15; i <= 10000; i += sqrt(2.0 * i)) {
		run_union(root, bs, 20, i);
	}
}

static void
t_inter_attr(struct bconf_node *root, struct bsconf *bs) {
	int i;

	for (i = 15; i <= 1000; i += sqrt(i)) {
		run_intersection(root, bs, 1000, i, 50);
	}
}

static void
t_inter_doc(struct bconf_node *root, struct bsconf *bs) {
	int i;

	for (i = 15; i <= 1000; i += sqrt(i)) {
		run_intersection(root, bs, 1000, 20, i);
	}
}

static void
t_id_doc(struct bconf_node *root, struct bsconf *bs) {
	int i;

	for (i = 15; i <= 10000000; i += 100000) {
		run_id(root, bs, 1, i);
	}
}

static void
t_inter_attr_doc(struct bconf_node *root, struct bsconf *bs) {
	int i, j;

	for (j = 2000; j > 0; j -= 100) {
		for (i = 2000; i > 0; i -= 100) {
			run_intersection(root, bs, 2000, j, i);
		}
	}
}

struct pdt {
	const char *name;
	void (*fn)(struct bconf_node *, struct bsconf *);
	const char *xlabel;
	const char *ylabel;
	const char *zlabel;
	const char *columnformat;
	const char *title;
} predef_test[] = {
	{ "index_attr", t_index_attr, "attributes", "seconds", NULL, "1:3", "Index time, variable number of attributes, 50 docs." },
	{ "index_doc", t_index_doc, "documents", "seconds", NULL, "2:3", "Index time, 20 attributes, variable number of docs." },
	{ "size_attr", t_size_attr, "attributes", "bytes", NULL, "7:3", "Index size, variable number of attributes, 50 docs." },
	{ "size_doc", t_size_doc, "documents", "bytes", NULL, "7:5", "Index time, 20 attributes, variable number of docs." },
	{ "union_attr", t_union_attr, "attributes", "seconds", NULL, "1:3", "Union, variable number of attributes, 50 docs." },
	{ "union_doc", t_union_doc, "documents", "seconds", NULL, "2:3", "Union, 20 attributes, variable number of docs." },
	{ "inter_attr", t_inter_attr, "attributes", "seconds", NULL, "1:3", "Intersection, variable number of attributes, 50 docs." },
	{ "inter_doc", t_inter_doc, "documents", "seconds", NULL, "2:3", "Intersection, 20 attributes, variable number of docs." },
	{ "inter_attr_doc", t_inter_attr_doc, "documents", "attributes", "seconds", "1:2:3", "Intersection, docs and attrs variable." },
	{ "id_doc", t_id_doc, "documents", "seconds", NULL, "2:3", "Doc fetch, variable number of docs." }
};

int npredef = sizeof(predef_test) / sizeof(predef_test[0]);

static void
usage(const char *progname) {
	int i;

	fprintf(stderr, "Usage: %s [-v] [-g <gnuplot.png>] -t <test> <bconf>\n", progname);
	fprintf(stderr, "Available tests:");
	for (i = 0; i < npredef; i++) {
		fprintf(stderr, "%s%s", i == 0 ? " " : ", ", predef_test[i].name);
	}
	fprintf(stderr, ".\n");
	exit(1);
}

int
main(int argc, char **argv) {
	const char *progname = argv[0];
	struct bpapi_vtree_chain vtree;
	struct bconf_node *root = NULL;
	struct bconf_node *node;
	const char *plot = NULL;
	struct bsconf bs;
	int testind = -1;
	int opt;

	while ((opt = getopt(argc, argv, "t:g:v")) != -1) {
		switch (opt) {
		case 't':
			if (testind != -1)
				usage(progname);
			for (testind = 0; testind < npredef; testind++) {
				if (strcmp(optarg, predef_test[testind].name) == 0)
					break;
			}
			if (testind == npredef)
				usage(progname);
			break;
		case 'g':
		       	plot = strdup(optarg);
			break;
		case 'v':
			verbose = 1;
			break;
		default:
			usage(progname);
		}
	}

	argc -= optind;
	argv += optind;
	if (argc != 1) {
		usage(progname);
	}

	if (load_bconf_file(NULL, &root, argv[0]))
		errx(1, "Failed to read bconf: %s", argv[0]);

	generate_index(bconf_get_string(root, "tsearch.db_name_new"), 1, 1);

	start_search(root);

	if ((node = bconf_get(root, "tsearch")) == NULL)
		errx(1, "No tsearch node.");

	if (bsconf_init_vtree(&bs, bconf_vtree(&vtree, node), 1))
		errx(1, "bsconf_init_vtree");
	vtree_free(&vtree);

	if (plot) {
		struct pdt *p = &predef_test[testind];
		FILE *f = popen("gnuplot", "w");

		/* To make the fonts work, set GDFONTPATH */
		fprintf(f, "set term png font \"DejaVuSans,11\"\n");
		fprintf(f, "set output \"%s\"\n", plot);
		fprintf(f, "set yrange [0:]\n");
		fprintf(f, "set xlabel \"%s\"\n", p->xlabel);
		fprintf(f, "set ylabel \"%s\"\n", p->ylabel);
		if (p->zlabel) {
			fprintf(f, "set view 70, 10, 1, 1\n");
			fprintf(f, "set surface\n");
			fprintf(f, "set zlabel \"%s\"\n", p->zlabel);
			fprintf(f, "set contours\n");
			fprintf(f, "splot '-' using %s with lines palette title \"%s\"\n", p->columnformat, p->title);
		} else {
			fprintf(f, "plot '-' using %s with linespoints title \"%s\"\n", p->columnformat, p->title);
		}
		p->fn(root, &bs);
		timer_foreach(timer_dump_gnuplot, f);
		fprintf(f, "e\n");

		pclose(f);
	} else {
		predef_test[testind].fn(root, &bs);
		timer_foreach(timer_dump, NULL);
	}

	search_command(bconf_get(root, "tsearch"), "stop");

	return 0;
}
