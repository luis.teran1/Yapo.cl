
#include "mod_templates.h"

#include "bconf.h"
#include "query_string.h"

#include <stdlib.h>
#include <httpd.h>

int setup_templates_local(apr_pool_t *pool, struct templates_conf *conf, struct bconf_node *root) {
	if (getenv("MOD_TEMPLATES_REGRESS_REQ_UTF8")) {
		/* XXX this is very abusive. Don't do this. */
		struct bconf_node *root = conf->root;
		struct bconf_node *newroot = NULL;
		enum req_utf8 req_utf8 = atoi(getenv("MOD_TEMPLATES_REGRESS_REQ_UTF8"));

		switch(req_utf8) {
		case RUTF8_NOCHECK:
			break;
		case RUTF8_REQUIRE:
			bconf_merge(&newroot, root);
			bconf_add_data(&newroot, "common.charset", "UTF-8");
			break;
		case RUTF8_FALLBACK_LATIN1:
			bconf_merge(&newroot, root);
			bconf_add_data(&newroot, "common.charset", "UTF-8");
			bconf_add_data(&newroot, "common.charset_fallback", "latin1");
			break;
		}
		if (newroot) {
			conf->root = newroot;
			conf->vbconf.vtree.data = newroot;
		}
	}
	return OK;
}

void
templates_start_call_local(struct template_call *call, struct bconf_node *tmpl_root, const char **lang) {
	if (bconf_get_int(tmpl_root, "null_domain") && bpapi_has_key(&call->ba, "nd"))
		call->cookie_domain = NULL;
}

void
templates_end_call_local(struct template_call *call, struct bconf_node *tmpl_root) {
}

static void
filter_state_foo(struct bpapi *api, const char *key, struct filter_state_data *fsd, void *v) {
	char *foo = malloc(2);
	foo[0] = 'a';
	foo[1] = 0;
	fsd->value = foo;
	fsd->freefn = free;
	fsd->type = FS_GLOBAL;
}

static const struct tpvar *
foo(struct bpapi *api, struct tpvar *dst, enum bpcacheable *cc, int argc, const struct tpvar **argv) {
	char *foo = filter_state(api, "foo", filter_state_foo, NULL);
	(*foo)++;
	return TPV_SET(dst, TPV_DYNSTR(strdup(foo)));
}

ADD_TEMPLATE_FUNCTION(foo);
