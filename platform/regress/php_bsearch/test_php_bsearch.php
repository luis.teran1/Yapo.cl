<?php

function handle_line(&$data, $key, $value) {

	if (in_array($key, array("unfiltered", "filtered")))
		return;

	if ($key == "body") {
		$data['body'][] = $data['_prefix'].$value;
	}
}


$bconf_node = $BCONF['*']['common']['asearch'];

$res = bsearch_search_vtree($bconf_node, "0 *:* fox or men");

foreach ($res['body'] as $line) {
	print $line."\n";
}

$data = array("_prefix" => "from_cb_");
bsearch_search_vtree($bconf_node, "0 *:* fox or men", handle_line, $data);

foreach ($data['body'] as $line) {
	print $line."\n";
}

?>
