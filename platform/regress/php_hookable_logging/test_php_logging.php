<?php

srand(time());
$num = rand();

openlog("php_hookable_logging", LOG_ODELAY, LOG_LOCAL0);

syslog(LOG_DEBUG, "foo $num");

closelog();

$syslogroot = getenv('SYSLOGROOT');
$logfile = $syslogroot . "/php_hookable_logging.log";

$lines = file($logfile);
if (!$lines) {
	echo "File not found\n";
	exit(1);
}

if (strpos(end($lines), "foo $num") === false) {
	echo "Line not matched\n";
	exit(1);
}

echo "All ok!\n";

?>
