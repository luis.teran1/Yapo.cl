#include <err.h>
#include <string.h>
#include <assert.h>

#include "buf_string.h"
#include "vtree.h"
#include "bconf.h"
#include "json_vtree.h"
#include "index.h"
#include "index_merge.h"
#include "timer.h"

static int
index_attrs_compare(struct index_db *a, struct index_db *b) {
	unsigned int an, bn;
	struct index_attrs_iter *ai = index_attrs_iter_open(a, 0, &an);
	struct index_attrs_iter *bi = index_attrs_iter_open(b, 0, &bn);

	if (an != bn) {
		printf("different number of attrs: %d != %d\n", an, bn);
		return -1;
	}
	for (;;) {
		const char *ak, *bk;
		int aks, bks, ads, bds;
		const int32_t *ad, *bd;


		index_attrs_iter_get(ai, &ak, &aks, &ad, &ads);
		index_attrs_iter_get(bi, &bk, &bks, &bd, &bds);

		if (aks != bks || memcmp(ak, bk, aks) != 0) {
			printf("attr mismatch: %d, %d [%.*s, %.*s]\n", aks, bks, aks, ak, bks, bk);
			return -1;
		}
		if (ads != bds || memcmp(ad, bd, ads)) {
			printf("attr docs mismatch (%.*s) %d, %d\n",  aks, ak, ads, bds);
			return -1;
		}
		int ani = !!index_attrs_iter_next(ai);
		int bni = !!index_attrs_iter_next(bi);
		if (ani != bni) {
			printf("attr iterator end mismatch\n");
			return -1;
		}
		if (!ani)
			break;
	}
	return 0;
}

static int
index_words_compare(struct index_db *a, struct index_db *b) {
	unsigned int an, bn;
	struct index_words_iter *ai = index_words_iter_open(a, 0, &an, NULL);
	struct index_words_iter *bi = index_words_iter_open(b, 0, &bn, NULL);

	if (an != bn) {
		printf("different number of words: %d != %d\n", an, bn);
		return -1;
	}
	for (;;) {
		const char *ak, *bk;
		int aks, bks, ads, bds, aps, bps;
		const struct docindex *ad, *bd;
		const struct docpos *ap, *bp;

		index_words_iter_get(ai, &ak, &aks, &ad, &ads, &ap, &aps);
		index_words_iter_get(bi, &bk, &bks, &bd, &bds, &bp, &bps);

		if (aks != bks || strncmp(ak, bk, aks) != 0) {
			printf("word mismatch: %d, %d [%.*s, %.*s]\n", aks, bks, aks, ak, bks, bk);
			return -1;
		}
		if (ads != bds || memcmp(ad, bd, ads) != 0) {
			printf("word docs mismatch (%.*s) %d, %d\n",  aks, ak, ads, bds);
			return -1;
		}
		int ani = !!index_words_iter_next(ai);
		int bni = !!index_words_iter_next(bi);
		if (ani != bni) {
			printf("word iterator end mismatch\n");
			return -1;
		}
		if (!ani)
			break;
	}
	return 0;
}

static int
index_docs_compare(struct index_db *a, struct index_db *b) {
	unsigned int an, bn;
	struct index_docs_iter *ai = index_docs_iter_open(a, 0, &an);
	struct index_docs_iter *bi = index_docs_iter_open(b, 0, &bn);

	if (an != bn) {
		printf("different number of docs: %d != %d\n", an, bn);
		return -1;
	}

	for (;;) {
		const struct doc *ak, *bk;
		int aks, bks, avs, bvs;
		const char *av, *bv;
		int32_t ao, bo;

		index_docs_iter_get(ai, &ak, &aks, &av, &avs, &ao);
		index_docs_iter_get(bi, &bk, &bks, &bv, &bvs, &bo);

		
		if (aks != bks || memcmp(ak, bk, sizeof(*ak)) != 0) {
			printf("docid mismatch: %d, %d\n", ak->id, bk->id);
			return -1;
		}
		if (avs != bvs || memcmp(av, bv, avs) != 0) {
			printf("doc content mismatch %d, %d\n", avs, bvs);
			return -1;
		}
		int ani = !!index_docs_iter_next(ai);
		int bni = !!index_docs_iter_next(bi);
		if (ani != bni) {
			printf("docs iterator end mismatch\n");
			return -1;
		}
		if (!ani)
			break;
	}
	return 0;
}

static int
p(void *v, int n, int d, const char *fmt, ...) {
	struct buf_string *bs = v;
	va_list ap;
	int res;

	va_start(ap, fmt);
	res = vbscat(bs, fmt, ap);
	va_end(ap);
	return res;
}

static int
index_meta_compare(struct index_db *a, struct index_db *b) {
	struct buf_string as = { 0 };
	struct buf_string bs = { 0 };
	struct bpapi_vtree_chain vt;
	int ret = 0;

	bconf_vtree(&vt, a->meta);
	vtree_json(&vt, 0, 0, p, &as);
	vtree_free(&vt);
	bconf_vtree(&vt, b->meta);
	vtree_json(&vt, 0, 0, p, &bs);
	vtree_free(&vt);

	if (as.pos != bs.pos || memcmp(as.buf, bs.buf, as.pos) != 0) {
		printf("meta mismatch { %.*s } != { %.*s }\n", as.pos, as.buf, bs.pos, bs.buf);
		ret = -1;
	}
	free(as.buf);
	free(bs.buf);
	return ret;
}

static int
index_compare(struct index_db *a, struct index_db *b) {
	return index_meta_compare(a, b) ?: index_attrs_compare(a, b) ?: index_words_compare(a, b) ?: index_docs_compare(a, b);
}

/*
 * This test opens the provided indexes, does a merge (with just one
 * source, so basically a copy) on each and then checks if the
 * contents match.
 *
 * The merges are done twice. First merge might rearrange some things
 * in the file, so it does a full check of the contents, second merge
 * should keep all previous sorting so it only compares the checksums.
 */
static int
clean_merges(int argc, char **argv) {
	int i;

	for (i = 0; i < argc; i++) {
		struct index_merge_state im = { 0 };
		struct timer_instance *ti;

		printf("testing %s\n", argv[i]);

		im.nfrom = 1;
		if ((im.from_db[0] = open_index(argv[i], INDEX_READER|INDEX_FILE)) == NULL)
			err(1, "open_index(%s)", argv[i]);
		if ((im.to = open_mem_index_writer()) == NULL)
			err(1, "open_mem_index_writer");

		ti = timer_start(NULL, "x");
		if (index_perform_merge(&im, ti) == -1)
			errx(1, "index_perform_merge failed");
		timer_end(ti, NULL);

		if (index_writer_downgrade_to_reader(im.to))
			errx(1, "index_writer_downgrade_to_reader failed");

		if (index_compare(im.from_db[0], im.to))
			errx(1, "indexes don't compare equal");

		struct index_db *nind = im.to;
		close_index(im.from_db[0], 0);

		/*
		 * Do a second merge from the newly merged index. Now
		 * they should generate the same checksum because
		 * documents ordering should be stable and its order in
		 * the file is well defined after a merge. Which we can
		 * check with a simple checksum compare.
		 */

		memset(&im, 0, sizeof(im));
		im.nfrom = 1;
		im.from_db[0] = nind;
		if ((im.to = open_mem_index_writer()) == NULL)
			err(1, "open_mem_index_writer");

		ti = timer_start(NULL, "x");
		if (index_perform_merge(&im, ti) == -1)
			errx(1, "index_perform_merge failed");
		timer_end(ti, NULL);

		if (index_writer_downgrade_to_reader(im.to))
			errx(1, "index_writer_downgrade_to_reader failed");

		const char *fc = index_get_checksum_string(im.from_db[0]);
		const char *tc = index_get_checksum_string(im.to);

		if (!tc || !fc || strcmp(tc, fc))
			errx(1, "checksum for (%s) mismatch %s != %s", argv[i], tc, fc);

		printf("OK %s == %s\n", fc, tc);

		close_index(im.from_db[0], 0);
		close_index(im.to, 0);
	}

	return 0;
}

static int
get_one_docid(struct index_db *db) {
	struct index_docs_iter *di;
	const struct doc *docid;
	const char *value;
	int32_t docoff;
	unsigned int ndocs;
	int ksz;
	int vsz;
	int ret;

	if ((di = index_docs_iter_open(db, 1, &ndocs)) == NULL)
		errx(1, "index_docs_iter_open");

	assert(ndocs);

	index_docs_iter_get(di, &docid, &ksz, &value, &vsz, &docoff);

	ret = docid->id;

	index_docs_iter_close(di);

	return ret;
}

/*
 * This test creates a delta that only contains a single deleted
 * document. We then try to merge that delta.
 */
static int
onedel_delta(int argc, char **argv) {
	struct index_merge_state im = { 0 };

	im.nfrom = 2;

	if ((im.from_db[0] = open_index(argv[0], INDEX_READER|INDEX_FILE)) == NULL)
		err(1, "open_index(%s)", argv[0]);

	if ((im.from_db[1] = open_mem_index_writer()) == NULL)
		errx(1, "open_mem_index_writer");

	if (index_prime_update(im.from_db[1], im.from_db[0]))
		errx(1, "index_prime_update");
	delete_doc(im.from_db[1], get_one_docid(im.from_db[0]));
	if (index_writer_downgrade_to_reader(im.from_db[1]))
		errx(1, "index_writer_downgrade_to_reader");

	if ((im.to = open_mem_index_writer()) == NULL)
		err(1, "open_mem_index_writer");
	struct timer_instance *ti = timer_start(NULL, "x");
	if (index_perform_merge(&im, ti) == -1)
		errx(1, "index_perform_merge failed");
	timer_end(ti, NULL);

	close_index(im.from_db[0], 0);
	close_index(im.from_db[1], 0);
	close_index(im.to, 0);

	return 0;
}

int
main(int argc, char **argv) {
	if (argc < 2) {
usage:
		fprintf(stderr, "%s: <clean_merge|onedel_delta> <index>+\n", argv[0]);
		exit(1);
	}
	if (!strcmp(argv[1], "clean_merge")) {
		return clean_merges(argc - 2, argv + 2);
	}
	if (!strcmp(argv[1], "onedel_delta")) {
		return onedel_delta(argc - 2, argv + 2);
	}
	goto usage;
}
