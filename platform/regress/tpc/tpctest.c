#include <stdlib.h>
#include <stdio.h>

#include <ctemplates.h>
#include <bconf.h>

int out_count;

static int
count_outstring_fmt(struct bpapi_output_chain *ochain, const char *fmt, ...) {
	out_count++;

	return 1;
}
static int
count_outstring_raw(struct bpapi_output_chain *ochain, const char *str, size_t len) {
	out_count++;

	return len;
}
static const struct bpapi_output count_api_output = {
	count_outstring_fmt,
	count_outstring_raw,
	NULL
};
static void
count_output_init(struct bpapi_output_chain *ochain) {
	ochain->fun = &count_api_output;
	ochain->data = NULL;
}

static int
out_merge(void) {
	struct bpapi api;
	struct bconf_node *root = NULL;

	BPAPI_INIT_BCONF(&api, root, count, hash);

	call_template(&api, "out_merge");

	bpapi_free(&api);

	if (out_count != 1)
		fprintf(stderr, "more than one outstring call\n");

	return out_count != 1;
}

int
main(int argc, char **argv) {
	return out_merge();
}
