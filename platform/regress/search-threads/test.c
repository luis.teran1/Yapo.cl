
#include <errno.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

#include <poll.h>
#include <sys/socket.h>
#include <unistd.h>

#include "fd_pool.h"
#include "util.h"

static int
start_query(struct fd_pool_conn *conn, const char *query) {
	int fd = fd_pool_get(conn, SBCS_START, NULL, NULL);
	int n, l;

	if (fd < 0)
		xerr(1, "Failed to connect to search");

	l = strlen(query);
	n = write(fd, query, l);
	if (n < l)
		xerr(1, "Failed to write query");
	shutdown(fd, SHUT_WR);

	return fd;
}

int
main(int argc, char *argv[]) {
	struct fd_pool *pool = fd_pool_create_single("localhost", argv[1], 1, 10000, NULL);
	struct fd_pool_conn *conn = fd_pool_new_conn(pool, NULL, 0);
	struct pollfd pollfd[4];
	int i, r;
	bool checked[4];
	bool reserve = false, goaway = false;

	pollfd[0].fd = start_query(conn, "C regress_delay");
	pollfd[1].fd = start_query(conn, "C regress_delay");
	usleep(150000);
	pollfd[2].fd = start_query(conn, "DOG");
	pollfd[3].fd = start_query(conn, "DOG");

	for (i = 0 ; i < 4 ; i++) {
		pollfd[i].events = POLLIN;
		pollfd[i].revents = 0;
		checked[i] = false;
	}

	/* Process contention means either DOG query could be the reserve
	 * or the goaway.
	 *
	 * There's a slight race condition risk I'm ignoring:
	 * If one of the DOG queries gets parsed before any of the regress_delay ones,
	 * test won't work. The nanosleep should help prevent that though.
	 */
	while (!reserve || !goaway) {
		r = poll(pollfd, 4, 10000);
		if (r == -1) {
			if (errno == EINTR)
				continue;
			xerr(1, "poll");
		}
		for (i = 2 ; i < 4 ; i++) {
			if ((pollfd[i].revents & POLLIN) && !checked[i]) {
				char buf[256];
				int r = read(pollfd[i].fd, buf, sizeof(buf) - 1);

				if (r < 0)
					xerr(1, "Failed to read search result");

				buf[r] = '\0';
				if (strncmp(buf, "info:filtered:", strlen("info:filtered:")) == 0) {
					if (reserve)
						xerrx(1, "Got double reserve reply");
					reserve = true;
				} else if (strncmp(buf, "info:goaway", strlen("info:goaway")) == 0) {
					if (goaway)
						xerrx(1, "Got double goaway");
					goaway = true;
				}
				checked[i] = true;
			}
		}
	}
	if (pollfd[0].revents || pollfd[1].revents)
		xerrx(1, "Unexpectedly got data on regress_delay query");

	return 0;
}

