<?php

$request = array();
$request["cmd"] = "bconf";
$request["type"] = "conf";
$request["stupid_float"] = 10.3;

$header["commit"] = 1;

$did_print_conf = false;

class test_trans {

	function print_conf($key, $val) {
		global $did_print_conf;

		var_dump($key, $val);
		$did_print_conf = true;
	}
}

$node = array("host" => array("1" => array("name" => "localhost", "port" => getenv('PHP_TEST_PORT'))));

$response = trans_commit($node, $header, $request, array('conf' => array(new test_trans(), 'print_conf')));

if (!is_array($response['conf'])) {
	echo "Didn't get array\n";
	print_r($response);
	exit(1);
}

if (!$did_print_conf) {
	echo "Didn't call callback\n";
	exit(1);
}

?>
