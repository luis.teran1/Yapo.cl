
CREATE TYPE enum_ads_status AS ENUM ('active', 'deleted');

CREATE TABLE ads (
	ad_id bigserial PRIMARY KEY,
	list_id bigint,
	status enum_ads_status NOT NULL default 'active',
	category integer NOT NULL default 0,
	lang varchar NOT NULL default 'en',
	subject varchar NOT NULL,
	body text NOT NULL,
	replaced_by bigint REFERENCES ads (ad_id)
);

CREATE INDEX index_ads_replaced_by ON ads (replaced_by);
CREATE UNIQUE INDEX index_ads_list_id ON ads (list_id) WHERE replaced_by IS NULL;
CREATE INDEX index_ads_status ON ads (status) WHERE replaced_by IS NULL;

