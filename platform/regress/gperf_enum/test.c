
#include <stdbool.h>
#include <stdio.h>
#include <string.h>

/* gperf generates these headers. */
#include "foo.h"
#include "bar.h"

int
main(int argc, char *argv[])
{
	if (argc < 2)
		return 1;

	/* basic */
	GPERF_ENUM(foo)
	switch(lookup_foo(argv[1], strlen(argv[1]))) {
	case GPERF_CASE("foo"):
		printf("foo\n");
		break;
	case GPERF_CASE("bar"):
		printf("bar\n");
		break;
	case GPERF_CASE_NONE:
		break;
	}

	/* advanced */
	GPERF_ENUM(bar; bool b)
	(void)GPERF_CASE("foo", false);
	(void)GPERF_CASE_VALUE(1, "bar", true);
	const struct bar_rec *b = bar_lookup(argv[1], strlen(argv[1]));
	if (b)
		printf("%s %d\n", b->b ? "true" : "false", b->val);
}
