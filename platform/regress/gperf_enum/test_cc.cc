
#include <stdio.h>
#include <string.h>

#include "foo_cc.h"
#include "bar_cc.h"

int
main(int argc, char *argv[])
{
	if (argc < 2)
		return 1;

	/* basic */
	GPERF_ENUM(foo_cc)
	switch(lookup_foo_cc(argv[1], strlen(argv[1]))) {
	case GPERF_CASE("foo"):
		printf("foo\n");
		break;
	case GPERF_CASE("bar"):
		printf("bar\n");
		break;
	case GPERF_CASE_NONE:
		break;
	}

	/* advanced */
	GPERF_ENUM(bar_cc; bool b)
	(void)GPERF_CASE("foo", false);
	(void)GPERF_CASE_VALUE(1, "bar", true);
	const struct bar_cc_rec *b = bar_cc_lookup(argv[1], strlen(argv[1]));
	if (b)
		printf("%s %d\n", b->b ? "true" : "false", b->val);
}
