#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "trans.h"
#include "util.h"
#include "command.h"

static struct c_param parameters_xor[] = {
	{"xor1;xor2",		P_XOR | P_OPTIONAL,	"v_bool;v_integer"},
	{"rxor1;rxor2",		P_XOR,			"v_bool;v_integer"},
	{NULL}
};

static struct c_param parameters_or[] = {
	{"or1;or2;or3",		P_OR,		"v_bool;v_integer;v_bool"},
	{NULL}
};

static struct c_param parameters_and[] = {
	{"and1;and2;and3",	P_AND,		"v_bool;v_integer;v_bool"},
	{NULL}
};

static struct c_param parameters_nand[] = {
	{"nand1;nand2;nand3",	P_NAND,		"v_bool;v_integer;v_bool"},
	{NULL}
};

static struct c_param parameters_eq[] = {
	{"eq1;eq2;eq3",		P_EQ,		"v_bool;v_integer;v_bool"},
	{NULL}
};

static struct c_param parameters_imply[] = {
	{"imply1;imply2;imply3", P_IMPLY,       "v_bool;v_integer;v_bool"},
	{NULL}
};

static struct c_param parameters_empty[] = {
	{"empty1",		P_REQUIRED | P_EMPTY,		"v_string_all"},
	{"empty2",		P_REQUIRED,			"v_string_all"},
	{NULL}
};

static struct c_param parameters_argerr[] = {
	{"err1",		P_REQUIRED | P_ARG_ERR,		"v_bool:KLOPPER"},
	{NULL}
};

static int
test_ptext(struct cmd_param *param, const char *arg) {
	struct cmd_param *mode_param = cmd_getparam(param->cs, "utf8");

	param->error = "WARNING";
	xasprintf(&param->message, "NORMALIZED_%s_%s", mode_param->value, param->value);

	return 0;
}

static struct validator v_text1[] = {
	VALIDATOR_FUNC(test_ptext),
	{0}
};
ADD_VALIDATOR(v_text1);

static struct c_param parameters_text[] = {
	{"text1",		P_REQUIRED | P_TEXT,		"v_text1"},
	{NULL}
};


static void
doit(struct cmd *cs)
{
	cmd_done(cs);
}

/*
static void
doit_text(struct cmd *cs)
{
	const char *text1 = cmd_getval(cs, "text1");

	transaction_printf(cs->ts, "result:%s\n", text1);
	cmd_done(cs);
}*/

ADD_COMMAND(test_pflags_xor, NULL, doit, parameters_xor, NULL);
ADD_COMMAND(test_pflags_or, NULL, doit, parameters_or, NULL);
ADD_COMMAND(test_pflags_and, NULL, doit, parameters_and, NULL);
ADD_COMMAND(test_pflags_nand, NULL, doit, parameters_nand, NULL);
ADD_COMMAND(test_pflags_eq, NULL, doit, parameters_eq, NULL);
ADD_COMMAND(test_pflags_imply, NULL, doit, parameters_imply, NULL);
ADD_COMMAND(test_pflags_empty, NULL, doit, parameters_empty, NULL);
ADD_COMMAND(test_pflags_argerr, NULL, doit, parameters_argerr, NULL);
ADD_COMMAND(test_pflags_text, NULL, doit, parameters_text, NULL);

