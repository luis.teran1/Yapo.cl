#ifdef STATE_LUT_FIRST_PASS
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <time.h>
#include <stdlib.h>
#include <unistd.h>

#include "trans.h"
#include "command.h"
#include "util.h"
#include "ctemplates.h"
#include "sql_worker.h"
#endif

#include "state_lut.h"

STATE_ENUM(block_sql_worker) {
	STATE(BLOCK_SQL_WORKER_DONE),
	STATE(BLOCK_SQL_WORKER_ERROR),
	STATE(BLOCK_SQL_WORKER_INIT),
	STATE(BLOCK_SQL_WORKER_READ),
};

#ifdef STATE_LUT_FIRST_PASS
#include __FILE__
#else

struct block_sql_worker_state {
	STATE_VAR(block_sql_worker) state;
	struct cmd *cs;
	int nworkers;
	int rworkers;
	struct sql_worker *block_hold;
};

static struct c_param parameters[] = {
	{NULL, 0}
};

static const char *block_sql_worker_fsm(void *v, struct sql_worker *worker, const char *errstr);

/*****************************************************************************
 * block_sql_worker_done
 * Exit function.
 **********/
static void
block_sql_worker_done(struct block_sql_worker_state *state) {
	struct cmd *cs = state->cs;
	free(state);
	cmd_done(cs);
}

/*****************************************************************************
 * bind_insert
 * Initialise template API before a call to sql_worker_template_query().
 **********/
/*****************************************************************************
 * block_sql_worker_fsm
 * State machine
 **********/
static const char *
block_sql_worker_fsm(void *v, struct sql_worker *worker, const char *errstr) {
	struct block_sql_worker_state *state = v;
	struct cmd *cs = state->cs;
	int i;

	while (1) {
		if (errstr) {
			transaction_logprintf(cs->ts, D_ERROR, "block_sql_worker_fsm statement failed (%s(%d), %s)",
					      STATE_NAME(block_sql_worker, state->state),
					      state->state,
					      errstr);
			cs->status = "TRANS_DATABASE_ERROR";
			cs->message = xstrdup(errstr);
			state->state = BLOCK_SQL_WORKER_ERROR;
		}

		transaction_logprintf(cs->ts, D_DEBUG, "block_sql_worker_fsm(%p, %p, %p), state=%s(%d)",
				      state, worker, errstr, STATE_NAME(block_sql_worker, state->state), state->state);

		switch (state->state) {
		case BLOCK_SQL_WORKER_INIT:

			state->state = BLOCK_SQL_WORKER_READ;

			for (i = 0 ; i < 25 ; i++) {
				transaction_printf(cs->ts, "info:requesting worker %d\n", i);
				sql_worker_template_query("pgsql.master", "sql/block_sql_worker.sql", NULL, 
						block_sql_worker_fsm, state, cs->ts->log_string);
				state->nworkers++;
			}

			return NULL;

		case BLOCK_SQL_WORKER_READ:
			transaction_printf(cs->ts, "info:result worker %d\n", state->rworkers++);

			if (state->rworkers < 24)
				return NULL;

			if (state->rworkers == 24) {
				state->block_hold = sql_worker_hold(worker);
				return NULL;
			}

			if (state->rworkers == 25) {
				sql_worker_template_newquery(state->block_hold, "sql/block_sql_worker.sql", NULL);
				state->block_hold = NULL;
				return NULL;
			}

			state->state = BLOCK_SQL_WORKER_DONE;
			continue;

		case BLOCK_SQL_WORKER_DONE:
			block_sql_worker_done(state);
			return NULL;

		case BLOCK_SQL_WORKER_ERROR:
			transaction_logprintf(cs->ts, D_ERROR, "Error state");
			block_sql_worker_done(state);
			return NULL;

		default:
			/* MAJOR STATE ERROR */
			cs->status = "TRANS_ERROR";
			transaction_logprintf(cs->ts, D_ERROR, "Unknown state (%d)",
					      state->state);

			block_sql_worker_done(state); /* If this state were run there would be a memory leak */
			return NULL;

		}
	}
}

/*****************************************************************************
 * block_sql_worker
 * Main function that initialise a state-structure and calls the state machine.
 **********/
static void
block_sql_worker(struct cmd *cs) {
	transaction_logprintf(cs->ts, D_DEBUG, "Start block_sql_worker transaction");
	struct block_sql_worker_state *state;
	state = zmalloc(sizeof(*state));
	state->cs = cs;
	state->state = BLOCK_SQL_WORKER_INIT;

	block_sql_worker_fsm(state, NULL, NULL);
}

ADD_COMMAND(block_sql_worker,     /* Name of command */
		NULL,
            block_sql_worker,     /* Main function name */
            parameters,  /* Parameters struct */
            "block_sql_worker command");/* Command description */

#endif /*STATE_LUT_FIRST_PASS*/
