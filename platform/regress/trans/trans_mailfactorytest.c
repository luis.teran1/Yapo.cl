#include "factory.h"

extern struct bconf_node *bconf_root;

static struct c_param parameters[] = {
	{"remote_addr", 	0, 			"v_ip"},
	{NULL, 0}
};

static struct factory_data data = {
	"factory_test",
	FACTORY_NO_RES_CNTR
};

static const struct factory_action actions[] = {
	FA_MAIL("mail/factory_mail.txt"),
	FA_DONE()
};

FACTORY_TRANS(mailfactorytest, parameters, data, actions);
