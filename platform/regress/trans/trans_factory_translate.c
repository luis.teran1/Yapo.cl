#include "factory.h"

static struct factory_data data = {
	NULL,
};

static const struct factory_action nested_actions[] = {
	FA_SQL(NULL, "pgsql.slave", "sql/nested_proc_raise_exception.sql", 0),
	FA_DONE()
};

static const struct factory_action error_actions[] = {
	FA_SQL(NULL, "pgsql.slave", "sql/raise_exception.sql", 0),
	FA_DONE()
};


FACTORY_TRANS(factory_translate_nested, NULL, data, nested_actions);
FACTORY_TRANS(factory_translate_error, NULL, data, error_actions);
