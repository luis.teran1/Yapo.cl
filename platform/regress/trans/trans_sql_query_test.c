
#include "trans.h"
#include "command.h"
#include "sql_worker.h"

#include "state_lut.h"

STATE_ENUM(sql_query_test) {
	STATE(SQL_QUERY_TEST_DONE),
	STATE(SQL_QUERY_TEST_ERROR),
	STATE(SQL_QUERY_TEST_INIT),
	STATE(SQL_QUERY_TEST_FAILED),
	STATE(SQL_QUERY_TEST_RESULT),
};

#ifdef STATE_LUT_FIRST_PASS
#include __FILE__
#else

static struct c_param parameters[] = {
	{NULL, 0}
};

struct state {
	STATE_VAR(sql_query_test) state;
	struct cmd *cs;
};

static const char *sql_query_test_fsm(void *v, struct sql_worker *worker, const char *errstr);

static void
sql_query_test_done(struct state *state) {
	struct cmd *cs = state->cs;
	free(state);
	cmd_done(cs);
}

static const char *
sql_query_test_fsm(void *v, struct sql_worker *worker, const char *errstr) {
	struct state *state = v;
	struct cmd *cs = state->cs;


	while (1) {
		if (errstr) {
			transaction_logprintf(cs->ts, D_ERROR, "sql_query_test_fsm statement failed (%s(%d), %s)",
					      STATE_NAME(sql_query_test, state->state),
					      state->state,
					      errstr);
			/* not going to error state here */
		}

		transaction_logprintf(cs->ts, D_DEBUG, "sql_query_test_fsm(%p, %p, %p), state=%s(%d)",
				      state, worker, errstr, STATE_NAME(sql_query_test, state->state), state->state);

		switch (state->state) {
		case SQL_QUERY_TEST_INIT:
			state->state = SQL_QUERY_TEST_FAILED;

			sql_worker_template_query("pgsql.slave", "sql/sql_query_test_fail.sql", NULL,
					sql_query_test_fsm, state, cs->ts->log_string);

			return NULL;

		case SQL_QUERY_TEST_FAILED:
			if (worker) {
				state->state = SQL_QUERY_TEST_RESULT;
				sql_worker_template_newquery(worker, "sql/sql_query_test_success.sql", NULL);
				return NULL;
			}
			state->state = SQL_QUERY_TEST_ERROR;
			continue;

		case SQL_QUERY_TEST_RESULT:
			state->state = SQL_QUERY_TEST_DONE;
			if (worker && worker->next_row(worker)) {
				transaction_printf(cs->ts, "meaningoflife:%s\n", worker->get_value(worker, 0));
			}
			continue;

		case SQL_QUERY_TEST_DONE:
			sql_query_test_done(state);
			return NULL;

		case SQL_QUERY_TEST_ERROR:
			cs->status = "TRANS_ERROR";
			if (errstr)
				cs->message = xstrdup(errstr);
			transaction_printf(cs->ts, "test:failed\n");
			transaction_logprintf(cs->ts, D_ERROR, "Error state");
			sql_query_test_done(state);
			return NULL;

		default:
			/* MAJOR STATE ERROR */
			cs->status = "TRANS_ERROR";
			transaction_logprintf(cs->ts, D_ERROR, "Unknown state (%d)",
					      state->state);

			sql_query_test_done(state);
			return NULL;
		}

	}
}

static void
sql_query_test(struct cmd *cs) {
	struct state *state;
	state = zmalloc(sizeof(*state));
	state->cs = cs;
	state->state = SQL_QUERY_TEST_INIT;

	sql_query_test_fsm(state, NULL, NULL);
}

ADD_COMMAND(sql_query_test, NULL, sql_query_test, parameters, NULL);

#endif /*STATE_LUT_FIRST_PASS*/
