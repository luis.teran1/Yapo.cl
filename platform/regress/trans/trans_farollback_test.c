#include <unistd.h>
#include "factory.h"

extern struct bconf_node *bconf_root;

static struct c_param parameters[] = {
	{"sleep", 	P_OPTIONAL,	"v_bool"},
	{"doit", 	P_OPTIONAL,	"v_bool"},
        {NULL, 0}
};

static struct factory_data data = {
        "fatrans_test",
        FACTORY_NO_RES_CNTR | FACTORY_EMPTY_RESULT_OK
};

static int
somefunc(struct cmd *cs, struct bpapi *api) {
	/* Use sleep if in need to test slow execution */
	if (cmd_haskey(cs, "sleep"))
		sleep(1);

	return 0;
}

static const struct factory_action farollback_success_single[] = {
	FA_SQL(NULL, "pgsql.master", "sql/farollback_begin.sql", FASQL_OUT | FASQL_HOLD_WORKER),
	FA_FUNC(somefunc),
	FA_SQL(NULL, "", "sql/rollback_commit.sql", FASQL_REUSE_WORKER),
        FA_DONE()
};

static const struct factory_action farollback_success_multi[] = {
	FA_SQL(NULL, "pgsql.master", "sql/farollback_begin.sql", FASQL_OUT | FASQL_HOLD_WORKER),
	FA_FUNC(somefunc),
	FA_SQL(NULL, "", "sql/sql_query_test_success.sql", FASQL_REUSE_WORKER),
	FA_SQL(NULL, "pgsql.master", "sql/farollback_begin.sql", FASQL_OUT | FASQL_HOLD_WORKER),
	FA_SQL(NULL, "", "sql/rollback_commit.sql", FASQL_REUSE_WORKER),
        FA_DONE()
};

static const struct factory_action farollback_fail_hold_worker[] = {
	FA_SQL(NULL, "pgsql.master", "sql/farollback_begin.sql", FASQL_OUT | FASQL_HOLD_WORKER),
	FA_FUNC(somefunc),
	FA_SQL(NULL, "pgsql.master", "sql/rollback_commit.sql", FASQL_HOLD_WORKER),
        FA_DONE()
};

static const struct factory_action farollback_fail_reuse_worker[] = {
	FA_SQL(NULL, "pgsql.master", "sql/sql_query_test_success.sql", FASQL_REUSE_WORKER),
        FA_DONE()
};

static const struct factory_action farollback_fail_both[] = {
	FA_SQL(NULL, "pgsql.master", "sql/fatrans_test.sql", FASQL_OUT | FASQL_HOLD_WORKER | FASQL_REUSE_WORKER),
        FA_DONE()
};

/* Should succeed */
FACTORY_TRANS(farollback_success_single, parameters, data, farollback_success_single);

/* Should succeed */
FACTORY_TRANS(farollback_success_multi, parameters, data, farollback_success_multi);

/* Should fail due to FASQL_HOLD_WORKER call before reusing it */
FACTORY_TRANS(farollback_fail_hold_worker, parameters, data, farollback_fail_hold_worker);

/* Should fail due to FASQL_REUSE_WORKER without a reserved worker */
FACTORY_TRANS(farollback_fail_reuse_worker, parameters, data, farollback_fail_reuse_worker);

/* Should fail due to incompatible flags set */
FACTORY_TRANS(farollback_fail_both, parameters, data, farollback_fail_both);

