#ifdef STATE_LUT_FIRST_PASS
#include "factory.h"
#include "trans_at.h"
#include <unistd.h>
#endif

#include "state_lut.h"

STATE_ENUM(sql_async_stress) {
	STATE(SLQ_ASYNC_STRESS_INIT),
	STATE(SQL_ASYNC_STRESS_QUERY),
	STATE(SLQ_ASYNC_STRESS_RESULT),
	STATE(SLQ_ASYNC_STRESS_CALL_SLAVE),
	STATE(SLQ_ASYNC_STRESS_RESULT_SLAVE),
	STATE(SLQ_ASYNC_STRESS_AT),
	STATE(SLQ_ASYNC_STRESS_DONE),
	STATE(SLQ_ASYNC_STRESS_ERROR)
};

#ifdef STATE_LUT_FIRST_PASS
#include __FILE__
#else

struct sql_async_stress_state {
	STATE_VAR(sql_async_stress) state;
	struct cmd *cs;
	struct command_bpapi cba;
};

static struct c_param sql_async_stress_params[] = {
	{"sleep", P_OPTIONAL, "v_integer"},
	{"master_first", P_OPTIONAL, "v_bool"},
	{"sqlw_dont_reuse", P_OPTIONAL, "v_bool"},
	{"at", P_OPTIONAL, "v_bool"},
	{NULL}
};

static void
sql_async_stress_done(struct sql_async_stress_state *state) {
	struct cmd *cs = state->cs;

        command_bpapi_free(&state->cba);
	free(state);

	cmd_done(cs);
}

static const char *
sql_async_stress_fsm(void *v, struct sql_worker *worker, const char *errstr) {
	struct sql_async_stress_state *state = v;
	struct cmd *cs = state->cs;

	if (errstr) {
		transaction_logprintf(cs->ts, D_ERROR, "sql statement failed [%s(%d)] [%s]",
					STATE_NAME(sql_async_stress, state->state),
					state->state,
					errstr);
		cs->status = "TRANS_DATABASE_ERROR";
		cs->message = xstrdup(errstr);
		state->state = SLQ_ASYNC_STRESS_ERROR;
	}

	while (1) {
		transaction_logprintf(cs->ts, D_DEBUG, "sql_async_stress_fsm(%p, %p, %p), state=%s(%d)",
				      state, worker, errstr, STATE_NAME(sql_async_stress, state->state), state->state);
		switch (state->state) {
			case SLQ_ASYNC_STRESS_INIT:
				state->state = SQL_ASYNC_STRESS_QUERY;
				if (!cmd_haskey(cs, "sqlw_dont_reuse"))
					continue;

				sql_worker_template_query_flags("pgsql.archive", SQLW_DONT_REUSE, "sql/statement_timeout.sql", NULL,
						sql_async_stress_fsm, state, cs->ts->log_string);

				return NULL;

			case SQL_ASYNC_STRESS_QUERY:
				state->state = SLQ_ASYNC_STRESS_RESULT;
				command_bpapi_init(&state->cba, cs, CMD_BPAPI_HOST, NULL);
				
				if (cmd_haskey(cs, "sleep"))
					bpapi_insert(&state->cba.ba, "sleep", cmd_getval(cs, "sleep"));
				sql_worker_template_query((cmd_haskey(cs, "master_first") ? "pgsql.master" : "pgsql.slave"),
						"sql/pg_sleep.sql", &state->cba.ba, sql_async_stress_fsm, state, cs->ts->log_string);
				return NULL;
 
			case SLQ_ASYNC_STRESS_RESULT:
				if (cmd_haskey(cs, "at"))
					state->state = SLQ_ASYNC_STRESS_AT;
				else
					state->state = SLQ_ASYNC_STRESS_CALL_SLAVE;
				
				if (cmd_haskey(cs, "sleep")) {
					transaction_printf(cs->ts, "sleeping:%s\n", cmd_getval(cs, "sleep"));
					sleep(atoi(cmd_getval(cs, "sleep")));
				}
				continue;

			case SLQ_ASYNC_STRESS_CALL_SLAVE:
				state->state = SLQ_ASYNC_STRESS_RESULT_SLAVE;
				sql_worker_template_query((cmd_haskey(cs, "master_first") ? "pgsql.slave" : "pgsql.master"),
						"sql/pg_sleep.sql", &state->cba.ba, sql_async_stress_fsm, state, cs->ts->log_string);

				return NULL;
			
			case SLQ_ASYNC_STRESS_RESULT_SLAVE:
				state->state = SLQ_ASYNC_STRESS_DONE;
				if (worker && worker->next_row(worker))
					transaction_printf(cs->ts, "got:%s\n", worker->get_value(worker, 0));

				continue;

			case SLQ_ASYNC_STRESS_AT:
				state->state = SLQ_ASYNC_STRESS_DONE;
				at_clear_commands("foo", "bar", cs->ts->log_string);

				continue;	
			case SLQ_ASYNC_STRESS_DONE:
				sql_async_stress_done(state);
				return NULL;

			case SLQ_ASYNC_STRESS_ERROR:
				transaction_logprintf(cs->ts, D_ERROR, "Error state (%d)",
					state->state);
				sql_async_stress_done(state);
				return NULL;

			default:
				cs->status = "TRANS_ERROR";
				transaction_logprintf(cs->ts, D_ERROR, "Unknown state (%d)",
					state->state);
				sql_async_stress_done(state);
				return NULL;
		}
	}
} 

static void
sql_async_stress(struct cmd *cs) {
	transaction_logprintf(cs->ts, D_DEBUG, "Start sql_async_stress transaction");
	struct sql_async_stress_state *state;
	state = zmalloc(sizeof(*state));
	state->cs = cs;
	state->state = SLQ_ASYNC_STRESS_INIT;

	sql_async_stress_fsm(state, NULL, NULL);
}

ADD_COMMAND(sql_async_stress, NULL, sql_async_stress, sql_async_stress_params, "Command used to test potential deadlock under stress"); 
#endif
