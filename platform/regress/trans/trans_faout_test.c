#include "factory.h"

extern struct bconf_node *bconf_root;

static struct c_param parameters[] = {
	{"remote_addr", 	0, 			"v_ip"},
	{"blobit", 	0, 			"v_bool"},
	{NULL, 0}
};

static struct factory_data data = {
	"factory_test",
	FACTORY_NO_RES_CNTR
};

static const struct factory_action actions[] = {
	FA_OUT("trans/faout_test.txt"),
	FA_OUT("trans/faout_blob_test.txt"),
	FA_DONE()
};

FACTORY_TRANS(faout_test, parameters, data, actions);
