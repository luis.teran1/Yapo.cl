#include <stdio.h>

#include "trans.h"
#include "trans_at.h"
#include "command.h"

static struct c_param parameters[] = {
	{"use_redis",	P_REQUIRED,	"v_bool"},
	{"backwards",	P_OPTIONAL,	"v_bool"},
	{NULL, 0, NULL}
};

static void
register_command(struct cmd *cs)
{
	const char *cmd = "cmd:foo\ncommit:0";
	const char *sub_queue = "register_queue";
	const char *info = "test";
	int critical = 0;
	int seconds = 60 * 60;
	int use_redis = atoi(cmd_getval(cs, "use_redis"));
	
	if (cmd_haskey(cs, "backwards")) {
		if (use_redis) {
			at_register_redis_cmd(cmd, seconds, sub_queue, info, critical);
		} else {
			at_register_cmd(cmd, seconds, sub_queue, info, critical);
		}
	} else {
		at_register_storage(cmd, seconds, sub_queue, info, critical, use_redis);
	}
	cmd_done(cs);
}

ADD_COMMAND(register_command, NULL, register_command, parameters, NULL);
