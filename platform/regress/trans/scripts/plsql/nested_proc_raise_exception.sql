CREATE OR REPLACE FUNCTION nested_proc_raise_exception() RETURNS void AS $$
BEGIN
	 PERFORM raise_exception();
END;
$$ LANGUAGE plpgsql;

