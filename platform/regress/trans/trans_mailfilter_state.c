#include "factory.h"

static struct factory_data data = {
	"mail filter test",
	FACTORY_NO_RES_CNTR
};

static const struct factory_action actions[] = {
	FA_MAIL("mail/factory_mail_fs.txt"),
	FA_DONE()
};

FACTORY_TRANS(mail_filter_state_test, NULL, data, actions);
