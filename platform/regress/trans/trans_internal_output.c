
#include "trans.h"
#include "command.h"

static void
internal_output(struct cmd *cs) {
	if (cs->ts->internal) {
		transaction_printf(cs->ts, "internal:1\n");
	} else {
		struct internal_cmd *cmd = zmalloc(sizeof (*cmd));
		char *buf, *ptr, *line;

		cmd->cmd_string = xstrdup("cmd:internal_output\ncommit:1\n");

		ptr = buf = transaction_internal_output(cmd);
		while ((line = strsep(&ptr, "\n")))
			transaction_printf(cs->ts, "i:%s\n", line);
		free(buf);

		/* Test non-NULL data doesn't crash. */
		transaction_internal_printf(TRANSACTION_INTERNAL_SYNC_CB, (void*)-1,
				"cmd:internal_output\n"
				"commit:1\n");
	}

	cmd_done(cs);
}

ADD_COMMAND(internal_output, NULL, internal_output, NULL, NULL);
