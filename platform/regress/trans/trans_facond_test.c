#include "factory.h"

static struct c_param parameters[] = {
	{"conditional", 0, "v_bool"},
	{NULL, 0}
};

static struct factory_data data = {
	"conditional"
};

static int
test_true_func(struct cmd *cs, struct bpapi *ba)
{
	transaction_printf(cs->ts, "block:true\n");
	return 0;
}

static int
test_false_func(struct cmd *cs, struct bpapi *ba)
{
	transaction_printf(cs->ts, "block:false\n");
	return 0;
}

static int
finished(struct cmd *cs, struct bpapi *ba)
{
	transaction_printf(cs->ts, "finished:ok\n");
	return 0;
}


static const struct factory_action test_true = FA_FUNC(test_true_func);
static const struct factory_action test_false = FA_FUNC(test_false_func);

static const struct factory_action *
test(struct cmd *cs, struct bpapi *ba)
{
	if (atoi(cmd_getval(cs, "conditional")) == 1)
		return &test_true;
	return &test_false;
}

static const struct factory_action *
test2(struct cmd *cs, struct bpapi *ba)
{
	if (atoi(cmd_getval(cs, "conditional")) == 1)
		return &test_true;
	return NULL;
}

static const struct factory_action actions[] = {
	FA_COND(test),
	FA_COND(test2),
	FA_FUNC(finished),
	FA_DONE()
};

FACTORY_TRANS(facond_test, parameters, data, actions);
