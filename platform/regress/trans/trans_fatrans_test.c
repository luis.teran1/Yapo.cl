#include "factory.h"

extern struct bconf_node *bconf_root;

static struct c_param parameters[] = {
	{"id", 	0, 			"v_integer"},
	{NULL, 0}
};

static struct factory_data data = {
	"fatrans_test",
	FACTORY_NO_RES_CNTR
};

static const struct factory_action actions[] = {
	FA_SQL(NULL, "pgsql.slave", "sql/fatrans_test.sql", 0),
	FA_TRANS("trans/fatrans_test.txt", "fat_", FATRANS_OUT | FATRANS_BACAPTURE),
	FA_MAIL("mail/fatrans_test.txt"),
	FA_DONE()
};

FACTORY_TRANS(fatrans_test, parameters, data, actions);
