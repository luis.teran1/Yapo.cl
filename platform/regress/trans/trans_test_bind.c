#include <stdio.h>

#include "trans.h"
#include "command.h"

/*
 * XXX: IMPORTANT!
 * It is not recommended to use validators of this nature
 * This validator has been added in order to test that the 
 * function works since some sites tend to use those.
 * Validate properly AND use the P_EMPTY flag.
 */
static struct validator v_none[] = {
	{0}
};
ADD_VALIDATOR(v_none);

static struct c_param parameters[] = {
	{"empty",	P_OPTIONAL,	"v_none"},
	{"multi",	P_OPTIONAL | P_MULTI,	"v_string_all"},
	{"allow_multi",	P_OPTIONAL,	"v_true"},
	{NULL}
};

static void
doit(struct cmd *cs)
{
	struct command_bpapi cba;
	int flags = 0;

	command_bpapi_init(&cba, cs, CMD_BPAPI_HOST, NULL);
	struct buf_string *buf = BPAPI_OUTPUT_DATA(&cba.ba);

	if (cmd_haskey(cs, "allow_multi"))
		flags |= BIND_ALLOW_MULTI;

	transaction_printf(cs->ts, "flag_multi:%s\n", (flags & BIND_ALLOW_MULTI) ? "1" : "0");

	transaction_bind_params_bpapi(cs, &cba.ba, "rendered_", flags);
	call_template(&cba.ba, "trans/bind_out.txt");
	if (buf->buf) {
		/* Ignore formating */
		transaction_printf(cs->ts, "%s", buf->buf);
	}

	command_bpapi_free(&cba);
	cmd_done(cs);
}

ADD_COMMAND(test_bind, NULL, doit, parameters, NULL);
