#include "factory.h"

static struct c_param parameters[] = {
	{NULL, 0}
};

static const char *cmd_strings[] = {
	"cmd:internal_log_string\ncommit:1\nend\n",
	"cmd:internal_log_string\ncommit:1\nlog_string:bar\nend\n",
};

static void
run_internal_cb(struct internal_cmd *cmd, const char *line, void *cbdata) {
	struct buf_string *str = cbdata;

	if (line)
		bscat(str, "i:%s\n", line);
}

static void
run_internal(struct cmd *cs, int cs_idx, char *ls) {
	struct internal_cmd *cmd = zmalloc(sizeof (*cmd));
	struct buf_string str = {0};

	cmd->cb = TRANSACTION_INTERNAL_SYNC_CB;
	cmd->cmd_string = xstrdup(cmd_strings[cs_idx]);
	cmd->log_string = ls;

	{
		struct event_base *ibase = NULL, *oldbase = NULL;
		extern pthread_key_t csbase_key;

		ibase = cmd->base = event_base_new();
		oldbase = pthread_getspecific(csbase_key);
		pthread_setspecific(csbase_key, ibase);

		cmd->cb = run_internal_cb;
		cmd->data = &str;

		transaction_internal(cmd);

		while (event_base_dispatch(ibase) == -1)
			;
		event_base_free(ibase);
		pthread_setspecific(csbase_key, oldbase);
	}

	if (str.buf) {
		transaction_printf(cs->ts, "%s", str.buf);
		free(str.buf);
	}
}

static void
internal_log_string(struct cmd *cs) {
	if (cs->ts->log_string)
		transaction_printf(cs->ts, "log_string:%s\n", cs->ts->log_string);
	if (!cs->ts->internal) {
		/* Case 1 (most common), no log_string set, no log_string in cmd_string */
		run_internal(cs, 0, NULL);

		/* Case 2, log_string set, no log_string in cmd_string */
		run_internal(cs, 0, xstrdup("foo"));

		/* Case 3, no log_string set, log_string in cmd_string */
		run_internal(cs, 1, NULL);

		/* Case 4, log_string set, log_string in cmd_string */
		run_internal(cs, 1, xstrdup("foo"));
	}
	cmd_done(cs);
}

ADD_COMMAND(internal_log_string,     /* Name of command */
		NULL,
		internal_log_string,     /* Main function name */
		parameters,  /* Parameters struct */
		"internal_log_string command");/* Command description */
