#include "factory.h"

static struct c_param parameters[] = {
	{"doit", P_REQUIRED, "v_bool"},
	{NULL, 0}
};

static struct factory_data data = {
	"faat_test",
	FACTORY_NO_RES_CNTR
};

static const struct factory_action actions[] = {
	FA_AT("trans/faat_test.txt", "redis_queue", "info", 60 * 60, 0, FAAT_REDIS),
	FA_AT("trans/faat_test.txt", "sub_queue", "info", 60 * 60, 0, FAAT_DB),
	FA_DONE()
};

FACTORY_TRANS(faat_test, parameters, data, actions);
