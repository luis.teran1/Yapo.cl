#include <unistd.h>

#include "trans.h"
#include "command.h"
#include "ev_popen.h"

static struct c_param parameters[] = {
	/* Number of processes launched */
	{"num", P_REQUIRED, "v_integer"},
	/* Passed as argument to cmd */
	{"arg", P_REQUIRED, "v_integer"},
	/* Delay between processes launch */
	{"delay", 0, "v_integer"},
	/* cmd to launch */
	{"cmd", 0, "v_string_all"},
	{NULL, 0, 0}
};

struct evpopen_test_state {
	struct cmd *cs;
	int nprocs;
};

static void
cb_read(void *arg, struct bufferevent *be) {
	return;
}


static void
cb_write(void *arg, struct bufferevent *be) {
	return;
}

static void
cb_close(void *arg, int ret) {
	struct evpopen_test_state *state = arg;

	if (--state->nprocs == 0) {
		cmd_done(state->cs);
		free(state);
	}
}


static void
evpopen_test(struct cmd *cs) {
	struct evpopen_test_state *state = zmalloc(sizeof(*state));
	state->cs = cs;

	int i = 0;
	int delay = cmd_haskey(cs, "delay") ? atoi(cmd_getval(cs, "delay")) : 0;
	char *cmd;
	xasprintf(&cmd, "%s", cmd_haskey(cs, "cmd") ? cmd_getval(cs, "cmd") : "/bin/sleep");

	const char **argv = xmalloc(sizeof(char*) * 3);
	argv[0] = cmd;
	argv[1] = cmd_getval(cs, "arg");
	argv[2] = NULL;

	transaction_logprintf(cs->ts, D_DEBUG, "Launching %s %s with delay %d", argv[1], cmd, delay);

	state->nprocs = atoi(cmd_getval(cs, "num"));
	for (i = 0 ; i < atoi(cmd_getval(cs, "num")) ; ++i) {
		transaction_printf(cs->ts, "launching:%d\n", i);
		ev_popen(cmd, argv, cb_read, cb_write, NULL, cb_close, state);
		if (delay)
			sleep(delay);
	}
	free(cmd);
	free(argv);
}

ADD_COMMAND(evpopen_test, NULL, evpopen_test, parameters, "Launch dummy processes");

