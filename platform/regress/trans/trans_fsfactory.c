#include "factory.h"

static struct factory_data data = {
	"factory_filter_state_test",
};

static const struct factory_action actions[] = {
	FA_OUT("trans/filter_state.txt"),
	FA_DONE()
};

FACTORY_TRANS(filter_state_test, NULL, data, actions);
