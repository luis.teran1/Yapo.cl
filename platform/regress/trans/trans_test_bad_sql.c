#include "factory.h"

static struct c_param parameters[] = {
	{0}
};

static struct factory_data data = {
	"test_bad_sql",
	FACTORY_NO_RES_CNTR
};

static const struct factory_action actions[] = {
	FA_SQL(NULL, "pgsql.slave", "sql/bad.sql", 0),
	FA_DONE()
};

FACTORY_TRANS(test_bad_sql, parameters, data, actions);

