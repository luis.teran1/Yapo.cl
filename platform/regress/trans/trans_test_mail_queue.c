#include "trans.h"
#include "command.h"
#include "trans_mail.h"
#include "mail_queue.h"

struct tmq_state {
	int state;
	struct cmd *cs;
	int calls;
	int fails;
};

static struct c_param tmq_parameters[] = {
	{"batch_size",		P_OPTIONAL,		"v_integer"},
	{NULL, 0, NULL}
};

static void
tmq_cb(struct mail_data *md, void *arg, int status) {
	struct tmq_state *state = arg;

	transaction_logprintf(state->cs->ts, D_DEBUG, "Call %d to test_mail_queue callback, status %d.", ++state->calls, status);

	if (status != 0)
		++state->fails;

}

static const char *
test_mail_queue_fsm(void *v, struct sql_worker *worker, const char *errstr) {
	struct tmq_state *state = v;
	struct cmd *cs = state->cs;

	struct mail_queue *mq = mail_queue_new(cs->ts, NULL);

	for (int i = 0; i < 100; i++) {
		struct mail_data *md = zmalloc(sizeof(*md));
		mail_insert_param(md, "name", "Mr Herp");
		mail_insert_param(md, "from", "herp@derp.com");
		mail_insert_param(md, "to", "herp@derp.com");
		mail_insert_paramf(md, "subject", "Mail queue test %d", i);
		mail_insert_paramf(md, "body", "This is the body of mail %d", i);

		md->command = state;
		md->callback = tmq_cb;
		md->log_string = cs->ts->log_string;

		mail_queue_add(mq, md, "mail/test_mail_queue.txt", NULL);

		transaction_logprintf(cs->ts, D_INFO, "Added mail %d to queue", i+1);
	}

	int processed = mail_queue_flush(mq, cmd_getint_default(cs, "batch_size", -1));
	mail_queue_destroy(mq);

	transaction_printf(cs->ts, "processed:%d\n", processed);
	transaction_printf(cs->ts, "callbacks:%d\n", state->calls);
	transaction_printf(cs->ts, "failed:%d\n", state->fails);

	if (state->fails > 0 || processed != state->calls) {
		transaction_logprintf(cs->ts, LOG_CRIT, "(CRIT) Mail queue test failed");
		cs->status = "TRANS_ERROR";
	}

	cmd_done(cs);
	return NULL;
}

static void
test_mail_queue(struct cmd *cs) {
	struct tmq_state *state;
	transaction_logprintf(cs->ts, D_DEBUG, "test_mail_queue command started");
	state = zmalloc(sizeof(*state));
	state->cs = cs;
	test_mail_queue_fsm(state, NULL, NULL);
}

ADD_COMMAND(test_mail_queue, NULL, test_mail_queue, tmq_parameters, NULL);

