#ifdef STATE_LUT_FIRST_PASS
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <time.h>
#include <stdlib.h>
#include <unistd.h>

#include "trans.h"
#include "command.h"
#include "util.h"
#include "ctemplates.h"
#include "sql_worker.h"
#endif

#include "state_lut.h"

STATE_ENUM(sql_async_put) {
	STATE(SQL_ASYNC_PUT_DONE),
	STATE(SQL_ASYNC_PUT_ERROR),
	STATE(SQL_ASYNC_PUT_INIT),
	STATE(SQL_ASYNC_PUT_QUERY_1),
	STATE(SQL_ASYNC_PUT_QUERY_2),
	STATE(SQL_ASYNC_PUT_SECOND_QUERY),
};

#ifdef STATE_LUT_FIRST_PASS
#include __FILE__
#else

struct sql_async_put_state {
	STATE_VAR(sql_async_put) state;
	struct cmd *cs;
	int nworkers;
	int rworkers;
};

static struct c_param parameters[] = {
	{NULL, 0}
};

static const char *sql_async_put_fsm(void *v, struct sql_worker *worker, const char *errstr);

static void
sql_async_put_done(struct sql_async_put_state *state) {
	struct cmd *cs = state->cs;
	free(state);
	cmd_done(cs);
}

static void
print_async_put(struct cmd *cs, struct sql_worker_async_put *async_put, const char *prefix) {
	transaction_printf(cs->ts,
			"%s.put:%s\n"
			"%s.tested:%s\n"
			"%s.errstr:%s\n",
			prefix, async_put->put ? "true" : "false",
			prefix, async_put->tested ? "true" : "false",
			prefix, async_put->errstr);
}

static const char *
sql_async_put_fsm(void *v, struct sql_worker *worker, const char *errstr) {
	struct sql_async_put_state *state = v;
	struct cmd *cs = state->cs;
	struct sql_worker_async_put *async_put = worker ? worker->async_put : NULL;

	while (1) {
		if (errstr) {
			transaction_logprintf(cs->ts, D_ERROR, "sql_async_put_fsm statement failed (%s(%d), %s)",
					      STATE_NAME(sql_async_put, state->state),
					      state->state,
					      errstr);
			cs->status = "TRANS_DATABASE_ERROR";
			cs->message = xstrdup(errstr);
			state->state = SQL_ASYNC_PUT_ERROR;
		}

		transaction_logprintf(cs->ts, D_DEBUG, "sql_async_put_fsm(%p, %p, %p), state=%s(%d)",
				      state, worker, errstr, STATE_NAME(sql_async_put, state->state), state->state);

		switch (state->state) {
		case SQL_ASYNC_PUT_INIT:

			state->state = SQL_ASYNC_PUT_QUERY_1;

			sql_worker_template_query("pgsql.slave", "sql/sql_put_async.sql", NULL,
					sql_async_put_fsm, state, cs->ts->log_string);

			return NULL;

		case SQL_ASYNC_PUT_QUERY_1:
			print_async_put(cs, async_put, "before_put");

			/* Will fail. XXX should probably log something */
			sql_worker_put(worker);

			print_async_put(cs, async_put, "after_put");

			state->state = SQL_ASYNC_PUT_QUERY_2;
			return NULL;

		case SQL_ASYNC_PUT_QUERY_2:
			print_async_put(cs, async_put, "before_query");

			/* Should put */
			sql_worker_template_query("pgsql.slave", "sql/sql_put_async_second.sql", NULL,
					sql_async_put_fsm, state, cs->ts->log_string);

			print_async_put(cs, async_put, "after_query");

			state->state = SQL_ASYNC_PUT_SECOND_QUERY;
			return NULL;

		case SQL_ASYNC_PUT_SECOND_QUERY:
			/* Should not put */
			sql_worker_template_query_flags("pgsql.slave", SQLR_NOAUTOPUT, "sql/sql_put_async_second.sql", NULL,
					sql_async_put_fsm, state, cs->ts->log_string);

			print_async_put(cs, async_put, "after_third_query");

			state->state = SQL_ASYNC_PUT_DONE;
			return NULL;

		case SQL_ASYNC_PUT_DONE:
			sql_async_put_done(state);
			return NULL;

		case SQL_ASYNC_PUT_ERROR:
			transaction_logprintf(cs->ts, D_ERROR, "Error state");
			sql_async_put_done(state);
			return NULL;

		default:
			/* MAJOR STATE ERROR */
			cs->status = "TRANS_ERROR";
			transaction_logprintf(cs->ts, D_ERROR, "Unknown state (%d)",
					      state->state);

			sql_async_put_done(state);
			return NULL;
		}
	}
}
static void
sql_async_put(struct cmd *cs) {
	transaction_logprintf(cs->ts, D_DEBUG, "Start sql_async_put transaction");
	struct sql_async_put_state *state;
	state = zmalloc(sizeof(*state));
	state->cs = cs;
	state->state = SQL_ASYNC_PUT_INIT;

	sql_async_put_fsm(state, NULL, NULL);
}

ADD_COMMAND(sql_async_put,     /* Name of command */
		NULL,
		sql_async_put,     /* Main function name */
		parameters,  /* Parameters struct */
		"sql_async_put command");/* Command description */

#endif
