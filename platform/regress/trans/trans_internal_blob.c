#include "factory.h"

static void
internal_cb(struct internal_cmd *cmd, const char *line, void *cbdata) {
	struct buf_string *buf = cbdata;
	
	if (line)
		bscat(buf, "line:%s\n", line);
}

static void
internal_blob(struct cmd *cs) {
	if (cs->ts->internal) {
		transaction_printf(cs->ts, "blob:%ld:foo\n1234\n6789", strlen("1234\n6789"));
		transaction_printf(cs->ts, "key:val\n");
	} else {
		struct buf_string buf = {0};
		struct event_base *internal_base = NULL;
		struct event_base *old_base = NULL;
		extern pthread_key_t csbase_key;

		internal_base = event_base_new();
		old_base = pthread_getspecific(csbase_key);

		transaction_internal_base_printf(internal_base, internal_cb, &buf, "cmd:internal_blob\ncommit:1\nend\n"); 

		while (event_base_dispatch(internal_base) == -1)
			continue;

		event_base_free(internal_base);
		pthread_setspecific(csbase_key, old_base);

		if (buf.buf) {
			transaction_printf(cs->ts, "%s", buf.buf);
			free(buf.buf);
		}
	}
	cmd_done(cs);
}

ADD_COMMAND(internal_blob, NULL, internal_blob, NULL, "Test transaction_internal blob in callback");
