<?php
if (!($im = imagecreatefromjpeg($argv[1])))
	$im = imagecreatefrompng($argv[1]);

$w = imagesx($im);
$h = imagesy($im);

function p($im, $x, $y, $l) {
	$c = imagecolorat($im, (int)$x, (int)$y);
	$r = (($c >> 16) & 255) >> 3;
	$g = (($c >> 8) & 255) >> 3;
	$b = (($c >> 0) & 255) >> 3;
	print("$l: $r, $g, $b\n");
}

$margin = intval(@$argv[2]);

print("$w $h\n");
p($im, $margin, $margin, "ul");
p($im, $w - 1 - $margin, $margin, "ur");
p($im, $margin, $h - 1 - $margin, "ll");
p($im, $w - 1 - $margin, $h - 1 - $margin, "lr");
p($im, $w / 2 , $h / 2, "cc");
if ($margin > 0) { /* Hacky */
	p($im, $w / 2 , 0, "tm");
	p($im, 0 , $h /2, "ml");
	p($im, $w - 1 - $margin , $h /2, "mr");
	p($im, $w / 2, $h - 1 - $margin, "bm");
}
?>
