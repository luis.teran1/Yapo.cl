<?php
$margin = $argv[2] / 8;
$im = imagecreatetruecolor($argv[2], $argv[2]);
imagefilledrectangle($im, 0, 0, $argv[2] - 1, $argv[2] - 1, imagecolorallocate($im, 255, 255, 0));
imagefilledrectangle($im, $margin, $margin, $argv[2] - $margin, $argv[2] - $margin, imagecolorallocate($im, 0, 255, 0));
imagepng($im, $argv[1]);
?>
