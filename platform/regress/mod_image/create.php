<?php
$width = 640;
$height = 480;
$nextarg = 2;

if (count($argv) >= 3 && strpos($argv[$nextarg], 'dim=') === 0) {
	list($width, $height) = explode(",", substr($argv[2], 4));
	++$nextarg;
}

/* print "Generating image with dim [$width, $height]\n"; */

$im = imagecreatetruecolor($width, $height);
imagefilledrectangle($im, 0, 0, $width-1, $height-1, imagecolorallocate($im, 0, 0, 255));
if ($width > 80 && $height > 60)
	imagefilledrectangle($im, 80, 60, $width-80, $height-60, imagecolorallocate($im, 255, 0, 0));
if (preg_match("/.jpg$/", $argv[1]))
	imagejpeg($im, $argv[1]);
else
	imagepng($im, $argv[1]);
if (count($argv) >= $nextarg+1 && strpos($argv[$nextarg], 'source=') === 0) {
	$fh = fopen($argv[1], 'a');
	$len = strlen($argv[2]);
	$magic = 235;
	fwrite($fh, "\xff" . chr($magic) . chr((($len + 6) >> 8) & 0xFF)  . chr(($len + 6) & 0xFF), 4);
	fwrite($fh, $argv[2]);
	fwrite($fh, chr($len & 0xFF) . chr(($len >> 8) & 0xFF) . chr(($len >> 16) & 0xFF) . chr(($len >> 24) & 0xFF), 4);
	fwrite($fh, "\xff" . chr($magic) . "\x00\x02", 4);
	fclose($fh);
}
?>
