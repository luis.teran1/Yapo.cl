
#include "mod_list.h"

static int
handle_ca(struct list_call *call, const char *ca) {
	char reg[12];
	int region = 0;

	sscanf(ca, "%d", &region);

	/* Validate region */
	if (region) {
		snprintf(reg, sizeof (reg), "%d", region);
		if (!bconf_vget(call->conf->root, "common.region", reg, NULL))
			region = 0;
	}

	if (region) {
		bpapi_set_printf(&call->bparse, "ca", "%d", region);
		bpapi_set_int(&call->bparse, "ca_region", region);
		return 1;
	}
	return 0;
}

void
parse_qs_cb_local(struct list_call *call, enum query_string_var qsv, char *key, char *value, int *insert_bparse, int *append_qs) {
	switch (qsv) {
	case QSV_CA:
		handle_ca(call, value);
		break;
	default:
		break;
	}
}
