#include <unistd.h>
#include <string.h>
#include <stdlib.h>

#include "bconf.h"
#include "bconfd_client.h"
#include "memalloc_functions.h"
#include "bconfig.h"

static void
usage(const char *prog) {
	fprintf(stderr, "Usage: %s [-r key=value] [-c command] [-p <protocol>] [-i] <config_file>\n", prog);
	exit(1);
}

int
main(int argc, char *argv[]) {
	struct bconfd_call call = { 0 };
	struct bconf_node *root = NULL;
	const char *progname = argv[0];
	const char *config_file = NULL;
	int init = 0;
	int opt, r;

	while ((opt = getopt(argc, argv, "r:c:p:i")) != -1) {
		switch (opt) {
		case 'c':
			call.cmd = optarg;
			break;
		case 'i':
			init = 1;
			break;
		case 'r':
		{
			char *p = xstrdup(optarg);
			char *c = strchr(p, '=');

			if (!c)
				usage(progname);

			*c++ = '\0';
			bconfd_add_request_param(&call, p, c);
			free(p);
			break;
		}
		case 'p':
		{
			char *c = optarg;
			switch (*c) {
			case 't':
				call.proto = PROTO_TRANS;
				break;
			case 'j':
				call.proto = PROTO_JSON;
				break;
			default:
				/* invalid */
				call.proto = 5;
			}
			break;
		}
		default:
			usage(progname);
		}
	}

	argc -= optind;
	argv += optind;

	if (argc != 1)
		usage(progname);

	config_file = argv[0];

	if (init) {
		root = config_init(config_file);
		r = bconfd_load(&call, root);
	} else {
		r = bconfd_load_with_file(&call, config_file);
	}

	if (r) {
		printf("error [%s]\n", call.errstr);
		return 0;
	}

	const char *hd = call.checksum;

	printf("%s\n", bconf_get_string_default(call.bconf, "*.*.uncommon.named_path.1.name", "star.star"));
	printf("%s\n", bconf_get_string_default(call.bconf, "*.uncommon.named_path.1.name", "host"));
	printf("%s\n", bconf_get_string_default(call.bconf, "uncommon.named_path.1.name", "host.appl"));
	printf("%s", hd ?: "checksum failed");

	bconf_free(&call.bconf);
	bconf_free(&root);
	return 0;
}

