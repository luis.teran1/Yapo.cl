#include "transapi.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/wait.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include "bconf.h"
#include "vtree.h"
#include "logging.h"
#include "util.h"

#define TRANSACTION_BANNER_OK "220 Welcome.\n"
#define TRANSACTION_BANNER_BUSY "521 Busy.\n"

static void
token_handler_cb(transaction_t *trans, const char *key, size_t klen, const char *val, size_t vlen, void *cbdata) {
	printf("token_handler_cb: key '%.*s' (%zu), val '%.*s' (%zu)\n", (int)klen, key, klen, (int)vlen, val, vlen);
}

int main(int argc, char *argv[]) {

	transaction_t *trans;
	int res; 
	char *key;
	char *value;
	char *ret;
	int n = 0;
	int status;
	pid_t pid;
	int p[2];
	short port;
	char *port_number;
	struct bconf_node *root = NULL;
	struct bpapi_vtree_chain vtree;
	int busy = 0;

	if (argc >= 2 && strcmp(argv[1], "busy") == 0)
		busy = 1;
	
	if (pipe(p))
		xerr(1, "pipe");

	switch ((pid = fork())) {
	case -1:
		xerr(1, "fork");
	case 0: {
			/* echo server. aka fake transaction server. */
			struct sockaddr_in addr;
			char buf[1024];
			size_t sz;
			int fd;
			int one;
			int r;
			socklen_t a_len = sizeof(addr);

			close(p[0]);
			one = 1;

			if ((fd = socket(AF_INET, SOCK_STREAM, 0)) == -1)
				xerr(1, "socket");
			memset(&addr, 0, sizeof(addr));

			if (setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, &one, sizeof(one)) < 0)
				xerr(1, "setsockopt");

			addr.sin_family = AF_INET;
			addr.sin_addr.s_addr = htonl(INADDR_ANY);
			addr.sin_port = htons(0);
			if (bind(fd, (struct sockaddr *) &addr, sizeof(addr)) < 0)
				xerr(1, "bind");

			if (listen(fd, 0) < 0)
				xerr(1, "listen");

			getsockname(fd, (struct sockaddr *) &addr, &a_len);

			if (write(p[1], &addr.sin_port, sizeof(short)) < 0)
				xwarn("write");
			close(p[1]);

			if ((fd = accept(fd, NULL, NULL)) < 0)
				xerr(1, "accept");

			if (busy)
				r = write(fd, TRANSACTION_BANNER_BUSY, sizeof(TRANSACTION_BANNER_BUSY) - 1);
			else
				r = write(fd, TRANSACTION_BANNER_OK, sizeof(TRANSACTION_BANNER_OK) - 1);
			if (r < 0)
				xwarn("write");

			while ((sz = read(fd, buf, sizeof(buf))) > 0) {
				if (write(fd, buf, sz) < 0)
					xwarn("write");
			}
			exit(sz != 0);
		}
	}

	log_setup_perror("test_transapi", "debug");

	close(p[1]);
	res = read(p[0], &port, sizeof(short));
	close(p[0]);

	xasprintf(&port_number, "%u", ntohs(port));

	bconf_add_data(&root, "myhost.common.transaction.host.1.name", "127.0.0.1");
	bconf_add_data(&root, "myhost.common.transaction.host.1.port", "56");
	bconf_add_data(&root, "myhost.common.transaction.host.2.name", "127.0.0.1");
	bconf_add_data(&root, "myhost.common.transaction.host.2.port", port_number);
	free(port_number);

	trans = trans_init_vtree(1000, bconf_vtree(&vtree, bconf_get(root, "myhost.common.transaction")));

	trans_add_pair(trans, "foo", "bar", strlen("bar"));
	trans_add_pair(trans, "baz", "17", strlen("17"));
	trans_add_pair(trans, "blob:10:blobtest", "\n234\n6789\n", 10);
	trans_add_pair(trans, "token", "main", strlen("main"));
	trans_add_pair(trans, "blob:8:cbtest",   "token:cb",  -1);

	/* To verify that the callback is only invoked for token:main and not the token embedded in the cbtest blob. */
	trans_add_callback(trans, "token", token_handler_cb);

	if ((res = trans_commit(trans)) != TRANS_COMMIT_OK) {
		printf("trans_commit: %s\n", trans_strerror(res));
		trans_free(trans);
		bconf_free(&root);
		return 1;
	}
	vtree_free(&vtree);
	printf("trans_commit: OK\n");

	while (trans_iter_next(trans, &key, &value)) {
		n++;
	}

	if (n != 5) {
		printf("trans_iter_next: failed %d\n", n);
		trans_free(trans);
		kill(pid, 9);
		return 1;
	}

	printf("trans_iter_next: OK\n");

	if ((ret = trans_get_key(trans, "blahonga")) != NULL) {
		printf("trans_get_key: failed, found bad key");
		free(ret);
		kill(pid, 9);
		return 1;
	}

	printf("trans_get_key(blahonga): OK\n");


	if ((ret = trans_get_key(trans, "baz")) != NULL ) {
		if (strcmp(ret, "17") == 0)
			printf("trans_get_key(baz): OK\n");
		else {
			printf("trans_get_key: failed, bad response\n");
			kill(pid, 9);
			return 1;
		}
		free(ret);
	}
	else {
		printf("trans_get_key: failed, no response\n");
	}

	if ((ret = trans_get_key(trans, "blobtest")) != NULL ) {
		if (strcmp(ret, "\n234\n6789\n") == 0)
			printf("trans_get_key(blobtest): OK\n");
		else {
			printf("trans_get_key: failed, bad response\n");
			kill(pid, 9);
			return 1;
		}
		free(ret);
	}
	else {
		printf("trans_get_key: failed, no response\n");
	}


	trans_free(trans);
	bconf_free(&root);

	if (wait(&status) < 0)
		return 1;

	return (WIFEXITED(status) ? WEXITSTATUS(status) : 1);
}

