#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "util.h"
#include "aes.h"

static int test_sign(const char *message, int pad, int tofail) {
	const char *aeskey = "WyR9saCf6tI2Jbr0mAcgKw==";
	const char *sign_key = "dmVyeXNlY3JldGtleQ==";
	const char *nonce = NULL;

	int ret = 0;
	char* enc = aes_encode_sign(message, -1, pad, nonce, aeskey, -1, sign_key);

	/* Crude error injection */
	if (tofail) {
		if (enc[tofail-1] == 'a')
			enc[tofail-1] = 'b';
		else
			enc[tofail-1] = 'a';
	}

	int verified = -1;
	char* dec = aes_decode_signed(enc, -1, aeskey, -1, NULL, sign_key, &verified);

	if (verified) {
		if (strcmp(message, dec) != 0) {
			printf("Decoded message mismatch!\n");
			ret = 1;
		}
	} else {
		if (!tofail)
			printf("Signature check failed.\n");
		ret = 2;
	}

	if (ret != 0 && !tofail) {
		printf("Enc: '%s'\n", enc);
		printf("Dec: '%s'\n", dec);
	}

	free(enc);
	free(dec);
	return ret;
}

int main(int argc, char *argv[])
{
	time_t current_time = time(NULL);

	srandom(current_time);

	/* Generate a 'random' message for testing */
	char *message;
	xasprintf(&message, "Test message at %s", ctime(&current_time));
	message[strlen(message)-1] = '\0';

	printf("In : '%s'\n", message);

	int ret = 0;
	int i;

	/* Verify signature over this message with different padding options */
	for (i=0 ; i < 4 ; ++i) {
		ret |= test_sign(message, i, 0);
	}
	if (ret) {
		printf("VERIFICATION TEST FAILED: %d.\n", ret);
		goto exit;
	}

	/* Verify that injection of bit-errors are detected */
	for (i=0 ; i < (int)strlen(message) ; ++i) {
		ret = test_sign(message, 0, i+1);
		if (ret != 2) {
			printf("UNEXPECTED VERIFICATION NON-FAIL: %d.\n", ret);
			ret = 10;
			goto exit;
		}
	}

	ret = EXIT_SUCCESS;

exit:
	free(message);

	return ret;
}

