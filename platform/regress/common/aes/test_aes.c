
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

#include "aes.h"
#include "base64.h"
#include "util.h"

int
main(int argc, char *argv[]) {
	const char *key;
	int klen;
	int enc;
	char line[1024];
	const int bklen = 128 / 8;
	char binkey[bklen];
	char b64key[BASE64_NEEDED(bklen)];

	if (argc == 2) {
		key = argv[1];
		klen = strlen(argv[1]);
		enc = 0;
	} else {
		int fd;

		fd = open("/dev/urandom", O_RDONLY);
		if (fd < 0)
			xerr(1, "open");

		if (read(fd, binkey, bklen) < bklen)
			xerr(1, "read");

		close(fd);

		klen = base64_encode(b64key, binkey, bklen);
		key = b64key;

		printf("key: %s\n", b64key);
		enc = 1;
	}

	while (fgets(line, sizeof(line), stdin)) {
		int l = strlen(line);
		char *res;

		if (l > 0 && line[l - 1] == '\n')
			line[--l] = '\0';

		if (enc)
			res = aes_encode(line, l + 1, 3, NULL, key, klen); /* Include \0 byte. */
		else
			res = aes_decode(line, l, key, klen, NULL);

		if (!res)
			xerr(1, "aes");
		printf ("%s\n", res);
		free(res);
	}
	return 0;
}
