#include <stdio.h>
#include "rand.h"

int
main(int argc, char **argv) {
	int sketchy_success_criteria = 0;
	rand_t *rand;

	rand = rand_open();
	for (int i = 0; i < 1000000; i++) {
		if (rand_uint16(rand) > 256)
			sketchy_success_criteria++;
	}
	rand_close(rand);

	if (sketchy_success_criteria == 0) {
		fprintf(stderr, "Error: rand_uint16() didn't return values gte 256\n");
		return 1;
	}

	return 0;
}
