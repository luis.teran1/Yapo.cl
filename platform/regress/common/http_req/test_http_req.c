
#include "event.h"

#include "http_req.h"
#include "error_functions.h"
#include "memalloc_functions.h"

#include <fcntl.h>

int done = 0;
int fail = 0;

static void
cb(struct request *req) {
	int expect = fail ? RESULT_BADCONNECT : 200;
	if (req->result != expect)
		xerrx(1, "Unexpected result %d != %d", req->result, expect);
	done = 1;
}

int
main(int argc, char *argv[]) {
	struct request *req = zmalloc(sizeof(*req));
	struct event_base *base = event_init();

	if (argc >= 3 && strcmp(argv[2], "fail") == 0)
		fail = 1;

	req->event_base = base;
	req->server = "localhost";
	req->port = argv[1];
	req->timeout = 10;
	req->cb_done = cb;

	req->header = xstrdup("GET / HTTP/1.0\r\nHost: localhost\r\n\r\n");
	req->hsize = strlen(req->header);

	if (http_req(req) && !fail)
		return 1;

	if (fail) {
		if (fcntl(0, F_GETFL) == -1) {
			perror("fcntl");
			return 1;
		}
		return 0;
	}

	event_dispatch();
	return !done;
}

