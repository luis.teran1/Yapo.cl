#include <stdio.h>
#include <assert.h>
#include <unistd.h>

#include "bconf.h"
#include "bconfig.h"
#include "vtree.h"
#include "cacheapi.h"
static int
test_cacheapi(struct bconf_node *config) {

	struct bpapi_vtree_chain vtc = { 0 };
	struct cache_pools session;

	bconf_vtree(&vtc, config);

	int res = cache_init_pools(&session, &vtc);
	printf("cache_init_pools -> %d\n", res);

	res = cache_set(&session, "sess1x", "key", "some value", 3600);
	printf("cache_set -> %d\n", res);

	int slp = bconf_get_int_default(config, "sleep", 0);
	sleep(slp);

	char *val = cache_get(&session, "sess1x", "key");
	printf("cache_get -> %s\n", val);
	free(val);

	cache_free_pools(&session);

	return 0;
}

int
main(int argc, char *argv[]) {
	char *config_file = argv[1];

	assert(config_file);

	struct bconf_node *config = config_init(config_file);

	test_cacheapi(config);

	bconf_free(&config);

	return 0;
}

