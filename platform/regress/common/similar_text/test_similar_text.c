#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "similar_text.h"

static int
chk_sim(int expected, const char *s1, const char *s2, int is_utf8) {

	int d = similar_text(s1, s2, is_utf8);
	if (d != expected) {
		fprintf(stderr, "Got d=%d, expected %d for\ns1=%s\ns2=%s\n", d, expected, s1, s2);
		return 1;
	}

	return 0;
}

int
main(int argc, char **argv) {
	int fail = 0;

	const char *cicero = "But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses to enjoy a pleasure that has no annoying consequences, or one who avoids a pain that produces no resultant pleasure?";
	const char *cicero_edit = "But I must explain to you how ALL this mistaken idea of denouncing pain and praising pleasure was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses to enjoy a pleasure that has NO consequences, or one who avoids a pain that produces no resultant pleasure?";

	fail += chk_sim(100, "test", "test", 0);
	fail += chk_sim(0, "test", "", 0);
	fail += chk_sim(0, "test", NULL, 0);
	fail += chk_sim(0, "", NULL, 0);
	fail += chk_sim(100, "åäö", "åäö", 1);
	fail += chk_sim(75, "åäö", " åäö ", 1);

	fail += chk_sim(98, cicero, cicero_edit, 0);

	return fail;
}
