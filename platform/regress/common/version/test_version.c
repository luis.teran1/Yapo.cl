/*
	Demonstrates how to use the platform version macros
*/
#include <stdio.h>

#include "platform_version.h"

int
main(int argc, char* argv[]) {
	printf("platform version is %d.%d\n", PLATFORM_MAJOR, PLATFORM_MINOR);

#if PLATFORM_PREREQ(2, 2, 0)
	printf("Platform version requisite fulfilled.\n");
#else
#	error Platform 2.2.0 requisite unfulfilled.
#endif

	return 0;
}
