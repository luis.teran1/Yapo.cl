#include <stdio.h>
#include "ctemplates.h"
#include "bconf.h"

struct _foo {
	struct bpapi_simplevars_chain *schain;
	char *buf;
	int len;
	int pos;
	const char *foo;
};

static int
foo_outstring_raw(struct bpapi_output_chain *ochain, const char *str, size_t len) {
	return bpo_outstring_raw(ochain->next, str, len);
}

static int
init_foobar(const struct bpfilter *filter, struct bpapi *api, int argc, const struct tpvar **argv) {
	struct _foo *foo = BPAPI_OUTPUT_DATA(api);
	
	foo->foo = filter_state(api, "foo", NULL, NULL);
	foo->schain = &api->schain;
	if (foo->foo) {
		if (bps_has_insert(foo->schain))
			bps_insert(foo->schain, "foo", foo->foo);
	}

	return 0;

}

struct bpapi_output foobar_output = {
        self_outstring_fmt,
        foo_outstring_raw,
        NULL,
        NULL
};
ADD_OUTPUT_FILTER(foobar, sizeof(struct _foo));

static void
local(struct bconf_node *root) {
	struct bconf_node *filter_conf = NULL;
	struct shadow_vtree shadow_tree = {};
	struct bpapi_vtree_chain temp_vtree = {0};
	struct bpapi ba = {};

	filter_state_bconf_init_all(&filter_conf);
	filter_state_bconf_set(&filter_conf, "foo", (void *)"_something", NULL, FS_SESSION);

	stdout_output_init(&ba.ochain);
	hash_simplevars_init(&ba.schain);
	ba.filter_state = filter_state_bconf;
	
	bconf_vtree(&shadow_tree.vtree, root);
	bconf_vtree(&temp_vtree, filter_conf);
	shadow_vtree_init(&ba.vchain, &shadow_tree, &temp_vtree);

	bpapi_insert(&ba, "some_var", "I really can not think of names");

	const char *kv = "some_keySome_valueX";
	bpapi_insert_buf(&ba, kv, 8, kv+8, 10); /* insert 'some_key' -> 'Some_value' */

	if (!call_template(&ba, "test/print_all.txt"))
		xerrx(1, "call_template(\"test/print_all.txt\") failed");

	bpapi_free(&ba);

	filter_state_bconf_free(&filter_conf);
}

int
main(int argc, char **argv) {
	struct bconf_node *root = NULL;

	filter_state_bconf_init_all(&root);
	default_filter_state = filter_state_bconf;

	bconf_add_data(&root, "somenode.whatnot1", "foo");
	bconf_add_data(&root, "somenode.whatnot2", "foo");
	bconf_add_data(&root, "somenode.whatever.1", "foo");
	bconf_add_data(&root, "somenode.whatever.2.1", "foo");
	bconf_add_data(&root, "somenode.whatever.2.2", "foo");
	bconf_add_data(&root, "somenode.whatever.2.3", "foo");
	bconf_add_data(&root, "somenode.whatever.2.4", "foo");
	bconf_add_datav(&root, 3, (const char *[]){ "someother", "node.that.i", "cannot" }, "thinkoff", BCONF_REF);
	
	local(root);

	filter_state_bconf_free(&root);

	return 0;
}
