
#include "charmap.h"

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int
main(int argc, char *argv[]) {
	struct charmap *cm;
	char buf1[16];
	char buf2[16];
	char buf3[16];

	cm = init_charmap(argv[1]);

	void *td = init_charmap_thread(cm);

	char a[] = "åäö";
	char b[] = "ÅÄÖ";
	char c[] = "åäö";

	printf ("%s != %s != %s\n", a, b, c);
	printf ("%s == %s == %s\n", casefold(buf1, sizeof(buf1), a, td, 0),
			casefold(buf2, sizeof(buf2), b, td, 0), casefold(buf3, sizeof(buf3), c, td, 0));

	char d[] = "ⓐⓑⓒ";
	char e[] = "abc";

	printf ("%s != %s\n", d, e);
	printf ("%s == %s\n", casefold(buf1, sizeof(buf1), d, td, 0), casefold(buf2, sizeof(buf2), e, td, 0));

	char f[] = "⒈⒉⒊";
	char g[] = "1.2.3.";

	printf ("%s != %s\n", f, g);
	printf ("%s == %s\n", casefold(buf1, sizeof(buf1), f, td, 0), casefold(buf2, sizeof(buf2), g, td, 0));

	const char *ptr;
	if ((ptr = strstr(argv[1], "greek-utf8")) && ptr[sizeof("greek-utf8") - 1] == '\0') {
		char h[] = "\xce\xb9\xcc\x88\xcc\x81"; /* ι ̈ ́ something adds spaces if typed out. */
		char j[] = "ΐ";

		printf ("%s != %s\n", h, j);
		printf ("%s == %s\n", casefold(buf1, sizeof(buf1), h, td, 0), casefold(buf2, sizeof(buf2), j, td, 0));
	}

	free_charmap_thread(td);
	free_charmap(cm);
	return 0;
}
