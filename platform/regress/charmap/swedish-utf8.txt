# This is the Swedish UTF-8 charmap source.
# This file is divided in sections based on those from Unicode NamesList.txt
# Switch section with @name or @number where name is a unique substring of one
# of the section names, or number is a hex number within its range.
# Each section is on the same form as the normal charmap.
# This is run after normalization and case folding, so you do not have to do
# those here. But of course the replacement character has to be in the
# lowercase NFKC set.
# As in the single byte version, basic latin and latin-1 supplement has
# English defaults.
@ Basic Latin
@ Latin-1 Supplement 
Ää
Åå
Æä
Öö
Øö
ää
åå
æä
öö
øö
