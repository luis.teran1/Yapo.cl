#!/bin/bash

# If canceled we need to kill the snooper
trap "kill 0" EXIT

test -z "${TOPDIR}" && TOPDIR=../..
test -z "${PLATFORMDIR}" && PLATFORMDIR=../..

source ${PLATFORMDIR}/scripts/build/regressvars.sh

exec 3< <(${DEST_BIN}/index_snooper -v $SNOOPER_FLAGS http://${REGRESS_HOSTIP}:${REGRESS_INDEXER_CONTROLLER_PORT}/register_consumer)

set -e

read <&3

make --quiet --no-print-directory "$@"

cat <&3
