BEGIN;
INSERT INTO action_states (ad_id, state_id, state) SELECT ad_id, state_id, 'activate' FROM fake_activate_five;
UPDATE ads SET status = 'active' WHERE ad_id IN (select ad_id FROM fake_activate_five);
DROP TABLE fake_activate_five;
COMMIT;
