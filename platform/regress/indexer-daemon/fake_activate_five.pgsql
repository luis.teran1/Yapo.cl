BEGIN;
DROP TABLE IF EXISTS fake_activate_five;
SELECT ad_id, NEXTVAL('action_states_state_id_seq') AS state_id INTO TABLE fake_activate_five FROM ads WHERE status != 'active' ORDER BY ad_id LIMIT 5;
SELECT pgq.insert_event('action_states_feed', 'ads', CAST(ad_id as text), CAST(state_id as text), 'activate', NULL, NULL) FROM fake_activate_five;
COMMIT;
