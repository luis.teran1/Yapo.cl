CREATE TYPE enum_ads_status AS ENUM ('inactive', 'active', 'deleted');

CREATE TABLE ads (
	ad_id bigserial PRIMARY KEY,
	list_id bigint,
	status enum_ads_status NOT NULL default 'inactive',
	category integer NOT NULL default 0,
	lang varchar NOT NULL default 'en',
	subject varchar NOT NULL,
	body text NOT NULL
);

CREATE UNIQUE INDEX index_ads_list_id ON ads (list_id);
CREATE INDEX index_ads_status ON ads (status);

CREATE TYPE enum_action_states_state AS ENUM ('activate', 'deactivate', 'fiddle');

CREATE TABLE action_states (
	state_id bigserial PRIMARY KEY,
	ad_id bigserial,
	state enum_action_states_state NOT NULL,
	FOREIGN KEY(ad_id) REFERENCES ads (ad_id)
);

CREATE INDEX index_action_states_ad_id ON action_states (ad_id);
CREATE INDEX index_action_states_state ON action_states (state);

CREATE EXTENSION pgq;
CREATE EXTENSION pgq_node;
CREATE EXTENSION pgq_ext;
CREATE EXTENSION pgq_coop;

SELECT pgq.create_queue('action_states_feed');
SELECT pgq.register_consumer('action_states_feed', 'indexer_as_pgq');

SELECT pgq.set_queue_config('action_states_feed', 'ticker_idle_period', '00:00:01');
SELECT pgq.set_queue_config('action_states_feed', 'ticker_max_lag', '00:00:01');

CREATE FUNCTION action_states_pgq_register() RETURNS trigger AS $$
BEGIN
	PERFORM pgq.insert_event('action_states_feed', 'ads', CAST(NEW.ad_id as text), CAST(NEW.state_id as text), CAST(NEW.state as text), NULL, NULL);
	RETURN NEW;
END;
$$ LANGUAGE plpgsql;
CREATE TRIGGER action_states_pgq_trig
AFTER INSERT OR UPDATE ON action_states
FOR EACH ROW
WHEN (NEW.state IN ('activate', 'deactivate'))
EXECUTE PROCEDURE action_states_pgq_register();

