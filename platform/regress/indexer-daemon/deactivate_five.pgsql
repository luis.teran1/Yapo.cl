BEGIN;
SELECT ad_id INTO TEMP TABLE my_ad_ids FROM ads WHERE status = 'active' ORDER BY ad_id LIMIT 5;
INSERT INTO action_states (ad_id, state) SELECT ad_id, 'deactivate' FROM my_ad_ids;
UPDATE ads SET status='inactive' WHERE ad_id IN (SELECT ad_id FROM my_ad_ids);
COMMIT;
