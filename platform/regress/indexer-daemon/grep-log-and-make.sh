#!/bin/bash

trap "kill 0" EXIT

num=1
if [ "$1" = "-n" ]; then
	num="$2"
	shift 2
fi

test -z "${TOPDIR}" && TOPDIR=../..
test -z "${PLATFORMDIR}" && PLATFORMDIR=../..

source ${PLATFORMDIR}/scripts/build/regressvars.sh

exec 3< <(tail -F -n 0 ${SYSLOGROOT}/indexer.log | grep -E -m "$num" "$1")
shift

set -e

make --quiet --no-print-directory "$@"

cut -d ' ' -f 3- <&3
