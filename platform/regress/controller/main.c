#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include <err.h>
#include <logging.h>
#include <inttypes.h>
#include <semaphore.h>
#include <unistd.h>

#include "ctemplates.h"
#include "bconf.h"
#include "bconfig.h"
#include "controller.h"
#include "sha2.h"
#include "stat_counters.h"
#include "daemon.h"

static void
hej_cb(struct ctrl_req *cr, struct bpapi *ba, void *v) {
	bpapi_outstring_fmt(ba, "hej called\n");
}

static void
hej_hopp_cb(struct ctrl_req *cr, struct bpapi *ba, void *v) {
	bpapi_outstring_fmt(ba, "hej/hopp called\n");
}

static int
echo_data(struct ctrl_req *cr, struct bpapi *ba, size_t tot_len, const char *data, size_t len, void *v) {
	bpapi_outstring_fmt(ba, "echo: (%lu) [%.*s]\n", tot_len, (int)len, data);
	return 0;
}

static void
echo_cb(struct ctrl_req *cr, struct bpapi *ba, void *v) {
	bpapi_outstring_fmt(ba, "echo done\n");
}

static int
discard_data(struct ctrl_req *cr, struct bpapi *ba, size_t tot_len, const char *data, size_t len, void *v) {
	/* Ignore everything. */
	return 0;
}

static void
discard_cb(struct ctrl_req *cr, struct bpapi *ba, void *v) {
	bpapi_outstring_fmt(ba, "discaring done\n");
}

struct sha256_cbdata {
	SHA2_CTX ctx;
};

STAT_COUNTER_DECLARE(stat_sha256_data, "counts", "sha256", "data");

static void
sha256_start(struct ctrl_req *cr, void *v) {
	struct sha256_cbdata *sd = calloc(1, sizeof(*sd));
	ctrl_set_handler_data(cr, sd);
	SHA256Init(&sd->ctx);
}

static int
sha256_data(struct ctrl_req *cr, struct bpapi *ba, size_t tot_len, const char *data, size_t len, void *v) {
	struct sha256_cbdata *sd = v;

	SHA256Update(&sd->ctx, (const unsigned char *)data, len);

	STATCNT_ADD(&stat_sha256_data, len);	

	return 0;
}

static void
sha256_cb(struct ctrl_req *cr, struct bpapi *ba, void *v) {
	struct sha256_cbdata *sd = v;
	uint8_t digest[SHA256_DIGEST_LENGTH];
	char digest_string[sizeof(digest) * 2 + 1];
	unsigned int i;

	SHA256Final(digest, &sd->ctx);
	for (i = 0; i < sizeof(digest); i++) {
		snprintf(&digest_string[i * 2], 3, "%02x", digest[i]);
	}
	bpapi_outstring_fmt(ba, "%s (%llu bytes)\n", digest_string, (unsigned long long)ctrl_get_content_length(cr));
	if (bpapi_has_key(ba, "expect_hash")) {
		if (strcmp(bpapi_get_element(ba, "expect_hash", 0), digest_string)) {
			ctrl_error(cr, 417, "hash mismatch");
		}
	}
	free(sd);
}

static void
some_json_start(struct ctrl_req *cr, void *v) {
	struct bconf_node **bc;

	bc = ctrl_get_bconfp(cr);
	bconf_add_data(bc, "hej.hopp.tratt", "1");
	bconf_add_data(bc, "hej.hopp.kaka", "2");
	bconf_add_data(bc, "hej.hatt.tratt", "3");
}

static void
some_json_finish(struct ctrl_req *cr, struct bpapi *ba, void *v) {
	ctrl_output_json(cr, ba, "hej");
}

static sem_t stop_sem;

static void
stop_cmd(struct ctrl_req *cr, struct bpapi *ba, void *v) {
	sem_t *s = v;

	sem_post(s);
	ctrl_quit(cr);
}

static void
custom_headers(struct ctrl_req *cr, struct bpapi *ba, void *v) {
	ctrl_set_custom_headers(cr, "X-Custom", "some-value");
	ctrl_output_json(cr, ba, "min elefanter dricker din vatten");
}

static struct ctrl_handler handlers[] = {
	{
		.url = "/hej", 
		.finish = hej_cb,
	},
	{
		.url = "/hej/hopp", 
		.finish = hej_hopp_cb,
	},
	{
		.url = "/hej/tjo/<param>", 
		.finish = render_template_cb,
		.cb_data = (char *)"dump_vars"
	},
	{
		.url = "/hej/partial<param>",
		.finish = render_template_cb,
		.cb_data = (char *)"dump_vars"
	},
	{
		.url = "/foo/<first>/<second>",
		.finish = render_template_cb,
		.cb_data = (char *)"dump_vars"
	},
	{
		.url = "/middle/<param>/test",
		.finish = render_template_cb,
		.cb_data = (char *)"dump_vars"
	},
	{
		.url = "/echo",
		.finish = echo_cb,
		.consume_post = echo_data
	},
	{
		.url = "/discard",
		.finish = discard_cb,
		.consume_post = discard_data
	},
	{
		.url = "/sha256",
		.start = sha256_start,
		.finish = sha256_cb,
		.consume_post = sha256_data
	},
	{
		.url = "/stop",
		.finish = stop_cmd,
		.cb_data = &stop_sem
	},
	{
		.url = "/dump_vars",
		.finish = render_template_cb,
		.cb_data = (char *)"dump_vars"
	},
	{
		.url = "/some_json",
		.start = some_json_start,
		.finish = some_json_finish,
	},
	{
		.url = "/stats",
	},
	{
		.url = "/custom_headers",
		.finish = custom_headers,
	},
};

static struct bconf_node *root;
static int p[2];

static int
run(void) {
	pthread_t *ct;
	int nthreads;
	char b = 1;
	void *r;
	int i;

	if (sem_init(&stop_sem, 0, 0) == -1)
		err(1, "sem_init");
	
	for (i = 0; i < (int)(sizeof(handlers) / sizeof(handlers[0])); i++) {
		if (!strcmp(handlers[i].url, "/stats")) {
			handlers[i] = ctrl_stats_handler;
		}
	}

	if (ctrl_thread_setup(bconf_get(root, "httpd_listen"), root, &ct, &nthreads, handlers, sizeof(handlers) / sizeof(handlers[0]), -1) == NULL)
		errx(1, "setup_command_thread");

	if (write(p[1], &b, 1) != 1)
		err(1, "write pipe");

	sem_wait(&stop_sem);
	for (i = 0; i < nthreads; i++) {
		pthread_join(ct[i], &r);
	}

	return 0;
}

int
main(int argc, char **argv) {
	char b;

	log_setup_perror("controller", "debug");

	if (argc != 2) {
		fprintf(stderr, "Usage: %s <bconf>\n", argv[0]);
		return 1;
	}

	root = config_init(argv[1]);
	if (root == NULL)
		err(1, "config_init(%s)", argv[1]);

	if (pipe(p) == -1) {
		err(1, "pipe");
	}

	set_quick_start(1);
	daemonify(1, run);

	/* Wait for the daemon to start up. */
	if (read(p[0], &b, 1) != 1)
		err(1, "pipe read");

	return 0;
}
