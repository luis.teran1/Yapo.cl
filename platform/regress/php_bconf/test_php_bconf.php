<?php
class Obj {
	var $pricelist;

	function setval($setting, $key, $value, &$data = null) {
		if ($key == "pricelist")
			$data = $value;
	}

	function lookup($setting, $key, $data = null) {
		switch ($key) {
			case "category":
				return 3020;
			case "type":
				return "s";
			case "apartment_type":
				return "tenant_ownership";
		}
	}
}
$ob  = new Obj();

get_settings($DIFFERENT_BCONF['*']["list_settings"], "searchextras", array($ob, "lookup"), array($ob, "setval"), $ob->pricelist);

$checksum = bconf_checksum();
if (empty($checksum))
	exit(1);

if ($ob->pricelist == 3)
	exit(0);
else
	exit(1);
?>
