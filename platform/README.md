# Building platform and running regression tests #

## Natively ##

To build the platform you first need to fulfill our package
dependencies, which are listed in the files 'rpms' and 'debs'
respectively. You also need to have our scmcoord-\* contrib
packages installed, see https://scmcoord.com/wiki/Repo

Then you need to fulfill the toolchain requirements from the
UPGRADE.md, such as minimum compiler version.

We also require the locales en\_US.utf8, en\_US.iso88591, and
for a couple of locale-specific tests, es\_US.iso88591.

You build with

	$ ./compile.sh

Or build and run tests with

	$ RUNTESTS=1 ./compile.sh

Once built, you can also enter individual test directories under
'regress' and use the classics 'make rd rdo', 'make rco' (continue),
'make rc' (clean up) and so on.

	$ cd regress/trans && make rd rdo

Usually you'd test everything with RUNTESTS=1 first, and if any
fail, go into the individual regress directories and run the test
manually to figure out what's going wrong.

## Docker ##

There are Docker files in scripts/docker for creating images capable
of running the platform tests in a variety of configurations.

Complete steps for building the centos6 platform image:

```bash
$ docker build -t <imagename> scripts/docker/platform-centos6/
```

To run the platform tests in the newly built image, execute the following
command:
```bash
$ DOCKER_IMAGE=<imagename> RUNTESTS=1 ./compile.sh
```

More information is available in docs/docker.md

