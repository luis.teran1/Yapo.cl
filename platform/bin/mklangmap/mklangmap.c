#include <sys/types.h>
#include <stdio.h>
#include <stdlib.h>
#include <err.h>
#include <string.h>
#include <tchdb.h>
#include <ctype.h>

#include "langmap.h"
#include "charmap.h"

int
main(int argc, char **argv) {
	TCHDB *langmap;
	char buf[1024];
	char buf2[2048];
	char **langs;
	int nlangs;
	int nwords;
	void *cfdata;
	struct charmap *cm;

	if (argc < 5) {
		errx(1, "Usage: %s <langmap> <charmap> <lang1|lang2|...>", argv[0]);
	}

	cm = init_charmap(argv[2]);
	cfdata = init_charmap_thread(cm);

	if ((langmap = tchdbnew()) == NULL)
		errx(1, "tchdbnew(): %s", tchdberrmsg(tchdbecode(langmap)));
	
	if (!tchdbopen(langmap, argv[1], HDBOWRITER|HDBOCREAT|HDBOTRUNC))
		errx(1, "tchdbopen(%s): %s", argv[1], tchdberrmsg(tchdbecode(langmap)));

	langs = argv + 3;
	nlangs = argc - 3;

	nwords = 0;

	while (fgets(buf, sizeof(buf), stdin)) {
		char *k;
		int i;

		nwords++;

		k = buf;

		for (i = 0; i < nlangs; i++) {
			char key[64];
			char val[16];
			char *n, *n2;

			n = strpbrk(k, ",\n");
			if (!n)
				errx(1, "Too few columns on line starting with \"%s\"", buf);
			*n++ = '\0';

			snprintf(val, sizeof(val), LANGMAP_WORD_PREFIX "%d", nwords);
			do {
				char *lkey;

				n2 = strchr(k, ';');
				if (n2)
					*n2++ = '\0';
				casefold(buf2, sizeof (buf2), k, cfdata, 0);
				if ((lkey = langmap_key(key, sizeof(key), langs[i], buf2)) == NULL)
					errx(1, "langmapkey overflow %s %s", langs[i], buf2);
				if (!tchdbputkeep2(langmap, lkey, val))
					errx(1, "tchdbput(%s, %s)", lkey, val);
			} while ((k = n2));
			k = n;
		}
	}
	tchdbclose(langmap);
	tchdbdel(langmap);

	return 0;
}
