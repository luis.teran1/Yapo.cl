#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <arpa/inet.h>
#include <string.h>
#include <err.h>
#include <fcntl.h>
#include <unistd.h>
#include <signal.h>
#include <netdb.h>
#include "macros.h"

#include "macros.h"

static
void usage(const char *prog) {
		errx (1, "Usage: %s [-4|-6] [-f] addr port [ack_sock]", prog);
}

static void
transmit(int s, char *buf, struct addrinfo *res, int ack_sock) {
	if (sendto(s, buf, strlen(buf), 0, res->ai_addr, res->ai_addrlen) < 0)
		warn("sendto");
	if (ack_sock != -1) {
		char buf[4];
		alarm(4);
		/* signal(SIGALRM, sig_alarm); */
		if (read(ack_sock, buf, 4) == -1) {
			err(1, "reading ACK failed");
		}
		alarm(0);
	}
}

int 
main(int argc, char *argv[]) {
	int s;
	char buf[1024];
	int ack_sock = -1;
	struct addrinfo hints = { .ai_socktype = SOCK_DGRAM, .ai_flags = AI_ADDRCONFIG };
	struct addrinfo *res = NULL;
	int f = 0;
	int r;
	int ch;

	if (argc < 3) {
		usage(argv[0]);
	}

	while ((ch = getopt(argc, argv, "46f")) != -1) {
		switch (ch) {
		case '4':
			hints.ai_family = AF_INET;
			break;
		case '6':
			hints.ai_family = AF_INET6;
			break;
		case 'f':
			f = 1;
			break;
		case '?':
		default:
			usage(argv[0]);
			break;
		}
	}

	argc -= optind;
	argv += optind;

	if ((r = getaddrinfo(argv[0], argv[1], &hints, &res)))
		errx(1, "getaddrinfo(%s, %s): %s", argv[0], argv[1], gai_strerror(r));

	s = socket(res->ai_family, res->ai_socktype, res->ai_protocol);
	if (s < 0)
		err(1, "socket");

	/* Wait for an ack */
	if (argc == 3) {
		ack_sock = open(argv[2], O_RDONLY);
		if (ack_sock == -1)
			err(1, "open");
	}

	if (!f) {
		while (fgets(buf, sizeof(buf), stdin)) {
			transmit(s, buf, res, ack_sock);
		}
	} else {
		UNUSED_RESULT(fgets(buf, sizeof(buf), stdin));
		while (1) {
			transmit(s, buf, res, ack_sock);
		}
	}

	if (ack_sock != -1)
		close(ack_sock);

	freeaddrinfo(res);

	return 0;
}
