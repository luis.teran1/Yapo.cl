
#include "controller.h"

#include "bconf.h"
#include "bconfig.h"
#include "bpapi.h"
#include "logging.h"
#include "sock_util.h"
#include "stat_messages.h"

#include <curl/curl.h>
#include <err.h>
#include <fcntl.h>
#include <getopt.h>
#include <limits.h>
#include <netdb.h>
#include <pthread.h>
#include <stdbool.h>
#include <stdio.h>
#include <sys/socket.h>
#include <unistd.h>

const char *url;
const char *writedir;
const char *port;
int n_times = 1;
bool verbose;
bool outofsync_pending = false;
bool outofsync_active = false;

static void
usage(const char *prog) {
	errx(1, "Usage: %s [-v] [-A] [-P] [-o] [-n <n events>] [-c <bconf.conf>] [-w <dir>] [-p <port>] [<register-at-url>]", prog);
}

static int
parse_opts(int argc, char *argv[]) {
	static const struct option options[] = {
		{ "writedir", true, NULL, 'w' },
		{ "once", false, NULL, 'o' },
		{ "verbose", false, NULL, 'v' },
		{ NULL },
	};
	extern char *optarg;
	extern int optind;

	int opt;
	while ((opt = getopt_long(argc, argv, "c:w:p:ovn:PA", options, NULL)) != -1) {
		switch (opt) {
		case 'w':
			writedir = optarg;
			break;
		case 'p':
			port = optarg;
			break;
		case 'o':
			n_times = 1;
			break;
		case 'n':
			n_times = atoi(optarg);
			break;
		case 'v':
			verbose = true;
			break;
		case 'P':
			outofsync_pending = true;
			break;
		case 'A':
			outofsync_active = true;
			break;
		default:
			return -1;
		}
	}

	return optind;
}

struct snoop_data
{
	char fname[PATH_MAX];
	int fd;
};

static void
receive_index_start(struct ctrl_req *cr, void *v) {
	ctrl_set_handler_data(cr, writedir ? zmalloc(sizeof(struct snoop_data)) : NULL);
}

static int
receive_index_data(struct ctrl_req *cr, struct bpapi *ba, size_t tot_len, const char *data, size_t len, void *v) {
	if (!writedir)
		return 0;

	struct snoop_data *td = v;

	if (td->fname[0] == '\0') {
		char key[100];
		snprintf(key, sizeof(key), "%s_checksum", (const char*)ctrl_get_handler(cr)->cb_data);

		const char *checksum = bpapi_get_element(ba, key, 0);

		snprintf(td->fname, sizeof(td->fname), "%s/%s", writedir, checksum);
		td->fd = open(td->fname, O_WRONLY|O_CREAT|O_EXCL, 0666);
		if (td->fd < 0)
			err(1, "Failed to open delta");
	}

	ssize_t n = write(td->fd, data, len);
	if (n < 0)
		err(1, "write");
	if (n < (ssize_t)len)
		errx(1, "write: short write");
	return 0;
}

static void
receive_index_finish(struct ctrl_req *cr, struct bpapi *ba, void *v) {
	const char *type = ctrl_get_handler(cr)->cb_data;
	char key[100];

	snprintf(key, sizeof(key), "%s_checksum", type);
	const char *checksum = bpapi_get_element(ba, key, 0);

	if (writedir) {
		struct snoop_data *td = v;

		close(td->fd);
		free(td);
	}

	/* Make sure to do an atomic write for thread safety. */
	char output[256];
	int l = snprintf(output, sizeof(output), "%s %s\n", type, checksum);
	UNUSED_RESULT(write(STDOUT_FILENO, output, l));

	if (verbose)
		warnx("receive_finish %d", n_times);
	if (--n_times == 0)
		ctrl_quit(cr);
}

static void
just_log_finish(struct ctrl_req *cr, struct bpapi *ba, void *v) {
	const char *msg = v;
	UNUSED_RESULT(write(STDOUT_FILENO, msg, strlen(msg)));
	if (verbose)
		warnx("%s %d", msg, n_times);
	if (--n_times == 0)
		ctrl_quit(cr);
}

static void
index_checksums_finish(struct ctrl_req *cr, struct bpapi *ba, void *v) {
	struct bconf_node **bc;

	bc = ctrl_get_bconfp(cr);

	bconf_add_data(bc, "checksums.search.index.active.checksum", "");
	bconf_add_data(bc, "checksums.search.index.pending.checksum", "");
	bconf_add_data(bc, "checksums.search.index.passive_observer.active", outofsync_active ? "outofsync" : "insync");
	bconf_add_data(bc, "checksums.search.index.passive_observer.pending", outofsync_pending ? "outofsync" : "insync");

	ctrl_output_json(cr, ba, "checksums");
}


pthread_t *ctrl_threads;
int n_threads;
struct ctrl_thread *ctrl;

static void
setup_controller(struct bconf_node *root) {
	const struct ctrl_handler controllers[] = {
		ctrl_stats_handler,
		{
			.url = "/delta",
			.start = receive_index_start,
			.consume_post = receive_index_data,
			.finish = receive_index_finish,
			.cb_data = (void*)"delta"
		},
		{
			.url = "/pending",
			.start = receive_index_start,
			.consume_post = receive_index_data,
			.finish = receive_index_finish,
			.cb_data = (void*)"pending"
		},
		{
			.url = "/activate_pending",
			.finish = just_log_finish,
			.cb_data = (void *)"activate_pending\n"
		},
		{
			.url = "/search_pause",
			.finish = just_log_finish,
			.cb_data = (void *)"search_pause\n"
		},
		{
			.url = "/search_resume",
			.finish = just_log_finish,
			.cb_data = (void *)"search_resume\n"
		},
		{
			.url = "/index_checksums",
			.finish = index_checksums_finish
		},
	};

	int s;
	if (port)
		s = create_socket(NULL, port);
	else
		s = create_socket_any_port(NULL, (char**)&port);

	if (s < 0)
		errx(1, "Failed to listen on port %s", port);

	ctrl = ctrl_thread_setup(NULL, root, &ctrl_threads, &n_threads, controllers, (int)(sizeof(controllers) / sizeof(controllers[0])), s);
}

static void
register_with_daemon(const char *url) {
	curl_global_init(CURL_GLOBAL_DEFAULT);

	if (!port)
		errx(1, "Can't register at URL without port");

	char full_url[1024];
	snprintf(full_url, sizeof(full_url), "%s?port=%s", url, port);
	if (verbose)
		warnx("Registering at %s", full_url);

	CURL *ci = curl_easy_init();
	curl_easy_setopt(ci, CURLOPT_URL, full_url);
	CURLcode r = curl_easy_perform(ci);

	long code = 0;
	if (!r)
		r = curl_easy_getinfo(ci, CURLINFO_RESPONSE_CODE, &code);

	if (r)
		errx(1, "Failed to register: %s", curl_easy_strerror(r));
	if (code != 200)
		errx(1, "Failed to register: HTTP status %ld", code);

	curl_easy_cleanup(ci);
}

int
main(int argc, char *argv[]) {
	int nopt = parse_opts(argc, argv);
	struct bconf_node *root;

	if (nopt < argc)
		url = argv[nopt++];
	if (nopt < argc)
		usage(argv[0]);

	log_setup_perror("index_snooper", verbose ? "debug" : "warning");

#if 0
	root = config_init(configfile ?: "/opt/blocket/conf/bconf.conf");
	load_bconf("index_snooper", &root);
#else
	root = NULL;
#endif

	setup_controller(root);

	if (url)
		register_with_daemon(url);

	UNUSED_RESULT(write(STDOUT_FILENO, "waiting\n", sizeof("waiting\n") - 1));

	while (n_threads > 0) {
		if (pthread_join(ctrl_threads[n_threads - 1], NULL) == 0)
			n_threads--;
	}
}

