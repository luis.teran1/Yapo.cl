#include "parse_nodes.hh"
#include "util.h"

#undef HEAP_FUNCTION_PREFIX
#define HEAP_FUNCTION_PREFIX template <class PN>
HEAP_GENERATE(ParseNodeUnion<PN>::node_heap, PN, heap_link, cmp_parse_node<PN>);

template <class PN>
int32_t
ParseNodeUnion<PN>::next_document(int32_t doc) {
	int32_t d;
	ParseNode *pn;
	PN *n;
	PN *last = NULL;
	int32_t cd;

	if (__predict_false(HEAP_EMPTY(&union_nodes))) {
		SLIST_FOREACH(pn, &node_list, list_link) {
			n = (PN*)pn;
			d = n->get_next_document(doc);
			if (d != NULL_DOCOFF)
				HEAP_INSERT(node_heap, &union_nodes, n);
		}
	}
	cd = NULL_DOCOFF;
	while ((n = HEAP_FIRST(&union_nodes)) != last) {
		d = n->get_current_doc();
		if (d >= doc) {
			return d;
		}
		d = n->get_next_document(doc);

		if (__predict_false(d == NULL_DOCOFF)) {
			HEAP_REMOVE_HEAD(node_heap, &union_nodes);
			continue;
		}

		HEAP_UPDATE_HEAD(ParseNodeUnion<PN>::node_heap, &union_nodes);
		last = n;
		cd = d;
	}

	return cd;
}

template <class PN>
static float
hn_bm25f(PN *n, int32_t doc, int ndocs, float k1) {
	int32_t cd;

	if (!n)
		return 0.0;

	cd = n->get_current_doc();
	/* The current doc will be on the top of the heap so no need to recurse when the doc mismatches. */
	if (cd != doc)
		return 0.0;

	return n->get_current_bm25(ndocs, k1) +
		hn_bm25f(n->heap_link.he_left, doc, ndocs, k1) +
		hn_bm25f(n->heap_link.he_right, doc, ndocs, k1);
}

template <class PN>
float
ParseNodeUnion<PN>::get_current_bm25(int ndocs, float k1) {
	return hn_bm25f(HEAP_FIRST(&union_nodes), current_doc(), ndocs, k1);
}

template <class PN>
int
ParseNodeUnion<PN>::get_parse_string(char *buf, size_t buflen) {
	size_t tlen = 0;
	int num = 0;
	int ilen;
	ParseNode *pn;

	SLIST_FOREACH(pn, &node_list, list_link) {
		PN *n = (PN*)pn;

		ilen = n->get_parse_string(buf + tlen, buflen - tlen);

		if (ilen < 0)
			return ilen;
		tlen += ilen;
		buf[tlen++] = ',';
		if (tlen >= buflen)
			return -1;
		num++;
	}
	ilen = snprintf(buf + tlen, buflen - tlen, "%du", num);
	if (ilen < 0 || tlen + ilen >= buflen)
		return -1;
	return tlen + ilen;
}

template <class PN>
ParseNode *
ParseNodeUnion<PN>::initialize(struct result *res, bool always_union) {
	ParseNode *n;
	ParseNode **np;

	for (np = &SLIST_FIRST(&node_list) ; (n = *np) ; ) {
		ParseNode *nn = n->initialize(res);

		if (nn) {
			if (typeid(*nn) == typeid(ParseNodeEmpty)) {
				*np = SLIST_NEXT(n, list_link);
				delete n;
				delete nn;
				continue;
			} else {
				nn->list_link = n->list_link;
				delete n;
				*np = n = nn;
			}
		}
		np = &SLIST_NEXT(n, list_link);
	}

	n = SLIST_FIRST(&node_list);
	if (!always_union && !n) {
		return new ParseNodeEmpty();
	}

	if (!always_union && !SLIST_NEXT(n, list_link)) {
		SLIST_REMOVE_HEAD(&node_list, list_link);
		return n;
	}

	return NULL;
}

template <class PN>
void
ParseNodeUnion<PN>::get_headers(struct buf_string *buf) {
	ParseNode *pn;

	SLIST_FOREACH(pn, &node_list, list_link) {
		PN *n = (PN*)pn;

		n->get_headers(buf);
	}
}

template <class PN> 
void
ParseNodeUnion<PN>::foreach_head_node2(PN *cur, int32_t doc, void (*cb)(void *, void *), void *cbdata) {
	(*cb)(cur, cbdata);
	if (cur->heap_link.he_left && cur->heap_link.he_left->get_current_doc() == doc)
		foreach_head_node2(cur->heap_link.he_left, doc, cb, cbdata);
	if (cur->heap_link.he_right && cur->heap_link.he_right->get_current_doc() == doc)
		foreach_head_node2(cur->heap_link.he_right, doc, cb, cbdata);
}

/* The callback argument should be PN *, but c++ is broken. */
template <class PN> 
void
ParseNodeUnion<PN>::foreach_head_node(void (*cb)(void *, void *), void *cbdata) {
	if (HEAP_FIRST(&union_nodes))
		foreach_head_node2(HEAP_FIRST(&union_nodes), current_doc(), cb, cbdata);
}

static ParseNodeUnion<> generate_parse_node_union UNUSED;
static ParseNodeUnion<ParseNodeInvword> generate_parse_node_union_invword UNUSED;
static ParseNodeUnion<ParseNodeInvattr> generate_parse_node_union_invattr UNUSED;

static ParseNode *(ParseNodeUnion<>::*generate_parse_node_union_initialize)(result *, bool) USED = &ParseNodeUnion<>::initialize;
static ParseNode *(ParseNodeUnion<ParseNodeInvword>::*generate_parse_node_union_invword_initialize)(result *, bool) USED = &ParseNodeUnion<ParseNodeInvword>::initialize;
static ParseNode *(ParseNodeUnion<ParseNodeInvattr>::*generate_parse_node_union_invattr_initialize)(result *, bool) USED = &ParseNodeUnion<ParseNodeInvattr>::initialize;

//static void (ParseNodeUnion<ParseNodeInvattr>::*generate_parse_node_union_invattr_foreach2)(void *, const struct doc *, void (*)(void *, void *), void *) USED = &ParseNodeUnion<ParseNodeInvattr>::foreach_head_node2;
static void (ParseNodeUnion<ParseNodeInvattr>::*generate_parse_node_union_invattr_foreach)(void (*)(void *, void *), void *) USED = &ParseNodeUnion<ParseNodeInvattr>::foreach_head_node;
