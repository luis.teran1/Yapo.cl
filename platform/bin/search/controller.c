#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <assert.h>
#include <fcntl.h>
#include <unistd.h>
#include <signal.h>

#include <logging.h>
#include <bconf.h>
#include <stat_counters.h>
#include <stat_messages.h>
#include <controller.h>
#include <index_merge.h>
#include <bpapi.h>

#include "search.h"
#include "command.h"
#include "engine.h"
#include "db.h"

/* Assume only one index_merge goes on at the same time, or else. XXX - we need locking for this. */
STAT_COUNTER_DECLARE(cnt_im_data, "index_merge", "data");

struct im_data {
	char *index_buf;
	size_t index_buf_len;
	size_t index_buf_pos;
	struct search_engine *se;
	struct index_merge_state im;
};

static void
index_merge_start(struct ctrl_req *cr, void *v) {
	struct search_engine *se = v;
	struct im_data *im = zmalloc(sizeof(*im));

	log_printf(LOG_DEBUG, "delta receive started");

	im->se = se;
	ctrl_set_handler_data(cr, im);
	/* XXX - we probably want to spawn a thread that starts to open the previous and target indexes here. */
}

static int
index_merge_data(struct ctrl_req *cr, struct bpapi *ba, size_t tot_len, const char *data, size_t len, void *v) {
	struct im_data *id = v;

	if (id->index_buf_len == 0) {
		id->index_buf = malloc(tot_len);
		if (id->index_buf == NULL) {
			ctrl_error(cr, 503, "out of memory");
			return 1;		/* This is a critical error, bail early. */
		}
		id->index_buf_len = tot_len;
	}

	assert((len <= id->index_buf_len) && (id->index_buf_pos <= (id->index_buf_len - len)));

	memcpy(&id->index_buf[id->index_buf_pos], data, len);
	id->index_buf_pos += len;

	STATCNT_ADD(&cnt_im_data, len);
	return 0;
}

static void
index_merge_finish(struct ctrl_req *cr, struct bpapi *ba, void *v) {
	struct im_data *id = v;
	char destname[PATH_MAX];
	struct timer_instance *base_ti, *ti;
	const char *delta_checksum = bpapi_get_element(ba, "delta_checksum", 0);
	const char *pending_checksum = bpapi_get_element(ba, "pending_checksum", 0);

	base_ti = timer_start(NULL, "index_merge");
	ti = timer_start(base_ti, "setup");

	STATCNT_RESET(&cnt_im_data);

	if (delta_checksum == NULL || pending_checksum == NULL) {
		ctrl_error(cr, 400, "delta and pending checksums required");
		goto out;
	}

	if (id->index_buf == NULL || id->index_buf_pos == 0) {
		ctrl_error(cr, 400, "empty post");
		goto out;
	}

	/* All received indexes are merged into the new pending index. */
	snprintf(destname, sizeof(destname), "%s.new", id->se->pending.fname);

	id->im.update = 1;
	id->im.nfrom = 2;

	timer_handover(ti, "mem_reader");

	log_printf(LOG_DEBUG, "delta merge lock");
	pthread_rwlock_rdlock(&id->se->index_lock);
	id->im.from_db[0] = id->se->pending.index ?: id->se->active.index;

	if (strcmp(pending_checksum, index_get_checksum_string(id->im.from_db[0]))) {
		ctrl_error(cr, 409, "pending checksum mismatch");
		goto outl;
	}

	if ((id->im.from_db[1] = open_mem_index_reader(id->index_buf, id->index_buf_pos, 0)) == NULL) {
		ctrl_error(cr, 503, "failed to load delta");
		goto outl;
	}

	if (strcmp(delta_checksum, index_get_checksum_string(id->im.from_db[1]))) {
		ctrl_error(cr, 500, "delta checksum mismatch");
		goto outl;
	}
	
	timer_handover(ti, "open_dest");

	if ((id->im.to = open_index(destname, INDEX_WRITER|INDEX_FILE)) == NULL) {
		ctrl_error(cr, 503, "writer open_index(%s)", destname);
		goto outl;
	}

	log_printf(LOG_DEBUG, "delta merge start");
	if (index_perform_merge(&id->im, ti) == -1) {
		ctrl_error(cr, 503, "index_perform_merge failed");
		close_index(id->im.to, 0);
		goto outl;
	}

	timer_handover(ti, "downgrade");
	log_printf(LOG_DEBUG, "delta merge done");
	if (index_writer_downgrade_to_reader(id->im.to)) {
		ctrl_error(cr, 503, "index downgrade failed");
		close_index(id->im.to, 0);
		goto outl;
	}

	if (rename(destname, id->se->pending.fname)) {
		char errbuf[256];

		UNUSED_RESULT(strerror_r(errno, errbuf, sizeof(errbuf)));
		ctrl_error(cr, 503, "rename %s -> %s failed: [%d] %s", destname, id->se->pending.fname, errno, errbuf);
		close_index(id->im.to, 0);
		goto outl;
	}

	timer_handover(ti, "lock_stuff");

	pthread_rwlock_unlock(&id->se->index_lock);

	log_printf(LOG_DEBUG, "delta merge relock");

	pthread_rwlock_wrlock(&id->se->index_lock);

	timer_handover(ti, "checksum");

	/*
	 * Now that we're ready to set this index as a new pending and
	 * hold the write lock double check that no one has replaced
	 * the pending index under our feet. This is somewhere between
	 * catastrophic and bad because it means that the linearity of
	 * data in the indexes has been violated.
	 *
	 * Note that it's perfectly legal for the pending index to
	 * become the new active index while we weren't holding the
	 * lock.
	 *
	 * Also note that the pointer in id->im.from_db[0] is no
	 * longer valid.
	 */
	if (strcmp(pending_checksum, index_get_checksum_string(id->se->pending.index ?: id->se->active.index))) {
		ctrl_error(cr, 503, "pending index modified while merging");
		goto outl;
	}

	timer_handover(ti, "set_index");

	search_engine_index_set_index(&id->se->pending, id->im.to, 1, ti);

	log_printf(LOG_DEBUG, "delta merge pending set");

	timer_handover(ti, "output");

	bpapi_outstring_fmt(ba, "new index: %s", destname);

outl:
	pthread_rwlock_unlock(&id->se->index_lock);

out:
	timer_handover(ti, "close_index");
	if (id->im.from_db[1])
		close_index(id->im.from_db[1], 0);

	timer_handover(ti, "free");
	free(id->index_buf);
	free(id);
	timer_end(ti, NULL);
	timer_end(base_ti, NULL);
}

static void
get_meta_finish(struct ctrl_req *cr, struct bpapi *ba, void *v) {
	struct bconf_node **bc;
	struct search_engine *se = v;
	const char *index = bpapi_get_element(ba, "index", 0);

	if (!index || (strcmp(index, "active") && strcmp(index, "pending"))) {
		ctrl_error(cr, 404, "unknown index");
		return;
	}

	bc = ctrl_get_bconfp(cr);

	pthread_rwlock_rdlock(&se->index_lock);
	if (strcmp(index, "active") == 0) {
		if (!se->active.index)
			ctrl_error(cr, 503, "active index missing");
		bconf_merge_prefix(bc, "meta", se->active.index->meta);
	} else if (strcmp(index, "pending") == 0) {
		if (!se->pending.index)
			ctrl_error(cr, 503, "pending index missing");
		bconf_merge_prefix(bc, "meta", se->pending.index->meta);
	}
	pthread_rwlock_unlock(&se->index_lock);

	ctrl_output_json(cr, ba, "meta");
}

static void
get_index_finish(struct ctrl_req *cr, struct bpapi *ba, void *v) {
	struct search_engine *se = v;
	const char *index = bpapi_get_element(ba, "index", 0);
	struct index_db *idx;

	pthread_rwlock_rdlock(&se->index_lock);

	if (index && !strcmp(index, "active")) {
		idx = se->active.index;
	} else if (index && !strcmp(index, "pending")) {
		idx = se->pending.index;
	} else {
		ctrl_error(cr, 404, "unknown index");
		return;
	}

	ctrl_set_raw_response_data(cr, idx->db_blob, idx->db_blob_size);
}

static void
index_checksums_finish(struct ctrl_req *cr, struct bpapi *ba, void *v) {
	struct bconf_node **bc;
	struct search_engine *se = v;

	bc = ctrl_get_bconfp(cr);

	bconf_add_data(bc, "checksums.search.index.active.checksum", se->active.index ? index_get_checksum_string(se->active.index) : "");
	bconf_add_data(bc, "checksums.search.index.pending.checksum", se->pending.index ? index_get_checksum_string(se->pending.index) : "");

	bconf_add_data(bc, "checksums.search.search_requests_paused", se->pause_requests ? "true" : "false");

	ctrl_output_json(cr, ba, "checksums");
}

static void
get_index_cleanup(struct ctrl_req *cr, void *v) {
	struct search_engine *se = v;
	pthread_rwlock_unlock(&se->index_lock);
}

extern pthread_mutex_t search_mutex;
static void
activate_pending_finish(struct ctrl_req *cr, struct bpapi *ba, void *v) {
	struct search_engine *se = v;
	struct search_instance *old_si;
	struct search_instance *si;
	const char *req_csum = bpapi_get_element(ba, "pending_checksum", 0);
	struct index_db *pending;

	log_printf(LOG_DEBUG, "activate start");

	pthread_rwlock_wrlock(&se->index_lock);

	if ((pending = se->pending.index) == NULL) {
		ctrl_error(cr, 503, "no pending index");
		goto out;
	}

	if (req_csum != NULL && strcmp(req_csum, index_get_checksum_string(pending))) {
		ctrl_error(cr, 409, "pending checksum mismatch");
		goto out;
	}

	log_printf(LOG_DEBUG, "activate init");
	if ((si = search_instance_init(se, se->current_si->sic, pending)) == NULL) {
		ctrl_error(cr, 503, "failed reinit search instance");
		goto out;
	}
	if (rename(se->pending.fname, se->active.fname)) {
		search_instance_destroy(si);
		ctrl_error(cr, 503, "failed index rename");
		goto out;
	}
	pthread_mutex_lock(&search_mutex);
	old_si = se->current_si;
	se->current_si = si;
	if (!search_instance_rel(old_si))
		old_si = NULL;

	search_engine_index_set_index(&se->pending, NULL, 0, NULL);
	search_engine_index_set_index(&se->active, pending, 0, NULL);

	pthread_mutex_unlock(&search_mutex);
	if (old_si)
		search_instance_destroy(old_si);

	bpapi_outstring_fmt(ba, "ok\n");
out:
	log_printf(LOG_DEBUG, "activate done");
	pthread_rwlock_unlock(&se->index_lock);
}

struct rp_data {
	struct search_engine *se;
	char destname[PATH_MAX];
	int destfd;
};

static void
receive_pending_start(struct ctrl_req *cr, void *v) {
	struct search_engine *se = v;
	struct rp_data *rp = zmalloc(sizeof(*rp));

	rp->se = se;
	snprintf(rp->destname, sizeof(rp->destname), "%s.new", se->pending.fname);
	rp->destfd = open(rp->destname, O_RDWR|O_CREAT|O_EXCL, 0644);

	if (rp->destfd == -1)
		ctrl_error(cr, 500, "failed to open [%s]: %s", rp->destname, xstrerror(errno));

	ctrl_set_handler_data(cr, rp);
}

static int
receive_pending_data(struct ctrl_req *cr, struct bpapi *ba, size_t tot_len, const char *data, size_t len, void *v) {
	struct rp_data *rp = v;

	if (rp->destfd == -1)
		return 0;

	if (write(rp->destfd, data, len) != (ssize_t)len) {
		ctrl_error(cr, 500, "write failed: %s", xstrerror(errno));
		close(rp->destfd);
		unlink(rp->destname);
	}
	return 0;
}

static void
receive_pending_finish(struct ctrl_req *cr, struct bpapi *ba, void *v) {
	struct rp_data *rp = v;
	bool success = false;
	struct index_db *newpending = NULL;

	if (rp->destfd == -1)
		return;

	if (fsync(rp->destfd)) {
		ctrl_error(cr, 500, "fsync failed: %s", xstrerror(errno));
		goto out;
	}
	close(rp->destfd);
	rp->destfd = -1;

	newpending = open_index(rp->destname, INDEX_READER|INDEX_FILE);
	if (!newpending) {
		ctrl_error(cr, 500, "open_index(%s)", rp->destname);
		goto out;
	}

	const char *csum = index_get_checksum_string(newpending);
	const char *pending_checksum = bpapi_get_element(ba, "pending_checksum", 0);

	if (pending_checksum == NULL) {
		ctrl_error(cr, 400, "pending checksum required");
		goto out;
	}

	if (strcmp(pending_checksum, csum) != 0) {
		ctrl_error(cr, 500, "pending checksum mismatch");
		goto out;
	}

	pthread_rwlock_wrlock(&rp->se->index_lock);
	rename(rp->destname, rp->se->pending.fname);
	search_engine_index_set_index(&rp->se->pending, newpending, 1, NULL);
	newpending = NULL;
	pthread_rwlock_unlock(&rp->se->index_lock);

	bpapi_outstring_fmt(ba, "new index: %s", rp->se->pending.fname);

	success = true;

out:
	if (rp->destfd != -1)
		close(rp->destfd);
	if (!success)
		unlink(rp->destname);
	free(rp);
	if (newpending)
		close_index(newpending, 0);
}

static void
restart_finish(struct ctrl_req *cr, struct bpapi *ba, void *v) {
	log_printf(LOG_INFO, "Received controller request to restart engine");

	quit_signal(1024);

	bpapi_outstring_fmt(ba, "Restart signalled to engine");
}

static void
stop_finish(struct ctrl_req *cr, struct bpapi *ba, void *v) {
	log_printf(LOG_INFO, "Received controller request to stop engine");

	quit_signal(SIGTERM);

	bpapi_outstring_fmt(ba, "Stop signalled to engine");
}

static void
set_pause_state(struct search_engine *se, bool s) {
	if (se->pause_requests == s)
		return;

	log_printf(LOG_INFO, "Search requests %s", s ? "paused" : "resumed");
	stat_message_printf(se->pause_state, s ? "true" : "false");
	se->pause_requests = s;
}

static void
pause_finish(struct ctrl_req *cr, struct bpapi *ba, void *v) {
	set_pause_state(v, true);
}

static void
resume_finish(struct ctrl_req *cr, struct bpapi *ba, void *v) {
	set_pause_state(v, false);
}

int
setup_controllers(struct search_engine *se, struct ctrl_thread **ctp) {
	struct ctrl_handler handlers[] = {
		ctrl_stats_handler,
		{
			.url = "/delta",
			.start = index_merge_start,
			.consume_post = index_merge_data,
			.finish = index_merge_finish,
			.cb_data = se
		},
		{
			.url = "/get_meta",
			.finish = get_meta_finish,
			.cb_data = se
		},
		{
			.url = "/get_index",
			.finish = get_index_finish,
			.cleanup = get_index_cleanup,
			.cb_data = se
		},
		{
			.url = "/index_checksums",
			.finish = index_checksums_finish,
			.cb_data = se
		},
		{
			.url = "/pending",
			.start = receive_pending_start,
			.consume_post = receive_pending_data,
			.finish = receive_pending_finish,
			.cb_data = se
		},
		{
			.url = "/restart",
			.start = NULL,
			.consume_post = NULL,
			.finish = restart_finish,
			.cb_data = NULL
		},
		{
			.url = "/activate_pending",
			.finish = activate_pending_finish,
			.cb_data = se
		},
		{
			.url = "/stop",
			.finish = stop_finish,
			.cb_data = NULL
		},
		{
			.url = "/search_pause",
			.finish = pause_finish,
			.cb_data = se
		},
		{
			.url = "/search_resume",
			.finish = resume_finish,
			.cb_data = se
		},
	};
	int nhandlers = sizeof(handlers) / sizeof(handlers[0]);
	struct bconf_node *conf_node;
	pthread_t *ct = NULL;
	int nthreads;

	conf_node = bconf_get(se->current_si->sic->bconf, "search.controller");
	if (!conf_node)
		return 0;		/* This is not a fatal error */

	if ((*ctp = ctrl_thread_setup(conf_node, NULL, &ct, &nthreads, handlers, nhandlers, se->control_socket)) == NULL) {
		log_printf(LOG_CRIT, "failed to set up controllers");
		return -1;
	}
	se->control_socket = ctrl_thread_get_listen_socket(*ctp);

	/*
	 * Since this is the only way someone can read this stat message, just allocate it here.
	 */
	se->pause_state = stat_message_dynamic_alloc(2, "search", "search_requests_paused");
	set_pause_state(se, false);

	return 0;
}
