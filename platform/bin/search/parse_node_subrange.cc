#include <limits.h>

#include "parse_nodes.hh"
#include "util.h"
#include "search.h"

int32_t
ParseNodeSubrange::next_document(int32_t doc) {
	int32_t d;
	int32_t search = doc;
	int32_t ad;

	while ((d = next->get_next_document(search)) != NULL_DOCOFF) {
		if (docoff_is_subdoc(res->si->db, d)) {
			if (!attr_node) {
				char *f = NULL;
				char *t = NULL;

				if (from)
					xasprintf (&f, "%d", from);
				if (to != INT_MAX)
					xasprintf (&t, "%d", to);

				attr_node = new ParseNodeAttributeRange(xstrdup("suborder"), f, t);
				replace_initialize(&attr_node, res);
				ad = attr_node->get_next_document(d);
				if (ad == NULL_DOCOFF) {
					attr_depleted = true;
				}
			}

			if (!attr_depleted) {
				int res;

				ad = attr_node->get_current_doc();

				res = ad - d;
				if (!res)
					break;
				if (res < 0) {
					ad = attr_node->get_next_document (d);
					if (!ad)
						attr_depleted = true;
					else if (d == ad)
						break;
				}
			}
		} else {
			const struct doc *doc = get_doc(res->si->db, d);
			if (doc->suborder >= from && doc->suborder <= to)
				break;
		}
		search = d;
		increment_doc(&search);
	}

	return d;
}

int
ParseNodeSubrange::get_parse_string(char *buf, size_t buflen) {
	int nlen;
	int ilen;

	nlen = next->get_parse_string(buf, buflen);
	if (nlen < 0)
		return nlen;

	if (to == INT_MAX)
		ilen = snprintf(buf + nlen, buflen - nlen, ",%d-s", from);
	else
		ilen = snprintf(buf + nlen, buflen - nlen, ",%d-%ds", from, to);
	if (ilen < 0 || (size_t)nlen + ilen >= buflen)
		return -1;
	return nlen + ilen;
}

