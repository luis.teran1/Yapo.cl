#include "parse_nodes.hh"
#include "logging.h"
#include "util.h"
#include "timer.h"
#include <assert.h>
#include <string.h>

static int
bfind_docarr(int32_t *docarr, int from, int to, int32_t max) {
	int middle = from + ((to - from) >> 5);  /* A guesstimate for a good split */

	assert(to >= from);

	while (1) {
		if (from > to) {
			log_printf(LOG_CRIT, "Oops in bfind: %u -> %u", from, to);
			return -1;
		}

		if (docarr[middle] == max) {
			return middle;
		}

		if (to == from) {
			if (docarr[middle] > max) {
				return to;
			}
			return -1;
		}

		if (docarr[middle] < max) {
			from = middle + 1;
			middle = from + ((to - from) >> 2);
		} else {
			if (from == middle)
				return middle;
			to = middle;
			middle = from + ((to - from) >> 5);
		}
	}
}

int32_t
ParseNodeDocarr::next_document(int32_t doc) {
	if (fixed_arr) {
		if (docptr < refs)
			return docarr[docptr++];
		return NULL_DOCOFF;
	}

	if (!refs || (docptr = bfind_docarr(docarr, docptr, refs - 1, doc)) == -1) {
		return NULL_DOCOFF;
	} else {
		return docarr[docptr];
	}
}

