#include <limits.h>
#include "parse_nodes.hh"
#include "db.h"
#include "util.h"
#include "search.h"

struct ce_counter {
	struct avl_node tree;
	char *name;
	unsigned int cnt;
};

static void
ce_counter_free(struct avl_node *n) {
	struct ce_counter *cec = avl_data(n, struct ce_counter, tree);

	if (n->link[0])
		ce_counter_free(n->link[0]);
	if (n->link[1])
		ce_counter_free(n->link[1]);
	free(cec);
}

static inline int
ce_cmp(const struct avl_node *an, const struct avl_node *bn) {
	struct ce_counter *a = avl_data(an, struct ce_counter, tree);
	struct ce_counter *b = avl_data(bn, struct ce_counter, tree);

	return strcmp(a->name, b->name);
}

ParseNodeCountEach::~ParseNodeCountEach() {
	if (counters)
		ce_counter_free(counters);
	if (count_node) {
		delete count_node;
	}
	free(attribute);
	free(start);
	free(end);
	free(tag);
}

static void
count_match(void *vn, void *v) {
	ParseNodeInvattr *ia = (ParseNodeInvattr *)vn;
	struct avl_node **root = (struct avl_node **)v;
	struct ce_counter sn;
	struct avl_node *n;

	sn.name = ia->keyword;

	if ((n = avl_lookup(&sn.tree, root, ce_cmp)) == NULL) {
		size_t nlen = strlen(ia->keyword) + 1;
		struct ce_counter *ce = (struct ce_counter *)xmalloc(sizeof(*ce) + nlen);
		ce->cnt = 1;
		ce->name = (char *)(ce + 1);
		memcpy(ce->name, ia->keyword, nlen);
		avl_insert(&ce->tree, root, ce_cmp);
	} else {
		avl_data(n, struct ce_counter, tree)->cnt++;		
	}
}

int32_t
ParseNodeCountEach::next_document(int32_t doc) {
	int32_t res = next->get_next_document(doc);
	int32_t d; 

	if (res == NULL_DOCOFF)
		return NULL_DOCOFF;

	if (count_node == NULL)
		return res;

	d = count_node->get_next_document(res);
	if (d == NULL_DOCOFF) {
		delete count_node;
		count_node = NULL;
		return res;
	}
	if (d == res) {
		count_node->foreach_head_node(count_match, &counters);
	}

	return res;
}

static void
print_counters(struct avl_node *n, struct buf_string *buf, const char *tag) {
	struct ce_counter *ce = avl_data(n, struct ce_counter, tree);
	char *esc_name, *p;
	int len;
	int i;
	int first = 0;

	len = strlen(ce->name);
	p = esc_name = (char *)alloca(len * 2 + 1);
	for (i = 0; i <= len; i++) {
		if (ce->name[i] == ':' || ce->name[i] == '\\') {
			/* First : separates key from value */
			if (first)
				*p++ = '\\';
			else
				first = 1;
		}
		*p++ = ce->name[i];
	}

	bscat(buf, "info:%s:%s:%d\n", tag, esc_name, ce->cnt);

	if (n->link[0])
		print_counters(n->link[0], buf, tag);
	if (n->link[1])
		print_counters(n->link[1], buf, tag);
}

void
ParseNodeCountEach::get_headers(struct buf_string *buf) {
	next->get_headers(buf);
	if (counters)
		print_counters(counters, buf, tag);
}

ParseNode *
ParseNodeCountEach::initialize(struct result *res) {
	ParseNodeAttributeRange *ar;

	replace_initialize(&next, res);
	if (typeid(*next) == typeid(ParseNodeEmpty)) {
		ParseNode *n = next;
		next = NULL;
		return n;
	}

	ar = new ParseNodeAttributeRange(attribute, start, end);
	count_node = (ParseNodeUnion<ParseNodeInvattr> *)ar->initialize(res, true);
	delete ar;
	/* We've given away those allocations to the nodeattributerange, not ours anymore. */
	attribute = start = end = NULL;

	return NULL;
}

int
ParseNodeCountEach::get_parse_string(char *buf, size_t buflen) {
	int nlen;
	int len;

	nlen = next->get_parse_string(buf, buflen);
	if (nlen < 0)
		return nlen;
	buf[nlen++] = ',';
	if ((size_t)nlen >= buflen)
		return -1;

	if (end)
		len = snprintf(buf + nlen, buflen - nlen, "%s:%s-%s,ce(%s)", attribute, start ?: "", end, tag ?: "");
	else
		len = snprintf(buf + nlen, buflen - nlen, "%s:%s-%d,ce(%s)", attribute, start ?: "", INT_MAX, tag ?: "");
	if (len < 0 || (size_t)len + (size_t)nlen >= buflen)
		return -1;
	return len + nlen;
}

