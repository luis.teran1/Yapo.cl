#ifndef PARSE_NODE_CACHE_HH
#define PARSE_NODE_CACHE_HH

#include "parse_nodes.hh"
#include "search.h"
#include "lru.h"

class ParseStore {
public:       
	char parse_string[CACHE_KEY_SIZE];
	size_t pslen;
	int print_parse;

	struct lru *cache;
	struct lru_entry *ce;

	ParseStore(const char *ps, size_t pslen, int pp) {
		if (pslen < sizeof(parse_string)) {
			memcpy(parse_string, ps, pslen + 1);
			this->pslen = pslen;
		} else {
			memcpy(parse_string, ps, sizeof(parse_string) -1 );
			parse_string[sizeof(parse_string) - 1] = '\0';
			this->pslen = sizeof(parse_string) - 1;
		}
		print_parse = pp;
	}

	int get_parse_string(char *buf, size_t buflen) {
		if (buflen > pslen) {
			memcpy(buf, parse_string, pslen + 1);
			return pslen;
		}
		return -1;
	};
};

class ParseNodeCacheLoad : public ParseNodeDocarr, public ParseStore {
public:
	ParseNodeCacheLoad(struct lru *c, struct lru_entry *ce, int fa, const char *ps, size_t pslen, int pp) :
			ParseNodeDocarr(((struct result*)ce->storage)->docoff_arr,
					((struct result*)ce->storage)->ndocs,
					fa),
       			ParseStore(ps, pslen, pp) { this->ce = ce; this->cache = c; };
	~ParseNodeCacheLoad() { log_printf(LOG_DEBUG, "CacheLoad: leaving cache \"%s\"", parse_string); lru_leave(cache, ce); };

	int get_parse_string(char *buf, size_t buflen) { return ParseStore::get_parse_string(buf, buflen); };
	u_int32_t get_sub_ad_counter() { return ((struct result*)this->ce->storage)->sub_ad_counter; }
	void get_headers(struct buf_string *buf) {
		struct result *res = (struct result*)ce->storage;
		bufwrite(&buf->buf, &buf->len, &buf->pos, res->headers.buf, res->headers.pos);
		if (print_parse == 1)
			bscat(buf, "info:parse_string:%s\n", parse_string);
	};
	
	ParseStore * get_parse_store() { return this; }

	union sort_value current_sort_value();

	void finalize(struct result *res, struct timer_instance *ti);

	int get_sortspec(void) {
		return ((struct result *)ce->storage)->res_sortspec;
	}

	void dump(int indent) {
		ParseNode::dump(indent);
		fprintf(stderr, "%*scache_key:[%s] ndocs:%d\n", indent + 1, "", ce->key, ((struct result *)ce->storage)->ndocs);
	}
};

class ParseNodeCacheStore : public ParseNodeNext, public ParseStore {
private:
	struct buf_string headers;
	int sortspec;

public:
	ParseNodeCacheStore(struct lru *c, struct lru_entry *ce, ParseNode *n, const char *ps, size_t pslen, int pp, int ss = SORTSPEC_UNSET) :
			ParseNodeNext(n), ParseStore(ps, pslen, pp) {
		this->cache = c;
		this->ce = ce;
		this->sortspec = ss;
		headers = (const struct buf_string){NULL};
	};
	~ParseNodeCacheStore() { log_printf(LOG_DEBUG, "CacheStore: leaving cache \"%s\"", parse_string); lru_leave(cache, ce); };

	int get_parse_string(char *buf, size_t buflen) { return ParseStore::get_parse_string(buf, buflen); };
	void get_headers(struct buf_string *buf);

	ParseStore * get_parse_store() { return this; }

	void finalize(struct result *res, struct timer_instance *ti);
};

class ParseNodeCache : public ParseNodeNext {
private:
	struct lru *cache;
	int fixed_arr;
	int print_parse;
	void (*pend_cb)(void*);
	void *pend_cb_arg;

public:
	struct timer_instance *ti;

public:
	ParseNodeCache(struct lru *c, int fa, ParseNode *n, struct timer_instance *ti = NULL, int pp = 0,
			void (*pcb)(void*) = NULL, void *pcbarg = NULL) :
			ParseNodeNext(n, false), cache(c), fixed_arr(fa), print_parse(pp), pend_cb(pcb),
			pend_cb_arg(pcbarg), ti(NULL) {
		this->ti = ti;
	};

	ParseNode *initialize(struct result *);

	void set_next(ParseNode *n) { delete next; next = n; };

};

class ParseNodeCacheSearch : public ParseNodeNext {
public:
	ParseNodeCacheSearch(ParseNode *n) : 
		ParseNodeNext(n, false) {};
	
	void set_next(ParseNode *n) { delete next; next = n; };
	
	ParseNode *initialize(struct result *res);
};

#endif /*PARSE_NODE_CACHE_HH*/
