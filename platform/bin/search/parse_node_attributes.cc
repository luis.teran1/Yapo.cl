#include <limits.h>
#include <stdio.h>
#include "parse_nodes.hh"
#include "db.h"
#include "util.h"
#include "search.h"

/* Attribute range */
ParseNode *
ParseNodeAttributeRange::initialize(struct result *res, bool always_union) {
	ParseNodeInvattr *inva = NULL;
	ParseNodeUnion<ParseNodeInvattr> *u = NULL;
	struct invattr_iter iai;
	struct invattr *ia;

	invattr_iter_init(res->si->db, &iai, attribute, start, end);

	while ((ia = invattr_iter_next(&iai)) != NULL) {
		if (inva && !u) {
			u = new ParseNodeUnion<ParseNodeInvattr>;
			u->add_node(inva);
		}
		inva = new ParseNodeInvattr(xstrdup(ia->attr), ia);
		inva->initialize(res);					/* XXX */
		if (u)
			u->add_node(inva);
	}

	invattr_iter_del(&iai);

	if (!u && always_union) {
		u = new ParseNodeUnion<ParseNodeInvattr>;
		if (inva)
			u->add_node(inva);
	}
	if (u) {
		ParseNode *nn = u->initialize(res, always_union);
		if (nn) {
			delete u;
			return nn;
		}
		return u;
	}

	if (inva)
		return inva;
	return new ParseNodeEmpty;
}

int
ParseNodeAttributeRange::get_parse_string(char *buf, size_t buflen) {
	int len;

	if (end) {
		if (strchr(end, '-') || (start && strchr(start, '-')))
			len = snprintf(buf, buflen, "%s:\"%s\"-\"%s\"", attribute, start ? start : "", end);
		else
			len = snprintf(buf, buflen, "%s:%s-%s", attribute, start ? start : "", end);
	} else if (start && strchr(start, '-')) {
		len = snprintf(buf, buflen, "%s:\"%s\"-%d", attribute, start ? start : "", INT_MAX);
	} else {
		len = snprintf(buf, buflen, "%s:%s-%d", attribute, start ? start : "", INT_MAX);
	}
	if (len < 0 || (size_t)len >= buflen)
		return -1;
	return len;
}

