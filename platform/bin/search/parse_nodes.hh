#ifndef PARSE_NODES_H
#define PARSE_NODES_H

#define QUEUE_TYPE_PREFIX /*class*/
#define HEAP_TYPE_PREFIX  /*class*/

#include "queue.h"
#include "heap.h"
#include "index.h"
#include "lru.h"
#include "db.h"
#include "util.h"
#include "subs.h"

#include "sortspec.h"

#include <sys/types.h>
#include <stdint.h>
#include <string.h>
#include <stack>
#include <typeinfo>

#include "logging.h" 

class StackException : public std::exception {
};

class StackEmptyException : public StackException {
public:
	virtual const char *what() const throw () { return "stack empty"; };
};

class StackCorruptException : public StackException {
public:
	virtual const char *what() const throw () { return "stack corrupt"; };
};

class ParseStore;

class ParseNode {
public:
	HEAP_ENTRY(ParseNode) heap_link;    /* Linkage for the union/intersection heap. */
	SLIST_ENTRY(ParseNode) list_link;

protected:
	void replace_initialize(ParseNode **pn, struct result *res) {
		ParseNode *r = (*pn)->initialize(res);
		if (r) {
			delete *pn;
			*pn = r;
		}
	}

	virtual int32_t next_document(int32_t doc) = 0;
	virtual int32_t current_doc() const = 0;
public:
	virtual ~ParseNode() { };

	int32_t get_next_document(int32_t doc) { return next_document(doc); };
	int32_t get_current_doc() const { return current_doc(); };
	virtual int get_parse_string(char *buf, size_t buflen) = 0;
	virtual u_int32_t get_sub_ad_counter() { return 0; };
	virtual void get_headers(struct buf_string *buf) { };

	/* Runtime check for count_level compatibility and type (default is not compatible) */
	virtual bool attrlist_is_union(void) { throw StackCorruptException(); };

	/* 
	 * initialize can be used to replace this object with another.
	 * Notice that get_parse_string usually will be called before this.
	 */
	virtual ParseNode *initialize(struct result *res) = 0;
	virtual void finalize(struct result *res, struct timer_instance *ti) { };

	virtual ParseStore * get_parse_store() { return NULL; };

	virtual void parent_doc_loaded(struct result *result, int32_t suboff, int32_t parentoff)
	{
	}

	void *operator new (size_t sz) { return ::xmalloc(sz); };
	void operator delete (void *ptr) { return ::free(ptr); };

	virtual void dump(int indent) {
		fprintf(stderr, "%*s%s\n", indent, "", typeid(*this).name());
	}
	virtual float get_current_bm25(int ndocs, float k1) {
		return 0.0;
	}

	virtual int get_sortspec(void) {
		return SORTSPEC_NATURAL;
	}
};

class ParseNodeEmpty : public ParseNode{
protected:
	int32_t next_document(int32_t doc) { return NULL_DOCOFF; };
	int32_t current_doc() const { abort(); };

public:
	ParseNodeEmpty() : ParseNode() { };

	int get_parse_string(char *buf, size_t buflen) {
		if (buflen < 2)
			return -1;
		strcpy(buf, "e");
		return 1;
	}

	ParseNode *initialize(struct result *) { return NULL; };
};


class ParseNodeMeta : public ParseNode {
protected:
	int32_t next_document(int32_t doc) { return NULL_DOCOFF; };
	int32_t current_doc() const { return NULL_DOCOFF; };

public:
	struct search_instance *si;

	enum
	{
		DOC_COUNT = 1 << 0,
	};

	int requests;

	ParseNodeMeta(int reqs) : ParseNode(), requests(reqs) { }

	int get_parse_string(char *buf, size_t buflen);
	void get_headers(struct buf_string *buf);

	ParseNode *initialize(struct result *res);
};

template <class PN> static inline int
cmp_parse_node(PN *a, PN *b) {
	return a->get_current_doc() - b->get_current_doc();
}

class ParseNodeNext : public ParseNode {
	public:
		ParseNode *next;
		bool promote_empty;

	protected:
		virtual int32_t next_document(int32_t doc) { return next->get_next_document(doc); };
		virtual int32_t current_doc() const { return next->get_current_doc(); };

	public:
		ParseNodeNext(ParseNode *n, bool pe = true) : next(n), promote_empty(pe) { };

		virtual int get_parse_string(char *buf, size_t buflen) { return next->get_parse_string(buf, buflen); };
		virtual void get_headers(struct buf_string *buf) { next->get_headers(buf); };

		virtual ParseNode *initialize(struct result *res) {
			replace_initialize(&next, res);
			if (promote_empty && typeid(*next) == typeid(ParseNodeEmpty)) {
				ParseNode *n = next;
				next = NULL;
				return n;
			}
			return NULL;
		};

		virtual void finalize(struct result *res, struct timer_instance *ti) { next->finalize(res, ti); };

		virtual ~ParseNodeNext(void) { delete next; };

		virtual void parent_doc_loaded(struct result *result, int32_t suboff, int32_t parentoff)
		{
			next->parent_doc_loaded(result, suboff, parentoff);
		}
		virtual void dump(int indent) {
			ParseNode::dump(indent);
			fprintf(stderr, "%*snext:\n", indent + 1, "");
			next->dump(indent + 2);
		}
		virtual float get_current_bm25(int ndocs, float k1) {
			return next->get_current_bm25(ndocs, k1);
		}
		virtual int get_sortspec(void) {
			return next->get_sortspec();
		}
};

template <class PN = ParseNode>
class ParseNodeUnion : public ParseNode {
	public:
		SLIST_HEAD(, ParseNode) node_list;
		HEAP_HEAD(node_heap, PN) union_nodes;

		HEAP_PROTOTYPE(node_heap, PN, heap_link, cmp_parse_node<PN>);
		int num_nodes;

	protected:
		int32_t next_document(int32_t);
		int32_t current_doc() const { return HEAP_FIRST(&union_nodes)->get_current_doc(); };

	public:
		ParseNodeUnion() : num_nodes(0) { SLIST_INIT(&node_list); HEAP_INIT(&union_nodes); };
		ParseNodeUnion(std::stack<PN*> *st, int start_num, int end_num, bool is_not) : num_nodes(0) {
			SLIST_INIT(&node_list);
			HEAP_INIT(&union_nodes);
			while (start_num--) {
				if (st->empty())
					throw StackEmptyException();
				add_node(st->top());
				st->pop();
			}
		};
		~ParseNodeUnion() {
			PN *n;

			while (!SLIST_EMPTY(&node_list)) {
				n = (PN*)SLIST_FIRST(&node_list);
				SLIST_REMOVE_HEAD(&node_list, list_link);
				delete n;
			}
		}

		int get_parse_string(char *buf, size_t buflen);
		int32_t get_next_document(int32_t doc) { return ParseNodeUnion::next_document(doc); };
		virtual bool attrlist_is_union(void) { return true; };

		ParseNode *initialize(struct result *res, bool always_union);
		ParseNode *initialize(struct result *res) { return initialize(res, false); };

		void add_node(PN *n) { SLIST_INSERT_HEAD(&node_list, n, list_link); num_nodes++; };

		PN *head_node() const { return HEAP_FIRST(&union_nodes); };
		void foreach_head_node(void (*)(void *, void *), void *);	/* First argument to callback should be PN * but c++ sucks */
		void foreach_head_node2(PN *, int32_t, void (*)(void *, void *), void *);	/* First argument to callback should be PN * but c++ sucks */
		int get_num_nodes() const { return num_nodes; };

		virtual void get_headers(struct buf_string *buf);
		virtual void dump(int indent) {
			ParseNode *n;
			ParseNode::dump(indent);
			fprintf(stderr, "%*snodes(%d):\n", indent + 1, "", num_nodes);
			SLIST_FOREACH(n, &node_list, list_link) {
				n->dump(indent + 2);
			}
		}
		float get_current_bm25(int ndocs, float k1);
		int get_sortspec(void) {
			/*
			 * A union only works when all subnodes are in the same sort order (must be natural sort order currently),
			 * the first node has to have the same sortspec as the rest of them.
			 */
			if (HEAP_FIRST(&union_nodes)) {
				return HEAP_FIRST(&union_nodes)->get_sortspec();
			}
			return SORTSPEC_NATURAL;
		}
};

class ParseNodeIntersection : public ParseNode {
private:
	SLIST_HEAD(, ParseNode) intersection_nodes;

protected:
	int32_t next_document(int32_t);
	int32_t current_doc() const { return SLIST_FIRST(&intersection_nodes)->get_current_doc(); };

public:
	ParseNodeIntersection() { SLIST_INIT(&intersection_nodes); };
	ParseNodeIntersection(std::stack<ParseNode*> *st, int start_num, int end_num, bool is_not) {
		SLIST_INIT(&intersection_nodes);

		while (start_num--) {
			if (st->empty())
				throw StackEmptyException();
			add_node(st->top());
			st->pop();
		}
	};
	~ParseNodeIntersection();

	int get_parse_string(char *buf, size_t buflen);

	ParseNode *initialize(struct result *res);
	void finalize(struct result *res, struct timer_instance *ti);

	void add_node(ParseNode *n) { SLIST_INSERT_HEAD(&intersection_nodes, n, list_link); };

	virtual void dump(int indent) {
		ParseNode *n;
		ParseNode::dump(indent);
		fprintf(stderr, "%*snodes:\n", indent + 1, "");
		SLIST_FOREACH(n, &intersection_nodes, list_link) {
			n->dump(indent + 2);
		}
	}
	float get_current_bm25(int ndocs, float k1);
	int get_sortspec(void) {
		/*
		 * An intersection only works when all subnodes are in the same sort order (must be natural sort order currently),
		 * the first node has to have the same sortspec as the rest of them.
		 */
		if (SLIST_FIRST(&intersection_nodes)) {
			return SLIST_FIRST(&intersection_nodes)->get_sortspec();
		}
		return SORTSPEC_NATURAL;
	}
};

class ParseNodeCounter : public ParseNodeNext {
private:
	char *tag;
	ParseNode *count_node;
	int count;
	int inserted_parent;
	bool print_zeros;
	struct counter_union_list *union_list;
	struct result *res;

protected:
	int32_t next_document(int32_t doc);

public:
	ParseNodeCounter(ParseNode *n, char *t, ParseNode *c, bool pz = true, struct counter_union_list *ul = NULL) 
		: ParseNodeNext(n, false), 
		  tag(t), 
		  count_node(c), 
		  count(0), 
		  inserted_parent(0),
		  print_zeros(pz),
		  union_list(ul)
	{ };

	ParseNodeCounter(std::stack<ParseNode*> *st, int start_num, int end_num, bool is_not, const char *t = NULL, int tl = 0, bool pz = true) 
		: ParseNodeNext(NULL, false), 
		  count_node(NULL), 
		  count(0),
		  print_zeros(pz),
		  union_list(NULL)
	{
		if (st->empty())
			throw StackEmptyException();
		count_node = st->top();
		st->pop();
		if (st->empty())
			throw StackEmptyException();
		next = st->top();
		st->pop();
		if (t)
			tag = xstrndup(t, tl);
		else
			tag = NULL;
	}

	~ParseNodeCounter() {
		free(tag);
		delete count_node;
	};

	int get_parse_string(char *buf, size_t buflen);
	void get_headers(struct buf_string *buf);
	
	ParseNode *initialize(struct result *res) { 
		this->res = res;
		replace_initialize(&count_node, res); 
		return ParseNodeNext::initialize(res); 
	};

	void dump(int indent) {
		ParseNode::dump(indent);
		fprintf(stderr, "%*stag(%s) counting on:\n", indent + 1, "", tag);
		count_node->dump(indent + 2);
		fprintf(stderr, "%*snext:\n", indent + 1, "");
		next->dump(indent + 2);
	}
};

class ParseNodeCountAll : public ParseNodeNext {
private:
	char *header;
	int count;
	int inserted_parent;
	struct result *res;
protected:
	int32_t next_document(int32_t doc);

public:
	ParseNodeCountAll(ParseNode *n, char *h) : ParseNodeNext(n, false), header(h), count(0), inserted_parent(0) { };
	ParseNodeCountAll(std::stack<ParseNode*> *st, int start_num, int end_num, bool is_not, const char *t = NULL, int tl = 0) : ParseNodeNext(NULL, false), count(0) {
		if (st->empty())
			throw StackEmptyException();
		next = st->top();
		st->pop();
		header = xstrndup(t, tl);
	}
	~ParseNodeCountAll() { free(header); }

	int get_parse_string(char *buf, size_t buflen);
	void get_headers(struct buf_string *buf);
	void dump(int indent) {
		ParseNode::dump(indent);
		fprintf(stderr, "%*sheader(%s), next:\n", indent + 1, "", header);
		next->dump(indent + 2);
	}

	ParseNode *initialize(struct result *res) {
		ParseNodeNext::initialize(res);
		this->res = res;
		return NULL;
	}

};

class ParseNodeInvattr;

class ParseNodeCountEach : public ParseNodeNext {
private:
	char *tag;
	char *attribute;
	char *start;
	char *end;
	ParseNodeUnion<ParseNodeInvattr> *count_node;
	struct avl_node *counters;

protected:
	int32_t next_document(int32_t doc);

public:
	ParseNodeCountEach(ParseNode *n, char *t, char *a, char *s, char *e) 
		: ParseNodeNext(n, false), 
		  tag(t), 
		  attribute(a),
		  start(s),
		  end(e),
		  count_node(NULL),
		  counters(NULL)
	{ }

	ParseNodeCountEach(std::stack<ParseNode*> *st, int start_num, int end_num, bool is_not, const char *t = NULL, int tl = 0) 
		: ParseNodeNext(NULL, false), 
		  tag(NULL),
		  attribute(NULL),
		  start(NULL),
		  end(NULL)
	{
		abort();
	}

	~ParseNodeCountEach();

	ParseNode *initialize(struct result *res);
	int get_parse_string(char *buf, size_t buflen);

	void get_headers(struct buf_string *);
	virtual void dump(int indent) {
		ParseNode::dump(indent);
		fprintf(stderr, "%*scounteach %s %s %s %s\n", indent + 1, "", tag, attribute, start ?: "<>", end ?: "<>");
	}
};

class ParseNodeFilter : public ParseNodeNext {
private:
	ParseNode *filter_node;
	bool negated;

protected:
	int32_t next_document(int32_t doc);

public:
	ParseNodeFilter(ParseNode *n, ParseNode *f, bool neg = false) : ParseNodeNext(n), filter_node(f), negated(neg) { };
	ParseNodeFilter(std::stack<ParseNode*> *st, int start_num, int end_num, bool is_not) : ParseNodeNext(NULL), filter_node(NULL) {
		if (st->empty())
			throw StackEmptyException();
		filter_node = st->top();
		st->pop();
		if (st->empty())
			throw StackEmptyException();
		next = st->top();
		st->pop();
		negated = is_not;
	}
	~ParseNodeFilter() { delete filter_node; };

	int get_parse_string(char *buf, size_t buflen);
	ParseNode *initialize(struct result *res) { 
		replace_initialize(&filter_node, res); 
		return ParseNodeNext::initialize(res); 
	};
	virtual void dump(int indent) {
		ParseNode::dump(indent);
		fprintf(stderr, "%*sfilter_node:\n", indent + 1, "");
		filter_node->dump(indent + 2);
		fprintf(stderr, "%*snext:\n", indent + 1, "");
		next->dump(indent + 2);
	}
};

class ParseNodeOrder : public ParseNodeNext {
private:
	int from;
	int to;
	struct bsearch_cache *db;

protected:
	int32_t next_document(int32_t );

public:
	ParseNodeOrder(ParseNode *n, int f, int t) : ParseNodeNext(n), from(f), to(t) { };
	ParseNodeOrder(std::stack<ParseNode*> *st, int start_num, int end_num, bool is_not) : ParseNodeNext(NULL), from(start_num), to(end_num) {
		if (st->empty())
			throw StackEmptyException();
		next = st->top();
		st->pop();
	};
	ParseNode *initialize(struct result *res);
	int get_parse_string(char *buf, size_t buflen);
	virtual void dump(int indent) {
		ParseNode::dump(indent);
		fprintf(stderr, "%*sorder (%d - %d), next:\n", indent + 1, "", from, to);
		next->dump(indent + 2);
	}
};

class ParseNodeSubrange : public ParseNodeNext {
private:
	int from;
	int to;
	bool attr_depleted;
	ParseNode *attr_node;
	struct result *res;

protected:
	int32_t next_document(int32_t );

public:
	ParseNodeSubrange(ParseNode *n, int f, int t) : ParseNodeNext(n), from(f), to(t), attr_depleted(false), attr_node(NULL) { };
	ParseNodeSubrange(std::stack<ParseNode*> *st, int start_num, int end_num, bool is_not) : ParseNodeNext(NULL), from(start_num), to(end_num), attr_depleted(false), attr_node(NULL) {
		if (st->empty())
			throw StackEmptyException();
		next = st->top();
		st->pop();
	};

	~ParseNodeSubrange() { delete attr_node; };

	int get_parse_string(char *buf, size_t buflen);
	ParseNode *initialize(struct result *res) {
		this->res = res;
		return ParseNodeNext::initialize(res);
	}
	virtual void dump(int indent) {
		ParseNode::dump(indent);
		fprintf(stderr, "%*srange (%d - %d):\n", indent + 1, "", from, to);
		if (attr_node)
			attr_node->dump(indent + 2);
		fprintf(stderr, "%*snext:\n", indent + 1, "");
		next->dump(indent + 2);
	}
};

class ParseNodeMinus : public ParseNodeNext {
private:
	ParseNode *minus_node;

protected:
	int32_t next_document(int32_t doc);

public:
	ParseNodeMinus(ParseNode *n, ParseNode *m) : ParseNodeNext(n), minus_node(m) { };
	ParseNodeMinus(std::stack<ParseNode*> *st, int start_num, int end_num, bool is_not) : ParseNodeNext(NULL), minus_node(NULL) {
		if (st->empty())
			throw StackEmptyException();
		minus_node = st->top();
		st->pop();
		if (st->empty())
			throw StackEmptyException();
		next = st->top();
		st->pop();
	}
	~ParseNodeMinus() { delete minus_node; };

	int get_parse_string(char *buf, size_t buflen);

	ParseNode *initialize(struct result *res) { 
		replace_initialize(&minus_node, res); 
		return ParseNodeNext::initialize(res);
	}
	virtual void dump(int indent) {
		ParseNode::dump(indent);
		fprintf(stderr, "%*sminus:\n", indent + 1, "");
		minus_node->dump(indent + 2);
		fprintf(stderr, "%*snext:\n", indent + 1, "");
		next->dump(indent + 2);
	}
};

class ParseNodeDocument : public ParseNode {
protected:
	int refs;         /* Number of matches */
	int docptr;	/* Current doc-pointer */

public:
	ParseNodeDocument(int r = 0) : refs(r), docptr(0) { };
	virtual void dump(int indent) {
		ParseNode::dump(indent);
		fprintf(stderr, "%*sd(%d %d)\n", indent + 1, "", refs, docptr);
	}
};

class ParseNodeInvword : public ParseNodeDocument {
protected:
	int skip_extra;			/* Skip words without a position (invisible words like synonyms) */
	int pos_flags;

	struct invword *invword;
	int loaded;

public:
	char *keyword;                  /* Pointer to the actual nul-terminated keyword */
	HEAP_ENTRY(ParseNodeInvword) heap_link;    /* Allows to define ParseNodeUnion<ParseNodeInvword> */

protected:
	int32_t next_document(int32_t doc);
	int32_t current_doc() const { return invword->docs[docptr].docoff; };

public:
	ParseNodeInvword(char *kw, int se = 0, int pf = 0, struct invword *iw = NULL) :
		ParseNodeDocument(), skip_extra(se), pos_flags(pf), invword(iw), loaded(0), keyword(kw) { };
	~ParseNodeInvword() { free(keyword); };

	int32_t get_next_document(int32_t doc) { return ParseNodeInvword::next_document(doc); };
	int32_t get_current_doc() const { return ParseNodeInvword::current_doc(); };

	int get_parse_string(char *buf, size_t buflen);

	ParseNode *initialize(struct result *);

	virtual void dump(int indent) {
		ParseNodeDocument::dump(indent);
		fprintf(stderr, "%*sword: %s\n", indent + 1, "", keyword);
	}
	float get_current_bm25(int ndocs, float k1);
};

class ParseNodeInvattr : public ParseNodeDocument {
protected:
	struct invattr *invattr;
	int loaded;

public:
	char *keyword;                  /* Pointer to the actual nul-terminated keyword */
	HEAP_ENTRY(ParseNodeInvattr) heap_link;    /* Allows to define ParseNodeUnion<ParseNodeInvattr> */
	bool combined;

protected:
	int32_t next_document(int32_t off);
	int32_t current_doc() const { return invattr->docs[docptr]; };

	void load(struct result *res);

public:
	ParseNodeInvattr(char *kw, struct invattr *ia = NULL) :
		ParseNodeDocument(), invattr(ia), loaded(0), keyword(kw), combined(false) { };
	~ParseNodeInvattr() { free(keyword); };

	int32_t get_next_document(int32_t doc) { return ParseNodeInvattr::next_document(doc); };
	int32_t get_current_doc() const { return ParseNodeInvattr::current_doc(); };

	int get_parse_string(char *buf, size_t buflen);

	ParseNode *initialize(struct result *);

	bool combine(const char *kw, struct result *res);
	virtual void dump(int indent) {
		ParseNodeDocument::dump(indent);
		fprintf(stderr, "%*sattr: %s\n", indent + 1, "", keyword);
	}
};

class ParseNodeInvwordPos : public ParseNodeInvword {
private:
	const struct docpos *word_index_ptr;	/* Pointer to the in-ad position index of the word. */
	int word_index_len;		/* Number of in-ad positions for the word */
	int word_index;			/* Index to the in-ad position of the word, used for phrase searches. */
public:
	int phrase_index;		/* This nodes index in a phrase-chain. */

public:
	ParseNodeInvwordPos(char *kw, int pi, int pf = 0, struct invword *iw = NULL) :
			ParseNodeInvword(kw, 1, pf, iw), phrase_index(pi) { };

	/* Get the in-ad index for the current ad. */
	void reset_word_index_ptr();

	/* Check wether the current doc contains the word in position pos + phrase_index */
	int doc_match_pos(int pos);
	virtual void dump(int indent) {
		ParseNodeInvword::dump(indent);
		fprintf(stderr, "%*spos: %d\n", indent + 1, "", phrase_index);
	}
};

class ParseNodeDocarr : public ParseNodeDocument {
protected:
	int fixed_arr;
	int32_t *docarr;
	int free_docs;

protected:
	int32_t next_document(int32_t doc);
	int32_t current_doc() const { return docarr[docptr - fixed_arr]; };

public:
	ParseNodeDocarr(int32_t *da, int r, int fa = 0, int frd = 0) :
		ParseNodeDocument(r), fixed_arr(fa), docarr(da), free_docs(frd) { };
	virtual ~ParseNodeDocarr() { if (free_docs) free(docarr); };

	int get_parse_string(char *buf, size_t buflen) { return -1; };

	ParseNode *initialize(struct result *) { return NULL; };
};

class ParseNodeAttributeList : public ParseNodeNext {
public:
	ParseNodeAttributeList(ParseNode *n) : ParseNodeNext(n) { }

	virtual void add_node(ParseNode *n, bool head = false, int is_union = 0) = 0;
};

class ParseNodeRelsort : public ParseNodeNext {
private:
	int tot_ndocs;
	int sortspec;
	void load_sort_value(int32_t doc);
	struct result *res;
public:
	ParseNodeRelsort(int ss) : ParseNodeNext(NULL), sortspec(ss) {}
	int32_t next_document(int32_t );
	int get_parse_string(char *buf, size_t buflen);
	ParseNode *initialize(struct result *res);
	void finalize(struct result *res, struct timer_instance *ti);
	void dump(int indent) {
		ParseNode::dump(indent);
		fprintf(stderr, "%*srelsort 0x%x, next:\n", indent + 1, "", sortspec);
		next->dump(indent + 2);
	}
	int get_sortspec(void) {
		return sortspec | SORTSPEC_FLOAT | SORTSPEC_ATTR;
	}
};

/*
 * AttributeList is a Next. This is false inheritance. In some particular cases, like this node for example, we want
 * to be an attributelist without being a bloody Next.
 */
class ParseNodeAttrlistSort : public ParseNodeAttributeList {
private:
	ParseNode *attrnode;
	bool attrnode_is_cache;
	bool attrnode_is_union;
	int sortspec;

	bool cache_attrnode(struct result *res);

public:
	ParseNodeAttrlistSort(ParseNode *n, ParseNode *an, int ss) : ParseNodeAttributeList(n), attrnode(an), sortspec(ss) {
		attrnode_is_union = an->attrlist_is_union();
	}
	~ParseNodeAttrlistSort() {
		delete attrnode;
	}
	int32_t next_document(int32_t doc);
	void add_node(ParseNode *n, bool head = false, int is_union = 0);
	void set_null_value(int32_t);
	friend void cache_load_sort_value(void *mdarg, struct result *result, int32_t docoff);
	ParseNode *initialize(struct result *res);
	int get_parse_string(char *buf, size_t buflen);
	virtual void dump(int indent) {
		ParseNode::dump(indent);
		fprintf(stderr, "%*sattrlistsort:\n", indent + 1, "");
		if (attrnode)
			attrnode->dump(indent + 2);
		fprintf(stderr, "%*snext:\n", indent + 1, "");
		if (next)
			next->dump(indent + 2);
	}

	union sort_value current_sort_value();
};

class ParseNodeSubsort : public ParseNodeNext {
private:
	int sortspec;
	ParseNodeAttrlistSort *sortnode;
	int32_t null_value;
	struct result *res;

protected:
	int32_t get_attrsort_value(int32_t docoff);
	void load_sort_value(int32_t docoff);
	int32_t next_document(int32_t doc);

public:
	ParseNodeSubsort(ParseNode *n, int ss, ParseNodeAttrlistSort *al) : ParseNodeNext(n), sortspec(ss), sortnode(al) { };
	ParseNodeSubsort(std::stack<ParseNode*> *st, int ss) : ParseNodeNext(NULL), sortspec(ss), sortnode(NULL) {
		abort();
#if 0
		if (so >= 4) {
			if (st->empty())
				throw StackEmptyException();
			attrnode = st->top();
			attrnode_is_union = attrnode->attrlist_is_union();
			st->pop();
		}
		if (st->empty())
			throw StackEmptyException();
		next = st->top();
		st->pop();
#endif
       	};
	~ParseNodeSubsort(void) { delete sortnode; }
	
	int get_parse_string(char *buf, size_t buflen);

	virtual ParseNode *initialize(struct result *res);

	void finalize(struct result *res, struct timer_instance *ti);

	void parent_doc_loaded(struct result *result, int32_t suboff, int32_t parentoff);

	virtual void dump(int indent) {
		ParseNode::dump(indent);
		fprintf(stderr, "%*ssubsort 0x%x:\n", indent + 1, "", sortspec);
		if (sortnode)
			sortnode->dump(indent + 2);
		fprintf(stderr, "%*snext:\n", indent + 1, "");
		next->dump(indent + 2);
	}
	int get_sortspec(void) {
		return sortspec | SORTSPEC_INT;
	}
};

class ParseNodeDistanceSort : public ParseNodeNext {
private:
	ParseNodeAttrlistSort *xn, *yn, *rn;
	float xcoord, ycoord;
	struct result *res;
	void load_sort_value(int32_t docoff);
public:
	ParseNodeDistanceSort(float x, float y) : ParseNodeNext(NULL), xn(NULL), yn(NULL), rn(NULL), xcoord(x), ycoord(y) {}
	~ParseNodeDistanceSort() {
		delete xn;
		delete yn;
		delete rn;
	}
	int32_t next_document(int32_t doc);
	int get_parse_string(char *buf, size_t buflen);
	ParseNode *initialize(struct result *res);
	void finalize(struct result *res, struct timer_instance *ti);
	void dump(int indent) {
		ParseNode::dump(indent);
		fprintf(stderr, "%*sdistsort %f,%f, next:\n", indent + 1, "", xcoord, ycoord);
		next->dump(indent + 2);
	}
	int get_sortspec(void) {
		return SORTSPEC_SORTVALUE|SORTSPEC_INC|SORTSPEC_FLOAT;
	}
};

class ParseNodeSortSlice : public ParseNodeNext {
private:
	unsigned int slices;
	int sortspec;
	struct unnecessary_struct_name {
		union {
			unsigned int i;
			float f;
		};
		unsigned int count;
	} *sizes;
public:
	ParseNodeSortSlice(int s, int ss) : ParseNodeNext(NULL), slices(s), sortspec(ss) { 
		sizes = (struct ParseNodeSortSlice::unnecessary_struct_name *)zmalloc(sizeof *sizes * s);
	}
	~ParseNodeSortSlice() {
		free(sizes);
	}

	void dump(int indent) {
		ParseNode::dump(indent);
		fprintf(stderr, "%*ssortslice %d, 0x%x next:\n", indent + 1, "", slices, sortspec);
		next->dump(indent + 2);
	}
	int get_parse_string(char *buf, size_t buflen);
	void finalize(struct result *, struct timer_instance *);
	void get_headers(struct buf_string *buf);
	int get_sortspec(void) {
		return SORTSPEC_SORTVALUE|SORTSPEC_INC|SORTSPEC_INT;
	}
};

class ParseNodePhrase : public ParseNode {
private:
	SLIST_HEAD(phrase_word_list, ParseNode) phrase_nodes;
	ParseNodeInvwordPos *last;

private:
	int match_phrase();

protected:
	int32_t next_document(int32_t doc);
	int32_t current_doc() const { return ((ParseNodeInvwordPos*)SLIST_FIRST(&phrase_nodes))->get_current_doc(); };

public:
	ParseNodePhrase() : ParseNode(), last(NULL) { SLIST_INIT(&phrase_nodes); };
	~ParseNodePhrase();

	int get_parse_string(char *buf, size_t buflen);

	ParseNode *initialize(struct result *);

	void add_word(ParseNodeInvwordPos *word) {
		if (last) {
			word->phrase_index = last->phrase_index + 1;
			SLIST_INSERT_AFTER(last, word, list_link);
		} else
			SLIST_INSERT_HEAD(&phrase_nodes, word, list_link);
		last = word;
	}

	virtual void dump(int indent) {
		ParseNode *n;
		ParseNode::dump(indent);
		fprintf(stderr, "%*sphrase:\n", indent + 1, "");
		SLIST_FOREACH(n, &phrase_nodes, list_link) {
			n->dump(indent + 2);
		}
		fprintf(stderr, "%*slast:\n", indent + 1, "");
		last->dump(indent + 2);
	}
};

class ParseNodeReplaced : public ParseNode {
protected:
	int32_t next_document(int32_t doc) { abort(); return NULL_DOCOFF; };
	int32_t current_doc() const { abort(); return NULL_DOCOFF; };
};

class ParseNodeSubword : public ParseNodeReplaced {
private:
	enum sub_type sub_type;
	char *base;
	int pos_flags;

public:
	ParseNodeSubword(enum sub_type st, char *b, int pf) : sub_type(st), base(b), pos_flags(pf) { };
	~ParseNodeSubword() { free(base); };

	int get_parse_string(char *buf, size_t buflen);

	ParseNode *initialize(struct result *);
	virtual void dump(int indent) {
		ParseNode::dump(indent);
		fprintf(stderr, "%*ssubword(%d, 0x%x): %s\n", indent + 1, "", sub_type, pos_flags, base);
	}
};

class ParseNodeAttributeRange : public ParseNodeReplaced {
private:
	char *attribute;
	char *start;
	char *end;

public:
	ParseNodeAttributeRange(char *a, char *s, char *e) : attribute(a), start(s), end(e) { };
	~ParseNodeAttributeRange() { free(attribute); free(start); free(end); };

	virtual bool attrlist_is_union(void) { return false; }

	int get_parse_string(char *buf, size_t buflen);

	ParseNode *initialize(struct result *res, bool always_union);
	ParseNode *initialize(struct result *res) { return initialize(res, false); };

	const char *get_attribute() { return attribute; };

	virtual void dump(int indent) {
		ParseNode::dump(indent);
		fprintf(stderr, "%*sattr/start/end: %s/%s/%s\n", indent + 1, "", attribute, start?:"", end?:"");
	}
};

struct count_level;

class ParseNodeCountLevel : public ParseNodeAttributeList {
private:
	SIMPLEQ_HEAD(, count_level) count_levels;
	bool count_only_parents;
	char *tag;
	struct bsearch_cache *db;

protected:
	int32_t next_document(int32_t doc);
	
public:
	ParseNodeCountLevel(ParseNode *n, char * t, bool _count)
		: ParseNodeAttributeList(n), count_only_parents(_count), tag(t)
	{ 
		SIMPLEQ_INIT(&count_levels); 
	};
	ParseNodeCountLevel(std::stack<ParseNode*> *st, int start_num, int end_num, bool is_not, bool _count = false) 
		: ParseNodeAttributeList(NULL), count_only_parents(_count), tag(NULL)
	{
		SIMPLEQ_INIT(&count_levels);
		while (start_num--) {
			if (st->empty())
				throw StackEmptyException();
			ParseNode *n = st->top();
			add_node(n, true, n->attrlist_is_union());
			st->pop();
		}
		if (st->empty())
			throw StackEmptyException();
		next = st->top();
		st->pop();
	}
	~ParseNodeCountLevel();

	int get_parse_string(char *buf, size_t buflen);
	
	void get_headers(struct buf_string *buf);

	ParseNode *initialize(struct result *res);

	void add_node(ParseNode *n, bool head = false, int is_union = 0);
};

class ParseNodeBucket : public ParseNodeAttributeList {
private:
	SIMPLEQ_HEAD(, count_level) count_levels;
	char * tag;

protected:
	int32_t next_document(int32_t doc);
	
public:
	ParseNodeBucket(ParseNode *n, char * t, bool _count)
		: ParseNodeAttributeList(n), tag(t)
	{ 
		SIMPLEQ_INIT(&count_levels); 
	};
	ParseNodeBucket(std::stack<ParseNode*> *st, int start_num, int end_num, bool is_not, bool _count = false) 
		: ParseNodeAttributeList(NULL), tag(NULL)
	{
		SIMPLEQ_INIT(&count_levels);
		while (start_num--) {
			if (st->empty())
				throw StackEmptyException();
			ParseNode *n = st->top();
			add_node(n, true, n->attrlist_is_union());
			st->pop();
		}
		if (st->empty())
			throw StackEmptyException();
		next = st->top();
		st->pop();
	}
	~ParseNodeBucket();

	int get_parse_string(char *buf, size_t buflen);
	
	void get_headers(struct buf_string *);

	ParseNode *initialize(struct result *res);

	void add_node(ParseNode *n, bool head = false, int is_union = 0);
};

struct avl_node;
class ParseNodeCountTree : public ParseNodeAttributeList {
private:
	SIMPLEQ_HEAD(, count_level) count_levels;

	struct avl_node *tree;
	char * tag;

protected:
	int32_t next_document(int32_t doc);
	
public:
	ParseNodeCountTree(ParseNode *n, char * t, bool _count)
		: ParseNodeAttributeList(n), tree(NULL), tag(t)
	{ 
		SIMPLEQ_INIT(&count_levels); 
	};
	ParseNodeCountTree(std::stack<ParseNode*> *st, int start_num, int end_num, bool is_not, bool _count = false) 
		: ParseNodeAttributeList(NULL), tree(NULL)
	{
		SIMPLEQ_INIT(&count_levels);
		while (start_num--) {
			if (st->empty())
				throw StackEmptyException();
			ParseNode *n = st->top();
			add_node(n, true, n->attrlist_is_union());
			st->pop();
		}
		if (st->empty())
			throw StackEmptyException();
		next = st->top();
		st->pop();
	}
	~ParseNodeCountTree();

	int get_parse_string(char *buf, size_t buflen);
	
	void get_headers(struct buf_string *);

	ParseNode *initialize(struct result *res);

	void add_node(ParseNode *n, bool head = false, int is_union = 0);
};

class ParseNodeUncacheable : public ParseNodeReplaced {
	/* Simple parse node disabling cache. */
private:
	ParseNode *node;

public:
	ParseNodeUncacheable (ParseNode *n) : ParseNodeReplaced(), node(n) { };
	~ParseNodeUncacheable () { delete node; }

	int get_parse_string(char *buf, size_t buflen) {
		return -1;
	}

	ParseNode *initialize(struct result *res) {
		ParseNode *n = node->initialize(res);

		if (n)
			delete node;
		else
			n = node;
		/* Disown node. */
		node = NULL;
		return n;
	}
};

#endif
