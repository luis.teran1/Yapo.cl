#include "parse_nodes.hh"
#include "util.h"
#include "avl.h"
#include "logging.h"
#include "search.h"

#include <stdio.h>

struct count_level {
	union {
		ParseNodeAttributeRange *ar;
		ParseNodeUnion<ParseNodeInvattr> *u;
	};
	int num_counters;
	int is_union;
	struct avl_node *counters;
	SIMPLEQ_ENTRY(count_level) list_link;
	int inserted_sub_id;
	int inserted_parent;
};

struct cl_counter {
	struct avl_node tree;
	char *name;
	unsigned int cnt;
};

static void
cl_counter_free(struct avl_node *n) {
	struct cl_counter *clc = avl_data(n, struct cl_counter, tree);

	if (n->link[0])
		cl_counter_free(n->link[0]);
	if (n->link[1])
		cl_counter_free(n->link[1]);
	free(clc->name);
	free(clc);
}

ParseNodeCountLevel::~ParseNodeCountLevel() {
	while (!SIMPLEQ_EMPTY(&count_levels)) {
		struct count_level *cl = SIMPLEQ_FIRST(&count_levels);
		SIMPLEQ_REMOVE_HEAD(&count_levels, list_link);

		if (cl->counters)
			cl_counter_free(cl->counters);
		if (cl->is_union) {
			delete cl->u;
		} else {
			delete cl->ar;
		}
		free(cl);
	}
	free(tag);
}

static int
cl_cmp(const struct avl_node *an, const struct avl_node *bn) {
	const struct cl_counter *a = avl_data(an, const struct cl_counter, tree);
	const struct cl_counter *b = avl_data(bn, const struct cl_counter, tree);
#if 0
	return (intptr_t)a->name - (intptr_t)b->name;
#else
	return strcmp(a->name, b->name);
#endif
}

int32_t
ParseNodeCountLevel::next_document(int32_t doc) {
	int32_t res = next->get_next_document(doc);
	struct count_level *cl;

	if (res == NULL_DOCOFF)
		return NULL_DOCOFF;

	SIMPLEQ_FOREACH(cl, &count_levels, list_link) {
		int32_t d;

		if (cl->u == NULL)
			continue;

		d = cl->u->get_next_document(res);

		if (d == NULL_DOCOFF) {
			delete cl->u;
			cl->u = NULL;
			continue;
		}

		if (d == res) {
			struct cl_counter *clc;
			struct cl_counter cls;
			ParseNodeInvattr *ia;
			struct avl_node *n;

			if (count_only_parents && docoff_is_subdoc(db, d)) {
				const struct doc *doc = get_doc(db, d);
				if (doc->suborder != cl->inserted_parent || doc->id == cl->inserted_sub_id) { /* XXX == or != ? */
					cl->inserted_parent = doc->suborder;
					cl->inserted_sub_id = doc->id;
				} else {
					continue;
				}
			}

			ia = cl->u->head_node();
			cls.name = ia->keyword;

			if ((n = avl_lookup(&cls.tree, &cl->counters, cl_cmp)) == NULL) {
				clc = (struct cl_counter *)xmalloc(sizeof(*clc));
				clc->cnt = 0;
				clc->name = xstrdup(ia->keyword);
				cl->num_counters++;
				avl_insert(&clc->tree, &cl->counters, cl_cmp);
			} else {
				clc = avl_data(n, struct cl_counter, tree);
			}
			clc->cnt++;
		}
	}

	return res;
}

void
ParseNodeCountLevel::add_node(ParseNode *n, bool head, int is_union) {
	struct count_level *cl = (struct count_level*)zmalloc(sizeof(*cl));

	if (is_union) {
		cl->u = (ParseNodeUnion<ParseNodeInvattr>*)n;
		cl->is_union = 1;
	} else {
		cl->ar = (ParseNodeAttributeRange*)n;
	}

	if (head)
		SIMPLEQ_INSERT_HEAD(&count_levels, cl, list_link);
	else
		SIMPLEQ_INSERT_TAIL(&count_levels, cl, list_link);
}

static void
print_counters(struct avl_node *n, struct buf_string *buf, const char *tag, const char *tag_append) {
	struct cl_counter *clc = avl_data(n, struct cl_counter, tree);
	char *esc_name, *p;
	int len;
	int i;
	int first = 0;
	if(!tag)
	   tag = "count_level";

	len = strlen(clc->name);
	p = esc_name = (char *)alloca(len * 2 + 1);
	for (i = 0; i <= len; i++) {
		if (clc->name[i] == ':' || clc->name[i] == '\\') {
			/* First : separates key from value */
			if (first)
				*p++ = '\\';
			else
				first = 1;
		}
		*p++ = clc->name[i];
	}

	bscat(buf, "info:%s%s:%s:%d\n", tag, (tag_append ? tag_append : ""), esc_name, clc->cnt);

	if (n->link[0])
		print_counters(n->link[0], buf, tag, tag_append);
	if (n->link[1])
		print_counters(n->link[1], buf, tag, tag_append);
}

void
ParseNodeCountLevel::get_headers(struct buf_string *buf) {
	struct count_level *cl;

	next->get_headers(buf);

	SIMPLEQ_FOREACH(cl, &count_levels, list_link) {
		/* Print counters for the first level where more than one key matched. */
		if (cl->num_counters > 1) {
			print_counters(cl->counters, buf, tag, NULL);
			return;
		} else if (cl->num_counters == 1) {
			print_counters(cl->counters, buf, tag, "_path");
		}
	}
}

ParseNode *
ParseNodeCountLevel::initialize(struct result *res) {
	struct count_level *cl;
	struct count_level *clp = NULL;

	replace_initialize(&next, res);
	if (typeid(*next) == typeid(ParseNodeEmpty)) {
		ParseNode *n = next;
		next = NULL;
		return n;
	}

	for (cl = SIMPLEQ_FIRST(&count_levels); cl; cl = (clp ? SIMPLEQ_NEXT(clp, list_link) : SIMPLEQ_FIRST(&count_levels))) {
		ParseNodeUnion<ParseNodeInvattr> *u;
	       
		if (cl->is_union) {
			u = (ParseNodeUnion<ParseNodeInvattr>*)cl->u->initialize(res, true);
			if (u) {
				delete cl->u;
				cl->u = u;
			}

			u = cl->u;
		} else {
			u = (ParseNodeUnion<ParseNodeInvattr>*)cl->ar->initialize(res, true);
			delete cl->ar;
			cl->u = u;
			cl->is_union = 1;
		}
		if (u && !SLIST_EMPTY(&u->node_list)) {
			cl->counters = NULL;
			clp = cl;
		} else {
			if (clp)
				SIMPLEQ_REMOVE_AFTER(&count_levels, clp, list_link);
			else
				SIMPLEQ_REMOVE_HEAD(&count_levels, list_link);
			free(cl);
			if (u)
				delete u;
		}
	}
	if (SIMPLEQ_EMPTY(&count_levels)) {
		ParseNode *n = next;
		next = NULL;
		return n;
	}

	db = res->si->db;
	return NULL;
}

int
ParseNodeCountLevel::get_parse_string(char *buf, size_t buflen) {
	size_t tlen = 0;
	struct count_level *cl;
	int num = 0;
	int ilen;

	ilen = next->get_parse_string(buf, buflen);
	if (ilen < 0)
		return ilen;
	buf[ilen++] = ',';
	tlen += ilen;
	if ((size_t)tlen >= buflen)
		return -1;

	SIMPLEQ_FOREACH(cl, &count_levels, list_link) {
		if (cl->is_union)
			ilen = cl->u->get_parse_string(buf + tlen, buflen - tlen);
		else
			ilen = cl->ar->get_parse_string(buf + tlen, buflen - tlen);

		if (ilen < 0)
			return ilen;
		tlen += ilen;
		buf[tlen++] = ',';
		if (tlen >= buflen)
			return -1;
		num++;
	}
	ilen = snprintf(buf + tlen, buflen - tlen, "%dcl(%s)", num, tag ?: "");
	if (ilen < 0 || tlen + ilen >= buflen)
		return -1;
	return tlen + ilen;
}
