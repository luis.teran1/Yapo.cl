#ifndef SUBS_H
#define SUBS_H

#include "queue.h"
#include "tree.h"

enum sub_type {
	SUB_NONE = 0,
	SUB_LEFT = 1,
	SUB_MIDDLE = 2,
	SUB_RIGHT = 4
};

struct sub_tree;
struct sub_cons;
struct sub {
	RB_ENTRY(sub) tree;
	union {
		char sub[4];
		int32_t i;
	} key;
	SLIST_HEAD(,sub_cons) list;
};

struct sub_cons {
	struct sub_word *w;
	SLIST_ENTRY(sub_cons) next;
};

struct sub_word {
	const char *word;
	struct sub_cons *c;
	SLIST_ENTRY(sub_word) next;
};

/* RB_HEAD(sub_tree, sub) */
struct sub_tree {
	void *storage;
	size_t storage_size;
	void *storage_offset;
	struct sub *rbh_root; /* root of the tree */
	SLIST_HEAD(,sub_word) word_list;
};

#ifdef __cplusplus
extern "C" {
#endif

struct sub_tree* subs_init(int words, int tot_word_len, int nchars);
void subs_register(struct bsearch_cache *, const char*);
void subs_free(struct bsearch_cache *);
int sub_match(const char *needle, const char *haystack, int type);
struct sub *sub_lookup(struct bsearch_cache *db, const char *sub);

#ifdef __cplusplus
}
#endif

#endif
