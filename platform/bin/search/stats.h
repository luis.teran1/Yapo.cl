#ifndef STATS_H
#define STATS_H

#ifdef __cplusplus
extern "C" {
#endif

struct lru;

void stat_inc(struct lru *c, const char *stat);
void stat_register(const char *stat);
void stat_print(int clear);

#ifdef __cplusplus
}
#endif

#endif
