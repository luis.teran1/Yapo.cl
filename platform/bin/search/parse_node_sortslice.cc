#include "parse_nodes.hh"
#include "search.h"
#include "qsort.h"

int
ParseNodeSortSlice::get_parse_string(char *buf, size_t buflen) {
	int nlen;
	int ilen;

	nlen = next->get_parse_string(buf, buflen);
	if (nlen < 0)
		return nlen;

	ilen = snprintf(buf + nlen, buflen - nlen, ";sl(%d,%d)", slices, sortspec);
	if (ilen < 0 || (size_t)nlen + ilen >= buflen)
		return -1;
	return nlen + ilen;
}

void
ParseNodeSortSlice::get_headers(struct buf_string *buf) {
	unsigned int i;
	unsigned int s = 0;
	int ss = next->get_sortspec();

	for (i = 0; i < slices; i++) {
		if (sizes[i].count == 0)
			continue;
		if ((ss & SORTSPEC_TYPE_MASK) == SORTSPEC_FLOAT) {
			if (i > 0) {
				bscat(buf, "info:slice:%d:boundary_lower:%f\n", s, sizes[i-1].f);
			}
			bscat(buf, "info:slice:%d:boundary_upper:%f\n", s, sizes[i].f);
		} else {
			if (i > 0) {
				bscat(buf, "info:slice:%d:boundary_lower:%d\n", s, sizes[i-1].i);
			}
			bscat(buf, "info:slice:%d:boundary_upper:%d\n", s, sizes[i].i);
		}
		bscat(buf, "info:slice:%d:count:%d\n", s, sizes[i].count);
		s++;
	}
}

void
ParseNodeSortSlice::finalize(struct result *res, struct timer_instance *ti) {
	unsigned int i;
	int child_sortspec;
	int docs_per_slice;

	next->finalize(res, ti);

	child_sortspec = next->get_sortspec();

	if ((child_sortspec & SORTSPEC_FIELD_MASK) != SORTSPEC_SORTVALUE) {
		log_printf(LOG_CRIT, "slice used when global sort not on suborder");
		return;
	}

	if (!res->ndocs)
		return;

	if (slices > res->ndocs)
		slices = res->ndocs;
	docs_per_slice = res->ndocs / slices;

	for (i = 0; i < slices - 1; i++) {
		if (child_sortspec & SORTSPEC_FLOAT) {
			sizes[i].f = res->sort_value_arr[docs_per_slice * (i + 1)].f;
		} else {
			sizes[i].i = res->sort_value_arr[docs_per_slice * (i + 1)].i;
		}
	}

	if (child_sortspec & SORTSPEC_FLOAT) {
		sizes[i].f = res->sort_value_arr[res->ndocs - 1].f;
	} else {
		sizes[i].i = res->sort_value_arr[res->ndocs - 1].i;
	}

	for (i = 0; i < res->ndocs; i++) {
		union sort_value sv = res->sort_value_arr[i];
		unsigned int j;
		for (j = 0; j < slices - 1; j++) {
			switch (child_sortspec & (SORTSPEC_DIR_MASK|SORTSPEC_TYPE_MASK)) {
			case SORTSPEC_INC|SORTSPEC_INT:
				if ((unsigned int)sv.i < sizes[j].i)
					goto found;
				break;
			case SORTSPEC_INC|SORTSPEC_FLOAT:
				if (sv.f < sizes[j].f)
					goto found;
				break;
			case SORTSPEC_DEC|SORTSPEC_INT:
				if ((unsigned int)sv.i > sizes[j].i)
					goto found;
				break;
			case SORTSPEC_DEC|SORTSPEC_FLOAT:
				if (sv.f > sizes[j].f)
					goto found;
				break;
			}
		}
found:
		sizes[j].count++;
		res->sort_value_arr[i].i = j;
	}

	sort_result(res, SORTSPEC_SORTVALUE | SORTSPEC_INT | (sortspec & SORTSPEC_DIR_MASK));
}
