#include "parse_nodes.hh"
#include "db.h"
#include "search.h"
#include "util.h"
#include "logging.h"

#include <assert.h>
#include <string.h>

#if 0
#define SEARCH_STAT(x) x
#else
#define SEARCH_STAT(x)
#endif

#define MAGIC_BFIND_RIGHT 2
#define MAGIC_BFIND_LEFT 5

/*
 * Quickly find the index for the doc in ads that is closest
 * to "max" (less than or equal).
 * This is very similar to a binary search. 
 */
#define V2DOC(vptr, index, stride) (*(const int32_t*)(const void*)((const char *)(vptr) + (index) * (stride)))
static int
bfind_w(const void *vptr, size_t vmembsz, int from, int to, int32_t max) {
	int32_t from_doc = V2DOC(vptr, from, vmembsz);
	int32_t to_doc = V2DOC(vptr, to, vmembsz);
	int32_t second_doc = V2DOC(vptr, from + 1, vmembsz);
	SEARCH_STAT(struct bsearch_t *search_data = pthread_getspecific(chatter_tsd));
	int rs = MAGIC_BFIND_RIGHT, ls = MAGIC_BFIND_LEFT;
	int cmp;
	int middle;
#if 0
	int interpolation = 0;
#endif

	assert(to >= from);

	/*
	 * It's almost always the first or second doc.
	 * Also catch the not found case.
	 */
	if (from_doc >= max) {
		return from;
	}
	if (to > from + 1 && from_doc < max && second_doc >= max) {
		return from + 1;
	}
	if (to_doc < max) {
		return -1;
	}

	cmp = 1;
	SEARCH_STAT(search_data->searches++);
	while (1) {
		int32_t middle_doc;
		SEARCH_STAT(search_data->search_loops++);

#if 0
		if (interpolation) {
			int32_t fd = V2DOC(vptr, from, vmembsz);
			int32_t td = V2DOC(vptr, to, vmembsz);
			long long orange = fd->order - td->order;
			long long arange = (to - from) + 1;
			long long fmdist = fd->order - max->order;
			long long mguess;

			if (orange != 0) {
				mguess = ((fmdist * arange) / orange) + 1;
			} else {
				/* Same order in array, go for the middle and give up interpolation.  */
				interpolation = 0;
				mguess = (to - from) / 2;
			}
			mguess += from;

			/*
			 * We end up with the guess being outside the range when we've hit a max between two elements. just clamp it and the rest will be taken care of.
			 * We could short-circuit here and just set from = to = mguess.
			 */
			if (mguess < from)
				mguess = from;
			else if (mguess > to)
				mguess = to;
			if (mguess == from || mguess == to) {
				/* If we guess one of the endpoints, try it once and then give up interpolation because badly distributed orders can make this regress to a linear search. */
				interpolation = 0;
			}
			middle = mguess;
		} else
#endif
		{
			if (cmp < 0) {
				middle = from + ((to - from) >> rs);
				if (rs > 1)
					rs--;
				SEARCH_STAT(search_data->search_right++);
			} else {
				middle = from + ((to - from) >> ls);
				if (ls > 1)
					ls--;
				SEARCH_STAT(search_data->search_left++);
			}
		}

		middle_doc = V2DOC(vptr, middle, vmembsz);
		cmp = middle_doc - max;

		if (cmp == 0) {
			return middle;
		}

		if (to == from) {
			if (cmp > 0) {
				return to;
			}
			return -1;
		}

		if (cmp < 0) {
			from = middle + 1;
		} else {
			if (from == middle)
				return middle;
			to = middle;
		}
	}
}
#undef V2DOC

ParseNode *
ParseNodeInvword::initialize(struct result *res) {
	if (!loaded) {
		if (!invword) {
			invword = find_invword(res->si->db, keyword);
			if (!invword)
				return new ParseNodeEmpty();
		}
		assert(invword->docs);
		loaded = 1;

		if (!invword->ndocs)
			return new ParseNodeEmpty();

		refs = invword->ndocs;
	}

	return NULL;
}

/* Invword */
int32_t
ParseNodeInvword::next_document(int32_t doc) {
	if (!loaded)
		return NULL_DOCOFF;
	if (!skip_extra) {
		if ((docptr = bfind_w(invword->docs, sizeof(*invword->docs), docptr, refs - 1, doc)) == -1) {
			return NULL_DOCOFF;
		} else {
			return invword->docs[docptr].docoff;
		}
	} else {
		const struct docindex *di;
		int32_t search = doc;

		/*
		 * Phrase and/or flag search, only return documents that has positions,
		 * and only those matching the flags.
		 */
		while ((docptr = bfind_w(invword->docs, sizeof(*invword->docs), docptr, refs - 1, search)) != -1) {
			const struct docpos *docpos;

			di = &invword->docs[docptr];
			if (di->ptr != INDEX_DOCINDEX_NO_POS) {
				docpos = &invword->docpos[di->ptr];
				if (docpos->pos) {
					if (pos_flags) {
						int i;

						/* Ok if at least one position matches the flags */
						for (i = 1; i <= docpos->pos; i++) {
							if ((docpos[i].flags & pos_flags) == pos_flags)
								return di->docoff;
						}
					} else
						return di->docoff;
				}
			}

			search = di->docoff;
			increment_doc(&search);
		}

		return NULL_DOCOFF;
	}
}

float
ParseNodeInvword::get_current_bm25(int ndocs, float k1) {
	const struct docindex *di = &invword->docs[docptr];
	const struct docpos *dp;
	int boost = 0;

	if (di->ptr != INDEX_DOCINDEX_NO_POS) {
		dp = &invword->docpos[di->ptr];
		/* Without boosts, just fall back to word count */
		boost = dp[0].rel_boost ?: dp[0].pos;
	}
	const float idf_floor = 0.00001;
	float tf = boost ?: 1;
	float n = invword->ndocs;
	float idf = log((ndocs - n + 0.5)/(n + 0.5));		/* XXX - this can be precalculated. */
	if (idf < idf_floor)
		idf = idf_floor;
#if 0
	fprintf(stderr, "pni:gcbm25: word: %s id: %d idf: %f tf: %f ndocs: %d n: %d\n", invword->word, invword->docs[docptr].doc.id, idf, tf, ndocs, invword->ndocs);
#endif
	return idf * tf / (k1 + tf);
}

int
ParseNodeInvword::get_parse_string(char *buf, size_t buflen) {
	size_t len = strlen(keyword);
	int slen;

	if (len >= buflen)
		return -1;
	if (strpbrk(keyword, " -")) {
		char *d;

		if ((d = strchr(keyword, ':'))) {
			if (len + 4 >= buflen)
				return -1;
			slen = snprintf(buf, buflen, "\"%.*s\":\"%s\"", (int)(d - keyword), keyword, d + 1);
			if (slen < 0 || (size_t)slen >= buflen)
				return -1;
			return slen;
		}
		if (len + 2 >= buflen)
			return -1;
		slen = snprintf(buf, buflen, "\"%s\"", keyword);
		if (slen < 0 || (size_t)slen >= buflen)
			return -1;
		return slen;
	}
	memcpy(buf, keyword, len + 1);
	return len;
}

void
ParseNodeInvattr::load(struct result *res) {
	loaded = 1;
	refs = invattr->ndocs;
}

ParseNode *
ParseNodeInvattr::initialize(struct result *res) {
	if (!loaded) {
		if (!invattr) {
			invattr = find_invattr(res->si->db, keyword);
			if (!invattr)
				return new ParseNodeEmpty();
		}

		load(res);

		if (!refs)
			return new ParseNodeEmpty();
	}

	return NULL;
}

int32_t
ParseNodeInvattr::next_document(int32_t doc) {
	if (!loaded)
		return NULL_DOCOFF;
	if ((docptr = bfind_w(invattr->docs, sizeof(*invattr->docs), docptr, refs - 1, doc)) == -1) {
		return NULL_DOCOFF;
	} else {
		return invattr->docs[docptr];
	}
}

int
ParseNodeInvattr::get_parse_string(char *buf, size_t buflen) {
	size_t len = strlen(keyword);

	if (len >= buflen)
		return -1;

	if (strchr(keyword, '-')) {
		const char *colon = strchr(keyword, ':');
		int l;
		if (colon)
			l = snprintf(buf, buflen, "%.*s:\"%s\"", (int)(colon - keyword), keyword, colon + 1);
		else
			l = snprintf(buf, buflen, "\"%s\"", keyword);
		if (l >= (int)buflen)
			return -1;
		return l;
	} else {
		memcpy(buf, keyword, len + 1);
	}
	return len;
}

bool
ParseNodeInvattr::combine(const char *kw, struct result *res) {
	char *combkey;
	const char *mc, *nc;
	struct invattr *ia;

	mc = strchr(keyword, ':');
	nc = strchr(kw, ':');

	if (!mc || !nc)
		return false;

	if (strcmp(keyword, kw) < 0) {
		xasprintf(&combkey, "%.*s_%.*s:%s_%s", (int)(mc - keyword), keyword, (int)(nc - kw), kw, mc + 1, nc + 1);
	} else {
		xasprintf(&combkey, "%.*s_%.*s:%s_%s", (int)(nc - kw), kw, (int)(mc - keyword), keyword, nc + 1, mc + 1);
	}
	ia = find_invattr(res->si->db, combkey);
	if (!ia) {
		free(combkey);
		return false;
	}

	free(keyword);
	keyword = combkey;
	invattr = ia;

	load(res);

	return true;
}

