
#include "parse_nodes.hh"
#include "search.h"

ParseNode *
ParseNodeMeta::initialize(struct result *res) {
	si = res->si;
	return NULL;
}

int
ParseNodeMeta::get_parse_string(char *buf, size_t buflen) {
	return -1;
}

void
ParseNodeMeta::get_headers(struct buf_string *buf) {
	if (requests & DOC_COUNT) {
		bscat(buf, "info:document_count:%d\n", db_ndocs(si->db));
	}
}
