#include <pthread.h>
#include <signal.h>
#include <dirent.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <sys/wait.h>
#include <stdio.h>
#include <errno.h>
#include <sys/time.h>

#include "search_lexer.h"
#include "search.h"
#include "logging.h"
#include "util.h"
#include "db.h"
#include "ps_status.h"
#include "stats.h"
#include "timer.h"
#include "hunstem.h"
#include "sock_util.h"
#include "command.h"
#include "engine.h"

static pid_t child_pid = 0;
static pid_t old_child_pid = 0;

int old_child_wait_time = 120;

/* Communication sockets for engines */
extern int child_sock;

/* For creating a new engine. */
static pid_t new_child_pid;
static int new_child_sock;

static void
sigchld(int sig) {
	/* Noop, just avoid it being SIG_IGN, since sigwait can't catch it then. */
}

/* Using alarm signal to make sure old child dies */
static void
alarm_signal(int sig) {
	alarm(0);

	if (old_child_pid && (kill(old_child_pid, 0) == 0)) {
		log_printf(LOG_ERR, "Old child (PID %d) is still alive after %d seconds! Will kill it.",
			(int)old_child_pid, old_child_wait_time);
		kill(old_child_pid, SIGABRT);
	}
	old_child_pid = 0;
}

/* 
 * swap_engine:
 * Reloads the datafiles into memory then waits for the server to become
 * inactive before switching the global pointers to the hash-tabels and
 * data-areas.
 * Cleans the old data from memory.
 */

static int
swap_engine(struct bconf_node *bconf_default, struct bconf_node *bconf_override) {
	int newpair[2];
	pid_t parentpid;

	set_ps_display("swapper swapping engine");

	log_printf(LOG_INFO, "swap_engine called");

	if (socketpair(AF_UNIX, SOCK_STREAM, 0, newpair)) {
		log_printf(LOG_CRIT, "Failed to create socketpair: %m");
		return 1;
	}

	parentpid = getpid();

	switch ((new_child_pid = fork())) {
	case -1:
		log_printf(LOG_CRIT, "Failed to fork: %m");
		return 1;
	case 0:
		close(newpair[0]);
		int res = run_engine(bconf_default, bconf_override, parentpid, newpair[1]);
		exit(res);
		/* Not reached */
	default:
		new_child_sock = newpair[0];
		close(newpair[1]);
		break;
	}

	return 0;
}

void
run_swapper(struct bconf_node *bconf_default, struct bconf_node *bconf_override) {
	sigset_t runset;
	sigset_t oldset;
	int sig;
	int stop = 0;
	int hup;

	sigfillset(&runset);
	sigdelset(&runset, SIGSEGV);
	sigdelset(&runset, SIGTRAP);
	sigdelset(&runset, SIGFPE);
	sigdelset(&runset, SIGILL);
	sigdelset(&runset, SIGABRT);
	sigdelset(&runset, SIGALRM);

	signal(SIGCHLD, sigchld);
	signal(SIGALRM, alarm_signal);

	child_sock = -1;

	sigprocmask(SIG_SETMASK, &runset, &oldset);
	while (!stop) {
		/* Start a new child. */
		swap_engine(bconf_default, bconf_override);
		if (!child_pid && !new_child_pid) {
			log_printf(LOG_CRIT, "No engine running");
			exit(1);
		}
		old_child_pid = child_pid;

		/* Wait for any signal to arrive and handle. */
		log_printf(LOG_INFO, "Waiting for signals");
		hup = 0;

		set_ps_display("swapper waiting for signal");

		while (!stop && !hup && sigwait(&runset, &sig) == 0) {
			log_printf(LOG_INFO, "Received signal %u", sig);
			switch (sig) {
			case SIGTERM:
			case SIGINT:
				stop = 1;
				break;
			case SIGHUP:
				if (new_child_pid) 
					log_printf(LOG_CRIT, "SIGHUP received but swapper is already running.");
				else
					hup = 1;
				break;
			case SIGCHLD:
			{
				int status;
				pid_t pid;

				while ((pid = waitpid(-1, &status, WNOHANG)) > 0) {
					if (pid == child_pid) {
						if (WIFEXITED(status)) {
							/* Child exited normally */
							log_printf(LOG_INFO, "Child %u exit status: %d", pid, WEXITSTATUS(status));
							if (WEXITSTATUS(status) == 0)
								exit(0);
						} else if (WIFSIGNALED(status)) {
							/* Child terminated by signal */
							log_printf(LOG_CRIT, "Child %u term signal: %d", pid, WTERMSIG(status));
						}
#ifdef WCOREDUMP
						if (WCOREDUMP(status)) {
							/* Child dumped core */
							log_printf(LOG_CRIT, "Child %u dumped core", pid);
						}
#endif
						if (new_child_pid) {
							log_printf(LOG_CRIT, "Current engine (%u) died while initializing new.", pid);
							/* The other child died, kill the new child and die. */
							kill(new_child_pid, SIGTERM);
						}
						exit(1);
					} else if (pid == new_child_pid) {
						/* Child failed to start with the new database, clean and keep running */
						if (WIFEXITED(status)) {
							/* Child exited normally */
							log_printf(LOG_CRIT, "New child %u exit status: %d", pid, WEXITSTATUS(status));
						} else if (WIFSIGNALED(status)) {
							/* Child terminated by signal */
							log_printf(LOG_CRIT, "New child %u term signal: %d", pid, WTERMSIG(status));
						}
#ifdef WCOREDUMP
						if (WCOREDUMP(status)) {
							/* Child dumped core */
							log_printf(LOG_CRIT, "New child %u dumped core", pid);
						}
#endif

						if (!old_child_pid) {
							/* This was the first child to be started. */
							log_printf(LOG_CRIT, "No children left");
							exit(1);
						}

						/* New socketpair is lost, keep old child_sock and close the new. */
						close(new_child_sock);
						new_child_sock = -1;
						new_child_pid = 0;
						old_child_pid = 0;
					} else {
						struct itimerval itv;

						log_printf(LOG_INFO, "Old child reaped");

						itv.it_value.tv_sec = 0;
						itv.it_value.tv_usec = 0;
						itv.it_interval.tv_sec = 0;
						itv.it_interval.tv_usec = 0;

						setitimer(ITIMER_REAL, &itv, NULL);
					}
				}
				break;
			}

			case SIGUSR2:
				if (new_child_pid) {
					/*
					 * Set child_sock to point to the new engine.
					 * The close will signal the old child to shut down.
					 */
					if (child_sock != -1)
						close(child_sock);
					child_sock = new_child_sock;
					new_child_sock = -1;

					/* Set an alarm to check that old child exited cleanly */
					if (old_child_pid) {
						struct itimerval itv;

						itv.it_value.tv_sec = old_child_wait_time;
						itv.it_value.tv_usec = 0;
						itv.it_interval.tv_sec = 0;
						itv.it_interval.tv_usec = 0;

						setitimer(ITIMER_REAL, &itv, NULL);
					}

					child_pid = new_child_pid;
					new_child_pid = 0;
					log_printf(LOG_INFO, "New main child: %u", child_pid);
				} else
					log_printf(LOG_CRIT, "SIGUSR2 received but no new child initializing");
				break;

			default:
				/* Unhandled signal. */
				log_printf(LOG_CRIT, "Reraising unhandled signal.");
				sigprocmask(SIG_SETMASK, &oldset, NULL);
				raise(sig);
				sigprocmask(SIG_SETMASK, &runset, &oldset);
			}
		}
	}

	/* Terminate child. */
	if (child_pid) {
		int status;
		char sbuf[256];

		log_printf(LOG_INFO, "Killing child: %u", child_pid);
		kill(child_pid, SIGTERM);
		close(child_sock);
		child_sock = -1;
		waitpid(child_pid, &status, 0);
		log_printf(LOG_INFO, "Reaped child %u status: %s", child_pid, strwait(status, sbuf, sizeof(sbuf)));
	}
	sigprocmask(SIG_SETMASK, &oldset, NULL);
}

