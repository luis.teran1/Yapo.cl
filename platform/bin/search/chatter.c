#include <sys/types.h>
#include <sys/poll.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <sys/signal.h>
#include <sys/time.h>
#include <sys/un.h>
#include <arpa/inet.h>
#include <semaphore.h>
#include <linux/unistd.h> /* for _syscall0 */
#include <assert.h>
#include <limits.h>

#include "search_lexer.h"
#include "util.h"
#include "search.h"
#include "db.h"
#include "logging.h"
#include "sock_util.h"
#include "stats.h"
#include "timer.h"
#include "charmap.h"
#include "chatter.h"
#include "text.h"
#include "engine.h"
#include "rand.h"
#include "strl.h"
#include "lru.h"
#include "bconf.h"
#include "hash.h"
#include "unicode.h"
#include "stat_counters.h"

#ifdef __NR_gettid
static pid_t gettid(void)
{
  return syscall(__NR_gettid);
}
#else
static pid_t gettid(void)
{
    return -ENOSYS;
}
#endif

pthread_key_t chatter_tsd;

/*
 * clear_chatter_data
 * Clears the query-data-structure.
 * This function is called after each query to the server.
 */
static void
clear_chatter_data(struct bsearch *data) {
	int i;
	struct counter_union_list_node *node;

	while ((node = data->query.counter_union_list.head) != NULL) {
		data->query.counter_union_list.head = node->next;
		free(node);
	}
	for (i = 0; i < data->query.conf_rev_size; i++) {
		free(data->query.conf_rev[i]);
		data->query.conf_rev[i] = NULL;
	}
	free(data->query.tag);
	free(data->query.error);
	memset(&data->query, 0, sizeof(data->query));
	data->query.limit = 100;

	data->result.max_order = 0;
	data->result.ndocs = 0;
	data->result.sub_ad_counter = 0;
	free(data->result.headers.buf);
	data->result.headers.buf = NULL;
	data->result.headers.pos = 0;
	data->result.headers.len = 0;
}

const char*
read_input(int fd, char *input, int input_len, int read_timeout_ms, int is_keepalive) {
	int n;

	stat_inc(NULL, "QUERY");

	stat_inc(NULL, "SEARCH");
	/* Read query. */
	if (read_timeout_ms) {
		struct pollfd pfd;
		int res;

		memset(&pfd, 0, sizeof(pfd));

		pfd.fd = fd;
		pfd.events = POLLIN | POLLERR | POLLHUP
#ifdef POLLRDHUP
				| POLLRDHUP
#endif
				;

		if ((res = poll(&pfd, 1, read_timeout_ms)) < 0) {
			if (errno == EINTR) {
				log_printf(LOG_WARNING, "EINTR on read");
				return NULL;
			} else {
				log_printf(LOG_CRIT, "Error on select (%m)");
				return NULL;
			}
		} else if (res == 0) {
			stat_inc(NULL, "TIMEOUT");
			log_printf(LOG_WARNING, "Timeout on read");
			return NULL;
		}
		if ((pfd.revents & POLLIN) == 0) {
			log_printf(LOG_DEBUG, "No POLLIN from poll.");
			return NULL;
		}
	}

	n = readline(fd, input, input_len);
	if (n == 0) {
		log_printf(LOG_DEBUG, "empty read");
		return NULL;
	}

	if (is_keepalive && n == input_len) {
		char dummy[1024] = {0};
		log_printf(LOG_DEBUG, "Possibly data left in pipe...draining");
		while (readline(fd, dummy, sizeof(dummy)) > 0)
			log_printf(LOG_DEBUG, "Drained: %s", dummy);
	}

	return input;
}

static char*
read_input_stdin(char *input, int input_len) {
	printf("$ ");
	if (!fgets(input, input_len, stdin)) {
		return NULL;
	}
	if (input[strlen(input) - 1] == '\n')
		input[strlen(input) - 1] = '\0';
	return input;
}

/*
 * Signal handlers
 *
 */
char crash_info[256];

static void
sig_handler(int signum) {
	sprintf(crash_info, "Sig %u received by %ld %u, terminating", signum, pthread_self(), gettid());
	signal(signum, SIG_DFL);
	abort();
}

static void
send_noresult(struct bsearch *data, const char *last_error, int fd) {
	struct iovec out[IOV_MAX];
	const char *dochdr;
	int niov = 0;
	char info_yyerror_buf[128];

	if (last_error) {
		out[niov].iov_base = info_yyerror_buf;
		out[niov].iov_len = snprintf(info_yyerror_buf, sizeof(info_yyerror_buf), "info:yyerror:%s\n", last_error);
		if (out[niov].iov_len > 0 && out[niov].iov_len < sizeof(info_yyerror_buf)) {
			++niov;
		}
	}

	const char* nolines = "info:lines:0\n";
	out[niov].iov_base = (void*)nolines;
	out[niov].iov_len = strlen(nolines);
	++niov;

	dochdr = get_doc_header(data->result.si->db);
	if (dochdr) {
		out[niov].iov_base = (void *)dochdr;
		out[niov].iov_len = strlen(dochdr);
		++niov;
	}
	out[niov].iov_base = (void *)"\n";
	out[niov].iov_len = 1;
	++niov;

	if (writev(fd, out, niov) == -1)
		log_printf(LOG_CRIT, "Failed to write lack of result. (%d)", niov);
}

static void
send_conf(struct bsearch *data, struct result *res, struct iovec *out, int *niov) {
	const char *conf_rev;
	int i;

	/* If the query doesn't request a configuration or there's none, don't send anything. */
	if (data->query.conf_rev_size == 0 || (conf_rev = get_conf_rev(res->si->db)) == NULL)
		return;

	/* Search for queried conf_rev, don't send configuration if found. */
	for (i = 0; i < data->query.conf_rev_size; i++) {
		if (strcmp(data->query.conf_rev[i], conf_rev) == 0)
			break;
	}
	if (i == data->query.conf_rev_size) {		/* Not found */
		if ((out[*niov].iov_base = (void *)get_config(res->si->db, &(out[*niov].iov_len))) != NULL)
			(*niov)++;
	}
        out[*niov].iov_base = (char *)"info:footprint:";
	out[*niov].iov_len = strlen("info:footprint:");
	(*niov)++;
	out[*niov].iov_base = (char *)conf_rev;
        out[*niov].iov_len = strlen(conf_rev);
        (*niov)++;
        out[*niov].iov_base = (char *)"\n";
	out[*niov].iov_len = 1;
        (*niov)++;
}

static inline int
sortspec_cmp(struct result *res, int32_t docoff_a, union sort_value sv_a, int32_t docoff_b) {
	const struct doc *docb = get_doc(res->si->db, docoff_b);
	switch (res->res_sortspec & (SORTSPEC_FIELD_MASK|SORTSPEC_DIR_MASK|SORTSPEC_TYPE_MASK)) {
	case SORTSPEC_SORTVALUE|SORTSPEC_INC|SORTSPEC_INT:
		return subsort_cmp_inc(docoff_a, sv_a.i, docoff_b, docb->suborder);
	case SORTSPEC_SORTVALUE|SORTSPEC_DEC|SORTSPEC_INT:
		return subsort_cmp_dec(docoff_a, sv_a.i, docoff_b, docb->suborder);
	case SORTSPEC_ORDER|SORTSPEC_INC|SORTSPEC_INT:
		return docoff_b - docoff_a;
	case SORTSPEC_SORTVALUE|SORTSPEC_INC|SORTSPEC_FLOAT:
		return subsort_cmp_inc_f(docoff_a, sv_a.f, docoff_b, docb->suborder_f);
	case SORTSPEC_SORTVALUE|SORTSPEC_DEC|SORTSPEC_FLOAT:
		return subsort_cmp_dec_f(docoff_a, sv_a.f, docoff_b, docb->suborder_f);
	default:
		log_printf(LOG_CRIT, "Unrecognized suborder 0x%x in sortspec_cmp, fix the code", res->res_sortspec);
		/* FALLTHROUGH */
	case SORTSPEC_NATURAL:
		return docoff_a - docoff_b;
	}
}

static inline int
sortspec_doc_cmp(struct result *res, int32_t docoff_a, union sort_value sv_a, const struct doc *docb) {
	int cmp;
	float cmpf;
	const struct doc *doca = get_doc(res->si->db, docoff_a);
	switch (res->res_sortspec & (SORTSPEC_FIELD_MASK|SORTSPEC_DIR_MASK|SORTSPEC_TYPE_MASK)) {
	case SORTSPEC_SORTVALUE|SORTSPEC_INC|SORTSPEC_INT:
		cmp = sv_a.i - docb->suborder;
		return cmp ?: index_doc_cmp(doca, docb);
	case SORTSPEC_SORTVALUE|SORTSPEC_DEC|SORTSPEC_INT:
		cmp = docb->suborder - sv_a.i;
		return cmp ?: index_doc_cmp(doca, docb);
	case SORTSPEC_ORDER|SORTSPEC_INC|SORTSPEC_INT:
		return -index_doc_cmp(doca, docb);
	case SORTSPEC_SORTVALUE|SORTSPEC_INC|SORTSPEC_FLOAT:
		cmpf = sv_a.f - docb->suborder_f;
		return cmpf > 0 ? 1 : cmpf < 0 ? -1 : index_doc_cmp(doca, docb);
	case SORTSPEC_SORTVALUE|SORTSPEC_DEC|SORTSPEC_FLOAT:
		cmpf = docb->suborder_f - sv_a.f;
		return cmpf > 0 ? 1 : cmpf < 0 ? -1 : index_doc_cmp(doca, docb);
	default:
		log_printf(LOG_CRIT, "Unrecognized suborder 0x%x in sortspec_doc_cmp, fix the code", res->res_sortspec);
		/* FALLTHROUGH */
	case SORTSPEC_NATURAL:
		return index_doc_cmp(doca, docb);
	}
}

static size_t
send_result(struct bsearch *data, struct result *res, int fd) {
	int32_t last_doc;
	struct iovec out[IOV_MAX];
	char info_to_id_buf[64];
	char info_sub_cnt_buf[64];
	char info_after_buf[64];
	char info_lines_buf[64];
	char info_idrange_before_buf[64];
	char info_idrange_after_buf[64];
	char info_paging_last[64];
	const char *to_id;
	int rand_reseed[64];
	int range_middle = -1;
	const char *dochdr;
	int niov = 0;
	unsigned int i;
	unsigned int lines;
	int first_line, last_line;
	size_t tot = 0;
	ssize_t w;
	int rel_debug = 0;

	if (data->query.rand_seed != 0) {
		rand_get(data->res_rand, rand_reseed, sizeof(rand_reseed));
		rand_set(data->res_rand, &data->query.rand_seed, sizeof(data->query.rand_seed));
		log_printf(LOG_DEBUG, "seeding random with %d", data->query.rand_seed);
	}

	send_conf(data, res, out, &niov);

	/* Send info:timestamp */
	const char * timestamp = get_timestamp(res->si->db);
	if (timestamp) {
		out[niov].iov_base = (char *)"info:timestamp:";
		out[niov].iov_len = strlen("info:timestamp:");
		niov++;
		out[niov].iov_base = (char *)timestamp;
		out[niov].iov_len = strlen(timestamp);
		niov++;
		out[niov].iov_base = (char *)"\n";
		out[niov].iov_len = 1;
		niov++;
	}

	if (res->headers.pos) {
		out[niov].iov_base = res->headers.buf;
		out[niov].iov_len = res->headers.pos;
		niov++;
	}

	if (data->query.idrange.middle && res->ndocs && (last_doc = load_docoff(res->si->db, data->query.idrange.middle)) != NULL_DOCOFF) {
		range_middle = -1;
		if (res->res_sortspec & SORTSPEC_ATTR) {
			for (unsigned int i = 0; i < res->ndocs; i++) {
				if (res->docoff_arr[i] == last_doc) {
					range_middle = i;
					break;
				}
			}
		} else {
			unsigned int middle, to, from;
			int cmp;

			from = 0;
			to = res->ndocs - 1;
			middle = (to - from) / 2;

			while (1) {
				cmp = sortspec_cmp(res, res->docoff_arr[middle], res->sort_value_arr[middle], last_doc);

				if (cmp == 0) {
					range_middle = middle;
					break;
				}

				if (to == from)
					break;

				if (cmp < 0) {
					from = middle + 1;
					middle = from + ((to - from) >> 2);
				} else {
					to = middle;
					middle = from + ((to - from) >> 5);
				}
			}
		}

		out[niov].iov_base = info_after_buf;
		out[niov].iov_len = snprintf(info_after_buf, sizeof(info_after_buf), "info:after_docs:%d\n", res->ndocs - (range_middle + 1));
		if (out[niov].iov_len > 0 && out[niov].iov_len < sizeof(info_after_buf)) {
			/* Paranoia. Only write the info header if the return from snprintf makes sense. */
			niov++;
		}
	}

	/* If sub_ad_counter have been incremented */
	if (res->sub_ad_counter) {
		out[niov].iov_base = info_sub_cnt_buf;
		out[niov].iov_len = snprintf(info_sub_cnt_buf, sizeof(info_sub_cnt_buf), "info:sub_ad_counter:%d\n", res->sub_ad_counter);
		if (out[niov].iov_len > 0 && out[niov].iov_len < sizeof(info_sub_cnt_buf)) {
			/* Paranoia. Only write the info header if the return from snprintf makes sense. */
			niov++;
		}
	}

	if ((to_id = get_index_to_id(res->si->db))) {
		out[niov].iov_base = info_to_id_buf;
		out[niov].iov_len = snprintf(info_to_id_buf, sizeof(info_to_id_buf), "info:to_id:%s\n", to_id);
		if (out[niov].iov_len > 0 && out[niov].iov_len < sizeof(info_to_id_buf)) {
			/* Paranoia. Only write the info header if the return from snprintf makes sense. */
			niov++;
		}
	}

	if (data->query.paging.enabled) {
		if (!res->ndocs) {
			first_line = -1;
			last_line = -1;
		} else if (data->query.paging.id == 0) {
			first_line = 0;
			last_line = data->query.limit - 1;
			if (last_line < first_line) {
				first_line = -1;
			}
			if ((unsigned int)last_line >= res->ndocs) {
				last_line = res->ndocs - 1;
			}
		} else {
			int middle, to, from;
			int cmp;
			struct doc d;
			/*
			 * paging.{id,order,suborder} is the doc  of the last document.
			 */
			d.id = data->query.paging.id;
			d.order = data->query.paging.order;
			d.suborder = data->query.paging.suborder;

			from = 0;
			to = res->ndocs - 1;
			middle = (to - from) / 2;

			while (1) {
				cmp = sortspec_doc_cmp(res, res->docoff_arr[middle], res->sort_value_arr[middle], &d);
				if (cmp == 0) {
					break;
				}

				if (to == from) {
					if (cmp < 0)
						middle = -1;
					break;
				}

				if (cmp < 0) {
					from = middle + 1;
					middle = from + ((to - from) >> 1);
				} else {
					to = middle;
					middle = from + ((to - from) >> 1);
				}
			}

			const struct doc *doc = middle == -1 ? NULL : get_doc(res->si->db, res->docoff_arr[middle]);
			if (middle == -1 || ((unsigned int)middle == res->ndocs - 1 && doc->id == d.id)) {
				first_line = -1;
				last_line = -1;
			} else {
				first_line = middle + (doc->id == d.id);
				last_line = first_line + data->query.limit -1;
				if ((unsigned int)last_line >= res->ndocs) {
					last_line = res->ndocs -1;
				}
			}
		}
		if (last_line != -1 && (unsigned int)last_line != res->ndocs -1) {
			const struct doc *ld = get_doc(res->si->db, res->docoff_arr[last_line]);
			int sv = ld->suborder;

			if (res->res_sortspec & SORTSPEC_SORTVALUE)
				sv = res->sort_value_arr[last_line].i;

			out[niov].iov_base = info_paging_last;
			out[niov].iov_len = snprintf(info_paging_last, sizeof(info_paging_last), "info:paging_last:%d,%d,%d\n", ld->id, ld->order, sv);
			if (out[niov].iov_len > 0 && out[niov].iov_len < sizeof(info_after_buf)) {
				/* Paranoia. Only write the info header if the return from snprintf makes sense. */
				niov++;
			}
		}
	} else if (data->query.idrange.middle) {
		if (range_middle == -1) {
			first_line = -1;
			last_line = -1;
		} else {
			first_line = range_middle - data->query.idrange.before;
			last_line = range_middle + data->query.idrange.after;

			if (first_line < 0) {
				first_line = 0;
			}

			if (last_line > 0 && (unsigned int)last_line >= res->ndocs) {
				last_line = res->ndocs - 1;
			}

			/* This can happen because idrange.before can be negative. */
			if ((unsigned int)first_line >= res->ndocs || last_line < first_line) {
				first_line = -1;
				last_line = -1;
			} else {
				out[niov].iov_base = info_idrange_before_buf;
				out[niov].iov_len = snprintf(info_idrange_before_buf, sizeof(info_idrange_before_buf), "info:idrange:before:%d\n", range_middle - first_line);
				if (out[niov].iov_len > 0 && out[niov].iov_len < sizeof(info_to_id_buf)) {
					niov++;
				}
				out[niov].iov_base = info_idrange_after_buf;
				out[niov].iov_len = snprintf(info_idrange_after_buf, sizeof(info_idrange_after_buf), "info:idrange:after:%d\n", last_line - range_middle);
				if (out[niov].iov_len > 0 && out[niov].iov_len < sizeof(info_to_id_buf)) {
					niov++;
				}
			}
		}
	} else {
		first_line = data->query.offset;
		last_line = data->query.offset + data->query.limit - 1;
		if ((unsigned int)first_line >= res->ndocs || first_line < 0 || last_line < first_line) {
			first_line = -1;
			last_line = -1;
		}
		if ((unsigned int)last_line >= res->ndocs) {
			last_line = res->ndocs - 1;
		}
	}

	if (first_line == -1) {
		lines = 0;
	} else {
		lines = last_line - first_line + 1;
	}

	out[niov].iov_base = info_lines_buf;
	out[niov].iov_len = snprintf(info_lines_buf, sizeof(info_lines_buf), "info:lines:%d\n", lines);
	if (out[niov].iov_len > 0 && out[niov].iov_len < sizeof(info_lines_buf)) {
		niov++;
	}

	dochdr = get_doc_header(res->si->db);
	if (dochdr) {
		if (rel_debug) {
			out[niov].iov_base = (char *)"rel\t";
			out[niov].iov_len = 4;
			niov++;
		}
		out[niov].iov_base = (void *)dochdr;
		out[niov].iov_len = strlen(dochdr);
		niov++;
	}
	out[niov].iov_base = (void *)"\n";
	out[niov].iov_len = 1;
	niov++;

	if (data->query.randomize == 1 && res->ndocs > data->query.limit * 2) {
		log_printf(LOG_DEBUG, "Randomizing doc_arr");
		/* Allocate an array of uint32_t doc ids, with the size query.limit */
		u_int32_t *idxs;
		idxs = alloca(data->query.limit * sizeof (int32_t)); 
		uint32_t mask = 1;
		while (mask < res->ndocs) {
			mask = mask << 1 | 1;
		}

		for (i = 0; i < data->query.limit; ++i) {
			unsigned int j;
			unsigned int idx;
			while (1) {
				while ((idx = rand_uint32(data->res_rand) & mask) >= res->ndocs) 
					;
				for (j = 0; j < i; j++)
					if (idxs[j] == idx)
						break;
				if (i==j)
					break;
			}
			
			idxs[i] = idx;
			const char *doc_data = load_doc_body(res->si->db, res->docoff_arr[idx]);
			if (doc_data) {
				if (rel_debug) {
					if (res->res_sortspec & SORTSPEC_FLOAT)
						ALLOCA_PRINTF(out[niov].iov_len, out[niov].iov_base, "%f\t", res->sort_value_arr[idx].f);
					else
						ALLOCA_PRINTF(out[niov].iov_len, out[niov].iov_base, "%d\t", res->sort_value_arr[idx].i);
					niov++;
				}
				out[niov].iov_base = (void *)doc_data;
				out[niov].iov_len = strlen(doc_data);
				niov++;
				out[niov].iov_base = (void *)"\n";
				out[niov].iov_len = 1;
				niov++;
				if (niov > IOV_MAX - 4) {
					if ((w = writev(fd, out, niov)) == -1)
						log_printf(LOG_CRIT, "Failed to write search result. (%d), %m", niov);
					tot += w;
					niov = 0;
				}
			} else {
				const struct doc *doc = get_doc(res->si->db, res->docoff_arr[idx]);
				log_printf(LOG_CRIT, "Failed to load ad: %u, %u, %d (%u)", 
						doc->id, doc->order, doc->suborder, i);
			}
		}
	} else {
		if (first_line != -1) {
			if (data->query.randomize == 1) {
				log_printf(LOG_DEBUG, "Shuffling doc_array with %d elements", res->ndocs);
				
				/* XXX need to keep sort_value_arr in sync if rel_debug */
				rand_shuffle(data->res_rand, res->docoff_arr, res->ndocs, sizeof(*res->docoff_arr));
			} 
			for (i = first_line; i <= (unsigned int)last_line; i++) {
				const char *doc_data = load_doc_body(res->si->db, res->docoff_arr[i]);
				if (doc_data) {
					if (rel_debug) {
						if (res->res_sortspec & SORTSPEC_FLOAT)
							ALLOCA_PRINTF(out[niov].iov_len, out[niov].iov_base, "%f\t", res->sort_value_arr[i].f);
						else
							ALLOCA_PRINTF(out[niov].iov_len, out[niov].iov_base, "%d\t", res->sort_value_arr[i].i);
						niov++;
					}
					out[niov].iov_base = (void *)doc_data;
					out[niov].iov_len = strlen(doc_data);
					niov++;
					out[niov].iov_base = (void *)"\n";
					out[niov].iov_len = 1;
					niov++;
					if (niov > IOV_MAX - 4) {
						if ((w = writev(fd, out, niov)) == -1)
							log_printf(LOG_CRIT, "Failed to write search result. (%d), %m", niov);
						tot += w;
						niov = 0;
					}
				} else {
					const struct doc *doc = get_doc(res->si->db, res->docoff_arr[i]);
					log_printf(LOG_CRIT, "Failed to load ad: %u, %u, %d (%u)", 
							doc->id, doc->order, doc->suborder, i);
				}
			}
		}
	}

	if ((w = writev(fd, out, niov)) == -1)
		log_printf(LOG_CRIT, "Failed to write search result. (%d), %m", niov);

	tot += w;

	/*
	 * If we polluted the randomness with a seed, restore it to something relatively sane.
	 */
	if (data->query.rand_seed != 0) {
		rand_set(data->res_rand, rand_reseed, sizeof(rand_reseed));
		data->query.rand_seed = 0;
	}

	return tot;
}

void
free_res(void *r) {
	struct result *res = (struct result *)r;

	free(res->headers.buf);
	free(res->docoff_arr);
	free(res->sort_value_arr);
	free(res);
}


static void
clean_query(char *dst, size_t dstsz, char *input, void *cfdata, struct syn_regexes *sr) {
	char *str;

	/* Clean up the query after the *:* terminator. Taking special care
	 * with the sub-operator '*'.
	 * * Remove single '*' chars.
	 * * Remove all multiple '*' chars.
	 *
	 * Remove commas and periods not in words.
	 */
	while (input) {
		unsigned char last = ' ';
		unsigned char next;
		unsigned char *s;
		char *new_str;
		char *str1, *str2;

		str1 = strstr(input, "*:*");
		str2 = strstr(input, "*-*");

		if (!str1)
			str = str2;
		else if (!str2)
			str = str1;
		else
			str = str1 < str2 ? str1 : str2;

		if (!str || (size_t)(str - input) >= dstsz) {
			strlcpy(dst, input, dstsz);
			break;
		}

		str += 3;
		memcpy(dst, input, str - input);
		dst += str - input;
		dstsz -= str - input;

		input = strchr(str, '\003');
		if (input)
			*input = '\0';

		casefold(dst, dstsz, str, cfdata, 0);
		s = (unsigned char*)dst;

		while (*s) {
			if (*s == '*') {
				next = *(s + 1) ? *(s + 1) == '*' ? '*' : *(s + 1) : ' ';
				if (next == '*') {
					last = '*';
					*s++ = ' ';
					continue;
				}

				if (last == '*') {
					*s++ = ' ';
					continue;
				}

				if (last == ' ' && next == ' ')
					*s = ' ';
				else if (last != ' ' && next != ' ')
					*s = ' ';
			} else {
				if (*s == '.' || *s == ',') {
					/* Ignore commas and periods at the beginning/end of the query/individual words */
					if (last == ' ' || *(s + 1) == ' ' || (char*)(s + 1) == input || !*(s + 1))
						*s = ' ';
				} else if (*s == '-') {
					/* Dashes are not allowed at end of word since they make hunstem freak out. */
					if (*(s + 1) == '-' || *(s + 1) == ' ' || (char*)(s + 1) == input || !*(s + 1))
						*s = ' ';
				}
			}

			last = *s;
			if (last == '-' || last == '"') last = ' ';
			s++;
		}

		if ((new_str = syn_regex_update(sr, dst, cfdata, NULL, NULL))) {
			strlcpy(dst, new_str, dstsz);
			free(new_str);
			if (input)
				s = (unsigned char *)dst + strlen(dst);
		}

		if (input) {
			*input = '\003';
			dstsz -= (char*)s - dst;
			dst = (char*)s;
		}
	}
}

void
chatter_done(struct bsearch *chatter_data, void *exitptr) {
	rand_close(chatter_data->res_rand);
	free(chatter_data->result.docoff_arr);
	free(chatter_data->result.sort_value_arr);
	free(chatter_data);
	pthread_exit(exitptr);
}

STAT_COUNTER_DECLARE(query_counter, "search", "queries");

void *
chatter_thread(void *thread_data) {
	char input[ENGINE_INPUT_BUFSZ];
	char input_clean[sizeof(input) * 2];
	struct bsearch *chatter_data = zmalloc(sizeof (*chatter_data));
	struct timespec ts;
	struct timer_instance *query_timer;
	struct timer_instance *ti;
	int exec_time;
	void *cfdata = NULL;
	ParseNode *node;
	struct search_conn conn = { };

	chatter_data->res_rand = rand_open();
	assert(chatter_data->res_rand);

	clear_chatter_data(chatter_data);
	chatter_data->result.docoff_arr_sz = 100000;
	chatter_data->result.docoff_arr = xmalloc(chatter_data->result.docoff_arr_sz * sizeof(*chatter_data->result.docoff_arr));
	chatter_data->result.sort_value_arr = xmalloc(chatter_data->result.docoff_arr_sz * sizeof(*chatter_data->result.sort_value_arr));

	log_register_thread("Chatter %d (parent %d)", (int)gettid(), (int)getpid());

	log_printf(LOG_DEBUG, "Starting chatter thread");

	signal(SIGFPE, sig_handler);
	signal(SIGSEGV, sig_handler);
	signal(SIGTRAP, sig_handler);

	while (1) {
		char client_address[NI_MAXHOST];
		struct sockaddr_storage sas;
		struct search_instance *si;
		socklen_t salen = sizeof(sas);
		size_t tot_bytes = 0;
		int doexit = 0;
		int one = 1;
		int res;
		int fd;

		fd = wait_for_fd(thread_data, &conn);
		if (fd == -1) {
			if (cfdata)
				free_charmap_thread(cfdata);
			chatter_done(chatter_data, NULL);
		}

		si = chatter_data->result.si = conn.si;
		if (cfdata)
			cfdata = reinit_charmap_thread(si->sic->charmap, cfdata);
		else
			cfdata = init_charmap_thread(si->sic->charmap);

		if (getpeername(fd, (struct sockaddr *)&sas, &salen) == 0) {
			getnameinfo((struct sockaddr *)&sas, salen, client_address, 
				    sizeof(client_address), NULL, 0, NI_NUMERICHOST);
		}

		setsockopt(fd, IPPROTO_TCP, TCP_NODELAY, &one, sizeof (one));

		/*
		 * No need to have a read timeout on the keepalive socket since we've already been woken by a poll.
		 */
		if (!read_input(fd, input, sizeof(input), conn.read_to_ms, conn.is_keepalive)) {
			log_printf(LOG_DEBUG, "Got null result from read_input from %s, shutting down fd %d ", client_address, fd);
			release_fd(thread_data, 1);
			goto out_nosock;
		}

		if (db_cache_lock(si->db)) {
			doexit = 1;
			goto out_nolock;
		}

		STATCNT_INC(&query_counter);
		query_timer = timer_start(NULL, "query");

		/* Simulate slow search */
		if (!strcmp(input, "regress_delay"))
			sleep(2);

		if (!strncmp(input, "regress_conf_rev:", sizeof("regress_conf_rev:") - 1)) {
			debug_set_conf_rev(chatter_data->result.si->db, input + sizeof("regress_conf_rev:") - 1);
		}

		log_printf(LOG_DEBUG, "Search (%s) from %s", input, client_address);
		/* Search for ads */

		ti = timer_start(query_timer, "parse_query");
		clean_query(input_clean, sizeof(input_clean), input, cfdata, si->sic->syn_regexes);
		log_printf(LOG_DEBUG, "Cleaned query (%s)", input_clean);
		res = parse(si, &chatter_data->query, input_clean, cfdata);
		if (res == 0) {
		       	if (chatter_data->query.tag) {
				timer_add_attribute(query_timer, chatter_data->query.tag);
			}

			timer_handover(ti, "main_search");
			node = main_search(&chatter_data->result, chatter_data->query.query, ti, chatter_data->query.print_parse, (void (*)(void*))release_reserve, thread_data);
			timer_handover(ti, "send_result");
			tot_bytes = send_result(chatter_data, &chatter_data->result, fd);
			timer_add_counter(query_timer, tot_bytes);
			timer_handover(ti, "cleanup");
			cleanup_search(node);
			timer_end(ti, NULL);

			/* Log empty results */
			if (!chatter_data->result.ndocs) {
#ifdef LOG_EMPTY_SEARCHES
				log_printf(LOG_INFO, "Empty search: \"%s\"", input);
#endif
				stat_inc(NULL, "EMPTY");
			}
		} else {
			/* Parse failed */
			timer_handover(ti, "no_result");
			log_printf(LOG_CRIT, "Parse failed (%s) from %s", input, client_address);
			send_noresult(chatter_data, chatter_data->query.error, fd);
			timer_end(ti, NULL);
		}

		timer_end(query_timer, &ts);
		exec_time = ts.tv_sec * 1000000 + ts.tv_nsec / 1000;
		/* Log slow queries */
		if (si->sic->slow_query_log && exec_time > si->sic->slow_query_log) {
			log_printf(LOG_INFO, "Slow query (%uus) (%lu) (%s): \"%s\"", exec_time, (unsigned long)tot_bytes, chatter_data->query.tag ?: "", input);
			stat_inc(NULL, "SLOW");
		}

		db_cache_unlock(si->db);
out_nolock:
		release_fd(thread_data, 0);
out_nosock:
		clear_chatter_data(chatter_data);

		if (doexit) {
			free_charmap_thread(cfdata);
			log_printf(LOG_INFO, "Terminating thread");
			chatter_done(chatter_data, NULL);
		}
	}
	return NULL;
}

void *
chatter_stdin(struct search_instance *si) {
	char input[ENGINE_INPUT_BUFSZ];
	char input_clean[sizeof(input) * 2];
	struct bsearch *chatter_data = (struct bsearch *)zmalloc(sizeof (*chatter_data));
	struct timer_instance *ti;
	void *cfdata;
	ParseNode *node;

	chatter_data->res_rand = rand_open();
	assert(chatter_data->res_rand);
	cfdata = init_charmap_thread(si->sic->charmap);

	clear_chatter_data(chatter_data);
	chatter_data->result.docoff_arr_sz = db_ndocs(si->db);
	chatter_data->result.docoff_arr = xmalloc(chatter_data->result.docoff_arr_sz * sizeof(*chatter_data->result.docoff_arr));
	chatter_data->result.sort_value_arr = xmalloc(chatter_data->result.docoff_arr_sz * sizeof(*chatter_data->result.sort_value_arr));
	chatter_data->result.headers = (struct buf_string){NULL};

	chatter_data->result.si = si;

	log_printf(LOG_DEBUG, "Starting chatter thread");

	signal(SIGFPE, sig_handler);
	signal(SIGSEGV, sig_handler);
	signal(SIGTRAP, sig_handler);

	while (1) {
		const char *str;
		int res;

		str = read_input_stdin(input, sizeof(input));
		if (!str || strcmp(str, "quit") == 0)
			break;

		/* Simulate slow search */
		if (strcmp(input, "regress_delay") == 0)
			sleep(2);

		log_printf(LOG_DEBUG, "Search (%s)", input);
		/* Search for ads */

		clean_query(input_clean, sizeof(input_clean), input, cfdata, si->sic->syn_regexes);
		res = parse(si, &chatter_data->query, input_clean, cfdata);
		if (res == 0) {
			ti = timer_start(NULL, "stdin_search");
			node = main_search(&chatter_data->result, chatter_data->query.query, ti, chatter_data->query.print_parse, NULL, NULL);
			cleanup_search(node);
			timer_end(ti, NULL);
			send_result(chatter_data, &chatter_data->result, STDOUT_FILENO);
		} else {
			/* Parse failed */
			log_printf(LOG_CRIT, "Parse failed (%s) from stdin", input_clean);
			send_noresult(chatter_data, chatter_data->query.error, STDOUT_FILENO);
		}
		clear_chatter_data(chatter_data);
	}
	free_charmap_thread(cfdata);

	return NULL;
}

