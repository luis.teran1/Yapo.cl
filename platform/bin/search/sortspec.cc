
#include "search.h"
#include "sortspec.h"
#include "logging.h"

#define _QSORT_SWAP(a, b, t) subsort_swap((a), (b), &(t), res->docoff_arr, res->sort_value_arr)
#define _QSORT_MOVE(a, b) ((void)(*a = *b, res->sort_value_arr[a - res->docoff_arr] = res->sort_value_arr[b - res->docoff_arr]))
#define _QSORT_TMPSTORE(t, a) ((void)(t = *a, tmpsv = res->sort_value_arr[a - res->docoff_arr]))
#define _QSORT_TMPLOAD(a, t) ((void)(*a = t, res->sort_value_arr[a - res->docoff_arr] = tmpsv))
#include "qsort.h"

static inline void
subsort_swap(int32_t *a, int32_t *b, int32_t *t, int32_t *base, union sort_value *sort_value_arr) {
	union sort_value st;
	*t = *a;
	*a = *b;
	*b = *t;
	st = sort_value_arr[a - base];
	sort_value_arr[a - base] = sort_value_arr[b - base];
	sort_value_arr[b - base] = st;
}

static inline int
qsubsort_int_inc(int32_t *da, int32_t *db, int32_t *base, union sort_value *sort_value_arr) {
	return subsort_cmp_inc(*da, sort_value_arr[da - base].i, *db, sort_value_arr[db - base].i) < 0;
}
#define qsubsort_int_inc_call(da, db) (qsubsort_int_inc((da), (db), res->docoff_arr, res->sort_value_arr))

static inline int
qsubsort_int_dec(int32_t *da, int32_t *db, int32_t *base, union sort_value *sort_value_arr) {
	return subsort_cmp_dec(*da, sort_value_arr[da - base].i, *db, sort_value_arr[db - base].i) < 0;
}
#define qsubsort_int_dec_call(da, db) (qsubsort_int_dec((da), (db), res->docoff_arr, res->sort_value_arr))

static inline int
qsubsort_float_inc(int32_t *da, int32_t *db, int32_t *base, union sort_value *sort_value_arr) {
	return subsort_cmp_inc_f(*da, sort_value_arr[da - base].f, *db, sort_value_arr[db - base].f) < 0;
}
#define qsubsort_float_inc_call(da, db) (qsubsort_float_inc((da), (db), res->docoff_arr, res->sort_value_arr))

static inline int
qsubsort_float_dec(int32_t *da, int32_t *db, int32_t *base, union sort_value *sort_value_arr) {
	return subsort_cmp_dec_f(*da, sort_value_arr[da - base].f, *db, sort_value_arr[db - base].f) < 0;
}
#define qsubsort_float_dec_call(da, db) (qsubsort_float_dec((da), (db), res->docoff_arr, res->sort_value_arr))

void
sort_result(struct result *res, int sortspec) {
	char buf[256];
	log_printf(LOG_DEBUG, "Sorting by %s", str_sortspec(sortspec, buf, sizeof(buf)));

	union sort_value tmpsv;
	switch (sortspec & (SORTSPEC_FIELD_MASK|SORTSPEC_DIR_MASK|SORTSPEC_TYPE_MASK)) {
	case SORTSPEC_SORTVALUE|SORTSPEC_INC|SORTSPEC_INT:
		QSORT(int32_t, res->docoff_arr, res->ndocs, qsubsort_int_inc_call);
		break;
	case SORTSPEC_SORTVALUE|SORTSPEC_DEC|SORTSPEC_INT:
		QSORT(int32_t, res->docoff_arr, res->ndocs, qsubsort_int_dec_call);
		break;
	case SORTSPEC_ORDER|SORTSPEC_INC|SORTSPEC_INT:
		{
			/* Reverse the array. */
			int32_t *a = res->docoff_arr;
			int32_t *b = res->docoff_arr + res->ndocs - 1;
			while (a < b) {
				int32_t tmp = *a;
				*a++ = *b;
				*b-- = tmp;
			}
		}
		break;
	case SORTSPEC_SORTVALUE|SORTSPEC_INC|SORTSPEC_FLOAT:
		QSORT(int32_t, res->docoff_arr, res->ndocs, qsubsort_float_inc_call);
		break;
	case SORTSPEC_SORTVALUE|SORTSPEC_DEC|SORTSPEC_FLOAT:
		QSORT(int32_t, res->docoff_arr, res->ndocs, qsubsort_float_dec_call);
		break;
	case SORTSPEC_NATURAL:
		/* This is the default ordering. */
		break;
	default:
		log_printf(LOG_CRIT, "Unexpected sortspec in sort_result (0x%X)", sortspec);
		break;
	}
}

const char *
str_sortspec(int sortspec, char *buf, size_t buflen) {
	const char *d_type = "*";
	const char *d_order = "*";
	const char *d_incdec = "*";

	switch (sortspec & SORTSPEC_TYPE_MASK) {
	case SORTSPEC_INT:
		d_type = "integer";
		break;
	case SORTSPEC_FLOAT:
		d_type = "float";
		break;
	}
	switch (sortspec & SORTSPEC_METHOD_MASK) {
	case SORTSPEC_SORTVALUE:
		d_order = "sortvalue";
		break;
	case SORTSPEC_ORDER:
		d_order = "order";
		break;
	case SORTSPEC_ATTR:
		d_order = "attribute";
		break;
	}
	switch (sortspec & SORTSPEC_DIR_MASK) {
	case SORTSPEC_INC:
		d_incdec = "increasing";
		break;
	case SORTSPEC_DEC:
		d_incdec = "decreasing";
		break;
	}

	snprintf(buf, buflen, "%s %s %s", d_type, d_order, d_incdec);
	return buf;
}
