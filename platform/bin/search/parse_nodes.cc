#include "parse_nodes.hh"
#include "logging.h"
#include "util.h"
#include "db.h"
#include "search.h"
#include <string.h>
#include <stdio.h>

/* Counter */
int32_t
ParseNodeCounter::next_document(int32_t doc) {
	struct counter_union_list_node **phead = NULL;

	if (union_list) {
		/* Remove all docs in union_list that we have already passed in our searching. */
		phead = &union_list->head;

		/* Always keep all subads, since they need to be "bitmap" checked. */
		while (*phead && docoff_is_subdoc(res->si->db, (*phead)->doc))
			phead = &(*phead)->next;

		while (*phead && (*phead)->doc < doc) {
			struct counter_union_list_node *node = *phead;

			*phead = node->next;
			free(node);
		}
	}

	int32_t d = next->get_next_document(doc);
	if (d != NULL_DOCOFF && count_node) {
		struct counter_union_list_node *pnode = NULL;

		if (union_list) {
			struct counter_union_list_node *node = NULL;
			int r = 1;

			for (node = *phead ; node && (r = node->doc - d) < 0 ; pnode = node, node = node->next)
				;
			if (r == 0) {
				/* doc is already counted. */
				return d;
			}
		}

		int32_t c = count_node->get_next_document(d);

		if (c != NULL_DOCOFF) {
			if (d == c) {
				int did_count = 1;

				if (docoff_is_subdoc(res->si->db, d)) {
					const struct doc *doc = get_doc(res->si->db, d);
					if (doc->suborder != inserted_parent) {
						if (union_list) {
							struct counter_union_list_node *node;

							/* Go through all list to check if the parent of the subad is already counted. */
							for (node = union_list->head ; node ; node = node->next) {
								if (docoff_is_subdoc(res->si->db, node->doc)) {
									const struct doc *ndoc = get_doc(res->si->db, node->doc);
									if (ndoc->suborder == doc->suborder)
										break;
								}
							}
							if (!node) {
								count++;
								inserted_parent = doc->suborder;
							} else
								did_count = 0;
						} else {
							count++;
							inserted_parent = doc->suborder;
						}
					} else
						did_count = 0;
				} else { 
					count++;
				}
				if (did_count && union_list) {
					/* Insert sorted into list. Fortunately, pnode already points to correct location. */
					struct counter_union_list_node *node = (struct counter_union_list_node*)xmalloc (sizeof (*node));

					node->doc = d;
					if (pnode) {
						node->next = pnode->next;
						pnode->next = node;
					} else {
						node->next = *phead;
						*phead = node;
					}
				}
			}
		} else {
			/* Exhausted count document list. */
			delete count_node;
			count_node = NULL;
		}
	}

	return d;
}

int
ParseNodeCounter::get_parse_string(char *buf, size_t buflen) {
	int nlen;
	int clen;
	size_t tlen;

	nlen = next->get_parse_string(buf, buflen);
	if (nlen < 0)
		return nlen;
	buf[nlen++] = ',';
	if ((size_t)nlen >= buflen)
		return -1;
	clen = count_node->get_parse_string(buf + nlen, buflen - nlen);
	if (clen < 0)
		return clen;
	nlen += clen;

	if (tag)
		tlen = strlen(tag) + 2;
	else
		tlen = 0;

	if ((size_t)nlen + 2 + tlen >= buflen)
		return -1;
	if (tag)
		snprintf(buf + nlen, buflen - nlen, ",c(%s)", tag);
	else
		snprintf(buf + nlen, buflen - nlen, ",c");
	return nlen + 2 + tlen;
}

void
ParseNodeCounter::get_headers(struct buf_string *buf) {
	if(count > 0 || print_zeros)
		bufcat(&buf->buf, &buf->len, &buf->pos, "info:%s:%d\n", tag ? tag : "counter", count);

	next->get_headers(buf);
}

/* Count all */
int32_t
ParseNodeCountAll::next_document(int32_t doc) {
	int32_t d = ParseNodeNext::next_document(doc);
	if (d != NULL_DOCOFF) {
		/*
		 * Here we do the same as in perform_search, namely only insert the parent ad once when we encounter a sub ad.
		 * We do this by comparing suborder to inserted_parent, and if it differs, increment the counter and store the suborder into inserted parent
		 * If suborder isn't negative, just increment.
		 */
		if (docoff_is_subdoc(res->si->db, d)) {
			const struct doc *doc = get_doc(res->si->db, d);
			if (doc->suborder != inserted_parent) {
				count++;
				inserted_parent = doc->suborder;
			}
		} else {
			count++;
		}
	}
	return d;
}

int
ParseNodeCountAll::get_parse_string(char *buf, size_t buflen) {
	int nlen = next->get_parse_string(buf, buflen);
	if (nlen < 0)
		return nlen;

	/* filtered is added transparently */
	if (strcmp(header, "filtered") == 0)
		return nlen;

	int res = snprintf(buf + nlen, buflen - nlen, ",ca(%s)", header);
	if (res < 0 || (size_t)res >= buflen - nlen)
		return -1;

	return res + nlen;
}

void
ParseNodeCountAll::get_headers(struct buf_string *buf) {
	bufcat(&buf->buf, &buf->len, &buf->pos, "info:%s:%d\n", header, count);
	next->get_headers(buf);
}

/* Filter */
int32_t
ParseNodeFilter::next_document(int32_t doc) {
	int32_t d = doc;
	int32_t search;
	
	if (negated && !filter_node)
		return next->get_next_document(doc);

	for (search = d; (d = next->get_next_document(search)) != NULL_DOCOFF; search = d, increment_doc(&search)) {
		int32_t c = filter_node->get_next_document(d);

		if (c != NULL_DOCOFF) {
			if (negated ? d != c : d == c) {
				return d;
			}
		} else if (negated) {
			delete filter_node;
			filter_node = NULL;
			return d;
		} else {
			/* Filter is empty. Exhaust the document list (for counters). */
			do {
				search = d;
				increment_doc(&search);
			} while ((d = next->get_next_document(search)) != NULL_DOCOFF);

			return NULL_DOCOFF;
		}
	}

	return NULL_DOCOFF;
}

int
ParseNodeFilter::get_parse_string(char *buf, size_t buflen) {
	int nlen;
	int flen;

	nlen = next->get_parse_string(buf, buflen);
	if (nlen < 0)
		return nlen;
	buf[nlen++] = ',';
	if ((size_t)nlen >= buflen)
		return -1;
	flen = filter_node->get_parse_string(buf + nlen, buflen - nlen);
	if (flen < 0)
		return flen;
	nlen += flen;
	if ((size_t)nlen + 3 >= buflen)
		return -1;
	if (negated) {
		strcpy(buf + nlen, ",!f");
		return nlen + 3;
	} else {
		strcpy(buf + nlen, ",f");
		return nlen + 2;
	}
}

/* Minus */
int32_t
ParseNodeMinus::next_document(int32_t doc) {
	if (!minus_node)
		return next->get_next_document(doc);

	int32_t d;
	int32_t search = doc;
	while ((d = next->get_next_document(search)) != NULL_DOCOFF) {
		int32_t c = minus_node->get_next_document(d);

		if (c == NULL_DOCOFF) {
			delete minus_node;
			minus_node = NULL;
			return d;
		}
		if (d != c) {
			return d;
		}

		search = d;
		increment_doc(&search);
	}

	return NULL_DOCOFF;
}

int
ParseNodeMinus::get_parse_string(char *buf, size_t buflen) {
	int nlen;
	int mlen;

	nlen = next->get_parse_string(buf, buflen);
	if (nlen < 0)
		return nlen;
	buf[nlen++] = ',';
	if ((size_t)nlen >= buflen)
		return -1;
	mlen = minus_node->get_parse_string(buf + nlen, buflen - nlen);
	if (mlen < 0)
		return mlen;
	nlen += mlen;
	if ((size_t)mlen + 2 >= buflen)
		return -1;
	strcpy(buf + nlen, ",m");
	return nlen + 2;
}

