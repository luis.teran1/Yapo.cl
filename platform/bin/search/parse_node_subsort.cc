
#define __STDC_LIMIT_MACROS
#include "parse_nodes.hh"
#include "parse_node_cache.hh"
#include "util.h"
#include "logging.h"
#include "search.h"
#include <stdio.h>
#include <stdint.h>
#include <assert.h>

void
ParseNodeSubsort::finalize(struct result *res, struct timer_instance *ti) {
	next->finalize(res, ti);

	sort_result(res, sortspec);
}

int
ParseNodeSubsort::get_parse_string(char *buf, size_t buflen) {
	char sortsign = '*';
	char sortempty = '*';
	int nlen;
	int ilen;

	nlen = next->get_parse_string(buf, buflen);
	if (nlen < 0)
		return nlen;

	switch (sortspec & (SORTSPEC_DIR_MASK|SORTSPEC_FIELD_MASK)) {
	case SORTSPEC_SORTVALUE|SORTSPEC_INC:
		sortsign = '+';
		break;
	case SORTSPEC_SORTVALUE|SORTSPEC_DEC:
		sortsign = '-';
		break;
	case SORTSPEC_ORDER|SORTSPEC_INC:
		sortsign = '>';
		break;
	case SORTSPEC_ORDER|SORTSPEC_DEC:
		sortsign = '<';
		break;
	}
	switch (sortspec & SORTSPEC_EMPTY_MASK) {
	case SORTSPEC_EMPTY_LAST:
		sortempty = '+';
		break;
	case SORTSPEC_EMPTY_FIRST:
		sortempty = '-';
	default:
		break;
	}

	switch (sortspec & SORTSPEC_METHOD_MASK) {
	case SORTSPEC_ATTR:
		if (buflen - nlen < 2)
			return -1;
		buf[nlen++] = ',';

		ilen = sortnode->get_parse_string(buf + nlen, buflen - nlen);
		if (ilen < 0 || (size_t)nlen + ilen >= buflen)
			return -1;
		nlen += ilen;

		ilen = snprintf(buf + nlen, buflen - nlen, ",%c%c;so", sortsign, sortempty);
		if (ilen < 0 || (size_t)nlen + ilen >= buflen)
			return -1;
		break;
	default:
		ilen = snprintf(buf + nlen, buflen - nlen, ",%c;so", sortsign);
		if (ilen < 0 || (size_t)nlen + ilen >= buflen)
			return -1;
	}
	return nlen + ilen;
}

ParseNode *
ParseNodeSubsort::initialize(struct result *res) {
	ParseNode *n = ParseNodeNext::initialize(res);
	if (n)
		return n;

	this->res = res;
	if (sortnode) {
		switch (sortspec & (SORTSPEC_DIR_MASK|SORTSPEC_EMPTY_MASK)) {
		case SORTSPEC_INC|SORTSPEC_EMPTY_LAST:
		case SORTSPEC_DEC|SORTSPEC_EMPTY_FIRST:
			null_value = INT32_MAX;
			break;
		case SORTSPEC_DEC|SORTSPEC_EMPTY_LAST:
		case SORTSPEC_INC|SORTSPEC_EMPTY_FIRST:
			null_value = 0;
			break;
		}
		
		sortnode->initialize(res);
	}
	return NULL;
}

int32_t
ParseNodeSubsort::get_attrsort_value(int32_t docoff) {
	if (!sortnode) {
		return null_value;
	}

	int32_t d = sortnode->get_next_document(docoff);

	if (d == NULL_DOCOFF) {
		delete sortnode;
		sortnode = NULL;
		/*log_printf(LOG_DEBUG, "sort node exhausted for docoff:%d", d);*/
		return null_value;
	}

	if (d == docoff) {
		int32_t v = sortnode->current_sort_value().i;
		/*log_printf(LOG_DEBUG, "sort value %d found for docoff:%d", v, d);*/
		return v;
	} else {
		/*log_printf(LOG_DEBUG, "sort value not found for docoff:%d", d);*/
		return null_value;
	}
}

void
ParseNodeSubsort::load_sort_value(int32_t docoff) {
	int32_t sortval;

	if ((sortspec & SORTSPEC_ATTR) != SORTSPEC_ATTR) {
		const struct doc *doc = get_doc(res->si->db, docoff);
		sortval = doc->suborder;
	} else {
		sortval = get_attrsort_value(docoff);
	}

	res->sort_value_arr[res->ndocs].i = sortval;
}

int32_t
ParseNodeSubsort::next_document(int32_t doc)
{
	doc = next->get_next_document(doc);
	if (doc != NULL_DOCOFF && !docoff_is_subdoc(res->si->db, doc))
		load_sort_value(doc);
	return doc;
}

void
ParseNodeSubsort::parent_doc_loaded(struct result *result, int32_t suboff, int32_t parentoff) {
	load_sort_value(parentoff);
}

int
ParseNodeAttrlistSort::get_parse_string(char *buf, size_t buflen) {
	return attrnode->get_parse_string(buf, buflen);
}

void
ParseNodeAttrlistSort::add_node(ParseNode *n, bool head, int is_union) {
	if (attrnode)
		return; /* XXX implement. */

	attrnode = n;
	attrnode_is_union = is_union;
}

void
cache_load_sort_value(void *mdarg, struct result *result, int32_t docoff) {
	ParseNodeAttrlistSort *me = (ParseNodeAttrlistSort*)mdarg;
	ParseNodeUnion<ParseNodeInvattr> *u = (ParseNodeUnion<ParseNodeInvattr>*)me->attrnode;

	ParseNodeInvattr *ia = u->head_node();
	const char *col = strchr(ia->keyword, ':');
	if (col)
		result->sort_value_arr[result->ndocs].i = atoi(col + 1) & INT32_MAX;
	else
		result->sort_value_arr[result->ndocs].i = atoi(ia->keyword) & INT32_MAX;
}

bool
ParseNodeAttrlistSort::cache_attrnode(struct result *res) {
	char parse_string[CACHE_KEY_SIZE];
	int len;
	ParseNode *n = next;

	/* Since attrsort unions are typically a union of many key:value lists it should be cached.
	 * We create a docarr cache similar to the normal one but with the sort value as suborder.
	 * For subads the parent ad is the one that should be cached, thus we need to set the
	 * suborder after it has been loaded.
	 * But we need to do it before the next call to get_next_document, since we use
	 * attrnode->head_node to find it. Therefore we need a callback inside perform_search.
	 */

	len = attrnode->get_parse_string(parse_string, sizeof(parse_string));
	if (len <= 0)
		return false;

	int new_entry;
	struct lru_entry *ce;

	ce = ::cache_lru(res->si->attrsort_cache, parse_string, &new_entry, NULL, NULL);
	log_printf(LOG_DEBUG, "attrsort: %s -> %s", parse_string, ce ? (new_entry ? "MISS" : "HIT") : (new_entry ? "FAIL" : "RECURSE"));

	if (!ce)
		return false;

	if (!new_entry) {
		delete attrnode;
		attrnode = new ParseNodeCacheLoad(res->si->attrsort_cache, ce, 0, parse_string, len, 0);
		return true;
	}

	/* XXX: Fugly workaround */
	if (((ParseNodeAttributeRange *)attrnode)->get_attribute() == NULL)
		return false;

	if (attrnode_is_union)
		n = ((ParseNodeUnion<ParseNodeInvattr>*)attrnode)->initialize(res, true);
	else
		n = ((ParseNodeAttributeRange*)attrnode)->initialize(res, true);
	if (n && n != attrnode) {
		delete attrnode;
		attrnode = n;
	}
	attrnode_is_union = true;

	ParseNode *node = new ParseNodeCacheStore(res->si->attrsort_cache, ce, attrnode, parse_string, len, 0, sortspec);

	assert(node);

	res->sub_ad_counter = 0;
	res->ndocs = 0;

	perform_search(res, node, NULL, 1, cache_load_sort_value, this);

	res->headers.pos = 0;

	ParseNodeCacheLoad * pncl = new ParseNodeCacheLoad(res->si->attrsort_cache, ce, 0, parse_string, len, 0);
	log_printf(LOG_DEBUG, "attrsort: retaining cache \"%s\"", parse_string);
	lru_retain(res->si->attrsort_cache, ce);
	delete node;
	attrnode = pncl;
	return true;
}

ParseNode *
ParseNodeAttrlistSort::initialize(struct result *res) {
	if (cache_attrnode(res)) {
		attrnode_is_cache = true;
	} else {
		ParseNode *n;
		if (attrnode_is_union)
			n = ((ParseNodeUnion<ParseNodeInvattr>*)attrnode)->initialize(res, true);
		else {
			/* If the attribute is NULL then initialisation will dump core */
			if (((ParseNodeAttributeRange*)attrnode)->get_attribute() == NULL)
				return NULL;
			n = ((ParseNodeAttributeRange*)attrnode)->initialize(res, true);
		}
		if (n && n != attrnode) {
			delete attrnode;
			attrnode = n;
		}
		attrnode_is_union = true;
	}
	return NULL;
}

int32_t
ParseNodeAttrlistSort::next_document(int32_t doc) {
	return attrnode->get_next_document(doc);
}

union sort_value
ParseNodeAttrlistSort::current_sort_value() {
	if (attrnode_is_cache) {
		ParseNodeCacheLoad *c = (ParseNodeCacheLoad*)attrnode;
		return c->current_sort_value();
	}
	ParseNodeUnion<ParseNodeInvattr> *u = (ParseNodeUnion<ParseNodeInvattr>*)attrnode;
	ParseNodeInvattr *ia = u->head_node();
	const char *col = strchr(ia->keyword, ':');
	return (union sort_value){ .i = atoi(col ? (col + 1) : ia->keyword) & INT32_MAX };
}

