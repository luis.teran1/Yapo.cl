#ifndef SEARCH_H
#define SEARCH_H

#include <pthread.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <netdb.h>
#include <stdbool.h>

#include "search_lexer.h"
#include "index.h"
#include "timer.h"
#include "tree.h"
#include "util.h"
#include "rand.h"

#include "bursttrie_serialized.h"
#include "sumtree.h"

#define SORT_MAX 8

#define NOCACHE -1
#define CACHE_KEY_SIZE 8192


enum server_state {INIT, RUNNING, RELOAD, STOP};
enum node_type {N_LEAF, N_AND, N_OR, N_PHRASE, N_CNT, N_FILT};

/*
 *  Search string: kw1 kw2 kw3 OR kw4 "kw5 kw6"
 * 
 *  kw1
 *   |
 *   | and
 *   |
 *  kw2
 *   |
 *   | and
 *   |   or
 *  kw3 ---- kw4
 *   |
 *   | and
 *   |   phrase
 *  kw5 -------- kw6
 */

union sort_value {
	int32_t i;
	float f;
};

struct result {
	int res_sortspec;
	int max_order;
	unsigned int ndocs;
	uint32_t sub_ad_counter;
	int32_t *docoff_arr;
	union sort_value *sort_value_arr;
	size_t docoff_arr_sz;
	struct buf_string headers;
	struct search_instance *si;
};

struct suggestion_instance {
	unsigned char *map_brt;
	unsigned char *map_sumtree;
	size_t map_brt_len;
	size_t map_sumtree_len;
	struct sumtree *sumtree;
	int brt_offset;
	int sumtree_offset;
	int brt_subtree_max_count;
	int brt_num_keys;

	int search_trigger;			/* Suggestion search only if fewer than this many results */
	int default_misspelled_threshold;	/* A keyword is considered potentially misspelled if below this threshold */
	int default_no_compound_threshold;	/* Turn off compound suggestions if keyword counts not below this high mark */
	int max_levenshtein_searches;		/* Max lev-searches to fire off for one query */
	int min_len_levenshtein_search;		/* keyword must be at least this long for search to happen */
	int min_len_levenshtein_suggest;	/* if non-zero, never suggest words shorter than this. */
};

struct search_instance_config {
	struct bconf_node *bconf;
	struct langmap *langmap;
	const char *default_lang;
	const char *default_attr_key;
	const char *default_attr_val;
	struct hash_table *hun_objs;
	struct syn_regexes *syn_regexes;
	struct charmap *charmap;
	int read_timeout;
	int slow_query_log;

	int ref;
};

struct search_instance {
	struct bsearch_cache *db;

	struct lru *query_cache;
	struct lru *subquery_cache;
	struct lru *attrsort_cache;

	struct search_instance_config *sic;

	struct hash_table *suggestions; /* key=db, value=suggestion_instance */

	int ref;
};

struct search_engine {
	struct search_instance *current_si;

	struct bconf_node *bconf_default;
	struct bconf_node *bconf_override;

	int command_socket;
	int search_socket;
	int keepalive_socket;
	int control_socket;

	bool pause_requests;
	struct stat_message *pause_state;

	/* This keeps track of indexes for running in indexer-daemon mode. */
	int indexer_daemon_mode;
	const char *workdir;

	pthread_rwlock_t index_lock;	/* This lock protects changing the indexes. */
	struct search_engine_index {
		char *fname;
		char *indexer_url;
		struct index_db *index;
		struct stat_message *checksum;
		struct stat_message *to;
		uint64_t *ndocs;
	} pending, active;
};

struct event_handler;

struct bsearch {
	struct parse_result query;
	struct result result;
	rand_t *res_rand;
};

struct search_conn {
	struct search_instance *si;
	int read_to_ms;
	int is_keepalive;
};

#ifdef __cplusplus
extern "C" {
#endif

static inline int32_t
increment_doc(int32_t *document) {
	*document += 1;
	return *document;
}

static inline int
subsort_cmp_inc(int32_t da, int32_t sva, int32_t db, int32_t svb) {
	int cmp = sva - svb;

	if (!cmp)
		return index_docoff_cmp(&da, &db);
	else
		return cmp;
}

static inline int
subsort_cmp_dec(int32_t da, int32_t sva, int32_t db, int32_t svb) {
	int cmp = svb - sva;

	if (!cmp)
		return index_docoff_cmp(&da, &db);
	else
		return cmp;
}

static inline int
subsort_cmp_inc_f(int32_t da, float sva, int32_t db, float svb) {
	float cmp = sva - svb;
	return cmp < 0.0 ? -1 : cmp > 0 ? 1 : index_docoff_cmp(&da, &db);
}

static inline int
subsort_cmp_dec_f(int32_t da, float sva, int32_t db, float svb) {
	float cmp = svb - sva;
	return cmp < 0.0 ? -1 : cmp > 0 ? 1 : index_docoff_cmp(&da, &db);
}

void perform_search(struct result *result, ParseNode *node, struct timer_instance *ti, int collapse_subads,
		void (*load_sort_value)(void *mdarg, struct result *result, int32_t docoff), void *mdarg);
ParseNode *main_search(struct result *result, ParseNode *node, struct timer_instance *ti, int print_parse,
		void (*pend_cb)(void*), void *pend_cb_arg) WARN_UNUSED_RESULT;
void cleanup_search(ParseNode *node);

ParseNode *reconstruct_search(struct bsearch_cache *, const char *parse_string, int *sortspec);

/* engine.c */
struct search_instance_config *search_instance_config_init(struct search_engine *);
void search_instance_config_drop(struct search_instance_config *);
void search_instance_config_ref(struct search_instance_config *);

struct search_instance *search_instance_init(struct search_engine *, struct search_instance_config *, struct index_db *);
void search_instance_destroy(struct search_instance *);
void search_instance_ref(struct search_instance *);
int search_instance_rel(struct search_instance *);

/* search_engine_index.c */
void search_engine_index_set_index(struct search_engine_index *, struct index_db *, int close, struct timer_instance *);
int search_engine_index_setup(struct bconf_node *, struct search_engine_index *, const char *, int force_fetch);

/* swapper.c */
void run_swapper(struct bconf_node *, struct bconf_node *);

#ifdef __cplusplus
}
#endif

#endif
