
#include <unistd.h>

#include "parse_nodes.hh"

class ParseNodeRegressDelay : public ParseNode {
public:
	ParseNodeRegressDelay()
		: ParseNode() {
	};

	int32_t next_document(int32_t doc) {
		sleep(2);
		return NULL_DOCOFF;
	}
	int32_t current_doc() const {
		return NULL_DOCOFF;
	}

	int get_parse_string(char *buf, size_t buflen) {
		if (buflen < 3)
			return -1;
		strcpy(buf, "rd");
		return 2;
	}

	ParseNode *initialize(struct result *res) {
		return NULL;
	}
};
