
#include "parse_nodes.hh"

struct suggestion_candidate;

struct suggest_state {
	int max_k;
	int misspelled_threshold;
	int category; /* for sumtree lookup */
	char* db;
	TAILQ_HEAD(,suggestion_candidate) candidates;
};

struct suggestion_candidate {
	char* word;
	enum iops op;
	int word_len;
	int count;
	int bare_count;

	char* s_word;
	int s_k;
	int s_count;
	int s_bare_count;
	int s_threshold;

	int compound_score;
	TAILQ_ENTRY(suggestion_candidate) next;
};

class ParseNodeSuggest : public ParseNodeNext {
private:
	struct buf_string infos;

	void suggestion_search(struct result* result);

public:
	struct suggest_state ss;

	ParseNodeSuggest(ParseNode *n, const struct suggest_state &s)
		: ParseNodeNext(n, false), ss(s)
	{
		TAILQ_INIT(&ss.candidates);
		infos.buf = NULL;
	}

	ParseNodeSuggest(std::stack<ParseNode*> *st, int start_num, int end_num, bool is_not, const char *t = NULL, int tl = 0)
		: ParseNodeNext(NULL, false)
	{
		abort(); /* TODO */
	}

	~ParseNodeSuggest()
	{
		/* Free dynamic data from suggest_state and suggestion_candidates */
		struct suggestion_candidate *sc;
		while ((sc = TAILQ_FIRST(&ss.candidates)) != NULL) {
			free(sc->word);
			free(sc->s_word);
			TAILQ_REMOVE(&ss.candidates, sc, next);
			free(sc);
		}
		free(ss.db);
		free(infos.buf);
	}

	int get_parse_string(char *buf, size_t buflen);

	virtual void finalize(struct result *res, struct timer_instance *ti);

	void get_headers(struct buf_string *);
};
