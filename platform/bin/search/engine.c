#include <errno.h>
#include <signal.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <semaphore.h>
#include <sys/epoll.h>
#include <netinet/tcp.h>
#include <sys/poll.h>
#include <sys/fcntl.h>
#include <dirent.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <stdbool.h>
#include <curl/curl.h>

#include "search_lexer.h"
#include "lru.h"
#include "chatter.h"
#include "db.h"
#include "engine.h"
#include "hunstem.h"
#include "logging.h"
#include "ps_status.h"
#include "search.h"
#include "queue.h"
#include "sock_util.h"
#include "stats.h"
#include "util.h"
#include "bconf.h"
#include "langmap.h"
#include "hash.h"
#include "text.h"
#include "charmap.h"
#include "bconfig.h"
#include "stat_counters.h"
#include "command.h"
#include "controller.h"

/* Engine signals */
static int quit_signaled;
static int status_signaled;

/* Communication sockets for engines */
int child_sock;

pthread_mutex_t search_mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t go_away_cond = PTHREAD_COND_INITIALIZER;
sem_t search_thread_sema;

struct event_handler;

struct chatter
{
	SLIST_ENTRY(chatter) list;
	TAILQ_ENTRY(chatter) thread_list;
	struct event_handler *keepalive;
	pthread_t chatter_id;
	pthread_cond_t cond;
	int die;
	int reserve_released;
	int connection_sock;
	struct search_instance *si;
};

struct go_away {
	TAILQ_ENTRY(go_away) list;
	int fd;
	int keepalive;
	struct event_handler *eh;
};

int total_threads;
int max_total_threads;
TAILQ_HEAD(,chatter) allthreads = TAILQ_HEAD_INITIALIZER(allthreads);
SLIST_HEAD(,chatter) search_thread_list = SLIST_HEAD_INITIALIZER(search_thread_list);
TAILQ_HEAD(,go_away) go_away_list = TAILQ_HEAD_INITIALIZER(go_away_list);
TAILQ_HEAD(,go_away) go_away_pool = TAILQ_HEAD_INITIALIZER(go_away_pool);

static void release_eh(struct event_handler *, int, int);

void
quit_signal(int sig) {
	quit_signaled = sig;
}

static void
status_signal(int sig) {
	status_signaled = sig;
}

static void
send_fds(int fd, struct search_engine *se) {
	struct msghdr msg = {0};
	struct cmsghdr *cmsg;
	struct iovec iov;
	const size_t cbuflen = CMSG_SPACE(4 * sizeof(int));
	char cbuf[cbuflen];
	ssize_t len;
	int *fdptr;

	msg.msg_iov = &iov;
	msg.msg_iovlen = 1;
	msg.msg_control = cbuf;
	msg.msg_controllen = cbuflen;
	iov.iov_base = (void*)"ok\n";
	iov.iov_len = sizeof("ok\n") - 1;
	
	cmsg = CMSG_FIRSTHDR(&msg);
	cmsg->cmsg_level = SOL_SOCKET;
	cmsg->cmsg_type = SCM_RIGHTS;
	if (se->control_socket != -1)
		cmsg->cmsg_len = CMSG_LEN(4 * sizeof(int));
	else
		cmsg->cmsg_len = CMSG_LEN(3 * sizeof(int));
	/* Initialize the payload */
	fdptr = (int *)(void*)CMSG_DATA(cmsg);
	fdptr[0] = se->search_socket;
	fdptr[1] = se->keepalive_socket;
	fdptr[2] = se->command_socket;
	if (se->control_socket != -1)
		fdptr[3] = se->control_socket;

	msg.msg_controllen = cmsg->cmsg_len;
	
	len = sendmsg(fd, &msg, 0);
	
	if (len <= 0) {
		log_printf(LOG_CRIT, "Failed to send listen fd: %m");
	}
}


static void *
go_away_thread(void *thread_data) {
	char buf[ENGINE_INPUT_BUFSZ];

	pthread_mutex_lock(&search_mutex);
	while (1) {
		struct go_away *go_away = TAILQ_FIRST(&go_away_list);

		while (!go_away) {
			pthread_cond_wait(&go_away_cond, &search_mutex);
			go_away = TAILQ_FIRST(&go_away_list);
		}

		TAILQ_REMOVE(&go_away_list, go_away, list);
		pthread_mutex_unlock(&search_mutex);

		log_printf(LOG_WARNING, "go_away: draining %d.", go_away->fd);

		if (!read_input(go_away->fd, buf, sizeof (buf), go_away->keepalive, go_away->keepalive ? 1 : 0)) {
			log_printf(LOG_WARNING, "NULL result from reading go_away fd %d", go_away->fd);
			release_eh(go_away->keepalive ? go_away->eh : NULL, go_away->fd, 1);
		} else {
			UNUSED_RESULT(write(go_away->fd, ENGINE_GO_AWAY, sizeof (ENGINE_GO_AWAY) - 1));
			release_eh(go_away->keepalive ? go_away->eh : NULL, go_away->fd, 0);
		}

		log_printf(LOG_WARNING, "go_away: %d done.", go_away->fd);

		pthread_mutex_lock(&search_mutex);
		TAILQ_INSERT_TAIL(&go_away_pool, go_away, list);
	}
	return NULL;
}

static struct chatter *
create_chatter_thread(bool isreserve) {
	struct chatter *thread = zmalloc(sizeof(*thread));
	int ret;

	if (total_threads >= max_total_threads)
		return NULL;
	total_threads++;

	TAILQ_INSERT_TAIL(&allthreads, thread, thread_list);

	if (isreserve)
		thread->reserve_released = true; /* Skip the initial release. */

	thread->connection_sock = -1;
	pthread_cond_init(&thread->cond, NULL);

	if ((ret = pthread_create(&thread->chatter_id, NULL, chatter_thread, thread)) != 0) {
		log_printf(LOG_CRIT, "Failed to create chatter thread %d %s", ret, xstrerror(ret));
		exit(1);
	}

	return thread;
}


static void
handle_search(struct event_handler *eh, int fd, struct search_engine *se) {
	struct chatter *thread;

	pthread_mutex_lock(&search_mutex);

	if (!se->pause_requests && !sem_trywait(&search_thread_sema)) {
		thread = SLIST_FIRST(&search_thread_list);
		if (thread) {
			SLIST_REMOVE_HEAD(&search_thread_list, list);
		} else {
			log_printf(LOG_WARNING, "Creating reserve thread");
			thread = create_chatter_thread(true);
		}
	} else {
		thread = NULL;
	}

	if (thread) {
		struct search_instance *si;

		/* Lock already held, can't use se_get_si */
		si = se->current_si;
		search_instance_ref(si);

		thread->connection_sock = fd;
		thread->si = si;
		thread->keepalive = eh;
		pthread_cond_signal(&thread->cond);

		pthread_mutex_unlock(&search_mutex);
	} else {
		struct go_away *go_away = TAILQ_FIRST(&go_away_pool);

		if (go_away) {
			TAILQ_REMOVE(&go_away_pool, go_away, list);
		} else {
			go_away = xmalloc(sizeof (struct go_away));
		}
		go_away->eh = eh;
		go_away->fd = fd;
		go_away->keepalive = eh != NULL;

		TAILQ_INSERT_TAIL(&go_away_list, go_away, list); 
		pthread_mutex_unlock(&search_mutex);		
		pthread_cond_signal(&go_away_cond);

		if (se->pause_requests) {
			log_printf(LOG_DEBUG, "Search engine paused, ignoring search (%d).", fd);
		} else {
			stat_inc(NULL, "BUSY");
			log_printf(LOG_WARNING, "All threads busy, ignoring search (%d).", fd);
		}
	}
}

static int
warm_cache(struct search_instance *si, int fd) {
	struct result result;
	char buf[CACHE_KEY_SIZE];
	size_t buflen = 0;
	int res = -1;

	memset(&result, 0, sizeof(result));
	result.docoff_arr_sz = db_ndocs(si->db);
	result.docoff_arr = (int32_t*)xmalloc(result.docoff_arr_sz * sizeof(*result.docoff_arr));
	result.sort_value_arr = (union sort_value*)xmalloc(result.docoff_arr_sz * sizeof(*result.sort_value_arr));
	result.headers = (struct buf_string){NULL};
	result.si = si;

	while (res == -1) {
		ssize_t len = read(fd, buf + buflen, sizeof(buf) - 1 - buflen);
		char *nl;
		ParseNode *node;

		if (len < 0) {
			log_printf(LOG_CRIT, "Read error while warming cache: %m");
			res = 1;
			break;
		}
		if (len == 0) {
			log_printf(LOG_CRIT, "EOF while warming cache");
			res = 1;
			break;
		}

		buflen += len;
		buf[buflen] = '\0';
		while ((nl = strchr(buf, '\n'))) {
			if (!nl) {
				log_printf(LOG_CRIT, "Buffer overflow while warming cache");
				res = 1;
				break;
			}

			if (nl == buf) {
				/* Done, return success. */
				res = 0;
				break;
			}

			*nl++ = '\0';
			int sortspec;
			if ((node = reconstruct_search(si->db, buf, &sortspec))) {
				result.ndocs = 0;
				node = main_search(&result, node, NULL, 0, NULL, NULL);
				cleanup_search(node);
			}
			buflen -= nl - buf;
			memmove(buf, nl, buflen + 1);
		}
	}

	free(result.docoff_arr);
	free(result.sort_value_arr);
	free(result.headers.buf);
	return res;
}


static void
timer_dump(struct timer_class *tc, void *data) {
	struct timespec avgts;
	double avg = (double)tc->tc_total.tv_sec + (double)tc->tc_total.tv_nsec / 1000000000.0;
	const char *name;

	if (tc->tc_count == 0)
		return;

	name = tc->tc_name;

	avg /= (double)tc->tc_count;

	avgts.tv_sec = avg;
	avgts.tv_nsec = (avg - (double)avgts.tv_sec) * 1000000000.0;

	log_printf(LOG_INFO, "timer %s count:%lld", name, tc->tc_count);
	log_printf(LOG_INFO, "timer %s bytes:%llu", name, (unsigned long long)tc->tc_counter);
	log_printf(LOG_INFO, "timer %s total:%ld.%03ld", 
			name, tc->tc_total.tv_sec, tc->tc_total.tv_nsec / 1000000);
	log_printf(LOG_INFO, "timer %s min:%ld.%03ld", 
			name, tc->tc_min.tv_sec, tc->tc_min.tv_nsec / 1000000);
	log_printf(LOG_INFO, "timer %s max:%ld.%03ld", 
			name, tc->tc_max.tv_sec, tc->tc_max.tv_nsec / 1000000);
	log_printf(LOG_INFO, "timer %s average:%ld.%03ld", 
			name, avgts.tv_sec, avgts.tv_nsec / 1000000);
}

static void
stat_dump(void *data, uint64_t cnt, const char **name) {
	struct buf_string bs = {0};
	int i;

	for (i = 0; name[i] != NULL; i++) {
		bscat(&bs, "%s%c", name[i], name[i + 1] ? '.' : ':');
	}

	log_printf(LOG_INFO, "stat_counter %s %llu", bs.buf, (unsigned long long)cnt);
	free(bs.buf);
}

/* Main loop state */
struct engine {
	int running;
	int epollfd;
};

/* Main loop events and their handlers */
struct event_handler {
	TAILQ_ENTRY(event_handler) list;
	void (*cb)(struct event_handler *, struct search_engine *);
	int fd;
	struct engine *eng;
};

pthread_mutex_t eh_mutex = PTHREAD_MUTEX_INITIALIZER;
TAILQ_HEAD(,event_handler) eh_list = TAILQ_HEAD_INITIALIZER(eh_list);

static void
eh_add(struct engine *eng, void (*cb)(struct event_handler *, struct search_engine *), int fd, int oneshot) {
	struct event_handler *eh = xmalloc(sizeof(*eh));
	struct epoll_event event = {EPOLLIN , {0}};

	eh->cb = cb;
	eh->fd = fd;
	eh->eng = eng;
	pthread_mutex_lock(&eh_mutex);
	TAILQ_INSERT_TAIL(&eh_list, eh, list);
	pthread_mutex_unlock(&eh_mutex);

	event.events = EPOLLIN|EPOLLHUP|(oneshot ? EPOLLONESHOT : 0);
	event.data.ptr = eh;
	if (epoll_ctl(eng->epollfd, EPOLL_CTL_ADD, fd, &event) < 0) {
		log_printf(LOG_CRIT, "Error adding socket to epoll set: %m");
		exit(1);
	}	
}

static void
eh_accept(struct event_handler *eh, struct search_engine *se) {
	int fd = accept(eh->fd, NULL, NULL);
	int one = 1;

	if (fd < 0) {
		log_printf(LOG_CRIT, "Error accepting search socket: %m");
		return;
	}

	if (setsockopt(fd, SOL_TCP, TCP_NODELAY, &one, sizeof(one)) == -1)
		log_printf(LOG_ERR, "TCP_NODELAY failed: %m");

	handle_search(NULL, fd, se);
}


static void
eh_keepalive(struct event_handler *eh, struct search_engine *se) {
	handle_search(eh, eh->fd, se);
}

static void
eh_keepalive_accept(struct event_handler *eh, struct search_engine *se) {
	int fd = accept(eh->fd, NULL, NULL);
	int one = 1;

	if (fd < 0) {
		log_printf(LOG_CRIT, "Error accepting keepalive socket: %m");
		return;
	}

	if (setsockopt(fd, SOL_TCP, TCP_NODELAY, &one, sizeof(one)) == -1)
		log_printf(LOG_ERR, "TCP_NODELAY failed: %m");

	eh_add(eh->eng, eh_keepalive, fd, 1);
}

static void
collect_cache(struct lru_entry *entry, void *cbdata) {
	struct buf_string *str = cbdata;

	bufcat(&str->buf, &str->len, &str->pos, "%s\n", entry->key);
}

static void
eh_command(struct event_handler *eh, struct search_engine *se) {
	char buf[1024];

	int len = read(eh->fd, buf, sizeof(buf) - 1);
	if (len == -1) {
		if (errno != EAGAIN && errno != EINTR) {
			log_printf(LOG_CRIT, "command read failed (%m)");
			eh->eng->running = 0;
		}
	} else if (len == 0) {
		log_printf(LOG_INFO, "Child got EOF on control socket, shutting down");
		eh->eng->running = 0;
	} else if (len > 0) {
		char *bp;
		char *np;
		
		buf[len] = '\0';

		for (bp = buf; (np = strchr(bp, '\n')); bp = np) {
			*np++ = '\0';

			log_printf(LOG_DEBUG, "Child received command \"%s\"", bp);
			if (!strcmp(bp, "send_cache")) {
				struct search_instance *si;
				struct buf_string str = {0};

				si = se_get_si(se);

				lru_foreach(si->query_cache, collect_cache, &str);
				bufwrite(&str.buf, &str.len, &str.pos, "\n", 1);
				UNUSED_RESULT(write(eh->fd, str.buf, str.pos));
				free(str.buf);

				se_put_si(se, si);
			} else if (!strcmp(bp, "send_fd")) {
				send_fds(eh->fd, se);
			}
		}
	}
}

static int
lock_db(const char *db_name, const char *suffix) {
	char *lockfile;
	int len;

	ALLOCA_PRINTF(len, lockfile, "%s%s%s/.lock", db_name, *suffix ? "." : "", suffix);
#if 1
	/*
	 * There's no need to create lock files anymore, the indexes are write once and
	 * after we open the index we'll keep it mmap:ed so it can be safely deleted.
	 * Just check that the lock file doesn't exist in case the indexer is still
	 * actively writing to the index.
	 */
	if (access(lockfile, F_OK) != -1)
		return -1;
	return 0;
#else
	int fd;

	if ((fd = open(lockfile, O_RDWR|O_CREAT|O_EXCL, 0644)) == -1)
		return -1;
	close(fd);

	return 0;
#endif
}

static void
unlock_db(const char *db_name, const char *suffix) {
#if 0
	char *lockfile;
	int len;

	ALLOCA_PRINTF(len, lockfile, "%s%s%s/.lock", db_name, *suffix ? "." : "", suffix);

	unlink(lockfile);
#endif
}

static int
exists_db(const char *db_name, const char *suffix) {
	char *name;
	int len;

	ALLOCA_PRINTF(len, name, "%s%s%s", db_name, *suffix ? "." : "", suffix);

	return !access(name, X_OK);
}

static int
clean_db(const char *db_name, const char *suffix) {
	struct dirent *dirent;
	char *name;
	DIR *dir;
	int len;

	if (!exists_db(db_name, suffix))
		return 0;

	ALLOCA_PRINTF(len, name, "%s%s%s", db_name, *suffix ? "." : "", suffix);

	if ((dir = opendir(name)) == NULL)
		return -1;

	/*
	 * Ignore errors, the final rmdir is the only thing that matters.
	 */
	while ((dirent = readdir(dir)) != NULL) {
		struct stat sb;
		char *fn;

		if (!strcmp(dirent->d_name, ".") || !strcmp(dirent->d_name, "..") || !strcmp(dirent->d_name, ".lock"))
			continue;
		ALLOCA_PRINTF(len, fn, "%s/%s", name, dirent->d_name);
		if (stat(fn, &sb) != 0)
			continue;
		if (S_ISDIR(sb.st_mode)) {
			clean_db(fn, "");
		} else {
			unlink(fn);
		}
		
	}
	closedir(dir);

	unlock_db(db_name, suffix);

	if (rmdir(name)) {
		log_printf(LOG_CRIT, "Failed to remove %s (%m)", name);
		return -1;
	}

	return 0;
}

static int
rename_db(const char *db_name, const char *from_suffix, const char *to_suffix) {
	char *from, *to;
	int len;

	ALLOCA_PRINTF(len, from, "%s%s%s", db_name, *from_suffix ? "." : "", from_suffix);
	ALLOCA_PRINTF(len, to, "%s%s%s", db_name, *to_suffix ? "." : "", to_suffix);

	return rename(from, to);
}

static struct bsearch_cache *
open_db(const char *db_name, const char *suffix, struct bconf_node *bconf, struct charmap *cm, int file) {
	char *name;
	int len;

	ALLOCA_PRINTF(len, name, "%s%s%s", db_name, *suffix ? "." : "", suffix);
	return db_init(name, bconf, cm, file, NULL);
}

static struct bsearch_cache *
choose_db(struct bconf_node *bconf, struct charmap *cm) {
	struct bsearch_cache *db;
	const char *db_name;
	const char *index_file;

	if ((index_file = bconf_get_string(bconf, "index_file")) == NULL &&
	    (db_name = bconf_get_string(bconf, "db_name")) == NULL) {
		log_printf(LOG_CRIT, "db_name not configured");
		exit(1);
	}
	if (index_file) {
		return open_db(index_file, "", bconf, cm, 1);
	}
	/*
	 * Attempt to open db.new. If there's a lock file it means that either:
	 *  - the old attempt to open db.new failed
	 *  - there's a merge going on (unlikely)
	 *  In this case, just fall back to db (the next merge will deal with the problem
	 *  by most likely doing a distribute-full).
	 */
	if (lock_db(db_name, "new") == 0) {
		if (clean_db(db_name, "old")) {
			log_printf(LOG_CRIT, "Failed to remove %s.old (%m)", db_name);
			return NULL;
		}

		db = open_db(db_name, "new", bconf, cm, 0);
		if (db) {
			unlock_db(db_name, "new");
			if (exists_db(db_name, "") && rename_db(db_name, "", "old")) {
				log_printf(LOG_CRIT, "Failed to rename %s to %s.old (%m)", db_name, db_name);
				db_cleanup(db);
				db = NULL;
			} else {
				if (!rename_db(db_name, "new", ""))
					return db;
				log_printf(LOG_CRIT, "Failed to rename %s.new to %s (%m)", db_name, db_name);
				db_cleanup(db);
				db = NULL;
			}
		}
	}

	if (lock_db(db_name, "") != 0) {
		log_printf(LOG_CRIT, "Failed to lock %s (%m)", db_name);
		return NULL;
	}

	db = open_db(db_name, "", bconf, cm, 0);
	unlock_db(db_name, "");

	return db;
}

static void
suggestions_free(void* obj) {
	struct suggestion_instance *sg = obj;

	if (sg->map_brt)
		munmap(sg->map_brt, sg->map_brt_len);
	if (sg->map_sumtree)
		munmap(sg->map_sumtree, sg->map_sumtree_len);
	sumtree_free(sg->sumtree);

	free(obj);
}

static int
mmap_brt_file(const char* filename, struct suggestion_instance *sui) {

	int fd = open(filename, O_RDONLY);
	if (fd < 0)
		return -1;

	struct stat s;
	fstat(fd, &s);
	if (s.st_size < (int)sizeof(struct ser_header)) {
		close(fd);
		return -2;
	}

	unsigned char *map = mmap(0, s.st_size, PROT_READ, MAP_SHARED, fd, 0);
	close(fd);
	if (map == MAP_FAILED) {
		return -3;
	}

	char id[8] = BRT_SERIALIZED_ID0;
	struct ser_header *header = (struct ser_header*)map;
	if (strncmp(header->id, id, 8) != 0) {
		munmap(map, s.st_size);
		return -4;
	}

	sui->map_brt = map;
	sui->map_brt_len = s.st_size;
	sui->brt_offset = header->root.offset;
	sui->brt_num_keys = header->num_keys;
	sui->brt_subtree_max_count = header->root.subtree_max_count;

	return 1;
}

static int
mmap_sumtree_file(const char* filename, struct suggestion_instance *sui) {

	FILE* f = fopen(filename, "rb");
	if (!f) {
		return -1;
	}
	sui->sumtree = sumtree_deserialize(f);
	fclose(f);
	if (!sui->sumtree)
		return -2;

	int fd = open(filename, O_RDONLY);
	if (fd < 0)
		return -3;

	struct stat s;
	fstat(fd, &s);
	if (s.st_size < (int)sizeof(struct sumtree_header)) {
		close(fd);
		return -4;
	}

	unsigned char *map = mmap(0, s.st_size, PROT_READ, MAP_SHARED, fd, 0);
	close(fd);
	if (map == MAP_FAILED) {
		return -5;
	}

	struct sumtree_header *header = (struct sumtree_header*)map;
	char id[8] = ST_SERIALIZED_ID0;

	int arch_diff = 0;
	if (header->ptrsize != sizeof(struct st_entry*)) {
		log_printf(LOG_ERR, "Sumtree incompatible with present architecture");
		arch_diff = 1;
	}

	if (arch_diff || strncmp(header->id, id, 8) != 0) {
		munmap(map, s.st_size);
		return -6;
	}

	sui->map_sumtree = map;
	sui->sumtree_offset = header->size + header->sumtreesize;
	sui->map_sumtree_len = s.st_size;

	return 1;
}

static int
setup_suggestion_instances(struct search_instance *si) {
	struct search_instance_config *sic = si->sic;
	struct bconf_node *voc_node = bconf_vget(sic->bconf, "vocabulary", NULL);
	struct bconf_node *cfg_node = NULL;

	if (!voc_node) {
		log_printf(LOG_INFO, "No vocabulary configuration available.");
		return 0;
	}

	int num_confed = 0;
	for (int i = 0 ; (cfg_node = bconf_byindex(voc_node, i)) ; ++i) {
		char* buf;
		int len;

		if (!bconf_get_int(cfg_node, "enabled"))
			continue;

		struct suggestion_instance temp_sui = { 0 };

		const char* base_fn = bconf_get_string(cfg_node, "db_name");
		if (!base_fn) {
			log_printf(LOG_ERR, "No db_name configured for vocabulary '%s'.", bconf_key(cfg_node));
			continue;
		}

		ALLOCA_PRINTF(len, buf, "%s.brt", base_fn);
		if (mmap_brt_file(buf, &temp_sui) < 1) {
			log_printf(LOG_INFO, "Vocabulary '%s' index file '%s' could not be loaded.", bconf_key(cfg_node), buf);
			continue;
		}

		if (temp_sui.brt_num_keys == 0) {
			log_printf(LOG_WARNING, "(WARN) Ignoring empty vocabulary '%s'.", bconf_key(cfg_node));
			munmap(temp_sui.map_brt, temp_sui.map_brt_len);
			continue;
		}

		ALLOCA_PRINTF(len, buf, "%s.sumtree", base_fn);
		if (mmap_sumtree_file(buf, &temp_sui) < 1) {
			/* ok */
		}

		temp_sui.search_trigger = bconf_get_int_default(cfg_node, "search_trigger", 50);
		temp_sui.default_misspelled_threshold = bconf_get_int_default(cfg_node, "default_misspelled_threshold", 200);
		temp_sui.default_no_compound_threshold = bconf_get_int_default(cfg_node, "default_no_compound_threshold", 1000000);
		temp_sui.max_levenshtein_searches = bconf_get_int_default(cfg_node, "max_levenshtein_searches", 3);
		temp_sui.min_len_levenshtein_search = bconf_get_int_default(cfg_node, "min_len_levenshtein_search", 4);
		temp_sui.min_len_levenshtein_suggest = bconf_get_int_default(cfg_node, "min_len_levenshtein_suggest", 0);

		log_printf(LOG_INFO, "Vocabulary configuration for '%s' successful, %d keys in db%s.", bconf_key(cfg_node), temp_sui.brt_num_keys, temp_sui.sumtree ? " (with sumtree)" : "");

		struct suggestion_instance *sug = xmalloc(sizeof(struct suggestion_instance));
		*sug = temp_sui;

		/* Create hash only if we have anything to insert into it */
		if (!si->suggestions) {
			si->suggestions = hash_table_create(5, suggestions_free);
			hash_table_free_keys(si->suggestions, 1);
		}

		hash_table_insert(si->suggestions, xstrdup(bconf_key(cfg_node)), -1, sug);
		++num_confed;
	}

	return num_confed;
}

struct search_instance_config *
search_instance_config_init(struct search_engine *se) {
	struct search_instance_config *sic = zmalloc(sizeof(*sic));
	struct bconf_node *hnode;
	struct bconf_node *bc;
	struct bconf_node *c;
	const char *config_file;
	int nhundicts;
	int i;

	sic->ref = 1;

	if ((config_file = bconf_get_string(se->bconf_override, "config_file")) == NULL)
		config_file = bconf_get_string(se->bconf_default, "config_file");

	if ((bc = config_init(config_file)) == NULL) {
		log_printf(LOG_CRIT, "Cannot read config file: %s", config_file);
		return NULL;
	}
	bconf_merge(&sic->bconf, se->bconf_default);
	bconf_merge(&sic->bconf, bc);
	bconf_merge(&sic->bconf, se->bconf_override);

	bconf_free(&bc);

	sic->charmap = init_charmap(bconf_get_string(sic->bconf, "charmap"));

	sic->read_timeout = bconf_get_int(sic->bconf, "timeout") * 1000;
	sic->default_lang = bconf_get_string(sic->bconf, "default.lang");
 	if ((c = bconf_byindex(bconf_get(sic->bconf, "default_attr"), 0))) {
 		sic->default_attr_key = bconf_key(c);
		sic->default_attr_val = bconf_value(c);
 		log_printf(LOG_DEBUG, "Default attribute: %s:%s", sic->default_attr_key, sic->default_attr_val);
 	}

	if (bconf_get(sic->bconf, "langmap"))
		sic->langmap = langmap_open(bconf_get_string(sic->bconf, "langmap"));
	else
		sic->langmap = NULL;

	sic->syn_regexes = init_syn_regexes(bconf_get(sic->bconf, "syn_regexes"));

	sic->slow_query_log = bconf_get_int(sic->bconf, "slow_query_log") * 1000;

	/* Set up hun_objs */
	sic->hun_objs = hash_table_create(1, hun_clear);
	hash_table_free_keys(sic->hun_objs, 1);

	hnode = bconf_get(sic->bconf, "hundict");
	nhundicts = bconf_count(hnode);

	log_printf(LOG_DEBUG, "Languages configured: %d", nhundicts);

	for (i = 0 ; i < nhundicts ; i++) {
		const char *dict = bconf_value(bconf_byindex(hnode, i));
		const char *lang = bconf_key(bconf_byindex(hnode, i));
		char *dic;
		char *aff;
		int len;
		void *hun_obj = NULL;

		log_printf(LOG_DEBUG, "Setting up hunspell for stemming (%s)", lang);
		ALLOCA_PRINTF(len, dic, "%s.dic", dict);
		ALLOCA_PRINTF(len, aff, "%s.aff", dict);

		hun_obj = hun_setup(aff, dic);
		if (!hun_obj) {
			log_printf(LOG_WARNING, "warning: failed to setup hunspell for stemming (%s)", lang);
			continue;
		}

		hash_table_insert(sic->hun_objs, xstrdup(lang), -1, hun_obj);
	}

	return sic;
}

void
search_instance_config_ref(struct search_instance_config *sic) {
	/*
	 * We don't need any additional locking than an atomic increment because the pointer will always be
	 * fetched from a referenced and/or locked search_instance which always keeps a reference to us.
	 */
	__sync_fetch_and_add(&sic->ref, 1);
}

void
search_instance_config_drop(struct search_instance_config *sic) {
	if (__sync_sub_and_fetch(&sic->ref, 1) != 0)
		return;
	if (sic->langmap)
		langmap_close(sic->langmap);
	hash_table_free(sic->hun_objs);
	free_syn_regexes(sic->syn_regexes);
	free_charmap(sic->charmap);
	bconf_free(&sic->bconf);
	free(sic);
}

struct search_instance *
search_instance_init(struct search_engine *se, struct search_instance_config *sic, struct index_db *index) {
	size_t cache_size_q, cache_size_sq, cache_size_a;
	struct search_instance *si;

	if (sic == NULL && (sic = search_instance_config_init(se)) == NULL) {
		return NULL;
	} else {
		search_instance_config_ref(sic);
	}

	si = zmalloc(sizeof(*si));
	si->sic = sic;

	if ((cache_size_q = bconf_get_int(sic->bconf, "cache_memory.query_cache_mb")) != 0) {
		cache_size_q *= 1024*1024;
		if ((cache_size_sq = bconf_get_int(sic->bconf, "cache_memory.subquery_cache_mb")) != 0) {
			cache_size_sq *= 1024*1024;
		} else {
			cache_size_sq = cache_size_q / 10;
		}
		if ((cache_size_a = bconf_get_int(sic->bconf, "cache_memory.attrsort_cache_mb")) != 0) {
			cache_size_a *= 1024*1024;
		} else {
			cache_size_a = cache_size_q / 10;
		}
	} else if (bconf_get_int(sic->bconf, "cache_size") != 0) {
		log_printf(LOG_WARNING, "Obsolete cache_size configuration. Use cache_memory instead. Defaulting to 0.5GB");
		cache_size_q = 512*1024*1024;
		cache_size_sq = 50*1024*1024;
		cache_size_a = 50*1024*1024;
	} else {
		log_printf(LOG_WARNING, "No cache_memory configured. Defaulting to 100MB");
		cache_size_q = 100*1024*1024;
		cache_size_sq = 10*1024*1024;
		cache_size_a = 10*1024*1024;
	}

	si->ref = 1;
	si->query_cache = lru_init(cache_size_q, free_res, stat_inc);
	si->subquery_cache = lru_init(cache_size_sq, free_res, stat_inc);
	si->attrsort_cache = lru_init(cache_size_a, free_res, NULL);

	log_printf(LOG_DEBUG, "Initializing DB");

	if (index == NULL) {
		if ((si->db = choose_db(sic->bconf, sic->charmap)) == NULL) {
			log_printf(LOG_CRIT, "Failed to open DB");
			lru_free(si->query_cache);
			lru_free(si->subquery_cache);
			lru_free(si->attrsort_cache);
			search_instance_config_drop(si->sic);
			free(si);
			return NULL;
		}
	} else {
		if ((si->db = db_init(NULL, sic->bconf, sic->charmap, 0, index)) == NULL) {
			log_printf(LOG_CRIT, "Failed to open DB");
			lru_free(si->query_cache);
			lru_free(si->subquery_cache);
			lru_free(si->attrsort_cache);
			search_instance_config_drop(si->sic);
			free(si);
			return NULL;
		}
	}

	int nvocabs = setup_suggestion_instances(si);
	log_printf(LOG_DEBUG, "Vocabularies configured: %d", nvocabs);

	return si;
}

void
search_instance_destroy(struct search_instance *si) {
	db_cleanup(si->db);
	lru_free(si->query_cache);
	lru_free(si->subquery_cache);
	lru_free(si->attrsort_cache);
	hash_table_free(si->suggestions);
	search_instance_config_drop(si->sic);
	free(si);
}

void
search_instance_ref(struct search_instance *si) {
	si->ref++;
}

int
search_instance_rel(struct search_instance *si) {
	return --si->ref == 0;
}

struct search_instance *
se_get_si(struct search_engine *se) {
	struct search_instance *si;

	pthread_mutex_lock(&search_mutex);
	si = se->current_si;
	search_instance_ref(si);
	pthread_mutex_unlock(&search_mutex);

	return si;
}

void
se_put_si(struct search_engine *se, struct search_instance *si) {
	pthread_mutex_lock(&search_mutex);
	if (!search_instance_rel(si))
		si = NULL;
	pthread_mutex_unlock(&search_mutex);
	if (si)
		search_instance_destroy(si);
}

int
run_engine(struct bconf_node *bconf_default, struct bconf_node *bconf_override, pid_t signal_pid, int command_sock) {
	pthread_t command_id, go_away_id;
	struct search_instance_config *sic;
	struct search_engine se = { 0 };
	struct ctrl_thread *ctrlthr = NULL;
	struct chatter *thread;
	struct bconf_node *bconf;
	struct engine eng = { 0 };
	sigset_t old_set;
	sigset_t new_set;
	int cnt;
	int ret;
	int threads, max_reserve_threads;

	curl_global_init(CURL_GLOBAL_ALL);

	set_ps_display("engine initializing");

	log_register_thread("Engine %d (parent: %d)", (int)getpid(), (int)getppid());

	stat_register("QUERY");
	stat_register("SEARCH");
	stat_register("GET");
	stat_register("EMPTY");
	stat_register("SLOW");
	stat_register("BUSY");
	stat_register("TIMEOUT");
#if 0
	stat_register("CACHE HIT");
	stat_register("CACHE MISS");
#endif
	stat_register("CACHE PENDING");
	stat_register("CACHE OUT");
	stat_register("CACHE FULL");
	stat_register("CACHE RECURSE");
	stat_register("CACHE WORD");
	stat_register("CACHE ATTR");

	stat_register("SUGGESTIONS");
	stat_register("SUGGESTIONS COMPOUND");

	log_printf(LOG_DEBUG, "Files read, warming cache");

	sigfillset(&new_set);
	sigdelset(&new_set, SIGFPE);
	sigdelset(&new_set, SIGSEGV);
	sigdelset(&new_set, SIGTRAP);
	sigdelset(&new_set, SIGABRT);
	sigdelset(&new_set, SIGTERM);
	sigdelset(&new_set, SIGINT);
	sigdelset(&new_set, SIGUSR1);

	signal(SIGTERM, quit_signal);
	signal(SIGINT, quit_signal);
	signal(SIGUSR1, status_signal);

	/* Block all asynchronous signals */
	pthread_sigmask(SIG_SETMASK, &new_set, &old_set);

	se.bconf_default = bconf_default;
	se.bconf_override = bconf_override;

	/*
	 * This slightly convoluted way of setting things up is
	 * because we need to load the config _before_ we set up the
	 * search instance so that we can from the config figure out
	 * if we should run in indexer-daemon mode or not.
	 *
	 * indexer-daemon mode uses a different method for keeping
	 * track of indexes than the classic mode.
	 */
	if ((sic = search_instance_config_init(&se)) == NULL) {
		log_printf(LOG_CRIT, "Failed to init search instance");
		exit(1);
	}

	bconf = sic->bconf;

	if (bconf_get_int(bconf, "index.daemon.enable")) {
		pthread_rwlock_init(&se.index_lock, NULL);
		se.indexer_daemon_mode = 1;
		if (search_engine_index_setup(bconf, &se.active, "active", 1) == -1 ||
		    search_engine_index_setup(bconf, &se.pending, "pending", 0)) {
			exit(1);
		}
		if ((se.current_si = search_instance_init(&se, sic, se.active.index)) == NULL) {
			log_printf(LOG_CRIT, "Failed to init search instance");
			exit(1);
		}
	} else {
		if ((se.current_si = search_instance_init(&se, sic, NULL)) == NULL) {
			log_printf(LOG_CRIT, "Failed to init search instance");
			exit(1);
		}
	}

	threads = bconf_get_int(bconf, "threads");
	max_reserve_threads = bconf_get_int(bconf, "max_reserve_threads");
	if (!max_reserve_threads)
		max_reserve_threads = threads / 5;

	max_total_threads = threads + max_reserve_threads;

	if (child_sock != -1 && !bconf_get_int(bconf, "dont_warm_cache")) {
		/* Tell old child to send us the cache */
		if (write(child_sock, "send_cache\n", sizeof("send_cache\n") - 1) == -1) {
			log_printf(LOG_CRIT, "Failed to request cache from old child: %m");
			exit(1);
		}

		/* Wait for cache being sent */
		set_ps_display("engine warming cache");
		warm_cache(se.current_si, child_sock);
	}

	log_printf(LOG_DEBUG, "Cache warmed, creating listening socket");

	if (child_sock != -1) {
		const size_t cbuflen = CMSG_SPACE(4 * sizeof(int));
		struct msghdr msg = {0};
		struct cmsghdr *cmsg;
		char cbuf[cbuflen];
		struct iovec iov;
		char buf[1024];
		ssize_t len;

		if (write(child_sock, "send_fd\n", sizeof("send_fd\n") - 1) == -1) {
			log_printf(LOG_CRIT, "Failed to request listen fd from old child: %m");
			exit(1);
		}

		msg.msg_iov = &iov;
		msg.msg_iovlen = 1;
		msg.msg_control = cbuf;
		msg.msg_controllen = cbuflen;
		iov.iov_base = buf;
		iov.iov_len = sizeof(buf);
		errno = 0;
		len = recvmsg(child_sock, &msg, 0);
		if (len <= 0) {
			log_printf(LOG_CRIT, "Failed to receive listen fd: %m");
			exit(1);
		}
		for (cmsg = CMSG_FIRSTHDR(&msg); cmsg != NULL; cmsg = CMSG_NXTHDR(&msg, cmsg)) {
			log_printf(LOG_DEBUG, "Received control message level %d type %d", (int)cmsg->cmsg_level, (int)cmsg->cmsg_type);
			if (cmsg->cmsg_level == SOL_SOCKET && cmsg->cmsg_type == SCM_RIGHTS) {
				int *fdptr = (int *)(void*)CMSG_DATA(cmsg);
				se.search_socket = fdptr[0];
				se.keepalive_socket = fdptr[1];
				se.command_socket = fdptr[2];
				if (cmsg->cmsg_len == CMSG_LEN(4 * sizeof(int)));
					se.control_socket = fdptr[3];
			}
		}
		if (se.search_socket == -1 || se.keepalive_socket == -1 || se.command_socket == -1) {
			log_printf(LOG_CRIT, "Didn't receive listen fd!");
			exit(1);
		}
	} else {
		se.search_socket = create_socket(NULL, bconf_get_string(bconf, "port.search"));
		log_printf(LOG_DEBUG, "Created search socket: %u", se.search_socket);
		if (se.search_socket == -1) {
			log_printf(LOG_CRIT, "Failed to create search socket on port %u", bconf_get_int(bconf, "port.search"));
			exit(1);
		}
		se.keepalive_socket = create_socket(NULL, bconf_get_string(bconf, "port.keepalive"));
		log_printf(LOG_DEBUG, "Created keepalive socket: %u", se.keepalive_socket);
		if (se.keepalive_socket == -1) {
			log_printf(LOG_CRIT, "Failed to create keepalive socket on port %u", bconf_get_int(bconf, "port.keepalive"));
			exit(1);
		}
		se.command_socket = create_socket(NULL, bconf_get_string(bconf, "port.command"));
		log_printf(LOG_DEBUG, "Created command socket: %u", se.command_socket);
		if (se.command_socket == -1) {
			log_printf(LOG_CRIT, "Failed to create command socket on port %u: %m", bconf_get_int(bconf, "port.keepalive"));
			exit(1);
		}
		se.control_socket = -1;
	}


	log_printf(LOG_DEBUG, "Listen socket created, ready to run");

	set_ps_display("engine creating threads");

	sem_init(&search_thread_sema, 0, 0);
	for (cnt = 0 ; cnt < threads ; cnt++) {
		create_chatter_thread(false);
	}

	if ((ret = pthread_create(&go_away_id, NULL, go_away_thread, NULL)) != 0) {
		log_printf(LOG_CRIT, "Failed to create go_away thread %d %s", ret, xstrerror(ret));
		exit(1);
	}

	/* Wait for all threads to startup by draining the semaphore and then refilling it. */
	for (cnt = 0; cnt < threads; cnt++)
		sem_wait(&search_thread_sema);
	for (cnt = 0; cnt < threads; cnt++)
		sem_post(&search_thread_sema);

	if ((ret = pthread_create(&command_id, NULL, command_thread, &se)) != 0) {
		log_printf(LOG_CRIT, "Failed to create command thread %d %s", ret, xstrerror(ret));
		exit(1);
	}

	if ((ret = setup_controllers(&se, &ctrlthr)) != 0) {
		log_printf(LOG_CRIT, "Failed to setup controllers %d", ret);
		exit(1);
	}

	set_ps_display("engine running");
	eng.running = 1;

	if (child_sock != -1) {
		close(child_sock);
		child_sock = -1;
	}

	eng.epollfd = epoll_create(ENGINE_NUM_FDS);
	if (eng.epollfd < 0) {
		log_printf(LOG_CRIT, "Failed to open epoll fd: %m");
		exit(1);
	}

	eh_add(&eng, eh_command, command_sock, 0);
	eh_add(&eng, eh_keepalive_accept, se.keepalive_socket, 0);
	eh_add(&eng, eh_accept, se.search_socket, 0);

	if (signal_pid) /* Signal loading ok. */
		kill(signal_pid, SIGUSR2);

	log_printf(LOG_INFO, "Child Waiting for command");
	while (eng.running) {
		struct epoll_event events[ENGINE_NUM_FDS];
		int nfds;
		int i;

		/* Wake up every other second to handle quit or status */
		if ((nfds = epoll_wait(eng.epollfd, events, ENGINE_NUM_FDS, 2000 /* ms */)) < 0) {
			if (errno == EINTR)
				continue;
			log_printf(LOG_CRIT, "Error calling epoll_wait (%d): %m", eng.epollfd);
			break;
		}
		
		for (i = 0; i < nfds; ++i) {
			struct event_handler *eh = events[i].data.ptr;
			eh->cb(eh, &se);
		}

		if (quit_signaled) {
			log_printf(LOG_INFO, "Child Recieved quit signal %d", quit_signaled);
			break;
		}
		if (status_signaled) {
			log_printf(LOG_INFO, "Child Recieved status signal %d", status_signaled);
			/* search_dump_stats(); */
			/* stat_print(1); */
		}
	}

	set_ps_display("engine shutting down");
	log_printf(LOG_INFO, "Engine shutting down");

	/*
	 * Tell all threads to go die.
	 */
	pthread_mutex_lock(&search_mutex);
	while ((thread = SLIST_FIRST(&search_thread_list)) != NULL)
		SLIST_REMOVE_HEAD(&search_thread_list, list);
	TAILQ_FOREACH(thread, &allthreads, thread_list) {
		thread->die = 1;
		pthread_cond_signal(&thread->cond);
	}
	pthread_mutex_unlock(&search_mutex);

	while ((thread = TAILQ_FIRST(&allthreads)) != NULL) {
		void *v;

		TAILQ_REMOVE(&allthreads, thread, thread_list);
		pthread_join(thread->chatter_id, &v);
		free(thread);
	}

	stat_print(1);
	timer_foreach(timer_dump, NULL);
	stat_counters_foreach(stat_dump, NULL);

	log_printf(LOG_INFO, "Waiting for signals done");
	/* 
	 * XXX - check if it should be rel.
	 */
	search_instance_destroy(se.current_si);
	search_instance_config_drop(sic);

	/*
	 * Shouldn't close the log since our children might still be running for a short while.
	 */
	log_shutdown();
	timer_clean();

	close(eng.epollfd);

	if (ctrlthr)
		ctrl_thread_quit(ctrlthr);
	/*
	 * Explicitly close the listening sockets before letting exit deal with
	 * the other sockets so that we know they are closed when the
	 * pending command transaction socket is closed.
	 */
	close(se.search_socket);
	close(se.keepalive_socket);
	close(se.command_socket);

	/* reached by error calling epoll_wait or by quit_singal() */
	if (quit_signaled != SIGTERM)
		return 1;

	return 0;
}

int
wait_for_fd(struct chatter *thread, struct search_conn *conn) {
	pthread_mutex_lock(&search_mutex);

	if (thread->connection_sock >= 0) {
		conn->si = thread->si;
		conn->read_to_ms = thread->keepalive ? 0 : thread->si->sic->read_timeout;
		conn->is_keepalive = thread->keepalive ? 1 : 0;
		pthread_mutex_unlock(&search_mutex);
		return thread->connection_sock;
	}

	if (conn->si && search_instance_rel(conn->si)) {
		pthread_mutex_unlock(&search_mutex);
		search_instance_destroy(conn->si);
		pthread_mutex_lock(&search_mutex);
	}

	if (thread->die) {
		pthread_mutex_unlock(&search_mutex);
		return -1;
	}
	SLIST_INSERT_HEAD(&search_thread_list, thread, list);
	release_reserve(thread);
	thread->reserve_released = 0;

	pthread_cond_wait(&thread->cond, &search_mutex);
	pthread_mutex_unlock(&search_mutex);
	if (thread->die)
		return -1;

	conn->si = thread->si;
	conn->read_to_ms = thread->keepalive ? 0 : thread->si->sic->read_timeout;
	conn->is_keepalive = thread->keepalive ? 1 : 0;
	return thread->connection_sock;
}

void
release_fd(struct chatter *thread, int hup) {
	release_eh(thread->keepalive, thread->connection_sock, hup);
	thread->connection_sock = -1;
}

void
release_reserve(struct chatter *thread) {
	if (thread->reserve_released)
		return;
	thread->reserve_released = 1;
	sem_post(&search_thread_sema);
}

void
release_eh(struct event_handler *eh, int fd, int hup) {
	struct epoll_event event = {EPOLLIN, {0}};

	if (eh && !hup) {
		event.events = EPOLLIN|EPOLLHUP|EPOLLONESHOT;
		event.data.ptr = eh;
		if (epoll_ctl(eh->eng->epollfd, EPOLL_CTL_MOD, fd, &event) < 0) {
			log_printf(LOG_CRIT, "Error re-adding keepalive fd to epoll set: %m");
			free(eh);
			close(fd);
		}
	} else {
		struct pollfd pollfd;

		/*
		 * This is run from the chatter thread, so sleeping here is ok.
		 */

		log_printf(LOG_DEBUG, "shutting down fd %d from chatter", fd);

		shutdown(fd, SHUT_WR);

		pollfd.fd = fd;
		pollfd.events = POLLIN;
		pollfd.revents = POLLIN|POLLHUP;

		if (poll(&pollfd, 1, 2000) == 1) {
			char dummy[512];
			if (read(fd, dummy, 512) == -1)
				;
		}

		close(fd);

		if (eh) {
			pthread_mutex_lock(&eh_mutex);
			TAILQ_REMOVE(&eh_list, eh, list);
			pthread_mutex_unlock(&eh_mutex);
			free(eh);
		}
	}
}
