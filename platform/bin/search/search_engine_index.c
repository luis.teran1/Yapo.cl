#include <sys/stat.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <curl/curl.h>
#include <math.h>
#include <assert.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>

#include <stat_messages.h>
#include <stat_counters.h>
#include <index.h>
#include <logging.h>
#include <bconf.h>

#include "search.h"

struct index_receiver {
	size_t sz;
	size_t pos;
	char *d;
	CURL *curl;
	int fd;
};

static size_t
curl_wr(char *ptr, size_t size, size_t nmemb, void *v) {
	struct index_receiver *ir = v;
	size_t wsz = size * nmemb;

	if (ir->sz == 0) {
		double libcurl_is_really_stupid;
		size_t non_stupid_size;

		if (curl_easy_getinfo(ir->curl, CURLINFO_CONTENT_LENGTH_DOWNLOAD, &libcurl_is_really_stupid) != CURLE_OK || libcurl_is_really_stupid < 1.0) {
			log_printf(LOG_CRIT, "fetch_from_indexer: no (or bad) content-length");
			return 0;
		}
		assert((libcurl_is_really_stupid - nextafter(libcurl_is_really_stupid, 0)) < 1.0);
		non_stupid_size = libcurl_is_really_stupid;
		ir->sz = non_stupid_size;
		if (ftruncate(ir->fd, ir->sz) == -1) {
			log_printf(LOG_CRIT, "fetch_from_indexer: frtuncate: %m");
			return 0;
		}
		if ((ir->d = mmap(NULL, ir->sz, PROT_READ|PROT_WRITE, MAP_FILE|MAP_SHARED, ir->fd, 0)) == MAP_FAILED) {
			log_printf(LOG_CRIT, "fetch_from_indexer: mmap: %m");
			return 0;
		}
       	}
	assert(ir->pos + wsz <= ir->sz);

	memcpy(&ir->d[ir->pos], ptr, wsz);
	ir->pos += wsz;

	return wsz;
}

static int
fetch_from_indexer(struct search_engine_index *sei) {
	struct index_receiver ir = { 0 };
	char *new_fname;
	long status;
	CURLcode r;
	char *url;

	xasprintf(&new_fname, "%s.new", sei->fname);

	log_printf(LOG_INFO, "fetching index %s from indexer into file %s", sei->indexer_url, new_fname);
	if ((ir.fd = open(new_fname, O_RDWR|O_CREAT|O_EXCL|O_TRUNC, 0644)) == -1) {
		log_printf(LOG_CRIT, "fetch_from_indexer: open(%s): %m", sei->fname);
		free(new_fname);
		return -1;
	}

	if (sei->index != NULL) {
		xasprintf(&url, "%s&have_csum=%s", sei->indexer_url, index_get_checksum_string(sei->index));
	} else {
		xasprintf(&url, "%s", sei->indexer_url);
	}

	ir.curl = curl_easy_init();
	curl_easy_setopt(ir.curl, CURLOPT_URL, url);
	curl_easy_setopt(ir.curl, CURLOPT_WRITEFUNCTION, curl_wr);
	curl_easy_setopt(ir.curl, CURLOPT_WRITEDATA, &ir);
	r = curl_easy_perform(ir.curl);

	curl_easy_getinfo(ir.curl, CURLINFO_RESPONSE_CODE, &status);
	curl_easy_cleanup(ir.curl);
	free(url);
	if (r == 0 && status == 200) {
		if (rename(new_fname, sei->fname)) {
			log_printf(LOG_CRIT, "fetch_from_indexer: rename [%s] -> [%s] failed: %m", new_fname, sei->fname);
		}
		search_engine_index_set_index(sei, open_mem_index_reader(ir.d, ir.sz, 1), 1, NULL);
		free(new_fname);
		close(ir.fd);
		return 0;
	}

	remove(new_fname);
	close(ir.fd);
	free(new_fname);

	if (ir.d) {
		if (munmap(ir.d, ir.sz) != 0) {
			log_printf(LOG_CRIT, "fetch_from_indexer: unmapping temporary index mapping failed: %m (you'll soon run out of disk and/or memory)");
		}
	}

	
	if (r != 0) {
		log_printf(LOG_CRIT, "fetch_from_indexer: fetching %s failed: %s", sei->indexer_url, curl_easy_strerror(r));
	} else if (status == 204) {
		log_printf(LOG_INFO, "fetch_from_indexer: indexes in sync: %s", sei->indexer_url);
		return 0;
	}

	return -1;
}

void
search_engine_index_set_index(struct search_engine_index *sei, struct index_db *index, int close, struct timer_instance *ti) {
	if (ti)
		timer_handover(ti, "close_old");
	if (close && sei->index) {
		close_index(sei->index, 0);
	}
	if (ti)
		timer_handover(ti, "set_messages");
	sei->index = index;
	if (sei->index) {
		stat_message_printf(sei->to, "%s", index_get_to_id(sei->index));
		stat_message_printf(sei->checksum, "%s", index_get_checksum_string(sei->index));
		STATCNT_SET(sei->ndocs, index_get_num_docs(sei->index));
	} else {
		stat_message_printf(sei->to, "");
		stat_message_printf(sei->checksum, "");
		STATCNT_SET(sei->ndocs, 0);
	}
}

int
search_engine_index_setup(struct bconf_node *conf, struct search_engine_index *sei, const char *index_name, int force_in_sync) {
	const char *workdir;
	const char *host;
	const char *port;

	if ((workdir = bconf_get_string(conf, "search.daemon.workdir")) == NULL) {
		log_printf(LOG_CRIT, "search.daemon.workdir not configured for indexer daemon mode in search engine");
		return -1;
	}
	if ((host = bconf_get_string(conf, "index.controller.host")) == NULL) {
		log_printf(LOG_CRIT, "index.controller.host not configured for indexer daemon mode in search engine");
		return -1;
	}
	if ((port = bconf_get_string(conf, "index.controller.port")) == NULL) {
		log_printf(LOG_CRIT, "index.controller.port not configured for indexer daemon mode in search engine");
		return -1;
	}

	mkdir(workdir, 0755);

	sei->checksum = stat_message_dynamic_alloc(4, "search", "index", index_name, "checksum");
	sei->to = stat_message_dynamic_alloc(4, "search", "index", index_name, "to");
	sei->ndocs = stat_counter_dynamic_alloc(4, "search", "index", index_name, "ndocs");
	xasprintf(&sei->fname, "%s/%s", workdir, index_name);
	xasprintf(&sei->indexer_url, "http://%s:%s/get_index?index=%s", host, port, index_name);
	search_engine_index_set_index(sei, open_index(sei->fname, INDEX_READER|INDEX_FILE), 1, NULL);	/* open_index is allowed to fail. */

	/* Clean up the workdir from previously failed index creation. */
	char fname[PATH_MAX];
	snprintf(fname, sizeof(fname), "%s.new", sei->fname);
	remove(fname);

	/*
	 * force_in_sync means that even if we have an index here, we need to make sure that it matches what the indexer wants us to have.
	 */
	if (force_in_sync) {
		useconds_t to = 250000L;	/* First timeout is 0.25 s, doubling 6 times. This gives us 0.25, 0.5, 1, 2, 4, 8 = 15.75s total wait. */
		int retry = 6;
		int ret;

		do {
			ret = fetch_from_indexer(sei);
			if (ret) {
				usleep(to);
				to *= 2;
			}
		} while (ret && retry--);
		return ret;
	}
	return 0;
}

