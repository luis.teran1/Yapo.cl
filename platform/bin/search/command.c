#include <sys/types.h>
#include <sys/poll.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <stdio.h>
#include <signal.h>

#include "search_lexer.h"
#include "search.h"
#include "queue.h"
#include "sock_util.h"
#include "util.h"
#include "logging.h"
#include "timer.h"
#include "db.h"
#include "lru.h"
#include "bconf.h"

#include "command.h"
#include "stat_counters.h"

static void handle_command(int, struct search_engine *);
struct param {
	TAILQ_ENTRY(param) p_list;
	char *key;
	char *value;
};

struct command {
	TAILQ_HEAD(,param) params;
};

static char*
get_value(struct command *cmd, const char *key) {
	struct param *param = NULL;

	TAILQ_FOREACH(param, &cmd->params, p_list) {
		if (strcmp(param->key, key) == 0)
			return param->value;
	}

	return NULL;
}

static void
timer_dump(struct timer_class *tc, void *data) {
	FILE *cmd_file = data;
	struct timespec avgts;
	double avg = (double)tc->tc_total.tv_sec + (double)tc->tc_total.tv_nsec / 1000000000.0;
	char *name, *n;

	if (tc->tc_count == 0)
		return;

	n = name = xstrdup(tc->tc_name);

	while ((n = strchr(n, '.')) != NULL)
		*n = '_';

	avg /= (double)tc->tc_count;

	avgts.tv_sec = avg;
	avgts.tv_nsec = (avg - (double)avgts.tv_sec) * 1000000000.0;

	fprintf(cmd_file, "timer.%s.count:%lld\n", name, tc->tc_count);
	fprintf(cmd_file, "timer.%s.bytes:%llu\n", name, (unsigned long long)tc->tc_counter);
	fprintf(cmd_file, "timer.%s.total:%ld.%03ld\n", 
			name, tc->tc_total.tv_sec, tc->tc_total.tv_nsec / 1000000);
	fprintf(cmd_file, "timer.%s.min:%ld.%03ld\n", 
			name, tc->tc_min.tv_sec, tc->tc_min.tv_nsec / 1000000);
	fprintf(cmd_file, "timer.%s.max:%ld.%03ld\n", 
			name, tc->tc_max.tv_sec, tc->tc_max.tv_nsec / 1000000);
	fprintf(cmd_file, "timer.%s.average:%ld.%03ld\n", 
			name, avgts.tv_sec, avgts.tv_nsec / 1000000);
	free(name);
}

static void
stat_dump(void *data, uint64_t cnt, const char **name) {
	FILE *cmd_file = data;
	int i;

	for (i = 0; name[i] != NULL; i++) {
		fprintf(cmd_file, "%s%c", name[i], name[i + 1] != NULL ? '.' : ':');
	}
	fprintf(cmd_file, "%llu\n", (unsigned long long)cnt);
}

#if 0
static void
warm_one(struct lru_entry *e, void *v) {
	struct search_instance *si = v;
	struct result res;
	ParseNode *node;

	log_printf(LOG_DEBUG, "warming cache: %s", e->key);

	memset(&res, 0, sizeof(res));
	res.docoff_arr_sz = 100;
	res.docoff_arr = xmalloc(res.docoff_arr_sz * sizeof(*res.docoff_arr));
	res.sort_value_arr = xmalloc(res.docoff_arr_sz * sizeof(*res.sort_value_arr));
	res.si = si;

	int sortspec;
	if ((node = reconstruct_search(si->db, e->key, &sortspec)) != NULL) {
		node = main_search(&res, node, NULL, false, NULL, NULL);
		cleanup_search(node);
	}

	free(res.docoff_arr);
	free(res.headers.buf);
}
#endif

static void
warm_cache(struct search_instance *new, struct search_instance *old) {
#if 0
	if (!bconf_get_int(new->bconf, "dont_warm_cache"))
		lru_foreach(old->query_cache, warm_one, new);
#endif
}

void *
command_thread(void *v) {
	struct search_engine *se = v;
	struct sockaddr_storage sas;
	socklen_t slen = sizeof(sas);
	int sock;

	log_register_thread("Command %d", (int)getpid());

	while (1) {
		struct timer_instance *command_timer;

		if ((sock = accept(se->command_socket, (struct sockaddr *)&sas, &slen)) == -1) {
			log_printf(LOG_CRIT, "Error on accept(%m)");
			pthread_exit(NULL);
		}
		command_timer = timer_start(NULL, "command");

		handle_command(sock, se);

		timer_end(command_timer, NULL);
	}
}

static void
handle_command(int fd, struct search_engine *se) {
	struct command cmd;
	FILE *cmd_file;
	cmd_file = fdopen(fd, "r+");
	struct param *param = NULL;
	char *value;
	char *line;
	ssize_t sz;
	size_t s;

	TAILQ_INIT(&cmd.params);

	fprintf(cmd_file, "220 Welcome.\n");
	fflush(cmd_file);

	line = NULL;
	s = 0;
	while ((sz = getline(&line, &s, cmd_file)) != -1) {
		char *eol;
		char *key;

		eol = &line[sz - 1];
		*eol = '\0';

		key = line;
		value = strchr(key, ':');
		if (value != NULL)
			*value++ = '\0';

		if (*key == '\0')
			continue;

		if (strcmp(key, "end") == 0) {
			free(key);
			break;
		}

		param = xmalloc(sizeof(*param));
		param->key = key;
		param->value = xstrdup(value ? value : "");
		log_printf(LOG_DEBUG, "CMD: %s -> %s", param->key, param->value);
		TAILQ_INSERT_TAIL(&cmd.params, param, p_list);

		line = NULL;
		s = 0;
	}

	if ((value = get_value(&cmd, "cmd"))) {
		if (strcmp(value, "hup") == 0) {
			log_printf(LOG_INFO, "reloading index");
			extern pthread_mutex_t search_mutex;
			struct search_instance *old_si;
			struct search_instance *si;
			static const char *immutable_bconf[] = {
				"threads",
				"port.search",
				"port.keepalive",
				"port.command"
			};
			unsigned int i;

			if ((si = search_instance_init(se, NULL, NULL)) == NULL) {
				log_printf(LOG_CRIT, "Failed to reinit search instance, restarting");
				goto restart;
			}

			/*
			 * Go through the bconf keys that we can't deal with being changed.
			 * If any of them changed, just restart.
			 */
			log_printf(LOG_DEBUG, "immutable bconfs: %lu", sizeof(immutable_bconf)/sizeof(immutable_bconf[0]));
			for (i = 0; i < sizeof(immutable_bconf)/sizeof(immutable_bconf[0]); i++) {
				const char *old = bconf_get_string(si->sic->bconf, immutable_bconf[i]);
				const char *new = bconf_get_string(se->current_si->sic->bconf, immutable_bconf[i]);
				if (((old && new) && strcmp(old, new)) || (!old && new) || (old && !new)) {
					log_printf(LOG_INFO, "Unhadled bconf mismatch: %s : '%s' != '%s', restarting", immutable_bconf[i], old, new);
					goto restart;
				}
			}

			/*
			 * We are the only ones who can drop the last reference
			 * to current_si, so there's no need to lock or ref it.
			 */
			warm_cache(si, se->current_si);

			pthread_mutex_lock(&search_mutex);
			old_si = se->current_si;
			se->current_si = si;
			if (!search_instance_rel(old_si))
				old_si = NULL;
			pthread_mutex_unlock(&search_mutex);
			if (old_si)
				search_instance_destroy(old_si);
		} else if (strcmp(value, "transinfo") == 0) {
			timer_foreach(timer_dump, cmd_file);
			stat_counters_foreach(stat_dump, cmd_file);
		} else if (strcmp(value, "reload") == 0) {
			/*
			 * Restart is managed by telling the parent to restart and then
			 * hanging reading the fd until we're killed or until the
			 * requester gives up. This way, the fd won't get closed until
			 * this process is dead which only happens when the new process
			 * is running. Stop is the same, but sending a TERM to the
			 * parent.
			 *
			 * Unfortunately, poll can't tell us that the other side has
			 * closed the read side of the socket (our write side). It might
			 * be a bug in the kernel or something, the result is
			 * that we have to leak the fd and hope it doesn't happen too
			 * often.
			 */
restart:
			kill(getppid(), SIGHUP);
			goto out_noclose;
		} else if (strcmp(value, "stop") == 0) {
			kill(getpgrp(), SIGTERM);
			goto out_noclose;
		} else if (strcmp(value, "ready") == 0) {
			fprintf(cmd_file, "ok\n");
		} else if (strcmp(value, "flush_cache") == 0) {
			lru_flush(se->current_si->query_cache);
			lru_flush(se->current_si->subquery_cache);
			lru_flush(se->current_si->attrsort_cache);
			fprintf(cmd_file, "ok\n");
		}
	}

	if (fd)
		fclose(cmd_file);

out_noclose:
	while ((param = TAILQ_FIRST(&cmd.params)) != NULL) {
		TAILQ_REMOVE(&cmd.params, param, p_list);
		free(param->key);
		free(param->value);
		free(param);
	}
}
