#ifndef CHATTER_H
#define CHATTER_H

#ifdef __cplusplus
extern "C" {
#endif

struct bsearch;
struct bconf_node;
struct search_instance;

const char* read_input(int fd, char *input, int input_len, int skip_poll, int is_keepalive);

void *chatter_thread(void *thread_data);
void *chatter_stdin(struct search_instance *);
void free_res(void *);

void chatter_done(struct bsearch *chatter_data, void *exitptr);

#ifdef __cplusplus
}
#endif

#endif /*CHATTER_H*/
