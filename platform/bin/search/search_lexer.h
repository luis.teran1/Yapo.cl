#ifndef SEARCH_LEXER_H
#define SEARCH_LEXER_H

#ifdef __cplusplus
class ParseNode;
extern "C" {
#else
typedef struct ParseNode ParseNode;
#endif

#include "index.h"
#include "sortspec.h"

struct search_instance;

struct counter_union_list_node {
	int32_t doc;
	struct counter_union_list_node *next;
};

struct counter_union_list {
	struct counter_union_list_node *head;
};

struct parse_result {
	unsigned int offset;
	unsigned int limit;
	int union_query;

	struct {
		int middle;
		int before;
		int after;
	} idrange;

	struct {
		int id;
		int order;
		int suborder;
		int enabled;
	} paging;

	int randomize;
	int rand_seed;
	int print_parse;
	ParseNode *query;
	int conf_rev_size;
	char *conf_rev[10];
	struct counter_union_list counter_union_list;

	char *tag;

	char *error;
};

int parse(struct search_instance *si, struct parse_result *result, const char *str, void *cfdata);

#ifdef __cplusplus
}
#endif

#endif
