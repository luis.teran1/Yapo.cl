#include <string.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <errno.h>
#include <stdio.h>
#include <fcntl.h>
#include <dirent.h>
#include <unistd.h>
#include <signal.h>
#include "db.h"
#include "subs.h"
#include "index.h"
#include "logging.h"
#include "chatter.h"
#include "util.h"
#include "hash.h"
#include "stats.h"
#include "timer.h"
#include "atomic.h"
#include "bconf.h"
#include "charmap.h"
#include <assert.h>

static int abort_signaled = 0;

static void abort_handler(int sig) {
	abort_signaled = 1;
}

static int
doc_cmp(const struct avl_node *an, const struct avl_node *bn) {
	const struct cache_doc *a = avl_data(an, const struct cache_doc, tree);
	const struct cache_doc *b = avl_data(bn, const struct cache_doc, tree);

	return a->id - b->id;
}

static int
word_cmp(const struct avl_node *an, const struct avl_node *bn) {
	const struct invword *a = avl_data(an, const struct invword, tree);
	const struct invword *b = avl_data(bn, const struct invword, tree);
	int kdiff = a->wlen - b->wlen;

	if (kdiff)
		return kdiff;
	return memcmp(a->word, b->word, a->wlen);
}

static int
attr_cmp(const struct avl_node *an, const struct avl_node *bn) {
	const struct invattr *a = avl_data(an, const struct invattr, tree);
	const struct invattr *b = avl_data(bn, const struct invattr, tree);
	const char *ap = a->attr;
	const char *bp = b->attr;
	char *aep, *bep;
	int res;

	do {
		int al, bl;

		aep = strchrnul(ap, ':');
		bep = strchrnul(bp, ':');

		al = aep - ap;
		bl = bep - bp;

		if ((res = al - bl) != 0)
			return res;
		if ((res = memcmp(ap, bp, al)) != 0)
			return res;
		if (!*aep || !*bep)
			return (*aep - *bep);
		ap = aep + 1;
		bp = bep + 1;
	} while (1);
}


static void
cache_words_cb(int i, const char *key, const struct docindex *docarr, int ndocs, const struct docpos *docpos, void *v) {
	struct bsearch_cache *db = v;
	struct invword *iw = &db->words.wordarr[i];

	iw->word = key;
	iw->wlen = strlen(key);
	iw->docs = docarr;
	iw->ndocs = ndocs;
	iw->docpos = docpos;

	avl_insert(&iw->tree, &db->words.words, word_cmp);
	subs_register(db, iw->word);
}

static int
cache_words(struct bsearch_cache *db, struct charmap *cm) {
	unsigned int nwords;
	size_t tot_word_len;

	db->words.it = index_words_iter_open(db->index, 1, &nwords, &tot_word_len);

	log_printf(LOG_DEBUG, "cache_words: %d", nwords);

	db->subs = subs_init(nwords, tot_word_len, charmap_ndst(cm));
	db->words.words = NULL;
	db->words.wordarr = xmalloc(nwords * sizeof(struct invword));

	if (db->words.it)
		index_words_iter_foreach(db->words.it, cache_words_cb, db);

	return 0;
}

static void
cache_attrs_cb(int i, const char *key, const int32_t *docarr, int ndocs, void *v) {
	struct bsearch_cache *db = v;
	struct invattr *ia = &db->attributes.attrarr[i];

	ia->attr = key;
	ia->docs = docarr;
	ia->ndocs = ndocs;

	avl_insert(&ia->tree, &db->attributes.attrs, attr_cmp);
}

static int
cache_attributes(struct bsearch_cache *db) {
	unsigned int nattrs;

	db->attributes.it = index_attrs_iter_open(db->index, 1, &nattrs);

	log_printf(LOG_DEBUG, "cache_attributes: %d", nattrs);

	db->attributes.attrs = NULL;
	db->attributes.attrarr = xmalloc(nattrs * sizeof(struct invattr));

	if (db->attributes.it)
		index_attrs_iter_foreach(db->attributes.it, cache_attrs_cb, db);

	return 0;
}

static void
cache_docs_cb(int i, const struct doc *key, int32_t docoff, const char *value, void *v) {
	struct bsearch_cache *db = v;
	struct cache_doc *docp = &db->docs.docarr[docoff];

	assert(i == docoff);

	docp->id = key->id;
	docp->document = value;
	avl_insert(&docp->tree, &db->docs.docs, doc_cmp);
}

static int
cache_docs(struct bsearch_cache *db) {
	unsigned int ndocs;

	db->docs.it = index_docs_iter_open(db->index, 1, &ndocs);
	db->docs.ndocs = ndocs;

	log_printf(LOG_DEBUG, "cache_docs %d", ndocs);

	db->docs.docs = NULL;
	db->docs.docarr = xmalloc(ndocs * sizeof(struct cache_doc));
	if (db->docs.it)
		index_docs_iter_foreach(db->docs.it, cache_docs_cb, db);

	return 0;
}

/*
 * init_bserch:
 * loads the datafiles into memory and builds the appropriate hash-tables.
 */

static int
init_bsearch(struct bsearch_cache *db, struct charmap *cm) {

	log_printf(LOG_INFO, "Cache words");
	if (cache_words(db, cm) != 0)
		return 1;
	log_printf(LOG_INFO, "Cache words done");

	log_printf(LOG_INFO, "Cache attributes");
	if (cache_attributes(db) != 0)
		return 1;
	log_printf(LOG_INFO, "Cache attributes done");

	log_printf(LOG_INFO, "Cache docs");
	if (cache_docs(db) != 0)
		return 1;
	log_printf(LOG_INFO, "Cache docs done");

	return 0;
}

/*
 * free_cache:
 * Clean the cache struct.
 */
static void
free_cache_docs(struct bsearch_cache *db) {
	if (db->docs.it)
		index_docs_iter_close(db->docs.it);
	free(db->docs.docarr);
}

static void
free_cache_words(struct bsearch_cache *db) {
	if (db->words.it)
		index_words_iter_close(db->words.it);
	free(db->words.wordarr);
}

static void
free_cache_attrs(struct bsearch_cache *db) {
	if (db->attributes.it)
		index_attrs_iter_close(db->attributes.it);
	free(db->attributes.attrarr);
}

static int
free_cache(struct bsearch_cache *db) {
	free_cache_attrs(db);
	free_cache_words(db);
	free_cache_docs(db);
	subs_free(db);

	return 0;
}

int
db_ndocs(struct bsearch_cache *db) {
	return db->docs.ndocs - index_get_num_subdocs(db->index);
}

int32_t
load_docoff(struct bsearch_cache *db, uint32_t id) {
	struct cache_doc s;
	struct avl_node *n;

	s.id = id;

	n = avl_lookup(&s.tree, &db->docs.docs, doc_cmp);
	if (n == NULL)
		return NULL_DOCOFF;

	return avl_data(n, struct cache_doc, tree) - db->docs.docarr;
}

/* Fetch a document from storage */
const char *
load_doc_body(struct bsearch_cache *db, int32_t docoff) {
	return db->docs.docarr[docoff].document;
}

const struct doc *
get_doc(struct bsearch_cache *db, int32_t docoff) {
	return index_get_doc(db->index, docoff);
}

const char *
get_doc_header(struct bsearch_cache *db) {
	return index_get_doc_header(db->index);
}

int
is_stopword(struct bsearch_cache *db, const char *kw) {
	return index_is_stopword(db->index, kw);
}

int
get_oper(struct bsearch_cache *db, const char *slang, const char *kw) {
	return index_get_oper(db->index, slang, kw);
}

const char *
get_conf_rev(struct bsearch_cache *db) {
	return index_get_conf_footprint(db->index);
}

void
debug_set_conf_rev(struct bsearch_cache *db, const char *debug_conf_rev) {
	index_debug_set_conf_rev(db->index, debug_conf_rev);
}

const char *
get_config(struct bsearch_cache *db, size_t *sz) {
	return index_get_config(db->index, sz);
}

const char *
get_timestamp(struct bsearch_cache *db) {
	return index_get_timestamp(db->index);
}

const char *
get_index_to_id(struct bsearch_cache *db) {
	return index_get_to_id(db->index);
}


struct invword *
find_invword(struct bsearch_cache *db, const char *word) {
	struct invword s;
	struct avl_node *n;

	s.word = word;
	s.wlen = strlen(word);
	n = avl_lookup(&s.tree, &db->words.words, word_cmp);
	if (n == NULL)
		return NULL;
	return avl_data(n, struct invword, tree);
}

struct invattr *
find_invattr(struct bsearch_cache *db, char *attr) {
	struct invattr sia;
	struct avl_node *n;

	sia.attr = attr;
	n = avl_lookup(&sia.tree, &db->attributes.attrs, attr_cmp);
	if (n == NULL)
		return NULL;
	return avl_data(n, struct invattr, tree);
}

void
invattr_iter_init(struct bsearch_cache *db, struct invattr_iter *iai, const char *attr, const char *start, const char *end) {
	char *at;

	/*
	 * Careful about open-ended attribute ranges.
	 *
	 * Start is easy to handle. "attr:" is strictly smaller than any attribute that starts with attr: so we can just use
	 * that.
	 * End is harder. There's no way to generate a key in the tree that will be larger than any key starting with "attr:",
	 * but smaller than other keys. For those ranges, we create an unterminated avl tree iterator and terminate it early
	 * in invattr_iter_next as soon as we get a key that doesn't start with "attr:". "elen" is dual-purpose, it's a
	 * flag to tell invattr_iter_next that this early termination needs to be done and it's the length of "attr:".
	 * iai->e.attr is also dual-purpose. It either contains the string we're looking for or the normal end attribute for
	 * close-ended ranges.
	 */

	xasprintf(&at, "%s:%s", attr, start ? start : "");
	iai->s.attr = at;

	if (end == NULL) {
		iai->elen = xasprintf(&at, "%s:", attr);
	} else {
		xasprintf(&at, "%s:%s", attr, end ? end : "");
		iai->elen = 0;
	}
	iai->e.attr = at;

	avl_it_init2(&iai->it, db->attributes.attrs, &iai->s.tree, end ? &iai->e.tree : NULL, 1, attr_cmp);
}

void
invattr_iter_del(struct invattr_iter *iai) {
	free((void *)iai->s.attr);
	free((void *)iai->e.attr);
}

struct invattr *
invattr_iter_next(struct invattr_iter *iai) {
	struct avl_node *n;
	struct invattr *ia;

	if ((n = avl_it_next(&iai->it)) == NULL)
		return NULL;
	ia = avl_data(n, struct invattr, tree);
	if (iai->elen != 0 && strncmp(ia->attr, iai->e.attr, iai->elen))
		return NULL;

	return ia;
}

int
db_cache_lock(struct bsearch_cache *db) {
	return pthread_rwlock_tryrdlock(&db->rwlock);
}

void
db_cache_unlock(struct bsearch_cache *db) {
	pthread_rwlock_unlock(&db->rwlock);
}

struct bsearch_cache *
db_init(const char *db_name, struct bconf_node *bconf, struct charmap *cm, int file, struct index_db *index) {
	struct bsearch_cache *db;
	sigset_t init_set;
	sigset_t old_set;

	sigemptyset(&init_set);
	sigaddset(&init_set, SIGTERM);
	sigprocmask(SIG_UNBLOCK, &init_set, &old_set);
	signal(SIGTERM, abort_handler);

	db = zmalloc(sizeof(*db));

	pthread_rwlock_init(&db->rwlock, NULL);

	log_printf(LOG_INFO, "Open index");
	if (index == NULL) {
		if ((db->index = open_index(db_name, INDEX_READER|(file ? INDEX_FILE:0))) == NULL) {
			log_printf(LOG_CRIT, "Failed to open index %s", db_name);
			return NULL;
		}
	} else {
		db->index = index;
	}
	log_printf(LOG_INFO, "Open index done");

	if (init_bsearch(db, cm) != 0) {
		free(db);
		return NULL;
	}

	sigprocmask(SIG_SETMASK, &old_set, NULL);

	return db;
}

void
db_cleanup(struct bsearch_cache *db) {
	pthread_rwlock_wrlock(&db->rwlock);
	free_cache(db);
	close_index(db->index, 0);

	pthread_rwlock_unlock(&db->rwlock);
	pthread_rwlock_destroy(&db->rwlock);
	free(db);
}

