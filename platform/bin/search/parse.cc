
#include "charmap.h"
#include "hash.h"
#include "hunstem.h"
#include "langmap.h"
#include "parse_nodes.hh"
#include "parse_node_cache.hh"
#include "parse_node_regress_delay.hh"
#include "parse_node_suggest.hh"
#include "search.h"
#include "search_lexer.h"
#include "unicode.h"
#include "text.h"

#include <iterator>
#include <memory>
#include <stdexcept>
#include <string>
#include <vector>

#include <string.h>

typedef std::vector<std::unique_ptr<ParseNodeNext>> filter_chain;
typedef std::vector<std::unique_ptr<ParseNode>> node_set;

struct parse_state
{
	struct search_instance *si;
	const char *curptr;
	void *cfdata;
	std::unique_ptr<ParseNodeSuggest> suggest;
	std::unique_ptr<ParseNodeIntersection> cache_root;
	ParseNodeCacheSearch *cache_here;
	int pos_flags;
	bool used_default_attr;
	bool is_fixed_order;
	bool primary; /* A primary query is one for the first page of a normal search */
	bool atc_initialized;
	struct add_text_config atc;
	std::string lang;
};

static char *
dups(const std::string &str) {
	return str.empty() ? NULL : xstrdup(str.c_str());
}

static void
set_lang(parse_state &state, const std::string &l)
{
	state.lang = l;
	state.atc.lang = state.lang.c_str();
	state.atc.hun_obj = hash_table_search(state.si->sic->hun_objs, state.atc.lang, -1, NULL);
}

static std::vector<std::string>
scan_arg_list(parse_state &state, char separator)
{
	const char seps[] = {separator, ' ', '\0'};
	std::vector<std::string> args;

	while (1) {
		const char *argstart = state.curptr;
		size_t argl = strcspn(state.curptr, seps);

		args.emplace_back(argstart, argl);

		state.curptr += argl;
		if (*state.curptr != separator)
			break;
		state.curptr++;
	}

	return args;
}

static void
scan_space(parse_state &state, bool required = false)
{
	const char *start = state.curptr;
	while (isspace(*state.curptr))
		state.curptr++;
	if (required && state.curptr == start && *state.curptr)
		throw std::runtime_error("missing whitespace between parameters");
}

static unsigned int
scan_number(parse_state &state, bool empty_ok = false)
{
	unsigned int n = 0;
	const char *start = state.curptr;

	while (*state.curptr >= '0' && *state.curptr <= '9') {
		if (n > INT_MAX / 10 || (n == INT_MAX / 10 && *state.curptr > INT_MAX % 10))
			throw std::range_error("number overflow");
		n = n * 10 + *state.curptr++ - '0';
	}
	if (!empty_ok && state.curptr == start)
		throw std::runtime_error("number expected");
	return n;
}

static int
scan_signed_number(parse_state &state)
{
	int mult = *state.curptr == '-' ? -1 : 1;
	if (mult == -1)
		state.curptr++;

	return mult * scan_number(state);
}

static std::string
scan_word(parse_state &state)
{
	size_t wl = strcspn(state.curptr, ":()-,\"';|* \002\003");
	std::string word(state.curptr, wl);

	state.curptr += wl;
	return word;
}

static std::string
scan_keyword(parse_state &state)
{
	size_t wl = strcspn(state.curptr, "*\" \002\003");
	std::string word(state.curptr, wl);

	state.curptr += wl;
	return word;
}

static void
expect(parse_state &state, char ch)
{
	if (*state.curptr != ch)
		throw std::runtime_error("expected " + std::string(1, ch));
	state.curptr++;
}

static void
expect_eol(parse_state &state)
{
	scan_space(state);

	if (*state.curptr)
		throw std::runtime_error("expected end of query");
}

static bool
scan_command_flag(parse_state &state)
{
	if (*state.curptr == 'C') {
		state.curptr++;
		return true;
	}
	return false;
}

static std::unique_ptr<ParseNode>
parse_command(parse_state &state)
{
	scan_space(state);

	std::string cmd = scan_word(state);

	if (cmd == "document_count")
		return std::unique_ptr<ParseNode>{new ParseNodeMeta(ParseNodeMeta::DOC_COUNT)};
	if (cmd == "regress_delay")
		return std::unique_ptr<ParseNode>{new ParseNodeRegressDelay()};
	if (cmd == "timestamp")
		return std::unique_ptr<ParseNode>{new ParseNodeEmpty()};
	throw std::runtime_error("invalid command");
}

enum scan_offset_result {
	OFFSET,
	CONFREV,
	IDRANGE,
	PAGING,
	RANDOMIZE,
};
static scan_offset_result
scan_offset(parse_state &state, unsigned int &offset)
{
	switch (*state.curptr) {
	case 'F':
		if (state.curptr[1] == ':') {
			state.curptr += 2;
			return CONFREV;
		}
		break;
	case 'I':
		if (state.curptr[1] == ':') {
			state.curptr += 2;
			return IDRANGE;
		}
		break;
	case 'P':
		if (state.curptr[1] == ':') {
			state.curptr += 2;
			return PAGING;
		}
		break;
	case 'R':
		state.curptr++;
		return RANDOMIZE;
	}

	offset = scan_number(state, true);
	const char *start = state.curptr;
	if (*state.curptr == ':') {
		/* Support numeric attribute keys */
		offset = 0;
		state.curptr = start;
	}
	return OFFSET;
}

static bool scan_unsigned_range(parse_state &state, unsigned int &from, unsigned int &to, unsigned int default_to)
{
	bool to_present = false;

	from = scan_number(state, true);
	expect(state, '-');
	if (*state.curptr >= '0' && *state.curptr <= '9') {
		to = scan_number(state, true);
		to_present = true;
	} else {
		to = default_to;
	}

	return to_present;
}

static void
parse_confrevs(parse_result &result, parse_state &state)
{
	auto args = scan_arg_list(state, ',');
	scan_space(state, true);

	if (result.conf_rev_size + args.size() >= (int)(sizeof(result.conf_rev) / sizeof(result.conf_rev[0])))
		throw std::overflow_error("conf rev");

	for (auto arg = args.begin() ; arg != args.end() ; arg++)
		result.conf_rev[result.conf_rev_size++] = dups(*arg);
}

static void
parse_randomize(parse_result &result, parse_state &state)
{
	result.randomize = 1;
	result.rand_seed = 0;
	if (*state.curptr == '(') {
		state.curptr++;
		result.rand_seed = scan_number(state);
		expect(state, ')');
	}
}

static void
parse_idrange(parse_result &result, parse_state &state)
{
	result.idrange.before = scan_signed_number(state);
	expect(state, ':');
	result.idrange.middle = scan_number(state);
	expect(state, ':');
	result.idrange.after = scan_signed_number(state);
}

static void
parse_paging(parse_result &result, parse_state &state)
{
	result.paging.id = scan_number(state);
	expect(state, ',');
	result.paging.order = scan_number(state);
	expect(state, ',');
	result.paging.suborder = scan_number(state);
	result.paging.enabled = 1;
}

static void
parse_offset(parse_result &result, parse_state &state)
{
	scan_offset_result scan;
	unsigned int offset;
	const char *start = state.curptr;

	while ((scan = scan_offset(state, offset)) == CONFREV)
		parse_confrevs(result, state);

	switch (scan)
	{
	case OFFSET:
		result.offset = offset;
		state.primary = (offset == 0);
		break;
	case RANDOMIZE:
		parse_randomize(result, state);
		break;
	case IDRANGE:
		parse_idrange(result, state);
		break;
	case PAGING:
		parse_paging(result, state);
		state.primary = (result.paging.id == 0);
		break;
	case CONFREV:
		/* silence warning */
		break;
	}
	scan_space(state, state.curptr != start);
}

static std::string
scan_attribute_value(parse_state &state)
{
	std::string value;
	if (*state.curptr == '"' || *state.curptr == '\'') {
		char ch = *state.curptr++;
		const char *end = strchr(state.curptr, ch);
		if (!end)
			throw std::runtime_error("unterminated attribute value");
		value = std::string(state.curptr, end - state.curptr);
		state.curptr = end + 1;
	} else {
		value = scan_word(state);
	}
	return value;
}

static std::unique_ptr<ParseNode>
parse_id_list(parse_state &state, bool isfixed, std::string val)
{
	state.is_fixed_order = isfixed;
	std::vector<int32_t> doclist;

	while (true) {
		int32_t doc = load_docoff(state.si->db, std::stoi(val));
		if (doc != NULL_DOCOFF)
			doclist.push_back(doc);
		if (*state.curptr != ',')
			break;
		state.curptr++;
		val = scan_attribute_value(state);
	}

	if (doclist.empty())
		return std::unique_ptr<ParseNode>(new ParseNodeEmpty());

	size_t n = doclist.size();
	int32_t *docarr = (int32_t*)xmalloc(n * sizeof (*docarr));
	memcpy(docarr, doclist.data(), n * sizeof (*docarr));
	if (!isfixed)
		qsort(docarr, n, sizeof(*docarr), index_docoff_cmp);
	return std::unique_ptr<ParseNode>{new ParseNodeDocarr(docarr, n, isfixed, 1)};
}

static std::string
scan_attribute_name(parse_state &state)
{
	std::string name = scan_word(state);
	expect(state, ':');

	if (!state.used_default_attr && state.si->sic->default_attr_key)
		state.used_default_attr = (name == state.si->sic->default_attr_key);
	return name;
}

static std::unique_ptr<ParseNode>
parse_attribute_range(parse_state &state, const std::string &name, const std::string &val)
{
	state.curptr++;
	std::string val2 = scan_attribute_value(state);
	return std::unique_ptr<ParseNode>{new ParseNodeAttributeRange(dups(name), dups(val), dups(val2))};
}

static std::unique_ptr<ParseNode>
parse_attribute(parse_state &state)
{
	std::string name = scan_attribute_name(state);
	std::string val = scan_attribute_value(state);

	if (*state.curptr == '-')
		return parse_attribute_range(state, name, val);

	if (name == "id" || name == "idfixed")
		return parse_id_list(state, name == "idfixed", std::move(val));

	name += ":";

	if (*state.curptr != ',')
		return std::unique_ptr<ParseNode>{new ParseNodeInvattr(dups(name + val))};

	auto u = new ParseNodeUnion<ParseNodeInvattr>();
	do {
		u->add_node(new ParseNodeInvattr(dups(name + val)));
		state.curptr++;
		val = scan_attribute_value(state);
	} while (*state.curptr == ',');
	u->add_node(new ParseNodeInvattr(dups(name + val)));
	return std::unique_ptr<ParseNode>{u};
}

static std::pair<std::unique_ptr<ParseNode>, bool>
parse_cl_attribute(parse_state &state)
{
	std::string name = scan_attribute_name(state);
	std::string val = scan_attribute_value(state);

	if (*state.curptr == '-')
		return {parse_attribute_range(state, name, val), false};

	name += ":";

	auto u = new ParseNodeUnion<ParseNodeInvattr>();
	while (*state.curptr == ',') {
		u->add_node(new ParseNodeInvattr(dups(name + val)));
		state.curptr++;
		val = scan_attribute_value(state);
	}
	u->add_node(new ParseNodeInvattr(dups(name + val)));
	return {std::unique_ptr<ParseNode>{u}, true};
}

static std::unique_ptr<ParseNode>
parse_attribute_union(parse_state &state)
{
	std::unique_ptr<ParseNode> node = parse_attribute(state);

	if (*state.curptr != '|')
		return node;

	std::unique_ptr<ParseNodeUnion<>> u{new ParseNodeUnion<>()};
	do {
		u->add_node(node.release());
		state.curptr++;
		node = parse_attribute(state);
	} while (*state.curptr == '|');
	u->add_node(node.release());
	return std::move(u);
}

static std::unique_ptr<ParseNode>
parse_attribute_intersection(parse_state &state)
{
	std::unique_ptr<ParseNode> node{parse_attribute_union(state)};

	if (*state.curptr != ';')
		return node;

	std::unique_ptr<ParseNodeIntersection> isect{new ParseNodeIntersection()};
	do {
		isect->add_node(node.release());
		state.curptr++;
		node = parse_attribute_union(state);
	} while (*state.curptr == ';');
	isect->add_node(node.release());
	return std::move(isect);
}

static void
parse_suggest(parse_state &state)
{
	if (state.suggest)
		throw std::runtime_error("duplicate suggest parameter");
	struct suggest_state ss = { 0 };
	ss.misspelled_threshold = -1; /* indicate to use default from conf */
	int pos = 0;
	do {
		if (pos++)
			state.curptr++;
		switch (pos) {
		case 1:
			ss.max_k = scan_number(state, true);
			break;
		case 2:
			ss.db = dups(scan_word(state));
			break;
		case 3:
			ss.category = scan_number(state, true);
			break;
		case 4:
			ss.misspelled_threshold = scan_number(state, true);
			break;
		}
	} while (pos < 4 && strchr(",;", *state.curptr));
	state.suggest.reset(new ParseNodeSuggest(NULL, ss));
	log_printf(LOG_DEBUG, "PARSED SUGGEST: max_k=%d, db=%s, category=%d, misspelled_threshold=%d", ss.max_k, ss.db, ss.category, ss.misspelled_threshold);
}

enum scan_sortspec_mode {
	SINGLE_OR_DOUBLE,
	SINGLE_ONLY,
	DOUBLE_ONLY
};
static int
scan_sortspec(parse_state &state, scan_sortspec_mode mode = SINGLE_OR_DOUBLE)
{
	int ss;

	switch (*state.curptr++) {
	case '-':
		ss = SORTSPEC_DEC;
		break;
	case '+':
		ss = SORTSPEC_INC;
		break;
	default:
		throw std::runtime_error("- or + expected");
	}
	if (mode == SINGLE_ONLY)
		return ss;
	switch (*state.curptr) {
	case '-':
		ss |= SORTSPEC_EMPTY_FIRST;
		state.curptr++;
		break;
	case '+':
		ss |= SORTSPEC_EMPTY_LAST;
		state.curptr++;
		break;
	default:
		if (mode == DOUBLE_ONLY)
			throw std::runtime_error("- or + expected");
		break;
	}
	return ss;
}

static std::unique_ptr<ParseNodeNext>
parse_count_each(parse_state &state, const std::string &tag)
{
	std::string name = scan_word(state);
	expect(state, ':');
	std::string val = scan_attribute_value(state);
	expect(state, '-');
	std::string val2 = scan_attribute_value(state);
	return std::unique_ptr<ParseNodeNext>{new ParseNodeCountEach(NULL, dups(tag), dups(name), dups(val), dups(val2))};
}

enum query_section {
	QS_NONE, QS_PAREN_LEFT, QS_PAREN_RIGHT, QS_TEXT_NORMAL, QS_TEXT_UNION
};
static enum query_section
scan_query_section(parse_state &state)
{
	switch(*state.curptr) {
	case '\002':
		return QS_PAREN_LEFT;
	case '\003':
		return QS_PAREN_RIGHT;
	case '*':
		if (state.curptr[1] == ':' && state.curptr[2] == '*')
			return QS_TEXT_NORMAL;
		if (state.curptr[1] == '-' && state.curptr[2] == '*')
			return QS_TEXT_UNION;
		break;
	}
	return QS_NONE;
}

#include "parameter.h"

static void
parse_parameters(parse_result &result, parse_state &state, bool in_parens, filter_chain &fchain)
{
	while (*state.curptr) {
		if (scan_query_section(state) != QS_NONE)
			break;

		const char *start = state.curptr;
		std::string param = scan_word(state);
		std::string tag;

		if (*state.curptr == '(') {
			state.curptr++;
			tag = scan_word(state);
			expect(state, ')');
		}

		if (param != "count_all") { /* special case count_all has no value */
			if (*state.curptr != ':') {
				/* Not a parameter */
				state.curptr = start;
				break;
			}
			state.curptr++;
		}

		GPERF_ENUM(parameter; bool taggable; bool in_parens)
		const parameter_rec *p = parameter_lookup(param.c_str(), param.length());
		if (!p) {
			/* Not a parameter */
			state.curptr = start;
			break;
		}
		if (!p->taggable && !tag.empty())
			throw std::runtime_error(param + " doesn't accept a tag");
		if (in_parens && !p->in_parens)
			throw std::runtime_error(param + " can't be used in parantheses");

		int ival;
		ParseNodeAttributeList *al = NULL;
		switch (p->val) {
		case GPERF_CASE("lim", false, false):
			result.limit = scan_number(state);
			break;
		case GPERF_CASE("slang", false, true):
			set_lang(state, scan_word(state));
			break;
		case GPERF_CASE("suggest", false, false):
			parse_suggest(state);
			break;
		case GPERF_CASE("hdrs", false, false):
			(void)scan_number(state);
			break;
		case GPERF_CASE("subsort", false, false):
			ival = scan_sortspec(state, SINGLE_ONLY);
			fchain.emplace_back(new ParseNodeSubsort(NULL, SORTSPEC_INT | SORTSPEC_SORTVALUE | ival, NULL));
			break;
		case GPERF_CASE("sort", false, false):
			ival = scan_sortspec(state);
			if (ival & SORTSPEC_EMPTY_MASK) {
				expect(state, ':');
				auto av = parse_cl_attribute(state);
				auto snode = new ParseNodeAttrlistSort(NULL, av.first.release(), SORTSPEC_INT | SORTSPEC_ATTR | ival);
				fchain.emplace_back(new ParseNodeSubsort((ParseNode *)NULL, SORTSPEC_INT | SORTSPEC_ATTR | ival, snode));
			} else {
				fchain.emplace_back(new ParseNodeSubsort(NULL, SORTSPEC_INT | SORTSPEC_ORDER | ival, NULL));
			}
			break;
		case GPERF_CASE("relsort", false, false):
			ival = scan_sortspec(state, SINGLE_ONLY);
			fchain.emplace_back(new ParseNodeRelsort(ival));
			break;
		case GPERF_CASE("distsort", false, false):
			{
				unsigned int x = scan_number(state);
				expect(state, ',');
				unsigned int y = scan_number(state);
				fchain.emplace_back(new ParseNodeDistanceSort(x, y));
			}
			break;
		case GPERF_CASE("sort_slice", false, false):
			{
				unsigned int n = scan_number(state);
				expect(state, ':');
				ival = scan_sortspec(state, DOUBLE_ONLY);
				fchain.emplace_back(new ParseNodeSortSlice(n, ival));
			}
			break;
		case GPERF_CASE("count", true, true):
			{
				auto cnode = parse_attribute_intersection(state);
				fchain.emplace_back(new ParseNodeCounter(NULL, dups(tag), cnode.release(), true,
						in_parens ? &result.counter_union_list : NULL));
			}
			break;
		case GPERF_CASE("count_each", true, false):
			fchain.push_back(parse_count_each(state, tag));
			break;
		case GPERF_CASE("filter", true, false):
			{
				bool neg = *state.curptr == '!';
				if (neg)
					state.curptr++;
				auto fnode = parse_attribute(state);
				fchain.emplace_back(new ParseNodeFilter(NULL, fnode.release(), neg));
			}
			break;
		case GPERF_CASE("bucket", true, false):
			al = new ParseNodeBucket(NULL, dups(tag), tag == "parents");
			/* fall through */
		case GPERF_CASE("count_tree", true, false):
			if (!al)
				al = new ParseNodeCountTree(NULL, dups(tag), tag == "parents");
			/* fall through */
		case GPERF_CASE("count_level", true, false):
			if (!al)
				al = new ParseNodeCountLevel(NULL, dups(tag), tag == "parents");
			ival = 0;
			do {
				if (ival++)
					state.curptr++;
				auto av = parse_cl_attribute(state);
				al->add_node(av.first.release(), false, av.second);
			} while (*state.curptr == ';');
			fchain.emplace_back(al);
			break;
		case GPERF_CASE("next_after", false, false):
			result.idrange.middle = scan_number(state);
			result.idrange.before = -1;
			result.idrange.after = 1;
			break;
		case GPERF_CASE("count_all", true, false):
			fchain.emplace_back(new ParseNodeCountAll(NULL, dups(tag)));
			break;
		case GPERF_CASE("posflags", false, false):
			state.pos_flags = std::stol(scan_word(state), NULL, 0);
			break;
		case GPERF_CASE("print_parse", false, false):
			result.print_parse = scan_number(state);
			break;
		case GPERF_CASE("_tag", true, false):
			result.tag = dups(scan_word(state));
			break;
		case GPERF_CASE_NONE:
			/* silence warning */
			break;
		}
		scan_space(state, true);
	}
}

/*
 * We want different interpretations of the word depending on the state of the keyword parser.
 *
 * default - ATW_LANGMAP > ATW_STEM > ATW_WORD
 * pos_flags - ATW_LANGMAP > ATW_WORD (XXX - why?)
 * inphrase - ATW_WORD
 * subword - ATW_WORD
 *
 */
struct parse_word_data {
	parse_state *ps;
	char *word;
	int word_at;
	enum pwd_mode {
		PWD_LSW,
		PWD_LW,
		PWD_W,
	} mode;

	/*
	 * For some reason suggestions need to know the last op and don't like to be called on subwords.
	 */
	int last_op;
	int no_suggest;
};

static const char *
parse_word_cb(void *v, int at, const char *word, int wordlen)
{
	struct parse_word_data *pw = (struct parse_word_data *)v;

	if (word && wordlen == -1)
		wordlen = strlen(word);

	/* We completely ignore secondary interpretations of words when building a search query for now. */
	if (at & ATW_SECONDARY)
		return NULL;

	/* We need to deal with suggestions here so that we capture the correct word regardless of its later interpretation. */
	if (at == ATW_WORD && !pw->no_suggest && pw->ps->suggest && pw->ps->suggest->ss.max_k && pw->ps->si->suggestions) {
		/* Default to using lang for db if a specific db was not selected */
		if (!pw->ps->suggest->ss.db)
			pw->ps->suggest->ss.db = strdup(pw->ps->atc.lang);
		struct suggestion_candidate* cand = (struct suggestion_candidate*)zmalloc(sizeof(struct suggestion_candidate));
		cand->word = strndup(word, wordlen);
		cand->word_len = charmap_is_utf8(pw->ps->si->sic->charmap) ? strlen_utf8(cand->word) : strlen(cand->word);
		cand->op = (enum iops)pw->last_op;
		TAILQ_INSERT_TAIL(&pw->ps->suggest->ss.candidates, cand, next);
		log_printf(LOG_DEBUG, "Added word '%s' (%d) to suggestion candidate list.", cand->word, cand->word_len);
	}

	if (((pw->mode == parse_word_data::PWD_LW || pw->mode == parse_word_data::PWD_LSW) && at == ATW_LANGMAP) ||
	    ((pw->mode == parse_word_data::PWD_LSW) && at == ATW_STEM && pw->word_at != ATW_LANGMAP) ||
	    (at == ATW_WORD && pw->word == NULL)) {
		free(pw->word);
		pw->word = strndup(word, wordlen);
		pw->word_at = at;
	}
	return NULL;
}

static void
parse_atc_init(parse_state &state, const std::string &lang)
{
	/*
	 * Initialize the add_text_config for parsing the words.
	 */
	state.atc = { 0 };
	state.atc.min_wordlen = 1;
	state.atc.max_wordlen = 100;
	state.atc.hun_obj = 
	state.atc.synizer = NULL;
	state.atc.langmap = state.si->sic->langmap;
	state.atc.cfdata = state.cfdata;
	state.atc.syn_regexes = NULL; /* This will not work from here, it needs to be applied to the whole query which we don't do here (we should). */
	state.atc.word_cb = parse_word_cb;
	set_lang(state, lang);
}

static std::pair<int,ParseNode *>
parse_keyword_or_oper(parse_state &state, bool inphrase, int last_op)
{
	enum sub_type subword = *state.curptr == '*' ? SUB_RIGHT : SUB_NONE;
	if (subword)
		state.curptr++;

	std::string word = scan_keyword(state);
	while (!inphrase && *state.curptr == '"') {
		word.push_back(*state.curptr++);
		word += scan_keyword(state);
	}

	if (*state.curptr == '*') {
		subword = subword == SUB_RIGHT ? SUB_MIDDLE : SUB_LEFT;
		state.curptr++;
	}

	if (word.empty())
		return {-1, (ParseNode*)NULL};

	int op;
	if (subword == 0 && (op = get_oper(state.si->db, state.atc.lang, word.c_str())) != -1)
		return {op, (ParseNode*)NULL};

	enum parse_word_data::pwd_mode mode = parse_word_data::PWD_LSW;
	if (inphrase || subword)
		mode = parse_word_data::PWD_W;
	else if (state.pos_flags)
		mode = parse_word_data::PWD_LW;
	
	struct parse_word_data pwd = { &state, NULL, 0, mode, last_op, subword };
	split_text(&state.atc, &pwd, word.c_str());

	if (pwd.word == NULL)
		return {-1, (ParseNode*)NULL};

	if (is_stopword(state.si->db, pwd.word)) {
		free(pwd.word);
		return {-1, (ParseNode*)NULL};
	}

	if (inphrase)
		return {-1, new ParseNodeInvwordPos(pwd.word, 0, state.pos_flags)};
	if (subword)
		return {-1, new ParseNodeSubword(subword, pwd.word, state.pos_flags)};

	if (state.pos_flags)
		return {-1, new ParseNodeInvword(pwd.word, 1, state.pos_flags)};
	else
		return {-1, new ParseNodeInvword(pwd.word)};
}

static std::unique_ptr<ParseNodeIntersection>
make_intersection_always(node_set &set)
{
	std::unique_ptr<ParseNodeIntersection> isect{new ParseNodeIntersection()};
	for (auto n = set.begin(); n != set.end() ; n++)
		isect->add_node(n->release());
	set.clear();
	return isect;
}

static std::unique_ptr<ParseNode>
make_intersection(node_set &set)
{
	if (set.size() == 1) {
		auto n = std::move(set.front());
		set.clear();
		return n;
	}
	return make_intersection_always(set);
}

static bool
parse_formal_query(parse_state &state, filter_chain &fchain, node_set &main_query)
{
	bool uquery = false;
	bool minus = false;
	bool has_words = false;

	while (*state.curptr) {
		auto qs = scan_query_section(state);
		if (qs == QS_PAREN_LEFT || qs == QS_PAREN_RIGHT)
			break;
		if (qs) {
			if (has_words)
				throw std::runtime_error("words are not allowed before " + std::string(state.curptr, 3));
			uquery = (qs == QS_TEXT_UNION);
			state.curptr += 3;
			scan_space(state, true);
			break;
		}

		const char *start = state.curptr;
		std::string word = scan_word(state);
		if (word == "NOT") {
			if (minus)
				throw std::runtime_error("duplicate NOT in formal query");
			minus = true;
		} else if (word == "suborder") {
			expect(state, ':');
			unsigned int val1, val2;
			scan_unsigned_range(state, val1, val2, INT_MAX);
			fchain.emplace_back(new ParseNodeSubrange(NULL, val1, val2));
		} else if (word == "order") {
			expect(state, ':');
			unsigned int val1, val2;
			scan_unsigned_range(state, val1, val2, INT_MAX);
			fchain.emplace_back(new ParseNodeOrder(NULL, val1, val2));
		} else if (word == "cache_here") {
			if (!main_query.empty()) {
				/* Clear out stuff to the left so we can cache everything to the right */
				if (state.cache_here)
					state.cache_here->set_next(make_intersection(main_query).release());
				else
					state.cache_root = make_intersection_always(main_query);
				state.cache_here = new ParseNodeCacheSearch(NULL);
			} else if (!state.cache_here) {
				state.cache_here = new ParseNodeCacheSearch(NULL);
				state.cache_root.reset(new ParseNodeIntersection());
			}
			state.cache_root->add_node(state.cache_here);
		} else if (*state.curptr == ':') {
			state.curptr = start;
			auto anode = parse_attribute_union(state);
			if (minus) {
				fchain.emplace_back(new ParseNodeMinus(NULL, anode.release()));
				minus = false;
			} else {
				main_query.push_back(std::move(anode));
			}
		} else {
			state.curptr = start;
			auto kw = parse_keyword_or_oper(state, false, minus ? IOP_NOT : IOP_AND);
			if (kw.second) {
				if (minus) {
					fchain.emplace_back(new ParseNodeMinus(NULL, kw.second));
					minus = false;
				} else {
					main_query.emplace_back(kw.second);
					has_words = true;
				}
			}
		}
		scan_space(state, true);
	}
	if (minus)
		throw std::runtime_error("NOT without an attribute or word");

	return uquery;
}

static void
parse_text_query(parse_state &state, filter_chain &fchain, bool uquery, node_set &main_query)
{
	/* XXX this is where clean_query should be called */
	enum iops last_op = IOP_AND;
	node_set unions;
	node_set current_set;

	while (*state.curptr) {
		auto qs = scan_query_section(state);
		if (qs == QS_PAREN_LEFT || qs == QS_PAREN_RIGHT)
			break;

		if (*state.curptr == '+' || *state.curptr == '-') {
			if (*state.curptr == '-')
				last_op = IOP_NOT;
			state.curptr++;
			scan_space(state);
			continue;
		}

		ParseNode *node = NULL;
		if (*state.curptr == '"') {
			state.curptr++;
			scan_space(state);
			ParseNodePhrase *phrase = NULL;
			while (*state.curptr && *state.curptr != '"') {
				auto kw = parse_keyword_or_oper(state, true, IOP_AND);
				if (kw.second) {
					if (!phrase)
						phrase = new ParseNodePhrase();
					phrase->add_word(static_cast<ParseNodeInvwordPos*>(kw.second));
				}
				scan_space(state);
			}
			if (*state.curptr == '"') {
				state.curptr++;
				scan_space(state);
			}
			node = phrase;
		} else {
			auto kw = parse_keyword_or_oper(state, false, last_op);
			scan_space(state);

			if (kw.first != -1) {
				last_op = (enum iops)kw.first;
				continue;
			}

			node = kw.second;
		}

		if (node) {
			if (last_op == IOP_NOT) {
				fchain.emplace_back(new ParseNodeMinus(NULL, node));
			} else {
				if ((last_op == IOP_OR || uquery) && !current_set.empty())
					unions.emplace_back(make_intersection(current_set));
				current_set.emplace_back(node);
			}
		}
		last_op = IOP_AND;
	}
	if (!current_set.empty()) {
		if (unions.empty()) {
			for (auto n = current_set.begin(); n != current_set.end() ; n++)
				main_query.push_back(std::move(*n));
			return;
		}
		unions.emplace_back(make_intersection(current_set));
	}
	switch (unions.size()) {
	case 0:
		break;
	case 1:
		main_query.push_back(std::move(unions.front()));
		break;
	default:
		auto u = new ParseNodeUnion<>();
		for (auto n = unions.begin() ; n != unions.end() ; n++)
			u->add_node(n->release());
		main_query.emplace_back(u);
		break;
	}
}

static std::unique_ptr<ParseNode>
make_chain(filter_chain &fchain, std::unique_ptr<ParseNode> last)
{
	for (auto it = fchain.rbegin() ; it != fchain.rend() ; it++) {
		(*it)->next = last.release();
		last = std::move(*it);
	}
	fchain.clear();
	return std::move(last);
}

static std::unique_ptr<ParseNode>
parse_query(parse_state &state, filter_chain &fchain)
{
	state.used_default_attr = false;
	state.is_fixed_order = false;
	node_set main_query;
	bool uquery = parse_formal_query(state, fchain, main_query);
	parse_text_query(state, fchain, uquery, main_query);
	if (state.si->sic->default_attr_key && !state.used_default_attr && !state.is_fixed_order) {
		/* Fixed order searches can't be intersected since they're in the wrong order */
		auto ia = new ParseNodeInvattr(dups(std::string(state.si->sic->default_attr_key) + ":" + state.si->sic->default_attr_val));
		main_query.emplace_back(ia);
	}
	std::unique_ptr<ParseNode> node;
	if (main_query.empty()) {
		node.reset(new ParseNodeEmpty());
	} else {
		node = make_intersection(main_query);
	}
	if (state.cache_here) {
		state.cache_here->set_next(node.release());
		state.cache_here = NULL;
		node = std::move(state.cache_root);
	}
	return make_chain(fchain, std::move(node));
}

static std::unique_ptr<ParseNode>
parse_queries(parse_result &result, parse_state &state)
{
	filter_chain fchain;
	if (*state.curptr != '\002')
		return parse_query(state, fchain);

	state.curptr++;
	std::unique_ptr<ParseNodeUnion<>> u;
	std::unique_ptr<ParseNode> node;
	while (true) {
		scan_space(state, true);
		if (node) {
			if (!u)
				u.reset(new ParseNodeUnion<>());
			u->add_node(node.release());
		}
		parse_parameters(result, state, true, fchain);
		node = parse_query(state, fchain);
		fchain.clear();

		expect(state, '\003');
		scan_space(state, true);
		const char *start = state.curptr;
		std::string word = scan_word(state);

		if (word != "OR") {
			state.curptr = start;
			break;
		}

		scan_space(state, true);
		expect(state, '\002');
	}
	if (node && u)
		u->add_node(node.release());
	if (u)
		return std::move(u);
	return node;
}

int
parse(struct search_instance *si, struct parse_result *result, const char *str, void *cfdata)
{
	parse_state state = { si, str, cfdata };

	parse_atc_init(state, si->sic->default_lang);

	try {
		scan_space(state);

		if (scan_command_flag(state)) {
			auto query = parse_command(state);
			expect_eol(state);
			result->query = query.release();
			return 0;
		}

		parse_offset(*result, state);
		filter_chain fchain;
		parse_parameters(*result, state, false, fchain);
		std::unique_ptr<ParseNode> node = parse_queries(*result, state);
		expect_eol(state);

		std::unique_ptr<ParseNode> f = make_chain(fchain, std::move(node));
		if (state.pos_flags)
			f.reset(new ParseNodeUncacheable(f.release()));
		/* XXX this should be done in main_search. */
		f.reset(new ParseNodeCountAll(f.release(), xstrdup("filtered")));
		if (state.suggest) {
			state.suggest->next = f.release();
			f = std::move(state.suggest);
		}
		result->query = f.release();
		return 0;
	} catch (std::runtime_error &err) {
		result->error = dups(err.what());
		return 1;
	} catch (...) {
		result->error = xstrdup("unknown error");
		return 1;
	}
}
