#include <pthread.h>
#include <string.h>
#include "tree.h"
#include "util.h"
#include "logging.h"
#include "atomic.h"
#include "stats.h"
#include "spinlock.h"

struct stat_entry {
	char *name;
	int counter;
	RB_ENTRY(stat_entry) rbe;
};

static __inline int
stat_compare(const struct stat_entry *a, const struct stat_entry *b) {
	return strcmp(a->name, b->name);
}

RB_HEAD(stat_tree, stat_entry) stats;
RB_GENERATE_STATIC(stat_tree, stat_entry, rbe, stat_compare);

static pthread_mutex_t stat_mutex = PTHREAD_MUTEX_INITIALIZER;

void
stat_inc(struct lru *c, const char *stat) {
	struct stat_entry *s;
	struct stat_entry f;
	f.name = (char*)stat;
	s = RB_FIND(stat_tree, &stats, &f);

	if (s) {
		spinlock_add_int(&s->counter, 1);
	}
}

void
stat_register(const char *stat) {
	struct stat_entry *s = zmalloc(sizeof(*s) + strlen(stat) + 1);
	s->name = (char*)(s + 1);
	strcpy(s->name, stat);

	pthread_mutex_lock(&stat_mutex);
	RB_INSERT(stat_tree, &stats, s);
	pthread_mutex_unlock(&stat_mutex);
}

void
stat_print(int clear) {
	struct stat_entry *s;

	pthread_mutex_lock(&stat_mutex);
	RB_FOREACH(s, stat_tree, &stats) {
		log_printf(LOG_INFO, "STAT %s: %u", s->name, s->counter);
		if (clear)
			s->counter = 0;
	}
	pthread_mutex_unlock(&stat_mutex);
}

