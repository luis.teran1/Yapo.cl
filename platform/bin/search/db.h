#ifndef DB_H
#define DB_H

#include <sys/types.h>
#include <pthread.h>
#include "index.h"
#include "avl.h"

struct invattr {
	struct avl_node tree;
	const char *attr;
	const int32_t *docs;
	int ndocs;
};

struct invattr_iter {
	struct avl_it it;
	struct invattr s;
	struct invattr e;
	int elen;
};

struct cache_doc {
	struct avl_node tree;
	int32_t id;
	const char *document;
};

struct invword {
	struct avl_node tree;
	const char *word;
	int wlen;
	const struct docindex *docs;
	const struct docpos *docpos;
	int ndocs;
};

struct bsearch_cache {
	struct docs {
		struct cache_doc *docarr;
		struct avl_node *docs;
		struct index_docs_iter *it;
		int ndocs;
	} docs;

	struct words {
		struct invword *wordarr;
		struct avl_node *words;
		struct index_words_iter *it;
	} words;

	struct {
		struct invattr *attrarr;
		struct avl_node *attrs;
		struct index_attrs_iter *it;
	} attributes;

	pthread_rwlock_t rwlock;

	struct index_db *index;

	void *subs;
};

struct parse_struct;
struct bsearch_t;
struct result;
struct bconf_node;
struct charmap;

#ifdef __cplusplus
extern "C" {
#endif

static inline int docoff_is_subdoc(struct bsearch_cache *, int32_t docoff) FUNCTION_PURE ARTIFICIAL;
static inline int
docoff_is_subdoc(struct bsearch_cache *db, int32_t docoff) {
	return index_docoff_is_subdoc(db->index, docoff);
}

int32_t load_docoff(struct bsearch_cache *, uint32_t id) FUNCTION_PURE;
const char *load_doc_body(struct bsearch_cache *, int32_t docoff) FUNCTION_PURE;
const struct doc *get_doc(struct bsearch_cache *, int32_t docoff) FUNCTION_PURE;
const char *get_doc_header(struct bsearch_cache *) FUNCTION_PURE;
const char *get_index_to_id(struct bsearch_cache *) FUNCTION_PURE;
const char *get_conf_rev(struct bsearch_cache *);
const char *get_config(struct bsearch_cache *, size_t *);

void debug_set_conf_rev(struct bsearch_cache *, const char *);

const char *get_timestamp(struct bsearch_cache *db);

int is_stopword(struct bsearch_cache *, const char *kw);
int get_oper(struct bsearch_cache *db, const char *slang, const char *kw);

struct invword *find_invword(struct bsearch_cache *, const char *word);
struct invattr *find_invattr(struct bsearch_cache *, char *attr);

void invattr_iter_init(struct bsearch_cache *, struct invattr_iter *iai, const char *attr, const char *start, const char *end);
void invattr_iter_del(struct invattr_iter *iai);
struct invattr *invattr_iter_next(struct invattr_iter *);

int cmp_attr_key(struct invattr *a, struct invattr *b);
pthread_rwlock_t *acquire_db_readlock();
struct bsearch_cache *db_init(const char *, struct bconf_node *, struct charmap *, int, struct index_db *index);
void db_cleanup(struct bsearch_cache *);
int db_ndocs(struct bsearch_cache *db);
void db_expand_attrib(struct parse_struct *, char*);

int db_cache_lock(struct bsearch_cache *db);
void db_cache_unlock(struct bsearch_cache *db);

#ifdef __cplusplus
}
#endif

#endif /*DB_H*/
