#ifndef SORTSPEC_H
#define SORTSPEC_H

#ifdef __cplusplus
extern "C" {
#endif

#define SORTSPEC_INC		0x001
#define SORTSPEC_DEC		0x002
#define SORTSPEC_DIR_MASK	(SORTSPEC_INC|SORTSPEC_DEC)

#define SORTSPEC_SORTVALUE	0x004
#define SORTSPEC_ORDER		0x008
#define SORTSPEC_FIELD_MASK	(SORTSPEC_ORDER|SORTSPEC_SORTVALUE)
#define SORTSPEC_ATTR		(0x010|SORTSPEC_SORTVALUE)
#define SORTSPEC_METHOD_MASK	(SORTSPEC_SORTVALUE|SORTSPEC_ORDER|SORTSPEC_ATTR)

#define SORTSPEC_EMPTY_FIRST	0x020
#define SORTSPEC_EMPTY_LAST	0x040
#define SORTSPEC_EMPTY_MASK	(SORTSPEC_EMPTY_FIRST|SORTSPEC_EMPTY_LAST)

#define SORTSPEC_INT		0x080
#define SORTSPEC_FLOAT		0x100
#define SORTSPEC_TYPE_MASK	(SORTSPEC_INT|SORTSPEC_FLOAT)

/* The natural sorting order of the document data. */
#define SORTSPEC_NATURAL	(SORTSPEC_ORDER|SORTSPEC_DEC|SORTSPEC_INT)

/* Used to inherit sortspec later */
#define SORTSPEC_UNSET          0

struct result;
void sort_result(struct result *res, int sortspec);

const char *str_sortspec(int sortspec, char *buf, size_t buflen);

#ifdef __cplusplus
}
#endif

#endif
