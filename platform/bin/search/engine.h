#ifndef _ENGINE_H
#define _ENGINE_H

#define ENGINE_NUM_FDS 64
#define ENGINE_INPUT_BUFSZ (18 * 1024)
#define ENGINE_GO_AWAY "info:goaway\n"

#ifdef __cplusplus
extern "C" {
#endif
struct bconf_node;
struct chatter;
struct search_conn;
int run_engine(struct bconf_node *, struct bconf_node *, pid_t signal_pid, int command_sock);
int wait_for_fd(struct chatter *chatter, struct search_conn *conn);
void release_fd(struct chatter *chatter, int hup);
void release_reserve(struct chatter *chatter);

struct search_engine;
struct search_instance *se_get_si(struct search_engine *);	/* In the future we'll want an argument identifying which search instance we want. */
void se_put_si(struct search_engine *, struct search_instance *);

void quit_signal(int sig);

#ifdef __cplusplus
}
#endif


#endif /* _ENGINE_H */
