#include <stdio.h>
#include "parse_nodes.hh"
#include "util.h"

/* Intersection */
int32_t
ParseNodeIntersection::next_document(int32_t doc) {
	int32_t d = NULL_DOCOFF;
	ParseNode *n;
	ParseNode *start_node = NULL;

	do {
		SLIST_FOREACH(n, &intersection_nodes, list_link) {
			/* 1 completed iteration over all nodes with all current documents
			 * equal. */
			if (n == start_node) {
				return d;
			}

			d = n->get_next_document(doc);

			if (d == NULL_DOCOFF) {
				return NULL_DOCOFF;
			}

			if (d != doc) {
				doc = d;
				start_node = n;
			}
		}
	} while (start_node);

	return d;
}

ParseNodeIntersection::~ParseNodeIntersection() {
	ParseNode *n;

	while (!SLIST_EMPTY(&intersection_nodes)) {
		n = SLIST_FIRST(&intersection_nodes);
		SLIST_REMOVE_HEAD(&intersection_nodes, list_link);
		delete n;
	}
}

int
ParseNodeIntersection::get_parse_string(char *buf, size_t buflen) {
	size_t tlen = 0;
	ParseNode *n;
	int num = 0;
	int ilen;

	SLIST_FOREACH(n, &intersection_nodes, list_link) {
		ilen = n->get_parse_string(buf + tlen, buflen - tlen);

		if (ilen < 0)
			return ilen;
		tlen += ilen;
		buf[tlen++] = ',';
		if (tlen >= buflen)
			return -1;
		num++;
	}
	ilen = snprintf(buf + tlen, buflen - tlen, "%di", num);
	if (ilen < 0 || tlen + ilen >= buflen)
		return -1;
	return tlen + ilen;
}

ParseNode *
ParseNodeIntersection::initialize(struct result *res) {
	ParseNode *n;
	ParseNode **np;

	for (np = &SLIST_FIRST(&intersection_nodes) ; (n = *np) ; ) {
		ParseNode *nn = n->initialize(res);

		if (nn) {
			if (typeid(*nn) == typeid(ParseNodeEmpty)) {
				*np = SLIST_NEXT(n, list_link);
				delete n;
				return nn;
			}
			nn->list_link = n->list_link;
			delete n;
			*np = n = nn;
		}
		if (typeid(*n) == typeid(ParseNodeInvattr)) {
			ParseNodeInvattr *ianode = (ParseNodeInvattr*)n;
			ParseNode **pn;
			bool deleteme = ianode->combined;
			char *kw = xstrdup(ianode->keyword);
			bool merged = false;

			for (pn = &SLIST_NEXT(n, list_link) ; (nn = *pn) ; ) {
				if (typeid(*nn) == typeid(ParseNodeInvattr)) {
					ParseNodeInvattr *nianode = (ParseNodeInvattr*)nn;

					if (ianode->combine(nianode->keyword, res)) {
						nianode->combined = true;
						merged = true;
					} else if (nianode->combine(kw, res)) {
						deleteme = true;
					}
				}
				pn = &SLIST_NEXT(nn, list_link);
			}
			if (!merged && deleteme) {
				*np = SLIST_NEXT(n, list_link);
				delete n;
				continue;
			}
			free(kw);
		} else if (typeid(*n) == typeid(ParseNodeUnion<ParseNodeInvattr>)) {
			ParseNodeUnion<ParseNodeInvattr> *unode = (ParseNodeUnion<ParseNodeInvattr>*)n;
			ParseNode **pn;
			ParseNode *un;

			for (pn = &SLIST_FIRST(&intersection_nodes) ; (nn = *pn) ; ) {
				if (typeid(*nn) == typeid(ParseNodeInvattr)) {
					ParseNodeInvattr *nianode = (ParseNodeInvattr*)nn;
					bool ok = true;

					SLIST_FOREACH(un, &unode->node_list, list_link) {
						ParseNodeInvattr *uianode = (ParseNodeInvattr*)un;

						if (!uianode->combine(nianode->keyword, res))
							ok = false;
					}
					if (ok) {
						*pn = SLIST_NEXT(nn, list_link);
						delete nn;
						continue;
					}
				}
				pn = &SLIST_NEXT(nn, list_link);
			}
		}
		np = &SLIST_NEXT(n, list_link);
	}

	n = SLIST_FIRST(&intersection_nodes);
	if (n && !SLIST_NEXT(n, list_link)) {
		SLIST_REMOVE_HEAD(&intersection_nodes, list_link);
		return n;
	}

	return NULL;
}

void
ParseNodeIntersection::finalize(struct result *res, struct timer_instance *ti) {
	ParseNode *n;
	SLIST_FOREACH(n, &intersection_nodes, list_link) {
		n->finalize(res, ti);
	}
}

float
ParseNodeIntersection::get_current_bm25(int ndocs, float k1) {
	float sum = 0.0;
	ParseNode *pn;

	SLIST_FOREACH(pn, &intersection_nodes, list_link) {
		sum += pn->get_current_bm25(ndocs, k1);
	}
	return sum;
}
