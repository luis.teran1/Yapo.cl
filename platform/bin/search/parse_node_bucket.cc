#include "parse_nodes.hh"
#include "util.h"
#include "hash.h"
#include "qsort.h"
#include "logging.h"
#include "strl.h"
#include <syslog.h>
#include <math.h>

#include <stdio.h>

#define TYPE_UNSET 0
#define TYPE_NUMERIC 1
#define TYPE_LIST 2
#define SET_TYPE(cl,new_type) (cl->type) = (((cl->type) == TYPE_UNSET) ? (new_type) : (((cl->type) != new_type) ? TYPE_LIST : (new_type)))

#define BIG_BUCKETS 5
#define SMALL_BUCKETS 3

#define int_lt(a,b) ((a->value)<(b->value))
#define freq_lt(a,b) ((a->freq)<(b->freq))
#define string_lt(a,b) (strcasecmp((a->value),(b->value))<0)

static int tot_cnt;

struct count_level {
	union {
		ParseNodeAttributeRange *ar;
		ParseNodeUnion<ParseNodeInvattr> *u;
	};
	int type;
	char *attribute;
	int num_counters;
	int is_union;
	struct hash_table *counters;
	SIMPLEQ_ENTRY(count_level) list_link;
};

ParseNodeBucket::~ParseNodeBucket() {
	while (!SIMPLEQ_EMPTY(&count_levels)) {
		struct count_level *cl = SIMPLEQ_FIRST(&count_levels);
		SIMPLEQ_REMOVE_HEAD(&count_levels, list_link);

		if (cl->counters)
			hash_table_free(cl->counters);
		if (cl->attribute) {
			free(cl->attribute);
			delete cl->u;
		} else {
			delete cl->ar;
		}
		free(cl);
	}
	free(tag);
}

static void *
new_bucket_counter(const void *key, int klen, void **data, void *v) {
	*data = zmalloc(sizeof(int));

	return xstrdup((const char*)key);
}

int32_t 
ParseNodeBucket::next_document(int32_t doc) {
	int32_t res = next->get_next_document(doc);
	struct count_level *cl;

	if (res == NULL_DOCOFF)
		return NULL_DOCOFF;

	SIMPLEQ_FOREACH(cl, &count_levels, list_link) {
		if (!cl->u)
			continue;

		int32_t d = cl->u->get_next_document(res);

		if (d == NULL_DOCOFF) {
			delete cl->u;
			cl->u = NULL;
		} else if (d == res) {
			ParseNodeInvattr *ia = cl->u->head_node();
			int *cnt = (int*)hash_table_update(cl->counters, ia->keyword, -1, new_bucket_counter, NULL);
			if (*cnt == 0)
				cl->num_counters++;

			(*cnt)++;

			cl->type = TYPE_LIST;
		}
	}

	return res;
}

void
ParseNodeBucket::add_node(ParseNode *n, bool head, int is_union) {
	struct count_level *cl = (struct count_level*)zmalloc(sizeof(*cl));
	cl->type = TYPE_UNSET;

	if (is_union) {
		cl->u = (ParseNodeUnion<ParseNodeInvattr>*)n;
		cl->is_union = 1;
	} else {
		cl->ar = (ParseNodeAttributeRange*)n;
	}

	if (head)
		SIMPLEQ_INSERT_HEAD(&count_levels, cl, list_link);
	else
		SIMPLEQ_INSERT_TAIL(&count_levels, cl, list_link);

	tot_cnt++;
}

struct num_bucket {
	int value;
	int freq;
	char strvalue[16];
};

struct string_bucket {
	char value[128];
	int freq;
};

struct print_counter_data {
	const char *info_tag;
	struct buf_string *buf;
	int cnt;
	int type;
	void *buck_val;
	char *buck_name;
};

static void
print_bucket_counter(const void *key, int klen, void *data, void *cb_data) {
	struct print_counter_data *pcd = (struct print_counter_data*)cb_data;
	struct string_bucket *string_buck;
	int *cnt = (int*)data;
	const char *k = (const char*)key;
	const char *delim;
	char *endptr;

	delim = strchr(k, ':') + 1;
	if (delim <= k)
		return;

	if (!pcd->buck_name)
		pcd->buck_name = xstrndup(k, (size_t)(delim - k - 1));

	/* If we are dealing with a numeric resultset, save the results for bucketing */
	if (pcd->type == TYPE_NUMERIC) {
		struct num_bucket *values = (struct num_bucket *)pcd->buck_val;
		values[pcd->cnt].value = (int)(strtod(delim, &endptr) * 1000);
		strlcpy(values[pcd->cnt].strvalue, delim, sizeof(values[pcd->cnt].strvalue));
		values[pcd->cnt].freq = *cnt;
		pcd->cnt++;
	} else {
		string_buck = (struct string_bucket *)pcd->buck_val;

		/* Insert in-order - replace QSORT */
		if (*cnt > string_buck[0].freq) {
			strlcpy(string_buck[0].value, delim, sizeof(string_buck[0].value));
			string_buck[0].freq = *cnt;
			QSORT(struct string_bucket, string_buck, BIG_BUCKETS, freq_lt);
		}
	}
}

static void
print_list_result(struct print_counter_data *pcd, int entries) {
	struct string_bucket *values = (struct string_bucket *)pcd->buck_val;
	int start = BIG_BUCKETS - ((entries > BIG_BUCKETS) ? BIG_BUCKETS : entries);
	int i;

	QSORT(struct string_bucket, values, BIG_BUCKETS, string_lt);

	for (i = start; i < BIG_BUCKETS; i++) {
		int len = strlen(values[i].value);
		if (!len)
			continue;
		char esc_name[len * 2 + 1], *p;
		int c;

		p = esc_name;
		for (c = 0; c <= len; c++) {
			if (values[i].value[c] == ':' || values[i].value[c] == '\\')
				*p++ = '\\';
			*p++ = values[i].value[c];
		}

		bscat(pcd->buf, 
		    "info:%s:%s:value:%d:%s\n"
		    "info:%s:%s:count:%d:%i\n",
		    pcd->info_tag, pcd->buck_name, i - start, esc_name,
		    pcd->info_tag, pcd->buck_name, i - start, values[i].freq);
	}
	
	if(pcd->buck_name)	
		bscat(pcd->buf, "info:%s:%s:type:list\n", pcd->info_tag, pcd->buck_name);
}

static void
print_bucket_result(struct print_counter_data *pcd, int buckets) {
	struct num_bucket *values = (struct num_bucket *)pcd->buck_val;
	int i;
	int j;
	int range_freq = 0;
	int start = 0;
	int end = 0;
	int tot_buck = (buckets < (BIG_BUCKETS*2)) ? SMALL_BUCKETS : BIG_BUCKETS;
	int n_buck = (int)ceil((double)buckets / (double)tot_buck);

	QSORT(struct num_bucket, values, buckets, int_lt);

	/* Print a list rather than really sparse buckets when we have few elements */
	if (buckets <= BIG_BUCKETS) {
		
		for (i = 0; i < buckets; i++) {
			bscat(pcd->buf, "info:%s:%s:value:%d:%s\n"
			    "info:%s:%s:count:%i:%i\n", 
			    pcd->info_tag, pcd->buck_name, i, values[i].strvalue, 
			    pcd->info_tag, pcd->buck_name, i, values[i].freq);
		}
	} else {

		for (i = 0; i < tot_buck && start < buckets; i++) {
		
			end = (((i+1) * n_buck) - 1);
			if (end > (buckets - 1))
				end = buckets - 1;

			range_freq = 0;
			for (j = start; j <= (start + n_buck - 1); j++)
				range_freq += values[j].freq;

			bscat(pcd->buf,	"info:%s:%s:value_from:%d:%s\n"
			    "info:%s:%s:value_to:%d:%s\n"
			    "info:%s:%s:count:%d:%d\n",
			    pcd->info_tag, pcd->buck_name, i, values[start].strvalue,
			    pcd->info_tag, pcd->buck_name, i, values[end].strvalue,
			    pcd->info_tag, pcd->buck_name, i, range_freq);
			start = end + 1;
		}
	}

	if (buckets <= BIG_BUCKETS)
		bufcat(&pcd->buf->buf, &pcd->buf->len, &pcd->buf->pos, "info:%s:%s:type:list\n", pcd->info_tag, pcd->buck_name);
	else
		bufcat(&pcd->buf->buf, &pcd->buf->len, &pcd->buf->pos, "info:%s:%s:type:range\n", pcd->info_tag, pcd->buck_name);
}

void
ParseNodeBucket::get_headers(struct buf_string *buf) {
	struct count_level *cl;

	next->get_headers(buf);

	SIMPLEQ_FOREACH(cl, &count_levels, list_link) {
		/* Print counters for the first level where more than one key matched. */
		struct print_counter_data pcd = {tag ?: "bucket", buf, 0, cl->type, NULL, NULL};
		if (cl->type == TYPE_NUMERIC)
			pcd.buck_val = calloc(cl->num_counters, sizeof(struct num_bucket));
		else
			pcd.buck_val = calloc(BIG_BUCKETS, sizeof(struct string_bucket));
		
		hash_table_do(cl->counters, print_bucket_counter, &pcd);
	
		if (cl->type == TYPE_NUMERIC) {
			print_bucket_result(&pcd, cl->num_counters);
		} else {
			print_list_result(&pcd, cl->num_counters);
		}

		free(pcd.buck_val);
		free(pcd.buck_name);
		return;			/* XXX - WTF? This is in the original code, but doesn't make sense. */
	}
}

ParseNode *
ParseNodeBucket::initialize(struct result *res) {
	struct count_level *cl;
	struct count_level *clp = NULL;

	tot_cnt = 0;

	replace_initialize(&next, res);
	if (typeid(*next) == typeid(ParseNodeEmpty)) {
		ParseNode *n = next;
		next = NULL;
		return n;
	}

	for (cl = SIMPLEQ_FIRST(&count_levels); cl; cl = (clp ? SIMPLEQ_NEXT(clp, list_link) : SIMPLEQ_FIRST(&count_levels))) {
		ParseNodeUnion<ParseNodeInvattr> *u;
	
		if (cl->is_union) {
			ParseNodeInvattr *iw = NULL;
			u = (ParseNodeUnion<ParseNodeInvattr>*)cl->u->initialize(res);
			if (u) {
				delete cl->u;
				cl->u = u;
			}

			u = cl->u;
			iw = cl->u->head_node();

			if (iw) 
				cl->attribute = xstrdup(iw->keyword);
		} else {
			u = (ParseNodeUnion<ParseNodeInvattr>*)cl->ar->initialize(res, true);

			if (u)
				cl->attribute = xstrdup(cl->ar->get_attribute());

			delete cl->ar;
		}
		if (u && !SLIST_EMPTY(&u->node_list)) {
			cl->u = u;
			cl->counters = hash_table_create(u->get_num_nodes(), free);
			hash_table_free_keys(cl->counters, 1);
			clp = cl;
		} else {
			if (clp)
				SIMPLEQ_REMOVE_AFTER(&count_levels, clp, list_link);
			else
				SIMPLEQ_REMOVE_HEAD(&count_levels, list_link);
			free(cl);
			if (u)
				delete u;
		}
	}
	if (SIMPLEQ_EMPTY(&count_levels)) {
		ParseNode *n = next;
		next = NULL;
		return n;
	}
	return NULL;
}

int
ParseNodeBucket::get_parse_string(char *buf, size_t buflen) {
	size_t tlen = 0;
	struct count_level *cl;
	int num = 0;
	int ilen;

	ilen = next->get_parse_string(buf, buflen);
	if (ilen < 0)
		return ilen;
	buf[ilen++] = ',';
	tlen += ilen;
	if ((size_t)tlen >= buflen)
		return -1;

	SIMPLEQ_FOREACH(cl, &count_levels, list_link) {
		if (cl->is_union)
			ilen = cl->u->get_parse_string(buf + tlen, buflen - tlen);
		else
			ilen = cl->ar->get_parse_string(buf + tlen, buflen - tlen);

		if (ilen < 0)
			return ilen;
		tlen += ilen;
		buf[tlen++] = ',';
		if (tlen >= buflen)
			return -1;
		num++;
	}
	ilen = snprintf(buf + tlen, buflen - tlen, "%dbu(%s)", num, tag ?: "");
	if (ilen < 0 || tlen + ilen >= buflen)
		return -1;
	return tlen + ilen;
}
