#include <limits.h>

#include "parse_nodes.hh"
#include "util.h"
#include "search.h"

ParseNode *
ParseNodeOrder::initialize(struct result *res) {
	db = res->si->db;
	return ParseNodeNext::initialize(res);
}

int32_t
ParseNodeOrder::next_document(int32_t doc) {
	int32_t d;
	int32_t search = doc;

	while ((d = next->get_next_document(search)) != NULL_DOCOFF) {
		const struct doc *doc = get_doc(db, d);
		if (doc->order >= from && doc->order <= to)
			break;
		search = d;
		increment_doc(&search);
	}

	return d;
}

int
ParseNodeOrder::get_parse_string(char *buf, size_t buflen) {
	int nlen;
	int ilen;

	nlen = next->get_parse_string(buf, buflen);
	if (nlen < 0)
		return nlen;

	if (to == INT_MAX)
		ilen = snprintf(buf + nlen, buflen - nlen, ",%d-o", from);
	else
		ilen = snprintf(buf + nlen, buflen - nlen, ",%d-%do", from, to);
	if (ilen < 0 || (size_t)nlen + ilen >= buflen)
		return -1;
	return nlen + ilen;
}

