#include "parse_nodes.hh"
#include "util.h"
#include "hash.h"
#include "tree.h"
#include "logging.h"

#include <stdio.h>

struct count_level {
	union {
		ParseNodeAttributeRange *ar;
		ParseNodeUnion<ParseNodeInvattr> *u;
	};
	int is_union;
	SIMPLEQ_ENTRY(count_level) list_link;
};

struct ct_counter {
	struct avl_node tree;
	char *key;
	int cnt;
	int depth;
};

static void
ct_counter_free(struct avl_node *n) {
	struct ct_counter *ct = avl_data(n, struct ct_counter, tree);

	if (n->link[0])
		ct_counter_free(n->link[0]);
	if (n->link[1])
		ct_counter_free(n->link[1]);
	free(ct);
}

ParseNodeCountTree::~ParseNodeCountTree() {
	while (!SIMPLEQ_EMPTY(&count_levels)) {
		struct count_level *cl = SIMPLEQ_FIRST(&count_levels);
		SIMPLEQ_REMOVE_HEAD(&count_levels, list_link);

		if (cl->is_union) {
			delete cl->u;
		} else {
			delete cl->ar;
		}
		free(cl);
	}
	if (tree)
		ct_counter_free(tree);
	free(tag);
}

static int
ct_cmp(const struct avl_node *an, const struct avl_node *bn) {
	const struct ct_counter *a = avl_data(an, struct ct_counter, tree);
	const struct ct_counter *b = avl_data(bn, struct ct_counter, tree);

	return strcmp(a->key, b->key);
}

int32_t
ParseNodeCountTree::next_document(int32_t doc) {
	int32_t res = next->get_next_document(doc);
	struct count_level *cl;
	int depth=0;

	if (res == NULL_DOCOFF)
		return NULL_DOCOFF;

	SIMPLEQ_FOREACH(cl, &count_levels, list_link) {
		if (!cl->u)
			continue;

		int32_t d = cl->u->get_next_document(res);

		if (d == NULL_DOCOFF) {
			delete cl->u;
			cl->u = NULL;
		} else if (d == res) {
			ParseNodeInvattr *ia = cl->u->head_node();
			struct ct_counter cts;
			struct ct_counter *ct;
			struct avl_node *n;

			cts.key = strchr(ia->keyword, ':') + 1;

			if ((n = avl_lookup(&cts.tree, &tree, ct_cmp)) == NULL) {
				ct = (struct ct_counter *)zmalloc(sizeof(*ct) + strlen(cts.key) + 1);
				ct->key = (char*)(ct + 1);
				strcpy(ct->key, cts.key);
				ct->cnt = 0;
				ct->depth = depth;
				avl_insert(&ct->tree, &tree, ct_cmp);
			} else {
				ct = avl_data(n, struct ct_counter, tree);
			}

			ct->cnt++;
			depth++;
		}
	}

	return res;
}

void
ParseNodeCountTree::add_node(ParseNode *n, bool head, int is_union) {
	struct count_level *cl = (struct count_level*)zmalloc(sizeof(*cl));

	if (is_union) {
		cl->u = (ParseNodeUnion<ParseNodeInvattr>*)n;
		cl->is_union = 1;
	} else 
		cl->ar = (ParseNodeAttributeRange*)n;

	if (head)
		SIMPLEQ_INSERT_HEAD(&count_levels, cl, list_link);
	else
		SIMPLEQ_INSERT_TAIL(&count_levels, cl, list_link);
}

static struct ct_counter *
print_counters(struct avl_node *n, const char * tag, struct buf_string *buf) {
	struct ct_counter *ct = avl_data(n, struct ct_counter, tree);
	struct ct_counter *lcd = ct;
	char *esc_name, *p;
	int len;
	int i;

	len = strlen(ct->key);
	p = esc_name = (char *)alloca(len * 2 + 1);
	for (i = 0; i <= len; i++) {
		if (ct->key[i] == ':' || ct->key[i] == '\\')
			*p++ = '\\';
		*p++ = ct->key[i];
	}

	bscat(buf, "info:%s:%s:%d\n", tag, esc_name, ct->cnt);
	if (n->link[0]) {
		ct = print_counters(n->link[0], tag, buf);
		if ((ct->cnt > lcd->cnt) || (ct->cnt == lcd->cnt && ct->depth > lcd->depth))
			lcd = ct;
	}
	if (n->link[1]) {
		ct = print_counters(n->link[1], tag, buf);
		if ((ct->cnt > lcd->cnt) || (ct->cnt == lcd->cnt && ct->depth > lcd->depth))
			lcd = ct;
	}
	return lcd;
}

void
ParseNodeCountTree::get_headers(struct buf_string *buf) {
	struct ct_counter *lcd = NULL;
	const char * t = tag ?: "count_tree";

	next->get_headers(buf);

	if (tree) {
		char *p, *esc_name;
		int i, len;

		lcd = print_counters(tree, t, buf);
		len = strlen(lcd->key);
		p = esc_name = (char *)alloca(len * 2 + 1);
		for (i = 0; i <= len; i++) {
			if (lcd->key[i] == ':' || lcd->key[i] == '\\')
				*p++ = '\\';
			*p++ = lcd->key[i];
		}
		bufcat(&buf->buf, &buf->len, &buf->pos, "info:%s_lcd:%s\ninfo:%s_lcd_depth:%d\n", t, esc_name, t, lcd->depth);
	}
}

ParseNode *
ParseNodeCountTree::initialize(struct result *res) {
	struct count_level *cl;
	struct count_level *clp = NULL;

	replace_initialize(&next, res);
	if (typeid(*next) == typeid(ParseNodeEmpty)) {
		ParseNode *n = next;
		next = NULL;
		return n;
	}

	for (cl = SIMPLEQ_FIRST(&count_levels); cl; cl = (clp ? SIMPLEQ_NEXT(clp, list_link) : SIMPLEQ_FIRST(&count_levels))) {
		ParseNodeUnion<ParseNodeInvattr> *u;
	       
		if (cl->is_union) {
			ParseNodeInvattr *iw UNUSED = NULL;
			u = (ParseNodeUnion<ParseNodeInvattr>*)cl->u->initialize(res);
			if (u) {
				delete cl->u;
				cl->u = u;
			}

			u = cl->u;
			iw = cl->u->head_node();
		} else {
			u = (ParseNodeUnion<ParseNodeInvattr>*)cl->ar->initialize(res, true);
			cl->is_union = 1;
			delete cl->ar;
		}
		if (u && !SLIST_EMPTY(&u->node_list)) {
			cl->u = u;
			clp = cl;
		} else {
			if (clp)
				SIMPLEQ_REMOVE_AFTER(&count_levels, clp, list_link);
			else
				SIMPLEQ_REMOVE_HEAD(&count_levels, list_link);
			free(cl);
			if (u)
				delete u;
		}
	}
	if (SIMPLEQ_EMPTY(&count_levels)) {
		ParseNode *n = next;
		next = NULL;
		return n;
	}
	return NULL;
}

int
ParseNodeCountTree::get_parse_string(char *buf, size_t buflen) {
	size_t tlen = 0;
	struct count_level *cl;
	int num = 0;
	int ilen;

	ilen = next->get_parse_string(buf, buflen);
	if (ilen < 0)
		return ilen;
	buf[ilen++] = ',';
	tlen += ilen;
	if ((size_t)tlen >= buflen)
		return -1;

	SIMPLEQ_FOREACH(cl, &count_levels, list_link) {
		if (cl->is_union)
			ilen = cl->u->get_parse_string(buf + tlen, buflen - tlen);
		else
			ilen = cl->ar->get_parse_string(buf + tlen, buflen - tlen);

		if (ilen < 0)
			return ilen;
		tlen += ilen;
		buf[tlen++] = ',';
		if (tlen >= buflen)
			return -1;
		num++;
	}
	ilen = snprintf(buf + tlen, buflen - tlen, "%dct(%s)", num, tag ?: "" );
	if (ilen < 0 || tlen + ilen >= buflen)
		return -1;
	return tlen + ilen;
}
