#include <assert.h>
#include "parse_nodes.hh"
#include "util.h"
#include "db.h"
#include "search.h"

ParseNode *
ParseNodePhrase::initialize(struct result *res) {
	ParseNode *n, *in = NULL;
	SLIST_HEAD(phrase_word_list, ParseNode) ipn = SLIST_HEAD_INITIALIZER(ipn);
	bool empty = false;

	while ((n = SLIST_FIRST(&phrase_nodes))) {
		SLIST_REMOVE_HEAD(&phrase_nodes, list_link);
		in = n->initialize(res);
		if (in && in != n) {
			delete n;
			if (typeid(*in) == typeid(ParseNodeEmpty)) {
				empty = true;
				break;
			}
			n = in;
		}
		SLIST_INSERT_HEAD(&ipn, n, list_link);
	}

	/* Need to reverse again. */
	while ((n = SLIST_FIRST(&ipn))) {
		SLIST_REMOVE_HEAD(&ipn, list_link);
		SLIST_INSERT_HEAD(&phrase_nodes, n, list_link);
	}

	if (empty)
		return in;

	return NULL;
}

/* Get the in-ad index for the current ad. */
void
ParseNodeInvwordPos::reset_word_index_ptr() {
	const struct docindex &di = invword->docs[docptr];

	if (di.ptr != INDEX_DOCINDEX_NO_POS) {
		word_index_len = invword->docpos[di.ptr].pos;
		word_index_ptr = &invword->docpos[di.ptr + 1];
	} else {
		word_index_len = 0;
		word_index_ptr = NULL;
	}

	word_index = 0;
}

/* Check wether the current doc contains the word in position pos + phrase_index */
int
ParseNodeInvwordPos::doc_match_pos(int pos) {
	/* Also check flags if they are set. */
	while (word_index < word_index_len &&
			(word_index_ptr[word_index].pos < phrase_index + pos
			|| (pos_flags && (word_index_ptr[word_index].flags & pos_flags) != pos_flags))) {
		word_index++;
	}
	if (word_index >= word_index_len) {
		/* Exhausted */
		return -1;
	}
	if (word_index_ptr[word_index].pos == phrase_index + pos) {
		return 0;
	} else {
		return word_index_ptr[word_index].pos - phrase_index;
	}
}

/*
 * Check wether the phrase-chain can be found in the currently
 * selected doc.
 */

int
ParseNodePhrase::match_phrase() {
	int pos = 0;     /* Current position of the first word */
	int res;
	ParseNode *n;
	ParseNodeInvwordPos *in;

	/* Set word index ptr for the document. */
	SLIST_FOREACH(n, &phrase_nodes, list_link) {
		in = (ParseNodeInvwordPos*)n;

		in->reset_word_index_ptr();
	}

	while (1) {
		SLIST_FOREACH(n, &phrase_nodes, list_link) {
			in = (ParseNodeInvwordPos*)n;
			/* Does the ad contain the current word in position pos + phrase_index?
			 * If not, start from the beginning.
			 */	
			res = in->doc_match_pos(pos);

			if (res == -1) {
				/* No match found, at least one in-ad index exhausted. */
				return 0;
			} else if (res) {
				/* No match found. res is set to the next possible position
				 * for the first word.
				 */
				pos = res;
				break;
			}
		}

		/* Got to the end of the chain. The current ad is a match! */
		if (n == TAILQ_END(&phrase_nodes))
			return 1;
	}
	/* Not reached */
}


int32_t
ParseNodePhrase::next_document(int32_t doc) {
	int32_t d = 0;
	int32_t search = doc;
	ParseNode *n;
	ParseNodeInvwordPos *start_node = NULL;

	assert(!SLIST_EMPTY(&phrase_nodes));

	while (1) {
		do {
			SLIST_FOREACH(n, &phrase_nodes, list_link) {
				ParseNodeInvwordPos *in = (ParseNodeInvwordPos*)n;

				/* 1 completed iteration over all nodes with all current documents
				 * equal. */
				if (in == start_node) {
					start_node = NULL;
					break;
				}

				d = in->get_next_document(search);

				if (d == NULL_DOCOFF) {
					return NULL_DOCOFF;
				}

				if (d != search) {
					search = d;
					start_node = in;
				}
			}
		} while (start_node);

		if (match_phrase()) {
			return d;
		}
		search = d;
		increment_doc(&search);
	}

	return d;
}

ParseNodePhrase::~ParseNodePhrase() {
	ParseNodeInvwordPos *n;

	while (!SLIST_EMPTY(&phrase_nodes)) {
		n = (ParseNodeInvwordPos*)SLIST_FIRST(&phrase_nodes);
		SLIST_REMOVE_HEAD(&phrase_nodes, list_link);
		delete n;
	}
}

int
ParseNodePhrase::get_parse_string(char *buf, size_t buflen) {
	size_t tlen = 0;
	ParseNode *n;
	int ilen;

	SLIST_FOREACH(n, &phrase_nodes, list_link) {
		ParseNodeInvwordPos *in = (ParseNodeInvwordPos*)n;

		if (n != SLIST_FIRST(&phrase_nodes))
			buf[tlen++] = ' ';
		if (tlen >= buflen)
			return -1;

		ilen = in->get_parse_string(buf + tlen, buflen - tlen);

		if (ilen < 0)
			return ilen;
		tlen += ilen;
	}

	return tlen;
}
