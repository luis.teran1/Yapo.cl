#include <string.h>
#include <sys/mman.h>
#include <tgmath.h>

#include "tree.h"
#include "queue.h"
#include "util.h"
#include "logging.h"
#include "db.h"
#include "subs.h"

#define SUB_LEN 2

static __inline int
sub_compare(const struct sub *a, const struct sub *b) {
	return a->key.i - b->key.i;
}

RB_GENERATE_STATIC(sub_tree, sub, tree, sub_compare);

struct sub *
sub_lookup(struct bsearch_cache *db, const char *sub) {
	struct sub f = {{0}};
	struct sub_tree *subs = db->subs;

	f.key.i = 0;

	memcpy(&f.key.sub, sub, SUB_LEN);
	f.key.sub[SUB_LEN] = '\0';
	return RB_FIND(sub_tree, subs, &f);
}

static size_t
subs_estimate_size(int words, int wordlen, int nchars) {
	return  
		sizeof(struct sub_tree) + 
		words * sizeof(struct sub_word) + 
		sizeof(struct sub_cons) * (wordlen - words * (SUB_LEN - 1)) +
		pow(nchars, SUB_LEN) * sizeof(struct sub);
}

static void *
subs_alloc(struct sub_tree *st, size_t size) {
	void *res = st->storage_offset;
	if (!st)
		xerrx(1, "No sub_tree struct in subs_alloc");

	if ((char*)st->storage_offset - (char*)st->storage + size > st->storage_size)
		xerrx(1, "out of memory in subs_alloc");

	st->storage_offset = (char*)st->storage_offset + size;

	return res;
}

struct sub_tree *
subs_init(int words, int tot_word_len, int nchars) {
	void *m;
	size_t s = subs_estimate_size(words, tot_word_len, nchars);
	struct sub_tree *st;

	log_printf(LOG_INFO, "Allocating space for subs(%d): %lu bytes", tot_word_len, (unsigned long)s);
	m = mmap(0, s, PROT_READ | PROT_WRITE, MAP_ANONYMOUS | MAP_PRIVATE, 0, 0);

	if (m == MAP_FAILED) {
		xerrx(1, "mmap failed for subs_init (%lu bytes)", (unsigned long)s);
	}

	st = m;

	st->storage = m;
	st->storage_size = s;
	st->storage_offset = (char*)m + sizeof(*st);
	RB_INIT(st);
	SLIST_INIT(&st->word_list);

	return st;
}

void
subs_free(struct bsearch_cache *db) {
	struct sub_tree *subs = db->subs;
	if (!subs)
		return;

	munmap(subs->storage, subs->storage_size);
}

void
subs_register(struct bsearch_cache *db, const char *word) {
	struct sub_tree *subs = db->subs;
	struct sub_word *w;
	struct sub *s;
	size_t wlen = strlen(word);
	int n;
	int i;

	if (!subs) {
		xerrx(1, "no subs structure passed to subs_register");
	}

	if (wlen < SUB_LEN)
		return;

	w = subs_alloc(subs, sizeof(*w));
	w->word = word;

	SLIST_INSERT_HEAD(&subs->word_list, w, next);

	n = wlen - (SUB_LEN - 1);

	w->c = subs_alloc(subs, sizeof(*(w->c)) * n);

	for (i = 0; i < n; i++) {
		s = sub_lookup(db, word + i);
		if (s == NULL) {
			s = subs_alloc(subs, sizeof(*s));
			s->key.i = 0;
			SLIST_INIT(&s->list);
			memcpy(&s->key.sub, word + i, SUB_LEN);
			RB_INSERT(sub_tree, subs, s);
		}

		w->c[i].w = w;
		SLIST_INSERT_HEAD(&s->list, &(w->c[i]), next);
	}
}

int
sub_match(const char *needle, const char *haystack, int type) {
	int nl = strlen(needle);
	int hl = strlen(haystack);

	if (nl > hl)
		return 0;

	switch (type) {
	case SUB_LEFT:
		return strncmp(haystack, needle, nl) == 0;
	case SUB_MIDDLE:
		return strstr(haystack, needle) != NULL;
	case SUB_RIGHT:
		return memcmp(haystack + hl - nl, needle, nl) == 0;
	}

	return 0;
}
