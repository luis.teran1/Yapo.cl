#ifndef COMMAND_H
#define COMMAND_H

void *command_thread(void *thread_data);

struct ctrl_thread;
int setup_controllers(struct search_engine *, struct ctrl_thread **);

#endif /*COMMAND_H*/
