#include <assert.h>
#include <ctype.h>

#define QUEUE_TYPE_PREFIX /*class*/
#include "logging.h"
#include "parse_nodes.hh" 
#include "parse_node_cache.hh" 
#include "search.h"

#include <typeinfo>
#include <iostream>
#include <limits.h>

void
perform_search(struct result *result, ParseNode *node, struct timer_instance *ti, int collapse_subads,
		void (*load_sort_value)(void *mdarg, struct result *result, int32_t docoff), void *mdarg) {
	int32_t docoff;
	int32_t search;
	
	assert(node);

	result->sub_ad_counter = 0;
	result->ndocs = 0;

	search = -1; /* We want the first increment_doc to return 0. */

	while ((docoff = node->get_next_document(increment_doc(&search))) != NULL_DOCOFF) {
		search = docoff;

		if (collapse_subads && docoff_is_subdoc(result->si->db, docoff)) {
			/* 
			 * Go back through the document array while docs still
			 * have the same order (i e list time).  If they have
			 * the same suborder (i e parent list id) then don't
			 * insert them
			 */
			const struct doc *doc = get_doc(result->si->db, docoff);
			int32_t parent_id = -doc->suborder;
			int found = 0;

			/* Increment sub_ad_counter for each sub ad */
			result->sub_ad_counter++;

			if (result->ndocs > 0) {
				int32_t *doff = &result->docoff_arr[result->ndocs - 1];

				while (doff >= result->docoff_arr) {
					const struct doc *d = get_doc(result->si->db, *doff);
					if (d->order != doc->order)
						break;
					/* Documents with negative suborder can have parent ad already in search result */
					if (d->id == parent_id) {
						found = 1;
						break;
					}
					doff--;
				}
			}

			if (!found) {
				/* Insert parent ad to search result */
				int32_t ndocoff = load_docoff(result->si->db, parent_id);
				if (ndocoff != NULL_DOCOFF) {
					node->parent_doc_loaded(result, docoff, ndocoff);
					docoff = ndocoff;
				} else {
					log_printf(LOG_CRIT, "Parent ad not found for doc {%d, %d, %d}", (int)doc->id, (int)doc->order, (int)doc->suborder);
				}
			} else {
				continue;
			}
		}
		result->docoff_arr[result->ndocs] = docoff;
		if (__predict_false(load_sort_value))
			load_sort_value(mdarg, result, docoff);
		result->ndocs++;
		if (result->ndocs >= result->docoff_arr_sz) {
			/* We need to do this one document too early, to allow get_next_document to write to sort_value_arr */
			result->docoff_arr_sz *= 2;
			result->docoff_arr = (int32_t*)xrealloc(result->docoff_arr, result->docoff_arr_sz * sizeof(*result->docoff_arr));
			result->sort_value_arr = (union sort_value*)xrealloc(result->sort_value_arr, result->docoff_arr_sz * sizeof(*result->sort_value_arr));
		}
	}
	
	/* Get cached sub_ad_counter */
	result->sub_ad_counter += node->get_sub_ad_counter();
	node->finalize(result, ti);

	result->res_sortspec = node->get_sortspec();

	node->get_headers(&result->headers);
}

ParseNode *
main_search(struct result *result, ParseNode *node, timer_instance *ti, int print_parse, void (*pend_cb)(void*), void *pend_cb_arg) {
#if 0
	node->dump(0);
#endif
	ParseNodeCache cache(result->si->query_cache, 1, node, ti, print_parse, pend_cb, pend_cb_arg);
	ParseNode *n = cache.initialize(result);

#if 0
	n->dump(0);
#endif

	assert(n);
	perform_search(result, n, ti, 1, NULL, NULL);
	return n;
}

void
cleanup_search(ParseNode *node) {
	delete node;
}

/*
 * XXX it is probably quite possible for this function to crash on corrupt input
 */
ParseNode *
reconstruct_search(struct bsearch_cache *db, const char *parse_string, int *sortspec) {
	const char *cp;
	int start_num;
	int end_num;
	std::stack<ParseNode*> stack;
	bool is_not;
	const char *colp;
	ParseNode *new_node;
	
	log_printf(LOG_DEBUG, "reconstructing search: %s", parse_string);
	const char *start_parse_string = parse_string;

	*sortspec = 0;

	do {
		cp = strchr(parse_string, ',');
		if (!cp)
			cp = parse_string + strlen(parse_string);

		start_num = 0;
		end_num = INT_MAX;
		is_not = false;
		if (isdigit(*parse_string) || *parse_string == '-') {
			const char *pp = parse_string;

			while (isdigit(*pp) || *pp == '-')
				pp++;

			if (islower(*pp)) {
				for (; isdigit(*parse_string); parse_string++)
					start_num = start_num * 10 + *parse_string - '0';
				if (*parse_string == '-') {
					parse_string++;
					for (end_num = 0; isdigit(*parse_string); parse_string++)
						end_num = end_num * 10 + *parse_string - '0';
					if (end_num == 0)
						end_num = INT_MAX;
				}
			}
		} 

		if (*parse_string == '!') {
			is_not = true;
			parse_string++;
		}


		log_printf(LOG_DEBUG, "stack size: %zd, start_num = %d, end_num = %d, is_not = %d, parsing %-.*s", stack.size(), start_num, end_num, (int)is_not, (int) (cp - parse_string), parse_string);
		new_node = NULL;
		try {
			if ((colp = (const char*)memchr((const void*)parse_string, ':', cp - parse_string))) {
				const char *dp = (const char*)memchr((const void*)colp, '-', cp - colp);

				if (dp) {
					new_node = new ParseNodeAttributeRange(xstrndup(parse_string, colp - parse_string),
							xstrndup(colp + 1, dp - colp - 1),
							xstrndup(dp + 1, cp - dp - 1));
				} else {
					new_node = new ParseNodeInvattr(xstrndup(parse_string, cp - parse_string));
				}
			} else if ((*parse_string == '+' || *parse_string == '-' || *parse_string == '<' || *parse_string == '>') && *(parse_string + 1) == ';') {
				int sortval = 0;

				switch (*parse_string) {
				case '+':
					sortval = SORTSPEC_SORTVALUE | SORTSPEC_INC;
					break;
				case '-':
					sortval = SORTSPEC_SORTVALUE | SORTSPEC_DEC;
					break;
				case '>':
					sortval = SORTSPEC_ORDER | SORTSPEC_INC;
					break;
				case '<':
					sortval = SORTSPEC_ORDER | SORTSPEC_DEC;
					break;
				}
				parse_string += 2;
				if (cp - parse_string == 2 && strncmp(parse_string, "so", cp - parse_string) == 0)
					new_node = new ParseNodeSubsort(&stack, *sortspec = sortval);
			} else if ((*parse_string == '+' || *parse_string == '-') && (*(parse_string + 1) == '+' || *(parse_string + 1) == '-') && *(parse_string + 2) == ';') {
				int sortval = SORTSPEC_ATTR;

				if (*parse_string == '+')
					sortval |= SORTSPEC_INC;
				else
					sortval |= SORTSPEC_DEC;
				if (*(parse_string + 1) == '+')
					sortval |= SORTSPEC_EMPTY_LAST;
				else
					sortval |= SORTSPEC_EMPTY_FIRST;
				parse_string += 3;
				if (cp - parse_string == 2 && strncmp(parse_string, "so", cp - parse_string) == 0)
					new_node = new ParseNodeSubsort(&stack, *sortspec = sortval);
			} else if (start_num || end_num != INT_MAX || islower(*parse_string)) {
				switch (*parse_string) {
				case 's':
					new_node = new ParseNodeSubrange(&stack, start_num, end_num, is_not);
					break;

				case 'u':
					new_node = new ParseNodeUnion<>(&stack, start_num, end_num, is_not);
					break;

				case 'i':
					new_node = new ParseNodeIntersection(&stack, start_num, end_num, is_not);
					break;

				case 'c':
					if (parse_string == cp - 1)
						new_node = new ParseNodeCounter(&stack, start_num, end_num, is_not);
					else if (*++parse_string == '(')
						new_node = new ParseNodeCounter(&stack, start_num, end_num, is_not, parse_string + 1, cp - parse_string - 2);
					else if (*parse_string == 'l')
						new_node = new ParseNodeCountLevel(&stack, start_num, end_num, is_not);
					else if (*parse_string == 'a')
						new_node = new ParseNodeCountAll(&stack, start_num, end_num, is_not, parse_string + 2, cp - parse_string - 3);
					break;

				case 'f':
					new_node = new ParseNodeFilter(&stack, start_num, end_num, is_not);
					break;
				
				case 'm':
					new_node = new ParseNodeMinus(&stack, start_num, end_num, is_not);
					break;

				case 'e':
					new_node = new ParseNodeEmpty();
					break;
				}
			} else {
				const char *sp = (const char*)memchr(parse_string, ' ', cp - parse_string);

				if (sp) {
					ParseNodePhrase *phrase;

					new_node = phrase = new ParseNodePhrase();

					while (sp) {
						phrase->add_word(new ParseNodeInvwordPos(xstrndup(parse_string, sp - parse_string), 0));
						parse_string = sp + 1;
						sp = (const char*)memchr(parse_string, ' ', cp - parse_string);
					}
					phrase->add_word(new ParseNodeInvwordPos(xstrndup(parse_string, cp - parse_string), 0));
				} else {
					new_node = new ParseNodeInvword(xstrndup(parse_string, cp - parse_string));
				}
			}
		} catch (std::exception &e) {
			log_printf(LOG_CRIT, "reconstruct_search: exception parsing %s (near %s): %s, start_num = %d, end_num = %d, is_not = %d", start_parse_string, parse_string, e.what(), start_num, end_num, (int)is_not);
			if (new_node)
				delete new_node;
			while (!stack.empty()) {
				delete stack.top();
				stack.pop();
			}
			return NULL;
		}
		if (new_node)
			stack.push(new_node);

		parse_string = cp + 1;
	} while (*cp);

	if (stack.empty()) {
		log_printf(LOG_CRIT, "reconstruct_search: empty stack when done parsing '%s'", start_parse_string);
		return NULL;
	}
	if (stack.size() > 1) {
		log_printf(LOG_CRIT, "reconstruct_search: stack size > 1 when done parsing '%s'", start_parse_string);
		while (!stack.empty()) {
			delete stack.top();
			stack.pop();
		}
		return NULL;
	}
	ParseNode *result = stack.top();
	/* Insert filtered counter. */
	return new ParseNodeCountAll(result, xstrdup("filtered"));
}
