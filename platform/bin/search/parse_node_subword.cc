#include "parse_nodes.hh"
#include "db.h"
#include "util.h"
#include "subs.h"
#include "logging.h"
#include "queue.h"
#include "search.h"

#include <stdio.h>

ParseNode *
ParseNodeSubword::initialize(struct result *res) {
	ParseNodeInvword *iw = NULL;
	ParseNodeUnion<ParseNodeInvword> *u = NULL;
	struct sub *sub;
	struct sub_cons *c;

	sub = sub_lookup(res->si->db, base);

	if (!sub)
		return new ParseNodeEmpty;

	SLIST_FOREACH(c, &sub->list, next) {
		if (sub_match(base, c->w->word, sub_type)) {
			if (iw && !u) {
				u = new ParseNodeUnion<ParseNodeInvword>;
				u->add_node(iw);
			}
			iw = new ParseNodeInvword(xstrdup(c->w->word), 0, pos_flags, NULL);
			iw->initialize(res);
			if (u)
				u->add_node(iw);
		}
	}

	if (u)
		return u;
	if (iw)
		return iw;
	return new ParseNodeEmpty;
}

int
ParseNodeSubword::get_parse_string(char *buf, size_t buflen) {
	int len;

	len = snprintf(buf, buflen, "%s%s%s", sub_type & (SUB_MIDDLE|SUB_RIGHT) ? "*" : "", base,
			sub_type & (SUB_MIDDLE|SUB_LEFT) ? "*" : "");

	if (len < 0 || (size_t)len >= buflen)
		return -1;
	return len;
}

