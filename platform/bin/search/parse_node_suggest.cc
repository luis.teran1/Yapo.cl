
#include "parse_node_suggest.hh"
#include "search.h"
#include "stats.h"

#include "bursttrie.h"
#include "unicode.h"
#include "hash.h"
#include "charmap.h"

struct internal_search_rec {
	struct suggestion_instance *sug;
	struct suggestion_candidate *sc;
	struct suggest_state *ss;
	int utf8;
};

/* Helper function to look up a word using brt_ser_find() and get both the bare and sumtree counts */
static brt_count
internal_find_with_sumtree(struct internal_search_rec *rec, const char *word, int *bare_count) {
	void* userdata = NULL;
	brt_count c = brt_ser_find(rec->sug->map_brt, rec->sug->brt_offset, word, &userdata);

	if (bare_count)
		*bare_count = c;

	if (userdata && rec->ss->category && rec->sug->sumtree && rec->sug->map_sumtree) {
		uint32_t sumtree_idx = *((uint32_t*)userdata);
		if (sumtree_idx) {
			uint32_t* scores = (uint32_t*)(void*)(rec->sug->map_sumtree + rec->sug->sumtree_offset + (sumtree_idx-1)*sumtree_size(rec->sug->sumtree)*sizeof(uint32_t));
			c = sumtree_get(rec->sug->sumtree, scores, rec->ss->category);
		}
	}

	return c;
}

/*
	Note: The brt_ser_levenshtein threshold is against the bare counts,
	so we will have callbacks on words with sumtree(score) < threshold
*/
static brt_count
suggestion_lev_cb(const char* candidate, brt_count count, int dist, void* userdata, void* cbdata) {
	struct internal_search_rec *rec = (struct internal_search_rec*)cbdata;
	struct suggestion_candidate *sc = rec->sc;
	brt_count bare_count = count;

	/* Reject suggestionsi below the minimum length */
	if (rec->sug->min_len_levenshtein_suggest > 0) {
		int len = rec->utf8 ? strlen_utf8(candidate) : strlen(candidate);
		if (len < rec->sug->min_len_levenshtein_suggest)
			return 0;
	}

	if (userdata && rec->ss->category && rec->sug->sumtree && rec->sug->map_sumtree) {
		uint32_t sumtree_idx = *((uint32_t*)userdata);
		if (sumtree_idx) {
			uint32_t *scores = (uint32_t*)(void*)(rec->sug->map_sumtree + rec->sug->sumtree_offset + (sumtree_idx-1)*sumtree_size(rec->sug->sumtree)*sizeof(uint32_t));
			count = sumtree_get(rec->sug->sumtree, scores, rec->ss->category);
		}
	}

	if ((dist < sc->s_k && (int)count > sc->s_threshold) || (dist == sc->s_k && (int)count > sc->s_count)) {
		/* TODO OPT: Static/reused buffer based on max_key_len in BRT instead of dups */
		free(sc->s_word);
		sc->s_word = xstrdup(candidate);
		sc->s_k = dist;
		sc->s_count = count;
		sc->s_bare_count = bare_count;
	}
	return 0;
}

void
ParseNodeSuggest::suggestion_search(struct result* result) {

	if (!result->si->suggestions || !ss.db || !ss.max_k) {
		log_printf(LOG_DEBUG, "No suggestion search due to incomplete data or missing configuration.");
		return;

	}

	struct suggestion_instance *sug = (struct suggestion_instance*)hash_table_search(result->si->suggestions, ss.db, -1, NULL);
	if (!sug) {
		log_printf(LOG_WARNING, "No suggestion search due to selected db '%s' not being configured.", ss.db);
		return;
	}

	/* Return if there are many documents in the result */
	if ((int)result->ndocs > sug->search_trigger) {
		log_printf(LOG_DEBUG, "Not running suggestions, ndocs %d > search_trigger %d", result->ndocs, sug->search_trigger);
		return;
	}

	int threshold = ss.misspelled_threshold < 0 ? sug->default_misspelled_threshold : ss.misspelled_threshold;

	log_printf(LOG_DEBUG, "Running suggestion search: max_k=%d, db=%s, threshold=%d, category=%d", ss.max_k, ss.db, threshold, ss.category);

	struct internal_search_rec search_rec;
	search_rec.utf8 = charmap_is_utf8(result->si->sic->charmap) ? 1 : 0;
	search_rec.sug = sug;
	search_rec.ss = &ss;

	struct suggestion_candidate *sc = NULL, *sc_prev = NULL;
	/* Look up counts and check do compounding */
	TAILQ_FOREACH(sc, &ss.candidates, next) {
		search_rec.sc = sc;
		sc->count = internal_find_with_sumtree(&search_rec, sc->word, &sc->bare_count);
		log_printf(LOG_DEBUG, "SUG_INPUT: %s: counts %d/%d", sc->word, sc->bare_count, sc->count);

		if (sc_prev && (sc->count < sug->default_no_compound_threshold || sc_prev->count < sug->default_no_compound_threshold)) {
			if (!sc_prev->s_word && sc->op == IOP_AND) {
				char* comp;
				xasprintf(&comp, "%s%s", sc_prev->word, sc->word);
				int count = internal_find_with_sumtree(&search_rec, comp, &sc->bare_count);

				log_printf(LOG_DEBUG, "SUGGEST COMPOUND CHECK '%s:%d'", comp, count);
				if (count > sc->count || count > sc_prev->count) {
					bscat(&infos, "info:suggest:%s %s:%s:%d\n", sc_prev->word, sc->word, comp, count);
					sc->compound_score = count;
					sc_prev->compound_score = count;
					stat_inc(NULL, "SUGGESTIONS COMPOUND");
				}
				free(comp);
			}
		}
		sc_prev = sc;
	}

	/* Use counts to search for suggestions for any words that we haven't otherwise used in a suggestion */
	int num_searches_done = 0;
	TAILQ_FOREACH(sc, &ss.candidates, next) {
		if (num_searches_done >= sug->max_levenshtein_searches) {
			break;
		}
		if (sc->word_len < sug->min_len_levenshtein_search) {
			continue;
		}
		log_printf(LOG_DEBUG, "SUGGEST %s count < threshold && !compound_score: %d < %d && %d", sc->word, sc->count, threshold, sc->compound_score);
		if (sc->count < threshold && !sc->compound_score) {
			sc->s_word = NULL;
			sc->s_threshold = sc->count; /* threshold or sc->count here, that's the question */
			sc->s_k = 999;
			search_rec.sc = sc;
			brt_ser_levenshtein(sug->map_brt, sc->word, ss.max_k, threshold, (enum ser_search_flags)0, suggestion_lev_cb, &search_rec);
			if (sc->s_word) {
				log_printf(LOG_DEBUG, "SUGGEST %s->%s: count %d", sc->word, sc->s_word, sc->s_count);
				bscat(&infos, "info:suggest:%s:%s:%d\n", sc->word, sc->s_word, sc->s_count);
				stat_inc(NULL, "SUGGESTIONS");
			}
			++num_searches_done;
		}
	}
}

int
ParseNodeSuggest::get_parse_string(char *buf, size_t buflen)
{
	int nlen = next->get_parse_string(buf, buflen);
	if (nlen < 0)
		return nlen;

	int res = snprintf(buf + nlen, buflen - nlen, ",sug(%d,%d,%d,%s)", ss.max_k, ss.misspelled_threshold, ss.category, ss.db);
	if (res < 0 || (size_t)res >= buflen - nlen)
		return -1;

	return res + nlen;
}

void
ParseNodeSuggest::finalize(struct result *res, struct timer_instance *ti)
{
	next->finalize(res, ti);

	struct timer_instance *subti = timer_start(ti, "suggestion_search");
	suggestion_search(res);
	timer_end(subti, NULL);
}

void
ParseNodeSuggest::get_headers(struct buf_string *header)
{
	next->get_headers(header);

	if (infos.buf) {
		/* Write the info lines into the result buffer */
		bswrite(header, infos.buf, infos.pos);
	}
}
