
#include "parse_nodes.hh"
#include "parse_node_cache.hh"
#include "lru.h"
#include "logging.h"
#include "timer.h"
#include "util.h"
#include <string.h>

void
ParseNodeCacheStore::get_headers(struct buf_string *buf) {
	char ps[CACHE_KEY_SIZE];
	int len;

	if (!headers.pos)
		next->get_headers(&headers);
	if (buf) {
		if (headers.pos)
			bufwrite(&buf->buf, &buf->len, &buf->pos, headers.buf, headers.pos);
		switch (print_parse) {
		case 1:
			bscat(buf, "info:parse_string:%s\n", parse_string);
			break;
		case 2:
			len = next->get_parse_string(ps, sizeof(ps));
			if (len > 0)
				bscat(buf, "info:parse_string_post_init:%s\n", ps);
			break;
		}
	}
}

void
ParseNodeCacheStore::finalize(struct result *res, struct timer_instance *ti) {
	struct result *rescopy;

	next->finalize(res, ti);
	if (!headers.pos)
		get_headers(NULL);

	size_t sz = headers.pos + sizeof(*res->docoff_arr) * res->ndocs + sizeof(*rescopy);
	rescopy = (struct result*)zmalloc(sizeof(*rescopy));
	rescopy->headers = headers;
	rescopy->ndocs = res->ndocs;
	rescopy->sub_ad_counter = res->sub_ad_counter;
	rescopy->docoff_arr = (int32_t*)xmalloc(sizeof(*res->docoff_arr) * res->ndocs);
	memcpy(rescopy->docoff_arr, res->docoff_arr, sizeof(*res->docoff_arr) * res->ndocs);
	rescopy->res_sortspec = sortspec == SORTSPEC_UNSET ? next->get_sortspec() : sortspec;
	if (rescopy->res_sortspec & SORTSPEC_SORTVALUE) {
		rescopy->sort_value_arr = (union sort_value*)xmalloc(sizeof(*res->sort_value_arr) * res->ndocs);
		memcpy(rescopy->sort_value_arr, res->sort_value_arr, sizeof(*res->sort_value_arr) * res->ndocs);
		sz += sizeof(*res->sort_value_arr) * res->ndocs;
	}
	ce->storage = rescopy;
	lru_store(cache, ce, sz);
}

union sort_value
ParseNodeCacheLoad::current_sort_value() {
	struct result *res = (struct result*)ce->storage;
	return res->sort_value_arr[docptr - fixed_arr];
}

void
ParseNodeCacheLoad::finalize(struct result *res, struct timer_instance *ti) {
	/*
	 * If you don't set fixed_arr then the sort_value_arr is your responsibility.
	 */
	struct result *cacheres = (struct result*)ce->storage;
	if (fixed_arr && cacheres->sort_value_arr) {
		memcpy(res->sort_value_arr, cacheres->sort_value_arr, cacheres->ndocs * sizeof(*cacheres->sort_value_arr));
	}
}

ParseNode *
ParseNodeCache::initialize(struct result *res) {
	char parse_string[CACHE_KEY_SIZE];
	int len;
	ParseNode *n = next;

	len = next->get_parse_string(parse_string, sizeof(parse_string));
	if (len > 0) {
		int new_entry;
		struct lru_entry *ce;

		ce = ::cache_lru(cache, parse_string, &new_entry, pend_cb, pend_cb_arg);
		log_printf(LOG_DEBUG, "PARSESTRING: %s -> %s", parse_string, ce ? (new_entry ? "MISS" : "HIT") : (new_entry ? "FAIL" : "RECURSE"));

		if (ce) {
			if (!new_entry) {
				if (ti)
					timer_add_attribute(ti, "CACHE_HIT");
				return new ParseNodeCacheLoad(cache, ce, fixed_arr, parse_string, len, print_parse);
			} else {
				if (ti)
					timer_add_attribute(ti, "CACHE_MISS");
				next = NULL; /* Disown next */
				replace_initialize(&n, res);
				return new ParseNodeCacheStore(cache, ce, n, parse_string, len, print_parse);
			}
		}
		if (ti) {
			if (new_entry)
				timer_add_attribute(ti, "CACHE_FULL");
			else
				timer_add_attribute(ti, "CACHE_RECURSE");
		}
	}
	next = NULL; /* Disown next */
	replace_initialize(&n, res);
	return n;
}

ParseNode*
ParseNodeCacheSearch::initialize(struct result *res) {
	/* Can't use query cache because of sub ads. Need a separate one. */
	ParseNodeCache cache(res->si->subquery_cache, 0, next, NULL);
	ParseNode *n = cache.initialize(res);
	ParseStore *p = n ? n->get_parse_store() : NULL;

	/* Disown next. */
	next = NULL;

	if (!p)
		return n;

	perform_search(res, n, NULL, 0, NULL, NULL);

	ParseNodeCacheLoad * pncl  = new ParseNodeCacheLoad(res->si->subquery_cache, p->ce, 0, p->parse_string, p->pslen, false);
	log_printf(LOG_DEBUG, "CacheSearch: retaining cache \"%s\"", p->parse_string);
	lru_retain(res->si->subquery_cache, p->ce);
	delete n;
	return pncl;
}

