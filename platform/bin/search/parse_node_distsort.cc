#define __STDC_LIMIT_MACROS
#include "parse_nodes.hh"
#include "parse_node_cache.hh"
#include "util.h"
#include "logging.h"
#include "qsort.h"
#include "search.h"
#include "bconf.h"

#include <stdio.h>
#include <stdint.h>
#include <float.h>
#include <assert.h>

/*
 * In the future we might want to make subsort generic and change this node to just rewrite the order.
 */
ParseNode *
ParseNodeDistanceSort::initialize(struct result *res) {
	/* XXX - maybe put this in meta? */
	const char *xattr = bconf_get_string(res->si->sic->bconf, "distance_search.attr.x");
	const char *yattr = bconf_get_string(res->si->sic->bconf, "distance_search.attr.y");
	const char *rattr = bconf_get_string(res->si->sic->bconf, "distance_search.attr.r");

	assert(sizeof(float) == sizeof(int));

	replace_initialize(&next, res);

	if (!xattr || !yattr) {
		ParseNode *n = next;
		next = NULL;
		return n;
	}

	xn = new ParseNodeAttrlistSort(NULL, new ParseNodeAttributeRange(xstrdup(xattr), NULL, NULL), SORTSPEC_INT | SORTSPEC_INC | SORTSPEC_ATTR);
	xn->initialize(res);
	yn = new ParseNodeAttrlistSort(NULL, new ParseNodeAttributeRange(xstrdup(yattr), NULL, NULL), SORTSPEC_INT | SORTSPEC_INC | SORTSPEC_ATTR);
	yn->initialize(res);
	if (rattr) {
		rn = new ParseNodeAttrlistSort(NULL, new ParseNodeAttributeRange(xstrdup(rattr), NULL, NULL), SORTSPEC_INT | SORTSPEC_INC | SORTSPEC_ATTR);
		rn->initialize(res);
	}
	this->res = res;
	return NULL;
}

void
ParseNodeDistanceSort::finalize(struct result *res, struct timer_instance *ti) {
	next->finalize(res, ti);

	log_printf(LOG_DEBUG, "Sorting by distance increasing");

	sort_result(res, SORTSPEC_SORTVALUE | SORTSPEC_FLOAT | SORTSPEC_INC);
}

int
ParseNodeDistanceSort::get_parse_string(char *buf, size_t buflen) {
	int nlen;
	int ilen;

	nlen = next->get_parse_string(buf, buflen);
	if (nlen < 0)
		return nlen;

	ilen = snprintf(buf + nlen, buflen - nlen, ";ds(%f,%f)", xcoord, ycoord);
	if (ilen < 0 || (size_t)nlen + ilen >= buflen)
		return -1;
	return nlen + ilen;
}

void
ParseNodeDistanceSort::load_sort_value(int32_t docoff) {
	float dist = FLT_MAX; // = null_value
	int32_t xd;
	int32_t yd;

	if (xn == NULL || yn == NULL) {
		goto out;
	}

	xd = xn->get_next_document(docoff);
	yd = yn->get_next_document(docoff);


	if (xd == NULL_DOCOFF || yd == NULL_DOCOFF) {
		delete xn;
		delete yn;
		xn = NULL;
		yn = NULL;
		goto out;
	}
	if (xd == docoff && yd == docoff) {
		float x = xn->current_sort_value().i - xcoord;
		float y = yn->current_sort_value().i - ycoord;
		// float r
		dist = sqrtf(x * x + y * y);
		if (rn) {
			int32_t rd = rn->get_next_document(docoff);
			if (rd == NULL_DOCOFF) {
				delete rn;
				rn = NULL;
				goto out;
			}
			if (rd == docoff) {
				dist += (float)rn->current_sort_value().i;
			}
		}
	}

out:
	res->sort_value_arr[res->ndocs].f = dist;
}

int32_t
ParseNodeDistanceSort::next_document(int32_t doc)
{
	doc = next->get_next_document(doc);
	if (doc != NULL_DOCOFF)
		load_sort_value(doc);

	return doc;
}
