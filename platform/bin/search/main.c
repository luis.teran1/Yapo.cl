#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <getopt.h>
#include "search_lexer.h"
#include "search.h"
#include "util.h"
#include "bconfig.h"
#include "bconf.h"
#include "hunstem.h"
#include "charmap.h"
#include "logging.h"
#include "db.h"
#include "sock_util.h"
#include "chatter.h"
#include "daemon.h"
#include "ps_status.h"
#include "text.h"
#include "hash.h"

static void 
search_tcfatalfunc(const char *msg) {
	xerrx(1, "%s", msg);
}

struct bconf_node *bconf_override;

static int debug = 0;
static char *logname = NULL;
static char *loglevel = NULL;
static char *logtag = NULL;

extern char *pidfile;

static void
usage(char *app) {
	fprintf(stderr, "Usage: %s [--debug] [--threads int] [--config file] [--timeout int] [--searchport portnumber] [--commandport portnumber] [--logfile file|stdout|stderr] [--loglevel debug|info|critical]\n", app);
	fprintf(stderr, "   --debug:            Debugging mode, don't daemonize, read from stdin.\n");
	fprintf(stderr, "   --threads:          Number of chatter threads to start. [3]\n");
	fprintf(stderr, "   --config:           Config file [bsearch.conf]\n");
	fprintf(stderr, "   --timeout:          Timeout for reader in s. [inf]\n");
	fprintf(stderr, "   --searchport:       Port on which to listen for searches. [8080]\n");
	fprintf(stderr, "   --commandport:      Port on which to listen for commands. [8082]\n");
	fprintf(stderr, "   --keepalive-port:   Port on which to listen for keepalive searches. [8084]\n");
	fprintf(stderr, "   --logfile:          Filename to log to or stdin or stderr. [bsearch.log]\n");
	fprintf(stderr, "   --loglevel:         One of debug, info or critical. [critical]\n");
	fprintf(stderr, "   --db_name:          Name of the db (path). [bdb]\n");
	fprintf(stderr, "   --foreground:       Don't daemonize.\n");
	fprintf(stderr, "   --slow-query-time:  Log queries (LOG_INFO) if the execution time exceeds this limit (in ms). [%u]\n", 100);
	fprintf(stderr, "   --quick-start:      Do not wait 5 seconds for an early child exit.\n");
	fprintf(stderr, "   --engine-mode:      Use command socket instead of signals/fork to manage db reloads\n");
	fprintf(stderr, "   --yydebug:          Enable bison parse tracing\n");
	fprintf(stderr, "   --logtag:           Use this logtag instead of the default\n");
	fprintf(stderr, "   --index-file:       Name of the index file (instead of directory)\n");
	fprintf(stderr, "   --help:             print this message\n");
}

static void
populate_bconf_default(struct bconf_node **conf) {
	bconf_add_data(conf, "config_file", "search.conf");
	bconf_add_data(conf, "port.search", "8080");
	bconf_add_data(conf, "port.command", "8082");
	bconf_add_data(conf, "port.keepalive", "8084");
	bconf_add_data(conf, "cache_size", "512");
	bconf_add_data(conf, "threads", "10");
	bconf_add_data(conf, "db_name", "index");
	bconf_add_data(conf, "slow_query_log", "100");
}

static int
mainloop(void) {
	struct bconf_node *bconf_default = NULL;
	struct bconf_node *bconfig = NULL;
	struct bconf_node *bc;
	const char *config_file;

	set_ps_display("mainloop");

	log_register_thread("Main %d", (int)getpid());

	populate_bconf_default(&bconf_default);

	if ((config_file = bconf_get_string(bconf_override, "config_file")) == NULL)
		config_file = bconf_get_string(bconf_default, "config_file");

	if ((bc = config_init(config_file)) == NULL) {
		fprintf(stderr, "Failed to read config file (%s)\n", config_file);
		exit(1);
	}

	bconf_merge(&bconfig, bconf_default);
	bconf_merge(&bconfig, bc);
	bconf_merge(&bconfig, bconf_override);

	if (!bconf_get(bconfig, "default.lang")) {
		log_printf(LOG_CRIT, "Missing default.lang configuration!");
		exit(1);
	}

	/* Initialize charmap for char-conversion (to-upper, � -> U etc.) */
	if (!bconf_get_string(bconfig, "charmap"))
		xerrx(1, "No charmap configured");

	log_register_thread("Main %d", (int)getpid());
	log_printf(LOG_INFO, "Starting Blocket search engine");

	if (!debug) {
		run_swapper(bconf_default, bconf_override);
	} else {
		struct search_instance *si;
		struct search_engine se;

		se.bconf_default = bconf_default;
		se.bconf_override = bconf_override;

		si = search_instance_init(&se, NULL, NULL);

		/* Start a stdin chatter */
		chatter_stdin(si);
	}
	bconf_free(&bconf_default);
	bconf_free(&bconf_override);
	bconf_free(&bc);
	bconf_free(&bconfig);

	log_printf(LOG_INFO, "Closing bsearch");

	return 0;
}

int
main(int argc, char **argv) {
	int foreground = 0;
	int nobos = 0;
	int opt;

	argv = save_ps_display_args(argc, argv);

	while (1) {
		int option_index = 0;
		static struct option long_options[] = {
			{"debug", 0, 0, 0},
			{"threads", 1, 0, 0},
			{"config", 1, 0, 0},
			{"timeout", 1, 0, 0},
			{"searchport", 1, 0, 0},
			{"commandport", 1, 0, 0},
			{"logfile", 1, 0, 0},
			{"loglevel", 1, 0, 0},
			{"db_name", 1, 0, 0},
			{"foreground", 0, 0, 0},
			{"slow-query-time", 1, 0, 0},
			{"pidfile", 1, 0, 0},
			{"help", 0, 0, 0},
			{"nobos", 0, 0, 0},
			{"logtag", 1, 0, 0},
			{"cache_size", 1, 0, 0},
			{"quick-start", 0, 0, 0},
			{"index-file", 1, 0, 0},
			{0, 0, 0, 0}
		};

		opt = getopt_long(argc, argv, "", long_options, &option_index);
		if (opt == -1)
			break;

		if (opt == 0) {
			switch (option_index) {
				case 0:
					debug = 1;
					break;
				case 1:
					bconf_add_data(&bconf_override, "threads", optarg);
					break;
				case 2:
					bconf_add_data(&bconf_override, "config_file", optarg);
					break;
				case 3:
					bconf_add_data(&bconf_override, "timeout", optarg);
					break;
				case 4:
					bconf_add_data(&bconf_override, "port.search", optarg);
					break;
				case 5:
					bconf_add_data(&bconf_override, "port.command", optarg);
					break;
				case 6:
					logname = strdup(optarg);
					break;
				case 7:
					loglevel = strdup(optarg);
					break;
				case 8:
					bconf_add_data(&bconf_override, "db_name", optarg);
					break;
				case 9:
					foreground = 1;
					break;
				case 10:
					bconf_add_data(&bconf_override, "slow_query_log", optarg);
					break;
				case 11:
					set_pidfile(optarg);
					break;
				default:
				case 12:
					usage(argv[0]);
					exit(1);
				case 13:
					nobos = 1;
					break;
				case 14:
					logtag = strdup(optarg);
					break;
				case 15:
					bconf_add_data(&bconf_override, "cache_size", optarg);
					break;
				case 16:
					set_quick_start(1);
					break;
				case 17:
					bconf_add_data(&bconf_override, "index_file", optarg);
					break;
			}
		}
	}


	if (!logname) {
		logname = strdup("search.log");
	}
	if (!logtag) {
		logtag = strdup("search");
	}
	if (!debug && !foreground && (strcmp(logname, "stdout") == 0 || strcmp(logname, "stderr") == 0)) {
		fprintf(stderr, "Redirecting log to search.log before daemonizing\n");
		free(logname);
		logname = strdup("search.log");
	}
	if (!loglevel) {
		loglevel = strdup("debug");
	}

	tcfatalfunc = search_tcfatalfunc;
	if (strcmp(logname, "stderr") == 0) {
		log_setup_perror(logtag, loglevel);
		x_err_init_err();
	} else {
		log_setup(logtag, loglevel);
		x_err_init_syslog(logtag, 0, LOG_LOCAL0, LOG_INFO);
	}

	init_ps_display(logtag);
	set_ps_display("BOS");

	if (!debug && !foreground) {
		daemonify(nobos, mainloop);
	} else {
		if (pidfile)
			write_pidfile();

		if (nobos)
			return mainloop();
		else
			return bos(mainloop);
	}

	free(logname);
	free(loglevel);
	free(logtag);
	log_shutdown();


	return 0;
}
