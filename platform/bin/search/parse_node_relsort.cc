#define __STDC_LIMIT_MACROS
#include "parse_nodes.hh"
#include "parse_node_cache.hh"
#include "util.h"
#include "logging.h"
#include "qsort.h"
#include "search.h"

#include <stdio.h>
#include <stdint.h>
#include <assert.h>

ParseNode *
ParseNodeRelsort::initialize(struct result *res) {
	assert(sizeof(float) == sizeof(int));
	tot_ndocs = db_ndocs(res->si->db);
	this->res = res;
	return ParseNodeNext::initialize(res);
}

void
ParseNodeRelsort::finalize(struct result *res, struct timer_instance *ti) {
	next->finalize(res, ti);

	log_printf(LOG_DEBUG, "Sorting by relevance %s", (sortspec & SORTSPEC_INC) ? "increasing" : "decreasing");

	sort_result(res, get_sortspec());
}

int
ParseNodeRelsort::get_parse_string(char *buf, size_t buflen) {
	int nlen;
	int ilen;

	nlen = next->get_parse_string(buf, buflen);
	if (nlen < 0)
		return nlen;

	ilen = snprintf(buf + nlen, buflen - nlen, ",%c;rs", (sortspec & SORTSPEC_INC) ? '+' : '-');
	if (ilen < 0 || (size_t)nlen + ilen >= buflen)
		return -1;
	return nlen + ilen;
}

void
ParseNodeRelsort::load_sort_value(int32_t doc) {
	res->sort_value_arr[res->ndocs].f = get_current_bm25(tot_ndocs, 1.2);
#if 0
	fprintf(stderr, "load_sort_value: %d -> %f\n", doc, get_current_bm25(tot_ndocs, 1.2));
#endif
}

int32_t
ParseNodeRelsort::next_document(int32_t doc)
{
	doc = next->get_next_document(doc);
	if (doc != NULL_DOCOFF)
		load_sort_value(doc);
	return doc;
}
