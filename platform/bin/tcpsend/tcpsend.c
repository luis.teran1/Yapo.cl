#include <poll.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <stdio.h>
#include <stdlib.h>
#include <netdb.h>
#include <unistd.h>

static void
usage(const char *cmd) {
	printf("Usage: %s [-w timeout] HOST PORT\n", cmd);
}

int
main(int argc, char *argv[]) {
	struct addrinfo hints = { .ai_family = AF_UNSPEC, .ai_socktype = SOCK_STREAM };
	struct addrinfo *res;
	struct addrinfo *rp;
	struct pollfd pfds[2];
	int error;
	int fd;
	int opt;
	int timeout = -1;
	char buf[1024];
	ssize_t sz;
	const char *pname = argv[0];


	while ((opt = getopt(argc, argv, "w:")) != -1) {
		switch (opt) {
			case 'w':
				timeout = atoi(optarg) * 1000;
				break;
			default:
				usage(argv[0]);
				exit(EXIT_FAILURE);
		}
	}

	argc -= optind;
	argv += optind;

	if (argc < 2) {
		usage(pname);
		exit(EXIT_FAILURE);
	}

	if ((error = getaddrinfo(argv[0], argv[1], &hints, &res)) != 0) {
		printf("getaddrinfo: %s\n", gai_strerror(error));
		exit(EXIT_FAILURE);
	}

	for (rp = res; rp != NULL; rp = rp->ai_next) {
		if ((fd = socket(rp->ai_family, rp->ai_socktype, rp->ai_protocol)) == -1)
			continue;

		if (connect(fd, rp->ai_addr, rp->ai_addrlen) != -1)
			break;

		close(fd);
	}

	if (rp == NULL) {
		printf("Could not connect\n");
		exit(EXIT_FAILURE);
	}

	pfds[0].fd = STDIN_FILENO;
	pfds[0].events = POLLIN | POLLHUP;
	pfds[1].fd = fd;
	pfds[1].events = POLLIN;

	while (poll(pfds, 2, timeout) > 0) {
		if (pfds[1].revents & POLLIN) {
			sz = read(pfds[1].fd, buf, sizeof(buf));
			if (sz < 0) {
				break;
			} else if (sz == 0) {
				shutdown(fd, SHUT_RD);
				break;
			} else { 
				printf("%.*s", (int) sz, buf);
			}
		}

		if (pfds[0].revents & POLLIN) {
			sz = read(pfds[0].fd, buf, sizeof(buf));
			if (sz < 0) {
				break;
			} else if (sz == 0) {
				shutdown(fd, SHUT_WR);
			} else {
				send(fd, buf, sz, 0);
			}
		} else if (pfds[0].revents & POLLHUP) {
			shutdown(fd, SHUT_WR);
		}
	}

	close(fd);
	return 0;
}
