#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <err.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/param.h>
#include <sys/time.h>
#include <getopt.h>

#include "ctemplates.h"
#include "timer.h"
#include "bconf.h"
#include "queue.h"
#include "memalloc_functions.h"

/* 60 seconds ought to be enough for everyone. */
#define TEST_TIMEOUT_DEFAULT 60

struct test_suite {
	TAILQ_ENTRY(test_suite) link;
	TAILQ_HEAD(,test_case) depend;
	TAILQ_HEAD(,test_case) cases;
	TAILQ_HEAD(,test_case) cleanup;
	char *name;

	int succeeded;
	int failed;
	int skipped;
	struct timespec duration;
};

struct test_case {
	TAILQ_ENTRY(test_case) link;
	char *name;
	char *output;
	int skipped;
	int failure;
	struct timespec ts;
};

static char *outdir = NULL;
static char *makeargs = NULL;
static bool exit_on_error = false;

TAILQ_HEAD(,test_suite) suites = TAILQ_HEAD_INITIALIZER(suites);

static void
usage(const char* progname)
{
	fprintf(stderr, "Usage: %s [--outdir|-o <path>] [--make-args|-m <args>]\n", progname);
	fprintf(stderr, "Purpose:\n");
	fprintf(stderr, " Helper for running tests and generating a report.\n");
	fprintf(stderr, "Arguments:\n");
	fprintf(stderr, " --outdir		-- Path to write report to.\n");
	fprintf(stderr, " --make-args		-- Additional arguments to pass to Make.\n");
	fprintf(stderr, "\n\nExample:\n");
	fprintf(stderr, " $ %s --make-args=\"FLAVOR=regress\"\n\n", progname);
	exit(1);
}

static char *
normalize_path(char *p) {
	static char pwd[MAXPATHLEN];
	static size_t pl;

	if (!pl) {
		if (getcwd(pwd, sizeof(pwd)) == NULL) {
			err(1, "getcwd");
		}
		pl = strlen(pwd);
      		if (pl >= sizeof(pwd) - 1)
			errx(1, "pwd too long");
		pwd[pl] = '/';
		pwd[pl+1] = '\0';
		pl++;
	}
	if (!strncmp(p, pwd, pl))
		return strdup(p + pl);
	return strdup(p);
	
}

volatile sig_atomic_t chld;
volatile sig_atomic_t quit;
volatile sig_atomic_t alrm;

/*
 * We need to handle SIGCHLD and close the pipe forcibly because processes that
 * inherit stdin/stdout/stderr from the pipe can still be running.
 */
static void
sig_handler(int signum) {
	switch (signum) {
	case SIGCHLD:
		chld = 1;
		break;
	case SIGALRM:
		alrm = 1;
		break;
	default:
		quit = 1;
	}
}

static int
make(const char *dir, const char *target, const char *args, char **retbuf) {
	struct itimerval itv = { {0, 0}, {0, 0} };
	size_t rbs = 0;
	size_t rp = 0;
	int status;
	int fds[2];
	char *cmd;
	pid_t pid;
	size_t r;

	if (pipe(fds))
		err(1, "pipe");

	chld = 0;
	switch ((pid = fork())) {
	case -1:
		err(1, "fork");
	case 0:
		xasprintf(&cmd, "make -C %s %s %s 2>&1", dir, target, args ? args : "");
		close(fds[0]);
		if (dup2(fds[1], 1) == -1) {
			warn("dup2(1)");
			_exit(1);
		}
		if (dup2(fds[1], 2) == -1) {
			warn("dup2(2)");
			_exit(1);
		}
		execlp("sh", "sh", "-c", cmd, NULL);
		warn("execlp");
		_exit(1);
	default:
		break;
	}

	close(fds[1]);

	itv.it_value.tv_sec = TEST_TIMEOUT_DEFAULT;
	if (setitimer(ITIMER_REAL, &itv, NULL))
		err(1, "setitimer");

	do {
		if (!rbs) {
			rbs = 4096;
			if ((*retbuf = malloc(rbs)) == NULL)
				err(1, "malloc");
		}
		if (rp == rbs) {
			rbs *= 2;
			if ((*retbuf = realloc(*retbuf, rbs)) == NULL)
				err(1, "realloc");
		}
		r = read(fds[0], *retbuf + rp, rbs - rp);
		rp += r;
	} while (!alrm && !chld && r > 0);

	close(fds[0]);
	itv.it_value.tv_sec = 0;
	if (setitimer(ITIMER_REAL, &itv, NULL))
		err(1, "setitimer 2");

	if (alrm) {
		alrm = 0;
		kill(pid, SIGKILL);
	}

	if (wait(&status) == -1)
		err(1, "wait");

	return WIFEXITED(status) ? WEXITSTATUS(status) : -1;
}

static int status_active;
static int ontty;
static int tss;
static int cts;

static void
status_printfnl(const char *fmt, ...) {
	va_list ap;

	if (ontty && status_active) {
		printf("\r\x1B[K");
		fflush(stdout);
		if (cts)
			printf("(Suite %d/%d) ", cts, tss);
	}
	va_start(ap, fmt);
	vprintf(fmt, ap);
	va_end(ap);
	status_active = 1;
	if (!ontty) {
		printf("\n");
	}
	fflush(stdout);
}

static void
status_printf(const char *fmt, ...) {
	va_list ap;

	if (ontty && status_active) {
		printf("\r\x1B[K");
		fflush(stdout);
		if (cts)
			printf("(Suite %d/%d) ", cts, tss);
	}
	va_start(ap, fmt);
	vprintf(fmt, ap);
	va_end(ap);
	status_active = 0;
	fflush(stdout);
}

static void
status_errf(const char *fmt, ...) {
	va_list ap;

	if (ontty && status_active) {
		printf("\n");
	}
	if (ontty)
		printf("\x1b[31m");
	va_start(ap, fmt);
	vprintf(fmt, ap);
	va_end(ap);
	status_active = 0;
	if (ontty)
		printf("\x1b[0m");
	printf("\n");
}

static void
generate_tests(void) {
	char tmpnam[] = "/tmp/rptXXXXXX";
	struct test_suite *cursuite = NULL;
	int fd = mkstemp(tmpnam);
	char buf[64];
	char *lp;
	FILE *f;
	size_t sz = 0;
	char *mout = NULL;

	status_printfnl("generating list of tests");
	sprintf(buf, "%s RPT_OUT=%s", makeargs ?: "", tmpnam);
	if (make("regress", "regress-print-tests", buf, &mout)) {
		status_errf("make regress-print-tests failed");
		exit(1);
	}
	free(mout);
	status_printfnl("tests generated");

	unlink(tmpnam);

	f = fdopen(fd, "r+");
	lp = NULL;
	while (getline(&lp, &sz, f) > 0) {
		struct test_case *c;
		char *cmd;
		char *s;

		if ((cmd = strchr(lp, ' ')) == NULL) {
			errx(1, "invalid line: %s\n", lp);
		}
		*cmd++ = '\0';

		if ((s = strchr(cmd, '\n')))
			*s = '\0';
		if ((s = strchr(lp, ':')))
			*s = '\0';

		if (strcmp(lp, "PWD") == 0) {
			cursuite = calloc(1, sizeof(*cursuite));
			TAILQ_INIT(&cursuite->depend);
			TAILQ_INIT(&cursuite->cases);
			TAILQ_INIT(&cursuite->cleanup);
			cursuite->name = normalize_path(cmd);
			TAILQ_INSERT_TAIL(&suites, cursuite, link);
			tss++;
		} else if (strcmp(lp, "DEPEND") == 0) {
			if (!cursuite) errx(1, "DEPEND before PWD");
			c = calloc(1, sizeof(*c));
			c->name = strdup(cmd);
			TAILQ_INSERT_TAIL(&cursuite->depend, c, link);
		} else if (strcmp(lp, "TEST") == 0) {
			if (!cursuite) errx(1, "TEST before PWD");
			c = calloc(1, sizeof(*c));
			c->name = strdup(cmd);
			TAILQ_INSERT_TAIL(&cursuite->cases, c, link);
		} else if (strcmp(lp, "CLEANUP") == 0) {
			if (!cursuite) errx(1, "CLEANUP before PWD");
			c = calloc(1, sizeof(*c));
			c->name = strdup(cmd);
			TAILQ_INSERT_TAIL(&cursuite->cleanup, c, link);
		} else {
			errx(1, "invalid line: %s/%s\n", lp, cmd);
		}
		free(lp);
		lp = NULL;
		sz = 0;
	}
	fclose(f);
}

static int
run_case(struct test_case *tc, struct test_suite *ts, int cnum, struct bconf_node *sn, struct timer_instance *gti, const char *skip, const char *type) {
	struct timer_instance *ti;
	char cstr[64];
	int ret = 0;

	snprintf(cstr, sizeof(cstr), "%d", cnum);
	bconf_add_datav(&sn, 3, (const char *[]){"case", cstr, "name"}, tc->name, 0);
	bconf_add_datav(&sn, 3, (const char *[]){"case", cstr, "type"}, type, 0);
	if (skip == NULL) {
		char tbuf[64];
		double t;

		ti = timer_start(gti, tc->name);
		ret = make(ts->name, tc->name, NULL, &tc->output);
		timer_end(ti, &tc->ts);

		bconf_add_datav(&sn, 3, (const char *[]){"case", cstr, "output"}, tc->output, 0);

		t = (double)tc->ts.tv_sec + (double)tc->ts.tv_nsec / 1000000000.0;
		snprintf(tbuf, sizeof(tbuf), "%.4f", t);
		bconf_add_datav(&sn, 3, (const char *[]){"case", cstr, "duration"}, tbuf, 1);

		if (ret == -2) {
			bconf_add_datav(&sn, 3, (const char *[]){"case", cstr, "result"}, "failed", 0);
			bconf_add_datav(&sn, 3, (const char *[]){"case", cstr, "reason"}, "timeout", 0);
			tc->failure = 1;
			ts->failed++;
			status_errf("TIMEOUT %.4fms", t);
		} else if (ret != 0) {
			bconf_add_datav(&sn, 3, (const char *[]){"case", cstr, "result"}, "failed", 0);
			bconf_add_datav(&sn, 3, (const char *[]){"case", cstr, "reason"}, "test failure", 0);
			tc->failure = 1;
			ts->failed++;
			status_errf("FAIL %.4fms", t);
		} else {
			bconf_add_datav(&sn, 3, (const char *[]){"case", cstr, "result"}, "success", 0);
			ts->succeeded++;
			status_printfnl("OK %.4fms", t);
		}
	} else {
		ts->skipped++;
		tc->skipped = 1;
		bconf_add_datav(&sn, 3, (const char *[]){"case", cstr, "result"}, "skipped", 0);
		bconf_add_datav(&sn, 3, (const char *[]){"case", cstr, "reason"}, skip, 0);
		status_errf("SKIPPED (%s)", skip);
	}
	return ret;
}

static int
run_suite(struct test_suite *ts, struct bconf_node *b) {
	struct timer_instance *gti;
	int depend_failed = 0;
	struct test_case *tc;
	char nbuf[64];
	int cnum = 0;
	double t;
	int early_quit = quit;
	int status = 0;

	gti = timer_start(NULL, ts->name);
	TAILQ_FOREACH(tc, &ts->depend, link) {
		status_printf("Depend: %.30s ", tc->name);
		if (run_case(tc, ts, cnum++, b, gti, quit ? "interrupted" : NULL, "depend")) {
			depend_failed = 1;
			status = 1;
			break;
		}
	}
	TAILQ_FOREACH(tc, &ts->cases, link) {
		status_printf("Test case %.30s ", tc->name);
		if (run_case(tc, ts, cnum++, b, gti, depend_failed ? "depend" : (quit ? "interrupted" : NULL), "test"))
			status = 1;
	}
	TAILQ_FOREACH(tc, &ts->cleanup, link) {
		status_printf("Cleanup: %.30s ", tc->name);
		run_case(tc, ts, cnum++, b, gti, early_quit ? "interrupted" : NULL, "cleanup");
	}
	timer_end(gti, &ts->duration);

	t = (double)ts->duration.tv_sec * 1000.0 + (double)ts->duration.tv_nsec / 1000000.0;
	status_printfnl("Suite duration: %.4f", t);
	snprintf(nbuf, sizeof(nbuf), "%.4f", t);
	bconf_add_data(&b, "duration", nbuf);
	snprintf(nbuf, sizeof(nbuf), "%d", ts->failed);
	bconf_add_data(&b, "failed", nbuf);
	snprintf(nbuf, sizeof(nbuf), "%d", ts->succeeded);
	bconf_add_data(&b, "succeeded", nbuf);
	snprintf(nbuf, sizeof(nbuf), "%d", ts->skipped);
	bconf_add_data(&b, "skipped", nbuf);

	return status;
}

static void
exit_program(int level) {
	free(outdir);
	free(makeargs);
	exit(level);
}

static int
parse_options(int argc, char **argv) {
	int opt;

	while (1) {
		int option_index = 0;
		static struct option long_options[] = {
			{"outdir", 1, NULL, 'o'},
			{"make-args", 1, NULL, 'm'},
			{"help", 0, NULL, 'h'},
			{"exit-on-error", 0, NULL, 'e'},
			{0, 0, 0, 0}
		};

		opt = getopt_long(argc, argv, "o:he", long_options, &option_index);
		if (opt == -1)
			break;

		switch (opt) {
			case 'o':
				outdir = xstrdup(optarg);
				break;
			case 'm':
				makeargs = xstrdup(optarg);
				break;
			case 'h':
				usage(argv[0]);
				break;
			case 'e':
				exit_on_error = true;
				break;
		}
	}

	return 0;
}

int
main(int argc, char **argv) {
	struct bconf_node *n, *s, *b = NULL;
	struct test_suite *ts;
	struct sigaction sa;
	int i;
	int status = 0;

	if ((i = parse_options(argc, argv)) != 0) {
		exit_program(i);
	}

	ontty = isatty(1);

	generate_tests();

	memset(&sa, 0, sizeof(sa));
	sa.sa_handler = sig_handler;
	if (sigaction(SIGCHLD, &sa, NULL))
		err(1, "sigaction");
	if (sigaction(SIGINT, &sa, NULL))
		err(1, "sigaction");
	if (sigaction(SIGQUIT, &sa, NULL))
		err(1, "sigaction");
	if (sigaction(SIGTERM, &sa, NULL))
		err(1, "sigaction");
	if (sigaction(SIGHUP, &sa, NULL))
		err(1, "sigaction");
	if (sigaction(SIGALRM, &sa, NULL))
		err(1, "sigaction");

	i = 0;
	TAILQ_FOREACH(ts, &suites, link) {
		char bkey[64];

		cts++;
		status_printfnl("SUITE: %s", ts->name);

		snprintf(bkey, sizeof(bkey), "suites.%d.name", i);
		bconf_add_data(&b, bkey, ts->name);
		snprintf(bkey, sizeof(bkey), "suites.%d", i);
		int ret = run_suite(ts, bconf_get(b, bkey));
		if (ret > status)
			status = ret;
		if (ret && exit_on_error)
			break;
		i++;
	}
	cts = 0;

	if (outdir == NULL)
		exit_program(status);

	s = bconf_get(b, "suites");
	for (i = 0; (n = bconf_byindex(s, i)) != NULL; i++) {
		struct bpapi api;
		char *ofname, *c;
		size_t dl;
		int fd;

		xasprintf(&ofname, "%s/%s.xml", outdir, bconf_get_string(n, "name"));
		/* We don't want the test name to have slashes. */
		dl = strlen(outdir) + 1;
		while ((c = strchr(ofname + dl, '/')))
			*c = '_';
		if ((fd = open(ofname, O_RDWR|O_CREAT|O_TRUNC, 0644)) == -1)
			err(1, "open(%s)", ofname);
		if (bconf_get_int(n, "failed") > 0)
			status_errf("Writing test output for (%s) into %s (%s failed)", bconf_get_string(n, "name"), ofname, bconf_get_string(n, "failed"));
		free(ofname);

		BPAPI_INIT_BCONF(&api, n, fd, hash);
		fd_output_set_fd(&api.ochain, &fd);
		call_template(&api, "out");
		flush_template_cache("out");
		bpapi_free(&api);
		close(fd);
	}
	status_errf("");
	exit_program(status);
}
