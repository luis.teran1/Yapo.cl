#include <getopt.h>
#include <pthread.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include "ack_fifo.h"
#include "bconf.h"
#include "bconfig.h"
#include "daemon.h"
#include "decoder.h"
#include "error_functions.h"
#include "logging.h"
#include "memalloc_functions.h"
#include "queue.h"
#include "receiver.h"
#include "redis_writer.h"
#include "statpoints.h"

struct queue input_events;
struct queue statpoints_events;
struct queue afs_events;

struct bconf_node *bconf_override;

int set_thread_name(pthread_t thread, const char *name) {
/* This function requires glibc >= 2.12 which is newer than what's provided on CentOS 5 */
#if (_POSIX_C_SOURCE - 0) >= 200809L
	return pthread_setname_np(thread, name);
#else
	return 0;
#endif
};


static void
usage(char *app) {
	fprintf(stderr, "Usage: %s [--config file] [--foreground] [--loglevel debug|info|critical] [--logtag tag] [--nobos] [--pidfile file] [--quick-start] [--uid uid]\n", app);
	fprintf(stderr, "   --config:           Config file [statpoints.conf]\n");
	fprintf(stderr, "   --foreground:       Don't daemonize\n");
	fprintf(stderr, "   --loglevel:         One of debug, info or critical. [critical]\n");
	fprintf(stderr, "   --logtag:           Use this logtag instead of the default\n");
	fprintf(stderr, "   --nobos:            Disable BOS\n");
	fprintf(stderr, "   --pidfile:          Set pid file\n");
	fprintf(stderr, "   --quick-start:      Do not wait 5 seconds for an early child exit.\n");
	fprintf(stderr, "   --uid:              Specifies which UID to run statpoints under\n");
	fprintf(stderr, "   --help:             print this message\n");
}

static void
populate_bconf_default(struct bconf_node **conf) {
	bconf_add_data(conf, "config_file", "statpoints.conf");
	bconf_add_data(conf, "ack_fifo.enabled", "0");
}

static struct bconf_node *
load_config(void) {
	struct bconf_node *bconf = NULL;
	struct bconf_node *bconf_default = NULL;
	struct bconf_node *bc;
	const char *config_file;

	populate_bconf_default(&bconf_default);

	if ((config_file = bconf_get_string(bconf_override, "config_file")) == NULL)
		config_file = bconf_get_string(bconf_default, "config_file");
	
	if ((bc = config_init(config_file)) == NULL) {
		xerrx(1, "Failed to read config file (%s)\n", config_file);
	}

	load_bconf("statpoints", &bc);

	bconf_merge(&bconf, bconf_default);
	bconf_merge(&bconf, bc);
	bconf_merge(&bconf, bconf_override);

	bconf_free(&bconf_default);
	bconf_free(&bconf_override);
	bconf_free(&bc);
	return bconf;
}

static int
mainloop(void) {
	sigset_t runset;
	sigset_t oldset;
	int sig;
	int stop = 0;
	struct bconf_node *bconf;
	struct receiver_state *rs = NULL;
	struct decoder_state *ds = NULL;
	struct redis_writer_state *rws = NULL;
	struct ack_fifo_state *afs = NULL;
	log_printf(LOG_DEBUG, "Entered mainloop");
	
	TAILQ_INIT(&input_events.queue);
	pthread_cond_init(&input_events.cond, NULL);
	pthread_mutex_init(&input_events.mutex, NULL);

	TAILQ_INIT(&statpoints_events.queue);
	pthread_cond_init(&statpoints_events.cond, NULL);
	pthread_mutex_init(&statpoints_events.mutex, NULL);

	TAILQ_INIT(&afs_events.queue);
	pthread_cond_init(&afs_events.cond, NULL);
	pthread_mutex_init(&afs_events.mutex, NULL);
	
	log_printf(LOG_DEBUG, "Setting signals to listen on");
	sigfillset(&runset);
	sigdelset(&runset, SIGSEGV);
	sigdelset(&runset, SIGTRAP);
	sigdelset(&runset, SIGFPE);
	sigdelset(&runset, SIGILL);
	sigdelset(&runset, SIGABRT);
	sigdelset(&runset, SIGALRM);
	sigprocmask(SIG_SETMASK, &runset, &oldset);

	bconf = load_config(); 

	rs = start_receiver(bconf, &input_events);
	ds = start_decoder(bconf, &input_events, &statpoints_events, &afs_events);
	rws = start_redis_writer(bconf, &statpoints_events);
	if (bconf_get_int(bconf, "ack_fifo.enabled"))
		afs = start_ack_fifo(bconf, &afs_events);

	while(!stop && sigwait(&runset, &sig) == 0) {
		log_printf(LOG_INFO, "Received signal %u", sig);
		switch (sig) {
		case SIGTERM:
		case SIGINT:
		case SIGHUP:
			log_printf(LOG_INFO, "Stopping");
			stop = 1;
			break;
		default:
			/* Unhandled signal. */
			log_printf(LOG_CRIT, "Reraising unhandled signal");
			sigprocmask(SIG_SETMASK, &oldset, NULL);
			raise(sig);
			sigprocmask(SIG_SETMASK, &runset, &oldset);
		}
	}

	stop_receiver(rs);
	stop_decoder(ds);
	stop_redis_writer(rws);
	if (bconf_get_int(bconf, "ack_fifo.enabled"))
		stop_ack_fifo(afs);

	bconf_free(&bconf);
	sigprocmask(SIG_SETMASK, &oldset, NULL);
	log_printf(LOG_DEBUG, "Exiting mainloop");
	return 0;
}

int
main(int argc, char *argv[]) {
	int nobos = 0;
	int foreground = 0;
	char *loglevel = NULL; 
	char *logtag = NULL;
	int ret = 0;
	int option_index = 0;
	int opt = 0;
	static struct option long_options[] = {
		{"nobos", 0, 0, 0},
		{"foreground", 0, 0, 0},
		{"config", 1, 0, 0},
		{"pidfile", 1, 0, 0},
		{"uid", 1, 0, 0},
		{"quick-start", 0, 0, 0},
		{"debug", 0, 0, 0},
		{"logtag", 1, 0, 0},
		{"help", 0, 0, 0},
		{"ack", 1, 0, 0},
		{0, 0, 0, 0}
	};

	while ((opt = getopt_long(argc, argv, "", long_options, &option_index)) != -1) {
		if (opt == 0) {
			switch (option_index) {
			case 0:
				nobos = 1;
				break;
			case 1:
				foreground = 1;
				break;
			case 2:

				bconf_add_data(&bconf_override, "config_file", optarg);
				break;
			case 3:
				set_pidfile(optarg);
				break;
			case 4:
				set_switchuid(optarg);
				break;
			case 5:
				set_quick_start(1);
				break;
			case 6:
				loglevel = xstrdup("debug");
				break;
			case 7:
				logtag = xstrdup(optarg);
				break;
			case 8:
			default: 
				usage(argv[0]);
				exit(1);
				break;
			case 9:
				bconf_add_data(&bconf_override, "ack_fifo.file", optarg);
				bconf_add_data(&bconf_override, "ack_fifo.enabled", "1");
				break;
			}
		}
	}

	if (!logtag)
		logtag = xstrdup("statpoints");
	if (!loglevel)
		loglevel = xstrdup("info");

	log_setup(logtag, loglevel);
	x_err_init_syslog(logtag, 0, LOG_LOCAL0, LOG_INFO);

	log_printf(LOG_INFO, "Starting statpoints");

	if (foreground) {
		if (nobos)
			ret = mainloop();
		else
			ret = bos(mainloop);
	} else {
		daemonify(nobos, mainloop);
	}
	
	log_printf(LOG_DEBUG, "Cleaning up");
	free(logtag);
	free(loglevel);
	return ret;
}
