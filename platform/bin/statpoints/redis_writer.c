#include <errno.h>
#include <pthread.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>
#include <time.h>

#include "error_functions.h"
#include "hiredis.h"
#include "logging.h"
#include "memalloc_functions.h"
#include "redis_writer.h"
#include "statpoints.h"

struct rsp_schema {
	const char *buck_name;
	int buck_size;
	int buck_granularity;
	int time_in_sec;
	int index;
};

struct rsp_event {
	time_t last_write;
	int total_enabled;
	int year_enabled;
	int month_enabled;
	int day_enabled;
	const char *event;
	struct rsp_schema *buckets;
	int num_buckets;
};

struct redis_writer_state {
	pthread_t thread;
	redisContext *c;
	struct bconf_node *bconf;
	int num_events;
	struct rsp_event *events;
	struct queue *queue;
};

static int redis_connect(struct redis_writer_state *);

static inline void
free_reply_object(redisReply *r) {
	if (r)
		freeReplyObject(r);
}

static int
init_redis_buckets(struct bconf_node *bconf, struct rsp_schema **buckets) {
	int num_buckets = bconf_count(bconf);
	struct rsp_schema *bs;

	if (!num_buckets)
		return 0;

	bs = *buckets = xcalloc(num_buckets, sizeof(**buckets));

	for (int i = 0; i < num_buckets; i++) {
		struct bconf_node *bucket_node = bconf_byindex(bconf, i);

		bs[i].buck_name = bconf_get_string(bucket_node, "buck_name");
		log_printf(LOG_DEBUG, "Setting up event bucket: %s", bs[i].buck_name);
		bs[i].buck_size = bconf_get_int(bucket_node, "buck_size");
		bs[i].buck_granularity = bconf_get_int(bucket_node, "granularity");
		bs[i].time_in_sec = bconf_get_int(bucket_node, "time_in_sec");
		bs[i].index = i;
	}

	return num_buckets;
}

static void
init_redis_timestamp(struct rsp_event *event, redisContext *c) {
	long reply = 0;
	redisReply *r;

	if (!event)
		return;

	r = redisCommand(c, "GET %s:last_ts", event->event);
	if (r && r->type == REDIS_REPLY_STRING) {
		reply = atol(r->str);
	}

	if (reply) {
		log_printf(LOG_INFO, "Restored timestamp for event %s from redis", event->event);
		event->last_write = reply;
	} else {
		log_printf(LOG_WARNING, "Failed to restore timestamp for event %s", event->event);
		event->last_write = 0;
	}

	free_reply_object(r);
}

static int
init_redis_events(struct bconf_node *bconf, struct rsp_event **events, struct redisContext *c) {
	int event_count = 0;
	struct bconf_node *redis_events = bconf_get(bconf, "statpoints.event");
	struct rsp_event *evs;
	*events = NULL;

	log_printf(LOG_DEBUG, "Setting up redis events");

	event_count = bconf_count(redis_events);
	if (event_count == 0) {
		log_printf(LOG_CRIT, "No events configured!");
		return -1;
	}

	evs = xcalloc(event_count, sizeof(*evs));

	for (int i = 0; i < event_count; i++) {
		struct bconf_node *event_node;

		/* Get event buckets */
		event_node = bconf_byindex(redis_events, i);
		evs[i].event = bconf_get_string(event_node, "name");
		log_printf(LOG_DEBUG, "Setting up redis event: %s", evs[i].event);
		evs[i].total_enabled = bconf_get_int(event_node, "total_enabled");
		evs[i].year_enabled = bconf_get_int(event_node, "year_enabled");
		evs[i].month_enabled = bconf_get_int(event_node, "month_enabled");
		evs[i].day_enabled = bconf_get_int(event_node, "day_enabled");

		if ((evs[i].num_buckets = init_redis_buckets(bconf_get(event_node, "buckets"), &evs[i].buckets)) == 0) {
			log_printf(LOG_CRIT, "Statpoints event %s has NO buckets defined!", evs[i].event);
		}

		init_redis_timestamp(&evs[i], c);
	}
	
	*events = evs;
	return event_count;
}

static void
free_redis_events(int num_events, struct rsp_event *events) {
	for (int i = 0; i < num_events; i++) {
		free(events[i].buckets);
	}
	free(events);
}

/*
	Calculate number of buckets that has passed since ts, depending on schema.

	A negative or zero granularity will default to granularity 1.

	The function will return negative values if ts has been surpassed based on the size of buckets
	i.e: Week bucket with size 7 now => Friday Week 2, while ts => Sunday Week 1, will return -2
*/
static int
buckets_since_ts(struct rsp_schema* schema, time_t current_ts, time_t ts, int* curr_bucket) {
        int ts_diff;

        int last_bucket = 0;
        int current_bucket = 0;
        int diff_bucket = 0;
	int bucket_size_in_sec = 0;

        if (schema->buck_granularity < 1) {
                schema->buck_granularity = 1;
        }
        
        ts_diff = current_ts - ts;

        last_bucket = (((ts / schema->time_in_sec) % schema->buck_size) / schema->buck_granularity) * schema->buck_granularity;
        current_bucket = (((current_ts / schema->time_in_sec) % schema->buck_size) / schema->buck_granularity) * schema->buck_granularity;

	if (curr_bucket)
		*curr_bucket = current_bucket;

        diff_bucket = (current_bucket - last_bucket);
       
       	/* An entire timespan for an event has been rolled */
        if (ts_diff > (schema->time_in_sec * schema->buck_size))
                return -1;

	/* Rolled back to the same bucket but not an entire timespan, remove */
	bucket_size_in_sec = (schema->time_in_sec * schema->buck_granularity);
	if (diff_bucket == 0 && ts_diff > bucket_size_in_sec) {
		return -1;
	}

	if (diff_bucket < 0) {
		diff_bucket += schema->buck_size;
	}

	return diff_bucket;
}

static int
buckets_delete(redisContext *c, struct rsp_schema* schema, const char* event, int buckets_diff, int current_bucket) {
	int last_bucket = 0;
	int i;
	int count;
	
	if (buckets_diff == 0) {
		return 0;
	} else if (buckets_diff < 0) {
		last_bucket = 0;
		current_bucket = schema->buck_size;
	} else {
		last_bucket = (schema->buck_size + current_bucket - buckets_diff) % schema->buck_size;
	}
	
	if (current_bucket > last_bucket) {
		count = (current_bucket - last_bucket) / schema->buck_granularity;
	} else {
		count = (current_bucket - last_bucket + schema->buck_size) / schema->buck_granularity;
	}

	for (i = 0; i < count; ++i) {
		int bucket_delete = (last_bucket + (i * schema->buck_granularity)) % schema->buck_size;
		free_reply_object(redisCommand(c, "DEL %s:%s:%d", event, schema->buck_name, bucket_delete));
	}
	
	return 0;
}

static int
buckets_increase(redisContext *c, struct rsp_schema* schema, const char* event, const char* id) {
	int i;
	int cmds = 0;

	for (i = 0 ; i < schema->buck_size ; i += schema->buck_granularity) {
		redisAppendCommand(c, "ZINCRBY %s:%s:%d 1 %s", event, schema->buck_name, i, id);
		cmds++;
	}
	return cmds;
}

static int
redis_time_bucket_delete(redisContext *c, struct rsp_event* event_schema, const char* id) {
	int b;
	redisReply *r;
	for (b = 0 ; b < event_schema->num_buckets ; b++) {
		struct rsp_schema *bucket_schema = &event_schema->buckets[b];
		for (int t = 0; t < bucket_schema->buck_size; t+=bucket_schema->buck_granularity) {
			r = redisCommand(c, "ZREM %s:%s:%d %s", event_schema->event, bucket_schema->buck_name, t, id);
			free_reply_object(r);
		}
	}

	if (event_schema->total_enabled == 1) {
		r = redisCommand(c, "ZREM %s:total %s", event_schema->event, id);
		free_reply_object(r);
	}
	if (event_schema->month_enabled == 1 || event_schema->day_enabled == 1 || event_schema->year_enabled == 1) {
		r = redisCommand(c, "SMEMBERS gckeys:%s:%s", event_schema->event, id);
		if (r->type == REDIS_REPLY_ARRAY) {
			for (unsigned int i = 0; i < r->elements; i++) {
				redisReply *rep = redisCommand(c,  "ZREM %s %s", r->element[i]->str, id);
				free_reply_object(rep);
			}
		}
		free_reply_object(r);
		
		r = redisCommand(c, "DEL gckeys:%s:%s", event_schema->event, id);
		free_reply_object(r);
	}

	return 0;
}

static int
redis_record_event(redisContext *c, struct rsp_event *event_schema, const char *event, const char *id) {
	time_t last_ts;
	time_t current_ts = time(NULL);
	int b;
	int cmds = 0;

	/* Get the last write timestamp */
	if (event_schema->last_write > 0) {
		last_ts = event_schema->last_write;
	} else {
		last_ts = time(NULL);
	}

	for (b = 0 ; b < event_schema->num_buckets ; b++) {
		int buckets_diff = 0;
		int curr_bucket = 0;
		struct rsp_schema *bucket_schema = &event_schema->buckets[b];

		buckets_diff = buckets_since_ts(bucket_schema, current_ts, last_ts, &curr_bucket);

		if (buckets_diff != 0) {
			buckets_delete(c, bucket_schema, event, buckets_diff, curr_bucket);
		} 

		cmds += buckets_increase(c, bucket_schema, event, id);
	}

	if (event_schema->total_enabled == 1) {
		redisAppendCommand(c, "ZINCRBY %s:total 1 %s", event, id);
		cmds++;
	}

	if (event_schema->month_enabled == 1 || event_schema->day_enabled == 1 || event_schema->year_enabled == 1) {
		time_t 		ctime;
		struct tm 	tm;
		char		datestr[20];

		ctime = time(NULL);
		localtime_r(&ctime, &tm);

		if (event_schema->year_enabled == 1) {
			strftime(datestr, 20, "%Y", &tm);
			redisAppendCommand(c, "ZINCRBY %s:%s 1 %s", event, datestr, id);
			cmds++;
			redisAppendCommand(c, "SADD gckeys:%s:%s %s:%s", event, id, event, datestr);
			cmds++;
		}
		if (event_schema->month_enabled == 1) {
			strftime(datestr, 20, "%Y-%m", &tm);
			redisAppendCommand(c, "ZINCRBY %s:%s 1 %s", event, datestr, id);
			cmds++;
			redisAppendCommand(c, "SADD gckeys:%s:%s %s:%s", event, id, event, datestr);
			cmds++;
		}
		if (event_schema->day_enabled == 1) {
			strftime(datestr, 20, "%F", &tm);
			redisAppendCommand(c, "ZINCRBY %s:%s 1 %s", event, datestr, id);
			cmds++;
			redisAppendCommand(c, "SADD gckeys:%s:%s %s:%s", event, id, event, datestr);
			cmds++;
		}
	}

	/* Save the last write timestamp */
	event_schema->last_write = current_ts;	
	redisAppendCommand(c, "SET %s:last_ts %d", event, (int)current_ts);
	cmds++;

	return cmds;
}

static void
process(struct redis_writer_state *s, struct statpoints_event *se) { 
	struct rsp_event *re = NULL;
	int cmds = 0;
	redisReply *r;

	if (s->c && s->c->err) {
		log_printf(LOG_CRIT, "Error occured while communicating with redis: %s", s->c->errstr ?: "unknown error");
		log_printf(LOG_INFO, "Will attempt to reconnect later");
		redisFree(s->c);
		s->c = NULL;
	}

	if (!s->c) {
		if (redis_connect(s) == -1) {
			log_printf(LOG_CRIT, "Failed to reconnect to redis. Discarding event.");
			return;
		} else {
			for (int i = 0; i < s->num_events; i++) {
				init_redis_timestamp(&s->events[i], s->c);
			}
		}
	}

	if (!se)
		return;

	/* Get the schema for the requested event */
	for (int i = 0; i < s->num_events; i++) {
		if (strcmp(s->events[i].event, se->event) == 0) {
			re = &s->events[i];
			break;
		}
	}

	if (!re) {
		log_printf(LOG_WARNING, "Could not find redis event: %s", se->event);
		return;
	}

	
	switch (se->type) {
	case ET_RECORD:
		cmds = redis_record_event(s->c, re, se->event, se->id);
		break;
	case ET_RESET:
	case ET_DELETE:
		redis_time_bucket_delete(s->c, re, se->id);
		break;
	default:
		log_printf(LOG_CRIT, "Redis writer got unknown event type! %d", se->type);
		break;
	}

	while (cmds-- > 0) {
		redisGetReply(s->c, (void **)&r);
		free_reply_object(r);
	}
}

static void
cleanup(void *data) {
	struct redis_writer_state *s = (struct redis_writer_state *)data;
	pthread_mutex_unlock(&s->queue->mutex);
}
	
static void *
redis_writer(void *data) {
	struct redis_writer_state *s = (struct redis_writer_state *)data;
	struct queue_entry *qe = NULL;
	struct timeval tv;
	struct timespec ts;
	
	log_printf(LOG_DEBUG, "Starting redis writer");
	set_thread_name(s->thread, "sp_redis_writer");

	pthread_cleanup_push(cleanup, s);

	pthread_mutex_lock(&s->queue->mutex);
	while(1) {
		pthread_setcancelstate(PTHREAD_CANCEL_DISABLE, NULL);
		gettimeofday(&tv, NULL);
		ts.tv_sec = tv.tv_sec + 1;
		ts.tv_nsec = (tv.tv_usec) * 1000;
		
		if (pthread_cond_timedwait(&s->queue->cond, &s->queue->mutex, &ts) == ETIMEDOUT) {
			redisReply *r;
			pthread_mutex_unlock(&s->queue->mutex);

			r = redisCommand(s->c, "PING");
			free_reply_object(r);

			pthread_mutex_lock(&s->queue->mutex);
		}

		while ((qe = TAILQ_FIRST(&s->queue->queue)) != NULL) {
			TAILQ_REMOVE(&s->queue->queue, qe, list);

			pthread_mutex_unlock(&s->queue->mutex);
			process(s, (struct statpoints_event *)qe->data);
			free(qe);
			pthread_mutex_lock(&s->queue->mutex);
		}
		pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, NULL);
		pthread_testcancel();
	}

	pthread_cleanup_pop(1);
	return NULL;
}

static int
redis_connect(struct redis_writer_state *rws) {
	const char *master;
	const char *redis_hostname;
	int redis_port;
	const char *redis_db;
	int timeout;

	if ((master = bconf_get_string(rws->bconf, "common.redisstat.master")) == NULL) {
		log_printf(LOG_CRIT, "No entry for common.redisstat.master found");
		return -1;
	}
	redis_hostname = bconf_vget_string(rws->bconf, "common.redisstat.host", master, "name", NULL);
	redis_port = bconf_vget_int(rws->bconf, "common.redisstat.host", master, "port", NULL);
	timeout = bconf_get_int(rws->bconf, "common.redisstat.timeout");
	redis_db = bconf_get_string(rws->bconf, "common.redisstat.db");

	rws->c = redisConnectWithTimeout(redis_hostname, redis_port, (struct timeval){timeout / 1000, (timeout % 1000) * 1000});

	if (rws->c == NULL || rws->c->err) {
		if (rws->c) {
			log_printf(LOG_CRIT, "Failed to connect to redis: %s", rws->c->errstr);
			redisFree(rws->c);
			rws->c = NULL;
		} else {
			log_printf(LOG_CRIT, "Failed to connect to redis: can't allocate redis context");
		}
		return -1;
	}
	if (redis_db) {
		redisReply *redis_rep = redisCommand(rws->c, "SELECT %s", redis_db);
		if (!redis_rep || redis_rep->type != REDIS_REPLY_STATUS) {
			log_printf(LOG_CRIT, "Failed to SELECT redis database %s", redis_db);
			return -1;
		} else {
			log_printf(LOG_DEBUG, "Successfully switched to database %s for last_ts restoration", redis_db);
		}
		free_reply_object(redis_rep);
	}
	return 0;
}

struct redis_writer_state *
start_redis_writer(struct bconf_node *bconf, struct queue *queue) {
	struct redis_writer_state *rws = xmalloc(sizeof *rws);

	rws->bconf = bconf;
	rws->queue = queue;
	
	if (redis_connect(rws) == -1) {
		xerrx(1, "Failed to connect to redis during startup");
	}
	rws->num_events = init_redis_events(bconf, &rws->events, rws->c);
	
	pthread_create(&rws->thread, NULL, redis_writer, rws);
	return rws;
}

void
stop_redis_writer(struct redis_writer_state *s) {
	pthread_cancel(s->thread);
	pthread_join(s->thread, NULL);
	free_redis_events(s->num_events, s->events);
	if (s->c)
		redisFree(s->c);	
	free(s);
	log_printf(LOG_DEBUG, "Redis writer stopped");
}
