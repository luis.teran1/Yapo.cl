#ifndef ACK_FIFO_H
#define ACK_FIFO_H
#include "bconf.h"
#include "statpoints.h"

struct ack_fifo_state;
struct ack_fifo_state *start_ack_fifo(struct bconf_node *bconf, struct queue *queue);
void stop_ack_fifo(struct ack_fifo_state *s);
#endif
