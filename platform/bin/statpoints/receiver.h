#ifndef RECEIVER_H
#define RECEIVER_H
#include "statpoints.h"

struct receiver_state;
struct receiver_state *start_receiver(struct bconf_node *bconf, struct queue *queue);
void stop_receiver(struct receiver_state *s);

#endif
