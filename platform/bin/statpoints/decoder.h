#ifndef DECODER_H
#define DECODER_H
#include "bconf.h"
#include "statpoints.h"

struct decoder_state;
struct decoder_state *start_decoder(struct bconf_node *bconf, struct queue *in_queue, struct queue *out_queue, struct queue *ack_fifo_queue);
void stop_decoder(struct decoder_state *s);

#endif
