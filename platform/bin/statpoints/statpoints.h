#ifndef STATPOINTS_H
#define STATPOINTS_H
#include <pthread.h>
#include "queue.h"

enum statpoints_event_type { 
	ET_RECORD = 0,
	ET_RESET,
	ET_DELETE
};

struct statpoints_event {
	enum statpoints_event_type type;
	char *event;
	char *id;
};

struct input_event {
	char *data;
	int len;
};

struct queue_entry {
	void *data;
	TAILQ_ENTRY(queue_entry) list;
};

struct queue {
	pthread_mutex_t mutex;
	pthread_cond_t cond;
	TAILQ_HEAD(, queue_entry) queue;
};

int set_thread_name(pthread_t thread, const char *name);

#endif 
