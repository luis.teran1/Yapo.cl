#include <errno.h>
#include <pthread.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>

#include "decoder.h"
#include "logging.h"
#include "memalloc_functions.h"
#include "statpoints.h"

struct decoder_state {
	pthread_t thread;
	struct bconf_node *bconf;
	struct queue *in_queue;
	struct queue *out_queue;
	struct queue *ack_fifo_queue;
	int ack_fifo_enabled;
};

static size_t
statpoints_queue_entry_create(struct queue_entry **entry, const char *event, const char *id, int reset, int delete) {
	struct queue_entry *e;
	struct statpoints_event *sev = NULL;
	size_t total_sz = 0;

	log_printf(LOG_DEBUG, "Creating event %s with id %s, reset: %d, delete %d", event, id, reset, delete);
	if (event && id) {
		size_t id_sz = strlen(id) + 1;	
		size_t ev_sz = strlen(event) + 1;	
		total_sz = sizeof *e + sizeof *sev + id_sz + ev_sz;

		if (total_sz) {
			e = xmalloc(total_sz);
			sev = (struct statpoints_event *)(e + 1);	
			e->data = sev;

			sev->event = (char *)(sev + 1);
			sev->id = (char *)(sev->event + ev_sz);

			if (reset) {
				sev->type = ET_RESET;
			} else if (delete) { 
				sev->type = ET_DELETE;
			} else {
				sev->type = ET_RECORD;
			}
			memcpy(sev->event, event, ev_sz);
			memcpy(sev->id, id, id_sz);

			*entry = e;
		} else
			*entry = NULL;
	}
	
	return total_sz;
}

static size_t
parse_line(struct queue_entry **entry, char *line) {
	size_t qe_sz; 
	char *event;
	char *id;
	char *value;
	int delete;
	int reset;
	char *next_word;

	log_printf(LOG_DEBUG, "Parsing query '%s'", line);

	event = id = NULL;
	reset = delete = 0;
	/* line is "event:foo id:bar */
	for (; line; line = next_word) {
		next_word = strchr(line, ' ');
		if (next_word)
			*next_word++ = '\0';

		value = strchr(line, ':');
		if (value)
			*value++ = '\0';

		if (strcmp(line, "event") == 0)
			event = value;
		else if (strcmp(line, "id") == 0)
			id = value;
		else if (strcmp(line, "delete") == 0)
			delete = 1;
		else if (strcmp(line, "reset") == 0)
			reset = 1;
	}

	qe_sz = statpoints_queue_entry_create(entry, event, id, reset, delete);
	if (!qe_sz) { 
		log_printf(LOG_WARNING, "Failed to parse event");
	}
	return qe_sz;
}

static void
process(struct decoder_state *s, struct input_event *e) {
	char *line;
	char *next_line;
	struct queue_entry *qe;
	size_t qe_sz;

	if (!e) 
		return;

	log_printf(LOG_DEBUG, "Decoder processing event: %s", e->data);

	for (line = e->data; line; line = next_line) {
		next_line = strchr(line, '\n');
		if (next_line)
			*next_line++ = '\0';

		/* Skip empty lines. */
		if (!*line)
			continue;

		if((qe_sz = parse_line(&qe, line)) > 0 && qe != NULL) {
			if (s->ack_fifo_enabled) {
				struct queue_entry *aqe = xmalloc(qe_sz);
				memcpy(aqe, qe, qe_sz);

				log_printf(LOG_DEBUG, "Pushing event to ack_fifo");
				pthread_mutex_lock(&s->ack_fifo_queue->mutex);
				TAILQ_INSERT_TAIL(&s->ack_fifo_queue->queue, aqe, list);
				pthread_mutex_unlock(&s->ack_fifo_queue->mutex);
				pthread_cond_signal(&s->ack_fifo_queue->cond);
			}
			log_printf(LOG_DEBUG, "Pushing event to redis writer");
			pthread_mutex_lock(&s->out_queue->mutex);
			TAILQ_INSERT_TAIL(&s->out_queue->queue, qe, list);
			pthread_mutex_unlock(&s->out_queue->mutex);
			pthread_cond_signal(&s->out_queue->cond);

		}
	}
}

static void
cleanup(void *data) {
	struct decoder_state *s = (struct decoder_state *)data;
	pthread_mutex_unlock(&s->in_queue->mutex);
}

static void *
decoder(void *data) {
	struct decoder_state *s = (struct decoder_state *)data;
	struct queue_entry *qe = NULL;
	struct timeval tv;
	struct timespec ts;
	
	log_printf(LOG_DEBUG, "Starting decoder");
	set_thread_name(s->thread, "sp_decoder");

	pthread_cleanup_push(cleanup, s);

	pthread_mutex_lock(&s->in_queue->mutex);
	while(1) {
		pthread_setcancelstate(PTHREAD_CANCEL_DISABLE, NULL);
		gettimeofday(&tv, NULL);
		ts.tv_sec = tv.tv_sec + 1;
		ts.tv_nsec = tv.tv_usec * 1000;
		
		pthread_cond_timedwait(&s->in_queue->cond, &s->in_queue->mutex, &ts);
		
		/*
		 * Another way of doing this would be to process in batches by
		 * using double in queues and swap between them to process
		 * batches. 
		 * Only process one event at a time currently. 
		 */
		while ((qe = TAILQ_FIRST(&s->in_queue->queue)) != NULL) {
			TAILQ_REMOVE(&s->in_queue->queue, qe, list);

			pthread_mutex_unlock(&s->in_queue->mutex);
			process(s, (struct input_event *)qe->data);
			free(qe);
			pthread_mutex_lock(&s->in_queue->mutex);
		}
		pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, NULL);
		pthread_testcancel();
	}

	pthread_cleanup_pop(1);
	return NULL;
}

struct decoder_state *
start_decoder(struct bconf_node *bconf, struct queue *in_queue, struct queue *out_queue, struct queue *ack_fifo_queue) {
	struct decoder_state *s = xmalloc(sizeof *s);
	s->bconf = bconf;

	s->in_queue = in_queue; 
	s->out_queue = out_queue; 
	s->ack_fifo_queue = ack_fifo_queue; 
	s->ack_fifo_enabled = bconf_get_int(s->bconf, "ack_fifo.enabled");

	pthread_create(&s->thread, NULL, decoder, s);
	return s;
}

void
stop_decoder(struct decoder_state *s) {
	pthread_cancel(s->thread);
	pthread_join(s->thread, NULL);
	free(s);
	log_printf(LOG_DEBUG, "Decoder stopped");
}
