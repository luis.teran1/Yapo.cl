#include <errno.h>
#include <fcntl.h>
#include <pthread.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/types.h>
#include <time.h>
#include <unistd.h>

#include "ack_fifo.h"
#include "error_functions.h"
#include "memalloc_functions.h"
#include "logging.h"
#include "statpoints.h"

struct ack_fifo_state {
	pthread_t thread;
	struct bconf_node *bconf;
	struct queue *queue;
	int fifo;
	const char *file;
};


static void
process(struct ack_fifo_state *s, struct statpoints_event *se) { 
	if (!se)
		return;
	log_printf(LOG_DEBUG, "Writing ACK to fifo");
	UNUSED_RESULT(write(s->fifo, "ACK\n" , 4));
}

static void
cleanup(void *data) {
	struct ack_fifo_state *s = (struct ack_fifo_state *)data;
	pthread_mutex_unlock(&s->queue->mutex);
}

static void *
ack_fifo(void *data) {
	struct ack_fifo_state *s = (struct ack_fifo_state *)data;
	struct queue_entry *qe = NULL;
	struct timeval tv;
	struct timespec ts;
	
	log_printf(LOG_DEBUG, "Starting ack_fifo");
	set_thread_name(s->thread, "sp_ack_fifo");

	pthread_cleanup_push(cleanup, s);

	pthread_mutex_lock(&s->queue->mutex);
	while(1) {
		pthread_setcancelstate(PTHREAD_CANCEL_DISABLE, NULL);
		gettimeofday(&tv, NULL);
		ts.tv_sec = tv.tv_sec + 1;
		ts.tv_nsec = tv.tv_usec * 1000;
		
		pthread_cond_timedwait(&s->queue->cond, &s->queue->mutex, &ts);
		while ((qe = TAILQ_FIRST(&s->queue->queue)) != NULL) {
			log_printf(LOG_DEBUG, "Got event");
			TAILQ_REMOVE(&s->queue->queue, qe, list);

			pthread_mutex_unlock(&s->queue->mutex);
			process(s, (struct statpoints_event *)qe->data);
			free(qe);
			pthread_mutex_lock(&s->queue->mutex);
		}
		pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, NULL);
		pthread_testcancel();
	}

	pthread_cleanup_pop(1); 
	return NULL;
}

struct ack_fifo_state *
start_ack_fifo(struct bconf_node *bconf, struct queue *queue) {
	struct ack_fifo_state *afs = xmalloc(sizeof *afs);

	afs->bconf = bconf;
	afs->queue = queue;
	afs->file = bconf_get_string(afs->bconf, "ack_fifo.file");
	
	if (access(afs->file, F_OK) == 0 && unlink(afs->file) != 0)
		xerr(1, "Failed to remove old ack-fifo");

	if (mkfifo(afs->file, 0700) != 0)
		xerr(1, "Failed to create ack-fifo (%s)", afs->file);

	if ((afs->fifo = open(afs->file, O_RDWR|O_NONBLOCK)) == -1)
		xerr(1, "Failed to open ack-fifo (%s)", afs->file);

	pthread_create(&afs->thread, NULL, ack_fifo, afs);
	return afs;
}

void
stop_ack_fifo(struct ack_fifo_state *s) {
	pthread_cancel(s->thread);
	pthread_join(s->thread, NULL);

	close(s->fifo);
	unlink(s->file);
	free(s);

	log_printf(LOG_DEBUG, "ack_fifo stopped");
}
