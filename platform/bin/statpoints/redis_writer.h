#ifndef REDIS_WRITER_H
#define REDIS_WRITER_H
#include "bconf.h"
#include "statpoints.h"

struct redis_writer_state;
struct redis_writer_state *start_redis_writer(struct bconf_node *bconf, struct queue *queue);
void stop_redis_writer(struct redis_writer_state *rws); 
#endif
