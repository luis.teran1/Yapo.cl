#include <errno.h>
#include <fcntl.h>
#include <netdb.h>
#include <poll.h>
#include <pthread.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>

#include "bconf.h"
#include "error_functions.h"
#include "logging.h"
#include "memalloc_functions.h"
#include "receiver.h"
#include "statpoints.h"
#include "string_functions.h"

struct receiver_state {
	pthread_t thread;
	struct bconf_node *bconf;
	struct queue *queue;
};

static int
listen_port(struct pollfd *fds, int maxfds, const char *port) {
	const struct addrinfo hints = { .ai_flags = AI_PASSIVE | AI_ADDRCONFIG, .ai_socktype = SOCK_DGRAM };
	struct addrinfo *res = NULL, *curr;
	int nfds;
	int r;

	if ((r = getaddrinfo(NULL, port, &hints, &res)) || !res)
		xerrx(1, "getaddrinfo: %s (port:%s)", gai_strerror(r), port);

	nfds = 0;
	for (curr = res ; curr && nfds < maxfds ; curr = curr->ai_next) {
		int sock;
		int val;
		int flags;

		if ((sock = socket(curr->ai_family, curr->ai_socktype, curr->ai_protocol)) < 0) {
			xwarn("socket");
			continue;
		}

		flags = fcntl(sock, F_GETFL, 0);
		fcntl(sock, F_SETFL, flags | O_NONBLOCK);

		val = 1;
		if (setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &val, sizeof(val)) < 0)
			xerr(1, "setsockopt(SO_REUSEADDR)");
		if (curr->ai_family == AF_INET6 && setsockopt(sock, IPPROTO_IPV6, IPV6_V6ONLY, &val, sizeof(val)) < 0)
			xerr(1, "setsockopt(IPV6_V6ONLY)");

		if (bind(sock, curr->ai_addr, curr->ai_addrlen))
			xerr(1, "bind");


		fds[nfds].fd = sock;
		fds[nfds].events = POLLIN;
		nfds++;
	}

	freeaddrinfo(res);
	return nfds;
}

static inline void
queue_message(struct receiver_state *s, char *buf, int len) {	
	struct queue_entry *e;
	struct input_event *ie;
	
	if (len <= 0)
		return;

	e = xmalloc(sizeof *e + sizeof *ie + len + 1);
	ie = (void *)(e + 1);
	ie->data = (char *)(ie + 1);
	memcpy(ie->data, buf, len);
	ie->data[len] = '\0';
	ie->len = len;
	e->data = ie;

	log_printf(LOG_DEBUG, "Receiver queueing message: %s", ie->data);
	pthread_mutex_lock(&s->queue->mutex);
	TAILQ_INSERT_TAIL(&s->queue->queue, e, list);
	pthread_mutex_unlock(&s->queue->mutex);
	pthread_cond_signal(&s->queue->cond);
}

static void *
receiver(void *data) {
	struct receiver_state *s = (struct receiver_state *)data;
	int nfds;
	struct pollfd fds[10];
	int ndata;
	char buf[4096];

	log_printf(LOG_DEBUG, "Receiver started");
	set_thread_name(s->thread, "sp_receiver");
	nfds = listen_port(fds, sizeof(fds)/sizeof(fds[0]), bconf_get_string(s->bconf, "common.statpoints.udp_port"));
	while(1) {
		pthread_setcancelstate(PTHREAD_CANCEL_DISABLE, NULL);
		if (poll(fds, nfds, 1000) > 0) {
			for (int i = 0; i < nfds; i++) {
				if (fds[i].revents & POLLIN) {
					ndata = recvfrom(fds[i].fd, buf, sizeof(buf), 0, NULL, NULL); 
					if (ndata == -1 && errno != EWOULDBLOCK && errno != EAGAIN) {
							log_printf(LOG_CRIT, "Failed to read: %s", xstrerror(errno));
					} else {
						queue_message(s, buf, ndata);
					}
				}
			}
		}
		pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, NULL);
		pthread_testcancel();
	}
	return NULL;
}

struct receiver_state *
start_receiver(struct bconf_node *bconf, struct queue *queue) {
	struct receiver_state *rs = xmalloc(sizeof *rs);
	log_printf(LOG_DEBUG, "Starting Receiver");

	rs->bconf = bconf;
	rs->queue = queue; 

	pthread_create(&rs->thread, NULL, receiver, rs);
	return rs;
}

void
stop_receiver(struct receiver_state *s) {
	pthread_cancel(s->thread);
	pthread_join(s->thread, NULL);
	free(s);
	log_printf(LOG_DEBUG, "Receiver stopped");
}
