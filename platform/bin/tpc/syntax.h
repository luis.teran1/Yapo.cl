#ifndef SYNTAX_H
#define SYNTAX_H

#include <typeinfo>
#include <deque>
#include <set>
#include <vector>
#include <string>
#include <sstream>
#include <queue>
#include <functional>

#include <stdio.h>
#include <assert.h>
#include <pcre.h>

#include "macros.h"

class SyntaxNode;
class BlockSyntaxNode;
class NodeSyntaxNode;
class ParamsSyntaxNode;
class FilterSyntaxNode;

const std::string get_static_name(const char *prefix, const void *id);
const std::string &get_static_name2(const std::string &vname);

class post_process_call
{
public:
	int prio;
	std::function<void()> fun;

	post_process_call(int p, decltype(fun) f)
		: prio(p), fun(f)
	{
	}

	bool operator < (const post_process_call &r) const {
		return prio < r.prio;
	}
	bool operator > (const post_process_call &r) const {
		return prio > r.prio;
	}
};
typedef std::priority_queue<post_process_call, std::vector<post_process_call>, std::greater<post_process_call>> pp_queue;

class ICEException : public std::exception {
public:
	SyntaxNode *node;
	char msg[10240];

	ICEException(SyntaxNode *node, const char *fmt, ...);

	virtual const char *what() const throw() { return msg; }
};

#define ICERR(sn, ...) do { if (sn) sn->dump(0); throw ICEException(sn, __VA_ARGS__); } while (0)

class ParseException : public std::exception {
public:
	SyntaxNode *node;
	char msg[10240];

	ParseException(SyntaxNode *node, const char *fmt, ...);

	virtual const char *what() const throw() { return msg; }
};

#define PERR(sn, ...) do { throw ParseException(sn, __VA_ARGS__); } while (0)

class local_var {
public:
	std::string decl;
	std::string cleanup;

	std::string get_name(void)
	{
		return get_static_name("l_", this);
	}
};

static const char * const sn_ret_name[] = {
	"SNR_NONE",
	"SNR_STR",
	"SNR_INT",
	"SNR_OUT",
	"SNR_BOOL",
	"SNR_VAR",
	"SNR_REGEX",
	"SNR_NODE",
};

class SyntaxNode {
public:
	BlockSyntaxNode *block;
	int sn_lineno;
	enum sn_ret {			/* Return type of the code in the node */
		SNR_NONE,		/* Do not transform this code. */
		SNR_STR,		/* (char *) */
		SNR_INT,		/* (int) */
		SNR_OUT,		/* (void), but can call output functions. */
		SNR_BOOL,		/* boolean */
		SNR_VAR,		/* tpvar* */
		SNR_REGEX,		/* cached_regex* */
		SNR_NODE,               /* struct bpapi_vtree_chain * */
	} sn_ret;

	SyntaxNode(BlockSyntaxNode *block, SyntaxNode *parent, enum sn_ret sn_ret, int sn_lineno) {
		if (block == NULL && parent != NULL)
			block = parent->block;
		this->block = block;
		this->sn_ret = sn_ret;
		if (sn_lineno == 0 && parent != NULL)
			sn_lineno = parent->sn_lineno;
		this->sn_lineno = sn_lineno;
		if (parent)
			parent->add(this);
	}

	virtual ~SyntaxNode(void) { }

	virtual bool isempty(void) = 0;

	virtual void add(SyntaxNode *child) {
		ICERR(this, "Add on wrong type (%s)", typeid(*this).name());
	}

	virtual void convert_node(void) {
	}

	virtual SyntaxNode *convert_to_str(void);
	virtual SyntaxNode *convert_to_int(void);
	virtual SyntaxNode *convert_to_bool(void);
	virtual SyntaxNode *convert_to_var(void);

	virtual SyntaxNode *ifdef(void);
	virtual SyntaxNode *getvar(void);
	virtual SyntaxNode *getlist(SyntaxNode *index);
	virtual SyntaxNode *fetchlist(const std::string &var);
	virtual SyntaxNode *listlen(void);
	virtual SyntaxNode *globlen(void);

	virtual SyntaxNode *reduce(void) {
		ICERR(this, "ICE: reduce (%s)", typeid(*this).name());
	}

	virtual SyntaxNode *apply(SyntaxNode *(*cb)(SyntaxNode *, void *), void *v) {
		bool transformable = this->is_transformable();
		SyntaxNode *ret = cb(this, v);

		if (this != ret && !transformable)
			ICERR(this, "transformation on non-transformable node");

		return ret;
	}

	/* Compilation steps through apply(). */

	/* Resolve loops, branch counters and local variables. */
	virtual void resolve_node(pp_queue &ppq) {}
	virtual void verify_loop_node(BlockSyntaxNode *parent_of) {}
	/* Change the parent block of a node. */
	virtual void reparent_node(BlockSyntaxNode *newblock, BlockSyntaxNode *oblock) {
		/* If we pass into a different block when doing the apply don't reparent the children of that block. */
		if (this->block == oblock)
			this->block = newblock;
	}
	/* Collapse redundant container blocks and nodes. */
	virtual SyntaxNode *collapse_node(void) {
		return this;
	}

	/* Detect and replace optimizable nodes */
	virtual SyntaxNode *optimize_node(void) {
		return this;
	}

	/* bind local variables */
	virtual void bind_locals_node(void) {
	}

	/* Wrappers for apply for the compilation steps. */
	void reparent(BlockSyntaxNode *);
	void resolve(pp_queue &ppq);
	void verify_loop(BlockSyntaxNode *parent_of);
	SyntaxNode *collapse(int *);
	SyntaxNode *optimize(void);
	void bind_locals(void);
	void convert(void);


	virtual std::string fold(int *indent) {
		ICERR(this, "ICE: non-NODE in fold (%s)", typeid(*this).name());
	}

	virtual std::deque<SyntaxNode*>::iterator flatten(std::deque<SyntaxNode*> *deque, std::deque<SyntaxNode*>::iterator pos) {
		return pos;
	}
	virtual int ws_type(void) {
		return 0;
	}

	virtual void dump(int indent) {
		fprintf(stderr, "%*s(%s%s) %s:%p (b:%p)\n", indent * 3, "", this->is_constant() ? "const " : "", sn_ret_name[sn_ret], typeid(*this).name(), this, this->block);
	}

	/*
	 * Having those operators do anything but fail is too dangerous.
	 */
	virtual bool operator==(const SyntaxNode &rhs) {
		ICERR(this, "ICE: operator== on virutal node");
	}
	virtual bool operator<(const SyntaxNode &rhs) {
		ICERR(this, "ICE: operator< on virtual node");
	}

	virtual bool is_constant(void) {
		return false;
	}

	/*
	 * Returns false for nodes that cannot be transformed through an apply.
	 * Some nodes accessed through apply have a more specific type in their container
	 * than just SyntaxNode. Since their container can't accept a more generic type, those
	 * nodes can't ever change indentity through an apply operation. This is mostly a debug
	 * tool so that we can catch violation of this assumption.
	 */
	virtual bool is_transformable(void) {
		return true;
	}

	/*
	 * Check for blocks to see if their content is trivially convertible to an SNR_VAR.
	 * All SNR_INT and SNR_STR are. SNR_OUT can be only in some cases in OutSyntaxNode.
	 * SNR_VAR can be and need to be checked case by case (blocks aren't).
	 */
	virtual bool is_trivial_var(void) {
		if (sn_ret == SNR_STR || sn_ret == SNR_INT)
			return true;
		return false;
	}

	/*
	 * yacc will call this to flag that the parameter value is not used but rather
	 * part of the template output.
	 */
	virtual void parameter_output(void) {
	}
};

template <class T> struct ptrclasscomp {
	bool operator() (T * const &lhs, T * const &rhs) const {
		return *lhs < *rhs;
	}
};


class NodeSyntaxNode : public SyntaxNode {
public:
	std::deque<SyntaxNode*> nodes;

	NodeSyntaxNode(BlockSyntaxNode *block, SyntaxNode *parent, enum sn_ret sn_ret, int sn_lineno)
			: SyntaxNode(block, parent, sn_ret, sn_lineno) {}

	virtual ~NodeSyntaxNode(void) {
		std::deque<SyntaxNode*>::iterator n;

		for (n = nodes.begin() ; n != nodes.end() ; n++)
			delete *n;
	}

	bool isempty(void) {
		return nodes.empty();
	}

	void add(SyntaxNode *child) {
		assert(child);
		nodes.push_back(child);
	}

	virtual SyntaxNode *reduce(void);
	virtual SyntaxNode *collapse_node(void);
	SyntaxNode *apply(SyntaxNode *(*cb)(SyntaxNode *, void *), void *);
	virtual std::string fold(int *indent);
	virtual std::string fold_node(SyntaxNode *sn, int *indent);

	std::deque<SyntaxNode*>::iterator flatten(std::deque<SyntaxNode*> *deque, std::deque<SyntaxNode*>::iterator pos);

	void dump(int indent) {
		fprintf(stderr, "%*s(", indent * 3, "");
		SyntaxNode::dump(0);
		std::deque<SyntaxNode*>::iterator n;
		for (n = nodes.begin() ; n != nodes.end() ; n++)
			(*n)->dump(indent + 1);
		fprintf(stderr, "%*s)\n", indent * 3, "");
	}

	bool operator==(const SyntaxNode &rhs) {
		if (typeid(rhs) != typeid(*this) || sn_ret != rhs.sn_ret)
			return false;
		const NodeSyntaxNode &r = (const NodeSyntaxNode&)rhs;
		if (nodes.size() != r.nodes.size())
			return false;
		std::deque<SyntaxNode*>::const_iterator ti, ri;

		for (ti = nodes.begin(), ri = r.nodes.begin() ; ti != nodes.end() ; ti++, ri++) {
			if (!(**ti == **ri)) /* argh! */
				return false;
		}
		return true;
	}

	bool operator<(const SyntaxNode &rhs) {
		if (typeid(rhs) != typeid(*this))
			return typeid(rhs).before(typeid(*this));
		if (sn_ret != rhs.sn_ret)
			return sn_ret < rhs.sn_ret;
		const NodeSyntaxNode &r = (const NodeSyntaxNode&)rhs;
		if (nodes.size() != r.nodes.size())
			return nodes.size() < r.nodes.size();
		std::deque<SyntaxNode*>::const_iterator ti, ri;

		for (ti = nodes.begin(), ri = r.nodes.begin() ; ti != nodes.end() ; ti++, ri++) {
			if (!(**ti == **ri)) /* argh! */
				return **ti < **ri;
		}
		return false;
	}

	virtual bool is_constant(void) {
		for (std::deque<SyntaxNode*>::const_iterator i = nodes.begin() ; i != nodes.end() ; i++) {
			if (!(*i)->is_constant())
				return false;
		}
		return true;
	}
};

class OutSyntaxNode : public NodeSyntaxNode {
public:
	bool reduced;

	OutSyntaxNode(BlockSyntaxNode *block, int sn_lineno)
		: NodeSyntaxNode(block, NULL, SNR_OUT, sn_lineno) {
		reduced = false;
	}

	virtual std::string fold_node(SyntaxNode *sn, int *indent);
	virtual SyntaxNode *reduce(void);
	SyntaxNode *convert_to_var(void);
	SyntaxNode *collapse_node(void);
	bool is_trivial_var(void);
};

class VararrSyntaxNode : public NodeSyntaxNode {
public:
	VararrSyntaxNode(BlockSyntaxNode *block, int sn_lineno)
		: NodeSyntaxNode(block, NULL, SNR_VAR, sn_lineno) {}

	void convert_node(void);
	SyntaxNode *reduce();
	bool is_trivial_var(void) {
		return true;
	}
};

#if 0
/*
 * Generic wrapper for other syntax nodes to generate SNR_VAR
 * output.
 */
class VarSyntaxNode : public SyntaxNode {
private:
	SyntaxNode *node;
public:
	VarSyntaxNode(SyntaxNode *n) :
		: SyntaxNode(n->block, n->parent, SNR_VAR, n->sn_lineno) {}
	SyntaxNode *reduce();
	SyntaxNode *apply(SyntaxNode *(*cb)(SyntaxNode *, void *), void *v) {
		node = node->apply(cb, v);
		return SyntaxNode::apply(cb, v);
	}
	bool operator==(const SyntaxNode &rhs) {
		if (typeid(rhs) != typeid(*this) || sn_ret != rhs.sn_ret)
			return false;
		const VarSyntaxNode &r = (const VarSyntaxNode&)rhs;
		return *this->node == *r.node;
	}
	bool operator<(const SyntaxNode &rhs) {
		if (typeid(rhs) != typeid(*this))
			return typeid(rhs).before(typeid(*this));
		if (sn_ret != rhs.sn_ret)
			return sn_ret < rhs.sn_ret;
		const VarSyntaxNode &r = (const VarSyntaxNode&)rhs;
		return *this->node < *r.node;
	}

}
#endif

class StringSyntaxNode : public SyntaxNode {
public:
	std::string sn_str;
	static pcre *compiled_id_regex;

	StringSyntaxNode(BlockSyntaxNode *block, SyntaxNode *parent, int sn_lineno, const std::string &str)
		: SyntaxNode(block, parent, SNR_STR, sn_lineno), sn_str(str) { }

	static pcre* init_regex(const char* regex) {
		const char *err;
		int erro;
		return pcre_compile(regex, PCRE_DOTALL | PCRE_NO_AUTO_CAPTURE, &err, &erro, NULL);
	}

	static void deinit_regex() {
		pcre_free(compiled_id_regex);
	}

	bool isempty(void) {
		return false;
	}

	bool isidentifier() {
		bool is_id = false;

		if (compiled_id_regex) {
			is_id = pcre_exec(compiled_id_regex, NULL, sn_str.c_str(), sn_str.length(), 0, 0, NULL, 0) >= 0;
		} else {
			ICERR(this, "Regular expression for ID not available. Internal Compiler Error.");
		}
		return is_id;
	}

	void dump(int indent) {
		fprintf(stderr, "%*s(const %s) %s:%p : \"%s\"\n", indent * 3, "", sn_ret_name[sn_ret], typeid(*this).name(), this, sn_str.c_str());
	}

	SyntaxNode *convert_to_int(void);
	SyntaxNode *convert_to_str(void);
	SyntaxNode *ifdef(void);
	SyntaxNode *getvar(void);
	SyntaxNode *getlist(SyntaxNode *index);
	SyntaxNode *fetchlist(const std::string &var);
	SyntaxNode *listlen(void);

	SyntaxNode *reduce(void);
	std::string fold(int *indent);

	bool operator==(const SyntaxNode &rhs) {
		if (typeid(rhs) != typeid(*this) || sn_ret != rhs.sn_ret)
			return false;
		const StringSyntaxNode &r = (const StringSyntaxNode&)rhs;
		return sn_str == r.sn_str;
	}

	bool operator<(const SyntaxNode &rhs) {
		if (typeid(rhs) != typeid(*this))
			return typeid(rhs).before(typeid(*this));
		if (sn_ret != rhs.sn_ret)
			return sn_ret < rhs.sn_ret;
		const StringSyntaxNode &r = (const StringSyntaxNode&)rhs;
		return sn_str < r.sn_str;
	}

	bool is_constant(void) {
		return true;
	}

	std::string escape(void);
};

class StrWhiteSyntaxNode : public SyntaxNode {
public:
	StrWhiteSyntaxNode(BlockSyntaxNode *block, SyntaxNode *parent, int sn_lineno)
		: SyntaxNode(block, parent, SNR_STR, sn_lineno) {}

	bool isempty(void) {
		return false;
	}

	int ws_type(void) {
		return 1;
	}

	SyntaxNode *reduce(void);
	bool operator==(const SyntaxNode &rhs) {
		return typeid(rhs) == typeid(*this);
	}
	bool operator<(const SyntaxNode &rhs) {
		return typeid(rhs).before(typeid(*this));
	}

	bool is_constant(void) {
		return true;
	}
};

class StrNlSyntaxNode : public SyntaxNode {
public:
	StrNlSyntaxNode(BlockSyntaxNode *block, SyntaxNode *parent, int sn_lineno)
		: SyntaxNode(block, parent, SNR_STR, sn_lineno) {}

	virtual bool isempty(void) {
		return false;
	}

	virtual int ws_type(void) {
		return 2;
	}

	virtual SyntaxNode *reduce(void);
	bool operator==(const SyntaxNode &rhs) {
		return typeid(rhs) == typeid(*this);
	}
	bool operator<(const SyntaxNode &rhs) {
		return typeid(rhs).before(typeid(*this));
	}

	bool is_constant(void) {
		return true;
	}
};

class IntegerSyntaxNode : public SyntaxNode {
public:
	int val;

	IntegerSyntaxNode(BlockSyntaxNode *block, SyntaxNode *parent, int sn_lineno, int val)
		: SyntaxNode(block, parent, SNR_INT, sn_lineno), val(val) {}

	virtual bool isempty(void) {
		return false;
	}

	SyntaxNode *reduce(void);

	std::string fold(int *indent);
	bool operator==(const SyntaxNode &rhs) {
		if (typeid(rhs) != typeid(*this) || sn_ret != rhs.sn_ret)
			return false;
		const IntegerSyntaxNode &r = (const IntegerSyntaxNode&)rhs;
		return val == r.val;
	}

	bool operator<(const SyntaxNode &rhs) {
		if (typeid(rhs) != typeid(*this))
			return typeid(rhs).before(typeid(*this));
		if (sn_ret != rhs.sn_ret)
			return sn_ret < rhs.sn_ret;
		const IntegerSyntaxNode &r = (const IntegerSyntaxNode&)rhs;
		return val < r.val;
	}

	bool is_constant(void) {
		return true;
	}
};

class CodeSyntaxNode : public SyntaxNode {
public:
	std::string sn_code;
	enum sn_indent {
		INLINE,
		NONE,
		UP,
		DOWN,
		DOWNUP,
		DOWNNONL,
		NONL,
		INLINEUP,
		INLINENL
	} sn_indent;
	bool constant;

	/*
	 * Notice that block and line number information is pretty useless (and possibly misleading) for code nodes.
	 * So instead of tracking that (potentially wrong) information, we just let it be NULL/0.
	 */
	static void strcode(SyntaxNode *parent, const std::string &strcode) NONNULL(1) {
		parent->add(new CodeSyntaxNode(SNR_STR, strcode));
	}
	static void intcode(SyntaxNode *parent, const std::string &intcode) NONNULL(1) {
		parent->add(new CodeSyntaxNode(SNR_INT, intcode));
	}
	static void code(SyntaxNode *parent, const std::string &code, enum sn_indent indent = NONE, bool isc = false) NONNULL(1) {
		parent->add(new CodeSyntaxNode(SNR_NONE, code, indent, isc));
	}

	CodeSyntaxNode(enum sn_ret sn_ret, const std::string &sn_code, enum sn_indent sn_indent = INLINE, bool isc = false)
		: SyntaxNode(NULL, NULL, sn_ret, -1), sn_code(sn_code), sn_indent(sn_indent), constant(isc) {}

	virtual bool isempty(void) {
		return sn_code == "";
	}

	void dump(int indent) {
		fprintf(stderr, "%*s%s%s:%p : %s\n", indent * 3, "", this->is_constant() ? "(const)" : "",  typeid(*this).name(), this, sn_code.c_str());
	}

	virtual SyntaxNode *reduce(void);

	std::string *fmt(int *depth);
	virtual std::string fold(int *indent);

	/*
	 * Do the operators even make any sense? When Code nodes are generated it's already too late to do anything
	 * where operators are used.
	 */
	bool operator==(const SyntaxNode &rhs) {
		if (typeid(rhs) != typeid(*this) || sn_ret != rhs.sn_ret || block != rhs.block)
			return false;
		const CodeSyntaxNode &r = (const CodeSyntaxNode&)rhs;
		return sn_code == r.sn_code;
	}

	bool operator<(const SyntaxNode &rhs) {
		if (typeid(rhs) != typeid(*this))
			return typeid(rhs).before(typeid(*this));
		if (sn_ret != rhs.sn_ret)
			return sn_ret < rhs.sn_ret;
		if (block != rhs.block)
			return block < rhs.block;
		const CodeSyntaxNode &r = (const CodeSyntaxNode&)rhs;
		return sn_code < r.sn_code;
	}

	bool is_constant(void) { return constant; }
	void set_constant(bool c) { constant = c; }
};

class InitLoopSyntaxNode : public SyntaxNode {
public:
	BlockSyntaxNode *loop_block;
	SyntaxNode *code;
	bool global;

	InitLoopSyntaxNode(BlockSyntaxNode *block, SyntaxNode *parent, int lineno, BlockSyntaxNode *lb, SyntaxNode *c, bool glob)
		: SyntaxNode(block, parent, SNR_NONE, lineno), loop_block(lb), code(c), global(glob) {}
	~InitLoopSyntaxNode(void) {}

	void resolve_node(pp_queue &ppq);
	SyntaxNode *apply(SyntaxNode *(*cb)(SyntaxNode *, void *), void *);
	virtual bool is_transformable(void) {
		return false;
	}
	SyntaxNode *reduce(void);
	SyntaxNode *cleanup(void);
	virtual void reduce_linit(NodeSyntaxNode *ret, std::string &var, std::string &cnt, std::string &cacheable) = 0;
	bool operator==(const class InitLoopSyntaxNode &rhs) {
		return (typeid(rhs) == typeid(*this) && sn_ret == rhs.sn_ret && block == rhs.block &&
		    loop_block == rhs.loop_block && ((code != NULL && rhs.code != NULL) ? *code == *rhs.code : code == rhs.code) && global == rhs.global);

	}
	bool isempty(void) {
		return false;
	}
	void dump(int indent) {
		SyntaxNode::dump(indent);
		if (code)
			code->dump(indent + 3);
	}

	bool operator<(const class InitLoopSyntaxNode &rhs) const {
		if (typeid(rhs) != typeid(*this))
			return typeid(rhs).before(typeid(*this));
		if (sn_ret != rhs.sn_ret)
			return sn_ret < rhs.sn_ret;
		if (block != rhs.block)
			return block < rhs.block;
		if (loop_block != rhs.loop_block)
			return loop_block < rhs.loop_block;
		if (global != rhs.global)
			return global < rhs.global;
		if (code == NULL || rhs.code == NULL)
			return code < rhs.code;
		return *code < *rhs.code;
	}
	std::string loop_var_name(void);
	virtual const char *lv_prefix(void) = 0;
	/* No operators to compare with SyntaxNode */
};

class BranchCountInitializerSyntaxNode;
class BlockSyntaxNode : public SyntaxNode {
public:
	enum {
		ctxInvalid,
		ctxLiters,
		ctxContent,
		ctxFilters,
	} context;
	std::deque<FilterSyntaxNode *> filters;
	SyntaxNode *content;
	std::set<InitLoopSyntaxNode *, ptrclasscomp<InitLoopSyntaxNode> > loops;
	bool have_counters;

	/*
	 * Outers are for copying references in function blocks to loop variables in parent blocks.
	 * Outer_loops are the same but for loop lengths and loop counts.
	 */
	std::set<InitLoopSyntaxNode *, ptrclasscomp<InitLoopSyntaxNode> > outers;
	std::set<BlockSyntaxNode *, ptrclasscomp<BlockSyntaxNode> > outer_loops;

	std::set<local_var *> liter_locals, content_locals, filter_locals;
	std::set<BranchCountInitializerSyntaxNode *, ptrclasscomp<BranchCountInitializerSyntaxNode> > branch_counts;
	std::set<BranchCountInitializerSyntaxNode *, ptrclasscomp<BranchCountInitializerSyntaxNode> > bc_outers;

	BlockSyntaxNode(BlockSyntaxNode *block, int sn_lineno)
		: SyntaxNode(block, NULL, SNR_VAR, sn_lineno), context(ctxInvalid), content(NULL), have_counters(false) {}

	~BlockSyntaxNode(void) {
		delete content;
	}

	virtual bool isempty(void) {
		return content == NULL && filters.empty();
	}

	SyntaxNode *collapse_node(void);
	SyntaxNode *optimize_node(void);

	virtual SyntaxNode *convert_to_var(void);
	virtual SyntaxNode *reduce(void);
	virtual SyntaxNode *reduce_to_function(void);
	virtual SyntaxNode *reduce_to_block(void);

	SyntaxNode *apply(SyntaxNode *(*cb)(SyntaxNode *, void *), void *);
	bool has_loop_filter(void);

	SyntaxNode *reduce_optimize(void);

	virtual void dump(int indent);

	std::string api_ptr_name(void);
	std::string loop_len_name(void);
	std::string outer_loop_len_decl(std::string *name);
	std::string loop_cnt_name(void);
	InitLoopSyntaxNode *insert_loop(InitLoopSyntaxNode *);

	void insert_local(local_var *l);

	void insert_outer(InitLoopSyntaxNode *);
	void insert_outer_loop(BlockSyntaxNode *);
	BranchCountInitializerSyntaxNode *insert_bc(BranchCountInitializerSyntaxNode *);
	void insert_bc_outer(BranchCountInitializerSyntaxNode *);

	SyntaxNode *merge_parent(void);

	virtual void parameter_output(void) {
		sn_ret = SNR_OUT;
	}

	BlockSyntaxNode *anchor(int level);

	void set_have_counters() {
		have_counters = true;
	}

	int depth();

	bool has_stree_filter();

	/*
	 * Only the same block is equal to itself.
	 */
	bool operator==(const SyntaxNode &rhs) {
		if (typeid(rhs) != typeid(*this))
			return false;
		return &this->sn_lineno == &rhs.sn_lineno;
	}
	/*
	 * Any ordering is fine. Just compare pointers.
	 */
	bool operator<(const SyntaxNode &rhs) {
		if (typeid(rhs) != typeid(*this))
			return typeid(rhs).before(typeid(*this));
		return &this->sn_lineno < &rhs.sn_lineno;
	}
};

/* Top level block. Just like a normal block, but it's not transformable. */
class TopBlockSyntaxNode : public BlockSyntaxNode {
public:
	TopBlockSyntaxNode(void)
		: BlockSyntaxNode(NULL, 0) {}
	SyntaxNode *collapse_node(void) { return this; }
	SyntaxNode *optimize_node(void) { return this; }
	bool is_transformable(void) {
		return true;
	}
};

class TemplateSyntaxNode : public SyntaxNode {
private:
	SyntaxNode *topblock;
public:
	std::set<std::string> vars;
	std::ostringstream vars_extra;

	TemplateSyntaxNode(void)
		: SyntaxNode(NULL, NULL, SNR_OUT, 0), topblock(NULL) {}

	virtual void generate_vars(void);
	virtual void generate_main(const std::string &code);
	virtual void generate_clear_cache(void);
	virtual void generate_linker(void);

	void set_topblock(TopBlockSyntaxNode *tb) {
		if (topblock)
			ICERR(this, "More than one top block for TemplateSyntaxNode");
		topblock = tb;
	}

	SyntaxNode *apply(SyntaxNode *(*cb)(SyntaxNode *, void *), void *v) {
		topblock->apply(cb, v);
		return SyntaxNode::apply(cb, v);
	}

	void dump(int indent) {
		SyntaxNode::dump(indent);
		if (topblock)
			topblock->dump(indent + 3);
	}

	SyntaxNode *reduce(void) {
		topblock = topblock->reduce();
		topblock->sn_ret = SNR_OUT;		/* XXX - horrible workaround to the crazy logic in NodeSyntaxNode::fold_node */
		return this;
	}

	std::string fold(int *indent) {
		return topblock->fold(indent);
	}

	bool isempty(void) {
		return false;
	}
	bool is_transformable(void) {
		return false;
	}	
};

class DefineTemplateSyntaxNode : public TemplateSyntaxNode {
public:
	struct tdirective *directive;
	int arg_offset;

	DefineTemplateSyntaxNode(void);

	virtual void generate_vars(void);
	virtual void generate_main(const std::string &code);
	virtual void generate_linker(void);
};

class ConditionSyntaxNode : public SyntaxNode {
public:
	SyntaxNode *condition;
	SyntaxNode *code;

	ConditionSyntaxNode(BlockSyntaxNode *block, int sn_lineno, SyntaxNode *cond, SyntaxNode *cod)
		: SyntaxNode(block, NULL, SNR_NONE, sn_lineno), condition(cond), code(cod) {}

	virtual bool isempty(void) {
		return condition == NULL && code == NULL;
	}

	SyntaxNode *apply(SyntaxNode *(*cb)(SyntaxNode *, void *), void *);
	virtual bool is_transformable(void) {
		return false;
	}
	/* No need to implement operators. Unreachable. */

	virtual void dump(int indent) {
		SyntaxNode::dump(indent);
		if (condition)
			condition->dump(indent + 3);
		if (code)
			code->dump(indent + 3);
	}
};

class ConditionalSyntaxNode : public SyntaxNode {
public:
	typedef std::deque<ConditionSyntaxNode*> deque;
	deque *conditions;

	ConditionalSyntaxNode(BlockSyntaxNode *block, int sn_lineno, deque *conds)
		: SyntaxNode(block, NULL, SNR_NONE, sn_lineno), conditions(conds) {}

	virtual bool isempty(void) {
		return !conditions || conditions->empty();
	}

	SyntaxNode *reduce(void);

	SyntaxNode *apply(SyntaxNode *(*cb)(SyntaxNode *, void *), void *);
	/* No need to implement operators. Unreachable. */

	virtual void dump(int indent) {
		SyntaxNode::dump(indent);
		for (deque::iterator it = conditions->begin() ; it != conditions->end() ; it++) {
			(*it)->dump(indent + 3);
		}
	}
};

class BranchCountInitializerSyntaxNode;
class BranchSyntaxNode : public SyntaxNode {
private:
	SyntaxNode *content;
public:
	BranchSyntaxNode *parent_branch;
	std::set<BranchCountInitializerSyntaxNode *, ptrclasscomp<BranchCountInitializerSyntaxNode> > branch_count_incrs;

	BranchSyntaxNode(BlockSyntaxNode *block, int sn_lineno, BranchSyntaxNode *parent_branch)
		: SyntaxNode(block, NULL, SNR_OUT, sn_lineno), parent_branch(parent_branch) {
		content = new OutSyntaxNode(block, sn_lineno);
	}
	~BranchSyntaxNode(void) { delete content; }

	SyntaxNode *reduce(void);
	SyntaxNode *collapse_node(void);

	std::string insert_branch_count(BlockSyntaxNode *bloop);

	SyntaxNode *apply(SyntaxNode *(*cb)(SyntaxNode *, void *), void *);

	void add(SyntaxNode *child) {
		content->add(child);
	}
	bool isempty(void) {
		return branch_count_incrs.empty() && content->isempty();
	}

	virtual void dump(int indent) {
		SyntaxNode::dump(indent);
		content->dump(indent + 3);
	}
	/* No need to implement operators. Unreachable. */
};

class ExpressionSyntaxNode : public SyntaxNode {
public:
	SyntaxNode *l;
	SyntaxNode *r;
	std::string op;
	enum sn_exp {
		SNE_EMPTY,
		SNE_NOT,
		SNE_AND,
		SNE_OR,
		SNE_TRUE,
		SNE_IFDEF,
		SNE_REGCOMP,
		SNE_OP
	} exp;

	ExpressionSyntaxNode(BlockSyntaxNode *block, SyntaxNode *parent, int sn_lineno,
		SyntaxNode *l, SyntaxNode *r, enum sn_exp exp, const std::string &op);

	~ExpressionSyntaxNode(void) {
		delete l;
		delete r;
	}

	virtual bool isempty(void) {
		return l == NULL && r == NULL;
	}

	virtual void dump(int indent) {
		SyntaxNode::dump(indent);
		fprintf(stderr, "%*sexpression %d op %s\n", indent * 3, "", exp, op.c_str());
		fprintf(stderr, "%*sleft side:\n", indent * 3, "");
		l->dump(indent + 3);
		if (r) {
			fprintf(stderr, "%*sright side:\n", indent * 3, "");
			r->dump(indent + 3);
		}
	}

	virtual void convert_node(void);
	virtual SyntaxNode *reduce(void);
	SyntaxNode *apply(SyntaxNode *(*cb)(SyntaxNode *, void *), void *);

	/* No need to implement operators. Unreachable. */

	bool is_constant(void) {
		return (!l || l->is_constant()) && (!r || r->is_constant());
	}
};

class PVarSyntaxNode : public SyntaxNode {
public:
	SyntaxNode *name;
	SyntaxNode *index;

	PVarSyntaxNode(BlockSyntaxNode *block, SyntaxNode *parent, int sn_lineno, SyntaxNode *name, SyntaxNode *index)
		: SyntaxNode(block, parent, SNR_STR, sn_lineno), name(name), index(index) {}

	~PVarSyntaxNode(void) {
		delete name;
		delete index;
	}

	virtual bool isempty(void) {
		return name == NULL && index == NULL;
	}

	void dump(int indent) {
		SyntaxNode::dump(indent);
		if (name)
			name->dump(indent + 3);
		if (index)
			index->dump(indent + 3);
	}

	virtual void convert_node(void);
	virtual SyntaxNode *reduce(void);
	SyntaxNode *apply(SyntaxNode *(*cb)(SyntaxNode *, void *), void *);

	bool operator==(const SyntaxNode &rhs) {
		if (typeid(rhs) != typeid(*this) || sn_ret != rhs.sn_ret || block != rhs.block)
			return false;
		const PVarSyntaxNode &r = (const PVarSyntaxNode&)rhs;
		return *name == *r.name && ((index == NULL || r.index == NULL) ? index == r.index : *index == *r.index);
	}

	bool operator<(const SyntaxNode &rhs) {
		if (typeid(rhs) != typeid(*this))
			return typeid(rhs).before(typeid(*this));
		if (sn_ret != rhs.sn_ret)
			return sn_ret < rhs.sn_ret;
		if (block != rhs.block)
			return block < rhs.block;
		const PVarSyntaxNode &r = (const PVarSyntaxNode&)rhs;
		if (!(*name == *r.name))
			return *name < *r.name;
		if (index == NULL || r.index == NULL)
			return index < r.index;
		return *index < *r.index;
	}
};


class ValuesInitLoopSyntaxNode : public InitLoopSyntaxNode {
public:
	ValuesInitLoopSyntaxNode(BlockSyntaxNode *block, SyntaxNode *parent, int lineno, BlockSyntaxNode *lb, SyntaxNode *c, bool glob)
		: InitLoopSyntaxNode(block, parent, lineno, lb, c, glob) {}

	void convert_node(void);
	void reduce_linit(NodeSyntaxNode *ret, std::string &var, std::string &cnt, std::string &cacheable);
	const char *lv_prefix(void) { return "sl"; }
};
class GlobInitLoopSyntaxNode : public InitLoopSyntaxNode {
public:
	GlobInitLoopSyntaxNode(BlockSyntaxNode *block, SyntaxNode *parent, int lineno, BlockSyntaxNode *lb, SyntaxNode *c, bool glob)
		: InitLoopSyntaxNode(block, parent, lineno, lb, c, glob) {}

	void convert_node(void);
	void reduce_linit(NodeSyntaxNode *ret, std::string &var, std::string &cnt, std::string &cacheable);
	const char *lv_prefix(void) { return "gl"; }
};
class VtreeKeysInitLoopSyntaxNode : public InitLoopSyntaxNode {
public:
	VtreeKeysInitLoopSyntaxNode(BlockSyntaxNode *block, SyntaxNode *parent, int lineno, BlockSyntaxNode *lb, SyntaxNode *c, bool glob)
		: InitLoopSyntaxNode(block, parent, lineno, lb, c, glob) {}

	void reduce_linit(NodeSyntaxNode *ret, std::string &var, std::string &cnt, std::string &cacheable);
	const char *lv_prefix(void) { return "vkl"; }
};
class VtreeValuesInitLoopSyntaxNode : public InitLoopSyntaxNode {
public:
	VtreeValuesInitLoopSyntaxNode(BlockSyntaxNode *block, SyntaxNode *parent, int lineno, BlockSyntaxNode *lb, SyntaxNode *c, bool glob)
		: InitLoopSyntaxNode(block, parent, lineno, lb, c, glob) {}

	void reduce_linit(NodeSyntaxNode *ret, std::string &var, std::string &cnt, std::string &cacheable);
	const char *lv_prefix(void) { return "vvl"; }
};
class VtreeByvalInitLoopSyntaxNode : public InitLoopSyntaxNode {
public:
	VtreeByvalInitLoopSyntaxNode(BlockSyntaxNode *block, SyntaxNode *parent, int lineno, BlockSyntaxNode *lb, SyntaxNode *c, bool glob)
		: InitLoopSyntaxNode(block, parent, lineno, lb, c, glob) {}
	void reduce_linit(NodeSyntaxNode *ret, std::string &var, std::string &cnt, std::string &cacheable);
	const char *lv_prefix(void) { return "vbvl"; }
};

class LoopSyntaxNode : public SyntaxNode {
public:
	int level;
	InitLoopSyntaxNode *init_loop;
	BlockSyntaxNode *loop_block;

	LoopSyntaxNode(BlockSyntaxNode *block, SyntaxNode *parent, enum sn_ret sn_ret, int lineno, int level)
		: SyntaxNode(block, parent, sn_ret, lineno), level(level), init_loop(NULL), loop_block(NULL) {}
	void resolve_node(pp_queue &ppq);
	void verify_loop_node(BlockSyntaxNode *parent_of);
	virtual SyntaxNode *reduce(void);
	bool isempty(void) {
		return false;
	}

	virtual bool operator==(const SyntaxNode &rhs) {
		if (typeid(rhs) != typeid(*this) || sn_ret != rhs.sn_ret)
			return false;
		const LoopSyntaxNode &r = (const LoopSyntaxNode &)rhs;
		return level == r.level;
	}
	virtual bool operator<(const SyntaxNode &rhs) {
		if (typeid(rhs) != typeid(*this))
			return typeid(rhs).before(typeid(*this));
		if (sn_ret != rhs.sn_ret)
			return sn_ret < rhs.sn_ret;
		const LoopSyntaxNode &r = (const LoopSyntaxNode &)rhs;
		return level < r.level;
	}
	virtual InitLoopSyntaxNode *resolve_loop(void) = 0;
};

class CountLoopSyntaxNode : public LoopSyntaxNode {
public:
	CountLoopSyntaxNode(BlockSyntaxNode *block, SyntaxNode *parent, int lineno, int level)
		: LoopSyntaxNode(block, parent, SNR_INT, lineno, level) {}
	SyntaxNode *reduce(void);
	InitLoopSyntaxNode *resolve_loop(void);
};

class LenLoopSyntaxNode : public LoopSyntaxNode {
public:
	LenLoopSyntaxNode(BlockSyntaxNode *block, SyntaxNode *parent, int lineno, int level)
		: LoopSyntaxNode(block, parent, SNR_INT, lineno, level) {}
	SyntaxNode *reduce(void);
	InitLoopSyntaxNode *resolve_loop(void);
};

class ValuesLoopSyntaxNode : public LoopSyntaxNode {
public:
	SyntaxNode *name;
	ValuesLoopSyntaxNode(BlockSyntaxNode *block, SyntaxNode *parent, int lineno, int level, SyntaxNode *name)
		: LoopSyntaxNode(block, parent, SNR_STR, lineno, level), name(name) {}

	virtual InitLoopSyntaxNode *resolve_loop(void);

	SyntaxNode *apply(SyntaxNode *(*cb)(SyntaxNode *, void *), void *);

	void dump(int indent) {
		SyntaxNode::dump(indent);
		fprintf(stderr, "%*s name:\n", indent * 3, "");
		if (name)
			name->dump(indent + 3);
	}

	bool operator==(const SyntaxNode &rhs) {
		if (!LoopSyntaxNode::operator==(rhs))
			return false;
		const ValuesLoopSyntaxNode &r = (const ValuesLoopSyntaxNode&)rhs;
		return (name == NULL || r.name == NULL) ? name == r.name : *name == *r.name;
	}
	bool operator<(const SyntaxNode &rhs) {
		if (!LoopSyntaxNode::operator==(rhs))
			return LoopSyntaxNode::operator<(rhs);
		const ValuesLoopSyntaxNode &r = (const ValuesLoopSyntaxNode&)rhs;
		if (name == NULL || r.name == NULL) {
			return name < r.name;
		}
		return *name < *r.name;
	}
};

class GlobLoopSyntaxNode : public ValuesLoopSyntaxNode {
public:
	GlobLoopSyntaxNode(BlockSyntaxNode *block, SyntaxNode *parent, int lineno, int level, SyntaxNode *name)
		: ValuesLoopSyntaxNode(block, parent, lineno, level, name) {}

	InitLoopSyntaxNode *resolve_loop(void);
};

class GlobLenSyntaxNode : public SyntaxNode {
public:
	SyntaxNode *name;

	GlobLenSyntaxNode(BlockSyntaxNode *block, int sn_lineno, SyntaxNode *name)
		: SyntaxNode(block, NULL, SNR_INT, sn_lineno), name(name) {}
	~GlobLenSyntaxNode(void) {
		delete name;
	}
	bool isempty(void) {
		return name == NULL;
	}

	void convert_node(void);
	SyntaxNode *reduce(void);
	SyntaxNode *apply(SyntaxNode *(*cb)(SyntaxNode *, void *), void *);

	bool operator==(const SyntaxNode &rhs) {
		if (typeid(rhs) != typeid(*this))
			return false;
		const GlobLenSyntaxNode &r = (const GlobLenSyntaxNode&)rhs;
		return *name == *r.name;
	}
	bool operator<(const SyntaxNode &rhs) {
		if (typeid(rhs) != typeid(*this))
			return typeid(rhs).before(typeid(*this));
		const GlobLenSyntaxNode &r = (const GlobLenSyntaxNode&)rhs;
		return *name < *r.name;
	}
};

class ListLenSyntaxNode : public SyntaxNode {
public:
	SyntaxNode *name;

	ListLenSyntaxNode(BlockSyntaxNode *block, int sn_lineno, SyntaxNode *name)
		: SyntaxNode(block, NULL, SNR_INT, sn_lineno), name(name) {}
	~ListLenSyntaxNode(void) {
		delete name;
	}
	bool isempty(void) {
		return name == NULL;
	}

	void convert_node(void);
	SyntaxNode *reduce(void);
	SyntaxNode *apply(SyntaxNode *(*cb)(SyntaxNode *, void *), void *);

	bool operator==(const SyntaxNode &rhs) {
		if (typeid(rhs) != typeid(*this))
			return false;
		const ListLenSyntaxNode &r = (const ListLenSyntaxNode&)rhs;
		return *name == *r.name;
	}
	bool operator<(const SyntaxNode &rhs) {
		if (typeid(rhs) != typeid(*this))
			return typeid(rhs).before(typeid(*this));
		const ListLenSyntaxNode &r = (const ListLenSyntaxNode&)rhs;
		return *name < *r.name;
	}
};

class BranchCountSyntaxNode : public SyntaxNode {
public:
	BranchSyntaxNode *branch;
	int level;
	int depth;
	BranchCountInitializerSyntaxNode *initializer;

	BranchCountSyntaxNode(BlockSyntaxNode *block, SyntaxNode *parent, int sn_lineno, BranchSyntaxNode *branch, int level, int depth)
		: SyntaxNode(block, parent, SNR_INT, sn_lineno), branch(branch), level(level), depth(depth), initializer(NULL) {}

	virtual bool isempty(void) {
		return false;
	}

	virtual SyntaxNode *reduce(void);
	virtual void resolve_node(pp_queue &ppq);
	bool operator==(const SyntaxNode &rhs) {
		if (typeid(rhs) != typeid(*this) || sn_ret != rhs.sn_ret)
			return false;
		const BranchCountSyntaxNode &r = (const BranchCountSyntaxNode&)rhs;
		/*
		 * Magic. Comparing unresolved branch counters is not possible without
		 * severe magic. We could walk the branch hierarchy and find the anchor
		 * and increment branches, but you're supposed to resolve your bloody
		 * branch counters before you use them as keys.
		 * Aren't C++ operators fun? We can do so many assumptions about the usage.
		 */
		if (!initializer || !r.initializer) {
			ICERR(this, "ICE: unresolved branch counter operator");
		} else {
			return initializer == r.initializer;
		}
	}

	bool operator<(const SyntaxNode &rhs) {
		if (typeid(rhs) != typeid(*this))
			return typeid(rhs).before(typeid(*this));
		if (sn_ret != rhs.sn_ret)
			return sn_ret < rhs.sn_ret;
		const BranchCountSyntaxNode &r = (const BranchCountSyntaxNode&)rhs;
		if (!initializer || !r.initializer) {
			ICERR(this, "ICE: unresolved branch counter operator");
		} else {
			return initializer < r.initializer;
		}
	}
};

class BranchCountInitializerSyntaxNode : public SyntaxNode {
public:
	BranchSyntaxNode *branch_anchor;
	BranchSyntaxNode *branch_incr;

	BranchCountInitializerSyntaxNode(BlockSyntaxNode *block, int sn_lineno, BranchSyntaxNode *anchor, BranchSyntaxNode *incr)
		: SyntaxNode(block, NULL, SNR_INT, sn_lineno), branch_anchor(anchor), branch_incr(incr) {}
	bool isempty(void) {
		return false;
	}
	std::string branch_count_name(void);
	SyntaxNode *reduce(void);
	bool operator==(const SyntaxNode &rhs) {
		if (typeid(rhs) != typeid(*this) || sn_ret != rhs.sn_ret)
			return false;
		const BranchCountInitializerSyntaxNode &r = (const BranchCountInitializerSyntaxNode&)rhs;
		/*
		 * Magic. Comparing unresolved branch counters is not possible without
		 * severe magic. We could walk the branch hierarchy and find the anchor
		 * and increment branches, but you're supposed to resolve your bloody
		 * branch counters before you use them as keys.
		 * Aren't C++ operators fun? We can do so many assumptions about the usage.
		 */
		if (!branch_anchor || !r.branch_anchor) {
			ICERR(this, "ICE: unresolved branch counter initializer operator");
		} else {
			return branch_anchor == r.branch_anchor && branch_incr == r.branch_incr;
		}
	}

	bool operator<(const SyntaxNode &rhs) {
		if (typeid(rhs) != typeid(*this))
			return typeid(rhs).before(typeid(*this));
		if (sn_ret != rhs.sn_ret)
			return sn_ret < rhs.sn_ret;
		const BranchCountInitializerSyntaxNode &r = (const BranchCountInitializerSyntaxNode&)rhs;
		if (!branch_anchor || !r.branch_anchor) {
			ICERR(this, "ICE: unresolved branch counter initializer operator");
		} else {
			if (branch_anchor != r.branch_anchor)
				return branch_anchor < r.branch_anchor;
			return branch_incr < r.branch_incr;
		}
	}
};

class ArithSyntaxNode : public SyntaxNode {
public:
	SyntaxNode *l;
	enum sn_arith {
		SNA_ADD,
		SNA_SUB,
		SNA_DIV,
		SNA_MUL,
		SNA_MOD
	} op;
	SyntaxNode *r;

	ArithSyntaxNode(BlockSyntaxNode *block, SyntaxNode *parent, int sn_lineno, SyntaxNode *l, enum sn_arith op, SyntaxNode *r)
		: SyntaxNode(block, parent, SNR_INT, sn_lineno), l(l), op(op), r(r) {}

	~ArithSyntaxNode(void) {
		delete l;
		delete r;
	}

	virtual bool isempty(void) {
		return l == NULL && r == NULL;
	}

	virtual void convert_node(void);
	virtual SyntaxNode *reduce(void);
	SyntaxNode *apply(SyntaxNode *(*cb)(SyntaxNode *, void *), void *);

	/* Operators unreachable */
};

class ParamsSyntaxNode : public SyntaxNode {
public:
	std::deque<SyntaxNode *> params;

	ParamsSyntaxNode(BlockSyntaxNode *block)
		: SyntaxNode(block, NULL, SNR_NONE, 0) {}

	~ParamsSyntaxNode(void) {
		for (std::deque<SyntaxNode *>::iterator i = params.begin() ; i != params.end() ; i++)
			delete *i;
	}

	virtual bool isempty(void) {
		return params.empty();
	}

	virtual void add(SyntaxNode *child) {
		params.push_back(child);
	}

	virtual void convert_node(void);
	virtual SyntaxNode *reduce(void);
	SyntaxNode *apply(SyntaxNode *(*cb)(SyntaxNode *, void *), void *);
	void dump(int indent) {
		for (std::deque<SyntaxNode *>::iterator i = params.begin(); i != params.end(); i++) {
			(*i)->dump(indent);
		}
	}

	bool operator==(const SyntaxNode &rhs) {
		if (typeid(rhs) != typeid(*this) || sn_ret != rhs.sn_ret || block != rhs.block)
			return false;
		const ParamsSyntaxNode &r = (const ParamsSyntaxNode&)rhs;

		if (params.size() != r.params.size())
			return false;

		std::deque<SyntaxNode *>::const_iterator i, ri;

		for (i = params.begin(), ri = r.params.begin(); i != params.end(); i++, ri++) {
			if (!(**i == **ri))
				return false;
		}
		return true;
	}

	bool operator<(const SyntaxNode &rhs) {
		if (typeid(rhs) != typeid(*this))
			return typeid(rhs).before(typeid(*this));
		if (sn_ret != rhs.sn_ret)
			return sn_ret < rhs.sn_ret;
		if (block != rhs.block)
			return block < rhs.block;
		const ParamsSyntaxNode &r = (const ParamsSyntaxNode&)rhs;

		if (params.size() != r.params.size())
			return params.size() < r.params.size();
		std::deque<SyntaxNode *>::const_iterator i, ri;

		for (i = params.begin(), ri = r.params.begin(); i != params.end(); i++, ri++) {
			if (!(**i == **ri))
				return **i < **ri;
		}
		return false;
	}

	bool is_constant(void) {
		for (std::deque<SyntaxNode*>::const_iterator i = params.begin() ; i != params.end() ; i++) {
			if (!(*i)->is_constant())
				return false;
		}
		return true;
	}
	virtual bool is_transformable(void) {
		return false;
	}
};

class FilterSyntaxNode : public SyntaxNode {
public:
	bool isloop;
	std::string name;
	ParamsSyntaxNode *args;

	/*
	 * Filters and params are always created before their parent. And line number is from the block too.
	 */
	FilterSyntaxNode(BlockSyntaxNode *block, bool isloop, const std::string &name, ParamsSyntaxNode *args)
			: SyntaxNode(block, NULL, SNR_NONE, 0), isloop(isloop), name(name), args(args) {}

	~FilterSyntaxNode(void) {
		delete args;
	}

	bool isempty(void) {
		return false;
	}

	SyntaxNode *apply(SyntaxNode *(*cb)(SyntaxNode *, void *), void *);
	bool is_transformable(void) {
		return false;
	}
	/* Operators never called */

	void dump(int indent) {
		fprintf(stderr, "%*s(", indent * 3, "");
		SyntaxNode::dump(0);
		fprintf(stderr, "%*sname=%s\n", indent * 3 + 1, "", name.c_str());
		if (args)
			args->dump(indent + 3);
		fprintf(stderr, "%*s)\n", indent * 3, "");
	}
};

class ConstructSyntaxNode : public SyntaxNode {
public:
	/* This started as a good idea to get a delete on first, but not sure it ended up as one. */
	class construct_parameter : public std::pair<std::string*,std::string>
	{
		public:
			construct_parameter(first_type f, second_type s) : std::pair<first_type, second_type>(f, s) { }
			construct_parameter(const construct_parameter &cp) : std::pair<first_type, second_type>(cp.first ? new std::string(*cp.first) : NULL, cp.second) { }

			~construct_parameter(void) { delete first; }
	};

	class construct_parameters : public std::pair<bool,std::vector<construct_parameter> >
	{
	};

	std::string name;
	construct_parameters *params;
	SyntaxNode *index;

	ConstructSyntaxNode(BlockSyntaxNode *block, enum sn_ret ret, const std::string &name, construct_parameters *params, SyntaxNode *index)
			: SyntaxNode(block, NULL, ret, 0), name(name), params(params), index(index) {}

	~ConstructSyntaxNode(void) {
		delete params;
		delete index;
	}

	bool isempty(void) {
		return false;
	}

	SyntaxNode *reduce(void) = 0;
	SyntaxNode *apply(SyntaxNode *(*cb)(SyntaxNode *, void *), void *);

	std::string generate_gperf_hash(void);

	/* Operators intentionally left blank. */
};

class PConstructSyntaxNode : public ConstructSyntaxNode {
public:
	PConstructSyntaxNode(BlockSyntaxNode *block, enum sn_ret ret, construct_parameters *params)
			: ConstructSyntaxNode(block, ret, "P", params, NULL) {}

	SyntaxNode *reduce(void);
};

class PHConstructSyntaxNode : public ConstructSyntaxNode {
public:
	PHConstructSyntaxNode(BlockSyntaxNode *block, enum sn_ret ret, construct_parameters *params, SyntaxNode *index)
			: ConstructSyntaxNode(block, ret, "PH", params, index) {}

	void convert_node(void);
	SyntaxNode *reduce(void);
};

class RegexConstructSyntaxNode : public ConstructSyntaxNode {
public:
	RegexConstructSyntaxNode(BlockSyntaxNode *block, construct_parameters *params)
			: ConstructSyntaxNode(block, SyntaxNode::SNR_REGEX, "R", params, NULL) {}

	SyntaxNode *reduce(void);
};

class LRUConstructSyntaxNode : public ConstructSyntaxNode {
public:
	std::string gn;
	local_var *entry_local;

	LRUConstructSyntaxNode(BlockSyntaxNode *block, enum sn_ret ret, construct_parameters *params, SyntaxNode *index)
			: ConstructSyntaxNode(block, ret, "LRU", params, index), entry_local(NULL) {}

	void bind_locals_node(void);

	void convert_node(void);
	SyntaxNode *reduce(void);
};

class MapConstructSyntaxNode : public ConstructSyntaxNode {
public:
	MapConstructSyntaxNode(BlockSyntaxNode *block, construct_parameters *params, SyntaxNode *index)
			: ConstructSyntaxNode(block, SyntaxNode::SNR_STR, "MAP", params, index) {}

	void convert_node(void);
	SyntaxNode *reduce(void);
};

class FunCallSyntaxNode : public SyntaxNode {
public:
	std::string name;
	SyntaxNode *params;
	local_var *res_local;

	FunCallSyntaxNode(BlockSyntaxNode *block, SyntaxNode *parent, int lineno, const std::string &name, ParamsSyntaxNode *params)
		: SyntaxNode(block, parent, SNR_VAR, lineno), name(name), params(params), res_local(NULL) {}

	bool isempty(void) {
		return false;
	}

	SyntaxNode *reduce(void);
	SyntaxNode *apply(SyntaxNode *(*cb)(SyntaxNode *, void *), void *);
	void resolve_node(pp_queue &ppq);

	void bind_locals_node(void);

	/*
	 * Only the same function is equal to itself.
	 */
	bool operator==(const SyntaxNode &rhs) {
		if (typeid(rhs) != typeid(*this))
			return false;
		return &this->sn_lineno == &rhs.sn_lineno;
	}
	/*
	 * Any ordering is fine. Just compare pointers.
	 */
	bool operator<(const SyntaxNode &rhs) {
		if (typeid(rhs) != typeid(*this))
			return typeid(rhs).before(typeid(*this));
		return &this->sn_lineno < &rhs.sn_lineno;
	}

	void dump(int indent) {
		fprintf(stderr, "%*s(", indent * 3, "");
		SyntaxNode::dump(0);
		fprintf(stderr, "%*sname=%s\n", indent * 3 + 1, "", name.c_str());
		if (params)
			params->dump(indent + 3);
		fprintf(stderr, "%*s)\n", indent * 3, "");
	}
};

class DefineSyntaxNode : public SyntaxNode {
public:
	SyntaxNode *name;
	SyntaxNode *value;

	DefineSyntaxNode(BlockSyntaxNode *block, int lineno, SyntaxNode *name, SyntaxNode *value)
		: SyntaxNode(block, NULL, SNR_OUT, lineno), name(name), value(value) {}
	~DefineSyntaxNode() {
		delete name;
		delete value;
	}

	bool isempty(void) {
		return false;
	}

	bool is_transformable(void) {
		return false;
	}

	void convert_node(void);
	SyntaxNode *reduce(void);
	SyntaxNode *apply(SyntaxNode *(*cb)(SyntaxNode *, void *), void *);

	void dump(int indent) {
		fprintf(stderr, "%*s(", indent * 3, "");
		SyntaxNode::dump(0);
		name->dump(indent + 3);
		value->dump(indent + 3);
		fprintf(stderr, "%*s)\n", indent * 3, "");
	}
};

class IncludeSyntaxNode : public SyntaxNode {
public:
	SyntaxNode *arg;

	IncludeSyntaxNode(BlockSyntaxNode *block, int lineno, SyntaxNode *arg)
		: SyntaxNode(block, NULL, SNR_OUT, lineno), arg(arg) {}
	~IncludeSyntaxNode() {
		delete arg;
	}

	bool isempty(void) {
		return false;
	}

	bool is_transformable(void) {
		return false;
	}

	void convert_node(void);
	SyntaxNode *reduce(void);
	SyntaxNode *apply(SyntaxNode *(*cb)(SyntaxNode *, void *), void *);

	void dump(int indent) {
		fprintf(stderr, "%*s(", indent * 3, "");
		SyntaxNode::dump(0);
		arg->dump(indent + 3);
		fprintf(stderr, "%*s)\n", indent * 3, "");
	}
};

class ConvertSyntaxNode : public SyntaxNode {
public:
	SyntaxNode *sn;

	ConvertSyntaxNode(SyntaxNode *sn, enum sn_ret ret)
		: SyntaxNode(sn->block, NULL, ret, sn->sn_lineno), sn(sn)
	{
	}
	virtual ~ConvertSyntaxNode(void)
	{
		delete sn;
	}

	bool isempty(void) {
		return false;
	}

	bool is_transformable(void) {
		return false;
	}

	virtual SyntaxNode *apply(SyntaxNode *(*cb)(SyntaxNode *, void *), void *);

	void dump(int indent) {
		fprintf(stderr, "%*s(", indent * 3, "");
		SyntaxNode::dump(0);
		sn->dump(indent + 3);
		fprintf(stderr, "%*s)\n", indent * 3, "");
	}

	bool is_constant(void) {
		return sn->is_constant();
	}

	bool is_trivial_var(void) {
		return sn->is_trivial_var();
	}
};

class StringConvertSyntaxNode : public ConvertSyntaxNode {
private:
	local_var *str_local;
public:
	StringConvertSyntaxNode(SyntaxNode *sn)
		: ConvertSyntaxNode(sn, SNR_STR), str_local(NULL)
	{
	}

	void bind_locals_node(void);
	SyntaxNode *reduce(void);
};

class IntConvertSyntaxNode : public ConvertSyntaxNode {
public:
	IntConvertSyntaxNode(SyntaxNode *sn)
		: ConvertSyntaxNode(sn, SNR_INT)
	{
	}

	SyntaxNode *reduce(void);
};

class VarConvertSyntaxNode : public ConvertSyntaxNode {
public:
	VarConvertSyntaxNode(SyntaxNode *sn)
		: ConvertSyntaxNode(sn, SNR_VAR)
	{
	}

	SyntaxNode *reduce(void);
};

class EmptyVarSyntaxNode : public SyntaxNode {
public:
	EmptyVarSyntaxNode(BlockSyntaxNode *block, int lineno)
		: SyntaxNode(block, NULL, SNR_VAR, lineno) {}

	SyntaxNode *reduce(void);
	bool isempty(void) { return 0; }

	bool is_trivial_var(void) {
		return true;
	}
};

enum eatspace
{
	EAT_NONE = -1, EAT_BUG, EAT_SOME, EAT_ALL, EAT_EOL
};
extern enum eatspace eatspace;

extern std::set<std::string> globals;
extern std::deque<std::string> global_funcs;
extern std::set<std::string> includes;
extern std::set<std::string> clear_cache;

std::string get_global_name(std::string *g);

#endif /*SYNTAX_H*/
