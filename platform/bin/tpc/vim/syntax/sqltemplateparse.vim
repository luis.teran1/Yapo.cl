" Vim syntax file
" Language:	SQL with Templateparse tags
" Maintainer:	Pelle Johansson <pelle@blocket.se>
" Last Change: 2006-10-19

" For version 5.x: Clear all syntax items
" For version 6.x: Quit when a syntax file was already loaded
if version < 600
  syntax clear
elseif exists("b:current_syntax")
  finish
endif

if !exists("main_syntax")
  let main_syntax = 'sql'
endif

if version < 600
  syn include @sqlTop <sfile>:p:h/sql.vim
  so <sfile>:p:h/templateparse.vim
else
  syn include @sqlTop syntax/sql.vim
  unlet b:current_syntax
  runtime! syntax/sql.vim
  unlet b:current_syntax
  runtime! syntax/templateparse.vim
endif

" syn cluster sqlTop remove=sqlString,sqlComment
" syn sync clear

syntax cluster sqlTop add=templateparseZone
syntax cluster templateparseText add=@sqlTop
" syntax region start="" end="" contains=sqlTop

let b:current_syntax = "sqltemplateparse"
