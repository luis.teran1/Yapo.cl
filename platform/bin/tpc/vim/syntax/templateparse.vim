" Vim syntax file
" Language:	Smarty Templates
" Maintainer:	Manfred Stienstra manfred.stienstra@dwerg.net
" Last Change:  Fri Nov 28 17:38:40 CET 2003
" Filenames:    *.tpl
" URL:		http://www.dwerg.net/projects/vim/templateparse.vim

" For version 5.x: Clear all syntax items
" For version 6.x: Quit when a syntax file was already loaded
if version < 600
  syntax clear
elseif exists("b:current_syntax")
  finish
endif

if !exists("main_syntax")
  let main_syntax = 'templateparse'
endif

syn cluster templateparseText contains=templateparseVtree,templateparseVar,templateparseExpr,templateparseSpecial,templateparseComment

syn region templateparseVtree start="\$\+(" end=")" contained oneline contains=templateparseVar,templateparseVtree

syn match templateparseVar contained "[%@#][%@]*[a-zA-Z#][a-zA-Z0-9_]*\*\?"
syn match templateparseExprVar contained "!\??\?[%@#][%@]*[a-zA-Z_#][a-zA-Z0-9_]*\*\?"
syn match templateparseComment contained "/\*\([^*]\|\*[^/]\|[\n]\)*\*/"

syn region templateparseExpr contained matchgroup=Delimiter start="\(<%\s*\)\@<=(" end=")" contained contains=templateparseVtree,templateparseVar,templateparseExprVar

syn region templateparseFilter matchgroup=Delimiter start="\(<%\s*\)\@<=|" end="|" contains=templateparseFilterFunc

syn region templateparseZone matchgroup=Delimiter start="<%" end="%>" fold contains=@templateparseText

syn match templateparseFilterFunc "[a-zA-Z_]*([^()]*)" contained

if version >= 508 || !exists("did_templateparse_syn_inits")
  if version < 508
    let did_templateparse_syn_inits = 1
    command -nargs=+ HiLink hi link <args>
  else
    command -nargs=+ HiLink hi def link <args>
  endif

  HiLink templateparseVtree Constant
  HiLink templateparseProperty Constant 
  HiLink templateparseFilterFunc Function
  HiLink templateparseVar SpecialKey
  HiLink templateparseExprVar SpecialKey
  HiLink templateparseComment Comment
  delcommand HiLink
endif 

let b:current_syntax = "templateparse"

if main_syntax == 'templateparse'
  unlet main_syntax
endif

" vim: ts=8

