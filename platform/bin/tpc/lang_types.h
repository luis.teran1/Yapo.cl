#ifndef LANG_TYPES_H
#define LANG_TYPES_H

#include "lang.hh"

int  yylex(YYSTYPE*);
void yyerror(TemplateSyntaxNode **, std::string);

#endif

