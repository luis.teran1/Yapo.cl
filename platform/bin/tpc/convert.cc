
#include <sstream>

#include "syntax.h"
#include "vtree_syntax_nodes.hh"

SyntaxNode *
ConvertSyntaxNode::apply(SyntaxNode *(*cb)(SyntaxNode *, void *), void *v) {
	sn = sn->apply(cb, v);
	return SyntaxNode::apply(cb, v);
}

/*
 * Convert any node we can into SNR_STR
 */

void
StringConvertSyntaxNode::bind_locals_node(void) {
	std::string ln;

	if (str_local)
		return;

	str_local = new local_var();
	ln = str_local->get_name();
	str_local->decl = "char *" + ln + " = NULL;";
	str_local->cleanup = "free(" + ln + ");";

	block->insert_local(str_local);
}

SyntaxNode *
StringConvertSyntaxNode::reduce(void) {
	int indent = 0;
	std::string ln = str_local->get_name();
	bool isc = sn->is_constant();

	sn = sn->reduce();
	return new CodeSyntaxNode(SNR_STR, "tpvar_str(" + sn->fold(&indent) + ", &" + ln + ")", CodeSyntaxNode::INLINE, isc);
}

SyntaxNode *
SyntaxNode::convert_to_str(void) {
	if (sn_ret == SNR_STR)
		return this;
	return new StringConvertSyntaxNode(this->convert_to_var());
}

SyntaxNode *
StringSyntaxNode::convert_to_str(void) {
	CodeSyntaxNode *ret = new CodeSyntaxNode(SNR_STR, "\"" + escape() + "\"", CodeSyntaxNode::INLINE, true);
	delete this;
	return ret;
}

/*
 * Convert any node we can into SNR_INT
 */

SyntaxNode *
IntConvertSyntaxNode::reduce(void) {
	bool isc = sn->is_constant();
	enum sn_ret ret = sn->sn_ret;
	int indent = 0;

	sn = sn->reduce();
	if (ret == SNR_STR) {
		return new CodeSyntaxNode(SNR_INT, "atoi(" + sn->fold(&indent) + ")", CodeSyntaxNode::INLINE, isc);
	} else {
		return new CodeSyntaxNode(SNR_INT, "tpvar_int(" + sn->fold(&indent) + ")", CodeSyntaxNode::INLINE, isc);
	}
}

SyntaxNode *
SyntaxNode::convert_to_int(void) {
	if (sn_ret == SNR_INT)
		return this;

	return new IntConvertSyntaxNode(this);
}

SyntaxNode *
StringSyntaxNode::convert_to_int(void) {
	SyntaxNode *sn = new IntegerSyntaxNode(block, NULL, sn_lineno, atoi(sn_str.c_str()));

	delete this;
	return sn;
}

#if 0
SyntaxNode *
IntegerSyntaxNode::intcode(void) {
	std::ostringstream n;

	n << val;

	SyntaxNode *res = new CodeSyntaxNode(block, NULL, SNR_INT, sn_lineno, n.str(), CodeSyntaxNode::INLINE, true);
	delete this;
	return res;
}
#endif

/*
 * varcode
 */

SyntaxNode *
VarConvertSyntaxNode::reduce(void) {
	int indent = 0;
	bool isc = sn->is_constant();
	enum sn_ret ret = sn->sn_ret;

	sn = sn->reduce();
	switch (ret) {
	case SNR_STR:
		return new CodeSyntaxNode(SNR_VAR, "&TPV_STRING(" + sn->fold(&indent) + ")", CodeSyntaxNode::INLINE, isc);
	case SNR_INT:
		return new CodeSyntaxNode(SNR_VAR, "&TPV_INT(" + sn->fold(&indent) + ")", CodeSyntaxNode::INLINE, isc);
	case SNR_REGEX:
		return new CodeSyntaxNode(SNR_VAR, "&TPV_REGEX(" + sn->fold(&indent) + ")", CodeSyntaxNode::INLINE, isc);
	case SNR_NONE:
		return new CodeSyntaxNode(SNR_VAR, "&TPV_OPAQUE(" + sn->fold(&indent) + ")", CodeSyntaxNode::INLINE, isc);
	case SNR_NODE:
		return new CodeSyntaxNode(SNR_VAR, "&TPV_NODE(" + sn->fold(&indent) + ")", CodeSyntaxNode::INLINE, isc);
	default:
		ICERR(this, "varcode (%s)", typeid(*sn).name());
	}
}

SyntaxNode *
SyntaxNode::convert_to_var(void) {
	if (sn_ret == SNR_VAR)
		return this;
	return new VarConvertSyntaxNode(this);
}

/*
 * bool
 */

SyntaxNode *
SyntaxNode::convert_to_bool(void) {
	if (sn_ret == SNR_BOOL)
		return this;

	ICERR(this, "convert_to_bool (%s)", typeid(*this).name());
}
