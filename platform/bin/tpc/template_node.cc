
#include "syntax.h"
#include "builtin.h"

#include <iostream>
#include <string.h>

extern char *template_name;

static std::string *
mangle_template_name(const char *t) {
	std::string *ret;
	std::string::size_type pos;

	ret = new std::string(t);

	pos = 0;
	while((pos = ret->find('.', pos)) != std::string::npos) {
		(*ret)[pos] = '_';
	}
	pos = 0;
	while((pos = ret->find('/', pos)) != std::string::npos) {
		ret->replace(pos, 1, "__");
	}

	return ret;
}

static std::string *
get_template_name(void) {
	static std::string *templ_name;

	if (!templ_name) {
		if (strlen(template_name) > 5 && strcmp(".tmpl", template_name + strlen(template_name) - 5) == 0) {
			*(template_name + strlen(template_name) - 5) = '\0';
		}
		templ_name = mangle_template_name(template_name);
	}
	return templ_name;
}

static std::string
indent(std::string str, int level = 1) {
	/* Add a \t on each line */
	int cnt;
	std::string::size_type pos = 0;
	
	for (cnt = 0 ; cnt < level ; cnt++)
		str.insert(0, "\t");
	while((pos = str.find_first_of('\n', pos)) != std::string::npos) {
		for (cnt = 0 ; cnt < level ; cnt++) 
			str.insert(pos+1, "\t");
		pos+=1 + level;
	}
	return str;
}

void
TemplateSyntaxNode::generate_vars(void)
{
	if (vars.size() || !vars_extra.str().empty()) {
		std::cout << std::endl << "struct __vars_" << *get_template_name() << " {" << std::endl;
		for (std::set<std::string>::iterator i = vars.begin() ; i != vars.end() ; i++ ) {
			std::cout << "\tTEMPLATE_VAR_DECL(" << *i << ")" << std::endl;
		}
		std::cout << vars_extra.str();
		std::cout << "};" << std::endl;

		std::cout << "#define COPY_VARS(vp) \\\n";
		std::cout << "\tstruct __vars_" + *get_template_name() + " *const ov UNUSED = (vp);\\\n";
		std::cout << "\tstruct __vars_" + *get_template_name() + " v UNUSED = *ov;\n\n";
	} else {
		std::cout << "#define COPY_VARS(vp) \\\n\tvoid *v UNUSED;\n\n";
	}

	for (std::set<std::string>::iterator i = vars.begin(); i != vars.end(); i++) {
		std::cout << "TEMPLATE_VAR(" << *get_template_name() << ", " << (*i) << ")\n\n";
	}
}

void
TemplateSyntaxNode::generate_main(const std::string &code)
{
	std::cout << "static void template_" << *get_template_name() << "(struct bpapi *api) {" << std::endl;

	if (vars.size() || !vars_extra.str().empty()) {
		std::cout << "\tstruct __vars_" << *get_template_name() << " v UNUSED = TEMPLATE_VAR_INIT;" << std::endl;
	} else {
		std::cout << "\tvoid *v UNUSED;" << std::endl;
	}

	std::cout << 
		"\tenum bpcacheable cacheable UNUSED = BPCACHE_CAN;\n"
		<< indent(code) << std::endl;

	std::cout << "}" << std::endl << std::endl;

}

void
TemplateSyntaxNode::generate_clear_cache(void)
{
	/* XXX clear_cache is a global, should be member. */
	if (clear_cache.size()) {
		std::cout << "static void\n";
		std::cout << "tmpl_clear_cache(void) {\n";
		for (std::set<std::string>::iterator i = clear_cache.begin() ; i != clear_cache.end() ; i++ ) {
			std::cout << indent(*i, 1) << ";\n";
		}
		std::cout << "}\n\n";
	}
}

void
TemplateSyntaxNode::generate_linker(void)
{
	/* XXX clear_cache is a global, should be member. */
	std::cout << "ADD_TEMPLATE(" << template_name << ", template_" << *get_template_name() << ", " << (clear_cache.size() ? "tmpl_clear_cache" : "NULL") << ");" << std::endl;
}

DefineTemplateSyntaxNode::DefineTemplateSyntaxNode(void) : TemplateSyntaxNode()
{
	includes.insert("ctemplates.h");
}

void
DefineTemplateSyntaxNode::generate_vars(void)
{
	std::ostringstream argvars;
	for (std::set<std::string>::iterator i = vars.begin() ; i != vars.end() ; ) {
		std::deque<std::string *>::iterator ai = directive->args.begin(), ae = directive->args.end();

		while (ai != ae) {
			if (*i == **ai || *i + "?" == **ai)
				break;
			ai++;
		}
		if (ai != ae) {
			argvars << "TEMPLATE_ARG_VAR(" << *get_template_name() << ", " << (*i) << ", " << ((ai - directive->args.begin()) - arg_offset) << ")\n";
			vars.erase(i);
			i = vars.begin(); /* sigh */
		} else {
			i++;
		}
	}
	vars_extra << "\tint argc;\n\tconst char **args;\n";
	TemplateSyntaxNode::generate_vars();
	std::cout << argvars.str();
}

void
DefineTemplateSyntaxNode::generate_main(const std::string &code)
{
	int nargs = (directive->args.size() - arg_offset);

	if (*directive->name == "deffilter" || *directive->name == "deffilter_overridable") {
		std::cout << "static void template_" << *get_template_name() << "(struct bpapi_output_chain *ochain) {" << std::endl;
		std::cout << "\tstruct deffilter_data *deffilter = ochain->data;\n";
		std::cout << "\tstruct bpapi *const oapi = deffilter->api;\n";
		std::cout << "\tstruct bpapi ba;\n";
		std::cout << "\tba.ochain = *ochain->next;\n";
	} else {
		std::cout << "static const struct tpvar *\ntemplate_" << *get_template_name() << "(struct bpapi *oapi, struct tpvar *dst, enum bpcacheable *cc, int argc, const struct tpvar **argv) {" << std::endl;
		std::cout << "\tstruct bpapi ba;\n";
		std::cout << "\tstruct buf_string *buf = buf_output_init(&ba.ochain);\n";
	}
	std::cout << "\tBPAPI_COPY(&ba, oapi, 0, 0, 1);\n";
	std::cout << "\thash_simplevars_init(&ba.schain);\n";
	std::cout << "\tba.schain.next = NULL;\n";
	std::cout << "\tstruct bpapi *api = &ba;\n";

	if (vars.size() || !vars_extra.str().empty()) {
		std::cout << "\tstruct __vars_" << *get_template_name() << " v UNUSED = TEMPLATE_VAR_INIT;" << std::endl;
	} else {
		std::cout << "\tvoid *v UNUSED;" << std::endl;
	}

	if (nargs > 0) {
		std::cout << "\tchar *fargs[" << nargs << "];\n";
		if (*directive->name == "deffun_vtree") {
			std::cout << "\tstruct vtree_keyvals_elem vtree_args_list[" << nargs << "];\n";
			std::cout << "\tstruct vtree_keyvals vtree_args = {\n";
			std::cout << "\t\t.type = vktDict,\n";
			std::cout << "\t\t.len = " << nargs << ",\n";
			std::cout << "\t\t.list = vtree_args_list,\n";
			std::cout << "\t\t.cleanup = NULL,\n";
			std::cout << "\t};\n";
			std::cout << "\tstruct shadow_vtree vtree_args_shadow = {\n";
			std::cout << "\t\t.vtree = {\n";
			std::cout << "\t\t\t.fun = &vtree_literal_vtree,\n";
			std::cout << "\t\t\t.data = &vtree_args,\n";
			std::cout << "\t\t\t.next = NULL,\n";
			std::cout << "\t\t},\n";
			std::cout << "\t\t.free_cb = NULL,\n";
			std::cout << "\t};\n";
			std::cout << "\tshadow_vtree_init(&ba.vchain, &vtree_args_shadow, &oapi->vchain);\n";
		} else {
			std::cout << "\tconst char *args[" << nargs << "];\n";
		}
		std::cout << "\tint i;\n";
		std::cout << "\tconst char *varnames[] = {";
		std::deque<std::string *>::iterator ai = directive->args.begin();
		ai += 2;
		for (; ai != directive->args.end(); ai++) {
			std::cout << "\"" + **ai+"\", ";
		}
		std::cout << "};\n";
	}

	if (*directive->name == "deffun_vtree") {
		if (nargs > 0) {
			std::cout << "\tfor (i = 0 ; i < " << nargs << " ; i++) {\n";
			std::cout << "\t\tvtree_args_list[i].key = varnames[i];\n";
			std::cout << "\t\tif (i >= argc) {\n";
			std::cout << "\t\t\tvtree_args_list[i].type = vkvNone;\n";
			std::cout << "\t\t\tfargs[i] = NULL;\n";
			std::cout << "\t\t} else if (argv[i]->type == TPV_NODE) {\n";
			std::cout << "\t\t\tvtree_args_list[i].type = vkvNode;\n";
			std::cout << "\t\t\tvtree_args_list[i].v.node = *(tpvar_node(argv[i]) ?: &(struct bpapi_vtree_chain){0});\n";
			std::cout << "\t\t\tfargs[i] = NULL;\n";
			std::cout << "\t\t} else {\n";
			std::cout << "\t\t\tvtree_args_list[i].type = vkvValue;\n";
			std::cout << "\t\t\tvtree_args_list[i].v.value = tpvar_str(argv[i], &fargs[i]);\n";
			std::cout << "\t\t}\n";
			std::cout << "\t}\n";
			std::cout << "\tv.argc = argc < i ? argc : i;\n";
		} else {
			std::cout << "\tv.argc = 0;\n";
		}
	} else {
		if (*directive->name == "deffilter" || *directive->name == "deffilter_overridable") {
			std::cout << "\targs[0] = deffilter->buf.buf ?: \"\";\n";
			std::cout << "\tfargs[0] = deffilter->buf.buf;\n";
			std::cout << "\tif (ba.schain.fun->insert) bps_insert(&ba.schain, varnames[0], args[0]);\n";
			if (nargs > 1) {
				std::cout << "\tfor (i = 0 ; i < " << nargs << " - 1; i++) {\n";
				std::cout << "\t\tif (i < deffilter->argc) {\n";
				std::cout << "\t\t\targs[i + 1] = tpvar_str(deffilter->argv[i], &fargs[i + 1]);\n";
				std::cout << "\t\t\tif (ba.schain.fun->insert) bps_insert(&ba.schain, varnames[i + 1], args[i + 1]);\n";
				std::cout << "\t\t} else {\n";
				std::cout << "\t\t\targs[i + 1] = NULL;\n";
				std::cout << "\t\t}\n";
				std::cout << "\t}\n";
				std::cout << "\tv.argc = (deffilter->argc < i ? deffilter->argc : i) + 1;\n";
			} else {
				std::cout << "\tv.argc = 1;\n";
			}
		} else {
			if (nargs > 0) {
				std::cout << "\tfor (i = 0 ; i < " << nargs << " ; i++) {\n";
				std::cout << "\t\tif (i < argc) {\n";
				std::cout << "\t\t\targs[i] = tpvar_str(argv[i], &fargs[i]);\n";
				std::cout << "\t\t\tif (ba.schain.fun->insert) bps_insert(&ba.schain, varnames[i], args[i]);\n";
				std::cout << "\t\t} else {\n";
				std::cout << "\t\t\targs[i] = NULL;\n";
				std::cout << "\t\t}\n";
				std::cout << "\t}\n";
				std::cout << "\tv.argc = argc < i ? argc : i;\n";
			} else {
				std::cout << "\tv.argc = 0;\n";
			}
		}
		if (nargs > 0)
			std::cout << "\tv.args = args;\n";
		else
			std::cout << "\tv.args = NULL;\n";
	}

	std::cout << 
		"\tenum bpcacheable cacheable UNUSED = BPCACHE_CAN;\n"
		<< indent(code) << std::endl;

	if (nargs > 0) {
		std::cout << "\tfor (i = 0 ; i < v.argc ; i++) {\n";
		std::cout << "\t\tfree(fargs[i]);\n";
		std::cout << "\t}\n";
	}

	std::cout << "\tbpapi_simplevars_free(&ba);\n";

	if (directive->name->substr(0, 6) == "deffun") {
		std::cout << "\t*cc = cacheable;\n";
		std::cout << "\t(void)TPV_SET(dst, buf->buf ? TPV_DYNSTR(buf->buf) : TPV_NULL);\n";
		std::cout << "\tbuf->buf = NULL;\n";
		std::cout << "\tbpapi_output_free(&ba);\n";
		std::cout << "\treturn dst;\n";
	}

	std::cout << "}" << std::endl << std::endl;

}

void
DefineTemplateSyntaxNode::generate_linker(void)
{
	if (*directive->name == "deffilter") {
		std::cout << "TEMPLATE_DEFFILTER(" << *directive->args[0] << ", template_" << *get_template_name() << ");\n";
	} else if (*directive->name == "deffilter_overridable") {
		std::cout << "TEMPLATE_DEFFILTER_WEAK(" << *directive->args[0] << ", template_" << *get_template_name() << ");\n";
	} else if (*directive->name == "deffun_overridable") {
		std::cout << "ADD_TEMPLATE_FUNCTION_WITH_NAME_ATTR(template_" << *get_template_name() << ", " << *directive->args[0] << ", TEMPLATE_WEAK);\n";
	} else {
		std::cout << "ADD_TEMPLATE_FUNCTION_WITH_NAME(template_" << *get_template_name() << ", " << *directive->args[0] << ");\n";
	}
	if (clear_cache.size())
		std::cout << "ADD_NULL_TEMPLATE(tmpl_clear_cache);" << std::endl;
}
