#include "syntax.h"

ExpressionSyntaxNode::ExpressionSyntaxNode(BlockSyntaxNode *block, SyntaxNode *parent, int sn_lineno,
		SyntaxNode *l, SyntaxNode *r, enum sn_exp exp, const std::string &op)
		: SyntaxNode(block, parent, SNR_BOOL, sn_lineno) {
	this->l = l;
	this->r = r;
	this->exp = exp;
	this->op = op;

	if (l == NULL)
		ICERR(this, "ICE: expression without left-hand operand");

	switch (exp) {
	case SNE_TRUE:
		/*
		 * Even though TRUE is a nullary operator, it still needs to take an
		 * operand so that we can do proper variable instantiation on it.
		 */
	case SNE_EMPTY:
	case SNE_NOT:
	case SNE_IFDEF:
		if (r != NULL)
			ICERR(this, "ICE: Unary expression got right-hand operand");
		break;
	case SNE_AND:
	case SNE_OR:
	case SNE_REGCOMP:
	case SNE_OP:
		if (r == NULL)
			ICERR(this, "ICE: binary expression without right-hand operand");
		break;
	}
	if (exp == SNE_OP) {
		if (op == "")
			ICERR(this, "ICE: operator expression without operator");
	} else {
		if (op != "")
			ICERR(this, "ICE: operator expression with operator");
	}
}

SyntaxNode *
SyntaxNode::ifdef(void) {
	int indent = 0;

	if (sn_ret == SNR_STR) {
		return new CodeSyntaxNode(SNR_BOOL, "template_ifdef(api, " + this->reduce()->fold(&indent) + ")");
	} else {
		return new CodeSyntaxNode(SNR_BOOL, "template_ifdef_var(api, " + this->reduce()->fold(&indent) + ")");
	}
}

SyntaxNode *
StringSyntaxNode::ifdef(void) {
	SyntaxNode *ret = new CodeSyntaxNode(SNR_BOOL, "__" + sn_str + "_ifdef(&v, api)");

	delete this;
	return ret;
}

SyntaxNode *
ExpressionSyntaxNode::apply(SyntaxNode *(*cb)(SyntaxNode *, void *), void *v) {
	l = l->apply(cb, v);
	if (r)
		r = r->apply(cb, v);
	return SyntaxNode::apply(cb, v);
}

void
ExpressionSyntaxNode::convert_node(void) {
	switch (exp) {
	case SNE_IFDEF:
		if (l->sn_ret != SNR_STR)
			l = l->convert_to_var();
		break;
	case SNE_OP:
		if (l->sn_ret == SNR_STR && r->sn_ret == SNR_STR) {
			/* nop */
		} else if (l->sn_ret == SNR_INT || r->sn_ret == SNR_INT) {
			l = l->convert_to_int();
			r = r->convert_to_int();
		} else {
			l = l->convert_to_var();
			r = r->convert_to_var();
		}
		break;
	case SNE_REGCOMP:
		if (r->sn_ret == SNR_REGEX) {
			l = l->convert_to_str();
		} else if (l->sn_ret == SNR_VAR || r->sn_ret == SNR_VAR) {
			l = l->convert_to_var();
			r = r->convert_to_var();
		} else {
			l = l->convert_to_str();
			r = r->convert_to_str();
		}
	default:
		break;
	}
}

SyntaxNode *
ExpressionSyntaxNode::reduce(void) {
	SyntaxNode *ret;
	int indent = 0;

	switch (exp) {
	case SNE_EMPTY:
		ret = new CodeSyntaxNode(SNR_BOOL, "(" + l->reduce()->fold(&indent) + ")");
		break;
	case SNE_NOT:
		ret = new CodeSyntaxNode(SNR_BOOL, "!(" + l->reduce()->fold(&indent) + ")");
		break;
	case SNE_AND:
		ret = new CodeSyntaxNode(SNR_BOOL, "(" + l->reduce()->fold(&indent) + "&&" + r->reduce()->fold(&indent) + ")");
		break;
	case SNE_OR:
		ret = new CodeSyntaxNode(SNR_BOOL, "(" + l->reduce()->fold(&indent) + "||" + r->reduce()->fold(&indent) + ")");
		break;
	case SNE_TRUE:
		ret = new CodeSyntaxNode(SNR_BOOL, "1");
		break;
	case SNE_IFDEF:
		ret = l->ifdef();
		break;
	case SNE_OP:
		if (l->sn_ret == SNR_STR && r->sn_ret == SNR_STR) {
			const char *comp;

			if (op == "==" || op == "!=") {
				comp = "(strcmp(";
			} else {
				comp = "(template_compare_str(";
			}
			ret = new CodeSyntaxNode(SNR_BOOL, comp + l->reduce()->fold(&indent) + ", " + r->reduce()->fold(&indent) + ") " + op + " 0)");
		} else if (l->sn_ret == SNR_INT || r->sn_ret == SNR_INT) {
			ret = new CodeSyntaxNode(SNR_BOOL, "((" + l->reduce()->fold(&indent) + ") " + op + " (" + r->reduce()->fold(&indent) + "))");
		} else {
			ret = new CodeSyntaxNode(SNR_BOOL, "(template_compare_vars(" + l->reduce()->fold(&indent) + ", " + r->reduce()->fold(&indent) + ") " + op + " 0)");
		}
		break;
	case SNE_REGCOMP:
		/*
		 * Yes, the arguments are in a different order for precompiled and
		 * not precompiled regexps. Yes, it's stupid.
		 */

		if (r->sn_ret == SNR_REGEX) {
			ret = new CodeSyntaxNode(SNR_BOOL, "cached_regex_match(" + r->reduce()->fold(&indent) + ", " + l->reduce()->fold(&indent) + ", NULL, 0)");
		} else if (l->sn_ret == SNR_VAR || r->sn_ret == SNR_VAR) {
			ret = new CodeSyntaxNode(SNR_BOOL, "template_regcompare_vars(" + l->reduce()->fold(&indent) + ", " + r->reduce()->fold(&indent) + ")");
		} else {
			ret = new CodeSyntaxNode(SNR_BOOL, "template_regcompare(" + l->reduce()->fold(&indent) + ", " + r->reduce()->fold(&indent) + ")");
		}
		break;
	default:
		ICERR(this, "ICE: unknown expression");
	}
	l = r = NULL;
	delete this;

	return ret;
}

