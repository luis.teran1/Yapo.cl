#include <iostream>
#include <string>
#include <list>
#include <deque>
#include "builtin.h"
#include "syntax.h"
#include <stdlib.h>

const char *
handle_directive(struct tdirective *td, DefineTemplateSyntaxNode *sn) {
	if (*td->name == "eatspace") {
		if (td->args.size() != 1)
			return "#eatspace takes one argument";
		if (*td->args[0] == "all")
			eatspace = EAT_ALL;
		else if (*td->args[0] == "some")
			eatspace = EAT_SOME;
		else if (*td->args[0] == "bug")
			eatspace = EAT_BUG;
		else if (*td->args[0] == "none")
			eatspace = EAT_NONE;
		else if (*td->args[0] == "eol")
			eatspace = EAT_EOL;
		else
			eatspace = (enum eatspace)atoi(td->args[0]->c_str());
		delete td->name;
		return NULL;
	} else if (*td->name == "deffun" || *td->name == "deffun_overridable" || *td->name == "deffun_vtree") {
		if (td->args.size() < 2)
			return "Too few arguments to #deffun";
		sn->directive = td;
		sn->arg_offset = 2;
		includes.insert("ctemplates.h");
		if (*td->name == "deffun_vtree")
			includes.insert("vtree_literal.h");
		return NULL;
	} else if (*td->name == "deffilter" || *td->name == "deffilter_overridable") {
		if (td->args.size() < 3)
			return "Too few arguments to #deffilter";
		sn->directive = td;
		sn->arg_offset = 2;
		return NULL;
	} else {
		return "unknown directive";
	}
}


