#include <string>
#include <set>
#include <stdarg.h>
#include <execinfo.h>
#include <map>
#include <stdlib.h>
#include <sstream>

#include "syntax.h"

const std::string
get_static_name(const char *prefix, const void *id) {
	static std::map<std::pair<const char *, const void *>, std::string> namemap;
	static int cntr;
	std::ostringstream c;

	c << prefix << cntr;

	/* NULL ids always return unique names without inserting anything into the map. */
	if (id == NULL) {
		cntr++;
		return c.str();
	}

	std::pair<std::map<std::pair<const char *, const void *>, std::string>::iterator, bool> i = namemap.insert(std::pair<std::pair<const char *, const void *>, std::string>(std::pair<const char *, const void *>(prefix, id), c.str()));
	if (i.second)
		cntr++;
	return i.first->second;
}

const std::string &
get_static_name2(const std::string &vname) {
	static std::map<std::string, std::string> namemap;
	static int cntr = 0;

	std::pair<std::map<std::string, std::string>::iterator, bool> ins = namemap.insert(std::pair<std::string, std::string>(vname, ""));

	if (ins.second) {
		ins.first->second = vname;
		std::string::size_type pos = ins.first->second.find("_");

		if (pos != std::string::npos) {
			char cntrbuf[32];

			snprintf(cntrbuf, sizeof(cntrbuf), "%d", cntr++);

			ins.first->second.resize(pos + 1);
			ins.first->second.append(cntrbuf);
		}
	}
	return ins.first->second;
}

std::string
get_global_name(std::string *g) {
	return get_static_name("g_", g);
}

ICEException::ICEException(SyntaxNode *node, const char *fmt, ...)
		: std::exception() {
	va_list ap;
	char tmp[1024];
	void *btbuf[50];
	int btsiz = 50;
	int bti;
	char **bts;
	char *ptr;

	this->node = node;

	va_start(ap, fmt);
	vsnprintf(tmp, sizeof (tmp), fmt, ap);
	va_end(ap);

	ptr = msg + snprintf(msg, sizeof (msg), "Parse error on line %d:\nICE: %s\n", node->sn_lineno, tmp);

	btsiz = backtrace(btbuf, btsiz);
	if (btsiz > 0) {
		bts = backtrace_symbols(btbuf, btsiz);
		for (bti = 1 ; bti < btsiz ; bti++)
			ptr = ptr + snprintf(ptr, sizeof(msg) - (ptr - msg), " bt: %s\n", bts[bti]);
		free(bts);
	}

}

ParseException::ParseException(SyntaxNode *node, const char *fmt, ...)
		: std::exception() {
	va_list ap;
	char tmp[1024];

	this->node = node;

	va_start(ap, fmt);
	vsnprintf(tmp, sizeof (tmp), fmt, ap);
	va_end(ap);

	snprintf(msg, sizeof (msg), "%d: %s", node->sn_lineno, tmp);
}

std::string
CodeSyntaxNode::fold(int *depth) {
	std::string s;
	int i, x = 0;

	switch (sn_indent) {
	case INLINE:
		return sn_code;
	case INLINEUP:
		(*depth)++;
		x = -*depth;
		break;
	case INLINENL:
		x = -*depth;
		break;
	case UP:
		x = -1;
		(*depth)++;
		break;
	case DOWN:
	case DOWNNONL:
		(*depth)--;
		break;
	case DOWNUP:
		x = -1;
		break;
	case NONL:
	case NONE:
		break;
	}

	for (i = 0; i < (*depth) + x; i++)
		s += "\t";

	s += sn_code;
	if (sn_indent != NONL && sn_indent != DOWNNONL)
		s += "\n";

	return s;
}

struct reparent_data {
	BlockSyntaxNode *nblock;
	BlockSyntaxNode *oblock;
};

static SyntaxNode *
do_reparent(SyntaxNode *sn, void *v) {
	struct reparent_data *rd = (struct reparent_data *)v;
	sn->reparent_node(rd->nblock, rd->oblock);
	return sn;
}

void
SyntaxNode::reparent(BlockSyntaxNode *newblock) {
	struct reparent_data rd = { newblock, this->block };

	if (newblock == this->block)
		return;

	this->apply(do_reparent, (void *)&rd);
}

static SyntaxNode *
do_resolve(SyntaxNode *sn, void *v) {
	sn->resolve_node(*static_cast<pp_queue*>(v));
	return sn;
}

void
SyntaxNode::resolve(pp_queue &ppq) {
	this->apply(do_resolve, &ppq);
}

static SyntaxNode *
do_verify_loop(SyntaxNode *sn, void *v) {
	sn->verify_loop_node((BlockSyntaxNode*)v);
	return sn;
}

void
SyntaxNode::verify_loop(BlockSyntaxNode *parent_of) {
	this->apply(do_verify_loop, (void*)parent_of);
}

static SyntaxNode *
do_collapse(SyntaxNode *sn, void *v) {
	SyntaxNode *n = sn->collapse_node();
	int *ndone = (int *)v;
	if (n != sn)
		(*ndone)++;
	return n;
}

SyntaxNode *
SyntaxNode::collapse(int *ndone) {
	return this->apply(do_collapse, (void *)ndone);
}

static SyntaxNode *
do_optimize(SyntaxNode *sn, void *v) {
	return sn->optimize_node();
}

SyntaxNode *
SyntaxNode::optimize(void) {
	return this->apply(do_optimize, NULL);
}

static SyntaxNode *
do_convert(SyntaxNode *sn, void *v) {
	sn->convert_node();
	return sn;
}

void
SyntaxNode::convert(void) {
	this->apply(do_convert, NULL);
}

static SyntaxNode *
do_bind_locals(SyntaxNode *sn, void *v) {
	sn->bind_locals_node();
	return sn;
}

void
SyntaxNode::bind_locals(void) {
	this->apply(do_bind_locals, NULL);
}

SyntaxNode *
ParamsSyntaxNode::apply(SyntaxNode *(*cb)(SyntaxNode *, void *), void *v) {
	for (std::deque<SyntaxNode *>::iterator i = params.begin(); i != params.end(); i++)
		*i = (*i)->apply(cb, v);
	return SyntaxNode::apply(cb, v);
}

void
ParamsSyntaxNode::convert_node(void) {
	for (std::deque<SyntaxNode *>::iterator i = params.begin(); i != params.end(); i++)
		*i = (*i)->convert_to_var();
}

SyntaxNode *
ParamsSyntaxNode::reduce(void) {
	bool first = true;
	NodeSyntaxNode *rn, *sn, *gn = NULL;
	int indent = 0;
	char buf[100];
	std::string gname;

	if (params.empty()) {
		return new CodeSyntaxNode(SNR_NONE, "0, NULL", CodeSyntaxNode::INLINE, true);
	}

	rn = new NodeSyntaxNode(block, NULL, SNR_NONE, sn_lineno);
	if (is_constant()) {
		gname = get_global_name(NULL);
		sn = gn = new NodeSyntaxNode(block, NULL, SNR_NONE, sn_lineno);
		CodeSyntaxNode::code(gn, "static const struct tpvar *const " + gname + "[] = {", CodeSyntaxNode::INLINE, true);
		snprintf(buf, sizeof(buf), "%lu, (const struct tpvar **)%s", (unsigned long)params.size(), gname.c_str());
	} else {
		sn = rn;
		snprintf(buf, sizeof(buf), "%lu, (const struct tpvar *[]) {", (unsigned long)params.size());
	}
	CodeSyntaxNode::code(rn, buf, CodeSyntaxNode::INLINE, true);

	while (!params.empty()) {
		SyntaxNode *n = params.front();
		params.pop_front();

		if (first)
			first = false;
		else
			CodeSyntaxNode::code(sn, ", ", CodeSyntaxNode::INLINE, true);

		n = n->reduce();
		CodeSyntaxNode::code(sn, n->fold(&indent), CodeSyntaxNode::INLINE, n->is_constant());
	}
	CodeSyntaxNode::code(sn, "}", CodeSyntaxNode::INLINE, true);

	if (gn) {
		int indent = 0;
		std::string g = gn->fold(&indent) + ";";
		globals.insert(g);
	}
	delete this;
	return rn->reduce();
}

SyntaxNode *
StrWhiteSyntaxNode::reduce(void) {
	return this;
}

SyntaxNode *
StrNlSyntaxNode::reduce(void) {
	return this;
}

SyntaxNode *
StringSyntaxNode::reduce(void) {
	return this;
}

SyntaxNode *
IntegerSyntaxNode::reduce(void) {
	return this;
}

SyntaxNode *
CodeSyntaxNode::reduce(void) {
	return this;
}

SyntaxNode *
NodeSyntaxNode::apply(SyntaxNode *(*cb)(SyntaxNode *, void *), void *v) {
	std::deque<SyntaxNode*>::iterator n;

	for (n = nodes.begin(); n != nodes.end(); n++) {
		*n = (*n)->apply(cb, v);
	}

	return SyntaxNode::apply(cb, v);
}

SyntaxNode *
NodeSyntaxNode::reduce(void) {
	std::deque<SyntaxNode*>::iterator n;

	for (n = nodes.begin() ; n != nodes.end() ; n++) {
		*n = (*n)->reduce();
	}

	return this;
}

void
VararrSyntaxNode::convert_node(void) {
	/* XXX this is never called */
	std::deque<SyntaxNode*>::iterator n;

	for (n = nodes.begin() ; n != nodes.end() ; n++) {
		*n = (*n)->convert_to_var();
	}
}

SyntaxNode *
VararrSyntaxNode::reduce(void) {
	SyntaxNode *ret;

	if (nodes.size() == 1) {
		ret = nodes.front();
	} else {
		NodeSyntaxNode *sn;
		std::deque<SyntaxNode *>::iterator n;
		char buf[30];

		sn = new NodeSyntaxNode(block, NULL, SNR_VAR, sn_lineno);
		snprintf(buf, sizeof(buf), "&TPV_TPVARR(%lu", (unsigned long)nodes.size());
		CodeSyntaxNode::code(sn, buf, CodeSyntaxNode::INLINE);
		for (n = nodes.begin(); n != nodes.end(); n++) {
			CodeSyntaxNode::code(sn, ", ", CodeSyntaxNode::INLINE);
			sn->add((*n)->reduce());
		}
		CodeSyntaxNode::code(sn, ")", CodeSyntaxNode::INLINE);
		ret = sn;
	}
	nodes.clear();

	delete this;
	return ret;
}

SyntaxNode *
NodeSyntaxNode::collapse_node(void) {
	if (block == NULL)
		ICERR(this, "NodeSyntaxNode::collapse_node: NULL block");
	if (nodes.size() == 1) {
		SyntaxNode *ret = nodes.front();
		nodes.clear();
		delete this;
		return ret;
	}
	return this;
}

/*
 * Merge all other OutSyntaxNode containers we contain into this container.
 * Concatenate all whitespace and strings into simple strings.
 * Then, if we only have one node, return that node instead of a full container.
 */
SyntaxNode *
OutSyntaxNode::collapse_node(void) {
	std::deque<SyntaxNode *> ret;
	std::string str_acc;
	int whitespace = 0;
	int lineno = 0;

	while (!nodes.empty()) {
		SyntaxNode *nn = nodes.front();
		nodes.pop_front();

		if (nn->isempty()) {
			delete nn;
			continue;
		}

		int ws = nn->ws_type();
		if (ws) {
			if (ws > whitespace)
				whitespace = ws;
			lineno = nn->sn_lineno;
			delete nn;
			continue;
		}

		if (whitespace) {
			str_acc += whitespace == 2 ? "\\n" : " ";
			whitespace = 0;
		}
		if (typeid(*nn) == typeid(StringSyntaxNode)) {
			StringSyntaxNode *sn = (StringSyntaxNode *)nn;
			str_acc += sn->sn_str;
			delete sn;
			continue;
		}
		if (str_acc.length() != 0) {
			SyntaxNode *s = new StringSyntaxNode(block, NULL, lineno, str_acc);
			ret.push_back(s);
			str_acc.clear();
		}

		if (typeid(*nn) == typeid(OutSyntaxNode)) {
			OutSyntaxNode *on = (OutSyntaxNode *)nn;
			ret.insert(ret.end(), on->nodes.begin(), on->nodes.end());
			on->nodes.clear();
			delete on;
		} else {
			ret.push_back(nn);
		}
	}
	if (whitespace) {
		str_acc += whitespace == 2 ? "\\n" : " ";
	}
	if (str_acc.length() != 0) {
		SyntaxNode *s = new StringSyntaxNode(block, NULL, lineno, str_acc);
		ret.push_back(s);
		str_acc.clear();
	}
	nodes.insert(nodes.begin(), ret.begin(), ret.end());

	return NodeSyntaxNode::collapse_node();
}

bool
OutSyntaxNode::is_trivial_var(void) {
	/*
	 * Check if it's possible to convert into a vararr.
	 *
	 * We bail if something isn't trivially convertible to VAR or if there's a function call in there (that is
	 * currently INT, STR and VAR).
	 * Also we bail if there's a function call in there. We can put them inside a vararr since a vararr is
	 * assembled with no sequence points between the elements which means that the function call order could change. 
	 */
	for (std::deque<SyntaxNode*>::iterator nit = nodes.begin(); nit != nodes.end(); nit++) {
		SyntaxNode *nn = *nit;

		if (nn->isempty())
			continue;

		if (nn->sn_ret != SNR_VAR && nn->sn_ret != SNR_STR && nn->sn_ret != SNR_INT)
			return false;

		if (typeid(*nn) == typeid(FunCallSyntaxNode))
			return false;
	}
	return true;
}

SyntaxNode *
OutSyntaxNode::convert_to_var(void) {
	VararrSyntaxNode *vararr;

	if (!is_trivial_var())
		ICERR(this, "outsyntaxnode::convert_to_var on non-trivial node");

	vararr = new VararrSyntaxNode(block, sn_lineno);
	for (std::deque<SyntaxNode*>::iterator nit = nodes.begin(); nit != nodes.end(); nit++) {
		SyntaxNode *nn = *nit;

		if (nn->isempty())
			continue;

		vararr->add(nn->convert_to_var());
	}

	nodes.clear();
	delete this;
	return vararr;
}

SyntaxNode *
OutSyntaxNode::reduce(void) {
	/*
	 * We can't get reduced a second time (which is usually legal), since
	 * the call ordering for function calls will be violated. You wouldn't want to violate
	 * call ordering, would you?
	 */
	if (reduced) {
		return NodeSyntaxNode::reduce();
	}
	reduced = true;

	std::deque<SyntaxNode*> newnodes;

	while (!nodes.empty()) {
		SyntaxNode *nn = nodes.front();
		nodes.pop_front();

		if (nn->isempty()) {
			delete nn;
			continue;
		}

		newnodes.push_back(nn);
	}
	nodes = newnodes;

	return NodeSyntaxNode::reduce();
}

SyntaxNode *
DefineSyntaxNode::apply(SyntaxNode *(*cb)(SyntaxNode *, void *), void *v) {
	name = name->apply(cb, v);
	value = value->apply(cb, v);
	return SyntaxNode::apply(cb, v);
}

void
DefineSyntaxNode::convert_node(void) {
	name = name->convert_to_var();
	value = value->convert_to_var();
}

SyntaxNode *
DefineSyntaxNode::reduce(void) {
	std::string key, v;
	int indent = 0;

	name = name->reduce();
	key = name->fold(&indent);

	value = value->reduce();
	v = value->fold(&indent);
	return new CodeSyntaxNode(SNR_NONE, "template_define_var(api, " + key + ", " + v + ");", CodeSyntaxNode::NONE);
}

SyntaxNode *
IncludeSyntaxNode::apply(SyntaxNode *(*cb)(SyntaxNode *, void *), void *v) {
	arg = arg->apply(cb, v);
	return SyntaxNode::apply(cb, v);
}

void
IncludeSyntaxNode::convert_node(void) {
	arg = arg->convert_to_str();
}

SyntaxNode *
IncludeSyntaxNode::reduce(void) {
	int indent = 0;

	arg = arg->reduce();
	std::string tmpl = arg->fold(&indent);

	return new CodeSyntaxNode(SNR_NONE, "call_template_auto(api, " + tmpl + ");", CodeSyntaxNode::NONE);
}

std::deque<SyntaxNode*>::iterator
NodeSyntaxNode::flatten(std::deque<SyntaxNode*> *deque, std::deque<SyntaxNode*>::iterator pos) {
	bool skipinsert;

	if (sn_ret != SNR_NONE && sn_ret != SNR_OUT) {
		int indent = 0;
		pos = deque->insert(pos, new CodeSyntaxNode(sn_ret, this->fold(&indent)));
		skipinsert = true;
	} else {
		skipinsert = false;
	}

	while (!nodes.empty()) {
		SyntaxNode *n = nodes.back();

		nodes.pop_back();
		if (skipinsert)
			delete n;
		else
			pos = deque->insert(pos, n);
	}
	return pos;
}

std::string
StringSyntaxNode::fold(int *indent) {
	return "\"" + escape() + "\"";
}

std::string
IntegerSyntaxNode::fold(int *indent) {
	std::ostringstream n;

	n << val;

	return n.str();
}

/*
 * Flatten and optimize a node into a CODE node.
 */
std::string
NodeSyntaxNode::fold(int *indent) {
	std::deque<SyntaxNode*>::iterator n, w;

	/*
	 * 1. Flatten
	 */
	n = nodes.begin();
	while (n != nodes.end()) {
		n = (*n)->flatten(&nodes, n + 1);
	}

	/*
	 * 2. Clean.
	 */
	n = nodes.begin();
	while (n != nodes.end()) {
		if ((*n)->isempty()) {
			delete *n;
			n = nodes.erase(n);
		} else {
			n++;
		}
	}

	/*
	 * 3. Make code, not strings.
	 */
	std::string s;
	while (!nodes.empty()) {
		SyntaxNode *sn = nodes.front();

		nodes.pop_front();

		s += this->fold_node(sn, indent);
		delete sn;
	}

	return s;
}

std::string
NodeSyntaxNode::fold_node(SyntaxNode *sn, int *indent) {
	/*
	 * At this point only one types of node can be SNR_OUT - reduced OutSyntaxNodes.
	 */

	if (sn_ret != SNR_OUT)
		return sn->fold(indent);

	const char *pre;
	const char *post;

	switch (sn->sn_ret) {
	case SNR_STR:
		pre = "template_output(api, ";
		post = ");\n";
		break;
	case SNR_INT:
		pre = "template_output_int(api, ";
		post = ");\n";
		break;
	case SNR_VAR:
		pre = "template_output_var(api, ";
		post = ");\n";
		break;
	default:
		return sn->fold(indent);
	}

	std::string code = sn->fold(indent);
	code.insert(0, pre);
	code.append(post);
	code.insert(0, *indent, '\t');
	return code;
}

/* XXX - garbage collect */
std::string
OutSyntaxNode::fold_node(SyntaxNode *sn, int *indent) {
	return NodeSyntaxNode::fold_node(sn, indent);
}

SyntaxNode *
EmptyVarSyntaxNode::reduce(void) {
	delete this;
	return new CodeSyntaxNode(SNR_VAR, "&TPV_STRING(\"\")", CodeSyntaxNode::INLINE);
}
