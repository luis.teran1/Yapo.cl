#include <getopt.h>
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <FlexLexer.h>
#include <string.h>
#include <unistd.h> /* for unlink */

#include "syntax.h"
#include "vtree_syntax_nodes.hh"
#include "lang.hh"
#include "builtin.h"
#include "lang_types.h"

extern int yydebug;
int verbose = 0;

int yyparse(TemplateSyntaxNode **result);

char *template_name;
extern FlexLexer *lexer;
int global_parse_error = 0;
const char *input_file;

pcre *StringSyntaxNode::compiled_id_regex = StringSyntaxNode::init_regex("^[a-zA-Z_][a-zA-Z0-9_]*$");

std::set<std::string> includes;
std::set<std::string> globals;
std::deque<std::string> global_funcs;
std::set<std::string> clear_cache;

void
yyerror(TemplateSyntaxNode **sn, std::string s) {
	char str[30];

	if (global_parse_error)
		return;
	std::cin.getline(str, sizeof(str) - 1);
	if (!str[0] || str[0] == '\n')
		strcpy(str, "end of line");
	std::cerr << (input_file ? input_file : template_name) << ":" << lexer->lineno() << ": error: " << s << " near " << str << std::endl;
	global_parse_error = 1;
}

static void
generate_file(TemplateSyntaxNode *root) {
	std::set<local_var *>::iterator viter;
	std::string code;
	TemplateSyntaxNode *sn = root;
	int ind = 0;
	int collapsed;

	if (verbose) {
		fprintf(stderr, "Initial syntax tree:\n");
		sn->dump(1);
	}

	/*
	 * Resolve unrolls loop variables and branch counters and
	 * anchors them in the blocks where they belong. It doesn't
	 * create locals and globals.
	 */
	pp_queue ppqueue;
	sn->resolve(ppqueue);
	while (!ppqueue.empty()) {
		ppqueue.top().fun();
		ppqueue.pop();
	}
	if (verbose) {
		fprintf(stderr, "After resolve:\n");
		sn->dump(1);
	}

	/*
	 * Collapse containers and strings.
	 * We repeat this until there are nothing more to collapse.
	 */
	do {
		collapsed = 0;
		sn->collapse(&collapsed);
	} while (collapsed);
	if (verbose) {
		fprintf(stderr, "After collapse:\n");
		sn->dump(1);
	}

	/*
	 * Do optimizations, e.g. builtin filters.
	 */
	sn->optimize();
	if (verbose) {
		fprintf(stderr, "After optimize:\n");
		sn->dump(1);
	}

	/*
	 * Convert node types
	 */
	sn->convert();
	if (verbose) {
		fprintf(stderr, "After convert:\n");
		sn->dump(1);
	}

	/*
	 * Bind locals to blocks.
	 */
	sn->bind_locals();
	if (verbose) {
		fprintf(stderr, "After bind_locals:\n");
		sn->dump(1);
	}

	/*
	 * Reduce turns specific syntax nodes into containers, code
	 * and strings.
	 */
	sn->reduce();
	if (verbose) {
		fprintf(stderr, "After reduce:\n");
		sn->dump(1);
	}
	/*
	 * Fold flattens containers, merges strings and whitespace and
	 * returns the final code.
	 */
	code = sn->fold(&ind);

	std::cout << "#include \"bpapi.h\"" << std::endl;
	std::cout << "#include \"templateparse_internal.h\"" << std::endl;

	if (includes.size()) {
		for (std::set<std::string>::iterator i = includes.begin() ; i != includes.end() ; i++ ) {
			std::cout << "#include \"" << *i << "\"\n";
		}
	}

	std::cout << "\n";

	sn->generate_vars();

	for (std::set<std::string>::iterator i = globals.begin(); i != globals.end(); i++) {
		std::cout << *i << "\n";
	}
	globals.clear();

	for (std::deque<std::string>::iterator i = global_funcs.begin(); i != global_funcs.end(); i++) {
		std::cout << *i << "\n";
	}
	global_funcs.clear();

	sn->generate_main(code);

	sn->generate_clear_cache();
	sn->generate_linker();

	delete sn;
}

int
main(int argc, char **argv) {
	std::ofstream of;
	int opt;
	char *progname = argv[0];
	std::ifstream inf;
	TemplateSyntaxNode *res;
	std::streambuf *coutbuf = NULL, *cinbuf = NULL;

	const struct option longopts[] = {
		{ "debug", no_argument, NULL, 'd' },
		{ "verbose", no_argument, NULL, 'v' },
		{ 0, 0, 0, 0 }
	};

	yydebug = 0;
	while ((opt = getopt_long(argc, argv, "dv", longopts, NULL)) != -1) {
		switch (opt) {
		case 'v':
			verbose = 1;
			break;
		case 'd':
			yydebug = 1;
			break;
		default:
			fprintf(stderr, "Usage: %s template_name [output_file] [input_file]\n", progname);
			global_parse_error = 1;
			goto fail;
		}
	}

	argc -= optind;
	argv += optind;

	if(argc > 3) {
		fprintf(stderr, "Usage: %s template_name [output_file] [input_file]\n", progname);
		global_parse_error = 1;
		goto fail;
	}

	template_name = argv[0];
	if (argc > 1) {
		of.open(argv[1]);
		if (!of) {		
			std::cerr << "Failed to open " << argv[1] << " for writing" << std::endl;
			global_parse_error = 1;
			goto fail;
		}

		coutbuf = std::cout.rdbuf(of.rdbuf());
	}

	if (argc > 2) {
		inf.open(argv[2]);
		if (!inf) {
			std::cerr << "Failed to open " << argv[2] << " for reading" << std::endl;
			global_parse_error = 1;
			goto fail;
		}

		cinbuf = std::cin.rdbuf(inf.rdbuf());
		input_file = argv[2];
	}

#if 0
	/* .html templates default to eating white space */
	if (strlen(argv[2]) > 10 && strcmp(&argv[2][strlen(argv[2]) - 10], ".html.tmpl") == 0) {
		extern int eatspace;
		eatspace = 1;
	}
#endif

	try {
		if (yyparse(&res) == 0)
			generate_file(res);
	} catch (ParseException &err) {
		std::cerr << (input_file ? input_file : template_name) << ":" << err.what() << "\n";
		global_parse_error = 1;
	}

fail:
	if (of) {
		of.close();
	}

	if (global_parse_error && argv[1]) {
		unlink(argv[1]);
	}

	StringSyntaxNode::deinit_regex();

	if (coutbuf)
		std::cout.rdbuf(coutbuf);
	if (cinbuf)
		std::cin.rdbuf(cinbuf);
	return global_parse_error;
}
