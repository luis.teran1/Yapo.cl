
#include "syntax.h"

SyntaxNode *
SyntaxNode::getvar(void) {
	int indent = 0;

	if (sn_ret == SNR_STR) {
		return new CodeSyntaxNode(SNR_STR, "template_getvar(api, " + this->reduce()->fold(&indent) + ")");
	} else {
		return new CodeSyntaxNode(SNR_STR, "template_getvar_var(api, " + this->reduce()->fold(&indent) + ")");
	}
}

SyntaxNode *
SyntaxNode::getlist(SyntaxNode *index) {
	std::string ind;
	int indent = 0;

	ind = index->reduce()->fold(&indent);
	if (sn_ret == SNR_STR) {
		return new CodeSyntaxNode(SNR_STR, "template_getlist(api, " + this->reduce()->fold(&indent) + ", " + ind + ")");
	} else {
		return new CodeSyntaxNode(SNR_STR, "template_getlist_var(api, " + this->reduce()->fold(&indent) + ", " + ind + ")");
	}
}

SyntaxNode *
SyntaxNode::fetchlist(const std::string &var) {
	int indent = 0;

	if (sn_ret == SNR_STR) {
		return new CodeSyntaxNode(SNR_NONE, "template_fetchlist(api, " + this->reduce()->fold(&indent) + ", " + var + ");", CodeSyntaxNode::NONE);
	} else {
		return new CodeSyntaxNode(SNR_NONE, "template_fetchlist_var(api, " + this->reduce()->fold(&indent) + ", " + var + ");", CodeSyntaxNode::NONE);
	}
}

SyntaxNode *
SyntaxNode::listlen(void) {
	int indent = 0;

	if (sn_ret == SNR_STR) {
		return new CodeSyntaxNode(SNR_INT, "template_listlen(api, " + this->reduce()->fold(&indent) + ")");
	} else {
		return new CodeSyntaxNode(SNR_INT, "template_listlen_var(api, " + this->reduce()->fold(&indent) + ")");
	}
}

SyntaxNode *
SyntaxNode::globlen(void) {
	int indent = 0;

	if (sn_ret == SNR_STR) {
		return new CodeSyntaxNode(SNR_INT, "bpapi_keyglob_len(api, " + this->reduce()->fold(&indent) + ")");
	} else {
		return new CodeSyntaxNode(SNR_INT, "bpapi_keyglob_len_var(api, " + this->reduce()->fold(&indent) + ")");
	}
}

SyntaxNode *
StringSyntaxNode::getvar(void) {
	SyntaxNode* ret = NULL;
	if (this->isidentifier()) {
		ret = new CodeSyntaxNode(SNR_STR, "__" + sn_str + "_getvar(&v, api)");
	} else {
		return SyntaxNode::getvar();
	}
	delete this;
	return ret;
}

SyntaxNode *
StringSyntaxNode::getlist(SyntaxNode *index) {
	int indent = 0;
	SyntaxNode* ret = NULL;
	if (this->isidentifier()) {
		ret = new CodeSyntaxNode(SNR_STR, "__" + sn_str + "_getlist(&v, api, " + index->reduce()->fold(&indent) + ")");
	} else {
		return SyntaxNode::getlist(index);
	}
	delete this;
	return ret;
}

SyntaxNode *
StringSyntaxNode::fetchlist(const std::string &var) {
	SyntaxNode* ret = NULL;
	if (this->isidentifier()) {
		ret = new CodeSyntaxNode(SNR_NONE, "__" + sn_str + "_fetchlist(&v, api, " + var + ");", CodeSyntaxNode::NONE);
	} else {
		return SyntaxNode::fetchlist(var);
	}
	delete this;
	return ret;
}

SyntaxNode *
StringSyntaxNode::listlen(void) {
	SyntaxNode* ret = NULL;
	if (this->isidentifier()) {
		ret = new CodeSyntaxNode(SNR_INT, "__" + sn_str + "_listlen(&v, api)");
	} else {
		return SyntaxNode::listlen();
	}
	delete this;
	return ret;
}

SyntaxNode *
GlobLenSyntaxNode::apply(SyntaxNode *(*cb)(SyntaxNode *, void *), void *v) {
	name = name->apply(cb, v);
	return SyntaxNode::apply(cb, v);
}

void
GlobLenSyntaxNode::convert_node(void) {
	if (name->sn_ret != SNR_STR)
		name = name->convert_to_var();
}

SyntaxNode *
GlobLenSyntaxNode::reduce(void) {
	SyntaxNode *ret = name->globlen();
	name = NULL;
	delete this;
	return ret->reduce();
}

SyntaxNode *
ListLenSyntaxNode::apply(SyntaxNode *(*cb)(SyntaxNode *, void *), void *v) {
	name = name->apply(cb, v);
	return SyntaxNode::apply(cb, v);
}

void
ListLenSyntaxNode::convert_node(void) {
	if (name->sn_ret != SNR_STR)
		name = name->convert_to_var();
}

SyntaxNode *
ListLenSyntaxNode::reduce(void) {
	SyntaxNode *ret = name->listlen();
	name = NULL;
	delete this;
	return ret->reduce();
}
