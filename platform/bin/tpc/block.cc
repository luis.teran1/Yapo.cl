
#include <iostream>
#include <sstream>
#include <stdlib.h>

#include "syntax.h"

SyntaxNode *
FilterSyntaxNode::apply(SyntaxNode *(*cb)(SyntaxNode *, void *), void *v) {
	if (args)
		args->apply(cb, v);
	return SyntaxNode::apply(cb, v);
}

SyntaxNode *
BlockSyntaxNode::apply(SyntaxNode *(*cb)(SyntaxNode *, void *), void *v) {
	context = ctxLiters;
	for (std::set<InitLoopSyntaxNode *>::iterator liter = loops.begin(); liter != loops.end(); liter++ ) {
		(*liter)->apply(cb, v);
	}

	/*
	 * We want to apply to content after initloops because in case we do resolve, content will create loop iterators,
	 * which error out on resolve (they don't really need to, but on the other hand it doesn't hurt if apply is done
	 * this way).
	 */
	context = ctxContent;
	content = content->apply(cb, v);

	context = ctxFilters;
	for (std::deque<FilterSyntaxNode *>::iterator f = filters.begin(); f != filters.end(); f++) {
		(*f)->apply(cb, v);
	}
	context = ctxInvalid;
	return SyntaxNode::apply(cb, v);
}

bool
BlockSyntaxNode::has_loop_filter(void) {
	std::deque<FilterSyntaxNode*>::iterator f;

	for (f = filters.begin() ; f != filters.end() ; f++) {
		if ((*f)->isloop)
			return true;
	}
	return false;
}

SyntaxNode *
BlockSyntaxNode::optimize_node(void) {
	if (sn_ret == SNR_OUT && filters.size() == 1) {
		FilterSyntaxNode *filter = filters.front();

		/* Attempt to optimize single argument defines. */
		if (filter->name == "define") {
			if (filter->args->params.size() == 1) {
				SyntaxNode *name = filter->args->params.front();

				filters.clear();

				name->reparent(block);

				return new DefineSyntaxNode(block, sn_lineno, name, this);
			}
		} else if (filter->name == "include") {
			if (filter->args->params.size() == 1 && (!content || content->isempty())) {
				SyntaxNode *arg = filter->args->params.front();

				filters.clear();

				arg->reparent(block);

				return new IncludeSyntaxNode(block, sn_lineno, arg);
			}
		}
	}

	return this;
}

SyntaxNode *
BlockSyntaxNode::collapse_node(void) {
	if (sn_ret != SNR_VAR && (content->sn_ret == SNR_VAR || content->sn_ret == SNR_INT || content->sn_ret == SNR_STR || content->sn_ret == SNR_OUT) &&
	    loops.empty() && branch_counts.empty() && filters.empty() && !have_counters) {
		return merge_parent();
	}
	return this;
}

SyntaxNode *
BlockSyntaxNode::reduce(void) {
	if (sn_ret == SNR_VAR)
		return reduce_to_function();
	else
		return reduce_to_block();
}

SyntaxNode *
BlockSyntaxNode::reduce_to_block(void) {
	NodeSyntaxNode *sn;
	std::deque<FilterSyntaxNode*>::iterator f;
	SyntaxNode *cleanup;
	SyntaxNode *ret;

	content = content->reduce();

	sn = new NodeSyntaxNode(block, NULL, SNR_NONE, sn_lineno);
	cleanup = new NodeSyntaxNode(block, NULL, SNR_NONE, sn_lineno);

	CodeSyntaxNode::code(sn, "{", CodeSyntaxNode::UP);

	if (!filters.empty()) {
		CodeSyntaxNode::code(sn, "int needs_flush = 0;");

		for (std::set<local_var *>::iterator i = filter_locals.begin() ; i != filter_locals.end() ; i++) {
			CodeSyntaxNode::code(sn, (*i)->decl);
		}

		for (f = filters.begin() ; f != filters.end() ; f++) {
			int indent = 0;
			std::string n = ((*f)->isloop ? "__TMPL_LOOP_FILTER(" : "__TMPL_FILTER(") + (*f)->name + ")";
			std::string args = (*f)->args ? (*f)->args->reduce()->fold(&indent) : "0, NULL";
			(*f)->args = NULL;

			if ((*f)->isloop && filters.size() > 1) {
				PERR((*f), "Can't mix loop filters currently.");
			}

			globals.insert("extern const struct bpfilter " + n + " TEMPLATE_VISIBILITY;");
			CodeSyntaxNode::code(sn, "struct bpapi napi;");
			CodeSyntaxNode::code(sn, "TMPL_FILTER_INIT(" + n + ", &napi, api, &needs_flush);");
			CodeSyntaxNode::code(sn, "if (!" + n + ".init(&" + n + ", &napi, " + args + ")) {", CodeSyntaxNode::UP);
			CodeSyntaxNode::code(sn, "struct bpapi *api = &napi;");

			if ((*f)->isloop) {
				std::string cnt = loop_cnt_name();
				std::string api_ptr = api_ptr_name();

				CodeSyntaxNode::code(sn, "int " + cnt + " = 0;");
				CodeSyntaxNode::code(sn, "struct bpapi * const " + api_ptr + " UNUSED = api;");
				CodeSyntaxNode::code(sn, "for ( ; " + n + ".loop(api) ; " + cnt + "++) {", CodeSyntaxNode::UP);
			}
		}

		CodeSyntaxNode::code(sn, "COPY_VARS(&v);");
	}

	for (std::set<BranchCountInitializerSyntaxNode *>::iterator i = branch_counts.begin(); i != branch_counts.end(); i++) {
		/* Do not reduce here, we need to let the reduce further down take care of it since we have children that depend on us presisting. XXX */
		sn->add((*i));
	}
	branch_counts.clear();

	if (!loops.empty()) {
		std::set<std::string> lists;
		std::string lc;
		std::string cnt = loop_cnt_name();
		std::string len = loop_len_name();

		CodeSyntaxNode::code(sn, "int " + cnt + ";");
		CodeSyntaxNode::code(sn, "int " + len + " = 0;");

		for (std::set<local_var *>::iterator i = liter_locals.begin() ; i != liter_locals.end() ; i++) {
			CodeSyntaxNode::code(sn, (*i)->decl);
		}

		for (std::set<InitLoopSyntaxNode *>::iterator liter = loops.begin() ; liter != loops.end() ; liter++ ) {
			/* The cleanup has to be added before reducing the node because reduce kills the node. */
			cleanup->add((*liter)->cleanup());
			sn->add((*liter)->reduce());
		}

		CodeSyntaxNode::code(sn, "for (" + cnt + " = 0 ; " + cnt + " < " + len + " ; " + cnt + "++) {", CodeSyntaxNode::UP);	/* loop */
	}

	for (std::set<local_var *>::iterator i = content_locals.begin() ; i != content_locals.end() ; i++) {
		CodeSyntaxNode::code(sn, (*i)->decl);
	}

	sn->add(content);
	content = NULL;

	for (std::set<local_var *>::iterator i = content_locals.begin() ; i != content_locals.end() ; i++) {
		if ((*i)->cleanup.length())				/* XXX - nodeify */
			CodeSyntaxNode::code(sn, (*i)->cleanup + ";");
		delete *i;
	}

	if (!filters.empty()) {
		CodeSyntaxNode::code(sn, "if (needs_flush) {", CodeSyntaxNode::UP);
		CodeSyntaxNode::code(sn, "bpapi_flush(api);");
		CodeSyntaxNode::code(sn, "}", CodeSyntaxNode::DOWN);
	}

	if (!loops.empty()) {
		CodeSyntaxNode::code(sn, "}", CodeSyntaxNode::DOWN);		/* loop */

		for (std::set<local_var *>::iterator i = liter_locals.begin() ; i != liter_locals.end() ; i++) {
			if ((*i)->cleanup.length())				/* XXX - nodeify */
				CodeSyntaxNode::code(sn, (*i)->cleanup + ";");
			delete *i;
		}
	}

	if (!cleanup->isempty()) {
		sn->add(cleanup->reduce());
	} else {
		delete cleanup;
	}

	while (!filters.empty()) {
		FilterSyntaxNode *filter = filters.back();
		std::string n = (filter->isloop ? "__TMPL_LOOP_FILTER(" : "__TMPL_FILTER(") + filter->name + ")";

		filters.pop_back();

		if (filter->isloop)
			CodeSyntaxNode::code(sn, "}", CodeSyntaxNode::DOWN);
		CodeSyntaxNode::code(sn, "}", CodeSyntaxNode::DOWN);
		CodeSyntaxNode::code(sn, "deinit_filter(&" + n + ", &napi, api);");

		delete filter;
	}
	for (std::set<local_var *>::iterator i = filter_locals.begin() ; i != filter_locals.end() ; i++) {
		if ((*i)->cleanup.length())				/* XXX - nodeify */
			CodeSyntaxNode::code(sn, (*i)->cleanup + ";");
		delete *i;
	}

	CodeSyntaxNode::code(sn, "}", CodeSyntaxNode::DOWN);

	ret = sn->reduce();
	delete this;
	return ret;
}

std::string
BlockSyntaxNode::api_ptr_name(void) {
	return get_static_name("api_", this);
}

std::string
BlockSyntaxNode::loop_len_name(void) {
	if (has_loop_filter()) {
		std::deque<FilterSyntaxNode*>::iterator f;

		for (f = filters.begin() ; f != filters.end() ; f++) {
			if ((*f)->isloop) {
				std::string n = "__TMPL_LOOP_FILTER("  + (*f)->name + ")";
				return n + ".loop_length(" + api_ptr_name() + ")";
			}
		}
	}
	return get_static_name("ll_", this);
}

std::string
BlockSyntaxNode::outer_loop_len_decl(std::string *name) {
	if (has_loop_filter()) {
		std::deque<FilterSyntaxNode*>::iterator f;

		for (f = filters.begin() ; f != filters.end() ; f++) {
			if ((*f)->isloop) {
				if (name)
					*name = api_ptr_name();
				return "struct bpapi *" + api_ptr_name();
			}
		}
	}
	if (name)
		*name = get_static_name("ll_", this);
	return "int " + get_static_name("ll_", this);
}

std::string
BlockSyntaxNode::loop_cnt_name(void) {
	return get_static_name("lc_", this);
}

void
BlockSyntaxNode::insert_local(local_var *l) {
	switch (context) {
	case ctxLiters:
		liter_locals.insert(l);
		break;
	case ctxContent:
		content_locals.insert(l);
		break;
	case ctxFilters:
		filter_locals.insert(l);
		break;
	default:
	case ctxInvalid:
		ICERR(this, "BlockSyntaxNode::insert_local: invalid context");
	}
}

void
BlockSyntaxNode::dump(int indent) {
	SyntaxNode::dump(indent);
	std::deque<FilterSyntaxNode*>::iterator f;
	for (f = filters.begin(); f != filters.end(); f++) {
		FilterSyntaxNode *filt = *f;
		filt->dump(indent + 1);
	}
	std::set<InitLoopSyntaxNode *>::iterator li;
	for (li = loops.begin(); li != loops.end(); li++) {
		InitLoopSyntaxNode *l = *li;

		l->dump(indent + 1);
	}
	fprintf(stderr, "%*s content:\n", indent * 3, "");
	content->dump(indent + 1);
}


SyntaxNode *
BranchSyntaxNode::apply(SyntaxNode *(*cb)(SyntaxNode *, void *), void *v) {
	content = content->apply(cb, v);
	return SyntaxNode::apply(cb, v);
}

SyntaxNode *
BranchSyntaxNode::collapse_node(void) {
	if (branch_count_incrs.empty()) {
		SyntaxNode *n = content;
		content = NULL;
		delete this;
		return n;
	}
	return this;
}

SyntaxNode *
BranchSyntaxNode::reduce(void) {
	NodeSyntaxNode *n = new OutSyntaxNode(NULL, sn_lineno);

	n->add(content->reduce());
	content = NULL;
	for (std::set<BranchCountInitializerSyntaxNode *>::iterator i = branch_count_incrs.begin(); i != branch_count_incrs.end(); i++) {
		CodeSyntaxNode::code(n, "(*" + (*i)->branch_count_name() + ")++;");
	}
	branch_count_incrs.clear();
	delete this;
	return n;
}

SyntaxNode *
BlockSyntaxNode::merge_parent(void) {
	SyntaxNode *ret;

	if (!content_locals.empty())
		ICERR(this, "merge_parent with locals");

	ret = content;
	content = NULL;
	ret->reparent(block);
	delete this;

	return ret;
}

SyntaxNode *
BlockSyntaxNode::convert_to_var(void) {
	if (loops.empty() && branch_counts.empty() && filters.empty()) {
		if (content->isempty()) {
			int ln = sn_lineno;
			BlockSyntaxNode *b = block;
			delete this;
			return new EmptyVarSyntaxNode(b, ln);
		}
		if (content->is_trivial_var()) {
			content = content->convert_to_var();
			return merge_parent();
		}
	}
	sn_ret = SNR_VAR;
	return this;
}

SyntaxNode *
BlockSyntaxNode::reduce_to_function(void) {
	int indent = 1;
	OutSyntaxNode *fn;

	content = content->reduce();

	std::string farg;
	std::string vp;
	std::string n = get_global_name(NULL);
	fn = new OutSyntaxNode(NULL, sn_lineno);
	std::string gt = "struct " + n + "_args {\n\tstruct bpapi *api;\n\tvoid *vp;\n";
	farg = "&(struct " + n + "_args){ api, &v";
	CodeSyntaxNode::code(fn, "const struct " + n + "_args *args = vp;");
	CodeSyntaxNode::code(fn, "struct bpapi *const api = args->api;");

	if (!outers.empty()) {
		for (std::set<InitLoopSyntaxNode *>::iterator oiter = outers.begin(); oiter != outers.end(); oiter++) {
			std::string var = (*oiter)->loop_var_name();
			gt += "\tstruct bpapi_loop_var *" + var + ";\n";
			farg += ", " + var;

			CodeSyntaxNode::code(fn, "struct bpapi_loop_var * const " + var + " UNUSED = args->" + var + ";");
		}
	}
	if (!outer_loops.empty()) {
		for (std::set<BlockSyntaxNode *>::iterator i = outer_loops.begin(); i != outer_loops.end(); i++) {
			std::string cnt = (*i)->loop_cnt_name();
			std::string len;
			std::string lendecl = (*i)->outer_loop_len_decl(&len);

			gt += "\tint " + cnt + ";\n";
			gt += "\t" + lendecl + ";\n";
			farg += ", " + cnt + ", " + len;
			CodeSyntaxNode::code(fn, "int " + cnt + " UNUSED = args->" + cnt + ";");
			CodeSyntaxNode::code(fn, lendecl + " UNUSED = args->" + len + ";");
		}
	}
	if (!bc_outers.empty()) {
		for (std::set<BranchCountInitializerSyntaxNode *>::iterator biter = bc_outers.begin(); biter != bc_outers.end(); biter++) {
			std::string bcn = (*biter)->branch_count_name();
			gt += "\tint *" + bcn + ";\n";
			farg += ", " + bcn;
			CodeSyntaxNode::code(fn, "int *" + bcn + " = args->" + bcn + ";");
		}
	}

	gt += "};";
	farg += " }";
	globals.insert(gt);

	CodeSyntaxNode::code(fn, "enum bpcacheable cacheable UNUSED = BPCACHE_CAN;");
	CodeSyntaxNode::code(fn, "COPY_VARS(args->vp);");
	CodeSyntaxNode::code(fn, "struct bpapi napi;");
	CodeSyntaxNode::code(fn, "struct buf_string *buf = buf_output_init(&napi.ochain);");
	CodeSyntaxNode::code(fn, "BPAPI_COPY(&napi, api, 0, 1, 1);");
	CodeSyntaxNode::code(fn, "{", CodeSyntaxNode::UP);
	CodeSyntaxNode::code(fn, "struct bpapi *api = &napi;");

	fn->add(this->reduce_to_block());

	CodeSyntaxNode::code(fn, "}", CodeSyntaxNode::DOWN);
	CodeSyntaxNode::code(fn, "char *res = buf->buf;");
	CodeSyntaxNode::code(fn, "*outlen = buf->pos;");
	CodeSyntaxNode::code(fn, "buf->buf = NULL;");
	CodeSyntaxNode::code(fn, "bps_free(&napi.ochain);");
	CodeSyntaxNode::code(fn, "return res;");

	global_funcs.push_back("static char *\n" + n + "(void *vp, int *outlen) {\n" + fn->fold(&indent) + "}\n");

	includes.insert("ctemplates.h");

	CodeSyntaxNode *sn = new CodeSyntaxNode(SNR_VAR, "&TPV_CODE(" + n + ", (" + farg + "))", CodeSyntaxNode::INLINE);

	return sn;
}

InitLoopSyntaxNode *
BlockSyntaxNode::insert_loop(InitLoopSyntaxNode *loop) {
	if (has_loop_filter())
		PERR(loop, "Can't mix loops and loop filters currently.");
	loop->reparent(this);
	std::pair<std::set<InitLoopSyntaxNode *>::iterator, bool> res = loops.insert(loop);
	if (!res.second)
		delete loop;
	return *res.first;
}

void
BlockSyntaxNode::insert_outer(InitLoopSyntaxNode *var) {
	outers.insert(var);
}

void
BlockSyntaxNode::insert_outer_loop(BlockSyntaxNode *var) {
	outer_loops.insert(var);
}

BranchCountInitializerSyntaxNode *
BlockSyntaxNode::insert_bc(BranchCountInitializerSyntaxNode *var) {
	std::pair<std::set<BranchCountInitializerSyntaxNode *>::iterator, bool> res = branch_counts.insert(var);
	if (!res.second)
		delete var;
	return *res.first;
}

void
BlockSyntaxNode::insert_bc_outer(BranchCountInitializerSyntaxNode *var) {
	bc_outers.insert(var);
}

BlockSyntaxNode *
BlockSyntaxNode::anchor(int level)
{
	BlockSyntaxNode *b = this;

	while (1) {
		while (b && b->context == BlockSyntaxNode::ctxFilters)
			b = b->block;
		if (!b || level-- == 0)
			break;
		b = b->block;
	}
	return b;
}

bool
BlockSyntaxNode::has_stree_filter()
{
	for (auto &f : filters)
		if (f->name == "stree")
			return true;
	if (!block)
		return false;
	return block->has_stree_filter();
}

int
BlockSyntaxNode::depth()
{
	int d = 0;
	for (auto b = block ; b ; b = b->block)
		d++;
	return d;
}
