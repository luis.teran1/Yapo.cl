#include "macros.h"
#include "syntax.h"

SyntaxNode *
PVarSyntaxNode::apply(SyntaxNode *(*cb)(SyntaxNode *, void *), void *v) {
	name = name->apply(cb, v);
	if (index)
		index = index->apply(cb, v);
	return SyntaxNode::apply(cb, v);
}

void
PVarSyntaxNode::convert_node(void) {
	if (name && name->sn_ret != SNR_STR)
		name = name->convert_to_var();
	if (index)
		index = index->convert_to_int();
}

SyntaxNode *
PVarSyntaxNode::reduce(void) {
	SyntaxNode *sn;

	if (index) {
		sn = name->getlist(index);
		index = NULL;
	} else {
		sn = name->getvar();
	}
	name = NULL;

	delete this;
	return sn->reduce();
}

void
BranchCountSyntaxNode::resolve_node(pp_queue &ppq) {
	BranchSyntaxNode *b, *branch_anchor, *branch_incr;
	int i;

	if (depth > level)
		PERR(this, "Branch counter depth (%d) exceeds level (%d)", depth, level);

	for (i = 0, branch_anchor = branch, branch_incr = branch; i < level; i++) {
		branch_anchor = branch_anchor->parent_branch;
		if (i < level - depth)
			branch_incr = branch_incr->parent_branch;
	}
	if (branch_anchor == NULL)
		PERR(this, "Branch counter without branch (too deep?)");

	initializer = branch_anchor->block->insert_bc(new BranchCountInitializerSyntaxNode(block, sn_lineno, branch_anchor, branch_incr));
	branch_incr->branch_count_incrs.insert(initializer);

	for (b = branch; b != branch_anchor; b = b->parent_branch) {
		b->block->insert_bc_outer(initializer);
	}
}

SyntaxNode *
BranchCountSyntaxNode::reduce(void) {
	SyntaxNode *ret = new CodeSyntaxNode(SNR_INT, "*" + initializer->branch_count_name());
	delete this;
	return ret;
}

SyntaxNode *
BranchCountInitializerSyntaxNode::reduce(void) {
	SyntaxNode *ret = new CodeSyntaxNode(SNR_NONE, "int " + branch_count_name() + "_storage = 0, * const " + branch_count_name() + " = & " + branch_count_name() + "_storage;", CodeSyntaxNode::NONE);
	delete this;
	return ret;
}

std::string
BranchCountInitializerSyntaxNode::branch_count_name(void) {
	return get_static_name("bc_", this);
}

SyntaxNode *
ArithSyntaxNode::apply(SyntaxNode *(*cb)(SyntaxNode *, void *), void *v) {
	l = l->apply(cb, v);
	r = r->apply(cb, v);
	return SyntaxNode::apply(cb, v);
}

void
ArithSyntaxNode::convert_node(void) {
	l = l->convert_to_int();
	r = r->convert_to_int();
}

SyntaxNode *
ArithSyntaxNode::reduce(void) {
	int indent = 0;
	std::string ls = l->reduce()->fold(&indent);
	std::string rs = r->reduce()->fold(&indent);
	const char *oper = " if you read this your compiler is stupid or you are looking at the code ";	/* unhandled enumeration in switch doesn't prevent gcc from warning about uninitialized */
	SyntaxNode *ret;

	l = r = NULL;

	switch (op) {
	case SNA_ADD:
		oper = " + ";
		break;
	case SNA_SUB:
		oper = " - ";
		break;
	case SNA_DIV:
		oper = " / ";
		break;
	case SNA_MUL:
		oper = " * ";
		break;
	case SNA_MOD:
		oper = " % ";
		break;
	}

	ret = new CodeSyntaxNode(SNR_INT, "(" + ls + oper + rs + ")");
	delete this;
	return ret;
}

SyntaxNode *
FunCallSyntaxNode::apply(SyntaxNode *(*cb)(SyntaxNode *, void *), void *v) {
	if (params)
		params = params->apply(cb, v);
	return SyntaxNode::apply(cb, v);
}

void
FunCallSyntaxNode::resolve_node(pp_queue &ppq) {
	globals.insert("extern const struct templf __TMPL_FUNCTION(" + name + ") TEMPLATE_VISIBILITY;");
}

void
FunCallSyntaxNode::bind_locals_node(void) {
	if (res_local)
		return;

	res_local = new local_var();
	std::string ln = res_local->get_name();
	res_local->decl = "struct tpvar " + ln + " = { .type = TPV_STRING };";
	res_local->cleanup = "if (" + ln + ".type == TPV_DYNSTR) free((char*)" + ln + ".v.str);";
	block->insert_local(res_local);
}

SyntaxNode *
FunCallSyntaxNode::reduce(void) {
	SyntaxNode *ret;
	std::string ln = res_local->get_name();

	if (params) {
		int indent = 0;
		ret = new CodeSyntaxNode(SNR_VAR, "__TMPL_FUNCTION(" + name + ").template_func(api, &" + ln + ", &cacheable, " + params->reduce()->fold(&indent) + ")");
		params = NULL;
	} else {
		ret = new CodeSyntaxNode(SNR_VAR, "__TMPL_FUNCTION(" + name + ").template_func(api, &" + ln + ", &cacheable, 0, NULL)");
	}
	delete this;
	return ret;
}

SyntaxNode *
ConditionSyntaxNode::apply(SyntaxNode *(*cb)(SyntaxNode *, void *), void *v) {
	if (condition)
		condition = condition->apply(cb, v);
	code = code->apply(cb, v);
	return SyntaxNode::apply(cb, v);
}

SyntaxNode *
ConditionalSyntaxNode::apply(SyntaxNode *(*cb)(SyntaxNode *, void *), void *v) {
	for (deque::iterator i = conditions->begin(); i != conditions->end(); i++)
		(*i)->apply(cb, v);
	return SyntaxNode::apply(cb, v);
}

SyntaxNode *
ConditionalSyntaxNode::reduce(void) {
	NodeSyntaxNode *sn = new NodeSyntaxNode(block, NULL, SNR_NONE, sn_lineno);
	bool first = true;

	for (deque::iterator iter = conditions->begin() ; iter != conditions->end() ; iter++) {
		if (!first)
			CodeSyntaxNode::code(sn, "} else ", CodeSyntaxNode::DOWNNONL);

		if ((*iter)->condition) {
			CodeSyntaxNode::code(sn, "if (", first ? CodeSyntaxNode::NONL : CodeSyntaxNode::INLINE);
			sn->add((*iter)->condition->reduce());
			(*iter)->condition = NULL;
			CodeSyntaxNode::code(sn, ") {", CodeSyntaxNode::INLINEUP);
		} else {
			CodeSyntaxNode::code(sn, "{", CodeSyntaxNode::INLINEUP);
		}
		sn->add((*iter)->code->reduce());
		(*iter)->code = NULL;
		first = false;
	}
	CodeSyntaxNode::code(sn, "}", CodeSyntaxNode::DOWN);
	return sn->reduce();
}
