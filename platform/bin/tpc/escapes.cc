#include <string>
#include <iostream>
#include <string.h>

#include "syntax.h"

std::string
StringSyntaxNode::escape(void) {
	static const char hexdigits[] = "0123456789ABCDEFabcdef";
	static const std::string trigraphs("=/'()!<>-");
	std::string res;

	for (std::string::const_iterator chi = sn_str.begin() ; chi != sn_str.end() ; chi++) {
		char ch = *chi;

		switch (ch) {
		case '?':
			if (*(chi + 1) == '?') {
				if (trigraphs.find_first_of(*(chi + 2)) != std::string::npos) {
					res += "?\\?";
					chi += 2;
				}
			}
			break;
		default:
			if (ch & 0x80) {
				res += "\\x";
				res += hexdigits[ch >> 4 & 0xF];
				res += hexdigits[ch & 0xF];
				if (chi + 1 != sn_str.end() && strchr(hexdigits, *(chi + 1)))
					res += "\"\""; /* Needed to cut off escape code */
				continue;
			}
			break;
		}
		res += *chi;
	}
	return res;
}
