%{
#include <sstream>
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <algorithm>
#include <limits.h>
#include <string.h>

#include "syntax.h"
#include "vtree_syntax_nodes.hh"
#include "lang_types.h"

#define YY_STACK_USED 1

struct outervar {
	outervar(std::string _str, int _level) : str(_str), level(_level) {}
	std::string str;
	int			level;
};

int last_state = 0;
int vtree_state = 0;
int vtree_level = 0;
int index_state = 0;
int comment_state = 0; /* Remember last state when in comment */
FlexLexer *lexer = new yyFlexLexer;
static std::string filter_name;

static int lookup_construct_token(const char* lexeme) {

	const char* lex[] = { "P", "PH", "R", "LRU", "MAP" };
	int tok[] = { CONSTRUCT_START_P, CONSTRUCT_START_PH, CONSTRUCT_START_R, CONSTRUCT_START_LRU, CONSTRUCT_START_MAP };

	for (unsigned int i = 0 ; i < sizeof(lex) / sizeof(lex[0]) ; ++i) {
		if (strcmp(lexeme, lex[i]) == 0) {
			return tok[i];
		}
	}
	return CONSTRUCT_START_UNKNOWN;
}

int yylex(YYSTYPE *yylval) {
	int result = lexer->yylex();
	if (result == LIST) {
		if (lexer->YYText()[0] == '@') {
			int cnt = 0;
			const char *text = lexer->YYText();

			while (text[cnt++] == '@');
			yylval->outer = new outervar(text + cnt - 1, cnt - 1);
			result = OUTERVAR;
		} else {
			yylval->str = new std::string(lexer->YYText());
		}
	}
	if (result == ROWCOUNT || result == LEN || result == INDEXLIST) {
		yylval->level = 0;
		if (lexer->YYText()[0] == '@') {
			int cnt = 0;
			const char *text = lexer->YYText();

			while (text[cnt++] == '@');
			yylval->level = cnt - 1;
		}
	}
	if (result == BRANCHCOUNT) {
		const char *text = lexer->YYText();
		int offs = 0;

		yylval->branchcount.level = yylval->branchcount.depth = 0;

		while (text[++offs] == '@')
			yylval->branchcount.level++;
		while (text[++offs] == '*')
			yylval->branchcount.depth++;
	}
	if (result == VARGLOB) {
		int cnt = 0;
		const char *text = lexer->YYText();

		while (text[cnt++] == '%');
		yylval->outer = new outervar(text + cnt - 1, cnt - 1);
	}

	if (result == HTML ||
	   result == HTMLSPACE ||
	   result == LIST ||
	   result == VAR ||
	   result == CONSTRUCT_START_UNKNOWN ||
	   result == FUNCOPEN ||
	   result == DIR ||
	   result == DEFINE_DIR ||
	   result == DIRARG ||
	   result == STRING ||
	   result == VTREE_PART ||
	   result == IFDEF ||
	   result == INT ||
	   result == LISTLEN ||
	   result == VARGLOB_LEN) {
		yylval->str = new std::string(lexer->YYText());
	}
	if (result == FILTEROPEN) {
		if (filter_name[0] == '@') {
			yylval->filter.isloop = 1;
			yylval->filter.name = new std::string(filter_name.substr(1, INT_MAX));
		} else {
			yylval->filter.isloop = 0;
			yylval->filter.name = new std::string(filter_name);
		}
	}
	/*
	 * The reason for escaping ENDL BACKSL and QUOTE twice here is because it first
	 * gets unescaped by the C++ compiler that will compile the lexer code, then
	 * it will get output in the generated C code where the C compiler will have to
	 * unescape it again.
	 * XXX - This does not belong here. This should be a behavior of the C code
	 *       generator, not the initial lexer.
	 */
	if (result == ENDL) {
		result = HTMLENDL;
		yylval->str = new std::string("\\n");
	}
	if (result == BACKSL) {
		result = HTML;
		yylval->str = new std::string("\\\\");
	}
	if (result == QUOTE) {
		result = HTML;
		yylval->str = new std::string("\\\"");
	}
	if (result == OPER) {
		yylval->str = new std::string(lexer->YYText());
	}
	if (result == VTREE_OPEN) {
		int cnt = 0;
		const char *text = lexer->YYText();
		while (text[cnt++] == '$');
		yylval->level = cnt - 2;
	}

	return result;
}

%}


%option yylineno
%option noyywrap
%x BLOCK_START BLOCK_CONTENTS BLOCK_CONDITIONS EXPR FUNC INDEX VTREE COMMENT FILTERS FILTER CONSTRUCT DIRNAME DIRECTIVE
ID [a-zA-Z][a-zA-Z0-9_]*
CONSTRUCTID [A-Z][A-Z0-9_]*
IDGLOB ([a-zA-Z][a-zA-Z0-9_]*)?\*
DIRID [a-zA-Z0-9_]*
VTREE_PART [a-zA-Z0-9_]+
INT [0-9]+
OPER (==|!=|<|<=|>|>=)
%%

"<\%#"/{DIRID}"("			{ yytext += 2; yy_push_state(DIRNAME); }
"<\%"					{ yy_push_state(BLOCK_START); return OPEN; }
[^<%\" \t\n\\]*        			{ return HTML; }
"%>"					{ return CLOSE; }
"<"           	 			{ return HTML; }
"%"           	 			{ return HTML; }
"\n"           	 			{ return ENDL; }
[ \t]+					{ return HTMLSPACE; }
"\\"           	 			{ return BACKSL; }
"\""           	 			{ return QUOTE; }

	/* Opening of a block, we can either find filters, or not find filters and start looking for conditions. */
<BLOCK_START>"|"			{ BEGIN(FILTERS); return FOPEN; }
<BLOCK_START>.	 			{ yyless(0); BEGIN(BLOCK_CONDITIONS); }

	/* Filters at the beginning of a block. */
<FILTERS>"|"				{ BEGIN(BLOCK_CONDITIONS); return FCLOSE; }
<FILTERS>@?{ID}/"("			{ filter_name = yytext; BEGIN(FILTER); return FILTEROPEN; }
<FILTERS>@?{ID}				{ filter_name = yytext; unput(')'); BEGIN(FILTER); return FILTEROPEN; }
<FILTERS>","				/* comma to separate filters */

<FILTER>")"				{ BEGIN(FILTERS); return FILTERCLOSE; }
<FILTER>"("				/* Eat opening paren */

	/* A block can be closed in any state of the block. */
<BLOCK_START,BLOCK_CONTENTS,BLOCK_CONDITIONS>"\%>"			{ yy_pop_state(); return CLOSE; }
	/* A comment can begin in any state of the block and inside filters. */
<BLOCK_START,BLOCK_CONTENTS,BLOCK_CONDITIONS,FILTERS,FILTER>"/*"	{ yy_push_state(COMMENT); }

	/* Beginning of a block after we've either parsed filters or not found any filters. */

	/* The following two conditions handle the special case for the weird "bug" whitespace mode. */
<BLOCK_CONDITIONS>[ \t]			{ if (eatspace != EAT_BUG) { BEGIN(BLOCK_CONTENTS); return HTMLSPACE; } }
<BLOCK_CONDITIONS>[\n]			{ if (eatspace != EAT_BUG) { BEGIN(BLOCK_CONTENTS); return ENDL; } }

<BLOCK_CONDITIONS>[^(]			{ BEGIN(BLOCK_CONTENTS); yyless(0); }
<BLOCK_CONDITIONS>"("			{ BEGIN(BLOCK_CONTENTS); yy_push_state(EXPR); return EOPEN; }

<COMMENT>"*/"				{ yy_pop_state(); }
<COMMENT>.				/* Remove comments */

<EXPR>"("				{ yy_push_state(EXPR); return EOPEN; }
<EXPR>")"				{ yy_pop_state(); return ECLOSE; }
<EXPR>{OPER}				{ return OPER; }
<EXPR>"=~"				{ return REGCOMP; }
<EXPR>"!"				{ yytext++; return NOT; }
<EXPR>"?"{ID}				{ yytext++; return IFDEF; }
<EXPR>"?"/"["				{ yytext++; yy_push_state(INDEX); return IFDEF_INDEX; }
<EXPR>"?"				{ return '?'; }
<FILTER,EXPR,INDEX,FUNC,CONSTRUCT>{INT}	{ return INT; }
<EXPR>"&&"				{ return AND; }
<EXPR>"||"				{ return OR; }
<EXPR,INDEX,FUNC>"+"			{ return '+'; }
<EXPR,INDEX,FUNC>"-"			{ return '-'; }
<EXPR,INDEX,FUNC>"/"			{ return '/'; }
<EXPR,INDEX,FUNC>"%"			{ return '%'; }
<EXPR,INDEX,FUNC>"*"			{ return '*'; }
<FUNC,FILTER,BLOCK_CONTENTS,EXPR,VTREE,INDEX>"%"{ID}/"["	{ yytext++; yy_push_state(INDEX); return VAR; }
<FUNC,FILTER,BLOCK_CONTENTS,EXPR,VTREE,INDEX>"%"/"["		{ yytext += 1; yy_push_state(INDEX); return INDEXNAMED; }
<FUNC,FILTER,BLOCK_CONTENTS,EXPR,VTREE,INDEX>@@*/"["		{ yytext += 1; yy_push_state(INDEX); return INDEXLIST; }
<INDEX>"["				{ return IOPEN; }
<INDEX>"]"/"["				{ return ICLOSE; }
<INDEX>"]"				{ yy_pop_state(); return ICLOSE; }
<FILTER,FUNC,BLOCK_CONTENTS,EXPR,INDEX,VTREE>"%"{ID}	{ yytext++; return VAR; }
<FILTER,FUNC,BLOCK_CONTENTS,EXPR,INDEX,VTREE>"%"/"$"	{ yyerror(NULL, "Obsolete %$<vtree> syntax, use %[$<vtree>] instead"); }
<FILTER,FUNC,BLOCK_CONTENTS,EXPR,INDEX,VTREE>[$][$]*\(	{ yy_push_state(VTREE); return VTREE_OPEN; }
<VTREE>")"				{ yy_pop_state(); return VTREE_CLOSE; }
<VTREE>"."				{ return COMMA; }
<VTREE>"*"				{ return '*'; }
<VTREE>"@"				{ return '@'; }
<VTREE>"#"				{ return '#'; }
<VTREE>[ \t]				/* eat whitespace */
<VTREE>"=="				{ return '='; }
<FILTER,FUNC,BLOCK_CONTENTS,EXPR,INDEX,VTREE>%%*{IDGLOB}	{ yytext++; return VARGLOB; }
<FILTER,FUNC,BLOCK_CONTENTS,EXPR,INDEX,VTREE>%#{IDGLOB}	{ yytext += 2; return VARGLOB_LEN; }
<FILTER,FUNC,BLOCK_CONTENTS,EXPR,INDEX,VTREE>@@*{ID}	{ yytext++; return LIST; }
<FILTER,FUNC,BLOCK_CONTENTS,EXPR,INDEX,VTREE>"&"/"("	{ yy_push_state(FUNC); return EMPTYFUNC; }
<FILTER,FUNC,BLOCK_CONTENTS,EXPR,INDEX,VTREE>"&"{ID}/"("	{ yytext++; yy_push_state(FUNC); return FUNCOPEN; }
<FUNC>"("				/* Eat it up */
<FUNC>")"				{ yy_pop_state(); return FUNCCLOSE; }
<FILTER,FUNC,CONSTRUCT,DIRECTIVE>[ \t\n]*,[ \t\n]*	{ return COMMA; }
<FILTER,FUNC,EXPR,VTREE,CONSTRUCT,DIRECTIVE,INDEX>["]([^"\n\\]|\\.)*["]	{ return STRING; }
<FILTER,FUNC,EXPR,VTREE>{CONSTRUCTID}/\[	{ yy_push_state(CONSTRUCT); return lookup_construct_token(yytext); }
<FILTER,FUNC,EXPR,VTREE,INDEX>"<%"	{ yy_push_state(BLOCK_START); return OPEN; }
<CONSTRUCT>\[				{ return CONSTRUCT_OPEN; }
<CONSTRUCT>\]/\[			{ BEGIN(INDEX); return CONSTRUCT_CLOSE; }
<CONSTRUCT>\]				{ yy_pop_state(); return CONSTRUCT_CLOSE; }
<CONSTRUCT>"=>"				{ return CONSTRUCT_ARROW; }
<CONSTRUCT>default			{ return CONSTRUCT_DEFAULT; }
<VTREE>{VTREE_PART}			{ return VTREE_PART; }
<DIRNAME>def{DIRID}			{ BEGIN(DIRECTIVE); return DEFINE_DIR; }
<DIRNAME>{DIRID}			{ BEGIN(DIRECTIVE); return DIR; }
<DIRECTIVE>[a-zA-Z0-9][a-zA-Z0-9_./]*	{ return DIRARG; }
<DIRECTIVE>"?"				{ return '?'; }
<DIRECTIVE>"("				/**/
<DIRECTIVE>")\%>"[\n]?			{ yy_pop_state(); return DIRCLOSE; }
<DIRECTIVE>[ \t\n]*			/* Skip whitespace inside the directive. */
<DIRECTIVE>.				{ return HTML; }
<BLOCK_CONTENTS>"<\%#/"{DIRID}"("	{ yytext += 2; yy_push_state(DIRNAME); }
<BLOCK_CONTENTS>[^\\_<%@#& \n\t:"$/*]* 		{ return HTML; }
<BLOCK_CONTENTS>"::"				{ BEGIN(BLOCK_CONDITIONS); return ELSE; }
<BLOCK_CONTENTS>"_"   				{ return HTML; }
<BLOCK_CONTENTS>"<"   				{ return HTML; }
<BLOCK_CONTENTS>"%"   				{ return HTML; }
<BLOCK_CONTENTS>"\$"   				{ return HTML; }
<BLOCK_CONTENTS>"@"   				{ return HTML; }
<BLOCK_CONTENTS>"#"   				{ return HTML; }
<BLOCK_CONTENTS>"*"   				{ return HTML; }
<BLOCK_CONTENTS>"&"   				{ return HTML; }
<BLOCK_CONTENTS>"\""   				{ return QUOTE; }
<BLOCK_CONTENTS>"/"   				{ return HTML; }
<BLOCK_CONTENTS>"\\n"  				{ return ENDL; }
<BLOCK_CONTENTS>"\\\\"	  			{ return BACKSL; }
<BLOCK_CONTENTS>"\\\""				{ return QUOTE; }
<BLOCK_CONTENTS>[\\].				{ yytext++; return HTML; }
<BLOCK_CONTENTS>":"   				{ return HTML; }
<BLOCK_CONTENTS>"\n"				{ return ENDL; }
<BLOCK_CONTENTS>[ \t]+				{ return HTMLSPACE; }
<FILTER,EXPR,FUNC,BLOCK_CONTENTS,INDEX>#@{ID} 		{ yytext += 2; return LISTLEN; }
<FILTER,EXPR,FUNC,BLOCK_CONTENTS,INDEX>#@/"[" 		{ yytext += 2; yy_push_state(INDEX); return LISTLEN_INDEX; }
<FILTER,EXPR,FUNC,BLOCK_CONTENTS,INDEX>#@@* 		{ yytext += 2; return LEN; }
<FILTER,FUNC,BLOCK_CONTENTS,EXPR,INDEX>@@*#		{ yytext++; return ROWCOUNT; }
<FILTER,FUNC,BLOCK_CONTENTS,EXPR,INDEX>@+\*+		{ return BRANCHCOUNT; }
<BLOCK_CONTENTS>"<\%"				{ yy_push_state(BLOCK_START); return OPEN; }
<FILTERS,FILTER,EXPR,INDEX,COMMENT>[ \t\n]+   	/* eat up whitespace */

<INDEX>.				{ yyerror(NULL, "Syntax error, expected variable, block or vtree"); }
<FILTERS>.				{ yyerror(NULL, "Syntax error, expected comma (,) or |"); }
<FILTER>.				{ yyerror(NULL, "Syntax error, expected comma (,) or )"); }
%%
