%{
#include <iostream>
#include <sstream>
#include <set>
#include <string>
#include <stack>
#include <deque>
#include <vector>
#include <list>
#include <sys/types.h>
#include <sys/wait.h>
#include <cstdio>

#include "builtin.h"
#include "syntax.h"
#include "vtree_syntax_nodes.hh"
#include "lang_types.h"

#include <FlexLexer.h>

#define YYDEBUG 1
#define YYERROR_VERBOSE 1

%}

%union {
	std::string *str;
	SyntaxNode *snode;
	TemplateSyntaxNode *tnode;
	ParamsSyntaxNode *pnode;
	VtreeParamsSyntaxNode *vpnode;
	BranchSyntaxNode *bnode;
	std::deque<FilterSyntaxNode*> *filterlist;
	struct outervar *outer;
	int level;
	struct {
		int level;
		int depth;
	} branchcount;
	std::vector<std::string> *vector;
	class ConstructSyntaxNode::construct_parameter *construct_parameter;
	class ConstructSyntaxNode::construct_parameters *construct_parameters;
	struct tdirective *tdirective;
	ConditionalSyntaxNode::deque *condlist;
	struct {
		int isloop;
		std::string *name;
	} filter;
}

%{

#define yytrue true
#define yyfalse false

extern int verbose;

enum eatspace eatspace = EAT_BUG;

struct outervar {
	outervar(std::string _str, int _level) : str(_str), level(_level) {}
	std::string str;
	int level;
};

BlockSyntaxNode *currblock;
BranchSyntaxNode *currbranch;

extern FlexLexer *lexer;
#define yylineno lexer->lineno()

/*
 * Macros for creating of syntax nodes.
 */
#define STR(parent, str) new StringSyntaxNode(currblock, parent, yylineno, str)
#define STRWHITE(parent, str) (eatspace == EAT_ALL ? NULL : (eatspace == EAT_SOME || eatspace == EAT_EOL) ? (SyntaxNode*)new StrWhiteSyntaxNode(currblock, parent, yylineno) : (SyntaxNode*)new StringSyntaxNode(currblock, parent, yylineno, str))
#define STRNL(parent, str) ((eatspace == EAT_ALL || eatspace == EAT_EOL) ? NULL : eatspace == EAT_SOME ? (SyntaxNode*)new StrNlSyntaxNode(currblock, parent, yylineno) : (SyntaxNode*)new StringSyntaxNode(currblock, parent, yylineno, str))

#define EXP(l, exp, r) new ExpressionSyntaxNode(currblock, NULL, yylineno, l, r, ExpressionSyntaxNode::SNE_##exp, "")
#define EXPOP(l, op, r) new ExpressionSyntaxNode(currblock, NULL, yylineno, l, r, ExpressionSyntaxNode::SNE_OP, op)

#define P_VAR(name, index) new PVarSyntaxNode(currblock, NULL, yylineno, name, index)

#define ARITH(l, op, r) new ArithSyntaxNode(currblock, NULL, yylineno, l, ArithSyntaxNode::SNA_##op, r)

%}

%pure-parser
%parse-param { TemplateSyntaxNode **result }

%token OPEN CLOSE EOPEN ECLOSE VAR LIST OUTERVAR FUNCOPEN EMPTYFUNC FUNCCLOSE
%token HTML HTMLSPACE HTMLENDL BACKSL ENDL ROWCOUNT COMMA QUOTE STRING IFDEF NOT INT OPER VTREE_PART
%token LISTLEN LISTLEN_INDEX LEN AND OR ELSE IOPEN ICLOSE VTREE_OPEN VTREE_CLOSE
%token REGCOMP FOPEN FCLOSE FILTEROPEN FILTERCLOSE BRANCHCOUNT VAR_VTREE VARGLOB VARGLOB_LEN INDEXNAMED INDEXLIST IFDEF_INDEX
%token CONSTRUCT_START_MAP CONSTRUCT_START_P CONSTRUCT_START_PH CONSTRUCT_START_R CONSTRUCT_START_LRU CONSTRUCT_START_UNKNOWN
%token CONSTRUCT_OPEN CONSTRUCT_CLOSE CONSTRUCT_ARROW CONSTRUCT_DEFAULT DIR DEFINE_DIR DIRARG DIRCLOSE

%type <level> ROWCOUNT VTREE_OPEN LEN INDEXLIST
%type <branchcount> BRANCHCOUNT
%type <str> HTML HTMLSPACE HTMLENDL VAR LIST FUNCOPEN STRING IFDEF INT OPER LISTLEN VTREE_PART VAR_VTREE VARGLOB_LEN
%type <str> DIR DEFINE_DIR DIRARG CONSTRUCT_START_UNKNOWN darg
%type <construct_parameters> construct_parameters_opt construct_parameters mapconstruct_parameters
%type <construct_parameter> construct_parameter mapconstruct_parameter
%type <snode> template stmts conditional optelse tcode condition exp parameter construct_index construct vtree vtree_part
%type <tnode> definedir
%type <condlist> elseifchain
%type <pnode> optparameters parameters
%type <vpnode> vtree_parts
%type <bnode> tstring
%type <filterlist> optfilters filters
%type <outer> OUTERVAR
%type <outer> VARGLOB
%type <tdirective> directive_args
%type <filter> FILTEROPEN

%left AND OR
%right NOT
%left '-' '+'
%left '/' '%' '*'
%left ELSE

%expect 0

%%

template: definedir
{
	TopBlockSyntaxNode *tb;
	*result = $1;
	currblock = tb = new TopBlockSyntaxNode();
	currblock->parameter_output();
	$$ = new OutSyntaxNode(currblock, 0);
	currblock->content = $$;
	(*result)->set_topblock(tb);
}
| template HTML
{
	$$ = $1;
	(void)STR($$, *$2);
	delete $2;
}
| template HTMLSPACE
{
	$$ = $1;
	(void)STRWHITE($$, *$2);
	delete $2;
}
| template HTMLENDL
{
	$$ = $1;
	(void)STRNL($$, *$2);
	delete $2;
}
| template tcode
{
	$$ = $1;
	if ($2) {
		$2->parameter_output();
		$$->add($2);
	}
}
;

definedir:
/* empty */
{
	$$ = new TemplateSyntaxNode();
}
| DEFINE_DIR directive_args DIRCLOSE
{
	const char *errstr;
	DefineTemplateSyntaxNode *sn;

	$$ = sn = new DefineTemplateSyntaxNode();

	$2->name = $1;
	if ((errstr = handle_directive($2, sn)) != NULL) {
		yyerror(result, errstr);
		YYABORT;
	}
}
;

tcode:  OPEN
{
	currblock = new BlockSyntaxNode(currblock, yylineno);
}
optfilters stmts CLOSE
{
	if ($3) {
		currblock->filters.insert(currblock->filters.end(), $3->begin(), $3->end());
		$3->clear();
		delete $3;
	}
	currblock->content = $4;

	$$ = currblock;
	currblock = currblock->block;
}
| DIR directive_args DIRCLOSE
{
	const char *errstr;

	$$ = NULL;

	$2->name = $1;
	if ((errstr = handle_directive($2, NULL)) != NULL) {
		yyerror(result, errstr);
		YYABORT;
	}
}
;

optfilters:	/* empty */	{ $$ = NULL; }
| FOPEN filters FCLOSE
{
	$$ = $2;
}
;

stmts: { currbranch = new BranchSyntaxNode(currblock, yylineno, currbranch); } tstring
{
	currbranch = $2->parent_branch;
	$$ = $2;
}
| conditional
{
	$$ = $1;
}
;

conditional: { currbranch = new BranchSyntaxNode(currblock, yylineno, currbranch); } condition tstring { currbranch = currbranch->parent_branch; } elseifchain optelse
{
	$5->push_front(new ConditionSyntaxNode(currblock, yylineno, $2, $3));
	if ($6)
		$5->push_back(new ConditionSyntaxNode(currblock, yylineno, NULL, $6));
	$$ = new ConditionalSyntaxNode(currblock, yylineno, $5);
}
;

optelse: /* empty */	{ $$ = NULL; }
| ELSE { currbranch = new BranchSyntaxNode(currblock, yylineno, currbranch); } tstring
{
	currbranch = $3->parent_branch;
	$$ = $3;
}
;

elseifchain: /* empty */	{ $$ = new std::deque<ConditionSyntaxNode*>; }
| elseifchain ELSE { currbranch = new BranchSyntaxNode(currblock, yylineno, currbranch); } condition tstring
{
	currbranch = $5->parent_branch;
	$$ = $1;
	$$->push_back(new ConditionSyntaxNode(currblock, yylineno, $4, $5));
}
;


filters: /* empty */		{ $$ = new std::deque<FilterSyntaxNode*>; }
| filters FILTEROPEN optparameters FILTERCLOSE
{
	$$ = $1;
	$$->push_back(new FilterSyntaxNode(currblock, $2.isloop, *$2.name, $3));
	delete $2.name;
}
;

tstring: /* empty */		{ $$ = currbranch; }
| tstring HTML
{
	$$ = $1;
	(void)STR($$, *$2);
	delete $2;
}
| tstring HTMLSPACE
{
	$$ = $1;
	(void)STRWHITE($$, *$2);
	delete $2;
}
| tstring HTMLENDL
{
	$$ = $1;
	(void)STRNL($$, *$2);
	delete $2;
}
| tstring parameter
{
	$$ = $1;

	if ($2) {
		/* hack? */
		$2->parameter_output();
		$$->add($2);
	}
}
;

directive_args: /* empty */	{ $$ = new tdirective; }
| directive_args COMMA darg
{
	$$ = $1;
	$$->args.push_back($3);
}
| darg
{
	$$ = new tdirective;
	$$->args.push_back($1);
}
;

darg:
DIRARG
{
	$$ = $1;
}
| DIRARG '?'
{
	$$ = $1;
}
;

optparameters: /* empty */	{ $$ = NULL; }
| parameters			{ $$ = $1; }
;


parameters:
parameters COMMA parameter
{
	$$ = $1;
	$$->add($3);
}
| parameter
{
	$$ = new ParamsSyntaxNode(currblock);
	$$->add($1);
}
;

parameter: VAR
{
	$$ = P_VAR(STR(NULL, *$1), NULL);

	(*result)->vars.insert(*$1);	/* XXX - resolve */
	delete $1;
}
| VAR IOPEN parameter ICLOSE
{
	$$ = P_VAR(STR(NULL, *$1), $3);

	(*result)->vars.insert(*$1);	/* XXX - resolve */
	delete $1;
}
| INDEXNAMED IOPEN parameter ICLOSE IOPEN parameter ICLOSE
{
	$$ = P_VAR($3, $6);
}
| INDEXNAMED IOPEN parameter ICLOSE
{
	$$ = P_VAR($3, NULL);
}
| INDEXLIST IOPEN parameter ICLOSE
{
	$$ = new ValuesLoopSyntaxNode(currblock, NULL, yylineno, $1, $3);
}
| OUTERVAR
{
	(*result)->vars.insert($1->str);	/* XXX - this should be done by resolve */
	$$ = new ValuesLoopSyntaxNode(currblock, NULL, yylineno, $1->level, STR(NULL, $1->str));
	delete $1;
}
| LIST
{
	(*result)->vars.insert(*$1);	/* XXX - this should be done by resolve */
	$$ = new ValuesLoopSyntaxNode(currblock, NULL, yylineno, 0, STR(NULL, *$1));
	delete $1;
}
| VARGLOB
{
	$$ = new GlobLoopSyntaxNode(currblock, NULL, yylineno, $1->level, STR(NULL, $1->str));
	delete $1;
}
| VARGLOB_LEN
{
	$$ = new GlobLenSyntaxNode(currblock, yylineno, STR(NULL, *$1));
	delete $1;
}
| STRING
{
	/* Remove " */
	$1->erase(0, 1);
	$1->erase($1->size() - 1, 1);
	$$ = STR(NULL, *$1);
	delete $1;
}
| ROWCOUNT
{
	$$ = new CountLoopSyntaxNode(currblock, NULL, yylineno, $1);
}
| BRANCHCOUNT
{
	$$ = new BranchCountSyntaxNode(currblock, NULL, yylineno, currbranch, $1.level, $1.depth);
}
| INT
{
	$$ = new IntegerSyntaxNode(currblock, NULL, yylineno, atoi($1->c_str()));
	delete $1;
}
| LISTLEN
{
	$$ = new ListLenSyntaxNode(currblock, yylineno, STR(NULL, *$1));

	(*result)->vars.insert(*$1);
	delete $1;
}
| LISTLEN_INDEX IOPEN parameter ICLOSE
{
	$$ = new ListLenSyntaxNode(currblock, yylineno, $3);
}
| LEN
{
	$$ = new LenLoopSyntaxNode(currblock, NULL, yylineno, $1);
}
| vtree
{
	$$ = $1;
}
| parameter '+' parameter
{
	$$ = ARITH($1, ADD, $3);
}
| parameter '-' parameter
{
	$$ = ARITH($1, SUB, $3);
}
| parameter '/' parameter
{
	$$ = ARITH($1, DIV, $3);
}
| parameter '%' parameter
{
	$$ = ARITH($1, MOD, $3);
}
| parameter '*' parameter
{
	$$ = ARITH($1, MUL, $3);
}
| EMPTYFUNC parameter FUNCCLOSE
{
	$$ = $2;
}
| FUNCOPEN optparameters FUNCCLOSE
{
	$$ = new FunCallSyntaxNode(currblock, NULL, yylineno, *$1, $2);
	delete $1;
}
| construct
{
	$$ = $1;
}
| tcode
;

construct:
CONSTRUCT_START_P CONSTRUCT_OPEN construct_parameters_opt CONSTRUCT_CLOSE
{
	enum SyntaxNode::sn_ret ret = SyntaxNode::SNR_NONE;

	if ($3->first)
		ret = SyntaxNode::SNR_STR;

	$$ = new PConstructSyntaxNode(currblock, ret, $3);
}
|
CONSTRUCT_START_PH CONSTRUCT_OPEN construct_parameters_opt CONSTRUCT_CLOSE construct_index
{
	enum SyntaxNode::sn_ret ret = SyntaxNode::SNR_NONE;

	if ($3->first)
		ret = SyntaxNode::SNR_STR;

	$$ = new PHConstructSyntaxNode(currblock, ret, $3, $5);
}
|
CONSTRUCT_START_LRU CONSTRUCT_OPEN construct_parameters_opt CONSTRUCT_CLOSE construct_index
{
	enum SyntaxNode::sn_ret ret = SyntaxNode::SNR_NONE;

	if ($3->first)
		ret = SyntaxNode::SNR_STR;

	$$ = new LRUConstructSyntaxNode(currblock, ret, $3, $5);
}
|
CONSTRUCT_START_R CONSTRUCT_OPEN construct_parameters_opt CONSTRUCT_CLOSE
{
	$$ = new RegexConstructSyntaxNode(currblock, $3);
}
|
CONSTRUCT_START_MAP CONSTRUCT_OPEN mapconstruct_parameters CONSTRUCT_CLOSE construct_index
{
	$$ = new MapConstructSyntaxNode(currblock, $3, $5);
}
|
CONSTRUCT_START_UNKNOWN
{
	yyerror(result, std::string("unknown construct ") + *$1);
	delete $1;
	YYABORT;
}
;

construct_parameters_opt:
/* empty */
{
	$$ = new ConstructSyntaxNode::construct_parameters;
}
| construct_parameters
;

construct_parameters:
construct_parameter
{
	$$ = new ConstructSyntaxNode::construct_parameters;
	$$->second.push_back(*$1);
	if ($1->second.length())
		$$->first = true;
	delete $1;
}
| construct_parameters COMMA construct_parameter
{
	$$ = $1;
	$$->second.push_back(*$3);
	if ($3->second.length())
		$$->first = true;
	delete $3;
}
;

construct_parameter:
STRING
{
	/* Remove " */
	$1->erase(0, 1);
	$1->erase($1->size() - 1, 1);
	$$ = new ConstructSyntaxNode::construct_parameter($1, "");
}
| INT
{
	$$ = new ConstructSyntaxNode::construct_parameter($1, "");
}
;

mapconstruct_parameters:
mapconstruct_parameter
{
	$$ = new ConstructSyntaxNode::construct_parameters;
	$$->second.push_back(*$1);
	if ($1->second.length())
		$$->first = true;
	delete $1;
}
| mapconstruct_parameters COMMA mapconstruct_parameter
{
	$$ = $1;
	$$->second.push_back(*$3);
	if ($3->second.length())
		$$->first = true;
	delete $3;
}
;

mapconstruct_parameter:
STRING CONSTRUCT_ARROW STRING
{
	$1->erase(0, 1);
	$1->erase($1->size() - 1, 1);
	$3->erase(0, 1);
	$3->erase($3->size() - 1, 1);
	$$ = new ConstructSyntaxNode::construct_parameter($1, *$3);
	delete $3;
}
| CONSTRUCT_DEFAULT CONSTRUCT_ARROW STRING
{
	$3->erase(0, 1);
	$3->erase($3->size() - 1, 1);
	$$ = new ConstructSyntaxNode::construct_parameter(NULL, *$3);
	delete $3;
}
;

construct_index:
IOPEN parameter ICLOSE
{
	$$ = $2;
}
;

condition: EOPEN exp ECLOSE			{ $$ = $2; }
;

exp:
NOT exp
{
	$$ = EXP($2, NOT, NULL);
}
| exp AND exp
{
	$$ = EXP($1, AND, $3);
}
| exp OR exp
{
	$$ = EXP($1, OR, $3);
}
| EOPEN exp ECLOSE
{
	$$ = EXP($2, EMPTY, NULL);
}
| EOPEN LIST ECLOSE
{
	yyerror(result, "Obsolete <%(@list) syntax");
	YYABORT;
}
| IFDEF
{
	$$ = EXP(STR(NULL, *$1), IFDEF, NULL);

	(*result)->vars.insert(*$1);
	delete $1;
}
| IFDEF_INDEX IOPEN parameter ICLOSE
{
	$$ = EXP($3, IFDEF, NULL);
}
| '?' VTREE_OPEN vtree_parts VTREE_CLOSE
{
	$$ = new IfdefVtreeSyntaxNode(currblock, yylineno, $3);
}
| parameter OPER parameter
{
	$$ = EXPOP($1, *$2, $3);
	delete $2;
}
| parameter REGCOMP parameter
{
	$$ = EXP($1, REGCOMP, $3);
}
| parameter NOT REGCOMP parameter
{
	$$ = EXP(EXP($1, REGCOMP, $4), NOT, NULL);
}
;

vtree: VTREE_OPEN vtree_parts VTREE_CLOSE
{
	$$ = new GetVtreeSyntaxNode(currblock, yylineno, $2);
}
| VTREE_OPEN vtree_parts COMMA VTREE_CLOSE
{
	$$ = new GetnodeVtreeSyntaxNode(currblock, yylineno, $2);
}
| VTREE_OPEN vtree_parts COMMA '*' VTREE_CLOSE
{
	$$ = new KeysLoopVtreeSyntaxNode(currblock, yylineno, $1, $2);
}
| VTREE_OPEN vtree_parts COMMA '@' VTREE_CLOSE
{
	$$ = new ValuesLoopVtreeSyntaxNode(currblock, yylineno, $1, $2, NULL);
}
| VTREE_OPEN vtree_parts COMMA '@' COMMA vtree_parts VTREE_CLOSE
{
	$$ = new ValuesLoopVtreeSyntaxNode(currblock, yylineno, $1, $2, $6);
}
| VTREE_OPEN vtree_parts COMMA '#' VTREE_CLOSE
{
	$$ = new GetlenVtreeSyntaxNode(currblock, yylineno, $2);
}
| VTREE_OPEN vtree_parts COMMA '*' COMMA vtree_parts '=' parameter VTREE_CLOSE
{
	$$ = new ByvalLoopVtreeSyntaxNode(currblock, yylineno, $1, $2, $6, new VtreeParamsSyntaxNode(currblock, $8));
}
| VTREE_OPEN vtree_parts COMMA '*' '=' parameter VTREE_CLOSE
{
	$$ = new ByvalLoopVtreeSyntaxNode(currblock, yylineno, $1, $2, NULL, new VtreeParamsSyntaxNode(currblock, $6));
}
;

vtree_parts: vtree_part
{
	$$ = new VtreeParamsSyntaxNode(currblock);

	$$->add($1);
}
| vtree_parts COMMA vtree_part
{
	$$ = $1;

	$$->add($3);
}

vtree_part: VTREE_PART
{
	$$ = STR(NULL, *$1);
	delete $1;
}
| parameter
{
	$$ = $1;
}
;
