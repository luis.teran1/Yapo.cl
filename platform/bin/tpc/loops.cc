#include "syntax.h"

void
InitLoopSyntaxNode::resolve_node(pp_queue &ppq) {
	ICERR(this, "ICE: resolve on InitLoopSyntaxNode");
}

SyntaxNode *
InitLoopSyntaxNode::apply(SyntaxNode *(*cb)(SyntaxNode *, void *), void *v) {
	if (code)
		code = code->apply(cb, v);
	return SyntaxNode::apply(cb, v);
}

SyntaxNode *
InitLoopSyntaxNode::reduce(void) {
	NodeSyntaxNode *sn = new NodeSyntaxNode(loop_block, NULL, SNR_NONE, sn_lineno);
	std::string var = loop_var_name();
	std::string len = loop_block->loop_len_name();
	std::string cnt = loop_block->loop_cnt_name();
	std::string cacheable;

	if (global)
		cacheable = "cc_" + var;
	else
		cacheable = "cacheable";

	CodeSyntaxNode::code(sn, "struct bpapi_loop_var " + var + "store, *" + var + " = &" + var + "store;");

	if (global) {
		std::string gvar = "g" + var;

		if (!globals.insert("static struct bpapi_loop_var " + gvar + " = {-1};").second)
			ICERR(this, "ICE: global already exists");

		clear_cache.insert("if (" + gvar + ".len != -1 && " + gvar + ".cleanup) " + gvar + ".cleanup(&" + gvar + ");\n\t" + gvar + ".len = -1");
		CodeSyntaxNode::code(sn, "enum bpcacheable " + cacheable + " = BPCACHE_CAN;");
		CodeSyntaxNode::code(sn, "TMPL_CACHED_LOOP_PREPARE(" + gvar + ", " + var + ", cc_" + var + ", cacheable, ", CodeSyntaxNode::UP);
	}

	this->reduce_linit(sn, var, cnt, cacheable);
	if (global) {
		CodeSyntaxNode::code(sn, ")", CodeSyntaxNode::DOWN);
	}
	CodeSyntaxNode::code(sn, "if (" + var + "->len > " + len + ") " + len + " = " + var + "->len;");

	code = NULL;
	delete this;
	return sn->reduce();
}

SyntaxNode *
InitLoopSyntaxNode::cleanup(void) {
	NodeSyntaxNode *ret = new NodeSyntaxNode(loop_block, NULL, SNR_NONE, sn_lineno);
	std::string var = loop_var_name();

	if (global) {
		std::string gvar = "g" + var;
		CodeSyntaxNode::code(ret, "TMPL_CACHED_LOOP_FINALIZE(" + gvar + ", " + var + ", cc_" + var + ");");
	} else {
		CodeSyntaxNode::code(ret, "if (" + var + "->cleanup) {", CodeSyntaxNode::UP);
		CodeSyntaxNode::code(ret, var + "->cleanup(" + var + ");");
		CodeSyntaxNode::code(ret, "}", CodeSyntaxNode::DOWN);
	}
	return ret;
}

void
ValuesInitLoopSyntaxNode::convert_node(void) {
	if (code->sn_ret != SNR_STR)
		code = code->convert_to_var();
}

void
ValuesInitLoopSyntaxNode::reduce_linit(NodeSyntaxNode *ret, std::string &var, std::string &cnt, std::string &cacheable) {
	ret->add(code->fetchlist(loop_var_name())->reduce());
}

void
GlobInitLoopSyntaxNode::convert_node(void) {
	if (code->sn_ret != SNR_STR)
		code = code->convert_to_var();
}

void
GlobInitLoopSyntaxNode::reduce_linit(NodeSyntaxNode *ret, std::string &var, std::string &cnt, std::string &cacheable) {
	int indent = 0;
	if (code->sn_ret == SNR_STR) {
		CodeSyntaxNode::code(ret, "template_fetchglob(api, " + code->reduce()->fold(&indent) + ", " + var + ");");
	} else {
		CodeSyntaxNode::code(ret, "template_fetchglob_var(api, " + code->reduce()->fold(&indent) + ", " + var + ");");
	}
}

void
VtreeKeysInitLoopSyntaxNode::reduce_linit(NodeSyntaxNode *ret, std::string &var, std::string &cnt, std::string &cacheable) {
	CodeSyntaxNode::code(ret, "vtree_fetch_keys_cachev(&api->vchain, " + var + ", &" + cacheable, CodeSyntaxNode::NONL);
	ret->add(code->reduce());
	CodeSyntaxNode::code(ret, ");", CodeSyntaxNode::INLINENL);
}

void
VtreeValuesInitLoopSyntaxNode::reduce_linit(NodeSyntaxNode *ret, std::string &var, std::string &cnt, std::string &cacheable) {
	CodeSyntaxNode::code(ret, "vtree_fetch_values_cachev(&api->vchain, " + var + ", &" + cacheable, CodeSyntaxNode::NONL);
	ret->add(code->reduce());
	CodeSyntaxNode::code(ret, ");", CodeSyntaxNode::INLINENL);
}

void
VtreeByvalInitLoopSyntaxNode::reduce_linit(NodeSyntaxNode *ret, std::string &var, std::string &cnt, std::string &cacheable) {
	CodeSyntaxNode::code(ret, "vtree_fetch_keys_by_value_cachev(&api->vchain, " + var + ", &" + cacheable + ", ", CodeSyntaxNode::NONL);
	ret->add(code->reduce());
	CodeSyntaxNode::code(ret, ");", CodeSyntaxNode::INLINENL);
}

std::string
InitLoopSyntaxNode::loop_var_name(void) {
	return get_static_name(lv_prefix(), this);
}

void
LoopSyntaxNode::resolve_node(pp_queue &ppq) {
	BlockSyntaxNode *b;

	if (loop_block)
		return;

	b = block->anchor(level);
	if (!b || !b->block)
		PERR(this, "Outer loop level exceeds nesting level");

	loop_block = b;
	if ((init_loop = resolve_loop()) != NULL) {
		init_loop->verify_loop(loop_block);
		init_loop = loop_block->insert_loop(init_loop);
	}

	for (b = block; b != loop_block; b = b->block) {
		if (init_loop)
			b->insert_outer(init_loop);
		b->insert_outer_loop(loop_block);
	}
}

SyntaxNode *
CountLoopSyntaxNode::reduce(void) {
	SyntaxNode *ret;

	if (loop_block->loops.empty() && !loop_block->has_loop_filter())
		PERR(this, "loop count without loop");
	ret = new CodeSyntaxNode(SNR_INT, loop_block->loop_cnt_name());
	delete this;
	return ret;
}

InitLoopSyntaxNode *
CountLoopSyntaxNode::resolve_loop(void) {
	loop_block->set_have_counters();
	return NULL;
}

SyntaxNode *
LenLoopSyntaxNode::reduce(void) {
	SyntaxNode *ret;

	if (loop_block->loops.empty() && !loop_block->has_loop_filter())
		PERR(this, "loop length without loop");
	ret = new CodeSyntaxNode(SNR_INT, loop_block->loop_len_name());
	delete this;
	return ret;
}

InitLoopSyntaxNode *
LenLoopSyntaxNode::resolve_loop(void) {
	loop_block->set_have_counters();
	return NULL;
}

SyntaxNode *
LoopSyntaxNode::reduce(void) {
	if (loop_block == NULL)
		ICERR(this, "ICE: loop_block unresolved");

	std::string cnt = loop_block->loop_cnt_name();
	std::string len = loop_block->loop_len_name();
	std::string var = init_loop->loop_var_name();

	delete this;
	return new CodeSyntaxNode(SNR_STR, "TMPL_L_VAR(" + var + ", " + cnt + ")");
}

void
LoopSyntaxNode::verify_loop_node(BlockSyntaxNode *parent_of) {
	BlockSyntaxNode *lb;

	if (!loop_block)
		ICERR(this, "verify_loop_node before resolve");

	for (lb = parent_of->block; lb; lb = lb->block) {
		if (lb == loop_block)
			break;
	}
	if (!lb)
		PERR(this, "Loop uses another loop on same/inner level.");
}

SyntaxNode *
ValuesLoopSyntaxNode::apply(SyntaxNode *(*cb)(SyntaxNode *, void *), void *v) {
	if (name)
		name = name->apply(cb, v);
	return SyntaxNode::apply(cb, v);
}


InitLoopSyntaxNode *
ValuesLoopSyntaxNode::resolve_loop(void) {
	SyntaxNode *n = name;
	name = NULL;

	return new ValuesInitLoopSyntaxNode(block, NULL, sn_lineno, loop_block, n, 0);
}

InitLoopSyntaxNode *
GlobLoopSyntaxNode::resolve_loop(void) {
	SyntaxNode *n = name;
	name = NULL;

	return new GlobInitLoopSyntaxNode(block, NULL, sn_lineno, loop_block, n, 0);
}

