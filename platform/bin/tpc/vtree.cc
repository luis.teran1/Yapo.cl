#include "vtree_syntax_nodes.hh"

#include <err.h>

static void
add_stree(BlockSyntaxNode *b) {
	if (!b->has_stree_filter())
		b->filters.push_back(new FilterSyntaxNode(b, false, "stree", NULL));
}

static void
check_and_queue_stree(pp_queue &ppq, const VtreeParamsSyntaxNode *params, BlockSyntaxNode *b) {
	if (!params || params->const_parts.empty())
		return;

	/*
	 * We want to add as few stree filters as possible, therefore do the check from outermost
	 * to innermost block. Since apply is depth first, we need a queue to accomplish this.
	 */
	const std::string &step = params->const_parts.front()->sn_str;
	if (step == "s" || step == "l")
		ppq.emplace(b->depth(), std::bind(add_stree, b));
}

void
VtreeParamsSyntaxNode::add(SyntaxNode *child) {
	if (dyn_parts.empty() && typeid(*child) == typeid(class StringSyntaxNode)) {
		const_parts.push_back((StringSyntaxNode *)child);
		const_cache_key += ((StringSyntaxNode *)child)->sn_str + "|";
	} else {
		dyn_parts.push_back(child);
	}
}

SyntaxNode *
VtreeParamsSyntaxNode::apply(SyntaxNode *(*cb)(SyntaxNode *, void *), void *v) {
	for (std::deque<StringSyntaxNode *>::iterator i = const_parts.begin(); i != const_parts.end(); i++)
		(*i)->apply(cb, v);
	for (std::deque<SyntaxNode *>::iterator i = dyn_parts.begin(); i != dyn_parts.end(); i++)
		*i = (*i)->apply(cb, v);
	return SyntaxNode::apply(cb, v);
}

void
VtreeParamsSyntaxNode::convert_node(void) {
	for (std::deque<SyntaxNode *>::iterator i = dyn_parts.begin(); i != dyn_parts.end(); i++) {
		*i = (*i)->convert_to_str();
	}
}

SyntaxNode *
VtreeParamsSyntaxNode::reduce_const(void) {
	if (const_parts.empty())
		return NULL;


	NodeSyntaxNode *ret = new NodeSyntaxNode(block, NULL, SNR_NONE, sn_lineno);

	for (std::deque<StringSyntaxNode *>::iterator i = const_parts.begin(); i != const_parts.end(); i++) {
		if (i != const_parts.begin())
			CodeSyntaxNode::code(ret, ", ", CodeSyntaxNode::INLINE);
		ret->add((*i)->reduce());
	}
	const_parts.clear();
	return ret;
}

SyntaxNode *
VtreeParamsSyntaxNode::reduce_dyn(void) {
	if (dyn_parts.empty())
		return NULL;

	NodeSyntaxNode *ret = new NodeSyntaxNode(block, NULL, SNR_NONE, sn_lineno);
	for (std::deque<SyntaxNode *>::iterator i = dyn_parts.begin(); i != dyn_parts.end(); i++) {
		if (i != dyn_parts.begin())
			CodeSyntaxNode::code(ret, ", ", CodeSyntaxNode::INLINE);
		ret->add((*i)->reduce());
	}
	dyn_parts.clear();
	return ret;
}

SyntaxNode *
VtreeParamsSyntaxNode::reduce(void) {
	int indent = 0;
	std::string c;
	bool first = true;

	for (std::deque<StringSyntaxNode *>::iterator i = const_parts.begin(); i != const_parts.end(); i++) {
		if (!first) {
			c += ", ";
		}
		first = false;
		c += (*i)->reduce()->fold(&indent);
	}
	const_parts.clear();
	for (std::deque<SyntaxNode *>::iterator i = dyn_parts.begin(); i != dyn_parts.end(); i++) {
		if (!first) {
			c += ", ";
		}
		first = false;
		c += (*i)->reduce()->fold(&indent);
	}
	dyn_parts.clear();
	delete this;
	
	return new CodeSyntaxNode(SNR_NONE, c);
}

std::string
VtreeParamsSyntaxNode::get_const_cache_key(void) {
	return "vt_" + const_cache_key;
}

SyntaxNode *
VtreeSyntaxNode::apply(SyntaxNode *(*cb)(SyntaxNode *, void *), void *v) {
	if (before)
		before->apply(cb, v);
	if (after)
		after->apply(cb, v);
	if (value)
		value->apply(cb, v);
	return SyntaxNode::apply(cb, v);
}

void
ValVtreeSyntaxNode::resolve_node(pp_queue &ppq) {
	check_and_queue_stree(ppq, before, block);
}

void
ValVtreeSyntaxNode::bind_locals_node(void) {
	if (const_part_local)
		return;

	if (!before->dyn_parts.empty() && !before->const_parts.empty()) {
		std::string cache = get_static_name2(before->get_const_cache_key() + "getnode_const");
		const_part_local = new local_var();

		/*
		 * Assumes it's safe to do cache = const_part_local multiple times.
		 * Can be fixed by adding a lock inside the cleanup if and checking the global again.
		 * Skipping that because it's not needed for current global vtree data.
		 */
		const_part_local->decl = "struct bpapi_vtree_chain " + const_part_local->get_name() + " = {}, *" + const_part_local->get_name() + "_cache = " + cache + "_cache;";
		const_part_local->cleanup = "if (" + const_part_local->get_name() + "_cache && !" + cache + "_cache) {\n"
			"\t" + cache + " = " + const_part_local->get_name() + ";\n"
			"\t" + cache + "_cache = " + const_part_local->get_name() + "_cache == (struct bpapi_vtree_chain*)-1 ? (struct bpapi_vtree_chain*)-1 : &" + cache + ";\n"
			"} else {\n"
			"\tvtree_free(&" + const_part_local->get_name() + ");\n"
			"}";
		block->insert_local(const_part_local);

		if (globals.insert("static struct bpapi_vtree_chain " + cache + ", *" + cache + "_cache;").second)
			clear_cache.insert("vtree_free(&" + cache + ");\n" + cache + ".fun = NULL;\n" + cache + "_cache = NULL;");
	}
}

SyntaxNode *
ValVtreeSyntaxNode::reduce(void) {
	if (after || value)
		ICERR(this, "ValVtree has after or value");

	SyntaxNode *ret;
	SyntaxNode *const_parts;
	SyntaxNode *dyn_parts;
	int indent = 0;
	char dynbuf[50], constbuf[50];

	snprintf(dynbuf, sizeof(dynbuf), "%lu", (unsigned long)before->dyn_parts.size());
	snprintf(constbuf, sizeof(constbuf), "%lu", (unsigned long)before->const_parts.size());

	const_parts = before->reduce_const();
	dyn_parts = before->reduce_dyn();

	if (dyn_parts == NULL) {
		std::string cache = get_static_name2(before->get_const_cache_key() + fun);

		if (globals.insert("static " + cachetype + cache + " = " + cacheclear + ";").second)
			clear_cache.insert(cache + " = " + cacheclear);

		ret = new CodeSyntaxNode(sn_ret, prefix + "vtree_" + fun + "_cachev(&api->vchain, &cacheable, " + inject_dst() + "&" + cache + ", " + constbuf + ", (const char *[]){" + const_parts->reduce()->fold(&indent) + "})" + suffix);
	} else if (const_parts != NULL) {
		ret = new CodeSyntaxNode(sn_ret, prefix + "vtree_" + fun + "_cachev(vtree_getnode_cachev(&api->vchain, &cacheable, &" + const_part_local->get_name() + ", &" + const_part_local->get_name() + "_cache, " + constbuf + ", (const char *[]){" + const_parts->reduce()->fold(&indent) + "}), &cacheable, " + inject_dst() + "NULL, " + dynbuf + ", (const char *[]){ " + dyn_parts->reduce()->fold(&indent) + "})" + suffix);
	} else {
		ret = new CodeSyntaxNode(sn_ret, prefix + "vtree_" + fun + "_cachev(&api->vchain, &cacheable, " + inject_dst() + "NULL, " + dynbuf + ", (const char *[]){" + dyn_parts->reduce()->fold(&indent) + "})" + suffix);
	}
	delete this;
	return ret;
}

std::string
ValVtreeSyntaxNode::inject_dst() {
	return "";
}

void
GetnodeVtreeSyntaxNode::bind_locals_node() {
	if (dst_local)
		return;

	ValVtreeSyntaxNode::bind_locals_node();

	dst_local = new local_var;
	dst_local->decl = "struct bpapi_vtree_chain " + dst_local->get_name() + " = {0};";
	dst_local->cleanup = "vtree_free(&" + dst_local->get_name() + ");";
	block->insert_local(dst_local);
}

std::string
GetnodeVtreeSyntaxNode::inject_dst() {
	return "&" + dst_local->get_name() + ", ";
}

void
LoopVtreeSyntaxNode::resolve_node(pp_queue &ppq) {
	BlockSyntaxNode *b;

	/* It's already resolved, don't bother. */
	if (init_loop)
		return;

	b = block->anchor(level);
	if (!b || !b->block)
		PERR(this, "Outer vtree loop exceeds nesting level.");

	loop_block = b;

	check_and_queue_stree(ppq, before, loop_block);

	NodeSyntaxNode *sn = new NodeSyntaxNode(block, NULL, SNR_NONE, sn_lineno);
	init_loop = loop_block->insert_loop(loop_initializer(sn, (before == NULL || before->dyn_parts.empty()) && (after == NULL || after->dyn_parts.empty()) && (value == NULL || value->dyn_parts.empty())));
	before = after = value = NULL;

	for (b = block; b != loop_block; b = b->block) {
		b->insert_outer(init_loop);
		b->insert_outer_loop(loop_block);
	}

	sn->verify_loop(loop_block);
}

InitLoopSyntaxNode *
KeysLoopVtreeSyntaxNode::loop_initializer(NodeSyntaxNode *sn, bool global) {
	char buf[50];

	snprintf(buf, sizeof(buf), ", %lu, (const char *[]){", (unsigned long)(before->dyn_parts.size() + before->const_parts.size()));
	CodeSyntaxNode::code(sn, buf, CodeSyntaxNode::INLINE);
	sn->add(before);
	CodeSyntaxNode::code(sn, "}", CodeSyntaxNode::INLINE);
	return new VtreeKeysInitLoopSyntaxNode(block, NULL, sn_lineno, loop_block, sn, global);
}
InitLoopSyntaxNode *
ValuesLoopVtreeSyntaxNode::loop_initializer(NodeSyntaxNode *sn, bool global) {
	char buf[50];

	snprintf(buf, sizeof(buf), ", %lu, (const char *[]){", (unsigned long)(before ? before->dyn_parts.size() + before->const_parts.size() : 0) + (after ? after->dyn_parts.size() + after->const_parts.size() : 0) + 1);
	CodeSyntaxNode::code(sn, buf, CodeSyntaxNode::INLINE);
	if (before)
		sn->add(before);
	CodeSyntaxNode::code(sn, ", VTREE_LOOP, ", CodeSyntaxNode::INLINE);
	if (after)
		sn->add(after);
	CodeSyntaxNode::code(sn, "}", CodeSyntaxNode::INLINE);
	return new VtreeValuesInitLoopSyntaxNode(block, NULL, sn_lineno, loop_block, sn, global);
}
InitLoopSyntaxNode *
ByvalLoopVtreeSyntaxNode::loop_initializer(NodeSyntaxNode *sn, bool global) {
	char buf[50];

	sn->add(value);
	snprintf(buf, sizeof(buf), ", %lu, (const char *[]){", (unsigned long)(before ? before->dyn_parts.size() + before->const_parts.size() : 0) + (after ? after->dyn_parts.size() + after->const_parts.size() : 0) + 1);
	CodeSyntaxNode::code(sn, buf, CodeSyntaxNode::INLINE);
	if (before)
		sn->add(before);
	CodeSyntaxNode::code(sn, ", VTREE_LOOP, ", CodeSyntaxNode::INLINE);
	if (after)
		sn->add(after);
	CodeSyntaxNode::code(sn, "}", CodeSyntaxNode::INLINE);
	return new VtreeByvalInitLoopSyntaxNode(block, NULL, sn_lineno, loop_block, sn, global);
}

SyntaxNode *
LoopVtreeSyntaxNode::reduce(void) {
	if (loop_block == NULL)
		ICERR(this, "lost loopvtree loop_block");

	std::string cnt = loop_block->loop_cnt_name();
	std::string len = loop_block->loop_len_name();
	std::string var = init_loop->loop_var_name();

	delete this;
	return new CodeSyntaxNode(SNR_STR, "TMPL_L_VAR(" + var + ", " + cnt + ")");
}

void
LoopVtreeSyntaxNode::verify_loop_node(BlockSyntaxNode *parent_of) {
	BlockSyntaxNode *lb;

	if (!loop_block)
		ICERR(this, "verify_loop_node before resolve");

	for (lb = parent_of->block; lb; lb = lb->block) {
		if (lb == loop_block)
			break;
	}
	if (!lb)
		PERR(this, "Loop used outside scope.");
}
