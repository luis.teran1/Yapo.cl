
#include <string>
#include <sstream>
#include <vector>
#include <iostream>

#include <stdlib.h>

#include <unistd.h>
#include <sys/wait.h>

#include "syntax.h"

std::string
ConstructSyntaxNode::generate_gperf_hash(void) {
	int inpipe[2];
	int outpipe[2];
	int res;
	std::ostringstream indata;
	std::ostringstream outdata;
	char buf[1024];
	std::vector<construct_parameter>::iterator kiter;
	std::string n = get_global_name(NULL);
	construct_parameter *empty_param = NULL;
	construct_parameter *default_param = NULL;

	/* Generate input */
	indata << "struct gperf_ph_elem;\n";
	indata << "%struct-type\n";
	indata << "%define hash-function-name " << n << "_hash\n";
	indata << "%define lookup-function-name lookup_" << n << "\n";
	indata << "%define word-array-name " << n << "_table\n";

	indata << "\n%%\n";

	for (kiter = params->second.begin() ; kiter != params->second.end() ; kiter++) {
		if (!kiter->first)
			default_param = &*kiter;
		else if (*kiter->first == "")
			empty_param = &*kiter;
		else {
			indata << *kiter->first;
			if (params->first)
				indata << ", {\"" << kiter->second << "\"}";
			indata << "\n";
		}
	}

	indata << "%%\n";

	/* Exec gperf */
	if (pipe(inpipe) || pipe(outpipe)) {
		ICERR(this, "Failed to create pipe");
		exit(1);
	}

	switch (fork()) {
	case -1:
		ICERR(this, "Failed to fork");
		exit(1);
	case 0:
		if (dup2(inpipe[0], 0) != 0 || dup2(outpipe[1], 1) != 1) {
			_exit(1);
		}

		close(inpipe[0]);
		close(inpipe[1]);
		close(outpipe[0]);
		close(outpipe[1]);

		execlp("gperf", "gperf", "-L", "ANSI-C", "--global-table", NULL);
		_exit(1);
	}

	close(inpipe[0]);
	close(outpipe[1]);

	/* Write indata */
	if (write(inpipe[1], indata.str().c_str(), indata.str().length()) < (int)indata.str().length()) {
		ICERR(this, "gperf write failed");
		exit(1);
	}
	close(inpipe[1]);

	/* Read result */
	while ((res = read(outpipe[0], buf, sizeof(buf))) > 0) {
		std::string data(buf, res);
		std::string::size_type lookup = data.find("*\nlookup");

		if (lookup != std::string::npos)
			data.insert(lookup, "static ");

		outdata << data;
	}
	if (res < 0) {
		ICERR(this, "gperf read failed");
		exit(1);
	}
	close(outpipe[0]);

	/* Reap gperf */
	::wait(&res);
	if (!WIFEXITED(res) || WEXITSTATUS(res)) {
		ICERR(this, "gperf exited abnormally");
		std::cerr << "gperf indata:\n" << indata.str();
		std::cerr << "gperf outdata:\n" << outdata.str();
		exit(1);
	}

	/* Append wrapper */
	if (!params->first) {
		if (empty_param)
			outdata << "static struct template_opaque_var " << n << "_emptystring;\n";
		if (default_param)
			outdata << "static struct template_opaque_var " << n << "_default;\n";
	}

	outdata << "static " << (params->first ? "const char *" : "struct template_opaque_var *") << "\n";
	outdata << "search_" << n << (index->sn_ret == SNR_STR ? "(const char *key)\n" : "(const struct tpvar *keyvar)\n");
	outdata << "{\n";
	if (index->sn_ret != SNR_STR) {
		outdata << "\tchar *fk;\n";
		outdata << "\tconst char *key = tpvar_str(keyvar, &fk);\n";
	}
	outdata << "\tint len = strlen(key);\n";
	outdata << "\tstruct gperf_ph_elem *elem;\n\n";
	if (empty_param) {
		outdata << "\tif (!len) {\n";
		if (index->sn_ret != SNR_STR)
			outdata << "\t\tfree(fk);\n";
		outdata << "\t\treturn " << (params->first ? "\"" + empty_param->second + "\"" : "&" + n + "_emptystring") << ";\n";
		outdata << "\t}\n\n";
	}
	outdata	<< "\telem = lookup_" << n << "(key, len);\n";
	if (index->sn_ret != SNR_STR)
		outdata << "\tfree(fk);\n";
	outdata << "\tif (elem)\n";
	outdata << "\t\treturn " << (params->first ? "elem->u.value" : "&elem->u.var") << ";\n";
	if (default_param)
		outdata << "\treturn " << (params->first ? "\"" + default_param->second + "\"" : "&" + n + "_default") << ";\n";
	else
		outdata << "\treturn NULL;\n";
	outdata << "}\n";
	outdata << "#undef TOTAL_KEYWORDS\n";
	outdata << "#undef MIN_WORD_LENGTH\n";
	outdata << "#undef MAX_WORD_LENGTH\n";
	outdata << "#undef MIN_HASH_VALUE\n";
	outdata << "#undef MAX_HASH_VALUE\n";

	global_funcs.push_front(outdata.str());

	/* Clear cache */
	if (!params->first) {
		if (empty_param)
			clear_cache.insert("if (" + n + "_emptystring.cleanup)\n\t" + n + "_emptystring.cleanup(&" + n + "_emptystring)");
		clear_cache.insert("{\n\tunsigned int i;\n\tfor (i = 0 ; i < sizeof(" + n + "_table) / sizeof(struct gperf_ph_elem) ; i++) {\n"
				"\t\tif (" + n + "_table[i].u.var.cleanup)\n\t\t\t" + n + "_table[i].u.var.cleanup(&" + n + "_table[i].u.var);\n\t}\n}");
	}

	return "search_" + n;
}

SyntaxNode *
ConstructSyntaxNode::apply(SyntaxNode *(*cb)(SyntaxNode *, void *), void *v) {
	if (index)
		index = index->apply(cb, v);
	return SyntaxNode::apply(cb, v);
}

SyntaxNode *PConstructSyntaxNode::reduce(void) {
	SyntaxNode *ret;

	std::string v = get_global_name(NULL);

	globals.insert("static struct template_opaque_var " + v + ";");
	clear_cache.insert("if (" + v + ".cleanup)\n\t" + v + ".cleanup(&" + v + ")");

	ret = new CodeSyntaxNode(SNR_NONE, "&" + v);

	delete this;
	return ret;
}

void
MapConstructSyntaxNode::convert_node(void) {
	if (index && index->sn_ret != SNR_STR)
		index = index->convert_to_var();
}

SyntaxNode *MapConstructSyntaxNode::reduce(void) {
	SyntaxNode *ret;
	int indent = 0;

	if (!params->second.size())
		PERR(this, "Empty map");
	if (!index)
		PERR(this, "No map index");

	std::string hashfn = generate_gperf_hash();

	ret = new CodeSyntaxNode(sn_ret, hashfn + "(" + index->reduce()->fold(&indent) + ")");

	index = NULL;
	delete this;
	return ret;
}

void
PHConstructSyntaxNode::convert_node(void) {
	if (index && index->sn_ret != SNR_STR)
		index = index->convert_to_var();
}

SyntaxNode *PHConstructSyntaxNode::reduce(void) {
	SyntaxNode *ret;
	int indent = 0;

	if (!params->second.size())
		PERR(this, "Empty perfect hash");
	if (!index)
		PERR(this, "No perfect hash index");

	std::string hashfn = generate_gperf_hash();

	ret = new CodeSyntaxNode(sn_ret, hashfn + "(" + index->reduce()->fold(&indent) + ")");

	index = NULL;
	delete this;
	return ret;
}

SyntaxNode *RegexConstructSyntaxNode::reduce(void) {
	SyntaxNode *ret;

	std::string r = get_global_name(NULL);
	std::string flags;

	if (params->first || params->second.size() < 1 || params->second.size() > 2 || index)
		PERR(this, "Regexp construct takes one or two arguments and no index");

	if (params->second.size() == 2) {
		std::string *fstr = params->second[1].first;
		if (fstr->find('i') != std::string::npos)
			flags += "PCRE_CASELESS | ";
		if (fstr->find('U') != std::string::npos)
			flags += "PCRE_UTF8 | ";
	}
	flags += "0";
	globals.insert("static struct cached_regex " + r + " = { \"" + *params->second[0].first + "\", " + flags + ", NULL };");

	includes.insert("cached_regex.h");

	/* Since we only support static strings, it is not strictly necessary to ever free them, but nice with proper cleanup. */
	clear_cache.insert("cached_regex_cleanup(&" + r + ");");

	ret = new CodeSyntaxNode(SNR_REGEX, "&" + r);

	index = NULL;
	delete this;
	return ret;
}

void
LRUConstructSyntaxNode::convert_node(void) {
	if (index)
		index = index->convert_to_var();
}

void
LRUConstructSyntaxNode::bind_locals_node(void) {
	if (entry_local)
		return;

	gn = get_global_name(NULL);
	globals.insert("static struct lru *" + gn + ";");

	entry_local = new local_var();
	std::string ln = entry_local->get_name();
	block->insert_local(entry_local);

	entry_local->decl = "struct template_lru_entry " + ln + " = { .entry = NULL };";
	entry_local->cleanup = "if (" + ln + ".entry) {\n\tif (" + ln + ".new_entry)\n\t\tlru_store(" + gn + ", " + ln + ".entry, 1);\n\tlru_leave(" + gn + ", " + ln + ".entry);\n}";
}

SyntaxNode *LRUConstructSyntaxNode::reduce(void) {
	SyntaxNode *ret;
	int indent = 0;

	if (params->first || params->second.size() != 1 || atoi(params->second[0].first->c_str()) < 1)
		PERR(this, "LRU takes exactly one numeric argument > 0 and one index");
	if (!index)
		PERR(this, "No LRU index");

	std::string ln = entry_local->get_name();

	includes.insert("tmpl_lru.h");
	clear_cache.insert("if (" + gn + ") {\n\tlru_free(" + gn + ");\n\t" + gn + " = NULL;\n}");

	ret = new CodeSyntaxNode(SNR_NONE, "template_lru_cache(&" + gn + ", " + *params->second[0].first + ", " + index->reduce()->fold(&indent) + ", &" + ln + ")");

	index = NULL;
	delete this;
	return ret;
}

