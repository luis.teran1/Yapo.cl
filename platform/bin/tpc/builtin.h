struct tdirective {
	tdirective() : name(NULL) { };
	std::string *name;
	std::deque<std::string *> args;
};

class DefineTemplateSyntaxNode;

const char *handle_directive(struct tdirective *, DefineTemplateSyntaxNode *sn);
