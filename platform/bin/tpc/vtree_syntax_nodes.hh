#ifndef VTREE_SYNTAX_NODES_HH
#define VTREE_SYNTAX_NODES_HH

#include "syntax.h"

class VtreeParamsSyntaxNode : public SyntaxNode {
public:
	std::deque<StringSyntaxNode *> const_parts;
	std::deque<SyntaxNode *> dyn_parts;
	std::string const_cache_key;

	VtreeParamsSyntaxNode(BlockSyntaxNode *block)
		: SyntaxNode(block, NULL, SNR_NONE, 0) {}

	VtreeParamsSyntaxNode(BlockSyntaxNode *block, SyntaxNode *node)
		: SyntaxNode(block, NULL, SNR_NONE, 0) {
		this->add(node);
	}

	~VtreeParamsSyntaxNode(void) {
		for (std::deque<StringSyntaxNode *>::iterator i = const_parts.begin(); i != const_parts.end(); i++)
			delete *i;
		for (std::deque<SyntaxNode *>::iterator i = dyn_parts.begin(); i != dyn_parts.end(); i++)
			delete *i;
	}

	bool isempty(void) {
		return const_parts.empty() && dyn_parts.empty();
	}
	bool is_transformable(void) {
		return false;
	}

	void add(SyntaxNode *child);

	void convert_node(void);
	SyntaxNode *reduce_const(void);
	SyntaxNode *reduce_dyn(void);
	std::string get_const_cache_key(void);
	SyntaxNode *apply(SyntaxNode *(*cb)(SyntaxNode *, void *), void *);
	SyntaxNode *reduce(void);

	bool operator==(const SyntaxNode &rhs) {
		if (typeid(rhs) != typeid(*this) || sn_ret != rhs.sn_ret)
			return false;
		const VtreeParamsSyntaxNode &r = (const VtreeParamsSyntaxNode&)rhs;
		if (const_parts.size() != r.const_parts.size() || dyn_parts.size() != r.dyn_parts.size())
			return false;
		std::deque<StringSyntaxNode *>::const_iterator ci, rci;
		for (ci = const_parts.begin(), rci = r.const_parts.begin(); ci != const_parts.end(); ci++, rci++) {
			if (!(**ci == **rci))
				return false;
		}
		std::deque<SyntaxNode *>::const_iterator di, rdi;
		for (di = dyn_parts.begin(), rdi = r.dyn_parts.begin(); di != dyn_parts.end(); di++, rdi++) {
			if (!(**di == **rdi))
				return false;
		}
		return true;
	}

	bool operator<(const SyntaxNode &rhs) {
		if (typeid(rhs) != typeid(*this))
			return typeid(rhs).before(typeid(*this));
		if (sn_ret != rhs.sn_ret)
			return sn_ret < rhs.sn_ret;
		const VtreeParamsSyntaxNode &r = (const VtreeParamsSyntaxNode&)rhs;
		if (const_parts.size() != r.const_parts.size())
			return const_parts.size() < r.const_parts.size();
		if (dyn_parts.size() != r.dyn_parts.size())
			return dyn_parts.size() < r.dyn_parts.size();
		std::deque<StringSyntaxNode *>::const_iterator ci, rci;
		for (ci = const_parts.begin(), rci = r.const_parts.begin(); ci != const_parts.end(); ci++, rci++) {
			if (!(**ci == **rci))
				return **ci < **rci;
		}
		std::deque<SyntaxNode *>::const_iterator di, rdi;
		for (di = dyn_parts.begin(), rdi = r.dyn_parts.begin(); di != dyn_parts.end(); di++, rdi++) {
			if (!(**di == **rdi))
				return **di < **rdi;
		}
		return false;
	}

	void dump(int indent) {
		fprintf(stderr, "%*s(", indent * 3, "");
		SyntaxNode::dump(0);
		fprintf(stderr, "%*sconst:\n", indent * 3, "");
		std::deque<StringSyntaxNode *>::const_iterator ci;
		for (ci = const_parts.begin(); ci != const_parts.end(); ci++) {
			(*ci)->dump(indent + 3);
		}
		fprintf(stderr, "%*sdyn:\n", indent * 3, "");
		std::deque<SyntaxNode *>::const_iterator di, rdi;
		for (di = dyn_parts.begin(); di != dyn_parts.end(); di++) {
			(*di)->dump(indent + 3);
		}
		fprintf(stderr, "%*s)\n", indent * 3, "");
	}
};

class VtreeSyntaxNode : public SyntaxNode {
public:
	VtreeParamsSyntaxNode *before;
	VtreeParamsSyntaxNode *after;
	VtreeParamsSyntaxNode *value;		/* XXX - This is evil, but I'm too lazy to do it right. */

	VtreeSyntaxNode(BlockSyntaxNode *block, enum sn_ret ret, int lineno, VtreeParamsSyntaxNode *b, VtreeParamsSyntaxNode *a, VtreeParamsSyntaxNode *v)
		: SyntaxNode(block, NULL, ret, lineno), before(b), after(a), value(v) {}
	~VtreeSyntaxNode(void) {
		delete before;
		delete after;
		delete value;
	}
	
	SyntaxNode *apply(SyntaxNode *(*cb)(SyntaxNode *, void *), void *v);
	virtual SyntaxNode *reduce(void) = 0;
	bool isempty(void) {
		return false;
	}

	bool operator==(const SyntaxNode &rhs) {
		if (typeid(rhs) != typeid(*this) || sn_ret != rhs.sn_ret || block != rhs.block)
			return false;
		const VtreeSyntaxNode &r = (const VtreeSyntaxNode&)rhs;
		return ((before == NULL || r.before == NULL) ? before == r.before : *before == *r.before) && ((after == NULL || r.after == NULL) ? after == r.after : *after == *r.after) && ((value == NULL || r.value == NULL) ? value == r.value : *value == *r.value);
	}

	bool operator<(const SyntaxNode &rhs) {
		if (typeid(rhs) != typeid(*this))
			return typeid(rhs).before(typeid(*this));
		if (sn_ret != rhs.sn_ret)
			return sn_ret < rhs.sn_ret;
		if (block != rhs.block)
			return block < rhs.block;
		const VtreeSyntaxNode &r = (const VtreeSyntaxNode&)rhs;
		if (before == NULL || r.before == NULL) {
			if (before != r.before)
				return before < r.before;
		} else {
			if (!(*before == *r.before))
				return *before < *r.before;
		}
		if (after == NULL || r.after == NULL) {
			if (after != r.after)
				return after < r.after;
		} else {
			if (!(*after == *r.after))
				return *after < *r.after;
		}
		if (value == NULL || r.value == NULL) {
			return value < r.value;
		}
		return *value < *r.value;
	}

	void dump(int indent) {
		fprintf(stderr, "%*s(", indent * 3, "");
		SyntaxNode::dump(0);
		if (before)
			before->dump(indent + 3);
		if (after)
			after->dump(indent + 3);
		if (value)
			value->dump(indent + 3);
		fprintf(stderr, "%*s)\n", indent * 3, "");
	}
};

class ValVtreeSyntaxNode : public VtreeSyntaxNode {
public:
	std::string fun;
	std::string cachetype;
	std::string cacheclear;
	std::string prefix;
	std::string suffix;
	local_var *const_part_local;

	ValVtreeSyntaxNode(BlockSyntaxNode *block, enum sn_ret ret, int lineno, VtreeParamsSyntaxNode *b, const char *f, const char *ct, const char *cc, const char *p, const char *s)
		: VtreeSyntaxNode(block, ret, lineno, b, NULL, NULL), fun(f), cachetype(ct), cacheclear(cc), prefix(p), suffix(s), const_part_local(NULL) {}

	void resolve_node(pp_queue &ppq);
	virtual void bind_locals_node(void);
	SyntaxNode *reduce(void);

	virtual std::string inject_dst();
};

class GetVtreeSyntaxNode : public ValVtreeSyntaxNode {
public:
	GetVtreeSyntaxNode(BlockSyntaxNode *block, int lineno, VtreeParamsSyntaxNode *b) : ValVtreeSyntaxNode(block, SNR_STR, lineno, b , "get", "const char *", "NULL", "(", "?: \"\")") {}
};
class IfdefVtreeSyntaxNode : public ValVtreeSyntaxNode {
public:
	IfdefVtreeSyntaxNode(BlockSyntaxNode *block, int lineno, VtreeParamsSyntaxNode *b) : ValVtreeSyntaxNode(block, SNR_BOOL, lineno, b , "haskey", "int ", "-1", "", "") {}
};
class GetlenVtreeSyntaxNode : public ValVtreeSyntaxNode {
public:
	GetlenVtreeSyntaxNode(BlockSyntaxNode *block, int lineno, VtreeParamsSyntaxNode *b) : ValVtreeSyntaxNode(block, SNR_INT, lineno, b , "getlen", "int ", "-1", "", "") {}
};

class GetnodeVtreeSyntaxNode : public ValVtreeSyntaxNode {
public:
	local_var *dst_local = NULL;

	GetnodeVtreeSyntaxNode(BlockSyntaxNode *block, int lineno, VtreeParamsSyntaxNode *b) : ValVtreeSyntaxNode(block, SNR_NODE, lineno, b , "getnode", "struct bpapi_vtree_chain *", "NULL", "", "") {}

	void bind_locals_node();
	std::string inject_dst();
};

class LoopVtreeSyntaxNode : public VtreeSyntaxNode {
public:
	int level;
	InitLoopSyntaxNode *init_loop;
	BlockSyntaxNode *loop_block;
	LoopVtreeSyntaxNode(BlockSyntaxNode *block, enum sn_ret ret, int lineno, int l, VtreeParamsSyntaxNode *b, VtreeParamsSyntaxNode *a, VtreeParamsSyntaxNode *v)
		: VtreeSyntaxNode(block, ret, lineno, b, a, v), level(l), init_loop(NULL), loop_block(NULL) {}
	void resolve_node(pp_queue &ppq);
	void verify_loop_node(BlockSyntaxNode *parent_of);
	SyntaxNode *reduce(void);
	virtual InitLoopSyntaxNode *loop_initializer(NodeSyntaxNode *code, bool global) = 0;

	bool operator==(const SyntaxNode &rhs) {
		if (typeid(rhs) != typeid(*this) || sn_ret != rhs.sn_ret || block != rhs.block)
			return false;
		const LoopVtreeSyntaxNode &r = (const LoopVtreeSyntaxNode&)rhs;
		return level == r.level && ((before == NULL || r.before == NULL) ? before == r.before : *before == *r.before) && ((after == NULL || r.after == NULL) ? after == r.after : *after == *r.after) && ((value == NULL || r.value == NULL) ? value == r.value : *value == *r.value);
	}

	bool operator<(const SyntaxNode &rhs) {
		if (typeid(rhs) != typeid(*this))
			return typeid(rhs).before(typeid(*this));
		if (sn_ret != rhs.sn_ret)
			return sn_ret < rhs.sn_ret;
		if (block != rhs.block)
			return block < rhs.block;
		const LoopVtreeSyntaxNode &r = (const LoopVtreeSyntaxNode&)rhs;
		if (level != r.level)
			return level < r.level;
		if (before == NULL || r.before == NULL) {
			if (before != r.before)
				return before < r.before;
		} else {
			if (!(*before == *r.before))
				return *before < *r.before;
		}
		if (after == NULL || r.after == NULL) {
			if (after != r.after)
				return after < r.after;
		} else {
			if (!(*after == *r.after))
				return *after < *r.after;
		}
		if (value == NULL || r.value == NULL) {
			return value < r.value;
		}
		return *value < *r.value;
	}
};
class KeysLoopVtreeSyntaxNode : public LoopVtreeSyntaxNode {
public:
	KeysLoopVtreeSyntaxNode(BlockSyntaxNode *block, int lineno, int l, VtreeParamsSyntaxNode *b)
		: LoopVtreeSyntaxNode(block, SNR_STR, lineno, l, b, NULL, NULL) {}
	InitLoopSyntaxNode *loop_initializer(NodeSyntaxNode *code, bool global);
};
class ValuesLoopVtreeSyntaxNode : public LoopVtreeSyntaxNode {
public:
	ValuesLoopVtreeSyntaxNode(BlockSyntaxNode *block, int lineno, int l, VtreeParamsSyntaxNode *b, VtreeParamsSyntaxNode *a)
		: LoopVtreeSyntaxNode(block, SNR_STR, lineno, l, b, a, NULL) {}
	InitLoopSyntaxNode *loop_initializer(NodeSyntaxNode *code, bool global);
};
class ByvalLoopVtreeSyntaxNode : public LoopVtreeSyntaxNode {
public:
	ByvalLoopVtreeSyntaxNode(BlockSyntaxNode *block, int lineno, int l, VtreeParamsSyntaxNode *b, VtreeParamsSyntaxNode *a, VtreeParamsSyntaxNode *v)
		: LoopVtreeSyntaxNode(block, SNR_STR, lineno, l, b, a, v) {}
	InitLoopSyntaxNode *loop_initializer(NodeSyntaxNode *code, bool global);
};

#endif /*VTREE_SYNTAX_NODES_HH*/
