#include <stdio.h>
#include <stdlib.h>
#include <err.h>
#include <unistd.h>

#include <curl/curl.h>

#include <bconf.h>
#include <ctemplates.h>
#include <json_vtree.h>
#include <buf_string.h>

static size_t
wr(char *ptr, size_t size, size_t nmemb, void *v) {
	struct buf_string *b = v;

	return bswrite(b, ptr, size * nmemb);
}

int
main(int argc, char **argv) {
	struct bconf_node *bc = NULL;
	struct bpapi ba;
	struct buf_string b = { 0 };
	CURL *ci;
	int i;
	int ret = 0;
	char ch;
	int extrastats = 0;

	curl_global_init(CURL_GLOBAL_DEFAULT);

	while ((ch = getopt(argc, argv, "s")) != -1) {
		switch (ch) {
		case 's':
			extrastats = 1;
			break;
		default:
			goto usage;
		}
	}

	if (argc < optind + 1) {
usage:
		fprintf(stderr, "%s: <indexer stats url> [<search stats url>]+\n", argv[0]);
		exit(1);
	}

	argv += optind;
	argc -= optind;

	for (i = 0; i < argc; i++) {
		struct bconf_node *sn = NULL;
		char nn[16];
		CURLcode r;

		snprintf(nn, sizeof(nn), "%d", i);

		ci = curl_easy_init();
		curl_easy_setopt(ci, CURLOPT_URL, argv[i]);
		curl_easy_setopt(ci, CURLOPT_WRITEFUNCTION, wr);
		curl_easy_setopt(ci, CURLOPT_WRITEDATA, &b);
		if ((r = curl_easy_perform(ci))) {
			bconf_add_datav(&sn, 2, (const char *[]){ nn, "error" }, curl_easy_strerror(r), 1);
			ret = 1;
		} else {
			if (json_bconf(&sn, nn, b.buf, b.pos, 0))
				errx(1, "json_bconf");
		}
		curl_easy_cleanup(ci);
		free(b.buf);
		memset(&b, 0, sizeof(b));
		bconf_add_datav(&sn, 2, (const char *[]){ nn, "url" }, argv[i], 1);
		bconf_merge_prefix(&bc, i == 0 ? "indexer" : "search", sn);
	}

	BPAPI_INIT_BCONF(&ba, bc, stdout, hash);
	if (extrastats) {
		bpapi_insert(&ba, "extrastats", "1");
	}
	call_template(&ba, "display");

	return ret;
}
