/*
	Reads one or more bconf-style configuration using config_init() and
	a list of patterns. Matched bconf paths and values are output in a
	format suitable to be evaluated by shell scripts.

	$ eval `getbconfvars -f trans.conf --prefix trans_ -k control_port -k max_connections`
	$ echo $trans_control_port
	20207

	Any periods in the resulting key/path will be replaced with underscores.
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <bconfig.h>
#include <bconf.h>
#include <getopt.h>
#include "util.h"
#include "string_functions.h"
#include "cached_regex.h"
#include "tree.h"

struct match_rec {
	SIMPLEQ_ENTRY(match_rec) next;
	char *prefix;
	char *pattern; /* kept around just in case */
	struct cached_regex re;
};

struct path_rec {
	SIMPLEQ_ENTRY(path_rec) next;
	char *path; /* conf node path */
	SIMPLEQ_HEAD(,match_rec) match_list;
};

struct config_rec {
	SIMPLEQ_ENTRY(config_rec) next;
	char *filename;
	SIMPLEQ_HEAD(,path_rec) path_list;
};

static const int MAX_BCONF_PATH_DEPTH = 10;

SIMPLEQ_HEAD(,config_rec) config_list = SIMPLEQ_HEAD_INITIALIZER(config_list);


static struct config_rec *
new_config(const char *filename) {

	struct config_rec *cr = zmalloc(sizeof(struct config_rec));
	SIMPLEQ_INIT(&cr->path_list);
	cr->filename = xstrdup(filename);
	return cr;
}

static void
free_config(struct config_rec *cr) {
	free(cr->filename);
	free(cr);
}

static struct path_rec *
new_path(const char *path) {

	struct path_rec *pr = zmalloc(sizeof(struct path_rec));
	SIMPLEQ_INIT(&pr->match_list);
	if (path)
		pr->path = xstrdup(path);
	return pr;
}

static void
free_path(struct path_rec *pr) {
	free(pr->path);
	free(pr);
}

static struct match_rec *
new_match(const char *prefix, const char *pattern) {

	struct match_rec *mr = zmalloc(sizeof(struct match_rec));
	if (prefix)
		mr->prefix = xstrdup(prefix);
	if (pattern)
		mr->pattern = xstrdup(pattern);
	memset(&mr->re, 0, sizeof(struct cached_regex));
	mr->re.regex = mr->pattern;
	return mr;
}

static void
free_match(struct match_rec *mr) {
	cached_regex_cleanup(&mr->re);
	free(mr->prefix);
	free(mr->pattern);
	free(mr);
}

static void usage(const char* progname)
{
	fprintf(stderr, "Usage: %s --file|-f <name> [--root|-r bconf.path] [--prefix|-p string] --key|-k <regex>\n", progname);
	fprintf(stderr, "Purpose:\n");
	fprintf(stderr, " Export subset of blocket config to environment variables.\n");
	fprintf(stderr, "Arguments:\n");
	fprintf(stderr, " file		-- Input file, blocket config/bconf style.\n");
	fprintf(stderr, " root		-- bconf path to process (optional)\n");
	fprintf(stderr, " prefix		-- Prefix output (optional)\n");
	fprintf(stderr, " key		-- Regex pattern to match in config path.\n");
	fprintf(stderr, "\n");
	fprintf(stderr, "Argument order matters, and arguments can be given more than once.\n");
	fprintf(stderr, "\nExample:\n");
	fprintf(stderr, " $ eval `%s --file trans.conf -p var_ --key control_port`\n", progname);
	fprintf(stderr, " $ echo $var_control_port\n");
	fprintf(stderr, " $ 20207\n");
	fprintf(stderr, "\n");
	exit(1);
}

static int
match(const char *path, const char *value, void *cbdata) {
	struct path_rec *pr = cbdata;

	struct match_rec *match;
	SIMPLEQ_FOREACH(match, &pr->match_list, next) {
		if (cached_regex_match(&match->re, path, NULL, 0)) {
			char *escaped_path = strtrchr(path, ".", '_');
			printf("%s%s=%s\n", match->prefix ?: "", escaped_path, value);
			free(escaped_path);
		}
	}
	return 0;
}

/*
	Build data-structure from arguments, containing all the data we need.

	Note: We will leak small amounts of memory for malformed argument lists,
	such as when given a prefix but no succeeding key to assign it to. While
	easy to fix, it's not worth the effort.
*/
static int parse_options(int argc, char **argv) {
	struct config_rec *curr_config = NULL;
	struct path_rec *curr_path = NULL;
	char *curr_prefix = NULL;
	int opt;

	while (1) {
		int option_index = 0;
		static struct option long_options[] = {
			{"file", 1, NULL, 'f'},
			{"root", 1, NULL, 'r'},
			{"key",  1, NULL, 'k'},
			{"prefix", 1, NULL, 'p'},
			{"help", 0, NULL, 'h'},
			{0, 0, 0, 0}
		};

		opt = getopt_long(argc, argv, "f:r:p:k:h", long_options, &option_index);
		if (opt == -1)
			break;

		switch (opt) {
			case 'f':
				curr_config = new_config(optarg);
				SIMPLEQ_INSERT_TAIL(&config_list, curr_config, next);
				/* Forget old context */
				curr_prefix = NULL;
				curr_path = NULL;
				break;

			case 'r':
				if (curr_config) {
					curr_path = new_path(optarg);
					SIMPLEQ_INSERT_TAIL(&curr_config->path_list, curr_path, next);
				} else {
					fprintf(stderr, "ERROR: root specified before configuration.\n");
					return 1;
				}
				break;

			case 'p':
				curr_prefix = xstrdup(optarg);
				break;

			case 'k':
				if (curr_config) {
					struct match_rec *mr = new_match(curr_prefix, optarg);

					/* Create a default NULL root path if none exist. */
					if (!curr_path) {
						curr_path = new_path(NULL);
						SIMPLEQ_INSERT_TAIL(&curr_config->path_list, curr_path, next);
					}

					SIMPLEQ_INSERT_TAIL(&curr_path->match_list, mr, next);
				} else {
					fprintf(stderr, "ERROR: key specified before configuration.\n");
					return 1;
				}
				break;

			case 'h':
				usage(argv[0]);
				break;
		}
	}

	return 0;
}

int
main(int argc, char **argv) {
	struct bconf_node *cfg_node;
	int retval = 0;

	if (argc < 2)
		usage(argv[0]);

	if ((retval = parse_options(argc, argv)) != 0) {
		return retval;
	}

	struct config_rec *cfg;
	SIMPLEQ_FOREACH(cfg, &config_list, next) {

		if ((cfg_node = config_init(cfg->filename)) == NULL) {
			fprintf(stderr, "Error reading input file '%s'\n", cfg->filename);
			retval = 1;
			break;
		}

		struct path_rec *path;
		SIMPLEQ_FOREACH(path, &cfg->path_list, next) {
			struct bconf_node *path_node = cfg_node;
			if (path->path) {
				path_node = bconf_vget(cfg_node, path->path, NULL);
				if (!path_node) {
					fprintf(stderr, "Path '%s' empty, ignoring.\n", path->path);
					continue;
				}
			}
			bconf_foreach(path_node, MAX_BCONF_PATH_DEPTH, match, path);

		}
		bconf_free(&cfg_node);
	}

	/*
		Spend some time freeing memory.
	*/
	while (!SIMPLEQ_EMPTY(&config_list)) {
		cfg = SIMPLEQ_FIRST(&config_list);

		while (!SIMPLEQ_EMPTY(&cfg->path_list)) {
			struct path_rec *path = SIMPLEQ_FIRST(&cfg->path_list);

			while (!SIMPLEQ_EMPTY(&path->match_list)) {
				struct match_rec *match = SIMPLEQ_FIRST(&path->match_list);
				SIMPLEQ_REMOVE_HEAD(&path->match_list, next);
				free_match(match);
			}

			SIMPLEQ_REMOVE_HEAD(&cfg->path_list, next);
			free_path(path);
		}

		SIMPLEQ_REMOVE_HEAD(&config_list, next);
		free_config(cfg);
	}

	return retval;
}

