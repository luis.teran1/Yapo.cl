/*
	Generate a graphiviz .dot from a serialized bursttrie (.brt)

	Example:
		$ brt2dot trie.brt
		$ dot -otrie.svg -Tsvg -Gcharset=latin1 output.dot
*/
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <unistd.h>
#include <limits.h>
#include <time.h>
#include <sys/time.h>

#include <errno.h>
#include <math.h>

#include "bursttrie_serialized.h"
#include "bursttrie_common.h"
#include "util.h"

static size_t filesize(int fd) {
	struct stat s;
	fstat(fd, &s);
	return s.st_size;
}

struct b2d_state {
	unsigned char* map;
	FILE* f;
	int user_entries;
};

static void sym_to_string(unsigned char sym, char* buf, size_t buflen) {
	if (sym == ' ') {
		snprintf(buf, buflen, "<spc>");
	} else if (sym > ' ' && sym < 0x80) {
		snprintf(buf, buflen, "%c", sym);
	} else {
		snprintf(buf, buflen, "\\<%d\\>", sym);
	}
}

static int generate_levels(struct b2d_state* s, int offset);

static int generate_full(struct b2d_state* s, int offset) {
	char buf[16];
	char nbuf[8];

	// printf("; FULL node at %d\n", offset);

	struct ser_level_header* header = (struct ser_level_header*)(s->map + offset);
	struct ser_level_entry* entries = (struct ser_level_entry*)(s->map + offset + sizeof(struct ser_level_header) + header->nsyms);
	unsigned char* syms = s->map + offset + sizeof(struct ser_level_header);

	fprintf(s->f, "level_%x [ label = \"{{", offset);

	// nsyms 
	fprintf(s->f, "{%d}|", header->nsyms);

	for (int i=0 ; i < header->nsyms ; ++i) {
		sym_to_string(syms[i], buf, sizeof(buf));
		nbuf[0] = '\0';
		if (entries[i].term_count > 0)
			snprintf(nbuf, sizeof(nbuf), "%d", entries[i].term_count);
		fprintf(s->f, "{");
		fprintf(s->f, "%s|%s|<entry_%d>%d", buf, nbuf, i, entries[i].subtree_max_count);
		fprintf(s->f, "}%s", i < header->nsyms-1 ? "|" : "");
	}

	fprintf(s->f, "}}\"];\n\n");

	for (int i=0 ; i < header->nsyms ; ++i) {
		sym_to_string(syms[i], buf, sizeof(buf));
		fprintf(s->f, "level_%x:entry_%d -> level_%x [label=\"%s\"]\n", offset, i, entries[i].offset, buf);
	}

	for (int i=0 ; i < header->nsyms ; ++i) {
		generate_levels(s, entries[i].offset);
	}

	return 1;
}

static int generate_bst(struct b2d_state* s, int offset) {

	struct ser_level_header* header = (struct ser_level_header*)(s->map + offset);
	unsigned char* base = s->map + offset + sizeof(struct ser_level_header);
	struct ser_bst_cntlen* idx = (struct ser_bst_cntlen*)(base + (s->user_entries ? sizeof(struct ser_user_entry)*header->nsyms : 0));
	char*  stringz = (char*)(idx) + sizeof(struct ser_bst_cntlen)*header->nsyms;

	// printf("; BST node at %d\n", offset);

	fprintf(s->f, "level_%x [ label = \"{", offset);
	// nsyms 
	fprintf(s->f, "{%d}|", header->nsyms);
	for (int i=0 ; i < header->nsyms ; ++i) {
		fprintf(s->f, "{");
		fprintf(s->f, "%s|%d", stringz + idx[i].offset, idx[i].count);
		fprintf(s->f, "}%s", i < header->nsyms-1 ? "|" : "");
	}

	fprintf(s->f, "}\" shape=\"Mrecord\"];\n\n");
	return 1;
}

static int generate_levels(struct b2d_state* s, int offset) {

	struct ser_level_header* header = (struct ser_level_header*)(s->map + offset);
	// printf("; offset=%x, type=%d, nsyms=%d\n", offset, header->type, header->nsyms);

	if (header->type == 1)
		generate_full(s, offset);
	else if (header->type == 0)
		generate_bst(s, offset);
	else
		xerrx(1, "Unknown header type");

	return 1;
}

static int generate_header(struct b2d_state* s) {

	struct ser_header* header = (struct ser_header*)s->map;

	fprintf(s->f, "root [ label = \"");
	fprintf(s->f, "{id|%.8s}|", header->id);
	fprintf(s->f, "{size|%d}|", header->tree_size);
	fprintf(s->f, "{keys|%d}|", header->num_keys);
	fprintf(s->f, "{max klen|%d}|", header->max_key_len);
	fprintf(s->f, "{max lvl|%d}|", header->max_level);
	fprintf(s->f, "{offset|%d}|", header->root.offset);
	fprintf(s->f, "{term_count|%d}|", header->root.term_count);
	fprintf(s->f, "{subtree_max_count|%d}|", header->root.subtree_max_count);
	fprintf(s->f, "{root data|%d}}\"\n", header->root_data.data);
	fprintf(s->f, "\n];\n\n");

	fprintf(s->f, "root -> level_%x\n\n", header->root.offset);

	s->map = s->map + sizeof(struct ser_header);
	s->user_entries = header->flags & BRT_SER_USER_DATA;
	generate_levels(s, header->root.offset);

	return 0;
}

static int generate_dot(unsigned char* map, const char* filename) {
	struct b2d_state state;

	state.f = fopen(filename, "w");
	state.map = map;

	fprintf(state.f, "digraph g {\n"
		"graph [ rankdir = \"TB\" ];\n"
		"node [ "
			"fontsize = \"12\"\n"
			"shape = \"record\""
		"];\n"
	);

	generate_header(&state);

	fprintf(state.f, "}\n");

	fclose(state.f);

	return 0;
}


int main(int argc, char* argv[])
{
	const char* infile = argc > 1 ? argv[1] : "suggestions.en.brt";
	const char* outfile = argc > 2 ? argv[2] : "output.dot";

	int fd = open(infile, O_RDONLY);
	if (fd < 0) {
		fprintf(stderr, "Error opening input file '%s' for reading.\n", infile);
		exit(EXIT_FAILURE);
	}

	size_t len = filesize(fd);
	if (len < sizeof(struct ser_header)) {
		fprintf(stderr, "Input file too small to be valid bursttrie (%zu bytes)\n", len);
		close(fd);
		exit(EXIT_FAILURE);
	}

	unsigned char* map = mmap(0, len, PROT_READ, MAP_SHARED, fd, 0);
	if (map == MAP_FAILED) {
		fprintf(stderr, "Failed to mmap infile, errno=%d\n", errno); 
		close(fd);
		exit(EXIT_FAILURE);
	}

	if (strncmp((char*)map, "BRSTRIE0", 8) == 0) {
		generate_dot(map, outfile);
	} else {
		fprintf(stderr, "File signature not recognized.\n");
	}

	if (munmap(map, len) == -1) {
		fprintf(stderr, "Failed to munmap infile, errno=%d\n", errno); 
	}

	close(fd);

	return EXIT_SUCCESS;
}

