/*
	bursttriegen generates a serialized form of the bursttrie based on an input file,
	with some basic filtering on length.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <getopt.h>

#include "bursttrie.h"

static char* infile = NULL;
static char* outfile = NULL;
static char* kv = NULL;
static int minlen = 1;
static int maxlen = 256;
static int limit = 1;

static void exit_level(int level) __attribute__ ((noreturn));

static void usage(const char* progname)
{
	printf("%s --infile <name> --outfile <name> [--limit] [--minlen n] [--maxlen n] [--kv SYM]", progname);
	printf("\n\n");
	printf("  --infile	File to use as input, either a list of words or a list of words and counts delimimted by <kv>\n");
	printf("  --outfile	The serialized representation is written to this file.\n");
	printf("  --kv		Specify the delimiter that separates words and their count.\n");
	printf("  --minlen	Minimum length of key in order to be inserted (default %d)\n", minlen);
	printf("  --maxlen	Maximum length of key in order to be inserted. (default %d)\n", maxlen);
	printf("  --limit	Threshold for # occurences required to a word in the output (default %d)\n", limit);
	printf("\n");
}

static void exit_level(int level)
{
	free(infile);
	free(outfile);
	free(kv);

	exit(level);
}

static void parse_options(int argc, char **argv) {
	int opt;

	while (1) {
		int option_index = 0;
		static struct option long_options[] = {
			{"infile", 1, 0, 0},
			{"outfile", 1, 0, 0},
			{"minlen", 1, 0, 0},
			{"maxlen", 1, 0, 0},
			{"limit", 1, 0, 0},
			{"kv", 1, 0, 0},
			{"help", 0, 0, 0},
			{0, 0, 0, 0}
		};

		opt = getopt_long(argc, argv, "", long_options, &option_index);
		if (opt == -1)
			break;

		if (opt == 0) {
			switch (option_index) {
				case 0:
					infile = strdup(optarg);
					break;
				case 1:
					outfile = strdup(optarg);
					break;
				case 2:
					minlen = atoi(optarg);
					break;
				case 3:
					maxlen = atoi(optarg);
					break;
				case 4:
					limit = atoi(optarg);
					break;
				case 5:
					kv = strdup(optarg);
					break;
				case 6:
					usage(argv[0]);
					exit_level(1);
			}
		}
	}

	int errors = 0;

	if (!infile) {
		fprintf(stderr, "Must specify infile using --infile <filename>\n");
		++errors;
	}

	if (!outfile) {
		fprintf(stderr, "Must specify outfile using --outfile <filename>\n");
		++errors;
	}

	if (maxlen > BRT_MAX_KEY_LENGTH) {
		fprintf(stderr, "maxlen can not be larger than %d.\n", BRT_MAX_KEY_LENGTH);
		++errors;
	}

	if (errors)
		exit_level(1);
}

static int insert_file(struct brt** bt, const char* filename) {

	FILE* f = fopen(filename, "r");
	if (!f)
		return -1;

	char buf[BRT_MAX_KEY_LENGTH];
	char* p = NULL;
	int line = 0;
	int inserted = 0;

	while (!feof(f)) {
		p = fgets(&buf[0], sizeof(buf), f);
		if (!p)
			break;
		++line;
		if ((p = strrchr(buf, '\n')) != NULL)
			*p = '\0';
		if ((p = strrchr(buf, '\f')) != NULL)
			*p = '\0';
		if ((p = strrchr(buf, '\r')) != NULL)
			*p = '\0';
		if (*buf) {
				int	v = 1;
				if (kv) {
					char* vs = strchr(buf, kv[0]);
					if (vs) {
						*vs++ = '\0';
						v = atoi(vs);
					}
				}

				ssize_t slen = strlen(buf);
				if (slen >= minlen && slen <= maxlen) {
					brt_insert_count(bt, &buf[0], v, NULL);
					++inserted;
				}
		}
		buf[0] = '\0';
	}

	fclose(f);
	return inserted;
}

int main(int argc, char* argv[])
{
	if (argc > 1) {
		parse_options(argc, argv);
	} else {
		usage(argv[0]);
		exit_level(1);
	}

	struct brt* bt = NULL;

	int num_words = insert_file(&bt, infile);
	if (num_words < 0) {
		fprintf(stderr, "Failed to open input file '%s' for reading.\n", infile);
		exit_level(EXIT_FAILURE);
	}

	FILE* f = fopen(outfile, "wb");
	if (!f) {
		fprintf(stderr, "Failed to open output file '%s' for writing.\n", outfile);
		exit_level(EXIT_FAILURE);
	}

	size_t len = brt_serialize_to(bt, limit, NULL, NULL, NULL, f, 0 /* NEED TO SUPPORT FLAGS */);
	printf("%d words of '%s' written into a serialized bursttrie '%s' of %zu bytes.\n", num_words, infile, outfile, len);
	fclose(f);

	brt_free(&bt, NULL);

	exit_level(EXIT_SUCCESS);
}

