#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>
#include <errno.h>
#include <stdint.h>
#include <string.h>

#include <getopt.h>

#include "macros.h"
#include "bursttrie_serialized.h"

static char* infile = NULL;
static char* prefix = NULL;
static char* find = NULL;
static char* lev = NULL;
static int limit = 1;
static int debug = 0;
static int userdata = 0;
static int max_k = 2;

void exit_level(int level) __attribute__ ((noreturn));

static void usage(const char* progname)
{
	printf("%s --infile <name> [--prefix <string> | --find <string> | --lev <string>] [--limit n] [--max_k n] [--userdata 0|1] [--debug n]\n", progname);
}

void exit_level(int level)
{
	exit(level);
}

static void parse_options(int argc, char **argv) {
	int opt;

	while (1) {
		int option_index = 0;
		static struct option long_options[] = {
			{"infile", 1, 0, 0},
			{"prefix", 1, 0, 0},
			{"find", 1, 0, 0},
			{"lev", 1, 0, 0},
			{"limit", 1, 0, 0},
			{"max_k", 1, 0, 0},
			{"userdata", 1, 0, 0},
			{"debug", 1, 0, 0},
			{"help", 0, 0, 0},
			{0, 0, 0, 0}
		};

		opt = getopt_long(argc, argv, "", long_options, &option_index);
		if (opt == -1)
			break;

		if (opt == 0) {
			switch (option_index) {
				case 0:
					infile = strdup(optarg);
					break;
				case 1:
					prefix = strdup(optarg);
					break;
				case 2:
					find = strdup(optarg);
					break;
				case 3:
					lev = strdup(optarg);
					break;
				case 4:
					limit = atoi(optarg);
					break;
				case 5:
					max_k = atoi(optarg);
					break;
				case 6:
					userdata = atoi(optarg);
					break;
				case 7:
					debug = atoi(optarg);
					break;
				case 8:
					usage(argv[0]);
					exit_level(1);
			}
		}
	}

	int errors = 0;

	if (!infile) {
		fprintf(stderr, "Must specify infile using --infile <filename>\n");
		++errors;
	}

	if (errors)
		exit_level(1);
}

static size_t filesize(int fd) {
	struct stat s;
	fstat(fd, &s);
	return s.st_size;
}

static unsigned int print_prefix(char* buf, char* str, brt_count count, void* data, void* cbdata UNUSED) {
	if (userdata && data)
		printf("%s%s:%u -> user-data is %d\n", buf, str ? str : "", count, *((uint32_t*)data));
	else
		printf("%s%s:%u\n", buf, str ? str : "", count);
	return 0;
}

static brt_count print_candidate(const char* candidate, brt_count count, int dist, void* data, void* cbdata) {
	printf("%s:%u:%d\n", candidate, dist, count);
	return 0;
}


static int process_map(unsigned char* map) {
	struct ser_header* header = (struct ser_header*)map;

	if (debug) {
		printf("ID '%.8s'\n", header->id);
		printf("Tree size: %d\n", header->tree_size);
		printf("# keys: %d\n", header->num_keys);
		printf("max key len: %d\n", header->max_key_len);
		printf("max level: %d\n", header->max_level);
		printf("flags: ");
		if (header->flags & BRT_SER_USER_DATA)
			printf("BRT_SER_USER_DATA ");
		if (header->flags & BRT_SER_UTF8)
			printf("BRT_SER_UTF8 ");
		printf("\n");
		printf("Root offset: %d\n", header->root.offset);
		printf("Root term_count: %d\n", header->root.term_count);
		printf("Root subtree_max_count: %d\n", header->root.subtree_max_count);
		printf("Root data: %d\n", header->root_data.data);
		printf("; -- sizeof(header) = %zu bytes\n", sizeof(*header));
	}

	void* offset = NULL;

	if (find) {
		printf("%s:%u\n", find, brt_ser_find(map, header->root.offset, find, &offset));
	}

	if (prefix) {
		printf("Prefix search results for '%s' (limit=%d):\n", prefix, limit);
		brt_ser_prefix(map, header->root.offset, prefix, limit, print_prefix, NULL);
	}

	if (lev) {
		printf("Levenshtein search results for '%s' (k=%d, limit=%d):\n", lev, max_k, limit);
		brt_ser_levenshtein(map, lev, max_k, limit, 0, print_candidate, NULL);

	}

	return 0;
}


int main(int argc, char* argv[])
{
	if (argc > 1) {
		parse_options(argc, argv);
	} else {
		usage(argv[0]);
		exit_level(1);
	}

	int fd = open(infile, O_RDONLY);
	if (fd < 0) {
		fprintf(stderr, "Error opening input file '%s' for reading.\n", infile);
		exit_level(EXIT_FAILURE);
	}

	size_t len = filesize(fd);
	if (len < sizeof(struct ser_header)) {
		fprintf(stderr, "Input file too small to be valid bursttrie (%zu bytes)\n", len);
		close(fd);
		exit_level(EXIT_FAILURE);
	}

	unsigned char* map = mmap(0, len, PROT_READ, MAP_SHARED | MAP_POPULATE, fd, 0);
	if (map == MAP_FAILED) {
		fprintf(stderr, "Failed to mmap infile, errno=%d\n", errno); 
		close(fd);
		exit_level(EXIT_FAILURE);
	}

	if (strncmp((char*)map, "BRSTRIE0", 8) == 0) {
		process_map(map);
	} else {
		fprintf(stderr, "File signature not recognized.\n");
	}

	if (munmap(map, len) == -1) {
		fprintf(stderr, "Failed to munmap infile, errno=%d\n", errno); 
	}
	close(fd);

	exit_level(EXIT_SUCCESS);
}

