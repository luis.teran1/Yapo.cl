
#include <getopt.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/poll.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/stat.h>

#include <signal.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <unistd.h>
#include <stdio.h>
#include <fcntl.h>
#include <errno.h>
#include <libintl.h>
#include <limits.h>
#include <syslog.h>
#include <sys/wait.h>
#include <time.h>
#include <malloc.h>
#include <netdb.h>
#include <locale.h>

#include "event.h"
#include "eventextra.h"
#include "trans.h"
#include "command.h"
#include "util.h"
#include "bconfig.h"
#include "logging.h"
#include "bconf.h"
#include "daemon.h"
#include "fd_pool.h"
#include "bpapi.h"
#include "garb.h"

struct listener {
	struct event accept_ev;
	int fd;
	void (*cb)(int fd, const char *rhost, const char *rport);
};

static void transaction_handle(struct transaction *);
static void transaction_end(struct transaction *);

int transaction_haskey(struct transaction *, const char *);
const char *transaction_getval(struct transaction *, const char *);


static void transaction_to(int, short, void *);

static void transaction_internal_read_cb(int fd, short what, void *v);

struct transaction *new_connection(int, struct internal_cmd *cmd, const char *peer, int is_control);

static void accept_event(int, short, void *);
static void new_conn_normal(int fd, const char *rhost, const char *rport);
static void new_conn_control(int fd, const char *rhost, const char *rport);

static void read_ev(struct bufferevent *, void *);
static void write_ev(struct bufferevent *, void *);
static void err_ev(struct bufferevent *, short, void *);

static void init_backends(char *check_only);
static void init_done_backends(void);
static void deinit_backends(void);
static int  config_backends(struct bconf_node *);

static void stdin_input(void);

static int read_config(const char*);
static int read_config_string(char** dest, struct bconf_node *conf, const char *key);
static int read_config_int(struct bconf_node *conf, const char *key, int default_value);


static void config_reload(void *);

static void reload_clean(void);

void usage(const char *);


enum debug_level debug_level = D_INFO;

static int listen_stdin = -1;
static int nobos = 0;
static int foreground = 0;
int keepalive = 0;
int flag_nodav = 0;
char *check_only = NULL;
int use_stdin = 0;
char *trans_user;
const unsigned char *pcre_table;

int init_status = 0;

struct listener nlistener[10];
int nnlisteners = 0;
struct listener clistener[10];
int nclisteners = 0;

static int parse_commandline(int, char **);

int mainloop(void);

extern struct event_base *current_base;
struct event_base *trans_base = NULL;
struct event_base *init_reload_base = NULL;

static struct event hupev;
static struct event usr1ev;
static struct event termev;

/* static struct event pipeev; */
static void sighup_handler(int, short, void *);
static void sigusr1_handler(int, short, void *);
static void sigterm_handler(int, short, void *);

struct bconf_node *trans_conf;

int reload_config = 0;
pthread_mutex_t reload_config_lock;
pthread_cond_t reload_config_signal;

int port_toggle = 0;
pthread_mutex_t port_toggle_lock;
pthread_cond_t port_toggle_signal;

int graceful_shutdown = 0;

extern pthread_key_t csbase_key;
extern pthread_key_t log_string_key;

static struct t_connection_state {
	int	pending_external_cnt;
	int	pending_internal_cnt;
} connection_state;

TAILQ_HEAD(,transaction) pending_transactions = TAILQ_HEAD_INITIALIZER(pending_transactions);
pthread_mutex_t pending_transactions_lock = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t pending_transactions_signal = PTHREAD_COND_INITIALIZER;
static int refuse_new_connections;

/* Tracks the last time we dumped debug info on 'busy', for rate-limiting */
static time_t debug_dump_time;
pthread_mutex_t debug_dump_time_lock;

static void
set_trans_user(void) {
	char host[100];

	if (gethostname(host, 100) == -1)
		xerr(1, "gethostname");
	xasprintf(&trans_user, "%u@%s", getpid(), host);
}

static void
free_config(void) {
	free(config.loglevel);
	free(config.logtag);

	free(config.basedir);

	free(config.port);
	free(config.failover_port);
	free(config.control_port);
	free(config.control_failover_port);

	free(config.filename);
}

static void
cleanup_main(void) {
	closelog();
	free_config();
	free(check_only);
}

static void
log_reconfigure(struct trans_config *tc) {
	if (foreground)
		x_err_init_err();
	else
		x_err_init_syslog(tc->logtag, tc->logoption, LOG_LOCAL0, LOG_INFO);
	log_setup(tc->logtag, tc->loglevel);
}

int
main(int argc, char **argv) {
	int rc = 0;

	bindtextdomain("blocket", NULL);

	parse_commandline(argc, argv);

	if (!config.logtag)
		config.logtag = xstrdup(PROG);
	if (!config.loglevel)
		config.loglevel = xstrdup("info");

	atexit(cleanup_main);

	/* Setup default logging. read_config() will reinit once configuration is available. */
	config.logoption = LOG_CONS | LOG_PID;
	log_reconfigure(&config);

	if (read_config(config.filename))
		xerrx(1, "configuration, read_config failed");

	srand(time(NULL) ^ getpid());

	if (listen_stdin == -1) {
		if (foreground) {
			do_switchuid();
			if (nobos) {
				rc = mainloop();
			} else {
				rc = bos(mainloop);
			}
		} else {
			config.logoption = 0;
			log_reconfigure(&config);
			daemonify(nobos, mainloop);
		}
	} else {
		x_err_init_err();
		do_switchuid();
		rc = mainloop();
	}

	return rc;
}

/* Signal handling for --stdin (graceful shutdown) */
static struct event termev;
static void
stdin_sigterm_handler(int fd, short ev, void *v) {
	event_loopexit(NULL);
}

static int
wait_pending_transactions(int in_reload) {
	int res = 1;

	/* Note: You must hold pending_transactions_lock when calling this. */

	if (!TAILQ_EMPTY(&pending_transactions)) {
		DPRINTF(D_INFO, ("Waiting for transactions to finish"));
	}

	while (!TAILQ_EMPTY(&pending_transactions)) {
		struct timeval tv;
		struct timespec ts;

		if (in_reload && (graceful_shutdown || port_toggle)) {
			res = 0;
			break;
		}

		/* Wait at most 1 second */
		gettimeofday(&tv, NULL);
		ts.tv_sec = tv.tv_sec + 1;
		ts.tv_nsec = tv.tv_usec * 1000;
		pthread_cond_timedwait(&pending_transactions_signal, &pending_transactions_lock, &ts);

		pthread_mutex_unlock(&pending_transactions_lock);
		/* Check for signals and unhandled transactions */
		event_base_loop(trans_base, EVLOOP_NONBLOCK);
		pthread_mutex_lock(&pending_transactions_lock);
	}

	return res;
}

static int
listen_port(struct listener *ls, int maxls, const char *port, void (*newconn_cb)(int, const char*, const char*))
{
	const struct addrinfo hints = { .ai_flags = AI_PASSIVE | AI_ADDRCONFIG, .ai_socktype = SOCK_STREAM };
	struct addrinfo *res = NULL, *curr;
	int nls;
	int r;

	if ((r = getaddrinfo(NULL, port, &hints, &res)) || !res)
		xerrx(1, "getaddrinfo: %s (port:%s)", gai_strerror(r), port);

	nls = 0;
	for (curr = res ; curr && nls < maxls ; curr = curr->ai_next) {
		int sock;
		int val;

		if ((sock = socket(curr->ai_family, curr->ai_socktype, curr->ai_protocol)) < 0) {
			xwarn("socket");
			continue;
		}

		val = 1;

		if (setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &val, sizeof(val)) < 0)
			xerr(1, "setsockopt(SO_REUSEADDR)");
		if (curr->ai_family == AF_INET6 && setsockopt(sock, IPPROTO_IPV6, IPV6_V6ONLY, &val, sizeof(val)) < 0)
			xerr(1, "setsockopt(IPV6_V6ONLY)");

		if ((val = fcntl(sock, F_GETFL, 0)) < 0)
			xerr(1, "fcntl(F_GETFL)");

		val |= O_NONBLOCK;
		if (fcntl(sock, F_SETFL, val) < 0)
			xerr(1, "fcntl(F_SETFL)");

		val = FD_CLOEXEC;
		if (fcntl(sock, F_SETFD, val) < 0)
			xerr(1, "fcntl(F_SETFD)");

		if (bind(sock, curr->ai_addr, curr->ai_addrlen))
			xerr(1, "bind");

		if (listen(sock, 100) < 0)
			xerr(1, "listen");

		ls[nls].fd = sock;
		ls[nls].cb = newconn_cb;

		event_set(&ls[nls].accept_ev, sock, EV_READ|EV_PERSIST, accept_event, &ls[nls]);
		event_add(&ls[nls].accept_ev, NULL);
		nls++;
	}

	freeaddrinfo(res);
	return nls;
}

static void
close_listeners(struct listener *ls, int nls) {
	int ils;

	for (ils = 0 ; ils < nls ; ils++)
	{
		event_del(&ls[ils].accept_ev);
		close(ls[ils].fd);
	}
}

static void
print_version_information(void) {
	DPRINTF(D_INFO, ("Version information (static/dynamic):"));

	const char *vs = "Unknown";
#if defined _EVENT_VERSION
	vs = _EVENT_VERSION;
#elif defined LIBEVENT_VERSION
	vs = LIBEVENT_VERSION;
#endif
	DPRINTF(D_INFO, ("libevent %s / %s", vs, event_get_version()));

#if PCRE_MAJOR >= 8
	DPRINTF(D_INFO, ("pcre %d.%d %s / %s", PCRE_MAJOR, PCRE_MINOR, MACRO_TO_STRING(PCRE_DATE), pcre_version()));
#endif

#if TC_VERSION_MAJOR >= 2
	DPRINTF(D_INFO, ("tcmalloc %s / %s", TC_VERSION_STRING, tc_version(NULL, NULL, NULL));
#endif
}

int
mainloop(void) {
	int port_toggled = 0, ret;
	const char *cur_locale = NULL;
	char *old_locale = NULL;

	set_trans_user();

	init_reload_base = event_init();
	trans_base = event_init();

	if (listen_stdin == -1) {
		signal_set(&hupev, SIGHUP, sighup_handler, NULL);
		signal_add(&hupev, NULL);

		signal_set(&usr1ev, SIGUSR1, sigusr1_handler, NULL);
		signal_add(&usr1ev, NULL);

		signal_set(&termev, SIGTERM, sigterm_handler, NULL);
		signal_add(&termev, NULL);
	}

	if (!textdomain("blocket")) {
		xerr(1, "textdomain");
	}

	if (config_backends(trans_conf) < 0)
		xerrx(1, "backend configuration failed");

	reload_register(config_reload, NULL);

	current_base = init_reload_base;

	cur_locale = setlocale(LC_ALL, NULL);
	old_locale = xstrdup(cur_locale ? cur_locale : "");
	setlocale(LC_ALL, "");
	pcre_table = pcre_maketables();
	setlocale(LC_ALL, old_locale);
	free(old_locale);

	print_version_information();

	preinit_command();

	init_backends(check_only);
	if (!init_status)
		init_commands(check_only);

	while (event_base_dispatch(init_reload_base) != 1)
		;
	if (check_only || init_status)
		return init_status;

	init_done_backends();
	while (event_base_dispatch(init_reload_base) != 1) { continue; }

	current_base = trans_base;

	if (debug_level == D_DEBUG) {
		/* Disable sbrk reducing when running in regress, for memory leak check */
		mallopt(M_TRIM_THRESHOLD, INT_MAX);
	}

	if (listen_stdin == -1) {
		nnlisteners = listen_port(nlistener, 10, config.port, new_conn_normal);
		if (!nnlisteners)
			xerrx(1, "Failed to open any listeners");
		nclisteners = listen_port(clistener, 10, config.control_port, new_conn_control);
		if (!nclisteners)
			xerrx(1, "Failed to open any control listeners");

		DPRINTF(D_INFO, ("Accepting connections"));
	} else {
		signal_set(&termev, SIGTERM, stdin_sigterm_handler, NULL);
		signal_add(&termev, NULL);
		new_connection(listen_stdin, NULL, "stdin", 0);
		event_base_dispatch(trans_base);

		pthread_mutex_lock(&pending_transactions_lock);
		wait_pending_transactions(0);
		DPRINTF(D_INFO, ("No more transactions in listen stdin"));
		pthread_mutex_unlock(&pending_transactions_lock);

		return 0;
	}

	while (1) {
		event_base_dispatch(trans_base);
		DPRINTF(D_DEBUG, ("Main event_dispatch returned"));

		if (reload_config) {
			struct event_base *tmp_base = current_base;

			pthread_mutex_lock(&reload_config_lock);
			pthread_mutex_lock(&pending_transactions_lock);
			/* Wait for all pending transactions */
			if ( (ret = wait_pending_transactions(1)) ) {
				tmp_base = current_base;

				DPRINTF(D_INFO, ("No more transactions in reload config"));

				current_base = init_reload_base;

				call_reload_handlers(); 
				event_base_dispatch(init_reload_base);
			}
			pthread_mutex_unlock(&pending_transactions_lock);

			if (ret) {
				call_post_reload_handlers();
				event_base_dispatch(init_reload_base);
				current_base = tmp_base;
			}

			reload_config = 0;
			pthread_mutex_unlock(&reload_config_lock);
			pthread_cond_broadcast(&reload_config_signal);
		}
		if (graceful_shutdown) {
			DPRINTF(D_INFO, ("Graceful shutdown started, waiting for transactions"));
			pthread_mutex_lock(&pending_transactions_lock);
			close_listeners(nlistener, nnlisteners);
			DPRINTF(D_INFO, ("Listener closed"));
			/* Wait for all pending transactions */
			wait_pending_transactions(0);
			DPRINTF(D_INFO, ("No more transactions in graceful shutdown"));
			pthread_mutex_unlock(&pending_transactions_lock);
			break;
		}
		if (port_toggle && !port_toggled) {
			port_toggled = 1;

			close_listeners(nlistener, nnlisteners);
			nnlisteners = listen_port(nlistener, 10, config.failover_port, new_conn_normal);
			close_listeners(clistener, nclisteners);
			nclisteners = listen_port(clistener, 10, config.control_failover_port, new_conn_control);

			pthread_mutex_lock(&port_toggle_lock);
			port_toggle = 0;
			pthread_mutex_unlock(&port_toggle_lock);
			pthread_cond_broadcast(&port_toggle_signal);
		}
	}

	close_listeners(clistener, nclisteners);

	reload_clean();
	reset_commands_data();
	cleanup_commands();
	deinit_backends();
	cleanup_garb();
	flush_template_cache(NULL);
	flush_template_list();
	timer_clean();

	bconf_free(&trans_conf);

	if(init_reload_base != NULL)
		event_base_free(init_reload_base);
	if(trans_base != NULL)
		event_base_free(trans_base);

	free(trans_user);
	pcre_free((void*)pcre_table);

	return 1;
}

void
usage(const char *name) {
	printf("Usage: %s [--config configfile] [--debug] [--stdin] [--nobos] [--foreground | -f] [--pidfile | -p pidfile] [--coresize coresize] [--check-only=bconf,dbprocs] [--help] [--logtag name] [--loglevel <info|debug>]\n", name);
}

int
parse_commandline(int argc, char **argv) {
	int opt;

	set_pidfile("/var/run/trans.pid");

	while (1) {
		int option_index = 0;
		static struct option long_options[] = {
			{"debug", 0, 0, 0},
			{"stdin", 0, 0, 0},
			{"nobos", 0, 0, 0},
			{"foreground", 0, 0, 'f'},
			{"config", 1, 0, 0},
			{"keepalive", 0, 0, 0},
			{"nodav", 0, 0, 0},
			{"coresize", 1, 0, 0},
			{"help", 0, 0, 0},
			{"pidfile", 1, 0, 'p'},
			{"uid", 1, 0, 'u'},
			{"check-only", 1, 0, 0},
			{"quick-start", 0, 0, 0},
			{"logtag", 1, 0, 0},
			{"syslog", 1, 0, 0},
			{"loglevel", 1, 0, 0},
			{0, 0, 0, 0}
		};

		opt = getopt_long(argc, argv, "fp:u:", long_options, &option_index);
		if (opt == -1)
			break;
		if (opt) {
			switch (opt) {
			case 'f':
				foreground = 1;
				break;
			case 'p':
				set_pidfile(optarg);
				break;
			case 'u':
				set_switchuid(optarg);
				break;
			default:
				usage(argv[0]);
				exit(1);
			}
		} else {
			switch (option_index) {
			case 0:
				debug_level = D_DEBUG;
				break;
			case 1:
				nobos = 1;
				use_stdin = 1;
				break;
			case 2:
				nobos = 1;
				break;
			case 3:
				foreground = 1;
				break;
			case 4:
				config.filename = xstrdup(optarg);
				break;
			case 5:
				keepalive = 1;
				break;
			case 6:
				flag_nodav = 1;
				break;
			case 7:
				set_coresize(atoi(optarg));
				break;
			case 11:
				check_only = xstrdup(optarg);
				break;
			case 12:
				set_quick_start(1);
				break;
			case 13:
			case 14:
				config.logtag = xstrdup(optarg);
				break;
			case 15:
				config.loglevel = xstrdup(optarg);
				break;
			default:
				usage(argv[0]);
				exit(1);
			}
		}
	}

	if (use_stdin)
		stdin_input();

	if (!config.filename) 
		config.filename = xstrdup(PROG ".conf");

	return 0;
}

struct transaction *
new_connection(int fd, struct internal_cmd *cmd, const char *peer, int is_control)
{
	struct transaction *ts;
	struct timeval tv;

	if (((fd >= 0) ^ (cmd != NULL)) != 1)
		xerr(1, "new_connection: must give either fd or cmd, but not both");

	ts = zmalloc(sizeof(*ts));
	ts->log = 1;
	ts->ts_fd = fd;
	ts->start_time = time(NULL);

	ts->peer = xstrdup(peer);

	TAILQ_INIT(&ts->ts_elems);
	ts->ts_blob = NULL;
	ts->ts_blob_size = 0;
	ts->ts_state = TS_BANNER;
	pthread_mutex_lock(&pending_transactions_lock);
	if (is_control)
		ts->is_control = 1;
	else
		TAILQ_INSERT_TAIL(&pending_transactions, ts, ts_list);
	if (cmd)
		++connection_state.pending_internal_cnt;
	else
		++connection_state.pending_external_cnt;
	pthread_mutex_unlock(&pending_transactions_lock);

	if (cmd) {
		/* Internal commands have no bufferevent/fd */
		ts->internal = cmd;
		if (cmd->log_string) {
			ts->log_string = xstrdup(cmd->log_string);
		} else {
			const char *log_string = pthread_getspecific(log_string_key);

			if (log_string)
				ts->log_string = xstrdup(log_string);
		}
		transaction_write(ts, TRANSACTION_BANNER_OK, sizeof(TRANSACTION_BANNER_OK) - 1);
		transaction_write(ts, "\n", 1);
		return ts;
	}

	if (!(ts->ts_buf = bufferevent_new(fd, read_ev, write_ev, err_ev, ts)))
		xerr(1, "bufferevent_new");
	evtimer_set(&ts->ts_timeout, transaction_to, ts);
	tv.tv_sec = TRANSACTION_TIMEOUT;
	tv.tv_usec = 0;
	evtimer_add(&ts->ts_timeout, &tv);

	DPRINTF(D_DEBUG, ("connection warning perc : %d%% ; Nb connections %d ; Max_connections %d", config.connection_warning_perc, connection_state.pending_external_cnt, config.max_connections));
	if (connection_state.pending_external_cnt >= (config.connection_warning_perc * config.max_connections / 100.0)) {
		DPRINTF(D_WARNING, ("Warning, reached more than %d %% of max connections : %d connections", config.connection_warning_perc, connection_state.pending_external_cnt));
	}

	if (!ts->is_control && (refuse_new_connections || connection_state.pending_external_cnt >= config.max_connections)) {
		DPRINTF(D_DEBUG, ("521 Busy."));
		transaction_write(ts, TRANSACTION_BANNER_GO_AWAY, sizeof(TRANSACTION_BANNER_GO_AWAY) - 1);
		transaction_write(ts, "\n", 1);
		ts->ts_state = TS_DONE;
		/* Limit dumping to log to about once every debug_dump_delay seconds */
		pthread_mutex_lock(&debug_dump_time_lock);
		if (ts->start_time >= debug_dump_time + config.debug_dump_delay) {
			transaction_transinfo_log(ts);
			transaction_cmdinfo_log(ts);
			backend_transinfo(ts);
			debug_dump_time = ts->start_time;
		}
		pthread_mutex_unlock(&debug_dump_time_lock);

	} else {
		transaction_write(ts, TRANSACTION_BANNER_OK, sizeof(TRANSACTION_BANNER_OK) - 1);
		transaction_write(ts, "\n", 1);
	}
	bufferevent_disable(ts->ts_buf, EV_READ);
	bufferevent_enable(ts->ts_buf, EV_WRITE);
	return ts;
}

static void
accept_event(int lsock, short _ev, void *v)
{
	struct timer_instance *ti = timer_start(NULL, "accept_event");
	struct listener *ls = v;
	struct sockaddr_storage addr;
	socklen_t addrlen;
	int fd;
	long val;
	char host[NI_MAXHOST];
	char serv[NI_MAXSERV];

	addrlen = sizeof(addr);

	DPRINTF(D_DEBUG, ("incoming connection"));

	memset(&addr, 0, sizeof(addr));
	fd = accept(ls->fd, (struct sockaddr *)&addr, &addrlen);
	if (fd < 0) {
		DPRINTF(D_WARNING, ("(WARN) accept_event: accept failed: %s", xstrerror(errno)));
		timer_add_attribute(ti, "failed");
		timer_end(ti, NULL);
		return;
	}

	val = FD_CLOEXEC;

	if (fcntl(fd, F_SETFD, val) < 0) {
		DPRINTF(D_WARNING, ("(WARN) accept_event: fcntl(F_SETFD)"));
		close(fd);
		return;
	}

	if ((val = fcntl(fd, F_GETFL, 0)) < 0) {
		DPRINTF(D_WARNING, ("(WARN) accept_event: GETFL failed: %s", strerror(errno)));
		close(fd);
		return;
	}
	val |= O_NONBLOCK;
	if (fcntl(fd, F_SETFL, val) < 0) {
		DPRINTF(D_WARNING, ("(WARN) accept_event: SETFL failed: %s", strerror(errno)));
		close(fd);
		return;
	}

	timer_end(ti, NULL);

	if (getnameinfo((struct sockaddr*)&addr, addrlen, host, sizeof(host),
				serv, sizeof(serv), NI_NUMERICHOST | NI_NUMERICSERV) != 0) {
		strcpy(host, "unknown");
		strcpy(serv, "unknown");
	}

	ls->cb(fd, host, serv);
}

static void
new_conn_normal(int fd, const char *rhost, const char *rport) {
	struct transaction *ts;
	char peer[256];

	snprintf(peer, sizeof(peer), "[%s]:%s", rhost, rport);
	ts = new_connection(fd, NULL, peer, 0);
	if (ts)
		transaction_logprintf(ts, D_INFO, "Connection from %s", rhost);
}

static void
new_conn_control(int fd, const char *rhost, const char *rport) {
	struct transaction *ts;
	char peer[256];

	snprintf(peer, sizeof(peer), "[%s]:%s", rhost, rport);
	ts = new_connection(fd, NULL, peer, 1);
	if (ts)
		transaction_logprintf(ts, D_INFO, "Control connection from %s", rhost);
}

#define NEWLINE_KEY "newline:"
#define NEWLINE_KEY_SZ (sizeof (NEWLINE_KEY) - 1)

static void
read_ev(struct bufferevent *be, void *v)
{
	struct transaction *ts = v;
	struct elem *el = NULL;
	char *line;
	size_t r;
	struct evbuffer *input = be ? be->input : ts->internal->inbuf;

again:

	if (ts->ts_state >= TS_READDONE) {
		transaction_abort(ts, "read_ev: read done");
		return;
	}

	if (ts->ts_state == TS_NEWLINE) {
		unsigned int len = EVBUFFER_LENGTH(input) < NEWLINE_KEY_SZ ?
			EVBUFFER_LENGTH(input) : NEWLINE_KEY_SZ;
		/* Peek at data to see if it is a newline key. */

		if (strncmp((char*) EVBUFFER_DATA(input), NEWLINE_KEY, len) != 0)
			ts->ts_state = TS_HEAD;
		else if (len < NEWLINE_KEY_SZ)
			return; /* Wait for more data */
		else {
			char *logline;

			/* Newline found */
			while (len < EVBUFFER_LENGTH(input) && isspace(EVBUFFER_DATA(input)[len]))
				len++;
			if (len == EVBUFFER_LENGTH(input))
				return; /* No non-whitespace found. Wait for more data. */
			len -= NEWLINE_KEY_SZ;
			ts->newline = xmalloc(len);
			memcpy(ts->newline, EVBUFFER_DATA(input) + NEWLINE_KEY_SZ, len);
			ts->newline_len = len;
			evbuffer_drain(input, NEWLINE_KEY_SZ + len);

			/* Create hex output of the newline. */
			logline = xmalloc(len * 2 + 1);
			logline[len * 2] = '\0';
			while (len--) {
				logline[len * 2 + 1] = (ts->newline[len] & 0x0F) + ((ts->newline[len] & 0x0F) >= 0x0A ? 'A' - 10 : '0');
				logline[len * 2] = ((ts->newline[len] & 0xF0) >> 4) + ((ts->newline[len] & 0xF0) >> 4 >= 0x0A ? 'A' - 10 : '0');
			}
			transaction_logprintf(ts, D_INFO, "<< " NEWLINE_KEY "%s", logline);
			free(logline);

			ts->ts_state = TS_HEAD;
		}
	}

	while (ts->ts_state == TS_HEAD && (line = evbuffer_read_newline(input, ts->newline, ts->newline_len)) != NULL) {
		char *key, *value;

		transaction_logprintf(ts, D_INFO, "<< %s", line);

		/*
		 * Skip empty lines and comments.
		 */
                if (line[0] == '#' || line[0] == '\0') {
			free(line);
			line = NULL;
			continue;
                }

		key = line;
		value = strchr(line, ':');

		if (value != NULL)
			*value++ = '\0';

		/*
		 * 
		 */
		if (!strcmp(key, "log_string")) {
			if (ts->log_string)
				free(ts->log_string);
			ts->log_string = xstrdup(value);
			free(line);
			line = NULL;
			continue;
		}


		/*
		 * special case. If the key is 'end' The request is
		 * over.
		 */
		if (!strcmp(key, "end")) {
			ts->ts_state = TS_READDONE;
			free(line);
			line = NULL;
			break;
		}

		/*
		 * Special case. If the key is "blob", the value
		 * specifies the number of bytes we want to read
		 * from the blob.
		 */
		if (!strcmp(key, "blob")) {
			long sz;
			char *ep;
			size_t klen;

			if (value == NULL || *value == '\0') {
				transaction_abort(ts, "no blob size");
				free(line);
				line = NULL;
				return;
			}

			key = strchr(value, ':');
			if (key == NULL) {
				transaction_abort(ts, "botched blob");
				free(line);
				line = NULL;
				return;
			}
			*key++ = '\0';

			sz = strtol(value, &ep, 10);
			if (*ep != '\0') {
				transaction_abort(ts, "blob size NaN");
				free(line);
				line = NULL;
				return;
			}
			if (sz < 0 || sz > MAX_BLOB) {
				transaction_abort(ts, "blob bad size");
				free(line);
				line = NULL;
				return;
			}

			klen = strlen(key);

			el = xmalloc(sizeof(*el) + klen + 1 + sz + 1);
			el->el_key = (char *)(el + 1);
			memcpy(el->el_key, key, klen);
			el->el_key[klen] = '\0';
			ts->ts_blob = el->el_key + klen + 1;
			ts->ts_blob[sz] = '\0';
			el->el_value = ts->ts_blob;
			el->el_size = sz;

			ts->ts_blob_size = sz;
			ts->ts_blob_index = 0;
			ts->ts_state = TS_BLOB;
		} else {
			size_t klen, vlen;

			klen = strlen(key);
			vlen = value != NULL ? strlen(value) : 0;
			el = xmalloc(sizeof(*el) + klen + 1 + vlen + 1);

			el->el_key = (char *)(el + 1);
			memcpy(el->el_key, key, klen);
			el->el_key[klen] = '\0';
			if (value) {
				el->el_value = el->el_key + klen + 1;
				memcpy(el->el_value, value, vlen);
				el->el_value[vlen] = '\0';
			} else {
				transaction_logprintf(ts, D_CRIT, "%s has NULL value", el->el_key);
				el->el_value = NULL;
			}

			el->el_size = vlen;

		}

		if (line) {
			free(line);
			line = NULL;
		}

		TAILQ_INSERT_TAIL(&ts->ts_elems, el, el_list);
	}

	/*
	 * Notice that this is not an else since the state might change in
	 * loop above with work still left to do.
	 */
	if (ts->ts_state == TS_BLOB) {
		if (!be) {
			/* For internal transaction, all the data has to be there. */
			if (EVBUFFER_LENGTH(input) < ts->ts_blob_size)
				xerrx(1, "internal blob too small");
			memcpy(ts->ts_blob, EVBUFFER_DATA(input), ts->ts_blob_size);
			evbuffer_drain(input, ts->ts_blob_size);
			ts->ts_blob_index = ts->ts_blob_size;
		} else {
			while (ts->ts_blob_index < ts->ts_blob_size && (r = bufferevent_read(be, &ts->ts_blob[ts->ts_blob_index],
					ts->ts_blob_size - ts->ts_blob_index)) > 0) {
				ts->ts_blob_index += r;
			}
		}

		if (ts->ts_blob_index == ts->ts_blob_size) {
			ts->ts_state = TS_HEAD;

			if (el && strcmp(el->el_key, "body") == 0)
				transaction_logprintf(ts, D_INFO, "<< %s:%s", el->el_key, el->el_value);

			goto again;
		}
	}

	if (ts->ts_state == TS_READDONE) {
		if (ts->ts_buf)
			bufferevent_disable(ts->ts_buf, EV_READ);
		transaction_handle(ts);
	}
}

static void
read_ev_internal(int fd, short what, void *v) {
	struct transaction *ts = v;

	transaction_logprintf(ts, D_DEBUG, "read_ev_internal");
	read_ev(NULL, ts);
	if (ts->ts_state < TS_READDONE && EVBUFFER_LENGTH(ts->internal->inbuf) > 0)
		event_base_once(ts->internal->base, -1, EV_TIMEOUT, read_ev_internal, ts, NULL);
}

/*
 * This is a bufferevent callback called when we empty our output buffer.
 *
 * We can be called in two different situations. Once when we're writing
 * out the banner and the banner has been written and once when we're done
 * writing out the response.
 *
 * We only act if the state is TS_DONE, which means that the whole response
 * has been written and we're waiting for the output buffers to drain.
 */
static void
write_ev(struct bufferevent *be, void *v)
{
	struct transaction *ts = v;

	if (ts->ts_state == TS_BANNER) {
		ts->ts_state = TS_NEWLINE;
		if (be) {
			bufferevent_disable(be, EV_WRITE);
			bufferevent_enable(be, EV_READ);
		} else if (ts->internal && EVBUFFER_LENGTH(ts->internal->inbuf) > 0) {
			transaction_logprintf(ts, D_DEBUG, "calling read_ev_internal");
			event_base_once(ts->internal->base, -1, EV_TIMEOUT, read_ev_internal, ts, NULL);
		}
	}

	if (ts->ts_state == TS_DONE)
		transaction_end(ts);
}

/*
 * Bufferevent callback for handling errors.
 */
static void
err_ev(struct bufferevent *be, short what, void *v)
{
	struct transaction *ts = v;
	char whatbuf[256];

	transaction_logprintf(ts, D_ERROR, "err_ev: fd = %d, what = %s, errno: %s", ts->ts_fd, bufferevent_strwhat(what, whatbuf, sizeof(whatbuf)), what & EVBUFFER_ERROR ? xstrerror(errno) : "n/a");
	transaction_abort(ts, "err_ev");
}

/*
 * Callback when the connection took more than TRANSACTION_TIMEOUT seconds.
 */
static void
transaction_to(int fd, short ev, void *v)
{
	struct transaction *ts = v;

	transaction_abort(ts, "timeout");
}

static void
transaction_end(struct transaction *ts)
{
	struct elem *el;

	transaction_logprintf(ts, D_INFO, "transaction ended after %lds", time(NULL) - ts->start_time);

	pthread_mutex_lock(&pending_transactions_lock);
	if (ts->ts_buf)
		--connection_state.pending_external_cnt;
	else
		--connection_state.pending_internal_cnt;
	if (!ts->is_control)
		TAILQ_REMOVE(&pending_transactions, ts, ts_list);
	pthread_mutex_unlock(&pending_transactions_lock);
	pthread_cond_broadcast(&pending_transactions_signal);

	if (ts->ts_buf) {
		evtimer_del(&ts->ts_timeout);
		bufferevent_free(ts->ts_buf);
		if (keepalive && ts->ts_state == TS_DONE)
			new_connection(ts->ts_fd, NULL, ts->peer, ts->is_control);
		else {
			close(ts->ts_fd);
		}
	}

	while ((el = TAILQ_FIRST(&ts->ts_elems)) != NULL) {
		TAILQ_REMOVE(&ts->ts_elems, el, el_list);

		free(el);
	}
	if (ts->log_string)
		free(ts->log_string);
	if (ts->peer)
		free(ts->peer);
	if (ts->newline)
		free(ts->newline);

	free(ts);
}

/*
 * Forcibly abort the transaction.
 */
void
transaction_abort(struct transaction *ts, const char *errstr)
{
	transaction_logprintf(ts, D_ERROR, "transaction aborted, reason: %s", errstr);

	/*
	 * We are only allowed to abort the transaction if
	 * we're not in the handling phase, in which 
	 * we don't "own" the struct transaction and forcibly
	 * ending the transaction would lead to horrible bugs.
	 */
	if (ts->ts_state != TS_HANDLING)
		transaction_end(ts);
}

/*
 * Called when the response is completed and we're only waiting for the
 * output buffer to drain.
 */
void
transaction_done(struct transaction *ts)
{
	transaction_printf(ts, "end\n");
	ts->ts_state = TS_DONE;
}

int
transaction_write(struct transaction *ts, const void *data, size_t sz)
{
	if (ts->internal) {
		if (EVBUFFER_LENGTH(ts->internal->outbuf) == 0)
			event_base_once(ts->internal->base, -1, EV_TIMEOUT, transaction_internal_read_cb, ts, NULL);
		if (evbuffer_add(ts->internal->outbuf, data, sz) < 0)
			xerr(1, "evbuffer_add");
		return sz;
	}
	bufferevent_enable(ts->ts_buf, EV_WRITE);
	return (bufferevent_write(ts->ts_buf, (void *)data, sz));
}

int
transaction_printf(struct transaction *ts, const char *fmt, ...)
{
	char *buf;
	int ret;
	va_list ap;

	va_start(ap, fmt);
	ret = vasprintf(&buf, fmt, ap);
	va_end(ap);
	if (ret != -1) {
		transaction_write(ts, buf, ret);
		transaction_logprintf(ts, D_INFO, ">> %s", buf);
		free(buf);
	}

	return ret;
}

static void
transaction_internal_free(struct internal_cmd *cmd) {
	evbuffer_free(cmd->inbuf);
	evbuffer_free(cmd->outbuf);
	evbuffer_free(cmd->blobbuf);
	free(cmd->log_string);
	free(cmd);
}

static void
drain_into_blob(struct internal_cmd *cmd) {
	size_t len = EVBUFFER_LENGTH(cmd->outbuf);

	len = (len > cmd->blob_size) ? cmd->blob_size : len;
	if (evbuffer_add(cmd->blobbuf, EVBUFFER_DATA(cmd->outbuf), len) == -1)
		xerrx(1, "transaction_internal: evbuffer_add failed");
	evbuffer_drain(cmd->outbuf, len);
	cmd->blob_size -= len;

	if (cmd->blob_size == 0) {
		if (evbuffer_add(cmd->blobbuf, "", 1) == -1)
			xerrx(1, "transaction_internal: evbuffer_add failed");
		if (cmd->cb && cmd->cb != TRANSACTION_INTERNAL_SYNC_CB)
			cmd->cb(cmd, (char *)EVBUFFER_DATA(cmd->blobbuf), cmd->data);
		evbuffer_drain(cmd->blobbuf, SIZE_MAX);	/* flush buffer */
	}
}

static void
transaction_internal_read_cb_parse_blob(struct internal_cmd *cmd, char *line) {
	char *ep, *value, *key;
	long sz;

	if (strncmp(line, "blob:", strlen("blob:")) != 0) 
		return;

	value = line + strlen("blob:");
	if (value == NULL || *value == '\0')
		xerrx(1, "transaction_internal: no blob size");

	key = strchr(value, ':');
	if (key == NULL)
		xerrx(1, "transaction_internal: botched blob");

	*key++ = '\0';
	sz = strtol(value, &ep, 10);
	if (*ep != '\0')
		xerrx(1, "transaction_internal: blob size NaN");

	if (sz < 0 || sz > MAX_BLOB)
		xerrx(1, "transaction_internal: blob bad size");

	cmd->blob_size = sz;
	if (evbuffer_add_printf(cmd->blobbuf, "%s:%s\n", line, key) == -1)
		xerrx(1, "transaction_internal: evbuffer_add_printf failed");

	drain_into_blob(cmd);
}

/*
 * Perform an internal transaction, with key-values from a TAILQ or a string.
 * The "end" line will be added.
 */
static void
transaction_internal_read_cb(int fd, short what, void *v) {
	struct transaction *ts = v;
	struct internal_cmd *cmd = ts->internal;
	char *line;
	int end = 0;

	if (cmd->blob_size > 0)
		drain_into_blob(cmd);

	while ((line = evbuffer_readline(cmd->outbuf))) {
		if (line == '\0')
			continue;	/* empty line, e.g. after the blob */

		if (strcmp(line, "end") == 0)
			end = 1;

		if (strncmp(line, "blob:", strlen("blob:")) == 0)
			transaction_internal_read_cb_parse_blob(cmd, line);
		else if (cmd->cb && cmd->cb != TRANSACTION_INTERNAL_SYNC_CB)
			cmd->cb(cmd, line, cmd->data);

		free(line);
	}

	if (end) {
		if (ts->ts_state != TS_DONE)
			xerrx(1, "transaction_internal: end received but transaction is not done.");

		if (cmd->cb && cmd->cb != TRANSACTION_INTERNAL_SYNC_CB)
			cmd->cb(cmd, NULL, cmd->data);

		transaction_internal_free(cmd);
		ts->internal = NULL;
	}
	write_ev(NULL, ts);
}

static void
transaction_internal_output_cb(struct internal_cmd *cmd, const char *line, void *cbarg) {
	struct buf_string *buf = cbarg;

	if (line)
		bscat(buf, "%s\n", line);
}

static void
transaction_internal_main(struct internal_cmd *cmd) {
	struct elem *el;
	struct event_base *ibase = NULL, *oldbase = NULL;
	char peer[256];

	if (cmd->cb == TRANSACTION_INTERNAL_SYNC_CB) {
		ibase = cmd->base = event_base_new();
		oldbase = pthread_getspecific(csbase_key);
		pthread_setspecific(csbase_key, ibase);
		if (cmd->data)
			cmd->cb = transaction_internal_output_cb;
	}

	if (!cmd->base) {
		extern pthread_key_t csbase_key;
		cmd->base = pthread_getspecific(csbase_key);

		if (!cmd->base)
			xerr(1, "no csbase for transaction_internal");
	}

	cmd->inbuf = evbuffer_new();
	cmd->outbuf = evbuffer_new();
	cmd->blobbuf = evbuffer_new();
	if (!cmd->inbuf || !cmd->outbuf || !cmd->blobbuf)
		xerr(1, "evbuffer_new");

	if (!TAILQ_EMPTY(&cmd->elems)) {
		TAILQ_FOREACH(el, &cmd->elems, el_list) {
			if (evbuffer_add_printf(cmd->inbuf, "%s:%s\n", el->el_key, el->el_value) < 0)
				xerr(1, "evbuffer_add_printf");
		}

		while ((el = TAILQ_FIRST(&cmd->elems)) != NULL) {
			TAILQ_REMOVE(&cmd->elems, el, el_list);
			free(el);
		}
	} else if (cmd->cmd_string && cmd->cmd_string[0]) {
		if (evbuffer_add(cmd->inbuf, cmd->cmd_string, strlen(cmd->cmd_string)) < 0)
			xerr(1, "evbuffer_add");

		free(cmd->cmd_string);
		cmd->cmd_string = NULL;
	}

	/* Support for missing last \n */
	if (EVBUFFER_LENGTH(cmd->inbuf) > 0 && EVBUFFER_DATA(cmd->inbuf)[EVBUFFER_LENGTH(cmd->inbuf) - 1] != '\n') {
		if (evbuffer_add(cmd->inbuf, "\nend\n", 5) < 0)
			xerr(1, "evbuffer_add");
	} else if (evbuffer_add(cmd->inbuf, "end\n", 4) < 0)
		xerr(1, "evbuffer_add");

	snprintf(peer, sizeof(peer), "internal:%p", cmd->data ?: cmd);
	new_connection(-1, cmd, peer, 0);

	if (cmd->cmd_string) {
		free(cmd->cmd_string);
		cmd->cmd_string = NULL;
	}

	if (ibase) {
		DPRINTF(D_DEBUG, ("Waiting for transaction_internal to complete."));
		/* Kinda ugly hack, but should work well enough. */
		while (event_base_dispatch(ibase) == -1)
			;
		DPRINTF(D_DEBUG, ("Done waiting for transaction_internal to complete."));
		event_base_free(ibase);
		pthread_setspecific(csbase_key, oldbase);
	}
}

int
transaction_internal(struct internal_cmd *cmd) {
	if (cmd->cb == TRANSACTION_INTERNAL_SYNC_CB && cmd->data != NULL) {
		DPRINTF(D_WARNING, ("TRANSACTION_INTERNAL_SYNC_CB: cmd->data wasn't NULL, fixing"));
		cmd->data = NULL;
	}
	transaction_internal_main(cmd);
	return 0;
}

char *
transaction_internal_output(struct internal_cmd *cmd) {
	struct buf_string buf = {0};

	cmd->cb = TRANSACTION_INTERNAL_SYNC_CB;
	cmd->data = &buf;

	transaction_internal_main(cmd);
	return buf.buf;
}

int transaction_internal_base_printf(struct event_base *base, void (*cb)(struct internal_cmd *cmd, const char *line, void *data), void *cbdata, const char *fmt, ...) {
	struct internal_cmd *cmd = zmalloc (sizeof (*cmd));
	va_list ap;

	va_start(ap, fmt);
	xvasprintf(&cmd->cmd_string, fmt, ap);
	va_end(ap);
	cmd->base = base;
	cmd->cb = cb;
	cmd->data = cbdata;
	return transaction_internal(cmd);
}

int transaction_internal_printf(void (*cb)(struct internal_cmd *cmd, const char *line, void *data), void *cbdata, const char *fmt, ...) {
	struct internal_cmd *cmd = zmalloc (sizeof (*cmd));
	va_list ap;

	va_start(ap, fmt);
	xvasprintf(&cmd->cmd_string, fmt, ap);
	va_end(ap);
	cmd->cb = cb;
	cmd->data = cbdata;
	return transaction_internal(cmd);
}

char *
transaction_internal_output_printf(const char *fmt, ...) {
	struct internal_cmd *cmd = zmalloc (sizeof (*cmd));
	va_list ap;

	va_start(ap, fmt);
	xvasprintf(&cmd->cmd_string, fmt, ap);
	va_end(ap);

	return transaction_internal_output(cmd);
}


void
transaction_logon(struct transaction *ts) {
	ts->log = 1;

	transaction_logprintf(ts, D_INFO, "Logging enabled.");
}

void
transaction_logoff(struct transaction *ts) {
	transaction_logprintf(ts, D_INFO, "Logging temporarily disabled.");

	ts->log = 0;
}


int
transaction_logprintf(struct transaction *ts, enum debug_level level, const char *fmt, ...)
{
	char *buf;
	va_list ap;
	int ret;

	if (!ts || !ts->log)
		return 0;

	va_start(ap, fmt);
	ret = vasprintf(&buf, fmt, ap);
	va_end(ap);
	if (ret != -1) {
		if (ts->log_string)
			DPRINTF(level, ("(%s %p %s) (%s) %s", ts->peer, ts, ts->log_string, debug_levels[level], buf));
		else
			DPRINTF(level, ("(%s %p) (%s) %s", ts->peer, ts, debug_levels[level], buf));
		free(buf);
	}

	return ret;
}

int
transaction_haskey(struct transaction *ts, const char *key) {
	struct elem *el;

	TAILQ_FOREACH(el, &ts->ts_elems, el_list) {
		if (strcmp(el->el_key, key) == 0) {
			return 1;
		}
	}

	return 0;
}

const char *
transaction_getval(struct transaction *ts, const char *key) {
	struct elem *el;

	TAILQ_FOREACH(el, &ts->ts_elems, el_list) {
		if (strcmp(el->el_key, key) == 0 && el->el_value) {
			return el->el_value;
		}
	}

	return NULL;
}

static void
transaction_handle(struct transaction *ts)
{
	ts->ts_state = TS_HANDLING;

	/* Disable the timeout event here to avoid calling event_del on it from another thread. */
	evtimer_del(&ts->ts_timeout);
	memset(&ts->ts_timeout, 0, sizeof(ts->ts_timeout));

	if (transaction_haskey(ts, "cmd")) {
		if (!ts->log_string)
			xasprintf(&ts->log_string,  "%p", ts);
		handle_command(transaction_getval(ts, "cmd"), ts);
	} else {
		transaction_printf(ts, "status:%s:Err no command\n", "TRANS_ERROR_NO_COMMAND");
		transaction_done(ts);
	}
}

void
transaction_transinfo(struct transaction *ts) {
	transaction_printf(ts, "pending_transactions total (internal/external):%u (%u/%u)\n", 
		connection_state.pending_external_cnt + connection_state.pending_internal_cnt,
     		connection_state.pending_internal_cnt,
		connection_state.pending_external_cnt
	);
}

void
transaction_transinfo_log(struct transaction *ts) {
	transaction_logprintf(ts, D_INFO, "pending_transactions total (internal/external):%u (%u/%u)", 
		connection_state.pending_external_cnt + connection_state.pending_internal_cnt,
     		connection_state.pending_internal_cnt,
		connection_state.pending_external_cnt
	);
}

static void
config_reload(void *v)
{
	if (read_config(config.filename))
		xerrx(1, "config failed");

	if (config_backends(trans_conf) < 0)
		xerrx(1, "backend reconfiguration failed");
}

static int
read_config(const char *cfgfile) {
	xwarnx("Using configuration from: %s \n", cfgfile);

	if (trans_conf)
		bconf_free(&trans_conf);

	if ((trans_conf = config_init(cfgfile)) == NULL) {
		xerr(1, "configuration, config_init failed");
		return -1;
	}

	if (read_config_int(trans_conf, "debug", 0))
		debug_level = D_DEBUG;

	int reinit_logging = 0;
	char* log_level = NULL;
	char* log_tag = NULL;

	if (read_config_string(&log_level, trans_conf, "log_level")) {
		free(config.loglevel);
		config.loglevel = log_level;
		reinit_logging = 1;
	}
	if (read_config_string(&log_tag, trans_conf, "log_tag")) {
		free(config.logtag);
		config.logtag = log_tag;
		reinit_logging = 1;
	}

	if (reinit_logging)
		log_reconfigure(&config);

	set_xerr_abort(read_config_int(trans_conf, "xerr_abort", 0));

	config.error = 0;

	/* Free read_config_string read strings in free_config() */
	config.error += !read_config_string(&config.basedir, trans_conf, "basedir");
	config.error += !read_config_string(&config.port, trans_conf, "port");
	config.error += !read_config_string(&config.failover_port, trans_conf, "failover_port");
	config.error += !read_config_string(&config.control_port, trans_conf, "control_port");
	config.error += !read_config_string(&config.control_failover_port, trans_conf, "control_failover_port");
	config.error += !read_config_string(&config.bconfd, trans_conf, "bconfd");

	if (config.error)
		return -1;

	config.max_connections = read_config_int(trans_conf, "max_connections", DFLT_MAX_CONNECTIONS);
	config.connection_warning_perc = read_config_int(trans_conf, "connection_warning_perc", DFLT_CONN_WARNING_PERC);
	config.max_command_threads = read_config_int(trans_conf, "max_command_threads", DFLT_MAX_COMMAND_THREADS);
	config.debug_dump_delay = read_config_int(trans_conf, "debug_dump_delay", DFLT_DEBUG_DUMP_DELAY);

	DPRINTF(D_INFO, ("config.basedir is %s", config.basedir));

	DPRINTF(D_INFO, ("config.port is %s", config.port));
	DPRINTF(D_INFO, ("config.failover_port is %s", config.failover_port));
	DPRINTF(D_INFO, ("config.control_port is %s", config.control_failover_port));
	DPRINTF(D_INFO, ("config.control_failover_port is %s", config.control_failover_port));

	DPRINTF(D_INFO, ("config.bconfd is %s", config.bconfd));

	DPRINTF(D_INFO, ("config.max_connections is %d", config.max_connections));
	DPRINTF(D_INFO, ("config.connection_warning_perc is %d", config.connection_warning_perc));
	DPRINTF(D_INFO, ("config.max_command_threads is %d", config.max_command_threads));
	DPRINTF(D_INFO, ("config.debug_dump_delay is %d", config.debug_dump_delay));

	config.pg_master = 'p';
	strcpy(config.pg_master_buf, "gsql.master");
	config.pg_slave = 'p';
	strcpy(config.pg_slave_buf, "gsql.slave");

	return 0;
}

static int
read_config_string(char** dest, struct bconf_node *conf, const char *key) {
	const struct bconf_node *node;

	if ((node = bconf_get(conf, key)) == NULL) {
		xwarnx("Failed to read config key %s", key);
		return 0;
	}

	const char *val = bconf_value(node);
	if (val && *val) {
		if (*dest)
			free(*dest);
		*dest = xstrdup(val);
	} else {
		xwarnx("Bad value for %s", key);
		return 0;
	}

	return 1;
}

static int
read_config_int(struct bconf_node *conf, const char *key, int default_value) {
	const struct bconf_node *node;

	if ((node = bconf_get(conf, key)) == NULL) {
		return default_value;
	}

	return atoi(bconf_value(node));
}

static void
init_backends(char *check_only) {
	struct backend * const *b;

	LINKER_SET_DECLARE(backends, struct backend);
	LINKER_SET_FOREACH(b, backends) {
		if ((*b)->init) {
			DPRINTF(D_DEBUG, ("Initializing backend \"%s\"", (*b)->name));
			(*b)->init(check_only);
		}
	}
}

static void
init_done_backends(void) {
	struct backend * const *b;

	LINKER_SET_DECLARE(backends, struct backend);
	LINKER_SET_FOREACH(b, backends) {
		if ((*b)->init_done) {
			DPRINTF(D_DEBUG, ("Initializing done backend \"%s\"", (*b)->name));
			(*b)->init_done();
		}
	}
}

static void
deinit_backends(void) {
	struct backend * const *b;

	LINKER_SET_DECLARE(backends, struct backend);
	LINKER_SET_FOREACH(b, backends) {
		if ((*b)->deinit) {
			DPRINTF(D_DEBUG, ("Deinitalizing backend \"%s\"", (*b)->name));
			(*b)->deinit();
		}
	}
}

static int
config_backends(struct bconf_node *conf_node) {
	struct backend * const *b;
	LINKER_SET_DECLARE(backends, struct backend);
	LINKER_SET_FOREACH(b, backends) {
		if ((*b)->config) {
			DPRINTF(D_DEBUG, ("Configuring backend \"%s\"", (*b)->name));
			if ((*b)->config(conf_node))
				return -1;
		}
	}
	return 0;
}


void
backend_transinfo(struct transaction *ts) {
	struct backend * const *b;

	LINKER_SET_DECLARE(backends, struct backend);
	LINKER_SET_FOREACH(b, backends) {
		if ((*b)->transinfo)
			(*b)->transinfo(ts);
	}
}

static void
stdin_input(void) {
	struct pollfd pev[2];
	int fd[2];
	int val;
	int numpoll;

	/*
	 * We want the same socket to work for both stdin and stdout.
	 * for this, we need to create a daemon that will listen to that
	 * socket and redirect input/output appropriately. ugh.
	 */

	if (socketpair(AF_LOCAL, SOCK_STREAM, PF_UNSPEC, fd) == -1)
		xerr(1, "socketpair");
	if (fcntl(fd[0], F_SETFD, FD_CLOEXEC))
		xerr(1, "fcntl(F_SETFD)");
	if (fcntl(fd[1], F_SETFD, FD_CLOEXEC))
		xerr(1, "fcntl(F_SETFD)");

	listen_stdin = fd[0];

	switch (fork()) {
	case -1:
		xerr(1, "fork");
	case 0:
		close(fd[0]);

		signal(SIGHUP, SIG_IGN);

		if ((val = fcntl(STDIN_FILENO, F_GETFL, 0)) < 0)
			xerr(1, "fcntl(F_GETFL)");

		val |= O_NONBLOCK;

		if (fcntl(STDIN_FILENO, F_SETFL, val) < 0)
			xerr(1, "fcntl(F_SETFL)");

		if ((val = fcntl(fd[1], F_GETFL, 0)) < 0)
			xerr(1, "fcntl(F_GETFL)");

		val |= O_NONBLOCK;

		if (fcntl(fd[1], F_SETFL, val) < 0)
			xerr(1, "fcntl(F_SETFL)");


		numpoll = 2;
		for (;;) {
			char buffer[1024];
			ssize_t sz;
			int ret;

			pev[0].fd = fd[1];
			pev[0].events = POLLIN;
			pev[1].fd = STDIN_FILENO;
			pev[1].events = POLLIN;

			ret = poll(pev, numpoll, -1);
			if (ret <= 0)
				xerr(1, "poll");
			if (pev[0].revents & (POLLRDNORM|POLLIN)) {
				do {
					sz = read(pev[0].fd, buffer, sizeof(buffer));
					if (sz == -1 && errno != EAGAIN)
						xerr(1, "read socket");
					if (sz > 0) {
						do {
							ret = write(STDOUT_FILENO, buffer, sz);
						} while (ret == -1 && errno == EAGAIN);

						if (ret != sz)
							xerr(1, "write stdout");
					}
				} while (sz > 0);
			}
			if (pev[0].revents & (POLLHUP | POLLERR)) {
				xwarnx("HUP or ERR on socket");
				break;
			}
			if (pev[1].revents & (POLLRDNORM|POLLIN)) {
				do {
					sz = read(pev[1].fd, buffer, sizeof(buffer));
					if (sz == -1 && errno != EAGAIN)
						xerr(1, "read stdin");
					if (sz > 0) {
						do {
							ret = write(fd[1], buffer, sz);
						} while (ret == -1 && errno == EAGAIN);
						if (ret != sz)
							xerr(1, "write socket");
					}
				} while (sz > 0);
				if (sz == 0)
				{
					numpoll = 1;
					fprintf(stderr, "shutting down input.\n");
					shutdown(fd[1], SHUT_WR);
				}
			}
			if (pev[1].revents & (POLLHUP|POLLERR)) {
				numpoll = 1;
				xwarnx("HUP or ERR on stdin");
				fprintf(stderr, "shutting down input.\n");
				shutdown(fd[1], SHUT_WR);
			}
		}

		/* XXX - To be fixed, signal the server to shutdown gracefully */
		kill(getppid(), SIGTERM);
		_exit(0);
	}
	close(fd[1]);
}

struct reload_handler {
	TAILQ_ENTRY(reload_handler) list;
	void (*fun)(void *);
	void *arg;
};

TAILQ_HEAD(,reload_handler) reload_handlers = TAILQ_HEAD_INITIALIZER(reload_handlers);
TAILQ_HEAD(,reload_handler) post_reload_handlers = TAILQ_HEAD_INITIALIZER(post_reload_handlers);

static void
sighup_handler(int fd, short ev, void *v)
{
	reload_config = 1;
	DPRINTF(D_DEBUG, ("HUP received"));
	event_loopexit(NULL);
}

static void
sigusr1_handler(int fd, short ev, void *v)
{
	port_toggle = 1;
	DPRINTF(D_DEBUG, ("USR1 received"));
	event_loopexit(NULL);
}

static void
sigterm_handler(int fd, short ev, void *v)
{
	graceful_shutdown = 1;
	DPRINTF(D_DEBUG, ("TERM received"));
	event_loopexit(NULL);
}

void
call_reload_handlers(void) {
	struct reload_handler *rh;

	DPRINTF(D_DEBUG, ("reloading configuration"));

	TAILQ_FOREACH(rh, &reload_handlers, list) {
		rh->fun(rh->arg);
	}
}

void
call_post_reload_handlers(void) {
	struct reload_handler *rh;

	DPRINTF(D_DEBUG, ("reloading configuration with backend dependency"));

	TAILQ_FOREACH(rh, &post_reload_handlers, list) {
		rh->fun(rh->arg);
	}

	DPRINTF(D_DEBUG, ("config reloaded"));
}

void
reload_register(void (*handler)(void *), void *arg)
{
	struct reload_handler *rh;

	rh = xmalloc(sizeof(*rh));

	rh->fun = handler;
	rh->arg = arg;
	TAILQ_INSERT_TAIL(&reload_handlers, rh, list);
}

static void
reload_clean(void)
{      
	struct reload_handler *rh;

	while ((rh = TAILQ_FIRST(&reload_handlers)) != NULL) {
		TAILQ_REMOVE(&reload_handlers, rh, list);

		free(rh);
	}  
}      

void
post_reload_register(void (*handler)(void *), void *arg)
{
	struct reload_handler *rh;

	rh = xmalloc(sizeof(*rh));

	rh->fun = handler;
	rh->arg = arg;
	TAILQ_INSERT_TAIL(&post_reload_handlers, rh, list);
}

void
trans_pending_wait(struct transaction *pts, int suspend_workers) {
	pthread_mutex_lock(&pending_transactions_lock);

	refuse_new_connections = 1;

	while (1) {
		struct transaction *ts;

		TAILQ_FOREACH(ts, &pending_transactions, ts_list) {
			if (ts->is_control) /* Avoid deadlock. */
				continue;
			if (ts != pts)
				break;
		}
		if (!ts)
			break;

		pthread_cond_wait(&pending_transactions_signal, &pending_transactions_lock);
	}

	if (suspend_workers)
		sql_worker_suspend_all();

	refuse_new_connections = 0;

	pthread_mutex_unlock(&pending_transactions_lock);
}

static void trans_redis_reload(void *cbarg)
{
    struct trans_redis *tr = cbarg;

    fd_pool_free(tr->pool);
    vtree_free(&tr->vtree);
    bconf_vtree(&tr->vtree, bconf_get(trans_bconf, tr->bconf_path));
    tr->pool = fd_pool_create(&tr->vtree, 1, NULL);
}

void trans_redis_init(struct trans_redis *dst, const char *bconf_path)
{
    static pthread_mutex_t trans_redis_init_lock = PTHREAD_MUTEX_INITIALIZER;

	if (dst->bconf_path)
		return;
	pthread_mutex_lock(&trans_redis_init_lock);
	if (!dst->bconf_path) {
		dst->bconf_path = bconf_path;
		trans_redis_reload(dst);
		reload_register(trans_redis_reload, dst);
	}
	pthread_mutex_unlock(&trans_redis_init_lock);
}
