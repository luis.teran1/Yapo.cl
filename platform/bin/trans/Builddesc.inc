	srcs[
		control_action.gperf.enum trans.c command.c
		settings_utils.c basic_validators.c bconf_backend.c
		ev_popen.c sql_worker.c pgsql_api.c mail_queue.c
		verify_proc_checksums.c stored_proc_checksums.c garb.c
		factory.c trans_help.c trans_timers.c trans_transinfo.c
		trans_bconf.c trans_mail.c trans_control.c
		trans_at.c trans_factory_example.c
		trans_flush_workers.c
		curl_backend.c
	]
	specialsrcs[proc_checksums:$platformdir/scripts/db/plsql,$plsqlprocdir:stored_proc_checksums.c]
	copts[-DPROG=\"trans\"]
	libs[pcre platform_eventextra platform_pgsql_templates $postgres_libs]
	templates[platform_trans_templates]
	incdirs[/usr/include/postgresql /usr/pgsql-9.3/include]
