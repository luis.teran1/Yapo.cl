#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <time.h>
#include "command.h"
#include "qsort.h"

#include "trans.h"
#include "util.h"

static struct validator v_command_name[] = {
	VALIDATOR_REGEX("^([0-9a-z_]+)$", REG_EXTENDED, "ERROR_COMMAND_INVALID"),
	{0}
};
ADD_VALIDATOR(v_command_name);

static struct validator v_param_name[] = {
	VALIDATOR_REGEX("^([0-9a-z_]+)$", REG_EXTENDED, "ERROR_PARAM_INVALID"),
	{0}
};
ADD_VALIDATOR(v_param_name);

static struct c_param parameters[] = {
	{"command", 0,	"v_command_name", "Name of command to get help about"},
	{"param",   0,	"v_param_name", "Name of parameter to get help about for a given command"},
	{NULL, 0}
};

/*****************************************************************************
 * dump_commands
 * Dump all existings commands
 **********/
static void
dump_commands(struct cmd *cs) {
	struct command * const *c;
	int i, num;
	struct command **data;

	LINKER_SET_DECLARE(cmds, struct command);

	num = LINKER_SET_END(cmds) - LINKER_SET_START(cmds);
	data = xmalloc(num * sizeof (*data));
	memcpy(data, LINKER_SET_START(cmds), num * sizeof (*data));
#define cmp(a, b) (strcmp((*a)->name, (*b)->name) < 0)
	QSORT(struct command*, data, num, cmp);
#undef cmp

	c = data;
	for (i = 0 ; i < num ; i++, c++) {
		transaction_printf(cs->ts, "cmd:%s", (*c)->name);
		if ((*c)->help) {
			transaction_printf(cs->ts, ":%s\n", (*c)->help);
		} else {
			transaction_printf(cs->ts, "\n");
		}
	}

	free(data);
}

/*****************************************************************************
 * dump_command
 * Display info about given command
 **********/
static void
dump_command(struct cmd *cs, const char *cmd) {
	struct command * const *c;
	const struct c_param *param;

	LINKER_SET_DECLARE(cmds, struct command);
	LINKER_SET_FOREACH(c, cmds) {
		if (strcmp(cmd, (*c)->name) == 0) {
			transaction_printf(cs->ts, "cmd:%s\n", (*c)->name);
			if ((*c)->params) {
				for (param = (*c)->params; param->name != NULL; param++) {
					transaction_printf(cs->ts, "param:%s:%s", param->name,
							   (param->validators ? param->validators : "No validators"));
					if (param->help) {
						transaction_printf(cs->ts, ":%s\n", param->help);
					} else {
						transaction_printf(cs->ts, "\n");
					}
				}
			}
		}
	}
}

/*****************************************************************************
 * dump_param
 * Display info about given parameter
 **********/

const char *validator_type[] = {
	"",
	"Required", 		/* V_REQ	0x01 */
	"Regex", 		/* V_REGEX	0x02 */
	"Function (might add additional validators)",	/* V_FUNC	0x03 */
	"Length",		/* V_LEN	0x04 */
	"Integer",		/* V_INT	0x05 */
	"Integer - 32 bit",	/* V_INT32	0x06 */
	"Integer - 64 bit",	/* V_INT64	0x07 */
	"Double",		/* V_DOUBLE	0x08 */ 
	"Subvalidator",		/* V_VAL	0x09 */
	"Bconf key",		/* V_BCONF_KEY	0x0A */
	"Bconf list",		/* V_BCONF_LIST	0x0B */
	"Gperf enum",		/* V_GPERF_ENUM	0x0C */
	"Other",		/* V_OTHER	0x0D */
};

static void
dump_validator(struct cmd *cs, struct validator *v) {
	transaction_printf(cs->ts, "validator:%s\n", validator_type[v->type]);
}

static void
dump_param(struct cmd *cs, const char *cmd, const char *param_name) {
	struct command * const *c;
	const struct c_param *param;

	LINKER_SET_DECLARE(cmds, struct command);
	LINKER_SET_FOREACH(c, cmds) {
		if (strcmp(cmd, (*c)->name) == 0) {
			transaction_printf(cs->ts, "cmd:%s\n", (*c)->name);
			if ((*c)->params) {
				for (param = (*c)->params; param->name != NULL; param++) {
					if (strcmp(param->name, param_name) == 0) {
						transaction_printf(cs->ts, "param:%s", 
								   param->name);

						if (param->help) {
							transaction_printf(cs->ts, ":%s\n", param->help);
						} else {
							transaction_printf(cs->ts, "\n");
						}

						if (param->validators) {
							struct validator *v;

							v = load_validator(param->validators, NULL, 0);
							while (v->type) {
								dump_validator(cs, v);
								v++;
							}
						}
					}
				}
			}
		}
	}
}

/*****************************************************************************
 * example
 * Main function that initialise a state-structure and calls the state machine.
 **********/
static void
help(struct cmd *cs) {

	if (cmd_haskey(cs, "command")) {
		if (cmd_haskey(cs, "param"))
			dump_param(cs, cmd_getval(cs, "command"), cmd_getval(cs, "param"));
		else
			dump_command(cs, cmd_getval(cs, "command"));
	} else {
		dump_commands(cs);
	}

	cmd_done(cs);
}

ADD_COMMAND(help,     	    /* Name of command */
            NULL,     	    /* Called during the initalisation phase */
            help,     	    /* Main function name */
            parameters,     /* Parameters struct */
	    "Display information about commands"); 
