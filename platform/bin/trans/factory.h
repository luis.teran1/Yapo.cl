#ifndef FACTORY_H
#define FACTORY_H

#include "trans.h"
#include "command.h"

struct bpapi;

/* Factory Flags */
#define FACTORY_NO_RES_CNTR       0
#define FACTORY_RES_CNTR          1
#define FACTORY_EMPTY_RESULT_OK   2
#define FACTORY_RES_NO_LOG        4
#define FACTORY_RES_SHORT_FORM    8
#define FACTORY_RES_GLOBAL_ROW    16
#define FACTORY_MAIL_SKIP_WRAPPER 32
#define FACTORY_SEND_MAIL_ALWAYS  64

struct factory_data
{
	const char *bconf_appl;
	int factory_flags;
};

enum factory_action_type {
	FA_DONE,
	FA_SQL,
	FA_MAIL,
	FA_OUT,
	FA_FUNC,
	FA_TRANS,
	FA_AT,
	FA_COND,
};

#define FASQL_OUT               0x1
#define FASQL_HOLD_WORKER       0x2
#define FASQL_REUSE_WORKER      0x4
#define FATRANS_OUT             0x1
#define FATRANS_BACAPTURE       0x2
#define FAAT_DB                 0x0
#define FAAT_REDIS              0x1

struct factory_action
{
	enum factory_action_type type;
	union {
		const char *tmpl;
		struct {
			const char *prefix;
			const char *config;
			const char *tmpl;
			int flags;
		} sql;
		struct {
			const char *tmpl;
			const char *prefix;
			int flags;
		} trans;
		struct {
			const char *tmpl;
			const char *sub_queue;
			const char *info;
			int sec; 
			int critical;
			int flags;
		} at;
		int (*func)(struct cmd *cs, struct bpapi *ba);
		const struct factory_action * (*cond)(struct cmd *cs, struct bpapi *ba);
	} u;
};

#define FA_DONE() { FA_DONE }
#define FA_SQL(p, c, t, f) { FA_SQL, { .sql = { (p), (c), (t), (f) } } }
#define FA_MAIL(t) { FA_MAIL, { .tmpl = (t) } }
#define FA_TRANS(t, p, f) { FA_TRANS, { .trans = { (t), (p), (f) } } }
#define FA_OUT(t) { FA_OUT, { .tmpl = (t) } }
#define FA_FUNC(f) { FA_FUNC, { .func = (f) } }
#define FA_AT(t, q, i, s, c, f) { FA_AT, { .at = { (t), (q), (i), (s), (c), (f) } } }
#define FA_COND(c) { FA_COND, { .cond = (c) } }

struct factory_cmd_arg {
	const struct factory_data *data;
	const struct factory_action *actions;
};

void factory(struct cmd *cs);
int factory_translate_trans_error(struct cmd *cs, const char *e);

#define FACTORY_TRANSACTION(name, dbconfig, sql_template, mail_template, parameters, factory_flags) FACTORY_TRANSACTION_BCONF(name, dbconfig, sql_template, mail_template, parameters, factory_flags, NULL)

#define FACTORY_TRANSACTION_BCONF(name, dbconfig, sql_template, mail_template, parameters, factory_flags, bconf_appl) \
	static const struct factory_data data = { \
		bconf_appl, \
		factory_flags \
	}; \
	static const struct factory_action actions[] = { \
		FA_SQL("", dbconfig, sql_template, FASQL_OUT), \
		FA_MAIL(mail_template), \
		FA_DONE() \
	}; \
	FACTORY_TRANS(name, parameters, data, actions)

#define FACTORY_TRANS(name, params, data, actions) \
	static struct command c##name = {#name, NULL, NULL, factory, params, "Factory [" #name "] command", CMD_NORMAL_PORT, &(struct factory_cmd_arg){ &data, actions} }; \
	LINKER_SET_ADD_DATA(cmds, c##name)

#endif /*FACTORY_H*/
