#include "trans.h"
#include "command.h"
#include "trans_at.h"
#include "trans_timers.h"
#include <string.h>

static struct c_param parameters[] = {
	{"transinfo", 0, "v_bool"},
	{"cmdinfo", 0, "v_bool"},
	{"printblobs", 0, "v_bool"},
	{"backends", 0, "v_bool"},
	{"timers", 0, "v_bool"},
	{"threadinfo", 0, "v_bool"},
	{"at_info", 0, "v_bool"},
	{NULL}
};

static void
transinfo(struct cmd *cs) {
	int all = 1;
	struct cmd_param *p;
	
	TAILQ_FOREACH(p, &cs->params, tq) {
		if (strcmp(p->key, "printblobs") != 0) {
			all = 0;
			break;
		}
	}

	transaction_logoff(cs->ts);
	if (all || cmd_haskey(cs, "transinfo"))
		transaction_transinfo(cs->ts);
	if (all || cmd_haskey(cs, "cmdinfo"))
		transaction_cmdinfo(cs->ts, cmd_haskey(cs, "printblobs"));
	if (all || cmd_haskey(cs, "threadinfo"))
		transaction_threadinfo(cs->ts);
	if (all || cmd_haskey(cs, "at_info"))
		transaction_at_info(cs->ts);
	if (all || cmd_haskey(cs, "backends"))
		backend_transinfo(cs->ts);
	if (all || cmd_haskey(cs, "timers"))
		trans_timers_transinfo(cs->ts);
	cmd_done(cs);
}

ADD_COMMAND_FLAGS(transinfo, NULL, NULL, transinfo, parameters, "Dump internal information", CMD_NORMAL_PORT | CMD_CONTROL_PORT);
