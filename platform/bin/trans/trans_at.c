#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <pthread.h>
#include <errno.h>
#include <stdbool.h>

#include "trans_at.h"
#include "command.h"
#include "trans.h"
#include "util.h"
#include "bconf.h"
#include "garb.h"
#include "sql_worker.h"
#include "fd_pool.h"
#include "sredisapi.h"

extern int init_status;
extern pthread_key_t csbase_key;

static TAILQ_HEAD(,at) at_queue = TAILQ_HEAD_INITIALIZER(at_queue);

/* Incoming job queue */
pthread_mutex_t at_redis_queue_lock;
pthread_cond_t at_redis_queue_cond;
static TAILQ_HEAD(,at) at_redis_queue = TAILQ_HEAD_INITIALIZER(at_redis_queue);

/* Queue for jobs ready to be executed */
pthread_mutex_t at_redis_job_queue_lock;
pthread_cond_t at_redis_job_queue_cond;
static TAILQ_HEAD(,at) at_redis_job_queue = TAILQ_HEAD_INITIALIZER(at_redis_job_queue);

pthread_t *at_redis_executor_thread_id;
pthread_t at_main_thread_id;
pthread_t at_redis_thread_id;
pthread_t at_redis_poll_id;

bool redis_disabled;
bool at_main_thread_running;
struct event_base *at_base;
int at_pipe[2];
pthread_mutex_t at_pipe_lock = PTHREAD_MUTEX_INITIALIZER;
static TAILQ_HEAD(,at) at_pipe_queue = TAILQ_HEAD_INITIALIZER(at_pipe_queue);
static TAILQ_HEAD(,at) at_done_queue = TAILQ_HEAD_INITIALIZER(at_done_queue);
static uint32_t at_pipe_registering_count;
static int at_do_reset;
static int at_do_exit;
struct event at_pipe_ev;
static struct trans_redis tr;
static char *redis_node;

extern pthread_mutex_t reload_config_lock;

struct at_thread
{
	pthread_t thread;
	pthread_cond_t cond;
	struct at *at;
	SLIST_ENTRY(at_thread) list;
	SLIST_ENTRY(at_thread) running_threads;
};

struct at_reg_status
{
	long res;
	pthread_mutex_t lock;
	pthread_cond_t signal;
};

SLIST_HEAD(, at_thread) at_pool = SLIST_HEAD_INITIALIZER(at_pool);
SLIST_HEAD(, at_thread) at_pool_running_threads = SLIST_HEAD_INITIALIZER(at_pool_running_threads);

int32_t at_threads_count = 0;

/* Mutexes */
pthread_mutex_t at_pool_lock = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t at_pool_running_threads_lock = PTHREAD_MUTEX_INITIALIZER;

struct validator v_command[] = {
	{0}
};
ADD_VALIDATOR(v_command);


static struct validator v_rel[] = {
	VALIDATOR_REGEX("^[0-9]+(:[0-9]+){0,2}$", REG_EXTENDED, "ERROR_TIME_FORMAT"),
	{0}
};
ADD_VALIDATOR(v_rel);

static struct validator v_ind[] = {
	VALIDATOR_REGEX("^[01]$", REG_EXTENDED, "ERROR_INDEPOTENT_FORMAT"),
	{0}
};
ADD_VALIDATOR(v_ind);

struct validator v_trans_queue_info[] = {
	VALIDATOR_LEN(0, 60, "ERROR_STRING_SIZE_INVALID"),
	VALIDATOR_REGEX("^[[:print:]]+$", REG_EXTENDED, "ERROR_STRING_INVALID"),
	{0}
};
ADD_VALIDATOR(v_trans_queue_info);

struct validator v_trans_queue_sub_queue[] = {
	VALIDATOR_LEN(0, 30, "ERROR_STRING_SIZE_INVALID"),
	VALIDATOR_REGEX("^[[:print:]]+$", REG_EXTENDED, "ERROR_STRING_INVALID"),
	{0}
};
ADD_VALIDATOR(v_trans_queue_sub_queue);

static int
vf_at_timestamp_format(struct cmd_param *param, const char *arg) {
	struct tm tm;
	if (strptime(param->value, "%F %T", &tm) == NULL) {
		param->error = "ERROR_TIME_FORMAT";
		param->cs->status = "TRANS_ERROR";
	}
	return 0;
}
static struct validator v_at_timestamp[] = {
	VALIDATOR_FUNC(vf_at_timestamp_format),
	{0}
};

ADD_VALIDATOR(v_at_timestamp);


static struct c_param parameters[] = {
	{"command",     P_REQUIRED,       "v_command"},
	{"ind",         0,                "v_ind"},
	{"abs;rel",     P_NAND,           "v_at_timestamp;v_rel"},
	{"sub_queue",   0,                "v_trans_queue_sub_queue"},
	{"info",        0,                "v_trans_queue_info"},
	{"use_redis",   P_OPTIONAL,       "v_bool"},
	{"reg_sync",	P_OPTIONAL,       "v_bool"},
	{NULL, 0}
};

static struct c_param clear_params[] = {
	{"sub_queue",   P_REQUIRED,       "v_trans_queue_sub_queue"},
	{"info",        P_REQUIRED,       "v_trans_queue_info"},
	{"use_redis",   P_OPTIONAL,       "v_bool"},
	{NULL, 0}
};

static void at_callout_next(int init);
static struct event empty_ev;
static int empty_ev_scheduled = 0;

void
transaction_at_info(struct transaction *ts) {
	struct at_thread *th = NULL;
	uint32_t cnt = 0;
	int i;

	pthread_mutex_lock(&at_pool_running_threads_lock);
	transaction_printf(ts, "at.threads.count:%u\n", at_threads_count);
	SLIST_FOREACH(th, &at_pool_running_threads, running_threads) {
		transaction_printf(ts, "at.threads.%u.id:%p\n", cnt, &th);
		cnt++;
	}

	if (!redis_disabled) {
		transaction_printf(ts, "at.redis.threads.redis.poll.id:%p\n", &at_redis_poll_id);
		transaction_printf(ts, "at.redis.threads.redis.id:%p\n", &at_redis_thread_id);
		for (i = 0; i < bconf_get_int(trans_bconf, "at.redis.num_workers"); i++) {
			transaction_printf(ts, "at.redis.threads.redis.worker.%d.id:%p\n", i, &at_redis_executor_thread_id[i]);
		}
	}

	pthread_mutex_unlock(&at_pool_running_threads_lock);
}

static void
at_done(struct at *at) {
	DPRINTF(D_DEBUG, ("at_done(%p)", at));
	if (at->cmd)
		free(at->cmd);
	if (at->res)
		free(at->res);
	if (at->status)
		free(at->status);
	if (at->sub_queue)
		free(at->sub_queue);
	if (at->info)
		free(at->info);
	evtimer_del(&at->ev);

	if (TAILQ_INITIALIZED(at, at_list)) {
		TAILQ_REMOVE(&at_queue, at, at_list);
	}

	free(at);
}

static const char *
at_execute_cb(void *v, struct sql_worker *conn, const char *errstr) {
	struct at *at = v;

	if (errstr != NULL)
		xwarnx("status:%s:Err pgsql-error (%s)\n", "TRANS_DATABASE_ERROR", errstr);

	pthread_mutex_lock(&at_pipe_lock);
	if (TAILQ_EMPTY(&at_done_queue))
		UNUSED_RESULT(write(at_pipe[1], "x", 1));
	TAILQ_INSERT_TAIL(&at_done_queue, at, at_done_list);
	pthread_mutex_unlock(&at_pipe_lock);

	if (bconf_get_int(trans_bconf, "at.regress.sleep_before_garb"))
		sleep(bconf_get_int(trans_bconf, "at.regress.sleep_before_garb"));

	garb_table("trans_queue");
	return NULL;
}

static void
at_read_result(struct internal_cmd *cmd, const char *line, void *v) {
	struct at *at = v;

	if (line) {
		if (strncmp(line, "status:", sizeof("status:") - 1) == 0) {
			xasprintf(&at->status, "%.*s", PGSQL_TRANS_QUEUE_STATUS_SIZE, line + sizeof("status:") - 1);
		}
		bufcat(&at->res, &at->len, &at->pos, "%s\n", line);
		return;
	}

	if (at->res && at->status) {
		struct bpapi ba;

		BPAPI_INIT(&ba, NULL, pgsql);
		bpapi_insert(&ba, "trans_user", trans_user);
		bpapi_set_int(&ba, "id", at->id);
		bpapi_insert(&ba, "res", at->res);
		bpapi_insert(&ba, "status", at->status);

		/* Don't lock reload_config_lock here since we're running inside a transaction (transaction_end not yet called) */
		sql_worker_base_template_query(bconf_get_string(trans_bconf, "at.db_node"), cmd->base, "sql/at_set_response.sql", &ba,
				at_execute_cb, at, "AT");

		bpapi_output_free(&ba);
		bpapi_simplevars_free(&ba);
		bpapi_vtree_free(&ba);
	} else {
		DPRINTF(D_ERROR, ("No status or response for at command %lu", at->id));
	}
}

static void *
at_execute_thread(void *arg) {
	struct at_thread th;
	struct event_base *base = event_base_new();

	th.thread = pthread_self();

	pthread_cond_init(&th.cond, NULL);
	pthread_setspecific(csbase_key, base);

	th.at = arg;

	pthread_mutex_lock(&at_pool_running_threads_lock);
	at_threads_count++;
	SLIST_INSERT_HEAD(&at_pool_running_threads, &th, running_threads);
	pthread_mutex_unlock(&at_pool_running_threads_lock);

	DPRINTF(D_INFO, ("AT worker thread created, thread_id: %p", &th));

	while (!at_do_exit) {
		struct internal_cmd *cmd;
		int res;

		cmd = zmalloc(sizeof (*cmd));
		cmd->cmd_string = xstrdup(th.at->cmd);
		cmd->cb = at_read_result;
		cmd->data = th.at;
		cmd->base = base;
		cmd->thread = pthread_self();
		transaction_internal(cmd);

		while ((res = event_base_dispatch(base)) < 0)
			DPRINTF(D_DEBUG, ("at execute thread event dispatch returned error: %s", xstrerror(errno)));

		DPRINTF(D_DEBUG, ("at execute thread event dispatch exited %s", res == 0 ? "by request" : "because events depleted"));

		/* Wait for next command. */
		pthread_mutex_lock(&at_pool_lock);

		if (at_do_exit) {
			pthread_mutex_unlock(&at_pool_lock);
			break;
		}

		SLIST_INSERT_HEAD(&at_pool, &th, list);
		pthread_cond_wait(&th.cond, &at_pool_lock);
		pthread_mutex_unlock(&at_pool_lock);
	}

	event_base_free(base);

	pthread_mutex_lock(&at_pool_running_threads_lock);
	at_threads_count--;
	DPRINTF(D_DEBUG, ("AT Thread exiting (remaining: %d)", at_threads_count));
	pthread_mutex_unlock(&at_pool_running_threads_lock);

	return NULL;
}

static const char *
at_lock_cb(void *v, struct sql_worker *worker, const char *errstr) {
	struct at *at = v;
	int res;
	
	/* XXX unlock on error? */

	if (errstr != NULL) {
		xwarnx("status:%s:Err pgsql-error (%s)\n", "TRANS_DATABASE_ERROR", errstr);
		DPRINTF(D_ERROR, ("Failed to execute %s after timeout: %u", at->cmd, (unsigned)at->timeout.tv_sec));
		at_done(at);
		at_callout_next(0);
		return NULL;
	}


	if (worker->affected_rows(worker) == 0) {
		DPRINTF(D_INFO, ("Locked when executing %s after timeout: %u", at->cmd, (unsigned)at->timeout.tv_sec));
		at_done(at);
		at_callout_next(0);
		return NULL;
	}

	pthread_mutex_lock(&at_pool_lock);
	if (SLIST_EMPTY(&at_pool)) {
		pthread_mutex_unlock(&at_pool_lock);
		if ((res = pthread_create(&at->thread, NULL, at_execute_thread, at))) {
			DPRINTF(D_ERROR, ("at_lock_cb: pthread_create: %d", res));
			at_done(at);
			at_callout_next(0);
		}
	} else {
		struct at_thread *th = SLIST_FIRST(&at_pool);

		SLIST_REMOVE_HEAD(&at_pool, list);
		pthread_mutex_unlock(&at_pool_lock);

		th->at = at;
		pthread_cond_signal(&th->cond);
	}
	return NULL;
}

static void
at_execute(int _fd, short _ev, void *v) {
	struct at *at = v;
	struct bpapi ba;

	DPRINTF(D_INFO, ("Locking %s after timeout: %u", at->cmd, (unsigned)at->timeout.tv_sec));

	BPAPI_INIT(&ba, NULL, pgsql);
	bpapi_insert(&ba, "trans_user", trans_user);
	bpapi_set_int(&ba, "id", at->id);

	pthread_mutex_lock(&reload_config_lock);
	sql_worker_base_template_query(bconf_get_string(trans_bconf, "at.db_node"), at_base, "sql/at_lock_command.sql", &ba,
			at_lock_cb, at, "AT");
	pthread_mutex_unlock(&reload_config_lock);
	bpapi_output_free(&ba);
	bpapi_simplevars_free(&ba);
	bpapi_vtree_free(&ba);
	at->executing = 1;

	/* Move the at job to the head of the queue, just to make sure all executing are at the head. */
	TAILQ_REMOVE(&at_queue, at, at_list);
	TAILQ_INSERT_HEAD(&at_queue, at, at_list);

	at_callout_next(0);
}

static int
filter_commands(struct at *at) {
	int match = 0;
	const char *cmd_regex = NULL;
	pcre *regex;
	const char *err;
	int erro;

	if ((cmd_regex = bconf_get_string(trans_bconf, "at.commands.regex"))
			&& cmd_regex[0]
			&& (regex = pcre_compile(cmd_regex, REG_EXTENDED | REG_NOSUB, &err, &erro, pcre_table))) {
		match = pcre_exec(regex, NULL, at->cmd, strlen(at->cmd), 0, 0, NULL, 0);
		pcre_free(regex);
		DPRINTF(D_DEBUG, ("AT: Not adding %s, doesn't match %s",
					at->cmd, cmd_regex));
	}
	return match;
}


static void
at_put_queue(struct at *at, int from_db) {
	int i;
	int max_executing;
	struct at *atq;

	DPRINTF(D_INFO, ("Will be executing %s after timeout: %u",
				at->cmd, (unsigned)at->timeout.tv_sec));

	at->execute_at = time(NULL) + at->timeout.tv_sec;

	evtimer_set(&at->ev, at_execute, at);
	event_base_set(at_base, &at->ev);

	/* Check if at should be inserted in at_queue */
	max_executing = bconf_get_int (trans_bconf, "at.execute.max");
	if (!max_executing) {
		max_executing = DFLT_AT_MAX_EXECUTE;
	}

	if (filter_commands(at) == PCRE_ERROR_NOMATCH) {
		/* Command doesn't match, don't put in queue. */
		at_done(at);
		return;
	}

	i = 0;
	TAILQ_FOREACH(atq, &at_queue, at_list) {
		if (atq->executing) {
			if (i >= max_executing) {
				/* Already have max_executing running jobs. */
				at_done(at);
				return;
			}
		} else if (at->execute_at < atq->execute_at) {
			evtimer_add(&at->ev, &at->timeout);
			TAILQ_INSERT_BEFORE(atq, at, at_list);
			if (empty_ev_scheduled) {
				evtimer_del(&empty_ev);
				empty_ev_scheduled = 0;
			}
			return;
		}
		i++;
	}

	/*
	 * Only allow append if event was selected from db.
	 * This is to prevent an event from blocking those in the db,
	 * since we only select new once all in memory has been run.
	 */
	if (from_db && i < bconf_get_int(trans_bconf, "at.memory_limit")) {
		evtimer_add(&at->ev, &at->timeout);
		TAILQ_INSERT_TAIL(&at_queue, at, at_list);
		if (empty_ev_scheduled) {
			evtimer_del(&empty_ev);
			empty_ev_scheduled = 0;
		}
	} else {
		/* at does not fit in the list. Free it up. */
		at_done(at);
		at_callout_next(0);
	}
}

static const char *
at_register_cb(void *v, struct sql_worker *worker, const char *errstr) {
	struct at *at = v;

	if (!errstr && worker->rows(worker)) {
		worker->next_row(worker);
		at->id = strtol(worker->get_value(worker, 0), NULL, 10);

		DPRINTF(D_DEBUG, ("at_register_cb %p ok, id %ld", at, at->id));

		if (at->reg_status) {
			pthread_mutex_lock(&at->reg_status->lock);
			at->reg_status->res = at->id;
			pthread_mutex_unlock(&at->reg_status->lock);
			pthread_cond_signal(&at->reg_status->signal);
		}

		at->reg_status = NULL;
		at_put_queue(at, 0);
	} else {
		if (errstr != NULL)
			xwarnx("status:%s:Err sql-error (%s)\n", "TRANS_DATABASE_ERROR", errstr);
		else
			xwarnx("Did not get rows from at_register.sql");

		if (at->reg_status) {
			pthread_mutex_lock(&at->reg_status->lock);
			at->reg_status->res = -1;
			pthread_mutex_unlock(&at->reg_status->lock);
			pthread_cond_signal(&at->reg_status->signal);
		}

		at_done(at);
	}
	at_pipe_registering_count--;

	return NULL;
}

void
at_reset(struct cmd *cs, const char *action) {
	/* Don't reset at for store */
	if (strcmp(action, "store") == 0)
		return;

	pthread_mutex_lock(&at_pipe_lock);
	if (!at_do_reset) {
		UNUSED_RESULT(write(at_pipe[1], "x", 1));
		at_do_reset = 1;
	}
	pthread_mutex_unlock(&at_pipe_lock);
}

static void
redis_queue_command(struct at *at) {
	if (filter_commands(at) == PCRE_ERROR_NOMATCH) {
		/* Command doesn't match, don't put in queue. */
		at_done(at);
		at_pipe_registering_count--;
		return;
	}
	pthread_mutex_lock(&at_redis_queue_lock);
	TAILQ_INSERT_TAIL(&at_redis_queue, at, at_redis_list);
	pthread_mutex_unlock(&at_redis_queue_lock);
	pthread_cond_signal(&at_redis_queue_cond);
	at_pipe_registering_count--;
}

static void
at_pipe_read(int fd, short ev, void *dummy) {
	char buf[256];
	uint32_t max_register = bconf_get_int(trans_bconf, "at.register.max");

	if (!max_register) {
		max_register = DFLT_AT_MAX_REGISTER;
	}

	pthread_mutex_lock(&at_pipe_lock);
	if (at_do_exit) {
		event_del(&at_pipe_ev);
		event_del(&empty_ev);
		/*event_base_loopexit(at_base, NULL);*/
		at_do_reset = 1;
	}
	if (at_do_reset) {
		struct at *at, *nat;

		for (at = TAILQ_FIRST(&at_queue) ; at ; at = nat) {
			nat = TAILQ_NEXT(at, at_list);

			/* If executing, we might have an outstanding SQL request we can't cancel. */
			if (at->executing)
				continue;

			TAILQ_REMOVE(&at_queue, at, at_list);

			if (!TAILQ_INITIALIZED(at, at_done_list))
				TAILQ_INSERT_TAIL(&at_done_queue, at, at_done_list);
		}
		while ((at = TAILQ_FIRST(&at_pipe_queue))) {
			TAILQ_REMOVE(&at_pipe_queue, at, at_list);

			if (!TAILQ_INITIALIZED(at, at_done_list))
				TAILQ_INSERT_TAIL(&at_done_queue, at, at_done_list);
		}
		at_do_reset = 0;
	}
	while (!TAILQ_EMPTY(&at_done_queue)) {
		struct at *at = TAILQ_FIRST(&at_done_queue);

		TAILQ_REMOVE(&at_done_queue, at, at_done_list);
		TAILQ_UNINITIALIZE(at, at_done_list);
		at_done(at);
	}
	if (!TAILQ_EMPTY(&at_pipe_queue)) {
	
		/* Do not register too many at jobs at the same time since we may
		   end eating up all the available sql_workers and this thread will
		   deadlock */
		/* XXX this is a busy loop */
		if (at_pipe_registering_count >= max_register) {
			DPRINTF(D_DEBUG, ("at_pipe_read: too many jobs already being registered (%d), queuing the new one", at_pipe_registering_count));
			pthread_mutex_unlock(&at_pipe_lock);
			return;
		}
		at_pipe_registering_count++;
		DPRINTF(D_DEBUG, ("at_pipe_read: registering a new job (cnt: %u, max: %u), ", at_pipe_registering_count, max_register));

		struct at *at = TAILQ_FIRST(&at_pipe_queue);
		struct bpapi ba;

		TAILQ_REMOVE(&at_pipe_queue, at, at_list);
		TAILQ_UNINITIALIZE(at, at_list);
		pthread_mutex_unlock(&at_pipe_lock);

		if (at->use_redis) {
			redis_queue_command(at);
			return;
		}

		BPAPI_INIT_BCONF(&ba, trans_bconf, buf, pgsql);
		bpapi_insert(&ba, "trans_user", trans_user);
		bpapi_set_int(&ba, "timeout", at->timeout.tv_sec);
		bpapi_insert(&ba, "critical", at->critical ? "t" : "f");
		bpapi_insert(&ba, "command", at->cmd);
		if (at->sub_queue)
			bpapi_insert(&ba, "sub_queue", at->sub_queue);
		if (at->info)
			bpapi_insert(&ba, "info", at->info);

		/* If at->reg_status is set then a transaction is waiting for the id, don't lock reload_config_lock in that case. */
		if (!at->reg_status)
			pthread_mutex_lock(&reload_config_lock);
		sql_worker_base_template_query(bconf_get_string(trans_bconf, "at.db_node"), at_base, "sql/at_register.sql", &ba,
				at_register_cb, at, "AT");
		if (!at->reg_status)
			pthread_mutex_unlock(&reload_config_lock);
		bpapi_output_free(&ba);
		bpapi_simplevars_free(&ba);
		bpapi_vtree_free(&ba);

		/* Since we don't drain the pipe we'll be called again. */
	} else {
		while (read(fd, buf, sizeof (buf)) == sizeof (buf))
			; /* Drain fd. */

		DPRINTF(D_DEBUG, ("at_pipe drained"));

		pthread_mutex_unlock(&at_pipe_lock);
		if (!at_do_exit)
			at_callout_next(0);
	}
}

void
at_register(struct at *at, const char *sub_queue, const char *info) {
	pthread_mutex_lock(&at_pipe_lock);

	if (sub_queue)
		at->sub_queue = xstrdup(sub_queue);
	if (info)
		at->info = xstrdup(info);

	at->reg_status = NULL;

	if (TAILQ_EMPTY(&at_pipe_queue))
		UNUSED_RESULT(write(at_pipe[1], "x", 1));

	TAILQ_INSERT_TAIL(&at_pipe_queue, at, at_list);
	pthread_mutex_unlock(&at_pipe_lock);
}

long
at_register_sync(struct at *at, const char *sub_queue, const char *info) {
	struct at_reg_status reg_status = {0, PTHREAD_MUTEX_INITIALIZER, PTHREAD_COND_INITIALIZER};

	pthread_mutex_lock(&at_pipe_lock);

	if (sub_queue)
		at->sub_queue = xstrdup(sub_queue);
	if (info)
		at->info = xstrdup(info);
	at->reg_status = &reg_status;

	if (TAILQ_EMPTY(&at_pipe_queue))
		UNUSED_RESULT(write(at_pipe[1], "x", 1));

	TAILQ_INSERT_TAIL(&at_pipe_queue, at, at_list);
	pthread_mutex_unlock(&at_pipe_lock);

	pthread_mutex_lock(&reg_status.lock);
	while (!reg_status.res)
		pthread_cond_wait(&reg_status.signal, &reg_status.lock);
	pthread_mutex_unlock(&reg_status.lock);

	return reg_status.res;
}

void
at_register_storage(const char *cmd, int seconds, const char *sub_queue, const char *info, int critical, int storage) {
	struct at *at;

	at = zmalloc(sizeof (*at));
	at->cmd = xstrdup(cmd);

	at->timeout.tv_sec = seconds;
	at->timeout.tv_usec = 0;

	at->critical = critical;
	at->use_redis = storage & AT_REDIS;

	return at_register(at, sub_queue, info);
}

void
at_register_storage_printf(int seconds, const char *sub_queue, const char *info, int critical, int storage, const char *cmdfmt, ...) {
	struct at *at;
	va_list ap;

	at = zmalloc(sizeof (*at));
	va_start(ap, cmdfmt);
	xvasprintf(&at->cmd, cmdfmt, ap);
	va_end(ap);

	at->timeout.tv_sec = seconds;
	at->timeout.tv_usec = 0;

	at->critical = critical;
	at->use_redis = storage & AT_REDIS;

	return at_register(at, sub_queue, info);
}

struct at_clear_data {
	const char *logstr;
	sql_query_cb cb;
	void *cbdata;
};

int
at_clear_commands(const char *sub_queue, const char *info, const char *logstring) {
	struct bpapi ba;
	const char *errstr = NULL;

	BPAPI_INIT(&ba, NULL, pgsql);
	bpapi_insert(&ba, "sub_queue", sub_queue);
	bpapi_insert(&ba, "info", info);
	struct sql_worker *worker = sql_blocked_template_query(bconf_get_string(trans_bconf, "at.db_node"), 0, "sql/at_clear_commands.sql", &ba,
			logstring, &errstr);

	bpapi_output_free(&ba);
	bpapi_simplevars_free(&ba);
	bpapi_vtree_free(&ba);

	if (!worker) {
		DPRINTF(D_ERROR, ("(CRIT) at_clear_commands: %s", errstr));
		return -1;
	}

	if (sql_blocked_next_result(worker, &errstr) != 1) {
		sql_worker_put(worker);
		DPRINTF(D_ERROR, ("%s - Failed to clear at commands: %s", logstring, errstr));
		return -1;
	}

	sql_worker_put(worker);

	return 0;
}

static void
at_recheck_empty(int _fd, short _ev, void *v) {
	empty_ev_scheduled = 0;
	at_callout_next(0);
}

static time_t at_callout_locked;

static const char *
at_callout_cb(void *v, struct sql_worker *worker, const char *errstr) {
	struct at *at;

	if (errstr != NULL) {
		DPRINTF(D_ERROR, ("(CRIT) status:%s:Err postgresql-error (%s)\n", "TRANS_DATABASE_ERROR", errstr));
	} else {
		while (worker->next_row(worker)) {
			at = zmalloc(sizeof (*at));

			at->id = atoi(worker->get_value(worker, 0));
			at->cmd = xstrdup(worker->get_value_byname(worker, "command"));
			at->timeout.tv_sec = atoi(worker->get_value(worker, 1)) < 0 ? 0 : atoi(worker->get_value(worker, 1));
			at->timeout.tv_usec = 0;
			/* Add a random of 10 seconds to avoid all transaction handlers trying to lock at the same time. */
			if (at->timeout.tv_sec > 5)
				at->timeout.tv_sec += random() % 5;
			at_put_queue(at, 1);
		}
	}

	if (TAILQ_EMPTY(&at_queue) && !empty_ev_scheduled) {
		struct timeval timeout;

		evtimer_set(&empty_ev, at_recheck_empty, NULL);
		event_base_set(at_base, &empty_ev);

		timeout.tv_sec = bconf_get_int(trans_bconf, "at.empty_recheck_timeout");
		timeout.tv_usec = 0;
		if (timeout.tv_sec) {
			evtimer_add(&empty_ev, &timeout);
			empty_ev_scheduled = 1;
		}
	}

	at_callout_locked = 0;

	return NULL;
}

/* Find next at job to execute.
 */
static void
at_callout_next(int init) {
	struct bpapi ba;
	time_t now;
	int i = 0;
	int max_executing = bconf_get_int(trans_bconf, "at.execute.max");
	struct at *at;

	if (!max_executing)
		max_executing = DFLT_AT_MAX_EXECUTE;

	/* If we have at least max_executing runnning jobs, or any waiting jobs, do not select new ones. */
	TAILQ_FOREACH(at, &at_queue, at_list) {
		if (!at->executing || ++i >= max_executing)
			return;
	}

	now = time(NULL);
	if (at_callout_locked >= now - 600)
		return;
	at_callout_locked = now;

	BPAPI_INIT_BCONF(&ba, trans_bconf, buf, pgsql);

	/* Use master for up to date data */
	pthread_mutex_lock(&reload_config_lock);
	sql_worker_base_template_query(bconf_get_string(trans_bconf, "at.db_node"), at_base, "sql/at_select_next.sql", &ba,
			at_callout_cb, (void*)(long)init, "AT");
	pthread_mutex_unlock(&reload_config_lock);
	bpapi_output_free(&ba);
	bpapi_simplevars_free(&ba);
	bpapi_vtree_free(&ba);
}

static void *
at_thread_main(void *dummy) {
	int res;

	at_base = event_base_new();

	pthread_setspecific(csbase_key, at_base);

	if (pipe(at_pipe)) {
		/* XXX this is actually fatal. */
		xwarn("AT pipe");
		return NULL;
	}

	event_set(&at_pipe_ev, at_pipe[0], EV_READ | EV_PERSIST, at_pipe_read, NULL);
	event_base_set(at_base, &at_pipe_ev);
	event_add(&at_pipe_ev, NULL);

	at_callout_next(1);

	DPRINTF(D_INFO, ("AT main thread created"));

	while ((res = event_base_dispatch(at_base)) < 0)
		DPRINTF(D_INFO, ("AT thread event dispatch returned error: %s", xstrerror(errno)));

	DPRINTF(D_INFO, ("AT event dispatch exited %s", res == 0 ? "by request" : "because events depleted"));

	event_base_free(at_base);

	close(at_pipe[0]);
	close(at_pipe[1]);

	return NULL;
}

static void *
at_redis_register_handler(void *v) {
	DPRINTF(D_INFO, ("AT redis register handler thread created"));
	pthread_mutex_lock(&at_redis_queue_lock);
	while (!at_do_exit) {
		if (!TAILQ_EMPTY(&at_redis_queue)) {
			DPRINTF(D_INFO, ("AT redis register handler: Processing next job"));
			struct fd_pool_conn *conn;
			char *id;
			char *now_str;
			char *timeout_str;
			struct at *at;
			time_t now = time(NULL);
			char *info;
			char *sub_queue;
			int res;

			/* handle one job */
			at = TAILQ_FIRST(&at_redis_queue);
			TAILQ_REMOVE(&at_redis_queue, at, at_redis_list);
			pthread_mutex_unlock(&at_redis_queue_lock);

			pthread_mutex_lock(&reload_config_lock);
			conn = redis_sock_conn(tr.pool, "master");
			pthread_mutex_unlock(&reload_config_lock);
			at->id = redis_sock_incr(conn, "trans_at_counter");

			xasprintf(&id, "%ld", at->id);
			xasprintf(&now_str, "%d", (int)now);
			xasprintf(&timeout_str, "%d", (int)(now + at->timeout.tv_sec));

			redis_sock_hset(conn, id, "trans_user", trans_user);
			redis_sock_hset(conn, id, "critical", at->critical ? "t" : "f");
			redis_sock_hset(conn, id, "added_at", now_str);
			redis_sock_hset(conn, id, "execute_at", timeout_str);
			redis_sock_hset(conn, id, "command", at->cmd);
			if (at->sub_queue) {
				ALLOCA_PRINTF(res, sub_queue, "sub_queue_%s", at->sub_queue);
				redis_sock_hset(conn, id, "sub_queue", at->sub_queue);
				redis_sock_sadd(conn, sub_queue, id);
			}
			if (at->info) {
				ALLOCA_PRINTF(res, info, "info_%s", at->info);
				redis_sock_hset(conn, id, "info", at->info);
				redis_sock_sadd(conn, info, id);
			}
			redis_sock_zadd(conn, "at_queue", now + at->timeout.tv_sec, id);

			fd_pool_free_conn(conn);
			free(id);
			free(now_str);
			free(timeout_str);

			if (at->reg_status) {
				pthread_mutex_lock(&at->reg_status->lock);
				at->reg_status->res = at->id;
				pthread_mutex_unlock(&at->reg_status->lock);
				pthread_cond_signal(&at->reg_status->signal);
			}

			at_done(at);
			
			pthread_mutex_lock(&at_redis_queue_lock);
		} else {
			DPRINTF(D_INFO, ("AT redis register handler: No more jobs in queue. Waiting."));
			pthread_cond_wait(&at_redis_queue_cond, &at_redis_queue_lock);
		}
	}
	pthread_mutex_unlock(&at_redis_queue_lock);
	DPRINTF(D_INFO, ("AT redis register handler thread exiting"));
	return NULL;
}

static void
at_redis_fetch_jobs_cb(const void *value, size_t vlen, const void *score, size_t slen, void *cbarg) {
	struct fd_pool_conn *conn = cbarg;
	struct at *at;
	char *id;
	char *idl;
	int res;

	ALLOCA_PRINTF(res, id, "%.*s", (int)vlen, (char *)value);
	ALLOCA_PRINTF(res, idl, "%.*s_lock", (int)vlen, (char *)value);
	DPRINTF(D_DEBUG, ("AT redis fetch jobs: Got job %s", id));

	if (redis_sock_setnx(conn, idl, trans_user) == 1) {
		redis_sock_expire(conn, idl, bconf_get_int(trans_bconf, "at.redis.lock_period"));
		at = zmalloc(sizeof *at);
		at->id = atol(id);
		pthread_mutex_lock(&at_redis_job_queue_lock);
		TAILQ_INSERT_TAIL(&at_redis_job_queue, at, at_redis_job_list);
		pthread_mutex_unlock(&at_redis_job_queue_lock);
		pthread_cond_signal(&at_redis_job_queue_cond);
	}
}

static void *
at_redis_executor(void *v) {
	struct at *at;
	struct fd_pool_conn *conn;

	DPRINTF(D_INFO, ("AT redis executor started."));

	pthread_mutex_lock(&at_redis_job_queue_lock);
	while (!at_do_exit) {
		if (!TAILQ_EMPTY(&at_redis_job_queue)) {
			int res;
			char *id;
			char *idl;
			char *sub_queue;
			char *info;

			at = TAILQ_FIRST(&at_redis_job_queue);
			TAILQ_REMOVE(&at_redis_job_queue, at, at_redis_job_list);
			pthread_mutex_unlock(&at_redis_job_queue_lock);

			ALLOCA_PRINTF(res, id, "%ld", at->id);
			ALLOCA_PRINTF(res, idl, "%ld_lock", at->id);
			pthread_mutex_lock(&reload_config_lock);
			conn = redis_sock_conn(tr.pool, "master");
			pthread_mutex_unlock(&reload_config_lock);
			redis_sock_zrem(conn, "at_queue", id);
			at->cmd = redis_sock_hget(conn, id, "command");
			at->info = redis_sock_hget(conn, id, "info");
			at->sub_queue = redis_sock_hget(conn, id, "sub_queue");
			redis_sock_del(conn, idl);
			redis_sock_del(conn, id);
			if (at->sub_queue) {
				ALLOCA_PRINTF(res, sub_queue, "sub_queue_%s", at->sub_queue);
				redis_sock_srem(conn, sub_queue, id);
			}
			if (at->info) {
				ALLOCA_PRINTF(res, info, "info_%s", at->info);
				redis_sock_srem(conn, info, id);
			}
			fd_pool_free_conn(conn);

			DPRINTF(D_DEBUG, ("AT redis executor: Executing command %s.", at->cmd));
			transaction_internal_printf(TRANSACTION_INTERNAL_SYNC_CB, NULL, "%s", at->cmd);
			at_done(at);
			pthread_mutex_lock(&at_redis_job_queue_lock);
		} else {
			pthread_cond_wait(&at_redis_job_queue_cond, &at_redis_job_queue_lock);
		}
	}

	pthread_mutex_unlock(&at_redis_job_queue_lock);
	DPRINTF(D_INFO, ("AT redis executor exiting."));
	return NULL;
}

static void
at_redis_poll_cleanup(void *v) {
	struct fd_pool_conn *conn = (struct fd_pool_conn *)v;
	DPRINTF(D_INFO, ("AT redis poll exiting: cleanup."));
	if (conn)
		fd_pool_free_conn(conn);
}
	
static void *
at_redis_poll(void *v) {
	struct fd_pool_conn *conn = NULL;
	int p = bconf_get_int(trans_bconf, "at.redis.poll_period");
	pthread_cleanup_push(at_redis_poll_cleanup, conn);
	DPRINTF(D_INFO, ("AT redis poll started."));

	while(1) {
		char *now_str;
		int res;
		sleep(p);
		pthread_mutex_lock(&reload_config_lock);
		conn = redis_sock_conn(tr.pool, "master");
		pthread_mutex_unlock(&reload_config_lock);
		ALLOCA_PRINTF(res, now_str, "%d", (int)time(NULL));
		redis_sock_zrangebyscore(conn, "at_queue", "0", now_str, -1, 0, at_redis_fetch_jobs_cb, conn);
		fd_pool_free_conn(conn);
		conn = NULL;
	}

	pthread_cleanup_pop(0);
	return NULL;
}



static void
at_init(char *check_only) {
	int res;
	int i;

	if (check_only)
		return;

	if (!bconf_get(trans_bconf, "at.db_node")) {
		xwarnx("Disabling AT because no DB node");
		return;
	}

	if ((res = pthread_create(&at_main_thread_id, NULL, at_thread_main, NULL)))
	{
		xwarnx("pthread_create: %d", res);
		init_status = 1;
		return;
	}

	if (!bconf_get(trans_bconf, "at.redis_node")) {
		xwarnx("Disabling AT redis because no redis node");
		redis_disabled = 1;
	} else {
		redis_node = xstrdup(bconf_get_string(trans_bconf, "at.redis_node"));
		trans_redis_init(&tr, redis_node);
		if (!tr.pool) {
			xwarnx("AT error when creating redis fd pool for node: %s", bconf_get_string(trans_bconf, "at.redis_node"));
			init_status = 1;
			return;
		}

		if ((res = pthread_create(&at_redis_thread_id, NULL, at_redis_register_handler, NULL))) {
			xwarnx("pthread_create: %d", res);
			init_status = 1;
			return;
		}

		if ((res = pthread_create(&at_redis_poll_id, NULL, at_redis_poll, NULL))) {
			xwarnx("pthread_create: %d", res);
			init_status = 1;
			return;
		}

		at_redis_executor_thread_id = xmalloc(bconf_get_int(trans_bconf, "at.redis.num_workers") * sizeof *at_redis_executor_thread_id);
		for (i = 0; i < bconf_get_int(trans_bconf, "at.redis.num_workers"); i++) {
			if ((res = pthread_create(&at_redis_executor_thread_id[i], NULL, at_redis_executor, NULL))) {
				xwarnx("pthread_create: %d", res);
				init_status = 1;
				return;
			}
		}
	}
	at_main_thread_running = true;
}

static void
at_cleanup(void) {
	int i;
	at_do_exit = 1;
	if (at_main_thread_running) {
		pthread_mutex_lock(&at_pipe_lock);
		UNUSED_RESULT(write(at_pipe[1], "x", 1));
		pthread_mutex_unlock(&at_pipe_lock);
		pthread_join(at_main_thread_id, NULL);
		at_main_thread_running = false;
	}
	pthread_mutex_lock(&at_pool_lock);
	while (!SLIST_EMPTY(&at_pool)) {
		struct at_thread *th = SLIST_FIRST(&at_pool);

		SLIST_REMOVE_HEAD(&at_pool, list);
		pthread_mutex_unlock(&at_pool_lock);

		th->at = NULL;
		pthread_t pth = th->thread;
		pthread_cond_signal(&th->cond);
		pthread_join(pth, NULL);

		pthread_mutex_lock(&at_pool_lock);
	}
	pthread_mutex_unlock(&at_pool_lock);

	if (!redis_disabled) {
		pthread_cond_signal(&at_redis_queue_cond);
		pthread_join(at_redis_thread_id, NULL);

		pthread_cancel(at_redis_poll_id);
		pthread_join(at_redis_poll_id, NULL);

		pthread_cond_broadcast(&at_redis_job_queue_cond);
		for (i = 0; i < bconf_get_int(trans_bconf, "at.redis.num_workers"); i++)
			pthread_join(at_redis_executor_thread_id[i], NULL);

		free(redis_node);
	}
}

static void
at_cmd(struct cmd *cs) {
	time_t now = time(NULL);
	time_t when;
	struct tm tm = { .tm_isdst = -1 };
	struct at *at = zmalloc(sizeof (*at));

	const char *cabs = cmd_getval(cs, "abs");
	const char *crel = cmd_getval(cs, "rel");

	char* rel = NULL;

	if (crel)
		rel = strdupa(crel);
	else
		rel = strdupa("00:00");

	if (cabs) {
		strptime(cabs, "%F %T", &tm);
		when = mktime(&tm) - now;
		if (when < 0) /* Overdue? */
			when = 0;
	} else {
		char *sep;
		when = 0;
		sep = strchr(rel, ':');
		if (sep)
			*sep = '\0';

		when += 60 * 60 * atoi(rel);

		if (sep) {
			rel = sep + 1;
			sep = strchr(rel, ':');
			if (sep)
				*sep = '\0';

			when += 60 * atoi(rel);

			if (sep) {
				rel = sep + 1;
				when += atoi(rel);
			}
		}
	}

	at->cmd = xstrdup(cmd_getval(cs, "command"));
	at->use_redis = cmd_haskey(cs, "use_redis") ? atoi(cmd_getval(cs, "use_redis")) : 0;
	at->timeout.tv_sec = when;
	at->timeout.tv_usec = 0;

	if (at->use_redis && redis_disabled) {
		cs->status = "TRANS_ERROR";
		cs->error = "AT_REDIS_DISABLED";
		cmd_done(cs);
		return;
	}

	if ( (cmd_haskey(cs, "reg_sync") ? atoi(cmd_getval(cs, "reg_sync")) : 1) ) {
		long res = at_register_sync(at, cmd_getval(cs, "sub_queue"), cmd_getval(cs, "info"));
		if (res == -1)
			cs->status = "TRANS_ERROR";
		else
			/* XXX - What to send back here when Redis was used ? */
			transaction_printf(cs->ts, "trans_queue_id:%ld\n", res);
	} else {
		at_register(at, cmd_getval(cs, "sub_queue"), cmd_getval(cs, "info"));
	}

	cmd_done(cs);
}

ADD_COMMAND_FLAGS(at, at_init, at_cleanup, at_cmd, parameters, NULL, CMD_NORMAL_PORT);

static void
at_redis_clear_command_cb(const void *value, size_t vlen, void *cbarg) {
	struct fd_pool_conn *conn = cbarg;
	char *id;
	char *idl;
	char *info;
	char *sub_queue;
	int res;

	ALLOCA_PRINTF(res, id, "%.*s", (int)vlen, (char *)value);
	ALLOCA_PRINTF(res, idl, "%.*s_lock", (int)vlen, (char *)value);

	if (redis_sock_setnx(conn, idl, trans_user) == 1) {
		char *p_info;
		char *p_sub_queue;

		redis_sock_expire(conn, idl, bconf_get_int(trans_bconf, "at.redis.lock_period"));

		info = redis_sock_hget(conn, id, "info");
		sub_queue = redis_sock_hget(conn, id, "sub_queue");

		ALLOCA_PRINTF(res, p_info, "info_%s", info);
		ALLOCA_PRINTF(res, p_sub_queue, "sub_queue_%s", sub_queue);

		redis_sock_zrem(conn, "at_queue", id);
		redis_sock_srem(conn, p_info, id);
		redis_sock_srem(conn, p_sub_queue, id);
		redis_sock_del(conn, idl);
		redis_sock_del(conn, id);
	}
}

static void
at_redis_clear_commands(const char *sub_queue, const char *info, struct cmd *cs) {
	struct fd_pool_conn *conn;
	conn = redis_sock_conn(tr.pool, "master");
	redis_sock_sinter(conn, sub_queue, info, at_redis_clear_command_cb, conn);
	fd_pool_free_conn(conn);
}

static void
at_clear(struct cmd *cs) {
	int res;
	char *p_info;
	char *p_sub_queue;

	if (cmd_haskey(cs, "use_redis") && atoi(cmd_getval(cs, "use_redis"))) {
		ALLOCA_PRINTF(res, p_info, "info_%s", cmd_getval(cs, "info"));
		ALLOCA_PRINTF(res, p_sub_queue, "sub_queue_%s", cmd_getval(cs, "sub_queue"));
		at_redis_clear_commands(p_sub_queue, p_info, cs);
	} else {
		if (at_clear_commands(cmd_getval(cs, "sub_queue"), cmd_getval(cs, "info"), cs->ts->log_string)) {
			cs->status = "TRANS_DATABASE_ERROR";
		}
	}
	cmd_done(cs);
}

ADD_COMMAND(at_clear, NULL, at_clear, clear_params, NULL);

ADD_DB_SNAP_ACTION(at, at_reset);

