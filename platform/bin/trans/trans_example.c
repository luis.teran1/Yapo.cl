#ifdef STATE_LUT_FIRST_PASS
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <time.h>
#include <stdlib.h>
#include <unistd.h>

#include "trans.h"
#include "command.h"
#include "util.h"
#include "ctemplates.h"
#include "sql_worker.h"
#endif

#include "state_lut.h"

STATE_ENUM(example) {
	STATE(EXAMPLE_DONE),
	STATE(EXAMPLE_ERROR),
	STATE(EXAMPLE_INIT),
	STATE(EXAMPLE_STATE1),
	STATE(EXAMPLE_STATE2),
	STATE(EXAMPLE_STATE3),
};

#ifdef STATE_LUT_FIRST_PASS
#include __FILE__
#else

struct example_state {
	STATE_VAR(example) state;
	struct cmd *cs;
};

static struct c_param parameters[] = {
	{"sleep",    P_OPTIONAL,  "v_integer"},
	{NULL, 0}
};

static void example_fsm(struct example_state *state);

/*****************************************************************************
 * example_done
 * Exit function.
 **********/
static void
example_done(struct example_state *state) {
	struct cmd *cs = state->cs;
	free(state);
	cmd_done(cs);
}

/*****************************************************************************
 * bind_insert
 * Initialise template API before a call to sql_worker_template_query().
 **********/
static void
bind_insert(struct bpapi *ba, struct example_state *state) {
	struct c_param *param;

	for (param = parameters; param->name; param++) {
		if (cmd_haskey(state->cs, param->name))
			bpapi_insert(ba, param->name, cmd_getval(state->cs, param->name));
	}
}

/*****************************************************************************
 * example_fsm
 * State machine
 **********/
static void
example_fsm(struct example_state *state) {
	struct cmd *cs = state->cs;
        struct bpapi ba;
	struct sql_worker *worker = NULL;
	const char *errstr = NULL;

	while (1) {
		if (errstr) {
			transaction_logprintf(cs->ts, D_ERROR, "example_fsm statement failed (%s(%d), %s)",
					      STATE_NAME(example, state->state),
					      state->state,
					      errstr);
			cs->status = "TRANS_DATABASE_ERROR";
			cs->message = xstrdup(errstr);
			state->state = EXAMPLE_ERROR;
			if (worker) {
				sql_worker_put(worker);
				worker = NULL;
			}
		}

		transaction_logprintf(cs->ts, D_DEBUG, "example_fsm(%p, %p, %p), state=%s(%d)",
				      state, worker, errstr, STATE_NAME(example, state->state), state->state);

		switch (state->state) {
		case EXAMPLE_INIT:

			if (bconf_get_int(host_bconf, "example.hang_forever"))
				pause();

			/* Initialise the templateparse structure */
			BPAPI_INIT(&ba, NULL, pgsql);
			bind_insert(&ba, state);

			/* Register SQL query with our example_fsm() as callback function */
			/* Make sure to switch to slave if read-only query. */
			worker = sql_blocked_template_query("pgsql.master", 0,
						  "sql/example.sql", &ba,
						  cs->ts->log_string, &errstr);

			/* Clean-up */
			bpapi_output_free(&ba);
			bpapi_simplevars_free(&ba);
			bpapi_vtree_free(&ba);

			if (!worker)
				continue;

			state->state = EXAMPLE_STATE1;
			continue;

                case EXAMPLE_STATE1:
			if (sql_blocked_next_result(worker, &errstr) != 1)
				continue;
			transaction_logprintf(cs->ts, D_INFO, "Got a result: %d rows",
					      worker->rows(worker));

                        state->state = EXAMPLE_STATE2;
			continue;

                case EXAMPLE_STATE2:
			if (sql_blocked_next_result(worker, &errstr) != 1)
				continue;

                        state->state = EXAMPLE_STATE3;
                        continue;

                case EXAMPLE_STATE3:
			if (sql_blocked_next_result(worker, &errstr) != 1)
				continue;
                        while (worker->next_row(worker)) {
                                transaction_printf(cs->ts, "debug:reading %s\n",
                                                   worker->get_value(worker, 0));
                                transaction_logprintf(cs->ts, D_INFO, "Got %s in %s",
                                                      worker->get_value(worker, 0),
                                                      worker->field_name(worker, 0));
                        }
			sql_worker_put(worker);

                        state->state = EXAMPLE_DONE;
			continue;

		case EXAMPLE_DONE:
			example_done(state);
			return;

		case EXAMPLE_ERROR:
			transaction_logprintf(cs->ts, D_ERROR, "Error state");
			example_done(state);
			return;

		default:
			/* MAJOR STATE ERROR */
			cs->status = "TRANS_ERROR";
			transaction_logprintf(cs->ts, D_ERROR, "Unknown state (%d)",
					      state->state);

			example_done(state); /* If this state were run there would be a memory leak */
			return;

		}
	}
}

/*****************************************************************************
 * example
 * Main function that initialise a state-structure and calls the state machine.
 **********/
static void
example(struct cmd *cs) {
	transaction_logprintf(cs->ts, D_DEBUG, "Start example transaction");
	struct example_state *state;
	state = zmalloc(sizeof(*state));
	state->cs = cs;
	state->state = EXAMPLE_INIT;

	example_fsm(state);
}

ADD_COMMAND(example,     /* Name of command */
		NULL,
            example,     /* Main function name */
            parameters,  /* Parameters struct */
            "example command");/* Command description */

#endif /*STATE_LUT_FIRST_PASS*/
