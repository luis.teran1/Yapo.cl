#ifdef STATE_LUT_FIRST_PASS
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <time.h>

#include "trans.h"
#include "command.h"
#include "util.h"
#include "factory.h"
#include "trans_at.h"
#include "sql_worker.h"
#include <ctemplates.h>
#include <bconf.h>
#include <event.h>
#include "trans_mail.h"
#include "pgsql_api.h"
#endif

#include "state_lut.h"

/* Factory States */
STATE_ENUM(factory_state) {
	STATE(FACTORY_DONE),
	STATE(FACTORY_ERROR),
	STATE(FACTORY_INIT),
	STATE(FACTORY_ACTION),
	STATE(FACTORY_SQL),
	STATE(FACTORY_SQL_RESULT),
	STATE(FACTORY_MAIL),
	STATE(FACTORY_MAIL_RESULT),
	STATE(FACTORY_OUT),
	STATE(FACTORY_TRANS),
	STATE(FACTORY_TRANS_RESULT),
	STATE(FACTORY_FUNC),
	STATE(FACTORY_AT),
	STATE(FACTORY_COND),
};

#ifdef STATE_LUT_FIRST_PASS
#include __FILE__
#else

struct factory_state {
	STATE_VAR(factory_state) state;
	struct command_bpapi cba;
	struct sql_worker *worker;
	const struct factory_action *action;
	const struct factory_action *actions;
	int num_rows;
	int num_res;
	int sql;
	int action_pos;
	struct timer_instance *ti;
	struct cmd *cs;
	int trans_internal_error;
};

static void factory_fsm(struct factory_state *state);

static void
factory_done(void *v) {
	struct factory_state *state = v;
	struct cmd *cs = state->cs;

	command_bpapi_free(&state->cba);

	if (state->worker)
		sql_worker_put(state->worker);

	free(state);
	cmd_done(cs);
}

static void
factory_bind_insert(struct bpapi *ba, struct factory_state *state) {
	struct cmd *cs = state->cs;
	struct cmd_param *cp;

	TAILQ_FOREACH(cp, &cs->params, tq) {
		bpapi_insert(ba, cp->key, cp->value);
	}
}

#define FACTORY_TRANSLATE_PREFIX "ERROR:  FACTORY_TRANSLATE:"
static int FACTORY_TRANSLATE_PREFIX_LEN = sizeof(FACTORY_TRANSLATE_PREFIX)-1;

/* A helper function to log information about the action to be executed */
static void
factory_debug_log_action(struct factory_state *state) {
	struct cmd *cs = state->cs;
	const struct factory_action *action = state->action;
	const char *name = cs->command->name;	

	switch(action->type) {
		case FA_DONE:
			transaction_logprintf(cs->ts, LOG_DEBUG, "factory_fsm [%s], action[%d] FA_DONE()", 
						name, state->action_pos);
			break;
		case FA_SQL:
			transaction_logprintf(cs->ts, LOG_DEBUG, "factory_fsm [%s], action[%d] FA_SQL(%s, %s, %s, %d)", 
						name, state->action_pos, action->u.sql.prefix, action->u.sql.config, action->u.sql.tmpl, action->u.sql.flags);
			break;
		case FA_MAIL:
			transaction_logprintf(cs->ts, LOG_DEBUG, "factory_fsm [%s], action[%d] FA_MAIL(%s)", 
						name, state->action_pos, action->u.tmpl);
			break;
		case FA_OUT:
			transaction_logprintf(cs->ts, LOG_DEBUG, "factory_fsm [%s], action[%d] FA_OUT(%s)", 
						name, state->action_pos, action->u.tmpl);
			break;
		case FA_FUNC:
			transaction_logprintf(cs->ts, LOG_DEBUG, "factory_fsm [%s], action[%d] FA_FUNC(%p)", 
						name, state->action_pos, action->u.func);
			break;
		case FA_TRANS:
			transaction_logprintf(cs->ts, LOG_DEBUG, "factory_fsm [%s], action[%d] FA_TRANS(%s)", 
						name, state->action_pos, action->u.tmpl);
			break;
		case FA_AT:
			transaction_logprintf(cs->ts, LOG_DEBUG, "factory_fsm [%s], action[%d] FA_AT(%s, %s, %s, %d, %d, %d)", 
						name, state->action_pos, action->u.at.tmpl, action->u.at.sub_queue, action->u.at.info, action->u.at.sec, action->u.at.critical, action->u.at.flags);
			break;
		case FA_COND:
			transaction_logprintf(cs->ts, LOG_DEBUG, "factory_fsm [%s], action[%d] FA_COND(%p)", 
						name, state->action_pos, action->u.cond);
			break;
		default:
			transaction_logprintf(cs->ts, LOG_DEBUG, "Undefined action");
			break;
	}
}

/*
 * If a psql raise an exception of the form "FACTORY_TRANSLATE:field:value", translate this
 * into a TRANS_ERROR on said field with said value.
 */
int
factory_translate_trans_error(struct cmd *cs, const char *e) {
	if (e == NULL || (e && strncmp(e, FACTORY_TRANSLATE_PREFIX, FACTORY_TRANSLATE_PREFIX_LEN) != 0)) 
		return 0;

	if (strchr(e + FACTORY_TRANSLATE_PREFIX_LEN, ':') == NULL)
		return 0;

	char *intmsg = xstrdup(e + FACTORY_TRANSLATE_PREFIX_LEN);
	char *field_end = strchr(intmsg, ':');
	char *msg_end = strchr(field_end + 1, '\n');

	*field_end = '\0';
	if (msg_end)
		*msg_end = '\0';

	transaction_printf(cs->ts, "%s:%s\n", intmsg, field_end + 1);
	free(intmsg);
	return 1;
}

static void
factory_mail_cb(struct mail_data *md, void *v, int status) {
	struct factory_state *state = v;
	struct cmd *cs = state->cs;

        if (status != 0) {
                cs->status = "TRANS_MAIL_ERROR";
		state->state = FACTORY_ERROR;
        }

	factory_fsm(v);
}

static void
factory_trans_cb(struct internal_cmd *intcmd, const char *line, void *data) {
       struct factory_state *state = data;

	if (!line)
		return factory_fsm(state);
	
	if (strchr(line, ':') == NULL)
		return;

	int np = !state->action->u.trans.prefix || !*state->action->u.trans.prefix;
	int is = strncmp(line, "status:", 7) == 0;

	/* Set error if status line isn't TRANS_OK */
	if (is && strstr(line+7, "TRANS_OK") == NULL)
		state->trans_internal_error = 1;

	/* Pass through the internal transaction output, but eat 'status' if there's no prefix */
	if (state->action->u.trans.flags & FATRANS_OUT) {
		if (!(np && is))
			transaction_printf(state->cs->ts, "%s%s\n", state->action->u.trans.prefix, line);
	}

	/* Capture key:value lines into the bpapi */
   	if (state->action->u.trans.flags & FATRANS_BACAPTURE) {
		if (!(np && is)) {
			char keybuf[256];
			char *keyval = xstrdup(line);
			char *val = strchr(keyval, ':');
			char *fp = keyval;
			*val++ = '\0';
			
			/* this only handles text blobs, binary blobs are not supported by bpapi */
			if (strcmp(keyval, "blob") == 0) {
				/* Here val holds size:key\nblob value
				   skip over the size, reset keyval to the key and
				   val to the start of the blob value */
				keyval = strchr(val, ':');
				if (keyval == NULL)
					return;
				*keyval++='\0';

				val = strchr(keyval, '\n');
				if (val == NULL)
					return;
				*val++ = '\0';
			}
			snprintf(keybuf, sizeof(keybuf), "%s%s", state->action->u.trans.prefix, keyval);
			bpapi_insert(&state->cba.ba, keybuf, val);
			free(fp);
		}
	}
}


/* For reasons of sanity and simplicity one should not be able to hold more than one workers */
static int
fasql_sanity_check_flags(struct cmd *cs, struct sql_worker *worker, int flags) {
	const char error_msg[] = "ERROR_FA_SQL_ACTION:error_flags";

	if ((flags & FASQL_HOLD_WORKER) && worker != NULL) {
		transaction_logprintf(cs->ts, D_ERROR, "FA_SQL, FASQL_HOLD_WORKER bit set while a worker is held");
		transaction_printf(cs->ts, "error:%s\n", error_msg);
		return 1;
	}

	if ((flags & FASQL_REUSE_WORKER) && worker == NULL) {
		transaction_logprintf(cs->ts, D_ERROR, "FA_SQL, FASQL_REUSE_WORKER bit set while no worker is held");
		transaction_printf(cs->ts, "error:%s\n", error_msg);
		return 1;
	}

	if ((flags & FASQL_REUSE_WORKER) && (flags & FASQL_HOLD_WORKER)) {
		transaction_logprintf(cs->ts, D_ERROR, "FA_SQL, FASQL_REUSE_WORKER and FASQL_HOLD_WORKER bits set");
		transaction_printf(cs->ts, "error:%s\n", error_msg);
		return 1;
	}

	return 0;
}

#define FACTORY_TIMER_STATE(st) if (state->ti != NULL) timer_end(state->ti, NULL); state->ti = timer_start(state->cs->ti, #st)

static void
factory_fsm(struct factory_state *state) {
	struct cmd *cs = state->cs;
	struct sql_worker *worker = NULL;
	const char *errstr = NULL;
	const struct factory_cmd_arg *arg = state->cs->command->aux;
	const struct factory_data *data = arg->data;
	const struct factory_action *action;

	while (1) {
		if (state->ti) {
			timer_end(state->ti, NULL);
			state->ti = NULL;
		}

		transaction_logprintf(cs->ts, D_DEBUG, "factory_fsm [%s] (%p, %p, %p), state=%s",
				      cs->command->name, state, worker, errstr, STATE_NAME(factory_state, state->state));

		switch (state->state) {
		case FACTORY_INIT:
			FACTORY_TIMER_STATE(INIT);
			command_bpapi_init(&state->cba, cs, CMD_BPAPI_HOST, data->bconf_appl);
			factory_bind_insert(&state->cba.ba, state);
			state->state = FACTORY_ACTION;
			state->actions = arg->actions;
			continue;

		case FACTORY_ACTION:
			FACTORY_TIMER_STATE(ACTION);

			if (state->action)
				state->actions++;
			state->action = state->actions;
			state->action_pos++;
			if (debug_level == D_DEBUG)
				factory_debug_log_action(state);
			/* intentional fall-through */

		case FACTORY_COND:
			switch (state->action->type) {
			case FA_DONE:
				state->state = FACTORY_DONE;
				break;
			case FA_SQL:
				state->state = FACTORY_SQL;
				break;
			case FA_MAIL:
				state->state = FACTORY_MAIL;
				break;
			case FA_OUT:
				state->state = FACTORY_OUT;
				break;
			case FA_FUNC:
				state->state = FACTORY_FUNC;
				break;
			case FA_TRANS:
				state->state = FACTORY_TRANS;
				break;
			case FA_AT:
				state->state = FACTORY_AT;
				break;
			case FA_COND:
				action = state->action->u.cond(cs, &state->cba.ba);
				if (action == NULL)
					continue;
				state->action = action;
				state->state = FACTORY_COND;
				break;
			}
			continue;

		case FACTORY_SQL:
			FACTORY_TIMER_STATE(SQL);

			if (!state->action->u.sql.tmpl) {
				/* Backward compat with FACTORY_TRANSACTION */
				state->state = FACTORY_ACTION;
				continue;
			}

			if (fasql_sanity_check_flags(cs, state->worker, state->action->u.sql.flags)) {
				state->state = FACTORY_ERROR;
				cs->status = "TRANS_ERROR";
				continue;
			}
			
			state->sql = 1;
			state->state = FACTORY_SQL_RESULT;

			if (state->action->u.sql.flags & FASQL_REUSE_WORKER) {
				worker = state->worker;
				state->worker = NULL;

				if (sql_blocked_template_newquery(worker, state->action->u.sql.tmpl, &state->cba.ba, &errstr) == -1) {
					state->state = FACTORY_ERROR;
					continue;
				}		
			} else {
				worker = sql_blocked_template_query(state->action->u.sql.config, 0,
						  state->action->u.sql.tmpl, &state->cba.ba, cs->ts->log_string, &errstr);
			}

			if (!worker) {
				state->state = FACTORY_ERROR;
				continue;
			}

			continue;

		case FACTORY_SQL_RESULT: {
			FACTORY_TIMER_STATE(SQL_RESULT);
                        int row = 0, col, cols;

			if (data->factory_flags & FACTORY_RES_NO_LOG)
				transaction_logoff(cs->ts);
			while (sql_blocked_next_result(worker, &errstr) == 1) {
				state->num_rows += worker->rows(worker);

				if (!(data->factory_flags & FACTORY_RES_GLOBAL_ROW))
					row = 0;

				if (!(state->action->u.sql.flags & FASQL_OUT)) {
					sql_worker_bpa_insert(worker, &state->cba.ba, state->action->u.sql.prefix);
					continue;
				}

				cols = worker->fields(worker);
				while (worker->next_row(worker)) {
					for (col = 0; col < cols; col++) {
						char keybuf[256];

						snprintf(keybuf, sizeof(keybuf), "%s%s", state->action->u.sql.prefix, worker->field_name(worker, col));

						if (worker->is_array(worker, col)) {
							char *arr = xstrdup(worker->get_value(worker, col));
							int num;
							char **elem;
							num = pgsql_split_array (arr, strlen(arr), &elem);
							if (num < 0) {
								transaction_logprintf(cs->ts, D_ERROR, "factory failed parsing array: %s", worker->get_value(worker, col));
							} else {
								int i;

								for (i = 0; i < num; i++) {
									char separator = ':';
									if (elem[i]&&(strchr(elem[i],'\n')!=NULL||strchr(elem[i],'\r'))) {
										transaction_printf(cs->ts, "blob:%ld:", (long) strlen(elem[i]));
										separator = '\n';
									}
									if (data->factory_flags & FACTORY_RES_CNTR)
										transaction_printf(cs->ts, "%s.%d.%d.%s%c%s\n",
												cs->command->name,
												state->num_res,
												row,
												worker->field_name(worker, col),
												separator,
												elem[i]);
									else
										transaction_printf(cs->ts, "%s.%d.%s%c%s\n",
												cs->command->name,
												row,
												worker->field_name(worker, col),
												separator,
												elem[i]);
									bpapi_insert(&state->cba.ba, keybuf, elem[i]);
								}
								if (elem)
									free (elem);
							}
							free (arr);
						} else {
							char *value = (char*) worker->get_value(worker,col);
							char separator = ':';
							if (value&&(strchr(value,'\n')!=NULL||strchr(value,'\r'))) {
								transaction_printf(cs->ts, "blob:%ld:", (long) strlen(value));
								separator = '\n';
							}
							if (data->factory_flags & FACTORY_RES_CNTR)
								transaction_printf(cs->ts, "%s.%d.%d.%s%c%s\n",
										cs->command->name,
										state->num_res,
										row,
										worker->field_name(worker, col),
										separator,
										value);
							else if (data->factory_flags & FACTORY_RES_SHORT_FORM) 
								transaction_printf(cs->ts, "%s%c%s\n",
									   worker->field_name(worker, col),
									   separator,
									   value);
							else
								transaction_printf(cs->ts, "%s.%d.%s%c%s\n",
										cs->command->name,
										row,
										worker->field_name(worker, col),
										separator,
										value);
							bpapi_insert(&state->cba.ba, keybuf, value);
						}
					}
					row++;
				}
				state->num_res++;
			}

			if (data->factory_flags & FACTORY_RES_NO_LOG)
				transaction_logon(cs->ts);

			if (errstr) {
				state->state = FACTORY_ERROR;
				continue;
			}

			if (state->action->u.sql.flags & FASQL_HOLD_WORKER)
				state->worker = worker;
			else
				sql_worker_put(worker);

			worker = NULL;
			state->state = FACTORY_ACTION;

			continue;
		}

		case FACTORY_MAIL: {
			FACTORY_TIMER_STATE(MAIL);
			struct mail_data *md;

			if (!state->action->u.tmpl) {
				/* Backward compat with FACTORY_TRANSACTION */
				state->state = FACTORY_ACTION;
				continue;
			}

			state->state = FACTORY_MAIL_RESULT;
		        if (state->sql && !state->num_rows && ((data->factory_flags & FACTORY_SEND_MAIL_ALWAYS) == 0)) 
				continue;

			md = zmalloc(sizeof (*md));
			md->command = state;
			md->callback = factory_mail_cb;
			md->log_string = cs->ts->log_string;
			md->bconf_app = data->bconf_appl;
			md->vars = &state->cba.ba.schain;
			if (data->factory_flags & FACTORY_MAIL_SKIP_WRAPPER) {
				mail_insert_param(md, "wrapper_template", "none");
			}
			mail_send(md, state->action->u.tmpl, cmd_getval(state->cs, "lang")); /* XXX language */
			return;
		}

		case FACTORY_MAIL_RESULT:
			FACTORY_TIMER_STATE(MAIL_RESULT);
			state->state = FACTORY_ACTION;
			continue;

		case FACTORY_OUT: {
			FACTORY_TIMER_STATE(OUT);
			struct buf_string *buf = BPAPI_OUTPUT_DATA(&state->cba.ba);

			call_template(&state->cba.ba, state->action->u.tmpl);

			if (buf->buf) {
				char *bufp = buf->buf;
				char *line;

				while ((line = strsep(&bufp, "\n"))) {
					char *col;

					while (isspace(*line))
						line++;
					col = strchr(line, ':');

					/* XXX probably need more cleaning here... */
					if (col) {
						*col++ = '\0';
						while (isspace(*col))
							col++;
					}

					if (*line) {
						if (col && (strcmp(line, "blob") == 0)) {
							int len = atoi(col);

							if (bufp + len <= buf->buf + buf->pos) {
								transaction_printf(cs->ts, "blob:%s\n%.*s\n",
										col, len, bufp);
								bufp += len;
							}
						} else if (col && *col) {
							transaction_printf(cs->ts, "%s:%s\n", line, col);
						} else {
							transaction_printf(cs->ts, "%s\n", line);
						}
					}
				}
				free(buf->buf);
				buf->buf = NULL;
			}

			state->state = FACTORY_ACTION;
			continue;
		}

		case FACTORY_TRANS:
			FACTORY_TIMER_STATE(TRANS);
			state->state = FACTORY_TRANS_RESULT;

			struct buf_string *buf = BPAPI_OUTPUT_DATA(&state->cba.ba);

			call_template(&state->cba.ba, state->action->u.tmpl);

			if (!buf->buf) {
				transaction_printf(cs->ts, "error:ERROR_RENDERING_TEMPLATE:%s\n", state->action->u.tmpl);
				cs->status = "TRANS_ERROR";
				state->state = FACTORY_ERROR;
				continue;
			}

			transaction_internal_printf(factory_trans_cb, state, "%s", buf->buf);
			free(buf->buf);
			buf->buf = NULL;
			return;

		case FACTORY_TRANS_RESULT:
			state->state = FACTORY_ACTION;
			if (state->trans_internal_error) {
				cs->status = "TRANS_INTERNAL_ERROR"; 
				state->state = FACTORY_ERROR;
			}

			continue;

		case FACTORY_AT:
			FACTORY_TIMER_STATE(AT);
			state->state = FACTORY_ACTION;

			struct buf_string *captured = BPAPI_OUTPUT_DATA(&state->cba.ba);

			call_template(&state->cba.ba, state->action->u.at.tmpl);

			if (!captured->buf) {
				transaction_printf(cs->ts, "error:ERROR_RENDERING_TEMPLATE:%s\n", state->action->u.at.tmpl);
				cs->status = "TRANS_ERROR";
				state->state = FACTORY_ERROR;
				continue;
			}
			
			at_register_storage(captured->buf, 
					state->action->u.at.sec,
					state->action->u.at.sub_queue,
					state->action->u.at.info,
					state->action->u.at.critical,
					state->action->u.at.flags);

			free(captured->buf);
			captured->buf = NULL;

			continue;

		case FACTORY_FUNC:
			FACTORY_TIMER_STATE(FUNC);
			state->state = FACTORY_ACTION;

			if (state->action->u.func(cs, &state->cba.ba))
				state->state = FACTORY_ERROR;

			continue;

		case FACTORY_DONE:
			FACTORY_TIMER_STATE(DONE);
		        if (state->sql && !state->num_rows && ((data->factory_flags & FACTORY_EMPTY_RESULT_OK) != FACTORY_EMPTY_RESULT_OK)) {
				const char *str = cs->command->name;
				char upper_buf[strlen(str) + 1];
				cs->status = "TRANS_ERROR";
				int i = 0;
				while (*str)
					upper_buf[i++] = toupper(*str++);
				upper_buf[i] = '\0';
				transaction_printf(cs->ts, "error:ERROR_%s_RESULT_EMPTY\n", upper_buf);
				state->state = FACTORY_ERROR;
				continue;
			}

			if (state->ti) {
				timer_end(state->ti, NULL);
				state->ti = NULL;
			}

			factory_done(state);
			return;

		case FACTORY_ERROR:
			FACTORY_TIMER_STATE(ERROR);
			if (errstr) {
				transaction_logprintf(cs->ts, D_ERROR, "factory_fsm [%s] statement failed (%s)",
						cs->command->name,
						errstr);

				if (factory_translate_trans_error(cs, errstr)) {
					cs->status = "TRANS_ERROR";
					cs->message = NULL;
				} else {
					cs->status = "TRANS_DATABASE_ERROR";
					cs->message = xstrdup(errstr);
				}
			}
			if (worker)
				sql_worker_put(worker);
			transaction_logprintf(cs->ts, D_ERROR, "Error state");
			factory_done(state);
			return;

		default:
			cs->status = "TRANS_ERROR";
			transaction_logprintf(cs->ts, D_ERROR, "Unknown state (%d)",
					      state->state);
			factory_done(state);
			return;

		}
	}
}

void
factory(struct cmd *cs) {
	transaction_logprintf(cs->ts, D_DEBUG, "Start Factory [%s] transaction", cs->command->name);
	struct factory_state *state;

	state = zmalloc(sizeof(*state));
	state->cs = cs;
	state->state = FACTORY_INIT;

	factory_fsm(state);
}

#endif
