
#include <stdlib.h>
#include <netdb.h>

#include "command.h"
#include "util.h"

/*
 * bool
 */
struct validator v_bool[] = {
	VALIDATOR_REGEX("^[01]$", REG_EXTENDED, "ERROR_NOT_BOOL"),
	{0}
};
ADD_VALIDATOR(v_bool);

/*
 * true
 */
struct validator v_true[] = {
	VALIDATOR_REGEX("^1$", REG_EXTENDED, "ERROR_NOT_TRUE"),
	{0}
};
ADD_VALIDATOR(v_true);

/*
 * Integer
 */
struct validator v_integer[] = {
	VALIDATOR_INT(0, -1, "ERROR_INTEGER_INVALID"),
	{0}
};
ADD_VALIDATOR(v_integer);

/*
 * float
 */
struct validator v_float[] = {
	VALIDATOR_REGEX("^[[:digit:]]+(\\.[[:digit:]]+)?$", REG_EXTENDED|REG_NOSUB, "ERROR_FLOAT_INVALID"),
	{0}
};
ADD_VALIDATOR(v_float);

/*
 * Strings
 */
struct validator v_string_all[] = {
	VALIDATOR_REGEX("^[\\pL\\pM\\pN\\pP\\pS\\pZ]+$", REG_EXTENDED, "ERROR_STRING_INVALID"),
	{0}
};
ADD_VALIDATOR(v_string_all);

struct validator v_string_all_blob[] = {
	VALIDATOR_REGEX("^[\\pL\\pM\\pN\\pP\\pS\\pZ\n\t]+$", REG_EXTENDED, "ERROR_STRING_INVALID"),
	{0}
};
ADD_VALIDATOR(v_string_all_blob);

/*
 * Arrays
 */
static int
vf_array_check(struct cmd_param *param, const char *arg) {
	if (strstrptrs(arg, param->value, NULL, ",") == NULL) {
		param->error = "ERROR_NOT_IN_ARRAY";
		param->cs->status = "TRANS_ERROR";
	}
	return 0;
}

struct validator v_array[] = {
        VALIDATOR_FUNC(vf_array_check),
	{0}
};
ADD_VALIDATOR(v_array);

/* IP ADRESS */
static int
vf_ip(struct cmd_param *param, const char *arg) {
	struct addrinfo hints = { AI_NUMERICHOST };
	struct addrinfo *res;
	int err;

	if ((err = getaddrinfo(param->value, NULL, &hints, &res))) {
		param->error = "ERROR_IP_INVALID";
		xasprintf(&param->message, "%s:%s", param->value, gai_strerror(err));
		param->cs->status = "TRANS_ERROR";
	} else {
		freeaddrinfo(res);
	}
	return 0;
}

static struct validator v_ip[] = {
	VALIDATOR_NREGEX("^::ffff:", REG_ICASE, "ERROR_IP_V4MAPPED"),
	VALIDATOR_NREGEX("[[:space:]]", 0, "ERROR_IP_INVALID"),
	VALIDATOR_FUNC(vf_ip),
	{0}
};
ADD_VALIDATOR(v_ip);

