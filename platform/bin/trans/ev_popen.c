#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <sys/wait.h>
#include <string.h>
#include <sys/errno.h>
#include <pthread.h>
#include <semaphore.h>

#include "queue.h"
#include "event.h"
#include "util.h"
#include "ev_popen.h"
#include "trans.h"

struct pending_child {
	TAILQ_ENTRY(pending_child) list;
	pid_t pid;
	ev_popen_cb rcb;
	ev_popen_cb wcb;
	ev_popen_cb ecb;
	ev_popen_ccb ccb;
	int iofd;
	int errfd;
	void *cb_arg;
	struct bufferevent *buf;
	struct bufferevent *errbuf;
	int child_dead_pipe[2];
	struct event child_dead_ev;
	int wait_status;
	time_t start_time;

	char *cmd;
	struct buf_string argv;
};

static struct event signal_ev;

TAILQ_HEAD(, pending_child) pending_children = TAILQ_HEAD_INITIALIZER(pending_children);
pthread_rwlock_t pending_children_lock = PTHREAD_RWLOCK_INITIALIZER;

/* Variables supporting process limiting */
static sem_t sem_popen_concurrent;
static int conf_limit;
static int popen_limit_max;
/* accessed only under pending_children_lock */
static int popen_limit_current;
static size_t popen_stat_limit_hit;
static size_t popen_stat_calls;

static struct pending_child *
find_pending_child(pid_t pid) {
	/* You need to hold the lock when calling this function. */
	struct pending_child *pc;
	TAILQ_FOREACH (pc, &pending_children, list) {
		if (pc->pid == pid)
			return pc;
	}
	return NULL;
}

pid_t
ev_popen_get_pid(struct bufferevent *be) {
	struct pending_child *pc;

	pthread_rwlock_rdlock(&pending_children_lock);
	TAILQ_FOREACH (pc, &pending_children, list) {
		if (pc->buf == be) {
			pthread_rwlock_unlock(&pending_children_lock);
			return pc->pid;
		}
	}
	pthread_rwlock_unlock(&pending_children_lock);

	return 0;
}

static void
child_dead(int pfd, short _ev, void *arg) {
	struct pending_child *pc = arg;

	if(pc->ccb)
		pc->ccb(pc->cb_arg, pc->wait_status);
	bufferevent_free(pc->buf);
	if (pc->iofd >= 0)
		close(pc->iofd);
	if (pc->errbuf)
		bufferevent_free(pc->errbuf);
	if (pc->errfd >= 0)
		close(pc->errfd);
	event_del(&pc->child_dead_ev);
	close(pc->child_dead_pipe[0]);
	pthread_rwlock_wrlock(&pending_children_lock);
	if (pc->child_dead_pipe[1] != -1)
		close(pc->child_dead_pipe[1]);
	TAILQ_REMOVE(&pending_children, pc, list);
	--popen_limit_current;
	pthread_rwlock_unlock(&pending_children_lock);
	if (popen_limit_max > 0)
		sem_post(&sem_popen_concurrent);
	free(pc->cmd);
	free(pc->argv.buf);
	free(pc);
}

static void
sigchild(int ssock, short _ev, void *arg) {
	pid_t  pid;
	int status = 0;
	struct pending_child *pc;

	pthread_rwlock_rdlock(&pending_children_lock);
	while ((pid = wait3(&status, WNOHANG, NULL)) > 0) {
		if ((pc = find_pending_child(pid))) {
			DPRINTF(D_DEBUG, ("!!!!SIGCHLD(%p, %u) -> %p", arg, _ev, pc));
			/* XXX Any better way? */
			int cfd = pc->child_dead_pipe[1];
			pc->child_dead_pipe[1] = -1;
			pc->wait_status = status;
			close(cfd);
		}
	}
	pthread_rwlock_unlock(&pending_children_lock);
}

static void 
read_ev(struct bufferevent *be, void *arg) {
	struct pending_child *c = arg;
	DPRINTF(D_DEBUG, ("POPEN read_ev (%p)", arg));

	if(c->rcb)
		c->rcb(c->cb_arg, be);
}

static void 
write_ev(struct bufferevent *be, void *arg) {
	struct pending_child *c = arg;
	DPRINTF(D_DEBUG, ("POPEN write_ev (%p)", arg));

	if(c->wcb)
		c->wcb(c->cb_arg, be);
}

static void 
err_ev(struct bufferevent *be, short _ev, void *arg) {
	struct pending_child *c = arg;
	DPRINTF(D_DEBUG, ("POPEN err_ev (%s) (%p, %u)", be == c->errbuf ? "stderr" : "stdio", arg, _ev));
	/* Wait for SIGCHLD. child_dead(-1, _ev, arg);*/
}

static void 
read_err_ev(struct bufferevent *be, void *arg) {
	struct pending_child *c = arg;
	DPRINTF(D_DEBUG, ("POPEN read_err_ev (%p)", arg));

	if(c->ecb)
		c->ecb(c->cb_arg, be);
}


struct bufferevent *
ev_popen_base(struct event_base *base, const char *cmd, const char * const argv[], ev_popen_cb rcb, ev_popen_cb wcb, 
	 ev_popen_cb ecb, ev_popen_ccb ccb, void *cb_arg) {
	int iofd[2];
	int errfd[2];
	int null = 0;
	int congested = 0;
	pid_t pid;
	struct pending_child *child;

	if (popen_limit_max > 0 && sem_trywait(&sem_popen_concurrent) == -1) {
		congested = 1;
		pthread_rwlock_rdlock(&pending_children_lock);
		xwarnx("(WARN) ev_popen process limit hit, using %d of %d slots. %s stalled.", popen_limit_current, popen_limit_max, cmd);
		pthread_rwlock_unlock(&pending_children_lock);
		/* We may be blocked in the same thread, pump some events to see if things have cleared up */
		struct timespec ts_yield = { 1, 0 };
		while (sem_trywait(&sem_popen_concurrent) == -1) {
			event_base_loop(base, EVLOOP_NONBLOCK);
			nanosleep(&ts_yield, NULL); /* Yield to make sure we're not busy-looping */
		}
	}

	if (rcb || wcb) {
		if (socketpair(AF_LOCAL, SOCK_STREAM, PF_UNSPEC, iofd) == -1)
			xerr(1, "socketpair");
		if (fcntl(iofd[0], F_SETFD, FD_CLOEXEC))
			xerr(1, "fcntl");
		if (fcntl(iofd[1], F_SETFD, FD_CLOEXEC))
			xerr(1, "fcntl");
	}

	if (ecb) {
		if (socketpair(AF_LOCAL, SOCK_STREAM, PF_UNSPEC, errfd) == -1)
			xerr(1, "socketpair");
		if (fcntl(errfd[0], F_SETFD, FD_CLOEXEC))
			xerr(1, "fcntl");
		if (fcntl(errfd[1], F_SETFD, FD_CLOEXEC))
			xerr(1, "fcntl");
	}

	/* Need to lock here to avoid race of SIGCHLD arriving before we have the child in the tailq. */
	pthread_rwlock_wrlock(&pending_children_lock);

	++popen_limit_current;
	if (congested)
		++popen_stat_limit_hit;
	++popen_stat_calls;

	switch ((pid = fork())) {
	case -1:
		xerr(1, "fork");
	case 0:
		if (!ecb || !wcb || !rcb) {
			if ((null = open("/dev/null", O_RDWR)) == -1)
				xerr(1, "open");
			if (fcntl(null, F_SETFD, FD_CLOEXEC))
				xerr(1, "fcntl");
		}
		
		close(STDIN_FILENO);
		close(STDOUT_FILENO);
		close(STDERR_FILENO);

		if (setpgid(0, getpid()) != 0)
			xerr(1, "setpgid");

		if (wcb) {
			if (dup2(iofd[1], STDIN_FILENO) == -1) 
				xerr(1, "dup2");
		} else {
			if (dup2(null, STDIN_FILENO) == -1) 
				xerr(1, "dup2");
		}

		if (rcb) {
			if (dup2(iofd[1], STDOUT_FILENO) == -1) 
				xerr(1, "dup2");
		} else {
			if (dup2(null, STDOUT_FILENO) == -1) 
				xerr(1, "dup2");
		}

		if (ecb) {
			if (dup2(errfd[1], STDERR_FILENO) == -1) 
				xerr(1, "dup2");
		} else {
			if (dup2(null, STDERR_FILENO) == -1) 
				xerr(1, "dup2");
		}

		/* XXX I assume exec really accepts const argv data,
		 * even though not declared as such.
		 */
		execv(cmd, (char*const*)argv);
		_exit(127);
	}
	if (rcb || wcb)
		close(iofd[1]);
	if (ecb)
		close(errfd[1]);

	child = zmalloc(sizeof (struct pending_child));
	child->pid = pid;
	child->rcb = rcb;
	child->wcb = wcb;
	child->ecb = ecb;
	child->ccb = ccb;
	child->cb_arg = cb_arg;
	child->start_time = time(NULL);
	child->cmd = strdup(cmd);
	/* Make a string copy of the argv array for transinfo backend purposes */
	const char **argv_it = (const char**)argv;
	while (argv_it && *argv_it) {
		bscat(&child->argv, "%s%s", argv_it != argv ? " " : "", *argv_it);
		++argv_it;
	}

	if (rcb || wcb)
		child->iofd = iofd[0];
	else
		child->iofd = -1;
	if (ecb)
		child->errfd = errfd[0];
	else
		child->errfd = -1;
	if (pipe(child->child_dead_pipe))
		xerr(1, "pipe");
	event_set(&child->child_dead_ev, child->child_dead_pipe[0], EV_READ, child_dead, child);
	event_base_set(base, &child->child_dead_ev);
	event_add(&child->child_dead_ev, NULL);

	TAILQ_INSERT_HEAD(&pending_children, child, list);
	pthread_rwlock_unlock(&pending_children_lock);

	child->buf = bufferevent_new(iofd[0], read_ev, write_ev, err_ev, (void*)child);
	if (!child->buf)
		xerr(1, "bufferevent_new");
	bufferevent_base_set(base, child->buf);
	bufferevent_enable(child->buf, EV_WRITE | EV_READ);
	if (ecb) {
		child->errbuf = bufferevent_new(errfd[0], read_err_ev, NULL, err_ev, (void*)child);
		if (!child->errbuf)
			xerr(1, "bufferevent_new");
		bufferevent_base_set(base, child->errbuf);
		bufferevent_enable(child->errbuf, EV_READ);
	}
	return child->buf;
}

struct bufferevent *
ev_popen(const char *cmd, const char * const argv[], ev_popen_cb rcb, ev_popen_cb wcb, 
	 ev_popen_cb ecb, ev_popen_ccb ccb, void *cb_arg) {
	extern pthread_key_t csbase_key;
	struct event_base *base = pthread_getspecific(csbase_key);

	if (!base)
		xerr(1, "no csbase for ev_popen");

	return ev_popen_base(base, cmd, argv, rcb, wcb, ecb, ccb, cb_arg);
}

extern struct event_base *trans_base;

static void
ev_popen_transinfo(struct transaction *ts) {
	struct pending_child *pc;
	int i = 0;

	pthread_rwlock_rdlock(&pending_children_lock);
	transaction_printf(ts, "ev_popen.load=%d/%d\n", popen_limit_current, popen_limit_max);
	transaction_printf(ts, "ev_popen.times_called=%zu\n", popen_stat_calls);
	transaction_printf(ts, "ev_popen.times_congested=%zu\n", popen_stat_limit_hit);
	TAILQ_FOREACH (pc, &pending_children, list) {
		transaction_printf(ts, "ev_popen.pending.%d.pid=%d\n", i, (int)pc->pid);
		transaction_printf(ts, "ev_popen.pending.%d.cmd=%s\n", i, pc->cmd);
		transaction_printf(ts, "ev_popen.pending.%d.argv=%s\n", i, pc->argv.buf ?: "<n/a>");
		transaction_printf(ts, "ev_popen.pending.%d.time=%lu (elapsed: %lds)\n", i, pc->start_time, time(NULL) - pc->start_time);
		++i;
	}
	pthread_rwlock_unlock(&pending_children_lock);
}

static void
ev_popen_reload(void *data) {
	/* We don't support changing the limit during a reload, but we can at least document that and warn. */
	if (popen_limit_max != conf_limit) {
		xwarnx("[ev_popen_backend] A restart is required to change the ev_popen limit from %d to %d", popen_limit_max, conf_limit);
	}
}

static int
ev_popen_config(struct bconf_node *conf) {

	conf_limit = bconf_get_int_default(conf, "ev_popen.limit", 0);
	return 0;
}

static void
ev_popen_init(char *check_only) {
	if (check_only)
		return;

	popen_limit_max = conf_limit;
	xwarnx("[ev_popen_backend] ev_popen limit is %d%s", popen_limit_max, popen_limit_max < 1 ? " (disabled)" : "");
	if (popen_limit_max > 0) {
		if (sem_init(&sem_popen_concurrent, 0, popen_limit_max)) {
			xerrx(1, "[ev_popen_backend] Semaphore initialization failed.");
		}
	}

	signal_set(&signal_ev, SIGCHLD, sigchild, &signal_ev);
	event_base_set(trans_base, &signal_ev);
	signal_add(&signal_ev, NULL);

	reload_register(ev_popen_reload, NULL);
}

static void
ev_popen_deinit(void) {
	if (popen_limit_max > 0) {
		sem_destroy(&sem_popen_concurrent);
	}
}

ADD_BACKEND(ev_popen, ev_popen_config, ev_popen_init, NULL, ev_popen_deinit, ev_popen_transinfo);

