#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdarg.h>
#include <unistd.h>
#include <netdb.h>

#include "command.h"
#include "trans.h"
#include "util.h"
#include "ev_popen.h"
#include <ctemplates.h>
#include "trans_mail.h"
#include "bconf.h"
#include "strl.h"

static struct c_param parameters[] = {
	{"name",	P_TEXT,	"v_string_all"},
	{"from",	P_TEXT,	"v_string_all"},
	{"to",		0,	"v_string_all"},
	{"subject",	P_TEXT,	"v_string_all"},
	{"body",	P_TEXT,	"v_string_all_blob"},
	{"wrapper_template",     0,      "v_mail_template"},
	{"tmpl",        0,      "v_mail_template"},
	{"attachment",  0,      "v_mail_attachment"},
	{"application", 0,      "v_string_all"},
	{NULL, 0}
};

struct validator v_mail_template[] = {
	VALIDATOR_REGEX("^[a-z0-9_]+/[a-z0-9_./]+$", REG_EXTENDED, "ERROR_INVALID_TEMPLATE"),
	{0}
};
ADD_VALIDATOR(v_mail_template);

struct validator v_mail_attachment[] = {
	VALIDATOR_REGEX("^[a-zA-Z0-9_()./-]+$", REG_EXTENDED, "ERROR_INVALID_FILENAME"),
	{0}
};
ADD_VALIDATOR(v_mail_attachment);

struct mail_outstring_cbdata {
	struct bufferevent *be;
	int bytes_written;
};

static int 
evbuffer_outstring_fmt(struct bpapi_output_chain *ochain, const char *fmt, ...) {
	int res;
	struct mail_outstring_cbdata *cbdata = ochain->data;
	struct bufferevent *be = cbdata->be;
	va_list ap;

	va_start(ap, fmt);
	res = evbuffer_add_vprintf(be->output, fmt, ap);
	va_end(ap);
	cbdata->bytes_written += res;

	return res;
}

static int 
evbuffer_outstring_raw(struct bpapi_output_chain *ochain, const char *str, size_t len) {
	struct mail_outstring_cbdata *cbdata = ochain->data;
	struct bufferevent *be = cbdata->be;
	int res = evbuffer_add(be->output, (void*)str, len);
	if (!res)
		cbdata->bytes_written += len;
	return res;
}

const struct bpapi_output evbuffer_output = {
	evbuffer_outstring_fmt,
	evbuffer_outstring_raw,
	NULL,
	NULL
};

static void
mail_read(void *arg, struct bufferevent *be) {
	char *line;
	struct mail_data *md = arg;

	DPRINTF(D_DEBUG, ("(%s) mail_read", md->log_string));

	while ((line = evbuffer_readline(be->input))) {
		DPRINTF(D_INFO, ("(%s) Mail read: %s", md->log_string, line));
		free(line);
	}
}

static void
mail_write(void *arg, struct bufferevent *be) {
	struct mail_data *md = arg;
	struct command_bpapi cba;

	switch (md->status) {
	case 0: {
		struct mail_outstring_cbdata cbdata = { .be = be, .bytes_written = 0 };
		int crit_limit;

		command_bpapi_init(&cba, NULL, CMD_BPAPI_HOST|CMD_BPAPI_NO_OUTPUT|CMD_BPAPI_NO_SIMPLEVARS, md->bconf_app);

		cba.ba.ochain.fun = &evbuffer_output;
		cba.ba.ochain.data = &cbdata;
		if (!md->vars) {
			hash_simplevars_init(&md->schain);
			md->vars = &md->schain;
		}
		cba.ba.schain = *md->vars;

		if (!bpapi_has_key(&cba.ba, "appl"))
			bpapi_insert(&cba.ba, "appl", "mail");

		if (!call_template_lang(&cba.ba, md->template, md->lang, bconf_get_string(trans_bconf, "common.site_country")))
			DPRINTF(D_ERROR, ("(CRIT) (%s) tried to call nonexisting template: %s", md->log_string, md->template));

		/* CRIT if an unreasonably small amount of template output was generated for this mail */
		if (cbdata.bytes_written < (crit_limit = bconf_get_int_default(trans_bconf, "common.mail.crit_below_length", 2)))
			DPRINTF(D_ERROR, ("(CRIT) (%s) mail template %s:%s output of %d bytes below threshold of %d.", md->log_string, md->template, md->lang, cbdata.bytes_written, crit_limit));

		bpapi_output_free(&cba.ba);
		command_bpapi_free(&cba);

		DPRINTF(D_DEBUG, ("(%s) Done writing mail", md->log_string));
		md->status = 1;
		bufferevent_enable(be, EV_WRITE);
		return;
		}
	case 1:
		DPRINTF(D_DEBUG, ("(%s) Drained, close output fd", md->log_string));

		shutdown(be->ev_write.ev_fd, SHUT_WR);
	}
}

static void
mail_err_read(void *arg, struct bufferevent *be) {
	char *line;
	struct mail_data *md = arg;

	DPRINTF(D_DEBUG, ("(%s) mail_err_read", md->log_string));

	while ((line = evbuffer_readline(be->input))) {
		DPRINTF(D_INFO, ("(%s) Mail err read: %s", md->log_string, line));
		free(line);
	}
}


static void
mail_close(void *arg, int ret) {
	struct mail_data *md = arg;
	char waitbuf[256];

	DPRINTF(D_DEBUG, ("(%s) Mail return value : %s", md->log_string, strwait(ret, waitbuf, sizeof(waitbuf))));
	md->callback(md, md->command, ret);

	/*
	 * Do not free log_string in mail_free since we should only free it if mail_send has been called.
	 * Yes, a bit messy.
	 */
	free((char*)md->log_string);
	mail_free(md);
}

static void
mail_done(struct mail_data *md, void *arg, int status) {
	struct cmd *cs = arg;

	if (status != 0) {
                cs->status = "TRANS_MAIL_ERROR";
                cmd_done(cs);	
		return;
	}

	cmd_done(cs);
}

void mail_free(struct mail_data *md)
{
	bps_free(&md->schain);
	free(md);
}

const char *
mail_get_param(struct mail_data *md, const char *name) {
	if (!md->vars)
		return NULL;

	return bps_get_element(md->vars, name, 0);
}

/*
   Translate the empty string and 'none' to NULL for us, to simplify the code.
*/
static const char*
translate_none(const char* wrapper_value) {

	if (wrapper_value && (!*wrapper_value || strcmp(wrapper_value, "none") == 0))
		wrapper_value = NULL;

	return wrapper_value;
}

void
mail_send(struct mail_data *md, const char *template, const char *lang) {
	const char *mail_command;
	const char *sender = NULL;
	const char *name = NULL;
	const char *from = NULL;
	const char **argv = NULL;
	const char *smtp_name = NULL;
	const char *remote_addr = NULL;
	char *smtp_from = NULL;
	int argc = 0;
	int i;
	int sender_mail_from = 0;

	const char *wrapper_template = translate_none(bconf_get_string_default(trans_bconf, "common.mail.wrapper_template", "mail/qp_wrapper.mime"));

	md->status = 0;
	md->template = template;
	if (lang == TRANS_MAIL_NOLANG)
		md->lang = NULL;
	else if (lang)
		md->lang = lang;
	else if (!(md->lang = mail_get_param(md, "lang")))
		md->lang = bconf_get_string(trans_bconf, "common.default.lang");

	/* Make a local copy of the log_string, since we might still be running when transaction ends */
	if (md->log_string)
		md->log_string = xstrdup(md->log_string);

	DPRINTF(D_DEBUG, ("(%s) in mail", md->log_string));

	if (md->vars) {
		remote_addr = bps_get_element(md->vars, "remote_addr", 0); 
		name = bps_get_element(md->vars, "name", 0);
		from = bps_get_element(md->vars, "from", 0);
		const char *wt = bps_get_element(md->vars, "wrapper_template", 0);
		if (wt)
			wrapper_template = translate_none(wt);
	}

	argv = xmalloc(sizeof(char*) * 9);
	argv[argc++] = "sendmail";

	if (name && from) {
		int res;

		smtp_name = name;
		ALLOCA_PRINTF(res, smtp_from, "<%s>", from);
		char *ep;

		/* XXX writing into the simplevar values. Should be ok but ugly. */
		while ((ep = strpbrk(name, "\\\"`$")))
			*ep = ' ';
		while ((ep = strpbrk(from, "\\\"`$")))
			*ep = ' ';
	}

	const char *local_appl = NULL;
	if (md->vars && (local_appl = bps_get_element(md->vars, "application", 0))) {
		sender = bconf_vget_string(trans_bconf, "common.brand.mail", local_appl, md->lang, "simple", NULL);
		if (!sender)
			sender = bconf_vget_string(trans_bconf, "common.brand.mail", local_appl, "simple", NULL);
		if (!smtp_from) {
			smtp_name = bconf_vget_string(trans_bconf, "common.brand.mail", local_appl, "name", NULL);
			smtp_from = (char*)bconf_vget_string(trans_bconf, "common.brand.mail", local_appl, "from", NULL);
		}
		sender_mail_from = bconf_vget_int(trans_bconf, "common.brand.mail", local_appl, "sender_mail_from.enabled", NULL);

	}
	if (!local_appl)
		local_appl = md->bconf_app;

	if (local_appl && remote_addr) {
		if (bconf_get_int(bconf_vget(trans_bconf, "common", "trans_mail", "reverse_dns", NULL), local_appl) == 1) {
			char hostname[NI_MAXHOST] = { 0 };
			if (get_hostname_by_addr(remote_addr, hostname, sizeof(hostname), 0) == 1)
				mail_insert_param(md, "REMOTE_HOSTNAME", hostname);	
		} else
			DPRINTF(D_DEBUG, ("(DEBUG) Reverse DNS not enabled for appl '%s'", local_appl));
	}

	if (!sender) {
		sender = bconf_get_string(trans_bconf, "common.brand.mail.no_reply.simple");
		sender_mail_from = bconf_get_int(trans_bconf, "common.brand.mail.no_reply.sender_mail_from.enabled");
	}
	if (!sender)
		xerrx(1, "No common.brand.mail.no_reply.simple mail address in bconf");
	if (!smtp_from) {
		smtp_name = bconf_get_string(trans_bconf, "common.brand.mail.no_reply.name");
		smtp_from = (char*)bconf_get_string(trans_bconf, "common.brand.mail.no_reply.from");
	}
	
	mail_insert_param(md, "sender", sender);

	if (wrapper_template) {
		mail_insert_param(md, "mail_template", md->template);
		md->template = wrapper_template;
	}
	
	mail_command = bconf_get_string(trans_bconf, "common.sendmail_path");
	argv[argc++] = "-t";
	argv[argc++] = "-i";
	if (smtp_name) {
		mail_insert_param(md, "trans_mail_name", smtp_name);
		mail_insert_param(md, "trans_mail_from", smtp_from);
	}
	argv[argc++] = "-r";
	if (sender && sender_mail_from) {
		argv[argc++] = sender;
	} else {
		argv[argc++] = smtp_from;
	}
	argv[argc++] = NULL;

	const char* original_template = bps_get_element(md->vars, "mail_template", 0);
	if (original_template)
		DPRINTF(D_DEBUG, ("mailing %s (via %s) lang=%s", original_template, md->template, md->lang));
	else
		DPRINTF(D_DEBUG, ("mailing %s lang=%s", md->template, md->lang));

	DPRINTF(D_DEBUG, ("(%s) Will run %s", md->log_string, mail_command));
	for (i = 0; i < argc; i++)
		DPRINTF(D_DEBUG, ("argv[%d] = %s", i, argv[i]));

	if (md->base)
		ev_popen_base(md->base, mail_command, argv, mail_read, mail_write, mail_err_read, mail_close, md);
	else
		ev_popen(mail_command, argv, mail_read, mail_write, mail_err_read, mail_close, md);

	if (argv)
		free(argv);
}

void
mail_insert_param(struct mail_data *md, const char *key, const char *value) {
	if (!md->vars) {
		hash_simplevars_init(&md->schain);
		md->vars = &md->schain;
	}

	bps_insert(md->vars, key, value ?: "");
}

void
mail_insert_int(struct mail_data *md, const char *key, int value) {
	if (!md->vars) {
		hash_simplevars_init(&md->schain);
		md->vars = &md->schain;
	}

	bps_set_int(md->vars, key, value);
}

void
mail_insert_paramf(struct mail_data *md, const char *key, const char *fmt, ...) {
	char *buf;
	int len;

	if (!md->vars) {
		hash_simplevars_init(&md->schain);
		md->vars = &md->schain;
	}

	ALLOCA_VPRINTF(len, buf, fmt);

	bps_insert(md->vars, key, buf);
}


static void
mail(struct cmd *cs) {
	struct mail_data *md;
	const char *lang = NULL;
	md = zmalloc(sizeof (*md));
	md->base = cs->base;
	md->command = cs;
	md->callback = mail_done;
	md->log_string = cs->ts->log_string;

	if (cmd_haskey(cs, "name"))
		mail_insert_param(md, "name", cmd_getval(cs, "name"));
	if (cmd_haskey(cs, "from"))
		mail_insert_param(md, "from", cmd_getval(cs, "from"));
	if (cmd_haskey(cs, "to"))
		mail_insert_param(md, "to", cmd_getval(cs, "to"));
	if (cmd_haskey(cs, "subject"))
		mail_insert_param(md, "subject", cmd_getval(cs, "subject"));
	if (cmd_haskey(cs, "body"))
		mail_insert_param(md, "body", cmd_getval(cs, "body"));
	if (cmd_haskey(cs, "wrapper_template"))
		mail_insert_param(md, "wrapper_template", cmd_getval(cs, "wrapper_template"));
	if (cmd_haskey(cs, "attachment"))
		mail_insert_param(md, "attachment", cmd_getval(cs, "attachment"));
	if (cmd_haskey(cs, "application"))
		mail_insert_param(md, "application", cmd_getval(cs, "application"));
	lang = cmd_getval(cs, "lang");

	if (cmd_haskey(cs, "tmpl"))
		mail_send(md, cmd_getval(cs, "tmpl"), lang);
	else
		mail_send(md, "mail/empty_mail.txt", lang);
}

ADD_COMMAND(mail, NULL, mail, parameters, NULL);
