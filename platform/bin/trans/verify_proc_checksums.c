#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <libintl.h>
#include <openssl/md5.h>

#include "verify_proc_checksums.h"
#include "trans.h"
#include "command.h"
#include "util.h"
#include "strl.h"
#include "sql_worker.h"

extern struct stored_proc_checksum stored_procs[];
extern int init_status;
extern struct bconf_node *trans_conf;

static const char *
verify_cb(void *arg, struct sql_worker *worker, const char *errstr) {
	struct stored_proc_checksum *proc;
	
	if (errstr) {
		xwarnx("SQL error when verifying procedures in db %s:%s: %s", worker ? worker->sw_sc->sc_host : "unknown", worker ? worker->sw_sc->sc_name : "unknown", errstr);
		init_status = 1;
		return NULL;
	}
	
	for (proc = stored_procs; proc->name; proc++) {
		if (worker->next_row(worker)) {
			unsigned char buf[MD5_DIGEST_LENGTH];
			char hash[(2 * MD5_DIGEST_LENGTH) + 1];
			const char *promd5 = hash;
			const char *proc_src = worker->get_value(worker, 1);

			MD5((unsigned char*)proc_src, strlen(proc_src), buf);

			int i;
			char *current = hash;
			for (i = 0; i < MD5_DIGEST_LENGTH; i++) {
				current += sprintf(current, "%02x", (unsigned int)buf[i]);
			}
			*current = '\0';

			const char *proname = worker->get_value(worker, 0);
			const char *procount = worker->get_value(worker, 2);

			if (atoi(procount) != 1) {
				xwarnx("Duplicate procedure '%s' in db %s:%s", proname, worker->sw_sc->sc_host, worker->sw_sc->sc_name);
				init_status = 1;
			}
			
			if (strcmp(proname, proc->name)) {
				DPRINTF(D_DEBUG, ("Skipped procedure '%s'", proname));
				proc--;
				continue;
			}
			if (strcmp(promd5, proc->md5)) {
				xwarnx("MD5 sum mismatch for procedure '%s' (db-md5='%s' vs file-md5='%s') in db %s:%s", proname, promd5, proc->md5, worker->sw_sc->sc_host, worker->sw_sc->sc_name);
				init_status = 1;
			}
		} else {
			DPRINTF(D_DEBUG, ("Checking procedure '%s'", proc->name));
			xwarnx("Too few stored procedures in db %s:%s", worker->sw_sc->sc_host, worker->sw_sc->sc_name);
			init_status = 1;
		}
	}
	return NULL;
}

void verify_proc_checksums(void) {
	int i, j;
	struct bconf_node *engine;
	struct bconf_node *node;
	char config_name[256];
	struct bconf_node *dnode = bconf_get(trans_conf, "db");

	if (stored_procs[0].name == NULL) {
		xwarnx("No md5-sums of local procs found to compare with database procs");
		init_status = 1;
	} else {
		for (i = 0 ; (engine = bconf_byindex(dnode, i)) ; i++) {
			for (j = 0 ; (node = bconf_byindex(engine, j)) ; j++) {
				snprintf(config_name, sizeof(config_name), "%s.%s", bconf_key(engine), bconf_key(node));

				sql_worker_base_template_query(config_name, NULL, "sql/verify_proc_checksums.sql", NULL,
						verify_cb, node, "verify_proc");
			}
		}
	}
}
