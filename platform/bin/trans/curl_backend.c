#include <curl/curl.h>
#include "trans.h"

static void
init(char *check_only) {
	CURLcode r;
	if (check_only)
		return;

	if ((r = curl_global_init(CURL_GLOBAL_ALL)) != CURLE_OK)
		xerrx(1, "Failed to init curl backend: %s", curl_easy_strerror(r));
}

static void
deinit(void) {
	curl_global_cleanup();
}

ADD_BACKEND(curl, NULL, init, NULL, deinit, NULL);
