#include <time.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "trans.h"
#include "trans_timers.h"
#include "command.h"

static void
trans_timers_transinfo_timer(struct timer_class *tc, void *data) {
	struct transaction *ts = data;
	struct timespec avgts;
	double avg = (double)tc->tc_total.tv_sec + (double)tc->tc_total.tv_nsec / 1000000000.0;
	char *name, *n;

	if (tc->tc_count == 0)
		return;

	n = name = xstrdup(tc->tc_name);

	while ((n = strchr(n, '.')) != NULL)
		*n = '_';

	avg /= (double)tc->tc_count;

	avgts.tv_sec = avg;
	avgts.tv_nsec = (avg - (double)avgts.tv_sec) * 1000000000.0;

	transaction_printf(ts, "timer.%s.count:%lld\n", 
			   name, tc->tc_count);
	transaction_printf(ts, "timer.%s.total:%d.%03ld\n", 
			   name, (int)tc->tc_total.tv_sec, tc->tc_total.tv_nsec / 1000000);
	transaction_printf(ts, "timer.%s.min:%d.%03ld\n", 
			   name, (int)tc->tc_min.tv_sec, tc->tc_min.tv_nsec / 1000000);
	transaction_printf(ts, "timer.%s.max:%d.%03ld\n", 
			   name, (int)tc->tc_max.tv_sec, tc->tc_max.tv_nsec / 1000000);
	transaction_printf(ts, "timer.%s.average:%d.%03ld\n", 
			   name, (int)avgts.tv_sec, avgts.tv_nsec / 1000000);
	free(name);
}

void
trans_timers_transinfo(struct transaction *ts) {
	timer_foreach(trans_timers_transinfo_timer, ts);
}

static void
timerreset(struct cmd *cs) {
	timer_reset();
	cmd_done(cs);
}

ADD_COMMAND(timerreset, NULL, timerreset, NULL, "Reset timers");
