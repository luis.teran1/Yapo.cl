#ifndef EV_POPEN_H
#define EV_POPEN_H

#include <sys/socket.h>
struct bufferevent;
struct event_base;

typedef void (*ev_popen_cb)(void *, struct bufferevent *);
typedef void (*ev_popen_ccb)(void *, int);

struct bufferevent *ev_popen_base(struct event_base *base, const char *, const char *const*, ev_popen_cb, ev_popen_cb, 
		ev_popen_cb, ev_popen_ccb, void *);
struct bufferevent *ev_popen(const char *, const char *const*, ev_popen_cb, ev_popen_cb, 
		ev_popen_cb, ev_popen_ccb, void *);

pid_t ev_popen_get_pid(struct bufferevent *be);

#endif
