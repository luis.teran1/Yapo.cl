/*
	Mail Queue abstraction with batching.

	NOTE: Not thread-safe by design. Create a transaction-local queue, use it, destroy it.
*/
#ifndef PLATFORM_MAIL_QUEUE_H
#define PLATFORM_MAIL_QUEUE_H

struct mail_queue;
struct mail_data;

/*
	Create a new mail_queue.

		'ts'	is only used for logging.
		'base'	Should be set to NULL in most cases, meaning you get a new event base only used for this queue.
*/
struct mail_queue *mail_queue_new(struct transaction *ts, struct event_base *base);

/*
		Add a mail to the queue.

		Same arguments as to mail_send() except with the mail_queue state up front.
*/
void mail_queue_add(struct mail_queue *mq, struct mail_data *md, const char *template, const char *lang);

/*	Flush (send) the mail on the queue, in batches of batch_size.

		'batch_size'	Upper limit on the number of mail processess you want to allow in flight.
						If batch_size zero or less a suitable default value will be used instead.

*/
int mail_queue_flush(struct mail_queue *mq, int batch_size);

void mail_queue_destroy(struct mail_queue *mq);

#endif

