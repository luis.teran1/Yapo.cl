#ifndef PGSQL_API_H
#define PGSQL_API_H

struct sql_worker;

void pgsql_init_worker(struct sql_worker *sw);
int pgsql_split_array(char *text, int len, char ***elems);

#endif
