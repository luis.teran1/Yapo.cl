#include <errno.h>

#include "trans.h"
#include "trans_mail.h"
#include "queue.h"

#include "mail_queue.h"

struct mq_entry {
	struct mail_data *md;
	char *template;
	char *lang;

	TAILQ_ENTRY(mq_entry) qe;
};

struct mail_queue {
	int queue_len;
	int free_base;
	struct event_base *event_base;
	TAILQ_HEAD(, mq_entry) q;
	TAILQ_HEAD(, mq_entry) q_running;

	struct transaction *ts; /* used only for logging */
};

struct mail_queue *
mail_queue_new(struct transaction *ts, struct event_base *base) {

	struct mail_queue *mq = xmalloc(sizeof(*mq));

	TAILQ_INIT(&mq->q);
	TAILQ_INIT(&mq->q_running);
	if (base) {
		mq->event_base = base;
		mq->free_base = 0;
	} else {
		mq->event_base = event_base_new();
		mq->free_base = 1;
	}

	mq->queue_len = 0;
	mq->ts = ts;

	return mq;
}

void
mail_queue_add(struct mail_queue *mq, struct mail_data *md, const char *template, const char *lang) {

	struct mq_entry *e = xmalloc(sizeof(*e));

	e->md = md;
	/* Take over event base */
	e->md->base = mq->event_base;
	e->template = template ? xstrdup(template) : NULL;
	e->lang = lang ? xstrdup(lang) : NULL;

	TAILQ_INSERT_TAIL(&mq->q, e, qe);
	++mq->queue_len;
}

static int mail_queue_send_batch(struct mail_queue *mq, int batch_size) {
	struct mq_entry *e;

	transaction_logprintf(mq->ts, D_DEBUG, "Request for dispatch of batch of %d mails, mq state at %p", batch_size, (void*)mq);

	int num = 0;
	while ((e = TAILQ_FIRST(&mq->q))) {
		TAILQ_REMOVE(&mq->q, e, qe);
		TAILQ_INSERT_TAIL(&mq->q_running, e, qe);
		if (++num >= batch_size)
			break;
	}

	transaction_logprintf(mq->ts, D_DEBUG, "Prepared %d mails in batch", num);

	/* Send mail */
	TAILQ_FOREACH(e, &mq->q_running, qe) {
		mail_send(e->md, e->template, e->lang);
	}

	/* Process all events */
	while (event_base_dispatch(mq->event_base) < 0)
		DPRINTF(D_INFO, ("mail queue event dispatch returned error: %s", xstrerror(errno)));

	/* Remove and free processed mail queue entries. At this point the original 'md' has already been freed. */
	while ((e = TAILQ_FIRST(&mq->q_running))) {
		TAILQ_REMOVE(&mq->q_running, e, qe);
		if (e->template)
			free(e->template);
		if (e->lang)
			free(e->lang);
		free(e);
	}
	mq->queue_len -= num;

	return num;
}

int mail_queue_flush(struct mail_queue *mq, int batch_size) {
	if (batch_size < 1)
		batch_size = 16;

	transaction_logprintf(mq->ts, D_INFO, "MQ: Starting to flush %d mails from mail queue at %p, in batches of %d", mq->queue_len, (void*)mq, batch_size);
	int processed = 0;
	while (!TAILQ_EMPTY(&mq->q)) {
		processed += mail_queue_send_batch(mq, batch_size);
	}
	transaction_logprintf(mq->ts, D_INFO, "MQ: Completed flush of %d mails on mail queue at %p (status not tracked)", processed, (void*)mq);

	return processed;
}


void mail_queue_destroy(struct mail_queue *mq) {
	if (!mq)
		return;

	if (mq->free_base)
		event_base_free(mq->event_base);
	free(mq);
}

