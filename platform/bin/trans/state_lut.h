#ifdef STATE_LUT_FIRST_PASS
#undef STATE_LUT_FIRST_PASS
#define STATE_LUT_SECOND_PASS

#undef STATE_ENUM
#undef STATE

#define STATE_ENUM(x) enum x ## _state_e
#define STATE(x) x

#define STATE_NAME(x, y) (x ## _state_lut[y])
#define STATE_VAR(x) enum x ## _state_e

#else
#define STATE_LUT_FIRST_PASS

#define STATE_ENUM(x) const char * const x ## _state_lut[] =
#define STATE(x) #x

#endif
