#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <time.h>

#include "trans.h"
#include "util.h"
#include <ctemplates.h>
#include <bconf.h>
#include "command.h"
#include "sql_worker.h"

#define DB_SNAP_DONE  0
#define DB_SNAP_ERROR 1
#define DB_SNAP_INIT  2
#define DB_SNAP_BEGIN  3
#define DB_SNAP_DOIT   4
#define DB_SNAP_ACTION 5
#define DB_SNAP_SPIN 6
#define DB_SNAP_FLUSH_FOREIGN 7

extern struct bconf_node *bconf_root;

struct db_snap_state {
	int state;
	const char * const *actions;
	struct cmd *cs;
	time_t start_time;
};

static struct c_param parameters[] = {
	{"action", P_REQUIRED, "v_array:store,snap,restore"},
	{"name", P_REQUIRED, "v_db_snap_name"},
	{NULL, 0}
};

struct validator v_db_snap_name[] = {
	VALIDATOR_REGEX("^[A-Za-z0-9_-]+$", REG_EXTENDED, "ERROR_NAME_INVALID"),
	{0}
};
ADD_VALIDATOR(v_db_snap_name);

static const char *db_snap_fsm(void *, struct sql_worker *, const char *);


/*****************************************************************************
 * db_snap_done
 * Exit function.
 **********/
static void
db_snap_done(void *v) {
	struct db_snap_state *state = v;
	struct cmd *cs = state->cs;
	free(state);
	cmd_done(cs);
}

/*****************************************************************************
 * bind_insert
 * Initialise template API before a call to sql_worker_template_query().
 **********/
static void
bind_insert(struct bpapi *ba, struct db_snap_state *state) {
	struct c_param *param;

	for (param = parameters; param->name; param++) {
		if (cmd_haskey(state->cs, param->name))
			bpapi_insert(ba, param->name, cmd_getval(state->cs, param->name));
	}
}

/*****************************************************************************
 * db_snap_fsm
 * State machine
 **********/
static const char *
db_snap_fsm(void *v, struct sql_worker *worker, const char *errstr) {
	struct db_snap_state *state = v;
	struct cmd *cs = state->cs;
	struct bpapi ba;
	int i;
	int flush_delay = 4;
	const char * const actions[] = {"snap", "store", "restore", NULL};
	const char * const action_templates[][5] = {
		{"sql/db_snap_drop_snap.sql", "sql/db_snap_create_snap.sql", "sql/db_snap_drop_blocketdb.sql", "sql/db_snap_restore_pristine.sql", NULL},
		{"sql/db_snap_drop_snap.sql", "sql/db_snap_create_snap.sql", NULL},
		{"sql/db_snap_drop_blocketdb.sql", "sql/db_snap_restore_snap.sql", NULL},
	};

	while (1) {
		transaction_logprintf(cs->ts, D_DEBUG, "db_snap_fsm(%p, %p, %p), state=%d",
				      v, worker, errstr, state->state);

		switch (state->state) {
		case DB_SNAP_INIT:
		{
			flush_delay = bconf_get_int_default(host_bconf, "*.common.trans_db_snap.external.flush_delay", flush_delay);

			const char *action = cmd_getval(cs, "action");
			int action_idx;

			/* next state */
			for (action_idx = 0; actions[action_idx]; action_idx++) {
				if (!strcmp (action, actions[action_idx]))
					break;
			}
			if (!actions[action_idx]) {
				state->state = DB_SNAP_ERROR;
				cs->error = "ERROR_INVALID_ACTION";
				cs->status = "TRANS_ERROR";
				continue;
			}
			state->actions = action_templates[action_idx];

			state->state = DB_SNAP_BEGIN;
			trans_pending_wait(cs->ts, 1);
			continue;
		}

		case DB_SNAP_BEGIN:
		{
			state->state = DB_SNAP_DOIT;

			BPAPI_INIT(&ba, NULL, pgsql);
			bind_insert(&ba, state);
			worker = sql_blocked_template_query("pgsql.master", SQLC_CONTROL, "sql/db_snap_begin.sql",
					&ba, cs->ts->log_string, &errstr);
			bpapi_output_free(&ba);
			bpapi_simplevars_free(&ba);
			bpapi_vtree_free(&ba);
			continue;
		}

		case DB_SNAP_DOIT:
		{
			int nconn;

			if (!worker) {
				state->state = DB_SNAP_ERROR;
				continue;
			}
			if (sql_blocked_next_result(worker, &errstr) != 1) {
				state->state = DB_SNAP_ERROR;
				continue;
			}
			worker->next_row(worker);
			nconn = atoi(worker->get_value(worker, 0));
			if (nconn > 0) {
				time_t present = time(NULL);
				if (present > state->start_time + flush_delay) {
					transaction_logprintf(cs->ts, D_INFO, "db-snap blocked for %d seconds, attempting flush", flush_delay);
					state->start_time = present;
					state->state = DB_SNAP_FLUSH_FOREIGN;
					continue;
				}

				/* "spinlock" waiting for connection to close. */
				state->state = DB_SNAP_SPIN;
				continue;
			}

			const char *action = cmd_getval(cs, "action");
			struct db_snap_action * const *f;

			/* at_reset */
			LINKER_SET_DECLARE(db_snap_actions, struct db_snap_action);
			LINKER_SET_FOREACH(f, db_snap_actions) {
				(**f).fun(cs, action);
			}

			state->state = DB_SNAP_ACTION;
			continue;
		}

		case DB_SNAP_SPIN: {
			BPAPI_INIT(&ba, NULL, pgsql);
			bind_insert(&ba, state);
			if (sql_blocked_template_newquery(worker, "sql/db_snap_begin.sql", &ba, &errstr) == -1)
				state->state = DB_SNAP_ERROR;
			bpapi_output_free(&ba);
			bpapi_simplevars_free(&ba);
			bpapi_vtree_free(&ba);
			state->state = DB_SNAP_DOIT;
			continue;
		}

		case DB_SNAP_FLUSH_FOREIGN: {
			/* Scan *.*.common.trans_db_snap.external.flush.X.node=*.mama_review.external_server */
			struct bconf_node *flushnode;
			struct bconf_node *snapnode = bconf_get(host_bconf, "*.common.trans_db_snap.external.flush");
			for (i = 0 ; (flushnode = bconf_byindex(snapnode, i)) ; ++i) {
				const char *exts = bconf_get_string(flushnode, "node");
				if (exts) {
					/* Verify node exists before doing internal transaction */
					struct bconf_node *external = bconf_get(bconf_root, exts);
					if (!external) {
						transaction_logprintf(cs->ts, D_DEBUG, "Node '%s' does not exist", exts);
						continue;
					}

					transaction_logprintf(cs->ts, D_INFO, "Calling internal transaction to flush workers on %s", exts);
					transaction_internal_printf(TRANSACTION_INTERNAL_SYNC_CB, NULL,
						"cmd:flush_workers\n"
						"target:%s\n"
						"suspend:1\n"
						"flushall:1\n"
						"commit:1\n",
						exts
					);
				}
			}

			state->state = DB_SNAP_SPIN;
			continue;
		}

		case DB_SNAP_ACTION:
			if (!*state->actions) {
				state->state = DB_SNAP_DONE;
				continue;
			}

			BPAPI_INIT(&ba, NULL, pgsql);
			bind_insert(&ba, state);
			if (sql_blocked_template_newquery(worker, *state->actions++, &ba, &errstr) == -1)
				state->state = DB_SNAP_ERROR;

			bpapi_output_free(&ba);
			bpapi_simplevars_free(&ba);
			bpapi_vtree_free(&ba);

			if (state->state == DB_SNAP_ERROR)
				continue;

			if (sql_blocked_next_result(worker, &errstr) != 1)
				state->state = DB_SNAP_ERROR;
			continue;

		case DB_SNAP_DONE: {
			sql_worker_reenable_all(worker);
			sql_worker_put(worker);
			db_snap_done(state);
			return NULL;
		}

		case DB_SNAP_ERROR:
			if (errstr) {
				transaction_logprintf(cs->ts, D_ERROR, "db_snap_fsm statement failed (%d, %s)",
						      state->state,
						      errstr);
				cs->status = "TRANS_DATABASE_ERROR";
				cs->message = xstrdup(errstr);
			}

			transaction_logprintf(cs->ts, D_ERROR, "Error state");
			if (worker) {
				sql_worker_reenable_all(worker);
				sql_worker_put(worker);
			}
			db_snap_done(state);
			return NULL;

		default:
			/* MAJOR STATE ERROR */
			cs->status = "TRANS_ERROR";
			transaction_logprintf(cs->ts, D_ERROR, "Unknown state (%d)",
					      state->state);
			return NULL;

		}
	}
}

/*****************************************************************************
 * db_snap
 * Main function that initialise a state-structure and calls the state machine.
 **********/
static void
db_snap(struct cmd *cs) {
	transaction_logprintf(cs->ts, D_DEBUG, "Start DB snap transaction");
	struct db_snap_state *state;
	state = zmalloc(sizeof(*state));
	state->cs = cs;
	state->state = DB_SNAP_INIT;
	state->start_time = time(NULL);

	db_snap_fsm(state, NULL, NULL);
}

ADD_COMMAND(db_snap,     /* Name of command */
		NULL,
            db_snap,     /* Main function name */
            parameters,  /* Parameters struct */
            "DB snap command");/* Command description */
