#ifndef TRANS_H
#define TRANS_H

#include <netinet/in.h>
#include <sys/time.h>
#include <libpq-fe.h>

#include "queue.h"
#include "event.h"
#include "util.h"
#include "bconf.h"
#include "linker_set.h"
#include "vtree.h"

struct trans_config {
	char *filename;

	char *port;
	char *failover_port;
	char *control_port;
	char *control_failover_port;
	char *bconfd;
	int max_connections;
	int connection_warning_perc;
	int max_command_threads;
	int debug_dump_delay;
	int error;

	char *basedir;

	int  logoption;
	char *loglevel;
	char *logtag;

	/* Backwards compat. Do not use. */
	char pg_master;
	char pg_master_buf[sizeof("gsql.master")];
	char pg_slave;
	char pg_slave_buf[sizeof("gsql.slave")];
} __attribute__((packed)) config;

extern struct bconf_node *host_bconf;
extern struct bconf_node *trans_bconf;
extern struct bconf_node *filter_state_global;

extern char *trans_user;

extern const unsigned char *pcre_table;

/*
 * No transaction may take more than two minutes.
 */
#define TRANSACTION_TIMEOUT 120
#define TRANSACTION_BANNER_OK "220 Welcome."
#define TRANSACTION_BANNER_GO_AWAY "521 Busy."

/*
 * Maximal size of the data blob.
 */
#define MAX_BLOB (7 * 1024 * 1024)

/*
 * Default values for missing config entries
 */
#define DFLT_MAX_CONNECTIONS		200
#define DFLT_CONN_WARNING_PERC		80
#define DFLT_MAX_COMMAND_THREADS	50
#define DFLT_DEBUG_DUMP_DELAY		10

struct elem {
	TAILQ_ENTRY(elem) el_list;
	char *el_key;
	char *el_value;
	size_t el_size;
};

struct transaction {
	struct bufferevent *ts_buf;
	struct internal_cmd *internal;
	struct event ts_timeout;
	int ts_fd;

	TAILQ_HEAD(,elem) ts_elems;
	int is_control;
	TAILQ_ENTRY(transaction) ts_list; 	/* Keep all transactions in a queue.*/
	char *ts_blob;
	char *log_string;
	char *peer;
	int log;
	size_t ts_blob_size;
	size_t ts_blob_index;

	time_t start_time;

	char *newline;
	int newline_len;

	enum {
		TS_BANNER,		/* writing banner */
		TS_NEWLINE,		/* newline check */
		TS_HEAD,		/* reading header */
		TS_BLOB,		/* reading blob */
		TS_READDONE,		/* reading done */
		TS_HANDLING,		/* being handled */
		TS_DONE			/* everything done */
	} ts_state;
};

struct internal_cmd {
	struct event_base *base;
	char *cmd_string;
	struct evbuffer *inbuf;
	struct evbuffer *outbuf;
	struct evbuffer *blobbuf;
	TAILQ_HEAD(,elem) elems;
	void (*cb)(struct internal_cmd *cmd, const char *line, void *data);
	void *data;
	pthread_t thread;
	char *log_string;
	size_t blob_size;
};

void transaction_abort(struct transaction *, const char *);

int transaction_write(struct transaction *, const void *, size_t);
int transaction_printf(struct transaction *, const char *, ...) __attribute__ ((format (printf, 2, 3)));
int transaction_status(struct transaction *, int status);
void transaction_done(struct transaction *);

#define TRANSACTION_INTERNAL_SYNC_CB ((void (*)(struct internal_cmd*, const char*, void *))-1)
char *transaction_internal_output(struct internal_cmd *cmd);
int transaction_internal(struct internal_cmd *cmd);
int transaction_internal_base_printf(struct event_base *base, void (*cb)(struct internal_cmd *cmd, const char *line, void *data), void *cbdata, const char *fmt, ...) __attribute__ ((format (printf, 4, 5)));
int transaction_internal_printf(void (*cb)(struct internal_cmd *cmd, const char *line, void *data), void *cbdata, const char *fmt, ...) __attribute__ ((format (printf, 3, 4)));
char *transaction_internal_output_printf(const char *fmt, ...) __attribute__((format (printf, 1, 2)));

void transaction_transinfo(struct transaction *ts);
void transaction_transinfo_log(struct transaction *ts);


int transaction_logprintf(struct transaction *, enum debug_level level, const char *, ...) __attribute__ ((format (printf, 3, 4)));
void transaction_logon(struct transaction *);
void transaction_logoff(struct transaction *);

void trans_pending_wait(struct transaction *, int suspend_workers);

/*
 * Reloading.
 */
void reload_register(void (*)(void *), void *);
void post_reload_register(void (*)(void *), void *);
void call_reload_handlers();
void call_post_reload_handlers();

void backend_transinfo(struct transaction *ts);

/* Backward compat bpapi support */
#define hash_output_init stdout_output_init
#define buf_simplevars_init hash_simplevars_init
#define pgsql_output_init buf_output_init
#define BPAPI_INIT(api, null, type)  BPAPI_INIT_APP(api, host_bconf, null, type, type)

/* List of backends */
struct backend {
	const char *name;
	int (*config)(struct bconf_node *conf);
	void (*init)(char *check_only);
	void (*init_done)(void);
	void (*deinit)(void);
	void (*transinfo)(struct transaction *ts);
};

#define ADD_BACKEND(name, configfunc, initfunc, init_done_func, deinit_func, tinfo)				\
	static struct backend b##name = {#name, configfunc, initfunc, init_done_func, deinit_func, tinfo};	\
	LINKER_SET_ADD_DATA(backends, b##name)

struct trans_redis
{
	const char *bconf_path;
	struct bpapi_vtree_chain vtree;
	struct fd_pool *pool;
};

void trans_redis_init(struct trans_redis *dst, const char *bconf_path);

#endif
