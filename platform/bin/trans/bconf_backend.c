
#include <string.h> 
#include <stdlib.h>
#include <curl/curl.h>
#include <pthread.h>

#include "trans.h"
#include "bpapi.h"
#include "ctemplates.h"
#include "json_vtree.h"

#include "bconfd_client.h"

const char *blocket_id;
int blocket_id_len;

struct bconf_node *bconf_root;
struct bconf_node *host_bconf;
struct bconf_node *trans_bconf;

struct bconf_node *filter_state_global;		/* The backing bconf for the global filter_state. */

extern int init_status;

struct bconf_transinfo {
	const char checksum[CHECKSUM_HEADER_LENGTH];
	pthread_mutex_t lock;
	time_t loaded_at;
};

static struct bconf_transinfo b_info;

static int
bconf_config(struct bconf_node *conf) {
	blocket_id = bconf_get_string(conf, "blocket_id");
	if (!blocket_id) {
		xwarnx("No blocket_id in config");
		return 1;
	}
	DPRINTF(D_DEBUG, ("[bconf_backend] blocket_id = %s", blocket_id));
	blocket_id_len = strlen(blocket_id);

	return 0;
}

static void
load_config(void) {
	struct bconfd_call call = { 0 };

	if (bconfd_load_with_file(&call, config.bconfd))
		xerrx(1, "unable to get bconf from bconfd [%s] [%s]", config.bconfd, call.errstr);

	pthread_mutex_lock(&b_info.lock);
	snprintf((char *)b_info.checksum, sizeof(b_info.checksum), "%s", call.checksum);
	b_info.loaded_at = time(NULL);
	pthread_mutex_unlock(&b_info.lock);

	bconf_root = call.bconf;
	host_bconf = NULL;
	trans_bconf = NULL;
	bconf_merge(&host_bconf, bconf_get(bconf_root, "*"));
	bconf_merge(&host_bconf, bconf_get(bconf_root, blocket_id));
	bconf_merge(&trans_bconf, bconf_get(host_bconf, "*"));
	bconf_merge(&trans_bconf, bconf_get(host_bconf, PROG));
}

static void
setup_trees(void) {
	load_config();

	filter_state_bconf_init(&filter_state_global, FS_GLOBAL);
}

static void
teardown_trees(void) {
	filter_state_bconf_free(&filter_state_global);

	bconf_free(&bconf_root);
	bconf_free(&host_bconf);
	bconf_free(&trans_bconf);
}

static void
reload(void *data) {
	teardown_trees();
	setup_trees();

	flush_template_cache(NULL);
}

static void
bconf_transinfo(struct transaction *ts) {
	pthread_mutex_lock(&b_info.lock);
	transaction_printf(ts, "bconf.checksum:%s\n", b_info.checksum);
	transaction_printf(ts, "bconf.loaded_at:%lu (elapsed: %lds)\n",
			b_info.loaded_at,
			time(NULL) - b_info.loaded_at
	);
	pthread_mutex_unlock(&b_info.lock);
}

static void
bconf_init(char *check_only) {
	if (check_only && !strstrptrs(check_only, "bconf", NULL, ","))
		return;

	reload_register(reload, NULL);
	
	pthread_mutex_init(&b_info.lock, NULL);

	setup_trees();
}

static void
bconf_deinit(void) {
	teardown_trees();

	pthread_mutex_destroy(&b_info.lock);
	DPRINTF(D_DEBUG, ("[bconf_backend] Deinitialized"));
}

ADD_BACKEND(bconf, bconf_config, bconf_init, NULL, bconf_deinit, bconf_transinfo);

