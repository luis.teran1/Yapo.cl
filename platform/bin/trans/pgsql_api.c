#include <libpq-fe.h>
#include <stdlib.h>
#include <string.h>
#include <poll.h>
#include "sql_worker.h"
#include "util.h"

#include "pgsql_api.h"

static const char *
send_query(struct sql_worker *sw) {
	struct sql_request *sr = sw->sw_req;

	if (PQsendQuery((PGconn*)(sw->sw_conn), sr->sr_query) != 1) {
		if (PQstatus((PGconn*)sw->sw_conn) != CONNECTION_OK) {
			sw->sw_state = ERROR;
		}
		return PQerrorMessage((PGconn*)sw->sw_conn);
	}

	return NULL;
}

static char *
read_notify(struct sql_worker *sw) {
	PGnotify *notify;
	char *msg;

	notify = PQnotifies((PGconn*)sw->sw_conn);
	if (notify) {
		msg = xstrdup(notify->relname);
		PQfreemem(notify);
		return msg;
	}
	return NULL;
}


static int 
next_row(struct sql_worker *sw) {
	if (!PQntuples((PGresult*)sw->_res) || 
	    (*(int*)(sw->_row) == PQntuples((PGresult*)sw->_res) - 1)) {
		return 0;
	} else {
		(*((int*)(sw->_row)))++;
		return 1;
	}
}

static const char* 
get_value(struct sql_worker *sw, int column) {
	return PQgetvalue((PGresult*)sw->_res, *(int*)(sw->_row), column);
}

static const char* 
get_value_byname(struct sql_worker *sw, const char* name) {
	int column = PQfnumber((const PGresult*)sw->_res, name);

	if (column >= 0)
		return get_value(sw, column);
	else
		return NULL;
}

static int
affected_rows(struct sql_worker *sw) {
	return atoi(PQcmdTuples((PGresult*)sw->_res));
}

static int
fields(struct sql_worker *sw) {
	return PQnfields((PGresult*)sw->_res);
}

static const char* 
field_name(struct sql_worker *sw, int column) {
	return PQfname((PGresult*)sw->_res, column);
}

static int
is_null(struct sql_worker *sw, int column) {
	return PQgetisnull((PGresult*)sw->_res, *(int*)(sw->_row), column);
}

static int
is_true(struct sql_worker *sw, int column) {
	const char *value = get_value(sw, column);

	if (value && strcmp(value, "t") == 0)
		return 1;

	return 0;
}

static int
is_array(struct sql_worker *sw, int column) {
	int type = PQftype((PGresult*)sw->_res, column);
	switch (type) {
	case 1007: /* INT4ARRAYOID */
	case 1021: /* FLOAT4ARRAYOID */
	case 1263: /* CSTRINGARRAYOID */
	case 2211: /* REGTYPEARRAYOID */
	case 2277: /* ANYARRAYOID */
		return 1;
	}
	return 0;
}

static int
is_null_byname(struct sql_worker *sw, const char* name) {
	int column = PQfnumber((const PGresult*)sw->_res, name);

	if (column >= 0)
		return is_null(sw, column);
	else
		return 1;
}

static int
is_true_byname(struct sql_worker *sw, const char *name) {
	const char *value = get_value_byname(sw, name);

	if (value && strcmp(value, "t") == 0)
		return 1;

	return 0;
}
static int 
rows(struct sql_worker *sw) {
	return PQntuples((PGresult*)sw->_res);
}

static const char* 
store_res(struct sql_worker *sw) {
	(*(int*)(sw->_row)) = -1;

	if (sw->_res) {
		DPRINTF(D_INFO, ("(CRIT) (%s) Running PQgetResult with non-NULL result in worker. Resource leak likely.", sw->sw_req->logstr));
	}

	sw->_res = PQgetResult((PGconn*)sw->sw_conn);

	if (sw->_res == NULL) {
		sw->sw_state = RESULT_DONE;
		return NULL;
	}

	if (PQresultStatus(sw->_res) == PGRES_COMMAND_OK ||
	    PQresultStatus(sw->_res) == PGRES_TUPLES_OK) {
		sw->sw_state = RESULT_STORED;
		return NULL;
	}

	sw->sw_state = RESULT_ERROR;
	return PQresultErrorMessage((PGresult*)sw->_res);
}

static int
more_results(struct sql_worker *sw) {
	/* We don't know if there are any more results, so assume there are and let store_res() signal RESULT_DONE */
	return 1;
}

static void 
free_res(struct sql_worker *sw) {
	sw->sw_state = RESULT_FREED;

	if (sw->_res) {
		PQclear((PGresult*)sw->_res);
		sw->_res = NULL;
	}
}

static void 
notice_proc(void *arg, const char *message) {
	struct sql_worker *sw = arg;
	struct sql_request *sr = sw->sw_req;
	struct notice *note = xmalloc(sizeof(*note) + strlen(message) + 1);
	note->message = (char*)(note + 1);
	strcpy(note->message, message);

	DPRINTF(D_INFO, ("(%s) NOTICE: %s", sr->logstr, note->message));
	TAILQ_INSERT_TAIL(&(sw->sw_notices), note, tq);
}

static const char*
sql_connect(struct sql_worker *sw) {
	char *conninfo;
	char *port = NULL;

	if (sw->sw_sc->sc_port)
		xasprintf(&port, "port = %u", sw->sw_sc->sc_port);

	xasprintf(&conninfo, "host = '%s' %s user = '%s' password = '%s' dbname = '%s' options = '%s'",
	          (sw->sw_sc->sc_socket ? sw->sw_sc->sc_socket : sw->sw_sc->sc_host),
		  port ? port : "",
		  sw->sw_sc->sc_user,
		  sw->sw_sc->sc_password,
		  sw->sw_sc->sc_flags & SQLC_CONTROL ? "postgres" : sw->sw_sc->sc_name,
		  (sw->sw_sc->sc_flags & SQLC_CONTROL) == 0 && sw->sw_sc->sc_options ?sw->sw_sc->sc_options: "");

	if (sw->sw_sc->sc_port)
		free(port);

	sw->sw_conn = PQconnectdb(conninfo);

	free(conninfo);

	if (sw->sw_conn == NULL) {
		sw->sw_state = ERROR;
		return "PQconnectdb: malloc failed.";
	} else if (PQstatus(sw->sw_conn) == CONNECTION_OK) {
		sw->sw_state = IDLE;
	} else {
		sw->sw_errstr = xstrdup (PQerrorMessage((PGconn*)sw->sw_conn));
		PQfinish((PGconn*)sw->sw_conn);
		sw->sw_state = ERROR;
		sw->sw_conn = NULL;
		return sw->sw_errstr;
	}

	/* Setup the notice processor */
	PQsetNoticeProcessor((PGconn*)sw->sw_conn, notice_proc, sw);
	return NULL;
}

static const char*
verify_connection(struct sql_worker *sw) {
	if (PQstatus((PGconn*)sw->sw_conn) != CONNECTION_BAD) {
		struct pollfd pfd = {0};
		int n;

		pfd.fd = sw->get_fd(sw);
		pfd.events = POLLHUP | POLLRDHUP;

		n = poll(&pfd, 1, 0);

		if (n == 0) {
			DPRINTF(D_DEBUG, ("verify_connection(%p): using existing fd", sw));
			return NULL;
		}

		if (n > 0)
			DPRINTF(D_DEBUG, ("verify_connection(%p): NOT using existing fd: EOF", sw));
		else
			DPRINTF(D_DEBUG, ("verify_connection(%p): NOT using existing fd: %m", sw));
	}

	PQreset((PGconn*)sw->sw_conn);

	if (PQstatus(sw->sw_conn) != CONNECTION_OK) {
		sw->sw_errstr = xstrdup (PQerrorMessage((PGconn*)sw->sw_conn));
		PQfinish((PGconn*)sw->sw_conn);
		sw->sw_state = ERROR;
		sw->sw_conn = NULL;
		return sw->sw_errstr;
	}
	return NULL;
}

static void
connection_identifier (struct sql_worker *sw, char *buf, size_t buflen) {
	snprintf (buf, buflen, "%d", PQbackendPID((PGconn*)sw->sw_conn));
}

static const char*
sql_errstr(struct sql_worker *sw) {
	return PQresultErrorMessage((PGresult*)sw->_res);
}

static int
sql_errno(struct sql_worker *sw) {
	return 0; /* XXX */
}

static void
sql_close(struct sql_worker *sw) {
	if (sw->sw_conn) {
		PQfinish((PGconn*)sw->sw_conn);
		sw->sw_conn = NULL;
	}
}

static int
get_fd(struct sql_worker *sw) {
	return PQsocket((PGconn*)sw->sw_conn);
}

static int
insert_id(struct sql_worker *sw) {
	return -1;
}

static void
free_worker(struct sql_worker *sw) {
	free(sw->_row);
	free(sw->sw_errstr);
	sw->_row = NULL;
	sw->sw_errstr = NULL;
}

void
pgsql_init_worker(struct sql_worker *sw) {
	sw->_row = xmalloc(sizeof(int));
	sw->next_row = next_row;
	sw->get_value = get_value;
	sw->get_value_byname = get_value_byname;
	sw->affected_rows = affected_rows;
	sw->fields = fields;
	sw->field_name = field_name;
	sw->is_null = is_null;
	sw->is_array = is_array;
	sw->is_null_byname = is_null_byname;
	sw->rows = rows;
	sw->send_query = send_query;
	sw->store_res = store_res;
	sw->free_res = free_res;
	sw->connect = sql_connect;
	sw->verify_connection = verify_connection;
	sw->connection_identifier = connection_identifier;
	sw->close = sql_close;
	sw->sql_errstr = sql_errstr;
	sw->sql_errno = sql_errno;
	sw->get_fd = get_fd;
	sw->insert_id = insert_id;
	sw->more_results = more_results;
	sw->read_notify = read_notify;
	sw->free_worker = free_worker;
	sw->is_true = is_true;
	sw->is_true_byname = is_true_byname;
}

int
pgsql_split_array (char *text, int len, char ***elems) {
	char *end;
	int nelems = 0;
	int max_elems;
	char *cp;
	enum {
		st_start,
		st_middle,
		st_end,
		st_quoted
	} state = st_start;

	if (len < 0)
		len = strlen(text);

	if (!len) {
		*elems = NULL;
		return 0;
	}

	if (text[0] != '{' || text[len - 1] != '}')
		return -1;

	max_elems = 0;
	end = text + len - 1;
	for (cp = text; cp; cp = memchr (cp + 1, ',', end - cp))
		max_elems++;

	*elems = xmalloc (max_elems * sizeof (**elems));

	for (text++ ; text < end ; ) {
		switch (state) {
		case st_start:
			if (*text == '"') {
				state = st_quoted;
				text++;
			} else
				state = st_middle;
			if (nelems >= max_elems) {
				free(*elems);
				return -1; /* XXX do something better here. */
			}
			(*elems)[nelems++] = text;
			break;
		case st_middle:
			if (*text == ',') {
				*text = '\0';
				state = st_start;
			} else if (*text == '\\')
				memmove(text, text + 1, --end - text);
			text++;
			break;
		case st_end:
			if (*text++ != ',') {
				free(*elems);
				return -1;
			}
			state = st_start;
			break;
		case st_quoted:
			if (*text == '"') {
				*text = '\0';
				state = st_end;
			} else if (*text == '\\')
				memmove(text, text + 1, --end - text);
			text++;
			break;
		}
	}
	*end = '\0';
	return nelems;
}

