
#include "trans.h"
#include "garb.h"
#include "bconf.h"
#include "hash.h"
#include "sql_worker.h"
#include "util.h"
#include "bpapi.h"
#include "ctemplates.h"
#include "logging.h"

#include <stdlib.h>
#include <pthread.h>

struct hash_table *counts;
pthread_mutex_t garb_table_lock = PTHREAD_MUTEX_INITIALIZER;

static void *
count_alloc(const void *key, int klen, void **data, void *v) {
	*data = zmalloc(sizeof (int));
	return xstrdup(key);
}

void
garb_table(const char *name) {
	int *cnt;
	struct bconf_node *garb_node = bconf_vget(trans_bconf, "garb", name, NULL);
	int interval = bconf_get_int(garb_node, "interval");

	if (!interval) {
		DPRINTF(D_DEBUG, ("Garbing disabled for table %s", name));
		return;
	}

	pthread_mutex_lock(&garb_table_lock);
	if (!counts) {
		counts = hash_table_create(8, free);
		hash_table_free_keys(counts, 1);
	}

	cnt = hash_table_update(counts, name, -1, count_alloc, NULL);

	if (++*cnt >= interval) {
		const char *cond = bconf_get_string(garb_node, "condition");
		struct bpapi ba;
		const char *errstr = NULL;
		struct sql_worker *worker;

		*cnt = 0;
		pthread_mutex_unlock(&garb_table_lock);

		if (!cond) {
			log_printf(LOG_ERR, "garb_table(%s): No condition", name);
			return;
		}

		DPRINTF(D_INFO, ("Garbing table %s (interval %d)", name, interval));

		BPAPI_INIT(&ba, NULL, pgsql);
		bpapi_insert(&ba, "table", name);
		bpapi_insert(&ba, "condition", cond);
		worker = sql_blocked_template_query(bconf_get_string(garb_node, "db_node") ?: "pgsql.master", 0, "sql/garb_table.sql", &ba,
				"GARB", &errstr);
		bpapi_output_free(&ba);
		bpapi_simplevars_free(&ba);
		bpapi_vtree_free(&ba);

		if (worker)
			sql_blocked_next_result(worker, &errstr);

		if (errstr)
			log_printf(LOG_CRIT, "Garbing table %s error: %s", name, errstr);
		else
			DPRINTF(D_INFO, ("Garbing table %s deleted %d rows", name, worker ? worker->affected_rows(worker) : -1));

		if (worker)
			sql_worker_put(worker);
	} else {
		pthread_mutex_unlock(&garb_table_lock);
		DPRINTF(D_DEBUG, ("Not garbing table %s yet (interval %d, counter %d)", name, interval, *cnt));
	}
}

void
cleanup_garb(void) {
	pthread_mutex_lock(&garb_table_lock);
	if (counts) {
		hash_table_free(counts);
		counts = NULL;
	}
	pthread_mutex_unlock(&garb_table_lock);
}
