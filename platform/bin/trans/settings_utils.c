#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include <ctype.h>
#include <time.h>
#include <stdlib.h> 

#include "settings_utils.h"
#include "settings.h"
#include "util.h"
#include "hash.h"
#include <bconf.h>
#include "vtree.h"
#include "trans.h"

/*
 *  Utility functions for working with both bconf and settings.
 */

struct has_feature_data {
	struct cmd_param *param;
	const char *feature;
	int has_feature;
	char *value;
};

static void
has_new_feature_cb(const char *setting, const char *key, const char *value, void *cbdata) {
	struct has_feature_data *hfd = cbdata;

	if (key) {
		hfd->has_feature = 1;
		hfd->value = xstrdup(key);
	}
}

static const char *
has_feature_key_lookup(const char *setting, const char *key, void *cbdata) {
	struct has_feature_data *hfd = cbdata;

	if (!strcmp(key, "parent")) {
		return bconf_value(bconf_vget(trans_bconf, "cat", cmd_getval(hfd->param->cs, "category"), "parent", NULL));
	}

	return cmd_getval(hfd->param->cs, key);
}

static int get_feature_value_internal(struct cmd_param *param, const char *feature, char **value);

int
has_feature(struct cmd_param *param, const char *feature) {
	return get_feature_value_internal(param, feature, NULL);
}

int
get_feature_value(struct cmd_param *param, const char *feature, char **value) {
	return get_feature_value_internal(param, feature, value);
}

/**
*/
static int
get_feature_value_internal(struct cmd_param *param, const char *feature, char **value) {
	struct has_feature_data hfd = { param, feature, 0, NULL };
	struct bpapi_vtree_chain vtree;

	get_settings(bconf_vtree(&vtree, bconf_get(trans_bconf, "category_settings")), feature, has_feature_key_lookup, has_new_feature_cb, &hfd);
	vtree_free(&vtree);

	if (value && hfd.has_feature && hfd.value) {
		*value = hfd.value;
	} else if (hfd.value) {
		free(hfd.value);
	}

	return hfd.has_feature;
}


/*
 * Extract a key-val pair from bconf, ex *..*.value=max:5,price:40
 * with kv->key set to max, kv->value will be 5
 */
struct keyval_pair {
	struct cmd *cs;
	const char *key;
	int value;
	char *strval;
};

static void
settings_keyval_set(const char *setting, const char *key, const char *value, void *cbdata) {
	struct keyval_pair *kv= cbdata;
	if (strcmp(key, kv->key) == 0) {
		kv->value = atoi(value);
	}
}

static const char*
settings_keyval_key_lookup(const char *setting, const char *key, void *cbdata) {
	struct keyval_pair *kv = cbdata;

	if (strcmp(key, "parent") == 0) {
		return bconf_value(bconf_vget(trans_bconf, "cat", cmd_getval(kv->cs, "category"), "parent", NULL));
	}
	
	return cmd_getval(kv->cs, key);
}

int
get_setting_keyval_value(struct cmd *cs, const char* setting_namespace, const char *setting, const char *key) {
	struct keyval_pair kv = {cs, key, 0, NULL};
	struct bpapi_vtree_chain vtree;

	get_settings(bconf_vtree(&vtree, bconf_vget(trans_bconf, setting_namespace ? setting_namespace : "category_settings", NULL)),
		     setting,
		     settings_keyval_key_lookup, 
		     settings_keyval_set, &kv);
	vtree_free(&vtree);
	return kv.value;
}

static void
settings_keyval_set_string(const char *setting, const char *key, const char *value, void *cbdata) {
	struct keyval_pair *kv = cbdata;

	if (strcmp(key, kv->key) == 0) {
		if (kv->strval)
			free(kv->strval);
		kv->strval = xstrdup(value);
	}
}

char *
get_setting_keyval_string(struct cmd *cs, const char* setting_namespace, const char *setting, const char *key) {
	struct keyval_pair kv = {cs, key, 0, NULL};
	struct bpapi_vtree_chain vtree;

	get_settings(bconf_vtree(&vtree, bconf_vget(trans_bconf, setting_namespace ? setting_namespace : "category_settings", NULL)),
		     setting,
		     settings_keyval_key_lookup, 
		     settings_keyval_set_string, &kv);
	vtree_free(&vtree);
	return kv.strval;
}

