#ifndef SETTINGS_UTILS_H
#define SETTINGS_UTILS_H

#include "command.h"
#include "settings.h"

/**
  Returns true if the feature flag is set, else false.
*/
int has_feature(struct cmd_param *param, const char *feature);

/**
  Returns true if the feature flag is set, and returns the value in the value argument.
  The caller is tasked with freeing this memory.
*/
int get_feature_value(struct cmd_param *param, const char *feature, char **value);

/**
 * Utility function for the default case of getting a value of a key:val paired
 * setting in the specified setting namespace, defaulting to 'category_settings' if NULL.
 *
 * Example
 *
 *  We have
 *    *.*.category_settings.extra_images.2.1160.value=max:3,price:20
 *  Retrieve the max entry:
 *    int max_extra_images =  get_setting_keyval_value(cs, NULL, "extra_images", "max");
 *
 */
int get_setting_keyval_value(struct cmd *cs, const char* setting_namespace, const char *setting, const char *key);

char *get_setting_keyval_string(struct cmd *cs, const char* setting_namespace, const char *setting, const char *key);


#endif

