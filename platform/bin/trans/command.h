#ifndef COMMAND_H
#define COMMAND_H

#include <queue.h>
#include <linker_set.h>
#include <pthread.h>
#include <pcre.h>

#include "trans_timers.h"
#include "sql_worker.h"
#include "bpapi.h"

#define P_REQUIRED 0x0001
#define P_OPTIONAL 0x0002
#define P_DYN_STATIC_VALIDATOR 0x0004
#define P_MULTI    0x0008
#define P_AND      0x0010
#define P_OR       0x0020
#define P_XOR      0x0040
#define P_EQ       0x0080
#define P_EMPTY    0x0100
#define P_ARG_ERR  0x0200
#define P_TEXT     0x0400
#define P_NAND     0x0800
#define P_IMPLY    0x1000

/* Flags for transaction_bind_params_bpa() */
#define BIND_ALLOW_MULTI 0x001

#define CMDP_NORMALIZED (1 << 0)
#define CMDP_VALIDATED  (1 << 1)

#define CMD_NORMAL_PORT  0x1
#define CMD_CONTROL_PORT 0x2

enum validator_type {
	V_REQ = 1,
	V_REGEX,
	V_FUNC,
	V_LEN,
	V_INT,
	V_INT32,
	V_INT64,
	V_DOUBLE,
	V_VAL,
	V_BCONF_KEY,
	V_BCONF_LIST,
	V_GPERF_ENUM,
	V_OTHER
};

#define REQ_EXECUTION   0x01
#define REQ_VALIDATION  0x02
#define REQ_ALL         REQ_EXECUTION | REQ_VALIDATION
#define FUNC_ALWAYS     0x10
#define STR_ALLOCED	0x20 /* Set if the error and regexp fields of the validator structure must be free'd in cmd_done() */
#define VFLAG_COMMALIST 0x40
#define VFLAG_DOTPATH  0x80
#define VFLAG_BCONF_ISKEY  0x100

/* Backward compat regex flags => pcre flags */
#define REG_EXTENDED 0
#define REG_ICASE PCRE_CASELESS
#define REG_NEWLINE (PCRE_DOLLAR_ENDONLY | PCRE_DOTALL) /* This is always on. */
#define REG_NOSUB PCRE_NO_AUTO_CAPTURE

#define VALIDATOR_REQUIRED(err)					\
	{V_REQ, 0, REQ_ALL, { NULL }, err, NULL, 0}

#define VALIDATOR_REQUIRED_EXECUTION(err)					\
	{V_REQ, 0, REQ_EXECUTION, { NULL }, err, NULL, 0}

#define VALIDATOR_REQUIRED_VALIDATION(err)					\
	{V_REQ, 0, REQ_VALIDATION, { NULL }, err, NULL, 0}

#define VALIDATOR_REGEX(re, flags, err)					\
	{V_REGEX, 0, 0, { .regex = { re, flags } }, err, NULL, 0}

#define VALIDATOR_NREGEX(re, flags, err)					\
	{V_REGEX, 1, 0, { .regex = { re, flags } }, err, NULL, 0}

#define VALIDATOR_FUNC(func)						\
	{V_FUNC, 0, 0,  { .v_func = func }, NULL, NULL, 0}

#define VALIDATOR_FUNC_ALWAYS(func)						\
	{V_FUNC, 0, FUNC_ALWAYS, { .v_func = func }, NULL, NULL, 0}

#define VALIDATOR_LEN(min, max, err)						\
	{V_LEN, 0, 0, { .minmax = { min, max } }, err, NULL, 0}

#ifndef NO_VALIDATOR_INT
#define VALIDATOR_INT(min, max, err)						\
	{V_INT, 0, 0, { .minmax = { min, max } }, err, NULL, 0}
#endif

#define VALIDATOR_INT32(min, max, err)						\
	{V_INT32, 0, 0, { .minmax = { min, max } }, err, NULL, 0}

#define VALIDATOR_INT64(min, max, err)						\
	{V_INT64, 0, 0, { .minmax = { min, max } }, err, NULL, 0}

#define VALIDATOR_DOUBLE(min, max, err)						\
	{V_DOUBLE, 0, 0, { .minmax = { min, max } }, err, NULL, 0}

#define VALIDATOR_SUB(name)						\
	{V_VAL, 0, 0, { .sub_validators = name }, "ERROR_OK", NULL, 0}

#define VALIDATOR_BCONF_KEY(node, err)					\
	{V_BCONF_KEY, 0, 0, { .bconf_node = node }, err, NULL, 0}

#define VALIDATOR_BCONF_LIST_KEY(node, err)					\
	{V_BCONF_LIST, 0, VFLAG_BCONF_ISKEY, { .bconf_path = { node, NULL } }, err, NULL, 0}

#define VALIDATOR_BCONF_LIST(node, err)					\
	{V_BCONF_LIST, 0, 0, { .bconf_path = { node, NULL } }, err, NULL, 0}

#define VALIDATOR_BCONF_LIST_DOT_PATH(node, path, err)					\
	{V_BCONF_LIST, 0, VFLAG_DOTPATH, { .bconf_path = { node, path } }, err, NULL, 0}

#define VALIDATOR_BCONF_LIST_DOT_NAME(node, err) VALIDATOR_BCONF_LIST_DOT_PATH(node, "name", err)

#define VALIDATOR_GPERF_ENUM(name, err) \
	{V_GPERF_ENUM, 0, 0, { .gperf_enum_func = lookup_##name##_int }, err, NULL, 0}

#define VALIDATOR_OTHER(name, flags, validator) \
	{V_OTHER, 0, 0, { .other = { name, flags, validator } }, "ERROR_OTHER_PARAM", NULL, 0}

struct c_param {
	const char *name;
	int type;
	const char *validators;
	const char *help;
};

struct d_param {
	char *name;
	int flags;
	struct validator *validators;
};

struct cmd;
struct cmd_param;
struct validator {
	enum validator_type type;
	int invert;
	int v_flags;
	union {
		void *null;
		struct {
			const char *regex;
			int reg_flags;
		} regex;
        	int (*v_func)(struct cmd_param *, const char *arg);
		struct {
			int min;
			int max;
		} minmax;
		const char *sub_validators;
		struct bconf_node *bconf_node;
		struct {
			const char *bconf_path;
			const char *dot_path;
		} bconf_path;
		int (*gperf_enum_func)(const char *str, int len);
		struct c_param other;
	};
	const char *error;
	void *data;
	int initialized;
};

struct _validator {
	const char *name;
	struct validator *v;
};

#define ADD_VALIDATOR(name)					\
	struct _validator v_##name = {#name, name};	\
	LINKER_SET_ADD_DATA(vlds, v_##name)

struct cmd;
struct command {
	const char *name;
	void (*init)(char *check_only);
	void (*reset)(void);
	void (*func)(struct cmd *cs);
	struct c_param *params;        
	const char *help;
	int flags;
	void *aux;
};

void preinit_command(void);
void init_commands(char *check_only);
void cleanup_commands(void);
void reset_commands_data(void);

struct transaction;
void handle_command(const char *command, struct transaction *ts);

struct cmd_param {
	TAILQ_ENTRY(cmd_param) tq;
	char *key;
	char *value;
	char *message;
	size_t value_size;
	const char *error;
	struct cmd *cs;
	int flags;
};

struct command_thread;

struct cmd {
	struct event_base *base;
	pthread_t thread;
	struct command_thread *thread_data;
	const struct c_param *valparam;

	struct transaction *ts;
	int pending_validators;
	int validate_only;
	int validate_phase;        
	const char *status;
        char *message;
        const char *error;
	const struct command *command;
	struct cmd_param base_param;
	TAILQ_HEAD(,cmd_param) params;
	TAILQ_ENTRY(cmd) cmd_list;
	TAILQ_ENTRY(cmd) waiting_list;
	void (*validator_cb)(struct cmd *cs);
#define NEXTRAPARAMS 64 
	struct c_param *extra_params[NEXTRAPARAMS];
	int nextra_params;
#define NDYNPARAMS 128
	struct d_param *dyn_params[NDYNPARAMS];
	int ndyn_params;
	struct timer_instance *ti;
};

int cmd_haskey(struct cmd *, const char *);
struct cmd_param* cmd_getparam(struct cmd *cs, const char *key);
struct cmd_param* cmd_nextparam(struct cmd *cs, struct cmd_param *prev);
const char*  cmd_getval(struct cmd *, const char *);
int cmd_getint_default(struct cmd *cs, const char *key, int default_value);
void cmd_setval(struct cmd *, const char *, char *);
void cmd_delval(struct cmd *, const char *);
size_t  cmd_getsize(struct cmd *, const char *);
int cmd_status(const char *name, const struct cmd *cs, int status);

/* Obs! cs->ts will be NULL after calling cmd_detach */
void cmd_detach(struct cmd *cs);
void cmd_done(struct cmd *cs);

void cmd_extra_param(struct cmd *, struct c_param *);
int cmd_has_extra_param(struct cmd *cs, const char *name);
void cmd_dyn_param(struct cmd *, struct d_param *);


/*
 * For template calls in the transaction handler that want to use filter_state, a little more setup is needed.
 * command_bpapi is the replacement interface for bpapi for this.
 */
struct command_bpapi {
	struct bpapi ba;
	struct filter_state_bconf_glue fs_glue;
	struct bpapi_vtree_chain bconf_vt;
	int flags;
};
#define CMD_BPAPI_HOST			0x01	/* host bconf */
#define CMD_BPAPI_TRANS			0x02	/* trans bconf */
#define CMD_BPAPI_NO_OUTPUT		0x04	/* Don't handle output. */
#define CMD_BPAPI_NO_SIMPLEVARS		0x08	/* Don't handle simplevars. */
#define CMD_BPAPI_SIMPLEVARS_HASH	0x10	/* Use hash simplevars instead of the default pgsql. */
void command_bpapi_init(struct command_bpapi *cba, struct cmd *cmd, int flags, const char *appl);
void command_bpapi_free(struct command_bpapi *cba);

struct validator* load_validator(const char *validator, const char **arg_out, int can_fail);
void transaction_cmdinfo(struct transaction *ts, int printblobs);
void transaction_threadinfo(struct transaction *ts);
void transaction_cmdinfo_log(struct transaction *ts);

void transaction_bind_params_bpapi(struct cmd *cs, struct bpapi *ba, const char *prefix, int flags);

/* */
#define IS_ERROR(code)					\
	((code) && (code)[0] == 'E')

#define IS_WARNING(code)					\
	((code) && (code)[0] == 'W')

/* */
#define ADD_COMMAND(name, func1, func2, params, help)				\
	static struct command c##name = {#name, func1, NULL, func2, params, (help), CMD_NORMAL_PORT};	\
	LINKER_SET_ADD_DATA(cmds, c##name)

#define ADD_COMMAND_FLAGS(name, func1, cleanup_func, func2, params, help, flags) \
	static struct command c##name = {#name, func1, cleanup_func, func2, params, (help), (flags)}; \
	LINKER_SET_ADD_DATA(cmds, c##name)

struct db_snap_action {
	const char *name;
	void (*fun)(struct cmd *cs, const char *action);
};

#define ADD_DB_SNAP_ACTION(name, fun) \
	static struct db_snap_action dbs##name = {#name, fun};	\
	LINKER_SET_ADD_DATA(db_snap_actions, dbs##name);

#endif
