#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "trans.h"
#include "command.h"
#include <ctemplates.h>
#include "util.h"
#include "bconf.h"
#include "hash.h"
#include "sql_worker.h"
#include "atomic.h"

struct bconf_data {
	int refs;
	int rows;
	char **conf_data;
	char *timestamp;
};

struct bconf_cache {
	char **bc;
	int rc;
};

static int bconf_alloc = 0;
struct bconf_data file_data;
struct bconf_node *bconf_root = NULL;
struct hash_table *bconf_cache;
pthread_mutex_t bconf_cache_lock;

static void
bconf_cache_free(void *v) {
	struct bconf_cache *cache = v;
	free(cache->bc);
	free(cache);
}

struct validator v_bconf_keyval[] = {
	VALIDATOR_LEN(0, 8192, "ERROR_STRING_SIZE_INVALID"),
	VALIDATOR_REGEX("(*UCP)^[[:print:][:alpha:]]+$", REG_EXTENDED, "ERROR_STRING_INVALID"),
	{0}
};
ADD_VALIDATOR(v_bconf_keyval);

struct validator v_host[] = {
	VALIDATOR_REGEX("^([0-9a-z._]+)$", REG_EXTENDED, "ERROR_HOST_INVALID"),
	{0}
};
ADD_VALIDATOR(v_host);

struct validator v_appl[] = {
	VALIDATOR_REGEX("^([0-9a-z_]+)$", REG_EXTENDED, "ERROR_APPL_INVALID"),
	{0}
};
ADD_VALIDATOR(v_appl);

static struct c_param parameters[] = {
	{"host", 	0,	  	  "v_host"},
	{"appl", 	0,	  	  "v_appl"},
	{NULL, 0}
};

static void
free_bconf_data (struct bconf_data *data) {
	int i;
	int curr = data->refs;
	int new;

	while ((new = atomic_cas_int(&data->refs, curr, curr - 1)) != curr)
		curr = new;

	if (curr > 1)
		return;

	for (i = 0 ; i < data->rows ; i++) {
		free(data->conf_data[i]);
	}
	free(data->conf_data);
	free(data->timestamp);
	if (data != &file_data)
		free(data);
}

static void
cache_bconf(const char *prefix, struct bconf_node *node) {
	const char *v;

	if (!node)
		return;

	if ((v = bconf_value(node))) {
		if (!bconf_alloc) {
			bconf_alloc = 4096;
			file_data.conf_data = xmalloc(bconf_alloc * sizeof (char*));
		}

		xasprintf(&file_data.conf_data[file_data.rows++], "%s%s=%s", prefix, bconf_key(node), v);

		if (file_data.rows == bconf_alloc) {
			bconf_alloc *= 2;
			file_data.conf_data = xrealloc(file_data.conf_data, bconf_alloc * sizeof(char*));
		}
	} else {
		char *newpref;
		int res;
		int i;
		int c = bconf_count(node);
		const char *k = bconf_key(node);

		if (k)
			ALLOCA_PRINTF(res, newpref, "%s%s.", prefix, k);
		else
			newpref = (char*)prefix;
		for (i = 0 ; i < c ; i++)
			cache_bconf(newpref, bconf_byindex(node, i));
	}
}

static void
reload(void *data) {
	int cnt;

	/* Clear bconf-API and cache */
	for (cnt = 0 ; cnt < file_data.rows ; cnt++) {
		free(file_data.conf_data[cnt]);
	}
	file_data.rows = 0;

	if (bconf_cache) {
		hash_table_free(bconf_cache);
		bconf_cache = NULL;
	}

	cache_bconf("", bconf_root);
}

static void
init(char *check_only) {
	if (check_only)
		return;

	reload_register(reload, NULL);

	cache_bconf("", bconf_root);
}

static int
bc_reduce(char **bc, int rows, const char *host, const char *appl) {
	int bc_cnt = 0;
	int cnt;
	char *rex;
	pcre *re;
	const char *err;
	int erro;

	xasprintf(&rex, "^(\\*|%s)\\.(\\*|%s)\\.", host, appl);

	if (!(re = pcre_compile(rex, REG_NOSUB | REG_EXTENDED | REG_ICASE, &err, &erro, pcre_table))) {
		free(rex);
		return 0;
	}
	free(rex);

	for (cnt = 0 ; cnt < rows ; cnt++) {
		if (pcre_exec(re, NULL, bc[cnt], strlen(bc[cnt]), 0, 0, NULL, 0) == 0) {
			char *pos = bc[cnt];
			bc[bc_cnt++] = pos;
		}
	}

	pcre_free(re);

	return bc_cnt;
}

static int
key_comp(char *k1, char *k2, int nskip) {
	char *s1 = strchr(k1, '=');
	char *s2 = strchr(k2, '=');

	/* Skip the first two parts. */
	while (nskip--) {
		k1 = strchr(k1, '.') + 1;
		k2 = strchr(k2, '.') + 1;
	}

	if (s1 - k1 != s2 - k2)
		return 0;

	return (strncmp(k1, k2, s1 - k1) == 0);
}

/*
 * Compare keys, part by part. First by length in descending order,
 * then in lex desc order. Last by number of parts in ascending order.
 * Finish off by sorting on the more specific key based on the host and
 * app.
 */


static int
bc_comp(const void *p1, const void *p2, int skip) {
	const char *k1 = *(const char **)p1;
	const char *k2 = *(const char **)p2;
	const char *d1 = NULL;
	const char *d2 = NULL;
	int s1 = 0;
	int s2 = 0;
	int res;
	int nskip = skip;

	if (p1 == p2)
		return 0;

	/* Skip the first n parts. */

	while (nskip--) {
		k1 = strchr(k1, '.') + 1;
		k2 = strchr(k2, '.') + 1;
	}
	
	while ((d1 = strpbrk(k1, ".=")) , (d2 = strpbrk(k2, ".=")) , (d1 && d2)) {
		if (d1 - k1 == d2 - k2) {
			if ((res = strncmp(k1, k2, d1 - k1)) == 0) {
				if (*d1 == '=')
					d1 = NULL;
				if (*d2 == '=')
					d2 = NULL;
				if (!d1 || !d2)
					break;

				k1 = d1 + 1;
				k2 = d2 + 1;
				continue;
			} else {
				return res;
			}
		}

		if (d1 - k1 > d2 - k2) {
			return 1;
		}

		if (d1 - k1 < d2 - k2) {
			return -1;
		}

		if (*d1 == '=')
			d1 = NULL;
		if (*d2 == '=')
			d2 = NULL;
		if (!d1 || !d2)
			break;

		k1 = d1 + 1;
		k2 = d2 + 1;
	}

	if (d1)
		return -1;
	if (d2)
		return 1;

	/* Count the number of '*' in the first n parts */
	k1 = *(const char **)p1;
	k2 = *(const char **)p2;

	nskip = skip;
	while (nskip) {
		/* Give set host higher prio than set appl, ie earlier stars have lower prio. */
		if (*k1 == '*') s1 += nskip;
		if (*k2 == '*') s2 += nskip;
		nskip--;

		k1 = strchr(k1, '.') + 1;
		k2 = strchr(k2, '.') + 1;
	}

	return s1 - s2;
}

static int
bc_comp_0(const void *a, const void *b) {
	return bc_comp(a, b, 0);
}

static int
bc_comp_1(const void *a, const void *b) {
	return bc_comp(a, b, 1);
}

static int
bc_comp_2(const void *a, const void *b) {
	return bc_comp(a, b, 2);
}

static int (*const bc_comps[])(const void *a, const void *b) = {
	bc_comp_0,
	bc_comp_1,
	bc_comp_2
};

static const char*
skip_fields(const char *string, int nskips) {
	const char *tmp = string;
	if (!nskips)
		return string;

	while (nskips--) {
		tmp = strchr(tmp, '.');
		if (!tmp)
			return string;
		tmp++;
	}

	return tmp;
}

static void
bconf(struct cmd *cs) {
	struct bconf_cache *cache;
	const char *host = cmd_getval(cs, "host");
	const char *appl = cmd_getval(cs, "appl");
	char **bc;
	char cache_key[256];
	int bcc = 0;
	int can_cache = 1;
	int cnt;
	int rc;
	int skip = 0;

	if (host) {
		skip++;
		if (appl)
			skip++;
	}

	snprintf (cache_key, sizeof(cache_key), "%s %s", host, appl);

	pthread_mutex_lock(&bconf_cache_lock);
	if (can_cache && bconf_cache && (cache = hash_table_search(bconf_cache, cache_key, -1, NULL))) {
		rc = cache->rc;
		bc = cache->bc;
		transaction_logprintf(cs->ts, D_DEBUG, "found cache %p with %d entries for %s", cache, rc, cache_key);
	} else {
		bcc = file_data.rows;

		bc = xmalloc((bcc * sizeof (char*)));

		if (bcc)
			memcpy(bc, file_data.conf_data, bcc * sizeof (char*));

		rc = bc_reduce(bc, bcc, (host ? host : "[^.]*"), (appl ? appl : "[^.]*"));

		qsort(bc, rc, sizeof (char*), bc_comps[skip]);

		if (can_cache && bconf_cache && hash_table_search(bconf_cache, cache_key, -1, NULL)) {
			transaction_logprintf(cs->ts, D_INFO, "Can't cache due to concurrent cache store.");
			can_cache = 0;
		}

		if (can_cache) {
			if (!bconf_cache) {
				bconf_cache = hash_table_create(20, bconf_cache_free);
				hash_table_free_keys(bconf_cache, 1);
			}
			cache = xmalloc(sizeof (*cache));
			cache->rc = rc;
			cache->bc = bc;
			hash_table_insert(bconf_cache, xstrdup(cache_key), -1, cache);
			transaction_logprintf(cs->ts, D_DEBUG, "Inserted cache %p with %d entries for %s", cache, rc, cache_key);
		}
	}

	if (rc) {
		transaction_logoff(cs->ts);
		for (cnt = 0 ; cnt < rc ; cnt++) {
			if (!cnt || (cnt && !key_comp(bc[cnt - 1], bc[cnt], skip))) {
				if (strchr(bc[cnt], '=')) {
					if (strchr(bc[cnt],'\n') || strchr(bc[cnt],'\r'))
						transaction_printf(cs->ts, "blob:%zu:conf\n%s\n",
								strlen(skip_fields(bc[cnt], skip)), skip_fields(bc[cnt], skip));
					else
						transaction_printf(cs->ts, "conf:%s\n", skip_fields(bc[cnt], skip));
				} else {
					transaction_printf(cs->ts, "del:%s\n", skip_fields(bc[cnt], skip));
				}
			}
		}
		transaction_logon(cs->ts);
	}
	pthread_mutex_unlock(&bconf_cache_lock);

	if (!can_cache)
		free(bc);

	cmd_done(cs);
}

static void
trans_bconf_cleanup(void) {
	free_bconf_data(&file_data);
	if (bconf_cache)
		hash_table_free(bconf_cache);
}

ADD_COMMAND_FLAGS(bconf, init, trans_bconf_cleanup, bconf, parameters, NULL, CMD_NORMAL_PORT);

/*Parameters for get_values command*/
static struct c_param get_values_params[] = {
        {"key", P_REQUIRED, "v_bconf_keyval"},
        {NULL}
};


/**********************************************************************************
*			bconf_find_values
*	Given a key (can be a regular expression) it searches through the bconf data
*	modifies the bc array of data and returns the number of found bconf keys that
*	matches with the given key.
*********************************************************************************/
static int
bconf_find_values (char **bc, int rows, const char *key, struct cmd *cs) {
	int bc_cnt = 0;
	int cnt;
 	pcre *re;
 	const char *err;
 	int erro;

 	if (!(re = pcre_compile(key, REG_NOSUB | REG_EXTENDED | REG_ICASE, &err, &erro, pcre_table))) {
		cs->status = "TRANS_ERROR_NOT_VALID_REGULAR_EXPRESSION";
		return 0;
	}
	for (cnt = 0 ; cnt < rows ; cnt++) {
 		if (pcre_exec(re, NULL, bc[cnt], strlen(bc[cnt]), 0, 0, NULL, 0) == 0) {
			char *pos = bc[cnt];
			bc[bc_cnt++] = pos;
		}
	}

 	pcre_free(re);

	return bc_cnt;
		
}

/********************************************************************************
*			bconf_print_values
*	After finding the values this function prints out the found values
*********************************************************************************/
static void 
bconf_print_values(struct cmd *cs) {	
	int rc;
	int cnt;
	int bcc = 0;
	char **bc;
	
	bcc = file_data.rows;
	bc = xmalloc(bcc * sizeof (char*));
	if (bcc)
		memcpy(bc, file_data.conf_data, bcc * sizeof (char*));
	rc = bconf_find_values(bc, bcc, cmd_getval(cs, "key"), cs);
	qsort(bc, rc, sizeof (char*), bc_comp_0);
	if (rc) {
		transaction_logoff(cs->ts);
		for (cnt = 0 ; cnt < rc ; cnt++) {
			if (!cnt || (cnt && !key_comp(bc[cnt - 1], bc[cnt], 0))) {
				if (strchr(bc[cnt], '=')) {
					transaction_printf(cs->ts, "conf:%s\n", bc[cnt]);
				} 
			}
		}
		transaction_logon(cs->ts);
	}
	free(bc);
	return;
}


/*********************************************************************************
*	Main function for the bconf_get_values command
**********************************************************************************/
static void
bconf_get_values(struct cmd *cs) {
	bconf_print_values(cs);
	cmd_done(cs);
}

ADD_COMMAND(bconf_get_values, NULL, bconf_get_values, get_values_params, "Command for getting a value or set of values given a key that can be a regular expression");

