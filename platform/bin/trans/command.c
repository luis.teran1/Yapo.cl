#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <libintl.h>
#include <ctype.h>
#include <errno.h>
#include <unicode/ustring.h>
#include <unicode/unorm.h>
#include <unicode/ucnv.h>
#include <unicode.h>
#include <pcre.h>

#include "trans.h"
#include "command.h"
#include "util.h"
#include "strl.h"
#include "bconf.h"
#include "bpapi.h"
#include "ctemplates.h"

struct command_thread {
	pthread_t thread;
	pthread_cond_t cond;
	struct cmd *cs;
	const struct command *command;
	SLIST_ENTRY(command_thread) list;
	TAILQ_ENTRY(command_thread) running_threads;

	struct bconf_node *filter_state_thread;
};

TAILQ_HEAD(,cmd) pending_commands = TAILQ_HEAD_INITIALIZER(pending_commands); /* XXX should be running_commands */
TAILQ_HEAD(,cmd) waiting_commands = TAILQ_HEAD_INITIALIZER(waiting_commands);
pthread_rwlock_t pending_commands_lock = PTHREAD_RWLOCK_INITIALIZER;
pthread_rwlock_t waiting_commands_lock = PTHREAD_RWLOCK_INITIALIZER;

SLIST_HEAD(, command_thread) ct_pool = SLIST_HEAD_INITIALIZER(ct_pool);
TAILQ_HEAD(, command_thread) ct_pool_running_threads = TAILQ_HEAD_INITIALIZER(ct_pool_running_threads);

int32_t ct_threads_count = 0;

pthread_mutex_t ct_pool_lock = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t ct_pool_running_threads_lock = PTHREAD_MUTEX_INITIALIZER;

pthread_key_t csbase_key;
pthread_key_t log_string_key;

static int match_param(const char *arg, int ptype, const char *pname);

static void verify_one_param(struct cmd *cs, const char *name, int type, const char *validators, struct validator *dyn);

static int
cmd_send(const struct cmd *cs, const char *key, const char *code, char *message) {

	if (!cs->ts)
		return 1;

	if (message != NULL)
		transaction_printf(cs->ts, "%s:%s:%s\n", key, code, message);
	else 
		transaction_printf(cs->ts, "%s:%s\n", key, code);

	return 0;
}

int
cmd_haskey(struct cmd *cs, const char *key) {
	struct cmd_param *cp;

	TAILQ_FOREACH(cp, &cs->params, tq) {
		if (strcmp(cp->key, key) == 0) {
			/* Can't return 1 here if not validated since users will assume getval returns a value then. */
			if (!(cp->flags & CMDP_VALIDATED))
				return 0;
			return 1;
		}
	}

	return 0;
}

struct cmd_param*
cmd_getparam(struct cmd *cs, const char *key) {
	struct cmd_param *cp;

	TAILQ_FOREACH(cp, &cs->params, tq) {
		if (strcmp(cp->key, key) == 0) {
			return cp;
		}
	}

	return NULL;
}

static int
cmd_hasdynparam(struct cmd *cs, const char *name) {
	int i;	
	for(i=0 ; i<cs->ndyn_params ; ++i)
	{
		if (strcmp(cs->dyn_params[i]->name, name) == 0)
			return 1;
	}

	return 0;
}

struct cmd_param*
cmd_nextparam(struct cmd *cs, struct cmd_param *prev) {
	const char *key = prev->key;
	
	while ((prev = TAILQ_NEXT(prev, tq)) != TAILQ_END(&cs->params)) {
		if (!strcmp(prev->key, key))
			return prev;
	}
	
	return NULL;
}

const char* 
cmd_getval(struct cmd *cs, const char *key) {
	struct cmd_param *cp;

	TAILQ_FOREACH(cp, &cs->params, tq) {
		if (strcmp(cp->key, key) == 0) {
			if (!(cp->flags & CMDP_VALIDATED))
				return NULL;
			if (cp->value)
				return cp->value;
			return "";
		}
	}

	return NULL;
}

int
cmd_getint_default(struct cmd *cs, const char *key, int default_value) {
	const char *val = cmd_getval(cs, key);

	if (!val)
		return default_value;

	return atoi(val);
}

size_t 
cmd_getsize(struct cmd *cs, const char *key) {
	struct cmd_param *cp;

	TAILQ_FOREACH(cp, &cs->params, tq) {
		if (strcmp(cp->key, key) == 0 && cp->value) {
			return cp->value_size;
		}
	}

	return 0;
}

void
cmd_setval(struct cmd *cs, const char *key, char *new_value) {
	struct cmd_param *cp;

	TAILQ_FOREACH(cp, &cs->params, tq) {
		if (strcmp(cp->key, key) == 0) {
			if (cp->value && (cp->value !=  cp->key + strlen(cp->key) + 1))
				free(cp->value);
			cp->value = new_value;
			cp->flags |= CMDP_NORMALIZED | CMDP_VALIDATED;
			return;
		}
	}

	cp = zmalloc(sizeof(*cp) + strlen(key) + 1 + strlen(new_value) + 1);
	cp->cs = cs;
	cp->key = (char*)(cp + 1);
	cp->value = cp->key + strlen(key) + 1;
	cp->value_size = strlen(new_value) + 1;
	cp->flags = CMDP_NORMALIZED | CMDP_VALIDATED;
	strcpy(cp->key, key);
	strcpy(cp->value, new_value);
	free(new_value);
			
	TAILQ_INSERT_TAIL(&cs->params, cp, tq);
}

void
cmd_delval(struct cmd *cs, const char *key) {
	struct cmd_param *cp;

	TAILQ_FOREACH(cp, &cs->params, tq) {
		if (strcmp(cp->key, key) == 0) {
			TAILQ_REMOVE(&cs->params, cp, tq);

			if (cp->message)
				free(cp->message);

			if (cp->value && (cp->value !=  cp->key + strlen(cp->key) + 1))
				free(cp->value);

			free(cp);

			return;
		}
	}
}

static void
free_dyn_param(struct d_param *d) {
	int j;
	
	free(d->name);
	if (d->validators && !(d->flags & P_DYN_STATIC_VALIDATOR)) {

		for (j = 0; d->validators[j].type; j++) {
			if (d->validators[j].v_flags & STR_ALLOCED ) {
				free((char *)d->validators[j].error);
				if (d->validators[j].type == V_REGEX)
					free((char *)d->validators[j].regex.regex);
			}
		}	
		free(d->validators);
	}	
	free(d);
}

void
cmd_detach(struct cmd *cs) {
	struct transaction *ts = cs->ts;
	struct cmd_param *cp;

	transaction_logprintf(cs->ts, D_DEBUG, "cmd_detach");

	TAILQ_FOREACH(cp, &cs->params, tq) {
                if (cp->error)
                        cmd_send(cs, cp->key, cp->error, cp->message);
	}

	/* Display error-message if any - should be a static string. */
	if (cs->error) {
		transaction_printf(cs->ts, "error:%s\n", cs->error);
	}

	/* Now send status field and end terminator */
	if (cs->message)  {
		char *escaped_message = escape_control_characters(cs->message);
		transaction_printf(cs->ts, "status:%s:%s\n", cs->status, escaped_message);
		free(escaped_message);
	} else {
		transaction_printf(cs->ts, "status:%s\n", cs->status);
	}

	transaction_done(ts);
	cs->ts = NULL;
}

void
cmd_done(struct cmd *cs) {
	int i;
	struct transaction *ts = cs->ts;
	struct cmd_param *cp;

	pthread_rwlock_wrlock(&pending_commands_lock);
	TAILQ_REMOVE(&pending_commands, cs, cmd_list);
	pthread_rwlock_unlock(&pending_commands_lock);

	if (ts) {
		transaction_logprintf(cs->ts, D_DEBUG, "cmd_done");
		cmd_detach(cs);
	}

        /* Traverse all parameters and send all pending errors */
	while ((cp = TAILQ_FIRST(&cs->params)) != NULL) {
		TAILQ_REMOVE(&cs->params, cp, tq);

		if (cp->message)
			free(cp->message);

		if (cp->value && (cp->value != cp->key + strlen(cp->key) + 1))
			free(cp->value);
		free(cp);
	}


        if (cs->message)
                free(cs->message);

	/* Free up dynamic params. */
	for (i = 0; i < cs->ndyn_params; i++) {
		free_dyn_param(cs->dyn_params[i]);
	}


	timer_add_attribute(cs->ti, strcmp(cs->status, "TRANS_OK") ? "error" : "ok");

	timer_end(cs->ti, NULL);

	/* Our caller will have to take care of the event_base. */

	/* Lock to make transinfo safe. */
	pthread_mutex_lock(&ct_pool_running_threads_lock);
	if (cs->thread_data)
		cs->thread_data->cs = NULL;
	pthread_mutex_unlock(&ct_pool_running_threads_lock);
	free(cs);
}

static void init_validators(const char *);

void
cmd_extra_param(struct cmd *cs, struct c_param *param) {

	transaction_logprintf(cs->ts, D_DEBUG, "adding extra param %s", param->name);

	if (cs->nextra_params >= NEXTRAPARAMS)
		xerrx(1, "cmd_extra_param: too many params");
	cs->extra_params[cs->nextra_params++] = param;


	if (param->validators) {
		init_validators(param->validators);
	} else {
		cs->status = "TRANS_ERROR";
		cmd_send(cs, param->name, "ERROR_VALIDATOR_MISSING", NULL);
	}
}

int
cmd_has_extra_param(struct cmd *cs, const char *name) {
	int i;
	struct c_param *param;

	for (i = 0; i < cs->nextra_params; i++) {
		param = cs->extra_params[i];
		if (match_param(name, param->type, param->name))
			return 1;
	}

	return 0;
}

void
cmd_dyn_param(struct cmd *cs, struct d_param *param) {

	if (!cmd_hasdynparam(cs, param->name)) {
		transaction_logprintf(cs->ts, D_DEBUG, "adding dynamic param %s", param->name);

		if (cs->ndyn_params >= NDYNPARAMS)
			xerrx(1, "cmd_dyn_param: too many params");
		cs->dyn_params[cs->ndyn_params++] = param;
	} else {
		/*transaction_logprintf(cs->ts, D_DEBUG, "ignoring re-add of dynamic param %s", param->name);*/
		free_dyn_param(param);
	}
}


struct validator*
load_validator(const char *validator, const char **arg_out, int can_fail) {
	struct _validator * const *_v;
	struct validator *v = NULL;
	const char *arg = strchr(validator, ':');
	char *vname = NULL;
	
	if (arg) {
		int vnlen = arg++ - validator;

		vname = xmalloc(vnlen + 1);
		memcpy(vname, validator, vnlen);
		vname[vnlen] = '\0';
		validator = vname;
	}
	LINKER_SET_DECLARE(vlds, struct _validator);
	LINKER_SET_FOREACH(_v, vlds) {
		if (strcmp((*_v)->name, validator) == 0) {
			v = (*_v)->v;
			if (arg_out)
				*arg_out = arg;
			break;
		}
	}
	if (!v && !can_fail) {
		xerrx(1, "validator not implemented <%s>", validator);
	}

	if (vname)
		free(vname);

	return v;
}
		

static int
validate(const char *vname, struct validator *vs, const char *arg, const char *name, int type, struct cmd *cs) {
        struct cmd_param *param;
	char *e;
        int status = 0;
	const char *sarg;
	struct validator *v;
	struct validator *sv;
	pcre *re;
	struct cmd_param *p;
	struct bconf_node *bconf_node = NULL;

	transaction_logprintf(cs->ts, D_DEBUG, "starting validator %s for %s", vname, name);

	if (name[0] ==  '_') {
		param = &cs->base_param;
	} else {
		param = cmd_getparam(cs, name);
	}

	do {
		for (v = vs; v->type; v++) {
	                status = 0;
			p = param;

			if (!param && v->type == V_FUNC && (v->v_flags & FUNC_ALWAYS))
				p = &cs->base_param;
			else if ((param == NULL || (param->value == NULL && v->type != V_FUNC)) && v->type != V_REQ && v->type != V_VAL)
				break;

			if (param && (type & P_TEXT) && !(p->flags & CMDP_NORMALIZED)) {
				const char *charset = bconf_get_string(trans_bconf, "common.charset");

				if (charset && strcmp(charset, "UTF-8") == 0) {
					struct cmd_param *utf8 = cmd_getparam(cs, "utf8");

					if (!utf8) {
						p->error = "ERROR_MISSING_UTF8_PARAM";
						cs->status = "TRANS_ERROR";
						status = 1;
						break;
					}

					if (strcmp(utf8->value, "nfc") != 0 && strcmp(utf8->value, "nfkc") != 0) {
						/* Normalization needed. */
						size_t ubufsz = p->value_size * 2 + 1;
						UChar *ubufa = xmalloc(ubufsz * sizeof (*ubufa));
						UChar *ubufb = xmalloc(ubufsz * sizeof (*ubufb));
						int32_t l;
						UErrorCode err = 0;

						if (strcmp(utf8->value, "yes") != 0
								&& strcmp(utf8->value, "nfd") != 0
								&& strcmp(utf8->value, "nfkd") != 0) {
							/* Assume it is a charset. */
							UConverter *cnv = ucnv_open(utf8->value, &err);
							if (!cnv) {
								p->error = "ERROR_DECODE_FAILED";
								utf8->error = "ERROR_INVALID";
								cs->status = "TRANS_ERROR";
								if (!utf8->message)
									utf8->message = xstrdup(u_errorName(err));
								status = 1;
								free(ubufa);
								free(ubufb);
								break;
							}
							l = ucnv_toUChars(cnv, ubufa, ubufsz, p->value, p->value_size, &err);
							ucnv_close(cnv);
						} else {
							u_strFromUTF8(ubufa, ubufsz, &l, p->value, p->value_size, &err);
						}
						if (err) {
							p->error = "ERROR_DECODE_FAILED";
							p->message = xstrdup(u_errorName(err));
							cs->status = "TRANS_ERROR";
							status = 1;
							free(ubufa);
							free(ubufb);
							break;
						}
						l = unorm_normalize(ubufa, l, UNORM_NFC, 0, ubufb, ubufsz, &err);
						if (err) {
							p->error = "ERROR_DECODE_FAILED";
							p->message = xstrdup(u_errorName(err));
							cs->status = "TRANS_ERROR";
							status = 1;
							free(ubufa);
							free(ubufb);
							break;
						}
						free(ubufa);
						p->value = xmalloc(p->value_size * 2 + 1);
						u_strToUTF8(p->value, p->value_size * 2 + 1, &l, ubufb, l, &err);
						free(ubufb);
						if (err) {
							p->error = "ERROR_DECODE_FAILED";
							p->message = xstrdup(u_errorName(err));
							cs->status = "TRANS_ERROR";
							status = 1;
							break;
						}
						p->value_size = l;
					}
				}
				p->flags |= CMDP_NORMALIZED;
			}

			switch (v->type) {
			case V_REQ:
				if (v->v_flags & (cs->validate_only ? REQ_VALIDATION : REQ_EXECUTION) &&
				    (p == NULL || p->value == NULL)) {
					cmd_send(cs, name, v->error, NULL);
					status = 1;
				}
				break;
			case V_REGEX:
				if (!v->initialized) {
					int flags = PCRE_DOLLAR_ENDONLY | PCRE_DOTALL | PCRE_NO_AUTO_CAPTURE;
					const char *charset = bconf_get_string(trans_bconf, "common.charset");
					const char *err;
					int erro;

					if (charset && strcmp(charset, "UTF-8") == 0)
						flags |= PCRE_UTF8;

					re = pcre_compile(v->regex.regex, flags | v->regex.reg_flags, &err, &erro, pcre_table);
					if (!re) {
						status = 1;
						break;
					}
				} else
					re = (pcre*)v->data;
				if (pcre_exec(re, NULL, p->value, strlen(p->value), 0, PCRE_NO_UTF8_CHECK, NULL, 0) != (v->invert ? PCRE_ERROR_NOMATCH : 0))
					status = 1;
				if (!v->initialized)
					pcre_free(re);
				break;
			case V_FUNC:
				if (v->v_func(p, arg) != 0)
					status = 1;
				break;
			case V_LEN:
			{
				int ln = (int)strlen_utf8(p->value);
				if (v->minmax.min > -1 && ln < v->minmax.min)
					status = 1;
				if (v->minmax.max > -1 && ln > v->minmax.max)
					status = 1;
				break;
			}
			case V_INT:
			case V_INT32:
			case V_INT64:
			{
				int64_t ll = strtoll(p->value, &e, 10);
				if (!*(p->value) || *e != '\0') {
					status = 1;
				} else {
					if (v->type == V_INT32 && ll > INT32_MAX)
						status = 1;
					else if ((v->type == V_INT32 || v->type == V_INT64) && ll < 0)
						status = 1; /* XXX not sure about this one but matches orig impl */
					else if (v->minmax.min >= 0 && ll < v->minmax.min)
						status = 1;
					else if (v->minmax.max >= 0 && ll > v->minmax.max)
						status = 1;
				}
				break;
			}
			case V_DOUBLE: /* imported from kapaza. */
			{
				/* Loop for change comma into dot */
				int i;
				double d;
				for (i=0; p->value[i]; i++){
					if (p->value[i] == ',') {
						p->value[i] = '.';
						break;
					}
				}
				d = strtod(p->value, &e);
				/* Check for min and max values*/
				if (!*(p->value) || *e != '\0') {
					status = 1;
				} else {
					if (v->minmax.min >= 0 && d < (double) v->minmax.min)
						status = 1;
					if (v->minmax.max >= 0 && d > (double) v->minmax.max)
						status = 1;
				}
				break;
			}
			case V_VAL:
				sv = load_validator(v->sub_validators, &sarg, 0);
				validate(v->sub_validators, sv, sarg, name, type, cs);
				break;
			case V_BCONF_LIST:
				bconf_node = bconf_get(trans_bconf, v->bconf_path.bconf_path);
				/* Fallthrough */
			case V_BCONF_KEY:
			{
				if (v->type == V_BCONF_KEY)
					bconf_node = v->bconf_node;
				int num = bconf_count(bconf_node);
				int i;

				if ((v->v_flags & VFLAG_COMMALIST) && p->value) {
					char *vcopy = xstrdup(p->value);
					char *vsep = vcopy;
					char *vv;

					while (!status && (vv = strsep(&vsep, ","))) {
						for (i = 0; i < num; i++) {
							const char *value = bconf_value(bconf_byindex(bconf_node, i));
							if (value && !strcmp(value, vv))
								break;
						}
						if (i == num) {
							transaction_logprintf(cs->ts, D_DEBUG, "value %s not found in list", vv);
							status = 1;
						}
					}
					free(vcopy);
				} else if (v->v_flags & VFLAG_BCONF_ISKEY) {
					if (bconf_get(bconf_node, p->value) == NULL) {
						transaction_logprintf(cs->ts, D_DEBUG, "key %s not found in list", p->value);
						status = 1;
					}
				} else {
					for (i = 0; i < num; i++) {
						struct bconf_node *node;
						if (v->v_flags & VFLAG_DOTPATH) {
							node = bconf_get(bconf_byindex(bconf_node, i), v->bconf_path.dot_path);
						} else {
							node = bconf_byindex(bconf_node, i);
						}
						const char *value = bconf_value(node);
						if (value && !strcmp(value, p->value))
							break;
					}
					if (i == num) {
						transaction_logprintf(cs->ts, D_DEBUG, "value %s not found in list", p->value);
						status = 1;
					}
				}
				break;
			}

			case V_GPERF_ENUM:
				if (v->gperf_enum_func(p->value, p->value_size) == 0)
					status = 1;
				break;

			case V_OTHER:
				{
					struct cmd_param *other = cmd_getparam(cs, v->other.name);
				
					verify_one_param(cs, v->other.name, v->other.type, v->other.validators, NULL);

					if ((other && other->error) || (!other && !(v->other.type & P_OPTIONAL)))
						status = 1;
				}
				break;
			}

	                /* Break on errors and overwrite old warnings */
	                if (status) {
				if (p == NULL) {
					cs->status = "TRANS_ERROR";
					break;
				} else if (!IS_ERROR(p->error)) {
	                                p->error = v->error;

	                                if ((p->error == NULL && (type & P_ARG_ERR)) || IS_ERROR(p->error)) {
						if (!strcmp(cs->status, "TRANS_OK"))
	                                                cs->status = "TRANS_ERROR";
	                                        break;
	                                }
	                        }
	                }
		}

		if (!param || (status && ((param->error == NULL && (type & P_ARG_ERR)) || IS_ERROR(param->error))))
			break;

		if (param)
			param->flags |= CMDP_VALIDATED;

		param = cmd_nextparam(cs, param);
		if (param && !(type & P_MULTI)) {
			param->error = "ERROR_MULTIPLE";
			if (!strcmp(cs->status, "TRANS_OK"))
				cs->status = "TRANS_ERROR";
			break;
		}
	} while (param);

	if (param != NULL) {
		if (param->error)
			transaction_logprintf(cs->ts, D_DEBUG, "ending validator for %s with status %s", name, param->error);
		else if (type & P_ARG_ERR) {
			transaction_logprintf(cs->ts, D_DEBUG, "ending validator for %s with status ERROR_%s_INVALID", name, arg);
			transaction_printf(cs->ts, "%s:ERROR_%s_INVALID\n", name, arg);
		}
	} else
		transaction_logprintf(cs->ts, D_DEBUG, "ending validator for %s", name);

	return status;
}

static int
match_param(const char *arg, int ptype, const char *pname) {
	if (strcmp(pname, arg) == 0) {
		return 1;
	} else if (ptype & (P_XOR|P_OR|P_AND|P_EQ|P_NAND|P_IMPLY)) {
		const char *sp;
		const char *nsp;
		int alen = strlen(arg);
		int plen;

		for (sp = pname; sp; sp = nsp) {
			nsp = strchr(sp, ';');
			if (nsp)
				plen = nsp++ - sp;
			else
				plen = strlen(sp);
			if (plen == alen && !strncmp(arg, sp, alen))
				return 1;
		}
	}
	return 0;
}

static void
extra_parameters(struct cmd *cs) {
	struct cmd_param *cp;
	const struct c_param *param;

	TAILQ_FOREACH(cp, &cs->params, tq) {
		int found;
		int i;

		if (cp->flags & CMDP_VALIDATED)
			continue;

		found = 0;
		if (strcmp(cp->key, "utf8") == 0)
			found = 1;
		for (param = cs->command->params; !found && param->name != NULL; param++) {
			if (match_param(cp->key, param->type, param->name))
				found = 1;
		}
		for (i = 0; !found && i < cs->nextra_params; i++) {
			param = cs->extra_params[i];
			if (match_param(cp->key, param->type, param->name))
				found = 1;
		}
		for (i = 0; !found && i < cs->ndyn_params; i++) {
			struct d_param *dparam = cs->dyn_params[i];
			if (match_param(cp->key, dparam->flags, dparam->name))
				found = 1;
		}

                /* Found an unrecognized parameter */
		if (!found) {
                        cmd_send(cs, cp->key, "TRANS_ERROR_NO_SUCH_PARAMETER", cp->message);
                        cs->status = "TRANS_ERROR";
		}
	}
}

static void 
vp_bool_op(struct cmd *cs, const char *param_name, int param_type, const char *param_validators, int bool_op) {
	char *arg_cpy = xstrdup(param_name);
	char *pname;
	char *validator_name = NULL;
	char *delim = NULL;
	char *delimv = NULL;
	char *paramname;
	char *errmsg;
	int nrofparam = 0;
	int totparam = 0;
	int firstparam = 0;
	struct validator *vs;
	const char *arg;
	int res = 0;

	if (param_validators) {
		char *val_cpy = xstrdup(param_validators);
		nrofparam = 0;
		totparam = 0;
		for (pname = strtok_r(arg_cpy, ";", &delim), validator_name = strtok_r(val_cpy, ";", &delimv);
		    pname && validator_name;
		    pname = strtok_r(NULL, ";", &delim), validator_name = strtok_r(NULL, ";", &delimv)) {
			totparam++;
			if (cmd_getparam(cs, pname)) {
				nrofparam++;
				if (totparam == 1)
					firstparam = 1;
				vs = load_validator(validator_name, &arg, 0);
				validate(validator_name, vs, arg, pname, param_type, cs);
			}
		}
		free(val_cpy);

		if (pname || validator_name) {
			cs->status = "TRANS_ERROR";
			cmd_send(cs, param_name, "ERROR_VALIDATOR_OR_PARAM_MISSING_XOR", NULL);
		}
	} else {
		nrofparam = 0;
		totparam = 0;
		for (pname = strtok_r(arg_cpy, ";", &delim);
		    pname;
		    pname = strtok_r(NULL, ";", &delim)) {
			totparam++;
			if (cmd_getparam(cs, pname)) {
				nrofparam++;
			}
		}
	}
	free(arg_cpy);

	if ((nrofparam != 1 && bool_op == P_XOR) && !(nrofparam == 0 && (param_type & P_OPTIONAL))) {
		cs->status = "TRANS_ERROR";
		paramname = xstrdup(param_name);
		res = asprintf(&errmsg, "%s;ERROR_XOR_MISSING", strmodify(paramname, toupper));
		cmd_send(cs, param_name, errmsg, NULL);
		free(errmsg);
		free(paramname);
	}
	if (nrofparam == 0 && bool_op == P_OR) {
		cs->status = "TRANS_ERROR";
		paramname = xstrdup(param_name); 
		res = asprintf(&errmsg, "%s;ERROR_OR_MISSING", strmodify(paramname, toupper));
		cmd_send(cs, param_name, errmsg, NULL);
		free(errmsg);
		free(paramname);
	}
	if (nrofparam != totparam && bool_op == P_AND) {
		cs->status = "TRANS_ERROR";
		paramname = xstrdup(param_name); 
		res = asprintf(&errmsg, "%s;ERROR_AND_MISSING", strmodify(paramname, toupper));
		cmd_send(cs, param_name, errmsg, NULL);
		free(errmsg);
		free(paramname);
	}
	
	if (nrofparam > 1 && bool_op == P_NAND) {
		cs->status = "TRANS_ERROR";
		paramname = xstrdup(param_name); 
		res = asprintf(&errmsg, "%s;ERROR_NAND_TOO_MANY", strmodify(paramname, toupper));
		cmd_send(cs, param_name, errmsg, NULL);
		free(errmsg);
		free(paramname);
	}	

	if ((nrofparam != totparam && nrofparam != 0) && bool_op == P_EQ) {
		cs->status = "TRANS_ERROR";
		paramname = xstrdup(param_name);
		res = asprintf(&errmsg, "%s;ERROR_EQ_MISSING", strmodify(paramname, toupper));
		cmd_send(cs, param_name, errmsg, NULL);
		free(errmsg);
		free(paramname);
	}
	if (firstparam && totparam != nrofparam && bool_op == P_IMPLY) {
		cs->status = "TRANS_ERROR";
		paramname = xstrdup(param_name);
		res = asprintf(&errmsg, "%s;ERROR_IMPLY_MISSING", strmodify(paramname, toupper));
		cmd_send(cs, param_name, errmsg, NULL);
		free(errmsg);
		free(paramname);
	}
	if (res < 0)
		transaction_logprintf(cs->ts, D_DEBUG, "asprintf() write error in %s", __FUNCTION__);

}

static void
verify_one_param(struct cmd *cs, const char *name, int type, const char *validators, struct validator *dyn) {
	struct cmd_param *tmpparam;

	if (type & P_REQUIRED) {
		tmpparam = cmd_getparam(cs, name);
		if (!tmpparam) {
			const char *arg = NULL;
			char *paramname, *errmsg;

			cs->status = "TRANS_ERROR";
			if (type & P_ARG_ERR)
				load_validator(validators, &arg, 0);
			if (arg)
				paramname = xstrdup(arg);
			else
				paramname = xstrdup(name);

			xasprintf(&errmsg, "ERROR_%s_MISSING", strmodify(paramname, toupper));
			cmd_send(cs, name, errmsg, NULL);
			free(errmsg);
			free(paramname);
		}
	} else if (type & P_AND) {
		vp_bool_op(cs, name, type, validators, P_AND);
	} else if (type & P_OR) {
		vp_bool_op(cs, name, type, validators, P_OR);
	} else if (type & P_XOR) {
		vp_bool_op(cs, name, type, validators, P_XOR);
	} else if (type & P_EQ) {
		vp_bool_op(cs, name, type, validators, P_EQ);
	} else if (type & P_NAND) {
		vp_bool_op(cs, name, type, validators, P_NAND);
	} else if (type & P_IMPLY) {
		vp_bool_op(cs, name, type, validators, P_IMPLY);
	}

	if ((type & P_EMPTY) && (tmpparam = cmd_getparam(cs, name)) && tmpparam->value_size == 0)
		/* Ok */;
	else if (!(type & (P_AND|P_OR|P_XOR|P_EQ|P_NAND|P_IMPLY))) {
		if (dyn) {
			validate("(dynamic)", dyn, NULL, name, type, cs);
		} else if (validators) {
			struct validator *vs;
			const char *arg;

			vs = load_validator(validators, &arg, 0);
			validate(validators, vs, arg, name, type, cs);
		}
	}
}

static void verify_parameters(struct cmd *cs);

static void
verify_parameters_async(int fd, short what, void *arg) {
	struct cmd *cs = arg;

	cs->validate_phase = 0;
	verify_parameters(cs);
}

static void
verify_parameters(struct cmd *cs) {
	const struct c_param *param = cs->valparam;
	int i;

	if (!param)
		param = cs->command->params;
	if (!param)
		cs->validate_phase = 4;

	transaction_logprintf(cs->ts, D_DEBUG, "verify_parameters: phase %d, pending %d", cs->validate_phase, cs->pending_validators);

	/* Validate supplied parameters and check for missing parameters */
	switch (cs->validate_phase) {
	case 1:
	case 3:
		return;
	case -1:
		if (cs->pending_validators)
			return;
		event_base_once(cs->base, -1, EV_TIMEOUT, verify_parameters_async, cs, NULL);
		return;
	case 0:
		cs->validate_phase = 1;
		/*
		 * Phase zero. Start the validation of all basic parameters.
		 */
		for (; param->name != NULL; param++) {
			verify_one_param(cs, param->name, param->type, param->validators, NULL);
			if (cs->pending_validators) {
				cs->valparam = param + 1;
				cs->validate_phase = -1;
				return;
			}
		}
		cs->validate_phase = 2;
	case 2:
		/*
		 * Phase one. All validators have been started. Wait until they complete.
		 * We'll be called as a callback by every validator that finishes until
		 * there are no pending validators left.
		 */
		if (cs->pending_validators)
			return;
		cs->validate_phase = 3;
		for (i = 0; i < cs->nextra_params; i++) {
			param = cs->extra_params[i];
			verify_one_param(cs, param->name, param->type, param->validators, NULL);
		}
		for (i = 0; i < cs->ndyn_params; i++) {
			struct d_param *dparam = cs->dyn_params[i];
			verify_one_param(cs, dparam->name, dparam->flags, NULL, dparam->validators);
		}
		cs->validate_phase = 4;
	case 4:
		/*
		 * Phase three. Do the final complex validation now that all parameters
		 * have been checked. The last entry in the param table with NULL name
		 * can contain a pointer to the complex validator.
		 */
		if (cs->pending_validators)
			return;
		break;
	}
	transaction_logprintf(cs->ts, D_DEBUG, "verify_parameters (post switch): phase %d, pending %d", cs->validate_phase, cs->pending_validators);

	/* Check for extra parameters */
	extra_parameters(cs);

	transaction_logprintf(cs->ts, D_DEBUG, "extra done");

	if (strcmp(cs->status, "TRANS_OK") != 0) {
		cmd_done(cs);
		return; 
	}

	if (cs->validate_only) {
		transaction_logprintf(cs->ts, D_INFO, "Command not executed, commit:1 not set.");
		cmd_done(cs);
		return;
	}

 	if (cs->command->func) 
		cs->command->func(cs);
	else
	 	cmd_done(cs);
}

static void
init_validators(const char *validator) {
	struct validator *v;
	char *val_cpy = xstrdup(validator);
	char *delimv = NULL;
	char *validator_name;
	static pthread_mutex_t init_validators_lock = PTHREAD_MUTEX_INITIALIZER;

	for (validator_name = strtok_r(val_cpy, ";", &delimv);
	    validator_name;
	    validator_name = strtok_r(NULL, ";", &delimv)) {
		v = load_validator(validator_name, NULL, 0);

		/* Compile regular expressions */
		while (v->type) {
			if (v->initialized) {
				v++;
				continue;
			}
			pthread_mutex_lock(&init_validators_lock);
			if (!v->initialized) {
				v->initialized = 1;
				if (v->type == V_REGEX) {
					int flags = PCRE_DOLLAR_ENDONLY | PCRE_DOTALL | PCRE_NO_AUTO_CAPTURE;
					const char *charset = bconf_get_string(trans_bconf, "common.charset");
					const char *err;
					int erro;
					pcre *re;

					if (charset && strcmp(charset, "UTF-8") == 0)
						flags |= PCRE_UTF8;

					re = pcre_compile(v->regex.regex, flags | v->regex.reg_flags, &err, &erro, pcre_table);
					if (!re) {
						xerrx(1, "regcomp (%s): %s", v->regex.regex, err);
						break;
					}

					v->data = re;
				}
			}
			pthread_mutex_unlock(&init_validators_lock);
			v++;
		}
	}
	free(val_cpy);
}

void
preinit_command(void) {
	pthread_key_create(&csbase_key, NULL);
	pthread_key_create(&log_string_key, NULL);
}

void
init_commands(char *check_only) {
	struct command * const *c;

	LINKER_SET_DECLARE(cmds, struct command);
	LINKER_SET_FOREACH(c, cmds) {
		if ((*c)->init) {
			DPRINTF(D_DEBUG, ("Initializing command \"%s\"", (*c)->name));
			(*c)->init(check_only);
		}

		if ((*c)->params) {
			DPRINTF(D_DEBUG, ("Loading validators for command \"%s\"", (*c)->name));

			struct c_param *p = (*c)->params;
			while (p->name) {
				if (p->validators) {
					init_validators(p->validators);
				} else {
					xerrx(1, "Missing validator for transaction command %s, parameter %s", (*c)->name, p->name);
				}
				p++;
			}
		}
	}
}

static void cleanup_validator(const char *validator) {
	struct validator    *v;
	char                *val_cpy = xstrdup(validator);
	char                *delimv = NULL;
	char                *validator_name;

	for (validator_name = strtok_r(val_cpy, ";", &delimv);
			validator_name;
			validator_name = strtok_r(NULL, ";", &delimv)) {

		v = load_validator(validator_name, NULL, 0);

		if(v) {
			for (; v->type; ++v) {
				if (v->type == V_REGEX && v->initialized) {
					pcre_free(v->data);
					v->data = NULL;
					v->initialized = 0;
				}
			}
		}

	}

	free(val_cpy);
}

void
cleanup_commands(void) {
	struct command * const *c;

	pthread_mutex_lock(&ct_pool_lock);
	while (!SLIST_EMPTY(&ct_pool)) {
		struct command_thread *th = SLIST_FIRST(&ct_pool);

		SLIST_REMOVE_HEAD(&ct_pool, list);

		th->cs = NULL;
		pthread_t pth = th->thread;
		pthread_cond_signal(&th->cond);
		pthread_mutex_unlock(&ct_pool_lock);
		pthread_join(pth, NULL);

		pthread_mutex_lock(&ct_pool_lock);
	}
	pthread_mutex_unlock(&ct_pool_lock);

	LINKER_SET_DECLARE(cmds, struct command);
	LINKER_SET_FOREACH(c, cmds) {
		if((*c)->params) {
			const struct c_param *p = (*c)->params;

			while (p->name) {
				if (p->validators) {
					cleanup_validator(p->validators);
				}

				++p;
			}
		}
	}
}

void
reset_commands_data(void) {
	struct command * const *c;

	LINKER_SET_DECLARE(cmds, struct command);
	LINKER_SET_FOREACH(c, cmds) {
		if ((*c)->reset)
			(*c)->reset();
	}
}

static void
copy_params(struct cmd *cs) {
	struct cmd_param *cp;
	struct elem *el;

	TAILQ_FOREACH(el, &cs->ts->ts_elems, el_list) {
		if (strcmp(el->el_key, "commit") == 0) {
			if (el->el_value)
				cs->validate_only = !atoi(el->el_value);
			continue;
		}

		if (strcmp(el->el_key, "end") == 0 || strcmp(el->el_key, "cmd") == 0)
			continue;
                
		cp = zmalloc(sizeof(*cp) + strlen(el->el_key) + 1 + el->el_size + 1);
                cp->cs = cs;
		cp->key = (char *)(cp + 1);
		cp->value_size = el->el_size;
		strlcpy(cp->key, el->el_key, strlen(el->el_key) + 1);
		if (el->el_value != NULL) {
			cp->value = cp->key + strlen(el->el_key) + 1;
			memcpy(cp->value, el->el_value, el->el_size);
			cp->value[el->el_size] = '\0';
		}

		TAILQ_INSERT_TAIL(&cs->params, cp, tq);
	}
}

/*
 * If cmd is NULL, we don't have a thread filter state, we'll just use the session state for the thread state.
 */
void
command_bpapi_init(struct command_bpapi *cba, struct cmd *cmd, int flags, const char *appl) {
	memset(cba, 0, sizeof(*cba));

	bpapi_common_init(&cba->ba);
	cba->flags = flags;

	/* I dislike negative flags, but this makes the calls less verbose. */
	if (!(flags & CMD_BPAPI_NO_OUTPUT))
		buf_output_init(&cba->ba.ochain);
	if (!(flags & CMD_BPAPI_NO_SIMPLEVARS)) {
		if (flags & CMD_BPAPI_SIMPLEVARS_HASH) {
			hash_simplevars_init(&cba->ba.schain);
		} else {
			pgsql_simplevars_init(&cba->ba.schain);
		}
	}

	/*
	 * There's no option to skip the vtree initialization since that's the whole point of this api.
	 */
	if (!(flags & CMD_BPAPI_HOST) == !(flags & CMD_BPAPI_TRANS)) {
		xerrx(1, "cmd_bpapi_init: one and only one of HOST or TRANS must be specified (0x%x)", flags);
	}
	if ((flags & CMD_BPAPI_TRANS) && appl) {
		xerrx(1, "cmd_bpapi_init: appl may be specified only for HOST");
	}

	if (flags & CMD_BPAPI_HOST) {
		bconf_vtree_app(&cba->bconf_vt, host_bconf, appl);
	} else {
		bconf_vtree(&cba->bconf_vt, trans_bconf);
	}
	filter_state_bconf_glue_session_start(&cba->ba.vchain, &cba->fs_glue, filter_state_global, (cmd && cmd->thread_data) ? cmd->thread_data->filter_state_thread : NULL, &cba->bconf_vt);
	cba->ba.filter_state = filter_state_bconf;
}

void
command_bpapi_free(struct command_bpapi *cba) {
	filter_state_bconf_glue_session_end(&cba->fs_glue);
	if (!(cba->flags & CMD_BPAPI_NO_OUTPUT))
		bpapi_output_free(&cba->ba);
	if (!(cba->flags & CMD_BPAPI_NO_SIMPLEVARS))
		bpapi_simplevars_free(&cba->ba);
	vtree_free(&cba->bconf_vt);
	bpapi_vtree_free(&cba->ba);
}

static void *
command_thread(void *arg) {
	struct event_base *base = event_base_new();
	int res;
	struct command_thread th = { 0 };
	char *log_string;
	struct sql_worker *sw;

	filter_state_bconf_init(&th.filter_state_thread, FS_THREAD);

	pthread_setspecific(csbase_key, base);
	pthread_cond_init(&th.cond, NULL);
	th.cs = arg;
	th.thread = pthread_self();

	pthread_mutex_lock(&ct_pool_running_threads_lock);
	TAILQ_INSERT_HEAD(&ct_pool_running_threads, &th, running_threads);
	pthread_mutex_unlock(&ct_pool_running_threads_lock);

	transaction_logprintf(th.cs->ts, D_DEBUG, "*** No available threads, new thread created (%d total)", ct_threads_count);

	while (th.cs) {
		th.cs->base = base;
		th.cs->thread = th.thread;
		th.cs->thread_data = &th;
		th.command = th.cs->command;

		/* A pass-by-global log_string used by transaction_internal if there wasn't one set explicitly */
		if (th.cs->ts->log_string)
			log_string = xstrdup(th.cs->ts->log_string);
		else
			log_string = NULL;
		pthread_setspecific(log_string_key, log_string);

		/* Needed to clear tv_cache for event_add */
		event_base_loop(base, EVLOOP_NONBLOCK);

		/* Switch the transaction bufferevent to our new event_base */
		bufferevent_disable(th.cs->ts->ts_buf, EV_READ | EV_WRITE);
		if (bufferevent_base_set(base, th.cs->ts->ts_buf))
			xerrx(1, "Failed to switch base for transaction bufferevent");

		verify_parameters(th.cs); 

		while ((res = event_base_dispatch(base)) < 0)
			DPRINTF(D_DEBUG, ("command thread event dispatch returned error: %s", xstrerror(errno)));

		DPRINTF(D_DEBUG, ("command thread event dispatch exited %s", res == 0 ? "by request" : "because events depleted"));
		
		if ((sw = sql_worker_find_active(th.thread)) != NULL) {
			DPRINTF(D_ERROR, ("(CRIT) command \"%s\" exited without releasing sql worker", th.command->name));
			sql_worker_put(sw);
		}

		if (th.cs) {
			DPRINTF(D_ERROR, ("(CRIT) command \"%s\" exited without calling cmd_done", th.cs->command->name));
			cmd_done(th.cs);
		}

		pthread_setspecific(log_string_key, NULL);
		free(log_string);

		/* Lock to make transinfo safe. */
		pthread_mutex_lock(&ct_pool_running_threads_lock);
		th.command = NULL;
		pthread_mutex_unlock(&ct_pool_running_threads_lock);

		pthread_mutex_lock(&ct_pool_lock);
		pthread_rwlock_wrlock(&waiting_commands_lock);
		if (TAILQ_EMPTY(&waiting_commands)) {
			pthread_rwlock_unlock(&waiting_commands_lock);
			if (ct_threads_count > config.max_command_threads) {
				DPRINTF(D_INFO, ("Thread %p exiting due to too many command threads (%d > %d)", &th, ct_threads_count, config.max_command_threads));
				th.cs = NULL;
				pthread_mutex_unlock(&ct_pool_lock);
			} else {
				DPRINTF(D_DEBUG, ("No queued commands, thread %p will wait for new commands", &th));

				/* Wait for next command. */
				SLIST_INSERT_HEAD(&ct_pool, &th, list);
				pthread_cond_wait(&th.cond, &ct_pool_lock);
				pthread_mutex_unlock(&ct_pool_lock);
			}
		} else {
			DPRINTF(D_DEBUG, ("Available commands in the queue for thread %p", &th));
			/* Pop the head from the queue */
			th.cs = TAILQ_FIRST(&waiting_commands);
			TAILQ_REMOVE(&waiting_commands, th.cs, waiting_list);

			/* Release the lock */
			pthread_rwlock_unlock(&waiting_commands_lock);
			pthread_mutex_unlock(&ct_pool_lock);
			DPRINTF(D_DEBUG, ("Thread %p about to handle command %s",
				&th,
				th.cs->command->name));
		}
	}

	event_base_free(base);

	filter_state_bconf_free(&th.filter_state_thread);

	pthread_mutex_lock(&ct_pool_running_threads_lock);
	ct_threads_count--;
	DPRINTF(D_DEBUG, ("Thread exiting (remaining: %d)", ct_threads_count));
	TAILQ_REMOVE(&ct_pool_running_threads, &th, running_threads);
	pthread_mutex_unlock(&ct_pool_running_threads_lock);

	return NULL;
}

void
handle_command(const char *command, struct transaction *ts) {
	struct command * const *c;

	LINKER_SET_DECLARE(cmds, struct command);
	LINKER_SET_FOREACH(c, cmds) {
		if (strcmp(command, (*c)->name) == 0
				&& ((*c)->flags & (ts->is_control ? CMD_CONTROL_PORT : CMD_NORMAL_PORT)) != 0) {
			struct cmd *cs = xcalloc(1, sizeof (*cs));

			cs->ti = timer_start(NULL, command);

			cs->ts = ts;
                        cs->status = "TRANS_OK";
			cs->command = *c;
			cs->validator_cb = verify_parameters;
			cs->validate_only = 1;
			cs->base_param.cs = cs;
			TAILQ_INIT(&cs->params);
			copy_params(cs);
			pthread_rwlock_wrlock(&pending_commands_lock);
			TAILQ_INSERT_TAIL(&pending_commands, cs, cmd_list);
			pthread_rwlock_unlock(&pending_commands_lock);
			if (ts->internal) {
				cs->base = ts->internal->base;
				cs->thread = ts->internal->thread;
				verify_parameters(cs); 
			} else {
				pthread_mutex_lock(&ct_pool_lock);
				if (SLIST_EMPTY(&ct_pool)) {

					pthread_mutex_lock(&ct_pool_running_threads_lock);
					if (ct_threads_count < config.max_command_threads || ts->is_control) {
						ct_threads_count++;
						pthread_mutex_unlock(&ct_pool_running_threads_lock);
						pthread_mutex_unlock(&ct_pool_lock);

						if (pthread_create(&cs->thread, NULL, command_thread, cs)) {
							xerr(1, "Failed to create command thread");
						}
					} else {
						/* Do not spawn an extra thread, put the command in the queue and wait for a thread to pick it up */
						pthread_mutex_unlock(&ct_pool_running_threads_lock);

						transaction_logprintf(ts, D_INFO, "The number of max allowed threads has been reached (%u), queuing the new command: %s\n", ct_threads_count, cs->command->name);
						pthread_rwlock_wrlock(&waiting_commands_lock);
						TAILQ_INSERT_TAIL(&waiting_commands, cs, waiting_list);
						pthread_rwlock_unlock(&waiting_commands_lock);
						pthread_mutex_unlock(&ct_pool_lock);
					}
				} else {
					struct command_thread *th = SLIST_FIRST(&ct_pool);

					SLIST_REMOVE_HEAD(&ct_pool, list);

					th->cs = cs;
					pthread_cond_signal(&th->cond);
					pthread_mutex_unlock(&ct_pool_lock);
				}
			}
			return;
		}
	}

	transaction_printf(ts, "status:%s:Err no such command\n", "TRANS_ERROR_NO_SUCH_COMMAND");
	transaction_done(ts);
}

void
transaction_cmdinfo(struct transaction *ts, int printblobs) {
	struct timespec ts_now;
	struct timespec ts_delta;

	struct cmd *c = NULL;
	int cnt = 0;

	clock_gettime(CLOCK_MONOTONIC, &ts_now);

	pthread_rwlock_rdlock(&pending_commands_lock);
	TAILQ_FOREACH(c, &pending_commands, cmd_list) {
		int pcnt = 0;
		struct cmd_param *param;

		memset(&ts_delta, 0, sizeof(ts_delta));
		timespecsub(&ts_now, &c->ti->ti_start, &ts_delta);

		transaction_printf(ts, "cmd.%u.name:%s\n", cnt, c->command->name);
		transaction_printf(ts, "cmd.%u.status:%s\n", cnt, c->status);
		transaction_printf(ts, "cmd.%u.message:%s\n", cnt, c->message);
		transaction_printf(ts, "cmd.%u.error:%s\n", cnt, c->error);
		transaction_printf(ts, "cmd.%u.transaction:%p\n", cnt, c->ts);
		transaction_printf(ts, "cmd.%u.thread:%p\n", cnt, c->thread_data);
		transaction_printf(ts, "cmd.%u.running_time:%ld.%03ld\n", 
					   cnt, (long int)ts_delta.tv_sec, (long int)ts_delta.tv_nsec / 1000000);

		if (c->ts && c->ts->log_string)
			transaction_printf(ts, "cmd.%u.log_string:%s\n", cnt, c->ts->log_string);
		TAILQ_FOREACH(param, &c->params, tq) {
			transaction_printf(ts, "cmd.%u.param.%d.key:%s\n", cnt, pcnt, param->key);

			if (param->value) {
				if (strpbrk(param->value, "\n\r")) {
					if (printblobs)
						transaction_printf(ts, "blob:%zu:cmd.%u.param.%d.value\n%s\n",
								param->value_size, cnt, pcnt, param->value);
					else
						transaction_printf(ts, "cmd.%u.param.%d.value:blob:%zd\n", cnt,
								pcnt, param->value_size);
				} else
					transaction_printf(ts, "cmd.%u.param.%d.value:%s\n", cnt, pcnt,
							param->value);
			}

			pcnt++;
		}
		cnt++;
	}
	pthread_rwlock_unlock(&pending_commands_lock);
}

void
transaction_threadinfo(struct transaction *ts) {
	struct command_thread *th = NULL;
	uint32_t cnt = 0;

	pthread_mutex_lock(&ct_pool_running_threads_lock);
	transaction_printf(ts, "cmd.threads.count:%u\n", ct_threads_count);
	TAILQ_FOREACH(th, &ct_pool_running_threads, running_threads) {
		transaction_printf(ts, "cmd.threads.%u.id:%p\n", cnt, th);
		if (th->cs) {
			transaction_printf(ts, "cmd.threads.%u.command:%s\n", cnt, th->cs->command->name);
		} else if (th->command) {
			transaction_printf(ts, "cmd.threads.%u.command:%s (exited)\n", cnt, th->command->name);
		} else {
			transaction_printf(ts, "cmd.threads.%u.command:(null)\n", cnt);
		}
		cnt++;
	}
	pthread_mutex_unlock(&ct_pool_running_threads_lock);
}

void
transaction_cmdinfo_log(struct transaction *ts) {
	struct cmd *c;
	int cnt = 0;

	pthread_rwlock_rdlock(&pending_commands_lock);
	TAILQ_FOREACH(c, &pending_commands, cmd_list) {
		transaction_logprintf(ts, D_INFO, "cmd.%u.name:%s", cnt, c->command->name);
		transaction_logprintf(ts, D_INFO, "cmd.%u.status:%s", cnt, c->status);
		transaction_logprintf(ts, D_INFO, "cmd.%u.message:%s", cnt, c->message);
		transaction_logprintf(ts, D_INFO, "cmd.%u.error:%s", cnt, c->error);
		transaction_logprintf(ts, D_INFO, "cmd.%u.transaction:%p", cnt, c->ts);
		cnt++;
	}
	pthread_rwlock_unlock(&pending_commands_lock);
}


/*
 * Only 1 flag is defined so far, BIND_ALLOW_MULTI, which will allow multiple bpapi_insert() calls
 * on the same key, to bind a list. More flags may be added in the future... or not
 */
void 
transaction_bind_params_bpapi(struct cmd *cs, struct bpapi *ba, const char *prefix, int flags) {
	struct cmd_param *cp = NULL;
	const char *param = NULL;
	int len;
	char *buf;

	TAILQ_FOREACH(cp, &cs->params, tq) {
		if (cp->value) {
			if (prefix && *prefix) {
				ALLOCA_PRINTF(len, buf, "%s%s", prefix, cp->key);
				param = buf;
			} else {
				param = cp->key;
			}

			if ((flags & BIND_ALLOW_MULTI) || !bpapi_has_key(ba, param))  {
				bpapi_insert(ba, param, cp->value);
			}
		}
	}
}

