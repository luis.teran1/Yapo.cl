#ifdef STATE_LUT_FIRST_PASS
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <time.h>
#include <signal.h>
#include <unistd.h>

#include "trans.h"
#include "util.h"
#include <ctemplates.h>
#include <bconf.h>
#include <event.h>
#include "command.h"

#include "control_action.h"
#endif

#include "state_lut.h"

STATE_ENUM(control) {
	STATE(CONTROL_DONE),
	STATE(CONTROL_ERROR),
	STATE(CONTROL_INIT),
	STATE(CONTROL_READY),
	STATE(CONTROL_RELOAD),
	STATE(CONTROL_PORT_TOGGLE),
	STATE(CONTROL_STOP),
	STATE(CONTROL_ABORT_MODIFY),
};

#ifdef STATE_LUT_FIRST_PASS
#include __FILE__
#else

struct control_state {
	STATE_VAR(control) state;
        char *sql;
	struct cmd *cs;
        struct event timeout_event;
	int abort_modify;
};

struct validator v_control_action[] = {
	VALIDATOR_GPERF_ENUM(control_action, "ERROR_ACTION_INVALID"),
	{0}
};
ADD_VALIDATOR(v_control_action);

static struct c_param parameters[] = {
	{"action", P_REQUIRED, "v_control_action"},
	{NULL, 0}
};

struct sql_worker;

static const char *control_fsm(void *, struct sql_worker *, const char *);

extern int reload_config;
extern pthread_mutex_t reload_config_lock;
extern pthread_cond_t reload_config_signal;

extern int port_toggle;
extern pthread_mutex_t port_toggle_lock;
extern pthread_cond_t port_toggle_signal;

/*****************************************************************************
 * control_done
 * Exit function.
 **********/
static void
control_done(void *v) {
	struct control_state *state = v;
	struct cmd *cs = state->cs;
	free(state);
	cmd_done(cs);
}

/*****************************************************************************
 * control_fsm
 * State machine
 **********/
static const char *
control_fsm(void *v, struct sql_worker *worker, const char *errstr) {
	struct control_state *state = v;
	struct cmd *cs = state->cs;

	/*
	 * General error handling for db errors.
	 */
	if (errstr) {
                transaction_logprintf(cs->ts, D_ERROR, "control_fsm statement failed (%s(%d), %s)",
				      STATE_NAME(control, state->state),
                                      state->state,
                                      errstr);
                cs->status = "TRANS_DATABASE_ERROR";
                cs->message = xstrdup(errstr);
		state->state = CONTROL_ERROR;
        }

	while (1) {
		transaction_logprintf(cs->ts, D_DEBUG, "control_fsm(%p, %p, %p), state=%s(%d)",
				      v, worker, errstr, STATE_NAME(control, state->state), state->state);

		switch (state->state) {
		case CONTROL_INIT:
			switch (lookup_control_action(cmd_getval(cs, "action"), -1)) {
			case CA_READY:
				state->state = CONTROL_READY;
				break;
			case CA_RELOAD:
				state->state = CONTROL_RELOAD;
				break;
			case CA_PORT_TOGGLE:
				state->state = CONTROL_PORT_TOGGLE;
				break;
			case CA_STOP:
				state->state = CONTROL_STOP;
				break;
			case CA_ABORT_ENABLE:
				state->state = CONTROL_ABORT_MODIFY;
				state->abort_modify = 1;
				break;
			case CA_ABORT_DISABLE:
				state->state = CONTROL_ABORT_MODIFY;
				state->abort_modify = 0;
				break;
			case CA_NONE:
				transaction_printf(cs->ts, "action:ERROR_UNKNOWN_ACTION\n");
				state->state = CONTROL_ERROR;
			}
			continue;

                case CONTROL_READY:
			/* If we get here, we are always ready. */
			transaction_printf(cs->ts, "ready:1\n");
			state->state = CONTROL_DONE;
			continue;

                case CONTROL_RELOAD:
			pthread_mutex_lock(&reload_config_lock);
			transaction_printf(cs->ts, "reloading:1\n");
			reload_config = 1;
			raise(SIGHUP); /* Needed to exit event loop */
			do {
				struct timeval tv;
				struct timespec ts;

				event_base_loop(cs->base, EVLOOP_ONCE | EVLOOP_NONBLOCK);

				gettimeofday(&tv, NULL);
				ts.tv_sec = tv.tv_sec + 1;
				ts.tv_nsec = tv.tv_usec * 1000;
				pthread_cond_timedwait(&reload_config_signal, &reload_config_lock, &ts);
			} while (reload_config);
			pthread_mutex_unlock(&reload_config_lock);
                        state->state = CONTROL_DONE;
			continue;

                case CONTROL_PORT_TOGGLE:
			pthread_mutex_lock(&port_toggle_lock);
			transaction_printf(cs->ts, "toggling:1\n");
			port_toggle = 1;
			raise(SIGUSR1); /* Needed to exit event loop */
			do {
				struct timeval tv;
				struct timespec ts;

				event_base_loop(cs->base, EVLOOP_ONCE | EVLOOP_NONBLOCK);

				gettimeofday(&tv, NULL);
				ts.tv_sec = tv.tv_sec + 1;
				ts.tv_nsec = tv.tv_usec * 1000;
				pthread_cond_timedwait(&port_toggle_signal, &port_toggle_lock, &ts);
			} while (port_toggle);
			pthread_mutex_unlock(&port_toggle_lock);
                        state->state = CONTROL_DONE;
			continue;

                case CONTROL_STOP:
			transaction_printf(cs->ts, "stopping:1\n");
			kill(getppid(), SIGTERM);
			return NULL; /* Wait forever (until exit) */

		case CONTROL_DONE:
			control_done(state);
			return NULL;

		case CONTROL_ABORT_MODIFY:
			transaction_printf(cs->ts, "abort_enable:%d\n", state->abort_modify);
			set_xerr_abort(state->abort_modify);
			control_done(state);
			return NULL;

		case CONTROL_ERROR:
			transaction_logprintf(cs->ts, D_ERROR, "Error state");
			control_done(state);
			return NULL;

		default:
			/* MAJOR STATE ERROR */
			cs->status = "TRANS_ERROR";
			transaction_logprintf(cs->ts, D_ERROR, "Unknown state (%d)",
					      state->state);

			control_done(state); /* If this state were run there would be a memory leak */
			return NULL;

		}
	}
}

/*****************************************************************************
 * control
 * Main function that initialise a state-structure and calls the state machine.
 **********/
static void
control(struct cmd *cs) {
	transaction_logprintf(cs->ts, D_DEBUG, "Start control transaction");
	struct control_state *state;
	state = zmalloc(sizeof(*state));
	state->cs = cs;
	state->state = CONTROL_INIT;

	control_fsm(state, NULL, NULL);
}

ADD_COMMAND_FLAGS(control,     /* Name of command */
		NULL,
		NULL,
		control,     /* Main function name */
		parameters,  /* Parameters struct */
		"control command",
		CMD_CONTROL_PORT);/* Command description */

#endif /*STATE_LUT_FIRST_PASS*/
