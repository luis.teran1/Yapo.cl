#ifndef SQL_WORKER_H
#define SQL_WORKER_H

#include <sys/types.h>
#include <sys/time.h>
#include <pthread.h>
#include <stdbool.h>

#include "event.h"
#include "queue.h"

/*
 * Async handling of sql queries.
 */

#define SQLC_MIN_MAXWORKERS 20

#define WORKER_TO_CONN(sw) ((sw)->sw_conn)

/* Connection flags */

#define SQLC_CONTROL	      (1 << 0)
#define SQLC_MASK	      0x000000FF

#define SQLR_ASYNC	      (1 << 8)
#define SQLR_NOAUTOPUT	      (1 << 9)
#define SQLR_MASK	      0x0000FF00

/* Worker flags */
#define SQLW_FINAL_CALLBACK   (1 << 16)
#define SQLW_DONT_REUSE       (1 << 17)

#define SQLW_MASK	      0xFFFF0000

struct sql_worker;
typedef const char *(*sql_query_cb)(void *, struct sql_worker *, const char *errstr); 

enum fixed_query_type {
	QUERY_ROLLBACK = 1,
	QUERY_COMMIT,
};

struct sql_conninfo {
	TAILQ_ENTRY(sql_conninfo) sc_list;
	TAILQ_HEAD(,sql_worker) sc_free_workers;
	TAILQ_HEAD(,sql_worker) sc_busy_workers;
	TAILQ_HEAD(,sql_request) sc_req_todo;
	char *sc_config;
	char *sc_host;
	char *sc_user;
	char *sc_password;
	char *sc_name;
	int sc_port;
	char *sc_socket;
	int sc_flags;
	int sc_nworkers;
	int sc_maxworkers;
	int sc_keepalive;
	char *sc_db_engine;
	char *sc_options;
	void (*sc_init_worker)(struct sql_worker*);
	int sc_suspended;
	int sc_flushing;
	int sc_gc_to;
};

struct sql_request {
	TAILQ_ENTRY(sql_request) sr_list;

	pthread_t thread;
	pthread_mutex_t lock;
	pthread_cond_t signal;

	char *sr_query;
	struct {
		struct event_base *base;
		sql_query_cb cb;
		void *cb_arg;
		struct event ev;
		char *errstr;
	} async;
	char *logstr;

	struct {
		char *template_name;
		char *logstr;
		const char *source_file;
		int source_line;
	} debug;

	int flags;
	time_t start_time;
	struct timer_instance *sr_ti;

	struct sql_worker *sr_sw;
};

struct notice {
	TAILQ_ENTRY(notice) tq;
	char *message;
};

struct sql_worker_async_put
{
	bool put;
	bool tested;
	char *errstr;
};

struct sql_worker {
	TAILQ_ENTRY(sql_worker) sw_list;

	TAILQ_HEAD(,notice) sw_notices;

	void *sw_conn;
	pthread_mutex_t sw_req_lock;
	struct sql_request *sw_req;
	struct sql_conninfo *sw_sc;
	enum {
		ERROR,
		IDLE,
		QUERY_READY,
		QUERY_SENT,
		RESULT_STORED,
		RESULT_FREED,
		RESULT_ERROR,
		RESULT_DONE
	} sw_state;
	int sw_holdcnt;
	int sw_flush;
	time_t sw_gc_ts;

	void *_res;
	void *_row;
	char *sw_errstr;

	struct sql_worker_async_put *async_put;
	TAILQ_HEAD(, sql_request) queued_requests;

	int (*next_row)(struct sql_worker *);
	const char* (*get_value)(struct sql_worker *, int);
	const char* (*get_value_byname)(struct sql_worker *, const char*);
	int (*affected_rows)(struct sql_worker *);
	int (*fields)(struct sql_worker *);
	const char* (*field_name)(struct sql_worker *, int);
	int (*is_null)(struct sql_worker *, int);
	int (*is_array)(struct sql_worker *, int);
	int (*is_null_byname)(struct sql_worker *, const char*);
	int (*rows)(struct sql_worker *);
	const char* (*send_query)(struct sql_worker *);
	const char* (*store_res)(struct sql_worker *);
	void (*free_res)(struct sql_worker *);
	const char* (*connect)(struct sql_worker *);
	void (*free_worker)(struct sql_worker *);
	const char* (*verify_connection)(struct sql_worker *);
	void (*connection_identifier)(struct sql_worker *sw, char *buf, size_t buflen);
	void (*close)(struct sql_worker *);
	int (*get_fd)(struct sql_worker *);
	int (*insert_id)(struct sql_worker *);
	const char* (*sql_errstr)(struct sql_worker *);
	int (*sql_errno)(struct sql_worker *);
	int (*more_results)(struct sql_worker *);
	char* (*read_notify)(struct sql_worker *);
	int (*is_true)(struct sql_worker *, int);
	int (*is_true_byname)(struct sql_worker *, const char *);
};


struct bpapi;

struct sql_worker * sql_worker_find_active(pthread_t t);
void sql_worker_suspend_all(void);
void sql_worker_reenable_all(struct sql_worker *worker);
void sql_worker_flush_all(void);

void sql_worker_base_template_query(const char *config, struct event_base *base, const char *template_name,
			       struct bpapi *ba, sql_query_cb cb, void *cb_arg, const char *logstring);
void sql_worker_base_template_query_flags(const char *config, struct event_base *base, int flags, const char *template_name,
			       struct bpapi *ba, sql_query_cb cb, void *cb_arg, const char *logstring);
void sql_worker_template_query(const char *config, const char *template_name,
			       struct bpapi *ba, sql_query_cb cb, void *cb_arg, const char *logstring);
void sql_worker_template_query_flags(const char *config, int flags, const char *template_name,
			       struct bpapi *ba, sql_query_cb cb, void *cb_arg, const char *logstring);
struct sql_worker *sql_blocked_template_query(const char *config, int flags, const char *template_name,
		struct bpapi *ba, const char *logstring, const char **errstr);

int sql_blocked_next_result(struct sql_worker *worker, const char **errstr);

extern int sql_worker_gc_timeout;
struct sql_worker *sql_worker_hold(struct sql_worker *);
void sql_worker_put(struct sql_worker *);

/* For fixed and _common_ queries such as ROLLBACK */
int sql_worker_newquery(struct sql_worker *sw, enum fixed_query_type query, const char **errstr);

void sql_worker_template_newquery(struct sql_worker *sw, const char *query_template, struct bpapi *ba);
int sql_blocked_template_newquery(struct sql_worker *sw, const char *query_template, struct bpapi *ba, const char **errstr);

void sql_worker_set_wflags(struct sql_worker *sw, int wflags);

void sql_worker_bpa_insert(struct sql_worker *sw, struct bpapi *ba, const char *prefix);

#define is_defined_value(d_worker, d_index) \
    (((d_index) < (d_worker)->fields(d_worker)) && !((d_worker)->is_null(d_worker, d_index)))

#define safe_get_value_with_default_return_value(d_worker, d_index, d_default_value) \
    (((d_index) < (d_worker)->fields(d_worker)) ? (((d_worker)->get_value(d_worker, d_index)) ?: (d_default_value)) : (d_default_value))

#define safe_get_value(d_worker, d_index) \
	safe_get_value_with_default_return_value(d_worker, d_index, "")

#define safe_get_value_return_null(d_worker, d_index) \
	safe_get_value_with_default_return_value(d_worker, d_index, NULL)

#endif
