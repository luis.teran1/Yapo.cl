#include "factory.h"

static struct c_param parameters[] = {
	{"id",    P_REQUIRED,  "v_integer"},
	{NULL, 0}
};

static struct factory_data data = {
	"factory_example",
	FACTORY_NO_RES_CNTR | FACTORY_RES_GLOBAL_ROW
};

static const struct factory_action actions[] = {
	FA_SQL(NULL, "pgsql.slave", "sql/factory_example.sql", FASQL_OUT),
	{0}
};

FACTORY_TRANS(factory_example, parameters, data, actions);

/*
 * Creates a transaction "factory_example" that takes all the parameters,
 * inserts them into template and runs sql template.
 * FASQL_OUT flag makes it print like this:
 *
 *      factory_example.0.id:3
 *      factory_example.0.col:14
 *      factory_example.1.col:14
 *	status:TRANS_OK
 *	end
 */
