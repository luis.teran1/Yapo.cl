#ifndef TRANS_AT_H
#define TRANS_AT_H

#include "command.h"
#include "trans.h"
#include "util.h"
#include "bconf.h"
#include "trans_mail.h"
#include <pthread.h>

/* Default values */
#define DFLT_AT_MAX_EXECUTE	5
#define DFLT_AT_MAX_REGISTER 10
#define AT_DB 0
#define AT_REDIS 1

/* status         | character varying(80)       | default NULL::character varying */
#define PGSQL_TRANS_QUEUE_STATUS_SIZE	80

struct at {
	TAILQ_ENTRY(at) at_list;
	TAILQ_ENTRY(at) at_done_list;
	TAILQ_ENTRY(at) at_redis_list;
	TAILQ_ENTRY(at) at_redis_job_list;

	pthread_t thread;
	struct at_reg_status *reg_status;

	char *cmd;
	struct timeval timeout;
	time_t execute_at;
	struct event ev;
	char *status;
	int use_redis;
	int len;
	int pos;
	char *res;
	long id;
	int executing;
	int critical;
	char *sub_queue;
	char *info;
};

void at_register(struct at *at, const char *sub_queue, const char *info);
long at_register_sync(struct at *at, const char *sub_queue, const char *info);
void at_reset(struct cmd *cs, const char *action);
void at_register_storage(const char *cmd, int seconds, const char *sub_queue, const char *info, int critical, int storage);
void at_register_storage_printf(int seconds, const char *sub_queue, const char *info, int critical, int storage,
	       	const char *cmdfmt, ...) __attribute__((format (printf, 6, 7)));
int at_clear_commands(const char *sub_queue, const char *info, const char *logstring);

/* macros for backwards compatibility */
#define at_register_cmd(cmd, seconds, sub_queue, info, critical) \
at_register_storage(cmd, seconds, sub_queue, info, critical, AT_DB);

#define at_register_redis_cmd(cmd, seconds, sub_queue, info, critical) \
at_register_storage(cmd, seconds, sub_queue, info, critical, AT_REDIS);

#define at_register_printf(seconds, sub_queue, info, critical, ...) \
at_register_storage_printf(seconds, sub_queue, info, critical, AT_DB, __VA_ARGS__);

#define at_register_redis_printf(seconds, sub_queue, info, critical, ...) \
at_register_storage_printf(seconds, sub_queue, info, critical, AT_REDIS, __VA_ARGS__);

void transaction_at_info(struct transaction *ts);

#endif
