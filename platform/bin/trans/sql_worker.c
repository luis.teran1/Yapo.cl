
#include "sql_worker.h"
#include "pgsql_api.h"
#include "trans.h"
#include "verify_proc_checksums.h"

#include "bconf.h"
#include "util.h"
#include "bpapi.h"
#include "ctemplates.h"
#include "timer.h"
#include "command.h"

#include <pthread.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <netinet/tcp.h>
#include <stdbool.h>
#include <assert.h>

int sql_worker_gc_timeout = 0;

extern struct bconf_node *trans_conf;
extern int use_stdin;
extern int keepalive;

static TAILQ_HEAD(,sql_conninfo) db_conns = TAILQ_HEAD_INITIALIZER(db_conns);
static pthread_mutex_t db_conns_lock = PTHREAD_MUTEX_INITIALIZER;
static pthread_cond_t db_conns_suspended_signal = PTHREAD_COND_INITIALIZER;
static int db_conns_suspending = 0;

static pthread_t sql_worker_gc_tid;
static bool sql_worker_gc_running;

static void prod_connection(struct sql_conninfo *sc);
static int sql_worker_gc(struct sql_worker *sw);
static int sql_worker_newquery_string(struct sql_worker *sw, char *query, const char **errstr);

static const char*
sql_worker_get_state(int state) {
	switch (state) {
	case ERROR:
		return "ERROR";
	case IDLE:
		return "IDLE";
	case QUERY_READY:
		return "QUERY_READY";
	case QUERY_SENT:
		return "QUERY_SENT";
	case RESULT_STORED:
		return "RESULT_STORED";
	case RESULT_FREED:
		return "RESULT_FREED";
	case RESULT_ERROR:
		return "RESULT_ERROR";
	case RESULT_DONE:
		return "RESULT_DONE";
	default:
		return "UNKNOWN";
	}
}

struct sql_worker *
sql_worker_find_active(pthread_t t) {
	struct sql_conninfo *sc;
	struct sql_worker *sw = NULL;
	pthread_mutex_lock(&db_conns_lock);
	TAILQ_FOREACH(sc, &db_conns, sc_list) {
		TAILQ_FOREACH(sw, &sc->sc_busy_workers, sw_list) {
			pthread_mutex_lock(&sw->sw_req_lock);
			if (sw->sw_req && pthread_equal(sw->sw_req->thread, t)) {
				pthread_mutex_unlock(&sw->sw_req_lock);
				break;
			}
			pthread_mutex_unlock(&sw->sw_req_lock);
		}
		if (sw)
			break;
	}
	pthread_mutex_unlock(&db_conns_lock);
	return sw;
}

/* 
 * Free any debug information associated with the worker
 */
static void
sql_worker_debug_free(struct sql_request *sr) {
	if (!sr)
		return;

	free(sr->debug.template_name);
	free(sr->debug.logstr);
	sr->debug.template_name = NULL;
	sr->debug.logstr = NULL;
}

static void
sql_worker_debug_reset(struct sql_worker *sw, const char *tmpl_name) {
	struct sql_request *sr = sw->sw_req;

	if (!sr)
		return;

	/* keep logstr as it was. */
	char *logstr = xstrdup(sr->debug.logstr);
	sql_worker_debug_free(sr);
	sr->debug.template_name = xstrdup(tmpl_name);
	sr->debug.source_file = "<none>";
	sr->debug.source_line = 0;
	sr->debug.logstr = logstr;
}

/* You must hold db_conns_lock when calling this function. */
static void
sc_suspended(struct sql_conninfo *sc) {
	if (!TAILQ_EMPTY(&sc->sc_busy_workers) || !TAILQ_EMPTY(&sc->sc_free_workers))
		return;

	if (db_conns_suspending == 1)
		return;

	pthread_cond_broadcast(&db_conns_suspended_signal);
}

void
sql_worker_suspend_all(void) {
	struct sql_conninfo *sc;

	pthread_mutex_lock(&db_conns_lock);

	/* First mark all db conns as suspended */
	db_conns_suspending = 1;
	TAILQ_FOREACH(sc, &db_conns, sc_list) {
		if (sc->sc_flags & SQLC_CONTROL)
			continue;
		sc->sc_suspended = 1;
		prod_connection(sc);
	}
	db_conns_suspending = 2;

	/* Now wait for them to finish processing */
	while (1) {
		TAILQ_FOREACH(sc, &db_conns, sc_list) {
			if (!(sc->sc_flags & SQLC_CONTROL) && (!TAILQ_EMPTY(&sc->sc_busy_workers) || !TAILQ_EMPTY(&sc->sc_free_workers)))
				break;
		}
		if (sc == TAILQ_END(&db_conns))
			break;
		pthread_cond_wait(&db_conns_suspended_signal, &db_conns_lock);
	}

	pthread_mutex_unlock(&db_conns_lock);
}

void
sql_worker_reenable_all(struct sql_worker *worker) {
	struct sql_conninfo *sc;
	struct sql_conninfo *wsc = worker->sw_sc;

	pthread_mutex_lock(&db_conns_lock);
	db_conns_suspending = 0;
	TAILQ_FOREACH(sc, &db_conns, sc_list) { 
		if (sc != wsc) {
			sc->sc_suspended = 0;
			prod_connection(sc);
		} else {
			sc->sc_gc_to = 0;
		}
	}
	pthread_mutex_unlock(&db_conns_lock);
}

void
sql_worker_flush_all(void) {
	struct sql_conninfo *sc;
	struct sql_worker *sw;

	pthread_mutex_lock(&db_conns_lock);
	TAILQ_FOREACH(sc, &db_conns, sc_list) {
		TAILQ_FOREACH(sw, &sc->sc_busy_workers, sw_list) {
			sw->sw_flush = 1;
		}
		sc->sc_flushing = 1;
		prod_connection(sc);
		sc->sc_flushing = 0;
	}
	pthread_mutex_unlock(&db_conns_lock);
}

/* Note: you must hold the db_conns_lock when calling this function. */
static struct sql_conninfo *
get_connection(const char *config, int flags) {
	struct sql_conninfo *sc;
	struct bconf_node *config_node = bconf_vget(trans_conf, "db", config, NULL);

	assert(config);

	if (!config_node)
		xerrx(1, "SQL worker tried to use unconfigured db node: %s", config);

	TAILQ_FOREACH(sc, &db_conns, sc_list) {
		if (!strcmp(sc->sc_config, config) &&
		    sc->sc_flags == flags) {
			return (sc);
		}
	}

	sc = zmalloc(sizeof(*sc));
	sc->sc_config = xstrdup(config);
	sc->sc_host = xstrdup(bconf_get_string(config_node, "hostname") ?: "");
	sc->sc_user = xstrdup(bconf_get_string(config_node, "user") ?: "");
	sc->sc_password = xstrdup(bconf_get_string_default(config_node, "password", ""));
	sc->sc_name = xstrdup(bconf_get_string(config_node, "name") ?: "");
	sc->sc_db_engine = xstrdup(bconf_get_string(config_node, "engine") ?: "PGSQL");
	sc->sc_options = xstrdup(bconf_get_string(config_node, "options") ?: "");
	sc->sc_gc_to = sql_worker_gc_timeout;
	sc->sc_maxworkers = bconf_get_int(config_node, "max_workers");
	if (sc->sc_maxworkers < SQLC_MIN_MAXWORKERS)
		sc->sc_maxworkers = SQLC_MIN_MAXWORKERS;
	sc->sc_keepalive = bconf_get_int(config_node, "keepalive");

	if (bconf_get(config_node, "socket"))
		sc->sc_socket = xstrdup(bconf_get_string(config_node, "socket"));
	else
		sc->sc_socket = NULL;

	sc->sc_port = bconf_get_int(config_node, "port");
	sc->sc_flags = flags;

	TAILQ_INIT(&sc->sc_free_workers);
	TAILQ_INIT(&sc->sc_busy_workers);
	TAILQ_INIT(&sc->sc_req_todo);

	TAILQ_INSERT_TAIL(&db_conns, sc, sc_list);

	sc->sc_nworkers = 0;

	if (strcmp(sc->sc_db_engine, "PGSQL") == 0)
		sc->sc_init_worker = pgsql_init_worker;
	else
		xerrx(1, "No such db_engine '%s'", sc->sc_db_engine);

	if (db_conns_suspending == 2 && !(flags & SQLC_CONTROL))
		sc->sc_suspended = 1;

	return (sc);
}

/* NOTE: you have to hold the db_conns_lock when calling this function */
static void
connection_queue_for_current_thread(struct sql_conninfo *sc) {
	pthread_t thread = pthread_self();
	struct sql_request *sr, *nsr;
	struct sql_worker *sw;

	for (sr = TAILQ_FIRST(&sc->sc_req_todo) ; sr != TAILQ_END(&sc->sc_req_todo) ; sr = nsr) {
		nsr = TAILQ_NEXT(sr, sr_list);

		if ((sr->flags & SQLR_ASYNC) == 0 || !pthread_equal(sr->thread, thread))
			continue;

		TAILQ_FOREACH(sw, &sc->sc_busy_workers, sw_list) {
			pthread_mutex_lock(&sw->sw_req_lock);
			if (!sw->sw_req || (sw->sw_req->flags & SQLR_ASYNC) == 0) {
				pthread_mutex_unlock(&sw->sw_req_lock);
				continue;
			}
			if (sw->sw_req->async.base == sr->async.base) {
				pthread_mutex_unlock(&sw->sw_req_lock);
				break;
			}
			pthread_mutex_unlock(&sw->sw_req_lock);
		}
		if (!sw)
			break;

		DPRINTF(D_WARNING, ("(WARN) (%s) Queued SQL request on busy worker", sr->logstr));

		TAILQ_REMOVE(&sc->sc_req_todo, sr, sr_list);
		TAILQ_INSERT_TAIL(&sw->queued_requests, sr, sr_list);

		/* Cycle the worker list. */
		TAILQ_REMOVE(&sc->sc_busy_workers, sw, sw_list);
		TAILQ_INSERT_TAIL(&sc->sc_busy_workers, sw, sw_list);

		pthread_mutex_lock(&sr->lock);
		sr->sr_sw = sw;
		pthread_mutex_unlock(&sr->lock);

		/* Shouldn't really be needed */
		pthread_cond_signal(&sr->signal);
	}
}

/* 
 * NOTE: you have to hold the db_conns_lock when calling this function 
 * 
 * The workers are collected into an array because if db_conns_lock
 * is released in the loop of db_conns, it is possible that sc_list gets
 * mutated, and the next iteration of db_conns will dereference an invalid
 * pointer to sql_conninfo with hilarious results 
 *
 */
static void
connection_autoput_for_current_thread(void) {
	pthread_t thread = pthread_self();
	struct sql_conninfo *sc;
	struct sql_worker *sw;
	struct sql_worker **put_canditates = NULL;
	int i = 0, alloced = 8;
	
	put_canditates = xmalloc(alloced * sizeof(struct sql_worker *)); 

	/* Scan for workers we need to put asynchronously and put them. */
	TAILQ_FOREACH(sc, &db_conns, sc_list) {
		TAILQ_FOREACH(sw, &sc->sc_busy_workers, sw_list) {
			pthread_mutex_lock(&sw->sw_req_lock);
			if (sw->async_put && sw->sw_req && pthread_equal(sw->sw_req->thread, thread)) {
				pthread_mutex_unlock(&sw->sw_req_lock);
				put_canditates[i++] = sw;
				if (i == alloced) {
					alloced *= 2;
					put_canditates = xrealloc(put_canditates, alloced * sizeof(struct sql_worker *));	
				}
				break;
			}
			pthread_mutex_unlock(&sw->sw_req_lock);
		}
	}

	while (i > 0) {
		pthread_mutex_unlock(&db_conns_lock);
		sql_worker_put(put_canditates[--i]);
		pthread_mutex_lock(&db_conns_lock);
	}

	free(put_canditates);

}

/*
 * Prod (as in cattle-prod) the connection to start processing requests on
 * the todo list.
 * NOTE: You need to hold the db_conns_lock when calling this function.
 */
static void
prod_connection(struct sql_conninfo *sc) {
	struct sql_worker *sw;
	struct sql_request *sr;

	if (sc->sc_flushing == 1) {
		while (!TAILQ_EMPTY(&sc->sc_free_workers))
			sql_worker_gc(TAILQ_FIRST(&sc->sc_free_workers));
		return;
	}

	if (sc->sc_suspended) {
		/*
		 * If there is a pending request on the same thread as any busy request,
		 * we probably need to run it to unblock the busy one.
		 * Exhaustive search, but small numbers.
		 */
		sw = NULL;
		TAILQ_FOREACH(sr, &sc->sc_req_todo, sr_list) {
			struct sql_conninfo *isc;

			TAILQ_FOREACH(isc, &db_conns, sc_list) {
				TAILQ_FOREACH(sw, &isc->sc_busy_workers, sw_list) {
					pthread_mutex_lock(&sw->sw_req_lock);
					if (sw->sw_req && pthread_equal(sw->sw_req->thread,sr->thread)) {
						pthread_mutex_unlock(&sw->sw_req_lock);
						break;
					}
					pthread_mutex_unlock(&sw->sw_req_lock);
				}
				if (sw)
					break;
			}
			if (sw)
				break;
		}

		if (!sw) {
			while (!TAILQ_EMPTY(&sc->sc_free_workers))
				sql_worker_gc(TAILQ_FIRST(&sc->sc_free_workers));
			if (TAILQ_EMPTY(&sc->sc_busy_workers))
				sc_suspended(sc);
			return;
		}
	}

	/*
	 * See if there is any request for us to handle.
	 */
	if ((sr = TAILQ_FIRST(&sc->sc_req_todo)) == NULL) {
		return;
	}

	/*
	 * Check if we've already allocated too many workers and there are no free.
	 */
	if (sc->sc_nworkers > sc->sc_maxworkers && TAILQ_EMPTY(&sc->sc_free_workers)) {
		connection_queue_for_current_thread(sc);
		return;
	}

	TAILQ_REMOVE(&sc->sc_req_todo, sr, sr_list);

	/*
	 * We are now commited to handle this request or fail trying.
	 */
	if ((sw = TAILQ_FIRST(&sc->sc_free_workers)) == NULL) {
		sw = zmalloc(sizeof(*sw));

		TAILQ_INIT(&sw->sw_notices);
		sc->sc_init_worker(sw);
		sw->sw_sc = sc;
		sw->_res = NULL;
		TAILQ_INIT(&sw->queued_requests);
		pthread_mutex_init(&sw->sw_req_lock, NULL);

		sc->sc_nworkers++;
		TAILQ_INSERT_TAIL(&sc->sc_busy_workers, sw, sw_list);
	} else {
		TAILQ_REMOVE(&sc->sc_free_workers, sw, sw_list);
		TAILQ_INSERT_TAIL(&sw->sw_sc->sc_busy_workers, sw, sw_list);
	}

	sw->sw_holdcnt = 1;

	pthread_mutex_lock(&sr->lock);
	pthread_mutex_lock(&sw->sw_req_lock);
	sw->sw_req = sr;
	pthread_mutex_unlock(&sw->sw_req_lock);
	sr->sr_sw = sw;
	pthread_mutex_unlock(&sr->lock);
	pthread_cond_signal(&sr->signal);
}

/* Note: you must hold the db_conns_lock when calling this function. */
static int
put_connection(struct sql_conninfo *sc) {
	if (sc->sc_suspended || sc->sc_flushing == 1)
	       return 1;

	if (!TAILQ_EMPTY(&sc->sc_free_workers) || !TAILQ_EMPTY(&sc->sc_busy_workers) || !TAILQ_EMPTY(&sc->sc_req_todo)) {
		prod_connection(sc);
		return 1;
	}

	TAILQ_REMOVE(&db_conns, sc, sc_list);

	free(sc->sc_config);
	free(sc->sc_host);
	free(sc->sc_user);
	free(sc->sc_password);
	free(sc->sc_name);
	free(sc->sc_socket);
	free(sc->sc_db_engine);
	free(sc->sc_options);
	free(sc);
	return 0;
}

static void
sql_worker_async_cb(int fd, short ev, void *v) {
	struct sql_request *sr = v;

	if (sr->async.cb)
		(*sr->async.cb)(sr->async.cb_arg, NULL, sr->async.errstr);

	free(sr->async.errstr);
	free(sr->logstr);
	free(sr->sr_query);
	free(sr);
	DPRINTF(D_DEBUG, ("Freed sql request %p", sr));
}

static void
sql_worker_event_read(int fd, short event, void *v) {
	struct sql_worker *sw = v;
	struct sql_request *sr = sw->sw_req;
	int query_held = 0;
	const char *next_query = NULL;
	char *errstr = NULL;
	struct sql_worker_async_put async_put = { false, false, NULL };

	if (sw->sw_state != QUERY_SENT) {
		xwarnx("sql_worker: weird event in sql_worker_read_event. event=%d, state=%s(%d)", event, sql_worker_get_state(sw->sw_state), sw->sw_state);
		return;
	}

	DPRINTF(D_DEBUG, ("(%s) read_result", sr->logstr));
	if (debug_level == D_DEBUG)
		DPRINTF(D_DEBUG, ("(%s) spent %ld seconds waiting for query", sr->logstr, time(NULL) - sr->start_time));

	sw->async_put = &async_put;
	do {
		if (async_put.tested) {
			errstr = async_put.errstr;
			async_put.tested = false;
			async_put.errstr = NULL;
		} else {
			const char *e = sw->store_res(sw);

			if (e && !errstr) {
				errstr = xstrdup(e);
			}
		}
		DPRINTF(D_DEBUG, ("(%s) worker %p store_res: %s, state=%s(%u)", sr->logstr, sw, errstr, sql_worker_get_state(sw->sw_state), sw->sw_state));

		if (errstr) {
			if (sw->sw_state != RESULT_DONE && sw->more_results(sw)) {
				sw->free_res(sw);
				continue;
			}
			if (sr->async.cb)
				next_query = (*sr->async.cb)(sr->async.cb_arg, sw, errstr);
			if (!async_put.put && sw->sw_state == QUERY_READY)
				query_held = 1;
			break;
		}

		/* Nothing left to read */
		if (sw->sw_state == RESULT_DONE) {
			break;
		} else if (next_query || query_held) {
			sw->free_res(sw);
			DPRINTF(D_WARNING, ("(%s) worker %p sql-commands out of sync (%s)", sr->logstr, sw, next_query));
			next_query = NULL;
			query_held = 0;
			if (sr->async.cb)
				(*sr->async.cb)(sr->async.cb_arg, NULL, "sql-commands out of sync");
			break;
		} else {
			if (sr->async.cb)
				next_query = (*sr->async.cb)(sr->async.cb_arg, sw, NULL);
			if (!async_put.put) {
				if (sw->sw_state == QUERY_READY)
					query_held = 1;
				if (!async_put.tested)
					sw->free_res(sw);
			}
		}
	} while (!async_put.put && sw->more_results(sw));

	/* If requested, and no new query or error, callback one more time. */
	if (!async_put.put && (sr->flags & SQLW_FINAL_CALLBACK) && !errstr && !next_query && !query_held) {
		if (sr->async.cb)
			next_query = (*sr->async.cb)(sr->async.cb_arg, sw, NULL);
		if (!async_put.put && sw->sw_state == QUERY_READY)
			query_held = 1;
	}

	if (errstr) {
		free(errstr);
		errstr = NULL;
	}

	if (async_put.put) {
		DPRINTF(D_DEBUG, ("worker %p was put async", sw));
		return;
	}
	sw->async_put = NULL;

	if (query_held) {
		DPRINTF(D_DEBUG, ("(%s) worker %p query held", sr->logstr, sw));
		errstr = (char*)sw->send_query(sw);
		sw->sw_state = QUERY_SENT;
	} else if (sw->sw_state == QUERY_SENT) {
		DPRINTF(D_DEBUG, ("(%s) worker %p query sent by final callback", sr->logstr, sw));
	} else if (next_query == NULL) {
		DPRINTF(D_DEBUG, ("(%s) worker %p put worker", sr->logstr, sw));
		sql_worker_put(sw);
		DPRINTF(D_DEBUG, ("put worker done"));
	} else {
		DPRINTF(D_DEBUG, ("(%s) worker %p New immediate sql-query", sr->logstr, sw));
		sql_worker_newquery_string(sw, xstrdup(next_query), (const char**)&errstr);
	}

	if (errstr) {
		if (sr->async.cb)
			(*sr->async.cb)(sr->async.cb_arg, NULL, errstr);
		sql_worker_put(sw);
	}
}

static char *
sql_worker_call_template(struct sql_worker *sw, const char *template_name, struct bpapi *ba, const char *logstr) {
	struct buf_string *sql;
	struct command_bpapi cba;
	char *res = NULL;

	if (!ba) {
		command_bpapi_init(&cba, NULL, CMD_BPAPI_HOST, NULL);
		ba = &cba.ba;
	}

	sql = BPAPI_OUTPUT_DATA(ba);

	/* sw->init_ba(ba); */
	pgsql_simplevars_setconn(&ba->schain, sw->sw_conn);

	/* Set up debugging information */
	if (sw && sw->sw_req) {
		pthread_mutex_lock(&sw->sw_req_lock);
		free(sw->sw_req->debug.logstr);
		free(sw->sw_req->debug.template_name);
		sw->sw_req->debug.template_name = xstrdup(template_name);
		sw->sw_req->debug.logstr = xstrdup(logstr);
		sw->sw_req->debug.source_file = ba->debug.source_file ?: "<unknown>";
		sw->sw_req->debug.source_line = ba->debug.source_line;
		pthread_mutex_unlock(&sw->sw_req_lock);
	}

	if (!call_template(ba, template_name))
		DPRINTF(D_ERROR, ("(CRIT) (%s) tried to call nonexisting template: %s", logstr, template_name));
	else if (sql->buf == NULL) {
		DPRINTF(D_WARNING, ("(WARN) (%s) NO SQL query generated from template: %s", logstr, template_name));
	}

	/* sw->deinit_ba(ba); */
	pgsql_simplevars_setconn(&ba->schain, NULL);

	if (sql->buf) {
		if (debug_level == D_DEBUG) {
			pthread_mutex_lock(&sw->sw_req_lock);
			DPRINTF(D_DEBUG, ("(%s) SQL query generated from template %s (BPAPI_INIT at %s:%d): %s", 
					  logstr,
					  template_name, 
					  (sw && sw->sw_req) ? sw->sw_req->debug.source_file : "<unknown>",
					  (sw && sw->sw_req) ? sw->sw_req->debug.source_line : 0,
					  sql->buf));
			pthread_mutex_unlock(&sw->sw_req_lock);
		}
		sql->pos = sql->len = 0;
		res = sql->buf;
		sql->buf = NULL;
	}

	if (ba == &cba.ba) {
		command_bpapi_free(&cba);
	}

	return res;
}

static const char *
sql_worker_connect(struct sql_worker *sw) {
	struct sql_conninfo *sc = sw->sw_sc;
	const char *errstr;

	if (sw->sw_conn)
		errstr = sw->verify_connection(sw);
	else {
		struct timer_instance *ti = timer_start(NULL, "sql_worker_connect");
		errstr = sw->connect(sw);
		timer_end(ti, NULL);
	}

	if (errstr)
		return errstr;

	if (sc->sc_keepalive) {
		int fd = sw->get_fd(sw);

		/* Ignore errors, might not be a TCP socket. */
		setsockopt(fd, SOL_SOCKET, SO_KEEPALIVE, &(int){ 1 }, sizeof(int));
		setsockopt(fd, SOL_TCP, TCP_KEEPIDLE, &sc->sc_keepalive, sizeof(sc->sc_keepalive));
	}

	return NULL;
}

static void
sql_worker_assigned(struct sql_request *sr, const char *template_name, struct bpapi *ba) {
	struct sql_worker *sw = sr->sr_sw;
	const char *errstr;
	char connid[256];

	if (debug_level == D_DEBUG)
		DPRINTF(D_DEBUG, ("(%s) spent %ld seconds waiting to execute query", sr->logstr, time(NULL) - sr->start_time));

	errstr = sql_worker_connect(sw);

	if (errstr) {
		DPRINTF(D_WARNING, ("(%s) connect failed: %s", sr->logstr, errstr));
		sr->async.errstr = xstrdup(errstr);

		if (sr->async.base)
			event_base_once(sr->async.base, -1, EV_TIMEOUT, sql_worker_async_cb, sr, NULL);
		else
			event_once(-1, EV_TIMEOUT, sql_worker_async_cb, sr, NULL);

		event_del(&sw->sw_req->async.ev);
		pthread_mutex_lock(&sw->sw_req_lock);
		/* free debug info here since the request is detached from the worker */
		sql_worker_debug_free(sw->sw_req);	
		sw->sw_req = NULL;
		pthread_mutex_unlock(&sw->sw_req_lock);
		sql_worker_put(sw);
		return;
	}

	if (!sr->sr_query)
		sr->sr_query = sql_worker_call_template(sw, template_name, ba, sr->logstr);

	sw->connection_identifier(sw, connid, sizeof (connid));
	DPRINTF(D_INFO, ("%s: connection identifier %s", sr->logstr, connid));

	/* Using a blocking write here. Probably fine. */
	errstr = sw->send_query(sw);
	if (errstr) {
		DPRINTF(D_WARNING, ("(%s) send_query failed: %s", sr->logstr, errstr));
		sr->async.errstr = xstrdup(errstr);
		if (sr->async.base)
			event_base_once(sr->async.base, -1, EV_TIMEOUT, sql_worker_async_cb, sr, NULL);
		else
			event_once(-1, EV_TIMEOUT, sql_worker_async_cb, sr, NULL);

		event_del(&sr->async.ev);
		pthread_mutex_lock(&sw->sw_req_lock);
		/* free debug info here since the request is detached from the worker */
		sql_worker_debug_free(sw->sw_req);
		sw->sw_req = NULL;
		pthread_mutex_unlock(&sw->sw_req_lock);
		sql_worker_put(sw);
		return;
	}
	sw->sw_state = QUERY_SENT;

	/* XXX this event really belongs to the worker rather than the sr */
	event_set(&sr->async.ev, sw->get_fd(sw), EV_READ|EV_PERSIST, sql_worker_event_read, sw);
	if (sr->async.base)
		event_base_set(sr->async.base, &sr->async.ev);
	event_add(&sr->async.ev, NULL);
}

static struct sql_request *
get_request(const char *config, int flags, const char *logstr, struct event_base *base) {
	struct sql_request *sr;
	struct sql_conninfo *sc;

	sr = zmalloc(sizeof(*sr));
	sr->logstr = xstrdup(logstr);
	sr->flags = flags;

	sr->start_time = time(NULL);

	sr->thread = pthread_self();
	pthread_mutex_init(&sr->lock, NULL);
	pthread_cond_init(&sr->signal, NULL);

	/* Needed for queuing */
	sr->async.base = base;

	pthread_mutex_lock(&db_conns_lock);
	sc = get_connection(config, flags & SQLC_MASK);

	TAILQ_INSERT_TAIL(&sc->sc_req_todo, sr, sr_list);

	if ((flags & (SQLR_ASYNC | SQLR_NOAUTOPUT)) == SQLR_ASYNC) {
		connection_autoput_for_current_thread();
	}

	prod_connection(sc);
	pthread_mutex_unlock(&db_conns_lock);

	/* Wait for a worker to be assigned. */
	pthread_mutex_lock(&sr->lock);
	while (!sr->sr_sw)
		pthread_cond_wait(&sr->signal, &sr->lock);
	pthread_mutex_unlock(&sr->lock);

	return sr;
}

void
sql_worker_base_template_query(const char *config, struct event_base *base, const char *template_name,
		struct bpapi *ba, sql_query_cb cb, void *cb_arg, const char *logstring) {
	sql_worker_base_template_query_flags(config, base, 0, template_name, ba, cb, cb_arg, logstring);
}

void
sql_worker_base_template_query_flags(const char *config, struct event_base *base, int flags, const char *template_name,
		struct bpapi *ba, sql_query_cb cb, void *cb_arg, const char *logstring) {
	struct sql_request *sr = get_request(config, flags | SQLR_ASYNC, logstring, base);

	sr->async.cb = cb;
	sr->async.cb_arg = cb_arg;

	if (sr->sr_sw->sw_req == sr)
		sql_worker_assigned(sr, template_name, ba);
	else
		sr->sr_query = sql_worker_call_template(sr->sr_sw, template_name, ba, logstring);
}

void
sql_worker_template_query(const char *config, const char *template_name,
		struct bpapi *ba, sql_query_cb cb, void *cb_arg, const char *logstring) {
	extern pthread_key_t csbase_key;
	struct event_base *base = pthread_getspecific(csbase_key);

	if (!base)
		xerr(1, "no csbase for sql_worker_template_query");
	return sql_worker_base_template_query(config, base, template_name, ba, cb, cb_arg, logstring);
}

void
sql_worker_template_query_flags(const char *config, int flags, const char *template_name,
		struct bpapi *ba, sql_query_cb cb, void *cb_arg, const char *logstring) {
	extern pthread_key_t csbase_key;
	struct event_base *base = pthread_getspecific(csbase_key);

	if (!base)
		xerr(1, "no csbase for sql_worker_template_query_flags");
	return sql_worker_base_template_query_flags(config, base, flags, template_name, ba, cb, cb_arg, logstring);
}

struct sql_worker *
sql_blocked_template_query(const char *config, int flags, const char *template_name,
		struct bpapi *ba, const char *logstring, const char **errstr) {
	struct sql_request *sr = get_request(config, flags & ~SQLR_ASYNC, logstring, NULL);
	struct sql_worker *sw = sr->sr_sw;
	const char *err;
	char connid[256];

	err = sql_worker_connect(sw);

	sr->sr_query = sql_worker_call_template(sr->sr_sw, template_name, ba, logstring);

	sr->sr_sw->connection_identifier(sr->sr_sw, connid, sizeof (connid));
	DPRINTF(D_INFO, ("%s: connection identifier %s", sr->logstr, connid));

	if (!err)
		err = sw->send_query(sw);

	if (err) {
		sql_worker_put(sw);
		if (errstr)
			*errstr = err;
		return NULL;
	}

	return sw;
}

int
sql_blocked_next_result(struct sql_worker *sw, const char **errstr) {
	sw->free_res(sw);
	const char *err = sw->store_res(sw);
	if (err) {
		DPRINTF(D_INFO, ("sql_blocked_next_result(%s) : %s", sw->sw_req->logstr, err));
		if (errstr)
			*errstr = err;
		return -1;
	}

	return sw->sw_state == RESULT_DONE ? 0 : 1;
}

struct sql_worker *
sql_worker_hold(struct sql_worker *sw) {
	sw->sw_holdcnt++;
	return sw;
}

void
sql_worker_put(struct sql_worker *sw) {
	struct notice *note;

	/* Clear notices */
	while ((note = TAILQ_FIRST(&sw->sw_notices)) != NULL) {

		TAILQ_REMOVE(&sw->sw_notices, note, tq);
		free(note);
	}

	sw->sw_state = IDLE;
	if (--sw->sw_holdcnt > 0)
		return;

	if (sw->async_put) {
		if (sw->async_put->tested)
			return;
		sw->async_put->tested = true;
		sw->free_res(sw);
		if (sw->more_results(sw)) {
			const char *e = sw->store_res(sw);
			if (e)
				sw->async_put->errstr = xstrdup(e);
		}
		if (sw->sw_state != RESULT_DONE)
			return;
		sw->async_put->put = true;
		sw->async_put = NULL;
	}

	if (sw->sw_req) {
		if (!(sw->sw_req->flags & SQLR_ASYNC)) {
			while (sql_blocked_next_result(sw, NULL) != 0)
				;
		}

		if (sw->sw_req->sr_ti != NULL)
			timer_end(sw->sw_req->sr_ti, NULL);

		if (sw->sw_req->flags & SQLW_DONT_REUSE)
			sw->sw_flush = 1;

		free(sw->sw_req->logstr);
		pthread_mutex_lock(&sw->sw_req_lock);
		sql_worker_debug_free(sw->sw_req);
		free(sw->sw_req->sr_query);
		if (sw->sw_req->flags & SQLR_ASYNC)
			event_del(&sw->sw_req->async.ev);
		pthread_mutex_destroy(&sw->sw_req->lock);
		pthread_cond_destroy(&sw->sw_req->signal);
		free(sw->sw_req);
		sw->sw_req = NULL;
		pthread_mutex_unlock(&sw->sw_req_lock);
	}

	if (!TAILQ_EMPTY(&sw->queued_requests)) {
		sw->sw_holdcnt = 1;
		pthread_mutex_lock(&sw->sw_req_lock);
		sw->sw_req = TAILQ_FIRST(&sw->queued_requests);
		pthread_mutex_unlock(&sw->sw_req_lock);
		TAILQ_REMOVE(&sw->queued_requests, sw->sw_req, sr_list);
		sql_worker_assigned(sw->sw_req, NULL, NULL);
		return;
	}

	pthread_mutex_lock(&db_conns_lock);

	TAILQ_REMOVE(&sw->sw_sc->sc_busy_workers, sw, sw_list);
	TAILQ_INSERT_HEAD(&sw->sw_sc->sc_free_workers, sw, sw_list);
	pthread_mutex_lock(&sw->sw_req_lock);
	sw->sw_req = NULL;
	pthread_mutex_unlock(&sw->sw_req_lock);

	if (sw->sw_conn == NULL || sw->sw_flush || sw->sw_sc->sc_gc_to == 0) {
		sql_worker_gc(sw);
		pthread_mutex_unlock(&db_conns_lock);
		return;
	}

	sw->sw_gc_ts = time(NULL) + sw->sw_sc->sc_gc_to;

	prod_connection(sw->sw_sc);
	pthread_mutex_unlock(&db_conns_lock);
}

/* Note: you must host the db_conns_lock when calling this function. */
static int
sql_worker_gc(struct sql_worker *sw) {
	struct sql_conninfo *sc;

	DPRINTF(D_DEBUG, ("sql_worker_gc"));

	sc = sw->sw_sc;

	TAILQ_REMOVE(&sc->sc_free_workers, sw, sw_list);
	sc->sc_nworkers--;

	pthread_mutex_destroy(&sw->sw_req_lock);
	sw->close(sw);
	sw->free_worker(sw);

	free(sw);

	return put_connection(sc);
}


static int
sql_worker_newquery_string(struct sql_worker *sw, char *query, const char **errstr) {
	struct sql_request *sr = sw->sw_req;

	if (sr->sr_query)
		free(sr->sr_query);
	sr->sr_query = query;

	/* If we're currently in a SQL callback we need to wait for it to finish. */
	if ((sw->sw_req->flags & SQLR_ASYNC) && sw->sw_state == RESULT_STORED) {
		sw->sw_state = QUERY_READY;
		return 0;
	}

	const char *err = sw->send_query(sw);

	if (err) {
		if (errstr)
			*errstr = err;
		return -1;
	}

	sw->sw_state = QUERY_SENT;

	return 0;
}

int
sql_worker_newquery(struct sql_worker *sw, enum fixed_query_type query, const char **errstr) {
	const char *sqlquery = NULL;

	switch (query) {
	case QUERY_ROLLBACK:
		sqlquery = "ROLLBACK";
		break;
	case QUERY_COMMIT:
		sqlquery = "COMMIT";
		break;
	}

	pthread_mutex_lock(&sw->sw_req_lock);
	sql_worker_debug_reset(sw, sqlquery);
	pthread_mutex_unlock(&sw->sw_req_lock);

	if (sqlquery)
		return sql_worker_newquery_string(sw, xstrdup(sqlquery), errstr);
	return -1;
}

void
sql_worker_template_newquery(struct sql_worker *sw, const char *query_template, struct bpapi *ba) {

	if (sw->sw_holdcnt > 1) {
		DPRINTF(D_ERROR, ("(CRIT) (%s) tried to call %s with worker holdcnt %d > 1", sw->sw_req->logstr, __FUNCTION__, sw->sw_holdcnt));
		return;
	}

	char *query = sql_worker_call_template(sw, query_template, ba, sw->sw_req->logstr);
	const char *errstr = NULL;
	int res = sql_worker_newquery_string(sw, query, &errstr);

	if (res == -1) {
		sw->sw_req->async.errstr = xstrdup(errstr);
		if (sw->sw_req->async.base)
			event_base_once(sw->sw_req->async.base, -1, EV_TIMEOUT, sql_worker_async_cb, sw->sw_req, NULL);
		else
			event_once(-1, EV_TIMEOUT, sql_worker_async_cb, sw->sw_req, NULL);

		event_del(&sw->sw_req->async.ev);
		pthread_mutex_lock(&sw->sw_req_lock);
		/* free debug info here since the request is detached from the worker */
		sql_worker_debug_free(sw->sw_req);
		sw->sw_req = NULL;
		pthread_mutex_unlock(&sw->sw_req_lock);
		sql_worker_put(sw);
	}
}

int
sql_blocked_template_newquery(struct sql_worker *sw, const char *query_template, struct bpapi *ba, const char **errstr) {
	int res = sql_blocked_next_result(sw, errstr);

	if (res == -1)
		return -1;
	if (res == 1) {
		*errstr = "sql-commands out of sync";
		return -1;
	}

	char *query = sql_worker_call_template(sw, query_template, ba, sw->sw_req->logstr);
	return sql_worker_newquery_string(sw, query, errstr);
}

void
sql_worker_set_wflags(struct sql_worker *sw, int wflags) {
	sw->sw_req->flags = (sw->sw_req->flags & ~SQLW_MASK) | (wflags & SQLW_MASK);
}

static int
sql_worker_config(struct bconf_node *conf) {
	if (!bconf_get(conf, "db")) {
		xwarnx("No db configured");
		/* return 1; Keep db optional for now. */
	}
	return 0;
}

static void
sql_worker_reload(void *data) {
	sql_worker_flush_all();

	/* verify_proc_checksums(); not here */
}

static void
sql_worker_init(char *check_only) {
	if (check_only && !strstrptrs(check_only, "dbprocs", NULL, ","))
		return;

	reload_register(sql_worker_reload, NULL);

	verify_proc_checksums();
}

static void *
sql_worker_gc_thread(void *arg) {
	pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, NULL);

	while (1) {
		struct sql_conninfo *sc, *nsc;
		struct sql_worker *sw, *nsw;
		time_t now = time(NULL);

		pthread_testcancel();

		pthread_mutex_lock(&db_conns_lock);

		for (sc = TAILQ_FIRST(&db_conns) ; sc ; sc = nsc) {
			nsc = TAILQ_NEXT(sc, sc_list);

			for (sw = TAILQ_FIRST(&sc->sc_free_workers) ; sw ; sw = nsw) {
				nsw = TAILQ_NEXT(sw, sw_list);

				if (sw->sw_gc_ts <= now)
					sql_worker_gc(sw);
			}
		}
		pthread_mutex_unlock(&db_conns_lock);

		sleep(sql_worker_gc_timeout / 10);
	}
	return NULL;
}

static void
sql_worker_init_done(void) {
	/*
	 * If we're accepting commands on stdin, we're most likely
	 * in debug mode. Let the sql workers time out fast so that
	 * the transaction handler can terminate properly when we're
	 * not in keepalive mode.
	 */
	if (!use_stdin || keepalive) {
		sql_worker_gc_timeout = 60; /* XXX configurable */
		pthread_create(&sql_worker_gc_tid, NULL, sql_worker_gc_thread, NULL);
		sql_worker_gc_running = true;
	}
}

static void
sql_worker_cleanup(void) {
	struct sql_conninfo *sc, *nsc;
	struct sql_worker *sw, *nsw;

	if (sql_worker_gc_running) {
		pthread_cancel(sql_worker_gc_tid);
		pthread_join(sql_worker_gc_tid, NULL);
		sql_worker_gc_running = false;
	}
	while (!TAILQ_EMPTY(&db_conns)) {
		pthread_mutex_lock(&db_conns_lock);
		for (sc = TAILQ_FIRST(&db_conns) ; sc ; sc = nsc) {
			nsc = TAILQ_NEXT(sc, sc_list);

			if (TAILQ_EMPTY(&sc->sc_free_workers)) {
				put_connection(sc);
			} else {
				for (sw = TAILQ_FIRST(&sc->sc_free_workers) ; sw ; sw = nsw) {
					nsw = TAILQ_NEXT(sw, sw_list);
					sql_worker_gc(sw);
				}
			}
		}
		pthread_mutex_unlock(&db_conns_lock);
	}
}

static void 
sql_worker_transinfo(struct transaction *ts) {
	struct sql_conninfo *sc;
	struct sql_worker *sw;
	struct sql_request *sr;
	char connid[256];
	int cnt;

	pthread_mutex_lock(&db_conns_lock);
	TAILQ_FOREACH(sc, &db_conns, sc_list) {
		transaction_printf(ts, "connection:%s %s (%s@%s)\n", sc->sc_config, sc->sc_db_engine, sc->sc_user, sc->sc_host);
		transaction_printf(ts, "max_workers:%u\n", sc->sc_maxworkers);
		transaction_printf(ts, "nworkers:%u\n", sc->sc_nworkers);
		cnt = 0;
		TAILQ_FOREACH(sw, &sc->sc_free_workers, sw_list) {
			cnt++;
		}
		transaction_printf(ts, "free:%u\n", cnt);
		cnt = 0;
		TAILQ_FOREACH(sw, &sc->sc_busy_workers, sw_list) {
			pthread_mutex_lock(&sw->sw_req_lock);
			if (!sw->sw_req) {
				transaction_printf(ts, "busy_%u:state %s, in transition\n", cnt,
					sql_worker_get_state(sw->sw_state)
				);
			} else {
				transaction_printf(ts, "busy_%u:state: %s, start_time: %lu (elapsed: %lds), holdcnt: %u, tmpl: \"%s\" from %s:%d. logstr=%s\n", cnt,
					sql_worker_get_state(sw->sw_state),
					sw->sw_req->start_time,
					time(NULL) - sw->sw_req->start_time,
					sw->sw_holdcnt,
					sw->sw_req->debug.template_name,
					sw->sw_req->debug.source_file,
					sw->sw_req->debug.source_line,
					sw->sw_req->debug.logstr
				);
			}
			sw->connection_identifier(sw, connid, sizeof(connid));
			transaction_printf(ts, "busy_%u:connid: %s\n", cnt, connid);
			cnt++;
			pthread_mutex_unlock(&sw->sw_req_lock);
		}
		cnt = 0;
		TAILQ_FOREACH(sr, &sc->sc_req_todo, sr_list) {
			cnt++;
		}
		transaction_printf(ts, "todo:%u\n", cnt);
	}
	pthread_mutex_unlock(&db_conns_lock);
}

void sql_worker_bpa_insert(struct sql_worker *sw, struct bpapi *ba, const char *prefix) {
	int fields = sw->fields(sw);
	int i;

	while (sw->next_row(sw)) {
		for (i = 0; i < fields; i++) {
			char buf[256];
			const char *k;

			if (prefix && *prefix) {
				snprintf(buf, sizeof(buf), "%s%s", prefix, sw->field_name(sw, i));
				k = buf;
			} else {
				k = sw->field_name(sw, i);
			}
			bpapi_insert(ba, k, sw->get_value(sw, i));
		}
	}
}

ADD_BACKEND(sql_worker, sql_worker_config, sql_worker_init, sql_worker_init_done, sql_worker_cleanup, sql_worker_transinfo);

