#include "trans.h"
#include "command.h"
#include "vtree.h"
#include "transapi.h"

/*
	This transaction can be used to tell a (remote) transaction server
	to flush all its workers. The principal use is for the db_snap
	transaction to tell MAMA to back off.

	It should not be available in production.
*/

extern struct bconf_node *bconf_root;

static struct c_param parameters[] = {
	{"target", P_REQUIRED | P_MULTI, "v_string_all"}, /* "self" or node such as "*.*.common.transaction" */
	{"flushall", P_OPTIONAL, "v_bool"},
	{"suspend", P_OPTIONAL, "v_bool"},
	{NULL, 0, 0}
};

static void
flush_workers(struct cmd *cs) {
	int suspend = cmd_haskey(cs, "suspend") ? atoi(cmd_getval(cs, "suspend")) : 0;
	int flushall = cmd_haskey(cs, "flushall") ? atoi(cmd_getval(cs, "flushall")) : 0;
	int timeout = 1000;
	int i;
	struct cmd_param *cp;
	struct bpapi_vtree_chain vtree;

	for (cp = cmd_getparam(cs, "target"); cp; cp = cmd_nextparam(cs, cp)) {
		if (!cp->value)
			continue;

		if (strcmp(cp->value, "self") == 0) {
			transaction_logprintf(cs->ts, D_INFO, "Waiting for pending transactions to complete%s.", suspend ? " (suspend)" : "");
			trans_pending_wait(cs->ts, suspend);

			if (flushall) {
				transaction_logprintf(cs->ts, D_INFO, "Flushing all workers.");
				sql_worker_flush_all();
				transaction_logprintf(cs->ts, D_INFO, "Flush completed.");
			}
		} else {
			struct bpapi_loop_var hostnames;
			struct bpapi_loop_var hostports;
			struct bconf_node *bnode = bconf_get(bconf_root, cp->value);

			if (!bnode) {
				transaction_printf(cs->ts, "error:Unknown node '%s'\n", cp->value);
				continue;
			}

			bconf_vtree(&vtree, bnode);
			vtree_fetch_values(&vtree, &hostnames, "host", VTREE_LOOP, "name", NULL);
			vtree_fetch_values(&vtree, &hostports, "host", VTREE_LOOP, "port", NULL);

			for (i = 0 ; i < hostnames.len ; i++) {
				const char *host = hostnames.l.list[i];
				const char *port = hostports.l.list[i];

				if (!port[0] || !host[0])
					continue;

				transaction_logprintf(cs->ts, D_INFO, "Flushing all workers on %s:%s (timeout=%d)", host, port, timeout);
				transaction_t *trans = trans_init_host(timeout, host, port);

				if (!trans)
					continue;

				trans_add_printf(trans, "cmd:flush_workers\n"
					"target:self\n"
					"suspend:%d\n"
					"flushall:%d\n"
					"commit:1\n",
					suspend, flushall
				);

				int res;
				if ((res = trans_commit(trans)) != TRANS_COMMIT_OK) {
					transaction_printf(cs->ts, "flush_%s_%s:%s\n", host, port, trans_strerror(res));
				} else {
					transaction_printf(cs->ts, "flush_%s_%s:TRANS_OK\n", host, port);
				}
				trans_free(trans);
			}

			if (hostnames.cleanup)
				hostnames.cleanup(&hostnames);
			if (hostports.cleanup)
				hostports.cleanup(&hostports);
			vtree_free(&vtree);
		}

	}

	cmd_done(cs);
}

ADD_COMMAND_FLAGS(flush_workers, NULL, NULL, flush_workers, parameters, "Flush trans db connections", CMD_NORMAL_PORT | CMD_CONTROL_PORT);
