#ifndef TRANS_MAIL_H
#define TRANS_MAIL_H
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdarg.h>
#include <unistd.h>

#include "command.h"
#include "trans.h"
#include "util.h"
#include "ev_popen.h"
#include <ctemplates.h>

struct bconf_node;

#define TRANS_MAIL_NOLANG ((const char*)-1)

struct mail_data {
	struct event_base *base;
	void *command;		/* This is not a command, it's the callback argument, why the hell is it called command? */
	const char *log_string;
	const char *template;
	const char *lang;
	int status;
	struct bpapi_simplevars_chain schain;
	struct bpapi_simplevars_chain *vars;
	void (*callback)(struct mail_data *, void *, int status);
	const char *bconf_app;
};

void
mail_free(struct mail_data *md);

void
mail_send(struct mail_data *md, const char *template, const char *lang);

void mail_insert_param(struct mail_data *md, const char *key, const char *value);
void mail_insert_int(struct mail_data *md, const char *key, int value);
void mail_insert_paramf(struct mail_data *md, const char *key, const char *fmt, ...);

const char *
mail_get_param(struct mail_data *md, const char *name);

#endif
