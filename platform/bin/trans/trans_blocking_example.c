#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <time.h>
#include <stdlib.h>
#include <unistd.h>

#include "trans.h"
#include "command.h"
#include "util.h"
#include "ctemplates.h"
#include "sql_worker.h"

static struct c_param parameters[] = {
	{"sleep",    P_OPTIONAL,  "v_integer"},
	{NULL, 0}
};

/*****************************************************************************
 * bind_insert
 * Initialise template API before a call to sql_blocked_template_query().
 **********/
static void
bind_insert(struct bpapi *ba, struct cmd *cs) {
	struct c_param *param;

	for (param = parameters; param->name; param++) {
		if (cmd_haskey(cs, param->name))
			bpapi_insert(ba, param->name, cmd_getval(cs, param->name));
	}
}

/*****************************************************************************
 * example
 * basic function that handles everything
 **********/
static void
example_blocking(struct cmd *cs) {
        struct bpapi ba;
	struct sql_worker *worker = NULL;
	const char *errstr = NULL;
 	transaction_logprintf(cs->ts, D_DEBUG, "Start example_blocking transaction");

	transaction_logprintf(cs->ts, D_DEBUG, "example_blocking (worker, errstr) (%p, %p)", worker, errstr);

	if (bconf_get_int(host_bconf, "example.hang_forever"))
		pause();

	/* Initialise the templateparse structure */
	BPAPI_INIT(&ba, NULL, pgsql);
	bind_insert(&ba, cs);

	/* Register SQL query */
	/* Make sure to switch to slave if read-only query. */
	worker = sql_blocked_template_query("pgsql.master", 0, "sql/example_blocking.sql", &ba, cs->ts->log_string, &errstr);

	/* Clean-up */
	bpapi_output_free(&ba);
	bpapi_simplevars_free(&ba);
	bpapi_vtree_free(&ba);

	/*
 	 * sql_blocked_template_query 
 	 * will return worker or populate the errst on error
 	 * if you have it, read it
 	 */
	if (worker) {
		while (sql_blocked_next_result(worker, &errstr) == 1) {
                        while (worker->next_row(worker)) {
				transaction_printf(cs->ts, "debug:reading %s:%s\n", worker->field_name(worker, 0), worker->get_value(worker, 0));
				transaction_logprintf(cs->ts, D_INFO, "Got %s in %s", worker->get_value(worker, 0), worker->field_name(worker, 0));
			}
		}
	}

	/* Something went wrong */
	if (errstr) {
		cs->message = xstrdup(errstr);
		cs->status = "TRANS_DATABASE_ERROR";
	}

	/* put the worker */
	if (worker)
		sql_worker_put(worker);

	cmd_done(cs);
	return;
}

ADD_COMMAND(example_blocking,     /* Name of command */
		NULL,
            example_blocking,     /* Main function name */
            parameters,  /* Parameters struct */
            "example_blocking which is not async command");/* Command description */


