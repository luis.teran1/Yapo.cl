#ifndef STORED_PROC_CHECKSUMS_H
#define STORED_PROC_CHECKSUMS_H

struct stored_proc_checksum {
  const char *name;
  const char *md5;
};

void verify_proc_checksums(void);

#endif /*STORED_PROC_CHECKSUMS_H*/
