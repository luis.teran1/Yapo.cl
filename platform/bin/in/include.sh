#!/bin/bash
#This script creates a .in base on .include file.
#Content of each filename following STATIC_INCLUDE tag will be dumped into output file

SOURCE=$1
DESTINATION=$2
INCLUDE="%STATIC_INCLUDE%"

rm -f ${DESTINATION}

NB_INCLUDE=$(grep "${INCLUDE}" ${SOURCE} | wc -l)
COUNTER=0

for LINE in $(grep "${INCLUDE}" ${SOURCE} -n | cut --delimiter=':' --field=1) 
do
    if [ ${COUNTER} -eq 0 ]
        then
        head -n $(expr ${LINE} - 1) ${SOURCE} > ${DESTINATION}
    elif [ $(expr ${COUNTER} + 1) -lt ${LINE} ]
        then
        sed -n "$(expr ${COUNTER} + 1),$(expr ${LINE} - 1)p" ${SOURCE} >> ${DESTINATION}
    fi
    INCLUDE_FILE_NAME=$(sed -n "${LINE}p" ${SOURCE} | cut --delimiter=':' --field=2 | sed "s/${INCLUDE}[[:space:]]*//")
    cat ${TOPDIR}/conf/${INCLUDE_FILE_NAME} >> ${DESTINATION}
    COUNTER=${LINE}
done
    sed -n "$(expr ${LINE} + 1),\$p" ${SOURCE}  >> ${DESTINATION}
