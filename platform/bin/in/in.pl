#!/usr/bin/perl

my @targs = ();

sub flush {
	($tmp = $current) =~ s/\.in$//;
	$tmp =~ s/^.*\///;
	print "$tmp: $current\n\t\@sed \\\n";

	push @targs, $tmp;

	if ($sects) {
		print "\t-nr '/%(\${BUILD_STAGE}|ANY)_START%/,/%(\${BUILD_STAGE}|ANY)_END%/p' < \$< |\\\n";
		print "\tsed -re '/^%(\${BUILD_STAGE}|ANY)_(START|END)%\$\$/d' \\\n"
	}
	
	
	foreach (keys %vars) {
		print "\t-e 's,%$_%,\${$_},g' \\\n";
	}

	print "\t-e 's,%%,%,g' \\\n";

	if (!$sects) {
		print "\t < \$< > \$@\n\n";
	} else {
		print "\t > \$@\n\n";
	}
}

$current = $ARGV[0];
while (<>) {
	if ($current ne $ARGV) {
		&flush;
		$current = $ARGV;
		undef %vars;
		$sects = 0;
	}

	while (/([^%]|^)%([a-zA-Z][a-zA-Z0-9_]*)%/) {
		$var = $2;

		if ($var =~ /^(.*)_(START|END)$/) {
			$sects = 1;
		} else {
			$vars{$var} = 1;
		}
		s/%([a-zA-Z][a-zA-Z0-9_]*)%//;
	}

}

&flush;
print "IN_TARGETS=" . join(' ', @targs) . "\n";
