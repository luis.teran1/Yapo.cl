#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <bconf.h>
#include <bpapi.h>
#include <controller.h>
#include <ctemplates.h>
#include <logging.h>
#include <memalloc_functions.h>

#include "json_vtree.h"
#include "bconf_daemon.h"
#include "handler.h"
#include "lru.h"
#include "timer.h"
#include "bconf_overwrite.h"
#include "dyn_bconf_action.h"

#define MIN_MAX_CACHE_SIZE_MB 512

struct json_state {
	struct handler *handler;
	int num_workers;
	struct bconf_node *cfg;
	struct lru *cache;

	struct {
		struct bconf_node *config;
		struct ctrl_thread *thread;
	} controller;
};

struct result {
	struct lru *cache;
	struct lru_entry *cache_entry;
	struct buf_string *outbuf;
	char digest[STORAGE_DIGEST_STRING_LENGTH];
};

/*
 * XXX: naive approach to get the correct result, optimize
 */
static void
create_bconf_hostappl_node(const char *host, const char *appl, struct bconf_node *root, struct bconf_node **merged) {
	if (!host && !appl)
		return;

	if (host && !appl) {
		bconf_merge(merged, bconf_get(root, "*"));
		bconf_merge(merged, bconf_get(root, host));
	} else if (!host && appl) {
		struct bconf_node *n;
		int i;
		int c = bconf_count(root);
		for (i = 0; i < c; i++) {
			n = bconf_byindex(root, i);

			struct bconf_node *hostappl = bconf_get(n, appl);
			struct bconf_node *hoststar = bconf_get(n, "*");
			struct bconf_node *tmp = NULL;

			if (hostappl)
				bconf_merge_prefix(&tmp, bconf_key(hostappl), hostappl);

			if (hoststar && hoststar != hostappl)
				bconf_merge_prefix(&tmp, "*", hoststar);

			bconf_merge_prefix(merged, bconf_key(n), tmp);
			bconf_free(&tmp);
		}
	} else {
		struct bconf_node *starstar = bconf_vget(root, "*", "*", NULL);
		struct bconf_node *starappl = bconf_vget(root, "*", appl, NULL);
		struct bconf_node *hoststar = bconf_vget(root, host, "*", NULL);
		struct bconf_node *hostappl = bconf_vget(root, host, appl, NULL);

		if (starstar)
			bconf_merge(merged, starstar);

		if (starappl && starappl != starstar)
			bconf_merge(merged, starappl);

		if (hoststar && hoststar != starstar && hoststar != starappl)
			bconf_merge(merged, hoststar);

		if (hostappl && hostappl != starstar && hostappl != starappl && hostappl != hoststar)
			bconf_merge(merged, hostappl);
	}
}

static int
sf(void *a, int d, int n, const char *fmt, ...) {
	struct buf_string *buf = a;
	va_list ap;
	int res;

	va_start(ap, fmt);
	res = vbscat(buf, fmt, ap);
	va_end(ap);

	return res;
}

static void
set_json_response(struct ctrl_req *cr, struct result *res) {
	ctrl_set_content_type(cr, "application/json");
	ctrl_set_custom_headers(cr, CHECKSUM_HEADER_NAME, res->digest);
	ctrl_set_raw_response_data(cr, res->outbuf->buf, res->outbuf->pos);
}

static void
generate_json_output(struct buf_string *outbuf, struct bconf_node *response) {
	struct bpapi_vtree_chain vchain;
	bconf_vtree(&vchain, response);

	vtree_json(&vchain, 0, 0, sf, outbuf);
	vtree_free(&vchain);
}

static void
free_rescopy(void *rc) {
	struct result *rescopy = rc;
	free(rescopy->outbuf->buf);
	free(rescopy->outbuf);
	free(rescopy);
}

static void
generate_json_bconf(struct ctrl_req *cr, struct bpapi *ba, struct json_state *s, enum storage_id storage) {
	const char *host = bpapi_get_element(ba, "host", 0);
	const char *appl = bpapi_get_element(ba, "appl", 0);

	struct timer_instance *ti = timer_start(NULL, "START");
	if (host && !valid_host_appl(host)) {
		ctrl_error(cr, 422, "Malformed host");
		log_printf(LOG_DEBUG, "Malformed host '%s'", host);
		return;
	}

	if (appl && !valid_host_appl(appl)) {
		ctrl_error(cr, 422, "Malformed appl");
		log_printf(LOG_DEBUG, "Malformed appl '%s'", appl);
		return;
	}

	struct lru_entry *cache_entry;
	struct result *res = zmalloc(sizeof(*res));
	char cache_key[1024];
	int new_entry;

	snprintf(cache_key, sizeof(cache_key), "%d,%s,%s", storage, host ?: ".", appl ?: ".");
	cache_entry = cache_lru(s->cache, cache_key, &new_entry, NULL, NULL); 

	if (cache_entry && (new_entry == 0)) {
		timer_handover(ti, "CACHE_HIT");
		res->cache = s->cache;
		res->cache_entry = cache_entry;
		set_json_response(cr, (struct result *)cache_entry->storage);
		ctrl_set_handler_data(cr, res);
		timer_end(ti, NULL);
		return;
	}

	timer_handover(ti, "CACHE_MISS");
	struct storage *ms = bconf_daemon_get_storage(s->handler->bconf_daemon, storage);
	struct bconf_node *root = storage_get_reader(ms);
	struct bconf_node *response = NULL;
	struct bconf_node *merged = NULL;

	storage_get_digest(ms, res->digest, sizeof(res->digest));
	create_bconf_hostappl_node(host, appl, root, &merged);

	response = merged ?: root;

	res->outbuf = zmalloc(sizeof(res->outbuf));
	generate_json_output(res->outbuf, response);
	storage_put(ms);
	bconf_free(&merged);

	if (cache_entry && new_entry) {
		timer_handover(ti, "CACHE_STORE");
		struct result *rescopy = zmalloc(sizeof(*rescopy));
		rescopy->outbuf = zmalloc(sizeof(rescopy->outbuf));
		bscat(rescopy->outbuf, "%s", res->outbuf->buf);
		memcpy(rescopy->digest, res->digest, sizeof(rescopy->digest));
		cache_entry->storage = rescopy;
		lru_store(s->cache, cache_entry, res->outbuf->pos + sizeof(res->digest));
 		lru_leave(s->cache, cache_entry);
	}

	set_json_response(cr, res);
	timer_end(ti, NULL);

	ctrl_set_handler_data(cr, res);
}

static void
generate_json_all_bconf_cleanup(struct ctrl_req *cr, void *v) {
	struct result *res = v;

	if (res->cache && res->cache_entry)
		lru_leave(res->cache, res->cache_entry);

	if (res->outbuf) {
		free(res->outbuf->buf);
		free(res->outbuf);
	}
	free(res);
}

static void
generate_json_all_bconf_finish(struct ctrl_req *cr, struct bpapi *ba, void *v) {
	generate_json_bconf(cr, ba, v, STORAGE_ALL);
}

static void
generate_json_dyn_bconf_finish(struct ctrl_req *cr, struct bpapi *ba, void *v) {
	generate_json_bconf(cr, ba, v, STORAGE_DYNAMIC);
}

static void
dyn_bconf_action_finish(struct ctrl_req *cr, struct bpapi *ba, void *v) {
	if (!dyn_bconf_action) {
		ctrl_error(cr, 404, "Actions not available.");
		return;
	}
	struct json_state *s = v;
	dyn_bconf_action(cr, ba, s->cfg);
}

static void
bconf_overwrite_finish(struct ctrl_req *cr, struct bpapi *ba, void *v) {
	if (!bconf_overwrite) {
		ctrl_error(cr, 404, "/bconf_overwrite not available.");
		return;
	}
	struct json_state *s = v;
	bconf_overwrite(cr, ba, s->handler->bconf_daemon);
}

static int
start(struct handler *h) {
	struct json_state *s = h->state;
	pthread_t *threads;
	int num_threads;

	struct ctrl_handler handlers[] = {
		{
			.url = "/bconf",
			.finish = generate_json_all_bconf_finish,
			.cleanup = generate_json_all_bconf_cleanup,
			.cb_data = s,
		},
		{
			.url = "/dynamic",
			.finish = generate_json_dyn_bconf_finish,
			.cleanup = generate_json_all_bconf_cleanup,
			.cb_data = s,
		},
		{
			.url = "/dynamic/<action>",
			.finish = dyn_bconf_action_finish,
			.cb_data = s,
		},
		{
			.url = "/bconf_overwrite",
			.finish = bconf_overwrite_finish,
			.cb_data = s,
		},
		ctrl_stats_handler,
	};

	log_printf(LOG_INFO, "handler %s: starting", h->name);
	if ((s->controller.thread = ctrl_thread_setup(s->controller.config, NULL, &threads, &num_threads, handlers, sizeof(handlers) / sizeof(handlers[0]), -1)) == NULL) {
		log_printf(LOG_CRIT, "handler %s: Failed to start controllers (%m)", h->name);
		return -1;
	}
	return 0;
}

static void
stop(struct handler **h) {
	struct json_state *s = (*h)->state;

	log_printf(LOG_INFO, "handler %s: stopping", (*h)->name);
	ctrl_thread_quit(s->controller.thread);

	lru_free(s->cache);
	free(s);
	free((*h)->name);
	free(*h);
	*h = NULL;
}

static void
storage_updated(struct handler *h) {
	struct json_state *s = h->state;
	log_printf(LOG_INFO, "handler %s: emptying cache", h->name);
	struct timer_instance *ti = timer_start(NULL, "CACHE_INVALIDATE");
	if (lru_invalidate(s->cache)) {
		log_printf(LOG_CRIT, "Possibly running with invalid cache");
	}
	timer_end(ti, NULL);
}

static struct handler *
create(const char *name, struct bconf_daemon_state *ds, struct bconf_node *cfg) {
	struct handler *h = xmalloc(sizeof(*h));
	struct json_state *s = xmalloc(sizeof(*s));

	log_printf(LOG_DEBUG, "creating handler of type json with name %s", name);

	h->bconf_daemon = ds;
	h->name = xstrdup(name);
	h->start = start;
	h->stop = stop;
	h->storage_updated = storage_updated;
	h->state = s;

	s->handler = h;
	s->controller.config = bconf_get(cfg, "controller");
	s->cfg = cfg;

	if (!s->controller.config) {
		log_printf(LOG_CRIT, "handler %s: Missing configuration for controller.", h->name);
		free(s);
		free(h);
		return NULL;
	}

	size_t cs = bconf_get_int(s->cfg, "cache_mb");
	if (cs < MIN_MAX_CACHE_SIZE_MB)
		cs = MIN_MAX_CACHE_SIZE_MB;

	cs *= (1024 * 1024);
	log_printf(LOG_INFO, "Initializing cache of max size %zuB", cs);
	s->cache = lru_init(cs, free_rescopy, NULL);

	return h;
}

ADD_HANDLER_TYPE(json, create);
