#ifndef BCONF_OVERRWITE_H
#define BCONF_OVERRWITE_H

struct ctrl_req;
struct bpapi;
struct bconf_daemon_state;

extern void bconf_overwrite(struct ctrl_req *, struct bpapi *, struct bconf_daemon_state *) WEAK;

#endif
