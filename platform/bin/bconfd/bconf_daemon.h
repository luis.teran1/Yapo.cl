#ifndef BCONF_DAEMON_H
#define BCONF_DAEMON_H
#include <bconf.h>
#include <stdbool.h>
#include "storage.h"

#define CHECKSUM_HEADER_NAME "X-Scm-Bconf-Checksum"

enum storage_id {
	STORAGE_STATIC,
	STORAGE_DYNAMIC,
	STORAGE_OVERWRITE,
	STORAGE_ALL
};

struct bconf_daemon_state;
struct bconf_node *bconfd_config;
void bconf_daemon_stop(struct bconf_daemon_state *, bool);
int bconf_daemon(void);
void bconf_daemon_storage_updated(struct bconf_daemon_state *, bool);

struct storage * bconf_daemon_get_storage(struct bconf_daemon_state *, enum storage_id);

/* Validates a string as being a valid host or appl parameter */
int valid_host_appl(const char *name);
/* Validates a string as being a valid type parameter */
bool valid_type(const char *name);

#endif
