#include <bconf.h>
#include <bconfig.h>
#include <daemon.h>
#include <error_functions.h>
#include <logging.h>
#include <macros.h>
#include <ps_status.h>

#include <stdio.h>
#include <getopt.h>

#include "bconf_daemon.h"

extern struct bconf_node *config;

static void
usage(const char *name) {
	fprintf(stderr, "Usage: %s [--foreground | -f] [--quick-start | -q] [--pidfile|-p <path>] [--config|-c <path>] [--bconf|-b <path>]\n", name);
	fprintf(stderr, " --bconf		-- Path to a bconf file. This will override whatever file has been set in the configuration.\n");
	fprintf(stderr, " --config		-- Path to configuration file.\n");
	fprintf(stderr, " --pidfile		-- File that holds pid.\n");
	fprintf(stderr, " --foreground		-- Run in the foreground.\n");
	fprintf(stderr, " --quick-start		-- Do not wait 5 seconds for an early child exit.\n");
}

static struct bconf_node * 
parse_options(int argc, char *argv[]) {
	int opt;
	struct bconf_node *config = NULL;
	struct option long_options[] = {
		{"foreground", 0, NULL, 'f'},
		{"pidfile", 1, NULL, 'p'},
		{"config", 1, NULL, 'c'},
		{"bconf", 1, NULL, 'b'},
		{"help", 0, NULL, 'h'},
		{"quick-start", 0, NULL, 'q'},
		{0, 0, 0, 0}
	};

	while (1) {
		opt = getopt_long(argc, argv, "f:p:c:b:hq", long_options, NULL);
		if (opt == -1)
			break;

		switch (opt) {
			case 'f':
				bconf_add_data(&config, "foreground", "1");
				break;
			case 'p':
				bconf_add_data(&config, "pid_file", optarg);
				break;
			case 'c':
				bconf_add_data(&config, "config_file", optarg);
				break;
			case 'b':
				bconf_add_data(&config, "bconf_file", optarg);
				break;
			case 'q':
				set_quick_start(1);
				break;
			case 'h':
				usage(argv[0]);
				break;
		}
	}

	return config;
}

int
main(int argc, char *argv[]) {
	struct bconf_node *command_params;
	struct bconf_node *bn;
	const char *config_file;
	int rc = 0;

	argv = save_ps_display_args(argc, argv);
	init_ps_display(NULL);
	set_ps_display("BOS");

	command_params = parse_options(argc, argv);

	config_file = bconf_get_string_default(command_params, "config_file", "bconfd.conf");
	if ((bconfd_config = config_init(config_file)) == NULL)
		xerrx(1, "Failed to load config file \"%s\"", config_file);

	if ((bn = bconf_get(command_params, "pid_file")) != NULL)
		set_pidfile(bconf_value(bn));
	
	if ((bn = bconf_get(command_params, "bconf_file")) != NULL)
		bconf_add_data(&bconfd_config, "bconf", bconf_value(bn));

	log_setup("bconfd", bconf_get_string_default(bconfd_config, "log_level", "debug"));

	if (bconf_get(command_params, "foreground") != NULL) {
		do_switchuid();
		rc = bos(bconf_daemon);
	} else {
		daemonify(false, bconf_daemon);
	}

	bconf_free(&bconfd_config);
	bconf_free(&command_params);
	return rc;
}
