#include <stdlib.h>

#include <bconf.h>
#include <bpapi.h>
#include <controller.h>
#include <ctemplates.h>
#include <logging.h>

#include "bconf_daemon.h"
#include "dyn_bconf_action.h"
#include "handler.h"

void 
dyn_bconf_action(struct ctrl_req *cr, struct bpapi *ba, struct bconf_node *config) {
	const char *action = bpapi_get_element(ba, "action", 0);
	struct {
		const char *action;
		const char **parameters;
		const char *template;
	} *ap, actions[] = {
		{"write", (const char *[]){"key", "value", NULL}, "dyn_bconf_set.sql"},
		{"delete", (const char *[]){"key", NULL} ,"dyn_bconf_delete.sql"},
		{NULL}
	};
	struct bpapi lba = {};
	struct bconf_node *filter_conf = NULL;
	struct shadow_vtree shadow_tree = {};
	struct bpapi_vtree_chain temp_vtree = {0};

	for (ap = actions; ap->action != NULL; ap++) {
		if (strcmp(ap->action, action) == 0)
			break;
	}

	if (ap->action == NULL) {
		ctrl_error(cr, 404, "unknown url");
		return;
	}

	for (const char **p = ap->parameters; *p; p++) {
		if (!bpapi_has_key(ba, *p)) {
			ctrl_error(cr, 422, "Missing parameter \"%s\"", *p);
			return;
		}

		if (!strlen(bpapi_get_element(ba, *p, 0))) {
			ctrl_error(cr, 422, "Parameter \"%s\" empty", *p);
			return;
		}
	}

	filter_state_bconf_init_all(&filter_conf);
	default_filter_state = filter_state_bconf;
	BPAPI_INIT_BCONF(&lba, filter_conf, null, pgsql);

	bconf_vtree(&shadow_tree.vtree, config);
	bconf_vtree(&temp_vtree, filter_conf);
	shadow_vtree_init(&lba.vchain, &shadow_tree, &temp_vtree);

	for (const char **p = ap->parameters; *p; p++) {
		bpapi_insert(&lba, *p, bpapi_get_element(ba, *p, 0));
	}
	call_template(&lba, ap->template);

	bpapi_free(&lba);
	filter_state_bconf_free(&filter_conf);
}

