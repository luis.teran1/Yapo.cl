#ifndef DYN_BCONF_ACTION
#define DYN_BCONF_ACTION

struct bconf_node;
struct bpapi;
struct ctrl_req;

extern void dyn_bconf_action(struct ctrl_req *, struct bpapi *, struct bconf_node *) WEAK;

#endif

