#ifndef DBPOLLER_H
#define DBPOLLER_H
#include <inttypes.h>
#include "bconf_daemon.h"
#include "storage.h"

struct stat_message;
struct dbpoller_state {
	pthread_t dbpoller_thread;
	struct bconf_daemon_state *ds;
	volatile bool running;
	volatile bool visited;

	struct timespec interval;

	struct bconf_node *cfg;

	struct storage *db;
	char *timestamp;
	int num_rows;

	struct stat_message *last_updated;
	uint64_t *connection_errors;
	uint64_t *invalid_keys;
	uint64_t *conflicting_keys;
};

bool dbpoller_start(struct dbpoller_state *dbps);
void dbpoller_stop(struct dbpoller_state *dbps);
struct dbpoller_state * dbpoller_create(struct bconf_node *cfg, struct bconf_daemon_state *ds, struct storage *db);
void dbpoller_destroy(struct dbpoller_state **dbps);

#endif
