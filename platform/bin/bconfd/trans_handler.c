#include <pthread.h>
#include <stdbool.h>
#include <unistd.h> /* XXX remove when sleep is removed */
#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>
#include <limits.h>
#include <string.h>
#include <ctype.h>

#include <pcre.h>
#include <avl.h>
#include <bconf.h>
#include <logging.h>
#include <memalloc_functions.h>
#include <queue.h>
#include <stat_counters.h>
#include <stat_messages.h>

#include "bconf_daemon.h"
#include "handler.h"
#include "util.h"
#include "buf_string.h"
#include "sock_util.h"

#define MIN_NUM_WORKERS 10
#define BCONFD_BANNER_OK "220 Welcome.\n"

struct param {
        TAILQ_ENTRY(param) p_list;
        char *key;
        char *value;
};

struct log_msg {
	TAILQ_ENTRY(log_msg) lm_list;
	int level;
	char *msg;
};

struct command {
	char *name;
	const char *errmsg;
	int status;
	int validate_only;
	TAILQ_HEAD(,param) params;
	TAILQ_HEAD(,log_msg) log;
};

static void command_free(struct command *cmd);
static void command_add_log(struct command *cmd, int level, const char *format, ...);
static void command_dump_log(struct command *cmd);

struct bconf_data {
	int allocated_rows;
	int rows;
	char **conf_data;
	char digest[STORAGE_DIGEST_STRING_LENGTH];
};

struct job {
	TAILQ_ENTRY(job) list;
	int fd;
};

struct cache_entry {
	struct avl_node tree;
	size_t size;
	char *key;
	char *data;
};

struct cache {
	pthread_rwlock_t lock;
	struct avl_node *entries;
};

struct trans_state {
	struct handler *handler;
	const char *listen_port;
	struct storage *all_storage;
	struct storage *dynamic_storage;
	struct cache *cache;

	volatile bool running;
	int num_workers;
	pthread_t *workers;

	pthread_t listener;

	pthread_mutex_t job_mutex;
	pthread_cond_t job_cond;
	TAILQ_HEAD(, job) job_list;

	pthread_rwlock_t raw_data_lock;
	struct rd {
		struct bconf_data raw_all_data;
		struct bconf_data raw_dyn_data;
	} raw_data;

	uint64_t *accept;
	uint64_t *out_bytes;
};

static int
cache_entry_cmp(const struct avl_node *an, const struct avl_node *bn) {
	const struct cache_entry *a = avl_data(an, const struct cache_entry, tree);
	const struct cache_entry *b = avl_data(bn, const struct cache_entry, tree);

	return strcmp(a->key, b->key);
}

static void
read_request(int fd, struct command *cmd) {
	FILE *cmd_input;
	struct param *parameter = NULL;
	char *line = NULL;
	ssize_t sz;
	size_t s;
	int found = 0;

	cmd_input = fdopen(fd, "r");
	cmd->validate_only = 1;

	while ((sz = getline(&line, &s, cmd_input)) != -1) {
		char *eol;
		char *key;
		char *value;

		eol = &line[sz - 1];
		*eol = '\0';

		key = line;
		value = strchr(key, ':');

		if (value)
			*value++ = '\0';

		if (*key == '\0')
			continue;

		if (!strcmp(key, "end")) {
			found = 1;
			free(key);
			break;
		} else if (!strcmp(key, "cmd") || !strcmp(key, "commit")) {
			if (!strcmp(key, "cmd"))
				cmd->name = xstrdup(value ?: "");
			else
				cmd->validate_only = !atoi(value ?: "0");
			free(key);
			line = NULL;
			s = 0;
			continue;
		}

		parameter = xmalloc(sizeof(*parameter));
		parameter->key = key;
		parameter->value = xstrdup(value ? value : "");

		TAILQ_INSERT_TAIL(&cmd->params, parameter, p_list);

		line = NULL;
		s = 0;
	}

	if (!found) {
		cmd->errmsg = "ERROR_INCOMPLETE_REQUEST";
		cmd->status = 1;
	}
}

static void
flatten_bconf(struct bconf_data *bc, const char *prefix, struct bconf_node *node) {
	const char *v;

	if (!node)
		return;

	if ((v = bconf_value(node))) {
		if (!bc->allocated_rows) {
			bc->allocated_rows = 4096;
			bc->conf_data = xmalloc(bc->allocated_rows * sizeof(char*));
		}

		xasprintf(&bc->conf_data[bc->rows++], "%s%s=%s", prefix, bconf_key(node), v);

		if (bc->rows == bc->allocated_rows) {
			bc->allocated_rows *= 2;
			bc->conf_data = xrealloc(bc->conf_data, bc->allocated_rows * sizeof(char*));
		}
	} else {
		char *newpref = NULL;
		bool newpref_allocated = false;
		int c = bconf_count(node);
		const char *k = bconf_key(node);

		if (k) {
			xasprintf(&newpref, "%s%s.", prefix, k);
			newpref_allocated = true;
		} else
			newpref = (char*)prefix;

		for (int i = 0 ; i < c ; i++)
			flatten_bconf(bc, newpref, bconf_byindex(node, i));

		if (newpref_allocated)
			free(newpref);
	}
}

static int
key_comp(char *k1, char *k2, int nskip) {
	char *s1 = strchr(k1, '=');
	char *s2 = strchr(k2, '=');

	/* Skip the first two parts. */
	while (nskip--) {
		k1 = strchr(k1, '.') + 1;
		k2 = strchr(k2, '.') + 1;
	}

	if (s1 - k1 != s2 - k2)
		return 0;

	return (strncmp(k1, k2, s1 - k1) == 0);
}

/*
 * Compare keys, part by part. First by length in descending order,
 * then in lex desc order. Last by number of parts in ascending order.
 * Finish off by sorting on the more specific key based on the host and
 * app.
 */

static int
bc_comp(const void *p1, const void *p2, int skip) {
	const char *k1 = *(const char **)p1;
	const char *k2 = *(const char **)p2;
	const char *d1 = NULL;
	const char *d2 = NULL;
	int s1 = 0;
	int s2 = 0;
	int res;
	int nskip = skip;

	if (p1 == p2)
		return 0;

	/* Skip the first n parts. */

	while (nskip--) {
		k1 = strchr(k1, '.') + 1;
		k2 = strchr(k2, '.') + 1;
	}

	while ((d1 = strpbrk(k1, ".=")) , (d2 = strpbrk(k2, ".=")) , (d1 && d2)) {
		if (d1 - k1 == d2 - k2) {
			if ((res = strncmp(k1, k2, d1 - k1)) == 0) {
				if (*d1 == '=')
					d1 = NULL;
				if (*d2 == '=')
					d2 = NULL;
				if (!d1 || !d2)
					break;

				k1 = d1 + 1;
				k2 = d2 + 1;
				continue;
			} else {
				return res;
			}
		}

		if (d1 - k1 > d2 - k2) {
			return 1;
		}

		if (d1 - k1 < d2 - k2) {
			return -1;
		}

		if (*d1 == '=')
			d1 = NULL;
		if (*d2 == '=')
			d2 = NULL;
		if (!d1 || !d2)
			break;

		k1 = d1 + 1;
		k2 = d2 + 1;
	}

	if (d1)
		return -1;
	if (d2)
		return 1;

	/* Count the number of '*' in the first n parts */
	k1 = *(const char **)p1;
	k2 = *(const char **)p2;

	nskip = skip;
	while (nskip) {
		/* Give set host higher prio than set appl, ie earlier stars have lower prio. */
		if (*k1 == '*') s1 += nskip;
		if (*k2 == '*') s2 += nskip;
		nskip--;

		k1 = strchr(k1, '.') + 1;
		k2 = strchr(k2, '.') + 1;
	}

	return s1 - s2;
}

static int
bc_comp_0(const void *a, const void *b) {
	return bc_comp(a, b, 0);
}

static int
bc_comp_1(const void *a, const void *b) {
	return bc_comp(a, b, 1);
}

static int
bc_comp_2(const void *a, const void *b) {
	return bc_comp(a, b, 2);
}

static int (*const bc_comps[])(const void *a, const void *b) = {
	bc_comp_0,
	bc_comp_1,
	bc_comp_2
};

static const char*
skip_fields(const char *string, int nskips) {
	const char *tmp = string;
	if (!nskips)
		return string;

	while (nskips--) {
		tmp = strchr(tmp, '.');
		if (!tmp)
			return string;
		tmp++;
	}

	return tmp;
}

static int
bc_reduce(char **bc, int rows, const char *host, const char *appl) {
	int bc_cnt = 0;
	int cnt;
	char *rex;
	pcre *re;
	const char *err;
	int erro;
	const unsigned char *pcre_table = pcre_maketables();

	xasprintf(&rex, "^(\\*|%s)\\.(\\*|%s)\\.", host, appl);

	if (!(re = pcre_compile(rex, PCRE_NO_AUTO_CAPTURE | PCRE_CASELESS, &err, &erro, pcre_table))) {
		free(rex);
		return 0;
	}
	free(rex);

	for (cnt = 0 ; cnt < rows ; cnt++) {
		if (pcre_exec(re, NULL, bc[cnt], strlen(bc[cnt]), 0, 0, NULL, 0) == 0) {
			char *pos = bc[cnt];
			bc[bc_cnt++] = pos;
		}
	}

	pcre_free(re);
	pcre_free((void*)pcre_table);

	return bc_cnt;
}

static void
cache_result(struct cache *c, char *key, char *bc, size_t rc) {
	struct cache_entry lookup_key;
	lookup_key.key = key;

	pthread_rwlock_wrlock(&c->lock);
	if (avl_lookup(&lookup_key.tree, &c->entries, cache_entry_cmp)) {
		/* entry already cached. do not cache again */
		pthread_rwlock_unlock(&c->lock);
		return;
	}

	struct cache_entry *new_data = malloc(sizeof(*new_data));
	if (new_data) {
		new_data->size = rc;
		new_data->data = xstrdup(bc);
		new_data->key = xstrdup(key);

		avl_insert(&new_data->tree, &c->entries, cache_entry_cmp);
	}

	pthread_rwlock_unlock(&c->lock);
}

static size_t
render_response(const char *host, const char *appl, struct bconf_data *raw_file_data, char **response) {
	struct buf_string *bs = zmalloc(sizeof(*bs));
	char **bc = NULL;
	size_t retval = 0;
	int rc = 0;
	int skip = 0;


	bc = xmalloc(raw_file_data->rows * sizeof(char*));

	if (host) {
		skip++;
		if (appl)
			skip++;
	}

	if (raw_file_data->rows)
		memcpy(bc, raw_file_data->conf_data, raw_file_data->rows * sizeof(char*));

	rc = bc_reduce(bc, raw_file_data->rows, host ?: "[^.]*", appl ?: "[^.]*");
	qsort(bc, rc, sizeof(char*), bc_comps[skip]);

	for (int cnt = 0 ; cnt < rc ; cnt++) {
		if (!cnt || (cnt && !key_comp(bc[cnt - 1], bc[cnt], skip))) {
			if (strchr(bc[cnt], '=')) {
				if (strchr(bc[cnt],'\n') || strchr(bc[cnt],'\r'))
					bscat(bs, "blob:%zu:conf\n%s\n", strlen(skip_fields(bc[cnt], skip)), skip_fields(bc[cnt], skip));
				else
					bscat(bs, "conf:%s\n", skip_fields(bc[cnt], skip));
			} else {
				bscat(bs, "del:%s\n", skip_fields(bc[cnt], skip));
			}
		}
	}
	bscat(bs, "%s:%s\n", CHECKSUM_HEADER_NAME, raw_file_data->digest);

	*response = bs->buf;
	retval = bs->pos;

	free(bc);
	free(bs);

	return retval;
}

static int
send_response(int fd, const char *response, size_t sz) {
	ssize_t sent = 0;
	while (sz) {
		if ((sent = write(fd, response, sz)) == -1) {
			return -1;
			break;
		}
		sz -= sent;
		response += sent;
	}

	return 0;
}

static void
send_response_done(int fd, struct command *cmd) {
	if (cmd->status) {
		if (cmd->errmsg)
			dprintf(fd, "error:%s\n", cmd->errmsg);
		dprintf(fd, "status:TRANS_ERROR\n");
	} else {
		dprintf(fd, "status:TRANS_OK\n");
	}
	dprintf(fd, "end\n");

	command_dump_log(cmd);

	command_free(cmd);

	close(fd);
}

static bool
cache_get(struct cache *c, char *key, char **r, size_t *rsz) {
	struct avl_node *n;
	struct cache_entry *ce;
	struct cache_entry e = { .key = key };


	pthread_rwlock_rdlock(&c->lock);
	if ((n = avl_lookup(&e.tree, &c->entries, cache_entry_cmp)) == NULL) {
		pthread_rwlock_unlock(&c->lock);
		return false;
	}

	ce = avl_data(n, struct cache_entry, tree);
	*r = ce->data;
	*rsz = ce->size;

	return true;
}

static void
cache_put(struct cache *c) {
	pthread_rwlock_unlock(&c->lock);
}

static int
validate_request(int fd, struct command *cmd) {
	struct param *param = NULL;

	if (cmd->status)
		return cmd->status;

	if (!cmd->name) {
		cmd->errmsg = "ERROR_MISSING_COMMAND";
		cmd->status = 1;
	} else if (!strcmp(cmd->name, "bconf")) {
		TAILQ_FOREACH(param, &cmd->params, p_list) {
			if (strcmp(param->key, "host") == 0) {
				if (!valid_host_appl(param->value)) {
					dprintf(fd, "host:ERROR_HOST_INVALID\n");
					command_add_log(cmd, LOG_DEBUG, "host:ERROR_HOST_INVALID");
					cmd->status = 1;
				}
			} else if (strcmp(param->key, "appl") == 0) {
				if (!valid_host_appl(param->value)) {
					dprintf(fd, "appl:ERROR_APPL_INVALID\n");
					command_add_log(cmd, LOG_DEBUG, "appl:ERROR_APPL_INVALID");
					cmd->status = 1;
				}
			} else if (!strcmp(param->key, "type")) {
				if (!valid_type(param->value)) {
					dprintf(fd, "type:ERROR_TYPE_INVALID\n");
					command_add_log(cmd, LOG_DEBUG, "%s", "type:ERROR_TYPE_INVALID");
					cmd->status = 1;
				}
			} else {
				dprintf(fd, "%s:TRANS_ERROR_NO_SUCH_PARAMETER\n", param->key);
				command_add_log(cmd, LOG_DEBUG, "%s:TRANS_ERROR_NO_SUCH_PARAMETER", param->key);
				cmd->status = 1;
			}
		}
	} else if (!strcmp(cmd->name, "bconf_get_values")) {
		int found = 0;
		TAILQ_FOREACH(param, &cmd->params, p_list) {
			if (!strcmp(param->key, "key")) {
				found = 1;
			} else {
				dprintf(fd, "%s:TRANS_ERROR_NO_SUCH_PARAMETER\n", param->key);
				command_add_log(cmd, LOG_DEBUG, "%s:TRANS_ERROR_NO_SUCH_PARAMETER", param->key);
				cmd->status = 1;
			}
		}
		if (!found) {
			dprintf(fd, "key:ERROR_KEY_MISSING\n");
			command_add_log(cmd, LOG_DEBUG, "%s", "key:ERROR_KEY_MISSING");
			cmd->status = 1;
		}
	} else {
		dprintf(fd, "%s:TRANS_ERROR_NO_SUCH_COMMAND\n", cmd->name);
		command_add_log(cmd, LOG_DEBUG, "%s:TRANS_ERROR_NO_SUCH_COMMAND", cmd->name);
		cmd->status = 1;
	}

	return cmd->status;
}

static void
command_free(struct command *cmd) {
	struct param *parameter;
	struct log_msg *lmsg;

	while ((parameter = TAILQ_FIRST(&cmd->params)) != NULL) {
		TAILQ_REMOVE(&cmd->params, parameter, p_list);
		free(parameter->key);
		free(parameter->value);
		free(parameter);
	}

	/* Free command log */
	while ((lmsg = TAILQ_FIRST(&cmd->log)) != NULL) {
		TAILQ_REMOVE(&cmd->log, lmsg, lm_list);
		free(lmsg->msg);
		free(lmsg);
	}

	free(cmd->name);
}

static char *
command_get_value(struct command *cmd, const char *key) {
	struct param *param = NULL;

	TAILQ_FOREACH(param, &cmd->params, p_list) {
		if (strcmp(param->key, key) == 0)
			return param->value;
	}

	return NULL;
}

static void
command_add_log(struct command *cmd, int level, const char *format, ...) {
	va_list args;

	struct log_msg *lmsg = xmalloc(sizeof(*lmsg));
	lmsg->level = level;

	va_start(args, format);
	xvasprintf(&lmsg->msg, format, args);
	va_end(args);

	TAILQ_INSERT_TAIL(&cmd->log, lmsg, lm_list);
}

static void
command_dump_log(struct command *cmd) {
	struct param *param = NULL;
	struct buf_string buf = {};

	char id[32] = { 0 };
	snprintf(id, sizeof(id), "%p", (void*)cmd);

	TAILQ_FOREACH(param, &cmd->params, p_list) {
		bscat(&buf, "%s:%s\\n", param->key, param->value);
	}

	log_printf(LOG_DEBUG, "%s << cmd:%s\\n%s -> %s", id, cmd->name, buf.buf ?: "", cmd->status ? "ERROR" : "OK");
	if (cmd->errmsg)
		log_printf(LOG_DEBUG, "%s >> errmsg:%s", id, cmd->errmsg);
	free(buf.buf);

	struct log_msg *lmsg;
	TAILQ_FOREACH(lmsg, &cmd->log, lm_list) {
		log_printf(lmsg->level, "%s >> %s", id, lmsg->msg);
	}

}

static char *
generate_cache_key(struct command *cmd) {
	char *cache_key = NULL;

	xasprintf(&cache_key, "%s.%s.%s",
			command_get_value(cmd, "type") ?: "",
			command_get_value(cmd, "host") ?: "",
			command_get_value(cmd, "appl") ?: ""
		);
	return cache_key;
}

static void
process_job(struct trans_state *s, struct job **jobpp) {
	struct job *jobp = *jobpp;
	struct command cmd = { 0 };
	char *cache_key = NULL;
	char *r = NULL;
	size_t rsz;

	if (!jobp)
		return;

	dprintf(jobp->fd, BCONFD_BANNER_OK);

	TAILQ_INIT(&cmd.params);
	TAILQ_INIT(&cmd.log);

	read_request(jobp->fd, &cmd);

	if (validate_request(jobp->fd, &cmd)) {
		send_response_done(jobp->fd, &cmd);

		free(jobp);
		*jobpp = NULL;

		return;
	}

	if (cmd.validate_only) {
		send_response_done(jobp->fd, &cmd);

		free(jobp);
		*jobpp = NULL;

		return;
	}

	if ((cache_key = generate_cache_key(&cmd)) == NULL) {
		log_printf(LOG_WARNING, "handler %s: failed to generate cache_key", s->handler->name);

		cmd.status = 1;
		send_response_done(jobp->fd, &cmd);

		free(jobp);
		*jobpp = NULL;

		return;
	}

 	log_printf(LOG_DEBUG, "handler %s: got request with cache_key \"%s\"", s->handler->name, cache_key);

	if (cache_get(s->cache, cache_key, &r, &rsz)) {
		log_printf(LOG_DEBUG, "handler %s: cache hit", s->handler->name);

		if (send_response(jobp->fd, r, rsz) == -1) {
			log_printf(LOG_WARNING, "handler %s: failed to send response (%m)", s->handler->name);
			cmd.errmsg = "ERROR_RESPONSE";
			cmd.status = 1;
		}
		cache_put(s->cache);
	} else {
		log_printf(LOG_DEBUG, "handler %s: cache miss", s->handler->name);
		pthread_rwlock_rdlock(&s->raw_data_lock);
		rsz = render_response(command_get_value(&cmd, "host"),
					command_get_value(&cmd, "appl"),
					(command_get_value(&cmd, "type") && !strcmp(command_get_value(&cmd, "type"), "conf")) ?  &s->raw_data.raw_dyn_data : &s->raw_data.raw_all_data,
					&r
		);
		cache_result(s->cache, cache_key, r, rsz);
		pthread_rwlock_unlock(&s->raw_data_lock);

		if (send_response(jobp->fd, r, rsz) == -1) {
			log_printf(LOG_WARNING, "handler %s: failed to send response (%m)", s->handler->name);
			cmd.errmsg = "ERROR_RESPONSE";
			cmd.status = 1;
		}
		free(r);
	}
	send_response_done(jobp->fd, &cmd);
	STATCNT_ADD(s->out_bytes, rsz);

	free(cache_key);
	free(jobp);
	*jobpp = NULL;
}

static void *
worker_thread(void *arg) {
	struct handler *h = (struct handler *)arg;
	struct trans_state *s = (struct trans_state *)h->state;
	struct job *job = NULL;
	log_printf(LOG_DEBUG, "handler %s: worker started", s->handler->name);

	pthread_mutex_lock(&s->job_mutex);
	while (s->running) {
		pthread_cond_wait(&s->job_cond, &s->job_mutex);
		while (!TAILQ_EMPTY(&s->job_list)) {
			job = TAILQ_FIRST(&s->job_list);
			TAILQ_REMOVE(&s->job_list, job, list);
			pthread_mutex_unlock(&s->job_mutex);

			process_job(s, &job);

			pthread_mutex_lock(&s->job_mutex);
		}
	}
	pthread_mutex_unlock(&s->job_mutex);

	log_printf(LOG_DEBUG, "handler %s: worker stopped", s->handler->name);
	return NULL;
}

static void *
listener_thread(void *arg) {
	struct handler *h = arg;
	struct trans_state *s = h->state;
	int listen_socket;
	struct job *job;
	int fd;
	log_printf(LOG_DEBUG, "handler %s: listener started", s->handler->name);

	if ((listen_socket = create_socket(NULL, s->listen_port)) < 0) {
		log_printf(LOG_CRIT, "handler %s: failed to open socket (%m)", s->handler->name);
		bconf_daemon_stop(h->bconf_daemon, true);
		return NULL;
	}

	while (s->running) {
		pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, NULL);
		if ((fd = accept(listen_socket, NULL, NULL)) > 0) {
			pthread_setcancelstate(PTHREAD_CANCEL_DISABLE, NULL);
			job = xmalloc(sizeof(*job));
			job->fd = fd;
			STATCNT_INC(s->accept);

			pthread_mutex_lock(&s->job_mutex);
			TAILQ_INSERT_TAIL(&s->job_list, job, list);
			pthread_cond_signal(&s->job_cond);
			pthread_mutex_unlock(&s->job_mutex);
		}
	}

	close(listen_socket);
	log_printf(LOG_DEBUG, "handler %s: listener stopped", s->handler->name);
	return NULL;
}

static void storage_updated(struct handler *h);

static int
start(struct handler *h) {
	struct trans_state *s = h->state;

	storage_updated(h);

	s->running = true;
	pthread_create(&s->listener, NULL, listener_thread, h);
	for (int i = 0; i < s->num_workers; i++) {
		pthread_create(&s->workers[i], NULL, worker_thread, h);
	}

	return 0;
}

static void
cache_free_contents(struct avl_node *n) {
	struct cache_entry *e;
	if (!n)
		return;

	cache_free_contents(n->link[0]);
	cache_free_contents(n->link[1]);

	e = avl_data(n, struct cache_entry, tree);
	free(e->data);
	free(e->key);
	free(e);
}

static void
cache_free(struct cache *c) {
	cache_free_contents(c->entries);
	free(c);
}

static void
stop(struct handler **h) {
	struct trans_state *s = (*h)->state;
	log_printf(LOG_INFO, "handler %s: stopping", (*h)->name);

	s->running = false;
	pthread_cancel(s->listener);
	pthread_join(s->listener, NULL);
	pthread_cond_broadcast(&s->job_cond);
	for (int i = 0; i < s->num_workers; i++) {
		pthread_join(s->workers[i], NULL);
	}

	for (int i = 0; i < s->raw_data.raw_all_data.rows; i++) {
		free(s->raw_data.raw_all_data.conf_data[i]);
	}
	free(s->raw_data.raw_all_data.conf_data);

	for (int i = 0; i < s->raw_data.raw_dyn_data.rows; i++) {
		free(s->raw_data.raw_dyn_data.conf_data[i]);
	}
	free(s->raw_data.raw_dyn_data.conf_data);

	cache_free(s->cache);

	stat_counter_dynamic_free(s->out_bytes);
	stat_counter_dynamic_free(s->accept);

	free(s->workers);
	free(s);

	free((*h)->name);
	free(*h);
	*h = NULL;
}

static void
set_raw_data(struct storage *storage, struct bconf_data *data) {
	struct bconf_node *bcd = NULL;
	int old_rows = data->rows;
	char **old_data = data->conf_data;

	bcd = storage_get_reader(storage);
	memset(data, 0, sizeof(*data));
	flatten_bconf(data, "", bcd);
	storage_get_digest(storage, data->digest, sizeof(data->digest));
	storage_put(storage);

	for (int i = 0; i < old_rows; i++) {
		free(old_data[i]);
	}
	free(old_data);
}

static void
storage_updated(struct handler *h) {
	struct trans_state *s = h->state;
	struct avl_node *old_cache_entries;
	log_printf(LOG_DEBUG, "handler %s: updating storage", s->handler->name);

	char buf[STORAGE_DIGEST_STRING_LENGTH];
	storage_get_digest(s->all_storage, buf, sizeof(buf));

	log_printf(LOG_DEBUG, "handler %s: processing merged configuration with checksum %s", s->handler->name, buf);

	pthread_rwlock_wrlock(&s->raw_data_lock);
	set_raw_data(s->all_storage, &s->raw_data.raw_all_data);
	set_raw_data(s->dynamic_storage, &s->raw_data.raw_dyn_data);
	pthread_rwlock_unlock(&s->raw_data_lock);

	pthread_rwlock_wrlock(&s->cache->lock);
	log_printf(LOG_DEBUG, "handler %s: emptying cache", s->handler->name);
	old_cache_entries = s->cache->entries;
	s->cache->entries = NULL;
	pthread_rwlock_unlock(&s->cache->lock);
	cache_free_contents(old_cache_entries);
}

static struct handler *
create(const char *name, struct bconf_daemon_state *ds, struct bconf_node *cfg) {
	struct handler *h = xmalloc(sizeof(*h));
	struct trans_state *s = xmalloc(sizeof(*s));

	log_printf(LOG_DEBUG, "creating handler of type trans with name %s", name);

	h->bconf_daemon = ds;
	h->name = xstrdup(name);
	h->start = start;
	h->stop = stop;
	h->storage_updated = storage_updated;
	h->state = s;

	s->handler = h;
	s->running = false;
	s->num_workers = bconf_get_int(cfg, "num_workers");

	if (s->num_workers < MIN_NUM_WORKERS)
		s->num_workers = MIN_NUM_WORKERS;

	s->workers = xcalloc(s->num_workers, sizeof(*s->workers));
	s->listen_port = bconf_get_string(cfg, "port");
	s->all_storage = bconf_daemon_get_storage(h->bconf_daemon, STORAGE_ALL);
	s->dynamic_storage = bconf_daemon_get_storage(h->bconf_daemon, STORAGE_DYNAMIC);
	s->cache = zmalloc(sizeof(*s->cache));

	memset(&s->raw_data.raw_all_data, 0, sizeof(s->raw_data.raw_all_data));
	memset(&s->raw_data.raw_dyn_data, 0, sizeof(s->raw_data.raw_dyn_data));
	pthread_rwlock_init(&s->cache->lock, NULL);
	pthread_rwlock_init(&s->raw_data_lock, NULL);

	pthread_mutex_init(&s->job_mutex, NULL);
	pthread_cond_init(&s->job_cond, NULL);
	TAILQ_INIT(&s->job_list);

	s->out_bytes = stat_counter_dynamic_alloc(3, "handler", name, "out_bytes");
	s->accept = stat_counter_dynamic_alloc(3, "handler", name, "accept");
	return h;
}

ADD_HANDLER_TYPE(trans, create);
