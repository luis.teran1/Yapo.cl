#include <pthread.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <limits.h>
#include <memalloc_functions.h>
#include <logging.h>
#include <bconf.h>
#include <bconfig.h>
#include "xxhash.h"
#include "storage.h"
#include "util.h"

#define MAX_BCONF_LINE 10000

struct storage_checksum {
	unsigned long long XXH64;
};

struct storage {
	pthread_rwlock_t rwlock;
	struct bconf_node *db;
	struct storage_checksum checksum;
};

struct internal_checksum_state {
	XXH64_state_t XXH64;
};

static bool
load_config(struct bconf_node **root, const char *filename, bool canfail) {
	FILE *file;
	char buf[MAX_BCONF_LINE];
	char *tmp;
	int lnum = 0;
	int res = 0;

	if (!(file = fopen(filename, "r"))) {
		log_printf(LOG_CRIT, "Failed to open bconf file %s", filename);

		/* Non fatal */
		if (canfail)
			return 0;
		return 1;
	}

	while (!feof(file) && (fgets(buf, sizeof(buf), file))) {
		if (strlen(buf) >= MAX_BCONF_LINE - 1)
			log_printf(LOG_CRIT, "MAX_BCONF_LINE reached (%d): possibly truncating line: %s", MAX_BCONF_LINE, buf);

		lnum++;
		trim(buf);

		if (strncmp(buf, "include", sizeof("include") - 1) == 0) {
			char *include_name;
			char *tmp = buf + sizeof("include") - 1;

			while (*tmp == ' ')
				tmp++;

			if (*tmp == '/') {
				res = load_config(root, tmp, true);
			} else {
				xasprintf(&include_name, "%.*s/%s", (int)(strrchr(filename, '/') - filename), filename, tmp);
				res = load_config(root, include_name, true);
				free(include_name);
			}

			if (res)
				break;

			continue;
		}

		/* Skip comments and empty lines */
		if (buf[0] == '#' || buf[0] == '\0')
			continue;

		tmp = strchr(buf, '=');

		if (!tmp) {
			log_printf(LOG_CRIT, "Syntax error in file %s:%d (missing =)", filename, lnum);
			res = 1;
			break;
		}

		*tmp = '\0';
		tmp++;

		trim(buf);
		trim(tmp);

		if (strlen(buf) == 0) {
			log_printf(LOG_CRIT, "Syntax error in file %s:%d missing key at =%s", filename, lnum, tmp);
			res = 1;
		} else if (buf[0] == '.' || buf[strlen(buf) - 1] == '.' || strstr(buf, "..")) {
			log_printf(LOG_CRIT, "Syntax error in file %s:%d stray '.' at %s=%s", filename, lnum, buf, tmp);
			res = 1;
		} else if (bconf_add_data_canfail(root, buf, tmp) == -1) {
			log_printf(LOG_CRIT, "bconf_root: node list/value conflict at %s=%s", buf, tmp);
			res = 1;
		}

		if (res)
			break;
	}

	fclose(file);
	return res;
}

static int
checksum_cb(const char *path, const char *value, void *cbdata) {
	struct internal_checksum_state *state = cbdata;

	/* Slightly inefficient, but easy */
	XXH64_update(&state->XXH64, path, strlen(path));
	XXH64_update(&state->XXH64, value, strlen(value));

	return 0;
}

static struct storage_checksum
calculate_node_checksum(struct bconf_node *node) {
	struct storage_checksum res = { 0 }; /* Set checksum to zero for empty storage */

	if (!node)
		return res;

	struct internal_checksum_state state;
	XXH64_reset(&state.XXH64, 0);

	bconf_foreach(node, INT_MAX, checksum_cb, &state);

	res.XXH64 = XXH64_digest(&state.XXH64);
	log_printf(LOG_DEBUG, "storage: new checksum for node at %p -> %016llx", (void*)node, res.XXH64);

	return res;
}

struct bconf_node *
storage_swap_db(struct storage *s, struct bconf_node *new) {
	struct bconf_node *old;
	struct storage_checksum new_chksum = calculate_node_checksum(new);

	pthread_rwlock_wrlock(&s->rwlock);
	old = s->db;
	s->db = new;
	s->checksum = new_chksum;
	pthread_rwlock_unlock(&s->rwlock);

	return old;
}

struct bconf_node *
storage_get_reader(struct storage *s) {
	pthread_rwlock_rdlock(&s->rwlock);

	return s->db;
}

struct bconf_node **
storage_get_writer(struct storage *s) {

	pthread_rwlock_wrlock(&s->rwlock);
	return &s->db;
}

void
storage_get_digest(struct storage *s, char *dest, ssize_t dest_size) {
	pthread_rwlock_rdlock(&s->rwlock);
	snprintf(dest, dest_size, "%016llx", s->checksum.XXH64);
	pthread_rwlock_unlock(&s->rwlock);
}

void
storage_put(struct storage *s) {
	pthread_rwlock_unlock(&s->rwlock);
}

struct storage *
storage_create_from_file(const char *path) {
	struct storage *s = zmalloc(sizeof(*s));
	struct bconf_node *new = NULL;

	if (load_config(&new, path, false)) {
		log_printf(LOG_CRIT, "storage: failed to read bconf file %s", path);
		bconf_free(&new);
		free(s);
		return NULL;
	}

	pthread_rwlock_init(&s->rwlock, NULL);
	storage_swap_db(s, new);

	return s;
}

struct storage *
storage_create_from_bconf(struct bconf_node *bc) {
	struct storage *s = zmalloc(sizeof(*s));

	pthread_rwlock_init(&s->rwlock, NULL);
	storage_swap_db(s, bc);

	return s;
}

bool
storage_reload_from_file(struct storage *s, const char *path) {
	struct bconf_node *new = NULL;

	if (!path) {
		log_printf(LOG_WARNING, "storage: tried to reload from null path");
		return false;
	}

	if (load_config(&new, path, false)) {
		log_printf(LOG_WARNING, "storage: failed to reload from path %s", path);
		bconf_free(&new);
		return false;
	}

	struct bconf_node *old = storage_swap_db(s, new);
	if (old)
		bconf_free(&old);

	return true;
}

void
storage_replace_with_bconf(struct storage *s, struct bconf_node *bc) {

	struct bconf_node *old = storage_swap_db(s, bc);
	if (old)
		bconf_free(&old);
}

void
storage_free(struct storage **s) {
	if (!s || !*s)
		return;
	bconf_free(&(*s)->db);
	free(*s);
	*s = NULL;
}
