#include <pthread.h>
#include <time.h>
#include <stdlib.h>
#include <stat_messages.h>
#include <stat_counters.h>

#include <logging.h>
#include <memalloc_functions.h>
#include <ctemplates.h>
#include <bpapi.h>

#include "dbpoller.h"

static void
polldb(struct dbpoller_state *dbps) {
	struct bconf_node *filter_conf = NULL;
	struct shadow_vtree shadow_conf = { {0} };
	struct bpapi_vtree_chain tvt = {0};
	struct bpapi ba = {};
	bool rebuild_storage = false;
	const char *newest_ts = "";

	filter_state_bconf_init_all(&filter_conf);
	default_filter_state = filter_state_bconf;

	null_output_init(&ba.ochain);
	hash_simplevars_init(&ba.schain);
	ba.filter_state = filter_state_bconf;

	bconf_vtree(&shadow_conf.vtree, filter_conf);
	bconf_vtree(&tvt, dbps->cfg);
	shadow_vtree_init(&ba.vchain, &shadow_conf, &tvt);

	call_template(&ba, "dbpoll");

	if (bpapi_has_key(&ba, "connerr")) {
		if (!dbps->visited && bconf_get_int_default(dbps->cfg, "startup_fail_fatal", 1)) {
			log_printf(LOG_CRIT, "dbpoller: Failed to poll db on start up: %s", bpapi_get_element(&ba, "connerr", 0));
			xerrx(1, "Failed to poll db on start up: %s", bpapi_get_element(&ba, "connerr", 0));
		}
		STATCNT_INC(dbps->connection_errors);
		goto out;
	}

	if (bpapi_length(&ba, "key") != dbps->num_rows) {
		log_printf(LOG_DEBUG, "dbpoller: Number of rows: %d in storage, %d fetched.", dbps->num_rows, bpapi_length(&ba, "key"));
		rebuild_storage = true;
	}

	for (int i = 0; i < bpapi_length(&ba, "modified_at"); i++) {
		const char *dbts = bpapi_get_element(&ba, "modified_at", i);
		if (!dbps->timestamp || strcmp(newest_ts, dbts) < 0) {
			newest_ts = dbts;
		}
	}

	if ((!dbps->timestamp && strlen(newest_ts)) || (dbps->timestamp && strcmp(dbps->timestamp, newest_ts) < 0)) {
		log_printf(LOG_DEBUG, "dbpoller: Timestamps not matching: %s %s", dbps->timestamp, newest_ts);
		rebuild_storage = true;
	}

	if (rebuild_storage) {
		log_printf(LOG_DEBUG, "dbpoller: Rebuilding storage.");
		struct storage *static_storage = bconf_daemon_get_storage(dbps->ds, STORAGE_STATIC);
		struct bconf_node *static_bconf = storage_get_reader(static_storage);

		struct bconf_node *s = NULL;
		dbps->num_rows = bpapi_length(&ba, "key");
		for (int i = 0; i < dbps->num_rows; i++) {
			const char *key = bpapi_get_element(&ba, "key", i);
			const char *value = bpapi_get_element(&ba, "value", i);

			/* paranoia: key can not be NULL due to restrain in the db but checking never hurts */
			if (!key || *key == '\0') {
				STATCNT_INC(dbps->invalid_keys);
				log_printf(LOG_CRIT, "Dynamic bconf: retrieved %s key", key ? "NULL" : "empty");
				continue;
			}
			if (key[0] == '.' || key[strlen(key) - 1] == '.' || strstr(key, "..")) {
				STATCNT_INC(dbps->invalid_keys);
				log_printf(LOG_CRIT, "Dynamic bconf: Skipping due to syntax error in entry %d stray '.' at %s=%s", i, key, value);
				continue;
			}

			/*
			 * Validate that the key is valid to be added to the file storage too
			 * otherwise merging the two configurations will bring the server down forever
			 */
			if (bconf_validate_key_conflict(static_bconf, key) == -1) {
				STATCNT_INC(dbps->conflicting_keys);
				log_printf(LOG_CRIT, "Dynamic bconf: skipping due to node list/value conflict with static bconf at %s=%s", key, value);
				continue;
			}

			if (bconf_add_data_canfail(&s, key, value) == -1) {
				STATCNT_INC(dbps->conflicting_keys);
				log_printf(LOG_CRIT, "Dynamic bconf: skipping due to node list/value conflict at %s=%s", key, value);
			}
		}
		storage_put(static_storage);
		log_printf(LOG_DEBUG, "dbpoller: Setting new timestamp %s", newest_ts);
		free(dbps->timestamp);
		dbps->timestamp = xstrdup(newest_ts);

		storage_replace_with_bconf(dbps->db, s);
		bconf_daemon_storage_updated(dbps->ds, false);
		stat_message_printf(dbps->last_updated, dbps->timestamp);
	}

out:
	dbps->visited = true;
	bpapi_free(&ba);
	filter_state_bconf_free(&filter_conf);
}

static void *
dbpoller_thread(void *arg) {
	struct dbpoller_state *dbps = arg;

	log_printf(LOG_DEBUG, "dbpoller: starting");
	dbps->running = true;

	while (dbps->running) {
		/* this should be a pthread condition... just in case people specify a really long interval. OR set specific cancellation points. */
		polldb(dbps);
		nanosleep(&dbps->interval, NULL);
	}

	log_printf(LOG_DEBUG, "dbpoller: stopping");
	return NULL;
}

bool
dbpoller_start(struct dbpoller_state *dbps) {
	if (!dbps)
		return false;

	int res = pthread_create(&dbps->dbpoller_thread, NULL, dbpoller_thread, dbps);
	if (res != 0) {
		log_printf(LOG_CRIT, "Failed to start dbpoller thread.");
		return false;
	}

	return true;
}

void
dbpoller_stop(struct dbpoller_state *dbps) {
	if (!dbps)
		return;

	if (dbps->running) {
		dbps->running = false;
		pthread_join(dbps->dbpoller_thread, NULL);
	}
}

struct dbpoller_state *
dbpoller_create(struct bconf_node *cfg, struct bconf_daemon_state *ds, struct storage *db) {
	struct dbpoller_state *dbps = zmalloc(sizeof(*dbps));
	int interval = bconf_get_int_default(cfg, "interval_ms", 1000); /* Poll every second per default */

	dbps->cfg = cfg;
	dbps->ds = ds;
	dbps->db = db;
	dbps->interval.tv_sec = interval / 1000;
	dbps->interval.tv_nsec = (interval % 1000) * 1000000;

	dbps->last_updated = stat_message_dynamic_alloc(2, "dbpoller", "last_updated");
	dbps->connection_errors = stat_counter_dynamic_alloc(3, "dbpoller", "errors", "connection");
	dbps->invalid_keys = stat_counter_dynamic_alloc(4, "dbpoller", "errors", "keys", "invalid");
	dbps->conflicting_keys = stat_counter_dynamic_alloc(4, "dbpoller", "errors", "keys", "conflicting");

	return dbps;
}

void
dbpoller_destroy(struct dbpoller_state **dbps) {
	if (!dbps || !*dbps)
		return;

	if ((*dbps)->running)
		dbpoller_stop(*dbps);

	stat_counter_dynamic_free((*dbps)->connection_errors);
	stat_counter_dynamic_free((*dbps)->invalid_keys);
	stat_counter_dynamic_free((*dbps)->conflicting_keys);
	stat_message_dynamic_free((*dbps)->last_updated);
	free((*dbps)->timestamp);
	free(*dbps);
	*dbps = NULL;
}
