#include <stdlib.h>

#include <bconf.h>
#include <bpapi.h>
#include <controller.h>
#include <logging.h>

#include "bconf_daemon.h"
#include "handler.h"
#include "bconf_overwrite.h"

void
bconf_overwrite(struct ctrl_req *cr, struct bpapi *ba, struct bconf_daemon_state *ds) {
	const char *key = bpapi_get_element(ba, "key", 0);
	const char *value = bpapi_get_element(ba, "value", 0);

	if (!key || !value) {
		ctrl_error(cr, 422, "bconf_overwrite missing param(s): %s %s",
			!key ? "key" : "",
			!value ? "value" : ""
		);
		return;
	}
	if (bpapi_length(ba, "key") > 1 || bpapi_length(ba, "value") > 1) {
		ctrl_error(cr, 422, "bconf_overwrite duplicate param(s): %s %s",
				bpapi_length(ba, "key") > 1 ? "key" : "",
				bpapi_length(ba, "value") > 1 ? "value" : ""
		);
		return;
	}

	struct storage *os = bconf_daemon_get_storage(ds, STORAGE_OVERWRITE);
	struct bconf_node **conf = storage_get_writer(os);

	if (bconf_add_data_canfail(conf, key, value) == -1) {
		storage_put(os);
		ctrl_error(cr, 500, "bconf_root: node list/value conflict at %s=%s", key, value); 
		return;
	}
	storage_put(os);

	bconf_daemon_storage_updated(ds, true);
	log_printf(LOG_DEBUG, "bconf_root: overwritten %s=%s", key, value);

	struct bconf_node **r = ctrl_get_bconfp(cr);
	bconf_add_data(r, "overwrite.status", "success");

	ctrl_output_json(cr, ba, "overwrite");
}

