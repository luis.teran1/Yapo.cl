#ifndef STORAGE_H
#define STORAGE_H
#include <stdbool.h>
#include <bconf.h>

#define STORAGE_DIGEST_STRING_LENGTH (16 + 1)

struct storage;
struct bconf_node *storage_get_reader(struct storage *s);
struct bconf_node **storage_get_writer(struct storage *s);
void storage_put(struct storage *s);
struct storage *storage_create_from_file(const char *path);
struct storage *storage_create_from_bconf(struct bconf_node *bc);
bool storage_reload_from_file(struct storage *s, const char *path);
void storage_replace_with_bconf(struct storage *s, struct bconf_node *bc);

void storage_get_digest(struct storage *s, char *dest, ssize_t dest_size);

struct bconf_node *storage_swap_db(struct storage *s, struct bconf_node *new);

void storage_free(struct storage **);
#endif
