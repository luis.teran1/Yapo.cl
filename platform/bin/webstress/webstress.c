
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <netdb.h>
#include <event.h>
#include <string.h>
#include <fcntl.h>
#include <time.h>
#include <err.h>
#include <errno.h>
#include <limits.h>

#include "buf_string.h"

struct request {
	struct addrinfo *addr;
	char *requeststr;
	size_t resid;
	size_t size;
	struct timespec time_start;
	struct timespec time_end;
	int result;
	ssize_t ressize;
	size_t bytes_read;
#define RESULT_TIMEOUT		1001
#define RESULT_BADSOCKET	1002
#define RESULT_DISCONNECT	1003
#define RESULT_WRITEERR		1004
#define RESULT_READERR		1005
#define RESULT_SHORTREAD	1006
#define RESULT_PROTOERR		1007
#define RESULT_BADCONNECT	1008
};

enum {PROTO_HTTP, PROTO_SEARCH, PROTO_GET, PROTO_TRANS};
static const char *proto_string[] = {
	MACRO_TO_STRING(PROTO_HTTP),
	MACRO_TO_STRING(PROTO_SEARCH),
	MACRO_TO_STRING(PROTO_GET),
	MACRO_TO_STRING(PROTO_TRANS),
};


#ifdef __APPLE__
int
clock_gettime(int what, struct timespec *ts)
{
	struct timeval tv;

	gettimeofday(&tv, NULL);

	ts->tv_sec = tv.tv_sec;
	ts->tv_nsec = tv.tv_usec * 1000;

	return (0);
}

#define CLOCK_MONOTONIC 1

#endif

struct request *requests;
int nreqs;
int curreq;
int timeout = 60;

struct requester {
	struct event ev_read;
	struct event ev_write;
	struct event ev_to;
	int fd;
	struct request *cur_request;
	int nreqs;
	int proto;
};

struct requester *requesters;
int nrequesters;
int pending;
const char *tag;

int verbose;

const char *result(int status);
void usage(void);
int parse_url(char *url, const char **server, const char **port, const char **request, int *proto);

void start_request(struct requester *);
void end_request(struct requester *, int);

void finish_connect(int, short, void *);
void read_ev(int, short, void *);
void write_ev(int, short, void *);
void timeout_ev(int, short, void *);

int
main(int argc, char **argv) {
	const char *server, *port, *request;
	char *req = NULL;
	struct addrinfo hints, *res, *res0;
	struct timespec start, end;
	int ch, i, error;
	double tm, maxt, mint, sumt;
	long long bytes;
	int cnt;
	int proto;
	FILE *listfile;
	const char *host = NULL;

	nreqs = 1000;
	nrequesters = 10;

	event_init();

	listfile = NULL;

	while ((ch = getopt(argc, argv, "vr:n:t:l:h:T:")) != -1) {
		switch (ch) {
		case 'v':
			verbose = 1;
			break;
		case 'n':
			nreqs = atoi(optarg);
			break;
		case 'r':
			nrequesters = atoi(optarg);
			break;
		case 't':
			timeout = atoi(optarg);
			break;
		case 'h':
			host = optarg;
			break;
		case 'l':
			if ((listfile = fopen(optarg, "r")) == NULL)
				usage();
			break;
		case 'T':
			tag = optarg;
			break;
		default:
			usage();
		}
	}

	argc -= optind;
	argv += optind;

	if (argc != 1)
		usage();

	if (parse_url(*argv, &server, &port, &request, &proto)) {
		fprintf(stderr, "URL badly formatted.\n");
		usage();
	}

	if (verbose)
		fprintf(stderr, "proto: %s ; server: %s ; port: %s ; request: %s\n", proto_string[proto], server, port, request);

	if ((requesters = malloc(nrequesters * sizeof(struct requester))) == NULL)
		err(1, "malloc");

	if ((requests = malloc(nreqs * sizeof(struct request))) == NULL)
		err(1, "malloc");

	memset(&hints, 0, sizeof(hints));
	hints.ai_family = PF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;

	error = getaddrinfo(server, port, &hints, &res0);
	if (error)
		errx(1, "%s", gai_strerror(error));

	res = res0;

	if (!host)
		host = server;

	for (i = 0; i < nreqs; i++) {
		char buffer[1024];
		struct buf_string trans_cmd = {NULL};
		requests[i].addr = res;
		if ((res = res->ai_next) == NULL)
			res = res0;
		requests[i].result = 0;

		if (listfile) {
again:
			if (fgets(buffer, sizeof(buffer), listfile) == NULL) {
				if (fseek(listfile, 0, SEEK_SET))
					err(1, "fseek");
				goto again;
			}
                        buffer[strlen(buffer)-1] = '\0';
			if (proto != PROTO_TRANS) {
				request = buffer;
			} else {
				if (strncmp(buffer, "end", 3) == 0) {
					bufcat(&trans_cmd.buf, &trans_cmd.len, &trans_cmd.pos, "%s", buffer);
					request = trans_cmd.buf;
				} else {
					bufcat(&trans_cmd.buf, &trans_cmd.len, &trans_cmd.pos, "%s\n", buffer);
					goto again;
				}
			}
		}

		switch (proto) {
		case PROTO_HTTP:
			if (asprintf(&req, "GET /%s HTTP/1.1\r\n"
				     "Host: %s:%s\r\n"
				     "User-Agent: Webstress/1.0\r\n"
				     "Accept: */*\r\n"
				     "Keep-Alive: 0\r\n"
				     "Connection: close\r\n"
				     "\r\n\r\n", request, host, port) == -1)
				err(1, "asprintf");
			break;
		case PROTO_SEARCH:
			if (tag) {
				size_t off = strcspn(request, " ");
				if (asprintf(&req, "%.*s _tag:%s%d %s\n", (int)off, request, tag, i, request + off) == -1)
					err(1, "asprintf");
			} else {
				if (asprintf(&req, "%s\n", request) == -1)
					err(1, "asprintf");
			}
			break;
		case PROTO_TRANS:
			if (asprintf(&req, "%s\n", request) == -1)
				err(1, "asprintf");
			if (trans_cmd.buf)
				free(trans_cmd.buf);
			break;
		case PROTO_GET:
			if (asprintf(&req, "%s\n", request) == -1)
				err(1, "asprintf");
			break;
		default:
			errx(1, "Unknown protocol (%d)", proto);
		} 


		requests[i].requeststr = req;
	}

	for (i = 0; i < nrequesters; i++) {
		requesters[i].nreqs = 0;
		requesters[i].cur_request = NULL; 
		requesters[i].proto = proto; 
	}

	if (clock_gettime(CLOCK_MONOTONIC, &start))
		err(1, "clock_gettime");
	
	for (i = 0; i < nrequesters; i++) {
		start_request(&requesters[i]);
	}

	while (pending) {
		event_loop(EVLOOP_ONCE);
	}

	if (clock_gettime(CLOCK_MONOTONIC, &end))
		err(1, "clock_gettime");

	bytes = 0;
	cnt = 0;
	maxt = 0;
	mint = 10E17;
	sumt = 0;
	for (i = 0; i < nreqs; i++) {
		struct request *req = &requests[i];
		double t;

		if (req->result == 200) {
			double st, et;

			cnt++;

			et = req->time_end.tv_sec * 1000000000.0 +
			    (double)req->time_end.tv_nsec;
			st = req->time_start.tv_sec * 1000000000.0 +
			    (double)req->time_start.tv_nsec;

			if (st > et)
				errx(1, "your clock sucks %f > %f\n", st, et);

			t = (et - st) / 1000000000.0;

			if (t > maxt)
				maxt = t;
			if (t < mint)
				mint = t;
			sumt += t;
		} else {
			fprintf(stderr, "request (%d) failed: %s\n%s\n", i, result(req->result), req->requeststr);
		}

		bytes += req->bytes_read;
	}

	tm = (end.tv_sec * 1000000000LL + end.tv_nsec - start.tv_sec * 1000000000LL - start.tv_nsec) / 1000000000.0;

	printf("Requests: %d\n", nreqs);
	printf("Errors: %d\n", nreqs - cnt);
	printf("Total time (successful requests): %.6f sec\n", tm);
	printf("Total bytes: %lld\n", bytes);
	printf("Bytes/sec: %f\n", (double)bytes / tm);
	printf("Average time (successful requests): %.6f sec\n", sumt / (double)cnt);
	printf("Min time (successful requests): %.6f sec\n", mint);
	printf("Max time (successful requests): %.6f sec\n", maxt);

	return 0;
}

void
usage(void) {
	fprintf(stderr, "usage: webstress [-v] [-n <requests>] [-r <concurrent>] [-l <replay log>] [-h <hostname>] [http|search|trans|get]://<server>[:<port>]/<request>\n");
	exit(1);
}

int
parse_url(char *url, const char **serverp, const char **portp, const char **requestp, int *p) {
	char *proto, *server, *port, *request;

	proto = url;

	server = strchr(proto, ':');
	if (server == NULL)
		return 1;

	*server++ = '\0';
	if (*server++ != '/' || *server++ != '/')
		return 1;

	request = strchr(server, '/');
	if (request == NULL)
		return 1;

	*request++ = '\0';

	/*
	 * port must be handled after the request since the NUL termination might
	 * will interfere with the separation of the request from the server.
	 */
	port = strchr(server, ':');
	if (port != NULL) {
		*port++ = '\0';
		*portp = port;
	} else {
		*portp = "80";
	}

	if (strcmp(proto, "http") == 0) {
		*p = PROTO_HTTP;
	} else if (strcmp(proto, "search") == 0) {
		*p = PROTO_SEARCH;
	} else if (strcmp(proto, "get") == 0) {
		*p = PROTO_GET;
	} else if (strcmp(proto, "trans") == 0) {
		*p = PROTO_TRANS;
	} else {
		return 1;
	}

	*serverp = server;
	*requestp = request;

	return 0;
}

void
read_ev(int fd, short ev, void *v)
{
	struct requester *reqs = v;
	struct request *req = reqs->cur_request;
	char buffer[65536];
	ssize_t sz;

	do {
		sz = read(reqs->fd, buffer, sizeof(buffer));

		if (sz < 0) {
			if (errno == EAGAIN)
				return;
			end_request(reqs, RESULT_READERR);
			return;
		}
		if (sz == 0) {
			end_request(reqs, req->result ? req->result : RESULT_DISCONNECT);
			return;
		}

		req->bytes_read += sz;

		switch (reqs->proto) {
		case PROTO_HTTP:
			if (req->result == 0) {
				char *stat;

				if (strncmp(buffer, "HTTP/1.", 7) ||
				    (stat = strchr(buffer, ' ')) == NULL ||
				    sscanf(stat + 1, "%d", &req->result) != 1) {
					fwrite(buffer, 1, (sz > 64 ? 64 : sz), stderr);
					end_request(reqs, RESULT_PROTOERR);
					return;
				}
			}
			break;
		case PROTO_SEARCH:
			if (req->result == 0) {
				if (strncmp(buffer, "info:", 5) != 0) {
					end_request(reqs, RESULT_PROTOERR);
					return;
				} else if (strncmp(buffer, "info:goaway", 11) == 0) {
					req->result = 503;
				} else if (strncmp(buffer, "info:yyerror", 11) == 0) {
					req->result = 400;
				} else {
					req->result = 200;
				}
			}
			break;
		case PROTO_GET:
			if (req->result == 0) {
				if (strncmp(buffer, "Error:", 6) == 0) {
					end_request(reqs, RESULT_PROTOERR);
					return;
				} else {
					req->result = 200;
				}
			}
			break;
		case PROTO_TRANS:
			if (req->result == 0) {
				if (strncmp(buffer, "521 Busy.", 9) == 0) {
					req->result = 503;
				} else if (strncmp(buffer, "220 Welcome.", 12) == 0) {
					req->result = 200;
				}
			}
			break;
		}
	} while (sz > 0);
}

void
write_ev(int fd, short ev, void *v)
{
	struct requester *reqs = v;
	struct request *req = reqs->cur_request;
	ssize_t sz;

	sz = write(reqs->fd, &req->requeststr[req->size - req->resid], req->resid);
	if (sz < 0) {
		end_request(reqs, RESULT_WRITEERR);
		return;
	}
	if ((req->resid -= sz) == 0) {
		event_del(&reqs->ev_write);
		event_add(&reqs->ev_read, NULL);
	}
}

void
timeout_ev(int fd, short ev, void *v)
{
	struct requester *reqs = v;

	end_request(reqs, RESULT_TIMEOUT);
}

void
start_request(struct requester *reqs)
{
	struct timeval tv;
	struct request *req;
	int val;

	static int lastprint;

	if (verbose && (curreq != lastprint && (curreq % 100) == 0))
		fprintf(stderr, "started %d requests\n", curreq);
	lastprint = curreq;


	if (curreq >= nreqs)
		return;
	req = &requests[curreq++];

	reqs->cur_request = req;
	if (clock_gettime(CLOCK_MONOTONIC, &req->time_start))
		err(1, "clock_gettime");

	tv.tv_sec = timeout;
	tv.tv_usec = 0;

	pending++;

	if ((reqs->fd = socket(req->addr->ai_family, req->addr->ai_socktype,
	    req->addr->ai_protocol)) == -1) {
		end_request(reqs, RESULT_BADSOCKET);
		return;
	}

	req->size = req->resid = strlen(req->requeststr);
	req->result = 0;
	req->ressize = -1;
	req->bytes_read = 0;

	evtimer_set(&reqs->ev_to, timeout_ev, reqs);
	event_set(&reqs->ev_read, reqs->fd, EV_READ|EV_PERSIST, read_ev, reqs);
	event_set(&reqs->ev_write, reqs->fd, EV_WRITE|EV_PERSIST, finish_connect, reqs);

	evtimer_add(&reqs->ev_to, &tv);

	if ((val = fcntl(reqs->fd, F_GETFL, 0)) < 0)
		err(1, "fcntl");
	val |= O_NONBLOCK;
	if (fcntl(reqs->fd, F_SETFL, val) < 0)
		err(1, "fcntl");

	if (connect(reqs->fd, req->addr->ai_addr, req->addr->ai_addrlen) < 0) {
		if (errno == EINPROGRESS) {
			event_add(&reqs->ev_write, NULL);
			return;
		}
		end_request(reqs, RESULT_BADCONNECT);
		return;
	}
	finish_connect(-1, 0, reqs);
}

void
finish_connect(int fd, short ev, void *v)
{
	struct requester *reqs = v;
	struct request *req = reqs->cur_request;
	int val;

	if (fd != -1) {	
		if (connect(reqs->fd, req->addr->ai_addr, req->addr->ai_addrlen) < 0) {
			if (errno == EINPROGRESS) {
				event_add(&reqs->ev_write, NULL);
				return;
			}
			end_request(reqs, RESULT_BADCONNECT);
			return;
		}
	}

	if ((val = fcntl(reqs->fd, F_GETFL, 0)) < 0)
		err(1, "fcntl");
	val |= O_NONBLOCK;
	if (fcntl(reqs->fd, F_SETFL, val) < 0)
		err(1, "fcntl");

	event_del(&reqs->ev_write);

	event_set(&reqs->ev_write, reqs->fd, EV_WRITE|EV_PERSIST, write_ev, reqs);

	event_add(&reqs->ev_write, NULL);
}

void
end_request(struct requester *reqs, int result)
{
	pending--;
	if (clock_gettime(CLOCK_MONOTONIC, &reqs->cur_request->time_end))
		err(1, "clock_gettime");
	reqs->cur_request->result = result;
#ifndef __APPLE__
	event_del(&reqs->ev_read);
	event_del(&reqs->ev_write);
#endif
	event_del(&reqs->ev_to);

	if (reqs->fd >= 0)
		close(reqs->fd);

	/* start a new one */
	start_request(reqs);
}

const char *
result(int status)
{
	static char static_buffer[1024];

	switch(status) {
	case 200:
		return "OK";
	case RESULT_TIMEOUT:
		return "Timeout";
	case RESULT_BADSOCKET:
		return "Bad socket";
	case RESULT_DISCONNECT:
		return "Server disconnect";
	case RESULT_WRITEERR:
		return "Write error";
	case RESULT_READERR:
		return "Read error";
	case RESULT_SHORTREAD:
		return "Short read";
	case RESULT_PROTOERR:
		return "Protocol error";
	case RESULT_BADCONNECT:
		return "Connection failed";
	case 400:
		return "Bad request";
	case 503:
		return "Resource busy";
	default:
		snprintf(static_buffer, sizeof(static_buffer), "unknown %d", status);
		return static_buffer;
	}
}
