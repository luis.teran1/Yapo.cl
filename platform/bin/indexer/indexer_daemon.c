#include <assert.h>
#include <fcntl.h>
#include <limits.h>
#include <netdb.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>

#include <curl/curl.h>

#include <bpapi.h>
#include <bitfield.h>
#include <daemon.h>
#include <index.h>
#include <controller.h>
#include <timer.h>
#include <logging.h>
#include <bconf.h>
#include <queue.h>
#include <ps_status.h>
#include <stat_counters.h>
#include <stat_messages.h>
#include <sha2.h>
#include <index_merge.h>
#include <json_vtree.h>
#include <patchvars.h>

#include "indexer.h"
#include "delta.h"

struct dc_thread {
	TAILQ_ENTRY(dc_thread) list;
	struct delta_consumer *dc;
	int idx;
	struct bconf_node *node;
};

enum {
	DAEMON_STOP_NORMAL = 1,
	DAEMON_STOP_ERROR
};

struct indexer_daemon_state {
	struct indexer_config *ic;

	const char *work_dir;

	struct stat_message *current_action;

	/*
	 * Active is the index that we know is being used for serving
	 * queries on the search engines.
	 */
	struct index_db *active_index;
	struct stat_message *active_to;
	struct stat_message *active_checksum;
	uint64_t *active_ndocs;

	char *active_fname;

	/*
	 * Pending is the index that is active+deltas since the last
 	 * switch of active. Will become the next active index.
	 *
	 * Can be NULL if no deltas have been applied to active yet.
	 */
	struct index_db *pending_index;
	struct stat_message *pending_to;
	struct stat_message *pending_checksum;
	uint64_t *pending_ndocs;

	char *pending_fname;
	char *pending_new_fname;

	/*
	 * This lock protects access to the pending and active
	 * indexes. While it's held, those pointers can't be
	 * updated and the indexes may not be closed.
	 */
	pthread_rwlock_t pending_and_active_rw;

	/*
	 * Delta currently being distributed to search egines and
	 * merged into pending.
	 * Can be NULL while being created and is already in sync
	 * with all search engines.
	 *
	 * The way for a thread to grab a delta from ids is to wait
	 * for the cond and grab a read lock before releasing the
	 * mutex.
	 */
	pthread_mutex_t delta_mtx;
	TAILQ_HEAD(,delta_control) delta_list;
	pthread_cond_t delta_cond;
	uint64_t *delta_cache_size;
	uint64_t delta_cache_limit;

	volatile int daemon_stop;
	volatile int full_index;

	struct delta_activation_policy {
		time_t timeout_seconds;
		time_t last_activation;

		int after_ndocs;
		int docs_seen;
		
	} delta_activation_policy;

	/*
	 * All the delta consumers run separate threads. The main thread
	 * handles the synchronization between them and index switching.
	 */
	struct delta_indexer *delta_indexer;
	TAILQ_HEAD(,dc_thread) delta_consumers;
	BITFIELD(dc_idx_map, MAX_CONSUMERS);
	TAILQ_HEAD(,dc_thread) stopped_delta_consumers;
	pthread_mutex_t delta_consumers_lock;

	struct timer_instance *ti;

	struct timespec last_heartbeat;
	int heartbeat_deltas;
	int heartbeat_loops;
};

void
indexer_daemon_stop(struct indexer_daemon_state *ids, bool fatal_error) {
	log_printf(LOG_DEBUG, "DAEMON STOP called");
	ids->daemon_stop = fatal_error ? DAEMON_STOP_ERROR : DAEMON_STOP_NORMAL;
}

void
indexer_daemon_full_index(struct indexer_daemon_state *ids) {
	ids->full_index = 1;
}

static void
pause_finish(struct ctrl_req *cr, struct bpapi *ba, void *v) {
	struct indexer_daemon_state *ids = v;

	delta_indexer_pause_resume(ids->delta_indexer, true);
}

static void
resume_finish(struct ctrl_req *cr, struct bpapi *ba, void *v) {
	struct indexer_daemon_state *ids = v;

	delta_indexer_pause_resume(ids->delta_indexer, false);
}

static void
add_var_finish(struct ctrl_req *cr, struct bpapi *ba, void *v) {
	struct indexer_daemon_state *ids = v;

	if (!bpapi_has_key(ba, "key") || !bpapi_has_key(ba, "value"))
		ctrl_error(cr, 400, "missing key or value");
	delta_indexer_add_var(ids->delta_indexer, bpapi_get_element(ba, "key", 0), bpapi_get_element(ba, "value", 0));

	/* Hack, but I think it's acceptable. */
	if (strcmp(bpapi_get_element(ba, "key", 0), "regress_fail_fsync") == 0) {
		extern int index_regress_fail_fsync;
		index_regress_fail_fsync = 1;
	}
}

static void
stop_finish(struct ctrl_req *cr, struct bpapi *ba, void *v) {
	struct indexer_daemon_state *ids = v;

	indexer_daemon_stop(ids, false);
	/* XXX controller should do this for us. */
	pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, NULL);
	pthread_setcanceltype(PTHREAD_CANCEL_ASYNCHRONOUS, NULL);
	while (1)
		pause();
}

static void
full_index_finish(struct ctrl_req *cr, struct bpapi *ba, void *v) {
	struct indexer_daemon_state *ids = v;

	indexer_daemon_full_index(ids);
}

static void
get_index_finish(struct ctrl_req *cr, struct bpapi *ba, void *v) {
	struct indexer_daemon_state *ids = v;
	const char *index = bpapi_get_element(ba, "index", 0);
	const char *csum = bpapi_get_element(ba, "have_csum", 0);
	struct index_db *idx = NULL;

	if (index && !strcmp(index, "active")) {
		idx = ids->active_index;
	} else if (index && !strcmp(index, "pending")) {
		idx = ids->pending_index;
	}
	if (idx == NULL) {
		ctrl_error(cr, 404, "unknown index");
		return;
	}

	if (!idx) {
		ctrl_error(cr, 503, "index not available yet");
		return;
	}

	if (csum && !strcmp(csum, index_get_checksum_string(idx))) {
		ctrl_status(cr, 204);
		return;
	}

	ctrl_set_raw_response_data(cr, idx->db_blob, idx->db_blob_size);
}

static struct dc_thread *
create_dc_thread(struct indexer_daemon_state *ids, struct bconf_node *node, bool aux, bool remove_if_down) {
	pthread_mutex_lock(&ids->delta_consumers_lock);

	uint64_t idx = bitfield_ffs(ids->dc_idx_map);
	if (idx == 0) {
		log_printf(LOG_CRIT, "Maximum number of consumers reached.");
		pthread_mutex_unlock(&ids->delta_consumers_lock);
		return NULL;
	}
	bitfield_clear(ids->dc_idx_map, --idx);

	struct dc_thread *th = zmalloc(sizeof(*th));
	th->dc = create_delta_consumer(ids, idx, node, aux, remove_if_down);
	th->idx = idx;
	if (th->dc)
		TAILQ_INSERT_TAIL(&ids->delta_consumers, th, list);

	pthread_mutex_unlock(&ids->delta_consumers_lock);

	if (!th->dc) {
		free(th);
		th = NULL;
	}
	return th;
}

void
indexer_daemon_stop_consumer(struct indexer_daemon_state *ids, struct delta_consumer *dc) {
	struct dc_thread *th;

	pthread_mutex_lock(&ids->delta_consumers_lock);
	TAILQ_FOREACH(th, &ids->delta_consumers, list) {
		if (th->dc == dc) {
			TAILQ_REMOVE(&ids->delta_consumers, th, list);
			bitfield_set(ids->dc_idx_map, th->idx);
			TAILQ_INSERT_TAIL(&ids->stopped_delta_consumers, th, list);
			break;
		}
	}
	pthread_mutex_unlock(&ids->delta_consumers_lock);
}

static void
register_consumer_finish(struct ctrl_req *cr, struct bpapi *ba, void *v) {
	struct indexer_daemon_state *ids = v;
	bool aux = atoi(bpapi_get_element(ba, "aux", 0) ?: "1");
	const char *host = bpapi_get_element(ba, "host", 0);
	char hostbuf[NI_MAXHOST];

	if (!host) {
		if (!ctrl_get_peer(cr, hostbuf, sizeof(hostbuf), NULL, 0, NI_NUMERICHOST)) {
			host = hostbuf;
		} else {
			ctrl_error(cr, 400, "Can't determine client address");
			return;
		}
	}

	const char *port = bpapi_get_element(ba, "port", 0);
	if (!port) {
		ctrl_error(cr, 400, "No port specified");
		return;
	}

	struct bconf_node *node = NULL;

	bconf_add_data(&node, "name", host);
	bconf_add_data(&node, "controller_port", port);

	struct dc_thread *th = create_dc_thread(ids, node, aux, true);
	if (!th) {
		ctrl_error(cr, 500, "Failed to create consumer");
		bconf_free(&node);
		return;
	}
	th->node = node;
	if (start_delta_consumer(th->dc)) {
		indexer_daemon_stop_consumer(ids, th->dc);
		ctrl_error(cr, 500, "Failed to start consumer");
	}
}

static void
activate_now_finish(struct ctrl_req *cr, struct bpapi *ba, void *v) {
	struct indexer_daemon_state *ids = v;
	struct delta_control *d = add_delta(ids, NULL, true, false);

	wait_for_everyone_to_consume_delta(ids, d, false);

	put_delta(ids, d, 1);
}

static int
setup_controllers(struct indexer_daemon_state *ids) {
	struct bconf_node *conf_node = bconf_get(ids->ic->root, "index.controller");
	struct ctrl_handler handlers[] = {
		ctrl_stats_handler,
		{ .url = "/stop", .finish = stop_finish, .cb_data = ids },
		{ .url = "/full_index", .finish = full_index_finish, .cb_data = ids },
		{ .url = "/get_index", .finish = get_index_finish, .cb_data = ids },
		{ .url = "/pause", .finish = pause_finish, .cb_data = ids },
		{ .url = "/resume", .finish = resume_finish, .cb_data = ids },
		{ .url = "/add_var", .finish = add_var_finish, .cb_data = ids },
		{ .url = "/register_consumer", .finish = register_consumer_finish, .cb_data = ids },
		{ .url = "/activate_now", .finish = activate_now_finish, .cb_data = ids },
	};
	struct ctrl_thread *ct;
	int nthreads;
	pthread_t *t;

	if (conf_node == NULL) {
		log_printf(LOG_CRIT, "no controller configured");
		return -1;
	}

	if ((ct = ctrl_thread_setup(conf_node, NULL, &t, &nthreads, handlers, sizeof(handlers) / sizeof(handlers[0]), -1)) == NULL) {
		log_printf(LOG_CRIT, "failed to set up controller %m");
		return -1;
	}
	return 0;
}

static int
setup_messages(struct indexer_daemon_state *ids) {
	ids->current_action = stat_message_dynamic_alloc(2, "indexer", "current_action");

	ids->active_to = stat_message_dynamic_alloc(4, "indexer", "index", "active", "to");
	ids->active_checksum = stat_message_dynamic_alloc(4, "indexer", "index", "active", "checksum");
	ids->active_ndocs = stat_counter_dynamic_alloc(4, "indexer", "index", "active", "ndocs");

	ids->pending_to = stat_message_dynamic_alloc(4, "indexer", "index", "pending", "to");
	ids->pending_checksum = stat_message_dynamic_alloc(4, "indexer", "index", "pending", "checksum");
	ids->pending_ndocs = stat_counter_dynamic_alloc(4, "indexer", "index", "pending", "ndocs");

	return 0;
}

static void
set_active_locked(struct indexer_daemon_state *ids, struct index_db *new_active, int close_old) {
	if (close_old && ids->active_index)
		close_index(ids->active_index, 0);
	ids->active_index = new_active;
	if (ids->active_index) {
		stat_message_printf(ids->active_to, "%s", index_get_to_id(ids->active_index));
		stat_message_printf(ids->active_checksum, "%s", index_get_checksum_string(ids->active_index));
		STATCNT_SET(ids->active_ndocs, index_get_num_docs(ids->active_index));
	} else {
		stat_message_printf(ids->active_to, "");
		stat_message_printf(ids->active_checksum, "");
		STATCNT_SET(ids->active_ndocs, 0);
	}
}

static void
set_active(struct indexer_daemon_state *ids, struct index_db *new_active, int close_old) {
	pthread_rwlock_wrlock(&ids->pending_and_active_rw);
	set_active_locked(ids, new_active, close_old);
	pthread_rwlock_unlock(&ids->pending_and_active_rw);
}

static void
set_pending_locked(struct indexer_daemon_state *ids, struct index_db *new_pending, int close_old) {
	if (close_old && ids->pending_index)
		close_index(ids->pending_index, 0);
	ids->pending_index = new_pending;
	if (ids->pending_index) {
		stat_message_printf(ids->pending_to, "%s", index_get_to_id(ids->pending_index));
		stat_message_printf(ids->pending_checksum, "%s", index_get_checksum_string(ids->pending_index));
		STATCNT_SET(ids->pending_ndocs, index_get_num_docs(ids->pending_index));
	} else {
		stat_message_printf(ids->pending_to, "");
		stat_message_printf(ids->pending_checksum, "");
		STATCNT_SET(ids->pending_ndocs, 0);
	}
}

static void
set_pending(struct indexer_daemon_state *ids, struct index_db *new_pending, int close_old) {
	pthread_rwlock_wrlock(&ids->pending_and_active_rw);
	set_pending_locked(ids, new_pending, close_old);
	pthread_rwlock_unlock(&ids->pending_and_active_rw);
}

static int
setup_workdir(struct indexer_daemon_state *ids) {
	/*
	 * Set up the workdir. We don't care if it already exists, but we need to be able
	 * to write to it.
	 */
	if ((ids->work_dir = bconf_get_string(ids->ic->root, "index.daemon.workdir")) == NULL) {
		log_printf(LOG_CRIT, "index.daemon.workdir not configured");
		return -1;
	}
	mkdir(ids->work_dir, 0755);
	if (access(ids->work_dir, W_OK) == -1) {
		log_printf(LOG_CRIT, "indexer_daemon: can't access workdir: %m");
		return -1;
	}

	/*
	 * These are the file names we'll be working with. Just
	 * pre-generate them here so that we have one place where it's
	 * done.
	 */
	xasprintf(&ids->active_fname, "%s/active", ids->work_dir);
	xasprintf(&ids->pending_fname, "%s/pending", ids->work_dir);
	xasprintf(&ids->pending_new_fname, "%s/pending.new", ids->work_dir);

	return 0;
}

static int
setup_pending_index(struct indexer_daemon_state *ids) {
	struct index_db *idx;

	/*
	 * If pending.new exists it means that the latest indexing was
	 * incomplete, since we don't commit a delta until all the
	 * renames have been done, we can safely remove it.
	 */
	if (access(ids->pending_new_fname, F_OK) == 0) {
		log_printf(LOG_WARNING, "indexer_daemon: pending.new [%s] exists", ids->pending_new_fname);
		if (remove(ids->pending_new_fname)) {
			log_printf(LOG_CRIT, "indexer_daemon: remove pending.new [%s]: %m", ids->pending_new_fname);
			return -1;
		}
	}
	/*
	 * An existing pending needs to be readable by us.
	 */
	if (access(ids->pending_fname, F_OK) == 0) {
		if ((idx = open_index(ids->pending_fname, INDEX_READER|INDEX_FILE)) == NULL) {
			log_printf(LOG_CRIT, "indexer_daemon: open_index(pending) failed");
			return -1;
		}
		set_pending(ids, idx, 1);
	}

	/* pending.old is dealt with during merging, we don't need to worry about it. */

	return 0;
}

static int
full_index_to_pending(struct indexer_daemon_state *ids) {
	set_pending(ids, NULL, 1);

	if (access(ids->pending_new_fname, F_OK) == 0) {
		if (remove(ids->pending_new_fname)) {
			log_printf(LOG_CRIT, "indexer_daemon: remove pending.new [%s]: %m", ids->pending_new_fname);
			return -1;
		}
	}
	if (access(ids->pending_fname, F_OK) == 0) {
		if (remove(ids->pending_fname)) {
			log_printf(LOG_CRIT, "indexer_daemon: remove pending [%s]: %m", ids->pending_fname);
			return -1;
		}
	}

	stat_message_printf(ids->current_action, "full index");
	log_printf(LOG_INFO, "indexer_daemon: doing a full index as requested");
	int retval;
	if ((retval = index_once(ids->ic, ids->ti, ids->pending_fname, 1)) != 0)
		return retval;

	struct index_db *idx;
	if ((idx = open_index(ids->pending_fname, INDEX_READER|INDEX_FILE)) == NULL) {
		return -1;
	}

	set_pending(ids, idx, 1);
	return 0;
}

static int
meta_cmp(struct bconf_node *a, struct bconf_node *b) {
	const char *a_to_id = bconf_get_string_default(a, "state.to_id", "");
	const char *b_to_id = bconf_get_string_default(b, "state.to_id", "");

	return strverscmp(a_to_id, b_to_id);
}

static int
fetch_newest_index(struct indexer_daemon_state *ids) {
	struct dc_thread *dc;
	struct bconf_node *newest = NULL;
	struct delta_consumer *newest_dc = NULL;
	int ret;
	long status;
	CURLcode res;
	int newest_idx = -1;

	TAILQ_FOREACH(dc, &ids->delta_consumers, list) {
		struct bconf_node *meta = NULL;

		log_printf(LOG_DEBUG, "indexer_daemon: %s: fetching meta from consumer %d", __func__, dc->idx);
		res = talk_to_client(dc->dc, "get_meta?index=active", NULL, 0, NULL, NULL, &meta, &status);
		if (res == CURLE_OK && status == 200) {
			if (meta_cmp(newest, meta) < 0) {
				log_printf(LOG_DEBUG, "indexer_daemon: %s: found newer index at consumer %d", __func__, dc->idx);
				bconf_free(&newest);
				newest = meta;
				newest_dc = dc->dc;
				newest_idx = dc->idx;
			} else {
				bconf_free(&meta);
			}
		} else if (res != CURLE_OK) {
			log_printf(LOG_WARNING, "indexer_daemon: %s failed to fetch meta from consumer %d: %s", __func__, dc->idx, curl_easy_strerror(res));
		} else {
			log_printf(LOG_WARNING, "indexer_daemon: %s failed to fetch meta from consumer %d: HTTP status %ld", __func__, dc->idx, status);
		}
	}

	if (!newest) {
		log_printf(LOG_WARNING, "indexer_daemon: %s could not find a reusable index at any consumer", __func__);
		ret = -1;
		goto out;
	}

	int fd = open(ids->active_fname, O_RDWR|O_CREAT|O_EXCL|O_TRUNC, 0644);
	if (fd == -1) {
		log_printf(LOG_CRIT, "indexer_daemon: open(%s): %m", ids->active_fname);
		ret = -1;
		goto out;
	}

	log_printf(LOG_WARNING, "indexer_daemon: %s fetching from consumer %d", __func__, newest_idx);
	ret = delta_consumer_fetch_index(newest_dc, fd);
	close(fd);

	if (!ret) {
		struct index_db *new_active = open_index(ids->active_fname, INDEX_READER|INDEX_FILE);
		if (new_active) {
			set_active(ids, new_active, 1);
		} else {
			log_printf(LOG_CRIT, "Failed to open fetched index");
			unlink(ids->active_fname);
			ret = -1;
		}
	}

out:
	bconf_free(&newest);
	return ret;
}

static int
setup_active_index(struct indexer_daemon_state *ids, int remote_fetch) {
	struct index_db *idx;
	int retval;

	pthread_rwlock_init(&ids->pending_and_active_rw, NULL);

	stat_message_printf(ids->current_action, "opening active index");

	if (access(ids->active_fname, F_OK) || (idx = open_index(ids->active_fname, INDEX_READER|INDEX_FILE)) == NULL) {
		/* We failed to open an index at startup, create a full index. */
		log_printf(LOG_INFO, "indexer_daemon: no pre-existing index.");

		if (remote_fetch) {
			log_printf(LOG_INFO, "indexer_daemon: searching for latest index among consumers");
			if (fetch_newest_index(ids) == 0) {
				return 0;
			}
		}

		stat_message_printf(ids->current_action, "full index");
		log_printf(LOG_INFO, "indexer_daemon: doing a full index %s", remote_fetch ? "during startup" : "as requested");
		if ((retval = index_once(ids->ic, ids->ti, ids->active_fname, 1)) != 0)
			return retval;

		/* Try again. */	
		log_printf(LOG_DEBUG, "indexer_daemon: trying to open index");
		if ((idx = open_index(ids->active_fname, INDEX_READER|INDEX_FILE)) == NULL) {
			return -1;
		}

	}

	set_active(ids, idx, 1);

	return 0;
}

static int
check_pending_activation_policy(struct indexer_daemon_state *ids, int ndocs, bool force_activate) {
	struct delta_activation_policy *dap = &ids->delta_activation_policy;
	struct timespec ts;

	clock_gettime(CLOCK_MONOTONIC, &ts);

	if (force_activate ||
	    (dap->timeout_seconds && (ts.tv_sec - dap->last_activation) > dap->timeout_seconds) ||
	    (dap->after_ndocs && (dap->docs_seen += ndocs) > dap->after_ndocs)) {
		dap->last_activation = ts.tv_sec;
		dap->docs_seen = 0;
		return 1;
	}

	return 0;
}

int
put_delta(struct indexer_daemon_state *ids, struct delta_control *d, int wait_until_last) {
	int ret = 0;

	pthread_mutex_lock(&ids->delta_mtx);
	/* XXX - if there's more than one wait_until_last caller we are royally screwed. */
	/* We might want to use a semaphore instead of a ref count. */
	while (wait_until_last && d->ref > 1) {
		pthread_cond_wait(&ids->delta_cond, &ids->delta_mtx);
	}
	if (--d->ref == 0) {
		pthread_cond_broadcast(&ids->delta_cond);		
		TAILQ_REMOVE(&ids->delta_list, d, list);
		pthread_mutex_unlock(&ids->delta_mtx);

		if (d->delta_index) {
			STATCNT_ADD(ids->delta_cache_size, -d->delta_index->db_blob_size);
			stat_message_dynamic_free(d->delta_from);
			stat_message_dynamic_free(d->delta_to);
			stat_message_dynamic_free(d->delta_checksum);
			stat_counter_dynamic_free(d->delta_ndocs);
			close_index(d->delta_index, 0);
		}
		pthread_mutex_destroy(&d->lock);
		pthread_cond_destroy(&d->signal);
		free(d->pending_to_merge_csum);
		free(d->pending_to_activate_csum);
		free(d);
		ret = 1;
	} else {
		pthread_cond_broadcast(&ids->delta_cond);		
		pthread_mutex_unlock(&ids->delta_mtx);
	}
	return ret;
}

struct delta_control *
add_delta(struct indexer_daemon_state *ids, struct index_db *new_delta, bool force_activate, bool sync_pending_before) {
	struct delta_control *d = calloc(1, sizeof(*d));

	if (new_delta) {
		const char *to = index_get_to_id(new_delta);
		char dname[64];

		snprintf(dname, sizeof(dname), "delta%s", to);
		STATCNT_ADD(ids->delta_cache_size, new_delta->db_blob_size);

		d->delta_index = new_delta;
		d->delta_from = stat_message_dynamic_alloc(4, "indexer", "index", dname, "from");
		stat_message_printf(d->delta_from, "%s", index_get_from_id(new_delta));
		d->delta_to = stat_message_dynamic_alloc(4, "indexer", "index", dname, "to");
		stat_message_printf(d->delta_to, "%s", to);
		d->delta_checksum = stat_message_dynamic_alloc(4, "indexer", "index", dname, "checksum");
		stat_message_printf(d->delta_checksum, "%s", index_get_checksum_string(new_delta));
		d->delta_ndocs = stat_counter_dynamic_alloc(4, "indexer", "index", dname, "ndocs");
		STATCNT_SET(d->delta_ndocs, index_get_num_docs(new_delta));
	}

	pthread_mutex_init(&d->lock, NULL);
	pthread_cond_init(&d->signal, NULL);
	d->state = dsQueued;

	d->sync_pending_before = sync_pending_before;
	d->activate_after = check_pending_activation_policy(ids, new_delta ? index_get_num_docs(new_delta) : 0, force_activate);

	pthread_mutex_lock(&ids->delta_mtx);
	d->ref = 1;
	TAILQ_INSERT_TAIL(&ids->delta_list, d, list);
	pthread_cond_broadcast(&ids->delta_cond);
	pthread_mutex_unlock(&ids->delta_mtx);
	return d;
}

struct timespec
timeout_from_now(const struct timespec *ts) {
	struct timespec abstime;
	clock_gettime(CLOCK_REALTIME, &abstime);
	abstime.tv_sec += ts->tv_sec;
	if ((abstime.tv_nsec += ts->tv_nsec) >= 1000000000) {
		abstime.tv_sec++;
		abstime.tv_nsec -= 1000000000;
	}
	return abstime;
}

/* Requires holding ids->delta_mtx */
static struct delta_control *
get_next_candidate_delta(struct indexer_daemon_state *ids, struct delta_control *delta, const struct timespec *abstime) {
	if (delta)
		delta = TAILQ_NEXT(delta, list);
	else
		delta = TAILQ_FIRST(&ids->delta_list);
	if (delta)
		return delta;

	while (pthread_cond_timedwait(&ids->delta_cond, &ids->delta_mtx, abstime) == 0) {
		if ((delta = TAILQ_FIRST(&ids->delta_list)))
			return delta;
	}
	return NULL;
}

static struct delta_control *
get_queued_delta(struct indexer_daemon_state *ids, const struct timespec *ts) {
	struct timespec abstime = timeout_from_now(ts);
	struct delta_control *delta = NULL;

	pthread_mutex_lock(&ids->delta_mtx);
	while ((delta = get_next_candidate_delta(ids, delta, &abstime))) {
		if (delta->state == dsQueued)
			break;
	}
	if (delta)
		delta->ref++;
	pthread_mutex_unlock(&ids->delta_mtx);
	return delta;
}

struct delta_control *
get_delta_for_merge(struct indexer_daemon_state *ids, int idx, const struct timespec *ts) {
	struct timespec abstime = timeout_from_now(ts);
	struct delta_control *delta = NULL;

	pthread_mutex_lock(&ids->delta_mtx);
	while ((delta = get_next_candidate_delta(ids, delta, &abstime))) {
		if (delta->state != dsReadyForMerge && delta->state != dsCommittable)
			continue;
		if (idx < 0 || !bitfield_union3_isset(delta->client_merged, delta->client_failed, delta->client_aux, idx))
			break;
	}
	if (delta)
		delta->ref++;
	pthread_mutex_unlock(&ids->delta_mtx);
	return delta;
}

static void
check_delta_committable(struct indexer_daemon_state *ids, struct delta_control *d) {
	if (d->state != dsReadyForMerge)
		return;
	if (d->indexer_merged != dimNotMerged && !bitfield_iszero(d->client_merged)) {
		d->state = dsCommittable;
		log_printf(LOG_DEBUG, "delta committable");
	} else if (bitfield_union2_check_complement_all_set(d->client_failed, d->client_aux, ids->dc_idx_map)) {
		d->state = dsFailed;
		log_printf(LOG_DEBUG, "all delta clients failed");
	}
}

static bool
check_delta_recoverable(struct indexer_daemon_state *ids, struct delta_control *d) {
	wait_for_everyone_to_consume_delta(ids, d, false);
	assert(d->state == dsFailed);
	return bitfield_compare_equal(d->client_failed, d->client_recoverable);
}

void
delta_client_merged(struct indexer_daemon_state *ids, struct delta_control *d, int idx, bool success, bool aux, bool recoverable) {
	pthread_mutex_lock(&d->lock);
	if (aux)
		bitfield_set(d->client_aux, idx);
	else if (success)
		bitfield_set(d->client_merged, idx);
	else
		bitfield_set(d->client_failed, idx);
	if (recoverable)
		bitfield_set(d->client_recoverable, idx);
	check_delta_committable(ids, d);
	pthread_cond_broadcast(&d->signal);
	pthread_mutex_unlock(&d->lock);
}

static struct index_db *
indexer_daemon_merge_delta(struct indexer_daemon_state *ids, struct index_db *delta) {
	struct index_merge_state im = {
		.update = 1,
		.nfrom = 2,
	};
	struct index_db *ret = NULL;

	im.from_db[0] = indexer_daemon_retain_pending(ids);
	im.from_db[1] = delta;

	if ((im.to = open_index(ids->pending_new_fname, INDEX_WRITER|INDEX_FILE)) == NULL) {
		log_printf(LOG_CRIT, "indexer_daemon_merge_delta: open_index(dest)");
		goto out;
	}

	if (index_perform_merge(&im, ids->ti) == -1) {
		log_printf(LOG_CRIT, "indexer_daemon_merge_delta: perform_merge failed");
		goto out;
	}

	ret = im.to;

out:
	indexer_daemon_release_pending(ids, im.from_db[0]);
	return ret;
}

bool
indexer_daemon_verify_checksums(struct indexer_daemon_state *ids, const char *s_active, const char *s_pending, bool *act_m, bool *pend_m) {
	pthread_rwlock_rdlock(&ids->pending_and_active_rw);
	*act_m = (!s_active == !ids->active_index) && (!s_active || (!strcmp(s_active, index_get_checksum_string(ids->active_index))));
	*pend_m = (!s_pending == !ids->pending_index) && (!s_pending || (!strcmp(s_pending, index_get_checksum_string(ids->pending_index))));
	pthread_rwlock_unlock(&ids->pending_and_active_rw);
	return *act_m && *pend_m;
}

struct index_db *
indexer_daemon_retain_pending(struct indexer_daemon_state *ids) {
	pthread_rwlock_rdlock(&ids->pending_and_active_rw);
	return ids->pending_index ?: ids->active_index;
}

void
indexer_daemon_release_pending(struct indexer_daemon_state *ids, struct index_db *pending_db) {
	pthread_rwlock_unlock(&ids->pending_and_active_rw);
}

int
indexer_daemon_prime_update(struct indexer_daemon_state *ids, struct index_db *db, char **old_to) {
	struct index_db *base_index = indexer_daemon_retain_pending(ids);
	int ret;

	ret = index_prime_update(db, base_index);
	if (ret == 0) {
		const char *to;
		if ((to = index_get_to_id(base_index))) {
			*old_to = strdup(to);
		} else {
			*old_to = NULL;
		}
	}

	indexer_daemon_release_pending(ids, base_index);
	return ret;
}

static int
setup_deltas(struct indexer_daemon_state *ids) {
	pthread_mutex_init(&ids->delta_mtx, NULL);
	TAILQ_INIT(&ids->delta_list);
	pthread_cond_init(&ids->delta_cond, NULL);
	ids->delta_cache_size = stat_counter_dynamic_alloc(3, "indexer", "delta_cache", "size");
	ids->delta_cache_limit = bconf_get_int(ids->ic->root, "index.daemon.delta_cache.limit") ?: 1000000000;
	return 0;
}

static int
setup_consumers(struct indexer_daemon_state *ids) {
	const char *client_list;
	struct bconf_node *clients;
	int i;

	TAILQ_INIT(&ids->delta_consumers);

	if ((client_list = bconf_get_string(ids->ic->root, "index.daemon.client_list")) == NULL) {
		log_printf(LOG_CRIT, "indexer_daemon: index.daemon.client_list not configured");
		return -1;
	}
	clients = bconf_get(ids->ic->root, client_list);

	struct bconf_node *aux_clients = NULL;
	if ((client_list = bconf_get_string(ids->ic->root, "index.daemon.client_list_aux")))
		aux_clients = bconf_get(ids->ic->root, client_list);

	for (i = 0; i < bconf_count(clients); i++) {
		if (!create_dc_thread(ids, bconf_byindex(clients, i), false, false))
			return -1;
	}

	for (i = 0; i < bconf_count(aux_clients); i++) {
		if (!create_dc_thread(ids, bconf_byindex(aux_clients, i), true, false))
			return -1;
	}
	return 0;
}

static int
setup_activation_policy(struct indexer_daemon_state *ids) {
	struct timespec ts;

	clock_gettime(CLOCK_MONOTONIC, &ts);
	ids->delta_activation_policy.last_activation = ts.tv_sec;

	ids->delta_activation_policy.timeout_seconds = bconf_get_int(ids->ic->root, "index.daemon.activation_policy.timeout_s");
	ids->delta_activation_policy.after_ndocs = bconf_get_int(ids->ic->root, "index.daemon.activation_policy.documents");

	if (ids->delta_activation_policy.timeout_seconds == 0 &&
	    ids->delta_activation_policy.after_ndocs == 0) {
		log_printf(LOG_WARNING, "index.daemon.activation_policy not configured, defaulting to reloading indexes after 300 seconds");
		ids->delta_activation_policy.timeout_seconds = 300;	/* Default to 5 minutes. */
	}
	return 0;
}

static int
setup_threads(struct indexer_daemon_state *ids) {
	ids->delta_indexer = create_delta_indexer(ids->ic, ids, ids->current_action);
	if (!ids->delta_indexer)
		return -1;

	struct dc_thread *dc;
	TAILQ_FOREACH(dc, &ids->delta_consumers, list) {
		if (start_delta_consumer(dc->dc))
			return -1;
	}
	return 0;
}

static int
stop_threads(struct indexer_daemon_state *ids) {

	if (stop_delta_indexer(ids->delta_indexer))
		return -1;

	struct dc_thread *dct;
	while (1) {
		pthread_mutex_lock(&ids->delta_consumers_lock);
		dct = TAILQ_FIRST(&ids->delta_consumers);
		if (dct) {
			TAILQ_REMOVE(&ids->delta_consumers, dct, list);
			bitfield_set(ids->dc_idx_map, dct->idx);
		}
		pthread_mutex_unlock(&ids->delta_consumers_lock);
		if (!dct)
			break;

		if (stop_delta_consumer(dct->dc))
			return -1;

		bconf_free(&dct->node);
		free(dct);
	}
	return 0;
}

static void
gc_threads(struct indexer_daemon_state *ids) {
	pthread_mutex_lock(&ids->delta_consumers_lock);
	struct dc_thread *dct;
	while ((dct = TAILQ_FIRST(&ids->stopped_delta_consumers))) {
		TAILQ_REMOVE(&ids->stopped_delta_consumers, dct, list);
		stop_delta_consumer(dct->dc);
		bconf_free(&dct->node);
		free(dct);
	}
	pthread_mutex_unlock(&ids->delta_consumers_lock);
}

static void
pause_resume_threads(struct indexer_daemon_state *ids, bool pause) {
	if (pause) {
		bool paused;
		do {
			struct delta_control *d;
			const struct timespec to = { .tv_sec = 0, .tv_nsec = 100000000 }; /* 100ms */

			paused = delta_indexer_pause_and_check_pause_state(ids->delta_indexer, &to);
			while ((d = get_queued_delta(ids, &to)) != NULL) {
				pthread_mutex_lock(&d->lock);
				d->state = dsFailed;
				pthread_cond_broadcast(&d->signal);
				pthread_mutex_unlock(&d->lock);
				wait_for_everyone_to_consume_delta(ids, d, false);
				put_delta(ids, d, 0);
			}
		} while (!paused);
	} else {
		delta_indexer_pause_resume(ids->delta_indexer, pause);
	}
	pthread_mutex_lock(&ids->delta_consumers_lock);
	struct dc_thread *dct;
	TAILQ_FOREACH(dct, &ids->delta_consumers, list) {
		delta_consumer_pause_resume(dct->dc, pause);
	}
	pthread_mutex_unlock(&ids->delta_consumers_lock);
}

static enum delta_state
adjust_delta_from_to(struct index_db *base, struct index_db *delta) {
	const char *b_to = index_get_to_id(base);
	const char *d_from = index_get_from_id(delta);
	const char *d_to = index_get_to_id(delta);

	int cmp = b_to ? strverscmp(b_to, d_to) : 0;

	if (cmp > 0) {
		log_printf(LOG_CRIT, "Delta to_id lower than base, %s < %s", d_to, b_to);
		return dsFailed;
	}

	cmp = b_to ? strverscmp(b_to, d_from) : 0;
	if (cmp != 0) {
		/* Adjust to match */
		log_printf(LOG_DEBUG, "Adjusting delta from_id %s => %s", d_from, b_to);
		index_set_from_id(delta, b_to);
	}

	return dsReadyForMerge;
}

void
wait_for_everyone_to_consume_delta(struct indexer_daemon_state *ids, struct delta_control *d, bool and_pending) {
	const struct timespec ts = { .tv_sec = 0, .tv_nsec = 100000000 };

	pthread_mutex_lock(&d->lock);
	while (!bitfield_union3_check_complement_all_set(d->client_merged, d->client_failed, d->client_aux, ids->dc_idx_map) || d->indexer_merged == dimNotMerged) {
		struct timespec abstime = timeout_from_now(&ts);

		/* Ignore return value, we need to recheck periodically as number of clients might change. */
		pthread_cond_timedwait(&d->signal, &d->lock, &abstime);
	}

	if (and_pending) {
		while (d->state != dsFailed && d->indexer_merged != dimMergedAndPending) {
			struct timespec abstime = timeout_from_now(&ts);
			pthread_cond_timedwait(&d->signal, &d->lock, &abstime);
		}
	}

	pthread_mutex_unlock(&d->lock);
}

static int
indexer_activate_pending(struct indexer_daemon_state *ids) {
	if (ids->pending_index == NULL)
		return 0;
	pthread_rwlock_wrlock(&ids->pending_and_active_rw);
	if (rename(ids->pending_fname, ids->active_fname)) {
		log_printf(LOG_CRIT, "rename pending->active failed: %m");
		pthread_rwlock_unlock(&ids->pending_and_active_rw);
		return -1;		/* This is a fatal error. */
	}
	set_active_locked(ids, ids->pending_index, 1);
	set_pending_locked(ids, NULL, 0);
	pthread_rwlock_unlock(&ids->pending_and_active_rw);

	return 0;
}

static void
check_and_log_heartbeat(struct indexer_daemon_state *ids) {
	struct timespec ts;
	const int secs = 30;
	int s;

	clock_gettime(CLOCK_MONOTONIC, &ts);

	if (ids->last_heartbeat.tv_sec == 0) {
		ids->last_heartbeat = ts;
	}

	ids->heartbeat_loops++;

	if ((s = ts.tv_sec - ids->last_heartbeat.tv_sec) > secs) {
		log_printf(LOG_INFO, "indexer daemon: %d delta indexings and %d non-empty deltas in the last %d seconds", ids->heartbeat_loops, ids->heartbeat_deltas, s);
		ids->heartbeat_loops = ids->heartbeat_deltas = 0;
		ids->last_heartbeat = ts;
	}
}

static int
set_new_pending(struct indexer_daemon_state *ids, struct index_db *new_pending, struct delta_control *delta) {
	if (rename(ids->pending_new_fname, ids->pending_fname)) {
		log_printf(LOG_CRIT, "set_new_pending: rename pending.new -> pending [%s] -> [%s]: %m", ids->pending_new_fname, ids->pending_fname);
		close_index(new_pending, 0);
		return -1;
	}
	set_pending(ids, new_pending, 1);

	pthread_mutex_lock(&delta->lock);
	delta->pending_to_activate_csum = xstrdup(index_get_checksum_string(new_pending)); /* always set, better safe than confused */
	delta->indexer_merged = dimMergedAndPending;
	pthread_cond_broadcast(&delta->signal);
	pthread_mutex_unlock(&delta->lock);

	return 0;
}

static void
fail_and_put(struct indexer_daemon_state *ids, struct delta_control *delta) {
	/* Probably don't really need to lock here */
	pthread_mutex_lock(&delta->lock);
	delta->state = dsFailed;
	pthread_cond_broadcast(&delta->signal);
	pthread_mutex_unlock(&delta->lock);

	put_delta(ids, delta, 0);
}

static int
one_delta_loop(struct indexer_daemon_state *ids) {
	struct timespec ts = { .tv_sec = 1 };

	check_and_log_heartbeat(ids);

	struct delta_control *d = get_queued_delta(ids, &ts);

	if (!d) {
		/* Successfully idled */

		/* Deal with activation timeouts even when we're idle. */
		if (check_pending_activation_policy(ids, 0, 0)) {
			d = add_delta(ids, NULL, true, false);
		} else {
			log_printf(LOG_DEBUG, "one_delta_loop: no delta, no activation");
			return 0;
		}
	}
	ids->heartbeat_deltas++;

	if (d->delta_index == NULL) {
		int ret;
		log_printf(LOG_DEBUG, "one_delta loop without delta");

		char *pending_csum;

		pthread_rwlock_rdlock(&ids->pending_and_active_rw);	/* overkill, this thread is the only writer */
		if (ids->pending_index == NULL) {
			/*
			 * We can end up here if someone forces an activation and there's no pending index.
			 * We set pending_to_activate_csum in delta_control to NULL and the clients will
			 * treat that as a NOP.
			 */
			pending_csum = NULL;
		} else {
			pending_csum = xstrdup(index_get_checksum_string(ids->pending_index));
		}
		pthread_rwlock_unlock(&ids->pending_and_active_rw);

		pthread_mutex_lock(&d->lock);
		d->pending_to_activate_csum = pending_csum;
		d->state = dsReadyForMerge;
		d->indexer_merged = dimMergedAndPending;
		pthread_cond_broadcast(&d->signal);
		pthread_mutex_unlock(&d->lock);

		wait_for_everyone_to_consume_delta(ids, d, false);

		ret = indexer_activate_pending(ids);
		put_delta(ids, d, 0);
		log_printf(LOG_DEBUG, "one_delta_loop: no delta, activate returns %d", ret);
		return ret;
	}

	struct index_db *base_index = indexer_daemon_retain_pending(ids);
	d->pending_to_merge_csum = xstrdup(index_get_checksum_string(base_index));
	enum delta_state newstate = adjust_delta_from_to(base_index, d->delta_index);
	indexer_daemon_release_pending(ids, base_index);

	pthread_mutex_lock(&d->lock);
	d->state = newstate;
	pthread_cond_broadcast(&d->signal);
	pthread_mutex_unlock(&d->lock);

	if (newstate == dsFailed) {
		put_delta(ids, d, 0);
		log_printf(LOG_DEBUG, "one_delta_loop: base index mismatch, reindex all the things");
		return FATAL_REINDEX;
	}

	struct index_db *new_pending = indexer_daemon_merge_delta(ids, d->delta_index);

	pthread_mutex_lock(&d->lock);
	if (new_pending == NULL) {
		log_printf(LOG_CRIT, "indexer_daemon: merge failed");
		d->state = dsFailed;
	} else {
		d->indexer_merged = dimMergedNotPending;
		log_printf(LOG_DEBUG, "delta merged");
		check_delta_committable(ids, d);
	}
	pthread_cond_broadcast(&d->signal);
	/* Wait for at least one distribution to succeed or all distributions to fail. */
	while (d->state != dsCommittable && d->state != dsFailed) {
		pthread_cond_wait(&d->signal, &d->lock);
	}
	pthread_mutex_unlock(&d->lock);

	if (!new_pending) {
		put_delta(ids, d, 0);
		log_printf(LOG_DEBUG, "one_delta_loop: merge failed, death of the indexer");
		return -1;
	}

	if (d->state == dsFailed) {
		close_index(new_pending, 0);
		if (remove(ids->pending_new_fname) == -1) {
			log_printf(LOG_CRIT, "one_delta_loop: removing pending.new failed. There will be an unrecoverable error soon: %m");
		}
		bool recoverable = check_delta_recoverable(ids, d);
		put_delta(ids, d, 0);
		log_printf(LOG_DEBUG, "one_delta_loop: delta failed and it is `%srecoverable`, but we pretend it's always recoverable", recoverable ? "" : "un");
#if 1
		return FATAL_RECOVERABLE;
#else
		return recoverable ? FATAL_RECOVERABLE : -1;
#endif
	}

	if (index_writer_downgrade_to_reader(new_pending)) {
		log_printf(LOG_CRIT, "indexer_daemon: downgrade pending failed.");
		close_index(new_pending, 0);
		fail_and_put(ids, d);
		log_printf(LOG_DEBUG, "one_delta_loop: downgrade failed and this is unrecoverable");
		return -1;
	}

	/*
	 * In case a client needs to sync pending before receiving the
	 * delta we can't set a new pending until everyone has consumed the delta.
	 * Another solution would be for the client to wait until the pending has
	 * been set and distribute that, but this will deadlock or fail the delta
	 * if there's just one client. And it's much harder to get right.
	 */
	wait_for_everyone_to_consume_delta(ids, d, false);

	if (set_new_pending(ids, new_pending, d)) {
		fail_and_put(ids, d);
		log_printf(LOG_DEBUG, "one_delta_loop: setting new pending failed and this is unrecoverable");
		return -1;
	}

	int activate = d->activate_after;

	if (activate) {
		if (indexer_activate_pending(ids)) {
			put_delta(ids, d, 0);
			log_printf(LOG_DEBUG, "one_delta_loop: activate pending failed and this is unrecoverable");
			return -1;
		}
	}

	put_delta(ids, d, 0);

	log_printf(LOG_DEBUG, "one_delta_loop: everything is ok");
	return 0;
}

static void
cleanup_indexer_daemon_state(struct indexer_daemon_state *ids) {
	free(ids->active_fname);
	free(ids->pending_fname);
	free(ids->pending_new_fname);
	stat_message_dynamic_free(ids->current_action);
	stat_message_dynamic_free(ids->active_to);
	stat_message_dynamic_free(ids->active_checksum);
	stat_counter_dynamic_free(ids->active_ndocs);
	stat_message_dynamic_free(ids->pending_to);
	stat_message_dynamic_free(ids->pending_checksum);
	stat_counter_dynamic_free(ids->pending_ndocs);
	stat_counter_dynamic_free(ids->delta_cache_size);
	close_index(ids->active_index, 0);
	if (ids->pending_index)
		close_index(ids->pending_index, 0);
}

int
indexer_daemon(struct indexer_config *ic, struct timer_instance *ti) {
	set_ps_display("BOS");
	set_respawn_backoff_attrs(3, 120, 1.4f); /* Configure BOS respawn delay with backoff */
	daemonify_here(false);

	set_ps_display("initializing");

	struct indexer_daemon_state ids = { .ic = ic, .ti = ti };
	int retval;

	bitfield_setall(ids.dc_idx_map);

	curl_global_init(CURL_GLOBAL_ALL);

	pthread_mutex_init(&ids.delta_consumers_lock, NULL);
	TAILQ_INIT(&ids.stopped_delta_consumers);

	if ((retval = setup_messages(&ids)) ||
	    (retval = setup_activation_policy(&ids)) ||
	    (retval = setup_workdir(&ids)) ||
	    (retval = setup_controllers(&ids)) ||
	    (retval = setup_consumers(&ids)) ||
	    (retval = setup_active_index(&ids, 1)) ||
	    (retval = setup_pending_index(&ids)) ||
	    (retval = setup_deltas(&ids)) ||
	    (retval = setup_threads(&ids)))
		return retval;

	while (!ids.daemon_stop) {
		struct delta_control *full_index_activation_delta = NULL;
		if (ids.full_index) {
			pause_resume_threads(&ids, true);

			if ((retval = full_index_to_pending(&ids)))
				break;

			ids.full_index = 0;

			full_index_activation_delta = add_delta(&ids, NULL, true, true);

			pause_resume_threads(&ids, false);
		}

		set_ps_display("waiting for delta");

		int r;

		switch ((r = one_delta_loop(&ids))) {
		case 0:
			break;
		case FATAL_REINDEX:
			ids.full_index = 1;
			break;
		case FATAL_RECOVERABLE:
			nanosleep(&((struct timespec){ .tv_sec = 1 }), NULL);
			break;
		default:
			log_printf(LOG_DEBUG, "default case after one_delta_loop: %d", r);
			ids.daemon_stop = DAEMON_STOP_ERROR;
			break;
		}
		if (full_index_activation_delta) {
			/* We can be sure that others (including us in one_delta_loop) will have seen and consumed the delta. */
			put_delta(&ids, full_index_activation_delta, 0);
		}

		gc_threads(&ids);
	}

	stop_threads(&ids);

	if (!retval && ids.daemon_stop == DAEMON_STOP_ERROR)
		retval = 1;

	cleanup_indexer_daemon_state(&ids);

	return retval;
}

