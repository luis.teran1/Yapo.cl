
#include <libpq-fe.h>
#include <stdlib.h>
#include <stdio.h>
#include <getopt.h>
#include <ctype.h>
#include <limits.h>
#include <signal.h>
#include <unistd.h>

#include "logging.h"
#include "bconf.h"
#include "ctemplates.h"
#include "bpapi.h"
#include "daemon.h"
#include "util.h"
#include "transapi.h"
#include "bconfig.h"
#include "index.h"
#include "charmap.h"
#include "hunstem.h"
#include "synize.h"
#include "ps_status.h"
#include "queue.h"
#include "strl.h"
#include "settings.h"
#include "special_key.h"
#include "hash.h"
#include "langmap.h"
#include "hunstem.h"
#include "stat_counters.h"

#include "indexer.h"

static char *config_file;
static char *bconf_file;

void (*indexer_local_addwords)(struct document *doc, void *hun_obj, const char *lang, struct docpos *dpos, void *cfdata, struct syn_regexes *syn_regexes, struct bconf_node *root, const char *text);
int (*indexer_local_setup)(struct bconf_node *);
void (*indexer_local_exit)(void);

static void
usage(char *app) {
	fprintf(stderr, "Usage: %s <engine> [<var>=<val> ...]\n", app);
	exit(1);
}

static struct bconf_node *
parse_options(struct indexer_config *ic, int argc, char **argv) {
	struct bconf_node *bconf_override = NULL;
	int opt;

	while (1) {
		int option_index = 0;
		static struct option long_options[] = {
			{"db_name", 1, 0, 0},
			{"config", 1, 0, 0},
			{"update_db", 0, 0, 0},
			{"update-db", 0, 0, 0},
			{"regress-fail-fsync", 0, 0, 0},
			{"bc-override", 1, 0, 0 },
			{"bc-file", 1, 0, 0 },
			{"index-file", 1, 0, 0 },
			{"pidfile", 1, 0, 0},
			{"quick-start", 0, 0, 0},
			{"help", 0, 0, 0},
			{0, 0, 0, 0}
		};

		opt = getopt_long (argc, argv, "", long_options, &option_index);
		if (opt == -1)
			break;

		if (opt == 0) {
			switch (option_index) {
				case 0:
					bconf_add_data(&bconf_override, "db_name", optarg);
					break;
				case 1:
					config_file = strdup(optarg);
					break;
				case 2:
				case 3:
					ic->update_db = 1;
					break;
				case 4:
					{
						extern int index_regress_fail_fsync;
						index_regress_fail_fsync = 1;
					}
					break;
				case 5:
					{
						char *k = strdup(optarg);
						char *v = strchr(k, '=');
						if (v == NULL)
							usage(argv[0]);
						*v++ = '\0';
						bconf_add_data(&bconf_override, k, v);
						free(k);
					}
					break;
				case 6:
					bconf_file = strdup(optarg);
					break;
				case 7:
					bconf_add_data(&bconf_override, "db_file", optarg);
					ic->index_is_file = 1;
					break;
				case 8:
					set_pidfile(optarg);
					break;
				case 9:
					set_quick_start(1);
					break;
				default:
					usage(argv[0]);
			}
		}
	}
	return bconf_override;
}


static void
timer_dump(struct timer_class *tc, void *v) {
	struct timespec avgts;
	double avg = (double)tc->tc_total.tv_sec + (double)tc->tc_total.tv_nsec / 1000000000.0;

	if (tc->tc_count == 0)
		return;

	avg /= (double)tc->tc_count;

	avgts.tv_sec = avg;
	avgts.tv_nsec = (avg - (double)avgts.tv_sec) * 1000000000.0;

	log_printf(LOG_DEBUG, "Timer: %s.count:%lld", tc->tc_name, tc->tc_count);
	log_printf(LOG_DEBUG, "Timer: %s.total:%d.%03ld", tc->tc_name, (int)tc->tc_total.tv_sec, tc->tc_total.tv_nsec / 1000000);
	if (tc->tc_count > 1) {
		log_printf(LOG_DEBUG, "Timer: %s.min:%d.%03ld", tc->tc_name, (int)tc->tc_min.tv_sec, tc->tc_min.tv_nsec / 1000000);
		log_printf(LOG_DEBUG, "Timer: %s.max:%d.%03ld", tc->tc_name, (int)tc->tc_max.tv_sec, tc->tc_max.tv_nsec / 1000000);
		log_printf(LOG_DEBUG, "Timer: %s.average:%d.%03ld", tc->tc_name, (int)avgts.tv_sec, avgts.tv_nsec / 1000000);
	}
}

static void
stats_dump(void *v, uint64_t cnt, const char **name) {
	char nbuf[1024] = "";
	int i;

	if (!cnt)
		return;

	for (i = 0; name[i] != NULL; i++) {
		if (i)
			strlcat(nbuf, ".", sizeof(nbuf));
		strlcat(nbuf, name[i], sizeof(nbuf));
	}
	log_printf(LOG_DEBUG, "Stats: %s: %llu", nbuf, (unsigned long long)cnt);
}

static void
commit_doc(struct indexing_state *is, struct document *doc) {
	if (doc->id.id >= 0) {
		if (bconf_get_int(is->ic->root, "index.adwords") && doc_num_attributes(doc) > 0) {
			const char *doc_word;
			int doc_words_pos = 0;
			char doc_words[2048];

			tcmapiterinit(doc->words);
			while ((doc_word = tcmapiternext2(doc->words)) != NULL) {
				int word_len;

				if (index_is_stopword(is->db, doc_word))
					continue;

				word_len = strlen(doc_word);
				if (word_len && (doc_words_pos + word_len + 1) < 2048) {
					if (doc_words_pos)
						doc_words[doc_words_pos++] = ' ';
					memcpy(doc_words + doc_words_pos, doc_word, word_len);
					doc_words_pos += word_len;
				}
			}
			doc_words[doc_words_pos] = '\0';
			doc_add_attr(doc, "adwords", doc_words, AATTR);
		}

		if (doc->id.suborder >= 0 && doc_num_attributes(doc) == 0) {
			log_printf(LOG_WARNING, "Not storing empty document id %d", doc->id.id);
			close_doc(doc);
		} else {
			if (store_doc(is->db, doc, 1) != 0)
				log_printf(LOG_CRIT, "error storing %d", doc->id.id);
			is->row_tot++;
		}
	} else {
		close_doc(doc);
	}
}

struct document_data {
	struct buf_string buf;
	struct bpapi_vtree_chain *vtree;
	struct indexing_state *is;
	int mask;
};

static int
init_document(const struct bpfilter *filter, struct bpapi *api, int argc, const struct tpvar **argv) {
	struct document_data *data = BPAPI_OUTPUT_DATA(api);
	int mask = AIND | AATTR;

	switch (argc) {
	case 2:
		if (!tpvar_int(argv[1]))
			mask &= ~AATTR;
	case 1:
		if (!tpvar_int(argv[0]))
			mask &= ~AIND;
	}

	data->vtree = &api->vchain;
	data->is = filter_state(api, "idxs", NULL, NULL);
	data->mask = mask;

	if (data->is->ic->shard_size && !data->is->ic->update_db && !data->is->ic->indexer_daemonize && data->is->row_tot && (data->is->row_tot % data->is->ic->shard_size == 0)) {
		timer_handover(data->is->ti, "reopen");
		log_printf(LOG_DEBUG, "flushing");
		if (close_index(data->is->db, 0) != 0) {
			log_printf(LOG_CRIT, "error closing index");
			exit(1);
		}
		data->is->db = index_prime_new(data->is->ic->index_name, ++data->is->shard, 0, (data->is->ic->index_is_file ? INDEX_FILE : 0));
	}

	timer_handover(data->is->ti, "one_ad");

	return 0;
}

static void
fini_document(struct bpapi_output_chain *ochain) {
	struct document_data *data = ochain->data;
	struct document *doc = data->is->gdoc ?: open_doc(data->is->db);
	const char *lang = bconf_get_string(data->is->ic->root, "default.lang");
	int relevance = bconf_get_int(data->is->ic->root, "index.relevance");
	char *line;
	char *nl;
	char *cp;
	char *ep;
	struct docpos dpos = { 0 };
	void *hun_obj = data->is->ic->single_lang_hun_obj;

	if (data->is->fatal)
		return;

	if (!data->is->gdoc) {
		doc->id.id = -1;
		doc->id.order = doc->id.suborder = 0;
	}

	char* original_buf = data->is->ic->debug_render_fh ? xstrdup(data->buf.buf) : NULL;
	size_t original_buf_size = data->buf.pos;

	for (line = data->buf.buf ; line ; line = nl) {
		int ind = 0;
		int addwords = 0;
		int rawwords = 0;
		int nopos = 0;
		int force = 0;
		const char *k = line;
		char *ks;

		dpos.flags = dpos.rel_boost = 0;

		nl = strchr(line, '\n');
		if (nl)
			*nl++ = '\0';

		while (isspace(*line))
			line++;

		ep = cp = strchr(line, ':');
		if (!cp)
			continue; /* Ignore invalid lines. */

		while (isspace(*++cp))
			;
		if (!*cp)
			continue;

		while (--ep >= line && isspace(*ep))
			;
		if (ep == line)
			continue;

		for (ks = line ; ks <= ep ; ) {
			while (isspace (*ks))
				ks++;
			k = ks;
			while (ks <= ep && !isspace (*ks) && *ks != '=')
				ks++;

			switch (lookup_special_key(k, ks - k)) {
			case SK_NONE:
				break;
			case SK_ATTR:
				ind |= AATTR;
				break;
			case SK_FORCEATTR:
				force |= AATTR;
				break;
			case SK_IND:
				ind |= AIND;
				break;
			case SK_FORCEIND:
				force |= AIND;
				break;
			case SK_WORDS:
				addwords = 1;
				break;
			case SK_NOPOS:
				nopos = 1;
				break;
			case SK_POSFLAGS:
				dpos.flags = strtol(ks + 1, NULL, 0);
				break;
			case SK_ID:
				doc->id.id = atoi(cp);
				if (doc->id.id < 0) {
					log_printf(LOG_WARNING, "Ignoring negative id = %d", doc->id.id);
					doc->id.id = -1;
				}
				break;
			case SK_ORDER:
				doc->id.order = atoi(cp);
				if (doc->id.order < 0) {
					log_printf(LOG_WARNING, "Ignoring negative order for id = %d", doc->id.id);
					doc->id.order = 0;
				}
				break;
			case SK_SUBORDER:
				if (!doc->id.suborder) {
					doc->id.suborder = atoi(cp);
					if (doc->id.suborder < 0) {
						log_printf(LOG_WARNING, "Ignoring negative suborder for id = %d", doc->id.id);
						doc->id.suborder = 0;
					}
				}
				break;
			case SK_SUBAD_PARENT_ID:
				if (!doc->id.suborder) {
					doc->id.suborder = -atoi(cp);
					if (doc->id.suborder > 0) {
						log_printf(LOG_WARNING, "Ignoring negative subad parent id for id = %d", doc->id.id);
						doc->id.suborder = 0;
					}
				}
				break;
			case SK_LANG:
				lang = cp;
				hun_obj = hash_table_search(data->is->ic->hun_objs, lang, -1, NULL);
				break;
			case SK_RAWWORDS:
				rawwords = 1;
				break;
			case SK_BOOST:
				if (relevance)
					dpos.rel_boost = strtol(ks + 1, NULL, 0);
				break;
			}
			while (ks <= ep && !isspace (*ks))
				ks++;
		}

		*ks = '\0';
		/*log_printf(LOG_DEBUG, "doc_add_attr({%d, %d, %d}, \"%s\", \"%s\", %s)", doc->id.id, doc->id.order, doc->id.suborder, k, cp, ((ind & data->mask) | force) == (AIND | AATTR) ? "AIND | AATTR" : ((ind & data->mask) | force) == AIND ? "AIND" : ((ind & data->mask) | force) == AATTR ? "AATTR" : "0");*/
		doc_add_attr(doc, k, cp, (ind & data->mask) | force);

                if (addwords) {
			if (indexer_local_addwords) {
				indexer_local_addwords(doc, hun_obj, lang, nopos ? NULL : &dpos, data->is->ic->atc.cfdata, data->is->ic->atc.syn_regexes, data->is->ic->root, cp);
			} else {
				if (!hun_obj) {
					log_printf(LOG_DEBUG, "No hunspelling set up for language %s, id: %d", lang, doc->id.id);
				} else {
					/* This should be what's hashed per lang instead of hun_obj. */
					struct add_text_config atc = data->is->ic->atc;		/* populate the defaults. */

					atc.lang = lang;
					atc.hun_obj = hun_obj;

					add_text(&atc, doc, cp, nopos ? NULL : &dpos);
				}
			}
		}

		if (rawwords) {
			/*
			 * When adding raw words, each word on the same line is added at the same position.
			 * We always start with *cp being non-whitspace and non-nul.
			 */
			do {
				char *ep;
				for (ep = cp; !isspace(*ep) && *ep; ep++)
					;
				doc_add_word(doc, cp, ep - cp, nopos ? NULL : &dpos);
				cp = ep;
				while (isspace(*cp))
					cp++;
			} while (*cp);

			if (!nopos)
				dpos.pos++;
		}
	}

	if (data->is->ic->debug_render_fh) {
		fprintf(data->is->ic->debug_render_fh, "\n---[ id:%d, order:%d, suborder:%d, render size:%zd ]---\n", doc->id.id, doc->id.order, doc->id.suborder, original_buf_size);
		fwrite(original_buf, original_buf_size, 1, data->is->ic->debug_render_fh);
		free(original_buf);
	}

	free(data->buf.buf);

	if (!data->is->gdoc)
		commit_doc(data->is, doc);
}

const struct bpapi_output document_output = {
	bufstring_outstring_fmt,
	bufstring_outstring_raw,
	fini_document
};

ADD_OUTPUT_FILTER(document, sizeof(struct document_data));

static int
init_open_doc(const struct bpfilter *filter, struct bpapi *api, int argc, const struct tpvar **argv) {
	struct indexing_state *is = filter_state(api, "idxs", NULL, NULL);

	if (is->gdoc)
		close_doc(is->gdoc);

	is->gdoc = open_doc(is->db);
	is->gdoc->id.id = -1;
	is->gdoc->id.order = is->gdoc->id.suborder = 0;
	return 0;
}

ADD_FILTER(open_doc, NULL, 0, NULL, 0, NULL, 0);

static int
init_store_doc(const struct bpfilter *filter, struct bpapi *api, int argc, const struct tpvar **argv) {
	struct indexing_state *is = filter_state(api, "idxs", NULL, NULL);
	if (is->gdoc) {
		commit_doc(is, is->gdoc);
		is->gdoc = NULL;
	}
	return 0;
}

ADD_FILTER(store_doc, NULL, 0, NULL, 0, NULL, 0);

static int
init_switch_doc(const struct bpfilter *filter, struct bpapi *api, int argc, const struct tpvar **argv) {
	struct indexing_state *is = filter_state(api, "idxs", NULL, NULL);

	if (is->gdoc) {
		int id = tpvar_int(argv[0]);

		if (is->gdoc->id.id >= 0 && is->gdoc->id.id != id) {
			commit_doc(is, is->gdoc);
			is->gdoc = open_doc(is->db);
			is->gdoc->id.order = is->gdoc->id.suborder = 0;
		}
		is->gdoc->id.id = id;
	}
	return 0;
}

ADD_FILTER(switch_doc, NULL, 0, NULL, 0, NULL, 0);

static const struct tpvar *
current_doc_id(struct bpapi *api, struct tpvar *dst, enum bpcacheable *cc, int argc, const struct tpvar **argv) {
	struct indexing_state *is = filter_state(api, "idxs", NULL, NULL);
	
	*cc = BPCACHE_CANT;
	if (is->gdoc)
		return TPV_SET(dst, TPV_INT(is->gdoc->id.id));
	return TPV_SET(dst, TPV_NULL);
}

ADD_TEMPLATE_FUNCTION(current_doc_id);

static const struct tpvar *
doc_has_attr(struct bpapi *api, struct tpvar *dst, enum bpcacheable *cc, int argc, const struct tpvar **argv) {
	struct indexing_state *is = filter_state(api, "idxs", NULL, NULL);

	*cc = BPCACHE_CANT;
	if (is->gdoc && argc == 1) {
		char *fa;

		int res = doc_has_attribute(is->gdoc, tpvar_str(argv[0], &fa));
		
		free(fa);

		return TPV_SET(dst, TPV_INT(res));
	}
	return TPV_SET(dst, TPV_NULL);
}

ADD_TEMPLATE_FUNCTION(doc_has_attr);

struct idx_buf_filter_data {
	struct buf_string bs;
	struct indexing_state *is;
	int argc;
	const struct tpvar **argv;
};

static int
init_idx_buf_filter(const struct bpfilter *filter, struct bpapi *api, int argc, const struct tpvar **argv) {
	struct idx_buf_filter_data *data = BPAPI_OUTPUT_DATA(api);
	data->is = filter_state(api, "idxs", NULL, NULL);
	data->argc = argc;
	data->argv = argv;
	return 0;
}

#define init_delete_document init_idx_buf_filter

static void
fini_delete_document(struct bpapi_output_chain *ochain) {
	struct idx_buf_filter_data *data = ochain->data;

	if (data->is->fatal)
		return;

	if (data->bs.buf) {
		if (delete_doc(data->is->db, atoi(data->bs.buf)))
			log_printf(LOG_DEBUG, "Failed to delete %s", data->bs.buf);
		free(data->bs.buf);
	}
}

struct bpapi_output delete_document_output = {
	bufstring_outstring_fmt,
	bufstring_outstring_raw,
	fini_delete_document,
	NULL
};

ADD_OUTPUT_FILTER(delete_document, sizeof(struct idx_buf_filter_data));

struct add_conf_data {
	struct buf_string bs;
	struct indexing_state *is;
	const char *key;
	char *fk;
	int indexer_only;
};

static int
init_add_conf(const struct bpfilter *filter, struct bpapi *api, int argc, const struct tpvar **argv) {
	struct add_conf_data *data = BPAPI_OUTPUT_DATA(api);
	data->is = filter_state(api, "idxs", NULL, NULL);

	if (argc < 1)
		return 0;

	data->key = tpvar_str(argv[0], &data->fk);
	if (argc >= 2)
		data->indexer_only = tpvar_int(argv[1]);
	return 0;
}

static void
fini_add_conf(struct bpapi_output_chain *ochain) {
	struct add_conf_data *data = ochain->data;

	if (data->is->fatal)
		return;
	
	if (strspn(data->key, "\n\r\t=") != 0 || strspn(data->bs.buf, "\n\r\t") != 0) {
		log_printf(LOG_CRIT, "reindex: bad conf add_conf(%s,%s)", data->key, data->bs.buf);
		return;
	}

	if (data->bs.buf) {
		if (data->key) {
			if (!data->indexer_only)
				index_add_conf(data->is->db, data->key, data->bs.buf);
			bconf_add_data((struct bconf_node **)/*XXX*/&data->is->ic->root, data->key, data->bs.buf);
		}
		free(data->bs.buf);
	}
	free(data->fk);
}

struct bpapi_output add_conf_output = {
	bufstring_outstring_fmt,
	bufstring_outstring_raw,
	fini_add_conf,
	NULL
};

ADD_OUTPUT_FILTER(add_conf, sizeof(struct add_conf_data));

static int
init_verify_conf(const struct bpfilter *filter, struct bpapi *api, int argc, const struct tpvar **argv) {
	struct indexing_state *is = filter_state(api, "idxs", NULL, NULL);
	char *old = xstrdup(index_get_conf_footprint(is->db));

	free(is->db->conf_footprint);	/* XXX */
	is->db->conf_footprint = NULL;

	const char *new = index_get_conf_footprint(is->db);

	if (strcmp(old, new) != 0) {
		log_printf(LOG_CRIT, "reindex: Footprint mismatch");
		is->fatal = FATAL_REINDEX;
	}
	free(old);
	return 0;
}

ADD_FILTER(verify_conf, NULL, 0, NULL, 0, NULL, 0);

#define init_set_to_id init_idx_buf_filter

static void
fini_set_to_id(struct bpapi_output_chain *ochain) {
	struct idx_buf_filter_data *id = ochain->data;

	if (id->bs.buf) {
		const char *oldid;
		if (id->argc >= 1 && tpvar_int(id->argv[0]) && (oldid = index_get_to_id(id->is->db))) {
			if (strverscmp(id->bs.buf, oldid) > 0)
				index_set_to_id(id->is->db, id->bs.buf);
		} else {
			index_set_to_id(id->is->db, id->bs.buf);
		}
		free(id->bs.buf);
	}
}

const struct bpapi_output set_to_id_output = {
	bufstring_outstring_fmt,
	bufstring_outstring_raw,
	fini_set_to_id
};

ADD_OUTPUT_FILTER(set_to_id, sizeof(struct idx_buf_filter_data));

#define init_set_from_id init_idx_buf_filter

static void
fini_set_from_id(struct bpapi_output_chain *ochain) {
	struct idx_buf_filter_data *id = ochain->data;

	if (id->bs.buf) {
		const char *oldid;
		if (id->argc >= 1 && tpvar_int(id->argv[0]) && (oldid = index_get_from_id(id->is->db))) {
			if (strverscmp(id->bs.buf, oldid) < 0)
				index_set_from_id(id->is->db, id->bs.buf);
		} else {
			index_set_from_id(id->is->db, id->bs.buf);
		}
		free(id->bs.buf);
	}
}

const struct bpapi_output set_from_id_output = {
	bufstring_outstring_fmt,
	bufstring_outstring_raw,
	fini_set_from_id
};

ADD_OUTPUT_FILTER(set_from_id, sizeof(struct idx_buf_filter_data));

static const struct tpvar *
to_id(struct bpapi *api, struct tpvar *dst, enum bpcacheable *cc, int argc, const struct tpvar **argv) {
	struct indexing_state *is = filter_state(api, "idxs", NULL, NULL);

	return TPV_SET(dst, TPV_STRING(index_get_to_id(is->db)));
}

ADD_TEMPLATE_FUNCTION(to_id);

static const struct tpvar *
from_id(struct bpapi *api, struct tpvar *dst, enum bpcacheable *cc, int argc, const struct tpvar **argv) {
	struct indexing_state *is = filter_state(api, "idxs", NULL, NULL);

	return TPV_SET(dst, TPV_STRING(index_get_from_id(is->db)));
}

ADD_TEMPLATE_FUNCTION(from_id);

static int
init_set_next_batch_delay_ms(const struct bpfilter *filter, struct bpapi *api, int argc, const struct tpvar **argv) {
	struct indexing_state *is = filter_state(api, "idxs", NULL, NULL);
	if (argc != 1)
		return 1;
	is->daemon_next_delay_ms = tpvar_int(argv[0]);
	return 0;
}
ADD_FILTER(set_next_batch_delay_ms, NULL, 0, NULL, 0, NULL, 0);

static int
init_fatal(const struct bpfilter *filter, struct bpapi *api, int argc, const struct tpvar **argv) {
	struct idx_buf_filter_data *data = BPAPI_OUTPUT_DATA(api);
	data->is = filter_state(api, "idxs", NULL, NULL);

	data->is->fatal = argc > 0 ? tpvar_int(argv[0]) : 1;

	if (!data->is->fatal) {
		const char *v;
		char *fv;

		v = tpvar_str(argv[0], &fv);

		if (strcmp(v, "fail") == 0)
			data->is->fatal = 1;
		else if (strcmp(v, "reindex") == 0)
			data->is->fatal = FATAL_REINDEX;
		else if (strcmp(v, "nochange") == 0)
			data->is->fatal = FATAL_NOCHANGES;
		else
			data->is->fatal = 1;
		free(fv);
	}
	return 0;
}

static void
fini_fatal(struct bpapi_output_chain *ochain) {
	struct idx_buf_filter_data *data = ochain->data;

	if (data->bs.buf) {
		const char *f = "fatal";
		int lvl = LOG_CRIT;

		switch (data->is->fatal) {
		case FATAL_REINDEX:
			f = "reindex";
			lvl = LOG_ERR;
			break;
		case FATAL_NOCHANGES:
			f = "nochange";
			lvl = LOG_INFO;
			break;
		}
		log_printf(lvl, "%s (%d): %s", f, data->is->fatal, data->bs.buf);
		free(data->bs.buf);
	}
}

struct bpapi_output fatal_output = {
	bufstring_outstring_fmt,
	bufstring_outstring_raw,
	fini_fatal,
	NULL
};

ADD_OUTPUT_FILTER(fatal, sizeof(struct idx_buf_filter_data));

struct log_data {
	struct buf_string buf;
	int level;
};

static void
fini_log(struct bpapi_output_chain *ochain) {
	struct log_data *data = ochain->data;

	if (data->buf.buf) {
		log_printf(data->level, "%s", data->buf.buf);
		free(data->buf.buf);
	}
}

struct bpapi_output log_output = {
	bufstring_outstring_fmt,
	bufstring_outstring_raw,
	fini_log,
	NULL
};

#define LOG_FILTER(name, lvl) \
	static int \
	init_ ## name(const struct bpfilter *filter, struct bpapi *api, int argc, const struct tpvar **argv) { \
		struct log_data *data = BPAPI_OUTPUT_DATA(api); \
		data->level = lvl; \
		return 0; \
	} \
	ADD_FILTER(name, &log_output, sizeof(struct log_data), NULL, 0, NULL, 0)

LOG_FILTER(crit, LOG_CRIT);
LOG_FILTER(warn, LOG_WARNING);
LOG_FILTER(info, LOG_INFO);
LOG_FILTER(debug, LOG_DEBUG);

struct timer_handover_data {
	struct buf_string bs;
	struct indexing_state *is;
}; 

static void
fini_timer_handover(struct bpapi_output_chain *ochain) {
	struct timer_handover_data *td = ochain->data;

	if (td->bs.buf) {
		timer_handover(td->is->ti, td->bs.buf);
		free(td->bs.buf);
	}
}

static int
init_timer_handover(const struct bpfilter *filter, struct bpapi *api, int argc, const struct tpvar **argv) {
	struct timer_handover_data *td = BPAPI_OUTPUT_DATA(api);
	td->is = filter_state(api, "idxs", NULL, NULL);
	return 0;
}

struct bpapi_output timer_handover_output = {
	bufstring_outstring_fmt,
	bufstring_outstring_raw,
	fini_timer_handover,
	NULL
};

ADD_OUTPUT_FILTER(timer_handover, sizeof (struct timer_handover_data));

/*
 * Can't do this in outstring because concatenating two normalized UTF8 strings
 * doesn't guarantee a normalized UTF8 string.
 */
static void
fini_casefold(struct bpapi_output_chain *ochain) {
	struct idx_buf_filter_data *data = ochain->data;

	if (data->bs.buf) {
		size_t ol = data->bs.pos * 2 + 1;
		char out[ol];

		casefold(out, ol, data->bs.buf, data->is->ic->atc.cfdata, 0);

		bpo_outstring_raw(ochain->next, out, strlen(out));
		free(data->bs.buf);
	}
}

#define init_casefold init_idx_buf_filter

struct bpapi_output casefold_output = {
	bufstring_outstring_fmt,
	bufstring_outstring_raw,
	fini_casefold,
	NULL
};

ADD_OUTPUT_FILTER(casefold, sizeof(struct idx_buf_filter_data));

static int
init_hun_expand(const struct bpfilter *filter, struct bpapi *api, int argc, const struct tpvar **argv) {
	struct indexing_state *is = filter_state(api, "idxs", NULL, NULL);
	const char *word;
	const char *lang;
	void *hun_obj = is->ic->single_lang_hun_obj;
	char *fw;

	switch (argc) {
	case 2:
		TPVAR_STRTMP(lang, argv[1]);
		/* ARGH. This will be super expensive to repeat for every word. */
		hun_obj = hash_table_search(is->ic->hun_objs, lang, -1, NULL);
	case 1:
		word = tpvar_str(argv[0], &fw);
		break;
	default:
		return -1;
	}

	bpapi_outstring_raw(api, word, strlen(word));

	if (hun_obj) {
		if (hun_spell_ok(hun_obj, word)) {
			char **stems = NULL;
			int nstems = hun_stem(hun_obj, &stems, word);
			int i;

			for (i = 0; i < nstems; i++) {
				size_t wl = strlen(stems[i]) * 2 + 1;
				char w[wl];

				casefold(w, wl, stems[i], is->ic->atc.cfdata, 0);
				if (strcmp(w, word) != 0) {
					bpapi_outstring_raw(api, " ", 1);
					bpapi_outstring_raw(api, w, strlen(w));
				}
			}
			hun_free_list(hun_obj, &stems, nstems);
		}
	}

	free(fw);
	return 0;
}

ADD_FILTER(hun_expand, NULL, 0, NULL, 0, NULL, 0);

static int
init_log_total_rows(const struct bpfilter *filter, struct bpapi *api, int argc, const struct tpvar **argv) {
	struct indexing_state *is = filter_state(api, "idxs", NULL, NULL);
	log_printf(LOG_INFO, "Indexed %u rows", is->row_tot);
	return 0;
}

ADD_FILTER(log_total_rows, NULL, 0, NULL, 0, NULL, 0);

static void
add_stopwords(struct indexing_state *is, void *hun_obj) {
	/* Add stop-words from bconf */
	struct bconf_node *snode = bconf_get(is->ic->root, "stopword");
	int add_stems = bconf_get_int(is->ic->root, "add_stopword_stems");
	int stop_words = bconf_count(snode);
	int cnt;

	log_printf(LOG_DEBUG, "Adding %u stopwords (with%s stems)", stop_words, add_stems ? "" : "out");
	for (cnt = 0 ; cnt < stop_words ; cnt++) {
		const char *word = bconf_value(bconf_byindex(snode, cnt));
		char **stems = NULL;
		int nstems;
		size_t wl = strlen(word) * 2 + 1;
		char w[wl];

		casefold(w, wl, word, is->ic->atc.cfdata, 0);
		add_stopword(is->db, w);

		if (add_stems && hun_obj) {
			if (hun_spell_ok(hun_obj, word) &&
			    (nstems = hun_stem(hun_obj, &stems, word))) {
				/* XXX look at more results than the first? */
				wl = strlen(stems[0]) * 2 + 1;
				char sw[wl];

				casefold(sw, wl, stems[0], is->ic->atc.cfdata, 0);
				add_stopword(is->db, sw);
				hun_free_list(hun_obj, &stems, nstems);
			}
		}
	}
}

static void
add_opers(struct indexing_state *is) {
	struct bconf_node *node = bconf_get(is->ic->root, "index.oper");
	const char *const operkeys[] = { "AND", "OR", "NOT", NULL };
	const char *const *opkey;
	const uint8_t opers[] = { IOP_AND, IOP_OR, IOP_NOT, -1 };
	const uint8_t *op;
	int i, j;

	for (opkey = operkeys, op = opers ; *opkey ; opkey++, op++) {
		struct bconf_node *onode = bconf_get(node, *opkey);
		int nlang = bconf_count(onode);

		if (!onode)
			log_printf(LOG_WARNING, "No %s opers in bconf.", *opkey);

		for (i = 0 ; i < nlang ; i++) {
			struct bconf_node *lnode = bconf_byindex(onode, i);
			const char *lang = bconf_key(lnode);
			int nword = bconf_count(lnode);

			for (j = 0 ; j < nword ; j++) {
				const char *word = bconf_value(bconf_byindex(lnode, j));
				int wlen = strlen(word);
				char cf[wlen * 2 + 1];

				casefold(cf, wlen * 2 + 1, word, is->ic->atc.cfdata, 0);
				log_printf(LOG_DEBUG, "Adding \"%s\", as operator %s for language %s", word, *opkey, lang);
				index_add_oper(is->db, lang, cf, *op);
			}
		}
	}
}

void
populate_index_defaults(struct indexing_state *is) {
	add_stopwords(is, is->ic->nhundicts ? is->ic->single_lang_hun_obj : NULL);
	add_opers(is);
	if (bconf_get_int(is->ic->root, "index.timestamp"))
		index_set_timestamp(is->db, time(NULL));
}

static void
bind_insert(struct bpapi *ba, struct bconf_node *node) {
	int i;
	struct bconf_node *var;

	for (i = 0; (var = bconf_byindex(node, i)); i++)
		bpapi_insert(ba, bconf_key(var), bconf_value(var));
}

void
populate_bpapi_custom_vars(struct bpapi *ba, struct indexing_state *is) {
	bind_insert(ba, is->ic->custom_vars);
	bind_insert(ba, bconf_get(is->ic->root, "index.templatevar"));
}

int
index_once(struct indexer_config *ic, struct timer_instance *ti, const char *index_name, int index_is_file) {
	struct indexing_state is = { .ti = ti, .ic = ic };
	struct bconf_node *filter_conf = NULL;
	struct shadow_vtree shadow_conf = { {0} };
	struct bpapi_vtree_chain tvt = {0};
	struct bpapi ba = { {0} };
	char odb_name[PATH_MAX];
	int retval = 0;

	/* open the index */
	if (ic->update_db) {
		is.db = index_prime_new(index_name, 0, 1, index_is_file);
	} else {
		snprintf(odb_name, sizeof(odb_name), "%s%s", index_name, ic->shard_size > 0 ? "0" : "");
		if ((is.db = open_index(odb_name, INDEX_WRITER|(index_is_file ? INDEX_FILE : 0))) == NULL) {
			log_printf(LOG_CRIT, "error: opening database %s", odb_name);
			return FATAL_REINDEX;
		}
		log_printf(LOG_DEBUG, "Indexing to %s", odb_name);
	}

	filter_state_bconf_init_all(&filter_conf);
	filter_state_bconf_set(&filter_conf, "idxs", &is, NULL, FS_SESSION);

	populate_index_defaults(&is);
	
	null_output_init(&ba.ochain);
	hash_simplevars_init(&ba.schain);
	ba.filter_state = filter_state_bconf;

	bconf_vtree(&shadow_conf.vtree, filter_conf);
	bconf_vtree(&tvt, ic->root);
	shadow_vtree_init(&ba.vchain, &shadow_conf, &tvt);

	populate_bpapi_custom_vars(&ba, &is);

	if (ic->update_db) {
		const char *from = index_get_from_id(is.db);
		if (from == NULL) {
			log_printf(LOG_CRIT, "Missing from for update");
			retval = 1;
			goto leave;
		}
		set_ps_printf("update from %s", from);
		bpapi_insert(&ba, "from", from);
		log_printf(LOG_DEBUG, "Update from %s", from);
	} else {
		set_ps_display("reindex");
		log_printf(LOG_DEBUG, "Reindex");
	}

	timer_handover(ti, "start");

	do {
		is.daemon_next_delay_ms = 1000;
		if (!call_template(&ba, ic->template)) {
			log_printf(LOG_CRIT, "Tried to call nonexisting template: %s", ic->template);
			retval = 1;
		}
	} while (is.daemon_next_delay_ms == 0);	/* Even in non-daemon mode, we need to eat empty batches until we get to the meat. */

	if (!retval && is.fatal)
		retval = is.fatal;

leave:
	/* close the database */
	bpapi_free(&ba);
	filter_state_bconf_free(&filter_conf);
	timer_handover(ti, "close");
	log_printf(LOG_DEBUG, "Closing index");
	if (close_index(is.db, 0) != 0) {
		log_printf(LOG_CRIT, "error: closing index");
		exit(1); /* XXX - to preserve ealier behavior */
	}
	log_printf(LOG_DEBUG, "Index closed");

	return retval;
}


int
main(int argc, char **argv) {
	struct indexer_config cfg = {0}, *ic = &cfg;
	struct timer_instance *all_ti, *ti;
	const char *engine;
	int retval = 0;
	int cnt;
	struct charmap *cm;
	const char *charmap;
	int i;
	struct bconf_node *bconf_override;

	argv = save_ps_display_args(argc, argv);

	bconf_override = parse_options(ic, argc, argv);
	
	if (argc <= optind)
		usage(argv[0]);

	engine = argv[optind];
	if (!engine || !*engine)
		usage(argv[0]);

	for (i = optind + 1 ; i < argc ; i++) {
		/* Simplevars filled in later. */
		if (!strchr(argv[i], '='))
			usage(argv[0]);
	}

	if (!config_file) {
		const char *bdir = getenv("BDIR");

		if (!bdir || !*bdir)
			bdir = "/opt/blocket";
		xasprintf(&config_file, "%s/conf/%s.conf", bdir, engine);
	}

	ic->root = config_init(config_file);

	if (!ic->root) {
		xerr(1, "Failed to load config file \"%s\"", config_file);
	}
	if (bconf_file)
		load_bconf_file(engine, &ic->root, bconf_file);
	else
		load_bconf(engine, &ic->root);
	bconf_merge(&ic->root, bconf_override);

	const char *logtag = bconf_vget_string(ic->root, "index.logtag", NULL);
	const char *loglevel = bconf_vget_string(ic->root, "index.loglevel", NULL);


	log_setup(logtag ?: engine, loglevel ?: "debug");
	x_err_init_syslog(logtag ?: engine, 0, LOG_LOCAL0, LOG_INFO);

	ic->indexer_daemonize = bconf_get(ic->root, "index.daemon") != NULL;
	if (!ic->index_name) {
		/* Try to load from config file */
		const char *dn = bconf_value(bconf_get(ic->root, "db_name"));
		if (dn) {
			ic->index_name = strdup(dn);
		} else {
			dn = bconf_get_string(ic->root, "db_file");
			if (dn) {
				ic->index_name = strdup(dn);
				ic->index_is_file = 1;
			}
		}
	       	
	}
	if ((!ic->index_name) ^ (!!ic->indexer_daemonize)) {
		if (!ic->indexer_daemonize)
			log_printf(LOG_CRIT, "indexer db_name not configured");
		else
			log_printf(LOG_CRIT, "do not configure db_name for daemonized indexer, use workdir.");
		usage(argv[0]);
	}

	if (!bconf_get(ic->root, "pgsql_session.db_name")) {
		log_printf(LOG_WARNING, "pgsql_session.db_name not configured, defaulting to 'index.db'");
		bconf_add_data(&ic->root, "pgsql_session.db_name", "index.db");
	}

	ic->template = bconf_get_string(ic->root, "index.template");
	if (!ic->template)
		xerrx(1, "No indexer template");

	/*
	 * Configure the global add_text_config.
	 */
	ic->atc.min_wordlen = 1;
	ic->atc.max_wordlen = 100;

	charmap = bconf_get_string(ic->root, "charmap");
	if (!charmap)
		xerrx(1, "No charmap in config file");
	cm = init_charmap(charmap);
	ic->atc.cfdata = init_charmap_thread(cm);

	/* Set up hun_objs */
	ic->hun_objs = hash_table_create(1, hun_clear);
	hash_table_free_keys(ic->hun_objs, 1);

	struct bconf_node *hroot = bconf_get(ic->root, "hundict");
	ic->nhundicts = bconf_count(hroot);
	log_printf(LOG_DEBUG, "Languages configured: %d", ic->nhundicts);

	for (cnt = 0 ; cnt < ic->nhundicts ; cnt++) {
		const char *dict = bconf_value(bconf_byindex(hroot, cnt));
		const char *lang = bconf_key(bconf_byindex(hroot, cnt));
		int dlen = strlen(dict);
		char dic[dlen + 5];
		char aff[dlen + 5];
		void *hun_obj = NULL;

		log_printf(LOG_DEBUG, "Setting up hunspell for stemming (%s)", lang);
		snprintf(dic, dlen + 5, "%s.dic", dict);
		snprintf(aff, dlen + 5, "%s.aff", dict);

		hun_obj = hun_setup(aff, dic);
		if (!hun_obj)
			log_printf(LOG_WARNING, "warning: failed to setup hunspell for stemming (%s)", lang);

		if (ic->nhundicts <= 1 && !ic->single_lang_hun_obj)
			ic->single_lang_hun_obj = hun_obj;

		hash_table_insert(ic->hun_objs, xstrdup(lang), -1, hun_obj);
	}

	if (bconf_get(ic->root, "index.langmap"))
		ic->atc.langmap = langmap_open(bconf_get_string(ic->root, "index.langmap"));

	if (bconf_get(ic->root, "index.debug_render_file")) {
		const char *fn = bconf_value(bconf_get(ic->root, "index.debug_render_file"));
		if (fn) {
			ic->debug_render_fh = fopen(fn, "w");
			if (ic->debug_render_fh)
				log_printf(LOG_DEBUG, "Rendering index template to '%s'", fn);
			else
				log_printf(LOG_DEBUG, "Failed to create index template render at '%s'", fn);
		}
	}

	if (bconf_get(ic->root, "index.shard_size"))
		ic->shard_size = bconf_get_int(ic->root, "index.shard_size");
	else if (bconf_get(ic->root, "shard_size")) /* bw compat */
		ic->shard_size = bconf_get_int(ic->root, "shard_size");

	if (ic->shard_size && ic->index_is_file)
		xerrx(1, "Can't deal with shard and indexes in files just yet.");

	ic->atc.syn_regexes = init_syn_regexes(bconf_get (ic->root, "syn_regexes"));

	all_ti = timer_start(NULL, "index");

	ti = timer_start(all_ti, "setup");

	init_ps_display(engine);

	set_ps_display("initializing");

	if (indexer_local_setup) {
		if (indexer_local_setup(ic->root))
			return 1;
	}

	if (!(ic->atc.synizer = syn_setup(ic->root))) {
		log_printf(LOG_CRIT, "Failed to setup synize");
	}

	for (i = optind + 1 ; i < argc ; i++) {
		char kbuf[256];
		const char *v;

		v = strchr(argv[i], '=');
		if (!v)
			usage(argv[0]);

		snprintf(kbuf, sizeof(kbuf), "%.*s", (int)(v - argv[i]), argv[i]);
		bconf_add_data(&ic->custom_vars, kbuf, v + 1);
	}

	if (ic->indexer_daemonize)
		retval = indexer_daemon(ic, ti);
	else
		retval = index_once(ic, ti, ic->index_name, ic->index_is_file);

	if (ic->debug_render_fh)
		fclose(ic->debug_render_fh);

	if (ic->atc.synizer)
		syn_clear(ic->atc.synizer);

	hash_table_free(ic->hun_objs);

	if (ic->atc.langmap)
		langmap_close(ic->atc.langmap);

	free_charmap_thread(ic->atc.cfdata);
	free_charmap(cm);

	free_syn_regexes(ic->atc.syn_regexes);

	free(ic->index_name);

	if (indexer_local_exit) {
		indexer_local_exit();
	}


	timer_end(ti, NULL);
	timer_end(all_ti, NULL);

	timer_foreach(timer_dump, NULL);
	stat_counters_foreach(stats_dump, NULL);

	bconf_free(&ic->root);
	bconf_free(&ic->custom_vars);
	bconf_free(&bconf_override);

	return retval;
}
