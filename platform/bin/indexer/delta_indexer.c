
#include "delta.h"
#include "indexer.h"

#include "bconf.h"
#include "index.h"
#include "timer.h"
#include "logging.h"
#include "stat_messages.h"
#include "bpapi.h"
#include "patchvars.h"
#include "ctemplates.h"

#include <pthread.h>
#include <unistd.h>
#include <assert.h>

struct delta_indexer {
	pthread_t thread;
	volatile bool stop;
	bool pause;

	bool paused;
	struct bconf_node *extra_vars;
	pthread_mutex_t ctrl_lock;
	pthread_cond_t ctrl_signal;

	struct indexer_config *ic;
	struct indexer_daemon_state *ids;
	struct stat_message *current_action;
};

static struct delta_control *
queue_delta(struct delta_indexer *di, struct indexing_state *is) {
	stat_message_printf(di->current_action, "finalizing delta");

	if (index_writer_downgrade_to_reader(is->db)) {
		log_printf(LOG_CRIT, "indexer_daemon: downgrade delta failed (out of disk and/or memory?)");
		return NULL;
	}

	struct delta_control *d = add_delta(di->ids, is->db, false, false);

	/* No longer own db */
	is->db = NULL;

	return d;
}

static int
wait_until_can_commit(struct delta_indexer *di, struct delta_control *d) {
	int retval = 0;

	pthread_mutex_lock(&d->lock);
	while (!di->stop && d->state != dsCommittable && d->state != dsFailed) {
		struct timespec abstime;
		clock_gettime(CLOCK_REALTIME, &abstime);
		abstime.tv_sec++;
		pthread_cond_timedwait(&d->signal, &d->lock, &abstime);
	}

	if (d->state != dsCommittable && di->stop)
		d->state = dsFailed;

	if (d->state == dsFailed)
		retval = -1;
	pthread_mutex_unlock(&d->lock);
	return retval;
}

/* Filter for triggering delta distribution in the indexing template. */
static int
init_distribute_delta(const struct bpfilter *filter, struct bpapi *api, int argc, const struct tpvar **argv) {
	struct patch_data *pd = BPAPI_SIMPLEVARS_DATA(api);
	struct delta_indexer *di = filter_state(api, "di", NULL, NULL);
	struct indexing_state *is = filter_state(api, "idxs", NULL, NULL);

	patchvars_init(pd, filter->name, "dd_", 3, NULL, NULL);

	timer_handover(is->ti, "close");

	switch (is->fatal) {
	case 0:
		break;
	case FATAL_REINDEX:
		log_printf(LOG_DEBUG, "reindex requested");
		return 0;
	case FATAL_NOCHANGES:
		log_printf(LOG_DEBUG, "no changes");
		bps_insert(&pd->vars, "dd_commit", "1");
		return 0;
	default:
		log_printf(LOG_DEBUG, "failed delta");
		return 0;
	}

	if (is->db->b_docs_off == 0 && bconf_get(is->db->meta, "deleted_docs") == NULL) {
		log_printf(LOG_DEBUG, "empty delta, ignoring");
		return 0;
	}

	struct delta_control *d = queue_delta(di, is);
	if (!d) {
		bps_insert(&pd->vars, "dd_error", "index finalize failed");
		/* This is fatal, exit to BOS. */
		is->fatal = 1;
		return 0;
	}

	timer_handover(is->ti, "distribute");

	stat_message_printf(di->current_action, "waiting until delta committable");

	if (wait_until_can_commit(di, d)) {
		log_printf(LOG_INFO, "delta not committable");
		bps_insert(&pd->vars, "dd_error", "merging or distribution failed");
	} else {
		bps_insert(&pd->vars, "dd_commit", "1");
	}
	put_delta(di->ids, d, 0);
	return 0;
}

ADD_FILTER(distribute_delta, NULL, 0, &patch_simplevars, sizeof(struct patch_data), NULL, 0);

static int
init_empty_batch(const struct bpfilter *filter, struct bpapi *api, int argc, const struct tpvar **argv) {
	struct indexing_state *is = filter_state(api, "idxs", NULL, NULL);

	is->fatal = FATAL_EMPTY_BATCH;
	return 0;
}

ADD_FILTER(empty_batch, NULL, 0, NULL, 0, NULL, 0);

static void
populate_extra_vars(struct delta_indexer *di, struct bpapi *ba) {
	pthread_mutex_lock(&di->ctrl_lock);
	int i;
	struct bconf_node *var;

	for (i = 0; (var = bconf_byindex(di->extra_vars, i)); i++)
		bpapi_insert(ba, bconf_key(var), bconf_value(var));
	bconf_free(&di->extra_vars);
	pthread_mutex_unlock(&di->ctrl_lock);
}
static int
one_delta_loop(struct delta_indexer *di, int *next_delay_ms, struct timer_instance *ti) {
	struct indexing_state iss = { .daemon_next_delay_ms = 1000, .ic = di->ic, .ti = timer_start(ti, "setup") }, *is = &iss;
	struct bconf_node *filter_conf = NULL;
	struct shadow_vtree shadow_conf = { {0} };
	struct bpapi_vtree_chain tvt = {0};
	struct bpapi ba = { {0} };
	int retval = -1;
	char *old_to;

	stat_message_printf(di->current_action, "indexing delta");

	is->db = open_mem_index_writer();
	if (!is->db)
		goto fail;

	if (indexer_daemon_prime_update(di->ids, is->db, &old_to))
		goto fail;

	filter_state_bconf_init_all(&filter_conf);
	filter_state_bconf_set(&filter_conf, "di", di, NULL, FS_SESSION);
	filter_state_bconf_set(&filter_conf, "idxs", is, NULL, FS_SESSION);
	populate_index_defaults(is);

	null_output_init(&ba.ochain);
	hash_simplevars_init(&ba.schain);
	ba.filter_state = filter_state_bconf;

	bconf_vtree(&shadow_conf.vtree, filter_conf);
	bconf_vtree(&tvt, is->ic->root);
	shadow_vtree_init(&ba.vchain, &shadow_conf, &tvt);

	timer_handover(is->ti, "start");

	log_printf(LOG_DEBUG, "new delta");

	bpapi_insert(&ba, "delta_is_update", "1");
	if (old_to) {
		bpapi_insert(&ba, "delta_old_to", old_to);
		free(old_to);
	}
	bpapi_insert(&ba, "from", index_get_from_id(is->db) ?: "0");
	populate_bpapi_custom_vars(&ba, is);
	populate_extra_vars(di, &ba);

	do {
		is->fatal = 0;
		if (!call_template(&ba, di->ic->template)) {
			log_printf(LOG_CRIT, "Tried to call nonexisting template: %s", di->ic->template);
			retval = 1;
		} else if (is->fatal) {
			retval = is->fatal;
		} else {
			retval = 0;
		}
	} while (retval == FATAL_EMPTY_BATCH);

	bpapi_free(&ba);
	filter_state_bconf_free(&filter_conf);

fail:
	if (is->db)
		close_index(is->db, 0);

	*next_delay_ms = is->daemon_next_delay_ms;

	timer_end(is->ti, NULL);

	return retval;
}


static void
check_control(struct delta_indexer *di) {
	pthread_mutex_lock(&di->ctrl_lock);
	if (di->pause != di->paused) {
		di->paused = di->pause;
		pthread_cond_broadcast(&di->ctrl_signal);
	}
	pthread_mutex_unlock(&di->ctrl_lock);
}

static void *
delta_indexer_thread(void *v) {
	struct delta_indexer *di = v;
	while (!di->stop) {
		check_control(di);

		struct timer_instance *ti = timer_start(NULL, "delta_indexer");
		int next_delay_ms = 0;
		int r = 0;
		if (di->paused)
			next_delay_ms = 1000;
		else
			r = one_delta_loop(di, &next_delay_ms, ti);
		timer_end(ti, NULL);

		switch (r) {
		case 0:
		case FATAL_NOCHANGES:
			break;
		case FATAL_REINDEX:
			di->pause = true;
			indexer_daemon_full_index(di->ids);
			break;
		default:
			indexer_daemon_stop(di->ids, true);
			di->stop = true;
			break;
		}

		if (next_delay_ms) {
			stat_message_printf(di->current_action, "idling %d ms", next_delay_ms);
			log_printf(LOG_DEBUG, "idling %d ms", next_delay_ms);
			usleep(next_delay_ms * 1000); /* For now */
		}
	}

	return NULL;
}

/*
 * Flag the delta indexer to pause and wait a short time for its pause state.
 */
bool
delta_indexer_pause_and_check_pause_state(struct delta_indexer *di, const struct timespec *timeout) {
	bool ret;

	pthread_mutex_lock(&di->ctrl_lock);
	di->pause = true;
	if (di->paused) {
		ret = true;
	} else {
		struct timespec abstime = timeout_from_now(timeout);
		pthread_cond_timedwait(&di->ctrl_signal, &di->ctrl_lock, &abstime);
		ret = di->paused;
	}
	pthread_mutex_unlock(&di->ctrl_lock);
	return ret;
}

void
delta_indexer_pause_resume(struct delta_indexer *di, bool pause) {
	/* Don't return until confirmed */
	pthread_mutex_lock(&di->ctrl_lock);
	di->pause = pause;
	while (di->paused != pause)
		pthread_cond_wait(&di->ctrl_signal, &di->ctrl_lock);
	pthread_mutex_unlock(&di->ctrl_lock);
}

void
delta_indexer_add_var(struct delta_indexer *di, const char *key, const char *value) {
	pthread_mutex_lock(&di->ctrl_lock);
	bconf_add_data(&di->extra_vars, key, value);
	pthread_mutex_unlock(&di->ctrl_lock);
}

struct delta_indexer *
create_delta_indexer(struct indexer_config *ic, struct indexer_daemon_state *ids, struct stat_message *current_action) {
	struct delta_indexer *di = zmalloc(sizeof (*di));
	if (!di)
		return NULL;

	di->ids = ids;
	di->ic = ic;
	di->current_action = current_action;
	di->stop = false;

	di->pause = false;
	pthread_mutex_init(&di->ctrl_lock, NULL);
	pthread_cond_init(&di->ctrl_signal, NULL);

	if (pthread_create(&di->thread, NULL, delta_indexer_thread, di)) {
		log_printf(LOG_CRIT, "indexer_daemon: delta indexer pthread_create: %m");
		return NULL;
	}

	return di;
}

int
stop_delta_indexer(struct delta_indexer *di) {
	di->stop = true;

	int r = pthread_join(di->thread, NULL);
	assert(r == 0);

	free(di);

	return r;
}
