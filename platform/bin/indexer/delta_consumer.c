
#include "delta.h"
#include "indexer.h"

#include "buf_string.h"
#include "memalloc_functions.h"
#include "json_vtree.h"
#include "stat_messages.h"
#include "bconf.h"
#include "index.h"
#include "logging.h"

#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <assert.h>
#include <unistd.h>
#include <sys/mman.h>

#include <curl/curl.h>

struct delta_consumer {
	pthread_t dc_thr;
	volatile bool stop;
	volatile bool pause;

	bool paused;
	pthread_mutex_t ctrl_lock;
	pthread_cond_t ctrl_signal;

	struct stat_message *dc_state_msg;			/* What is the thread doing. */

	struct stat_message *dc_client_msg;			/* The identifier of the client. */
	struct stat_message *dc_client_state_msg;		/* The state of the client, string representation of dc_client_state below. */
	struct stat_message *dc_client_state_reason_msg;	/* Reason the client is in this state. */

	/*
	 * Last known checksums of the indexes on the client.
	 */
	char *dc_client_active;
	char *dc_client_pending;

	enum dc_client_state {
		UNKNOWN,
		INSYNC,
		OUTOFSYNC_A,
		OUTOFSYNC_P,
		OUTOFSYNC_AP,
		BROKEN,
		DOWN,
	} dc_client_state;
	bool aux;

	char *dc_client_url;

	struct indexer_daemon_state *ids;
	int idx;
	bool remove_if_down;
};

static void check_control(struct delta_consumer *dc);


static const char *client_state_name[] = {
	"UNKNOWN",
	"INSYNC",
	"OUTOFSYNC_A",
	"OUTOFSYNC_P",
	"OUTOFSYNC_AP",
	"BROKEN",
	"DOWN",
};

enum dc_transition {
	COMM_ERROR,				/* communication attempts return errors. */
	COMM_DOWN, 				/* communication attempts can't connect. */
	PENDING_AND_ACTIVE_MISMATCH, 		/* active or pending index checksum mismatch. */
	PENDING_AND_ACTIVE_MATCH,		/* pending and active index checksum match. */
	PENDING_MISMATCH,			/* pending index checksum mismatch. */
	ACTIVE_MISMATCH,			/* active index checksum mismatch. */
	PENDING_DISTRIBUTED,			/* pending sucessfully sent. */
	DELTA_DISTRIBUTED,			/* delta successfully sent. */
	DELTA_PENDING_SYNC_FAILED,		/* delta 409 despite pending sync. */
	PENDING_ACTIVATED,			/* pending successfully activated. */
	OBSERVER_ACTIVE_MATCH,			/* index observer active match */
	OBSERVER_PENDING_MATCH,			/* index observer pending match */
};

static const char *state_transition_name[] = {
	"COMM_ERROR",
	"COMM_DOWN",
	"PENDING_AND_ACTIVE_MISMATCH",
	"PENDING_AND_ACTIVE_MATCH",
	"PENDING_MISMATCH",
	"ACTIVE_MISMATCH",
	"PENDING_DISTRIBUTED",
	"DELTA_DISTRIBUTED",
	"DELTA_PENDING_SYNC_FAILED",
	"PENDING_ACTIVATED",
	"OBSERVER_ACTIVE_MATCH",
	"OBSERVER_PENDING_MATCH",
};

static void
delta_consumer_transition_state(struct delta_consumer *dc, enum dc_transition transition, const char *reasonf, ...) {
	enum dc_client_state new_state = dc->dc_client_state;
	enum dc_client_state old_state = dc->dc_client_state;

	switch (transition) {
	case COMM_ERROR:
		new_state = BROKEN;
		break;
	case COMM_DOWN:
		new_state = DOWN;
		break;
	case PENDING_AND_ACTIVE_MISMATCH:
		new_state = OUTOFSYNC_AP;
		break;
	case PENDING_AND_ACTIVE_MATCH:
		new_state = INSYNC;
		break;
	case PENDING_MISMATCH:
		switch (old_state) {
		case OUTOFSYNC_A:
			new_state = OUTOFSYNC_AP;
			break;
		default:
			new_state = OUTOFSYNC_P;
			break;
		}
		break;
	case ACTIVE_MISMATCH:
		switch (old_state) {
		case OUTOFSYNC_P:
			new_state = OUTOFSYNC_AP;
			break;
		default:
			new_state = OUTOFSYNC_A;
			break;
		}
		break;
	case DELTA_DISTRIBUTED:
		/* Distributing a delta is like distributing a pending. */
		/* FALLTHROUGH */
	case OBSERVER_PENDING_MATCH:
		/* If the observer reports a matching pending, it's like a normal client distributing pending. */
		/* FALLTHROUGH */
	case PENDING_DISTRIBUTED:
		switch (old_state) {
		case OUTOFSYNC_P:
		case INSYNC:
			new_state = INSYNC;
			break;
		case OUTOFSYNC_AP:
			new_state = OUTOFSYNC_A;
			break;
		case OUTOFSYNC_A:
			/* Nothing changes in this case. */
			new_state = OUTOFSYNC_A;
			break;
		case UNKNOWN:
		case DOWN:
		case BROKEN:
		default:
			/*
			 * Now what?
			 *
			 * The cases we handled above are obvious. In here we don't actually know the state
			 * of the active index, we know that the pending is in sync, but active can be bad.
			 *
			 * Should we just keep the old state and let the future client_sync_state take care of
			 * getting us in sync?  For now we do the opposite: be optimistic and assume we're in
			 * sync and let client_sync_state catch any situations where we aren't.
			 */
			new_state = INSYNC;
			break;
		}
		break;
	case DELTA_PENDING_SYNC_FAILED:
		new_state = BROKEN;
		break;
	case PENDING_ACTIVATED:
		/* We should never succeed activating the wrong pending, so it is safe to assume that everything is in sync now. */
		new_state = INSYNC;
		break;
	case OBSERVER_ACTIVE_MATCH:
		switch (old_state) {
		case OUTOFSYNC_A:
			new_state = INSYNC;
			break;
		case OUTOFSYNC_AP:
			new_state = OUTOFSYNC_P;
			break;
		default:
		case OUTOFSYNC_P:
		case INSYNC:
			break;
		}
	default:
		log_printf(LOG_CRIT, "delta_consumer_transition_state: BAD TRANSITION");
		break;
	}

	if (dc->dc_client_state == new_state)
		return;
	char *msg = NULL;
	if (reasonf) {
		va_list va;
		va_start(va, reasonf);
		xvasprintf(&msg, reasonf, va);
		va_end(va);
	}

	log_printf(LOG_INFO, "client(%d) state transition(%s): %s -> %s%s%s", dc->idx, state_transition_name[transition], client_state_name[dc->dc_client_state], client_state_name[new_state], msg ? ", reason: " : "", msg ?: "");

	dc->dc_client_state = new_state;
	stat_message_printf(dc->dc_client_state_msg, "%s", client_state_name[new_state]);

	stat_message_printf(dc->dc_client_state_reason_msg, "%s", msg ?: "");
	free(msg);

	if (new_state == DOWN && dc->remove_if_down) {
		/* Before locking the global consumers lock, we need to make sure nobody is waiting for us while holding it. */
		dc->stop = true;
		check_control(dc);
		indexer_daemon_stop_consumer(dc->ids, dc);
	}
}

static size_t
curl_wr(char *d, size_t s, size_t n, void *v) {
	struct buf_string *b = v;

	return bswrite(b, d, s * n);
}

CURLcode
talk_to_client(struct delta_consumer *dc, const char *url, void *post_data, size_t post_data_sz, char **response_buf, size_t *response_len, struct bconf_node **response_json, long *status) {
	struct buf_string response = { 0 };
	char *full_url;
	CURLcode r;
	CURL *c;

	c = curl_easy_init();

	xasprintf(&full_url, "%s/%s", dc->dc_client_url, url);
	curl_easy_setopt(c, CURLOPT_URL, full_url);
	curl_easy_setopt(c, CURLOPT_NOSIGNAL, 1L);	
#ifdef CURLOPT_CONNECTTIMEOUT_MS
	curl_easy_setopt(c, CURLOPT_CONNECTTIMEOUT_MS, 1000L);  /* 1000ms should be enough to connect to anybody. */
#else
	curl_easy_setopt(c, CURLOPT_CONNECTTIMEOUT, 1L);
#endif
	if (post_data) {
		curl_off_t csz = post_data_sz;
		curl_easy_setopt(c, CURLOPT_POSTFIELDSIZE_LARGE, csz);
		curl_easy_setopt(c, CURLOPT_POSTFIELDS, post_data);
	}
	curl_easy_setopt(c, CURLOPT_WRITEFUNCTION, curl_wr);
	curl_easy_setopt(c, CURLOPT_WRITEDATA, &response);

	r = curl_easy_perform(c);

	if (!r && status) {
		curl_easy_getinfo(c, CURLINFO_RESPONSE_CODE, status);
	}
	curl_easy_cleanup(c);

	free(full_url);
	if (response_len && response.buf) {
		*response_len = response.len;
	}

	if (response_json && response.buf) {
		if (json_bconf(response_json, NULL, response.buf, response.pos, 0)) {
			if (response_buf) {
				*response_buf = response.buf;
			} else {
				free(response.buf);
			}
			if (status)
				*status = 500;
		} else {
			free(response.buf);
		}
	} else if (response_buf) {
		*response_buf = response.buf;
	} else {
		free(response.buf);
	}
	return r;
}

struct index_receiver {
	size_t sz;
	size_t pos;
	char *d;
	CURL *curl;
	int fd;
};

static size_t
index_curl_writer(char *ptr, size_t size, size_t nmemb, void *v) {
	struct index_receiver *ir = v;
	size_t wsz = size * nmemb;

	if (ir->sz == 0) {
		double libcurl_is_really_stupid;
		size_t non_stupid_size;

		if (curl_easy_getinfo(ir->curl, CURLINFO_CONTENT_LENGTH_DOWNLOAD, &libcurl_is_really_stupid) != CURLE_OK || libcurl_is_really_stupid < 1.0) {
			log_printf(LOG_CRIT, "indexer_daemon: no (or bad) content-length");
			return 0;
		}
		assert((libcurl_is_really_stupid - nextafter(libcurl_is_really_stupid, 0)) < 1.0);
		non_stupid_size = libcurl_is_really_stupid;
		ir->sz = non_stupid_size;
		if (ftruncate(ir->fd, ir->sz) == -1) {
			log_printf(LOG_CRIT, "indexer_daemon: ftruncate: %m");
			return 0;
		}
		if ((ir->d = mmap(NULL, ir->sz, PROT_READ|PROT_WRITE, MAP_FILE|MAP_SHARED, ir->fd, 0)) == MAP_FAILED) {
			log_printf(LOG_CRIT, "indexer_daemon: mmap: %m");
			return 0;
		}
	}
	assert(ir->pos + wsz <= ir->sz);

	memcpy(&ir->d[ir->pos], ptr, wsz);
	ir->pos += wsz;

	return wsz;
}

int
delta_consumer_fetch_index(struct delta_consumer *dc, int fd) {
	struct index_receiver ir = { .fd = fd };
	long status;
	CURLcode r;
	char *index_url = NULL;

	log_printf(LOG_DEBUG, "indexer_daemon: fetching index from consumer %s", dc->dc_client_url);
	xasprintf(&index_url, "%s/%s", dc->dc_client_url, "get_index?index=active");

	ir.curl = curl_easy_init();
	curl_easy_setopt(ir.curl, CURLOPT_URL, index_url);
	curl_easy_setopt(ir.curl, CURLOPT_WRITEFUNCTION, index_curl_writer);
	curl_easy_setopt(ir.curl, CURLOPT_WRITEDATA, &ir);
	r = curl_easy_perform(ir.curl);

	curl_easy_getinfo(ir.curl, CURLINFO_RESPONSE_CODE, &status);
	curl_easy_cleanup(ir.curl);
	free(index_url);

	if (ir.d) {
		if (munmap(ir.d, ir.sz) != 0) {
			log_printf(LOG_CRIT, "indexer_daemon: unmapping temporary index mapping failed: %m (you'll soon run out of disk and/or memory)");
		}
		if (fsync(fd)) {
			log_printf(LOG_CRIT, "indexer_daemon: fsync temporary index failed: %m (you'll soon run out of disk and/or memory)");
		}
	}

	if (r == CURLE_OK && status == 200)
		return 0;

	if (r != CURLE_OK) {
		log_printf(LOG_CRIT, "indexer_daemon: fetching %s failed: %s", dc->dc_client_url, curl_easy_strerror(r));
	}

	return -1;
}

/*
 * General client state synchronization. We check the consumer state
 * (even in situations when everything is ok, this doesn't hurt), put
 * our consumer into the right state then double check that an out of
 * sync client is properly paused and then resumed.
 */
static void
client_sync_state(struct delta_consumer *dc) {
	struct bconf_node *b = NULL;
	long status;
	CURLcode r;
	char *s_active = NULL;
	char *s_pending = NULL;

	r = talk_to_client(dc, "index_checksums", NULL, 0, NULL, NULL, &b, &status);
	if (r != 0) {
		delta_consumer_transition_state(dc, COMM_DOWN, "%s", curl_easy_strerror(r));
	} else {
		if (status == 200) {
			bool act_m, pend_m;
			const char *s;

			/*
			 * We need to have clients that don't keep track of the checksums pretend to us that they are in sync.
			 */
			if (bconf_get(b, "search.index.passive_observer")) {
				if ((s = bconf_get_string(b, "search.index.passive_observer.active")) == NULL || !strcmp(s, "insync")) {
					delta_consumer_transition_state(dc, OBSERVER_ACTIVE_MATCH, NULL);
				} else {
					delta_consumer_transition_state(dc, ACTIVE_MISMATCH, NULL);
				}
				if ((s = bconf_get_string(b, "search.index.passive_observer.pending")) == NULL || !strcmp(s, "insync")) {
					delta_consumer_transition_state(dc, OBSERVER_PENDING_MATCH, NULL);
				} else {
					delta_consumer_transition_state(dc, PENDING_MISMATCH, NULL);
				}
			} else {
				if ((s = bconf_get_string(b, "search.index.active.checksum")) && *s)
					s_active = strdup(s);
				if ((s = bconf_get_string(b, "search.index.pending.checksum")) && *s)
					s_pending = strdup(s);

				if (indexer_daemon_verify_checksums(dc->ids, s_active, s_pending, &act_m, &pend_m)) {					
					delta_consumer_transition_state(dc, PENDING_AND_ACTIVE_MATCH, NULL);
				} else {
					if (!act_m && !pend_m) {
						delta_consumer_transition_state(dc, PENDING_AND_ACTIVE_MISMATCH, "pending: %s, active: %s", s_pending, s_active);
					} else if (!act_m) {
						delta_consumer_transition_state(dc, ACTIVE_MISMATCH, "active: %s", s_active);
					} else {
						delta_consumer_transition_state(dc, PENDING_MISMATCH, "pending: %s", s_pending);
					}
				}
			}
			bconf_free(&b);
		} else {
			delta_consumer_transition_state(dc, COMM_ERROR, "check_client_state HTTP %ld", status); 
		}
	}
	/* We need to always set and reset the known indexes. */
	free(dc->dc_client_active);
	free(dc->dc_client_pending);
	dc->dc_client_active = s_active;
	dc->dc_client_pending = s_pending;

	bool client_paused = false;
	const char *s = bconf_get_string(b, "search.search_requests_paused");
	if (s != NULL && !strcmp(s, "true")) {
		client_paused = true;
	}

	/*
	 * Make sure that a client that should be paused is paused and one that should not be paused is not paused.
	 *
	 * We explicitly just handle states in which we know we should enable/disable the client (instead of a negative list).
	 *
	 * We ignore the return values here. If the request fail, we'll just try again.
	 */
	if (client_paused && (dc->dc_client_state == INSYNC || dc->dc_client_state == OUTOFSYNC_P)) {
		log_printf(LOG_DEBUG, "PAUSE resuming client %d because it is %s", dc->idx, client_state_name[dc->dc_client_state]);
		talk_to_client(dc, "search_resume", NULL, 0, NULL, NULL, NULL, NULL);
	}
	if (!client_paused && (dc->dc_client_state == OUTOFSYNC_AP || dc->dc_client_state == OUTOFSYNC_A || dc->dc_client_state == BROKEN)) {
		log_printf(LOG_DEBUG, "PAUSE pausing client %d because it is %s", dc->idx, client_state_name[dc->dc_client_state]);
		talk_to_client(dc, "search_pause", NULL, 0, NULL, NULL, NULL, NULL);
	}
}

/*
 * Distribute the delta and perform the right state transition for the client.
 */
static int
client_distribute_delta(struct delta_consumer *dc, struct delta_control *delta) {
	long status = 0;
	char url[1024];
	CURLcode r;

	stat_message_printf(dc->dc_state_msg, "distributing %s", index_get_to_id(delta->delta_index));

	snprintf(url, sizeof(url), "delta?delta_checksum=%s&pending_checksum=%s", index_get_checksum_string(delta->delta_index), delta->pending_to_merge_csum);
	r = talk_to_client(dc, url, delta->delta_index->db_blob, delta->delta_index->db_blob_size, NULL, NULL, NULL, &status);
	if (r != 0) {
		delta_consumer_transition_state(dc, COMM_DOWN, "%s", curl_easy_strerror(r));
		stat_message_printf(dc->dc_state_msg, "failed %s", index_get_to_id(delta->delta_index));
		return -1;
	} else if (status == 409) {
		/*
		 * 409 indicates the pending index on the server doesn't match the delta we sent.
		 * We should resync the pending index when this happens.
		 */
		log_printf(LOG_WARNING, "delta distribution client %d failed %s, pending mismatch", dc->idx, url);
		stat_message_printf(dc->dc_state_msg, "delta / pending mismatch %s", url);
		delta_consumer_transition_state(dc, PENDING_MISMATCH, "client pending mismatch");

		return 1;
	} else if (status != 200) {
		log_printf(LOG_CRIT, "delta distribution client %d failed %s (%ld)", dc->idx, url, status);
		delta_consumer_transition_state(dc, COMM_ERROR, "client_distribute_delta HTTP status: %ld", status);
		stat_message_printf(dc->dc_state_msg, "failed %s (%ld)", index_get_to_id(delta->delta_index), status);

		return -1;
	}

	delta_consumer_transition_state(dc, DELTA_DISTRIBUTED, "delta distributed");
	stat_message_printf(dc->dc_state_msg, "distributed %s", index_get_to_id(delta->delta_index));

	return 0;
}

static int
client_distribute_pending(struct delta_consumer *dc) {
	char url[1024];
	long status;
	CURLcode r;
	int ret;

	struct index_db *pending_db = indexer_daemon_retain_pending(dc->ids);

	stat_message_printf(dc->dc_state_msg, "distributing pending %s", index_get_to_id(pending_db));

	snprintf(url, sizeof(url), "pending?pending_checksum=%s", index_get_checksum_string(pending_db));
	
	r = talk_to_client(dc, url, pending_db->db_blob, pending_db->db_blob_size, NULL, NULL, NULL, &status);
	if (r != 0) {
		delta_consumer_transition_state(dc, COMM_DOWN, "%s", curl_easy_strerror(r));
		ret = -1;
	} else if (status != 200) {
		log_printf(LOG_CRIT, "pending distribution failed %s/%s", dc->dc_client_url, url);
		delta_consumer_transition_state(dc, COMM_ERROR, "client_distribut_pending HTTP status: %d", status);
		ret = -1;
	} else {
		log_printf(LOG_INFO, "pending distributed %s/%s", dc->dc_client_url, url);
		delta_consumer_transition_state(dc, PENDING_DISTRIBUTED, NULL);
		ret = 0;
	}

	if (ret != 0) {
		stat_message_printf(dc->dc_state_msg, "failed %s", index_get_to_id(pending_db));
	} else {
		stat_message_printf(dc->dc_state_msg, "distributed pending %s", index_get_to_id(pending_db));
	}

	indexer_daemon_release_pending(dc->ids, pending_db);
	return ret;
}

static int
client_activate_pending(struct delta_consumer *dc, const char *pending_csum) {
	char *response = NULL;
	long status = 0;
	char url[1024];
	CURLcode r;
	int ret;

	stat_message_printf(dc->dc_state_msg, "activating pending with csum %s", pending_csum);

	snprintf(url, sizeof(url), "activate_pending?pending_checksum=%s", pending_csum);

	r = talk_to_client(dc, url, NULL, 0, &response, NULL, NULL, &status);
	if (r != 0) {
		log_printf(LOG_CRIT, "activate_pending failed: %s", curl_easy_strerror(r));
		delta_consumer_transition_state(dc, COMM_DOWN, "%s", curl_easy_strerror(r));
		ret = -1;
	} else if (status == 409) {
		log_printf(LOG_CRIT, "activate_pending failed: pending checksum mismatch");
		delta_consumer_transition_state(dc, PENDING_MISMATCH, "client pending mismatch (activate)");
		ret = 1;
	} else if (status != 200) {
		log_printf(LOG_CRIT, "activate_pending failed: %ld", status);
		delta_consumer_transition_state(dc, COMM_ERROR, "client_activate_pending HTTP status: %ld", status);
		ret = -1;
	} else {
		delta_consumer_transition_state(dc, PENDING_ACTIVATED, NULL);
		ret = 0;
	}
	free(response);

	if (ret != 0) {
		stat_message_printf(dc->dc_state_msg, "activate failed");
	} else {
		stat_message_printf(dc->dc_state_msg, "activated");
	}

	return ret;
}

/*
 * Handle delta_control messages.
 * A control message can be: (1-4 are the only allowed states when coming in, 5+6 can be triggered in this function)
 *  state | delta_index | activate_after | sync_pending_before
 *  ---------------------------------------------------
 *    1   | !NULL       | 0              | 0
 *    2   | !NULL       | 1              | 0
 *    3   | NULL        | 1              | 0
 *    4   | NULL        | 1              | 1
 *    5   | !NULL       | 0              | 1
 *    6   | !NULL       | 1              | 1
 *
 *  if (sync_pending_before) sync_pending();
 *  if (delta_index) distribute_delta();
 *  if (activate_after) activate();
 */
static int
client_handle_delta_control(struct delta_consumer *dc, struct delta_control *delta) {
	int sync_pending_attempted = 0;
	int res = 0;

	assert(delta->sync_pending_before == 0 || delta->delta_index == NULL);	/* This is a disallowed initial state. */

restart:
	if (delta->sync_pending_before || sync_pending_attempted) {
		if (client_distribute_pending(dc)) {
			delta_client_merged(dc->ids, delta, dc->idx, false, dc->aux, false);
			res = -1;
			goto out;
		}
	}

	if (delta->delta_index) {
		res = client_distribute_delta(dc, delta);
		if (res != 0) {
			if (res == 1) {
				if (!sync_pending_attempted) {
					sync_pending_attempted = 1;
					goto restart;
				}
				/* This client seems beyond repair. */
				delta_consumer_transition_state(dc, DELTA_PENDING_SYNC_FAILED, "delta unsuccessful despite pending sync");
			}
			delta_client_merged(dc->ids, delta, dc->idx, false, dc->aux, false);
			goto out;
		}
	}
	delta_client_merged(dc->ids, delta, dc->idx, true, dc->aux, true);

	if (delta->activate_after) {
		wait_for_everyone_to_consume_delta(dc->ids, delta, true);
		if (delta->pending_to_activate_csum != NULL) {
			res = client_activate_pending(dc, delta->pending_to_activate_csum);
		}
	}
out:
	log_printf(LOG_DEBUG, "client_handle_delta_control client %d: %s", dc->idx, dc->dc_client_state == INSYNC ? "success" : "fail");
	return 0;
}

static void
check_control(struct delta_consumer *dc) {
	pthread_mutex_lock(&dc->ctrl_lock);
	if (dc->pause != dc->paused) {
		dc->paused = dc->pause;
		pthread_cond_broadcast(&dc->ctrl_signal);
	} else if (dc->stop) {
		/* If we're exiting, wake any threads waiting for us. */
		pthread_cond_broadcast(&dc->ctrl_signal);
	}
	pthread_mutex_unlock(&dc->ctrl_lock);
}

static void *
delta_distribution_thread(void *v) {
	struct delta_consumer *dc = v;
	struct indexer_daemon_state *ids = dc->ids;
	const struct timespec one_second = { .tv_sec = 1 };

	stat_message_printf(dc->dc_client_msg, dc->dc_client_url);
	log_printf(LOG_DEBUG, "client %d starting", dc->idx);

	while (!dc->stop) {
		struct delta_control *delta;

		check_control(dc);

		if (dc->paused) {
			nanosleep(&one_second, NULL);
			continue;
		}

		/*
		 * Everything is stable, wait for control messages and periodically recheck client state.
		 */
		if ((delta = get_delta_for_merge(ids, dc->idx, &one_second)) != NULL) {
			log_printf(LOG_DEBUG, "client %d new delta, pending_to_merge_csum %s", dc->idx, delta->pending_to_merge_csum);
			client_handle_delta_control(dc, delta);
			/*
			 * For now, double check the state of the client after each delta, we might want to ease up this requirement
			 * in the future.
			 */
			if (1) {
				/*
				 * Wait for indexer to make a new pending so that client_check_state doesn't flap
				 * our state when the client finishes merging before the indexer.
				 */
				wait_for_everyone_to_consume_delta(dc->ids, delta, true);
				client_sync_state(dc);
			}
			put_delta(ids, delta, 0);
		}

		switch (dc->dc_client_state) {
		case OUTOFSYNC_AP:
		case OUTOFSYNC_A:
			/*
			 * Always do agressive client_sync_state here to make sure that out of sync clients are properly paused or synced.
			 */
			client_sync_state(dc);
			break;
		case OUTOFSYNC_P:
		case INSYNC:
			/*
			 * 
			 */
			if (delta == NULL) {
				client_sync_state(dc);
			}
			break;
		case UNKNOWN:
		case BROKEN:
		case DOWN:
		default:
			nanosleep(&one_second, NULL);
			client_sync_state(dc);
			break;
		}
	}
	log_printf(LOG_DEBUG, "client %d exiting", dc->idx);

	/* Since we might exit spontaneously, check control one last time in case someone is waiting on the signal. */
	check_control(dc);

	return NULL;
}

void
delta_consumer_pause_resume(struct delta_consumer *dc, bool pause) {
	/* Don't return until confirmed */
	pthread_mutex_lock(&dc->ctrl_lock);
	dc->pause = pause;
	while (dc->paused != pause && !dc->stop)
		pthread_cond_wait(&dc->ctrl_signal, &dc->ctrl_lock);
	pthread_mutex_unlock(&dc->ctrl_lock);
}

struct delta_consumer *
create_delta_consumer(struct indexer_daemon_state *ids, int idx, struct bconf_node *n, bool aux, bool remove_if_down) {
	struct delta_consumer *dc = zmalloc(sizeof(*dc));
	if (!dc)
		return NULL;
	const char *port = bconf_get_string(n, "controller_port");
	const char *host = bconf_get_string(n, "name");
	char buf[16];

	if (port == NULL || host == NULL) {
		log_printf(LOG_CRIT, "indexer_daemon: misconfigured host %s: %s %s missing", bconf_key(n), port == NULL ? "'controller_port'" : "", host == NULL ? "'name'" : "");
		return NULL;
	}

	snprintf(buf, sizeof(buf), "%d", idx);

	dc->stop = false;
	dc->pause = false;
	dc->paused = false;
	pthread_mutex_init(&dc->ctrl_lock, NULL);
	pthread_cond_init(&dc->ctrl_signal, NULL);
	dc->ids = ids;
	dc->idx = idx;
	dc->dc_state_msg = stat_message_dynamic_alloc(4, "indexer", "delta_consumer", buf, "status");
	dc->dc_client_msg = stat_message_dynamic_alloc(4, "indexer", "delta_consumer", buf, "client");
	dc->dc_client_state_msg = stat_message_dynamic_alloc(4, "indexer", "delta_consumer", buf, "client_state");
	dc->dc_client_state_reason_msg = stat_message_dynamic_alloc(4, "indexer", "delta_consumer", buf, "client_state_reason");
	dc->aux = aux;
	dc->remove_if_down = remove_if_down;
	xasprintf(&dc->dc_client_url, "http://%s:%s", host, port);

	return dc;
}

int
start_delta_consumer(struct delta_consumer *dc) {
	if (pthread_create(&dc->dc_thr, NULL, delta_distribution_thread, dc)) {
		log_printf(LOG_CRIT, "indexer_daemon: delta consumer pthread_create: %m");
		return -1;
	}
	return 0;
}

int
stop_delta_consumer(struct delta_consumer *dc) {
	dc->stop = true;
	int res = pthread_join(dc->dc_thr, NULL);

	stat_message_dynamic_free(dc->dc_state_msg);
	stat_message_dynamic_free(dc->dc_client_msg);
	stat_message_dynamic_free(dc->dc_client_state_msg);
	stat_message_dynamic_free(dc->dc_client_state_reason_msg);
	pthread_cond_destroy(&dc->ctrl_signal);
	pthread_mutex_destroy(&dc->ctrl_lock);

	free(dc->dc_client_active);
	free(dc->dc_client_pending);
	free(dc->dc_client_url);
	free(dc);

	return res;
}

