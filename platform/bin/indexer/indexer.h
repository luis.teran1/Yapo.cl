#ifndef INDEXER_H
#define INDEXER_H

#include "text.h"

#include <stdbool.h>
#include <stdio.h>

struct indexer_daemon_state;
struct bpapi;

#define MAX_CONSUMERS 1024

/*
 * Configuration for the whole indexer.
 *
 * We keep it in here just to make it clear what belongs where instead of having a random forest of global variables.
 */
struct indexer_config {
	struct bconf_node *root;
	struct bconf_node *custom_vars;

	struct hash_table *hun_objs;
	int nhundicts;
	void *single_lang_hun_obj;
	int shard_size;
	const char *template;

	struct add_text_config atc;

	int index_is_file;		/* If the index we want is just a file. */
	char *index_name;
	FILE *debug_render_fh;

	int update_db;
	int indexer_daemonize;
};

/*
 * This is the state for one indexing run.
 */
enum {
	FATAL_REINDEX = 10,
	FATAL_NOCHANGES = 20, /* Not really fatal */
	FATAL_EMPTY_BATCH = 30, /* Quick retry, hacky */
	FATAL_RECOVERABLE = 40, /* Not fatal. We can recover. */
};
struct indexing_state {
	struct index_db *db;
	struct document *gdoc;
	struct timer_instance *ti;
	const struct indexer_config *ic;
	int fatal;
	int daemon_next_delay_ms;
	int row_tot;
	int shard;
};

int index_once(struct indexer_config *, struct timer_instance *, const char *, int);
int indexer_daemon(struct indexer_config *, struct timer_instance *);

void populate_index_defaults(struct indexing_state *);
void populate_bpapi_custom_vars(struct bpapi *, struct indexing_state *);


bool indexer_daemon_verify_checksums(struct indexer_daemon_state *ids, const char *s_active, const char *s_pending, bool *act_m, bool *pend_m);
int indexer_daemon_prime_update(struct indexer_daemon_state *ids, struct index_db *db, char **old_to);

struct index_db *indexer_daemon_retain_pending(struct indexer_daemon_state *ids);
void indexer_daemon_release_pending(struct indexer_daemon_state *ids, struct index_db *pending_db);

void indexer_daemon_stop(struct indexer_daemon_state *ids, bool fatal_error);
void indexer_daemon_full_index(struct indexer_daemon_state *ids);

#endif /*INDEXER_H*/
