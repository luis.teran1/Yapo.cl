
#include "indexer.h"

#include "bitfield.h"
#include "queue.h"

#include <stdint.h>
#include <time.h>
#include <stdbool.h>
#include <pthread.h>

#include <curl/curl.h>

struct bconf_node;
struct delta_consumer;
struct indexer_daemon_state;
struct delta_indexer;
struct stat_message;
struct indexer_config;

struct delta_control {
	TAILQ_ENTRY(delta_control) list;
	int ref;

	pthread_mutex_t lock;
	pthread_cond_t signal;

	enum delta_state {
		dsQueued,
		dsReadyForMerge,
		dsCommittable,
		dsFailed,
	} state;
	enum {
		dimNotMerged,
		dimMergedNotPending,
		dimMergedAndPending,
	} indexer_merged;
	BITFIELD(client_merged, MAX_CONSUMERS);
	BITFIELD(client_aux, MAX_CONSUMERS);
	BITFIELD(client_failed, MAX_CONSUMERS);
	BITFIELD(client_recoverable, MAX_CONSUMERS);

	struct index_db *delta_index;
	struct stat_message *delta_from;
	struct stat_message *delta_to;
	struct stat_message *delta_checksum;
	uint64_t *delta_ndocs;

	char *pending_to_merge_csum;

	char *pending_to_activate_csum;
	bool activate_after;
	bool sync_pending_before;
};

struct delta_control *get_delta_for_merge(struct indexer_daemon_state *ids, int idx, const struct timespec *ts);
struct delta_control *add_delta(struct indexer_daemon_state *ids, struct index_db *new_delta, bool force_activate, bool sync_pending);
int put_delta(struct indexer_daemon_state *ids, struct delta_control *d, int wait_until_last);
void delta_client_merged(struct indexer_daemon_state *ids, struct delta_control *d, int idx, bool success, bool aux, bool recoverable);

void delta_indexer_pause_resume(struct delta_indexer *di, bool pause);
bool delta_indexer_pause_and_check_pause_state(struct delta_indexer *di, const struct timespec *timeout);
void delta_indexer_add_var(struct delta_indexer *di, const char *key, const char *value);

CURLcode talk_to_client(struct delta_consumer *dc, const char *url, void *post_data, size_t post_data_sz, char **response_buf, size_t *response_len, struct bconf_node **response_json, long *status);

struct delta_indexer *create_delta_indexer(struct indexer_config *ic, struct indexer_daemon_state *ids, struct stat_message *current_action);
int stop_delta_indexer(struct delta_indexer *di);

int delta_consumer_fetch_index(struct delta_consumer *dc, int fd);

struct delta_consumer *create_delta_consumer(struct indexer_daemon_state *ids, int idx, struct bconf_node *n, bool aux, bool remove_if_down);
int start_delta_consumer(struct delta_consumer *dc);
int stop_delta_consumer(struct delta_consumer *dc);
void delta_consumer_pause_resume(struct delta_consumer *ds, bool pause);
void delta_consumer_distribute_pending(struct delta_consumer *dc, bool activate);

void indexer_daemon_stop_consumer(struct indexer_daemon_state *ids, struct delta_consumer *dc);

void wait_for_everyone_to_consume_delta(struct indexer_daemon_state *ids, struct delta_control *d, bool and_pending);

struct timespec timeout_from_now(const struct timespec *ts);
