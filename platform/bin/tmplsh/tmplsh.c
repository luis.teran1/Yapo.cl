#include <err.h>
#include <stdio.h>
#include <fcntl.h>
#include <limits.h>
#include <unistd.h>
#include <stdarg.h>

#include "unicode.h"
#include "ctemplates.h"
#include "bpapi.h"
#include "bconf.h"
#include "bconfig.h"
#include "patchvars.h"

struct bconf_node *vtree = NULL;

int
main(int argc, char *argv[]) {
	struct bpapi ba;

	BPAPI_INIT_BCONF(&ba, vtree, stdout, hash);

	if (!call_template(&ba, "main"))
		errx(1, "call_template failed");

	bpapi_free(&ba);
	bconf_free(&vtree);
	return 0;
}

static int
loop_stdin(struct bpapi *api) {
	struct patch_data *pd = BPAPI_SIMPLEVARS_DATA(api);
	char *str;
	size_t n;

	if (bps_has_key(&pd->vars, "stdin"))
		bps_remove(&pd->vars, "stdin");

	if (feof(stdin))
		return 0;

	str = NULL;
	n = 0;
	if (getline(&str, &n, stdin) == -1)
		return 0;
	bps_insert(&pd->vars, "stdin", str);
	free(str);
	return 1;
}

#define loop_length_stdin loop_length_patch

static int
init_stdin(const struct bpfilter *filter, struct bpapi *api, int argc, const struct tpvar **argv) {
	struct patch_data *pd = BPAPI_SIMPLEVARS_DATA(api);

	patchvars_init(pd, "stdin", "", 0, NULL, NULL);

	return 0;
}

ADD_LOOP_FILTER(stdin, NULL, 0, &patch_simplevars, sizeof(struct patch_data), NULL, 0);
