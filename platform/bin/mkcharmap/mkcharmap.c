
#include <stdio.h>
#include <err.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>
#include <unicode/uchar.h>

#include "sections.h"
#include "util.h"

static char charmap[256];

static void
init_default_charmap(void) {
	int i;

	for (i = 0 ; i < 0x20 ; i++)
		charmap[i] = ' ';
	charmap['\2'] = '\2';
	charmap['\3'] = '\3';
	for (i = 0x20 ; i <= 0x60 ; i++)
		charmap[i] = i;
	charmap['!'] = ' ';
	charmap['#'] = ' ';
	charmap['$'] = ' ';
	charmap['%'] = ' ';
	charmap['\''] = ' ';
	charmap['('] = ' ';
	charmap[')'] = ' ';
	charmap['+'] = ' ';
	charmap[','] = '.';
	charmap[';'] = ' ';
	charmap['<'] = ' ';
	charmap['='] = ' ';
	charmap['>'] = ' ';
	charmap['?'] = ' ';
	charmap['@'] = ' ';
	charmap['['] = ' ';
	charmap['\\'] = ' ';
	charmap[']'] = ' ';
	charmap['^'] = ' ';
	charmap['_'] = ' ';
	charmap['`'] = ' ';
	for (i = 0x61 ; i <= 0x7A ; i++)
		charmap[i] = i - 0x20;
	charmap['{'] = ' ';
	charmap['|'] = ' ';
	charmap['}'] = ' ';
	charmap['~'] = ' ';
	charmap[0x7F] = ' ';
	for (i = 0x80 ; i <= 0xBF ; i++)
		charmap[i] = ' ';
	charmap[0x83] = 'F';
	charmap[0x8A] = 'S';
	charmap[0x8C] = 'O';
	charmap[0x8E] = 'Z';
	charmap[0x9A] = 'S';
	charmap[0x9C] = 'O';
	charmap[0x9E] = 'Z';
	charmap[0x9F] = 'Y';
	for (i = 0xC0 ; i <= 0xC6 ; i++)
		charmap[i] = 'A';
	charmap[0xC7] = 'C';
	for (i = 0xC8 ; i <= 0xCB ; i++)
		charmap[i] = 'E';
	for (i = 0xCC ; i <= 0xCF ; i++)
		charmap[i] = 'I';
	charmap[0xD0] = 'D';
	charmap[0xD1] = 'N';
	for (i = 0xD2 ; i <= 0xD6 ; i++)
		charmap[i] = 'O';
	charmap[0xD7] = 'X';
	charmap[0xD8] = 'O';
	for (i = 0xD9 ; i <= 0xDC ; i++)
		charmap[i] = 'U';
	charmap[0xDD] = 'Y';
	charmap[0xDE] = ' ';
	charmap[0xDF] = 'B';
	for (i = 0xE0 ; i <= 0xE6 ; i++)
		charmap[i] = 'A';
	charmap[0xE7] = 'C';
	for (i = 0xE8 ; i <= 0xEB ; i++)
		charmap[i] = 'E';
	for (i = 0xEC ; i <= 0xEF ; i++)
		charmap[i] = 'I';
	charmap[0xF0] = 'D';
	charmap[0xF1] = 'N';
	for (i = 0xF2 ; i <= 0xF6 ; i++)
		charmap[i] = 'O';
	charmap[0xF7] = ' ';
	charmap[0xF8] = 'O';
	for (i = 0xF9 ; i <= 0xFC ; i++)
		charmap[i] = 'U';
	charmap[0xFD] = 'Y';
	charmap[0xFE] = ' ';
	charmap[0xFF] = 'Y';
}

static int
charmap_sb(FILE *outfile, FILE *infile) {
	char buf[256];
	unsigned int i;
	int n = 0;
	unsigned char dstmap[256] = {0};
	uint32_t ndst = 0;

	while (fgets(buf, sizeof (buf), infile)) {
		char src = buf[0];
		char dst = ' ';
		
		if (!src || src == '#')
			continue;

		i = 1;
		switch (src) {
		case '\\':
			src = buf[i++];
			break;
		case '^':
			src = buf[i++];
			if (src == '?')
				src = 0x7F;
			else
				src = ((unsigned char)src - '@') & 0x1F;
			break;
		case '~':
			src = buf[i++];
			src = 0x80 + (((unsigned char)src - '@') & 0x1F);
			break;
		case '@':
			errx(1, "@ has to be escaped since it used in UTF-8 maps.");
		}
		if (buf[i] && buf[i] != '\n') /* XXX no actual support for \n as dst */
			dst = buf[i];

		charmap[src & 0xFF] = dst;
		n++;
	}

	for (i = 0 ; i < 0x100 ; i++) {
		if (!dstmap[charmap[i] & 0xFF]) {
			dstmap[charmap[i] & 0xFF] = 1;
			ndst++;
		}
	}

#if 0
	printf("Parsed %d entries (%d default) mapped to %d characters.\n", n, 256 - n, (int)ndst);
#endif

	if (fwrite(charmap, sizeof (charmap), 1, outfile) != 1)
		err(1, "fwrite(charmap)");
	if (fwrite(&ndst, sizeof(ndst), 1, outfile) != 1)
		err(1, "fwrite(ndst)");

	return 0;
}

static struct section *
find_section(const char *str) {
	struct section *sec;
	char *estr = NULL;
	int val;
	int notval = 0;
	struct section *cand = NULL;

	while (*str == '@' || isspace(*str))
		str++;

	val = strtol(str, &estr, 16);
	if (estr && *estr && !isspace(*estr))
		notval = 1;

	for (sec = sections ; sec->name ; sec++) {
		if (notval) {
			if (strcasecmp(sec->name, str) == 0) {
				/* Exact match. */
				return sec;
			}
			if (strcasestr(sec->name, str)) {
				if (cand)
					errx(1, "Multiple sections matches \"%s\"", str);
				cand = sec;
			}
		} else {
			if (sec->start <= val && sec->end >= val)
				return sec;
		}
	}
	return cand;
}

static int
charmap_utf8(FILE *outfile, FILE *infile) {
	struct section *sec = NULL;
	UChar32 *data = NULL;
	char buf[256];
	int i;
	int nsec = 0;
	int n = 0;

	while (fgets(buf, sizeof (buf), infile)) {
		UChar32 from;
		UChar32 to;

		if (!buf[0] || buf[0] == '#')
			continue;

		if (buf[0] == '@') {
			int l = strlen(buf);
			while (l > 0 && isspace(buf[l - 1]))
				l--;
			buf[l] = '\0';
			sec = find_section(buf);
			if (!sec)
				errx(1, "Section \"%s\" not found.", buf);
			if (!sec->data) {
				data = sec->data = xmalloc((sec->end - sec->start + 1) * sizeof(UChar32));
				if (sec->start <= 0x80) {
					for (i = sec->start ; i <= sec->end ; i++)
						data[i - sec->start] = tolower(charmap[i]);
				} else {
					for (i = sec->start ; i <= sec->end ; i++)
						data[i - sec->start] = i;
				}
				nsec++;
			} else {
				data = sec->data;
			}
			continue;
		}

		if (!sec)
			errx(1, "Character but no section.");

		const char *ptr = buf;

		switch (*ptr) {
		case '\\':
			if (!*++ptr)
				errx(1, "\\ with nothing after.");
			from = utf8_char(&ptr);
			break;
		case '^':
			from = *++ptr;
			if (from == '?')
				from = 0x7F;
			else
				from = ((unsigned char)from - '@') & 0x1F;
			ptr++;
			break;
		case '~':
			from = *++ptr;
			from = 0x80 + (((unsigned char)from - '@') & 0x1F);
			ptr++;
			break;
		default:
			from = utf8_char(&ptr);
			break;
		}

		to = utf8_char(&ptr);
		if (!to || to == '\n')
			to = ' ';

		if (from < sec->start || from > sec->end)
			errx(1, "Character U+%04X outside of section \"%s\"", (int)from, sec->name);

		if (from < 0x10000 && to >= 0x10000)
			errx(1, "Can't map from a character < U+10000 to one >= U+10000");

		data[from - sec->start] = to;
		n++;
	}

	/* We need to know how many subs to allocate. Since subs are byte pairs, figure out have many
	 * bytes are used in UTF-8 encoding.
	 */
	int sum = 0;
	unsigned char dstmap[0x100] = {0};
	uint32_t ndst = 0;
	for (sec = sections ; sec->name ; sec++) {
		if (sec->data) {
			for (i = 0 ; i <= sec->end - sec->start ; i++) {
				int to = ((UChar32*)sec->data)[i];
				unsigned char to8[5];
				int j = 0;

				memset(to8, 0, sizeof(to8));
				U8_APPEND_UNSAFE(to8, j, to);

				for (j = 0 ; to8[j] ; j++) {
					if (!dstmap[to8[j]]) {
						dstmap[to8[j]] = 1;
						ndst++;
					}
				}
			}
			sum += sec->end - sec->start + 1;
		}
	}

#if 0
	printf("Parsed %d entries (%d default) mapped to %d bytes.\n", n, sum - n, (int)ndst);
#endif

	fwrite("UTF8", sizeof ("UTF8") - 1, 1, outfile);
	fwrite(&ndst, sizeof(ndst), 1, outfile);
	fwrite(&nsec, sizeof(nsec), 1, outfile);
	uint32_t off = sizeof("UTF8") - 1 + 2 * sizeof(uint32_t) + nsec * sizeof(uint32_t[3]);

	/* Align to 16 bytes. probably not needed but easier on hexdumps. */
	size_t pad = 0;
	if (off & 0xF) {
		pad = 0x10 - (off & 0xF);
		off += pad;
	}

	for (sec = sections ; sec->name ; sec++) {
		uint32_t flat[3];

		if (sec->data) {
			flat[0] = sec->start;
			flat[1] = sec->end;
			flat[2] = off;
			fwrite(flat, sizeof(flat), 1, outfile);
			off += (sec->end - sec->start + 1) * sizeof (UChar32);
		}
	}

	if (pad)
		fwrite ("\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0", 1, pad, outfile);

	for (sec = sections ; sec->name ; sec++) {
		if (sec->data) {
			fwrite(sec->data, (sec->end - sec->start + 1) * sizeof (UChar32), 1, outfile);
			free(sec->data);
		}
	}

	return 0;
}

int
main(int argc, char *argv[]) {
	FILE *of;
	FILE *inf;
	int res;
	char buf[256];

	if (argc != 3)
		errx(1, "Usage: %s <outfile> <infile>", argv[0]);

	init_default_charmap();

	inf = fopen(argv[2], "r");
	if (!inf)
		err(1, "open(%s)", argv[2]);

	of = fopen(argv[1], "wb");
	if (!of)
		err(1, "open(%s)", argv[1]);

	if (!fgets(buf, sizeof (buf), inf))
		errx(1, "file is empty!?");

	if (strstr(buf, "UTF-8"))
		res = charmap_utf8(of, inf);
	else
		res = charmap_sb(of, inf);

	fclose(inf);
	fclose(of);

	return res;
}
