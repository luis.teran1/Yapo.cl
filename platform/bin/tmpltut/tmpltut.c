#include <dlfcn.h>
#include <stdio.h>
#include <fcntl.h>
#include <limits.h>
#include <pthread.h>
#include <unistd.h>
#include <stdarg.h>

#include "unicode.h"
#include "ctemplates.h"
#include "bpapi.h"
#include "bconf.h"
#include "bconfig.h"
#include "transapi.h"
#include "logging.h"
#include "timer.h"

int syslog_ident(const char *ident, const char *fmt, ...);

const char *tmpls[] = {
	"simplevars",
	"functions_and_filters",
	"vtree",
	"advanced",
	"tpfilters",
	"hash_filters",
	"html_filters",
	"tpfunctions",
	"vtree_filters",
	"blocket_functions",
	"pgsqlfilter",
	"search_filters",
	"trans_filters",
	"trans_conf_filters",
	"redis_filters",
	"utf8_filters",
	"regressions",
	"blockargs",
	"xml_filters",
	"loop_filters",
	"friendly_example",
	"issues_and_defects",
	"search_colon",
	"json_filters",
	"deffun",
	NULL
};

static void
usage(const char *progname) {
	const char **t;

	fprintf(stderr, "Usage %s [-c <bconf.conf>] <topic>\n\nTopic is one of:\n", progname);

	for (t = tmpls ; *t ; t++)
		fprintf(stderr, "   %s\n", *t);
	exit(1);
}

int
syslog_ident(const char *ident, const char *fmt, ...) {
	/* syslog_ident override for regress test */
	va_list ap;
	int res;

	if (strcmp(ident, "sysloghook") == 0)
		return 0;

	va_start(ap, fmt);
	res = vprintf(fmt, ap);
	va_end(ap);
	res += printf("\n");
	return res;
}

static void
catfile(const char *fn) {
	int fd = open(fn, O_RDONLY);
	char buf[1024];
	int n;

	if (fd < 0)
		xerr(1, "open(%s)", fn);

	while ((n = read(fd, buf, sizeof(buf))) > 0)
		UNUSED_RESULT(write(1, buf, n));
	close(fd);
}

int
main(int argc, char *argv[]) {
	struct bpapi ba;
	const char **t;
	char tnbuf[256];
	const char *prog = argv[0];
	struct bconf_node *vtree = NULL;
	int fstate = 1;
	char ch;

	while ((ch = getopt(argc, argv, "c:f")) != -1) {
		switch (ch) {
		case 'c':
			vtree = config_init(optarg);

			if (!vtree)
				xerr(1, "config_init");

			load_bconf("tmpltut", &vtree);
			break;
		case 'f':
			fstate = 0;
			break;
		default:
			usage(prog);
		}
	}
	argc -= optind;
	argv += optind;

	if (argc != 1)
		usage(prog);

	for (t = tmpls ; *t ; t++) {
		if (strcmp(*t, argv[0]) == 0)
			break;
	}
	if (!*t)
		usage(prog);

	log_setup("tmpltut", "debug");

	if (fstate)
		filter_state_bconf_init_all(&vtree);

	default_filter_state = filter_state_bconf;

	bconf_add_data(&vtree, "pgsql_session.db_name", "db");

	bconf_add_data(&vtree, "path.to.value", "foo");
	bconf_add_data(&vtree, "path.and.value", "foo");
	bconf_add_data(&vtree, "path.thisnode.test", "bar");
	bconf_add_data(&vtree, "path.foo.foo", "foobar");
	bconf_add_data(&vtree, "path.this.node.test", "barfoo");
	bconf_add_data(&vtree, "path.this.value", "other");
	bconf_add_data(&vtree, "other.one", "1");
	bconf_add_data(&vtree, "other.two", "2");

	bconf_add_data(&vtree, "testing.settings.some.path", "foo");

	bconf_add_data(&vtree, "settings.foo.1.keys.1", "x1");
	bconf_add_data(&vtree, "settings.foo.1.keys.2", "list1");
	bconf_add_data(&vtree, "settings.foo.1.test.one.value", "answer:yes");
	bconf_add_data(&vtree, "settings.foo.1.test.two.value", "answer:nope");

	bconf_add_data(&vtree, "settings.foo_qm.1.keys.1", "?simplevar");
	bconf_add_data(&vtree, "settings.foo_qm.1.true.value", "answer:yes");
	bconf_add_data(&vtree, "settings.foo_qm.1.false.value", "answer:nope");

	bconf_add_data(&vtree, "settings.foo_ep.1.keys.1", "!simplevar");
	bconf_add_data(&vtree, "settings.foo_ep.1.true.value", "answer:yes");
	bconf_add_data(&vtree, "settings.foo_ep.1.false.value", "answer:nope");

	bconf_add_data(&vtree, "settings.foo_vt.1.keys.1", "$some.path");
	bconf_add_data(&vtree, "settings.foo_vt.1.foo.value", "answer:yes");

	bconf_add_data(&vtree, "settings.foo_vtqm.1.keys.1", "?$some.path");
	bconf_add_data(&vtree, "settings.foo_vtqm.1.true.value", "answer:yes");
	bconf_add_data(&vtree, "settings.foo_vtqm.1.false.value", "answer:nope");

	bconf_add_data(&vtree, "settings.foo_vtep.1.keys.1", "!$some.path");
	bconf_add_data(&vtree, "settings.foo_vtep.1.true.value", "answer:yes");
	bconf_add_data(&vtree, "settings.foo_vtep.1.false.value", "answer:nope");

	bconf_add_data(&vtree, "settings.whitespace.1.default", "answer:yes\nno\tmaybe");

	bconf_add_data(&vtree, "common.referer_searchwords.max_words", "5");
	bconf_add_data(&vtree, "common.referer_searchwords.searchengines.1.name_pattern", "google.com");
	bconf_add_data(&vtree, "common.referer_searchwords.searchengines.1.query_param", "q");
	bconf_add_data(&vtree, "common.referer_searchwords.searchengines.2.name_pattern", "msn.com");
	bconf_add_data(&vtree, "common.referer_searchwords.searchengines.2.query_param", "q");
	bconf_add_data(&vtree, "common.referer_searchwords.searchengines.3.name_pattern", "live.com");
	bconf_add_data(&vtree, "common.referer_searchwords.searchengines.3.query_param", "q");
	bconf_add_data(&vtree, "common.referer_searchwords.searchengines.4.name_pattern", "yahoo.com");
	bconf_add_data(&vtree, "common.referer_searchwords.searchengines.4.query_param", "p");
	bconf_add_data(&vtree, "common.referer_searchwords.searchengines.5.name_pattern", "trovit.pt");
	bconf_add_data(&vtree, "common.referer_searchwords.searchengines.5.path.start", "/what_d.");
	bconf_add_data(&vtree, "common.referer_searchwords.searchengines.5.path.end", "/");
	bconf_add_data(&vtree, "common.referer_searchwords.query_autodetect", "0");
	bconf_add_data(&vtree, "common.referer_searchwords.query_autodetect_ignore.1.hostpart", "blocket.se");

	bconf_add_data(&vtree, "lang_settings.code_message.1.keys.1", "code");
	bconf_add_data(&vtree, "lang_settings.code_message.1.TRANSLATE_ME.value", "value:translated");
	bconf_add_data(&vtree, "lang_settings.code_message.1.TODAY.value", "value:today");
	bconf_add_data(&vtree, "lang_settings.code_message.1.YESTERDAY.value", "value:yesterday");
	bconf_add_data(&vtree, "lang_settings.code_message.2.keys.1", "code_cat");
	bconf_add_data(&vtree, "lang_settings.code_message.2.keys.2", "code");
	bconf_add_data(&vtree, "lang_settings.code_message.2.boat_type.1.value", "value:sailing boat");
	bconf_add_data(&vtree, "lang_settings.code_message.2.long_month.4.value", "value:April");
	bconf_add_data(&vtree, "lang_settings.code_message.2.short_month.4.value", "value:Apr");
	bconf_add_data(&vtree, "lang_settings.code_message.3.keys.1", "code");
	bconf_add_data(&vtree, "lang_settings.code_message.3.keys.2", "quantity");
	bconf_add_data(&vtree, "lang_settings.code_message.3.DAYS.1.value", "value:day");
	bconf_add_data(&vtree, "lang_settings.code_message.3.DAYS.*.value", "value:days");
	bconf_add_data(&vtree, "lang_settings.code_message.3.HOURS.1.value", "value:hour");
	bconf_add_data(&vtree, "lang_settings.code_message.3.HOURS.*.value", "value:hours");
	bconf_add_data(&vtree, "lang_settings.code_message.3.MINUTES.1.value", "value:minute");
	bconf_add_data(&vtree, "lang_settings.code_message.3.MINUTES.*.value", "value:minutes");

	bconf_add_data(&vtree, "common.region.1.municipality.1.name", "Stockholm city");
	bconf_add_data(&vtree, "common.region.1.municipality.1.subarea.1.name", "Vasastan");
	bconf_add_data(&vtree, "common.region.1.city.1.municipality.1.name", "Danderyd");

	bconf_add_data(&vtree, "cat.1000.name", "VEHICLES");
	bconf_add_data(&vtree, "cat.1000.level", "0");
	bconf_add_data(&vtree, "cat.1020.name", "Cars");
	bconf_add_data(&vtree, "cat.1020.level", "1");
	bconf_add_data(&vtree, "cat.1020.parent", "1000");

	bconf_add_data(&vtree, "lang_settings.type_name.1.keys.1", "type");
	bconf_add_data(&vtree, "lang_settings.type_name.1.s.value", "value:for sell");

	bconf_add_data(&vtree, "common.linkshelf.layout.3.10.block_cols", "3");
	bconf_add_data(&vtree, "common.linkshelf.layout.3.10.tail_rows", "1");
	bconf_add_data(&vtree, "brand.car.volvo.name", "Volvo");
	bconf_add_data(&vtree, "brand.car.volvo.model.xc70.name", "XC70");
	bconf_add_data(&vtree, "brand.car.volvo.model.xc70.regex", "\\b(XC70|V70XC)\\b");

	bconf_add_data(&vtree, "common.basedir", MYDIR);
	bconf_add_data(&vtree, "common.sitename", "coordination.blocket.com");
	bconf_add_data(&vtree, "common.base_url.vi", "http://coordination.blocket.com");
	bconf_add_data(&vtree, "common.base_url.li", "http://coordination.blocket.com");

	bconf_add_data(&vtree, "common.seo.friendly.enabled", "1");
	bconf_add_data(&vtree, "common.seo.url_replace.chars",
			"\xE5""a\xE4""a\xE1""a\xE0""a\xE2""a\xE3""a\xF6o\xF3o\xF2o\xF4o\xF5o\xC5""A\xC4""A\xC1""A"
			"\xC0""A\xC2""A\xC3""A\xD6O\xD3O\xD2O\xD4O\xD5O\xEB""e\xE9""e\xE8""e\xEA""e\xCB""E\xC9""E\xC8""E"
			"\xCA""E\xF1n\xD1N _\"_,_/_\\_(_)_:_");
			/* åaäaáaàaâaãaöoóoòoôoõoÅAÄAÁAÀAÂAÃAÖOÓOÒOÔOÕOëeéeèeêeËEÉEÈEÊEñnÑN */
	bconf_add_data(&vtree, "common.seo.nearby.text", "nearby-counties");
	bconf_add_data(&vtree, "common.seo.allcountry.text.en", "all-country");
	bconf_add_data(&vtree, "common.seo.region.friendlyname.1.en", "stockholm");
	bconf_add_data(&vtree, "common.seo.city.friendlyname.1_1.en", "norrort");
	bconf_add_data(&vtree, "common.seo.cat.friendlyname.1020.en", "cars");

	BPAPI_INIT_BCONF(&ba, vtree, stdout, hash);

	bpapi_insert(&ba, "x1", "test");

	bpapi_insert(&ba, "list1", "one");
	bpapi_insert(&ba, "list1", "two");
	bpapi_insert(&ba, "list1", "three");

	bpapi_insert(&ba, "list2", "1");
	bpapi_insert(&ba, "list2", "2");
	bpapi_insert(&ba, "list2", "3");

	bpapi_insert(&ba, "listten", "1");
	bpapi_insert(&ba, "listten", "2");
	bpapi_insert(&ba, "listten", "3");
	bpapi_insert(&ba, "listten", "4");
	bpapi_insert(&ba, "listten", "5");
	bpapi_insert(&ba, "listten", "6");
	bpapi_insert(&ba, "listten", "7");
	bpapi_insert(&ba, "listten", "8");
	bpapi_insert(&ba, "listten", "9");
	bpapi_insert(&ba, "listten", "10");

	bpapi_insert(&ba, "b__lang", "en");

	bpapi_insert(&ba, "err_foo", "Not enough bars");
	bconf_add_data(&vtree, "chk_err_settings.error_message.1.keys.1", "appl");
	bconf_add_data(&vtree, "chk_err_settings.error_message.1.keys.2", "field");
	bconf_add_data(&vtree, "chk_err_settings.error_message.1.keys.3", "error");
	bconf_add_data(&vtree, "chk_err_settings.error_message.1.ai.subject.ERROR_SUBJECT_BLOCKED.value", "template:error_subject.txt");
	bconf_add_data(&vtree, "lang_settings.code_message.1.ERROR_SUBJECT_BLOCKED.value", "value:Your title contains prohibited words");
	bpapi_insert(&ba, "err_default", "Not enough beers");
	bconf_add_data(&vtree, "common.error.template", "error_default.txt");
	bpapi_insert(&ba, "appl", "ai");
	bpapi_insert(&ba, "err_subject", "ERROR_SUBJECT_BLOCKED");

	if (isatty(1)) {
		snprintf(tnbuf, sizeof(tnbuf), MYDIR "/templates/%s.txt.tmpl", *t);
		catfile(tnbuf);
		printf("\x1b[1mPress enter to see output\x1b[0m");
		getchar();
	}

	template_timers_ctl(1);
	snprintf(tnbuf, sizeof(tnbuf), "%s.txt", *t);
	if (!call_template(&ba, tnbuf))
		xerrx(1, "call_template(%s) failed", tnbuf);

	flush_template_cache(NULL);
	bpapi_output_free(&ba);
	bpapi_simplevars_free(&ba);
	bpapi_vtree_free(&ba);
	/*
	 * Filter_state_bconf_free assumes that the bconf tree we have is exclusively ours and
	 * will bconf_free it.
	 */
	filter_state_bconf_free(&vtree);
	flush_template_list();
	timer_clean();

	/* Make an effort to kill of TLS in glibc, should remove all "still reachable" from valgrind */
	pthread_exit(NULL);

	return 0;
}
