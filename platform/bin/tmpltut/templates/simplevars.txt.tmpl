Templates are used for rendering text from variables, configuration and some user defined functions and filters.

In a normal mode, text is passed through without any modifications:
Ordinary text << << < % < % >> >> > % > % \/\/

Any characters are legal until we end up in a template block:
<% this is a template block, most characters behave normally in here too %>

Template blocks use \ as escape character, similar to other languages. For example \n will generate a new line
<% like \n this %>

As you can see, template blocks are rendered just like any other text. The interesting things start happening when
we start using variables:
<% This is a variable access - %x1 %>

%x1 means that we access the contents of the variable named "x1". Variables are set by the calling application.

Variables can have multiple values, and thus become a list. To access every value in a list, use the @ character instead of %.
<% - @list1 %>
The whole template block is repeated once for every item in the list.

<% %list1 %> outputs the first element only.

Template blocks can be nested, which is useful for lists:
<% line - @list1 <% column - @list2 %>\n%>

If you wish to output the value of @list1 in every column, but only change it for every line just like above, you can use @@. Additional @ can be added as more blocks are nested.
<% line - <% @@list1 - @list2 %>\n%>

@# is a special variable containing the current loop index, starting at 0.
#@ is another special variable containing the length of the loop.
<% @#/#@ - @list1 %>
You will get a compile error if you try to use these where there is no loop.

You can also see the size of a specific list without creating a loop:
<% #@list1 %>

You can decide if a block should be evaluated using conditionals.
<%(%x1 == "test")This is printed.%>
:: is the else statement, and can also have a conditional to create an else-if.
<%(%x1 == "nope")This is not printed::(%x1 == "foo")nor is this::but this is%>

<% @list1 - <%@list2<%(@@# < #@@ - 1), %>%>\n%>
<% @list1 - <%<%(@@# > 0), %>@list2%>\n%>
both output the same text, see if you can understand how it works.

