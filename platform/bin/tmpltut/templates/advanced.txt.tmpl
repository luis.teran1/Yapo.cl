
When you are looping on multiple lists at the same level, the longest list determines the number of loops, and the shorter one is wrapped to beginning.
<% @listten @list1\n%>

Lists can be accessed by index.
<% %list2[1] == 2 %>

There are several operators available for conditionals.
<%(?x1)variable existence operator%>
<%(%x1 == "test")equility operator%>
<%(%x1 != "bar")inequility operator%>
<%(%list2[2] > 1)greater than%>
<%(%list2[2] >= 1)greater than or equal%>
<%(%list2[1] < 3)lesser than%>
<%(%list2[1] <= 3)lesser than or equal%>
<%(%x1 =~ "/es/")regex match%>
<%(%x1 ! =~ "/et/")regex negative match%>
<%(%x1 =~ "/et/v")another regex negative match%>
<%(!?x1)boolean negation operator%>
<%(%x1 == "test" || %x1 == "fest")boolean or%>
<%(%x1 == "test" && %x1 != "fest")boolean and%>
<%((%x1 == "test" && %x1 != "fest") || %x1 == "foo")parentheses%>

The operator and function arguments support simple arithmetics.
<%(%list2[1] + 1 == 3)addition%>
<%(%list2[1] - 1 == 1)subtraction%>
<%(%list2[1] * 1 == 2)multiplication%>
<%(%list2[1] / 2 == 1)division%>
<%(%list2[1] % 2 == 0)remainder%>

A useful debugging tool, and also sometimes used for looping, is to create a loop over all defined variable names.
<% %* %>
Outer looping is done by adding more %
<% %* <% %%* %>\n%>

It is also possible to only select variables based on a prefix.
<% %list* %>

To access the value of a named variable %[] or @[] can be used, ?[] and #@[] also works.
<%(?["x1"])%["x1"]%>
This is useful to combine with globs. To output all variables and values:
<%%* => (#@[%*]) <%<%(@@# > 0), %>@[%%*]%>\n%>

@# has a companion branch counter variable named @*. In a normal loop they have the same value.
<% @list1, @# == @*\n%>

If we add a conditional at the same level as the loop, @* will only be incremented when the branch it is used in is evaluated.
<%(@# % 2 == 0)@listten has branch counter @*, loop counter is @#\n%>

Outer branch counters are accessed with more @, as usual.
<%@listten <%(@@# % 2 == 0)has branch counter @@*, loop counter is @@#%>\n%>

However, where the counter is incremented is determined by the number of stars, so to only increase when the conditional is true:
<%@listten <%(@@# % 2 == 0)has two-star branch counter @@**, loop counter is @@#%>\n%>

This works for double loops as well.
<%<%@@listten-@list1 has two-star branch counter @@**, loop counters are @#/@@#\n%>%>

Multiple filters can be applied to the same block.
<%|rot13,rot13|rot26%>

Filters can take constructs as arguments, for example the regex_replace filter that can take a regex construct.
<%|regex_replace(R["FE", "i"], "te")|fest%>
The second parameter is regexp flags:
i is for case insensitive match.
U is to do a UTF-8 match.

The regex construct can also be used for regex conditionals.
<%(%x1 =~ R["st$"])matched%>

The MAP construct can be used to map between strings.
<%&(MAP["" => "empty", "test" => "tset", "foo" => "bar"][%x1])%>
It also supports a default key if no match.
<%&(MAP["foo" => "bar", default => "baz"][%x1])%>

