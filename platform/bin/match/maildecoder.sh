#!/bin/bash

MAXLINE=76

line=

IFS=

if [ "$1" = "full" ]; then
	DECODER=(perl -pe 's/=([0-9A-Fa-f]{2})/chr hex($1)/ge')
	shift
else
	DECODER=cat
fi

function pqparse {
	local softline=0

	while read line; do
		if [[ ${#line} -gt $MAXLINE ]]; then
			echo -e "\nTHIS LINE IS TOO LONG"
			return 2;
		fi

		if [[ $softline -ne 1 ]]; then
			if [[ $line == "--${1}" ]]; then
				echo "$line"
				return 0
			fi
			if [[ $line == "--${1}--" ]]; then
				echo "$line"
				return 1
			fi
		fi
		if [[ "$line" == *= ]]; then
			echo -n "${line:0:${#line}-1}" | ${DECODER[@]}
			softline=1
		else
			echo "$line" | ${DECODER[@]}
			softline=0
		fi
	done

	return 1
}

function parsemime {
	# Read headers
	local qp=0
	local boundary=
	local mime_block_stated=0

	if [[ -n $line && $line == "--${1}--" ]]; then
		return 1
	fi

	while read line; do
		if [[ -n "${1}" && $line == "--${1}" ]]; then
			mime_block_started=1
		fi

		if [[ ${#line} -eq 0 ]]; then
			echo 
			# If whe have extra empty lines before a MIME block, just ignore them
			if [[ -n "${1}" && $mime_block_started -eq 0 ]]; then
				continue
			else
				break
			fi
		fi	

		if [[ $line == "Content-Transfer-Encoding: quoted-printable"* ]]; then
			qp=1 # Only allow QP or mime multipart...
		fi

		if [[ $line == *"boundary=\""* ]]; then
			boundary=${line##*boundary=\"};
			boundary=${boundary%%\"*}
		fi

		echo "$line"
	done

	if [[ $qp -eq 1 ]]; then
		pqparse $1
		return $?
	fi

	if [[ -n $boundary ]]; then
		# Print everyting prior to marker
		while [[ 1 ]]; do
			parsemime $boundary || break
		done
		return $?
	fi

	while read line; do
		echo $line
	done;

	return 1
}

parsemime

