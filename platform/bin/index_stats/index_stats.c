
#include "index.h"
#include "hash.h"
#include "util.h"
#include "logging.h"
#include "tree.h"
#include "bconf.h"
#include "json_vtree.h"
#include "vtree.h"
#include <stdio.h>
#include <err.h>
#include <string.h>
#include <getopt.h>

struct counter {
	SPLAY_ENTRY(counter) link;
	char *name;
	size_t total;
	size_t key;
	size_t value;
	size_t docs;
	size_t docpos;
	size_t n;
};

int r_flag = 0;
int v_flag = 0;
int d_flag = 0;
int idfilter[100];
unsigned int nidfilter;

static int
cmp_counter(struct counter *a, struct counter *b) {
	int mult = r_flag ? -1 : 1;

	if (a->total != b->total)
		return (a->total - b->total) * mult;
	if (a->key != b->key)
		return (a->key - b->key) * mult;
	if (a->value != b->value)
		return (a->value - b->value) * mult;
	if (a->docs != b->docs)
		return (a->docs - b->docs) * mult;
	if (a->docpos != b->docpos)
		return (a->docpos - b->docpos) * mult;
	if (a->n != b->n)
		return (a->n - b->n) * mult;
	if (strcmp(a->name, b->name) != 0)
		return strcmp(a->name, b->name) * mult;
	return (a - b) * mult;
}

SPLAY_HEAD(counter_tree, counter) tree;
SPLAY_PROTOTYPE(counter_tree, counter, link, cmp_counter);
SPLAY_GENERATE(counter_tree, counter, link, cmp_counter);

static void *
counter_alloc(const void *key, int klen, void **data, void *cbarg) {
	*data = zmalloc(sizeof (struct counter));
	return ((struct counter*)*data)->name = xstrndup(key, klen);
}

static void
dump_invattr_sizes(struct index_db *db) {
	unsigned int n;
	struct index_attrs_iter *iter = index_attrs_iter_open(db, 0, &n);
	const char *curkey;
	int ckl;
	int cvl;
	struct hash_table *tbl = hash_table_create(1000000, free);
	struct counter *cnt;

	hash_table_free_keys(tbl, 1);

	SPLAY_INIT(&tree);
	if (iter) {
		do {
			const char *keycol;
			int colon;
			const int32_t *curval;

			index_attrs_iter_get(iter, &curkey, &ckl, &curval, &cvl);

			if (v_flag)
				keycol = NULL;
			else
				keycol = strchr(curkey, ':');
			if (!keycol) {
				colon = 0;
				keycol = curkey + ckl;
			} else {
				colon = 1;
			}

			if (nidfilter) {
				unsigned int i, j;
				int n = 0;
				for (i = 0 ; i < cvl / sizeof (*curval) ; i++) {
					const struct doc *doc = index_get_doc(db, curval[i]);
					for (j = 0 ; j < nidfilter ; j++) {
						if (doc->id == idfilter[j]) {
							n++;
							break;
						}
					}
				}
				if (!n)
					continue;
				cvl = n * sizeof (*curval);
			}

			cnt = hash_table_update(tbl, curkey, keycol - curkey, counter_alloc, NULL);
			if (cnt->n)
				SPLAY_REMOVE(counter_tree, &tree, cnt);
			cnt->total += ckl + cvl;
			cnt->key += keycol - curkey + colon;
			cnt->value += ckl - (keycol - curkey);
			cnt->docs += cvl;
			cnt->n++;
			SPLAY_INSERT(counter_tree, &tree, cnt);
		} while (index_attrs_iter_next(iter));
	}

	printf("Key\tTotal size\tKey size\tValue size\tDoclist size\tN\n");
	SPLAY_FOREACH(cnt, counter_tree, &tree) {
		printf("%s\t%zu\t%zu\t%zu\t%zu\t%zu\n", cnt->name, cnt->total, cnt->key, cnt->value, cnt->docs, cnt->n);
	}
	hash_table_free(tbl);
}

static void
dump_invword_sizes(struct index_db *db) {
	unsigned int n;
	struct index_words_iter *iter = index_words_iter_open(db, 0, &n, NULL);
	const char *curkey;
	int ckl;
	const struct docindex *curval;
	int cvl;
	const struct docpos *curpos;
	int cpl;
	struct counter *cnt;

	SPLAY_INIT(&tree);
	if (iter) {
		do {
			void *p;
			index_words_iter_get(iter, &curkey, &ckl, &curval, &cvl, &curpos, &cpl);

			if (nidfilter) {
				unsigned int i, j;
				int n = 0;
				int ndp = 0;

				for (i = 0 ; i < cvl / sizeof (*curval) ; i++) {
					const struct doc *doc = index_get_doc(db, curval[i].docoff);
					for (j = 0 ; j < nidfilter ; j++) {
						if (doc->id == idfilter[j]) {
							n++;
							if (curval[i].ptr != INDEX_DOCINDEX_NO_POS)
								ndp += 1 + curpos[curval[i].ptr].pos;
							break;
						}
					}
				}
				if (!n)
					continue;
				cvl = n * sizeof (*curval);
				cpl = ndp * sizeof (*curpos);
			}

			counter_alloc(curkey, ckl, &p, NULL);
			cnt = p;
			cnt->total = ckl + cvl + cpl;
			cnt->key = ckl;
			cnt->docs = cvl;
			cnt->docpos = cpl;
			cnt->n = 1;
			SPLAY_INSERT(counter_tree, &tree, cnt);
		} while (index_words_iter_next(iter));
	}

	printf("Key\tTotal size\tKey size\tDoclist size\tDocpos size\n");
	while ((cnt = SPLAY_MIN(counter_tree, &tree))) {
		SPLAY_REMOVE(counter_tree, &tree, cnt);
		printf("%s\t%zu\t%zu\t%zu\t%zu\t\n", cnt->name, cnt->total, cnt->key, cnt->docs, cnt->docpos);
		free(cnt);
	}
}

static void
dump_docs_it(int n, const struct doc *key, int32_t docoff, const char *value, void *v) {
	if (nidfilter) {
		unsigned int j;
		for (j = 0 ; j < nidfilter ; j++) {
			if (key->id == idfilter[j])
				break;
		}
		if (j == nidfilter)
			return;
	}
	printf("[%d, %d, %d](%d) ", key->id, key->order, key->suborder, docoff);
}

static void
dump_docs(struct index_db *db) {
	struct index_docs_iter *it;
	unsigned int ndocs;

	if ((it = index_docs_iter_open(db, 0, &ndocs)) != NULL) {
		printf("DOCS: (%d)[", ndocs);
		if (v_flag)
			index_docs_iter_foreach(it, dump_docs_it, NULL);
		printf("]\n");
		index_docs_iter_close(it);
	}
}

static int
p_meta(void *v, int d, int n, const char *fmt, ...) {
	va_list ap;
	int res;

	while (d--)
		printf("\t");
	va_start(ap, fmt);
	res = vprintf(fmt, ap);
	va_end(ap);
	if (n)
		printf("\n");
	return res;
}

static void
dump_meta(struct index_db *db) {
	struct bpapi_vtree_chain vt;
	printf("META: ");
	bconf_vtree(&vt, db->meta);
	vtree_json(&vt, 0, 0, p_meta, NULL);
	vtree_free(&vt);
	printf("\n");
}

static void
dump_word(struct index_db *db, const char *word) {
	unsigned int n;
	struct index_words_iter *iter = index_words_iter_open(db, 0, &n, NULL);
	const char *curkey;
	int ckl;
	const struct docindex *curval;
	int cvl;
	const struct docpos *curpos;
	int cpl;

	do {
		index_words_iter_get(iter, &curkey, &ckl, &curval, &cvl, &curpos, &cpl);

		if (!strcmp(curkey, word)) {
			unsigned int i;
			int j;

			printf("%s: %d %d %d\n", word, ckl, cvl, cpl);

			for (i = 0; i < cvl / sizeof(*curval); i++) {
				const struct doc *doc = index_get_doc(db, curval[i].docoff);
				int pos = curval[i].ptr;
				printf("[%d,%d,%d](%d) -> ", doc->id, doc->order, doc->suborder, curval[i].docoff);
				for (j = 0; j < curpos[pos].pos + 1; j++) {
					const struct docpos *p = &curpos[j + pos];
					printf("%s{f:%d, p:%d, b:%d}", j ? ", " : "", p->flags, p->pos, p->rel_boost);
				}
				printf("\n");
			}
		}
	} while (index_words_iter_next(iter));
}

static void
dump_attr(struct index_db *db, const char *attr) {
	unsigned int n;
	struct index_attrs_iter *iter = index_attrs_iter_open(db, 0, &n);

	if (iter == NULL || n == 0)
		return;

	do {
		const char *key;
		int ksz;
		const int32_t *docarr;
		int dsz;

		index_attrs_iter_get(iter, &key, &ksz, &docarr, &dsz);

		if (!strcmp(key, attr)) {
			unsigned int i;

			for (i = 0; i < dsz / sizeof(*docarr); i++) {
				const struct doc *doc = index_get_doc(db, docarr[i]);

				printf("[%d,%d,%d], ", doc->id, doc->order, doc->suborder);
			}
		}
	} while (index_attrs_iter_next(iter));

	printf("\n");
}

extern int optind;
extern char *optarg;

int
main(int argc, char *argv[]) {
	struct index_db *db;
	int all = 1;
	int attrs = 0;
	int words = 0;
	int meta = 0;
	int index_file = 0;
	char *word = NULL;
	char *attr = NULL;
	char opt;

	while ((opt = getopt(argc, argv, "awrmvdi:W:fA:")) != -1) {
		switch (opt) {
		case 'a':
			all = 0;
			attrs = 1;
			break;
		case 'w':
			all = 0;
			words = 1;
			break;
		case 'r':
			r_flag = 1;
			break;
		case 'm':
			all = 0;
			meta = 1;
			break;
		case 'v':
			v_flag = 1;
			break;
		case 'd':
			d_flag = 1;
			all = 0;
			break;
		case 'i':
			if (nidfilter >= sizeof(idfilter) / sizeof(idfilter[0]))
				errx(1, "Too many ids");
			idfilter[nidfilter++] = atoi(optarg);
			break;
		case 'W':
			all = 0;
			word = strdup(optarg);
			break;
		case 'A':
			all = 0;
			attr = strdup(optarg);
			break;
		case 'f':
			index_file = 1;
			break;
		}
	}

	if (argc - optind < 1)
		errx(1, "usage: %s [-a] [-d] [-w] [-v] [-i <id>] <index>", argv[0]);

	log_setup_perror("index_stats", "debug");

	if ((db = open_index(argv[optind], INDEX_READER|(index_file ? INDEX_FILE : 0))) == NULL)
		err(1, "Failed to open index %s", argv[optind]);

	if ((all && !nidfilter) || meta)
		dump_meta(db);
	if (all || attrs)
		dump_invattr_sizes(db);
	if (all || words)
		dump_invword_sizes(db);
	if (all || d_flag)
		dump_docs(db);
	if (word)
		dump_word(db, word);
	if (attr)
		dump_attr(db, attr);

	close_index(db, 0);
	return 0;
}
