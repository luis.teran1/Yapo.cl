#include <getopt.h>
#include <string.h>
#include <err.h>
#include <stdio.h>

#include "bpapi.h"
#include "ctemplates.h"
#include "transapi.h"
#include "bconfig.h"
#include "bconf.h"
#include "unicode.h"

extern char *optarg;
extern int optind, opterr, optopt;

struct bconf_node *root = NULL;

static void
parse_varfile(struct bpapi *ba, const char *fname) {
	FILE *f = fopen(fname, "r");
	char line[1024];

	if (!f)
		err(1, "fopen");

	while (fgets(line, sizeof(line), f)) {
		char *eol = line + strlen(line);
		char *eq = strchr(line, '=');

		while (eol > line && *(eol - 1) == '\n')
			eol--;

		if (eol == line)
			continue;

		if (!eq)
			errx(1, "= missing in %s", fname);
		*eq++ = '\0';
		*eol = '\0';

		bpapi_insert(ba, line, eq);
	}
	fclose(f);
}

static void
load_conffile(const char *configfile, struct bconf_node **root) {
	struct bconf_node *conf = config_init(configfile);

	if (!conf)
		err(1, "config_init");

	bconf_merge(root, conf);
	bconf_free(&conf);
}

/*
 * The options:
 *  --configfile/-c: config-style bconf files (without the *.* prefixes).
 *  --varfile/-f: files with <key>=<value> pairs to populate simplevars for the template.
 *  --bconffile/-b: bconf-style bconf files (with the *.* or <host>.<appl> prefixes).
 *  --appl/-a: application used to parse the bconf.
 */

int
main(int argc, char *argv[])
{
	const struct option longopts[] = {
		{"configfile", 1, NULL, 'c'},
		{"appl", 1, NULL, 'a'},
		{"varfile", 1, NULL, 'f'},
		{"bconffile", 1, NULL, 'b'},
		{NULL}
	};
	const char *bconf_appl = NULL;
	char *varfiles[10];
	size_t nvarfiles = 0;
	char *conffiles[10];
	size_t nconffiles = 0;
	const char *bconf_file = NULL;
	int i;
	struct bpapi ba;
	char opt;
	unsigned n;
	
	while ((opt = getopt_long(argc, argv, "c:b:a:f:", longopts, NULL)) != -1) {
		switch (opt) {
		case 'c':
			if (nconffiles >= sizeof(conffiles) / sizeof(conffiles[0]))
				errx(1, "max number of conffiles reached");
			conffiles[nconffiles++] = optarg;
			break;
		case 'a':
			bconf_appl = optarg;
			break;
		case 'f':
			if (nvarfiles >= sizeof(varfiles) / sizeof(varfiles[0]))
				errx(1, "max number of varfiles reached");
			varfiles[nvarfiles++] = optarg;
			break;
		case 'b':
			bconf_file = optarg;
			break;
		default:
			errx(1, "Usage %s [-c/--configfile <bconf.conf>] [-b/--bconffile <bconf.txt>] [-a/--appl <bconf appl>] [-f/--varfile <varfile>] <var=val...> <template...>", argv[0]);
		}
	}

	for (n = 0 ; n < nconffiles ; n++)
		load_conffile(conffiles[n], &root);

	if (bconf_file) {
		if (load_bconf_file(bconf_appl, &root, bconf_file) == -1)
			errx(1, "load_bconf_file(%s)", bconf_file);
	} else {
		if (bconf_appl) {
			if (!root)
				load_conffile("/opt/blocket/conf/bconf.conf", &root);
			load_bconf(bconf_appl, &root);
		}
	}
#if 0
	/* Let's not default for now and see how things work out. */
	if (!bconf_get(root, "pgsql_session.db_name")) {
		warnx("pgsql_session.db_name not configured, defaulting to 'db'");
		bconf_add_data(&root, "pgsql_session.db_name", "db");
	}
#endif

	filter_state_bconf_init_all(&root);
	default_filter_state = filter_state_bconf;
	BPAPI_INIT_BCONF(&ba, root, stdout, hash);

	for (n = 0 ; n < nvarfiles ; n++)
		parse_varfile(&ba, varfiles[n]);

	for (i = optind ; i < argc ; i++) {
		const char *eq;

		if ((eq = strchr(argv[i], '='))) {
			char *k = strndupa(argv[i], eq - argv[i]);

			bpapi_insert(&ba, k, eq + 1);
		} else {
			if (!call_template(&ba, argv[i]))
				errx(1, "Template not found: %s", argv[i]);
		}
	}

	filter_state_bconf_free(&root);
	flush_template_list();
	bpapi_free(&ba);

	return 0;
}
