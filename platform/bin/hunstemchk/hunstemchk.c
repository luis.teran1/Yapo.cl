/*
	Run the platform version of hunspell against a wordlist, output how words stem.
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <getopt.h>
#include <hunspell.h>
#include "tree.h"
#include "util.h"

static char* hunpath = NULL;
static char* hundict = NULL;
static char* infile = NULL;
static int suggestions = 0;
static int byclass = 0;

struct word_entry {
	char* word;
	struct word_entry* next;
};

struct stem_entry {
	char* stem;
	struct word_entry* words;
	RB_ENTRY(stem_entry) rbe;
};

static __inline  int
stem_entry_compare(const struct stem_entry *a, const struct stem_entry *b) {
	return strcmp(a->stem, b->stem);
}

RB_HEAD(stem_tree, stem_entry) tree;
RB_GENERATE_STATIC(stem_tree, stem_entry, rbe, stem_entry_compare);

static void exit_level(int level) __attribute__ ((noreturn));

static void usage(const char* progname)
{
	printf("%s [--byclass|--suggestions] --hunpath <path> --hundict <name> --infile <file>\n", progname);
	printf("\n");
	printf(" --byclass		Group by stem. Reporting which words map each stem.\n");
}

static void parse_options(int argc, char **argv) {
	int opt;

	while (1) {
		int option_index = 0;
		static struct option long_options[] = {
			{"hunpath", 1, 0, 0},
			{"hundict", 1, 0, 0},
			{"infile", 1, 0, 0},
			{"suggestions", 0, 0, 0},
			{"byclass", 0, 0, 0},
			{"help", 0, 0, 0},
			{0, 0, 0, 0}
		};

		opt = getopt_long(argc, argv, "", long_options, &option_index);
		if (opt == -1)
			break;

		if (opt == 0) {
			switch (option_index) {
				case 0:
					hunpath = strdup(optarg);
					break;
				case 1:
					hundict = strdup(optarg);
					break;
				case 2:
					infile = strdup(optarg);
					break;
				case 3:
					suggestions = 1;
					break;
				case 4:
					byclass = 1;
					break;
				case 5:
					usage(argv[0]);
					exit_level(1);
			}
		}
	}

	if (!hundict)
		hundict = xstrdup("sv_SE");

	if (!hunpath)
		hunpath = xstrdup("../../../share/search");

	if (!infile)
		infile = xstrdup("hunstemchk.txt");
}

static void output_stems(Hunhandle* h, const char* word)
{
	char** result;
	int nstems = Hunspell_stem(h, &result, word);
	int i = 0;

	if (nstems > 0) {
		printf("%s:", word);
		for (i=0 ; i < nstems ; ++i) {
			if (i > 0)
				printf(", ");
			printf("[%d]->'%s'", i, result[i]);
			/*
			if (strcmp(result[i], word) != 0 && strncmp(result[i], word, strlen(result[i])) == 0) {
				const char* left = word + strlen(result[i]);
				if (strlen(left) > 2 && Hunspell_spell(h, left)) {
					printf(", fixup->'%s%s'", result[0], left);
				}
			}
			*/
		}
		printf("\n");
		Hunspell_free_list(h, &result, nstems);
	} else {
			printf("No stems for '%s'\n", word);
	}

}

static void output_suggs(Hunhandle* h, const char* word)
{
	char** result;
	int nsuggs = Hunspell_suggest(h, &result, word);
	int i = 0;

	if (nsuggs > 0) {
		printf("%s:", word);
		for (i=0 ; i < nsuggs ; ++i) {
			if (i > 0)
				printf(", ");
			printf("[%d]->'%s'", i, result[i]);
		}
		printf("\n");
		Hunspell_free_list(h, &result, nsuggs);
	} else {
		printf("No suggestions for %s\n", word);
	}

}

static Hunhandle* open_dictionary(const char* hunpath, const char* hundict)
{
	char* fn_aff;
	char* fn_dic;

	printf("Loading dictionary %s.{aff,dic} from '%s'\n", hundict, hunpath);

	xasprintf(&fn_aff, "%s/%s.aff", hunpath, hundict);
	xasprintf(&fn_dic, "%s/%s.dic", hunpath, hundict);
	Hunhandle* h = Hunspell_create(fn_aff, fn_dic);

	free(fn_aff);
	free(fn_dic);

	return h;
}

static void byclass_build(Hunhandle* h, char* word) {
	char** result;
	int nstems = Hunspell_stem(h, &result, word);
	int i = 0;

	if (nstems > 0 && result[0]) {
		struct stem_entry fi;
		fi.stem = result[i];

		// Find stem in tree
		struct stem_entry* e = RB_FIND(stem_tree, &tree, &fi);
		if (!e) {
			e = malloc(sizeof(struct stem_entry));
			e->stem = __builtin_strdup(result[0]);
			e->words = NULL;
			RB_INSERT(stem_tree, &tree, e);
		}

		// Allocate new word and add it to the list in the stem entry.
		struct word_entry* we = malloc(sizeof(struct word_entry));
		if (!we)
			abort();
		we->word = __builtin_strdup(word);
		we->next = e->words;
		e->words = we;

		Hunspell_free_list(h, &result, nstems);
	}
}

static void byclass_report(void) {
	struct stem_entry* e = NULL;
	RB_FOREACH(e, stem_tree, &tree) {
		printf("%s:\n", e->stem);
		struct word_entry* we = e->words;
		while (we) {
			printf("\t%s\n", we->word);
			we = we->next;
		}
	}
}

static void byclass_free(void) {
	struct stem_entry *e;
	struct stem_entry *next_e;

	for (e = RB_MIN(stem_tree, &tree) ; e != NULL ; e = next_e) {
		next_e = RB_NEXT(stem_tree, &tree, e);
		free(e->stem);
		struct word_entry* we = e->words;
		struct word_entry* we_next = NULL;
		while (we) {
			we_next = we->next;
			free(we->word);
			free(we);
			we = we_next;
		}
		RB_REMOVE(stem_tree, &tree, e);
		free(e);
    }
}

static void exit_level(int level)
{
	if (hunpath)
		free(hunpath);
	if (hundict)
		free(hundict);
	if (infile)
		free(infile);

	exit(level);
}

int main(int argc, char* argv[])
{
	int exitlvl = 0;
	parse_options(argc, argv);

	Hunhandle* h = NULL;

	h = open_dictionary(hunpath, hundict);
	if (!h) {
		printf("Couldn't open dictionary '%s' from '%s'\n", hundict, hunpath);
		exit_level(2);
	}

	FILE* f = fopen(infile, "r");
	if (f) {
		char buf[256];
		char* p = NULL;

		while (!feof(f)) {
			if (!fgets(buf, sizeof(buf), f))
				continue;
			if ((p = strrchr(buf, '\n')) != NULL)
				*p = '\0';
			if (*buf) {
				if (*buf == '#') { /* Pass through comments */
					printf("%s\n", buf);
				} else {
					if (byclass) {
						byclass_build(h, buf);
					} else {
						int spell_passed = Hunspell_spell(h, buf);
						if (!spell_passed)
							printf("spell rejected '%s'\n", buf);

						if (suggestions)
							output_suggs(h, buf);
						else if (spell_passed)
							output_stems(h, buf);
					}
				}
			}
			buf[0] = '\0';
		}
		fclose(f);
 		if (byclass) {
 			byclass_report();
 			byclass_free();
 		}
	} else {
		printf("Couldn't open infile '%s'\n", infile);
 		exitlvl = 1;
	}
	Hunspell_destroy(h);

	exit_level(exitlvl);
}

