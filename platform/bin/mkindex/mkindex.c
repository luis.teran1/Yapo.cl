
#include <ctype.h>
#include <err.h>
#include <stdio.h>
#include <string.h>
#include <limits.h>
#include <unistd.h>

#include "index.h"
#include "logging.h"
#include "charmap.h"
#include "text.h"

static struct index_db *
put_index(struct index_db *db, const char *file, int *numads, int shardsz, const char *db_name, void *cfdata) {
	FILE *f = fopen(file, "r");
	char header[1024];
	char *eoh;
	char line[1024];
	char *eol;
	
	if (!f)
		err(1, "fopen");

	if (!fgets(header, sizeof(header), f))
		err(1, "fgets");
	
	if (!(eoh = strchr(header, '\n')))
		eoh = header + strlen(header);

	while (fgets(line, sizeof(line), f)) {
		char *chdr, *cl;
		char *ech, *ecl;
		struct document *doc;
		struct docpos dpos = { .rel_boost = 1 };

		if (shardsz && *numads && *numads % shardsz == 0) {
			if (close_index(db, 0) != 0)
				errx(1, "error closing index");
			db = index_prime_new(db_name, (*numads / shardsz), 0, 0);
		}
		(*numads)++;

		if (!(eol = strchr(line, '\n')))
			eol = line + strlen(line);

		doc = open_doc(db);

		chdr = header;
		cl = line;
		for (chdr = header, cl = line ; chdr < eoh && cl < eol ; chdr = ech + strspn(ech, "\t\n"), cl = ecl + 1) {
			int ind = AATTR;
			int addwords = 0;
			bool addid = true;

			dpos.flags = 0;

			if (!(ech = strpbrk(chdr, "\t\n")))
				ech = eoh;
			if (!(ecl = strpbrk(cl, "\t\n")))
				ecl = eol;

			if (*chdr == '+') {
				ind |= AIND;
				chdr++;
			}
			if (*chdr == '-') {
				ind &= ~AATTR;
				ind |= AIND; /* didn't have this here first, but made no sense not to */
				chdr++;
			}
			if (*chdr == '!') {
				addwords = 1;
				chdr++;
				if (*chdr == '(') {
					dpos.flags = strtol(chdr + 1, &chdr, 0);
					if (!chdr || *chdr++ != ')')
						errx(1, "Syntax error in posflags");
				}
				ind &= ~AIND;
			}
			if (*chdr == ';') {
				chdr++;
				addid = false;
			}
			if (ecl != cl) {
				doc_add_attr_len(doc, chdr, ech - chdr, cl, ecl - cl, ind);

				if (addid) {
					if (strncmp(chdr, "id", ech - chdr) == 0)
						doc->id.id = atoi(cl);
					else if (strncmp(chdr, "order", ech - chdr) == 0)
						doc->id.order = atoi(cl);
					else if (strncmp(chdr, "suborder", ech - chdr) == 0)
						doc->id.suborder = atoi(cl);
				}

				if (addwords) {
					char text[ecl - cl + 1];
					/* In the future, this should be what's hashed per lang instead of hun_obj. */
					struct add_text_config atc = {
						.min_wordlen = 1,
						.max_wordlen = 100,
						.cfdata = cfdata,
					};

					memcpy(text, cl, ecl - cl);
					text[ecl - cl] = '\0';
					add_text(&atc, doc, text, &dpos);
				}
			}
		}
		if (store_doc(db, doc, 0))
			err(1, "store_doc");
	}
	fclose(f);
	return db;
}

static void
put_conf(struct index_db *db, const char *fname) {
	FILE *f = fopen(fname, "r");
	char line[1024];

	while (fgets(line, sizeof(line), f)) {
		char *key = line;
		char *val = strchr(line, '=');

		*val++ = '\0';
		*strchr(val, '\n') = '\0';

		index_add_conf(db, key, val);
	}
}

int
main(int argc, char *argv[]) {
	struct index_db *db;
	char db_name[PATH_MAX];
	const char *progname;
	extern char *optarg;
	extern int optind;
	int shardsz = 0;
	int numads = 0;
	char ch;
	int i;
	void *cfdata;
	char *conf = NULL;

	progname = argv[0];

	while ((ch = getopt(argc, argv, "cg:s:")) != -1) {
		switch (ch) {
		case 's':
			shardsz = atoi(optarg);
			break;
		case 'g':
			conf = strdup(optarg);
			break;
		default:
			errx(1, "Usage: %s [-s <shard size>] [-g <conf>] <indexfile> <sourcefile...>", progname);
		}
	}
	argc -= optind;
	argv += optind;

	if (argc < 3)
		errx(1, "Usage: %s [-b] [-c] [-s <shard size>] [-g <conf>] <indexfile> <charmap> <sourcefile...>", progname);

	log_setup("mkindex", "debug");

	snprintf(db_name, sizeof(db_name), "%s%s", argv[0], shardsz ? "0" : "");

	if ((db = open_index(db_name, INDEX_WRITER)) == NULL)
		errx(1, "open_index");

        index_set_to_id(db, "4711");

	index_add_oper(db, "en", "AND", IOP_AND);
	index_add_oper(db, "en", "and", IOP_AND);
	index_add_oper(db, "en", "OR", IOP_OR);
	index_add_oper(db, "en", "or", IOP_OR);
	index_add_oper(db, "en", "NOT", IOP_NOT);
	index_add_oper(db, "en", "not", IOP_NOT);

	add_stopword(db, "ONE");
	add_stopword(db, "one");

	const char *charmap = argv[1];
	struct charmap *cm = init_charmap(charmap);
	cfdata = init_charmap_thread(cm);

	if (conf) {
		put_conf(db, conf);
	}

	for (i = 2 ; i < argc ; i++) {
		db = put_index(db, argv[i], &numads, shardsz, argv[0], cfdata);
#if 0
		flush_db(db);
#endif
	}
	close_index(db, 0);
	return 0;
}
