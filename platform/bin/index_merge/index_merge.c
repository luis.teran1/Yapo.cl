#include <sys/types.h>
#include <sys/stat.h>
#include <sys/time.h>

#include <stdio.h>
#include <unistd.h>
#include <err.h>
#include <string.h>
#include <getopt.h>
#include <dirent.h>
#include <limits.h>
#include <stdint.h>
#include <assert.h>

#include "strl.h"
#include "index.h"
#include "util.h"
#include "timer.h"
#include "logging.h"
#include "bconf.h"
#include "index_merge.h"

static void
timer_dump(struct timer_class *tc, void *v) {
	struct timespec avgts;
	double avg = (double)tc->tc_total.tv_sec + (double)tc->tc_total.tv_nsec / 1000000000.0;

	if (tc->tc_count == 0)
		return;

	avg /= (double)tc->tc_count;

	avgts.tv_sec = avg;
	avgts.tv_nsec = (avg - (double)avgts.tv_sec) * 1000000000.0;

	log_printf(LOG_INFO, "Timer: %s.count:%lld", tc->tc_name, tc->tc_count);
	log_printf(LOG_INFO, "Timer: %s.total:%d.%03ld", tc->tc_name, (int)tc->tc_total.tv_sec, tc->tc_total.tv_nsec / 1000000);
	if (tc->tc_count > 1) {
		log_printf(LOG_INFO, "Timer: %s.min:%d.%03ld", tc->tc_name, (int)tc->tc_min.tv_sec, tc->tc_min.tv_nsec / 1000000);
		log_printf(LOG_INFO, "Timer: %s.max:%d.%03ld", tc->tc_name, (int)tc->tc_max.tv_sec, tc->tc_max.tv_nsec / 1000000);
		log_printf(LOG_INFO, "Timer: %s.average:%d.%03ld", tc->tc_name, (int)avgts.tv_sec, avgts.tv_nsec / 1000000);
	}
}

static int
delta_filter(const struct dirent *ent) {
	return !strncmp(ent->d_name, "delta_", 6);
}

static int
create_deltamerge_index(const char *dbname) {
	struct index_db *odb;
	struct index_db *db;
	struct dirent **entries = NULL;
	char *delta_name;
	int ret = 0;
	int res = 0;
	int l;
   
	if ((res = scandir(dbname, &entries, delta_filter, versionsort)) < 0) {
		log_printf(LOG_CRIT, "scandir error: %m");
		return 1;
	}

	if (!res) {
		log_printf(LOG_INFO, "scandir returned no match");
		return 1;
	}

	ALLOCA_PRINTF(l, delta_name, "%s/%s", dbname, entries[0]->d_name);
	while (res > 0)
		free (entries[--res]);
	free (entries);
	
	if ((odb = open_index(delta_name, INDEX_READER)) == NULL) {
		log_printf(LOG_CRIT, "Couldn't open %s", delta_name);
		return 1;
	}
	
	if ((db = open_index(dbname, INDEX_WRITER)) == NULL) { 
		log_printf(LOG_CRIT, "Couldn't open %s", delta_name);
		return 1;
	}

	bconf_merge_prefix(&db->meta, "attr", bconf_get(odb->meta, "attr"));
	
	index_set_to_id(db, index_get_from_id(odb));
	close_index(odb, 0);
	close_index(db, 0);
	
	return ret;
}

static void
usage(const char *prog_name) {
	errx(1, "Usage: %s [-l <loglevel>] [-u] <from_base> <to>", prog_name);
}

int
main(int argc, char **argv) {
	struct timer_instance *all_ti, *ti;
	struct index_merge_state ind = {0};
	const char *prog_name;
	const char *loglevel = "info";
	int ret = 1;
	int ch;
	int i;

	all_ti = timer_start(NULL, "merge");

	prog_name = argv[0];
	while ((ch = getopt(argc, argv, "ucl:")) != -1) {
		switch (ch) {
		case 'u':
			ind.update = 1;
			break;
		case 'c':
			ind.create = 1;
			break;
		case 'l':
			loglevel = strdup(optarg);
			break;
		default:
			usage(prog_name);
		}
	}
	argc -= optind;
	argv += optind;

	log_setup("index_merge", loglevel);

	if (argc != 2)
		usage(prog_name);


	log_printf(LOG_INFO, "Start %s", argv[0]);

	ti = timer_start(all_ti, "open");

	ind.nfrom = sizeof(ind.from_db) / sizeof(ind.from_db[0]);

	if (ind.create == 1) {
		if (create_deltamerge_index(argv[0])) {
			exit(1);
		}
	}

	log_printf(LOG_INFO, "Opening index %s", argv[0]);
	if (open_indexes(argv[0], TUNE_SEARCH, ind.update, ind.from_db, &ind.nfrom) != 0) {
		errx(1, "failed opening indexes %d", ind.nfrom);
	}

	/* Open new index */
	ind.to = open_index(argv[1], INDEX_WRITER);
	if (!ind.to)
		errx(1, "failed to open destination %s", argv[1]);

	if (index_perform_merge(&ind, ti) == -1) {
		ret = 1;
	} else {
		ret = 0;
	}

	log_printf(LOG_INFO, "Closing");
	timer_handover(ti, "close");
	for (i = 0; i < ind.nfrom; i++)
		close_index(ind.from_db[i], 0);
	close_index(ind.to, INDEX_CLOSE_SKIP_SORT);

	timer_end(all_ti, NULL);
	timer_foreach(timer_dump, NULL);

	return ret;
}
