#!/bin/bash

# See docs/docker.md

if [ "$1" = "--cmd" ]; then
	# E.g. ./docker.sh --cmd make -C regress/foo r
	shift
	DOCKER_CMD="env REGRESS_FAST_DIR=${REGRESS_FAST_DIR:-/tmp} NO_DOCKER=1 ./compile.sh && $*"
fi
if [ "$1" == "--shell" ] || [ "$1" == "-s" ]; then
	shift
	DOCKER_DONT_RUN=1
	DOCKER_RUN_FLAGS=-i
	source docker.sh
	exec $DOCKER_RUN bash -c "$setup cd /platform ; exec env REGRESS_FAST_DIR=${REGRESS_FAST_DIR:-/tmp} NO_DOCKER=1 HOME=/platform su -s ${SHELL:-/bin/bash} -m tester -- -l"
fi
if [ -z "$DOCKER_CMD" ]; then
	DOCKER_CMD="env RUNTESTS=${RUNTESTS} REGRESS_FAST_DIR=${REGRESS_FAST_DIR:-/tmp} NO_DOCKER=1 ./compile.sh $*"
fi

test -z "$DOCKER_IMAGE" && DOCKER_IMAGE="platform-centos6"

dockerfile() {
	local files="$(echo $DOCKER_IMAGE | sed -e 's/^platform-\([^-]*\).*/\1/')"

	echo "From $DOCKER_IMAGE"
	cat DOCKER.*."$files" 2>/dev/null
}

errfile="$(mktemp)"
CACHED_IMAGE="$(dockerfile | docker build - | tee "$errfile" | sed -n -e 's/Successfully built //p')"

if [ -z "$CACHED_IMAGE" ]; then
	echo -e "\x1b[31mFailed to build image" >&2
	cat "$errfile" >&2
	rm -f "$errfile"
	exit 1
fi

rm -f "$errfile"

if [ -t 1 ]; then
	TTY_FLAG=-t
else
	TTY_FLAG=
fi

# Used for setup by scripts/docker/verify_packages.sh

setup="
	set -e

	groupadd -o -g $(stat -c %g .) tester
	useradd -o -u $(stat -c %u .) -g $(stat -c %g .) --home-dir /platform tester
"

runtests="
	su -l tester <<TEST
		# XXX we should probably support LANG=POSIX
		LANG=en_US.UTF-8
		export LANG

		cd /platform
		$DOCKER_CMD
TEST
"

DOCKER_RUN="docker run $DOCKER_FLAGS -v $(pwd):/platform -v /home/coord:/home/coord $DOCKER_RUN_FLAGS $TTY_FLAG -u root --rm $CACHED_IMAGE"

test -n "$DOCKER_DONT_RUN" && return

exec $DOCKER_RUN bash -c "
	$setup
	$runtests
"
