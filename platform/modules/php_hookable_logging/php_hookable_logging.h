#ifndef PHP_HOOKABLE_LOGGING_H
#define PHP_HOOKABLE_LOGGING_H 1

#define PHP_HOOKABLE_LOGGING_WORLD_VERSION "1.0"
#define PHP_HOOKABLE_LOGGING_WORLD_EXTNAME "hookable_logging"

int hookable_logging_mstartup(INIT_FUNC_ARGS);

PHP_FUNCTION(hookable_openlog);
PHP_FUNCTION(hookable_syslog);
PHP_FUNCTION(hookable_closelog);

extern zend_module_entry hookable_logging_module_entry;
#define phpext_hookable_logging_ptr &hookable_logging_module_entry

#endif
