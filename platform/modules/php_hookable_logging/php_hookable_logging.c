#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include <syslog.h>
#include <dlfcn.h>

#include <SAPI.h>
#include <php.h>
#include <php_ini.h>
#include "php_hookable_logging.h"
#include "platform_php.h"
#include "macros.h"

static zend_function_entry hookable_logging_functions[] = {
	PHP_FE(hookable_openlog, NULL)
	PHP_FE(hookable_syslog, NULL)
	PHP_FE(hookable_closelog, NULL)
	{NULL, NULL, NULL}
};

static zend_function_entry replace_logging_functions[] = {
	PHP_FE(hookable_openlog, NULL)
	PHP_FE(hookable_syslog, NULL)
	PHP_FE(hookable_closelog, NULL)
	{NULL, NULL, NULL}
};

zend_module_entry hookable_logging_module_entry EXPORTED = {
	STANDARD_MODULE_HEADER,
	PHP_HOOKABLE_LOGGING_WORLD_EXTNAME,
	hookable_logging_functions,
	hookable_logging_mstartup,
	NULL,
	NULL,
	NULL,
	NULL,
	PHP_HOOKABLE_LOGGING_WORLD_VERSION,
	STANDARD_MODULE_PROPERTIES
};

#ifdef COMPILE_DL_HOOKABLE_LOGGING
#undef ZEND_DLEXPORT
#define ZEND_DLEXPORT EXPORTED
ZEND_GET_MODULE_PROTOTYPE(hookable_logging)
ZEND_GET_MODULE(hookable_logging)
#endif

PHP_INI_BEGIN()
PHP_INI_ENTRY("replace_syslog",  "1", PHP_INI_ALL, NULL)
PHP_INI_END()

PHP_FUNCTION(hookable_openlog) {

	char*	ident;
	long	ident_len = 0;
	long	option;
	long	facility;

	if (ZEND_NUM_ARGS() != 3) {
		WRONG_PARAM_COUNT;
	}

	if (zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC,
					  "sll",
					  &ident, &ident_len, &option, &facility) == FAILURE) {
		RETURN_NULL();
	}

	void (*hook_openlog)(const char *ident, int option, int facility) = dlsym(RTLD_DEFAULT, "sysloghook_openlog");
	if (hook_openlog) {
		hook_openlog(ident, option, facility);
	} else {
		openlog(ident, option, facility);
	}

	RETURN_TRUE;
}

PHP_FUNCTION(hookable_syslog) {

	long	facility;
	char*	logstring = NULL;
	long	logstring_len = 0;

	if (ZEND_NUM_ARGS() != 2) {
		WRONG_PARAM_COUNT;
	}

	if (zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC,
					  "ls",
					  &facility, &logstring, &logstring_len) == FAILURE) {
		RETURN_NULL();
	}

	/* log_printf(facility, logstring); */
	void (*hook_syslog)(int priority, const char *format, ...) = dlsym(RTLD_DEFAULT, "sysloghook_syslog");
	if (hook_syslog) {
		hook_syslog(facility, "%s", logstring);
	} else {
		syslog(facility, "%s", logstring);
	}

	RETURN_TRUE;
}

PHP_FUNCTION(hookable_closelog) {

	if (ZEND_NUM_ARGS() != 0) {
		WRONG_PARAM_COUNT;
	}

	void (*hook_closelog)(void) = dlsym(RTLD_DEFAULT, "sysloghook_closelog");
	if (hook_closelog) {
		hook_closelog();
	} else {
		closelog();
	}

	RETURN_TRUE;
}

int 
hookable_logging_mstartup(INIT_FUNC_ARGS) {
	REGISTER_INI_ENTRIES();

	if (INI_INT("replace_syslog")) {
		replace_logging_functions[0].fname = "openlog";
		replace_logging_functions[1].fname = "syslog";
		replace_logging_functions[2].fname = "closelog";
		zend_unregister_functions(replace_logging_functions, -1, NULL);
		zend_register_functions(NULL, replace_logging_functions, NULL, type);
	}
	return SUCCESS;
}

