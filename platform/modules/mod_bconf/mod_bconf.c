
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <time.h>
#include <ctype.h>

#include <httpd.h>
#include <http_log.h>
#include <http_config.h>
#include <http_protocol.h>
#include <http_request.h>
#include <apr_strings.h>
#include <malloc.h>

#include "transapi.h"
#include "bconfig.h"
#include "bconf.h"
#include "bconfd_client.h"
#include "vtree.h"
#include "logging.h"
#include "mempool.h"
#include "filter_state.h"

#include "mod_bconf_extern.h"

extern module AP_MODULE_DECLARE_DATA bconf_module;

static void *
create_srv_config(apr_pool_t *pool, server_rec *s) {
	return apr_pcalloc(pool, sizeof(struct mod_bconf_cfg));
}

static apr_status_t
cleanup_srv_config(void *v) {
	struct mod_bconf_cfg *cfg = v;
#if 0
	if (cfg->root)
		bconf_free(&cfg->root);
#endif
	mempool_free(cfg->mempool);
	if (cfg->filter) {
		filter_state_bconf_free(&cfg->filter);
	}
	return APR_SUCCESS;
}

static int
add_bconf_data(const char *key, const char *value, void *cbdata) {
	struct mod_bconf_cfg *cfg = cbdata;

	bconf_add_data_pool(cfg->mempool, &cfg->root, key, value);
	return 0;
}

static const char *
command_set_config_file(cmd_parms *cmd, void *mconfig, const char *config_file, const char *application) {
	struct bconf_node *bconfd_conf;
	struct mod_bconf_cfg *cfg;
	struct bconfd_call call = { 0 };
	const char *blocket_id;
	int res;

	cfg = ap_get_module_config(cmd->server->module_config, &bconf_module);

	if (cfg->root)
		return "Bconf already loaded";

	cfg->mempool = mempool_create(2 * 1024 * 1024);

	if ((bconfd_conf = config_init(config_file)) == NULL) {
		ap_log_error(APLOG_MARK, APLOG_CRIT, OK, NULL, "Failed to read the configuration file (%s)", config_file);
		return "Failed to read bconfd config";
	}

	if ((blocket_id = bconf_get_string(bconfd_conf, "blocket_id")) == NULL) {
		ap_log_error(APLOG_MARK, APLOG_CRIT, OK, NULL, "No blocket_id set in the configuration file (%s)", config_file);
		bconf_free(&bconfd_conf);
		return "blocket_id missing";
	}

	bconfd_add_request_param(&call, "host", blocket_id);
	bconfd_add_request_param(&call, "appl", application ?: "mod_blocket");
	if ((res = bconfd_load(&call, bconfd_conf)) != 0) {
		ap_log_error(APLOG_MARK, APLOG_CRIT, OK, NULL, "Failed to get bconf from bconfd server (%d):%s", res, call.errstr);
		bconf_free(&bconfd_conf);
		bconf_free(&call.bconf);
		return "Failed to get bconf from bconfd";
	}

	bconf_foreach(call.bconf, INT_MAX, add_bconf_data, cfg);

	ap_log_error(APLOG_MARK, APLOG_WARNING, OK, NULL, "Received bconf - file: %s checksum %s id: %s -> %d entries",
				config_file,
				call.checksum,
				blocket_id,
				bconf_count(cfg->root));

	bconf_free(&bconfd_conf);
	mempool_finalize(cfg->mempool);

	if (!cfg->root)
		return "Got empty bconf from bconfd";

	filter_state_bconf_init(&cfg->filter, FS_GLOBAL);

	apr_pool_cleanup_register(cmd->pool, cfg, cleanup_srv_config, apr_pool_cleanup_null);

        return NULL;
}

static const command_rec commands[] = {
        AP_INIT_TAKE12("BlocketConfig", command_set_config_file, NULL, RSRC_CONF, "Base configuration file"),
        {NULL}
};

module AP_MODULE_DECLARE_DATA bconf_module EXPORTED = {
	STANDARD20_MODULE_STUFF,
	NULL,
	NULL,
	create_srv_config,
	NULL,
	commands,
	NULL
};
