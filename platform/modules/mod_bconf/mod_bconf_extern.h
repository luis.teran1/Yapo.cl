struct bconf_node;
struct mempool;
struct mod_bconf_cfg {
	struct mempool *mempool;
	struct bconf_node *root;
	struct bconf_node *filter;
};

static inline struct bconf_node *
mod_bconf_get_root(server_rec *s) {
	struct mod_bconf_cfg *mbc;
	module *mod;

	if ((mod = ap_find_linked_module("mod_bconf.c")) == NULL) {
		return NULL;
	}
	if ((mbc = ap_get_module_config(s->module_config, mod)) == NULL) {
		return NULL;
	}
	return mbc->root;
}

static inline struct bconf_node **
mod_bconf_get_filterp(server_rec *s) {
	struct mod_bconf_cfg *mbc;
	module *mod;

	if ((mod = ap_find_linked_module("mod_bconf.c")) == NULL) {
		return NULL;
	}
	if ((mbc = ap_get_module_config(s->module_config, mod)) == NULL) {
		return NULL;
	}
	return &mbc->filter;
}
