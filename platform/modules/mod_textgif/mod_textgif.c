#include <httpd.h>
#include <http_log.h>
#include <http_config.h>
#include <http_protocol.h>
#include <http_request.h>
#include <apr_strings.h>

#include <inttypes.h>

#include <aes.h>
#include <bconf.h>
#include <strl.h>

#include <ft2build.h>
#include FT_FREETYPE_H

#include "mod_bconf_extern.h"

extern module AP_MODULE_DECLARE_DATA textgif_module EXPORTED;

struct textgif_conf {
	const char *textgif_key;
	int textgif_key_len;
	struct pgc {
		int left;
		int top;
		int width;
		int advance;
		int rows;
		unsigned char *bitmap;
	} textgif_char[256], textgif_char_small[256];
	const uint8_t *pre_hdr;
	unsigned int pre_hdr_len;
	unsigned int pre_hdr_w_off1;
	unsigned int pre_hdr_w_off2;
	unsigned int pre_hdr_h_off1;
	unsigned int pre_hdr_h_off2;	
};

static void write_gif_from_buf(struct textgif_conf *conf, request_rec *r, uint8_t *buf, int w, int h, int rowstride);
static int prebuild_gif_hdr(apr_pool_t *pool, struct textgif_conf *, struct bconf_node *);
static int setup_textgif(apr_pool_t *pool, struct textgif_conf *, struct bconf_node *);
static int textgif_handler(request_rec *r);


static void *
config_server_create(apr_pool_t *pool, server_rec *s) {
	return apr_pcalloc(pool, sizeof(struct textgif_conf));
}

static int
post_config(apr_pool_t *pool, apr_pool_t *plog, apr_pool_t *ptemp, server_rec *s) {
	struct textgif_conf *conf;
	struct bconf_node *br;
	int res;

	for ( ; s != NULL; s = s->next) {
		if ((br = mod_bconf_get_root(s)) == NULL) {
			ap_log_error(APLOG_MARK, APLOG_WARNING, OK, NULL, "mod_textgif - Failed to get bconf root.");
			return HTTP_INTERNAL_SERVER_ERROR;
		}
		conf = ap_get_module_config(s->module_config, &textgif_module);
		res = setup_textgif(pool, conf, br);
		if (res != OK)
			return res;
	}

	return OK;
}

static void
register_hooks(apr_pool_t *p) {
	ap_hook_handler(textgif_handler, NULL, NULL, APR_HOOK_MIDDLE);
	ap_hook_post_config(post_config, NULL, NULL, APR_HOOK_MIDDLE);
}

module AP_MODULE_DECLARE_DATA textgif_module EXPORTED = {
	STANDARD20_MODULE_STUFF,
	NULL,
	NULL,
	config_server_create,
	NULL,
	NULL,
	register_hooks
};

static int
setup_textgif(apr_pool_t *pool, struct textgif_conf *conf, struct bconf_node *bconf_root) {
	FT_Library library;
	FT_Face face;
	FT_GlyphSlot slot;
	FT_Face face_small;
	FT_GlyphSlot slot_small;
	const char *fontfile;
	int size;
	int size_small;
	int i;

	conf->textgif_key = bconf_get_string(bconf_root, "common.textgif.key");
	if (conf->textgif_key == NULL) {
		ap_log_error(APLOG_MARK, APLOG_ERR, OK, NULL, "textgif - no key found");
		return HTTP_INTERNAL_SERVER_ERROR;
	}
	conf->textgif_key_len = strlen(conf->textgif_key);
	fontfile = bconf_get_string(bconf_root, "common.textgif.font");
	size = bconf_get_int(bconf_root, "common.textgif.size");
	if (size < 1) {
		ap_log_error(APLOG_MARK, APLOG_ERR, OK, NULL, "failed to get size");
		return HTTP_INTERNAL_SERVER_ERROR;
	}
	size_small = bconf_get_int(bconf_root, "common.textgif.size_small");

	/* Init normal faces */
	if (FT_Init_FreeType(&library) ||
	    FT_New_Face(library, fontfile, 0, &face) ||
	    FT_Set_Char_Size(face, 0, size * 64, 72, 72)) {
		ap_log_error(APLOG_MARK, APLOG_ERR, OK, NULL, "Failed to initialize FreeType font \"%s\" for textgif", fontfile);
		return HTTP_INTERNAL_SERVER_ERROR;
	}


	/* Init small faces */
	if (FT_New_Face(library, fontfile, 0, &face_small) ||
	    FT_Set_Char_Size(face_small, 0, size_small * 64, 72, 72)) {
		ap_log_error(APLOG_MARK, APLOG_ERR, OK, NULL, "Failed to initialize FreeType for textgif (small)");
		return HTTP_INTERNAL_SERVER_ERROR;
	}

	slot = face->glyph;
	slot_small = face_small->glyph;

	/* Generate normal characters */
	for (i = 0; i < 256; i++) {
		FT_Bitmap *bitmap;
		struct pgc *pgc;

		if (FT_Load_Char(face, i, FT_LOAD_RENDER)) {
			conf->textgif_char[i].bitmap = NULL;
			continue;
		}
		bitmap = &slot->bitmap;
		pgc = &conf->textgif_char[i];
		pgc->left = slot->bitmap_left;
		pgc->top = slot->bitmap_top;
		pgc->width = bitmap->width;
		pgc->rows = bitmap->rows;
		pgc->advance = slot->advance.x / 64;
		pgc->bitmap = apr_pcalloc(pool, bitmap->rows * bitmap->width);
		memcpy(pgc->bitmap, bitmap->buffer, bitmap->rows * bitmap->width);
	}

	/* Generate small kind characters */
	for (i = 0; i < 256; i++) {
		FT_Bitmap *bitmap;
		struct pgc *pgc;

		if (FT_Load_Char(face_small, i, FT_LOAD_RENDER)) {
			conf->textgif_char_small[i].bitmap = NULL;
			continue;
		}
		bitmap = &slot_small->bitmap;
		pgc = &conf->textgif_char_small[i];
		pgc->left = slot_small->bitmap_left;
		pgc->top = slot_small->bitmap_top;
		pgc->width = bitmap->width;
		pgc->rows = bitmap->rows;
		pgc->advance = slot_small->advance.x / 64;
		pgc->bitmap = apr_pcalloc(pool, bitmap->rows * bitmap->width);
		memcpy(pgc->bitmap, bitmap->buffer, bitmap->rows * bitmap->width);
	}

	FT_Done_Face(face);
	FT_Done_Face(face_small);
	FT_Done_FreeType(library);
	return prebuild_gif_hdr(pool, conf, bconf_root);
}

static int
textgif_handler(request_rec *r) {
	unsigned char textstring[256], *ptr;
	struct textgif_conf *conf;
	unsigned char *buf;
	int w, h, x, maxy = 0, miny = 50, hoff;
	size_t textstringlen;
	unsigned int i;

	if (strcmp(r->handler, "textgif-handler"))
		return DECLINED;

	conf = ap_get_module_config(r->server->module_config, &textgif_module);

	/* Look for magic indicator of requested size */
	unsigned short small = r->uri[4] == '1' ? 1 : 0;

	ptr = (unsigned char *)strstr(r->uri + 5, ".gif");

	w = sizeof(textstring);
	if (ptr == NULL || aes_decode_buf(r->uri + 5, ptr - (unsigned char *)(r->uri + 5), conf->textgif_key, conf->textgif_key_len, textstring, &w) == NULL) {
		memcpy(textstring, "-", 2);
	}
	textstringlen = strlen((char *)textstring);

	w = 0;
	for (i = 0; i < textstringlen; i++) {
		struct pgc *pgc;
		if (small)
			pgc = &conf->textgif_char_small[textstring[i]];
		else
			pgc = &conf->textgif_char[textstring[i]];

		if (pgc->bitmap == NULL)
			continue;

		w += pgc->bitmap ? pgc->advance : 0;

		if (miny > -pgc->top)
			miny = -pgc->top;
		if (maxy < -pgc->top + pgc->rows)
			maxy = -pgc->top + pgc->rows;
	}
	h = maxy - miny + 1;
	hoff = -miny;
	buf = alloca(h * w);
	memset(buf, 0, h * w);

	x = 0;
	for (i = 0; i < textstringlen; i++) {
		int y, bx;
		struct pgc *pgc;
		if (small)
			pgc = &conf->textgif_char_small[textstring[i]];
		else
			pgc = &conf->textgif_char[textstring[i]];


		if (pgc->bitmap == NULL)
			continue;

		bx = x + pgc->left;
		for (y = 0; y < pgc->rows; y++) {
			int by = y + hoff - pgc->top;
			memcpy(&buf[by * w + bx], &pgc->bitmap[y * pgc->width], pgc->width);
		}
		x += pgc->advance;
	}

	ap_set_content_type(r, "image/gif");
	write_gif_from_buf(conf, r, buf, w, h, w);

	return OK;
}

/* Code based on gd_gif_out.c from libgd.
** Code drawn from ppmtogif.c, from the pbmplus package
**
** Based on GIFENCOD by David Rowley <mgardi@watdscu.waterloo.edu>. A
** Lempel-Zim compression based on "compress".
**
** Modified by Marcel Wijkstra <wijkstra@fwi.uva.nl>
**
** Copyright (C) 1989 by Jef Poskanzer.
**
** Permission to use, copy, modify, and distribute this software and its
** documentation for any purpose and without fee is hereby granted, provided
** that the above copyright notice appear in all copies and that both that
** copyright notice and this permission notice appear in supporting
** documentation.  This software is provided "as is" without express or
** implied warranty.
**
** The Graphics Interchange Format(c) is the Copyright property of
** CompuServe Incorporated.  GIF(sm) is a Service Mark property of
** CompuServe Incorporated.
*/
#include <endian.h>
#ifndef htole16
#if __BYTE_ORDER == __LITTLE_ENDIAN
#define htole16(a) (a)
#define htole32(a) (a)
#else
#include <byteswap.h>
#define htole16(a) bswap_16(a)
#define htole32(a) bswap_32(a)
#endif
#endif

#define HSIZE  5003            /* 80% occupancy */
#define GIFBITS 12

struct gifctx {
	request_rec *r;

        int n_bits;                        /* number of bits/code */
        int maxcode;                  /* maximum code, given n_bits */
        int htab[HSIZE];
        unsigned short codetab[HSIZE];
	int free_ent;                  /* first unused entry */
	/*
	 * block compression parameters -- after all codes are used up,
	 * and compression rate changes, start over.
	 */
	int clear_flg;

	int g_init_bits;

	int ClearCode;
	int EOFCode;
	unsigned long cur_accum;
	int cur_bits;
        /*
         * Number of characters so far in this 'packet'
         */
        int a_count;
        /*
         * Define the storage for the packet accumulator
         */
        char accum[256];
};


#define MAXCODE(n_bits)        ((1 << (n_bits)) - 1)
/* should NEVER generate this code */
#define maxmaxcode (1 << GIFBITS)

/*
 * compress stdin to stdout
 *
 * Algorithm:  use open addressing double hashing (no chaining) on the
 * prefix code / next character combination.  We do a variant of Knuth's
 * algorithm D (vol. 3, sec. 6.4) along with G. Knott's relatively-prime
 * secondary probe.  Here, the modular division first probe is gives way
 * to a faster exclusive-or manipulation.  Also do block compression with
 * an adaptive reset, whereby the code table is cleared when the compression
 * ratio decreases, but after the table fills.  The variable-length output
 * codes are re-sized at this point, and a special CLEAR code is generated
 * for the decompressor.  Late addition:  construct the table according to
 * file size for noticeable speed improvement on small files.  Please direct
 * questions about this implementation to ames!jaw.
 */

static unsigned long masks[] = { 0x0000, 0x0001, 0x0003, 0x0007, 0x000F,
                                  0x001F, 0x003F, 0x007F, 0x00FF,
                                  0x01FF, 0x03FF, 0x07FF, 0x0FFF,
                                  0x1FFF, 0x3FFF, 0x7FFF, 0xFFFF };


/*
 * Flush the packet to disk, and reset the accumulator
 */
static void
flush_char(struct gifctx *ctx)
{
        if (ctx->a_count > 0) {
		ap_rputc(ctx->a_count, ctx->r);
		ap_rwrite(ctx->accum, ctx->a_count, ctx->r);
                ctx->a_count = 0;
        }
}

/*
 * Add a character to the end of the current packet, and if it is 254
 * characters, flush the packet to disk.
 */
static void
char_out(int c, struct gifctx *ctx)
{
        ctx->accum[ctx->a_count++] = c;
        if (ctx->a_count >= 254)
                flush_char(ctx);
}

static void
output(int code, struct gifctx *ctx)
{
	ctx->cur_accum &= masks[ctx->cur_bits];

	if (ctx->cur_bits > 0)
		ctx->cur_accum |= ((long)code << ctx->cur_bits);
	else
		ctx->cur_accum = code;

	ctx->cur_bits += ctx->n_bits;

	while (ctx->cur_bits >= 8) {
		char_out((unsigned int)(ctx->cur_accum & 0xff), ctx);
		ctx->cur_accum >>= 8;
		ctx->cur_bits -= 8;
	}

	/*
	 * If the next entry is going to be too big for the code size,
	 * then increase it, if possible.
	 */
	if (ctx->free_ent > ctx->maxcode || ctx->clear_flg) {
		if (ctx->clear_flg) {
			ctx->maxcode = MAXCODE(ctx->n_bits = ctx->g_init_bits);
			ctx->clear_flg = 0;
		} else {
			++(ctx->n_bits);
			if (ctx->n_bits == GIFBITS)
				ctx->maxcode = maxmaxcode;
			else
				ctx->maxcode = MAXCODE(ctx->n_bits);
		}
        }

	if (code == ctx->EOFCode) {
		/*
		 * At EOF, write the rest of the buffer.
		 */
		while (ctx->cur_bits > 0) {
			char_out((unsigned int)(ctx->cur_accum & 0xff), ctx);
			ctx->cur_accum >>= 8;
			ctx->cur_bits -= 8;
		}

		flush_char(ctx);
	}
}

static void
cl_block(struct gifctx *ctx)             /* table clear for block compress */
{
	int i;

	for (i = 0; i < HSIZE; i++)
		ctx->htab[i] = -1;
        ctx->free_ent = ctx->ClearCode + 2;
        ctx->clear_flg = 1;

        output(ctx->ClearCode, ctx);
}

static void
compress(int init_bits, request_rec *r, uint8_t *bitmap, int w, int h, int rowstride)
{
	int fcode;
	int i;
	int ent;
	int disp;
	int hshift;
	struct gifctx ctxs, *ctx = &ctxs;
	int offset;

	memset(ctx, 0, sizeof(*ctx));

	/*
	 * Set up the globals:  g_init_bits - initial number of bits
	 */
	ctx->g_init_bits = init_bits;
	ctx->r = r;

	/*
	 * Set up the necessary values
	 */
	ctx->clear_flg = 0;
	ctx->maxcode = MAXCODE(ctx->n_bits = ctx->g_init_bits);

	ctx->ClearCode = (1 << (init_bits - 1));
	ctx->EOFCode = ctx->ClearCode + 1;
	ctx->free_ent = ctx->ClearCode + 2;

	ctx->a_count = 0;

	offset = 0;
	ent = bitmap[offset++];

	hshift = 0;
	for (fcode = HSIZE; fcode < 65536; fcode *= 2)
		++hshift;
	hshift = 8 - hshift;                /* set hash code range bound */

	/* clear hash table */
	for (i = 0; i < HSIZE; i++)
		ctx->htab[i] = -1;

	output(ctx->ClearCode, ctx);

	for (; offset < h * w; offset++) {
		int c = bitmap[offset % w + (offset / w) * rowstride];

		fcode = ((c << GIFBITS) + ent);
		i = ((c << hshift) ^ ent);    /* xor hashing */

		if (ctx->htab[i] == fcode) {
			ent = ctx->codetab[i];
			continue;
		} else if (ctx->htab[i] < 0) {     /* empty slot */
			goto nomatch;
		}
		disp = HSIZE - i;           /* secondary hash (after G. Knott) */
		if (i == 0)
			disp = 1;
probe:
		if ((i -= disp) < 0)
			i += HSIZE;

		if (ctx->htab[i] == fcode) {
			ent = ctx->codetab[i];
			continue;
		}
		if (ctx->htab[i] > 0)
			goto probe;
nomatch:
		output(ent, ctx);
		ent = c;
		if (ctx->free_ent < maxmaxcode) {
			ctx->codetab[i] = ctx->free_ent++; /* code -> hashtable */
			ctx->htab[i] = fcode;
		} else {
			cl_block(ctx);
		}
	}

	/*
	 * Put out the final code.
	 */
	output(ent, ctx);
	output(ctx->EOFCode, ctx);
}

static int
prebuild_gif_hdr(apr_pool_t *pool, struct textgif_conf *conf, struct bconf_node *bconf_root) {
	uint8_t hdrbuf[4096], *hdr;
	int i;

	int fg_r = bconf_get_int_default(bconf_root, "common.textgif.fg.r", 0);
	int fg_g = bconf_get_int_default(bconf_root, "common.textgif.fg.g", 0);
	int fg_b = bconf_get_int_default(bconf_root, "common.textgif.fg.b", 0);

	int bg_r = bconf_get_int_default(bconf_root, "common.textgif.bg.r", 0xff);
	int bg_g = bconf_get_int_default(bconf_root, "common.textgif.bg.g", 0xff);
	int bg_b = bconf_get_int_default(bconf_root, "common.textgif.bg.b", 0xeb);

	hdr = hdrbuf;
	memcpy(hdr, "GIF89a", 6);
	hdr += 6;

	/* Logical Screen Descriptor */
	conf->pre_hdr_w_off1 = hdr - hdrbuf;
	hdr += 2;
	conf->pre_hdr_h_off1 = hdr - hdrbuf;
	hdr += 2;

	*hdr++ = 0x80 | (7 << 5) | 7; /* global flags = 1 110 0 111 (0xe7) */
	*hdr++ = 0;	/* background color index */
	*hdr++ = 0;

	float slope[3];
	slope[0] = (float)(fg_r - bg_r) / 255;
	slope[1] = (float)(fg_g - bg_g) / 255;
	slope[2] = (float)(fg_b - bg_b) / 255;

	/* Global Color Table (palette) */
	for (i = 0; i < 256; i++) {
		*hdr++ = (uint8_t)(bg_r + slope[0]*i);
		*hdr++ = (uint8_t)(bg_g + slope[1]*i);
		*hdr++ = (uint8_t)(bg_b + slope[2]*i);
	}

	/* GIF: Graphics Control Extension Block */
	*hdr++ = '!';
	*hdr++ = 0xf9;
	*hdr++ = 4;
	*hdr++ = 1;
	*hdr++ = 0;
	*hdr++ = 0;
	*hdr++ = 0; 	/* transparent color index */
	*hdr++ = 0;

	/* GIF: Image Block */
	*hdr++ = ',';
	*hdr++ = 0;
	*hdr++ = 0;
	*hdr++ = 0;
	*hdr++ = 0;

	conf->pre_hdr_w_off2 = hdr - hdrbuf;	/* image width */
	hdr += 2;
	conf->pre_hdr_h_off2 = hdr - hdrbuf;	/* image height */
	hdr += 2;

	*hdr++ = 0;
	*hdr++ = 8;

	conf->pre_hdr_len = hdr - hdrbuf;
	conf->pre_hdr = apr_pcalloc(pool, conf->pre_hdr_len);
	memcpy((uint8_t *)conf->pre_hdr, hdrbuf, conf->pre_hdr_len);

	return OK;
}

static void
write_gif_from_buf(struct textgif_conf *conf, request_rec *r, uint8_t *buf, int w, int h, int rowstride) {
	uint8_t hdrbuf[conf->pre_hdr_len];
	uint16_t d16;

	memcpy(hdrbuf, conf->pre_hdr, conf->pre_hdr_len);

	d16 = htole16(w);
	memcpy(&hdrbuf[conf->pre_hdr_w_off1], &d16, 2);
	memcpy(&hdrbuf[conf->pre_hdr_w_off2], &d16, 2);

	d16 = htole16(h);
	memcpy(&hdrbuf[conf->pre_hdr_h_off1], &d16, 2);
	memcpy(&hdrbuf[conf->pre_hdr_h_off2], &d16, 2);

	ap_rwrite(hdrbuf, conf->pre_hdr_len, r);

	compress(9, r, buf, w, h, rowstride);

	hdrbuf[0] = 0;		/* block terminator */
	hdrbuf[1] = ';';	/* trailer */
	ap_rwrite(hdrbuf, 2, r);
}
