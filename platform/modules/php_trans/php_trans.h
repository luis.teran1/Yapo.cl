#ifndef PHP_TRANS_H
#define PHP_TRANS_H 1

#define PHP_TRANS_WORLD_VERSION "1.0"
#define PHP_TRANS_WORLD_EXTNAME "trans"

PHP_FUNCTION(trans_commit);

extern zend_module_entry trans_module_entry;
#define phpext_trans_ptr &trans_module_entry

#endif
