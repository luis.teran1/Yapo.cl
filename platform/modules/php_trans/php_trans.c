#include <php.h>
#include <stdio.h>

#include <transapi.h>
#include <util.h>

#include "php_trans.h"
#include "logging.h"
#include "vtree.h"
#include "util.h"
#include "platform_php.h"

static zend_function_entry trans_functions[] = {
    PHP_FE(trans_commit, NULL)
    {NULL, NULL, NULL}
};

zend_module_entry trans_module_entry EXPORTED = {
#if ZEND_MODULE_API_NO >= 20010901
    STANDARD_MODULE_HEADER,
#endif
    PHP_TRANS_WORLD_EXTNAME,
    trans_functions,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
#if ZEND_MODULE_API_NO >= 20010901
    PHP_TRANS_WORLD_VERSION,
#endif
    STANDARD_MODULE_PROPERTIES
};

#ifdef COMPILE_DL_TRANS
#undef ZEND_DLEXPORT
#define ZEND_DLEXPORT EXPORTED
ZEND_GET_MODULE_PROTOTYPE(trans)
ZEND_GET_MODULE(trans)
#endif


static void
add_array_to_trans(transaction_t *trans, zval *arr, char *transkey) {
	zval **data;
	HashTable *arr_hash;
	HashPosition pointer;
	unsigned long index;
	unsigned int len;
	char *key;
	char buf[256];
	size_t size;
	char *cp;
	int type = 0;

	arr_hash = Z_ARRVAL_P(arr);

	for(zend_hash_internal_pointer_reset_ex(arr_hash, &pointer); 
			zend_hash_get_current_data_ex(arr_hash, (void**) &data, &pointer) == SUCCESS; 
			zend_hash_move_forward_ex(arr_hash, &pointer)) {

		type = zend_hash_get_current_key_ex(arr_hash, &key, &len, &index, 0, &pointer);
		if (type == HASH_KEY_IS_STRING || type == HASH_KEY_IS_LONG) {
			if (transkey) {
				key = transkey;
				len = strlen(key);
			}
			if (Z_TYPE_PP(data) == IS_STRING) {
				if ((cp = strstr(key, ":")) != NULL) {
					cp++;
					size = zend_atoi(cp, len - (cp - key));
				} else {
					size = strlen(Z_STRVAL_PP(data));
				}
				trans_add_pair(trans, key, Z_STRVAL_PP(data), size);
			} else if (Z_TYPE_PP(data) == IS_LONG) {
				snprintf(buf, sizeof(buf), "%ld", Z_LVAL_PP(data));
				size = strlen(buf);
				trans_add_pair(trans, key, buf, size);
			} else if (Z_TYPE_PP(data) == IS_DOUBLE) {
				/* Float is bad, but instead of just skipping the value, convert it
				 * to integer. Still awful, but a bit better then the previous behavior */
				snprintf(buf, sizeof(buf), "%d", (int)Z_DVAL_PP(data));
				size = strlen(buf);
				trans_add_pair(trans, key, buf, size);
			} else if (Z_TYPE_PP(data) == IS_ARRAY) {
				add_array_to_trans(trans, *data, key);
			}
		}
	}
}

static void
trans_direct_cb(transaction_t *trans, const char *key, size_t klen, const char *val, size_t vlen, void *cbdata) {
	zval *cbarr = cbdata;
        HashTable *cbarr_hash = Z_ARRVAL_P(cbarr);
	zval **cb;
	char *kbuf = alloca(klen + 1);
	
	memcpy(kbuf, key, klen);
	kbuf[klen] = '\0';

	if (zend_hash_find(cbarr_hash, (char*)kbuf, klen + 1, (void**)&cb) == SUCCESS) {
		zval *k;
		zval *v;
		zval *retval = NULL;
		zval **params[2] = {&k, &v};

		MAKE_STD_ZVAL(k);
		ZVAL_STRINGL(k, (char*)key, klen, 1);
		MAKE_STD_ZVAL(v);
		ZVAL_STRINGL(v, (char*)val, vlen, 1);

		if (Z_TYPE_PP(cb) == IS_STRING) {
			call_user_function_ex(CG(function_table), NULL, *cb, &retval, 2, params, 0, NULL TSRMLS_CC);
		} else if (Z_TYPE_PP(cb) == IS_ARRAY) {
			HashTable *hash = Z_ARRVAL_PP(cb);
			zval **obj;
			zval **f;

			zend_hash_index_find(hash, 0, (void**)&obj);
			zend_hash_index_find(hash, 1, (void**)&f);

			call_user_function_ex(NULL, obj, *f, &retval, 2, params, 0, NULL TSRMLS_CC);
		}
		zval_dtor(k);
		zval_dtor(v);
		if (retval)
			zval_dtor(retval);
	}
}

static void
set_trans_cbs(transaction_t *trans, zval *arr) {
        HashTable *arr_hash = Z_ARRVAL_P(arr);
	HashPosition pointer;
	zval **data;
        
        for(zend_hash_internal_pointer_reset_ex(arr_hash, &pointer); 
            zend_hash_get_current_data_ex(arr_hash, (void**) &data, &pointer) == SUCCESS; 
            zend_hash_move_forward_ex(arr_hash, &pointer)) {
		char *key;
		unsigned int len;
		unsigned long index;
                int type = zend_hash_get_current_key_ex(arr_hash, &key, &len, &index, 0, &pointer);

		if (type == HASH_KEY_IS_STRING)
			trans_add_callback(trans, key, trans_direct_cb);
	}
}

PHP_FUNCTION(trans_commit) {
	transaction_t *trans;
	zval *header_arr;
	zval *data_arr;
	zval *node_arr;
	char *key;
	char *value;
	int status; 
	zval *cbarr = NULL;
	int res;

	if (ZEND_NUM_ARGS() < 3 || ZEND_NUM_ARGS() > 4) WRONG_PARAM_COUNT;

	if (array_init(return_value) != SUCCESS) {
		RETURN_NULL();
	}

	if (ZEND_NUM_ARGS() == 4)
		res = zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, "aa!aa!", &node_arr, &header_arr, &data_arr, &cbarr); 
	else
		res = zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, "aa!a", &node_arr, &header_arr, &data_arr);
	if (res == FAILURE) {
		add_assoc_string(return_value, "status", "-1", 1);
	} else {
		struct bpapi_vtree_chain vtree;
		/* XXX this could probably be considered cheating... */
		extern const struct bpapi_vtree php_vtree_node;

		vtree.fun = &php_vtree_node;
		vtree.data = node_arr;
		vtree.next = NULL;

		if ((trans = trans_init_vtree(vtree_getint(&vtree, "php_timeout", NULL) ?: 50000, &vtree)) == NULL) {
			RETURN_NULL();
		}
		trans_set_cbdata(trans, cbarr);

		if (header_arr)
			add_array_to_trans(trans, header_arr, NULL);

		add_array_to_trans(trans, data_arr, NULL);

		if (cbarr)
			set_trans_cbs(trans, cbarr);

		if ((status = trans_commit(trans)) == TRANS_COMMIT_OK) {
			while (trans_iter_next(trans, &key, &value)) {
				zval **oldval;
				zval *newarr;

				if (zend_hash_find(Z_ARRVAL_P(return_value), key, strlen(key) + 1, (void**)&oldval) == SUCCESS) {
					if ((*oldval)->type == IS_STRING) {
						MAKE_STD_ZVAL(newarr);
						if (array_init(newarr) != SUCCESS) {
							RETURN_NULL();
						}
						add_next_index_string(newarr, (*oldval)->value.str.val, 1);
						if (zend_hash_del(Z_ARRVAL_P(return_value), key, strlen(key) + 1) != SUCCESS) {
							RETURN_NULL();
						}
						add_assoc_zval(return_value, key, newarr);
						oldval = &newarr;
					} else if((*oldval)->type != IS_ARRAY) {
						add_assoc_string(return_value, "oldval error", "not array string", 1);
						return;
					}
					add_next_index_string(*oldval, value, 1);
				} else
					add_assoc_string(return_value, key, value, 1);
			}
		} else {
			char *value;
			int try_rescue_token = 0;

			switch (status) {
				case TRANS_COMMIT_ERROR_TIMEOUT:
				case TRANS_COMMIT_ERROR_EOF:
				case TRANS_COMMIT_ERROR_BLOB_NO_NL:
					try_rescue_token = 1;
					break;
			}

			xasprintf(&value, "%s:%s (status:%d)", "TRANS_FATAL", trans_strerror(status), status);
			add_assoc_string(return_value, "status", value, 1);

			free(value);

			if (try_rescue_token) {
				value = trans_get_key(trans, "token");
				if (value) {
					add_assoc_string(return_value, "token", value, 1);
					free (value);
				}
			}
		}
		vtree_free(&vtree);

		trans_free(trans);
	}
}
