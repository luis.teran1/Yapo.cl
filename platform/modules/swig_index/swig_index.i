%module search_index
%{
#include "swig_index.h"

%}
#define AATTR 1
#define AIND 2
enum {INDEX_READER, INDEX_WRITER};

struct index_db *openindex(const char *base, int mode, int compress);
struct index_db *indexprimenew(const char *base, int shard, int compress);
int close_index(struct index_db *db, int);

struct document *opendoc(struct index_db *db, int id, int order, int suborder);
int store_doc(struct index_db *, struct document *, int);
int doc_add_attr(struct document *doc, const char *name, const char *value, int);
int docaddword(struct document *doc, const char *word, int pos, unsigned short posflags);

int index_log_setup(const char *logtag, const char *level);

#ifdef SWIGPERL
%{
/* Hackery: The generated perl module need these declarations to build with -Wmissing-declarations */
XS(_wrap_openindex);
XS(_wrap_indexprimenew);
XS(_wrap_close_index);
XS(_wrap_opendoc);
XS(_wrap_store_doc);
XS(_wrap_doc_add_attr);
XS(_wrap_doc_add_word);
XS(_wrap_index_log_setup);
XS(_wrap_docaddword);
%}

%perlcode %{

sub open_index($$$) {
        return &openindex(@_);
}

sub doc_add_word($$$$) {
	return &docaddword(@_);
}

@EXPORT = qw(INDEX_READER INDEX_WRITER AATTR AIND);
@EXPORT_OK = qw(openindex open_index indexprimenew close_index opendoc store_doc doc_add_attr docaddword doc_add_word index_log_setup);
my @export_all;
push @export_all, @EXPORT, @EXPORT_OK;
%EXPORT_TAGS = ('all' => \@export_all);
%}
#endif

#ifdef SWIGPHP
%{
/* Needed to generate prototype for ZEND_GET_MODULE */
#include "platform_php.h"

ZEND_GET_MODULE_PROTOTYPE(dummy);

%}
#endif

#if SWIG_VERSION < 0x010340
%runtime %{
/* Declare internal function if older than 1.3.40. %begin seems unsupported on swig 1.3.29 */
void SWIG_ResetError();
%}
#else
#ifdef SWIGPHP
%runtime %{
/* Required to build with -Wmissing-declarations on newer SWIG versions */
ZEND_NAMED_FUNCTION(_wrap_swig_search_index_alter_newobject);
ZEND_NAMED_FUNCTION(_wrap_swig_search_index_get_newobject);
%}
#endif
#endif

