TOPDIR?=../../..
PLATFORMDIR?=../..
SRCS=swig_index_perl.c swig_index_helpers.c
LIB= search_index
INSTALL_TARGETS += search_index.pm
EXTRACLEAN += search_index.pm swig_index_perl.c

USE_SEARCH_INDEX=yes
USE_PERL=yes

CONLYWARN= -Wno-old-style-definition -Wno-unused -Wno-uninitialized -Wno-sign-compare
CONLYFLAGS+=-fno-strict-aliasing
CPPFLAGS+=-D_REENTRANT -D_GNU_SOURCE -I${COMMONSRCDIR} -I$(shell perl -MConfig -e 'print $$Config{archlib}')/CORE

%_perl.c: %.i
	swig -perl -const -o $@  $<

INSTALLDIR=${PERL_INSTALL}

include ${PLATFORMDIR}/mk/lib.mk
