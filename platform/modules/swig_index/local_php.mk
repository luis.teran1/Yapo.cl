TOPDIR?=../../..
PLATFORMDIR?=../..
SRCS = swig_index_php.c swig_index_helpers.c
LIB= php_index
EXTRACLEAN+=php_index.h search_index.php swig_index_php.c

USE_SEARCH_INDEX=yes
USE_PHP=yes

all:

#COPTS+=-fno-strict-aliasing
# Zend does not use const.
CWARNFLAGS+=-Wno-write-strings
CONLYWARN= -Wno-old-style-definition

%_php.c: %.i
	swig -php -o $@  $<

INSTALLDIR=/modules

include ${PLATFORMDIR}/mk/commonsrc.mk
include ${PLATFORMDIR}/mk/lib.mk
