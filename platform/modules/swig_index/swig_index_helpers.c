#include "swig_index.h"
#include "util.h"
#include "logging.h"

struct index_db *
openindex(const char *base, int mode, int compress) {
	return open_index(base, mode);
}

struct index_db *
indexprimenew(const char *base, int shard, int compress) {
	return index_prime_new(base, shard, 0, 0);
}

struct index_db *
indexprimenewupdate(const char *base, int shard, int update, int compress) {
	return index_prime_new(base, shard, update, 0);
}

struct document *
opendoc(struct index_db *db, int id, int order, int suborder) {
	struct document *doc = open_doc(db);

	doc->id.id = id;
	doc->id.order = order;
	doc->id.suborder = suborder;

	return doc;
}

int
docaddword(struct document *doc, const char *word, int pos, unsigned short posflags) {
	struct docpos dpos = { .flags = posflags, .pos = pos };
	doc_add_word(doc, word, -1, &dpos);
	return 0;
}

int
index_log_setup(const char *logtag, const char *level) {
	return log_setup(logtag, level);
}
