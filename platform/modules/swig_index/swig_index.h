#include "index.h"

struct index_db *openindex(const char *base, int mode, int compress);
struct index_db *indexprimenew(const char *base, int shard, int compress);
struct index_db *indexprimenewupdate(const char *base, int shard, int update, int compress);
struct document *opendoc(struct index_db *db, int id, int order, int suborder);
int docaddword(struct document *doc, const char *word, int pos, unsigned short posflags);

int index_log_setup(const char *logtag, const char *level);
