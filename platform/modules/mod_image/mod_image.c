#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <regex.h>
#include <time.h>
#include <apr_network_io.h>
#include <apr_strings.h>

#include <sys/types.h>
#include <sys/mman.h>
#include <unistd.h>
#include <errno.h>
#include <limits.h>
#include <sys/stat.h>

#define CORE_PRIVATE
#include "httpd.h"
#include "http_config.h"
#include "http_log.h"
#include "http_protocol.h"


#include <gd.h>
#include <pcre.h>
#include <math.h>

#define W(img) (img->sx)
#define H(img) (img->sy)

#include "platform_version.h"
#include "util.h"
#include "bconf.h"
#include "sbalance.h"
#include "avl.h"
#include "mod_bconf_extern.h"
#include "fd_pool.h"
#ifdef HAS_REDIS_UPLOAD
#include "upload.h"
#endif

#define DEGRADE_STEP 5

/* Apache define module */
module AP_MODULE_DECLARE_DATA image_module EXPORTED;

#if GD_MAJOR_VERSION > 2 || (GD_MAJOR_VERSION == 2 && GD_MINOR_VERSION >= 1)
#define MODERN_LIB_GD
#endif

/* HTTP support */
#define DEF_SOCK_TIMEOUT	(APR_USEC_PER_SEC * 30)
#define SOCKBUF_SIZE	4096
#define SB_RETRIES 2
#define SB_FAILCOST 2048
#define SB_SOFTFAILCOST 1024

struct image_req {
	char *path;
	char *image;
	char *data_ptr;
	apr_size_t header_len;
	apr_size_t read;
	long image_len;
};

struct http_node {
	apr_uri_t uri;
	const char *url;
	apr_sockaddr_t *sa;
};

struct img_req;
struct image_transformation {
	struct avl_node tree;
	const char *key;

	int min_width;
	int min_height;
	int max_width;
	int max_height;
	enum {
		SM_ASPECT,
		SM_CENTER,
		SM_CENTER_SCALE,
		SM_ASPECT_BG,
		SM_CROP,
	} scale_mode;

	int quality;
	int sharpen;
	int max_size;
	int interlace;

	enum {
		ROT_0,
		ROT_90,
		ROT_180,
		ROT_270,
	} rotation;

	const char *base_path;
	const char *fallback_file;
	const char *src_type;

	int watermark_padding_percentage;
	const char *watermark_position;
	unsigned char watermark_comment_byte;
	int watermark_padding;
	gdImagePtr watermark_image;
	gdImagePtr watermark_image_small;

	gdImagePtr fail_pixel;

	int crop_margin;
	int resample;
	int resample_fast_min_pixels;	/* Require at least this many pixels to attempt fast-resampling */
	int resample_fast_max_level;	/* Maximum number of times to halve dimensions */
	int resample_fast_backoff;		/* Back off this many levels from the maximum */

	int bg_color;

	/* For crop_image_xy */
	int crop_w;
	int crop_h;

	int (**transformations)(struct img_req *);
};

struct image_conf {
	struct bconf_node *mi_root;
	pcre *url_scheme;
	struct sbalance sb;
#ifdef HAS_REDIS_UPLOAD
	struct upload_pool redis;
#endif
	struct avl_node *tr_root;
};

struct img_req {
	struct image_conf *conf;
	struct image_transformation *it;
	char *tr_type;
	char *image_ref;			/* Request reference. */
	const char *watermark_position;
	int watermark_padding_percentage;
	enum {
		IT_JPEG,
		IT_PNG,
		IT_GIF
	} output_type;				/* Request file type. */
	request_rec *r;

	size_t raw_img_sz;
	void *raw_img_buf;
	enum {
		RIT_UNKNOWN,
		RIT_JPEG,
		RIT_PNG,
		RIT_GIF
	} raw_img_type;

	/* For crop_image_xy */
	int crop_x;
	int crop_y;

	gdImagePtr image;
};

static int cache_headers(struct img_req *);
static int fetch_http(struct img_req *);
static int fetch_file(struct img_req *);
static int cleanup_file(struct img_req *);
#ifdef HAS_REDIS_UPLOAD
static int fetch_redis(struct img_req *);
static int cleanup_redis(struct img_req *);
#endif
static int open_magic(struct img_req *);
static int crop_image(struct img_req *);
static int crop_image_xy(struct img_req *);
static int scale_image(struct img_req *);
static int sharpen_image(struct img_req *);
static int check_watermark_comment(struct img_req *);
static int watermark_image(struct img_req *);
static int rotate_image(struct img_req *);
static int output_image(struct img_req *);
static void rotate90(gdImagePtr, gdImagePtr);
static void rotate180(gdImagePtr, gdImagePtr);
static void rotate270(gdImagePtr, gdImagePtr);

static apr_status_t do_connect(apr_socket_t **sock, apr_pool_t *mp, struct http_node *node);
static enum sbalance_conn_status do_client_task(apr_socket_t *sock, apr_pool_t *mp, request_rec *r, struct image_req *image_http, apr_uri_t *uri);
static void *create_jpeg(request_rec *r, struct image_transformation *it, gdImagePtr image, int *len);


static int
it_cmp(const struct avl_node *l, const struct avl_node *r) {
	const struct image_transformation *lt = avl_data(l, struct image_transformation, tree);
	const struct image_transformation *rt = avl_data(r, struct image_transformation, tree);

	return strcmp(lt->key, rt->key);
}

static gdImagePtr
load_watermark(struct image_transformation *it, const char *wf) {
	FILE *wm = fopen(wf, "r");
	if (wm == NULL) {
		ap_log_error(APLOG_MARK, APLOG_WARNING, OK, NULL, "mod_image: transformation %s can't open watermark '%s'", it->key, wf);
		return NULL;
	}

	gdImagePtr res = NULL;
#define EXTCMP(fn, ext) ((strlen(fn) > sizeof(ext) - 1) ? strcmp(&fn[strlen(fn) - sizeof(ext) + 1], ext) == 0 : 0)
	if (EXTCMP(wf, ".jpg") || EXTCMP(wf, ".jpeg"))
		res = gdImageCreateFromJpeg(wm);
	else if (EXTCMP(wf, ".gif"))
		res = gdImageCreateFromGif(wm);
	else if (EXTCMP(wf, ".png"))
		res = gdImageCreateFromPng(wm);
	else
		ap_log_error(APLOG_MARK, APLOG_WARNING, OK, NULL, "mod_image: transformation %s, watermark (%s) extension unknown", it->key, wf);
#undef EXTCMP

	fclose(wm);
	return res;
}

static int
image_transformation_init(apr_pool_t *pool, struct image_conf *ic, struct bconf_node *n) {
	struct image_transformation *it = apr_pcalloc(pool, sizeof(struct image_transformation));
	const char *scale_mode;
	const char *rotation;
	int i;

	it->key = bconf_key(n);

	
	if ((scale_mode = bconf_get_string(n, "scale.mode")) == NULL || strcmp(scale_mode, "aspect") == 0) {
		it->scale_mode = SM_ASPECT;
		if (bconf_get(n, "scale.width")) {
			it->max_width = bconf_get_int(n, "scale.width");
			it->min_width = bconf_get_int(n, "scale.width");
			it->max_height = bconf_get_int(n, "scale.height");
			it->min_height = bconf_get_int(n, "scale.height");
		} else {
			it->max_width = bconf_get_int(n, "scale.max_width");
			it->min_width = bconf_get_int(n, "scale.min_width");
			it->max_height = bconf_get_int(n, "scale.max_height");
			it->min_height = bconf_get_int(n, "scale.min_height");
		}
	} else {
		if (strcmp(scale_mode, "center") == 0) {
			it->scale_mode = SM_CENTER;
		} else if (strcmp(scale_mode, "center_scale") == 0) {
			it->scale_mode = SM_CENTER_SCALE;
		} else if (strcmp(scale_mode, "aspect_bg") == 0) {
			it->scale_mode = SM_ASPECT_BG;
		} else if (strcmp(scale_mode, "crop") == 0) {
			it->scale_mode = SM_CROP;
		}
		if (bconf_get(n, "max_width") || bconf_get(n, "min_width")) {
			ap_log_error(APLOG_MARK, APLOG_WARNING, OK, NULL, "mod_image: can't set min/max width/height when scale_mode is not \"aspect\"");
			return -1;
		}

		it->max_width = bconf_get_int(n, "scale.width");
		it->min_width = 0;
		it->max_height = bconf_get_int(n, "scale.height");
		it->min_height = 0;
	}


	if ((rotation = bconf_get_string(n, "rotation")) != NULL) {
		if (strcmp(rotation, "0") == 0) {
			it->rotation = ROT_0;
		} else if (strcmp(rotation, "90") == 0) {
			it->rotation = ROT_90;
		} else if (strcmp(rotation, "180") == 0) {
			it->rotation = ROT_180;
		} else if (strcmp(rotation, "270") == 0) {
			it->rotation = ROT_270;
		}
	}

	it->quality = bconf_get_int(n, "quality");
	it->sharpen = bconf_get_int(n, "sharpen");
	it->interlace = bconf_get_int(n, "interlace");
	it->max_size = bconf_get_int(n, "max_size") * 1024;
	if (bconf_get(n, "base_path"))
		it->base_path = bconf_get_string(n, "base_path");
	else
		it->base_path = bconf_get_string(ic->mi_root, "base_path");

	if (bconf_get(n, "ft")) {
		it->src_type = bconf_get_string(n, "ft");
	} else if (bconf_get(ic->mi_root, "ft")) {
		it->src_type = bconf_get_string(ic->mi_root, "ft");
	} else {
		it->src_type = NULL;
	}
	if (bconf_get(n, "watermark_image")) {
		it->watermark_image = load_watermark(it, bconf_get_string(n, "watermark_image"));
		if (!it->watermark_image)
			return -1;
		if (bconf_get(n, "watermark_image_small")) {
			it->watermark_image_small = load_watermark(it, bconf_get_string(n, "watermark_image_small"));
			if (!it->watermark_image_small)
				return -1;
		}

		it->watermark_position = bconf_get_string(n, "watermark_position");
		it->watermark_comment_byte = bconf_get_int(n, "watermark_comment_byte");
		it->watermark_padding = bconf_get_int(n, "watermark_padding");
		it->watermark_padding_percentage = bconf_get_int(n, "watermark_padding_percentage");

		/* Over 100% is no good. Over 50% -> wrap around */
		if (it->watermark_padding_percentage > 100)
			it->watermark_padding_percentage = it->watermark_padding_percentage % 50;
		else if (it->watermark_padding_percentage > 50)
			it->watermark_padding_percentage = 100 - it->watermark_padding_percentage;

	}

	if (bconf_get_int(n, "fail_pixel")) {
		it->fail_pixel = gdImageCreateTrueColor(1, 1);
		gdImageSetPixel(it->fail_pixel, 0, 0, gdTrueColorAlpha(255, 255, 255, 0));
	}

	if (bconf_get(n, "fallback_file"))
		it->fallback_file = bconf_get_string(n, "fallback_file");

	it->bg_color = gdTrueColorAlpha(bconf_get_int(n, "bg_color.r"), bconf_get_int(n, "bg_color.g"), bconf_get_int(n, "bg_color.b"), bconf_get_int(n, "bg_color.a") / 2);

	it->crop_margin = bconf_get_int(n, "crop_margin");

	/* Setup resampling options, with fallback to values specified at root */
	it->resample = bconf_get_int_default(n, "resample.enabled", bconf_get_int_default(ic->mi_root, "resample.enabled", 1));
	it->resample_fast_min_pixels = bconf_get_int_default(n, "resample.fast_min_pixels", bconf_get_int_default(ic->mi_root, "resample.fast_min_pixels", 640*480));
	it->resample_fast_max_level = bconf_get_int_default(n, "resample.fast_max_level", bconf_get_int_default(ic->mi_root, "resample.fast_max_level", 0));
	it->resample_fast_backoff = bconf_get_int_default(n, "resample.fast_backoff", bconf_get_int_default(ic->mi_root, "resample.fast_backoff", 0));

	if (it->quality == 0)
		it->quality = 100;

	/* Static for now */
	it->transformations = apr_pcalloc(pool, sizeof(int (*)(struct image_req *)) * 12);

	i = 0;
	it->transformations[i++] = cache_headers;
	if (bconf_get_int(ic->mi_root, "http.enabled") == 1) {
		it->transformations[i++] = fetch_http;
		it->transformations[i++] = open_magic;
		if (it->watermark_comment_byte)
			it->transformations[i++] = check_watermark_comment;
#ifdef HAS_REDIS_UPLOAD
	} else if (bconf_get_int(ic->mi_root, "redis.enabled") == 1) {
		it->transformations[i++] = fetch_redis;
		it->transformations[i++] = open_magic;
		if (it->watermark_comment_byte)
			it->transformations[i++] = check_watermark_comment;
		it->transformations[i++] = cleanup_redis;
#endif
	} else {
		it->transformations[i++] = fetch_file;
		it->transformations[i++] = open_magic;
		if (it->watermark_comment_byte)
			it->transformations[i++] = check_watermark_comment;
		it->transformations[i++] = cleanup_file;
	}

	if (bconf_get_int(n, "crop_image"))
		it->transformations[i++] = crop_image;
	if (bconf_get_int(n, "crop_image_xy")) {
		it->transformations[i++] = crop_image_xy;

		it->crop_w = 100;
		it->crop_h = 100;

		if (bconf_get_int(n, "width"))
			it->crop_w = bconf_get_int(n, "width");
		if (bconf_get_int(n, "height"))
			it->crop_h = bconf_get_int(n, "height");
	}
	if (bconf_get(n, "scale"))
		it->transformations[i++] = scale_image;
	if (it->sharpen)
		it->transformations[i++] = sharpen_image;
	if (it->rotation)
		it->transformations[i++] = rotate_image;
	if (it->watermark_image) /* keep last */
		it->transformations[i++] = watermark_image;
	it->transformations[i++] = output_image;
	it->transformations[i++] = NULL;

	avl_insert(&it->tree, &ic->tr_root, it_cmp);
	return 0;
}

static struct image_transformation *
image_transformation_lookup(struct image_conf *ic, const char *name) {
	struct avl_node *n;
	struct image_transformation s;

	s.key = name;

	if ((n = avl_lookup(&s.tree, &ic->tr_root, it_cmp)) == NULL)
		return NULL;
	return avl_data(n, struct image_transformation, tree);
}

static void
image_transformation_destroy(struct avl_node *ait) {
	struct image_transformation *it;

	if (ait == NULL)
		return;

	it = avl_data(ait, struct image_transformation, tree);
	image_transformation_destroy(ait->link[0]);
	image_transformation_destroy(ait->link[1]);

	if (it->watermark_image)
		gdFree(it->watermark_image);
}

static apr_status_t
image_cleanup(void *data) {
        struct image_conf *conf = data;

        if (conf->mi_root) {
		ap_log_error(APLOG_MARK, APLOG_DEBUG, OK, NULL, "mod_image: Unloading.");
		pcre_free(conf->url_scheme);
        	conf->mi_root = NULL;
		image_transformation_destroy(conf->tr_root);
        }

	return APR_SUCCESS;
}  

/* set up our image_conf */
static void *
create_dir_config(apr_pool_t *pool, char *x) {
	ap_log_error(APLOG_MARK, APLOG_DEBUG, OK, NULL, "mod_image: create_dir_config %s", x);
	return apr_pcalloc(pool, sizeof(struct image_conf));
}

static const char *
command_config_root(cmd_parms *cmd, void *mconfig, const char *bcroot) {
	struct image_conf *conf;
	const char *regex;
	struct bconf_node *transformations;
	struct bconf_node *tr;
	struct bconf_node *br;
	const char *err;
	int errval;
	int i;

	ap_log_error(APLOG_MARK, APLOG_DEBUG, OK, NULL, "mod_image: command_config_root %s", bcroot);

	conf = mconfig;

	/* If bconf root is already set up, this worker is already configured. */
	if (conf->mi_root)
		return NULL;

#ifdef GD_VERSION_STRING
	ap_log_error(APLOG_MARK, APLOG_INFO, OK, cmd->server, "mod_image configuration '%s' on platform %s using GD %s", bcroot, PLATFORM_VERSION_STRING, GD_VERSION_STRING);
#endif

	if ((br = mod_bconf_get_root(cmd->server)) == NULL) {
		ap_log_error(APLOG_MARK, APLOG_WARNING, OK, NULL, "mod_image: Failed to get bconf root.");
		return "no bconf root";
	}
	conf->mi_root = bconf_get(br, bcroot);

	/* Compile the regexp */
	regex = bconf_get_string(conf->mi_root, "url_scheme");
	if (!regex || strcmp(regex, "") == 0) {
		ap_log_error(APLOG_MARK, APLOG_WARNING, OK, NULL, "mod_image: mod_image.url_scheme missing.");
		return "url_scheme missing";
	}
	if ((conf->url_scheme = pcre_compile(regex, 0, &err, &errval, NULL)) == NULL) {
		ap_log_error(APLOG_MARK, APLOG_WARNING, OK, NULL, "mod_image: url_scheme failed to compile (%s).", err);
		return "url_scheme regexp compilation error";
	}

	apr_pool_cleanup_register(cmd->pool, conf, image_cleanup, apr_pool_cleanup_null);

	transformations = bconf_get(conf->mi_root, "type");
	for (i = 0; (tr = bconf_byindex(transformations, i)); i++) {
		if (image_transformation_init(cmd->pool, conf, tr) != 0) {
			ap_log_error(APLOG_MARK, APLOG_WARNING, OK, NULL, "mod_image: can't init transformation %s", bconf_key(tr));
			return "transformation init error";
		}
	}

	/* Set up the sbalance stuff. */
	if (bconf_get_int(conf->mi_root, "http.enabled") == 1) {
		struct bconf_node *costs;
		struct bconf_node *urls;
		int sb_softfailcost;
		int sb_failcost;
		int sb_retries;
		int i;

		if ((sb_retries = bconf_get_int(conf->mi_root, "http.sb_retries")) == 0)
			sb_retries = SB_RETRIES;
		if ((sb_failcost = bconf_get_int(conf->mi_root, "http.sb_failcost")) == 0)
			sb_failcost = SB_FAILCOST;
		if ((sb_softfailcost = bconf_get_int(conf->mi_root, "http.sb_softfailcost")) == 0)
			sb_softfailcost = SB_SOFTFAILCOST;

		ap_log_error(APLOG_MARK, APLOG_NOTICE, OK, NULL, "mod_image: HTTP - using sb_retries %d", sb_retries);
		ap_log_error(APLOG_MARK, APLOG_NOTICE, OK, NULL, "mod_image: HTTP - using sb_failcost %d", sb_failcost);
		ap_log_error(APLOG_MARK, APLOG_NOTICE, OK, NULL, "mod_image: HTTP - using sb_softfailcost %d", sb_softfailcost);

		sbalance_init(&conf->sb, sb_retries, sb_failcost, sb_softfailcost, ST_RANDOM);

		if ((urls = bconf_vget(conf->mi_root, "http", "http_backend", "base_url", NULL)) == NULL) {
			ap_log_error(APLOG_MARK, APLOG_ERR, OK, NULL, "mod_image: no http.http_backend.base_url");
			return "failed to load http.backend.base_url";
		}
		if ((costs = bconf_vget(conf->mi_root, "http", "http_backend", "cost", NULL)) == NULL) {
			ap_log_error(APLOG_MARK, APLOG_ERR, OK, NULL, "mod_image: no http.http_backend.cost.NN");
			return "failed to load http.backend.cost";
		}

		if (bconf_count(urls) != bconf_count(costs)) {
			ap_log_error(APLOG_MARK, APLOG_ERR, OK, NULL, "mod_image: HTTP - # of .base_url doesn't match # of .cost in bconf");
			return "base_url vs. cost mismatch";
		}
		for (i = 0; i < bconf_count(urls); i++) {
			struct http_node *node;
			const char *url;
			int node_cost;

			node = apr_pcalloc(cmd->pool, sizeof(*node));

			url = bconf_value(bconf_byindex(urls, i));
			node_cost = atoi(bconf_value(bconf_byindex(costs, i)));

			ap_log_error(APLOG_MARK, APLOG_NOTICE, OK, NULL, "mod_image: HTTP - setting up backend host %s (%d)", url, node_cost);

			if (apr_uri_parse(cmd->pool, url, &node->uri) != APR_SUCCESS) {
				ap_log_error(APLOG_MARK, APLOG_ERR, OK, NULL, "mod_image: HTTP - mod_image.http.http_backend.base_url couldn't be parsed: %s", url);
				return "unparseable http.http_backend.base_url";
			}
			if (strncmp(node->uri.scheme, "http", 4) != 0) {
				ap_log_error(APLOG_MARK, APLOG_ERR, OK, NULL, "mod_image: HTTP - unsupported scheme in mod_image.http.http_backend.base_url: %s", node->uri.scheme);
				return "unsupported http.http_backend scheme";
			}

			if (!node->uri.port) {
				node->uri.port = apr_uri_port_of_scheme(node->uri.scheme);
			}

			if (apr_sockaddr_info_get(&node->sa, node->uri.hostname, APR_INET, node->uri.port, 0, cmd->pool) != APR_SUCCESS) {
				ap_log_error(APLOG_MARK, APLOG_ERR, OK, NULL, "mod_image: HTTP - connection %s port %d",node->uri.hostname,node->uri.port );
				ap_log_error(APLOG_MARK, APLOG_ERR, OK, NULL, "mod_image: HTTP - apr_sockaddr_info_get failed");
				return "apr_sockaddr_info_get";
			}
			
			node->url = url;
			sbalance_add_serv(&conf->sb, node_cost, node);
		}
	}

#ifdef HAS_REDIS_UPLOAD
	/* Set up the sbalance stuff. */
	if (bconf_get_int(conf->mi_root, "redis.enabled") == 1) {
		struct upload_pool *pool = &conf->redis;
		const char *server;
		memset(&pool->redis_node, 0, sizeof(pool->redis_node));
		if ((server = bconf_get_string(conf->mi_root, "redis.server")) != NULL) {
			bconf_vtree(&pool->redis_node,
			    bconf_vget(br, "common", server, NULL));
			pool->redis = fd_pool_create(&pool->redis_node, 1, NULL);
			ap_log_error(APLOG_MARK, APLOG_NOTICE, OK, NULL, "mod_image: REDIS - setting up backend host %s: pool->redis %lx", server, (long)pool->redis);
		}
	}
#endif
	return NULL;	
}

#define MAX_REGEXP_MATCHES 6

static int 
image_handler(request_rec *r) {
	int matches[MAX_REGEXP_MATCHES * 3];
	struct img_req ireq;
	char *tr_type;
	char *output_type;
	int res;
	int errval;
	int i;
	char * crop_x = NULL;
	char * crop_y = NULL;
	char *crop_bconf_key = NULL;

	ap_log_rerror(APLOG_MARK, APLOG_DEBUG, 0, r, "mod_image: handler: '%s' '%s'", r->handler, r->uri);

	if (strcmp(r->handler, "mod_image") != 0) {
		return DECLINED;
	}

	/* Only allow GET */
	r->allowed = (AP_METHOD_BIT << M_GET);
	if (r->method_number != M_GET)
		return DECLINED;

	if ((ireq.conf = ap_get_module_config(r->per_dir_config, &image_module)) == NULL) {
		ap_log_rerror(APLOG_MARK, APLOG_WARNING, 0, r, "mod_image: no config for directory (%s)", r->uri);
		return DECLINED;
	}

	if ((errval = pcre_exec(ireq.conf->url_scheme, NULL, r->uri, strlen(r->uri), 0, 0, matches, MAX_REGEXP_MATCHES * 3)) < 4) {
		ap_log_rerror(APLOG_MARK, APLOG_DEBUG, 0, r, "mod_image: Regexp match if '%s' failed (%d).", r->uri, errval);
		return DECLINED;
	}

	tr_type = apr_pstrndup(r->pool, r->uri + matches[1 * 2 + 0],  matches[1 * 2 + 1] - matches[1 * 2 + 0]);

	if ((ireq.it = image_transformation_lookup(ireq.conf, tr_type)) == NULL) {
		ap_log_rerror(APLOG_MARK, APLOG_DEBUG, 0, r, "mod_image: unknown transformation (%s)", tr_type);
		return DECLINED;
	}

	ireq.watermark_padding_percentage = ireq.it->watermark_padding_percentage;
	ireq.watermark_position = ireq.it->watermark_position;

	ireq.image_ref = apr_pstrndup(r->pool, r->uri + matches[2 * 2 + 0],  matches[2 * 2 + 1] - matches[2 * 2 + 0]);
	output_type = apr_pstrndup(r->pool, r->uri + matches[3 * 2 + 0],  matches[3 * 2 + 1] - matches[3 * 2 + 0]);

	asprintf(&crop_bconf_key, "type.%s.crop_image_xy", tr_type);

	if (bconf_get_int(ireq.conf->mi_root, crop_bconf_key)) {
		crop_x = apr_pstrndup(r->pool, r->uri + matches[4 * 2 + 0],  matches[4 * 2 + 1] - matches[4 * 2 + 0]);
		crop_y = apr_pstrndup(r->pool, r->uri + matches[5 * 2 + 0],  matches[5 * 2 + 1] - matches[5 * 2 + 0]);

		if (crop_x && crop_y) {
			ireq.crop_x = atoi(crop_x);
			ireq.crop_y = atoi(crop_y);

			ap_log_rerror(APLOG_MARK, APLOG_DEBUG, 0, r, "mod_image crop_x: %s", crop_x);
			ap_log_rerror(APLOG_MARK, APLOG_DEBUG, 0, r, "mod_image crop_y: %s", crop_y);
		}
	}

	free(crop_bconf_key);
	crop_bconf_key = NULL;

	if (strcmp(output_type, "jpg") == 0 || strcmp(output_type, "jpeg") == 0) {
	        ireq.output_type = IT_JPEG;
		ap_set_content_type(r, "image/jpeg");
	} else if (strcmp(output_type, "gif") == 0) {
		ireq.output_type = IT_GIF;
		ap_set_content_type(r, "image/gif");
	} else if (strcmp(output_type, "png") == 0) {
		ireq.output_type = IT_PNG;
		ap_set_content_type(r, "image/png");
	} else {
		ap_log_rerror(APLOG_MARK, APLOG_DEBUG, 0, r, "mod_image: unknown file type: %s", output_type);
		return DECLINED;
	}

	ap_log_rerror(APLOG_MARK, APLOG_DEBUG, 0, r, "mod_image r->uri: %s, size_type: %s, image_ref: %s, output_type: %s", r->uri, tr_type, ireq.image_ref, output_type);
	ireq.r = r;

	res = DECLINED;
	for (i = 0; ireq.it->transformations[i]; i++) {
		if ((res = (*(ireq.it->transformations)[i])(&ireq)) != 0)
			break;
	}

	if (res == HTTP_NOT_FOUND && ireq.it->fail_pixel != NULL) {
		void *buf = NULL;
		int len;

		apr_table_set(r->headers_out, "Cache-Control", "no-cache");
		switch (ireq.output_type) {
		case IT_JPEG:
			buf = create_jpeg(ireq.r, ireq.it, ireq.it->fail_pixel, &len);
			break;
		case IT_PNG:
			buf = gdImagePngPtr(ireq.it->fail_pixel, &len);
			break;
		case IT_GIF:
			buf = gdImageGifPtr(ireq.it->fail_pixel, &len);
			break;
		}
		if (buf == NULL) {
			ap_log_rerror(APLOG_MARK, APLOG_WARNING, 0, r, "mod_image: fail pixel failed");
			return HTTP_INTERNAL_SERVER_ERROR;
		}
		ap_set_content_length(r, len);
		ap_rwrite(buf, len, r);
		gdFree(buf);
		return 0;
	}

	return res;
}

/*
 * Set last modified for header
 * 1. get current time and today = (t / 86400) % 100; 
 * 2. get files day 2 first digits in it->base_path
 * 3. compare today 2 digits and file 2 digits 
 */
static int
cache_headers(struct img_req *ireq) {
	char buf_len[1024];
	char time_buf[1024];
	const char *etag;
	const char *mod_time;
	struct tm *my_time;
	time_t unix_time;
	int my_two_day;
	int file_two_day;
	int day_diff;

	unix_time = time(NULL);
	my_two_day = (unix_time / 86400) % 100;

	/* Get two first digits from filename */
	memcpy(time_buf, ireq->image_ref, 2);
	time_buf[2] = '\0';
	file_two_day = atoi(time_buf);

	/* Check time difference */
	if (my_two_day > file_two_day) {
		day_diff = my_two_day - file_two_day;
	} else if (my_two_day < file_two_day) {
		day_diff = my_two_day + (100-file_two_day); 
	} else {
		day_diff = 0;
	}

	unix_time -= (day_diff * 86400);
	my_time = gmtime(&unix_time);
	time_buf[0] = '\0';
	strftime(time_buf, sizeof(time_buf), "%a, %d %b %Y 00:00:00 GMT", my_time);
	apr_table_set(ireq->r->headers_out, "Last-Modified", time_buf);

	snprintf(buf_len, 1024, "%x-%x-%s-%s", ireq->it->max_width, ireq->it->max_height, ireq->image_ref, ireq->it->src_type ?: "X");
	apr_table_set(ireq->r->headers_out, "Etag", buf_len);

	if ((mod_time = apr_table_get(ireq->r->headers_in, "If-Modified-Since")) && (etag = apr_table_get(ireq->r->headers_in, "If-None-Match"))) {
		if((strcmp(mod_time,time_buf) == 0) && (strcmp(etag, buf_len) == 0)) {
			return HTTP_NOT_MODIFIED;
		}
	}

	return 0;
}

/*
 * Ignore file extensions, mime-types and all that. Just use good old magic numbers.
 */
static int
open_magic(struct img_req *ireq) {
	char *buf = ireq->raw_img_buf;

	switch ((((int)buf[0] << 8) & 0xff00) | (buf[1] & 0xff)) {
	case 0x8950:
		ireq->raw_img_type = RIT_PNG;
		ireq->image = gdImageCreateFromPngPtr(ireq->raw_img_sz, buf);
		if (ireq->image == NULL)
			break;
		if (ireq->output_type != IT_PNG) {
			gdImagePtr i = ireq->image;

			/*
			 * Input is a PNG with potential transparency and the output doesn't have transparency,
			 * we need to create and merge in a background.
			 */
			ireq->image = gdImageCreateTrueColor(W(i), H(i));
			gdImageFilledRectangle(ireq->image, 0, 0, W(i) - 1, H(i) - 1, ireq->it->bg_color);
			gdImageCopy(ireq->image, i, 0, 0, 0, 0, W(i), H(i));
			gdImageDestroy(i);
		}
		break;
	case 0xffd8:
		ireq->raw_img_type = RIT_JPEG;
		ireq->image = gdImageCreateFromJpegPtr(ireq->raw_img_sz, buf);
		break;
	case 0x4749:
		ireq->raw_img_type = RIT_GIF;
		ireq->image = gdImageCreateFromGifPtr(ireq->raw_img_sz, buf);
		break;
#if 0
	case 0x424d:
		return there is no BMP support in libgd.
#endif
	default:
		ap_log_rerror(APLOG_MARK, APLOG_WARNING, 0, ireq->r, "unknown file format 0x%x", (int)((buf[0] << 8) | buf[1]));
		return HTTP_INTERNAL_SERVER_ERROR;
	}
	if (ireq->image == NULL) {
		ap_log_rerror(APLOG_MARK, APLOG_WARNING, 0, ireq->r, "unable to parse image");
		return HTTP_INTERNAL_SERVER_ERROR;
	}
	return 0;
}

static int
fetch_fallback_file(struct img_req *ireq) {
	struct image_transformation *it = ireq->it;
	request_rec *r = ireq->r;
	char *buf;
	struct stat sb;
	int fd;

	if(!it->fallback_file) {
		return HTTP_NOT_FOUND;
	}

	if ((fd = open(it->fallback_file, O_RDONLY)) == -1 ||
	    fstat(fd, &sb) == -1 ||
	    (buf = mmap(NULL, sb.st_size, PROT_READ, MAP_SHARED|MAP_FILE, fd, 0)) == MAP_FAILED) {
		close(fd);
		ap_log_rerror(APLOG_MARK, APLOG_WARNING, 0, r, "mod_image: Unable to open fallback file: '%s'.", it->fallback_file);
		return HTTP_NOT_FOUND;
	}
	close(fd);

	ap_log_rerror(APLOG_MARK, APLOG_DEBUG, 0, r, "mod_image: Reading fallback source file: '%s'.", it->fallback_file);

	ireq->raw_img_sz = sb.st_size;
	ireq->raw_img_buf = buf;
	ireq->raw_img_type = RIT_UNKNOWN;

	return 0;
}


static int
fetch_http(struct img_req *ireq) {
	struct sbalance_connection sc;
	struct http_node *node = NULL;
	enum sbalance_conn_status status = SBCS_START;
	struct image_req image_http;
	struct image_conf *conf = ireq->conf;
	request_rec *r = ireq->r;
	struct image_transformation *it = ireq->it;

	memset(&image_http, 0, sizeof(image_http));
	image_http.path = apr_psprintf(r->pool, "%s/%s%s%s", bconf_get_string(conf->mi_root, "http.http_backend.base_path"), ireq->image_ref, it->src_type ? "." : "", it->src_type ?: "");

	sbalance_conn_new(&sc, &conf->sb, 0);
	while (1) {
		apr_status_t rv;
		apr_socket_t *s;

		if ((node = sbalance_conn_next(&sc, status)) == NULL)
			break;
		if ((rv = do_connect(&s, r->pool, node)) != APR_SUCCESS) {
			ap_log_error(APLOG_MARK, APLOG_WARNING, OK, NULL, "mod_image: HTTP - socket connect to %s:%s failed (%d)", node->uri.hostname, node->uri.port_str, rv);
			status = SBCS_FAIL;
			continue;
		}
		status = do_client_task(s, r->pool, r, &image_http, &node->uri);
		apr_socket_close(s);
		if (status == SBCS_START) {
			break;
		}
	}
	sbalance_conn_done(&sc);
	if (node == NULL) {
		ap_log_rerror(APLOG_MARK, APLOG_ERR, 0, r, "mod_image: HTTP - No backend server could deliver the requested image");
		return HTTP_SERVICE_UNAVAILABLE;
	}

	if (image_http.image_len == 0) {
		ap_log_rerror(APLOG_MARK, APLOG_DEBUG, 0, r, "mod_image: not found");
		return fetch_fallback_file(ireq);
	}

	ireq->raw_img_sz = image_http.image_len;
	ireq->raw_img_buf = image_http.image;
	ireq->raw_img_type = RIT_UNKNOWN;

	return 0;
}

static int
fetch_file(struct img_req *ireq) {
	struct image_transformation *it = ireq->it;
	request_rec *r = ireq->r;
	char filename[1024];
	char *buf;
	struct stat sb;
	int fd;

	snprintf(filename, sizeof(filename), "%s/%s%s%s", it->base_path, ireq->image_ref, it->src_type ? "." : "", it->src_type ?: "");

	if ((fd = open(filename, O_RDONLY)) == -1 ||
	    fstat(fd, &sb) == -1 ||
	    (buf = mmap(NULL, sb.st_size, PROT_READ, MAP_SHARED|MAP_FILE, fd, 0)) == MAP_FAILED) {
		close(fd);
		ap_log_rerror(APLOG_MARK, APLOG_WARNING, 0, r, "mod_image: Unable to open file: '%s'.", filename);
		return fetch_fallback_file(ireq);
	}
	close(fd);

	ap_log_rerror(APLOG_MARK, APLOG_DEBUG, 0, r, "mod_image: Reading source file: '%s'.", filename);

	ireq->raw_img_sz = sb.st_size;
	ireq->raw_img_buf = buf;
	ireq->raw_img_type = RIT_UNKNOWN;

	return 0;
}

static int
cleanup_file(struct img_req *ireq) {
	munmap(ireq->raw_img_buf, ireq->raw_img_sz);
	return 0;
}

#if HAS_REDIS_UPLOAD
static int
fetch_redis(struct img_req *ireq) {
	request_rec *r = ireq->r;
	struct upload_pool *pool = &ireq->conf->redis;
	char *buf;
	size_t size;

	if (upload_get(pool, ireq->image_ref, &buf, &size) != 0) {
		ap_log_rerror(APLOG_MARK, APLOG_WARNING, 0, r, "mod_image: Unable fetching from redis: '%s'.", ireq->image_ref);
		return fetch_fallback_file(ireq);
	}

	ap_log_rerror(APLOG_MARK, APLOG_DEBUG, 0, r, "mod_image: Fetching from redis: '%s'.", ireq->image_ref);

	ireq->raw_img_sz = size;
	ireq->raw_img_buf = buf;
	ireq->raw_img_type = RIT_UNKNOWN;

	return 0;
}

static int
cleanup_redis(struct img_req *ireq) {
	free(ireq->raw_img_buf);
	return 0;
}
#endif

/*
 * Generate a JPEG image from an gdImage, trying to meet max_size requirements.
 */
static void *
create_jpeg(request_rec *r, struct image_transformation *it, gdImagePtr image, int *len) {
	int quality = it->quality;
	void *ret = NULL;

	/*
	 * Lower quality until we reach the max_size goal (if there's one) or can't lower the quality anymore.
	 */
	do {
		if (ret != NULL) {
			gdFree(ret);
			quality -= DEGRADE_STEP;
		}

		ret = gdImageJpegPtr(image, len, quality);
	     
		if (ret == NULL || *len < 1) {
			ap_log_rerror(APLOG_MARK, APLOG_ERR, 0, r, "mod_image: gdImageJpegPtr failed image.");
			if (ret != NULL)
				gdFree(ret);
			return NULL;
		}
		ap_log_rerror(APLOG_MARK, APLOG_DEBUG, 0, r, "mod_image: generated %sjpeg %d kbytes with quality %d.", image->interlace ? "progressive " : "", (*len) / 1024, quality );
	} while (*len > it->max_size && it->max_size && quality > DEGRADE_STEP);

	return ret;
}

static int
output_image(struct img_req *ireq) {	
	void *buf = NULL;
	int len;

	if (ireq->it->interlace)
		gdImageInterlace(ireq->image, 1);

	switch (ireq->output_type) {
	case IT_JPEG:
		buf = create_jpeg(ireq->r, ireq->it, ireq->image, &len);
		break;
	case IT_PNG:
		gdImageSaveAlpha(ireq->image, 1);
		buf = gdImagePngPtr(ireq->image, &len);
		break;
	case IT_GIF:
		buf = gdImageGifPtr(ireq->image, &len);
		break;
	}

	gdImageDestroy(ireq->image);
	if (!buf || len < 1)
		return HTTP_INTERNAL_SERVER_ERROR;

	ap_set_content_length(ireq->r, len);
	ap_rwrite(buf, len, ireq->r);

	gdFree(buf);

	return 0;
}

static int
sharpen_image(struct img_req *ireq) {
	gdImageSharpen(ireq->image, ireq->it->sharpen);
	return 0;
}

/* 
 * Read the comment written at the end of images by imgput and determine
 * whether watermarking should be disabled. 
 */
static int
check_watermark_comment(struct img_req *ireq) {
	/* Only jpeg for now. */
	if (ireq->raw_img_type != RIT_JPEG)
		return 0;

	if (ireq->raw_img_sz >= 8) {
		unsigned char magic_byte = ireq->it->watermark_comment_byte;
		unsigned char *comment_buf = (unsigned char*)ireq->raw_img_buf + ireq->raw_img_sz - 4;

		/* Tagged images will have FFEB0002 at the end */
		if (comment_buf[0] == 0xff && 
				comment_buf[1] == magic_byte &&
				comment_buf[2] == 0 &&
				comment_buf[3] == 2) {
			/* XXX byte order? */
			uint32_t comment_len = *(uint32_t*)(void*)((char*)ireq->raw_img_buf + ireq->raw_img_sz - 8);

			if (comment_len && comment_len < 256 && comment_len <= ireq->raw_img_sz - 8) {
				const void *comment_ptr = (char*)ireq->raw_img_buf + ireq->raw_img_sz - 8 - comment_len;
				char *comment = strndupa(comment_ptr, comment_len);

					
				ap_log_rerror(APLOG_MARK, APLOG_DEBUG, 0, ireq->r, "mod_image: comment found: '%s'", comment);
				if (strncmp(comment, "source=", sizeof("source=") - 1) == 0) {
					const char *source = comment + sizeof("source=") - 1;

					if (bconf_vget_int(ireq->conf->mi_root, "source", source, "watermark", NULL))
						return 0;
				}
			}
		}
	}
	ireq->watermark_position = NULL;
	return 0;
}

static void
get_watermark_pos(struct img_req *ireq, gdImagePtr wm_image, const char *watermark_position, int pos[2]) {

	if (ireq->watermark_padding_percentage) {
		int minx = W(wm_image) / 2;
		int miny = H(wm_image) / 2;
		int x = W(ireq->image) * ireq->watermark_padding_percentage / 100 < minx ? minx : ireq->watermark_padding_percentage;
		int y = H(ireq->image) * ireq->watermark_padding_percentage / 100 < miny ? miny : ireq->watermark_padding_percentage;

		/* topleft and left position are on the reference quadrant. No changes for them */
		if (strstr(watermark_position, "bottomleft"))
			y = 100 - y;
		if (strstr(watermark_position, "topright") || strcmp(watermark_position, "right") == 0)
			x = 100 - x;
		if (strstr(watermark_position, "bottomright"))
			x = 100 - x, y = 100 - y;

		pos[0] = (W(ireq->image) * x / 100) - minx;
		pos[1] = (H(ireq->image) * y / 100) - miny;

	} else {

		int pad = ireq->it->watermark_padding;

		if (strstr(watermark_position, "topleft") || strcmp(watermark_position, "left") == 0)
			pos[0] = pad, pos[1] = pad;
		if (strstr(watermark_position, "bottomleft"))
			pos[0] = pad, pos[1] = H(ireq->image) - H(wm_image) - pad;
		if (strstr(watermark_position, "topright") || strcmp(watermark_position, "right") == 0)
			pos[0] = W(ireq->image) - W(wm_image) - pad, pos[1] = pad;
		if (strstr(watermark_position, "bottomright"))
			pos[0] = W(ireq->image) - W(wm_image) - pad, pos[1] = H(ireq->image) - H(wm_image) - pad;
		if (strstr(watermark_position, "topmiddle"))
			pos[0] = W(ireq->image)/2 - W(wm_image) / 2, pos[1] = pad;
		if (strstr(watermark_position, "middleright"))
			pos[0] = W(ireq->image) - W(wm_image) - pad, pos[1] = H(ireq->image)/2 - H(wm_image)/2;
		if (strstr(watermark_position, "bottommiddle"))
			pos[0] = W(ireq->image)/2 - W(wm_image)/2, pos[1] = H(ireq->image) - H(wm_image) - pad;
		if (strstr(watermark_position, "middleleft"))
			pos[0] = pad, pos[1] = H(ireq->image)/2 - H(wm_image)/2;
	}
}

static int
watermark_image(struct img_req *ireq) {
	const char *watermark_position = ireq->watermark_position;
	gdImagePtr watermark_chosen_image = ireq->it->watermark_image;

	/* Check if the image is small enough to use the small watermark */
	if ( ireq->it->watermark_image_small && (W(ireq->image) < bconf_get_int(ireq->conf->mi_root, "threshold.small_marker.width") || H(ireq->image) < bconf_get_int(ireq->conf->mi_root, "threshold.small_marker.height")) )
		watermark_chosen_image = ireq->it->watermark_image_small;

	if (!watermark_position)
		return 0;

	/* Insert watermark at a "random" position */
	if (strncmp(watermark_position, "random", sizeof("random") - 1) == 0) {
		int i = strlen(ireq->image_ref) - 1;
		int v = 0;
		int nv = 1;

		/* Find two last digits in uri, since 100 is divisible by 4 */
		for ( ; i >= 0 && nv <= 10 ; i--) {
			if (ireq->image_ref[i] >= '0' && ireq->image_ref[i] <= '9') {
				v += nv * (ireq->image_ref[i] - '0');
				nv *= 10;
			}
		}

		int denom = strcmp(watermark_position, "random_middle_corner") == 0 ? 8 : 4;
		int add = strcmp(watermark_position, "randommiddle") == 0 ? 4 : 0;
		switch (v % denom + add) {
		case 0:
			watermark_position = "topleft";
			break;
		case 1:
			watermark_position = "bottomleft";
			break;
		case 2:
			watermark_position = "topright";
			break;
		case 3:
			watermark_position = "bottomright";
			break;
		case 4:
			watermark_position = "topmiddle";
			break;
		case 5:
			watermark_position = "middleright";
			break;
		case 6:
			watermark_position = "bottommiddle";
			break;
		case 7:
			watermark_position = "middleleft";
			break;
		}
	}

	int pos[2] = { 0 };
	get_watermark_pos(ireq, watermark_chosen_image, watermark_position, pos);
	gdImageCopy(ireq->image, watermark_chosen_image, pos[0], pos[1], 0, 0, W(watermark_chosen_image), H(watermark_chosen_image));

	return 0;
}

static int
rotate_image(struct img_req *ireq) {

	int new_height = 0;
	int new_width = 0;
	void (*rotation)(gdImagePtr, gdImagePtr);

	switch (ireq->it->rotation) {
		case ROT_90:
			new_height = W(ireq->image);
			new_width = H(ireq->image);
			rotation = rotate90;
			break;
		case ROT_180:
			new_height = H(ireq->image);
			new_width = W(ireq->image);
			rotation = rotate180;
			break;
		case ROT_270:
			new_height = W(ireq->image);
			new_width = H(ireq->image);
			rotation = rotate270;
			break;
		default:
			rotation = NULL;
	}

	if (rotation) {
		gdImagePtr image = ireq->image;
		ireq->image = gdImageCreateTrueColor(new_width, new_height);
		if (ireq->image == NULL) {
			gdImageDestroy(image);
			ap_log_rerror(APLOG_MARK, APLOG_ERR, 0, ireq->r, "mod_image: Unable to rotate image '%s'.", ireq->image_ref);
			return -1;
		}

		rotation(image, ireq->image);

		gdImageDestroy(image);
	}

	return 0;
}

static void
rotate90(gdImagePtr src, gdImagePtr dst) {
	int x;
	int y;

	for (y = 0; y < H(src); y++) {
		for (x = 0; x < W(src); x++) {
			dst->tpixels[x][W(dst) - y - 1] = src->tpixels[y][x];
		}
	}
}

static void
rotate180(gdImagePtr src, gdImagePtr dst) {
	int x;
	int y;

	for (y = 0; y < H(src); y++) {
		for (x = 0; x < W(src); x++) {
			dst->tpixels[H(dst) - y - 1][W(dst) - x - 1] = src->tpixels[y][x];
		}
	}
}

static void
rotate270(gdImagePtr src, gdImagePtr dst) {
	int x;
	int y;

	for (y = 0; y < H(src); y++) {
		for (x = 0; x < W(src); x++) {
			dst->tpixels[H(dst) - 1 - x][y] = src->tpixels[y][x];
		}
	}
}


static apr_status_t
do_connect(apr_socket_t **sock, apr_pool_t *mp, struct http_node *node) {
	apr_socket_t *s;
	apr_status_t rv;

	rv = apr_socket_create(&s, node->sa->family, SOCK_STREAM, APR_PROTO_TCP, mp);
	if (rv != APR_SUCCESS) {
 		ap_log_error(APLOG_MARK, APLOG_ERR, OK, NULL, "mod_image: HTTP - apr_socket_create failed %s:%s (%d)", node->uri.hostname, node->uri.port_str, rv);
		return rv;
	}

	/* it is a good idea to specify socket options explicitly.
	 * in this case, we make a blocking socket with timeout. */
	apr_socket_opt_set(s, APR_SO_NONBLOCK, 1);
	apr_socket_timeout_set(s, DEF_SOCK_TIMEOUT);

	rv = apr_socket_connect(s, node->sa);
	if (rv != APR_SUCCESS) {
		return rv;
	}

	apr_socket_opt_set(s, APR_SO_NONBLOCK, 0);
	apr_socket_timeout_set(s, DEF_SOCK_TIMEOUT);

	*sock = s;
	return APR_SUCCESS;
}

/*
 * Send a request as a simple HTTP request protocol.
 */
static enum sbalance_conn_status
do_client_task(apr_socket_t *sock, apr_pool_t *mp, request_rec *r, struct image_req *image_http, apr_uri_t *uri) {
	apr_status_t rv;
	int header_is_read = 0;
	char sockbuf[SOCKBUF_SIZE];
	char *dataptr = NULL;
	char *bufptr = NULL;
	char *dataptr_end = NULL;
	char *req_hdr = NULL;
	int part_of_img = 0;
	apr_size_t len = 0;

	req_hdr = apr_pstrcat(mp, "GET ", image_http->path, " HTTP/1.0", CRLF, "Host: ", uri->hostname, ":", uri->port_str, CRLF CRLF, NULL);

	len = strlen(req_hdr);

	rv = apr_socket_send(sock, req_hdr, &len);

	if (rv != APR_SUCCESS) {
		ap_log_rerror(APLOG_MARK, APLOG_ERR, 0, r, "mod_image: HTTP - sending '%s' failed (%d)", req_hdr, rv);
		return SBCS_FAIL;
	} else {
		ap_log_rerror(APLOG_MARK, APLOG_DEBUG, 0, r, "mod_image: HTTP - sending '%s' ok", req_hdr);
	}

	len = SOCKBUF_SIZE;
	bufptr = sockbuf;

	while (1) {
		rv = apr_socket_recv(sock, sockbuf, &len);

		if (!header_is_read) {
			/* We assume the first socket read will give us the headers */
			header_is_read = 1;
			bufptr += len;
			/* May not be the case if we get a HTTP/0.9 response? */
			if (memmem(sockbuf, 20, "200 OK" CRLF, strlen("200 OK" CRLF)) == NULL) {
				if (memmem(sockbuf, 20, " 503 ", strlen(" 503 ")) != NULL) {
					ap_log_rerror(APLOG_MARK, APLOG_ERR, 0, r, "mod_image: HTTP - 503 Service unavailable (%s)", req_hdr);
					return SBCS_TEMPFAIL;
				} else if (memmem(sockbuf, 20, " 404 ", strlen(" 404 ")) != NULL) {
					ap_log_rerror(APLOG_MARK, APLOG_ERR, 0, r, "mod_image: HTTP - 404 File not found (%s)", req_hdr);
					/* We don't class this as a server error since it's likely that all other servers will fail */
					return SBCS_START;
				} else {
					ap_log_rerror(APLOG_MARK, APLOG_ERR, 0, r, "mod_image: HTTP - Did not get 200 OK (%s)", req_hdr);
					ap_log_rerror(APLOG_MARK, APLOG_ERR, 0, r, "sockbuf: %s", sockbuf);
					return SBCS_FAIL;
				}
			}

			if ((dataptr = memmem(sockbuf, len, "Content-Length:", strlen("Content-Length:"))) == NULL) {
				ap_log_rerror(APLOG_MARK, APLOG_ERR, 0, r, "mod_image: HTTP - Did not find Content-Length");
				return SBCS_FAIL;
			}
			dataptr += strlen("Content-Length:");

			if ((dataptr_end = memmem(dataptr, bufptr - dataptr, CRLF, strlen(CRLF))) == NULL) {
				ap_log_rerror(APLOG_MARK, APLOG_ERR, 0, r, "mod_image: HTTP - Did not find CRLF after Content-Length");
				return SBCS_FAIL;
			}

			errno = 0;
			image_http->image_len = strtol(dataptr, &dataptr_end, 10);
			if ((errno == ERANGE && (image_http->image_len == LONG_MAX || image_http->image_len == LONG_MIN)) || (errno != 0 && image_http->image_len == 0)) {
				ap_log_rerror(APLOG_MARK, APLOG_ERR, 0, r, "mod_image: HTTP - Content-Length out of range or other error");
				return SBCS_FAIL;
           		}

           		if (dataptr_end == dataptr) {
				ap_log_rerror(APLOG_MARK, APLOG_ERR, 0, r, "mod_image: HTTP - Content-Length - no digits found");
				return SBCS_FAIL;
           		}

			if (image_http->image_len <= 0) {
				ap_log_rerror(APLOG_MARK, APLOG_ERR, 0, r, "mod_image: HTTP - Content-Length is negative or 0");
				return SBCS_FAIL;
			}

			if ((dataptr = memmem(dataptr_end, bufptr - dataptr_end, CRLF CRLF, strlen(CRLF CRLF))) == NULL) {
				ap_log_rerror(APLOG_MARK, APLOG_ERR, 0, r, "mod_image: HTTP - didn't find end of header");
				return SBCS_FAIL;
			}

			dataptr += strlen(CRLF CRLF);

			image_http->data_ptr = dataptr;
			image_http->header_len = dataptr - sockbuf;

			dataptr = apr_pcalloc(mp, image_http->image_len);
			part_of_img = bufptr - image_http->data_ptr;

			image_http->data_ptr = memcpy(dataptr, image_http->data_ptr, part_of_img);
			image_http->data_ptr += part_of_img;

			image_http->image = dataptr;
			image_http->read = 0;

			ap_log_rerror(APLOG_MARK, APLOG_DEBUG, 0, r, "mod_image: HTTP - Allocated image size:%ld (%s)", image_http->image_len, uri->path);
			len = SOCKBUF_SIZE;
			continue;
		}

		if (header_is_read && rv != APR_EOF && header_is_read && image_http->read < (apr_size_t)image_http->image_len) {
			image_http->read += len;
			if (image_http->read > (apr_size_t)image_http->image_len) {
				ap_log_rerror(APLOG_MARK, APLOG_ERR, 0, r, "mod_image: HTTP - Tried to write outside allocated memory");
				return SBCS_FAIL;
			}

			memcpy(image_http->data_ptr, sockbuf, len);
			image_http->data_ptr += len;
			continue;
		}

		if (rv == APR_EOF || len == 0 || image_http->read == (apr_size_t)image_http->image_len) {
			if (rv != APR_EOF) {
				ap_log_rerror(APLOG_MARK, APLOG_WARNING, 0, r, "mod_image: HTTP - Did not get EOF. data_len:%d, buflen:%d, header_len:%d",
					      (int)image_http->image_len, (int)image_http->read, (int)image_http->header_len);
			}
			if ((int)image_http->read + part_of_img != image_http->image_len) {
				ap_log_rerror(APLOG_MARK, APLOG_WARNING, 0, r, "mod_image: HTTP - Only: %d bytes of the total image size %d read", 
					      (int)image_http->read + part_of_img, (int)image_http->image_len);
			}
			break;
		}
	}

	return SBCS_START;
}

static int
crop_image(struct img_req *ireq) {
	struct image_transformation *it = ireq->it;
	int color;
	int x, y;
	int c_top = 0;
	int c_height = 0;
	int c_left = 0;
	int c_width = 0;

	/*
	 * Scan each row horisontally.
	 * First find the highest row where all pixels are the same color as top left pixel.
	 * Then find the lowest row where all pixels are not.
	 *
	 * Then repeat the same thing vertically (except leftmost column and rightmost column).
	 *
	 * This can be done much more efficiently.
	 */

	color = gdImageGetTrueColorPixel(ireq->image, 0, 0);

	for (y = 0; y < H(ireq->image); y++) {
		for (x = 0; x < W(ireq->image); x++) {
			if (gdImageGetTrueColorPixel(ireq->image, x, y) != color)
				break;
		}
		if (x == W(ireq->image)) {
			if (y == c_top)
				c_top = y + 1;
		} else {
			c_height = y + 1;
		}
	}

	for (x = 0; x < W(ireq->image); x++) {
		for (y = 0; y < H(ireq->image); y++) {
			if (gdImageGetTrueColorPixel(ireq->image, x, y) != color)
				break;
		}
		if (y == H(ireq->image)) {
			if (x == c_left)
				c_left = x + 1;
		} else {
			c_width = x + 1;
		}
	}
	
#ifdef CROP_DEBUG	
	gdImageRectangle(ireq->image, c_left, c_top, c_width-1, c_height-1, gdTrueColorAlpha(0, 0, 255, 0));
#endif

	if (it->crop_margin) {
		if (c_left > it->crop_margin) {
			c_left -= it->crop_margin;
		} else {
			c_left = 0;
		}
		if (c_top > it->crop_margin) {
			c_top -= it->crop_margin;
		} else {
			c_top = 0;
		}
		if (c_width < W(ireq->image) - it->crop_margin) {
			c_width += it->crop_margin;
		} else {
			c_width = W(ireq->image);
		}
		if (c_height < H(ireq->image) - it->crop_margin) {
			c_height += it->crop_margin;
		} else {
			c_height = H(ireq->image);
		}
	}

	if (c_top == 0 && c_left == 0 && c_width == W(ireq->image) && c_height == H(ireq->image))
		return 0;

	/* Fix potential segfault, when trying to allocate an image below, without width or height */
	if (c_width - c_left <= 0 || c_height - c_top <= 0)
		return 0;
	
#ifdef CROP_DEBUG
	 gdImageRectangle(ireq->image, c_left, c_top, c_width - 1, c_height - 1, gdTrueColorAlpha(0, 255, 0, 0));
	 gdImageRectangle(ireq->image, 0, 0, W(ireq->image) - 1, H(ireq->image) - 1, gdTrueColorAlpha(255, 0, 0, 0)); 
#else
	/* Crop image to new dimensions */
	gdImagePtr cut_image = gdImageCreateTrueColor(c_width - c_left, c_height - c_top);

	/* Pay attention to transparency. */
	if (ireq->output_type == IT_PNG) {
		gdImageAlphaBlending(cut_image, 0); 
		int transparent = gdImageColorAllocateAlpha(cut_image,  0, 0, 0, 127);
 		gdImageFilledRectangle(cut_image, 0, 0, W(cut_image), H(cut_image), transparent); 
	}

	gdImageCopy(cut_image, ireq->image, 0, 0, c_left, c_top, c_width, c_height);
	gdImageDestroy(ireq->image);
	ireq->image = cut_image;
#endif

	return 0;
}

#ifdef MODERN_LIB_GD
/*
	This function takes the input parameters and the image dimensions and calculates
	the intermediate image size based on those parameters, returning the number of
	levels of reduction.

	'w' and 'h' are only written to if an intermediate level was found.
*/
static int
calculate_intermediate_size(int max_level, int backoff, int target_w, int target_h, int *w, int *h) {
	int levels = 0;
	int iw = *w;
	int ih = *h;

	while (levels < max_level && iw/2 >= target_w && ih/2 >= target_h) {
		++levels;
		iw /= 2;
		ih /= 2;
	}

	if (levels) {
		int i;
		for (i = 0; i < levels - backoff ; ++i) {
			*w /= 2;
			*h /= 2;
		}
		levels = i;
	}

	return levels;
}
#endif

static int
crop_image_xy(struct img_req *ireq) {
	request_rec *r = ireq->r;
	struct image_transformation *it = ireq->it;

	ap_log_rerror(APLOG_MARK, APLOG_DEBUG, 0, r, "mod_image crop_image_xy: w: %i - h: %i", it->crop_w, it->crop_h);

	gdImagePtr cut_image = gdImageCreateTrueColor(it->crop_w, it->crop_h);
	gdImageCopy(cut_image, ireq->image, 0, 0, ireq->crop_x, ireq->crop_y, it->crop_w, it->crop_h);

	gdImageDestroy(ireq->image);
	ireq->image = cut_image;
	return 0;
}

static int
scale_image(struct img_req *ireq) {
	struct image_transformation *it = ireq->it;
	const char *image_ref = ireq->image_ref;
	gdImagePtr image = ireq->image;
	request_rec *r = ireq->r;
	int target_width = W(image);
	int target_height = H(image);
	int res_width = it->max_width;
	int res_height = it->max_height;
	double wratio = 1;
	double hratio = 1;
	int offset_x = 0;
	int offset_y = 0;

	if (it->scale_mode == SM_CENTER) {
	} else if (it->scale_mode == SM_CENTER_SCALE) {
		wratio = it->max_width / (double)W(image);
		hratio = it->max_height / (double)H(image);
		target_width = (int)((double)W(image) * fmax(wratio, hratio));
		target_height = (int)((double)H(image) * fmax(wratio, hratio));
	} else if (it->scale_mode == SM_CROP) {
		target_width = it->max_width;
		target_height = it->max_height;
		wratio = it->max_width / (double)W(image);
		hratio = it->max_height / (double)H(image);

		if (wratio < hratio)
			offset_x = (W(image) - (double)target_width / hratio)/2;
		else
			offset_y = (H(image) - (double)target_height / wratio)/2;
	} else {
		if ((it->max_width && it->max_height) && (target_width > it->max_width || target_height > it->max_height)) {
			wratio = it->max_width / (double)W(image);
			hratio = it->max_height / (double)H(image);
			target_width = (int)((double)W(image) * fmin(wratio, hratio));
			target_height = (int)((double)H(image) * fmin(wratio, hratio));
		} else if ((it->min_width && it->min_height) && (target_width < it->min_width || target_height < it->min_height)) {
			res_width = it->min_width;
			res_height = it->min_height;
			wratio = it->min_width / (double)W(image);
			hratio = it->min_height / (double)H(image);
			target_width = (int)((double)W(image) * fmax(wratio, hratio));
			target_height = (int)((double)H(image) * fmax(wratio, hratio));
		} else {
			res_width = W(image);
			res_height = H(image);
		}

		if (it->scale_mode == SM_ASPECT_BG) {
			res_width = it->max_width;
			res_height = it->max_height;
		} else if (it->scale_mode == SM_CROP) {
			target_width = it->max_width;
			target_height = it->max_height;

			if (wratio < hratio)
				offset_x = (W(image) - (double)target_width / hratio)/2;
			else
				offset_y = (H(image) - (double)target_height / wratio)/2;
		}
	}

	/* Safety net */
	if (target_height < 1)
		target_height = 1;
	if (target_width < 1)
		target_width = 1;

	if (it->scale_mode == SM_ASPECT) {
		res_width = (int)target_width;
		res_height = (int)target_height;
		/* If the size doesn't change, don't do anything. */
		if (res_width == W(image) && res_height == H(image))
			return 0;
	}

	ap_log_rerror(APLOG_MARK, APLOG_DEBUG, 0, r, "mod_image: Original [%dpx, %dpx], Resize to [%dpx, %dpx], Image size [%dpx, %dpx] (+%d,+%d offset).",
	    W(image), H(image), target_width, target_height, res_width, res_height,
		offset_x, offset_y);

	if ((ireq->image = gdImageCreateTrueColor(res_width, res_height)) == NULL) {
		gdImageDestroy(image);
		ap_log_rerror(APLOG_MARK, APLOG_ERR, 0, r, "mod_image: Unable to resize image '%s' to width: %i height: %i.", image_ref, target_width, target_height);
		return HTTP_INTERNAL_SERVER_ERROR;
	}

	if (ireq->output_type == IT_PNG) {
		/* The usual reason to use PNG is because we want alpha. */
		gdImageAlphaBlending(ireq->image, 0);
	}

	if (res_width != target_width || res_height != target_height) {
		/* Fill background color. */
		gdImageFilledRectangle(ireq->image, 0, 0, W(ireq->image) - 1, H(ireq->image) - 1, it->bg_color);
	}

	if (it->resample) {
		int iw = W(image);
		int ih = H(image);
		int levels_scaled = 0;
		gdImagePtr loc = image;

#ifdef MODERN_LIB_GD
		/* Check if fast-resampling has been requested */
		if (it->resample_fast_max_level && iw*ih >= it->resample_fast_min_pixels) {
			levels_scaled = calculate_intermediate_size(it->resample_fast_max_level, it->resample_fast_backoff, target_width, target_height, &iw, &ih);

			/* A suitable intermediate image size was calculated, do a fast rescale to it */
			if (levels_scaled) {
				ap_log_rerror(APLOG_MARK, APLOG_DEBUG, 0, r, "mod_image: Using fast-rescale of %d levels to [%dpx, %dpx] first.", levels_scaled, iw, ih);

				/* Scale source offsets */
				offset_x >>= levels_scaled;
				offset_y >>= levels_scaled;

				/* Future: Make configurable, and set back original for safety */
				gdImageSetInterpolationMethod(image, GD_NEAREST_NEIGHBOUR);
				loc = gdImageScale(image, iw, ih);
			}
		}
#endif

		gdImageCopyResampled(ireq->image, loc,
			(res_width - target_width) / 2, (res_height - target_height) / 2, /* dstX, dstY */
			offset_x, offset_y, /* srcX, srcY */
			target_width, target_height, /* dstW, dstH */
			iw - offset_x * 2, ih - offset_y * 2 /* srcW, srcH */
		);

		if (levels_scaled)
			gdImageDestroy(loc);

	} else {
		gdImageCopyResized(ireq->image, image,
			(res_width - target_width) / 2, (res_height - target_height) / 2,
			offset_x, offset_y,
			target_width, target_height,
			W(image) - offset_x * 2, H(image) - offset_y * 2
		);
	}

	gdImageDestroy(image);

	return 0;
}

static void 
register_hooks(apr_pool_t *p) {
	ap_hook_handler(image_handler, NULL, NULL, APR_HOOK_MIDDLE);
}

static const command_rec commands[] = {
	AP_INIT_TAKE1("ModImageConf", command_config_root, NULL, ACCESS_CONF, "bconf root for mod_image config"),
};

module AP_MODULE_DECLARE_DATA image_module EXPORTED = {
	STANDARD20_MODULE_STUFF,
	create_dir_config,		/* dir config creater */
	NULL,				/* dir merger --- default is to override */
	NULL,				/* server config */
	NULL,				/* merge server config */
	commands,			/* command table */
	register_hooks			/* register_hooks */
};

