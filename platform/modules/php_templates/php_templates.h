#ifndef PHP_TEMPLATES_H
#define PHP_TEMPLATES_H 1

#include "macros.h"

#ifndef PHP_TEMPLATES_WORLD_VERSION
#define PHP_TEMPLATES_WORLD_VERSION "1.0"
#endif
#ifndef PHP_TEMPLATES_WORLD_EXTNAME
#define PHP_TEMPLATES_WORLD_EXTNAME "templates"
#endif

PHP_FUNCTION(call_template);
PHP_FUNCTION(flush_template_cache);

struct bpapi;

void php_templates_start_local(struct bpapi *api, const char *template_name, const char *lang) WEAK;
void php_templates_end_local(struct bpapi *api, const char *template_name, const char *lang) WEAK;

extern zend_module_entry templates_module_entry EXPORTED;
#define phpext_templates_ptr &templates_module_entry

#define PHP_TEMPLATE_FUNCTION(fun) \
static const struct tpvar * \
template_##fun(struct bpapi *api, struct tpvar *dst, enum bpcacheable *cc, int argc, const struct tpvar *argv[]) { \
	zval *f; \
	zval *retval = NULL; \
	zval *p[argc]; \
	zval **pp[argc]; \
	int i; \
 \
	MAKE_STD_ZVAL(f); \
	ZVAL_STRING(f, "template_" #fun, 0); \
	for (i = 0 ; i < argc ; i++) { \
		const char *a; \
 \
		TPVAR_STRTMP(a, argv[i]); \
		MAKE_STD_ZVAL(p[i]); \
		ZVAL_STRING(p[i], (char*)a, 0); \
		pp[i] = &p[i]; \
	} \
	if (call_user_function_ex(CG(function_table), NULL, f, &retval, argc, pp, 0, NULL TSRMLS_CC) == SUCCESS ) { \
		if (Z_TYPE_P(retval) != IS_STRING) \
			convert_to_string_ex(&retval); \
		if (Z_TYPE_P(retval) == IS_STRING) \
			return TPV_SET(dst, TPV_DYNSTR(xstrndup(Z_STRVAL_P(retval), Z_STRLEN_P(retval)))); \
	} \
	return TPV_SET(dst, TPV_NULL); \
} \
ADD_TEMPLATE_FUNCTION_WITH_NAME(template_##fun, fun)

#define PHP_OUTPUT_FILTER(filt) \
static int \
outstring_raw_##filt(struct bpapi_output_chain *ochain, const char *str, size_t len) \
{ \
       zval *f; \
       zval *arg; \
       zval **argp[1] = {&arg}; \
       zval *retval; \
 \
       MAKE_STD_ZVAL(f); \
       ZVAL_STRING(f, "template_outstring_" #filt, 0); \
       MAKE_STD_ZVAL(arg); \
       ZVAL_STRINGL(arg, (char*)str, len, 0); \
       call_user_function_ex(CG(function_table), NULL, f, &retval, 1, argp, 0, NULL TSRMLS_CC); \
       return 0; /* XXX parse retval? */ \
} \
 \
static int \
init_##filt(const struct bpfilter *filter, struct bpapi *api, int argc, const struct tpvar **argv) { \
       zval *f; \
       zval *p[argc]; \
       zval **pp[argc]; \
       zval *retval; \
       int i; \
 \
       MAKE_STD_ZVAL(f); \
       ZVAL_STRING(f, "template_init_" #filt, 0); \
       for (i = 0; i < argc; i++) { \
	       const char *s; \
 \
	       TPVAR_STRTMP(s, argv[i]); \
               MAKE_STD_ZVAL(p[i]); \
               ZVAL_STRING(p[i], (char*)s, 0); /* Zend does not use const */ \
               pp[i] = &p[i]; \
       } \
 \
       call_user_function_ex(CG(function_table), NULL, f, &retval, argc, pp, 0, NULL TSRMLS_CC); \
       /* XXX parse retval? */ \
 \
       return 0; \
} \
 \
static void \
fini_##filt(struct bpapi_output_chain *ochain) { \
       zval *f; \
       zval *retval; \
 \
       MAKE_STD_ZVAL(f); \
       ZVAL_STRING(f, "template_fini_" #filt, 0); \
       call_user_function_ex(CG(function_table), NULL, f, &retval, 0, NULL, 0, NULL TSRMLS_CC); \
} \
 \
static void \
flush_##filt(struct bpapi_output_chain *ochain) { \
       zval *f; \
       zval *retval; \
 \
       MAKE_STD_ZVAL(f); \
       ZVAL_STRING(f, "template_flush_" #filt, 0); \
       call_user_function_ex(CG(function_table), NULL, f, &retval, 0, NULL, 0, NULL TSRMLS_CC); \
} \
 \
const struct bpapi_output filt##_output = { \
	self_outstring_fmt, \
	outstring_raw_##filt, \
	fini_##filt, \
	flush_##filt \
}; \
ADD_OUTPUT_FILTER(filt, 0)

#define PHP_SIMPLEVARS_FILTER(filt) \
static int \
init_##filt(const struct bpfilter *filter, struct bpapi *api, ...) { \
	zval *f; \
	int nargs; \
	zval *p[10]; \
	zval **pp[10]; \
	const char *arg; \
	va_list ap; \
	zval *retval; \
 \
	MAKE_STD_ZVAL(f); \
	ZVAL_STRING(f, "template_init_" #filt, 0); \
	nargs = 0; \
	va_start(ap, api); \
	while ((arg = va_arg(ap, const char *))) { \
		if (nargs >= 10) \
			xerrx(1, "too many args"); \
		MAKE_STD_ZVAL(p[nargs]); \
		ZVAL_STRING(p[nargs], (char*)arg, 0); /* Zend does not use const */ \
		pp[nargs] = &p[nargs]; \
		nargs++; \
	} \
	va_end(ap); \
 \
	call_user_function_ex(CG(function_table), NULL, f, &retval, nargs, pp, 0, NULL TSRMLS_CC); \
 \
	struct patch_data *data = BPAPI_SIMPLEVARS_DATA(api); \
	patchvars_init(data, #filt, "", 0, NULL, NULL); \
 \
	if (Z_TYPE_P(retval) == IS_ARRAY) { \
		int len = zend_hash_num_elements(Z_ARRVAL_P(retval)); \
		int i; \
		ulong index = 0; \
 \
		zend_hash_internal_pointer_reset(Z_ARRVAL_P(retval)); \
		for (i = 0; i < len; i++) { \
			char *keyname; \
			zval **value; \
 \
			if (zend_hash_get_current_key(Z_ARRVAL_P(retval), &keyname, &index, 0) == HASH_KEY_IS_LONG) { \
				continue; \
			} \
 \
			zend_hash_get_current_data(Z_ARRVAL_P(retval), (void**)&value); \
 \
			if (Z_TYPE_PP(value) == IS_STRING) { \
				bps_insert(&data->vars, keyname, Z_STRVAL_PP(value)); \
			} else if (Z_TYPE_PP(value) == IS_LONG) { \
				bps_set_int(&data->vars, keyname, Z_LVAL_PP(value)); \
			} \
 \
			zend_hash_move_forward(Z_ARRVAL_P(retval)); \
		} \
		zend_hash_internal_pointer_reset(Z_ARRVAL_P(retval)); \
	} \
 \
	return 0; \
} \
ADD_FILTER(filt, NULL, 0, &patch_simplevars, sizeof(struct patch_data), NULL, 0);

#endif
