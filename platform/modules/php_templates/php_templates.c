#include <stdio.h>
#include <stdarg.h>
#include <string.h>


#include <php.h>
#include <php_ini.h>
#include <SAPI.h>
#include "php_templates.h"
#include "bpapi.h"
#include "tree.h"
#include "logging.h"
#include "bconf.h"
#include "platform_php.h"

ZEND_MINIT_FUNCTION(templates);
ZEND_MSHUTDOWN_FUNCTION(templates);

static zend_function_entry templates_functions[] = {
    PHP_FE(call_template, NULL)
    PHP_FE(flush_template_cache, NULL)
    {NULL, NULL, NULL}
};

zend_module_entry template_module_entry EXPORTED = {
    STANDARD_MODULE_HEADER,
    PHP_TEMPLATES_WORLD_EXTNAME,
    NULL,
    /* Module init / shutdown */
    ZEND_MINIT(templates),
    ZEND_MSHUTDOWN(templates),
    /* Request init / shutdown */
    NULL,
    NULL,
    NULL,
    PHP_TEMPLATES_WORLD_VERSION,
    STANDARD_MODULE_PROPERTIES
};

#ifdef COMPILE_DL_TEMPLATES
#undef ZEND_DLEXPORT
#define ZEND_DLEXPORT EXPORTED
ZEND_GET_MODULE_PROTOTYPE(template)
ZEND_GET_MODULE(template)
#endif

PHP_INI_BEGIN()
PHP_INI_ENTRY(PHP_TEMPLATES_WORLD_EXTNAME "_" "call_template_name", "call_template", PHP_INI_ALL, NULL)
PHP_INI_ENTRY(PHP_TEMPLATES_WORLD_EXTNAME "_" "flush_template_cache_name", "flush_template_cache", PHP_INI_ALL, NULL)
PHP_INI_ENTRY(PHP_TEMPLATES_WORLD_EXTNAME "_" "log_level", "info", PHP_INI_SYSTEM, NULL)
PHP_INI_END()

static void phpvtree_zval_fetch_nodes(zval *vtree, struct bpapi_loop_var *loop);
static struct bpapi_vtree_chain *phpvtree_node_getnode(struct bpapi_vtree_chain *vchain, enum bpcacheable *cc, struct bpapi_vtree_chain *dst, int argc, const char **argv);
static void phpvtree_zval_fetch_keys_and_values(zval *vtree, struct vtree_keyvals *loop, int argc, const char **argv);

struct key_entry {
	const char *key;
	RB_ENTRY(key_entry) rbe;
};

struct _vtree_data {
	zval *vtree_cacheable;
	zval *vtree_uncacheable;
};

struct _simplevars_data {
	zval *data;
	RB_HEAD(key_tree, key_entry) keys;
	int nkeys;
};

struct bconf_node *filter_state_global;

static void free_keys(struct _simplevars_data *);

ZEND_MINIT_FUNCTION(templates) {
	REGISTER_INI_ENTRIES();

	char *log_level = INI_STR(PHP_TEMPLATES_WORLD_EXTNAME "_" "log_level");
	log_setup("php_templates", log_level);

	templates_functions[0].fname = INI_STR(PHP_TEMPLATES_WORLD_EXTNAME "_" "call_template_name");
	templates_functions[1].fname = INI_STR(PHP_TEMPLATES_WORLD_EXTNAME "_" "flush_template_cache_name");

	zend_register_functions(NULL, templates_functions, NULL, type);

	filter_state_bconf_init(&filter_state_global, FS_GLOBAL);
	filter_state_bconf_init(&filter_state_global, FS_THREAD);

	log_printf(LOG_DEBUG, ("php_templates configuration done"));
	return SUCCESS;
}

ZEND_MSHUTDOWN_FUNCTION(templates) {
	UNREGISTER_INI_ENTRIES();
	filter_state_bconf_free(&filter_state_global);
	return SUCCESS;
}

static __inline int
key_compare(const struct key_entry *pa, const struct key_entry *pb) {
	if (pa->key[0] != pb->key[0])
		return pa->key[0] - pb->key[0];

	return strcmp(pa->key, pb->key);
}

RB_GENERATE_STATIC(key_tree, key_entry, rbe, key_compare);

/* Needed as long as it's not included in the Zend API */
static int 
_zend_vprintf(const char *fmt, va_list ap) {
        char *buf;
        int res;

        if (vasprintf(&buf, fmt, ap) < 0)
                return 0;

        res = zend_printf("%s", buf);
        free(buf);

        return res;
}

static int 
outstring_fmt(struct bpapi_output_chain *ochain, const char *fmt, ...) {
	int res;
	va_list ap;

	va_start(ap, fmt);
	res = _zend_vprintf(fmt, ap);
	va_end(ap);
	return res;
}

static int 
outstring_raw(struct bpapi_output_chain *ochain, const char *str, size_t len) {
	return ZEND_WRITE(str, len);
}

static int 
has_key(struct bpapi_simplevars_chain *schain, const char *key) {
	int res = zend_hash_exists(Z_ARRVAL_P((zval*)((struct _simplevars_data*)(schain->data))->data), (char*)key, strlen(key) + 1);
	return res;
}

static void
insert_val_local(struct bpapi_simplevars_chain *schain, const char *key, int klen, const char *value, int vlen) {
	struct _simplevars_data *d = schain->data;
	zval *zval_data = (zval*)d->data;
	zval **oldval;
	zval *newarr = NULL;

	if (klen < 0)
		klen = strlen(key);
	if (vlen < 0)
		vlen = strlen(value);

	if (zend_hash_find(Z_ARRVAL_P(zval_data), (char*)key, klen+1, (void**)&oldval) == SUCCESS) {
		if (Z_TYPE_PP(oldval) != IS_ARRAY) {
			MAKE_STD_ZVAL(newarr);
			if (array_init(newarr) != SUCCESS) {
				return;
			}
			if (Z_TYPE_PP(oldval) == IS_STRING) {
				add_next_index_string(newarr, Z_STRVAL_PP(oldval), 1);
			} else if (Z_TYPE_PP(oldval) == IS_LONG) {
				add_next_index_long(newarr, Z_LVAL_PP(oldval));
			}
			if (zend_hash_del(Z_ARRVAL_P(zval_data), (char*)key, klen+1) != SUCCESS) {
				return;
			}
			add_assoc_zval_ex(zval_data, (char*)key, klen+1, newarr);
			oldval = &newarr;
		}
		add_next_index_stringl(*oldval, (char*)value, vlen, 1);
	} else {
		add_assoc_stringl_ex(zval_data, (char*)key, klen+1, (char*)value, vlen, 1);
		if (!RB_EMPTY(&d->keys)) {
			struct key_entry *ke = zmalloc(sizeof(*ke) + klen + 1);
			char *newkey;
			ke->key = newkey = (char *)(ke + 1);
			memcpy(newkey, key, klen);
			RB_INSERT(key_tree, &d->keys, ke);
		}
	}
}

static void
insert_val(struct bpapi_simplevars_chain *schain, const char *key, int klen, const char *value, int vlen) {

	/* Fast-path if we have a zero-terminated string already */
	if (klen < 0) {
		insert_val_local(schain, key, klen, value, vlen);
	} else {
		/*	Much of Zend and our code depends on the zero-terminator being included in the key,
			so if the key came from a buffer we must make a temporary copy of it */
		char k[klen+1];
		strlcpy(k, key, klen+1);
		insert_val_local(schain, key, klen, value, vlen);
	}
}

static void
remove_val(struct bpapi_simplevars_chain *schain, const char *key) {
	struct _simplevars_data *d = schain->data;
	zend_hash_del(Z_ARRVAL_P((zval*)d->data), (char*)key, strlen(key) + 1);
	free_keys(d);
}

static int 
length(struct bpapi_simplevars_chain *schain, const char *key) {
	zval **sub_array;
  
	if (zend_hash_find(Z_ARRVAL_P((zval*)((struct _simplevars_data*)(schain->data))->data), (char*)key, strlen(key) + 1, (void**)&sub_array) != FAILURE) {
		if ((Z_TYPE_PP(sub_array)) == IS_STRING || (Z_TYPE_PP(sub_array)) == IS_LONG) {
			return 1;
		} else if ((Z_TYPE_PP(sub_array)) != IS_ARRAY) {
			return 0;
		}
		return zend_hash_num_elements(Z_ARRVAL_PP(sub_array));
	} else
		return 0;
}

static const char* 
get_element(struct bpapi_simplevars_chain *schain, const char *key, int pos) {
	zval **sub_array;
	zval **value;
	int num_elements;

	if (zend_hash_find(Z_ARRVAL_P((zval*)((struct _simplevars_data*)(schain->data))->data), (char*)key, strlen(key) + 1, (void**)&sub_array) != FAILURE) {
		if ((Z_TYPE_PP(sub_array)) == IS_STRING) {
			return Z_STRVAL_PP(sub_array);
		} else if ((Z_TYPE_PP(sub_array)) == IS_LONG || (Z_TYPE_PP(sub_array)) == IS_DOUBLE) {
			convert_to_string_ex(sub_array);
			return Z_STRVAL_PP(sub_array);
		} else if ((Z_TYPE_PP(sub_array)) != IS_ARRAY) {
			return NULL;
		}

		num_elements = zend_hash_num_elements(Z_ARRVAL_PP(sub_array));
		if (num_elements && zend_hash_index_find(Z_ARRVAL_PP(sub_array), pos % num_elements, (void **)&value) != FAILURE) {
			if (Z_TYPE_PP(value) == IS_STRING) {
				return Z_STRVAL_PP(value);
			} else if (Z_TYPE_PP(value) == IS_LONG || Z_TYPE_PP(value) == IS_DOUBLE) {
				convert_to_string_ex(value);
				return Z_STRVAL_PP(value);
			}
		}
	}

	return NULL;
}

static void
fetch_loop_cleanup(struct bpapi_loop_var *loop) {
	free(loop->l.list);
}

static void
fetch_loop(struct bpapi_simplevars_chain *schain, const char *key, struct bpapi_loop_var *loop) {
	zval **sub_array;
	zval **value;
	int i;

	if (zend_hash_find(Z_ARRVAL_P((zval*)((struct _simplevars_data*)(schain->data))->data), (char*)key, strlen(key) + 1, (void**)&sub_array) != FAILURE) {
		if ((Z_TYPE_PP(sub_array)) == IS_STRING) {
			loop->len = 1;
			loop->l.list = xmalloc(sizeof(*loop->l.list));
			loop->l.list[0] = Z_STRVAL_PP(sub_array);
			loop->cleanup = fetch_loop_cleanup;
			return;
		} else if ((Z_TYPE_PP(sub_array)) == IS_LONG) {
			convert_to_string_ex(sub_array);
			loop->len = 1;
			loop->l.list = xmalloc(sizeof(*loop->l.list));
			loop->l.list[0] = Z_STRVAL_PP(sub_array);
			loop->cleanup = fetch_loop_cleanup;
			return;
		} else if ((Z_TYPE_PP(sub_array)) != IS_ARRAY) {
			loop->len = 0;
			loop->l.list = NULL;
			loop->cleanup = NULL;
			return;
		}

		loop->len = zend_hash_num_elements(Z_ARRVAL_PP(sub_array));
		loop->l.list = xmalloc(loop->len * sizeof(*loop->l.list));
		for (i = 0; i < loop->len; i++) {
			if (zend_hash_index_find(Z_ARRVAL_PP(sub_array), i, (void **)&value) != FAILURE) {
				if (Z_TYPE_PP(value) == IS_STRING) {
					loop->l.list[i] = Z_STRVAL_PP(value);
				} else if (Z_TYPE_PP(value) == IS_LONG || Z_TYPE_PP(value) == IS_DOUBLE) {
					convert_to_string_ex(value);
					loop->l.list[i] = Z_STRVAL_PP(value);
				} else
					loop->l.list[i] = "";
			} else {
				log_printf(LOG_WARNING, "php_templates: fetch_loop: impossible condition. [%s] (%d/%d)", key, i, loop->len);
				loop->l.list[i] = "";
			}
		}
		loop->cleanup = fetch_loop_cleanup;
		return;
	}
	loop->len = 0;
	loop->l.list = NULL;
	loop->cleanup = NULL;
}

static void
free_keys_recurse(struct key_entry *ke) {
	if (ke == NULL)
		return;
	free_keys_recurse(RB_LEFT(ke, rbe));
	free_keys_recurse(RB_RIGHT(ke, rbe));
	free(ke);	
}

static void
free_keys(struct _simplevars_data *d) {
	free_keys_recurse(RB_ROOT(&d->keys));
	RB_INIT(&d->keys);
	d->nkeys = 0;
}

static void
init_keys(struct _simplevars_data *d) {
	Bucket *p;
	HashTable *ht = Z_ARRVAL_P(d->data);
	
	p = ht->pListHead;
	while (p) {
		struct key_entry *ke = xmalloc(sizeof(*ke));
		ke->key = p->arKey;
		RB_INSERT(key_tree, &d->keys, ke);
		d->nkeys++;
		p = p->pListNext;
	}
}

static int
glob_match(const char *glob, const char *key) {
	while (*glob == *key) {
		glob++;
		key++;
	}

	return *glob == '*';
}

static int
keyglob_len(struct bpapi_simplevars_chain *schain, const char *glob) {
	struct _simplevars_data *d = schain->data;
	struct key_entry *ent;
	struct key_entry s;
	int len = 0;

	if (RB_EMPTY(&d->keys)) {
		init_keys(d);
	}

	if (*glob == '*')
		return d->nkeys;

	s.key = glob;
	ent = RB_NFIND(key_tree, &d->keys, &s);

	while (ent && glob_match(glob, ent->key)) {
		len++;
		ent = RB_NEXT(key_tree, key_entry, ent);
	}

	return len;
}

static int
recurse_fetch_glob_loop(struct key_entry *ent, const char *key, const char ***dest, int pos) {
	if (ent && glob_match(key, ent->key)) {
		int res = recurse_fetch_glob_loop(RB_NEXT(key_tree, key_entry, ent), key, dest, pos + 1);
		(*dest)[pos] = ent->key;
		return res;
	}
	if (pos)
		*dest = xmalloc(pos * sizeof(**dest));
	else
		*dest = NULL;
	return pos;
}

static void
fetch_glob_cleanup(struct bpapi_loop_var *loop) {
	free(loop->l.list);
}

static void
fetch_glob_loop(struct bpapi_simplevars_chain *schain, const char *key, struct bpapi_loop_var *loop) {
	struct _simplevars_data *d = schain->data;
	struct key_entry s;

	if (RB_EMPTY(&d->keys)) {
		init_keys(d);
	}

	s.key = key;
	loop->len = recurse_fetch_glob_loop(RB_NFIND(key_tree, &d->keys, &s), key, &loop->l.list, 0);
	if (loop->len)
		loop->cleanup = fetch_glob_cleanup;
	else
		loop->cleanup = NULL;
}


#ifdef TODO
static void
init_var(struct bpapi_simplevars_chain *schain, const char *key, struct template_var *var) {
	zval **sub_array;

	if (zend_hash_find(Z_ARRVAL_P((zval*)((struct _data*)(api->data))->data), (char*)key, strlen(key) + 1, (void**)&sub_array) != FAILURE) {
		var->isdef = 1;
		var->rep = NULL;

		if ((Z_TYPE_PP(sub_array)) == IS_STRING)
			var->len = 1;
		else if ((Z_TYPE_PP(sub_array)) != IS_ARRAY)
			var->len = 0;
		else
			var->len = zend_hash_num_elements(Z_ARRVAL_PP(sub_array));
	} else {
		var->isdef = 0;
	}

	var->initialized = 1;
}
#endif

static zval *
phpvtree_find(zval *root, const char *sentinel, int argc, const char **argv) {
	zval **c = &root;
	const char *key;
	int res;
	int i;

	if (!root)
		return NULL;

	for (i = 0; i < argc && argv[i] && argv[i] != sentinel; i++) {
		char *dp;

		key = argv[i];

		if ((dp = strchr(key, '.'))) {
			char *dk = strdupa(key);
			
			dp = dk + (dp - key);
			key = dk;
		}

		while (1) {
			if ((*c)->type != IS_ARRAY) {
				return NULL;
			}

			if (dp)
				*dp++ = '\0';
			res = zend_hash_find(Z_ARRVAL_PP(c), (char *)key, strlen(key) + 1, (void**)&c);

			if (res != SUCCESS && is_numeric_string((char *)key, strlen(key), NULL, NULL, 0) == IS_LONG)
				res = zend_hash_index_find(Z_ARRVAL_PP(c), zend_atoi((char *)key, strlen(key)), (void**)&c);

			if (res != SUCCESS) {
				return NULL;
			}

			if (!dp)
				break;

			key = dp;
			dp = strchr(key, '.');
		}
	} 

	return *c;
}

static zval *
phpvtree_find_cc(struct bpapi_vtree_chain *vchain, enum bpcacheable *cc, const char *sentinel, int argc, const char **argv) {
	zval *vtree_cacheable = ((struct _vtree_data*)(vchain->data))->vtree_cacheable;
	zval *vtree_uncacheable = ((struct _vtree_data*)(vchain->data))->vtree_uncacheable;
	zval *vtree;

	if ((vtree = phpvtree_find(vtree_uncacheable, sentinel, argc, argv)))
		*cc = BPCACHE_CANT;
	else if ((vtree = phpvtree_find(vtree_cacheable, sentinel, argc, argv)))
		*cc = BPCACHE_CAN;
	else
		*cc = BPCACHE_UNKNOWN;

	return vtree;
}

static int
phpvtree_getlen(struct bpapi_vtree_chain *vchain, enum bpcacheable *cc, int argc, const char **argv) {
	zval *vtree = phpvtree_find_cc(vchain, cc, NULL, argc, argv);

	if (vtree) {
		switch (Z_TYPE_P(vtree)) {
		case IS_STRING:
		case IS_LONG:
		case IS_DOUBLE:
			return 0;
		case IS_ARRAY:
			return zend_hash_num_elements(Z_ARRVAL_P(vtree));
		/* XXX more types here */
		}
	}
	return 0;
}

static const char*
phpvtree_get(struct bpapi_vtree_chain *vchain, enum bpcacheable *cc, int argc, const char **argv) {
	zval *vtree = phpvtree_find_cc(vchain, cc, NULL, argc, argv);
	char buf[100];

	if (vtree) {
		if (vtree->type == IS_STRING) {
			return vtree->value.str.val;
		} else if (vtree->type == IS_LONG) {
			snprintf(buf, sizeof(buf), "%lu", vtree->value.lval);
			*cc = BPCACHE_CANT;
			return estrdup(buf);
		}
	}

	return NULL;
}

static int
phpvtree_haskey(struct bpapi_vtree_chain *vchain, enum bpcacheable *cc, int argc, const char **argv) {
	zval *vtree = phpvtree_find_cc(vchain, cc, NULL, argc, argv);

	return vtree != NULL;
}

static void
phpvtree_fetch_cleanup(struct bpapi_loop_var *loop) {
	int i;

	for (i = 0; i < loop->len; i++)
		free((char *)loop->l.list[i]);
	free(loop->l.list);
}

static void
phpvtree_fetch_keyvals_cleanup(struct vtree_keyvals *loop) {
	int i;

	for (i = 0; i < loop->len; i++)
		free((char *)loop->list[i].key);
	free(loop->list);
}

static void
phpvtree_fetch_nodes_cleanup(struct bpapi_loop_var *loop) {
	free(loop->l.vlist);
}

static void
phpvtree_zval_fetch_keys(zval *vtree, struct bpapi_loop_var *loop) {
	char *keyname;
	ulong index = 0;

	if (vtree && vtree->type == IS_ARRAY) {
		int i;
		loop->len = zend_hash_num_elements(Z_ARRVAL_P(vtree));
		loop->l.list = xmalloc(loop->len * sizeof (*loop->l.list));

		zend_hash_internal_pointer_reset(Z_ARRVAL_P(vtree));
		for (i = 0; i < loop->len; i++) {
			if (zend_hash_get_current_key(Z_ARRVAL_P(vtree), &keyname, &index, 0) == HASH_KEY_IS_LONG) {
				/* XXX can we convert to string and skip the alloc? */
				xasprintf((char **)&loop->l.list[i], "%lu", index);
			} else {
				loop->l.list[i] = xstrdup(keyname);
			}
			zend_hash_move_forward(Z_ARRVAL_P(vtree));
		}
		zend_hash_internal_pointer_reset(Z_ARRVAL_P(vtree));
		loop->cleanup = phpvtree_fetch_cleanup;
		return;
	}

	loop->len = 0;
	loop->l.list = NULL;
	loop->cleanup = NULL;
}

static void
phpvtree_fetch_keys(struct bpapi_vtree_chain *vchain, struct bpapi_loop_var *loop, enum bpcacheable *cc, int argc, const char **argv) {
	zval *vtree = phpvtree_find_cc(vchain, cc, NULL, argc, argv);
	
	phpvtree_zval_fetch_keys(vtree, loop);
}

static void
phpvtree_zval_fetch_values(zval *vtree, struct bpapi_loop_var *loop, int argc, const char **argv) {
	zval **value;
	int argoff;

	for (argoff = 0; argv[argoff] != VTREE_LOOP && argv[argoff] != NULL && argoff < argc; argoff++)
		;
	argoff++;

	if (vtree) {
		if (vtree->type == IS_ARRAY) {
			int i;

			loop->len = zend_hash_num_elements(Z_ARRVAL_P(vtree));

			if (!loop->len) {
				loop->l.list = NULL;
				loop->cleanup = NULL;
				return;
			}

			loop->l.list = xmalloc(loop->len * sizeof(*loop->l.list));
			zend_hash_internal_pointer_reset(Z_ARRVAL_P(vtree));
			for (i = 0; i < loop->len; i++) {
				zval *v;

				zend_hash_get_current_data(Z_ARRVAL_P(vtree), (void**)&value);

				loop->l.list[i] = NULL;

				if ((*value)->type == IS_STRING) {
					if (argoff >= argc)
						loop->l.list[i] = xstrdup((*value)->value.str.val);
				} else if ((*value)->type == IS_LONG) {
					if (argoff >= argc)
						xasprintf((char **)&loop->l.list[i], "%lu", (*value)->value.lval);
				} else if ((*value)->type == IS_ARRAY) {
					v = phpvtree_find(*value, NULL, argc - argoff, argv + argoff);

					if (v) {
						if (v->type == IS_STRING) {
							loop->l.list[i] = xstrdup(v->value.str.val);
						} else if (v->type == IS_LONG) {
							xasprintf((char **)&loop->l.list[i], "%lu", v->value.lval);
						}
					}
				}

				if (!loop->l.list[i])
					loop->l.list[i] = xstrdup("");
				zend_hash_move_forward(Z_ARRVAL_P(vtree));
			}
			zend_hash_internal_pointer_reset(Z_ARRVAL_P(vtree));
			loop->cleanup = phpvtree_fetch_cleanup;
			return;
		}
	}

	loop->len = 0;
	loop->l.list = NULL;
	loop->cleanup = NULL;
}

static void
phpvtree_fetch_values(struct bpapi_vtree_chain *vchain, struct bpapi_loop_var *loop, enum bpcacheable *cc, int argc, const char **argv) {
	zval *vtree;

	vtree = phpvtree_find_cc(vchain, cc, VTREE_LOOP, argc, argv);

	phpvtree_zval_fetch_values(vtree, loop, argc, argv);
}

static void
phpvtree_zval_fetch_keys_by_value(zval *vtree, struct bpapi_loop_var *loop, const char *needle, int argc, const char **argv) {
	zval **value;
	int argoff;

	for (argoff = 0; argv[argoff] != VTREE_LOOP && argv[argoff] != NULL; argoff++)
		;
	argoff++;

	if (vtree) {
		int len;
		int i;

		if (vtree->type != IS_ARRAY || (len = zend_hash_num_elements(Z_ARRVAL_P(vtree))) == 0) {
			loop->len = 0;
			loop->l.list = NULL;
			loop->cleanup = NULL;
			return;
		}

		loop->l.list = xmalloc(len * sizeof(*loop->l.list));
		loop->len = 0;

		zend_hash_internal_pointer_reset(Z_ARRVAL_P(vtree));
		for (i = 0; i < len; i++) {
			zval *v;
			char *keyname;
			ulong index;

			zend_hash_get_current_data(Z_ARRVAL_P(vtree), (void **)&value);
			if ((*value)->type == IS_STRING) {
				if (argoff >= argc || strcmp((*value)->value.str.val, needle) != 0)
					goto next;
			} else if ((*value)->type == IS_LONG) {
				if (argoff >= argc || atoi(needle) != (*value)->value.lval)
					goto next;
			} else if ((*value)->type == IS_ARRAY) {
				v = phpvtree_find(*value, NULL, argc - argoff, argv + argoff);

				if (v) {
					if (v->type == IS_STRING) {
						if (strcmp(v->value.str.val, needle) != 0)
							goto next;
					} else if (v->type == IS_LONG) {
						if (atoi(needle) != v->value.lval)
							goto next;
					} else
						goto next;
				} else {
					goto next;
				}
			} else {
				goto next;
			}
			if (zend_hash_get_current_key(Z_ARRVAL_P(vtree), &keyname, &index, 0) == HASH_KEY_IS_LONG) {
				/* XXX can we convert to string and skip the alloc? */
				xasprintf((char **)&loop->l.list[loop->len++], "%lu", index);
			} else {
				loop->l.list[loop->len++] = xstrdup(keyname);
			}
next:
			zend_hash_move_forward(Z_ARRVAL_P(vtree));
		}
		zend_hash_internal_pointer_reset(Z_ARRVAL_P(vtree));
		loop->cleanup = phpvtree_fetch_cleanup;
		return;
	}

	loop->len = 0;
	loop->l.list = NULL;
	loop->cleanup = NULL;
}

static void
phpvtree_fetch_keys_by_value(struct bpapi_vtree_chain *vchain, struct bpapi_loop_var *loop, enum bpcacheable *cc, const char *needle, int argc, const char **argv) {
	zval *vtree;

	vtree = phpvtree_find_cc(vchain, cc, VTREE_LOOP, argc, argv);

	phpvtree_zval_fetch_keys_by_value(vtree, loop, needle, argc, argv);
}

static void
phpvtree_fetch_nodes(struct bpapi_vtree_chain *vchain, struct bpapi_loop_var *loop, enum bpcacheable *cc, int argc, const char **argv) {
	zval *vtree = phpvtree_find_cc(vchain, cc, NULL, argc, argv);
	
	phpvtree_zval_fetch_nodes(vtree, loop);
}

static void
phpvtree_fetch_keys_and_values(struct bpapi_vtree_chain *vchain, struct vtree_keyvals *loop, enum bpcacheable *cc, int argc, const char **argv) {
	zval *vtree;

	vtree = phpvtree_find_cc(vchain, cc, VTREE_LOOP, argc, argv);

	phpvtree_zval_fetch_keys_and_values(vtree, loop, argc, argv);
}


static const char*
phpvtree_node_get(struct bpapi_vtree_chain *vchain, enum bpcacheable *cc, int argc, const char **argv) {
	char buf[100];
	zval *vtree = phpvtree_find(vchain->data, NULL, argc, argv);

	if (vtree) {
		if (vtree->type == IS_STRING) {
			return vtree->value.str.val;
		} else if (vtree->type == IS_LONG) {
			snprintf(buf, sizeof(buf), "%lu", vtree->value.lval);
			return estrdup(buf);
		}
	}

	return NULL;
}

static int
phpvtree_node_getlen(struct bpapi_vtree_chain *vchain, enum bpcacheable *cc, int argc, const char **argv) {
	zval *vtree = phpvtree_find(vchain->data, NULL, argc, argv);

	if (vtree) {
		switch (Z_TYPE_P(vtree)) {
		case IS_STRING:
		case IS_LONG:
			return 1;
		case IS_ARRAY:
			return zend_hash_num_elements(Z_ARRVAL_P(vtree));
		/* XXX more types here */
		}
	}
	return 0;
}

static int
phpvtree_node_haskey(struct bpapi_vtree_chain *vchain, enum bpcacheable *cc, int argc, const char **argv) {
	zval *vtree = phpvtree_find(vchain->data, NULL, argc, argv);

	return vtree != NULL;
}

static void
phpvtree_node_fetch_keys(struct bpapi_vtree_chain *vchain, struct bpapi_loop_var *loop, enum bpcacheable *cc, int argc, const char **argv) {
	zval *vtree = phpvtree_find(vchain->data, NULL, argc, argv);
	
	phpvtree_zval_fetch_keys(vtree, loop);
}

static void
phpvtree_node_fetch_values(struct bpapi_vtree_chain *vchain, struct bpapi_loop_var *loop, enum bpcacheable *cc, int argc, const char **argv) {
	zval *vtree;

	vtree = phpvtree_find(vchain->data, VTREE_LOOP, argc, argv);
	
	phpvtree_zval_fetch_values(vtree, loop, argc, argv);
}

static void
phpvtree_node_fetch_keys_by_value(struct bpapi_vtree_chain *vchain, struct bpapi_loop_var *loop, enum bpcacheable *cc, const char *value, int argc, const char **argv) {
	zval *vtree;

	vtree = phpvtree_find(vchain->data, VTREE_LOOP, argc, argv);
	
	phpvtree_zval_fetch_keys_by_value(vtree, loop, value, argc, argv);
}

static void
phpvtree_node_fetch_nodes(struct bpapi_vtree_chain *vchain, struct bpapi_loop_var *loop, enum bpcacheable *cc, int argc, const char **argv) {
	zval *vtree = phpvtree_find(vchain->data, NULL, argc, argv);
	
	phpvtree_zval_fetch_nodes(vtree, loop);
}

static void
phpvtree_node_fetch_keys_and_values(struct bpapi_vtree_chain *vchain, struct vtree_keyvals *loop, enum bpcacheable *cc, int argc, const char **argv) {
	zval *vtree;

	vtree = phpvtree_find(vchain->data, VTREE_LOOP, argc, argv);
	
	phpvtree_zval_fetch_keys_and_values(vtree, loop, argc, argv);
}

/* XXX move this to a common library */
const struct bpapi_vtree php_vtree_node EXPORTED = {
	phpvtree_node_getlen,
	phpvtree_node_get,
	phpvtree_node_haskey,
	phpvtree_node_fetch_keys,
	phpvtree_node_fetch_values,
	phpvtree_node_fetch_keys_by_value,
	phpvtree_node_getnode,
	phpvtree_node_fetch_nodes,
	phpvtree_node_fetch_keys_and_values
};

static struct bpapi_vtree_chain *
phpvtree_getnode(struct bpapi_vtree_chain *vchain, enum bpcacheable *cc, struct bpapi_vtree_chain *dst, int argc, const char **argv) {
	zval *vtree = phpvtree_find_cc(vchain, cc, NULL, argc, argv);

	if (vtree) {
		dst->data = vtree;
		dst->fun = &php_vtree_node;
		dst->next = NULL;
		return dst;
	}
	return NULL;
}

static struct bpapi_vtree_chain *
phpvtree_node_getnode(struct bpapi_vtree_chain *vchain, enum bpcacheable *cc, struct bpapi_vtree_chain *dst, int argc, const char **argv) {
	zval *vtree = phpvtree_find(vchain->data, NULL, argc, argv);

	if (vtree) {
		dst->data = vtree;
		dst->fun = &php_vtree_node;
		dst->next = NULL;
		return dst;
	}
	return NULL;
}

static void
phpvtree_zval_fetch_nodes(zval *vtree, struct bpapi_loop_var *loop) {
	if (vtree && vtree->type == IS_ARRAY) {
		int i;
		loop->len = zend_hash_num_elements(Z_ARRVAL_P(vtree));
		loop->l.vlist = xmalloc(loop->len * sizeof (*loop->l.vlist));

		zend_hash_internal_pointer_reset(Z_ARRVAL_P(vtree));
		for (i = 0; i < loop->len; i++) {
			zval **n;

			zend_hash_get_current_data(Z_ARRVAL_P(vtree), (void**)&n);
			loop->l.vlist[i].data = *n;
			loop->l.vlist[i].fun = &php_vtree_node;
			loop->l.vlist[i].next = NULL;
			zend_hash_move_forward(Z_ARRVAL_P(vtree));
		}
		zend_hash_internal_pointer_reset(Z_ARRVAL_P(vtree));
		loop->cleanup = phpvtree_fetch_nodes_cleanup;
		return;
	}

	loop->len = 0;
	loop->l.list = NULL;
	loop->cleanup = NULL;
}

static void
phpvtree_zval_fetch_keys_and_values(zval *vtree, struct vtree_keyvals *loop, int argc, const char **argv) {
	zval **value;
	int argoff;

	for (argoff = 0; argv[argoff] != VTREE_LOOP && argv[argoff] != NULL && argoff < argc; argoff++)
		;
	argoff++;

	if (vtree && vtree->type == IS_ARRAY) {
		int i;

		loop->len = zend_hash_num_elements(Z_ARRVAL_P(vtree));

		zend_hash_internal_pointer_reset(Z_ARRVAL_P(vtree));
		loop->type = vktList;
		for (i = 0; i < loop->len; i++) {
			char *keyname;
			ulong index = 0;

			if (zend_hash_get_current_key(Z_ARRVAL_P(vtree), &keyname, &index, 0) != HASH_KEY_IS_LONG) {
				loop->type = vktUnknown; /* Keep as unknown in case they're numeric strings */
				break;
			}
			/* Be extra careful to not discard meaningful numeric keys */
			if (index != (ulong)i) {
				loop->type = vktUnknown;
				break;
			}
			zend_hash_move_forward(Z_ARRVAL_P(vtree));
		}

		if (!loop->len) {
			loop->list = NULL;
			loop->cleanup = NULL;
			return;
		}

		loop->list = xmalloc(loop->len * sizeof (*loop->list));
		zend_hash_internal_pointer_reset(Z_ARRVAL_P(vtree));
		for (i = 0; i < loop->len; i++) {
			if (loop->type == vktList) {
				loop->list[i].key = NULL;
			} else {
				char *keyname;
				ulong index = 0;

				if (zend_hash_get_current_key(Z_ARRVAL_P(vtree), &keyname, &index, 0) == HASH_KEY_IS_LONG) {
					/* XXX can we convert to string and skip the alloc? */
					xasprintf((char **)&loop->list[i].key, "%lu", index);
				} else {
					loop->list[i].key = xstrdup(keyname);
				}
			}
			zend_hash_get_current_data(Z_ARRVAL_P(vtree), (void**)&value);

			loop->list[i].type = vkvNone;

			if ((*value)->type == IS_STRING) {
				if (argoff >= argc) {
					loop->list[i].type = vkvValue;
					loop->list[i].v.value = xstrdup((*value)->value.str.val);
				}
			} else if ((*value)->type == IS_LONG) {
				if (argoff >=argc) {
					loop->list[i].type = vkvValue;
					xasprintf((char **)&loop->list[i].v.value, "%lu", (*value)->value.lval);
				}
			} else if ((*value)->type == IS_ARRAY) {
				zval *v = phpvtree_find(*value, NULL, argc - argoff, argv + argoff);
				if (v) {
					if (v->type == IS_STRING) {
						loop->list[i].type = vkvValue;
						loop->list[i].v.value = xstrdup(v->value.str.val);
					} else if (v->type == IS_LONG) {
						loop->list[i].type = vkvValue;
						xasprintf((char **)&loop->list[i].v.value, "%lu", v->value.lval);
					} else if (v->type == IS_ARRAY) {
						loop->list[i].type = vkvNode;
						loop->list[i].v.node.data = v;
						loop->list[i].v.node.fun = &php_vtree_node;
						loop->list[i].v.node.next = NULL;
					}
				}
			}

			zend_hash_move_forward(Z_ARRVAL_P(vtree));
		}
		zend_hash_internal_pointer_reset(Z_ARRVAL_P(vtree));
		loop->cleanup = phpvtree_fetch_keyvals_cleanup;
		return;
	}

	loop->len = 0;
	loop->list = NULL;
	loop->cleanup = NULL;
}


const struct bpapi_output php_output = {
	outstring_fmt,
	outstring_raw,
	NULL,
	NULL
};
const struct bpapi_simplevars php_simplevars = {
	has_key,
	length,
	get_element,
	fetch_loop,
	keyglob_len,
	fetch_glob_loop,
	insert_val,
	NULL,
	NULL,
	remove_val,
	NULL,
	NULL
};
const struct bpapi_vtree php_vtree = {
	phpvtree_getlen,
	phpvtree_get,
	phpvtree_haskey,
	phpvtree_fetch_keys,
	phpvtree_fetch_values,
	phpvtree_fetch_keys_by_value,
	phpvtree_getnode,
	phpvtree_fetch_nodes,
	phpvtree_fetch_keys_and_values
};

static char *
get_option(zval *options, const char *key) {
	zval **value = NULL;
	char *retval = NULL;
	/* PHP 5.1 does not use const, remove key cast when no longer supported. */
	if (zend_hash_find(Z_ARRVAL_P(options), (char*)key, strlen(key)+1, (void**)&value) == SUCCESS) {
		if (Z_TYPE_PP(value) == IS_STRING) {
			retval = estrdup(Z_STRVAL_PP(value));
		}
	}
	return retval;
}

static void
fs_remote_addr(struct bconf_node **fs_node) {
	zval **zserver;
	zval **zremote_addr;

	if (zend_hash_find(&EG(symbol_table), "_SERVER", sizeof("_SERVER"), (void **) &zserver) == SUCCESS) {
		if (Z_TYPE_PP(zserver) == IS_ARRAY &&
		    zend_hash_find(Z_ARRVAL_PP(zserver), "REMOTE_ADDR", sizeof("REMOTE_ADDR"), (void **) &zremote_addr) == SUCCESS) {
			filter_state_bconf_set(fs_node, "remote_addr", Z_STRVAL_PP(zremote_addr), NULL, FS_SESSION);
		}
	}
}

PHP_FUNCTION(call_template) {
	zval *arr;
	zval *vtree_cacheable;
	zval *vtree_uncacheable;

	zval *options = NULL;
	char *lang = NULL;
	char *country = NULL;

	char *template_name;
	int template_name_len;

	struct _simplevars_data sd;
	struct _vtree_data vd;
	struct bpapi_vtree_chain vt = { &php_vtree, &vd };
	struct bconf_node *filter_session = NULL;
	struct bpapi_vtree_chain gfsvt;
	struct shadow_vtree filter_state_global_shadow;
	struct shadow_vtree filter_session_shadow;
	struct bpapi php_api = {
		{ &php_output, NULL },
		{ &php_simplevars, &sd, NULL, 1 },
		{ NULL },
		filter_state_bconf
	};

	if (ZEND_NUM_ARGS() == 4 || ZEND_NUM_ARGS() == 5) {
		if (zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC,
					  "aaas|a",
					  &arr, &vtree_uncacheable, &vtree_cacheable, &template_name, &template_name_len, &options) == FAILURE)
		RETURN_NULL();

	} else
		WRONG_PARAM_COUNT;

	bconf_vtree(&filter_state_global_shadow.vtree, filter_state_global);
	shadow_vtree_init(&gfsvt, &filter_state_global_shadow, &vt);

	filter_state_bconf_init(&filter_session, FS_SESSION);
	bconf_vtree(&filter_session_shadow.vtree, filter_session);
	shadow_vtree_init(&php_api.vchain, &filter_session_shadow, &gfsvt);

	/* XXX - This is only needed for testing through template_test. */
	filter_state_bconf_set(&filter_session, "define", (void *)2, NULL, FS_SESSION);
	fs_remote_addr(&filter_session);

	vd.vtree_cacheable = vtree_cacheable;
	vd.vtree_uncacheable = vtree_uncacheable;
	sd.data = arr;
	sd.nkeys = 0;
	RB_INIT(&sd.keys);

	if (options) {
		lang = get_option(options, "lang");
		country = get_option(options, "country");
	}

	if (php_templates_start_local)
		php_templates_start_local(&php_api, template_name, lang);

	int res = 0;
	if (lang && *lang) {
		res = call_template_lang(&php_api, template_name, lang, country);
	} else {
		res = call_template_auto(&php_api, template_name);
	}

	if (!res) {
		char *msg;
		ALLOCA_PRINTF(res, msg, "failed to render template: %s (lang=%s, country=%s)", template_name, lang, country);
		php_log_err(msg);
	}

	if (php_templates_end_local)
		php_templates_end_local(&php_api, template_name, lang);

	filter_state_bconf_free(&filter_session);

	if (lang)
		efree(lang);
	if (country)
		efree(country);

	if (sd.nkeys)
		free_keys(&sd);

	RETURN_TRUE;
}

PHP_FUNCTION(flush_template_cache) {
	if (ZEND_NUM_ARGS() >= 1) {
                char *template_name;
                int template_name_len;

                if (zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC,
                                          "s", &template_name, &template_name_len) == FAILURE) {
                        RETURN_FALSE;
                }
                flush_template_cache(template_name);
        } else
                flush_template_cache(NULL);
        RETURN_TRUE;
}

static const struct tpvar *
errorlog(struct bpapi *api, struct tpvar *dst, enum bpcacheable *cc, int argc, const struct tpvar *argv[])
{
	char *msg;
	int res;
	char *fa0 = NULL, *fa1 = NULL;

        switch (argc) {
        case 0:
		msg = "errorlog() called with no args";
                break;
        case 1:
		ALLOCA_PRINTF(res, msg, "Error: %s", tpvar_str(argv[0], &fa0));
                break;
        case 2:
		ALLOCA_PRINTF(res, msg, "Error: %s : %s", tpvar_str(argv[0], &fa0), tpvar_str(argv[1], &fa1));
                break;
        default:
		ALLOCA_PRINTF(res, msg, "errorlog() called with %d args", argc);
                break;
        }
	php_log_err(msg);
	*cc = BPCACHE_CANT;
	free(fa0);
	free(fa1);
	return NULL;
}
ADD_TEMPLATE_FUNCTION(errorlog);

PHP_TEMPLATE_FUNCTION(set_expire);
PHP_TEMPLATE_FUNCTION(redirect);
PHP_TEMPLATE_FUNCTION(has_cookie);
PHP_TEMPLATE_FUNCTION(get_cookie);
PHP_TEMPLATE_FUNCTION(clear_cookie);
PHP_TEMPLATE_FUNCTION(mime_type);

PHP_TEMPLATE_FUNCTION(template_statuscode);

PHP_OUTPUT_FILTER(set_cookie);
PHP_OUTPUT_FILTER(set_secure_cookie);
PHP_OUTPUT_FILTER(set_cookie_domain);

PHP_OUTPUT_FILTER(add_header);
