#include <httpd.h>
#include <http_log.h>
#include <http_config.h>
#include <http_protocol.h>
#include <apr_strings.h>
#include <log_event.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <ctype.h>
#include <ap_mpm.h>

#include "cacheapi.h"
#include "query_string.h"
#include "mod_templates.h"
#include "ctemplates.h"
#include "transapi.h"
#include "cookies.h"
#include "accept_lang.h"
#include "strl.h"
#include "logging.h"
#include "remote_addr.h"
#include "mod_bconf_extern.h"
#include "filter_state_ucnv.h"

static struct template_alias_rec*
alias_rec_create(apr_pool_t *pool, const char* tmpl, const char *tmpl_idx, const char *alias_idx) {
	struct template_alias_rec *ar = apr_palloc(pool, sizeof(struct template_alias_rec));
	if (ar) {
		ar->tmpl_idx = tmpl_idx;
		ar->alias_idx = alias_idx;
		ar->tmpl = tmpl;
	}
	return ar;
}

static int
add_template(apr_pool_t *pool, struct templates_conf *conf, struct bconf_node *node) {
	const char *name = bconf_get_string(node, "name");
	struct bconf_node *id_prefixes = bconf_get(node, "id_prefix");
	struct bconf_node *aliases = bconf_get(node, "alias");
	struct bconf_node *path_prefixes = bconf_get(node, "path_prefix");
	int i;

	if (name) {
		if (apr_table_get(conf->templates, name)) {
			/* We can only do template-name aliasing if we have aliases defined */
			if (!bconf_get_int(node, "allow_name_aliasing") || aliases == NULL) {
				ap_log_error(APLOG_MARK, APLOG_ERR, OK, NULL, "Duplicate template name in bconf for template key %s: %s", bconf_key(node), name);
				return HTTP_INTERNAL_SERVER_ERROR;
			}
		} else {
			apr_table_set(conf->templates, name, bconf_key(node));
		}

		if (path_prefixes) {
			for (i = 0; i < bconf_count(path_prefixes); i++) {
				apr_table_set(conf->template_path_prefixes, bconf_value(bconf_byindex(path_prefixes, i)), name);
			}
		}

		if (id_prefixes) {
			for (i = 0; i < bconf_count(id_prefixes); i++) {
				const char *id_prefix = bconf_value(bconf_byindex(id_prefixes, i));
				if (id_prefix)
					apr_table_set(conf->template_id_prefixes, id_prefix, name);
			}
		}

		/* Create a map of aliases to their template, the template and alias node keys.

		   We need to store the template node key (idx) so that we can map back from the alias
	  	   in the get_template_name() function. This is what allows us to use the same
		   template name under different template definitions. */
		if (aliases) {
			for (i = 0; i < bconf_count(aliases); i++) {
				const char *alias = bconf_value(bconf_byindex(aliases, i));
				if (alias) {
					struct template_alias_rec *ar = alias_rec_create(pool, name, bconf_key(node), bconf_key(bconf_byindex(aliases, i)));
					if (ar)
						apr_hash_set(conf->template_aliases, alias, APR_HASH_KEY_STRING, ar);
				}
			}
		}
	}
	return OK;
}

static void *
create_templates_config(apr_pool_t *pool, server_rec *s) {
	return apr_pcalloc(pool, sizeof(struct templates_conf));
}

static const char *
command_set_templates(cmd_parms *cmd, void *mconfig, int argc, char * const argv[]) {
	struct templates_conf *conf = ap_get_module_config(cmd->server->module_config, &templates_module);
	struct bconf_node *filt_root;
	struct bconf_node **frp;
	struct bconf_node *root;
	struct bconf_node *node;
	const char *tmpl_path;
	int i = 0;

	if (argc == 0)
		tmpl_path = "mod_templates";
	else
		tmpl_path = argv[0];

	if ((root = mod_bconf_get_root(cmd->server)) == NULL) {
		return "mod_templates - Failed to get bconf root.";
	}

	if ((frp = mod_bconf_get_filterp(cmd->server)) == NULL) {
		return "mod_templates - Failed to get filter root.";
	}
	filt_root = *frp;

	conf->root = root;
	conf->tmpl_conf = bconf_get(root, tmpl_path);
	conf->tmpl_list = bconf_get(root, bconf_get_string(conf->tmpl_conf, "templates_list"));
	bconf_vtree(&conf->vbconf.vtree, root);
	bconf_vtree(&conf->vfconf, filt_root);
	shadow_vtree_init(&conf->vt, &conf->vbconf, &conf->vfconf);

	conf->templates = apr_table_make(cmd->pool, bconf_count(conf->tmpl_list));
	conf->template_id_prefixes = apr_table_make(cmd->pool, 10);
	conf->template_path_prefixes = apr_table_make(cmd->pool, 10);
	conf->template_aliases = apr_hash_make(cmd->pool);

	const char* log_level = bconf_get_string(conf->tmpl_conf, "log_level");
	if (!log_level || !*log_level) {
		log_level = "info";
	}
	/* The lib/common code is using the stardard logging functionality */
	log_setup("mod_templates", log_level);

	if (bconf_get_int(conf->tmpl_conf, "xerr_abort"))
		set_xerr_abort(1);

	while ((node = bconf_byindex(conf->tmpl_list, i++))) {
		if (add_template(cmd->pool, conf, node) != OK) {
			return apr_psprintf(cmd->pool, "template node %s failed", bconf_key(node));
		}
	}

	strlcpy(conf->hostname, "nohostname", sizeof(conf->hostname));
	gethostname(conf->hostname, sizeof(conf->hostname));

	/* XXX - maybe this needs to be done post-config? */
	cache_init_pools(&conf->session, &conf->vbconf.vtree);

	if (setup_templates_local) {
		if (setup_templates_local(cmd->pool, conf, root) != OK)
			return "setup_templates_local failed";
	}

	return NULL;
}

static apr_status_t
cleanup_thread_data(void *v) {
	struct templates_conf *conf = v;
	int i;

	for (i = 0; i < conf->num_threads; i++) {
		filter_state_bconf_free(&conf->thread_data[i].filter_state);
	}
	return APR_SUCCESS;
}

static void
templates_child_init(apr_pool_t *pool, server_rec *s) {
	for ( ; s ; s = s->next) {
		struct templates_conf *conf = ap_get_module_config(s->module_config, &templates_module);
		int threaded;
		int i;

		if (!conf)
			continue;

		ap_mpm_query(AP_MPMQ_IS_THREADED, &threaded);
		if (threaded)
			ap_mpm_query(AP_MPMQ_MAX_THREADS, &conf->num_threads);
		else
			conf->num_threads = 1;

		conf->thread_data = apr_pcalloc(pool, sizeof(struct templates_thread_conf) * conf->num_threads);
		for (i = 0; i < conf->num_threads; i++) {
			filter_state_bconf_init(&conf->thread_data[i].filter_state, FS_THREAD);
		}
		apr_pool_cleanup_register(pool, conf, cleanup_thread_data, apr_pool_cleanup_null);
	}
}

/*
 *  Extract the requested template name and other information from from the URI
 *  At this point it would probably make sense to setup a struct rather than add more arguments.
 */

static const char*
default_get_template_name(struct request_rec *r, struct rule_rec *rule, const char **tmpl_idx, const char **alias_idx, unsigned long *id, char *id_prefix, size_t id_prefix_sz, const char **path_info) {
	const char *sp;
	const char *tmpl;
	struct templates_conf *conf = ap_get_module_config(r->server->module_config, &templates_module);
	const char *host = r->server->server_hostname;
	const char *prefix;
	const char *pinfo;
	int i;

	*path_info = NULL;

	/* TODO: This is a slightly odd rule. Consider moving check to rules that actually care */
	if (conf->template_aliases == NULL)
		RETURN_TEMPLATE_RULE_TAG(NULL, DEFAULT_RULE_LOGLEVEL, "<no aliases available>");

	/*
	 * If the URI exactly matches an alias, we have a match.
	 *
	 * By returning the tmpl_idx associated with the alias, we're freeing ourself to be able
	 * to use the same template-name for different template definitions, as long as we use aliases.
	 */
	struct template_alias_rec *ar = apr_hash_get(conf->template_aliases, r->uri, APR_HASH_KEY_STRING);
	if (ar) {
		if (alias_idx)
			*alias_idx = ar->alias_idx;
		if (tmpl_idx)
			*tmpl_idx = ar->tmpl_idx;
		RETURN_TEMPLATE_RULE_TAG(ar->tmpl, DEFAULT_RULE_LOGLEVEL, "Exact alias match");
	}

	pinfo = r->uri + strlen(r->uri) - 1;
	for (i = 0 ; i < 10 ; i++) {
		const char *k;

		while (pinfo > r->uri && *pinfo != '/')
			pinfo--;
		if (pinfo <= r->uri)
			break;

		k = apr_pstrndup(r->pool, r->uri, pinfo - r->uri);
		ar = apr_hash_get(conf->template_aliases, k, APR_HASH_KEY_STRING);
		if (ar) {
			if (path_info)
				*path_info = pinfo;
			if (alias_idx)
				*alias_idx = ar->alias_idx;
			RETURN_TEMPLATE_RULE_TAG(ar->tmpl, DEFAULT_RULE_LOGLEVEL, "sub-path alias match");
		}
		--pinfo;
	}

	/*
	 * If the URI starts with "/templates/" the rest is a literal name of
	 * a template. E.g. "/template/foo.bar/blahonga.txt" will always go for the template
	 * name "foo.bar/blahonga.txt"
	 */
	if (strncmp(r->uri, "/templates/", sizeof("/templates/") - 1) == 0) {
		tmpl = apr_psprintf(r->pool, "%s", r->uri + sizeof("/templates/") - 1);
		RETURN_TEMPLATE_RULE_TAG(tmpl, DEFAULT_RULE_LOGLEVEL, "/templates/ match");
	}

	/* Template root */
	/* XXX deprecate this in favor of aliases. */
	if (!(prefix = bconf_value(bconf_vget (conf->tmpl_conf, "root", host, NULL)))) {
		prefix = bconf_get_string(conf->tmpl_conf, "default.root") ?: "docs";
	}

	/*
	 * Match '/prefix/some/path/whatever_you_want_<list_id>.htm'
	 * Typically used to go from semantic url to the ad view page.
	 *
	 * If matched, %id will be set to the id, and id_prefix the matched prefix.
	*/
	sp = strchr(r->uri + 1, '/');
	if (sp) {
		const char *sep = bconf_get_string(conf->tmpl_conf, "id_prefix.separator") ?: "_";
		const char *id_start, *id_end;
		unsigned long n;
		char *ep;

		char *uriendptr = r->uri + strlen(r->uri);

		/* Support semantic url by checking for id after last separator */
		const char *uc = strrchr(sp + 1, *sep);

		if (bconf_get_int(conf->tmpl_conf, "id_prefix.before")) {
			if (!uc)
				uc = uriendptr;
			id_start = sp + 1;
			id_end = uc;
		} else {
			if (!uc)
				uc = sp;
			id_start = uc + 1;
			id_end = uriendptr;
		}

		n = strtol(id_start, &ep, 10);
		if (n > 0 && n != LONG_MAX && ep <= id_end) {
			size_t l = sp - r->uri - 1;

			if (l < id_prefix_sz) {
				memcpy(id_prefix, r->uri + 1, l);
				id_prefix[l] = '\0';
				if ((tmpl = apr_table_get(conf->template_id_prefixes, id_prefix))) {
					*id = n; 
					RETURN_TEMPLATE_RULE_TAG(tmpl, DEFAULT_RULE_LOGLEVEL, "id_prefix match");
				}
			}
		}
	}

	/*
	 * Path prefix matches are next. If the URI is "/foo/bar.blahonga/tratt"
	 * and there's a path prefix "foo", we will match and let the template
	 * itself deal with parsing the rest of the URI.
	 *
	 * This happens after id_prefix so that id_prefix (which will only match
	 * if there's an id in the url) can have precedence. This allows id_prefix
	 * and path_prefix to share a set of prefixes, yet map to the correct template.
	 */

	/* If there's no slash, use the whole uri */
	if (!sp)
		sp = r->uri + strlen(r->uri);

	char *path = apr_psprintf(r->pool, "%.*s", (int)(sp - r->uri - 1), r->uri + 1);
	if ((tmpl = apr_table_get(conf->template_path_prefixes, path)) != NULL) {
	 	RETURN_TEMPLATE_RULE_TAG(tmpl, DEFAULT_RULE_LOGLEVEL, "path_prefix match");
	}

	RETURN_TEMPLATE_RULE_TAG(NULL, 1, "<no rule matched>");
}

/* setup default get_template_name, can override this in local */
const char *(*mod_template_get_template_name)(struct request_rec *r, struct rule_rec *rule, const char **tmpl_idx, const char **alias_idx, unsigned long *id, char *id_prefix, size_t id_prefix_sz, const char **path_info) = default_get_template_name;

void
template_set(struct template_call *call, const char * key, const char * value)
{
	const char *keyv[] = {"req", key};

	bconf_add_datav(&call->filter_bconf, 2, keyv, value, BCONF_DUP);
	if (!bconf_get_int(call->conf->tmpl_conf, "no_simplevars"))
		bpapi_insert(&call->ba, key, value);
}

void
template_set_prefixed(struct template_call *call, const char *prefix, const char * key, const char * value)
{
	if (prefix) {
		const char *keyv[] = {"req", prefix, key};

		bconf_add_datav(&call->filter_bconf, 3, keyv, value, BCONF_DUP);
		if (!bconf_get_int(call->conf->tmpl_conf, "no_simplevars"))
			bpapi_insert(&call->ba, key, value);
	} else {
		template_set(call, key, value);
	}
}

void
template_set_int(struct template_call *call, const char * key, const int value)
{
	const char *keyv[] = {"req", key};
	char buf2[24];

	snprintf(buf2, sizeof(buf2), "%d", value);
	bconf_add_datav(&call->filter_bconf, 2, keyv, buf2, BCONF_DUP);
	if (!bconf_get_int(call->conf->tmpl_conf, "no_simplevars"))
		bpapi_insert(&call->ba, key, buf2);
}

void
template_set_printf(struct template_call *call, const char * key, const char *fmt, ...) {
	char *buf;
	int len;

	ALLOCA_VPRINTF(len, buf, fmt);
	template_set(call, key, buf);
}

void
template_set_hex(struct template_call *call, const char *key, const void *data, size_t len) {
	char hexbuf[len * 2 + 1];
	unsigned int i;
	const char hexchars[] = "0123456789ABCDEF";
	const unsigned char *dp = data;

	for (i = 0 ; i < len ; i++) {
		hexbuf[i * 2] = hexchars[(dp[i] >> 4) & 0xF];
		hexbuf[i * 2 + 1] = hexchars[dp[i] & 0xF];
	}
	hexbuf[i * 2] = '\0';
	template_set(call, key, hexbuf);
}


void
add_template_var(struct bconf_node *root, struct bconf_node *node, struct template_call *call, const char* keyname, const char *varname, const char *suffix) {
	struct bconf_node *key;
	
	if (!suffix)
		suffix = "";
	if ((key = bconf_get(node, keyname))) {
		int i;
		struct bconf_node *n;
		char idxbuf[32];
		const char *keyv[] = {"req", varname, idxbuf};
		char vbuf[256];

		/* if child count is 0 we are a leaf */
		if (bconf_count(key) == 0) {
			if (*suffix)
				template_set_printf(call, varname, "%s%s", bconf_value(key), suffix);
			else
				template_set(call, varname, bconf_value(key));
		}
		for (i = 0; i < bconf_count(key); i++) {
			n = bconf_byindex(key, i);

			const char *value = bconf_get_string(n, "name");
			const char *bu = NULL;

			if (value) {
				/* .name found, so look for a base_url */
				bu = bconf_get_string(n, "base_url");
				if (bu)
					bu = bconf_vget_string(root, "common.base_url", bu, NULL);
			} else {
				value = bconf_value(n);
			}
			if (!bu)
				bu = "";

			snprintf(vbuf, sizeof(vbuf), "%s%s%s", bu, value, suffix);
			snprintf(idxbuf, sizeof(idxbuf), "%d", i);

			bconf_add_datav(&call->filter_bconf, 3, keyv, vbuf, BCONF_DUP);
			if (!bconf_get_int(call->conf->tmpl_conf, "no_simplevars"))
				bpapi_insert(&call->ba, varname, vbuf);
		}
	}
}

static void
add_template_vars_by_node(struct template_call *call, struct bconf_node *node) {
	int i;
	for (i = 0 ; i < bconf_count(node) ; i++) {
		struct bconf_node* n = bconf_byindex(node, i);
		const char *v = bconf_value(n);
		if (v)
			template_set_prefixed(call, "var", bconf_key(n), v);
	}
}

static int 
template_outstring_fmt(struct bpapi_output_chain *ochain, const char *fmt, ...) {
	int res;
	struct template_call *call = ochain->data;
	va_list ap;

	if (!call->hasoutput) {
		/* Skip initial whitespace to simplify use of redirect and template_decline. */
		while (*fmt && isspace(*fmt)) {
			fmt++;
		}

		if (!*fmt)
			return 0;
	}

	va_start(ap, fmt);
	res = ap_vrprintf(call->r, fmt, ap);
	va_end(ap);

	if (res > 0)
		call->hasoutput = 1; /* XXX should check for whitespace here. */

	return res;
}

static int
template_outstring_raw(struct bpapi_output_chain *ochain, const char *str, size_t len) {
	struct template_call *call = ochain->data;

	if (!call->hasoutput) {
		/* Skip initial whitespace to simplify use of redirect and template_decline. */
		while (len > 0 && isspace(*str)) {
			len--;
			str++;
		}
		if (!len)
			return 0;
		call->hasoutput = 1;
	}

	return ap_rwrite(str, len, call->r);
}

const struct bpapi_output templates_output = {
	template_outstring_fmt,
	template_outstring_raw,
	NULL,
	NULL
};

static void
trans_filter_error_cb(transaction_t *t, int res) {
	struct buf_string str = {NULL};
	char *key;
	char *value;

	trans_iter_reset_to_request(t);
	while (trans_iter_next(t, &key, &value)) {
		if (strcmp(key, "blob") != 0) {
			bufcat(&str.buf, &str.len, &str.pos, "%s:%s ", key, value);
		}
	}
	ap_log_error(APLOG_MARK, APLOG_CRIT, OK, NULL, "filter trans(%s) got error %d", str.buf, res);
	free(str.buf);
}

static void
template_vtree(struct template_call *call, struct request_rec *r, struct templates_conf *conf, const char *remote_addr) {
	filter_state_bconf_init(&call->filter_bconf, FS_SESSION);

	filter_state_bconf_set(&call->filter_bconf, "remote_addr", (void *)remote_addr, NULL, FS_SESSION);
	filter_state_bconf_set(&call->filter_bconf, "template_call", call, NULL, FS_SESSION);

	/* This appears like it should be global, but the global filter state is shared between different modules. */
	filter_state_bconf_set(&call->filter_bconf, "trans_errorcb", trans_filter_error_cb, NULL, FS_SESSION);

	bconf_vtree(&call->filter_thread_conf.vtree, call->thread_conf->filter_state);
	shadow_vtree_init(&call->tvt, &call->filter_thread_conf, &conf->vt);

	bconf_vtree(&call->filter_session_conf.vtree, call->filter_bconf);
	shadow_vtree_init(&call->ba.vchain, &call->filter_session_conf, &call->tvt);
}

static void
global_session_cb(struct parse_cb_data *d, char *key, int klen, char *value, int vlen) {
	struct template_call *call = d->cb_data;

	const char *keyv[] = {"req", "session", key};
	bconf_add_datav(&call->filter_bconf, 3, keyv, value, BCONF_DUP);	
	if (!bconf_get_int(call->conf->tmpl_conf, "no_simplevars"))
		bpapi_insert(&call->ba, apr_psprintf(call->r->pool, "session_%s", key), value);
}

static int
setup_parse_request_options(struct parse_qs_options *pq_options, struct templates_conf *conf, struct bconf_node *tmpl_root) {
	const char *s;
	struct bconf_node *node;

	if (!pq_options || !tmpl_root || !conf || !conf->tmpl_conf)
		return 0;

	pq_options->escape_html = TRI_UNDEF;

	/* Allow us to set the default for escape_html on the template level */
	pq_options->defaulted_escape_html = bconf_get_tristate(tmpl_root, "escape_html", TRI_UNDEF);

	/* Allow us to set the default for escape_html on a template-type level (lower precedence than template-level) */
	if (pq_options->defaulted_escape_html == TRI_UNDEF && (s = bconf_get_string(tmpl_root, "type")) && (node = bconf_vget(conf->tmpl_conf, "type", s, NULL)))
		pq_options->defaulted_escape_html = bconf_get_tristate(node, "escape_html", TRI_UNDEF);

	return 1;
}

/*
 * Set the request content-type using either the provided 'tmpl_root', or a heuristic against the template name.
 */
static void
setup_content_type(struct template_call *call, struct bconf_node *node, const char *template) {
	const char *ct = NULL;
	const char *charset = NULL;

	if (node) {
		ct = bconf_get_string(node, "content_type");
		charset = bconf_get_string(node, "charset");
	}

	if (!ct && template) {
		if (strstr(template, ".js"))
			ct = "text/javascript";
		else if (strstr(template, ".xml"))
			ct = "text/xml";
		else if (strstr(template, ".txt"))
			ct = "text/plain";
		else if (strstr(template, ".css"))
			ct = "text/css";
	}

	if (!ct)
		ct = "text/html";

	if (!charset)
		charset = bconf_get_string(call->conf->root, "common.charset");

	if (charset && strcmp(charset, "none") != 0 && !strstr(ct, "charset="))
		ct = apr_psprintf(call->r->pool, "%s; charset=%s", ct, charset);

	ap_set_content_type(call->r, ct);
}

static void
log_template_name_rule(struct request_rec *r, struct rule_rec *rule, const char *template, int level) {

	if (rule && rule->level && level >= rule->level) {
		ap_log_rerror(APLOG_MARK, APLOG_ERR, OK, r, "Matched rule at line %d: %s for uri '%s' -> template '%s'", rule->line, rule->tag, r->uri, template ?: "<none>");
	}
}

/*
 *  The request handler
 */
static int
template_handler(request_rec *r) {
	const char *template;
	const char *tmpl_idx = NULL;
	const char *ip_allowed_list;
	char *cookie;
	struct bconf_node *allowed_hours;
	struct templates_conf *conf = ap_get_module_config(r->server->module_config, &templates_module);
	char *query;
	struct template_call call = {0};
	int wfsession;
	const char *cookiename;
	unsigned long id = 0;
	const char *alias_idx = NULL;
	char id_prefix[100];
	struct rule_rec rule_match = { 0 };
	const char *pinfo;

	int return_status = DECLINED;

	struct language_setup_data lsd = { .type = TYPE_TEMPLATES_CONF, .conf = conf };

	int rule_log_level = bconf_get_int_default(conf->tmpl_conf, "rule_log.level", 1);
	int rule_log_all = bconf_get_int_default(conf->tmpl_conf, "rule_log.all", 0);

	if ((template = mod_template_get_template_name(r, &rule_match, &tmpl_idx, &alias_idx, &id, id_prefix, sizeof(id_prefix), &pinfo))) {
		/* Check if template is allowed. */
		if (tmpl_idx || (tmpl_idx = apr_table_get(conf->templates, template))) {
			const char *type = NULL;
			struct bconf_node *get_vars;
			struct bconf_node *post_vars;
			struct bconf_node *tmpl_root;
			int ip_override;
			int hour_override;
			const char *application_name;
			struct bconf_node *template_var;
			struct tm lt;
			char tbuf[32];
			const char *h;
			const char *referer;
			int elak;
			struct bconf_node *typenode;
			const char *remote_addr = get_remote_addr(r, conf->root);
			const char *lang = NULL;
			const char *country = NULL; 

			lsd.tmpl_root = tmpl_root = bconf_vget(conf->tmpl_list, tmpl_idx, NULL);

			language_setup(r, conf->root, &lsd);

			lang = lsd.lang;
			country = lsd.country;

			if (pinfo && !bconf_get_int(tmpl_root, "accept_subpath"))
				goto RETURN;

			/* Show only for https */
			if (bconf_get_int(tmpl_root, "ssl_only") && strcmp(ap_run_http_scheme(r), "https") != 0) {
				goto RETURN;
			}

			ip_allowed_list = bconf_get_string(tmpl_root, "ip_allowed_list");
			allowed_hours = bconf_get(tmpl_root, "allowed_hours");

			ip_override = bconf_get_int(conf->root, "common.ip_allowed_override");
			hour_override = bconf_get_int(conf->root, "common.access_hour_override");

			if (!ip_override && ip_allowed_list) {
				struct bconf_node *ip_node;
				struct bconf_node *blocket_node;

				ip_node = bconf_vget(conf->root, "common", "ip_allowed_list", ip_allowed_list, NULL);
				blocket_node = bconf_get(conf->root, "common.ip_allowed_list.blocket");

				if (ip_node) {
					int ip_allowed = 0;

					/* Check if IP is in allowed list */
					ip_allowed = bconf_in_list(remote_addr, NULL, ip_node);

					/* Check if IP is blocket ip */
					if (ip_allowed == -1 && blocket_node)
						ip_allowed = bconf_in_list(remote_addr, NULL, bconf_get(conf->root, "common.ip_allowed_list.blocket"));

					if (ip_allowed == -1) {
						ap_log_error(APLOG_MARK, APLOG_ERR, OK, NULL, "Remote address %s was declined (template_handler) %s", remote_addr, template);
						goto RETURN;
					}
				} else {
					goto RETURN;
				}
			}

			gettimeofday(&call.start_time, 0);

			/* Templates that have hours restrictions */
			if (!hour_override && allowed_hours) {
				char hour[3];
				struct tm tm;

				strftime(hour, sizeof(hour), "%H", localtime_r(&call.start_time.tv_sec, &tm));

				if (bconf_in_list(hour, NULL, allowed_hours) == -1)
					goto RETURN;
			}

			/* Decline checks done, init local vars. */

			/* XXX This is based on mail list info, not verified correct */
			call.thread_conf = &conf->thread_data[r->connection->id % conf->num_threads];
			call.result = OK;
			call.ba.ochain.fun = &templates_output;
			call.ba.ochain.data = &call;
			hash_simplevars_init(&call.ba.schain);

			call.ba.filter_state = filter_state_bconf;
			call.r = r;
			call.conf = conf;

			call.cookie_domain = bconf_get_string(conf->root, "common.cookie.domain");

			template_vtree(&call, r, conf, remote_addr);

			if (!call.thread_conf->utf8_cnv) {
				call.thread_conf->utf8_cnv = filter_state(&call.ba, "ucnv_utf8", fs_ucnv_utf8, NULL);
				call.thread_conf->dst_cnv = filter_state(&call.ba, "ucnv_dst", fs_ucnv_dst, NULL);
			}
			/* Parse request */
			get_vars = bconf_get(tmpl_root, "vars");
			if (!get_vars)
				get_vars = bconf_get(conf->tmpl_conf, "default.vars");
			post_vars = bconf_get(tmpl_root, "post_vars");

			struct parse_qs_options pq_options = { 0 }; /* .escape_html=TRI_UNDEF, .defaulted_escape_html=TRI_UNDEF */

			setup_parse_request_options(&pq_options, conf, tmpl_root);

			parse_request(&call, get_vars, post_vars, tmpl_root, &pq_options);

			/* Backward compat insert sq cookie directly */
			if ((query = get_cookie(r, "sq")))
				template_set(&call, "sq", escape_html(r->pool, query));

			template_set(&call, "tmpl_idx", tmpl_idx);

			if (alias_idx) {
				template_set(&call, "tmpl_alias_idx", alias_idx);
			}

			if (r->args)
				template_set(&call, "QUERY_STRING", r->args);
			if (r->uri) {
				/* Skip / */
				template_set(&call, "REQUEST_URI", r->uri + 1);
				if (pinfo)
					template_set(&call, "PATH_INFO", pinfo + 1);
			}
			if (r->unparsed_uri) {
				/* Skip / */
				template_set(&call, "UNPARSED_URI", r->unparsed_uri + 1);

				char *unparsed_query_string = rindex(r->unparsed_uri, '?');
				if (unparsed_query_string)
					template_set(&call, "UNPARSED_QUERY_STRING", unparsed_query_string + 1);
			}
			if ((referer = apr_table_get(r->headers_in, "Referer")))
				template_set(&call, "REFERER", referer);

			strftime(tbuf, sizeof (tbuf), "%F %T", localtime_r(&call.start_time.tv_sec, &lt));
			template_set(&call, "NOW", tbuf);
			template_set_int(&call, "TSNOW", call.start_time.tv_sec);
			if ((h = apr_table_get(r->headers_in, "User-Agent")))
				template_set(&call, "REMOTE_BROWSER", h);
			
			template_set(&call, "FORWARDED_PROTO", get_forwarded_proto(r,conf->root));
			template_set(&call, "REMOTE_ADDR", remote_addr);
			if ((h = apr_table_get(r->headers_in, "Host"))) {
				template_set(&call, "REQUEST_HOST", h);
			}

			application_name = bconf_get_string(tmpl_root, "application_name");
			if (application_name)
				template_set(&call, "application_name", application_name);
			else
				template_set(&call, "application_name", "support"); /* XXX silly default */

			if ((template_var = bconf_vget(tmpl_root, "templatevar", NULL))) {
				add_template_vars_by_node(&call, template_var);
			}

			if (!lang || !bconf_vget(conf->root, "common.lang", lang, NULL)) {
				lang = bconf_get_string(conf->root, "common.default.lang");
				template_set(&call, "DEFAULT_LANGUAGE_USED", "1") ;
			}

			add_template_var(conf->root, tmpl_root, &call, "css", "headstyles", NULL);
			add_template_var(conf->root, tmpl_root, &call, "js", "headscripts", NULL);
			
			add_template_var(conf->root, tmpl_root, &call, "langjs", "headscripts", apr_psprintf(r->pool, "?lang=%s", lang));

			add_template_var(conf->root, tmpl_root, &call, "langcountryjs", "headscripts", apr_psprintf(r->pool, "?lang=%s&country=%s", lang, country));

			elak = atoi(apr_table_get(r->subprocess_env, "elak") ?: "0");
			if (id) {
				template_set(&call, "id_prefix", id_prefix);
				template_set_printf(&call, "id", "%lu", id);
			}
			if (elak == 2) {
				struct in_addr ia;
				struct in6_addr ia6;

				if (inet_pton(AF_INET, remote_addr, &ia) == 1)
					bpapi_set_hex(&call.ba, "trackme", &ia, sizeof(ia));
				else if (inet_pton(AF_INET6, remote_addr, &ia6) == 1)
					bpapi_set_hex(&call.ba, "trackme", &ia6, sizeof(ia6));
				else
					template_set(&call, "trackme", "error");
			}

			if (bconf_get_int(tmpl_root, "no_cache")) {
				apr_table_add(r->headers_out, "Pragma", "no-cache");
				apr_table_add(r->headers_out, "Cache-Control", "no-store, no-cache, must-revalidate, post-check=0, pre-check=0");
				apr_table_add(r->headers_out, "Expires", "Thu, 19 Nov 1981 08:52:00 GMT");
			}

			if (apr_table_get(r->headers_in, "Accept") &&
					(strstr(apr_table_get(r->headers_in, "Accept"), "vnd.wap") != NULL)) {
				/* XXX should be template function */
				template_set(&call, "display_mobile", "1");
			}

			wfsession = bconf_get_int(conf->root, "common.session.webfront.always");
			if (!wfsession && (cookiename = bconf_get_string(conf->root, "common.session.webfront.boolcookie"))) {
				cookie = get_cookie(r, cookiename);
				if (cookie && strcmp(cookie, "1") == 0)
					wfsession = 1;
			}
			if (wfsession && (cookiename = bconf_get_string(conf->root, "common.session.webfront.cookie"))) {
				if ((cookie = get_cookie(r, cookiename))) {
					/* Get global session data from cache */

					char *global_session = cache_get(&call.conf->session, cookie, "global");

					if (global_session) {
						parse_query_string(apr_pstrdup(r->pool, global_session), global_session_cb, &call, NULL, 0, 0, NULL);
						free(global_session);
					} else
						ap_log_error(APLOG_MARK, APLOG_DEBUG, OK, NULL, "No global session for %s", cookie);
				}
			}

			typenode = NULL;
			if (bpapi_has_key(&call.ba, "l")) {
				typenode = bconf_vget(conf->tmpl_conf, "type", "layout", bpapi_get_element(&call.ba, "l", 0), NULL);
				type = NULL;
			}
			if (!typenode) {
				type = bconf_get_string(tmpl_root, "type");
				if (!type)
					type = bconf_get_string(conf->tmpl_conf, "default.type");
				if (type)
					typenode = bconf_vget(conf->tmpl_conf, "type", type, NULL);
			}

			if (typenode) {

				setup_content_type(&call, typenode, template);

				template_set(&call, "content", template);

				if (templates_start_call_local)
					templates_start_call_local(&call, tmpl_root, &lang);

				add_template_var(conf->root, typenode, &call, "css", "headstyles", NULL);
				add_template_var(conf->root, typenode, &call, "js", "headscripts", NULL);

				if (!call_template_lang(&call.ba, bconf_get_string(typenode, "template"), lang, country)) {
					ap_log_rerror(APLOG_MARK, APLOG_ERR, OK, r, 
							"Try to call nonexisting template \"%s\" for \"%s\" for type:%s", bconf_get_string(typenode, "template"), template, type);
					call.result = DECLINED;
				}

				if (!bconf_get_int(typenode, "clean")) {
					/* Insert date, time, host and timer info into page footer */
					struct timeval stop_time;
					gettimeofday(&stop_time, 0);
					ap_rprintf(r, "<!-- %s %s %ld -->\n", &tbuf[0],
						   conf->hostname,
						   1000000 * (stop_time.tv_sec - call.start_time.tv_sec) +
						   stop_time.tv_usec - call.start_time.tv_usec);
				}
			} else {
				setup_content_type(&call, tmpl_root, template);

				if (templates_start_call_local)
					templates_start_call_local(&call, tmpl_root, &lang);
   
				if (!call_template_lang(&call.ba, template, lang, country))
					ap_log_rerror(APLOG_MARK, APLOG_ERR, OK, r,
							"Tried to call nonexisting template \"%s\"", template);
			}

			if (templates_end_call_local)
				templates_end_call_local(&call, tmpl_root);

			bpapi_output_free(&call.ba);
			bpapi_simplevars_free(&call.ba);
			bpapi_vtree_free(&call.ba);

			if (call.filter_bconf)
				filter_state_bconf_free(&call.filter_bconf);

			return_status = call.result;
		}
	}

RETURN:
	if (rule_log_all || return_status == OK)
		log_template_name_rule(r, &rule_match, template, rule_log_level);

	return return_status;
}

static void
register_hooks(apr_pool_t *p) {
	ap_hook_handler(template_handler, NULL, NULL, APR_HOOK_MIDDLE);
	ap_hook_child_init(templates_child_init, NULL, NULL, APR_HOOK_MIDDLE);
}

static const command_rec commands[] = {
	AP_INIT_TAKE_ARGV("BTemplates", command_set_templates, NULL, RSRC_CONF, "Template bconf root"),
	{NULL}
};

module AP_MODULE_DECLARE_DATA templates_module EXPORTED = {
	STANDARD20_MODULE_STUFF,
	NULL, /* per dir create config */
	NULL, /* per dir merge config */
	create_templates_config, /* per server create config */
	NULL, /* per server merge config */
	commands, /* config file commands */
	register_hooks /* register hooks */
};

