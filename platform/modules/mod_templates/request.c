
#include <http_request.h>
#include <http_log.h>
#include <apr_strings.h>

#include "mod_templates.h"
#include "vtree.h"
#include "query_string.h"
#include "cookies.h"
#include "remote_addr.h"

char *
escape_html(apr_pool_t *p, char *str) {
	char *html, *ptr, *redzone;
	int size;

	size = 15;
	html = apr_pcalloc(p, size);
	redzone = html + size - 10;

	for (ptr = html; *str; str++) {
		switch (*str) {
		case '"':
			memcpy(ptr, "&quot;", 6);
			ptr += 6;
			break;
		case '&':
			memcpy(ptr, "&amp;", 5);
			ptr += 5;
			break;
		case '<':
			memcpy(ptr, "&lt;", 4);
			ptr += 4;
			break;
		case '>':
			memcpy(ptr, "&gt;", 4);
			ptr += 4;
			break;
		default:
			*ptr++ = *str;
			break;
		}

		if (ptr > redzone) {
			char *old = html;
			int chars = ptr - old;

			size *= 2;
			html = apr_pcalloc(p, size);
			redzone = html + size - 10;

			memcpy(html, old, chars);		       
			ptr = html + chars;
		}
	}

	*ptr = '\0';
	return html;
}

struct parse_request_cb_data
{
	struct template_call *call;
	const char *prefix;
};

static void
parse_request_cb(struct parse_cb_data *d, char *key, int klen, char *value, int vlen) {
	static const int MAX_STACK_ALLOC_SIZE = 1024;
	struct parse_request_cb_data *data = d->cb_data;
	struct template_call *call = data->call;
	char *k, *v;
	int kbufsz = klen * 2 + 1;
	int vbufsz = vlen * 2 + 1;

	if (klen < MAX_STACK_ALLOC_SIZE)
		k = alloca(kbufsz);
	else
		k = xmalloc(kbufsz);

	if (value) {
		if (vlen < MAX_STACK_ALLOC_SIZE)
			v = alloca(vbufsz);
		else
			v = xmalloc(vbufsz);
	} else {
		v = NULL;
	}

	utf8_decode(k, &kbufsz, key, klen, call->thread_conf->utf8_cnv, call->thread_conf->dst_cnv);
	utf8_decode(v, &vbufsz, value, vlen, call->thread_conf->utf8_cnv, call->thread_conf->dst_cnv);

	/* Assume we're escaping html, then apply escape-options */
	int do_escape_html = 1;
	if (d->options) {
		if (d->options->escape_html != TRI_UNDEF)
			do_escape_html = d->options->escape_html == TRI_FALSE ? 0 : 1;
		else if (d->options->defaulted_escape_html != TRI_UNDEF)
			do_escape_html = d->options->defaulted_escape_html == TRI_FALSE ? 0 : 1;
	}

	if (!parse_request_key_local || !parse_request_key_local(d, k, kbufsz, v, vbufsz)) {
		if (!v || !do_escape_html)
			template_set_prefixed(call, data->prefix, k, v ?: "");
		else
			template_set_prefixed(call, data->prefix, k, escape_html(call->r->pool, v));
	}

	if (klen >= MAX_STACK_ALLOC_SIZE)
		free(k);
	if (vlen >= MAX_STACK_ALLOC_SIZE)
		free(v);
}

static void
parse_post_qs(struct template_call *call, struct bconf_node *post_vars, enum req_utf8 req_utf8, struct parse_qs_options *options) {
	apr_bucket_brigade *bb;
	int done = 0;
	char *payload = NULL;
	const char *cl = apr_table_get(call->r->headers_in, "Content-Length");
	size_t alloc_len = 16 * 1024;
	size_t payload_len = 0;
	apr_status_t rv;
	struct bpapi_vtree_chain vtree;

	if (cl) {
		unsigned long l = strtol(cl, NULL, 10) & 0x7FFFFFFF;

		if (l > 0 && l < alloc_len)
			alloc_len = l;
	}

	payload = apr_palloc(call->r->pool, alloc_len + 1);
	bb = apr_brigade_create(call->r->pool, call->r->connection->bucket_alloc);
	while (!done) {
		apr_bucket *bucket = NULL;

		rv = ap_get_brigade(call->r->input_filters, bb, AP_MODE_READBYTES, APR_BLOCK_READ, HUGE_STRING_LEN);
		if (rv != APR_SUCCESS) {
			ap_log_error(APLOG_MARK, APLOG_CRIT, OK, NULL, "Error reading POST data, error_code=%i", rv);
			return;
		}

		while (!APR_BRIGADE_EMPTY(bb)) {
			bucket = APR_BRIGADE_FIRST(bb);

			if (APR_BUCKET_IS_EOS(bucket)) {
				done = 1;
				break;
			} else if (APR_BUCKET_IS_FLUSH(bucket)) {
				/* do nothing */
			} else {
				const char *data;
				apr_size_t len;

                		rv = apr_bucket_read(bucket, &data, &len, APR_BLOCK_READ);
				if (rv != APR_SUCCESS) {
					ap_log_error(APLOG_MARK, APLOG_CRIT, OK, NULL, "Error reading from a bucket");
					return;
				}

				if (payload_len + len <= alloc_len) {
					memcpy(payload + payload_len, data, len);
					payload_len += len;
				} else {
					ap_log_error(APLOG_MARK, APLOG_ERR, OK, NULL, "Buffer overflow while reading POST payload: %zd > %zd", payload_len + len, alloc_len);
					return;
				}
			}
			apr_bucket_delete(bucket);
		}
	}

	payload[payload_len] = '\0';
	ap_log_error(APLOG_MARK, APLOG_DEBUG, OK, NULL, "Parsing post payload: %s", payload);
	struct parse_request_cb_data cbdata = { call, "post" };
	parse_query_string(payload, parse_request_cb, &cbdata, bconf_vtree(&vtree, post_vars), 1, req_utf8, options);
	vtree_free(&vtree);
}

void
parse_request(struct template_call *call, struct bconf_node *get_vars, struct bconf_node *post_vars, struct bconf_node *tmpl_root, struct parse_qs_options *pq_options) {
	struct bpapi_vtree_chain vtree;
	enum req_utf8 req_utf8 = get_req_utf8_bconf(call->conf->root);

	struct parse_request_cb_data cbdata = { call, NULL };
	struct parse_cb_data pqcb_data = { .cb_data = &cbdata, .options = pq_options };

	switch (call->r->method_number) {
		case M_GET:
			break;
		case M_POST:
			if (post_vars) {
				const char *st = apr_table_get(call->r->headers_in, "Content-Type");

				if (st && strcasecmp(st, "multipart/form-data") == 0) {
					/* Should be added some day */
				} else if (!st || (strncasecmp(st, "application/x-www-form-urlencoded", strlen("application/x-www-form-urlencoded")) == 0 
					&& (st[strlen("application/x-www-form-urlencoded")] == ';' || st[strlen("application/x-www-form-urlencoded")] == '\0'))) {
					parse_post_qs(call, post_vars, req_utf8, pq_options);
				}
			}
			break;
		default: {
			const char* remote_addr = get_remote_addr(call->r, call->conf->root);
			ap_log_error(APLOG_MARK, APLOG_CRIT, OK, NULL, "Unsupported method number (%d) in parse_request for '%s' from client %s", call->r->method_number, call->r->uri ?: "<unknown>", remote_addr ?: "<unknown>");
			break;
		}
	}

	if (call->r->args) {
		cbdata.prefix = "get";
		parse_query_string(apr_pstrdup(call->r->pool, call->r->args), parse_request_cb,
				&cbdata, bconf_vtree(&vtree, get_vars), 0, req_utf8, pq_options);
		vtree_free(&vtree);
		cbdata.prefix = NULL;
	}

	/* Default caller */
	if (!bpapi_has_key(&call->ba, "ca") && !bconf_get_int(tmpl_root, "no_cookie_ca")) {
		char *d_ca;

		if ((d_ca = get_cookie(call->r, "default_ca"))) {
			/* XXX regex check. */
			parse_request_cb(&pqcb_data, apr_pstrdup(call->r->pool, "ca"), 2, d_ca, strlen(d_ca));
			template_set(call, "ca_default_set", "1");
		}
		if ((!d_ca || !bpapi_has_key(&call->ba, "ca")) && !bconf_get_int(tmpl_root, "no_default_ca")) {
			const char *bd_ca = bconf_get_string(call->conf->root, "common.default.caller");

			if (bd_ca) {
				template_set(call, "ca", bd_ca);
				template_set(call, "ca_default_set", "1");
			}
		}
	}
}


