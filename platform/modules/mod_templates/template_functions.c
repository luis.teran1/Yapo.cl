#include <apr_strings.h>
#include <pcre.h>
#include <httpd.h>
#include <http_log.h>
#include <http_protocol.h>
#include <ctype.h>

#include "mod_templates.h"
#include "bpapi.h"
#include "cookies.h"

/* Template handler specific template functions */

#define LOG_WARN_IF_HAS_OUTPUT(call) do { \
	if ((call)->hasoutput) {	\
		ap_log_rerror(APLOG_MARK,	\
				APLOG_ERR, OK,		\
				(call)->r,	\
				"URI: %s, args: %s, referer: %s, template call has output before calling %s", \
				(call)->r->uri, \
				(call)->r->args, \
				apr_table_get((call)->r->headers_in, "Referer"), \
				__FUNCTION__ \
		); \
	} \
} while(0)

static int 
outstring_fmt_null(struct bpapi_output_chain *ochain, const char *fmt, ...) {
	return 0;
}

static int
outstring_raw_null(struct bpapi_output_chain *ochain, const char *str, size_t len) {
	return 0;
}

const struct bpapi_output null_output = {
	outstring_fmt_null,
	outstring_raw_null,
	NULL,
	NULL
};

static const struct tpvar *
template_decline(struct bpapi *api, struct tpvar *dst, enum bpcacheable *cc, int argc, const struct tpvar *argv[]) {
	struct template_call *current_call = filter_state(api, "template_call", NULL, NULL);

	LOG_WARN_IF_HAS_OUTPUT(current_call);

	current_call->ba.ochain.fun = &null_output;
	current_call->result = DECLINED;
	*cc = BPCACHE_CANT;
	return TPV_SET(dst, TPV_NULL);
}
ADD_TEMPLATE_FUNCTION(template_decline);

static const struct tpvar *
template_statuscode(struct bpapi *api, struct tpvar *dst, enum bpcacheable *cc, int argc, const struct tpvar *argv[]) {
	struct template_call *current_call = filter_state(api, "template_call", NULL, NULL);

	LOG_WARN_IF_HAS_OUTPUT(current_call);

	if (argc == 2 && tpvar_int(argv[1])) {
		current_call->r->status = tpvar_int(argv[0]);
	} else if (argc >= 1) {
		current_call->result = tpvar_int(argv[0]);
	}

	return TPV_SET(dst, TPV_NULL);
}
ADD_TEMPLATE_FUNCTION(template_statuscode);

static const struct tpvar *
set_expire(struct bpapi *api, struct tpvar *dst, enum bpcacheable *cc, int argc, const struct tpvar *argv[]) {
	struct template_call *current_call = filter_state(api, "template_call", NULL, NULL);
	const char *maxage;
	char *fma = NULL;

	if (argc > 0 && (maxage = tpvar_str(argv[0], &fma))) {
		LOG_WARN_IF_HAS_OUTPUT(current_call);

		char buf[1024];
		snprintf(buf, 1024, "max-age=%s", maxage);
		apr_table_add(current_call->r->headers_out, "Cache-Control", apr_pstrdup(current_call->r->pool, buf));
	}
	free(fma);
	return TPV_SET(dst, TPV_NULL);
}
ADD_TEMPLATE_FUNCTION(set_expire);

static const struct tpvar *
template_redirect(struct bpapi *api, struct tpvar *dst, enum bpcacheable *cc, int argc, const struct tpvar *argv[]) {
	struct template_call *current_call = filter_state(api, "template_call", NULL, NULL);
	const char *loc;
	char *fl = NULL;

	if (argc > 0 && (loc = tpvar_str(argv[0], &fl)) && *loc) {
		LOG_WARN_IF_HAS_OUTPUT(current_call);

		if (apr_table_get(current_call->r->headers_out, "Location") != NULL) {
			ap_log_rerror(APLOG_MARK, APLOG_ERR, OK, current_call->r, "uri: %s, args: %s, referer: %s: multiple redirects %s -> %s",
					current_call->r->uri, current_call->r->args, apr_table_get(current_call->r->headers_in, "Referer"),
					apr_table_get(current_call->r->headers_out, "Location"), loc);
		}
		apr_table_set(current_call->r->headers_out, "Location", apr_pstrdup(current_call->r->pool, loc));

		current_call->ba.ochain.fun = &null_output;

		if (argc == 2)
			current_call->r->status = tpvar_int(argv[1]);
		else
			current_call->r->status = HTTP_MOVED_TEMPORARILY;
	}
	free(fl);
	*cc = BPCACHE_CANT;
	return TPV_SET(dst, TPV_NULL);
}
ADD_TEMPLATE_FUNCTION_WITH_NAME(template_redirect, redirect);

static const struct tpvar *
template_has_cookie(struct bpapi *api, struct tpvar *dst, enum bpcacheable *cc, int argc, const struct tpvar *argv[]) {
	struct template_call *current_call = filter_state(api, "template_call", NULL, NULL);
	*cc = BPCACHE_CANT;

	if (argc >= 1) {
		char *fa0;
		const char *value = get_cookie(current_call->r, tpvar_str(argv[0], &fa0));
		free(fa0);
		if (value != NULL && (argc == 1 || *value != '\0')) {
			return TPV_SET(dst, TPV_INT(1));
		}
	}

	return TPV_SET(dst, TPV_INT(0));
}
ADD_TEMPLATE_FUNCTION_WITH_NAME(template_has_cookie, has_cookie);

static const struct tpvar *
template_get_cookie(struct bpapi *api, struct tpvar *dst, enum bpcacheable *cc, int argc, const struct tpvar *argv[]) {
	struct template_call *current_call = filter_state(api, "template_call", NULL, NULL);
	char *res = NULL;

	if (argc == 2) {
		char *fp, *fr;
		const char *param = tpvar_str(argv[0], &fp);
		const char *regex = tpvar_str(argv[1], &fr);
		const char *value = get_cookie(current_call->r, param);

		if (value && strlen(value) && regex && strlen(regex)) {
			pcre *pregex;
			const char *err;
			int erri;
			int flags = PCRE_DOLLAR_ENDONLY | PCRE_DOTALL | PCRE_NO_AUTO_CAPTURE | PCRE_CASELESS;
			const char *charset = vtree_get(&api->vchain, "common", "charset", NULL);

			if (charset && strcmp(charset, "UTF-8") == 0)
				flags |= PCRE_UTF8;

			pregex = pcre_compile(regex,
					      flags,
					      &err, &erri, NULL);

			if (pregex) {
				if ((erri = pcre_exec(pregex, NULL, value, strlen(value), 0, 0, NULL, 0)) >= 0) {
					res = xstrdup(value);
				}
				pcre_free(pregex);
			}
		}

		free(fp);
		free(fr);
	}

	*cc = BPCACHE_CANT;
	return TPV_SET(dst, res ? TPV_DYNSTR(res) : TPV_NULL);
}
ADD_TEMPLATE_FUNCTION_WITH_NAME(template_get_cookie, get_cookie);

static const struct tpvar *
template_clear_cookie(struct bpapi *api, struct tpvar *dst, enum bpcacheable *cc, int argc, const struct tpvar *argv[]) {
	struct template_call *current_call = filter_state(api, "template_call", NULL, NULL);

	if (argc == 1) {
		LOG_WARN_IF_HAS_OUTPUT(current_call);

		char *fp;
		const char *param = tpvar_str(argv[0], &fp);
		clear_cookie(current_call->r, param, current_call->cookie_domain);
		free(fp);
	}

	*cc = BPCACHE_CANT;
	return TPV_SET(dst, TPV_NULL);
}
ADD_TEMPLATE_FUNCTION_WITH_NAME(template_clear_cookie, clear_cookie);

struct _set_cookie {
	const char *name;
	int time;
	char *value;
	struct template_call *call;
	char *fn;
	char type;
	int httponly;
};

static int 
set_cookie_outstring_raw(struct bpapi_output_chain *ochain, const char *str, size_t len) {
	struct _set_cookie *sc = ochain->data;

	if (len)
		sc->value = apr_psprintf(sc->call->r->pool, "%s%.*s", sc->value == NULL ? "" : sc->value, (int)len, str);

	return 0;
}

static int
init_set_cookie(const struct bpfilter *filter, struct bpapi *api, int argc, const struct tpvar **argv) {
	struct _set_cookie *sc = BPAPI_OUTPUT_DATA(api);
	struct template_call *current_call = filter_state(api, "template_call", NULL, NULL);
	char *fsa = NULL;

	sc->httponly = 0;

	switch (argc) {
	case 4:
		sc->httponly = 1;
	case 3:
		sc->time = tpvar_int(argv[2]);
		sc->type = *tpvar_str(argv[1], &fsa);
		break;
	case 2:
		sc->time = tpvar_int(argv[1]);
	case 1:
		break;
	default:
		return 1;
	}
	sc->name = tpvar_str(argv[0], &sc->fn);
	sc->value = NULL;
	sc->call = current_call;
	free(fsa);

	return 0;
}

static void
fini_set_cookie(struct bpapi_output_chain *ochain) {
	struct _set_cookie *sc = ochain->data;

	if (sc) {
		if (sc->value && *sc->value) {
			LOG_WARN_IF_HAS_OUTPUT(sc->call);

			if (!sc->type)
				set_cookie(sc->call->r, sc->name, sc->value, sc->call->cookie_domain, sc->httponly);
			else if (sc->time && sc->type == 'd')
				set_cookie_with_expiry(sc->call->r, sc->name, sc->value, sc->call->cookie_domain, sc->time, sc->httponly);
			else if (sc->time && sc->type == 'm')
				set_cookie_with_expiry_minutes(sc->call->r, sc->name, sc->value, sc->call->cookie_domain, sc->time, sc->httponly);
			else if (sc->time && sc->type == 's')
				set_cookie_with_expiry_seconds(sc->call->r, sc->name, sc->value, sc->call->cookie_domain, sc->time, sc->httponly);
		}
		free(sc->fn);
	}
}

static const struct bpapi_output set_cookie_output = {
	self_outstring_fmt,
	set_cookie_outstring_raw,
	fini_set_cookie,
	NULL
};

ADD_OUTPUT_FILTER(set_cookie, sizeof(struct _set_cookie));

static const struct tpvar *
errorlog(struct bpapi *api, struct tpvar *dst, enum bpcacheable *cc, int argc, const struct tpvar **argv) {
	struct template_call *current_call = filter_state(api, "template_call", NULL, NULL);
	char *fa0 = NULL, *fa1 = NULL;

	*cc = BPCACHE_CANT;
        switch (argc) {
        case 0:
                ap_log_rerror(APLOG_MARK, APLOG_ERR, OK, current_call->r, "errorlog() called with no args");
                break;
        case 1:
                ap_log_rerror(APLOG_MARK, APLOG_ERR, OK, current_call->r, "Error: %s", tpvar_str(argv[0], &fa0));
                break;
        case 2:
                ap_log_rerror(APLOG_MARK, APLOG_ERR, OK, current_call->r, "Error: %s : %s", tpvar_str(argv[0], &fa0), tpvar_str(argv[1], &fa1));
                break;
        default:
                ap_log_rerror(APLOG_MARK, APLOG_ERR, OK, current_call->r, "errorlog() called with %d args", argc);
                break;
        }
	free(fa0);
	free(fa1);
	return TPV_SET(dst, TPV_NULL);
}

ADD_TEMPLATE_FUNCTION(errorlog);

struct apache_note_data {
	const char *key;
	char *fk;
	char *value;
	struct template_call *call;
};

static int 
apache_note_outstring_raw(struct bpapi_output_chain *ochain, const char *str, size_t len) {
	struct apache_note_data *data = ochain->data;

	if (len)
		data->value = apr_psprintf(data->call->r->pool, "%s%.*s", data->value == NULL ? "" : data->value, (int)len, str);

	return 0;
}

static int
init_apache_note(const struct bpfilter *filter, struct bpapi *api, int argc, const struct tpvar **argv) {
	struct apache_note_data *data = BPAPI_OUTPUT_DATA(api);
	struct template_call *current_call = filter_state(api, "template_call", NULL, NULL);

	data->key = tpvar_str(argv[0], &data->fk);

	data->value = NULL;
	data->call = current_call;

	return 0;
}

static void
fini_apache_note(struct bpapi_output_chain *ochain) {
	struct apache_note_data *data = ochain->data;

	if (data->value) {
                apr_table_add(data->call->r->notes, data->key, data->value);
		ap_log_error(APLOG_MARK, APLOG_INFO, OK, NULL, "%s", data->value);
	}
	free(data->fk);
}

static const struct bpapi_output apache_note_output = {
	self_outstring_fmt,
	apache_note_outstring_raw,
	fini_apache_note,
	NULL
};

ADD_OUTPUT_FILTER(apache_note, sizeof(struct apache_note_data));

static const struct tpvar *
template_mime_type(struct bpapi *api, struct tpvar *dst, enum bpcacheable *cc, int argc, const struct tpvar *argv[]) {
	struct template_call *current_call = filter_state(api, "template_call", NULL, NULL);
	*cc = BPCACHE_CANT;

	if (argc >= 1) {
		LOG_WARN_IF_HAS_OUTPUT(current_call);

		char *fa0;
		ap_set_content_type(current_call->r, tpvar_str(argv[0], &fa0));
		free(fa0);
	}
	return TPV_SET(dst, TPV_NULL);
}
ADD_TEMPLATE_FUNCTION_WITH_NAME(template_mime_type, mime_type);

struct set_cookie_domain_data {
	char *value;
	struct template_call *call;
};

static int 
set_cookie_domain_outstring_raw(struct bpapi_output_chain *ochain, const char *str, size_t len) {
	struct set_cookie_domain_data *data = ochain->data;

	if (len)
		data->value = apr_psprintf(data->call->r->pool, "%s%.*s", data->value == NULL ? "" : data->value, (int)len, str);

	return 0;
}

static int
init_set_cookie_domain(const struct bpfilter *filter, struct bpapi *api, int argc, const struct tpvar *argv[]) {
	struct set_cookie_domain_data *data = BPAPI_OUTPUT_DATA(api);
	struct template_call *current_call = filter_state(api, "template_call", NULL, NULL);

	data->value = NULL;
	data->call = current_call;

	return 0;
}

static void
fini_set_cookie_domain(struct bpapi_output_chain *ochain) {
	struct set_cookie_domain_data *data = ochain->data;

	data->call->cookie_domain = data->value ?: "";
}

static const struct bpapi_output set_cookie_domain_output = {
	self_outstring_fmt,
	set_cookie_domain_outstring_raw,
	fini_set_cookie_domain,
	NULL
};

ADD_OUTPUT_FILTER(set_cookie_domain, sizeof(struct set_cookie_domain_data));

struct add_header_data {
	struct buf_string buf;
	struct template_call *call;
	const struct tpvar *name;
};

static int
init_add_header(const struct bpfilter *filter, struct bpapi *api, int argc, const struct tpvar *argv[]) {
	struct add_header_data *data = BPAPI_OUTPUT_DATA(api);

	if (argc >= 1)
		data->name = argv[0];
	data->call = filter_state(api, "template_call", NULL, NULL);

	return 0;
}

static void
fini_add_header(struct bpapi_output_chain *ochain) {
	struct add_header_data *data = ochain->data;

	if (data->name && data->buf.buf) {
		LOG_WARN_IF_HAS_OUTPUT(data->call);

		const char *name;
		TPVAR_STRTMP(name, data->name);
		apr_table_add(data->call->r->err_headers_out, apr_pstrdup(data->call->r->pool, name), apr_pstrdup(data->call->r->pool, data->buf.buf));
	}
	free(data->buf.buf);
}

static const struct bpapi_output add_header_output = {
	bufstring_outstring_fmt,
	bufstring_outstring_raw,
	fini_add_header,
	NULL
};


ADD_OUTPUT_FILTER(add_header, sizeof(struct add_header_data));

/*
 * Set secure session cookie
 */
struct _set_secure_cookie {
	const char *name;
	const char *type;
	const char *time;
	char *value;
	struct template_call *call;
	char *fn, *fty, *fti;
};

static int 
set_secure_cookie_outstring_raw(struct bpapi_output_chain *ochain, const char *str, size_t len) {
	struct _set_secure_cookie *sc = ochain->data;

	if (len)
		sc->value = apr_psprintf(sc->call->r->pool, "%s%.*s", sc->value == NULL ? "" : sc->value, (int)len, str);

	return 0;
}

static int
init_set_secure_cookie(const struct bpfilter *filter, struct bpapi *api, int argc, const struct tpvar **argv) {
	struct _set_secure_cookie *sc = BPAPI_OUTPUT_DATA(api);
	struct template_call *current_call = filter_state(api, "template_call", NULL, NULL);
	const char *sc_arg;
	char *fa = NULL;

	if (argc < 1)
		return 0;

	sc->name = tpvar_str(argv[0], &sc->fn);
	sc_arg = argc < 2 ? NULL : tpvar_str(argv[1], &fa);

	/* Backward compability. If argument is numeric, use it as time. Otherwise it's type of time units */
	if (sc_arg && isdigit(*sc_arg)) {
		sc->time = sc_arg;
		sc->fti = fa;
	} else {
		sc->type = sc_arg;
		sc->fty = fa;
		sc->time = argc < 3 ? NULL : tpvar_str(argv[2], &sc->fti);
	}
	sc->value = NULL;
	sc->call = current_call;

	return 0;
}

static void
fini_set_secure_cookie(struct bpapi_output_chain *ochain) {
	struct _set_secure_cookie *sc = ochain->data;

	if (sc) {
		if (sc->value && *sc->value) {
			LOG_WARN_IF_HAS_OUTPUT(sc->call);
			if (!sc->type)
				set_secure_cookie(sc->call->r, sc->name, sc->value, sc->call->cookie_domain);
			else if (sc->time && *sc->type == 'd')
				set_secure_cookie_with_expiry(sc->call->r, sc->name, sc->value, sc->call->cookie_domain, atoi(sc->time));
			else if (sc->time && *sc->type == 'm')
				set_secure_cookie_with_expiry_minutes(sc->call->r, sc->name, sc->value, sc->call->cookie_domain, atoi(sc->time));
			else if (sc->time && *sc->type == 's')
				set_secure_cookie_with_expiry_seconds(sc->call->r, sc->name, sc->value, sc->call->cookie_domain, atoi(sc->time));
		}
		free(sc->fn);
		free(sc->fty);
		free(sc->fti);
	}
}

static const struct bpapi_output set_secure_cookie_output = {
	self_outstring_fmt,
	set_secure_cookie_outstring_raw,
	fini_set_secure_cookie,
	NULL
};

ADD_OUTPUT_FILTER(set_secure_cookie, sizeof(struct _set_secure_cookie));
