#ifndef MOD_TEMPLATES_H
#define MOD_TEMPLATES_H

#include <httpd.h>
#include <http_config.h>
#include <sys/types.h>

#include "apr_hash.h"
#include "bconf.h"
#include "bpapi.h"
#include "unicode.h"
#include "cacheapi.h"

/* Associate template aliases with their index */
struct template_alias_rec {
	const char *alias_idx;
	const char *tmpl_idx;
	const char *tmpl;
};

struct rule_rec {
	int line;
	int level;
	const char* tag;
};

#define DEFAULT_RULE_LOGLEVEL 9
#define RETURN_TEMPLATE_RULE_TAG(tmplname, loglvl, rule_tag) do { \
	if (rule) { rule->line = __LINE__; rule->level = loglvl; rule->tag = rule_tag; } \
	return tmplname; \
	} while (0)


struct templates_thread_conf {
	struct bconf_node *filter_state;
	UConverter *utf8_cnv;
	UConverter *dst_cnv;
};

struct templates_conf {
	struct bconf_node *root;
	struct bconf_node *tmpl_conf;
	struct bconf_node *tmpl_list;
	char hostname[256];

	struct shadow_vtree vbconf;
	struct bpapi_vtree_chain vfconf;
	struct bpapi_vtree_chain vt;

	apr_table_t *templates;
	apr_table_t *template_id_prefixes;
	apr_table_t *template_path_prefixes;
	apr_hash_t *template_aliases;

	int num_threads;
	struct templates_thread_conf *thread_data;

	struct cache_pools session;

	void *local;
};

struct template_call {
	int hasoutput;
	int result;
	struct bpapi ba;
	struct shadow_vtree filter_thread_conf;
	struct bpapi_vtree_chain tvt;
	struct shadow_vtree filter_session_conf;
	struct bconf_node *filter_bconf;

	struct request_rec *r;
	struct timeval start_time;

	struct templates_conf *conf;
	struct templates_thread_conf *thread_conf;

	const char *cookie_domain;

	void *local;
};

struct parse_qs_options;
struct parse_cb_data;


char *escape_html(apr_pool_t *p, char *str);
void parse_request(struct template_call *call, struct bconf_node *get_vars, struct bconf_node *post_vars, struct bconf_node *tmpl_root, struct parse_qs_options *pq_options);

void template_set(struct template_call *call, const char * key, const char * value);
void template_set_prefixed(struct template_call *call, const char *prefix, const char * key, const char * value);
void template_set_int(struct template_call *call, const char * key, const int value);
void template_set_printf(struct template_call *call, const char * key, const char *fmt, ...) FORMAT_PRINTF(3, 4);
void template_set_hex(struct template_call *call, const char *key, const void *data, size_t len);

extern const char *(*mod_template_get_template_name)(struct request_rec *r, struct rule_rec *rule, const char **tmpl_idx, const char **alias_idx, unsigned long *id, char *id_prefix, size_t id_prefix_sz, const char **path_info);

void add_template_var(struct bconf_node *root, struct bconf_node *node, struct template_call *call, const char* keyname, 
		const char *varname, const char *suffix);

extern int setup_templates_local(apr_pool_t *pool, struct templates_conf *conf, struct bconf_node *root) WEAK;
extern void templates_start_call_local(struct template_call *call, struct bconf_node *tmpl_root, const char **lang) WEAK;
extern void templates_end_call_local(struct template_call *call, struct bconf_node *tmpl_root) WEAK;
extern int parse_request_key_local(struct parse_cb_data *d, char *key, int klen, char *value, int vlen) WEAK;

extern module AP_MODULE_DECLARE_DATA templates_module;

#endif /*MOD_TEMPLATES_H*/
