#include <apr_strings.h>
#include <httpd.h>
#include <http_log.h>
#include <http_config.h>
#include <ap_mpm.h>
#include <unistd.h>

#include "mod_list.h"
#include "strl.h"
#include "logging.h"
#include "mod_bconf_extern.h"

static void
setup_week_info(struct list_conf *conf, time_t now) {
	struct tm tm;
	struct date_rec thursday;
	int startwnum;
	int wnum;
	int week;

	localtime_r(&now, &tm);

	/* Find thursday of current week. */
	date_set(&thursday, &tm);
	if (tm.tm_wday == 0)
		date_set_day_offset(&thursday, 4 - 7);
	else if (tm.tm_wday != 4)
		date_set_day_offset(&thursday, 4 - tm.tm_wday);

	/* Find current week number. */
	startwnum = wnum = (thursday.day_of_year - 1) / 7 + 1;
	for (week = 0; week < 53; week++, wnum++) {
		if (thursday.day_of_year <= 7)
			wnum = 1;
		if (week > 50 && wnum == startwnum)
			break;

		conf->weeks[week].year = thursday.year;
		conf->weeks[week].week = wnum;

		date_set_day_offset(&thursday, 7);
	}
	conf->num_weeks = week;
}

/*
 *  Setup the date for today and yesterday
 */

void
setup_date(struct list_conf *conf, time_t now) {
	struct tm t;

	/* Setup today */
	localtime_r(&now, &t);
	date_set(&conf->date.today, &t);

	/* Setup yesterday */
	memcpy(&conf->date.yesterday, &conf->date.today, sizeof(conf->date.yesterday));
	date_set_prev_day(&conf->date.yesterday);

	/* Calculate the time_t for next midnight */
	conf->date.next_midnight_at = now + ((24 - 1 - t.tm_hour) * 60 * 60) + ((60 - 1 - t.tm_min) * 60) + (60 - t.tm_sec);

	/* Should be done at start, restart and midnight Sunday -> Monday*/
	setup_week_info(conf, now);
}

static void
setup_language(apr_pool_t *pool, struct list_conf *conf) {
	const char *str;

	/* Setup the fallback language code. */
	if ( (str = bconf_get_string(conf->root, "common.default.lang")) == NULL) {
		str = "sv";
		ap_log_error(APLOG_MARK, APLOG_ERR, OK, NULL, "Missing bconf key: common.default.lang - using default (%s)", str);
	}

	conf->lang.code = apr_pstrdup(pool, str);
}

static int
setup_conf(apr_pool_t *pool, struct list_conf *conf, struct bconf_node *root, struct bconf_node *filt_root) {
	const char *str;
	const char *store_regexp;

	if (conf->root)
		return 0; /* XXX We are called twice apparently. Investigate later. */

	const char *log_level = bconf_get_string(root, "mod_list.log_level");
	if (!log_level || !*log_level) {
		log_level = "info";
	}
	/* The lib/common code is using the stardard logging functionality */
	log_setup("mod_list", log_level);

	strlcpy(conf->hostname, "nohostname", sizeof(conf->hostname));
	gethostname(conf->hostname, sizeof(conf->hostname));

	conf->root = root;

	bconf_vtree(&conf->vbconf, root);
	bconf_vtree(&conf->vfconf.vtree, filt_root);
	shadow_vtree_init(&conf->vt, &conf->vfconf, &conf->vbconf);

	if ((str = bconf_get_string(conf->root, "common.index_referer"))) {
		if ((conf->index_referer = ap_pregcomp(pool, str, AP_REG_ICASE)) == NULL) {
			ap_log_error(APLOG_MARK, APLOG_ERR, OK, NULL, "Failed to compile regex (%s)", str);
			return HTTP_INTERNAL_SERVER_ERROR;
		}
	}

	store_regexp = bconf_value(bconf_vget(conf->root, "store", "regex", NULL));
	if (store_regexp && (conf->store_regex = ap_pregcomp(pool, store_regexp, AP_REG_ICASE)) == NULL) {
		ap_log_error(APLOG_MARK, APLOG_CRIT, OK, NULL, "Failed to compile regex (%s)", store_regexp);
		return HTTP_INTERNAL_SERVER_ERROR;
	}

	setup_date(conf, time(NULL));
	setup_language(pool, conf);

	conf->use_th_param = bconf_get_int(conf->root, "common.listing_mode.use_th_param");

	cache_init_pools(&conf->session, &conf->vbconf);

	if (setup_list_local)
		setup_list_local(pool, conf, root);

	return 0;
}

int
list_post_config(apr_pool_t *pool, apr_pool_t *plog, apr_pool_t *ptemp, server_rec *s) {
	struct bconf_node **filterp;
	struct list_conf *conf;
	struct bconf_node *br;
	int ret;

	if ((br = mod_bconf_get_root(s)) == NULL) {
		ap_log_error(APLOG_MARK, APLOG_WARNING, OK, NULL, "mod_templates - Failed to get bconf root.");
		return HTTP_INTERNAL_SERVER_ERROR;
	}

	if ((filterp = mod_bconf_get_filterp(s)) == NULL) {
		ap_log_error(APLOG_MARK, APLOG_WARNING, OK, NULL, "mod_templates - Failed to get filt root.");
		return HTTP_INTERNAL_SERVER_ERROR;
	}

	for ( ; s ; s = s->next) {
		conf = ap_get_module_config(s->module_config, &list_module);
		ret = setup_conf(pool, conf, br, *filterp);
		if (ret)
			return ret;
	}

	return OK;
}

void *
list_create_config(apr_pool_t *pool, server_rec *s) {
	return apr_pcalloc(pool, sizeof(struct list_conf));
}

static apr_status_t
cleanup_thread_data(void *v) {
	struct list_conf *conf = v;
	int i;

	for (i = 0; i < conf->num_threads; i++) {
		filter_state_bconf_free(&conf->thread_data[i].filter_state);
	}
	return APR_SUCCESS;
}

void
list_child_init(apr_pool_t *pool, server_rec *s) {
	struct list_conf *conf = ap_get_module_config(s->module_config, &list_module);
	int threaded;
	int i;

	ap_mpm_query(AP_MPMQ_IS_THREADED, &threaded);
	if (threaded)
		ap_mpm_query(AP_MPMQ_MAX_THREADS, &conf->num_threads);
	else
		conf->num_threads = 1;

	conf->thread_data = zmalloc(sizeof(struct list_thread_conf) * conf->num_threads);
	for (i = 0; i < conf->num_threads; i++) {
		filter_state_bconf_init(&conf->thread_data[i].filter_state, FS_THREAD);
	}
	apr_pool_cleanup_register(pool, conf, cleanup_thread_data, apr_pool_cleanup_null);
}

