#include <httpd.h>
#include <http_log.h>
#include <http_config.h>
#include <http_protocol.h>
#include <apr_strings.h>
#include <ctype.h>

#include <bsapi.h>
#include <bconf.h>
#include <util.h>
#include <log_event.h>
#include <query_string.h>
#include "cookies.h"
#include "accept_lang.h"
#include "ctemplates.h"
#include "cacheapi.h"
#include "remote_addr.h"

#include <stdlib.h>
#include <time.h>

#include "mod_list.h"
#include <pcre.h>

#include "filter_state_ucnv.h"
#include "search/filter_state_bsconf.h"

/*
 *  Functions used to compose and send the query to bsearch
 */

static void
set_company_query(struct list_call *b) {
	int offset = b->offset;

	if (b->company_id) {
		/* Initialize the query */
		b->query = apr_psprintf(b->r->pool,
					"0 lim:1 count_all(unfiltered) id:%d", b->company_id);
	} else {
		const char *q = b->q_raw;

		if (!q)
			q = bpapi_get_element(&b->bparse, "q", 0);

		/* Initialize the query - the limit is always set to 50 */
		b->query = apr_psprintf(b->r->pool,
					"%d subsort:- lim:%d count_all(unfiltered)", offset, 50);

		if (b->category)
			b->query = apr_psprintf(b->r->pool,
						"%s c_cat:%d", b->query, b->category);
		else
			b->query = apr_psprintf(b->r->pool,
						"%s c_cat:1-", b->query);

		if (set_company_query_local)
			set_company_query_local(b);

		/* Finally append the search string  */
		if (q && *q) {
			b->query = apr_psprintf(b->r->pool, "%s *:* %s", b->query, q);
		}
	}
}

void
set_search_query(struct list_call *b, const char *failover_query) {
	int offset = 0;
	struct qs_sq_data data = {0};

	data.vtree = &b->bparse.vchain;

	/* Don't use callback when doing failover queries, we don't want bpapi to change */
	if (failover_query)
		data.parse_qs_cb = parse_qs_decode_only_cb;
	else
		data.parse_qs_cb = parse_qs_cb;
	data.cb_data = b;
	/* Use the user-selected/auto-detected language if available, else fall back to common.default.lang */
	data.lang = b->lang.code ? b->lang.code : b->conf->lang.code;
	data.rootpath = "qs";
	data.req_utf8 = get_req_utf8_bconf(b->conf->root);

	b->vars = apr_table_make(b->r->pool, 64);

	/* Prepend show thumb from cookie. If one exists in args it will be used primarily. */
	query_string_search_query("li", apr_psprintf(b->r->pool, "md=%s&%s", b->mode, failover_query ? failover_query : b->args), &data);

	if (!failover_query) {
		/* Insert md here to avoid problems with it changing multiple times in the parsing. */
		/*If we are in adwatch we do not want md to be set to mapmode(mp)*/
		if (apr_table_get(b->vars, "wid") && (strncmp(b->mode, "mp", 2) == 0 )) {
			bpapi_insert(&b->bparse, "md", "th");
		} else {
			bpapi_insert(&b->bparse, "md", b->mode);
			/* backwards compat th */
			if (strncmp(b->mode, "th", 2) == 0)
				bpapi_insert(&b->bparse, "th", "1");
			else
				bpapi_insert(&b->bparse, "th", "0");
		}
	}

	b->ads_per_page = data.limit;

	/* Calculate the bsearch offset - criterias.offset is equal to the current page */
	if (data.offset) {
		offset = (data.offset - 1) * b->ads_per_page;
	}

	/* Initialize the query */
	if (data.params.buf || data.query.buf)
		b->query = apr_psprintf(b->r->pool, "%d lim:%d slang:%s %s %s %s %s *:* %s", offset, data.limit, data.lang,
				data.count_level.buf ?: "",
				data.filter.buf ?: "",
				data.unfiltered.buf ?: "",
				data.params.buf ?: "",
				data.query.buf ?: "");
	else
		b->query = NULL;
	free_qs_sq(&data);
	b->offset = offset;
}

/*
 *  Setup helper variables used by the searchbox
 */
static void
set_searchbox(struct list_call *b) {
	int week;

	for (week = 0; week < b->conf->num_weeks; week++) {
		bpapi_set_int(&b->bparse, "weeks_year", b->conf->weeks[week].year);
		bpapi_set_int(&b->bparse, "weeks_week", b->conf->weeks[week].week);
		bpapi_set_printf(&b->bparse, "weeks_value", "%d:%d", b->conf->weeks[week].year, b->conf->weeks[week].week);
	}
}

/*
 *  Setup helper variables for the filter (the all/private/company tab)
 */

static void
set_filters(struct list_call *b) {
	int count;
	const char *f = bpapi_get_element(&b->bparse, "f", 0);

	/* Set the tab selector variable */
	switch (f ? *f : 0) {
	case 'p':
		count = bpapi_get_int(&b->bparse, "company");
		bpapi_insert(&b->bparse, "tab", count > 0 ? "private" : "all");
		break;
	case 'c':
		count = bpapi_get_int(&b->bparse, "private");
		bpapi_insert(&b->bparse, "tab", count > 0 ? "company" : "all");
		break;
	default:
		bpapi_insert(&b->bparse, "tab", "all");
		if (!f || !*f)
			bpapi_insert(&b->bparse, "f", "a");
		break;
	}
}

void (*mod_list_set_filters)(struct list_call *) = set_filters;

/*
 *  Setup the query strings used for the different links on the page
 */

static void
add_qs_key(struct list_call *b, const char *key, int exclude) {
	char *buf = NULL;
	const char *mode_default = bconf_get_string_default(b->conf->root, "common.listing_mode.default", "th");
	int offset;

	offset = b->offset / b->ads_per_page + 1;

	buf = apr_pstrcat(b->r->pool,
			  (!(exclude & PARAM_OFFSET) && offset != 1) ?
			  apr_psprintf(b->r->pool, "&amp;o=%d", offset) : "",

			  (!(exclude & PARAM_LIST_MODE) && strncmp(b->mode, mode_default, 2) != 0) ?
			  (b->conf->use_th_param ?
			   apr_psprintf(b->r->pool, "&amp;th=%d", strncmp(b->mode, "th", 2) == 0)
			   : apr_psprintf(b->r->pool, "&amp;md=%s", b->mode)) : "",

			  (!(exclude & PARAM_SORT_PRICE) && b->sort_price) ?
			  apr_psprintf(b->r->pool, "&amp;sp=%d", b->sort_price) : "",

			  (!(exclude & PARAM_FILTER) && b->filter && b->filter != 'a') ?
			  apr_psprintf(b->r->pool, "&amp;f=%c", b->filter) : "",

			  NULL);

	if (add_qs_key_local)
		add_qs_key_local(b, key, exclude, &buf);

	if (buf)
		bpapi_insert(&b->bparse, key, buf);
}

/*
 *  Setup misc helper variables that are used to display the list page correctly
 */

static void
set_helper_variables(struct list_call *b) {
	/* Set all query string variables that will be used in the template */
	if (b->qs) {
		bpapi_insert(&b->bparse, "qs", escape_html(b->r->pool, b->qs));
		bpapi_insert(&b->bparse, "qs_noamp", escape_html_ex(b->r->pool, b->qs, NO_AMP));
	}

	add_qs_key(b, "qs_md",  PARAM_LIST_MODE);
	add_qs_key(b, "qs_fi",  PARAM_OFFSET | PARAM_FILTER);
	add_qs_key(b, "qs_sp",  PARAM_OFFSET | PARAM_SORT_PRICE);
	add_qs_key(b, "qs_vi",  PARAM_OFFSET | PARAM_FILTER | PARAM_SORT_PRICE | PARAM_LIST_MODE | PARAM_WHERE);
	add_qs_key(b, "qs_nav", PARAM_OFFSET);
}

/*
 *  Setup helper variables for the result navigation (first, previous, next, last and the 1, 2, 3 ...)
 */

static void
set_result_navigation(struct list_call *b) {
	int private, company, filtered, unfiltered;
	int pages, max_pages, start_page, end_page, current_page;
	int first_ad, last_ad, i;

	/* Set helper variables that indicates wheather prices and images exists in the result */
	bpapi_set_int(&b->bparse, "has_prices", b->has_prices);

	/* Do not set the has_images variable for the company-ad-list (always thumbnails) */
	if (b->company_id == 0) {
		bpapi_set_int(&b->bparse, "has_images", b->has_images);
		bpapi_set_int(&b->bparse, "show_map_link", b->has_map);
	}

	/* */
	filtered = bpapi_get_int(&b->bparse, "filtered");
	unfiltered = bpapi_get_int(&b->bparse, "unfiltered");
	company = atoi(bpapi_get_element(&b->bparse, "c_tab", 0) ?: "0");

	bpapi_set_int(&b->bparse, "total", filtered);

	bpapi_set_int(&b->bparse, "company", company);
	private = unfiltered - company;
	bpapi_set_int(&b->bparse, "private", private);
	bpapi_set_int(&b->bparse, "all", private + company);

	/* Total is equal to the total number of ads for the current filter (private-only, company-only, or all)  */
	if (unfiltered < 1)
		return;

	/* Log search hits */
	if (0 < unfiltered && unfiltered <= bconf_get_int(b->conf->root, "common.log.searchhits.maxhits") &&
	    bconf_get_int(b->conf->root, "common.log.searchhits.activated")) {
		char *log_string = NULL;
		log_string = apr_psprintf(b->r->pool, "SEARCHHITS: Got search with %d hits (logging up to %d hits)", unfiltered, bconf_get_int(b->conf->root, "common.log.searchhits.maxhits"));
		apr_table_add(b->r->notes, "searchhits_log", log_string);
	}

	/* Calculate the total number of pages in the result */
	if (filtered == b->ads_per_page)
		pages = (filtered / b->ads_per_page);
	else
		pages = (filtered / b->ads_per_page) + ((filtered % b->ads_per_page) ? 1 : 0);

	current_page = b->offset / b->ads_per_page + 1;

	/* Calculate the maximum number of page-links */
	if (bconf_get(b->conf->root, "common.paginator")) {
		/* New style */
		if (current_page > bconf_get_int(b->conf->root, "common.paginator.4.num_pages"))
			max_pages = bconf_get_int(b->conf->root, "common.paginator.4.max_pages");
		else if (current_page > bconf_get_int(b->conf->root, "common.paginator.3.num_pages"))
			max_pages = bconf_get_int(b->conf->root, "common.paginator.3.max_pages");
		else if (current_page > bconf_get_int(b->conf->root, "common.paginator.2.num_pages"))
			max_pages = bconf_get_int(b->conf->root, "common.paginator.2.max_pages");
		else
			max_pages = bconf_get_int(b->conf->root, "common.paginator.1.max_pages");
	} else {
		/* Old style */
		if (current_page > 1000 - 5)
			max_pages = bconf_get_int(b->conf->root, "common.max_page_links_1000") ?: 10;
		else if (current_page > 100 - 20)
			max_pages = bconf_get_int(b->conf->root, "common.max_page_links_100") ?: 12;
		else
			max_pages = bconf_get_int(b->conf->root, "common.max_page_links") ?: 20;
	}

	/* Check which navigation selections that are necessary */
	if (pages > 1) {
		/* Set first and previous */
		if (current_page != 1)
			bpapi_set_int(&b->bparse, "nav_prev_offset", current_page - 1);

		bpapi_set_int(&b->bparse, "nav_total_pages", pages);

		/* Set last and next*/
		if (current_page == pages) {
			bpapi_set_int(&b->bparse, "nav_first_offset", 1);
		} else {
			bpapi_set_int(&b->bparse, "nav_last_offset", pages);
			bpapi_set_int(&b->bparse, "nav_next_offset", current_page + 1);
		}

		/* Page scrolling does not start until we reach max_pages */
		if (current_page < max_pages) {
			start_page = 1;
			end_page = max_pages > pages ? pages : max_pages;
		} else {
			if (current_page - 1 + max_pages <= pages) {
				start_page = current_page - 1;
				end_page = start_page + max_pages - 1;
			} else {
				end_page = pages;
				start_page = (end_page - max_pages > 1) ? end_page - max_pages + 1 : 1;
			}
		}

		for (i = start_page; i <= end_page; i++) {
			bpapi_set_int(&b->bparse, "nav_offset", i);
			bpapi_set_int(&b->bparse, "nav_selected", (i == current_page) ? 1 : 0);
		}
	}

	/* Calculate the ad span in the current page (for example: 1 - 100) */
	first_ad = b->offset + 1;
	last_ad = current_page * b->ads_per_page;

	if (last_ad > filtered)
		last_ad = filtered;

	/* Just to aviod printing out "501 - 600 out of 23" for errornous offset */
	if (first_ad <= filtered) {
		bpapi_insert(&b->bparse, "list_span", apr_psprintf(b->r->pool, "%d - %d", first_ad, last_ad));
		bpapi_insert(&b->bparse, "list_span_start", apr_psprintf(b->r->pool, "%d", first_ad));
		bpapi_insert(&b->bparse, "list_span_end", apr_psprintf(b->r->pool, "%d", last_ad));
	}

}

void (*mod_list_set_result_navigation)(struct list_call *) = set_result_navigation;

static void
set_list_vars(struct list_call *b) {
	set_helper_variables(b);
}

static void
set_list_navigation(struct list_call *b) {
	(*mod_list_set_result_navigation)(b);
	(*mod_list_set_filters)(b);
}

static void
set_list_templates(struct list_call *b, const char *selector) {
	struct bconf_node *node = bconf_vget(b->conf->root, "mod_list", "templates", selector, NULL);
	if (node) {
		const char *name = bconf_get_string(node, "page_name_template");
		const char *title = bconf_get_string(node, "page_title_template");

		if (title)
			bpapi_insert(&b->bparse, "page_title_template", title);

		if (name)
			bpapi_insert(&b->bparse, "page_name_template", name);

		bpapi_insert(&b->bparse, "content", bconf_get_string(node, "content"));
	} else {
		ap_log_rerror(APLOG_MARK, APLOG_ERR, OK, b->r, "(CRIT) mod_list.templates.%s not configured!", selector);
	}
}


/*
 *  Handle the search request and setup the criterias
 */

static void
handle_search_request(struct list_call *b) {
	const char *listing_mode;
	const char *cookie_domain = b->cookie_domain ? b->cookie_domain : bconf_get_string(b->conf->root, "common.cookie.domain");
	int count = 0;
	const char *cookie_name;

	if (b->conf->use_th_param == -1)
		cookie_name = NULL;
	else if (b->conf->use_th_param)
		cookie_name = "th";
	else
		cookie_name = "md";

	if ((listing_mode = get_cookie_with_count(b->r, "mp", &count)) && bconf_get_int(b->conf->root, "common.listing_mode.mp.enabled"))
		strncpy(b->mode, "mp", 2);
	else if (cookie_name && (listing_mode = get_cookie_with_count(b->r, cookie_name, &count))) {
		if (b->conf->use_th_param)
			strncpy(b->mode, atoi(listing_mode) ? "th" : "li", 2);
		else
			strncpy(b->mode, listing_mode, 2);

		if (bconf_in_list(b->mode, "common.listing_mode.modes", b->conf->root) == -1
				|| (strncmp(b->mode, "mp", 2) == 0))
			strncpy(b->mode, bconf_get_string_default(b->conf->root, "common.listing_mode.default", "th"), 2);
	} else {
		/* Default */
		strncpy(b->mode, bconf_get_string_default(b->conf->root, "common.listing_mode.default", "th"), 2);
	}

	if (handle_search_request_local)
		handle_search_request_local(b);

	/* Parse the query string */
	set_search_query(b, NULL);

	/* Set listing mode/map cookie in li */
	if (strncmp(b->mode, "mp", 2) == 0) {
		set_cookie(b->r, "mp", "1", cookie_domain, 0);
	} else if (cookie_name) {
		if (b->conf->use_th_param)
			set_cookie_with_expiry(b->r, "th", strncmp(b->mode, "th", 2) == 0 ? "1" : "0",
					cookie_domain,
					bconf_get_int(b->conf->root, "common.listing_mode.cookie_expire_days"), 0);
		else
			set_cookie_with_expiry(b->r, "md", b->mode, cookie_domain,
					bconf_get_int(b->conf->root, "common.listing_mode.cookie_expire_days"), 0);
		clear_cookie(b->r, "mp", cookie_domain);
	}

	/* Clear the non-domain cookie if it's set. */
	if (count > 1 && strncmp(b->mode, "mp", 2) != 0)
		clear_cookie(b->r, cookie_name, NULL);
}

/*
 *
 */

static int
is_ad_id(const char *query) {
	const char *ptr;

	if (query == NULL)
		return FALSE;

	/*
	 * simple workaround for phone numbers
	 *
	 * stuff starting with '0' is not an ad_id
	 */
	if (*query == '0')
		return FALSE;

	for (ptr = query; *ptr; ptr++)
		if (!isdigit(*ptr))
			return FALSE;

	/* Check that the length is equal to at least 6 digits */
	return (ptr > query + 5);
}

static void
get_search_info (int state, int info, const char *key, const char **value, void *data) {
	switch (state) {
	case BS_INFO:
		/* ap_log_error(APLOG_MARK, APLOG_NOTICE, OK, NULL, "info: (%s) -> (%s)", key, value); */
		if (strcmp(key, "filtered") == 0) {
			*(int *)data = atoi(*value);
		}
		break;
	}
}

int
check_search_query(struct list_call *b, struct bsconf *bc) {
	struct bsearch_api bsearch;
	int res = -1;
	int total = 0;
	char *query = NULL;

	/* Obviously, the query should not be sent if unset */
	if (b->query == NULL)
		return 0;

	query = apr_psprintf(b->r->pool, "%s", b->query ? b->query : "0 lim:0");

	if (bsearch_init_bsconf(&bsearch, bc, b->remote_addr) == 0) {
		if (bsearch_search(&bsearch, query, get_search_info, &total) == 0) {
			res = total;
		} else {
			ap_log_error(APLOG_MARK, APLOG_ERR, OK, NULL, "bsearch_search(%s) failed %u", query, bsearch.berrno);
		}
	} else {
		ap_log_error(APLOG_MARK, APLOG_CRIT, OK, NULL, "bsearch_init() failed %u", bsearch.berrno);
	}

	bsearch_cleanup(&bsearch);

	return res;
}

/* Loop thorugh category, category parent, all categories */
/* And check ads in region, region near, entire  country */
static int
failover_search(struct list_call *b, int limit, struct bsconf *search_conf) {
	int where = WHERE_UNSET, original_where = 0;
	const char *w = bpapi_get_element(&b->bparse, "w", 0);
	const char *m = bpapi_get_element(&b->bparse, "m", 0);
	const char *ca = bpapi_get_element(&b->bparse, "ca", 0);
	const char *city = bpapi_get_element(&b->bparse, "ci", 0);
	const char *query_string = NULL;
	char *q = NULL;
	char* strtok_state = NULL;
	char *var = NULL;
	int category;
	int total = 0;
	int catlevel = 0;
	int catparent = 0;
	const char *qs_cat;
	char cat_buf[32];
	int failover_cat = 1;
	const char * caller;

	enum tristate failover_near_enabled = bconf_get_tristate(b->conf->root, "failover_near.enabled", TRI_TRUE);

	category = b->category;

	if (!bconf_count(bconf_get(b->conf->root, "cat")))
		failover_cat = 0;
	else {
		catlevel = bconf_get_int(b->conf->root, apr_psprintf(b->r->pool, "cat.%d.level", category));
		catparent = bconf_get_int(b->conf->root, apr_psprintf(b->r->pool, "cat.%d.parent", category));
	}

	qs_cat = bconf_get_string(b->conf->root, "mod_list.qs.category");
	if (!qs_cat)
		qs_cat = "cg";

	snprintf(cat_buf, sizeof(cat_buf), "%s=", qs_cat);

	if (w && *w)
		where = atoi(w);
	if (where > 100 && where < 200) {
		caller = apr_psprintf(b->r->pool, "%d", where - 100);
		where = WHERE_IN_REGION;
	} else if (where > 200) {
		caller = apr_psprintf(b->r->pool, "%d", where - 200);
		where = WHERE_NEAR_REGION;
	} else if (ca) {
		caller = ca;
	} else {
		caller = "";
	}
	if (m && *m) {
		where = WHERE_IN_MUNIC;
	}
	original_where = where;

	q = apr_psprintf(b->r->pool, "%s", b->args);

	while ((var = strtok_r(q, "&", &strtok_state))) {
		if (strncmp(var, "w=", 2) != 0 &&
		    strncmp(var, cat_buf, strlen(cat_buf)) != 0 &&
		    strncmp(var, "c=", 2) != 0 &&
		    strncmp(var, "ca=", 3) != 0 &&
		    strncmp(var, "m=", 2) != 0)
			query_string = apr_psprintf(b->r->pool, "%s&%s", query_string ? query_string : "", var);
		q = NULL;
	}

	/* Defensive programming -- none of the code below copes with query_string being null */
	if (!query_string)
		query_string = "";

	while (1) {
		if (where != WHERE_ALL) {
			while (1) {
				/* Check for results */
				if (catlevel == 2)
					q = apr_psprintf(b->r->pool, "%s&w=%d&cg=%d&c=%d&ca=%s", query_string, where, catparent, category, caller);
				else
					q = apr_psprintf(b->r->pool, "%s&w=%d&%s=%d&ca=%s", query_string, where, qs_cat, category, caller);

				if (where == WHERE_IN_MUNIC) {
					q = apr_psprintf(b->r->pool, "%s&m=%d", q, atoi(m));
				} else if (city && *city && where > WHERE_SPECIFIC_REGION_START) {
					int ci = atoi(city);
					q = apr_psprintf(b->r->pool, "%s&ci=%d", q, ci);
				}

				set_search_query(b, q);
				total = check_search_query(b, search_conf);

				if (total >= limit || where == WHERE_ALL)
					break;

				switch (where) {
				case WHERE_IN_MUNIC:
					where = WHERE_IN_REGION;
					break;
				case WHERE_UNSET:
				case WHERE_IN_REGION:
					/* expand directly to entire contry */
					if (failover_near_enabled != TRI_FALSE)
						where = WHERE_NEAR_REGION;
					else 
						where = WHERE_ALL;
					break;
				case WHERE_NEAR_REGION:
					where = WHERE_ALL;
					break;
				default:
					where = WHERE_IN_REGION;
					break;
				}
			}
		} else {
			if (catlevel == 2)
				q = apr_psprintf(b->r->pool, "%s&ca=%s&w=%d&cg=%d&c=%d", query_string, caller,  where, catparent, category);
			else
				q = apr_psprintf(b->r->pool, "%s&ca=%s&w=%d&%s=%d", query_string, caller, where, qs_cat, category);
			set_search_query(b, q);
			total = check_search_query(b, search_conf);
		}

		/* If no result, check for parent category */
		if (category != 0 && total < limit && failover_cat) {
			if (catlevel > 0) {
				/* Get parent */
				category = catparent;
				catparent = bconf_get_int(b->conf->root, apr_psprintf(b->r->pool, "cat.%d.parent", category));
				catlevel--;
			} else {
				/* All categories */
				category = 0;
			}
			where = original_where;
		} else {
			break;
		}
	}

	/* save the real search query for template */
	if (q && q[0]) {
		bpapi_insert(&b->bparse, "xsq", (q[0] == '&')? q+1:q);
	}

	/* See also if there has been category failover */
	if (b->category != category && total > 0) {
		bpapi_set_int(&b->bparse, "search_failover_category", category);
	}

	if (where != WHERE_UNSET && (original_where != where) && total > 0) {
		/* 
		 * Determine and set failover_w
		 * Set to 200 + caller if expanded to near regions
		 * search_failover_where indicates what kind of expansion while failover_w determines the value of w
		 */
		switch (where) {
			case WHERE_NEAR_REGION:
				if (w && atoi(w) > 100)
					bpapi_set_int(&b->bparse, "failover_w", atoi(caller) + 200);
				else
					bpapi_set_int(&b->bparse, "failover_w", WHERE_NEAR_REGION);
				break;
			case WHERE_ALL:
				bpapi_set_int(&b->bparse, "failover_w", WHERE_ALL);
				break;
			default:
				ap_log_error(APLOG_MARK, APLOG_NOTICE, OK, NULL, "failover_search(): failover is not near region nor entire country");

		}
		if (w && *w) {
			bpapi_set_int(&b->bparse, "org_w", atoi(w));
		}
		bpapi_set_int(&b->bparse, "search_failover_where", where);
	}

	return 0;
}

int (*mod_list_failover_search)(struct list_call *b, int limit, struct bsconf *search_conf) = failover_search;

static void
global_session_cb(struct parse_cb_data *d, char *key, int klen, char *value, int vlen) {
	struct list_call *b = d->cb_data;

	bpapi_insert(&b->bparse, apr_psprintf(b->r->pool, "session_%s", key), value);
}

static int
list_outstring_fmt(struct bpapi_output_chain *ochain, const char *fmt, ...) {
	int res;
	struct list_call *call = ochain->data;
	va_list ap;

	va_start(ap, fmt);
	res = ap_vrprintf(call->r, fmt, ap);
	va_end(ap);

	return res;
}

static int
list_outstring_raw(struct bpapi_output_chain *ochain, const char *str, size_t len) {
	struct list_call *call = ochain->data;

	return ap_rwrite(str, len, call->r);
}

const struct bpapi_output list_output = {
	list_outstring_fmt,
	list_outstring_raw,
	NULL,
	NULL
};

static void
list_vtree(struct list_call *call, struct request_rec *r, struct list_conf *conf) {
	filter_state_bconf_init(&call->filter_bconf, FS_SESSION);

	filter_state_bconf_set(&call->filter_bconf, "remote_addr", (void *)call->remote_addr, NULL, FS_SESSION);
	filter_state_bconf_set(&call->filter_bconf, "list_call", call, NULL, FS_SESSION);

	bconf_vtree(&call->filter_thread_conf.vtree, call->thread_conf->filter_state);
	shadow_vtree_init(&call->tvt, &call->filter_thread_conf, &conf->vt);

	bconf_vtree(&call->filter_session_conf.vtree, call->filter_bconf);
	shadow_vtree_init(&call->bparse.vchain, &call->filter_session_conf, &call->tvt);

}

void (*mod_list_call_init_local)(struct list_call *b, request_rec *r, struct list_conf *conf);
void (*mod_list_call_free_local)(struct list_call *b);

static void
list_call_init(struct list_call *b, request_rec *r, struct list_conf *conf, struct language_setup_data *lsd) {
	struct tm lt;
	char tbuf[32];
	char *cookie;
	const char *ua;
	const char *cookiename;
	int wfsession;

	memset(b, 0, sizeof(*b));

	b->conf = conf;
	b->thread_conf = &conf->thread_data[r->connection->id % conf->num_threads]; /* XXX based on mail list info, not verified correct */
	b->r = r;
	b->args = r->args ? apr_pstrdup(r->pool, r->args) : NULL;
	b->remote_addr = get_remote_addr(r, conf->root);
	/* Use detected language for searches */
	if (lsd && lsd->lang)
		b->lang.code = apr_pstrdup(b->r->pool, lsd->lang);
	/* Start timer and check if we have reached a new day */
	gettimeofday(&b->start_time, 0);

	if (b->start_time.tv_sec > conf->date.next_midnight_at)
		setup_date(conf, b->start_time.tv_sec);

	b->bparse.ochain.fun = &list_output;
	b->bparse.ochain.data = b;
	hash_simplevars_init(&b->bparse.schain);
	list_vtree(b, r, conf);

	b->bparse.filter_state = filter_state_bconf;

	if (!b->thread_conf->utf8_cnv) {
		b->thread_conf->utf8_cnv = filter_state(&b->bparse, "ucnv_utf8", fs_ucnv_utf8, NULL);
		b->thread_conf->dst_cnv = filter_state(&b->bparse, "ucnv_dst", fs_ucnv_dst, NULL);
	}

	wfsession = bconf_get_int(conf->root, "common.session.webfront.always");
	if (!wfsession && (cookiename = bconf_get_string(conf->root, "common.session.webfront.boolcookie"))) {
		cookie = get_cookie(r, cookiename);
		if (cookie && strcmp(cookie, "1") == 0)
			wfsession = 1;
	}
	if (wfsession && (cookiename = bconf_get_string(conf->root, "common.session.webfront.cookie"))) {
		if ((cookie = get_cookie(r, cookiename))) {
			/* Get global session data from cache */

			char *global_session = cache_get(&b->conf->session, cookie, "global");

			if (global_session) {
				parse_query_string(apr_pstrdup(b->r->pool, global_session), global_session_cb, b, NULL, 0, 0, NULL);
				free(global_session);
			} else
				ap_log_error(APLOG_MARK, APLOG_DEBUG, OK, NULL, "No global session for %s", cookie);
		}
	}

	strftime(tbuf, sizeof (tbuf), "%F %T", localtime_r(&b->start_time.tv_sec, &lt));
	bpapi_insert(&b->bparse, "NOW", tbuf);

	bpapi_set_int(&b->bparse, "TSNOW", (int)b->start_time.tv_sec);
	if ((ua = apr_table_get(b->r->headers_in, "User-Agent")))
		bpapi_insert(&b->bparse, "REMOTE_BROWSER", ua);
	bpapi_insert(&b->bparse, "FORWARDED_PROTO", get_forwarded_proto(r, conf->root));
	bpapi_insert(&b->bparse, "REMOTE_ADDR", b->remote_addr);
	if (b->r->args)
		bpapi_insert(&b->bparse, "QUERY_STRING", r->args);

	if (apr_table_get(b->r->headers_in, "Host")) {
		bpapi_insert(&b->bparse, "REQUEST_HOST", apr_table_get(b->r->headers_in, "Host"));
	}

	bpapi_insert(&b->bparse, "appl", "li");
	if (mod_list_call_init_local)
		mod_list_call_init_local(b, r, conf);
}

static void
list_call_free(struct list_call *b) {
	struct timeval stop_time;
	char *report_info;

	if (mod_list_call_free_local)
		mod_list_call_free_local(b);

	/* Stop timer */
	gettimeofday(&stop_time, 0);

	/* Write the report */
	report_info = apr_psprintf(b->r->pool, "%s %ld",  b->conf->hostname, 1000000 * (stop_time.tv_sec - b->start_time.tv_sec) + stop_time.tv_usec - b->start_time.tv_usec);
	apr_table_add(b->r->headers_out, "X-Yapo-Info", report_info);

	bpapi_output_free(&b->bparse);
	bpapi_simplevars_free(&b->bparse);
	bpapi_vtree_free(&b->bparse);
	bsearch_cleanup(&b->bsearch);
	filter_state_bconf_free(&b->filter_bconf);
}

/*
 * Store url support.
 */

struct csearch_store_id {
	request_rec *r;
	char *c_id;
};

static void
t_csearch_data(int state, int info, const char *key, const char **value, void *data) {
	struct csearch_store_id *c = (struct csearch_store_id *)data;

	switch (state) {
	case BS_INFO:
		break;
	case BS_COLS:
		break;
	case BS_DONE:
		break;
	case BS_VALUE:
		if (strcmp(key, "c_id") == 0) {
			/*ap_log_error(APLOG_MARK, APLOG_NOTICE, OK, NULL, "data (%u): (%s) = (%s)", info, key, value); */
			c->c_id = apr_psprintf(c->r->pool, "%s", *value);
		}
		break;
	case BS_NOAD:
		break;
	default:
		ap_log_error(APLOG_MARK, APLOG_CRIT, OK, NULL, "Unknown state (%d) in add_bsearch_data", state);
		break;
	}
}

static char*
get_store_id(request_rec *r, struct list_conf *conf) {
	char *query;
	struct csearch_store_id c;
	struct bsearch_api bsearch;
	char *dp;
	const char *remote_addr = get_remote_addr(r, conf->root);
	struct bsconf *csearch_conf;

	/* Filter input */
	if (conf->store_regex && ap_regexec(conf->store_regex, r->uri, 0, NULL, 0) != 0) {
		/* We have got something that is not possibly a store.*/
		return NULL;
	}

	ap_log_error(APLOG_MARK, APLOG_DEBUG, OK, NULL, "Looking for store at %s", r->uri);

	c.r = r;
	c.c_id = NULL;
	/* Handle the query as defined (or not) in bconf */
	if ( bconf_get_int(conf->root, "mod_list.list_stores.c_url_path_quote_value") ) {
		/* Initialize the query */
		query = apr_psprintf(r->pool, "0 lim:1 c_url_path:\"%s\"", (r->uri)+1);
	}
	else {
		/* Initialize the query */
		query = apr_psprintf(r->pool, "0 lim:1 c_url_path:%s", (r->uri)+1);
		/* Replace - with _ cause search interprets dash as range */
		for (dp = query; (dp = strchr(dp, '-')); dp++)
			*dp = '_';
	}

	{	/* XXX - terrible layering violation. But so is the rest of mod_list. */
		struct bpapi ba = { {0} };
		ba.vchain = conf->vt;
		ba.filter_state = filter_state_bconf;
		csearch_conf = filter_state(&ba, "bsconf_csearch", filter_state_bsconf, (void *)"csearch");
	}

	/* Perform a csearch, looking for c_id. */
	if (bsearch_init_bsconf(&bsearch, csearch_conf, remote_addr) == 0) {
		if (bsearch_search(&bsearch, query, t_csearch_data, &c) != 0) {
			ap_log_rerror(APLOG_MARK, APLOG_ERR, OK, r, "bsearch_search(%s) failed: %s", query,
				      bsearch_errstr(bsearch.berrno));
			bsearch_cleanup(&bsearch);
			return NULL;
		}
	} else {
		ap_log_error(APLOG_MARK, APLOG_CRIT, OK, NULL, "bsearch_init() failed: %s", bsearch_errstr(bsearch.berrno));
		return NULL;
	}
	bsearch_cleanup(&bsearch);

	return c.c_id;
}

static char*
get_store_url(request_rec *r, struct list_conf *conf) {
	char *id;

	id = get_store_id(r, conf);

	if (id) {
		return apr_psprintf(r->pool, "id=%s%s%s", id, r->args ? "&" : "", r->args ?: "");
	} else {
		return NULL;
	}
}

/*
 *  The request handler
 */

static int
list_handler(request_rec *r) {
	struct list_call b;
	const char *referer, *appl = NULL;
	const char *application_name = NULL;
	int set_focus, from_index;
	int res = 0;
	const char *mean = apr_table_get(r->subprocess_env, "elak");
	char *bs_buffer = NULL;
	int failover;
	char *replace_args = NULL;
	struct list_conf *conf = ap_get_module_config(r->server->module_config, &list_module);
	const char *cookiedomain;
	const char *lang = NULL;
	const char *country = NULL; 
	const char *host;

	struct language_setup_data lsd = { .type = TYPE_LIST_CONF, .conf = conf };

	language_setup(r, conf->root, &lsd);

	lang = lsd.lang;
	country = lsd.country;

	if (strcmp(r->handler, "store-handler") == 0) {
		replace_args = get_store_url(r, conf);
		ap_log_error(APLOG_MARK, APLOG_DEBUG, OK, NULL, "get_store_url(%s?%s) -> %s", r->uri, r->args, replace_args);
		if (!replace_args)
			return DECLINED;
	} else {
		if (strcmp(r->handler, "list-handler") != 0)
			return DECLINED;

		if (strstr(r->uri, ".htm") != NULL) {
			/* Pass on to template handler. */
			return DECLINED;
		}

		if (mod_list_friendlyurl_converter) {
			replace_args = (*mod_list_friendlyurl_converter)(r, conf);
			ap_log_error(APLOG_MARK, APLOG_DEBUG, OK, NULL, "friendlyurl_converter(%s?%s) -> %s", r->uri, r->args, replace_args);
		}
	}

	list_call_init(&b, r, conf, &lsd);

	if (replace_list_vars_local)
		replace_args = replace_list_vars_local(&b, r, replace_args);

	if (replace_args)
		b.args = replace_args;

	handle_search_request(&b);

	if (mean)
		b.elak = atoi(mean);

	/*
	 * Don't let google cache pages since they break horribly.
	 */
	if (!bconf_get_int(b.conf->root, "mod_list.googlebot_allow_archive"))
		bpapi_insert(&b.bparse, "googlebot_noarchive", "1");

	if (!lang || !bconf_vget(b.conf->root, "common.lang", lang, NULL)) {
		lang = bconf_get_string(b.conf->root, "common.default.lang");
		bpapi_insert(&b.bparse, "DEFAULT_LANGUAGE_USED", "1") ;
	}

	if (r->uri) {
		/* Skip / */
		bpapi_insert(&b.bparse, "REQUEST_URI", r->uri + 1);
	}
	/*
	 * Save search query in cookie
	 * If there's no cookie domain configured, we assume that the local code will deal with setting the appropriate cookies.
	 */
	cookiedomain = bconf_get_string(b.conf->root, "common.cookie.domain");
	if (cookiedomain && !*cookiedomain)
		cookiedomain = NULL;

	/* Call a couple of functions in order to setup template variables */
	set_list_vars(&b);
	set_searchbox(&b);

	/* Check if the query is equal to an ad-id - if so, show the ad */
	if (!bconf_get_int(conf->root, "mod_list.no_id_search")) {
		const char *q = b.q_raw;
		if (q && is_ad_id(q)) {
			const char *l = bpapi_get_element(&b.bparse, "l", 0);
			const char *layout = "";

			if (l && atoi(l))
				layout = apr_psprintf(r->pool, "&l=%d", atoi(l));

			clear_cookie(r, "csw", cookiedomain);
			set_cookie(r, "sq", b.args, cookiedomain, 0);

			const char *find_host = apr_table_get(r->headers_in, "Host");
			const char *mob_host = bconf_get_string(b.conf->root, "common.base_url.mobile");
			if(find_host && mob_host && strstr(mob_host, find_host)) {
				apr_table_add(r->headers_out, "Location", apr_psprintf(r->pool, "%s/vi/%s.htm?ca=%s%s",
							mob_host, q,
							bpapi_get_element(&b.bparse, "ca", 0) ?: "",
							(layout?layout:"")));
			} else {
				apr_table_add(r->headers_out, "Location", apr_psprintf(r->pool, "%s/vi/%s.htm?ca=%s%s",
							bconf_get_string(b.conf->root, "common.base_url.vi"), q,
							bpapi_get_element(&b.bparse, "ca", 0) ?: "",
							(layout?layout:"")));
			}

			list_call_free(&b);
			return HTTP_MOVED_PERMANENTLY;
		}
	}

	/* Check if the request comes from the index-page */
	from_index = FALSE;

	if ((referer = apr_table_get(r->headers_in, "Referer"))) {
		bpapi_insert(&b.bparse, "REFERER", referer);
	}
	if (b.conf->index_referer && referer && referer[0])
		from_index = (ap_regexec(b.conf->index_referer, referer, 0, NULL, 0) == 0);

	/* Allow a hostname check for whether we should actualy do the search or not. */
	int skipsearch = 0;
	if ((host = apr_table_get(b.r->headers_in, "Host"))) {
		char *escaped_host = replace_chars(host, -1, "._");

		if (bconf_vget_int(b.conf->root, "mod_list.no_search.host", escaped_host, NULL))
			skipsearch = 1;
		free(escaped_host);
	}

	/* Now set and the send the actual search string to bsearch */
	if (b.company_list == 0) {
		struct bsconf *asearch_conf = filter_state(&b.bparse, "bsconf_asearch", filter_state_bsconf, (void *)"asearch");
		if (!asearch_conf) {
			res = 1;
		} else {
			if (b.company_id == 0 && bconf_get_int(b.conf->root, "failover.enabled") == 1) {
				/* Failover listing configuration */
				if (from_index)
					failover = bconf_get_int(b.conf->root, "failover.index");
				else
					failover = bconf_get_int(b.conf->root, "failover.search");
				if (mod_list_failover_search)
					mod_list_failover_search(&b, failover, asearch_conf);
			}	
			if (!skipsearch)
				res = send_search_query(&b, asearch_conf);
		}
	}

	if (b.args) {
		if (bconf_get_int(b.conf->root, "mod_list.sq_adjust_w")) {
			int failover_w;
			char *w;
			if ((failover_w = bpapi_get_int(&b.bparse, "search_failover_where"))) {
				if ((w = strstr(b.args, "w="))) {
					w[2] = '0' + failover_w;
				}
			}
		}
		if (!bpapi_get_int(&b.bparse, "has_failover"))
			set_cookie(r, "sq", b.args, cookiedomain, 0);
	} else {
		/* Default to entire country if no arguments to url is set */
		set_cookie(r, "sq", "w=3", cookiedomain, 0);
	}

	/* Now talk with the company search engine */
	if (res == 0 && (b.company_list || b.company_id)) {
		set_company_query(&b);

		if (b.bsearch.buffer) {
			/* hack, save any existing buffer for freeing later (it might still be in use) */
			bs_buffer = b.bsearch.buffer;
			b.bsearch.buffer = NULL;
			bsearch_cleanup(&b.bsearch);
		}
		if (!skipsearch) {
			struct bsconf *csearch_conf;
			if ((csearch_conf = filter_state(&b.bparse, "bsconf_csearch", filter_state_bsconf, (void *)"csearch")))
				res = send_search_query(&b, csearch_conf);
			else
				res = 1;
		}

		/* Split the email-field */
		if (b.company_id) {
			char *email, *ptr, *delim=NULL, *url_path, *dp;
			email = apr_pstrdup(r->pool, bpapi_get_element(&b.bparse, "c_email", 0));
			url_path = apr_pstrdup(r->pool, bpapi_get_element(&b.bparse, "c_url_path", 0));

			if (url_path) {
				bpapi_insert(&b.bparse, "c_url_path_orig", url_path);
				if ( !bconf_get_int(conf->root, "mod_list.list_stores.c_url_path_quote_value") ) {
					for (dp = url_path; (dp = strchr(dp, '_')); dp++)
						*dp = '-';
					bpapi_insert(&b.bparse, "c_url_path_dash", url_path);
				}
			}

			if (email) {
				while ( (ptr = strtok_r(email, ",", &delim)) ) {
					trim(ptr);
					bpapi_insert(&b.bparse, "c_email_list", ptr);
					email = NULL;
				}
			}
		}
	}

	if (list_start_call_local)
                list_start_call_local(&b, res);
	
	if (bpapi_has_key(&b.bparse,"tmpl_lang")) {
		lang = apr_pstrdup(r->pool,bpapi_get_element( &b.bparse , "tmpl_lang", 0));
	}

	if (res == 0) {
		int filtered = atoi(bpapi_get_element(&b.bparse, "filtered", 1) ?: "0");
		struct bconf_node *js_node;

		set_list_navigation(&b);

		if (b.company_id) {
			application_name = "view_store";
			if (filtered == 0) {
				ap_log_rerror(APLOG_MARK, APLOG_ERR, OK, b.r, "list_handler store %u not found", b.company_id);
				set_list_templates(&b, "view_missing");
			} else {
				struct bpapi_vtree_chain vtree;

				set_list_templates(&b, "view_company"); /* view_company */
				appl = "butik";
				/* Statistics */
				bconf_vtree(&vtree, bconf_get(b.conf->root, "common.statpoints"));
				stat_log(&vtree, "VIEW", apr_psprintf(b.r->pool, "store_%d", b.company_id));
				vtree_free(&vtree);
			}
		} else {
			if (b.company_list) {
				application_name = "list_stores";
				appl = "butik";
			} else {
				appl = "search";
				application_name = "list_ads";
			}

			if (bconf_get_int(b.conf->root, "merken.status") == 1) {
				appl = "merken_search";
				application_name = "merken";
				set_list_templates(&b, "merken");
				bpapi_insert(&b.bparse, "merken_page", "listing");
			} else {
				set_list_templates(&b, "list_ads");
			}

		}

		bpapi_insert(&b.bparse, "meta_template", bconf_get_string_default(b.conf->root, "mod_list.meta_template", "list/meta.html"));

		if ((js_node = bconf_vget(b.conf->root, "mod_list", application_name, "js", NULL))) {
			int i;
			const char *h;

			for (i = 0 ; (h = bconf_value(bconf_get(bconf_byindex(js_node, i), "name"))) ; i++) {
				const char *headscripts = bconf_get_string_default(bconf_byindex(js_node, i), "variable", "headscripts");
				const char *bu = bconf_get_string(bconf_byindex(js_node, i), "base_url");
				if (bu)
					h = apr_psprintf(r->pool, "%s%s", bconf_vget_string(b.conf->root, "common.base_url", bu, NULL) ?: "", h);
				if (bconf_get_int(bconf_byindex(js_node, i), "addlang")) {
					/* to avoid caching issue, specially array_v2 */
					if (bconf_get_int(bconf_byindex(js_node, i), "addcountry"))
						bpapi_insert(&b.bparse, headscripts, apr_psprintf(r->pool, "%s?lang=%s&amp;country=%s", h, lang, country));
					else
						bpapi_insert(&b.bparse, headscripts, apr_psprintf(r->pool, "%s?lang=%s", h, lang));
				} else
					bpapi_insert(&b.bparse, headscripts, h);
			}
		}

		if (apr_table_get(b.vars, "v") && strcmp(apr_table_get(b.vars, "v"), "1") == 0) {
			const char *h = bconf_get_string(b.conf->root, "mod_list.video.js");

			if (h)
				bpapi_insert(&b.bparse, "headscripts", h);
		}

		if (application_name)
			bpapi_insert(&b.bparse, "application_name", application_name);

		if (apr_table_get(r->subprocess_env, "REDIRECT_search_engine_redir") || apr_table_get(b.vars, "ser")) {
			bpapi_insert(&b.bparse, "search_engine_redir", "1");
		}

		/* Check if the request comes from the index-page */
		if (from_index) {
			bpapi_insert(&b.bparse, "from_index", "1");
			set_focus = TRUE;
		} else {
			set_focus = b.set_focus;
		}

		if (set_focus)
			bpapi_insert(&b.bparse, "set_focus", "1");

		if (appl) {
			bpapi_insert(&b.bparse, "domain", appl);

			if (apr_table_get(r->headers_in, "Accept") &&
			    (strstr(apr_table_get(r->headers_in, "Accept"), "vnd.wap") != NULL)) {
				bpapi_insert(&b.bparse, "display_mobile", "1");
			}
		}
	} else {
		clear_cookie(r, "csw", cookiedomain);
		bpapi_insert(&b.bparse, "error_message", apr_psprintf(r->pool, "list code = %d", res));
		set_list_templates(&b, "error_page_unexpected");
	}

	const char *ct = "text/html";
	const char *charset = bconf_get_string(b.conf->root, "common.charset");
	if (charset && strcmp(charset, "none") != 0)
		ct = apr_psprintf(r->pool, "%s; charset=%s", ct, charset);

	ap_set_content_type(r, ct);

	if (bconf_get(b.conf->root, "mod_list.wrapper_template"))
		call_template_lang(&b.bparse, bconf_get_string(b.conf->root, "mod_list.wrapper_template"), lang, country);
	else
		call_template_lang(&b.bparse, "common/general0.html", lang, country);

	if (list_end_call_local)
		list_end_call_local(&b);

	if (bs_buffer)
		free(bs_buffer);

	list_call_free(&b);

	return OK;
}

static const struct tpvar *
errorlog(struct bpapi *api, struct tpvar *dst, enum bpcacheable *cc, int argc, const struct tpvar **argv) {
	struct list_call *current_call = filter_state(api, "list_call", NULL, NULL);
	char *fa0 = NULL, *fa1 = NULL;

	*cc = BPCACHE_CANT;
        switch (argc) {
        case 0:
                ap_log_rerror(APLOG_MARK, APLOG_ERR, OK, current_call->r, "errorlog() called with no args");
                break;
        case 1:
                ap_log_rerror(APLOG_MARK, APLOG_ERR, OK, current_call->r, "Error: %s", tpvar_str(argv[0], &fa0));
                break;
        case 2:
                ap_log_rerror(APLOG_MARK, APLOG_ERR, OK, current_call->r, "Error: %s : %s", tpvar_str(argv[0], &fa0), tpvar_str(argv[1], &fa1));
                break;
        default:
                ap_log_rerror(APLOG_MARK, APLOG_ERR, OK, current_call->r, "errorlog() called with %d args", argc);
                break;
        }
	free(fa0);
	free(fa1);
	return TPV_SET(dst, TPV_NULL);
}
ADD_TEMPLATE_FUNCTION(errorlog);

static const struct tpvar *
set_expire(struct bpapi *api, struct tpvar *dst, enum bpcacheable *cc, int argc, const struct tpvar *argv[]) {
	struct list_call *current_call = filter_state(api, "list_call", NULL, NULL);
	const char *maxage;
	char *fma = NULL;

/*	if (current_call->hasoutput) {
		ap_log_rerror(APLOG_MARK, APLOG_ERR, OK, current_call->r, "uri: %s, args: %s, template call has output before calling &set_expire()", current_call->r->uri, current_call->r->args);
	}
*/

	if (argc > 0 && (maxage = tpvar_str(argv[0], &fma))) {
		char buf[1024];
		snprintf(buf, 1024, "max-age=%s", maxage);
		apr_table_add(current_call->r->headers_out, "Cache-Control", apr_pstrdup(current_call->r->pool, buf));
	}
	free(fma);
	return TPV_SET(dst, TPV_NULL);
}
ADD_TEMPLATE_FUNCTION(set_expire);

static const struct tpvar *
template_get_cookie(struct bpapi *api, struct tpvar *dst, enum bpcacheable *cc, int argc, const struct tpvar *argv[]) {
	struct list_call *current_call = filter_state(api, "list_call", NULL, NULL);
	char *res = NULL;

	if (argc == 2) {
		char *fp, *fr;
		const char *param = tpvar_str(argv[0], &fp);
		const char *regex = tpvar_str(argv[1], &fr);
		const char *value = get_cookie(current_call->r, param);

		if (value && strlen(value) && regex && strlen(regex)) {
			pcre *pregex;
			const char *err;
			int erri;
			int flags = PCRE_DOLLAR_ENDONLY | PCRE_DOTALL | PCRE_NO_AUTO_CAPTURE | PCRE_CASELESS;
			const char *charset = vtree_get(&api->vchain, "common", "charset", NULL);

			if (charset && strcmp(charset, "UTF-8") == 0)
				flags |= PCRE_UTF8;

			pregex = pcre_compile(regex,
					      flags,
					      &err, &erri, NULL);

			if (pregex) {
				if ((erri = pcre_exec(pregex, NULL, value, strlen(value), 0, 0, NULL, 0)) >= 0) {
					res = xstrdup(value);
				}
				pcre_free(pregex);
			}
		}

		free(fp);
		free(fr);
	}

	*cc = BPCACHE_CANT;
	return TPV_SET(dst, res ? TPV_DYNSTR(res) : TPV_NULL);
}
ADD_TEMPLATE_FUNCTION_WITH_NAME(template_get_cookie, get_cookie);


static const struct tpvar *
template_has_cookie(struct bpapi *api, struct tpvar *dst, enum bpcacheable *cc, int argc, const struct tpvar *argv[]) {
	struct list_call *current_call = filter_state(api, "list_call", NULL, NULL);
	*cc = BPCACHE_CANT;

	if (argc >= 1) {
		char *fa0;
		const char *value = get_cookie(current_call->r, tpvar_str(argv[0], &fa0));
		free(fa0);
		if (value != NULL && (argc == 1 || *value != '\0')) {
			return TPV_SET(dst, TPV_INT(1));
		}
	}

	return TPV_SET(dst, TPV_INT(0));
}
ADD_TEMPLATE_FUNCTION_WITH_NAME(template_has_cookie, has_cookie);

static int 
outstring_fmt_null(struct bpapi_output_chain *ochain, const char *fmt, ...) { 
        return 0; 
} 
 
static int 
outstring_raw_null(struct bpapi_output_chain *ochain, const char *str, size_t len) { 
        return 0; 
} 
 
const struct bpapi_output null_output = { 
        outstring_fmt_null, 
        outstring_raw_null, 
        NULL, 
        NULL
};

static const struct tpvar *
template_redirect(struct bpapi *api, struct tpvar *dst, enum bpcacheable *cc, int argc, const struct tpvar *argv[]) {
	struct list_call *current_call = filter_state(api, "list_call", NULL, NULL);
	const char *loc;
	char *fl = NULL;

        if (current_call->r->bytes_sent) {
		ap_log_rerror(APLOG_MARK, APLOG_ERR, OK, current_call->r, "uri: %s, args: %s, referer: %s, template call has output before calling &redirect()", current_call->r->uri, current_call->r->args, apr_table_get(current_call->r->headers_in, "Referer"));
	}

	if (argc > 0 && (loc = tpvar_str(argv[0], &fl))) {
		apr_table_add(current_call->r->headers_out, "Location", apr_pstrdup(current_call->r->pool, loc));

		current_call->bparse.ochain.fun = &null_output;

		if (argc == 2)
			current_call->r->status = tpvar_int(argv[1]);
		else
			current_call->r->status = HTTP_MOVED_TEMPORARILY;
	}
	free(fl);
	*cc = BPCACHE_CANT;
	return TPV_SET(dst, TPV_NULL);
}
ADD_TEMPLATE_FUNCTION_WITH_NAME(template_redirect, redirect);

struct _set_cookie {
	const char *name;
	char type;
	int time;
	char *value;
	struct list_call *call;
	char *fn;
	int httponly;
};

static int
set_cookie_outstring_raw(struct bpapi_output_chain *ochain, const char *str, size_t len) {
	struct _set_cookie *sc = ochain->data;

	if (str && strlen(str))
		sc->value = apr_psprintf(sc->call->r->pool, "%s%s", sc->value == NULL ? "" : sc->value, str);

	return 0;
}

static int
init_set_cookie(const struct bpfilter *filter, struct bpapi *api, int argc, const struct tpvar **argv) {
	struct _set_cookie *sc = BPAPI_OUTPUT_DATA(api);
	struct list_call *current_call = filter_state(api, "list_call", NULL, NULL);
	char *fsa = NULL;

	sc->httponly = 0;

	switch (argc) {
	case 4:
		sc->httponly = 1;
	case 3:
		sc->time = tpvar_int(argv[2]);
		sc->type = *tpvar_str(argv[1], &fsa);
		break;
	case 2:
		sc->time = tpvar_int(argv[1]);
	case 1:
		break;
	default:
		return 1;
	}
	sc->name = tpvar_str(argv[0], &sc->fn);
	sc->value = NULL;
	sc->call = current_call;
	free(fsa);

	return 0;
}

static void
fini_set_cookie(struct bpapi_output_chain *ochain) {
	struct _set_cookie *sc = ochain->data;
	const char *cookie_domain = sc->call->cookie_domain ? sc->call->cookie_domain : bconf_get_string(sc->call->conf->root, "common.cookie.domain");

	if (sc) {
		if (sc->value && *sc->value) {
			if (!sc->type)
				set_cookie(sc->call->r, sc->name, sc->value, cookie_domain, sc->httponly);
			else if (sc->time && sc->type == 'd')
				set_cookie_with_expiry(sc->call->r, sc->name, sc->value, cookie_domain, sc->time, sc->httponly);
			else if (sc->time && sc->type == 'm')
				set_cookie_with_expiry_minutes(sc->call->r, sc->name, sc->value, cookie_domain, sc->time, sc->httponly);
		}
		free(sc->fn);
	}
}

static const struct bpapi_output set_cookie_output = {
	self_outstring_fmt,
	set_cookie_outstring_raw,
	fini_set_cookie,
	NULL
};

ADD_OUTPUT_FILTER(set_cookie, sizeof(struct _set_cookie));

struct add_header_data {
	struct buf_string buf;
	struct list_call *call;
	const struct tpvar *name;
};

static int
init_add_header(const struct bpfilter *filter, struct bpapi *api, int argc, const struct tpvar *argv[]) {
	struct add_header_data *data = BPAPI_OUTPUT_DATA(api);

	if (argc >= 1)
		data->name = argv[0];
	data->call = filter_state(api, "list_call", NULL, NULL);

	return 0;
}

static void
fini_add_header(struct bpapi_output_chain *ochain) {
	struct add_header_data *data = ochain->data;

	if (data->name && data->buf.buf) {
		const char *name;

		TPVAR_STRTMP(name, data->name);
		apr_table_add(data->call->r->err_headers_out, apr_pstrdup(data->call->r->pool, name), apr_pstrdup(data->call->r->pool, data->buf.buf));
	}
	free(data->buf.buf);
}

static const struct bpapi_output add_header_output = {
	bufstring_outstring_fmt,
	bufstring_outstring_raw,
	fini_add_header,
	NULL
};


ADD_OUTPUT_FILTER(add_header, sizeof(struct add_header_data));

static const struct tpvar *
template_mime_type(struct bpapi *api, struct tpvar *dst, enum bpcacheable *cc, int argc, const struct tpvar *argv[]) {
	struct list_call *current_call = filter_state(api, "list_call", NULL, NULL);
	*cc = BPCACHE_CANT;

	/*
	if (current_call->hasoutput) {
		ap_log_rerror(APLOG_MARK,
				APLOG_ERR,
				OK,
				current_call->r,
				"URI: %s, args: %s, referer: %s, template call has output before calling &mime_type()",
				current_call->r->uri,
				current_call->r->args,
				apr_table_get(current_call->r->headers_in, "Referer"));
	}
	*/
	if (argc >= 1) {
		char *fa0;
		const char *ct = apr_psprintf(current_call->r->pool, "%s", tpvar_str(argv[0], &fa0));
		ap_set_content_type(current_call->r, ct);
		free(fa0);
	}
	return TPV_SET(dst, TPV_NULL);
}
ADD_TEMPLATE_FUNCTION_WITH_NAME(template_mime_type, mime_type);

static const struct tpvar *
template_clear_cookie(struct bpapi *api, struct tpvar *dst, enum bpcacheable *cc, int argc, const struct tpvar *argv[]) {
	struct list_call *current_call = filter_state(api, "list_call", NULL, NULL);

	if (argc == 1) {
		char *fp;
		const char *param = tpvar_str(argv[0], &fp);

		clear_cookie(current_call->r, param, current_call->cookie_domain);
		free(fp);
	}

	*cc = BPCACHE_CANT;
	return TPV_SET(dst, TPV_NULL);
}
ADD_TEMPLATE_FUNCTION_WITH_NAME(template_clear_cookie, clear_cookie);

/*
 * Set secure session cookie
 */
struct _set_secure_cookie {
	const char *name;
	const char *type;
	const char *time;
	char *value;
	struct list_call *call;
	char *fn, *fty, *fti;
};

static int 
set_secure_cookie_outstring_raw(struct bpapi_output_chain *ochain, const char *str, size_t len) {
	struct _set_secure_cookie *sc = ochain->data;

	if (len)
		sc->value = apr_psprintf(sc->call->r->pool, "%s%.*s", sc->value == NULL ? "" : sc->value, (int)len, str);

	return 0;
}

static int
init_set_secure_cookie(const struct bpfilter *filter, struct bpapi *api, int argc, const struct tpvar **argv) {
	struct _set_secure_cookie *sc = BPAPI_OUTPUT_DATA(api);
	struct list_call *current_call = filter_state(api, "list_call", NULL, NULL);
	const char *sc_arg;
	char *fa = NULL;

	if (argc < 1)
		return 0;

	sc->name = tpvar_str(argv[0], &sc->fn);
	sc_arg = argc < 2 ? NULL : tpvar_str(argv[1], &fa);

	/* Backward compability. If argument is numeric, use it as time. Otherwise it's type of time units */
	if (sc_arg && isdigit(*sc_arg)) {
		sc->time = sc_arg;
		sc->fti = fa;
	} else {
		sc->type = sc_arg;
		sc->fty = fa;
		sc->time = argc < 3 ? NULL : tpvar_str(argv[2], &sc->fti);
	}
	sc->value = NULL;
	sc->call = current_call;

	return 0;
}

static void
fini_set_secure_cookie(struct bpapi_output_chain *ochain) {
	struct _set_secure_cookie *sc = ochain->data;

	if (sc) {
		if (sc->value && *sc->value) {
			if (!sc->type)
				set_secure_cookie(sc->call->r, sc->name, sc->value, sc->call->cookie_domain);
			else if (sc->time && *sc->type == 'd')
				set_secure_cookie_with_expiry(sc->call->r, sc->name, sc->value, sc->call->cookie_domain, atoi(sc->time));
			else if (sc->time && *sc->type == 'm')
				set_secure_cookie_with_expiry_minutes(sc->call->r, sc->name, sc->value, sc->call->cookie_domain, atoi(sc->time));
		}
		free(sc->fn);
		free(sc->fty);
		free(sc->fti);
	}
}

static const struct bpapi_output set_secure_cookie_output = {
	self_outstring_fmt,
	set_secure_cookie_outstring_raw,
	fini_set_secure_cookie,
	NULL
};

ADD_OUTPUT_FILTER(set_secure_cookie, sizeof(struct _set_secure_cookie));

static void
list_register_hooks(apr_pool_t *p) {
	ap_hook_handler(list_handler, NULL, NULL, APR_HOOK_MIDDLE);
	ap_hook_post_config(list_post_config, NULL, NULL, APR_HOOK_MIDDLE);
	ap_hook_child_init(list_child_init, NULL, NULL, APR_HOOK_MIDDLE);
}

module AP_MODULE_DECLARE_DATA list_module EXPORTED = {
	STANDARD20_MODULE_STUFF,
	NULL,				/* create per-directory config structure */
	NULL,				/* merge per-directory config structures */
	list_create_config,		/* create per-server config structure */
	NULL,				/* merge per-server config structures */
	NULL,				/* command apr_table_t */
	list_register_hooks		/* register hooks */
};
