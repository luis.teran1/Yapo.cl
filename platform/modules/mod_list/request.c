
#include <stdio.h>

#include <httpd.h>
#include <http_log.h>
#include <http_config.h>
#include <http_protocol.h>
#include <apr_strings.h>
#include <util_md5.h>

#include <bconf.h>
#include <strl.h>
#include <hash.h>
#include <ctype.h>

#include "util.h"
#include "ctemplates.h"
#include "query_string.h"
#include "unicode.h"

#include "mod_list.h"

static char *
escape_string(apr_pool_t *p, char *str) {
	char *escaped, *ptr, *redzone;
	int size;

	size = 15;
	escaped = apr_pcalloc(p, size);
	redzone = escaped + size - 10;

	for (ptr = escaped; *str; str++) {
		switch (*str) {
		case '&':
			memcpy(ptr, "%26", 3);
			ptr += 3;
			break;
		case '%':
			memcpy(ptr, "%25", 3);
			ptr += 3;
			break;
        		case '=':
        			memcpy(ptr, "%3D", 3);
			ptr += 3;
			break;
		default:
			*ptr++ = *str;
			break;
		}

		if (ptr > redzone) {
			char *old = escaped;
			int chars = ptr - old;

			size *= 2;
			escaped = apr_pcalloc(p, size);
			redzone = escaped + size - 10;

			memcpy(escaped, old, chars);		       
			ptr = escaped + chars;
		}
	}

	*ptr = '\0';
	return escaped;
}

char *
escape_html_ex(apr_pool_t *p, char *str, enum escape_ignore ignores) {
	char *html, *ptr, *redzone;
	int size;

	size = 15;
	html = apr_pcalloc(p, size);
	redzone = html + size - 10;

	for (ptr = html; *str; str++) {
		switch (*str) {
		case '"':
			memcpy(ptr, "&quot;", 6);
			ptr += 6;
			break;
		case '&':
			*ptr++ = *str;
			if ((ignores & NO_AMP) != NO_AMP) {
				memcpy(ptr, "amp;", 4);
				ptr += 4;
			}
			break;
		case '<':
			memcpy(ptr, "&lt;", 4);
			ptr += 4;
			break;
		case '>':
			memcpy(ptr, "&gt;", 4);
			ptr += 4;
			break;
		default:
			*ptr++ = *str;
			break;
		}

		if (ptr > redzone) {
			char *old = html;
			int chars = ptr - old;

			size *= 2;
			html = apr_pcalloc(p, size);
			redzone = html + size - 10;

			memcpy(html, old, chars);		       
			ptr = html + chars;
		}
	}

	*ptr = '\0';
	return html;
}

char *
escape_html(apr_pool_t *p, char *str) {
	return escape_html_ex(p, str, NONE);
}

/*
 *
 */

void
parse_qs_decode_only_cb(void *cb_data, char *key, int klen, int kbufsz, char *value, int vlen, int vbufsz) {
	struct list_call *b = cb_data;

	utf8_decode(key, &kbufsz, key, klen, b->thread_conf->utf8_cnv, b->thread_conf->dst_cnv);
	utf8_decode(value, &vbufsz, value, vlen, b->thread_conf->utf8_cnv, b->thread_conf->dst_cnv);
}

void
parse_qs_cb(void *cb_data, char *key, int klen, int kbufsz, char *value, int vlen, int vbufsz) {
	struct list_call *b = cb_data;
	int insert_bparse = TRUE;
	int append_qs = TRUE;
	enum query_string_var qsv;

	utf8_decode(key, &kbufsz, key, klen, b->thread_conf->utf8_cnv, b->thread_conf->dst_cnv);
	utf8_decode(value, &vbufsz, value, vlen, b->thread_conf->utf8_cnv, b->thread_conf->dst_cnv);

	/* Since regexps have been run here we can assume certain things about the value. */
	switch ((qsv = lookup_query_string_var(key, strlen(key)))) {
	case QSV_NONE:
		break;
	case QSV_MD:
		if (!b->company_id)
			strncpy(b->mode, value, 2);
		append_qs = insert_bparse = FALSE;
		break;
	case QSV_TH:
		strncpy(b->mode, atoi(value) ? "th" : "li", 2);
		append_qs = insert_bparse = FALSE;
		break;
	case QSV_FK:
		if (!b->company_id)
			b->company_list = 1;
		break;
	case QSV_ID:
		b->company_id = atoi(value);
		/* Reset company-list if company-id is set (for backward compat) */
		b->company_list = 0;
		strncpy(b->mode, "th", 2);
		insert_bparse = FALSE;
		break;
	case QSV_F:
		b->filter = *value;
		append_qs = FALSE;
		break;
	case QSV_SP:
		b->sort_price = atoi(value);
		append_qs = FALSE;
		break;
	case QSV_CG:
		if (!b->category)
			b->category = atoi(value);
		break;
	case QSV_C:
		b->category = atoi(value);
		break;
	case QSV_CA:
		insert_bparse = append_qs = FALSE;
		break;
	case QSV_S:
		b->set_focus = atoi(value);
		insert_bparse = append_qs = FALSE;
		break;
	case QSV_Q:
		/* Append to QS immediately in order to avoid adding the unescaped string */
		if (value[0]) {
			b->q_raw = apr_pstrdup(b->r->pool, value);
			b->qs = apr_psprintf(b->r->pool, "%s&%s=%s", b->qs ? b->qs : "", key, escape_string(b->r->pool, b->q_raw));

			b->q_html = escape_html(b->r->pool, value);

			bpapi_remove(&b->bparse, "q");
			bpapi_insert(&b->bparse, "q", b->q_html);

			bpapi_remove(&b->bparse, "q_raw");
			bpapi_insert(&b->bparse, "q_raw", b->q_raw);
		}
		insert_bparse = append_qs = FALSE;
		break;

	case QSV_W: {
		const char *cp;

		if ((cp = strchr(value, ':'))) {
			const char *ci = ++cp;

			apr_table_set(b->vars, "ci", apr_pstrdup(b->r->pool, ci));
			bpapi_remove(&b->bparse, "ci");
			bpapi_insert(&b->bparse, "ci", ci);
		}
		append_qs = FALSE;
		break;
	}

	case QSV_O:
	case QSV_WID:
	case QSV_AD_ID:
	case QSV_AJAX:
	case QSV_ELAK:
	case QSV_Q_IS_ID:
		append_qs = FALSE;
		break;
	case QSV_LAST:
	case QSV_NEXT:
	case QSV_NEXT_ERROR:
		insert_bparse = append_qs = FALSE;
		break;

	case QSV_COUNT_FILTER: {
		char *vp = strchr(value, ':');
		if (vp) {
                	/* Insert a fake count_path for the bread crumbs and linkshelf. */
			char *k;

			xasprintf(&k, "count_path_%.*s", (int)(vp - value), value);
			bpapi_remove(&b->bparse, k);
			bpapi_insert(&b->bparse, k, vp + 1);
			free(k);
		}
		return; /* Special case. */
	}

	case QSV_ST:
		insert_bparse = FALSE;
		bpapi_insert(&b->bparse, key, value);
		break;
	}

	if (parse_qs_cb_local)
		parse_qs_cb_local(b, qsv, key, value, &insert_bparse, &append_qs);

	apr_table_set(b->vars, key, value);

	if (insert_bparse) {
		if (!bconf_vget_int(b->conf->root, "qs.vars", key, "multiple", NULL))
			bpapi_remove(&b->bparse, key);
		bpapi_insert(&b->bparse, key, value);
	}
	if (append_qs)
                b->qs = apr_psprintf(b->r->pool, "%s&%s=%s", b->qs ? b->qs : "", key, value);

}

