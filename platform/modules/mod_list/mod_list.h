#ifndef MOD_LIST_H
#define MOD_LIST_H

#include <httpd.h>
#include <http_config.h>
#include <unicode/ucnv.h>

#include "bconf.h"
#include "bsapi.h"
#include "util.h"
#include "bpapi.h"
#include "query_string_var.h"
#include "cacheapi.h"

struct list_thread_conf {
	struct bconf_node *filter_state;
	UConverter *utf8_cnv;
	UConverter *dst_cnv;
	void *local;
};

struct list_conf {
	char hostname[256];

	struct bconf_node *root;
	struct bpapi_vtree_chain vbconf;
	struct shadow_vtree vfconf;

	struct bpapi_vtree_chain vt;

	int use_th_param;

	struct {
		int year;
		int week;
	} weeks[53];
	int num_weeks;

	struct {
		struct date_rec today;
		struct date_rec yesterday;
		time_t next_midnight_at;
	} date;

	ap_regex_t *index_referer;
	ap_regex_t *store_regex;

	struct {
		char *code;
	} lang;

	int num_threads;
	struct list_thread_conf *thread_data;

	struct cache_pools session;

	void *local;
};

struct list_call {
	struct list_conf *conf;
	struct list_thread_conf *thread_conf;

	request_rec *r;
	char *args;
	struct timeval start_time;
	const char *remote_addr;

	struct bpapi bparse;
	char *qs;
	char *query;
	apr_table_t *vars;
	struct bsearch_api bsearch;
	const char **valuelists;
	struct shadow_vtree filter_thread_conf;
	struct bpapi_vtree_chain tvt;
	struct shadow_vtree filter_session_conf;
	struct bconf_node *filter_bconf;

	const char *cookie_domain;

	struct {
		char *code; /* two character language code (ISO 639-1). Typically from user cookie */
	} lang;

	int offset;
	int ads_per_page;
	char filter;
	int sort_price;
	int elak;

	int company_id;
	int company_list;
	int category;
	char *q_raw;
	const char *q_html;
	char mode[3];

	int set_focus;

	int has_prices;
	int has_images;
	int has_map;

	void *local;

};

enum escape_ignore {
	NONE = 0,
	NO_AMP = 1
};

extern module AP_MODULE_DECLARE_DATA list_module;

void *list_create_config(apr_pool_t *pool, server_rec *s);
void setup_date(struct list_conf *conf, time_t now);
int list_post_config(apr_pool_t *pool, apr_pool_t *plog, apr_pool_t *ptemp, server_rec *s);
void list_child_init(apr_pool_t *pool, server_rec *s);

void handle_search_request_local(struct list_call *call) WEAK;

int setup_list_local(apr_pool_t *pool, struct list_conf *conf, struct bconf_node *root) WEAK;
void set_company_query_local(struct list_call *call) WEAK;
void list_start_call_local(struct list_call *call, int res) WEAK;
void list_end_call_local(struct list_call *call) WEAK;
void parse_qs_cb_local(struct list_call *call, enum query_string_var qsv, char *key, char *value, int *insert_bparse, int *append_qs) WEAK;
void add_qs_key_local(struct list_call *call, const char *key, int exclude, char **buf) WEAK;
char *replace_list_vars_local(struct list_call *b, request_rec *r, char *replace_args) WEAK;

char *escape_html(apr_pool_t *p, char *str);
void parse_qs_decode_only_cb(void *cb_data, char *key, int klen, int kbufsz, char *value, int vlen, int vbufsz);
void parse_qs_cb(void *cb_data, char *key, int klen, int kbufsz, char *value, int vlen, int vbufsz);

char *escape_html_ex(apr_pool_t *p, char *str, enum escape_ignore);

int check_search_query(struct list_call *b, struct bsconf *bc);
void set_search_query(struct list_call *b, const char *failover_query);
int send_search_query(struct list_call *b, struct bsconf *bc);

extern char *(*mod_list_friendlyurl_converter)(request_rec *r, struct list_conf *conf) VISIBILITY_HIDDEN;
extern int (*mod_list_failover_search)(struct list_call *b, int limit, struct bsconf *search_conf) VISIBILITY_HIDDEN;
extern void (*mod_list_call_init_local)(struct list_call *b, request_rec *r, struct list_conf *conf);
extern void (*mod_list_call_free_local)(struct list_call *b);
extern void (*mod_list_set_result_navigation)(struct list_call *) VISIBILITY_HIDDEN;
extern void (*mod_list_set_filters)(struct list_call *);

#define WHERE_UNSET		-1
#define WHERE_IN_CITY		0
#define WHERE_IN_REGION		1
#define WHERE_NEAR_REGION	2
#define WHERE_ALL 		3
#define WHERE_IN_MUNIC 		4
#define WHERE_IN_SUBAREA	5
#define WHERE_SPECIFIC_REGION_START 100

/*
 *  internal types
 */

#define PARAM_OFFSET		0x01 
#define PARAM_LIST_MODE		0x02
#define PARAM_SORT_PRICE	0x04
#define PARAM_FILTER		0x08
#define PARAM_WHERE		0x10
#define PARAM_THUMB		0x20 /* XXX Remove this later */

#endif /*MOD_LIST_H*/
