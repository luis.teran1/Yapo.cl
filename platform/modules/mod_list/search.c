#include <string.h>
#include <httpd.h>
#include <apr_strings.h>
#include <http_log.h>
#include <ctype.h>

#include "mod_list.h"
#include "search_field.h"
#include "cookies.h"

/*
 *  Callback function which handles indata from bsearch in key-value-pairs
 */

struct add_search_data {
	struct list_call *call;
	int col;
	int row;
	int cols;
	int values;
};

#define ADD_VALUE(key, val) do {				\
	if (d->row == 0)					\
		*(b->valuelists + d->col * 101) = key; \
	*(b->valuelists + d->col * 101 + 1 + d->row) = (val);	\
	d->col++;						\
	} while (0)

static void
add_search_data(int state, int info, const char *key, const char **value, void *data) {
	struct add_search_data *d = (struct add_search_data*)data;
	struct list_call *b = d->call;
	int row = 0;
	int cnt;
	int dontadd = 0;

	switch ((enum bsearch_state)state) {
	case BS_INFO:
		/*ap_log_error(APLOG_MARK, APLOG_ERR, OK, NULL, "info: (%s) -> (%s)", key, value);*/

		/* Add header-field */
		if (*value) {
			/* Disable the max_order cookie until realtime indexing */
			if (strcmp(key, "count_level") == 0) {
				/* count_level:munic:131:400 -> count_munic_code=131, count_munic_amount=400 */
				bpapi_insert(&b->bparse, apr_psprintf(b->r->pool, "count_%s_code", value[0]), value[1]);
				if (value[2]) /* Should always be true, except there was a bug in search for a while where it wasn't. */
					bpapi_insert(&b->bparse, apr_psprintf(b->r->pool, "count_%s_amount", value[0]), value[2]);
			} else if (strcmp(key, "count_level_path") == 0) {
				/* count_level_path:region:23:4711 count_path_region=23, count_path_amount=4711 */
				bpapi_insert(&b->bparse, apr_psprintf(b->r->pool, "count_path_%s", value[0]), value[1]);
				if (value[2]) /* Should always be true, except there was a bug in search for a while where it wasn't. */
					bpapi_insert(&b->bparse, apr_psprintf(b->r->pool, "count_path_%s_amount", value[0]), value[2]);
			} else if (strcmp(key, "suggest") == 0) {
				bpapi_insert(&b->bparse, "info_suggest_0", value[0]);
				bpapi_insert(&b->bparse, "info_suggest_1", value[1]);
				bpapi_insert(&b->bparse, "info_suggest_2", value[2]);
			} else if (strcmp(key, "count_each") == 0) {
				/* count_each:category:1020:5
				 * -> count_each_category=1020 count_each_category_amount=5
				 */
				if (value[0] && value[1] && value[2]) {
					bpapi_insert(&b->bparse, apr_psprintf(b->r->pool, "count_each_%s",
						value[0]), value[1]);

					bpapi_insert(&b->bparse, apr_psprintf(b->r->pool, "count_each_%s_amount",
						value[0]), value[2]);
				}
			} else {
				bpapi_insert(&b->bparse, key, *value);
			}
		}
		/* Reset col and row */
		d->col = 0;
		d->row = 0;
		break;
	case BS_COLS:
		/* ap_log_error(APLOG_MARK, APLOG_NOTICE, OK, NULL, "cols: (%u)", info); */

		/* Allocate buffers for the number of columns. Assume 100 lines of data. */
		d->cols = info + 20;
		b->valuelists = apr_pcalloc(b->r->pool, d->cols * sizeof(char*) * 101);
		break;
	case BS_DONE:
		/* ap_log_error(APLOG_MARK, APLOG_NOTICE, OK, NULL, "done! (%u)", info);  */

		/* Store all allocated buffers */
		d->values = info;
		d->cols = d->col;
		for (cnt = 0 ; cnt < d->cols ; cnt++) {
			bpapi_insert_list(&b->bparse, *(b->valuelists + cnt * 101), 
					      (b->valuelists + cnt * 101 + 1), d->values);
		}
		break;
	case BS_VALUE:
		/* ap_log_error(APLOG_MARK, APLOG_NOTICE, OK, NULL, "data (%u): (%s) = (%s)", info, key, value);  */
		row = info;
		if (d->row != row) {
			/* Adjust to the actual number of stored columns */
			d->cols = d->col;
			d->col = 0;
			d->row = row;
		}

		switch (lookup_search_field(key, strlen (key))) {
		case SF_HITTA_HAS_COORDS: /* XXX blocket specific */
			if (**value && atoi(*value)) {
				b->has_map = TRUE;
			}
			break;
		case SF_AD_TYPE:
			dontadd = 1;
			break;
		case SF_PACKAGE_PRICE:
			if (**value && atoi(*value)) {
				b->has_prices = TRUE;
			}
			break;
		case SF_PERIODIC_FEE:
		case SF_OLD_PRICE:
			if (!(*value && atoi(*value))) {
				dontadd = 1;
				ADD_VALUE(key, "");
			}
			break;
		case SF_PRICE:
		case SF_MONTHLY_RENT:
		case SF_MAX_RENT:
		case SF_WEEKLY_RENT_OFFSEASON:
		case SF_WEEKLY_RENT_PEAKSEASON:
		case SF_WEEKLY_RENT:
		case SF_YEARLY_RENT:
			if (**value && atoi(*value)) {
				b->has_prices = TRUE;
			} else {
				dontadd = 1;
				ADD_VALUE(key, "");
			}
			break;
		case SF_EMAIL:
			ADD_VALUE("has_email", (**value != '-') ? "1" : "0");
			break;
		case SF_EXTRA_IMAGES:
			if (strlen(*value) > 2) {
				ADD_VALUE("has_extra_images", "1");
			} else {
				ADD_VALUE("has_extra_images", "0");
			}
			break;
		case SF_IMAGE_WIDTHS:
			{
				/* Extract first value (main image) so we won't have to split in list template. */
				const char *end = strchr(*value, ',');
				if (end) {
					ADD_VALUE("main_image_width", apr_psprintf(b->r->pool, "%.*s", (int)(end - *value), *value));
				} else {
					ADD_VALUE("main_image_width", *value);
				}
			}
			break;
		case SF_IMAGE_HEIGHTS:
			{
				/* Extract first value (main image) so we won't have to split in list template. */
				const char *end = strchr(*value, ',');
				if (end) {
					ADD_VALUE("main_image_height", apr_psprintf(b->r->pool, "%.*s", (int)(end - *value), *value));
				} else {
					ADD_VALUE("main_image_height", *value);
				}
			}
			break;
		case SF_ID:
		case SF_LIST_ID:
		{
			char *val = (char *)*value;
			if (b->elak == 1) {
				int off;

				off = strlen(val) - 1;
				val[off] = '0' + ((val[off] - '0') ^ (random() & 3)) % 10;
			}
			ADD_VALUE("id", val);
			dontadd = 1;
			break;
		}
		case SF_IMAGE:
		{
			if (strlen(*value) > 2) {
				ADD_VALUE("has_image", "1");
				b->has_images = TRUE;
			} else {
				ADD_VALUE("has_image", "0");
			}
			break;
		}
		case SF_ZIPCODE:
		{
			char zipcode[10];
			const char *tmp = *value;
			int i = 0;

			while (*tmp && i < 10) {
				if (isdigit(*tmp)) {
					zipcode[i++] = *tmp;
				}
				tmp++;
				if (i == 5)
					break;
			}

			zipcode[i] = '\0';
			if (i)
				b->has_map = TRUE;
			ADD_VALUE(key, apr_pstrdup(b->r->pool, zipcode));
			dontadd = 1;
			break;
		}
		default:
			if (strncmp(key, "item_price", 10) == 0) {
				if (**value && atoi(*value)) {
					b->has_prices = TRUE;
				}
			}
			break;
		}

		if (!dontadd)
			ADD_VALUE(key, *value);
		break;
	case BS_NOAD:
		/* No action */
		break;
	default:
		ap_log_error(APLOG_MARK, APLOG_CRIT, OK, NULL, "Unknown state (%d) in add_search_data", state);
		break;
	}
}

int
send_search_query(struct list_call *b, struct bsconf *bc) {
	int res = -1;
	struct add_search_data data = {b};

	/* Obviously, the query should not be sent if unset */
	if (b->query == NULL)
		return 0;

	bpapi_insert(&b->bparse, "search_query", b->query);

	if (bsearch_init_bsconf(&b->bsearch, bc, b->remote_addr) == 0) {
		if (bsearch_search(&b->bsearch, b->query, add_search_data, &data) == 0) {
			res = 0;
		} else {
			ap_log_rerror(APLOG_MARK, APLOG_ERR, OK, b->r, "bsearch_search(%s) failed: %s", b->query, 
				      bsearch_errstr(b->bsearch.berrno));
			res = b->bsearch.berrno;
		}
	} else {
		ap_log_error(APLOG_MARK, APLOG_CRIT, OK, NULL, "bsearch_init() failed: %s", bsearch_errstr(b->bsearch.berrno));
		res = b->bsearch.berrno;
	}

	return res;
}

