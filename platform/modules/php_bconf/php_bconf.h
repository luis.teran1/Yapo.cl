#ifndef PHP_BCONF_H
#define PHP_BCONF_H 1

#define PHP_BCONF_WORLD_VERSION "1.1"
#ifndef PHP_BCONF_WORLD_EXTNAME
#define PHP_BCONF_WORLD_EXTNAME "bconf"
#endif

PHP_FUNCTION(_bconf_root);
PHP_FUNCTION(bconf_update_time);
PHP_FUNCTION(bconf_checksum);
PHP_FUNCTION(get_settings);

extern zend_module_entry bconf_module_entry;
#define phpext_bconf_ptr &bconf_module_entry

#endif
