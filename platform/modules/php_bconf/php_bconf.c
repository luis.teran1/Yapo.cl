#include <stdio.h>


#include <transapi.h>
#include <php.h>
#include <php_ini.h>

#include "php_bconf.h"
#include "util.h"
#include "bconfig.h"
#include "bconf.h"
#include "bconfd_client.h"
#include "vtree.h"
#include "settings.h"
#include "mempool.h"
#include "platform_php.h"

#ifdef GC_ZVAL_INIT
#define ALLOC_POOLED_ZVAL(z) \
	do { \
		(z) = mempool_alloc(php_mempool, sizeof(zval_gc_info)); \
		GC_ZVAL_INIT(z); \
	} while (0)
#else
#define ALLOC_POOLED_ZVAL(z) (z) = mempool_alloc(php_mempool, sizeof(zval))
#endif
#ifndef ALLOC_PERMANENT_ZVAL
#define ALLOC_PERMANENT_ZVAL(z) (z) = pemalloc(sizeof (zval), 1)
#endif

#ifndef Z_SET_ISREF_P
#define Z_SET_ISREF_P(z) PZVAL_IS_REF(z) = 1;
#define Z_ADDREF_P(pz) ZVAL_ADDREF(pz)
#endif

/* Forward declare init and shutdown functions */
ZEND_MINIT_FUNCTION(bconf);
ZEND_MSHUTDOWN_FUNCTION(bconf);
ZEND_RINIT_FUNCTION(bconf);
ZEND_RSHUTDOWN_FUNCTION(bconf);

ZEND_BEGIN_ARG_INFO(fifth_arg_force_ref, 0)
ZEND_ARG_PASS_INFO(0)
ZEND_ARG_PASS_INFO(0)
ZEND_ARG_PASS_INFO(0)
ZEND_ARG_PASS_INFO(0)
ZEND_ARG_PASS_INFO(1)
ZEND_END_ARG_INFO();

static zend_function_entry bconf_functions[] = {
    PHP_FE(bconf_update_time, NULL)
    PHP_FE(get_settings, fifth_arg_force_ref)
    PHP_FE(bconf_checksum, NULL)

    {NULL, NULL, NULL}
};

zend_module_entry bconf_module_entry EXPORTED = {
    STANDARD_MODULE_HEADER,
    PHP_BCONF_WORLD_EXTNAME,
    NULL,
    /* Module init/shutdown */
    ZEND_MINIT(bconf),
    ZEND_MSHUTDOWN(bconf),
    /* Request init/shutdown */
    ZEND_RINIT(bconf),
    ZEND_RSHUTDOWN(bconf),
    NULL,
    PHP_BCONF_WORLD_VERSION,
    STANDARD_MODULE_PROPERTIES
};

#ifdef COMPILE_DL_BCONF
#undef ZEND_DLEXPORT
#define ZEND_DLEXPORT EXPORTED
ZEND_GET_MODULE_PROTOTYPE(bconf);
ZEND_GET_MODULE(bconf)
#endif
     
PHP_INI_BEGIN()
PHP_INI_ENTRY(PHP_BCONF_WORLD_EXTNAME "_" "basedir",  ".", PHP_INI_ALL, NULL)
PHP_INI_ENTRY(PHP_BCONF_WORLD_EXTNAME "_" "configfile", NULL, PHP_INI_ALL, NULL)
PHP_INI_ENTRY(PHP_BCONF_WORLD_EXTNAME "_" "global_name", "BCONF", PHP_INI_ALL, NULL)
PHP_INI_ENTRY(PHP_BCONF_WORLD_EXTNAME "_" "bconf_update_time_name", "bconf_update_time", PHP_INI_ALL, NULL)
PHP_INI_ENTRY(PHP_BCONF_WORLD_EXTNAME "_" "get_settings_name", "get_settings", PHP_INI_ALL, NULL)
PHP_INI_ENTRY(PHP_BCONF_WORLD_EXTNAME "_" "bconf_checksum_name", "bconf_checksum", PHP_INI_ALL, NULL)
PHP_INI_ENTRY(PHP_BCONF_WORLD_EXTNAME "_" "extra_bconf", NULL, PHP_INI_ALL, NULL)
PHP_INI_END()
     
static zval *bconf_data = NULL;
static time_t start_time;
static struct mempool *php_mempool;
static char checksum[CHECKSUM_HEADER_LENGTH] = { 0 };

static void
free_bconf_data(void *v) {
	zval **data = v;

	/*zend_error((int)E_NOTICE, "%s", "foo");*/
	switch (Z_TYPE_PP(data)) {
	case IS_ARRAY:
		zend_hash_destroy(Z_ARRVAL_PP(data));
		free(Z_ARRVAL_PP(data));
		break;

	case IS_STRING:
		/*free((*data)->value.str.val);*/
		break;
	}
	free(*data);
	*data = NULL;
}

ZEND_MSHUTDOWN_FUNCTION(bconf) {
	if (bconf_data) {
		free_bconf_data(&bconf_data);
	}
	if (php_mempool)
		mempool_free(php_mempool);

	UNREGISTER_INI_ENTRIES();
	return SUCCESS;
}

static int
add_bconf_data(const char *key, const char *value, void *cbdata) {
	zval *bconf_data = cbdata;
	zval *c = bconf_data;
	zval **cp;
	char *keysep, *okey;
	zval *v;
	char *k;
	int cnt;

	okey = keysep = strdup(key);

	for (cnt = 0 ; (k = strsep(&keysep, ".")); cnt++) {
		if (keysep) {
			if (zend_symtable_find(Z_ARRVAL_P(c), k, strlen(k) + 1, (void **) &cp) == FAILURE
					|| Z_TYPE_PP(cp) != IS_ARRAY) {
				zval *n;

				ALLOC_PERMANENT_ZVAL(n);
				INIT_PZVAL(n);
				Z_TYPE_P(n) = IS_ARRAY;
				Z_ARRVAL_P(n) = pemalloc(sizeof (*Z_ARRVAL_P(n)), 1);
				Z_SET_ISREF_P(n);
				Z_ADDREF_P(n);
				zend_hash_init(Z_ARRVAL_P(n), 13, NULL, free_bconf_data, 1);
				zend_symtable_update(Z_ARRVAL_P(c), k, strlen(k) + 1, &n, sizeof(zval *), (void **) &cp);
			}
			c = *cp;
		} else {
			break;
		}
	}

	ALLOC_PERMANENT_ZVAL(v);
	INIT_PZVAL(v);
	Z_TYPE_P(v) = IS_STRING;
	Z_STRLEN_P(v) = strlen(value);
	Z_STRVAL_P(v) = (char*)mempool_strdup(php_mempool, value, Z_STRLEN_P(v));
	Z_SET_ISREF_P(v);
	Z_ADDREF_P(v);
	zend_symtable_update(Z_ARRVAL_P(c), k, strlen(k) + 1, &v, sizeof(zval *), NULL);

	free(okey);

	return 0;
}

static zend_bool
workaround_for_a_bug_in_zend(char *name, uint name_len TSRMLS_DC) {
	return 0;
}

ZEND_MINIT_FUNCTION(bconf) {
	struct bconfd_call call = { 0 };
	char *bconfd_conf_filename;
	char *extra_bconf;
	const char *blocket_id;
	struct bconf_node *conf;
	char *global_name;

	php_mempool = mempool_create(20 * 1024 * 1024);

	REGISTER_INI_ENTRIES();
	
	time(&start_time);

	bconf_functions[0].fname = INI_STR(PHP_BCONF_WORLD_EXTNAME "_" "bconf_update_time_name");
	bconf_functions[1].fname = INI_STR(PHP_BCONF_WORLD_EXTNAME "_" "get_settings_name");

	zend_register_functions(NULL, bconf_functions, NULL, type);

	bconfd_conf_filename = INI_STR(PHP_BCONF_WORLD_EXTNAME "_" "configfile");
	if (!bconfd_conf_filename) {
		int res;
		ALLOCA_PRINTF(res, bconfd_conf_filename, "%s/conf/bconf.conf", INI_STR(PHP_BCONF_WORLD_EXTNAME "_" "basedir"));
	}
	if ((conf = config_init(bconfd_conf_filename)) == NULL) {
		xerr(1, "Failed to read bconf configuration file %s", bconfd_conf_filename);
	}

	if ( (blocket_id = bconf_get_string(conf, "blocket_id")) == NULL) {
		xerrx(1, "Failed to get blocket_id from configuration file %s", bconfd_conf_filename);
	}

	bconfd_add_request_param(&call, "host", blocket_id);
	if (bconfd_load(&call, conf)) {
		xwarnx("Failed to load from bconfd, error: %s", call.errstr);
		return FAILURE;
	}
	snprintf(checksum, sizeof(checksum), "%s", call.checksum);

	/* Free bconf_data if already set. */
	if (bconf_data)
		free_bconf_data(&bconf_data);

	global_name = INI_STR(PHP_BCONF_WORLD_EXTNAME "_" "global_name");
	if (!global_name)
		global_name = "BCONF";

	ALLOC_PERMANENT_ZVAL(bconf_data);
	INIT_PZVAL(bconf_data);
	Z_TYPE_P(bconf_data) = IS_ARRAY;
	Z_ARRVAL_P(bconf_data) = pemalloc(sizeof (*Z_ARRVAL_P(bconf_data)), 1);
	zend_hash_init(Z_ARRVAL_P(bconf_data), 13, NULL, free_bconf_data, 1);
#if PHP_MAJOR_VERSION > 5 || (PHP_MAJOR_VERSION == 5 && PHP_MINOR_VERSION >= 4)
	zend_register_auto_global(global_name, strlen(global_name), 0 /* jit */, (zend_auto_global_callback)workaround_for_a_bug_in_zend TSRMLS_CC);
#else
	zend_register_auto_global(global_name, strlen(global_name), workaround_for_a_bug_in_zend TSRMLS_CC);
#endif

	/* HOSTNAME CONFIG */
	zval *v;
	ALLOC_PERMANENT_ZVAL(v);
	INIT_PZVAL(v);
	Z_TYPE_P(v) = IS_STRING;
	Z_STRLEN_P(v) = strlen(blocket_id);
	Z_STRVAL_P(v) = (char*)mempool_strdup(php_mempool, blocket_id, Z_STRLEN_P(v));
	Z_SET_ISREF_P(v);
	Z_ADDREF_P(v);
	zend_symtable_update(Z_ARRVAL_P(bconf_data), "blocket_id", sizeof("blocket_id"), &v, sizeof(zval *), NULL);
	bconf_free(&conf);

	extra_bconf = INI_STR(PHP_BCONF_WORLD_EXTNAME "_" "extra_bconf");
	if (extra_bconf) {
		struct bconf_node *eb = config_init(extra_bconf);
		bconf_foreach(eb, INT_MAX, add_bconf_data, bconf_data);
	}

	bconf_foreach(call.bconf, INT_MAX, add_bconf_data, bconf_data);
	mempool_finalize(php_mempool);
	bconf_free(&call.bconf);

	return SUCCESS;
}

ZEND_RINIT_FUNCTION(bconf) {
	zval *v;

	MAKE_STD_ZVAL(v);
	Z_TYPE_P(v) = IS_ARRAY;
	Z_ARRVAL_P(v) = Z_ARRVAL_P(bconf_data);
	Z_SET_ISREF_P(v);
	Z_ADDREF_P(v);
	zend_hash_update(&EG(symbol_table), INI_STR(PHP_BCONF_WORLD_EXTNAME "_" "global_name"), strlen(INI_STR(PHP_BCONF_WORLD_EXTNAME "_" "global_name")) + 1, &v, sizeof(zval *), NULL);

	return SUCCESS;
}

ZEND_RSHUTDOWN_FUNCTION(bconf) {
	zval **cp;
	zval *f;
	zval *retval;

	/* Try to flush the session first, since it can't use bconf after we remove it. */
	MAKE_STD_ZVAL(f);
	ZVAL_STRING(f, "session_write_close", 0);
	MAKE_STD_ZVAL(retval);
	call_user_function(CG(function_table), NULL, f, retval, 0, NULL TSRMLS_CC);

	if (zend_symtable_find(&EG(symbol_table), INI_STR(PHP_BCONF_WORLD_EXTNAME "_" "global_name"), strlen(INI_STR(PHP_BCONF_WORLD_EXTNAME "_" "global_name")) + 1, (void **) &cp) == SUCCESS) {
		Z_TYPE_PP(cp) = IS_NULL; /* Yes, hack, but works. */
		zend_hash_del(&EG(symbol_table), INI_STR(PHP_BCONF_WORLD_EXTNAME "_" "global_name"), strlen(INI_STR(PHP_BCONF_WORLD_EXTNAME "_" "global_name")) + 1);
	}

	return SUCCESS;
}

PHP_FUNCTION(bconf_update_time) {
	RETVAL_LONG(start_time);
}

PHP_FUNCTION(bconf_checksum) {
	RETVAL_STRING(checksum, 1);
}

struct get_settings_data {
	zval *setting;
	zval *key_lookup;
	zval *set_value;
	zval *cbdata;
};

static void
set_value_func(const char *setting, const char *key, const char *value, void *cbdata) {
	struct get_settings_data *cbd = cbdata;
	zval *retval = NULL;
	zval *k;
	zval *v;
	zval **params[4] = {&cbd->setting, &k, &v, &cbd->cbdata};

	MAKE_STD_ZVAL(k);
	ZVAL_STRING(k, (char*)key, 1);

	MAKE_STD_ZVAL(v);
	if (value) {
		ZVAL_STRING(v, (char*)value, 1);
	} else {
		ZVAL_EMPTY_STRING(v);
	}

	call_user_function_ex(CG(function_table), NULL, cbd->set_value, &retval, (cbd->cbdata ? 4 : 3), params, 0, NULL TSRMLS_CC);
}

static const char *
key_lookup_func(const char *setting, const char *key, void *cbdata) {
	struct get_settings_data *cbd = cbdata;
	zval *retval = NULL;
	zval *k;
	zval **params[3] = {&cbd->setting, &k, &cbd->cbdata};

	MAKE_STD_ZVAL(k);
	ZVAL_STRING(k, (char*)key, 1);

	if (call_user_function_ex(CG(function_table), NULL, cbd->key_lookup, &retval, (cbd->cbdata ? 3 : 2), params, 0, NULL TSRMLS_CC) != SUCCESS) {
		return NULL;

	}
	if (ZVAL_IS_NULL(retval))
		return NULL;

	if (Z_TYPE_P(retval) != IS_STRING)
		convert_to_string(retval);

	return retval->value.str.val;
}

PHP_FUNCTION(get_settings) {
	struct get_settings_data cbd = {0};
	int res;
	zval *rootnode;
	struct bpapi_vtree_chain vchain;
	/* XXX this could probably be considered cheating... */
	extern const struct bpapi_vtree php_vtree_node;

        if (ZEND_NUM_ARGS() != 4 && ZEND_NUM_ARGS() != 5) WRONG_PARAM_COUNT;

	if (ZEND_NUM_ARGS() == 4) {
	        res = zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC,"azzz",  &rootnode,
					    &cbd.setting, &cbd.key_lookup, &cbd.set_value);
	} else {
	        res = zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC,"azzzz", &rootnode,
					    &cbd.setting, &cbd.key_lookup, &cbd.set_value, &cbd.cbdata);
	}

        if (res == FAILURE)
		RETURN_NULL();

	vchain.fun = &php_vtree_node;
	vchain.data = rootnode;
	vchain.next = NULL;

	get_settings(&vchain, cbd.setting->value.str.val, key_lookup_func, set_value_func, &cbd);
	vtree_free(&vchain);
}

