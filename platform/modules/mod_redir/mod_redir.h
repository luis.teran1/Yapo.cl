#ifndef MOD_REDIR_H
#define MOD_REDIR_H

#include "vtree.h"

struct apr_table_t;
struct request_rec;
struct bconf_node;
struct fd_pool;

struct redir_conf {
	struct bconf_node *root;
	struct bpapi_vtree_chain redis_vtree;
	struct fd_pool *redis_pool;
};

struct redir_call {
	apr_table_t *vars;
};

void local_NextUrlLogicHandler(request_rec *r, struct redir_conf *conf, char **next_url_ptr) WEAK;

#endif
