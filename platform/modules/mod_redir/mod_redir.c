#include <httpd.h>
#include <http_log.h>
#include <http_config.h>
#include <http_protocol.h>
#include <apr_strings.h>
#include <ctype.h>
#include <string.h>

#include "bconf.h"
#include "util.h"
#include "parse_query_string.h"
#include "cookies.h"
#include "mod_bconf_extern.h"
#include "sredisapi.h"
#include "fd_pool.h"
#include "strl.h"
#include "url.h"
#include "remote_addr.h"

#include "mod_redir.h"

/*
 *  The request handler
 */

static void *create_redir_config(apr_pool_t *pool, server_rec *s);
static void register_redir_hooks(apr_pool_t *p);

module AP_MODULE_DECLARE_DATA redir_module EXPORTED = {
	STANDARD20_MODULE_STUFF,
	NULL, /* per dir create config */
	NULL, /* per dir merge config */
	create_redir_config, /* per server create config */
	NULL, /* per server merge config */
	NULL, /* config file commands */
	register_redir_hooks /* register hooks */
};

static void *
create_redir_config(apr_pool_t *pool, server_rec *s) {
	return apr_pcalloc(pool, sizeof(struct redir_conf));
}

static int
redir_post_config(apr_pool_t *pool, apr_pool_t *plog, apr_pool_t *ptemp, server_rec *s) {
	struct redir_conf *conf;
	struct bconf_node *br;

	for ( ; s != NULL; s = s->next) {
		if ((br = mod_bconf_get_root(s)) == NULL) {
			ap_log_error(APLOG_MARK, APLOG_WARNING, OK, NULL, "mod_redir - Failed to get bconf root.");
			return HTTP_INTERNAL_SERVER_ERROR;
		}
		conf = ap_get_module_config(s->module_config, &redir_module);
		conf->root = br;

		const char *redis_node = bconf_get_string(conf->root, "mod_redir.redis.node");
		if (redis_node) {
			bconf_vtree(&conf->redis_vtree, bconf_get(conf->root, redis_node));
			conf->redis_pool = fd_pool_create(&conf->redis_vtree, 1, NULL);
			if (!conf->redis_pool) {
				ap_log_error(APLOG_MARK, APLOG_ERR, OK, NULL, "mod_redir - Failed to initialize redis");
				return HTTP_INTERNAL_SERVER_ERROR;
			}
			ap_log_error(APLOG_MARK, APLOG_INFO, OK, NULL, "mod_redir - using redis node \"%s\"", redis_node);
		}
	}

	return OK;
}

static void
redir_qs_cb(struct parse_cb_data *pqcb_data, char *key, int klen, char *value, int vlen) {
	struct redir_call *call = pqcb_data->cb_data;

	apr_table_set(call->vars, key, value);
}

static int
redir_handler(request_rec *r) {
	struct redir_call call = {0};
	char *previous_site  = NULL;
	char *redir_site = NULL;
	char *redir_referer  = NULL;
	const char *redir_type  = NULL;
	const char *base_url = NULL;
	const char *site = NULL;
	const char *referer  = NULL;
	char *cp = NULL;
	char datestring[64];
	time_t t;
	struct tm tm;
	char *log_string = NULL;
	char *cookie = NULL;
	char *next_url = NULL;
	char *args = NULL;
	struct redir_conf *conf;
	const char *cookie_name;
	struct bconf_node *uanode;

	if (strcmp(r->handler, "redir-handler") != 0)
		return DECLINED;

	conf = ap_get_module_config(r->server->module_config, &redir_module);
	call.vars = apr_table_make(r->pool, 4);
	if (r->args)
		parse_query_string(apr_pstrdup(r->pool, r->args), redir_qs_cb, &call, NULL, 0, 0, NULL);

	base_url = bconf_get_string(conf->root, "common.base_url.blocket");
	if (!base_url || !base_url[0]) {
		ap_log_error(APLOG_MARK, APLOG_ERR, OK, NULL, "mod_redir - Failed to lookup base_url in bconf.");
		return HTTP_INTERNAL_SERVER_ERROR;
	}
	ap_set_content_type(r, "text/html");

	/* Set P3P header */
	apr_table_add(r->err_headers_out, "P3P", "CP='NOI DSP COR PSAo PSDo'");

	/* Read previous cookie */
	cookie_name = bconf_vget_string(conf->root, "mod_redir.cookie", r->uri + 1, NULL);
	if (!cookie_name)
		cookie_name = "redir";
	cookie = get_cookie(r, cookie_name);
	if (cookie && ((cp = strrchr(cookie, '-')) != NULL)) {
		*cp = '\0';

		previous_site = apr_pstrdup(r->pool, cookie);
		/* if previous date is needed, just cp++ and it will be under the
		   char pointer for copying */
	}

	/* Redir site */
	site = apr_table_get(call.vars, "s");
	if (site) {
		if (bconf_get_int(conf->root, "mod_redir.base64encode_cookie")) {
			redir_site = ap_pbase64encode(r->pool, (char *)site);
		} else {
			redir_site = apr_pstrdup(r->pool, site);
		}
	}

	/* Referer */
	referer = apr_table_get(r->headers_in, "Referer");
	if (referer) {
		redir_referer = apr_pstrdup(r->pool, referer);
	}

	/* Redirect to next url */
	if (apr_table_get(call.vars, "url")) {
		char *pos, *npos;

		args = apr_pstrdup(r->pool, r->args);
		next_url = strstr(args, "url=");
		/* Point to after url= */
		if (next_url)
			next_url += 4;

		if (next_url) {
			/* Replace first %3f with ? in redir */
			if ((pos = strcasestr(next_url, "%3f")) != NULL) {
				memmove(pos+1, pos+3, strlen(pos+3)+1); /* +1 to copy \0 */
				*pos = '?';
			}
			/* Decode fragment. */
			for (pos = next_url ; (npos = strstr(pos + 1, "%23")) ; pos = npos)
				;
			if (pos != next_url) {
				memmove(pos + 1, pos + 3, strlen(pos + 3) + 1);
				*pos = '#';
			}
		}
	}

	/* Log redir in sql format */
	t = time(NULL);
	gmtime_r(&t, &tm);
	strftime(datestring, sizeof(datestring), "%Y/%m/%d+%H_%M_%S", &tm);
	if (redir_site && previous_site && (strcmp(redir_site, previous_site) == 0 || strcmp(site, previous_site) == 0)) {
		redir_type = "repeated_redir";
	} else {
		redir_type = "first_redir";
	}

	int nolog = 0;
	if ((uanode = bconf_get(conf->root, "mod_redir.nolog.useragents"))) {
		const char *useragent = referer = apr_table_get(r->headers_in, "User-Agent");
		const char *needle;
		int i;

		if (useragent) {
			for (i = 0 ; (needle = bconf_value(bconf_byindex(uanode, i))) ; i++) {
				if (strstr(useragent, needle)) {
					nolog = 1;
					break;
				}
			}
		}
	}

	if (!apr_table_get(call.vars, "nc") && redir_site) {
		/* Create cookie */
		cookie = apr_psprintf(r->pool, "%s-%s", redir_site, datestring);

		/* Set cookie */
		int expire = bconf_get_int_default(conf->root, "mod_redir.cookie.timeout", 3 * 30);
		set_cookie_with_expiry(r, cookie_name, cookie, bconf_get_string(conf->root, "common.cookie.domain"), expire);

		/* Set new "from_click" cookie -- used by Pixel */
		const char* from_click_cookie_name = bconf_get_string(conf->root, "common.from_click_cookie_name");
		if (from_click_cookie_name)
			set_cookie_with_expiry_minutes(r, from_click_cookie_name, "y", bconf_get_string(conf->root, "common.cookie.domain"), 1);
	}

	const char* xtredir = bconf_get_string(conf->root, "mod_redir.add_redir_site");
	if (redir_site && xtredir) {
		if (!next_url)
			next_url = apr_psprintf(r->pool, "/");

		if (strchr(next_url, '?'))
			next_url = apr_psprintf(r->pool, "%s&%s=%s", next_url, xtredir, redir_site);
		else
			next_url = apr_psprintf(r->pool, "%s?%s=%s", next_url, xtredir, redir_site);
	}

	if (next_url && (local_NextUrlLogicHandler))
                local_NextUrlLogicHandler(r, conf, &next_url);

	const char *pass;
	int i;
	struct bconf_node *pnode = bconf_get(conf->root, "mod_redir.pass");
	for (i = 0 ; (pass = bconf_value(bconf_byindex(pnode, i))) ; i++) {
		const char *val;

		if ((val = apr_table_get(call.vars, pass))) {
			struct buf_string kv = {0};

			url_encode(&kv, pass, strlen(pass));
			bufwrite(&kv.buf, &kv.len, &kv.pos, "=", 1);
			url_encode(&kv, val, strlen(val));

			if (!next_url) {
				next_url = apr_psprintf(r->pool, "?%s", kv.buf);
			} else if (!strstr(next_url, kv.buf)) {
				if (strchr(next_url, '?'))
					next_url = apr_psprintf(r->pool, "%s&%s", next_url, kv.buf);
				else
					next_url = apr_psprintf(r->pool, "%s?%s", next_url, kv.buf);
			}
			free(kv.buf);
		}
	}
	
	int internal_redir = (next_url && next_url[0] == '/') ? 1 : 0;

	/* Sanity checking of redirection url */
	if (!next_url || strpbrk(next_url, "\r\n")) {
		/* Missing or bad next_url, goto first page */
		next_url = apr_pstrdup(r->pool, base_url);
		internal_redir = 1;
	} else if (!((strncmp(next_url, "http://", 7) == 0) || (strncmp(next_url, "https://", 8) == 0)) && !internal_redir) {
		/* Prepend base_url to next_url */
		next_url = apr_psprintf(r->pool, "%s/%s", base_url, next_url);
		internal_redir = 1;
	}

	int whitelist_disabled = bconf_get_int(conf->root, "mod_redir.disable_whitelist");
	int ok_host = 1;

	/* Because goodsite.com:password@evilsite.com can be used to fool the code (and users),
	   we simply don't allow that style of URLs, ever. There'll never be a legitimate use anyhow.
	   URLs of the same form where the '@' is something else, like say '%40' will technically
	   be accepted, but the redirect will fail with 'Bad port number.' */
	if (strchr(next_url, '@') != NULL) {
		ap_log_rerror(APLOG_MARK, APLOG_NOTICE, OK, r, "Attempt to use redir using name@passwd, '%s'. Not OK.", next_url);
		ok_host = 0;
	}

	if (ok_host && !internal_redir && !whitelist_disabled) {
		ok_host = 0;
		struct bpapi_loop_var keys = {0};
		struct bpapi_vtree_chain temp_node = {0};
		struct bpapi_vtree_chain vtree;

		bconf_vtree(&vtree, conf->root);
		/* Loop over bconf allowed hosts */
		if (vtree_getnode(&vtree, &temp_node, "mod_redir", "allowed_host", NULL)) {
			vtree_fetch_keys(&temp_node, &keys, NULL);
			int i = 0;
			for (i = 0; i < keys.len; i++) {
				/* Verify that the user supplied URL points to a white-listed protocol+host combo */
				const char* candidate_url = vtree_get(&temp_node, keys.l.list[i], NULL);
				char* base = strstr(next_url, candidate_url);
				if (base && (base == next_url)) {
					/* Make sure it's not just a prefix of a bad site, like 'http://blocket.se.badsite.com' */
					base += strlen(candidate_url);
					if (*base == '\0' || *base == '/' || *base == ':' || *base == '?') {
						ok_host = 1;
						break;
					}

				}
			}
			if (keys.cleanup)
				keys.cleanup(&keys);
			vtree_free(&temp_node);
			vtree_free(&vtree);
		} else {
			ap_log_rerror(APLOG_MARK, APLOG_ERR, OK, r, "(CRIT) No mod_redir.allowed_host list in bconf");
		}
	}

	if (!nolog) {
		int log_next_url = bconf_get_int(conf->root, "mod_redir.log_next_url");
		int log_encoded = bconf_get_int(conf->root, "mod_redir.base64encode_cookie_logged");
		/* site (code), type, remote_addr, referer */
		char *nospace = apr_pstrdup(r->pool, log_encoded ? redir_site : site);

		if (nospace){
			char *ptr = nospace;
			while (*ptr != '\0') {
				if ( *ptr == ' ') {
					*ptr = '_';
				}
				ptr++;
			}
		}

		log_string = apr_psprintf(r->pool, "%s %s %s %s%s%s", nospace, redir_type, get_remote_addr(r, conf->root), redir_referer? redir_referer : "", log_next_url ? " " : "", log_next_url ? next_url : "");

		if (strlen(log_string) < 900) {
			apr_table_add(r->notes, "redir_log", log_string);
			ap_log_error(APLOG_MARK, APLOG_INFO, OK, NULL, "REDIR: %s", log_string);
		}
	}

	if (apr_table_get(call.vars, "logonly")) {
		return DONE;
	}

	if (!ok_host) {
		const char *redir_error_url;

		log_string = apr_psprintf(r->pool, "found bad host - %s", next_url);
		/* Log bad host */
		if (strlen(log_string) < 900) {
			apr_table_merge(r->notes, "redir_log", log_string);
			ap_log_error(APLOG_MARK, APLOG_INFO, OK, NULL, "REDIR: %s", log_string);
		}
		/* Failover to first page */
		if ((redir_error_url = bconf_get_string(conf->root, "mod_redir.error_url")) != NULL)
			next_url = apr_psprintf(r->pool, "%s%s", base_url, redir_error_url);
		else
			next_url = apr_pstrdup(r->pool, base_url);
	}

	apr_table_add(r->headers_out, "Location", next_url);

	if (site && conf->redis_pool) {
		struct fd_pool_conn *conn = redis_sock_conn(conf->redis_pool, "master");
		char field[256];
		const char *sep = bconf_get_string(conf->root, "mod_redir.redis.separator");
		char *sp;

		localtime_r(&t, &tm);
		strftime(datestring, sizeof(datestring), "redir_%Y%m%d", &tm);

		strlcpy(field, site, sizeof(field));
		if (sep && (sp = strpbrk(field, sep)))
			*sp = '\0';

		int res = redis_sock_hincrby(conn, datestring, field, 1);
		if (res < 0)
			ap_log_rerror(APLOG_MARK, APLOG_ERR, OK, r, "(CRIT) mod_redir: failed to store to redis");

		fd_pool_free_conn(conn);
	}

	return HTTP_MOVED_TEMPORARILY;
}

static void
register_redir_hooks(apr_pool_t *p) {
	ap_hook_handler(redir_handler, NULL, NULL, APR_HOOK_MIDDLE);
	ap_hook_post_config(redir_post_config, NULL, NULL, APR_HOOK_MIDDLE);
}

