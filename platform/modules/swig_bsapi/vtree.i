
%{

#include "python_vtree.h"

%}

#ifdef SWIGPYTHON
%typemap(in) struct bpapi_vtree_chain * {
	if (!PyDict_Check($input)) {
		PyErr_SetString(PyExc_TypeError, "not a dictionary");
		return NULL;
	}

	$1 = malloc(sizeof (*$1));
	if (!$1) {
		PyErr_NoMemory();
		return NULL;
	}

	python_vtree_init($1, $input);
	$1->next = NULL;
}

%typemap(freearg) struct bpapi_vtree_chain * {
	if ($1)
		vtree_free($1);
	free($1);
}
#endif
