
%{

SWIGEXPORT
#if PY_VERSION_HEX >= 0x03000000
PyObject*
#else
void
#endif
SWIG_init(void);

struct bsapi_object_iter_columns {
	struct bsapi_object *bo;
};

struct bsapi_object_iter_info {
	struct bsapi_object *bo;
};

%}

%typemap(in) const char * {
	$1 = PyBytes_AsString($input);
	if (!$1) {
		PyErr_SetString(PyExc_TypeError, "byte string expected, you need to encode to the site charset");
		return NULL;
	}
}
%typemap(freearg) const char * {
}

%typemap(out) struct bsapi_object_keyval * {
	if ($1) {
		$result = PyTuple_New(2);
		if ($result) {
			PyTuple_SetItem($result, 0, PyBytes_FromString($1->key));
			PyTuple_SetItem($result, 1, PyBytes_FromString($1->value));
		}
	} else {
		$result = NULL;
	}
}

%typemap(out) struct bsapi_object_info * {
        if ($1) {
		$result = PyTuple_New(2);
		if ($result) {
			PyObject *vs = PyList_New($1->nvals);
			if (vs) {
				int i;

				for (i = 0 ; i < $1->nvals ; i++)
					PyList_SetItem(vs, i, PyBytes_FromString($1->value[i]));

				PyTuple_SetItem($result, 0, PyBytes_FromString($1->key));
				PyTuple_SetItem($result, 1, vs);
			}
		}
	} else {
		$result = NULL;
	}
}

%nodefaultctor;
struct bsapi_object_iter_info
{
};

struct bsapi_object_iter_columns
{
};

%typemap(out) char ** {
	int nrows = bsapi_object_num_rows(arg1);
	int i;

	$result = PyList_New(nrows);

	for (i = 0 ; i < nrows ; i++)
		PyList_SetItem($result, i, PyBytes_FromString($1[i]));
}

%nothread;
%extend bsapi_object_iter_info {
	const struct bsapi_object_info *__getitem__(size_t i) {
		if (i >= (unsigned)bsapi_object_num_infos($self->bo)) {
			PyErr_SetString(PyExc_StopIteration, "End of infos");
			return NULL;
		}
		return bsapi_object_info_idx($self->bo, i);
	}
};

%extend bsapi_object_iter_columns {
	const struct bsapi_object_keyval *__getitem__(size_t i) {
		if (i >= (unsigned)bsapi_object_num_cols($self->bo)) {
			PyErr_SetString(PyExc_StopIteration, "End of columns");
			return NULL;
		}
		return bsapi_object_column($self->bo, i);
	}
};
%clearnothread;

%extend bsapi_object {
	struct bsapi_object_iter_info infos(void) {
		return (struct bsapi_object_iter_info){$self};
	}

	struct bsapi_object_iter_columns columns(void) {
		return (struct bsapi_object_iter_columns){$self};
	}
};

