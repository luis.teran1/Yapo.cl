
%module bsapi

%{
#include "bsapi_object.h"

%}

%include "vtree.i"
%include "cpointer.i"

#ifdef SWIGPYTHON
%include "python.i"
#endif

%nodefaultctor;
struct bsapi_object
{
};

%pointer_class(struct bsconf, bsconf_struct)

%nothread bsconf_init_vtree;
int bsconf_init_vtree(struct bsconf *, struct bpapi_vtree_chain *, int use_keepalive);

void bsconf_del(struct bsconf *bs);

struct bsapi_object *bsapi_object_new(struct bsconf *bsconf, const char *remote_addr);
void bsapi_object_free(struct bsapi_object *);

int bsapi_object_search(struct bsapi_object *, const char *query);

const struct bsapi_object_info *bsapi_object_get_info(struct bsapi_object *, const char *key, int idx);
const struct bsapi_object_info *bsapi_object_next_info(struct bsapi_object *);

const char *bsapi_object_column_name(struct bsapi_object *, int idx);

int bsapi_object_num_infos(struct bsapi_object *);
int bsapi_object_num_cols(struct bsapi_object *);
int bsapi_object_num_rows(struct bsapi_object *);

bool bsapi_object_next_row(struct bsapi_object *);
bool bsapi_object_set_row(struct bsapi_object *, int row);

const struct bsapi_object_keyval *bsapi_object_next_column(struct bsapi_object *);

const char *bsapi_object_column_value(struct bsapi_object *, const char *column);

const char **bsapi_object_all_values(struct bsapi_object *, int colidx);
const char **bsapi_object_column_all_values(struct bsapi_object *, const char *column);

