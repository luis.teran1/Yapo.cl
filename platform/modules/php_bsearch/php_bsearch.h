#ifndef PHP_BSEARCH_H
#define PHP_BSEARCH_H 1

#define PHP_BSEARCH_WORLD_VERSION "1.0"
#define PHP_BSEARCH_WORLD_EXTNAME "bsearch"

PHP_FUNCTION(bsearch_search_vtree);

extern zend_module_entry bsearch_module_entry;
#define phpext_bsearch_ptr &bsearch_module_entry

#endif
