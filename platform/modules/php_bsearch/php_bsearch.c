#include <php.h>

#include "php_bsearch.h"
#include "bsapi.h"
#include "buf_string.h"
#include "platform_php.h"

#ifndef Z_DELREF_P
#define Z_DELREF_P(pz) ZVAL_DELREF(pz)
#define Z_ADDREF_P(pz) ZVAL_ADDREF(pz)
#endif

ZEND_BEGIN_ARG_INFO(fourth_arg_force_ref, 0)
ZEND_ARG_PASS_INFO(0)
ZEND_ARG_PASS_INFO(0)
ZEND_ARG_PASS_INFO(0)
ZEND_ARG_PASS_INFO(1)
ZEND_END_ARG_INFO();

static zend_function_entry bsearch_functions[] = {
    PHP_FE(bsearch_search_vtree, fourth_arg_force_ref)
    {NULL, NULL, NULL}
};

zend_module_entry bsearch_module_entry EXPORTED = {
    STANDARD_MODULE_HEADER,
    PHP_BSEARCH_WORLD_EXTNAME,
    bsearch_functions,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    PHP_BSEARCH_WORLD_VERSION,
    STANDARD_MODULE_PROPERTIES
};

#ifdef COMPILE_DL_BSEARCH
#undef ZEND_DLEXPORT
#define ZEND_DLEXPORT EXPORTED
ZEND_GET_MODULE_PROTOTYPE(bsearch)
ZEND_GET_MODULE(bsearch)
#endif

struct callback_data {
	zval *function;
	zval *data;

	zval **params[5];
};

/*
 *
 */

static void
add_complex_data(int state, int info, const char *key, const char **value, void *data) {
	zval **sub_array;

	if (state != BS_VALUE)
		return;

	if (key && *value) {
		if (zend_hash_find(Z_ARRVAL_P((zval*)data), (char*)key, strlen(key) + 1, (void**)&sub_array) != FAILURE) {
			if ((Z_TYPE_PP(sub_array)) == IS_STRING) {
				convert_to_array(*sub_array);
				add_next_index_string(*sub_array, (char*)*value, 1);
			} else if ((Z_TYPE_PP(sub_array)) == IS_ARRAY) {
				add_next_index_string(*sub_array, (char*)*value, 1);
			}
		} else {
			add_assoc_string((zval*)data, (char*)key, (char*)*value, 1);
		}

	}
}

static void
add_callback_data(int state, int info, const char *key, const char **value, void *data) {
	struct callback_data *cb = (struct callback_data*)data;
	zval *key_val;
	zval *value_val;
	zval *info_val;
	zval *state_val;
	zval *return_value;

	if (key && value) {
		MAKE_STD_ZVAL(key_val);
		ZVAL_STRING(key_val, (char*)key, 1);

		cb->params[1] = &key_val;

		MAKE_STD_ZVAL(value_val);
		if (info == BS_INFO && value[1] != NULL) {
			struct buf_string infokey = {NULL};
			const char **v;

			bswrite(&infokey, *value, strlen(*value));
			for (v = value + 1 ; *v ; v++)
				bscat(&infokey, ":%s", *v);
			ZVAL_STRING(value_val, infokey.buf, 1);
			free(infokey.buf);
		} else {
			ZVAL_STRING(value_val, (char*)*value, 1);
		}

		cb->params[2] = &value_val;

		MAKE_STD_ZVAL(info_val);
		ZVAL_LONG(info_val, info);

		cb->params[3] = &info_val;

		MAKE_STD_ZVAL(state_val);
		ZVAL_LONG(state_val, state);

		cb->params[4] = &state_val;

		if (call_user_function_ex(CG(function_table), NULL, cb->function, &return_value, 5, cb->params, 0, NULL TSRMLS_CC) != SUCCESS)
			zend_error(E_ERROR, "Failed to call function: %s \n", Z_STRVAL_PP(&cb->function));
	
		if (Z_DELREF_P(key_val) == 0)
			FREE_ZVAL(key_val);

		if (Z_DELREF_P(value_val) == 0)
			FREE_ZVAL(value_val);
		
		if (Z_DELREF_P(info_val) == 0)
			FREE_ZVAL(info_val);
		
		if (Z_DELREF_P(state_val) == 0)
			FREE_ZVAL(state_val);
	}
}

PHP_FUNCTION(bsearch_search_vtree) {
	struct callback_data cb;
	struct bsearch_api ba;
	char *search_string;
	unsigned int ss_len;
	zval *search_node;

	memset(&cb, 0, sizeof(cb));

	if (ZEND_NUM_ARGS() == 2) {
		if (zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, "as", &search_node, &search_string, &ss_len) == FAILURE) {
			RETURN_NULL();
		}
	} else if (ZEND_NUM_ARGS() == 4) {
		if (zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, "asz!z", &search_node, &search_string, &ss_len, &cb.function, &cb.data) == FAILURE) {
			RETURN_NULL();
		}

#ifdef Z_SET_ISREF_P
		Z_SET_ISREF_P(cb.data);
#else
		PZVAL_IS_REF(cb.data) = 1;
#endif
		Z_ADDREF_P(cb.data);
		cb.params[0] = &cb.data;
	} else 
		WRONG_PARAM_COUNT;

	if (search_node) {
		struct bpapi_vtree_chain vtree;
		/* XXX this could probably be considered cheating... */
		extern const struct bpapi_vtree php_vtree_node;

		vtree.fun = &php_vtree_node;
		vtree.data = search_node;
		vtree.next = NULL;
		if (bsearch_init_vtree(&ba, &vtree, NULL) != 0) {
			zend_error(E_ERROR, "bsearch_init_vtree() failed: %s\n", bsearch_errstr(ba.berrno));
			RETURN_NULL();
		}
		vtree_free(&vtree);
	} else {
		RETURN_NULL();
	}

	if (array_init(return_value) != SUCCESS) {
		RETURN_NULL();
	}

	if (!cb.function) {
		bsearch_search(&ba, search_string, add_complex_data, return_value);
	} else {
		bsearch_search(&ba, search_string, add_callback_data, &cb);
	}

	bsearch_cleanup(&ba);
}
