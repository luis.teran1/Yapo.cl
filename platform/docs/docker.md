Platform tests with docker
==========================

Base images
-----------

We have a set of base images, with all possible combination of:

* OS: CentOS 5, CentOS 6, CentOS 7, Ubuntu 14.04
* PostgreSQL: 8.4 (maybe?), 9.2, 9.3
* Compiler: gcc 4.8, gcc 4.9, clang 3.4, clang 3.5

Note: we don't need the cross product, one image can cover several
cases.

These base images contain everything needed to build stable and
are available in `docker images`.

The scripts to build the base images are currently kept in a separate
repository, https://scmcoord.com/gitlab/coord/docker
.

New dependencies
----------------

When we introduce new dependencies we add those as `yum install`
or whatever to a file called DOCKER.branch.os, e.g.
`DOCKER.gcc-4.8.centos6`. When docker.sh is run, it creates a new
image from the base image and all those files and then runs the tests
in the new image. Docker caches the commands used to create an image
and when it detects the same set of commands it will simply reuse
the cache, making subsequent runs very fast.

The OS name is derived from the base image name, see the sed in
docker.sh.

Once the branch is released on stable, we update the base images
to contain the new dependency. This likely just comes down to move
the tag to a new image since docker has already cached it.

Running a single test suite
---------------------------

When using docker, a single make target can be run by adding `--cmd make foo`
Example:

	DOCKER_IMAGE=platform-centos6 ./compile.sh --cmd make -C regress/search regress

Debugging / manual runs
-----------------------

You can get a docker shell by using the shell flag.

	DOCKER_IMAGE=platform-centos6 ./compile.sh --shell

Inside the shell, you can run `./compile.sh` and make targets as usual.

Jenkins
-------

One slave will have to be created per base image. Presumably they will
all be different users on the host machine, since shared folders are
required (see [docker.sh](../docker.sh).
