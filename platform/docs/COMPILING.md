# Building with the new build system. #

## Intro ##

The new build system replaces make with ninja. Ninja is a tool developed by Evan Martin at Google and is used to build Chrome as fast as possible. As a Make replacement it's slightly quirky, it doesn't really have much logic other than handling dependencies correctly, and you need an external tool to generate ninja files for ninja to usable. It's possible to write them by hand, but it gets very chatty and unreadable fast.

The big advantage of ninja is that it forces you to think about what you're doing and it is bloody fast.

### About this documentation ###

 * "The basics" describes how to setup and use the build system day to day.

 * "High level concepts" is a mess, but describes dependencies, libraries and how the file tree is organized.

 * "Writing Builddesc files" and on describes how to actually write Builddesc files to build new components.

## The basics ##

You'll need to have ninja in your $PATH, you can get ninja from [github](https://github.com/martine/ninja).

If you want to install through packages, beware that there are other unrelated software projects out there called 'ninja'.

If this document gets out of date, the tools we have are made roughly around the time of ninja 1.0 and anything from around that time should work.

### How to run ###

The basic way to build a tree is the compile.sh script in the root directory.

    $ ./compile.sh

This will compile everything you need.

The two things it does is to run a perl script that's in platform/scripts/build/build-build.pl that generates ninja files and then runs ninja itself.

If you changed some file and want to recompile, just run compile.sh again. It costs 50ms extra to run the script compared to just ninja directly,it's a lot less typing and you'll never lose dependencies. Reading this sentence cost you more time than you'd ever save skipping the script.

If you're not sure if you changed anything, you can still recompile and see what happens. An empty compilation of the platform code is 100ms. All compilations need to be done from the root directory.

## High level concepts ##

### What to build ###

Components of the system are described by Builddesc files. Those files get parsed by the perl script and translated into ninja files. The perl script does most of the heavy lifting for figuring out dependencies.

### Dependencies ###

The important thing to understand about the new build system is that everything must have proper dependencies. You can't get away with knowing that make will run the compilation in some order that happens to work most of the time. This is important because it allows ninja to optimize the compilation very hard and make it faster than you'd imagine was possible, and it allows ninja to only rebuild the things you need.

To make sure that you don't cheat on your dependencies, there's a certain amount of randomness in the build process. Things will not compile in the same order every time. This has a disadvantage though - if you have multiple errors in your compilation and fix one error, you might get the next error before you see if your fix worked or didn't work. It's slightly annoying, but worth the advantages.

How to express dependencies is described in the documentation of how to write Builddesc files.

### Libraries. ###

Most dependencies on generic components are expressed through libraries. If a source file is used by multiple binaries, it needs to end up in a library. This includes highly localized sources like for example the files shared by redis-server and redis-cli. The libraries we build are all static, so there shouldn't be much size or performance overhead. But one thing this means is that you can't have defines controlling certain behaviors of the build. It was trivial to fix the existing defines, so it shouldn't much of a problem.

### Build directory. ###

Everything during the build is contained into the build/ directory at top of the tree. If you believe that you messed something up really badly and think that you need a fresh build, nuke that directory and rebuild.

The organization of the build directory requires some explanation:

    $ ls build
    build.ninja  regress  rpm  version
    $ 

The `build.ninja` file is the top level ninja file that describes how to build everything. It includes various global configuration and rules files, contains a target to rebuild the ninja files (with proper dependencies) and finally includes the flavors we'll build.  The default flavor is `regress`, but most sites will have `regress` and `rpm` flavors. `regress` is what we need to run regression tests and get a local environment running. `rpm` is where the rpm build happens. Both will have mirrored ninja files, binaries, modules, etc.

    $ ls build/regress/
    bin  build.ninja  buildvars.ninja  include  lib  modules  obj
    regress templates  tools
    $

This gets a little bit more interesting.

 * `bin` and `modules` are the destination directories to where binaries (search, trans, etc.) and modules (mod_templtes, php_trans, etc.) end up.

 * `lib` contains all the libraries we've built that we're linking against.

 * `include` contains all the include files that our libraries provide as an external API. This is mostly to avoid an inlude path hell. Instead of having an -I option to gcc for every library you happen to use and conflicting include files (remember `config.h`?), all includes get put into that directory and if you manage to create two include files with the same name, instead of randomly including the wrong file half the time ninja will yell at you. You wouldn't want a ninja yelling at you, would you?

 * `templates` are where the compiled template containers end up. See later sections for a description of how that works.

 * `tools` are binaries that aren't part of the destination build. The template compiler, mkcharmap, etc.

 * `obj` is a rough mirror of the source tree where we drop compiled object files and such. The idea is that nothing except ninja and the compiler should ever touch things in here. That's why you don't need to know what's specifically in there. If you have tests where you need to refer to their output in config files or such you should use `regress`

 * `regress` is a directory where regress tests and such store their work. Generated config files, search indexes, work directories and such.

## Writing Builddesc files. ##

### General syntax. ###

A Builddesc file contains one or more build descriptors, and maybe some comments. Example:

    # Let's build libplatform_foo
    LIB(platform_foo
       srcs[src1.c src2.cc]
       includes[foo.h]
    )
    # Build program "bar" with libplatform_foo
    PROG(bar
       srcs[main.c]
       libs[platform_foo]
    )

Comments start with '#' and continue to the end of the line.

Build descriptors have the general form of:

    DESC(name argument1[element1 element2 element3] argument2[el])

`DESC` is the descriptor, it can be one of: `CONFIG`, `COMPONENT`, `PROG`, `TOOL_PROG`, `LIB`, `MODULE`, `TEMPLATES`, `INSTALL`, `TOOL_INSTALL`. These will be described later on.

All descriptors except `COMPONENT` and `CONFIG` require a name and at least one argument. Name and arguments are separated by spaces.

Arguments are usually lists of elements. The elements are separated by whitespace. Some arguments can only contain one element, it will become obvious later in the documentation which ones those are. Arguments are things like: srcs, includes, libs, deps, templates, etc.

### Descriptors ###

#### Global configuation - CONFIG ####

To configure what's being built and how you need something like this:

    CONFIG(
        buildversion_script[scripts/build/buildversion.sh]
        flavors[regress rpm]
        rules[
            scripts/build/config.ninja
            scripts/build/gcc.ninja
            scripts/build/rules.ninja
        ]
        extravars[scripts/build/toolrules.ninja scripts/build/static.ninja]
        buildpath[build]
	buildvars[copts cflags cwarnflags conlyflags cxxflags]
	ruledeps[in:$inconf,$intool]
	prefix:rpm[opt/blocket]
    )

This configures some aspects of the build system.

 * `buildversion_script` - script that outputs one number which is the version of what's being built. It's highly recommended that the version number is unique for at least every commit to your repository.
 * `compiler` - Override the compiler used, set it to the C compiler, C++ one will be guessed with some heuristics.
 * `flavors` - Various build environments needed to build your site. The usual is to build rpm and regress.
 * `rules` - Global compilation rules. These ninja files gets included globally.
 * `extravars` - Per flavor-included ninja files. This means they can depend on the variables defined in the flavor files. Can be flavored.
 * `buildpath` - Where the build files are put. See other sections of this document to see how files are organized.
 * `buildvars` - attributes in other build descriptors that are copied into ninja files as variables. As in this example, those are various variables we want to be able to specify in build decsriptors that override default variables.
 * `ruledeps` - Per-rule dependencies. Targets built with a certain rule will depend on those additional target. In this example everything built with `in` will also depend on `$inconf` and `$intool`.
 * `prefix:flavor` - Set a prefix for the installed files for the specified flavor.

The defaults are hard-coded to have buildpath set to `build` and one flavor - `regress`. Everything else needs to be configured.
The environment varialbe `BUILDPATH` can also be used to set the buildpath, but the config option has precedence.

There can be only one `CONFIG` directive in the whole build system and it has to be before any `COMPONENT` directives. In practice this means that it has to be first in the first Builddesc or not at all.

#### Compiler ####

You can switch the compiler used in CONFIG. The available choices are gcc or clang:

	compiler[clang]

You will need respective compiler to be installed, obviously.

For clang you will need at least version 3.4, and it has to be configured with a libstdc++ from gcc 4.7 or newer.
For example you can install devtoolset-2 and configure clang with `--with-gcc-toolchain=/opt/rh/devtoolset-2/root/usr`

#### Top level - COMPONENT ####

The top level Builddesc contains something like this:

    COMPONENT(flavors[regress rpm] [
         platform/bin/tpc
         platform/bin/bconfd
         platform/regress
         platform/scripts/db
    ])

`COMPONENT` is a special build descriptor that doesn't really follow the general rules. It doesn't have a name and has one argument with no name and a list of directories where we will find further Builddesc files describing other components of our build. It is possible for one or more of those directories to contain a Builddesc with `COMPONENT` in it, but be very careful with that. It's much more transparent and readable to have a long list of everything built instead of a magic tree where everything is hidden.

The only other argumnet for `COMPONENT` build descriptors is `flavors`. See flavors documentation below for the semantics of that. If `flavors` is omitted, the components will be included in all flavors.

#### Normal programs - PROG ####

A program we want installed as part of the distribution can look something like this:

    PROG(search
        srcs[search_parser.yy search_lex.ll search.cc sock_util.c
          stats.c engine.c parse_node_invword.cc parse_node_docarr.cc
          parse_node_count_each.cc]
        libs[platform_search]
    )

This specifies that our program is called "search", the binary ends up in bin/, it has a bunch of source files and links to one library.

The interesting part here is that the sources are in four different languages. The perl script will figure out how to build everything and generate the right build directives for ninja. It will also figure out that since at least one of our source files is c++, the final linking of the binary will be done correctly.

Another thing worth noting is that one of our source files is yacc and those always generate an include file. It's quite likely that lots of other source files will include it. Instead of specifying a dependency on it, our magical perl script will generate a general dependency so that no other files are compiled until yacc has generated our include file. This might not be the most performance optimal thing to do, but it's the most convenient.

#### Build tools - TOOL_PROG ####

     TOOL_PROG(mkcharmap
             srcs[mkcharmap.c]
             specialsrcs[mksections:NamesList.txt.bz2:sections.h]
             deps[mkcharmap.c:sections.h]
             libs[platform_util]
     )
    

`TOOL_PROG` is just like `PROG`, with two differences. Tools get installed under tools/ and instead of like `PROG` - being part of the default build target tools get built only if something depends on them. If you happen to end up with a tree that doesn't have any templates, you won't build the template compiler since it's a tool.

#### Build libraries - LIB ####

     LIB(platform_search
             srcs[charmap.c hunstem.cc index.c langmap.c synize.c text.c]
             includes[charmap.h  hunstem.h index.h langmap.h synize.h
                 text.h index_blob.h]
             libs[platform_tokyo platform_core hunspell]
     )
    
`LIB` describes how to build a library. Libraries end up in lib/ and are only built when something links with them. An interesting thing to note here is that the magic perl script generates targets for normal and PIC targets for all shared libraries and the PIC targets get only built for the libraries that get linked into modules.

`includes` and `libs` are worth noting here. The includes argument specifies which include files are the external interface for this library and those get installed in includes/. The libs argument is special. Since we're building a static library, it can't be linked with other libraries (unless they are static, but there's madness there). Instead `libs` create dependencies that make programs built with this library to also link with those other libraries. So if you link your program with platform_search from this example, you don't need to specify that it needs to link with hunspell and platform_core, that will happen automatically.

Another thing worth noting is that programs that link to this library will get an automatic dependency on the includes from this library, so the programs source files won't be compiled until the include files for this library are installed. This is also recursive, the program will depend on all the include files from the libraries this library depends on.

#### Modules - MODULE ####

     MODULE(php_trans
            srcs[php_trans.c]
            libs[platform_core]
            copts[-DCOMPILE_DL_TRANS=1 $php_module_cflags]
     )
    
`MODULE` describes how to build various shared object modules. Those end up in modules/. php, perl and apache modules are all built the same way. All modules are added to the default build target.

#### Configuration and other files - INSTALL ####

     INSTALL(regress/indexer/conf
             srcs[isearch.conf.in]
             conf[isearch.conf]
     )
    
     INSTALL(bin
             srcs[create_proschema.pgsql.in]
             scripts[create_proschema.pgsql]
     )
    
Installs files into the destination directory specified in the name. There are different installation arguments, "scripts" specifies executable scripts and such, "conf" specifies non-executable files, "python" should only be used for python scripts (they will be compiled after install). A thing worth noting is that you can specify files that are copied straight from the source directory and also configuration files that have been compiled somehow (like .in). The magical perl script will figure out which source file is built where and where to copy it from.

#### Non binary build tools - TOOL_INSTALL ####

     TOOL_INSTALL(perl_xs_compiler
             srcs[perl_xs_compiler.in]
			 scripts[perl_xs_compiler]
     )

`TOOL_INSTALL` is just like `INSTALL` with one major difference: non binary tools get installed under tool/ and the target argument is the script name instead of the target directory. This descriptor is used to install build tools that are either static scripts or autogenerated scripts from a `.in` file.
The `name` argument is currently ignored since the destination directory is always `tool/`.

#### TEMPLATES ####

`TEMPLATES` is explained in a special section below.

### Build descriptor arguments ###

#### flavors ####

    flavors[regress]

Limits the building of this descriptor to certain flavors. See below for more explaination of flavors.

#### srcs ####

    srcs[foo.c bar.cc]

Source files to build a program, library, module, tool or configuration file. The magical perl script automatically knows what to do with the following extensions:

 * c - C source
 * cc, cxx - C++ source
 * yy - C++ yacc
 * ll - C++ lex
 * gperf.enum - enumerated gperf source
 * in - to be processed by in.pl
 * tmpl - templates

#### libs ####

    libs[platform_search z stdc++]

In `LIB` this establishes the other libraries that this library depends on. This makes everything else link to those other library when they link to this library.

For all others this is a list of libraries that need to be linked to. `MODULE` automatically links to the PIC versions of libraries if necessary. As a special case, if the name of the library contains a `/` or `$` the script assumes that it's a special case library from a full path or a predefined variable and links to it directly instead of through -l.

#### Standard buildvars ####

These are standard in that they're commonly used and when defined, should be used for the purpose stated. They're not necessarily required or even defined per default.

##### cflags, cwarnflags, conlyflags, cxxflags #####

    cflags[-O3 -funroll-loops]
    cwarnflags[]
    conlyflags[-Wno-dump-warning]
    cxxflags[-fno-exceptions]

Override the default compilation (optimization settings mostly) and warning flags. Don't do this unless you really can't fix the code.

##### copts #####

    copts[\`php-config --includes\` -fno-strict-aliasing]

Options passed to C and C++ compilers. Mostly optimizations, warnings and such. Does not override the default flags.

##### ldopts #####

    ldopts[-fprofile-arcs]

Options passed to linker. Does not override the default flags.

#### conf ####

    conf[bconf.txt]

Configuration files to be installed.

Only valid in `INSTALL`.

#### scripts ####

    scripts[index_ctl]

Scripts to be installed.

Only valid in `INSTALL`

#### incdirs ####

    incdirs[/usr/include/postgresql]

Paths where include files can be found. What you'd normally add with -I, but without -I. The reason this isn't part of copts is because without special handling this could end up with very long lines of repeated include directories, now the script can resolve include paths and get the ordering right.

#### specialsrcs ####

    specialsrcs[charmap:swedish.txt:charmap-swedish
                charmap:russian.txt:charmap-russian
                charmap:swedish-utf8.txt:charmap-swedish-utf8
    ]           
    
This argument specifies a method to compile a source file where the automatic file type detection can't figure out how to compile. Each element in is of the form: `<command>:<src>[,<src>]*:<target>[:<extravars>[,<extravars>]*]`

`command` is the ninja rule to build the target, `src` is a comma separated list of sources, `target` is the resulting output. `extravars` is a comma separated list of extra variables added to the build directive for ninja.

Notice that specialsrcs is mostly useless on its own, the script will not pick up and do anything with the output from the script. You will need to specify additional arguments that use the output somehow (usually `conf` or `srcs`).

##### tmplin in specialsrcs #####

    INSTALL(regress/tmplin
           extravars[tmplinvars.ninja]
           specialsrcs[
                   tmplin:test.txt.tmpl:test.txt:tmplin_args=$a_test
                   tmplin:simplevar.txt.tmpl:simplevar.txt:tmplin_args=$a_simplevars
                   tmplin:bconf.txt.tmpl:bconf.txt:tmplin_args=$a_bconf
                   tmplin:filter.txt.tmpl:filter.txt:tmplin_args=$a_filter
           ]
           conf[test.txt simplevar.txt bconf.txt filter.txt]
    )
   
In most cases specialsrcs will be either install targets or special rules that the build system doesn't understand. One exception is the tmplin target. It's hard to match normally in srcs because we don't have an extension to match on and in most cases tmplin will be called with extra arguments.

`tmplin` takes a template (or list of templates if you have includes) and renders that with a set of variables and bconf. The arguments are passed through the extra variable tmplin_args and are the same as the arguments to show_template. Since we can't have spaces in the extra arguments to specialsrcs, the easiest way to pass arguments into is to define variables in a ninja file included through `extravars` and then then reference those variables as `tmplin_args=$var` as in the example above. The template called will be the first template specified in the source files.

#### extravars ####

    extravars[tmplinvars.ninja]

Extra ninja variables needed to build this descriptor. `extravars` will be included only in this build descriptor and can therefore reference generated paths. Since rules are global and `extranvars` can be included multiple times it can't define rules. See special section below to see how to deal with this.

#### deps ####

    deps[config.h mkcharmap.c:sections.h]

Extra dependencies that the script can't figure out by itself. Two forms:

 * `<depender>:<dependency>`
   specifies that `<depender>` can't be built until `<dependency>`
   has been built.
 * `<global dependency>`
   specifies a dependency for all source files in this descriptor.

#### srcdir ####

    srcdir[hunspell-1.2.11/src/hunspell]

Specifies a different directory where the source files can be found relative to where the Builddesc is. Source files can always be given as a path, but if all source files are somewhere deeper inside the tree, this makes `srcs` shorter. Another use is in `TEMPLATES` to make sure that the generated templates have correct names.

#### destdir ####

    destdir[lib/perl5]

Normally PROG and MODULE installs in predefined target directories, but in rare cases you might need to override this, for example for perl modules or to install to libexec/ instead of bin/.

#### collect_target_var ####

    INSTALL(conf/a
        conf[x.conf y.conf]
        collect_target_var[all_confs a_confs]
    )
    INSTALL(conf/b
        conf[x.conf]
        collect_target_var[all_confs]
    INSTALL(conf/csums
        specialsrcs[
            checksum:$all_confs:allconfs.csum
            checksum:$a_confs:aconfs.csum
        ]
        conf[allconfs.csum aconfs.csum]
    )

Collects the names of final targets into one or more variables that can later be used as a sources in other descriptors.

In this example $all_confs will contain `$destroot/conf/a/x.conf`, `$destroot/conf/a/y.conf` and `$destroot/conf/b/x.conf`, $a_confs will contain `$destroot/conf/a/x.conf` and `$destroot/conf/y.conf`. Notice that the variables will be expanded when generating the ninja files to get proper dependencies (and because ninja treats variables as one file, never a list).

#### templates ####

    templates[platform_regress_template_test_test]

Explained in the section below.

### Templates - TEMPLATES build descriptor and templates argument. ###

#### Standard way. ####

Templates are handled with the combination of the TEMPLATES descriptor and the templates argument.

The TEMPLATES descriptor creates a package of templates.

    TEMPLATES(trans_templates, srcdir[templates] tmpldirs[mail sql])

The `srcdir` argument specifies which directory contains the template source files, just like normal srcdir in other descriptors. This will not be part of the name of templates in the package. The `tmpldirs` argument specifies which subdirectories under the tmpldir directory will be included. The `tmpldirs` directory names will be part of the template name in the package. If `tmpldirs` is omitted, all subdirectories will be included.

`PROG`, `TOOL_PROG`, `LIB`, `MODULE` or other `TEMPLATES` can then include the templates packages with the templates argument:

    templates[trans_templates]

#### Simplified way. ####

It's also possible to just compile in your templates like you would with any other sources.

    srcs[templater.c foo.txt.tmpl]

This will compile in the template inside `foo.txt.tmpl` and make it available under the name `foo.txt`. Notice that no name mangling is done in this case. The template gets the same name as its source file, so if it's called `templates/foo/bar.txt.tmpl`, the name will be `templates/foo/bar.txt`.

#### PHP ####

    php[blocket_newad.php]

PHP code to be installed and syntax checked using the PHP lint command.

#### More tweaks. ####

    TEMPLATES(platform_regress_template_test_test
   	srcdir[templates]
   	srcs[test/foo.txt.tmpl.in test/foo.txt.tmpl]
   	tmpldirs[test]
    )
    TEMPLATES(platform_regress_template_test_all
           srcdir[templates]
           tmpldirs[mod_templates qs]
           templates[platform_regress_template_test_test
                   platform_regress_template_test_shared]
    )
   
In case you want your templates to be generated from .in files or why not through tmplin specialsrcs or some other means, you can also specify them with srcs inside the `TEMPLATES` directive.

You can also make a template blob link to other template blobs to include templates in that blob. This can be done by specifying directories as well, but this way saves on compilation time.

## Builddesc fragments and includes. ##

In some cases we need to build a binary multiple times with optional components. In that case it's useful to have partial Builddesc files included in the main Builddesc. The INCLUDE[] argument does this. The included fragment contains arguments to the build descriptor.

Example:

 * in modules/mod_templates/Builddesc.inc:

           srcs[mod_templates.c request.c template_functions.c]
           libs[platform_apache platform_templates]
 
 * in regress/mod_templates/Builddesc:

         MODULE(mod_templates
                INCLUDE[../../modules/mod_templates/Builddesc.inc]
                srcs[local.c]
                templates[platform_regress_template_test_all]
         )
   
Two things are worth noting here. The same arguments can be present in both the included fragment and in the main descriptor.  Source files paths for the fragment are relative to where the fragment lives.

The path specified in `INCLUDE` can either be relative to the top directory as all other paths or if it starts with a `./` or `../` will be relative to the directory of the Builddesc where the `INCLUDE` argument is. The `.` and `..` semantics are only really for platform specific includes where we don't know the name of the platform directory.

## Flavors ##

The build system supports building various flavors of the same code. The typical configuration will build 'regress' and 'prod' versions of the code.

There are three ways the flavors can be differentiated between each other.

### .in flavors. ###

The typical way to generate the variables for .in files is to allow the script that generates the input to include the file with various build variables defined. These will be defined inside `build/<flavor>/buildvars.ninja` and include definitions for the flavor being built and various directories.

### COMPONENT flavors ###

`COMPONENT` descriptors can contain the argument `flavors` which contains the list of flavors this component will only be built for. The default is to build for all flavors.

If you want some components to be only built for regress, you can do:

    COMPONENT(flavors[regress]
     [
   	regress/mod_templates
   	regress/search
   	regress/indexer
     ]
    )
   
### flavor modifiers on arguments. ###

Almost all arguments can be modfied with the flavor with a colon and the flavor name after. Like this:

    srcs[a.c b.c]
    srcs:regress[c.c]
   
The result will be a merge between the unflavored arguments and the arguments for this flavor (`srcs[a.c b.c c.c]` in the example).

### flavors argument. ###

All build descriptors (except `CONFIG`) also support a `flavors` argument. This argument lists the flavors where that build descriptor applies.

## Various special rules and definition files. ##

### build_version.h ###

The version of the build is always written with a define BUILD_VERSION into the `build_version.h` file. The build version is generated by the script in `platform/scripts/build/buildversion.sh`. If you include this file in your source you'll have to add an explicit dependency on it.

Another way to get the build version is through .in files. There is an automatic dependency there and you don't need to do anything.

Inside templates the build version is available through the function `&build_version()`, or through bconf is you choose to add it in there. Notice that the vtree key is the version of the bconf in trans while the function is the version of the templates. When in doubt, use the function.

### invars.sh ###

The variables for .in files are generated into `build/<flavor>/tools/in.conf` with a script. The generated file must be of the form `KEY=value` and nothing else, no comments, no extra whitespace, no nothing. This allows us to include it from Makefiles, ninja files and shell scripts.

The variables are generated by the shell script in `platform/scripts/build/invars.sh`.

### static.ninja ###

The file `platform/scripts/build/static.ninja` contains certain static targets we need to build and don't really have other ways to express them. Currently it contains the rules for `build_version.h` and `in.conf`.

### rules.ninja & config.ninja ###

In `platform/scripts/build` there are the two files `config.ninja` and `rules.ninja`.

`config.ninja` contains various variables that control where to find certain tools for the build and other settings.

`rules.ninja` contain the main rules for compilation (like how to invoke compilers, how to link, etc.). Read the ninja documentation before changing anything in `rules.ninja`. Be careful about failing rules which just simply redirect some script output into the $out file. Make sure to remove the $out file if the script fails. If you don't, the next build can get confused and not rebuild the file even though it failed the previous time. make removes the file for you, ninja doesn't. There are some examples in there how to do this, but the safest is probably to wrap everything in a script.

### various variables like incdir & co ###

There's a bunch of variables defined inside the ninja files that can be relevant. It will work to use them in Builddesc since they will get expanded in the generated ninja file.

 * `$incdir` - directory with all the includes installed. Mostly used in dependencies if you need to depend on a particular include file: `deps[$incdir/build_version.h]`
 * `$libdir` - directory with all the libraries.
 * `$builddir` - the root of all ninja and intermediate files. (`build/obj/regress` or such).
 * `$destroot` - the root installation directory. (`build/regress` or such).
 * `$buildtools` - directory where the tools are if you need an explicit dependency or refer to a tool in a extravars file.
 * `$platformdir` - need to be defined to point to the root of the platform directory relative to the root of where the build is done. Normally it will be `platform` on sites.

### ninja.mk ###

In some situations we'll need to build things from Makefiles. This can be done by using `ninja.mk`. `ninja.mk` replaces all current make targets like `all.mk`, `in.mk`, `prog.mk`, `lib.mk`, etc. It should be used for regress tests only and then you'll need to include `ninja.mk`, `regress.mk`, `regressinit.mk`. It defines the minimum of variables needed for running regress tests and everything defined for in-files.

### Multiple targets with the same name. ###

Since the script resolves dependencies and build rules according to the names of generated files, special care needs to be taken when you end up with multiple rules creating several different files with the same names in different directories. In general this can only happen when you do something like this:

    INSTALL(conf
        srcs[bconf.txt.in]
        conf[bconf.txt]
        specialsrcs[magic:bconf.txt:foobar]
    )
  
In the srcs argument `bconf.txt.in` generates `bconf.txt`, but the `conf` argument does the same. Then the `specialsrcs` depends on `bconf.txt` and it's not really clear which one. The only safe heuristics the script can apply to resolve this is when one of the targets is an install target (`conf` or `scripts`). In that case the dependency chain gets transformed from:

    $src/bconf.txt.in -> $objdir/bconf.txt -> $conf/bconf.txt

to:

    $src/bconf.txt.in -> $objdir/TMP_BUILDbconf.txt -> $conf/bconf.txt

So if you look in the object directory for the generated file, it might have a different name.

### Writing own rules. ###

One behavior of ninja that is important to understand is that rules are global while variables can be local (read up ninja documentation to understand variable scoping). This means that all rules files can only be included once. If you compile a tool that will then be used inside a rule you must therefore do something like this:

 * `Builddesc`:

          TOOL_PROG(mytool
              srcs[...]
          )
          INSTALL(conf
              conf[fooconf]
              specialsrcs[myrule:fooconf:in.foo]
              extravars[myvars.ninja]
          )

 * `<global>/rules.ninja`:

          rule myrule
              command=$mytool $in $out

 * `myvars.ninja`:

          mytool=$buildtools/mytool

 * `<topdir>/Builddesc`
          CONFIG(... ruledeps[... myrule:$mytool] ...)

This uses the fact that rules have to defined globally, but the variables that the rules are using are scoped. So we can get away with having a local scope on the definition of the tool we use.

Notice the special top level CONFIG addition. This tells ninja that all targets built with the new rule should depend on the tool. If the tool changes, everything built with the tool should be rebuilt. If you depend on a tool and a configuration file you can specify multiple dependencies for the rule separated by a comma, like this `ruledeps[... myrule:$mytool,$myconfigfile ...]`.

### Globbing ###

Whenever an argument accepts real source files those source files can also be given with a *-glob. The build script will keep dependencies correct and react when the source directory changes. Globbing only works on real source files that exist in source directories. Any files generated by intermediate steps will still have to be specified with full name. The current arguments that support globs are: `srcs`, the source part of `specialsrcs`, `conf` and `scripts`. Example:

    TOOL_PROG(tpc
        srcs[*.yy *.ll *.cc]
        libs[pcre platform_util]
        copts[-Wno-unused]
    )

Globbing is guaranteed to remove duplicate source files and preserve order, so in case you do a specialsrcs with tmplin, you can specify the main template first and then *.tmpl.

## Libraries provided by platform ##

### Third party ###

The 3rd party libraries are the same as before: libhunspell, libevent, libyalj. Those can in some cases be broken out and imported as an rpm if you want to do that.

### Platform libraries ###

Additionally, to make writing Builddesc files less hair-tearing, parts of the platform have been bundled up into platform specific libraries.

#### libplatform_util ####

Various utility functions; xmalloc, log_printf, buf_string, avl trees, query string parsing, etc. On a conceptual level this is stuff that can be put up on github without revealing anything about how our platform works. If a protocol or algorithm is described on wikipedia, it can be in here.

Also, this library doesn't have any dependencies on anything not public. If your implementation of google protocol buffers requires bconf to set up it doesn't belong in here, if it doesn't do search engine calls, bconf and transactions it probably does.

#### libplatform_core ####

The core of our platform. The central thing here is bconf. Almost everything in here is configured by bconf, so that's a clear sign that something belongs in here.  Components include the search engine api, trans api, redis and memcache apis, settings, qs_sq, etc.

#### libplatform_search ####

The text processing part of the search engine and indexing. charmap, langmap, synonyms, splitting of text, interface to read/write indexes.

#### libplatform_templates ####

The goo needed to render templates and various template filters and functions.

## Custom library paths ##

If you have libraries in custom places, e.g. `/usr/pgsql-x.x`, the best way to find them is to modify your `compile.sh` to setup the paths:

    export LIBRARY_PATH=/usr/pgsql-x.x/lib:$LIBRARY_PATH
    export CPATH=/usr/pgsql-x.x/include

Maybe even

    export PATH=/usr/pgsql-x.x/bin:$PATH

This is done at the very top of the script.

## Static analyser ##

You can run the clang static analyser on your code. This is setup
automatically, but disabled by default since it's very slow. Run

./compile.sh build/<flavor>/analyse

to generate a report in that directory.
