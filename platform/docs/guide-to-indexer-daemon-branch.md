# indexer-daemon branch exegesis #

## Branches ##

The indexer-daemon branch is based off the index-offset branch. This
is because both branches do quite intrusive changes to the same files
and would cause too much grief to resolve conflicts later.  So to diff
and log do:

git diff origin/index-offset...
git log origin/index-offset...

Unless of course you want to figure out the index-offset branch at the
same time. But it makes more sense to do separately. The fixes to
index-offset that were made in the indexer-daemon branch have been
merged back. If I missed something, let me know and I'll merge them
back.

Other branches that have been merged into this branch:

command-threads-stats-dump
stat_messages (through command-threads-stats-dump)
stat-counters (through command-threads-stats-dump)
command-threads

### command-threads ###

command-threads is a branch for a generalized mechanism for
controlling daemons. Like the command thread we have in the search
engine and others, this provides a relatively simple mechanism for
hooking in handlers that receive commands through http and query or
modify the behavior of a daemon.

### stat-counters and stat_messages ###

stat-counters and stat_messages are a generalized mechanism for
registering and querying counters and messages. Counters are numbers
that we can show - number of calls to something, number of bytes
transferred, cache hits/misses, etc. Messages are for setting various
messages like what a thread is doing, some internal state, etc.


### command-threads-stats-dump ###

command-threads-stats-dump is a branch that ties command threads and
stat_counters and stats_messages together, so that every daemon that
uses counters and messages also can have a very simple way to plug in
a handler to query messages and counters.

## New general functionality ##

Things in here might be a mix on all the branches.

### controller threads ###

This is from the misnamed command-threads branch. To use controllers
we setup a bunch of threads that respond to HTTP requests with a bunch
of callback functions that handle different steps of the http request.

 `start`

     beginning of the request, called as soon as we've parsed the url
     and figured out that this will be the handler we use.

 `consume_post`

     eat POST data. The idea is that there will be a general function
     that decodes JSON if that's posted, but currently that's not
     implemented because it wasn't. This is used by indexer-daemon to
     receive deltas and indexes from the indexer to the search engine.

 `finish`

     this callback is called after the full requests is processed.
     Most handlers will only need this callback. Used to stopping
     daemons, reloading indexes, getting stats, etc.

Controller threads are available through the `platform_controller`
library. The main setup function is `ctrl_thread_setup` (if someone
wants to clean up the `retthr` argument and tearing down of the
threads, it would be a nice refactoring project.

### stat counters ###

Very simple uin64_t counters with structured names. Made available to
look at through the `stat_counters_foreach` function. There are two
types of counters - static ones that are declared through linker sets
and dynamic ones that are allocated and free dynamically by the
application.

Possible refactoring here is to make the `stat_counter_dynamic*` api
match `stat_message_dyn*`.

### stat messages ###

Like stat counters, but for strings instead of simple counters.
Messages are for example when the indexer daemon sets a message for
each major step of the indexing so that we can easily query it and see
what it's doing. Another use right now is to clearly show what version
of the indexes are active/pending, what checksums they have, etc.

### lib/common/sha2.c ###

We should start using SHA2 for newer checksums. MD5 is already broken
beyond repair and SHA1 is creaking. The sha2 code is brought in
from OpenBSD libc.

### parse_query_string tests ###

New tests for parse_query_string have been added. Mostly so that I can
understand what's going on in those functions.

## High level overview of new indexing ##

The general idea is to have the indexer daemon compute the deltas
continuously, distribute them without external scripts and decide
when to reload indexes.

For this we need a new structure for how indexes are maintained
on both the indexer and search engine and to ensure consistency.

Thoughts about how to acheieve that are written down in
docs/pgq-indexing-design.md

### classic mode ###

Unless the indexer daemon is enabled, the indexer and search engine
function exactly like before.

The new mode is enabled with `index.daemon.enable` config key. This
enabled the new mode in both the indexer and search engine.

### new indexer index handling ###

We're moving away from the idea of an index being a directory. This
actually complicated things beyond belief. All indexes that the
indexer maintains are kept in one directory, configured through the
`index.daemon.workdir` configuration key.

### new search index handling ###

Same as above, but `search.workdir` key instead.

### search and indexer controllers ###

To work in the new mode, both the indexer and search have to have
their controllers configured.

## New indexing specific functionality and general refactoring. ##

### bin/watch_indexer ###

There's a new program to fetch and show the stats from the indexer
(and soon search engine, not implemented yet) - bin/watch_indexer.

It's quite simple, but it makes life easier for my debugging.

### librarification of index_merge ###

Most of the work that index_merge does has been adapted to be in the
platform_search library callable from other daemons/programs. This
turned out to be relatively trivial. The only major change needed was
to return proper errors and not leak memory instead of just exiting on
errors. The diff looks scary, but it's mostly just moving most of
bin/index_merge/index_merge.s into lib/search/index_merge.c.

To use it, we open the index readers we want to merge and the index
writer we want to write to, fill in the fields `to`, `from_db` and
`nfrom` in `struct index_merge_state` and call `index_perform_merge`.

### open_index and index_prime_new ###

I really got pissed off at the cruft in the arguments to `open_index`
and fixed it. `open_index` and `index_prime_new` will now always
allocate a new index (instead of being passed in a potentially NULL
pointer to a `struct index_db`). And `open_index` no longer takes a
bunch of obsolete arguments. This was necessary because the
inconsistent handling of the pre-existing index to `index_prime_new`
could lead to use-after-free. Also, since the flags `INDEX_WRITER` and
`INDEX_WRITER_BLOB` were the same there's just one of them now.

Sorry, this might confuse the diff a lot, but it was necessary.

### memory only indexes ###

It's now possible to open an index in memory only.

You can open an index reader with `open_mem_index_reader`. It takes a
preallocated blob of memory and parses it as a blob index. This is so
that we can receive a delta on a search engine without writing it to
disk just to delete it a few seconds later. There's a bit of
refactoring needed here so that we can actually give it an mmap:ed
file and make it handle it just like a normal `open_index` this is
needed on the search engines to receive full pending and active
indexes.

You can open an index writer with `open_mem_index_writer`. This is so
that we can create a delta index without writing it to disk just to
delete it after merging.

### index writer downgrading ###

To make it simple to create and apply a delta in memory in the same
process, there's now a function to downgrade an index writer to an
index reader - `index_writer_downgrade_to_reader`.

## indexer refactoring ##

To be more manageable the indexer needed some serious refactoring.

### struct indexer_config ###

This is a new struct that contains most of the previously global
variables. This is the static configuration that the indexer
needs. bconf, hunspell dictionaries, charmap, langmap, etc.

### struct indexing_state ###

This is a new struct that contains the state needed to perform one
indexing run instead of keeping everything in global variables.

It contains things like the current document, which index we're
writing to, timers, various flags that communicate between the
indexing template and the indexer, etc.

### filter_state ###

Because of the two above structs, the template filters and functions
can no longer access things through global variables. All the state is
now reached with a filter_state called "idxs". This gives a small
slowdown on the indexing, around 300ns per document. This should be
mostly invisible, even for LBC (8 seconds extra for a full index).

### others ###

A few things have also moved around in the indexer, put into
functions, etc. to make it simpler to be able to create indexes in
classic mode and daemon mode with the same code. Most notably, the
classic mode creation of indexes is done in the function `index_once`
which is also used by the daemon in case the daemon needs to create a
full index for some reason (startup most likely).

### daemon ###

The new functionality for running the indexer daemon is in the
indexer_daemon.c file.

#### struct indexer_daemon_state ####

As explained in the pgq-indexing-daemon.md document, the deamon
keeps track of two indexes: active and pending. This is handled by
`struct indexer_daemon_state`, each index has has a few messages
and counters connected to it (which is what we view with the
watch_indexer program) and some configuration.

#### delta creation and application ####

The indexer daemon loops and creates deltas. Each of these deltas are
passed to one of the delta consumer threads that are kept track of in
the `delta_consumers` TAILQ. There are two different delta
consumers. One thread takes the delta and merges it with the current
pending index to create the next pending index. The rest of the
threads send the deltas to the search engines, one thread per search
engine we keep in sync.

The locking protocol for deltas is a little bit funky:

The delta creator thread (main thread for now) grabs a write lock on
`delta_lock`. Ability of obtain a write lock means that no one is
using the old delta for anything anymore. The delta creator closes the
old delta and sets the pointer to point to the new delta (and sets the
messages and counters). Then it unlocks `delta_lock`, broadcasts a
condition to all the threads waiting for the next delta saying that a
new delta is ready for consuming, all the threads take the pointer to
the new delta, lock `delta_lock` with a read lock, then they wait at a
barrier to make sure that everyone is in the right state XXX and I
just realized that the current protocol is broken and should be fixed
differently. Ignore this part for now, read the comments in the
locking code instead.

## search refactoring ##

### search_instance split ###

The big problem with creating search_instance right now is that it
re-reads the configuration from disk. This more or less expects a
conscious decision from a sysadmin so that we don't re-read the
configuration when someone is editing it (or copying new dictionaries,
charmaps, etc.).

This can't be done when we're automatically reloading the indexes
in the background.

For this reason search_instance is split into search_instance and
search_instance_config.


## Tests. ##

### regress/indexer-bench ###

Manual test that I've used for manually testing the performance impact
of changes in the indexer. Might be nice to plug into automatic tests
to generate benchmark output. Does nothing unless ran manually right
now.

### regress/indexer-daemon ###

Manual test for now. Contains the necessary config of pgqd and an
example trigger for the ads table (not what needs to be in production,
I've started an action_states trigger, but it's not done yet).

The configuration is deliberately tweaked so that pgq pushes things as
fast as possible.

No automated tests yet because the search engine parts are not done.

Best way to run it is to first run in one window:

    watch -n 1 ./build/regress/bin/watch_indexer http://<...>/stats

Then:

    make -C regress/indexer-daemon rd

and then:

    make -C regress/indexer-daemon populate-db-activate_five

The last thing activates five ads which should TRIGGER the pgq, which
gets caught by the indexer and the result should be visible in the
watch_indexer window.

If you want to read the stats from the indexer daemon manually do:

    curl http://<...>/stats?pretty_json=1

`pretty_json=1` makes the json human readable.

# XXX #

index_stats.c has a docoff >> 1 that hasn't been merged to the
index-offset branch.

delta_lock should be grabbed with the mutex held in indexer daemon.
