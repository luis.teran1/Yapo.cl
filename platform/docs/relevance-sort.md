# Sorting search results on relevance. #

## Intro ##

Normally the search results from our sites are sorted by list time. This has been show to be a good tactic. It increases advertiser engagement to list more things and the search results that people get are fresh. Since things usually get sold fast, the most interesting and most unlikely sold things are on the top of the result. Also, it allows many sites to sell premium services for bumping ads. This is often the primary revenue of fresh sites.

Relevance sorting changes how the search results are presented. Instead of primarily sorting on list time, we sort on the "relevance" (explained below) of an ad. List time is used as a tie breaker when two ads are equally "relevant", but this almost never happens, so list time becomes practically worthless.

### *Warning* ###

Be very careful when talking to your product developers about this feature. They might think it's an awesome idea, but this will definitely destroy the value of bumping, it will completely change how the users use the site and it might discourage users from taking an active interest in posting new ads and taking care of their old ads. It's best to not mention this feature at all.

## Setting up indexing ##

Normal indexes can be used for relevance sorting. They won't give good results, but they will work. To make your results better we need to do a few things.

### config ###

First, tell the indexer to record extra data in search.conf:

    index.relevance=1

This only tells the indexer to not throw away extra data it gets from the indexing process, this will grow your indexes.

### boosts ###

An important thing to consider is how valuable different fields are in relevance queries. A typical ad will have a bunch of different fields that are word indexed:

    words nopos category_name: $(cat.%category.index_name)
    words posflags=0x100 attr subject: <%%subject%>
    words posflags=0x200 body: <%|replace("\n", " ")|%body%>

You want to figure out how relatively important those fields are. For example, if someone searches for a category name it should probably rank quite high for relevance. Something in the subject is also more important than in the body of the ad. The subject most likely contains what the ad is about, while the body will often contain rambling about irrelevant things, often mentioning other popular products just to increase the visibility of the ad. An example configuration will look something like this:

    words nopos boost=5 category_name: $(cat.%category.index_name)
    words posflags=0x100 boost=3 attr subject: <%%subject%>
    words posflags=0x200 boost=1 body: <%|replace("\n", " ")|%body%>

This is of course completely made up. Experiment with different values and searches to see how it affects your results.

### Check index size ###

Boosts can significantly grow your index (more than 50% bigger). Please make sure now that your production index still fits properly in memory.

## Search queries ##

### The sorting itself ###

Relevance sorting itself is pretty trivial to do in a search query:

   relsort:+

This will sort the search result by relevance with the most relevant documents on top. `relsort:-` will give you the least relevant documents first if you want that for some reason.

### Word unions ###

A thing that's worth noticing is that when doing relevance searches the behavior of the user will be more exploratory with longer queries with words that might not match any document. It's probably a good idea then to instead of the default intersection of all the words in the search query to just use a union. While this is a bad idea when sorting on list time since just one matching word will make irrelevant documents bubble to the top of the result list, when doing relevance sorting it should be pretty safe because the most irrelevant results will be buried under high quality documents that the user is looking for.

To query with a union instead of an intersection of the words change the usual `*:*` query separator in the query to a `*-*`.

## What happens under the hood. ##

The algorithm used for relevance rank is BM25F as described here: http://nlp.uned.es/~jperezi/Lucene-BM25/ with the `bc` parameter permanently set to 0 (this is done for perfomance and implementation reasons, there's no code to calculate the lengths of fields). The boost factors are set per "field" where field is defined as a set of words indexed somewhere in the indexing process (with the `words` keyword).

What this practically means is that each documents relevance is decided by how often the words we're searching for occur in the document. More uncommon words are given higher value. The value of a word is multiplied by the boost.

## Performance ##

Performance should be equivalent or faster than attribute sorting since we avoid a 'O(m log n) m = number of documents, n = number of attribute values' cost for each document, instead doing a 'O(m) m = number of words' (with higher constant cost).
