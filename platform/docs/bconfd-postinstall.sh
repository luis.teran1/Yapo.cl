#!/bin/bash
# Post installation script for bconfd
BDIR=/opt/blocket
test -x /usr/sbin/useradd && ADDUSER=/usr/sbin/useradd
test -x /usr/sbin/adduser && ADDUSER=/usr/sbin/adduser
create_duid () {
        /usr/sbin/groupadd www
        $ADDUSER -g nobody -G www -d /var/empty/$1 $1 || exit 1
        mkdir /var/empty/$1 >/dev/null 2>&1
        chmod 755 /var/empty/$1
}

grep "^bconfd:" /etc/passwd || create_duid bconfd
ln -sf $BDIR/etc/init.d/bconfd /etc/init.d/bconfd
mkdir -p /var/run/bconfd
chown -R bconfd.nobody /var/run/bconfd
chkconfig --add bconfd
chkconfig --level 2345 bconfd on
echo "Bconfd is installed. Manual restart needed."
