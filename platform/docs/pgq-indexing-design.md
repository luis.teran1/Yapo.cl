## Design considerations ##

 - No overlap between deltas

 - No missed ads to be indexed

 - No large joins with action_states (on delta creation)

## PGQ feed ##

The currently agreed on solution is to have the PGQ feed us tuples
with (ad_id,state_id). The indexer daemon takes those tuples, check if
the state_id exists in the replica and if it doesn't retry the event
with a short, configurable delay. It might be a good idea to set up a
default delay on reading the queue (this is possible) that puts the
majority of the ads within the replication delay.

The pgq is fed by a trigger on action_states that's equivalent to the
current select in the index_select template.

## Full index ##

Can still be done with the classic select.

Could it make sense to feed pgq with all currently active ads? Or is
20M events too insane? It probably is, there is a better solution.
Tests show that this is perfectly doable and pretty quick, but it puts
a very heavy load spike on the master. Also, since feeding the pgq in
this case is done within a single transaction we get a batch with all
the inserted events which makes everything fall over (why can't pgq
divide up the batch more?). This is probably a terrible idea.

It's a bad idea to drain the queue before/after full index because of
replication delay. The closest we can come to getting something safe
is: unregister/register consumer, wait for one event, wait until
replication has caught up to that state_id, then start full index.
Then process all the events in that batch as part of the full index.
This is overly complex and not necessary, we have to accept a certain
amount of overlap between a full index and the first delta (this
already happens today).

## Multiple indexers? / Threaded indexing? ##

Since there can be multiple batch processors connected to the same pgq
consumer (this terminology is really confusing[1]) there's nothing
other than index distribution logistics preventing us from having
multiple indexers processing the deltas. And in case of the full index
we can figure out a way to divide up the ad_id space to index and
distribute that. Let's keep that in mind when doing everything else
here.

## PGQ configuration and template filters/functions ##

There's a problem right now with us only being able to have one
pgsql_session in the indexing templates. To handle the pgq correctly
we'll want to get events from the master and read the ads data from a
slave. There's a good chance that we'll have to do the pgq handling
outside of the index templates anyway, but this is something that
bears thinking about.

## Index writing and syncing ##

Since the pgq sends us ads in batches and the events in a batch are
discarded after the batch is properly finished we need to sync the
index to disk (with fsync) after every batch if we want to be as crash
proof as the current indexers.

There is an additional caveat here that is worth keeping in mind.

The way the indexing is done currently if an indexer machine burns up
before distributing an index to the search engines it can be restored
by pulling up a new machine, taking any index from a search engine and
restarting the deltas starting from that index. With a pgq based solution
if the indexer burns up after creating an index but before distributing it
we've lost and have to perform a full index.

Maybe a part of this solution should be to distribute the deltas to
the search engines for redundancy and safety?

## Delta naming ##

A part of the previous problem. How do we name the deltas? Previously we
had only one delta in flight at any point in time. Now we can end up
with dozens if not hundreds.

Maybe a part of this solution should be to distribute the deltas to
the search engines as soon as they are made?

## document versioning ##

With potentially multiple indexers running we can end up in a
situation where one indexer delivers us an index delta that contains
an older version of a document.

Even with just one single central indexer we can still end up in a
sitation where the full index gave us a more recent version of a
document than what it is being fed from the queue since the queue has
to be started/reset _before_ the full index starts.

There needs to be versioning of documents. This can in the current
model be done by keeping track of the max(state_id) of each ad we
reindex. Will this make the full index too expensive?

XXX - is this really necessary? Indexing always picks up the latest
version of an ad and even when we reindex an ad we'll get the newest
version. Worst case is that when we get (17,1) (17,2) in the queue
and (17,1) is in one batch it can potentially index the state_id=2
version of the ad anyway. 

## index distribution ##


When distributing the delta from the indexer to the search engines
the indexer only needs to wait for one search engine to succeed.

The state in the search engines and the indexer:

1. indexer:

%workdir% = index.daemon.workdir=/path/to/workdir

1.1. Active index - in memory and on disk as %workdir%/active

1.2. pending index - "current index" + the deltas in 1.1 - in memory
     and on disk as %workdir%/pending.
 
1.2.1. If there have been no deltas between active index and now, pending
     index doesn't exist on disk and is the same as the active index.

1.3. (as a potential optimization: all deltas between active and
     pending in memory.).

2. search:

%indexdir% = search.indexdir=/path/to/indexdir

2.1. Active index in memory, serving queries and on disk as %indexdir%/active

2.2. Pending index - "current index" + all received deltas from the indexer
     in memory and on disk as %indexdir%/pending

2.2.1. See 1.2.1

The delta distribution process is:

0. Assumptions.

0.1. Index distribution maintains the "pending" index on the search
   engines and the indexer.

0.1.1. In case of things getting out of sync, the pending index on the
   indexer is the authoritative index.

0.2. The pending index is the result of the "active" index plus all the
     deltas.

0.3. In case of an indexer crash without filesystem loss the pending
   index on the indexer will have all deltas applied and can restart from
   that.

0.3.1. There is a small window for the indexer to crash between fsync
   on the pending index and sending COMMIT to pgq, in this case the last
   delta will be applied twice, this is ok. Other mechanisms in here will
   ensure that the pending index on the search engines will be correct
   even though it might eat a lot of bandwidth.

0.4. In case of a search engine crash the search engine will receive
   the pending index from the indexer (this can be optimized by having
   the indexer keep a log of deltas between the last active index and
   the current pending index).

1. indexer opens a pgq batch.

2. indexer generates delta based on the batch. (batch remains open)

3. indexer sends the delta to search engines and simultaneously it
   performs a merge to pending.

3.1. delta is sent with: sha256 of delta, sha256 of the pending index.

3.2. delta reception and finalized merge is acknowledged with sha256
     of the resulting index.

3.3. The search engine can respond that the sha256 of the delta
     mismatched. Retry 2 more times.

3.3.1. If retry doesn't work, that search engine is broken and is told
     to kill itself. (there needs to be a command for it). Both the
     indexer and the search engine scream loudly to the log. This is a
     sysadmin error that can't be resolved automatically. The search
     engine stays down until it can come up again.

3.3.2. If the seach engine fails to kill itself, goto 6.

3.4. The search engine can respond that its pending index doesn't
     match the sha256 it was sent: (this is not a fatal error in case of
     crash recovery on either the search engine or indexer, don't try to
     figure out which, LOG_WARNING).

3.4.1. The indexer marks this search engine as in need of a new
     pending index.

3.4.2. After indexer finishes the merge, a new pending index is sent.

4. indexer waits for one search engine to confirm index reception and
   finalized merge.

4.1. If timeout (no search engines report success within X minutes), goto 6.

4.2. If at least one search engine confirmed a finalized merge:

4.2.1. indexer waits until its own index_merge is finished. Indexer
   calculates a sha256 sum of its local merged index.

4.2.2. If checksum on any of the search engines mismatches what the indexer
   has, goto 6. (this is a fatal bug in merging).

5. After enough time has passed/enough deltas have been generated/other
   mechanism, it's time to make a new active index.

5.1. Indexer makes sure that all the search engines have "pending" in sync.

5.2. command to activate the pending index is sent.

5.3. command is acknowledged when the new index starts serving queries.

5.3.1. If one or more, but fewer than all search engines don't respond
   within reasonable time, follow procedure in 3.3.1.

5.3.2. If all search engines failed, CRIT in log, goto 6.

5.4. %db_name%.old is removed. (there's a possibility for archiving here).

5.5. %db_name% is renamed to %db_name%.old

5.6. %db_name%.pending is renamed to %db_name%

5.7. active = pending, (deltas ar purged from memory if necessary)

6. batch/delta/indexing failure

6.1. ROLLBACK the batch, CRIT in the log once per minute until a
     sysadmin resolves the problem.


## index naming on search engines ##

normal state for search engine:

%db_name%   -  current index.

startup/reload/hup for search engine:

before:
assert(%db_name%.new doesn't exist)
rename %db_name%.pending to %db_name%.new (if pending exists)

during:
%db_name%.old removed
%db_name%.new renamed to %db_name% if .new exists.

after:
%db_name% current index


search.index_merge:

base index: %db_name%.pending or %db_name% (aka. si->db->bsdb)
result: %db_name%.pending



search responds to indexer with the sha2 of the merged index after
receiving a delta. If it doesn't match what the indexer has merged,
indexer sends full index.


## Issues ##

Will LBC be able to do a full index without sharding? Should shard
merging be built into the indexer for all cases? This is not hard to
implement.


[1] Terminology to unconfuse things:

 - pgq producer (or just producer): the trigger that puts things into
   pgq.

 - pgq consumer (or just consumer): the queue of the events where we
   keep track of which produced events we've consumed.

 - listener or batch processor: the thing that reads events from the
   pgq consumer and actually does things with them. This is what would
   normally be considered a consumer, but when dealing with pgq, isn't.