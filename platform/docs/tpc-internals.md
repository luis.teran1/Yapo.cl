# Template compiler internals #

## Overview ##

The template compiler parses template files and outputs C files that are then compiled with a normal C compiler. The overall function is split into several steps. The highest level view would be:

 * input is read and parsed by lex.

 * lex feeds a yacc that builds up a syntax tree. 

 * complation happens where the syntax tree gets transformed in various steps eventually leading to a tree of code nodes that can be output.

 * code is written to a c file.

## Lex ##

The lexer part of the compiler does a little bit too much work. It's implemented in [lex.ll](lex.ll).

### Start Conditions ###

We have the following *start conditions* (refer to lex documentation for the meaning of the term if you don't know what it means in lex speak; it does not mean what you think what it means), all of them *exclusive* (once again, RTFM, this is important):

 *  - the initial start condition. The only relevant thing that will be matched is `<%` for starting blocks and directives (we'll also catch `%>` to generate errors, but that's just a bonus). As you know, you can't have variables and vtree references without a block. From this start condition we can only go into `TMPL`. Everything else is text (aka. `HTML`).

 * `BLOCK_START` - beginning of a template block. We can match a filter - we then go into `FILTERS` or if a filter is missing we go into `BLOCK_CONDITIONS`.

 * `FILTERS` - parsing of filters, can match one or more filters and go into `FILTER` or the ending `|` and go into `BLOCK_CONDITIONS`.

 * `FILTER` - parsing of one filter call.

 * `BLOCK_CONDITIONS` - the part of a block where conditions can occur. After we matched 0 or more filters at the beginning of a block or after an else inside a block. If the opening parenthese of a condition doesn't happen, goes into `BLOCK_CONTENTS`.

 * `BLOCK_CONTENTS` - normal contents of a block.

 * `EXPR` - Contents of conditionals.

 * `FUNC` - Function calls.

 * `INDEX` - expressions inside indexed references eg. `%[INDEX]`

 * `VTREE` - Contents of vtree lookups.

 * `COMMENT` - Comments.

 * `CONSTRUCT` - Constructs. eg. `R[CONSTRUCT]`

 * `DIRNAME` - name of directive because we want to handle define directives differently. AAAARGH. Why isn't the name handling done in yacc?

 * `DIRECTIVE` - Direcive parsing.

## Yacc ##

The yacc part of the compiler is implemented in [lang.yy](lang.yy).

The best documentation for the grammar is the yacc source itself. A few tweaks are documented below. In general everything generates some subclasses of SyntaxNode that get inserted into the right place in the syntax tree.

### Whitspace handling ###

Most of the logic for eatspace is implemented in the yacc. Specifically the two macros `STRWHITE` and `STRNL` check the current eatspace setting and generate normal string nodes if the whitespace shouldn't be ignored. If there's a possibility that the whitespace should be ignored special StrWhite and StrNl syntax nodes will be created. Those are either thrown away or converted to strings in later compiler stages when the compiler knows what to do with them.

### conditionals ###

Be very careful when doing things to `conditional`, `elseifchain` and `optelse`. Those are very specifically crafted to avoid dangling elses and error out on an else without an if.

### tstring ###

tstring is contents of a block after filters and conditionals. parameter inside tstring means any reference to variables, vtrees and other blocks.

## The syntax tree ##

### SyntaxNode ###

The base class of the syntax tree is `SyntaxNode`. It's an abstract class and all nodes will be subclasses. All classes are declared in [syntax.h](syntax.h).

The central methods are:

 * `apply` - Iterates over all nodes in the syntax tree depth first and calls a callback function on all nodes. The callback on a node will be called after iterating over its children nodes (this is kind of implied in depth first, but there are two ways depth first can be implemented).

 * `resolve`, `collapse`, `optimize`, `convert`, `bind_locals`, `reduce`, `fold` - the main compilation steps.

 * `strcode`, `intcode`, `boolcode`, `varcode` - conversion operations. All syntax nodes have a "return type" which indicates what the node does naturally when converted into code. The conversion operations transform the node from their natural type, to code that does something else. `strcode` converts the node to something that will have the C type `(char *)`, `intcode` will have the type `(int)`, `boolcode` will be a boolean, `varcode` is special and will convert nodes to `(struct tpvar *)` which is what gets used in template function and filter parameters (and some other things). Some nodes can't be converted and will throw exceptions.

 * `operator==` and `operator<` - needed for STL containers. Be very, very careful when implementing those. Look at the other implementations and understand them. Among other things notice that because of operator overloading (especially for nodes) `a != b` is not the same as `!(a == b)`. When comparing nodes, only `==` and `<` are implemented, other operators will not do what you think.

### NodeSyntaxNode ###

`NodeSyntaxNode` is the generic container for other nodes. Has one important subclass: `OutSyntaxNode`.

### OutSyntaxNode ###

`OutSyntaxNode` is the general container of all stuff that generates output (the stuff that templates output). The subclass of `OutSyntaxNode` called `BranchSyntaxNode` is what will initially be the content of every block, except the top level block (the one where normal template syntax doesn't apply).

The top level SyntaxNode for the whole template is either a `TemplateSyntaxNode` for normal templates, or `DefineTemplateSyntaxNode` for `deffun` and `deffiler` templates.

### BlockSyntaxNode ###

`BlockSyntaxNode` is another special node. It describes a block of code (everything inside `<%%>`). The special thing about BlockSyntaxNode (other than the large amount of code that's needed to handle it correctly) is that every other `SyntaxNode` belongs to a block. Even the top level code that doesn't contain any template code has an implicit block around it. Blocks are a pretty central concept when dealing with lots of things inside the templates.

### InitLoopSyntaxNode ###

`InitLoopSyntaxNode` is also special in many ways. They only exist inside `BlockSyntaxNode::loops` and are never created during initial parsing. They come to life during the resolve step of compilation. More about that later.

## Compilation steps ##

The compilation steps are:

### resolve ###

Resolve is for "anchoring" branch and loop nodes. When created, loop and branch nodes don't know where they are actually looping. The only thing a loop node knows is that it should print out its value at the point where the number of `@` signs (or equivalent). Resolve is for loops and branches to locate their anchor point and add nodes where necessary to initialize the loop/branch. Loops are initialized through `InitLoopSyntaxNode` and branches are initialized (and incremented) through `BranchCountInitializerSyntaxNode`.

### collapse ###

Used in certain nodes to merge their contents with their siblings or parents. As a simple example:

    foo<%bar%>

without `collapse` would generate:

    template_output(..., "foo");
    {
        <unnecessary block initialization code>
        template_output(..., "bar");
        <unnecessary block cleanup code>
    }

`collapse` is responsible for that this can be compiled to:

    template_output(..., "foobar");

### optimize ###

Replace some common expressions with optimized versions, e.g. define filter is replaced by a direct call to `bps_insert`, if possible.

### convert ###

Some nodes can only work with values of a certain kind. This step is for setting up conversion between different value types where needed.

### bind_locals ###

Prepare code generation by figuring out what local variables are needed by the code and adding them to the correct block.

### reduce ###

Reduce converts nodes to CodeSyntaxNode nodes (or containers), it also concatenates strings and whitespace to proper strings and does all the final conversions and everything.

### fold ###

Flattens all node containers and converts CodeSyntaxNode nodes into strings.

## All classes ##

* SyntaxNode
  * NodeSyntaxNode - nodes
    * OutSyntaxNode
      * TemplateSyntaxNode
        * DefineTemplateSyntaxNode
      * BranchSyntaxNode
    * VararrSyntaxNode
  * StringSyntaxNode
  * StrWhiteSyntaxNode
  * StrNlSyntaxNode
  * IntegerSyntaxNode
  * CodeSyntaxNode
  * InitLoopSyntaxNode - code
    * ValuesInitLoopSyntaxNode
    * GlobInitLoopSyntaxNode
    * VtreeKeysInitLoopSyntaxNode
    * VtreeValuesInitLoopSyntaxNode
    * VtreeByvalInitLoopSyntaxNode
  * BlockSyntaxNode - filters[]*, loops[], content
  * ConditionSyntaxNode - condition, code
  * ConditionalSyntaxNode - conditions[]
  * ExpressionSyntaxNode - l, r
  * PVarSyntaxNode - name, index
  * LoopSyntaxNode
    * CountLoopSyntaxNode
    * LenLoopSyntaxNode
    * ValuesLoopSyntaxNode
      * GlobLoopSyntaxNode
  * GlobLenSyntaxNode - name
  * ListLenSyntaxNode - name
  * BranchCountSyntaxNode
  * BranchCountInitializerSyntaxNode
  * ArithSyntaxNode - l, r
  * ParamsSyntaxNode - params[]
  * FilterSyntaxNode - args
  * ConstructSyntaxNode - params, index
    * PConstructSyntaxNode
    * PHConstructSyntaxNode
    * RegexConstructSyntaxNode
    * LRUConstructSyntaxNode
    * MapConstructSyntaxNode
  * VtreeParamsSyntaxNode - const_parts[], dyn_parts[]
  * VtreeSyntaxNode - before, after, value
    * ValVtreeSyntaxNode
      * GetVtreeSyntaxNode
      * IfdefVtreeSyntaxNode
      * GetlenVtreeSyntaxNode
    * LoopVtreeSyntaxNode
      * KeysLoopVtreeSyntaxNode
      * ValuesLoopVtreeSyntaxNode
      * ByvalLoopVtreeSyntaxNode
  * FuncallSyntaxNode - params
