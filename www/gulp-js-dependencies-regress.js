/*
 * Source files needed to build each js file only in regress
 */
var jsDependenciesRegress = [
  {
    final: 'tag_tealium_regress.js',
    sources: ['www/js/tealium_regress.js']
  }
];

module.exports = jsDependenciesRegress;
