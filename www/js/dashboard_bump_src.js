/* global userOneClick, CartHandler, xiti:true, try_xtc, Yapo,
   paymentMethod, paymentChangeRegion, accountIsCompany, upsellingProcess,
   comboProcessPrice */

/* exported xiti */

var emptyAddressMsg;
var emptyCommuneMsg;
var emptyLobMsg;
var emptyNameMsg;
var emptyRegionMsg;
var emptyRutMsg;
var invalidRutMsg;

$(document).ready(function init() {
  'use strict';

  // Validation Messages
  var noMsg = $('#remove-no').val(); // 'No'
  var yesMsg = $('#remove-yes').val(); // 'Si'
  var deletionSuccessMsg = $('#remove-ad-success').val(); // 'El aviso fue borrado de tu carro'
  var deletionConfirmMsg = $('#remove-ad-confirmation').val();
    // '&iquest;Seguro quieres borrar este aviso de tu carro?'
  invalidRutMsg = $('#invalid-rut').val(); // 'Escribe un rut v�lido'
  emptyLobMsg = $('#empty-lob').val(); // 'Escribe tu giro'
  emptyRutMsg = $('#empty-rut').val(); // 'Escribe tu rut'
  emptyNameMsg = $('#empty-name').val(); // 'Escribe tu nombre'
  emptyRegionMsg = $('#empty-region').val();
  emptyCommuneMsg = $('#empty-commune').val();
  emptyAddressMsg = $('#empty-address').val(); // 'Escribe tu direcci�n'

  updateStickyValues();
  CartHandler.addListener(updateStickyValues);

  // The sticky-bar shouldnt be hidden
  $('#sticky-bar').show();

  if (typeof Yapo === 'object' && Yapo.informativeNotification) {
    Yapo.informativeNotification();
  }

  /*
   *  TODO: Transform all this into its own module (Yapo Style) and
   *  get all mobile specific into another file (this one is being shared
   *  between desktop and mobile)
   *
   **/
  (function mobilePayment() {
    // Choose between form types, boleta or factura
    (function initializeRadioButtons() {
      var ICON_CLASS = 'icon-yapo icon-check paymentType-icon';
      var RADIO_CLASS = 'paymentType-radioCheck';
      var OPTION_CLASS = 'paymentType-option';
      var CHECKED_CLASS = ' __checked';
      var TYPE_TEXT_CONTAINER = document.getElementById('billOrInvoice');
      var buttons = document.getElementsByClassName(OPTION_CLASS);

      var ICON;
      var RADIO;
      var OPTION;
      var firstSelectedButton;

      if (!buttons.length) {
        return;
      }

      function clearPreviousCheck() {
        if (!ICON && !RADIO && !OPTION) {
          return false;
        }

        ICON.className = ICON_CLASS;
        RADIO.className = RADIO_CLASS;
        OPTION.className = OPTION_CLASS;

        return true;
      }

      function checkMethod(element) {
        clearPreviousCheck();

        OPTION = element;
        RADIO = OPTION.children[0];
        ICON = RADIO.children[0];

        TYPE_TEXT_CONTAINER.innerText = OPTION.attributes['data-type'].value.toLowerCase();

        OPTION.className = OPTION_CLASS + CHECKED_CLASS;
        RADIO.className = RADIO_CLASS + CHECKED_CLASS;
        ICON.className = ICON_CLASS + CHECKED_CLASS;
      }

      function buttonListener(check, btn) {
        if (typeof check === 'function') {
          check(btn);
        }
      }

      function setButtonListeners(butt) {
        butt.addEventListener('click', function() {
          buttonListener(checkMethod, butt);
        }, false);
      }

      [].forEach.call(buttons, setButtonListeners);


      if (accountIsCompany === 't') {
        firstSelectedButton = buttons[1];
      } else {
        firstSelectedButton = buttons[0];
      }

      checkMethod(firstSelectedButton);
    })();

    return true;
  })();

  function changeForms() {
    var $this = $(this);

    if ($this.is(':checked')) {
      toggleForms($this.val());
    }
  }

  $('input[name=document]').each(changeForms);

  // Set modals for each payment method
  (function activateModals() {
    function setOneClickModal() {
      $.pgwModal({
        target: '#oneclick_info'
      });
    }

    function setWebpayModal() {
      $.pgwModal({
        target: '#webpay_info'
      });
    }

    function setServipagModal() {
      $.pgwModal({
        target: '#servipag_info',
        maxWidth: 840
      });
    }

    function setYapesosModal() {
      $.pgwModal({
        target: '#yapesos_info'
      });
    }

    function setKhipuModal() {
      $.pgwModal({
        target: '#khipu_info',
        maxWidth: 840
      });
    }
    $('#oneclick_preview').click(setOneClickModal);
    $('#webpay_preview').click(setWebpayModal);
    $('#servipag_preview').click(setServipagModal);
    $('#yapesos_preview').click(setYapesosModal);
    $('#khipu_preview').click(setKhipuModal);
  })();

  function yeah(element) {
    var itemId = $(element).attr('data-itemid');
    var inputs = $('#row' + itemId).find('input');
    var id = inputs[0].value;
    var prod = inputs[1].value;

    deleteItem(itemId);

    CartHandler.remove(prod, {
      list_id: id
    });
  }

  if ($('.remove-icon').length > 0) {
    $('.remove-icon').darkTooltip({
      gravity: 'west',
      content: deletionConfirmMsg,
      animation: 'flipIn',
      trigger: 'click',
      confirm: true,
      yes: yesMsg,
      no: noMsg,
      onYes: yeah,
      hideAll: true,
      data: 'itemid',
      finalMessage: '<i class = "sprite-checkout success-icon"></i>' +
        '<div class="success-message">' + deletionSuccessMsg + '</div>',
      finalMessageDuration: 1500
    });
  }

  function toggleSomeForms() {
    toggleForms($(this).val());
  }

  $('input[name=document]').each(function(index, elem) {
    $(elem).on('click', toggleSomeForms);
  });

  function toggleSomeShit(e) {
    e.preventDefault();
    toggleCaret(this);
    $('#email-placeholder-bill').toggle();
    $('#email-bill').focus();
  }

  $('a#modify-email-bill').on('click', toggleSomeShit, false);

  function toggleSomeOtherShit(e) {
    e.preventDefault();
    toggleCaret(this);
    $('#payment-forms-placeholder').toggle();
  }

  $('a#payment-forms').click(toggleSomeOtherShit);

  // What is this?
  $('.validation_msg.success').hide();

  function hideNext() {
    $(this).next().hide();
  }

  $('input').keyup(hideNext);

  function isThereAnEmail() {
    var $value = $('input[type=email]').val();
    if ($value !== '') {
      validateEmail($(this));
    }
  }
  $('input[type=email]').change(isThereAnEmail);

  $('#id_name').change(function() {
    validateMandatory($(this), emptyNameMsg);
  });

  $('#id_lob').change(function() {
    validateMandatory($(this), emptyLobMsg);
  });

  $('#id_address').change(function() {
    validateMandatory($(this), emptyAddressMsg);
  });

  $('#id_rut').change(function() {
    validateRut($(this), invalidRutMsg);
    validateMandatory($(this), emptyRutMsg);
  });

  $('#id_region').change(function() {
    validateMandatory($(this), emptyRegionMsg);
    paymentChangeRegion();
  });

  $('#id_communes').change(function() {
    validateMandatory($(this), emptyCommuneMsg);
  });

  validateMandatory($('#id_region'), emptyRegionMsg);
  validateMandatory($('#id_communes'), emptyCommuneMsg);

  $('#payment_form').submit(validateForm);

  $('.icon-close').click(function() {
    $('.products-info-wrap').hide();
  });
 
  //Add name option for prod_id selected
  $('.productTable-row').each(function(index, row) {
    var prodId = parseInt($(row).find('input[name="mpid[' + index + ']"]').val());

    if (CartHandler.products_options.prod_active.indexOf(prodId)) {
      var adId = parseInt($(row).find('input[name="mpad[' + index + ']"]').val());
      $(row).find(".productTable-description").append(getOptionName(prodId, adId));
    }
  });
  
  /*
   * @name getOptionname
   * 
   * @description
   * Get name of option for prod with multiple options
   *
   * @param {number} prodId product identifier
   * @param {number} adId advise identifier
   *
   * @return {string} name option
   *
  */
  function getOptionName(prodId, adId) {
    var nameOption = '';
    var optionSelected = CartHandler.getProductSelectedParam(prodId, adId);
    var prodOpts = CartHandler.products_options.options_list;
    
    $.each(prodOpts, function(ind, val) {
      $.each(val.prod_id, function(indx, prod_id) {
        if (parseInt(prod_id) === prodId) {
          nameOption = ' (' + val.option[optionSelected]  + ')';
        }
      })
    });

    return nameOption;
  }
});

function validateForm() {
  var display = $('#invoice').css('display');
  var validation;

  if (display === 'none') {
    validation = validateBoleta();
  } else {
    validation = validateFactura();
  }

  return validation;
}

function validateBoleta() {
  var $email = $('#email-invoice');
  var returnValue;

  if ($email.val().length > 0) {
    returnValue = validateEmail($email);
  } else {
    returnValue = true;
  }

  return returnValue;
}

function validateFactura() {
  var $email = $('#email-invoice');
  var $name = $('#id_name');
  var $lob = $('#id_lob');
  var $address = $('#id_address');
  var $rut = $('#id_rut');
  var $region = $('#id_region');
  var $communes = $('#id_communes');
  var hasError = false;

  // Msite use others ids for region and communes
  if (!$region.length) {
    $region = $('#pay_region');
  }

  if (!$communes.length) {
    $communes = $('#pay_communes');
  }

  if ($email.val().length > 0) {
    if (!validateEmail($email)) {
      hasError = true;
    }
  }

  if (!validateMandatory($name, emptyNameMsg)) {
    hasError = true;
  }

  if (!validateMandatory($lob, emptyLobMsg)) {
    hasError = true;
  }

  if (!validateMandatory($address, emptyAddressMsg)) {
    hasError = true;
  }

  if (!validateRut($rut, invalidRutMsg)) {
    hasError = true;
  } else if (!validateMandatory($rut, emptyRutMsg)) {
    hasError = true;
  }

  if (!validateMandatory($region, emptyRegionMsg)) {
    hasError = true;
  }

  if (!validateMandatory($communes, emptyCommuneMsg)) {
    hasError = true;
  }

  if (hasError) {
    return false;
  }

  return true;
}

function validateRut(field, msg) {
  var rut = field.val();
  var isValid = $.Rut.validar(rut);

  if (isValid) {
    showSuccess(field);
    return true;
  }

  showFailure(field, msg);
  return false;
}

function validateMandatory(field, msg) {
  if (field.val() && field.val().length > 0) {
    showSuccess(field);
    return true;
  }

  showFailure(field, msg);
  return false;
}

function validateEmail(field) {
  var invalidEmailMsg = $('#invalid-email').val();
    // 'Confirma que el correo electr�nico est� en el formato correcto'
  var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/; // eslint-disable-line

  $('#err_email').hide();

  if (re.test(field.val())) {
    showSuccess(field);
    return true;
  }

  showFailure(field, invalidEmailMsg);
  return false;
}

function showSuccess(field) {
  var $elem = getErrorElement(field);
  var $wrap = field.parent();

  field.parent().find('.icon-check-circled').show();

  $elem.removeClass('error');
  $elem.removeClass('input-error');
  $wrap.removeClass('wrap-error');

  $elem.addClass('success');
  $wrap.addClass('wrap-valid');

  $elem.html('');
  $elem.show();
}

function showFailure(field, errorMsg) {
  var $elem = getErrorElement(field);
  var $wrap = field.parent();

  $elem.removeClass('success');
  $wrap.removeClass('wrap-valid');

  if (errorMsg) {
    $elem.addClass('error');
    $elem.addClass('input-error');
    $elem.html(errorMsg);
    $wrap.addClass('wrap-error');
    $elem.show();
  }

  field.parent().find('.icon-check-circled').hide();
}

function isHorizontalSelect(field) {
  return field.parent().hasClass('hs-wrapper');
}

function getErrorElement(field) {
  var span;
  var newField;

  if (isHorizontalSelect(field)) {
    newField = field.next().find('.hs-trigger-link');
  } else {
    newField = field;
  }

  span = newField.next();

  if (!span || !span.length) {
    span = $('<span>');
    newField.after(span);
  }

  return span;
}

function toggleCaret(element) {
  var $element = $(element);
  $element.next('span.caret').toggleClass('right');
  $element.next('span.caret').toggleClass('down');

  $element.find('.caret').toggleClass('right');
  $element.find('.caret').toggleClass('down');
}

function toggleForms(type) {
  var $invoiceTooltipTip = $('#invoice-tooltip-tip');
  var $bill = $('#bill');
  var $invoice = $('#invoice');
  var emailInvoice;
  var emailBill;

  $invoiceTooltipTip.removeClass('left');
  $invoiceTooltipTip.removeClass('right');

  if (type === '1') {
    emailInvoice = $('#id_email_2').children('input').val();
    $bill.show(400);
    $invoice.hide(400);
    $('#id_email_1').children('input').val(emailInvoice);
    $invoiceTooltipTip.addClass('left');
  } else {
    emailBill = $('#id_email_1').children('input').val();
    $invoice.show(400);
    $bill.hide(400);
    $('#id_email_2').children('input').val(emailBill);
    $invoiceTooltipTip.addClass('right');
  }

  $('#radio-' + type).iCheck('check');
}

function deleteItem(itemId) {
  $('#row' + itemId).addClass('removing');

  setTimeout(function() {
    $('#row' + itemId).fadeOut(500);
  }, 1000);

  setTimeout(function() {
    $('#row' + itemId).remove();
    updateStickyValues();
  }, 1500);

  setTimeout(function() {
    $('.dark-tooltip.west').hide();
  }, 1500);

  setTimeout(function() {
    checkIfEmpty();
  }, 1600);
}

function getQtyProds(prodList) {
  var qty = 0;
  var i;
  var len;

  for (i = 0, len = prodList.length; i < len; i++) {
    qty += CartHandler.getQuantityByProduct(prodList[i]);
  }

  return qty;
}

function updateStickyValues() {
  var qbump = getQtyProds([
    CartHandler.BUMP,
    CartHandler.DAILY_BUMP,
    CartHandler.UPSELLING_DAILY_BUMP,
    CartHandler.WEEKLY_BUMP,
    CartHandler.UPSELLING_WEEKLY_BUMP
  ]);

  var qgallery = getQtyProds([
    CartHandler.GALLERY,
    CartHandler.UPSELLING_GALLERY,
    CartHandler.GALLERY_1,
    CartHandler.GALLERY_30
  ]);

  var qComboAt = getQtyProds([
    CartHandler.COMBO_AT_1,
    CartHandler.COMBO_AT_2,
    CartHandler.COMBO_AT_3
  ]);


  var qComboDiscount = getQtyProds([
    CartHandler.DISCOUNT_COMBO_1,
    CartHandler.DISCOUNT_COMBO_2,
    CartHandler.DISCOUNT_COMBO_3,
    CartHandler.DISCOUNT_COMBO_4
  ]);

  var qPackPremiumCar = getQtyProds([
    CartHandler.IF_PACK_CAR_PREMIUM
  ]);

  var qlabel = getQtyProds([CartHandler.LABEL, CartHandler.UPSELLING_LABEL]);

  var qmonthlystore = getQtyProds([CartHandler.MONTHLY_STORE]);
  var qquarterlystore = getQtyProds([CartHandler.QUARTERLY_STORE]);
  var qbiannualstore = getQtyProds([CartHandler.BIANNUAL_STORE]);
  var qannualstore = getQtyProds([CartHandler.ANNUAL_STORE]);

  var carpack = getQtyProds([CartHandler.CARS_PACK]) + getQtyProds([CartHandler.INMO_PACK]);
  var autobump = getQtyProds([CartHandler.AUTOBUMP]);
  var credits = getQtyProds([CartHandler.CREDITS]);
  var autofact = getQtyProds([CartHandler.AUTOFACT]);
  var inserting_fee = getQtyProds([CartHandler.INSERTING_FEE, CartHandler.IF_PACK_CAR, CartHandler.IF_PACK_INMO]);
  var inserting_fee_premium = getQtyProds([CartHandler.INSERTING_FEE_PREMIUM]);

  var amount = formatThousands(CartHandler.getTotalAmount());
  var stores = qmonthlystore + qquarterlystore + qbiannualstore + qannualstore;
  var all = qbump + qgallery + qlabel + qmonthlystore + qquarterlystore +
    qbiannualstore + qannualstore + carpack + autobump + credits + autofact + inserting_fee + qComboAt + qComboDiscount + inserting_fee_premium + qPackPremiumCar;

  updateStickyElementValue('#summary-bar-amount', all);

  if (typeof comboProcessPrice !== 'undefined' && comboProcessPrice) {
    amount = comboProcessPrice;
  }

  $('#total-amount').html('$ ' + amount);

  if (all === 0) {
    $('#sticky-bar').addClass('is-down');
  }

  if (qmonthlystore > 0) {
    $('#store_type').html('Mensual');
  } else if (qquarterlystore > 0) {
    $('#store_type').html('Trimestral');
  } else if (qbiannualstore > 0) {
    $('#store_type').html('Semestral');
  } else if (qannualstore > 0) {
    $('#store_type').html('Anual');
  } else {
    $('#store_type').html('');
  }
}

function updateStickyElementValue(element, value) {
  if (value > 9999) {
    $(element).html('+9999');
  } else {
    $(element).html(value);
  }
}

// xitiPayment info
/* eslint-disable */
function xitiPayment() {
  try {
    xiti = try_xtc('payment::premium_products::premium_products::webpay', 'N', this);
  } catch(e) {}
}
/* eslint-enable */

/*
 * This functions gets called through onclick on the following templates:
 * - msite/templates/mobile/payment/pay_multiproduct.html.tmpl
 * - templates/payment/pay_multiproduct.html.tmpl
 *
 * */
function send_payment_form() { // eslint-disable-line
  $("#loading").show();

  setTimeout(
    function(){
      CartHandler.validate();

      if (CartHandler.status === "OK") {
        xitiPayment();
        $("#loading").hide();
        $('#payment_form').submit();
      } else {
        $("#loading").hide();
        showDesktopModal('ShoppingCartModal');
        return false;
      }
    },
    10
  );
}

function formatThousands(amount) {
  var n = amount.toString();
  var rgx = /(\d+)(\d{3})/;
  while (rgx.test(n)) {
    n = n.replace(rgx, '$1.$2');
  }
  return n;
}

function checkIfEmpty() {
  var nElements = $('.productTable-row').length;

  if (nElements === 0) {
    location.reload();
  }
}

