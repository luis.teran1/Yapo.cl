$(document).ready( function(){

	if($('#error-details').length){
		$(".error-form li").each( function(){
			$(this).wrapInner("<span class = 'error-text'></span>");
			$(this).prepend("<i class = 'error-x'></i>");
		});
		
		$("a#error-more-info").show();
		$("a#error-more-info").click( function(){
			$.pgwModal({
				target: '#error-details'
			});
			setHelpPopupEvents();
			if ($(this).attr("data-xtpage-xtpage") && $(this).attr("data-xtpage-tag")) {
				xt_med('F', $(this).attr("data-xtpage-xtpage"), $(this).attr("data-xtpage-tag"));
			}
		});
	}else{
		$("a#error-more-info").hide();
	}

	setHelpPopupEvents();

});

function showCustomerSupportPopup(clicked_tag){
	$.pgwModal({
		target: '#pay-help'
	});
	setCustomerSupportPopupEvents();
	$("#help-ok-message").hide();
	$("div.error").hide();

	if (clicked_tag.attr("data-xiti-page-xtn2") && clicked_tag.attr("data-xiti-page-tag")) {
    	xt_med('F', clicked_tag.attr("data-xiti-page-xtn2"), clicked_tag.attr("data-xiti-page-tag"));
    }
}

function showMessageFormSubmitted(){
	$(".pm-content #help-form").hide();
	$(".pm-content #help-ok-message").show();
	$(".pm-content #ok-help").click( function(){
		$.pgwModal('close');
	});

	$("input").blur();
	$("textarea").blur();
}

function setCustomerSupportPopupEvents(){
	
	$(".pm-content .help-form").submit( function(e){
		e.preventDefault();

		$("div.error").hide();
		//validate fields
		var errors = false;
		if($.trim($(".pm-content #name").val()).length == 0){
			$(".pm-content #error-name").show();
			errors = true;
		}
		if($.trim($(".pm-content #email").val()).length == 0){
			$(".pm-content #error-email").show();
			errors = true;
		}
		if($.trim($(".pm-content #support_body").val()).length == 0){
			$(".pm-content #error-body").show();
			errors = true;
		}

		if(!errors){
			$.ajax({
				url: $(this).attr('action'),
				data: 	{
					is_ajax: 1,
					name: $(this).find('#name').val(),
					email: $(this).find('#email').val(),
					support_subject: $(this).find('#support_subject').val(),
					support_body: $(this).find('#support_body').val()
				},
				type: $(this).attr('method'),
				dataType: 'json',
				success: function(data, status, jqXHR){
					
					if(data.status == "OK"){
						showMessageFormSubmitted();
					}else{
						if (data.email != null) {
							$(".pm-�tent #error-email").html(data.email)
							$(".pm-content #error-email").show();
						}
						if (data.name != null){
							$(".pm-content #error-name").html(data.name)
							$(".pm-content #error-name").show();
						}
						if (data.support_body != null){
							$(".pm-content #error-body").html(data.support_body)
							$(".pm-content #error-body").show();
						}
					}
				},
				error: function (data, status) {
					showMessageFormSubmitted();
				}
			})
		}
	});
}

function setHelpPopupEvents(){
	$("span.nohistory a").click( function(){
		showCustomerSupportPopup($(this));
	});

	$("a#help-payment").click( function(){
		showCustomerSupportPopup($(this));
	});
	
	$(".popup-acc-body").hide();

	$(".popup-acc-head").click( function(){
		
		$(".popup-acc-body").not($(this).next()).hide();
		$("span[class~=\"ui-accordion-header-icon\"]").removeClass("ui-icon-triangle-1-s");
		$("span[class~=\"ui-accordion-header-icon\"]").addClass("ui-icon-triangle-1-e");
		
		if($(this).next().is(":visible")){
			$(this).next().hide();
			$(this).find("span[class~=\"ui-accordion-header-icon\"]").removeClass("ui-icon-triangle-1-s");
			$(this).find("span[class~=\"ui-accordion-header-icon\"]").addClass("ui-icon-triangle-1-e");
		}else{
			$(this).next().show();
			$(this).find("span[class~=\"ui-accordion-header-icon\"]").removeClass("ui-icon-triangle-1-e");
			$(this).find("span[class~=\"ui-accordion-header-icon\"]").addClass("ui-icon-triangle-1-s");
		}
	});

}