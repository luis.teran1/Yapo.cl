function hideLoading() {
  $('.boxSpinner-logo').hide();
  $('.boxSpinner').hide();
}

function showLoading(isNormal, social) {
  (isNormal) ? $('.boxSpinner').addClass('__normal') :
    $('.boxSpinner-logo' + social + '').show();
  
  $('.boxSpinner').show();
}


