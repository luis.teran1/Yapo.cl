'use strict';

function get_settings(setting_name, keylookup_func, settings_root, extra) {
  if (!settings_root)
    settings_root = settings;

  var res;

  for (var i in settings_root[setting_name]) {
    var setting = settings_root[setting_name][i];
    var val;

    val = null;
    if (settings_root[setting_name][i]['keys']) {
      for (var j in settings_root[setting_name][i]['keys']) {
        var key = settings_root[setting_name][i]['keys'][j];
        var key_val = keylookup_func(key, extra);

        if (setting[key_val]) {
          setting = setting[key_val];
        } else if (setting['*']) {
          setting = setting['*'];
        } else {
          break;
        }
      }

      if (setting['value'])
        val = setting['value'];
    } else if (settings_root[setting_name][i]['default']) {
      val = settings_root[setting_name][i]['default'];
    }
    if (val) {
      if (res)
        res += ',' + val;
      else
        res = val;
      if (!settings_root[setting_name][i]['continue'])
        break;
    }
  }

  return res;
}

function split_setting(val) {
	if (!val)
		return {};

	var arr = val.split(',');
	var res = {};

	for (i = 0; i < arr.length; i++) {
		var kv = arr[i].split(':', 2);

		if (kv && kv.length > 1)
			res[kv[0]] = kv[1];
		else {
			res[arr[i]] = 1;
		}
	}

	return res;
}
