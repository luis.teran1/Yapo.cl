var Yapo = Yapo || {};

/**
 * @name toggleAdvice
 * @description
 *
 * @param {String} adviceSelector - Advice CSS selector
 * @param {Object} features - Reference to global variable called `category_features`
 * @param {Object} settings - Reference to global variable called `category_settings`
 * @param {String} options.featureKey - Bconf name to show or hide the advice element
 * @param {String} options.category - Reference to the selected category
 * @param {Object} options.replaceItems - Object with the "template variables"
 * @param {Boolean} options.formatPrice - Indicates if the feature value should be formatted as money
 * @param {Boolean} options.user.logged - Indicates if the user is logged
 * @param {Boolean} options.user.isPro - Indicates if the user is pro for the selected category
 * @param {Boolean} options.user.autoClose - Indicates that advice will be close automatically
 *
 */
Yapo.toggleAdvice = function(adviceSelector, features, settings, options) {
  var advice = document.querySelector(adviceSelector);
  var userInfo = document.querySelector('.account-info');
  var dataLogged;
  var dataPro;
  var isLogged;
  var isPro;

  if (
    !advice ||
    !features ||
    !settings ||
    !features.hasOwnProperty(options.featureKey) ||
    !settings.hasOwnProperty('ai_alert') ||
    (!userInfo && options.hasOwnProperty('user'))
  ) {
    return;
  }

  if (userInfo && options.hasOwnProperty('user')) {
    dataLogged = userInfo.dataset.islogged;
    dataPro = userInfo.dataset.isprofor;

    isLogged = Boolean(dataLogged) === options.user.logged;
    isPro = (dataPro.length > 0 && dataPro.indexOf(options.category) !== -1) === options.user.isPro;

    if (!(isLogged && isPro)) {
      advice.style.display = 'none';
      return;
    }
  }

  var adviceFeature = features[options.featureKey];
  var adviceSetting = settings.ai_alert[1];
  var hasCategoryFeature = adviceFeature && adviceFeature.cat.hasOwnProperty(options.category);
  var hasCategorySetting = adviceSetting && adviceSetting.hasOwnProperty(options.category);
  var replaceItems = {};
  var priceRegex = /(\d)(?=(\d{3})+(?!\d))/g;

  if (!hasCategoryFeature || !hasCategorySetting) {
    advice.style.display = 'none';
    return;
  }

  replaceItems[options.featureKey] = adviceFeature.cat[options.category];

  if (options.formatPrice) {
    replaceItems[options.featureKey] = replaceItems[options.featureKey][options.platform]
      .toString()
      .replace(priceRegex, '$1.');
  }

  if (options.hasOwnProperty('replaceItems')) {
    Object.keys(options.replaceItems).forEach(function(replaceKey) {
      replaceItems[replaceKey] = options.replaceItems[replaceKey];
    });
  }

  adviceSetting = this.getSettingAsObject(adviceSetting[options.category].value);

  this.setAdviceText(adviceSelector, {
    showAdvice: !!adviceSetting[options.featureKey],
    autoClose: options.autoClose,
    autoCloseDuration: adviceFeature.duration || 10000,
    replaceItems: replaceItems
  });
};

/**
 * @name setAdviceAutoCloseable
 * @description
 *
 * Enable events to close advice with a button or automatically
 *
 * @param {String} adviceSelector - Advice CSS Selector
 * @param {Number} duration - Advice duration before auto close
 *
 */
Yapo.setAdviceAutoCloseable = function(adviceSelector, duration) {
  var advice = document.querySelector(adviceSelector);
  var adviceClose = advice.querySelector(adviceSelector + '-close');

  adviceClose.addEventListener('click', function() {
    advice.style.display = 'none';
  });

  setTimeout(function() {
    advice.style.display = 'none';
  }, duration);
}

/**
 * @name setAdviceText
 * @description
 *
 * Set the advice template to the advice HTMLElement, replacing the "template variables".
 *
 * @param {String} adviceSelector - Advice CSS selector
 * @param {Boolean} options.showAdvice - Shows or Hides the advice element
 * @param {Object} options.replaceItems - Object with the "template variables"
 *
 */
Yapo.setAdviceText = function(adviceSelector, options) {
  var adviceElement = document.querySelector(adviceSelector);
  var adviceTextElement = adviceElement.querySelector(adviceSelector + '-text');
  var adviceText = adviceTextElement.dataset.template;
  var replaceItem;

  Object.keys(options.replaceItems).forEach(function(replaceKey) {
    replaceItem = options.replaceItems[replaceKey];
    adviceText = adviceText.replace('{' + replaceKey + '}', replaceItem);
  });

  adviceTextElement.innerHTML = adviceText;
  adviceElement.style.display = options.showAdvice ? 'block' : 'none';

  if (options.showAdvice && options.autoClose) {
    this.setAdviceAutoCloseable(adviceSelector, options.autoCloseDuration);
  }
};

/**
 * @name getSettingAsObject
 * @description
 *
 * Converts a bconf string value to a dictionary
 *
 * @param {String} setting - Bconf setting like `settinga:1,settingb:1`
 *
 * @returns {Object}
 */
Yapo.getSettingAsObject = function(setting) {
  var settingObject = {};
  var currentSetting;

  setting.split(',').forEach(function(settingValue) {
    currentSetting = settingValue.split(':');

    settingObject[currentSetting[0]] = isNaN(currentSetting[1])
      ? currentSetting[1]
      : parseInt(currentSetting[1], 10);
  });

  return settingObject;
};

/**
 * @name toggleJobEditionAdvice
 * @description
 *
 * Changes the advice visibility for categories that have `edit_allowed_days` setting enabled.
 *
 * @param {String} shownCategory - Selected category id. (ie. "7020")
 * @param {String} categoryName - Selected category name. (ie. "Ofertas de empleo")
 * @param {Object} features - Reference to global variable called `category_features`
 * @param {Object} settings - Reference to global variable called `category_settings`
 */
Yapo.toggleJobEditionAdvice = function(shownCategory, categoryName, features, settings) {
  this.toggleAdvice('.maxEditionAdvice', features, settings, {
    featureKey: 'edit_allowed_days',
    category: shownCategory,
    replaceItems: {
      category_name: categoryName,
    }
  });
};

/**
 * @name toggleJobInsertionPriceAdvice
 * @description
 *
 * Changes the advice visibility for categories that have `insertion_price` setting enabled.
 *
 * @param {String} shownCategory - Selected category id. (ie. "7020")
 * @param {Object} features - Reference to global variable called `category_features`
 * @param {Object} settings - Reference to global variable called `category_settings`
 * @param {Object} autoClose - Indicates that advice will be close automatically
 *
 */
Yapo.toggleJobInsertionPriceAdvice = function(shownCategory, features, settings, autoClose) {
  var adviceAutoClose = autoClose || false;

  this.toggleAdvice('.insertionPriceAdvice', features, settings, {
    autoClose: adviceAutoClose,
    category: shownCategory,
    featureKey: 'insertion_price',
    platform: 'site',
    formatPrice: true,
    user: {
      logged: true,
      isPro: true
    }
  });
};
