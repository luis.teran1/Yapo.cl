var Yapo = Yapo || {};

Yapo.hectares = (function() {

  var value;
  var target;
  var inmoType;
  var hectares;
  var classList;
  var sizeInput;
  var estateType;
  var findIptText;
  var surfaceInput;
  var hectaresPreview;
  var findSizeOptional;

  var maxLength;

  var square = 'm&sup2;';
  var optional = ' (opcional)';

  // Check if Terreno is chosen
  function checkIfLand(e) {
    if ($('#estate_type').length == 0) {
      return false;
    }

    inmoType = $('#estate_type option:selected').text();
    sizeInput = document.getElementById('size');

    if (inmoType === 'Terreno') {
      // Set input maxlength as 8
      updateMaxLength(8);
      updateHectaresPreview(e);      
    } else {
      return false;
    }
  }

  function updateMaxLength(limit) {
    maxLength = sizeInput.attributes['maxlength']; 

    if (maxLength) {
      sizeInput.attributes['maxlength'].value = limit;
    } else {
      sizeInput.setAttribute('maxlength', limit);
    }
  }

  // Update hectares
  function updateHectaresPreview(e) {
    hectaresPreview = document.getElementById('size_optional');
    target = e.target;

    classList = target.classList;
    findSizeOptional = classList.contains('size_optional');
    findIptText = classList.contains('ipt_text');

    /* Find correct input class */
    if (!findSizeOptional || !findIptText) {
      return false;
    }

    /* If amount is larger than 10.000, show and update hectares */
    if (target.value && parseInt(target.value) > 9999) {
      value = (target.value) / 10000;
      hectares = String(truncate(value, 1));

      if (hectares.indexOf('.') > -1 || hectares.indexOf(',') > -1) {
        if (hectares[hectares.length - 1] == 0) {
          hectares = hectares.substr(0, hectares.length - 2);
        }
      }

      hectaresPreview.innerHTML = square + '&nbsp;&nbsp;= ' + hectares + ' ha' + optional;
    } else {
      /* If amount is smaller than 10.000, hide hectares help*/
      hectaresPreview.innerHTML = square + optional;
    }
  }

  /* @TODO: Move truncate to utils.js, this is being 
   * used in msite/www/js/adview_src.js
   */
  function truncate(number, places) {
    var shift = Math.pow(10, places);
    return ((number * shift) | 0) / shift;
  }

  function init() {
    surfaceInput = document.getElementsByClassName('da-information ai-box')[0];

    if (surfaceInput) {
      surfaceInput.addEventListener('keyup', checkIfLand, true);
    }
  }

  return init;
})();

