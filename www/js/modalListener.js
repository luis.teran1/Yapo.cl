window.addEventListener('open-modal-favorites', function openModal () {
  var loginFavOptions = {
    title: 'Inicia sesi&oacute;n para agregar favoritos',
    subtitle: 'iniciar sesi&oacute;n o crear cuenta con tu perfil de Facebook',
    footer: ''
  };
  if (typeof Yapo !== 'undefined' &&  Yapo.modalLogin) {
    Yapo.modalLogin(loginFavOptions);
  } else {
    if (window.localStorage) {
      window.localStorage.setItem('loginFavOptions', JSON.stringify(loginFavOptions));
    }
    window.location.href = '/login';
  }
});
