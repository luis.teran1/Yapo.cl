/*!
 * radioSlider 
 * =================================
 * jQuery plugin to convert radio buttons to a slider
 *
 * (c) 2013 Ruben Torres, http://rubentd.com
 */

(function($) {

	var _radioSlider = 'radioSlider';

	// Plugin init
	$.fn[_radioSlider] = function(options, fire) {

  		$(this).addClass('radio-slider');
  		$(this).find('input[type=radio]').each(function(){
  			$("<ins id = 'helper" + $(this).val() + "' class = 'slider-helper'></ins>").insertAfter($(this));
  			$('#helper' + $(this).val()).css('top', '5px');
  			$('#helper' + $(this).val()).css('left', $().offset);
  		});

  		
	}

})(jQuery);