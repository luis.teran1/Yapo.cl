var currentReason=0;

$(document).ready( function(){

	$('html').removeClass('no-js');

	$('.deletion-reason').hide();
	$("a.bump-help").hide();

	//Give styles to the radio buttons
	$('input.icheck').iCheck({
		radioClass: 'iradio_green-check',
		increaseArea: '20%' // optional
	});

	$('input[type=radio]').on( "ifChecked", function(){
		var reasonNumber = $(this).attr("data-reason");
		ShowReason(reasonNumber);
	});

	$('input[type=radio]').each( function(){
		if($(this).is(':checked')){
			var reasonNumber = $(this).attr("data-reason");
			ShowReason(reasonNumber);
			$(".form-wrap").show();
		}
	});

	$('.bump-help').click( function(){
		$.pgwModal({
			target: '#bump-more-info',
			maxWidth: 850
		});
	});

});

function ShowReason(reasonNumber){

	//Update arrow position
	SetArrow(reasonNumber);

	SetReasonBox(reasonNumber);

	//Return current reason html to its original container
	HideCurrentReason();
	currentReason=reasonNumber;

	//Show deletion reason explanation on tooltip
	$('#reason-details').show();
	$('#reason-details-cont').html($('#reason' + reasonNumber).html());
	//Remove reason number class
	var reasonCount = $('ul.reasons li').length;
	//Change number reasonCount+ in for, need remove reason 6 class
	for (var i = 0; i < (reasonCount+2); i++) {
		$('#reason-details-cont').removeClass('reason-cont-'+i);
	};
	//Add this class to give custom style depending on the number of the reason
	$('#reason-details-cont').addClass('reason-cont-' + reasonNumber);
	$('#reason' + reasonNumber).html('');

	if(reasonNumber == 1){
		//hide delete form on option one, until the user selects in how much time it sold
		$(".form-wrap").hide();
		UndoSlider();
		$("#time-slider").radiosToSlider();
		$("#time-slider .slider-level").on('click', function(){
			$(".form-wrap").show();
		});
	}else{
		$(".form-wrap").show();
	}
	ShowHelp(reasonNumber);

	//rotation animation
	$('.gray-tooltip').addClass('animate');
	setTimeout(function(){
		$('.gray-tooltip').removeClass('animate');
	}, 300);

	$('.gray-tooltip').find('input').focus();
	$('.gray-tooltip').find('textarea').focus();

}

function HideCurrentReason(){
	//Return html to its container to keep just one copy of the cody in the DOM
	$('#reason' + currentReason).html($('#reason-details-cont').html());
}

function SetArrow(reasonNumber){
	var tooltipClass='is-top-' + reasonNumber;
	var reasonCount = $('ul.reasons li').length;
	//Change number reasonCount+ in for, need remove reason 6 class
	for (var i = 0; i < (reasonCount+2); i++) {
		$('.tooltip-tip-left').removeClass('is-top-'+i)
	};
	$('.tooltip-tip-left').addClass(tooltipClass);
}

function SetReasonBox (reasonNumber) {
	var reasonBox = $('.reasons-tooltip.delete-page.disabled');
	var reasonCount = $('ul.reasons li').length;
	//Change number reasonCount+ in for, need remove reason 6 class
	for (var i = 1; i < (reasonCount+2); i++) {
		reasonBox.removeClass('reason-'+i)
	};
	reasonBox.addClass('reason-'+reasonNumber);
}

function ShowHelp(reasonNumber){
	if(reasonNumber > 2){
		$("a.bump-help").show();
		var onclick = $("#more-info-onclick-" + reasonNumber).val();
		$("a.bump-help").attr("onclick", onclick);
	}else{
		$("a.bump-help").hide();
		$("a.bump-help").attr("onclick", "");
	}
}

function UndoSlider(){
	$('#time-slider').removeClass('radios-to-slider');
	$('#time-slider').removeClass('medium');
	$('#time-slider').find('ins').remove();
}

