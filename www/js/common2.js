/* globals uf_conversion_factor, noLeftZeroes */
/*
 * GLOBAL VARS
 */
var lastClickedAt = 0;


/** ****************************
*  Base64 encode / decode
*  http://www.webtoolkit.info/
**********************************/
var Base64 = {

  // private property
  _keyStr: 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=',

  // public, basic check for base64
  isBase64: function (input) {
// Most basic check
    if ((input.length % 4) != 0)
      return false;

    // Check for our most common characters.... (still basic)
    if (input.split(',') || input.split('.') || input.split('%'))
      return false;

    // TODO : More checks....
    return true;
  },

  // public method for encoding
  encode: function (input) {
    var output = '';
    var chr1, chr2, chr3, enc1, enc2, enc3, enc4;
    var i = 0;

    input = Base64._utf8_encode(input);

    while (i < input.length) {
      chr1 = input.charCodeAt(i++);
      chr2 = input.charCodeAt(i++);
      chr3 = input.charCodeAt(i++);

      enc1 = chr1 >> 2;
      enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
      enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
      enc4 = chr3 & 63;

      if (isNaN(chr2)) {
        enc3 = enc4 = 64;
      } else if (isNaN(chr3)) {
        enc4 = 64;
      }

      output = output +
      this._keyStr.charAt(enc1) + this._keyStr.charAt(enc2) +
      this._keyStr.charAt(enc3) + this._keyStr.charAt(enc4);
    }

    return output;
  },

  // public method for decoding
  decode: function (input) {
    var output = '';
    var chr1, chr2, chr3;
    var enc1, enc2, enc3, enc4;
    var i = 0;

    input = input.replace(/[^A-Za-z0-9\+\/\=]/g, '');

    while (i < input.length) {
      enc1 = this._keyStr.indexOf(input.charAt(i++));
      enc2 = this._keyStr.indexOf(input.charAt(i++));
      enc3 = this._keyStr.indexOf(input.charAt(i++));
      enc4 = this._keyStr.indexOf(input.charAt(i++));

      chr1 = (enc1 << 2) | (enc2 >> 4);
      chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
      chr3 = ((enc3 & 3) << 6) | enc4;

      output = output + String.fromCharCode(chr1);

      if (enc3 != 64) {
        output = output + String.fromCharCode(chr2);
      }
      if (enc4 != 64) {
        output = output + String.fromCharCode(chr3);
      }
    }

    output = Base64._utf8_decode(output);

    return output;
  },

  // private method for UTF-8 encoding
  _utf8_encode: function (string) {
    string = string.replace(/\r\n/g, '\n');
    var utftext = '';

    for (var n = 0; n < string.length; n++) {
      var c = string.charCodeAt(n);

      if (c < 128) {
        utftext += String.fromCharCode(c);
      }
      else if ((c > 127) && (c < 2048)) {
        utftext += String.fromCharCode((c >> 6) | 192);
        utftext += String.fromCharCode((c & 63) | 128);
      }
      else {
        utftext += String.fromCharCode((c >> 12) | 224);
        utftext += String.fromCharCode(((c >> 6) & 63) | 128);
        utftext += String.fromCharCode((c & 63) | 128);
      }
    }

    return utftext;
  },

  // private method for UTF-8 decoding
  _utf8_decode: function (utftext) {
    var string = '';
    var i = 0;
    var c = c1 = c2 = 0;

    while (i < utftext.length) {
      c = utftext.charCodeAt(i);

      if (c < 128) {
        string += String.fromCharCode(c);
        i++;
      }
      else if ((c > 191) && (c < 224)) {
        c2 = utftext.charCodeAt(i + 1);
        string += String.fromCharCode(((c & 31) << 6) | (c2 & 63));
        i += 2;
      }
      else {
        c2 = utftext.charCodeAt(i + 1);
        c3 = utftext.charCodeAt(i + 2);
        string += String.fromCharCode(((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
        i += 3;
      }
    }
  }
};


/* Browser detection */
function compare_version(ver1, ver2) {
  if (typeof(ver1) == 'string')
    ver1 = ver1.split('.');
  else if (typeof(ver1) == 'number')
    ver1 = [ver1];

  if (typeof(ver2) == 'string')
    ver2 = ver2.split('.');
  else if (typeof(ver2) == 'number')
    ver2 = [ver2];

  var i = 0;
  while (1) {
    if (!ver1[i]) {
      if (!ver2[i])
        return 0;
      else
        return 1;
    } else if (!ver2[i])
      return -1;

    if (parseInt(ver1[i]) > parseInt(ver2[i]))
      return -1;
    else if (parseInt(ver1[i]) < parseInt(ver2[i]))
      return 1;

    i++;
  }
}

var BrowserDetect = {
  init: function () {
    this.browser = this.searchString(this.dataBrowser) || 'An unknown browser';
    this.version = this.searchVersion(navigator.userAgent)
      || this.searchVersion(navigator.appVersion)
      || 'an unknown version';
    this.OS = this.searchString(this.dataOS) || 'an unknown OS';
  },
  searchString: function (data) {
    for (var i = 0; i < data.length; i++) {
      var dataString = data[i].string;
      var dataProp = data[i].prop;
      this.versionSearchString = data[i].versionSearch || data[i].identity;
      if (dataString) {
        if (dataString.indexOf(data[i].subString) != -1)
          return data[i].identity;
      }
      else if (dataProp)
        return data[i].identity;
    }
  },
  searchVersion: function (dataString) {
    var index = dataString.indexOf(this.versionSearchString);
    if (index == -1) return;
    var version = dataString.substring(index + this.versionSearchString.length + 1);


    if (version.indexOf(' ') > 0) {
      version = version.substring(0, version.indexOf(' '));
    }

    return version;
  },
  isValid: function (browsers) {
    var i = 0;
    var valid = false;

    for (i = 0; i < browsers.length; i++) {
      if (browsers[i].agent == this.browser) {
        if (compare_version(browsers[i].version, this.version) >= 0) {
          valid = true;
          break;
        }
      }
    }

    return valid;
  },
  dataBrowser: [
    { string: navigator.userAgent,
      subString: 'OmniWeb',
      versionSearch: 'OmniWeb/',
      identity: 'OmniWeb'
    },
    {
      string: navigator.vendor,
      subString: 'Apple',
      identity: 'Safari'
    },
    {
      prop: window.opera,
      identity: 'Opera'
    },
    {
      prop: window.clientInformation,
      identity: 'Chrome'
    },
    {
      string: navigator.vendor,
      subString: 'iCab',
      identity: 'iCab'
    },
    {
      string: navigator.vendor,
      subString: 'KDE',
      identity: 'Konqueror'
    },
    {
      string: navigator.userAgent,
      subString: 'Firefox',
      identity: 'Firefox'
    },
    {
      string: navigator.vendor,
      subString: 'Camino',
      identity: 'Camino'
    },
    {    // for newer Netscapes (6+)
      string: navigator.userAgent,
      subString: 'Netscape',
      identity: 'Netscape'
    },
    {
      string: navigator.userAgent,
      subString: 'MSIE',
      identity: 'Explorer',
      versionSearch: 'MSIE'
    },
    {
      string: navigator.userAgent,
      subString: 'Gecko',
      identity: 'Mozilla',
      versionSearch: 'rv'
    },
    {     // for older Netscapes (4-)
      string: navigator.userAgent,
      subString: 'Mozilla',
      identity: 'Netscape',
      versionSearch: 'Mozilla'
    }
  ],
  dataOS: [
    {
      string: navigator.platform,
      subString: 'Win',
      identity: 'Windows'
    },
    {
      string: navigator.platform,
      subString: 'Mac',
      identity: 'Mac'
    },
    {
      string: navigator.platform,
      subString: 'Linux',
      identity: 'Linux'
    }
  ]

};

BrowserDetect.init();


/* ----------------------------------------------------------------------- */
// file: pagequery_api.js
// javascript query string parsing utils
// pass location.search to the constructor: var page = new PageQuery(location.search)
// get values like: var myValue = page.getValue("param1") etc.
// djohnson@ibsys.com {{djohnson}}
// you may use this file as you wish but please keep this header with it thanks
/* ----------------------------------------------------------------------- */

function PageQuery(q) {
  if (q.length > 1) this.q = q.substring(1, q.length);
  else this.q = null;
  this.keyValuePairs = new Array();
  if (q) {
    for (var i = 0; i < this.q.split('&').length; i++) {
      this.keyValuePairs[i] = this.q.split('&')[i];
    }
  }
  this.getKeyValuePairs = function() { return this.keyValuePairs; };
  this.getValue = function(s) {
    for (var j = 0; j < this.keyValuePairs.length; j++) {
      if (this.keyValuePairs[j].split('=')[0] == s)
        return this.keyValuePairs[j].split('=')[1];
    }
    return -1;
  };
  this.getParameters = function() {
    var a = new Array(this.getLength());
    for (var j = 0; j < this.keyValuePairs.length; j++) {
      a[j] = this.keyValuePairs[j].split('=')[0];
    }
    return a;
  };
  this.getLength = function() { return this.keyValuePairs.length; };
}

function queryString(key) {
  var page = new PageQuery(window.location.search);
  return unescape(page.getValue(key));
}

/* --------------------------------------------------------------------------- */

/*
 * Array handling
 */
function isInArray(needle, arrayHaystack) {
  if (!arrayHaystack || arrayHaystack.length == 0)
    return false;

  for (var x in arrayHaystack) {
    if (arrayHaystack[x].split(':')[0] == needle)
      return true;
  }

  return false;
}
/*
 * Cookie handling
 */
function setCookie(name, value, expires, path, domain, secure) {
  var today = new Date();

  today.setTime(today.getTime());

  if (expires)
    expires = expires * 1000 * 60 * 60 * 24;

  var expires_date = new Date(today.getTime() + (expires));

  document.cookie = name + '=' + escape(value) +
    ((expires) ? ';expires=' + expires_date.toGMTString() : '') + // expires.toGMTString()
    ((path) ? ';path=' + path : '') +
    ((domain) ? ';domain=' + domain : '') +
    ((secure) ? ';secure' : '');
}

function deleteCookie(name, path, domain) {
  if (getCookie(name)) {
    document.cookie = name + '=' +
      ((path) ? ';path=' + path : '') +
      ((domain) ? ';domain=' + domain : '') +
      ';expires=Thu, 01-Jan-1970 00:00:01 GMT';
  }
}

/*
 * Check if the given feature has been selected
 */
function setFeatureVal(_feat, _id) {
  var Item = typeof(_id) == 'string' ? getElementById(_id) : _id;

  var cookie_str = getCookie('features');
  var feat_elements = Item.length;
  if (cookie_str && cookie_str.indexOf(_feat) >= 0) {
    var feature = parseInt(cookie_str.substr(cookie_str.indexOf(_feat + ':') + _feat.length + 1));
    for (var i = 0; i < feat_elements; i++) {
      if (feature == Item.options[i].value)
        Item.options[i].selected = true;
    }
  }
}


/*
 * Check if the given feature has been selected
 */
function setRadioVal() {
  var cookie_str = getCookie('features');
  if (cookie_str && cookie_str.indexOf('st') >= 0) {
    var feature = cookie_str.substr(cookie_str.indexOf('st:') + 3);
    for (var j = 0; j < document.f.st.length; j++) {
      if (document.f.st[j].value == feature) {
        document.f.st[j].checked = true;
      }
    }
  }
}


/*
 * Gets the ad type from caller argument
 */
function getAdTypeFromCaller() {
  var type;

  if (queryString('ca') < 0) {
    type = 's';
  } else {
    var caller = queryString('ca');
    var split_ca = caller.split('_');
    type = split_ca[split_ca.length - 1];
  }

  return type;
}

/*
 * Layer handling
 */
function showField() {
  var ShowItem = document.getElementById(showField.arguments[0]);
  if (ShowItem)
    ShowItem.style.display = showField.arguments[1];
  if (showField.arguments.length == 3) {
    ShowItem.innerHTML = showField.arguments[2];
  }
}

function showElement(id, showHide, retval) {
  var element = document.getElementById(id);
  if (element)
    element.style.display = showHide == true ? 'block' : 'none';
  if (retval != null)
    return retval;
}

function scrollToTop() {
  window.scrollTo(0, 0);
}

function scrollToBottom() {
  window.scrollTo(0, 10000);
}

function scrollToObject(offsetTrail) {
  var offsetLeft = 0;
  var offsetTop = 0;

  // Calculate the position
  while (offsetTrail) {
    offsetLeft += offsetTrail.offsetLeft;
    offsetTop += offsetTrail.offsetTop;
    offsetTrail = offsetTrail.offsetParent;
  }

  if (typeof(document.body.leftMargin) != 'undefined') {
    offsetLeft += document.body.leftMargin;
    offsetTop += document.body.topMargin;
  }

  // Scroll
  window.scrollTo(0, offsetTop);
}

var focused = false;
function scrollToError(elemId) {
  if (focused) return;
  var offsetTrail = document.getElementById(elemId);

  scrollToObject(offsetTrail);

  if (document.getElementById(elemId))
    document.getElementById(elemId).focus();
  focused = true;
}


function setFocus(_field) {
  var field = document.getElementById(_field);
  if (field && field.type != 'hidden') {
    document.getElementById(_field).focus();
  }
}

function setChecked(_Id, _check) {
  var Item = document.getElementById(_Id);
  if (Item == null) return;
  Item.checked = _check;
}

function setValue(_Id, _check) {
  var Item = typeof(_Id) == 'string' ? document.getElementById(_Id) : _Id;
  if (Item == null) return;

  Item.value = _check;
}

/*
 * Popup
 */
// window.name = "shl";
var newWin;
function popUp(page, name, details) {
  newWin = window.open(page, name, details);
  newWin.focus();
  return false;
}

function newsPopUp(page) {
  newWin = window.open(page, '_blank', ' width=800, height=' + Math.round(window.screen.availHeight * 0.8) + ', scrollbars=yes, screenX = 0, screenY = 0, top = 0, left = 0');
  newWin.focus();
  return false;
}


/*
 * Table row hiliting for IE
 */
function tableRowHilite() {
  if (document.getElementById('hl') == null) return;

  var table = document.getElementById('hl');
  var rows = table.getElementsByTagName('tr');

  for (var i = 0; i < rows.length; i++) {
    rows[i].onmouseover = function() {
      this.className += 'hilite';
    };

    rows[i].onmouseout = function() {
      this.className = this.className.replace('hilite', '');
    };
  }
}

/*
 * Disable and enable input fields in forms
 */
function enable_field(_name) {
  var Item = typeof(_name) == 'string' ? document.getElementById(_name) : _name;

  if (Item == null) return;

  if (Item.disabled)
    Item.disabled = false;
}

function disable_field(_name) {
  var Item = typeof(_name) == 'string' ? document.getElementById(_name) : _name;

  if (Item == null) return;

  if (!Item.disabled)
    Item.disabled = true;
}

function check_dc(_key) {
  var date = new Date;
  var time = date.getTime();

  if ((lastClickedAt + 2500) >= time) {
    document.getElementById(_key).value = 1;
  } else {
    document.getElementById(_key).value = 0;
  }

  lastClickedAt = time;
}

/*
 * Text area limit
 */
function maxlength(e, obj, max) {
  if (!e) e = window.event; // IE

  if (e.which) {
    var keycode = e.which; // Mozilla
    var ie = false;
  } else {
    var keycode = e.keyCode; // IE
    var ie = true;
  }

  x = obj.value.length;

  if (x > max) {
    obj.value = obj.value.substr(0, max);
    x = max;
  }

  if (keycode == 0 && ie) { // PASTE ONLY FOR IE
    var select_range = document.selection.createRange();
    var max_insert = max - x + select_range.text.length;
    var data = window.clipboardData.getData('Text').substr(0, max_insert);
    select_range.text = data;
  } else if (x == max && (keycode != 8 && keycode != 46)) {
    return false;
  }

  return true;
}

/*
 * Positioning of elements
 */
function findPosX(obj, end) {
  var curleft = 0;
  var width = obj.clientWidth;

  if (obj.offsetParent) {
    while (obj.offsetParent) {
      curleft += obj.offsetLeft;
      obj = obj.offsetParent;
    }
  } else if (obj.x)
    curleft += obj.x;

  return curleft + (end ? width : 0);
}

function findPosY(obj, end) {
  var curtop = 0;
  var height = obj.clientHeight;

  if (obj.offsetParent) {
    while (obj.offsetParent) {
      curtop += obj.offsetTop;
      obj = obj.offsetParent;
    }
  } else if (obj.y)
    curtop += obj.y;

  return curtop + (end ? height : 0);
}

/*
 * Progress bar
 */
function ProgressBar(_container) {
  this.progress = [];
  this.container = _container || 'progressbar_container';
  this.completed = false;

  /* Don't show estimate until progress reach (x) procent */
  this.ESTIMATE_MIN_PROGRESS = 10;
  /* Speed calculation include latest (x) procent */
  this.SPEED_CALC_LATEST = 30;

  /* Clear container */
  this.clear();

  /* Init the container */

  var container = document.getElementById(this.container);
  if (!container)
    return;

  /* Create progress table */
  var progress_bar = document.createElement('div');
  progress_bar.className = 'progress_bar';

  var progress_cell = document.createElement('div');
  progress_cell.className = 'progress_blue';
  progress_cell.style.width = '0px';

  var debug = document.createElement('div');
  debug.className = 'progress_debug';
  debug.id = 'progress_debug_id';

  progress_bar.appendChild(progress_cell);
  container.appendChild(progress_bar);
  container.appendChild(debug);
  container.appendChild(document.createElement('br'));
}

ProgressBar.prototype.clear = function () {
  this.progress = [];

  var container = document.getElementById(this.container);
  if (!container) return;

  while (container.childNodes.length > 0)
    container.removeChild(container.childNodes[0]);
};

ProgressBar.prototype.update = function (progress, total) {
  if (this.completed) return;

  var id = this.progress.length;
  var time = new Date();
  this.progress[id] = { progress: progress, total: total, time: time.getTime() };
  this.completed = progress == total;
};

ProgressBar.prototype.current = function () {
  var id = this.progress.length;

  if (id == 0) return;

  return this.progress[id - 1];
};

ProgressBar.prototype.procent = function (_id) {
  var progress = this.progress[_id] || this.current();

  if (progress && progress.progress)
    return Math.round(progress.progress / progress.total * 100);

  return 0;
};

ProgressBar.prototype.speed = function () {
  if (this.progress.length == 0) return;

  var start_at = this.progress.length - Math.floor(this.progress.length * this.SPEED_CALC_LATEST / 100) - 1;

  if (start_at < 0)
    start_at = 0;

  var first = this.progress[start_at];
  var current = this.current();

  var current_progress = current.progress - first.progress;
  var time = (current.time - first.time);

  return (current_progress / time);
};

ProgressBar.prototype.estimate = function () {
  if (this.procent() < this.ESTIMATE_MIN_PROGRESS) return;

  var speed = this.speed();
  var progress = this.current();

  var remaining_progress = progress.total - progress.progress;
  var remaining_time = Math.round(remaining_progress / speed);

  return remaining_time;
};

ProgressBar.prototype.draw = function () {
  var container = document.getElementById(this.container);
  if (!container) return;
  container.style.display = 'block';

  var estimate = this.estimate() / 1000;
  var speed = this.speed();
  var procent = this.procent();

  if (container.childNodes.length) {
    var progress_bar = container.getElementsByTagName('div')[0];
    var progress_cell = container.getElementsByTagName('div')[1];
    progress_cell.style.width = Math.round((progress_bar.offsetWidth - 2) * procent / 100) + 'px';

    var debug = container.getElementsByTagName('div')[2];
    var minutes_left = Math.floor(estimate / 60);
    var seconds_left = Math.round(estimate - minutes_left * 60);
    var time_left = '';

    if (minutes_left + seconds_left > 0)
      time_left = js_info['TIME_LEFT'] + ': ';
    if (minutes_left > 0)
      time_left += minutes_left + ' min ';
    if (seconds_left > 0)
      time_left += seconds_left + ' s';

    debug.innerHTML = procent + '%&nbsp;&nbsp;&nbsp;' + time_left;
  }
};

ProgressBar.prototype.update_draw = function(progress, total) {
  if (this.completed) return;

  this.update(progress, total);
  this.draw();
};

/*
 * Progress bar
 */
function progressBar(text) {
  document.write('<div id="loading" class="progressBar">' + text + '<span id="loading_dots"></span></div>');
}

function startProgressBar(pos) {
  var loading = document.getElementById('loading');
  var dots = '';

  pos %= 4;
  for (var i = 0; i < pos; i++)
    dots += '.';

  document.getElementById('loading_dots').innerHTML = dots;

  pos++;
  loading.timer = setTimeout('startProgressBar(' + pos + ')', 500);
}

/*
 * Position progress bar
 */
function showProgressBar(obj, posY, posX) {
  var loading = document.getElementById('loading');

  if (!posY)
    posY = 0;
  if (!posX)
    posX = 20;
  startProgressBar(1);

  loading.style.top = '' + (findPosY(obj, true) + posY) + 'px';
  loading.style.left = '' + (findPosX(obj, true) + posX) + 'px';
  loading.style.display = 'inline';
}

function hideProgressBar() {
  var loading = document.getElementById('loading');

  clearTimeout(loading.timer);
  loading.style.display = 'none';
}

function select_all_weeks(_name, _form, _select) {
  for (var i = 1; i < 53; i++) {
    var week = eval('document.' + _form + '.' + _name + i);

    week.checked = _select;
  }
}

function waitForNextImage(next_image, ad_id) {
  var ad_id = ad_id ? ad_id : '';
  var image = document.getElementById('display_image' + ad_id).firstChild;

  if (next_image.width > 0) {
    image.width = next_image.width;
    image.height = next_image.height;
  } else {
    setTimeout(function () { waitForNextImage(next_image, ad_id); }, 100);
  }
}

function resizeImage(image, path, next_image, admin) {
  if (!next_image) {
    next_image = new Image;
    next_image.src = path;
  }

  if (next_image.width == 0) {
    next_image.onload = setTimeout(function () { resizeImage(image, path, next_image, admin); }, 0);
    return;
  }

  image.src = next_image.src;

  if (admin && next_image.width > 400) {
    var factor = (next_image.width - 400) / next_image.width;
    image.height = next_image.height * (1 - factor);
    image.width = 400;
  } else {
    image.width = next_image.width;
    image.height = next_image.height;
  }
}

function showLargeImage(strDisplayPath, ad_id, admin) {
  var ad_id = ad_id ? ad_id : '';
  var admin = admin ? admin : false;
  var image = document.getElementById('display_image' + ad_id).firstChild;

  if (admin) {
    resizeImage(image, strDisplayPath, null, admin);
  } else {
    var ctrl = $('#display_image' + ad_id + ' img:first-child');
    ctrl.attr('src', '/img/wait.gif');
    ctrl.fadeOut(200, function() {
      ctrl.attr('src', strDisplayPath);
    }).fadeIn(200);
  }
}

function next_image (clicked) {
  if (!images[counter]) {
    counter = 0;
    reset_scroll();
    return false;
  }

  /* Preload next image */
  var thumb = document.getElementById('thumb' + counter);
  var image = new Image;
  image.src = image_url + images[counter];

  /*
   * We have just steped out of the scroll tab
   * so we have to move the scroll to the right
   */
  if ((counter % scroll_images_visible) == 0 && clicked == 1 && counter != 0) {
    scroll_right(images.length, 1);
  }

  showLargeImage(image_url + images[counter]);
  thumbnailBorder(thumb, images.length);

  set_alt_title('main_pict');

  counter++;
}

function reset_scroll () {
  var image_num = images.length;

  for (i = 0; i < image_num; i++) {
    if (i < scroll_images_visible) {
      document.getElementById('thumb' + i).className = ' ';
    } else {
      document.getElementById('thumb' + i).className = 'hidden ';
    }
  }

  showLargeImage(image_url + images[0]);
  thumbnailBorder(document.getElementById('thumb0'), image_num);

  document.getElementById('arrow_left').style.display = 'none';
  document.getElementById('arrow_right').style.display = 'inline';
  counter = 1;
}

function set_alt_title(call_div) {
  var main_image = document.getElementById('main_image');
  var adder = 0;

  if (call_div == 'thumb') {
    adder = 1;
  }

  main_image.alt = js_info['CLICK_FOR_NEXT_IMAGE'];
  main_image.title = js_info['CLICK_FOR_NEXT_IMAGE'];
}

function thumbnailBorder(thumb, image_num, ad_id) {
  var ad_id = ad_id ? ad_id : '';

  if (!thumb)
    return;

  for (i = 0; i < image_num; i++) {
    var thumb_obj = document.getElementById('thumb' + i + ad_id);
    if (thumb_obj.className.match(/selected/)) {
      if (thumb_obj.className.match(/hidden/)) {
        thumb_obj.className = 'hidden';
      } else {
        thumb_obj.className = '';
      }
    }

    if (thumb.id == thumb_obj.id) {
      thumb_obj.className = 'selected';
    }
  }
}

function scroll_right(image_num, inner_call) {
  var k = image_num + 1;

  for (i = 0; i < image_num; i++) {
    var thumb_obj = document.getElementById('thumb' + i);
    var classn = thumb_obj.className;
    if (!classn.match(/hidden/)) {
      k = i;
      thumb_obj.className = 'hidden';
    } else if (i > k && i < k + (scroll_images_visible + 1)) {
      thumb_obj.className = ' ';
    }
  }

  if (k >= image_num - (scroll_images_visible + 1)) {
    document.getElementById('arrow_right').style.display = 'none';
    document.getElementById('arrow_left').style.display = 'inline';
  } else {
    document.getElementById('arrow_right').style.display = 'inline';
    document.getElementById('arrow_left').style.display = 'inline';
  }

  /*
   * Now we have to show the first image of the visible block
   * and not forget to fix the counter for the next image step
   */
  if (inner_call != 1) {
    var large_index = k + 1;
    counter = large_index + 1;

    showLargeImage(image_url + images[large_index]);
    document.getElementById('thumb' + large_index).className = 'selected';
  }
}

function scroll_left(image_num) {
  var k = 0;
  var last = 0;
  for (i = image_num - 1; i >= 0; i--) {
    var thumb_obj = document.getElementById('thumb' + i);
    var classn = thumb_obj.className;
    if (!classn.match(/hidden/)) {
      k = i;
      thumb_obj.className = 'hidden';
    } else if (i < k && i > k - (scroll_images_visible + 1)) {
      thumb_obj.className = ' ';
      last = i;
    }
  }
  if (k < 7) {
    document.getElementById('arrow_left').style.display = 'none';
    document.getElementById('arrow_right').style.display = 'inline';
  } else {
    document.getElementById('arrow_left').style.display = 'inline';
    document.getElementById('arrow_right').style.display = 'inline';
  }

  /*
   * Now we have to show the first image of the visible block
   * and not forget to fix the counter for the next image step
   */
  counter = last + 1;

  showLargeImage(image_url + images[last]);
  document.getElementById('thumb' + last).className = ' selected';
}

/* Hide image and display image-add input */
function delete_image(element_show, element_hide, hidden) {
  var obj1 = document.getElementById(element_show);
  var obj2 = document.getElementById(element_hide);
  showField(obj1.id, 'block');

  obj2.innerHTML = "<input type='hidden' name='" + hidden + "' value='1'>";

  return false;
}

function getElementsByClassName(oElm, strTagName, strClassName) {
  var arrElements = (strTagName == '*' && oElm.all) ? oElm.all : oElm.getElementsByTagName(strTagName);
  var arrReturnElements = new Array();
  strClassName = strClassName.replace(/\-/g, '\\-');
  var oRegExp = new RegExp('(^|\\s)' + strClassName + '(\\s|$)');
  var oElement;
  for (var i = 0; i < arrElements.length; i++) {
    oElement = arrElements[i];
    if (oRegExp.test(oElement.className)) {
      arrReturnElements[arrReturnElements.length] = oElement;
    }
  }
  return (arrReturnElements);
}

function show_hidden_elements() {
  var elements = getElementsByClassName(document, '*', 'hide');
  for (var i = 0; i < elements.length; i++) {
    elements[i].className = elements[i].className.replace(/hide/, '');
  }
}

function show_tabbed_data() {
  document.getElementById('tabbed_data').style.display = 'block';
  document.getElementById('show_tabbed_text').style.display = 'none';
}

function hide_tabbed_data() {
  document.getElementById('tabbed_data').style.display = 'none';
  document.getElementById('show_tabbed_text').style.display = 'block';
}

function get_settings(setting_name, keylookup_func, settings_root, extra) {
  if (!settings_root)
    settings_root = settings;

  var res;

  for (var i in settings_root[setting_name]) {
    var setting = settings_root[setting_name][i];
    var val;

    val = null;
    if (settings_root[setting_name][i]['keys']) {
      for (var j in settings_root[setting_name][i]['keys']) {
        var key = settings_root[setting_name][i]['keys'][j];
        var key_val = keylookup_func(key, extra);

        if (setting[key_val]) {
          setting = setting[key_val];
        } else if (setting['*']) {
          setting = setting['*'];
        } else {
          break;
        }
      }

      if (setting['value'])
        val = setting['value'];
    } else if (settings_root[setting_name][i]['default']) {
      val = settings_root[setting_name][i]['default'];
    }
    if (val) {
      if (res)
        res += ',' + val;
      else
        res = val;
      if (!settings_root[setting_name][i]['continue'])
        break;
    }
  }

  return res;
}

/* TODO support \ */
function split_setting(val) {
  if (!val)
    return {};

  var arr = val.split(',');
  var res = {};

  for (i = 0; i < arr.length; i++) {
    var kv = arr[i].split(':', 2);

    if (kv && kv.length > 1)
      res[kv[0]] = kv[1];
    else {
      res[arr[i]] = 1;
    }
  }

  return res;
}

function mergeElementValues(arr, htmlCollection) {
  for (var ii = 0; ii < htmlCollection.length; ii++) {
    var elem = htmlCollection[ii];

    if (!elem.getAttribute('name'))
      continue;

    var key = null;
    var options = null;
    if (elem.className) {
      var element_group = elem.className.replace(/.*element_group([0-9]+).*/, '$1');
      if (element_group != '')
        element_group += '.';
      key = element_group + elem.getAttribute('name');

      if (elem.className.match(/(^| )cat_data_select($| )/)) {
        var a = [];
        for (var i = 0; i < elem.options.length; i++) {
          a[i] = elem.options[i];
        }
        options = a;
      }
    } else
      key = elem.getAttribute('name');
    if (key.match(/\[\]$/) && elem.value)
      key += elem.value;
    if (((elem.type == 'radio' || elem.type == 'checkbox') && !elem.checked)) {
      if (typeof arr[key] != 'undefined')
        delete arr[key];
    } else if ((elem.value || options) && !elem.disabled) {
      arr[key] = elem.value;
      if (options)
        arr[key + '.options'] = options;
    } else {
      arr[key] = '';
      if (typeof arr[key + '.options'] != 'undefined')
        delete arr[key + '.options'];
    }
  }

  return arr;
}

function setElementValues(arr, htmlCollection) {
  for (var ii = 0; ii < htmlCollection.length; ii++) {
    var elem = htmlCollection[ii];

    var options = null;
    if (elem.className) {
      var element_group = elem.className.replace(/.*element_group([0-9]+).*/, '$1');
      if (element_group != '')
        element_group += '.';
      key = element_group + elem.getAttribute('name');
      if (elem.className.match(/(^| )cat_data_select($| )/)) {
        options = arr[key + '.options'];
      }
    } else
      key = elem.getAttribute('name');
    if (!key) {
      continue;
    }

    if (key.match(/\[\]$/) && elem.value)
      key += elem.value;
    var value = arr[key];

    if (elem.type == 'radio' || elem.type == 'checkbox') {
      if (value == elem.value)
        elem.checked = true;
    } else if (value || options) {
      if (options) {
        elem.options.length = 0;
        for (var i = 0; i < options.length; i++) {
          elem.options[i] = options[i];
        }
        elem.disabled = false;
      }
      if (elem.options && elem.options.length) {
        for (var i = 0; i < elem.options.length; i++) {
          if (elem.options[i].value == value) {
            elem.value = value;
            break;
          }
        }
      } else {
        elem.value = value;
      }
    }
  }
}

/*
 * AJAX
 */
function ajax_request(dest, post, callback, params, evaluate, method) {
  var xmlhttp = false;

  if (method == null) {
    method = 'POST';
  }

  if (typeof evaluate == 'undefined')
    evaluate = true;

  try {
    xmlhttp = window.XMLHttpRequest ? new XMLHttpRequest() : new ActiveXObject('Microsoft.XMLHTTP');
  } catch (e) {
    // browser doesn't support ajax. handle however you want
    // XXX ? callback(false, xmlhttp, params);
  }

  if (xmlhttp !== false) {
    xmlhttp.onreadystatechange = function () { ajax_callback(callback, params, xmlhttp, evaluate); };
    xmlhttp.open(method, dest, true);
    xmlhttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    xmlhttp.send(post);
  }
}

function ajax_callback(callback, params, xmlhttp, evaluate) {
  if (xmlhttp.readyState == 4) {
    if (xmlhttp.status == 200 && xmlhttp.responseText.indexOf('<!DOCTYPE') < 0) {
      if (evaluate)
        callback(eval('(' + xmlhttp.responseText + ')'), xmlhttp, params);
      else
        callback(xmlhttp.responseText, xmlhttp, params);
    } else {
      callback(false, xmlhttp, params);
    }
  }
}

function setabusereport(url, list_id, abuse_type, lang) {
  var xmlhttp = false;
  var reporter_id = getCookie('uid');

  try {
    xmlhttp = window.XMLHttpRequest ? new XMLHttpRequest() : new ActiveXObject('Microsoft.XMLHTTP');
  } catch (e) {
    return false;
  }
  xmlhttp.onreadystatechange = function () {
    if (xmlhttp.readyState == 4) {
      var layer = document.getElementById('abuse_wrapper');
      if (xmlhttp.status == 200 && xmlhttp.responseText.indexOf('<!DOCTYPE') < 0) {
        var newuid = document.getElementById('new_uid');
        if (newuid && reporter_id != newuid.value) {
          var expiration_time = 10 * 365 * 24 * 60 * 60;
          setCookie('uid', newuid.value, expiration_time, '/');
        }
        try {
          var xtcustom = document.getElementById('xtcustom_' + abuse_type).value;
          var xtclick = document.getElementById('xtclick_' + abuse_type).value;
          xt_med('C', '', xtclick, 'N');
          xt_med('F', '', 'foo&stc=' + xtcustom);
        } catch (e) { }
        layer.innerHTML = xmlhttp.responseText;
        // Disable abuse link on tooltip close
        if ($('#darktooltip-abusebutton_link')) {
          $('#darktooltip-abusebutton_link').data('hide-link', 'true');
        }
      } else {
        /* error */
        var xtcustom = document.getElementById('xtcustom_error').value;
        var xtcustom_var = get_page_custom_variables();
        xt_med('F', '', 'foo&stc=' + xtcustom + '&' + xtcustom_var);
        layer.innerHTML = '<div><span class="error">Error</span></div>';
      }
    }
  };

  /* we send data via GET, but POST would be better (mod_blocket needs to be modified to handle POST vars) */
  var address = url + '?action=insert&id=' + list_id + '&abuse_type=' + abuse_type + '&lang=' + lang;
  var reporter_id = getCookie('uid');

  if (reporter_id != null) {
    address = address + '&uid=' + reporter_id;
  }

  xmlhttp.open('GET', address, true);
  xmlhttp.setRequestHeader('If-Modified-Since', 'Thu, 1 Jan 1970 00:00:00 GMT');
  xmlhttp.setRequestHeader('Cache-Control', 'no-cache, no-store');
  xmlhttp.setRequestHeader('Pragma', 'no-cache');
  xmlhttp.send(null);
}

function getValueFromSq(s) {
  var sq_value = getCookie('sq');
  if (sq_value) {
    var pairs = sq_value.split('&');
    for (var j = 0; j < pairs.length; j++) {
      if (pairs[j].split('=')[0] == s)
        return pairs[j].split('=')[1];
    }
  }
  return -1;
}

function getRegionFromSq() {
  var ca_value = getValueFromSq('ca');
  if (ca_value != -1) {
    return ca_value.split('_')[0];
  }
  return null;
}

function scroll_down(image_num, inner_call) {
  var k = image_num + 1;

  for (i = 0; i < image_num; i++) {
    var thumb_obj = document.getElementById('thumb' + i);
    var classn = thumb_obj.className;
    if (!classn.match(/hidden/)) {
      k = i;
      thumb_obj.className = 'hidden';
    } else if (i > k && i < k + (scroll_images_visible + 1)) {
      thumb_obj.className = ' ';
    }
  }

  if (k >= image_num - (scroll_images_visible + 1)) {
    document.getElementById('arrow_downB').style.display = 'none';
    document.getElementById('arrow_upB').style.display = 'inline';
  } else {
    document.getElementById('arrow_downB').style.display = 'inline';
    document.getElementById('arrow_upB').style.display = 'inline';
  }

  /*
   * Now we have to show the first image of the visible block
   * and not forget to fix the counter for the next image step
   */
  if (inner_call != 1) {
    var large_index = k + 1;
    counter = large_index + 1;

    showLargeImage(image_url + images[large_index]);
    document.getElementById('thumb' + large_index).className = 'selected';
  }
}

function scroll_up(image_num) {
  var k = 0;
  var last = 0;
  for (i = image_num - 1; i >= 0; i--) {
    var thumb_obj = document.getElementById('thumb' + i);
    var classn = thumb_obj.className;
    if (!classn.match(/hidden/)) {
      k = i;
      thumb_obj.className = 'hidden';
    } else if (i < k && i > k - (scroll_images_visible + 1)) {
      thumb_obj.className = ' ';
      last = i;
    }
  }
  if (k < 7) {
    document.getElementById('arrow_upB').style.display = 'none';
    document.getElementById('arrow_downB').style.display = 'inline';
  } else {
    document.getElementById('arrow_upB').style.display = 'inline';
    document.getElementById('arrow_downB').style.display = 'inline';
  }

  /*
   * Now we have to show the first image of the visible block
   * and not forget to fix the counter for the next image step
   */
  counter = last + 1;

  showLargeImage(image_url + images[last]);
  document.getElementById('thumb' + last).className = ' selected';
}

function next_imageB (clicked) {
  if (!images[counter]) {
    counter = 1;
    reset_scrollB();
    return false;
  }

  /* Preload next image */
  var thumb = document.getElementById('thumb' + counter);
  var image = new Image;
  image.src = image_url + images[counter];

  /*
   * We have just steped out of the scroll tab
   * so we have to move the scroll to the right
   */
  if ((counter % scroll_images_visible) == 0 && clicked == 1 && counter != 0) {
    scroll_down(images.length, 1);
  }

  showLargeImage(image_url + images[counter]);
  thumbnailBorder(thumb, images.length);

  set_alt_title('main_pict');

  counter++;
}

function reset_scrollB () {
  var image_num = images.length;

  for (i = 0; i < image_num; i++) {
    if (i < scroll_images_visible) {
      document.getElementById('thumb' + i).className = ' ';
    } else {
      document.getElementById('thumb' + i).className = 'hidden ';
    }
  }
  showLargeImage(image_url + images[0]);
  thumbnailBorder(document.getElementById('thumb0'), image_num);

  var arrow_upB = document.getElementById('arrow_upB');
  var arrow_downB = document.getElementById('arrow_downB');
  if (arrow_upB && arrow_downB) {
    arrow_upB.style.display = 'none';
    arrow_downB.style.display = 'inline';
  }
  counter = 1;
}

function format_phone() {
  var phone = document.getElementById('phone').value;
  var phone_rep = phone.replace(/[^0-9]/g, '');
  if (phone_rep.length > 11) {
    phone_rep = phone_rep.substring(0, 11);
  }
  if (phone_rep != phone)
    document.getElementById('phone').value = phone_rep;
}

function addDot(a) {
  var b = '';
  for (i = 0; i < a.length; i ++) {
    if (i != 0 && i % 3 == a.length % 3) {
      b = b + '.' + a.charAt(i);
    } else {
      b = b + a.charAt(i);
    }
  }
  return b;
}

function real_format_price(num, noLeftZeros) {
  noLeftZeroes = noLeftZeros || false;
  var str = '';
  var nums;

  if (!isNaN(num)) {
    if (noLeftZeroes)
      num = num.replace(/^[0,\.]+/g, '');

    str = num + '';
    nums = str.split('.');

    if (nums.length == 2) {
      if (nums[1].length <= 2) {
        str = str.replace('\.', ',');
      }
    }

    nums = str.split(',');
    if (nums[0].search('\\.') == -1) {
      nums[0] = addDot(nums[0]);
      str = nums[0];
    }

    if (nums.length == 2) {
      str = nums[0] + ',' + nums[1];
    }

    return str;
  }
}

function getElementsByRegexId(regexpParam, tagParam) {
  tagParam = (tagParam === undefined) ? '*' : tagParam;

  var elementsTable = new Array();
  for (var i = 0; i < document.getElementsByTagName(tagParam).length; i++) {
    if (document.getElementsByTagName(tagParam)[i].id && document.getElementsByTagName(tagParam)[i].id.match(regexpParam)) {
      elementsTable.push(document.getElementsByTagName(tagParam)[i]);
    }
  }

  return elementsTable;
}

function scrollToInicialError() {
  var elementsError = getElementsByRegexId('^err_.*$');
  var erroresString = '';

  for (var i = 0; i < elementsError.length; i++) {
    if (elementsError[i].style.display != 'none') {
      var elementId = elementsError[i].getAttribute('id');
      var errorName = elementId.substr(4, elementId.length);
      if (errorName == 'company_ad') errorName = 'c_ad';
      if (errorName == 'job_category') errorName = 'jc_1';
      if (errorName == 'service_type') errorName = 'sct_1';
      if (errorName == 'category') errorName = 'category_group';
      var elementFocus = document.getElementById(errorName);
      if (elementFocus == null) {
        errorName = elementId;
      }
      scrollToError(errorName);
      return;
    }
  }
}

