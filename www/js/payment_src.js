$(document).ready( function(){

	$('.more-info').click( function(){
		$.pgwModal({
			target: '#mas-info',
			maxWidth: 800
		});
	});

	$('.change-email').click( function(e){
		$caret = $(this).find('span');
		
		if($caret.hasClass('right')){
			$caret.removeClass('right');
			$caret.addClass('down');
		}else if($caret.hasClass('down')){
			$caret.removeClass('down');
			$caret.addClass('right');
		}
		
	});

});