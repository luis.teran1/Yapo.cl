/* Global:  selected_brand, selected_model*/

(function() {
  let handleObservers = [];

  function loadInputParams(values) {
    const priceEl = document.getElementById('price');

    priceEl.value = values.price;
    priceEl.focus();
  }
  
  function loadParams(values) {
    const catSelect = document.getElementById('category_group');
    let brandLoaded = 0;
    let modelLoaded = 0;
    let versionLoaded = 0;
    let yearLoaded = 0;

    catSelect.value = "2020";
    fireEvent(catSelect, 'change');
    
    checkChangeElement('category_contents', function checkBrand() {
      loadInputParams(values);

      if (brandLoaded === 0) {
        setCarParamSelect('brand', values.brand);
        brandLoaded = 1;
      }
      
      checkChangeElement('model', function checkModel() {
        if (modelLoaded === 0 && typeof selected_brand.models != 'undefined') {
          setCarParamSelect('model', values.model);
          modelLoaded = 1;
        }
      }, handleObservers);

      checkChangeElement('version', function checkVersion() {
        if (versionLoaded === 0 && typeof selected_model != 'undefined' && selected_model !== '') {
          setCarParamSelect('version', values.version);
          versionLoaded = 1;
          
          if (yearLoaded === 0) {
            setCarParamSelect('regdate', values.year);
            yearLoaded = 1;

            removeLocalCarValues('car_info', handleObservers);
          }
        }
      }, handleObservers);
    }, handleObservers);
  }

  function init() {
    const carValues = getCarValues();
   
    if (carValues) {
      loadParams(handleCarValues(carValues));
    }
  }

  docReady(init);
})();
