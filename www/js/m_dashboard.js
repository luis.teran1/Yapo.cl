/* globals adjustThumbs, CartHandler, scrollAction, xt_click */

jQuery.events = function(expr) {
  var evo;
  var rez = [];

  jQuery(expr).each(function() {
    evo = jQuery._data(this, 'events'); // eslint-disable-line no-underscore-dangle
    if (evo) {
      rez.push({ element: this, events: evo });
    }
  });

  return rez.length > 0 ? rez : null;
};

$(document).ready(function() {
  var dpi = window.devicePixelRatio;
  var thumbPath = $('#thumbnail-path').val();
  var thumbPath2x = $('#thumbnail-path-2x').val();
  var loadingMsg = $('#loading-msg').val();
  var ProductSelects = $('.product-list-wrap select');

  // Don't do this, remove this from the html file, not through js
  $('.ios_app .da-options #da-highlight').remove();

  showOpMsg();

  // New Select products dashboard
  $('.product-list-wrap select:disabled').parent().addClass('disable');

  $('body').on('click', '.product-list-wrap ul li .icon-close-circled', function() {
    closeButtonSelectClick($(this));
    hideMsgPromo($(this));
  });

  $('body').on('change', '.product-list-wrap select', function() {
    checkSelected();
  });

  // End new select

  $('.scroll').jscroll({
    padding: 150,
    loadingHtml: '<div class="loading-das">' + loadingMsg + '</div>',
    callback: function() {
      adjustThumbs(dpi, thumbPath, thumbPath2x);
      activateProductsOnReady();
      dotsSubjects();
    }
  });

  $('#clear-search').click(function() {
    $('#search-ads').val('');
    $('#search-ads').focus();
    toggleCleanSearch();
  });

  $('#search-ads').keyup(function() {
    toggleCleanSearch();
  });

  $('#search-ads').focus(function() {
    $('.dashboard-options-wrap').removeClass('fixed-options');
    $('.msite-dashboard').removeClass('bar-is-fixed');
  });

  $('#search-ads').blur(function() {
    scrollAction();
  });

  $('.single-da.inactive').click(function() {
    var $this = $(this);
    var $msg = $this.find('.inactive-msg');
    $msg.addClass('expand');

    setTimeout(function() {
      $msg.removeClass('expand');
    }, 3000);
  });

  toggleCleanSearch();
  adjustThumbs(dpi, thumbPath, thumbPath2x);

  activateProductsOnReady();
  dotsSubjects();

  $('#activate-da.active').click(function (e) {
    var $element = $(e.currentTarget);

    if ($element.hasClass('active')) {
      activateAd($element);
    }
  });

  $('body').on('click', 'button.combo-openSlide', function() {
    var selectCombo = $(this).parent().find('select');

    openPageSlideCombo(selectCombo);
  });

  CartHandler.addListener(updateStickyBarValues);
  CartHandler.executeListeners();

  setTimeout(function(){
    checkSelected();
  }, 1000);
});

function checkSelected() {
  $('.product-list-wrap select').each(function(index, value) {
    var ICON_CLOSE = '.icon-close-circled';
    var ICON_ARROW_DOWN = '.icon-arrow-down-bold';
    var ICON_HIDE = 'icon-hide';

    var $this = $(this);
    var selectVal = $(value).val();
    var valParent = $(value).parent();

    if (selectVal !== '0') {
      $this.addClass(ICON_HIDE);
      valParent.find(ICON_ARROW_DOWN).hide();
      valParent.find(ICON_CLOSE).show();
    } else {
      $this.removeClass(ICON_HIDE);
      valParent.find(ICON_ARROW_DOWN).show();
      valParent.find(ICON_CLOSE).hide();
    }
  });
}

function toggleCleanSearch() {
  if ($('#search-ads').val()) {
    $('#clear-search').show();
  } else {
    $('#clear-search').hide();
  }
}

function activateEvents(jqnode) {
  var highlightButton = jqnode.find('#da-highlight');
  var eventsList = jQuery.events(highlightButton);

  jqnode.find('.bump-select').on('change', function() {
    var $this = $(this);
    var adId = $this.attr('data-da_id');
    var listId = $this.attr('data-list_id');
    var productUpselling = $this.attr('data-product_upselling');
    var tac = $this.attr('data-tac').toString();
    var value = $this.val();
    var link = $this.find('.bump-selectCustomOption').data('link');

    var data = {
      list_id: listId,
      tac: tac
    };

    if (value === 'bump') {
      $this.attr('data-selected', 'bump');
      CartHandler.select(CartHandler.BUMP, data);
    } else if (value === 'daily_bump') {
      $this.attr('data-selected', 'daily-bump');
      CartHandler.select(CartHandler.DAILY_BUMP, data);
    } else if (value === 'weekly_bump') {
      $this.attr('data-selected', 'weekly_bump');
      CartHandler.select(CartHandler.WEEKLY_BUMP, data);
    } else if (value === 'autobump') {
	    window.location.assign(link);
    } else {
      if (typeof productUpselling !== 'undefined' && productUpselling) {
        CartHandler.unselect(productUpselling, {
          list_id: adId,
          tac: tac
        });
      } else {
        $this.attr('data-product_upselling', '');

        if ($this.data('selected') !== undefined && $this.data('selected') === 'autobump') {
          CartHandler.unselect(CartHandler.AUTOBUMP, data);
        } else {
          CartHandler.unselect(CartHandler.BUMP, data);
        }

        $this.attr('data-selected', '');
      }
    }
  });

  jqnode.find('.gallery-select').on('change', function() {
    var $this = $(this);
    var adId = $this.attr('data-da_id');
    var listId = $this.attr('data-list_id');
    var productUpselling = $this.attr('data-product_upselling');
    var tac = $this.attr('data-tac').toString();
    var value = $this.val();

    var data = {
      list_id: listId,
      tac: tac
    };

    if (value === 'gallery') {
      CartHandler.select(CartHandler.GALLERY, data);
    } else if (value === 'gallery_1') {
      CartHandler.select(CartHandler.GALLERY_1, data);
    } else if (value === 'gallery_30') {
      CartHandler.select(CartHandler.GALLERY_30, data);
    } else {
      if (typeof productUpselling !== 'undefined' && productUpselling) {
        CartHandler.unselect(productUpselling, {
          list_id: adId,
          tac: tac
        });
      } else {
        $this.attr('data-product_upselling', '');
        CartHandler.unselect(CartHandler.GALLERY, data);
      }
    }
  });

  jqnode.find('.label-select').on('change', function() {
    var $this = $(this);
    var listId = $this.attr('data-list_id');
    var tac = $this.attr('data-tac').toString();
    var value = $this.val();
    var productUpselling = $this.attr('data-product_upselling');
    var adId = $this.attr('data-da_id');

    if (value > 0 && value <= 4) {
      CartHandler.select(CartHandler.LABEL, {
        list_id: listId,
        tac: tac,
        lt: value
      });
    } else {
      if (typeof productUpselling !== 'undefined' && productUpselling) {
        CartHandler.unselect(productUpselling, {
          list_id: adId,
          tac: tac
        });
      } else {
        $this.attr('data-product_upselling', '');
        CartHandler.unselect(CartHandler.LABEL, {
          list_id: listId,
          tac: tac
        });
      }
    }
  });

  jqnode.find('.combo-at-select').on('change', function(e) {
    var $this = $(this);
    var listId = $this.attr('data-list_id');
    var tac = $this.attr('data-tac').toString();
    var adId = $this.attr('data-da_id');
    var value = $this.val();
    var data = {
      list_id: listId,
      tac: tac,
      lt: 0
    };

    var targets = {
      at_combo1: CartHandler.COMBO_AT_1,
      at_combo2: CartHandler.COMBO_AT_2,
      at_combo3: CartHandler.COMBO_AT_3
    };

    if (targets[value]) {
      if (value === 'at_combo3') {
        data.lt = $this.data('label');
      } else {
        cleanComboPremium($this);
      }

      CartHandler.select(targets[value], data);
    } else {
      cleanComboPremium($this);
      CartHandler.unselect(targets['at_combo1'], data);
    }
  });


  jqnode.find('.combo-discount-select').on('change', function(e) {
    var $this = $(this);
    var listId = $this.attr('data-list_id');
    var tac = $this.attr('data-tac').toString();
    var adId = $this.attr('data-da_id');
    var value = $this.val();
    var data = {
      list_id: listId,
      tac: tac,
      lt: 0
    };

    var targets = {
      discount_combo1: CartHandler.DISCOUNT_COMBO_1,
      discount_combo2: CartHandler.DISCOUNT_COMBO_2,
      discount_combo3: CartHandler.DISCOUNT_COMBO_3,
      discount_combo4: CartHandler.DISCOUNT_COMBO_4
    };

    if (targets[value]) {
      CartHandler.select(targets[value], data);
    } else {
      CartHandler.unselect(targets['discount_combo2'], data);
    }

    showMsgPromo($this);
    hideMsgPromoChange($this);

  });

  if (eventsList === null) {
    highlightButton.on('click', function() {
      toggleProductsDetails($(this).parent().parent());
    });
  }

  jqnode.find('.icon-close').on('click', function(e) {
    e.preventDefault();
    toggleProductsDetails($(this).parent().parent());
  });
}

function activateProducts(jqnode) {
  var listID = jqnode.attr('data-list_id');
  var daID = jqnode.attr('data-da_id');
  var productsSelected = CartHandler.getProductsSelected(listID, daID);
  var $box = jqnode.find('.product-list-wrap');
  var $bumpSelect = $box.find('.bump-select');
  var $gallerySelect = $box.find('.gallery-select');
  var $labelSelect = $box.find('.label-select');
  var $comboAtSelect = $box.find('.combo-at-select');
  var $comboDiscountSelect = $box.find('.combo-discount-select');
  var iconHideClass = 'icon-hide';
  var valueLabel;
  var highlight = $box.parent().find('#da-highlight');

  var isAutoBumpSelected = productsSelected.indexOf(CartHandler.AUTOBUMP) !== -1;

  if (jqnode.hasClass('inactive') || jqnode.hasClass('already-loaded')) {
    return false;
  }

  $box.find('select:disabled').parent().addClass('disable');
  if (productsSelected.length && highlight.length > 0) {
    $box.addClass('is-visible');

    if (isAutoBumpSelected && $bumpSelect) {
      $bumpSelect.data('selected', 'autobump');
      $bumpSelect.val('autobump');
    }

    if (productsSelected.indexOf(CartHandler.BUMP) >= 0) {
      $bumpSelect.data('selected', 'bump');
      $bumpSelect.val('bump');
      $bumpSelect.addClass(iconHideClass);
    } else if (productsSelected.indexOf(CartHandler.DAILY_BUMP) >= 0 ||
      productsSelected.indexOf(CartHandler.UPSELLING_DAILY_BUMP) >= 0) {
      $bumpSelect.addClass(iconHideClass);
      $bumpSelect.data('selected', 'dayly_bump');
      $bumpSelect.val('daily_bump');
      $bumpSelect.attr('data-product_upselling',
        productsSelected.indexOf(CartHandler.UPSELLING_DAILY_BUMP) >= 0 ?
          CartHandler.UPSELLING_DAILY_BUMP : '');
    } else if (productsSelected.indexOf(CartHandler.WEEKLY_BUMP) >= 0 ||
      productsSelected.indexOf(CartHandler.UPSELLING_WEEKLY_BUMP) >= 0) {
      $bumpSelect.addClass(iconHideClass);
      $bumpSelect.data('selected', 'weekly_bump');
      $bumpSelect.val('weekly_bump');
      $bumpSelect.attr('data-product_upselling',
        productsSelected.indexOf(CartHandler.UPSELLING_WEEKLY_BUMP) >= 0 ?
          CartHandler.UPSELLING_WEEKLY_BUMP : '');
    }

    if (productsSelected.indexOf(CartHandler.GALLERY) >= 0 ||
      productsSelected.indexOf(CartHandler.UPSELLING_GALLERY) >= 0) {
      $gallerySelect.addClass(iconHideClass);
      $gallerySelect.val('gallery');
      $gallerySelect.attr('data-product_upselling',
        productsSelected.indexOf(CartHandler.UPSELLING_GALLERY) >= 0 ?
        CartHandler.UPSELLING_GALLERY : '');
    } else if (productsSelected.indexOf(CartHandler.GALLERY_1) >= 0) {
      $gallerySelect.addClass(iconHideClass);
      $gallerySelect.val('gallery_1');
    } else if (productsSelected.indexOf(CartHandler.GALLERY_30) >= 0) {
      $gallerySelect.addClass(iconHideClass);
      $gallerySelect.val('gallery_30');
    }

    if (productsSelected.indexOf(CartHandler.LABEL) >= 0 ||
      productsSelected.indexOf(CartHandler.UPSELLING_LABEL) >= 0) {
      $labelSelect.addClass(iconHideClass);
      valueLabel = CartHandler.getProductSelectedParam(
        CartHandler.LABEL, listID) + CartHandler.getProductSelectedParam(
        CartHandler.UPSELLING_LABEL, daID);
      $labelSelect.val(valueLabel);
      $labelSelect.attr('data-product_upselling',
        productsSelected.indexOf(CartHandler.UPSELLING_LABEL) >= 0 ?
        CartHandler.UPSELLING_LABEL : '');
    }

    if (productsSelected.indexOf(CartHandler.COMBO_AT_1) >= 0) {
      $comboAtSelect.addClass(iconHideClass);
      $comboAtSelect.val('at_combo1');
    } else if (productsSelected.indexOf(CartHandler.COMBO_AT_2) >= 0) {
      valueCombo = CartHandler.getProductSelectedParam(CartHandler.COMBO_AT_2, listID);
      $comboAtSelect.addClass(iconHideClass);
      $comboAtSelect.val('at_combo2');
    } else if (productsSelected.indexOf(CartHandler.COMBO_AT_3) >= 0) {
      var labelId = CartHandler.getProductSelectedParam(CartHandler.COMBO_AT_3, listID);

      $comboAtSelect.addClass(iconHideClass);
      $comboAtSelect.val('at_combo3');
      setComboPremiumLabel($comboAtSelect, CartHandler.products_options.options_list.label.option[labelId]);
    }


    if (productsSelected.indexOf(CartHandler.DISCOUNT_COMBO_1) >= 0) {
      valueCombo = CartHandler.getProductSelectedParam(CartHandler.DISCOUNT_COMBO_1, listID);
      $comboDiscountSelect.addClass(iconHideClass);
      $comboDiscountSelect.val('discount_combo1');
    } else if (productsSelected.indexOf(CartHandler.DISCOUNT_COMBO_2) >= 0) {
      valueCombo = CartHandler.getProductSelectedParam(CartHandler.DISCOUNT_COMBO_2, listID);
      $comboDiscountSelect.addClass(iconHideClass);
      $comboDiscountSelect.val('discount_combo2');
      showMsgPromo($comboDiscountSelect);
    } else if(productsSelected.indexOf(CartHandler.DISCOUNT_COMBO_4) >= 0){
      valueCombo = CartHandler.getProductSelectedParam(CartHandler.DISCOUNT_COMBO_4, listID);
      $comboDiscountSelect.addClass(iconHideClass);
      $comboDiscountSelect.val('discount_combo4');
    }
  }
  
  activateEvents(jqnode);
  jqnode.addClass('already-loaded');
  return true;
}

function activateProductsOnReady() {
  $('.single-da').each(function() {
    activateProducts($(this));
  });
  checkSelected();
}

function toggleProductsDetails(jqnode) {
  var $box = jqnode.find('.product-list-wrap');
  var top;


  if ($box.hasClass('is-visible')) {
    $box.removeClass('is-visible');
  } else {
    $box.addClass('is-visible');
    top = $box.offset().top;

    $('html, body').animate({
      scrollTop: (top + -100)
    }, 500);
  }
}

function dotsSubjects() {
  $('.da-subject').dotdotdot({
    watch: 'window',
    wrap: 'letter',
    fallbackToLetter: true
  });
}

function activateAd($element) {
  var adId = $element.data('ad-id');

  $.ajax({
    type: 'PUT',
    url: '/ad/activate_ad',
    data: JSON.stringify({
      list_id: adId
    }),
    dataType: 'json'
  }).done(function(response) {
    var $activateDa = $('li[data-list_id="' + adId + '"] #activate-da');

    if (response.status === 'ok') {
      $activateDa.removeClass('active').removeClass('da-options__option-highlight--blue');
      $activateDa.addClass('disable').addClass('da-options__option-highlight--inactive');
      $activateDa.attr('href','javascript:void(0)');
      $('.activate-msg.success').slideToggle('fast');
    } else {
      $('.activate-msg.error').slideToggle('fast');
    }
  });
}

function updateStickyBarValues() {
  var quantity = CartHandler.getTotalQuantity();
  var amount = CartHandler.getFormattedAmount(CartHandler.getTotalAmount());
  var stickyBar = document.querySelector('cart-sticky-bar');
  var tCart = '$ ' + amount;

  if (stickyBar) {
    stickyBar.productCount = quantity;
    stickyBar.cartTotal = tCart;
  }
}

function showOpMsg() {
  var $opMsg = $('.op-message');

  var opMsgHash;
  var opMsgLocation;
  var opMsgExpireDate;
  var opMsgAccID;
  var matches;
  var date;
  var expires;
  var cookieKey;

  if (!$opMsg.length) {
    return false;
  }

  opMsgHash = $opMsg.attr('data-msg-hash');
  opMsgLocation = $opMsg.attr('data-msg-location');
  opMsgExpireDate = $opMsg.attr('data-msg-expire-date');
  opMsgAccID = $opMsg.attr('data-acc-id');
  matches = opMsgExpireDate.match(/^\s*([0-9]+)\s*\/\s*([0-9]+)\s*\/\s*([0-9]+)(.*)$/);
  date = new Date(matches[2] + '/' + matches[1] + '/' + matches[3] + ' ' +
    matches[4]);
  expires = 'expires=' + date.toUTCString();
  cookieKey = 'opMsgHash_' + opMsgAccID + '_' + opMsgLocation;

  if (getCookie(cookieKey) !== opMsgHash) {
    $opMsg[0].style.display = 'block';
    document.cookie = cookieKey + '=' + opMsgHash + '; ' + expires;
  }

  return true;
}

function getCookie(name) {
  var start = document.cookie.indexOf(name + '=');
  var len = start + name.length + 1;
  var end = document.cookie.indexOf(';', len);

  if (!start && name !== document.cookie.substring(0, name.length)) {
    return null;
  }

  if (start === -1) {
    return null;
  }

  if (end === -1) {
    end = document.cookie.length;
  }

  return unescape(document.cookie.substring(len, end));
}

/*
 * @name closeButtonSelectClick
 * @description
 *
 * Clean select, hide close icon and show down arrow
 *
 * @param {element} el select of close button click
 *
 * @returns {void}
 *
 */
function closeButtonSelectClick(el) {
  var $this = el;
  var $parent = $this.parent();
  var $select = $parent.find('select');

  $select.val(0);
  $this.hide();
  $parent
    .find('.icon-arrow-down-bold')
    .show();
  $select.trigger('change');
  
}

/*
 * @name openPageSlideCombo
 * @description
 *
 * Handle plugin horizontalPageSlide for ComboA AT
 *
 * @param {element} el select option clicked
 *
 * @returns {void}
 *
 */
function openPageSlideCombo(elm) {
  $('#pageSlideCombo').horizontalPageSlide({
    title: $('#pageSlideCombo').data('title'),
    classContent: $('#pageSlideCombo').data('class'),
    classCustom: $('#pageSlideCombo').data('custom_class'),
    type: $('#pageSlideCombo').data('type'),
    optionSel: $('#pageSlideCombo').data('option_sel'),
    extraOptionSel: $('#pageSlideCombo').data('extra_option_sel'),
    select: elm,
    noImagesSelectors: ['at_combo2', 'at_combo3'],
    onClose: function (el) {
      var option = this.optionSelected;
      var label = '';
      var labelName = '';
      var iconClose = el.parent().find('.icon-close-circled');
      var comboName = $(el).find('option[value="' + option + '"]').data('name').toLowerCase();

      if (option == 0) {
        closeButtonSelectClick(iconClose);
      } else {
        if (option == 'at_combo3') {
          label = this.labelSelected;
          labelName = this.labelName;

          setComboPremiumLabel(el, labelName);
          el.attr('data-label', label);
        }

        xt_click(el,'C','','payment::combos::1260::' + normalizeWordAccent(comboName) + '','A');
        el.val(option).trigger('change');
      }
    }
  });
}

/*
 * @name setComboPremiumLabel
 * @description
 *
 * Edit and add label if user select Combo Premium Option
 *
 * @param {element} el select option clicked
 * @param {string} name of label
 *
 * @returns {void}
 *
 */
function setComboPremiumLabel(el, name) {
  var optionSelected = el.find('option[value="at_combo3"]');
  var textOriginal = el.data('original');

  if (textOriginal == undefined) {
    el.attr('data-original', optionSelected.html());
    textOriginal = optionSelected.html();
  }

  el.addClass('__withLabel');
  optionSelected.html(textOriginal + ' Etiqueta: ' + name + '');
}

/*
 * @name cleanComboPremium
 * @description
 *
 * handle close and clean of Combo Premium
 *
 * @param {element} el select option clicked
 *
 * @returns {void}
 *
 */
function cleanComboPremium(el) {
  el.removeClass('__withLabel');
  el.find('option[value="at_combo3"]').html(el.data('original'));
}

function showMsgPromo(el){
  var li = el.parent();
  var idSelected = el.val();

  if( idSelected == 'discount_combo2'){
    li.find('.msg_promo').show();
  }
}

function hideMsgPromo(el){
  var li = el.parent();
  var cb = li.find('.combo-discount-select');

  if(cb != 'undefined'){
    li.find('.msg_promo').hide();
  }
} 

function hideMsgPromoChange(el){
  var li = el.parent();
  var idSelected = el.val();

  if( idSelected != 'discount_combo2'){
    li.find('.msg_promo').hide();
  }
}