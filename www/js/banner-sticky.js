function bannerSticky(id, classFix, classMove, offset) {
	if (id == ""){
		return false;
	}
	var elem = window.parent.document.getElementById(id);
	if(elem == null){
		return;
	}
	if (classFix == "" || classMove == "") {
		return false;
	}
	if (offset == 0) {
		offset = 132;
	}
	window.parent.onscroll = function(e) {
		var coord = getScrollOffsets(window.parent);
		if (coord.y >= offset) {
			elem.className = classMove + " " + classFix;
		}else {
			elem.className = classMove;
		}
	}
}

function getScrollOffsets(wind) {

    // This works for all browsers except IE versions 8 and before
    if ( wind.pageXOffset != null ) 
       return {
           x: wind.pageXOffset, 
           y: wind.pageYOffset
       };

    // For browsers in Standards mode
    var doc = wind.document;
    if ( wind.document.compatMode === "CSS1Compat" ) {
        return {
            x: doc.documentElement.scrollLeft, 
            y: doc.documentElement.scrollTop
        };
    }

    // For browsers in Quirks mode
    return { 
        x: doc.body.scrollLeft, 
        y: doc.body.scrollTop 
    }; 
}
