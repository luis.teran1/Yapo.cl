/* global userOneClick, CartHandler, userCredits, xitiPayment,
   upsellingProcess, validateForm */

(function paymentMethodMobile() {
  document.addEventListener('DOMContentLoaded', onDOMLoaded);

  /**
   *
   * @name paymentMethodMobile#onDOMLoaded
   * @private
   * @description
   *
   * Set listeners for radio buttons. Also, it selectes the initial payment method.
   *
   */
  function onDOMLoaded() {
    var checkoutButton = document.getElementById('mobileCheckout');
    var backButton = document.getElementsByClassName('horizontalView-arrowIcon')[0];
    var paymentMethods = document.getElementsByClassName('paymentMethod');
    var thereAreCredits = thereAreCreditsOnCart();
    var enoughtCredits = haveEnoughCredits();
    var methodName = 'webpay';

    if (checkoutButton) {
      checkoutButton.addEventListener('click', payOrShowPaymentMethods);
    }
    if (backButton) {
      backButton.addEventListener('click', hidePaymentMethods);
    }

    CartHandler.addListener(onCartChange);
    toggleCredits(thereAreCredits || !enoughtCredits);

    if (!thereAreCredits && enoughtCredits) {
      methodName = 'credits';
    } else if (typeof userOneClick !== 'undefined' && userOneClick) {
      methodName = 'oneclick';
    }

    [].forEach.call(paymentMethods, function onEaccPaymentMethod(element) {
      element.addEventListener('click', onChangeRadio);
    });

    selectMethod(methodName);
  }


  /**
   *
   * @name paymentMethodMobile#onCartChange
   * @private
   * @description
   *
   * Callback called when cart changes and if users has enough credits and
   * there aren't credits on cart the payment method credits is enable.
   *
   */
  function onCartChange() {
    var canSelectCredits = !thereAreCreditsOnCart() && haveEnoughCredits();
    toggleCredits(!canSelectCredits);

    if (canSelectCredits) {
      selectMethod('credits');
    }
  }


  /**
   *
   * @name paymentMethodMobile#haveEnoughCredits
   * @private
   * @description
   *
   * Check if user has enough credits.
   *
   * @return {Boolean}
   *
   */
  function haveEnoughCredits() {
    if (typeof userCredits === 'undefined') {
      return false;
    }

    return userCredits >= getCartTotal();
  }


  /**
   *
   * @name paymentMethodMobile#onChangeRadio
   * @private
   * @description
   *
   * Get the payment method name of radio button to select its elements.
   *
   * @param {Object} event - Mouse event click.
   *
   */
  function onChangeRadio(event) {
    var paymentMethod = event.target;
    var methodName = paymentMethod.getAttribute('data-modifier');

    selectMethod(methodName);
  }


  /**
   *
   * @name paymentMethodMobile#cleanSelectedMethod
   * @private
   * @description
   *
   * Remove modifiers of current selected payment method.
   *
   */
  function cleanSelectedMethod() {
    var selectedElements = document.querySelectorAll('.horizontalView .__checked');
    var i = selectedElements.length - 1;

    for (; i >= 0; i--) {
      selectedElements[i].classList.remove('__checked');
    }
  }


  /**
   *
   * @name paymentMethodMobile#selectedMethod
   * @private
   * @description
   *
   * Select a new payment method by the method's name and update the pay
   * button's text.
   *
   * @param {String} methodName - payment method name.
   *
   */
  function selectMethod(methodName) {
    var paymentMethod = document.querySelector('.paymentMethod.__' + methodName);
    var radio = document.querySelector('[data-modifier="' + methodName + '"]');
    var checkbox = false;
    var input = false;
    if (paymentMethod && !radio.disabled) {
      checkbox = paymentMethod.querySelector('.paymentMethod-checkbox');
      input = paymentMethod.getElementsByTagName('input')[0];

      cleanSelectedMethod();

      paymentMethod.classList.add('__checked');
      checkbox.classList.add('__checked');
      input.checked = true;

      changeCheckoutText(methodName);
    }
  }


  /**
   *
   * @name paymentMethodMobile#changeCheckoutText
   * @private
   * @description
   *
   * Change pay button's text by payment method. OneClick shows "Inscribir" and
   * the others methods shows "Pagar".
   *
   * @param {String} selectMethod - payment method name.
   *
   */
  function changeCheckoutText(selectedMethod) {
    var checkoutTextElement = document
      .getElementsByClassName('summaryBar-payContainer')[0];
    var text = 'Inscribir';
    var horizontalView = document.getElementsByClassName('horizontalView')[0];

    if (selectedMethod !== 'oneclick' ||
      (typeof userCredits !== 'undefined' && userOneClick) ||
      horizontalView.classList.contains('__left')) {
      text = 'Pagar';
    }

    checkoutTextElement.textContent = text;
  }


  /**
   *
   * @name paymentMethodMobile#toggleCredits
   * @private
   * @description
   *
   * Enable/disable credits as payment method.
   *
   * @param {Boolean} disable - Flag to indicate if enable/disable the module.
   *
   */
  function toggleCredits(disable) {
    var creditWrapper = document.getElementById('creditPaymentMethod');
    var creditContent = document.getElementsByClassName('paymentMethodCredit')[0];
    var input = document.getElementById('creditPaymentMethod-input');
    var MODIFIER = '__disabled';
    var functionName = disable ? 'add' : 'remove';

    if (!creditWrapper) {
      return;
    }

    creditWrapper.classList[functionName](MODIFIER);
    creditContent.classList[functionName](MODIFIER);

    if (disable) {
      input.setAttribute('disabled', 'disabled');
    } else {
      input.removeAttribute('disabled');
    }
  }


  /**
   *
   * @name paymentMethodMobile#thereAreCreditsOnCart
   * @private
   * @description
   *
   * Check if there are credits on cart.
   *
   * @return {Boolean}
   *
   */
  function thereAreCreditsOnCart() {
    var productsID = Object.keys(CartHandler.products);
    var creditID = CartHandler.CREDITS.toString();
    var thereAre = false;

    productsID.forEach(function onEachProductID(productID) {
      if (productID === creditID) {
        thereAre = true;
      }
    });

    return thereAre;
  }


  /**
   *
   * @name paymentMethodMobile#payOrShowPaymentMethods
   * @private
   * @description
   *
   * Callback called when user tapped over pay button. If the user is in
   * "payment method horizontalView" send data to xiti and trigger a submit
   * over the form, else open the "horizontalView".
   *
   * @param {Object} event - Mouse event.
   *
   */
  function payOrShowPaymentMethods(event) {
    var horizontalView = document.getElementsByClassName('horizontalView')[0];
    var isActive = !horizontalView.classList.contains('__left');

    if (!validateForm()) {
      return;
    }

    if (isActive) {
      xitiPayment();
      document.getElementById('payment_form').submit();
    } else {
      $("#loading").show();

      setTimeout(
        function(){
          CartHandler.validate();
          $("#loading").hide();
          if (CartHandler.status === "OK") {
            showPaymentMethods();
          } else {
            showMsiteModal('ShoppingCartModal');
          }
        },
      10
      );
    }

    event.preventDefault();
  }


  /**
   *
   * @name paymentMethodMobile#showPaymentMethods
   * @private
   * @description
   *
   * Remove CSS modifier to make visible the payment methods horizontalView and
   * add a modifier to body to prevent the scroll.
   *
   */
  function showPaymentMethods() {
    var body = document.getElementsByTagName('body')[0];
    var horizontalView = document.getElementsByClassName('horizontalView')[0];

    changeCheckoutText();

    body.classList.add('u-noScroll');
    horizontalView.classList.remove('__left');

    if (upsellingProcess) {
      var sticky = document.getElementById('sticky-bar');
      var replacedClass = sticky.className.replace(' is-down', '');
      sticky.className = replacedClass;
      var currentPrice = document.querySelector('.m-price span').innerHTML;
      document.getElementById('total-amount').innerHTML = currentPrice.replace('$', '$ ');
      var thereAreCredits = thereAreCreditsOnCart();
      var enoughtCredits = haveEnoughCredits();
      toggleCredits(thereAreCredits || !enoughtCredits);

      var methodName = 'webpay';
      if (!thereAreCredits && enoughtCredits) {
        methodName = 'credits';
      } else if (typeof userOneClick !== 'undefined' && userOneClick) {
        methodName = 'oneclick';
      }

      selectMethod(methodName);
    }
  }


  /**
   *
   * @name paymentMethodMobile#hidePaymentMethods
   * @private
   * @description
   *
   * Hide payment methods horizontalView adding and removing CSS modifiers and
   * change pay button's text to "Pagar".
   *
   */
  function hidePaymentMethods() {
    var body = document.getElementsByTagName('body')[0];
    var horizontalView = document.getElementsByClassName('horizontalView')[0];

    body.classList.remove('u-noScroll');
    horizontalView.classList.add('__left');
    changeCheckoutText();
  }


  /**
   *
   * @name paymentMethodMobile#getCartTotal
   * @private
   * @description
   *
   * Get the cart's total amount from HTML.
   *
   * @return {Number}
   *
   */
  function getCartTotal() {
    var total = document.getElementById('total-amount');
    var totalText = total ? total.textContent : '';

    return totalText.replace(/[\.\$]/g, '') * 1;
  }
})();
