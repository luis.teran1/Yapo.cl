var regionColor = '#6eb961';
var regionHover = '#ff6600';
$(document).ready( function(){

	//Create alternative RaphaelJS version of the map for IE8
	var regions = [];
	var paper = Raphael('raphael_map_wrap', 280, 475);
	for(i=1; i < 16; i++){
		regions[i] = convertToVML(paper, 'r' + i);
		setEventsForRegion(regions, i);
	}

	$('.region-links li').hover( function(){
		var r = $(this).data('region');
		regions[r].attr('fill', regionHover);
	});

	$('.region-links li').mouseout( function(){
		var r = $(this).data('region');
		regions[r].attr('fill', regionColor);
	});

});

function convertToVML(paper, elementId){
	var region = paper.importSVG(document.getElementById(elementId));
	region.attr('fill', regionColor);
	$('#raphael_map_wrap').show();
	$('#map_wrap').hide();
	return region;
}

function setEventsForRegion(regions, i){
	var region = regions[i];
	var regionLink = i + 1;
	if(i == 15){
		regionLink = 1;
	}
	region.mouseover( function(){
		regions[i].attr('fill', regionHover);
		$('.region-links li:nth-child(' + (regionLink) + ')').css('text-decoration', 'underline');
	});
	region.mouseout( function(){
		regions[i].attr('fill', regionColor);
		$('.region-links li:nth-child(' + (regionLink) + ')').css('text-decoration', 'none');
	});
	region.click( function(){
		//Click the actual link from the original svg map
		var $link = $('#map a:nth-child(' + regionLink + ')');
		$link.trigger('click');
	});
}