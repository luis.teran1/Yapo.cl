/* global paymentChangeRegion */

$(document).ready(function() {
  var HIDDENCLASS = 'is-hidden';

  var validateOptions = {
    onkeyup: false,
    onfocusout: false,
    errorPlacement: function(error, element) {
      element.parent().next('.validation_msg')
        .text(error.text())
        .removeClass('success')
        .addClass('error')
        .show();
    },
    success: function(label, element) {
      $(element).parent().next('.validation_msg').hide();
    }
  };

  var $payment1 = $('#payment_1');
  var $payment2 = $('#payment_2');

  $('.invoice-selector input').iCheck({
    radioClass: 'iradio_minimal-black'
  });

  $('.invoice-selector label').click(toggleInvoice);
  $('.invoice-selector .iCheck-helper').on('click', toggleInvoice);

  $('.edit-email').click(toggleEmail);

  $('#id_region').change(paymentChangeRegion); // payment_common_util.js

  $('.email-wrapper input').change(function onChangeEmail(e) {
    syncValue.apply(this, [e, '.email-wrapper input']);
  });

  $('input.input').blur(moveLabel).keyup(moveLabel).each(function() {
    moveLabel({target: this });
  });



  $payment1.validate(validateOptions);
  $payment2.validate(validateOptions);


  if ($payment2.length) {
    $('#id_name').rules('add', {
      required: true,
      minlength: 2,
      messages: {
        required: 'Escribe por lo menos dos letras',
        minlength: 'Escribe por lo menos dos letras'
      }
    });

    $('#id_lob').rules('add', {
      required: true,
      messages: {
        required: 'Escribe tu giro'
      }
    });

    $('#id_rut').rules('add', {
      required: true,
      minlength: 5,
      rut: true,
      messages: {
        required: 'El RUT es muy corto',
        minlength: 'El RUT es muy corto',
        rut: 'El RUT no est&aacute; en el formato adecuado: XX.XXX.XXX-Y`'
      }
    });

    $('#id_address').rules('add', {
      required: true,
      minlength: 4,
      messages: {
        required: 'La direcci&oacute;n es muy corta',
        minlength: 'La direcci&oacute;n es muy corta'
      }
    });

    $('#id_communes').rules('add', {
      required: true,
      messages: {
        required: 'Selecciona una comuna'
      }
    });
  }

  if ($payment1.length || $payment2.length) {
    $('.email-wrapper input').rules('add', {
      required: true,
      email: true,
      messages: {
        required: 'Escribe un email v&aacute;lido',
        email: 'Escribe un email v&aacute;lido'
      }
    });
  }


  $.validator.addMethod('rut', function validateRut(value) {
    return $.Rut.validar(value);
  });


  $.validator.addMethod('email', function validateEmail(value) {
    return /^[a-zA-Z0-9._-]+@[a-zA-Z0-9-]+\.[a-zA-Z.]{2,5}$/i.test(value);
  });

  // Success
  $('.accordion-head').click( function() {
    var $this = $(this);

    $this.next().toggleClass('hidden');
    $('span', $this)
      .toggleClass('ui-icon-triangle-1-e')
      .toggleClass('ui-icon-triangle-1-s');
  });


  /**
   * @jsdoc function
   * @name toggleEmail
   * @description
   *
   * Prevent event action and toggleClass HIDDENCLASS of .email-wrapper
   *
   * @param {Object} event DOM's event
   * @return void
   *
   */
  function toggleEmail(event) {
    event.preventDefault();
    $('.email-wrapper').toggleClass(HIDDENCLASS);
  }


  /**
   * 
   * @jsdoc function
   * @name toggleInvoice
   * @description
   *
   * Add HIDDENCLASS to all div.form and remove it from element who its ID match
   * with event.target.value
   *
   * @param {Object} event DOM's event
   * @return void 0
   *
   */
  function toggleInvoice() {
    var value = $('input', $(this).parent()).val();
    $('div.form').addClass(HIDDENCLASS);
    $('#' + value).removeClass(HIDDENCLASS);
  }


  /**
   * @jsdoc function
   * @name syncValue
   * @description 
   *
   * Syncronize value between elements with same selector
   *
   * @param {Object} event DOM's event
   * @param {String=} selector CSS's selector
   * @return void 0
   *
   */
  function syncValue(event, selector) {
    var elem = $(selector || $(this).selector);
    elem.val(event.target.value);
    moveLabel({target: elem[0]});
  }


  /**
   *
   * @jsdoc function
   * @name moveLabel
   * @description
   *
   * Add/remove dirty css class to a label who match its `for` attr with 
   * event.target's id.
   * That css class move the label like the nativa android app
   *
   * @param {Object} event DOM's event
   * @return void 0 
   *
   */
  function moveLabel(event) {
    var fn = event.target.value ? 'addClass' : 'removeClass';
    $('label[for=' +event.target.id+ ']')[fn]('dirty');
  }
});

