/* 
* Yapo Map
* Conect to Nokia Here API and perform basic location operations
*/

(function($) {

	/**
	 * @constructor
	 * @param {String} mapWrapper - Selector for the map container
	 * @param {Object} options - Object containing the options
	 */
	function YapoMap(mapWrapper, options){
		this.$mapWrapper = mapWrapper;
		this.options = options;
		this.attribution = this.options.nokiaAttribution + (this.options.scheme.indexOf("hybrid") != -1 ? this.options.dgAttribution : '');
		this.baseUrl = this.options.baseUrl
			.replace('{proto}', document.location.protocol)
			.replace('{scheme}', this.options.scheme)
			.replace('{appId}', this.options.appId)
			.replace('{appCode}', this.options.appCode);
		this.geoUrl = this.options.geoUrl
			.replace('{proto}', document.location.protocol)
			.replace('{maxResults}', this.options.maxResults)
			.replace('{appId}', this.options.appId)
			.replace('{appCode}', this.options.appCode);
		this.marker = null;
		this.appl = this.options.appl;
		this.getPreciseOptionValue = this.options.getPreciseOptionValue;
		this.$region = $(this.options.regionInput);
		this.$commune = $(this.options.communeInput);
		this.$street = $(this.options.streetInput);
		this.$number = $(this.options.numberInput);
		this.$precise = $(this.options.preciseInput);
		this.$approx = $(this.options.approxInput);
		this.$geoPosition = $(this.options.getPositionInput);

		// Number of requests performed
		this.requests = 0;
		this.loadTileSuccess = false;

		this.buildMap();
		if(!this.options.marker)
			this.pinpointLocation();
		this.setEvents();
	}

	YapoMap.prototype = {
		/**
		 * Builds the map into the desired container
		 */
		buildMap: function(){
			var ym = this;

			try{
				ym.layer = ym.createLayer(ym.options.scheme);
			}catch(e){
				ym.options.onServiceNotAvailable();
				return;
			}

			ym.map = L.map(
				ym.$mapWrapper[0], {
				center: ym.options.center,
				zoom: ym.options.zoom,
				layers: [ym.layer]
			});
			ym.map.zoomControl.setPosition("bottomright");
			window.mapInstance = ym.map;
			
			//Decide if I must initially show the marker 
			if(this.options.marker){
				this.setMarker([this.options.marker[0], this.options.marker[1]], ym.getPreciseOptionValue());
			}

			//When images finished loading, we draw the market if they are not images with errors
			ym.layer.on('tileload', function(tile){
				var matches = tile.url.match(ym.options.errorTileLayer);

				//errorTileUrl do not exists in map
				if(!ym.loadTileSuccess && (!matches || (matches && matches.pop() != ym.options.errorTileLayer))){
					if(ym.marker){
						ym.map.removeLayer(ym.marker);
						ym.marker.addTo(ym.map);
						ym.resizeCircleDragBox();
						ym.loadTileSuccess = true;
					}
				}
			});

			ym.addTargetBlankToLeaftLetLink();

		},
		/**
		 * Adds a layer into the map
		 * @param  {String} scheme - Mode for the map eg.normal.day.transit
		 * @return {Object} - Layer object to be rendered into the map 
		 */
		createLayer: function(scheme){
			var ym = this;
			layer = L.tileLayer(ym.baseUrl,
				{
					attribution: ym.options.attribution,
					errorTileUrl: ym.options.errorTileLayer
				}
			);
			return layer;
		},
		/**
		 * Handler for the API response
		 * @param  {Object} response - Object delivered by the API
		 */
		processGeoResponse: function(response){
			var ym = this;

			if(ym.marker){
				ym.map.removeLayer(ym.marker);
			}
			if(response && response.Response && response.Response.View[0] && response.Response.View[0].Result[0]){
				if(response.Response.View[0].Result[0].Location == undefined){
					lat = response.Response.View[0].Result[0].Place.Locations[0].DisplayPosition.Latitude;
					lng = response.Response.View[0].Result[0].Place.Locations[0].DisplayPosition.Longitude;
				}else{
					lat = response.Response.View[0].Result[0].Location.DisplayPosition.Latitude;
					lng = response.Response.View[0].Result[0].Location.DisplayPosition.Longitude;
				}
				var relevance = response.Response.View[0].Result[0].Relevance;

				position = new L.LatLng(lat, lng);
				ym.$geoPosition.val([position.lat, position.lng].join(",")).trigger('change');
				ym.setMarker(position, ym.getPreciseOptionValue());

				if (relevance >= 0.9){
					if (ym.options.onAddressMapMatch)
						ym.options.onAddressMapMatch();
				} else {
					if (ym.options.onAddressMapDoesNotMatch)
						ym.options.onAddressMapDoesNotMatch();
				}
			} else {
				if (ym.options.onAddressMapDoesNotMatch)
					ym.options.onAddressMapDoesNotMatch();
			}
		},
		/**
		 * Get parameters and then make geolocation request 
		 */
		pinpointLocation: function(){
			var ym = this;

			var dataCommune = $(ym.options.communeInput+" option:selected").data().map;
			var dataRegion = $(ym.options.regionInput+" option:selected").data().map;
			var communeText = dataCommune ? dataCommune : '';
			var regionText = dataRegion ? dataRegion : '';

			ym.geocode(ym.$street.val(), ym.$number.val(), communeText, regionText);
		},
		/**
		 * Make API request to ged coordinates given a human readable address
		 * @param  {String} streetText  - Street name
		 * @param  {String} numberText  - Number of the street
		 * @param  {String} communeText - Commune name
		 * @param  {String} regionText  - Region name
		 */
		geocode: function(streetText, numberText, communeText, regionText){
			var ym = this;

			if(ym.requests >= ym.options.maxRequests){
				ym.options.onRequestsExceeded();
				return;
			}

			var searchText = streetText + " " + numberText + ", " + communeText + ", " + regionText + ", Republica de Chile";
			var geoUrl = ym.geoUrl.replace("{searchtext}", encodeURIComponent(searchText)) + '&token=' + Math.floor(Date.now() / 1000);

			$.ajax({
				url: geoUrl,
				dataType: "json",
				data: {},
				type: 'GET',
				success: function(data) {
					ym.requests ++;
					ym.processGeoResponse(data);
					ym.options.onSuccess();
				},
				error: function(XMLHttpRequest, textStatus, errorThrown){
					ym.options.onServiceNotAvailable();
				}
			});

		},
		/**
		 * Sets the upper and lower bound for zoom level
		 * @param {Number} minZoom 
		 * @param {Number} maxZoom 
		 */
		limitZoom: function(minZoom, maxZoom){
			var ym = this;

			var $zoomControlContainer = $(ym.map.zoomControl.getContainer());
			ym.map.options.minZoom = minZoom;
			ym.map.options.maxZoom = maxZoom;

			if(ym.map.getZoom() <= minZoom){
				$zoomControlContainer.find(".leaflet-control-zoom-out").addClass("leaflet-disabled");
			}else{
				$zoomControlContainer.find(".leaflet-control-zoom-out").removeClass("leaflet-disabled");
			}

			if(ym.map.getZoom() >= maxZoom){
				$zoomControlContainer.find(".leaflet-control-zoom-in").addClass("leaflet-disabled");
			}else{
				$zoomControlContainer.find(".leaflet-control-zoom-in").removeClass("leaflet-disabled");
			}
		},
		invalidateSize: function(){
			var ym = this;
			var size_map = ym.map.getSize();
			if(size_map.x== 0 && size_map.y == 0){
				setTimeout(function(){
					ym.map.invalidateSize();
					ym.layer.redraw();
				}, 1000);
			}
		},
		/**
		 * Sets a merker inside the map at the desired position
		 * @param {Object} position - Object with the position and precision
		 */
		setMarker: function(position, precise){
			var ym = this;
			if(precise){
				var icon_marker = L.icon(this.options.markerIcon);
				ym.marker = new L.marker(position, {draggable: ym.options.draggable, icon: icon_marker});
				ym.limitZoom(ym.options.minZoom.marker, ym.options.maxZoom.marker);
			}else{
				ym.addCircleMarker(position);
			}

			ym.marker.on('dragend', function(ev){
				position = ev.target._latlng;
				ym.map.setView(position);
				ym.$geoPosition.val([position.lat, position.lng].join(",")).trigger('change');
			});

			//when images of the map (tile layer) is correct, we add the marker to the map
			if(ym.loadTileSuccess){
				//Bug fixed: when size of map is 0, it is call invalidateSize.
				ym.invalidateSize();
				ym.marker.addTo(ym.map);
				ym.resizeCircleDragBox();
			}

			ym.map.setZoomAround(position, ym.options.zoom, { animate: 'true'});
			ym.map.setView(position, ym.options.zoom);


			ym.cleanMarkers();
		},

		cleanMarkers: function(){
			var ym = this;
			if (ym.$mapWrapper.is(':visible')) {
				ym.$mapWrapper.find('.leaflet-marker-icon').each(
					function(){
						if (! $(this).is(':visible'))
							$(this).remove();
					});
			}
		},

		addCircleMarker: function(position){
			var ym = this;
			if($.browser && $.browser.msie){
				ym.marker = new L.marker(position, {
					draggable: ym.options.draggable, 
					icon: L.icon(this.options.ieCircleMarker)
				});
				ym.limitZoom(ym.options.minZoom.marker, ym.options.maxZoom.marker);
				ym.map.on('zoomend', function(){
					if(!ym.getPreciseOptionValue()){
						ym.resizeCircleIE();
					}
				});
			}else{
				ym.marker = new L.circle(position, ym.options.circle.radius, this.options.circle);
				if (ym.options.draggable)
					ym.marker.editing.enable();
				ym.limitZoom(ym.options.minZoom.circleMarker, ym.options.maxZoom.circleMarker);
			}
		},

		resizeCircleIE: function(){
			//manually resize icon to match zoom level
			var ym = this;
			var zoomLevel = ym.map.getZoom();
			var size = ym.getCircleSizeByZoomLevel(zoomLevel);
			ym.options.ieCircleMarker.iconSize = [size, size];
			ym.options.ieCircleMarker.iconAnchor = [size/2, size/2];
			ym.marker.setIcon(
				L.icon(ym.options.ieCircleMarker)
			);
		},

		getCircleSizeByZoomLevel: function(zoomLevel){
			return (zoomLevel <= 20) ? Math.pow(2, (zoomLevel - 7)) : 340;
		},

		/**
		 * Adjust circlemarker draggable area
		 * */
		resizeCircleDragBox: function(){
			var ym = this;
			if (ym.options.draggable) {
				if (!ym.getPreciseOptionValue() && !($.browser && $.browser.msie)){
					zoomLevel = ym.map.getZoom();
					size = ym.getCircleSizeByZoomLevel(zoomLevel);
					margin = [-size/2, "px"].join("");
					side = [size, "px"].join("")
					v = {marginTop: margin, marginLeft: margin, width: side, height: side}
					if(ym.marker && ym.marker.editing && ym.marker.editing._moveMarker && ym.marker.editing._moveMarker._icon){
						$.extend(ym.marker.editing._moveMarker._icon.style, v)
					}
				}
			}
		},
		/**
		* Link to leaflet open in new window
		*/
		addTargetBlankToLeaftLetLink: function(){
			$(".leaflet-control-attribution a").attr("target","_blank");
		},
		/**
		 * Set-up dom events which affect the map
		 */
		setEvents: function(){
			var ym = this;

			if (ym.appl == 'ai'){
				var inputRadioIds = ym.options.preciseInput + ', ' + ym.options.approxInput;
				$(inputRadioIds).change(function(event){
					if(ym.marker){
						var position = ym.marker.getLatLng();
						ym.map.removeLayer(ym.marker);
						ym.setMarker(position, ym.getPreciseOptionValue());
					}
				});
			}

			// Get the zoom level when zoom action ends
			ym.map.on("zoomend", function(ev){
				ym.resizeCircleDragBox();
			});
			
			$(ym.options.showMapTrigger).click(function(event){
				ym.pinpointLocation();
			});

		}
	};

	$.fn.yapomap = function(options) {
		return this.each(function(){
			options = $.extend({}, $.fn.yapomap.defaults, options);
			var yapomap = new YapoMap($(this), options);
		});
	};

	$.fn.yapomap.defaults = {
		
		/*
		 * Whether or not the marker must be initially shown 
		 */
		precise: false, 
		marker: null,

		draggable: true,
		debug: false,
		scheme: 'normal.day.transit',
		zoom: 14,
		minZoom: {
			marker: 12,
			circleMarker: 12
		},
		maxZoom: {
			marker: 18,
			circleMarker: 18
		},
		center: [-33.4366458, -70.634649],
		markerIcon: {
			iconUrl: '/img/marker-icon.png',
			iconRetinaUrl: '/img/marker-icon-2x.png',
			iconSize: [25, 41],
			iconAnchor: [12, 41],
			popupAnchor: [-3, -76],
		},
		ieCircleMarker: {
			iconUrl: '/img/circle-marker.png',
			iconSize: [120, 120],
			iconAnchor: [60, 60],
			popupAnchor: [-3, -76],
		},
		circle: {
			radius: 500,
			fill: true,
			color: '#0055AA',
			opacity: 1,
			fillOpacity: 0.6,
			weight: 0,
			clickable: true,
			className: 'map-circle'
		},
		regionInput: '#region',
		communeInput: '#communes',
		streetInput: '#address',
		numberInput: '#address_number',
		preciseInput: '#exact',
		approxInput: '#approx',
		getPositionInput: '#geoposition',
		showMapTrigger: '#show_location',
		maxResults: 1,
		nokiaAttribution: '<a href="https://here.com/terms">Map &copy; 1987 - 2013 HERE</a>',
		dgAttribution: ', Imagery &copy; 2013 <a href="https://www.digitalglobe.com/">DigitalGlobe</a>',
		baseUrl: 'https://1.base.maps.api.here.com/maptile/2.1/maptile/newest/{scheme}/{z}/{x}/{y}/256/png8?app_id={appId}&app_code={appCode}&congestion',
		geoUrl: 'https://geocoder.api.here.com/search/6.2/search.json?searchtext={searchtext}&maxresults={maxResults}&app_id={appId}&app_code={appCode}',
		maxRequests: 5,
		errorTileLayer: '/img/map-provider-not-available.png',
		//Save map instance here
		onAddressNotFound: function(){
			// console.log('Address not found');
		},
		onSuccess: function(){
			// console.log('Success');
		},
		onServiceNotAvailable: function(){
			// console.log('Service not available');
		},
		onRequestsExceeded: function(){
			// console.log('Reached max number of requests');
		}

	};

})(jQuery);
