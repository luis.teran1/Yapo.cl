/* global jQuery  */
var listXiti = (function ($) { // eslint-disable-line no-unused-vars
  'use strict';

  var regexOnlyDigit = '';

  function createRegExp (regExp) {
    var newRegex = '';
    if (regExp !== '') {
      newRegex = new RegExp(regExp, 'g');
    }
    return newRegex;
  }
  function isNumeric (n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
  }
  function localParseInt (val) {
    var newValue = val;
    if (isNumeric(val) && val) {
      newValue = parseInt(val, 10);
    } else {
      newValue = 0;
    }
    return newValue;
  }
  /**
   * Obtiene un objeto select por el nombre
   * @param {string} name nombre del elemento select buscado
   * @returns {object} del elemento select encontrado
   */
  function getSelectByName (name) {
    var select = $('select[name=' + name + ']:visible');
    if (select.length > 1) {
      select = $('select[name=' + name + ']:visible:eq(0)');
    } else if (select.length === 0) {
      select = $('select[name=' + name + ']:eq(0)');
    }
    return select;
  }
  /**
   * Obtiene el texto de una opci�n seg�n el id enviado y element select.
   * @param {object} ele select utilizado donde se buscar� la opci�n
   * @param {number} idValue id de option a buscar
   * @returns {string} con el texto encontrado
   */
  function getTextValueByID (ele, idValue) {
    var tmpIdValue = idValue;
    var textValue = '';
    if (!idValue) {
      tmpIdValue = $(ele).val();
    }
    if (!idValue || idValue === '') {
      tmpIdValue = '0';
    }
    textValue = $(ele).find('option[value=' + tmpIdValue + ']').text();
    return textValue;
  }
  /**
   * Funci�n encargada de aplicar filtro en texto
   * @param {string} strFormat con texto a aplicar formato
   * @param {string} strReplace con expresi�n regular
   * @returns {string} con filtro aplicado
   */
  function filterTextOnlyDigit (strFormat, strReplace) {
    var newStr = '';
    newStr = strFormat.replace(regexOnlyDigit, strReplace);
    return newStr;
  }
  /**
   * Quita formato a texto
   * @param {string} price con el texto a quitar formato
   * @returns {number} con el texto sin formato
   */
  function unFormat (price) {
    var priceUnFormat = filterTextOnlyDigit(price.toString(), '').toLowerCase().trim();
    priceUnFormat = localParseInt(priceUnFormat);
    return priceUnFormat;
  }
  /**
   * Obtiene �ltima opci�n de un select
   * @param {object} select con las opciones a buscar
   * @return {number} con el valor de la �ltima opci�n del select
   */
  function getLastOption (select) {
    var lastValue = '';
    lastValue = $(select).find('option:last').val();
    lastValue = localParseInt(lastValue);
    return lastValue;
  }
  /**
   * Obtiene valor anterior al solicitado
   * @param {object} select con las opciones a buscar
   * @param {number} option con id de valor a consultar
   * @returns {string} con valor anterior
   */
  function getValPreviousOption (select, option) {
    var previousValue = '';
    var opt = $(select).find('option[value=' + option + ']');
    var posOpt = $(select).find('option').index(opt);
    if (posOpt === -1) {
      return '';
    }
    previousValue = $(select).find('option:eq(' + (posOpt - 1) + ')').text();
    return previousValue;
  }
  /**
   * Obtiene la descripci�n de la opci�n solicitada sin formato.
   * Ej: $ 12.000 => 12000
   * @param {string} name con el nombre del elemento select
   * @param {number} idValue con el id del elemento option
   * @param {string} ufStr con la expresi�n regular que se quiere aplicar al texto
   * @returns {number} con el valor sin formato encontrado en el select
   */
  function getValueByName (name, idValue, ufStr) {
    regexOnlyDigit = createRegExp(ufStr || '');
    var tmpIdValue = localParseInt(idValue);
    var select = getSelectByName(name);
    var text = getTextValueByID(select, tmpIdValue);
    var numberFormat = unFormat(text);
    var lastIdValue = getLastOption(select);
    if (lastIdValue === tmpIdValue) {
      var prevVal = getValPreviousOption(select, tmpIdValue);
      prevVal = unFormat(prevVal);
      if (prevVal === numberFormat) {
        numberFormat += 1;
      }
    }
    return numberFormat;
  }
  return {
    getValueByName: getValueByName
  };
})(jQuery);
