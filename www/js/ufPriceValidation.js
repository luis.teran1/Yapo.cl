var Yapo = Yapo || {};

Yapo.validateUFPrice = function validateUFPrice(evt) {
  // validates only has digits and no more of two decimals
  var expectedRegex = /(?:^\d{1,6}(,\d{1,2})?$)/;
  var charCode = evt.which;
  var inputChar = String.fromCharCode(charCode);
  var priceText = evt.currentTarget.value + inputChar;

  // Firefox trick evt.which=0 => null char evt.which=8 => backspace
  var isEmptyChar = charCode === 0 || charCode === 8;
  var commaOccurrences = priceText.match(/,/g);
  var hasEnoughCommas = commaOccurrences && commaOccurrences.length === 1 && priceText.length > 1;
  var val = priceText.replace(',', '.');

  if (!isEmptyChar && !expectedRegex.test(priceText) && !(inputChar === ',' && hasEnoughCommas)) {
    evt.preventDefault();
  } else {
    // ufMax as global bconf validation
    if (Math.ceil(val) > ufMax) {
      evt.preventDefault();
    }
  }
};

Yapo.formatUFPrice = function formatUFPrice(evt) {
  var el = evt.currentTarget;
  var price = el.value;
  var splittedPrice = price.replace(/\./g, '').split(',');
  var convertedPrice = splittedPrice[0];
  convertedPrice = convertedPrice.length
    ? parseInt(convertedPrice, 10).toLocaleString('es-CL')
    : '0';

  convertedPrice += splittedPrice[1] ? ',' + splittedPrice[1] : ''; // has decimmals
  convertedPrice === '0' ? convertedPrice = '' : convertedPrice;
  el.value = price.length ? convertedPrice : '';
};

Yapo.deleteUFPriceFormat = function formatUFPrice(evt) {
  var el = evt.currentTarget;
  var price = el.value;

  el.value = price.length ? price.replace(/\./g, '') : price;
};

Yapo.removeExtraDecimals = function removeExtraDecimals(value, decimalQuantity) {
  var dotPosition;

  var decimals = decimalQuantity || 2;
  var formattedValue = value.toString().replace(/,/g, '.');
  dotPosition = formattedValue.indexOf('.');

  return dotPosition !== -1
    ? formattedValue.substring(0, dotPosition + (decimals + 1))
    : formattedValue;
};

Yapo.convertPesosToUF = function convertUFToPesos(conversionFactor, price) {
  var factor = this.removeExtraDecimals(conversionFactor, 2);
  var convertedPrice = price || 0;
  convertedPrice = this.removeExtraDecimals(price, 2);

  return this.removeExtraDecimals(convertedPrice / factor, 2);
};

Yapo.convertUFToPesos = function convertUFToPesos(conversionFactor, price) {
  var factor = this.removeExtraDecimals(conversionFactor, 2);
  var convertedPrice = price || 0;
  convertedPrice = this.removeExtraDecimals(price, 2);

  return parseInt(convertedPrice * factor, 10);
};

Yapo.setMoneyFormat = function setMoneyFormat(price) {
  return parseFloat(price).toLocaleString('es-CL');
};
