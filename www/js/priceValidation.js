/* global Yapo */

/**
*
* @name Yapo#validatePrice
* @public
* @description
*
* Validate price for prevent numbers dont start 0
*
* @param {Object} evt - event handler keypress from input#price
*
*/
Yapo.validatePrice = function validatePrice(event) {
  var val = event.currentTarget.value;

  if (val[0] == 0) {
    event.currentTarget.value  = val.replace(/^0+/, '');
    val = event.currentTarget.value;
  }
};
