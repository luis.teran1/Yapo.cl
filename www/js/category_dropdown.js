
function form_key_lookup(key, sel_id) {
	sel_id = sel_id ? sel_id : '';

	if (key == 'type') {
		var selectType = document.getElementById("type" + sel_id);

		if (selectType && selectType.tagName != 'SELECT') /* IE fix */
			selectType = false;

		if (selectType) {
			return selectType.value;
		} else {
			var cont = document.getElementById("type_container" + sel_id);

			if (cont != null) {
				var inputs = cont.getElementsByTagName('INPUT');

				for (var idx = 0; idx < inputs.length; idx++) {
					if (inputs[idx].type == 'radio' && inputs[idx].checked)
						return inputs[idx].value;
				}
			}

			return 's';
		}
	}

	var category_group = document.getElementById("category_group" + sel_id);
	var sub_cat = document.getElementById("sub_category" + sel_id);
	var cat_id_elem = document.getElementById("cat_id");
	var cat_id;

	if (cat_id_elem && cat_id_elem.value != 0)
		cat_id = cat_id_elem.value;
	else if (sub_cat && sub_cat.style && sub_cat.style.display != 'none' && sub_cat.value != 0 && category_list[sub_cat.value].parent == category_group.value)
		cat_id = sub_cat.value;
	else
		cat_id = category_group.value;

	if (key == "category")
		return cat_id;
	if (key == "parent" && category_list[cat_id])
		return category_list[cat_id]['parent'];

	if (key == "has_store") {
		var store_row = document.getElementById('store_row');
		var res;

		if (store_row)
			res = store_row.style.display != 'none';
		else
			res = document.getElementById('store_holder') != null;
		if (res)
			return 1;
		else
			return 0;
	}

	if(key== "company_ad"){
		var c_ad = document.getElementById("c_ad");
		if(c_ad && c_ad.checked){
			return 1;
		}
		return 0;
	}

	var elem = document.getElementById(key + sel_id);
	if (elem) {
		return elem.value;
	}
}

function show_estate_types(e, args){
	var sel_id = this.getAttribute("data-sel_id");
	var category = document.getElementById("category_group" + sel_id).value;
	var all_params = get_settings('params', form_key_lookup, category_settings, sel_id);
	var params_list = all_params.split(',');
	/* Don't display estate type if the category does not have the param */
	if (params_list.indexOf("estate_type") == -1){
		document.getElementById("estate_type_div_" + sel_id).style.display = 'none';
	}
	else {
		/* Display the Estate Type */
		document.getElementById("estate_type_div_" + sel_id).style.display = 'block';
		var selectType = document.getElementById("estate_type_" + sel_id);
		var sequence = estateTypeArray['sequence'][category].split(",");
		var selected;
		/* Remove options and save previous selected */
		for (i = selectType.options.length - 1; i > 0; i--) {
			if (selectType[i].selected)
				selected = selectType[i].value;
			selectType.options.remove(i);
		}
		/* Add new options for the category & select previous selected if apply  */
		for (i = 0; i < sequence.length; i++) {
			seqValue = sequence[i];
			seqLabel = estateTypeArray['list'][seqValue];
			selectType.options[i+1] = new Option(seqLabel, seqValue);
			if (selected == seqValue)
				selectType.options[i+1].selected = true;
		}
	}
}

function show_category_types(e, args) {
	var sel_id = this.getAttribute("data-sel_id");
	var selectType = document.getElementById("type" + sel_id);
	var selected;


	/* Don't display type radio buttons when editing an old category ad */
	if (selectType && selectType.type == 'hidden')
		return 0;

	if (selectType && selectType.tagName != 'SELECT')
		selectType = false;

	/* Get the selected type */
	if (selectType) {
		for (i = 0; i < selectType.options.length; i++) {
			if (selectType[i].selected)
				selected = selectType[i].value;
		}
	}

	var types = get_settings('types', form_key_lookup, category_settings, sel_id);
	var type = types.split(',');

	if (!selectType) {
		for (var t in type) {
			var radio = document.getElementById("r" + type[t] + sel_id);
			if (radio && radio.checked) {
				selected = type[t];
				break;
			}
		}
	}

	if (type.length == 1)
		selected = type[0];
	/* Store new type options for this category */
	if (selectType) {
		selectType.options.length = 1;
		for (i = 0; i < type.length; i++) {
			selectType.options[i+1] = new Option(typeList[type[i]], type[i]);
			
			if (selected == type[i])
				selectType.options[i+1].selected = true;
		}
	} else {
		/* Don't try to optimize this code, it wont work for older ie if you do! */
		var cont = document.getElementById("type_container" + sel_id);

		cont.innerHTML = '';

		if (type.length > 1) {
			var inner_html = ''; 
			var j = 0;
			for (var i in type) {
				var t = type[i];
				if (inner_html != ""){
					if(i%2 != 0)
						inner_html += '</td><td style=" padding:0px 0px 6px 65px ">';
					else
						inner_html +='</td></tr><tr><td style=" padding:0px 0px 6px 0px ">';
				}
				else{
					inner_html = '<table id="types_tb"><tr><td style=" padding:0px 0px 6px 0px ">';
				}
				inner_html += '<input name="type" value="' + t + '" ' + (j == 0? 'checked="checked"' : '') + ' id="r' + t + sel_id + '" type="radio" sel_id="' + sel_id + '"> <label for="r' + t + sel_id + '">' +typeList[t] + '</label>&nbsp;';
				j++;
			}
			inner_html += '</td></tr></table>';
			cont.innerHTML = inner_html;
		}
		register_events(jsevents.ai, document);

		if (selected) { 
			setChecked("r" + selected + sel_id, true);
		}
	}
}

function set_category_changed() {
	var sel_id = this.getAttribute("data-sel_id");
	var selectType = document.getElementById("type" + sel_id);

	if (document.getElementById('category_changed' + sel_id)) {
		var cat_id = form_key_lookup('category', sel_id);
		document.getElementById('category_changed' + sel_id).value = (cat_id != document.getElementById('original_category').value) ? '1' : '0';
	}
}

function show_sub_category(e, args) {
	var sel_id = this.getAttribute("data-sel_id");
	var category_group = document.getElementById("category_group" + sel_id);
	var sub_cat = document.getElementById("sub_category" + sel_id);

	var par_id = category_group.value;

	var sub_cats = Array();
	if (category_list[par_id] && category_list[par_id]['level'] > 0) {
		for (var c in category_list) {
			if (category_list[c]['parent'] == par_id)
				sub_cats[sub_cats.length] = c;
		}
	}

	if (sub_cats.length > 0) {
		sub_cat.options.length = 1;
		for (var i = 0; i < sub_cats.length; i++) {
			var cat_id = sub_cats[i];
			var name = category_list[cat_id]['name'];
			if (args && args.noprice) {
				var price = split_setting(get_settings('price', 
							 function (key) {
								 if (key == "category")
									 return cat_id;
								 if (key == "parent" && category_list[cat_id])
									 return category_list[cat_id]['parent'];
							 },
							 category_settings));
				
				if (price)
					name += " \xA0 " + price.price + " " + js_info['MONETARY_UNIT'];
			}
			sub_cat.options[i + 1] = new Option(name, sub_cats[i]);
		}
		sub_cat.style.display = 'block';
		sub_cat.disabled = false;
	} else {
		sub_cat.style.display = 'none';
		sub_cat.disabled = true;
	}
}

function show_hide_private_company(e) {
	var category_selector = document.getElementById('category_group');
	var selected_option = category_selector.options[category_selector.selectedIndex];
	if (selected_option.value == 0) return 0;
	var value = split_setting(get_settings('pdefault',form_key_lookup, category_settings));
	var pdefault;
	if (value && (pdefault = document.getElementById('pdefault'))) {
		document.getElementById('pdefault').style.display=value.display;
		if (value.checked)
			document.getElementById(value.checked).checked=true;	
		/* For the label (de los cojones) */
		var label = split_setting(get_settings("name", form_key_lookup, label_settings));
		document.getElementById("name_label").innerHTML = label.label + ":";
		name_label_elemnt = document.getElementById("name_label");
		if(name_label_elemnt){
			name_label_elemnt.innerHTML = label.label + " *";
		}
	}
}

