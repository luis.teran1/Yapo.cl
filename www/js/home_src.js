/* global setCookie, xt_med */
'use strict';

(function() {
  $(document).on('mouseover', '.region-links li', overRegionName);
  $(document).on('mouseout', '.region-links li', outRegionName);
  $(document).on('click', '.region-link', clickRegion);
  $(document).on('mouseover', '.region-link', overRegion);
  $(document).on('mouseout', '.region-link', outRegion);


  function overRegionName() {
    var $elem = getRegionElement($(this));
    var classes = $elem.attr('class');
    $elem.attr('class', classes + ' hover');
  }


  function outRegionName() {
    var $elem = getRegionElement($(this));
    var classes = $elem.attr('class').replace('hover', '');

    $elem.attr('class', classes);
  }


  function clickRegion() {
    var $element = $(this);
    var url = $element.attr('href');
    var xtClickName = $element.data('xtclick-name');
    var domain = $element.data('domain');
    var regionOrder = $element.data('region-order');

    xt_med('C', '', xtClickName, 'N');
    startpage_set_default_ca(regionOrder, domain);

    location.href = url;
  }


  function overRegion() {
    var region = $(this).data('region') + 1;
    $('.region-links li:nth-child(' + region + ')').css('text-decoration', 'underline');
  }


  function outRegion() {
    var region = $(this).data('region') + 1;
    $('.region-links li:nth-child(' + region + ')').css('text-decoration', 'none');
  }


  function getRegionElement($element) {
    var region = $element.data('region');
    return $('#r' + region);
  }
})();


function startpage_set_default_ca(ca, domain, xtn2, region) {
  setCookie('default_ca', ca, 365, '/', domain, false);
  if (xtn2 && region) {
    xt_med('C', xtn2, region, 'N');
  }
  return true;
}
