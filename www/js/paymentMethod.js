/* global userOneClick, paymentMethod, userCredits, CartHandler, xt_click */

(function paymentMethods() {
  document.addEventListener('DOMContentLoaded', onDOMLoaded);

  /**
   *
   * @name paymentMethods#onDOMLoaded
   * @private
   * @description
   *
   * Set listener for radiobuttons and checks. Also, it selects the initial
   * payment method.
   *
   */
  function onDOMLoaded() {
    var radios = document.getElementsByName('payment_method');
    var checks = document.getElementsByClassName('radioCheck');
    var paymentTranslation = {
      oneclick: 'oneClick',
      webpay: 'webPay',
      servipag: 'servipag',
      credits: 'credits'
    };
    var methodName = paymentTranslation.webpay;
    var enoughCredtis = haveEnoughCredits();
    var thereAreCredits = thereAreCreditsOnCart();
    var radio;

    CartHandler.addListener(onCartChange);
    toggleCredit(thereAreCredits || !enoughCredtis);

    if (!thereAreCredits && enoughCredtis) {
      methodName = paymentTranslation.credits;
    } else if (typeof paymentMethod !== 'undefined' && paymentMethod) {
      methodName = paymentTranslation[paymentMethod];
    } else if (typeof userOneClick !== 'undefined' && userOneClick) {
      methodName = paymentTranslation.oneclick;
    }

    [].forEach.call(radios, function onEachRadio(element) {
      element.addEventListener('change', onChangeRadio);
    });

    [].forEach.call(checks, function onEachCheck(check) {
      check.addEventListener('click', triggerLabelClick);
    });

    radio = document.querySelector('[data-modifier="' + methodName + '"]');

    //check the corresponding radio button only if its not disabled
    if (radio && !radio.disabled) {
      radio.checked = true;
      selectMethod(methodName);
      changeCheckoutText(methodName);
    }
  }


  /**
   *
   * @name paymentMethod#onCartChange
   * @private
   * @description
   *
   * Callback called when cart changes and if user has enough credits
   * and there aren't credits on cart the payment method credits is enable.
   *
   */
  function onCartChange() {
    toggleCredit(!haveEnoughCredits() || thereAreCreditsOnCart());
  }


  /**
   *
   * @name paymentMethod#thereAreCreditsOnCart
   * @private
   * @description
   *
   * Check if there are credits on cart.
   *
   * @return {Boolean}
   *
   */
  function thereAreCreditsOnCart() {
    var productsID = Object.keys(CartHandler.products);
    var thereAre = false;

    productsID.forEach(function onEachProductID(productID) {
      if (productID === CartHandler.CREDITS.toString()) {
        thereAre = true;
      }
    });

    return thereAre;
  }


  /**
   *
   * @name paymentMethod#haveEnoughCredits
   * @private
   * @description
   *
   * Check if user has enough credits.
   *
   * @return {Boolean}
   *
   */
  function haveEnoughCredits() {
    if (typeof userCredits === 'undefined') {
      return false;
    }

    return userCredits >= getCartTotal();
  }


  /**
   *
   * @name getCartTotal
   * @private
   * @description
   *
   * Get the cart's total amount from HTML.
   *
   * @return {Number}
   *
   */
  function getCartTotal() {
    var total = document.getElementById('total-amount');
    var totalText = total ? total.textContent : '';

    return totalText.replace(/[\.\$]/g, '') * 1;
  }


  /**
   *
   * @name paymentMethods#onChangeRadio
   * @private
   * @description
   *
   * Get the payment method name of radiobutton for select its elements.
   *
   * @param {Object} event - Mouse event click.
   *
   */
  function onChangeRadio(event) {
    var radio = event.target;
    var methodName = radio.getAttribute('data-modifier');

    cleanSelectedMethod();
    selectMethod(methodName);
  }


  /**
   *
   * @name paymentMethods#selectMethod
   * @private
   * @description
   *
   * Select a new payment method by the method's name and update the pay
   * button's text.
   *
   */
  function selectMethod(methodName) {
    var input = document.getElementById(methodName + 'Check');
    var inputNext = false;
    if (input) {
      inputNext = input.nextElementSibling;
      sendToXiti(inputNext);
    }

    addModifier(methodName, 'Container');
    addModifier(methodName, 'Header');
    addModifier(methodName, 'Check');

    changeCheckoutText(methodName);
    toggleDocumentForm(methodName === 'credits');
  }


  /**
   *
   * @name paymentMethods#sendToXiti
   * @private
   * @description
   *
   * Get data from input to send to Xiti.
   *
   * @param {HTMLEntities} input - Radio button.
   *
   */
  function sendToXiti(input) {
    var name = input.getAttribute('data-xiti1');
    var type = input.getAttribute('data-xiti2');

    xt_click(this, 'C', '', name, type);
  }


  /**
   *
   * @name paymentMethods#addModifier
   * @private
   * @description
   *
   * Find an element and put a modifier.
   *
   * @param {String} methodName - Payment method's name
   * @param {String} ID - Last word of Payment method elements' ID
   *
   */
  function addModifier(methodName, ID) {
    var element = document.getElementById(methodName + ID);
    if (element) {
      element.classList.add('__' + methodName);
      element.classList.add('__selected');
    }
  }


  /**
   *
   * @name paymentMethods#cleanSelectedMethod
   * @private
   * @description
   *
   * Remove modifiers to current selected payment method.
   *
   */
  function cleanSelectedMethod() {
    var elements = document.getElementsByClassName('__selected');
    if (elements.length > 0) {
      var radio = elements[0].getElementsByTagName('input')[0];
      var modifier = '__' + radio.getAttribute('data-modifier');
      var i = elements.length - 1;

      for (i; i >= 0; i--) {
        elements[i].classList.remove(modifier);
        elements[i].classList.remove('__selected');
      }
    }
  }


  /**
   *
   * @name paymentMethods#triggerLabelClick
   * @private
   * @description
   *
   * Callback called when `span.radioCheck` is clicked. This CB dispatch a
   * click event over `label` to trigger a change of payment method.
   *
   * @param {Object} event - Mouse event click.
   *
   */
  function triggerLabelClick(event) {
    var target = event.target;
    var label = target.nextElementSibling.nextElementSibling;

    label.click();
  }


  /**
   *
   * @name paymentMethods#changeCheckoutText
   * @private
   * @description
   *
   * Change pay button's text by payment method. OneClick shows `Inscribir` and
   * the others one shows `Pagar`
   *
   * @param {String} methodName - Payment Method's name
   *
   */
  function changeCheckoutText(methodName) {
    var button = document.getElementById('checkout');
    var text = 'Inscribir';

    if (!button) {
      return;
    }

    if (methodName !== 'oneClick' || typeof userOneClick !== 'undefined' &&
      userOneClick) {
      text = 'Pagar';
    }

    button.innerHTML = '<span>' + text + '</span>';
  }


  /**
   *
   * @name paymentMethods#toggleCredit
   * @private
   * @description
   *
   * Enable/disable credits as payment method.
   *
   * @param {Boolean} disable - Flag to indicate if disable/enable the module.
   *
   */
  function toggleCredit(disable) {
    var credits = document.getElementsByClassName('creditPaymentMethod')[0];
    var container = document.getElementById('creditsContainer');
    var input = document.getElementById('payment_credits_big');
    var MODIFIER = '__disabled';
    var functionName = disable ? 'add' : 'remove';

    if (!container) {
      return;
    }

    credits.classList[functionName](MODIFIER);
    container.classList[functionName](MODIFIER);

    if (disable) {
      input.setAttribute('disabled', 'disabled');
    } else {
      input.removeAttribute('disabled');
    }
  }


  /**
   *
   * @name paymentMethods#toggleDocumentForm
   * @private
   * @description
   *
   * Hide/show summary's type options of sidebar's form.
   *
   */
  function toggleDocumentForm(hide) {
    var radios = document.getElementById('summaryType-options');
    var credit = document.getElementById('summaryType-credit');
    var MODIFIER = 'is-hidden';

    if (hide) {
      selectBill();
      radios.classList.add(MODIFIER);
      credit.classList.remove(MODIFIER);
    } else {
      radios.classList.remove(MODIFIER);
      credit.classList.add(MODIFIER);
    }
  }


  /**
   *
   * @name paymentMethods#selectBill
   * @private
   * @description
   *
   * Trigger a click over radio-bill.
   *
   */
  function selectBill() {
    var billRadio = document.getElementById('radio-bill');
    billRadio.click();
  }
})();

