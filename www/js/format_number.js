/* 
CHUS465: As a mobile user I would like standardized numeric fields in ad insert so that I can avoid mistakes when typing data

This file was created to have a common/generic functions related with format number form normal and mobile site.

*/

function format_number_input(field_id) {
	var field = document.getElementById(field_id);
	if (field == null)
        return;
	field.value = field.value.replace(/\D/g,'');
}


