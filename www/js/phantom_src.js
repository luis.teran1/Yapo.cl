$(document).ready( function(){

	var failureIcon = "<div class='diff-status diff-failure'>" +
						"FAIL" +
						"</div>";
	var successIcon = "<div class='diff-status diff-success'>" +
						"OK" +
						"</div>";

	$(".btn.delete").click( function(){
		var image = $(this).data("image");
		if(confirm("Are you sure you want to delete this image?")){
			location.href = "/phantom?action=delete&image=" + image;
		};
	});

	$(".previews").each( function(){
		//Add conflict markers
		if($(this).find(".fail").length > 0){
			$(this).append(failureIcon);
		}else{
			$(this).append(successIcon);
		}
	});

	$('.approve').click( function(e){
		e.preventDefault();

		var imageUrl = $(this).data('image-url');
		
		if(imageUrl.indexOf('diff') != -1){
			//Deleting original imgA
			var otherImage = imageUrl.replace('.diff', '');
			$(this).parents('.previews').find('li.imgA').fadeOut();
		}else{
			//Deleting diff imgB
			var otherImage = imageUrl.replace('.png', '.diff.png');
			$(this).parents('.previews').find('li.imgB').fadeOut();
		}
		deleteImage(otherImage);
	});

	$('.preview').mouseover( function(){
		$(this).find('.preview-options').show();
	});

	$('.preview').mouseout( function(){
		$(this).find('.preview-options').hide();
	});

});

function deleteImage(imageUrl){
	$.get('/phantom?action=delete&image=' + imageUrl);
}
