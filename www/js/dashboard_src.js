/* global CartHandler, xt_med, formatPrice */


/**
 * Account dashboard stuff
 */

$(document).ready(main);

function main() {
  'use strict';

  var arrowOffset = 5;
  var $message = $('#user-message');
  var $parent = $message.parent();
  var $text = $message.find('#user-message-text');
  var $close = $parent.find('.close-message');
  var $arrow = $('#insert-ads-arrow');
  $arrow.show();

  // Previously hidden with inline style
  $('#sticky-bar').addClass('is-down');

  checkSelectedProductsInGrid();
  CartHandler.addListener(updateStickyBar);
  CartHandler.executeListeners();

  $('#sort_by').sortby();

  $('.bump-select').productselect({
    theme: 'green',
    removeLabel: 'Quitar'
  });

  $('.gallery-select').productselect({
    theme: 'blue',
    removeLabel: 'Quitar'
  });

  $('.label-select').productselect({
    theme: 'purple',
    removeLabel: 'Quitar'
  });

  $('.combo-at-select').productselect({
    theme: 'light-blue',
    removeLabel: 'Quitar'
  });
  $('.combo-discount-select').productselect({
    theme: 'fucsia',
    removeLabel: 'Quitar'
  });

  function sortByChange() {
    window.location = $(this).find(':selected').data('url');
  }
  $('#sort_by').change(sortByChange);

  function bumpChange() {
    var adId;
    var unselect;
    var $this = $(this);
    var listId = $this.data('list_id');
    var value = $this.val();
    var tac = $this.data('tac').toString();

    var data = {
      list_id: listId, // eslint-disable-line
      tac: tac
    };

    var productUpselling = $this.data('product_upselling');

    var targets = {
      bump: CartHandler.BUMP,
      weekly_bump: CartHandler.WEEKLY_BUMP,
      daily_bump: CartHandler.DAILY_BUMP,
      autobump: CartHandler.AUTOBUMP
    };

    if (targets[value]) {
      CartHandler.select(targets[value], data);
    } else {
      if (productUpselling && typeof productUpselling !== 'undefined') {
        adId = $this.data('da_id');

        data.list_id = adId; // eslint-disable-line

        unselect = productUpselling;
      } else {
        $this.attr('data-product_upselling', '');

        if ($this.data('selected') !== undefined && $this.data('selected') === 'autobump') {
          unselect = CartHandler.AUTOBUMP;
        } else {
          unselect = CartHandler.BUMP;
        }

        $this.attr('data-selected', '');
      }

      CartHandler.unselect(unselect, data);
    }
  }

  // Change events for products
  $('.bump-select').change(bumpChange);

  function labelChange() {
    var adId;
    var unselect;
    var $this = $(this);
    var listId = $this.data('list_id');
    var value = $this.val();
    var tac = $this.data('tac').toString();

    var data = {
      list_id: listId, // eslint-disable-line
      tac: tac,
      lt: value
    };

    var productUpselling = $this.data('product_upselling');

    if (value > 0 && value <= 4) {
      CartHandler.select(CartHandler.LABEL, data);
    } else {
      if (typeof productUpselling !== 'undefined' &&
          productUpselling !== false && productUpselling !== '') {
        adId = $this.data('da_id');

        datal.list_id = adId // eslint-disable-line

        unselect = productUpselling;
      } else {
        $this.attr('data-product_upselling', '');
        unselect = CartHandler.LABEL;
      }

      CartHandler.unselect(unselect, data);
    }
  }

  $('.label-select').change(labelChange);

  function galleryChange() {
    var $this = $(this);
    var listId = $this.data('list_id');
    var value = $this.val();
    var tac = $this.data('tac').toString();

    var data = {
      list_id: listId, // eslint-disable-line
      tac: tac
    };

    var targets = {
      gallery_7: CartHandler.GALLERY,
      gallery_1: CartHandler.GALLERY_1,
      gallery_30: CartHandler.GALLERY_30
    };

    if (targets[value]) {
      CartHandler.select(targets[value], data);
    } else {
      CartHandler.unselect(targets.gallery_7, data);
    }
  }

  $('.gallery-select').change(galleryChange);

  //Add Combo At to cart
  function comboChange() {
    var adId;
    var unselect;
    var select = this;
    var $this = $(this);
    var listId = $this.data('list_id');
    var value = $this.val();
    var tac = $this.data('tac').toString();

    var data = {
      list_id: listId,
      tac: tac,
      lt: 0
    };

    var targets = {
      at_combo1: CartHandler.COMBO_AT_1,
      at_combo2: CartHandler.COMBO_AT_2,
      at_combo3: CartHandler.COMBO_AT_3
    };

    if ($this.data('label_selected')) {
      data.lt = select.getAttribute('data-label_selected');
    }

    if (targets[value]) {
      CartHandler.select(targets[value], data);
    } else {
      CartHandler.unselect(targets['at_combo1'], data);
    }
  }

  $('.combo-at-select').change(comboChange);


  //Add Combo discount to cart
  function comboDiscountChange() {
    var unselect;
    var select = this;
    var $this = $(this);
    var listId = $this.data('list_id');
    var value = $this.val();
    var tac = $this.data('tac').toString();

    var data = {
      list_id: listId,
      tac: tac,
      lt: 0
    };

    var targets = {
      discount_combo1: CartHandler.DISCOUNT_COMBO_1,
      discount_combo2: CartHandler.DISCOUNT_COMBO_2,
      discount_combo3: CartHandler.DISCOUNT_COMBO_3,
      discount_combo4: CartHandler.DISCOUNT_COMBO_4
    };

    if (targets[value]) {
      CartHandler.select(targets[value], data);
    } else {
      CartHandler.unselect(targets['discount_combo2'], data);
    }
  }

  $('.combo-discount-select').change(comboDiscountChange);

  if (isFirstTime() || $('.dummy-dashboard').length > 0) {
    setTimeout(openMessageFirst, 1000);
    $('.fade').show();
    $('.dummy-dashboard').fadeIn();
    $('#site-wrapper').css('max-height', '600px');
  }

  // Opens the message for the first time login/misc news.
  function openMessageFirst() {
    $message.removeClass('not-visible');
    $close.hide();
    $text.hide();
    $message.css('width', '0px');
    $message.css('height', '0px');

    $message.animate({
      width: '0px'
    }, 100, doSomething);

    function doSomething() {
      openMessageSecond($text, $message, $close);
    }
  }


  /** Bind handlers to open/close the message **/
  function closeClick() {
    closeMessage($text, $message, $close);
  }

  $close.click(closeClick);


  function arrowUp() {
    $arrow.animate({
      'margin-top': -arrowOffset
    }, 1000, arrowDown);
  }

  function arrowDown() {
    $arrow.animate({
      'margin-top': arrowOffset
    }, 1000, arrowUp);
  }

  arrowDown();

  // Tooltips
  $('.darktooltip').darkTooltip({
    animation: 'flipIn',
    gravity: 'north',
    opacity: 0.7
  });

  $('.bump-tooltip').darkTooltip({
    animation: 'flipIn',
    gravity: 'south',
    opacity: 0.7
  });

  // Galery warning tooltips
  function gallerySelect() {
    var $this = $(this);
    var next = $this.next();
    var tooltipContent = $this.data('tooltip');

    if (typeof tooltipContent !== 'undefined') {
      next.attr('data-tooltip', tooltipContent);
      next.darkTooltip({
        animation: 'flipIn',
        gravity: 'north',
        opacity: 0.7
      });
    }
  }

  $('.gallery-select').each(gallerySelect);

  // Combo Discount warning tooltips
  function comboDiscountSelect() {
    var $this = $(this);
    var next = $this.next();
    var tooltipContent = $this.data('tooltip');

    if (typeof tooltipContent !== 'undefined') {
      next.attr('data-tooltip', tooltipContent);
      next.darkTooltip({
        animation: 'flipIn',
        gravity: 'north',
        opacity: 0.7
      });
    }
  }

  $('.combo-discount-select').each(comboDiscountSelect);

  // Label warning tooltips
  function labelSelect() {
    var next;
    var tooltipContent = $(this).data('tooltip');

    if (typeof tooltipContent !== 'undefined') {
      next = $(this).next();
      next.attr('data-tooltip', tooltipContent);

      next.darkTooltip({
        animation: 'flipIn',
        gravity: 'north',
        opacity: 0.7
      });
    }
  }
  $('.label-select').each(labelSelect);

  // Bump more info
  function openBump(e) {
    e.preventDefault();
    openBumpPopup();
  }
  $('#bump-open-popup').click(openBump);

  // Label more info
  function moreInfoLabel(e) {
    e.preventDefault();
    openLabelPopup();
  }
  $('#label-more-info').click(moreInfoLabel);

  // All products more info
  function openProductsInfo(e) {
    e.preventDefault();
    openProductsInfoPopup();
  }
  $('#productInfo-open-popup').click(openProductsInfo);

  $('.da-heading .da-subject-cont').dotdotdot({
    watch: 'window'
  });

  if (isIE()) {
    preloadIconsForIE();
  }

  // On/off switches to activate/deactivate ads
  $('.toggles').toggles({
    text: {
      on: '',
      off: ''
    },
    drag: false,
    click: false
  });

  $('.toggles').click(toggleSomeShit);

  function toggleSomeShit(e) {
    var $element = $(e.currentTarget);
    var $toggle = $element.data('toggle-on');
    var $pack = $element.data('pack-expired');
    var $products = $element.data('products-running');

    if ($toggle) {
      if ($products) {
        confirmDeactivateModal($element);
      } else {
        packDeactivateAd($element);
      }
    } else if ($pack) {
      expiredPackModal($element);
    } else {
      packActivateAd($element);
    }

    var intervalOpeningModal = setInterval(function() {
      if ($('.buy-inserting-fee:visible').length > 0) {
        replaceIdModal($element);
        clearInterval(intervalOpeningModal);
      }
    }, 20);
  }

  $('.toggles.disabled').off();
  $('.toggles.disabled .toggle-blob').off();

  initStatusLabels();

  hiddenAdsMessage();

  function activateDa(e) {
    var $element = $(e.currentTarget);
    if ($element.hasClass('active')) {
      activateAd($element);
    }
  }
  $('.activate-da').click(activateDa);

  if (typeof statusPack !== 'undefined' && statusPack === "expired"){
    showSplash();
  }

  downloadReport();

}

function activateAd($element) {
  var adId = $element.data('ad-id');

  $.ajax({
    type: 'PUT',
    url: '/ad/activate_ad',
    data: JSON.stringify({
      'list_id': adId // eslint-disable-line
    }),
    dataType: 'json'
  }).done(function(response) {
    if (response.status === 'ok') {
      $element.removeClass('active');
      $element.addClass('disable');

      $('#ad_row_' + adId + ' .activate-da').attr('href','javascript:void(0)'); // eslint-disable-line
      $('#ad_row_' + adId + ' .cont-activate-da').addClass('disable');

      activateAdModal($element);
    } else {
      errActivateAdModal($element);
    }
  });
}

function packActivateAd($element) {
  var $adRow;
  var $subject;
  var $hasImages;
  var $bumpSelect;
  var $labelSelect;
  var $gallerySelect;
  var $comboAtSelect;
  var $hasLabelRunning;
  var $hasGalleryRunning;
  var adId = $element.data('ad-id');

  $.ajax({
    type: 'PUT',
    url: '/pack/ad_status',
    data: JSON.stringify({
      status: 'active',
      list_id: adId
    }),
    dataType: 'json'
  }).done(function(response) {
    if (response.status === 'ok') {
      // enabling the ad on the UI
      $element.parent().find('.label-status.active').show();
      $element.parent().find('.label-status.inactive').hide();
      $adRow = $('#ad_row_' + adId);
      $adRow.removeClass('delete disabled');
      $element.data('toggle-on', true);
      $element.toggles(true);

      // Changes the L&F of the ad for the active state
      $adRow.addClass('active');
      $subject = $adRow.find('.da-subject');

      if ('url' in response) {
        $subject.replaceWith(
          '<a href="'
          + response.url
          + '" class="da-subject" title="'
          + $subject.text() + '">'
          + $subject.text() + '</p>'
        );
      } else {
        $subject.replaceWith('<p class="da-subject">' + $subject.text() + '</p>');
      }

      $bumpSelect = $adRow.find('.product-select.green');

      if ('premium_products' in response) {
        $bumpSelect.removeClass('ps-disabled');
        $adRow.find('.bumpSelector').removeClass('__disable');
      }

      $gallerySelect = $adRow.find('.product-select.blue');
      $hasImages = $adRow.find('.gallery-select').data('image-count') > 0;
      $hasGalleryRunning = $adRow.find('.gallery-select').data('gallery-running');

      if ($hasImages && !$hasGalleryRunning && 'premium_products' in response) {
        $gallerySelect.removeClass('ps-disabled');
      }

      $labelSelect = $adRow.find('.product-select.purple');
      $hasLabelRunning = $adRow.find('.label-select').data('label-running');

      if (!$hasLabelRunning && 'premium_products' in response) {
        $labelSelect.removeClass('ps-disabled');
      }
      if ('update_cart' in response) {
        CartHandler.fetchProducts();
      }
    } else {
      noPackQuotaModal($element);
    }
  });
}

function packDeactivateAd($element) {
  var $adRow;
  var $subject;
  var $bumpSelect;
  var $labelSelect;
  var $gallerySelect;
  var adId = $element.data('ad-id');

  $.ajax({
    type: 'PUT',
    url: '/pack/ad_status',
    data: JSON.stringify({
      status: 'disabled',
      list_id: adId
    }),
    dataType: 'json'
  }).done(function(response) {
    // disabling the ad on the UI
    if (response.status === 'ok') {
      $element.toggles(false);
      $element.data('toggle-on', false);

      // Changes the L&F of the ad for the inactive state
      $adRow = $('#ad_row_' + adId);

      $element.parent().find('.label-status.inactive').show();
      $element.parent().find('.label-status.active').hide();

      $adRow.removeClass('active');
      $adRow.addClass('delete disabled');

      $subject = $adRow.find('.da-subject');
      $subject.replaceWith('<p class="da-subject">' + $subject.text() + '</p>');

      $bumpSelect = $adRow.find('.product-select.green');
      $bumpSelect.addClass('ps-disabled');
      $adRow.find('.bumpSelector').addClass('__disable');

      $gallerySelect = $adRow.find('.product-select.blue');
      $gallerySelect.addClass('ps-disabled');

      $labelSelect = $adRow.find('.product-select.purple');
      $labelSelect.addClass('ps-disabled');

      // Remove the product selected on the row
      $('#ad_row_' + adId + ' li.ps-default i.icon-close-circled').click();

      // Update the products from the cart
      CartHandler.fetchProducts();
    }
  });
}


function isIE() {
  var myNav = navigator.userAgent.toLowerCase();
  return myNav.indexOf('msie') !== -1 ? parseInt(myNav.split('msie')[1], 10) : false;
}


function openBumpPopup() {
  $.pgwModal({
    mainClassName: 'pgwModal bump-modal',
    target: '#bump-more-info',
    maxWidth: 800
  });
}

function openLabelPopup() {
  $.pgwModal({
    mainClassName: 'pgwModal bump-modal',
    target: '#label-more-info-popup',
    maxWidth: 800
  });
}

function openProductsInfoPopup() {
  $.pgwModal({
    mainClassName: 'pgwModal productsInfo-modal',
    target: '#productsInfo-container',
    maxWidth: 920
  });
}

function openMessageSecond($text, $message, $close) {
  $text.show();

  $message.animate({
    width: '944px'
  }, 800, function() {
    $close.fadeIn();
  });
}


function closeMessage($text, $message, $close) {
  /** Closes the message for the first time login/misc news **/
  $text.fadeOut(200);
  $close.hide();

  $message.animate({
    width: '0px',
    padding: '0px'
  }, 1000, function() {
    $message.addClass('not-visible');
  });
}


function isFirstTime() {
  var value;
  var url = location.href;
  var parts = url.split('#');

  if (parts.length < 2 || parts[1] !== 'welcome') {
    value = false;
  } else {
    value = true;
  }

  return value;
}


/**
 *
 * @name checkSelectedProductsInGrid
 * @public
 * @description
 *
 * Check if each Ad have a product in CartHandler and change it's selector
 * status.
 *
 */
function checkSelectedProductsInGrid() {
  var bumpSelectors = document.getElementsByClassName('bumpSelector');

  [].forEach.call(bumpSelectors, function onEachBumpSelector(bumpSelector) {
    var listID = bumpSelector.getAttribute('list_id');

    if (CartHandler.isProductSelected(CartHandler.AUTOBUMP, listID)) {
      bumpSelector.classList.add('__active');
    }
  });

  $('.bump-select').each(function() {
    var $this = $(this);
    var listId = $this.data('list_id');
    var adId = $this.data('ad_id');

    if (CartHandler.isProductSelected(CartHandler.BUMP, listId)) {
      $this.find('option[value=bump]').prop('selected', true).change();
    } else if (CartHandler.isProductSelected(CartHandler.AUTOBUMP, listId)) {
      $this.find('option[value=autobump]').prop('selected', true).change();
    } else if (CartHandler.isProductSelected(CartHandler.WEEKLY_BUMP, listId) || CartHandler.isProductSelected(CartHandler.UPSELLING_WEEKLY_BUMP, adId)) {
      $this.find('option[value=weekly_bump]').prop('selected', true).change();
      $this.attr('data-product_upselling',
        CartHandler.isProductSelected(CartHandler.UPSELLING_WEEKLY_BUMP, adId) ?
        CartHandler.UPSELLING_WEEKLY_BUMP : ''
      );
    } else if (CartHandler.isProductSelected(CartHandler.DAILY_BUMP, listId) || CartHandler.isProductSelected(CartHandler.UPSELLING_DAILY_BUMP, adId)) {
      $this.find('option[value=daily_bump]').prop('selected', true).change();
      $this.attr(
        'data-product_upselling',
        CartHandler.isProductSelected(CartHandler.UPSELLING_DAILY_BUMP, adId) ?
        CartHandler.UPSELLING_DAILY_BUMP : ''
      );
    }
  });

  $('.gallery-select').each(function() {
    var $this = $(this);
    var listId = $this.data('list_id').toString();
    var adId = $this.data('da_id').toString();

    if (CartHandler.isProductSelected(CartHandler.GALLERY, listId) ||
        CartHandler.isProductSelected(CartHandler.UPSELLING_GALLERY, adId)) {
      $this.find('option[value=gallery_7]').prop('selected', true).change();
    } else if (CartHandler.isProductSelected(CartHandler.GALLERY_1, listId)) {
      $this.find('option[value=gallery_1]').prop('selected', true).change();
    } else if (CartHandler.isProductSelected(CartHandler.GALLERY_30, listId)) {
      $this.find('option[value=gallery_30]').prop('selected', true).change();
    }
  });

  $('.label-select').each(function() {
    var selectedValue;

    var id = null;
    var product = null;
    var $this = $(this);
    var listId = $this.data('list_id').toString();
    var adId = $this.data('da_id').toString();

    if (CartHandler.isProductSelected(CartHandler.LABEL, listId)) {
      product = CartHandler.LABEL;
      id = listId;
    } else if (CartHandler.isProductSelected(CartHandler.UPSELLING_LABEL, adId)) {
      product = CartHandler.UPSELLING_LABEL;
      id = adId;
    }

    if (product !== null) {
      selectedValue = CartHandler.getProductSelectedParam(product, id);
      $this.find('option[value=' + selectedValue + ']').prop('selected', true).change();
    } else {
      $this.find('option[value=0]').prop('selected', true).change();
    }
  });

  $('.combo-at-select').each(function() {
    var $this = $(this);
    var listId = $this.data('list_id').toString();
    var adId = $this.data('da_id').toString();

    if (CartHandler.isProductSelected(CartHandler.COMBO_AT_1, listId)) {
      $this.find('option[value=at_combo1]').prop('selected', true).change();
    } else if (CartHandler.isProductSelected(CartHandler.COMBO_AT_2, listId)) {
      $this.find('option[value=at_combo2]').prop('selected', true).change();
    } else if (CartHandler.isProductSelected(CartHandler.COMBO_AT_3, listId)) {
      $this.find('option[value=at_combo3]').prop('selected', true).change();
    }
  });

  $('.combo-discount-select').each(function() {
    var $this = $(this);
    var listId = $this.data('list_id').toString();
    var adId = $this.data('da_id').toString();
    if (CartHandler.isProductSelected(CartHandler.DISCOUNT_COMBO_1, listId)) {
      $this.find('option[value=discount_combo1]').prop('selected', true).change();
    } else if (CartHandler.isProductSelected(CartHandler.DISCOUNT_COMBO_2, listId)) {
      $this.find('option[value=discount_combo2]').prop('selected', true).change();
    } else if (CartHandler.isProductSelected(CartHandler.DISCOUNT_COMBO_3, listId)) {
      $this.find('option[value=discount_combo3]').prop('selected', true).change();
    } else if (CartHandler.isProductSelected(CartHandler.DISCOUNT_COMBO_4, listId)) {
      $this.find('option[value=discount_combo4]').prop('selected', true).change();
    }
  });
}


function updateStickyBarElementQuantity(element, q) {
  if (q > 9999) {
    $(element).html('+9999');
  } else {
    $(element).html(q);
  }
}


/**
 *
 * @name updateStickyBar
 * @public
 * @description
 *
 * Show/hide sticky bar by quantity of selected products.
 * Update the total amount on sticky bar.
 * Update quantity in cart's badge.
 *
 */
function updateStickyBar() {
  var amount = CartHandler.getTotalAmount();
  var quantity = CartHandler.getTotalQuantity();
  var stickyBar = document.getElementById('sticky-bar');
  var totalAmount = document.getElementById('total-amount');

  if (quantity) {
    stickyBar.style.display = 'block';
    stickyBar.classList.remove('is-down');
  } else {
    stickyBar.classList.add('is-down');
  }

  totalAmount.innerHTML = formatPrice(amount);

  updateStickyBarElementQuantity('#number-items-cart', quantity);
}

function updateNewStickyBarCart(productsCount, totalCart) {
  var stickyBar = document.querySelector('cart-sticky-bar');
  var tCart = '$ ' + totalCart;

  if (stickyBar) {
    stickyBar.productCount = productsCount;
    stickyBar.cartTotal = tCart;
  }
}

function preloadIconsForIE() {
  var icon = '<i id="ie-icon" class="icon-yapo icon-close-circled"></i>';
  $('header').append(icon);
  $('#ie-icon').remove();
}

function initStatusLabels() {
  var $this;
  var active = '.label-status.active';
  var inactive = '.label-status.inactive';

  $('.toggles').each(function() {
    $this = $(this).parent();
    if ($(this).find('.toggle-blob').hasClass('on')) {
      $this.find(active).show();
      $this.find(inactive).hide();
    } else {
      $this.find(inactive).show();
      $this.find(active).hide();
    }
  });
}

function activateAdModal() {
  $.pgwModal({
    mainClassName: 'pgwModal activate-modal',
    target: '#activate_ad_mdl',
    maxWidth: 460
  });
  setupCloseButton();
}

function errActivateAdModal() {
  $.pgwModal({
    mainClassName: 'pgwModal activate-modal',
    target: '#err_activate_ad_mdl',
    maxWidth: 460
  });
  setupCloseButton();
}

function noPackQuotaModal($element) {
  var category = $element.data('ad-category');
  if(isCarCategory(category)) {
    $.pgwModal({
      mainClassName: 'pgwModal dark-modal __inserting-fee-modal __inserting-fee-modal-premium-cars',
      target: '#no_pack_quota',
      maxWidth: 696
    });
    carPremium($element);
  } else {
    $.pgwModal({
      mainClassName: 'pgwModal dark-modal __inserting-fee-modal',
      target: '#no_pack_quota',
      maxWidth: 550
    });
  }
  setupCloseButton();
}

function expiredPackModal($element) {
  var category = $element.data('ad-category');
  if(isCarCategory(category)) {
    $.pgwModal({
      mainClassName: 'pgwModal dark-modal __inserting-fee-modal __inserting-fee-modal-premium-cars',
      target: '#no_pack_quota',
      maxWidth: 696
    });
    carPremium($element);
  } else {
    $.pgwModal({
      mainClassName: 'pgwModal dark-modal __inserting-fee-modal',
      target: '#expired_pack',
      maxWidth: 550
    });
    replaceIdModal($element);
  }
  setupCloseButton();
}

function confirmDeactivateModal($element) {
  $.pgwModal({
    mainClassName: 'pgwModal dark-modal',
    target: '#confirm_deactivate',
    maxWidth: 650
  });

  $('.pm-content .btn-cancel').click(function() {
    var xtclickName = $(this).data('xtclick-name');
    xt_med('C', '', xtclickName, 'N');

    packDeactivateAd($element);
    $('.pm-close').trigger('click');
  });
  setupCloseButton();
}

function hiddenAdsMessage() {
  $('#dashboard-title').darkTooltip({
    gravity: 'west',
    content: '#hidden_ads',
    trigger: 'click',
    closable: false
  }).click();
}

function setupCloseButton() {
  $('.pm-content .btn-basic').off('click');
  $('.pm-content .btn-basic').click(function() {
    $('.pm-close').trigger('click');
  });
}

/**
 *
 * @param $element
 */
function carPremium($element) {
  $('.dialog-content.__inserting-fee').hide();
  $('.premium-pack-car').show();
  var price = $element.data("da-if-price");
  var id = atob($element.data("da-id"));
  var basic = $('.card-pack-destop-basic')[1];
  var premium = $('.card-pack-destop-premium')[1];
  var basicOldAction = basic.buttonAction;
  var premiumOldAction = premium.buttonAction;
  basic.price = price;
  basic.buttonAction = basicOldAction.replace('REPLACE_ID', id);
  premium.buttonAction = premiumOldAction.replace('REPLACE_ID', id);
}

/**
 *
 * @param category {int}
 * @returns {boolean}
 */
function isCarCategory(category) {
  return category === 2020 || category === 2040;
}

function replaceIdModal($element) {
  $modal = $(".buy-inserting-fee:visible");

  if ($modal.length > 0) {
    var category = $element.data('ad-category');
    if(!isCarCategory(category)) {
      var id = atob($element.data("da-id"));
      var href = $modal.attr("href").replace("REPLACE_ID", id);
      $modal.attr("href", href);

      var $price = $('.inserting-fee-price');
      var price = $element.data("da-if-price");
      var href = $modal.attr("href").replace("REPLACE_ID", id);
      $price.html(price);
    }
  }
}

/**
 *
 * @description
 *
 * Check event of PgwModal, when add new content for show, if is productInfo Modal
 * for build bxslider and event to assign.
 *
 */
$(document).bind('PgwModal::PushContent', function() {
  if ($('#pgwModal').hasClass('productsInfo-modal')) {
    var sliderPos = [];
    var sliders = $('#productsInfo .productsInfo-slider').children();
    var iconCloseElem = $('#pgwModal .pm-close .pm-icon');
    var prodSlider = $('#pgwModal .productsInfo-slider').bxSlider({
      mode: 'fade',
      controls: false,
      pager: false,
      slideWidth: 920,
      onSliderLoad: function () {
        iconCloseElem.addClass('__grey');
      },
      onSlideAfter: function (el) {
        var className = el.attr('class');

        if (className.indexOf('__index') >= 0) {
          iconCloseElem.addClass('__grey');
        } else {
          iconCloseElem.removeClass('__grey');
        }
      }
    });

    $('body').on('click', '.productsInfo-slider_control.__prev', function (e) {
      e.preventDefault();
      prodSlider.goToPrevSlide();
    });

    $('body').on('click', '.productsInfo-slider_control.__next', function (e) {
      e.preventDefault();
      prodSlider.goToNextSlide();
    });

    sliders.each(function (index) {
      var op = $(this).attr('class').replace('productsInfo-slider_item __', '');
      sliderPos[op] = index;
    });

    $('.productsInfo-slider_item.__index .index-options a').click(function (e) {
      e.preventDefault();
      var op = $(this).attr('href').replace('#', '');
      var pos = sliderPos[op];

      prodSlider.goToSlide(pos);
    });
  }
});
