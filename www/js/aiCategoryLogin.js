var Yapo = Yapo || {};

Yapo.aiCategoryLogin = (function() {
  function init(currentCategory, categoryData) {
    var hasModalConfig = categoryData && categoryData.hasOwnProperty(currentCategory);
    var isDisabled = hasModalConfig && parseInt(categoryData[currentCategory].disabled, 10) === 1;
    var isLogged = document.querySelector('.account-info') !== null;

    var loginButton;
    var modal;
    var modalConfig;

    if (!hasModalConfig || isLogged || isDisabled) {
      return;
    }

    modalConfig = categoryData[currentCategory];
    loginButton = document.getElementById('login-account-link');

    if (loginButton) {
      // open modal using the existent login button (Desktop)
      loginButton.click();
    }

    modal = document.querySelector('.loginBox');

    modal.classList.add(modalConfig.css_modifier);
    setLoginBoxContent(modalConfig);
    $(document).one('PgwModal::Close', closeModal);
  }

  function closeModal() {
    var categoryEl = document.getElementById('category_group');
    var event = document.createEvent('HTMLEvents');
    event.initEvent('change', false, true);

    categoryEl.value = '0';
    categoryEl.dispatchEvent(event);
  }

  function setLoginBoxContent(content) {
    var modal = document.querySelector('.loginBox');
    var element;

    var modalContent = [{
      selector: '.loginBox-header-title',
      html: content.title
    }, {
      selector: '.loginBox-header-subtitle',
      html: content.subtitle
    }, {
      selector: '.loginBox-footerText',
      html: content.footer
    }];

    modalContent.forEach(function(item) {
      element = modal.querySelector(item.selector);

      if (element) {
        modal.querySelector(item.selector).innerHTML = item.html;
      }
    });

    $(document).unbind('PgwModal::Close');
  }

  return init;
})();
