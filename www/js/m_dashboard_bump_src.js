/* globals updateStickyValues, CartHandler, adjustThumbs, mainFormBconf, Yapo, formatThousands */

function documentReady() {
  'use strict';
  function deleteSummaryTableItem(event) {
    var element = event.target;

    if (element.nodeName === 'SPAN') {
      element = element.parentElement;
    }

    var inputs = element.querySelectorAll('input');
    var id = inputs[0].value;
    var product = inputs[1].value;

    updateStickyValues();

    CartHandler.remove(product, {
      'list_id': id
    });

    var amount = CartHandler.getTotalAmount();
    var thousands = '$ ' + formatThousands(amount);

    // Refresh CartHandler amount
    var totalAmount = document.getElementById('total-amount');

    if (totalAmount) {
      totalAmount.innerHTML = thousands;
    }

    var summaryBarAmount = document.getElementById('summary-bar-amount');

    if (summaryBarAmount) {
      summaryBarAmount.innerHTML = CartHandler.getTotalQuantity();
    }

    // Hide Sticky Bar if there's no elements on it
    if (CartHandler.getTotalQuantity() === 0) {
      document.getElementById('sticky-bar').style.display = 'none';
    }
  }

  if (upsellingProcess) {
    var currentPrice;
    var sticky = document.getElementById('sticky-bar');
    var replacedClass = sticky.className.replace(' is-down', '');

    sticky.className = replacedClass;

    currentPrice = document.querySelector('.m-price span').innerHTML;
    document.getElementById('total-amount').innerHTML = currentPrice.replace('$', '$ ');
  } else {
    Yapo.summaryTable(deleteSummaryTableItem);
  }

  var dpi = window.devicePixelRatio;
  var thumbPath = $('#thumbnail-path').val();
  var thumbPath2x = $('#thumbnail-path-2x').val();

  adjustThumbs(dpi, thumbPath, thumbPath2x);

  $('#payment_form input').mdTextField();

  var $region = $('#pay_region');
  var $commune = $('#pay_communes');

  // horizontalSelect config
  if ($commune.length) {
    $region.horizontalSelect({
      onSelect: function onSelectRegion(event, select) {
        updateCommunes(select.val());
        $.horizontalSelect.changePage('pay_communes');
      }
    });

    $commune.horizontalSelect();
    /* @TODO: This should be done by the plugin, this forces the label to show when
     * you've got a preselected option
     */
    $commune.trigger('change');
  } else {
    $region.horizontalSelect();
  }

  // @TODO: This should be done by the plugin
  $region.trigger('change');

}

$(document).ready(documentReady);

function updateCommunes(region) {
  'use strict';

  function setOption(name, value) {
    return '<option value="' + value + '">' + name + '</option>';
  }

  function getOptions(bconf, set) {
    return $.map(bconf.communes[region], set);
  }

  var options = getOptions(mainFormBconf, setOption);
  var $communes = $('#pay_communes');

  $communes.empty();
  $communes.html(options.join('\n'));
  $communes.val(0).trigger('change');
  $.horizontalSelect.updatePage('pay_communes');
  $('#hst-pay_communes').show();
}

