var galleryPopupOpen = false;

$(document).ready( function(){

    AdjustThumbnails();

    //Open gallery walkthrough
    $("#gallery-info").click( function(e){
        e.preventDefault();
        //check if I need to show normal gallery info or awesome gallery info
        if($('.gallery-list.real').children() && $('.gallery-list.real').children().length == 5){
            OpenGalleryInfoList();
        }else{
            OpenGalleryInfoDash();
        }
    });

    //Open gallery walkthrough
    $("#gallery-more-info").click( function(e){
        e.preventDefault();
        OpenGalleryInfoDash();
    });

	AdjustGalleryForIE8();

	$(document).bind('PgwModal::Close', function() {
	    galleryPopupOpen = false;
	});

});

function OpenGalleryInfoList(){
	var delayAnimation = 200;
  // Fix scroll popup when there are banner top
  var isBannerTop = false;

	if (galleryPopupOpen){
		return;
	}

  if ($('#banners-top-wrapper').css('display') == 'block') {
    isBannerTop = true;
  }

	$('body, html').animate({
		'scrollTop' : 95
	}, delayAnimation, function(){
		eventCloseGallery();
		galleryPopupOpen = true;
		makeAdsShine(isBannerTop);
	});

	$('body').addClass('perspective');
	$('body').css('overflow-x', 'hidden');
	$(".panel.header").addClass('blurry');
	$(".SearchBarTable").addClass('blurry');
	$(".TabContainer").addClass('blurry');
	
  showTopAndBottom();

	$("body").append("<ins class='fade-gallery'></ins>");

	//Close events
	$('.icon-close-gallery').click( function(){
		CloseAll();
	});

	$(".fade-gallery").click( function(e){
		CloseAll();
	});
}

function makeAdsShine(isBannerTop){
	//Show gallery ads or dummy ads
	if($('.gallery-list.real').children()){
		$(".gallery-list.real").addClass('gallery-stick-out');
		$(".gallery-list li").addClass('shine');
	}else{
		$(".gallery-list.dummy").show();
		$(".gallery-list.dummy").addClass('gallery-stick-out');
	}

  if (isBannerTop) {
    $('.gallery-list').addClass('withBannerTop');
  }
}

function eventCloseGallery(){
	$(document).scroll( function(){
		if(galleryPopupOpen){
			var sc = $(document).scrollTop();
			if(sc != 95){
				CloseGalleryInfoList();
			}
		}
	});
}

function CloseGalleryInfoList(){
	$('body').removeClass('perspective');
	$('body').css('overflow-x', 'visible');
	$(".blurry").removeClass('blurry');
	$("ins.fade-gallery").remove();

	$(".gallery-list").removeClass('gallery-stick-out');
	$(".gallery-list").removeClass('withBannerTop');
	$(".gallery-list li").removeClass('shine');

	galleryPopupOpen = false
	RemoveTopAndBottom();
}

function OpenGalleryInfoDash(){
	if(galleryPopupOpen){
		return;
	}
	$.pgwModal({
		mainClassName: 'pgwModal gallery-modal',
		target: '#gallery-more-info-popup',
		maxWidth: 700
	});
	galleryPopupOpen = true;

}

function CloseGalleryInfoDash(){
	galleryPopupOpen = false;
	$.pgwModal('close');
}

//Open top and bottom parts of the gallery popup
function showTopAndBottom(){
	$('body').append($('#gallery-info-listing-top').clone().html());
	$('body > .gallery-info-top').addClass('top-gal-listing');

	$('body').append($('#gallery-info-listing-bottom').clone().html());
	$('body > .gallery-info-bottom-wrap').addClass('bottom-gal-listing');
	$('.bottom-gal-listing > .gallery-info-foot').addClass('foot-gal-listing');

	setTimeout( function(){
		$('.top-gal-listing').addClass('reveal-top');
		$('.bottom-gal-listing').addClass('reveal-bottom');
	}, 100);
}

function RemoveTopAndBottom(){
	$('body > .gallery-info-top').remove();
	$('body > .gallery-info-bottom-wrap').remove();
	$('body > .gallery-info-foot').remove();
}

function CloseAll(){
	CloseGalleryInfoDash();
	CloseGalleryInfoList();
}

function AdjustGalleryForIE8(){
	$($('.gallery-list li')[4]).css('margin-right', '0px');
}

function AdjustThumbnails(){
	//Adjust gallery thumbnails
	$(".gallery-image").each(function(){

		var imgUrl = $(this).css('background-image').replace('url(','').replace(')','');
		imgUrl = imgUrl.replace(/\"/g, '');
		imgUrl = imgUrl.replace(/\'/g, '');
		imgUrl = $.trim(imgUrl);
		if(imgUrl == 'none'){
			return;
		}
		var bgImg = new Image();
		bgImg.src=imgUrl;
		var container = this;

		$(bgImg).load( function(){
			var imgWidth = bgImg.width;
		    var imgHeight = bgImg.height;
		    if(imgWidth >= imgHeight){
		    	$(container).css('background-size', 'auto 100%');
		    }
		})
	});
}
