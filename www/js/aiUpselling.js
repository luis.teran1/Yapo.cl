/* globals paymentSettings, upsellingProducts, upsellingProductsCategories, upsellingProductsLabels */

var Yapo = Yapo || {};

Yapo.aiUpselling = function asdf() {
  var CURRENT_CAT = '';
  var IS_PRO_FOR = $('.account-info').data('isprofor') || '';
  var BOX_CLASS = 'aiUpselling-box';
  var boxes = document.getElementsByClassName(BOX_CLASS);

  /**
   * External xiti dependency. Not sure how to handle this.
   *
   * @TODO: Figure out how to handle xiti calls
   **/
  function xitiClick(box, radio) {
    var cat;
    var tag;
    var subCat;
    var xitiTag = box.attributes.xititag;

    if (typeof xt_click === 'function' && xitiTag) { // eslint-disable-line
      subCat = document.getElementById('category_group').value;
      cat = '::' + subCat[0] + '000::';

      tag = 'Ad_insertion' + cat + subCat + '::' + xitiTag.value;

      xt_click(this, 'C', '', tag, 'A'); // eslint-disable-line
    }
  }

  /*
   * Check if the variable upsellingProducts or upsellingProductsCategories
   * exist, they are defined on the template.
   **/
  function checkData() {
    if (typeof upsellingProducts !== 'undefined' ||
        typeof upsellingProductsCategories !== 'undefined') {
      return upsellingProducts || upsellingProductsCategories;
    }
    return false;
  }

  /*
   * Finds and returns first child of #elem by its #className
   **/
  function findChildByClass(elem, className) {
    var i;
    var foundChild;
    var childs = elem.children;

    for (i = 0; i < childs.length; i++) {
      if (childs[i].className.indexOf(className) > -1) {
        foundChild = childs[i];
      }
    }

    return foundChild;
  }

  /*
   * Expand a single box.
   **/
  function expandBox(box) {
    var child = findChildByClass(box, 'aiUpselling-body');

    if (child) {
      box.className += ' __expanded'; // eslint-disable-line
      child.className = 'aiUpselling-body';
    }
    return true;
  }

  /*
   * Expand all boxes.
   * Assumes "boxes" contains all box elements
   **/
  function expandBoxes() {
    var i;

    for (i = 0; i < boxes.length; i++) {
      expandBox(boxes[i]);
    }
  }

  /**
   * Set click listener to container.
   **/
  function listenerHandling(type, elems, callback) {
    var i;
    var action = type + 'EventListener';

    for (i = 0; i < elems.length; i++) {
      elems[i][action]('click', callback, false);
    }
  }

  /**
   * Select clicked combo, which means deselecting all other combos and clearing
   * tag labels.
   **/
  function chooseBox(e) {
    var nope;
    var isTag;
    var anyChosen;
    var isDisabled;
    var selectedTag;
    var tagContainer;

    var productId = '';
    var radio = this.querySelector('.aiRadioButton');

    // Not functional, should(will?) remove.
    if (e && e.target) {
      isTag = e.target.className.indexOf('aiUpselling-tag') < -1;
    }

    isDisabled = this.className.indexOf('__disabled') > -1;

    if (isDisabled) {
      anyChosen = document.getElementsByClassName('aiRadioButton');

      [].forEach.call(anyChosen, function(elem) {
        if (elem.className.indexOf('__selected') > 0) {
          nope = true;
        }
      });

      if (!nope) {
        chooseDefault();
      }

      return false;
    }

    if (radio.className.indexOf('__selected') < 0 && isTag) {
      clearLabels();
    }

    clearRadioButtons(radio.className);

    if (radio.className.indexOf('__selected') === -1) {
      radio.className += ' __selected';
      xitiClick(this, radio);
    }

    // Set product id
    if (this.attributes['product-id']) {
      productId = this.attributes['product-id'].value;
    }
    setProduct(productId);

    // If there's any tags
    tagContainer = this.querySelector('.aiUpselling-tagContainer');
    selectedTag = document.querySelector('.aiUpselling-tag.__' + this.id);

    if (tagContainer && !selectedTag) {
      // Default option
      if (!isTag) {
        clearLabels();
        this.querySelector('.aiUpselling-tag').className = 'aiUpselling-tag __' + this.id;
        setLabel(1);
      }
    } else if (!tagContainer && !selectedTag && !isTag) {
      clearLabels();
      setLabel('');
    }

    return true;
  }

  function setProduct(productId) {
    var text;
    var product = document.getElementById('upselling_product');

    if (!productId || productId == 'noproduct') {
      product.removeAttribute('value');
      text = 'Publicar ahora';
    } else {
      product.value = productId;
      text = 'Publicar y pagar';
    }

    changePublishText(text);
  }

  function setLabel(labelId) {
    var label = document.getElementById('upselling_product_label');
    label.value = labelId;
  }

  function changePublishText(text) {
    document.getElementById('submit_create_now').value = text;
  }

  /*
   * Category Listener
   * Change content of combos depending on category selected
   **/
  function categoryListener() {
    var catGroup = document.getElementById('category_group');

    catGroup.addEventListener('selectCategory', function passEvent(e) {
      setCurrentProducts(e.target.value, IS_PRO_FOR);
    }, false);
  }

  /**
   * This is the least functional function (duh) in this componente, sorry.
   **/
  function setCurrentProducts(category, isProFor) {
    var tags;
    var type;
    var filter;

    var productList = Yapo.aiUpsellingProducts.getUpsellingCombos(
        paymentSettings,
        upsellingProducts,
        upsellingProductsCategories,
        category,
        isProFor
    );

    var currentCat = upsellingProductsCategories[category];
    var isThereCurrentCombo = document.getElementById('upselling_product').value;
    var isThereCurrentLabel = document.getElementById('upselling_product_label').value;

    if (category === '0') {
      hideUpselling();
    } else {
      showUpselling();
    }

    for (type in productList) { // eslint-disable-line
      updateTemplate(type, productList[type]);
    }

    detectImages();

    if (CURRENT_CAT !== currentCat && isThereCurrentCombo === '') {
      chooseDefault();
    } else if (isThereCurrentCombo) {
      filter = filterBox(isThereCurrentCombo);

      chooseBox.call(document.getElementById(filter));

      if (isThereCurrentLabel) {
        tags = document.getElementById(filter).querySelectorAll('.aiUpselling-tag');

        [].forEach.call(tags, function(elem) {
          if (elem.attributes['label-id'].value === isThereCurrentLabel) {
            clearLabels();
            elem.className = 'aiUpselling-tag __' + filter; // eslint-disable-line
            setLabel(isThereCurrentLabel);
          }
        });

        expandBoxes(boxes);
      }
    } else {
      chooseDefault();
    }

    CURRENT_CAT = currentCat;
  }

  function filterBox(productId) {
    var filter;
    var returnId;

    boxes = document.getElementsByClassName('aiUpselling-box');

    [].forEach.call(boxes, function(elem) {
      filter = elem.attributes['product-id'];

      if (filter && filter.value === productId) {
        returnId = elem.id;
      }
    });

    return (typeof returnId != 'undefined')?returnId:boxes[0].id;
  }

  // @TODO: Remove jQuery dep
  function hideUpselling() {
    $('.aiUpselling').hide(500);
  }

  // @TODO: Remove jQuery dep
  function showUpselling() {
    $('.aiUpselling').show(500, () => {
      $('.aiUpselling').css('overflow', 'visible');
    });
  }

  /**
   * Update a section (title, subtitle, body, prize) from a combo
   **/
  function updateSection(type, section, content, split) {
    var newContent = '';
    var container = document.getElementById(type + section);
    if (!container) {
      return;
    }

    [].forEach.call(content.split('|'), function forEach(elem, index, obj) {
      newContent += '<p>' + elem + '</p>';

      if (split && obj.length - 1 > index) {
        newContent += split;
      }
    });

    container.innerHTML = newContent;
  }

  /**
   * Update the entire contents of a box.
   **/
  function updateTemplate(type, productList) {
    var cont = document.getElementById(type);

    cont.setAttribute('product-id', productList.product_id);

    cont.setAttribute('xitiTag', productList.xitiTagName);

    // Update title
    updateSection(type, '-title', productList.title);

    // Update subtitle
    updateSection(type, '-subtitle', productList.subtitle);

    // Update body
    updateSection(type, '-body', productList.body, '<div class="aiUpselling-bodySplit">+</div>');

    // Update price
    updateSection(type, '-price', productList.price);

    // Detect labels
    if (productList.hasLabels) {
      showLabels(type, type + '-body');
    }

    // Clean old class
    cont.className = cont.className.replace(/hasGallery/g, '');

    // Detect gallery
    if (productList.hasGallery) {
      cont.className += ' hasGallery';
    }
  }

  function detectImages() {
    var images = document.getElementsByClassName('ai_image');

    if (images.length > 0) {
      enableGalleryCombos();
    } else {
      disableGalleryCombos();
    }
  }

  function detectUploadedImages() {
    var imageUploader = document.querySelector('#image_upload_button');

    imageUploader.addEventListener('change', function() {
      detectImages();
    }, false);
  }

  function detectDeletedImages() {
    var uploadedContainer = document.getElementById('uploaded_images');

    uploadedContainer.addEventListener('click', function() {
      setTimeout(function() {
        detectImages();
      }, 100);
    }, true);
  }

  function enableGalleryCombos() {
    var originalClass;
    var galleries = document.getElementsByClassName('hasGallery');

    cleanDisabled();

    [].forEach.call(galleries, function(box) {
      originalClass = box.className;

      box.className = originalClass.replace(/__disabled/g, ''); // eslint-disable-line
    });

    hideGalleryMessage();
  }

  function disableGalleryCombos() {
    var box;
    var foundId;
    var anyChosen;
    var isDisabled;

    var ids = [];
    var nope = true;
    var galleries = document.getElementsByClassName('hasGallery');

    cleanDisabled();

    [].forEach.call(galleries, function(_box) {
      ids.push(_box.querySelector('.aiUpselling-boxTitle p').textContent);
      if (_box.className.indexOf('__disabled') === -1) {
        _box.className += ' __disabled'; // eslint-disable-line
      }
    });

    // This is so shitty, please fix
    anyChosen = document.getElementsByClassName('aiRadioButton');

    [].forEach.call(anyChosen, function(elem) {
      if (elem.className.indexOf('__selected') > 0) {
        foundId = elem.className.split('__')[1];

        box = document.getElementById(foundId.replace(/\s+/g, ''));

        if (box) {
          isDisabled = box.className.indexOf('__disabled');
        } else {
          isDisabled = undefined;
        }

        nope = false;
      }
    });

    if (isDisabled > -1 && !nope) {
      chooseDefault();
    }

    showGalleryMessage();
  }

  function chooseDefault() {
    var elem = document.getElementById('basic');

    setLabel('');
    setProduct('');

    chooseBox.call(elem);
  }

  function cleanDisabled() {
    var disabled = document.getElementsByClassName('__disabled');

    [].forEach.call(disabled, function(box) {
      box.className = box.className.replace(/__disabled/g, ''); // eslint-disable-line
    });
  }

  function hideGalleryMessage() {
    var msg = document.getElementsByClassName('aiUpselling-message')[0];

    if (msg.className.indexOf('__hide') < 0) {
      msg.className += ' __hide';
    }
  }

  function showGalleryMessage() {
    var msg = document.getElementsByClassName('aiUpselling-message')[0];
    var currentClass = msg.className;
    msg.className = currentClass.replace(' __hide', '');
  }

  function createLabel(text, id) {
    var div = document.createElement('div');
    div.className = 'aiUpselling-tag';
    div.innerHTML = text;
    div.setAttribute('label-id', id);

    return div;
  }

  function showLabels(type, cont) {
    var i;
    var tagId;
    var title;
    var container;
    var currentLabel;

    var elements = [];
    var body = document.getElementById(cont);

    container = document.createElement('div');
    container.className = 'aiUpselling-tagContainer';
    container.id = type + '-labels';

    title = document.createElement('h5');
    title.className = 'aiUpselling-tagTitle';
    title.innerHTML = 'Elige tu Etiqueta';

    elements.push(title);

    /* Labels */
    for (tagId in upsellingProductsLabels) { // eslint-disable-line
      currentLabel = createLabel(upsellingProductsLabels[tagId], tagId);
      elements.push(currentLabel);
    }

    for (i = 0; i < elements.length; i++) {
      container.appendChild(elements[i]);
    }

    body.appendChild(container);
    initLabelSelection(type, body);
  }

  function initLabelSelection(type, cont) {
    cont.addEventListener('click', function(e) {
      chooseTag(e, type);
    }, false);
  }

  function chooseTag(e, type) {
    var labelId;
    var elem = e.target;

    var isDisabled = document.getElementById(type).className.indexOf('__disabled') > -1;

    if (elem.className === 'aiUpselling-tag' && !isDisabled) {
      clearLabels();

      elem.className = 'aiUpselling-tag __' + type;

      labelId = elem.attributes['label-id'].value;
      setLabel(labelId);
    }
  }

  function clearLabels() {
    var labels = document.getElementsByClassName('aiUpselling-tag');

    [].forEach.call(labels, function(elem) {
      elem.className = 'aiUpselling-tag'; // eslint-disable-line
    });
  }

  function clearRadioButtons(box) {
    if (box.indexOf('selected') > -1) {
      return false;
    }

    var radioButtons = document.querySelectorAll('.aiRadioButton');

    // Clear radio buttons
    [].forEach.call(radioButtons, function(radio) {
      radio.className = radio.className.replace(/ __selected/g, ''); // eslint-disable-line
    });
  }

  // Listener function which expand boxes and removes itself (boxes get expanded just once)
  function expandAndRemove() {
    expandBoxes(boxes);
    listenerHandling('remove', boxes, expandAndRemove);
  }

  function initialCategory() {
    var currentCat;
    var currentCatElm = document.getElementById('category_group');

    if (currentCatElm.value) {
      currentCat = currentCatElm.value;
    }

    // Update template
    setTimeout(function() {
      setCurrentProducts(currentCat, IS_PRO_FOR);
    }, 200);
  }

  (function init() {
    if (!checkData()) {
      return false;
    }

    // Set event listener on boxes
    listenerHandling('add', boxes, expandAndRemove);
    listenerHandling('add', boxes, chooseBox);

    initialCategory();

    // Set change listener on select
    categoryListener();
    detectUploadedImages();
    detectDeletedImages();

    return true;
  })();
};

