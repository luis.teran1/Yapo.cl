var Yapo = Yapo || {};

Yapo.informativeNotification = (function main() {
  'use strict';

  function addCloseAction() {
    var elements = document.getElementsByClassName('informativeNotification-closeTrigger');

    function hideParent(element) {
      /*
       * <div class="informativeNotification">              <-- To here.
       *  <div class="informativeNotification-close">
       *    <i class="icon-yapo icon-close-message"></i>    <-- From here.
       *  </div>
       * ...
       * </div>
      */
      element.parentElement.parentElement.className += ' __hide';
    }

    function callHide() {
      hideParent(this);
    }

    if (elements.length < 1) {
      return false;
    }

    for (var i = 0; i < elements.length; i++) {
      elements[i].addEventListener('click', callHide, false);
    }
  }

  return addCloseAction;
})();

