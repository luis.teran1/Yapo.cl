function Pop(message, links, callback){
	var buttons = '';
	var buttonLeft = '';
	var buttonRight = '';
	var buttonClass = '';

	if(links != undefined){
		if(links[0] != undefined){
			var hrefLeft = '#';
			if(links[0].url != undefined){
				hrefLeft = links[0].url;
			}
			buttonLeft = '<a href = "' + hrefLeft + '" class="btn btn-0 btn-primary">' + links[0].text + '</a>';
		}
		if(links[1] != undefined){
			var hrefRight = '#';
			if(links[1].url != undefined){
				hrefRight = links[1].url;
			}
			buttonRight = '<a href = "' + hrefRight + '" class="btn btn-1  btn-primary">' + links[1].text + '</a>';
		}
	}else{
		buttonLeft = '<a href="javascript:void(0)" onclick="$.pgwModal(\'close\');" class="btn btn-primary">OK</a>';
	}
	
	buttons = '<div class="pm-buttons">' + buttonLeft + buttonRight + '</div>';

	var finalContent = '<div><div class="pm-message">' + message + '</div>' + buttons + '</div>';
	$.pgwModal({
		content: finalContent,
		closable: false
	});

	if(callback != undefined){
		$(document).bind('PgwModal::Close', function() {
	    	callback();
		});
	}

	//bind callbacks for buttons
	if(links != undefined){
		bindCallbackButton(links[0], '.btn-0');
		bindCallbackButton(links[1], '.btn-1');	
	}

}

function bindCallbackButton(link, selector){
	
	if(link == undefined){
		return;
	}

	if(link.click != undefined){
		$('.pm-buttons ' + selector).click( function(e){
			e.preventDefault();
			link.click();
			$.pgwModal('close');
		});
	}else if(link.url == undefined){
		$('.pm-buttons ' + selector).click( function(e){
			e.preventDefault();
			$.pgwModal('close');
		});
	}
}
