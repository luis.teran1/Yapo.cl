document.addEventListener('DOMContentLoaded', init);

function init() {
  categoryAction();
  setModelYear();
  plateEvents();
}

/**
  *
  * @name categoryAction
  * @public
  * @description
  *
  * Add event change to category select.
  *
  *
  */
function categoryAction() {
  var catGroup = document.getElementById('category_group');
  if (catGroup == undefined) {
    return;
  }

  catGroup.addEventListener('change', function passEvent() {

    if (catGroup.value === '2020') {
      setModelYear();
    }
  }, false);
}

/**
  *
  * @name setModelYear
  * @public
  * @description
  *
  * Add event  change to Year model select.
  * enable/disable is new checkbox
  *
  *
  */
function setModelYear() {
  var categoryContent = document.getElementById('category_contents');

  if (categoryContent) {
    categoryContent.addEventListener('change', function(evnt) {
      plateEvents();
    }, true);
  }
}

/**
  *
  * @name plateEvents
  * @public
  * @description
  *
  * Validate plate number
  *
  *
  */
function plateEvents() {
  var plate = document.getElementById('plates');

  if (plate) {
    plate.addEventListener('keyup', eventPlateValid, true);
    plate.addEventListener('blur', eventPlateValid, true);
  }
}

/**
  *
  * @name eventPlateValid
  * @public
  * @description
  *
  * Set validate messages to validate plates
  *
  *
  */
function eventPlateValid() {
  if (getPlateValue() === '') {
    return;
  }
  var regex = /^[A-Za-z]{2}([A-Za-z]{2}|[0-9]{2})[0-9]{2}$/;
  var regexMoto = /^[A-Za-z]{2}([A-Za-z]{1}0?|[0-9]{1})[0-9]{2}$/;
  var error = document.getElementById('err_plates');
  var catGroupVal = document.getElementById('category_group').value;
  var valid = (catGroupVal === "2060")? regexMoto.test(getPlateValue()) : regex.test(getPlateValue());
  var errorMsg = 'La patente ingresada no corresponde a';

  switch (catGroupVal) {
  case "2060":
    errorMsg = errorMsg +' una motocicleta';
    break;
  case "2040":
    errorMsg = errorMsg +' un bus o camión';
    break;
  default:
    errorMsg = errorMsg +' un automóvil';
  }

  if (valid) {
    error.classList.remove('error');
    error.classList.add('success');
    error.innerHTML = 'Ok!';
  } else {
    error.classList.remove('success');
    error.classList.add('error');
    error.innerHTML = errorMsg;
  }
}

/**
  *
  * @name getPlateValue
  * @public
  * @description
  *
  * Get plate value
  *
  *
  */
function getPlateValue() {
  return document.getElementById('plates').value;
}
