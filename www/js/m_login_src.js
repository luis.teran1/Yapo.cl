$(document).ready( function(){

	toggleCompanyFields();
	adjustSelectBackground();
	
	$(".icheck-radio").iCheck({
		radioClass: 'iradio_minimal-blue',
		increaseArea: '20%' // optional
	});

	$(".icheck").iCheck({
		checkboxClass: 'icheckbox_minimal-blue',
		increaseArea: '20%' // optional
	});

	//Detect change on type of user
	$('input[name=is_company]').on( 'ifChanged', function(){
		toggleCompanyFields();
	});

	// Form validations
	$("#account_name").on("keyup blur", function(){
		validateNonEmpty($(this));
		validateGE(2, $(this));
	});

	$("#account_rut").Rut({
		on_success: function(){ showSuccess($("#account_rut")); },
		on_error: function(){ hideSuccess($("#account_rut")); }
	});

	$("#account_phone").on("keyup blur", function(){
		validateNonEmpty($(this));
		validateGE(6, $(this));
	});

	$("#account_email").on("keyup blur", function(){
		validateNonEmpty($(this));
		validateEmail($(this));
	});

	$("#account_password").on("keyup blur", function(){
		validateNonEmpty($(this));
	});

	$("#account_password_verify").on("keyup blur", function(){
		validateNonEmpty($(this));
		validateEquals($("#account_password"), $(this));
	});

	adjustEditTitle();

	window.addEventListener('orientationchange', adjustSelectBackground);
	window.addEventListener("deviceorientation", adjustSelectBackground, true);

});

function validateNonEmpty(field){
	if(field.val().length){
		showSuccess(field);
	}else{
		hideSuccess(field);
	}
}

function validateGE(number, field){
	if(field.val().length >= number){
		showSuccess(field);
	}else{
		hideSuccess(field);
	}
}

function validateEquals(fieldToCompare, field){
	if(field.val() == fieldToCompare.val() && fieldToCompare.val().length > 0){
		showSuccess(field);
	}else{
		hideSuccess(field);
	}
}

function validateEmail(field){
	var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	if(re.test(field.val())){
		showSuccess(field);
	}else{
		hideSuccess(field);
	}
}

function showSuccess(field){
	field.parent().find(".error").hide();
	field.parent().find(".icon-check-circled").show();
}

function hideSuccess(field){
	field.parent().find(".icon-check-circled").hide();
}

function toggleCompanyFields(){
	if($("#is_company_c").is(":checked")){
		$(".company-fields").show();
	}else{
		$(".company-fields").hide();
	}
}

function adjustEditTitle(){
	var w = $(".edit-account-title").width();
	if(w > 0){
		$(".edit-account-title").css("margin-left", "-" + (w/2) + "px");
	}
}

function adjustSelectBackground(){
	var w = $(".arrow-down").width();
	var x = w - 200;
	$(".arrow-down").css("background-position", x + 'px -397px');
}
