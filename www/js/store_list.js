$(document).ready(function() {

	adjustStoreTitles();

	if(Array.prototype.indexOf) {
		$('#search_region').selectize({
			allowEmptyOption: true,
			create: false
		});

		$('#categories').selectize({
			allowEmptyOption: true,
			create: false,
			render: {
				option: function(data, escape) {
					var label = data['text'];
					if (label.indexOf("--") == 0) {
						return "<div class='option optgroup-custom'>" + escape(data['text']).replace(/\s?--\s?/g, '') + "</div>";
					} else {
						return "<div class='option'>" + escape(data['text']) + "</div>";
					}
				}
			},
			onChange: function(){
				$("#search_ads_form").submit();
			}
		});
	}

	formatStoreDescription();

	$(".detail-address > span").dotdotdot();

	$('#categories').next().addClass('categories');

	if(Array.prototype.indexOf) {
		$('#order').selectize({
			allowEmptyOption: true,
			create: false,
			onChange: function(){
				$("#search_ads_form").submit();
			}
		});
	}

	$('#order').next().addClass('order');

	shareOnClick('#share_facebook', '#share_twitter');

	trimUrl();

	//Fix select text key
	$('.selectize-input input').prop('disabled', true);

	$("img.lazy").lazyload();
	
});

function adjustStoreTitles(){
	$(".item-stores h3").each( function(){
		var n = $(this).text().length;
		if(n > 80){
			$(this).css("font-size", "1em");
		}else if(n > 50){
			$(this).css("font-size", "1.15em");
		}else if(n > 30){
			$(this).css("font-size", "1.3em");
		}
	});
}

function trimUrl(){
	var $url =  $("#store_website_url");
	if($url.length > 0 && $url.html().length > 38){
		var t = truncate($url.text(), 38, '...');
		$url.html(t);
	}
}

function truncate(fullStr, strLen, separator) {
    if (fullStr.length <= strLen) return fullStr;
    separator = separator || '...';
    var sepLen = separator.length,
        charsToShow = strLen - sepLen,
        frontChars = Math.ceil(charsToShow/2),
        backChars = Math.floor(charsToShow/2);

    return fullStr.substr(0, frontChars) + 
           separator + 
           fullStr.substr(fullStr.length - backChars);
};


function formatStoreDescription(){
	//get initial height of element
	var initheight = $(".store-description").outerHeight();
	$(".store-description").dotdotdot({
		height: 200,
		ellipsis: ''
	});
	var finalHeight = $(".store-description").outerHeight();
	if(initheight > finalHeight){
		 $(".store-description").append("<div class='ellipsis'>...</div>");
	}
}
