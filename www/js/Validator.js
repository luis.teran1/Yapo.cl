function FormatString(format){
  this.str_format = format;
}

FormatString.prototype.format = function(obj) {
  return this.str_format.replace(/\{(\w+)\}/g, function(str, meth){
    return obj[meth]
  })
}

function Validator(data) {
  //if(Validator.list == null)
  //  Validator.list = [];

  //Validator.list.push(this);
  var funcs = [];

  if(typeof(data) != typeof({}))
    throw "Data is not a hash."

  var default_validation_error = function(arr_errors){
    for(var i = 0; i < arr_errors.length; i++) {
      var err = arr_errors[i];
    }
  }
  var default_validation_success = function(){
  }
  if(data.on_error != null) {
    default_validation_error = data.on_error;
  }

  if(data.on_success != null) {
    default_validation_success = data.on_success;
  }

  if(data.error_msgs) {
    for(var i = 0; i < data.error_msgs.length; i++) {
      var err = data.error_msgs[i];
      new ValidatorError(err.cod, err.format);
    }
  }

  if(data.form != null) {
    if(data.validations != null && typeof(data.validations) == typeof({})) {
      for(var element = 0; element < data.validations.length; element++) {
        var selector = data.validations[element].selector;
        for(event_name in data.validations[element]) {
          if(event_name == "selector") continue;
          var action_array = [];
          if(typeof(data.validations[element][event_name]) == typeof([]) && data.validations[element][event_name].length != null) {
            action_array = data.validations[element][event_name];
          } else {
            action_array.push(data.validations[element][event_name]);
          }
          var func_array = [];
          for(var i = 0; i < action_array.length; i++){
            if(action_array[i].validator != null) {
              var obj_element;
              if(selector == "__this__") {
                obj_element = $(data.form);
              } else {
                obj_element = $(data.form).find(selector);
              }

              func_array.push(
                [
                  Validator.string2function(action_array[i].validator),
                  action_array[i].args,
                  action_array[i].on_error,
                  action_array[i].on_success
                ]
              );
            }
          }
          obj_element.each(function (){
            this.func_array = func_array;
          });
          obj_element[event_name](function(){
            var func_array = this.func_array;
            var response = [];
            for(var i = 0; i < func_array.length; i++) {
              var resp = func_array[i][0].call(this, func_array[i][1]);
              for(var j = 0; j < resp.length; j++) {
                response.push(resp[j]);
              }
              if(resp.length > 0) {
                if(func_array[i][2] != null) {
                  var on_error = func_array[i][2];
                  on_error = Validator.string2function(on_error);
                  on_error.call(this, resp);
                }
              } else {
                if(func_array[i][3] != null) {
                  var on_success = func_array[i][3];
                  on_success = Validator.string2function(on_success);
                  on_success.call(this, resp);
                }
              }
            }
            if(response.length > 0) {
              var on_error = default_validation_error;
              on_error = Validator.string2function(on_error);
              on_error.call(this, response);
            } else {
              var on_success = default_validation_success;
              on_success = Validator.string2function(on_success);
              on_success.call(this, response);
            }
          });
        }
      }
    }
  }
}

Validator.string2function = function(str) {
  if(typeof(str) != typeof(function(){})) {
    if(/^[\w.]+$/.test(str)) {
      return eval(str);
    }
  }
  return str;
}

Validator.validate_string = function(args) {
  args = args ? args : [];
  var min_size = args[0];
  var code_small = args[1] ? args[1] : "STR_SMALLER_THAN_SIZE_ERROR";
  var max_size = args[2];
  var code_big = args[3] ? args[3] : "STR_BIGGER_THAN_SIZE_ERROR";
  var error = [];
  var value = $(this).val();
  if(min_size != null && value.length < min_size) {
    error.push({err_code: code_small, data: {val: value, min_size: min_size, max_size: max_size}});
  }
  if(max_size != null && value.length > max_size) {
    error.push({err_code: code_big, data: {val: value, min_size: min_size, max_size: max_size}});
  }
  return error;
}

Validator.validate_int = function(args) {
  var only_positive = args[0];
  var error = [];
  if(!/^[+-]?\d*$/.test(this.value)) {
    error.push({err_code: "INT_PARSE_ERROR", data: {val: this.value}});
  }
  else if(only_positive != null && this.value > 0) {
    error.push({err_code: "INT_POSITIVE_ERROR", data: {val: this.value}});
  }
  return error;
}

Validator.validate_selected = function(args) {
  var unselected_value = args && args[0] ? args[0] : "";
  var error = [];
  var code = args && args[1] ? args[1] : "SELECTED_ERROR";

  var value = $(this).val();
  if(unselected_value == value) {
    error.push({err_code: code, data: {}});
  }
  return error;
}

Validator.validate_regex = function(args) {
  var error = [];
  var regex = new RegExp(args[0]);
  var code  = args[1];

  var value = $(this).val();

  if (args[2] && args[2] == 'without_decimals') {
    value = value.replace(/\./g,'');
  }

  if (!regex.test(value)) {
    error.push({err_code: code, data: {val: value}});
  }
  return error;
}

Validator.validate_not_empty = function(args) {
  var error = [];
  var code = args && args[0] ? args[0] : "NOT_EMPTY_ERROR";

  var value = $(this).val();
  if("" == value) {
    error.push({err_code: code, data: {}});
  }
  return error;
}

Validator.validate_not_empty_inmo = function(args) {
  var error = [];
  var code = args && args[0] ? args[0] : "NOT_EMPTY_ERROR";
  var code_min = args && args[2] ? args[2] : "NOT_EMPTY_ERROR";
  var regex = new RegExp(args[1]);
  var value = $(this).val().replace(/\./g,'');
  var reqPrice = $('#category_group').children(":selected").data('reqprice');

  if (reqPrice && value == '') {
    error.push({err_code: code, data: {}});
  } else if (reqPrice && !regex.test(value)) {
    error.push({err_code: code_min, data: {}});
  }

  return error;
}

Validator.validate_equals = function (data){
  var error = [];
  var base = $(data[0]).val();
  var val = $(this).val();
  var code = data && data[1] ? data[1] : "EQUALS_ERROR";
  if(base != val) {
    error.push({err_code: code, data: {val: val, base: base}});
  }
  return error;
}

Validator.validate_any_checked = function (data){
  var error = [];
  var fieldsSelector = data[0];
  var code = data && data[1] ? data[1] : "ANY_CHECKED_ERROR";
  var anyChecked = false;
  $(fieldsSelector).each(function(){
    anyChecked = anyChecked || this.checked;
  });
  if(!anyChecked) {
    error.push({err_code: code, data: {}});
  }
  return error;
}

Validator.validate_nothing = function (data){
  return [];
}

/*
 * @name Validate Year
 * @public
 *
 * @Description
 * Validate if year is bigger than 999 and less that
 * current year, validate four digits
 *
 * @param {Object} args - Object from bconf vars on jsvalidators
 *
 */

Validator.validate_year = function (args) {
  var error = [];
  var code = args && args[1] ? args[1] : "NOT_EMPTY_ERROR";
  var value = $(args[0]).val();
  var yearNow = new Date().getFullYear();

  if (value != '' && (value <= 999 || value > yearNow)) {
    error.push({err_code: code, data: {}});
  }

  return error;
}

function ValidatorError(err_code, format) {
  if(ValidatorError.cache == null)
    ValidatorError.cache = {};

  ValidatorError.cache[err_code] = this;
  this.msg_format = new FormatString(format);
}

ValidatorError.createErrorMsg = function(data){
  return ValidatorError.cache[data.err_code].msg_format.format(data.data);
};
