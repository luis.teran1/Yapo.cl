/* globals numberLimit, Yapo, real_format_price, uf_conversion_factor, pesos2uf,
  real_state_low_price */
$(document).ready(function onReady() {
  function preventDefault(e) {
    e.preventDefault();
  }

  function showModal(e) {
    preventDefault(e);
  }

  $('.icon-help.more-info').click(function onIconClick(e) {
    e.preventDefault();
    $.pgwModal({
      target: '#mas-info',
      maxWidth: 800
    });
  });

  $('.icon-help.more-info').click(showModal);

  $('#email_confirm').on('paste', preventDefault);

  numberLimit($('#address_number'), 5);
  setCurrencyValue();
  limitJobCategorySelection();

  function removeWhite() {
    $('.pm-close .pm-icon').removeClass('white');
  }

  $(document).bind('PgwModal::Close', removeWhite);
  $(document).on('change', '.currency-type', setCurrencyValue);
  $(document).on('keyup', '#uf_price', showUFConversion);
  $(document).on('keypress', '#uf_price', Yapo.validateUFPrice);
  $(document).on('blur', '#uf_price', Yapo.formatUFPrice);
  $(document).on('focus', '#uf_price', Yapo.deleteUFPriceFormat);
  $(document).on('keydown', '#price', Yapo.validatePrice);
  $(document).on('blur', '#price', Yapo.validatePrice);
  $(document).on('change', '.jobCategory', limitJobCategorySelection);

  var category_group = document.getElementById('category_group');
  if (category_group) {
    category_group.addEventListener('change', function(evnt) {
      HandleCommuneAdvice(evnt);
    });
  }

  function HandleCommuneAdvice(evnt){
    var advice_div = document.getElementById('advicecommune');
    if (advice_div) {
      if (evnt.target[evnt.target.selectedIndex].dataset.showcommunemsg) {
        advice_div.style.display='block';
      } else {
        advice_div.style.display='none';
      }
    }
  }

  // Fix: Clear Address name and address number ie8
  $('.ie8 #submit_create_now').click(function onCreateAd() {
    var address = $('#address');
    var addressNum = $('#address_number');

    if (address.val() === 'Ej. Nueva Providencia') {
      address.val('');
    }

    if (addressNum.val() === 'Ej. 1111') {
      addressNum.val('');
    }
  });

  // iCheck
  $('input.icheck').iCheck({
    checkboxClass: 'icheckbox_minimal-blue',
    increaseArea: '20%' // optional
  });

  if (typeof Yapo !== 'undefined') {
    if (typeof Yapo.hectares === 'function') {
      Yapo.hectares();
    }

    if (typeof Yapo.aiUpselling === 'function') {
      Yapo.aiUpselling();
    }
  }
});

function customEventCategory() {
  var event = new CustomEvent("selectCategory");

  this.dispatchEvent(event); 
} 

function showFootwearSize() { // eslint-disable-line no-unused-vars
  var value = $('#footwear_gender').val();
  if (value !== '') {
    $('#footwear_size').prop('disabled', false);
  }
  $('#footwear_gender').change(function onChangeFootwear() {
    if (value === '') {
      $('#footwear_size').prop('disabled', true);
      $('#footwear_size').val('');
    }
  });
}

function resetFootwearGender() { // eslint-disable-line no-unused-vars
  $('#footwear_gender').val('');
}

function setCurrencyValue() {
  var $currency = $('.currency-type:checked');
  var $price = $('#price');
  var $uf = $('#uf_price');

  if ($currency.hasClass('__uf')) {
    $('#warning_container').hide();
    $price.hide().attr('disabled', 'disabled');
    $uf.show().removeAttr('disabled');
    showUFConversion();
  } else {
    $uf.hide().attr('disabled', 'disabled');
    $price.show().removeAttr('disabled');
    real_state_low_price();
  }

  toggleArrowPriceMessage();
}

function toggleArrowPriceMessage() {
  var $message = $('#warning_container_red_arrow');
  var $currency = $('.currency-type:checked');

  if ($currency.length) {
    if ($message.data('currency') === $currency.attr('id')) {
      $message.show();
    } else {
      $message.hide();
    }
  }
}

function clearCurrencySelection() { // eslint-disable-line no-unused-vars
  $('.currency-type').prop('checked', false);
  $('#currency_peso').click();
}

function showUFConversion () {
  var $label = $('#pesos2uf');
  var ufPrice = $('#uf_price')
    .val()
    .replace(/\./, '');
  var price = Yapo.convertUFToPesos(uf_conversion_factor, ufPrice);

  if (price) {
    $label.text('($ ' + Yapo.setMoneyFormat(price) + ')');
  } else {
    $label.text('');
  }
}

/**
 * @name limitJobCategorySelection
 * @description
 *
 * Prevents the sub category selection when the limit is reached.
 *
 */
function limitJobCategorySelection() {
  var jobContainer = document.querySelector('.job_container');
  var CATEGORY_LIMIT;
  var categories;
  var selectedCount;
  var forEach = function (array, fn) {
    [].forEach.call(array, fn);
  };

  if (!jobContainer) {
    return;
  }

  CATEGORY_LIMIT = parseInt(jobContainer.dataset.limit, 10);
  categories = document.getElementsByClassName('jobCategory');
  selectedCount = document.querySelectorAll('.jobCategory:checked').length;

  if (selectedCount >= CATEGORY_LIMIT) {
    forEach(categories, function(el) {
      if (!el.checked) {
        el.setAttribute('disabled', true);
      }
    });
  } else {
    forEach(categories, function(el) {
      if (el.disabled) {
        el.removeAttribute('disabled');
      }
    });
  }
}

/**
 * @name clearJobCategorySelection
 * @description
 *
 * Cleans the sub category selection after each category selection.
 *
 */
function clearJobCategorySelection() {
  var jobContainer = document.querySelector('.job_container');
  var categories;

  if (!jobContainer) {
    return;
  }

  categories = document.getElementsByClassName('jobCategory');
  [].forEach.call(categories, function(el) {
    el.removeAttribute('disabled');
    el.checked = false;
  });
}
