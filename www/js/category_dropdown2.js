/*globals category_list, get_settings, typeList, register_events, category_settings, jsevents, setChecked,
 show_sub_category, split_setting, js_info, label_settings */

/* exported show_category_types, show_sub_category, show_hide_private_company, selectType*/

function form_key_lookup(key, sel_id) {
  sel_id = sel_id ? sel_id : '';

  if (key == 'type') {
    var selectType = document.getElementById('type' + sel_id);

    if (selectType && selectType.tagName != 'SELECT') {/* IE fix */
      selectType = false;
    }

    if (selectType) {
      return selectType.value;
    } else {
      var cont = document.getElementById('type_container' + sel_id);
      var inputs = cont.getElementsByTagName('INPUT');

      for (var idx = 0; idx < inputs.length; idx++) {
        if (inputs[idx].type == 'radio' && inputs[idx].checked) {
          return inputs[idx].value;
        }
      }

      return 's';
    }
  }

  var category_group = document.getElementById('category_group' + sel_id);
  var sub_cat = document.getElementById('sub_category' + sel_id);
  var cat_id_elem = document.getElementById('cat_id');
  var cat_id;

  if (cat_id_elem && cat_id_elem.value != 0) {
    cat_id = cat_id_elem.value; 
  } else if (sub_cat && sub_cat.style && sub_cat.style.display != 'none' && sub_cat.value != 0 && category_list[sub_cat.value].parent == category_group.value) {
    cat_id = sub_cat.value;
  } else {
    cat_id = category_group.value;
  }

  if (key == 'category') {
    return cat_id;
  }

  if (key == 'parent' && category_list[cat_id]) {
    return category_list[cat_id]['parent'];
  }

  if (key == 'has_store') {
    var store_row = document.getElementById('store_row');
    var res;

    if (store_row) {
      res = store_row.style.display != 'none';
    } else {
      res = document.getElementById('store_holder') != null;
    }

    if (res) {
      return 1;
    } else {
      return 0;
    }
  }

  if (key == 'company_ad') {
    var c_ad = document.getElementById('c_ad');
    if (c_ad && c_ad.checked) {
      return 1;
    }
    return 0;
  }

  if (key == 'create_account') {
    var createacc = document.getElementById('create_account');

    if (createacc && createacc.checked) {
      return 1;
    }
    return 0;
  }

  var elem = document.getElementById(key + sel_id);

  if (elem) {
    return elem.value;
  }
}

function show_category_types() {
  var t;
  var sel_id = this.getAttribute('data-sel_id');
  var selectType = document.getElementById('type' + sel_id);
  var selected;

  /* Don't display type radio buttons when editing an old category ad */
  if (selectType && selectType.type == 'hidden') {
    return 0;
  }

  if (selectType && selectType.tagName != 'SELECT') {
    selectType = false;
  }

  /* Get the selected type */
  if (selectType) {
    for (i = 0; i < selectType.options.length; i++) {
      if (selectType[i].selected) {
        selected = selectType[i].value;
      }
    }
  }

  var types = get_settings('types', form_key_lookup, category_settings, sel_id);
  var type = types.split(',');

  if (!selectType) {
    for (t in type) {
      var radio = document.getElementById('r' + type[t] + sel_id);
      if (radio && radio.checked) {
        selected = type[t];
        break;
      }
    }
  }

  /* Store new type options for this category */
  if (selectType) {
    selectType.options.length = 1;
    for (i = 0; i < type.length; i++) {
      selectType.options[i+1] = new Option(typeList[type[i]], type[i]);

      if (selected == type[i]) {
        selectType.options[i+1].selected = true;
      }
    }

  } else {
    /* Don't try to optimize this code, it wont work for older ie if you do! */
    var cont = document.getElementById('type_container' + sel_id);

    cont.innerHTML = '';

    if (type.length > 1) {
      var inner_html = ''; 
      var j = 0;

      for (var i in type) {
        t = type[i];

        if (inner_html != '') {
          if (i%2 != 0) {
            inner_html += '</div><div class="item_radio right">';
          } else {
            inner_html +='</div></div><div class="line mb10px"><div class="item_radio left">';
          }
        } else {
          inner_html = '<div id="types_tb"><div class="line mb10px"><div class="item_radio left">';
        }

        inner_html += '<input class="ipt_radio" name="type" value="' + t + '" ' + (j == 0? 'checked="checked"' : '') + ' id="r' + t + sel_id + '" type="radio" sel_id="' + sel_id + '"> <label class="ipt_label" for="r' + t + sel_id + '">' +typeList[t] + '</label>&nbsp;';
        j++;
      }

      inner_html += '</div></div></div>';
      cont.innerHTML = inner_html;
    }

    register_events(jsevents.ai, document);

    if (selected) { 
      setChecked('r' + selected + sel_id, true);
    }
  }
}

function set_category_changed() { // eslint-disable-line
  var sel_id = this.getAttribute('data-sel_id');
  var selectType = document.getElementById('type' + sel_id);

  if (document.getElementById('category_changed' + sel_id)) {
    var cat_id = form_key_lookup('category', sel_id);
    document.getElementById('category_changed' + sel_id).value = (cat_id != document.getElementById('original_category').value) ? '1' : '0';
  }
}

function show_sub_category(e, args) {
  var sel_id = this.getAttribute('data-sel_id');
  var category_group = document.getElementById('category_group' + sel_id);
  var sub_cat = document.getElementById('sub_category' + sel_id);

  var par_id = category_group.value;

  var sub_cats = Array();

  if (category_list[par_id] && category_list[par_id]['level'] > 0) {
    for (var c in category_list) {
      if (category_list[c]['parent'] == par_id) {
        sub_cats[sub_cats.length] = c;
      }
    }
  }

  if (sub_cats.length > 0) {
    sub_cat.options.length = 1;
    for (var i = 0; i < sub_cats.length; i++) {
      var cat_id = sub_cats[i];
      var name = category_list[cat_id]['name'];
      if (args && args.noprice) {
        var price = split_setting(get_settings('price', 
              function (key) {
                if (key == 'category')
                  return cat_id;
                if (key == 'parent' && category_list[cat_id])
                  return category_list[cat_id]['parent'];
              },
              category_settings));

        if (price)
          name += ' \xA0 ' + price.price + ' ' + js_info['MONETARY_UNIT'];
      }
      sub_cat.options[i + 1] = new Option(name, sub_cats[i]);
    }
    sub_cat.style.display = 'block';
    sub_cat.disabled = false;
  } else {
    sub_cat.style.display = 'none';
    sub_cat.disabled = true;
  }
}

function show_hide_private_company() {
  var rut_visibility = split_setting(get_settings('rut_control',form_key_lookup, category_settings));

  if (rut_visibility && rut_visibility.display && document.getElementById('rut_control') != null) {
    document.getElementById('rut_control').style.display=rut_visibility.display;
  }

  var business_name_visibility = split_setting(get_settings('business_name_control',form_key_lookup, category_settings));

  if (business_name_visibility && business_name_visibility.display && document.getElementById('business_name_control') != null) {
    document.getElementById('business_name_control').style.display=business_name_visibility.display;
  }

  var value = split_setting(get_settings('pdefault',form_key_lookup, category_settings));

  if (value && document.getElementById('pdefault') != undefined) {
    document.getElementById('pdefault').style.display=value.display;
  }

  var label = split_setting(get_settings('name', form_key_lookup, label_settings)),
    name_label_elemnt = document.getElementById('name_label');

  if (name_label_elemnt) {
    name_label_elemnt.innerHTML = label.label + ' *';
  }

  var is_pro = split_setting(get_settings('is_pro_for',form_key_lookup, category_settings));
  if (document.getElementById('pdefault') != undefined && document.getElementById('pdefault').getAttribute('data-is_pro_for') != '') {
    if (is_pro[1] == 1) {
      document.getElementById('pdefault').style.display='block';
    } else {
      document.getElementById('pdefault').style.display='none';
    }
  }
}

