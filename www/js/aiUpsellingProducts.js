var Yapo = Yapo || {};

Yapo.aiUpsellingProducts = (function () {

  /**
   * @name getRecursive
   * @description
   *
   * Recursively get keys from an array.
   * If all keys are found, returns corresponding value. Undefined otherwise
   *
   * @param {Array} array - List of values to select
   * @param {Array} keys - List of keys to search
   *
   * @returns {Object}
   */
  function getRecursive(array, keys) {
    if (!array || !keys) {
      return;
    }

    var node = array;
    for (var i = 0; i < keys.length; i++) {
      if (keys[i] in node) {
        node = node[keys[i]];
      } else {
        return;
      }
    }

    return node;
  }

  /**
   * @name getUpsellingCombos
   * @description
   *
   * Retrieve the corresponding upselling combos from the configuration
   *
   * @param {Object} upsellingProducts - Upselling product list by category alias
   * @param {Object} upsellingProductsCategories - Dictionary category id: alias. ie (7020: JOBS)
   * @param {String} categoryId - Selected category id
   * @param {String} isProFor - Separated list by command, that contains the categories
   *                            where user is pro.
   *
   * @returns {Object}
   */
  function getUpsellingCombos(paymentSettings, upsellingProducts, upsellingProductsCategories, categoryId, isProFor) {
    var currentCat = upsellingProductsCategories[categoryId];
    var regex = new RegExp('\\b'+categoryId+'\\b');
    var proForCategory = isProFor.toString().match(regex) ? '1' : '0';

    var currentContent = getRecursive(upsellingProducts, [currentCat, proForCategory])
                      || getRecursive(upsellingProducts, [currentCat, '*'])
                      || getRecursive(upsellingProducts.default, [proForCategory])
                      || getRecursive(upsellingProducts.default, ['*']);

    for (var type in currentContent) {
      var combo = currentContent[type];
      var settings = split_setting(get_settings(
        'product',
        function (key, data) { return data[key]; },
        paymentSettings,
        {product: combo.product_id, category: categoryId}
      ));
      combo.price = '$' + real_format_price(settings.price);
    }

    return currentContent;
  }

  return {
    getUpsellingCombos: getUpsellingCombos
  };
})();
