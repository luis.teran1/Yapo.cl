(function ($) {
    $.fn.extend({
		esRepositionDisable: function() {
			return this.each(function () {
				var $mask = $(this);
				var $img = $("img", $mask);
				$img.draggable('destroy');
			});
		},
        esRePosition: function (options) {
            var defaults = {
                input: "input"
            };
            options = $.extend(defaults, options);
            return this.each(function () {
                //Plugin options
                var o = options;

                //Main object
                var $mask = $(this);
                var $id = $mask.attr("id");
                var $input = $("#" + $id + "_" + $mask.data("input"));
                if ($input.length === 0) {
                    $input = $(o.input);
                }

                var $img = $("img", $mask);
                var $mask_w = $mask.width();
                var $mask_h = $mask.height();
                var $photo_org_w = $img.width();
                var $photo_org_h = $img.height();
                $img.width($mask_w);
                var $photo_w = $img.width();
                $img.draggable({
                    axis: "y",
                    cursor: "move",
                    drag: function (event, ui) {
						$photo_h = $(this).height();
						$max_margin = ($photo_h - $mask_h) * -1;
                        $topmarginpx = ui.position.top;
                        if ($topmarginpx > 0) {
                            ui.position.top = 0;
                        }
                        if ($topmarginpx < $max_margin) {
                            ui.position.top = $max_margin;
                        }
                        $topmarginpx = ui.position.top;
                        $topmarginpercent = $topmarginpx * 100 / $photo_h;
                        $input.val(ui.position.top * -1);
                    }
                });
				
				var topval = parseInt($input.val());
				if (topval > 0) {
					$img.css({'top': topval * -1});
				}
            });
        }
    });
})(jQuery);
