/* global adID, isMobileLayout, formatPrice */

'use strict';

(function bumpFrequency() {
  var daysSelected;
  var timeSelected;

  document.addEventListener('DOMContentLoaded', onReady);

  function onReady() {
    var frequencyUseNight = document.getElementById('bumpFrequency-useNight');
    var frequencyDays;
    var frequencyTime;

    document.removeEventListener('DOMContentLoaded', onReady);

    if (isMobileLayout) {
      frequencyDays = document.getElementById('frequencyDays');
      frequencyDays.addEventListener('change', onChangeDaysMobile);

      frequencyTime = document.getElementById('frequencyTime');
      frequencyTime.addEventListener('change', onChangeTimeMobile);
    } else {
      $('#frequencyDays').selectize({
        onChange: onChangeDays
      });

      $('#frequencyTime').selectize({
        onChange: onChangeTime
      });

      $('.selectize-input input').attr('readonly', 'readonly');
    }

    frequencyUseNight.addEventListener('change', requestResume);
  }

  function onChangeDays(value) {
    daysSelected = value;
    requestResume();
  }

  function onChangeDaysMobile(event) {
    onChangeDays(event.target.value);
  }

  function onChangeTime(value) {
    var dropdown = document.querySelector(
      '.bumpFrequency-selectorDay .selectize-dropdown-content');
    var HIDE_FIRST_OPTION = '__hideFirstOption';

    timeSelected = value;
    requestResume();

    if (value === '48') {
      dropdown.classList.add(HIDE_FIRST_OPTION);
    } else {
      dropdown.classList.remove(HIDE_FIRST_OPTION);
    }
  }

  function onChangeTimeMobile() {
    var option = $('#frequencyDays option:first-child + option');

    timeSelected = $(this).val();
    requestResume();

    if (timeSelected === '48') {
      option.hide();
      if (daysSelected === '1') {
        $('#frequencyDays').val('2');
      }
    } else {
      option.show();
    }
  }

  function requestResume() {
    if (!daysSelected || !timeSelected) {
      hideResumeAndSummaryBar();
      return;
    }

    showSpinner();

    $.ajax({
      url: '/payment_rest/autobump',
      data: {
        id: adID,
        frequency: timeSelected,
        num_days: daysSelected,
        use_night: getUseNight()
      },
      method: 'get',
      success: function onAjaxResponse(response) {
        if (response.status !== 'TRANS_OK') {
          onAjaxError();
        }

        if (!response.total_price || response.total_price === '0') {
          showNoValueResume();
          return;
        }

        hideNoValueResume();
        updateResume({
          totalBumps: response.total_bumps,
          dateEnd: new Date(response.date_end.replace(' ', 'T')),
          dateStart: new Date(response.date_start.replace(' ', 'T')),
          totalPrice: response.total_price
        });
      },
      error: onAjaxError
    });
  }

  function showSpinner() {
    var resume = document.getElementById('bumpFrequency-resume');
    resume.classList.add('__loading');
    resume.classList.remove('is-hidden');
  }

  function hideSpinner() {
    var resume = document.getElementById('bumpFrequency-resume');
    resume.classList.remove('is-hidden');
    resume.classList.remove('__loading');
  }

  function onAjaxError() {
    hideSpinner();
  }

  function showNoValueResume() {
    var body = document.getElementsByClassName('bumpFrequency-body')[0];
    var resume = document.getElementById('bumpFrequency-resume');
    var summaryBar = document.getElementsByClassName('summaryBar')[0];

    body.classList.remove('__expanded');
    summaryBar.classList.remove('__show');
    resume.classList.add('__noBump');
    hideSpinner();
  }

  function hideNoValueResume() {
    var resume = document.getElementById('bumpFrequency-resume');
    resume.classList.remove('__noBump');
  }

  function updateResume(data) {
    var time = document.getElementById('bumpFrequency-frequencyTime');
    var days = document.getElementById('bumpFrequency-frequencyDays');
    var date = document.getElementById('bumpFrequency-date');
    var times = document.getElementById('bumpFrequency-times');
    var timesText = document.getElementById('bumpFrequency-timesText');
    var totalAmount = document.getElementById('total-amount');

    var body = document.getElementsByClassName('bumpFrequency-body')[0];
    var summaryBar = document.getElementsByClassName('summaryBar')[0];
    var buyLink = document.getElementById('checkout');

    var urlData = {
      prod: 10,
      id: adID,
      ab_freq: timeSelected,
      ab_num_days: daysSelected,
      ab_use_night: getUseNight()
    };

    time.innerHTML = 'cada ' + timeSelected + ' hora' +
      (timeSelected > 1 ? 's' : '');

    days.innerHTML = daysSelected + ' d&iacute;a' + (daysSelected > 1 ? 's' : '');

    date.innerHTML = 'Desde el ' + getFormatDateString(data.dateStart) +
      ' al ' + getFormatDateString(data.dateEnd);
    times.innerHTML = data.totalBumps;
    timesText.innerHTML = data.totalBumps > 1 ? 'veces' : 'vez';
    totalAmount.innerHTML = '$' + formatPrice(data.totalPrice);

    hideSpinner();
    body.classList.add('__expanded');

    buyLink.setAttribute('href', '/pagos?' + objectToQueryString(urlData));

    summaryBar.classList.add('__show');
  }

  function getUseNight() {
    var nightCheckbox = document.getElementById('bumpFrequency-useNight');
    return nightCheckbox.checked ? 0 : 1;
  }

  function getFormatDateString(date) {
    return date.getDate() + '/' + (date.getMonth() + 1) + '/' +
      date.getFullYear();
  }

  function hideResumeAndSummaryBar() {
    var summaryBar = document.getElementsByClassName('summaryBar')[0];
    var resume = document.getElementById('bumpFrequency-resume');
    summaryBar.classList.remove('__show');
    resume.classList.add('is-hidden');
  }

  function objectToQueryString(data) {
    return [].map.call(Object.keys(data), function onEachKey(key) {
      return key + '=' + data[key];
    }).join('&');
  }
})();
