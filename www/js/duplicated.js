function duplicated(data) {
  var duplicated_submitting = false;

  // account user
  data.errors.has_account = data.errors.account_id > 0;
  data.errors.require_password = !(data.errors.require_password == 'f');

  // filling templates
  ['#duplicated_da_tmpl', '#activated_da_tmpl'].forEach(function(elementId){
    var h = $(elementId).text();
    var pagefn = doT.template(h);
    var html = pagefn(data.errors);
    var desktop = $('#content').length;
    if (desktop > 0){
      $('#content').after(html);
    } else {
      $('#newad_form').after(html);
    }
  });

  $('#noaccount').on('submit', function(evt) {
    evt.preventDefault();
    $("#activate_da").click();
  });

  // publish the ad anyway
  $('#publish_f').on('click', function(e){
    if (duplicated_submitting)
      return false;
    duplicated_submitting = true;
    $('input[name=skip_duplicated]').val('1');
    if ($('#newad_submit').size() == 1) {
      // fix for old ad insert (used for editions) and new ai
      // TODO: remove the changes of this commit when the new ai
      // is also used for editions
      submitting = false;
      $('#newad_submit').trigger("click");
    } else if ($('#newad_form').size() == 1) {
      submitting = false;
      $('#newad_form').submit();
    } else {
      $('#submit_create_now').trigger('click');
    }
  });

  /* if not account
  * disable the buttons
  * on password write
  * activate buttons
  * on button click
  *  check password
  * if pass ok then follow the regular flow already written
  * if password is not correct
  *   display the error message
  *   disable the buttons again
  *   clean the passwords fields
  * on password write should I clean the error message?
  * go to step 1
  */
  if ($('#noaccount').size() == 1) {

    function disableButtons() {
      $('#activate_and_bump, #activate_da').attr('disabled', 'disabled');
    }

    function validatePassword(ok) {
      var $form = $('#noaccount');
      $.ajax({
        url: $form.attr('action'),
        type: $form.attr('method'),
        data: $form.serialize(),
        dataType: 'json',
        success: function(response) {
          if (response.status == 'OK') {
            // if pass ok then follow the regular flow already written
            ok(response);
          } else {
            // if password is not correct
            //   display the error message
            $('#noaccount .input-row #err_form').html(response.message).show();
            //   disable the buttons again
            disableButtons();
            //   clean the passwords fields
            $('#noaccount input[name=passwd]').val('');
          }
        }
      });
    }

    // disable the buttons
    disableButtons();
    // on password write
    $('#noaccount input[name=passwd]').on('keydown', function(e) {
      // activate buttons
      $('#activate_and_bump, #activate_da').removeAttr('disabled', 'disabled');
    });

    // on button click
    $('#activate_and_bump').on('click', function(e) {
      if ($(this).attr('disabled') == 'disabled') {
        return;
      }
      e.preventDefault();
      function ok(response) {
        // TODO: validate that validations are being made in next page cof cof Jaime
        window.location = $('#activate_and_bump').attr('href');
      }
      //  check password
      validatePassword(ok);
    });

    // on button click
    $('#activate_da').on('click', function(e) {
      if ($(this).attr('disabled') == 'disabled') {
        return;
      }
      e.preventDefault();
      function ok(response) {
        // if pass ok then follow the regular flow already written
        var $element = $(e.currentTarget);
        activateAd($element);
      }
      //  check password
      validatePassword(ok);
    });
  } else {
    // if account, just do it
    // no password checking
    $('#activate_da').click(function(e) {
      activateAd($(e.currentTarget));
    });

    // TODO: this is necessary because the element is a button
    // discuss with oliver / marco if we can change this
    $('#activate_and_bump').click(function(e) {
      window.location = $(this).attr('href');
    });
  }

  $('#newad_form, #draft_wrap, .new-ai > h1').hide();
  $('#duplicated_da').removeClass('is-hidden');
  return true;
}


function activateAd($element){
  var data = {'list_id': $element.data('ad-id')};
  if ($('#noaccount').size() == 1) {
    data['passwd'] = $('#password_input').val();
  }

  $.ajax({
    type: 'PUT',
    url: '/ad/activate_ad',
    data: JSON.stringify(data),
    dataType: 'json'
  }).done(function(response) {
    if (response.status == 'ok') {
      activateAdDone();
    } else {
      activateAdFailed();
      errorHandles(response);
    }
  });
}

function errorHandles(response){
  if (response.status == 'error' && response.error == "ERROR_NO_SLOTS_AVAILABLE") {
    changeFailedMsg(response.error_msg);
    changeFailedTitle("Ups!");
    changeLink(response.pack_type);
  }
}

function changeLink(type) {
  $element = $(".pgwModal #see_das");
  if ($element.length == 0) {
    $element = $("#see_das");
  }
  $element.attr("href", $element.data('url-pack-' + type));
  $element.html($element.data("label-pack"));
}
