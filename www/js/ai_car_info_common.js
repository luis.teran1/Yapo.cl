function getCarValues() {
  const values = getCookie('car_info');

  return (values) ? JSON.parse(values) : JSON.parse(sessionStorage.getItem('car_info'));
}

function removeLocalCarValues(name, observers) {
  observers.forEach(function (observer) {
    observer.disconnect();
  });
  setCookie(name, '', -1, '/', '.yapo.cl');
  sessionStorage.removeItem(name);
}

function handleCarValues(carValues) {
  const carValuesFilter = {
    brand: carValues.brand.id,
    model: carValues.model.id,
    version: carValues.version.id,
    year: carValues.year
  };

  carValuesFilter.price = (carValues.type === 'with') ? carValues.value : '';

  return carValuesFilter;
}

function setCarParamSelect(idEl, value, time = 1) {
  setTimeout(function () {
    const select = document.getElementById(idEl);
    select.value = value;
    fireEvent(select, 'change'); 
  }, time);
}

function checkChangeElement(id, fn, pageObservers) {
  const targetNode = document.getElementById(id);
 
  const observerOptions = {
    childList: true,
    attributes: false,
    subtree: false
  }
  
  const observer = new MutationObserver(fn);
  observer.observe(targetNode, observerOptions);

  if (pageObservers) {
    pageObservers.push(observer);
  }
}

function fireEvent(element, event){
  if (document.createEventObject){
    // dispatch for IE
    var evt = document.createEventObject();
    return element.fireEvent('on' + event, evt)
  } else {
    // dispatch for firefox + others
    var evt = document.createEvent("HTMLEvents");
    evt.initEvent(event, true, true ); // event type,bubbling,cancelable
    return !element.dispatchEvent(evt);
  }
}

function docReady(fn) {
  if (document.readyState === "complete" || document.readyState === "interactive") {
    setTimeout(fn, 1);
  } else {
    document.addEventListener("DOMContentLoaded", fn);
  }
}

