/* global CartHandler */

var Yapo = Yapo || {};

/**
 *  @name deleteModal
 *  @description
 *
 *  Show modal when user want delete a unpaid Ad
 *
 *  @param {object} modalData Data text and width
 *  @param {boolean} msite Validate if is msite view
 *
 */

Yapo.deleteModal = (function ($) {
  function init(modalData, msite, event) {
    var modalContainer = document.querySelector('#delete-modal');

    if (!modalContainer) {
      return;
    }

    setContent(modalContainer, modalData, msite, event.currentTarget);
  }

  /**
   *  @name deleteUnpaid
   *  @description
   *
   *  Hide and remove element on html, and send id for server archive
   *
   *  @param {object} dataModal Data text
   *  @param {element} ad click element
   *  @param {string} device - option mobile o desktop
   *
 */
  function deleteUnpaid(dataModal, ad, device) {
    var adId = ad.getAttribute('data-id');
    var prodId = parseInt(ad.getAttribute('data-prod'), 10);
    var data = JSON.stringify({ad_id: adId});
    var buttons = $('.deleteModal button');
    var $row = '';

    if (device === 'desktop') {
      $row = $(ad).parents('.single-loop-das');
    } else {
      $row = $(ad).parents('.single-da');
    }

    buttons.prop('disabled', true);

    $.post('/accounts/delete_unpaid', data, function onSuccess() {
      $row.hide('slow', function () {
         $row.remove();
      });

      buttons.prop('disabled', false);

      showSuccess(device);

      if (CartHandler.isProductSelected(prodId, adId)) {
        CartHandler.remove(prodId, { list_id: adId });
      }
    }).fail(function onError() {
      buttons.prop('disabled', false);

      $('.deleteModal .deleteModal-headerText.success').html(dataModal.text.error);

      showSuccess(device);
    });
  }

  /**
   *  @name setContent
   *  @description
   *
   *  Add content to modal template and show for device
   *
   *  @param {element} modalContainer - html element of modal
   *  @param {object} data - modal info
   *  @param {boolean} msite - is on msite o not
   *  @param {element} clickElement - click element
   *
 */
  function setContent(modalContainer, data, msite, clickElement) {
    var tagPackIF = clickElement.getAttribute('data-tag');
    var element;

    var modalContent = [{
      selector: '.deleteModal-headerText',
      html: tagPackIF ? data.text.question_pack : data.text.question,
    }, {
      selector: '.deleteModal-headerText.success',
      html: data.text.success,
    }
    ];

    if (tagPackIF) {
      modalContent.push({
        selector: '.deleteModal-bodyText',
        html: tagPackIF ? data.text.remember_pack : '',
      });
    }

    modalContent.forEach(function (item) {
      element = modalContainer.querySelector(item.selector);

      if (element) {
        element.innerHTML = item.html;
      }
    });

    if (msite) {
      return showModalMobile(data, clickElement);
    }

    return showModalDesktop(data, clickElement);
  }

  /**
   *  @name showModalDesktop
   *  @description
   *
   *  display modal on desktop and add event listener buttons
   *
   *  @param {object} data - modal info
   *  @param {element} clickElement - click element
   *
 */
  function showModalDesktop(data, clickElement) {
    var tagPackIF = clickElement.getAttribute('data-tag');
    var linkDeleteIF = clickElement.getAttribute('data-link');
    var agreeBtn = '';
    var closeBtns = [];
    var ifModifier = tagPackIF ? ' __insertingFee' : '';

    $.pgwModal({
      target: '#delete-modal',
      mainClassName: 'pgwModal deleteModal' + ifModifier,
      maxWidth: parseInt(data.width, 10)
    });

    agreeBtn = document.querySelector('#pgwModal .deleteModal-button.__Agree');
    closeBtns = [
      document.querySelector('#pgwModal .deleteModal-button.__Disagree'),
      document.querySelector('#pgwModal .deleteModal-button.__Close')
    ];

    closeBtns.forEach(function (elem) {
      if (elem) {
        elem.addEventListener('click', function () {
          $.pgwModal('close');
        });
      }
    });

    if (agreeBtn) {
      agreeBtn.addEventListener('click', function(event) {
        if (tagPackIF) {
          window.location.href = linkDeleteIF;
        } else {
          deleteUnpaid(data, clickElement, 'desktop');
        }
      });
    }
  }

  /**
   *  @name showModalMobile
   *  @description
   *
   *  display modal on mobile and add event listener buttons
   *
   *  @param {object} data - modal info
   *  @param {element} clickElement - click element
   *
 */
  function showModalMobile(data, clickElement) {
    var tagPackIF = clickElement.getAttribute('data-tag');
    var linkDeleteIF = clickElement.getAttribute('data-link');
    var modal = document.getElementById('delete-modal');
    var agreeBtn = modal.querySelector('.deleteModal-button.__Agree');
    var closeBtns = [
      modal.querySelector('.deleteModal-button.__Disagree'),
      modal.querySelector('.deleteModal-button.__Close')
    ];
    var overlay = document.querySelector('.overlayMobile');

    modal.classList.add('__insertingFee');
    modal.classList.add('__show');
    overlay.classList.add('__show');

    closeBtns.forEach(function (elem) {
      if (elem) {
        elem.addEventListener('click', closeMobile);
      }
    });

    if (agreeBtn) {
      agreeBtn.addEventListener('click', function(event) {
        if (tagPackIF) {
          window.location.href = linkDeleteIF;
        } else {
          deleteUnpaid(data, clickElement, 'mobile');
        }
      });
    }
  }

  /**
   *  @name showSuccess
   *  @description
   *
   *  display second step whith success or error information of delete ad
   *
   *  @param {string} device where is running
   *
 */
  function showSuccess(device) {
    toggleModal(device, 'success');

    setTimeout(function () {
      if (device === 'desktop') {
        $.pgwModal('close');
      } else {
        document.getElementById('delete-modal').classList.remove('__show');

        setTimeout(function () {
          document.querySelector('.overlayMobile').classList.remove('__show');
          toggleModal(device, 'reset');
        }, 800);
      }
    }, 7000);
  }

  /**
   *  @name toggleModal
   *  @description
   *
   *  turn/on html classes for show steps
   *
   *  @param {string} device where is running
   *  @param {string} mode if on mobile need reset to step 1 on close
   *
 */
  function toggleModal(device, mode) {
    var modal = document.querySelector('#pgwModal');

    if (device === 'mobile') {
      modal = document.getElementById('delete-modal');
    }

    var questionText = modal.querySelector('.deleteModal-headerText');
    var bodyText = modal.querySelector('.deleteModal-bodyText');
    var successText = modal.querySelector('.deleteModal-headerText.success');
    var questionsBtns = modal.querySelector('.deleteModal-buttonsQuestion');
    var successBtns = modal.querySelector('.deleteModal-buttonsSuccess');

    if (mode === 'reset') {
      questionText.classList.remove('hide');
      bodyText.classList.remove('hide');
      questionsBtns.classList.remove('hide');
      successText.classList.add('hide');
      successBtns.classList.add('hide');
    } else {
      questionText.classList.add('hide');
      bodyText.classList.add('hide');
      questionsBtns.classList.add('hide');
      successText.classList.remove('hide');
      successBtns.classList.remove('hide');
    }
  }

  /**
   *  @name closeMobile
   *  @description
   *
   *  handle close Modal on mobile
   *
   *  @param {element} modal element of modal html
   *
 */
  function closeMobile() {
    var modal = document.getElementById('delete-modal');
    var closeBtns = [
      modal.querySelector('.deleteModal-button.__Disagree'),
      modal.querySelector('.deleteModal-button.__Close')
    ];

    modal.classList.remove('__show');

    setTimeout(function () {
      document.querySelector('.overlayMobile').classList.remove('__show');
      toggleModal('mobile', 'reset');

      modal.classList.remove('__insertingFee');

      closeBtns.forEach(function (elem) {
        if (elem) {
          elem.removeEventListener('click', closeMobile);
        }
      });
    }, 800);
  }


  return init;
})(jQuery);
