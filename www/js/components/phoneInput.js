/* global Yapo, regionArray, phoneErrorMessages */

Yapo.PhoneInput = (function() {
  'use strict';

  var areaCodes = [];


  /**
   *
   * @name phoneInput
   * @constructor
   * @description
   *
   * Set listener to radio buttons.
   *
   * @param {Function} onTypeChange - Callback called at end of phoneInput#onRadioChange.
   * @param {Boolean} isMsite - Flag to indicate if is used on the Msite.
   *
   */
  function phoneInput(onTypeChange, isMsite) {
    var radios = document.getElementsByClassName('phoneInput-type');
    var areaCode = document.getElementsByClassName('phoneInput-areaCode')[0];
    var number = document.getElementsByClassName('phoneInput-number')[0];
    var isMobile;

    if (!radios.length) {
      return false;
    }

    isMobile = isMobileNumber();
    areaCodes = getAllAreaCode();

    phoneInput.isMsite = isMsite;
    if (typeof onTypeChange === 'function') {
      phoneInput.onTypeChange = onTypeChange;
    }

    Array.prototype.forEach.call(radios, function onEachRadio(radio) {
      radio.addEventListener('change', onRadioChange);
    });

    if (!isMsite) {
      areaCode.addEventListener('keyup', onlyNumbers);
      number.addEventListener('keyup', onlyNumbers);
      areaCode.addEventListener('change', validate);
      number.addEventListener('change', validate);
      areaCode.addEventListener('blur', validate);
      number.addEventListener('blur', validate);
    } else {
      areaCode.addEventListener('change', function onChangeAreaCode(evt) {
        changePhoneLength(parseInt(evt.target.value, 10));
      });
    }

    toggleCountryCode(isMobile);
    toggleInputs(isMobile);
  }


  /**
   *
   * @name phoneInput#getAllAreaCode
   * @private
   * @description
   *
   * Read from array_v2.js and return all area codes in an array.
   *
   * @return {Array} An array with all area codes.
   *
   */
  function getAllAreaCode() {
    var codes = [];

    Object.keys(regionArray).forEach(function onEachRegion(regionKey) {
      var region = regionArray[regionKey];

      Object.keys(region.commune).forEach(function onEachCommune(communeKey) {
        var commune = region.commune[communeKey];
        var code = parseInt(commune.code, 10);

        if (codes.indexOf(code) === -1) {
          codes.push(code);
        }
      });
    });

    return codes;
  }


  /**
   *
   * @name phoneInput#setAreaCodeByCommune
   * @public
   * @description
   *
   * Update area code by commune Id.
   *
   * @param {String} communeId - Commune's id.
   *
   */
  phoneInput.prototype.setAreaCodeByCommune = function(communeId) {
    var areaCode = getAreaCode();

    if (areaCode && areaCode !== 9) {
      return;
    }

    Object.keys(regionArray).forEach(function onEachRegion(regionKey) {
      var region = regionArray[regionKey];
      var communes = Object.keys(region.commune);
      var commune;

      if (communes.indexOf(communeId) !== -1) {
        commune = region.commune[communeId];
        setAreaCode(parseInt(commune.code, 10));
        return false;
      }
    });
  };


  phoneInput.prototype.validate = validate;
  phoneInput.prototype.isValidAreaCode = isValidAreaCode;
  phoneInput.prototype.getArea = getAreaCode;
  phoneInput.prototype.getNumber = getPhoneNumber;
  phoneInput.prototype.setArea = setAreaCode;
  phoneInput.prototype.showError = showError;
  phoneInput.prototype.hideError = hideError;


  return phoneInput;


  /**
   *
   * @name phoneinput#validate
   * @public
   * @description
   *
   * Validate area code and number and hide/show errors.
   *
   * @return {Boolean}
   */
  function validate() {
    var isValid = true;

    hideError();

    if (!isMobileNumber()) {
      isValid = validateAreaCode();
    }

    if (isValid) {
      isValid = validatePhoneNumber();
    }

    return isValid;
  }


  /**
   *
   * @name phoneInput#onRadioChange
   * @private
   * @description
   *
   * Listen change event on radio buttons and call functions to toggle UI.
   *
   * @param {Object} evt - DOM's change event.
   *
   */
  function onRadioChange(evt) {
    var radio = evt.target;
    var isMobile = radio.value === 'm';

    toggleCountryCode(isMobile);
    toggleInputs(isMobile);

    validate();

    if (phoneInput.onTypeChange) {
      phoneInput.onTypeChange(evt, isMobile);
    }
  }


  /**
   *
   * @name phoneInput#toggleCountryCode
   * @private
   * @description
   *
   * Change textContent of countryCode.
   *
   * @param {Boolean} isMobile - Flag to indicate is mobile number.
   *
   */
  function toggleCountryCode(isMobile) {
    var countryCode = document
      .getElementsByClassName('phoneInput-countryCode')[0];

    countryCode.textContent = isMobile ? '+56 9' : '+56';
  }


  /**
   *
   * @name phoneInput#toggleInputs
   * @private
   * @description
   *
   * Hide and show inputs by isMobile params.
   *
   * @param {Boolean} isMobile - Flag to indicate is mobile number.
   *
   */
  function toggleInputs(isMobile) {
    var areaCode = document.getElementsByClassName('phoneInput-areaCode')[0];
    var areaCodeWrapper = document.getElementsByClassName(
      'phoneInput-areaCodeWrapper')[0];
    var inputs = document.getElementsByClassName('phoneInput-inputs')[0];
    var areaCodeNumber = areaCode.value ? parseInt(areaCode.value, 10) : 0;

    var MOBILE_CLASS = '__mobileNumber';
    var HIDDEN_CLASS = 'is-hidden';

    changePhoneLength(areaCodeNumber);

    if (isMobile) {
      areaCodeWrapper.classList.add(HIDDEN_CLASS);
      areaCode.required = false;
      inputs.classList.add(MOBILE_CLASS);
    } else {
      areaCodeWrapper.classList.remove(HIDDEN_CLASS);
      areaCode.required = true;
      inputs.classList.remove(MOBILE_CLASS);
    }
  }


  /**
   *
   * @name phoneInput#isMobileNumber
   * @private
   * @description
   *
   * Return a boolean to indicate is a mobile number or not.
   *
   * @return {Boolean}
   *
   */
  function isMobileNumber() {
    var mobileRadio = document.getElementById('phoneInputTypeMobile');
    return mobileRadio.checked;
  }


  /**
   *
   * @name phoneInput#showError
   * @private
   * @description
   *
   * Show an error message.
   *
   * @param {String} errorMessage - Message to show.
   * @param {Boolean} areaCodeError - Flag to indicate if is an error for areaCode.
   *
   */
  function showError(errorMessage, areaCodeError) {
    var wrapper = document.getElementsByClassName('phoneInput-inputs')[0];
    var error = document.getElementsByClassName('phoneInput-errorMessage')[0];

    error.textContent = errorMessage;
    error.classList.remove('is-hidden');
    wrapper.classList.add(areaCodeError ? '__errorCodeArea' : '__errorNumber');
  }


  /*
   *
   * @name phoneInput#hideError
   * @private
   * @description
   *
   * Hide the error message and remove css classes.
   *
   */
  function hideError() {
    var wrapper = document.getElementsByClassName('phoneInput-inputs')[0];
    var error = document.getElementsByClassName('phoneInput-errorMessage')[0];

    error.textContent = '';
    error.classList.add('is-hidden');
    wrapper.classList.remove('__error', '__errorCodeArea', '__errorNumber');
  }


  /**
   *
   * @name phoneInput#validateAreaCode
   * @private
   * @description
   *
   * Validate area code and show/hide it's error.
   *
   * @return {Boolean}
   *
   */
  function validateAreaCode() {
    if (isMobileNumber()) {
      return true;
    }

    var code = getAreaCode();
    var isValid = false;
    var error;

    changePhoneLength(code);

    if (code === 0) {
      error = phoneErrorMessages.AREA_CODE_MISSING;
    } else if (code.toString().length > 0) {
      isValid = isValidAreaCode(code);
      if (!isValid) {
        error = phoneErrorMessages.AREA_CODE_INVALID;
      }
    }

    if (error) {
      showError(error, true);
    } else {
      hideError();
    }

    return isValid;
  }


  /**
   *
   * @name phoneInput#isValidAreaCode
   * @private
   * @description
   *
   * Check if areaCode param is a valid area code.
   *
   * @param {Number} areaCode - Area code's value.
   * @return {Boolean}
   *
   */
  function isValidAreaCode(areaCode) {
    if (!areaCode) {
      return false;
    }

    return areaCodes.indexOf(areaCode) !== -1;
  }


  /**
   * @name phoneInput#validatePhoneNumber
   * @private
   * @description
   *
   * Validate phone number and show/hide it's error.
   *
   * @return {Boolean}
   *
   */
  function validatePhoneNumber() {
    var number = getPhoneNumber();
    var numberLength = number.toString().length;
    var validLength = getPhoneMaxLength();
    var error;

    if (number === 0) {
      error = phoneErrorMessages.PHONE_MISSING;
    } else if (numberLength > validLength) {
      error = phoneErrorMessages.PHONE_TOO_LONG.replace('{0}',validLength);
    } else if (numberLength < validLength) {
      error = phoneErrorMessages.PHONE_TOO_SHORT.replace('{0}',validLength);
    }

    if (error) {
      showError(error);
    } else if (validateAreaCode()) {
      hideError();
    }

    return !error;
  }


  /**
   *
   * @name phoneInput#getNumber
   * @private
   * @description
   *
   * Return phone number.
   *
   * @return {Number}
   *
   */
  function getPhoneNumber() {
    var input = document.getElementsByClassName('phoneInput-number')[0];
    var number = input.value;

    return number ? parseInt(number, 10) : 0;
  }

  /**
   *
   * @name phoneInput#getPhoneMaxLength
   *
   * @private
   * @description
   *
   * Return max length of phone number input.
   *
   * @return {Number}
   *
   */
  function getPhoneMaxLength() {
    var input = document.getElementsByClassName('phoneInput-number')[0];
    var number = input.getAttribute('maxlength');

    return number ? parseInt(number, 10) : 0;
  }

  /**
   *
   * @name phoneInput#getArea
   * @private
   * @description
   *
   * Return area code.
   *
   * @return {Number}
   *
   */
  function getAreaCode() {
    if (isMobileNumber()) {
      return 9;
    }

    var input = document.getElementsByClassName('phoneInput-areaCode')[0];
    var code = input.value;
    return code ? parseInt(code, 10) : 0;
  }


  /**
   *
   * @name phoneInput#setAreaCode
   * @private
   * @description
   *
   * Set area code if a valid area code.
   *
   * @param {Number} area - Area code number.
   * @return {Boolean} Return true if is a valid area code number.
   *
   */
  function setAreaCode(area) {
    if (area && areaCodes.indexOf(area) === -1) {
      return false;
    }

    var input = document.querySelector('.phoneInput-areaCode');

    if (input.value !== area) {
      input.value = area || '';
      validateAreaCode();
    }

    changePhoneLength(area);
    return true;
  }


  /**
   *
   * @name phoneInput#onlyNumbers
   * @private
   * @description
   *
   * Remove no numeric characters.
   *
   * @param {Object} evt - DOM's Event.
   * @return {String} return the new value.
   *
   */
  function onlyNumbers(evt) {
    var input = evt.target;
    var value = input.value;
    var cleanValue;

    if (!value) {
      return value;
    }

    cleanValue = value.replace(/\D/g, '');
    if (cleanValue !== value) {
      value = cleanValue;
      input.value = value;
    }

    return value;
  }


  /**
   *
   * @name phoneInput#changePhoneLength
   * @private
   * @description
   *
   * Update max and min length on phone depending of phone type and area code.
   *
   * @param {Number} code - Area code.
   *
   */
  function changePhoneLength(code) {
    var phone = document.getElementsByClassName('phoneInput-number')[0];
    var isMobile = isMobileNumber();

    var len = 8;
    var placeholder = 'Ej. 63068746';

    if (code !== 2 && !isMobile) {
      // Not mobile number or it's from Santiago
      len = 7;
    }

    if (!isMobile) {
      placeholder = 'Ej. 2823785';
    }

    phone.setAttribute('maxlength', len);
    phone.setAttribute('minlength', len);

    if (!phoneInput.isMsite) {
      phone.setAttribute('placeholder', placeholder);
    }
  }

})();
