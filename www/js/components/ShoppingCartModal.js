/**
 * Shows the desktop modal.
 *
 * @param      {object}  element  The element
 */
function showDesktopModal(id) {

  $.pgwModal({
    target: '#' + id,
    mainClassName: 'pgwModal ShoppingCartModal',
    maxWidth: 409
  });
}

/**
 * Closes a desktop modal.
 */
function closeDesktopModal() {
  $.pgwModal('close');
  $(location).attr('href', document.location.hostname + '/pagos');
}

/**
 * Shows the msite modal.
 *
 * @param      {object}  element  The element
 */
function showMsiteModal(id) {
  var modal = document.getElementById(id);
  var overlay = document.querySelector('.OverlayMobile');

  overlay.classList.add('__show');
  modal.classList.add('__show');

  window.setTimeout(function () {
    modal.classList.add('__animation');
  }, 0);
}

/**
 * Closes a msite modal.
 */
function closeMsiteModal() {
  var modal = document.querySelector('.ShoppingCartModal');
  var overlay = document.querySelector('.OverlayMobile');

  modal.classList.remove('__show');
  modal.classList.remove('__animation');
  overlay.classList.remove('__show');
  $(location).attr('href', document.location.hostname + '/pagos');
}
