/* global SplashScreen, getCookie */
/**
 *  @name splashScreen
 *  @description
 *
 *  Show splash screen for move to app from msite
 *
 */
(function () {
  this.SplashScreen = function (props) {
    this.splash = null;
    this.closeButton = null;
    this.options = {};

    var defaults = {
      autoOpen: true,
      className: 'splash-screen',
      closeButton: true,
      closeButtonClass: 'splash-screenClose',
      linkButtonClass: 'splash-screenLink',
      imageName: 'splash-screen-',
      imageType: 'png',
      numberOfImages: 1,
      multipleText: false,
      numberOfText: 1,
      data: ''
    };

    // Create options by extending defaults with the passed in arguments
    if (props && typeof props === 'object') {
      this.options = extendDefaults(defaults, props);
    } else {
      this.options = defaults;
    }
    if (this instanceof SplashScreen && this.options.autoOpen) this.open();
  };

  /*
  *
  *  @name SplashScreen close
  *  @description
  *
  *  Public function for close splash
  *
  */
  SplashScreen.prototype.close = function () {
    this.splash.classList.remove('__show');
    setTimeout(function () {
      this.splash.classList.remove('__open');
    }.bind(this), 500);
  };

  /*
  *
  *  @name SplashScreen open
  *  @description
  *
  *  Public function for open splash
  *
  */
  SplashScreen.prototype.open = function () {
    if (parseInt(this.options.data.enabled, 10) === 1 && checkVisibility.call(this)) {
      this.splash = document.querySelector('.' + this.options.className);

      buildSplash.call(this);
      initializeEvents.call(this);
      this.splash.className = this.options.className + ' __open';

      setTimeout(function () {
        this.splash.classList.add('__show');
      }.bind(this), 500);
    }
  };

  /*
  *
  *  @name buildSplash
  *  @description
  *
  *  Private function for build and load info from arguments
  *  load content into html elements
  *
  */
  function buildSplash() {
    var randomImage = getRandom(this.options.numberOfImages);
    var elements = this.options.data.content;

    if (this.options.closeButton) {
      this.closeButton = document.createElement('button');
      this.closeButton.className = this.options.className
        + 'ButtonClose '
        + this.options.closeButtonClass;
      this.closeButton.onclick = function () {
        return xt_click(this.closeButton,'C','','home::splash_screen::::close_on_top','A');
      };
      this.splash.appendChild(this.closeButton);
    }

    Object.keys(elements).forEach(function(key, index) {
      var div = document.querySelector('.' + elements[key].classelement);
      if (key === 'textapp' && this.options.multipleText === 'true') {
        var randomText = getRandom(this.options.numberOfText);
        div.innerHTML = elements[key].text[randomText];
      } else {
        div.innerHTML = elements[key].text;
      }
    }.bind(this));
  }

  /*
  *
  *  @name checkVisibility
  *  @description
  *
  *  Private function for check from cookie configuration
  *  if is available for show the splash screen (percent of clients to show and times on 24hrs)
  *
  */
  function checkVisibility() {
    var data = this.options.data;
    var oldCookiePercent = null;
    var oldCookieShow = null;
    var cookiePercent = data.cookie.percent;
    var cookieShow = data.cookie.show;
    var value = getRandom(100);

    if (!cookiePercent && !cookiePercent.name && !cookieShow.name) {
      return false;
    }

    oldCookiePercent = getCookie(cookiePercent.name);
    oldCookieShow = getCookie(cookieShow.name);

    if (oldCookiePercent) {
      if (oldCookieShow) {
        return false;
      }

      value = oldCookiePercent;
    } else {
      createCookie.call(this, cookiePercent.name, cookiePercent.lifetime, value);
    }

    if (value <= parseInt(cookiePercent.maxvalue, 10)) {
      createCookie.call(this, cookieShow.name, cookieShow.lifetime, cookieShow.value);
      return true;
    }

    return false;
  }

  /*
  *
  *  @name createCookie
  *  @description
  *
  *  Private function for create and set cookie
  *
  *  @param {string} name for cookie
  *  @param {string} lifetime value on millisecond for cookie expires
  *  @param {value} value of cookie
  *
  */
  function createCookie(name, lifetime, value) {
    var expires = '';
    var date = new Date();

    date.setTime(date.getTime() + parseInt(lifetime, 10));
    expires = '; expires=' + date.toUTCString();
    document.cookie = name + '=' + value + expires + '; path=/';
  }

  /*
  *
  *  @name extendDefaults
  *  @description
  *
  *  Private function for help and set defaults options
  *  from props
  *
  *  @param {object} source Object with defaults props
  *  @param {object} properties Obejct with new instance values
  *
  *  @return Obejct with update values from defaults
  *
  */
  function extendDefaults(source, properties) {
    Object.keys(properties).forEach(function(property, index) {
      source[property] = properties[property];
    });

    return source;
  }

  /*
  *
  *  @name initializeEvents
  *  @description
  *
  *  Private function for add listener events to close and links buttons
  *
  */
  function initializeEvents() {
    var closeButtons = document.querySelectorAll('.' + this.options.closeButtonClass);
    var linkButtons = document.querySelectorAll('.' + this.options.linkButtonClass);

    if (closeButtons) {
      [].forEach.call(closeButtons, function(elem) {
        elem.addEventListener('click', this.close.bind(this));
      }.bind(this));
    }

    if (linkButtons) {
      [].forEach.call(linkButtons, function(elem) {
        elem.addEventListener('click', goLink.bind(this, this.options.data.deeplink));
      }.bind(this));
    }
  }

  /*
  *
  *  @name goLink
  *  @description
  *
  *  Private function for redirect to link and close splash
  *
  *  @param {string} link to redirect
  *
  */
  function goLink(link) {
    window.location.href = link
    this.close();
  }

  /*
  *
  *  @name getRandom
  *  @description
  *
  *  Private function for get a random number with a max option
  *
  *  @param {number} max limit for random number
  *
  *  @return number random
  *
  */
  function getRandom(max) {
    return Math.floor(Math.random() * max) + 1;
  }
}());
