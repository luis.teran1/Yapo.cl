/* global sendXitiTag:true, xt_med, format_number_input, draftNodeOnChange,
 initDraft */

var requestsExceeded = false;

$(document).ready(function onReady() {

  selectRegionCommunes();
  enableDisableParams();
  initAutocomplete();

  $('#region').change(function onChangeRegion(event) {
    selectRegionCommunes(event.target.value);
    if (requestsExceeded) {
      return;
    }
    enableDisableParams();
  });

  $('#communes').change(function onChangeCommune() {
    if (requestsExceeded) {
      return;
    }

    if ($('#add_location').is(':checked')) {
      resetMap();
    }

    enableDisableAddLocation();
  });

  $('#add_location').change(function onChangeLocation(event) {
    checkMapParams();
    sendXitiTag($(event.target));
  });

  $('#address').on('change keyup', function(e) {
    if (e.keyCode === 13 || e.which === 13) {
      e.preventDefault();
      $('#show_location').click();
    } else {
      $('#add_location').is(':checked') && resetMap();
      processAddress();
    }
  });

  $('#address_number').on('input', function() {
    format_number_input('address_number');
    setupExactLocation();
  }).on('change keyup', function(e) {
    if (e.keyCode === 13 || e.which === 13) {
      e.preventDefault();
      $('#show_location').click();
    } else {
      $('#add_location').is(':checked') && resetMap();
      setupExactLocation();
    }
  });

  $('#address, #address_number').on('keydown', function(e) {
    if (e.keyCode === 13 || e.which === 13) {
      e.preventDefault();
      return false;
    }
  });

  $('#maps_help').click( function(e) {
    e.preventDefault();
    $.pgwModal({
      target: '#maps_modal',
      maxWidth: 654
    });
    $('.pm-close .pm-icon').addClass('white');
  });

  $('#show_location').one('click', function(e) {
    e.preventDefault();
    showMap();
  });

  $('#approx').change( function(e) {
    sendXitiTag($(e.target));
  });

  $('#exact').change( function(e) {
    sendXitiTag($(e.target));
  });

  sendXitiTag = function($node) {
    xt_med('C', '', $node.data('xtclick_name'), $node.data('xtclick_type'));
  };

});

function resetMap() {
  if (!$('.icon-face-sad-circled').is(':visible')) {
    $('#map_preview').show();
    $('#show_location').show();
    $('#address_ok').hide();
    $('#address_nok').hide();
  }
}

function enableDisableParams() {
  if ($('#region').val() !== '0') {
    $('#communes').prop('disabled', false);
  } else {
    $('#communes').prop('disabled', true);
  }
}

function enableDisableAddLocation() {
  if ($('#communes').val()) {
    enableAddLocation();
  } else {
    disableAddLocation();
  }
}

function enableAddLocation() {
  $('label[for=add_location]').prop('disabled', false);
  $('label[for=add_location]').removeAttr('disabled');
  $('#add_location').prop('disabled', false);
}

function disableAddLocation() {
  $('label[for=add_location]').prop('disabled', true);
  $('label[for=add_location]').attr('disabled', 'true');
  $('#add_location').prop('disabled', true);
  $('#add_location').prop('checked', false);
  hideMapParams();
}

function checkMapParams() {
  $('#add_location').is(':checked') ? showMapParams() : hideMapParams();
}

function selectRegionCommunes(region) {
  // select region if we have to
  selectRegion(region);
  var $region = $('#region');
  var $communes = $('#communes');
  var regionValue = $region.val();
  // populate communes if the region is selected or it is a different region
  if (regionValue > 0 && $communes.data('region') !== regionValue) {
    $.ajax({
      method: 'GET',
      url: '/templates/common/communes.html?region=' + regionValue,
      success: populateCommunes
    });
  // if there is no region selected, select the first element of communes selector
  } else if (regionValue === '0') {
    $communes.find("option[value='']").prop('selected',true);
    populateCommunes();
  }
}

function selectRegion(value) {
  if (value === -1) {
    // reset to default
    selectRegion($('#region').data('value'));
  } else if (value > 0) {
    $('#region option[value=' + value + ']').prop('selected',true);
  }
}

function populateCommunes(result) {
  var $communes = $('#communes');
  // @TODO: use JSON instead of eval but it's necessary repair result content
  var communes = eval('[' + result + ']'); // eslint-disable-line no-eval
  var option;
  var names;
  var defaultOption = $communes.find('option').first();

  $communes.empty().append(defaultOption.get());

  communes.forEach(function onEachCommune(commune) {
    if (!commune) {
      return;
    }

    Object.keys(commune).forEach(function onEachKey(key) {
      names = commune[key];
      option = '<option data-map="' + names.map_name + '" value="' + key +
        '">' + names.yapo_name + '</option>';
      $communes.append(option);
    });
  });

  setCommunesValue();
  if (typeof initDraft === 'function') {
    initDraft('#communes', true);
    if (!$communes.data('draft_change_event')) {
      $communes.data('draft_change_event', true);
      $communes.on('change', draftNodeOnChange);
    }
  }
  $('#communes').attr('data-region', $('#region').val());
}

function setCommunesValue() {
  var $communes = $('#communes');
  var value = $communes.data('value');
  var event = document.createEvent('Event');

  event.initEvent('change', true, true);

  if (value > 0) {
    $communes.data('value', '-1');
    $communes.val(value);
    $communes[0].dispatchEvent(event);
    checkMapParams();
  }

  enableDisableAddLocation();
}

function showMapParams() {
  $('#map_adv_params').show();
  $('#map_preview_wrap').show();

  hideAllMapMessages();

  $('.map-preview-bottom').addClass('is-bw');
  $('.map-preview-bottom .maps-icon').hide();
  $('.map-preview-bottom .icon-face-sad-circled').hide();
  $('#advice_location').show();

  if (!$('#geoposition').val()) {
    $('#map_preview_top_info').show();
    $('#show_location').show();
  } else { // show map
    $('#map_preview_exact').show();
    showMap();
  }
}

function hideMapParams() {
  $('#map_adv_params').hide();
  $('#map_preview_wrap').hide();

  hideAllMapMessages();
  $('#map_preview_top_default').show();

  $('.map-preview-bottom').removeClass('is-bw');
  $('.map-preview-bottom .maps-icon').show();
  $('.map-preview-bottom .icon-face-sad-circled').hide();
  $('#advice_location').hide();
  $('#show_location').hide();
  $('#map_preview').show();

  $('#geoposition').val('').trigger('change');
  $('.map-preview-wrap').css('height', '315px');
}

function showMap() {
  $('#show_location').hide();
  $('#map_preview').hide();
  $('#map_container').show();
  var $geoposition = $('#geoposition');
  var geopositionCoordinates = $geoposition.val().split(',');

  if (geopositionCoordinates.length === 2) {
    $geoposition.data('geoposition_coordinates',[
      parseFloat(geopositionCoordinates[0]),
      parseFloat(geopositionCoordinates[1])
    ]);
    $geoposition.data('geoposition_marker', [
      parseFloat(geopositionCoordinates[0]),
      parseFloat(geopositionCoordinates[1])
    ]);
  } else {
    $geoposition.data('geoposition_coordinates', $().yapomap.defaults.center);
    $geoposition.data('geoposition_marker', null);
  }

  // Init map if not initialized already
  if (!$('#map_container .leaflet-map-pane').length) {
    $('#map_container').yapomap({
      debug: true,
      appl: 'ai',
      appId: $('#map_setup').data('map_app_id'),
      appCode: $('#map_setup').data('map_app_code'),
      zoom: $('#map_setup').data('initial_zoom_level'),
      center: $('#geoposition').data('geoposition_coordinates'),
      marker: $('#geoposition').data('geoposition_marker'),
      geoUrl: $('#map_setup').data('geocoder_url'),
      baseUrl: $('#map_setup').data('base_url'),
      getPreciseOptionValue: function() {
        return this.$precise.is(':checked');
      },
      circle: {
        radius: $('#map_setup').data('circle_radius'),
        fill: true,
        color: '#0055AA',
        opacity: 1,
        fillOpacity: 0.6,
        weight: 0,
        clickable: true,
        className: 'map-circle'
      },
      onSuccess: mapGetSuccess,
      onServiceNotAvailable: showMapErrorProvider,
      onAddressNotFound: showMapErrorNotFound,
      onRequestsExceeded: showMapErrorRequestsExceeded,
      onAddressMapMatch: mapAddressMatch,
      onAddressMapDoesNotMatch: mapAddressDoesNotMatch
    });
  }
}

function setupExactLocation() {
  $('#address_number').val().length ?
    enableExactLocation() : disableExactLocation();
}

function enableExactLocation() {
  $('#exact').removeAttr('disabled');
  $('#exact+label').removeAttr('disabled');
  $('#exact').prop('checked', true);
}

function disableExactLocation() {
  $('#exact').attr('disabled', 'disabled');
  $('#exact+label').attr('disabled', 'disabled');
  $('#exact').prop('checked', false);
  $('#approx').prop('checked', true);
}

function processAddress() {
  validateAddress() && showExactLocationMessage();
}

function validateAddress() {
  if ($.trim($('#address').val()).length > 2) {
    $('#show_location').prop('disabled', false);
    $('#address_number').prop('disabled', false);
    return true;
  }

  addressNotFound();
  return false;
}

function addressNotFound() {
  $('#show_location').prop('disabled', true);
  $('#address_number').prop('disabled', true);
  $('#address_number').val('');
  disableExactLocation();
}

function showExactLocationMessage() {
  if (!$('.icon-face-sad-circled').is(':visible')) {
    hideAllMapMessages();
    $('#map_preview_exact').show();
  }
}

function showMapErrorProvider() {
  hideMapParams();
  disableAddLocation();
  $('#map_preview_wrap').show();
  showMapError('map_error_provider');
}

function showMapErrorNotFound() {
  hideAllMapMessages();
  $('#map_error_not_found').show();
}

function showMapErrorRequestsExceeded() {
  requestsExceeded = true;
  hideMapParams();
  disableAddLocation();
  $('#map_preview_wrap').show();
  showMapError('map_error_requests_exceeded');
  sendXitiTag($('#map_error_requests_exceeded'));
}

function showMapError(error) {
  $('#map_preview_top_default').hide();
  $('#map_preview_top_info').hide();
  $('.map-preview-bottom').addClass('is-bw');
  $('.map-preview-bottom .maps-icon').hide();
  $('.map-preview-bottom .icon-face-sad-circled').show();
  $('#' + error).show();
}

function mapAddressMatch() {
  $('#address_ok').show();
}

function mapAddressDoesNotMatch() {
  $('#address_nok').show();
  sendXitiTag($('#address_nok'));
  $('#address_ok').hide();
}

function mapGetSuccess() {
  showMap();
  sendXitiTag($('#show_location'));
}

function hideAllMapMessages() {
  $('.map-preview-top').hide();
  $('.map-preview-bottom.icon-face-sad-circled').hide();
}

function initAutocomplete() {
  $('#address').autocomplete({
    source: function(request, response) {
      $.ajax({
        url: '/streets_autocomplete.json',
        dataType: 'json',
        data: {
          address: request.term,
          limit: $('#map_setup').data('street_lim'),
          region: $('#region').val(),
          commune: $('#communes').val()
        },
        success: response
      });
    },
    minLength: 3,
    select: function(event, ui) {
      $('#address').val(ui.item.street_name).trigger('change');
      $('#street_id').val(ui.item.id);
      return false;
    },
    focus: function(e, ui) {
      e.preventDefault();
      $('#address').val(ui.item.street_name);
      processAddress();
    },
    create: function() {
      $(this).data('ui-autocomplete')._renderItem = function(ul, item) {
        return $('<li>')
          .append('<a>' + item.street_name + '</a>')
          .appendTo(ul);
      };
    }
  });
}
