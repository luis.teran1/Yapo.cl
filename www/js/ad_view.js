/* globals
 * getCookie,
 * setCookie,
 * real_format_price,
 * xt_med,
 * setabusereport,
 * xt_click,
 * ajax_request,
 * sendTealiumTag,
 * adwatch,
 * js_info
 * get_page_custom_variables
 * setFocus
 * */
$(document).ready(function() {
  // Show banner
  var doc = document.documentElement;
  var adName;
  var currentText;
  var xtcustom;
  var mpId;
  var $div;
  var $body;
  var url;
  var w;
  var h;

  const adData = GetAdData();
  if (adData) {
    SetupAdviewDataLayer(ParseDataForGTM(adData), GetUserId(), document.referrer);
  }

  if (window.dataLayer) {
    dataLayer.push({
      event: 'adview_event',
      event_name: 'Ad detail viewed',
      event_type: 'View',
      object_type: 'ClassifiedAd',
      target_type: 'ClassifiedAd',
    });
  }

  $('#ad-4').show();

  doc.setAttribute('data-useragent', navigator.userAgent);

  // Navigation bar buttons
  $('#top_prev, #top_next, #bottom_prev, #bottom_next').hover(function() {
    titleHover($(this));
    adName = $(this).data('link-text');
    currentText = $(this).text();
    $(this)
      .find('.prev-text, .next-text')
      .hide()
      .text(adName)
      .fadeIn('fast');
    $(this).data('link-text', currentText);
  }, function () {
    currentText = $(this).data('link-text');
    adName = $(this).text();
    $(this)
      .find('.prev-text, .next-text')
      .hide()
      .text(currentText)
      .fadeIn('fast');
    $(this).data('link-text', adName);
  });

  $('#da_reply_form').adReply();

  $('#da_reply_form').on('adreply.done', function(event, data) {
    /* Here we do anything that must be done after a successful ad reply */
    $body = $('body');
    $body.append(
      '<iframe src="/fb_conversion_ar.html" width="0" height="0" style="display:none);">'
    );
    /* Return Xiti click for stats */
    xtcustom = $('#adreply_confirm').val();
    xt_click(this, 'F', '', xtcustom);
  });

  $('#abusebutton_link').darkTooltip({
    trigger: 'click',
    modal: true,
    opacity: 1,
    gravity: 'east',
    animation: 'flipIn',
    theme: 'custom',
    onClose: abuseClose
  }).click(abuseOpen);

  $('#btn_sendtip').darkTooltip({
    trigger: 'click',
    gravity: 'west',
    animation: 'flipIn',
    theme: 'custom',
    modal: true,
    onClose: function() { sendTipClose(); }
  }).click(sendTipOpen);

  $('#save-fav').click(addFavorite);

  $('#save-fav').darkTooltip({
    trigger: 'click',
    gravity: 'east',
    size: 'small',
    animation: 'flipIn'
  });

  $('#share_facebook').click(function(event) {
    event.preventDefault();
    url = $(this).attr('data-facebook');
    // TODO: fix the xtn2
    // xt_click(this, 'F', xtn2_share, 'Share_on_Facebook');
    window.open('https://www.facebook.com/sharer.php?t=&u=' + url, 'facebook_popup', 'width=640, height=480');
  });

  $('#share_twitter').click(function(event) {
    event.preventDefault();
    url = $(this).attr('data-tweet');
    // TODO: fix the xtn2
    // xt_click(this, 'F', xtn2_share, 'Share_on_Twitter');
    window.open('https://twitter.com/share?text=' + url, 'twitter_popup', 'width=640, height=480');
  });

  $('#da_view_map').mouseover(function() {
    w = $('#da_view_map').outerWidth();
    h = $('#da_view_map').outerHeight();
    $('#da_view_map_hover').css('width', w);
    $('#da_view_map_hover').css('height', h);
    $('#da_view_map_hover').show();
  }).mouseout(function() {
    $('#da_view_map_hover').hide();
  });

  $('#da_view_map_hover').on('click', function() {
    openMapModal();
  });

  $('#da_view_map, #go_to_map').on('touchstart', function(e) {
    e.preventDefault();
    openMapModal();
  });

  $(document).bind('PgwModal::Close', function() {
    $('.pm-close .pm-icon').removeClass('white');
  });

  if (adData) {
    showBtnFinancing(adData.category, adData.type, adData.title, adData.id);
  }
});

function showBtnFinancing(category, adType, title, adId){

  if(adType === '1' && urlYapoQL && urlYapoQL != '' && category === categoryFinance){
    //Loan manager
    quotaFinance.initialize({ ad: {
                                title: title,
                                adId: adId,
                                urlYapoQL: urlYapoQL,
                                device: 'desktop'}
                            });
  }
}

function openMapModal() {
  $.pgwModal({
    target: '#main_map_modal',
    maxWidth: 700
  });
  $('.pm-close .pm-icon').addClass('white');
  $('.pm-content #modal_map_wrap').yapomap({
    debug: true,
    appl: 'vi',
    appId: $('#modal_map_wrap').data('app_id'),
    appCode: $('#modal_map_wrap').data('app_code'),
    center: $('#modal_map_wrap').data('location'),
    marker: $('#modal_map_wrap').data('location'),
    preciseInput: $('#modal_map_wrap'),
    getPreciseOptionValue: function() {
      return parseInt(this.$precise.data('precise'), 10) === 1;
    },
    zoom: $('#modal_map_wrap').data('zoom'),
    baseUrl: $('#base_url').val(),
    draggable: false
  });
  try {
    xt_med(
      'C', '',
      $('#da_view_map_hover').data('xtclick_name'),
      $('#da_view_map_hover').data('xtclick_type')
    );
  } catch (e) {}
}

function setAbuseEvents() {
  var url;
  var listId;
  var abuseType;
  var lang;
  var category;

  // Set events for abuse reasons
  $('.abuse-reason').hover(function() {
    $(this).find('i').show();
  }).on('mouseout', function() {
    $(this).find('i').hide();
  })
  .click(function(e) {
    e.preventDefault();
    url = $(this).data('url');
    listId = $(this).data('list-id');
    abuseType = $(this).data('abuse-type');
    lang = $(this).data('lang');
    category = $(this).data('category');
    setabusereport(url, listId, abuseType, lang, category);
  });

  $(document).on('focus', '#abuse_message', function() {
    $(this).css('height', '100px');
  });

  $(document).on('blur', '#abuse_message', function() {
    if (!$('#abuse_wrapper .btn').is(':hover')) {
      $(this).css('height', '34px');
    }
  });
}

function checkSendTip(result, xmlhttp) {
  var displayMessage;
  var strConfirm;
  var errorMsg;
  var isError;
  var strErr;
  var xtCustomVar;
  var newbody;

  if (result) {
    displayMessage = document.getElementById('sendtip_error');

    if (result.match(/Status:TRANS_OK/)) {
      // Show success message
      $('#send_tip_wrap').hide();
      $('#send_tip_success').show();
      $('#darktooltip-btn_sendtip').addClass('micro');
      $('#darktooltip-btn_sendtip').removeClass('with-errors');
      $('#sendtip_error').remove();

      strConfirm = document.getElementById('sendTipConfirmXiti').value;

      sendTealiumTag($('#send_tip_form').attr('data-tealium-view-ok'), true);
      return xt_click(this, 'F', '', 'foo&stc=' + strConfirm);
    } else {
      sendTealiumTag($('#send_tip_form').attr('data-tealium-view-error'), true);
      if (errorMsg = result.match(/Name:(.*?);/)) {
        displayMessage.innerHTML = errorMsg[1];
        displayMessage.className = 'error';
      } else if (errorMsg = result.match(/Email:(.*?);/)) {
        displayMessage.innerHTML = errorMsg[1];
        displayMessage.className = 'error';
      } else {
        document.getElementById('tip_email').value = '';
        displayMessage.innerHTML = js_info['TIP_SENT'];
        displayMessage.className = 'okey';
      }
      isError = true;
      $('#darktooltip-btn_sendtip').addClass('with-errors');
    }
    displayMessage.style.display = 'block';
    if (isError) {
      strErr = document.getElementById('sendTipErrXiti').value;
      xtCustomVar = get_page_custom_variables();
      return xt_click(this, 'F', '', 'foo&stc=' + strErr + '&' + xtCustomVar);
    }
  } else if (xmlhttp.responseText.indexOf('<!DOCTYPE') >= 0) {
    newbody = xmlhttp.responseText;

    /*
     * Since we can't intercept 302 responses from the server
     * indicating that something strange has happened (eg.
     * that we need to log in again) because xmlhttp automagically
     * follows the redirect, we try to detect that this has happened
     * through finding a "<!DOCTYPE" in the document and replacing
     * our body with what appears to be the body of the document
     * we got.
     */
    newbody = newbody.replace(/(.|\n)*\<body\>/, '');
    newbody = newbody.replace(/\<\/body\>[^<]*\<\/html\>[^<]*$/m, '');
    document.body.innerHTML = newbody;
    return;
  }
}

function sendTipOpen() {
  var $element = $('#darktooltip-btn_sendtip');
  var id = $('[data-list_id]').data('list_id');
  var data = { id: id };

  $.ajax({
    async: false,
    timeout: 30000,
    data: data,
    dataType: 'html',
    url: '/get_send_tip.html'
  }).done(function(requestData) {
    $element.find('div:first').html(requestData);
  });
}

function sendTip(form) {
  var get = '/sendtip.htm';
  var post = '';
  var i;
  var temp;

  for (i = 0; i < form.elements.length; i++) {
    temp = form.elements[i];
    post += temp.name + '=' + escape(temp.value).replace(/%u[0-9][0-9][0-9][0-9]/g, '%3F') + '&';
  }
  ajax_request(get, post, checkSendTip, form, false);
  return false;
}

/* SCARFACE JS Functions*/

function abuseClose() {
  forceCloseTooltip('#darktooltip-abusebutton_link');
  try {
    abuseCloseHistory();
  } catch (e) {}
}

function abuseCloseHistory() {
  forceCloseTooltip('#darktooltip-scf_abusebutton_link');
}

function sendTipClose() {
  forceCloseTooltip('#darktooltip-btn_sendtip');
  $('#sendtip_error').remove();
}

function forceCloseTooltip(tooltipId) {
  var $darktooltip = $(tooltipId);
  var hideLink;
  $darktooltip.hide();

  $('.darktooltip-modal-layer').hide();
  $('.dark-tooltip').removeClass('micro');
  $darktooltip.removeClass('with-errors');

  // Check if I need to hide the abuse link or not
  hideLink = $darktooltip.data('hide-link');
  if (hideLink !== undefined) {
    hideAbuseLink();
  }
}

function hideAbuseLink() {
  // Slide left for abuse link
  $('#abuse_tool_wrapper').css('transition', 'all .3s');
  $('#abuse_tool_wrapper').css('width', '0');
  $('#abuse_tool_wrapper').css('height', '20px');
  $('#abuse_tool_wrapper').css('border', '0');
  $('#abuse_tool_wrapper').css('overflow', 'hidden');
  $('#adview_menu_options').css('width', '100%');
  $('#adview_menu_options').css('text-align', 'center');
  $('#adview_menu_options a').css('margin', '0');
}

function abuseOpen() {
  var $tooltipWrapper = $('#darktooltip-abusebutton_link').find('div:first');
  var id = $('[data-list_id]').data('list_id');
  var data = { id: id };
  $.ajax({
    async: false,
    timeout: 30000,
    data: data,
    url: '/get_abuse_buttons.html'
  }).done(function(doneData) {
    $tooltipWrapper.html(doneData);
    setAbuseEvents();
  });
}

function showSendTip() {
  var socialdiv;
  var tipbox;

  try {
    tipbox = document.getElementById('TipBox');
    socialdiv = document.getElementById('social_icons_div');
    if (tipbox.style.display == 'none') {
      tipbox.style.display = 'block';
      socialdiv.style.display = 'none';
    } else {
      tipbox.style.display = 'none';
      socialdiv.style.display = 'block';
    }
    setFocus('tip_name');
  } catch (e) {}
}

(function($) {
  $.fn.ext_form = function(data) {
    if (data.hasOwnProperty('load'))data.load.fn(this, data.load);
    if (data.hasOwnProperty('validate'))data.validate.fn(this, data.validate, data.validate.consts);
    if (data.hasOwnProperty('callback'))data.callback.fn(this, data.callback);
    if (data.hasOwnProperty('submit'))data.submit.fn(this, data.submit);

    return this;
  };
}(jQuery));

/* Navigation bar */

function titleHover($element) {
  var listId;
  var qs;
  var to;
  var data;

  if ($element.data('link-text')) {
    return;
  }

  listId = $('[data-list_id]').data('list_id');
  qs = $.cookie('sq');
  to = $element.attr('id').split('_')[1];
  data = { id: listId };
  data[to] = 1;
  data['qs'] = qs;

  $.ajax({
    async: false,
    timeout: 30000,
    data: data,
    url: '/navigation_popup.html'
  }).done(function(doneData) {
    $element.data('link-text', $(doneData).text());
  });
}

/* End navigation bar */

function addFavorite(event) {
  var id;
  var domain;
  var uuid;
  var data;
  var saveAdsCookie;

  event.preventDefault();

  id = $('[data-list_id]').data('list_id');
  domain = $(this).data('domain');
  uuid = getCookie('adw');

  // Ad watch based on cookies
  if (adwatch.save_ads.cookie_storage === 1) {
    saveAdsCookie = getCookie(adwatch.save_ads.cookie_name);
    if (saveAdsCookie && saveAdsCookie.length) {
      saveAdsCookie += ',' + id;
    } else {
      saveAdsCookie = id;
    }

    setCookie(adwatch.save_ads.cookie_name, saveAdsCookie, 180, '/', domain);
  } else {
    data = { aid: id };
    if (uuid != null) {
      data['uuid'] = uuid;
    }

    $.ajax({
      url: '/send_aw',
      data: data,
      dataType: 'json'
    }).done(function(requestData) {
      if (requestData['status'] === 'OK') {
        // set cookie with uuid in response
        uuid = requestData['watch_unique_id'];
        setCookie('adw', uuid, requestData.cookie_expire_days, '/', domain);
        setCookie('adw_ic', 1, requestData.cookie_expire_days, '/', domain);
        $('#save-fav')
          .find('.icon-favorite')
          .removeClass('.icon-favorite')
          .addClass('icon-heart-full');
      }
    });
  }
}

function loadAbusePreviuosMessages() {
  var loaded = document.body.getAttribute('loadAbusePreviuosMessages');
  var $trigger;

  if (loaded !== 'true') {
    $trigger = $('#scf_abusebutton_link');
    $trigger.darkTooltip({
      trigger: 'click',
      modal: true,
      gravity: 'east',
      delete_content: true,
      theme: 'custom',
      opacity: 1
    });
    $trigger.trigger('click');
    cleanAbuseHistory();
  }
  document.body.setAttribute('loadAbusePreviuosMessages', 'true');
}

function showAbuseThanks(data) {
  var mpId;

  $('#darktooltip-scf_abusebutton_link').addClass('micro');
  $('#darktooltip-abusebutton_link').addClass('micro');
}

function cleanAbuseHistory() {
  $('.abuse-prev-messages .abuse-message').each(function() {
    if ($(this).text().length < 10) {
      $(this).remove();
    }
  });
}

function displayModal(linkName) {
  $.pgwModal({
    target: '#cotiza_modal_' + linkName.toLowerCase(),
    maxWidth: 700
  });
  $('.pm-close .pm-icon').addClass('white');
}

function openPaymentDeliveryInfo(origin) {	
  const url = `${origin}/payment-delivery/app/modal/advantages`;	
  $.pgwModal({	
    content: `<iframe style="visibility:hidden;" id="pdModalIframe" width="800px;" height="600px;" src="${url}"></iframe>`,	
    mainClassName: 'pgwModal pdInfoModal',	
    maxWidth: 800,	
  });	
  document.getElementById('pdModalIframe').onload = function () {	
    $('#pdModalIframe').contents().find("html").css('overflow','hidden');	
    $('#pdModalIframe').css('visibility','visible');	
  };	
}

