// correctly handle PNG transparency in Win IE 5.5 or higher.
// http://homepage.ntlworld.com/bobosola/

function correctPNG() {
	for(var i=0; i<document.images.length; i++) {
		var img = document.images[i]
			var imgName = img.src.toUpperCase();
		if (imgName.indexOf('.PNG') > 0 || imgName.indexOf('.PHP') > 0) {
			var imgID = (img.id) ? "id='" + img.id + "' " : ""
				var imgClass = (img.className) ? "class='" + img.className + "' " : ""
				var imgTitle = (img.title) ? "title='" + img.title + "' " : "title='" + img.alt + "' "
				var imgStyle = "display:inline-block;" + img.style.cssText 
				var imgAttribs = img.attributes;
			for (var j=0; j<imgAttribs.length; j++) {
				var imgAttrib = imgAttribs[j];
				if (imgAttrib.nodeName == "align") {
					if (imgAttrib.nodeValue == "left") imgStyle = "float:left;" + imgStyle
						if (imgAttrib.nodeValue == "right") imgStyle = "float:right;" + imgStyle
							break
				}
			}
			var strNewHTML = "<span " + imgID + imgClass + imgTitle
				strNewHTML += " style=\"" + "width:" + img.width + "px; height:" + img.height + "px;" + imgStyle + ";"
				strNewHTML += "filter:progid:DXImageTransform.Microsoft.AlphaImageLoader"
				strNewHTML += "(src='" + img.src + "');\"></span>"
				img.outerHTML = strNewHTML
				i = i-1
		}
	}
}


function alphaBackgrounds(){
	var rslt = navigator.appVersion.match(/MSIE (\d+\.\d+)/, '');
	var itsAllGood = (rslt != null && Number(rslt[1]) >= 5.5);
	for (i=0; i<document.all.length; i++){
		var bg = document.all[i].currentStyle.backgroundImage;
		if (itsAllGood && bg){
			if (bg.match(/\.png/i) != null){
				var mypng = bg.substring(5,bg.length-2);
				document.all[i].style.filter = "progid:DXImageTransform.Microsoft.AlphaImageLoader(src='"+mypng+"', sizingMethod='scale')";
				document.all[i].style.backgroundImage = "url('/assets/images/x.gif')";
			}
		}
	}
}


if (navigator.platform == "Win32" && navigator.appName == "Microsoft Internet Explorer" && window.attachEvent) {
	window.attachEvent("onload", alphaBackgrounds);
	window.attachEvent("onload", correctPNG);
}
