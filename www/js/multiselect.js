const MultiSelectNameSpace = {};

(function(){
	document.addEventListener("DOMContentLoaded", function() {
		drawMulticomWidget();
		drawFootwearTypeWidget();
		drawServiceTypeWidget();
		drawJobCategoryWidget();
	});

	function drawMulticomWidget(){
		const communes = document.querySelectorAll('#communes_cmn option');
		const elements = [];
		
		communes.forEach((comune, i) => {
			if(i === 0){
				return;
			}
			const label = comune.innerHTML;
			const id = comune.value;
			let checked = '';

			if(id !== ''){
				if(comune.getAttribute('selected') === 'selected'){
					checked = 'checked';
				}
				elements.push({
					id: id,
					label: label,
					checked: checked,
					inputAttributes: [{
						key: 'data-comid',
						value: id,
					}]
				});
			}
		});
	
		CreateMultiSelect({
			selector: '.multicomune-container', 
			title: 'Comuna', 
			elements: elements,
			common: {
				value: 'all',
				id: 'all-comunes'
			},
			callbackCheckbox: (event) => {
				const checked = event.target.checked;
				const communeId = event.target.getAttribute("data-comid");
				const communeElement = document.querySelector("#communes_cmn option[value='" + communeId + "']");
				if (checked) {
					communeElement.setAttribute("selected", "selected");
				} else {
					communeElement.removeAttribute("selected");
				}
			}, 
			callbackCheckboxAll: (event) => {
				const checked = event.target.checked;
				if(checked){
					document.querySelectorAll("#communes_cmn option").forEach((element) => element.removeAttribute("selected"));
				}
			}
		});
	}

	function drawFootwearTypeWidget(){
		const elements = GetMultiselectElementsFromMeta('.footwear_filters_meta');
		CreateMultiSelect({
			selector: '.footwear_container', 
			title: 'Tipo de calzado', 
			elements: elements,
			common: {
				label: 'Todos',
			}
		});
	}

	function drawServiceTypeWidget(){
		const elements = GetMultiselectElementsFromMeta('.service_type_meta');
		CreateMultiSelect({
			selector: '.service_type_container', 
			title: 'Tipo de servicio', 
			elements: elements,
			common: {
				label: 'Todos',
			}
		});
	}

	function drawJobCategoryWidget(){
		const elements = GetMultiselectElementsFromMeta('.job_category_meta');
		CreateMultiSelect({
			selector: '.job_categories_container', 
			title: 'Rubro', 
			elements: elements,
			common: {
				label: 'Todos',
			}
		});
	}

	function GetMultiselectElementsFromMeta(tag){
		const elements = [];
		const metaElements = document.querySelectorAll(tag);

		metaElements.forEach((metaElement) => {
			const name = metaElement.getAttribute('name');
			const value = metaElement.getAttribute('value');
			const id = metaElement.getAttribute('inputId');
			const label = metaElement.getAttribute('label');
			const checked = metaElement.getAttribute('checked');

			elements.push({
				name: name,
				value: value,
				label: label,
				id: id,
				checked: checked,
			});
		});

		return elements;
	}

	function CreateMultiSelect(options){
		function Initialize(container, options){
			const mainElement = document.createElement('div');
			mainElement.innerHTML = `
				<div class="multiselect__head multiselect__head--open">
					${options.title}
				</div>
				<div class="multiselect__content"></div>
			`
			mainElement.classList.add('multiselect');
			container.append(mainElement);
			
			Update(container, options)
			return mainElement;
		}

		function Update(container, options){
			const mainElement = container.querySelector('.multiselect')
			const contentElement = mainElement.querySelector('.multiselect__content')

			Clear(contentElement);
			CreateMultiSelectElements(contentElement, options.elements, options.common);
			SetClickEvents(mainElement, options.callbackCheckbox, options.callbackCheckboxAll);
			CheckAllIfNoneSelected(mainElement);
			ShowFirstSelected(mainElement);
		}

		function Clear(contentElement){
			contentElement.innerHTML = "";
		}

		function CreateMultiSelectElements(contentElement, elements, common){
			contentElement.append(CreateMultiSelectElement({
				id: common.id,
				name: common.name, 
				value: common.value, 
				label: (common.label) ? common.label : 'Todas', 
				classes: ['multiselect__all-checkbox'],
			}));
		
			elements.forEach((element, i) => {
				const elementContainer = CreateMultiSelectElement({
					id: element.id, 
					name: (element.name) ? element.name : common.name, 
					value: (element.value) ? element.value : common.value,
					label: element.label, 
					checked: element.checked, 
					classes: ['multiselect__checkbox'], 
					inputAttributes: element.inputAttributes,
				});
				contentElement.append(elementContainer);
			})
		}

		function CreateMultiSelectElement(options){
			const elementContainer = document.createElement('div');
			const elementInput = document.createElement('input');
			const elementLabel = document.createElement('label');
	
			elementContainer.classList.add('search-listing__filters-checkbox');
			if(options.classes){
				options.classes.forEach(className => {
					elementContainer.classList.add(className);
				})
			}
			if(options.inputAttributes){
				options.inputAttributes.forEach((attribute) => {
					elementInput.setAttribute(attribute.key, attribute.value);
				});
			}
			if(options.id){
				elementInput.setAttribute('id', options.id);
			}
			if(options.name){
				elementInput.setAttribute('name', options.name);
			}
			if(options.value){
				elementInput.setAttribute('value', options.value);
			}
			elementInput.setAttribute('type', 'checkbox');
			if(options.checked){
				elementInput.setAttribute('checked', options.checked);
			}
			elementLabel.setAttribute('for', options.id);
			elementLabel.innerHTML = `<span class="search-listing__filters-checkbox-count">${options.label}</span>`
	
			elementContainer.append(elementInput);
			elementContainer.append(elementLabel);
			
			return elementContainer;
		}
	
		function ShowFirstSelected(mainElement){
			const firstSelected = GetFirstSelected(mainElement);
			
			if(firstSelected){
				SetVisibility(mainElement, true);
				const top = $(`${options.selector} .multiselect__content .search-listing__filters-checkbox input:checked`).position().top - 10;
				$(`${options.selector} .multiselect__content`).scrollTop(top);
			} else {
				SetVisibility(mainElement, false);
			}
		}

		function GetFirstSelected(mainElement){
			const elements = mainElement.querySelectorAll('.multiselect__checkbox');
			for(let i = 0; i < elements.length; i++){
				const element = elements[i];
				const checked = element.querySelector('input:checked');
				if(checked){
					return checked;
				}
			}
			return false;
		}
	
		function SetVisibility(mainElement, isVisible){
			const head = mainElement.querySelector('.multiselect__head');
			const content = mainElement.querySelector('.multiselect__content');
			if(isVisible){
				content.classList.remove('multiselect__content--hide');
				head.classList.add('multiselect__head--open');
			} else {
				content.classList.add('multiselect__content--hide');
				head.classList.remove('multiselect__head--open');
			}
		}
	
		function SetClickEvents(mainElement, callbackCheckbox, callbackCheckboxAll){
			const allCheckbox = mainElement.querySelector('.multiselect__all-checkbox input');
			const checkboxes = mainElement.querySelectorAll('.multiselect__checkbox input');
			const head = mainElement.querySelector('.multiselect__head');
			const content = mainElement.querySelector('.multiselect__content');
		
			head.onclick = () => {
				SetVisibility(mainElement, content.classList.contains('multiselect__content--hide'));
			};
		
			allCheckbox.onchange = function(event){
				const checked = event.target.checked;
		
				if(checked){
					checkboxes.forEach((checkbox) => {
						checkbox.checked = false;
					});
				} else {
					CheckAllIfNoneSelected(mainElement);
				}
		
				if(callbackCheckboxAll){
					callbackCheckboxAll(event);
				}
			};
			
			checkboxes.forEach((checkbox) => {
				checkbox.onchange = function(event){
					const checked = event.target.checked;
					
					if (checked) {
						const allCheckbox = mainElement.querySelector('.multiselect__all-checkbox input');
						allCheckbox.checked = false;
					} else {
						CheckAllIfNoneSelected(mainElement);
					}
	
					if(callbackCheckbox){
						callbackCheckbox(event);
					}
				}
			});
		}
		
		function CheckAllIfNoneSelected(mainElement){
			const selectedElements = mainElement.querySelectorAll('.multiselect__checkbox input:checked').length;
			if (selectedElements === 0) {
				const allCheckbox = mainElement.querySelector('.multiselect__all-checkbox input');
				allCheckbox.checked = true;
			} else {
				SetVisibility(mainElement, true);
			}
		}


		const container = document.querySelector(options.selector);
		if(!container.querySelector('.multiselect')){
			return Initialize(container, options);
		} else {
			return Update(container, options);
		}
	}
	
	MultiSelectNameSpace.drawMulticomWidget = drawMulticomWidget;
})();


function drawMulticomWidget(){
	MultiSelectNameSpace.drawMulticomWidget();
}