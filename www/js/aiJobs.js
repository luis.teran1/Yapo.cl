/* global category_settings, category_features, forcedLoginData, category_list, Yapo */

!(function() {
  document.addEventListener('DOMContentLoaded', init);

  function init() {
    if (document.getElementById('category_group') == undefined) {
      return;
    }
    var currentCategory = parseInt(document.getElementById('category_group').value, 10);
    var categoryName;

    if (!currentCategory) {
      return;
    }

    categoryName = category_list[currentCategory].name;

    Yapo.toggleJobEditionAdvice(
      currentCategory,
      categoryName,
      category_features,
      category_settings
    );
    Yapo.toggleJobInsertionPriceAdvice(currentCategory, category_features, category_settings);
    Yapo.aiCategoryLogin(currentCategory, forcedLoginData);
  }
})();
