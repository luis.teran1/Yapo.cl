$(document).ready(function(){
	
	if ($('input[id=form_to_show]').val() != "") {
		toggleForms($('input[id=form_to_show]').val());
	} else {
		toggleForms($('input[name=invoice-type]').val());
	}

	$('input[name=invoice-type]').on('ifChecked', function(event){
	 	toggleForms($(this).val());
	});

	$('.edit-email').click(function(e) {
		e.preventDefault();
		$('#id_email_1').show();
		$('#id_email_2').show();
	});

	$('input').iCheck({
		radioClass: 'iradio_minimal-green',
		increaseArea: '20%' // optional
	});

	$('.invoice-selector label').click( function(){
		var idC = $(this).attr("for");
		$("#" + idC).iCheck('check');
	});

	$("#submit-bill").click( function(){
		$("#payment_1").submit();
	});

	$("#submit-invoice").click( function(){
		$("#payment_2").submit();
	});

	$('.validation_msg:visible').each(function() {
		location.href=location.href+'#show-errors';
		return false;
	});

	$("a#more-info-payment").click( function(){
		$.pgwModal({
			target: '#pay-info'
		});
		setHelpPopupEvents();
		if ($(this).attr("data-xtpage-xtpage") && $(this).attr("data-xtpage-tag")) {
			xt_med('F', $(this).attr("data-xtpage-xtpage"), $(this).attr("data-xtpage-tag"), 'N');
		}
	});

	var dpi = window.devicePixelRatio;
	var thumbPath = $("#thumbnail-path").val();
	var thumbPath2x = $("#thumbnail-path-2x").val();

	adjustThumbs(dpi, thumbPath, thumbPath2x);

});


function toggleForms(type) {
	if (type =='bill') {
					var emailInvoice = $('#id_email_2').children('input').val();
        	$('#bill').show();
        	$('#invoice').hide();
					$('#id_email_1').children('input').val(emailInvoice);
    	}else {
					var emailBill = $('#id_email_1').children('input').val();
       		$('#invoice').show();
					$('#bill').hide();
					$('#id_email_2').children('input').val(emailBill);
    	}
    	$("#radio-" + type).iCheck('check');
}


