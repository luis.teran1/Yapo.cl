/**
 * Gets the named cookie.
 *
 * @param      {string}  name    The name
 * @return     {string}  The cookie.
 */
function getCookie(name) {
  function escape(s) { return s.replace(/([.*+?\^${}()|\[\]\/\\])/g, '\\$1'); };
  var match = document.cookie.match(RegExp('(?:^|;\\s*)' + escape(name) + '=([^;]*)'));
  return match ? match[1] : null;
}

/**
 * Delete the named cookie.
 *
 * @param      {string}  name    The name
 */
function deleteCookie(name) {
  document.cookie = name + '=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
}