/* global formatPrice, CartHandler, bconfCredits */

(function credits() {
  document.addEventListener('DOMContentLoaded', onLoaded);


  /**
   *
   * @name credits#onLoaded
   * @private
   * @description
   *
   * Set listener after DOM content is loaded.
   *
   */
  function onLoaded() {
    var input = document.getElementById('creditBuy-value');
    var form = document.getElementById('creditBuy');
    var packButtons = document.getElementsByClassName('creditPack-button');

    input.addEventListener('keyup', formatValueAndUpdateUI);
    input.addEventListener('change', formatValueAndUpdateUI);
    form.addEventListener('submit', onSubmit, false);

    [].forEach.call(packButtons, function onEachButton(button) {
      button.addEventListener('click', addCreditsToCart);
    });
  }


  /**
   *
   * @name credits#formatValueAndUpdateUI
   * @private
   * @description
   *
   * Format price (if arrow left, arrow right or shift key aren't pressed)
   * of credit's value and dispatch toogleError and toggleSubmit.
   *
   * @param {Object} event - Keyboard's event.
   *
   */
  function formatValueAndUpdateUI(event) {
    var target = event.target;
    var value = valueToNumber(target.value);
    var isValid = isValidValue(value);

    if ([37, 39, 16].indexOf(event.keyCode) !== -1) {
      return;
    }

    toggleError(!isValid);
    toggleSubmit(isValid);
    hideAlert();
    target.value = value ? formatPrice(value) : '';
  }


  /**
   *
   * @name credits#onSubmit
   * @private
   * @description
   *
   * Add credits to Cart with CartHandler if value is valid or show an error
   * if value isn't valid.
   *
   * @param {Object} event - Form's event.
   *
   */
  function onSubmit(event) {
    var input = document.getElementById('creditBuy-value');
    var value = valueToNumber(input.value);
    var form = event.target;

    event.preventDefault();

    if (!isValidValue(value)) {
      return;
    }

    showSpinner();
    CartHandler.addCredits(value); // Do a sync ajax request

    if (CartHandler.status === 'OK') {
      location.href = form.getAttribute('action');
    } else {
      showAlert();
    }
  }


  /**
   *
   * @name credits#addCreditsToCart
   * @private
   * @description
   *
   * Add credit to cart when click on "Comprar" button of each creditPack's box.
   *
   * @param {Object} event - Mouse click event.
   *
   */
  function addCreditsToCart(event) {
    var button = event.target;
    var amount = button.getAttribute('data-amount');
    var form = document.getElementById('creditBuy');

    if (!isValidValue(amount)) {
      return;
    }

    button.classList.add('__spinner');
    CartHandler.addCredits(amount); // Do a sync ajax request

    if (CartHandler.status === 'OK') {
      location.href = form.getAttribute('action');
    }

    button.classList.remove('__spinner');
  }

  /**
   *
   * @name credits#valueToNumber
   * @private
   * @description
   *
   * Remove dots of value and transform it to Number.
   *
   * @param {String} value - number in String.
   * @return {Number}
   *
   */
  function valueToNumber(value) {
    return parseInt(value.replace(/\./g, ''), 10);
  }


  /**
   *
   * @name credits#isValidValue
   * @private
   * @description
   *
   * Check if value is a valid amount.
   *
   * @param {Number} value - amount to check
   * @return {Boolean}
   *
   */
  function isValidValue(value) {
    return value >= bconfCredits.minAmount && value <= bconfCredits.maxAmount;
  }


  /**
   *
   * @name credits#toggleSubmit
   * @private
   * @description
   *
   * Toggle disable attribute of submit button by enable param.
   *
   * @param {Boolean} enable - Flag to indicate to add/remove disable attribute.
   *
   */
  function toggleSubmit(enable) {
    var submit = document.getElementById('creditBuy-submit');
    var fn = enable ? 'removeAttribute' : 'setAttribute';

    submit[fn]('disabled', 'disabled');
  }


  /**
   *
   * @name credits#toggleError
   * @private
   * @description
   *
   * Toggle `__error` modifier CSS classname of form and boxContent by
   * show param.
   *
   * @param {Boolean} show - Flag to indicate add/remove __error class.
   *
   */
  function toggleError(show) {
    var boxContent = document.getElementById('creditBox-content');
    var form = document.getElementById('creditBuy');
    var ERROR_CLASS = '__error';
    var fn = show ? 'add' : 'remove';

    boxContent.classList[fn](ERROR_CLASS);
    form.classList[fn](ERROR_CLASS);
  }


  /**
   *
   * @name credits#showAlert
   * @private
   * @description
   *
   * Show an alert after ajax request failed.
   *
   */
  function showAlert() {
    var errorElement = document.getElementById('creditBuy-errorMessage');
    errorElement.classList.remove('is-hidden');
    hideSpinner();
  }


  /**
   *
   * @name credits#hideAlert
   * @private
   * @description
   *
   * Hide the ajax's failed alert.
   *
   */
  function hideAlert() {
    var errorElement = document.getElementById('creditBuy-errorMessage');
    errorElement.classList.add('is-hidden');
  }


  /**
   *
   * @name credits#hideSpinner
   * @private
   * @description
   *
   * Hide spinner.
   *
   */
  function hideSpinner() {
    var input = document.getElementById('creditBuy-value');
    var submit = document.getElementById('creditBuy-submit');
    var spinner = document.getElementById('spinner');
    var icon = document.querySelector('.creditBuy-data .iconCredit');
    var iconClass = icon.getAttribute('class');

    submit.removeAttribute('disabled', 'disabled');
    input.removeAttribute('disabled', 'disabled');
    spinner.classList.remove('__visible');

    icon.setAttribute('class', iconClass.replace(' __gray', ''));
  }


  /**
   *
   * @name credits#showSpinner
   * @private
   * @description
   *
   * Show spinner.
   *
   */
  function showSpinner() {
    var input = document.getElementById('creditBuy-value');
    var submit = document.getElementById('creditBuy-submit');
    var spinner = document.getElementById('spinner');
    var icon = document.querySelector('.creditBuy-data .iconCredit');
    var iconClass = icon.getAttribute('class');

    submit.setAttribute('disabled', 'disabled');
    input.setAttribute('disabled', 'disabled');
    spinner.classList.add('__visible');

    icon.setAttribute('class', iconClass + ' __gray');
  }
})();

