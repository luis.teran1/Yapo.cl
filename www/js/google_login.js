/* loginBconf */

var googleUser;

function onGoogleSuccess(googleUser) {
  var session = getCookie('acc_session');
  
  if (session) {
    return;
  }
  
  var gprofile = googleUser.getBasicProfile();
  var id_token = googleUser.getAuthResponse().id_token;

  console.log('Logged on google as: ' + gprofile.getName());

  var xhr = new XMLHttpRequest();
  xhr.open('POST', loginBconf.baseUrl + '/google/login');
  xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
  xhr.onload = function() {
    var response = JSON.parse(xhr.responseText);

    if (response.code == "LOGIN_OK") {
        console.log('Signed in as: ' + response.email);
        if (response.redirect) {
          location.href = decodeURIComponent(response.redirect);
        }
    }
  };
  xhr.send('source=web&token=' + id_token);

  hideLoading();
}

var googleConnect = (function() {
  'use strict';

  var auth2;
  var baseUrl; 
  var debug = false;
  var isConfigured = false;
  var attachButtonCallback;

  var onSuccess;
  var onError;
  var showLoading;

  return {
    attachSigninButton: attachSigninButton,
    startLoginGoogle: startLoginGoogle,
    config: config,
    goSignOut: goSignOut,
    runLogin: runLogin,
  };

  function config(options) {
    if (isConfigured) { return; }
    isConfigured = true;

    baseUrl = options.baseUrl;
    debug = options.debug;

    log('config', options);

    onSuccess = options.onSuccess || function() {};
    onError = options.onError || function() {};
  }

  function startLoginGoogle() {
    gapi.load('auth2', function() {
      auth2 = gapi.auth2.init({
        client_id: loginBconf.googleAppId,
        cookiepolicy: 'single_host_origin',
        scope: 'profile email'
      });

      auth2.then(function () {
        log('google api ready', auth2);
        if (attachButtonCallback) attachButtonCallback();
      });
    });
  };
  
  function log() {
    if (!debug) { return; }
    console.log.apply(console, arguments); // eslint-disable-line no-console
  };

  function errorConnect(error) {
    log('errorAtachButton',error);
    onError();
  }

  function attachSigninButton(element) {
    (auth2 == undefined) ? attachButtonCallback = function () { buttonHandler(element)} : buttonHandler(element);
  };

  function buttonHandler(element) {
    log('add buttonGoogle', element);
    auth2.attachClickHandler(element, {}, onSuccess, errorConnect);
  }

  function goSignOut() {
    var authGoogle = gapi.auth2.getAuthInstance();
    
    if (authGoogle != undefined) {
      auth2.signOut().then(function () {
        log('Google Logut','User signed out.');
      });
    }
    auth2.disconnect();
  };

  function runLogin() {
    console.log("running google login");
    gapi.auth2.getAuthInstance().signIn();
  };
})();

function initGoogle() {
  googleConnect.startLoginGoogle();
}
