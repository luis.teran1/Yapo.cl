/* globals Schibsted, messagingCenterConfig */

(function init() {
  function initializeWidget() {
    // messagingCenterConfig is a template generated var
    if (typeof messagingCenterConfig !== 'undefined') {
      Schibsted.Messaging.initWidget('#messagingCenter', messagingCenterConfig);
    }
  }

  document.addEventListener('DOMContentLoaded', initializeWidget);
})();

