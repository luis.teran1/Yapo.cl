var Yapo = Yapo || {};

Yapo.modalLogin = (function aiLogin () {
  function init (modalConfig) {
    var isLogged = document.querySelector('.account-info') !== null;

    var loginButton;
    var modal;

    if (isLogged) {
      return;
    }
    loginButton = document.getElementById('login-account-link');

    if (loginButton) {
      // open modal using the existent login button (Desktop)
      loginButton.click();
    }

    modal = document.querySelector('.loginBox');

    modal.classList.add(modalConfig.css_modifier);
    setLoginBoxContent(modalConfig);
    $(document).one('PgwModal::Close', closeModal);
  }

  function closeModal (e) {
    var custom = new CustomEvent('close-modal-favorites', e);
    document.dispatchEvent(custom);
  }

  function setLoginBoxContent (content) {
    var modal = document.querySelector('.loginBox');
    var element;

    var modalContent = [{
      selector: '.loginBox-header-title',
      html: content.title
    }, {
      selector: '.loginBox-header-subtitle',
      html: content.subtitle
    }, {
      selector: '.loginBox-footerText',
      html: content.footer
    }];

    modalContent.forEach(function setOptions (item) {
      element = modal.querySelector(item.selector);
      if (element) {
        modal.querySelector(item.selector).innerHTML = item.html;
      }
    });
    $(document).unbind('PgwModal::Close');
  }
  return init;
})();
